/******************************************************************
Project: Release 14.3 Date: 13.OCT.2020
Comments: STORE PROCEDURES
*******************************************************************/

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='GetTimeSpendOnTaskByProject')
DROP FUNCTION GetTimeSpendOnTaskByProject
GO
CREATE FUNCTION [dbo].[GetTimeSpendOnTaskByProject]
(
	@numDomainID NUMERIC(18,0)
	,@numTaskID NUMERIC(18,0)
	,@tintReturnType TINYINT
)    
RETURNS VARCHAR(500)    
AS
BEGIN
	DECLARE @vcTotalTimeSpendOnTask VARCHAR(50) = ''
	DECLARE @numTotalMinutes INT = 0

	DECLARE @TEMTIMELong TABLE
	(
		ID INT IDENTITY(1,1)
		,tintAction TINYINT
		,dtActionTime DATETIME
	)

	INSERT INTO @TEMTIMELong
	(
		tintAction
		,dtActionTime
	)
	SELECT
		tintAction
		,dtActionTime
	FROM
		StagePercentageDetailsTaskTimeLog SPDTTL
	WHERE
		SPDTTL.numDomainID=@numDomainID
		AND SPDTTL.numTaskID=@numTaskID
	ORDER BY
		dtActionTime
		,ID 

	DECLARE @i INT = 1
	DECLARE @iCount INT
	DECLARE @tintLastAction TINYINT
	DECLARE @dtLastActionTime DATETIME

	DECLARE @tintAction TINYINT
	DECLARE @dtActionTime DATETIME
	SELECT @iCount = COUNT(*) FROM @TEMTIMELong

	WHILE @i <= @iCount
	BEGIN
		SELECT
			@tintAction=tintAction
			,@dtActionTime=dtActionTime
		FROM
			@TEMTIMELong
		WHERE
			ID = @i

		IF @i = 1 
		BEGIN
		
			IF @tintAction <> 1
			BEGIN
				SET @vcTotalTimeSpendOnTask = 'Invalid time sequence'
				BREAK;
			END
			ELSE
			BEGIN
				SET @tintLastAction = @tintAction
				SET @dtLastActionTime = @dtActionTime
			END
		END
		ELSE
		BEGIN
			
			IF @tintLastAction = 1 AND @tintAction NOT IN (2,4)
			BEGIN
				SET @vcTotalTimeSpendOnTask = 'Invalid time sequence'
				BREAK;
			END
			ELSE IF @tintLastAction = 2 AND @tintAction NOT IN(3,4)
			BEGIN
				SET @vcTotalTimeSpendOnTask = 'Invalid time sequence'
				BREAK;
			END
			ELSE IF @tintLastAction = 3 AND @tintAction NOT IN (2,4)
			BEGIN
				SET @vcTotalTimeSpendOnTask = 'Invalid time sequence'
				BREAK;
			END
			ELSE IF @tintLastAction = 4
			BEGIN
				SET @vcTotalTimeSpendOnTask = 'Invalid time sequence'
				BREAK;
			END
			ELSE
			BEGIN
				IF @tintAction IN (2,4)
				BEGIN
					SET @numTotalMinutes = @numTotalMinutes + DATEDIFF(MINUTE,@dtLastActionTime,@dtActionTime)
				END

				SET @tintLastAction = @tintAction
				SET @dtLastActionTime = @dtActionTime
			END
		END

		SET @i = @i + 1
	END

	IF @vcTotalTimeSpendOnTask <> 'Invalid time sequence'
	BEGIN
		IF ISNULL(@tintReturnType,0) = 1
		BEGIN
			RETURN CONCAT('<ul class="list-inline"><li style="padding-right:0px">',(CASE WHEN (@numTotalMinutes / 60) > 0 THEN CONCAT('<label style="color:#3366ff">',(@numTotalMinutes / 60),(CASE WHEN (@numTotalMinutes / 60) > 1 THEN ' Hours' ELSE ' Hour' END),'</label>') ELSE '' END),'</li><li>',(CASE WHEN (@numTotalMinutes % 60) > 0 THEN CONCAT('<label>',@numTotalMinutes % 60,(CASE WHEN (@numTotalMinutes % 60) > 1 THEN ' Minutes' ELSE ' Minute' END),'</label>') ELSE '' END),'</li></ul>')
		END
		ELSE IF ISNULL(@tintReturnType,0) = 2
		BEGIN
			RETURN @numTotalMinutes
		END
		ELSE
		BEGIN
			RETURN FORMAT(@numTotalMinutes / 60,'00') + ':' + FORMAT(@numTotalMinutes % 60,'00')
		END
	END
	ELSE IF ISNULL(@tintReturnType,0) = 2
	BEGIN
		SET @vcTotalTimeSpendOnTask = '0'
	END
	
	RETURN @vcTotalTimeSpendOnTask
END

GO
/****** Object:  StoredProcedure [dbo].[USP_GetCategory]    Script Date: 08/08/2009 16:37:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                        
-- exec [dbo].[USP_GetCategory] 0,0,'',0,1,3
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcategory')
DROP PROCEDURE usp_getcategory
GO
CREATE PROCEDURE [dbo].[USP_GetCategory]                        
@numCatergoryId as numeric(9),                        
@byteMode as tinyint ,                  
@str as varchar(1000),                  
@numDomainID as numeric(9)=0,
@numSiteID AS NUMERIC(9)=0 ,
@numCurrentPage as numeric(9)=0,
@PageSize AS INT=20,
@numTotalPage as numeric(9) OUT,    
@numItemCode AS NUMERIC(9,0),
@SortChar CHAR(1) = '0',
@numCategoryProfileID NUMERIC(18,0)
               
as   

set nocount on                     
--selecting all categories                     
declare @strsql as varchar(8000)
DECLARE @strRowCount VARCHAR(8000)  


create table #TempCategory
(PKId numeric(9) identity,
numItemCode numeric(9),
vcItemName varchar(250));

                  
if @byteMode=0                        
BEGIN
	 ;WITH CategoryList
As 
( SELECT C1.numCategoryID,C1.vcCategoryName,C1.vcCategoryNameURL,C1.numDepCategory,C1.tintLevel,case when C1.bitLink=0 then 'No' when C1.bitLink=1 then 'Yes' end as Link,C1.vcLink as vcLink,CAST('' AS VARCHAR(1000)) as DepCategory,
	C1.intDisplayOrder,C1.vcDescription,
	CASE WHEN LEN(ISNULL(C1.vcDescription ,'')) > 0 THEN  
	CASE WHEN LEN(C1.vcDescription) > 40 THEN SUBSTRING(C1.vcDescription,0,40) + ' [...]' 
         ELSE C1.vcDescription + ' [...]'	END 
    ELSE
		 ISNULL(C1.vcDescription ,'')   
    END     
         AS vcShortDescription,1 AS [Level],
	CAST (REPLICATE('.',1) + C1.vcCategoryName as varchar(1000) )  as vcNewCategoryName,
	CAST(C1.numCategoryID AS VARCHAR(MAX)) AS [Order]
 FROM dbo.Category C1 WHERE C1.numDepCategory=0 AND  C1.numDomainID=@numDomainID AND C1.numCategoryProfileID = @numCategoryProfileID
 UNION ALL
 SELECT  C1.numCategoryID,C1.vcCategoryName,C1.vcCategoryNameURL,C1.numDepCategory,C1.tintLevel,case when C1.bitLink=0 then 'No' when C1.bitLink=1 then 'Yes' end as Link,C1.vcLink as vcLink,CL.vcCategoryName as DepCategory ,
	C1.intDisplayOrder,C1.vcDescription,
	CASE WHEN LEN(ISNULL(C1.vcDescription ,'')) > 0 THEN  
	CASE WHEN LEN(C1.vcDescription) > 40 THEN SUBSTRING(C1.vcDescription,0,40) + ' [...]' 
         ELSE C1.vcDescription + ' [...]'	END 
    ELSE
		 ISNULL(C1.vcDescription ,'')   
    END     
         AS vcShortDescription,CL.[Level] + 1 AS [Level],
	CAST (REPLICATE('.',[Level] * 3) + C1.vcCategoryName as varchar(1000) )  as vcNewCategoryName,
	[Order] + '.' + CAST(C1.numCategoryID AS VARCHAR(MAX)) AS [Order]
 FROM dbo.Category C1 
 INNER JOIN CategoryList CL
 ON CL.numCategoryID = C1.numDepCategory  WHERE  C1.numDomainID=@numDomainID  AND C1.numCategoryProfileID = @numCategoryProfileID
)   

SELECT *,(SELECT COUNT(*) FROM [SiteCategories] SC WHERE SC.[numSiteID] =@numSiteID AND SC.[numCategoryID]= C1.[numCategoryID]) AS ShowInECommerce,
ISNULL((SELECT COUNT(*) FROM [ItemCategory] WHERE [numCategoryID] = C1.[numCategoryID]),0) AS ItemCount FROM CategoryList C1 ORDER BY [Order]
end                                                
--deleting a category                      
else if @byteMode=1                        
begin     
	delete from SiteCategories where numCategoryID=@numCatergoryId                     
	delete from ItemCategory where numCategoryID=@numCatergoryId
	delete from Category where numCategoryID=@numCatergoryId       
	select 1                        
end                        
--selecting all catories where level is 1                       
else if @byteMode=2                      
begin                        
	select numCategoryID,vcCategoryName,vcCategoryNameURL,intDisplayOrder,vcDescription from Category                      
	where ((tintlevel=1 )  or (tintlevel<>1 and numDepCategory=@numCatergoryId))  and numCategoryID!=@numCatergoryId                      
	and numDomainID=@numDomainID                    
	ORDER BY [vcCategoryName]
end                      
--selecting subCategories                      
else if @byteMode=3                      
begin                        
	select numCategoryID,vcCategoryName,vcCategoryNameURL,intDisplayOrder,vcDescription  from Category                      
	where numDepCategory=@numCatergoryId                      
	ORDER BY [vcCategoryName]
end                      
                      
--selecting Items                      
if @byteMode=4                      
begin                        
	select numItemCode,vcItemName  from Item           
	where numDomainID=@numDomainID                       
end                      
                      
--selecting Item belongs to a category                      
else if @byteMode=5                      
begin    


	insert into #TempCategory
	                       
	select numItemCode,CONCAT(numItemCode,' - ',vcItemName) AS vcItemName  from Item                      
	join ItemCategory                       
	on numItemCode=numItemID                      
	where numCategoryID=@numCatergoryId           
	and numDomainID=@numDomainID                       
	ORDER BY [vcItemName]


	select @numTotalPage =COUNT(*) from #TempCategory

	if @numTotalPage<@PageSize 
		begin
			set @numTotalPage=1
		end
	else	
		begin
			set @numTotalPage=@numTotalPage/@PageSize
		end

	select * from #TempCategory  where PKId between (@numCurrentPage*@PageSize)- (@PageSize-1) and  @numCurrentPage*@PageSize

	select @numTotalPage

end                      
                    
else if @byteMode=6                    
begin                        

	select C1.numCategoryID,c1.vcCategoryName,((select Count(*) from Category C2 where C2.numDepCategory=C1.numCategoryID)+(select count(*) from ItemCategory C3 where C3.numCategoryID=C1.numCategoryID  ) ) as [Count],convert(integer,bitLink) as Type,vcLink,C1.intDisplayOrder,
	C1.vcDescription   
	from Category C1                     
	where tintlevel=1                     
	and numDomainID=@numDomainID   
	AND numCategoryProfileID=@numCategoryProfileID                   
                    
end                     
                    
else if @byteMode=7                    
begin                        

	select C1.numCategoryID,c1.vcCategoryName,((select Count(*) from Category C2 where C2.numDepCategory=C1.numCategoryID)+(select count(*) from ItemCategory C3 where C3.numCategoryID=C1.numCategoryID ) ) as [Count],convert(integer,bitLink) as Type,vcLink,C1.intDisplayOrder,
	C1.vcDescription,numDepCategory as Category
	from Category C1                     
	where C1.numDepCategory=@numCatergoryId and numDomainID=@numDomainID                      
	union                     
	select numItemCode,vcItemName,0 ,2,'',0,'',0  from Item                      
	join ItemCategory                       
	on numItemCode=numItemID                      
	where numCategoryID=@numCatergoryId  and numDomainID=@numDomainID                    
end                     
                
else if @byteMode=8                     
begin                    
     --kishan               
	set @strsql='select numItemCode,vcItemName,txtItemDesc,0,
	(
	SELECT TOP 1 II.vcPathForTImage 
    FROM dbo.ItemImages II 
        WHERE II.numItemCode = Item.numItemCode 
    AND II.bitDefault = 1 , AND II.bitIsImage = 1 
    AND II.numDomainID ='+ cast(@numDomainId as varchar(50))+') 
	vcPathForTImage ,
	1 as Type,monListPrice as price,0 as numQtyOnHand  from Item                    
	join ItemCategory                       
	on numItemCode=numItemID                      
	where numCategoryID in('+@str+')'                  
	exec (@strsql)                   
end                    
                  
else if @byteMode=9                  
begin                    
                    
	select distinct(numItemCode),vcItemName,txtItemDesc,0 , (SELECT TOP 1 vcPathForImage FROM ItemImages WHERE numDomainId = @numDomainId AND bitDefault = 1 AND numItemCode = Item.numItemCode) AS vcPathForImage,1 as Type,monListPrice as price,0 as numQtyOnHand from Item                      
	join ItemCategory                       
	on numItemCode=numItemID                      
	where ( vcItemName like '%'+@str+'%' or txtItemDesc like '%'+@str+'%') and numDomainID= @numDomainID                                    
end        
      
else if @byteMode=10      
begin       
	select ROW_NUMBER() OVER (ORDER BY Category Asc) AS RowNumber,* from (select   C1.numCategoryID,c1.vcCategoryName,convert(integer,bitLink) as Type,vcLink ,numDepCategory as Category,C1.intDisplayOrder,
	C1.vcDescription         
	from Category C1                     
	where  numDomainID=@numDomainID)X       
      
end 


--selecting Items not present in Category                   
else if @byteMode=11                    
begin     

	insert into #TempCategory
	         
	SELECT I.numItemCode, CONCAT(I.numItemCode,' - ',I.vcItemName) AS vcItemName FROM item  I LEFT OUTER JOIN [ItemCategory] IC ON IC.numItemID = I.numItemCode
	WHERE [numDomainID] =@numDomainID AND ISNULL(IC.numCategoryID,0) <> @numCatergoryId
	ORDER BY [vcItemName]
          
--	select numItemCode,vcItemName  from Item LEFT OUTER JOIN [ItemCategory] ON Item.[numItemCode] = [ItemCategory].[numCategoryID]
--	where [ItemCategory].[numCategoryID] <> @numCatergoryId
----numItemCode not in (select numItemID from ItemCategory where numCategoryID=@numCatergoryId)
--	and numDomainID=@numDomainID                      
--	ORDER BY [vcItemName]



	select @numTotalPage =COUNT(*) from #TempCategory

	set @numTotalPage=@numTotalPage/@PageSize

	select * from #TempCategory  where PKId between (@numCurrentPage*@PageSize)-(@PageSize-1) and  @numCurrentPage*@PageSize

	select @numTotalPage


end   


--selecting Items From Item Groups                  
else if @byteMode=12                
begin   

	insert into #TempCategory
	                     
	select numItemCode,vcItemName  from Item 
	where numItemGroup= @numCatergoryId  
	ORDER BY [vcItemName] 



	select @numTotalPage =COUNT(*) from #TempCategory

	set @numTotalPage=@numTotalPage/@PageSize

	select * from #TempCategory  where PKId between (@numCurrentPage*@PageSize)-(@PageSize-1) and  @numCurrentPage*@PageSize

	select @numTotalPage

              
end

-- Selecting Categories Site Wise and Sorted into Sort Order 
else if @byteMode=13
BEGIN
	SELECT  
		@numCategoryProfileID=CPS.numCategoryProfileID
		,@numDomainID=S.numDomainID
	FROM 
		Sites S
	JOIN
		CategoryProfileSites CPS
	ON
		S.numSiteID=CPS.numSiteID
	JOIN
		CategoryProfile CP
	ON
		CPS.numCategoryProfileID = CP.ID
	WHERE 
		S.numSiteID=@numSiteID


	DECLARE @bParentCatrgory BIT;
	SELECT @bParentCatrgory=vcAttributeValue FROM PageElementDetail WHERE [numSiteID] = @numSiteID AND numAttributeID=39
	SET @bParentCatrgory=ISNULL(@bParentCatrgory,0)

	DECLARE @tintDisplayCategory AS TINYINT
	SELECT @tintDisplayCategory = ISNULL(bitDisplayCategory,0) FROM dbo.eCommerceDTL WHERE numSiteID = @numSiteId

	
	IF ISNULL(@tintDisplayCategory,0) = 0 
	BEGIN
		SELECT C.numCategoryID,
			   C.vcCategoryName,
			   C.tintLevel,
			   convert(integer,C.bitLink) as TYPE,
			   C.vcLink AS vcLink,
			   numDepCategory as Category,C.intDisplayOrder,C.vcDescription 
		FROM   [SiteCategories] SC
			   INNER JOIN [Category] C ON SC.[numCategoryID] = C.[numCategoryID]
		WHERE  SC.[numSiteID] = @numSiteID
			   AND SC.[numCategoryID] = C.[numCategoryID] 
			   AND 1 = ( CASE WHEN @bParentCatrgory = 1 THEN tintLevel ELSE 1 END )
			   ORDER BY tintLevel , 
						ISNULL(C.intDisplayOrder,0),
						Category	
	END	
	ELSE
	BEGIN
		SELECT C.numCategoryID,
			   C.vcCategoryName,
			   C.tintLevel,
			   convert(integer,C.bitLink) as TYPE,
			   C.vcLink AS vcLink,
			   numDepCategory as Category,C.intDisplayOrder,C.vcDescription 
		FROM   [SiteCategories] SC
			   INNER JOIN [Category] C ON SC.[numCategoryID] = C.[numCategoryID]
			   LEFT JOIN ItemCategory IC ON IC.numCategoryID=C.numCategoryID
			   LEFT JOIN WareHouseItems WI ON WI.numDomainID=@numDomainID AND IC.numItemID=WI.numItemID  
		WHERE  SC.[numSiteID] = @numSiteID
			   AND SC.[numCategoryID] = C.[numCategoryID] 
			   AND 1 = ( CASE WHEN @bParentCatrgory = 1 THEN tintLevel ELSE 1 END )
			   AND ((ISNULL(numOnHand,0) > 0 OR ISNULL(numAllocation,0) > 0) OR tintLevel = 1)
		GROUP BY
			C.numCategoryID,
			C.vcCategoryName,
			C.tintLevel,
			C.bitLink,
			C.vcLink,
			numDepCategory,C.intDisplayOrder,C.vcDescription			   
		ORDER BY 
			tintLevel
			,ISNULL(C.intDisplayOrder,0)
			,Category
	END			

END

else if @byteMode=14
BEGIN

	SELECT C.numCategoryID,
		   C.vcCategoryName,
		   C.tintLevel,
		   convert(integer,C.bitLink) as TYPE,
		   C.vcLink AS vcLink,
		   numDepCategory as Category,C.intDisplayOrder,C.vcPathForCategoryImage,C.vcDescription 
	FROM   [SiteCategories] SC
		   INNER JOIN [Category] C
			 ON SC.[numCategoryID] = C.[numCategoryID]
	WHERE  SC.[numSiteID] = @numSiteID
		   AND SC.[numCategoryID] = C.[numCategoryID]
		   AND ISNULL(numDepCategory,0) = 0
		 ORDER BY ISNULL(C.intDisplayOrder,0),Category
END

else if @byteMode=15
BEGIN

DECLARE @bSubCatrgory BIT;

SELECT @bSubCatrgory=vcAttributeValue FROM PageElementDetail WHERE [numSiteID] = @numSiteID AND numAttributeID=40
SET @bSubCatrgory=ISNULL(@bSubCatrgory,0)

	SELECT C.numCategoryID,
		   C.vcCategoryName,
		   C.tintLevel,
		   convert(integer,C.bitLink) as TYPE,
		   C.vcLink AS vcLink,
		   numDepCategory as Category,C.intDisplayOrder,C.vcPathForCategoryImage,C.vcDescription 
		   
		   
	FROM   [SiteCategories] SC
		   INNER JOIN [Category] C
			 ON SC.[numCategoryID] = C.[numCategoryID]
	WHERE  SC.[numSiteID] = @numSiteID
		   AND SC.[numCategoryID] = C.[numCategoryID] AND numDepCategory=@numCatergoryId
		   AND 1=(CASE WHEN @bSubCatrgory=1 THEN 1 ELSE 0 END)
		    ORDER BY ISNULL(C.intDisplayOrder,0),Category
END

ELSE if @byteMode=16
BEGIN
 SELECT vcPathForCategoryImage FROM Category WHERE numCategoryID=@numCatergoryId AND numDomainID=@numDomainID
 
END
ELSE if @byteMode=17
BEGIN
 SELECT numCategoryID ,vcCategoryName, vcCategoryNameURL ,vcDescription,intDisplayOrder,vcPathForCategoryImage,numDepCategory,ISNULL(vcMetaTitle,'') vcMetaTitle,ISNULL(vcMetaKeywords,'') vcMetaKeywords,ISNULL(vcMetaDescription,'') vcMetaDescription FROM Category  WHERE numCategoryID = @numCatergoryId AND numDomainID=@numDomainID
END
ELSE IF @byteMode = 18
BEGIN
	SELECT
		*
	FROM
	(
		-- USED FOR HIERARCHICAL CATEGORY LIST
		SELECT 
			ROW_NUMBER() OVER ( ORDER BY vcCategoryName) AS RowNum,
			C1.numCategoryID,
			C1.vcCategoryName,
			C1.vcCategoryNameURL,
			NULLIF(numDepCategory,0) numDepCategory,
			ISNULL(numDepCategory,0) numDepCategory1,
			0 AS tintLevel,
			ISNULL((SELECT COUNT(*) FROM [ItemCategory] WHERE [numCategoryID] = C1.[numCategoryID]),0) AS ItemCount,
			ISNULL((SELECT COUNT(*) FROM [ItemCategory] WHERE [numCategoryID] IN (SELECT numCategoryID FROM category WHERE numDepCategory = C1.[numCategoryID])),0) AS ItemSubcategoryCount
			,ISNULL(bitVisible,1) bitVisible
		FROM 
			Category C1 
		WHERE 
			numDomainID =@numDomainID AND numCategoryProfileID = @numCategoryProfileID
	) TEMp
	ORDER BY
		vcCategoryName
END

ELSE IF @byteMode=19
BEGIN
	SELECT  
		@numCategoryProfileID=CPS.numCategoryProfileID
	FROM 
		Sites S
	JOIN
		CategoryProfileSites CPS
	ON
		S.numSiteID=CPS.numSiteID
	JOIN
		CategoryProfile CP
	ON
		CPS.numCategoryProfileID = CP.ID
	WHERE 
		S.numSiteID=@numSiteID

	BEGIN
	 ;WITH CategoryList
As 
( SELECT C1.numCategoryID,C1.vcCategoryName,C1.numDepCategory,C1.tintLevel,case when C1.bitLink=0 then 'No' when C1.bitLink=1 then 'Yes' end as Link,C1.vcLink as vcLink,CAST('' AS VARCHAR(1000)) as DepCategory,
	C1.intDisplayOrder,C1.vcDescription,
	CASE WHEN LEN(ISNULL(C1.vcDescription ,'')) > 0 THEN  
	CASE WHEN LEN(C1.vcDescription) > 40 THEN SUBSTRING(C1.vcDescription,0,40) + ' [...]' 
         ELSE C1.vcDescription + ' [...]'	END 
    ELSE
		 ISNULL(C1.vcDescription ,'')   
    END     
         AS vcShortDescription,1 AS [Level],
	CAST (REPLICATE('.',1) + C1.vcCategoryName as varchar(1000) )  as vcNewCategoryName,
	CAST(C1.numCategoryID AS VARCHAR(MAX)) AS [Order]
 FROM dbo.Category C1 WHERE C1.numDepCategory=0 AND  C1.numDomainID=@numDomainID AND C1.numCategoryProfileID=@numCategoryProfileID
 UNION ALL
 SELECT  C1.numCategoryID,C1.vcCategoryName,C1.numDepCategory,C1.tintLevel,case when C1.bitLink=0 then 'No' when C1.bitLink=1 then 'Yes' end as Link,C1.vcLink as vcLink,CL.vcCategoryName as DepCategory ,
	C1.intDisplayOrder,C1.vcDescription,
	CASE WHEN LEN(ISNULL(C1.vcDescription ,'')) > 0 THEN  
	CASE WHEN LEN(C1.vcDescription) > 40 THEN SUBSTRING(C1.vcDescription,0,40) + ' [...]' 
         ELSE C1.vcDescription + ' [...]'	END 
    ELSE
		 ISNULL(C1.vcDescription ,'')   
    END     
         AS vcShortDescription,CL.[Level] + 1 AS [Level],
	CAST (REPLICATE('.',[Level] * 3) + C1.vcCategoryName as varchar(1000) )  as vcNewCategoryName,
	[Order] + '.' + CAST(C1.numCategoryID AS VARCHAR(MAX)) AS [Order]
 FROM dbo.Category C1 
 INNER JOIN CategoryList CL
 ON CL.numCategoryID = C1.numDepCategory  WHERE  C1.numDomainID=@numDomainID AND C1.numCategoryProfileID=@numCategoryProfileID
)   

SELECT numCategoryID,vcCategoryName,(CASE WHEN  numDepCategory=0 THEN NULL ELSE numDepCategory END) AS numDepCategory ,tintLevel,Link,vcLink,DepCategory,(SELECT COUNT(*) FROM [SiteCategories] SC WHERE SC.[numSiteID] =@numSiteID AND SC.[numCategoryID]= C1.[numCategoryID]) AS ShowInECommerce,
ISNULL((SELECT COUNT(*) FROM [ItemCategory] WHERE [numCategoryID] = C1.[numCategoryID]),0) AS ItemCount FROM CategoryList C1 ORDER BY [Order]
END


DROP TABLE #TempCategory
END
/****** Object:  StoredProcedure [dbo].[Usp_getCorrespondance]    Script Date: 07/26/2008 16:17:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcorrespondance')
DROP PROCEDURE usp_getcorrespondance
GO
CREATE PROCEDURE [dbo].[usp_getcorrespondance]                          
@FromDate as datetime,                          
@toDate as datetime,                          
@numContactId as numeric,                          
@vcMessageFrom as varchar(200)='' ,                        
@tintSortOrder as numeric,                        
@SeachKeyword as varchar(100),                        
@CurrentPage as INT,                        
@PageSize as INT,                        
@TotRecs as numeric output,                        
@columnName as varchar(100),                        
@columnSortOrder as varchar(50),                        
@filter as numeric,                        
@numdivisionId as numeric =0,              
@numDomainID as numeric(9)=0,              
@numCaseID as numeric(9)=0,              
@ClientTimeZoneOffset INT,
@numOpenRecordID NUMERIC=0,
@tintMode TINYINT=0,
@bitOpenCommu BIT=0,
@numUserCntID NUMERIC=0,
@vcTypes VARCHAR(200)
as 
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
DECLARE @vcUserLoginEmail AS VARCHAR(MAX)=''
SET @vcUserLoginEmail = (SELECT 
distinct  
    stuff((
        select ',' +  replace(vcEmailID,'''','''''') FROM UserMaster AS U where numDomainID=@numDomainID 
		order by U.vcEmailID
        for xml path('')
    ),1,1,'') as userlist
from UserMaster where numDomainID=@numDomainID 
group by vcEmailID)
set @vcMessageFrom=replace(@vcMessageFrom,'''','|')
                         
if @filter>0 set @tintSortOrder=  @filter
              
if ((@filter <=2 and @tintSortOrder<=2) or (@tintSortOrder>2 and @filter>2))                 
begin  
                                                                  
 declare @strSql as nvarchar(MAX)  =''     
                                  
  declare @firstRec as integer                                                        
  declare @lastRec as integer                                                        
  set @firstRec= (@CurrentPage-1) * @PageSize                                                        
  set @lastRec= (@CurrentPage*@PageSize+1)                 
  set @strSql=''                

 if (@filter <=2 and @tintSortOrder<=2 and @bitOpenCommu=0)                 
 begin  
 
 declare @strCondition as varchar(MAX);
 declare @vcContactEmail as varchar(MAX)='';
              
  if @numdivisionId =0 and  @vcMessageFrom <> '' 
 BEGIN 
	If @filter = 1 --ReceivedMessages
		set  @strCondition=' and (vcFrom like ''%'+ REPLACE(@vcMessageFrom,'''','''''')+'%'')  ' 
	ELSE IF @filter=2 --SentMessages
		set  @strCondition=' and (vcTo like ''%'+ REPLACE(@vcMessageFrom,'''','''''')+'%'')  ' 
	ELSE
		set  @strCondition=' and (vcFrom like ''%'+ REPLACE(@vcMessageFrom,'''','''''')+'%'' or vcTo like ''%'+ REPLACE(@vcMessageFrom,'''','''''')+'%'')  ' 
 END
  else if @numdivisionId <>0 
 BEGIN

 /* This approach does not work as numEmailId stores who sent email, id of sender
  set @strCondition=' and HDR.numEmailID in (
  SELECT numEmailID FROM dbo.EmailMaster WHERE numDomainID= ' + CONVERT(VARCHAR(15),@numDomainID) +' AND numContactID 
  IN (SELECT numcontactid FROM dbo.AdditionalContactsInformation WHERE numDivisionID = ' + convert(varchar(15),@numDivisionID) + ' AND numDomainID = ' + CONVERT(VARCHAR(15),@numDomainID) +' ))';
*/
-- bug fix id 1591,1628 
  DECLARE @TnumContactID NUMERIC(9);set @strCondition=''

  select TOP 1 @TnumContactID=numContactID,@vcContactEmail=replace(vcEmail,'''','''''') from AdditionalContactsInformation where numdivisionid=@numdivisionId and len(vcEmail)>2
  ORDER BY numContactID ASC
  SET @vcContactEmail = ISNULL(@vcContactEmail,'')
  WHILE @TnumContactID>0
  BEGIN
   if len(@strCondition)>0
    SET @strCondition=@strCondition + ' or '

	If @filter = 1 --ReceivedMessages
		set  @strCondition = @strCondition + ' (vcFrom like ''%'+ REPLACE(@vcContactEmail,'''','''''') +'%'')  ' 
	ELSE IF @filter=2 --SentMessages
		set  @strCondition= @strCondition + ' (vcTo like ''%'+ REPLACE(@vcContactEmail,'''','''''') +'%'')  ' 
	ELSE
		set  @strCondition= @strCondition + ' (vcFrom like ''%'+ REPLACE(@vcContactEmail,'''','''''')+'%'' or vcTo like ''%'+ REPLACE(@vcContactEmail,'''','''''')+'%'')  '

   select TOP 1 @TnumContactID=numContactID,@vcContactEmail=vcEmail from AdditionalContactsInformation where numdivisionid=@numdivisionId and len(vcEmail)>2 and numContactID>@TnumContactID
   ORDER BY numContactID ASC

   IF @@rowcount = 0 SET @TnumContactID = 0 
  END
 
  if len(@strCondition)>0
   SET @strCondition= 'and (' + @strCondition + ')'
  else if @tintMode <> 6 AND @tintMode <> 5
   SET @strCondition=' and (1=0)' 
  --set @strCondition=' and DTL.numEmailId  in (select numEmailId from EmailMaster where vcEmailID in (select vcEmail  from AdditionalContactsInformation where numdivisionid='''+ convert(varchar(15),@numdivisionId)+''' ))    '          
  END
  else if @numContactId>0 
 BEGIN
  select @vcContactEmail=isnull(replace(vcEmail,'''',''''''),'') from AdditionalContactsInformation where numContactID=@numContactId and len(vcEmail)>2
  SET @vcContactEmail = ISNULL(@vcContactEmail,'')
  if len(@vcContactEmail)>0
	If @filter = 1 --ReceivedMessages
		set  @strCondition=' and (vcFrom like ''%'+ REPLACE(@vcContactEmail,'''','''''') +'%'')  ' 
	ELSE IF @filter=2 --SentMessages
		set  @strCondition=' and (vcTo like ''%'+ REPLACE(@vcContactEmail,'''','''''')+'%'')  ' 
	ELSE
		set  @strCondition=' and (vcFrom like ''%'+ REPLACE(@vcContactEmail,'''','''''')+'%'' or vcTo like ''%'+ REPLACE(@vcContactEmail,'''','''''')+'%'')' 
  else if @tintMode <> 6 AND @tintMode <> 5
   SET @strCondition=' and (1=0)'
 END
 else 
  SET @strCondition=''

  CREATE TABLE #TEMPData
  (
		numTotalRecords INT
		,tintCountType TINYINT
		,numEmailHstrID NUMERIC(18,0)
		,DelData VARCHAR(200)
		,bintCreatedOn DATETIME
		,[date] VARCHAR(100)
		,[Subject] VARCHAR(MAX)
		,[type] VARCHAR(100)
		,phone VARCHAR(100)
		,assignedto VARCHAR(100)
		,caseid NUMERIC(18,0)
		,vcCasenumber VARCHAR(200)
		,caseTimeid NUMERIC(18,0)
		,caseExpid NUMERIC(18,0)
		,tinttype NUMERIC(18,0)
		,dtCreatedDate DATETIME
		,[From] VARCHAR(MAX)
		,bitClosedflag BIT
		,bitHasAttachments BIT
		,vcBody VARCHAR(MAX)
		,InlineEdit VARCHAR(1000)
		,numCreatedBy NUMERIC(18,0)
		,numOrgTerId NUMERIC(18,0)
		,TypeClass VARCHAR(100)
  )


  set @strSql= 'INSERT INTO 
					#TEMPData 
				SELECT 
					COUNT(*) OVER() AS numTotalRecords
					,1 AS tintCountType
					,HDR.numEmailHstrID
					,convert(varchar(15),HDR.numEmailHstrID)+''~1~0''+convert(varchar(15),HDR.numUserCntId) as DelData
					,HDR.dtReceivedOn AS bintCreatedOn
					,dbo.FormatedDateTimeFromDate(DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+', dtReceivedOn),'+convert(varchar(15),@numDomainID)+') as [date]
					,vcSubject as [Subject]
					,CASE WHEN (SELECT COUNT(*) FROM dbo.SplitByString(vcFrom,''$^$'') WHERE Items 
					IN( select  replace(vcEmailID,'''''''','''''''') FROM UserMaster AS U where numDomainID='+convert(varchar(15),@numDomainID)+'))  > 0  THEN ''Email Out'' ELSE ''Email In'' END as [type]
					,'''' as phone
					,'''' as assignedto
					,0 as caseid
					,null as vcCasenumber
					,0 as caseTimeid
					,0 as caseExpid
					,hdr.tinttype
					,HDR.dtReceivedOn as dtCreatedDate
					,vcFrom + '','' + vcTo as [From]
					,0 as bitClosedflag
					,ISNULL(HDR.bitHasAttachments,0) bitHasAttachments
					,CASE WHEN LEN(Cast(vcBodyText AS VARCHAR(1000)))>150 THEN SUBSTRING(vcBodyText,0,150) + ''...'' ELSE Cast(vcBodyText AS VARCHAR(1000)) END AS vcBody
					,''''  AS InlineEdit
					,HDR.numUserCntId AS numCreatedBy
					,0 AS numOrgTerId
					,CASE WHEN (SELECT COUNT(*) FROM dbo.SplitByString(vcFrom,''$^$'') WHERE Items 
					IN( select  replace(vcEmailID,'''''''','''''''') FROM UserMaster AS U where numDomainID='+convert(varchar(15),@numDomainID)+'))  > 0 THEN ''redColorLabel'' ELSE ''greenColorLabel'' END as TypeClass
			  FROM EmailHistory HDR                                 
			  LEFT JOIN [Correspondence] CR ON CR.numEmailHistoryID = HDR.numEmailHstrID ' 
     

   set  @strSql=@strSql +' where ISNULL(HDR.bitInvisible,0) = 0 AND  (HDR.numNodeId IN (SELECT numNodeID FROM InboxTreeSort WHERE vcNodeName NOT IN (''All Mail'',''Important'',''Drafts'') AND numDomainID='+convert(varchar(15),@numDomainID)+')  OR HDR.numNodeId=0) AND HDR.numDomainID='+convert(varchar(15),@numDomainID)
   + ' AND (CR.numOpenRecordID = '+ convert(varchar(15),@numOpenRecordID) +' OR '+ convert(varchar(15),@numOpenRecordID) +'=0)'
   + (CASE WHEN ISNULL(@vcTypes,'') <> '' THEN ' AND 1 = 0' ELSE ' AND 1 = 1' END)
   
   IF(@FromDate IS NOT NULL AND @FromDate<>'')
   BEGIN
  SET @strSql = @strSql +' and  (dtReceivedOn between '''+ convert(varchar(30),@FromDate)+''' and '''+ convert(varchar(30),@ToDate)+''') '
  END
 set  @strSql=@strSql+ @strCondition


  IF @tintMode=1 SET @strSql=@strSql+ ' and  (CR.tintCorrType IN(1,3) OR '+ convert(varchar(15),@numOpenRecordID) +'=0)'
  IF @tintMode=2 SET @strSql=@strSql+ ' and  (CR.tintCorrType IN(2,4) OR '+ convert(varchar(15),@numOpenRecordID) +'=0)'
  IF @tintMode=5 SET @strSql=@strSql+ ' and  (CR.tintCorrType IN(5) OR '+ convert(varchar(15),@numOpenRecordID) +'=0)'
  IF @tintMode=6 SET @strSql=@strSql+ ' and  (CR.tintCorrType IN(6) OR '+ convert(varchar(15),@numOpenRecordID) +'=0)'
  
  if @filter =0 set  @strSql=@strSql+ ' and (hdr.tinttype=1 or hdr.tinttype=2 or  hdr.tinttype=5)'                  
  else if @filter =1 OR @filter =2 set  @strSql=@strSql+ ' and (hdr.tinttype=1 OR hdr.tinttype=2)'                                             
  if @filter > 2 set  @strSql=@strSql+ ' and hdr.tinttype=0'                 
                        
  if @tintSortOrder =0 and @SeachKeyword <> ''                 
  begin                
   set  @strSql=@strSql+ ' and (vcFrom like ''%'+ @SeachKeyword+'%'' or vcTo like ''%'+ @SeachKeyword+'%'' or vcsubject like ''%'+@SeachKeyword+'%'' or vcBody like ''%'+@SeachKeyword+'%'')'               
  end                
  if @tintSortOrder =1 and @SeachKeyword <> ''                 
  begin                
     set  @strSql=@strSql+ ' and (vcFrom like ''%'+ @SeachKeyword+'%'' or vcsubject like ''%'+@SeachKeyword+'%'' or  vcBody like ''%'+@SeachKeyword+'%'')'                 
  end                
  if @tintSortOrder =2 and @SeachKeyword <> ''                 
  begin                
     set  @strSql=@strSql+ ' and (vcTo like ''%'+ @SeachKeyword+'%'' or vcsubject like ''%'+@SeachKeyword+'%'' or  vcBody like ''%'+@SeachKeyword+'%'')'                 
  end                            
 end                

 

 if ((@filter=0 or @filter>2) and (@tintSortOrder=0 or @tintSortOrder>2))                
 begin                
  set  @strSql=@strSql+ '                     
  INSERT INTO #TEMPData SELECT COUNT(*) OVER() AS numTotalRecords, 2 AS tintCountType, c.numCommId as numEmailHstrID,convert(varchar(15),C.numCommId)+''~2~''+convert(varchar(15),C.numCreatedBy) as DelData,CASE WHEN c.bitTask = 973 THEN dtCreatedDate WHEN c.bitTask = 974 THEN dtCreatedDate WHEN c.bitTask = 971 THEN dtStartTime ELSE dtEndTime END AS bintCreatedOn,
  case when c.bitTask=973 then dbo.FormatedDateTimeFromDate(DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+', dtCreatedDate),'+convert(varchar(15),@numDomainID)+') 
  when c.bitTask=974 then dbo.FormatedDateTimeFromDate(DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+', dtEndTime),'+convert(varchar(15),@numDomainID)+') 
  when c.bitTask=971 THEN dbo.FormatedDateFromDate(DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+', dtEndTime),'+convert(varchar(15),@numDomainID)+') + '' '' + RIGHT(CONVERT(VARCHAR,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+', [dtStartTime]),100),7)  +'' - '' + RIGHT(CONVERT(VARCHAR,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+', [dtEndTime]),100),7)
  else  dbo.FormatedDateFromDate(DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+', dtEndTime),'+convert(varchar(15),@numDomainID)+') + '' '' + RIGHT(CONVERT(VARCHAR,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+', [dtStartTime]),100),7)  +'' - '' + RIGHT(CONVERT(VARCHAR,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+', [dtEndTime]),100),7)  END AS [date],
  convert(varchar(8000),textdetails) as [Subject],                                                                         
 dbo.fn_GetListItemName(c.bitTask)  AS [Type],                                                                 
  A1.vcFirstName +'' ''+ A1.vcLastName +'' ''+                                      
  case when A1.numPhone<>'''' then + A1.numPhone +case when A1.numPhoneExtension<>'''' then '' - '' + A1.numPhoneExtension else '''' end  else '''' end as [Phone],                        
  A2.vcFirstName+ '' ''+A2.vcLastName  as assignedto,                         
  c.caseid,                           
  (select top 1 vcCaseNumber from cases where cases.numcaseid= c.caseid )as vcCasenumber,                               
  isnull(caseTimeid,0)as caseTimeid,                              
  isnull(caseExpid,0)as caseExpid  ,                        
  1 as tinttype ,dtCreatedDate,'''' as [FROM]     ,isnull(C.bitClosedflag,0) as bitClosedflag,0 as bitHasAttachments,
'''' as vcBody,
''id="Tickler~184~False~''+CAST(c.bitTask AS VARCHAR)+''~''+CAST(c.numCommId AS VARCHAR)+''" class="editable_textarea" onmousemove="bgColor=''''lightgoldenRodYellow''''"  title="Double click to edit..." bgcolor="lightgoldenRodYellow" '' AS InlineEdit,
C.numCreatedBy,
ISNULL(Div.numTerId,0) AS numOrgTerId,
''blueColorLabel'' as TypeClass
  from  Communication C                  
  JOIN AdditionalContactsInformation  A1                                           
  ON c.numContactId = A1.numContactId
  LEFT JOIN CommunicationLinkedOrganization clo ON C.numCommID=clo.numCommID                                                                             
  left join AdditionalContactsInformation A2 on A2.numContactID=c.numAssign
  left join UserMaster UM on A2.numContactid=UM.numUserDetailId and isnull(UM.bitActivateFlag,0)=1                                                                           
  left join listdetails on numActivity=numListID                           
  LEFT JOIN [Correspondence] CR ON CR.numCommID = C.numCommID
  JOIN DivisionMaster Div                                                                         
 ON A1.numDivisionId = Div.numDivisionID    
  where  C.numDomainID=' +convert(varchar(15),@numDomainID)
  + (CASE WHEN ISNULL(@vcTypes,'') <> '' THEN ' AND C.bitTask IN (SELECT Id FROM dbo.SplitIDs(''' + ISNULL(@vcTypes,'') + ''','',''))' ELSE '' END) + 
  + ' AND (numOpenRecordID = '+ convert(varchar(15),@numOpenRecordID) +' OR '+ convert(varchar(15),@numOpenRecordID) +'=0)'
  if (@numdivisionId =0 and @numContactId<>0) set  @strSql=@strSql+' and C.numContactId='+ convert(varchar(15),@numContactId)                        
  if @numdivisionId <> 0 set  @strSql=@strSql+' and  (C.numDivisionId='+ convert(varchar(15),@numdivisionId) + ' OR clo.numDivisionID='+ convert(varchar(15),@numdivisionId) + ') '             
  if @numCaseID>0  set  @strSql=@strSql+' and  C.CaseId='+ convert(varchar(15),@numCaseID)                       
  if @filter =0  
  BEGIN
	IF @tintMode <> 9
	BEGIN
		
		IF(@FromDate IS NOT NULL AND @FromDate<>'')
		BEGIN
			SET @strSql=@strSql+ ' and dtStartTime  >= '''+ convert(varchar(30),@FromDate)+''' and dtStartTime  <= '''+ convert(varchar(30),@ToDate)  +''''
		END
	END
  END
  else
    SET @strSql=@strSql+ ' and c.bitTask=' + CONVERT(varchar(12),@filter)  
		IF(@FromDate IS NOT NULL AND @FromDate<>'')
		BEGIN
			SET @strSql=@strSql+ ' and ((dtStartTime  >= '''+ convert(varchar(30),@FromDate)+''' and dtStartTime  <= '''+ convert(varchar(30),@ToDate)  +''') OR (dtEndTime  >= '''+ convert(varchar(30),@FromDate)+''' and dtEndTime  <= '''+ convert(varchar(30),@ToDate)  +'''))'
		END
  IF @tintMode=1 SET @strSql=@strSql+ ' and  (CR.tintCorrType IN(1,3) OR '+ convert(varchar(15),@numOpenRecordID) +'=0)'
  IF @tintMode=2 SET @strSql=@strSql+ ' and  (CR.tintCorrType IN(2,4) OR '+ convert(varchar(15),@numOpenRecordID) +'=0)'
  IF @tintMode=5 SET @strSql=@strSql+ ' and  (CR.tintCorrType IN(5) OR '+ convert(varchar(15),@numOpenRecordID) +'=0)'
  IF @tintMode=6 SET @strSql=@strSql+ ' and  (CR.tintCorrType IN(6) OR '+ convert(varchar(15),@numOpenRecordID) +'=0)'
                          
  IF @tintSortOrder =0 and @SeachKeyword <>'' 
	SET @strSql=@strSql+ ' and textdetails like ''%'+@SeachKeyword+'%'''
  ELSE
	IF ISNULL(@filter,0) = 0 
	BEGIN
		IF @SeachKeyword <>''
		BEGIN
			SET @strSql=@strSql+ ' and textdetails like ''%'+@SeachKeyword+'%'''
		END
	END
	ELSE
	BEGIN
		SET @strSql=@strSql+ ' and c.bitTask=' + CONVERT(varchar(12),@filter)
		
		IF @SeachKeyword <>''
		BEGIN
			SET @strSql= @strSql + ' and textdetails like ''%'+@SeachKeyword+'%'''
		END                     
	END
  IF @bitOpenCommu=1  SET @strSql=@strSql+ ' and isnull(C.bitClosedflag,0)=0'
 end                




 --set @strSql='With tblCorr as (SELECT ROW_NUMBER() OVER (ORDER BY '+@columnName+' '+ @columnSortOrder+') AS RowNumber,X.* from('+@strSql+')X)'                

 set @strSql= CONCAT(@strSql,'; SELECT ROW_NUMBER() OVER(ORDER BY bintCreatedOn DESC) RowNumber, * FROM #TEMPData ORDER BY bintCreatedOn DESC OFFSET ',((@CurrentPage-1) * @PageSize),' ROWS FETCH NEXT ',@PageSize,' ROWS ONLY;')
                    

print CAST(@strSql AS NTEXT)          

set @strSql = CONCAT(@strSql,'; SET @TotRecs = ISNULL((SELECT SUM(numTotalRecords) FROM (SELECT tintCountType,numTotalRecords FROM #TEMPData GROUP BY tintCountType,numTotalRecords) X),0)')
EXEC sp_executesql @strSql,N'@TotRecs INT OUTPUT',@TotRecs OUTPUT;
                   
IF OBJECT_ID(N'tempdb..#TEMPData') IS NOT NULL
BEGIN
	DROP TABLE #TEMPData
END
                
              
end              
END
GO

/****** Object:  StoredProcedure [USP_GetCustomerCredits]    ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetCustomerCredits')
DROP PROCEDURE USP_GetCustomerCredits
GO
CREATE PROCEDURE [USP_GetCustomerCredits]                              
(
    @numDomainId numeric(18, 0),
    @numDivisionId numeric(18, 0),
    @numReferenceId NUMERIC(18,0),
    @tintMode TINYINT,
    @numCurrencyID NUMERIC(18,0)=0,
    @numAccountClass NUMERIC(18,0)=0
)            
AS                                           
BEGIN 

    DECLARE @numBaseCurrencyID NUMERIC
	    DECLARE @BaseCurrencySymbol nVARCHAR(3);SET @BaseCurrencySymbol='$'
	    
        SELECT @numBaseCurrencyID = ISNULL(numCurrencyID,0) FROM dbo.Domain WHERE numDomainId=@numDomainId
        SELECT @BaseCurrencySymbol = ISNULL(varCurrSymbol,'$') FROM dbo.Currency WHERE numCurrencyID = @numBaseCurrencyID
    
    
IF @tintMode=1
BEGIN
	
	DECLARE @tintDepositePage AS TINYINT;SET @tintDepositePage=0
	
	IF @numReferenceId>0
	BEGIN
		SELECT @tintDepositePage=tintDepositePage FROM depositMaster WHERE numDepositID=@numReferenceId
	END
	
	PRINT @tintDepositePage
	
	IF @tintDepositePage=3
	BEGIN
		SELECT tintDepositePage AS tintRefType,
		CASE WHEN tintDepositePage=3 THEN 'Credit Memo #' + CAST((SELECT TOP 1 [ReturnHeader].[vcRMA] FROM [dbo].[ReturnHeader] WHERE [ReturnHeader].[numReturnHeaderID] = DM.[numReturnHeaderID]) AS VARCHAR(20)) ELSE 'Unapplied Payment' END AS vcReference,
		numDepositId AS numReferenceID,CAST(monDepositAmount AS DECIMAL(19,2)) AS monAmount,
		ISNULL(CAST(monDepositAmount AS DECIMAL(19,2)),0) - ISNULL(CAST(monRefundAmount AS DECIMAL(19,2)),0) AS monNonAppliedAmount,
		ISNULL(numReturnHeaderID,0) AS numReturnHeaderID,CAST (1 AS BIT) AS bitIsPaid,ISNULL(CAST(monAppliedAmount AS DECIMAL(19,2)),0) AS monAmountPaid,
		DM.numCurrencyID,ISNULL(C.varCurrSymbol,'') varCurrSymbol,ISNULL(NULLIF(DM.fltExchangeRate,0),1) fltExchangeRateReceivedPayment
		,@BaseCurrencySymbol BaseCurrencySymbol,CASE WHEN tintDepositePage=3 THEN '' ELSE ISNULL(DM.vcReference,'') END  AS vcDepositReference 
		FROM DepositMaster DM LEFT JOIN dbo.Currency C ON C.numCurrencyID = DM.numCurrencyID
		WHERE DM.numDomainId=@numDomainId AND numDivisionID=@numDivisionID AND tintDepositePage IN(3)
		AND numDepositId=@numReferenceId AND (ISNULL(DM.numCurrencyID,@numBaseCurrencyID) = @numCurrencyID OR @numCurrencyID =0)
		AND (ISNULL(DM.numAccountClass,0)=@numAccountClass OR @numAccountClass=0)
		 
		UNION
		
		SELECT tintDepositePage AS tintRefType,CASE WHEN tintDepositePage=3 THEN 'Credit Memo #' + CAST((SELECT TOP 1 [ReturnHeader].[vcRMA] FROM [dbo].[ReturnHeader] WHERE [ReturnHeader].[numReturnHeaderID] = DM.[numReturnHeaderID]) AS VARCHAR(20)) ELSE 'Unapplied Payment' END AS vcReference,
		numDepositId AS numReferenceID,CAST(monDepositAmount AS DECIMAL(19,2)) AS monAmount,
		ISNULL(CAST(monDepositAmount AS DECIMAL(19,2)),0) - ISNULL(CAST(monAppliedAmount AS DECIMAL(19,2)),0)  /*- ISNULL(CAST(monRefundAmount AS DECIMAL(19,2)),0)*/ AS monNonAppliedAmount,
		ISNULL(numReturnHeaderID,0) AS numReturnHeaderID,CAST (0 AS BIT) AS bitIsPaid,0 AS monAmountPaid,DM.numCurrencyID,ISNULL(C.varCurrSymbol,'') varCurrSymbol,
		ISNULL(NULLIF(DM.fltExchangeRate,0),1) fltExchangeRateReceivedPayment,@BaseCurrencySymbol BaseCurrencySymbol,
		CASE WHEN tintDepositePage=3 THEN '' ELSE ISNULL(DM.vcReference,'') END AS vcDepositReference  
		FROM DepositMaster DM LEFT JOIN dbo.Currency C ON C.numCurrencyID = DM.numCurrencyID
		WHERE DM.numDomainId=@numDomainId AND numDivisionID=@numDivisionID AND tintDepositePage IN(2,3)
		AND (ISNULL(CAST(monDepositAmount AS DECIMAL(19,2)),0) - ISNULL(CAST(monAppliedAmount AS DECIMAL(19,2)),0)) > 0 AND numDepositId!=@numReferenceId
		AND (ISNULL(DM.numCurrencyID,@numBaseCurrencyID) = @numCurrencyID OR @numCurrencyID =0)
		AND (ISNULL(DM.numAccountClass,0)=@numAccountClass OR @numAccountClass=0)
	END
	ELSE
	BEGIN
		SELECT tintDepositePage AS tintRefType,CASE WHEN tintDepositePage=3 THEN 'Credit Memo #' + CAST((SELECT TOP 1 [ReturnHeader].[vcRMA] FROM [dbo].[ReturnHeader] WHERE [ReturnHeader].[numReturnHeaderID] = DM.[numReturnHeaderID]) AS VARCHAR(20)) ELSE 'Unapplied Payment' END AS vcReference,
		numDepositId AS numReferenceID,CAST(monDepositAmount AS DECIMAL(19,2)) AS monAmount,
		ISNULL(CAST(monDepositAmount AS DECIMAL(19,2)),0) - ISNULL(CAST(monAppliedAmount AS DECIMAL(19,2)),0) /*- ISNULL(CAST(monRefundAmount AS DECIMAL(19,2)),0)*/ AS monNonAppliedAmount,
		ISNULL(numReturnHeaderID,0) AS numReturnHeaderID,CAST (0 AS BIT) AS bitIsPaid,0 AS monAmountPaid,DM.numCurrencyID,ISNULL(C.varCurrSymbol,'') varCurrSymbol
		,ISNULL(NULLIF(DM.fltExchangeRate,0),1) fltExchangeRateReceivedPayment,@BaseCurrencySymbol BaseCurrencySymbol,CASE WHEN tintDepositePage=3 THEN '' ELSE ISNULL(DM.vcReference,'') END  AS vcDepositReference 
		FROM DepositMaster DM LEFT JOIN dbo.Currency C ON C.numCurrencyID = DM.numCurrencyID
		WHERE DM.numDomainId=@numDomainId AND numDivisionID=@numDivisionID AND tintDepositePage IN(2,3)
		AND (ISNULL(CAST(monDepositAmount AS DECIMAL(19,2)),0) - ISNULL(CAST(monAppliedAmount AS DECIMAL(19,2)),0)) > 0 AND numDepositId!=@numReferenceId
		AND (ISNULL(DM.numCurrencyID,@numBaseCurrencyID) = @numCurrencyID OR @numCurrencyID =0)
		AND (ISNULL(DM.numAccountClass,0)=@numAccountClass OR @numAccountClass=0)
		ORDER BY numDepositId DESC
	END		
	
--UNION 
--
--SELECT 2 AS tintRefType,numReturnHeaderID AS numReferenceID,monAmount,monAmountUsed AS monAppliedAmount FROM ReturnHeader WHERE numDomainId=@numDomainId AND numDivisionID=@numDivisionID AND tintReturnType IN (1,4)
END

ELSE IF @tintMode=2
BEGIN

	DECLARE @numReturnHeaderID AS NUMERIC(18,0);SET @numReturnHeaderID=0
	
	IF @numReferenceId>0
	BEGIN
		SELECT @numReturnHeaderID=numReturnHeaderID FROM BillPaymentHeader WHERE numBillPaymentID=@numReferenceId
	END
	
	IF @numReturnHeaderID>0
	BEGIN
		SELECT 1 AS tintRefType,RH.vcRMA AS vcReference
		,BPH.numBillPaymentID AS numReferenceID,monPaymentAmount AS monAmount,ISNULL(monPaymentAmount,0) - ISNULL(monRefundAmount,0) AS monNonAppliedAmount,CAST (1 AS BIT) AS bitIsPaid,ISNULL(monAppliedAmount,0) AS monAmountPaid,ISNULL(BPH.numReturnHeaderID,0) AS numReturnHeaderID,
		BPH.numCurrencyID
						,ISNULL(C.varCurrSymbol,'') varCurrSymbol
						,ISNULL(NULLIF(BPH.fltExchangeRate,0),1) fltExchangeRateReceivedPayment
						,@BaseCurrencySymbol BaseCurrencySymbol
		FROM BillPaymentHeader BPH LEFT JOIN dbo.Currency C ON C.numCurrencyID = BPH.numCurrencyID 
		LEFT JOIN ReturnHeader RH ON RH.numReturnHeaderID=ISNULL(BPH.numReturnHeaderID,0)
		WHERE BPH.numDomainId=@numDomainId AND BPH.numDivisionID=@numDivisionID 
		 AND numBillPaymentID=@numReferenceId AND (ISNULL(BPH.numCurrencyID,@numBaseCurrencyID) = @numCurrencyID OR @numCurrencyID =0)
		 AND (ISNULL(BPH.numAccountClass,0)=@numAccountClass OR @numAccountClass=0)
		 
		UNION
		
		SELECT CASE WHEN ISNULL(BPH.numReturnHeaderID,0)>0 THEN 1 ELSE 0 END AS tintRefType,
			  CASE WHEN ISNULL(BPH.numReturnHeaderID,0)>0 THEN RH.vcRMA ELSE 'Unapplied Bill Payment' END AS vcReference
		,BPH.numBillPaymentID AS numReferenceID,monPaymentAmount AS monAmount,ISNULL(monPaymentAmount,0) - ISNULL(monAppliedAmount,0) - ISNULL(monRefundAmount,0) AS monNonAppliedAmount,CAST (0 AS BIT) AS bitIsPaid,0 AS monAmountPaid,ISNULL(BPH.numReturnHeaderID,0) AS numReturnHeaderID,
		BPH.numCurrencyID
						,ISNULL(C.varCurrSymbol,'') varCurrSymbol
						,ISNULL(NULLIF(BPH.fltExchangeRate,0),1) fltExchangeRateReceivedPayment
						,@BaseCurrencySymbol BaseCurrencySymbol
		FROM BillPaymentHeader BPH LEFT JOIN dbo.Currency C ON C.numCurrencyID = BPH.numCurrencyID 
		LEFT JOIN ReturnHeader RH ON RH.numReturnHeaderID=ISNULL(BPH.numReturnHeaderID,0)
		WHERE BPH.numDomainId=@numDomainId AND BPH.numDivisionID=@numDivisionID 
		AND (ISNULL(CAST(monPaymentAmount AS DECIMAL(19,2)),0) - ISNULL(CAST(monAppliedAmount AS DECIMAL(19,2)),0)) > 0 AND numBillPaymentID!=@numReferenceId
		AND (ISNULL(BPH.numCurrencyID,@numBaseCurrencyID) = @numCurrencyID OR @numCurrencyID =0)
		AND (ISNULL(BPH.numAccountClass,0)=@numAccountClass OR @numAccountClass=0)
		
	END
	ELSE
	BEGIN
		SELECT CASE WHEN ISNULL(BPH.numReturnHeaderID,0)>0 THEN 1 ELSE 0 END AS tintRefType,
			  CASE WHEN ISNULL(BPH.numReturnHeaderID,0)>0 THEN RH.vcRMA ELSE 'Unapplied Bill Payment' END AS vcReference
		,BPH.numBillPaymentID AS numReferenceID,monPaymentAmount AS monAmount,ISNULL(monPaymentAmount,0) - ISNULL(monAppliedAmount,0) - ISNULL(monRefundAmount,0) AS monNonAppliedAmount,CAST (0 AS BIT) AS bitIsPaid,0 AS monAmountPaid  ,ISNULL(BPH.numReturnHeaderID,0) AS numReturnHeaderID,
		BPH.numCurrencyID
						,ISNULL(C.varCurrSymbol,'') varCurrSymbol
						,ISNULL(NULLIF(BPH.fltExchangeRate,0),1) fltExchangeRateReceivedPayment
						,@BaseCurrencySymbol BaseCurrencySymbol
		FROM BillPaymentHeader BPH LEFT JOIN dbo.Currency C ON C.numCurrencyID = BPH.numCurrencyID 
		LEFT JOIN ReturnHeader RH ON RH.numReturnHeaderID=ISNULL(BPH.numReturnHeaderID,0)
		WHERE BPH.numDomainId=@numDomainId AND BPH.numDivisionID=@numDivisionID 
		AND (ISNULL(CAST(monPaymentAmount AS DECIMAL(19,2)),0) - ISNULL(CAST(monAppliedAmount AS DECIMAL(19,2)),0)) > 0.01 AND numBillPaymentID!=@numReferenceId
		AND (ISNULL(BPH.numCurrencyID,@numBaseCurrencyID) = @numCurrencyID OR @numCurrencyID =0)
		AND (ISNULL(BPH.numAccountClass,0)=@numAccountClass OR @numAccountClass=0)
	END		
	
--	SELECT tintReturnType AS tintRefType,'Purchase Return Credit' AS vcReference,numReturnHeaderID AS numReferenceID,monBizDocAmount AS monAmount, ISNULL(monBizDocAmount,0) - ISNULL(monBizDocUsedAmount,0) AS monNonAppliedAmount,CAST (0 AS BIT) AS bitIsPaid,0 AS monAmountPaid  
--		FROM ReturnHeader WHERE numDomainId=@numDomainId AND numDivisionID=@numDivisionID
--		AND numReturnHeaderID NOT IN (SELECT numReturnHeaderID FROM ReturnPaymentHistory WHERE numReferenceID=@numReferenceId AND tintReferenceType=2)
--		AND (ISNULL(monBizDocAmount,0) - ISNULL(monBizDocUsedAmount,0)) >0 AND tintReturnType=2 AND tintReceiveType=2
--		
--	UNION
--	
--	SELECT tintReturnType AS tintRefType,'Purchase Return Credit' AS vcReference,RH.numReturnHeaderID AS numReferenceID,RH.monBizDocAmount, ISNULL(RH.monBizDocAmount,0) - ISNULL(RH.monBizDocUsedAmount,0) + ISNULL(SUM(ISNULL(RPH.monAmount,0)),0) AS monNonAppliedAmount,CAST (1 AS BIT) AS bitIsPaid,ISNULL(SUM(ISNULL(RPH.monAmount,0)),0) AS monAmountPaid  
--		FROM ReturnHeader RH JOIN ReturnPaymentHistory RPH ON RH.numReturnHeaderID=RPH.numReturnHeaderID  
--		WHERE RH.numDomainId=@numDomainId AND RH.numDivisionID=@numDivisionID 
--		AND RPH.numReferenceID=@numReferenceId AND RPH.tintReferenceType=2 AND tintReturnType=2 AND tintReceiveType=2
--		GROUP BY RH.numReturnHeaderID,RH.monBizDocAmount,RH.monBizDocUsedAmount,RH.tintReturnType
--	
	
END
  
END
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetPageNavigationAuthorizationDetails' ) 
    DROP PROCEDURE USP_GetPageNavigationAuthorizationDetails
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- EXEC USP_GetPageNavigationAuthorizationDetails 1,1
CREATE PROCEDURE USP_GetPageNavigationAuthorizationDetails
    (
      @numGroupID NUMERIC(18, 0),
      @numTabID NUMERIC(18, 0),
      @numDomainID NUMERIC(18, 0)
    )
AS 
    SET NOCOUNT ON
    SET TRANSACTION ISOLATION LEVEL READ COMMITTED
  
    BEGIN
		/*
		SELECT * FROM dbo.TabMaster
		SELECT * FROM dbo.ModuleMaster
		SELECT * FROM 
		*/
        PRINT @numTabID 
        DECLARE @numModuleID AS NUMERIC(18, 0)
        SELECT TOP 1
                @numModuleID = numModuleID
        FROM    dbo.PageNavigationDTL
        WHERE   numTabID = @numTabID 
        PRINT @numModuleID
        
        IF @numModuleID = 10 
            BEGIN
                PRINT 10
                SELECT  *
                FROM    ( SELECT    ISNULL(( SELECT TOP 1
                                                    TNA.[numPageAuthID]
                                             FROM   dbo.TreeNavigationAuthorization TNA
                                             WHERE  TNA.numPageNavID = PND.[numPageNavID]
                                                    AND TNA.numDomainID = @numDomainID
                                                    AND TNA.numGroupID = @numGroupID
                                                    AND TNA.numTabID = @numTabID
                                           ), 0) AS [numPageAuthID],
                                    ISNULL(@numGroupID, 0) AS [numGroupID],
                                    ISNULL(PND.[numTabID], 0) AS [numTabID],
                                    ISNULL(PND.[numPageNavID], 0) AS [numPageNavID],
                                    ISNULL(( SELECT TOP 1
                                                    TNA.[bitVisible]
                                             FROM   dbo.TreeNavigationAuthorization TNA
                                             WHERE  TNA.numPageNavID = PND.[numPageNavID]
                                                    AND TNA.numDomainID = @numDomainID
                                                    AND TNA.numGroupID = @numGroupID
                                                    AND TNA.numTabID = @numTabID
                                           ), 0) AS [bitVisible],
                                    @numDomainID AS [numDomainID],
                                    ( SELECT TOP 1
                                                vcPageNavName
                                      FROM      dbo.PageNavigationDTL
                                      WHERE     PageNavigationDTL.numPageNavID = PND.numPageNavID
                                    ) AS [vcNodeName],
                                    ISNULL(( SELECT TOP 1
                                                    TNA.tintType
                                             FROM   dbo.TreeNavigationAuthorization TNA
                                             WHERE  TNA.numPageNavID = PND.[numPageNavID]
                                                    AND TNA.numDomainID = @numDomainID
                                                    AND TNA.numGroupID = @numGroupID
                                                    AND TNA.numTabID = @numTabID
                                           ), 1) AS [tintType]
                          FROM      dbo.PageNavigationDTL PND
                          WHERE     ISNULL(PND.bitVisible, 0) = 1
                                    AND PND.numTabID = @numTabID
                                    AND numModuleID = @numModuleID
                          UNION ALL
                          SELECT    ISNULL(( SELECT TOP 1
                                                    TNA.[numPageAuthID]
                                             FROM   dbo.TreeNavigationAuthorization TNA
                                             WHERE  TNA.numPageNavID = LD.[numListItemID]
                                                    AND TNA.numDomainID = @numDomainID
                                                    AND TNA.numGroupID = @numGroupID
                                                    AND TNA.numTabID = @numTabID
                                           ), 0) AS [numPageAuthID],
                                    @numGroupID AS [numGroupID],
                                    1 AS [numTabID],
                                    ISNULL(LD.[numListItemID], 0) AS [numPageNavID],
                                    ISNULL(( SELECT TOP 1
                                                    TNA.[bitVisible]
                                             FROM   dbo.TreeNavigationAuthorization TNA
                                             WHERE  TNA.numPageNavID = LD.[numListItemID]
                                                    AND TNA.numDomainID = @numDomainID
                                                    AND TNA.numGroupID = @numGroupID
                                                    AND TNA.numTabID = @numTabID
                                           ), 0) AS [bitVisible],
                                    ISNULL(LD.[numDomainID], 0) AS [numDomainID],
                                    ISNULL(vcData, '') + 's' AS [vcNodeName],
                                    ISNULL(( SELECT TOP 1
                                                    TNA.tintType
                                             FROM   dbo.TreeNavigationAuthorization TNA
                                             WHERE  TNA.numPageNavID = LD.[numListItemID]
                                                    AND TNA.numDomainID = @numDomainID
                                                    AND TNA.numGroupID = @numGroupID
                                                    AND TNA.numTabID = @numTabID
                                           ), 1) AS [tintType]
                          FROM      dbo.ListDetails LD
						--LEFT JOIN dbo.TreeNavigationAuthorization TNA ON LD.numListItemID = TNA.numPageNavID 
                          WHERE     numListID = 27
                                    AND LD.numDomainID = @numDomainID
                                    AND ISNULL(constFlag, 0) = 0
						--AND ( numTabID = (CASE WHEN @numModuleID = 10 THEN 1 ELSE 0 END) OR ISNULL(numTabID,0) = 0)
                          UNION ALL
                          SELECT    ISNULL(( SELECT TNA.[numPageAuthID]
                                             FROM   dbo.TreeNavigationAuthorization TNA
                                             WHERE  TNA.numPageNavID = LD.[numListItemID]
                                                    AND TNA.numDomainID = @numDomainID
                                                    AND TNA.numGroupID = @numGroupID
                                                    AND TNA.numTabID = @numTabID
                                           ), 0) AS [numPageAuthID],
                                    @numGroupID AS [numGroupID],
                                    1 AS [numTabID],
                                    ISNULL(LD.[numListItemID], 0) AS [numPageNavID],
                                    ISNULL(( SELECT TNA.[bitVisible]
                                             FROM   dbo.TreeNavigationAuthorization TNA
                                             WHERE  TNA.numPageNavID = LD.[numListItemID]
                                                    AND TNA.numDomainID = @numDomainID
                                                    AND TNA.numGroupID = @numGroupID
                                                    AND TNA.numTabID = @numTabID
                                           ), 0) AS [bitVisible],
                                    @numDomainID AS [numDomainID],
                                    ISNULL(vcData, '') + 's' AS [vcNodeName],
                                    ISNULL(( SELECT TNA.tintType
                                             FROM   dbo.TreeNavigationAuthorization TNA
                                             WHERE  TNA.numPageNavID = LD.[numListItemID]
                                                    AND TNA.numDomainID = @numDomainID
                                                    AND TNA.numGroupID = @numGroupID
                                                    AND TNA.numTabID = @numTabID
                                           ), 1) AS [tintType]
                          FROM      dbo.ListDetails LD
						--LEFT JOIN dbo.TreeNavigationAuthorization TNA ON LD.numListItemID = TNA.numPageNavID AND ISNULL(TNA.[numDomainID], 0) = @numDomainID
                          WHERE     numListID = 27
                                    AND ISNULL(constFlag, 0) = 1
                        ) TABLE1
                ORDER BY numPageNavID
            END
		
        IF @numModuleID = 14 
            BEGIN
                IF NOT EXISTS ( SELECT  *
                                FROM    TreeNavigationAuthorization
                                WHERE   numTabID = 8
                                        AND numDomainID = 1
                                        AND numGroupID = 1 ) 
                    BEGIN
                        SELECT  TNA.numPageAuthID,
                                TNA.numGroupID,
                                TNA.numTabID,
                                TNA.numPageNavID,
                                TNA.bitVisible,
                                TNA.numDomainID,
                                ISNULL(PND.vcPageNavName, '') AS [vcNodeName],
                                tintType
                        FROM    PageNavigationDTL PND
                                JOIN dbo.TreeNavigationAuthorization TNA ON PND.numPageNavID = TNA.numPageNavID  --AND PND.numTabID = TNA.numTabID       
                        WHERE   1 = 1
                                --AND ISNULL(TNA.bitVisible, 0) = 1
                                AND ISNULL(PND.bitVisible, 0) = 1
                                AND numModuleID = @numModuleID
                                AND TNA.numDomainID = @numDomainID
                                AND numGroupID = @numGroupID
                        UNION
                        SELECT  0 numPageAuthID,
                                @numGroupID numGroupID,
                                @numTabID numTabID,
                                1111 numPageNavID,
                                1 bitVisible,
                                @numDomainID numDomainID,
                                'Regular Documents' AS [vcNodeName],
                                1 tintType
                        UNION
                        SELECT  0 numPageAuthID,
                                @numGroupID numGroupID,
                                @numTabID numTabID,
                                Ld.numListItemId,
                                1 bitVisible,
                                @numDomainID numDomainID,
                                ISNULL(vcData, '') AS [vcNodeName],
                                1 tintType
                        FROM    listdetails Ld
                                LEFT JOIN listorder LO ON Ld.numListItemID = LO.numListItemID
                                                          AND Lo.numDomainId = @numDomainID
                        WHERE   Ld.numListID = 29
                                AND ( constFlag = 1
                                      OR Ld.numDomainID = @numDomainID
                                    )
                        UNION
                        SELECT  0 numPageAuthID,
                                @numGroupID numGroupID,
                                @numTabID numTabID,
                                ld.numListItemID numPageNavID,
                                1 bitVisible,
                                @numDomainID numDomainID,
                                ISNULL(vcData, '') AS [vcNodeName],
                                1 tintType
                        FROM    listdetails Ld
                                LEFT JOIN listorder LO ON Ld.numListItemID = LO.numListItemID
                                                          AND lo.numDomainId = @numDomainID
                                INNER JOIN AuthoritativeBizDocs AB ON ( LD.numListItemID = AB.numAuthoritativeSales )
                        WHERE   LD.numListID = 27
                                AND ( constFlag = 1
                                      OR LD.numDomainID = @numDomainID
                                    )
                                AND AB.numDomainID = @numDomainID
                        UNION
                        SELECT  0 numPageAuthID,
                                @numGroupID numGroupID,
                                @numTabID numTabID,
                                ld.numListItemID numPageNavID,
                                1 bitVisible,
                                @numDomainID numDomainID,
                                ISNULL(vcData, '') AS [vcNodeName],
                                1 tintType
                        FROM    listdetails Ld
                                LEFT JOIN listorder LO ON Ld.numListItemID = LO.numListItemID
                                                          AND lo.numDomainId = @numDomainID
                                INNER JOIN AuthoritativeBizDocs AB ON ( ld.numListItemID = AB.numAuthoritativePurchase )
                        WHERE   LD.numListID = 27
                                AND ( constFlag = 1
                                      OR LD.numDomainID = @numDomainID
                                    )
                                AND AB.numDomainID = @numDomainID
                        UNION
                        SELECT  0 numPageAuthID,
                                @numGroupID numGroupID,
                                @numTabID numTabID,
                                0 numPageNavID,
                                1 bitVisible,
                                @numDomainID numDomainID,
                                ISNULL(LT.vcData, '') AS [vcNodeName],
                                1 tintType
                        FROM    ListDetails LT
						OUTER APPLY
						(SELECT * FROM listdetails WHERE numListID = 27
                                AND (constFlag = 1 OR numDomainID = @numDomainID)
                                AND (numListItemID IN (
                                      SELECT    AB.numAuthoritativeSales
                                      FROM      AuthoritativeBizDocs AB
                                      WHERE     AB.numDomainId = @numDomainID )
                                      OR numListItemID IN (
                                      SELECT    AB.numAuthoritativePurchase
                                      FROM      AuthoritativeBizDocs AB
                                      WHERE     AB.numDomainId = @numDomainID )
                                    )) ld
                        WHERE   LT.numListID = 11
                                AND Lt.numDomainID = @numDomainID
                        UNION
                        SELECT  0 numPageAuthID,
                                @numGroupID numGroupID,
                                @numTabID numTabID,
                                2222 numPageNavID,
                                1 bitVisible,
                                @numDomainID numDomainID,
                                'Other BizDocs' AS [vcNodeName],
                                1 tintType
                        UNION
                        SELECT  0 numPageAuthID,
                                @numGroupID numGroupID,
                                @numTabID numTabID,
                                ld.numListItemID numPageNavID,
                                1 bitVisible,
                                @numDomainID numDomainID,
                                ISNULL(vcData, '') AS [vcNodeName],
                                1 tintType
                        FROM    listdetails Ld
                        WHERE   ld.numListID = 27
                                AND ( constFlag = 1
                                      OR LD.numDomainID = @numDomainID
                                    )
                                AND LD.numListItemID NOT IN (
                                SELECT  AB.numAuthoritativeSales
                                FROM    AuthoritativeBizDocs AB
                                WHERE   AB.numDomainID = @numDomainID )
                                AND LD.numListItemID NOT IN (
                                SELECT  AB.numAuthoritativePurchase
                                FROM    AuthoritativeBizDocs AB
                                WHERE   AB.numDomainID = @numDomainID )
                        UNION
                        SELECT  0 numPageAuthID,
                                @numGroupID numGroupID,
                                @numTabID numTabID,
                                ls.numListItemID numPageNavID,
                                1 bitVisible,
                                @numDomainID numDomainID,
                                ISNULL(ls.vcData, '') AS [vcNodeName],
                                1 tintType
                        FROM    listdetails Ld,
                                ( SELECT    *
                                  FROM      ListDetails LS
                                  WHERE     LS.numDomainID = @numDomainID
                                            AND LS.numListID = 11
                                ) LS
                        WHERE   ld.numListID = 27
                                AND ( ld.constFlag = 1
                                      OR LD.numDomainID = @numDomainID
                                    )
                                AND LD.numListItemID NOT IN (
                                SELECT  AB.numAuthoritativeSales
                                FROM    AuthoritativeBizDocs AB
                                WHERE   AB.numDomainID = @numDomainID
                                UNION
                                SELECT  AB.numAuthoritativePurchase
                                FROM    AuthoritativeBizDocs AB
                                WHERE   AB.numDomainID = @numDomainID )
                        ORDER BY tintType,
                                numPageNavID
                    END
            END
        ELSE 
            BEGIN
				
                SELECT  *
                FROM    ( SELECT    TNA.numPageAuthID,
                                    TNA.numGroupID,
                                    TNA.numTabID,
                                    TNA.numPageNavID,
                                    TNA.bitVisible,
                                    TNA.numDomainID,
                                    ISNULL(PND.vcPageNavName, '') AS [vcNodeName],
                                    tintType
                          FROM      PageNavigationDTL PND
                                    JOIN dbo.TreeNavigationAuthorization TNA ON PND.numPageNavID = TNA.numPageNavID  --AND PND.numTabID = TNA.numTabID       
                          WHERE     1 = 1
                                    --AND ISNULL(TNA.bitVisible, 0) = 1
                                    AND ISNULL(PND.bitVisible, 0) = 1
                                    AND numModuleID = @numModuleID
									AND TNA.numTabID = @numTabID
                                    AND TNA.numDomainID = @numDomainID
                                    AND numGroupID = @numGroupID
                          UNION
                          SELECT    0 numPageAuthID,
                                    @numGroupID numGroupID,
                                    @numTabID numTabID,
                                    1111 numPageNavID,
                                    1 bitVisible,
                                    @numDomainID numDomainID,
                                    'Regular Documents' AS [vcNodeName],
                                    1 tintType
                          UNION
                          SELECT    0 numPageAuthID,
                                    @numGroupID numGroupID,
                                    @numTabID numTabID,
                                    Ld.numListItemId,
                                    1 bitVisible,
                                    @numDomainID numDomainID,
                                    ISNULL(vcData, '') AS [vcNodeName],
                                    1 tintType
                          FROM      listdetails Ld
                                    LEFT JOIN listorder LO ON Ld.numListItemID = LO.numListItemID
                                                              AND Lo.numDomainId = @numDomainID
                          WHERE     Ld.numListID = 29
                                    AND ( constFlag = 1
                                          OR Ld.numDomainID = @numDomainID
                                        )
                          UNION
                          SELECT    0 numPageAuthID,
                                    @numGroupID numGroupID,
                                    @numTabID numTabID,
                                    ld.numListItemID numPageNavID,
                                    1 bitVisible,
                                    @numDomainID numDomainID,
                                    ISNULL(vcData, '') AS [vcNodeName],
                                    1 tintType
                          FROM      listdetails Ld
                                    LEFT JOIN listorder LO ON Ld.numListItemID = LO.numListItemID
                                                              AND lo.numDomainId = @numDomainID
                                    INNER JOIN AuthoritativeBizDocs AB ON ( LD.numListItemID = AB.numAuthoritativeSales )
                          WHERE     LD.numListID = 27
                                    AND ( constFlag = 1
                                          OR LD.numDomainID = @numDomainID
                                        )
                                    AND AB.numDomainID = @numDomainID
                          UNION
                          SELECT    0 numPageAuthID,
                                    @numGroupID numGroupID,
                                    @numTabID numTabID,
                                    ld.numListItemID numPageNavID,
                                    1 bitVisible,
                                    @numDomainID numDomainID,
                                    ISNULL(vcData, '') AS [vcNodeName],
                                    1 tintType
                          FROM      listdetails Ld
                                    LEFT JOIN listorder LO ON Ld.numListItemID = LO.numListItemID
                                                              AND lo.numDomainId = @numDomainID
                                    INNER JOIN AuthoritativeBizDocs AB ON ( ld.numListItemID = AB.numAuthoritativePurchase )
                          WHERE     LD.numListID = 27
                                    AND ( constFlag = 1
                                          OR LD.numDomainID = @numDomainID
                                        )
                                    AND AB.numDomainID = @numDomainID
                          UNION
                          SELECT    0 numPageAuthID,
                                    @numGroupID numGroupID,
                                    @numTabID numTabID,
                                    0 numPageNavID,
                                    1 bitVisible,
                                    @numDomainID numDomainID,
                                    ISNULL(LT.vcData, '') AS [vcNodeName],
                                    1 tintType
                          FROM      ListDetails LT,
                                    listdetails ld
                          WHERE     LT.numListID = 11
                                    AND Lt.numDomainID = @numDomainID
                                    AND LD.numListID = 27
                                    AND ( ld.constFlag = 1
                                          OR LD.numDomainID = @numDomainID
                                        )
                                    AND ( LD.numListItemID IN (
                                          SELECT    AB.numAuthoritativeSales
                                          FROM      AuthoritativeBizDocs AB
                                          WHERE     AB.numDomainId = @numDomainID )
                                          OR ld.numListItemID IN (
                                          SELECT    AB.numAuthoritativePurchase
                                          FROM      AuthoritativeBizDocs AB
                                          WHERE     AB.numDomainId = @numDomainID )
                                        )
                          UNION
                          SELECT    0 numPageAuthID,
                                    @numGroupID numGroupID,
                                    @numTabID numTabID,
                                    2222 numPageNavID,
                                    1 bitVisible,
                                    @numDomainID numDomainID,
                                    'Other BizDocs' AS [vcNodeName],
                                    1 tintType
                          UNION
                          SELECT    0 numPageAuthID,
                                    @numGroupID numGroupID,
                                    @numTabID numTabID,
                                    ld.numListItemID numPageNavID,
                                    1 bitVisible,
                                    @numDomainID numDomainID,
                                    ISNULL(vcData, '') AS [vcNodeName],
                                    1 tintType
                          FROM      listdetails Ld
                          WHERE     ld.numListID = 27
									AND @numTabID <> 133
                                    AND ( constFlag = 1
                                          OR LD.numDomainID = @numDomainID
                                        )
                                    AND LD.numListItemID NOT IN (
                                    SELECT  AB.numAuthoritativeSales
                                    FROM    AuthoritativeBizDocs AB
                                    WHERE   AB.numDomainID = @numDomainID )
                                    AND LD.numListItemID NOT IN (
                                    SELECT  AB.numAuthoritativePurchase
                                    FROM    AuthoritativeBizDocs AB
                                    WHERE   AB.numDomainID = @numDomainID )
                          UNION
                          SELECT    0 numPageAuthID,
                                    @numGroupID numGroupID,
                                    @numTabID numTabID,
                                    ls.numListItemID numPageNavID,
                                    1 bitVisible,
                                    @numDomainID numDomainID,
                                    ISNULL(ls.vcData, '') AS [vcNodeName],
                                    1 tintType
                          FROM      listdetails Ld,
                                    ( SELECT    *
                                      FROM      ListDetails LS
                                      WHERE     LS.numDomainID = @numDomainID
                                                AND LS.numListID = 11
                                    ) LS
                          WHERE     ld.numListID = 27
									AND @numTabID <> 133
                                    AND ( ld.constFlag = 1
                                          OR LD.numDomainID = @numDomainID
                                        )
                                    AND LD.numListItemID NOT IN (
                                    SELECT  AB.numAuthoritativeSales
                                    FROM    AuthoritativeBizDocs AB
                                    WHERE   AB.numDomainID = @numDomainID
                                    UNION
                                    SELECT  AB.numAuthoritativePurchase
                                    FROM    AuthoritativeBizDocs AB
                                    WHERE   AB.numDomainID = @numDomainID )
                        ) PageNavTable
                        JOIN dbo.TreeNavigationAuthorization TreeNav ON PageNavTable.numPageNavID = TreeNav.numPageNavID
                                                                        AND PageNavTable.numGroupID = TreeNav.numGroupID
                                                                        AND PageNavTable.numTabID = TreeNav.numTabID
                ORDER BY TreeNav.tintType,
                        TreeNav.numPageNavID

            END

        SELECT  *
        FROM    ( SELECT    ISNULL(( SELECT TOP 1
                                            TNA.[numPageAuthID]
                                     FROM   dbo.TreeNavigationAuthorization TNA
                                     WHERE  TNA.numPageNavID = PND.[numPageNavID]
                                            AND TNA.numDomainID = @numDomainID
                                            AND TNA.numGroupID = @numGroupID
                                            AND TNA.numTabID = @numTabID
                                   ), 0) AS [numPageAuthID],
                            ISNULL(@numGroupID, 0) AS [numGroupID],
                            ISNULL(PND.[numTabID], 0) AS [numTabID],
                            ISNULL(PND.[numPageNavID], 0) AS [numPageNavID],
                            ISNULL(( SELECT TOP 1
                                            TNA.[bitVisible]
                                     FROM   dbo.TreeNavigationAuthorization TNA
                                     WHERE  TNA.numPageNavID = PND.[numPageNavID]
                                            AND TNA.numDomainID = @numDomainID
                                            AND TNA.numGroupID = @numGroupID
                                            AND TNA.numTabID = @numTabID
                                   ), 0) AS [bitVisible],
                            @numDomainID AS [numDomainID],
                            ( SELECT TOP 1
                                        vcPageNavName
                              FROM      dbo.PageNavigationDTL
                              WHERE     PageNavigationDTL.numPageNavID = PND.numPageNavID
                            ) AS [vcNodeName],
                            ISNULL(( SELECT TOP 1
                                            TNA.tintType
                                     FROM   dbo.TreeNavigationAuthorization TNA
                                     WHERE  TNA.numPageNavID = PND.[numPageNavID]
                                            AND TNA.numDomainID = @numDomainID
                                            AND TNA.numGroupID = @numGroupID
                                            AND TNA.numTabID = @numTabID
                                   ), 1) AS [tintType]
                  FROM      dbo.PageNavigationDTL PND
                  WHERE     ISNULL(PND.bitVisible, 0) = 1
                            AND PND.numTabID = @numTabID
                            AND numModuleID = @numModuleID
                  UNION ALL
                  SELECT    ISNULL([numPageAuthID], 0) AS [numPageAuthID],
                            @numGroupID AS [numGroupID],
                            @numTabID AS [numTabID],
                            ISNULL(LD.[numListItemID], 0) AS [numPageNavID],
                            ISNULL(TNA.[bitVisible], 0) AS [bitVisible],
                            ISNULL(LD.[numDomainID], 0) AS [numDomainID],
                            vcData AS [vcNodeName],
                            1 AS [tintType]
                  FROM      dbo.ListDetails LD
                            LEFT JOIN dbo.TreeNavigationAuthorization TNA ON LD.numListItemID = TNA.numPageNavID
                  WHERE     numListID = 27
                            AND LD.numDomainID = @numDomainID
                            AND ISNULL(constFlag, 0) = 0
                            AND numTabID = ( CASE WHEN @numModuleID = 10
                                                  THEN 1
                                                  ELSE 0
                                             END )
                  UNION ALL
                  SELECT    ISNULL([numPageAuthID], 0) AS [numPageAuthID],
                            @numGroupID AS [numGroupID],
                            @numTabID AS [numTabID],
                            ISNULL(LD.[numListItemID], 0) AS [numPageNavID],
                            ISNULL(TNA.[bitVisible], 0) AS [bitVisible],
                            ISNULL(TNA.[numDomainID], 0) AS [numDomainID],
                            vcData AS [vcNodeName],
                            1 AS [tintType]
                  FROM      dbo.ListDetails LD
                            LEFT JOIN dbo.TreeNavigationAuthorization TNA ON LD.numListItemID = TNA.numPageNavID
                                                                             AND ISNULL(TNA.[numDomainID], 0) = @numDomainID
                  WHERE     numListID = 27
                            AND ISNULL(constFlag, 0) = 1
                            AND numTabID = @numTabID
                ) TABLE2
        ORDER BY numPageNavID  
    END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetReportsForSRG')
DROP PROCEDURE dbo.USP_GetReportsForSRG
GO
CREATE PROCEDURE [dbo].[USP_GetReportsForSRG]
(
	@numDomainID NUMERIC(18,0)
	,@numUserCntID NUMERIC(18,0)
	,@tintMode TINYINT
	,@tintReportType TINYINT
	,@numDashboardTemplateID NUMERIC(18,0)
	,@vcSearchText VARCHAR(300)
	,@numCurrentPage INT
	,@numPageSize INT
	,@numSRGID NUMERIC(18,0)	
)
AS 
BEGIN
	IF @tintMode = 1
	BEGIN
		IF @tintReportType = 2 -- Pre defined reports
		BEGIN
			SELECT 
				COUNT(*) OVER() AS TotalRecords
				,ReportListMaster.numReportID
				,(CASE WHEN  ISNULL(bitDefault,0) = 1 THEN 2 ELSE 1 END) AS tintReportType
				,ISNULL(vcReportName,'') vcReportName
				,'' AS vcReportType
				,'' vcModuleName
				,'' AS vcPerspective
				,'' AS vcTimeline
			FROM 
				ReportListMaster 
			LEFT JOIN 
				ReportModuleMaster RMM 
			ON 
				ReportListMaster.numReportModuleID=RMM.numReportModuleID
			WHERE
				ISNULL(ReportListMaster.bitDefault,0) = 1
				AND (ISNULL(@vcSearchText,'')='' OR vcReportName LIKE CONCAT('%',@vcSearchText,'%'))
			ORDER BY
				vcReportName
			OFFSET 
				(@numCurrentPage - 1) * @numPageSize ROWS 
			FETCH NEXT @numPageSize ROWS ONLY
		END
		ELSE IF @tintReportType = 3 -- My Reports
		BEGIN
			SELECT 
				COUNT(*) OVER() AS TotalRecords
				,URL.numReportID
				,1 AS tintReportType
				,ISNULL(vcReportName,'') vcReportName
				,(CASE RLM.tintReportType WHEN 0 THEN 'Tabular' WHEN 1 THEN 'Summary' WHEN 2 THEN 'Matrix' WHEN 3 THEN 'KPI' WHEN 4 THEN 'ScoreCard' END) AS vcReportType
				,ISNULL(RMM.vcModuleName,'') vcModuleName
				,(CASE RLM.tintRecordFilter WHEN 5 THEN 'User logged in (Record Owner)' WHEN 4 THEN 'User logged in (Assigned-To)' WHEN 3 THEN 'User logged in (Territory Scope)' WHEN 2 THEN 'User logged in (Team Scope)' WHEN 1 THEN 'User logged in (Assigned-To/Record Owner)' ELSE 'All Records' END) AS vcPerspective
				,(CASE 
					WHEN ISNULL(RLM.numDateFieldID,0) > 0 AND LEN(ISNULL(RLM.vcDateFieldValue,'')) > 0
					THEN 
						CONCAT((CASE WHEN bitDateFieldColumnCustom = 1 THEN (SELECT Fld_label FROM CFW_Fld_Master WHERE Fld_Id = numDateFieldID) ELSE (SELECT vcFieldName FROM DycFieldMaster WHERE numFieldId=numDateFieldID) END),' (',
						(CASE RLM.vcDateFieldValue
							WHEN 'AllTime' THEN 'All Time'
							WHEN 'Custom' THEN 'Custom'
							WHEN 'CurYear' THEN 'Current CY'
							WHEN 'PreYear' THEN 'Previous CY'
							WHEN 'Pre2Year' THEN 'Previous 2 CY'
							WHEN 'Ago2Year' THEN '2 CY Ago'
							WHEN 'NextYear' THEN 'Next CY'
							WHEN 'CurPreYear' THEN 'Current and Previous CY'
							WHEN 'CurPre2Year' THEN 'Current and Previous 2 CY'
							WHEN 'CurNextYear' THEN 'Current and Next CY'
							WHEN 'CuQur' THEN 'Current CQ'
							WHEN 'CurNextQur' THEN 'Current and Next CQ'
							WHEN 'CurPreQur' THEN 'Current and Previous CQ'
							WHEN 'NextQur' THEN 'Next CQ'
							WHEN 'PreQur' THEN 'Previous CQ'
							WHEN 'LastMonth' THEN 'Last Month'
							WHEN 'ThisMonth' THEN 'This Month'
							WHEN 'NextMonth' THEN 'Next Month'
							WHEN 'CurPreMonth' THEN 'Current and Previous Month'
							WHEN 'CurNextMonth' THEN 'Current and Next Month'
							WHEN 'LastWeek' THEN 'Last Week'
							WHEN 'ThisWeek' THEN 'This Week'
							WHEN 'NextWeek' THEN 'Next Week'
							WHEN 'Yesterday' THEN 'Yesterday'
							WHEN 'Today' THEN 'Today'
							WHEN 'Tomorrow' THEN 'Tomorrow'
							WHEN 'Last7Day' THEN 'Last 7 Days'
							WHEN 'Last30Day' THEN 'Last 30 Days'
							WHEN 'Last60Day' THEN 'Last 60 Days'
							WHEN 'Last90Day' THEN 'Last 90 Days'
							WHEN 'Last120Day' THEN 'Last 120 Days'
							WHEN 'Next7Day' THEN 'Next 7 Days'
							WHEN 'Next30Day' THEN 'Next 30 Days'
							WHEN 'Next60Day' THEN 'Next 60 Days'
							WHEN 'Next90Day' THEN 'Next 90 Days'
							WHEN 'Next120Day' THEN 'Next 120 Days'
							ELSE '-'
						END),')')
					ELSE 
						'' 
				END) AS vcTimeline
			FROM 
				UserReportList URL
			INNER JOIN 
				ReportListMaster RLM
			ON 
				URL.numReportID=RLM.numReportID
			INNER JOIN 
				ReportModuleMaster RMM 
			ON 
				RLM.numReportModuleID=RMM.numReportModuleID
			WHERE 
				URL.numDomainID=@numDomainID 
				AND URL.numUserCntID=@numUserCntID 
				AND URL.tintReportType=1
				AND RLM.numDomainID=@numDomainID
				AND (ISNULL(@vcSearchText,'')='' OR vcReportName LIKE CONCAT('%',@vcSearchText,'%'))
			ORDER BY
				vcReportName
			OFFSET 
				(@numCurrentPage - 1) * @numPageSize ROWS 
			FETCH NEXT @numPageSize ROWS ONLY
		END
		ELSE IF @tintReportType = 4 -- Saved searches
		BEGIN
			SELECT  
				COUNT(*) OVER() AS TotalRecords,
				numSearchID AS numReportID,
				4 AS tintReportType,
				ISNULL(vcSearchName,'') AS vcReportName,
				'' AS vcReportType,
				(CASE DFM.numFormId WHEN 1 THEN 'Companies & Contacts Search' ELSE DFM.vcFormName END) AS vcModuleName,
				'' AS vcPerspective,
				'' AS vcTimeline
			FROM
				dbo.SavedSearch S
			INNER JOIN
				dbo.DynamicFormMaster DFM 
			ON 
				DFM.numFormId = S.numFormId
			WHERE 
				numDomainID = @numDomainID
				AND numUserCntID = @numUserCntID
				AND ISNULL(numSharedFromSearchID,0) = 0
				AND (ISNULL(@vcSearchText,'')='' OR vcSearchName LIKE CONCAT('%',@vcSearchText,'%'))
			ORDER BY
				vcSearchName
			OFFSET 
				(@numCurrentPage - 1) * @numPageSize ROWS 
			FETCH NEXT @numPageSize ROWS ONLY
		END
		ELSE IF @tintReportType = 5 -- Shared saved searched
		BEGIN
			SELECT  
				COUNT(*) OVER() AS TotalRecords,
				numSearchID AS numReportID,
				4 AS tintReportType,
				ISNULL(vcSearchName,'') AS vcReportName,
				'' AS vcReportType,
				(CASE DFM.numFormId WHEN 1 THEN 'Companies & Contacts Search' ELSE DFM.vcFormName END) AS vcModuleName,
				'' AS vcPerspective,
				'' AS vcTimeline
			FROM
				dbo.SavedSearch S
			INNER JOIN
				dbo.DynamicFormMaster DFM 
			ON 
				DFM.numFormId = S.numFormId
			WHERE 
				numDomainID = @numDomainID
				AND numUserCntID = @numUserCntID
				AND ISNULL(numSharedFromSearchID,0) > 0
				AND (ISNULL(@vcSearchText,'')='' OR vcSearchName LIKE CONCAT('%',@vcSearchText,'%'))
			ORDER BY
				vcSearchName
			OFFSET 
				(@numCurrentPage - 1) * @numPageSize ROWS 
			FETCH NEXT @numPageSize ROWS ONLY
		END
		ELSE IF @tintReportType = 6 -- Dashboar templates
		BEGIN
			SELECT 
				COUNT(*) OVER() AS TotalRecords
				,ReportDashboard.numDashBoardID
				,6 AS tintReportType
				,ISNULL(vcReportName,'') vcReportName
				,'' AS vcReportType
				,'' vcModuleName
				,'' AS vcPerspective
				,'' AS vcTimeline
			FROM 
				DashboardTemplate
			INNER JOIN
				ReportDashboard 
			ON
				DashboardTemplate.numTemplateID = ReportDashboard.numDashboardTemplateID
			INNER JOIN
				ReportListMaster 
			ON
				ReportDashboard.numReportID = ReportListMaster.numReportID
			LEFT JOIN 
				ReportModuleMaster RMM 
			ON 
				ReportListMaster.numReportModuleID=RMM.numReportModuleID
			WHERE
				DashboardTemplate.numDomainID = @numDomainID
				AND DashboardTemplate.numTemplateID = @numDashboardTemplateID
				AND ReportDashboard.numDomainID = @numDomainID
				AND ReportDashboard.numUserCntID = @numUserCntID
				AND (ReportListMaster.numDomainID = @numDomainID OR ReportListMaster.bitDefault = 1)
				AND (ISNULL(@vcSearchText,'')='' OR vcReportName LIKE CONCAT('%',@vcSearchText,'%'))
			ORDER BY
				vcReportName
			OFFSET 
				(@numCurrentPage - 1) * @numPageSize ROWS 
			FETCH NEXT @numPageSize ROWS ONLY
		END
		ELSE
		BEGIN
			SELECT 
				COUNT(*) OVER() AS TotalRecords
				,RLM.numReportID
				,1 AS tintReportType
				,ISNULL(vcReportName,'') vcReportName
				,(CASE RLM.tintReportType WHEN 0 THEN 'Tabular' WHEN 1 THEN 'Summary' WHEN 2 THEN 'Matrix' WHEN 3 THEN 'KPI' WHEN 4 THEN 'ScoreCard' END) AS vcReportType
				,ISNULL(RMM.vcModuleName,'') vcModuleName
				,(CASE RLM.tintRecordFilter WHEN 5 THEN 'User logged in (Record Owner)' WHEN 4 THEN 'User logged in (Assigned-To)' WHEN 3 THEN 'User logged in (Territory Scope)' WHEN 2 THEN 'User logged in (Team Scope)' WHEN 1 THEN 'User logged in (Assigned-To/Record Owner)' ELSE 'All Records' END) AS vcPerspective
				,(CASE 
					WHEN ISNULL(RLM.numDateFieldID,0) > 0 AND LEN(ISNULL(RLM.vcDateFieldValue,'')) > 0
					THEN 
						CONCAT((CASE WHEN bitDateFieldColumnCustom = 1 THEN (SELECT Fld_label FROM CFW_Fld_Master WHERE Fld_Id = numDateFieldID) ELSE (SELECT vcFieldName FROM DycFieldMaster WHERE numFieldId=numDateFieldID) END),' (',
						(CASE RLM.vcDateFieldValue
							WHEN 'AllTime' THEN 'All Time'
							WHEN 'Custom' THEN 'Custom'
							WHEN 'CurYear' THEN 'Current CY'
							WHEN 'PreYear' THEN 'Previous CY'
							WHEN 'Pre2Year' THEN 'Previous 2 CY'
							WHEN 'Ago2Year' THEN '2 CY Ago'
							WHEN 'NextYear' THEN 'Next CY'
							WHEN 'CurPreYear' THEN 'Current and Previous CY'
							WHEN 'CurPre2Year' THEN 'Current and Previous 2 CY'
							WHEN 'CurNextYear' THEN 'Current and Next CY'
							WHEN 'CuQur' THEN 'Current CQ'
							WHEN 'CurNextQur' THEN 'Current and Next CQ'
							WHEN 'CurPreQur' THEN 'Current and Previous CQ'
							WHEN 'NextQur' THEN 'Next CQ'
							WHEN 'PreQur' THEN 'Previous CQ'
							WHEN 'LastMonth' THEN 'Last Month'
							WHEN 'ThisMonth' THEN 'This Month'
							WHEN 'NextMonth' THEN 'Next Month'
							WHEN 'CurPreMonth' THEN 'Current and Previous Month'
							WHEN 'CurNextMonth' THEN 'Current and Next Month'
							WHEN 'LastWeek' THEN 'Last Week'
							WHEN 'ThisWeek' THEN 'This Week'
							WHEN 'NextWeek' THEN 'Next Week'
							WHEN 'Yesterday' THEN 'Yesterday'
							WHEN 'Today' THEN 'Today'
							WHEN 'Tomorrow' THEN 'Tomorrow'
							WHEN 'Last7Day' THEN 'Last 7 Days'
							WHEN 'Last30Day' THEN 'Last 30 Days'
							WHEN 'Last60Day' THEN 'Last 60 Days'
							WHEN 'Last90Day' THEN 'Last 90 Days'
							WHEN 'Last120Day' THEN 'Last 120 Days'
							WHEN 'Next7Day' THEN 'Next 7 Days'
							WHEN 'Next30Day' THEN 'Next 30 Days'
							WHEN 'Next60Day' THEN 'Next 60 Days'
							WHEN 'Next90Day' THEN 'Next 90 Days'
							WHEN 'Next120Day' THEN 'Next 120 Days'
							ELSE '-'
						END),')')
					ELSE 
						'' 
				END) AS vcTimeline
			FROM 
				ReportListMaster RLM
			INNER JOIN 
				ReportModuleMaster RMM 
			ON 
				RLM.numReportModuleID=RMM.numReportModuleID
			WHERE 
				RLM.numDomainID=@numDomainID 
				AND (ISNULL(@vcSearchText,'')='' OR vcReportName LIKE CONCAT('%',@vcSearchText,'%'))
			ORDER BY
				vcReportName
			OFFSET 
				(@numCurrentPage - 1) * @numPageSize ROWS 
			FETCH NEXT @numPageSize ROWS ONLY
		END
	END
	ELSE IF @tintMode = 2
	BEGIN
		DECLARE @TEMP TABLE
		(
			ID NUMERIC(18,0)
			,numReportID NUMERIC(18,0)
			,tintReportType TINYINT
			,vcReportName VARCHAR(MAX)
			,vcReportType VARCHAR(100)
			,vcModuleName VARCHAR(100)
			,vcPerspective VARCHAR(100)
			,vcTimeline VARCHAR(100)
			,intSortOrder INT
		)

		INSERT INTO @TEMP
		(
			ID
			,numReportID
			,tintReportType
			,vcReportName
			,vcReportType
			,vcModuleName
			,vcPerspective
			,vcTimeline
			,intSortOrder
		)
		SELECT 
			SRGR.ID
			,ReportListMaster.numReportID
			,(CASE WHEN ISNULL(bitDefault,0) = 1 THEN 2 ELSE 1 END) AS tintReportType
			,(CASE WHEN ISNULL(bitDefault,0) = 1 THEN ISNULL(vcReportName,'') ELSE CONCAT('<a target="_blank" href="../reports/frmCustomReportRun.aspx?ReptID=',ReportListMaster.numReportID,'">',ISNULL(vcReportName,''),'</a>') END) vcReportName
			,(CASE WHEN ISNULL(ReportListMaster.bitDefault,0) = 1 THEN '' ELSE (CASE ReportListMaster.tintReportType WHEN 0 THEN 'Tabular' WHEN 1 THEN 'Summary' WHEN 2 THEN 'Matrix' WHEN 3 THEN 'KPI' WHEN 4 THEN 'ScoreCard' END) END) AS vcReportType
			,(CASE WHEN ISNULL(ReportListMaster.bitDefault,0) = 1 THEN '' ELSE ISNULL(RMM.vcModuleName,'') END) vcModuleName
			,(CASE WHEN ISNULL(ReportListMaster.bitDefault,0) = 1 THEN '' ELSE (CASE ReportListMaster.tintRecordFilter WHEN 5 THEN 'User logged in (Record Owner)' WHEN 4 THEN 'User logged in (Assigned-To)' WHEN 3 THEN 'User logged in (Territory Scope)' WHEN 2 THEN 'User logged in (Team Scope)' WHEN 1 THEN 'User logged in (Assigned-To/Record Owner)' ELSE 'All Records' END) END) AS vcPerspective
			,(CASE 
				WHEN ISNULL(ReportListMaster.bitDefault,0) = 1 
				THEN '' 
				ELSE (CASE 
						WHEN ISNULL(ReportListMaster.numDateFieldID,0) > 0 AND LEN(ISNULL(ReportListMaster.vcDateFieldValue,'')) > 0
						THEN 
							CONCAT((CASE WHEN bitDateFieldColumnCustom = 1 THEN (SELECT Fld_label FROM CFW_Fld_Master WHERE Fld_Id = numDateFieldID) ELSE (SELECT vcFieldName FROM DycFieldMaster WHERE numFieldId=numDateFieldID) END),' (',
							(CASE ReportListMaster.vcDateFieldValue
								WHEN 'AllTime' THEN 'All Time'
								WHEN 'Custom' THEN 'Custom'
								WHEN 'CurYear' THEN 'Current CY'
								WHEN 'PreYear' THEN 'Previous CY'
								WHEN 'Pre2Year' THEN 'Previous 2 CY'
								WHEN 'Ago2Year' THEN '2 CY Ago'
								WHEN 'NextYear' THEN 'Next CY'
								WHEN 'CurPreYear' THEN 'Current and Previous CY'
								WHEN 'CurPre2Year' THEN 'Current and Previous 2 CY'
								WHEN 'CurNextYear' THEN 'Current and Next CY'
								WHEN 'CuQur' THEN 'Current CQ'
								WHEN 'CurNextQur' THEN 'Current and Next CQ'
								WHEN 'CurPreQur' THEN 'Current and Previous CQ'
								WHEN 'NextQur' THEN 'Next CQ'
								WHEN 'PreQur' THEN 'Previous CQ'
								WHEN 'LastMonth' THEN 'Last Month'
								WHEN 'ThisMonth' THEN 'This Month'
								WHEN 'NextMonth' THEN 'Next Month'
								WHEN 'CurPreMonth' THEN 'Current and Previous Month'
								WHEN 'CurNextMonth' THEN 'Current and Next Month'
								WHEN 'LastWeek' THEN 'Last Week'
								WHEN 'ThisWeek' THEN 'This Week'
								WHEN 'NextWeek' THEN 'Next Week'
								WHEN 'Yesterday' THEN 'Yesterday'
								WHEN 'Today' THEN 'Today'
								WHEN 'Tomorrow' THEN 'Tomorrow'
								WHEN 'Last7Day' THEN 'Last 7 Days'
								WHEN 'Last30Day' THEN 'Last 30 Days'
								WHEN 'Last60Day' THEN 'Last 60 Days'
								WHEN 'Last90Day' THEN 'Last 90 Days'
								WHEN 'Last120Day' THEN 'Last 120 Days'
								WHEN 'Next7Day' THEN 'Next 7 Days'
								WHEN 'Next30Day' THEN 'Next 30 Days'
								WHEN 'Next60Day' THEN 'Next 60 Days'
								WHEN 'Next90Day' THEN 'Next 90 Days'
								WHEN 'Next120Day' THEN 'Next 120 Days'
								ELSE '-'
							END),')')
						ELSE 
							'' 
					END)
			END) AS vcTimeline
			,SRGR.intSortOrder
		FROM
			ScheduledReportsGroupReports SRGR
		INNER JOIN
			ReportListMaster 
		ON
			SRGR.numReportID = ReportListMaster.numReportID
			AND SRGR.tintReportType IN (1,2)
		LEFT JOIN 
			ReportModuleMaster RMM 
		ON 
			ReportListMaster.numReportModuleID=RMM.numReportModuleID
		WHERE
			SRGR.numSRGID = @numSRGID
			AND (ReportListMaster.numDomainID = @numDomainID OR ISNULL(ReportListMaster.bitDefault,0) = 1)
			AND SRGR.tintReportType IN (1,2)

		INSERT INTO @TEMP
		(
			ID
			,numReportID
			,tintReportType
			,vcReportName
			,vcReportType
			,vcModuleName
			,vcPerspective
			,vcTimeline
			,intSortOrder
		)
		SELECT 
			SRGR.ID
			,ReportListMaster.numReportID
			,(CASE WHEN ISNULL(bitDefault,0) = 1 THEN 2 ELSE 1 END) AS tintReportType
			,(CASE WHEN ISNULL(bitDefault,0) = 1 THEN ISNULL(vcReportName,'') ELSE CONCAT('<a target="_blank" href="../reports/frmCustomReportRun.aspx?ReptID=',ReportListMaster.numReportID,'">',ISNULL(vcReportName,''),'</a>') END) vcReportName
			,(CASE WHEN ISNULL(ReportListMaster.bitDefault,0) = 1 THEN '' ELSE (CASE ReportListMaster.tintReportType WHEN 0 THEN 'Tabular' WHEN 1 THEN 'Summary' WHEN 2 THEN 'Matrix' WHEN 3 THEN 'KPI' WHEN 4 THEN 'ScoreCard' END) END) AS vcReportType
			,(CASE WHEN ISNULL(ReportListMaster.bitDefault,0) = 1 THEN '' ELSE ISNULL(RMM.vcModuleName,'') END) vcModuleName
			,(CASE WHEN ISNULL(ReportListMaster.bitDefault,0) = 1 THEN '' ELSE (CASE ReportListMaster.tintRecordFilter WHEN 5 THEN 'User logged in (Record Owner)' WHEN 4 THEN 'User logged in (Assigned-To)' WHEN 3 THEN 'User logged in (Territory Scope)' WHEN 2 THEN 'User logged in (Team Scope)' WHEN 1 THEN 'User logged in (Assigned-To/Record Owner)' ELSE 'All Records' END) END) AS vcPerspective
			,(CASE 
				WHEN ISNULL(ReportListMaster.bitDefault,0) = 1 
				THEN '' 
				ELSE (CASE 
						WHEN ISNULL(ReportListMaster.numDateFieldID,0) > 0 AND LEN(ISNULL(ReportListMaster.vcDateFieldValue,'')) > 0
						THEN 
							CONCAT((CASE WHEN bitDateFieldColumnCustom = 1 THEN (SELECT Fld_label FROM CFW_Fld_Master WHERE Fld_Id = numDateFieldID) ELSE (SELECT vcFieldName FROM DycFieldMaster WHERE numFieldId=numDateFieldID) END),' (',
							(CASE ReportListMaster.vcDateFieldValue
								WHEN 'AllTime' THEN 'All Time'
								WHEN 'Custom' THEN 'Custom'
								WHEN 'CurYear' THEN 'Current CY'
								WHEN 'PreYear' THEN 'Previous CY'
								WHEN 'Pre2Year' THEN 'Previous 2 CY'
								WHEN 'Ago2Year' THEN '2 CY Ago'
								WHEN 'NextYear' THEN 'Next CY'
								WHEN 'CurPreYear' THEN 'Current and Previous CY'
								WHEN 'CurPre2Year' THEN 'Current and Previous 2 CY'
								WHEN 'CurNextYear' THEN 'Current and Next CY'
								WHEN 'CuQur' THEN 'Current CQ'
								WHEN 'CurNextQur' THEN 'Current and Next CQ'
								WHEN 'CurPreQur' THEN 'Current and Previous CQ'
								WHEN 'NextQur' THEN 'Next CQ'
								WHEN 'PreQur' THEN 'Previous CQ'
								WHEN 'LastMonth' THEN 'Last Month'
								WHEN 'ThisMonth' THEN 'This Month'
								WHEN 'NextMonth' THEN 'Next Month'
								WHEN 'CurPreMonth' THEN 'Current and Previous Month'
								WHEN 'CurNextMonth' THEN 'Current and Next Month'
								WHEN 'LastWeek' THEN 'Last Week'
								WHEN 'ThisWeek' THEN 'This Week'
								WHEN 'NextWeek' THEN 'Next Week'
								WHEN 'Yesterday' THEN 'Yesterday'
								WHEN 'Today' THEN 'Today'
								WHEN 'Tomorrow' THEN 'Tomorrow'
								WHEN 'Last7Day' THEN 'Last 7 Days'
								WHEN 'Last30Day' THEN 'Last 30 Days'
								WHEN 'Last60Day' THEN 'Last 60 Days'
								WHEN 'Last90Day' THEN 'Last 90 Days'
								WHEN 'Last120Day' THEN 'Last 120 Days'
								WHEN 'Next7Day' THEN 'Next 7 Days'
								WHEN 'Next30Day' THEN 'Next 30 Days'
								WHEN 'Next60Day' THEN 'Next 60 Days'
								WHEN 'Next90Day' THEN 'Next 90 Days'
								WHEN 'Next120Day' THEN 'Next 120 Days'
								ELSE '-'
							END),')')
						ELSE 
							'' 
					END)
			END) AS vcTimeline
			,SRGR.intSortOrder
		FROM
			ScheduledReportsGroupReports SRGR
		INNER JOIN
			DashboardTemplateReports DTR
		ON
			SRGR.numReportID = DTR.numDTRID
		INNER JOIN
			ReportListMaster 
		ON
			DTR.numReportID = ReportListMaster.numReportID
		LEFT JOIN 
			ReportModuleMaster RMM 
		ON 
			ReportListMaster.numReportModuleID=RMM.numReportModuleID
		WHERE
			SRGR.numSRGID = @numSRGID
			AND (ReportListMaster.numDomainID = @numDomainID OR ISNULL(ReportListMaster.bitDefault,0) = 1)
			AND SRGR.tintReportType = 6

		INSERT INTO @TEMP
		(
			ID
			,numReportID
			,tintReportType
			,vcReportName
			,vcReportType
			,vcModuleName
			,vcPerspective
			,vcTimeline
			,intSortOrder
		)
		SELECT
			SRGR.ID
			,numSearchID AS numReportID,
			(CASE WHEN ISNULL(numSharedFromSearchID,0) > 0 THEN 4 ELSE 3 END) AS tintReportType,
			(CASE WHEN ISNULL(S.vcSearchConditionJson,'') = '' THEN CONCAT('<a href="javascript:OpenSearchResult(',S.numSearchID,',',ISNULL(S.numFormID,0),')">',ISNULL(vcSearchName,''),'</a>') ELSE ISNULL(vcSearchName,'') END) AS vcReportName,
			'' AS vcReportType,
			(CASE DFM.numFormId WHEN 1 THEN 'Companies & Contacts Search' ELSE DFM.vcFormName END) AS vcModuleName,
			'' AS vcPerspective,
			'' AS vcTimeline,
			SRGR.intSortOrder
		FROM
			ScheduledReportsGroupReports SRGR
		INNER JOIN
			dbo.SavedSearch S
		ON
			SRGR.numReportID = S.numSearchID
		LEFT JOIN
			dbo.DynamicFormMaster DFM 
		ON 
			DFM.numFormId = S.numFormId
		WHERE 
			numDomainID = @numDomainID
			AND SRGR.numSRGID = @numSRGID
			AND SRGR.tintReportType IN (4,5)

		SELECT 0 AS TotalRecords,* FROM @TEMP ORDER BY intSortOrder
	END
	ELSE IF @tintMode = 3
	BEGIN
		UPDATE ScheduledReportsGroup SET tintDataCacheStatus = 1 WHERE numDomainID=@numDomainID AND ID=@numSRGID

		SELECT 
			SRGR.ID
			,SRGR.numSRGID
			,(CASE 
				WHEN SRGR.tintReportType = 6 THEN RLMDashboard.numReportID
				ELSE SRGR.numReportID
			END) numReportID
			,(CASE 
				WHEN SRGR.tintReportType = 6 THEN (CASE WHEN ISNULL(RLMDashboard.bitDefault,0) = 1 THEN 2 ELSE 1 END)
				ELSE SRGR.tintReportType
			END) tintReportType
			,SRGR.intSortOrder
			,(CASE 
				WHEN SRGR.tintReportType IN (1,2) THEN ISNULL(RLM.vcReportName,'') 
				WHEN SRGR.tintReportType IN (4,5) THEN ISNULL(SS.vcSearchName,'') 
				WHEN SRGR.tintReportType = 6 THEN (CASE WHEN ISNULL(RD.vcHeaderText,'') <> '' THEN ISNULL(RD.vcHeaderText,'') ELSE ISNULL(RLMDashboard.vcReportName,'') END)
				ELSE ''
			END) vcReportName
			,(CASE 
				WHEN SRGR.tintReportType = 6 THEN (CASE WHEN ISNULL(RLMDashboard.bitDefault,0) = 1 THEN ISNULL(RLMDashboard.intDefaultReportID,0) ELSE 0 END)
				ELSE ISNULL(RLM.intDefaultReportID,0)
			END) intDefaultReportID
			,ISNULL(SS.numFormID,0) numFormID
			,ISNULL(SS.numUserCntID,0) numSearchUserCntID
			,ISNULL(SS.vcSearchConditionJson,'') vcSearchConditionJson
			,ISNULL(SS.vcDisplayColumns,'') vcDisplayColumns
			,ISNULL(RD.numDashBoardID,0) numDashBoardID
		FROM 
			ScheduledReportsGroup SRG 
		INNER JOIN 
			ScheduledReportsGroupReports SRGR 
		ON 
			SRG.ID=SRGR.numSRGID 
		LEFT JOIN
			ReportListMaster RLM
		ON
			SRGR.numReportID = RLM.numReportID
			AND SRGR.tintReportType IN (1,2)
		LEFT JOIN
			ReportDashboard RD
		ON
			SRGR.numReportID = RD.numDashBoardID
			AND SRGR.tintReportType = 6
		LEFT JOIN
			ReportListMaster RLMDashboard
		ON
			RD.numReportID = RLMDashboard.numReportID
		LEFT JOIN
			SavedSearch SS
		ON
			SRGR.numReportID = SS.numSearchID
			AND SRGR.tintReportType IN (4,5)
		WHERE 
			SRG.numDomainID = @numDomainID 
			AND SRG.ID=@numSRGID 
		ORDER BY 
			intSortOrder
	END
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER  ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetWFRecordData')
DROP PROCEDURE USP_GetWFRecordData
GO
Create PROCEDURE [dbo].[USP_GetWFRecordData]     
    @numDomainID numeric(18, 0),
    @numRecordID numeric(18, 0),
    @numFormID numeric(18, 0)
AS                 
BEGIN
	DECLARE @numContactId AS NUMERIC(18),
			@numRecOwner AS NUMERIC(18),
			@numAssignedTo AS NUMERIC(18),
			@numDivisionId AS NUMERIC(18),
			@txtSignature VARCHAR(8000),
			@vcDateFormat VARCHAR(30),
			@numInternalPM numeric(18,0),
			@numExternalPM numeric(18,0),
			@numNextAssignedTo numeric(18,0),
			@numPhone varchar(15),
			@numOppIDPC numeric(18,0),
			@numStageIDPC numeric(18,0),
			@numOppBizDOCIDPC numeric(18,0),
			@numProjectID NUMERIC(18,0),
			@numCaseId NUMERIC(18,0),
			@numCommId NUMERIC(18,0)

	IF @numFormID=70 --Opportunities & Orders
	BEGIN
		SELECT 
			@numContactId=ISNULL(numContactId,0)
			,@numRecOwner=ISNULL(numRecOwner,0)
			,@numAssignedTo=ISNULL(numAssignedTo,0)
			,@numDivisionId=ISNULL(numDivisionId,0)
		FROM 
			dbo.OpportunityMaster 
		WHERE 
			numDomainId=@numDomainID AND numOppId=@numRecordID

		SELECT 
			@numStageIDPC=ISNULL(s.numStageDetailsId,0)
		FROM 
			OpportunityMaster o 
		LEFT JOIN 
			StagePercentageDetails s 
		on 
			s.numOppID=o.numOppId
		WHERE 
			o.numDomainId=@numDomainID 
			AND o.numOppId=@numRecordID

		SELECT 
			@numOppBizDOCIDPC=ISNULL(s.numOppBizDocsId,0)
			,@numOppIDPC=ISNULL(o.numOppId,0)
		FROM 
			OpportunityMaster o 
		LEFT JOIN 
			OpportunityBizDocs s 
		ON 
			s.numOppID=o.numOppId
		WHERE 
			o.numDomainId=@numDomainID 
			AND o.numOppId=@numRecordID
	END

	IF @numFormID=49 --BizDocs
	BEGIN
		SELECT 
			@numOppBizDOCIDPC=ISNULL(obd.numOppBizDocsId,0)
			,@numContactId=ISNULL(om.numContactId,0)
			,@numRecOwner=ISNULL(om.numRecOwner,0)
			,@numOppIDPC=ISNULL(om.numOppId,0)
			,@numAssignedTo=ISNULL(om.numAssignedTo,0)
			,@numDivisionId=ISNULL(om.numDivisionId,0)
			,@vcDateFormat=D.vcDateFormat
		FROM 
			dbo.OpportunityBizDocs AS obd 
		INNER JOIN 
			dbo.OpportunityMaster AS OM 
		ON 
			obd.numOppId=om.numOppId 
		INNER JOIN 
			dbo.Domain AS d 
		ON 
			d.numDomainId=om.numDomainId
		WHERE 
			om.numDomainId=@numDomainID 
			AND obd.numOppBizDocsId=@numRecordID
	END

	IF @numFormID=94 --Sales/Purchase/Project
	BEGIN
		DECLARE @numOppId NUMERIC(18,0)
		SELECT 
			@numProjectID=ISNULL(numProjectID,0)
			,@numOppId=Isnull(numOppId,0) 
		FROM 
			StagePercentageDetails 
		WHERE 
			numStageDetailsId=@numRecordID

		--@numInternalPM numeric(18,0),@numExternalPM numeric(18,0),@numNextAssignedTo numeric(18,0)
		IF @numProjectID=0--Order
		BEGIN
			SELECT 
				@numContactId=ISNULL(om.numContactId,0)
				,@numRecOwner=ISNULL(om.numRecOwner,0)
				,@numAssignedTo=ISNULL(obd.numAssignTo,0)
				,@numDivisionId=ISNULL(om.numDivisionId,0)
				,@vcDateFormat=D.vcDateFormat
			FROM 
				dbo.StagePercentageDetails AS obd 
			INNER JOIN 
				dbo.OpportunityMaster AS OM 
			on 
				obd.numOppId=om.numOppId 
			INNER JOIN 
				dbo.Domain AS d 
			ON 
				d.numDomainId=om.numDomainId
			WHERE 
				om.numDomainId=@numDomainID 
				AND obd.numStageDetailsId=@numRecordID
		END
		ELSE
		BEGIN
			SELECT 
				@numNextAssignedTo=numAssignTo 
			FROM 
				StagePercentageDetails 
			WHERE 
				numStageDetailsId=@numRecordID+1

			SELECT 
				@numContactId=ISNULL(om.numIntPrjMgr,0)
				,@numRecOwner=ISNULL(om.numRecOwner,0)
				,@numAssignedTo=ISNULL(obd.numAssignTo,0)
				,@numDivisionId=ISNULL(om.numDivisionId,0)
				,@numInternalPM=ISNULL(om.numIntPrjMgr,0)
				,@numExternalPM=ISNULL(om.numCustPrjMgr,0)
			FROM 
				dbo.StagePercentageDetails AS  obd 
			INNER JOIN 
				dbo.ProjectsMaster AS OM 
			on 
				obd.numProjectID=om.numProId 	
			WHERE 
				om.numDomainId=@numDomainID 
				AND obd.numStageDetailsId=@numRecordID
		END
	END
	
	IF (@numFormID=68 OR @numFormID=138) --Organization , AR Aging
	BEGIN
		SELECT 
			@numContactId=ISNULL(adc.numContactId,0)
			,@numRecOwner=ISNULL(obd.numRecOwner,0)
			,@numAssignedTo=ISNULL(obd.numAssignedTo,0)
			,@numDivisionId=ISNULL(obd.numDivisionId,0)
			,@vcDateFormat=d.vcDateFormat
		FROM 
			dbo.DivisionMaster AS  obd 	
		INNER join 
			dbo.AdditionalContactsInformation as adc 
		ON 
			adc.numDivisionId=obd.numDivisionID
		INNER JOIN 
			dbo.Domain AS d 
		ON 
			d.numDomainId=obd.numDomainId
		WHERE 
			obd.numDomainId=@numDomainID 
			AND obd.numDivisionID=@numRecordID
	END

	IF @numFormID=69 --Contacts
	BEGIN
		SELECT 
			@numContactId=ISNULL(adc.numContactId,0)
			,@numRecOwner=ISNULL(obd.numRecOwner,0)
			,@numAssignedTo=ISNULL(obd.numAssignedTo,0)
			,@numDivisionId=ISNULL(obd.numDivisionId,0)
			,@vcDateFormat=d.vcDateFormat
		FROM 
			dbo.DivisionMaster AS  obd 	
		INNER JOIN 
			dbo.AdditionalContactsInformation as adc 
		ON 
			adc.numDivisionId=obd.numDivisionID
		INNER JOIN 
			dbo.Domain AS d 
		ON 
			d.numDomainId=obd.numDomainId
		WHERE 
			obd.numDomainId=@numDomainID 
			AND adc.numContactId=@numRecordID
	END

	IF @numFormID=73 --Projects
	BEGIN
		SELECT 
			@numProjectID=ISNULL(OM.numProId,0)
			,@numContactId=ISNULL(om.numIntPrjMgr,0)
			,@numRecOwner=ISNULL(om.numRecOwner,0)
			,@numAssignedTo=ISNULL(om.numAssignedTo,0)
			,@numDivisionId=ISNULL(om.numDivisionId,0)
			,@numInternalPM=ISNULL(om.numIntPrjMgr,0)
			,@numExternalPM=ISNULL(om.numCustPrjMgr,0)
		FROM 
			dbo.ProjectsMaster AS OM	
		WHERE 
			om.numDomainId=@numDomainID 
			AND om.numProId=@numRecordID

		SELECT 
			@numStageIDPC=ISNULL(s.numStageDetailsId,0)
		FROM 
			ProjectsMaster o 
		left join 
			StagePercentageDetails s 
		on 
			s.numProjectID=o.numProId
		WHERE 
			o.numDomainId=@numDomainID 
			AND o.numProId=@numRecordID
	END

	IF @numFormID=72 --Cases
	BEGIN
		SELECT 
			@numCaseId=ISNULL(OM.numCaseId,0)
			,@numContactId=ISNULL(om.numContactId,0)
			,@numRecOwner=ISNULL(om.numRecOwner,0)
			,@numAssignedTo=ISNULL(om.numAssignedTo,0)
			,@numDivisionId=ISNULL(om.numDivisionId,0)
			,@vcDateFormat=d.vcDateFormat
		FROM 
			dbo.Cases AS OM	
		INNER JOIN 
			dbo.Domain AS d 
		ON
			d.numDomainId=om.numDomainId
		WHERE 
			om.numDomainId=@numDomainID 
			AND om.numCaseId=@numRecordID
	
		SELECT 
			@numOppIDPC=ISNULL(o.numOppId,0)
		FROM 
			Cases om 
		left JOIN 
			CaseOpportunities co 
		on 
			om.numCaseId=co.numCaseId  
		left join 
			OpportunityMaster o 
		on
			o.numOppId=co.numOppId
		WHERE 
			om.numDomainId=@numDomainID 
			AND om.numCaseId=@numRecordID
	END

	IF @numFormID=124 --Action items(Ticklers)
	BEGIN
		SELECT 
			@numCommId=ISNULL(OM.numCommId,0)
			,@numContactId=ISNULL(om.numContactId,0)
			,@numRecOwner=ISNULL(om.numAssignedBy,0)
			,@numAssignedTo=ISNULL(om.numAssign,0)
			,@numDivisionId=ISNULL(om.numDivisionId,0)
			,@vcDateFormat=d.vcDateFormat
		FROM 
			dbo.Communication AS OM	
		INNER JOIN 
			dbo.Domain AS d 
		ON 
			d.numDomainId=om.numDomainId
		WHERE 
			om.numDomainId=@numDomainID 
			AND om.numCommId=@numRecordID
	END

	DECLARE @vcEmailID AS VARCHAR(100),
			@ContactName AS VARCHAR(150),
			@bitSMTPServer AS bit,
			@vcSMTPServer AS VARCHAR(100),
			@numSMTPPort AS NUMERIC,
			@bitSMTPAuth AS BIT,
			@vcSmtpPassword AS VARCHAR(100),
			@bitSMTPSSL BIT 

	IF @numRecOwner>0
	BEGIN
		SELECT 
			@vcEmailID=vcEmailID
			,@ContactName=isnull(vcFirstname,'')+' '+isnull(vcLastName,'')
			,@bitSMTPServer=isnull(bitSMTPServer,0)
			,@vcSMTPServer=case when bitSMTPServer= 1 then vcSMTPServer else '' end
			,@numSMTPPort=case when bitSMTPServer= 1 then  isnull(numSMTPPort,0) else 0 end
			,@bitSMTPAuth=isnull(bitSMTPAuth,0)
			,@vcSmtpPassword=isnull([vcSmtpPassword],'')
			,@bitSMTPSSL=isnull(bitSMTPSSL,0)
			,@txtSignature=u.txtSignature
			,@numPhone=numPhone
		FROM 
			UserMaster U 
		JOIN 
			AdditionalContactsInformation A 
		ON 
			A.numContactID=U.numUserDetailId 
		WHERE 
			U.numDomainID=@numDomainID 
			AND numUserDetailId=@numRecOwner
	END

	IF @numAssignedTo=0 --As per Auto Routing Rule
	BEGIN
		DECLARE @numRuleID NUMERIC	 
				
		SELECT @numRuleID=numRoutID FROM RoutingLeads  WHERE numDomainID=@numDomainID and bitDefault=1  

		SELECT 
			@numAssignedTo=rd.numEmpId           
		FROM 
			RoutingLeads r 
		INNER JOIN 
			Routingleaddetails rd 
		ON 
			r.numRoutID=rd.numRoutID
		WHERE 
			r.numRoutID=@numRuleID
	END

	SELECT 
		@vcEmailID = ISNULL(vcPSMTPUserName,@vcEmailID)
	FROM
		Domain
	WHERE
		numDomainId=@numDomainID

	SELECT 
		@numContactId AS numContactId
		,@numRecOwner AS numRecOwner
		,@numAssignedTo AS numAssignedTo
		,@numDivisionId AS numDivisionId
		,@vcEmailID AS vcEmailID
		,ISNULL((SELECT vcPSMTPDisplayName FROM Domain WHERE numDomainId=@numDomainID),@ContactName) AS ContactName
		,@bitSMTPServer AS bitSMTPServer
		,@vcSMTPServer AS vcSMTPServer
		,@numSMTPPort AS numSMTPPort
		,@bitSMTPAuth AS bitSMTPAuth
		,@vcSmtpPassword AS vcSmtpPassword
		,@bitSMTPSSL AS bitSMTPSSL
		,@txtSignature AS Signature
		,@numInternalPM as numInternalPM
		,@numExternalPM as numExternalPM
		,@numNextAssignedTo as numNextAssignedTo
		,@numPhone as numPhone
		,@numOppIDPC as numOppIDPC
		,@numStageIDPC as numStageIDPC
		,@numOppBizDOCIDPC as numOppBizDOCIDPC
		,@numProjectID AS numProjectID
		,@numCaseId AS numCaseId
		,@numCommId AS numCommId
END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_MassPurchaseFulfillment_GetRecords')
DROP PROCEDURE dbo.USP_MassPurchaseFulfillment_GetRecords
GO

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_MassPurchaseFulfillment_GetRecords]
(
	@numDomainID NUMERIC(18,0)
	,@numUserCntID NUMERIC(18,0)
	,@ClientTimeZoneOffset INT
	,@numViewID NUMERIC(18,0)
	,@numWarehouseID NUMERIC(18,0)
	,@bitGroupByOrder BIT
	,@tintFlag tinyint -- if 1 then ShowBilled = true, 2 then received but not billed
	,@bitIncludeSearch BIT
	,@vcOrderStauts VARCHAR(MAX)
	,@vcCustomSearchValue VARCHAR(MAX)
	,@numPageIndex INT
	,@vcSortColumn VARCHAR(100) OUTPUT
	,@vcSortOrder VARCHAR(4) OUTPUT
	,@numTotalRecords NUMERIC(18,0) OUTPUT
)
AS 
BEGIN
	DECLARE @numDivisionID NUMERIC(18,0)
	DECLARE @bitShowOnlyFullyReceived BIT = 0
	DECLARE @tintBillType TINYINT = 1
	
	SELECT @numDivisionID=ISNULL(numDivisionID,0) FROM Domain WHERE numDomainId=@numDomainID
	
	IF EXISTS (SELECT ID FROM MassPurchaseFulfillmentConfiguration WHERE numDomainID=@numDomainID AND numUserCntID=@numUserCntID)
	BEGIN
		SELECT 
			@bitShowOnlyFullyReceived=ISNULL(bitShowOnlyFullyReceived,0)
			,@tintBillType = ISNULL(tintBillType,1)
		FROM 
			MassPurchaseFulfillmentConfiguration 
		WHERE 
			numDomainID=@numDomainID 
			AND numUserCntID=@numUserCntID
	END

	IF @numViewID = 4
	BEGIN
		SET @bitGroupByOrder = 1
	END

	DECLARE @TempFieldsLeft TABLE
	(
		numPKID INT IDENTITY(1,1)
		,ID VARCHAR(50)
		,numFieldID NUMERIC(18,0)
		,vcFieldName VARCHAR(100)
		,vcOrigDbColumnName VARCHAR(100)
		,vcListItemType VARCHAR(10)
		,numListID NUMERIC(18,0)
		,bitAllowSorting BIT
		,bitAllowFiltering BIT
		,vcAssociatedControlType VARCHAR(20)
		,vcLookBackTableName VARCHAR(100)
		,bitCustomField BIT
		,intRowNum INT
		,intColumnWidth FLOAT
		,bitIsRequired BIT
		,bitIsEmail BIT
		,bitIsAlphaNumeric BIT
		,bitIsNumeric BIT
		,bitIsLengthValidation BIT
		,intMaxLength INT
		,intMinLength INT
		,bitFieldMessage BIT
		,vcFieldMessage VARCHAR(MAX)
	)

	IF @bitGroupByOrder = 1
	BEGIN
		IF @vcSortColumn NOT IN ('CompanyInfo.vcCompanyName','OpportunityMaster.bintCreatedDate','OpportunityMaster.vcPoppName','OpportunityMaster.numStatus','OpportunityMaster.numAssignedTo','OpportunityMaster.numRecOwner')
		BEGIN
			SET @vcSortColumn = 'OpportunityMaster.bintCreatedDate'
			SET @vcSortOrder = 'DESC'
		END
	END
	ELSE 
	BEGIN
		IF @vcSortColumn NOT IN ('CompanyInfo.vcCompanyName'
								,'OpportunityMaster.bintCreatedDate'
								,'OpportunityMaster.vcPoppName'
								,'OpportunityMaster.numStatus'
								,'OpportunityMaster.numAssignedTo'
								,'OpportunityMaster.numRecOwner'
								,'WarehouseItems.numOnHand'
								,'WarehouseItems.numOnOrder'
								,'WarehouseItems.numAllocation'
								,'WarehouseItems.numBackOrder'
								,'Item.numItemCode'
								,'Item.vcItemName'
								,'OpportunityMaster.numUnitHour'
								,'OpportunityMaster.numUnitHourReceived')
		BEGIN
			SET @vcSortColumn = 'OpportunityMaster.bintCreatedDate'
			SET @vcSortOrder = 'DESC'
		END
	END

	INSERT INTO @TempFieldsLeft
	(
		ID
		,numFieldID
		,vcFieldName
		,vcOrigDbColumnName
		,vcListItemType
		,numListID
		,bitAllowSorting
		,bitAllowFiltering
		,vcAssociatedControlType
		,vcLookBackTableName
		,bitCustomField
		,intRowNum
		,intColumnWidth
		,bitIsRequired
		,bitIsEmail
		,bitIsAlphaNumeric
		,bitIsNumeric
		,bitIsLengthValidation
		,intMaxLength
		,intMinLength
		,bitFieldMessage
		,vcFieldMessage
	)
	SELECT
		CONCAT(135,'~',DFM.numFieldID,'~',0)
		,DFM.numFieldID
		,(CASE WHEN @numViewID=6 AND DFM.numFieldID=248 THEN 'BizDoc' ELSE DFFM.vcFieldName END)
		,DFM.vcOrigDbColumnName
		,ISNULL(DFM.vcListItemType,'')
		,ISNULL(DFM.numListID,0)
		,DFFM.bitAllowSorting
		,DFFM.bitAllowFiltering
		,DFM.vcAssociatedControlType
		,DFM.vcLookBackTableName
		,0
		,DFCD.intRowNum
		,ISNULL(DFCD.intColumnWidth,0)
		,0
		,0
		,0
		,0
		,0
		,0
		,0
		,0
		,''
	FROM
		DycFormConfigurationDetails DFCD
	INNER JOIN
		DycFormField_Mapping DFFM
	ON
		DFCD.numFormId = DFFM.numFormID
		AND DFCD.numFieldId = DFFM.numFieldID
	INNER JOIN
		DycFieldMaster DFM
	ON
		DFFM.numFieldID = DFM.numFieldId
	WHERE 
		DFCD.numDomainId=@numDomainID 
		AND DFCD.numUserCntID=@numUserCntID 
		AND DFCD.numFormId=135 
		AND DFCD.numViewId=@numViewID
		AND ISNULL(DFFM.bitDeleted,0) = 0
		AND ISNULL(DFFM.bitSettingField,0) = 1
		AND ISNULL(DFCD.bitCustom,0) = 0
	UNION
	SELECT
		CONCAT(135,'~',CFM.Fld_id,'~',0)
		,CFM.Fld_id
		,CFM.Fld_label
		,CONCAT('CUST',CFM.Fld_id)
		,''
		,ISNULL(CFM.numListID,0)
		,0
		,0
		,CFM.Fld_type
		,''
		,1
		,DFCD.intRowNum
		,ISNULL(DFCD.intColumnWidth,0)
		,ISNULL(bitIsRequired,0)
		,ISNULL(bitIsEmail,0)
		,ISNULL(bitIsAlphaNumeric,0)
		,ISNULL(bitIsNumeric,0)
		,ISNULL(bitIsLengthValidation,0)
		,ISNULL(intMaxLength,0)
		,ISNULL(intMinLength,0)
		,ISNULL(bitFieldMessage,0)
		,ISNULL(vcFieldMessage,'')
	FROM
		DycFormConfigurationDetails DFCD
	INNER JOIN
		CFW_Fld_Master CFM
	ON
		DFCD.numFieldId = CFM.Fld_id
	LEFT JOIN
		CFW_Validation CV
	ON
		CFM.Fld_id = CV.numFieldID
	WHERE
		DFCD.numDomainId=@numDomainID 
		AND DFCD.numUserCntID=@numUserCntID 
		AND DFCD.numFormId=135 
		AND DFCD.numViewId=@numViewID
		AND ISNULL(DFCD.bitCustom,0) = 1

	IF (SELECT COUNT(*) FROM @TempFieldsLeft) = 0
	BEGIN
		INSERT INTO @TempFieldsLeft
		(
			ID
			,numFieldID
			,vcFieldName
			,vcOrigDbColumnName
			,vcListItemType
			,numListID
			,bitAllowSorting
			,bitAllowFiltering
			,vcAssociatedControlType
			,vcLookBackTableName
			,bitCustomField
			,intRowNum
			,intColumnWidth
		)
		SELECT
			CONCAT(135,'~',DFM.numFieldID,'~',0)
			,DFM.numFieldID
			,DFFM.vcFieldName
			,DFM.vcOrigDbColumnName
			,ISNULL(DFM.vcListItemType,'')
			,ISNULL(DFM.numListID,0)
			,DFFM.bitAllowSorting
			,DFFM.bitAllowFiltering
			,DFM.vcAssociatedControlType
			,DFM.vcLookBackTableName
			,0
			,DFFM.[order]
			,100
		FROM
			DycFormField_Mapping DFFM
		INNER JOIN
			DycFieldMaster DFM
		ON
			DFFM.numFieldID = DFM.numFieldId
		WHERE 
			DFFM.numFormId=135 
			AND ISNULL(DFFM.bitDeleted,0) = 0
			AND ISNULL(DFFM.bitSettingField,0) = 1
			AND ISNULL(DFFM.bitDefault,0) = 1
	END

	DECLARE @j INT = 0
	DECLARE @jCount INT
	DECLARE @numFieldId NUMERIC(18,0)
	DECLARE @vcFieldName AS VARCHAR(50)                                                                                                
	DECLARE @vcAssociatedControlType VARCHAR(10)  
	DECLARE @vcDbColumnName VARCHAR(100)                                                
	--DECLARE @numListID AS NUMERIC(9)       
	DECLARE @bitCustomField BIT       
	SET @jCount = (SELECT COUNT(*) FROM @TempFieldsLeft)
	DECLARE @vcCustomFields VARCHAR(MAX) = ''
	DECLARE @vcCustomWhere VARCHAR(MAX) = ''

	WHILE @j <= @jCount
	BEGIN
		SELECT
			@numFieldId=numFieldID,
			@vcFieldName = vcFieldName,
			@vcAssociatedControlType = vcAssociatedControlType,
			@vcDbColumnName = CONCAT('Cust',numFieldID),
			@bitCustomField=ISNULL(bitCustomField,0)
		FROM 
			@TempFieldsLeft
		WHERE 
			numPKID = @j     
		
		
			
		IF @bitCustomField = 1
		BEGIN         
			IF @vcAssociatedControlType = 'TextBox' OR @vcAssociatedControlType = 'TextArea' 
			BEGIN
				SET @vcCustomFields = CONCAT(@vcCustomFields,',CFW',@numFieldId,'.Fld_Value  [',@vcDbColumnName,']')
             
				SET @vcCustomWhere = CONCAT(@vcCustomWhere,' left Join CFW_FLD_Values_Opp CFW',@numFieldId
								,' ON CFW' , @numFieldId , '.Fld_Id='
								,@numFieldId
								, ' and CFW' , @numFieldId
								, '.RecId=T1.numOppID')                                                         
			END   
			ELSE IF @vcAssociatedControlType = 'CheckBox' 
			BEGIN
				SET @vcCustomFields =CONCAT( @vcCustomFields
					, ',case when isnull(CFW'
					, @numFieldId
					, '.Fld_Value,0)=0 then 0 when isnull(CFW'
					, @numFieldId
					, '.Fld_Value,0)=1 then 1 end   ['
					,  @vcDbColumnName
					, ']')               
 
				SET @vcCustomWhere = CONCAT(@vcCustomWhere
					, ' left Join CFW_FLD_Values_Opp CFW'
					, @numFieldId
					, ' ON CFW',@numFieldId,'.Fld_Id='
					, @numFieldId
					, ' and CFW'
					, @numFieldId
					, '.RecId=T1.numOppID   ')                                                    
			END                
			ELSE IF @vcAssociatedControlType = 'DateField' 
			BEGIN
				SET @vcCustomFields = CONCAT(@vcCustomFields
					, ',dbo.FormatedDateFromDate(CFW'
					, @numFieldId
					, '.Fld_Value,'
					, @numDomainId
					, ')  [',@vcDbColumnName ,']' )  
					                  
				SET @vcCustomWhere = CONCAT(@vcCustomWhere
					, ' left Join CFW_FLD_Values_Opp CFW'
					, @numFieldId
					, ' on CFW', @numFieldId, '.Fld_Id='
					, @numFieldId
					, ' and CFW'
					, @numFieldId
					, '.RecId=T1.numOppID   ' )                                                        
			END                
			ELSE IF @vcAssociatedControlType = 'SelectBox' 
			BEGIN
				SET @vcCustomFields = CONCAT(@vcCustomFields,',L',@numFieldId,'.vcData',' [',@vcDbColumnName,']')
					                                                    
				SET @vcCustomWhere = CONCAT(@vcCustomWhere
					, ' left Join CFW_FLD_Values_Opp CFW'
					, @numFieldId
					, ' on CFW',@numFieldId ,'.Fld_Id='
					, @numFieldId
					, ' and CFW'
					, @numFieldId
					, '.RecId=T1.numOppID    ')     
					                                                    
				SET @vcCustomWhere = CONCAT(@vcCustomWhere
					, ' left Join ListDetails L'
					, @numFieldId
					, ' on L'
					, @numFieldId
					, '.numListItemID=CFW'
					, @numFieldId
					, '.Fld_Value' )              
			END
			ELSE
			BEGIN
				SET @vcCustomFields = CONCAT(@vcCustomFields,CONCAT(',dbo.GetCustFldValueOpp(',@numFieldId,',T1.numOppID)'),' [',@vcDbColumnName,']')
			END 
		END

		SET @j = @j + 1
	END
	
	CREATE TABLE #TEMPMSRecords
	(
		numOppID NUMERIC(18,0)
		,numOppItemID NUMERIC(18,0)
		,numWOID NUMERIC(18,0)
	)

	DECLARE @vcSQL NVARCHAR(MAX)
	DECLARE @vcSQLFinal NVARCHAR(MAX)
	
	SET @vcSQL = CONCAT('INSERT INTO #TEMPMSRecords
						(
							numOppID
							,numOppItemID
							,numWOID
						)
						SELECT
							OpportunityMaster.numOppId
							,',(CASE WHEN @bitGroupByOrder = 1 THEN '0' ELSE 'OpportunityItems.numoppitemtCode' END),'
							,0
						FROM
							OpportunityMaster
						INNER JOIN
							DivisionMaster
						ON
							OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
						INNER JOIN
							CompanyInfo
						ON
							DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
						INNER JOIN 
							OpportunityItems
						ON
							OpportunityMaster.numOppId = OpportunityItems.numOppId
						INNER JOIN
							Item
						ON
							OpportunityItems.numItemCode = Item.numItemCode
						LEFT JOIN
							WareHouseItems
						ON
							OpportunityItems.numWarehouseItmsID = WareHouseItems.numWareHouseItemID
						WHERE
							OpportunityMaster.numDomainId=@numDomainID
							AND OpportunityMaster.tintOppType = 2
							AND OpportunityMaster.tintOppStatus = 1
							AND ISNULL(OpportunityMaster.tintshipped,0) = 0
							AND 1 = (CASE 
										WHEN @numViewID=1 
										THEN (CASE WHEN ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyReceived,0) > 0 THEN 1 ELSE 0 END)
										WHEN @numViewID=2
										THEN (CASE WHEN ISNULL(OpportunityItems.numQtyReceived,0) > 0 AND ISNULL(OpportunityItems.numQtyReceived,0) - ISNULL(OpportunityItems.numUnitHourReceived,0) > 0 THEN 1 ELSE 0 END)
										WHEN @numViewID=3
										THEN (CASE 
												WHEN (CASE 
														WHEN @tintBillType=2 
														THEN ISNULL(OpportunityItems.numQtyReceived,0) 
														WHEN @tintBillType=3 THEN ISNULL(OpportunityItems.numUnitHourReceived,0) 
														ELSE ISNULL(OpportunityItems.numUnitHour,0) 
														END)  - ISNULL((SELECT 
																			SUM(OpportunityBizDocItems.numUnitHour)
																		FROM
																			OpportunityBizDocs
																		INNER JOIN
																			OpportunityBizDocItems
																		ON
																			OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																		WHERE
																			OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
																			AND OpportunityBizDocs.numBizDocId=644
																			AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode),0) > 0 
												THEN 1
												ELSE 0 
											END)
										WHEN @numViewID=4 AND ISNULL(@bitShowOnlyFullyReceived,0) = 1
										THEN (CASE 
												WHEN (SELECT COUNT(*) FROM OpportunityItems OIInner WHERE OIInner.numOppID=OpportunityMaster.numOppID AND ISNULL(OIInner.numUnitHour,0) <> ISNULL(OIInner.numUnitHourReceived,0) ) > 0 
												THEN 0 
												ELSE 1 
											END)
										ELSE 1
									END)
							AND 1 = (CASE WHEN @numViewID IN (1,2) AND ISNULL(@numWarehouseID,0) > 0 AND ISNULL(OpportunityItems.numWarehouseItmsID,0) > 0 THEN (CASE WHEN @numWarehouseID=WareHouseItems.numWarehouseID THEN 1 ELSE 0 END) ELSE 1 END)
							AND 1 = (CASE WHEN LEN(ISNULL(@vcOrderStauts,'''')) > 0 THEN (CASE WHEN OpportunityMaster.numStatus ',(CASE WHEN ISNULL(@bitIncludeSearch,0) = 0 THEN 'NOT IN' ELSE 'IN' END),' (SELECT Id FROM dbo.SplitIDs(@vcOrderStauts,'','')) THEN 1 ELSE 0 END) ELSE 1 END)'
							,@vcCustomSearchValue
							,(CASE WHEN @bitGroupByOrder = 1 THEN ' GROUP BY OpportunityMaster.numOppId' ELSE '' END));

	EXEC sp_executesql @vcSQL, N'@numDomainID NUMERIC(18,0), @vcOrderStauts VARCHAR(MAX), @numViewID NUMERIC(18,0), @numWarehouseID NUMERIC(18,0), @bitShowOnlyFullyReceived BIT, @tintBillType TINYINT', @numDomainID, @vcOrderStauts, @numViewID, @numWarehouseID, @bitShowOnlyFullyReceived, @tintBillType;


	SET @vcSQL = CONCAT('INSERT INTO #TEMPMSRecords
						(
							numOppID
							,numOppItemID
							,numWOID
						)
						SELECT
							0
							,0
							,WorkOrder.numWOID
						FROM
							WorkOrder
						LEFT JOIN
							OpportunityMaster
						ON
							WorkOrder.numOppID = OpportunityMaster.numOppID
						LEFT JOIN
							DivisionMaster
						ON
							OpportunityMaster.numDivisionId = @numDivisionID
						LEFT JOIN
							CompanyInfo
						ON
							DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
						INNER JOIN
							Item
						ON
							WorkOrder.numItemCode = Item.numItemCode
						LEFT JOIN
							WareHouseItems
						ON
							WorkOrder.numWarehouseItemID = WareHouseItems.numWareHouseItemID
						WHERE
							WorkOrder.numDomainId=@numDomainID
							AND 1 = (CASE 
										WHEN @numViewID=1 
										THEN (CASE WHEN ISNULL(WorkOrder.numQtyBuilt,0) - ISNULL(WorkOrder.numQtyReceived,0) > 0 THEN 1 ELSE 0 END)
										WHEN @numViewID=2
										THEN (CASE WHEN ISNULL(WorkOrder.numQtyReceived,0) > 0 AND ISNULL(WorkOrder.numQtyReceived,0) - ISNULL(WorkOrder.numUnitHourReceived,0) > 0 THEN 1 ELSE 0 END)
										ELSE 0
									END)
							AND 1 = (CASE WHEN @numViewID IN (1,2) AND ISNULL(@numWarehouseID,0) > 0 AND ISNULL(WorkOrder.numWareHouseItemId,0) > 0 THEN (CASE WHEN @numWarehouseID=WareHouseItems.numWarehouseID THEN 1 ELSE 0 END) ELSE 1 END)'
							,@vcCustomSearchValue);

	EXEC sp_executesql @vcSQL, N'@numDomainID NUMERIC(18,0), @vcOrderStauts VARCHAR(MAX), @numViewID NUMERIC(18,0), @numWarehouseID NUMERIC(18,0), @bitShowOnlyFullyReceived BIT, @tintBillType TINYINT, @numDivisionID NUMERIC(18,0)', @numDomainID, @vcOrderStauts, @numViewID, @numWarehouseID, @bitShowOnlyFullyReceived, @tintBillType, @numDivisionID;



	SET @numTotalRecords = (SELECT COUNT(*) FROM #TEMPMSRecords)
	
	CREATE TABLE #TEMPResult
	(
		numOppID NUMERIC(18,0)
		,numWOID NUMERIC(18,0)
		,numDivisionID NUMERIC(18,0)
		,numTerID NUMERIC(18,0)
		,numItemCode NUMERIC(18,0)
		,numOppItemID NUMERIC(18,0)
		,numWarehouseItemID NUMERIC(18,0)
		,vcCompanyName VARCHAR(300)
		,vcPoppName VARCHAR(300)
		,numStatus VARCHAR(300)
		,bintCreatedDate VARCHAR(50)
		,numAssignedTo VARCHAR(100)
		,numRecOwner VARCHAR(100)
		,vcItemName VARCHAR(300)
		,numOnHand FLOAT
		,numOnOrder FLOAT
		,numAllocation FLOAT
		,numBackOrder FLOAT
		,numBarCodeId VARCHAR(50)
		,vcLocation VARCHAR(100)
		,vcModelID VARCHAR(200)
		,vcItemDesc VARCHAR(2000)
		,numUnitHour FLOAT
		,numUOMId VARCHAR(50)
		,vcAttributes VARCHAR(300)
		,vcPathForTImage VARCHAR(300)
		,numUnitHourReceived FLOAT
		,numQtyReceived FLOAT
		,vcNotes VARCHAR(MAX)
		,SerialLotNo VARCHAR(MAX)
		,vcSKU VARCHAR(50)
		,numRemainingQty FLOAT
		,numQtyToShipReceive FLOAT
		,monPrice DECIMAL(20,5)
		,monTotAmount DECIMAL(20,5)
		,vcBilled FLOAT
	)
	
	IF @bitGroupByOrder = 1
	BEGIN
		SET @vcSQLFinal = CONCAT('INSERT INTO #TEMPResult
								(
									numOppID
									,numWOID
									,numDivisionID
									,numTerID
									,vcCompanyName
									,vcPoppName
									,bintCreatedDate
									,numStatus
									,numAssignedTo
									,numRecOwner
								)
								SELECT
									OpportunityMaster.numOppID
									,WorkOrder.numWOID
									,DivisionMaster.numDivisionID
									,ISNULL(DivisionMaster.numTerID,0) numTerID
									,ISNULL(CompanyInfo.vcCompanyName,'''')
									,(CASE WHEN WorkOrder.numWOID IS NOT NULL THEN CONCAT(ISNULL(WorkOrder.vcWorkOrderName,''-''),(CASE WHEN (ISNULL(WorkOrder.numQtyItemsReq,0) - ISNULL(WorkOrder.numQtyBuilt,0)) > 0 THEN CONCAT('' Remaining build qty:'',ISNULL(WorkOrder.numQtyItemsReq,0) - ISNULL(WorkOrder.numQtyBuilt,0)) ELSE '''' END)) ELSE ISNULL(OpportunityMaster.vcPoppName,'''') END)
									,dbo.FormatedDateFromDate(DATEADD(MINUTE,@ClientTimeZoneOffset * -1,(CASE WHEN WorkOrder.numWOID IS NOT NULL THEN WorkOrder.bintCreatedDate  ELSE OpportunityMaster.bintCreatedDate END)),@numDomainID)
									,dbo.GetListIemName(OpportunityMaster.numStatus)
									,(CASE WHEN WorkOrder.numWOID IS NOT NULL THEN dbo.fn_GetContactName(WorkOrder.numAssignedTo) ELSE dbo.fn_GetContactName(OpportunityMaster.numAssignedTo) END)
									,(CASE WHEN WorkOrder.numWOID IS NOT NULL THEN dbo.fn_GetContactName(WorkOrder.numCreatedBy) ELSE dbo.fn_GetContactName(OpportunityMaster.numRecOwner) END)
								FROM
									#TEMPMSRecords TEMPOrder
								LEFT JOIN
									OpportunityMaster
								ON
									TEMPOrder.numOppID = OpportunityMaster.numOppId
								LEFT JOIN
									WorkOrder
								ON
									TEMPOrder.numWOID = WorkOrder.numWOID
								INNER JOIN
									DivisionMaster
								ON
									DivisionMaster.numDivisionID = (CASE WHEN WorkOrder.numWOID IS NOT NULL THEN @numDivisionID ELSE OpportunityMaster.numDivisionId END)
								INNER JOIN
									CompanyInfo
								ON
									DivisionMaster.numCompanyID = CompanyInfo.numCompanyId ORDER BY ',
								(CASE @vcSortOrder
									WHEN 'CompanyInfo.vcCompanyName' THEN 'CompanyInfo.vcCompanyName '
									WHEN 'OpportunityMaster.vcPoppName' THEN 'OpportunityMaster.vcPoppName'
									WHEN 'OpportunityMaster.bintCreatedDate' THEN 'OpportunityMaster.bintCreatedDate'
									WHEN 'OpportunityMaster.numStatus' THEN 'dbo.GetListIemName(OpportunityMaster.numStatus)'
									WHEN 'OpportunityMaster.numAssignedTo' THEN 'dbo.fn_GetContactName(OpportunityMaster.numAssignedTo)'
									WHEN 'OpportunityMaster.numRecOwner' THEN 'dbo.fn_GetContactName(OpportunityMaster.numRecOwner)'
									ELSE 'OpportunityMaster.bintCreatedDate'
								END),' ',(CASE WHEN ISNULL(@vcSortOrder,'') <> '' THEN @vcSortOrder ELSE 'DESC' END),' OFFSET (@numPageIndex-1) * 200 ROWS FETCH NEXT 200 ROWS ONLY')


		EXEC sp_executesql @vcSQLFinal, N'@numDomainID NUMERIC(18,0), @ClientTimeZoneOffset INT, @numPageIndex INT, @numDivisionID NUMERIC(18,0)', @numDomainID, @ClientTimeZoneOffset, @numPageIndex, @numDivisionID;
	END
	ELSE
	BEGIN
		SET @vcSQLFinal = CONCAT('INSERT INTO #TEMPResult
								(
									numOppID
									,numWOID
									,numDivisionID
									,numTerID
									,vcCompanyName
									,vcPoppName
									,bintCreatedDate
									,numStatus
									,numAssignedTo
									,numRecOwner
									,numOppItemID
									,numWarehouseItemID
									,numItemCode
									,vcItemName
									,numOnHand
									,numOnOrder
									,numAllocation
									,numBackOrder
									,numBarCodeId
									,vcLocation
									,vcModelID
									,vcItemDesc
									,numUnitHour
									,numUOMId
									,vcAttributes
									,vcPathForTImage
									,numUnitHourReceived
									,numQtyReceived
									,vcNotes
									,SerialLotNo
									,vcSKU
									,numRemainingQty
									,numQtyToShipReceive
									,monPrice
									,monTotAmount
									,vcBilled
								)
								SELECT
									OpportunityMaster.numOppID
									,WorkOrder.numWOID
									,DivisionMaster.numDivisionID
									,ISNULL(DivisionMaster.numTerID,0) numTerID
									,ISNULL(CompanyInfo.vcCompanyName,'''')
									,(CASE 
										WHEN WorkOrder.numWOID IS NOT NULL THEN ISNULL(WorkOrder.vcWorkOrderName,'''')
										ELSE ISNULL(OpportunityMaster.vcPoppName,'''')
									END)
									,(CASE 
										WHEN WorkOrder.numWOID IS NOT NULL THEN dbo.FormatedDateFromDate(DATEADD(MINUTE,@ClientTimeZoneOffset * -1,WorkOrder.bintCreatedDate),@numDomainID)
										ELSE dbo.FormatedDateFromDate(DATEADD(MINUTE,@ClientTimeZoneOffset * -1,OpportunityMaster.bintCreatedDate),@numDomainID)
									END)
									,dbo.GetListIemName(OpportunityMaster.numStatus)
									,(CASE 
										WHEN WorkOrder.numWOID IS NOT NULL THEN dbo.fn_GetContactName(WorkOrder.numAssignedTo)
										ELSE dbo.fn_GetContactName(OpportunityMaster.numAssignedTo)
									END)
									,(CASE 
										WHEN WorkOrder.numWOID IS NOT NULL THEN dbo.fn_GetContactName(WorkOrder.numCreatedBy)
										ELSE dbo.fn_GetContactName(OpportunityMaster.numRecOwner)
									END) 
									,(CASE 
										WHEN WorkOrder.numWOID IS NOT NULL THEN 0
										ELSE OpportunityItems.numoppitemtCode
									END)
									,(CASE 
										WHEN WorkOrder.numWOID IS NOT NULL THEN WorkOrder.numWareHouseItemId
										ELSE ISNULL(OpportunityItems.numWarehouseItmsID,0)
									END)
									,Item.numItemCode
									,ISNULL(Item.vcItemName,'''')
									,ISNULL(numOnHand,0) + ISNULL(numAllocation,0)
									,ISNULL(numOnOrder,0)
									,ISNULL(numAllocation,0)
									,ISNULL(numBackOrder,0)
									,ISNULL(Item.numBarCodeId,'''')
									,ISNULL(WarehouseLocation.vcLocation,'''')
									,ISNULL(Item.vcModelID,'''')
									,(CASE 
										WHEN WorkOrder.numWOID IS NOT NULL THEN ISNULL(WorkOrder.vcInstruction,'''')
										ELSE ISNULL(OpportunityItems.vcItemDesc,'''')
									END)
									,(CASE 
										WHEN WorkOrder.numWOID IS NOT NULL THEN ISNULL(WorkOrder.numQtyItemsReq,0)
										ELSE ISNULL(OpportunityItems.numUnitHour,0)
									END)
									,(CASE 
										WHEN WorkOrder.numWOID IS NOT NULL THEN ''Units''
										ELSE dbo.fn_GetUOMName(OpportunityItems.numUOMId)
									END)
									,(CASE 
										WHEN WorkOrder.numWOID IS NOT NULL THEN ''''
										ELSE ISNULL(OpportunityItems.vcAttributes,'''')
									END)
									,ISNULL(Item.vcPathForTImage,'''')
									,(CASE 
										WHEN WorkOrder.numWOID IS NOT NULL THEN ISNULL(WorkOrder.numUnitHourReceived,0)
										ELSE ISNULL(OpportunityItems.numUnitHourReceived,0)
									END)
									,(CASE 
										WHEN WorkOrder.numWOID IS NOT NULL THEN ISNULL(WorkOrder.numQtyReceived,0)
										ELSE ISNULL(OpportunityItems.numQtyReceived,0)
									END)
									,(CASE 
										WHEN WorkOrder.numWOID IS NOT NULL THEN ''''
										ELSE ISNULL(OpportunityItems.vcNotes,'''')
									END)
									,SUBSTRING((SELECT '','' + vcSerialNo + CASE WHEN isnull(Item.bitLotNo,0)=1 THEN '' ('' + CONVERT(VARCHAR(15),whi.numQty) + '')'' ELSE '''' end 
									FROM OppWarehouseSerializedItem oppI JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID=whi.numWareHouseItmsDTLID where oppI.numOppID=OpportunityItems.numOppId and oppI.numOppItemID=OpportunityItems.numoppitemtCode 
									ORDER BY vcSerialNo FOR XML PATH('''')),2,200000)
									,ISNULL(Item.vcSKU,'''')
									,(CASE 
										WHEN WorkOrder.numWOID IS NOT NULL THEN (CASE 
																					WHEN @numViewID=1 THEN ISNULL(WorkOrder.numQtyBuilt,0) - ISNULL(WorkOrder.numQtyReceived,0) 
																					WHEN @numViewID=2 THEN ISNULL(WorkOrder.numQtyReceived,0) - ISNULL(WorkOrder.numUnitHourReceived,0) 
																					ELSE 0 
																				END)
										ELSE (CASE 
										WHEN @numViewID=1 THEN ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyReceived,0) 
										WHEN @numViewID=2 THEN ISNULL(OpportunityItems.numQtyReceived,0) - ISNULL(OpportunityItems.numUnitHourReceived,0) 
										WHEN @numViewID=3 THEN (CASE 
																	WHEN @tintBillType=2 
																	THEN ISNULL(OpportunityItems.numQtyReceived,0) 
																	WHEN @tintBillType=3 THEN ISNULL(OpportunityItems.numUnitHourReceived,0) 
																	ELSE ISNULL(OpportunityItems.numUnitHour,0) 
																END)  - ISNULL((SELECT 
																					SUM(OpportunityBizDocItems.numUnitHour)
																				FROM
																					OpportunityBizDocs
																				INNER JOIN
																					OpportunityBizDocItems
																				ON
																					OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																				WHERE
																					OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
																					AND OpportunityBizDocs.numBizDocId=644
																					AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode),0)
										ELSE ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numUnitHourReceived,0) 
									END)
									END)
									,(CASE 
										WHEN WorkOrder.numWOID IS NOT NULL THEN (CASE 
																					WHEN @numViewID=1 THEN ISNULL(WorkOrder.numQtyBuilt,0) - ISNULL(WorkOrder.numQtyReceived,0) 
																					WHEN @numViewID=2 THEN ISNULL(WorkOrder.numQtyReceived,0) - ISNULL(WorkOrder.numUnitHourReceived,0) 
																					ELSE 0 
																				END)
										ELSE (CASE 
										WHEN @numViewID=1 THEN ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyReceived,0) 
										WHEN @numViewID=2 THEN ISNULL(OpportunityItems.numQtyReceived,0) - ISNULL(OpportunityItems.numUnitHourReceived,0) 
										ELSE ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numUnitHourReceived,0) 
									END)
									END)
									,ISNULL(OpportunityItems.monPrice,0)
									,ISNULL(OpportunityItems.monTotAmount,0)
									,ISNULL((SELECT 
												SUM(OpportunityBizDocItems.numUnitHour)
											FROM
												OpportunityBizDocs
											INNER JOIN
												OpportunityBizDocItems
											ON
												OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
											WHERE
												OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
												AND OpportunityBizDocs.numBizDocId=644
												AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode),0)
												
								FROM
									#TEMPMSRecords TEMPOrder
								LEFT JOIN
									OpportunityMaster
								ON
									TEMPOrder.numOppID = OpportunityMaster.numOppId
								LEFT JOIN
									WorkOrder
								ON
									TEMPOrder.numWOID = WorkOrder.numWOID
								INNER JOIN
									DivisionMaster
								ON
									DivisionMaster.numDivisionID = (CASE WHEN WorkOrder.numWOID IS NOT NULL THEN @numDivisionID ELSE OpportunityMaster.numDivisionId END)
								INNER JOIN
									CompanyInfo
								ON
									DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
								LEFT JOIN 
									OpportunityItems
								ON
									OpportunityMaster.numOppId = OpportunityItems.numOppId
									AND TEMPOrder.numOppItemID = OpportunityItems.numoppitemtCode
								INNER JOIN
									Item
								ON
									(CASE WHEN WorkOrder.numWOID IS NOT NULL THEN WorkOrder.numItemCode ELSE OpportunityItems.numItemCode END) = Item.numItemCode
								INNER JOIN
									WareHouseItems
								ON
									(CASE WHEN WorkOrder.numWOID IS NOT NULL THEN WorkOrder.numWareHouseItemId ELSE OpportunityItems.numWarehouseItmsID END) = WareHouseItems.numWareHouseItemID
								LEFT JOIN
									WarehouseLocation
								ON
									WareHouseItems.numWLocationID = WarehouseLocation.numWLocationID ORDER BY ',
								(CASE @vcSortOrder
									WHEN 'CompanyInfo.vcCompanyName' THEN 'CompanyInfo.vcCompanyName '
									WHEN 'OpportunityMaster.vcPoppName' THEN '(CASE 
																					WHEN WorkOrder.numWOID IS NOT NULL THEN WorkOrder.vcWorkOrderName 
																					ELSE OpportunityMaster.vcPoppName
																				END)'
									WHEN 'OpportunityMaster.bintCreatedDate' THEN '(CASE 
																						WHEN WorkOrder.numWOID IS NOT NULL THEN WorkOrder.bintCreatedDate 
																						ELSE OpportunityMaster.bintCreatedDate
																					END)'
									WHEN 'OpportunityMaster.numStatus' THEN 'dbo.GetListIemName(OpportunityMaster.numStatus)'
									WHEN 'OpportunityMaster.numAssignedTo' THEN '(CASE 
																						WHEN WorkOrder.numWOID IS NOT NULL THEN dbo.fn_GetContactName(WorkOrder.numAssignedTo) 
																						ELSE dbo.fn_GetContactName(OpportunityMaster.numAssignedTo)
																					END)'
									WHEN 'OpportunityMaster.numRecOwner' THEN '(CASE 
																					WHEN WorkOrder.numWOID IS NOT NULL THEN dbo.fn_GetContactName(WorkOrder.numCreatedBy) 
																					ELSE dbo.fn_GetContactName(OpportunityMaster.numRecOwner)
																				END)'
									WHEN 'numOnHand' THEN 'ISNULL(WareHouseItems.numOnHand,0) + ISNULL(WareHouseItems.numAllocation,0)'
									WHEN 'numOnOrder' THEN 'ISNULL(WareHouseItems.numOnOrder,0)'
									WHEN 'numAllocation' THEN 'ISNULL(WareHouseItems.numAllocation,0)'
									WHEN 'numBackOrder' THEN 'ISNULL(WareHouseItems.numBackOrder,0)'
									WHEN 'numItemCode' THEN 'Item.numItemCode'
									WHEN 'vcItemName' THEN 'Item.vcItemName'
									WHEN 'numUnitHour' THEN '(CASE 
																WHEN WorkOrder.numWOID IS NOT NULL THEN WorkOrder.numQtyItemsReq
																ELSE OpportunityItems.numUnitHour
															END)'
									WHEN 'numUnitHourReceived' THEN '(CASE 
																		WHEN WorkOrder.numWOID IS NOT NULL THEN WorkOrder.numUnitHourReceived
																		ELSE OpportunityItems.numUnitHourReceived
																	END)'
									ELSE '(CASE 
												WHEN WorkOrder.numWOID IS NOT NULL THEN WorkOrder.bintCreatedDate 
												ELSE OpportunityMaster.bintCreatedDate
											END)'
								END),' ',(CASE WHEN ISNULL(@vcSortOrder,'') <> '' THEN @vcSortOrder ELSE 'DESC' END),' OFFSET (@numPageIndex-1) * 200 ROWS FETCH NEXT 200 ROWS ONLY')

		EXEC sp_executesql @vcSQLFinal, N'@numDomainID NUMERIC(18,0), @numViewID NUMERIC(18,0), @ClientTimeZoneOffset INT, @numPageIndex INT, @tintBillType TINYINT, @numDivisionID NUMERIC(18,0)', @numDomainID, @numViewID, @ClientTimeZoneOffset, @numPageIndex, @tintBillType, @numDivisionID;
	END
	
	/*Show records which have been billed*/
	IF @numViewID=1 AND  @tintFlag=1 
	BEGIN
	
		SET @vcSQLFinal = CONCAT('SELECT T1.*',@vcCustomFields,' FROM #TEMPResult T1 where vcBilled>0',@vcCustomWhere)
		SET @numTotalRecords = (SELECT COUNT(*) FROM #TEMPResult where vcBilled>0 )
	END
	ELSE IF @numViewID= 3 AND  @tintFlag=2 /* Received but not billed*/
	BEGIN
	SET @vcSQLFinal = CONCAT('SELECT T1.*',@vcCustomFields,' FROM #TEMPResult T1 where numQtyReceived>0',@vcCustomWhere)
	SET @numTotalRecords = (SELECT COUNT(*) FROM #TEMPResult where numQtyReceived>0 )
	END
	else
	BEGIN 
		SET @vcSQLFinal = CONCAT('SELECT T1.*',@vcCustomFields,' FROM #TEMPResult T1 ',@vcCustomWhere)
		END
	


	EXEC sp_executesql @vcSQLFinal

	SELECT * FROM @TempFieldsLeft ORDER BY intRowNum

	IF OBJECT_ID('tempdb..#TEMPMSRecords') IS NOT NULL DROP TABLE #TEMPMSRecords
	IF OBJECT_ID('tempdb..#TEMPResult') IS NOT NULL DROP TABLE #TEMPResult
END
GO
/****** Object:  StoredProcedure [dbo].[USP_OppBizDocs]    Script Date: 03/25/2009 15:23:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_oppbizdocs')
DROP PROCEDURE usp_oppbizdocs
GO
CREATE PROCEDURE [dbo].[USP_OppBizDocs]
(                        
	@byteMode as tinyint=null,                        
	@numOppId as numeric(9)=null,                        
	@numOppBizDocsId as numeric(9)=null,
	@ClientTimeZoneOffset AS INT=0,
	@numDomainID AS NUMERIC(18,0) = NULL,
	@numUserCntID AS NUMERIC(18,0) = NULL
)                        
AS      
BEGIN
	DECLARE @ParentBizDoc AS NUMERIC(9)=0
	DECLARE @lngJournalId AS NUMERIC(18,0)
	DECLARE @numDivisionID NUMERIC(18,0)
	
	SELECT @numDivisionID=numDivisionId FROM OpportunityMaster WHERE numOppId=@numOppId
    
	SELECT 
		ISNULL(CompanyInfo.numCompanyType,0) AS numCompanyType,
		ISNULL(CompanyInfo.vcProfile,0) AS vcProfile,
		ISNULL(OpportunityMaster.numAccountClass,0) AS numAccountClass
	FROM 
		OpportunityMaster 
	JOIN 
		DivisionMaster 
	ON 
		DivisionMaster.numDivisionId = OpportunityMaster.numDivisionId
	JOIN 
		CompanyInfo
	ON 
		CompanyInfo.numCompanyId = DivisionMaster.numCompanyID
	WHERE 
		numOppId = @numOppId
		                                  
	IF @byteMode= 1                        
	BEGIN 
		DECLARE @numBizDocID AS INT
		DECLARE @bitFulfilled AS BIT
		Declare @tintOppType as tinyint
		DECLARE @tintShipped AS BIT
		DECLARE @bitAuthoritativeBizDoc AS BIT

		SELECT @tintOppType=tintOppType,@tintShipped=ISNULL(tintshipped,0) FROM OpportunityMaster WHERE  numOppId=@numOppId  and numDomainID= @numDomainID 
		SELECT @numBizDocID = numBizDocId, @bitFulfilled=bitFulfilled, @bitAuthoritativeBizDoc=ISNULL(bitAuthoritativeBizDocs,0) FROM OpportunityBizDocs WHERE numOppBizDocsId = @numOppBizDocsId

		IF ISNULL(@tintShipped,0) = 1 AND (@numBizDocID=296 OR ISNULL(@bitAuthoritativeBizDoc,0)=1)
		BEGIN
			RAISERROR('NOT_ALLOWED_TO_DELETE_AUTHORITATIVE_BIZDOC_AFTER_ORDER_IS_CLOSED',16,1)
			RETURN
		END

		IF EXISTS (SELECT numDepositeDetailID FROM dbo.DepositeDetails WHERE numOppBizDocsID = @numOppBizDocsId)
		BEGIN
			RAISERROR('PAID',16,1);
			RETURN
		END

		IF EXISTS (SELECT OBPD.* FROM OpportunityBizDocsPaymentDetails OBPD INNER JOIN EmbeddedCost EC ON OBPD.numBizDocsPaymentDetId=EC.numBizDocsPaymentDetId WHERE EC.numOppBizDocID=@numOppBizDocsId)
		BEGIN
			RAISERROR('PAID',16,1);
			RETURN
		END

		DECLARE @tintCommitAllocation AS TINYINT
		DECLARE @bitAllocateInventoryOnPickList AS BIT

		SELECT @tintCommitAllocation=ISNULL(tintCommitAllocation,1) FROM Domain WHERE numDomainId=@numDomainID
		SELECT @bitAllocateInventoryOnPickList=ISNULL(bitAllocateInventoryOnPickList,0) FROM DivisionMaster WHERE numDivisionID=@numDivisionID

		IF ISNULL(@tintCommitAllocation,0)=2 AND ISNULL(@bitAllocateInventoryOnPickList,0) = 1 AND @numBizDocID=29397 AND (SELECT COUNT(*) FROM OpportunityBizDocs WHERE numOppId=@numOppId AND numBizDocId=296 AND numSourceBizDocId=@numOppBizDocsId) > 0
		BEGIN
			RAISERROR('NOT_ALLOWED_TO_DELETE_PICK_LIST_AFTER_FULFILLMENT_BIZDOC_IS_GENERATED_AGAINST_IT',16,1)
			RETURN
		END

		IF ISNULL(@tintCommitAllocation,0)=2 AND ISNULL(@bitAllocateInventoryOnPickList,0) = 1 AND @numBizDocID=29397
		BEGIN
			-- Revert Allocation
			EXEC USP_RevertAllocationPickList @numDomainID,@numOppID,@numOppBizDocsId,@numUserCntID
		END

		DECLARE @TEMP TABLE
		(
			ID INT IDENTITY(1,1)
			,numOppBizDocsId NUMERIC(18,0)
		)

		INSERT INTO @TEMP
		(
			numOppBizDocsId
		)
		SELECT
			numOppBizDocsId
		FROM
			OpportunityBizDocs 
		WHERE 
			numSourceBizDocId= @numOppBizDocsId

		BEGIN TRY
		BEGIN TRANSACTION

			DECLARE @i AS INT = 1
			DECLARE @iCount AS INT

			SELECT @iCount = COUNT(*) FROM @TEMP

			WHILE @i <= @iCount
			BEGIN
				SELECT @ParentBizDoc=numOppBizDocsId FROM @TEMP WHERE ID = @i

				IF NOT EXISTS (SELECT numDepositeDetailID FROM dbo.DepositeDetails WHERE numOppBizDocsID = @ParentBizDoc)
				BEGIN
					EXEC USP_OppBizDocs @byteMode=@byteMode,@numOppId=@numOppId,@numOppBizDocsId=@ParentBizDoc,@ClientTimeZoneOffset=@ClientTimeZoneOffset,@numDomainID=@numDomainID,@numUserCntID=@numUserCntID
				END

				SET @i = @i + 1
			END

			--IF it's as sales order and BizDoc is of type of fulfillment and already fulfilled revert items to allocations
			IF @tintOppType = 1 AND @numBizDocID = 296 AND @bitFulfilled = 1
			BEGIN
				EXEC USP_OpportunityBizDocs_RevertFulFillment @numDomainID,@numUserCntID,@numOppId,@numOppBizDocsId
			END

			IF @numBizDocID = 29397
			BEGIN
				UPDATE
					OI
				SET
					OI.numQtyPicked = (CASE 
											WHEN ISNULL(OI.numQtyPicked,0) > ISNULL(OBDI.numUnitHour,0) 
											THEN (ISNULL(OI.numQtyPicked,0) - ISNULL(OBDI.numUnitHour,0)) 
											ELSE 0 
										END)
				FROM	
					OpportunityBizDocItems OBDI
				INNER JOIN
					OpportunityItems OI
				ON
					OBDI.numOppItemID = OI.numoppitemtCode
				WHERE
					OBDI.numOppBizDocID=@numOppBizDocsId
			END

			DELETE FROM [ProjectsOpportunities] WHERE numOppBizDocID=@numOppBizDocsId
			DELETE FROM [ShippingReportItems] WHERE [numShippingReportId] IN  (SELECT numShippingReportId from [ShippingReport] WHERE [numOppBizDocId] = @numOppBizDocsId)
			DELETE FROM [ShippingBox] WHERE [numShippingReportId] IN  (SELECT numShippingReportId from [ShippingReport] WHERE [numOppBizDocId] = @numOppBizDocsId)
			DELETE FROM [ShippingReport] WHERE [numOppBizDocId] = @numOppBizDocsId
			DELETE  FROM OpportunityBizDocItems WHERE numOppBizDocID=@numOppBizDocsId              
			DELETE FROM OpportunityRecurring WHERE numOppBizDocID=@numOppBizDocsId and tintRecurringType=4
			UPDATE dbo.OpportunityRecurring SET numOppBizDocID=NULL WHERE numOppBizDocID=@numOppBizDocsId
			DELETE FROM [RecurringTransactionReport] WHERE [numRecTranBizDocID] = @numOppBizDocsId
				--Delete All entries for current bizdoc
			--EXEC [USP_DeleteEmbeddedCost] @numOppBizDocsId, 0, @numDomainID 
			DELETE OBPD FROM OpportunityBizDocsPaymentDetails OBPD INNER JOIN EmbeddedCost EC ON OBPD.numBizDocsPaymentDetId=EC.numBizDocsPaymentDetId WHERE EC.numOppBizDocID=@numOppBizDocsId
			DELETE ECI FROM [EmbeddedCostItems] ECI INNER JOIN EmbeddedCost EC ON ECI.numEmbeddedCostID=EC.numEmbeddedCostID WHERE EC.[numOppBizDocID]=@numOppBizDocsId AND EC.[numDomainID] = @numDomainID
			DELETE FROM [EmbeddedCost] WHERE [numOppBizDocID] = @numOppBizDocsId	AND [numDomainID] = @numDomainID
			DELETE OBDD	FROM OpportunityBizDocsDetails OBDD INNER JOIN EmbeddedCost EC ON OBDD.numBizDocsPaymentDetId = EC.numBizDocsPaymentDetId WHERE	numOppBizDocID = @numOppBizDocsId

		

			--Credit Balance
			DECLARE @monCreditAmount AS DECIMAL(20,5);
			SET @monCreditAmount=0
			SET @numDivisionID=0

			SELECT 
				@monCreditAmount=ISNULL(oppBD.monCreditAmount,0)
				,@numDivisionID=Om.numDivisionID
				,@numOppId=OM.numOppId 
			FROM 
				OpportunityBizDocs oppBD 
			JOIN 
				OpportunityMaster Om 
			ON 
				OM.numOppId=oppBD.numOppId 
			WHERE
				numOppBizDocsId = @numOppBizDocsId

			DELETE FROM [CreditBalanceHistory] where numOppBizDocsId=@numOppBizDocsId

 
			IF @tintOppType=1 --Sales
				UPDATE DivisionMaster SET monSCreditBalance=isnull(monSCreditBalance,0) + isnull(@monCreditAmount,0) where numDivisionID=@numDivisionID and numDomainID=@numDomainID
			ELSE IF @tintOppType=2 --Purchase
				UPDATE DivisionMaster SET monPCreditBalance=isnull(monPCreditBalance,0) + isnull(@monCreditAmount,0) where numDivisionID=@numDivisionID and numDomainID=@numDomainID


			DELETE FROM DocumentWorkflow WHERE numDocID=@numOppBizDocsId AND cDocType='B'      
			DELETE BA FROM BizDocAction BA INNER JOIN BizActionDetails BAD ON BA.numBizActionId=BAD.numBizActionId WHERE BAD.numOppBizDocsId =@numOppBizDocsId and btDocType=1
			DELETE FROM BizActionDetails where numOppBizDocsId=@numOppBizDocsId and btDocType=1


			DELETE GJD FROM General_Journal_Details GJD INNER JOIN General_Journal_Header GJH ON GJD.numJournalID=GJH.numJournal_Id WHERE GJH.numOppId=@numOppId AND GJH.numOppBizDocsId=@numOppBizDocsId AND GJH.numDomainId=@numDomainID
			DELETE FROM dbo.General_Journal_Header WHERE numOppId=@numOppId AND numOppBizDocsId=@numOppBizDocsId AND numDomainId= @numDomainID

			DELETE FROM OpportunityBizDocs WHERE numOppBizDocsId=@numOppBizDocsId                        

		COMMIT TRANSACTION
		END TRY
		BEGIN CATCH
			DECLARE @ErrorMessage NVARCHAR(4000)
			DECLARE @ErrorNumber INT
			DECLARE @ErrorSeverity INT
			DECLARE @ErrorState INT
			DECLARE @ErrorLine INT
			DECLARE @ErrorProcedure NVARCHAR(200)

			IF @@TRANCOUNT > 0
				ROLLBACK TRANSACTION

			SELECT 
				@ErrorMessage = ERROR_MESSAGE(),
				@ErrorNumber = ERROR_NUMBER(),
				@ErrorSeverity = ERROR_SEVERITY(),
				@ErrorState = ERROR_STATE(),
				@ErrorLine = ERROR_LINE(),
				@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

			RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
		END CATCH             
	END                        
                        
if @byteMode= 2                        
begin  

DECLARE @BaseCurrencySymbol nVARCHAR(3);SET @BaseCurrencySymbol='$'
SELECT @BaseCurrencySymbol = ISNULL(varCurrSymbol,'$') FROM dbo.Currency 
														   WHERE numCurrencyID IN ( SELECT numCurrencyID FROM dbo.Domain 
																					WHERE numDomainId=@numDomainId)
--PRINT @BaseCurrencySymbol

																					                      
select *, isnull(X.monPAmount,0)-X.monAmountPaid as BalDue from (select  opp.numOppBizDocsId,                        
 opp.numOppId,                        
 opp.numBizDocId,                        
 ISNULL((SELECT TOP 1 numOrientation FROM BizDocTemplate WHERE numBizDocTempID = Opp.numBizDocTempID),0) AS numOrientation, 
 ISNULL((SELECT TOP 1 vcTemplateName FROM BizDocTemplate WHERE numBizDocTempID = Opp.numBizDocTempID),0) AS vcTemplateName, 
 ISNULL((SELECT TOP 1 bitKeepFooterBottom FROM BizDocTemplate WHERE numBizDocTempID = Opp.numBizDocTempID),0) AS bitKeepFooterBottom,                  
 oppmst.vcPOppName,                        
 ISNULL(ListDetailsName.vcName,mst.vcData) as BizDoc,                        
 opp.numCreatedBy,                        
DATEADD(minute,-@ClientTimeZoneOffset,opp.dtCreatedDate) CreatedDate,
 opp.vcRefOrderNo,                       
 vcBizDocID,
 CASE WHEN LEN(ISNULL(vcBizDocName,''))=0 THEN vcBizDocID ELSE vcBizDocName END vcBizDocName,      
  [dbo].[GetDealAmount](@numOppId ,getutcdate(),opp.numOppBizDocsId ) as monPAmount,
  CASE 
	WHEN tintOppType=1  
	THEN (CASE 
			WHEN (ISNULL(numAuthoritativeSales,0)=numBizDocId OR numBizDocId=296) 
			THEN 'Authoritative' + CASE WHEN ISNULL(Opp.tintDeferred,0)=1 THEN ' (Deferred)' ELSE '' END 
			WHEN opp.numBizDocId =  304 THEN 'Deferred-Authoritative'
			ELSE 'Non-Authoritative' END ) 
  when tintOppType=2  then (case when isnull(numAuthoritativePurchase,0)=numBizDocId then 'Authoritative' + Case when isnull(Opp.tintDeferred,0)=1 then ' (Deferred)' else '' end else 'Non-Authoritative' end )  end as BizDocType,
  isnull(monAmountPaid,0) monAmountPaid, 
  ISNULL(C.varCurrSymbol,'') varCurrSymbol ,
  ISNULL(Opp.[bitAuthoritativeBizDocs],0) bitAuthoritativeBizDocs,
  ISNULL(oppmst.[tintshipped] ,0) tintshipped,isnull(opp.numShipVia,0) as numShipVia,oppmst.numDivisionId,isnull(Opp.tintDeferred,0) as tintDeferred,
  ISNULL(Opp.numBizDocTempID,0) AS numBizDocTempID,con.numContactid,isnull(con.vcEmail,'') AS vcEmail,ISNULL(Opp.numBizDocStatus,0) AS numBizDocStatus,
--  case when isnumeric(ISNULL(dbo.fn_GetListItemName(isnull(oppmst.intBillingDays,0)),0))=1 
--	   then ISNULL(dbo.fn_GetListItemName(isnull(oppmst.intBillingDays,0)),0) 
--	   else 0 
--  end AS numBillingDaysName,
  CASE WHEN ISNUMERIC((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(oppmst.intBillingDays,0))) = 1
 	   THEN (SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(oppmst.intBillingDays,0))
	   ELSE 0
  END AS numBillingDaysName,
  DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,opp.dtFromDate) dtFromDate,
  DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,opp.dtFromDate) AS BillingDate,
  ISNULL(C.fltExchangeRate,1) fltExchangeRateCurrent,
  ISNULL(oppmst.fltExchangeRate,1) fltExchangeRateOfOrder,
  @BaseCurrencySymbol BaseCurrencySymbol,0 AS numReturnHeaderID,0 AS tintReturnType,ISNULL(Opp.fltExchangeRateBizDoc,ISNULL(oppmst.fltExchangeRate,1)) AS fltExchangeRateBizDoc,
  CASE WHEN (SELECT COUNT(*) FROM dbo.ShippingReport WHERE numOppBizDocId = opp.numOppBizDocsId AND numDomainID = @numDomainID) > 0 THEN 1 ELSE 0 END AS [IsShippingReportGenerated],
  --CASE WHEN (SELECT ISNULL(vcTrackingNo,'') FROM dbo.OpportunityBizDocs WHERE numOppBizDocsId = opp.numOppBizDocsId) <> '' THEN 1 ELSE 0 END AS [ISTrackingNumGenerated]
  CASE WHEN (SELECT COUNT(*) FROM dbo.ShippingBox 
			 WHERE numShippingReportId IN (SELECT numShippingReportId FROM dbo.ShippingReport WHERE numOppBizDocId = opp.numOppBizDocsId AND numDomainID = @numDomainID)
			 AND LEN(ISNULL(vcTrackingNumber,'')) > 0) > 0 THEN 1 ELSE 0 END AS [ISTrackingNumGenerated],
  ISNULL((SELECT MAX(numShippingReportId) from ShippingReport SR 
		  WHERE SR.numOppBizDocId = opp.numoppbizdocsid 
		  AND SR.numDomainID = oppmst.numDomainID),0) AS numShippingReportId,
  ISNULL((SELECT MAX(SB.numShippingReportId) FROM [ShippingBox] SB 
		  JOIN dbo.ShippingReport SR ON SB.numShippingReportId = SR.numShippingReportId 
		  where SR.numOppBizDocId = opp.numoppbizdocsid AND SR.numDomainID = oppmst.numDomainID 
		  AND LEN(ISNULL(SB.vcTrackingNumber,''))>0),0) AS numShippingLabelReportId			 
 ,ISNULL(STUFF(( SELECT  ',' +  CASE WHEN LEN(ISNULL(W.[vcWareHouse],'')  + ':' + ISNULL([WL].[vcLocation],'')) > 0 THEN ISNULL(W.[vcWareHouse],'')  + ':' + ISNULL([WL].[vcLocation],'') ELSE '' END
                FROM    [dbo].[WarehouseLocation] AS WL
				JOIN [dbo].[Warehouses] W ON [WL].[numWarehouseID] = [W].[numWareHouseID]
                WHERE  [WL].[numWLocationID] IN (
                        SELECT  WHI.[numWLocationID]
                        FROM    [dbo].[WareHouseItems] AS WHI
                                JOIN [dbo].[OpportunityItems] AS OI ON [WHI].[numWareHouseItemID] = [OI].[numWarehouseItmsID]
                        WHERE   [OI].[numOppId] = oppmst.[numOppId])
              FOR
                XML PATH('')
              ), 1, 1, ''),'') AS [vcLocation],
		(CASE WHEN ISNULL(RecurrenceConfiguration.numRecConfigID,0) = 0 THEN 0 ELSE 1 END) AS bitRecur,
		ISNULL(Opp.bitRecurred,0) AS bitRecurred,
		ISNULL((SELECT COUNT(*) FROM OpportunityBizDocs WHERE numOppID=@numOppID AND ISNULL(numDeferredBizDocID,0) = ISNULL(opp.numOppBizDocsId,0)),0) AS intInvoiceForDiferredBizDoc,
		ISNULL(Opp.bitShippingGenerated,0) AS bitShippingGenerated,
		ISNULL(oppmst.intUsedShippingCompany,0) as intUsedShippingCompany,
		numSourceBizDocId,
		ISNULL((SELECT TOP 1 vcBizDocID FROM OpportunityBizDocs WHERE numOppBizDOcsId=opp.numSourceBizDocId),'NA') AS vcSourceBizDocID,
		ISNULL(oppmst.numShipmentMethod,0) numShipmentMethod,
		ISNULL(oppmst.numShippingService,0) numShippingService,
		(CASE WHEN (SELECT COUNT(*) FROM OpportunityBizDocs OBInner WHERE OBInner.numOppID=@numOppID AND OBInner.numBizDocId=296 AND OBInner.numSourceBizDocId=opp.numOppBizDocsId AND opp.numBizDocId=29397) > 0 THEN 1 ELSE 0 END) AS IsFulfillmentGeneratedOnPickList
 FROM                         
 OpportunityBizDocs  opp                        
 left join ListDetails mst                        
 on mst.numListItemID=opp.numBizDocId  
 LEFT JOIN ListDetailsName
 ON ListDetailsName.numListItemID=opp.numBizDocId 
 AND ListDetailsName.numDomainID=@numDomainID
 join OpportunityMaster oppmst                        
 on oppmst.numOppId= opp.numOppId                                
 left join  AuthoritativeBizDocs  AB
 on AB.numDomainId=  oppmst.numDomainID             
 LEFT OUTER JOIN [Currency] C
  ON C.numCurrencyID = oppmst.numCurrencyID
  LEFT JOIN AdditionalContactsInformation con ON con.numContactid = oppmst.numContactId
  LEFT JOIN
	RecurrenceConfiguration
  ON
	RecurrenceConfiguration.numOppBizDocID = opp.numOppBizDocsId
where opp.numOppId=@numOppId

UNION ALL

select  0 AS numOppBizDocsId,                        
 RH.numOppId,                        
 0 AS numBizDocId,                        
 1 AS numOrientation,  
 '' AS vcTemplateName,
 0 AS bitKeepFooterBottom,                   
 '' AS vcPOppName,                        
 'RMA' as BizDoc,                        
 RH.numCreatedBy,                        
DATEADD(minute,-@ClientTimeZoneOffset,RH.dtCreatedDate) CreatedDate,
 '' vcRefOrderNo,                       
 RH.vcRMA AS vcBizDocID,
 RH.vcRMA AS vcBizDocName,      
  ISNULL(monAmount,0) as monPAmount,
  'RMA' as BizDocType,
  0 AS monAmountPaid, 
  '' varCurrSymbol ,
  0 bitAuthoritativeBizDocs,
  0 tintshipped,0 as numShipVia,RH.numDivisionId,0 as tintDeferred,
  0 AS numBizDocTempID,con.numContactid,isnull(con.vcEmail,'') AS vcEmail,0 AS numBizDocStatus,
  0 AS numBillingDaysName,RH.dtCreatedDate AS dtFromDate,
  RH.dtCreatedDate AS BillingDate,
  1 fltExchangeRateCurrent,
  1 fltExchangeRateOfOrder,
  @BaseCurrencySymbol BaseCurrencySymbol,RH.numReturnHeaderID,RH.tintReturnType,1 AS fltExchangeRateBizDoc
  ,0 [IsShippingReportGenerated],0 [ISTrackingNumGenerated],
  0 AS numShippingReportId,
  0 AS numShippingLabelReportId			 
  ,ISNULL(STUFF(( SELECT  ',' +  CASE WHEN LEN(ISNULL(W.[vcWareHouse],'')  + ':' + ISNULL([WL].[vcLocation],'')) > 0 THEN ISNULL(W.[vcWareHouse],'')  + ':' + ISNULL([WL].[vcLocation],'') ELSE '' END
                FROM    [dbo].[WarehouseLocation] AS WL
				JOIN [dbo].[Warehouses] W ON [WL].[numWarehouseID] = [W].[numWareHouseID]
                WHERE  [WL].[numWLocationID] IN (
                        SELECT  WHI.[numWLocationID]
                        FROM    [dbo].[WareHouseItems] AS WHI
                                JOIN [dbo].[OpportunityItems] AS OI ON [WHI].[numWareHouseItemID] = [OI].[numWarehouseItmsID]
                        WHERE   [OI].[numOppId] = RH.[numOppId])
              FOR
                XML PATH('')
              ), 1, 1, ''),'') AS [vcLocation],
			  0 AS bitRecur,
			  0 AS bitRecurred,
		0 AS intInvoiceForDiferredBizDoc ,
		0 AS bitShippingGenerated,
		0 as intUsedShippingCompany,
		0 AS numSourceBizDocId,
		'NA' AS vcSourceBizDocID,
		0 AS numShipmentMethod,
		0 AS numShippingService
		,0 AS IsFulfillmentGeneratedOnPickList
from                         
 ReturnHeader RH                        
 LEFT JOIN AdditionalContactsInformation con ON con.numContactid = RH.numContactId
  
where RH.numOppId=@numOppId
)X ORDER BY CreatedDate ASC       
                        
END
END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OPPGetINItems')
DROP PROCEDURE USP_OPPGetINItems
GO
-- =============================================  
-- Modified by: <Author,,Sachin Sadhu>  
-- Create date: <Create Date,,24thDec2013>  
-- Description: <Description,,add 3 fields:vcBizDocImagePath,vcBizDocFooter,vcPurBizDocFooter>  
-- =============================================  
CREATE PROCEDURE [dbo].[USP_OPPGetINItems]
(
	@byteMode AS TINYINT  = NULL,
	@numOppId AS NUMERIC(18,0)  = NULL,
	@numOppBizDocsId AS NUMERIC(18,0)  = NULL,
	@numDomainID AS NUMERIC(18,0)  = 0,
	@numUserCntID AS NUMERIC(9)  = 0,
	@ClientTimeZoneOffset INT=0
)
AS
BEGIN
	-- TRANSFERRED PARAMETER VALUES TO TEMPORARY VARIABLES TO AVAOID PARAMETER SNIFFING
	DECLARE @byteModeTemp AS TINYINT
	DECLARE @numOppIdTemp AS NUMERIC(18,0)
	DECLARE @numOppBizDocsIdTemp AS NUMERIC(18,0)
	DECLARE @numDomainIDTemp AS NUMERIC(18,0)
	DECLARE @numUserCntIDTemp AS NUMERIC(18,0)
	DECLARE @ClientTimeZoneOffsetTemp INT

	SET @byteModeTemp= @byteMode
	SET @numOppIdTemp = @numOppId
	SET @numOppBizDocsIdTemp=@numOppBizDocsId
	SET @numDomainIDTemp = @numDomainID
	SET @numUserCntIDTemp = @numUserCntID
	SET @ClientTimeZoneOffsetTemp = @ClientTimeZoneOffset


	IF @byteModeTemp = 1
    BEGIN
      DECLARE  @BizDcocName  AS VARCHAR(100)
      DECLARE  @OppName  AS VARCHAR(100)
      DECLARE  @Contactid  AS VARCHAR(100)
      DECLARE  @shipAmount  AS DECIMAL(20,5)
      DECLARE  @tintOppType  AS TINYINT
      DECLARE  @numlistitemid  AS VARCHAR(15)
      DECLARE  @RecOwner  AS VARCHAR(15)
      DECLARE  @MonAmount  AS VARCHAR(15)
      DECLARE  @OppBizDocID  AS VARCHAR(100)
      DECLARE  @bizdocOwner  AS VARCHAR(15)
      DECLARE  @tintBillType  AS TINYINT
      DECLARE  @tintShipType  AS TINYINT
      DECLARE  @numCustomerDivID AS NUMERIC 
      
      SELECT @RecOwner = numRecOwner,@MonAmount = CONVERT(VARCHAR(20),monPAmount),
             @Contactid = numContactId,@OppName = vcPOppName,
             @tintOppType = tintOppType,@tintBillType = tintBillToType,@tintShipType = tintShipToType
      FROM   OpportunityMaster WHERE  numOppId = @numOppIdTemp
      
      -- When Creating PO from SO and Bill type is Customer selected 
      SELECT @numCustomerDivID = ISNULL(numDivisionID,0) FROM dbo.OpportunityMaster WHERE 
			numOppID IN (SELECT  ISNULL(numParentOppID,0) FROM dbo.OpportunityLinking WHERE numChildOppID = @numOppIdTemp)
			
      --select @BizDcocName=vcBizDocID from OpportunityBizDocs where numOppBizDocsId=@numOppBizDocsIdTemp
      SELECT @BizDcocName = vcdata,
             @shipAmount = monShipCost,
             @numlistitemid = numlistitemid,
             @OppBizDocID = vcBizDocID,
             @bizdocOwner = OpportunityBizDocs.numCreatedBy
      FROM   listdetails JOIN OpportunityBizDocs ON numBizDocId = numlistitemid
      WHERE  numOppBizDocsId = @numOppBizDocsIdTemp
      
      SELECT @bizdocOwner AS BizDocOwner,
             @OppBizDocID AS BizDocID,
             @MonAmount AS Amount,
             isnull(@RecOwner,1) AS Owner,
             @BizDcocName AS BizDcocName,
             @numlistitemid AS BizDoc,
             @OppName AS OppName,
             VcCompanyName AS CompName,
             @shipAmount AS ShipAmount,
             dbo.fn_getOPPAddress(@numOppIdTemp,@numDomainIDTemp,1) AS BillAdd,
             dbo.fn_getOPPAddress(@numOppIdTemp,@numDomainIDTemp,2) AS ShipAdd,
             AD.VcStreet,
             AD.VcCity,
             isnull(dbo.fn_GetState(AD.numState),'') AS vcBilState,
             AD.vcPostalCode,
             isnull(dbo.fn_GetListItemName(AD.numCountry),
                    '') AS vcBillCountry,
             con.vcFirstName AS ConName,
             ISNULL(con.numPhone,'') numPhone,
             con.vcFax,
             isnull(con.vcEmail,'') AS vcEmail,
             div.numDivisionID,
             numContactid,
             isnull(vcFirstFldValue,'') vcFirstFldValue,
             isnull(vcSecndFldValue,'') vcSecndFldValue,
             isnull(bitTest,0) AS bitTest,
			dbo.fn_getOPPAddress(@numOppIdTemp,@numDomainIDTemp,1) AS BillToAddressName,
			dbo.fn_getOPPAddress(@numOppIdTemp,@numDomainIDTemp,1) AS ShipToAddressName,
             CASE 
               WHEN @tintBillType IS NULL THEN Com.vcCompanyName
             	-- When Sales order and bill to is set to customer 
               WHEN @tintBillType = 1 AND @tintOppType = 1 THEN Com.vcCompanyName
             -- When Create PO from SO and Bill to is set to Customer 
			   WHEN @tintBillType = 1 AND @tintOppType = 2 THEN dbo.fn_GetComapnyName(@numCustomerDivID)
               WHEN @tintBillType = 0 THEN (SELECT TOP 1 Com1.vcCompanyName
                                            FROM   companyinfo [Com1]
                                                   JOIN divisionmaster div1
                                                     ON com1.numCompanyID = div1.numCompanyID
                                                   JOIN Domain D1
                                                     ON D1.numDivisionID = div1.numDivisionID
                                                    JOIN dbo.AddressDetails AD1
														ON AD1.numDomainID = div1.numDomainID AND AD1.numRecordID = div1.numDivisionID AND tintAddressOf = 2 AND tintAddressType = 1 AND bitIsPrimary=1
                                            WHERE  D1.numDomainID = @numDomainIDTemp)
               WHEN @tintBillType = 2 THEN (SELECT vcBillCompanyName
                                            FROM   OpportunityAddress
                                            WHERE  numOppID = @numOppIdTemp)
				WHEN @tintBillType = 3 THEN  Com.vcCompanyName
              END AS BillToCompanyName,
             CASE 
               WHEN @tintShipType IS NULL THEN Com.vcCompanyName
               WHEN @tintShipType = 1 AND @tintOppType = 1 THEN Com.vcCompanyName
				-- When Create PO from SO and Ship to is set to Customer 
   			   WHEN @tintShipType = 1 AND @tintOppType = 2 THEN dbo.fn_GetComapnyName(@numCustomerDivID)
               WHEN @tintShipType = 0 THEN (SELECT TOP 1 Com1.vcCompanyName
                                            FROM   companyinfo [Com1]
                                                   JOIN divisionmaster div1
                                                     ON com1.numCompanyID = div1.numCompanyID
                                                   JOIN Domain D1
                                                     ON D1.numDivisionID = div1.numDivisionID
                                                    JOIN dbo.AddressDetails AD1
													 ON AD1.numDomainID = div1.numDomainID AND AD1.numRecordID = div1.numDivisionID AND tintAddressOf = 2 AND tintAddressType = 2 AND bitIsPrimary=1
                                            WHERE  D1.numDomainID = @numDomainIDTemp)
               WHEN @tintShipType = 2 THEN (SELECT vcShipCompanyName
                                            FROM   OpportunityAddress
                                            WHERE  numOppID = @numOppIdTemp)
				WHEN @tintShipType = 3 THEN (SELECT vcShipCompanyName
														FROM   OpportunityAddress
														WHERE  numOppID = @numOppIdTemp)
             END AS ShipToCompanyName
      FROM   companyinfo [Com] JOIN divisionmaster div ON com.numCompanyID = div.numCompanyID
             JOIN AdditionalContactsInformation con ON con.numdivisionId = div.numdivisionId
             JOIN Domain D ON D.numDomainID = div.numDomainID
             LEFT JOIN PaymentGatewayDTLID PGDTL ON PGDTL.intPaymentGateWay = D.intPaymentGateWay AND D.numDomainID = PGDTL.numDomainID
             LEFT JOIN AddressDetails AD --Billing add
				ON AD.numDomainID=DIV.numDomainID AND AD.numRecordID=div.numDivisionID AND AD.tintAddressOf = 2 AND AD.tintAddressType = 1 AND AD.bitIsPrimary=1
			 LEFT JOIN AddressDetails AD2--Shipping address
				ON AD2.numDomainID=DIV.numDomainID AND AD2.numRecordID=div.numDivisionID AND AD2.tintAddressOf = 2 AND AD2.tintAddressType = 2 AND AD2.bitIsPrimary=1	
      WHERE  numContactid = @Contactid AND Div.numDomainID = @numDomainIDTemp
    END
  IF @byteModeTemp = 2
    BEGIN
      SELECT opp.vcBizDocID,
             Mst.fltDiscount AS decDiscount,
             isnull(opp.monAmountPaid,0) AS monAmountPaid,
             isnull(opp.vcComments,'') AS vcComments,
             CONVERT(VARCHAR(20),DATEADD(minute,-@ClientTimeZoneOffsetTemp,opp.dtCreatedDate)) dtCreatedDate,
			 dbo.fn_GetContactName(Opp.numCreatedby)+ ' ' + CONVERT(VARCHAR(20),dateadd(MINUTE,-@ClientTimeZoneOffsetTemp,Opp.dtCreatedDate)) AS CreatedBy,
             dbo.fn_GetContactName(opp.numCreatedby) AS numCreatedby,
             dbo.fn_GetContactName(opp.numModifiedBy) AS numModifiedBy,
             CONVERT(VARCHAR(20),DATEADD(minute,-@ClientTimeZoneOffsetTemp,opp.dtModifiedDate)) dtModifiedDate,
			 dbo.fn_GetContactName(Opp.numModifiedBy) + ' ' + CONVERT(VARCHAR(20),dateadd(MINUTE,-@ClientTimeZoneOffsetTemp,Opp.dtModifiedDate)) AS ModifiedBy,
             CASE 
               WHEN isnull(opp.numViewedBy,0) = 0 THEN ''
               ELSE dbo.fn_GetContactName(opp.numViewedBy)
             END AS numViewedBy,
             CASE 
               WHEN opp.numViewedBy = 0 THEN NULL
               ELSE opp.dtViewedDate
             END AS dtViewedDate,
             (CASE WHEN isnull(Mst.intBillingDays,0) > 0 THEN CAST(1 AS BIT) ELSE isnull(Mst.bitBillingTerms,0) END) AS tintBillingTerms,
             isnull(Mst.intBillingDays,0) AS numBillingDays,
			 ISNULL(BTR.numNetDueInDays,0) AS numBillingDaysName,
			 BTR.vcTerms AS vcBillingTermsName,
             isnull(Mst.bitInterestType,0) AS tintInterestType,
             isnull(Mst.fltInterest,0) AS fltInterest,
             tintOPPType,
			 tintOppStatus,
             Opp.numBizDocId,
             dbo.fn_GetContactName(numApprovedBy) AS ApprovedBy,
             dtApprovedDate,
             Mst.bintAccountClosingDate,
             tintShipToType,
             tintBillToType,
             tintshipped,
             dtShippedDate bintShippedDate,
             isnull(opp.monShipCost,0) AS monShipCost,
             (SELECT COUNT(* )
              FROM   DocumentWorkflow
              WHERE  numDocID = @numOppBizDocsIdTemp
                     AND numContactID = @numUserCntIDTemp
                     AND cDocType = 'B'
                     AND tintApprove = 0) AS AppReq,
             (SELECT COUNT(* )
              FROM   DocumentWorkflow
              WHERE  numDocID = @numOppBizDocsIdTemp
                     AND cDocType = 'B'
                     AND tintApprove = 1) AS Approved,
             (SELECT COUNT(* )
              FROM   DocumentWorkflow
              WHERE  numDocID = @numOppBizDocsIdTemp
                     AND cDocType = 'B'
                     AND tintApprove = 2) AS Declined,
             (SELECT COUNT(* )
              FROM   DocumentWorkflow
              WHERE  numDocID = @numOppBizDocsIdTemp
                     AND cDocType = 'B'
                     AND tintApprove = 0) AS Pending,
             ISNULL(BT.vcBizDocFooter,D.vcBizDocFooter) AS BizdocFooter,
             ISNULL(BT.vcBizDocImagePath,D.vcBizDocImagePath) As vcBizDocImagePath, 
             Mst.numDivisionID,
             ISNULL(BT.vcPurBizDocFooter,D.vcPurBizDocFooter) AS PurBizdocFooter, 
             isnull(Mst.fltDiscount,0) AS fltDiscount,
             isnull(Mst.bitDiscountType,0) AS bitDiscountType,
             isnull(Opp.numShipVia,ISNULL(Mst.intUsedShippingCompany,0)) AS numShipVia,
--             isnull(vcTrackingURL,'') AS vcTrackingURL,
			ISNULL( (SELECT vcShipFieldValue FROM dbo.ShippingFieldValues WHERE numDomainID=MSt.numDomainID AND intShipFieldID IN ( SELECT intShipFieldID FROM dbo.ShippingFields WHERE numListItemID=Opp.numShipVia  AND vcFieldName='Tracking URL')),'')		AS vcTrackingURL,
             isnull(Opp.numBizDocStatus,0) AS numBizDocStatus,
             CASE 
               WHEN Opp.numBizDocStatus IS NULL THEN '-'
               ELSE dbo.fn_GetListItemName(Opp.numBizDocStatus)
             END AS BizDocStatus,
             CASE 
               WHEN ISNULL(Opp.numShipVia,0) = 0 THEN (CASE WHEN Mst.intUsedShippingCompany IS NULL THEN '-' WHEN Mst.intUsedShippingCompany = -1 THEN 'Will-call' ELSE dbo.fn_GetListItemName(Mst.intUsedShippingCompany) END)
			   WHEN Opp.numShipVia = -1 THEN 'Will-call'
               ELSE dbo.fn_GetListItemName(Opp.numShipVia)
             END AS ShipVia,
			 ISNULL((SELECT vcShipmentService FROM ShippingService WHERE (numDomainID=@numDomainIDTemp OR ISNULL(numDomainID,0)=0) AND numShippingServiceID = Mst.numShippingService),'') AS vcShippingService,
             ISNULL(C.varCurrSymbol,'') varCurrSymbol,
             (SELECT COUNT(*) FROM [ShippingReport] WHERE numOppBizDocId = opp.numOppBizDocsId )AS ShippingReportCount,
			 isnull(bitPartialFulfilment,0) bitPartialFulfilment,
			 ISNULL(Opp.vcBizDocName,'') vcBizDocName,
			 dbo.[fn_GetContactName](opp.[numModifiedBy])  + ', '
				+ CONVERT(VARCHAR(20),dateadd(MINUTE,-@ClientTimeZoneOffsetTemp,Opp.dtModifiedDate)) AS ModifiedBy,
			 dbo.[fn_GetContactName](Mst.[numRecOwner]) + ', ' +  dbo.[fn_GetContactName](Mst.[numAssignedTo]) AS OrderRecOwner,
			 dbo.[fn_GetContactName](DM.[numRecOwner]) + ', ' +  dbo.[fn_GetContactName](DM.[numAssignedTo]) AS AccountRecOwner,
			 DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,Opp.dtFromDate) AS dtFromDate,
			 Mst.tintTaxOperator,
			 dbo.GetAccountedEmbeddedCost(opp.numOppBizDocsID,1) AS monTotalEmbeddedCost,
			 dbo.GetAccountedEmbeddedCost(opp.numOppBizDocsID,2) AS monTotalAccountedCost,
			 ISNULL(BT.bitEnabled,0) bitEnabled,
			 ISNULL(BT.txtBizDocTemplate,'') txtBizDocTemplate, 
			 ISNULL(BT.txtCSS,'') txtCSS,
			 ISNULL(BT.numOrientation,0) numOrientation,
			 ISNULL(BT.bitKeepFooterBottom,0) bitKeepFooterBottom,
			 dbo.fn_GetContactName(Mst.numAssignedTo) AS AssigneeName,
			 ISNULL((SELECT vcEmail FROM dbo.AdditionalContactsInformation WHERE numContactID = Mst.numAssignedTo),'') AS AssigneeEmail,
			 ISNULL((SELECT numPhone FROM dbo.AdditionalContactsInformation WHERE numContactID = Mst.numAssignedTo),'') AS AssigneePhone,
			 dbo.fn_GetComapnyName(Mst.numDivisionId) OrganizationName,
			 dbo.fn_GetContactName(Mst.numContactID)  OrgContactName,

--CASE WHEN Mst.tintOppType = 1 THEN 
--dbo.getCompanyAddress((select ISNULL(DM.numDivisionID,0)  as numDivisionID from AdditionalContactsInformation AC INNER JOIN DivisionMaster DM 
--on AC.numDivisionID = DM.numDivisionID where numContactID = Mst.numCreatedBy),1,Mst.numDomainId)
--               ELSE dbo.getCompanyAddress(Mst.numDivisionId,1,Mst.numDomainId)
--             END AS CompanyBillingAddress1,

CASE WHEN Mst.tintOppType = 1 THEN 
dbo.getCompanyAddress((select ISNULL(AC.numDivisionID,0)  as numDivisionID from AdditionalContactsInformation AC
INNER JOIN Domain D ON D.numDomainID = AC.numDomainID where AC.numContactID = D.numAdminID AND D.numDomainID = Mst.numDomainId),1,Mst.numDomainId)
               ELSE dbo.getCompanyAddress(Mst.numDivisionId,1,Mst.numDomainId)
             END AS CompanyBillingAddress,

CASE WHEN Mst.tintOppType = 1 THEN 
dbo.getCompanyAddress((select ISNULL(AC.numDivisionID,0)  as numDivisionID from AdditionalContactsInformation AC
INNER JOIN Domain D ON D.numDomainID = AC.numDomainID where AC.numContactID = D.numAdminID AND D.numDomainID = Mst.numDomainId),1,Mst.numDomainId)
               ELSE dbo.getCompanyAddress(Mst.numDivisionId,2,Mst.numDomainId)
             END AS CompanyShippingAddress,
--
--			 dbo.getCompanyAddress(Mst.numDivisionId,1,Mst.numDomainId) CompanyBillingAddress,
--			 dbo.getCompanyAddress(Mst.numDivisionId,2,Mst.numDomainId) CompanyShippingAddress,
			 case when ACI.numPhone<>'' then ACI.numPhone +case when ACI.numPhoneExtension<>'' then ' - ' + ACI.numPhoneExtension else '' end  else '' END OrgContactPhone,
			 DM.vcComPhone as OrganizationPhone,
             ISNULL(ACI.vcEmail,'') AS OrgContactEmail,
 			 dbo.[fn_GetContactName](Mst.[numRecOwner]) AS OnlyOrderRecOwner,
 			 ISNULL(Opp.bitAuthoritativeBizDocs,0) bitAuthoritativeBizDocs,
			 ISNULL(Opp.tintDeferred,0) tintDeferred,isnull(Opp.monCreditAmount,0) AS monCreditAmount,
			 isnull(Opp.bitRentalBizDoc,0) as bitRentalBizDoc,isnull(BT.numBizDocTempID,0) as numBizDocTempID,isnull(BT.vcTemplateName,'') as vcTemplateName,
		     [dbo].[GetDealAmount](opp.numOppId ,getutcdate(),opp.numOppBizDocsId ) as monPAmount,
			 --(select top 1 SD.vcSignatureFile from SignatureDetail SD join OpportunityBizDocsDetails OBD on SD.numSignID=OBD.numSignID where SD.numDomainID = @numDomainIDTemp and OBD.numDomainID = @numDomainIDTemp and OBD.numSignID IS not null and OBD.numBizDocsId=opp.numOppBizDocsId order by OBD.numBizDocsPaymentDetId desc) as vcSignatureFile,
			 isnull(CMP.txtComments,'') as vcOrganizationComments,
			 isnull(Opp.vcTrackingNo,'') AS  vcTrackingNo
			 ,'' AS  vcShippingMethod
			 ,Opp.dtDeliveryDate,
			 CASE WHEN Opp.vcRefOrderNo IS NULL OR LEN(Opp.vcRefOrderNo)=0 THEN ISNULL(Mst.vcOppRefOrderNo,'') ELSE 
			  isnull(Opp.vcRefOrderNo,'') END AS vcRefOrderNo,ISNULL(Mst.numDiscountAcntType,0) AS numDiscountAcntType,
			 ISNULL(opp.numSequenceId,'') AS numSequenceId,
			 CASE WHEN ISNULL(Opp.numBizDocId,0)=296 THEN ISNULL((SELECT SUBSTRING((SELECT '$^$' + dbo.fn_GetListItemName(numBizDocStatus) +'#^#'+ dbo.fn_GetContactName(numUserCntID) +'#^#'+ dbo.FormatedDateTimeFromDate(DateAdd(minute, -@ClientTimeZoneOffsetTemp , dtCreatedDate),@numDomainIDTemp) + '#^#'+ isnull(DFCS.vcColorScheme,'NoForeColor')
					FROM OppFulfillmentBizDocsStatusHistory OFBS left join DycFieldColorScheme DFCS on OFBS.numBizDocStatus=DFCS.vcFieldValue AND DFCS.numDomainID=@numDomainIDTemp
					WHERE numOppId=opp.numOppId and numOppBizDocsId=opp.numOppBizDocsId FOR XML PATH('')),4,200000)),'') ELSE '' END AS vcBizDocsStatusList,ISNULL(Mst.numCurrencyID,0) AS numCurrencyID,
			ISNULL(Opp.fltExchangeRateBizDoc,Mst.fltExchangeRate) AS fltExchangeRateBizDoc,
			ISNULL(TBLEmployer.vcCompanyName,'') AS EmployerOrganizationName,ISNULL(TBLEmployer.vcComPhone,'') as EmployerOrganizationPhone,ISNULL(opp.bitAutoCreated,0) AS bitAutoCreated,
			ISNULL(RecurrenceConfiguration.numRecConfigID,0) AS numRecConfigID,
			(CASE WHEN RecurrenceConfiguration.numRecConfigID IS NULL THEN 0 ELSE 1 END) AS bitRecur,
			(CASE WHEN RecurrenceTransaction.numRecTranID IS NULL THEN 0 ELSE 1 END) AS bitRecurred,
			ISNULL(Mst.bitRecurred,0) AS bitOrderRecurred,
			dbo.FormatedDateFromDate(RecurrenceConfiguration.dtStartDate,@numDomainIDTemp) AS dtStartDate,
			RecurrenceConfiguration.vcFrequency AS vcFrequency,
			ISNULL(RecurrenceConfiguration.bitDisabled,0) AS bitDisabled,
			ISNULL(opp.bitFulfilled,0) AS bitFulfilled,
			ISNULL(opp.numDeferredBizDocID,0) AS numDeferredBizDocID,
			dbo.GetTotalQtyByUOM(opp.numOppBizDocsId) AS vcTotalQtybyUOM,
			ISNULL(Mst.numShippingService,0) AS numShippingService,
			dbo.FormatedDateFromDate(Mst.dtReleaseDate,@numDomainIDTemp) AS vcReleaseDate,
			dbo.FormatedDateFromDate(Mst.intpEstimatedCloseDate,@numDomainIDTemp) AS vcRequiredDate,
			(CASE WHEN ISNULL(Mst.numPartner,0) > 0 THEN ISNULL((SELECT CONCAT(DivisionMaster.vcPartnerCode,'-',CompanyInfo.vcCompanyName) FROM DivisionMaster INNER JOIN CompanyInfo ON DivisionMaster.numCompanyID=CompanyInfo.numCompanyId WHERE DivisionMaster.numDivisionID=Mst.numPartner),'') ELSE '' END) AS vcPartner,
			dbo.FormatedDateFromDate(mst.dtReleaseDate,@numDomainIDTemp) AS dtReleaseDate,
			(select TOP 1 vcBizDocID from OpportunityBizDocs where numOppId=opp.numOppId and numBizDocId=296) as vcFulFillment,
			(select TOP 1 vcOppRefOrderNo from OpportunityMaster WHERE numOppId=opp.numOppId) AS vcPOName,
			ISNULL(vcCustomerPO#,'') AS vcCustomerPO#,
			ISNULL(Mst.txtComments,'') vcSOComments,
			ISNULL((SELECT DMSA.vcAccountNumber FROm [dbo].[DivisionMasterShippingAccount] DMSA WHERE DMSA.numDivisionID=DM.numDivisionID AND DMSA.numShipViaID=opp.numShipVia),'') AS vcShippersAccountNo,
			CONVERT(VARCHAR(20),DATEADD(minute,-@ClientTimeZoneOffsetTemp,Mst.bintCreatedDate)) OrderCreatedDate,
			(CASE WHEN ISNULL(opp.numSourceBizDocId,0) > 0 THEN ISNULL((SELECT vcVendorInvoice FROM OpportunityBizDocs WHERE OpportunityBizDocs.numOppBizDocsId=opp.numSourceBizDocId),'') ELSE ISNULL(vcVendorInvoice,'') END) vcVendorInvoice,
			(CASE 
				WHEN ISNULL(opp.numSourceBizDocId,0) > 0
				THEN ISNULL((SELECT vcBizDocID FROM OpportunityBizDocs WHERE OpportunityBizDocs.numOppBizDocsId=Opp.numSourceBizDocId),'')
				ELSE ''
			END) vcPackingSlip
			,ISNULL(numARAccountID,0) numARAccountID
			,(CASE WHEN (SELECT COUNT(*) FROM OpportunityBizDocs OBInner WHERE OBInner.numOppID=@numOppID AND OBInner.numBizDocId=296 AND OBInner.numSourceBizDocId=opp.numOppBizDocsId AND opp.numBizDocId=29397) > 0 THEN 1 ELSE 0 END) AS IsFulfillmentGeneratedOnPickList
			,ISNULL((SELECT COUNT(*) FROM OpportunityBizDocs WHERE numOppID=@numOppID AND ISNULL(numDeferredBizDocID,0) = ISNULL(opp.numOppBizDocsId,0)),0) AS intInvoiceForDiferredBizDoc
			,ISNULL(dbo.CheckOrderInventoryStatus(Mst.numOppID,Mst.numDomainID,2),'') vcInventoryStatus,
			CASE WHEN ISNULL(DM.intDropShip,0)=1 THEN 'Not Available'
				 WHEN ISNULL(DM.intDropShip,0)=2 THEN 'Blank Available'
				 WHEN ISNULL(DM.intDropShip,0)=3 THEN 'Vendor Label'
				 WHEN ISNULL(DM.intDropShip,0)=4 THEN 'Private Label'
				 ELSE '-'
			END AS vcDropShip
			,ISNULL(D.numShippingServiceItemID,0) numShippingServiceItemID
			,ISNULL(D.numDiscountServiceItemID,0) numDiscountServiceItemID 
		FROM   OpportunityBizDocs opp JOIN OpportunityMaster Mst ON Mst.numOppId = opp.numOppId
             JOIN Domain D ON D.numDomainID = Mst.numDomainID
             LEFT OUTER JOIN [Currency] C ON C.numCurrencyID = Mst.numCurrencyID
             LEFT JOIN [DivisionMaster] DM ON DM.numDivisionID = Mst.numDivisionID   
			 LEFT JOIN CompanyInfo CMP ON DM.numCompanyId = CMP.numCompanyId and CMP.numDomainID = @numDomainIDTemp  
             LEFT JOIN dbo.AdditionalContactsInformation ACI ON ACI.numContactID = Mst.numContactID
             LEFT JOIN [BizDocTemplate] BT ON BT.numDomainID = @numDomainIDTemp AND BT.numBizDocTempID=opp.numBizDocTempID
             LEFT JOIN BillingTerms BTR ON BTR.numTermsID = ISNULL(Mst.intBillingDays,0)
             JOIN divisionmaster div1 ON Mst.numDivisionID = div1.numDivisionID
			 JOIN companyinfo [Com1] ON com1.numCompanyID = div1.numCompanyID
			 LEFT JOIN RecurrenceConfiguration ON Opp.numOppBizDocsId = RecurrenceConfiguration.numOppBizDocID
			 LEFT JOIN RecurrenceTransaction ON Opp.numOppBizDocsId = RecurrenceTransaction.numRecurrOppBizDocID
			 OUTER APPLY
			 (
				SELECT TOP 1 
					Com1.vcCompanyName, div1.vcComPhone
                FROM   companyinfo [Com1]
                        JOIN divisionmaster div1
                            ON com1.numCompanyID = div1.numCompanyID
                        JOIN Domain D1
                            ON D1.numDivisionID = div1.numDivisionID
                        JOIN dbo.AddressDetails AD1
							ON AD1.numDomainID = div1.numDomainID AND AD1.numRecordID = div1.numDivisionID AND tintAddressOf = 2 AND tintAddressType = 1 AND bitIsPrimary=1
                WHERE  D1.numDomainID = @numDomainIDTemp
			 ) AS TBLEmployer
      WHERE  opp.numOppBizDocsId = @numOppBizDocsIdTemp
             AND Mst.numDomainID = @numDomainIDTemp
    END
END
GO
/****** Object:  StoredProcedure [dbo].[USP_Projects]    Script Date: 07/26/2008 16:20:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_projects')
DROP PROCEDURE usp_projects
GO
CREATE PROCEDURE [dbo].[USP_Projects]                          
(                          
@numProId numeric(9)=null ,  
@numDomainID as numeric(9),  
@ClientTimeZoneOffset as int         
                         
)                          
as                          
                          
begin                          
                                          
 select  pro.numProId,                          
  pro.vcProjectName,                         
  pro.numintPrjMgr,                          
  pro.numOppId,                    
  pro.intDueDate,                          
  pro.numCustPrjMgr,                          
  pro.numDivisionId,                          
  pro.txtComments,      
  Div.tintCRMType,                          
  div.vcDivisionName,                          
  com.vcCompanyName,
  A.vcFirstname + ' ' + A.vcLastName AS vcContactName,
  isnull(A.vcEmail,'') AS vcEmail,
         isnull(A.numPhone,'') AS Phone,
         isnull(A.numPhoneExtension,'') AS PhoneExtension,
		 		 (SELECT TOP 1 ISNULL(vcData,'') FROM ListDetails WHERE numListID=5 AND numListItemID=com.numCompanyType AND numDomainId=@numDomainID) AS vcCompanyType,
pro.numcontractId,                       
  dbo.fn_GetContactName(pro.numCreatedBy)+' '+convert(varchar(20),dateadd(minute,-@ClientTimeZoneOffset,pro.bintCreatedDate)) as vcCreatedBy ,                          
  dbo.fn_GetContactName(pro.numModifiedBy)+' '+convert(varchar(20),dateadd(minute,-@ClientTimeZoneOffset,pro.bintModifiedDate)) as vcModifiedby,                        
  dbo.fn_GetContactName(pro.numCompletedBy)+' '+convert(varchar(20),dateadd(minute,-@ClientTimeZoneOffset,pro.dtCompletionDate)) as vcFinishedby,                        
  pro.numDomainId,                          
  dbo.fn_GetContactName(pro.numRecOwner) as vcRecOwner,  pro.numRecOwner,   div.numTerID,       
  pro.numAssignedby,pro.numAssignedTo,          
(select  count(*) from dbo.GenericDocuments where numRecID=@numProId and  vcDocumentSection='P') as DocumentCount,
	
 numAccountID,
 '' TimeAndMaterial,
isnull((select sum(isnull(monTimeBudget,0)) from StagePercentageDetails where numProjectID=@numProId),0) TimeBudget,
isnull((select sum(isnull(monExpenseBudget,0)) from StagePercentageDetails where numProjectID=@numProId),0) ExpenseBudget,
isnull((select min(isnull(dtStartDate,getdate()))-1 from StagePercentageDetails where numProjectID=@numProId),getdate()) dtStartDate,
isnull((select max(isnull(dtEndDate,getdate()))+1 from StagePercentageDetails where numProjectID=@numProId),getdate()) dtEndDate,
ISNULL(numProjectType,0) AS numProjectType,
isnull(dbo.fn_GetExpenseDtlsbyProStgID(pro.numProId,0,0),0) as UsedExpense,                        
isnull(dbo.fn_GeTimeDtlsbyProStgID_BT_NBT(pro.numProId,0,1),'Billable Time (0)  Non Billable Time (0)') as UsedTime,
isnull(Pro.numProjectStatus,0) as numProjectStatus,
(SELECT ISNULL(vcStreet,'') + ',<pre>' + ISNULL(vcCity,'') + ',<pre>' +  dbo.[fn_GetState]([numState]) + ',<pre>' + dbo.fn_GetListItemName(numCountry) + ',<pre>' + vcPostalCode 
 FROM dbo.AddressDetails WHERE numDomainID = pro.numDomainID AND numRecordID = pro.numAddressID AND tintAddressOf = 4 AND tintAddressType = 2 AND bitIsPrimary = 1 )as ShippingAddress,
Pro.numBusinessProcessID,
SLP.Slp_Name AS vcProcessName,
pro.dtCompletionDate AS vcProjectedFinish,
NULL AS dtActualStartDate,
pro.dtmEndDate,
pro.dtmStartDate,
ISNULL(pro.numClientBudget,0) AS numClientBudget,
0 AS numClientBudgetBalance,
(CASE 
	WHEN EXISTS (SELECT CInner.numContractId FROM Contracts CInner WHERE CInner.numDomainId=@numDomainID AND CInner.numDivisonId=pro.numDivisionID AND intType=1) 
	THEN 1
	ELSE 0
END) AS bitContractExists,
0 AS numClientBudgetBalance,
(CASE 
	WHEN EXISTS (SELECT CInner.numContractId FROM Contracts CInner WHERE CInner.numDomainId=@numDomainID AND CInner.numDivisonId=pro.numDivisionID AND intType=1) 
	THEN dbo.fn_SecondsConversion(ISNULL((SELECT TOP 1 ((ISNULL(numHours,0) * 60 * 60) + (ISNULL(numMinutes,0) * 60)) - ISNULL((SELECT SUM(CLInner.numUsedTime) FROM ContractsLog CLInner WHERE CLInner.numContractId=C.numContractId),0) FROM Contracts C WHERE C.numDomainId=@numDomainID AND C.numDivisonId=pro.numDivisionID AND intType=1 ORDER BY C.numContractID DESC),0))
	ELSE '' 
END) AS timeLeft,
(CASE 
	WHEN EXISTS (SELECT CInner.numContractId FROM Contracts CInner WHERE CInner.numDomainId=@numDomainID AND CInner.numDivisonId=pro.numDivisionID AND intType=1) 
	THEN ISNULL((SELECT TOP 1 ((ISNULL(numHours,0) * 60 * 60) + (ISNULL(numMinutes,0) * 60)) - ISNULL((SELECT SUM(CLInner.numUsedTime) FROM ContractsLog CLInner WHERE CLInner.numContractId=C.numContractId),0) FROM Contracts C WHERE C.numDomainId=@numDomainID AND C.numDivisonId=pro.numDivisionID AND intType=1 ORDER BY C.numContractID DESC),0)
	ELSE 0
END) AS timeLeftInMinutes
from ProjectsMaster pro                          
  left join DivisionMaster div                          
  on pro.numDivisionID=div.numDivisionID    
  LEFT JOIN AdditionalContactsInformation A ON pro.numCustPrjMgr = A.numContactId
  left join CompanyInfo com                          
  on div.numCompanyID=com.numCompanyId         
  LEFT JOIN dbo.AddressDetails AD ON AD.numAddressID = pro.numAddressID       
		LEFT JOIN Sales_process_List_Master AS SLP ON pro.numBusinessProcessID=SLP.Slp_Id          
  where numProId=@numProId    and pro.numDomainID=@numDomainID                      
                          
end
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ReportDashboard_GetForDataCache')
DROP PROCEDURE dbo.USP_ReportDashboard_GetForDataCache
GO
CREATE PROCEDURE [dbo].[USP_ReportDashboard_GetForDataCache]

AS 
BEGIN
	SELECT 
		ReportDashboard.numDomainID
		,ReportDashboard.numUserCntID
		,ReportDashboard.numDashBoardID
		,ReportDashboard.numReportID
		,ISNULL(ReportListMaster.bitDefault,0) bitDefault
		,ISNULL(ReportListMaster.intDefaultReportID,0) intDefaultReportID
		,ISNULL(ReportListMaster.tintReportType,0) tintReportType
	FROM
		ReportDashboard
	INNER JOIN
		Domain
	ON
		ReportDashboard.numDomainID = Domain.numDomainId
	INNER JOIN
		Subscribers
	ON
		Domain.numDomainId = Subscribers.numTargetDomainID
	INNER JOIN
		ReportListMaster
	ON
		ReportDashboard.numReportID = ReportListMaster.numReportID
	WHERE
		CAST(Subscribers.dtSubEndDate AS DATE) >= CAST(GETUTCDATE() AS dATE)
		AND (DATEPART(HOUR,GETDATE()) >= 0 OR DATEPART(HOUR,GETDATE()) <= 7) -- BETWEEN 0 AM TO 7 AM
		AND (ReportDashboard.dtLastCacheDate IS NULL OR ReportDashboard.dtLastCacheDate <> CAST(GETUTCDATE() AS DATE))
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ReportDashboard_UpdateLastCacheDate')
DROP PROCEDURE dbo.USP_ReportDashboard_UpdateLastCacheDate
GO
CREATE PROCEDURE [dbo].[USP_ReportDashboard_UpdateLastCacheDate]
(
	@numDomainID NUMERIC(18,0)
	,@numDashboardID NUMERIC(18,0)
)
AS 
BEGIN
	UPDATE 
		ReportDashboard 
	SET 
		dtLastCacheDate = GETUTCDATE()
	WHERE 
		numDomainID=@numDomainID 
		AND numDashBoardID = @numDashboardID
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ScheduledReportsGroup_Delete')
DROP PROCEDURE dbo.USP_ScheduledReportsGroup_Delete
GO
CREATE PROCEDURE [dbo].[USP_ScheduledReportsGroup_Delete]
(
	@numDomainID NUMERIC(18,0)
	,@vcRecords NVARCHAR(300)
)
AS 
BEGIN
	IF ISNULL(@vcRecords,'') <> ''
	BEGIN
		DELETE SRGR FROM ScheduledReportsGroup SRG INNER JOIN ScheduledReportsGroupReports SRGR ON SRG.ID = SRGR.numSRGID WHERE SRG.numDomainID=@numDomainID AND SRG.ID IN (SELECT Id FROM dbo.SplitIDs(@vcRecords,','))
		DELETE FROM ScheduledReportsGroup WHERE numDomainID=@numDomainID AND ID IN (SELECT Id FROM dbo.SplitIDs(@vcRecords,','))
	END
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ScheduledReportsGroup_Get')
DROP PROCEDURE dbo.USP_ScheduledReportsGroup_Get
GO
CREATE PROCEDURE [dbo].[USP_ScheduledReportsGroup_Get]
(
	@numDomainID NUMERIC(18,0)
	,@numSRGID NUMERIC(18,0)
	,@ClientTimeZoneOffset INT
)
AS 
BEGIN
	SELECT 
		ID
		,vcName
		,tintFrequency
		,DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,dtStartDate) dtStartDate
		,numEmailTemplate
		,vcSelectedTokens
		,vcRecipientsEmail
	FROM
		ScheduledReportsGroup
	WHERE
		numDomainID=@numDomainID 
		AND ID = @numSRGID
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ScheduledReportsGroup_GetAll')
DROP PROCEDURE dbo.USP_ScheduledReportsGroup_GetAll
GO
CREATE PROCEDURE [dbo].[USP_ScheduledReportsGroup_GetAll]
(
	@numDomainID NUMERIC(18,0)
	,@ClientTimeZoneOffset INT
	,@tintMode TINYINT
)
AS 
BEGIN
	SELECT
		ID
		,ISNULL(vcName,'') vcName
		,CONCAT((CASE tintFrequency WHEN 3 THEN 'Monthly' WHEN 2 THEN 'Weekly' ELSE 'Daily' END),' at ',DATEPART(HOUR, DATEADD(MINUTE,-@ClientTimeZoneOffset,dtStartDate)),SUBSTRING(CONVERT(varchar(20), DATEADD(MINUTE,-@ClientTimeZoneOffset,dtStartDate), 22), 18, 3), ' starting ',DATENAME(WEEKDAY,DATEADD(MINUTE,-@ClientTimeZoneOffset,dtStartDate)),' ',dbo.FormatedDateFromDate(DATEADD(MINUTE,-@ClientTimeZoneOffset,dtStartDate),@numDomainID)) vcSchedule
		,numEmailTemplate
		,ISNULL(GenericDocuments.vcDocName,'') vcEmailTemplate
		,ISNULL(vcRecipientsEmail,'') vcRecipients
	FROM
		ScheduledReportsGroup
	LEFT JOIN
		GenericDocuments
	ON
		ScheduledReportsGroup.numEmailTemplate = GenericDocuments.numGenericDocID
	WHERE
		ScheduledReportsGroup.numDomainID = @numDomainID
		AND 1 = (CASE WHEN @tintMode=1 THEN (CASE WHEN ISNULL((SELECT COUNT(*) FROM ScheduledReportsGroupReports SRGR WHERE SRGR.numSRGID=ScheduledReportsGroup.ID),0) < 5 THEN 1 ELSE 0 END) ELSE 1 END)
	ORDER BY
		vcName
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ScheduledReportsGroup_GetForDataCache')
DROP PROCEDURE dbo.USP_ScheduledReportsGroup_GetForDataCache
GO
CREATE PROCEDURE [dbo].[USP_ScheduledReportsGroup_GetForDataCache]
(
	@numDomainID NUMERIC(18,0)
	,@numSRGID NUMERIC(18,0)
	,@tintMode TINYINT
)
AS 
BEGIN
	IF @tintMode = 1 AND EXISTS (SELECT ID FROM ScheduledReportsGroup WHERE numDomainID=@numDomainID AND ID=@numSRGID)
	BEGIN
		SELECT * FROM ScheduledReportsGroup WHERE numDomainID=@numDomainID AND ID=@numSRGID
	END
	ELSE IF @tintMode = 2
	BEGIN
		SELECT 
			*
		FROM 
			ScheduledReportsGroup 
		WHERE 
			ISNULL(tintDataCacheStatus,0) <> 1 --0: NOT STARTED, 1: STARTED, 2: FINISHED
			AND (DATEPART(HOUR,GETDATE()) >= 0 OR DATEPART(HOUR,GETDATE()) <= 7) -- BETWEEN 0 AM TO 7 AM
			AND 1 = (CASE 
						WHEN dtNextDate IS NULL AND CAST(dtStartDate AS DATE) <= CAST(DATEADD(DAY,1,GETUTCDATE()) AS DATE) THEN 1
						WHEN dtNextDate IS NOT NULL AND CAST(dtNextDate AS DATE) <= CAST(DATEADD(DAY,1,GETUTCDATE()) AS DATE) THEN 1
						ELSE 0
					END)
	END
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ScheduledReportsGroup_GetForEmail')
DROP PROCEDURE dbo.USP_ScheduledReportsGroup_GetForEmail
GO
CREATE PROCEDURE [dbo].[USP_ScheduledReportsGroup_GetForEmail]
(
	@numDomainID NUMERIC(18,0)
	,@numSRGID NUMERIC(18,0)
)
AS 
BEGIN
	SELECT
		ScheduledReportsGroup.numDomainID
		,ScheduledReportsGroup.numCreatedBy
		,ScheduledReportsGroup.ID
		,ScheduledReportsGroup.numEmailTemplate
		,ISNULL(ScheduledReportsGroup.[vcSelectedTokens],'') vcSelectedTokens
		,ISNULL(ScheduledReportsGroup.vcRecipientsEmail,'') vcRecipientsEmail
		,ISNULL(ScheduledReportsGroup.vcReceipientsContactID,'') vcReceipientsContactID
		,ISNULL(ScheduledReportsGroup.tintDataCacheStatus,0) tintDataCacheStatus
		,ISNULL(Domain.vcPSMTPDisplayName,'') AS vcFromName
		,ISNULL(Domain.vcPSMTPUserName,'') AS vcFromEmail
		,ISNULL(Domain.bitPSMTPServer,0) AS bitPSMTPServer
		,ISNULL(Domain.vcLogoForBizTheme,'') vcLogoForBizTheme
		,ISNULL(Domain.vcThemeClass,'') vcThemeClass
	FROM
		ScheduledReportsGroup
	INNER JOIN
		Domain
	ON
		ScheduledReportsGroup.numDomainID = Domain.numDomainId
	WHERE
		1 = (CASE 
				WHEN ISNULL(@numDomainID,0) > 0 AND ISNULL(@numSRGID,0) > 0 
				THEN (CASE WHEN ScheduledReportsGroup.numDomainID=@numDomainID AND ScheduledReportsGroup.ID=@numSRGID THEN 1 ELSE 0 END)
				ELSE (CASE WHEN (dtNextDate <= GETUTCDATE() OR (dtNextDate IS NULL AND dtStartDate <= GETUTCDATE())) AND ISNULL(intNotOfTimeTried,0) < 5 THEN 1 ELSE 0 END)
			END)
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ScheduledReportsGroup_Save')
DROP PROCEDURE dbo.USP_ScheduledReportsGroup_Save
GO
CREATE PROCEDURE [dbo].[USP_ScheduledReportsGroup_Save]
(
	@numDomainID NUMERIC(18,0)
	,@numUserCntID NUMERIC(18,0)
	,@numSRGID NUMERIC(18,0)
	,@vcName NVARCHAR(200)
	,@tintFrequency TINYINT
	,@dtStartDate DATETIME
	,@numEmailTemplate NUMERIC(18,0)
	,@vcSelectedTokens NVARCHAR(MAX)
	,@vcRecipientsEmail NVARCHAR(MAX)
	,@vcReceipientsContactID NVARCHAR(MAX)
	,@ClientTimeZoneOffset INT
)
AS 
BEGIN
	IF ISNULL(@numSRGID,0) > 0
	BEGIN
		DECLARE @dtStartDateExisting DATETIME
		SELECT @dtStartDateExisting=dtStartDate FROM ScheduledReportsGroup WHERE numDomainID = @numDomainID AND ID = @numSRGID

		UPDATE
			ScheduledReportsGroup
		SET
			vcName=@vcName
			,tintFrequency=@tintFrequency
			,dtStartDate=@dtStartDate
			,numEmailTemplate=@numEmailTemplate
			,vcSelectedTokens=@vcSelectedTokens
			,vcRecipientsEmail=@vcRecipientsEmail
			,vcReceipientsContactID=@vcReceipientsContactID
			,dtNextDate=(CASE WHEN @dtStartDateExisting <> @dtStartDate THEN NULL ELSE dtNextDate END)
			,intNotOfTimeTried=(CASE WHEN @dtStartDateExisting <> @dtStartDate THEN 0 ELSE intNotOfTimeTried END)
		WHERE
			numDomainID = @numDomainID
			AND ID = @numSRGID
	END
	ELSE
	BEGIN
		IF (SELECT COUNT(*) FROM ScheduledReportsGroup WHERE numDomainID=@numDomainID) >= 19
		BEGIN
			RAISERROR('MAX_20_REPORT_GROUPS',16,1)
		END
		ELSE
		BEGIN
			INSERT INTO ScheduledReportsGroup
			(
				numDomainID
				,numCreatedBy
				,vcName
				,tintFrequency
				,dtStartDate
				,numEmailTemplate
				,vcSelectedTokens
				,vcRecipientsEmail
				,vcReceipientsContactID
				,intTimeZoneOffset
			)
			VALUES
			(
				@numDomainID
				,@numUserCntID
				,@vcName
				,@tintFrequency 
				,@dtStartDate
				,@numEmailTemplate
				,@vcSelectedTokens
				,@vcRecipientsEmail
				,@vcReceipientsContactID
				,@ClientTimeZoneOffset
			)

			SET @numSRGID = SCOPE_IDENTITY()
		END
	END

	SELECT @numSRGID
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ScheduledReportsGroup_UpdateDataCacheStatus')
DROP PROCEDURE dbo.USP_ScheduledReportsGroup_UpdateDataCacheStatus
GO
CREATE PROCEDURE [dbo].[USP_ScheduledReportsGroup_UpdateDataCacheStatus]
(
	@numDomainID NUMERIC(18,0)
	,@numSRGID NUMERIC(18,0)
	,@tintDataCacheStatus TINYINT
	,@vcExceptionMessage NVARCHAR(MAX)
)
AS 
BEGIN
	UPDATE 
		ScheduledReportsGroup 
	SET 
		tintDataCacheStatus = @tintDataCacheStatus
		,vcExceptionMessage = @vcExceptionMessage
	WHERE 
		numDomainID=@numDomainID AND ID=@numSRGID
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ScheduledReportsGroup_UpdateNextDate')
DROP PROCEDURE dbo.USP_ScheduledReportsGroup_UpdateNextDate
GO
CREATE PROCEDURE [dbo].[USP_ScheduledReportsGroup_UpdateNextDate]
(
	@numDomainID NUMERIC(18,0)
	,@numSRGID NUMERIC(18,0)
)
AS 
BEGIN
	DECLARE @tintFrequency TINYINT
	DECLARE @dtStartDate DATETIME

	SELECT @tintFrequency=tintFrequency,@dtStartDate=dtStartDate FROM ScheduledReportsGroup WHERE numDomainID=@numDomainID AND ID=@numSRGID

	IF ISNULL(@tintFrequency,0) = 1
	BEGIN
		IF @dtStartDate < GETUTCDATE()
		BEGIN
			WHILE CAST(@dtStartDate AS DATE) <= CAST(GETUTCDATE() AS DATE)
			BEGIN
				SET @dtStartDate = DATEADD(DAY,1,@dtStartDate)
			END

			UPDATE ScheduledReportsGroup SET dtNextDate=@dtStartDate,intNotOfTimeTried=0 WHERE numDomainID=@numDomainID AND ID=@numSRGID
		END
		ELSE
		BEGIN
			UPDATE ScheduledReportsGroup SET dtNextDate=DATEADD(DAY,1,dtStartDate),intNotOfTimeTried=0 WHERE numDomainID=@numDomainID AND ID=@numSRGID
		END
	END
	ELSE IF ISNULL(@tintFrequency,0) = 2
	BEGIN
		IF @dtStartDate < GETUTCDATE()
		BEGIN
			WHILE CAST(@dtStartDate AS DATE) <= CAST(GETUTCDATE() AS DATE)
			BEGIN
				SET @dtStartDate = DATEADD(WEEK,1,@dtStartDate)
			END

			UPDATE ScheduledReportsGroup SET dtNextDate=@dtStartDate,intNotOfTimeTried=0 WHERE numDomainID=@numDomainID AND ID=@numSRGID
		END
		ELSE
		BEGIN
			UPDATE ScheduledReportsGroup SET dtNextDate=DATEADD(WEEK,1,dtStartDate),intNotOfTimeTried=0 WHERE numDomainID=@numDomainID AND ID=@numSRGID
		END
	END
	ELSE IF ISNULL(@tintFrequency,0) = 3
	BEGIN
		IF @dtStartDate < GETUTCDATE()
		BEGIN
			WHILE CAST(@dtStartDate AS DATE) <= CAST(GETUTCDATE() AS DATE)
			BEGIN
				SET @dtStartDate = DATEADD(MONTH,1,@dtStartDate)
			END

			UPDATE ScheduledReportsGroup SET dtNextDate=@dtStartDate,intNotOfTimeTried=0 WHERE numDomainID=@numDomainID AND ID=@numSRGID
		END
		ELSE
		BEGIN
			UPDATE ScheduledReportsGroup SET dtNextDate=DATEADD(MONTH,1,dtStartDate),intNotOfTimeTried=0 WHERE numDomainID=@numDomainID AND ID=@numSRGID
		END
	END
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ScheduledReportsGroupLog_Save')
DROP PROCEDURE dbo.USP_ScheduledReportsGroupLog_Save
GO
CREATE PROCEDURE [dbo].[USP_ScheduledReportsGroupLog_Save]
(
	@numSRGID NUMERIC(18,0)
	,@vcException NVARCHAR(MAX)
)
AS 
BEGIN
	UPDATE ScheduledReportsGroup SET intNotOfTimeTried = ISNULL(intNotOfTimeTried,0) + 1 WHERE ID=@numSRGID

	IF ISNULL((SELECT intNotOfTimeTried FROM ScheduledReportsGroup WHERE ID=@numSRGID),0) = 5
	BEGIN
		DECLARE @numDomainID NUMERIC(18,0)
		SELECT @numDomainID=numDomainID FROM ScheduledReportsGroup WHERE ID=@numSRGID
		EXEC USP_ScheduledReportsGroup_UpdateNextDate @numDomainID,@numSRGID
	END

	INSERT INTO ScheduledReportsGroupLog
	(
		numSRGID
		,dtExecutionDate
		,vcException
	)
	VALUES
	(
		@numSRGID
		,GETUTCDATE()
		,@vcException
	)
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ScheduledReportsGroupReports_Delete')
DROP PROCEDURE dbo.USP_ScheduledReportsGroupReports_Delete
GO
CREATE PROCEDURE [dbo].[USP_ScheduledReportsGroupReports_Delete]
(
	@numDomainID NUMERIC(18,0)
	,@numSRGID NUMERIC(18,0)
	,@vcReports VARCHAR(MAX)
)
AS 
BEGIN
	IF ISNULL(@vcReports,'') <> ''
	BEGIN
		DELETE 
			SRGR 
		FROM 
			ScheduledReportsGroup SRG 
		INNER JOIN 
			ScheduledReportsGroupReports SRGR 
		ON 
			SRG.ID = SRGR.numSRGID 
		WHERE 
			SRG.numDomainID = @numDomainID 
			AND numSRGID=@numSRGID
			AND SRGR.ID IN (SELECT Id FROM dbo.SplitIDs(@vcReports,','))
	END
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ScheduledReportsGroupReports_Save')
DROP PROCEDURE dbo.USP_ScheduledReportsGroupReports_Save
GO
CREATE PROCEDURE [dbo].[USP_ScheduledReportsGroupReports_Save]
(
	@numDomainID NUMERIC(18,0)
	,@numSRGID NUMERIC(18,0)
	,@vcReports VARCHAR(MAX)
	,@tintMode TINYINT
)
AS 
BEGIN
	IF @tintMode = 1
	BEGIN
		DECLARE @TEMP TABLE
		(
			tintReportType TINYINT
			,numReportID NUMERIC(18,0)
			,intSortOrder INT
		)

		DECLARE @hDoc as INTEGER
		EXEC sp_xml_preparedocument @hDoc OUTPUT, @vcReports

		INSERT INTO @TEMP
		(
			tintReportType
			,numReportID
			,intSortOrder
		)
		SELECT
			ReportType
			,ReportID
			,SortOrder
		FROM
			OPENXML(@hDoc,'/NewDataSet/Table1',2)                                                                          
		WITH                       
		(
			ReportType TINYINT
			,ReportID NUMERIC(18,0)
			,SortOrder INT
		)

		EXEC sp_xml_removedocument @hDoc

		IF (SELECT COUNT(*) FROM @TEMP) > 5
		BEGIN
			RAISERROR('MAX_5_REPORTS',16,1)
		END
		ELSE
		BEGIN
			DELETE SRGR FROM ScheduledReportsGroup SRG INNER JOIN ScheduledReportsGroupReports SRGR ON SRG.ID = SRGR.numSRGID WHERE SRG.numDomainID = @numDomainID AND numSRGID=@numSRGID

			INSERT INTO ScheduledReportsGroupReports
			(
				numSRGID
				,tintReportType
				,numReportID
				,intSortOrder
			)
			SELECT
				@numSRGID
				,tintReportType
				,numReportID
				,intSortOrder
			FROM
				@TEMP
		END
	END
	ELSE IF @tintMode = 2
	BEGIN
		IF EXISTS (SELECT numReportID FROM ReportListMaster WHERE numDomainID=@numDomainID AND numReportID=CAST(@vcReports AS NUMERIC))
		BEGIN
			IF EXISTS (SELECT ID FROM ScheduledReportsGroupReports WHERE numSRGID=@numSRGID AND tintReportType=1 AND numReportID=CAST(@vcReports AS NUMERIC))
			BEGIN
				RAISERROR('REPORT_ALREADY_ADDED',16,1)
			END
			ELSE
			BEGIN
				IF (SELECT COUNT(*) FROM ScheduledReportsGroupReports WHERE numSRGID=@numSRGID) >= 5
				BEGIN
					RAISERROR('MAX_5_REPORTS',16,1)
				END
				ELSE
				BEGIN
					INSERT INTO ScheduledReportsGroupReports
					(
						numSRGID
						,tintReportType
						,numReportID
						,intSortOrder
					)
					VALUES
					(
						@numSRGID
						,1
						,CAST(@vcReports AS NUMERIC)
						,0
					)
				END
			END
		END
		ELSE
		BEGIN
			RAISERROR('REPORT_DOES_NOT_EXISTS',16,1)
		END
	END
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ScheduledReportsGroupReports_UpdateSortOrder')
DROP PROCEDURE dbo.USP_ScheduledReportsGroupReports_UpdateSortOrder
GO
CREATE PROCEDURE [dbo].[USP_ScheduledReportsGroupReports_UpdateSortOrder]
(
	@numDomainID NUMERIC(18,0)
	,@numSRGID NUMERIC(18,0)
	,@numSRGRID NUMERIC(18,0)
	,@intSortOrder INT
)
AS 
BEGIN
	UPDATE 
		SRGR
	SET 
		intSortOrder = ISNULL(@intSortOrder,0)
	FROM 
		ScheduledReportsGroup SRG 
	INNER JOIN 
		ScheduledReportsGroupReports SRGR 
	ON 
		SRG.ID = SRGR.numSRGID 
	WHERE 
		SRG.numDomainID = @numDomainID 
		AND numSRGID=@numSRGID
		AND SRGR.ID = @numSRGRID
END
GO
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_StagePercentageDetailsTaskTimeLog_Delete')
DROP PROCEDURE dbo.USP_StagePercentageDetailsTaskTimeLog_Delete
GO
CREATE PROCEDURE [dbo].[USP_StagePercentageDetailsTaskTimeLog_Delete]
(
	@numDomainID NUMERIC(18,0)
	,@ID NUMERIC(18,0)
	,@numTaskID NUMERIC(18,0)
)
AS 
BEGIN
	IF @ID = -1
	BEGIN
		DELETE FROM StagePercentageDetailsTaskTimeLog WHERE numDomainID=@numDomainID AND numTaskID=@numTaskID
	END
	ELSE
	BEGIN
		DELETE FROM StagePercentageDetailsTaskTimeLog WHERE numDomainID=@numDomainID AND numTaskID=@numTaskID AND ID=@ID
	END
END
GO
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_StagePercentageDetailsTaskTimeLog_Save')
DROP PROCEDURE dbo.USP_StagePercentageDetailsTaskTimeLog_Save
GO
CREATE PROCEDURE [dbo].[USP_StagePercentageDetailsTaskTimeLog_Save]
(
	@ID NUMERIC(18,0)
	,@numDomainID NUMERIC(18,0)
	,@numUserCntID NUMERIC(18,0)
	,@numTaskID NUMERIC(18,0)
	,@tintAction TINYINT
	,@dtActionTime DATETIME
	,@numProcessedQty FLOAT
	,@numReasonForPause NUMERIC(18,0)
	,@vcNotes VARCHAR(1000)
	,@bitManualEntry BIT
)
AS 
BEGIN

	IF NOT EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numDomainID=@numDomainID AND numTaskID=@numTaskID AND tintAction=4)
	BEGIN
		IF @tintAction=1 AND EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numDomainID=@numDomainID AND numTaskID=@numTaskID AND tintAction=1 AND ID <> ISNULL(@ID,0))
		BEGIN
			RAISERROR('TASK_IS_ALREADY_STARTED',16,1)
		END
		ELSE
		BEGIN
			IF EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE ID=@ID)
			BEGIN
				UPDATE
					StagePercentageDetailsTaskTimeLog
				SET
					tintAction = @tintAction
					,dtActionTime =  DATEADD(MINUTE, DATEDIFF(MINUTE,0,@dtActionTime),0)
					,numProcessedQty=@numProcessedQty
					,vcNotes = @vcNotes
					,bitManualEntry=@bitManualEntry
					,numModifiedBy = @numUserCntID
					,numReasonForPause = @numReasonForPause
					,dtModifiedDate = GETUTCDATE()
				WHERE
					ID = @ID			
			END
			ELSE
			BEGIN
				IF EXISTS (SELECT numTaskId FROM StagePercentageDetailsTask WHERE numDomainID=@numDomainID AND numTaskId=@numTaskID)
				BEGIN
					DECLARE @numTaskAssignee AS NUMERIC(18,0)
					SELECT @numTaskAssignee=numAssignTo FROM StagePercentageDetailsTask WHERE numDomainID=@numDomainID AND numTaskId=@numTaskID

					INSERT INTO StagePercentageDetailsTaskTimeLog
					(
						numDomainID
						,numUserCntID
						,numTaskID
						,tintAction
						,dtActionTime
						,numProcessedQty
						,numReasonForPause
						,vcNotes
						,bitManualEntry
					)
					VALUES
					(
						@numDomainID
						,@numTaskAssignee
						,@numTaskID
						,@tintAction
						,DATEADD(MINUTE, DATEDIFF(MINUTE,0,@dtActionTime),0)
						,@numProcessedQty
						,@numReasonForPause
						,@vcNotes
						,@bitManualEntry
					)
				END
				ELSE
				BEGIN
					RAISERROR('TASK_DOES_NOT_EXISTS',16,1)
				END
			END
		END
	END
	ELSE
	BEGIN
		RAISERROR('TASK_IS_MARKED_AS_FINISHED',16,1)
	END
	DECLARE @numProjectId NUMERIC=0
	SET @numProjectId = ISNULL((SELECT TOP 1 numProjectId FROM StagePercentageDetailsTask where numTaskId=@numTaskID),0)
	IF(@numProjectId>0)
	BEGIN
		DECLARE @dtmActualStartDate AS DATETIME
		DECLARE @dtmActualFinishDate AS DATETIME
		SET @dtmActualStartDate = (SELECT MIN(dtActionTime) FROM StagePercentageDetailsTaskTimeLog where numDomainID=@numDomainID AND numTaskID IN(SELECT numTaskId FROM StagePercentageDetailsTask WHERE numDomainID=@numDomainID AND numProjectId=@numProjectId))
		
		DECLARE @intTotalProgress AS INT =0
		DECLARE @intTotalTaskCount AS INT=0
		DECLARE @intTotalTaskClosed AS INT =0
		SET @intTotalTaskCount=(SELECT COUNT(*) FROM StagePercentageDetailsTask WHERE numProjectId=@numProjectId AND numDomainID=@numDomainID)
		SET @intTotalTaskClosed=(SELECT COUNT(distinct numTaskID) FROM StagePercentageDetailsTaskTimeLog WHERE numTaskId IN (SELECT numTaskId FROM StagePercentageDetailsTask WHERE numProjectId=@numProjectId AND numDomainID=@numDomainID) AND tintAction=4)
		IF(@intTotalTaskCount=@intTotalTaskClosed)
		BEGIN
			SET @dtmActualFinishDate = (SELECT MAX(dtActionTime) FROM StagePercentageDetailsTaskTimeLog where numDomainID=@numDomainID AND numTaskID IN(SELECT numTaskId FROM StagePercentageDetailsTask WHERE numDomainID=@numDomainID AND numProjectId=@numProjectId))
			UPDATE ProjectsMaster SET dtCompletionDate=@dtmActualFinishDate WHERE numProId=@numProjectId
		END
		IF(@intTotalTaskCount>0 AND @intTotalTaskClosed>0)
		BEGIN
			SET @intTotalProgress=ROUND(((@intTotalTaskClosed*100)/@intTotalTaskCount),0)
		END
		IF((SELECT COUNT(*) FROM ProjectProgress WHERE ISNULL(numProId,0)=@numProjectId AND numDomainId=@numDomainID)>0)
		BEGIN
			UPDATE ProjectProgress SET intTotalProgress=@intTotalProgress WHERE ISNULL(numProId,0)=@numProjectId AND numDomainId=@numDomainID
		END
		ELSE
		BEGIN
			INSERT INTO ProjectProgress(
				numProId,
				numOppId,
				numDomainId,
				intTotalProgress,
				numWorkOrderId
			)VALUES(
				@numProjectId,
				NULL,
				@numDomainID,
				@intTotalProgress,
				NULL
			)
		END

		DECLARE @numContractsLogId NUMERIC(18,0)
		DECLARE @numContractID NUMERIC(18,0)
		DECLARE @numUsedTime INT
		DECLARE @balanceContractTime INT

		-- REMOVE EXISTING CONTRACT LOG ENTRY
		IF EXISTS (SELECT 
					CL.numContractId  
				FROM 
					ContractsLog CL 
				WHERE
					CL.numDivisionID = @numDomainID
					AND CL.numReferenceId = @numTaskID
					AND vcDetails='1')
		BEGIN
			SELECT 
				@numContractsLogId=numContractsLogId
				,@numContractID = CL.numContractId
				,@numUsedTime = numUsedTime
			FROM
				ContractsLog CL 
			INNER JOIN
				Contracts C
			ON
				CL.numContractId = C.numContractId
			WHERE
				C.numDomainId = @numDomainID
				AND CL.numReferenceId = @numTaskID
				AND vcDetails='1'

			UPDATE Contracts SET timeUsed=ISNULL(timeUsed,0)-ISNULL(@numUsedTime,0),dtmModifiedOn=GETUTCDATE() WHERE numContractId=@numContractID
			DELETE FROM ContractsLog WHERE numContractsLogId=@numContractsLogId
		END

		IF EXISTS (SELECT 
						C.numContractId 
					FROM 
						ProjectsMaster PM 
					INNER JOIN 
						Contracts C 
					ON 
						PM.numDivisionId=C.numDivisonId 
					WHERE 
						PM.numDomainID = @numDomainID
						AND PM.numProID = @numProjectId
						AND C.numDomainId=@numDomainID
						AND C.numDivisonId=PM.numDivisionId 
						AND C.intType=1)
		BEGIN
			IF EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numDomainID = @numDomainID AND numTaskId = @numTaskID AND tintAction = 4)
			BEGIN
				SELECT TOP 1
					@numContractID =  C.numContractId
				FROM
					ProjectsMaster PM
				INNER JOIN 
					Contracts C 
				ON 
					PM.numDivisionId=C.numDivisonId 
				WHERE 
					PM.numDomainID = @numDomainID
					AND PM.numProID = @numProjectId
					AND C.numDomainId=@numDomainID
					AND C.numDivisonId=PM.numDivisionId 
					AND C.intType=1
				ORDER BY
					C.numContractId DESC

				-- GET UPDATED TIME
				SET @numUsedTime = CAST(dbo.GetTimeSpendOnTaskByProject(@numDomainID,@numTaskID,2) AS INT)

				-- CREATE NEW ENTRY FOR
				UPDATE Contracts SET timeUsed=timeUsed+(ISNULL(@numUsedTime,0) * 60),dtmModifiedOn=GETUTCDATE() WHERE numContractId=@numContractID
				SET @balanceContractTime = (SELECT ISNULL((ISNULL(timeLeft,0)-ISNULL(timeUsed,0)),0) FROM Contracts WHERE numContractId=@numContractID)
				INSERT INTO ContractsLog 
				(
					intType, numDivisionID, numReferenceId, dtmCreatedOn, numCreatedBy,vcDetails,numUsedTime,numBalance,numContractId,tintRecordType
				)
				SELECT 
					1,Contracts.numDivisonId,@numTaskID,GETUTCDATE(),0,1,(ISNULL(@numUsedTime,0) * 60),@balanceContractTime,numContractId,1
				FROM 
					Contracts
				WHERE 
					numContractId = @numContractID
			END
		END
	END
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_WorkOrder_GetWorkOrderDetailByID')
DROP PROCEDURE USP_WorkOrder_GetWorkOrderDetailByID
GO
CREATE PROCEDURE [dbo].[USP_WorkOrder_GetWorkOrderDetailByID]  
	@numDomainID NUMERIC(18,0),
	@numWOID NUMERIC(18,0),
	@ClientTimeZoneOffset INT
AS
BEGIN
	SELECT 
		WO.numWOId,
		Opp.numDivisionId,
		ISNULL(WO.numAssignedTo,0) numRecOwner,
		CONCAT(',',STUFF((
					SELECT 
						CONCAT(',',SPDT.numAssignTo)
					FROM 
						StagePercentageDetailsTask SPDT
					WHERE 
						SPDT.numDomainID = @numDomainID
						AND SPDT.numWorkOrderId=WO.numWOId
					FOR XML PATH, TYPE).value(N'.[1]', N'nvarchar(max)'),1,1,''),',') vcTasksAssignee,
		ISNULL(WO.monAverageCost,0) monAverageCost,
		I.numItemCode,
		ISNULL((SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = I.numItemCode AND bitDefault = 1 AND bitIsImage = 1),'') AS vcPathForTImage,
		ISNULL(WO.numOppId,0) AS numOppId,
		I.vcItemName,
		I.numAssetChartAcntId,
		WO.numQtyItemsReq,
		WO.vcWorkOrderName,
		WO.numBuildProcessId,
		ISNULL(Slp_Name,'') vcProcessName,
		WO.numWOStatus,
		dbo.GetTotalProgress(@numDomainID,WO.numWOId,1,1,'',0) numTotalProgress,
		(CASE WHEN EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID IN (SELECT SPDT.numTaskID FROM StagePercentageDetailsTask SPDT WHERE SPDT.numWorkOrderId=WO.numWOId)) THEN 1 ELSE 0 END) bitTaskStarted,
		CONCAT(dbo.fn_GetContactName(WO.numCreatedby),' ',dbo.FormatedDateTimeFromDate(DATEADD(MINUTE,-@ClientTimeZoneOffset,WO.bintCreatedDate),@numDomainID)) AS CreatedBy,
        CONCAT(dbo.fn_GetContactName(WO.numModifiedBy),' ',dbo.FormatedDateTimeFromDate(DATEADD(MINUTE,-@ClientTimeZoneOffset,WO.bintModifiedDate),@numDomainID)) AS ModifiedBy,
		CONCAT(dbo.fn_GetContactName(WO.numCompletedBy),' ',dbo.FormatedDateTimeFromDate(DATEADD(MINUTE,-@ClientTimeZoneOffset,WO.bintCompletedDate),@numDomainID)) AS CompletedBy,
		ISNULL(A.vcFirstname + ' ' + A.vcLastName,'') AS vcContactName,
         isnull(A.vcEmail,'') AS vcEmail,
         isnull(A.numPhone,'') AS Phone,
         isnull(A.numPhoneExtension,'') AS PhoneExtension,
         CONCAT(C2.vcCompanyName,Case when isnull(D2.numCompanyDiff,0)>0 then  '  ' + dbo.fn_getlistitemname(D2.numCompanyDiff) + ':' + isnull(D2.vcCompanyDiff,'') else '' end) as vcCompanyname,
		ISNULL((SELECT TOP 1 ISNULL(vcData,'') FROM ListDetails WHERE numListID=5 AND numListItemID=C2.numCompanyType AND numDomainId=@numDomainID),'') AS vcCompanyType,
		CONCAT(dbo.fn_GetContactName(WO.numCompletedBy),' ',dbo.FormatedDateTimeFromDate(DATEADD(MINUTE,-@ClientTimeZoneOffset,WO.bintCompletedDate),@numDomainID)) AS CompletedBy,
		CONCAT(ISNULL(WO.numQtyBuilt,0),' (',WO.numQtyItemsReq,')') vcBuiltQty,
		(CASE 
			WHEN EXISTS (SELECT 
						WorkOrderDetails.numWODetailId
					FROM
						WorkOrderDetails
					OUTER APPLY
					(
						SELECT
							SUM(numPickedQty) numPickedQty
						FROM
							WorkOrderPickedItems
						WHERE
							WorkOrderPickedItems.numWODetailId = WorkOrderDetails.numWODetailId
					) TEMP
					WHERE
						WorkOrderDetails.numWOId=@numWOId
						AND ISNULL(numWareHouseItemId,0) > 0
						AND ISNULL(WorkOrderDetails.numQtyItemsReq,0) <> ISNULL(TEMP.numPickedQty,0)) 
			THEN 0 
			ELSE 1 
		END) bitBOMPicked
	FROM
		WorkOrder WO
	LEFT JOIN OpportunityMaster Opp 
	ON
		WO.numOppId=Opp.numoppid
    LEFT JOIN divisionMaster D2
           ON Opp.numDivisionID = D2.numDivisionID
	LEFT JOIN CompanyInfo C2 ON C2.numCompanyID = D2.numCompanyID
	LEFT JOIN AdditionalContactsInformation A ON Opp.numContactId = A.numContactId
	LEFT JOIN
		Sales_process_List_Master SPLFM
	ON
		WO.numBuildProcessId = SPLFM.Slp_Id
	INNER JOIN
		Item I
	ON
		WO.numItemCode = I.numItemCode
	WHERE
		WO.numDomainID=@numDomainID
		AND WO.numWOId = @numWOID

	SELECT
		WOD.numQtyItemsReq AS RequiredQty,
		WOD.monAverageCost,
		I.numItemCode,
		I.vcItemName,
		I.numAssetChartAcntId
	FROM 
		WorkOrderDetails WOD
	INNER JOIN
		Item I
	ON
		WOD.numChildItemID = I.numItemCode
	WHERE
		WOD.numWOId=@numWOID
		AND I.charItemType = 'P'

END

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ContractDetails')
DROP PROCEDURE USP_ContractDetails
GO
CREATE PROCEDURE [dbo].[USP_ContractDetails]
@numContractId AS NUMERIC(18,0),
@numDomainID AS NUMERIC(18,0),
@ClientTimeZoneOffset AS INT
AS
BEGIN
				SELECT 
								C.numContractId,
								C.numIncidents,
								ISNULL(CI.vcCompanyName,'') AS vcCompanyName,
								C.numDivisonId,
								(ISNULL(C.numIncidents,0)  - ISNULL(C.numIncidentsUsed,0)) AS [numIncidentLeft],
								C.[numHours],
								C.[numMinutes],
								dbo.fn_SecondsConversion(((ISNULL(C.numHours,0) * 60 * 60) + (ISNULL(C.numMinutes,0) * 60)) - ISNULL((SELECT SUM(CLInner.numUsedTime) FROM ContractsLog CLInner WHERE CLInner.numContractId=C.numContractId),0)) AS [timeLeft],
								C.[vcItemClassification],
								C.[numWarrantyDays],
								C.[vcNotes],
								C.[vcContractNo],
								C.timeUsed,
								dbo.fn_GetContactName(isnull(C.numCreatedBy,0)) AS CreatedBy,
								dbo.FormatedDateFromDate( DateAdd(MINUTE,-1 * @ClientTimeZoneOffset, C.dtmCreatedOn),@numDomainID) AS CreatedOn,
								dbo.fn_GetContactName(isnull(C.numModifiedBy,0)) AS ModifiedBy,
								ISNULL(dbo.FormatedDateFromDate( DateAdd(MINUTE,-1 * @ClientTimeZoneOffset, C.dtmModifiedOn),@numDomainID),'') AS ModifiedOn
							FROM 
								Contracts AS C
							LEFT JOIN 
								DivisionMaster AS D
							ON
								C.numDivisonId=D.numDivisionID
							LEFT JOIN
								CompanyInfo AS CI
							ON
								D.numCompanyId=CI.numCompanyId
							WHERE
								C.numDomainID=@numDomainID AND
								C.numContractId=@numContractId
END

GO
/****** Object:  StoredProcedure [dbo].[Activity_Add]    Script Date: 07/26/2008 16:14:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*  
**  Inserts a new Activity added by a given Resource.  
**  
**  If no Resource is specified, then the Activity is related to  
**  the Unassigned Resource.  
*/  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ContractsLog')
DROP PROCEDURE USP_ContractsLog
GO
CREATE PROCEDURE [dbo].[USP_ContractsLog] 
	@numDomainID NUMERIC(18,0)
	,@numContractId NUMERIC(18,0)
AS 
BEGIN
	DECLARE @numTotalTime NUMERIC(18,0)
	SET @numTotalTime = ISNULL((SELECT ((ISNULL(numHours,0) * 60 * 60) + (ISNULL(numMinutes,0) * 60)) FROM Contracts WHERE numDomainId=@numDomainID AND numContractId=@numContractId),0)

	DECLARE @TEMP TABLE
	(
		ID INT IDENTITY(1,1)
		,numContractId NUMERIC(18,0)
		,vcType VARCHAR(300)
		,dtmCreatedOn DATETIME
		,numBalance VARCHAR(300)
		,dtStartTime DATETIME
		,dtEndTime DATETIME
	)

	INSERT INTO @TEMP
	(
		numContractId
		,vcType
		,dtmCreatedOn
		,numBalance
		,dtStartTime
		,dtEndTime
	)
	SELECT
		CL.numContractId
		,CASE 
			WHEN C.intType=3 THEN 'Incidents' 
			WHEN C.intType=1 THEN (CASE 
										WHEN ISNULL(CL.tintRecordType,0) = 1 THEN CONCAT('<a href="../projects/frmProjects.aspx?ProId=',SP.numProjectId,'" target="_blank">Project Task</a>')
										WHEN ISNULL(CL.tintRecordType,0) = 2 THEN CONCAT('<a href="../admin/ActionItemDetailsOld.aspx?CommId=',CL.numReferenceId ,'&lngType=0" target="_blank">Communication</a>')
										WHEN ISNULL(CL.tintRecordType,0) = 3 THEN 'Calendar'
										WHEN CP.numCaseId IS NOT NULL THEN CONCAT('<a href="../cases/frmCases.aspx?CaseID=', CL.numReferenceId,'" target="_blank">Case</a>')
										ELSE '-'
									END)
			ELSE '-' 
		END AS vcType,
		CL.dtmCreatedOn,
		dbo.fn_SecondsConversion(CAST((@numTotalTime - ISNULL((SELECT SUM(CLInner.numUsedTime) FROM ContractsLog CLInner WHERE CLInner.numContractId=@numContractId AND CLInner.numContractsLogId <= CL.numContractsLogId),0)) AS VARCHAR)),
		CASE 
			WHEN COM.numCommId > 0 
			THEN COM.dtStartTime 
			WHEN SP.numTaskId > 0 THEN (SELECT TOP 1 dtActionTime FROM StagePercentageDetailsTaskTimeLog where numTaskId=SP.numTaskId AND tintAction=1) 
			ELSE NULL 
		END AS dtStartTime,
		CASE 
			WHEN COM.numCommId > 0 
			THEN COM.dtEndTime
			WHEN SP.numTaskId>0 
			THEN (SELECT TOP 1 dtActionTime FROM StagePercentageDetailsTaskTimeLog where numTaskId=SP.numTaskId AND tintAction=4) 
			ELSE NULL 
		END AS dtEndTime
	FROM
		Contracts C
	INNER JOIN
		ContractsLog CL
	ON
		C.numContractId = CL.numContractId
	LEFT JOIN
		Cases AS CP 
	ON 
		CL.numReferenceId=CP.numCaseId
		AND ISNULL(CL.tintRecordType,0) NOT IN (1,2,3)
	LEFT JOIN 
		Communication AS COM 
	ON 
		CL.numReferenceId=COM.numCommId 
		AND ISNULL(CL.tintRecordType,0)= 2
	LEFT JOIN 
		StagePercentageDetailsTask AS SP 
	ON 
		CL.numReferenceId=SP.numTaskId
		AND ISNULL(CL.tintRecordType,0)= 1
	WHERE
		C.numDomainId=@numDomainID
		AND C.numContractId=@numContractId
	ORDER BY
		CL.numContractsLogId ASC

	SELECT * FROM @TEMP
END
GO
/****** Object:  StoredProcedure [dbo].[USP_CaseDetails]    Script Date: 07/26/2008 16:15:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON                     
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_GetActivityDetailsById')
DROP PROCEDURE usp_GetActivityDetailsById
GO
CREATE PROCEDURE [dbo].[usp_GetActivityDetailsById]  
@ActivityID AS NUMERIC(18,0)=0
AS
BEGIN
	SELECT ac.Duration AS timeInSecounds,ac.ActivityID,ac.StartDateTimeUtc,DATEADD(ss,ac.Duration,ac.StartDateTimeUtc) AS EndDateTime,
	ACI.numContactId,ac.Subject,isnull(dbo.fn_GetContactEmail(1,0,ACI.numContactId),'') AS AssignEmail,ACI.numDivisionId,ISNULL(bitTimeAddedToContract,0) AS bitTimeAddedToContract,
isnull(dbo.fn_GetContactName(ACI.numContactId),'') AS AssignName,
dbo.fn_SecondsConversion(ISNULL((SELECT TOP 1 ISNULL(CAST(timeLeft AS INT),0) FROM Contracts WHERE numDomainId=ACI.numDomainID AND numDivisonId=ACI.numDivisionId AND intType=1),0)) AS timeLeft,
ISNULL((SELECT TOP 1 ISNULL(CAST(timeLeft AS INT),0) FROM Contracts WHERE numDomainId=ACI.numDomainID AND numDivisonId=ACI.numDivisionId AND intType=1),0) AS timeLeftinSec
	From activity ac join             
activityresource ar on  ac.activityid=ar.activityid             
join resource res on ar.resourceid=res.resourceid                         
join AdditionalContactsInformation ACI on ACI.numContactId = res.numUserCntId                
 JOIN DivisionMaster Div                                                          
 ON ACI.numDivisionId = Div.numDivisionID                                   
 JOIN CompanyInfo  Comp                             
 ON Div.numCompanyID = Comp.numCompanyId
WHERE ac.ActivityID=@ActivityID
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetContracts2')
DROP PROCEDURE USP_GetContracts2
GO
CREATE PROCEDURE [dbo].[USP_GetContracts2]    
@numUserCntID NUMERIC(9),
@numDomainID NUMERIC(18,0),
@CurrentPage int,                                                         
@PageSize INT,
@TotRecs INT  OUTPUT,
@ClientTimeZoneOffset  INT,         
@intType INT,      
@vcRegularSearchCriteria varchar(MAX)='',
@vcCustomSearchCriteria varchar(MAX)='',
@SearchText VARCHAR(300) = ''
AS                            
BEGIN

	DECLARE @vcDynamicQuery AS NVARCHAR(MAX)=''
	DECLARE @vcStatusQuery VARCHAR(500) = ''
	IF(LEN(@vcRegularSearchCriteria)>0)
	BEGIN
		SET @vcRegularSearchCriteria = ' AND '+@vcRegularSearchCriteria
		SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'.vcCompanyName','CI.vcCompanyName')
		SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'.vcContractsItemClassificationName','C.vcItemClassification')
	END
	SET @vcDynamicQuery = ' SELECT 
								COUNT(*) OVER() AS TotalRecords,
								C.numContractId,
								C.numIncidents,
								CI.vcCompanyName,
								C.numDivisonId AS numDivisionID,
								C.[numIncidentLeft],
								C.[numHours],
								C.[numMinutes],
								C.[timeLeft],
								C.[vcItemClassification],
								C.[numWarrantyDays],
								C.[vcNotes],
								C.[vcContractNo],
								C.timeUsed,
								(select
								   distinct  
									stuff((
										select '','' + vcData
										from ListDetails
										where numListID=36
										AND numListItemID IN (
								SELECT Items FROM dbo.Split(C.[vcItemClassification],'','') WHERE Items<>'''')
										order by vcData
										for xml path('''')
									),1,1,'''') as userlist
								from ListDetails where numListID=36
								AND numListItemID IN (
								SELECT Items FROM dbo.Split(C.[vcItemClassification],'','') WHERE Items<>'''')
								group by vcData) AS vcContractsItemClassificationName,
								dbo.fn_GetContactName(isnull(C.numCreatedBy,0)) + dbo.FormatedDateFromDate(DateAdd(MINUTE,-1 * ' +convert(varchar(15),@ClientTimeZoneOffset)+', C.dtmCreatedOn),'''+CAST(@numDomainID AS VARCHAR(MAX))+''') AS CreatedByOn,
								dbo.fn_GetContactName(isnull(C.numModifiedBy,0)) + dbo.FormatedDateFromDate( DateAdd(MINUTE,-1 * ' +convert(varchar(15),@ClientTimeZoneOffset)+', C.dtmModifiedOn),'''+CAST(@numDomainID AS VARCHAR(MAX))+''') AS ModifiedByOn,
								CONCAT(''<a href="javascript:void(0)" onclick="openContractPopup(''+CAST(C.numContractId AS VARCHAR)+'',''+CAST(C.intType AS VARCHAR)+'')">'',ISNULL(C.vcContractNo,''-''),''</a>'') AS [vcContractNo~1~0],
								dbo.FormatedDateFromDate(DateAdd(MINUTE,-1 * ' +convert(varchar(15),@ClientTimeZoneOffset)+', C.dtmCreatedOn) ,'''+CAST(@numDomainID AS VARCHAR(MAX))+''')+'',''+dbo.fn_GetContactName(isnull(C.numCreatedBy,0)) AS [CreatedByOn~2~0],
								dbo.FormatedDateFromDate( DateAdd(MINUTE,-1 * ' +convert(varchar(15),@ClientTimeZoneOffset)+', C.dtmModifiedOn),'''+CAST(@numDomainID AS VARCHAR(MAX))+''')+'',''+dbo.fn_GetContactName(isnull(C.numModifiedBy,0)) AS [ModifiedByOn~3~0],
								CI.vcCompanyName AS [vcCompanyName~4~0],
								D.tintCRMType,
								(select
								   distinct  
									stuff((
										select '','' + vcData
										from ListDetails
										where numListID=36
										AND numListItemID IN (
								SELECT Items FROM dbo.Split(C.[vcItemClassification],'','') WHERE Items<>'''')
										order by vcData
										for xml path('''')
									),1,1,'''') as userlist
								from ListDetails where numListID=36
								AND numListItemID IN (
								SELECT Items FROM dbo.Split(C.[vcItemClassification],'','') WHERE Items<>'''')
								group by vcData) AS [vcContractsItemClassificationName~5~0],
								
								(ISNULL(C.numIncidents,0)  - ISNULL(C.numIncidentsUsed,0)) AS [numIncidentLeft~6~0],
								dbo.fn_SecondsConversion(((ISNULL(C.numHours,0) * 60 * 60) + (ISNULL(C.numMinutes,0) * 60)) - ISNULL((SELECT SUM(CLInner.numUsedTime) FROM ContractsLog CLInner WHERE CLInner.numContractId=C.numContractId),0)) AS [timeLeft~7~0],
								C.numWarrantyDays AS [numWarrantyDays~8~0],
								C.numCreatedBy AS numRecOwner,
								D.numTerID
							FROM 
								Contracts AS C
							LEFT JOIN 
								DivisionMaster AS D
							ON
								C.numDivisonId=D.numDivisionID
							LEFT JOIN
								CompanyInfo AS CI
							ON
								D.numCompanyId=CI.numCompanyId
							WHERE
								C.numDomainID='''+CAST(@numDomainID AS VARCHAR(MAX))+''' 
								AND C.intType='+CAST(@intType AS VARCHAR(MAX))+' '+@vcRegularSearchCriteria+' ORDER BY C.dtmCreatedOn desc OFFSET '+CAST(((@CurrentPage - 1) * @PageSize) AS VARCHAR(500))+' ROWS FETCH NEXT '+CAST(@PageSize AS VARCHAR(500))+' ROWS ONLY '

PRINT CAST(@vcDynamicQuery AS NTEXT)
EXEC(@vcDynamicQuery)

	DECLARE @TempColumns TABLE
	(
        vcFieldName VARCHAR(200)
        ,vcDbColumnName VARCHAR(200)
        ,vcOrigDbColumnName VARCHAR(200)
        ,bitAllowSorting BIT
        ,numFieldId INT
        ,bitAllowEdit BIT
        ,bitCustomField BIT
        ,vcAssociatedControlType VARCHAR(50)
        ,vcListItemType  VARCHAR(200)
        ,numListID INT
        ,ListRelID INT
        ,vcLookBackTableName VARCHAR(200)
        ,bitAllowFiltering BIT
        ,vcFieldDataType  VARCHAR(20)
		,intColumnWidth INT
		,bitClosedColumn BIT
	)
INSERT INTO @TempColumns
		(
			vcFieldName
			,vcDbColumnName
			,vcOrigDbColumnName
			,bitAllowSorting
			,numFieldId
			,bitAllowEdit
			,bitCustomField
			,vcAssociatedControlType
			,vcListItemType
			,numListID
			,ListRelID
			,vcLookBackTableName
			,bitAllowFiltering
			,vcFieldDataType
			,bitClosedColumn
		)
		VALUES
		('Contract ID','vcContractNo','vcContractNo',0,1,0,0,'TextBox','',0,0,'Contracts',0,'V',0)
		,('Create on, by','CreatedByOn','CreatedByOn',0,2,0,0,'TextBox','',0,0,'Contracts',0,'V',0)
		,('Modified on, by','ModifiedByOn','ModifiedByOn',0,3,0,0,'TextBox','',0,0,'Contracts',0,'V',0)
		,('Organizations','vcCompanyName','vcCompanyName',0,4,0,0,'TextBox','',0,0,'Contracts',1,'V',0)
		,('Item Classifications','vcContractsItemClassificationName','vcContractsItemClassificationName',0,5,0,0,'SelectBox','',0,0,'Contracts',1,'V',0)
		,('Incidents Left','numIncidentLeft','numIncidentLeft',0,6,0,0,'TextBox','',0,0,'Contracts',0,'V',0)
		,('Time Left','timeLeft','timeLeft',0,7,0,0,'TextBox','',0,0,'Contracts',0,'V',0)
		,('Days Left','numWarrantyDays','numWarrantyDays',0,8,0,0,'TextBox','',0,0,'Contracts',0,'V',0)

	UPDATE
		TC
	SET
		TC.intColumnWidth = ISNULL(DFCD.intColumnWidth,0)
	FROM
		@TempColumns TC
	INNER JOIN
		DycFormConfigurationDetails DFCD
	ON
		TC.numFieldId = DFCD.numFieldId
	WHERE
		DFCD.numDomainId = @numDomainID
		AND DFCD.numUserCntID = @numUserCntID
		AND DFCD.numFormId = 146
		AND DFCD.tintPageType = 1

		SELECT * FROM @TempColumns
	--IF @numWOStatus <> 1 AND EXISTS (SELECT numFieldId FROM DycFormConfigurationDetails WHERE numDomainId = @numDomainID AND numUserCntID = @numUserCntID AND numFormId = 145 AND tintPageType = 1)
	--BEGIN
	--	SELECT 
	--		TC.*
	--	FROM 
	--		DycFormConfigurationDetails DFCD 
	--	INNER JOIN 
	--		@TempColumns TC 
	--	ON 
	--		DFCD.numFieldId=TC.numFieldId
	--	WHERE 
	--		numDomainId = @numDomainID 
	--		AND numUserCntID = @numUserCntID 
	--		AND numFormId = 145 
	--		AND tintPageType = 1
	--	ORDER BY
	--		DFCD.intRowNum
	--END
	--ELSE
	--BEGIN
	--	SELECT * FROM @TempColumns
	--END
END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_TicklerActItemsV2')
DROP PROCEDURE USP_TicklerActItemsV2
GO
CREATE Proc [dbo].[USP_TicklerActItemsV2]       
@numUserCntID as numeric(9)=null,                                                                        
@numDomainID as numeric(9)=null,                                                                        
@startDate as datetime,                                                                        
@endDate as datetime,                                                                    
@ClientTimeZoneOffset Int,  --Added by Debasish to enable calculation of date according to client machine             
@bitFilterRecord bit=0,
@columnName varchar(50), 
@columnSortOrder varchar(50),
@vcBProcessValue varchar(500),
@RegularSearchCriteria VARCHAR(MAX)='',
@CustomSearchCriteria VARCHAR(MAX)='', 
@OppStatus int,
@CurrentPage int,                                                              
@PageSize int
AS
BEGIN
	SET @endDate = DATEADD(MILLISECOND,-2,DATEADD(DAY,1,@endDate))

--SET @PageSize=10
DECLARE @tintActionItemsViewRights TINYINT  = 3
DECLARE @bitREQPOApproval AS BIT
DECLARE @bitARInvoiceDue AS BIT
DECLARE @bitAPBillsDue AS BIT
DECLARE @vchARInvoiceDue AS VARCHAR(500)
DECLARE @vchAPBillsDue AS VARCHAR(500)
DECLARE @decReqPOMinValue AS DECIMAL(18,2)
DECLARE @vchREQPOApprovalEmp AS VARCHAR(500)=''
DECLARE @numDomainDivisionID NUMERIC(18,0)
DECLARE @dynamicPartQuery AS NVARCHAR(MAX)=''
DECLARE @ActivityRegularSearch AS VARCHAR(MAX)=''
DECLARE @OtherRegularSearch AS VARCHAR(MAX)=''
DECLARE @IndivRecordPaging AS NVARCHAR(MAX)=''
--SET @IndivRecordPaging=' '
DECLARE @TotalRecordCount INT =0
SET @IndivRecordPaging=  ' ORDER BY dtDueDate DESC OFFSET '+CAST(((@CurrentPage - 1 ) * @PageSize) AS VARCHAR) +' ROWS FETCH NEXT '+CAST(@PageSize AS VARCHAR)+' ROWS ONLY'

SELECT TOP 1 
	@bitREQPOApproval=bitREQPOApproval,
	@vchREQPOApprovalEmp=vchREQPOApprovalEmp,
	@decReqPOMinValue=ISNULL(decReqPOMinValue,0),
	@bitARInvoiceDue=bitARInvoiceDue,
	@bitAPBillsDue = bitAPBillsDue,
	@vchARInvoiceDue = vchARInvoiceDue,
	@vchAPBillsDue = vchAPBillsDue,
	@numDomainDivisionID=numDivisionID
FROM 
	Domain 
WHERE 
	numDomainId=@numDomainID

IF EXISTS (SELECT * FROM GroupAuthorization WHERE numDomainID=@numDomainID AND numModuleID=1 AND numPageID=2 AND numGroupID IN (SELECT ISNULL(numGroupID,0) FROM UserMaster WHERE numDomainID=@numDomainID AND numUserDetailId=@numUserCntID))
BEGIN
	SELECT @tintActionItemsViewRights=ISNULL(intViewAllowed,0) FROM GroupAuthorization WHERE numDomainID=@numDomainID AND numModuleID=1 AND numPageID=2 AND numGroupID IN (SELECT ISNULL(numGroupID,0) FROM UserMaster WHERE numDomainID=@numDomainID AND numUserDetailId=@numUserCntID)
END
--12 - Communication task
--1 - Case Task
--2 - Project Task
--3 - Opportunity Task
--4 - Sales Order Task
--5 - Work Order Management
--6 - Work Center Task
--7 - Requisition Approval
--8 - PO Approval
--9 - Price Margin Approval
--10 - A/R Invoice Due
--11 - A/P Bill Due

DECLARE @dynamicQuery AS NVARCHAR(MAX)=''
DECLARE @dynamicQueryCount AS NVARCHAR(MAX)=''

Create table #tempRecords (RecordCount NUMERIC(9),RecordId NUMERIC(9),numTaskId numeric(9),IsTaskClosed bit,TaskTypeName VARCHAR(100),TaskType NUMERIC(9),
OrigDescription NVARCHAR(MAX),Description NVARCHAR(MAX),TotalProgress INT,numAssignToId NUMERIC(9),
Priority NVARCHAR(MAX),PriorityId  NUMERIC(9),ActivityId NUMERIC(9),OrgName NVARCHAR(MAX),CompanyRating NVARCHAR(MAX),
numContactID numeric(9),numDivisionID numeric(9),tintCRMType tinyint,
Id numeric(9),bitTask varchar(100),Startdate datetime,EndTime datetime,
itemDesc text,[Name] varchar(200),vcFirstName varchar(100),vcLastName varchar(100),numPhone varchar(15),numPhoneExtension varchar(7),
[Phone] varchar(30),vcCompanyName varchar(100),vcProfile VARCHAR(100), vcEmail varchar(50),Task varchar(100),Activity varchar(MAX),
Status varchar(100),numCreatedBy numeric(9),numRecOwner VARCHAR(500), numModifiedBy VARCHAR(500), numOrgTerId NUMERIC(18,0), numTerId VARCHAR(200),numAssignedTo varchar(1000),numAssignedBy varchar(50),
 caseid numeric(9),vcCasenumber varchar(50),casetimeId numeric(9),caseExpId numeric(9),type int,itemid varchar(50),
changekey varchar(50),dtDueDate DATETIME, DueDate varchar(500),bitFollowUpAnyTime bit,numNoOfEmployeesId varchar(50),vcComPhone varchar(50),numFollowUpStatus varchar(200),dtLastFollowUp varchar(500),vcLastSalesOrderDate VARCHAR(100),monDealAmount DECIMAL(20,5), vcPerformance VARCHAR(100),vcColorScheme VARCHAR(50),
Slp_Name varchar(100),intTotalProgress INT,vcPoppName varchar(100), numOppId NUMERIC(18,0),numBusinessProcessID numeric(18,0),
numCompanyRating varchar(500),numStatusID varchar(500),numCampaignID varchar(500),[Action-Item Participants] NVARCHAR(MAX),HtmlLink varchar(500),
Location nvarchar(150),OrignalDescription text)                                                         
 

declare @strSql as varchar(MAX)                                                            
declare @strSql1 as varchar(MAX)                         
declare @strSql2 as varchar(MAX)                                                            
declare @vcCustomColumnName AS VARCHAR(8000)
declare @vcnullCustomColumnName AS VARCHAR(8000)
   -------Change Row Color-------
Declare @vcCSOrigDbCOlumnName as varchar(50),@vcCSLookBackTableName as varchar(50),@vcCSAssociatedControlType as varchar(50)
SET @vcCSOrigDbCOlumnName=''
SET @vcCSLookBackTableName=''

declare @tintOrder as tinyint                                                
declare @vcFieldName as varchar(50)                                                
declare @vcListItemType as varchar(3)                                           
declare @vcAssociatedControlType varchar(20)                                                
declare @numListID AS numeric(9)                                                
declare @vcDbColumnName varchar(20)                    
declare @WhereCondition varchar(2000)                     
declare @vcLookBackTableName varchar(2000)              
Declare @bitCustomField as BIT
DECLARE @bitAllowEdit AS CHAR(1)                 
Declare @numFieldId as numeric  
DECLARE @bitAllowSorting AS CHAR(1)   
declare @vcColumnName AS VARCHAR(500)       
declare @vcCustomColumnNameOnly AS VARCHAR(500)       


IF(@RegularSearchCriteria='')
BEGIN
	SET @RegularSearchCriteria =@RegularSearchCriteria + ' 1=1'
END
if(@OppStatus=0)
BEGIN
SET @RegularSearchCriteria =@RegularSearchCriteria + ' AND IsTaskClosed = 0'
END
if(@OppStatus=1)
BEGIN
SET @RegularSearchCriteria = @RegularSearchCriteria + ' AND IsTaskClosed = 1'
END
 SET @RegularSearchCriteria  = REPLACE(@RegularSearchCriteria,'TaskTypeName','TaskType')

 SET @ActivityRegularSearch = ''
 SET @OtherRegularSearch = ''

 SET @ActivityRegularSearch = ISNULL((SELECT STUFF( 
(
	SELECT  'AND ' + [Items] FROM 
	(
	  SELECT [Items] FROM SplitByString(@RegularSearchCriteria,'AND') WHERE Items<>'' AND 1=
	  (CASE WHEN Items LIKE '%IsTaskClosed%' THEN 0
	 WHEN Items LIKE '%TaskType%'  THEN 0
	 WHEN  Items LIKE '%numAssignToId%'  THEN 0
	 WHEN  Items LIKE '%PriorityId%'  THEN 0
	 WHEN  Items LIKE '%ActivityId%'  THEN 0
	 WHEN  Items LIKE '%Description%'   THEN 0
	 WHEN  Items LIKE '%OrgName%'   THEN 0 ELSE 1 END)
	) AS T FOR XML PATH('')
)
,1,3,'') AS [Items]),'')
SET @ActivityRegularSearch = REPLACE(@ActivityRegularSearch,'&gt;','>')
SET @ActivityRegularSearch = REPLACE(@ActivityRegularSearch,'&lt;','<')
 SET @OtherRegularSearch = ISNULL((SELECT STUFF( 
(
	SELECT  'AND ' + [Items] FROM 
	(
	  SELECT [Items] FROM SplitByString(@RegularSearchCriteria,'AND') WHERE Items<>'' AND 1=
	  (CASE WHEN Items LIKE '%IsTaskClosed%' THEN 1
	 WHEN Items LIKE '%TaskType%'  THEN 1
	 WHEN  Items LIKE '%numAssignToId%'  THEN 1
	 WHEN  Items LIKE '%PriorityId%'  THEN 1
	 WHEN  Items LIKE '%ActivityId%'  THEN 1
	 WHEN  Items LIKE '%Description%'   THEN 1
	 WHEN  Items LIKE '%OrgName%'   THEN 1 ELSE 0 END)
	) AS T FOR XML PATH('')
)
,1,3,'') AS [Items]),'')
SET @OtherRegularSearch = ISNULL(REPLACE(@OtherRegularSearch,'T.',''),'')
SET @OtherRegularSearch = REPLACE(@OtherRegularSearch,'&gt;','>')
SET @OtherRegularSearch = REPLACE(@OtherRegularSearch,'&lt;','<')
IF(LEN(@OtherRegularSearch)=0)
BEGIN
	SET @OtherRegularSearch = ' 1=1 '
END
PRINT @OtherRegularSearch
Declare @ListRelID as numeric(9)             
set @tintOrder=0   

CREATE TABLE #tempForm 
	(
		tintOrder INT,vcDbColumnName nvarchar(50),vcFieldDataType nvarchar(50),vcOrigDbColumnName nvarchar(50),bitAllowFiltering BIT,vcFieldName nvarchar(50)
		,vcAssociatedControlType nvarchar(50),vcListItemType char(3),numListID numeric(9),vcLookBackTableName varchar(50),bitCustomField BIT,numFieldId NUMERIC
		,bitAllowSorting BIT,bitAllowEdit BIT,bitIsRequired BIT,bitIsEmail BIT,bitIsAlphaNumeric BIT,bitIsNumeric BIT,bitIsLengthValidation BIT,intMaxLength int
		,intMinLength int,bitFieldMessage BIT,vcFieldMessage VARCHAR(500),ListRelID numeric(9),intColumnWidth NUMERIC(18,2)
	)
                
	--Custom Fields related to Organization
	INSERT INTO 
		#tempForm
	SELECT  
		tintRow + 1 AS tintOrder,vcDbColumnName,'',vcDbColumnName,1,vcFieldName,vcAssociatedControlType,'' as vcListItemType,numListID,'' as vcLookBackTableName                                                 
		,bitCustom,numFieldId,bitAllowSorting,0,bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength
		,bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth
	FROM 
		View_DynamicCustomColumns
	WHERE 
		numFormId=43 
		AND numUserCntID=@numUserCntID 
		AND numDomainID=@numDomainID 
		AND tintPageType=1
		AND ISNULL(bitCustom,0)=1
 
	SELECT TOP 1 
		@tintOrder = tintOrder + 1
		,@vcDbColumnName=vcDbColumnName
		,@vcFieldName=vcFieldName
		,@vcAssociatedControlType=vcAssociatedControlType
		,@vcListItemType=vcListItemType
		,@numListID=numListID
		,@vcLookBackTableName=vcLookBackTableName                                               
		,@bitCustomField=bitCustomField
		,@numFieldId=numFieldId
		,@bitAllowSorting=bitAllowSorting
		,@bitAllowEdit=0
		,@ListRelID=ListRelID
	FROM 
		#tempForm 
	ORDER BY 
		tintOrder ASC     
  
	SET @vcCustomColumnName=''
	SET @vcnullCustomColumnName=''
	SET @vcCustomColumnNameOnly = ''
	WHILE @tintOrder>0      
	BEGIN  
		SET @vcColumnName= @vcDbColumnName
		SET @strSql = 'ALTER TABLE #tempRecords ADD [' + @vcColumnName + '] NVARCHAR(MAX)'
		SET @vcCustomColumnNameOnly = @vcCustomColumnNameOnly+','+@vcColumnName
		EXEC(@strSql)
		
		IF @bitCustomField = 1
		BEGIN 
			IF @vcAssociatedControlType = 'TextBox' OR @vcAssociatedControlType = 'TextArea'            
			BEGIN            
				SET @vcCustomColumnName= @vcCustomColumnName+', ISNULL((SELECT TOP 1 CFW.Fld_Value FROM CFW_FLD_Values AS CFW WHERE CFW.Fld_ID='+CAST(@numFieldId AS VARCHAR(50))+' AND CFW.RecId=Div.numDivisionID),'''') ['+ @vcColumnName +']'
				SET @vcnullCustomColumnName= @vcnullCustomColumnName+','''' AS  ['+ @vcColumnName +']'
			END  
			ELSE IF @vcAssociatedControlType = 'CheckBox'         
			BEGIN            
				SET @vcCustomColumnName= @vcCustomColumnName+',ISNULL((SELECT TOP 1 CFW.Fld_Value FROM CFW_FLD_Values AS CFW WHERE CFW.Fld_ID='+CAST(@numFieldId AS VARCHAR(50))+' AND CFW.RecId=Div.numDivisionID),0)  ['+ @vcColumnName + ']'
				SET @vcnullCustomColumnName= @vcnullCustomColumnName+',0 AS ['+ @vcColumnName + ']'
			END            
			ELSE IF @vcAssociatedControlType = 'DateField'        
			BEGIN            
				SET @vcCustomColumnName= @vcCustomColumnName+',dbo.FormatedDateFromDate((SELECT TOP 1 CFW.Fld_Value FROM CFW_FLD_Values AS CFW WHERE CFW.Fld_ID='+CAST(@numFieldId AS VARCHAR(50))+' AND CFW.RecId=Div.numDivisionID),'+convert(varchar(10),@numDomainId)+')  ['+ @vcColumnName +']'              
				SET @vcnullCustomColumnName= @vcnullCustomColumnName+','''' AS  ['+ @vcColumnName +']'
			END            
			ELSE IF @vcAssociatedControlType = 'SelectBox'           
			BEGIN            
				SET @vcCustomColumnName=@vcCustomColumnName+',(SELECT TOP 1 ISNULL(L.vcData,'''') FROM CFW_FLD_Values CFW left Join ListDetails L on L.numListItemID=CFW.Fld_Value WHERE  CFW.Fld_ID='+CAST(@numFieldId AS VARCHAR(50))+' AND CFW.RecId=Div.numDivisionID)'+' ['+ @vcColumnName +']'                                                         
				SET @vcnullCustomColumnName=@vcnullCustomColumnName+','''''+'AS  ['+ @vcColumnName +']'                                                         
			END
			ELSE IF @vcAssociatedControlType = 'CheckBoxList'
			BEGIN
				SET @vcCustomColumnName=@vcCustomColumnName+',(SELECT STUFF((SELECT CONCAT('', '', vcData) FROM ListDetails WHERE numListID= ' + CAST(@numListID AS varchar) + ' AND numlistitemid IN (SELECT ISNULL(Id,0) FROM dbo.SplitIDs(ISNULL((SELECT CFWInner.Fld_Value FROM CFW_FLD_Values CFWInner WHERE CFWInner.Fld_ID='+CAST(@numFieldId AS VARCHAR(50))+' AND CFWInner.RecId=Div.numDivisionID),''''),'','')) FOR XML PATH('''')), 1, 1, ''''))'+' ['+ @vcColumnName +']'
				SET @vcnullCustomColumnName=@vcnullCustomColumnName+','''''+'AS  ['+ @vcColumnName +']'
			END
			ELSE
			BEGIN
				SET @vcCustomColumnName= CONCAT(@vcCustomColumnName,',(SELECT dbo.fn_GetCustFldStringValue(',@numFieldId,',CFW.RecId,CFW.Fld_Value',') FROM CFW_FLD_Values CFW left Join ListDetails L on L.numListItemID=CFW.Fld_Value WHERE  CFW.Fld_ID=',@numFieldId,' AND CFW.RecId=Div.numDivisionID)',' [', @vcColumnName ,']')
				SET @vcnullCustomColumnName=@vcnullCustomColumnName+','''''+'AS  ['+ @vcColumnName +']' 
			END
		END
		  
		SELECT TOP 1 
			@tintOrder=tintOrder + 1
			,@vcDbColumnName=vcDbColumnName
			,@vcFieldName=vcFieldName
			,@vcAssociatedControlType=vcAssociatedControlType
			,@vcListItemType=vcListItemType
			,@numListID=numListID
			,@vcLookBackTableName=vcLookBackTableName                                               
			,@bitCustomField=bitCustomField
			,@numFieldId=numFieldId
			,@bitAllowSorting=bitAllowSorting
			,@bitAllowEdit=0
			,@ListRelID=ListRelID
		FROM 
			#tempForm 
		WHERE 
			tintOrder > @tintOrder-1 
		ORDER BY 
			tintOrder asc            
 
		IF @@rowcount=0 set @tintOrder=0 
	END
SET @strSql=''

Create table #tempColorScheme(vcOrigDbCOlumnName varchar(50),vcLookBackTableName varchar(50),vcFieldValue varchar(50),vcFieldValue1 varchar(50),vcColorScheme varchar(50),vcAssociatedControlType varchar(50))

insert into #tempColorScheme select DFM.vcOrigDbCOlumnName,DFM.vcLookBackTableName,DFCS.vcFieldValue,DFCS.vcFieldValue1,DFCS.vcColorScheme,DFFM.vcAssociatedControlType 
from DycFieldColorScheme DFCS join DycFormField_Mapping DFFM on DFCS.numFieldID=DFFM.numFieldID
join DycFieldMaster DFM on DFM.numModuleID=DFFM.numModuleID and DFM.numFieldID=DFFM.numFieldID
where DFCS.numDomainID=@numDomainID and DFFM.numFormID=43 AND DFCS.numFormID=43 and isnull(DFFM.bitAllowGridColor,0)=1

IF(SELECT COUNT(*) FROM #tempColorScheme)>0
BEGIN
   select top 1 @vcCSOrigDbCOlumnName=vcOrigDbCOlumnName,@vcCSLookBackTableName=vcLookBackTableName,@vcCSAssociatedControlType=vcAssociatedControlType from #tempColorScheme
END   
-------------------- 0 - TASK----------------------------

IF(LEN(@CustomSearchCriteria)>0)
BEGIN
SET @CustomSearchCriteria = ' AND '+@CustomSearchCriteria
END
IF(LEN(@ActivityRegularSearch)=0)
BEGIN
	SET @ActivityRegularSearch = '1=1'
END
DECLARE @StaticColumns AS NVARCHAR(MAX)=''

SET @StaticColumns = ' RecordCount,RecordId,
numCreatedBy,
numOrgTerId,
numTaskId,
IsTaskClosed,
TaskTypeName,
TaskType,
OrigDescription,
Description,
dtDueDate,
DueDate,
TotalProgress,
numAssignedTo,
numAssignToId,
Priority,
Activity,
PriorityId,
ActivityId,
OrgName,
CompanyRating,
numContactID,numDivisionID,tintCRMType,Id,bitTask,Startdate,EndTime,itemDesc,
[Name],vcFirstName,vcLastName,numPhone,numPhoneExtension,[Phone],vcCompanyName,vcProfile,vcEmail,Task,Status,numRecOwner,numModifiedBy,numTerId,numAssignedBy,caseid,vcCasenumber,
casetimeId,caseExpId,type,itemid,bitFollowUpAnyTime,numNoOfEmployeesId,vcComPhone,numFollowUpStatus,dtLastFollowUp,vcLastSalesOrderDate,monDealAmount,vcPerformance'
SET @dynamicQuery = ' INSERT INTO #tempRecords(
 '+@StaticColumns +@vcCustomColumnNameOnly+'
) SELECT COUNT(*) OVER() RecordCount,* FROM (SELECT 
Comm.numCommId AS RecordId,
Comm.numCreatedBy AS numCreatedBy, 
ISNULL(Div.numTerId,0) AS numOrgTerId,
-2 AS numTaskId,
bitClosedFlag AS IsTaskClosed,
''<a onclick="openCommTask(''+CAST(Comm.numCommId AS VARCHAR)+'',0)" href="javascript:void(0)">''+dbo.GetListIemName(Comm.bitTask)+''</a>'' As TaskTypeName,
Comm.bitTask AS TaskType,
'''' AS OrigDescription,
comm.textDetails AS Description,
DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', dtStartTime) AS dtDueDate,
DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', dtStartTime) as DueDate,
''-'' As TotalProgress,
dbo.fn_GetContactName(numAssign) As numAssignedTo,
numAssign As numAssignToId,
listdetailsStatus.VcData As Priority, -- Priority column   
 listdetailsActivity.VcData As Activity ,
listdetailsStatus.NumlistItemID As PriorityId, -- Priority column   
 listdetailsActivity.NumlistItemID As ActivityId ,
 ''<a href="../account/frmAccounts.aspx?DivID=''+CAST(Div.numDivisionID AS VARCHAR)+''">''+Comp.vcCompanyName+ CASE WHEN  ISNULL(listCompanyRating.VcData,'''')<>'''' THEN ''(''+listCompanyRating.VcData+'')'' ELSE '''' END +''</a>''  AS OrgName,
 listCompanyRating.VcData As CompanyRating,
 Addc.numContactID,Addc.numDivisionID,Div.tintCRMType,Comm.numCommId AS Id,convert(varchar(15),bitTask) as bitTask,
  DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', dtStartTime) as Startdate,                        
 DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', dtEndTime) as EndTime ,                                                                        
 Comm.textDetails AS itemDesc,  
 AddC.vcFirstName + '' '' + AddC.vcLastName  as [Name], 
 AddC.vcFirstName,AddC.vcLastName,Addc.numPhone,AddC.numPhoneExtension,                               
 case when Addc.numPhone<>'''' then + AddC.numPhone +case when AddC.numPhoneExtension<>'''' then '' - '' + AddC.numPhoneExtension else '''' end  else '''' end as [Phone], 
  Comp.vcCompanyName As vcCompanyName , ISNULL((SELECT vcData FROM ListDetails WHERE numListID =21 AND numDomainID='+ CAST(@numDomainID AS VARCHAR) +' AND  numListItemID=Comp.vcProfile ) ,'''') vcProfile ,                                                        
 AddC.vcEmail As vcEmail ,    
 dbo.GetListIemName(Comm.bitTask)AS Task,   
 listdetailsStatus.VcData As Status,dbo.fn_GetContactName(Comm.numCreatedBy) AS numRecOwner,dbo.fn_GetContactName(Comm.numModifiedBy) AS numModifiedBy,  
  (select TOP 1 vcData from ListDetails where numListItemID=Div.numTerId) AS numTerId,dbo.fn_GetContactName(Comm.numAssignedBy) AS numAssignedBy,
  convert(varchar(50),comm.caseid) as caseid,(select top 1 vcCaseNumber from cases where cases.numcaseid= comm.caseid )as vcCasenumber,
  ISNULL(comm.casetimeId,0) casetimeId,ISNULL(comm.caseExpId,0) caseExpId,0 as type,'''' as itemid,ISNULL(bitFollowUpAnyTime,0) bitFollowUpAnyTime,  
  dbo.GetListIemName(Comp.numNoOfEmployeesId) AS numNoOfEmployeesId,Div.vcComPhone,
dbo.GetListIemName(Div.numFollowUpStatus) AS numFollowUpStatus,' + 
CONCAT('ISNULL(dbo.FormatedDateFromDate(Div.dtLastFollowUp,',@numDomainID,'),'''')') + ' AS dtLastFollowUp,
' + CONCAT('ISNULL(dbo.FormatedDateFromDate(TempLastOrder.bintCreatedDate,',@numDomainID,'),'''')') + ' AS vcLastSalesOrderDate,ISNULL(TempPerformance.monDealAmount,0) monDealAmount,
(CASE WHEN ISNULL(TempPerformance.monDealAmount,0) > 0 THEN CONCAT(FORMAT(TempPerformance.monDealAmount,''##,####.00''),'' ('',CAST(TempPerformance.fltProfitPer AS DECIMAL(18,2)),''%)'') ELSE '''' END) AS vcPerformance
  '+@vcCustomColumnName+'
 FROM  Communication Comm                              
 JOIN AdditionalContactsInformation AddC                                                  
 ON Comm.numContactId = AddC.numContactId                                  
 JOIN DivisionMaster Div                                                                         
 ON AddC.numDivisionId = Div.numDivisionID                                   
 JOIN CompanyInfo  Comp                                                                         
 ON Div.numCompanyID = Comp.numCompanyId  
 OUTER APPLY(' + CONCAT('SELECT MAX(bintCreatedDate) AS bintCreatedDate FROM OpportunityMaster WHERE numDomainID = ',@numDomainID,' AND numDivisionID=Div.numDivisionID AND tintOppType = 1 AND tintOppStatus = 1') + ' ) AS TempLastOrder
 OUTER APPLY(' + CONCAT('SELECT
																	SUM(monDealAmount) AS monDealAmount
																	,AVG(ProfitPer) AS fltProfitPer
																FROM
																(
																	SELECT
																		OM.numDivisionId
																		,monDealAmount
																		,AVG(((ISNULL(OI.monPrice,0) - (CASE D.numCost WHEN 3 THEN ISNULL(V.monCost,0) * dbo.fn_UOMConversion(I.numBaseUnit,I.numItemCode,I.numDomainID,I.numPurchaseUnit) WHEN 2 THEN ISNULL(OI.numCost,0) ELSE ISNULL(I.monAverageCost,0) END)) / ISNULL(NULLIF(OI.monPrice,0),1)) * 100) ProfitPer
																	FROM
																		OpportunityMaster OM
																	INNER JOIN
																		Domain D
																	ON
																		OM.numDomainID = D.numDomainID
																	INNER JOIN
																		OpportunityItems OI
																	ON
																		OM.numOppId = OI.numOppId
																	INNER JOIN
																		Item I
																	ON
																		OI.numItemCode = I.numItemCode
																	LEFT JOIN
																		Vendor V
																	ON
																		V.numVendorID = I.numVendorID
																		AND V.numItemCode = I.numItemCode
																	WHERE
																		OM.numDomainId = ',@numDomainID,'
																		AND OM.numDivisionId = Div.numDivisionID
																		AND ISNULL(I.bitContainer,0) = 0
																		AND tintOppType = 1
																		AND tintOppStatus=1
																	GROUP BY
																		OM.numDivisionId
																		,OM.numOppId
																		,monDealAmount
																) T1
																GROUP BY
																	T1.numDivisionId )') + ' AS TempPerformance
 LEFT JOIN OpportunityMaster Opp ON Opp.numDivisionId = Div.numDivisionID AND Opp.numContactId = AddC.numContactId  AND Opp.numOppID = Comm.numOppID 
 LEFT JOIN Sales_process_List_Master splm ON splm.Slp_Id = Opp.numBusinessProcessID 
 LEFT JOIN ProjectProgress PP On PP.numOppId = Opp.numOppId
  Left Join listdetails listdetailsStatus                                                 
 On Comm.numStatus = listdetailsStatus.NumlistItemID
  Left Join listdetails listdetailsActivity                                                  
 On Comm.numActivity = listdetailsActivity.NumlistItemID
 LEFT JOIN ListDetails AS listCompanyRating
 ON Comp.numCompanyRating=listCompanyRating.NumlistItemID
WHERE '+@ActivityRegularSearch+' AND
	Comm.numDomainID = ' + Cast(@numDomainID as varchar(10))  + ' AND 1 = (CASE ' + CAST( @tintActionItemsViewRights AS VARCHAR) + 
' WHEN 3 THEN 1 
WHEN 2 THEN (CASE WHEN Div.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+ CONVERT(VARCHAR(15),@numUserCntID) + ' ) THEN 1 ELSE 0 END)
WHEN 1 THEN (CASE WHEN (Comm.numCreatedBy= ' + CONVERT(VARCHAR(15),@numUserCntID) + ' or Comm.numAssign= '+ CONVERT(VARCHAR(15),@numUserCntID) + ') THEN 1 ELSE 0 END) 
ELSE 0 END)  AND (DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',dtStartTime) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',dtStartTime) <= '''+Cast(@endDate as varchar(30))+''')
'+@CustomSearchCriteria+'
 )   Q WHERE '+@OtherRegularSearch+'
  '+@IndivRecordPaging+'
'


EXEC (@dynamicQuery)

-------------------- 0 - Email Communication----------------------------
--''<a onclick="openCommTask(''+CAST(ac.activityid AS VARCHAR)+'',1)" href="javascript:void(0)">Calendar</a>'' As TaskTypeName,
IF(LEN(@ActivityRegularSearch)=0 OR RTRIM(LTRIM(@ActivityRegularSearch))='1=1')
BEGIN
DECLARE @FormattedItems As VARCHAR(MAX)
SET @FormattedItems ='
RecordCount,RecordId,
numCreatedBy,
numOrgTerId,
numTaskId,
IsTaskClosed,
TaskTypeName,
TaskType,
OrigDescription,
Description,
dtDueDate,
DueDate,
TotalProgress,
numAssignedTo,
numAssignToId,
Priority,
Activity,
PriorityId,
ActivityId,
OrgName,
CompanyRating'
SET @dynamicQuery =  ' INSERT INTO #tempRecords(
'+@FormattedItems+'
) SELECT  COUNT(*) OVER() RecordCount,* FROM (  SELECT 
ac.activityid AS RecordId,
0 AS numCreatedBy,
ISNULL(Div.numTerId,0) AS numOrgTerId,
-2 AS numTaskId,
case when alldayevent = 1 then ''1'' else ''0'' end AS IsTaskClosed,
''<a href="javascript:void(0)">Calendar</a>'' As TaskTypeName,
12 AS TaskType,
CASE WHEN ISNULL(ac.Comments,'''') ='''' THEN ac.ActivityDescription ELSE ISNULL(ac.Comments,'''') END AS OrigDescription,
(SELECT DISTINCT  
replace(replace((SELECT 
CASE(ISNULL(bitpartycalendarTitle,1)) WHEN 1 THEN ''<b><font color=red>Title</font></b>'' + CASE(ISNULL(ac.Subject,'''')) WHEN '''' THEN '''' else ac.Subject END + ''<br>'' else ''''  END  
+ CASE(ISNULL(bitpartycalendarLocation,1)) WHEN 1 THEN ''<b><font color=green>Location</font></b>'' + CASE(ISNULL(ac.location,'''')) WHEN '''' THEN '''' else ac.location END + ''<br>'' else ''''  END  
+ CASE ISNULL(bitpartycalendarDescription,1) 
	WHEN 1 
	THEN CONCAT(''<b><font color=#3c8dbc>Description</font></b>'',
				(CASE 
					WHEN LEN(CASE WHEN ISNULL(ac.Comments,'''') ='''' THEN ac.ActivityDescription ELSE ISNULL(ac.Comments,'''') END) > 100 
					THEN CONCAT(CAST((CASE WHEN ISNULL(ac.Comments,'''') ='''' THEN ac.ActivityDescription ELSE ISNULL(ac.Comments,'''') END) AS VARCHAR(100)),CONCAT(''...'',''<a href="#" role="button" title="Description" data-toggle="popover" data-content="'',(CASE WHEN ISNULL(ac.Comments,'''') ='''' THEN ac.ActivityDescription ELSE ISNULL(ac.Comments,'''') END) ,''"> more</a>'')) 
					ELSE (CASE WHEN ISNULL(ac.Comments,'''') ='''' THEN ac.ActivityDescription ELSE ISNULL(ac.Comments,'''') END) 
				END)) 
	ELSE ''''  
	END  
FROM Domain where numDomainId='+ CAST(@numDomainID AS VARCHAR) +' 
FOR XML PATH('''')
),''&lt;'', ''<''), ''&gt;'', ''>''))   AS Description,
DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', dateadd(second,duration,startdatetimeutc)) AS dtDueDate,
DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', dateadd(second,duration,startdatetimeutc)) as DueDate,
''0'' As TotalProgress,
''-'' As numAssignedTo,
''0'' As numAssignToId,
'''' As Priority, -- Priority column   
'''' As Activity ,
0 As PriorityId, -- Priority column   
 0 As ActivityId ,
 ''<a href="../account/frmAccounts.aspx?DivID=''+CAST(Div.numDivisionID AS VARCHAR)+''">''+Comp.vcCompanyName+ CASE WHEN  ISNULL(listCompanyRating.VcData,'''')<>'''' THEN ''(''+listCompanyRating.VcData+'')'' ELSE '''' END +''</a>''  AS OrgName,
 listCompanyRating.VcData As CompanyRating
 From activity ac join             
activityresource ar on  ac.activityid=ar.activityid             
join resource res on ar.resourceid=res.resourceid                         
--join usermaster UM on um.numUserId = res.numUserCntId   
--res.numUserCntId 
join AdditionalContactsInformation ACI on ACI.numContactId =  (SELECT TOP 1 ContactId FROM ActivityAttendees where ActivityID=ac.activityid)           
 JOIN DivisionMaster Div                                                          
 ON ACI.numDivisionId = Div.numDivisionID                                   
 JOIN CompanyInfo  Comp                             
 ON Div.numCompanyID = Comp.numCompanyId
  LEFT JOIN ListDetails AS listCompanyRating
 ON Comp.numCompanyRating=listCompanyRating.NumlistItemID
 OUTER APPLY(' + CONCAT('SELECT MAX(bintCreatedDate) AS bintCreatedDate FROM OpportunityMaster WHERE numDomainID = ',@numDomainID,' AND numDivisionID=Div.numDivisionID AND tintOppType = 1 AND tintOppStatus = 1') + ' ) AS TempLastOrder                 
 OUTER APPLY(' + CONCAT('SELECT
																	SUM(monDealAmount) AS monDealAmount
																	,AVG(ProfitPer) AS fltProfitPer
																FROM
																(
																	SELECT
																		OM.numDivisionId
																		,monDealAmount
																		,AVG(((ISNULL(OI.monPrice,0) - (CASE D.numCost WHEN 3 THEN ISNULL(V.monCost,0) * dbo.fn_UOMConversion(I.numBaseUnit,I.numItemCode,I.numDomainID,I.numPurchaseUnit) WHEN 2 THEN ISNULL(OI.numCost,0) ELSE ISNULL(I.monAverageCost,0) END)) / ISNULL(NULLIF(OI.monPrice,0),1)) * 100) ProfitPer
																	FROM
																		OpportunityMaster OM
																	INNER JOIN
																		Domain D
																	ON
																		OM.numDomainID = D.numDomainID
																	INNER JOIN
																		OpportunityItems OI
																	ON
																		OM.numOppId = OI.numOppId
																	INNER JOIN
																		Item I
																	ON
																		OI.numItemCode = I.numItemCode
																	LEFT JOIN
																		Vendor V
																	ON
																		V.numVendorID = I.numVendorID
																		AND V.numItemCode = I.numItemCode
																	WHERE
																		OM.numDomainId = ',@numDomainID,'
																		AND OM.numDivisionId = Div.numDivisionID
																		AND ISNULL(I.bitContainer,0) = 0
																		AND tintOppType = 1
																		AND tintOppStatus=1
																		
																	GROUP BY
																		OM.numDivisionId
																		,OM.numOppId
																		,monDealAmount
																) T1
																GROUP BY
																	T1.numDivisionId )') + ' AS TempPerformance

 where  res.numUserCntId = ' + Cast(@numUserCntID as varchar(10)) +'             
and ac.OriginalStartDateTimeUtc IS NULL            
and res.numDomainID = ' + Cast(@numDomainID as varchar(10)) +'  and 
(DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', startdatetimeutc) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', startdatetimeutc) <= '''+Cast(@endDate as varchar(30))+''')
--(startdatetimeutc between dateadd(month,-1,getutcdate()) and dateadd(month,1,getutcdate())) 
and [status] <> -1                                                     
and ac.activityid not in 
(select distinct(numActivityId) from communication Comm where Comm.numDomainID = ' + Cast(@numDomainID as varchar(10)) +'  
AND 1=(Case ' + Cast(@bitFilterRecord as varchar(10)) +' when 1 then Case when Comm.numAssign =' + Cast(@numUserCntID as varchar(10)) +'  then 1 else 0 end 
		else  Case when (Comm.numAssign =' + Cast(@numUserCntID as varchar(10)) +'  or Comm.numCreatedBy=' + Cast(@numUserCntID as varchar(10)) +' ) then 1 else 0 end end)   
		AND 1 = (CASE ' + CAST( @tintActionItemsViewRights AS VARCHAR) + 
' WHEN 3 THEN 1 
WHEN 2 THEN (CASE WHEN Div.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+ CONVERT(VARCHAR(15),@numUserCntID) + ' ) THEN 1 ELSE 0 END)
WHEN 1 THEN (CASE WHEN (Comm.numCreatedBy= ' + CONVERT(VARCHAR(15),@numUserCntID) + ' or Comm.numAssign= '+ CONVERT(VARCHAR(15),@numUserCntID) + ') THEN 1 ELSE 0 END) 
ELSE 0 END)                                                                   

)) Q WHERE '+@OtherRegularSearch+'  '+@IndivRecordPaging+'
'
PRINT CAST(@dynamicQuery AS NTEXT)
EXEC (@dynamicQuery)
-------------------- 1 - CASE----------------------------

SET @dynamicQuery =  ' INSERT INTO #tempRecords(
RecordCount,
RecordId,
numCreatedBy,
numOrgTerId,
numTaskId,
IsTaskClosed,
TaskTypeName,
TaskType,
OrigDescription,
Description,
dtDueDate,
DueDate,
TotalProgress,
numAssignedTo,
numAssignToId,
Priority,
Activity,
PriorityId,
ActivityId,
OrgName,
CompanyRating
) SELECT  COUNT(*) OVER() RecordCount,* FROM (  SELECT 
C.numCaseId AS RecordId,
C.numCreatedBy AS numCreatedBy,
ISNULL(Div.numTerId,0) AS numOrgTerId,
0 AS numTaskId,
CASE WHEN C.numStatus=1008 THEN 1 ELSE 0 END AS IsTaskClosed,
''<a onclick="openCaseDetails(''+CAST(C.numCaseId AS VARCHAR)+'')" href="javascript:void(0)">Case</a>'' As TaskTypeName,
1 AS TaskType,
'''' AS OrigDescription,
C.textSubject AS Description,
cast(C.intTargetResolveDate as datetime) AS dtDueDate,
cast(C.intTargetResolveDate as datetime) as DueDate,
''0'' As TotalProgress,
dbo.fn_GetContactName(C.numAssignedTo) As numAssignedTo,
C.numAssignedTo As numAssignToId,
listdetailsStatus.VcData As Priority, -- Priority column   
'''' As Activity ,
listdetailsStatus.NumlistItemID As PriorityId, -- Priority column   
 0 As ActivityId ,
 ''<a href="../account/frmAccounts.aspx?DivID=''+CAST(Div.numDivisionID AS VARCHAR)+''">''+Com.vcCompanyName+ CASE WHEN  ISNULL(listCompanyRating.VcData,'''')<>'''' THEN ''(''+listCompanyRating.VcData+'')'' ELSE '''' END +''</a>''  AS OrgName,
 listCompanyRating.VcData As CompanyRating

 FROM 
	Cases  C                          
INNER JOIN  
	AdditionalContactsInformation  AddC                          
ON 
	C.numContactId = AddC.numContactId                           
INNER JOIN 
	DivisionMaster Div                           
ON 
	AddC.numDivisionId = Div.numDivisionID AND  
	AddC.numDivisionId = Div.numDivisionID                           
INNER JOIN  
	CompanyInfo  Com                          
ON 
	Div.numCompanyID = Com.numCompanyId    
 LEFT JOIN ListDetails AS listCompanyRating
 ON Com.numCompanyRating=listCompanyRating.NumlistItemID
  Left Join listdetails listdetailsStatus                                                 
 On C.numPriority = listdetailsStatus.NumlistItemID
WHERE
	C.numDomainID = ' + Cast(@numDomainID as varchar(10))  + ' AND C.numStatus <> 136  AND 1 = (CASE ' + CAST( @tintActionItemsViewRights AS VARCHAR) + 
' WHEN 3 THEN 1 
WHEN 2 THEN (CASE WHEN Div.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+ CONVERT(VARCHAR(15),@numUserCntID) + ' ) THEN 1 ELSE 0 END)
WHEN 1 THEN (CASE WHEN (C.numCreatedBy= ' + CONVERT(VARCHAR(15),@numUserCntID) + ' or C.numAssignedTo= '+ CONVERT(VARCHAR(15),@numUserCntID) + ') THEN 1 ELSE 0 END) 
ELSE 0 END)  AND (DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',intTargetResolveDate) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',intTargetResolveDate) <= '''+Cast(@endDate as varchar(30))+''') 
) Q WHERE '+@OtherRegularSearch+' '+@IndivRecordPaging+'
'
EXEC (@dynamicQuery)

-------------------- 2 - Project Task ----------------------------

SET @dynamicQuery =  ' INSERT INTO #tempRecords(
RecordCount,
RecordId,
numCreatedBy,
numOrgTerId,
numTaskId,
IsTaskClosed,
TaskTypeName,
TaskType,
OrigDescription,
Description,
dtDueDate,
DueDate,
TotalProgress,
numAssignedTo,
numAssignToId,
Priority,
Activity,
PriorityId,
ActivityId,
OrgName,
CompanyRating
)  SELECT  COUNT(*) OVER() RecordCount,* FROM (SELECT
T.numProjectId AS RecordId,
T.numCreatedBy AS numCreatedBy,
ISNULL(Div.numTerId,0) AS numOrgTerId,
T.numTaskId AS numTaskId,
T.bitTaskClosed AS IsTaskClosed,
''<a onclick="openProjectTask(''+CAST(T.numProjectId AS VARCHAR)+'')" href="javascript:void(0)">Project Task</a>'' As TaskTypeName,
2 AS TaskType,
'''' AS OrigDescription,
T.vcTaskName AS Description,
CASE WHEN S.bitIsDueDaysUsed=1 THEN CONVERT(VARCHAR(10),DATEADD(DAY,S.intDueDays,CAST(S.dtStartDate AS DATE)), 101)
		ELSE CONVERT(VARCHAR(10),DATEADD(DAY,(SELECT TOP 1 CAST(Items AS DECIMAL(18,2)) FROM Split(dbo.fn_calculateTotalHoursMinutesByStageDetails(T.numStageDetailsId,T.numOppId,T.numProjectId),''_'')),CAST(S.dtStartDate AS DATE)), 101) END AS dtDueDate,
CASE WHEN S.bitIsDueDaysUsed=1 THEN CONVERT(VARCHAR(10),DATEADD(DAY,S.intDueDays,CAST(S.dtStartDate AS DATE)), 101)
		ELSE CONVERT(VARCHAR(10),DATEADD(DAY,(SELECT TOP 1 CAST(Items AS DECIMAL(18,2)) FROM Split(dbo.fn_calculateTotalHoursMinutesByStageDetails(T.numStageDetailsId,T.numOppId,T.numProjectId),''_'')),CAST(S.dtStartDate AS DATE)), 101) END as DueDate,
ISNULL((SELECT TOP 1 intTotalProgress FROM ProjectProgress WHERE numProId = T.numProjectId),0) As TotalProgress,
dbo.fn_GetContactName(T.numAssignTo) As numAssignedTo,
T.numAssignTo As numAssignToId,
'''' As Priority, -- Priority column   
'''' As Activity ,
0 As PriorityId, -- Priority column   
 0 As ActivityId ,
 ''<a href="../account/frmAccounts.aspx?DivID=''+CAST(Div.numDivisionID AS VARCHAR)+''">''+Com.vcCompanyName+ CASE WHEN  ISNULL(listCompanyRating.VcData,'''')<>'''' THEN ''(''+listCompanyRating.VcData+'')'' ELSE '''' END +''</a>''  AS OrgName,
 listCompanyRating.VcData As CompanyRating

 FROM 
	StagePercentageDetailsTask AS T
	LEFT JOIN StagePercentageDetails AS S
	ON S.numStageDetailsId=T.numStageDetailsId AND S.numProjectID=T.numProjectID
	LEFT JOIN ProjectsMaster AS OP 
	ON T.numProjectID=OP.numProId
	LEFT JOIN StagePercentageMaster AS SM
	ON S.numStagePercentageId=SM.numStagePercentageId                        
INNER JOIN 
	DivisionMaster Div                           
ON 
	OP.numDivisionId = Div.numDivisionID                       
INNER JOIN  
	CompanyInfo  Com                          
ON 
	Div.numCompanyID = Com.numCompanyId    
LEFT JOIN ListDetails AS listCompanyRating
 ON Com.numCompanyRating=listCompanyRating.NumlistItemID
WHERE
	T.numDomainID = ' + Cast(@numDomainID as varchar(10))  + ' AND T.numProjectId>0 AND s.numProjectId>0 AND T.bitSavedTask=1 AND T.bitTaskClosed=0 AND 1 = (CASE ' + CAST( @tintActionItemsViewRights AS VARCHAR) + 
' WHEN 3 THEN 1 
WHEN 2 THEN (CASE WHEN Div.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+ CONVERT(VARCHAR(15),@numUserCntID) + ' ) THEN 1 ELSE 0 END)
WHEN 1 THEN (CASE WHEN (T.numCreatedBy= ' + CONVERT(VARCHAR(15),@numUserCntID) + ' or T.numAssignTo= '+ CONVERT(VARCHAR(15),@numUserCntID) + ') THEN 1 ELSE 0 END) 
ELSE 0 END) ) Q WHERE '+@OtherRegularSearch+' AND (DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',Q.DueDate) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',Q.DueDate) <= '''+Cast(@endDate as varchar(30))+''') 
'+@IndivRecordPaging+'
'

EXEC (@dynamicQuery)

------------------ 3 - Opportunity  Task ----------------------------

SET @dynamicQuery =  ' INSERT INTO #tempRecords(
RecordCount,
RecordId,
numCreatedBy,
numOrgTerId,
numTaskId,
IsTaskClosed,
TaskTypeName,
TaskType,
OrigDescription,
Description,
dtDueDate,
DueDate,
TotalProgress,
numAssignedTo,
numAssignToId,
Priority,
Activity,
PriorityId,
ActivityId,
OrgName,
CompanyRating
) SELECT  COUNT(*) OVER() RecordCount,* FROM ( SELECT 
T.numOppId AS RecordId,
T.numCreatedBy AS numCreatedBy,
ISNULL(Div.numTerId,0) AS numOrgTerId,
T.numTaskId AS numTaskId,
T.bitTaskClosed AS IsTaskClosed,
''<a onclick="openTask(''+CAST(T.numOppId AS VARCHAR)+'',''+CAST(S.numStagePercentageId AS VARCHAR)+'',''+CAST(S.tinProgressPercentage AS VARCHAR)+'',''+CAST(S.tintConfiguration AS VARCHAR)+'')" href="javascript:void(0)">Opportunity Task</a>'' As TaskTypeName,
3 AS TaskType,
'''' AS OrigDescription,
T.vcTaskName AS Description,
CASE WHEN S.bitIsDueDaysUsed=1 THEN CONVERT(VARCHAR(10),DATEADD(DAY,S.intDueDays,CAST(S.dtStartDate AS DATE)), 101)
		ELSE CONVERT(VARCHAR(10),DATEADD(DAY,(SELECT TOP 1 CAST(Items AS DECIMAL(18,2)) FROM Split(dbo.fn_calculateTotalHoursMinutesByStageDetails(T.numStageDetailsId,T.numOppId,T.numProjectId),''_'')),CAST(S.dtStartDate AS DATE)), 101) END AS dtDueDate,
CASE WHEN S.bitIsDueDaysUsed=1 THEN CONVERT(VARCHAR(10),DATEADD(DAY,S.intDueDays,CAST(S.dtStartDate AS DATE)), 101)
		ELSE CONVERT(VARCHAR(10),DATEADD(DAY,(SELECT TOP 1 CAST(Items AS DECIMAL(18,2)) FROM Split(dbo.fn_calculateTotalHoursMinutesByStageDetails(T.numStageDetailsId,T.numOppId,T.numProjectId),''_'')),CAST(S.dtStartDate AS DATE)), 101) END as DueDate,
ISNULL((SELECT TOP 1 intTotalProgress FROM ProjectProgress WHERE numOppId = T.numOppId),0) As TotalProgress,
dbo.fn_GetContactName(T.numAssignTo) As numAssignedTo,
T.numAssignTo As numAssignToId,
'''' As Priority, -- Priority column   
'''' As Activity ,
0 As PriorityId, -- Priority column   
 0 As ActivityId ,
 ''<a href="../account/frmAccounts.aspx?DivID=''+CAST(Div.numDivisionID AS VARCHAR)+''">''+Com.vcCompanyName+ CASE WHEN  ISNULL(listCompanyRating.VcData,'''')<>'''' THEN ''(''+listCompanyRating.VcData+'')'' ELSE '''' END +''</a>''  AS OrgName,
 listCompanyRating.VcData As CompanyRating

 FROM 
	StagePercentageDetailsTask AS T
	LEFT JOIN StagePercentageDetails AS S
	ON S.numStageDetailsId=T.numStageDetailsId AND S.numOppId=T.numOppId
	LEFT JOIN OpportunityMaster AS OP 
	ON T.numOppId=OP.numOppId
	LEFT JOIN StagePercentageMaster AS SM
	ON S.numStagePercentageId=SM.numStagePercentageId                        
INNER JOIN 
	DivisionMaster Div                           
ON 
	OP.numDivisionId = Div.numDivisionID                       
INNER JOIN  
	CompanyInfo  Com                          
ON 
	Div.numCompanyID = Com.numCompanyId    
LEFT JOIN ListDetails AS listCompanyRating
 ON Com.numCompanyRating=listCompanyRating.NumlistItemID
WHERE
	T.numDomainID = ' + Cast(@numDomainID as varchar(10))  + ' AND OP.tintOppType=1 AND OP.tintOppStatus=0 AND T.numOppId>0 AND s.numOppId>0 AND T.bitSavedTask=1 AND T.bitTaskClosed=0 AND 1 = (CASE ' + CAST( @tintActionItemsViewRights AS VARCHAR) + 
' WHEN 3 THEN 1 
WHEN 2 THEN (CASE WHEN Div.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+ CONVERT(VARCHAR(15),@numUserCntID) + ' ) THEN 1 ELSE 0 END)
WHEN 1 THEN (CASE WHEN (T.numCreatedBy= ' + CONVERT(VARCHAR(15),@numUserCntID) + ' or T.numAssignTo= '+ CONVERT(VARCHAR(15),@numUserCntID) + ') THEN 1 ELSE 0 END) 
ELSE 0 END) ) Q WHERE '+@OtherRegularSearch+' AND (DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',Q.DueDate) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',Q.DueDate) <= '''+Cast(@endDate as varchar(30))+''')
'+@IndivRecordPaging+' 
 '
 
 EXEC (@dynamicQuery)

------------------ 4 -  Sales Order Task ----------------------------

SET @dynamicQuery =  ' INSERT INTO #tempRecords(
RecordCount,
RecordId,
numCreatedBy,
numOrgTerId,
numTaskId,
IsTaskClosed,
TaskTypeName,
TaskType,
OrigDescription,
Description,
dtDueDate,
DueDate,
TotalProgress,
numAssignedTo,
numAssignToId,
Priority,
Activity,
PriorityId,
ActivityId,
OrgName,
CompanyRating
)SELECT  COUNT(*) OVER() RecordCount,* FROM (  SELECT 
T.numOppId AS RecordId,
T.numCreatedBy AS numCreatedBy,
ISNULL(Div.numTerId,0) AS numOrgTerId,
T.numTaskId AS numTaskId,
T.bitTaskClosed AS IsTaskClosed,
''<a onclick="openTask(''+CAST(T.numOppId AS VARCHAR)+'',''+CAST(S.numStagePercentageId AS VARCHAR)+'',''+CAST(S.tinProgressPercentage AS VARCHAR)+'',''+CAST(S.tintConfiguration AS VARCHAR)+'')" href="javascript:void(0)">Sales Order Task</a>'' As TaskTypeName,
4 AS TaskType,
'''' AS OrigDescription,
T.vcTaskName AS Description,
CASE WHEN S.bitIsDueDaysUsed=1 THEN CONVERT(VARCHAR(10),DATEADD(DAY,S.intDueDays,CAST(S.dtStartDate AS DATE)), 101)
		ELSE CONVERT(VARCHAR(10),DATEADD(DAY,(SELECT TOP 1 CAST(Items AS DECIMAL(18,2)) FROM Split(dbo.fn_calculateTotalHoursMinutesByStageDetails(T.numStageDetailsId,T.numOppId,T.numProjectId),''_'')),CAST(S.dtStartDate AS DATE)), 101) END AS dtDueDate,
CASE WHEN S.bitIsDueDaysUsed=1 THEN CONVERT(VARCHAR(10),DATEADD(DAY,S.intDueDays,CAST(S.dtStartDate AS DATE)), 101)
		ELSE CONVERT(VARCHAR(10),DATEADD(DAY,(SELECT TOP 1 CAST(Items AS DECIMAL(18,2)) FROM Split(dbo.fn_calculateTotalHoursMinutesByStageDetails(T.numStageDetailsId,T.numOppId,T.numProjectId),''_'')),CAST(S.dtStartDate AS DATE)), 101) END as DueDate,
ISNULL((SELECT TOP 1 intTotalProgress FROM ProjectProgress WHERE numOppId = T.numOppId),0) As TotalProgress,
dbo.fn_GetContactName(T.numAssignTo) As numAssignedTo,
T.numAssignTo As numAssignToId,
'''' As Priority, -- Priority column   
'''' As Activity ,
0 As PriorityId, -- Priority column   
 0 As ActivityId ,
 ''<a href="../account/frmAccounts.aspx?DivID=''+CAST(Div.numDivisionID AS VARCHAR)+''">''+Com.vcCompanyName+ CASE WHEN  ISNULL(listCompanyRating.VcData,'''')<>'''' THEN ''(''+listCompanyRating.VcData+'')'' ELSE '''' END +''</a>''  AS OrgName,
 listCompanyRating.VcData As CompanyRating

 FROM 
	StagePercentageDetailsTask AS T
	LEFT JOIN StagePercentageDetails AS S
	ON S.numStageDetailsId=T.numStageDetailsId AND S.numOppId=T.numOppId
	LEFT JOIN OpportunityMaster AS OP 
	ON T.numOppId=OP.numOppId
	LEFT JOIN StagePercentageMaster AS SM
	ON S.numStagePercentageId=SM.numStagePercentageId                        
INNER JOIN 
	DivisionMaster Div                           
ON 
	OP.numDivisionId = Div.numDivisionID                       
INNER JOIN  
	CompanyInfo  Com                          
ON 
	Div.numCompanyID = Com.numCompanyId    
LEFT JOIN ListDetails AS listCompanyRating
 ON Com.numCompanyRating=listCompanyRating.NumlistItemID
WHERE
	T.numDomainID = ' + Cast(@numDomainID as varchar(10))  + ' AND OP.tintOppType=1 AND OP.tintOppStatus=1 AND T.numOppId>0 AND s.numOppId>0 AND T.bitSavedTask=1 AND T.bitTaskClosed=0 AND 1 = (CASE ' + CAST( @tintActionItemsViewRights AS VARCHAR) + 
' WHEN 3 THEN 1 
WHEN 2 THEN (CASE WHEN Div.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+ CONVERT(VARCHAR(15),@numUserCntID) + ' ) THEN 1 ELSE 0 END)
WHEN 1 THEN (CASE WHEN (T.numCreatedBy= ' + CONVERT(VARCHAR(15),@numUserCntID) + ' or T.numAssignTo= '+ CONVERT(VARCHAR(15),@numUserCntID) + ') THEN 1 ELSE 0 END) 
ELSE 0 END) ) Q WHERE '+@OtherRegularSearch+' AND (DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',Q.DueDate) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',Q.DueDate) <= '''+Cast(@endDate as varchar(30))+''')
'+@IndivRecordPaging+'
 '
 EXEC (@dynamicQuery)
  
------------------ 5 -  Work Order Task ----------------------------

SET @dynamicQuery =  ' INSERT INTO #tempRecords(
RecordCount,
RecordId,
numCreatedBy,
numOrgTerId,
numTaskId,
IsTaskClosed,
TaskTypeName,
TaskType,
OrigDescription,
Description,
dtDueDate,
DueDate,
TotalProgress,
numAssignedTo,
numAssignToId,
Priority,
Activity,
PriorityId,
ActivityId,
OrgName,
CompanyRating
)SELECT  COUNT(*) OVER() RecordCount,* FROM ( SELECT 
WorkOrder.numWOID AS RecordId,
WorkOrder.numCreatedBy AS numCreatedBy,
ISNULL(Div.numTerId,0) AS numOrgTerId,
WorkOrder.numWOID AS numTaskId,
(CASE WHEN WorkOrder.numWOStatus = 23184 THEN 1 ELSE 0 END) AS IsTaskClosed,
CONCAT(''<a href="../items/frmWorkOrder.aspx?WOID='',WorkOrder.numWOID,''" target="_blank">Work Order Management</a>'') As TaskTypeName,
5 AS TaskType,
'''' AS OrigDescription,
CONCAT(''<b>'',Item.vcItemName,''</b>'','' ('',WorkOrder.numQtyItemsReq,'')'','' <b>Work Order Status :'', ''</b>'',''<label class="lblWorkOrderStatus" id="'',WorkOrder.numWOID,''"><i class="fa fa-refresh fa-spin"></i></lable>'') AS Description,
DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',WorkOrder.dtmEndDate) AS dtDueDate,
DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',WorkOrder.dtmEndDate) as DueDate,
dbo.GetTotalProgress(WorkOrder.numDomainID,WorkOrder.numWOID,1,1,'''',0) As TotalProgress,
dbo.fn_GetContactName(WorkOrder.numAssignedTo) As numAssignedTo,
WorkOrder.numAssignedTo As numAssignToId,
'''' As Priority, -- Priority column   
'''' As Activity ,
0 As PriorityId, -- Priority column   
 0 As ActivityId ,
 ''<a href="../account/frmAccounts.aspx?DivID=''+CAST(Div.numDivisionID AS VARCHAR)+''">''+Com.vcCompanyName+ CASE WHEN  ISNULL(listCompanyRating.VcData,'''')<>'''' THEN ''(''+listCompanyRating.VcData+'')'' ELSE '''' END +''</a>''  AS OrgName,
 listCompanyRating.VcData As CompanyRating

 FROM 
	WorkOrder
INNER JOIN 
	Item
ON
	WorkOrder.numItemCode = Item.numItemCode
LEFT JOIN 
	OpportunityMaster
ON 
	WorkOrder.numOppId=OpportunityMaster.numOppId
INNER JOIN 
	DivisionMaster Div                           
ON 
	Div.numDivisionId = (CASE WHEN OpportunityMaster.numOppID IS NOT NULL THEN OpportunityMaster.numDivisionID ELSE ' + CAST(@numDomainDivisionID AS VARCHAR) + ' END)                        
INNER JOIN  
	CompanyInfo  Com                          
ON 
	Div.numCompanyID = Com.numCompanyId    
LEFT JOIN 
	ListDetails AS listCompanyRating
ON 
	Com.numCompanyRating=listCompanyRating.NumlistItemID
WHERE
	WorkOrder.numDomainID = ' + Cast(@numDomainID as varchar(10))  + ' 
	AND WorkOrder.numWOStatus <> 23184
	AND WorkOrder.numAssignedTo= '+ CONVERT(VARCHAR(15),@numUserCntID) + '
	AND ((DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',WorkOrder.dtmStartDate) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',WorkOrder.dtmStartDate) <= '''+Cast(@endDate as varchar(30))+''') OR (DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',WorkOrder.dtmEndDate) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',WorkOrder.dtmEndDate) <= '''+Cast(@endDate as varchar(30))+'''))
) Q WHERE '+@OtherRegularSearch+' '+@IndivRecordPaging+'
 '
 EXEC (@dynamicQuery)
------------------ 6 -  Work Center Task ----------------------------

SET @dynamicQuery =  ' INSERT INTO #tempRecords(
RecordCount,
RecordId,
numCreatedBy,
numOrgTerId,
numTaskId,
IsTaskClosed,
TaskTypeName,
TaskType,
OrigDescription,
Description,
dtDueDate,
DueDate,
TotalProgress,
numAssignedTo,
numAssignToId,
Priority,
Activity,
PriorityId,
ActivityId,
OrgName,
CompanyRating
) SELECT  COUNT(*) OVER() RecordCount,* FROM ( SELECT 
T.numTaskId AS RecordId,
T.numCreatedBy AS numCreatedBy,
ISNULL(Div.numTerId,0) AS numOrgTerId,
T.numTaskId AS numTaskId,
(CASE WHEN EXISTS (SELECT * FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID=T.numTaskID AND SPDTTL.tintAction=4) THEN 1 ELSE 0 END) AS IsTaskClosed,
CONCAT(''<a href="../items/frmWorkOrder.aspx?WOID='',W.numWOID,''" target="_blank">Work Center Task</a>'') As TaskTypeName,
6 AS TaskType,
'''' AS OrigDescription,
T.vcTaskName AS Description,
DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',W.dtmEndDate) AS dtDueDate,
DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',W.dtmEndDate) as DueDate,
(CASE 
	WHEN EXISTS (SELECT * FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID=T.numTaskID AND SPDTTL.tintAction=4) THEN CAST(100 AS INT)
	ELSE CAST(((ISNULL((SELECT SUM(numProcessedQty) FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID=T.numTaskID),0) / W.numQtyItemsReq) * 100) AS INT)
END) As TotalProgress,
dbo.fn_GetContactName(T.numAssignTo) As numAssignedTo,
T.numAssignTo As numAssignToId,
'''' As Priority, -- Priority column   
'''' As Activity ,
0 As PriorityId, -- Priority column   
 0 As ActivityId ,
 ''<a href="../account/frmAccounts.aspx?DivID=''+CAST(Div.numDivisionID AS VARCHAR)+''">''+Com.vcCompanyName+ CASE WHEN  ISNULL(listCompanyRating.VcData,'''')<>'''' THEN ''(''+listCompanyRating.VcData+'')'' ELSE '''' END +''</a>''  AS OrgName,
 listCompanyRating.VcData As CompanyRating

 FROM 
	StagePercentageDetailsTask AS T
	INNER JOIN WorkOrder AS W 
	ON T.numWorkOrderId=W.numWOId     
LEFT JOIN OpportunityMaster AS OP 
	ON W.numOppId=OP.numOppId
INNER JOIN 
	DivisionMaster Div                           
ON 
	Div.numDivisionId = (CASE WHEN OP.numOppID IS NOT NULL THEN OP.numDivisionID ELSE ' + CAST(@numDomainDivisionID AS VARCHAR) + ' END)                         
INNER JOIN  
	CompanyInfo  Com                          
ON 
	Div.numCompanyID = Com.numCompanyId    
LEFT JOIN ListDetails AS listCompanyRating
 ON Com.numCompanyRating=listCompanyRating.NumlistItemID
WHERE
	W.numDomainID = ' + Cast(@numDomainID as varchar(10))  + '
	AND W.numWOStatus <> 23184 
	AND NOT EXISTS (SELECT * FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID=T.numTaskID AND SPDTTL.tintAction=4) 
	AND T.numAssignTo = ' + CAST(@numUserCntID AS VARCHAR) + '
	AND ((DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',W.dtmStartDate) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',W.dtmStartDate) <= '''+Cast(@endDate as varchar(30))+''') OR (DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',W.dtmEndDate) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',W.dtmEndDate) <= '''+Cast(@endDate as varchar(30))+''')) 
) Q WHERE '+@OtherRegularSearch+' '+@IndivRecordPaging+'
'
EXEC (@dynamicQuery)

------------------ 7 -  Requisition Approval ----------------------------

IF(@bitREQPOApproval=1 AND @numUserCntID IN(SELECT * FROM Split(@vchREQPOApprovalEmp,',') WHERE Items<>''))
BEGIN
SET @dynamicQuery =  ' INSERT INTO #tempRecords(
RecordCount,
RecordId,
numCreatedBy,
numOrgTerId,
numTaskId,
IsTaskClosed,
TaskTypeName,
TaskType,
OrigDescription,
Description,
dtDueDate,
DueDate,
TotalProgress,
numAssignedTo,
numAssignToId,
Priority,
Activity,
PriorityId,
ActivityId,
OrgName,
CompanyRating
) SELECT  COUNT(*) OVER() RecordCount,* FROM( SELECT 
OP.numOppId AS RecordId,
OP.numCreatedBy AS numCreatedBy,
ISNULL(Div.numTerId,0) AS numOrgTerId,
0 AS numTaskId,
CASE WHEN ISNULL(OP.intReqPOApproved,0)=0 THEN 0 ELSE 1 END AS IsTaskClosed,
''<a  onclick="openSalesOrder(''+CAST(OP.numOppId AS VARCHAR)+'')" href="javascript:void(0)">Requisition Approval</a>'' As TaskTypeName,
7 AS TaskType,
'''' AS OrigDescription,
''<b>For :</b> <a class="underLineHyperLink" onclick="openSalesOrder(''+CAST(OP.numOppId AS VARCHAR)+'')" href="javascript:void(0)">''+OP.vcPOppName+''</a> &nbsp;<i onclick="ApproveRejectPORequistion(''+CAST(OP.numOppId AS VARCHAR)+'',1)"  class="fa fa-thumbs-up cursor"></i>  &nbsp;<i onclick="ApproveRejectPORequistion(''+CAST(OP.numOppId AS VARCHAR)+'',2)"  class="fa fa-thumbs-down cursor"></i>'' AS Description,
OP.intPEstimatedCloseDate AS dtDueDate,
OP.intPEstimatedCloseDate as DueDate,
0 As TotalProgress,
dbo.fn_GetContactName('+CAST(@numUserCntID AS VARCHAR(200))+') As numAssignedTo,
'+CAST(@numUserCntID AS VARCHAR(200))+' As numAssignToId,
'''' As Priority, -- Priority column   
'''' As Activity ,
0 As PriorityId, -- Priority column   
 0 As ActivityId ,
 ''<a href="../account/frmAccounts.aspx?DivID=''+CAST(Div.numDivisionID AS VARCHAR)+''">''+Com.vcCompanyName+ CASE WHEN  ISNULL(listCompanyRating.VcData,'''')<>'''' THEN ''(''+listCompanyRating.VcData+'')'' ELSE '''' END +''</a>''  AS OrgName,
 listCompanyRating.VcData As CompanyRating

 FROM  
 OpportunityMaster AS OP 
INNER JOIN 
	DivisionMaster Div                           
ON 
	OP.numDivisionId = Div.numDivisionID                       
INNER JOIN  
	CompanyInfo  Com                          
ON 
	Div.numCompanyID = Com.numCompanyId    
LEFT JOIN ListDetails AS listCompanyRating
 ON Com.numCompanyRating=listCompanyRating.NumlistItemID
WHERE
	OP.numDomainID = ' + Cast(@numDomainID as varchar(10))  + ' AND OP.tintOppType = 2 AND OP.tintOppStatus=0 
	AND 1 = (CASE WHEN '+CAST(@decReqPOMinValue AS VARCHAR(100))+'>0 AND Op.monPAmount>'+CAST(@decReqPOMinValue AS VARCHAR(100))+' THEN 1  WHEN '+CAST(@decReqPOMinValue AS VARCHAR(100))+'=0 THEN 1  ELSE 0 END ) ) AS Q
WHERE '+@OtherRegularSearch+' AND (DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',Q.DueDate) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',Q.DueDate) <= '''+Cast(@endDate as varchar(30))+''') 
'+@IndivRecordPaging+'
'
EXEC (@dynamicQuery)

END
------------------ 8 -  PO Approval ----------------------------
IF(@bitREQPOApproval=1 AND @numUserCntID IN(SELECT * FROM Split(@vchREQPOApprovalEmp,',') WHERE Items<>''))
BEGIN
SET @dynamicQuery =  ' INSERT INTO #tempRecords(
RecordCount,
RecordId,
numCreatedBy,
numOrgTerId,
numTaskId,
IsTaskClosed,
TaskTypeName,
TaskType,
OrigDescription,
Description,
dtDueDate,
DueDate,
TotalProgress,
numAssignedTo,
numAssignToId,
Priority,
Activity,
PriorityId,
ActivityId,
OrgName,
CompanyRating
) SELECT  COUNT(*) OVER() RecordCount,* FROM ( SELECT
OP.numOppId AS RecordId,
OP.numCreatedBy AS numCreatedBy,
ISNULL(Div.numTerId,0) AS numOrgTerId,
0 AS numTaskId,
CASE WHEN ISNULL(OP.intReqPOApproved,0)=0 THEN 0 ELSE 1 END AS IsTaskClosed,
''<a onclick="openSalesOrder(''+CAST(OP.numOppId AS VARCHAR)+'')" href="javascript:void(0)">PO Approval</a>'' As TaskTypeName,
8 AS TaskType,
'''' AS OrigDescription,
''<b>For:</b> <a href="javascript:void(0)"  class="underLineHyperLink" onclick="openSalesOrder(''+CAST(OP.numOppId AS VARCHAR)+'')">''+OP.vcPOppName+''</a> &nbsp;<i onclick="ApproveRejectPORequistion(''+CAST(OP.numOppId AS VARCHAR)+'',1)"  class="fa fa-thumbs-up cursor"></i>  &nbsp;<i onclick="ApproveRejectPORequistion(''+CAST(OP.numOppId AS VARCHAR)+'',2)"  class="fa fa-thumbs-down cursor"></i> '' AS Description,
OP.intPEstimatedCloseDate AS dtDueDate,
OP.intPEstimatedCloseDate as DueDate,
0 As TotalProgress,
dbo.fn_GetContactName('+CAST(@numUserCntID AS VARCHAR(200))+') As numAssignedTo,
'+CAST(@numUserCntID AS VARCHAR(200))+' As numAssignToId,
'''' As Priority, -- Priority column   
'''' As Activity ,
0 As PriorityId, -- Priority column   
 0 As ActivityId ,
 ''<a href="../account/frmAccounts.aspx?DivID=''+CAST(Div.numDivisionID AS VARCHAR)+''">''+Com.vcCompanyName+ CASE WHEN  ISNULL(listCompanyRating.VcData,'''')<>'''' THEN ''(''+listCompanyRating.VcData+'')'' ELSE '''' END +''</a>''  AS OrgName,
 listCompanyRating.VcData As CompanyRating

 FROM  
 OpportunityMaster AS OP 
INNER JOIN 
	DivisionMaster Div                           
ON 
	OP.numDivisionId = Div.numDivisionID                       
INNER JOIN  
	CompanyInfo  Com                          
ON 
	Div.numCompanyID = Com.numCompanyId    
LEFT JOIN ListDetails AS listCompanyRating
 ON Com.numCompanyRating=listCompanyRating.NumlistItemID
WHERE
	OP.numDomainID = ' + Cast(@numDomainID as varchar(10))  + ' AND OP.tintOppType = 2 AND OP.tintOppStatus=1 
	AND 1 = (CASE WHEN '+CAST(@decReqPOMinValue AS VARCHAR(100))+'>0 AND Op.monPAmount>'+CAST(@decReqPOMinValue AS VARCHAR(100))+' THEN 1  WHEN '+CAST(@decReqPOMinValue AS VARCHAR(100))+'=0 THEN 1  ELSE 0 END )) AS Q
	WHERE  '+@OtherRegularSearch+' AND  (DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',Q.DueDate) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',Q.DueDate) <= '''+Cast(@endDate as varchar(30))+''') 
'+@IndivRecordPaging+'
'
EXEC (@dynamicQuery)

END

------------------ 9 -  Price Margin Approval ----------------------------

SET @dynamicQuery =  ' INSERT INTO #tempRecords(
RecordCount,
RecordId,
numCreatedBy,
numOrgTerId,
numTaskId,
IsTaskClosed,
TaskTypeName,
TaskType,
OrigDescription,
Description,
dtDueDate,
DueDate,
TotalProgress,
numAssignedTo,
numAssignToId,
Priority,
Activity,
PriorityId,
ActivityId,
OrgName,
CompanyRating
) SELECT COUNT(*) OVER() RecordCount,* FROM (SELECT
OP.numOppId AS RecordId,
OP.numCreatedBy AS numCreatedBy,
ISNULL(Div.numTerId,0) AS numOrgTerId,
0 AS numTaskId,
ISNULL(OP.bitReqPOApproved,0) AS IsTaskClosed,
''<a onclick="OpenMarginApproval(''+CAST(OP.numOppId AS VARCHAR)+'')" href="javascript:void(0)">Price Margin Approval</a>'' As TaskTypeName,
9 AS TaskType,
'''' AS OrigDescription,
''<b>For : </b> <a href="javascript:void(0)"  onclick="openSalesOrder(''+CAST(OP.numOppId AS VARCHAR)+'')">''+OP.vcPOppName+''</a> : '' +dbo.ItemUnitPriceApproval_CheckAndGetItemDetails(OP.numOppId,OP.numDomainId)+''  &nbsp;<i onclick="ApproveRejectPriceMarginApproval(''+CAST(OP.numOppId AS VARCHAR)+'',1)" class="fa fa-thumbs-up cursor"></i>  &nbsp;<i onclick="ApproveRejectPriceMarginApproval(''+CAST(OP.numOppId AS VARCHAR)+'',2)" class="fa fa-thumbs-down cursor"></i>''  AS Description,
OP.bintCreatedDate AS dtDueDate,
OP.bintCreatedDate as DueDate,
0 As TotalProgress,
dbo.fn_GetContactName('+CAST(@numUserCntID AS VARCHAR(200))+') As numAssignedTo,
'+CAST(@numUserCntID AS VARCHAR(200))+' As numAssignToId,
'''' As Priority, -- Priority column   
'''' As Activity ,
0 As PriorityId, -- Priority column   
 0 As ActivityId ,
 ''<a href="../account/frmAccounts.aspx?DivID=''+CAST(Div.numDivisionID AS VARCHAR)+''">''+Com.vcCompanyName+ CASE WHEN  ISNULL(listCompanyRating.VcData,'''')<>'''' THEN ''(''+listCompanyRating.VcData+'')'' ELSE '''' END +''</a>''  AS OrgName,
 listCompanyRating.VcData As CompanyRating

 FROM  
 OpportunityMaster AS OP 
INNER JOIN 
	DivisionMaster Div                           
ON 
	OP.numDivisionId = Div.numDivisionID                       
INNER JOIN  
	CompanyInfo  Com                          
ON 
	Div.numCompanyID = Com.numCompanyId    
LEFT JOIN ListDetails AS listCompanyRating
 ON Com.numCompanyRating=listCompanyRating.NumlistItemID
WHERE
	OP.numDomainID = ' + Cast(@numDomainID as varchar(10))  + ' AND OP.tintOppType = 1 AND (SELECT COUNT(OpportunityItems.numoppitemtCode) FROM OpportunityItems 
						WHERE OpportunityItems.bitItemPriceApprovalRequired=1 AND OpportunityItems.numOppId=OP.numOppId) >0
	AND '+CAST(@numUserCntID AS VARCHAR(100))+' IN (SELECT numUserID FROM UnitPriceApprover WHERE numDomainId= ' + Cast(@numDomainID as varchar(10))  + ' )) AS Q
	WHERE  '+@OtherRegularSearch+' AND  (DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',Q.DueDate) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',Q.DueDate) <= '''+Cast(@endDate as varchar(30))+''') 
'+@IndivRecordPaging+'

'
EXEC (@dynamicQuery)

------------------------10 - A/R Invoice Due------------------------------------
IF(@bitARInvoiceDue=1 AND @numUserCntID IN(SELECT * FROM Split(@vchARInvoiceDue,',') WHERE Items<>''))
BEGIN
 DECLARE  @AuthoritativeSalesBizDocId  AS INTEGER
    SELECT @AuthoritativeSalesBizDocId = isnull(numAuthoritativeSales,0)
    FROM   AuthoritativeBizDocs
    WHERE  numDomainId = @numDomainId;
SET @dynamicQuery=  ' INSERT INTO #tempRecords(
RecordCount,
RecordId,
numCreatedBy,
numOrgTerId,
numTaskId,
IsTaskClosed,
TaskTypeName,
TaskType,
OrigDescription,
Description,
dtDueDate,
DueDate,
TotalProgress,
numAssignedTo,
numAssignToId,
Priority,
Activity,
PriorityId,
ActivityId,
OrgName,
CompanyRating
) SELECT  COUNT(*) OVER() RecordCount,* FROM ( SELECT
OM.numOppId AS RecordId,
OM.numCreatedBy AS numCreatedBy,
ISNULL(DM.numTerId,0) AS numOrgTerId,
OB.numOppBizDocsId AS numTaskId,
0 AS IsTaskClosed,
''<a href="javascript:void(0)"  onclick="openBizDoc(''+CAST(OM.numOppId AS VARCHAR)+'',''+CAST(OB.numOppBizDocsId AS VARCHAR)+'')">A/R Invoice Due</a>'' As TaskTypeName,
10 AS TaskType,
'''' AS OrigDescription,
''<b>For : </b> <a href="javascript:void(0)" class="underLineHyperLink" onclick="openBizDoc(''+CAST(OM.numOppId AS VARCHAR)+'',''+CAST(OB.numOppBizDocsId AS VARCHAR)+'')">''+CAST(OB.[vcBizDocID] AS VARCHAR)+''</a> of <a href="javascript:void(0)" class="underLineHyperLink" onclick="openSalesOrder(''+CAST(OM.numOppId AS VARCHAR)+'')">''+CAST(OM.[vcPOppName] AS VARCHAR)+''</a> <b>Balance Due</b> <a href="javascript:void(0)" onclick="OpenAmtPaid(''+CAST(OM.numOppId AS VARCHAR)+'',''+CAST(OB.numOppBizDocsId AS VARCHAR)+'',''+CAST(OM.[numDivisionId] AS VARCHAR)+'')"   class="underLineHyperLink">'' + CAST(ISNULL(C.[varCurrSymbol],'''') AS VARCHAR) + '''' + CAST(CAST(isnull(OB.monDealAmount  - ISNULL(TablePayments.monPaidAmount,0),0) AS DECIMAL(18,2)) AS VARCHAR)+''</a>''  AS Description,
 CASE ISNULL(bitBillingTerms,0) 
                          WHEN 1 THEN DATEADD(dd,convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0)),OB.dtFromDate)
                          WHEN 0 THEN ob.[dtFromDate]
                        END AS dtDueDate,
 CASE ISNULL(bitBillingTerms,0) 
                          WHEN 1 THEN [dbo].[FormatedDateFromDate](DATEADD(dd,convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0)),OB.dtFromDate),
                                                                   1)
                          WHEN 0 THEN [dbo].[FormatedDateFromDate](ob.[dtFromDate],1)
                        END as DueDate,
0 As TotalProgress,
'''' As numAssignedTo,
0 As numAssignToId,
'''' As Priority, -- Priority column   
'''' As Activity ,
0 As PriorityId, -- Priority column   
 0 As ActivityId ,
 ''<a href="../account/frmAccounts.aspx?DivID=''+CAST(DM.numDivisionID AS VARCHAR)+''">''+Com.vcCompanyName+ CASE WHEN  ISNULL(listCompanyRating.VcData,'''')<>'''' THEN ''(''+listCompanyRating.VcData+'')'' ELSE '''' END +''</a>''  AS OrgName,
 listCompanyRating.VcData As CompanyRating

 FROM   [OpportunityMaster] OM INNER JOIN [DivisionMaster] DM ON OM.[numDivisionId] = DM.[numDivisionID]
				LEFT JOIN CompanyInfo  Com ON DM.numCompanyID = Com.numCompanyId    
				LEFT JOIN ListDetails AS listCompanyRating
 ON Com.numCompanyRating=listCompanyRating.NumlistItemID
               LEFT OUTER JOIN OpportunityBizDocs OB ON OB.numOppId = OM.numOppID
               LEFT OUTER JOIN [Currency] C ON OM.[numCurrencyID] = C.[numCurrencyID]
			   OUTER APPLY
			   (
					SELECT 
						SUM(monAmountPaid) AS monPaidAmount
					FROM
						DepositeDetails DD
					INNER JOIN
						DepositMaster DM
					ON
						DD.numDepositID=DM.numDepositId
					WHERE 
						numOppID = OM.numOppID
						AND numOppBizDocsID=OB.numOppBizDocsID
						AND CONVERT(DATE,DM.dtDepositDate) <= DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +','''+Cast(@endDate as varchar(100))+''')
				) TablePayments
        WHERE  OM.[tintOppType] = 1
               AND OM.[tintOppStatus] = 1
               AND om.[numDomainId] = ' + Cast(@numDomainId as varchar(10))  + '
               AND OB.[numBizDocId] = '+CAST(@AuthoritativeSalesBizDocId As VARCHAR)+'
               AND OB.bitAuthoritativeBizDocs=1
               AND ISNULL(OB.tintDeferred,0) <> 1
			   AND (OM.numAccountClass= 0 OR 0=0)
			   AND CONVERT(DATE,OB.[dtCreatedDate]) <= DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +','''+Cast(@endDate as varchar(100))+''')
               AND isnull(OB.monDealAmount
                            ,0) > 0  AND isnull(OB.monDealAmount * OM.fltExchangeRate
                        - ISNULL(TablePayments.monPaidAmount,0) * OM.fltExchangeRate,0) != 0
	) AS Q
	WHERE  '+@OtherRegularSearch+' AND  (DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',Q.DueDate) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',Q.DueDate) <= '''+Cast(@endDate as varchar(30))+''') 
'+@IndivRecordPaging+'
'
EXEC (@dynamicQuery)
END
------------------------11 - A/P Bill Due------------------------------------
IF(@bitAPBillsDue=1 AND @numUserCntID IN(SELECT * FROM Split(@vchAPBillsDue,',') WHERE Items<>''))
BEGIN   
   DECLARE  @AuthoritativePurchaseBizDocId  AS INTEGER
    SELECT  @AuthoritativePurchaseBizDocId= isnull(numAuthoritativePurchase,0)
    FROM   AuthoritativeBizDocs
    WHERE  numDomainId =  @numDomainId
	

SET @dynamicQuery = '  INSERT INTO #tempRecords(
RecordCount,
RecordId,
numCreatedBy,
numOrgTerId,
numTaskId,
IsTaskClosed,
TaskTypeName,
TaskType,
OrigDescription,
Description,
dtDueDate,
DueDate,
TotalProgress,
numAssignedTo,
numAssignToId,
Priority,
Activity,
PriorityId,
ActivityId,
OrgName,
CompanyRating
) SELECT  COUNT(*) OVER() RecordCount,* FROM (SELECT
OM.numOppId AS RecordId,
OM.numCreatedBy AS numCreatedBy,
ISNULL(DM.numTerId,0) AS numOrgTerId,
OB.numOppBizDocsId AS numTaskId,
0 AS IsTaskClosed,
''<a href="javascript:void(0)" onclick="openPurchaseBill(''+CAST(DM.[numDivisionID] As VARCHAR)+'')">A/P Bill Due<a/>'' As TaskTypeName,
11 AS TaskType,
'''' AS OrigDescription,
''<b>For : </b> <a href="javascript:void(0)" class="underLineHyperLink" onclick="openBizDoc(''+CAST(OM.numOppId AS VARCHAR)+'',''+CAST(OB.numOppBizDocsId AS VARCHAR)+'')">''+CAST(OB.[vcBizDocID] AS VARCHAR)+''</a> of <a href="javascript:void(0)" class="underLineHyperLink" onclick="openSalesOrder(''+CAST(OM.numOppId AS VARCHAR)+'')">''+CAST(OM.[vcPOppName] AS VARCHAR)+''</a> <b>Balance Due : </b> <a href="javascript:void(0)" onclick="openBizDoc(''+CAST(OM.numOppId AS VARCHAR)+'',''+CAST(OB.numOppBizDocsId AS VARCHAR)+'')"   class="underLineHyperLink">'' + CAST(ISNULL(C.[varCurrSymbol],'''') AS VARCHAR) + '''' + CAST(CAST(isnull((OB.monDealAmount * OM.fltExchangeRate)  - (ISNULL(TablePayment.monPaidAmount,0)* OM.fltExchangeRate),0) AS DECIMAL(18,2))  AS VARCHAR)+''</a>''  AS Description,
 CASE ISNULL(bitBillingTerms,0) 
                          WHEN 1 THEN DATEADD(dd,convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0)),OB.dtFromDate)
                          WHEN 0 THEN ob.[dtFromDate]
                        END AS dtDueDate,
 CASE ISNULL(bitBillingTerms,0) 
                          WHEN 1 THEN [dbo].[FormatedDateFromDate](DATEADD(dd,convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0)),OB.dtFromDate),
                                                                   1)
                          WHEN 0 THEN [dbo].[FormatedDateFromDate](ob.[dtFromDate],1)
                        END as DueDate,
0 As TotalProgress,
'''' As numAssignedTo,
0 As numAssignToId,
'''' As Priority, -- Priority column   
'''' As Activity ,
0 As PriorityId, -- Priority column   
 0 As ActivityId ,
 ''<a href="../account/frmAccounts.aspx?DivID=''+CAST(DM.numDivisionID AS VARCHAR)+''">''+Com.vcCompanyName+ CASE WHEN  ISNULL(listCompanyRating.VcData,'''')<>'''' THEN ''(''+listCompanyRating.VcData+'')'' ELSE '''' END +''</a>''  AS OrgName,
 listCompanyRating.VcData As CompanyRating

   FROM   [OpportunityMaster] OM
               INNER JOIN [DivisionMaster] DM
                 ON OM.[numDivisionId] = DM.[numDivisionID]
				 LEFT JOIN CompanyInfo  Com ON DM.numCompanyID = Com.numCompanyId    
				LEFT JOIN ListDetails AS listCompanyRating
 ON Com.numCompanyRating=listCompanyRating.NumlistItemID
               LEFT OUTER JOIN OpportunityBizDocs OB
                 ON OB.numOppId = OM.numOppID
               LEFT OUTER JOIN [Currency] C
                 ON OM.[numCurrencyID] = C.[numCurrencyID]
				 OUTER APPLY
				(
					SELECT
						SUm(BPD.monAmount) monPaidAmount
					FROM	
						BillPaymentDetails BPD
					INNER JOIN
						BillPaymentHeader BPH
					ON
						BPD.numBillPaymentID=BPH.numBillPaymentID
					WHERE
						BPD.numOppBizDocsID = OB.numOppBizDocsId
						AND CAST(BPH.dtPaymentDate AS DATE) <= DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +','''+Cast(@endDate as varchar(100))+''')
				) TablePayment
        WHERE  OM.[tintOppType] = 2
               AND OM.[tintOppStatus] = 1
               AND om.[numDomainId] = 1
               AND OB.[numBizDocId] = '+CAST(@AuthoritativePurchaseBizDocId AS VARCHAR)+' 
               AND OB.bitAuthoritativeBizDocs=1
               AND ISNULL(OB.tintDeferred,0) <> 1
			   AND (OM.numAccountClass= 0 OR 0=0)
			   AND CONVERT(DATE,OB.[dtCreatedDate]) <= DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +','''+Cast(@endDate as varchar(100))+''')
               AND  isnull(OB.monDealAmount * OM.fltExchangeRate,0) > 0 
			   AND isnull(OB.monDealAmount * OM.fltExchangeRate
                        - ISNULL(TablePayment.monPaidAmount,0) * OM.fltExchangeRate,0) > 0
	) AS Q
	WHERE  '+@OtherRegularSearch+' AND  (DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',Q.DueDate) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',Q.DueDate) <= '''+Cast(@endDate as varchar(30))+''') 
'+@IndivRecordPaging+'
'

EXEC (@dynamicQuery)
END

END
--SELECT DueDate FROM #tempRecords
SET @FormattedItems =''
DECLARE @startEndDate AS VARCHAR(MAX)='  CONCAT(''<span style="color:#909090;font-size:23px;font-weight:bold">'',(SELECT FORMAT(CAST(DueDate AS DATE), ''MMM-dd-yyyy'')),CASE WHEN Startdate IS NOT NULL AND EndTime IS NOT NULL THEN  '' @ ''+LEFT(RIGHT(''0''+LTRIM(RIGHT(CONVERT(varchar,Startdate,100),8)),7),PATINDEX(''%[a,p]m'',RIGHT(''0''+LTRIM(RIGHT(CONVERT(varchar,Startdate,100),8)),7))-1) WHEN Startdate IS NOT NULL AND EndTime IS NULL THEN  '' @ ''+RIGHT(''0''+LTRIM(RIGHT(CONVERT(varchar,Startdate,100),8)),7) ELSE '''' END,CASE WHEN Startdate IS NOT NULL AND EndTime IS NOT NULL THEN  ''<Span style="font-size:20px;font-weight:normal;font-style: italic;"><br/>to ''+RIGHT(''0''+LTRIM(RIGHT(CONVERT(varchar,EndTime,100),8)),7)+''</Span>'' ELSE '''' END,''</span>'') END '
SET @FormattedItems=' UPDATE #tempRecords SET DueDate = 
CASE 
WHEN convert(varchar(11),DueDate)=convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',getdate())) then CONCAT(''<b><font color="#FF0000" style="font-size:23px">Today'',CASE WHEN Startdate IS NOT NULL AND EndTime IS NOT NULL THEN  '' @ ''+LEFT(RIGHT(''0''+LTRIM(RIGHT(CONVERT(varchar,Startdate,100),8)),7),PATINDEX(''%[a,p]m'',RIGHT(''0''+LTRIM(RIGHT(CONVERT(varchar,Startdate,100),8)),7))-1) WHEN Startdate IS NOT NULL AND EndTime IS NULL THEN  '' @ ''+RIGHT(''0''+LTRIM(RIGHT(CONVERT(varchar,Startdate,100),8)),7) ELSE '''' END,CASE WHEN Startdate IS NOT NULL AND EndTime IS NOT NULL THEN  ''<Span style="font-size:20px;font-weight:normal;font-style: italic;"><br/>to ''+RIGHT(''0''+LTRIM(RIGHT(CONVERT(varchar,EndTime,100),8)),7)+''</Span>'' ELSE '''' END ,''</font></b>'') 
WHEN convert(varchar(11),DueDate)=convert(varchar(11),DateAdd(day,1,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',getdate()))) then CONCAT(''<b><font color=#ED8F11 style="font-size:23px">Tommorow'',CASE WHEN Startdate IS NOT NULL AND EndTime IS NOT NULL THEN  '' @ ''+LEFT(RIGHT(''0''+LTRIM(RIGHT(CONVERT(varchar,Startdate,100),8)),7),PATINDEX(''%[a,p]m'',RIGHT(''0''+LTRIM(RIGHT(CONVERT(varchar,Startdate,100),8)),7))-1) WHEN Startdate IS NOT NULL AND EndTime IS NULL THEN  '' @ ''+RIGHT(''0''+LTRIM(RIGHT(CONVERT(varchar,Startdate,100),8)),7) ELSE '''' END,CASE WHEN Startdate IS NOT NULL AND EndTime IS NOT NULL THEN  ''<Span style="font-size:20px;font-weight:normal;font-style: italic;"><br/>to ''+RIGHT(''0''+LTRIM(RIGHT(CONVERT(varchar,EndTime,100),8)),7)+''</Span>'' ELSE '''' END ,''</font></b>'') 
WHEN convert(varchar(11),DueDate)=convert(varchar(11),DateAdd(day,2,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',getdate()))) then CONCAT(''<b><font color=#8FAADC style="font-size:23px">'',datename(dw,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',DueDate)),''<span style="color:#000;font-weight:500"> - '',CAST(DAY(Startdate) AS VARCHAR(10)),CASE
	WHEN DAY(Startdate) % 100 IN (11, 12, 13) THEN ''th''
	WHEN DAY(Startdate) % 10 = 1 THEN ''st''
	WHEN DAY(Startdate) % 10 = 2 THEN ''nd''
	WHEN DAY(Startdate) % 10 = 3 THEN ''rd''
ELSE ''th'' END,''</span>'',''</font></b>'',''<b>'',''<br/>''
,''<Span style="font-size:20px;font-weight:normal;font-style: italic;">'',CASE WHEN Startdate IS NOT NULL AND EndTime IS NOT NULL THEN  LEFT(RIGHT(''0''+LTRIM(RIGHT(CONVERT(varchar,Startdate,100),8)),7),PATINDEX(''%[a,p]m'',RIGHT(''0''+LTRIM(RIGHT(CONVERT(varchar,Startdate,100),8)),7))-1) WHEN Startdate IS NOT NULL AND EndTime IS NULL THEN  RIGHT(''0''+LTRIM(RIGHT(CONVERT(varchar,Startdate,100),8)),7) ELSE '''' END,''</span>''
,CASE WHEN Startdate IS NOT NULL AND EndTime IS NOT NULL THEN  ''<Span style="font-size:20px;font-weight:normal;font-style: italic;"> to ''+RIGHT(''0''+LTRIM(RIGHT(CONVERT(varchar,EndTime,100),8)),7)+''</Span>'' ELSE '''' END ,''</b>'')
WHEN convert(varchar(11),DueDate)=convert(varchar(11),DateAdd(day,3,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',getdate()))) then CONCAT(''<b><font color=#CC99FF style="font-size:23px">'',datename(dw,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',DueDate)),''<span style="color:#000;font-weight:500"> - '',CAST(DAY(Startdate) AS VARCHAR(10)),CASE
	WHEN DAY(Startdate) % 100 IN (11, 12, 13) THEN ''th''
	WHEN DAY(Startdate) % 10 = 1 THEN ''st''
	WHEN DAY(Startdate) % 10 = 2 THEN ''nd''
	WHEN DAY(Startdate) % 10 = 3 THEN ''rd''
ELSE ''th'' END,''</span>'',''</font></b>'',''<b>'',''<br/>''
,''<Span style="font-size:20px;font-weight:normal;font-style: italic;">'',CASE WHEN Startdate IS NOT NULL AND EndTime IS NOT NULL THEN  LEFT(RIGHT(''0''+LTRIM(RIGHT(CONVERT(varchar,Startdate,100),8)),7),PATINDEX(''%[a,p]m'',RIGHT(''0''+LTRIM(RIGHT(CONVERT(varchar,Startdate,100),8)),7))-1) WHEN Startdate IS NOT NULL AND EndTime IS NULL THEN  RIGHT(''0''+LTRIM(RIGHT(CONVERT(varchar,Startdate,100),8)),7) ELSE '''' END,''</span>''
,CASE WHEN Startdate IS NOT NULL AND EndTime IS NOT NULL THEN  ''<Span style="font-size:20px;font-weight:normal;font-style: italic;"> to ''+RIGHT(''0''+LTRIM(RIGHT(CONVERT(varchar,EndTime,100),8)),7)+''</Span>'' ELSE '''' END ,''</b>'')
WHEN convert(varchar(11),DueDate)=convert(varchar(11),DateAdd(day,4,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',getdate()))) then CONCAT(''<b><font color=#AED495 style="font-size:23px">'',datename(dw,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',DueDate)),''<span style="color:#000;font-weight:500"> - '',CAST(DAY(Startdate) AS VARCHAR(10)),CASE
	WHEN DAY(Startdate) % 100 IN (11, 12, 13) THEN ''th''
	WHEN DAY(Startdate) % 10 = 1 THEN ''st''
	WHEN DAY(Startdate) % 10 = 2 THEN ''nd''
	WHEN DAY(Startdate) % 10 = 3 THEN ''rd''
ELSE ''th'' END,''</span>'',''</font></b>'',''<b>'',''<br/>''
,''<Span style="font-size:20px;font-weight:normal;font-style: italic;">'',CASE WHEN Startdate IS NOT NULL AND EndTime IS NOT NULL THEN  LEFT(RIGHT(''0''+LTRIM(RIGHT(CONVERT(varchar,Startdate,100),8)),7),PATINDEX(''%[a,p]m'',RIGHT(''0''+LTRIM(RIGHT(CONVERT(varchar,Startdate,100),8)),7))-1) WHEN Startdate IS NOT NULL AND EndTime IS NULL THEN  RIGHT(''0''+LTRIM(RIGHT(CONVERT(varchar,Startdate,100),8)),7) ELSE '''' END,''</span>''
,CASE WHEN Startdate IS NOT NULL AND EndTime IS NOT NULL THEN  ''<Span style="font-size:20px;font-weight:normal;font-style: italic;"> to ''+RIGHT(''0''+LTRIM(RIGHT(CONVERT(varchar,EndTime,100),8)),7)+''</Span>'' ELSE '''' END ,''</b>'')
WHEN convert(varchar(11),DueDate)=convert(varchar(11),DateAdd(day,5,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',getdate()))) then CONCAT(''<b><font color=#72DFDC style="font-size:23px">'',datename(dw,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',DueDate)),''<span style="color:#000;font-weight:500"> - '',CAST(DAY(Startdate) AS VARCHAR(10)),CASE
	WHEN DAY(Startdate) % 100 IN (11, 12, 13) THEN ''th''
	WHEN DAY(Startdate) % 10 = 1 THEN ''st''
	WHEN DAY(Startdate) % 10 = 2 THEN ''nd''
	WHEN DAY(Startdate) % 10 = 3 THEN ''rd''
ELSE ''th'' END,''</span>'',''</font></b>'',''<b>'',''<br/>''
,''<Span style="font-size:20px;font-weight:normal;font-style: italic;">'',CASE WHEN Startdate IS NOT NULL AND EndTime IS NOT NULL THEN  LEFT(RIGHT(''0''+LTRIM(RIGHT(CONVERT(varchar,Startdate,100),8)),7),PATINDEX(''%[a,p]m'',RIGHT(''0''+LTRIM(RIGHT(CONVERT(varchar,Startdate,100),8)),7))-1) WHEN Startdate IS NOT NULL AND EndTime IS NULL THEN  RIGHT(''0''+LTRIM(RIGHT(CONVERT(varchar,Startdate,100),8)),7) ELSE '''' END,''</span>''
,CASE WHEN Startdate IS NOT NULL AND EndTime IS NOT NULL THEN  ''<Span style="font-size:20px;font-weight:normal;font-style: italic;"> to ''+RIGHT(''0''+LTRIM(RIGHT(CONVERT(varchar,EndTime,100),8)),7)+''</Span>'' ELSE '''' END ,''</b>'')
WHEN convert(varchar(11),DueDate)=convert(varchar(11),DateAdd(day,6,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',getdate()))) then CONCAT(''<b><font color=#FF9999 style="font-size:23px">'',datename(dw,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',DueDate)),''<span style="color:#000;font-weight:500"> - '',CAST(DAY(Startdate) AS VARCHAR(10)),CASE
	WHEN DAY(Startdate) % 100 IN (11, 12, 13) THEN ''th''
	WHEN DAY(Startdate) % 10 = 1 THEN ''st''
	WHEN DAY(Startdate) % 10 = 2 THEN ''nd''
	WHEN DAY(Startdate) % 10 = 3 THEN ''rd''
ELSE ''th'' END,''</span>'',''</font></b>'',''<b>'',''<br/>''
,''<Span style="font-size:20px;font-weight:normal;font-style: italic;">'',CASE WHEN Startdate IS NOT NULL AND EndTime IS NOT NULL THEN  LEFT(RIGHT(''0''+LTRIM(RIGHT(CONVERT(varchar,Startdate,100),8)),7),PATINDEX(''%[a,p]m'',RIGHT(''0''+LTRIM(RIGHT(CONVERT(varchar,Startdate,100),8)),7))-1) WHEN Startdate IS NOT NULL AND EndTime IS NULL THEN  RIGHT(''0''+LTRIM(RIGHT(CONVERT(varchar,Startdate,100),8)),7) ELSE '''' END,''</span>''
,CASE WHEN Startdate IS NOT NULL AND EndTime IS NOT NULL THEN  ''<Span style="font-size:20px;font-weight:normal;font-style: italic;"> to ''+RIGHT(''0''+LTRIM(RIGHT(CONVERT(varchar,EndTime,100),8)),7)+''</Span>'' ELSE '''' END ,''</b>'')
ELSE   '
 EXEC (@FormattedItems+@startEndDate)
 
 DECLARE @strSql3 VARCHAR(MAX)=''
 IF LEN(@columnName) > 0
BEGIN
	SET @strSql3= CONCAT(@strSql3,' order by ',CASE WHEN @columnName = 'Action-Item Participants' THEN CONCAT('[','Action-Item Participants',']') ELSE @columnName END,' ',@columnSortOrder)
END
ELSE
BEGIN
	SET @strSql3= CONCAT(@strSql3,' ORDER BY dtDueDate DESC  ')
END
SET @TotalRecordCount=(SELECT ISNULL(SUM(DISTINCT RecordCount),0) FROM #tempRecords)


SET @dynamicQuery = 'SELECT '+CAST(@TotalRecordCount AS varchar(400))+' AS TotalRecords,T.*
 FROM #tempRecords T WHERE 1=1 '+@strSql3


EXEC(@dynamicQuery)
DECLARE @TempColumns TABLE
	(
        vcFieldName VARCHAR(200)
        ,vcDbColumnName VARCHAR(200)
        ,vcOrigDbColumnName VARCHAR(200)
        ,bitAllowSorting BIT
        ,numFieldId INT
        ,bitAllowEdit BIT
        ,bitCustomField BIT
        ,vcAssociatedControlType VARCHAR(50)
        ,vcListItemType  VARCHAR(200)
        ,numListID INT
        ,ListRelID INT
        ,vcLookBackTableName VARCHAR(200)
        ,bitAllowFiltering BIT
        ,vcFieldDataType  VARCHAR(20)
		,intColumnWidth INT
		,bitClosedColumn BIT
		,bitFieldMessage BIT DEFAULT 0
		,vcFieldMessage VARCHAR(500)
		,bitIsRequired BIT DEFAULT 0
		,bitIsNumeric BIT DEFAULT 0
		,bitIsAlphaNumeric BIT DEFAULT 0
		,bitIsEmail BIT DEFAULT 0
		,bitIsLengthValidation  BIT DEFAULT 0
		,intMaxLength INT DEFAULT 0
		,intMinLength INT DEFAULT 0
	)



		declare @Nocolumns as tinyint         
set @Nocolumns=0       
         
Select @Nocolumns=isnull(count(*),0) from View_DynamicColumns where numFormId=43 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 
  
  
if @Nocolumns > 0            
begin      
INSERT INTO #tempForm
select tintRow+1 as tintOrder,vcDbColumnName,vcFieldDataType,vcOrigDbColumnName,bitAllowFiltering,ISNULL(vcCultureFieldName,vcFieldName),                  
 vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName                                                 
,bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth
 FROM View_DynamicColumns 
 where numFormId=43 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 
  AND ISNULL(bitSettingField,0)=1 AND ISNULL(bitCustom,0)=0
  order by tintOrder asc  
  
end            
else            
begin 

	--Check if Master Form Configuration is created by administrator if NoColumns=0
	DECLARE @IsMasterConfAvailable BIT = 0
	DECLARE @numUserGroup NUMERIC(18,0)

	SELECT @numUserGroup = numGroupID FROM UserMaster WHERE numUserDetailId = @numUserCntID 

	IF (SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = @numDomainID AND numFormID = 43 AND bitGridConfiguration = 1 AND numGroupID = @numUserGroup) > 0
	BEGIN
		SET @IsMasterConfAvailable = 1
	END

	--If MasterConfiguration is available then load it otherwise load default columns
	IF @IsMasterConfAvailable = 1
	BEGIN
		INSERT INTO DycFormConfigurationDetails 
		(
			numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth
		)
		SELECT 
			43,numFieldId,intColumnNum,intRowNum,@numDomainID,@numUserCntID,NULL,1,0,intColumnWidth
		FROM 
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId=43 AND 
			View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
			ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0

		INSERT INTO #tempForm
		SELECT 
			(intRowNum + 1) as tintOrder, vcDbColumnName,vcFieldDataType,vcOrigDbColumnName,bitAllowFiltering, ISNULL(vcCultureFieldName,vcFieldName), vcAssociatedControlType,
			vcListItemType,numListID,vcLookBackTableName, bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth
		FROM 
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId=43 AND 
			View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
			ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
	ORDER BY 
		tintOrder ASC  
	END
	ELSE                                        
	BEGIN
		 INSERT INTO #tempForm
		select tintOrder,vcDbColumnName,vcFieldDataType
		,vcOrigDbColumnName,bitAllowFiltering,ISNULL(vcCultureFieldName,vcFieldName),                  
		 vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName                                                 
		,bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
		bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
		intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth
		 FROM View_DynamicDefaultColumns
		 where numFormId=43 and bitDefault=1 AND ISNULL(bitSettingField,0)=1 AND numDomainID=@numDomainID
		order by tintOrder asc  
	END 
enD

DELETE FROM #tempForm WHERE vcOrigDbColumnName IN('numActivity','numStatus','intTotalProgress','textDetails','vcCompanyName','bitTask','numAssignedTo','FormattedDate')
--UPDATE #tempForm SET bitCustomField=1
INSERT INTO #tempForm
		(
			tintOrder
			,vcFieldName
			,vcDbColumnName
			,vcOrigDbColumnName
			,bitAllowSorting
			,numFieldId
			,bitAllowEdit
			,bitCustomField
			,vcAssociatedControlType
			,vcListItemType
			,numListID
			,ListRelID
			,vcLookBackTableName
			,bitAllowFiltering
			,vcFieldDataType
			
		)
		VALUES
		(-9,'Type','TaskTypeName','TaskTypeName',0,1,0,0,'SelectBox','',0,0,'T',1,'V')
		,(-8,'Descriptions & Comments','Description','Description',0,184,1,0,'TextArea','',0,0,'T',1,'V')
		,(-7,'Due Date','DueDate','DueDate',1,3,0,0,'DateField','',0,0,'T',1,'V')
		,(-6,'Total Progress','TotalProgress','TotalProgress',1,4,0,0,'TextBox','',0,0,'T',0,'V')
		,(-5,'Assigned to','numAssignedTo','numAssignedTo',0,5,0,0,'SelectBox','',0,0,'T',1,'V')
		,(-4,'Priority','Priority','Priority',0,183,1,0,'SelectBox','',0,0,'T',1,'V')
		,(-3,'Activity','Activity','Activity',0,182,1,0,'SelectBox','',0,0,'T',1,'V')
		,(-2,'Organization (Rating)','OrgName','OrgName',0,8,0,0,'TextBox','',0,0,'T',1,'V')
UPDATE
		TC
	SET
		TC.intColumnWidth = ISNULL(DFCD.intColumnWidth,0)
	FROM
		#tempForm TC
	INNER JOIN
		DycFormConfigurationDetails DFCD
	ON
		TC.numFieldId = DFCD.numFieldId
	WHERE
		DFCD.numDomainId = @numDomainID
		AND DFCD.numUserCntID = @numUserCntID
		AND DFCD.numFormId = 43
		AND DFCD.tintPageType = 1 


	SELECT * FROM #tempForm order by tintOrder asc  
END
GO
/****** Object:  StoredProcedure [dbo].[USP_CaseDetails]    Script Date: 07/26/2008 16:15:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON                     
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_UpdateTimeContract')
DROP PROCEDURE usp_UpdateTimeContract
GO
CREATE PROCEDURE [dbo].[usp_UpdateTimeContract]  
@numDomainID as numeric(9),
@numDivisonId as numeric(9) ,    
@numSecounds as numeric(9) ,  
@intType AS INT =0 ,
@numTaskID AS numeric(9),
@outPut AS INT  = 0 OUTPUT,
@balanceContractTime AS BIGINT=0  OUTPUT,
@isProjectActivityEmail AS INT
AS
BEGIN
	IF EXISTS(SELECT * FROM Contracts WHERE numDivisonId=@numDivisonId AND numDomainId=@numDomainID AND intType=1)
	BEGIN
		
		--@intType = 1
		IF(@intType=1)
		BEGIN
			SET @outPut=3
				IF(@isProjectActivityEmail=1)
				BEGIN
					UPDATE StagePercentageDetailsTask SET bitTimeAddedToContract=0 WHERE numTaskId=@numTaskID
				END
				IF(@isProjectActivityEmail=2)
				BEGIN
					UPDATE Communication SET bitTimeAddedToContract=0 WHERE numCommId=@numTaskID
				END
				
				IF(@isProjectActivityEmail=3)
				BEGIN
					UPDATE Activity SET bitTimeAddedToContract=0 WHERE ActivityID=@numTaskID
				END

				IF @isProjectActivityEmail <> 1 --PROJECTS TAsKS TIME IS MANAGED THROUGH TRIGGER ON StagePercentageDetailsTaskTimeLog TABLE
				BEGIN
					UPDATE Contracts SET timeUsed=timeUsed-@numSecounds,dtmModifiedOn=GETUTCDATE() WHERE numDivisonId=@numDivisonId AND numDomainId=@numDomainID
					DELETE FROM ContractsLog WHERE numDivisionID=@numDivisonId  AND numReferenceId=@numTaskID
					AND numContractId=(SELECT TOP 1 numContractId FRoM Contracts WHERE numDivisonId=@numDivisonId AND numDomainId=@numDomainID)
				END
		END
		--@intType = 2
		IF(@intType=2)
		BEGIN			
			IF((SELECT ISNULL((ISNULL(timeLeft,0)-ISNULL(timeUsed,0)),0) FROM Contracts WHERE numDivisonId=@numDivisonId AND numDomainId=@numDomainID AND intType=1)<@numSecounds)
			BEGIN
				SET @outPut=1 --Time is not available
			END
			ELSE
			BEGIN
				SET @outPut=2 --Time is available
				IF(@isProjectActivityEmail=1)
				BEGIN
					UPDATE StagePercentageDetailsTask SET bitTimeAddedToContract=1 WHERE numTaskId=@numTaskID
				END
				IF(@isProjectActivityEmail=2)
				BEGIN
					UPDATE Communication SET bitTimeAddedToContract=1 WHERE numCommId=@numTaskID
				END
				IF(@isProjectActivityEmail=3)
				BEGIN
					UPDATE Activity SET bitTimeAddedToContract=1 WHERE ActivityID=@numTaskID
				END

				IF @isProjectActivityEmail <> 1 --PROJECTS TAsKS TIME IS MANAGED THROUGH TRIGGER ON StagePercentageDetailsTaskTimeLog TABLE
				BEGIN
					UPDATE Contracts SET timeUsed=timeUsed+@numSecounds,dtmModifiedOn=GETUTCDATE() WHERE numDivisonId=@numDivisonId AND numDomainId=@numDomainID
					SET @balanceContractTime = (SELECT ISNULL((ISNULL(timeLeft,0)-ISNULL(timeUsed,0)),0) FROM Contracts WHERE numDivisonId=@numDivisonId AND numDomainId=@numDomainID AND intType=1)
					INSERT INTO ContractsLog (
						intType, numDivisionID, numReferenceId, dtmCreatedOn, numCreatedBy,vcDetails,numUsedTime,numBalance,numContractId,tintRecordType
					)
					SELECT TOP 1
						1,@numDivisonId,@numTaskID,GETUTCDATE(),0,@isProjectActivityEmail,@numSecounds,@balanceContractTime,numContractId,@isProjectActivityEmail
					FROM Contracts WHERE numDivisonId=@numDivisonId AND numDomainId=@numDomainID AND intType=1 ORDER BY numContractId DESC
				END
			END
		END
	END
	ELSE
	BEGIN
		SET @outPut=0
	END

	SET @balanceContractTime = (SELECT TOP 1 ((ISNULL(C.numHours,0) * 60 * 60) + (ISNULL(C.numMinutes,0) * 60)) - ISNULL((SELECT SUM(CLInner.numUsedTime) FROM ContractsLog CLInner WHERE CLInner.numContractId=C.numContractId),0) FROM Contracts C WHERE numDivisonId=@numDivisonId AND numDomainId=@numDomainID AND intType=1 ORDER BY numContractId DESC)
END
