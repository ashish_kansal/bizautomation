/******************************************************************
Project: Release 13.7 Date: 20.JUN.2020
Comments: ALTER SCRIPTS
*******************************************************************/

/******************************************** PRASHANT *********************************************/

BEGIN TRY
BEGIN TRANSACTION

	DECLARE @numMAXPageNavID INT
	SELECT @numMAXPageNavID = MAX(numPageNavID) FROM PageNavigationDTL


	INSERT INTO PageNavigationDTL
	(
		numPageNavID,numModuleID,numParentID,vcPageNavName,vcNavURL,bitVisible,numTabID,vcAddURL,bitAddIsPopUp
	)
	VALUES
	(
		(@numMAXPageNavID + 1),(SELECT TOP 1 numModuleID FROM ModuleMaster WHERE vcModuleName='Contract Management'),(SELECT numPageNavID FROM PageNavigationDTL WHERE vcPageNavName='Contracts'),'Incident','../ContractManagement/frmContractListv2.aspx?type=3',1,(SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Contracts' AND numDomainID=1),'../ContractManagement/frmMngContract.aspx?type=3',1
	)

	INSERT INTO dbo.TreeNavigationAuthorization
	(
		numGroupID,
		numTabID,
		numPageNavID,
		bitVisible,
		numDomainID,
		tintType
	)
	SELECT  
		numGroupID,
		(SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Contracts' AND numDomainID=1),
		(@numMAXPageNavID + 1),
		1,
		numDomainID,
		1
	FROM    
		AuthenticationGroupMaster
	WHERE
		vcGroupName='System Administrator'
COMMIT
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		ERROR_MESSAGE(),
		ERROR_NUMBER(),
		ERROR_SEVERITY(),
		ERROR_STATE(),
		ERROR_LINE(),
		ISNULL(ERROR_PROCEDURE(), '-');
END CATCH
-----------------------------------------------------------------
BEGIN TRY
BEGIN TRANSACTION

	DECLARE @numMAXPageNavID INT
	SELECT @numMAXPageNavID = MAX(numPageNavID) FROM PageNavigationDTL


	INSERT INTO PageNavigationDTL
	(
		numPageNavID,numModuleID,numParentID,vcPageNavName,vcNavURL,bitVisible,numTabID,vcAddURL,bitAddIsPopUp
	)
	VALUES
	(
		(@numMAXPageNavID + 1),(SELECT TOP 1 numModuleID FROM ModuleMaster WHERE vcModuleName='Contract Management'),(SELECT numPageNavID FROM PageNavigationDTL WHERE vcPageNavName='Contracts'),'Time','../ContractManagement/frmContractListv2.aspx?type=1',1,(SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Contracts' AND numDomainID=1),'../ContractManagement/frmMngContract.aspx?type=1',1
	)

	INSERT INTO dbo.TreeNavigationAuthorization
	(
		numGroupID,
		numTabID,
		numPageNavID,
		bitVisible,
		numDomainID,
		tintType
	)
	SELECT  
		numGroupID,
		(SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Contracts' AND numDomainID=1),
		(@numMAXPageNavID + 1),
		1,
		numDomainID,
		1
	FROM    
		AuthenticationGroupMaster
	WHERE
		vcGroupName='System Administrator'
COMMIT
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		ERROR_MESSAGE(),
		ERROR_NUMBER(),
		ERROR_SEVERITY(),
		ERROR_STATE(),
		ERROR_LINE(),
		ISNULL(ERROR_PROCEDURE(), '-');
END CATCH

---------------------------------------------------------------------------------------------------

UPDATE PageNavigationDTL SET vcNavURL='#',bitAddIsPopUp=0,vcAddURL=NULL  WHERE vcPageNavName='Contracts'
UPDATE TabMaster SET vcURL='#',bitAddIsPopUp=0,vcAddURL=NULL  WHERE numTabName='Contracts' AND tintTabType=1

INSERT  INTO dbo.NameTemplate
(
	numDomainID,vcModuleName,vcNameTemplate,tintModuleID,numSequenceId,numMinLength,numRecordID
)
SELECT 
	numDomainId,'Incidents','Incident-',1,1,4,6
FROM   
	Domain
ALTER TABLE Activity ADD bitTimeAddedToContract BIT DEFAULT 0
ALTER TABLE Communication ADD bitTimeAddedToContract BIT DEFAULT 0
ALTER TABLE StagePercentageDetailsTask ADD bitTimeAddedToContract BIT DEFAULT 0
CREATE TABLE [dbo].[ContractsLog](
	[numContractsLogId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[intType] [int] NULL,
	[numDivisionID] [numeric](18, 0) NULL,
	[numReferenceId] [numeric](18, 0) NULL,
	[dtmCreatedOn] [datetime] NULL,
	[numCreatedBy] [numeric](18, 0) NULL,
 CONSTRAINT [PK_ContractsLog] PRIMARY KEY CLUSTERED 
(
	[numContractsLogId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
--------------------------------

CREATE TABLE [dbo].[Contracts](
	[numContractId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[intType] [int] NULL,
	[numDomainId] [numeric](18, 0) NOT NULL,
	[numDivisonId] [numeric](18, 0) NULL,
	[numIncidents] [numeric](18, 0) NULL,
	[numIncidentLeft] [numeric](18, 0) NULL,
	[numHours] [numeric](18, 2) NULL,
	[numMinutes] [numeric](18, 2) NULL,
	[timeLeft] [numeric](18, 0) NULL,
	[vcItemClassification] [varchar](max) NULL,
	[numWarrantyDays] [numeric](18, 0) NULL,
	[vcNotes] [varchar](max) NULL,
	[vcContractNo] [varchar](500) NULL,
 CONSTRAINT [PK_Contracts] PRIMARY KEY CLUSTERED 
(
	[numContractId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [dbo].[Contracts] ADD  CONSTRAINT [DF_Contracts_intType]  DEFAULT ((1)) FOR [intType]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1-Contract,2-Warranty' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Contracts', @level2type=N'COLUMN',@level2name=N'intType'
GO

--------------------------------------------------

ALTER TABLE Contracts ADD numIncidentsUsed NUMERIC(18,2)

-----------------------------------------------------
BEGIN TRY
BEGIN TRANSACTION

	DECLARE @numMAXPageNavID INT
	SELECT @numMAXPageNavID = MAX(numPageNavID) FROM PageNavigationDTL


	INSERT INTO PageNavigationDTL
	(
		numPageNavID,numModuleID,numParentID,vcPageNavName,vcNavURL,bitVisible,numTabID,vcAddURL,bitAddIsPopUp
	)
	VALUES
	(
		(@numMAXPageNavID + 1),(SELECT TOP 1 numModuleID FROM ModuleMaster WHERE vcModuleName='Contract Management'),(SELECT numPageNavID FROM PageNavigationDTL WHERE vcPageNavName='Contracts'),'Warranty','../ContractManagement/frmContractListv2.aspx?type=2',1,(SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Contracts' AND numDomainID=1),'../ContractManagement/frmMngContract.aspx?type=2',1
	)

	INSERT INTO dbo.TreeNavigationAuthorization
	(
		numGroupID,
		numTabID,
		numPageNavID,
		bitVisible,
		numDomainID,
		tintType
	)
	SELECT  
		numGroupID,
		(SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Contracts' AND numDomainID=1),
		(@numMAXPageNavID + 1),
		1,
		numDomainID,
		1
	FROM    
		AuthenticationGroupMaster
	WHERE
		vcGroupName='System Administrator'
COMMIT
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		ERROR_MESSAGE(),
		ERROR_NUMBER(),
		ERROR_SEVERITY(),
		ERROR_STATE(),
		ERROR_LINE(),
		ISNULL(ERROR_PROCEDURE(), '-');
END CATCH
-----------------------------------------------------------------------------
BEGIN TRY
BEGIN TRANSACTION

	DECLARE @numMAXPageNavID INT
	SELECT @numMAXPageNavID = MAX(numPageNavID) FROM PageNavigationDTL


	INSERT INTO PageNavigationDTL
	(
		numPageNavID,numModuleID,numParentID,vcPageNavName,vcNavURL,bitVisible,numTabID,vcAddURL,bitAddIsPopUp
	)
	VALUES
	(
		(@numMAXPageNavID + 1),(SELECT TOP 1 numModuleID FROM ModuleMaster WHERE vcModuleName='Contract Management'),0,'Contracts','../ContractManagement/frmContractListv2.aspx?type=1',1,(SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Contracts' AND numDomainID=1),'../ContractManagement/frmMngContract.aspx?type=1',1
	)

	INSERT INTO dbo.TreeNavigationAuthorization
	(
		numGroupID,
		numTabID,
		numPageNavID,
		bitVisible,
		numDomainID,
		tintType
	)
	SELECT  
		numGroupID,
		(SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Contracts' AND numDomainID=1),
		(@numMAXPageNavID + 1),
		1,
		numDomainID,
		1
	FROM    
		AuthenticationGroupMaster
	WHERE
		vcGroupName='System Administrator'
COMMIT
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		ERROR_MESSAGE(),
		ERROR_NUMBER(),
		ERROR_SEVERITY(),
		ERROR_STATE(),
		ERROR_LINE(),
		ISNULL(ERROR_PROCEDURE(), '-');
END CATCH
UPDATE TabMaster SET vcAddURL='ContractManagement/frmMngContract.aspx?type=1',
vcURL= 'ContractManagement/frmContractListv2.aspx?type=1'
WHERE numTabName='Contracts' AND tintTabType=1

ALTER TABLE Domain ADD bitDisplayContractElement BIT DEFAULT 0  
ALTER TABLE Domain ADD vcEmployeeForContractTimeElement VARCHAR(500) DEFAULT NULL  

ALTER TABLE Contracts ADD numModifiedBy NUMERIC(18,0)
ALTER TABLE Contracts ADD dtmModifiedOn DATETIME

ALTER TABLE Contracts ADD numCreatedBy NUMERIC(18,0)
ALTER TABLE Contracts ADD dtmCreatedOn DATETIME
ALTER TABLE Contracts ADD timeUsed NUMERIC(18,2)


---------------------------------------------------------------------------------------------
INSERT  INTO dbo.NameTemplate
(
	numDomainID,vcModuleName,vcNameTemplate,tintModuleID,numSequenceId,numMinLength,numRecordID
)
SELECT 
	numDomainId,'Contract','Time-',1,1,4,4
FROM   
	Domain	

INSERT  INTO dbo.NameTemplate
(
	numDomainID,vcModuleName,vcNameTemplate,tintModuleID,numSequenceId,numMinLength,numRecordID
)
SELECT 
	numDomainId,'Warrantee','Warrantee-',1,1,4,5
FROM   
	Domain	

/******************************************** SANDEEP *********************************************/

ALTER TABLE PricingTable ADD numCurrencyID NUMERIC(18,0) DEFAULT 0
UPDATE PricingTable SET numCurrencyID=0
-------------------------------

USE [Production.2014]
GO

/****** Object:  Table [dbo].[ItemCurrencyPrice]    Script Date: 17-Jun-20 3:21:10 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ItemCurrencyPrice](
	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numDomainID] [numeric](18, 0) NOT NULL,
	[numItemCode] [numeric](18, 0) NOT NULL,
	[numCurrencyID] [numeric](18, 0) NOT NULL,
	[monListPrice] [decimal](20, 5) NOT NULL,
 CONSTRAINT [PK_ItemCurrencyPrice] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

----------------------------------------------

SET IDENTITY_INSERT ListDetails ON

INSERT INTO ListDetails
(
	numListItemID,numListID,vcData,bitDelete,numDomainID,constFlag,sintOrder
)
VALUES
(
	2,31,'ACH',0,1,1,0
)

SET IDENTITY_INSERT ListDetails OFF

---------------------------------------------------

UPDATE DycFormField_Mapping SET vcAssociatedControlType='SelectBox' WHERE numFormID=29 AND numFieldID=311

---------------------------------------------------

-- EXECUTE AFTER EXECUTING EXEC TO CONVERT PASSWORD TO ENCTYPTED PASWORD
UPDATE ExtranetAccountsDtl SET vcPassword=NULL WHERE vcPassword='+XzdWtmW1RDGNvofcN89Fw=='

-----------------------------------------

ALTER TABLE Domain ADD numARContactPosition NUMERIC(18,0)
ALTER TABLE Domain ADD bitShowCardConnectLink BIT
ALTER TABLE Domain ADD vcBluePayFormName VARCHAR(500)
ALTER TABLE Domain ADD vcBluePaySuccessURL VARCHAR(500)
ALTER TABLE Domain ADD vcBluePayDeclineURL VARCHAR(500)

-----------------------------------------

UPDATE GenericDocuments SET vcSubject='Customer Statement' WHERE VcFileName='#SYS#CUSTOMER_STATEMENT'

-----------------------------------------

UPDATE GenericDocuments SET numModuleID=45, vcDocDesc='Dear ##ContactFirstName## ##ContactLastName##,
<br />
<br />
Below please find your customer statement. If you have any questions, Please feel free to call us.
<br />
<br />
##Signature##
<br />
<br />
##ARStatement##' WHERE VcFileName='#SYS#CUSTOMER_STATEMENT'

-----------------------------------------

INSERT INTO EmailMergeFields 
(
	vcMergeField,vcMergeFieldValue,numModuleID,tintModuleType
)
VALUES
(
	'Org. A/R Statement','##ARStatement##',45,0
)

-----------------------------------------

ALTER TABLE Subscribers ADD intNoofLimitedAccessUsers INT
ALTER TABLE Subscribers ADD intNoofBusinessPortalUsers INT
ALTER TABLE Subscribers ADD monFullUserCost DECIMAL(20,5)
ALTER TABLE Subscribers ADD monLimitedAccessUserCost DECIMAL(20,5)
ALTER TABLE Subscribers ADD monBusinessPortalUserCost DECIMAL(20,5)
ALTER TABLE Subscribers ADD monSiteCost DECIMAL(20,5)
ALTER TABLE Subscribers ADD intFullUserConcurrency INT
-----------------------------------------

ALTER TABLE PageMaster ADD bitViewPermission1Applicable BIT DEFAULT 1
ALTER TABLE PageMaster ADD bitViewPermission2Applicable BIT DEFAULT 1
ALTER TABLE PageMaster ADD bitViewPermission3Applicable BIT DEFAULT 1
ALTER TABLE PageMaster ADD bitAddPermission1Applicable BIT DEFAULT 1
ALTER TABLE PageMaster ADD bitAddPermission2Applicable BIT DEFAULT 1
ALTER TABLE PageMaster ADD bitAddPermission3Applicable BIT DEFAULT 1
ALTER TABLE PageMaster ADD bitUpdatePermission1Applicable BIT DEFAULT 1
ALTER TABLE PageMaster ADD bitUpdatePermission2Applicable BIT DEFAULT 1
ALTER TABLE PageMaster ADD bitUpdatePermission3Applicable BIT DEFAULT 1
ALTER TABLE PageMaster ADD bitDeletePermission1Applicable BIT DEFAULT 1
ALTER TABLE PageMaster ADD bitDeletePermission2Applicable BIT DEFAULT 1
ALTER TABLE PageMaster ADD bitDeletePermission3Applicable BIT DEFAULT 1
ALTER TABLE PageMaster ADD bitExportPermission1Applicable BIT DEFAULT 1
ALTER TABLE PageMaster ADD bitExportPermission2Applicable BIT DEFAULT 1
ALTER TABLE PageMaster ADD bitExportPermission3Applicable BIT DEFAULT 1

-----------------------------------------

UPDATE 
	PageMaster 
SET 
	bitViewPermission1Applicable=1,bitViewPermission2Applicable=1,bitViewPermission3Applicable=1
	,bitAddPermission1Applicable=1,bitAddPermission2Applicable=1,bitAddPermission3Applicable=1
	,bitUpdatePermission1Applicable=1,bitUpdatePermission2Applicable=1,bitUpdatePermission3Applicable=1
	,bitDeletePermission1Applicable=1,bitDeletePermission2Applicable=1,bitDeletePermission3Applicable=1
	,bitExportPermission1Applicable=1,bitExportPermission2Applicable=1,bitExportPermission3Applicable=1

-----------------------------------------------------

USE [Production.2014]
GO

/****** Object:  Table [dbo].[ModuleMasterGroupType]    Script Date: 13-May-20 5:29:09 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ModuleMasterGroupType](
	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numModuleID] [numeric](18, 0) NOT NULL,
	[tintGroupType] [tinyint] NOT NULL,
 CONSTRAINT [PK_ModuleMasterGroupType] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[ModuleMasterGroupType]  WITH CHECK ADD  CONSTRAINT [FK_ModuleMasterGroupType_ModuleMaster] FOREIGN KEY([numModuleID])
REFERENCES [dbo].[ModuleMaster] ([numModuleID])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[ModuleMasterGroupType] CHECK CONSTRAINT [FK_ModuleMasterGroupType_ModuleMaster]
GO

-----------------------------------------------------------------


USE [Production.2014]
GO

/****** Object:  Table [dbo].[PageMasterGroupSetting]    Script Date: 14-May-20 4:49:35 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[PageMasterGroupSetting](
	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numPageID] [numeric](18, 0) NOT NULL,
	[numModuleID] [numeric](18, 0) NOT NULL,
	[numGroupID] [numeric](18, 0) NOT NULL,
	[bitIsViewApplicable] [bit] NULL,
	[bitViewPermission1Applicable] [bit] NULL,
	[bitViewPermission2Applicable] [bit] NULL,
	[bitViewPermission3Applicable] [bit] NULL,
	[bitIsAddApplicable] [bit] NULL,
	[bitAddPermission1Applicable] [bit] NULL,
	[bitAddPermission2Applicable] [bit] NULL,
	[bitAddPermission3Applicable] [bit] NULL,
	[bitIsUpdateApplicable] [bit] NULL,
	[bitUpdatePermission1Applicable] [bit] NULL,
	[bitUpdatePermission2Applicable] [bit] NULL,
	[bitUpdatePermission3Applicable] [bit] NULL,
	[bitIsDeleteApplicable] [bit] NULL,
	[bitDeletePermission1Applicable] [bit] NULL,
	[bitDeletePermission2Applicable] [bit] NULL,
	[bitDeletePermission3Applicable] [bit] NULL,
	[bitIsExportApplicable] [bit] NULL,
	[bitExportPermission1Applicable] [bit] NULL,
	[bitExportPermission2Applicable] [bit] NULL,
	[bitExportPermission3Applicable] [bit] NULL,
 CONSTRAINT [PK_PageMasterGroupSetting] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

-----------------------------------------------------------------

UPDATE AuthenticationGroupMaster SET bitConsFlag=1 WHERE tintGroupType=1 AND numGroupID IN (SELECT MIN(numGroupID) FROM AuthenticationGroupMaster WHERE tintGroupType=1 GROUP BY numDomainID)
UPDATE AuthenticationGroupMaster SET bitConsFlag=1 WHERE tintGroupType=2 AND numGroupID IN (SELECT MIN(numGroupID) FROM AuthenticationGroupMaster WHERE tintGroupType=2 GROUP BY numDOmainID)
UPDATE AuthenticationGroupMaster SET vcGroupName='Business Portal Users' WHERE tintGroupType=2 AND vcGroupName='Default Extranet'


INSERT INTO AuthenticationGroupMaster (vcGroupName,numDomainID,tintGroupType,bitConsFlag) SELECT 'Limited Access Users',numDomainId,4,1 FROM Domain
--INSERT INTO AuthenticationGroupMaster (vcGroupName,numDomainID,tintGroupType,bitConsFlag) SELECT 'Time & Expense Users',numDomainId,5,1 FROM Domain
--INSERT INTO AuthenticationGroupMaster (vcGroupName,numDomainID,tintGroupType,bitConsFlag) SELECT 'Associated Contact User',numDomainId,6,1 FROM Domain

INSERT INTO GroupTabDetails (numGroupId,numTabId,numRelationShip,bitallowed,numOrder,tintType) SELECT numGroupID,135,0,1,1,0 FROM AuthenticationGroupMaster WHERE tintGroupType = 4
INSERT INTO GroupTabDetails (numGroupId,numTabId,numRelationShip,bitallowed,numOrder,tintType) SELECT numGroupID,5,0,1,1,0 FROM AuthenticationGroupMaster WHERE tintGroupType = 4
INSERT INTO GroupTabDetails (numGroupId,numTabId,numRelationShip,bitallowed,numOrder,tintType) SELECT numGroupID,36,0,1,1,0 FROM AuthenticationGroupMaster WHERE tintGroupType = 4
INSERT INTO GroupTabDetails (numGroupId,numTabId,numRelationShip,bitallowed,numOrder,tintType) SELECT numGroupID,132,0,1,1,0 FROM AuthenticationGroupMaster WHERE tintGroupType = 4


INSERT INTO TreeNavigationAuthorization
(
	numDomainID,numGroupID,numTabID,numPageNavID,bitVisible,tintType
)
SELECT 
	numDomainID,numGroupID,5,279,1,1
FROM
	AuthenticationGroupMaster
WHERE
	tintGroupType=4
UNION
SELECT 
	numDomainID,numGroupID,5,280,1,1
FROM
	AuthenticationGroupMaster
WHERE
	tintGroupType=4

INSERT INTO TreeNavigationAuthorization
(
	numDomainID,numGroupID,numTabID,numPageNavID,bitVisible,tintType
)
SELECT 
	numDomainID,numGroupID,135,274,1,1
FROM
	AuthenticationGroupMaster
WHERE
	tintGroupType=4
UNION
SELECT 
	numDomainID,numGroupID,135,275,1,1
FROM
	AuthenticationGroupMaster
WHERE
	tintGroupType=4

INSERT INTO TreeNavigationAuthorization
(
	numDomainID,numGroupID,numTabID,numPageNavID,bitVisible,tintType
)
SELECT 
	numDomainID,numGroupID,132,84,1,1
FROM
	AuthenticationGroupMaster
WHERE
	tintGroupType=4
UNION
SELECT 
	numDomainID,numGroupID,132,271,1,1
FROM
	AuthenticationGroupMaster
WHERE
	tintGroupType=4

INSERT INTO TreeNavigationAuthorization
(
	numDomainID,numGroupID,numTabID,numPageNavID,bitVisible,tintType
)
SELECT 
	numDomainID,numGroupID,36,250,1,1
FROM
	AuthenticationGroupMaster
WHERE
	tintGroupType=4
-----------------------------------------

UPDATE PageMaster SET bitDeleted=1 WHERE numModuleID=10 AND numPageID IN (24,25)
UPDATE PageMaster SET vcPageDesc='Work Orders Grid (Open View)',bitIsUpdateApplicable=0,bitIsDeleteApplicable=0 WHERE numModuleID=16 AND numPageID=152 
UPDATE PageMaster SET bitIsDeleteApplicable=0 WHERE numModuleID=16 AND numPageID=153

INSERT INTO PageMaster
(
	numPageID,numModuleID,vcFileName,vcPageDesc,bitIsViewApplicable,bitIsAddApplicable,bitIsUpdateApplicable,bitIsDeleteApplicable,bitIsExportApplicable
)
VALUES
(154,16,'frmWorkOrderList.aspx','"Finish Work Order" button from initial grid (Open view) and detail page',1,0,0,0,0),
(155,16,'frmWorkOrderList.aspx','"Remover" button from initial grid (Open view) and detail page',1,0,0,0,0)
,(156,16,'frmWorkOrder.aspx','Work Orders Grid (Finished view)',1,0,0,0,0)


INSERT INTO PageMaster
(
	numPageID,numModuleID,vcFileName,vcPageDesc,bitIsViewApplicable,bitIsAddApplicable,bitIsUpdateApplicable,bitIsDeleteApplicable,bitIsExportApplicable
)
VALUES
(157,16,'frmWorkOrder.aspx','Work In Progress (WIP) Form',0,0,1,0,0)
,(158,16,'frmWorkOrder.aspx','Manage WIP Form',1,0,1,0,0)
,(159,16,'frmWorkOrder.aspx','Edit Source/Default Processes from the Manage WIP Form',0,0,1,0,0)
,(160,16,'frmWorkOrder.aspx','Bill Of Materials (BOM) Form',1,0,0,0,0)


INSERT INTO PageMaster
(
	numPageID,numModuleID,vcFileName,vcPageDesc,bitIsViewApplicable,bitIsAddApplicable,bitIsUpdateApplicable,bitIsDeleteApplicable,bitIsExportApplicable
	,bitViewPermission1Applicable,bitViewPermission2Applicable,bitViewPermission3Applicable
	,bitAddPermission1Applicable,bitAddPermission2Applicable,bitAddPermission3Applicable
	,bitUpdatePermission1Applicable,bitUpdatePermission2Applicable,bitUpdatePermission3Applicable
	,bitDeletePermission1Applicable,bitDeletePermission2Applicable,bitDeletePermission3Applicable
	,bitExportPermission1Applicable,bitExportPermission2Applicable,bitExportPermission3Applicable
)
VALUES
(161,16,'frmWorkOrderList.aspx','"Pick List" and "Pick Selected" button from initial grid (Open view)',1,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0)
,(162,16,'frmWorkOrder.aspx','"Pick BOM" button from work order detail',1,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0)
,(163,16,'frmWorkOrder.aspx','"Open Selected Item(s) in Demand Plan" button from Bill Of Materials (BOM) tab in work order detail',1,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0)
,(164,16,'frmWorkOrderList.aspx','Grid Configuration',1,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0)
,(165,16,'frmWorkOrder.aspx','Layout',1,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0)

-----------------------------------------


INSERT INTO PageMasterGroupSetting 
(
	numPageID
	,numModuleID
	,numGroupID
	,bitIsViewApplicable
	,bitViewPermission1Applicable
	,bitViewPermission2Applicable
	,bitViewPermission3Applicable
	,bitIsAddApplicable
	,bitAddPermission1Applicable
	,bitAddPermission2Applicable
	,bitAddPermission3Applicable
	,bitIsUpdateApplicable
	,bitUpdatePermission1Applicable
	,bitUpdatePermission2Applicable
	,bitUpdatePermission3Applicable
	,bitIsDeleteApplicable
	,bitDeletePermission1Applicable
	,bitDeletePermission2Applicable
	,bitDeletePermission3Applicable
	,bitIsExportApplicable
	,bitExportPermission1Applicable
	,bitExportPermission2Applicable
	,bitExportPermission3Applicable
)
SELECT 152,16,numGroupID,1,1,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 FROM AuthenticationGroupMaster WHERE tintGroupType=4 
UNION
SELECT 153,16,numGroupID,1,1,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 FROM AuthenticationGroupMaster WHERE tintGroupType=4
UNION
SELECT 154,16,numGroupID,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 FROM AuthenticationGroupMaster WHERE tintGroupType=4
UNION
SELECT 155,16,numGroupID,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 FROM AuthenticationGroupMaster WHERE tintGroupType=4
UNION
SELECT 156,16,numGroupID,1,1,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 FROM AuthenticationGroupMaster WHERE tintGroupType=4
UNION
SELECT 157,16,numGroupID,0,0,0,0,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0 FROM AuthenticationGroupMaster WHERE tintGroupType=4
UNION
SELECT 158,16,numGroupID,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 FROM AuthenticationGroupMaster WHERE tintGroupType=4
UNION
SELECT 159,16,numGroupID,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 FROM AuthenticationGroupMaster WHERE tintGroupType=4
UNION
SELECT 160,16,numGroupID,1,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 FROM AuthenticationGroupMaster WHERE tintGroupType=4 
UNION
SELECT 161,16,numGroupID,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 FROM AuthenticationGroupMaster WHERE tintGroupType=4
UNION
SELECT 162,16,numGroupID,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 FROM AuthenticationGroupMaster WHERE tintGroupType=4
UNION
SELECT 163,16,numGroupID,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 FROM AuthenticationGroupMaster WHERE tintGroupType=4
UNION
SELECT 164,16,numGroupID,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 FROM AuthenticationGroupMaster WHERE tintGroupType=4
UNION
SELECT 165,16,numGroupID,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 FROM AuthenticationGroupMaster WHERE tintGroupType=4

----------------------------------------

INSERT INTO GroupAuthorization
(
	numDomainID,numGroupID,numPageID,numModuleID,intViewAllowed,intAddAllowed,intUpdateAllowed,intDeleteAllowed,intExportAllowed,intPrintAllowed
)
SELECT numDomainID,numGroupID,152,16,1,0,0,0,0,0 FROM AuthenticationGroupMaster WHERE tintGroupType=4 
UNION
SELECT numDomainID,numGroupID,153,16,1,0,0,0,0,0 FROM AuthenticationGroupMaster WHERE tintGroupType=4
UNION
SELECT numDomainID,numGroupID,154,16,0,0,0,0,0,0 FROM AuthenticationGroupMaster WHERE tintGroupType=4
UNION
SELECT numDomainID,numGroupID,155,16,0,0,0,0,0,0 FROM AuthenticationGroupMaster WHERE tintGroupType=4
UNION
SELECT numDomainID,numGroupID,156,16,1,0,0,0,0,0 FROM AuthenticationGroupMaster WHERE tintGroupType=4
UNION
SELECT numDomainID,numGroupID,157,16,1,0,1,0,0,0 FROM AuthenticationGroupMaster WHERE tintGroupType=4
UNION
SELECT numDomainID,numGroupID,158,16,0,0,0,0,0,0 FROM AuthenticationGroupMaster WHERE tintGroupType=4
UNION
SELECT numDomainID,numGroupID,159,16,0,0,0,0,0,0 FROM AuthenticationGroupMaster WHERE tintGroupType=4
UNION
SELECT numDomainID,numGroupID,160,16,3,0,0,0,0,0 FROM AuthenticationGroupMaster WHERE tintGroupType=4 
UNION
SELECT numDomainID,numGroupID,161,16,0,0,0,0,0,0 FROM AuthenticationGroupMaster WHERE tintGroupType=4
UNION
SELECT numDomainID,numGroupID,162,16,0,0,0,0,0,0 FROM AuthenticationGroupMaster WHERE tintGroupType=4
UNION
SELECT numDomainID,numGroupID,163,16,0,0,0,0,0,0 FROM AuthenticationGroupMaster WHERE tintGroupType=4
UNION
SELECT numDomainID,numGroupID,164,16,0,0,0,0,0,0 FROM AuthenticationGroupMaster WHERE tintGroupType=4
UNION
SELECT numDomainID,numGroupID,165,16,0,0,0,0,0,0 FROM AuthenticationGroupMaster WHERE tintGroupType=4


-----------------------------------------------
-- TIME & EXPENSE --

INSERT INTO PageMasterGroupSetting 
(
	numPageID
	,numModuleID
	,numGroupID
	,bitIsViewApplicable
	,bitViewPermission1Applicable
	,bitViewPermission2Applicable
	,bitViewPermission3Applicable
	,bitIsAddApplicable
	,bitAddPermission1Applicable
	,bitAddPermission2Applicable
	,bitAddPermission3Applicable
	,bitIsUpdateApplicable
	,bitUpdatePermission1Applicable
	,bitUpdatePermission2Applicable
	,bitUpdatePermission3Applicable
	,bitIsDeleteApplicable
	,bitDeletePermission1Applicable
	,bitDeletePermission2Applicable
	,bitDeletePermission3Applicable
	,bitIsExportApplicable
	,bitExportPermission1Applicable
	,bitExportPermission2Applicable
	,bitExportPermission3Applicable
)
SELECT 117,40,numGroupID,1,1,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0 FROM AuthenticationGroupMaster WHERE tintGroupType=4 

------------------------------------------

INSERT INTO GroupAuthorization
(
	numDomainID,numGroupID,numPageID,numModuleID,intViewAllowed,intAddAllowed,intUpdateAllowed,intDeleteAllowed,intExportAllowed,intPrintAllowed
)
SELECT numDomainID,numGroupID,117,40,1,1,0,0,0,0 FROM AuthenticationGroupMaster WHERE tintGroupType=4 

--------------------------------------------------
--- ACCOUNTING ---

INSERT INTO PageMasterGroupSetting 
(
	numPageID,numModuleID,numGroupID
	,bitIsViewApplicable,bitViewPermission1Applicable,bitViewPermission2Applicable,bitViewPermission3Applicable
	,bitIsAddApplicable,bitAddPermission1Applicable,bitAddPermission2Applicable,bitAddPermission3Applicable
	,bitIsUpdateApplicable,bitUpdatePermission1Applicable,bitUpdatePermission2Applicable,bitUpdatePermission3Applicable
	,bitIsDeleteApplicable,bitDeletePermission1Applicable,bitDeletePermission2Applicable,bitDeletePermission3Applicable
	,bitIsExportApplicable,bitExportPermission1Applicable,bitExportPermission2Applicable,bitExportPermission3Applicable
)
SELECT
	PageMaster.numPageID,PageMaster.numModuleID,numGroupID,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
FROM
	AuthenticationGroupMaster, PageMaster
WHERE 
	tintGroupType=4 
	AND numModuleID=35
	AND numPageID <> 87
UNION
SELECT
	PageMaster.numPageID,PageMaster.numModuleID,numGroupID,1,1,0,0,0,0,0,0,1,1,0,0,0,0,0,0,1,1,0,0
FROM
	AuthenticationGroupMaster, PageMaster
WHERE 
	tintGroupType=4 
	AND numModuleID=35
	AND numPageID = 87

--

INSERT INTO GroupAuthorization
(
	numDomainID,numGroupID,numPageID,numModuleID,intViewAllowed,intAddAllowed,intUpdateAllowed,intDeleteAllowed,intExportAllowed,intPrintAllowed
)
SELECT
	numDomainID,numGroupID,PageMaster.numPageID,PageMaster.numModuleID,0,0,0,0,0,0
FROM
	AuthenticationGroupMaster, PageMaster
WHERE 
	tintGroupType=4 
	AND numModuleID=35
	AND numPageID <> 87
UNION
SELECT
	numDomainID,numGroupID,PageMaster.numPageID,PageMaster.numModuleID,1,0,1,0,1,0
FROM
	AuthenticationGroupMaster, PageMaster
WHERE 
	tintGroupType=4 
	AND numModuleID=35
	AND numPageID = 87

-----------------------------------------

INSERT INTO ModuleMasterGroupType (numModuleID,tintGroupType) VALUES (1,4),(12,4),(16,4),(35,4),(40,4)

------------------------------------------

UPDATE PageMaster SET bitViewPermission3Applicable=1,bitViewPermission2Applicable=0,bitViewPermission1Applicable=1,bitAddPermission3Applicable=1,bitAddPermission2Applicable=0,bitAddPermission1Applicable=0 WHERE numModuleID=16 AND numPageID=152
UPDATE PageMaster SET bitViewPermission3Applicable=1,bitViewPermission2Applicable=0,bitViewPermission1Applicable=1,bitUpdatePermission2Applicable=0 WHERE numModuleID=16 AND numPageID=153
UPDATE PageMaster SET bitViewPermission3Applicable=1,bitViewPermission2Applicable=0,bitViewPermission1Applicable=0 WHERE numModuleID=16 AND numPageID=154
UPDATE PageMaster SET bitViewPermission3Applicable=1,bitViewPermission2Applicable=0,bitViewPermission1Applicable=0 WHERE numModuleID=16 AND numPageID=155
UPDATE PageMaster SET bitViewPermission3Applicable=1,bitViewPermission2Applicable=0,bitViewPermission1Applicable=1 WHERE numModuleID=16 AND numPageID=156
UPDATE PageMaster SET bitUpdatePermission3Applicable=1,bitUpdatePermission2Applicable=0,bitUpdatePermission1Applicable=1 WHERE numModuleID=16 AND numPageID=157
UPDATE PageMaster SET bitViewPermission3Applicable=1,bitViewPermission2Applicable=0,bitViewPermission1Applicable=0,bitUpdatePermission3Applicable=1,bitUpdatePermission2Applicable=0,bitUpdatePermission1Applicable=1 WHERE numModuleID=16 AND numPageID=158
UPDATE PageMaster SET bitUpdatePermission3Applicable=1,bitUpdatePermission2Applicable=0,bitUpdatePermission1Applicable=0 WHERE numModuleID=16 AND numPageID=159
UPDATE PageMaster SET bitViewPermission3Applicable=1,bitViewPermission2Applicable=0,bitViewPermission1Applicable=0 WHERE numModuleID=16 AND numPageID=160
UPDATE PageMaster SET bitViewPermission3Applicable=1,bitViewPermission2Applicable=0,bitViewPermission1Applicable=0 WHERE numModuleID=16 AND numPageID=161
UPDATE PageMaster SET bitViewPermission3Applicable=1,bitViewPermission2Applicable=0,bitViewPermission1Applicable=0 WHERE numModuleID=16 AND numPageID=162
UPDATE PageMaster SET bitViewPermission3Applicable=1,bitViewPermission2Applicable=0,bitViewPermission1Applicable=0 WHERE numModuleID=16 AND numPageID=163

----------------------------------------------

-- PROJECTS --

UPDATE 
	PageMaster 
SET 
	vcPageDesc='Projects Grid (Open view)'
	,vcFileName='frmProjectList.aspx'
	,bitIsViewApplicable=1,bitViewPermission1Applicable=1,bitViewPermission2Applicable=1,bitViewPermission3Applicable=1
	,bitIsAddApplicable=1,bitAddPermission1Applicable=0,bitAddPermission2Applicable=0,bitAddPermission3Applicable=1
	,bitIsUpdateApplicable=1,bitUpdatePermission1Applicable=1,bitUpdatePermission2Applicable=1,bitUpdatePermission3Applicable=1
	,bitIsDeleteApplicable=0,bitDeletePermission1Applicable=0,bitDeletePermission2Applicable=0,bitDeletePermission3Applicable=0
	,bitIsExportApplicable=0,bitExportPermission1Applicable=0,bitExportPermission2Applicable=0,bitExportPermission3Applicable=0
WHERE 
	numModuleID=12 
	AND numPageID=1

----------------

UPDATE 
	PageMaster 
SET 
	vcPageDesc='Projects Grid (Finished view)'
	,bitIsViewApplicable=1,bitViewPermission1Applicable=1,bitViewPermission2Applicable=1,bitViewPermission3Applicable=1
	,bitIsAddApplicable=0,bitAddPermission1Applicable=0,bitAddPermission2Applicable=0,bitAddPermission3Applicable=0
	,bitIsUpdateApplicable=0,bitUpdatePermission1Applicable=0,bitUpdatePermission2Applicable=0,bitUpdatePermission3Applicable=0
	,bitIsDeleteApplicable=0,bitDeletePermission1Applicable=0,bitDeletePermission2Applicable=0,bitDeletePermission3Applicable=0
	,bitIsExportApplicable=0,bitExportPermission1Applicable=0,bitExportPermission2Applicable=0,bitExportPermission3Applicable=0
WHERE 
	numModuleID=12 
	AND numPageID=2

------------------

INSERT INTO PageMaster
(
	numPageID,numModuleID,vcFileName,vcPageDesc
	,bitIsViewApplicable,bitViewPermission1Applicable,bitViewPermission2Applicable,bitViewPermission3Applicable
	,bitIsAddApplicable,bitAddPermission1Applicable,bitAddPermission2Applicable,bitAddPermission3Applicable
	,bitIsUpdateApplicable,bitUpdatePermission1Applicable,bitUpdatePermission2Applicable,bitUpdatePermission3Applicable
	,bitIsDeleteApplicable,bitDeletePermission1Applicable,bitDeletePermission2Applicable,bitDeletePermission3Applicable
	,bitIsExportApplicable,bitExportPermission1Applicable,bitExportPermission2Applicable,bitExportPermission3Applicable
)
VALUES
(
	15,12,'frmProjectList.aspx','Projects Grid (Canceled view)'
	,1,1,1,1
	,0,0,0,0
	,0,0,0,0
	,0,0,0,0
	,0,0,0,0
)

------------------

INSERT INTO PageMaster
(
	numPageID,numModuleID,vcFileName,vcPageDesc
	,bitIsViewApplicable,bitViewPermission1Applicable,bitViewPermission2Applicable,bitViewPermission3Applicable
	,bitIsAddApplicable,bitAddPermission1Applicable,bitAddPermission2Applicable,bitAddPermission3Applicable
	,bitIsUpdateApplicable,bitUpdatePermission1Applicable,bitUpdatePermission2Applicable,bitUpdatePermission3Applicable
	,bitIsDeleteApplicable,bitDeletePermission1Applicable,bitDeletePermission2Applicable,bitDeletePermission3Applicable
	,bitIsExportApplicable,bitExportPermission1Applicable,bitExportPermission2Applicable,bitExportPermission3Applicable
)
VALUES
(
	16,12,'frmProjectList.aspx','�Remove� button from initial grid (Open view)'
	,1,0,0,1
	,0,0,0,0
	,0,0,0,0
	,0,0,0,0
	,0,0,0,0
)

-------------------

UPDATE
	PageMaster
SET 
	bitIsViewApplicable=1,bitViewPermission1Applicable=1,bitViewPermission2Applicable=1,bitViewPermission3Applicable=1
	,bitIsAddApplicable=0,bitAddPermission1Applicable=0,bitAddPermission2Applicable=0,bitAddPermission3Applicable=0
	,bitIsUpdateApplicable=1,bitUpdatePermission1Applicable=1,bitUpdatePermission2Applicable=1,bitUpdatePermission3Applicable=1
	,bitIsDeleteApplicable=1,bitDeletePermission1Applicable=1,bitDeletePermission2Applicable=1,bitDeletePermission3Applicable=1
	,bitIsExportApplicable=0,bitExportPermission1Applicable=0,bitExportPermission2Applicable=0,bitExportPermission3Applicable=0
WHERE
	numModuleID=12
	AND numPageID=3

-------------------

UPDATE
	PageMaster
SET 
	vcPageDesc='Project Tasks Form'
	,bitIsViewApplicable=0,bitViewPermission1Applicable=0,bitViewPermission2Applicable=0,bitViewPermission3Applicable=0
	,bitIsAddApplicable=0,bitAddPermission1Applicable=0,bitAddPermission2Applicable=0,bitAddPermission3Applicable=0
	,bitIsUpdateApplicable=1,bitUpdatePermission1Applicable=1,bitUpdatePermission2Applicable=0,bitUpdatePermission3Applicable=1
	,bitIsDeleteApplicable=0,bitDeletePermission1Applicable=0,bitDeletePermission2Applicable=0,bitDeletePermission3Applicable=0
	,bitIsExportApplicable=0,bitExportPermission1Applicable=0,bitExportPermission2Applicable=0,bitExportPermission3Applicable=0
WHERE
	numModuleID=12
	AND numPageID=4

---------------

UPDATE
	PageMaster
SET 
	vcPageDesc='Manage Project Tasks From'
	,bitIsViewApplicable=1,bitViewPermission1Applicable=1,bitViewPermission2Applicable=1,bitViewPermission3Applicable=1
	,bitIsAddApplicable=0,bitAddPermission1Applicable=0,bitAddPermission2Applicable=0,bitAddPermission3Applicable=0
	,bitIsUpdateApplicable=1,bitUpdatePermission1Applicable=1,bitUpdatePermission2Applicable=1,bitUpdatePermission3Applicable=1
	,bitIsDeleteApplicable=0,bitDeletePermission1Applicable=0,bitDeletePermission2Applicable=0,bitDeletePermission3Applicable=0
	,bitIsExportApplicable=0,bitExportPermission1Applicable=0,bitExportPermission2Applicable=0,bitExportPermission3Applicable=0
WHERE
	numModuleID=12
	AND numPageID=5

-------------------

UPDATE
	PageMaster
SET 
	vcPageDesc='Project Teams From'
	,bitIsViewApplicable=1,bitViewPermission1Applicable=1,bitViewPermission2Applicable=1,bitViewPermission3Applicable=1
	,bitIsAddApplicable=0,bitAddPermission1Applicable=0,bitAddPermission2Applicable=0,bitAddPermission3Applicable=0
	,bitIsUpdateApplicable=1,bitUpdatePermission1Applicable=1,bitUpdatePermission2Applicable=1,bitUpdatePermission3Applicable=1
	,bitIsDeleteApplicable=0,bitDeletePermission1Applicable=0,bitDeletePermission2Applicable=0,bitDeletePermission3Applicable=0
	,bitIsExportApplicable=0,bitExportPermission1Applicable=0,bitExportPermission2Applicable=0,bitExportPermission3Applicable=0
WHERE
	numModuleID=12
	AND numPageID=6

----------------

UPDATE
	PageMaster
SET 
	vcPageDesc='P&L From'
	,bitIsViewApplicable=1,bitViewPermission1Applicable=1,bitViewPermission2Applicable=1,bitViewPermission3Applicable=1
	,bitIsAddApplicable=0,bitAddPermission1Applicable=0,bitAddPermission2Applicable=0,bitAddPermission3Applicable=0
	,bitIsUpdateApplicable=1,bitUpdatePermission1Applicable=1,bitUpdatePermission2Applicable=1,bitUpdatePermission3Applicable=1
	,bitIsDeleteApplicable=0,bitDeletePermission1Applicable=0,bitDeletePermission2Applicable=0,bitDeletePermission3Applicable=0
	,bitIsExportApplicable=0,bitExportPermission1Applicable=0,bitExportPermission2Applicable=0,bitExportPermission3Applicable=0
WHERE
	numModuleID=12
	AND numPageID=7

-----------------

UPDATE
	PageMaster
SET 
	vcPageDesc='Edit Source/Default Processes from the Manage Project Tasks From'
	,bitIsViewApplicable=0,bitViewPermission1Applicable=0,bitViewPermission2Applicable=0,bitViewPermission3Applicable=0
	,bitIsAddApplicable=0,bitAddPermission1Applicable=0,bitAddPermission2Applicable=0,bitAddPermission3Applicable=0
	,bitIsUpdateApplicable=1,bitUpdatePermission1Applicable=1,bitUpdatePermission2Applicable=1,bitUpdatePermission3Applicable=1
	,bitIsDeleteApplicable=0,bitDeletePermission1Applicable=0,bitDeletePermission2Applicable=0,bitDeletePermission3Applicable=0
	,bitIsExportApplicable=0,bitExportPermission1Applicable=0,bitExportPermission2Applicable=0,bitExportPermission3Applicable=0
WHERE
	numModuleID=12
	AND numPageID=8

--------------------

INSERT INTO PageMaster
(
	numPageID,numModuleID,vcFileName,vcPageDesc
	,bitIsViewApplicable,bitViewPermission1Applicable,bitViewPermission2Applicable,bitViewPermission3Applicable
	,bitIsAddApplicable,bitAddPermission1Applicable,bitAddPermission2Applicable,bitAddPermission3Applicable
	,bitIsUpdateApplicable,bitUpdatePermission1Applicable,bitUpdatePermission2Applicable,bitUpdatePermission3Applicable
	,bitIsDeleteApplicable,bitDeletePermission1Applicable,bitDeletePermission2Applicable,bitDeletePermission3Applicable
	,bitIsExportApplicable,bitExportPermission1Applicable,bitExportPermission2Applicable,bitExportPermission3Applicable
)
VALUES
(
	17,12,'frmProjects.aspx','�Finish Project� button from Project Details'
	,1,1,1,1
	,0,0,0,0
	,0,0,0,0
	,0,0,0,0
	,0,0,0,0
)

--------------------

DELETE FROM PageMaster WHERE numModuleID=12 AND numPageID IN (9,10,11,12,135)

--------------------

UPDATE PageMaster SET vcFileName='frmProjects.aspx' WHERE numModuleID=12 AND numPageID=13
UPDATE PageMaster SET vcFileName='frmProjectList.aspx' WHERE numModuleID=12 AND numPageID=14

--------------------

UPDATE 
	PageMaster 
SET 
	bitIsAddApplicable=1,bitAddPermission1Applicable=0,bitAddPermission2Applicable=0,bitAddPermission3Applicable=1
	,bitIsUpdateApplicable=1,bitUpdatePermission1Applicable=1,bitUpdatePermission2Applicable=0,bitUpdatePermission3Applicable=1
	,bitIsDeleteApplicable=1,bitDeletePermission1Applicable=1,bitDeletePermission2Applicable=0,bitDeletePermission3Applicable=1
WHERE 
	numModuleID=1 AND numPageID=1

----------------------

UPDATE 
	PageMaster 
SET 
	bitIsAddApplicable=1,bitAddPermission1Applicable=0,bitAddPermission2Applicable=0,bitAddPermission3Applicable=1
WHERE 
	numModuleID=1 AND numPageID=5

UPDATE 
	PageMaster 
SET 
	bitIsAddApplicable=1,bitAddPermission1Applicable=0,bitAddPermission2Applicable=0,bitAddPermission3Applicable=1
WHERE 
	numModuleID=1 AND numPageID=6

----------------------

UPDATE 
	PageMaster 
SET 
	vcFileName='frmTicklerdisplay.aspx'
WHERE 
	numModuleID=1 
	AND vcFileName='ticklerdisplay.aspx'

UPDATE 
	PageMaster 
SET 
	vcFileName='frmTicklerdisplay.aspx',bitViewPermission1Applicable=0,bitViewPermission2Applicable=0,bitViewPermission3Applicable=1
WHERE 
	numModuleID=1 AND numPageID=10

--------------------------------------

UPDATE PageMaster SET bitDeleted=1 WHERE numModuleID=1 AND numPageID IN (2,3,4,7,9)

--------------------------------------

UPDATE
	GroupAuthorization
SET
	intViewAllowed=3
	,intAddAllowed=3
	,intUpdateAllowed=3
	,intDeleteAllowed=3
	,intExportAllowed=0
	,intPrintAllowed=0
WHERE	
	numPageID=1
	AND numModuleID=1
	AND numGroupID IN (SELECT MIN(numGroupID) FROM AuthenticationGroupMaster WHERE tintGroupType=1 GROUP BY numDomainID)

-----------------------------------------

UPDATE
	GroupAuthorization
SET
	intViewAllowed=3
	,intAddAllowed=3
	,intUpdateAllowed=3
	,intDeleteAllowed=3
	,intExportAllowed=0
	,intPrintAllowed=0
WHERE	
	numPageID=10
	AND numModuleID=1
	AND numGroupID IN (SELECT MIN(numGroupID) FROM AuthenticationGroupMaster WHERE tintGroupType=1 GROUP BY numDomainID)

------------------------------------------

INSERT INTO PageMasterGroupSetting 
(
	numPageID,numModuleID,numGroupID
	,bitIsViewApplicable,bitViewPermission1Applicable,bitViewPermission2Applicable,bitViewPermission3Applicable
	,bitIsAddApplicable,bitAddPermission1Applicable,bitAddPermission2Applicable,bitAddPermission3Applicable
	,bitIsUpdateApplicable,bitUpdatePermission1Applicable,bitUpdatePermission2Applicable,bitUpdatePermission3Applicable
	,bitIsDeleteApplicable,bitDeletePermission1Applicable,bitDeletePermission2Applicable,bitDeletePermission3Applicable
	,bitIsExportApplicable,bitExportPermission1Applicable,bitExportPermission2Applicable,bitExportPermission3Applicable
)
SELECT
	1,1,numGroupID,1,1,0,0,0,0,0,0,1,1,0,0,1,1,0,0,0,0,0,0
FROM
	AuthenticationGroupMaster
WHERE 
	tintGroupType=4 
UNION
SELECT
	5,1,numGroupID,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
FROM
	AuthenticationGroupMaster
WHERE 
	tintGroupType=4 
UNION
SELECT
	6,1,numGroupID,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
FROM
	AuthenticationGroupMaster
WHERE 
	tintGroupType=4 
UNION
SELECT
	10,1,numGroupID,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
FROM
	AuthenticationGroupMaster
WHERE 
	tintGroupType=4 

----------------------------------

-- BUSINESS PORTAL USER ACCESS --

INSERT INTO ModuleMasterGroupType
(
	numModuleID
	,tintGroupType
)
VALUES
(32,2)
,(2,2)
,(3,2)
,(4,2)
,(11,2)
,(37,2)
,(10,2)
,(46,2)
,(12,2)
,(7,2)
,(16,2)

---------------------------------

INSERT INTO PageMasterGroupSetting 
(
	numPageID,numModuleID,numGroupID
	,bitIsViewApplicable,bitViewPermission1Applicable,bitViewPermission2Applicable,bitViewPermission3Applicable
	,bitIsAddApplicable,bitAddPermission1Applicable,bitAddPermission2Applicable,bitAddPermission3Applicable
	,bitIsUpdateApplicable,bitUpdatePermission1Applicable,bitUpdatePermission2Applicable,bitUpdatePermission3Applicable
	,bitIsDeleteApplicable,bitDeletePermission1Applicable,bitDeletePermission2Applicable,bitDeletePermission3Applicable
	,bitIsExportApplicable,bitExportPermission1Applicable,bitExportPermission2Applicable,bitExportPermission3Applicable
)
SELECT
	numPageID,numModuleID,numGroupID
	,1,1,0,0
	,0,0,0,0
	,0,0,0,0
	,0,0,0,0
	,0,0,0,0
FROM 
	AuthenticationGroupMaster, PageMaster
WHERE
	tintGroupType=2
	AND numModuleID IN (32,2,3,4,11,37,10,46,12,7,16)

----------------------------------

UPDATE
	GA
SET 
	intExportAllowed=0,intPrintAllowed=0,intViewAllowed=1,intAddAllowed=0,intUpdateAllowed=0,intDeleteAllowed=0
FROM
	GroupAuthorization GA
INNER JOIN
	AuthenticationGroupMaster
ON
	GA.numGroupID=AuthenticationGroupMaster.numGroupID
WHERE
	tintGroupType=2
	AND numPageID IN (SELECT numPageID FROM PageMaster WHERE numModuleID IN (32,2,3,4,11,37,10,46,12,7,16))

INSERT INTO GroupAuthorization
(
	numPageID,numModuleID,numGroupID,intExportAllowed,intPrintAllowed,intViewAllowed,intAddAllowed,intUpdateAllowed,intDeleteAllowed,numDomainID
)
SELECT
	numPageID,numModuleID,numGroupID,0,0,1,0,0,0,numDomainID
FROM 
	AuthenticationGroupMaster, PageMaster
WHERE
	tintGroupType=2
	AND numModuleID IN (32,2,3,4,11,37,10,46,12,7,16)
	AND NOT EXISTS (SELECT numPageID FROM GroupAuthorization GA WHERE GA.numPageID=PageMaster.numPageID AND GA.numModuleID=PageMaster.numModuleID AND GA.numGroupID =AuthenticationGroupMaster.numGroupID)

INSERT INTO GroupAuthorization
(
	numPageID,numModuleID,numGroupID,intExportAllowed,intPrintAllowed,intViewAllowed,intAddAllowed,intUpdateAllowed,intDeleteAllowed,numDomainID
)
SELECT
	numListItemID,32,numGroupID,0,0,1,0,0,0,numDomainID
FROM 
	AuthenticationGroupMaster
CROSS APPLY
(	
	SELECT 
		numListItemID 
	FROM 
		ListDetails 
	WHERE 
		numListID=5 
		AND ((constFlag=1 AND numListItemID <> 46) OR constFlag=0)
) PageMaster
WHERE
	tintGroupType=2
	AND NOT EXISTS (SELECT numPageID FROM GroupAuthorization GA WHERE GA.numPageID=PageMaster.numListItemID AND GA.numModuleID=32 AND GA.numGroupID =AuthenticationGroupMaster.numGroupID)
---------------------------------------

UPDATE PageMasterGroupSetting SET bitIsViewApplicable=0,bitViewPermission1Applicable=0 WHERE numModuleID=4 AND numPageID IN (12,13,16,17)
UPDATE GroupAuthorization SET intViewAllowed=0 WHERE numModuleID=4 AND numPageID IN (12,13,16,17) AND numGroupID IN (SELECT numGroupID FROM AuthenticationGroupMaster WHERE tintGroupType=2)
UPDATE PageMasterGroupSetting SET bitIsViewApplicable=0,bitViewPermission1Applicable=0 WHERE numModuleID=11 AND numPageID IN (1,134)
UPDATE GroupAuthorization SET intViewAllowed=0 WHERE numModuleID=11 AND numPageID IN (1,134) AND numGroupID IN (SELECT numGroupID FROM AuthenticationGroupMaster WHERE tintGroupType=2)
UPDATE PageMasterGroupSetting SET bitIsViewApplicable=0,bitViewPermission1Applicable=0 WHERE numModuleID=37 AND numPageID IN (34,130,132,149)
UPDATE GroupAuthorization SET intViewAllowed=0 WHERE numModuleID=37 AND numPageID IN (34,130,132,149) AND numGroupID IN (SELECT numGroupID FROM AuthenticationGroupMaster WHERE tintGroupType=2)
UPDATE PageMasterGroupSetting SET bitIsViewApplicable=0,bitViewPermission1Applicable=0 WHERE numModuleID=2 AND numPageID IN (7,9,10,11,12)
UPDATE GroupAuthorization SET intViewAllowed=0 WHERE numModuleID=2 AND numPageID IN (7,9,10,11,12) AND numGroupID IN (SELECT numGroupID FROM AuthenticationGroupMaster WHERE tintGroupType=2)
UPDATE PageMasterGroupSetting SET bitIsViewApplicable=0,bitViewPermission1Applicable=0 WHERE numModuleID=16 AND numPageID IN (154,155,158,159,161,162,163,164,165)
UPDATE GroupAuthorization SET intViewAllowed=0 WHERE numModuleID=16 AND numPageID IN (154,155,158,159,161,162,163,164,165) AND numGroupID IN (SELECT numGroupID FROM AuthenticationGroupMaster WHERE tintGroupType=2)
UPDATE PageMasterGroupSetting SET bitIsViewApplicable=0,bitViewPermission1Applicable=0 WHERE numModuleID=10 AND numPageID IN (15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35)
UPDATE GroupAuthorization SET intViewAllowed=0 WHERE numModuleID=10 AND numPageID IN (15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35) AND numGroupID IN (SELECT numGroupID FROM AuthenticationGroupMaster WHERE tintGroupType=2)
UPDATE PageMasterGroupSetting SET bitIsViewApplicable=0,bitViewPermission1Applicable=0 WHERE numModuleID=12 AND numPageID IN (5,6,7,8,13,14,16,17)
UPDATE GroupAuthorization SET intViewAllowed=0 WHERE numModuleID=12 AND numPageID IN (5,6,7,8,13,14,16,17) AND numGroupID IN (SELECT numGroupID FROM AuthenticationGroupMaster WHERE tintGroupType=2)
UPDATE PageMasterGroupSetting SET bitIsViewApplicable=0,bitViewPermission1Applicable=0 WHERE numModuleID=3 AND numPageID IN (10,11,13,14,15)
UPDATE GroupAuthorization SET intViewAllowed=0 WHERE numModuleID=3 AND numPageID IN (10,11,13,14,15) AND numGroupID IN (SELECT numGroupID FROM AuthenticationGroupMaster WHERE tintGroupType=2)
UPDATE PageMasterGroupSetting SET bitIsViewApplicable=0,bitViewPermission1Applicable=0 WHERE numModuleID=32 AND numPageID IN (2)
UPDATE GroupAuthorization SET intViewAllowed=0 WHERE numModuleID=32 AND numPageID IN (2) AND numGroupID IN (SELECT numGroupID FROM AuthenticationGroupMaster WHERE tintGroupType=2)
UPDATE PageMasterGroupSetting SET bitIsViewApplicable=0,bitViewPermission1Applicable=0 WHERE numModuleID=7 AND numPageID IN (1,12,13)
UPDATE GroupAuthorization SET intViewAllowed=0 WHERE numModuleID=7 AND numPageID IN (1,12,13) AND numGroupID IN (SELECT numGroupID FROM AuthenticationGroupMaster WHERE tintGroupType=2)
UPDATE PageMasterGroupSetting SET bitIsAddApplicable=1,bitAddPermission1Applicable=1 WHERE numModuleID=7 AND numPageID = 1
UPDATE GroupAuthorization SET intAddAllowed=1 WHERE numModuleID=7 AND numPageID = 1 AND numGroupID IN (SELECT numGroupID FROM AuthenticationGroupMaster WHERE tintGroupType=2)
UPDATE PageMasterGroupSetting SET bitIsAddApplicable=1,bitAddPermission1Applicable=1,bitIsUpdateApplicable=1,bitUpdatePermission1Applicable=1,bitIsDeleteApplicable=1,bitDeletePermission1Applicable=1 WHERE numModuleID=10 AND numPageID = 5
UPDATE GroupAuthorization SET intAddAllowed=1,intUpdateAllowed=1,intDeleteAllowed=1 WHERE numModuleID=10 AND numPageID = 5 AND numGroupID IN (SELECT numGroupID FROM AuthenticationGroupMaster WHERE tintGroupType=2)
UPDATE PageMasterGroupSetting SET bitIsAddApplicable=1,bitAddPermission1Applicable=1 WHERE numModuleID=10 AND numPageID = 28
UPDATE GroupAuthorization SET intAddAllowed=1 WHERE numModuleID=10 AND numPageID = 28 AND numGroupID IN (SELECT numGroupID FROM AuthenticationGroupMaster WHERE tintGroupType=2)
UPDATE PageMasterGroupSetting SET bitIsAddApplicable=1,bitAddPermission1Applicable=1 WHERE numModuleID=10 AND numPageID = 1
UPDATE GroupAuthorization SET intAddAllowed=1 WHERE numModuleID=10 AND numPageID = 1 AND numGroupID IN (SELECT numGroupID FROM AuthenticationGroupMaster WHERE tintGroupType=2)
------------------------------------------

DELETE FROM GroupTabDetails WHERE numTabId IN (1,4,5,7,80,112,132,133,134,135) AND numGroupId IN (SELECT numGroupId FROM AuthenticationGroupMaster WHERE tintGroupType=2)

-----------------------------------------

DECLARE @TEMP TABLE
(
	numTabID INT
	,numOrder INT
)
INSERT INTO @TEMP (numTabID,numOrder) VALUES (7,1),(80,2),(1,3),(134,4),(133,5),(135,6),(5,7),(4,8),(112,9),(132,10)

INSERT INTO GroupTabDetails
(
	numGroupId,numTabId,numRelationShip,bitallowed,numOrder
)
SELECT
	numGroupID,numTabID,0,1,numOrder
FROM
	AuthenticationGroupMaster,@TEMP
WHERE
	tintGroupType=2

-----------------------------------------

DELETE FROM TreeNavigationAuthorization WHERE numTabID=1 AND numGroupID IN (SELECT numGroupID FROM AuthenticationGroupMaster WHERE tintGroupType=2)

------------------------------------------

DECLARE @TEMP TABLE
(
	numPageNavID INT
)
INSERT INTO @TEMP (numPageNavID) VALUES (122),(123),(125),(149)

INSERT INTO TreeNavigationAuthorization
(
	numDomainID,numGroupID,numTabID,numPageNavID,bitVisible,tintType
)
SELECT
	numDomainID,numGroupID,1,numPageNavID,1,1
FROM
	AuthenticationGroupMaster,@TEMP
WHERE
	tintGroupType=2

-----------------------------------------

DELETE FROM TreeNavigationAuthorization WHERE numTabID=4 AND numGroupID IN (SELECT numGroupID FROM AuthenticationGroupMaster WHERE tintGroupType=2)

------------------------------------------

DECLARE @TEMP TABLE
(
	numPageNavID INT
)
INSERT INTO @TEMP (numPageNavID) VALUES (246),(247),(248)

INSERT INTO TreeNavigationAuthorization
(
	numDomainID,numGroupID,numTabID,numPageNavID,bitVisible,tintType
)
SELECT
	numDomainID,numGroupID,4,numPageNavID,1,1
FROM
	AuthenticationGroupMaster,@TEMP
WHERE
	tintGroupType=2

-----------------------------------------

DELETE FROM TreeNavigationAuthorization WHERE numTabID=5 AND numGroupID IN (SELECT numGroupID FROM AuthenticationGroupMaster WHERE tintGroupType=2)

------------------------------------------

DECLARE @TEMP TABLE
(
	numPageNavID INT
)
INSERT INTO @TEMP (numPageNavID) VALUES (279),(280)

INSERT INTO TreeNavigationAuthorization
(
	numDomainID,numGroupID,numTabID,numPageNavID,bitVisible,tintType
)
SELECT
	numDomainID,numGroupID,5,numPageNavID,1,1
FROM
	AuthenticationGroupMaster,@TEMP
WHERE
	tintGroupType=2

-----------------------------------------

DELETE FROM TreeNavigationAuthorization WHERE numTabID=7 AND numGroupID IN (SELECT numGroupID FROM AuthenticationGroupMaster WHERE tintGroupType=2)

------------------------------------------

DECLARE @TEMP TABLE
(
	numPageNavID INT
)
INSERT INTO @TEMP (numPageNavID) VALUES (6),(7),(11),(12)

INSERT INTO TreeNavigationAuthorization
(
	numDomainID,numGroupID,numTabID,numPageNavID,bitVisible,tintType
)
SELECT
	numDomainID,numGroupID,7,numPageNavID,1,1
FROM
	AuthenticationGroupMaster,@TEMP
WHERE
	tintGroupType=2

-----------------------------------------

DELETE FROM TreeNavigationAuthorization WHERE numTabID=80 AND numGroupID IN (SELECT numGroupID FROM AuthenticationGroupMaster WHERE tintGroupType=2)

------------------------------------------

DECLARE @TEMP TABLE
(
	numPageNavID INT
)
INSERT INTO @TEMP (numPageNavID) VALUES (62),(1),(2),(3),(4),(71),(72),(76),(171),(240)

INSERT INTO TreeNavigationAuthorization
(
	numDomainID,numGroupID,numTabID,numPageNavID,bitVisible,tintType
)
SELECT
	numDomainID,numGroupID,80,numPageNavID,1,1
FROM
	AuthenticationGroupMaster,@TEMP
WHERE
	tintGroupType=2

-----------------------------------------

DELETE FROM TreeNavigationAuthorization WHERE numTabID=132 AND numGroupID IN (SELECT numGroupID FROM AuthenticationGroupMaster WHERE tintGroupType=2)

------------------------------------------

DECLARE @TEMP TABLE
(
	numPageNavID INT
)
INSERT INTO @TEMP (numPageNavID) VALUES (270),(84)

INSERT INTO TreeNavigationAuthorization
(
	numDomainID,numGroupID,numTabID,numPageNavID,bitVisible,tintType
)
SELECT
	numDomainID,numGroupID,132,numPageNavID,1,1
FROM
	AuthenticationGroupMaster,@TEMP
WHERE
	tintGroupType=2

-----------------------------------------

DELETE FROM TreeNavigationAuthorization WHERE numTabID=133 AND numGroupID IN (SELECT numGroupID FROM AuthenticationGroupMaster WHERE tintGroupType=2)

------------------------------------------

DECLARE @TEMP TABLE
(
	numPageNavID INT
)
INSERT INTO @TEMP (numPageNavID) VALUES (273),(124),(126),(207)

INSERT INTO TreeNavigationAuthorization
(
	numDomainID,numGroupID,numTabID,numPageNavID,bitVisible,tintType
)
SELECT
	numDomainID,numGroupID,133,numPageNavID,1,1
FROM
	AuthenticationGroupMaster,@TEMP
WHERE
	tintGroupType=2

-----------------------------------------

DELETE FROM TreeNavigationAuthorization WHERE numTabID=134 AND numGroupID IN (SELECT numGroupID FROM AuthenticationGroupMaster WHERE tintGroupType=2)

------------------------------------------

DECLARE @TEMP TABLE
(
	numPageNavID INT
)
INSERT INTO @TEMP (numPageNavID) VALUES (13)

INSERT INTO TreeNavigationAuthorization
(
	numDomainID,numGroupID,numTabID,numPageNavID,bitVisible,tintType
)
SELECT
	numDomainID,numGroupID,134,numPageNavID,1,1
FROM
	AuthenticationGroupMaster,@TEMP
WHERE
	tintGroupType=2

-----------------------------------------

DELETE FROM TreeNavigationAuthorization WHERE numTabID=135 AND numGroupID IN (SELECT numGroupID FROM AuthenticationGroupMaster WHERE tintGroupType=2)

------------------------------------------

DECLARE @TEMP TABLE
(
	numPageNavID INT
)
INSERT INTO @TEMP (numPageNavID) VALUES (274),(275)

INSERT INTO TreeNavigationAuthorization
(
	numDomainID,numGroupID,numTabID,numPageNavID,bitVisible,tintType
)
SELECT
	numDomainID,numGroupID,135,numPageNavID,1,1
FROM
	AuthenticationGroupMaster,@TEMP
WHERE
	tintGroupType=2