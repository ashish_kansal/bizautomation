/******************************************************************
Project: Inventory Adjustment Date: 19.01.2013
Comments: 
*******************************************************************/
BEGIN TRANSACTION
GO


--SELECT TOP 50 * FROM dbo.General_Journal_Details WHERE numDomainId=171 ORDER BY numTransactionId DESC 
--SELECT TOP 50 * FROM dbo.General_Journal_Header  ORDER BY numJournal_Id DESC 


IF NOT EXISTS(SELECT * FROM dbo.AccountingChargeTypes WHERE chChargeCode='IA' )
BEGIN
INSERT INTO dbo.AccountingChargeTypes
        ( vcChageType ,
          vcChargeAccountCode ,
          chChargeCode
        )
VALUES  ( 'Inventory Adjustment' , -- vcChageType - varchar(100)
          '' , -- vcChargeAccountCode - varchar(50)
          'IA'  -- chChargeCode - char(10)
        )
	
END


UPDATE dbo.General_Journal_Details SET varDescription= 'Inventory_Adjustment',chBizDocItems='IA1'  WHERE numItemID>0 AND numJournalId IN (SELECT numJournal_Id FROM dbo.General_Journal_Header WHERE varDescription LIKE 'Stock Adjustment:%') 
SELECT * FROM dbo.General_Journal_Details WHERE varDescription <> 'Inventory_Adjustment' AND numJournalId IN (SELECT numJournal_Id FROM dbo.General_Journal_Header WHERE varDescription LIKE 'Stock Adjustment:%') 


DECLARE @v sql_variant 
SET @v = N'Project Expenses->PE   Project Time ->PT  Opportunity Expenses->OE Opportunity Time->OT,Inventory Adjustment->IA1, COGS->IA'
EXECUTE sp_updateextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'General_Journal_Details', N'COLUMN', N'chBizDocItems'
GO


IF NOT EXISTS(SELECT * FROM dbo.AccountingChargeTypes WHERE chChargeCode='OE' )
BEGIN
INSERT INTO dbo.AccountingChargeTypes
        ( vcChageType ,
          vcChargeAccountCode ,
          chChargeCode
        )
VALUES  ( 'Opening Balance Equity' , -- vcChageType - varchar(100)
          '' , -- vcChargeAccountCode - varchar(50)
          'OE'  -- chChargeCode - char(10)
        )
	
END

/*Profit and loss formula */
DECLARE @numDomainID NUMERIC(9)
DECLARE @TotalIncome MONEY;
DECLARE @TotalExpense MONEY;
DECLARE @TotalCOGS MONEY;
DECLARE @PLCHARTID NUMERIC(9)
DECLARE @PLOPENING MONEY;
DECLARE @CURRENTPL MONEY ;

SET @numDomainID = 153 

SELECT @TotalIncome = ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM view_journal VJ WHERE numDomainID=@numDomainID AND vcAccountCode  LIKE '0103%'
SELECT @TotalExpense = ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM view_journal VJ WHERE numDomainID=@numDomainID AND vcAccountCode  LIKE '0104%'
SELECT @TotalCOGS =  ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM view_journal VJ WHERE numDomainID=@numDomainID AND vcAccountCode  LIKE '0106%'
PRINT 'TotalIncome=									' + CAST(@TotalIncome AS VARCHAR(20))
PRINT 'TotalExpense=									'+ CAST(@TotalExpense AS VARCHAR(20))
PRINT 'TotalCOGS=										'+ CAST(@TotalCOGS AS VARCHAR(20))
SET @CURRENTPL = (@TotalIncome + @TotalExpense + @TotalCOGS)
PRINT 'Current Profit/Loss = (Income - expense - cogs)= ' + CAST( @CURRENTPL AS VARCHAR(20))


SELECT @PLCHARTID = numAccountId FROM dbo.Chart_Of_Accounts WHERE numDomainId=@numDomainID AND bitProfitLoss=1
--PRINT 'PL Account ID=' +CAST( @PLCHARTID AS VARCHAR(20))
SELECT @PLOPENING = ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM view_journal VJ WHERE numDomainID=@numDomainID AND numAccountId=@PLCHARTID
PRINT 'PL Account Opening=' +CAST( @PLOPENING AS VARCHAR(20))
PRINT 'PL = ' + CAST( @PLOPENING + @CURRENTPL AS VARCHAR(20))





-----Maintanance task

/*Step 1: Opening balance equity account must be created befre this task TO executed

step 2* disable validation on fy closing */
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ValidateFinancialYearClosingDate' ) 
    DROP PROCEDURE USP_ValidateFinancialYearClosingDate
GO

CREATE PROCEDURE USP_ValidateFinancialYearClosingDate
@numDomainID NUMERIC,
@dtEntryDate DATETIME -- pass date on which journal will be passed to accounting 
AS 
BEGIN
--	IF EXISTS(SELECT * FROM dbo.FinancialYear WHERE numDomainId=@numDomainID AND ( ISNULL(bitCloseStatus,0)=1 OR ISNULL(bitAuditStatus,0)=1 )
--	AND @dtEntryDate BETWEEN dtPeriodFrom AND dtPeriodTo)
--	BEGIN
--			RAISERROR ('FY_CLOSED',16,1);
            RETURN -1
--	END
	
END
--> Step 3 :Run Opening balance journal entry creating task USP_GetCOAwithOpeningBalance_Temp

--> Step 4 :
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ValidateFinancialYearClosingDate' ) 
    DROP PROCEDURE USP_ValidateFinancialYearClosingDate
GO

CREATE PROCEDURE USP_ValidateFinancialYearClosingDate
@numDomainID NUMERIC,
@dtEntryDate DATETIME=NULL, -- pass date on which journal will be passed to accounting 
@tintRecordType AS TINYINT=0,
@numRecordID AS NUMERIC=0
AS 
BEGIN
	IF @tintRecordType=0 --General for Date
	BEGIN
		IF EXISTS(SELECT * FROM dbo.FinancialYear WHERE numDomainId=@numDomainID AND ( ISNULL(bitCloseStatus,0)=1 OR ISNULL(bitAuditStatus,0)=1 )
			AND @dtEntryDate BETWEEN dtPeriodFrom AND dtPeriodTo)
			BEGIN
				RAISERROR ('FY_CLOSED',16,1);
				RETURN -1
			END
	END
	ELSE IF @tintRecordType=1 --For Item Delete
	BEGIN
		IF EXISTS(SELECT GJD.numJournalId FROM dbo.General_Journal_Header GJH JOIN dbo.General_Journal_Details GJD ON GJH.numJournal_Id = GJD.numJournalId
	       JOIN FinancialYear FY ON GJH.numDomainId = FY.numDomainId WHERE GJH.numDomainId=@numDomainID AND GJD.numItemID=@numRecordID AND ( ISNULL(FY.bitCloseStatus,0)=1 OR ISNULL(FY.bitAuditStatus,0)=1 )
			AND GJH.datEntry_Date BETWEEN FY.dtPeriodFrom AND FY.dtPeriodTo )
			BEGIN
				RAISERROR ('FY_CLOSED',16,1);
				RETURN -1
			END
	END	
	ELSE IF @tintRecordType=2 --For Opportunity Delete
	BEGIN
		IF EXISTS(SELECT GJD.numJournalId FROM dbo.General_Journal_Header GJH JOIN dbo.General_Journal_Details GJD ON GJH.numJournal_Id = GJD.numJournalId
	       JOIN FinancialYear FY ON GJH.numDomainId = FY.numDomainId WHERE GJH.numDomainId=@numDomainID AND GJH.numOppId=@numRecordID AND ( ISNULL(FY.bitCloseStatus,0)=1 OR ISNULL(FY.bitAuditStatus,0)=1 )
			AND GJH.datEntry_Date BETWEEN FY.dtPeriodFrom AND FY.dtPeriodTo )
			BEGIN
				RAISERROR ('FY_CLOSED',16,1);
				RETURN -1
			END
	END	
	
	
END



--UPDATE dbo.General_Journal_Details SET varDescription= 'Inventory_Adjustment' WHERE numItemID>0 AND numJournalId IN (SELECT numJournal_Id FROM dbo.General_Journal_Header WHERE varDescription LIKE 'Stock Adjustment:%') 
ROLLBACK TRANSACTION
--========================================================================
/******************************************************************
Project: BACRMUI   Date: 19.Feb.2013
Comments: BUG ID:267
*******************************************************************/
BEGIN TRANSACTION

-------- INSERT 'Relationship Type','Group' Fields in DycFormField_Mapping
DECLARE @numRelationShipTypeFieldID AS NUMERIC(9)
SELECT @numRelationShipTypeFieldID = MAX(numFieldID) FROM dbo.DycFieldMaster WHERE vcDbColumnName = 'tintCRMType' AND vcFieldName = 'Relationship Type'
PRINT @numRelationShipTypeFieldID

INSERT INTO dbo.DycFormField_Mapping 
(numModuleID,numFieldID,numDomainID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,PopupFunctionName,[order],tintRow,tintColumn,
bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitImport,bitExport,bitAllowFiltering,bitRequired,
numFormFieldID,intSectionID,bitAllowGridColor )
SELECT  2,@numRelationShipTypeFieldID,NULL,1,1,0,'Relationship Type','SelectBox','CRMType','',0,0,0,	0,	0,	0,	1,	0,	0,	0,	0,	1,	0,	0,	0,	0,	1,	0

UPDATE dbo.DycFieldMaster SET vcListItemType = 'SYS' WHERE vcFieldName = 'Relationship Type'
--SELECT * FROM View_DynamicDefaultColumns WHERE numDomainID = 1 and bitAllowEdit = 1 AND numformid =1

ROLLBACK

/******************************************************************
Project: BACRMUI   Date: 15.Feb.2013
Comments: Import Item,Warehouse Change
*******************************************************************/
BEGIN TRANSACTION

--SELECT * FROM dbo.DycFormField_Mapping WHERE numFormID = 48 AND vcFieldName IN ('Serial/Lot #s','Comments','Qty')
UPDATE dbo.DycFormField_Mapping SET bitImport = 1 WHERE numFormID = 48 AND vcFieldName IN ('Serial/Lot #s','Comments','Qty')

ROLLBACK

/******************************************************************
Project: BACRMUI   Date: 15.Feb.2013
Comments: Import Item,Warehouse Change
*******************************************************************/
BEGIN TRANSACTION

UPDATE dbo.DycFieldMaster SET vcLookBackTableName = 'Vendor' WHERE vcFieldName = 'Vendor' AND vcDbColumnName = 'numVendorID'

ROLLBACK

/******************************************************************
Project: BACRMUI   Date: 13.Feb.2013
Comments: Import Item,Warehouse Change
*******************************************************************/
BEGIN TRANSACTION

INSERT INTO dbo.DycFormSectionDetail(numFormID,intSectionID,vcSectionName,Loc_id) 
SELECT  20,3,'Item Warehouse Details',0
UNION ALL
SELECT  20,4,'Item Vendor Details',0

INSERT INTO dbo.DycFormField_Mapping 
(numModuleID,numFieldID,numDomainID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,PopupFunctionName,[order],tintRow,tintColumn,
bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitImport,bitExport,bitAllowFiltering,bitRequired,numFormFieldID,
intSectionID,	bitAllowGridColor) 
SELECT numModuleID,numFieldID,numDomainID,20,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,PopupFunctionName,[order],tintRow,tintColumn,
bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitImport,bitExport,bitAllowFiltering,bitRequired,numFormFieldID,
3,	bitAllowGridColor
FROM dbo.DycFormField_Mapping 
WHERE numFormID = 48 
AND ISNULL(bitImport,0) = 1
AND vcFieldName NOT IN ('Item','Item Code')
UNION ALL 
SELECT numModuleID,numFieldID,numDomainID,20,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,PopupFunctionName,[order],tintRow,tintColumn,
bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitImport,bitExport,bitAllowFiltering,bitRequired,numFormFieldID,
4,	bitAllowGridColor
FROM dbo.DycFormField_Mapping 
WHERE numFormID = 55
AND ISNULL(bitImport,0) = 1
AND vcFieldName NOT IN ('Item','Item ID')

SELECT * FROM dbo.DycFormField_Mapping WHERE numFormID = 20 ORDER BY numFieldID
exec USP_GET_DYNAMIC_IMPORT_FIELDS @numFormID=20,@intMode=2,@numImportFileID=0,@numDomainID=1
EXEC dbo.USP_GetItemForUpdate @numDomainID = 1 --  numeric(9, 0)

ROLLBACK
---------------


/******************************************************************
Project: BACRMUI   Date: 11.Feb.2013
Comments: Default Site Entry
*******************************************************************/
BEGIN TRANSACTION
SET IDENTITY_INSERT dbo.Sites ON
INSERT INTO dbo.Sites 
(numSiteID,vcSiteName,vcDescription,vcHostName,numCreatedBy,dtCreateDate,numModifiedBy,dtModifiedDate,numDomainID,bitIsActive,vcLiveURL,numCurrencyID,bitOnePageCheckout,
 tintRateType,bitIsMaintainScroll) 
 VALUES ( 
	-1,
	/* vcSiteName - varchar(50) */ 'Default Generic Site',
	/* vcDescription - varchar(1000) */ 'Do Not Delete',
	/* vcHostName - varchar(500) */ '',
	/* numCreatedBy - numeric(18, 0) */ 0,
	/* dtCreateDate - datetime */ '2013-2-11 19:30:45.826',
	/* numModifiedBy - numeric(18, 0) */ 0,
	/* dtModifiedDate - datetime */ '2013-2-11 19:30:45.826',
	/* numDomainID - numeric(18, 0) */ -1,
	/* bitIsActive - bit */ 1,
	/* vcLiveURL - varchar(100) */ '',
	/* numCurrencyID - numeric(18, 0) */ 0,
	/* bitOnePageCheckout - bit */ 0,
	/* tintRateType - tinyint */ 0,
	/* bitIsMaintainScroll - bit */ 0 ) 

SET IDENTITY_INSERT dbo.Sites OFF	
SELECT * FROM dbo.Sites WHERE vcSiteName = 'Default Generic Site'  
ROLLBACK

---------------
/******************************************************************
Project: BACRMUI   Date: 06.Feb.2013
Comments: Sales Order Modification
*******************************************************************/
BEGIN TRANSACTION

ALTER TABLE OpportunityMaster ADD vcCouponCode VARCHAR(100)

ROLLBACK
--
--/******************************************************************
--Project: BACRMUI   Date: 04.Feb.2013
--Comments: Sales Order/Shipping Report Modification
--*******************************************************************/
--BEGIN TRANSACTION
--
ALTER TABLE dbo.ShippingReport ADD numOppID NUMERIC(18,0)
--ALTER TABLE dbo.ShippingReportItems ADD numOppItemID NUMERIC(18,0)
--
--ROLLBACK


/******************************************************************
Project: BACRMUI   Date: 01.Feb.2013
Comments: Shipping Report Modification
*******************************************************************/
BEGIN TRANSACTION

ALTER TABLE dbo.ShippingBox ADD fltDimensionalWeight FLOAT

ROLLBACK

/******************************************************************
Project: BACRMUI/Bizservice   Date: 29.01.2013
Comments: Inserting new records for a Marketplace, Item Category into DynamicFormMaster,DycFormField_Mapping
*******************************************************************/
BEGIN TRANSACTION
INSERT INTO dbo.DycFieldMaster 
(numModuleID,numDomainID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,
vcToolTip,vcListItemType,numListID,PopupFunctionName,[order],tintRow,tintColumn,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,
bitDetailField,bitAllowSorting,bitWorkFlowField,bitImport,bitExport,bitAllowFiltering,bitInlineEdit,bitRequired,intColumnWidth,intFieldMaxLength ) 
SELECT 4,NULL,'Marketplace','vcExportToAPI','vcExportToAPI','ListOfAPI','Item','V','R','SelectBox',
NULL,'',0,NULL,NULL,NULL,1,0,0,0,0,0,0,0,
NULL,NULL,1,NULL,NULL,	NULL,	NULL,	NULL,NULL

DECLARE @numFieldID AS NUMERIC(9)
SELECT @numFieldID = numFieldID FROM dbo.DycFieldMaster WHERE vcFieldName = 'Marketplace' AND vcDbColumnName = 'vcExportToAPI'
PRINT @numFieldID

--SELECT * FROM dbo.DycFieldMaster WHERE vcFieldName = 'Marketplace' AND vcDbColumnName = 'vcExportToAPI'

INSERT INTO dbo.DycFormField_Mapping 
(numModuleID,numFieldID,numDomainID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,PopupFunctionName,[order],tintRow,tintColumn,
bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitImport,bitExport,bitAllowFiltering,bitRequired,numFormFieldID,
intSectionID,bitAllowGridColor) 
SELECT MASTER.numModuleID,MASTER.numFieldID,MASTER.numDomainID,20,MASTER.bitAllowEdit,MASTER.bitInlineEdit,MASTER.vcFieldName,
MASTER.vcAssociatedControlType,'ListOfCategories',MASTER.PopupFunctionName,MASTER.[order],1,1,
1,0,0,0,0,0,0,0,1,0,0,0,0,1,NULL
FROM dbo.DycFieldMaster MASTER 
WHERE vcFieldName = 'Item Category' 
AND vcDbColumnName = 'numCategoryID'

UNION ALL

SELECT MASTER.numModuleID,MASTER.numFieldID,MASTER.numDomainID,20,MASTER.bitAllowEdit,MASTER.bitInlineEdit,MASTER.vcFieldName,
MASTER.vcAssociatedControlType,MASTER.vcPropertyName,MASTER.PopupFunctionName,MASTER.[order],1,1,
1,0,0,0,0,0,0,0,1,0,0,0,0,1,NULL
FROM dbo.DycFieldMaster MASTER 
WHERE vcFieldName = 'Marketplace' AND vcDbColumnName = 'vcExportToAPI'

--SELECT * FROM dbo.DycFormField_Mapping WHERE numFormID = 20 AND bitImport = 1
UPDATE DycFormField_Mapping SET vcPropertyName = 'ListOfCategories' WHERE numFormID = 20 AND vcFieldName = 'Item Category'
ROLLBACK 


/******************************************************************
Project: BACRMUI/Bizservice   Date: 29.01.2013
Comments: 
*******************************************************************/
BEGIN TRANSACTION
--SELECT * FROM dbo.DycFieldMaster WHERE vcFieldName = 'Item Detail ID' AND vcDbColumnName = 'numItemDetailID'
UPDATE dbo.DycFieldMaster SET vcLookBackTableName = 'ItemDetails'
WHERE vcFieldName = 'Item Detail ID' AND vcDbColumnName = 'numItemDetailID'

UPDATE dbo.DycFieldMaster SET vcLookBackTableName = 'Item' WHERE vcDbColumnName = 'numItemCode' AND vcLookBackTableName = 'ItemImages' 

ROLLBACK

/******************************************************************
Project: BACRMUI/Bizservice   Date: 26.01.2013
Comments: Inserting new records for a web api detail into DynamicFormMaster,DycFormField_Mapping
*******************************************************************/
BEGIN TRANSACTION

INSERT INTO dbo.DynamicFormMaster (
	numFormId,
	vcFormName,
	cCustomFieldsAssociated,
	cAOIAssociated,
	bitDeleted,
	tintFlag,
	bitWorkFlow,
	vcLocationID,
	bitAllowGridColor
) 

SELECT 65,'Item Web Api Detail','N','N',0,1,NULL,NULL,NULL

DECLARE @numFormID AS NUMERIC(10,0)
SELECT @numFormID = numFormID FROM dbo.DynamicFormMaster WHERE vcFormName = 'Item Web Api Detail'
PRINT @numFormID

INSERT INTO dbo.DycFormField_Mapping 
(numModuleID,numFieldID,numDomainID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,PopupFunctionName,[order],tintRow,tintColumn,
bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitImport,bitExport,bitAllowFiltering,bitRequired,numFormFieldID,
intSectionID,bitAllowGridColor) 
SELECT MAPPING.numModuleID,MASTER.numFieldID,MAPPING.numDomainID,@numFormID,MAPPING.bitAllowEdit,MAPPING.bitInlineEdit,MASTER.vcFieldName,
MASTER.vcAssociatedControlType,MASTER.vcPropertyName,MASTER.PopupFunctionName,MASTER.[order],MASTER.tintRow,MASTER.tintColumn,
1,0,0,0,0,0,0,0,1,0,0,0,MAPPING.numFormFieldID,MAPPING.intSectionID,MAPPING.bitAllowGridColor
FROM dbo.DycFormField_Mapping MAPPING
JOIN dbo.DycFieldMaster MASTER ON MASTER.numFieldId = MAPPING.numFieldID
WHERE 1=1
AND numFormID = 50
AND MASTER.numFieldId NOT IN (406)

UNION ALL

SELECT MAPPING.numModuleID,MAPPING.numFieldID,MAPPING.numDomainID,@numFormID,0,MAPPING.bitInlineEdit,MAPPING.vcFieldName,
MAPPING.vcAssociatedControlType,MAPPING.vcPropertyName,MAPPING.PopupFunctionName,1,MAPPING.tintRow,MAPPING.tintColumn,
MAPPING.bitInResults,MAPPING.bitDeleted,0,0,0,0,0,0,1,0,0,0,NULL,1,NULL
FROM dbo.DycFieldMaster MAPPING
WHERE 1=1
AND vcFieldName = 'Item Code' AND vcDbColumnName = 'numItemCode'

SELECT * FROM dbo.DycFormField_Mapping WHERE numFormID = @numFormID
--SELECT * FROM dbo.DynamicFormMaster WHERE numFormID = 50

 INSERT INTO dbo.DycFormSectionDetail 
 (numFormID,intSectionID,vcSectionName,Loc_id) 
SELECT 65,	1,	'More Item Details',0
UNION ALL
SELECT 65,	2,	'Amazon',0
UNION ALL
SELECT 65,	3,	'E-Bay',0
UNION ALL
SELECT 65,	4,	'Google',0
UNION ALL
SELECT 65,	5,	'Magento',0

ROLLBACK 


--========================================================================
/************************************************************************************************/
/************************28_Jan_2013*******************************************************************/
/************************************************************************************************/

ALTER TABLE dbo.Domain  ALTER COLUMN
	bitCreateInvoice TINYINT NULL


ALTER TABLE dbo.Domain ADD
	numSOBizDocStatus numeric(18, 0) NULL
	
--------------------------------------------------------------------------	

ALTER TABLE dbo.Domain ADD
	bitAutolinkUnappliedPayment bit NULL	
	

INSERT INTO dbo.AccountingChargeTypes (
	vcChageType,
	vcChargeAccountCode,
	chChargeCode
) VALUES ( 
	/* vcChageType - varchar(100) */ 'Purchase Clearing',
	/* vcChargeAccountCode - varchar(50) */ '010201',
	/* chChargeCode - char(10) */ 'PC' ) 	


INSERT INTO dbo.AccountingChargeTypes (
	vcChageType,
	vcChargeAccountCode,
	chChargeCode
) VALUES ( 
	/* vcChageType - varchar(100) */ 'Purchase Price Variance',
	/* vcChargeAccountCode - varchar(50) */ '0106',
	/* chChargeCode - char(10) */ 'PV' ) 	
	
--------------------------------------------------------------------------	

ALTER TABLE dbo.OppWarehouseSerializedItem ADD
	numReturnHeaderID numeric(18, 0) NULL,
	numReturnItemID numeric(18, 0) NULL	

--------------------------------------------------------------------------	

ALTER TABLE dbo.OpportunityBizDocs ADD
	fltExchangeRateBizDoc float(53) NULL	


--ALTER TABLE dbo.Domain ADD
--	bitAllowPPVariance bit NULL

--------------------------------------------------------------------------	

--SELECT * FROM dbo.General_Journal_Details WHERE tintReferenceType=6 and numCustomerID=0

UPDATE GJD SET numCustomerID=(SELECT TOP 1 numCustomerID FROM General_Journal_Details WHERE tintReferenceType=7 AND GJD.numJournalID=numJournalID) 
FROM General_Journal_Details GJD
WHERE tintReferenceType=6 and numCustomerID=0

--------------------------------------------------------------------------	
ALTER TABLE dbo.TempARRecord ADD
	monUnAppliedAmount money NULL

ALTER TABLE dbo.TempARRecord1 ADD
	monUnAppliedAmount money NULL
	
ALTER TABLE dbo.TempARRecord1 ADD
	numTotal money NULL

	
	
--------------------------------------------------------------------------	
--SELECT OM.numDomainId,OM.tintSourceType,monAmountPaid,OBD.monDealAmount,OBD.* FROM dbo.OpportunityBizDocs OBD JOIN dbo.OpportunityMaster OM ON OBD.numOppId = OM.numOppId
-- WHERE monAmountPaid > OBD.monDealAmount and OM.tintOppType=1 
 
 UPDATE OBD SET monAmountPaid=OBD.monDealAmount FROM dbo.OpportunityBizDocs OBD JOIN dbo.OpportunityMaster OM ON OBD.numOppId = OM.numOppId
 WHERE monAmountPaid > OBD.monDealAmount and OM.tintOppType=1  
 	
 
 --------------------------------------------------------------------------	
 --SELECT * FROM BillPaymentHeader BPH WHERE monPaymentAmount IS NULL

 UPDATE BillPaymentHeader SET monAppliedAmount=(SELECT ISNULL(SUM(ISNULL(monAmount,0)),0) FROM BillPaymentDetails WHERE numBillPaymentID=BillPaymentHeader.numBillPaymentID),
 								monPaymentAmount=(SELECT ISNULL(SUM(ISNULL(monAmount,0)),0) FROM BillPaymentDetails WHERE numBillPaymentID=BillPaymentHeader.numBillPaymentID)
  							WHERE monPaymentAmount IS NULL 
 
 --------------------------------------------------------------------------	
 --SELECT * FROM dbo.General_Journal_Header WHERE numBillPaymentID>0 AND numOppId>0 AND numOppBizDocsId>0
 UPDATE dbo.General_Journal_Header SET numOppId=NULL,numOppBizDocsId=NULL WHERE numBillPaymentID>0 AND numOppId>0 AND numOppBizDocsId>0
  	
 
  --------------------------------------------------------------------------	
  	SELECT GJH.numAmount,OBD.monDealAmount,OM.tintOppType
	 FROM  dbo.General_Journal_Header GJH 
	INNER JOIN dbo.OpportunityBizDocs OBD ON GJH.numOppBizDocsId = OBD.numOppBizDocsId AND GJH.numOppId = OBD.numOppId
	JOIN dbo.OpportunityMaster OM ON OM.numOppId = OBD.numOppId 
	WHERE GJH.numAmount!=OBD.monDealAmount  AND OBD.bitAuthoritativeBizDocs=1
	
	
 UPDATE GJH SET numAmount=(SELECT SUM(numDebitAmt) FROM dbo.General_Journal_Details WHERE numJournalID=GJH.numJournal_ID)
	 FROM  dbo.General_Journal_Header GJH 
	INNER JOIN dbo.OpportunityBizDocs OBD ON GJH.numOppBizDocsId = OBD.numOppBizDocsId AND GJH.numOppId = OBD.numOppId
	JOIN dbo.OpportunityMaster OM ON OM.numOppId = OBD.numOppId 
	WHERE GJH.numAmount!=OBD.monDealAmount  AND OBD.bitAuthoritativeBizDocs=1				
						

  --------------------------------------------------------------------------	
ALTER TABLE dbo.OpportunityMaster ADD
	dtItemReceivedDate datetime NULL				 	
	
	

  --------------------------------------------------------------------------	
UPDATE BPD SET dtAppliedDate=BPH.dtPaymentDate FROM BillPaymentHeader BPH JOIN BillPaymentDetails BPD 
ON BPH.numBillPaymentID = BPD.numBillPaymentID
WHERE BPD.dtAppliedDate IS NULL
	
	
  --------------------------------------------------------------------------	
ALTER TABLE dbo.OpportunityMaster ADD
	bitPPVariance bit NULL	
	
	
  --------------------------------------------------------------------------
  ALTER TABLE dbo.WareHouseItems_Tracking ADD
	numWareHouseItemTrackingID numeric(18, 0) NOT NULL IDENTITY (1, 1)
GO
ALTER TABLE dbo.WareHouseItems_Tracking ADD CONSTRAINT
	PK_WareHouseItems_Tracking PRIMARY KEY CLUSTERED 
	(
	numWareHouseItemTrackingID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

	
	ALTER TABLE dbo.WareHouseItems_Tracking ADD
	monAverageCost money NULL	
	
	ALTER TABLE dbo.WareHouseItems_Tracking ADD
	numTotalOnHand numeric(18, 0) NULL,
	dtRecordDate datetime NULL
	
--	INSERT INTO dbo.WareHouseItems_Tracking (
--	numWareHouseItemID,numOnHand,numOnOrder,numReorder,numAllocation,numBackOrder,numDomainID,
--	vcDescription,tintRefType,numReferenceID,dtCreatedDate,numModifiedBy,monAverageCost) 
--SELECT numWareHouseItemID,numOnHand,numOnOrder,numReorder,numAllocation,numBackOrder,I.numDomainID,
--	'Average Cost',1,numItemID,GETUTCDATE(),1,I.monAverageCost
-- 	FROM Item I JOIN WareHouseItems WHI ON I.numItemCode=WHI.numItemID
 	
 	
 --------------------------------------------------------------------------	

-- 	UPDATE DycFormConfigurationDetails SET numRelCntType=0,numVIewID=0 WHERE numFormID=21 AND numRelCntType>0
-- 	
-- 	
-- 	select numUserCntID,numDomainID,numFieldID,COUNT(*) from DycFormConfigurationDetails where numFormId=21 AND tintPageType = 1
--GROUP BY numUserCntID,numDomainID,numFieldID HAVING COUNT(*)>1
--
--
--WITH [CTE] AS
--(
--select numUserCntID,numDomainID,numFieldID,ROW_NUMBER() OVER(PARTITION BY numUserCntID,numDomainID,numFieldID 
--ORDER BY numFieldID) AS NUm  from DycFormConfigurationDetails where numFormId=21 AND tintPageType = 1
--)
--DELETE FROM CTE WHERE NUm > 1

 --------------------------------------------------------------------------	

--SELECT * FROM dbo.Chart_Of_Accounts WHERE vcAccountName LIKE 'Price Variance%'

--SELECT * FROM dbo.General_Journal_Details WHERE numChartAcntId IN(SELECT numAccountId FROM dbo.Chart_Of_Accounts WHERE vcAccountName LIKE 'Price Variance%')

--SELECT * FROM ChartAccountOpening WHERE numAccountId IN(SELECT numAccountId FROM dbo.Chart_Of_Accounts WHERE vcAccountName LIKE 'Price Variance%')

DELETE FROM ChartAccountOpening WHERE numAccountId IN(SELECT numAccountId FROM dbo.Chart_Of_Accounts WHERE vcAccountName LIKE 'Price Variance%')
DELETE FROM Chart_Of_Accounts WHERE vcAccountName LIKE 'Price Variance%'

  -------------Set Default Account for existing domain-------------------------------------------------------------	

BEGIN TRANSACTION

DECLARE @IA AS VARCHAR(10)       
SELECT @IA = ISNULL(numChargeTypeId,0) FROM dbo.AccountingChargeTypes WHERE chChargeCode='IA'

DECLARE @OE AS VARCHAR(10)       
SELECT @OE = ISNULL(numChargeTypeId,0) FROM dbo.AccountingChargeTypes WHERE chChargeCode='OE'

DECLARE @PC AS VARCHAR(10)       
SELECT @PC = ISNULL(numChargeTypeId,0) FROM dbo.AccountingChargeTypes WHERE chChargeCode='PC'

DECLARE @PV AS VARCHAR(10)       
SELECT @PV = ISNULL(numChargeTypeId,0) FROM dbo.AccountingChargeTypes WHERE chChargeCode='PV'

DECLARE  @maxDomainID NUMERIC(9)
DECLARE  @minDomainID NUMERIC(9)

DECLARE @IAAccountId NUMERIC(9),@OEAccountId NUMERIC(9),@PCAccountId NUMERIC(9),@PVAccountId NUMERIC(9)
DECLARE @numExpenseAccountTypeID  AS NUMERIC(9,0),@numEquityAccountTypeID  AS NUMERIC(9,0),@numLiabilityAccountTypeID  AS NUMERIC(9,0),@numCOGSAccountTypeID  AS NUMERIC(9,0)
DECLARE @numOtherDirectExpenseID AS NUMERIC(9),@numRSccountTypeID AS NUMERIC(9),@numCLAccountTypeID AS NUMERIC(9),@numCOGSExpenseID AS NUMERIC(9)

DECLARE @dtTodayDate AS DATETIME
        SET @dtTodayDate = GETDATE()
        
SELECT @maxDomainID = max([numDomainId]),@minDomainID = min([numDomainId]) FROM   [Domain] where numDomainId>0
WHILE @minDomainID <= @maxDomainID
  BEGIN
	 PRINT 'DomainID=' + CONVERT(VARCHAR(20),@minDomainID)
	 
	 SELECT @IAAccountId=0,@PCAccountId=0,@PVAccountId=0,@OEAccountId=0
	 SELECT @numExpenseAccountTypeID =0,@numEquityAccountTypeID=0,@numLiabilityAccountTypeID=0,@numCOGSAccountTypeID=0
	 SELECT @numOtherDirectExpenseID =0,@numRSccountTypeID =0,@numCLAccountTypeID =0,@numCOGSExpenseID =0

	 IF NOT EXISTS (SELECT 1 FROM AccountingCharges WHERE numDomainID=@minDomainID AND numChargeTypeId=@IA)
	 BEGIN
		 SELECT @numExpenseAccountTypeID = numAccountTypeId FROM dbo.AccountTypeDetail WHERE vcAccountCode ='0104' AND numDomainID = @minDomainID 
		 SELECT @numOtherDirectExpenseID = numAccountTypeID FROM dbo.AccountTypeDetail WHERE numDomainID = @minDomainID   AND vcAccountCode = '01040101' 

	 			EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numExpenseAccountTypeID,
			@numParntAcntTypeID = @numOtherDirectExpenseID, @vcAccountName = 'Inventory Adjustment',
			@vcAccountDescription = 'Inventory Adjustment',
			@monOriginalOpeningBal = $0, @monOpeningBal = $0,
			@dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = @IAAccountId output,
			@bitFixed = 0, @numDomainID = @minDomainID   , @numListItemID = 0, @numUserCntId = 1,
			@bitProfitLoss = 0, @bitDepreciation = 0,
			@dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
			@vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0
			
			INSERT INTO dbo.AccountingCharges (numChargeTypeId,numAccountID,numDomainID) 
				VALUES (@IA,@IAAccountId,@minDomainID) 
	 END

	 IF NOT EXISTS (SELECT 1 FROM AccountingCharges WHERE numDomainID=@minDomainID AND numChargeTypeId=@OE)
	 BEGIN
		 SELECT @numEquityAccountTypeID = numAccountTypeId FROM dbo.AccountTypeDetail WHERE vcAccountCode ='0105' AND numDomainID = @minDomainID  
		 SELECT @numRSccountTypeID = numAccountTypeID FROM dbo.AccountTypeDetail WHERE vcAccountCode = '01050101'  AND numDomainID = @minDomainID  

	 		EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numEquityAccountTypeID,
		@numParntAcntTypeID = @numRSccountTypeID, @vcAccountName = 'Opening Balance Equity',
		@vcAccountDescription = 'Opening Balance Equity', @monOriginalOpeningBal = $0,
		@monOpeningBal = $0, @dtOpeningDate = @dtTodayDate, @bitActive = 1,
		@numAccountId = @OEAccountId output, @bitFixed = 0, @numDomainID = @minDomainID   , @numListItemID = 0,
		@numUserCntId = 1, @bitProfitLoss = 0, @bitDepreciation = 0,
		@dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
		@vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0
		
		INSERT INTO dbo.AccountingCharges (numChargeTypeId,numAccountID,numDomainID) 
				VALUES (@OE,@OEAccountId,@minDomainID)
	 END
	 
	 IF NOT EXISTS (SELECT 1 FROM AccountingCharges WHERE numDomainID=@minDomainID AND numChargeTypeId=@PC)
	 BEGIN
		SELECT @numLiabilityAccountTypeID = numAccountTypeId FROM dbo.AccountTypeDetail WHERE vcAccountCode ='0102' AND numDomainID = @minDomainID 
		SELECT @numCLAccountTypeID = numAccountTypeID FROM dbo.AccountTypeDetail WHERE vcAccountCode = '01020102'  AND numDomainID = @minDomainID  

	 			EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numLiabilityAccountTypeID,
			@numParntAcntTypeID =  @numCLAccountTypeID, @vcAccountName = 'Purchase Clearing',
			@vcAccountDescription = 'Purchase Clearing',
			@monOriginalOpeningBal = $0, @monOpeningBal = $0,
			@dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = @PCAccountId output,
			@bitFixed = 0, @numDomainID = @minDomainID   , @numListItemID = 0, @numUserCntId = 1,
			@bitProfitLoss = 0, @bitDepreciation = 0,
			@dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
			@vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0
			
			INSERT INTO dbo.AccountingCharges (numChargeTypeId,numAccountID,numDomainID) 
				VALUES (@PC,@PCAccountId,@minDomainID)
	 END
	 
	 IF NOT EXISTS (SELECT 1 FROM AccountingCharges WHERE numDomainID=@minDomainID AND numChargeTypeId=@PV)
	 BEGIN
		SELECT @numCOGSAccountTypeID = numAccountTypeId FROM dbo.AccountTypeDetail WHERE vcAccountCode ='0106' AND numDomainID = @minDomainID  
		SELECT @numCOGSExpenseID = numAccountTypeID FROM dbo.AccountTypeDetail WHERE numDomainID = @minDomainID   AND vcAccountCode = '010601' 

	 			EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numCOGSAccountTypeID,
		@numParntAcntTypeID = @numCOGSExpenseID, @vcAccountName = 'Purchase Price Variance',
		@vcAccountDescription = 'Stores difference of purchase price when Cost of Product is different then what is entered in Purchase Order.',
		@monOriginalOpeningBal = $0, @monOpeningBal = $0,
		@dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = @PVAccountId output,
		@bitFixed = 0, @numDomainID = @minDomainID   , @numListItemID = 0, @numUserCntId = 1,
		@bitProfitLoss = 0, @bitDepreciation = 0,
		@dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
		@vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0
		
		INSERT INTO dbo.AccountingCharges (numChargeTypeId,numAccountID,numDomainID) 
				VALUES (@PV,@PVAccountId,@minDomainID)
	 END
	 
	 SELECT @minDomainID = min([numDomainId]) FROM   [Domain] WHERE  [numDomainId]> @minDomainID 
  END

ROLLBACK TRANSACTION


  -------------Average Cost : SO/PO Audit-------------------------------------------------------------	

BEGIN TRANSACTION

SELECT OI.numItemCode,OM.numDomainId,ROW_NUMBER() OVER(ORDER BY OI.numItemCode,OM.numDomainId) RowNO INTO #tempItem FROM OpportunityMaster OM JOIN OpportunityItems OI ON OM.numOppId = OI.numOppId
JOIN item I ON OI.numItemCode=I.numItemCOde WHERE tintOppType=2 AND tintshipped=1 AND I.charItemType='P' AND ISNULL(OI.numWarehouseItmsID,0)>0
GROUP BY OI.numItemCode,OM.numDomainId

DECLARE @minNo AS NUMERIC,@maxNo AS NUMERIC;
SELECT @minNo=MIN(RowNO),@maxNo=MAX(RowNO) FROM #tempItem

DECLARE @numOnHand AS NUMERIC,@monAvgCost AS MONEY
DECLARE @numItemCode AS NUMERIC,@numDomainId AS NUMERIC,@tintOppType AS NUMERIC
DECLARE @minOpp AS NUMERIC,@maxOpp AS NUMERIC;
DECLARE @numOppId AS NUMERIC,@numUnitHour AS NUMERIC,@numWarehouseItmsID AS NUMERIC,@monPrice AS MONEY,@bintAccountClosingDate AS DATETIME;

WHILE @minNo <= @maxNo
  BEGIN
	SELECT @numOnHand=0,@monAvgCost=0,@minOpp=0,@maxOpp=0,@numOppId=0,@numUnitHour=0,@numWarehouseItmsID=0,@monPrice=0,
	@numItemCode=0,@numDomainId=0,@tintOppType=0
	
	SELECT @numItemCode=numItemCode,@numDomainId=numDomainId FROM #tempItem WHERE RowNO=@minNo 	
	
	SELECT OM.tintOppType,OM.numOppId,numUnitHour,numWarehouseItmsID,isnull(monPrice,0) * (CASE WHEN ISNULL(fltExchangeRate,0)=0 THEN 1 ELSE fltExchangeRate END) AS monPrice,
	bintAccountClosingDate,ROW_NUMBER() OVER(ORDER BY bintAccountClosingDate) RowNO INTO #temp1 FROM dbo.OpportunityMaster OM JOIN dbo.OpportunityItems OI ON OM.numOppId = OI.numOppId
		WHERE tintshipped=1 AND numDomainId=@numDomainId AND numItemCode=@numItemCode AND ISNULL(OI.numWarehouseItmsID,0)>0
		ORDER BY bintAccountClosingDate 

	SELECT @minOpp=MIN(RowNO),@maxOpp=MAX(RowNO) FROM #temp1

	WHILE @minOpp <= @maxOpp
     BEGIN
		SELECT @numOppId=numOppId,@numUnitHour=numUnitHour,@numWarehouseItmsID=numWarehouseItmsID,@monPrice=monPrice,
			@bintAccountClosingDate=bintAccountClosingDate,@tintOppType=tintOppType FROM #temp1 WHERE RowNO=@minOpp
		
		IF @numUnitHour>0
		BEGIN
			PRINT CAST(@tintOppType AS VARCHAR(2)) + '::' + CAST(@numOnHand AS VARCHAR(10)) + '::'+ CAST(@numUnitHour AS VARCHAR(10))
			
			IF @tintOppType=1
			BEGIN
  			     IF @numOnHand>=@numUnitHour
  			     BEGIN
				   SET @numOnHand = @numOnHand - @numUnitHour	
  			     
  			      INSERT INTO dbo.WareHouseItems_Tracking (
						numWareHouseItemID,numOnHand,numOnOrder,numReorder,numAllocation,numBackOrder,
						numDomainID,vcDescription,tintRefType,numReferenceID,dtCreatedDate,numModifiedBy,
						monAverageCost,numTotalOnHand,dtRecordDate
					) VALUES ( @numWarehouseItmsID,@numOnHand,0,0,@numUnitHour,0,@numDomainId,'SO Created (Qty:'+ CAST(@numUnitHour AS VARCHAR(15)) +' Price:' + CAST(@monPrice AS VARCHAR(15)) +')',6,@numOppId,@bintAccountClosingDate,1,
					@monAvgCost,@numOnHand,@bintAccountClosingDate)  
					
				 INSERT INTO dbo.WareHouseItems_Tracking (
						numWareHouseItemID,numOnHand,numOnOrder,numReorder,numAllocation,numBackOrder,
						numDomainID,vcDescription,tintRefType,numReferenceID,dtCreatedDate,numModifiedBy,
						monAverageCost,numTotalOnHand,dtRecordDate
					) VALUES ( @numWarehouseItmsID,@numOnHand,0,0,0,0,@numDomainId,'SO Closed (Qty:'+ CAST(@numUnitHour AS VARCHAR(15)) +' Price:' + CAST(@monPrice AS VARCHAR(15)) +')',6,@numOppId,@bintAccountClosingDate,1,
					@monAvgCost,@numOnHand,@bintAccountClosingDate)  
				 END
			END
			ELSE
			BEGIN
					INSERT INTO dbo.WareHouseItems_Tracking (
						numWareHouseItemID,numOnHand,numOnOrder,numReorder,numAllocation,numBackOrder,
						numDomainID,vcDescription,tintRefType,numReferenceID,dtCreatedDate,numModifiedBy,
						monAverageCost,numTotalOnHand,dtRecordDate
					) VALUES ( @numWarehouseItmsID,@numOnHand,@numUnitHour,0,0,0,@numDomainId,'PO Created (Qty:'+ CAST(@numUnitHour AS VARCHAR(15)) +' Cost:' + CAST(@monPrice AS VARCHAR(15)) +')',6,@numOppId,@bintAccountClosingDate,1,
					@monAvgCost,@numOnHand,@bintAccountClosingDate) 
							
					SET @monAvgCost = ( ( @numOnHand * @monAvgCost )
													+ (@numUnitHour * @monPrice))
									/ ( @numOnHand + @numUnitHour )
				
					SET @numOnHand = @numOnHand + @numUnitHour	
				
					INSERT INTO dbo.WareHouseItems_Tracking (
						numWareHouseItemID,numOnHand,numOnOrder,numReorder,numAllocation,numBackOrder,
						numDomainID,vcDescription,tintRefType,numReferenceID,dtCreatedDate,numModifiedBy,
						monAverageCost,numTotalOnHand,dtRecordDate
					) VALUES ( @numWarehouseItmsID,@numOnHand,0,0,0,0,@numDomainId,'PO Closed (Qty:'+ CAST(@numUnitHour AS VARCHAR(15)) +' Cost:' + CAST(@monPrice AS VARCHAR(15)) +')',6,@numOppId,@bintAccountClosingDate,1,
					@monAvgCost,@numOnHand,@bintAccountClosingDate) 
									
			END
		END
		
		SET @minOpp = @minOpp + 1
	END
           
    INSERT INTO dbo.WareHouseItems_Tracking (
	numWareHouseItemID,numOnHand,numOnOrder,numReorder,numAllocation,numBackOrder,numDomainID,
	vcDescription,tintRefType,numReferenceID,dtCreatedDate,numModifiedBy,monAverageCost) 
	SELECT numWareHouseItemID,numOnHand,numOnOrder,numReorder,numAllocation,numBackOrder,I.numDomainID,
	'Biz: Calculated avg cost, old avg cost:' + CAST(CAST(ISNULL(I.monAverageCost,0) AS DECIMAL(18,4)) AS VARCHAR(15)),1,numItemID,GETUTCDATE(),1,@monAvgCost
 	FROM Item I JOIN WareHouseItems WHI ON I.numItemCode=WHI.numItemID WHERE I.numItemCode=@numItemCode
 	       
 	--UPDATE Item SET monAverageCost=@monAvgCost WHERE numItemCode=@numItemCode 
 	
    DROP TABLE #temp1                 
    
    SET @minNo = @minNo + 1
END
  
DROP TABLE #tempItem

ROLLBACK TRANSACTION 

--========================================================================
/******************************************************************
Project: Sales Tax Service Item Mapping Date: 19/02/2013
Comments:  
*******************************************************************/
BEGIN TRANSACTION
GO

ALTER TABLE dbo.WebAPIDetail ADD
	numSalesTaxItemMapping NUMERIC(18,0) NULL
GO
ROLLBACK TRANSACTION
/******************************************************************
Project: BizDoc status to create BizDoc  Date: 30/01/2013
Comments:  
*******************************************************************/

BEGIN TRANSACTION
GO

ALTER TABLE dbo.WebAPIDetail ADD
	bitEnableItemUpdate BIT NULL
GO

ALTER TABLE dbo.WebAPIDetail ADD
	bitEnableInventoryUpdate BIT NULL
GO

ALTER TABLE dbo.WebAPIDetail ADD
	bitEnableOrderImport BIT NULL
GO

ALTER TABLE dbo.WebAPIDetail ADD
	bitEnableTrackingUpdate BIT NULL
GO

ALTER TABLE dbo.WebAPIDetail ADD
	numBizDocStatusId varchar(100) NULL
GO

ROLLBACK TRANSACTION

/******************************************************************
Project: FBA Order Import Last Update Flag   Date: 26/01/2013
Comments:  
*******************************************************************/

BEGIN TRANSACTION
GO

ALTER TABLE dbo.WebAPI ADD
	vcSixteenthFldName VARCHAR(50) NULL

ALTER TABLE dbo.WebAPIDetail ADD
	vcSixteenthFldValue varchar(100) NULL
GO

ROLLBACK TRANSACTION
--========================================================================
******************************************************************
Project:    Date: 12-2-2013
Note : Issue found when testing on demo server.
*******************************************************************/

--Note its added in demo server 
BEGIN TRANSACTION

INSERT INTO dbo.ErrorMaster (
	vcErrorCode,
	vcErrorDesc,
	tintApplication
) VALUES ( 
	/* vcErrorCode - varchar(6) */ 'ERR071',
	/* vcErrorDesc - nvarchar(max) */ N'Please enter a valid postal code in the address.',
	/* tintApplication - tinyint */ 3 ) 
GO
ROLLBACK TRANSACTION

--19-02-2013
BEGIN TRANSACTION

ALTER TABLE ExtranetAccountsDtl 
ADD vcGoogleID VARCHAR(100) NULL

GO
ROLLBACK TRANSACTION

--21-02-2013
BEGIN TRANSACTION

UPDATE dbo.PageNavigationDTL SET numParentID = 73 WHERE numPageNavID = 187 AND vcPageNavName LIKE 'Promotions & Offers'
UPDATE dbo.PageNavigationDTL SET numParentID = 73 WHERE numPageNavID = 193 AND vcPageNavName LIKE 'Shipping Rule List'
GO
ROLLBACK TRANSACTION

BEGIN TRANSACTION
ALTER TABLE eCommercePaymentConfig
ADD  numFailedOrderStatus NUMERIC(18,0) null
GO
ROLLBACK TRANSACTION

BEGIN TRANSACTION
ALTER TABLE eCommercePaymentConfig
ADD  numBizDocStatus NUMERIC(18,0) null
GO
ROLLBACK TRANSACTION


/******************************************************************
Project:    Date: 23-1-2013
Comments: Note : After adding Error message it requires to click get Error Message button on Maintanance Page .
*******************************************************************/
--AppleNv web.config replace SMTPLicense with this for sending email of Mirror Bizdoc
<add key="SMTPLicense" value="MN700-D686805186065845A8EFDF190775-AA3C" />

BEGIN TRANSACTION
        ALTER TABLE dbo.SitePages
		ADD bitIsMaintainScroll bit NOT NULL
		CONSTRAINT MaintainScroll_Default_Constraint  DEFAULT 1
GO
ROLLBACK TRANSACTION


BEGIN TRANSACTION
UPDATE dbo.SitePages SET bitIsMaintainScroll = 1 WHERE numSiteID = 60 AND numDomainID = 156
GO
ROLLBACK TRANSACTION

BEGIN TRANSACTION
UPDATE dbo.SitePages SET bitIsMaintainScroll = 0 WHERE numSiteID <> 60 AND numDomainID <> 156
GO
ROLLBACK TRANSACTION


ALTER TABLE eCommercePaymentConfig
ADD  numFailedOrderStatus NUMERIC(18,0) null



--Note Fire only this script for 25-1-2013 Demo
BEGIN TRANSACTION
DELETE FROM dbo.ShippingServiceTypes WHERE numDomainID = 0 AND numRuleID IS NOT NULL
GO
ROLLBACK TRANSACTION

--========================================================================