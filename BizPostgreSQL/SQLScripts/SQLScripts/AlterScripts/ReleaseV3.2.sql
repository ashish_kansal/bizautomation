/******************************************************************
Project: Release 3.2 Date: 28.06.2014
Comments: 
*******************************************************************/

------------------- SACHIN ------------------

--RUN USP_AdvancedSearchOpp.StoredProcedure

----------------------------------------------


------------------- KAMAL SIR ------------------

  ALTER TABLE dbo.Domain ADD bitLandedCost BIT NULL,vcLanedCostDefault varchar(10) NULL
  
  
  CREATE TABLE [dbo].[LandedCostVendors](
	[numLCVendorId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numDomainId] [numeric](18, 0) NOT NULL,
	[numVendorId] [numeric](18, 0) NOT NULL,
 CONSTRAINT [PK_LandedCostVendors] PRIMARY KEY CLUSTERED 
(
	[numLCVendorId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


CREATE TABLE [dbo].[LandedCostExpenseAccount](
	[numLCExpenseId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numDomainId] [numeric](18, 0) NOT NULL,
	[vcDescription] [varchar](50) NOT NULL,
	[numAccountId] [numeric](18, 0) NOT NULL,
 CONSTRAINT [PK_LandedCostExpenseAccount] PRIMARY KEY CLUSTERED 
(
	[numLCExpenseId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
  


ALTER TABLE dbo.BillHeader ADD
	numOppId numeric(18, 0) NULL,bitLandedCost bit NULL
	

ALTER TABLE dbo.OpportunityMaster ADD
	vcLanedCost varchar(50) NULL,
	monLandedCostTotal money NULL
	

ALTER TABLE dbo.OpportunityItems ADD
	monLandedCost money NULL		
	
	
--************************************
INSERT INTO dbo.DycFieldMaster		
SELECT 3,NULL,'Landed Cost','monLandedCost','monLandedCost','LandedCost','OpportunityItems','M','R',
'Label',NULL,'',0,NULL,48,NULL,NULL,1,0,0,0,1,0,1,0,0,0,0,0,0,0,NULL,NULL

SELECT * FROM dbo.DycFormField_Mapping WHERE numFormID=26

INSERT INTO dbo.DycFormField_Mapping
SELECT numModuleID,numFieldID,numDOmainID,26,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,PopupFunctionName,[ORDER],tintRow,tintColumn,
bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitImport,bitExport,bitAllowFiltering,bitRequired,NULL,NULL,NULL
FROM dbo.DycFieldMaster	WHERE vcDbColumnName='monLandedCost' AND numModuleID=3	
--************************************


ALTER TABLE dbo.OpportunityItems ADD
	monAvgLandedCost money NULL

-- GET LATEST THIS SPs & Add them into TFS
/*
    USP_GetGLTransactionDetailById
    USP_GetGeneralLedger_Virtual
    USP_GetOrganizationTransaction


*/

  INSERT INTO AccountingChargeTypes
  SELECT 'Purchase Tax Credit','','PT'
  
  
  ALTER TABLE dbo.Domain ADD bitPurchaseTaxCredit BIT NULL
  
  delete from OpportunityMasterTaxItems where numOppID in (select numOppId from OpportunityMaster where tintOppType=2)
  
  
  
  	DECLARE @numPageNavID numeric 
	SELECT @numPageNavID = MAX(numPageNavID) + 1  FROM dbo.PageNavigationDTL 
	INSERT INTO dbo.PageNavigationDTL
			( numPageNavID ,
			  numModuleID ,
			  numParentID ,
			  vcPageNavName ,
			  vcNavURL ,
			  vcImageURL ,
			  bitVisible ,
			  numTabID
			)
	SELECT @numPageNavID ,35,222,'Tax Reports (All Customers & Vendors)','../Accounting/frmTaxReports.aspx',NULL,1,45 
	EXEC USP_CreateTreeNavigationForDomain 0,@numPageNavID --Give permission to tree node.

----------------------------------------------


------------------- SANDEEP ------------------


UPDATE BizAPIThrottlePolicy SET numPerMinute = 60

--usp_SetUsersWithDomains
--USP_UserList
--USP_Item_SearchForSalesOrder

----------------------------------------------


------------------- MANISH ------------------

------/******************************************************************
------Project: BACRMUI   Date: 26.06.2014
------Comments: Change in Name of field "Contact Record Owner"
------*******************************************************************/
BEGIN TRANSACTION
--SELECT * FROM [dbo].[DycFormField_Mapping] AS DFFM WHERE [DFFM].[vcFieldName] = 'Contact Record Owner' AND [DFFM].[numFormID] = 36
UPDATE [dbo].[DycFormField_Mapping] SET [vcFieldName] = 'Organization & Contact Record Owner'  WHERE [vcFieldName] = 'Contact Record Owner' AND [numFormID] = 36
UPDATE [dbo].[DynamicFormField_Validation] SET [vcNewFormFieldName] = 'Organization & Contact Record Owner'  WHERE [vcNewFormFieldName] = 'Contact Record Owner' AND [numFormID] = 36
--SELECT * FROM [dbo].[DycFormField_Mapping] AS DFFM WHERE [DFFM].[vcFieldName] = 'Organization & Contact Record Owner' AND [DFFM].[numFormID] = 36
ROLLBACK


------/******************************************************************
------Project: BACRMUI   Date: 18.06.2014
------Comments: Add new Import Category "Address Details"
------*******************************************************************/
BEGIN TRANSACTION

----------------------- RUN 1 ----------------
INSERT  INTO [dbo].[DynamicFormMaster]
        ( [numFormId] ,
          [vcFormName] ,
          [cCustomFieldsAssociated] ,
          [cAOIAssociated] ,
          [bitDeleted] ,
          [tintFlag] ,
          [bitWorkFlow] ,
          [vcLocationID] ,
          [bitAllowGridColor]
        )
VALUES  ( 98 , -- numFormId - numeric
          N'Update Address' , -- vcFormName - nvarchar(50)
          'N' , -- cCustomFieldsAssociated - char(1)
          'N' , -- cAOIAssociated - char(1)
          0 , -- bitDeleted - bit
          2 , -- tintFlag - tinyint
          NULL , -- bitWorkFlow - bit
          '' , -- vcLocationID - varchar(50)
          NULL  -- bitAllowGridColor - bit
        )

ROLLBACK

BEGIN TRANSACTION
----------------------- RUN 2 ----------------

SET IDENTITY_INSERT dbo.DycFieldMaster ON
INSERT INTO dbo.DycFieldMaster 
([numFieldId],numModuleID,numDomainID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,
vcToolTip,vcListItemType,numListID,PopupFunctionName,[order],tintRow,tintColumn,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,
bitDetailField,bitAllowSorting,bitWorkFlowField,bitImport,bitExport,bitAllowFiltering,bitInlineEdit,bitRequired,intColumnWidth,intFieldMaxLength)
SELECT 529,2,NULL,'AddressID','numAddressID','numAddressID','AddressID','AddressDetails','N','R','TextBox',
NULL,'',0,NULL,32,NULL,NULL,1,0,0,0,0,NULL,
0,0,0,1,1,0,1,1,NULL,NULL
UNION
SELECT 530,2,NULL,'Address Name','vcAddressName','vcAddressName','AddressName','AddressDetails','V','R','TextBox',
NULL,'',0,NULL,32,NULL,NULL,1,0,0,0,0,NULL,
0,0,0,1,1,0,1,NULL,NULL,NULL
UNION
SELECT 531,2,NULL,'RecordID','numRecordID','numRecordID','RecordID','AddressDetails','N','R','TextBox',
NULL,'',0,NULL,32,NULL,NULL,1,0,0,0,0,NULL,
0,0,0,1,1,0,1,1,NULL,NULL
UNION
SELECT 532,2,NULL,'AddressType','tintAddressType','tintAddressType','AddressType','AddressDetails','N','R','TextBox',
NULL,'',0,NULL,32,NULL,NULL,1,0,0,0,0,NULL,
0,0,0,1,1,0,1,1,NULL,NULL
UNION
SELECT 533,2,NULL,'IsPrimary','bitIsPrimary','bitIsPrimary','IsPrimaryAddress','AddressDetails','Y','R','CheckBox',
NULL,'',0,NULL,32,NULL,NULL,1,0,0,0,0,NULL,
0,0,0,1,1,0,1,1,NULL,NULL
UNION
SELECT 534,2,NULL,'AddressOf','tintAddressOf','tintAddressOf','AddresOf','AddressDetails','N','R','TextBox',
NULL,'',0,NULL,32,NULL,NULL,1,0,0,0,0,NULL,
0,0,0,1,1,0,1,1,NULL,NULL

SET IDENTITY_INSERT dbo.DycFieldMaster OFF

-- SELECT * FROM  DycFieldMaster WHERE [DycFieldMaster].[numFieldId] IN (529,530)
ROLLBACK

---------------------------------------------------- RUN 3 ----------------------------------------------------
BEGIN TRANSACTION
-- SELECT * FROM  [dbo].[DycFormSectionDetail] AS DFSD
INSERT INTO dbo.DycFormSectionDetail(numFormID,intSectionID,vcSectionName,Loc_id) 
SELECT  98,1,'Address Details',0
ROLLBACK

---------------------------------------------------- RUN 4 ----------------------------------------------------
BEGIN TRANSACTION
INSERT INTO dbo.DycFormField_Mapping 
	(numModuleID,numFieldID,numDomainID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,PopupFunctionName,[order],
	 tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitImport,bitExport,
	 bitAllowFiltering,bitRequired,numFormFieldID,intSectionID,bitAllowGridColor)
SELECT DISTINCT numModuleID,numFieldID,numDomainID,98,bitAllowEdit,bitInlineEdit,'Street',vcAssociatedControlType,'Street',PopupFunctionName,1,
	 tintRow,tintColumn,1,0,bitDefault,1,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,1,1,
	 0,bitRequired,numFormFieldID,1,bitAllowGridColor 
	 FROM dbo.DycFormField_Mapping 
	 WHERE [DycFormField_Mapping].[numFieldID] IN (82)
	 AND [DycFormField_Mapping].[numFormID] = 36
UNION 
SELECT DISTINCT numModuleID,numFieldID,numDomainID,98,bitAllowEdit,bitInlineEdit,'City',vcAssociatedControlType,'City',PopupFunctionName,1,
	 tintRow,tintColumn,1,0,bitDefault,1,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,1,1,
	 0,bitRequired,numFormFieldID,1,bitAllowGridColor 
	 FROM dbo.DycFormField_Mapping 
	 WHERE [DycFormField_Mapping].[numFieldID] IN (83)
	 AND [DycFormField_Mapping].[numFormID] = 36
UNION 
SELECT DISTINCT numModuleID,numFieldID,numDomainID,98,bitAllowEdit,bitInlineEdit,'State',vcAssociatedControlType,'State',PopupFunctionName,1,
	 tintRow,tintColumn,1,0,bitDefault,1,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,1,1,
	 0,bitRequired,numFormFieldID,1,bitAllowGridColor 
	 FROM dbo.DycFormField_Mapping 
	 WHERE [DycFormField_Mapping].[numFieldID] IN (84)
	 AND [DycFormField_Mapping].[numFormID] = 36
UNION 
SELECT DISTINCT numModuleID,numFieldID,numDomainID,98,bitAllowEdit,bitInlineEdit,'Country',vcAssociatedControlType,'Country',PopupFunctionName,1,
	 tintRow,tintColumn,1,0,bitDefault,1,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,1,1,
	 0,bitRequired,numFormFieldID,1,bitAllowGridColor 
	 FROM dbo.DycFormField_Mapping 
	 WHERE [DycFormField_Mapping].[numFieldID] IN (85)
	 AND [DycFormField_Mapping].[numFormID] = 36
UNION 
SELECT DISTINCT numModuleID,numFieldID,numDomainID,98,bitAllowEdit,bitInlineEdit,'Postal Code',vcAssociatedControlType,'PostalCode',PopupFunctionName,1,
	 tintRow,tintColumn,1,0,bitDefault,1,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,1,1,
	 0,bitRequired,numFormFieldID,1,bitAllowGridColor 
	 FROM dbo.DycFormField_Mapping 
	 WHERE [DycFormField_Mapping].[numFieldID] IN (86)
	 AND [DycFormField_Mapping].[numFormID] = 36
UNION 
SELECT DISTINCT numModuleID,numFieldID,numDomainID,98,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,PopupFunctionName,1,
	 tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,1,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,1,1,
	 0,bitRequired,NULL,1,NULL 
	 FROM dbo.[DycFieldMaster] AS DFM 
	 WHERE DFM.[numFieldID] IN (529,530)
UNION
SELECT DISTINCT numModuleID,numFieldID,numDomainID,98,bitAllowEdit,bitInlineEdit,'RecordID',vcAssociatedControlType,'RecordID',PopupFunctionName,1,
	 tintRow,tintColumn,1,0,bitDefault,1,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,1,1,
	 0,bitRequired,NULL,1,NULL
	 FROM dbo.[DycFieldMaster] AS DFM 
	 WHERE DFM.[numFieldID] IN (531)
UNION
SELECT DISTINCT numModuleID,numFieldID,numDomainID,98,bitAllowEdit,bitInlineEdit,'AddressType',vcAssociatedControlType,'AddressType',PopupFunctionName,1,
	 tintRow,tintColumn,1,0,bitDefault,1,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,1,1,
	 0,bitRequired,NULL,1,NULL
	 FROM dbo.[DycFieldMaster] AS DFM 
	 WHERE DFM.[numFieldID] IN (532)
UNION
SELECT DISTINCT numModuleID,numFieldID,numDomainID,98,bitAllowEdit,bitInlineEdit,'IsPrimary',vcAssociatedControlType,'IsPrimaryAddress',PopupFunctionName,1,
	 tintRow,tintColumn,1,0,bitDefault,1,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,1,1,
	 0,bitRequired,NULL,1,NULL
	 FROM dbo.[DycFieldMaster] AS DFM 
	 WHERE DFM.[numFieldID] IN (533)
UNION
SELECT DISTINCT numModuleID,numFieldID,numDomainID,98,bitAllowEdit,bitInlineEdit,'AddressOf',vcAssociatedControlType,'AddresOf',PopupFunctionName,1,
	 tintRow,tintColumn,1,0,bitDefault,1,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,1,1,
	 0,bitRequired,NULL,1,NULL
	 FROM dbo.[DycFieldMaster] AS DFM 
	 WHERE DFM.[numFieldID] IN (534)


SELECT * FROM [dbo].[DycFormField_Mapping] AS DFFM WHERE [DFFM].[numFormID] = 98
ROLLBACK


------/******************************************************************
------Project: BACRMUI   Date: 11.06.2014
------Comments: Update Inventory Status to use for filtering
------*******************************************************************/
BEGIN TRANSACTION

UPDATE [dbo].[DycFieldMaster] SET [bitAllowFiltering] = 1, [bitAllowSorting] = 0 WHERE [vcFieldName] = 'Inventory Status' AND [vcDbColumnName] = 'vcInventoryStatus'
UPDATE [dbo].[DycFormField_Mapping] SET [bitAllowFiltering] = 1, [bitAllowSorting] = 0, [vcAssociatedControlType]= 'SelectBox' WHERE [vcFieldName] = 'Inventory Status' AND [vcPropertyName] = 'InventoryStatus'

ROLLBACK



----------------------------------------------
