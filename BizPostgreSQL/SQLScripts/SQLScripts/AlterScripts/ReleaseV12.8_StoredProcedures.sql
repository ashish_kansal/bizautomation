/******************************************************************
Project: Release 12.8 Date: 16.OCT.2019
Comments: STORE PROCEDURES
*******************************************************************/

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='Activity_GoogleEventId')
DROP PROCEDURE Activity_GoogleEventId
GO
CREATE PROCEDURE [dbo].[Activity_GoogleEventId]  
 @ActivityID int, 
 @GoogleEventId varchar(MAX) 
AS  
BEGIN  
    UPDATE Activity SET GoogleEventId=@GoogleEventId WHERE ActivityID=@ActivityID
END
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='GetOrderShipReceiveStatus')
DROP FUNCTION GetOrderShipReceiveStatus
GO
CREATE FUNCTION [dbo].[GetOrderShipReceiveStatus] 
(
      @numOppID AS NUMERIC(18,0),
	  @tintOppType AS NUMERIC(18,0),
	  @tintshipped AS TINYINT,
	  @tintDecimalPoints TINYINT
)
RETURNS VARCHAR(100)
AS 
BEGIN	
	DECLARE @vcShippedReceivedStatus VARCHAR(100) = ''

	DECLARE @TotalQty AS FLOAT  = 0

	IF @tintOppType = 1 --SALES ORDER
	BEGIN
		DECLARE @numQtyShipped AS FLOAT = 0

		SELECT @TotalQty = ISNULL((SELECT SUM(ISNULL(dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), OI.numItemCode,I.numDomainId, ISNULL(OI.numUOMId, 0)),1) * numUnitHour) FROM OpportunityItems OI INNER JOIN Item I ON OI.numItemCode = I.numItemCode WHERE UPPER(I.charItemType) = 'P' AND ISNULL(bitDropship,0) = 0 AND numOppId = @numOppID),0)

		-- IF ORDER IS CLOSED WITHOUT FULFILLMENT MARK ALL QTY IS FULFILLED
		IF ISNULL(@tintshipped,0) = 1
		BEGIN
			SET @numQtyShipped = @TotalQty
		END
		ELSE
		BEGIN
			SELECT @numQtyShipped = ISNULL((SELECT SUM(ISNULL(dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), OI.numItemCode,I.numDomainId, ISNULL(OI.numUOMId, 0)),1) * numQtyShipped) FROM OpportunityItems OI INNER JOIN Item I ON OI.numItemCode = I.numItemCode  WHERE UPPER(I.charItemType) = 'P' AND ISNULL(bitDropship,0) = 0 AND numOppId = @numOppID),0)
		END

		SELECT @vcShippedReceivedStatus =(CASE WHEN  @TotalQty = @numQtyShipped THEN '<b><font color="green">' ELSE '<b><font color="red">' END) + CONCAT((CASE @tintDecimalPoints
		WHEN 1 THEN
			CONVERT(VARCHAR,FORMAT(@TotalQty,'#,##0.#'))
		WHEN 2 THEN
			CONVERT(VARCHAR,FORMAT(@TotalQty,'#,##0.##'))
		WHEN 3 THEN
			CONVERT(VARCHAR,FORMAT(@TotalQty,'#,##0.###'))
		WHEN 4 THEN
			CONVERT(VARCHAR,FORMAT(@TotalQty,'#,##0.####'))
		WHEN 5 THEN
			CONVERT(VARCHAR,FORMAT(@TotalQty,'#,##0.#####'))
		ELSE
			convert(varchar,FORMAT(@TotalQty,'#,##0.##'))
		END),' / ',(CASE @tintDecimalPoints
		WHEN 1 THEN
			CONVERT(VARCHAR,FORMAT(@numQtyShipped,'#,##0.#'))
		WHEN 2 THEN
			CONVERT(VARCHAR,FORMAT(@numQtyShipped,'#,##0.##'))
		WHEN 3 THEN
			CONVERT(VARCHAR,FORMAT(@numQtyShipped,'#,##0.###'))
		WHEN 4 THEN
			CONVERT(VARCHAR,FORMAT(@numQtyShipped,'#,##0.####'))
		WHEN 5 THEN
			CONVERT(VARCHAR,FORMAT(@numQtyShipped,'#,##0.#####'))
		ELSE
			convert(varchar,FORMAT(@numQtyShipped,'#,##0.##'))
		END)) + '</font></b>'
	END
	ELSE IF @tintOppType = 2 --PURCHASE ORDER
	BEGIN
		DECLARE @numQtyReceived AS FLOAT = 0

		SELECT @TotalQty = ISNULL((SELECT SUM(ISNULL(dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), OI.numItemCode,I.numDomainId, ISNULL(OI.numUOMId, 0)),1) * numUnitHour) FROM OpportunityItems OI INNER JOIN Item I ON OI.numItemCode = I.numItemCode  WHERE UPPER(I.charItemType) = 'P' AND ISNULL(bitDropship,0) = 0 AND numOppId = @numOppID),0)
		
		-- IF ORDER IS CLOSED WITHOUT FULFILLMENT MARK ALL QTY IS FULFILLED
		IF ISNULL(@tintshipped,0) = 1
		BEGIN
			SET @numQtyReceived = @TotalQty
		END
		ELSE
		BEGIN
			SELECT @numQtyReceived = ISNULL((SELECT SUM(ISNULL(dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), OI.numItemCode,I.numDomainId, ISNULL(OI.numUOMId, 0)),1) * numUnitHourReceived) FROM OpportunityItems OI INNER JOIN Item I ON OI.numItemCode = I.numItemCode  WHERE UPPER(I.charItemType) = 'P' AND ISNULL(bitDropship,0) = 0 AND numOppId = @numOppID),0)
		END

		SELECT @vcShippedReceivedStatus =(CASE WHEN  @TotalQty = @numQtyReceived THEN '<b><font color="green">' ELSE '<b><font color="red">' END) + CONCAT((CASE @tintDecimalPoints
		WHEN 1 THEN
			CONVERT(VARCHAR,FORMAT(@TotalQty,'#,##0.#'))
		WHEN 2 THEN
			CONVERT(VARCHAR,FORMAT(@TotalQty,'#,##0.##'))
		WHEN 3 THEN
			CONVERT(VARCHAR,FORMAT(@TotalQty,'#,##0.###'))
		WHEN 4 THEN
			CONVERT(VARCHAR,FORMAT(@TotalQty,'#,##0.####'))
		WHEN 5 THEN
			CONVERT(VARCHAR,FORMAT(@TotalQty,'#,##0.#####'))
		ELSE
			convert(varchar,FORMAT(@TotalQty,'#,##0.##'))
		END),' / ',(CASE @tintDecimalPoints
		WHEN 1 THEN
			CONVERT(VARCHAR,FORMAT(@numQtyReceived,'#,##0.#'))
		WHEN 2 THEN
			CONVERT(VARCHAR,FORMAT(@numQtyReceived,'#,##0.##'))
		WHEN 3 THEN
			CONVERT(VARCHAR,FORMAT(@numQtyReceived,'#,##0.###'))
		WHEN 4 THEN
			CONVERT(VARCHAR,FORMAT(@numQtyReceived,'#,##0.####'))
		WHEN 5 THEN
			CONVERT(VARCHAR,FORMAT(@numQtyReceived,'#,##0.#####'))
		ELSE
			convert(varchar,FORMAT(@numQtyReceived,'#,##0.##'))
		END)) + '</font></b>'
	END

	RETURN @vcShippedReceivedStatus
END
GO
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_AddressDetails_UpdateRecordID')
DROP PROCEDURE USP_AddressDetails_UpdateRecordID
GO
CREATE PROCEDURE [dbo].[USP_AddressDetails_UpdateRecordID]    
	@numDomainID NUMERIC(18,0)
	,@numAddressID NUMERIC(18,0)
	,@numRecordID NUMERIC(18,0)
AS
BEGIN
	UPDATE
		AddressDetails
	SET
		numRecordID=@numRecordID
	WHERE
		numDomainID=@numDomainID
		AND numAddressID=@numAddressID
END
GO
/****** Object:  StoredProcedure [dbo].[USP_cfwGetFields]    Script Date: 07/26/2008 16:15:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_cfwgetfields')
DROP PROCEDURE usp_cfwgetfields
GO
CREATE PROCEDURE [dbo].[USP_cfwGetFields]                            
@PageId as tinyint,                              
@numRelation as numeric(9),                              
@numRecordId as numeric(9),                  
@numDomainID as numeric(9)                              
AS            
BEGIN
                  
	if @PageId= 1 
	begin                              
		select CFW_Fld_Master.fld_id,fld_type,fld_label,numlistid,numOrder,Rec.FldDTLID,CASE WHEN CFW_Fld_Master.Fld_type='SelectBox' THEN isnull(Rec.Fld_Value,0) ELSE isnull(Rec.Fld_Value,'') END as Value,subgrp as TabId,Grp_Name as tabname,vcURL,CFW_Fld_Master.vcToolTip from CFW_Fld_Master join CFW_Fld_Dtl                              
		on Fld_id=numFieldId                              
		left join CFw_Grp_Master                               
		on subgrp=CFw_Grp_Master.Grp_id 
		left join dbo.GetCustomFieldDTLIDAndValues(@PageId,@numRecordId) Rec
		on Rec.Fld_ID=CFW_Fld_Master.fld_id                             
		where CFW_Fld_Master.grp_id=@PageId and numRelation=@numRelation and CFW_Fld_Master.numDomainID=@numDomainID                          
         
		union        
		select 0 as fld_id,'Frame' as fld_type,'' as fld_label,0 as numlistid,0,0,'' as Value,Grp_id as TabId,Grp_Name as tabname,vcURLF,Null as vcToolTip from CFw_Grp_Master        
		where tintType=1 and loc_Id = @PageId   and numDomainID=@numDomainID           
		order by subgrp,numOrder                             
	end                              

	---select Shared Custom fields and tab specific customfields -- where Pageid=1 stands for shared custom fields among accounts,leads,prospects
	IF  @PageId= 12 or  @PageId= 13 or  @PageId= 14
	BEGIN
		select CFW_Fld_Master.fld_id,fld_type,fld_label,numlistid,numOrder,Rec.FldDTLID,CASE WHEN CFW_Fld_Master.Fld_type='SelectBox' THEN isnull(Rec.Fld_Value,0) ELSE isnull(Rec.Fld_Value,'') END as Value,subgrp as TabId,Grp_Name as tabname,vcURL,CFW_Fld_Master.vcToolTip from CFW_Fld_Master join CFW_Fld_Dtl                              
		on Fld_id=numFieldId                              
		left join CFw_Grp_Master                               
		on subgrp=CFw_Grp_Master.Grp_id 
		left join dbo.GetCustomFieldDTLIDAndValues(@PageId,@numRecordId) Rec
		on Rec.Fld_ID=CFW_Fld_Master.fld_id                             
		where CFW_Fld_Master.grp_id=@PageId and numRelation=@numRelation and CFW_Fld_Master.numDomainID=@numDomainID                          
	
		UNION 
	
		select CFW_Fld_Master.fld_id,fld_type,fld_label,numlistid,numOrder,Rec.FldDTLID,CASE WHEN CFW_Fld_Master.Fld_type='SelectBox' THEN isnull(Rec.Fld_Value,0) ELSE isnull(Rec.Fld_Value,'') END as Value,subgrp as TabId,Grp_Name as tabname,vcURL,CFW_Fld_Master.vcToolTip from CFW_Fld_Master join CFW_Fld_Dtl                              
		on Fld_id=numFieldId                              
		left join CFw_Grp_Master                               
		on subgrp=CFw_Grp_Master.Grp_id 
		left join dbo.GetCustomFieldDTLIDAndValues(1,@numRecordId) Rec
		on Rec.Fld_ID=CFW_Fld_Master.fld_id                             
		where CFW_Fld_Master.grp_id=1 and numRelation=@numRelation and CFW_Fld_Master.numDomainID=@numDomainID
	         
		union        
		select 0 as fld_id,'Frame' as fld_type,'' as fld_label,0 as numlistid,0,0,'' as Value,Grp_id as TabId,Grp_Name as tabname,vcURLF,NUll as vcToolTip from CFw_Grp_Master        
		where tintType=1 and loc_Id = @PageId   and numDomainID=@numDomainID           
		order by subgrp,numOrder                             
	
	END


                              
	if @PageId= 4                              
	begin       
		---------------------Very Important----------------------    
		-------Who wrote this part    
		---------------------------------------------------------    
    
    
		--declare @ContactId as numeric(9)                              
		---select @ContactId=numContactId from cases where numCaseid=@numRecordId                              
		--select @numRelation=numCompanyType from AdditionalContactsInformation AddC                              
		---join DivisionMaster Div on AddC.numDivisionId=Div.numDivisionId                              
		---join CompanyInfo Com on  Com.numCompanyId=Div.numCompanyId                              
		---where AddC.numContactID=@ContactId                              
                              
		select CFW_Fld_Master.fld_id,fld_type,fld_label,numlistid,numOrder,
		Rec.FldDTLID,CASE WHEN CFW_Fld_Master.Fld_type='SelectBox' THEN isnull(Rec.Fld_Value,0) ELSE isnull(Rec.Fld_Value,'') END as Value,
		subgrp as TabId,Grp_Name as tabname,vcURL,CFW_Fld_Master.vcToolTip from CFW_Fld_Master join CFW_Fld_Dtl                              
		on Fld_id=numFieldId                              
		left join CFw_Grp_Master                               
		on subgrp=CFw_Grp_Master.Grp_id  
		left join dbo.GetCustomFieldDTLIDAndValues(@PageId,@numRecordId) Rec
		on Rec.Fld_ID=CFW_Fld_Master.fld_id                             
		where CFW_Fld_Master.grp_id=@PageId and numRelation=@numRelation and CFW_Fld_Master.numDomainID=@numDomainID                             
		--select @numRelation=numContactType from AdditionalContactsInformation AddC                              
		--join DivisionMaster Div on AddC.numDivisionId=Div.numDivisionId                              
		--join CompanyInfo Com on  Com.numCompanyId=Div.numCompanyId                              
		--where AddC.numContactID=@numRecordId                              
                              
		union        
		select 0 as fld_id,'Frame' as fld_type,'' as fld_label,0 as numlistid,0,0,'' as Value,
		Grp_id as TabId,Grp_Name as tabname,vcURLF,NUll as vcToolTip from CFw_Grp_Master        
		where tintType=1 and loc_Id = @PageId   and numDomainID=@numDomainID                              
		   order by subgrp,numOrder                           
	end                              
                              
	if @PageId= 2 or  @PageId= 3 or @PageId= 5 or @PageId= 6 or @PageId= 7 or @PageId= 8     or @PageId= 11   or @PageId = 17                
	begin                              
		select CFW_Fld_Master.fld_id,fld_type,fld_label,numlistid,ISNULL(Rec.FldDTLID,0) FldDTLID,
		(CASE 
			WHEN ISNULL(Rec.Fld_Value, '') = '' AND CFW_Fld_Master.fld_type = 'Checkbox' THEN '0' 
			WHEN ISNULL(Rec.Fld_Value, '') = '' AND CFW_Fld_Master.fld_type = 'SelectBox' THEN '0'
			WHEN CFW_Fld_Master.fld_type = 'SelectBox' THEN ISNULL(Rec.Fld_Value,0) 
			ELSE ISNULL(Rec.Fld_Value,'') 
			END)   AS Value,
		subgrp as TabId,Grp_Name as tabname,vcURL,CFW_Fld_Master.vcToolTip  from CFW_Fld_Master                               
		left join CFw_Grp_Master                               
		on subgrp=CFw_Grp_Master.Grp_id  
		left join dbo.GetCustomFieldDTLIDAndValues(@PageId,@numRecordId) Rec
		on Rec.Fld_ID=CFW_Fld_Master.fld_id                             
		where CFW_Fld_Master.grp_id=@PageId and CFW_Fld_Master.numDomainID=@numDomainID         
		union        
		select 0 as fld_id,'Frame' as fld_type,'' as fld_label,0 as numlistid,0 as FldDTLID,'0' as Value,Grp_id as TabId,Grp_Name as tabname,vcURLF,NULL as vcToolTip from CFw_Grp_Master        
		where tintType=1 and loc_Id = @PageId  and numDomainID=@numDomainID      
	end

	if @PageId= 18               
	begin                              
		select CFW_Fld_Master.fld_id,fld_type,fld_label,numlistid,ISNULL(Rec.FldDTLID,0) FldDTLID,
		(CASE 
			WHEN ISNULL(Rec.Fld_Value, '') = '' AND fld_type = 'Checkbox' THEN '0' 
			WHEN ISNULL(Rec.Fld_Value, '') = '' AND fld_type = 'SelectBox' THEN '0'
			WHEN fld_type = 'SelectBox' THEN ISNULL(Rec.Fld_Value,0) 
			ELSE ISNULL(Rec.Fld_Value,'') END)  AS Value,
		subgrp as TabId,Grp_Name as tabname,vcURL,CFW_Fld_Master.vcToolTip  from CFW_Fld_Master                               
		left join CFw_Grp_Master                               
		on subgrp=CFw_Grp_Master.Grp_id  
		left join dbo.GetCustomFieldDTLIDAndValues(@PageId,@numRecordId) Rec
		on Rec.Fld_ID=CFW_Fld_Master.fld_id                             
		where CFW_Fld_Master.grp_id=5 and CFW_Fld_Master.numDomainID=@numDomainID         
		union        
		select 0 as fld_id,'Frame' as fld_type,'' as fld_label,0 as numlistid,0 as FldDTLID,'0' as Value,Grp_id as TabId,Grp_Name as tabname,vcURLF,NULL as vcToolTip from CFw_Grp_Master        
		where tintType=1 and loc_Id = 5  and numDomainID=@numDomainID      
	end

select GRp_Id as TabID,Grp_Name AS TabName from CFw_Grp_Master where 
(Loc_ID=@PageId OR 1=(CASE WHEN @PageId IN (12,13,14) THEN CASE WHEN Loc_Id=1 THEN 1 ELSE 0 END ELSE 0 END))
 and numDomainID=@numDomainID
/*tintType stands for static tabs */ AND tintType<>2

END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DemandForecast_Execute')
DROP PROCEDURE dbo.USP_DemandForecast_Execute
GO
CREATE PROCEDURE [dbo].[USP_DemandForecast_Execute]
	@numDFID NUMERIC (18,0)
	,@numUserCntID NUMERIC(18,0)
	,@ClientTimeZoneOffset INT
	,@CurrentPage INT
    ,@PageSize INT
	,@columnName VARCHAR(MAX)                                                          
	,@columnSortOrder VARCHAR(10)
	,@numDomainID NUMERIC(18,0)
	,@vcRegularSearchCriteria VARCHAR(MAX)=''
	,@bitShowAllItems BIT
	,@bitShowHistoricSales BIT
	,@numHistoricalAnalysisPattern BIT
	,@bitBasedOnLastYear BIT
	,@bitIncludeOpportunity BIT
	,@numOpportunityPercentComplete NUMERIC(18,0)
	,@tintDemandPlanBasedOn TINYINT
AS 
BEGIN
	DECLARE @dtLastExecution DATETIME

	IF (SELECT COUNT(*) FROM DemandForecast WHERE numDFID = @numDFID) > 0
	BEGIN
		DECLARE @tintUnitsRecommendationForAutoPOBackOrder TINYINT
		DECLARE @bitIncludeRequisitions BIT

		SELECT 
			@tintUnitsRecommendationForAutoPOBackOrder=ISNULL(tintUnitsRecommendationForAutoPOBackOrder,1)
			,@bitIncludeRequisitions=ISNULL(bitIncludeRequisitions,0)
		FROM 
			Domain 
		WHERE 
			numDomainID = @numDomainID

		DECLARE @bitWarehouseFilter BIT = 0
		DECLARE @bitItemClassificationFilter BIT = 0
		DECLARE @bitItemGroupFilter BIT = 0

		IF (SELECT COUNT(*) FROM DemandForecastWarehouse WHERE numDFID = @numDFID) > 0
		BEGIN
			SET @bitWarehouseFilter = 1
		END

		IF (SELECT COUNT(*) FROM DemandForecastItemClassification WHERE numDFID = @numDFID) > 0
		BEGIN
			SET @bitItemClassificationFilter = 1
		END

		IF (SELECT COUNT(*) FROM DemandForecastItemGroup WHERE numDFID = @numDFID) > 0
		BEGIN
			SET @bitItemGroupFilter = 1
		END				

		DECLARE @TEMP TABLE
		(
			numItemCode NUMERIC(18,0)
			,numWarehouseItemID NUMERIC(18,0)
			,numUnitHour FLOAT
			,dtReleaseDate DATE
		)

		INSERT INTO @TEMP
		(
			numItemCode
			,numWarehouseItemID
			,numUnitHour
			,dtReleaseDate
		)
		SELECT
			Item.numItemCode
			,OpportunityItems.numWarehouseItmsID
			,OpportunityItems.numUnitHour - ISNULL(OpportunityItems.numQtyShipped,0)
			,ISNULL(OpportunityItems.ItemReleaseDate,OpportunityMaster.dtReleaseDate)
		FROM
			OpportunityMaster
		INNER JOIN
			OpportunityItems
		ON
			OpportunityMaster.numOppId = OpportunityItems.numOppId
		INNER JOIN
			Item
		ON
			OpportunityItems.numItemCode = Item.numItemCode
		INNER JOIN
			WareHouseItems
		ON
			OpportunityItems.numWarehouseItmsID = WareHouseItems.numWareHouseItemID
		WHERE
			OpportunityMaster.numDomainId=@numDomainID
			AND OpportunityMaster.tintOppType=1
			AND OpportunityMaster.tintOppStatus=1
			AND ISNULL(OpportunityMaster.tintshipped,0)=0
			AND (ISNULL(OpportunityItems.numUnitHour,0)  - ISNULL(OpportunityItems.numQtyShipped,0)) > 0
			AND ISNULL(OpportunityItems.bitDropShip,0) = 0
			AND ISNULL(Item.IsArchieve,0) = 0
			AND ISNULL(Item.bitAssembly,0) = 0
			AND ISNULL(Item.bitKitParent,0) = 0
			AND (OpportunityItems.ItemReleaseDate IS NOT NULL OR OpportunityMaster.dtReleaseDate IS NOT NULL)
			AND
			(
				@bitWarehouseFilter = 0 OR
				WareHouseItems.numWareHouseID IN (SELECT numWareHouseID FROM DemandForecastWarehouse WHERE numDFID =@numDFID)
			)
			AND
			(
				@bitItemClassificationFilter = 0 OR
				Item.numItemClassification IN (SELECT numItemClassificationID FROM DemandForecastItemClassification WHERE numDFID = @numDFID)
			)
			AND
			(
				@bitItemGroupFilter = 0 OR
				Item.numItemGroup IN (SELECT numItemGroupID FROM DemandForecastItemGroup WHERE numDFID = @numDFID)
			)

		INSERT INTO @TEMP
		(
			numItemCode
			,numWarehouseItemID
			,numUnitHour
			,dtReleaseDate
		)
		SELECT
			Item.numItemCode
			,OKI.numWareHouseItemId
			,OKI.numQtyItemsReq - ISNULL(OKI.numQtyShipped,0)
			,ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate)
		FROM
			OpportunityMaster OM
		INNER JOIN
			OpportunityItems OI
		ON
			OM.numOppId = OI.numOppId
		INNER JOIN
			OpportunityKitItems OKI
		ON
			OI.numoppitemtCode=OKI.numOppItemID
		INNER JOIN
			Item
		ON
			OKI.numChildItemID = Item.numItemCode
		INNER JOIN
			WareHouseItems WI
		ON
			OKI.numWareHouseItemId = WI.numWareHouseItemID
		WHERE
			OM.numDomainId=@numDomainID
			AND OM.tintOppType=1
			AND OM.tintOppStatus=1
			AND ISNULL(OM.tintshipped,0)=0
			AND ISNULL(OI.bitDropShip,0) = 0
			AND ISNULL(OI.bitWorkOrder,0) = 0
			AND ISNULL(Item.IsArchieve,0) = 0
			AND ISNULL(Item.bitAssembly,0) = 0
			AND ISNULL(Item.bitKitParent,0) = 0
			AND (ISNULL(OKI.numQtyItemsReq,0) - ISNULL(OKI.numQtyShipped,0)) > 0
			AND (OM.dtReleaseDate IS NOT NULL OR OI.ItemReleaseDate IS NOT NULL)
			AND
			(
				@bitWarehouseFilter = 0 OR
				WI.numWareHouseID IN (SELECT numWareHouseID FROM DemandForecastWarehouse WHERE numDFID =@numDFID)
			)
			AND
			(
				@bitItemClassificationFilter = 0 OR
				Item.numItemClassification IN (SELECT numItemClassificationID FROM DemandForecastItemClassification WHERE numDFID = @numDFID)
			)
			AND
			(
				@bitItemGroupFilter = 0 OR
				Item.numItemGroup IN (SELECT numItemGroupID FROM DemandForecastItemGroup WHERE numDFID = @numDFID)
			)

		INSERT INTO @TEMP
		(
			numItemCode
			,numWarehouseItemID
			,numUnitHour
			,dtReleaseDate
		)
		SELECT
			Item.numItemCode
			,OKCI.numWareHouseItemId
			,OKCI.numQtyItemsReq - ISNULL(OKCI.numQtyShipped,0)
			,ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate)
		FROM
			OpportunityMaster OM
		INNER JOIN
			OpportunityItems OI
		ON
			OM.numOppId = OI.numOppId
		INNER JOIN
			OpportunityKitChildItems OKCI
		ON
			OI.numoppitemtCode = OKCI.numOppItemID
		INNER JOIN
			Item
		ON
			OKCI.numItemID = Item.numItemCode
		INNER JOIN
			WareHouseItems WI
		ON
			OKCI.numWareHouseItemId = WI.numWareHouseItemID
		WHERE
			OM.numDomainId=@numDomainID
			AND OM.tintOppType=1
			AND OM.tintOppStatus=1
			AND ISNULL(OM.tintshipped,0)=0
			AND ISNULL(OI.bitDropShip,0) = 0
			AND ISNULL(OI.bitWorkOrder,0) = 0
			AND ISNULL(Item.IsArchieve,0) = 0
			AND ISNULL(Item.bitAssembly,0) = 0
			AND ISNULL(Item.bitKitParent,0) = 0
			AND (ISNULL(OKCI.numQtyItemsReq,0) - ISNULL(OKCI.numQtyShipped,0)) > 0
			AND (OM.dtReleaseDate IS NOT NULL OR OI.ItemReleaseDate IS NOT NULL)
			AND
			(
				@bitWarehouseFilter = 0 OR
				WI.numWareHouseID IN (SELECT numWareHouseID FROM DemandForecastWarehouse WHERE numDFID =@numDFID)
			)
			AND
			(
				@bitItemClassificationFilter = 0 OR
				Item.numItemClassification IN (SELECT numItemClassificationID FROM DemandForecastItemClassification WHERE numDFID = @numDFID)
			)
			AND
			(
				@bitItemGroupFilter = 0 OR
				Item.numItemGroup IN (SELECT numItemGroupID FROM DemandForecastItemGroup WHERE numDFID = @numDFID)
			)

		INSERT INTO @TEMP
		(
			numItemCode
			,numWarehouseItemID
			,numUnitHour
			,dtReleaseDate
		)
		SELECT
			WOD.numChildItemID
			,WOD.numWareHouseItemId
			,WOD.numQtyItemsReq
			,(CASE WHEN OM.numOppId IS NOT NULL THEN ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate) ELSE WO.bintCompliationDate END)
		FROM
			WorkOrderDetails WOD
		INNER JOIN
			Item
		ON
			WOD.numChildItemID = Item.numItemCode
		INNER JOIN
			WareHouseItems WI
		ON
			WOD.numWareHouseItemId = WI.numWareHouseItemID
		INNER JOIN
			WorkOrder WO
		ON
			WOD.numWOId=WO.numWOId
		LEFT JOIN
			OpportunityItems OI
		ON
			WO.numOppItemID = OI.numoppitemtCode
		LEFT JOIN
			OpportunityMaster OM
		ON
			OI.numOppId=OM.numOppId
		WHERE
			WO.numDomainID=@numDomainID
			AND WO.numWOStatus <> 23184 -- NOT COMPLETED
			AND ISNULL(Item.IsArchieve,0) = 0
			AND ISNULL(Item.bitAssembly,0) = 0
			AND ISNULL(Item.bitKitParent,0) = 0
			AND ISNULL(WOD.numQtyItemsReq,0) > 0
			AND (OM.dtReleaseDate IS NOT NULL OR OI.ItemReleaseDate IS NOT NULL OR WO.bintCompliationDate IS NOT NULL)
			AND 1 = (CASE WHEN OM.numOppId IS NOT NULL THEN (CASE WHEN OM.tintOppStatus=1 THEN 1 ELSE 0 END) ELSE 1 END)
			AND
			(
				@bitWarehouseFilter = 0 OR
				WI.numWareHouseID IN (SELECT numWareHouseID FROM DemandForecastWarehouse WHERE numDFID =@numDFID)
			)
			AND
			(
				@bitItemClassificationFilter = 0 OR
				Item.numItemClassification IN (SELECT numItemClassificationID FROM DemandForecastItemClassification WHERE numDFID = @numDFID)
			)
			AND
			(
				@bitItemGroupFilter = 0 OR
				Item.numItemGroup IN (SELECT numItemGroupID FROM DemandForecastItemGroup WHERE numDFID = @numDFID)
			)

		CREATE TABLE #TEMPItems
		(
			numItemCode NUMERIC(18,0)
			,numWarehouseItemID NUMERIC(18,0)
			,numUnitHour FLOAT
			,dtReleaseDate VARCHAR(MAX)
			,dtReleaseDateHidden VARCHAR(MAX)
		)

		INSERT INTO #TEMPItems
		(
			numItemCode
			,numWarehouseItemID
			,numUnitHour
			,dtReleaseDate
			,dtReleaseDateHidden
		)
		SELECT
			numItemCode
			,numWarehouseItemID
			,SUM(numUnitHour)
			,STUFF((SELECT 
						CONCAT(', <a href="#" onclick="return OpenDFReleaseDateRecords(',numItemCode,',',numWarehouseItemID,',''',CONVERT(VARCHAR(10), dtReleaseDate, 101),''');">',CONVERT(VARCHAR(10), dtReleaseDate, 101),'</a> (',SUM(numUnitHour),')')
					FROM 
						@TEMP
					WHERE 
						numItemCode=T1.numItemCode
						AND numWarehouseItemID=T1.numWarehouseItemID
					GROUP BY
						numItemCode
						,numWarehouseItemID
						,dtReleaseDate
					FOR XML PATH(''),TYPE).value('(./text())[1]','VARCHAR(MAX)')
				  ,1,2,'')
			,STUFF((SELECT 
						CONCAT(', ',CONVERT(VARCHAR(10), dtReleaseDate, 101),' (',SUM(numUnitHour),')')
					FROM 
						@TEMP
					WHERE 
						numItemCode=T1.numItemCode
						AND numWarehouseItemID=T1.numWarehouseItemID
					GROUP BY
						numItemCode
						,numWarehouseItemID
						,dtReleaseDate
					FOR XML PATH(''),TYPE).value('(./text())[1]','VARCHAR(MAX)')
				  ,1,2,'')
		FROM
			@TEMP T1
		GROUP BY
			numItemCode
			,numWarehouseItemID

		---------------------------- Dynamic Query -------------------------------
		
		CREATE TABLE #tempForm 
		(
			tintOrder TINYINT,vcDbColumnName nvarchar(50),vcOrigDbColumnName nvarchar(50),vcFieldName nvarchar(50),vcAssociatedControlType nvarchar(50),vcListItemType varchar(3),
			numListID numeric(9),vcLookBackTableName varchar(50),bitCustomField BIT,numFieldId NUMERIC,bitAllowSorting BIT,bitAllowEdit BIT,bitIsRequired BIT,bitIsEmail BIT,
			bitIsAlphaNumeric BIT,bitIsNumeric BIT,bitIsLengthValidation BIT,intMaxLength int,intMinLength int,bitFieldMessage BIT,vcFieldMessage VARCHAR(500),
			ListRelID numeric(9),intColumnWidth int,bitAllowFiltering BIT,vcFieldDataType CHAR(1)
		)

		-- FIRST CHECK IF GRID COLUMNS ARE CONFIGURED
		DECLARE  @Nocolumns  AS TINYINT = 0

		SELECT
			@Nocolumns=ISNULL(SUM(TotalRow),0) 
		FROM
		(            
			SELECT 
				COUNT(*) TotalRow 
			FROM 
				View_DynamicColumns 
			WHERE 
				numFormId=139 
				AND numUserCntID=@numUserCntID 
				AND numDomainID=@numDomainID 
				AND tintPageType=1 
				AND numRelCntType = 0
			UNION 
			SELECT 
				COUNT(*) TotalRow 
			FROM 
				View_DynamicCustomColumns 
			WHERE 
				numFormId=139 
				AND numUserCntID=@numUserCntID 
				AND numDomainID=@numDomainID 
				AND tintPageType=1 
				AND numRelCntType = 0
		) TotalRows

		IF @Nocolumns=0
		BEGIN
			INSERT INTO DycFormConfigurationDetails 
			(
				numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth
			)
			SELECT 
				139,numFieldId,0,Row_number() over(order by tintRow desc),@numDomainID,@numUserCntID,0,1,0,intColumnWidth
			FROM 
				View_DynamicDefaultColumns
			WHERE 
				numFormId=139 
				AND bitDefault=1 
				AND ISNULL(bitSettingField,0)=1 
				AND numDomainID=@numDomainID
			ORDER BY 
				tintOrder asc   
		END

		INSERT INTO 
			#tempForm
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcOrigDbColumnName,ISNULL(vcCultureFieldName,vcFieldName),                  
			vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName,bitCustom,numFieldId,bitAllowSorting,bitInlineEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength,bitFieldMessage,
			vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType  
		FROM 
			View_DynamicColumns 
		WHERE 
			numFormId=139 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND ISNULL(bitSettingField,0)=1 
			AND ISNULL(bitCustom,0)=0  
			AND numRelCntType = 0
		UNION
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName,vcAssociatedControlType,'' as vcListItemType,numListID,'',bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,'' 
		FROM 
			View_DynamicCustomColumns
		WHERE 
			numFormId=139 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND ISNULL(bitCustom,0)=1 
			AND numRelCntType = 0
		ORDER BY 
			tintOrder asc
					
		DECLARE  @strColumns AS VARCHAR(MAX) = ''
		SET @strColumns = CONCAT(' COUNT(*) OVER() TotalRowsCount, Item.numItemCode, Item.vcItemName, Item.vcSKU
		,ISNULL((SELECT vcUnitName FROM UOM WHERE numUOMId = Item.numBaseUnit),''-'') AS vcUOM
		,Item.charItemType AS vcItemType
		,(SELECT TOP 1 numContactId FROM AdditionalContactsInformation WHERE numDivisionId = Item.numVendorID ORDER BY ISNULL(bitPrimaryContact,0) DESC) AS numContactID
		,ISNULL(Item.numPurchaseUnit,0) AS numUOM
		,dbo.fn_UOMConversion(ISNULL(numBaseUnit, 0), Item.numItemCode, Item.numDomainId, ISNULL(Item.numPurchaseUnit, 0)) fltUOMConversionfactor
		,dbo.fn_GetAttributes(WI.numWareHouseItemId,1) AS vcAttributes
		,WI.numWareHouseItemID
		,W.vcWarehouse
		,ISNULL(V.numVendorID,0) numVendorID
		,ISNULL(V.intMinQty,0) intMinQty
		,ISNULL(monCost,0) * dbo.fn_UOMConversion(Item.numBaseUnit,Item.numItemCode,Item.numDomainID,Item.numPurchaseUnit) AS monVendorCost
		,T1.numUnitHour AS numQtyToPurchase, T1.dtReleaseDateHidden, T1.dtReleaseDate,(CASE
			WHEN ',@tintUnitsRecommendationForAutoPOBackOrder,'= 3
			THEN 
				(ISNULL(numBackOrder,0) - ISNULL(numOnOrder,0) + (CASE WHEN ',@bitIncludeRequisitions,' = 1  
												THEN ISNULL((SELECT 
															SUM(numUnitHour) 
														FROM 
															OpportunityItems 
														INNER JOIN 
															OpportunityMaster 
														ON 
															OpportunityItems.numOppID=OpportunityMaster.numOppId 
														WHERE 
															OpportunityMaster.numDomainId=',@numDomainID,'
															AND OpportunityMaster.tintOppType=2 
															AND OpportunityMaster.tintOppStatus=0 
															AND OpportunityItems.numItemCode=Item.numItemCode 
															AND OpportunityItems.numWarehouseItmsID=WI.numWareHouseItemID),0) 
												ELSE 0 
												END)) +
				(CASE WHEN ISNULL(Item.fltReorderQty,0) >= ISNULL(V.intMinQty,0) THEN ISNULL(Item.fltReorderQty,0) ELSE ISNULL(V.intMinQty,0) END) 
			WHEN ',@tintUnitsRecommendationForAutoPOBackOrder,'=2
			THEN 
				(CASE
					WHEN ISNULL(Item.fltReorderQty,0) >= ISNULL(V.intMinQty,0) AND ISNULL(Item.fltReorderQty,0) >= ISNULL(numBackOrder,0) THEN ISNULL(Item.fltReorderQty,0)
					WHEN ISNULL(V.intMinQty,0) >= ISNULL(Item.fltReorderQty,0) AND ISNULL(V.intMinQty,0) >= ISNULL(numBackOrder,0) THEN ISNULL(V.intMinQty,0)
					WHEN ISNULL(numBackOrder,0) >= ISNULL(Item.fltReorderQty,0) AND ISNULL(numBackOrder,0) >= ISNULL(V.intMinQty,0) THEN ISNULL(numBackOrder,0)
					ELSE ISNULL(Item.fltReorderQty,0)
				END) - (ISNULL(numOnOrder,0) + (CASE WHEN ',@bitIncludeRequisitions,' = 1  THEN ISNULL((SELECT 
															SUM(numUnitHour) 
														FROM 
															OpportunityItems 
														INNER JOIN 
															OpportunityMaster 
														ON 
															OpportunityItems.numOppID=OpportunityMaster.numOppId 
														WHERE 
															OpportunityMaster.numDomainId=',@numDomainID,'
															AND OpportunityMaster.tintOppType=2 
															AND OpportunityMaster.tintOppStatus=0 
															AND OpportunityItems.numItemCode=Item.numItemCode 
															AND OpportunityItems.numWarehouseItmsID=WI.numWareHouseItemID),0) ELSE 0 END))
			ELSE
				(CASE WHEN ISNULL(Item.fltReorderQty,0) > ISNULL(V.intMinQty,0) THEN ISNULL(Item.fltReorderQty,0) ELSE ISNULL(V.intMinQty,0) END) 
				+ ISNULL(numBackOrder,0) - (ISNULL(numOnOrder,0) + (CASE WHEN ',@bitIncludeRequisitions,' = 1  THEN ISNULL((SELECT 
																														SUM(numUnitHour) 
																													FROM 
																														OpportunityItems 
																													INNER JOIN 
																														OpportunityMaster 
																													ON 
																														OpportunityItems.numOppID=OpportunityMaster.numOppId 
																													WHERE 
																														OpportunityMaster.numDomainId=',@numDomainID,'
																														AND OpportunityMaster.tintOppType=2 
																														AND OpportunityMaster.tintOppStatus=0 
																														AND OpportunityItems.numItemCode=Item.numItemCode 
																														AND OpportunityItems.numWarehouseItmsID=WI.numWareHouseItemID),0) ELSE 0 END)) 
		END) AS numQtyBasedOnPurchasePlan ')

		--Custom field 
		DECLARE @tintOrder AS TINYINT = 0                                                 
		DECLARE @vcFieldName AS VARCHAR(50)                                                  
		DECLARE @vcListItemType AS VARCHAR(3)                                             
		DECLARE @vcAssociatedControlType VARCHAR(20)     
		declare @numListID AS numeric(9)
		DECLARE @vcDbColumnName VARCHAR(50)                      
		DECLARE @WhereCondition VARCHAR(MAX) = ''                   
		DECLARE @vcLookBackTableName VARCHAR(2000)                
		DECLARE @bitCustom AS BIT
		DECLARE @bitAllowEdit AS CHAR(1)                   
		Declare @numFieldId as numeric  
		DECLARE @bitAllowSorting AS CHAR(1)    
		DECLARE @vcColumnName AS VARCHAR(500)             
		Declare @ListRelID as numeric(9) 
		DECLARE @SearchQuery AS VARCHAR(MAX) = '' 
	   
		SELECT TOP 1 
			@tintOrder=tintOrder+1
			,@vcDbColumnName=vcDbColumnName
			,@vcFieldName=vcFieldName
			,@vcAssociatedControlType=vcAssociatedControlType
			,@vcListItemType=vcListItemType
			,@numListID=numListID
			,@vcLookBackTableName=vcLookBackTableName
			,@bitCustom=bitCustomField
			,@numFieldId=numFieldId
			,@bitAllowSorting=bitAllowSorting
			,@bitAllowEdit=bitAllowEdit
			,@ListRelID=ListRelID
		FROM  
			#tempForm 
		ORDER BY 
			tintOrder ASC   

		WHILE @tintOrder>0                                                  
		BEGIN
	
			IF @bitCustom = 0  
			BEGIN
				DECLARE @Prefix AS VARCHAR(10)

				IF @vcLookBackTableName = 'AdditionalContactsInformation'
					SET @Prefix = 'ADC.'
				ELSE IF @vcLookBackTableName = 'DivisionMaster'
					SET @Prefix = 'Div.'
				ELSE IF @vcLookBackTableName = 'OpportunityMaster'
					SET @PreFix = 'Opp.'			
				ELSE IF @vcLookBackTableName = 'CompanyInfo'
					SET @PreFix = 'cmp.'
				ELSE IF @vcLookBackTableName = 'DemandForecastGrid'
					SET @PreFix = 'DF.'
				ELSE IF @vcLookBackTableName = 'Warehouses'
					SET @PreFix = 'W.'
				ELSE IF @vcLookBackTableName = 'WareHouseItems'
					SET @PreFix = 'WI.'
				ELSE 
					SET @PreFix = CONCAT(@vcLookBackTableName,'.')
			
				SET @vcColumnName= @vcDbColumnName+'~'+ convert(varchar(10),@numFieldId) +'~0'

				IF @vcAssociatedControlType = 'TextBox'
				BEGIN
					IF @vcDbColumnName = 'numQtyToPurchase'
					BEGIN
						SET @strColumns=@strColumns+',' + ' (CASE WHEN ISNULL(WI.numBackOrder,0) > ISNULL(WI.numOnOrder,0) THEN  CEILING(ISNULL(WI.numBackOrder,0) - ISNULL(WI.numOnOrder,0))  ELSE 0 END) '  + ' [' + @vcColumnName + ']'
					END
					ELSE IF @vcDbColumnName = 'moncost'
					BEGIN
						SET @strColumns=@strColumns+',' + ' ISNULL((SELECT vcUnitName FROM UOM WHERE numUOMId = Item.numBaseUnit),''-'') '  + ' [' + @vcColumnName + ']'
					END
					ELSE
					BEGIN
						SET @strColumns=@strColumns+','+  @Prefix + @vcDbColumnName +' ['+ @vcColumnName+']' 
					END
				END
				ELSE IF  @vcAssociatedControlType='Label'                                              
				BEGIN
					IF @vcDbColumnName = 'vc7Days'
					BEGIN
						SET @strColumns=@strColumns+ CONCAT(',dbo.fn_DemandForecastDaysDisplay(',@numDomainID,',',@ClientTimeZoneOffset,',Item.numItemCode,ISNULL(TempLeadDays.numLeadDays,0),WI.numWarehouseItemID,7,',@bitShowHistoricSales,',',@numHistoricalAnalysisPattern,',',@bitBasedOnLastYear,',',@bitIncludeOpportunity,',',@numOpportunityPercentComplete,',',@tintDemandPlanBasedOn,') [',@vcColumnName,']')
					END
					ELSE IF @vcDbColumnName = 'vc15Days'
					BEGIN
						SET @strColumns=@strColumns+ CONCAT(',dbo.fn_DemandForecastDaysDisplay(',@numDomainID,',',@ClientTimeZoneOffset,',Item.numItemCode,ISNULL(TempLeadDays.numLeadDays,0),WI.numWarehouseItemID,15,',@bitShowHistoricSales,',',@numHistoricalAnalysisPattern,',',@bitBasedOnLastYear,',',@bitIncludeOpportunity,',',@numOpportunityPercentComplete,',',@tintDemandPlanBasedOn,') [',@vcColumnName,']')
					END
					ELSE IF @vcDbColumnName = 'vc30Days'
					BEGIN
						SET @strColumns=@strColumns+ CONCAT(',dbo.fn_DemandForecastDaysDisplay(',@numDomainID,',',@ClientTimeZoneOffset,',Item.numItemCode,ISNULL(TempLeadDays.numLeadDays,0),WI.numWarehouseItemID,30,',@bitShowHistoricSales,',',@numHistoricalAnalysisPattern,',',@bitBasedOnLastYear,',',@bitIncludeOpportunity,',',@numOpportunityPercentComplete,',',@tintDemandPlanBasedOn,') [',@vcColumnName,']')
					END
					ELSE IF @vcDbColumnName = 'vc60Days'
					BEGIN
						SET @strColumns=@strColumns+ CONCAT(',dbo.fn_DemandForecastDaysDisplay(',@numDomainID,',',@ClientTimeZoneOffset,',Item.numItemCode,ISNULL(TempLeadDays.numLeadDays,0),WI.numWarehouseItemID,60,',@bitShowHistoricSales,',',@numHistoricalAnalysisPattern,',',@bitBasedOnLastYear,',',@bitIncludeOpportunity,',',@numOpportunityPercentComplete,',',@tintDemandPlanBasedOn,') [',@vcColumnName,']')
					END
					ELSE IF @vcDbColumnName = 'vc90Days'
					BEGIN
						SET @strColumns=@strColumns+ CONCAT(',dbo.fn_DemandForecastDaysDisplay(',@numDomainID,',',@ClientTimeZoneOffset,',Item.numItemCode,ISNULL(TempLeadDays.numLeadDays,0),WI.numWarehouseItemID,90,',@bitShowHistoricSales,',',@numHistoricalAnalysisPattern,',',@bitBasedOnLastYear,',',@bitIncludeOpportunity,',',@numOpportunityPercentComplete,',',@tintDemandPlanBasedOn,') [',@vcColumnName,']')
					END
					ELSE IF @vcDbColumnName = 'vc180Days'
					BEGIN
						SET @strColumns=@strColumns+ CONCAT(',dbo.fn_DemandForecastDaysDisplay(',@numDomainID,',',@ClientTimeZoneOffset,',Item.numItemCode,ISNULL(TempLeadDays.numLeadDays,0),WI.numWarehouseItemID,180,',@bitShowHistoricSales,',',@numHistoricalAnalysisPattern,',',@bitBasedOnLastYear,',',@bitIncludeOpportunity,',',@numOpportunityPercentComplete,',',@tintDemandPlanBasedOn,') [',@vcColumnName,']')
					END
					ELSE IF (@vcDbColumnName = 'vcBuyUOM')
					BEGIN
						SET @strColumns=@strColumns+',' + ' ISNULL((SELECT vcUnitName FROM UOM WHERE numUOMId = ISNULL(Item.numPurchaseUnit,0)),'''')' + ' [' + @vcColumnName + ']'
					END
					ELSE IF @vcDbColumnName = 'numOnOrderReq'
					BEGIN
						SET @strColumns=@strColumns+',' + ' (ISNULL(WI.numOnOrder,0) + ISNULL(WI.numReorder,0)) '  + ' [' + @vcColumnName + ']'
					END
					ELSE IF @vcDbColumnName = 'numAvailable'
					BEGIN
						SET @strColumns=@strColumns+',' + ' ISNULL(WI.numOnHand,0) '  + ' [' + @vcColumnName + ']'
					END
					ELSE IF @vcDbColumnName = 'numTotalOnHand'
					BEGIN
						SET @strColumns=@strColumns+',' + ' ISNULL((SELECT SUM(numOnHand) FROM WarehouseItems WIInner WHERE WIInner.numItemID=Item.numItemCode),0) '  + ' [' + @vcColumnName + ']'
					END
					ELSE
					BEGIN
						SET @strColumns=@strColumns + ',' + @Prefix + @vcDbColumnName  +' ['+ @vcColumnName+']' 
					END   
				END
				ELSE IF  @vcAssociatedControlType='SelectBox' or @vcAssociatedControlType='ListBox'  
				BEGIN
					IF @vcDbColumnName = 'numVendorID'
					BEGIN
						SET @strColumns=@strColumns+',' + ' V.numVendorID '  + ' [' + @vcColumnName + ']'
					END
					ELSE IF @vcDbColumnName = 'ShipmentMethod'
					BEGIN
						SET @strColumns=@strColumns+',' + ' '''' '  + ' [' + @vcColumnName + ']'
					END
					ELSE
					BEGIN
						SET @strColumns=@strColumns + ',' + @Prefix + @vcDbColumnName  +' ['+ @vcColumnName+']' 
					END
				END
				ELSE IF @vcAssociatedControlType='DateField'   
				BEGIN
					IF @vcDbColumnName = 'dtExpectedDelivery'
					BEGIN
						SET @strColumns=@strColumns+',' + ' GETDATE() '  + ' [' + @vcColumnName + ']'
					END
					ELSE IF @vcDbColumnName = 'ItemRequiredDate'
					BEGIN
						SET @strColumns=@strColumns+',T1.dtReleaseDate [' + @vcColumnName + ']'
					END
					ELSE
					BEGIN
						SET @strColumns=@strColumns + ',' + @Prefix + @vcDbColumnName  +' ['+ @vcColumnName+']' 
					END					
				END
			END

			SELECT TOP 1 
				@tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName,@vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,
				@numListID=numListID,@vcLookBackTableName=vcLookBackTableName,@bitCustom=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,
				@bitAllowEdit=bitAllowEdit,@ListRelID=ListRelID
			FROM
				#tempForm 
			WHERE 
				tintOrder > @tintOrder-1 
			ORDER BY 
				tintOrder asc            
 
			IF @@rowcount=0 SET @tintOrder=0 

		 END
	  
		DECLARE @StrSql AS VARCHAR(MAX) = ''
		SET @StrSql = CONCAT('SELECT ',@strColumns,' 
							FROM
								#TEMPItems T1
							INNER JOIN
								Item
							ON
								T1.numItemCode = Item.numItemCode
							INNER JOIN
								WarehouseItems WI
							ON
								Item.numItemCode=WI.numItemID
								AND T1.numWarehouseItemID=WI.numWarehouseItemID
							INNER JOIN
								Warehouses W
							ON
								WI.numWarehouseID = W.numWareHouseID
							LEFT JOIN
								Vendor V
							ON
								Item.numVendorID = V.numVendorID
								AND Item.numItemCode = V.numItemCode
							OUTER APPLY
							(
								SELECT TOP 1
									numListValue AS numLeadDays
								FROM
									VendorShipmentMethod VSM
								WHERE
									VSM.numVendorID = V.numVendorID
								ORDER BY
									ISNULL(bitPrimary,0) DESC, bitPreferredMethod DESC, numListItemID ASC
							) TempLeadDays
							WHERE
								Item.numDomainID=',@numDomainID,'
								AND 1 = (CASE 
											WHEN ',@bitShowAllItems,' = 1 
											THEN 1 
											ELSE 
												(CASE 
													WHEN ', ISNULL(@tintDemandPlanBasedOn,1),' = 2
													THEN (CASE 
															WHEN (CASE
																	WHEN ',@tintUnitsRecommendationForAutoPOBackOrder,'= 3
																	THEN 
																		(ISNULL(numBackOrder,0) - ISNULL(numOnOrder,0) + (CASE WHEN ',@bitIncludeRequisitions,' = 1  
																										THEN ISNULL((SELECT 
																													SUM(numUnitHour) 
																												FROM 
																													OpportunityItems 
																												INNER JOIN 
																													OpportunityMaster 
																												ON 
																													OpportunityItems.numOppID=OpportunityMaster.numOppId 
																												WHERE 
																													OpportunityMaster.numDomainId=',@numDomainID,'
																													AND OpportunityMaster.tintOppType=2 
																													AND OpportunityMaster.tintOppStatus=0 
																													AND OpportunityItems.numItemCode=Item.numItemCode 
																													AND OpportunityItems.numWarehouseItmsID=WI.numWareHouseItemID),0) 
																										ELSE 0 
																										END)) +
																		(CASE WHEN ISNULL(Item.fltReorderQty,0) >= ISNULL(V.intMinQty,0) THEN ISNULL(Item.fltReorderQty,0) ELSE ISNULL(V.intMinQty,0) END) 
																	WHEN ',@tintUnitsRecommendationForAutoPOBackOrder,'=2
																	THEN 
																		(CASE
																			WHEN ISNULL(Item.fltReorderQty,0) >= ISNULL(V.intMinQty,0) AND ISNULL(Item.fltReorderQty,0) >= ISNULL(numBackOrder,0) THEN ISNULL(Item.fltReorderQty,0)
																			WHEN ISNULL(V.intMinQty,0) >= ISNULL(Item.fltReorderQty,0) AND ISNULL(V.intMinQty,0) >= ISNULL(numBackOrder,0) THEN ISNULL(V.intMinQty,0)
																			WHEN ISNULL(numBackOrder,0) >= ISNULL(Item.fltReorderQty,0) AND ISNULL(numBackOrder,0) >= ISNULL(V.intMinQty,0) THEN ISNULL(numBackOrder,0)
																			ELSE ISNULL(Item.fltReorderQty,0)
																		END) - (ISNULL(numOnOrder,0) + (CASE WHEN ',@bitIncludeRequisitions,' = 1  THEN ISNULL((SELECT 
																													SUM(numUnitHour) 
																												FROM 
																													OpportunityItems 
																												INNER JOIN 
																													OpportunityMaster 
																												ON 
																													OpportunityItems.numOppID=OpportunityMaster.numOppId 
																												WHERE 
																													OpportunityMaster.numDomainId=',@numDomainID,'
																													AND OpportunityMaster.tintOppType=2 
																													AND OpportunityMaster.tintOppStatus=0 
																													AND OpportunityItems.numItemCode=Item.numItemCode 
																													AND OpportunityItems.numWarehouseItmsID=WI.numWareHouseItemID),0) ELSE 0 END))
																	ELSE
																		(CASE WHEN ISNULL(Item.fltReorderQty,0) > ISNULL(V.intMinQty,0) THEN ISNULL(Item.fltReorderQty,0) ELSE ISNULL(V.intMinQty,0) END) 
																		+ ISNULL(numBackOrder,0) - (ISNULL(numOnOrder,0) + (CASE WHEN ',@bitIncludeRequisitions,' = 1  THEN ISNULL((SELECT 
																																												SUM(numUnitHour) 
																																											FROM 
																																												OpportunityItems 
																																											INNER JOIN 
																																												OpportunityMaster 
																																											ON 
																																												OpportunityItems.numOppID=OpportunityMaster.numOppId 
																																											WHERE 
																																												OpportunityMaster.numDomainId=',@numDomainID,'
																																												AND OpportunityMaster.tintOppType=2 
																																												AND OpportunityMaster.tintOppStatus=0 
																																												AND OpportunityItems.numItemCode=Item.numItemCode 
																																												AND OpportunityItems.numWarehouseItmsID=WI.numWareHouseItemID),0) ELSE 0 END)) 
																END) > 0
															THEN 1
															ELSE 0
														END)
													ELSE
														(CASE 
															WHEN 
																T1.numUnitHour > 0 
															THEN 1 
															ELSE 0 
														END)
												END)
										END)
							')

		IF CHARINDEX('I.',@vcRegularSearchCriteria) > 0
		BEGIN			   
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'I.','Item.')
		END
		IF CHARINDEX('WH.',@vcRegularSearchCriteria) > 0
		BEGIN			   
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'WH.','W.')
		END
	
		IF @vcRegularSearchCriteria<>'' 
		BEGIN
			SET @strSql=@strSql +' AND ' + @vcRegularSearchCriteria 
		END
	  
		DECLARE  @firstRec  AS INTEGER
		DECLARE  @lastRec  AS INTEGER
  
		SET @firstRec = (@CurrentPage - 1) * @PageSize
		SET @lastRec = (@CurrentPage * @PageSize + 1)

		-- EXECUTE FINAL QUERY
		DECLARE @strFinal AS NVARCHAR(MAX)
		SET @strFinal = CONCAT(@StrSql,' ORDER BY ',CASE WHEN CHARINDEX(@columnName,@strColumns) > 0 THEN @columnName ELSE 'Item.numItemCode' END,' OFFSET ',(@CurrentPage-1) * @PageSize,' ROWS FETCH NEXT ',@PageSize,' ROWS ONLY')

		PRINT CAST(@strFinal AS NTEXT)
		EXEC sp_executesql @strFinal

		SELECT * FROM #tempForm

		DROP TABLE #tempForm

		DROP TABLE #TEMPItems


		UPDATE DemandForecast SET dtExecutionDate = dateadd(MINUTE,-@ClientTimeZoneOffset,GETDATE()) WHERE numDFID = @numDFID

		SELECT CONVERT(VARCHAR(20),dateadd(MINUTE,-@ClientTimeZoneOffset,@dtLastExecution)) AS dtLastExecution
	END
END
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DivisionMasterShippingAccount_GetByDividionAndShipViaID')
DROP PROCEDURE USP_DivisionMasterShippingAccount_GetByDividionAndShipViaID
GO
CREATE PROCEDURE [dbo].[USP_DivisionMasterShippingAccount_GetByDividionAndShipViaID]    
	@numDivisionID NUMERIC(18,0)
	,@numShipViaID NUMERIC(18,0)
AS
BEGIN
	SELECT * FROM DivisionMasterShippingAccount WHERE numDivisionID=@numDivisionID AND numShipViaID=@numShipViaID
END
GO
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DivisionMasterShippingAccount_Save')
DROP PROCEDURE USP_DivisionMasterShippingAccount_Save
GO
CREATE PROCEDURE [dbo].[USP_DivisionMasterShippingAccount_Save]    
	@numUserCntID NUMERIC(18,0)
	,@numDivisionID NUMERIC(18,0)
	,@numShipViaID NUMERIC(18,0)
	,@vcAccountNumber VARCHAR(100)
	,@vcStreet VARCHAR(300)
	,@vcCity VARCHAR(100)
	,@numCountry NUMERIC(18,0)
	,@numState NUMERIC(18,0)
	,@vcZipCode VARCHAR(50)
AS
BEGIN
	IF EXISTS (SELECT ID FROM DivisionMasterShippingAccount WHERE numDivisionID=@numDivisionID AND numShipViaID=@numShipViaID)
	BEGIN
		UPDATE
			DivisionMasterShippingAccount 
		SET
			vcAccountNumber=@vcAccountNumber
			,vcStreet=@vcStreet
			,vcCity=@vcCity
			,numCountry=@numCountry
			,numState=@numState
			,vcZipCode=@vcZipCode
			,numModifiedBy=@numUserCntID
			,dtModifiedDate=GETUTCDATE()
		WHERE 
			numDivisionID=@numDivisionID 
			AND numShipViaID=@numShipViaID
	END
	ELSE
	BEGIN
		INSERT INTO DivisionMasterShippingAccount
		(
			numDivisionID
			,numShipViaID
			,vcAccountNumber
			,vcStreet
			,vcCity
			,numCountry
			,numState
			,vcZipCode
			,numCreatedBy
			,dtCreatedDate
		)
		VALUES
		(
			@numDivisionID
			,@numShipViaID
			,@vcAccountNumber
			,@vcStreet
			,@vcCity
			,@numCountry
			,@numState
			,@vcZipCode
			,@numUserCntID
			,GETUTCDATE()
		)
	END
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetAcivitybyGoogleEventId')
DROP PROCEDURE USP_GetAcivitybyGoogleEventId
GO
CREATE PROCEDURE [dbo].[USP_GetAcivitybyGoogleEventId]      
 @GoogleEventId varchar(MAX),
 @ResourceID AS int 
as       
      
 SELECT       
  [Activity].[AllDayEvent],       
  [Activity].[ActivityDescription],       
  [Activity].[Duration],       
  [Activity].[Location],       
  [Activity].[ActivityID],       
  [Activity].[StartDateTimeUtc],       
  [Activity].[Subject],       
  [Activity].[EnableReminder],       
  [Activity].[ReminderInterval],      
  [Activity].[ShowTimeAs],      
  [Activity].[Importance],      
  [Activity].[Status],      
  [Activity].[RecurrenceID],      
  [Activity].[VarianceID],      
[ActivityResource].[ResourceID],      
[Activity].[ItemID],    
[Activity].[ChangeKey],     
[Activity].[ItemIDOccur],  
  [Activity].[_ts] ,  [Activity].GoogleEventId   
 FROM       
  [Activity] INNER JOIN [ActivityResource] ON [Activity].[ActivityID] = [ActivityResource].[ActivityID]      
 WHERE       
  ISNULL(Activity.GoogleEventId,'') = @GoogleEventId AND ISNULL(ResourceID,0)=@ResourceID;   
GO
/****** Object:  StoredProcedure [dbo].[USP_GetDealsList1]    Script Date: 05/07/2009 22:12:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getdealslist1')
DROP PROCEDURE usp_getdealslist1
GO
CREATE PROCEDURE [dbo].[USP_GetDealsList1]
               @numUserCntID              NUMERIC(9)  = 0,
               @numDomainID               NUMERIC(9)  = 0,
               @tintSalesUserRightType    TINYINT  = 0,
               @tintPurchaseUserRightType TINYINT  = 0,
               @tintSortOrder             TINYINT  = 4,
               @CurrentPage               INT,
               @PageSize                  INT,
               @TotRecs                   INT  OUTPUT,
               @columnName                AS VARCHAR(50),
               @columnSortOrder           AS VARCHAR(10),
               @numCompanyID              AS NUMERIC(9)  = 0,
               @bitPartner                AS BIT  = 0,
               @ClientTimeZoneOffset      AS INT,
               @tintShipped               AS TINYINT,
               @intOrder                  AS TINYINT,
               @intType                   AS TINYINT,
               @tintFilterBy              AS TINYINT  = 0,
               @numOrderStatus			  AS NUMERIC,
			   @vcRegularSearchCriteria varchar(MAX)='',
			   @vcCustomSearchCriteria varchar(MAX)='',
			   @SearchText VARCHAR(300) = '',
			   @SortChar char(1)='0',
			   @tintDashboardReminderType TINYINT = 0
AS
BEGIN
	DECLARE @PageId  AS TINYINT
	DECLARE @numFormId  AS INT 
	DECLARE @tintDecimalPoints TINYINT

	SELECT
		@tintDecimalPoints=ISNULL(tintDecimalPoints,0)
	FROM
		Domain 
	WHERE 
		numDomainId=@numDomainID

	SET @PageId = 0

	IF @inttype = 1
	BEGIN
		SET @PageId = 2
		SET @inttype = 3
		SET @numFormId=39
	END
	ELSE IF @inttype = 2
	BEGIN
		SET @PageId = 6
		SET @inttype = 4
		SET @numFormId=41
	END

	CREATE TABLE #tempForm 
	(
		tintOrder TINYINT,vcDbColumnName nvarchar(200),vcOrigDbColumnName nvarchar(50),vcFieldName nvarchar(50),vcAssociatedControlType nvarchar(50),vcListItemType varchar(3),
		numListID numeric(9),vcLookBackTableName varchar(50),bitCustomField BIT,numFieldId NUMERIC,bitAllowSorting BIT,bitAllowEdit BIT,bitIsRequired BIT,bitIsEmail BIT,
		bitIsAlphaNumeric BIT,bitIsNumeric BIT,bitIsLengthValidation BIT,intMaxLength int,intMinLength int,bitFieldMessage BIT,vcFieldMessage VARCHAR(500),
		ListRelID numeric(9),intColumnWidth int,bitAllowFiltering BIT,vcFieldDataType CHAR(1)
	)

	-- FIRST CHECK IF GRID COLUMNS ARE CONFIGURED
	DECLARE  @Nocolumns  AS TINYINT = 0

	SELECT 
		@Nocolumns=ISNULL(SUM(TotalRow),0) 
	FROM
	(            
		SELECT 
			COUNT(*) TotalRow 
		FROM 
			View_DynamicColumns 
		WHERE 
			numFormId=@numFormId 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND numRelCntType = @numFormId
		UNION 
		SELECT 
			COUNT(*) TotalRow 
		FROM 
			View_DynamicCustomColumns 
		WHERE 
			numFormId=@numFormId 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND numRelCntType = @numFormId
	) TotalRows

	-- IF GRID COLUMNS ARE NOT CONFIGURES THEN CHECK IF MASTER FORM CONFIGURATION IS CREATED BY ADMINISTRATOR
	DECLARE @IsMasterConfAvailable BIT = 0
	DECLARE @numUserGroup NUMERIC(18,0)

	IF @Nocolumns=0
	BEGIN
		SELECT @numUserGroup = numGroupID FROM UserMaster WHERE numUserDetailId = @numUserCntID 

		IF (SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = @numDomainID AND numFormID = @numFormId AND numRelCntType=@numFormId AND bitGridConfiguration = 1 AND numGroupID = @numUserGroup) > 0
		BEGIN
			SET @IsMasterConfAvailable = 1
		END
	END

	--IF MASTERCONFIGURATION IS AVAILABLE THEN LOAD IT OTHERWISE LOAD DEFAULT COLUMNS
	IF @IsMasterConfAvailable = 1
	BEGIN
		INSERT INTO DycFormConfigurationDetails 
		(
			numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth
		)
		SELECT 
			@numFormId,numFieldId,intColumnNum,intRowNum,@numDomainID,@numUserCntID,@numFormId,1,0,intColumnWidth
		FROM 
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId=@numFormId AND 
			View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicColumnsMasterConfig.numRelCntType=@numFormId AND 
			View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
			ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
		UNION
		SELECT 
			@numFormId,numFieldId,intColumnNum,intRowNum,@numDomainID,@numUserCntID,@numFormId,1,1,intColumnWidth
		FROM 
			View_DynamicCustomColumnsMasterConfig
		WHERE 
			View_DynamicCustomColumnsMasterConfig.numFormId=@numFormId AND 
			View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicCustomColumnsMasterConfig.numRelCntType=@numFormId AND 
			View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1


		INSERT INTO 
			#tempForm
		SELECT 
			(intRowNum + 1) as tintOrder, vcDbColumnName,	vcOrigDbColumnName,	ISNULL(vcCultureFieldName,vcFieldName), vcAssociatedControlType,
			vcListItemType, numListID, vcLookBackTableName,	bitCustom, numFieldId, bitAllowSorting,	bitInlineEdit, bitIsRequired,
			bitIsEmail,	bitIsAlphaNumeric, bitIsNumeric, bitIsLengthValidation,	intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,
			ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType 
		FROM 
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId=@numFormId AND 
			View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicColumnsMasterConfig.numRelCntType=@numFormId AND 
			View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
			ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
		UNION
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName, vcAssociatedControlType,'' as vcListItemType,numListID,'',
			bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,''
		FROM 
			View_DynamicCustomColumnsMasterConfig
		WHERE 
			View_DynamicCustomColumnsMasterConfig.numFormId=@numFormId AND 
			View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicCustomColumnsMasterConfig.numRelCntType=@numFormId AND 
			View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1
		ORDER BY 
			tintOrder ASC  
	END
	ELSE                                        
	BEGIN
		IF @Nocolumns=0
		BEGIN
			INSERT INTO DycFormConfigurationDetails 
			(
				numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth
			)
			SELECT 
				@numFormId,numFieldId,0,Row_number() over(order by tintRow desc),@numDomainID,@numUserCntID,@numFormId,1,0,intColumnWidth
			FROM 
				View_DynamicDefaultColumns
			WHERE 
				numFormId=@numFormId 
				AND bitDefault=1 
				AND ISNULL(bitSettingField,0)=1 
				AND numDomainID=@numDomainID
			ORDER BY 
				tintOrder asc   
		END

		INSERT INTO 
			#tempForm
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcOrigDbColumnName,ISNULL(vcCultureFieldName,vcFieldName),                  
			vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName,bitCustom,numFieldId,bitAllowSorting,bitInlineEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength,bitFieldMessage,
			vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType  
		FROM 
			View_DynamicColumns 
		WHERE 
			numFormId=@numFormId 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND ISNULL(bitSettingField,0)=1 
			AND ISNULL(bitCustom,0)=0  
			AND numRelCntType = @numFormId
		UNION
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName,vcAssociatedControlType,'' as vcListItemType,numListID,'',bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,'' 
		FROM 
			View_DynamicCustomColumns
		WHERE 
			numFormId=@numFormId 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND ISNULL(bitCustom,0)=1 
			AND numRelCntType = @numFormId
		ORDER BY 
			tintOrder asc  
	END

	DECLARE  @strColumns AS VARCHAR(MAX)
	SET @strColumns = ' ADC.numContactId, ADC.vcEmail, Div.numDivisionID,ISNULL(Div.numTerID,0) numTerID, opp.numRecOwner as numRecOwner, Div.tintCRMType, opp.numOppId, ISNULL(Opp.monDealAmount,0) monDealAmount 
	,ISNULL(opp.numShippingService,0) AS numShippingService,ADC.vcFirstName+'' ''+ADC.vcLastName AS vcContactName,ADC.numPhone AS numContactPhone,ADC.numPhoneExtension AS numContactPhoneExtension,ADC.vcEmail AS vcContactEmail '
	--Corr.bitIsEmailSent,

	DECLARE  @tintOrder  AS TINYINT;SET @tintOrder = 0
	DECLARE  @vcFieldName  AS VARCHAR(50)
	DECLARE  @vcListItemType  AS VARCHAR(3)
	DECLARE  @vcListItemType1  AS VARCHAR(1)
	DECLARE  @vcAssociatedControlType VARCHAR(30)
	DECLARE  @numListID  AS NUMERIC(9)
	DECLARE  @vcDbColumnName VARCHAR(200)
	DECLARE  @WhereCondition VARCHAR(2000);SET @WhereCondition = ''
	DECLARE  @vcLookBackTableName VARCHAR(2000)
	DECLARE  @bitCustom  AS BIT
	Declare @numFieldId as numeric  
	DECLARE @bitAllowSorting AS CHAR(1)   
	DECLARE @bitAllowEdit AS CHAR(1)                 
	DECLARE @vcColumnName AS VARCHAR(500)          
	
	Declare @ListRelID as numeric(9) 
	DECLARE @SearchQuery AS VARCHAR(MAX) = ''        

	SELECT TOP 1 
		@tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName,@vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,
		@numListID=numListID,@vcLookBackTableName=vcLookBackTableName ,@bitCustom=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,@bitAllowEdit=bitAllowEdit,
		@ListRelID=ListRelID
	FROM 
		#tempForm 
	ORDER BY 
		tintOrder ASC            

	WHILE @tintOrder > 0
    BEGIN
		IF @bitCustom = 0
		BEGIN
			DECLARE  @Prefix  AS VARCHAR(10)

			IF @vcLookBackTableName = 'AdditionalContactsInformation'
				SET @Prefix = 'ADC.'
			IF @vcLookBackTableName = 'DivisionMaster'
				SET @Prefix = 'Div.'
			IF @vcLookBackTableName = 'OpportunityMaster'
				SET @PreFix = 'Opp.'
			IF @vcLookBackTableName = 'OpportunityRecurring'
				SET @PreFix = 'OPR.'
			IF @vcLookBackTableName = 'CompanyInfo'
				SET @PreFix = 'cmp.'
			IF @vcLookBackTableName = 'ProjectProgress'
				SET @PreFix = 'PP.'
			SET @vcColumnName= @vcDbColumnName+'~'+ convert(varchar(10),@numFieldId) +'~0'

			IF @vcAssociatedControlType = 'SelectBox'
			BEGIN
				-- KEEP THIS IF CONDITION SEPERATE FROM IF ELSE
				IF @vcDbColumnName = 'numStatus'
				BEGIN
					SET @strColumns=@strColumns+ ' ,(SELECT COUNT(OpportunityItems.numoppitemtCode) FROM OpportunityItems WHERE OpportunityItems.bitItemPriceApprovalRequired=1 AND OpportunityItems.numOppId=Opp.numOppId) ApprovalMarginCount'
				END


				IF @vcDbColumnName = 'numPartner'
				BEGIN
					SET @strColumns=@strColumns+ ' ,(Opp.numPartner) '+' ['+ @vcColumnName+']'    
					SET @strColumns=@strColumns+ ' ,(SELECT D.vcPartnerCode+''-''+C.vcCompanyName FROM DivisionMaster AS D LEFT JOIN CompanyInfo AS C ON D.numCompanyID=C.numCompanyID WHERE D.numDivisionID=Opp.numPartner) AS vcPartner' 
				END
				ELSE IF @vcDbColumnName = 'vcSignatureType'
				BEGIN
					SET @strColumns=@strColumns+ ' ,(SELECT CASE WHEN vcSignatureType = 0 THEN ''Service Default''
																	WHEN vcSignatureType = 1 THEN ''Adult Signature Required''
																	WHEN vcSignatureType = 2 THEN ''Direct Signature''
																	WHEN vcSignatureType = 3 THEN ''InDirect Signature''
																	WHEN vcSignatureType = 4 THEN ''No Signature Required''
																ELSE '''' END 
					FROM DivisionMaster AS D INNER JOIN DivisionMasterShippingConfiguration AS DMSC ON DMSC.numDivisionID=D.numDivisionID 
					WHERE D.numDivisionID=Div.numDivisionID) '  + ' [' + @vcColumnName + ']'
				END
				ELSE IF @vcDbColumnName = 'numShippingService'
				BEGIN
					SET @strColumns=@strColumns+ ' ,ISNULL((SELECT vcShipmentService FROM ShippingService WHERE (numDomainID=Opp.numDomainID OR ISNULL(numDomainID,0)=0) AND numShipmentServiceID = ISNULL(Opp.numShippingService,0)),'''')' + ' [' + @vcColumnName + ']'
				END
				ELSE IF @vcDbColumnName = 'tintEDIStatus'
				BEGIN
					--SET @strColumns = @strColumns + ',(CASE Opp.tintEDIStatus WHEN 1 THEN ''850 Received'' WHEN 2 THEN ''850 Acknowledged'' WHEN 3 THEN ''940 Sent'' WHEN 4 THEN ''940 Acknowledged'' WHEN 5 THEN ''856 Received'' WHEN 6 THEN ''856 Acknowledged'' WHEN 7 THEN ''856 Sent'' ELSE '''' END)' + ' [' + @vcColumnName + ']'
					SET @strColumns = @strColumns + ',(CASE Opp.tintEDIStatus WHEN 2 THEN ''850 Acknowledged'' WHEN 3 THEN ''940 Sent'' WHEN 4 THEN ''940 Acknowledged'' WHEN 5 THEN ''856 Received'' WHEN 6 THEN ''856 Acknowledged'' WHEN 7 THEN ''856 Sent'' WHEN 11 THEN ''850 Partially Created'' WHEN 12 THEN ''850 SO Created'' ELSE '''' END)' + ' [' + @vcColumnName + ']'
				END
				ELSE IF @vcDbColumnName = 'numPartenerContact'
				BEGIN
					SET @strColumns=@strColumns+ ' ,(Opp.numPartenerContact) '+' ['+ @vcColumnName+']'    
					SET @strColumns=@strColumns+ ' ,(SELECT A.vcFirstName+'' ''+ A.vcLastName FROM AdditionalContactsInformation AS A WHERE A.numContactId=Opp.numPartenerContact) AS vcPartenerContact' 
				END
				ELSE IF @vcDbColumnName = 'tintInvoicing'
				BEGIN
					SET @strColumns=CONCAT(@strColumns,', CONCAT(ISNULL((SELECT SUBSTRING((SELECT ''$^$'' + CONCAT(numOppBizDocsId,''#^#'',vcBizDocID,''#^#'',ISNULL(bitisemailsent,0),''#^#'')
										FROM OpportunityBizDocs WHERE numOppId=Opp.numOppId AND numBizDocId = ISNULL((SELECT TOP 1 numAuthoritativeSales FROM AuthoritativeBizDocs WHERE numDomainId=' + CAST (@numDomainID AS VARCHAR) + '),287) FOR XML PATH('''')),4,200000)),'''')',
										CASE 
										WHEN CHARINDEX('tintInvoicing=3',@vcRegularSearchCriteria) > 0
											THEN ', (SELECT 
													CONCAT(''('',SUM(X.InvoicedQty),'' Out of '',SUM(X.OrderedQty),'')'')
												FROM 
												(
													SELECT
														OI.numoppitemtCode,
														ISNULL(OI.numUnitHour,0) AS OrderedQty,
														ISNULL(TempInvoice.InvoicedQty,0) AS InvoicedQty
													FROM
														OpportunityItems OI
													INNER JOIN
														Item I
													ON
														OI.numItemCode = I.numItemCode
													OUTER APPLY
													(
														SELECT
															SUM(OpportunityBizDocItems.numUnitHour) AS InvoicedQty
														FROM
															OpportunityBizDocs
														INNER JOIN
															OpportunityBizDocItems 
														ON
															OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
														WHERE
															OpportunityBizDocs.numOppId = Opp.numOppId
															AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
															AND ISNULL(OpportunityBizDocs.bitAuthoritativeBizDocs,0) = 1
													) AS TempInvoice
													WHERE
														OI.numOppID = Opp.numOppId
												) X)'

											WHEN CHARINDEX('tintInvoicing=4',@vcRegularSearchCriteria) > 0
											THEN ', (SELECT 
													CONCAT(''('',(X.InvoicedPercentage),'' % '','')'')
												FROM 
												(

													SELECT fltBreakupPercentage  AS InvoicedPercentage
													FROM dbo.OpportunityRecurring OPR 
														INNER JOIN dbo.OpportunityMaster OM ON OPR.numOppID = OM.numOppID
														LEFT JOIN dbo.OpportunityBizDocs OBD ON OBD.numOppBizDocsID = OPR.numOppBizDocID
													WHERE OPR.numOppId = Opp.numOppId 
														and tintRecurringType <> 4
												) X)'

											ELSE 
													','''''
											END,') [', @vcColumnName,']')


				END
				ELSE IF @vcDbColumnName = 'tintSource'
				BEGIN
					SET @strColumns = @strColumns + ',ISNULL(dbo.fn_GetOpportunitySourceValue(ISNULL(Opp.tintSource,0),ISNULL(Opp.tintSourceType,0),Opp.numDomainID),''Internal Order'')' + ' [' + @vcColumnName + ']'

					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'ISNULL(dbo.fn_GetOpportunitySourceValue(ISNULL(Opp.tintSource,0),ISNULL(Opp.tintSourceType,0),Opp.numDomainID),''Internal Order'') LIKE ''%' + @SearchText + '%'''
					END
				END
				ELSE IF @vcDbColumnName = 'numCampainID'
				BEGIN
					SET @strColumns = @strColumns + ', (SELECT vcCampaignName FROM CampaignMaster WHERE numCampaignID= Opp.numCampainID) ' + ' [' + @vcColumnName + ']'
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'ISNULL((SELECT vcCampaignName FROM CampaignMaster WHERE numCampaignID= Opp.numCampainID),'''') LIKE ''%' + @SearchText + '%'''
					END
				END
				ELSE IF @vcDbColumnName = 'numContactID'
				BEGIN
					SET @strColumns = @strColumns + ',dbo.fn_GetContactName(' + @Prefix + @vcDbColumnName + ') [' + @vcColumnName + ']'
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'dbo.fn_GetContactName(' + @Prefix + @vcDbColumnName + ') LIKE ''%' + @SearchText + '%'''
					END
				END
				ELSE IF @vcListItemType = 'LI'
				BEGIN
					SET @strColumns = @strColumns + ',L' + CONVERT(VARCHAR(3),@tintOrder) + '.vcData' + ' [' + @vcColumnName + ']'
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'L' + CONVERT(VARCHAR(3),@tintOrder) + '.vcData LIKE ''%' + @SearchText + '%'''
					END

					SET @WhereCondition = @WhereCondition + ' left Join ListDetails L' + CONVERT(VARCHAR(3),@tintOrder) + ' on L' + CONVERT(VARCHAR(3),@tintOrder) + '.numListItemID=' + @vcDbColumnName
				END
				ELSE IF @vcListItemType = 'S'
				BEGIN
					SET @strColumns = @strColumns + ',S' + CONVERT(VARCHAR(3),@tintOrder) + '.vcState' + ' [' + @vcColumnName + ']'
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'S' + CONVERT(VARCHAR(3),@tintOrder) + '.vcState LIKE ''%' + @SearchText + '%'''
					END

					SET @WhereCondition = @WhereCondition + ' left join State S' + CONVERT(VARCHAR(3),@tintOrder) + ' on S' + CONVERT(VARCHAR(3),@tintOrder) + '.numStateID=' + @vcDbColumnName
				END
				ELSE IF @vcListItemType = 'BP'
				BEGIN
					SET @strColumns = @strColumns + ',SPLM.Slp_Name' + ' [' + @vcColumnName + ']'
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'SPLM.Slp_Name LIKE ''%' + @SearchText + '%'''
					END

					SET @WhereCondition = @WhereCondition + ' LEFT JOIN Sales_process_List_Master SPLM on SPLM.Slp_Id = OPP.numBusinessProcessID '
				END
				ELSE IF @vcListItemType = 'PP'
				BEGIN
					SET @strColumns = @strColumns + ',ISNULL(PP.intTotalProgress,0)' + ' [' + @vcColumnName + ']'

					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'ISNULL(PP.intTotalProgress,0) LIKE ''%' + @SearchText + '%'''
					END
				END
				ELSE IF @vcListItemType = 'T'
				BEGIN
					SET @strColumns = @strColumns + ',L' + CONVERT(VARCHAR(3),@tintOrder) + '.vcData' + ' [' + @vcColumnName + ']'
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'L' + CONVERT(VARCHAR(3),@tintOrder) + '.vcData LIKE ''%' + @SearchText + '%'''
					END

					SET @WhereCondition = @WhereCondition + ' left Join ListDetails L' + CONVERT(VARCHAR(3),@tintOrder) + ' on L' + CONVERT(VARCHAR(3),@tintOrder) + '.numListItemID=' + @vcDbColumnName
				END
				ELSE IF @vcListItemType = 'U'
				BEGIN
					SET @strColumns = @strColumns + ',dbo.fn_GetContactName(' + @Prefix + @vcDbColumnName + ') [' + @vcColumnName + ']'
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'dbo.fn_GetContactName(' + @Prefix + @vcDbColumnName + ') LIKE ''%' + @SearchText + '%'''
					END
				END
				ELSE IF @vcListItemType = 'WI'
				BEGIN
					SET @strColumns = @strColumns + ', Case when isnull(OL.numSiteID,0)>0 then Sites.vcSiteName else WebAPI.vcProviderName end [' + @vcColumnName + ']'
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + '(Case when isnull(OL.numSiteID,0)>0 then Sites.vcSiteName else WebAPI.vcProviderName end) LIKE ''%' + @SearchText + '%'''
					END

					set @WhereCondition= @WhereCondition +' left JOIN OpportunityLinking OL on OL.numChildOppID=Opp.numOppId left join WebAPI on WebAPI.WebApiId=isnull(OL.numWebApiId,0)
													left join Sites on isnull(OL.numSiteID,0)=Sites.numSIteID and Sites.numDomainID=' + convert(varchar(15),@numDomainID)                                                       
				END 
            END
			ELSE IF @vcAssociatedControlType = 'DateField'
			BEGIN
				IF @Prefix ='OPR.'
				BEGIN
					SET @strColumns = @strColumns + ', (SELECT TOP 1 dbo.FormatedDateFromDate( DateAdd(minute,+'+convert(varchar(15),-@ClientTimeZoneOffset)+', dtRecurringDate)'+ ','+ CONVERT(VARCHAR(10),@numDomainId) +') FROM  dbo.OpportunityRecurring WHERE numOppID=OPP.numOppID AND ISNULL(numOppBizDocID,0)=0 and dtRecurringDate > dbo.GetUTCDateWithoutTime()) AS  ['+ @vcColumnName + ']'

					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + '(SELECT TOP 1 dbo.FormatedDateFromDate( DateAdd(minute,+'+convert(varchar(15),-@ClientTimeZoneOffset)+', dtRecurringDate)'+ ','+ CONVERT(VARCHAR(10),@numDomainId) +') FROM  dbo.OpportunityRecurring WHERE numOppID=OPP.numOppID AND ISNULL(numOppBizDocID,0)=0 and dtRecurringDate > dbo.GetUTCDateWithoutTime()) LIKE ''%' + @SearchText + '%'''
					END
				END
				ELSE IF @vcDbColumnName = 'dtReleaseDate'
				BEGIN
					SET @strColumns = @strColumns
                                + ',case when convert(varchar(11),'
                                + @Prefix
                                + @vcDbColumnName
                                + ')= convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',getdate())) then ''<b><font color=red>Today</font></b>'''
					SET @strColumns = @strColumns
									+ 'when convert(varchar(11),'
									+ @Prefix
									+ @vcDbColumnName
									+ ')= convert(varchar(11),dateadd(day,-1,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',getdate()))) then ''<b><font color=purple>Yesterday</font></b>'''
					SET @strColumns = @strColumns
									+ 'when convert(varchar(11),'
									+ @Prefix
									+ @vcDbColumnName
									+ ')= convert(varchar(11),dateadd(day,1,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',getdate()))) then''<b><font color=orange>Tommorow</font></b>'' '
					SET @strColumns = @strColumns
									+ 'else dbo.FormatedDateFromDate('
									+ @Prefix
									+ @vcDbColumnName
									+ ','
									+ CONVERT(VARCHAR(10),@numDomainId)
									+ ') end  ['
									+ @vcColumnName + ']' 	

					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + @Prefix + @vcDbColumnName + ' LIKE ''%' + @SearchText + '%'''
					END

				END
				ELSE
				BEGIN
					SET @strColumns = @strColumns
                                + ',case when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+','
                                + @Prefix
                                + @vcDbColumnName
                                + ') )= convert(varchar(11),getdate()) then ''<b><font color=red>Today</font></b>'''
					SET @strColumns = @strColumns
									+ 'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+','
									+ @Prefix
									+ @vcDbColumnName
									+ ') )= convert(varchar(11),dateadd(day,-1,getdate())) then ''<b><font color=purple>Yesterday</font></b>'''
					SET @strColumns = @strColumns
									+ 'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+','
									+ @Prefix
									+ @vcDbColumnName
									+ ') )= convert(varchar(11),dateadd(day,1,getdate())) then''<b><font color=orange>Tommorow</font></b>'' '
					SET @strColumns = @strColumns
									+ 'else dbo.FormatedDateFromDate('
									+ @Prefix
									+ @vcDbColumnName
									+ ','
									+ CONVERT(VARCHAR(10),@numDomainId)
									+ ') end  ['
									+ @vcColumnName + ']' 	

					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + @Prefix + @vcDbColumnName + ' LIKE ''%' + @SearchText + '%'''
					END
				 END	
            END
            ELSE
            IF @vcAssociatedControlType = 'TextBox'
            BEGIN
				IF @vcDbColumnName = 'vcCompactContactDetails'
				BEGIN
					SET @strColumns=@strColumns+ ' ,'''' AS vcCompactContactDetails'   
				END 
				ELSE
				BEGIN 
                SET @strColumns = @strColumns
                                  + ','
                                  + CASE 
                                      WHEN @vcDbColumnName = 'numAge' THEN 'year(getutcdate())-year(bintDOB)'
                                      WHEN @vcDbColumnName = 'monPAmount' THEN 'isnull((SELECT varCurrSymbol FROM [Currency] WHERE numCurrencyID = Opp.[numCurrencyID]),'''') + '' ''+ CONVERT(VARCHAR,CONVERT(DECIMAL(20,5),monPAmount),1)'
                                      WHEN @vcDbColumnName = 'numDivisionID' THEN 'Div.numDivisionID'
                                      WHEN @vcDbColumnName = 'tintOppStatus' THEN 'CASE tintOppStatus WHEN 0 then ''Open'' WHEN 1 THEN ''Closed-Won'' WHEN 2 THEN ''Closed-Lost'' END '
                                      WHEN @vcDbColumnName = 'tintOppType' THEN 'CASE tintOppType WHEN 1 then ''Sales'' WHEN 2 THEN ''Purchase'' Else ''NA'' END '
									  WHEN @vcDbColumnName = 'vcPOppName' THEN 'dbo.CheckChildRecord(Opp.numOppId,Opp.numDomainID)'
									  WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'vcBizDocsList' THEN 'ISNULL((SELECT SUBSTRING((SELECT ''$^$'' + CAST(numOppBizDocsId AS VARCHAR(18)) +''#^#''+ vcBizDocID
										FROM OpportunityBizDocs WHERE numOppId=Opp.numOppId AND 1 = (Case When bitAuthoritativeBizDocs=1 AND 1=' + (CASE WHEN @inttype=1 THEN '1' ELSE '0' END) + '  Then 0 ELSE 1 END) FOR XML PATH('''')),4,200000)),'''')'
                                      ELSE @Prefix + @vcDbColumnName
                                    END
                                  + ' ['
                                  + @vcColumnName + ']'
				END
				IF(@vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'vcBizDocsList')
				BEGIN
					SET @strColumns=@strColumns+','+' (SELECT COUNT(I.vcItemName) FROM 
													 OpportunityItems AS t LEFT JOIN  Item as I ON 
													 t.numItemCode=I.numItemCode WHERE 
													 t.numOppId=Opp.numOppId AND t.numoppItemtCode NOT IN 
													 (SELECT numOppItemId FROM OpportunityBizDocs JOIN 
													 OpportunityBizDocItems ON OpportunityBizDocs.numOppBizDocsId=OpportunityBizDocItems.numOppBizDocID  
													 WHERE OpportunityBizDocs.numOppId=Opp.numOppId 
													 AND 1 = (Case When bitAuthoritativeBizDocs=1 Or numBizDocID=293 Then 0 ELSE 1 END) 
													 AND ISNULL(bitAuthoritativeBizDocs,0)=1)) AS [List_Item_Approval_UNIT] '
				END
				
				IF LEN(@SearchText) > 0
				BEGIN
					SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + (CASE 
                                      WHEN @vcDbColumnName = 'numAge' THEN 'year(getutcdate())-year(bintDOB)'
                                      WHEN @vcDbColumnName = 'monPAmount' THEN 'isnull((SELECT varCurrSymbol FROM [Currency] WHERE numCurrencyID = Opp.[numCurrencyID]),'''') + '' ''+ CONVERT(VARCHAR,CONVERT(DECIMAL(20,5),monPAmount),1)'
                                      WHEN @vcDbColumnName = 'numDivisionID' THEN 'Div.numDivisionID'
                                      WHEN @vcDbColumnName = 'tintOppStatus' THEN 'CASE tintOppStatus WHEN 0 then ''Open'' WHEN 1 THEN ''Closed-Won'' WHEN 2 THEN ''Closed-Lost'' END '
                                      WHEN @vcDbColumnName = 'tintOppType' THEN 'CASE tintOppType WHEN 1 then ''Sales'' WHEN 2 THEN ''Purchase'' Else ''NA'' END '
									  WHEN @vcDbColumnName = 'vcPOppName' THEN 'dbo.CheckChildRecord(Opp.numOppId,Opp.numDomainID)'
									  WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'vcBizDocsList' THEN 'ISNULL((SELECT SUBSTRING((SELECT ''$^$'' + CAST(numOppBizDocsId AS VARCHAR(18)) +''#^#''+ vcBizDocID
										FROM OpportunityBizDocs WHERE numOppId=Opp.numOppId AND 1 = (Case When bitAuthoritativeBizDocs=1 Or numBizDocID=293 Then 0 ELSE 1 END)  FOR XML PATH('''')),4,200000)),'''')'
                                      ELSE @Prefix + @vcDbColumnName
                                    END) + ' LIKE ''%' + @SearchText + '%'''
				END
            END
            ELSE
            IF @vcAssociatedControlType = 'TextArea'
            BEGIN
				SET @strColumns=@strColumns+','+  @Prefix + @vcDbColumnName +' ['+ @vcColumnName+']' 

				IF LEN(@SearchText) > 0
				BEGIN
					SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + @Prefix + @vcDbColumnName + ' LIKE ''%' + @SearchText + '%'''
				END
            END
            ELSE IF  @vcAssociatedControlType='Label'                                              
			BEGIN
				SET @strColumns=@strColumns + ',' + CASE               
				WHEN @vcDbColumnName = 'vcTrackingDetail' THEN 'STUFF((SELECT 
																			CONCAT(''<br/>'',OpportunityBizDocs.vcBizDocID,'': '',vcTrackingDetail)
																		FROM 
																			ShippingReport 
																		INNER JOIN 
																			OpportunityBizDocs 
																		ON 
																			ShippingReport.numOppBizDocId=OpportunityBizDocs.numOppBizDocsId 
																		WHERE 
																			ShippingReport.numOppId=Opp.numOppID
																			AND ISNULL(ShippingReport.vcTrackingDetail,'''') <> ''''
																		FOR XML PATH(''''), TYPE).value(''(./text())[1]'',''varchar(max)''), 1, 5, '''')'     
				WHEN @vcDbColumnName = 'CalAmount' THEN 'Opp.monDealAmount'--'[dbo].[getdealamount](Opp.numOppId,Getutcdate(),0)'
				WHEN @vcDbColumnName = 'vcInventoryStatus' THEN 'ISNULL(dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID),'''')'
				WHEN @vcDbColumnName = 'vcOrderedShipped' THEN CONCAT('dbo.GetOrderShipReceiveStatus(Opp.numOppId,1,Opp.tintshipped,',@tintDecimalPoints,')')
				WHEN @vcDbColumnName = 'vcOrderedReceived' THEN CONCAT('dbo.GetOrderShipReceiveStatus(Opp.numOppId,2,Opp.tintshipped,',@tintDecimalPoints,')')
				WHEN @vcDbColumnName = 'vcShipStreet' THEN 'dbo.fn_getOPPState(Opp.numOppId,Opp.numDomainID,2)'
				WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'monDealAmount' THEN 'ISNULL((SELECT SUM(ISNULL(monDealAmount,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)'
				WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'monAmountPaid' THEN 'ISNULL((SELECT SUM(ISNULL(monAmountPaid,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)'
				ELSE @Prefix + @vcDbColumnName END +' ['+ @vcColumnName+']'   
				
				IF @vcDbColumnName='vcOrderedShipped'
				BEGIN
				DECLARE @temp VARCHAR(MAX)
				SET @temp = '(SELECT 
				STUFF((SELECT '', '' + CAST((CAST(numOppBizDocsId AS varchar)+''~''+CAST(ISNULL(numShippingReportId,0) AS varchar) ) AS VARCHAR(MAX)) [text()]
				FROM OpportunityBizDocs
				LEFT JOIN ShippingReport
				ON ShippingReport.numOppBizDocId=OpportunityBizDocs.numOppBizDocsId
				WHERE OpportunityBizDocs.numOppId=Opp.numOppId  AND OpportunityBizDocs.bitShippingGenerated=1  FOR XML PATH(''''), TYPE)
						.value(''.'',''NVARCHAR(MAX)''),1,2,'' ''))'
					SET @strColumns = @strColumns + ' , (SELECT SUBSTRING('+ @temp +', 2, 200000))AS ShippingIcons '
					SET @strColumns = @strColumns + ' , CASE WHEN (SELECT COUNT(*) FROM ShippingBox INNER JOIN ShippingReport ON ShippingBox.numShippingReportId = ShippingReport.numShippingReportId WHERE ShippingReport.numOppID = Opp.numOppID AND LEN(vcTrackingNumber) > 0) > 0 THEN 1 ELSE 0 END [ISTrackingNumGenerated]'
				END

				IF LEN(@SearchText) > 0
				BEGIN
					SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + (CASE                    
					WHEN @vcDbColumnName = 'CalAmount' THEN 'Opp.monDealAmount'
					WHEN @vcDbColumnName = 'vcInventoryStatus' THEN 'ISNULL(dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID),'''')'
					WHEN @vcDbColumnName = 'vcOrderedShipped' THEN CONCAT('dbo.GetOrderShipReceiveStatus(Opp.numOppId,1,Opp.tintshipped,',@tintDecimalPoints,')')
					WHEN @vcDbColumnName = 'vcOrderedReceived' THEN CONCAT('dbo.GetOrderShipReceiveStatus(Opp.numOppId,2,Opp.tintshipped,',@tintDecimalPoints,')')
					WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'monDealAmount' THEN 'ISNULL((SELECT SUM(ISNULL(monDealAmount,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)'
					WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'monAmountPaid' THEN 'ISNULL((SELECT SUM(ISNULL(monAmountPaid,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)'
					else @Prefix + @vcDbColumnName END) + ' LIKE ''%' + @SearchText + '%'''
				END    
			END
            ELSE IF @vcAssociatedControlType='Popup' and @vcDbColumnName='numShareWith'                                                   
			BEGIN
				SET @strColumns = @strColumns + ',(SELECT SUBSTRING(
								(SELECT '','' + A.vcFirstName+'' ''+A.vcLastName + CASE WHEN ISNULL(SR.numContactType,0)>0 THEN ''('' + dbo.fn_GetListItemName(SR.numContactType) + '')'' ELSE '''' END
								FROM ShareRecord SR JOIN UserMaster UM ON UM.numUserDetailId=SR.numAssignedTo 
								JOIN AdditionalContactsInformation A ON UM.numUserDetailId=A.numContactID 
								WHERE SR.numDomainID=Opp.numDomainID AND SR.numModuleID=3 AND SR.numRecordID=Opp.numOppId
								AND UM.numDomainID=Opp.numDomainID and UM.numDomainID=A.numDomainID FOR XML PATH('''')),2,200000)) ['+ @vcColumnName+']'                
			END 
        END
		ELSE
        BEGIN
			SET @vcColumnName= @vcDbColumnName +'~'+ convert(varchar(10),@numFieldId)+'~1'

			IF @vcAssociatedControlType = 'TextBox' or @vcAssociatedControlType = 'TextArea'
            BEGIN
              SET @strColumns = @strColumns
                              + ',CFW'
                              + CONVERT(VARCHAR(3),@tintOrder)
                              + '.Fld_Value  ['
                              + @vcColumnName + ']'
              SET @WhereCondition = @WhereCondition
                                      + ' left Join CFW_FLD_Values_Opp CFW'
                                      + CONVERT(VARCHAR(3),@tintOrder)
                                      + ' on CFW'
                                      + CONVERT(VARCHAR(3),@tintOrder)
                                      + '.Fld_Id='
                                      + CONVERT(VARCHAR(10),@numFieldId)
                                      + 'and CFW'
                                      + CONVERT(VARCHAR(3),@tintOrder)
                                      + '.RecId=Opp.numOppid   '
			END
			ELSE
            IF @vcAssociatedControlType = 'CheckBox' 
            BEGIN
                SET @strColumns = @strColumns
                                + ',case when isnull(CFW'
                                + CONVERT(VARCHAR(3),@tintOrder)
                                + '.Fld_Value,0)=0 then 0 when isnull(CFW'
                                + CONVERT(VARCHAR(3),@tintOrder)
                                + '.Fld_Value,0)=1 then 1 end   ['
                                + @vcColumnName + ']'
                SET @WhereCondition = @WhereCondition
                                        + ' left Join CFW_FLD_Values_Opp CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + ' on CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.Fld_Id='
                                        + CONVERT(VARCHAR(10),@numFieldId)
                                        + 'and CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.RecId=Opp.numOppid   '
            END
            ELSE
            IF @vcAssociatedControlType = 'DateField'
            BEGIN
                  SET @strColumns = @strColumns
                                  + ',dbo.FormatedDateFromDate(CFW'
                                  + CONVERT(VARCHAR(3),@tintOrder)
                                  + '.Fld_Value,'
                                  + CONVERT(VARCHAR(10),@numDomainId)
                                  + ')  ['
                                  + @vcColumnName + ']'
                  SET @WhereCondition = @WhereCondition
                                          + ' left Join CFW_FLD_Values_Opp CFW'
                                          + CONVERT(VARCHAR(3),@tintOrder)
                                          + ' on CFW'
                                          + CONVERT(VARCHAR(3),@tintOrder)
                                          + '.Fld_Id='
                                          + CONVERT(VARCHAR(10),@numFieldId)
                                          + 'and CFW'
                                          + CONVERT(VARCHAR(3),@tintOrder)
                                          + '.RecId=Opp.numOppid    '
            END
            ELSE
            IF @vcAssociatedControlType = 'SelectBox'
            BEGIN
                SET @vcDbColumnName = 'DCust'
                                            + CONVERT(VARCHAR(10),@numFieldId)
                SET @strColumns = @strColumns
                                + ',L'
                                + CONVERT(VARCHAR(3),@tintOrder)
                                + '.vcData'
                                + ' ['
                                + @vcColumnName + ']'

                SET @WhereCondition = @WhereCondition
                                        + ' left Join CFW_FLD_Values_Opp CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + ' on CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.Fld_Id='
                                        + CONVERT(VARCHAR(10),@numFieldId)
                                        + 'and CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.RecId=Opp.numOppid '

                SET @WhereCondition = @WhereCondition
                                        + ' left Join ListDetails L'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + ' on L'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.numListItemID=CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.Fld_Value'
            END
			ELSE
			BEGIN
				SET @strColumns = @strColumns + CONCAT(',dbo.GetCustFldValueOpp(',@numFieldId,',opp.numOppID)') + ' [' + @vcColumnName + ']'
			END

			IF LEN(@SearchText) > 0
			BEGIN
				SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + CONCAT('dbo.GetCustFldValueOpp(',@numFieldId,',opp.numOppID) LIKE ''%',@SearchText,'%''')
			END
		END
      
     
		SELECT TOP 1 
			@tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName,@vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,
			@numListID=numListID,@vcLookBackTableName=vcLookBackTableName,@bitCustom=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,@bitAllowEdit=bitAllowEdit,
			@ListRelID=ListRelID
		FROM 
			#tempForm 
		WHERE 
			tintOrder > @tintOrder-1 
		ORDER BY 
			tintOrder asc            
 
		IF @@rowcount=0 SET @tintOrder=0 
	END 

	

	DECLARE @strShareRedordWith AS VARCHAR(300);
	SET @strShareRedordWith=' Opp.numOppId in (select SR.numRecordID FROM ShareRecord SR WHERE SR.numDomainID='+ convert(varchar(15),@numDomainID )+' AND SR.numModuleID=3 AND SR.numAssignedTo='+convert(varchar(15),@numUserCntId)+') '


	DECLARE @StrSql AS VARCHAR(MAX) = ''

	SET @StrSql = @StrSql + ' FROM OpportunityMaster Opp                                               
                                 LEFT JOIN AdditionalContactsInformation ADC ON Opp.numContactId = ADC.numContactId                                                               
                                 INNER JOIN DivisionMaster Div ON Opp.numDivisionId = Div.numDivisionID 
								 INNER JOIN CompanyInfo cmp ON Div.numCompanyID = cmp.numCompanyId 
								 LEFT JOIN ProjectProgress PP On PP.numOppId = Opp.numOppId ' + @WhereCondition
								 --INNER JOIN Correspondence Corr ON opp.numOppId = Corr.numOpenRecordID 
	-------Change Row Color-------
	
	DECLARE @vcCSOrigDbCOlumnName AS VARCHAR(50) = ''
	DECLARE @vcCSLookBackTableName AS VARCHAR(50) = ''
	DECLARE @vcCSAssociatedControlType AS VARCHAR(50)

	CREATE TABLE #tempColorScheme
	(
		vcOrigDbCOlumnName VARCHAR(50),
		vcLookBackTableName VARCHAR(50),
		vcFieldValue VARCHAR(50),
		vcFieldValue1 VARCHAR(50),
		vcColorScheme VARCHAR(50),
		vcAssociatedControlType varchar(50)
	)

	INSERT INTO 
		#tempColorScheme 
	SELECT 
		DFM.vcOrigDbCOlumnName,
		DFM.vcLookBackTableName,
		DFCS.vcFieldValue,
		DFCS.vcFieldValue1,
		DFCS.vcColorScheme,
		DFFM.vcAssociatedControlType 
	FROM 
		DycFieldColorScheme DFCS 
	JOIN 
		DycFormField_Mapping DFFM 
	ON 
		DFCS.numFieldID=DFFM.numFieldID
	JOIN 
		DycFieldMaster DFM 
	ON 
		DFM.numModuleID=DFFM.numModuleID and DFM.numFieldID=DFFM.numFieldID
	WHERE
		DFCS.numDomainID=@numDomainID 
		AND DFFM.numFormID=@numFormId 
		AND DFCS.numFormID=@numFormId 
		AND isnull(DFFM.bitAllowGridColor,0)=1

	IF(SELECT COUNT(*) FROM #tempColorScheme)>0
	BEGIN
		SELECT TOP 1 @vcCSOrigDbCOlumnName=vcOrigDbCOlumnName,@vcCSLookBackTableName=vcLookBackTableName,@vcCSAssociatedControlType=vcAssociatedControlType from #tempColorScheme
	END                        

	IF len(@vcCSOrigDbCOlumnName)>0 and len(@vcCSLookBackTableName)>0
	BEGIN
		SET @strColumns=@strColumns + ',tCS.vcColorScheme'
	END

	IF len(@vcCSOrigDbCOlumnName)>0 and len(@vcCSLookBackTableName)>0
	BEGIN
		IF @vcCSLookBackTableName = 'AdditionalContactsInformation'                  
			SET @Prefix = 'ADC.'                  
		IF @vcCSLookBackTableName = 'DivisionMaster'                  
			SET @Prefix = 'Div.'
		if @vcCSLookBackTableName = 'CompanyInfo'                  
			SET @Prefix = 'CMP.'   
		if @vcCSLookBackTableName = 'OpportunityMaster'                  
			SET @Prefix = 'Opp.'   
 
		IF @vcCSAssociatedControlType='DateField'
		BEGIN
			SET @strSql=@strSql+' LEFT JOIN #tempColorScheme tCS ON CONVERT(VARCHAR(10),' + @Prefix + @vcCSOrigDbCOlumnName + ',111) >= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue as int),GETUTCDATE()),111)
				 and CONVERT(VARCHAR(10),' + @Prefix + @vcCSOrigDbCOlumnName + ',111) <= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue1 as int),GETUTCDATE()),111)'
		END
		ELSE IF @vcCSAssociatedControlType='SelectBox' OR @vcCSAssociatedControlType='ListBox' OR @vcCSAssociatedControlType='CheckBox'
		BEGIN
			SET @strSql=@strSql+' LEFT JOIN #tempColorScheme tCS on tCS.vcFieldValue=' + @Prefix + @vcCSOrigDbCOlumnName
		END
	END
                          
	IF @columnName like 'CFW.Cust%'             
	BEGIN            
		SET @strSql = @strSql + ' LEFT JOIN CFW_Fld_Values_Opp CFW on CFW.RecId=Opp.numOppid and CFW.fld_id= '+ REPLACE(@columnName,'CFW.Cust','') +' '                                                     
		SET @columnName='CFW.Fld_Value'            
	END 

	IF @bitPartner = 1
		SET @strSql = @strSql + ' LEFT JOIN OpportunityContact OppCont ON OppCont.numOppId=Opp.numOppId AND OppCont.bitPartner=1 AND OppCont.numContactId=' + CONVERT(VARCHAR(15),@numUserCntID)
                    
                
	IF @tintFilterBy = 1 --Partially Fulfilled Orders 
		SET @strSql = @strSql + ' LEFT JOIN AdditionalContactsInformation ADC1 ON ADC1.numContactId=Opp.numRecOwner                                                               
                             LEFT OUTER JOIN [OpportunityBizDocs] OB ON OB.[numOppId] = Opp.[numOppId] 
                             WHERE Opp.numDomainID=' + CONVERT(VARCHAR(15),@numDomainID) + ' and Opp.tintOppstatus=1 and (Opp.tintShipped=' + CONVERT(VARCHAR(15),@tintShipped) + ' OR ' + CONVERT(VARCHAR(15),@tintShipped) + '=2)'
	ELSE
		SET @strSql = @strSql + ' LEFT JOIN AdditionalContactsInformation ADC1 on ADC1.numContactId=Opp.numRecOwner                                                               
                             WHERE Opp.numDomainID=' + CONVERT(VARCHAR(15),@numDomainID) + ' and Opp.tintOppstatus=1 and (Opp.tintShipped='+ CONVERT(VARCHAR(15),@tintShipped)  + ' OR ' + CONVERT(VARCHAR(15),@tintShipped) + '=2)'
  
  
	IF @intOrder <> 0
    BEGIN
      IF @intOrder = 1
        SET @strSql = @strSql + ' and Opp.bitOrder = 0 '
      IF @intOrder = 2
        SET @strSql = @strSql + ' and Opp.bitOrder = 1'
    END
    
	IF @numCompanyID <> 0
		SET @strSql = @strSql + ' And Div.numCompanyID ='+ CONVERT(VARCHAR(15),@numCompanyID)

	IF @SortChar <> '0' 
		SET @strSql = @strSql + ' And Opp.vcPOppName like ''' + @SortChar + '%'''
  

	IF @inttype = 3
	BEGIN
		IF @tintSalesUserRightType = 0
			SET @strSql = @strSql + ' AND opp.tintOppType=0'
		ELSE IF @tintSalesUserRightType = 1
			SET @strSql = @strSql + ' AND (Opp.numRecOwner= ' + CONVERT(VARCHAR(15),@numUserCntID)
						+ ' or Opp.numAssignedTo= '+ CONVERT(VARCHAR(15),@numUserCntID)
						+ CASE WHEN @bitPartner = 1 THEN ' or ( OppCont.bitPartner=1 and OppCont.numContactId='+ CONVERT(VARCHAR(15),@numUserCntID) + ')' ELSE '' END
						+ ' or ' + @strShareRedordWith +')'
		ELSE IF @tintSalesUserRightType = 2
			SET @strSql = @strSql + ' AND (Div.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+ CONVERT(VARCHAR(15),@numUserCntID) + ' ) 
									 or Opp.numAssignedTo= '+ CONVERT(VARCHAR(15),@numUserCntID)
									+ ' or ' + @strShareRedordWith +')'
	END
	ELSE IF @inttype = 4
	BEGIN
		IF @tintPurchaseUserRightType = 0
			SET @strSql = @strSql + ' AND opp.tintOppType=0'
		ELSE IF @tintPurchaseUserRightType = 1
			SET @strSql = @strSql + ' AND (Opp.numRecOwner= ' + CONVERT(VARCHAR(15),@numUserCntID)
						+ ' or Opp.numAssignedTo= '+ CONVERT(VARCHAR(15),@numUserCntID)
						+ CASE WHEN @bitPartner = 1 THEN ' or ( OppCont.bitPartner=1 and OppCont.numContactId='+ CONVERT(VARCHAR(15),@numUserCntID) + ')' ELSE '' END
						+ ' or ' + @strShareRedordWith +')'
		ELSE IF @tintPurchaseUserRightType = 2
			SET @strSql = @strSql + ' AND (Div.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+ CONVERT(VARCHAR(15),@numUserCntID) + ' ) 
									 or Opp.numAssignedTo= '+ CONVERT(VARCHAR(15),@numUserCntID)
									+ ' or ' + @strShareRedordWith +')'
	END

	
					
	IF @tintSortOrder <> '0'
		SET @strSql = @strSql + '  AND Opp.tintOppType= '+ CONVERT(VARCHAR(1),@tintSortOrder)
    
    
	IF @tintFilterBy = 1 --Partially Fulfilled Orders
		SET @strSql = @strSql + ' AND OB.bitPartialFulfilment = 1 AND OB.bitAuthoritativeBizDocs = 1 '
	ELSE IF @tintFilterBy = 2 --Fulfilled Orders
		SET @strSql = @strSql + ' AND Opp.[numOppId] IN (SELECT OM.numOppId FROM [OpportunityMaster] OM
                      			INNER JOIN [OpportunityBizDocs] OB ON OB.[numOppId] = OM.[numOppId]
                      			INNER JOIN [OpportunityItems] OI ON OM.[numOppId] = OI.[numOppId]
                      			INNER JOIN [OpportunityBizDocItems] BI ON BI.[numOppBizDocID] = OB.[numOppBizDocsId]
                      			Where OM.numDomainId=' + CONVERT(VARCHAR(15),@numDomainId) + 'AND OB.[bitAuthoritativeBizDocs] = 1 GROUP BY OM.[numOppId]
                      			HAVING   COUNT(OI.[numUnitHour]) = COUNT(BI.[numUnitHour])) '
	ELSE IF @tintFilterBy = 3 --Orders without Authoritative-BizDocs
		SET @strSql = @strSql + ' AND Opp.[numOppId] not IN (SELECT DISTINCT OM.[numOppId]
                      		  FROM [OpportunityMaster] OM JOIN [OpportunityBizDocs] OB ON OM.[numOppId] = OB.[numOppId]
                      		  Where OM.numDomainId=' + CONVERT(VARCHAR(15),@numDomainId) + ' AND OM.tintOppstatus=1 AND (OM.tintShipped=' + CONVERT(VARCHAR(15),@tintShipped) + ' OR ' + CONVERT(VARCHAR(15),@tintShipped) + '=2)' + ' AND OM.tintOppType= '+ CONVERT(VARCHAR(1),@tintSortOrder) + ' AND isnull(OB.[bitAuthoritativeBizDocs],0)=1) '
	ELSE IF @tintFilterBy = 4 --Orders with items yet to be added to Invoices
		SET @strSql = @strSql + ' AND Opp.[numOppId] IN (SELECT distinct OPPM.numOppId FROM [OpportunityMaster] OPPM
                                  INNER JOIN [OpportunityItems] OPPI ON OPPM.[numOppId] = OPPI.[numOppId]
                                  Where OPPM.numDomainId=' + CONVERT(VARCHAR(15),@numDomainId) + ' 
								  and OPPI.numoppitemtCode not in (select numOppItemID from [OpportunityBizDocs] OB INNER JOIN [OpportunityBizDocItems] BI
                      			  ON BI.[numOppBizDocID] = OB.[numOppBizDocsId] where OB.[numOppId] = OPPM.[numOppId]))'      
	ELSE IF @tintFilterBy = 6 --Orders with deferred Income/Invoices
		SET @strSql = @strSql + ' AND Opp.[numOppId] IN (SELECT DISTINCT OM.[numOppId]
                      		 FROM [OpportunityMaster] OM LEFT OUTER JOIN [OpportunityBizDocs] OB ON OM.[numOppId] = OB.[numOppId]
                      		 Where OM.numDomainId=' + CONVERT(VARCHAR(15),@numDomainId) + ' AND OB.[bitAuthoritativeBizDocs] =1 and OB.tintDeferred=1)'

	IF @numOrderStatus <>0 
		SET @strSql = @strSql + ' AND Opp.numStatus = ' + CONVERT(VARCHAR(15),@numOrderStatus);

	IF CHARINDEX('OI.vcInventoryStatus',@vcRegularSearchCriteria) > 0
	BEGIN
		IF CHARINDEX('OI.vcInventoryStatus=1',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'OI.vcInventoryStatus=1',' CHARINDEX(''Not Applicable'', dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID)) > 0 ')
		IF CHARINDEX('OI.vcInventoryStatus=2',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'OI.vcInventoryStatus=2',' CHARINDEX(''Shipped'', dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID)) > 0 ')
		IF CHARINDEX('OI.vcInventoryStatus=3',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'OI.vcInventoryStatus=3',' CHARINDEX(''BO'', dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID)) > 0 AND CHARINDEX(''Shippable'', dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID)) = 0 ')
		IF CHARINDEX('OI.vcInventoryStatus=4',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'OI.vcInventoryStatus=4',' CHARINDEX(''Shippable'', dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID)) > 0 ')
	END
	
	IF CHARINDEX('Opp.tintInvoicing',@vcRegularSearchCriteria) > 0
	BEGIN
		IF CHARINDEX('Opp.tintInvoicing=0',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.tintInvoicing=0',' CHARINDEX(''All'', dbo.CheckOrderInvoicingStatus(Opp.numOppID,Opp.numDomainID)) > 0 ')
		IF CHARINDEX('Opp.tintInvoicing=1',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.tintInvoicing=1',' (SELECT (CASE WHEN SUM(ISNULL(InvoicedQty,0)) = SUM(ISNULL(OrderedQty,0)) THEN 1 ELSE 0 END)
	FROM 
	(
		SELECT OI.numoppitemtCode, ISNULL(OI.numUnitHour,0) AS OrderedQty, ISNULL(TempInvoice.InvoicedQty,0) AS InvoicedQty
		FROM OpportunityItems OI
		INNER JOIN Item I ON OI.numItemCode = I.numItemCode
		OUTER APPLY
		(
			SELECT SUM(OpportunityBizDocItems.numUnitHour) AS InvoicedQty
			FROM OpportunityBizDocs
				INNER JOIN OpportunityBizDocItems ON OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
			WHERE OpportunityBizDocs.numOppId = Opp.numOppID
				AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
				AND numBizDocId IN (SELECT numAuthoritativeSales FROM AuthoritativeBizDocs WHERE numDomainId = Opp.numDomainID)
		) AS TempInvoice
		WHERE
		OI.numOppID = Opp.numOppID
	) X) > 0 ')
		IF CHARINDEX('Opp.tintInvoicing=2',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.tintInvoicing=2',' (SELECT COUNT(*) 
		FROM OpportunityBizDocs 
		WHERE numOppId = Opp.numOppID 
			AND numBizDocId IN (SELECT numAuthoritativeSales FROM AuthoritativeBizDocs WHERE numDomainId = Opp.numDomainID)) = 0 ')
		IF CHARINDEX('Opp.tintInvoicing=3',@vcRegularSearchCriteria) > 0 
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.tintInvoicing=3',' (SELECT (CASE WHEN SUM(ISNULL(InvoicedQty,0)) > 0 THEN SUM(ISNULL(OrderedQty,0)) - SUM(ISNULL(InvoicedQty,0)) ELSE 0 END)
	FROM 
	(
		SELECT OI.numoppitemtCode, ISNULL(OI.numUnitHour,0) AS OrderedQty, ISNULL(TempInvoice.InvoicedQty,0) AS InvoicedQty
		FROM OpportunityItems OI
		INNER JOIN Item I ON OI.numItemCode = I.numItemCode
		OUTER APPLY
		(
			SELECT SUM(OpportunityBizDocItems.numUnitHour) AS InvoicedQty
			FROM OpportunityBizDocs
				INNER JOIN OpportunityBizDocItems ON OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
			WHERE OpportunityBizDocs.numOppId = Opp.numOppID
				AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
				AND numBizDocId IN (SELECT numAuthoritativeSales FROM AuthoritativeBizDocs WHERE numDomainId = Opp.numDomainID)
		) AS TempInvoice
		WHERE
		OI.numOppID = Opp.numOppID
	) X) > 0 ')
		
		IF CHARINDEX('Opp.tintInvoicing=4',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.tintInvoicing=4',' (SELECT (CASE WHEN SUM(ISNULL(InvoicedQty,0)) > 0 THEN SUM(ISNULL(OrderedQty,0)) - SUM(ISNULL(InvoicedQty,0)) ELSE 0 END)
	FROM 
	(
		SELECT OI.numoppitemtCode, ISNULL(OI.numUnitHour,0) AS OrderedQty, ISNULL(TempInvoice.InvoicedQty,0) AS InvoicedQty
		FROM OpportunityItems OI
		INNER JOIN Item I ON OI.numItemCode = I.numItemCode
		OUTER APPLY
		(
			SELECT SUM(OpportunityBizDocItems.numUnitHour) AS InvoicedQty
			FROM OpportunityBizDocs
				INNER JOIN OpportunityBizDocItems ON OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
			WHERE OpportunityBizDocs.numOppId = Opp.numOppID
				AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
				AND numBizDocId IN (SELECT numAuthoritativeSales FROM AuthoritativeBizDocs WHERE numDomainId = Opp.numDomainID)
		) AS TempInvoice
		WHERE
		OI.numOppID = Opp.numOppID
	) X) > 0 ')

		IF CHARINDEX('Opp.tintInvoicing=5',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.tintInvoicing=5',' 
				Opp.[numOppId] IN (SELECT DISTINCT OM.[numOppId]
                      		 FROM [OpportunityMaster] OM INNER JOIN [OpportunityBizDocs] OB ON OM.[numOppId] = OB.[numOppId]
                      		 Where OM.numDomainId = Opp.numDomainID AND OB.[bitAuthoritativeBizDocs] = 1 and OB.tintDeferred=1) ')
	END
	
	IF CHARINDEX('Opp.tintEDIStatus',@vcRegularSearchCriteria) > 0
	BEGIN
		--IF CHARINDEX('Opp.tintEDIStatus=1',@vcRegularSearchCriteria) > 0
		--	SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.tintEDIStatus=1',' 1 = (SELECT CASE WHEN Opp.tintEDIStatus = 1 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.tintEDIStatus=2',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.tintEDIStatus=2',' 1 = (SELECT CASE WHEN Opp.tintEDIStatus = 2 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.tintEDIStatus=3',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.tintEDIStatus=3',' 1 = (SELECT CASE WHEN Opp.tintEDIStatus = 3 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.tintEDIStatus=4',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.tintEDIStatus=4',' 1 = (SELECT CASE WHEN Opp.tintEDIStatus = 4 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.tintEDIStatus=5',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.tintEDIStatus=5',' 1 = (SELECT CASE WHEN Opp.tintEDIStatus = 5 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.tintEDIStatus=6',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.tintEDIStatus=6',' 1 = (SELECT CASE WHEN Opp.tintEDIStatus = 6 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.tintEDIStatus=7',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.tintEDIStatus=7',' 1 = (SELECT CASE WHEN Opp.tintEDIStatus = 7 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.tintEDIStatus=7',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.tintEDIStatus=11',' 1 = (SELECT CASE WHEN Opp.tintEDIStatus = 11 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.tintEDIStatus=7',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.tintEDIStatus=12',' 1 = (SELECT CASE WHEN Opp.tintEDIStatus = 12 THEN 1 ELSE 0 END) ')
	END

	IF CHARINDEX('Opp.vcSignatureType',@vcRegularSearchCriteria) > 0
	BEGIN
		IF CHARINDEX('Opp.vcSignatureType=0',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.vcSignatureType=0',' 1 = (SELECT CASE WHEN (SELECT COUNT(*) FROM DivisionMaster AS D 
																			LEFT JOIN DivisionMasterShippingConfiguration AS DMSC ON DMSC.numDivisionID=D.numDivisionID 
																			WHERE D.numDivisionID=Div.numDivisionID AND vcSignatureType=0 ) > 0 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.vcSignatureType=1',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.vcSignatureType=1','  1 = (SELECT CASE WHEN (SELECT COUNT(*) FROM DivisionMaster AS D 
																			LEFT JOIN DivisionMasterShippingConfiguration AS DMSC ON DMSC.numDivisionID=D.numDivisionID 
																			WHERE D.numDivisionID=Div.numDivisionID AND vcSignatureType=1 ) > 0 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.vcSignatureType=2',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.vcSignatureType=2',' 1 = (SELECT CASE WHEN (SELECT COUNT(*) FROM DivisionMaster AS D 
																			LEFT JOIN DivisionMasterShippingConfiguration AS DMSC ON DMSC.numDivisionID=D.numDivisionID 
																			WHERE D.numDivisionID=Div.numDivisionID AND vcSignatureType=2 ) > 0 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.vcSignatureType=3',@vcRegularSearchCriteria) > 0								   
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.vcSignatureType=3',' 1 = (SELECT CASE WHEN (SELECT COUNT(*) FROM DivisionMaster AS D 
																			LEFT JOIN DivisionMasterShippingConfiguration AS DMSC ON DMSC.numDivisionID=D.numDivisionID 
																			WHERE D.numDivisionID=Div.numDivisionID AND vcSignatureType=3 ) > 0 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.vcSignatureType=4',@vcRegularSearchCriteria) > 0								   
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.vcSignatureType=4',' 1 = (SELECT CASE WHEN (SELECT COUNT(*) FROM DivisionMaster AS D 
																			LEFT JOIN DivisionMasterShippingConfiguration AS DMSC ON DMSC.numDivisionID=D.numDivisionID 
																			WHERE D.numDivisionID=Div.numDivisionID AND vcSignatureType=4 ) > 0 THEN 1 ELSE 0 END) ')
	END

	IF CHARINDEX('OBD.monAmountPaid',@vcRegularSearchCriteria) > 0
	BEGIN
		IF CHARINDEX('OBD.monAmountPaid like ''%0%''',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'OBD.monAmountPaid like ''%0%''',' 1 = (SELECT ISNULL((SELECT SUM(ISNULL(monAmountPaid,0)) FROM  OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)) ')
		IF CHARINDEX('OBD.monAmountPaid like ''%1%''',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'OBD.monAmountPaid like ''%1%''','  1 = (SELECT (CASE WHEN (SELECT (CASE WHEN (SUM(ISNULL(monDealAmount,0)) = SUM(ISNULL(monAmountPaid,0)) AND SUM(ISNULL(monAmountPaid,0)) <> 0) THEN 1 ELSE 0 END) FROM OpportunityBizDocs BD WHERE BD.numOppId =  Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0) = 1) > 0 THEN 1 ELSE 0 END))  ')
		IF CHARINDEX('OBD.monAmountPaid like ''%2%''',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'OBD.monAmountPaid like ''%2%''',' 1 = (SELECT CASE WHEN (SELECT (CASE WHEN ((SUM(ISNULL(monDealAmount,0)) - SUM(ISNULL(monAmountPaid,0))) > 0 AND SUM(ISNULL(monAmountPaid,0)) <> 0) THEN 1 ELSE 0 END) FROM OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0) = 1) > 0 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('OBD.monAmountPaid like ''%3%''',@vcRegularSearchCriteria) > 0								   
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'OBD.monAmountPaid like ''%3%''',' 1 = (SELECT CASE WHEN (SELECT (CASE WHEN (SUM(ISNULL(monAmountPaid,0)) = 0) THEN 1 ELSE 0 END) FROM OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0) = 1) > 0 THEN 1 ELSE 0 END) ')
	END
	
	
	IF @vcRegularSearchCriteria<>'' 
	BEGIN
		SET @strSql=@strSql +' AND ' + @vcRegularSearchCriteria 
	END
	
	


	IF @vcCustomSearchCriteria<>'' 
	BEGIN
		SET @strSql = @strSql +' AND ' +  @vcCustomSearchCriteria
	END

	IF LEN(@SearchQuery) > 0
	BEGIN
		SET @strSql = @strSql + ' AND (' + @SearchQuery + ') '
	END

	IF ISNULL(@tintDashboardReminderType,0) = 19
	BEGIN
		SET @strSql = CONCAT(@strSql,' AND Opp.numOppID IN (','SELECT DISTINCT
																	OMInner.numOppID
																FROM 
																	StagePercentageDetails SPDInner
																INNER JOIN 
																	OpportunityMaster OMInner
																ON 
																	SPDInner.numOppID=OMInner.numOppId 
																WHERE 
																	SPDInner.numDomainId=',@numDOmainID,'
																	AND OMInner.numDomainId=',@numDOmainID,' 
																	AND OMInner.tintOppType = 1
																	AND ISNULL(OMInner.tintOppStatus,0) = 1
																	AND tinProgressPercentage <> 100',') ')

	END
	ELSE IF ISNULL(@tintDashboardReminderType,0) = 10
	BEGIN
		SET @strSql = CONCAT(@strSql,' AND Opp.numOppID IN (','SELECT DISTINCT
																	OMInner.numOppId
																FROM
																	OpportunityMaster OMInner
																INNER JOIN
																	OpportunityItems OIInner
																ON
																	OIInner.numOppId = OMInner.numOppID
																INNER JOIN
																	Item IInner
																ON
																	OIInner.numItemCode = IInner.numItemCode
																OUTER APPLY
																(
																	SELECT
																		SUM(OpportunityBizDocItems.numUnitHour) AS BilledQty
																	FROM
																		OpportunityBizDocs
																	INNER JOIN
																		OpportunityBizDocItems 
																	ON
																		OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																	WHERE
																		OpportunityBizDocs.numOppId = OMInner.numOppID
																		AND OpportunityBizDocItems.numOppItemID = OIInner.numoppitemtCode
																		AND OpportunityBizDocs.numBizDocId = (SELECT ISNULL(numAuthoritativePurchase,0) FROM AuthoritativeBizDocs WHERE numDomainId=',@numDomainID,')
																) AS TempBilled
																WHERE
																	OMInner.numDomainId = ',@numDomainID,'
																	AND ISNULL(OMInner.bitStockTransfer,0) = 0
																	AND OMInner.tintOppType = 2
																	AND OMInner.tintOppStatus  = 1
																	AND ISNULL(OIInner.numUnitHour,0) <> ISNULL(TempBilled.BilledQty,0)',') ')
	END
	ELSE IF ISNULL(@tintDashboardReminderType,0) = 11
	BEGIN
		SET @strSql = CONCAT(@strSql,' AND Opp.numOppID IN (','SELECT DISTINCT
																	OMInner.numOppId
																FROM
																	OpportunityMaster OMInner
																INNER JOIN
																	OpportunityItems OIInner
																ON
																	OIInner.numOppId = OMInner.numOppId
																INNER JOIN
																	Item IInner
																ON
																	OIInner.numItemCode = IInner.numItemCode
																OUTER APPLY
																(
																	SELECT
																		SUM(OpportunityBizDocItems.numUnitHour) AS PickPacked
																	FROM
																		OpportunityBizDocs
																	INNER JOIN
																		OpportunityBizDocItems 
																	ON
																		OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																	WHERE
																		OpportunityBizDocs.numOppId = OMInner.numOppId
																		AND ISNULL(OpportunityBizDocs.numBizDocId,0) = ',(SELECT ISNULL(numDefaultSalesShippingDoc,0) FROM Domain WHERE numDomainId=@numDomainID),'
																		AND OpportunityBizDocItems.numOppItemID = OIInner.numoppitemtCode
																) AS TempFulFilled
																WHERE
																	OMInner.numDomainId = ',@numDomainID,'
																	AND OMInner.tintOppType = 1
																	AND OMInner.tintOppStatus = 1
																	AND UPPER(IInner.charItemType) = ''P''
																	AND ISNULL(OMInner.tintshipped,0) = 0
																	AND ISNULL(OIInner.bitDropShip,0) = 0
																	AND ISNULL(OIInner.numUnitHour,0) <> ISNULL(TempFulFilled.PickPacked,0)',') ')
	END
	ELSE IF ISNULL(@tintDashboardReminderType,0) = 12
	BEGIN
		SET @strSql = CONCAT(@strSql,' AND Opp.numOppID IN (','SELECT DISTINCT
																	OMInner.numOppId
																FROM
																	OpportunityMaster OMInner
																INNER JOIN
																	OpportunityItems OIInner
																ON
																	OIInner.numOppId = OMInner.numOppId
																INNER JOIN
																	Item IInner
																ON
																	OIInner.numItemCode = IInner.numItemCode
																OUTER APPLY
																(
																	SELECT
																		SUM(OpportunityBizDocItems.numUnitHour) AS FulFilledQty
																	FROM
																		OpportunityBizDocs
																	INNER JOIN
																		OpportunityBizDocItems 
																	ON
																		OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																	WHERE
																		OpportunityBizDocs.numOppId = OMInner.numOppId
																		AND ISNULL(OpportunityBizDocs.numBizDocId,0) = 296
																		AND ISNULL(OpportunityBizDocs.bitFulFilled,0) = 1
																		AND OpportunityBizDocItems.numOppItemID = OIInner.numoppitemtCode
																) AS TempFulFilled
																WHERE
																	OMInner.numDomainId = ',@numDomainID,'
																	AND OMInner.tintOppType = 1
																	AND OMInner.tintOppStatus = 1
																	AND ISNULL(OMInner.tintshipped,0) = 0
																	AND UPPER(IInner.charItemType) = ''P''
																	AND ISNULL(OIInner.bitDropShip,0) = 0
																	AND ISNULL(OIInner.numUnitHour,0) <> ISNULL(TempFulFilled.FulFilledQty,0)',') ')
	END
	ELSE IF ISNULL(@tintDashboardReminderType,0) = 13
	BEGIN
		SET @strSql = CONCAT(@strSql,' AND Opp.numOppID IN (','SELECT DISTINCT
																	OMInner.numOppId
																FROM
																	OpportunityMaster OMInner
																INNER JOIN
																	OpportunityItems OIInner
																ON
																	OIInner.numOppId = OMInner.numOppID
																INNER JOIN
																	Item IInner
																ON
																	OIInner.numItemCode = IInner.numItemCode
																OUTER APPLY
																(
																	SELECT
																		SUM(OpportunityBizDocItems.numUnitHour) AS InvoicedQty
																	FROM
																		OpportunityBizDocs
																	INNER JOIN
																		OpportunityBizDocItems 
																	ON
																		OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																	WHERE
																		OpportunityBizDocs.numOppId = OMInner.numOppID
																		AND OpportunityBizDocItems.numOppItemID = OIInner.numoppitemtCode
																		AND OpportunityBizDocs.numBizDocId = ISNULL((SELECT TOP 1 numAuthoritativeSales FROM AuthoritativeBizDocs WHERE numDomainId=',@numDomainID,'),287)
																) AS TempInvoice
																WHERE
																	OMInner.numDomainId = ',@numDomainID,'
																	AND OMInner.tintOppType = 1
																	AND OMInner.tintOppStatus  = 1
																	AND ISNULL(OIInner.numUnitHour,0) <> ISNULL(TempInvoice.InvoicedQty,0)',') ')
	END
	ELSE IF ISNULL(@tintDashboardReminderType,0) = 14
	BEGIN
		DECLARE @TEMPOppID TABLE
		(
			numOppID NUMERIC(18,0)
		)

		INSERT INTO @TEMPOppID
		(
			numOppID
		)
		SELECT DISTINCT
			OM.numOppID
		FROM
			OpportunityMaster OM
		INNER JOIN
			OpportunityItems OI
		ON
			OM.numOppId = OI.numOppId
		WHERE   
			OM.numDomainId = @numDomainId
			AND OM.tintOppType = 1
			AND OM.tintOppStatus=1
			AND ISNULL(OI.bitDropShip, 0) = 1
			AND 1 = (CASE WHEN (SELECT COUNT(*) FROM OpportunityLinking OMInner INNER JOIN OpportunityItems OIInner ON OMInner.numChildOppID=OIInner.numOppId WHERE OMInner.numParentOppID = OM.numOppID AND OIInner.numItemCode=OI.numItemCode AND ISNULL(OIInner.numWarehouseItmsID,0)=ISNULL(OI.numWarehouseItmsID,0)) > 0 THEN 0 ELSE 1 END)


		If ISNULL((SELECT bitReOrderPoint FROM Domain WHERE numDomainId=@numDomainID),0) = 1
		BEGIN
			INSERT INTO @TEMPOppID
			(
				numOppID
			)
			SELECT DISTINCT
				OM.numOppID
			FROM
				OpportunityMaster OM
			INNER JOIN
				OpportunityItems OI
			ON
				OI.numOppId = OM.numOppId
			INNER JOIN 
				Item I 
			ON 
				I.numItemCode = OI.numItemCode
			INNER JOIN 
				WarehouseItems WI 
			ON 
				OI.numWarehouseItmsID = WI.numWareHouseItemID
			WHERE   
				OM.numDomainId = @numDomainId
				AND ISNULL((SELECT bitReOrderPoint FROM Domain WHERE numDomainId=@numDomainID),0) = 1
				AND (ISNULL(WI.numOnHand,0) + ISNULL(WI.numOnOrder,0) <= ISNULL(WI.numReOrder,0))
				AND OM.tintOppType = 1
				AND OM.tintOppStatus=1
				AND ISNULL(I.bitAssembly, 0) = 0
				AND ISNULL(I.bitKitParent, 0) = 0
				AND ISNULL(OI.bitDropship,0) = 0
				AND OM.numOppId NOT IN (SELECT numOppID FROM @TEMPOppID)

			IF (SELECT COUNT(*) FROM @TEMPOppID) > 0
			BEGIN
				DECLARE @tmpIds VARCHAR(MAX) = ''
				SELECT @tmpIds = CONCAT(@tmpIds,numOppID,', ') FROM @TEMPOppID
				SET @strSql = CONCAT(@strSql,' AND Opp.numOppID IN (',SUBSTRING(@tmpIds, 0, LEN(@tmpIds)),') ')
			END
			ELSE
			BEGIN
				SET @strSql = CONCAT(@strSql,' AND Opp.numOppID IN (0) ')
			END
		END
	END
	ELSE IF ISNULL(@tintDashboardReminderType,0) = 15
	BEGIN
		SET @strSql = CONCAT(@strSql,' AND Opp.numOppID IN (','SELECT
																	OMInner.numOppID
																FROM
																	OpportunityMaster OMInner
																WHERE
																	numDomainId=',@numDomainID,'
																	AND tintOppType=1
																	AND tintOppStatus=1
																	AND ISNULL(OMInner.tintshipped,0) = 0
																	AND dtReleaseDate IS NOT NULL
																	AND dtReleaseDate < DateAdd(minute,',@ClientTimeZoneOffset * -1,',GETUTCDATE())
																	AND (SELECT 
																			COUNT(*) 
																		FROM 
																		(
																			SELECT
																				OIInner.numoppitemtCode,
																				ISNULL(OIInner.numUnitHour,0) AS OrderedQty,
																				ISNULL(TempFulFilled.FulFilledQty,0) AS FulFilledQty
																			FROM
																				OpportunityItems OIInner
																			INNER JOIN
																				Item IInner
																			ON
																				OIInner.numItemCode = IInner.numItemCode
																			OUTER APPLY
																			(
																				SELECT
																					SUM(OpportunityBizDocItems.numUnitHour) AS FulFilledQty
																				FROM
																					OpportunityBizDocs
																				INNER JOIN
																					OpportunityBizDocItems 
																				ON
																					OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																				WHERE
																					OpportunityBizDocs.numOppId = OMInner.numOppId
																					AND ISNULL(OpportunityBizDocs.numBizDocId,0) = 296
																					AND ISNULL(OpportunityBizDocs.bitFulFilled,0) = 1
																					AND OpportunityBizDocItems.numOppItemID = OIInner.numoppitemtCode
																			) AS TempFulFilled
																			WHERE
																				OIInner.numOppID = OMInner.numOppID
																				AND UPPER(IInner.charItemType) = ''P''
																				AND ISNULL(OIInner.bitDropShip,0) = 0
																		) X
																		WHERE
																			X.OrderedQty <> X.FulFilledQty) > 0',') ')
	END



	DECLARE  @firstRec  AS INTEGER
	DECLARE  @lastRec  AS INTEGER
  
	SET @firstRec = (@CurrentPage - 1) * @PageSize
	SET @lastRec = (@CurrentPage * @PageSize + 1)



	-- EXECUTE FINAL QUERY
	DECLARE @strFinal AS NVARCHAR(MAX)
	SET @strFinal = CONCAT('SELECT COUNT(*) OVER() AS TotalRecords, ',@strColumns,@strSql,' ORDER BY ',@columnName,' ',@columnSortOrder,' OFFSET ',(@CurrentPage - 1) * @PageSize,' ROWS FETCH NEXT ',@PageSize,' ROWS ONLY;')

	PRINT CAST(@strFinal AS NTEXT)
	EXEC sp_executesql @strFinal

	SELECT * FROM #tempForm

	DROP TABLE #tempForm

	DROP TABLE #tempColorScheme
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetMirrorBizDocDetails' ) 
    DROP PROCEDURE USP_GetMirrorBizDocDetails
GO

-- EXEC USP_GetMirrorBizDocDetails 53,8,1,1,0
CREATE PROCEDURE [dbo].[USP_GetMirrorBizDocDetails]
    (
      @numReferenceID NUMERIC(9) = 0,
      @numReferenceType TINYINT,
      @numDomainId NUMERIC(9),
      @numUserCntID NUMERIC(9),
      @ClientTimeZoneOffset INT
    )
AS 
    BEGIN
--        DECLARE @numBizDocID AS NUMERIC
        DECLARE @numBizDocType AS NUMERIC
        
        IF @numReferenceType = 1
			SELECT  @numBizDocType = numListItemID FROM dbo.ListDetails WHERE numListID=27 AND vcData='Sales Order' AND constFlag=1
        ELSE IF @numReferenceType = 2
			SELECT  @numBizDocType = numListItemID FROM dbo.ListDetails WHERE numListID=27 AND vcData='Purchase Order' AND constFlag=1
        ELSE IF @numReferenceType = 3
			SELECT  @numBizDocType = numListItemID FROM dbo.ListDetails WHERE numListID=27 AND vcData='Sales Opportunity' AND constFlag=1
        ELSE IF @numReferenceType = 4
			SELECT  @numBizDocType = numListItemID FROM dbo.ListDetails WHERE numListID=27 AND vcData='Purchase Opportunity' AND constFlag=1
        ELSE IF @numReferenceType = 5 
			SELECT @numBizDocType =  numListItemID FROM dbo.ListDetails WHERE vcData='RMA' AND constFlag=1
		ELSE IF @numReferenceType = 6 
			SELECT @numBizDocType =  numListItemID FROM dbo.ListDetails WHERE vcData='RMA' AND constFlag=1 
		ELSE IF @numReferenceType = 7 
			SELECT @numBizDocType =  numListItemID FROM dbo.ListDetails WHERE vcData='Sales Credit Memo' AND constFlag=1 
		ELSE IF @numReferenceType = 8
			SELECT @numBizDocType =  numListItemID FROM dbo.ListDetails WHERE vcData='Purchase Credit Memo' AND constFlag=1 
		ELSE IF @numReferenceType = 9
			SELECT @numBizDocType =  numListItemID FROM dbo.ListDetails WHERE vcData='Refund Receipt' AND constFlag=1 
		ELSE IF @numReferenceType = 10
			SELECT @numBizDocType =  numListItemID FROM dbo.ListDetails WHERE vcData='Credit Memo' AND constFlag=1 
		ELSE IF @numReferenceType = 11
			SELECT  @numBizDocType = numListItemID FROM dbo.ListDetails WHERE vcData='Invoice' AND constFlag=1
	 
        IF @numReferenceType = 1 OR @numReferenceType = 2 OR @numReferenceType = 3 OR @numReferenceType = 4 
		BEGIN
		      
			---------------------------------------------------------------------------------------
      
				  SELECT Mst.vcPOppName AS vcBizDocID,
						 Mst.vcPOppName AS OppName,Mst.vcPOppName AS BizDcocName,@numBizDocType AS BizDoc,ISNULL(Mst.numRecOwner,0) AS Owner,
						 CMP.VcCompanyName AS CompName,Mst.numContactID,Mst.numCreatedBy AS BizDocOwner,
						 Mst.fltDiscount AS decDiscount,
						 (SELECT SUM(ISNULL(monAmountPaid,0)) FROM OpportunityBizDocs WHERE numOppId=MSt.numOppId) AS monAmountPaid,
						 isnull(Mst.txtComments,'') AS vcComments,
						 CONVERT(VARCHAR(20),DATEADD(minute,-@ClientTimeZoneOffset,Mst.bintCreatedDate)) dtCreatedDate,
						 dbo.fn_GetContactName(Mst.numCreatedby) AS numCreatedby,
						 dbo.fn_GetContactName(Mst.numModifiedBy) AS numModifiedBy,
						 Mst.bintModifiedDate AS dtModifiedDate,
						 '' AS numViewedBy,
						 NULL AS dtViewedDate,
						 isnull(Mst.bitBillingTerms,0) AS tintBillingTerms,
						 isnull(Mst.intBillingDays,0) AS numBillingDays,
						 ISNULL(BTR.numNetDueInDays,0) AS numBillingDaysName,
						 BTR.vcTerms AS vcBillingTermsName,
						 isnull(Mst.bitInterestType,0) AS tintInterestType,
						 isnull(Mst.fltInterest,0) AS fltInterest,
						 tintOPPType,
						 @numBizDocType AS numBizDocId,
						 Mst.bintAccountClosingDate,
						 tintShipToType,
						 tintBillToType,
						 tintshipped,
						 NULL bintShippedDate,
						 ISNULL(Mst.monShipCost ,0) AS monShipCost,
						 ISNULL(BT.vcBizDocFooter,D.vcBizDocFooter) AS BizdocFooter,
						 ISNULL(BT.vcBizDocImagePath,D.vcBizDocImagePath) As vcBizDocImagePath, 
						 Mst.numDivisionID,
						  ISNULL(BT.vcPurBizDocFooter,D.vcPurBizDocFooter) AS PurBizdocFooter,
						 isnull(Mst.fltDiscount,0) AS fltDiscount,
						 isnull(Mst.bitDiscountType,0) AS bitDiscountType,
						 0 AS numShipVia,
						 '' AS vcTrackingURL,
						 0 AS numBizDocStatus,
						 '' AS BizDocStatus,
						 CASE 
							WHEN Mst.intUsedShippingCompany IS NULL 
							THEN '-' 
							WHEN Mst.intUsedShippingCompany = -1 
							THEN 'Will-call' 
							ELSE dbo.fn_GetListItemName(Mst.intUsedShippingCompany) 
						 END AS ShipVia,
						 ISNULL((SELECT vcShipmentService FROM ShippingService WHERE (numDomainID=@numDomainID OR ISNULL(numDomainID,0)=0) AND numShippingServiceID = Mst.numShippingService),'') AS vcShippingService,
						'' AS vcPackingSlip,
						 ISNULL(C.varCurrSymbol,'') varCurrSymbol,
						 0 AS ShippingReportCount,
						 0 bitPartialFulfilment,
						 vcPOppName as vcBizDocName,
						 dbo.[fn_GetContactName](Mst.[numModifiedBy])  + ', '
							+ CONVERT(VARCHAR(20),dateadd(MINUTE,-@ClientTimeZoneOffset,Mst.bintModifiedDate)) AS ModifiedBy,
						 dbo.[fn_GetContactName](Mst.[numRecOwner]) + ', ' +  dbo.[fn_GetContactName](Mst.[numAssignedTo]) AS OrderRecOwner,
						 dbo.[fn_GetContactName](DM.[numRecOwner]) + ', ' +  dbo.[fn_GetContactName](DM.[numAssignedTo]) AS AccountRecOwner,
						 Mst.bintCreatedDate AS dtFromDate,
						 Mst.tintTaxOperator,
						 0 AS monTotalEmbeddedCost,
						 0 AS monTotalAccountedCost,
						 ISNULL(BT.bitEnabled,0) bitEnabled,
						 ISNULL(BT.txtBizDocTemplate,'') txtBizDocTemplate, 
						 ISNULL(BT.txtCSS,'') txtCSS,
						 dbo.fn_GetContactName(Mst.numAssignedTo) AS AssigneeName,
						 ISNULL((SELECT vcEmail FROM dbo.AdditionalContactsInformation WHERE numContactID = Mst.numAssignedTo),'') AS AssigneeEmail,
						 ISNULL((SELECT numPhone FROM dbo.AdditionalContactsInformation WHERE numContactID = Mst.numAssignedTo),'') AS AssigneePhone,
						 dbo.fn_GetComapnyName(Mst.numDivisionId) OrganizationName,
						 dbo.fn_GetContactName(Mst.numContactID)  OrgContactName,
						 case when ACI.numPhone<>'' then ACI.numPhone +case when ACI.numPhoneExtension<>'' then ' - ' + ACI.numPhoneExtension else '' end  else '' END OrgContactPhone,
						 DM.vcComPhone as OrganizationPhone,
						 ISNULL(ACI.vcEmail,'') AS OrgContactEmail,
 						 dbo.[fn_GetContactName](Mst.[numRecOwner]) AS OnlyOrderRecOwner,
 						 0 bitAuthoritativeBizDocs,
						0 tintDeferred,0 AS monCreditAmount,
						 0 as bitRentalBizDoc,isnull(BT.numBizDocTempID,0) as numBizDocTempID,isnull(BT.vcTemplateName,'') as vcTemplateName,
						 [dbo].[GetDealAmount](Mst.numOppId ,getutcdate(),0 ) as monPAmount,
						 --(select top 1 SD.vcSignatureFile from SignatureDetail SD join OpportunityBizDocsDetails OBD on SD.numSignID=OBD.numSignID where SD.numDomainID = @numDomainID and OBD.numDomainID = @numDomainID and OBD.numSignID IS not null and OBD.numBizDocsId=opp.numOppBizDocsId order by OBD.numBizDocsPaymentDetId desc) as vcSignatureFile,
						 isnull(CMP.txtComments,'') as vcOrganizationComments,
						 ISNULL((SELECT SUBSTRING((SELECT '$^$' + ISNULL( (SELECT vcShipFieldValue FROM dbo.ShippingFieldValues WHERE numDomainID=MSt.numDomainID AND intShipFieldID IN ( SELECT intShipFieldID FROM dbo.ShippingFields WHERE numListItemID=OBD.numShipVia  AND vcFieldName='Tracking URL')),'') +'#^#'+ RTRIM(LTRIM(vcTrackingNo) )  FROM dbo.OpportunityBizDocs OBD WHERE OBD.numOppId=Mst.numOppID FOR XML PATH('')),4,200000)),'') AS  vcTrackingNo,'' AS  vcShippingMethod,
						 NULL AS dtDeliveryDate,ISNULL(Mst.vcOppRefOrderNo,'') AS vcOppRefOrderNo,ISNULL(Mst.numDiscountAcntType,0) AS numDiscountAcntType ,
						 isnull(numOppBizDocTempID,0) as numOppBizDocTempID,
						 com1.vcCompanyName AS EmployerOrganizationName,div1.vcComPhone as EmployerOrganizationPhone,dbo.FormatedDateFromDate(Mst.dtReleaseDate,@numDomainID) AS vcReleaseDate
						 ,dbo.FormatedDateFromDate(Mst.intpEstimatedCloseDate,@numDomainID) AS vcRequiredDate
						 ,(CASE WHEN ISNULL(Mst.numPartner,0) > 0 THEN ISNULL((SELECT CONCAT(DivisionMaster.vcPartnerCode,'-',CompanyInfo.vcCompanyName) FROM DivisionMaster INNER JOIN CompanyInfo ON DivisionMaster.numCompanyID=CompanyInfo.numCompanyId WHERE DivisionMaster.numDivisionID=Mst.numPartner),'') ELSE '' END) AS vcPartner,
						 ISNULL(vcCustomerPO#,'') vcCustomerPO#,
						 ISNULL(Mst.txtComments,'') vcSOComments,
						 ISNULL(DM.vcShippersAccountNo,'') AS vcShippersAccountNo,
						 CONVERT(VARCHAR(20),DATEADD(minute,-@ClientTimeZoneOffset,Mst.bintCreatedDate)) AS OrderCreatedDate,
						 '' AS vcVendorInvoice
				  FROM    OpportunityMaster Mst JOIN Domain D ON D.numDomainID = Mst.numDomainID
						  LEFT OUTER JOIN [Currency] C ON C.numCurrencyID = Mst.numCurrencyID
						  LEFT JOIN [DivisionMaster] DM ON DM.numDivisionID = Mst.numDivisionID   
						  LEFT JOIN CompanyInfo CMP ON DM.numCompanyId = CMP.numCompanyId and CMP.numDomainID = @numDomainID  
						  LEFT JOIN dbo.AdditionalContactsInformation ACI ON ACI.numContactID = Mst.numContactID
						  LEFT JOIN [BizDocTemplate] BT ON BT.numDomainID = @numDomainID AND BT.numBizDocID = @numBizDocType 
							 AND ((BT.numBizDocTempID=ISNULL(Mst.numOppBizDocTempID, 0)) OR (ISNULL(BT.bitDefault,0)=1 AND ISNULL(BT.bitEnabled,0)=1))  
						  LEFT JOIN BillingTerms BTR ON BTR.numTermsID = ISNULL(Mst.intBillingDays,0)
						  JOIN divisionmaster div1 ON D.numDivisionID = div1.numDivisionID
						  JOIN companyinfo [Com1] ON com1.numCompanyID = div1.numCompanyID
				  WHERE  Mst.numOppID = @numReferenceID AND Mst.numDomainID = @numDomainID
			             
						 EXEC USP_OPPGetOppAddressDetails @numReferenceID,@numDomainId,0
             
            END
    
    
        ELSE IF @numReferenceType = 5 OR @numReferenceType = 6 OR @numReferenceType = 7 OR @numReferenceType = 8 
					OR @numReferenceType = 9 OR @numReferenceType = 10
         BEGIN
         
								DECLARE @numBizDocTempID AS NUMERIC(9)
								DECLARE @numDivisionID AS NUMERIC(9)
         
                                SELECT @numDivisionID=numDivisionID,@numBizDocTempID=(CASE WHEN @numReferenceType = 5 OR @numReferenceType = 6 OR @numReferenceType = 9 OR @numReferenceType = 10 THEN numRMATempID ELSE numBizDocTempID END)				
                                FROM    dbo.ReturnHeader RH
                                WHERE   RH.numReturnHeaderID = @numReferenceID
                                
    			---------------------
               
                                SELECT  RH.vcBizDocName AS [vcBizDocID],
                                  RH.vcRMA AS OppName,RH.vcBizDocName AS BizDcocName,@numBizDocType AS BizDoc,ISNULL(RH.numCreatedBy,0) AS Owner,
								   CMP.VcCompanyName AS CompName,RH.numContactID,RH.numCreatedBy AS BizDocOwner,
                                        RH.monTotalDiscount AS decDiscount,
                                        ISNULL(RDA.monAmount + RDA.monTotalTax - RDA.monTotalDiscount, 0) AS monAmountPaid,
                                        ISNULL(RH.vcComments, '') AS vcComments,
                                        CONVERT(VARCHAR(20), DATEADD(minute, -@ClientTimeZoneOffset, RH.dtCreatedDate)) dtCreatedDate,
                                        dbo.fn_GetContactName(RH.numCreatedby) AS numCreatedby,
                                        dbo.fn_GetContactName(RH.numModifiedBy) AS numModifiedBy,
                                        RH.dtModifiedDate,
                                        '' AS numViewedBy,
                                        '' AS dtViewedDate,
                                        0 AS tintBillingTerms,
                                        0 AS numBillingDays,
                                        0 AS [numBillingDaysName],
                                        '' AS vcBillingTermsName,
                                        0 AS tintInterestType,
                                        0 AS fltInterest,
                                        tintReturnType AS tintOPPType,
                                        @numBizDocType AS numBizDocId,
                                        dbo.fn_GetContactName(RH.numCreatedby) AS ApprovedBy,
                                        dtCreatedDate AS dtApprovedDate,
                                        0 AS bintAccountClosingDate,
                                        0 AS tintShipToType,
                                        0 AS tintBillToType,
                                        0 AS tintshipped,
                                        0 AS bintShippedDate,
                                        0 AS monShipCost,
                                        ISNULL(BT.vcBizDocFooter,D.vcBizDocFooter) AS BizdocFooter,
                                        ISNULL(BT.vcBizDocImagePath,D.vcBizDocImagePath) As vcBizDocImagePath,
                                        RH.numDivisionID,
                                         ISNULL(BT.vcPurBizDocFooter,D.vcPurBizDocFooter) AS PurBizdocFooter,
                                        ISNULL(RH.monTotalDiscount, 0) AS fltDiscount,
                                        0 AS bitDiscountType,
                                        0 AS numShipVia,
                                        '' AS vcTrackingURL,
                                        0 AS numBizDocStatus,
                                        '-' AS BizDocStatus,
                                        '' AS ShipVia,
										'' AS vcShippingService,
										'' AS vcPackingSlip,
                                        ISNULL(C.varCurrSymbol, '') varCurrSymbol,
                                        0 AS ShippingReportCount,
                                        0 bitPartialFulfilment,
                                        vcBizDocName,
                                        dbo.[fn_GetContactName](RH.[numModifiedBy])
                                        + ', '
                                        + CONVERT(VARCHAR(20), DATEADD(MINUTE, -@ClientTimeZoneOffset, RH.dtModifiedDate)) AS ModifiedBy,
                                        dbo.[fn_GetContactName](RH.[numCreatedBy]) AS OrderRecOwner,
                                        dbo.[fn_GetContactName](DM.[numRecOwner])
                                        + ', '
                                        + dbo.[fn_GetContactName](DM.[numAssignedTo]) AS AccountRecOwner,
                                        RH.dtcreatedDate AS dtFromDate,
                                        0 AS tintTaxOperator,
                                        1 AS monTotalEmbeddedCost,
                                        2 AS monTotalAccountedCost,
                                        ISNULL(BT.bitEnabled, 0) bitEnabled,
                                        ISNULL(BT.txtBizDocTemplate, '') txtBizDocTemplate,
                                        ISNULL(BT.txtCSS, '') txtCSS,
                                        '' AS AssigneeName,
                                        '' AS AssigneeEmail,
                                        '' AS AssigneePhone,
                                        dbo.fn_GetComapnyName(RH.numDivisionId) OrganizationName,
                                        dbo.fn_GetContactName(RH.numContactID) OrgContactName,
                                        dbo.getCompanyAddress(RH.numDivisionId, 1,RH.numDomainId) CompanyBillingAddress,
                                        CASE WHEN ACI.numPhone <> ''
                                             THEN ACI.numPhone
                                                  + CASE WHEN ACI.numPhoneExtension <> ''
                                                         THEN ' - ' + ACI.numPhoneExtension
                                                         ELSE ''
                                                    END
                                             ELSE ''
                                        END OrgContactPhone,
                                        DM.vcComPhone AS OrganizationPhone,
                                        ISNULL(ACI.vcEmail, '') AS OrgContactEmail,
                                        dbo.[fn_GetContactName](RH.[numCreatedBy]) AS OnlyOrderRecOwner,
                                        1 bitAuthoritativeBizDocs,
                                        0 tintDeferred,
                                        0 AS monCreditAmount,
                                        0 AS bitRentalBizDoc,
                                        ISNULL(BT.numBizDocTempID, 0) AS numBizDocTempID,
                                        ISNULL(BT.vcTemplateName, '') AS vcTemplateName,
                                        0 AS monPAmount,
                                        ISNULL(CMP.txtComments, '') AS vcOrganizationComments,
                                        '' AS vcTrackingNo,
                                        '' AS vcShippingMethod,
                                        '' AS dtDeliveryDate,
                                        '' AS vcOppRefOrderNo,
                                        0 AS numDiscountAcntType ,
										0 AS numOppBizDocTempID
                                        ,com1.vcCompanyName AS EmployerOrganizationName,div1.vcComPhone as EmployerOrganizationPhone
										,'' AS vcReleaseDate
										,'' AS vcRequiredDate
										,'' AS vcPartner
										,'' vcCustomerPO#
										,'' vcSOComments
										,'' AS vcShippersAccountNo
										,'' AS OrderCreatedDate
										,'' AS vcVendorInvoice
                                FROM    dbo.ReturnHeader RH
                                        JOIN Domain D ON D.numDomainID = RH.numDomainID
                                        JOIN [DivisionMaster] DM ON DM.numDivisionID = RH.numDivisionID
                                        LEFT OUTER JOIN [Currency] C ON C.numCurrencyID = DM.numCurrencyID
                                        LEFT JOIN CompanyInfo CMP ON DM.numCompanyId = CMP.numCompanyId
                                                                     AND CMP.numDomainID = @numDomainID
                                        LEFT JOIN dbo.AdditionalContactsInformation ACI ON ACI.numContactID = RH.numContactID
                                        LEFT JOIN [BizDocTemplate] BT ON BT.numDomainID = @numDomainID
                                                                         AND BT.numBizDocID = @numBizDocType	
                                                                         AND ((BT.numBizDocTempID=ISNULL(@numBizDocTempID, 0)) OR (ISNULL(BT.bitDefault,0)=1 AND ISNULL(BT.bitEnabled,0)=1))  
                                         JOIN divisionmaster div1 ON D.numDivisionID = div1.numDivisionID
										 JOIN companyinfo [Com1] ON com1.numCompanyID = div1.numCompanyID,
                                                                         dbo.GetReturnDealAmount(@numReferenceID,@numReferenceType) RDA
                                WHERE   RH.numReturnHeaderID = @numReferenceID
                                
                                	--************Customer/Vendor Billing Address************
                                		SELECT  '<pre>' + isnull(AD.vcStreet,'') + '</pre>' + isnull(AD.vcCity,'') + ' ,' + isnull(dbo.fn_GetState(AD.numState),'') + ' ' + isnull(AD.vcPostalCode,'') + ' <br>' + isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcFullAddress,
											isnull(AD.vcStreet,'') AS vcStreet,
											isnull(AD.vcCity,'') AS vcCity,
											isnull(dbo.fn_GetState(AD.numState),'') AS vcState,
											isnull(AD.vcPostalCode,'') AS vcPostalCode,
											isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcCountry,
											ISNULL(AD.vcAddressName,'') AS vcAddressName,
            								CMP.vcCompanyName
											,(CASE 
													WHEN ISNULL(bitAltContact,0) = 1 
													THEN ISNULL(vcAltContact,'') 
													ELSE 
														(CASE 
															WHEN ISNULL(numContact,0) > 0 
															THEN 
																ISNULL((SELECT TOP 1 CONCAT(vcFirstName,' ',vcLastName) FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND numContactId=numContact),'')
															ELSE 
																ISNULL((SELECT TOP 1 CONCAT(vcFirstName,' ',vcLastName) FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND numDivisionId=AD.numRecordID AND ISNULL(bitPrimaryContact,0)=1),'')
															END
														)  
											END) AS vcContact
										 FROM AddressDetails AD 
										 JOIN [DivisionMaster] DM ON DM.numDivisionID = AD.numRecordID   
										 JOIN CompanyInfo CMP ON DM.numCompanyId = CMP.numCompanyId and CMP.numDomainID = AD.numDomainID  
										 --LEFT JOIN dbo.AdditionalContactsInformation ACI ON ACI.numContactID = @numContactID AND ACI.numDivisionId=DM.numDivisionID
										 WHERE AD.numDomainID=@numDomainID AND AD.numRecordID=@numDivisionID 
										 AND AD.tintAddressOf = 2 AND AD.tintAddressType = 1 AND AD.bitIsPrimary=1

                                        
                                	--************Customer/Vendor Shipping Address************
                                	SELECT  '<pre>' + isnull(AD.vcStreet,'') + '</pre>' + isnull(AD.vcCity,'') + ' ,' + isnull(dbo.fn_GetState(AD.numState),'') + ' ' + isnull(AD.vcPostalCode,'') + ' <br>' + isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcFullAddress,
											isnull(AD.vcStreet,'') AS vcStreet,
											isnull(AD.vcCity,'') AS vcCity,
											isnull(dbo.fn_GetState(AD.numState),'') AS vcState,
											isnull(AD.vcPostalCode,'') AS vcPostalCode,
											isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcCountry,
											ISNULL(AD.vcAddressName,'') AS vcAddressName,
            								CMP.vcCompanyName,
											(CASE 
													WHEN ISNULL(bitAltContact,0) = 1 
													THEN ISNULL(vcAltContact,'') 
													ELSE 
														(CASE 
															WHEN ISNULL(numContact,0) > 0 
															THEN 
																ISNULL((SELECT TOP 1 CONCAT(vcFirstName,' ',vcLastName) FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND numContactId=numContact),'')
															ELSE 
																ISNULL((SELECT TOP 1 CONCAT(vcFirstName,' ',vcLastName) FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND numDivisionId=AD.numRecordID AND ISNULL(bitPrimaryContact,0)=1),'')
															END
														)  
											END) AS vcContact
										 FROM AddressDetails AD 
										 JOIN [DivisionMaster] DM ON DM.numDivisionID = AD.numRecordID   
										 JOIN CompanyInfo CMP ON DM.numCompanyId = CMP.numCompanyId and CMP.numDomainID = AD.numDomainID  
										 --LEFT JOIN dbo.AdditionalContactsInformation ACI ON ACI.numContactID = @numContactID AND ACI.numDivisionId=DM.numDivisionID
										 WHERE AD.numDomainID=@numDomainID AND AD.numRecordID=@numDivisionID 
										 AND AD.tintAddressOf = 2 AND AD.tintAddressType = 2 AND AD.bitIsPrimary=1

                                --************Employer Shipping Address************ 
									SELECT  '<pre>' + isnull(AD.vcStreet,'') + '</pre>' + isnull(AD.vcCity,'') + ' ,' + isnull(dbo.fn_GetState(AD.numState),'') + ' ' + isnull(AD.vcPostalCode,'') + ' <br>' + isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcFullAddress,
											isnull(AD.vcStreet,'') AS vcStreet,
											isnull(AD.vcCity,'') AS vcCity,
											isnull(dbo.fn_GetState(AD.numState),'') AS vcState,
											isnull(AD.vcPostalCode,'') AS vcPostalCode,
											isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcCountry,
											ISNULL(AD.vcAddressName,'') AS vcAddressName,vcCompanyName
									 FROM Domain D1 JOIN divisionmaster div1 ON D1.numDivisionID = div1.numDivisionID
										  JOIN companyinfo [Com1] ON com1.numCompanyID = div1.numCompanyID
										  JOIN dbo.AddressDetails AD ON AD.numDomainID = div1.numDomainID AND AD.numRecordID = div1.numDivisionID 
									  WHERE  D1.numDomainID = @numDomainID
										AND AD.tintAddressOf = 2 AND AD.tintAddressType = 1 AND AD.bitIsPrimary=1


								--************Employer Shipping Address************ 
								SELECT  '<pre>' + isnull(AD.vcStreet,'') + '</pre>' + isnull(AD.vcCity,'') + ' ,' + isnull(dbo.fn_GetState(AD.numState),'') + ' ' + isnull(AD.vcPostalCode,'') + ' <br>' + isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcFullAddress,
										isnull(AD.vcStreet,'') AS vcStreet,
										isnull(AD.vcCity,'') AS vcCity,
										isnull(dbo.fn_GetState(AD.numState),'') AS vcState,
										isnull(AD.vcPostalCode,'') AS vcPostalCode,
										isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcCountry,
										ISNULL(AD.vcAddressName,'') AS vcAddressName,vcCompanyName
								 FROM Domain D1 JOIN divisionmaster div1 ON D1.numDivisionID = div1.numDivisionID
									  JOIN companyinfo [Com1] ON com1.numCompanyID = div1.numCompanyID
									  JOIN dbo.AddressDetails AD ON AD.numDomainID = div1.numDomainID AND AD.numRecordID = div1.numDivisionID 
								 WHERE AD.numDomainID=@numDomainID 
								 AND AD.tintAddressOf = 2 AND AD.tintAddressType = 2 AND AD.bitIsPrimary=1

								 --************Customer/Vendor Billing Address************
                                		SELECT  '<pre>' + isnull(AD.vcStreet,'') + '</pre>' + isnull(AD.vcCity,'') + ' ,' + isnull(dbo.fn_GetState(AD.numState),'') + ' ' + isnull(AD.vcPostalCode,'') + ' <br>' + isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcFullAddress,
											isnull(AD.vcStreet,'') AS vcStreet,
											isnull(AD.vcCity,'') AS vcCity,
											isnull(dbo.fn_GetState(AD.numState),'') AS vcState,
											isnull(AD.vcPostalCode,'') AS vcPostalCode,
											isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcCountry,
											ISNULL(AD.vcAddressName,'') AS vcAddressName,
            								CMP.vcCompanyName
											,(CASE 
													WHEN ISNULL(bitAltContact,0) = 1 
													THEN ISNULL(vcAltContact,'') 
													ELSE 
														(CASE 
															WHEN ISNULL(numContact,0) > 0 
															THEN 
																ISNULL((SELECT TOP 1 CONCAT(vcFirstName,' ',vcLastName) FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND numContactId=numContact),'')
															ELSE 
																ISNULL((SELECT TOP 1 CONCAT(vcFirstName,' ',vcLastName) FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND numDivisionId=AD.numRecordID AND ISNULL(bitPrimaryContact,0)=1),'')
															END
														)  
											END) AS vcContact
										 FROM AddressDetails AD 
										 JOIN [DivisionMaster] DM ON DM.numDivisionID = AD.numRecordID   
										 JOIN CompanyInfo CMP ON DM.numCompanyId = CMP.numCompanyId and CMP.numDomainID = AD.numDomainID  
										 --LEFT JOIN dbo.AdditionalContactsInformation ACI ON ACI.numContactID = @numContactID AND ACI.numDivisionId=DM.numDivisionID
										 WHERE AD.numDomainID=@numDomainID AND AD.numRecordID=@numDivisionID 
										 AND AD.tintAddressOf = 2 AND AD.tintAddressType = 1 AND AD.bitIsPrimary=1

                                        
                                	--************Customer/Vendor Shipping Address************
                                	SELECT  '<pre>' + isnull(AD.vcStreet,'') + '</pre>' + isnull(AD.vcCity,'') + ' ,' + isnull(dbo.fn_GetState(AD.numState),'') + ' ' + isnull(AD.vcPostalCode,'') + ' <br>' + isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcFullAddress,
											isnull(AD.vcStreet,'') AS vcStreet,
											isnull(AD.vcCity,'') AS vcCity,
											isnull(dbo.fn_GetState(AD.numState),'') AS vcState,
											isnull(AD.vcPostalCode,'') AS vcPostalCode,
											isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcCountry,
											ISNULL(AD.vcAddressName,'') AS vcAddressName,
            								CMP.vcCompanyName,
											(CASE 
													WHEN ISNULL(bitAltContact,0) = 1 
													THEN ISNULL(vcAltContact,'') 
													ELSE 
														(CASE 
															WHEN ISNULL(numContact,0) > 0 
															THEN 
																ISNULL((SELECT TOP 1 CONCAT(vcFirstName,' ',vcLastName) FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND numContactId=numContact),'')
															ELSE 
																ISNULL((SELECT TOP 1 CONCAT(vcFirstName,' ',vcLastName) FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND numDivisionId=AD.numRecordID AND ISNULL(bitPrimaryContact,0)=1),'')
															END
														)  
											END) AS vcContact
										 FROM AddressDetails AD 
										 JOIN [DivisionMaster] DM ON DM.numDivisionID = AD.numRecordID   
										 JOIN CompanyInfo CMP ON DM.numCompanyId = CMP.numCompanyId and CMP.numDomainID = AD.numDomainID  
										 --LEFT JOIN dbo.AdditionalContactsInformation ACI ON ACI.numContactID = @numContactID AND ACI.numDivisionId=DM.numDivisionID
										 WHERE AD.numDomainID=@numDomainID AND AD.numRecordID=@numDivisionID 
										 AND AD.tintAddressOf = 2 AND AD.tintAddressType = 2 AND AD.bitIsPrimary=1
           END
		
        ELSE IF @numReferenceType = 11
		BEGIN
		      
			---------------------------------------------------------------------------------------
      
				  SELECT Mst.vcPOppName AS vcBizDocID,
						 Mst.vcPOppName AS OppName,OBD.vcBizDocID AS BizDcocName,@numBizDocType AS BizDoc,ISNULL(Mst.numRecOwner,0) AS Owner,
						 CMP.VcCompanyName AS CompName,Mst.numContactID,Mst.numCreatedBy AS BizDocOwner,
						 Mst.fltDiscount AS decDiscount,
						 (SELECT SUM(ISNULL(monAmountPaid,0)) FROM OpportunityBizDocs WHERE numOppId=MSt.numOppId) AS monAmountPaid,
						 isnull(Mst.txtComments,'') AS vcComments,
						 CONVERT(VARCHAR(20),DATEADD(minute,-@ClientTimeZoneOffset,Mst.bintCreatedDate)) dtCreatedDate,
						 dbo.fn_GetContactName(Mst.numCreatedby) AS numCreatedby,
						 dbo.fn_GetContactName(Mst.numModifiedBy) AS numModifiedBy,
						 Mst.bintModifiedDate AS dtModifiedDate,
						 '' AS numViewedBy,
						 NULL AS dtViewedDate,
						 isnull(Mst.bitBillingTerms,0) AS tintBillingTerms,
						 isnull(Mst.intBillingDays,0) AS numBillingDays,
						 ISNULL(BTR.numNetDueInDays,0) AS numBillingDaysName,
						 BTR.vcTerms AS vcBillingTermsName,
						 isnull(Mst.bitInterestType,0) AS tintInterestType,
						 isnull(Mst.fltInterest,0) AS fltInterest,
						 tintOPPType,
						 @numBizDocType AS numBizDocId,
						 Mst.bintAccountClosingDate,
						 tintShipToType,
						 tintBillToType,
						 tintshipped,
						 NULL bintShippedDate,
						 ISNULL(Mst.monShipCost ,0) AS monShipCost,
						 ISNULL(BT.vcBizDocFooter,D.vcBizDocFooter) AS BizdocFooter,
						 ISNULL(BT.vcBizDocImagePath,D.vcBizDocImagePath) As vcBizDocImagePath, 
						 Mst.numDivisionID,
						  ISNULL(BT.vcPurBizDocFooter,D.vcPurBizDocFooter) AS PurBizdocFooter,
						 isnull(Mst.fltDiscount,0) AS fltDiscount,
						 isnull(Mst.bitDiscountType,0) AS bitDiscountType,
						 0 AS numShipVia,
						 '' AS vcTrackingURL,
						 0 AS numBizDocStatus,
						 '' AS BizDocStatus,
						 CASE 
						   WHEN ISNULL(OBD.numShipVia,0) = 0 THEN (CASE WHEN Mst.intUsedShippingCompany IS NULL THEN '-' WHEN Mst.intUsedShippingCompany = -1 THEN 'Will-call' ELSE dbo.fn_GetListItemName(Mst.intUsedShippingCompany) END)
						   WHEN OBD.numShipVia = -1 THEN 'Will-call'
						   ELSE dbo.fn_GetListItemName(OBD.numShipVia)
						 END AS ShipVia,
						 ISNULL((SELECT vcShipmentService FROM ShippingService WHERE (numDomainID=@numDomainID OR ISNULL(numDomainID,0)=0) AND numShippingServiceID = Mst.numShippingService),'') AS vcShippingService,
						'' AS vcPackingSlip,
						 ISNULL(C.varCurrSymbol,'') varCurrSymbol,
						 0 AS ShippingReportCount,
						 0 bitPartialFulfilment,
						 vcPOppName as vcBizDocName,
						 dbo.[fn_GetContactName](Mst.[numModifiedBy])  + ', '
							+ CONVERT(VARCHAR(20),dateadd(MINUTE,-@ClientTimeZoneOffset,Mst.bintModifiedDate)) AS ModifiedBy,
						 dbo.[fn_GetContactName](Mst.[numRecOwner]) + ', ' +  dbo.[fn_GetContactName](Mst.[numAssignedTo]) AS OrderRecOwner,
						 dbo.[fn_GetContactName](DM.[numRecOwner]) + ', ' +  dbo.[fn_GetContactName](DM.[numAssignedTo]) AS AccountRecOwner,
						 Mst.bintCreatedDate AS dtFromDate,
						 Mst.tintTaxOperator,
						 0 AS monTotalEmbeddedCost,
						 0 AS monTotalAccountedCost,
						 ISNULL(BT.bitEnabled,0) bitEnabled,
						 ISNULL(BT.txtBizDocTemplate,'') txtBizDocTemplate, 
						 ISNULL(BT.txtCSS,'') txtCSS,
						 dbo.fn_GetContactName(Mst.numAssignedTo) AS AssigneeName,
						 ISNULL((SELECT vcEmail FROM dbo.AdditionalContactsInformation WHERE numContactID = Mst.numAssignedTo),'') AS AssigneeEmail,
						 ISNULL((SELECT numPhone FROM dbo.AdditionalContactsInformation WHERE numContactID = Mst.numAssignedTo),'') AS AssigneePhone,
						 dbo.fn_GetComapnyName(Mst.numDivisionId) OrganizationName,
						 dbo.fn_GetContactName(Mst.numContactID)  OrgContactName,
						 case when ACI.numPhone<>'' then ACI.numPhone +case when ACI.numPhoneExtension<>'' then ' - ' + ACI.numPhoneExtension else '' end  else '' END OrgContactPhone,
						 DM.vcComPhone as OrganizationPhone,
						 ISNULL(ACI.vcEmail,'') AS OrgContactEmail,
 						 dbo.[fn_GetContactName](Mst.[numRecOwner]) AS OnlyOrderRecOwner,
 						 0 bitAuthoritativeBizDocs,
						0 tintDeferred,0 AS monCreditAmount,
						 0 as bitRentalBizDoc,isnull(BT.numBizDocTempID,0) as numBizDocTempID,isnull(BT.vcTemplateName,'') as vcTemplateName,
						 [dbo].[GetDealAmount](Mst.numOppId ,getutcdate(),0 ) as monPAmount,
						 --(select top 1 SD.vcSignatureFile from SignatureDetail SD join OpportunityBizDocsDetails OBD on SD.numSignID=OBD.numSignID where SD.numDomainID = @numDomainID and OBD.numDomainID = @numDomainID and OBD.numSignID IS not null and OBD.numBizDocsId=opp.numOppBizDocsId order by OBD.numBizDocsPaymentDetId desc) as vcSignatureFile,
						 isnull(CMP.txtComments,'') as vcOrganizationComments,
						 ISNULL((SELECT SUBSTRING((SELECT '$^$' + ISNULL( (SELECT vcShipFieldValue FROM dbo.ShippingFieldValues WHERE numDomainID=MSt.numDomainID AND intShipFieldID IN ( SELECT intShipFieldID FROM dbo.ShippingFields WHERE numListItemID=OBD.numShipVia  AND vcFieldName='Tracking URL')),'') +'#^#'+ RTRIM(LTRIM(vcTrackingNo) )  FROM dbo.OpportunityBizDocs OBD WHERE OBD.numOppId=Mst.numOppID FOR XML PATH('')),4,200000)),'') AS  vcTrackingNo,'' AS  vcShippingMethod,
						 NULL AS dtDeliveryDate,ISNULL(Mst.vcOppRefOrderNo,'') AS vcOppRefOrderNo,ISNULL(Mst.numDiscountAcntType,0) AS numDiscountAcntType ,
						 isnull(numOppBizDocTempID,0) as numOppBizDocTempID,
						 com1.vcCompanyName AS EmployerOrganizationName,div1.vcComPhone as EmployerOrganizationPhone
						 ,dbo.FormatedDateFromDate(Mst.dtReleaseDate,@numDomainID) AS vcReleaseDate
						 ,dbo.FormatedDateFromDate(Mst.intpEstimatedCloseDate,@numDomainID) AS vcRequiredDate
						 ,(CASE WHEN ISNULL(Mst.numPartner,0) > 0 THEN ISNULL((SELECT CONCAT(DivisionMaster.vcPartnerCode,'-',CompanyInfo.vcCompanyName) FROM DivisionMaster INNER JOIN CompanyInfo ON DivisionMaster.numCompanyID=CompanyInfo.numCompanyId WHERE DivisionMaster.numDivisionID=Mst.numPartner),'') ELSE '' END) AS vcPartner,
						 ISNULL(Mst.vcCustomerPO#,'') vcCustomerPO#,
						 ISNULL(Mst.txtComments,'') vcSOComments,
						 ISNULL(DM.vcShippersAccountNo,'') AS vcShippersAccountNo,
						 CONVERT(VARCHAR(20),DATEADD(minute,-@ClientTimeZoneOffset,Mst.bintCreatedDate)) AS OrderCreatedDate,
						 (CASE WHEN ISNULL(OBD.numSourceBizDocId,0) > 0 THEN ISNULL((SELECT vcVendorInvoice FROM OpportunityBizDocs WHERE OpportunityBizDocs.numOppBizDocsId=OBD.numSourceBizDocId),'') ELSE ISNULL(vcVendorInvoice,'') END) vcVendorInvoice,
						 (CASE 
							WHEN ISNULL(OBD.numSourceBizDocId,0) > 0
							THEN ISNULL((SELECT vcBizDocID FROM OpportunityBizDocs WHERE OpportunityBizDocs.numOppBizDocsId=OBD.numSourceBizDocId),'')
							ELSE ''
						END) vcPackingSlip
				  FROM    OpportunityMaster Mst JOIN Domain D ON D.numDomainID = Mst.numDomainID
					LEFT JOIN OpportunityBizDocs AS OBD ON Mst.numOppid=OBD.numOppId
						  LEFT OUTER JOIN [Currency] C ON C.numCurrencyID = Mst.numCurrencyID
						  LEFT JOIN [DivisionMaster] DM ON DM.numDivisionID = Mst.numDivisionID   
						  LEFT JOIN CompanyInfo CMP ON DM.numCompanyId = CMP.numCompanyId and CMP.numDomainID = @numDomainID  
						  LEFT JOIN dbo.AdditionalContactsInformation ACI ON ACI.numContactID = Mst.numContactID
						  LEFT JOIN [BizDocTemplate] BT ON BT.numDomainID = @numDomainID AND BT.numBizDocID = @numBizDocType 
							 AND ((BT.numBizDocTempID=ISNULL(Mst.numOppBizDocTempID, 0)) OR (ISNULL(BT.bitDefault,0)=1 AND ISNULL(BT.bitEnabled,0)=1))  
						  LEFT JOIN BillingTerms BTR ON BTR.numTermsID = ISNULL(Mst.intBillingDays,0)
						  JOIN divisionmaster div1 ON D.numDivisionID = div1.numDivisionID
						  JOIN companyinfo [Com1] ON com1.numCompanyID = div1.numCompanyID
				  WHERE  OBD.numOppBizDocsId = @numReferenceID AND  Mst.numDomainID = @numDomainID
				  SET @numReferenceType=1
			      SET @numReferenceID=(SELECT TOP 1 numOppId FROM OpportunityBizDocs WHERE numOppBizDocsId = @numReferenceID)
					EXEC USP_OPPGetOppAddressDetails @numReferenceID,@numDomainId,0	
             
            END
    END

GO
--exec USP_GetMirrorBizDocItems @numReferenceID=37056,@numReferenceType=1,@numDomainID=1
--created by anoop jayaraj

-- exec USP_GetOppItemBizDocs @numBizDocID=644,@numDomainID=169,@numOppID=476291,@numOppBizDocID=0,@bitRentalBizDoc=0
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetOppItemBizDocs' ) 
    DROP PROCEDURE USP_GetOppItemBizDocs
GO
CREATE PROCEDURE USP_GetOppItemBizDocs
    @numBizDocID AS NUMERIC(9),
    @numDomainID AS NUMERIC(9),
    @numOppID AS NUMERIC(9),
    @numOppBizDocID AS NUMERIC(9),
    @bitRentalBizDoc AS BIT = 0
AS 
BEGIN

	DECLARE @tintCommitAllocation AS TINYINT
	DECLARE @bitAllocateInventoryOnPickList AS BIT

	SELECT @tintCommitAllocation=ISNULL(tintCommitAllocation,1) FROM Domain WHERE numDomainId=@numDomainID
	SELECT @bitAllocateInventoryOnPickList=ISNULL(bitAllocateInventoryOnPickList,0) FROM OpportunityMaster INNER JOIN DivisionMaster ON OpportunityMaster.numDivisionId=DivisionMaster.numDivisionID WHERE numOppId=@numOppID

	DECLARE @numSalesAuthoritativeBizDocID AS INT
	SELECT @numSalesAuthoritativeBizDocID = numAuthoritativeSales FROM [AuthoritativeBizDocs] WHERE numDomainID = @numDomainID


    IF @numOppBizDocID = 0 
        IF @bitRentalBizDoc = 1 
        BEGIN
            SELECT  *
            FROM    ( SELECT    OI.numoppitemtCode,
                                OI.numItemCode,
                                OI.vcItemName,
                                OI.vcModelID,
                                OI.numUnitHour AS QtyOrdered,
                                (OI.numUnitHour - ISNULL(SUM(OBI.numUnitHour), 0)) AS QtytoFulFilled,
                                (OI.numUnitHour - ISNULL(SUM(OBI.numUnitHour), 0)) AS QtytoFulFill,
                                ISNULL(numAllocation, 0) AS numAllocation,
                                ISNULL(numBackOrder, 0) AS numBackOrder,
                                CASE WHEN OBI.vcNotes IS NULL THEN OI.vcNotes ELSE OBI.vcNotes END AS vcNotes,
                                '' AS vcTrackingNo,
                                '' vcShippingMethod,
                                0 monShipCost,
                                GETDATE() dtDeliveryDate,
                                CONVERT(BIT, 1) AS Included,
                                ISNULL(OI.[numQtyShipped], 0) numQtyShipped,
                                ISNULL(OI.[numUnitHourReceived], 0) numQtyReceived,
                                I.[charItemType],
                                SUBSTRING(( SELECT  ',' + vcSerialNo
                                                    + CASE WHEN ISNULL(I.bitLotNo, 0) = 1 THEN ' (' + CONVERT(VARCHAR(15), oppI.numQty) + ')'
                                                            ELSE ''
                                                        END
                                            FROM    OppWarehouseSerializedItem oppI
                                                    JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID = whi.numWareHouseItmsDTLID
                                            WHERE   oppI.numOppID = @numOppID
                                                    AND oppI.numOppItemID = OI.numoppitemtCode
                                                    AND oppI.numOppBizDocsId = @numOppBizDocID
                                            ORDER BY vcSerialNo
                                            FOR
                                            XML PATH('')
                                            ), 2, 200000) AS SerialLotNo,
                                I.bitSerialized,
                                ISNULL(I.bitLotNo, 0) AS bitLotNo,
                                OI.numWarehouseItmsID,
                                ISNULL(I.numItemClassification, 0) AS numItemClassification,
                                I.vcSKU,
                                dbo.fn_GetAttributes(OI.numWarehouseItmsID,
                                                        I.bitSerialized) AS Attributes,
                                OM.bintCreatedDate AS dtRentalStartDate,
                                GETDATE() AS dtRentalReturnDate,
                                ISNULL(OI.numUOMId, 0) AS numUOM,
                                OI.monPrice AS monOppItemPrice,
                                ISNULL(OBI.monPrice, OI.monPrice) AS monOppBizDocPrice,
								OI.monTotAmount AS monOppBizDocAmount,
                                ISNULL(OI.vcItemDesc, '') AS [vcItemDesc],
								ISNULL(OI.bitDropShip,0) AS bitDropShip,
								ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate) dtReleaseDate
                        FROM      OpportunityItems OI
                                LEFT JOIN OpportunityMaster OM ON OI.numOppID = oM.numOppID
                                LEFT JOIN OpportunityBizDocs OB ON OB.numOppId = OI.numOppId
                                                                    AND OB.numBizDocId = @numBizDocID
                                LEFT JOIN OpportunityBizDocItems OBI ON OB.numOppBizDocsId = OBI.numOppBizDocId
                                                                        AND OBI.numOppItemID = OI.numoppitemtCode
                                LEFT JOIN WareHouseItems WI ON WI.numWareHouseItemID = OI.numWarehouseItmsID
                                INNER JOIN Item I ON I.[numItemCode] = OI.[numItemCode]
                        WHERE     OI.numOppId = @numOppID
                                AND OI.numoppitemtCode NOT IN (
                                SELECT  OBDI.numOppItemID
                                FROM    OpportunityBizDocs OBD
                                        JOIN OpportunityBizDocItems OBDI ON OBD.numOppBizDocsId = OBDI.numOppBizDocId
                                WHERE   OBD.numOppId = @numOppID )
                        GROUP BY  OI.numoppitemtCode,
                                OI.numItemCode,
                                OI.vcItemName,
                                OI.vcModelID,
								OI.vcNotes,
								OBI.vcNotes,
                                OI.numUnitHour,
                                numAllocation,
                                numBackOrder,
                                OI.numQtyShipped,
                                OI.[numUnitHourReceived],
                                I.[charItemType],
                                I.bitSerialized,
                                I.bitLotNo,
                                OI.numWarehouseItmsID,
                                I.numItemClassification,
                                I.vcSKU,
                                OI.numWarehouseItmsID,
                                OBI.dtRentalStartDate,
                                OBI.dtRentalReturnDate,
                                OI.numUOMId,
                                OM.bintCreatedDate,
                                OI.monPrice,
								OI.monTotAmount,
                                OBI.monPrice,
                                OI.vcItemDesc,OI.bitDropShip,OI.ItemReleaseDate,OM.dtReleaseDate
                    ) X
            WHERE   X.QtytoFulFill > 0
        END
        ELSE 
        BEGIN
            SELECT  *
            FROM    ( SELECT    OI.numoppitemtCode,
                                OI.numItemCode,
                                OI.vcItemName,
                                OI.vcModelID,
                                OI.numUnitHour AS QtyOrdered,
								CASE 
									WHEN @numBizDocId = 296 
									THEN
										(CASE 
											WHEN (I.charItemType = 'p' AND ISNULL(OI.bitDropShip, 0) = 0 AND ISNULL(bitAsset,0)=0)
											THEN 
												(CASE WHEN (OI.numUnitHour - ISNULL(SUM(OBI.numUnitHour),0)) <= dbo.GetOrderItemAvailableQtyToShip(OI.numoppitemtCode,OI.numWarehouseItmsID,ISNULL(I.bitKitParent,0),@tintCommitAllocation,@bitAllocateInventoryOnPickList) THEN (OI.numUnitHour - ISNULL(SUM(OBI.numUnitHour),0)) ELSE dbo.GetOrderItemAvailableQtyToShip(OI.numoppitemtCode,OI.numWarehouseItmsID,ISNULL(I.bitKitParent,0),@tintCommitAllocation,@bitAllocateInventoryOnPickList) END)
											ELSE (OI.numUnitHour - ISNULL(SUM(OBI.numUnitHour),0))
										END)
									ELSE
										(OI.numUnitHour - ISNULL(SUM(OBI.numUnitHour),0))
								END AS QtytoFulFilled,
                                CASE 
									WHEN @numBizDocId = 296 
									THEN
										(CASE 
											WHEN (I.charItemType = 'p' AND ISNULL(OI.bitDropShip, 0) = 0 AND ISNULL(bitAsset,0)=0)
											THEN 
												(CASE WHEN (OI.numUnitHour - ISNULL(SUM(OBI.numUnitHour),0)) <= dbo.GetOrderItemAvailableQtyToShip(OI.numoppitemtCode,OI.numWarehouseItmsID,ISNULL(I.bitKitParent,0),@tintCommitAllocation,@bitAllocateInventoryOnPickList) THEN (OI.numUnitHour - ISNULL(SUM(OBI.numUnitHour),0)) ELSE dbo.GetOrderItemAvailableQtyToShip(OI.numoppitemtCode,OI.numWarehouseItmsID,ISNULL(I.bitKitParent,0),@tintCommitAllocation,@bitAllocateInventoryOnPickList) END)
											ELSE (OI.numUnitHour - ISNULL(SUM(OBI.numUnitHour),0))
										END)
									ELSE
										(OI.numUnitHour - ISNULL(SUM(OBI.numUnitHour),0))
								END AS QtytoFulFill,
                                ISNULL(numAllocation, 0) AS numAllocation,
                                ISNULL(numBackOrder, 0) AS numBackOrder,
                                CASE WHEN OBI.vcNotes IS NULL THEN OI.vcNotes ELSE OBI.vcNotes END AS vcNotes,
                                '' AS vcTrackingNo,
                                '' vcShippingMethod,
                                0 monShipCost,
                                GETDATE() dtDeliveryDate,
                                CONVERT(BIT, 1) AS Included,
                                ISNULL(OI.[numQtyShipped], 0) numQtyShipped,
                                ISNULL(OI.[numUnitHourReceived], 0) numQtyReceived,
                                I.[charItemType],
                                SUBSTRING(( SELECT  ',' + vcSerialNo
                                                    + CASE WHEN ISNULL(I.bitLotNo, 0) = 1 THEN ' (' + CONVERT(VARCHAR(15), oppI.numQty) + ')'
                                                            ELSE ''
                                                        END
                                            FROM    OppWarehouseSerializedItem oppI
                                                    JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID = whi.numWareHouseItmsDTLID
                                            WHERE   oppI.numOppID = @numOppID
                                                    AND oppI.numOppItemID = OI.numoppitemtCode
                                                    AND oppI.numOppBizDocsId = @numOppBizDocID
                                            ORDER BY vcSerialNo
                                            FOR
                                            XML PATH('')
                                            ), 2, 200000) AS SerialLotNo,
                                I.bitSerialized,
                                ISNULL(I.bitLotNo, 0) AS bitLotNo,
                                OI.numWarehouseItmsID,
                                ISNULL(I.numItemClassification, 0) AS numItemClassification,
                                I.vcSKU,
                                dbo.fn_GetAttributes(OI.numWarehouseItmsID,
                                                        I.bitSerialized) AS Attributes,
                                OBI.dtRentalStartDate,
                                OBI.dtRentalReturnDate,
                                ISNULL(OI.numUOMId, 0) AS numUOM,
                                OI.monPrice AS monOppItemPrice,
                                ISNULL(OBI.monPrice, OI.monPrice) AS monOppBizDocPrice,
								OI.monTotAmount AS monOppBizDocAmount,
                                ISNULL(OI.vcItemDesc, '') AS [vcItemDesc],
								ISNULL(OI.bitDropShip,0) AS bitDropShip,
								ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate) dtReleaseDate
                        FROM      OpportunityItems OI
                                LEFT JOIN OpportunityMaster OM ON OI.numOppID = oM.numOppID 
                                LEFT JOIN 
									OpportunityBizDocItems OBI 
								ON 
									OBI.numOppItemID = OI.numoppitemtCode 
                                    AND OBI.numOppBizDocId IN ( SELECT  
																	numOppBizDocsId
                                                                FROM    
																	OpportunityBizDocs OB
                                                                WHERE   
																	OB.numOppId = OI.numOppId
                                                                    AND 1 = (CASE 
																				WHEN @numBizDocId = @numSalesAuthoritativeBizDocID --Invoice OR Deferred Income
																				THEN 
																					(CASE WHEN numBizDocId = 304 OR (ISNULL(OB.bitAuthoritativeBizDocs,0) = 1 AND ISNULL(OB.numDeferredBizDocID,0) = 0) THEN 1 ELSE 0 END)
																				WHEN  @numBizDocId = 304
																				THEN (CASE WHEN ISNULL(OB.bitAuthoritativeBizDocs,0) = 1 THEN 1 ELSE 0 END)
																				ELSE 
																					(CASE WHEN numBizDocId = @numBizDocId THEN 1 ELSE 0 END) 
																				END) 
																)
                                LEFT JOIN WareHouseItems WI ON WI.numWareHouseItemID = OI.numWarehouseItmsID
                                INNER JOIN Item I ON I.[numItemCode] = OI.[numItemCode]
                        WHERE     OI.numOppId = @numOppID
                        GROUP BY  OI.numoppitemtCode,
                                OI.numItemCode,
                                OI.vcItemName,
                                OI.vcModelID,
								OI.vcNotes,
								OBI.vcNotes,
                                OI.numUnitHour,
                                numAllocation,
                                numBackOrder,
                                OI.numQtyShipped,
                                OI.[numUnitHourReceived],
                                I.[charItemType],
                                I.bitSerialized,
                                I.bitLotNo,
								I.bitAsset,
								I.bitKitParent,
                                OI.numWarehouseItmsID,
                                I.numItemClassification,
                                I.vcSKU,
                                OI.numWarehouseItmsID,
                                OBI.dtRentalStartDate,
                                OBI.dtRentalReturnDate,
                                OI.numUOMId,
                                OI.monPrice,
								OI.monTotAmount,
                                OBI.monPrice,
                                OI.vcItemDesc,OI.bitDropShip,OI.ItemReleaseDate,OM.dtReleaseDate
                    ) X
            WHERE   X.QtytoFulFill > 0
        END
    ELSE 
        IF @bitRentalBizDoc = 1 
        BEGIN
            SELECT  OI.numoppitemtCode,
                    OI.numItemCode,
                    OI.vcItemName,
                    OI.vcModelID,
                    OI.numUnitHour AS QtyOrdered,
                    OBI.numUnitHour AS QtytoFulFilled,
                    ( OI.numUnitHour - ISNULL(SUM(OBI.numUnitHour), 0) ) AS QtytoFulFill,
                    ISNULL(numAllocation, 0) AS numAllocation,
                    ISNULL(numBackOrder, 0) AS numBackOrder,
                    CASE WHEN OBI.vcNotes IS NULL THEN OI.vcNotes ELSE OBI.vcNotes END AS vcNotes,
                    ISNULL(OBI.vcTrackingNo, '') AS vcTrackingNo,
                    OBI.vcShippingMethod,
                    OBI.monShipCost,
                    OBI.dtDeliveryDate,
                    CONVERT(BIT, 1) AS Included,
                    ISNULL(OI.[numQtyShipped], 0) numQtyShipped,
                    ISNULL(OI.[numUnitHourReceived], 0) numQtyReceived,
                    I.[charItemType],
                    SUBSTRING(( SELECT  ',' + vcSerialNo
                                        + CASE WHEN ISNULL(I.bitLotNo, 0) = 1
                                                THEN ' ('
                                                    + CONVERT(VARCHAR(15), oppI.numQty)
                                                    + ')'
                                                ELSE ''
                                            END
                                FROM    OppWarehouseSerializedItem oppI
                                        JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID = whi.numWareHouseItmsDTLID
                                WHERE   oppI.numOppID = @numOppID
                                        AND oppI.numOppItemID = OI.numoppitemtCode
                                        AND oppI.numOppBizDocsId = @numOppBizDocID
                                ORDER BY vcSerialNo
                                FOR
                                XML PATH('')
                                ), 2, 200000) AS SerialLotNo,
                    I.bitSerialized,
                    ISNULL(I.bitLotNo, 0) AS bitLotNo,
                    OI.numWarehouseItmsID,
                    ISNULL(I.numItemClassification, 0) AS numItemClassification,
                    I.vcSKU,
                    dbo.fn_GetAttributes(OI.numWarehouseItmsID,
                                            I.bitSerialized) AS Attributes,
                    OBI.dtRentalStartDate,
                    OBI.dtRentalReturnDate,
                    ISNULL(OI.numUOMId, 0) AS numUOM,
                    OI.monPrice AS monOppItemPrice,
                    ISNULL(OBI.monPrice, OI.monPrice) AS monOppBizDocPrice,
					OI.monTotAmount AS monOppBizDocAmount,
                    ISNULL(OI.vcItemDesc, '') AS [vcItemDesc],
					ISNULL(OI.bitDropShip,0) AS bitDropShip,
					ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate) dtReleaseDate
            FROM    OpportunityItems OI
                    LEFT JOIN OpportunityMaster OM ON OI.numOppID = oM.numOppID
                    JOIN OpportunityBizDocs OB ON OB.numOppId = OI.numOppId
                    JOIN OpportunityBizDocItems OBI ON OB.numOppBizDocsId = OBI.numOppBizDocId
                                                        AND OBI.numOppItemID = OI.numoppitemtCode
                    LEFT JOIN WareHouseItems WI ON WI.numWareHouseItemID = OI.numWarehouseItmsID
                    INNER JOIN Item I ON I.[numItemCode] = OI.[numItemCode]
            WHERE   OI.numOppId = @numOppID
                    AND OB.numOppBizDocsId = @numOppBizDocID
            GROUP BY OI.numoppitemtCode,
                    OI.numItemCode,
                    OI.vcItemName,
                    OI.vcModelID,
					OI.vcNotes,
                    OI.numUnitHour,
                    numAllocation,
                    numBackOrder,
                    OBI.vcNotes,
                    OBI.vcTrackingNo,
                    OBI.vcShippingMethod,
                    OBI.monShipCost,
                    OBI.dtDeliveryDate,
                    OBI.numUnitHour,
                    OI.[numQtyShipped],
                    OI.[numUnitHourReceived],
                    I.[charItemType],
                    I.bitSerialized,
                    I.bitLotNo,
                    OI.numWarehouseItmsID,
                    I.numItemClassification,
                    I.vcSKU,
                    OI.numWarehouseItmsID,
                    OBI.dtRentalStartDate,
                    OBI.dtRentalReturnDate,
                    OI.numUOMId,
                    OI.monPrice,
					OI.monTotAmount,
                    OBI.monPrice,
                    OI.vcItemDesc,OI.bitDropShip,OI.ItemReleaseDate,OM.dtReleaseDate
        END
        ELSE 
        BEGIN
            SELECT  OI.numoppitemtCode,
                    OI.numItemCode,
                    OI.vcItemName,
                    OI.vcModelID,
                    OI.numUnitHour AS QtyOrdered,
                    OBI.numUnitHour AS QtytoFulFilled,
                    (OI.numUnitHour - ISNULL(SUM(OBI.numUnitHour), 0) - ISNULL(TEMPDeferredIncomeItem.numUnitHour,0)) AS QtytoFulFill,
                    ISNULL(numAllocation, 0) AS numAllocation,
                    ISNULL(numBackOrder, 0) AS numBackOrder,
                    CASE WHEN OBI.vcNotes IS NULL THEN OI.vcNotes ELSE OBI.vcNotes END AS vcNotes,
                    ISNULL(OBI.vcTrackingNo, '') AS vcTrackingNo,
                    OBI.vcShippingMethod,
                    OBI.monShipCost,
                    OBI.dtDeliveryDate,
                    CONVERT(BIT, 1) AS Included,
                    ISNULL(OI.[numQtyShipped], 0) numQtyShipped,
                    ISNULL(OI.[numUnitHourReceived], 0) numQtyReceived,
                    I.[charItemType],
                    SUBSTRING(( SELECT  ',' + vcSerialNo
                                        + CASE WHEN ISNULL(I.bitLotNo, 0) = 1
                                                THEN ' ('
                                                    + CONVERT(VARCHAR(15), oppI.numQty)
                                                    + ')'
                                                ELSE ''
                                            END
                                FROM    OppWarehouseSerializedItem oppI
                                        JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID = whi.numWareHouseItmsDTLID
                                WHERE   oppI.numOppID = @numOppID
                                        AND oppI.numOppItemID = OI.numoppitemtCode
                                        AND oppI.numOppBizDocsId = @numOppBizDocID
                                ORDER BY vcSerialNo
                                FOR
                                XML PATH('')
                                ), 2, 200000) AS SerialLotNo,
                    I.bitSerialized,
                    ISNULL(I.bitLotNo, 0) AS bitLotNo,
                    OI.numWarehouseItmsID,
                    ISNULL(I.numItemClassification, 0) AS numItemClassification,
                    I.vcSKU,
                    dbo.fn_GetAttributes(OI.numWarehouseItmsID,
                                            I.bitSerialized) AS Attributes,
                    OBI.dtRentalStartDate,
                    OBI.dtRentalReturnDate,
                    ISNULL(OI.numUOMId, 0) AS numUOM,
                    OI.monPrice AS monOppItemPrice,
                    ISNULL(OBI.monPrice, OI.monPrice) AS monOppBizDocPrice,
					OI.monTotAmount AS monOppBizDocAmount,
                    ISNULL(OI.vcItemDesc, '') AS [vcItemDesc],
					ISNULL(OI.bitDropShip,0) AS bitDropShip,
					ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate) dtReleaseDate
            FROM    OpportunityItems OI
                    LEFT JOIN OpportunityMaster OM ON OI.numOppID = oM.numOppID
                    JOIN OpportunityBizDocs OB ON OB.numOppId = OI.numOppId
                    JOIN OpportunityBizDocItems OBI ON OB.numOppBizDocsId = OBI.numOppBizDocId
                                                        AND OBI.numOppItemID = OI.numoppitemtCode
                    LEFT JOIN WareHouseItems WI ON WI.numWareHouseItemID = OI.numWarehouseItmsID
                    INNER JOIN Item I ON I.[numItemCode] = OI.[numItemCode]
					OUTER APPLY
					(
						SELECT
							SUM(OpportunityBizDocItems.numUnitHour) AS numUnitHour
						FROM 
							OpportunityBizDocs
						INNER JOIN
							OpportunityBizDocItems 
						ON
							OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
						WHERE
							numOppId = @numOppID
							AND numOppBizDocID <> @numOppBizDocID
							AND	1 = (CASE 
									WHEN @numBizDocID = 304 --DEFERRED BIZDOC OR INVOICE
									THEN (CASE WHEN (numBizDocId = @numSalesAuthoritativeBizDocID OR numBizDocId=304) THEN 1 ELSE 0 END) -- WHEN DEFERRED BIZDOC(304) IS BEING ADDED SUBTRACT ITEMS ADDED TO INVOICE AND OTHER DEFERRED BIZDOC IN QtytoFulFill
									ELSE (CASE WHEN numBizDocId = @numBizDocID THEN 1 ELSE 0 END)
									END) 
							AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
					) TEMPDeferredIncomeItem
            WHERE   OI.numOppId = @numOppID
                    AND OB.numOppBizDocsId = @numOppBizDocID
            GROUP BY OI.numoppitemtCode,
                    OI.numItemCode,
                    OI.vcItemName,
                    OI.vcModelID,
					OI.vcNotes,
                    OI.numUnitHour,
                    numAllocation,
                    numBackOrder,
                    OBI.vcNotes,
                    OBI.vcTrackingNo,
                    OBI.vcShippingMethod,
                    OBI.monShipCost,
                    OBI.dtDeliveryDate,
                    OBI.numUnitHour,
                    OI.[numQtyShipped],
                    OI.[numUnitHourReceived],
                    I.[charItemType],
                    I.bitSerialized,
                    I.bitLotNo,
                    OI.numWarehouseItmsID,
                    I.numItemClassification,
                    I.vcSKU,
                    OI.numWarehouseItmsID,
                    OBI.dtRentalStartDate,
                    OBI.dtRentalReturnDate,
                    OI.numUOMId,
                    OI.monPrice,
					OI.monTotAmount,
                    OBI.monPrice,
                    OI.vcItemDesc,OI.bitDropShip,OI.ItemReleaseDate,OM.dtReleaseDate,
					TEMPDeferredIncomeItem.numUnitHour
            UNION
            SELECT  *
            FROM    ( SELECT    OI.numoppitemtCode,
                                OI.numItemCode,
                                OI.vcItemName,
                                OI.vcModelID,
                                OI.numUnitHour AS QtyOrdered,
                                ( OI.numUnitHour
                                    - ISNULL(SUM(OBI.numUnitHour), 0) ) AS QtytoFulFilled,
                                ( OI.numUnitHour
                                    - ISNULL(SUM(OBI.numUnitHour), 0) ) AS QtytoFulFill,
                                ISNULL(numAllocation, 0) AS numAllocation,
                                ISNULL(numBackOrder, 0) AS numBackOrder,
                                CASE WHEN OBI.vcNotes IS NULL THEN OI.vcNotes ELSE OBI.vcNotes END AS vcNotes,
                                '' AS vcTrackingNo,
                                '' vcShippingMethod,
                                1 AS monShipCost,
                                NULL dtDeliveryDate,
                                CONVERT(BIT, 0) AS Included,
                                ISNULL(OI.[numQtyShipped], 0) numQtyShipped,
                                ISNULL(OI.[numUnitHourReceived], 0) numQtyReceived,
                                I.[charItemType],
                                SUBSTRING(( SELECT  ',' + vcSerialNo
                                                    + CASE WHEN ISNULL(I.bitLotNo, 0) = 1 THEN ' (' + CONVERT(VARCHAR(15), oppI.numQty) + ')'
                                                            ELSE ''
                                                        END
                                            FROM    OppWarehouseSerializedItem oppI
                                                    JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID = whi.numWareHouseItmsDTLID
                                            WHERE   oppI.numOppID = @numOppID
                                                    AND oppI.numOppItemID = OI.numoppitemtCode
                                                    AND oppI.numOppBizDocsId = @numOppBizDocID
                                            ORDER BY vcSerialNo
                                            FOR
                                            XML PATH('')
                                            ), 2, 200000) AS SerialLotNo,
                                I.bitSerialized,
                                ISNULL(I.bitLotNo, 0) AS bitLotNo,
                                OI.numWarehouseItmsID,
                                ISNULL(I.numItemClassification, 0) AS numItemClassification,
                                I.vcSKU,
                                dbo.fn_GetAttributes(OI.numWarehouseItmsID,
                                                        I.bitSerialized) AS Attributes,
                                OBI.dtRentalStartDate,
                                OBI.dtRentalReturnDate,
                                ISNULL(OI.numUOMId, 0) AS numUOM,
                                OI.monPrice AS monOppItemPrice,
                                ISNULL(OBI.monPrice, OI.monPrice) AS monOppBizDocPrice,
								OI.monTotAmount AS monOppBizDocAmount,
                                ISNULL(OI.vcItemDesc, '') AS [vcItemDesc],
								ISNULL(OI.bitDropShip,0) AS bitDropShip,
								ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate) dtReleaseDate
                        FROM      OpportunityItems OI
                                LEFT JOIN OpportunityMaster OM ON OI.numOppID = oM.numOppID
                                LEFT JOIN OpportunityBizDocs OB ON OB.numOppId = OI.numOppId
                                                                    AND OB.numBizDocId = @numBizDocID
                                LEFT JOIN OpportunityBizDocItems OBI ON OB.numOppBizDocsId = OBI.numOppBizDocId
                                                                        AND OBI.numOppItemID = OI.numoppitemtCode
                                LEFT JOIN WareHouseItems WI ON WI.numWareHouseItemID = OI.numWarehouseItmsID
                                INNER JOIN Item I ON I.[numItemCode] = OI.[numItemCode]
                        WHERE     OI.numOppId = @numOppID
                                AND OI.numoppitemtCode NOT IN (
                                    SELECT  numOppItemID
                                    FROM    OpportunityBizDocItems OBI2
                                            JOIN OpportunityBizDocs OB2 ON OBI2.numOppBizDocID = OB2.numOppBizDocsId
                                    WHERE   numOppID = @numOppID
                                            AND numBizDocId = @numBizDocID )
                        GROUP BY  OI.numoppitemtCode,
                                OI.numItemCode,
                                OI.vcItemName,
                                OI.vcModelID,
								OI.vcNotes,
								OBI.vcNotes,
                                OI.numUnitHour,
                                numAllocation,
                                numBackOrder,
                                OBI.numUnitHour,
                                OI.[numQtyShipped],
                                OI.[numUnitHourReceived],
                                I.[charItemType],
                                I.bitSerialized,
                                I.bitLotNo,
                                OI.numWarehouseItmsID,
                                I.numItemClassification,
                                I.vcSKU,
                                OI.numWarehouseItmsID,
                                OBI.dtRentalStartDate,
                                OBI.dtRentalReturnDate,
                                OI.numUOMId,
                                OI.monPrice,
								OI.monTotAmount,
                                OBI.monPrice,
                                OI.vcItemDesc,OI.bitDropShip,OI.ItemReleaseDate,OM.dtReleaseDate
                    ) X
            WHERE   X.QtytoFulFill > 0
        END
END



GO
/****** Object:  StoredProcedure [dbo].[USP_GetOpportunityList1]    Script Date: 03/06/2009 00:37:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- created by anoop jayaraj                                                               
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getopportunitylist1')
DROP PROCEDURE usp_getopportunitylist1
GO
CREATE PROCEDURE [dbo].[USP_GetOpportunityList1]                                                                    
	@numUserCntID numeric(9)=0,
	@numDomainID numeric(9)=0,
	@tintUserRightType tinyint=0,
	@tintSortOrder tinyint=4,
	@dtLastDate datetime,
	@OppType as tinyint,
	@CurrentPage int,
	@PageSize int,
	@columnName as Varchar(MAX),
	@columnSortOrder as Varchar(10),
	@numDivisionID as numeric(9)=0,
	@bitPartner as bit=0,
	@ClientTimeZoneOffset as int,
	@inttype as tinyint,
	@vcRegularSearchCriteria varchar(MAX)='',
	@vcCustomSearchCriteria varchar(MAX)='',
	@SearchText VARCHAR(300) = '',
	@SortChar char(1)='0',
	@tintDashboardReminderType TINYINT = 0,
	@tintOppStatus TINYINT = 0                                                           
AS                
BEGIN
         
	DECLARE @PageId as TINYINT = 0
	DECLARE @numFormId  AS INT
           
	IF @inttype = 1 
	BEGIN           
		SET @PageId =2  
		SET @numFormId=38          
	END
	ELSE IF @inttype = 2            
	BEGIN
		SET @PageId =6 
		SET @numFormId=40           
	END

	CREATE TABLE #tempForm 
	(
		tintOrder TINYINT,vcDbColumnName nvarchar(200),vcOrigDbColumnName nvarchar(50),vcFieldName nvarchar(50),vcAssociatedControlType nvarchar(50),vcListItemType varchar(3),
		numListID numeric(9),vcLookBackTableName varchar(50),bitCustomField BIT,numFieldId NUMERIC,bitAllowSorting BIT,bitAllowEdit BIT,bitIsRequired BIT,bitIsEmail BIT,
		bitIsAlphaNumeric BIT,bitIsNumeric BIT,bitIsLengthValidation BIT,intMaxLength int,intMinLength int,bitFieldMessage BIT,vcFieldMessage VARCHAR(500),ListRelID numeric(9),
		intColumnWidth int,bitAllowFiltering BIT,vcFieldDataType CHAR(1)
	)

	-- FIRST CHECK IF GRID COLUMNS ARE CONFIGURED
	DECLARE @Nocolumns AS TINYINT = 0  

	SELECT 
		@Nocolumns = ISNULL(SUM(TotalRow),0)  
	FROM
	(            
		SELECT 
			COUNT(*) TotalRow 
		FROM 
			View_DynamicColumns 
		WHERE 
			numFormId=@numFormId 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND numRelCntType = @numFormId
		UNION 
		SELECT 
			COUNT(*) TotalRow 
		FROM 
			View_DynamicCustomColumns 
		WHERE 
			numFormId=@numFormId 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND numRelCntType = @numFormId
	) TotalRows
	
	DECLARE @IsMasterConfAvailable BIT = 0
	DECLARE @numUserGroup NUMERIC(18,0)

	-- IF NoColumns=0 CHECK IF MASTER FORM CONFIGURATION IS CREATED BY ADMINISTRATOR
	IF @Nocolumns=0
	BEGIN
		SELECT @numUserGroup = numGroupID FROM UserMaster WHERE numUserDetailId = @numUserCntID 

		IF (SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = @numDomainID AND numFormID = @numFormId AND numRelCntType=@numFormId AND bitGridConfiguration = 1 AND numGroupID = @numUserGroup) > 0
		BEGIN
			SET @IsMasterConfAvailable = 1
		END
	END

	--IF MASTERCONFIGURATION IS AVAILABLE THEN LOAD IT OTHERWISE LOAD DEFAULT COLUMNS
	IF @IsMasterConfAvailable = 1
	BEGIN
		INSERT INTO DycFormConfigurationDetails 
		(
			numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth
		)
		SELECT 
			@numFormId,numFieldId,intColumnNum,intRowNum,@numDomainID,@numUserCntID,@numFormId,1,0,intColumnWidth
		FROM 
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId=@numFormId AND 
			View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicColumnsMasterConfig.numRelCntType=@numFormId AND 
			View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
			ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
		UNION
		SELECT 
			@numFormId,numFieldId,intColumnNum,intRowNum,@numDomainID,@numUserCntID,@numFormId,1,1,intColumnWidth
		FROM 
			View_DynamicCustomColumnsMasterConfig
		WHERE 
			View_DynamicCustomColumnsMasterConfig.numFormId=@numFormId AND 
			View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicCustomColumnsMasterConfig.numRelCntType=@numFormId AND 
			View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1

		INSERT INTO 
			#tempForm
		SELECT 
			(intRowNum + 1) as tintOrder, vcDbColumnName,	vcOrigDbColumnName,	ISNULL(vcCultureFieldName,vcFieldName), vcAssociatedControlType,
			vcListItemType, numListID, vcLookBackTableName,	bitCustom, numFieldId, bitAllowSorting,	bitInlineEdit, bitIsRequired,
			bitIsEmail,	bitIsAlphaNumeric, bitIsNumeric, bitIsLengthValidation,	intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,
			ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType 
		FROM 
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId=@numFormId AND 
			View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicColumnsMasterConfig.numRelCntType=@numFormId AND 
			View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
			ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
		UNION
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName, vcAssociatedControlType,'' as vcListItemType,numListID,'',
			bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,''
		FROM 
			View_DynamicCustomColumnsMasterConfig
		WHERE 
			View_DynamicCustomColumnsMasterConfig.numFormId=@numFormId AND 
			View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicCustomColumnsMasterConfig.numRelCntType=@numFormId AND 
			View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1
		ORDER BY 
			tintOrder ASC  
	END
	ELSE                                        
	BEGIN 
		IF @Nocolumns=0
		BEGIN
			INSERT INTO DycFormConfigurationDetails 
			(
				numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth
			)
			SELECT 
				@numFormId,numFieldId,0,Row_number() over(order by tintRow desc),@numDomainID,@numUserCntID,@numFormId,1,0,intColumnWidth
			FROM 
				View_DynamicDefaultColumns
			WHERE 
				numFormId=@numFormId 
				AND bitDefault=1 
				AND ISNULL(bitSettingField,0)=1 
				AND numDomainID=@numDomainID
			ORDER BY 
				tintOrder ASC  
		END
  

		INSERT INTO 
			#tempForm
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcOrigDbColumnName,ISNULL(vcCultureFieldName,vcFieldName),vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName,
			bitCustom,numFieldId,bitAllowSorting,bitInlineEdit,bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength,bitFieldMessage,
			vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType  
		FROM 
			View_DynamicColumns 
		WHERE 
			numFormId=@numFormId 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND ISNULL(bitSettingField,0)=1 
			AND ISNULL(bitCustom,0)=0  
			AND numRelCntType = @numFormId
		UNION
		SELECT 
			tintRow+1 AS tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName,vcAssociatedControlType,'' as vcListItemType,numListID,'',bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,'' 
		FROM 
			View_DynamicCustomColumns
		WHERE 
			numFormId=@numFormId 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND ISNULL(bitCustom,0)=1 
			AND numRelCntType = @numFormId
		ORDER BY 
			tintOrder ASC             
	END
	
	DECLARE  @strColumns AS VARCHAR(MAX)
	SET @strColumns = ' ADC.numContactId, DM.numDivisionID, ISNULL(DM.numTerID,0) as numTerID, Opp.numRecOwner, DM.tintCRMType, Opp.numOppID, ISNULL(Opp.numBusinessProcessID,0) AS numBusinessProcessID,ADC.vcFirstName+'' ''+ADC.vcLastName AS vcContactName,ADC.numPhone AS numContactPhone,ADC.numPhoneExtension AS numContactPhoneExtension,ADC.vcEmail AS vcContactEmail '
        
	
	DECLARE @tintOrder as tinyint  = 0                                                  
	DECLARE @vcFieldName as varchar(50)                                                      
	DECLARE @vcListItemType as varchar(3)                                                 
	DECLARE @vcListItemType1 as varchar(1)                                                     
	DECLARE @vcAssociatedControlType varchar(10)                                                      
	DECLARE @numListID AS numeric(9)                                                      
	DECLARE @vcDbColumnName varchar(200)                          
	DECLARE @WhereCondition varchar(2000) = ''                          
	DECLARE @vcLookBackTableName varchar(2000)                    
	DECLARE @bitCustom as bit              
	DECLARE @numFieldId as numeric  
	DECLARE @bitAllowSorting AS CHAR(1)   
	DECLARE @bitAllowEdit AS CHAR(1)                 
	DECLARE @vcColumnName AS VARCHAR(500)                  
	Declare @ListRelID AS NUMERIC(9) 
	DECLARE @SearchQuery AS VARCHAR(MAX) = ''   

	SELECT TOP 1
		@tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName, @vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,
		@numListID=numListID,@vcLookBackTableName=vcLookBackTableName,@bitCustom=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,@bitAllowEdit=bitAllowEdit,
		@ListRelID=ListRelID
	FROM 
		#tempForm 
	ORDER BY 
		tintOrder asc            
                                                   
	WHILE @tintOrder>0                                                      
	BEGIN 
		IF @bitCustom = 0            
		BEGIN 
			DECLARE @Prefix as varchar(5)                                               
			
			IF @vcLookBackTableName = 'AdditionalContactsInformation'                    
				SET @Prefix = 'ADC.'                    
			IF @vcLookBackTableName = 'DivisionMaster'                    
				SET @Prefix = 'DM.'                    
			IF @vcLookBackTableName = 'OpportunityMaster'                    
				SET @PreFix ='Opp.'      
			IF @vcLookBackTableName = 'OpportunityRecurring'
				SET @PreFix = 'OPR.'
			IF @vcLookBackTableName = 'CompanyInfo'
				SET @PreFix = 'cmp.'
			IF @vcLookBackTableName = 'ProjectProgress'
				SET @PreFix = 'PP'

			SET @vcColumnName= @vcDbColumnName+'~'+ convert(varchar(10),@numFieldId) +'~0'
      
			IF @vcAssociatedControlType='SelectBox'                                                      
			BEGIN  
				IF @vcDbColumnName = 'numStatus'
				BEGIN
					SET @strColumns=@strColumns+ ' ,(SELECT COUNT(OpportunityItems.numoppitemtCode) FROM OpportunityItems 
					WHERE OpportunityItems.bitItemPriceApprovalRequired=1 AND OpportunityItems.numOppId=Opp.numOppId) ApprovalMarginCount,ISNULL(Opp.intPromotionApprovalStatus,0) AS intPromotionApprovalStatus '
				END
				
				IF @vcDbColumnName='numPartner'
				BEGIN
					SET @strColumns=@strColumns+ ' ,(Opp.numPartner) '+' ['+ @vcColumnName+']'    
					SET @strColumns=@strColumns+ ' ,(SELECT D.vcPartnerCode+''-''+C.vcCompanyName FROM DivisionMaster AS D LEFT JOIN CompanyInfo AS C ON D.numCompanyID=C.numCompanyID WHERE D.numDivisionID=Opp.numPartner) AS vcPartner' 
				END
				ELSE IF @vcDbColumnName = 'numPartnerContact'
				BEGIN
					SET @strColumns=@strColumns+ ' ,(Opp.numPartenerContact) '+' ['+ @vcColumnName+']'    
					SET @strColumns=@strColumns+ ' ,(SELECT A.vcFirstName+'' ''+ A.vcLastName FROM AdditionalContactsInformation AS A WHERE A.numContactId=Opp.numPartenerContact) AS vcPartnerContact' 
				END
				ELSE IF @vcDbColumnName = 'tintSource'
				BEGIN
					SET @strColumns = @strColumns + ',dbo.fn_GetOpportunitySourceValue(ISNULL(Opp.tintSource,0),ISNULL(Opp.tintSourceType,0),Opp.numDomainID) ' + ' [' + @vcColumnName + ']'

					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'dbo.fn_GetOpportunitySourceValue(ISNULL(Opp.tintSource,0),ISNULL(Opp.tintSourceType,0),Opp.numDomainID) LIKE ''%' + @SearchText + '%'''
					END
				END
				ELSE IF @vcDbColumnName = 'tintOppStatus'
				BEGIN
					SET @strColumns=@strColumns+ ' ,(CASE ISNULL(Opp.tintOppStatus,0) WHEN 1 THEN ''Deal Won'' WHEN 2 THEN ''Deal Lost'' ELSE ''Open'' END)' + ' [' + @vcColumnName + ']'
				END
				ELSE IF @vcDbColumnName = 'numContactId'
				BEGIN
					SET @strColumns = @strColumns + ',dbo.fn_GetContactName('+@Prefix+@vcDbColumnName+') ['+ @vcColumnName+']'     
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'dbo.fn_GetContactName('+@Prefix+@vcDbColumnName+') LIKE ''%' + @SearchText + '%'''
					END             
				END
				ELSE if @vcListItemType='LI'                                                       
				BEGIN
					SET @strColumns = @strColumns+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName+']'                                                      
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'L' + CONVERT(VARCHAR(3),@tintOrder) + '.vcData LIKE ''%' + @SearchText + '%'''
					END

					SET @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID='+@vcDbColumnName                                                      
				END                                                      
				ELSE IF @vcListItemType='S'                                                       
				BEGIN
					SET @strColumns = @strColumns+',S'+ convert(varchar(3),@tintOrder)+'.vcState'+' ['+ @vcColumnName+']'   
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'S'+ convert(varchar(3),@tintOrder)+'.vcState LIKE ''%' + @SearchText + '%'''
					END
					                                                   
					SET @WhereCondition= @WhereCondition +' left join State S'+ convert(varchar(3),@tintOrder)+ ' on S'+convert(varchar(3),@tintOrder)+ '.numStateID='+@vcDbColumnName                                                      
				END     
				ELSE IF @vcListItemType = 'BP'
				BEGIN
					SET @strColumns = @strColumns + ',SPLM.Slp_Name' + ' ['	+ @vcColumnName + ']'

					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'SPLM.Slp_Name LIKE ''%' + @SearchText + '%'''
					END

					SET @WhereCondition = @WhereCondition + ' LEFT JOIN Sales_process_List_Master SPLM on SPLM.Slp_Id = OPP.numBusinessProcessID '
				END
				ELSE IF @vcListItemType='PP'
				BEGIN
					SET @strColumns = @strColumns+', CASE WHEN PP.numOppID >0 then ISNULL(PP.intTotalProgress,0) ELSE dbo.GetListIemName(OPP.numPercentageComplete) END '+' ['+ @vcColumnName+']'

					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + '(CASE WHEN PP.numOppID >0 then CONCAT(ISNULL(PP.intTotalProgress,0),''%'') ELSE dbo.GetListIemName(OPP.numPercentageComplete) END) LIKE ''%' + @SearchText + '%'''
					END
				END  
				ELSE IF @vcListItemType='T'                                                       
				BEGIN
					SET @strColumns = @strColumns+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName+']' 
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'L'+ convert(varchar(3),@tintOrder)+'.vcData LIKE ''%' + @SearchText + '%'''
					END
					                                                     
					SET @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID='+@vcDbColumnName                                                      
				END                     
				ELSE IF @vcListItemType='U'                                                   
				BEGIN
					SET @strColumns = @strColumns + ',dbo.fn_GetContactName('+@Prefix+@vcDbColumnName+') ['+ @vcColumnName+']'     
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'dbo.fn_GetContactName('+@Prefix+@vcDbColumnName+') LIKE ''%' + @SearchText + '%'''
					END              
				END  
				ELSE IF @vcListItemType = 'WI'
				BEGIN
					SET @strColumns = @strColumns + ', Case when isnull(OL.numSiteID,0)>0 then Sites.vcSiteName else WebAPI.vcProviderName end [' + @vcColumnName + ']'
				  
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + '(Case when isnull(OL.numSiteID,0)>0 then Sites.vcSiteName else WebAPI.vcProviderName end) LIKE ''%' + @SearchText + '%'''
					END 

					SET @WhereCondition= @WhereCondition +' left join WebAPI on WebAPI.WebApiId=isnull(OL.numWebApiId,0)
															left join Sites on isnull(OL.numSiteID,0)=Sites.numSIteID and Sites.numDomainID=' + convert(varchar(15),@numDomainID)                                                    
			   END                  
			END               
			ELSE IF @vcAssociatedControlType='DateField'                                                      
			BEGIN  
				SET @strColumns=@strColumns+',case when convert(varchar(11),'+@Prefix+@vcDbColumnName+')= convert(varchar(11),getdate()) then ''<b><font color=red>Today</font></b>'''      
				SET @strColumns=@strColumns+'when convert(varchar(11),'+@Prefix+@vcDbColumnName+')= convert(varchar(11),dateadd(day,-1,getdate())) then ''<b><font color=purple>YesterDay</font></b>'''      
				SET @strColumns=@strColumns+'when convert(varchar(11),'+@Prefix+@vcDbColumnName+')= convert(varchar(11),dateadd(day,1,getdate())) then''<b><font color=orange>Tommorow</font></b>'' '      
				SET @strColumns=@strColumns+'else dbo.FormatedDateFromDate('+@Prefix+@vcDbColumnName+','+convert(varchar(10),@numDomainId)+') end  ['+ @vcColumnName+']'               

				IF LEN(@SearchText) > 0
				BEGIN
					SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'dbo.FormatedDateFromDate('+@Prefix+@vcDbColumnName+','+ CONVERT(VARCHAR(10),@numDomainId)+') LIKE ''%' + @SearchText + '%'''
				END
			END              
			ELSE IF @vcAssociatedControlType='TextBox'                                                      
			BEGIN  
				IF @vcDbColumnName = 'vcCompactContactDetails'
				BEGIN
					SET @strColumns=@strColumns+ ' ,'''' AS vcCompactContactDetails'   
				END 
				ELSE
				BEGIN
				SET @strColumns = @strColumns + ','+ CASE 
														WHEN @vcDbColumnName='numAge' then 'year(getutcdate())-year(bintDOB)'                   
														WHEN @vcDbColumnName='monPAmount' then 'isnull((SELECT varCurrSymbol FROM [Currency] WHERE numCurrencyID = Opp.[numCurrencyID]),'''') + '' ''+ CONVERT(VARCHAR,CONVERT(DECIMAL(20,5),monPAmount),1)'
														WHEN @vcDbColumnName='numDivisionID' then 'DM.numDivisionID'
														WHEN @vcDbColumnName = 'tintOppStatus' THEN 'CASE tintOppStatus WHEN 0 then ''Open'' WHEN 1 THEN ''Closed-Won'' WHEN 2 THEN ''Closed-Lost'' END '
														WHEN @vcDbColumnName = 'tintOppType' THEN 'CASE tintOppType WHEN 1 then ''Sales'' WHEN 2 THEN ''Purchase'' Else ''NA'' END ' 
														ELSE @Prefix + @vcDbColumnName 
													END +' ['+ @vcColumnName+']'  
				END
				
				IF LEN(@SearchText) > 0
				BEGIN
					SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + CASE 
														WHEN @vcDbColumnName='numAge' then 'year(getutcdate())-year(bintDOB)'                   
														WHEN @vcDbColumnName='monPAmount' then 'isnull((SELECT varCurrSymbol FROM [Currency] WHERE numCurrencyID = Opp.[numCurrencyID]),'''') + '' ''+ CONVERT(VARCHAR,CONVERT(DECIMAL(20,5),monPAmount),1)'
														WHEN @vcDbColumnName='numDivisionID' then 'DM.numDivisionID'
														WHEN @vcDbColumnName = 'tintOppStatus' THEN 'CASE tintOppStatus WHEN 0 then ''Open'' WHEN 1 THEN ''Closed-Won'' WHEN 2 THEN ''Closed-Lost'' END '
														WHEN @vcDbColumnName = 'tintOppType' THEN 'CASE tintOppType WHEN 1 then ''Sales'' WHEN 2 THEN ''Purchase'' Else ''NA'' END ' 
														ELSE @Prefix + @vcDbColumnName 
													END + ' LIKE ''%' + @SearchText + '%'''
				END              
			END  
			ELSE IF @vcAssociatedControlType='TextArea'                                              
			BEGIN  
				SET @strColumns=@strColumns+','+  @Prefix + @vcDbColumnName +' ['+ @vcColumnName+']' 

				IF LEN(@SearchText) > 0
				BEGIN
					SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + @Prefix + @vcDbColumnName + ' LIKE ''%' + @SearchText + '%'''
				END     
			END
			ELSE IF @vcAssociatedControlType='Label'                                              
			BEGIN  
				SET @strColumns = @strColumns + ',' + CASE 
												WHEN @vcDbColumnName = 'CalAmount' THEN '[dbo].[getdealamount](Opp.numOppId,Getutcdate(),0)'
												WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'monDealAmount' THEN 'ISNULL((SELECT SUM(ISNULL(monDealAmount,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)'
												WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'monAmountPaid' THEN 'ISNULL((SELECT SUM(ISNULL(monAmountPaid,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)'
												WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'vcBizDocsList' THEN 'ISNULL((SELECT SUBSTRING((SELECT ''$^$'' + CAST(numOppBizDocsId AS VARCHAR(18)) +''#^#''+ vcBizDocID
												FROM OpportunityBizDocs WHERE numOppId=Opp.numOppId FOR XML PATH('''')),4,200000)),'''')'
												else @Prefix + @vcDbColumnName 
											END +' ['+ @vcColumnName+']'     
											
				IF LEN(@SearchText) > 0
				BEGIN
					SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + CASE 
												WHEN @vcDbColumnName = 'CalAmount' THEN '[dbo].[getdealamount](Opp.numOppId,Getutcdate(),0)'
												WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'monDealAmount' THEN 'ISNULL((SELECT SUM(ISNULL(monDealAmount,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)'
												WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'monAmountPaid' THEN 'ISNULL((SELECT SUM(ISNULL(monAmountPaid,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)'
												WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'vcBizDocsList' THEN 'ISNULL((SELECT SUBSTRING((SELECT ''$^$'' + CAST(numOppBizDocsId AS VARCHAR(18)) +''#^#''+ vcBizDocID
												FROM OpportunityBizDocs WHERE numOppId=Opp.numOppId FOR XML PATH('''')),4,200000)),'''')'
												else @Prefix + @vcDbColumnName
											END + ' LIKE ''%' + @SearchText + '%'''
				END           
			END
  			ELSE IF @vcAssociatedControlType='Popup' and @vcDbColumnName='numShareWith'                                                   
			BEGIN  
				SET @strColumns = @strColumns+',(SELECT SUBSTRING(
				(SELECT '','' + A.vcFirstName+'' ''+A.vcLastName + CASE WHEN ISNULL(SR.numContactType,0)>0 THEN ''('' + dbo.fn_GetListItemName(SR.numContactType) + '')'' ELSE '''' END
				FROM ShareRecord SR JOIN UserMaster UM ON UM.numUserDetailId=SR.numAssignedTo 
											JOIN AdditionalContactsInformation A ON UM.numUserDetailId=A.numContactID 
						WHERE SR.numDomainID=Opp.numDomainID AND SR.numModuleID=3 AND SR.numRecordID=Opp.numOppId
							AND UM.numDomainID=Opp.numDomainID and UM.numDomainID=A.numDomainID FOR XML PATH('''')),2,200000)) ['+ @vcColumnName+']'    
				
				IF LEN(@SearchText) > 0
				BEGIN
					SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + '(SELECT SUBSTRING(
						(SELECT '','' + A.vcFirstName+'' ''+A.vcLastName + CASE WHEN ISNULL(SR.numContactType,0)>0 THEN ''('' + dbo.fn_GetListItemName(SR.numContactType) + '')'' ELSE '''' END
						FROM ShareRecord SR JOIN UserMaster UM ON UM.numUserDetailId=SR.numAssignedTo 
													JOIN AdditionalContactsInformation A ON UM.numUserDetailId=A.numContactID 
								WHERE SR.numDomainID=Opp.numDomainID AND SR.numModuleID=3 AND SR.numRecordID=Opp.numOppId
									AND UM.numDomainID=Opp.numDomainID and UM.numDomainID=A.numDomainID FOR XML PATH('''')),2,200000)) LIKE ''%' + @SearchText + '%'''
				END            
			END  
		END               
		Else                          
		BEGIN 
			SET @vcColumnName= @vcDbColumnName +'~'+ convert(varchar(10),@numFieldId)+'~1'
               
			IF @vcAssociatedControlType = 'TextBox' or @vcAssociatedControlType = 'TextArea'          
			BEGIN              
				SET @strColumns= @strColumns+',CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value  ['+ @vcColumnName + ']'               
				SET @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values_Opp CFW'+ convert(varchar(3),@tintOrder)+ '             
				on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=Opp.numOppid   '                                                     
			END            
			ELSE IF @vcAssociatedControlType = 'CheckBox'         
			BEGIN          
				SET @strColumns = @strColumns + ',case when isnull(CFW'+ CONVERT(VARCHAR(3),@tintOrder)+'.Fld_Value,0)=0 then ''No'' when isnull(CFW'+ CONVERT(VARCHAR(3),@tintOrder)+'.Fld_Value,0)=1 then ''Yes'' end   ['+ @vcColumnName + ']'             
				SET @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values_Opp CFW'+ convert(varchar(3),@tintOrder)+ '           
					on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=Opp.numOppid   '                                                   
			END          
			ELSE IF @vcAssociatedControlType = 'DateField'       
			BEGIN            
				set @strColumns= @strColumns+',dbo.FormatedDateFromDate(CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value,'+convert(varchar(10),@numDomainId)+')  ['+ @vcColumnName + ']'               
				set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values_Opp CFW'+ convert(varchar(3),@tintOrder)+ '             
				on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=Opp.numOppid    '                                                     
			END            
			ELSE IF @vcAssociatedControlType = 'SelectBox'         
			BEGIN            
				SET @vcDbColumnName = 'DCust'+Convert(varchar(10),@numFieldId)    
				        
				SET @strColumns = @strColumns+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName + ']'                                                      
				SET @WhereCondition = @WhereCondition +' left Join CFW_FLD_Values_Opp CFW'+ convert(varchar(3),@tintOrder)+ '             
									on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=Opp.numOppid     '                                                     
				SET @WhereCondition = @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID=CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value' 
			END
			ELSE
			BEGIN
				SET @strColumns = @strColumns + CONCAT(',dbo.GetCustFldValueOpp(',@numFieldId,',opp.numOppID)') + ' [' + @vcColumnName + ']'
			END
		   
			IF LEN(@SearchText) > 0
			BEGIN
				SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + CONCAT('dbo.GetCustFldValueOpp(',@numFieldId,',opp.numOppID) LIKE ''%',@SearchText,'%''')
			END        
		END 
  
		SELECT TOP 1 
			@tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName,@vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,
			@numListID=numListID,@vcLookBackTableName=vcLookBackTableName,@bitCustom=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,@bitAllowEdit=bitAllowEdit,
			@ListRelID=ListRelID
		FROM 
			#tempForm 
		WHERE 
			tintOrder > @tintOrder-1 
		ORDER BY 
			tintOrder asc            
 
		IF @@rowcount=0 
			SET @tintOrder=0 
	END                            
		
	 
	DECLARE @strShareRedordWith AS VARCHAR(300);
	SET @strShareRedordWith=' Opp.numOppId in (select SR.numRecordID FROM ShareRecord SR WHERE SR.numDomainID=' + CONVERT(VARCHAR(15),@numDomainID) + ' AND SR.numModuleID=3 AND SR.numAssignedTo=' + CONVERT(VARCHAR(15),@numUserCntId) + ') '
                                             
	DECLARE @strSql AS VARCHAR(MAX) = ''                                       
                                                                                 
	SET @strSql = ' FROM 
						OpportunityMaster Opp                                                                     
					INNER JOIN 
						AdditionalContactsInformation ADC                                                                     
					ON
						Opp.numContactId = ADC.numContactId                                  
					INNER JOIN 
						DivisionMaster DM                                                                     
					ON 
						Opp.numDivisionId = DM.numDivisionID AND ADC.numDivisionId = DM.numDivisionID                                                                     
					INNER JOIN 
						CompanyInfo cmp                                                                     
					ON 
						DM.numCompanyID = cmp.numCompanyId                                                                         
					LEFT JOIN 
						AdditionalContactsInformation ADC1 
					ON 
						ADC1.numContactId=Opp.numRecOwner 
					LEFT JOIN 
						ProjectProgress PP 
					ON 
						PP.numOppID = Opp.numOppID
					LEFT JOIN 
						OpportunityLinking OL 
					on 
						OL.numChildOppID=Opp.numOppId ' + @WhereCondition
                                     

	-------Change Row Color-------
	DECLARE @vcCSOrigDbCOlumnName AS VARCHAR(50) = ''
	DECLARE @vcCSLookBackTableName AS VARCHAR(50) = ''
	DECLARE @vcCSAssociatedControlType AS VARCHAR(50) = ''

	CREATE TABLE #tempColorScheme
	(
		vcOrigDbCOlumnName VARCHAR(50),
		vcLookBackTableName VARCHAR(50),
		vcFieldValue VARCHAR(50),
		vcFieldValue1 VARCHAR(50),
		vcColorScheme VARCHAR(50),
		vcAssociatedControlType VARCHAR(50)
	)

	INSERT INTO 
		#tempColorScheme 
	SELECT 
		DFM.vcOrigDbCOlumnName,
		DFM.vcLookBackTableName,
		DFCS.vcFieldValue,
		DFCS.vcFieldValue1,
		DFCS.vcColorScheme,
		DFFM.vcAssociatedControlType 
	FROM 
		DycFieldColorScheme DFCS 
	JOIN 
		DycFormField_Mapping DFFM 
	ON 
		DFCS.numFieldID=DFFM.numFieldID
	JOIN 
		DycFieldMaster DFM 
	ON 
		DFM.numModuleID=DFFM.numModuleID 
		AND DFM.numFieldID=DFFM.numFieldID
	WHERE 
		DFCS.numDomainID=@numDomainID 
		AND DFFM.numFormID=@numFormId 
		AND DFCS.numFormID=@numFormId 
		AND ISNULL(DFFM.bitAllowGridColor,0)=1

	IF(SELECT COUNT(*) FROM #tempColorScheme)>0
	BEGIN
		SELECT TOP 1 @vcCSOrigDbCOlumnName=vcOrigDbCOlumnName,@vcCSLookBackTableName=vcLookBackTableName,@vcCSAssociatedControlType=vcAssociatedControlType FROM #tempColorScheme
	END   

	IF len(@vcCSOrigDbCOlumnName)>0 and len(@vcCSLookBackTableName)>0
	BEGIN
		SET @strColumns = @strColumns + ',tCS.vcColorScheme'
	END

	IF len(@vcCSOrigDbCOlumnName)>0 and len(@vcCSLookBackTableName)>0
	BEGIN
		IF @vcCSLookBackTableName = 'AdditionalContactsInformation'                  
			SET @Prefix = 'ADC.'                  
		IF @vcCSLookBackTableName = 'DivisionMaster'                  
			SET @Prefix = 'DM.'
		IF @vcCSLookBackTableName = 'CompanyInfo'                  
			SET @Prefix = 'CMP.' 
		IF @vcCSLookBackTableName = 'OpportunityMaster'                  
			SET @Prefix = 'Opp.'
		
		IF @vcCSAssociatedControlType='DateField'
		BEGIN
			SET @strSql=@strSql+' LEFT JOIN #tempColorScheme tCS on CONVERT(VARCHAR(10),' + @Prefix + @vcCSOrigDbCOlumnName + ',111) >= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue as int),GETUTCDATE()),111)
					and CONVERT(VARCHAR(10),' + @Prefix + @vcCSOrigDbCOlumnName + ',111) <= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue1 as int),GETUTCDATE()),111)'
		END
		ELSE IF @vcCSAssociatedControlType='SelectBox' OR @vcCSAssociatedControlType='ListBox' OR @vcCSAssociatedControlType='CheckBox'
		BEGIN
			SET @strSql=@strSql+' LEFT JOIN #tempColorScheme tCS on tCS.vcFieldValue=' + @Prefix + @vcCSOrigDbCOlumnName
		END
	END      

    DECLARE @CustomControlType AS VARCHAR(10)      
	IF @columnName like 'CFW.Cust%'             
	BEGIN

		SET @strSql = @strSql +' LEFT JOIN CFW_Fld_Values_Opp CFW on CFW.RecId=Opp.numOppid and CFW.fld_id= ' + REPLACE(@columnName,'CFW.Cust','') +' ' 
		
		SET @CustomControlType = (SELECT Fld_type FROM CFW_Fld_Master CFWM where CFWM.Fld_id = REPLACE(@columnName,'CFW.Cust',''))

		IF @CustomControlType = 'DateField'
		BEGIN
			 SET @columnName='CAST((CASE WHEN CFW.Fld_Value = ''0'' THEN NULL ELSE CFW.Fld_Value END) AS Date)'   
		END
		ELSE
		BEGIN
			SET @columnName='CFW.Fld_Value'   
		END
	END
	
	ELSE IF @columnName like 'DCust%'            
	BEGIN            
		SET @strSql = @strSql + ' left Join CFW_Fld_Values_Opp CFW on CFW.RecId=Opp.numOppid   and CFW.fld_id= '+replace(@columnName,'DCust','')                                                                                               
		SET @strSql = @strSql + ' left Join ListDetails LstCF on LstCF.numListItemID=CFW.Fld_Value  '            
		set @columnName='LstCF.vcData'                   
	END
                                        
	IF @bitPartner=1 
	BEGIN
		SET @strSql=@strSql+' LEFT JOIN OpportunityContact OppCont ON OppCont.numOppId=Opp.numOppId'                                                                               
	END

	SET @strSql = CONCAT(@strSql,' WHERE Opp.numDomainID=',@numDomainID,' AND Opp.tintOppType= ',@OppType,' AND Opp.tintOppstatus=',ISNULL(@tintOppStatus,0),' ')                                                              

	IF @SortChar <> '0' 
		SET @strSql = @strSql + ' And Opp.vcPOppName like ''' + @SortChar + '%'''
                                
	IF @numDivisionID <> 0 
	BEGIN
		SET @strSql = @strSql + ' AND DM.numDivisionID =' + CONVERT(VARCHAR(15),@numDivisionID)                              
    END
	                                                                
	IF @tintUserRightType=1 
	BEGIN
		SET @strSql = @strSql + ' AND (Opp.numRecOwner = '
							+ CONVERT(VARCHAR(15),@numUserCntID) 
							+ ' or Opp.numAssignedTo= '
							+ convert(varchar(15),@numUserCntID)  
							+' or  Opp.numOppId in (select distinct(numOppId) from OpportunityStageDetails where numAssignTo ='
							+ convert(varchar(15),@numUserCntID)+')'
							+ case when @bitPartner=1 then ' or ( OppCont.bitPartner=1 and OppCont.numContactId='+convert(varchar(15),@numUserCntID)+ ')' else '' end 
							+ ' or ' + @strShareRedordWith +')'
    END                                        
	ELSE IF @tintUserRightType=2 
	BEGIN
		SET @strSql = @strSql + ' AND (DM.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '
							+ convert(varchar(15),@numUserCntID)+' ) or DM.numTerID=0 or Opp.numAssignedTo= '
							+ convert(varchar(15),@numUserCntID)  +' or  Opp.numOppId in (select distinct(numOppId) from OpportunityStageDetails where numAssignTo ='
							+ convert(varchar(15),@numUserCntID)+')' + ' or ' + @strShareRedordWith +')'                                                                        
	END           
	
	                                                  
	IF @tintSortOrder=1 
	BEGIN 
		SET @strSql = @strSql + ' AND (Opp.numRecOwner = ' 
							+ CONVERT(VARCHAR(15),@numUserCntID) 
							+ ' or Opp.numAssignedTo= '+ convert(varchar(15),@numUserCntID) 
							+ ' or  Opp.numOppId in (select distinct(numOppId) from OpportunityStageDetails where numAssignTo ='
							+ convert(varchar(15),@numUserCntID)+')'
							+ case when @bitPartner=1 then ' or ( OppCont.bitPartner=1 and OppCont.numContactId='+convert(varchar(15),@numUserCntID)+ ')' else '' end 
							+ ' or ' + @strShareRedordWith +')'                        
    END
	ELSE IF @tintSortOrder=2  
	BEGIN
		SET @strSql = @strSql + 'AND (Opp.numRecOwner in (select A.numContactID from AdditionalContactsInformation A where A.numManagerID='
							+ convert(varchar(15),@numUserCntID) 
							+') or Opp.numAssignedTo in (select A.numContactID from AdditionalContactsInformation A where A.numManagerID='
							+ convert(varchar(15),@numUserCntID)  
							+') or  Opp.numOppId in (select distinct(numOppId) from OpportunityStageDetails where numAssignTo in (select A.numContactID from AdditionalContactsInformation A where A.numManagerID='
							+ convert(varchar(15),@numUserCntID)+'))'
							+ case when @bitPartner=1 then ' or ( OppCont.bitPartner=1 and OppCont.numContactId in (select A.numContactID from AdditionalContactsInformation A where A.numManagerID='+ convert(varchar(15),@numUserCntID)+ '))' else '' end 
							+ ' or ' + @strShareRedordWith +')'                                                                    
	END
	ELSE IF @tintSortOrder=4  
	BEGIN
		SET @strSql=@strSql + ' AND Opp.bintCreatedDate> '''+convert(varchar(20),dateadd(day,-8,@dtLastDate))+''''                                                                    
	END
                                                 

	IF @vcRegularSearchCriteria <> '' 
	BEGIN
		SET @strSql = @strSql + ' AND ' + @vcRegularSearchCriteria 
	END

	IF @vcCustomSearchCriteria <> '' 
	BEGIN
		SET @strSql = @strSql + ' AND ' + @vcCustomSearchCriteria
	END

	IF LEN(@SearchQuery) > 0
	BEGIN
		SET @strSql = @strSql + ' AND (' + @SearchQuery + ') '
	END

	IF ISNULL(@tintDashboardReminderType,0) = 4
	BEGIN
		SET @strSql = CONCAT(@strSql,' AND Opp.numOppID IN (','SELECT DISTINCT
																	OMInner.numOppID
																FROM 
																	StagePercentageDetails SPDInner
																INNER JOIN 
																	OpportunityMaster OMInner
																ON 
																	SPDInner.numOppID=OMInner.numOppId 
																WHERE 
																	SPDInner.numDomainId=',@numDOmainID,'
																	AND OMInner.numDomainId=',@numDOmainID,' 
																	AND OMInner.tintOppType = 1
																	AND ISNULL(OMInner.tintOppStatus,0) = 0
																	AND tinProgressPercentage <> 100',') ')

	END
	ELSE IF ISNULL(@tintDashboardReminderType,0) = 18
	BEGIN
		SET @strSql = CONCAT(@strSql,' AND Opp.numOppID IN (','SELECT DISTINCT
																	OMInner.numOppID
																FROM 
																	StagePercentageDetails SPDInner
																INNER JOIN 
																	OpportunityMaster OMInner
																ON 
																	SPDInner.numOppID=OMInner.numOppId 
																WHERE 
																	SPDInner.numDomainId=',@numDOmainID,'
																	AND OMInner.numDomainId=',@numDOmainID,' 
																	AND OMInner.tintOppType = 2
																	AND ISNULL(OMInner.tintOppStatus,0) = 0
																	AND tinProgressPercentage <> 100',') ')

	END    
                              
	-- EXECUTE FINAL QUERY
	DECLARE @strFinal AS VARCHAR(MAX)
	SET @strFinal = CONCAT('SELECT COUNT(*) OVER() AS TotalRecords, ',@strColumns,@strSql, ' ORDER BY ',@columnName,' ',@columnSortOrder,' OFFSET ',(@CurrentPage-1) * @PageSize,' ROWS FETCH NEXT ',@PageSize,' ROWS ONLY')

	PRINT @strFinal
	EXEC (@strFinal) 

	SELECT * FROM #tempForm

	DROP TABLE #tempForm
	DROP TABLE #tempColorScheme
END
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetOrderItems' ) 
    DROP PROCEDURE USP_GetOrderItems
GO
CREATE PROCEDURE USP_GetOrderItems ( @tintMode TINYINT = 0,
                                     @numOppItemID NUMERIC(9) = 0,
                                     @numOppID NUMERIC(9) = 0,
                                     @numDomainId NUMERIC(9),
                                     @ClientTimeZoneOffset INT,
									 @vcOppItemIDs VARCHAR(2000)='' )
AS 
BEGIN
	
    IF @tintMode = 1 --Add Return grid data & For Recurring Get Original Units to split
        BEGIN
            SELECT  OI.numoppitemtCode,
                    OI.numItemCode,
                    OI.vcItemName,
                    OI.vcItemDesc,
					OI.vcNotes as txtNotes,ISNULL(OI.numCost,0) AS numCost,
                    OI.numUnitHour numOriginalUnitHour,
					CAST(( dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), OI.numItemCode,
                                    OM.numDomainId, ISNULL(OI.numUOMId, 0))
						* OI.numUnitHour ) AS NUMERIC(18, 2)) AS numUnitHour,
					OI.monPrice
            FROM    dbo.OpportunityMaster OM
                    INNER JOIN dbo.OpportunityItems OI ON OM.numOppId = OI.numOppId
                    INNER JOIN dbo.Item I ON I.numItemCode = OI.numItemCode
            WHERE   OM.numDomainId = @numDomainId
                    AND OM.numOppId = @numOppID
                    
        END
 
    IF @tintMode = 2 -- Get single item based on PK, or all items
		-- Used by Sales/Purchase Template Load items in New Sales Order
        BEGIN 
            SELECT  OI.numoppitemtCode,
                    OI.numOppId,
                    OI.numItemCode,
					OI.bitItemPriceApprovalRequired,
                    OI.numUnitHour numOriginalUnitHour,
                    OI.monPrice,
                    OI.monTotAmount,
                    OI.numSourceID,
					ISNULL(OI.numCost,0) AS numCost,
                    OI.vcItemDesc,
					OI.vcNotes as txtNotes,
                    OI.numWarehouseItmsID,
                    OI.vcType,
                    OI.vcAttributes,
                    OI.bitDropShip,
                    ISNULL(OI.numUnitHourReceived,0) AS numUnitHourReceived,
                    OI.bitDiscountType,
                    OI.fltDiscount,
                    OI.monTotAmtBefDiscount,
                    ISNULL(OI.numQtyShipped,0) AS numQtyShipped,
                    OI.vcItemName,
                    OI.vcModelID,
                    OI.vcPathForTImage,
                    OI.vcManufacturer,
                    OI.monVendorCost,
                    ISNULL(OI.numUOMId, 0) AS numUOMId,
                    ISNULL(OI.numUOMId, 0) numUOM, -- used from add edit order
                    ISNULL(U.vcUnitName, '') vcUOMName,
                    dbo.fn_UOMConversion(OI.numUOMId, OI.numItemCode,
                                         OM.numDomainId, NULL) AS UOMConversionFactor,
                    OI.bitWorkOrder,
                    isnull(OI.numVendorWareHouse,0) as numVendorWareHouse,
                    OI.numShipmentMethod,
                    OI.numSOVendorId,
                    OI.numProjectID,
                    OI.numClassID,
                    OI.numProjectStageID,
                    OI.numToWarehouseItemID,
                    WItems.numWarehouseID,
                    W.vcWareHouse AS Warehouse,
					CAST(( dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), OI.numItemCode,
                                    OM.numDomainId, ISNULL(OI.numUOMId, 0))
						* OI.numUnitHour ) AS NUMERIC(18, 2)) AS numUnitHour,
					isnull(OI.numClassID,0) as numClassID,
					isnull(OI.numProjectID,0) as numProjectID,
					ISNULL(dbo.fn_GetUOMName(I.numBaseUnit),'') AS vcBaseUOMName,
					OI.bitDropShip AS bitAllowDropShip,
					I.numVendorID,
					ISNULL(I.bitFreeShipping,0) AS [bitFreeShipping],
					ISNULL(I.txtItemDesc,'') txtItemDesc,
					ISNULL(I.vcModelID,'') vcModelID,
					ISNULL(I.vcManufacturer,'') vcManufacturer,
					ISNULL(I.numBarCodeId,'') numBarCodeId,
					ISNULL(I.bitSerialized,0) bitSerialized,
					ISNULL(I.bitKitParent,0) bitKitParent,
					ISNULL(I.bitAssembly,0) bitAssembly,
					ISNULL(I.IsArchieve,0) IsArchieve,
					ISNULL(I.bitLotNo,0) bitLotNo,
					ISNULL(WItems.monWListPrice,0) monWListPrice,
					ISNULL(I.bitTaxable, 0) bitTaxable,
					ISNULL(I.numItemClassification,0) AS numItemClassification,
                    I.charItemType,
                    I.bitKitParent,
					I.bitAssembly,
					I.bitSerialized,
					ISNULL(I.fltWeight,0) AS [fltWeight],
					ISNULL(I.fltLength,0) AS [fltLength],
					ISNULL(I.fltWidth,0) AS [fltWidth],
					ISNULL(I.fltHeight,0) AS [fltHeight],
					CASE WHEN I.numItemGroup > 0 AND ISNULL(I.bitMatrix,0)=0 THEN ISNULL(WItems.vcWHSKU,ISNULL(I.vcSKU,'')) ELSE ISNULL(I.vcSKU,'') END AS [vcSKU],
					(CASE WHEN ISNULL(I.bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(I.monAverageCost,0) END)  AS monAverageCost,
					CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numOnHand),0) AS VARCHAR(30)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numOnHand,
					CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numOnOrder),0) AS VARCHAR(30)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numOnOrder,
					CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numAllocation),0) AS VARCHAR(30)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numAllocation,
					CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numBackOrder),0) AS VARCHAR(30)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numBackOrder,
					ISNULL(OI.vcAttrValues,'') AS vcAttrValues,
					ISNULL(OI.numSortOrder,0) numSortOrder
					,(CASE WHEN OM.tintOppType=2 THEN ISNULL(vcNotes,'') ELSE '' END) AS vcVendorNotes
					,CPN.CustomerPartNo
            FROM    dbo.OpportunityItems OI
                    INNER JOIN dbo.OpportunityMaster OM ON OM.numOppId = OI.numOppId
					INNER JOIN DivisionMaster DM ON OM.numDivisionId = DM.numDivisionID
                    INNER JOIN dbo.Item I ON I.numItemCode = OI.numItemCode
                    LEFT JOIN UOM U ON U.numUOMId = OI.numUOMId
                    LEFT JOIN WareHouseItems WItems ON WItems.numWareHouseItemID = OI.numWarehouseItmsID
                    LEFT JOIN Warehouses W ON W.numWarehouseID = WItems.numWarehouseID
					LEFT JOIN CustomerPartNumber CPN ON CPN.numItemCode = I.numItemCode 
						AND CPN.numCompanyId=DM.numCompanyID
            WHERE   OI.numOppId = @numOppID
                    AND ( OI.numoppitemtCode = @numOppItemID
                          OR @numOppItemID = 0
                        )
                    AND OM.numDomainId = @numDomainId
                    
        END
 
    IF @tintMode = 3 -- Get item for SO to PO and vice versa creation
        BEGIN
            DECLARE @tintOppType AS TINYINT
			SELECT @tintOppType=tintOppType FROM OpportunityMaster WHERE numOppId=@numOppID

			IF @tintOppType = 1
			BEGIN
				DECLARE @TempItems TABLE
				(
					ID INT IDENTITY(1,1),numItemCode NUMERIC(18,0),vcItemName VARCHAR(300),txtNotes Varchar(MAX),charItemType char,vcModelID VARCHAR(100),vcItemDesc TEXT,vcType VARCHAR(20),vcPathForTImage VARCHAR(200),
					numWarehouseItmsID NUMERIC(18,0),vcAttributes VARCHAR(500),bitDropShip BIT,numUnitHour FLOAT,numUOM NUMERIC(18,0),vcUOMName VARCHAR(100),UOMConversionFactor DECIMAL(18,5),
					monPrice DECIMAL(20,5),numVendorID NUMERIC(18,0),numCost decimal(30, 16),vcPartNo VARCHAR(300),monVendorCost DECIMAL(20,5),numSOVendorId NUMERIC(18,0),numClassID NUMERIC(18,0),numProjectID NUMERIC(18,0),tintLevel TINYINT, vcAttrValues VARCHAR(500),
					ItemRequiredDate DATETIME
				)

				INSERT INTO 
					@TempItems
				SELECT
					OI.numItemCode,OI.vcItemName,OI.vcNotes as txtNotes,I.charItemType,OI.vcModelID,OI.vcItemDesc,OI.vcType,ISNULL(OI.vcPathForTImage, ''),ISNULL(OI.numWarehouseItmsID,0),ISNULL(OI.vcAttributes, ''),
					ISNULL(OI.bitDropShip, 0),OI.numUnitHour * dbo.fn_UOMConversion(I.numBaseUnit, OI.numItemCode,OM.numDomainId, OI.numUOMId),ISNULL(OI.numUOMId, 0),ISNULL(U.vcUnitName, ''),dbo.fn_UOMConversion(OI.numUOMId, OI.numItemCode,OM.numDomainId, NULL) AS UOMConversionFactor,
					OI.monPrice,ISNULL(I.numVendorID, 0),ISNULL(OI.numCost,0),ISNULL(V.vcPartNo, ''),ISNULL(V.monCost, 0),ISNULL(OI.numSOVendorId, 0),0,0,0,ISNULL(vcAttrValues,'')
					,(CASE WHEN ISNULL(OI.ItemReleaseDate,'') = '' THEN
							 ISNULL(OM.dtReleaseDate,'') 
						ELSE OI.ItemReleaseDate 
						END )
				FROM    
					dbo.OpportunityItems OI
					INNER JOIN dbo.OpportunityMaster OM ON OM.numOppId = OI.numOppId
					INNER JOIN dbo.Item I ON I.numItemCode = OI.numItemCode
					LEFT JOIN UOM U ON U.numUOMId = OI.numUOMId
					LEFT JOIN Vendor V ON V.numVendorID = I.numVendorID	AND I.numItemCode = V.numItemCode
				WHERE   
					OI.numOppId = @numOppID
					AND OM.numDomainId = @numDomainId
					AND ISNULL(I.bitKitParent,0) = 0
					AND ISNULL(I.bitAssembly,0) = 0
					AND 1 = (CASE WHEN (SELECT COUNT(*) FROM OpportunityLinking OMInner INNER JOIN OpportunityItems OIInner ON OMInner.numChildOppID=OIInner.numOppId WHERE OMInner.numParentOppID = @numOppID AND OIInner.numItemCode=OI.numItemCode AND ISNULL(OIInner.numWarehouseItmsID,0)=ISNULL(OI.numWarehouseItmsID,0)) > 0 THEN 0 ELSE 1 END)
				ORDER BY
					OI.numSortOrder


				--INSERT KIT CHILD ITEMS WHICH ARE NOT KIT AND NOT YET ADDED
				INSERT INTO 
					@TempItems
				SELECT
					OKI.numChildItemID,'' as txtNotes,I.vcItemName,I.charItemType,I.vcModelID,I.txtItemDesc,(CASE WHEN I.charitemType = 'P' THEN 'Product' WHEN I.charitemType = 'S' THEN 'Service' END),
					ISNULL((SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = I.numItemCode AND bitDefault=1),''),ISNULL(OKI.numWareHouseItemId,0),
					ISNULL(dbo.USP_GetAttributes(OKI.numWarehouseItemID, bitSerialized),''),0,OKI.numQtyItemsReq,ISNULL(OKI.numUOMId, 0),ISNULL(U.vcUnitName, ''),
					dbo.fn_UOMConversion(OKI.numUOMId, OKI.numChildItemID,OM.numDomainId, NULL),
					(CASE WHEN I.charitemType = 'P' THEN WI.monWListPrice ELSE I.monListPrice END),
					ISNULL(I.numVendorID, 0),ISNULL(t1.numCost,0),ISNULL(V.vcPartNo, ''),ISNULL(V.monCost, 0),0,0,0,1,'',
					t1.ItemRequiredDate
				FROM    
					dbo.OpportunityKitItems OKI
				INNER JOIN @TempItems t1 ON OKI.numChildItemID = t1.numItemCode AND ISNULL(OKI.numWareHouseItemId,0) = ISNULL(t1.numWarehouseItmsID,0)
				LEFT JOIN WarehouseItems WI ON OKI.numWareHouseItemId = WI.numWarehouseItemID
				INNER JOIN dbo.OpportunityMaster OM ON OM.numOppId = OKI.numOppId
				INNER JOIN dbo.Item I ON I.numItemCode = OKI.numChildItemID
				LEFT JOIN UOM U ON U.numUOMId = OKI.numUOMId
				LEFT JOIN Vendor V ON V.numVendorID = I.numVendorID	AND I.numItemCode = V.numItemCode
				WHERE   
					OKI.numOppId = @numOppID
					AND OM.numDomainId = @numDomainId
					AND ISNULL(I.bitKitParent,0) = 0
					AND ISNULL(I.bitAssembly,0) = 0
					AND t1.ID IS NULL


				-- FOR KIT CHILD ITEMS WHICH ARE ALREADY ADDED AT PARENT LEVEL - UPDATE QTY ONLY
				UPDATE 
					t2
				SET 
					t2.numUnitHour = ISNULL(t2.numUnitHour,0) + ISNULL(OKI.numQtyItemsReq,0),
					t2.monPrice = (CASE 
									WHEN ISNULL(t2.monPrice,0) < (CASE WHEN I.charitemType = 'P' THEN ISNULL(WI.monWListPrice,0) ELSE ISNULL(I.monListPrice,0) END)  
									THEN t2.monPrice
									ELSE (CASE WHEN I.charitemType = 'P' THEN ISNULL(WI.monWListPrice,0) ELSE ISNULL(I.monListPrice,0) END)
									END)
				FROM
					@TempItems t2
				INNER JOIN
					OpportunityKitItems OKI 
				ON
					OKI.numChildItemID = t2.numItemCode AND ISNULL(OKI.numWareHouseItemId,0) = ISNULL(t2.numWarehouseItmsID,0)
				LEFT JOIN WarehouseItems WI ON OKI.numWareHouseItemId = WI.numWarehouseItemID
				INNER JOIN dbo.OpportunityMaster OM ON OM.numOppId = OKI.numOppId
				INNER JOIN dbo.Item I ON I.numItemCode = OKI.numChildItemID
				LEFT JOIN UOM U ON U.numUOMId = OKI.numUOMId
				LEFT JOIN Vendor V ON V.numVendorID = I.numVendorID	AND I.numItemCode = V.numItemCode
				WHERE   
					OKI.numOppId = @numOppID
					AND OM.numDomainId = @numDomainId
					AND ISNULL(I.bitKitParent,0) = 0
					AND ISNULL(I.bitAssembly,0) = 0
					AND t2.tintLevel <> 1

				--INSERT SUB KIT CHILD ITEMS WHICH ARE NOT KIT AND NOT YET ADDED
				INSERT INTO 
					@TempItems
				SELECT
					OKCI.numItemID,'' as txtNotes,I.vcItemName,I.charItemType,I.vcModelID,I.txtItemDesc,(CASE WHEN I.charitemType = 'P' THEN 'Product' WHEN I.charitemType = 'S' THEN 'Service' END),
					ISNULL((SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = I.numItemCode AND bitDefault=1),''),ISNULL(OKCI.numWareHouseItemId,0),
					ISNULL(dbo.USP_GetAttributes(OKCI.numWarehouseItemID, bitSerialized),''),0,OKCI.numQtyItemsReq,ISNULL(OKCI.numUOMId, 0),ISNULL(U.vcUnitName, ''),
					dbo.fn_UOMConversion(OKCI.numUOMId, OKCI.numItemID,OM.numDomainId, NULL),
					(CASE WHEN I.charitemType = 'P' THEN WI.monWListPrice ELSE I.monListPrice END),
					ISNULL(I.numVendorID, 0),ISNULL(t1.numCost,0),ISNULL(V.vcPartNo, ''),ISNULL(V.monCost, 0),0,0,0,2,'',
					t1.ItemRequiredDate
				FROM    
					dbo.OpportunityKitChildItems OKCI
				INNER JOIN @TempItems t1 ON OKCI.numItemID = t1.numItemCode AND ISNULL(OKCI.numWareHouseItemId,0) = ISNULL(t1.numWarehouseItmsID,0)
				LEFT JOIN WarehouseItems WI ON OKCI.numWareHouseItemId = WI.numWarehouseItemID
				INNER JOIN dbo.OpportunityMaster OM ON OM.numOppId = OKCI.numOppId
				INNER JOIN dbo.Item I ON I.numItemCode = OKCI.numItemID
				LEFT JOIN UOM U ON U.numUOMId = OKCI.numUOMId
				LEFT JOIN Vendor V ON V.numVendorID = I.numVendorID	AND I.numItemCode = V.numItemCode
				WHERE   
					OKCI.numOppId = @numOppID
					AND OM.numDomainId = @numDomainId
					AND ISNULL(I.bitKitParent,0) = 0
					AND ISNULL(I.bitAssembly,0) = 0
					AND t1.ID IS NULL

				-- FOR SUB KIT CHILD ITEMS WHICH ARE ALREADY ADDED AT PARENT LEVEL - UPDATE QTY ONLY
				UPDATE 
					t2
				SET 
					t2.numUnitHour = ISNULL(t2.numUnitHour,0) + ISNULL(OKCI.numQtyItemsReq,0),
					t2.monPrice = (CASE 
									WHEN ISNULL(t2.monPrice,0) < (CASE WHEN I.charitemType = 'P' THEN ISNULL(WI.monWListPrice,0) ELSE ISNULL(I.monListPrice,0) END)  
									THEN t2.monPrice
									ELSE (CASE WHEN I.charitemType = 'P' THEN ISNULL(WI.monWListPrice,0) ELSE ISNULL(I.monListPrice,0) END)
									END)
				FROM
					@TempItems t2
				INNER JOIN
					OpportunityKitChildItems OKCI 
				ON
					OKCI.numItemID = t2.numItemCode AND ISNULL(OKCI.numWareHouseItemId,0) = ISNULL(t2.numWarehouseItmsID,0)
				LEFT JOIN WarehouseItems WI ON OKCI.numWareHouseItemId = WI.numWarehouseItemID
				INNER JOIN dbo.OpportunityMaster OM ON OM.numOppId = OKCI.numOppId
				INNER JOIN dbo.Item I ON I.numItemCode = OKCI.numItemID
				LEFT JOIN UOM U ON U.numUOMId = OKCI.numUOMId
				LEFT JOIN Vendor V ON V.numVendorID = I.numVendorID	AND I.numItemCode = V.numItemCode
				WHERE   
					OKCI.numOppId = @numOppID
					AND OM.numDomainId = @numDomainId
					AND ISNULL(I.bitKitParent,0) = 0
					AND ISNULL(I.bitAssembly,0) = 0
					AND t2.tintLevel <> 2

				SELECT * FROM @TempItems
			END
			ELSE
			BEGIN
				SELECT  OI.numoppitemtCode,
                    OI.numOppId,
					OI.vcNotes as txtNotes,
					OI.bitItemPriceApprovalRequired,
                    OI.vcItemDesc,
                    ISNULL(OI.vcPathForTImage, '') vcPathForTImage,
                    ISNULL(OI.numWarehouseItmsID, 0) numWarehouseItmsID,
                    OI.vcItemName,
                    OI.vcModelID,
					ISNULL(OI.numCost,0) numCost,
                    ISNULL(OI.vcAttributes, '') vcAttributes,
                    OI.vcType,
                    ISNULL(OI.bitDropShip, 0) bitDropShip,
                    OI.numItemCode,
                    OI.numUnitHour numOriginalUnitHour,
                    ISNULL(OI.numUOMId, 0) AS numUOM,
                    ISNULL(U.vcUnitName, '') vcUOMName,
                    dbo.fn_UOMConversion(OI.numUOMId, OI.numItemCode,
                                         OM.numDomainId, NULL) AS UOMConversionFactor,
                    ISNULL(I.numVendorID, 0) numVendorID,
                    ISNULL(V.monCost, 0) monVendorCost,
                    ISNULL(V.vcPartNo, '') vcPartNo,
                    OI.monPrice,
                    ISNULL(OI.numSOVendorId, 0) numSOVendorId,
                    ISNULL(I.charItemType, 'N') charItemType,
					CAST(( dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), OI.numItemCode,
                                    OM.numDomainId, ISNULL(OI.numUOMId, 0))
						* OI.numUnitHour ) AS NUMERIC(18, 2)) AS numUnitHour,
					isnull(OI.numClassID,0) as numClassID,isnull(OI.numProjectID,0) as numProjectID,ISNULL(I.fltWeight,0) AS [fltWeight],ISNULL(I.bitFreeShipping,0) AS [bitFreeShipping],
					ISNULL(I.fltLength,0) AS [fltLength],
					ISNULL(I.fltWidth,0) AS [fltWidth],
					ISNULL(I.fltHeight,0) AS [fltHeight],
					ISNULL((SELECT vcWHSKU FROM dbo.WareHouseItems WHERE numWarehouseItemID = OI.numWarehouseItmsID ),ISNULL(I.vcSKU,'')) AS [vcSKU],
					ISNULL(vcAttrValues,'') AS vcAttrValues,
					(CASE WHEN ISNULL(OI.ItemReleaseDate,'') = '' THEN
							 ISNULL(OM.dtReleaseDate,'') 
						ELSE  OI.ItemReleaseDate
						END ) AS ItemRequiredDate
				FROM    dbo.OpportunityItems OI
						INNER JOIN dbo.OpportunityMaster OM ON OM.numOppId = OI.numOppId
						INNER JOIN dbo.Item I ON I.numItemCode = OI.numItemCode
						LEFT JOIN UOM U ON U.numUOMId = OI.numUOMId
						LEFT JOIN Vendor V ON V.numVendorID = I.numVendorID
												AND I.numItemCode = V.numItemCode
				WHERE   OI.numOppId = @numOppID
						AND OM.numDomainId = @numDomainId
			END
        END
        IF @tintMode = 4 -- Add Edit Order items
        BEGIN 
            SELECT  OI.numoppitemtCode,
                    OI.numOppId,
					OI.vcNotes as txtNotes,
                    OI.numItemCode,
					OI.bitItemPriceApprovalRequired,
					OI.numUnitHour AS numOrigUnitHour,
                    OI.numUnitHour,
                    OI.monPrice,
                    OI.monTotAmount,
                    OI.numSourceID,
                    OI.vcItemDesc,
					OI.vcNotes,
					ISNULL(OI.numCost,0) numCost,
                    OI.numWarehouseItmsID,
                    OI.vcType ItemType,
                    OI.vcAttributes Attributes,
                    OI.bitDropShip DropShip,
                    OI.numUnitHourReceived,
                    OI.bitDiscountType,
                    OI.fltDiscount,
                    OI.monTotAmtBefDiscount,
					ISNULL(OI.monTotAmtBefDiscount,0) - ISNULL(OI.monTotAmount,0) AS TotalDiscountAmount,
                    OI.numQtyShipped,
                    OI.vcItemName,
                    OI.vcModelID,
                    OI.vcPathForTImage,
                    OI.vcManufacturer,
                    OI.monVendorCost,
                    ISNULL(OI.numUOMId, 0) AS numUOM,
                    ISNULL(U.vcUnitName, '') vcUOMName,
                    ISNULL(dbo.fn_UOMConversion(OI.numUOMId, OI.numItemCode,
                                         OM.numDomainId, NULL),1) AS UOMConversionFactor,
                    OI.bitWorkOrder,
                    isnull(OI.numVendorWareHouse,0) as numVendorWareHouse,
                    OI.numShipmentMethod,
                    OI.numSOVendorId,
                    OI.numProjectStageID,
                    OI.numToWarehouseItemID,
                   UPPER(I.charItemType) charItemType,
                    WItems.numWarehouseID,
                    W.vcWareHouse AS Warehouse,
                    0 as Op_Flag,
					CAST(( dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), OI.numItemCode,
                                    OM.numDomainId, ISNULL(OI.numUOMId, 0))
						* OI.numUnitHour ) AS NUMERIC(18, 2)) AS numUnitHour,
					isnull(OI.numClassID,0) as numClassID,isnull(OI.numProjectID,0) as numProjectID,
					CASE WHEN (SELECT COUNT(*) FROM OpportunityBizDocs OB JOIN dbo.OpportunityBizDocItems OBI ON ob.numOppBizDocsId=obi.numOppBizDocID
					AND (ob.bitAuthoritativeBizDocs=1 OR ob.numBizDocId=304) AND obi.numOppItemID=oi.numoppitemtCode AND ob.numOppId=OM.numOppId)>0 THEN 1 ELSE 0 END  AS bitIsAuthBizDoc,
					ISNULL(OI.numUnitHourReceived,0) AS numUnitHourReceived,ISNULL(numQtyShipped,0) AS numQtyShipped,ISNULL(dbo.fn_GetUOMName(I.numBaseUnit),'') AS vcBaseUOMName,ISNULL(I.fltWeight,0) AS [fltWeight],ISNULL(I.bitFreeShipping,0) AS [bitFreeShipping],
					ISNULL(I.fltLength,0) AS [fltLength],
					ISNULL(I.fltWidth,0) AS [fltWidth],
					ISNULL(I.fltHeight,0) AS [fltHeight],
					ISNULL(WItems.vcWHSKU,ISNULL(I.vcSKU,'')) AS [vcSKU],
					(CASE WHEN ISNULL(I.bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(I.monAverageCost,0) END) AS monAverageCost,
					ISNULL(I.bitFreeShipping,0) AS [bitFreeShipping],
					ISNULL(I.txtItemDesc,'') txtItemDesc,
					ISNULL(I.numBarCodeId,'') numBarCodeId,
					ISNULL(I.bitSerialized,0) bitSerialized,
					ISNULL(I.bitKitParent,0) bitKitParent,
					ISNULL(I.bitAssembly,0) bitAssembly,
					ISNULL(I.IsArchieve,0) IsArchieve,
					ISNULL(I.bitLotNo,0) bitLotNo,
					ISNULL(WItems.monWListPrice,0) monWListPrice,
					ISNULL(I.bitTaxable, 0) bitTaxable,
					ISNULL(I.numItemClassification,0) AS numItemClassification,
					CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numOnHand),0) AS VARCHAR(30)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numOnHand,
					CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numOnOrder),0) AS VARCHAR(30)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numOnOrder,
					CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numAllocation),0) AS VARCHAR(30)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numAllocation,
					CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numBackOrder),0) AS VARCHAR(30)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numBackOrder,
					case when ISNULL(I.bitAssembly,0)=1 then dbo.fn_GetAssemblyPossibleWOQty(I.numItemCode,WItems.numWareHouseID) else 0 end AS numMaxWorkOrderQty,
					ISNULL((SELECT COUNT(*) FROM OppWarehouseSerializedItem WHERE numOppId=@numOppID AND numOppItemID=OI.numoppitemtCode),0) AS numSerialNoAssigned,
					(CASE WHEN (SELECT COUNT(*) FROM OpportunityBizDocs OB JOIN dbo.OpportunityBizDocItems OBI ON OB.numOppBizDocsId=OBI.numOppBizDocID WHERE OB.numOppId = OI.numOppId AND OBI.numOppItemID=OI.numoppitemtCode AND OB.numBizDocId=296) > 0 THEN 1 ELSE 0 END) AS bitAddedFulFillmentBizDoc,
					ISNULL(OI.vcAttrValues,'') AS AttributeIDs,
					ISNULL(I.numContainer,0) AS numContainer
					,ISNULL(I.numNoItemIntoContainer,0) AS numContainerQty
					,ISNULL(numPromotionID,0) AS numPromotionID,
					ISNULL(bitPromotionTriggered,0) AS bitPromotionTriggered,
					ISNULL(vcPromotionDetail,'') AS vcPromotionDetail,
					CASE WHEN ISNULL(OI.numSortOrder,0)=0 THEN row_number() OVER (ORDER BY OI.numOppId) ELSE ISNULL(OI.numSortOrder,0) END AS numSortOrder
					,ISNULL(vcNotes,'') AS vcVendorNotes
					,ISNULL(CPN.CustomerPartNo,'') CustomerPartNo
					,(CASE WHEN ISNULL(OI.ItemReleaseDate,'') = '' THEN
							 ISNULL(OM.dtReleaseDate,'') 
						ELSE OI.ItemReleaseDate
						END ) AS ItemRequiredDate
					,ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate) ItemReleaseDate
					,(CASE WHEN ISNULL(bitMarkupDiscount,0) = 0 THEN '0' ELSE '1' END) AS bitMarkupDiscount
					,ISNULL(vcChildKitSelectedItems,'') KitChildItems
					,(CASE 
						WHEN ISNULL(I.bitKitParent,0) = 1 
						THEN 
							(CASE 
								WHEN EXISTS (SELECT ID.numItemDetailID FROM ItemDetails ID INNER JOIN Item IInner ON ID.numChildItemID=IInner.numItemCode WHERE ID.numItemKitID=I.numItemCode AND ISNULL(IInner.bitKitParent,0)=1)
								THEN 1
								ELSE 0
							END)
						ELSE 0 
					END) bitHasKitAsChild
					,ISNULL(I.bitFreeShipping,0) IsFreeShipping
					,(CASE 
						WHEN ISNULL(I.bitKitParent,0)=1
								AND (CASE 
										WHEN EXISTS (SELECT ID.numItemDetailID FROM ItemDetails ID INNER JOIN Item IInner ON ID.numChildItemID=IInner.numItemCode WHERE ID.numItemKitID=I.numItemCode AND ISNULL(IInner.bitKitParent,0)=1)
										THEN 1
										ELSE 0
									END) = 0
						THEN 
							dbo.GetKitWeightBasedOnItemSelection(I.numItemCode,OI.vcChildKitSelectedItems)
						ELSE 
							ISNULL(I.fltWeight,0) 
					END) fltItemWeight
					,ISNULL(I.fltWidth,0) fltItemWidth
					,ISNULL(I.fltHeight,0) fltItemHeight
					,ISNULL(I.fltLength,0) fltItemLength
            FROM    dbo.OpportunityItems OI
                    INNER JOIN dbo.OpportunityMaster OM ON OM.numOppId = OI.numOppId
					INNER JOIN DivisionMaster DM ON OM.numDivisionId=DM.numDivisionID
                    INNER JOIN dbo.Item I ON I.numItemCode = OI.numItemCode
                    LEFT JOIN UOM U ON U.numUOMId = OI.numUOMId
                    LEFT JOIN WareHouseItems WItems ON WItems.numWareHouseItemID = OI.numWarehouseItmsID
                    LEFT JOIN Warehouses W ON W.numWarehouseID = WItems.numWarehouseID
					LEFT JOIN CustomerPartNumber CPN ON CPN.numItemCode = I.numItemCode AND CPN.numCompanyId=DM.numCompanyID
            WHERE   OI.numOppId = @numOppID
                    AND OM.numDomainId = @numDomainId
			ORDER BY OI.numSortOrder
                    
			SELECT  
				vcSerialNo,
				vcComments AS Comments,
				numOppItemID AS numoppitemtCode,
				W.numWarehouseItmsDTLID,
				numWarehouseItmsID AS numWItmsID,
				dbo.Fn_getattributes(W.numWarehouseItmsDTLID, 1) Attributes
			FROM    
				WareHouseItmsDTL W
			JOIN 
				OppWarehouseSerializedItem O 
			ON 
				O.numWarehouseItmsDTLID = W.numWareHouseItmsDTLID
			WHERE   
				numOppID = @numOppID         
        END
        
        
		IF @tintMode = 5 -- Get WebApi Order Items to enter WebApi Order line Item Details 
		BEGIN 
			SELECT OI.numOppItemtCode,OI.numOppId,OI.numItemCode,OI.vcNotes as txtNotes,ISNULL(OI.numCost,0) numCost,
			ISNULL((SELECT vcWHSKU FROM dbo.WareHouseItems WHERE WareHouseItems.numWarehouseItemID = OI.numWarehouseItmsID),ISNULL(IT.vcSKU,'')) AS [vcSKU],
			OI.vcItemDesc,OMAPI.vcAPIOppId
			FROM OpportunityItems OI 
			INNER JOIN dbo.OpportunityMaster OM ON OI.numOppId = OM.numOppId 
			INNER JOIN dbo.OpportunityMasterAPI OMAPI ON OM.numOppId = OMAPI.numOppId
			INNER JOIN dbo.Item IT ON OI.numItemCode = IT.numItemCode
			WHERE OI.numOppId = @numOppID and OM.numDomainID = @numDomainId 
        END

	IF @tintMode = 6 -- Add Edit Order items
        BEGIN 
            SELECT  OI.numoppitemtCode,
                    OI.numOppId,
					OI.vcNotes as txtNotes,
                    OI.numItemCode,
					OI.bitItemPriceApprovalRequired,
					OI.numUnitHour AS numOrigUnitHour,
                    (OI.numUnitHour-ISNULL((SELECT SUM(numUnitHour) FROM ReturnItems where 
					numReturnHeaderID IN (SELECT numReturnHeaderID FROM ReturnHeader 
					WHERE numOppId=@numOppID) AND numOppItemID=OI.numoppitemtCode),0))
					AS numUnitHour,
                    OI.monPrice,
                    OI.monTotAmount,
                    OI.numSourceID,
                    OI.vcItemDesc,
					OI.numCost,
                    OI.numWarehouseItmsID,
                    OI.vcType ItemType,
                    OI.vcAttributes Attributes,
                    OI.bitDropShip DropShip,
                    OI.numUnitHourReceived,
                    OI.bitDiscountType,
                    OI.fltDiscount,
                    OI.monTotAmtBefDiscount,
                    OI.numQtyShipped,
                    OI.vcItemName,
                    OI.vcModelID,
                    OI.vcPathForTImage,
                    OI.vcManufacturer,
                    OI.monVendorCost,
                    ISNULL(OI.numUOMId, 0) AS numUOM,
                    ISNULL(U.vcUnitName, '') vcUOMName,
                    ISNULL(dbo.fn_UOMConversion(OI.numUOMId, OI.numItemCode,
                                         OM.numDomainId, NULL),1) AS UOMConversionFactor,
                    OI.bitWorkOrder,
                    isnull(OI.numVendorWareHouse,0) as numVendorWareHouse,
                    OI.numShipmentMethod,
                    OI.numSOVendorId,
                    OI.numProjectID,
                    OI.numClassID,
                    OI.numProjectStageID,
                    OI.numToWarehouseItemID,
                   UPPER(I.charItemType) charItemType,
                    WItems.numWarehouseID,
                    W.vcWareHouse AS Warehouse,
                    0 as Op_Flag,
					CAST(( dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), OI.numItemCode,
                                    OM.numDomainId, ISNULL(OI.numUOMId, 0))
						* OI.numUnitHour ) AS NUMERIC(18, 2)) AS numUnitHour,
					isnull(OI.numClassID,0) as numClassID,isnull(OI.numProjectID,0) as numProjectID,
					CASE WHEN (SELECT COUNT(*) FROM OpportunityBizDocs OB JOIN dbo.OpportunityBizDocItems OBI ON ob.numOppBizDocsId=obi.numOppBizDocID
					AND (ob.bitAuthoritativeBizDocs=1 OR ob.numBizDocId=304) AND obi.numOppItemID=oi.numoppitemtCode AND ob.numOppId=OM.numOppId)>0 THEN 1 ELSE 0 END  AS bitIsAuthBizDoc,
					ISNULL(OI.numUnitHourReceived,0) AS numUnitHourReceived,ISNULL(numQtyShipped,0) AS numQtyShipped,ISNULL(dbo.fn_GetUOMName(I.numBaseUnit),'') AS vcBaseUOMName,ISNULL(I.fltWeight,0) AS [fltWeight],ISNULL(I.bitFreeShipping,0) AS [bitFreeShipping],
					ISNULL(I.fltLength,0) AS [fltLength],
					ISNULL(I.fltWidth,0) AS [fltWidth],
					ISNULL(I.fltHeight,0) AS [fltHeight],
					ISNULL(WItems.vcWHSKU,ISNULL(I.vcSKU,'')) AS [vcSKU],
					(CASE WHEN ISNULL(I.bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(I.monAverageCost,0) END) AS monAverageCost,
					ISNULL(I.bitFreeShipping,0) AS [bitFreeShipping],
					ISNULL(I.txtItemDesc,'') txtItemDesc,
					ISNULL(I.vcModelID,'') vcModelID,
					ISNULL(I.vcManufacturer,'') vcManufacturer,
					ISNULL(I.numBarCodeId,'') numBarCodeId,
					ISNULL(I.bitSerialized,0) bitSerialized,
					ISNULL(I.bitKitParent,0) bitKitParent,
					ISNULL(I.bitAssembly,0) bitAssembly,
					ISNULL(I.IsArchieve,0) IsArchieve,
					ISNULL(I.bitLotNo,0) bitLotNo,
					ISNULL(WItems.monWListPrice,0) monWListPrice,
					ISNULL(I.bitTaxable, 0) bitTaxable,
					CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numOnHand),0) AS VARCHAR(30)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numOnHand,
					CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numOnOrder),0) AS VARCHAR(30)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numOnOrder,
					CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numAllocation),0) AS VARCHAR(30)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numAllocation,
					CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numBackOrder),0) AS VARCHAR(30)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numBackOrder,
					case when ISNULL(I.bitAssembly,0)=1 then dbo.fn_GetAssemblyPossibleWOQty(I.numItemCode,WItems.numWareHouseID) else 0 end AS numMaxWorkOrderQty,
					ISNULL((SELECT COUNT(*) FROM OppWarehouseSerializedItem WHERE numOppId=@numOppID AND numOppItemID=OI.numoppitemtCode),0) AS numSerialNoAssigned,
					(CASE WHEN (SELECT COUNT(*) FROM OpportunityBizDocs OB JOIN dbo.OpportunityBizDocItems OBI ON OB.numOppBizDocsId=OBI.numOppBizDocID WHERE OB.numOppId = OI.numOppId AND OBI.numOppItemID=OI.numoppitemtCode AND OB.numBizDocId=296) > 0 THEN 1 ELSE 0 END) AS bitAddedFulFillmentBizDoc,
					ISNULL(OI.vcAttrValues,'') AS AttributeIDs
					,(CASE WHEN OM.tintOppType=2 THEN ISNULL(vcNotes,'') ELSE '' END) AS vcVendorNotes
					,(CASE WHEN ISNULL(bitMarkupDiscount,0) = 0 THEN '0' ELSE '1' END) AS bitMarkupDiscount 
--					0 AS [Tax0],0 AS bitTaxable0
            FROM    dbo.OpportunityItems OI
                    INNER JOIN dbo.OpportunityMaster OM ON OM.numOppId = OI.numOppId
                    INNER JOIN dbo.Item I ON I.numItemCode = OI.numItemCode
                    LEFT JOIN UOM U ON U.numUOMId = OI.numUOMId
                    LEFT JOIN WareHouseItems WItems ON WItems.numWareHouseItemID = OI.numWarehouseItmsID
                    LEFT JOIN Warehouses W ON W.numWarehouseID = WItems.numWarehouseID
            WHERE   OI.numOppId = @numOppID
                    AND OM.numDomainId = @numDomainId
					AND (OI.numUnitHour-ISNULL((SELECT SUM(numUnitHour) FROM ReturnItems where 
					numReturnHeaderID IN (SELECT numReturnHeaderID FROM ReturnHeader 
					WHERE numOppId=@numOppID) AND numOppItemID=OI.numoppitemtCode),0))>0
                    
		SELECT  vcSerialNo,
				vcComments AS Comments,
				numOppItemID AS numoppitemtCode,
				W.numWarehouseItmsDTLID,
				numWarehouseItmsID AS numWItmsID,
				dbo.Fn_getattributes(W.numWarehouseItmsDTLID, 1) Attributes
		FROM    WareHouseItmsDTL W
				JOIN OppWarehouseSerializedItem O ON O.numWarehouseItmsDTLID = W.numWareHouseItmsDTLID
		WHERE   numOppID = @numOppID AND numOppItemID IN (SELECT OI.numoppitemtCode FROM OpportunityItems OI WHERE OI.numOppId = @numOppID AND (OI.numUnitHour-ISNULL((SELECT SUM(numUnitHour) FROM ReturnItems where 
					numReturnHeaderID IN (SELECT numReturnHeaderID FROM ReturnHeader 
					WHERE numOppId=@numOppID) AND numOppItemID=OI.numoppitemtCode),0))>0)
					              
                   
END
END
/****** Object:  StoredProcedure [dbo].[USP_GetReceivePaymentDetailsForBizDocs]    Script Date: 07/26/2008 16:18:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Siva            
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getreceivepaymentdetailsforbizdocs')
DROP PROCEDURE usp_getreceivepaymentdetailsforbizdocs
GO
CREATE PROCEDURE [dbo].[USP_GetReceivePaymentDetailsForBizDocs]            
(@numDomainId as numeric(9)=0,      
 @numOppId as numeric(9)=0,  
 @OppBizDocID as numeric(9)=0,
 @ClientTimeZoneOffset as INT,
 @dtFromDate DATETIME =NULL,
 @dtToDate DATETIME =NULL,
 @tintType TINYINT=0,
 @numDivisionId AS NUMERIC(9)=NULL      
)            
As            
Begin   

--IF @tintType=0       --Specific Order
--BEGIN
--
--SELECT @tintType=tintOppType FROM OpportunityMaster WHERE numDomainId=@numDomainId AND numOppId=@numOppId
--
--IF @tintType=1 --Sales
--BEGIN
--	 Select DD.numDepositeDetailID as numBizDocsPaymentDetId,Opp.numOppId as numOppId, Opp.numDivisionId as numDivisionId,            
--   DD.numOppBizDocsId as numBizDocsId, Opp.vcPOppName,DD.monAmountPaid As Amount,OppBizDocs.vcBizDocID,
--	dbo.fn_GetListItemName(DM.numPaymentMethod)  as PaymentMethod,   
--   dbo.fn_GetContactName(DM.numCreatedBy) +' - '+dbo.FormatedDateTimeFromDate(dateadd(minute,-@ClientTimeZoneOffset,DM.dtCreationDate),@numDomainId )  As CreatedBy,
--   DD.vcMemo as Memo,ISNULL(DD.vcReference,'') + ' ' + OppBizDocs.vcRefOrderNo as Reference,opp.tintopptype,C.vcCompanyName,0 AS bitCardAuthorized,0 as bitAmountCaptured,
--0 AS bitIntegratedToAcnt,0 as numComissionID 
--   FROM dbo.DepositMaster DM JOIN dbo.DepositeDetails DD ON DM.numDepositId=DD.numDepositId 
--Inner Join OpportunityBizDocs OppBizDocs on DD.numOppBizDocsID=OppBizDocs.numOppBizDocsId  
--   Inner Join OpportunityMaster Opp On OppBizDocs.numOppId = Opp.numOppId                                                 
--   Inner Join AdditionalContactsInformation ADC On Opp.numContactId = ADC.numContactId                                                 
--   Inner Join DivisionMaster Div On Opp.numDivisionId = Div.numDivisionID AND ADC.numDivisionId = Div.numDivisionID                                                 
--   Inner Join CompanyInfo C On Div.numCompanyID = C.numCompanyId             
--WHERE OppBizDocs.bitAuthoritativeBizDocs = 1 AND  DM.numDomainId=@numDomainId  
--   And Opp.numOppId=@numOppId and DD.numOppBizDocsId =@OppBizDocID
--   
----   UNION
----
----   Select OppBizDocsDet.numBizDocsPaymentDetId as numBizDocsPaymentDetId,Opp.numOppId as numOppId, Opp.numDivisionId as numDivisionId,            
----   BDC.numOppBizDocId as numBizDocsId, Opp.vcPOppName As Name,monAmount As Amount,OBD.vcBizDocID,
----	dbo.fn_GetListItemName(OppBizDocsDet.numPaymentMethod)  as PaymentMethod,   
----   dbo.fn_GetContactName(OppBizDocsDet.numCreatedBy) +' - '+dbo.FormatedDateTimeFromDate(dateadd(minute,-@ClientTimeZoneOffset,OppBizDocsDet.dtCreationDate),@numDomainId)  As CreatedBy,
----   '' as Memo,'Commission Expenses (' +I.vcItemName + ') - ' + dbo.fn_GetContactName(BDC.numUserCntID)   as Reference,
----   opp.tintopptype,C.vcCompanyName,ISNULL(OppBizDocsDet.bitCardAuthorized,0) bitCardAuthorized,ISNULL(OppBizDocsDet.bitAmountCaptured,0) bitAmountCaptured,isnull(OppBizDocsDet.bitIntegratedToAcnt,0) bitIntegratedToAcnt,BDC.numComissionID
----   FROM  BizDocComission BDC JOIN OpportunityBizDocs OBD ON BDC.numOppBizDocId = OBD.numOppBizDocsId
----		 join OpportunityMaster Opp on OPP.numOppId=OBD.numOppId
----		 JOIN OpportunityBizDocItems OBDI on OBDI.numOppBizDocID=OBD.numOppBizDocsId and BDC.numOppBizDocItemID=OBDI.numOppBizDocItemID
----		 join Item I on I.numItemCode=OBDI.numItemCode and I.numDomainID = @numDomainId
----		 JOIN Domain D on D.numDomainID=BDC.numDomainID
----		 JOIN OpportunityBizDocsDetails OppBizDocsDet ON BDC.numBizDocsPaymentDetId=isnull(OppBizDocsDet.numBizDocsPaymentDetId,0)
----		 Inner Join AdditionalContactsInformation ADC On Opp.numContactId = ADC.numContactId                                                 
----		 Inner Join DivisionMaster Div On Opp.numDivisionId = Div.numDivisionID AND ADC.numDivisionId = Div.numDivisionID                                                 
----		 Inner Join CompanyInfo C On Div.numCompanyID = C.numCompanyId
----	    WHERE   OPP.numDomainID = @numDomainId and BDC.numDomainID = @numDomainId
----				--AND BDC.bitCommisionPaid=0
----				AND OBD.bitAuthoritativeBizDocs = 1
----				--AND isnull(OppBizDocsDet.bitIntegratedToAcnt,0) = 1
----				And BDC.numOppId=@numOppId And BDC.numOppBizDocId=@OppBizDocID 
--END
--
--ELSE IF @tintType=2 --Purchase
--BEGIN
--	 Select BPD.numBillPaymentDetailID as numBizDocsPaymentDetId,Opp.numOppId as numOppId, Opp.numDivisionId as numDivisionId,            
--   BPD.numOppBizDocsId as numBizDocsId, Opp.vcPOppName,BPD.monAmount As Amount,OppBizDocs.vcBizDocID,
--	dbo.fn_GetListItemName(BPH.numPaymentMethod)  as PaymentMethod,   
--   dbo.fn_GetContactName(BPH.numCreatedBy) +' - '+dbo.FormatedDateTimeFromDate(dateadd(minute,-@ClientTimeZoneOffset,BPH.dtCreateDate),@numDomainId )  As CreatedBy,
--   '' as Memo,'' as Reference,opp.tintopptype,C.vcCompanyName,0 AS bitCardAuthorized,0 as bitAmountCaptured,0 AS bitIntegratedToAcnt,0 as numComissionID   FROM 
--dbo.BillPaymentHeader BPH JOIN dbo.BillPaymentDetails BPD ON BPH.numBillPaymentID=BPD.numBillPaymentID 
--LEFT OUTER JOIN dbo.CheckHeader CH ON CH.numReferenceID=BPH.numBillPaymentID AND CH.numReferenceID=8
--Inner Join OpportunityBizDocs OppBizDocs on BPD.numOppBizDocsID=OppBizDocs.numOppBizDocsId  
--   Inner Join OpportunityMaster Opp On OppBizDocs.numOppId = Opp.numOppId                                                 
--   Inner Join AdditionalContactsInformation ADC On Opp.numContactId = ADC.numContactId                                                 
--   Inner Join DivisionMaster Div On Opp.numDivisionId = Div.numDivisionID AND ADC.numDivisionId = Div.numDivisionID                                                 
--   Inner Join CompanyInfo C On Div.numCompanyID = C.numCompanyId             
--WHERE OppBizDocs.bitAuthoritativeBizDocs = 1 AND  BPH.numDomainId=@numDomainId  
--   And Opp.numOppId=@numOppId and BPD.numOppBizDocsId= @OppBizDocID
--
----UNION
--
----   Select OppBizDocsDet.numBizDocsPaymentDetId as numBizDocsPaymentDetId,Opp.numOppId as numOppId, Opp.numDivisionId as numDivisionId,            
----   BDC.numOppBizDocId as numBizDocsId, Opp.vcPOppName As Name,monAmount As Amount,OBD.vcBizDocID,
----	dbo.fn_GetListItemName(OppBizDocsDet.numPaymentMethod)  as PaymentMethod,   
----   dbo.fn_GetContactName(OppBizDocsDet.numCreatedBy) +' - '+dbo.FormatedDateTimeFromDate(dateadd(minute,-@ClientTimeZoneOffset,OppBizDocsDet.dtCreationDate),@numDomainId)  As CreatedBy,
----   '' as Memo,'Commission Expenses (' +I.vcItemName + ') - ' + dbo.fn_GetContactName(BDC.numUserCntID)   as Reference,
----   opp.tintopptype,C.vcCompanyName,ISNULL(OppBizDocsDet.bitCardAuthorized,0) bitCardAuthorized,ISNULL(OppBizDocsDet.bitAmountCaptured,0) bitAmountCaptured,isnull(OppBizDocsDet.bitIntegratedToAcnt,0) bitIntegratedToAcnt,BDC.numComissionID
----   FROM  BizDocComission BDC JOIN OpportunityBizDocs OBD ON BDC.numOppBizDocId = OBD.numOppBizDocsId
----		 join OpportunityMaster Opp on OPP.numOppId=OBD.numOppId
----		 JOIN OpportunityBizDocItems OBDI on OBDI.numOppBizDocID=OBD.numOppBizDocsId and BDC.numOppBizDocItemID=OBDI.numOppBizDocItemID
----		 join Item I on I.numItemCode=OBDI.numItemCode and I.numDomainID = @numDomainId
----		 JOIN Domain D on D.numDomainID=BDC.numDomainID
----		 JOIN OpportunityBizDocsDetails OppBizDocsDet ON BDC.numBizDocsPaymentDetId=isnull(OppBizDocsDet.numBizDocsPaymentDetId,0)
----		 Inner Join AdditionalContactsInformation ADC On Opp.numContactId = ADC.numContactId                                                 
----		 Inner Join DivisionMaster Div On Opp.numDivisionId = Div.numDivisionID AND ADC.numDivisionId = Div.numDivisionID                                                 
----		 Inner Join CompanyInfo C On Div.numCompanyID = C.numCompanyId
----	    WHERE   OPP.numDomainID = @numDomainId and BDC.numDomainID = @numDomainId
----				--AND BDC.bitCommisionPaid=0
----				AND OBD.bitAuthoritativeBizDocs = 1
----				--AND isnull(OppBizDocsDet.bitIntegratedToAcnt,0) = 1
----				And BDC.numOppId=@numOppId And BDC.numOppBizDocId=@OppBizDocID  
--END
--
--   
--
--END

--Else 
IF @tintType=1 OR @tintType=2     -- 1 : SO Receivable,2 : PO Payable
BEGIN

--SET @dtFromDate = DateAdd(minute, @ClientTimeZoneOffset, @dtFromDate);
--SET @dtToDate = DateAdd(minute, @ClientTimeZoneOffset, @dtToDate);

IF @tintType=1 --Sales
BEGIN
	 Select DD.numDepositId AS numReferenceID,DD.numDepositeDetailID as numReferenceDetailID,Opp.numOppId as numOppId, Opp.numDivisionId as numDivisionId,            
   DD.numOppBizDocsId as numBizDocsId, Opp.vcPOppName,DD.monAmountPaid As Amount,OppBizDocs.vcBizDocID,
	dbo.fn_GetListItemName(DM.numPaymentMethod)  as PaymentMethod,   
   dbo.fn_GetContactName(DM.numCreatedBy) +' - '+dbo.FormatedDateTimeFromDate(dateadd(minute,-@ClientTimeZoneOffset,DM.dtCreationDate),@numDomainId )  As CreatedBy,
   DD.vcMemo as Memo,ISNULL(DD.vcReference,'') + ' ' + OppBizDocs.vcRefOrderNo as Reference,opp.tintopptype,C.vcCompanyName,[dbo].[FormatedDateFromDate](DM.[dtDepositDate],@numDomainId) AS dtPaymentDate
   FROM dbo.DepositMaster DM JOIN dbo.DepositeDetails DD ON DM.numDepositId=DD.numDepositId 
Inner Join OpportunityBizDocs OppBizDocs on DD.numOppBizDocsID=OppBizDocs.numOppBizDocsId  
   Inner Join OpportunityMaster Opp On OppBizDocs.numOppId = Opp.numOppId                                                 
   Inner Join AdditionalContactsInformation ADC On Opp.numContactId = ADC.numContactId                                                 
   Inner Join DivisionMaster Div On Opp.numDivisionId = Div.numDivisionID AND ADC.numDivisionId = Div.numDivisionID                                                 
   Inner Join CompanyInfo C On Div.numCompanyID = C.numCompanyId             
WHERE OppBizDocs.bitAuthoritativeBizDocs = 1 AND  DM.numDomainId=@numDomainId  
AND DM.tintDepositePage=2
   and DM.dtDepositDate <= @dtToDate AND  DM.dtDepositDate >= @dtFromDate 
   AND Opp.tintOppType=1 AND (Opp.numOppId=@numOppId OR @numOppId=0) and (OppBizDocs.numOppBizDocsId =@OppBizDocID OR @OppBizDocID=0)
   AND (Opp.numDivisionId = @numDivisionId OR @numDivisionId IS NULL)
END

ELSE IF @tintType=2 --Purchase
BEGIN
	 Select BPH.numBillPaymentID AS numReferenceID,BPD.numBillPaymentDetailID as numReferenceDetailID,Opp.numOppId as numOppId, Opp.numDivisionId as numDivisionId,            
   BPD.numOppBizDocsId as numBizDocsId, Opp.vcPOppName,BPD.monAmount As Amount,OppBizDocs.vcBizDocID,
	dbo.fn_GetListItemName(BPH.numPaymentMethod)  as PaymentMethod,   
   dbo.fn_GetContactName(BPH.numCreatedBy) +' - '+dbo.FormatedDateTimeFromDate(dateadd(minute,-@ClientTimeZoneOffset,BPH.dtCreateDate),@numDomainId )  As CreatedBy,
   '' as Memo,ISNULL(OppBizDocs.vcRefOrderNo,'') as Reference,opp.tintopptype,C.vcCompanyName,[dbo].[FormatedDateFromDate]([BPH].[dtPaymentDate],@numDomainId) AS dtPaymentDate   FROM 
dbo.BillPaymentHeader BPH JOIN dbo.BillPaymentDetails BPD ON BPH.numBillPaymentID=BPD.numBillPaymentID 
LEFT OUTER JOIN dbo.CheckHeader CH ON CH.numReferenceID=BPH.numBillPaymentID AND CH.numReferenceID=8
Inner Join OpportunityBizDocs OppBizDocs on BPD.numOppBizDocsID=OppBizDocs.numOppBizDocsId  
   Inner Join OpportunityMaster Opp On OppBizDocs.numOppId = Opp.numOppId                                                 
   Inner Join AdditionalContactsInformation ADC On Opp.numContactId = ADC.numContactId                                                 
   Inner Join DivisionMaster Div On Opp.numDivisionId = Div.numDivisionID AND ADC.numDivisionId = Div.numDivisionID                                                 
   Inner Join CompanyInfo C On Div.numCompanyID = C.numCompanyId             
WHERE OppBizDocs.bitAuthoritativeBizDocs = 1 AND  BPH.numDomainId=@numDomainId  
   and BPH.dtPaymentDate <= @dtToDate AND  BPH.dtPaymentDate >= @dtFromDate 
   AND Opp.tintOppType=2 AND (Opp.numDivisionId = @numDivisionId OR @numDivisionId IS NULL)

END
--   Select OppBizDocsDet.numBizDocsPaymentDetId as numBizDocsPaymentDetId,Opp.numOppId as numOppId, Opp.numDivisionId as numDivisionId,            
--   OppBizDocsDet.numBizDocsId as numBizDocsId, Opp.vcPOppName As Name,monAmount As Amount,
--	dbo.fn_GetListItemName(OppBizDocsDet.numPaymentMethod)  as PaymentMethod,   
--   dbo.fn_GetContactName(OppBizDocsDet.numCreatedBy) +' - '+dbo.FormatedDateTimeFromDate(dateadd(minute,-@ClientTimeZoneOffset,OppBizDocsDet.dtCreationDate),@numDomainId)  As CreatedBy,isnull(OppBizDocsDet.vcMemo,'') as Memo,isnull(OppBizDocsDet.vcReference,'') as Reference,
--   Opp.vcPOppName,opp.tintopptype,C.vcCompanyName,isnull(OppBizDocsDet.bitIntegratedToAcnt,0) bitIntegratedToAcnt,0 as numComissionID  
--   From OpportunityBizDocsDetails OppBizDocsDet  
--   Inner Join OpportunityBizDocs OppBizDocs on OppBizDocsDet.numBizDocsId=OppBizDocs.numOppBizDocsId  
--   Inner Join OpportunityMaster Opp On OppBizDocs.numOppId = Opp.numOppId                                                 
--   Inner Join AdditionalContactsInformation ADC On Opp.numContactId = ADC.numContactId                                                 
--   Inner Join DivisionMaster Div On Opp.numDivisionId = Div.numDivisionID AND ADC.numDivisionId = Div.numDivisionID                                                 
--   Inner Join CompanyInfo C On Div.numCompanyID = C.numCompanyId             
--   WHERE OppBizDocs.bitAuthoritativeBizDocs = 1 AND  OppBizDocsDet.numDomainId=@numDomainId 
--   and OppBizDocsDet.dtCreationDate <= @dtToDate AND  OppBizDocsDet.dtCreationDate >=@dtFromDate 
--   AND Opp.tintOppType=@tintType        
END

End
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetReportListMasterDetail')
DROP PROCEDURE USP_GetReportListMasterDetail
GO
CREATE PROCEDURE [dbo].[USP_GetReportListMasterDetail]     
    @numDomainID numeric(18, 0),
	@numReportID numeric(18, 0)
as                 

SELECT RLM.*,RMM.vcModuleName,RMGM.vcGroupName FROM ReportListMaster RLM 
JOIN ReportModuleMaster RMM ON RLM.numReportModuleID=RMM.numReportModuleID
JOIN ReportModuleGroupMaster RMGM ON RLM.numReportModuleGroupID=RMGM.numReportModuleGroupID    
 WHERE RLM.numDomainID=@numDomainID AND RLM.numReportID=@numReportID

SELECT ReportFieldsList.*,ISNULL(vcFieldDataType,'') vcFieldDataType FROM ReportFieldsList LEFT JOIN ReportFieldGroupMappingMaster ON ReportFieldsList.numFieldID=ReportFieldGroupMappingMaster.numFieldId AND ReportFieldsList.numReportFieldGroupID = ReportFieldGroupMappingMaster.numReportFieldGroupID WHERE numReportID=@numReportID ORDER BY numReportFieldID

SELECT * FROM ReportFilterList WHERE numReportID=@numReportID ORDER BY numReportFilterID

SELECT * FROM ReportSummaryGroupList WHERE numReportID=@numReportID ORDER BY numReportGroupID

--SELECT * FROM ReportMatrixBreakList WHERE numReportID=@numReportID ORDER BY numReportMatrixID

SELECT * FROM ReportMatrixAggregateList WHERE numReportID=@numReportID ORDER BY numReportAggregateID

SELECT * FROM ReportKPIThresold WHERE numReportID=@numReportID ORDER BY numReportThresoldID
/****** Object:  StoredProcedure [dbo].[USP_GetShippingReport]    Script Date: 05/07/2009 17:34:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--SELECT * FROM ShippingReport
--SELECT * FROM ShippingReportItems
-- USP_GetShippingReport 1,60754,792
-- exec USP_GetShippingReport @numDomainID=72,@numOppBizDocId=5465,@ShippingReportId=4
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetShippingReport' ) 
    DROP PROCEDURE USP_GetShippingReport
GO
CREATE PROCEDURE [dbo].[USP_GetShippingReport]
    @numDomainID NUMERIC(9),
    @numOppBizDocId NUMERIC(9),
    @ShippingReportId NUMERIC(9)
AS 
    BEGIN
  
        SELECT  SRI.numItemCode,
                I.vcItemName,
                I.vcModelID,
                CASE WHEN LEN(BDI.vcItemDesc) > 100
                     THEN CONVERT(VARCHAR(100), BDI.vcItemDesc) + '..'
                     ELSE BDI.vcItemDesc
                END vcItemDesc,
                dbo.FormatedDateFromDate(SRI.dtDeliveryDate, @numDomainID) dtDeliveryDate,
                SRI.tintServiceType,
                ISNULL(I.fltWeight, 0) AS [fltTotalWeight],
                ISNULL(I.[fltHeight], 0) AS [fltHeight],
                ISNULL(I.[fltLength], 0) AS [fltLength],
                ISNULL(I.[fltWidth], 0) AS [fltWidth],
                ISNULL(SRI.monShippingRate,(SELECT monTotAmount FROM dbo.OpportunityItems 
											WHERE SR.numOppID = dbo.OpportunityItems.numOppId
											AND numItemCode = (SELECT numShippingServiceITemID FROM dbo.Domain WHERE numDomainId = @numDomainId))) [monShippingRate],
                SB.vcShippingLabelImage,
                SB.vcTrackingNumber,
                SRI.ShippingReportItemId,
                SRI.intNoOfBox,
                SR.[vcValue3],
                SR.[vcValue4],
                CASE WHEN ISNUMERIC(SR.vcFromState) = 1
                     THEN ( SELECT  vcStateCode
                            FROM    ShippingStateMaster
                            WHERE   vcStateName = ( SELECT TOP 1
                                                            vcState
                                                    FROM    State
                                                    WHERE   numStateID = SR.vcFromState
                                                  )
                                    AND [numShipCompany] = SR.[numShippingCompany]
                          )
                     ELSE ''
                END vcFromState,
                CASE WHEN ISNUMERIC(SR.vcFromCountry) = 1
                     THEN ( SELECT  vcCountryCode
                            FROM    ShippingCountryMaster
                            WHERE   vcCountryName = ( SELECT TOP 1
                                                                vcData
                                                      FROM      ListDetails
                                                      WHERE     numListItemID = SR.vcFromCountry
                                                    )
                                    AND [numShipCompany] = SR.[numShippingCompany]
                          )
                     ELSE ''
                END vcFromCountry,
                CASE WHEN ISNUMERIC(SR.vcToState) = 1
                     THEN ( SELECT  vcStateCode
                            FROM    ShippingStateMaster
                            WHERE   vcStateName = ( SELECT TOP 1
                                                            vcState
                                                    FROM    State
                                                    WHERE   numStateID = SR.vcToState
                                                  )
                                    AND [numShipCompany] = SR.[numShippingCompany]
                          )
                     ELSE ''
                END vcToState,
                CASE WHEN ISNUMERIC(SR.vcToCountry) = 1
                     THEN ( SELECT  vcCountryCode
                            FROM    ShippingCountryMaster
                            WHERE   vcCountryName = ( SELECT TOP 1
                                                                vcData
                                                      FROM      ListDetails
                                                      WHERE     numListItemID = SR.vcToCountry
                                                    )
                                    AND [numShipCompany] = SR.[numShippingCompany]
                          )
                     ELSE ''
                END vcToCountry,
                SR.vcToZip,
                SR.vcFromZip,
                SR.[numShippingCompany],
                SB.[numBoxID],
                SB.[vcBoxName],
                BDI.numOppBizDocItemID,
                ISNULL(SR.tintPayorType,0) tintPayorType,
                ISNULL(SR.vcPayorAccountNo, '') vcPayorAccountNo,
				SR.vcPayerStreet,
				SR.vcPayerCity,
                ISNULL(SR.numPayorCountry, 0) numPayorCountry,
				ISNULL(SR.numPayerState, 0) numPayerState,
                ( SELECT    vcCountryCode
                  FROM      ShippingCountryMaster
                  WHERE     vcCountryName = ( SELECT TOP 1
                                                        vcData
                                              FROM      ListDetails
                                              WHERE     numListItemID = SR.numPayorCountry
                                            )
                            AND [numShipCompany] = SR.[numShippingCompany]
                ) vcPayorCountryCode,
				( SELECT  vcStateCode
                            FROM    ShippingStateMaster
                            WHERE   vcStateName = ( SELECT TOP 1
                                                            vcState
                                                    FROM    State
                                                    WHERE   numStateID = SR.numPayerState
                                                  )
                                    AND [numShipCompany] = SR.[numShippingCompany]
                ) vcPayorStateCode,
                SR.vcPayorZip,
                SR.vcFromCity,
                SR.vcFromAddressLine1,
                SR.vcFromAddressLine2,
                ISNULL(SR.vcFromCountry,0) [numFromCountry],
                ISNULL(SR.vcFromState,0) [numFromState],
                SR.vcToCity,
                SR.vcToAddressLine1,
                SR.vcToAddressLine2,
                ISNULL(SR.vcToCountry,0) [numToCountry],
                ISNULL(SR.vcToState,0) [numToState],
                SRI.intBoxQty,
                ISNULL(SB.numServiceTypeID, 0) AS [numServiceTypeID],
                ISNULL(SR.vcFromCompany, '') AS vcFromCompany,
                ISNULL(SR.vcFromName, '') AS vcFromName,
                ISNULL(SR.vcFromPhone, '') AS vcFromPhone,
                ISNULL(SR.vcToCompany, '') AS vcToCompany,
                ISNULL(SR.vcToName, '') AS vcToName,
                ISNULL(SR.vcToPhone, '') AS vcToPhone,
                ISNULL(SB.fltDimensionalWeight, 0) AS [fltDimensionalWeight],
                dbo.fn_GetContactName(SR.numCreatedBy) + ' : '
                + CONVERT(VARCHAR(20), SR.dtCreateDate) AS [CreatedDetails],
                CASE WHEN SR.numShippingCompany = 91 THEN 'Fedex'
                     WHEN SR.numShippingCompany = 88 THEN 'UPS'
                     WHEN SR.numShippingCompany = 90 THEN 'USPS'
                     ELSE 'Other'
                END AS [vcShipCompany],
                CAST(ROUND(( ISNULL(SRI.fltTotalWeight,0) * intBoxQty ), 2) AS DECIMAL(9, 2)) AS [fltTotalRegularWeight],
                CAST(ROUND(ISNULL(fltDimensionalWeight,0), 2) AS DECIMAL(9, 2)) AS [fltTotalDimensionalWeight],
                ISNULL(monTotAmount, 0) AS [monTotAmount],
                ISNULL(bitFromResidential,0) [bitFromResidential],
                ISNULL(bitToResidential,0) [bitToResidential],
                ISNULL(IsCOD,0) [IsCOD],
				ISNULL(IsDryIce,0) [IsDryIce],
				ISNULL(IsHoldSaturday,0) [IsHoldSaturday],
				ISNULL(IsHomeDelivery,0) [IsHomeDelivery],
				ISNULL(IsInsideDelevery,0) [IsInsideDelevery],
				ISNULL(IsInsidePickup,0) [IsInsidePickup],
				ISNULL(IsReturnShipment,0) [IsReturnShipment],
				ISNULL(IsSaturdayDelivery,0) [IsSaturdayDelivery],
				ISNULL(IsSaturdayPickup,0) [IsSaturdayPickup],
				ISNULL(IsAdditionalHandling,0) [IsAdditionalHandling],
				ISNULL(IsLargePackage,0) [IsLargePackage],
				ISNULL(vcCODType,0) [vcCODType],
				SR.numOppID,
				ISNULL(numCODAmount,0) [numCODAmount],
				ISNULL([SR].[numTotalInsuredValue],0) AS [numTotalInsuredValue],
				ISNULL([SR].[numTotalCustomsValue],0) AS [numTotalCustomsValue],
				ISNULL(tintSignatureType,0) AS tintSignatureType,
				OpportunityMaster.numShipFromWarehouse,
				ISNULL(vcDeliveryConfirmation,0) AS vcDeliveryConfirmation,
				ISNULL(SR.vcDescription,'') AS vcDescription
        FROM    ShippingReport AS SR
				INNER JOIN OpportunityMaster ON SR.numOppID = OpportunityMaster.numOppId
                INNER JOIN ShippingBox SB ON SB.numShippingReportId = SR.numShippingReportId
                INNER JOIN ShippingReportItems AS SRI ON SR.numShippingReportId = SRI.numShippingReportId
                                                         AND SRI.numBoxID = SB.numBoxID
                INNER JOIN OpportunityBizDocItems BDI ON SRI.numItemCode = BDI.numItemCode
                                                         AND SR.numOppBizDocId = BDI.numOppBizDocID
                INNER JOIN Item I ON BDI.numItemCode = I.numItemCode
        WHERE   SR.[numOppBizDocId] = @numOppBizDocId
                AND SR.numDomainId = @numDomainID
                AND SR.[numShippingReportId] = @ShippingReportId
--           AND (SB.bitIsMasterTrackingNo = 1 OR SB.bitIsMasterTrackingNo IS NULL)
           
        SELECT  vcBizDocID
        FROM    [OpportunityBizDocs]
        WHERE   [numOppBizDocsId] = @numOppBizDocId

    END
/****** Object:  StoredProcedure [dbo].[USP_GetUsageReport]    Script Date: 07/26/2008 16:18:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj          
--exec USP_GetUsageReport 1,'',3          
          
          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getusagereport')
DROP PROCEDURE usp_getusagereport
GO
CREATE PROCEDURE [dbo].[USP_GetUsageReport]          
@numDomainID as numeric(9)=0,          
@SearchWord as varchar(100),          
@tintLastLoggedIn as tinyint,      
@ClientTimeZoneOffset Int,
@vcSortColumn VARCHAR(200),
@vcSortOrder VARCHAR(4)         
as          
          
declare @strSQL as varchar(2000)          
          
          
          
set @strSQL='select numSubscriberID,vcCompanyName,isnull(vcFirstname,'''')+ '' ''+isnull(vcLastname,'''') as [Name],vcPosition,          
dbo.FormatedDateTimeFromDate(DateAdd(minute,'+Convert(varchar(10), -@ClientTimeZoneOffset)+',dtLastLoggedIn),'+Convert(varchar(10),@numDomainID)+') as dtLastLoggedIn,(select count(*) from UserAccessedDTL where numDivisionID=A.numDivisionID           
and numDomainID=S.numTargetDomainID and bitPortal=0'           
          
if @tintLastLoggedIn=1 set @strSQL=@strSQL + ' and dtLoggedInTime between '''+convert(varchar(30),dateadd(day,-7,getutcdate()))          
+''' and '''+convert(varchar(30),dateadd(day,1,getutcdate()))+''''          
          
if @tintLastLoggedIn=2 set @strSQL=@strSQL + ' and dtLoggedInTime between '''+convert(varchar(30),dateadd(day,-14,getutcdate()))          
+''' and '''+convert(varchar(30),dateadd(day,1,getutcdate()))+''''          
          
if @tintLastLoggedIn=3 set @strSQL=@strSQL + ' and dtLoggedInTime between '''+convert(varchar(30),dateadd(day,-30,getutcdate()))          
+''' and '''+convert(varchar(30),dateadd(day,1,getutcdate()))+''''          
          
          
          
set @strSQL=@strSQL +') as TotalCount,          
intNoofUsersSubscribed,intNoOfPartners,dbo.FormatedDateTimeFromDate(dtSubStartDate,'+Convert(varchar(10),@numDomainID)+') as dtSubStartDate,  
dbo.FormatedDateTimeFromDate(dtSubEndDate,'+Convert(varchar(10),@numDomainId)+') as dtSubEndDate ,bitTrial          
from Subscribers S           
join DivisionMaster D          
on D.numDivisionID=S.numDivisionID          
Join CompanyInfo C          
on C.numCompanyID = D.numCompanyID          
Join AdditionalContactsInformation A          
on A.numContactId= S.numAdminContactID          
where D.numDomainID=' + convert(varchar(15),@numDomainID)          
          
if @tintLastLoggedIn=1 set @strSQL=@strSQL + ' and dtLastLoggedIn between '''+convert(varchar(30),dateadd(day,-7,getutcdate()))          
+''' and '''+convert(varchar(30),dateadd(day,1,getutcdate()))+''''          
          
if @tintLastLoggedIn=2 set @strSQL=@strSQL + ' and dtLastLoggedIn between '''+convert(varchar(30),dateadd(day,-14,getutcdate()))        
+''' and '''+convert(varchar(30),dateadd(day,1,getutcdate()))+''''          
          
if @tintLastLoggedIn=3 set @strSQL=@strSQL + ' and dtLastLoggedIn between '''+convert(varchar(30),dateadd(day,-30,getutcdate()))          
+''' and '''+convert(varchar(30),dateadd(day,1,getutcdate()))+''''          
          
if @SearchWord<>'' set @strSQL=@strSQL + ' and vcCompanyName like ''%'+@SearchWord+'%'''          
  
IF ISNULL(@vcSortColumn,'') = ''
BEGIN
	SET @strSQL=@strSQL + ' ORDER BY vcCompanyName ASC'
END
ELSE
BEGIN
	IF @vcSortColumn='dtSubStartDate' OR @vcSortColumn='dtSubEndDate'  OR @vcSortColumn='dtLastLoggedIn'
	BEGIN
		SET @vcSortColumn = CONCAT('S.',@vcSortColumn)
	END

	SET @strSQL = CONCAT(@strSQL,' ORDER BY ',@vcSortColumn,' ',@vcSortOrder)
END
     
PRINT CAST(@strSQL AS NTEXT)
	      
exec (@strSQL)
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetUserReportList')
DROP PROCEDURE USP_GetUserReportList
GO
CREATE PROCEDURE [dbo].[USP_GetUserReportList]
@numDomainID as numeric(9)=0,         
@numUserCntID as numeric(9)=0          
as          
select URL.numReportID,vcFileName,RPtHeading,RptDesc,tintReportType from PageMaster           
join ReportList on numPageID=NumID          
join UserReportList URL on RptID=URL.numReportID          
where numModuleID=8 AND URL.numDomainID=@numDomainID AND URL.numUserCntID=@numUserCntID and URL.tintReportType =0 AND [bitEnable]=1
    
UNION
    
select URL.numReportID,'frmCustomReportRun.aspx?ReptID='+convert(varchar(10),URL.numReportID) as vcFileName ,    
vcReportName as RPtHeading,vcReportDescription as RptDesc,URL.tintReportType    
 from ReportListMaster RLM    
join UserReportList URL on RLM.numReportID=URL.numReportID          
WHERE URL.numDomainID=@numDomainID AND URL.numUserCntID=@numUserCntID and URL.tintReportType =1

GO
/****** Object:  StoredProcedure [dbo].[USP_GetWarehouses]    Script Date: 07/26/2008 16:18:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj        
GO
--exec [USP_GetWarehouses] 72,0
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getwarehouses')
DROP PROCEDURE usp_getwarehouses
GO
CREATE PROCEDURE [dbo].[USP_GetWarehouses]        
@numDomainID as numeric(9)=0,        
@numWareHouseID as numeric(9)=0        
as
BEGIN         
	SELECT 
		[numWareHouseID]         
		,(CASE WHEN AD.numAddressID IS NOT NULL THEN AD.vcStreet ELSE vcWStreet END) [vcWStreet]        
		,(CASE WHEN AD.numAddressID IS NOT NULL THEN AD.vcCity ELSE [vcWCity] END) [vcWCity]        
		,(CASE WHEN AD.numAddressID IS NOT NULL THEN AD.vcPostalCode ELSE [vcWPinCode] END) [vcWPinCode]        
		,(CASE WHEN AD.numAddressID IS NOT NULL THEN AD.numState ELSE [numWState] END) [numWState]        
		,(CASE WHEN AD.numAddressID IS NOT NULL THEN AD.numCountry ELSE [numWCountry] END) [numWCountry]        
		,W.[numDomainID]              
		,vcWareHouse
		,W.numAddressID
		,W.vcPrintNodeAPIKey
		,W.vcPrintNodePrinterID
		,(CASE WHEN AD.numAddressID IS NULL THEN '' ELSE CONCAT(ISNULL(vcStreet,''),', ',ISNULL(vcCity,''),', ',ISNULL(dbo.fn_GetState(numState),''),', ',ISNULL(vcPostalCode,''),', ',ISNULL(dbo.fn_GetListItemName(numCountry),'')) END) AS vcAddress
		, ISNULL((SELECT TOP 1 vcStateCode FROM ShippingStateMaster 
			where vcStateName= ( CASE WHEN AD.numState IS NOT NULL THEN (dbo.fn_GetState(AD.numState)) ELSE (dbo.fn_GetState(W.numWState)) END) ), '') AS StateAbbr		
		,ISNULL((SELECT TOP 1 vcCountryCode from ShippingCountryMaster 
			where vcCountryName=( CASE WHEN AD.numCountry IS NOT NULL THEN (dbo.fn_GetListItemName(AD.numCountry)) ELSE (dbo.fn_GetListItemName([numWCountry])) END)),'') AS CountryAbbr

   FROM [dbo].[Warehouses] W 
   LEFT JOIN
		AddressDetails AD
	ON
        W.numAddressID = AD.numAddressID
   WHERE 
		W.numDomainID=@numDomainID
		AND (numWareHouseID=@numWareHouseID  OR ISNULL(@numWareHouseID,0) = 0)
END
GO
set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

--created by anoop jayaraj  
--exec USP_GetWebTrafficDetails 1  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getwebtrafficdetails')
DROP PROCEDURE usp_getwebtrafficdetails
GO
CREATE PROCEDURE [dbo].[USP_GetWebTrafficDetails]  
@numDivisionID as numeric(9)=0,
@numDomainID as numeric(9)=0 
AS  
BEGIN
	SELECT 
		TOP 1
		ISNULL(vcSearchTerm, '-') vcSearchTerm
		,(CASE WHEN vcOrginalRef='-' THEN ISNULL(vcOrginalURL,'-') ELSE ISNULL(vcOrginalRef,'-') END) vcOrginalRef
		,vcUserHostAddress
		,CONVERT(VARCHAR(20), dtCreated) AS StartDate
		,CONVERT(VARCHAR(20), (SELECT TOP 1 dtCreated FROM TrackingVisitorsHDR WHERE numDivisionID = @numDivisionID ORDER BY numTrackingID DESC)) EndDate
		,ISNULL(vcCountry,'') AS vcLocation
		,ISNULL((SELECT TOP 1 vcPageName FROM TrackingVisitorsDTL WHERE  numTracVisitorsHDRID IN (SELECT numTrackingID FROM TrackingVisitorsHDR WHERE numDivisionID=@numDivisionID) ORDER by numTracVisitorsDTLID DESC),'') vcLastPageVisited
	FROM    
		TrackingVisitorsHDR TVHDR
	WHERE
		numDivisionID = @numDivisionID
	ORDER BY 
		numTrackingID ASC  
  
  
	SELECT TOP 10 
		numTrackingID
		,(SELECT COUNT(*) FROM TrackingVisitorsDTL WHERE numTracVisitorsHDRID=TrackingVisitorsHDR.numTrackingID) AS NoOfTimes
		,CONVERT(VARCHAR(8),vcTotalTime) vcTotalTime
		,CONVERT(VARCHAR(20),dtCreated) AS dtCreated  
	FROM 
		TrackingVisitorsHDR 
	WHERE
		numDivisionID=@numDivisionID 
	ORDER BY 
		dtCreated ASC

END
/****** Object:  StoredProcedure [dbo].[USP_ManageShippingReport]    Script Date: 05/07/2009 17:31:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--SELECT * FROM [ShippingReport]
--SELECT * FROM [ShippingReportItems]
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageShippingReport')
DROP PROCEDURE USP_ManageShippingReport
GO
CREATE PROCEDURE [dbo].[USP_ManageShippingReport]
  @numDomainId NUMERIC(9),
    @numShippingReportId NUMERIC(9) = 0 OUTPUT,
    @numOppBizDocId NUMERIC(9),
    @numShippingCompany NUMERIC(9),
    @Value1 VARCHAR(100) = NULL,
    @Value2 VARCHAR(100) = NULL,
    @Value3 VARCHAR(100) = NULL,
    @Value4 VARCHAR(100) = NULL,
    @vcFromState VARCHAR(50) = '',
    @vcFromZip VARCHAR(50) = '',
    @vcFromCountry VARCHAR(50) = '',
    @vcToState VARCHAR(50) = '',
    @vcToZip VARCHAR(50) = '',
    @vcToCountry VARCHAR(50) = '',
    @numUserCntId NUMERIC(9),
    @strItems TEXT = NULL,
    @tintMode TINYINT=0,
	@tintPayorType TINYINT=0,
	@vcPayorAccountNo VARCHAR(20)='',
	@numPayorCountry NUMERIC(9),
	@vcPayorZip VARCHAR(50)='',
	@vcFromCity VARCHAR(50)='',
    @vcFromAddressLine1 VARCHAR(50)='',
    @vcFromAddressLine2 VARCHAR(50)='',
    @vcToCity VARCHAR(50)='',
    @vcToAddressLine1 VARCHAR(50)='',
    @vcToAddressLine2 VARCHAR(50)='',
    @vcFromName			VARCHAR(1000)='',
    @vcFromCompany		VARCHAR(1000)='',
    @vcFromPhone		VARCHAR(100)='',
    @bitFromResidential	BIT=0,
    @vcToName			VARCHAR(1000)='',
    @vcToCompany		VARCHAR(1000)='',
    @vcToPhone			VARCHAR(100)='',
    @bitToResidential	BIT=0,
    @IsCOD					BIT = 0,
	@IsDryIce				BIT = 0,
	@IsHoldSaturday			BIT = 0,
	@IsHomeDelivery			BIT = 0,
	@IsInsideDelevery		BIT = 0,
	@IsInsidePickup			BIT = 0,
	@IsReturnShipment		BIT = 0,
	@IsSaturdayDelivery		BIT = 0,
	@IsSaturdayPickup		BIT = 0,
	@numCODAmount			NUMERIC(18,0) = 0,
	@vcCODType				VARCHAR(50) = 'Any',
	@numTotalInsuredValue	NUMERIC(18,2) = 0,
	@IsAdditionalHandling	BIT,
	@IsLargePackage			BIT,
	@vcDeliveryConfirmation	VARCHAR(1000),
	@vcDescription			VARCHAR(MAX),
	@numOppId				NUMERIC(18,0),
	@numTotalCustomsValue	NUMERIC(18,2),
	@tintSignatureType NUMERIC(18,0)
AS 
BEGIN
    IF @numShippingReportId = 0 
    BEGIN
		SELECT
			@IsCOD = IsCOD
			,@IsHomeDelivery = IsHomeDelivery
			,@IsInsideDelevery = IsInsideDelevery
			,@IsInsidePickup = IsInsidePickup
			,@IsSaturdayDelivery = IsSaturdayDelivery
			,@IsSaturdayPickup = IsSaturdayPickup
			,@IsAdditionalHandling = IsAdditionalHandling
			,@IsLargePackage=IsLargePackage
			,@vcCODType = vcCODType
			,@vcDeliveryConfirmation = vcDeliveryConfirmation
			,@vcDescription = vcDescription
			,@tintSignatureType = CAST(ISNULL(vcSignatureType,'0') AS TINYINT)
		FROM
			OpportunityMaster
		INNER JOIN
			DivisionMaster
		ON
			OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
		INNER JOIN
			DivisionMasterShippingConfiguration
		ON
			DivisionMaster.numDivisionID = DivisionMasterShippingConfiguration.numDivisionID
		WHERE
			OpportunityMaster.numDomainId = @numDomainId
			AND DivisionMaster.numDomainID=@numDomainId
			AND DivisionMasterShippingConfiguration.numDomainID=@numDomainId
			AND OpportunityMaster.numOppId = @numOppID

            INSERT  INTO [ShippingReport]
            (
               [numOppBizDocId],
               [numShippingCompany],
               vcValue1,
               vcValue2,
               vcValue3,
               vcValue4,
               vcFromState,
               vcFromZip,
               vcFromCountry,
               vcToState,
               vcToZip,
               vcToCountry,
               [dtCreateDate],
               [numCreatedBy],
               numDomainId,
				tintPayorType,
				vcPayorAccountNo,
				numPayorCountry,
				vcPayorZip,
				vcFromCity ,
				vcFromAddressLine1 ,
				vcFromAddressLine2 ,
				vcToCity ,
				vcToAddressLine1 ,
				vcToAddressLine2,
				vcFromName,
				vcFromCompany,
				vcFromPhone,
				bitFromResidential,
				vcToName,
				vcToCompany,
				vcToPhone,
				bitToResidential,
				IsCOD,					
				IsDryIce,				
				IsHoldSaturday,			
				IsHomeDelivery,			
				IsInsideDelevery,		
				IsInsidePickup,			
				IsReturnShipment,		
				IsSaturdayDelivery,		
				IsSaturdayPickup,		
				numCODAmount,			
				vcCODType,				
				numTotalInsuredValue,
				IsAdditionalHandling,
				IsLargePackage,
				vcDeliveryConfirmation,
				vcDescription,
				numOppId,numTotalCustomsValue,tintSignatureType
            )
            VALUES  
			(
               @numOppBizDocId,
               @numShippingCompany,
               @value1,
               @value2,
               @value3,
               @value4,
               @vcFromState,
               @vcFromZip,
               @vcFromCountry,
               @vcToState,
               @vcToZip,
               @vcToCountry,
               GETUTCDATE(),
               @numUserCntId,
               @numDomainId,
				@tintPayorType,
				@vcPayorAccountNo,
				@numPayorCountry,
				@vcPayorZip,
				@vcFromCity ,
				@vcFromAddressLine1 ,
				@vcFromAddressLine2 ,
				@vcToCity ,
				@vcToAddressLine1 ,
				@vcToAddressLine2,
				@vcFromName,
				@vcFromCompany,
				@vcFromPhone,
				@bitFromResidential,
				@vcToName,
				@vcToCompany,
				@vcToPhone,
				@bitToResidential,
				@IsCOD,					
				@IsDryIce,				
				@IsHoldSaturday,			
				@IsHomeDelivery,			
				@IsInsideDelevery,		
				@IsInsidePickup,			
				@IsReturnShipment,		
				@IsSaturdayDelivery,		
				@IsSaturdayPickup,		
				@numCODAmount,			
				@vcCODType,				
				@numTotalInsuredValue,
				@IsAdditionalHandling,
				@IsLargePackage,
				@vcDeliveryConfirmation,
				@vcDescription,
				@numOppId,@numTotalCustomsValue,@tintSignatureType						  
            )
                        
			SET @numShippingReportId = SCOPE_IDENTITY()
				
            DECLARE @numBoxID NUMERIC 
			INSERT INTO [ShippingBox] (
				[vcBoxName],
				[numShippingReportId],
				[dtCreateDate],
				[numCreatedBy]
			) VALUES ( 
				/* vcBoxName - varchar(20) */ 'Box1',
				/* numShippingReportId - numeric(18, 0) */ @numShippingReportId,
				/* dtCreateDate - datetime */ GETUTCDATE(),
				/* numCreatedBy - numeric(18, 0) */ @numUserCntId ) 
		                
			SET @numBoxID = SCOPE_IDENTITY()
--				END
                
                
            IF (@strItems IS NOT NULL)
			BEGIN
				DECLARE @hDocItem INT
				EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems
	  
				INSERT  INTO [ShippingReportItems]
						(
							[numShippingReportId],
							[numItemCode],
							[tintServiceType],
							[dtDeliveryDate],
							[monShippingRate],
							[fltTotalWeight],
							[intNoOfBox],
							[fltHeight],
							[fltWidth],
							[fltLength],
							[dtCreateDate],
							[numCreatedBy],
							numBoxID,
							[numOppBizDocItemID]
						)
						SELECT  @numShippingReportId,--X.[numShippingReportId],
								X.[numItemCode],
								X.[tintServiceType],
								X.[dtDeliveryDate],
								X.[monShippingRate],
								X.[fltTotalWeight],
								X.[intNoOfBox],
								X.[fltHeight],
								X.[fltWidth],
								X.[fltLength],
								GETUTCDATE(),--X.[dtCreateDate],
								@numUserCntId,
								@numBoxID,
								CASE @tintMode WHEN 0 THEN X.numOppBizDocItemID ELSE (SELECT TOP 1 numOppBizDocItemID FROM OpportunityBizDocItems OBI WHERE OBI.numItemCode = X.numItemCode) END AS numOppBizDocItemID
						FROM    ( SELECT    *
									FROM      OPENXML (@hDocItem, '/NewDataSet/Items', 2)
											WITH ( --numShippingReportId  NUMERIC(18,0),
											numItemCode NUMERIC(18, 0), tintServiceType TINYINT, dtDeliveryDate DATETIME, monShippingRate DECIMAL(20,5), 
											fltTotalWeight FLOAT, intNoOfBox INT, fltHeight FLOAT, fltWidth FLOAT, fltLength FLOAT,numOppBizDocItemID NUMERIC)
								) X
					
				EXEC sp_xml_removedocument @hDocItem	
			END                                
                
			IF ISNULL(@numOppId,0) > 0
			BEGIN
				DECLARE @bitUseShippersAccountNo BIT
				DECLARE @numTempDivisionID NUMERIC(18,0)
				DECLARE @vcPayerAccountNo VARCHAR(MAX)
				DECLARE @vcPayerStreet VARCHAR(MAX)
				DECLARE @vcPayerCity VARCHAR(MAX)
				DECLARE @numPayerState NUMERIC(18,0)

				SELECT 
					@bitUseShippersAccountNo=ISNULL(bitUseShippersAccountNo,0) 
					,@numTempDivisionID=DivisionMaster.numDivisionID
				FROM 
					OpportunityMaster 
				INNER JOIN
					DivisionMaster
				ON
					OpportunityMaster.numDivisionId=DivisionMaster.numDivisionID
				WHERE 
					OpportunityMaster.numDomainId=@numDomainId 
					AND OpportunityMaster.numOppId=@numOppId

				IF ISNULL(@bitUseShippersAccountNo,0) = 1 AND ISNULL(@tintPayorType,0) <> 2 AND EXISTS (SELECT ID FROM DivisionMasterShippingAccount WHERE numDivisionID=@numTempDivisionID AND numShipViaID=@numShippingCompany)
				BEGIN
					SELECT
						@vcPayerAccountNo=ISNULL(vcAccountNumber,'')
						,@vcPayerStreet=ISNULL(vcStreet,'')
						,@vcPayerCity=ISNULL(vcCity,'')
						,@numPayorCountry=numCountry
						,@numPayerState=numState
						,@vcPayorZip=vcZipCode
					FROM
						DivisionMasterShippingAccount
					WHERE
						numDivisionID=@numTempDivisionID 
						AND numShipViaID=@numShippingCompany

					UPDATE
						ShippingReport
					SET
						tintPayorType=2
						,vcPayorAccountNo=@vcPayerAccountNo
						,vcPayerStreet=@vcPayerStreet
						,vcPayerCity=@vcPayerCity
						,numPayorCountry=@numPayorCountry
						,numPayerState=@numPayerState
						,vcPayorZip=@vcPayorZip
					WHERE
						numShippingReportId=@numShippingReportId
				END
			END

			

        END
        ELSE IF @numShippingReportId > 0 
        BEGIN
            UPDATE 
				[ShippingReport]
            SET
				[numShippingCompany] = @numShippingCompany,
                vcValue1 = @Value1,
                vcValue2 = @Value2,
                vcValue3 = @Value3,
                vcValue4 = @Value4,
                vcFromState = @vcFromState,
                vcFromZip = @vcFromZip,
                vcFromCountry = @vcFromCountry,
                vcToState = @vcToState,
                vcToZip = @vcToZip,
                vcToCountry = @vcToCountry,
				tintPayorType = @tintPayorType,
				vcPayorAccountNo=@vcPayorAccountNo,
				numPayorCountry=@numPayorCountry,
				vcPayorZip=@vcPayorZip,
				vcFromCity=@vcFromCity,
				vcFromAddressLine1=@vcFromAddressLine1,
				vcFromAddressLine2=@vcFromAddressLine2,
				vcToCity=@vcToCity,
				vcToAddressLine1=@vcToAddressLine1,
				vcToAddressLine2=@vcToAddressLine2,
				vcFromName = @vcFromName,
				vcFromCompany = @vcFromCompany,
				vcFromPhone = @vcFromPhone,
				bitFromResidential = @bitFromResidential,
				vcToName = @vcToName,
				vcToCompany = @vcToCompany,
				vcToPhone = @vcToPhone,
				bitToResidential = @bitToResidential,
				IsCOD = @IsCOD ,					
				IsDryIce = @IsDryIce,				
				IsHoldSaturday  = @IsHoldSaturday,			
				IsHomeDelivery = @IsHomeDelivery,			
				IsInsideDelevery = @IsInsideDelevery,		
				IsInsidePickup = @IsInsidePickup,			
				IsReturnShipment = @IsReturnShipment,		
				IsSaturdayDelivery = @IsSaturdayDelivery,		
				IsSaturdayPickup = @IsSaturdayPickup,		
				numCODAmount = @numCODAmount,			
				vcCODType = @vcCODType,				
				numTotalInsuredValue = @numTotalInsuredValue,
				IsAdditionalHandling = @IsAdditionalHandling,
				IsLargePackage = @IsLargePackage,
				vcDeliveryConfirmation = @vcDeliveryConfirmation,
				vcDescription = @vcDescription,							
				numOppId = @numOppId,
				numTotalCustomsValue = @numTotalCustomsValue,
				tintSignatureType=@tintSignatureType
            WHERE   
				[numShippingReportId] = @numShippingReportId
        END
    END
GO
set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

--created by anoop jayaraj                      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_managetrackingvisitors')
DROP PROCEDURE usp_managetrackingvisitors
GO
CREATE PROCEDURE [dbo].[USP_ManageTrackingVisitors]                      
@vcUserHostAddress as varchar(100)='',
@vcUserDomain as varchar(100)='',
@vcUserAgent as varchar(1000)='',
@vcBrowser as varchar(50)='',
@vcCrawler as varchar(50)='',
@vcURL as varchar(500)='',
@vcReferrer as varchar(500)='',
@numVisits as numeric(9)=0,
@vcOrginalURL as varchar(500)='',                      
@vcOrginalRef as varchar(500)='',                      
@numVistedPages as numeric(9)=0,                      
@vcTotalTime as varchar(100)='',                      
@strURL as text='',                               
@vcCountry as varchar(50),                
@IsVisited as bit,
@numDivisionID as numeric(9)  ,    
@numDomainId as numeric (9) =0,
@vcCampaign AS VARCHAR(500)
AS  
BEGIN
	SET @vcOrginalURL = replace(replace(replace(replace(isnull(@vcOrginalURL,'-'),'%3A',':'),'%3F','?'),'%3D','='),'%26','&')                      
	SET @vcOrginalRef = replace(replace(replace(replace(isnull(@vcOrginalRef,'-'),'%3A',':'),'%3F','?'),'%3D','='),'%26','&')  
	SET @vcURL = replace(replace(replace(replace(isnull(@vcURL,'-'),'%3A',':'),'%3F','?'),'%3D','='),'%26','&')                      
	SET @vcReferrer = replace(replace(replace(replace(isnull(@vcReferrer,'-'),'%3A',':'),'%3F','?'),'%3D','='),'%26','&')
           
	DECLARE @numIdentity AS NUMERIC           
	DECLARE @vcTime AS VARCHAR(15)           

	/*Check for online campaign existance*/   
	DECLARE @numCampaignID NUMERIC(9)
	IF @numDivisionID > 0
	BEGIN
		SELECT TOP 1 
			@numCampaignID = [numCampaignId] 
		FROM 
			[CampaignMaster] 
		WHERE 
			 vcCampaignCode =@vcCampaign
			AND [numDomainID]=@numDomainId 
			AND [bitIsOnline] = 1
	END          
          
          


	SELECT 
		@IsVisited = COUNT(*) 
	FROM 
		TrackingVisitorsHDR
	WHERE 
		vcUserHostAddress=@vcUserHostAddress 
		AND dtCreated BETWEEN CONVERT(VARCHAR(20),GETUTCDATE(),101) AND CONVERT(VARCHAR(20),DATEADD(DAY,1,GETUTCDATE()),101) 
		AND numDomainID= @numDomainID


		IF @numDivisionID > 0
		BEGIN
			/*Check if previous visit had already created Division- if yes then update old record else create new record for TrackingVisitorsHDR */          
			IF (SELECT 
					COUNT(*) 
				FROM 
					TrackingVisitorsHDR
				WHERE 
					numDomainID= @numDomainID
					AND ISNULL([numDivisionID],0) = @numDivisionID) > 0
			BEGIN
				SET @IsVisited = 1
			END
		END
       
       
PRINT   @IsVisited
if @IsVisited=0          
begin
		declare @startPos as integer          
		declare @EndPos as integer          
		declare @vcSearchTerm as varchar(100)          
		          
		          
		          
		 SELECT @startPos=CHARINDEX( 'Referrer', @vcURL)          
		 if @startPos>0          
		 begin          
		  set @vcReferrer=substring(@vcURL,@startPos+9,500)          
				end          
		           
		 SELECT @startPos=CHARINDEX( '&q=', @vcReferrer)          
		 if @startPos>0          
		 begin          
		  set @vcSearchTerm=substring(@vcReferrer,@startPos+1,150)          
		            
		  SELECT @EndPos=CHARINDEX( '&', @vcSearchTerm)          
		  if @EndPos= 0 set @EndPos=100          
		  set @vcSearchTerm=substring(@vcSearchTerm,3,@EndPos-3)          
		           
		 end          
		 SELECT @startPos=CHARINDEX( '&p=', @vcReferrer)          
		 if @startPos>0          
		 begin          
		  set @vcSearchTerm=substring(@vcReferrer,@startPos+1,150)          
		            
		            
		  SELECT @EndPos=CHARINDEX( '&', @vcSearchTerm)          
		  if @EndPos= 0 set @EndPos=100          
		  set @vcSearchTerm=substring(@vcSearchTerm,3,@EndPos-3)          
		           
		 end           
		 SELECT @startPos=CHARINDEX( '?q=', @vcReferrer)          
		 if @startPos>0          
		 begin          
		  set @vcSearchTerm=substring(@vcReferrer,@startPos+1,150)          
		            
		  SELECT @EndPos=CHARINDEX( '&', @vcSearchTerm)          
		  if @EndPos= 0 set @EndPos=100          
		  set @vcSearchTerm=substring(@vcSearchTerm,3,@EndPos-3)          
		           
		 end          
		 SELECT @startPos=CHARINDEX( '?p=', @vcReferrer)          
		 if @startPos>0          
		 begin          
		  set @vcSearchTerm=substring(@vcReferrer,@startPos+1,150)          
		            
		            
		  SELECT @EndPos=CHARINDEX( '&', @vcSearchTerm)          
		  if @EndPos= 0 set @EndPos=100          
		  set @vcSearchTerm=substring(@vcSearchTerm,3,@EndPos-3)          
		           
		 end          
		          
		           
		 set @vcSearchTerm=replace(@vcSearchTerm,'+',' ')       
		      
		    
		if (@vcUserDomain like '%inktomisearch.com' or @vcUserDomain like '%ask.com'  or @vcUserDomain like  '%yahoo.net' or @vcUserDomain like  '%chello.nl' or @vcUserDomain like  '%linksmanager.com' or @vcUserDomain like  '%googlebot.com') set @vcCrawler='True'
  
   
   


         
          
          
          
			INSERT  INTO TrackingVisitorsHDR
					(
					  vcUserHostAddress,
					  vcUserDomain,
					  vcUserAgent,
					  vcBrowser,
					  vcCrawler,
					  vcURL,
					  vcReferrer,
					  vcSearchTerm,
					  numVisits,
					  vcOrginalURL,
					  vcOrginalRef,
					  numVistedPages,
					  vcTotalTime,
					  dtCreated,
					  vcCountry,
					  numDivisionID,
					  numDomainId,
					  [numCampaignID],
					  [vcDomainName]
					)
			VALUES  (
					  @vcUserHostAddress,
					  @vcUserDomain,
					  @vcUserAgent,
					  @vcBrowser,
					  @vcCrawler,
					  @vcURL,
					  @vcReferrer,
					  @vcSearchTerm,
					  @numVisits,
					  @vcOrginalURL,
					  @vcOrginalRef,
					  @numVistedPages,
					  @vcTotalTime,
					  GETUTCDATE(),
					  @vcCountry,
					  @numDivisionID,
					  @numDomainId,
					  @numCampaignID,
					  dbo.GetDomainNameFromURL(@vcOrginalURL)
					)           
			          
			select @numIdentity=@@identity   

	

                    
 end          
else          
begin          
          
	select top 1 @numIdentity=numTrackingID,@vcTime=vcTotalTime from TrackingVisitorsHDR where vcUserHostAddress=@vcUserHostAddress order by numTrackingID desc          
	declare @sec1 as integer          
	declare @min1 as integer           
	declare @hour1 as integer          
	declare @sec2 as integer          
	declare @min2 as integer          
	declare @hour2 as integer          
	set @sec1=substring(@vcTime,7,2)          
	set @min1=substring(@vcTime,4,2)          
	set @hour1=left(@vcTime,2)          
	set @sec2=substring(@vcTotalTime,7,2)          
	set @min2=substring(@vcTotalTime,4,2)          
	set @hour2=left(@vcTotalTime,2)          
	--print @sec1          
	--print @min1          
	--print @hour1          
	--print @sec2          
	--print @min2          
	--print @hour2          
	          
	set @sec1=@sec1+@sec2          
	 if @sec1>=60           
	 begin          
	  set @sec1=@sec1-60          
	  set @min1=@min1+1          
	 end          
	set @min1=@min1+@min2          
	 if @min1>=60           
	 begin          
	  set @min1=@min1-60          
	  set @hour1=@hour1+1           
	 end          
	set @hour1=@hour1+@hour2          
	--print @sec1          
	--print @min1          
	--print @hour1      
	if len(@hour1)=1 set @vcTotalTime='0'+convert(varchar,@hour1)+':'      
	else set @vcTotalTime=convert(varchar,@hour1)+':'      
	if len(@min1)=1  set @vcTotalTime=@vcTotalTime+'0'+convert(varchar,@min1)+':'      
	else set @vcTotalTime=@vcTotalTime+convert(varchar,@min1)+':'      
	      
	         
	if len(@sec1)=1 set @vcTotalTime=@vcTotalTime+'0'+convert(varchar,@sec1)      
	else set @vcTotalTime=@vcTotalTime+convert(varchar,@sec1)      
	         
	      
--set @vcTotalTime=convert(varchar,@hour1)+':'+convert(varchar,@min1)+':'+convert(varchar,@sec1)          
--print @vcTotalTime          
end          
          
                           
 DECLARE @hDoc int                            
 EXEC sp_xml_preparedocument @hDoc OUTPUT, @strURL                            
                            
 insert into TrackingVisitorsDTL                            
  (numTracVisitorsHDRID,vcPageName,vcElapsedTime,numPageID,dtTime)                            
                             
 select @numIdentity,X.URL,X.ElapsedTime,(select numPageID from PageMasterWebAnlys where X.URL like '%'+ vcPageURL+'%' ),X.[Time] from(                            
 SELECT *FROM OPENXML (@hDoc,'/NewDataSet/Table',2)                            
 WITH  (                            
  URL varchar(500),                            
  ElapsedTime varchar(100),          
  [Time] datetime                             
  ))X                   
  EXEC sp_xml_removedocument @hDoc                   
                          
update TrackingVisitorsHDR set vcTotalTime=@vcTotalTime,  numVistedPages=(select count(*) from TrackingVisitorsDTL where numTracVisitorsHDRID=@numIdentity)  where  numTrackingID= @numIdentity                      
        
if @numDivisionID>0         
begin        
  update TrackingVisitorsHDR set numDivisionID=@numDivisionID  where  numTrackingID= @numIdentity     
  
	DECLARE @vcTempOrigReferer VARCHAR(MAX)
	SELECT @vcTempOrigReferer=(CASE WHEN ISNULL(vcOrginalRef,'-') = '-' THEN ISNULL(vcOrginalURL,'-') ELSE vcOrginalRef END) FROM TrackingVisitorsHDR WHERE numTrackingID= @numIdentity

	DECLARE @numTempDomainID NUMERIC(18,0)
	SELECT @numTempDomainID=numDomainID FROM DivisionMaster WHERE numDivisionID=@numDivisionID

	IF EXISTS (SELECT CompanyInfo.numCompanyID FROM DivisionMaster INNER JOIN CompanyInfo ON DivisionMaster.numCompanyID=CompanyInfo.numCompanyId WHERE numDivisionID=@numDivisionID AND ISNULL(vcHow,0) = 0)
	BEGIN
		IF EXISTS (SELECT numTrackingID FROM TrackingVisitorsHDR WHERE numDomainID=@numTempDomainID AND ISNULL(numListItemID,0) > 0 AND CHARINDEX(vcOrginalRef,@vcTempOrigReferer) > 0)
		BEGIN
			DECLARE @vcHow NUMERIC(18,0)
	
			SELECT TOP 1
				@vcHow=numListItemID 
			FROM 
				TrackingVisitorsHDR 
			WHERE 
				numDomainID=@numTempDomainID 
				AND ISNULL(numListItemID,0) > 0 
				AND @vcTempOrigReferer LIKE CONCAT(vcOrginalRef,'%')

			UPDATE
				C
			SET
				vcHow=@vcHow
			FROM
				DivisionMaster D
			INNER JOIN 
				CompanyInfo C
			ON  
				D.numCompanyID=C.numCompanyId 
			WHERE 
				numDivisionID=@numDivisionID
		END
	END
END


/*In Order to increase performace for campaign report we need to insert filtered data into seperate table-chintan*/
IF @numCampaignID >0 AND @numIdentity>0 AND @numDivisionID>0
	BEGIN
	-- making TrackingCampaignData table obsolete .. by directly updating data on division
	UPDATE dbo.DivisionMaster SET numCampaignID = @numCampaignID WHERE numDivisionID = @numDivisionID

	END
END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_MassPurchaseFulfillment_GetConfiguration')
DROP PROCEDURE dbo.USP_MassPurchaseFulfillment_GetConfiguration
GO

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_MassPurchaseFulfillment_GetConfiguration]
(
	@numDomainID NUMERIC(18,0)
	,@numUserCntID NUMERIC(18,0)
)
AS
BEGIN
	IF EXISTS (SELECT ID FROM MassPurchaseFulfillmentConfiguration WHERE numDomainID=@numDomainID AND numUserCntID=@numUserCntID)
	BEGIN
		SELECT 
			ISNULL(bitGroupByOrderForReceive,0) bitGroupByOrderForReceive
			,ISNULL(bitGroupByOrderForPutAway,0) bitGroupByOrderForPutAway
			,ISNULL(bitGroupByOrderForBill,0) bitGroupByOrderForBill
			,ISNULL(bitGroupByOrderForClose,0) bitGroupByOrderForClose
			,ISNULL(bitReceiveBillOnClose,0) bitReceiveBillOnClose
			,ISNULL(tintScanValue,1) tintScanValue
		FROM	
			MassPurchaseFulfillmentConfiguration
		WHERE
			numDomainID=@numDomainID
			AND numUserCntID=@numUserCntID
	END
	ELSE 
	BEGIN
		SELECT 
			0 bitGroupByOrderForReceive
			,0 bitGroupByOrderForPutAway
			,0 bitGroupByOrderForBill
			,0 bitGroupByOrderForClose
			,0 bitReceiveBillOnClose
			,1 tintScanValue
	END
END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_MassPurchaseFulfillment_GetOrderItems')
DROP PROCEDURE dbo.USP_MassPurchaseFulfillment_GetOrderItems
GO

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_MassPurchaseFulfillment_GetOrderItems]
(
	@numDomainID NUMERIC(18,0)
	,@numUserCntID NUMERIC(18,0)
	,@numOppID NUMERIC(18,0)
	,@tintMode TINYINT
	,@vcSelectedRecords VARCHAR(MAX)
)
AS 
BEGIN
	DECLARE @tintBillType TINYINT = 1
	
	IF EXISTS (SELECT ID FROM MassPurchaseFulfillmentConfiguration WHERE numDomainID=@numDomainID AND numUserCntID=@numUserCntID)
	BEGIN
		SELECT 
			@tintBillType = ISNULL(tintBillType,1)
		FROM 
			MassPurchaseFulfillmentConfiguration 
		WHERE 
			numDomainID=@numDomainID 
			AND numUserCntID=@numUserCntID
	END

	IF ISNULL(@vcSelectedRecords,'') <> ''
	BEGIN
		DECLARE @TempItems TABLE
		(	
			numOppItemID NUMERIC(18,0),
			numUnitHour FLOAT
		)

		INSERT INTO @TempItems
		(
			numOppItemID,
			numUnitHour
		)
		SELECT
			CAST(SUBSTRING(OutParam,0,CHARINDEX('-',OutParam)) AS NUMERIC(18,0))
			,CAST(SUBSTRING(OutParam,CHARINDEX('-',OutParam) + 1,LEN(OutParam)) AS FLOAT)
		FROM
			dbo.SplitString(@vcSelectedRecords,',')

		IF @tintMode = 1 -- For bill orders
		BEGIN
			SELECT
				numoppitemtCode
				,(CASE WHEN ISNULL(T1.numUnitHour,0) > 0 
					THEN T1.numUnitHour
					ELSE (CASE 
							WHEN @tintBillType=2 
							THEN ISNULL(OpportunityItems.numQtyReceived,0) 
							WHEN @tintBillType=3 THEN ISNULL(OpportunityItems.numUnitHourReceived,0) 
							ELSE ISNULL(OpportunityItems.numUnitHour,0) 
						END) - ISNULL((SELECT 
											SUM(OpportunityBizDocItems.numUnitHour)
										FROM
											OpportunityBizDocs
										INNER JOIN
											OpportunityBizDocItems
										ON
											OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
										WHERE
											OpportunityBizDocs.numOppId = @numOppID
											AND OpportunityBizDocs.numBizDocId=644
											AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode),0) 
				END) numUnitHour
				,monPrice
			FROM
				@TempItems T1
			INNER JOIN
				OpportunityItems
			ON
				T1.numOppItemID=OpportunityItems.numoppitemtCode
			INNER JOIN
				OpportunityMaster
			ON
				OpportunityItems.numOppId = OpportunityMaster.numOppId
			WHERE
				OpportunityMaster.numDomainId=@numDomainID
				AND OpportunityMaster.numOppId=@numOppID
				AND OpportunityMaster.tintOppType = 2
				AND OpportunityMaster.tintOppStatus = 1
				AND ISNULL(OpportunityMaster.tintshipped,0) = 0
				AND (CASE 
							WHEN @tintBillType=2 
							THEN ISNULL(OpportunityItems.numQtyReceived,0) 
							WHEN @tintBillType=3 THEN ISNULL(OpportunityItems.numUnitHourReceived,0) 
							ELSE ISNULL(OpportunityItems.numUnitHour,0) 
						END) - ISNULL((SELECT 
											SUM(OpportunityBizDocItems.numUnitHour)
										FROM
											OpportunityBizDocs
										INNER JOIN
											OpportunityBizDocItems
										ON
											OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
										WHERE
											OpportunityBizDocs.numOppId = @numOppID
											AND OpportunityBizDocs.numBizDocId=644
											AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode),0) > 0
		END
	END
	ELSE
	BEGIN
		IF @tintMode = 1 -- For bill orders
		BEGIN
			SELECT
				numoppitemtCode
				,(CASE 
					WHEN @tintBillType=2 
					THEN ISNULL(OpportunityItems.numQtyReceived,0) 
					WHEN @tintBillType=3 THEN ISNULL(OpportunityItems.numUnitHourReceived,0) 
					ELSE ISNULL(OpportunityItems.numUnitHour,0) 
				END) - ISNULL((SELECT 
								SUM(OpportunityBizDocItems.numUnitHour)
							FROM
								OpportunityBizDocs
							INNER JOIN
								OpportunityBizDocItems
							ON
								OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
							WHERE
								OpportunityBizDocs.numOppId = @numOppID
								AND OpportunityBizDocs.numBizDocId=644
								AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode),0) numUnitHour
				,monPrice
			FROM
				OpportunityMaster
			INNER JOIN
				OpportunityItems
			ON
				OpportunityMaster.numOppId=OpportunityItems.numOppId
			WHERE
				OpportunityMaster.numDomainId=@numDomainID
				AND OpportunityMaster.numOppId=@numOppID
				AND OpportunityMaster.tintOppType = 2
				AND OpportunityMaster.tintOppStatus = 1
				AND ISNULL(OpportunityMaster.tintshipped,0) = 0
				AND (CASE 
						WHEN @tintBillType=2 
						THEN ISNULL(OpportunityItems.numQtyReceived,0) 
						WHEN @tintBillType=3 THEN ISNULL(OpportunityItems.numUnitHourReceived,0) 
						ELSE ISNULL(OpportunityItems.numUnitHour,0) 
					END) - ISNULL((SELECT 
										SUM(OpportunityBizDocItems.numUnitHour)
									FROM
										OpportunityBizDocs
									INNER JOIN
										OpportunityBizDocItems
									ON
										OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
									WHERE
										OpportunityBizDocs.numOppId = @numOppID
										AND OpportunityBizDocs.numBizDocId=644
										AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode),0) > 0
		END
		ELSE IF @tintMode = 2 -- For close order with auto receive and bill on
		BEGIN
			SELECT 
				OpportunityMaster.numOppID OppID
				,OpportunityMaster.vcPoppName OppName
				,OpportunityItems.numoppitemtCode OppItemID
				,(CASE WHEN ISNULL(OpportunityMaster.bitStockTransfer,0) = 1 THEN OpportunityItems.numToWarehouseItemID ELSE OpportunityItems.numWarehouseItmsID END) WarehouseItemID
				,ISNULL(OpportunityMaster.bitStockTransfer,0) IsStockTransfer
				,ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numUnitHourReceived,0) QtyToReceive
				,OpportunityItems.numItemCode ItemCode
				,Item.vcItemName ItemName
				,ISNULL(bitSerialized,0) IsSerial
				,ISNULL(bitLotNo,0) IsLot
				,'' SerialLot
				,ISNULL(OpportunityMaster.bitPPVariance,0) IsPPVariance
				,OpportunityMaster.numDivisionId DivisionID
				,ISNULL(OpportunityItems.monPrice,0) Price
				,ISNULL(OpportunityMaster.numCurrencyID,0) CurrencyID
				,ISNULL(OpportunityMaster.fltExchangeRate,0) ExchangeRate
				,Item.charItemType CharItemType
				,OpportunityItems.vcType ItemType
				,ISNULL(OpportunityItems.bitDropShip,0) DropShip
				,ISNULL(OpportunityItems.numProjectID,0) ProjectID
				,ISNULL(OpportunityItems.numClassID,0) ClassID
				,ISNULL(Item.numAssetChartAcntId,0) AssetChartAcntId
				,ISNULL(Item.numCOGsChartAcntId,0) COGsChartAcntId
				,ISNULL(Item.numIncomeChartAcntId,0) IncomeChartAcntId
			FROM
				OpportunityMaster
			INNER JOIN
				OpportunityItems
			ON
				OpportunityMaster.numOppId = OpportunityItems.numOppId
			INNER JOIN
				Item 
			ON
				OpportunityItems.numItemCode = Item.numItemCode
			WHERE
				OpportunityItems.numOppId=@numOppID
				AND (ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numUnitHourReceived,0)) > 0
		END
	END

END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_MassPurchaseFulfillment_GetPOItemsPendingToReceive')
DROP PROCEDURE USP_MassPurchaseFulfillment_GetPOItemsPendingToReceive
GO
CREATE PROCEDURE [dbo].[USP_MassPurchaseFulfillment_GetPOItemsPendingToReceive]
	@numDomainID as numeric(18,0),
	@numOppID numeric(18,0)=0
AS
BEGIN
	SELECT
		OpportunityMaster.numOppID AS OppID
		,ISNULL(OpportunityMaster.bitStockTransfer,0) AS IsStockTransfer
		,OpportunityMaster.numDivisionID AS DivisionID
		,ISNULL(OpportunityMaster.vcPoppName,'''') AS OppName
		,ISNULL(OpportunityMaster.bitPPVariance,0) AS IsPPVariance
		,ISNULL(OpportunityMaster.numCurrencyID,0) AS CurrencyID
		,ISNULL(OpportunityMaster.fltExchangeRate,0) AS ExchangeRate
		,ISNULL(OpportunityItems.numoppitemtCode,0) AS OppItemID 
        ,ISNULL(Item.vcItemName,0) AS ItemName
        ,ISNULL(OpportunityItems.numWarehouseItmsID,0) AS WarehouseItemID
        ,ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyReceived,0) AS RemainingQty
        ,ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyReceived,0) AS QtyToReceive
        ,'' AS SerialLot
        ,ISNULL(Item.bitSerialized,0) AS IsSerial
        ,ISNULL(Item.bitLotNo,0) AS IsLot
        ,ISNULL(Item.charItemType,'') AS CharItemType
        ,ISNULL(OpportunityItems.vcType,'') AS ItemType
        ,ISNULL(OpportunityItems.monPrice,0) AS Price
        ,ISNULL(OpportunityItems.bitDropShip,0) AS DropShip
        ,ISNULL(OpportunityItems.numProjectID,0) AS ProjectID
        ,ISNULL(OpportunityItems.numClassID,0) AS ClassID
        ,ISNULL(Item.numAssetChartAcntId,0) AS AssetChartAcntId
        ,ISNULL(Item.numCOGsChartAcntId,0) AS COGsChartAcntId
        ,ISNULL(Item.numIncomeChartAcntId,0) AS IncomeChartAcntId
		,1 AS IsSuccess
		,'' AS ErrorMessage
	FROM
		OpportunityMaster
	INNER JOIN
		OpportunityItems
	ON
		OpportunityMaster.numOppId = OpportunityItems.numoppitemtCode
	INNER JOIN
		Item
	ON
		OpportunityItems.numItemCode = Item.numItemCode
	WHERE
		OpportunityMaster.numDomainID=@numDomainID
		AND OpportunityMaster.numOppId=@numOppID
		AND OpportunityMaster.tintOppType = 2
		AND OpportunityMaster.tintOppStatus = 1
		AND ISNULL(OpportunityMaster.tintshipped,0) = 0
		AND ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numUnitHourReceived,0) - ISNULL(OpportunityItems.numQtyReceived,0) > 0
END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_MassPurchaseFulfillment_GetRecords')
DROP PROCEDURE dbo.USP_MassPurchaseFulfillment_GetRecords
GO

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_MassPurchaseFulfillment_GetRecords]
(
	@numDomainID NUMERIC(18,0)
	,@numUserCntID NUMERIC(18,0)
	,@ClientTimeZoneOffset INT
	,@numViewID NUMERIC(18,0)
	,@numWarehouseID NUMERIC(18,0)
	,@bitGroupByOrder BIT
	,@bitIncludeSearch BIT
	,@vcOrderStauts VARCHAR(MAX)
	,@vcCustomSearchValue VARCHAR(MAX)
	,@numPageIndex INT
	,@vcSortColumn VARCHAR(100) OUTPUT
	,@vcSortOrder VARCHAR(4) OUTPUT
	,@numTotalRecords NUMERIC(18,0) OUTPUT
)
AS 
BEGIN
	DECLARE @bitShowOnlyFullyReceived BIT = 0
	DECLARE @tintBillType TINYINT = 1
	
	IF EXISTS (SELECT ID FROM MassPurchaseFulfillmentConfiguration WHERE numDomainID=@numDomainID AND numUserCntID=@numUserCntID)
	BEGIN
		SELECT 
			@bitShowOnlyFullyReceived=ISNULL(bitShowOnlyFullyReceived,0)
			,@tintBillType = ISNULL(tintBillType,1)
		FROM 
			MassPurchaseFulfillmentConfiguration 
		WHERE 
			numDomainID=@numDomainID 
			AND numUserCntID=@numUserCntID
	END

	IF @numViewID = 4
	BEGIN
		SET @bitGroupByOrder = 1
	END

	DECLARE @TempFieldsLeft TABLE
	(
		numPKID INT IDENTITY(1,1)
		,ID VARCHAR(50)
		,numFieldID NUMERIC(18,0)
		,vcFieldName VARCHAR(100)
		,vcOrigDbColumnName VARCHAR(100)
		,vcListItemType VARCHAR(10)
		,numListID NUMERIC(18,0)
		,bitAllowSorting BIT
		,bitAllowFiltering BIT
		,vcAssociatedControlType VARCHAR(20)
		,vcLookBackTableName VARCHAR(100)
		,bitCustomField BIT
		,intRowNum INT
		,intColumnWidth FLOAT
		,bitIsRequired BIT
		,bitIsEmail BIT
		,bitIsAlphaNumeric BIT
		,bitIsNumeric BIT
		,bitIsLengthValidation BIT
		,intMaxLength INT
		,intMinLength INT
		,bitFieldMessage BIT
		,vcFieldMessage VARCHAR(MAX)
	)

	IF @bitGroupByOrder = 1
	BEGIN
		IF @vcSortColumn NOT IN ('CompanyInfo.vcCompanyName','OpportunityMaster.bintCreatedDate','OpportunityMaster.vcPoppName','OpportunityMaster.numStatus','OpportunityMaster.numAssignedTo','OpportunityMaster.numRecOwner')
		BEGIN
			SET @vcSortColumn = 'OpportunityMaster.bintCreatedDate'
			SET @vcSortOrder = 'DESC'
		END
	END
	ELSE 
	BEGIN
		IF @vcSortColumn NOT IN ('CompanyInfo.vcCompanyName'
								,'OpportunityMaster.bintCreatedDate'
								,'OpportunityMaster.vcPoppName'
								,'OpportunityMaster.numStatus'
								,'OpportunityMaster.numAssignedTo'
								,'OpportunityMaster.numRecOwner'
								,'WarehouseItems.numOnHand'
								,'WarehouseItems.numOnOrder'
								,'WarehouseItems.numAllocation'
								,'WarehouseItems.numBackOrder'
								,'Item.numItemCode'
								,'Item.vcItemName'
								,'OpportunityMaster.numUnitHour'
								,'OpportunityMaster.numUnitHourReceived')
		BEGIN
			SET @vcSortColumn = 'OpportunityMaster.bintCreatedDate'
			SET @vcSortOrder = 'DESC'
		END
	END

	INSERT INTO @TempFieldsLeft
	(
		ID
		,numFieldID
		,vcFieldName
		,vcOrigDbColumnName
		,vcListItemType
		,numListID
		,bitAllowSorting
		,bitAllowFiltering
		,vcAssociatedControlType
		,vcLookBackTableName
		,bitCustomField
		,intRowNum
		,intColumnWidth
		,bitIsRequired
		,bitIsEmail
		,bitIsAlphaNumeric
		,bitIsNumeric
		,bitIsLengthValidation
		,intMaxLength
		,intMinLength
		,bitFieldMessage
		,vcFieldMessage
	)
	SELECT
		CONCAT(135,'~',DFM.numFieldID,'~',0)
		,DFM.numFieldID
		,(CASE WHEN @numViewID=6 AND DFM.numFieldID=248 THEN 'BizDoc' ELSE DFFM.vcFieldName END)
		,DFM.vcOrigDbColumnName
		,ISNULL(DFM.vcListItemType,'')
		,ISNULL(DFM.numListID,0)
		,DFFM.bitAllowSorting
		,DFFM.bitAllowFiltering
		,DFM.vcAssociatedControlType
		,DFM.vcLookBackTableName
		,0
		,DFCD.intRowNum
		,ISNULL(DFCD.intColumnWidth,0)
		,0
		,0
		,0
		,0
		,0
		,0
		,0
		,0
		,''
	FROM
		DycFormConfigurationDetails DFCD
	INNER JOIN
		DycFormField_Mapping DFFM
	ON
		DFCD.numFormId = DFFM.numFormID
		AND DFCD.numFieldId = DFFM.numFieldID
	INNER JOIN
		DycFieldMaster DFM
	ON
		DFFM.numFieldID = DFM.numFieldId
	WHERE 
		DFCD.numDomainId=@numDomainID 
		AND DFCD.numUserCntID=@numUserCntID 
		AND DFCD.numFormId=135 
		AND DFCD.numViewId=@numViewID
		AND ISNULL(DFFM.bitDeleted,0) = 0
		AND ISNULL(DFFM.bitSettingField,0) = 1
		AND ISNULL(DFCD.bitCustom,0) = 0
	UNION
	SELECT
		CONCAT(135,'~',CFM.Fld_id,'~',0)
		,CFM.Fld_id
		,CFM.Fld_label
		,CONCAT('CUST',CFM.Fld_id)
		,''
		,ISNULL(CFM.numListID,0)
		,0
		,0
		,CFM.Fld_type
		,''
		,1
		,DFCD.intRowNum
		,ISNULL(DFCD.intColumnWidth,0)
		,ISNULL(bitIsRequired,0)
		,ISNULL(bitIsEmail,0)
		,ISNULL(bitIsAlphaNumeric,0)
		,ISNULL(bitIsNumeric,0)
		,ISNULL(bitIsLengthValidation,0)
		,ISNULL(intMaxLength,0)
		,ISNULL(intMinLength,0)
		,ISNULL(bitFieldMessage,0)
		,ISNULL(vcFieldMessage,'')
	FROM
		DycFormConfigurationDetails DFCD
	INNER JOIN
		CFW_Fld_Master CFM
	ON
		DFCD.numFieldId = CFM.Fld_id
	LEFT JOIN
		CFW_Validation CV
	ON
		CFM.Fld_id = CV.numFieldID
	WHERE
		DFCD.numDomainId=@numDomainID 
		AND DFCD.numUserCntID=@numUserCntID 
		AND DFCD.numFormId=135 
		AND DFCD.numViewId=@numViewID
		AND ISNULL(DFCD.bitCustom,0) = 1

	IF (SELECT COUNT(*) FROM @TempFieldsLeft) = 0
	BEGIN
		INSERT INTO @TempFieldsLeft
		(
			ID
			,numFieldID
			,vcFieldName
			,vcOrigDbColumnName
			,vcListItemType
			,numListID
			,bitAllowSorting
			,bitAllowFiltering
			,vcAssociatedControlType
			,vcLookBackTableName
			,bitCustomField
			,intRowNum
			,intColumnWidth
		)
		SELECT
			CONCAT(135,'~',DFM.numFieldID,'~',0)
			,DFM.numFieldID
			,DFFM.vcFieldName
			,DFM.vcOrigDbColumnName
			,ISNULL(DFM.vcListItemType,'')
			,ISNULL(DFM.numListID,0)
			,DFFM.bitAllowSorting
			,DFFM.bitAllowFiltering
			,DFM.vcAssociatedControlType
			,DFM.vcLookBackTableName
			,0
			,DFFM.[order]
			,100
		FROM
			DycFormField_Mapping DFFM
		INNER JOIN
			DycFieldMaster DFM
		ON
			DFFM.numFieldID = DFM.numFieldId
		WHERE 
			DFFM.numFormId=135 
			AND ISNULL(DFFM.bitDeleted,0) = 0
			AND ISNULL(DFFM.bitSettingField,0) = 1
			AND ISNULL(DFFM.bitDefault,0) = 1
	END

	DECLARE @j INT = 0
	DECLARE @jCount INT
	DECLARE @numFieldId NUMERIC(18,0)
	DECLARE @vcFieldName AS VARCHAR(50)                                                                                                
	DECLARE @vcAssociatedControlType VARCHAR(10)  
	DECLARE @vcDbColumnName VARCHAR(100)                                                
	--DECLARE @numListID AS NUMERIC(9)       
	DECLARE @bitCustomField BIT       
	SET @jCount = (SELECT COUNT(*) FROM @TempFieldsLeft)
	DECLARE @vcCustomFields VARCHAR(MAX) = ''
	DECLARE @vcCustomWhere VARCHAR(MAX) = ''

	WHILE @j <= @jCount
	BEGIN
		SELECT
			@numFieldId=numFieldID,
			@vcFieldName = vcFieldName,
			@vcAssociatedControlType = vcAssociatedControlType,
			@vcDbColumnName = CONCAT('Cust',numFieldID),
			@bitCustomField=ISNULL(bitCustomField,0)
		FROM 
			@TempFieldsLeft
		WHERE 
			numPKID = @j     
		
		
			
		IF @bitCustomField = 1
		BEGIN         
			IF @vcAssociatedControlType = 'TextBox' OR @vcAssociatedControlType = 'TextArea' 
			BEGIN
				SET @vcCustomFields = CONCAT(@vcCustomFields,',CFW',@numFieldId,'.Fld_Value  [',@vcDbColumnName,']')
             
				SET @vcCustomWhere = CONCAT(@vcCustomWhere,' left Join CFW_FLD_Values_Opp CFW',@numFieldId
								,' ON CFW' , @numFieldId , '.Fld_Id='
								,@numFieldId
								, ' and CFW' , @numFieldId
								, '.RecId=T1.numOppID')                                                         
			END   
			ELSE IF @vcAssociatedControlType = 'CheckBox' 
			BEGIN
				SET @vcCustomFields =CONCAT( @vcCustomFields
					, ',case when isnull(CFW'
					, @numFieldId
					, '.Fld_Value,0)=0 then 0 when isnull(CFW'
					, @numFieldId
					, '.Fld_Value,0)=1 then 1 end   ['
					,  @vcDbColumnName
					, ']')               
 
				SET @vcCustomWhere = CONCAT(@vcCustomWhere
					, ' left Join CFW_FLD_Values_Opp CFW'
					, @numFieldId
					, ' ON CFW',@numFieldId,'.Fld_Id='
					, @numFieldId
					, ' and CFW'
					, @numFieldId
					, '.RecId=T1.numOppID   ')                                                    
			END                
			ELSE IF @vcAssociatedControlType = 'DateField' 
			BEGIN
				SET @vcCustomFields = CONCAT(@vcCustomFields
					, ',dbo.FormatedDateFromDate(CFW'
					, @numFieldId
					, '.Fld_Value,'
					, @numDomainId
					, ')  [',@vcDbColumnName ,']' )  
					                  
				SET @vcCustomWhere = CONCAT(@vcCustomWhere
					, ' left Join CFW_FLD_Values_Opp CFW'
					, @numFieldId
					, ' on CFW', @numFieldId, '.Fld_Id='
					, @numFieldId
					, ' and CFW'
					, @numFieldId
					, '.RecId=T1.numOppID   ' )                                                        
			END                
			ELSE IF @vcAssociatedControlType = 'SelectBox' 
			BEGIN
				SET @vcCustomFields = CONCAT(@vcCustomFields,',L',@numFieldId,'.vcData',' [',@vcDbColumnName,']')
					                                                    
				SET @vcCustomWhere = CONCAT(@vcCustomWhere
					, ' left Join CFW_FLD_Values_Opp CFW'
					, @numFieldId
					, ' on CFW',@numFieldId ,'.Fld_Id='
					, @numFieldId
					, ' and CFW'
					, @numFieldId
					, '.RecId=T1.numOppID    ')     
					                                                    
				SET @vcCustomWhere = CONCAT(@vcCustomWhere
					, ' left Join ListDetails L'
					, @numFieldId
					, ' on L'
					, @numFieldId
					, '.numListItemID=CFW'
					, @numFieldId
					, '.Fld_Value' )              
			END
			ELSE
			BEGIN
				SET @vcCustomFields = CONCAT(@vcCustomFields,CONCAT(',dbo.GetCustFldValueOpp(',@numFieldId,',T1.numOppID)'),' [',@vcDbColumnName,']')
			END 
		END

		SET @j = @j + 1
	END
	
	CREATE TABLE #TEMPMSRecords
	(
		numOppID NUMERIC(18,0)
		,numOppItemID NUMERIC(18,0)
	)

	DECLARE @vcSQL NVARCHAR(MAX)
	DECLARE @vcSQLFinal NVARCHAR(MAX)
	
	SET @vcSQL = CONCAT('INSERT INTO #TEMPMSRecords
						(
							numOppID
							,numOppItemID
						)
						SELECT
							OpportunityMaster.numOppId
							,',(CASE WHEN @bitGroupByOrder = 1 THEN '0' ELSE 'OpportunityItems.numoppitemtCode' END),'
						FROM
							OpportunityMaster
						INNER JOIN
							DivisionMaster
						ON
							OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
						INNER JOIN
							CompanyInfo
						ON
							DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
						INNER JOIN 
							OpportunityItems
						ON
							OpportunityMaster.numOppId = OpportunityItems.numOppId
						INNER JOIN
							Item
						ON
							OpportunityItems.numItemCode = Item.numItemCode
						LEFT JOIN
							WareHouseItems
						ON
							OpportunityItems.numWarehouseItmsID = WareHouseItems.numWareHouseItemID
						WHERE
							OpportunityMaster.numDomainId=@numDomainID
							AND OpportunityMaster.tintOppType = 2
							AND OpportunityMaster.tintOppStatus = 1
							AND ISNULL(OpportunityMaster.tintshipped,0) = 0
							AND 1 = (CASE 
										WHEN @numViewID=1 
										THEN (CASE WHEN ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyReceived,0) > 0 THEN 1 ELSE 0 END)
										WHEN @numViewID=2
										THEN (CASE WHEN ISNULL(OpportunityItems.numQtyReceived,0) > 0 AND ISNULL(OpportunityItems.numQtyReceived,0) - ISNULL(OpportunityItems.numUnitHourReceived,0) > 0 THEN 1 ELSE 0 END)
										WHEN @numViewID=3
										THEN (CASE 
												WHEN (CASE 
														WHEN @tintBillType=2 
														THEN ISNULL(OpportunityItems.numQtyReceived,0) 
														WHEN @tintBillType=3 THEN ISNULL(OpportunityItems.numUnitHourReceived,0) 
														ELSE ISNULL(OpportunityItems.numUnitHour,0) 
														END)  - ISNULL((SELECT 
																			SUM(OpportunityBizDocItems.numUnitHour)
																		FROM
																			OpportunityBizDocs
																		INNER JOIN
																			OpportunityBizDocItems
																		ON
																			OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																		WHERE
																			OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
																			AND OpportunityBizDocs.numBizDocId=644
																			AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode),0) > 0 
												THEN 1
												ELSE 0 
											END)
										WHEN @numViewID=4 AND ISNULL(@bitShowOnlyFullyReceived,0) = 1
										THEN (CASE 
												WHEN (SELECT COUNT(*) FROM OpportunityItems OIInner WHERE OIInner.numOppID=OpportunityMaster.numOppID AND ISNULL(OpportunityItems.numUnitHour,0) <> ISNULL(OpportunityItems.numUnitHourReceived,0) ) > 0 
												THEN 0 
												ELSE 1 
											END)
										ELSE 1
									END)
							AND 1 = (CASE WHEN @numViewID IN (1,2) AND ISNULL(@numWarehouseID,0) > 0 AND ISNULL(OpportunityItems.numWarehouseItmsID,0) > 0 THEN (CASE WHEN @numWarehouseID=WareHouseItems.numWarehouseID THEN 1 ELSE 0 END) ELSE 1 END)
							AND 1 = (CASE WHEN LEN(ISNULL(@vcOrderStauts,'''')) > 0 THEN (CASE WHEN OpportunityMaster.numStatus ',(CASE WHEN ISNULL(@bitIncludeSearch,0) = 0 THEN 'NOT IN' ELSE 'IN' END),' (SELECT Id FROM dbo.SplitIDs(@vcOrderStauts,'','')) THEN 1 ELSE 0 END) ELSE 1 END)'
							,@vcCustomSearchValue
							,(CASE WHEN @bitGroupByOrder = 1 THEN ' GROUP BY OpportunityMaster.numOppId' ELSE '' END));

	EXEC sp_executesql @vcSQL, N'@numDomainID NUMERIC(18,0), @vcOrderStauts VARCHAR(MAX), @numViewID NUMERIC(18,0), @numWarehouseID NUMERIC(18,0), @bitShowOnlyFullyReceived BIT, @tintBillType TINYINT', @numDomainID, @vcOrderStauts, @numViewID, @numWarehouseID, @bitShowOnlyFullyReceived, @tintBillType;

	SET @numTotalRecords = (SELECT COUNT(*) FROM #TEMPMSRecords)
	
	CREATE TABLE #TEMPResult
	(
		numOppID NUMERIC(18,0)
		,numDivisionID NUMERIC(18,0)
		,numTerID NUMERIC(18,0)
		,numItemCode NUMERIC(18,0)
		,numOppItemID NUMERIC(18,0)
		,numWarehouseItemID NUMERIC(18,0)
		,vcCompanyName VARCHAR(300)
		,vcPoppName VARCHAR(300)
		,numStatus VARCHAR(300)
		,bintCreatedDate VARCHAR(50)
		,numAssignedTo VARCHAR(100)
		,numRecOwner VARCHAR(100)
		,vcItemName VARCHAR(300)
		,numOnHand FLOAT
		,numOnOrder FLOAT
		,numAllocation FLOAT
		,numBackOrder FLOAT
		,numBarCodeId VARCHAR(50)
		,vcLocation VARCHAR(100)
		,vcModelID VARCHAR(200)
		,vcItemDesc VARCHAR(2000)
		,numUnitHour FLOAT
		,numUOMId VARCHAR(50)
		,vcAttributes VARCHAR(300)
		,vcPathForTImage VARCHAR(300)
		,numUnitHourReceived FLOAT
		,numQtyReceived FLOAT
		,vcNotes VARCHAR(MAX)
		,SerialLotNo VARCHAR(MAX)
		,vcSKU VARCHAR(50)
		,numRemainingQty FLOAT
		,numQtyToShipReceive FLOAT
		,monPrice DECIMAL(20,5)
		,monTotAmount DECIMAL(20,5)
		,vcBilled FLOAT
	)
	
	IF @bitGroupByOrder = 1
	BEGIN
		SET @vcSQLFinal = CONCAT('INSERT INTO #TEMPResult
								(
									numOppID
									,numDivisionID
									,numTerID
									,vcCompanyName
									,vcPoppName
									,bintCreatedDate
									,numStatus
									,numAssignedTo
									,numRecOwner
								)
								SELECT
									OpportunityMaster.numOppID
									,DivisionMaster.numDivisionID
									,ISNULL(DivisionMaster.numTerID,0) numTerID
									,ISNULL(CompanyInfo.vcCompanyName,'''')
									,ISNULL(OpportunityMaster.vcPoppName,'''')
									,dbo.FormatedDateFromDate(DATEADD(MINUTE,@ClientTimeZoneOffset * -1,OpportunityMaster.bintCreatedDate),@numDomainID)
									,dbo.GetListIemName(OpportunityMaster.numStatus)
									,dbo.fn_GetContactName(OpportunityMaster.numAssignedTo)
									,dbo.fn_GetContactName(OpportunityMaster.numRecOwner)
								FROM
									#TEMPMSRecords TEMPOrder
								INNER JOIN
									OpportunityMaster
								ON
									TEMPOrder.numOppID = OpportunityMaster.numOppId
								INNER JOIN
									DivisionMaster
								ON
									OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
								INNER JOIN
									CompanyInfo
								ON
									DivisionMaster.numCompanyID = CompanyInfo.numCompanyId ORDER BY ',
								(CASE @vcSortOrder
									WHEN 'CompanyInfo.vcCompanyName' THEN 'CompanyInfo.vcCompanyName '
									WHEN 'OpportunityMaster.vcPoppName' THEN 'OpportunityMaster.vcPoppName'
									WHEN 'OpportunityMaster.bintCreatedDate' THEN 'OpportunityMaster.bintCreatedDate'
									WHEN 'OpportunityMaster.numStatus' THEN 'dbo.GetListIemName(OpportunityMaster.numStatus)'
									WHEN 'OpportunityMaster.numAssignedTo' THEN 'dbo.fn_GetContactName(OpportunityMaster.numAssignedTo)'
									WHEN 'OpportunityMaster.numRecOwner' THEN 'dbo.fn_GetContactName(OpportunityMaster.numRecOwner)'
									ELSE 'OpportunityMaster.bintCreatedDate'
								END),' ',(CASE WHEN ISNULL(@vcSortOrder,'') <> '' THEN @vcSortOrder ELSE 'DESC' END),' OFFSET (@numPageIndex-1) * 200 ROWS FETCH NEXT 200 ROWS ONLY')


		EXEC sp_executesql @vcSQLFinal, N'@numDomainID NUMERIC(18,0), @ClientTimeZoneOffset INT, @numPageIndex INT', @numDomainID, @ClientTimeZoneOffset, @numPageIndex;
	END
	ELSE
	BEGIN
		SET @vcSQLFinal = CONCAT('INSERT INTO #TEMPResult
								(
									numOppID
									,numDivisionID
									,numTerID
									,vcCompanyName
									,vcPoppName
									,bintCreatedDate
									,numStatus
									,numAssignedTo
									,numRecOwner
									,numOppItemID
									,numWarehouseItemID
									,numItemCode
									,vcItemName
									,numOnHand
									,numOnOrder
									,numAllocation
									,numBackOrder
									,numBarCodeId
									,vcLocation
									,vcModelID
									,vcItemDesc
									,numUnitHour
									,numUOMId
									,vcAttributes
									,vcPathForTImage
									,numUnitHourReceived
									,numQtyReceived
									,vcNotes
									,SerialLotNo
									,vcSKU
									,numRemainingQty
									,numQtyToShipReceive
									,monPrice
									,monTotAmount
									,vcBilled
								)
								SELECT
									OpportunityMaster.numOppID
									,DivisionMaster.numDivisionID
									,ISNULL(DivisionMaster.numTerID,0) numTerID
									,ISNULL(CompanyInfo.vcCompanyName,'''')
									,ISNULL(OpportunityMaster.vcPoppName,'''')
									,dbo.FormatedDateFromDate(DATEADD(MINUTE,@ClientTimeZoneOffset * -1,OpportunityMaster.bintCreatedDate),@numDomainID)
									,dbo.GetListIemName(OpportunityMaster.numStatus)
									,dbo.fn_GetContactName(OpportunityMaster.numAssignedTo)
									,dbo.fn_GetContactName(OpportunityMaster.numRecOwner)
									,OpportunityItems.numoppitemtCode
									,ISNULL(OpportunityItems.numWarehouseItmsID,0)
									,OpportunityItems.numItemCode
									,ISNULL(Item.vcItemName,'''')
									,ISNULL(numOnHand,0) + ISNULL(numAllocation,0)
									,ISNULL(numOnOrder,0)
									,ISNULL(numAllocation,0)
									,ISNULL(numBackOrder,0)
									,ISNULL(Item.numBarCodeId,'''')
									,ISNULL(WarehouseLocation.vcLocation,'''')
									,ISNULL(Item.vcModelID,'''')
									,ISNULL(OpportunityItems.vcItemDesc,'''')
									,ISNULL(OpportunityItems.numUnitHour,0)
									,dbo.fn_GetUOMName(OpportunityItems.numUOMId)
									,ISNULL(OpportunityItems.vcAttributes,'''')
									,ISNULL(Item.vcPathForTImage,'''')
									,ISNULL(OpportunityItems.numUnitHourReceived,0)
									,ISNULL(OpportunityItems.numQtyReceived,0)
									,ISNULL(OpportunityItems.vcNotes,'''')
									,SUBSTRING((SELECT '','' + vcSerialNo + CASE WHEN isnull(Item.bitLotNo,0)=1 THEN '' ('' + CONVERT(VARCHAR(15),whi.numQty) + '')'' ELSE '''' end 
									FROM OppWarehouseSerializedItem oppI JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID=whi.numWareHouseItmsDTLID where oppI.numOppID=OpportunityItems.numOppId and oppI.numOppItemID=OpportunityItems.numoppitemtCode 
									ORDER BY vcSerialNo FOR XML PATH('''')),2,200000)
									,ISNULL(Item.vcSKU,'''')
									,(CASE 
										WHEN @numViewID=1 THEN ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyReceived,0) 
										WHEN @numViewID=2 THEN ISNULL(OpportunityItems.numQtyReceived,0) - ISNULL(OpportunityItems.numUnitHourReceived,0) 
										WHEN @numViewID=3 THEN (CASE 
																	WHEN @tintBillType=2 
																	THEN ISNULL(OpportunityItems.numQtyReceived,0) 
																	WHEN @tintBillType=3 THEN ISNULL(OpportunityItems.numUnitHourReceived,0) 
																	ELSE ISNULL(OpportunityItems.numUnitHour,0) 
																END)  - ISNULL((SELECT 
																					SUM(OpportunityBizDocItems.numUnitHour)
																				FROM
																					OpportunityBizDocs
																				INNER JOIN
																					OpportunityBizDocItems
																				ON
																					OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																				WHERE
																					OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
																					AND OpportunityBizDocs.numBizDocId=644
																					AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode),0)
										ELSE ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numUnitHourReceived,0) 
									END)
									,(CASE 
										WHEN @numViewID=1 THEN ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyReceived,0) 
										WHEN @numViewID=2 THEN ISNULL(OpportunityItems.numQtyReceived,0) - ISNULL(OpportunityItems.numUnitHourReceived,0) 
										ELSE ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numUnitHourReceived,0) 
									END)
									,ISNULL(OpportunityItems.monPrice,0)
									,ISNULL(OpportunityItems.monTotAmount,0)
									,ISNULL((SELECT 
												SUM(OpportunityBizDocItems.numUnitHour)
											FROM
												OpportunityBizDocs
											INNER JOIN
												OpportunityBizDocItems
											ON
												OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
											WHERE
												OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
												AND OpportunityBizDocs.numBizDocId=644
												AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode),0)
								FROM
									#TEMPMSRecords TEMPOrder
								INNER JOIN
									OpportunityMaster
								ON
									TEMPOrder.numOppID = OpportunityMaster.numOppId
								INNER JOIN
									DivisionMaster
								ON
									OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
								INNER JOIN
									CompanyInfo
								ON
									DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
								INNER JOIN 
									OpportunityItems
								ON
									OpportunityMaster.numOppId = OpportunityItems.numOppId
									AND TEMPOrder.numOppItemID = OpportunityItems.numoppitemtCode
								INNER JOIN
									Item
								ON
									OpportunityItems.numItemCode = Item.numItemCode
								LEFT JOIN
									WareHouseItems
								ON
									OpportunityItems.numWarehouseItmsID = WareHouseItems.numWareHouseItemID
								LEFT JOIN
									WarehouseLocation
								ON
									WareHouseItems.numWLocationID = WarehouseLocation.numWLocationID ORDER BY ',
								(CASE @vcSortOrder
									WHEN 'CompanyInfo.vcCompanyName' THEN 'CompanyInfo.vcCompanyName '
									WHEN 'OpportunityMaster.vcPoppName' THEN 'OpportunityMaster.vcPoppName'
									WHEN 'OpportunityMaster.bintCreatedDate' THEN 'OpportunityMaster.bintCreatedDate'
									WHEN 'OpportunityMaster.numStatus' THEN 'dbo.GetListIemName(OpportunityMaster.numStatus)'
									WHEN 'OpportunityMaster.numAssignedTo' THEN 'dbo.fn_GetContactName(OpportunityMaster.numAssignedTo)'
									WHEN 'OpportunityMaster.numRecOwner' THEN 'dbo.fn_GetContactName(OpportunityMaster.numRecOwner)'
									WHEN 'numOnHand' THEN 'ISNULL(WareHouseItems.numOnHand,0) + ISNULL(WareHouseItems.numAllocation,0)'
									WHEN 'numOnOrder' THEN 'ISNULL(WareHouseItems.numOnOrder,0)'
									WHEN 'numAllocation' THEN 'ISNULL(WareHouseItems.numAllocation,0)'
									WHEN 'numBackOrder' THEN 'ISNULL(WareHouseItems.numBackOrder,0)'
									WHEN 'numItemCode' THEN 'Item.numItemCode'
									WHEN 'vcItemName' THEN 'Item.vcItemName'
									WHEN 'numUnitHour' THEN 'OpportunityItems.numUnitHour'
									WHEN 'numUnitHourReceived' THEN 'OpportunityItems.numUnitHourReceived'
									ELSE 'OpportunityMaster.bintCreatedDate'
								END),' ',(CASE WHEN ISNULL(@vcSortOrder,'') <> '' THEN @vcSortOrder ELSE 'DESC' END),' OFFSET (@numPageIndex-1) * 200 ROWS FETCH NEXT 200 ROWS ONLY')

		EXEC sp_executesql @vcSQLFinal, N'@numDomainID NUMERIC(18,0), @numViewID NUMERIC(18,0), @ClientTimeZoneOffset INT, @numPageIndex INT, @tintBillType TINYINT', @numDomainID, @numViewID, @ClientTimeZoneOffset, @numPageIndex, @tintBillType;
	END
	
	SET @vcSQLFinal = CONCAT('SELECT T1.*',@vcCustomFields,' FROM #TEMPResult T1',@vcCustomWhere)
	EXEC sp_executesql @vcSQLFinal

	SELECT * FROM @TempFieldsLeft ORDER BY intRowNum

	IF OBJECT_ID('tempdb..#TEMPMSRecords') IS NOT NULL DROP TABLE #TEMPMSRecords
	IF OBJECT_ID('tempdb..#TEMPResult') IS NOT NULL DROP TABLE #TEMPResult
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_MassPurchaseFulfillmentConfiguration_Save')
DROP PROCEDURE dbo.USP_MassPurchaseFulfillmentConfiguration_Save
GO
CREATE PROCEDURE [dbo].[USP_MassPurchaseFulfillmentConfiguration_Save]
(
	@numDomainID NUMERIC(18,0)
    ,@numUserCntID NUMERIC(18,0)
	,@bitGroupByOrderForReceive BIT
	,@bitGroupByOrderForPutAway BIT
	,@bitGroupByOrderForBill BIT
	,@bitGroupByOrderForClose BIT
	,@bitShowOnlyFullyReceived BIT
	,@bitReceiveBillOnClose BIT
	,@tintScanValue TINYINT
	,@tintBillType TINYINT
)
AS 
BEGIN
	IF EXISTS (SELECT ID FROM MassPurchaseFulfillmentConfiguration WHERE numDomainID=@numDomainID AND numUserCntID=@numUserCntID)
	BEGIN
		UPDATE
			MassPurchaseFulfillmentConfiguration
		SET
			bitGroupByOrderForReceive=@bitGroupByOrderForReceive
			,bitGroupByOrderForPutAway=@bitGroupByOrderForPutAway
			,bitGroupByOrderForBill=@bitGroupByOrderForBill
			,bitGroupByOrderForClose=@bitGroupByOrderForClose
			,bitShowOnlyFullyReceived=@bitShowOnlyFullyReceived
			,bitReceiveBillOnClose=@bitReceiveBillOnClose
			,tintScanValue=@tintScanValue
			,tintBillType=@tintBillType
		WHERE
			numDomainID=@numDomainID 
			AND numUserCntID=@numUserCntID
	END
	ELSE
	BEGIN
		INSERT INTO MassPurchaseFulfillmentConfiguration
		(
			numDomainID 
			,numUserCntID
			,bitGroupByOrderForReceive
			,bitGroupByOrderForPutAway
			,bitGroupByOrderForBill
			,bitGroupByOrderForClose
			,bitShowOnlyFullyReceived
			,bitReceiveBillOnClose
			,tintScanValue
			,tintBillType
		)
		VALUES
		(
			@numDomainID 
			,@numUserCntID
			,@bitGroupByOrderForReceive
			,@bitGroupByOrderForPutAway
			,@bitGroupByOrderForBill
			,@bitGroupByOrderForClose
			,@bitShowOnlyFullyReceived
			,@bitReceiveBillOnClose
			,@tintScanValue
			,@tintBillType
		)
	END
END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_MassSalesFulfillment_GetOrderItems')
DROP PROCEDURE dbo.USP_MassSalesFulfillment_GetOrderItems
GO

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_MassSalesFulfillment_GetOrderItems]
(
	@numDomainID NUMERIC(18,0)
	,@numUserCntID NUMERIC(18,0)
	,@numOppID NUMERIC(18,0)
	,@tintMode TINYINT
	,@vcSelectedRecords VARCHAR(MAX)
)
AS 
BEGIN
	DECLARE @tintPackingMode TINYINT
	DECLARE @tintInvoicingType TINYINT
	DECLARE @numDefaultShippingBizDoc NUMERIC(18,0)

	SELECT
		@numDefaultShippingBizDoc = ISNULL(numDefaultSalesShippingDoc,0)
	FROM
		Domain
	WHERE
		numDomainID=@numDomainID

	SELECT
		@tintPackingMode=tintPackingMode
		,@tintInvoicingType=tintInvoicingType
	FROM
		MassSalesFulfillmentConfiguration
	WHERE
		MassSalesFulfillmentConfiguration.numDomainID = @numDomainID 
		AND MassSalesFulfillmentConfiguration.numUserCntID = @numUserCntID

	IF ISNULL(@vcSelectedRecords,'') <> ''
	BEGIN
		DECLARE @TempItems TABLE
		(	
			numOppItemID NUMERIC(18,0),
			numUnitHour FLOAT
		)

		INSERT INTO @TempItems
		(
			numOppItemID,
			numUnitHour
		)
		SELECT
			CAST(SUBSTRING(OutParam,0,CHARINDEX('-',OutParam)) AS NUMERIC(18,0))
			,CAST(SUBSTRING(OutParam,CHARINDEX('-',OutParam) + 1,LEN(OutParam)) AS FLOAT)
		FROM
			dbo.SplitString(@vcSelectedRecords,',')

		IF @tintMode = 1 -- For ship orders
		BEGIN
			SELECT
				numoppitemtCode
				,ISNULL(T1.numUnitHour,0) numUnitHour
				,monPrice
			FROM
				OpportunityItems
			INNER JOIN
				@TempItems T1
			ON
				OpportunityItems.numoppitemtCode=T1.numOppItemID
			WHERE
				numOppId = @numOppID
				AND (CASE 
						WHEN ISNULL(@tintPackingMode,1) = 3 
						THEN ISNULL(OpportunityItems.numQtyPicked,0) - ISNULL((SELECT 
																					SUM(SRI.intBoxQty)
																				FROM 
																					ShippingReport SR 
																				INNER JOIN 
																					ShippingBox SB 
																				ON 
																					SR.numShippingReportId=SB.numShippingReportId 
																				INNER JOIN 
																					ShippingReportItems SRI 
																				ON 
																					SB.numBoxID = SRI.numBoxID 
																				INNER JOIN
																					OpportunityBizDocItems OBDI
																				ON
																					 SRI.numOppBizDocItemID = OBDI.numOppBizDocItemID
																				WHERE 
																					SR.numDomainId = @numDomainID
																					AND SR.numOppID = @numOppID
																					AND LEN(ISNULL(SB.vcTrackingNumber,'')) > 0
																					AND OBDI.numOppItemID = OpportunityItems.numoppitemtCode),0) 
						WHEN ISNULL(@tintPackingMode,1) = 2 
						THEN ISNULL(OpportunityItems.numQtyPicked,0) - ISNULL(OpportunityItems.numQtyShipped,0) 
						ELSE ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyShipped,0) 
					END) > 0				
		END
		ELSE IF @tintMode = 2 -- For invoicing orders
		BEGIN
			SELECT
				numoppitemtCode
				,ISNULL(T1.numUnitHour,0) numUnitHour
				,monPrice
			FROM
				OpportunityItems
			INNER JOIN
				@TempItems T1
			ON
				OpportunityItems.numoppitemtCode=T1.numOppItemID
			WHERE
				numOppId = @numOppID
				AND (CASE 
							WHEN ISNULL(@tintInvoicingType,0)=1 
							THEN ISNULL(OpportunityItems.numQtyShipped,0) 
							ELSE ISNULL(OpportunityItems.numUnitHour,0) 
						END) - ISNULL((SELECT 
											SUM(OpportunityBizDocItems.numUnitHour)
										FROM
											OpportunityBizDocs
										INNER JOIN
											OpportunityBizDocItems
										ON
											OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
										WHERE
											OpportunityBizDocs.numOppId = @numOppID
											AND OpportunityBizDocs.numBizDocId=287
											AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode),0) > 0
		END
		ELSE IF @tintMode = 3 -- For deffault shipping bizdoc
		BEGIN
			IF ISNULL(@numDefaultShippingBizDoc,0) > 0
			BEGIN
				SELECT
					numoppitemtCode
					,ISNULL(T1.numUnitHour,0) numUnitHour
					,monPrice
				FROM
					OpportunityItems
				INNER JOIN
					@TempItems T1
				ON
					OpportunityItems.numoppitemtCode=T1.numOppItemID
				WHERE
					numOppId = @numOppID
					AND ISNULL(OpportunityItems.numUnitHour,0)  - ISNULL((SELECT 
																		SUM(OpportunityBizDocItems.numUnitHour)
																	FROM
																		OpportunityBizDocs
																	INNER JOIN
																		OpportunityBizDocItems
																	ON
																		OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																	WHERE
																		OpportunityBizDocs.numOppId = @numOppID
																		AND OpportunityBizDocs.numBizDocId=@numDefaultShippingBizDoc
																		AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode),0) > 0
			END
			ELSE
			BEGIN
				RAISERROR('DEFAULT_SHIPPING_BIZDOC_NOT_CONFIGURED',16,1)
			END
			
		END
	END
	ELSE
	BEGIN
		IF @tintMode = 1 -- For ship orders
		BEGIN
			SELECT
				numoppitemtCode
				,(CASE WHEN ISNULL(@tintPackingMode,1) = 2 THEN ISNULL(OpportunityItems.numQtyPicked,0) - ISNULL(OpportunityItems.numQtyShipped,0) ELSE ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyShipped,0) END) numUnitHour
				,monPrice
			FROM
				OpportunityItems
			WHERE
				numOppId = @numOppID
				AND (CASE WHEN ISNULL(@tintPackingMode,1) = 2 THEN ISNULL(OpportunityItems.numQtyPicked,0) - ISNULL(OpportunityItems.numQtyShipped,0) ELSE ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyShipped,0) END) > 0
		END
		ELSE IF @tintMode = 2 -- For invoicing orders
		BEGIN
			SELECT
				numoppitemtCode
				,(CASE 
						WHEN ISNULL(@tintInvoicingType,0)=1 
						THEN ISNULL(OpportunityItems.numQtyShipped,0) 
						ELSE ISNULL(OpportunityItems.numUnitHour,0) 
					END) - ISNULL((SELECT 
										SUM(OpportunityBizDocItems.numUnitHour)
									FROM
										OpportunityBizDocs
									INNER JOIN
										OpportunityBizDocItems
									ON
										OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
									WHERE
										OpportunityBizDocs.numOppId = @numOppID
										AND OpportunityBizDocs.numBizDocId=287
										AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode),0) numUnitHour
				,monPrice
			FROM
				OpportunityItems
			WHERE
				numOppId = @numOppID
				AND (CASE 
							WHEN ISNULL(@tintInvoicingType,0)=1 
							THEN ISNULL(OpportunityItems.numQtyShipped,0) 
							ELSE ISNULL(OpportunityItems.numUnitHour,0) 
						END) - ISNULL((SELECT 
											SUM(OpportunityBizDocItems.numUnitHour)
										FROM
											OpportunityBizDocs
										INNER JOIN
											OpportunityBizDocItems
										ON
											OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
										WHERE
											OpportunityBizDocs.numOppId = @numOppID
											AND OpportunityBizDocs.numBizDocId=287
											AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode),0) > 0
		END
		ELSE IF @tintMode = 3 -- For deffault shipping bizdoc
		BEGIN
			IF ISNULL(@numDefaultShippingBizDoc,0) > 0
			BEGIN
				SELECT
					numoppitemtCode
					,ISNULL(OpportunityItems.numUnitHour,0) - ISNULL((SELECT 
																		SUM(OpportunityBizDocItems.numUnitHour)
																	FROM
																		OpportunityBizDocs
																	INNER JOIN
																		OpportunityBizDocItems
																	ON
																		OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																	WHERE
																		OpportunityBizDocs.numOppId = @numOppID
																		AND OpportunityBizDocs.numBizDocId=@numDefaultShippingBizDoc
																		AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode),0) numUnitHour
					,monPrice
				FROM
					OpportunityItems
				WHERE
					numOppId = @numOppID
					AND (CASE 
								WHEN ISNULL(@tintInvoicingType,0)=1 
								THEN ISNULL(OpportunityItems.numQtyShipped,0) 
								ELSE ISNULL(OpportunityItems.numUnitHour,0) 
							END) - ISNULL((SELECT 
												SUM(OpportunityBizDocItems.numUnitHour)
											FROM
												OpportunityBizDocs
											INNER JOIN
												OpportunityBizDocItems
											ON
												OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
											WHERE
												OpportunityBizDocs.numOppId = @numOppID
												AND OpportunityBizDocs.numBizDocId=@numDefaultShippingBizDoc
												AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode),0) > 0
			END
			ELSE
			BEGIN
				RAISERROR('DEFAULT_SHIPPING_BIZDOC_NOT_CONFIGURED',16,1)
			END
		END
	END

END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_MassSalesFulfillment_GetRecords')
DROP PROCEDURE dbo.USP_MassSalesFulfillment_GetRecords
GO

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_MassSalesFulfillment_GetRecords]
(
	@numDomainID NUMERIC(18,0)
	,@numUserCntID NUMERIC(18,0)
	,@ClientTimeZoneOffset INT
	,@numViewID NUMERIC(18,0)
	,@numWarehouseID NUMERIC(18,0)
	,@bitGroupByOrder BIT
	,@bitRemoveBO BIT
	,@numShippingZone NUMERIC(18,0)
	,@bitIncludeSearch BIT
	,@vcOrderStauts VARCHAR(MAX)
	,@tintFilterBy TINYINT
	,@vcFilterValue VARCHAR(MAX)
	,@vcCustomSearchValue VARCHAR(MAX)
	,@numBatchID NUMERIC(18,0)
	,@tintPackingViewMode TINYINT
	,@tintPrintBizDocViewMode TINYINT
	,@tintPendingCloseFilter TINYINT
	,@numPageIndex INT
	,@vcSortColumn VARCHAR(100) OUTPUT
	,@vcSortOrder VARCHAR(4) OUTPUT
	,@numTotalRecords NUMERIC(18,0) OUTPUT
)
AS 
BEGIN
	IF @numViewID = 4 OR @numViewID = 5 OR @numViewID = 6
	BEGIN
		SET @bitGroupByOrder = 1
	END

	SET @vcCustomSearchValue = REPLACE(@vcCustomSearchValue,'OpportunityMaster.dtItemReceivedDate','(SELECT TOP 1 OMInner.dtItemReceivedDate FROM OpportunityMaster OMInner INNER JOIN OpportunityItems OIInner ON OMInner.numOppID=OIInner.numOppID WHERE OMInner.tintOppType=2 AND OMInner.tintOppStatus=1 AND OIInner.numItemCode=OpportunityItems.numItemCode ORDER BY OMInner.dtItemReceivedDate DESC)')

	DECLARE @TempFieldsLeft TABLE
	(
		numPKID INT IDENTITY(1,1)
		,ID VARCHAR(50)
		,numFieldID NUMERIC(18,0)
		,vcFieldName VARCHAR(100)
		,vcOrigDbColumnName VARCHAR(100)
		,vcListItemType VARCHAR(10)
		,numListID NUMERIC(18,0)
		,bitAllowSorting BIT
		,bitAllowFiltering BIT
		,vcAssociatedControlType VARCHAR(20)
		,vcLookBackTableName VARCHAR(100)
		,bitCustomField BIT
		,intRowNum INT
		,intColumnWidth FLOAT
		,bitIsRequired BIT
		,bitIsEmail BIT
		,bitIsAlphaNumeric BIT
		,bitIsNumeric BIT
		,bitIsLengthValidation BIT
		,intMaxLength INT
		,intMinLength INT
		,bitFieldMessage BIT
		,vcFieldMessage VARCHAR(MAX)
		,Grp_id INT
	)

	CREATE TABLE #TEMPResult
	(
		numOppID NUMERIC(18,0)
		,numDivisionID NUMERIC(18,0)
		,numTerID NUMERIC(18,0)
		,numOppItemID NUMERIC(18,0)
		,numOppBizDocID NUMERIC(18,0)
		,bintCreatedDate VARCHAR(50)
		,numItemCode NUMERIC(18,0)
		,vcCompanyName VARCHAR(300)
		,vcPoppName VARCHAR(300)
		,numAssignedTo VARCHAR(100)
		,numRecOwner VARCHAR(100)
		,vcBizDocID VARCHAR(300)
		,numStatus VARCHAR(300)
		,txtComments VARCHAR(MAX)
		,numBarCodeId VARCHAR(50)
		,vcLocation VARCHAR(100)
		,vcItemName VARCHAR(300)
		,vcItemDesc VARCHAR(2000)
		,numQtyShipped FLOAT
		,numUnitHour FLOAT
		,monAmountPaid DECIMAL(20,5)
		,monAmountToPay DECIMAL(20,5)
		,numItemClassification VARCHAR(100)
		,vcSKU VARCHAR(50)
		,charItemType VARCHAR(50)
		,vcAttributes VARCHAR(300)
		,vcNotes VARCHAR(MAX)
		,vcItemReleaseDate DATE
		,dtItemReceivedDate VARCHAR(50)
		,intUsedShippingCompany VARCHAR(300)
		,intShippingCompany VARCHAR(300)
		,numPreferredShipVia NUMERIC(18,0)
		,numPreferredShipService NUMERIC(18,0)
		,vcInvoiced FLOAT
		,vcInclusionDetails VARCHAR(MAX)
		,bitPaidInFull BIT
		,numRemainingQty FLOAT
		,numAge VARCHAR(50)
		,dtAnticipatedDelivery  DATE
		,vcShipStatus VARCHAR(MAX)
		,dtExpectedDate VARCHAR(20)
		,dtExpectedDateOrig DATE
		,numQtyPicked FLOAT
		,numQtyPacked FLOAT
		,vcPaymentStatus VARCHAR(MAX)
		,numShipRate VARCHAR(40)
	)

	IF @bitGroupByOrder = 1
	BEGIN
		IF @vcSortColumn NOT IN ('CompanyInfo.vcCompanyName','OpportunityBizDocs.monAmountPaid','OpportunityBizDocs.vcBizDocID','OpportunityMaster.bintCreatedDate','OpportunityMaster.bitPaidInFull','OpportunityMaster.numAge','OpportunityMaster.numStatus','OpportunityMaster.vcPoppName','OpportunityMaster.dtItemReceivedDate','OpportunityMaster.numAssignedTo','OpportunityMaster.numRecOwner')
		BEGIN
			SET @vcSortColumn = 'OpportunityMaster.bintCreatedDate'
			SET @vcSortOrder = 'DESC'
		END
	END
	ELSE 
	BEGIN
		IF @vcSortColumn NOT IN ('CompanyInfo.vcCompanyName'
								,'Item.charItemType'
								,'Item.numBarCodeId'
								,'Item.numItemClassification'
								,'Item.vcSKU'
								,'OpportunityBizDocs.monAmountPaid'
								,'OpportunityMaster.dtExpectedDate'
								,'OpportunityItems.numQtyPacked'
								,'OpportunityItems.numQtyPicked'
								,'OpportunityItems.numQtyShipped'
								,'OpportunityItems.numRemainingQty'
								,'OpportunityItems.numUnitHour'
								,'OpportunityItems.vcInvoiced'
								,'OpportunityItems.vcItemName'
								,'OpportunityItems.vcItemReleaseDate'
								,'OpportunityMaster.bintCreatedDate'
								,'OpportunityMaster.bitPaidInFull'
								,'OpportunityMaster.numAge'
								,'OpportunityMaster.numStatus'
								,'OpportunityMaster.vcPoppName'
								,'WareHouseItems.vcLocation'
								,'OpportunityMaster.dtItemReceivedDate'
								,'OpportunityBizDocs.vcBizDocID')
		BEGIN
			SET @vcSortColumn = 'OpportunityMaster.bintCreatedDate'
			SET @vcSortOrder = 'DESC'
		END
	END

	DECLARE @tintPackingMode TINYINT
	DECLARE @tintInvoicingType TINYINT
	DECLARE @tintCommitAllocation TINYINT
	DECLARE @numShippingServiceItemID NUMERIC(18,0)
	DECLARE @numDefaultSalesShippingDoc NUMERIC(18,0)
	SELECT 
		@tintCommitAllocation=ISNULL(tintCommitAllocation,1) 
		,@numShippingServiceItemID=ISNULL(numShippingServiceItemID,0)
		,@numDefaultSalesShippingDoc=ISNULL(numDefaultSalesShippingDoc,0)
	FROM 
		Domain 
	WHERE 
		numDomainId=@numDomainID 

	IF @numViewID = 2 AND @tintPackingViewMode=3 AND @numDefaultSalesShippingDoc = 0
	BEGIN
		RAISERROR('DEFAULT_SHIPPING_BIZDOC_NOT_CONFIGURED',16,1)
		RETURN
	END

	SELECT
		@tintPackingMode=tintPackingMode
		,@tintInvoicingType=tintInvoicingType
	FROM
		MassSalesFulfillmentConfiguration
	WHERE
		numDomainID = @numDomainID 
		AND numUserCntID = @numUserCntID

	INSERT INTO @TempFieldsLeft
	(
		ID
		,numFieldID
		,vcFieldName
		,vcOrigDbColumnName
		,vcListItemType
		,numListID
		,bitAllowSorting
		,bitAllowFiltering
		,vcAssociatedControlType
		,vcLookBackTableName
		,bitCustomField
		,intRowNum
		,intColumnWidth
		,bitIsRequired
		,bitIsEmail
		,bitIsAlphaNumeric
		,bitIsNumeric
		,bitIsLengthValidation
		,intMaxLength
		,intMinLength
		,bitFieldMessage
		,vcFieldMessage
		,Grp_id
	)
	SELECT
		CONCAT(141,'~',DFM.numFieldID,'~',0)
		,DFM.numFieldID
		,(CASE WHEN @numViewID=6 AND DFM.numFieldID=248 THEN 'BizDoc' ELSE DFFM.vcFieldName END)
		,DFM.vcOrigDbColumnName
		,ISNULL(DFM.vcListItemType,'')
		,ISNULL(DFM.numListID,0)
		,DFFM.bitAllowSorting
		,DFFM.bitAllowFiltering
		,DFM.vcAssociatedControlType
		,DFM.vcLookBackTableName
		,0
		,DFCD.intRowNum
		,ISNULL(DFCD.intColumnWidth,0)
		,0
		,0
		,0
		,0
		,0
		,0
		,0
		,0
		,''
		,0
	FROM
		DycFormConfigurationDetails DFCD
	INNER JOIN
		DycFormField_Mapping DFFM
	ON
		DFCD.numFormId = DFFM.numFormID
		AND DFCD.numFieldId = DFFM.numFieldID
	INNER JOIN
		DycFieldMaster DFM
	ON
		DFFM.numFieldID = DFM.numFieldId
	WHERE 
		DFCD.numDomainId=@numDomainID 
		AND DFCD.numUserCntID=@numUserCntID 
		AND DFCD.numFormId=141 
		AND DFCD.numViewId=@numViewID
		AND ISNULL(DFFM.bitDeleted,0) = 0
		AND ISNULL(DFFM.bitSettingField,0) = 1
		AND ISNULL(DFCD.bitCustom,0) = 0
	UNION
	SELECT
		CONCAT(141,'~',CFM.Fld_id,'~',0)
		,CFM.Fld_id
		,CFM.Fld_label
		,CONCAT('CUST',CFM.Fld_id)
		,''
		,ISNULL(CFM.numListID,0)
		,0
		,0
		,CFM.Fld_type
		,''
		,1
		,DFCD.intRowNum
		,ISNULL(DFCD.intColumnWidth,0)
		,ISNULL(bitIsRequired,0)
		,ISNULL(bitIsEmail,0)
		,ISNULL(bitIsAlphaNumeric,0)
		,ISNULL(bitIsNumeric,0)
		,ISNULL(bitIsLengthValidation,0)
		,ISNULL(intMaxLength,0)
		,ISNULL(intMinLength,0)
		,ISNULL(bitFieldMessage,0)
		,ISNULL(vcFieldMessage,'')
		,CFM.Grp_id
	FROM
		DycFormConfigurationDetails DFCD
	INNER JOIN
		CFW_Fld_Master CFM
	ON
		DFCD.numFieldId = CFM.Fld_id
	LEFT JOIN
		CFW_Validation CV
	ON
		CFM.Fld_id = CV.numFieldID
	WHERE
		DFCD.numDomainId=@numDomainID 
		AND DFCD.numUserCntID=@numUserCntID 
		AND DFCD.numFormId=141 
		AND DFCD.numViewId=@numViewID
		AND ISNULL(DFCD.bitCustom,0) = 1

	IF (SELECT COUNT(*) FROM @TempFieldsLeft) = 0
	BEGIN
		INSERT INTO @TempFieldsLeft
		(
			ID
			,numFieldID
			,vcFieldName
			,vcOrigDbColumnName
			,vcListItemType
			,numListID
			,bitAllowSorting
			,bitAllowFiltering
			,vcAssociatedControlType
			,vcLookBackTableName
			,bitCustomField
			,intRowNum
			,intColumnWidth
		)
		SELECT
			CONCAT(141,'~',DFM.numFieldID,'~',0)
			,DFM.numFieldID
			,DFFM.vcFieldName
			,DFM.vcOrigDbColumnName
			,ISNULL(DFM.vcListItemType,'')
			,ISNULL(DFM.numListID,0)
			,DFFM.bitAllowSorting
			,DFFM.bitAllowFiltering
			,DFM.vcAssociatedControlType
			,DFM.vcLookBackTableName
			,0
			,DFFM.[order]
			,100
		FROM
			DycFormField_Mapping DFFM
		INNER JOIN
			DycFieldMaster DFM
		ON
			DFFM.numFieldID = DFM.numFieldId
		WHERE 
			DFFM.numFormId=141 
			AND ISNULL(DFFM.bitDeleted,0) = 0
			AND ISNULL(DFFM.bitSettingField,0) = 1
			AND ISNULL(DFFM.bitDefault,0) = 1
	END

	DECLARE @j INT = 0
	DECLARE @jCount INT
	DECLARE @numFieldId NUMERIC(18,0)
	DECLARE @vcFieldName AS VARCHAR(50)                                                                                                
	DECLARE @vcAssociatedControlType VARCHAR(10)  
	DECLARE @vcDbColumnName VARCHAR(100)
	DECLARE @Grp_id INT                                          
	--DECLARE @numListID AS NUMERIC(9)       
	DECLARE @bitCustomField BIT       
	SET @jCount = (SELECT COUNT(*) FROM @TempFieldsLeft)
	DECLARE @vcCustomFields VARCHAR(MAX) = ''
	DECLARE @vcCustomWhere VARCHAR(MAX) = ''

	WHILE @j <= @jCount
	BEGIN
		SELECT
			@numFieldId=numFieldID,
			@vcFieldName = vcFieldName,
			@vcAssociatedControlType = vcAssociatedControlType,
			@vcDbColumnName = CONCAT('Cust',numFieldID),
			@bitCustomField=ISNULL(bitCustomField,0),
			@Grp_id = ISNULL(Grp_id,0)
		FROM 
			@TempFieldsLeft
		WHERE 
			numPKID = @j     
		
		
			
		IF @bitCustomField = 1
		BEGIN         
			IF @Grp_id = 5
			BEGIN
				SET @vcCustomFields = CONCAT(@vcCustomFields,CONCAT(',dbo.GetCustFldValueOppItems(',@numFieldId,',T1.numOppItemID,T1.numItemCode)'),' [',@vcDbColumnName,']')
			END
			ELSE
			BEGIN
				IF @vcAssociatedControlType = 'TextBox' OR @vcAssociatedControlType = 'TextArea' 
				BEGIN
					SET @vcCustomFields = CONCAT(@vcCustomFields,',CFW',@numFieldId,'.Fld_Value  [',@vcDbColumnName,']')
             
					SET @vcCustomWhere = CONCAT(@vcCustomWhere,' left Join CFW_FLD_Values_Opp CFW',@numFieldId
									,' ON CFW' , @numFieldId , '.Fld_Id='
									,@numFieldId
									, ' and CFW' , @numFieldId
									, '.RecId=T1.numOppID')                                                         
				END   
				ELSE IF @vcAssociatedControlType = 'CheckBox' 
				BEGIN
					SET @vcCustomFields =CONCAT( @vcCustomFields
						, ',case when isnull(CFW'
						, @numFieldId
						, '.Fld_Value,0)=0 then 0 when isnull(CFW'
						, @numFieldId
						, '.Fld_Value,0)=1 then 1 end   ['
						,  @vcDbColumnName
						, ']')               
 
					SET @vcCustomWhere = CONCAT(@vcCustomWhere
						, ' left Join CFW_FLD_Values_Opp CFW'
						, @numFieldId
						, ' ON CFW',@numFieldId,'.Fld_Id='
						, @numFieldId
						, ' and CFW'
						, @numFieldId
						, '.RecId=T1.numOppID   ')                                                    
				END                
				ELSE IF @vcAssociatedControlType = 'DateField' 
				BEGIN
					SET @vcCustomFields = CONCAT(@vcCustomFields
						, ',dbo.FormatedDateFromDate(CFW'
						, @numFieldId
						, '.Fld_Value,'
						, @numDomainId
						, ')  [',@vcDbColumnName ,']' )  
					                  
					SET @vcCustomWhere = CONCAT(@vcCustomWhere
						, ' left Join CFW_FLD_Values_Opp CFW'
						, @numFieldId
						, ' on CFW', @numFieldId, '.Fld_Id='
						, @numFieldId
						, ' and CFW'
						, @numFieldId
						, '.RecId=T1.numOppID   ' )                                                        
				END                
				ELSE IF @vcAssociatedControlType = 'SelectBox' 
				BEGIN
					SET @vcCustomFields = CONCAT(@vcCustomFields,',L',@numFieldId,'.vcData',' [',@vcDbColumnName,']')
					                                                    
					SET @vcCustomWhere = CONCAT(@vcCustomWhere
						, ' left Join CFW_FLD_Values_Opp CFW'
						, @numFieldId
						, ' on CFW',@numFieldId ,'.Fld_Id='
						, @numFieldId
						, ' and CFW'
						, @numFieldId
						, '.RecId=T1.numOppID    ')     
					                                                    
					SET @vcCustomWhere = CONCAT(@vcCustomWhere
						, ' left Join ListDetails L'
						, @numFieldId
						, ' on L'
						, @numFieldId
						, '.numListItemID=CFW'
						, @numFieldId
						, '.Fld_Value' )              
				END
				ELSE
				BEGIN
					SET @vcCustomFields = CONCAT(@vcCustomFields,CONCAT(',dbo.GetCustFldValueOpp(',@numFieldId,',T1.numOppID)'),' [',@vcDbColumnName,']')
				END 
			END
			
		END

		SET @j = @j + 1
	END

	CREATE TABLE #TEMPMSRecords
	(
		numOppID NUMERIC(18,0)
		,numOppItemID NUMERIC(18,0)
		,numOppBizDocID NUMERIC(18,0)
	)

	DECLARE @vcSQL NVARCHAR(MAX)
	DECLARE @vcSQLFinal NVARCHAR(MAX)
	
	SET @vcSQL = CONCAT('INSERT INTO #TEMPMSRecords
						(
							numOppID
							,numOppItemID
							,numOppBizDocID
						)
						SELECT
							OpportunityMaster.numOppId
							,',(CASE WHEN @bitGroupByOrder = 1 THEN '0' ELSE 'OpportunityItems.numoppitemtCode' END),'
							,',(CASE 
									WHEN @numViewID = 4 OR @numViewID = 6
									THEN 'OpportunityBizDocs.numOppBizDocsId'  
									ELSE '0' 
								END),'
						FROM
							OpportunityMaster
						INNER JOIN
							DivisionMaster
						ON
							OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
						INNER JOIN
							CompanyInfo
						ON
							DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
						INNER JOIN 
							OpportunityItems
						ON
							OpportunityMaster.numOppId = OpportunityItems.numOppId
						INNER JOIN
							Item
						ON
							OpportunityItems.numItemCode = Item.numItemCode
						LEFT JOIN
							WareHouseItems
						ON
							OpportunityItems.numWarehouseItmsID = WareHouseItems.numWareHouseItemID
						',
						(CASE 
							WHEN ISNULL(@numBatchID,0) > 0 
							THEN 
								' INNER JOIN 
										MassSalesFulfillmentBatchOrders
								ON
									OpportunityMaster.numOppId = MassSalesFulfillmentBatchOrders.numOppID
									AND (OpportunityItems.numOppItemtCode = MassSalesFulfillmentBatchOrders.numOppItemID OR ISNULL(MassSalesFulfillmentBatchOrders.numOppItemID,0) = 0)'
							ELSE 
								'' 
						END)	
						,
						(CASE 
							WHEN @numViewID = 4 
							THEN
								'INNER JOIN 
									OpportunityBizDocs
								ON
									OpportunityMaster.numOppId = OpportunityBizDocs.numOppId
									AND OpportunityBizDocs.numBizDocID=287'
							WHEN @numViewID = 6
							THEN
								CONCAT('INNER JOIN 
											OpportunityBizDocs
										ON
											OpportunityMaster.numOppId = OpportunityBizDocs.numOppId
											AND OpportunityBizDocs.numBizDocID=',(CASE @tintPrintBizDocViewMode WHEN 2 THEN 29397 WHEN 3 THEN 296 WHEN 4 THEN 287 ELSE 55206 END))
							ELSE ''

						END)
						,'
						WHERE
							OpportunityMaster.numDomainId=@numDomainID
							AND OpportunityMaster.tintOppType = 1
							AND OpportunityMaster.tintOppStatus = 1
							AND ISNULL(OpportunityMaster.tintshipped,0) = 0
							AND 1 = (CASE 
										WHEN @numViewID IN (1,2) AND ISNULL(@bitRemoveBO,0) = 1
										THEN (CASE 
												WHEN (Item.charItemType = ''p'' AND ISNULL(OpportunityItems.bitDropShip, 0) = 0 AND ISNULL(Item.bitAsset,0)=0)
												THEN 
													(CASE WHEN dbo.CheckOrderItemInventoryStatus(OpportunityItems.numItemCode,(CASE 
				WHEN @numViewID = 1
				THEN ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyPicked,0)
				WHEN @numViewID = 2 AND @tintPackingViewMode = 2
				THEN (CASE WHEN ISNULL(@tintPackingMode,1) = 2 THEN ISNULL(OpportunityItems.numQtyPicked,0) - ISNULL(OpportunityItems.numQtyShipped,0) ELSE ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyShipped,0) END)
				WHEN @numViewID = 2 AND @tintPackingViewMode = 3
				THEN (CASE WHEN ISNULL(@tintPackingMode,1) = 2 THEN ISNULL(OpportunityItems.numQtyPicked,0) ELSE ISNULL(OpportunityItems.numUnitHour,0) END) - ISNULL((SELECT 
																																											SUM(OpportunityBizDocItems.numUnitHour)
																																										FROM
																																											OpportunityBizDocs
																																										INNER JOIN
																																											OpportunityBizDocItems
																																										ON
																																											OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																																										WHERE
																																											OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
																																											AND OpportunityBizDocs.numBizDocId=@numDefaultSalesShippingDoc
					 																																					AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode),0)
				
				ELSE OpportunityItems.numUnitHour
			END),OpportunityItems.numoppitemtCode,OpportunityItems.numWarehouseItmsID,(CASE WHEN ISNULL(Item.bitKitParent,0) = 1 THEN 1 ELSE 0 END)) = 1 THEN 0 ELSE 1 END)
												ELSE 1
											END) 
										ELSE 1
									END)		
							AND 1 = (CASE WHEN LEN(ISNULL(@vcOrderStauts,'''')) > 0 THEN (CASE WHEN OpportunityMaster.numStatus ',(CASE WHEN ISNULL(@bitIncludeSearch,0) = 0 THEN 'NOT IN' ELSE 'IN' END),' (SELECT Id FROM dbo.SplitIDs(@vcOrderStauts,'','')) THEN 1 ELSE 0 END) ELSE 1 END)
							AND 1 = (CASE 
										WHEN LEN(ISNULL(@vcFilterValue,'''')) > 0
										THEN
											CASE 
												WHEN @tintFilterBy=1 --Item Classification
												THEN (CASE WHEN Item.numItemClassification ',(CASE WHEN ISNULL(@bitIncludeSearch,0) = 0 THEN 'NOT IN' ELSE 'IN' END),' (SELECT Id FROM dbo.SplitIDs(@vcFilterValue,'','')) THEN 1 ELSE 0 END)
												ELSE 1
											END
										ELSE 1
									END)
							AND 1 = (CASE
										WHEN @numViewID = 1 THEN (CASE WHEN ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyPicked,0) > 0 THEN 1 ELSE 0 END)
										WHEN @numViewID = 2 THEN (CASE 
																		WHEN @tintPackingViewMode = 1 
																		THEN (CASE 
																				WHEN ISNULL((SELECT COUNT(*) FROM OpportunityItems OIInner WHERE OIInner.numOppID=OpportunityMaster.numOppId AND OIInner.numItemCode=ISNULL(@numShippingServiceItemID,0)),0) = 0 
																				THEN 1 
																				ELSE 0 
																			END)
																		WHEN @tintPackingViewMode = 2
																		THEN (CASE 
																				WHEN ',(CASE WHEN ISNULL(@tintPackingMode,1) = 2 THEN 'ISNULL(OpportunityItems.numQtyPicked,0)' ELSE 'ISNULL(OpportunityItems.numUnitHour,0)' END),' - ISNULL(OpportunityItems.numQtyShipped,0) > 0
																				THEN 1 
																				ELSE 0 
																			END)
																		WHEN @tintPackingViewMode = 3
																		THEN (CASE 
																				WHEN ',(CASE WHEN ISNULL(@tintPackingMode,1) = 2 THEN 'ISNULL(OpportunityItems.numQtyPicked,0)' ELSE 'ISNULL(OpportunityItems.numUnitHour,0)' END),' - ISNULL((SELECT 
																																																													SUM(OpportunityBizDocItems.numUnitHour)
																																																												FROM
																																																													OpportunityBizDocs
																																																												INNER JOIN
																																																													OpportunityBizDocItems
																																																												ON
																																																													OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																																																												WHERE
																																																													OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
																																																													AND OpportunityBizDocs.numBizDocId=@numDefaultSalesShippingDoc
																																																													AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode),0) > 0
																				THEN 1 
																				ELSE 0 
																			END)','
																		ELSE 0
																	END)
										WHEN @numViewID = 3
										THEN (CASE 
												WHEN (CASE 
														WHEN ISNULL(@tintInvoicingType,0)=1 
														THEN ISNULL(OpportunityItems.numQtyShipped,0) 
														ELSE ISNULL(OpportunityItems.numUnitHour,0) 
													END) - ISNULL((SELECT 
																		SUM(OpportunityBizDocItems.numUnitHour)
																	FROM
																		OpportunityBizDocs
																	INNER JOIN
																		OpportunityBizDocItems
																	ON
																		OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																	WHERE
																		OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
																		AND OpportunityBizDocs.numBizDocId=287
																		AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode),0) > 0 
												THEN 1
												ELSE 0 
											END)
										WHEN @numViewID = 4 THEN 1
										WHEN @numViewID = 5 THEN 1
										WHEN @numViewID = 6 THEN 1
									END)
							AND 1 = (CASE
										WHEN @numViewID = 1 THEN (CASE WHEN ISNULL(WareHouseItems.numWareHouseItemID,0) > 0 AND ISNULL(OpportunityItems.bitDropship,0) = 0 THEN 1 ELSE 0 END)
										WHEN @numViewID = 2 AND @tintPackingViewMode <> 2 THEN (CASE WHEN ISNULL(WareHouseItems.numWareHouseItemID,0) > 0 AND ISNULL(OpportunityItems.bitDropship,0) = 0 THEN 1 ELSE 0 END)
										WHEN @numViewID = 2 AND @tintPackingViewMode = 2 THEN 1
										WHEN @numViewID = 3 THEN 1
										WHEN @numViewID = 4 THEN 1
										WHEN @numViewID = 5 THEN 1
										WHEN @numViewID = 6 THEN 1
										ELSE 0
									END)',
							(CASE 
								WHEN @numViewID = 5 AND ISNULL(@tintPendingCloseFilter,0) = 2
								THEN ' AND (SELECT 
												COUNT(*) 
											FROM 
											(
												SELECT
													OI.numoppitemtCode,
													ISNULL(OI.numUnitHour,0) AS OrderedQty,
													ISNULL(TempFulFilled.FulFilledQty,0) AS FulFilledQty
												FROM
													OpportunityItems OI
												INNER JOIN
													Item I
												ON
													OI.numItemCode = I.numItemCode
												OUTER APPLY
												(
													SELECT
														SUM(OpportunityBizDocItems.numUnitHour) AS FulFilledQty
													FROM
														OpportunityBizDocs
													INNER JOIN
														OpportunityBizDocItems 
													ON
														OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
													WHERE
														OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
														AND ISNULL(OpportunityBizDocs.numBizDocId,0) = 296
														AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
												) AS TempFulFilled
												WHERE
													OI.numOppID = OpportunityMaster.numOppID
													AND ISNULL(OI.numUnitHour,0) > 0
													AND ISNULL(OI.bitDropShip,0) = 0
											) X
											WHERE
												X.OrderedQty <> X.FulFilledQty) = 0
										AND (SELECT 
												COUNT(*) 
											FROM 
											(
												SELECT
													OI.numoppitemtCode,
													ISNULL(OI.numUnitHour,0) AS OrderedQty,
													ISNULL(TempInvoice.InvoicedQty,0) AS InvoicedQty
												FROM
													OpportunityItems OI
												INNER JOIN
													Item I
												ON
													OI.numItemCode = I.numItemCode
												OUTER APPLY
												(
													SELECT
														SUM(OpportunityBizDocItems.numUnitHour) AS InvoicedQty
													FROM
														OpportunityBizDocs
													INNER JOIN
														OpportunityBizDocItems 
													ON
														OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
													WHERE
														OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
														AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
														AND ISNULL(OpportunityBizDocs.bitAuthoritativeBizDocs,0) = 1
												) AS TempInvoice
												WHERE
													OI.numOppID = OpportunityMaster.numOppID
													AND ISNULL(OI.numUnitHour,0) > 0
											) X
											WHERE
												X.OrderedQty > X.InvoicedQty) = 0'
								ELSE '' 
							END)
							,(CASE WHEN @numViewID = 4 THEN 'AND 1 = (CASE WHEN ISNULL(OpportunityBizDocs.monDealAmount,0) - ISNULL(OpportunityBizDocs.monAmountPaid,0) > 0 THEN 1 ELSE 0 END)' ELSE '' END)
							,' AND 1 = (CASE WHEN @numViewID = 2 AND @tintPackingViewMode = 3 THEN (CASE WHEN ISNULL(Item.bitContainer,0)=1 THEN 0 ELSE 1 END) ELSE 1 END)
								AND 1 = (CASE WHEN ISNULL(@numWarehouseID,0) > 0 AND ISNULL(OpportunityItems.numWarehouseItmsID,0) > 0 THEN (CASE WHEN @numWarehouseID=WareHouseItems.numWarehouseID THEN 1 ELSE 0 END) ELSE 1 END)'
								,(CASE WHEN ISNULL(@numShippingZone,0) > 0 THEN ' AND 1 = (CASE 
																				WHEN ISNULL((SELECT TOP 1 numState FROM fn_getOPPAddressDetails(OpportunityMaster.numOppId,OpportunityMaster.numDomainID,2)),0) > 0 
																				THEN
																					CASE 
																						WHEN (SELECT COUNT(*) FROM dbo.State WHERE numDomainID=@numDomainId AND numStateID =  (SELECT TOP 1 numState FROM fn_getOPPAddressDetails(OpportunityMaster.numOppId,OpportunityMaster.numDomainID,2)) AND numShippingZone=@numShippingZone) > 0 
																						THEN 1
																						ELSE 0
																					END
																				ELSE 0 
																			END)' ELSE '' END),
								@vcCustomSearchValue,(CASE WHEN @bitGroupByOrder = 1 THEN CONCAT('GROUP BY OpportunityMaster.numOppId',(CASE WHEN @numViewID = 4 THEN ',OpportunityBizDocs.numOppBizDocsId,OpportunityBizDocs.monAmountPaid' WHEN @numViewID = 6 THEN ',OpportunityBizDocs.numOppBizDocsId'  ELSE '' END)) ELSE '' END));

	EXEC sp_executesql @vcSQL, N'@numDomainID NUMERIC(18,0), @vcOrderStauts VARCHAR(MAX), @vcFilterValue VARCHAR(MAX), @tintFilterBy TINYINT, @numViewID NUMERIC(18,0), @tintPackingViewMode TINYINT, @numShippingServiceItemID NUMERIC(18,0), @numWarehouseID NUMERIC(18,0), @numShippingZone NUMERIC(18,0), @numDefaultSalesShippingDoc NUMERIC(18,0), @tintInvoicingType TINYINT, @tintPackingMode TINYINT, @bitRemoveBO BIT', @numDomainID, @vcOrderStauts, @vcFilterValue, @tintFilterBy, @numViewID, @tintPackingViewMode, @numShippingServiceItemID, @numWarehouseID, @numShippingZone, @numDefaultSalesShippingDoc, @tintInvoicingType, @tintPackingMode, @bitRemoveBO;

	If @numViewID = 2 AND @tintPackingViewMode = 3
	BEGIN
		SET @vcSQL = CONCAT('INSERT INTO #TEMPMSRecords
						(
							numOppID
							,numOppItemID
							,numOppBizDocID
						)
						SELECT
							OpportunityMaster.numOppId
							,',(CASE WHEN @bitGroupByOrder = 1 THEN '0' ELSE 'OpportunityItems.numoppitemtCode' END),'
							,OpportunityBizDocs.numOppBizDocsId
						FROM
							OpportunityMaster
						INNER JOIN
							DivisionMaster
						ON
							OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
						INNER JOIN
							CompanyInfo
						ON
							DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
						INNER JOIN 
							OpportunityBizDocs
						ON
							OpportunityMaster.numOppId = OpportunityBizDocs.numOppId
							AND OpportunityBizDocs.numBizDocID=@numDefaultSalesShippingDoc
						INNER JOIN 
							OpportunityItems
						ON
							OpportunityMaster.numOppId = OpportunityItems.numOppId
						INNER JOIN
							Item
						ON
							OpportunityItems.numItemCode = Item.numItemCode
						LEFT JOIN
							WareHouseItems
						ON
							OpportunityItems.numWarehouseItmsID = WareHouseItems.numWareHouseItemID
						
						',
						(CASE 
							WHEN ISNULL(@numBatchID,0) > 0 
							THEN 
								' INNER JOIN 
										MassSalesFulfillmentBatchOrders
								ON
									OpportunityMaster.numOppId = MassSalesFulfillmentBatchOrders.numOppID
									AND (OpportunityItems.numOppItemtCode = MassSalesFulfillmentBatchOrders.numOppItemID OR ISNULL(MassSalesFulfillmentBatchOrders.numOppItemID,0) = 0)'
							ELSE 
								'' 
						END)	
						,'
						WHERE
							OpportunityMaster.numDomainId=@numDomainID
							AND OpportunityMaster.tintOppType = 1
							AND OpportunityMaster.tintOppStatus = 1
							AND ISNULL(OpportunityMaster.tintshipped,0) = 0		
							AND 1 = (CASE WHEN LEN(ISNULL(@vcOrderStauts,'''')) > 0 THEN (CASE WHEN OpportunityMaster.numStatus ',(CASE WHEN ISNULL(@bitIncludeSearch,0) = 0 THEN 'NOT IN' ELSE 'IN' END),' (SELECT Id FROM dbo.SplitIDs(@vcOrderStauts,'','')) THEN 1 ELSE 0 END) ELSE 1 END)
							AND 1 = (CASE 
										WHEN LEN(ISNULL(@vcFilterValue,'''')) > 0
										THEN
											CASE 
												WHEN @tintFilterBy=1 --Item Classification
												THEN (CASE WHEN Item.numItemClassification ',(CASE WHEN ISNULL(@bitIncludeSearch,0) = 0 THEN 'NOT IN' ELSE 'IN' END),' (SELECT Id FROM dbo.SplitIDs(@vcFilterValue,'','')) THEN 1 ELSE 0 END)
												ELSE 1
											END
										ELSE 1
									END)
							AND 1 = (CASE 
										WHEN ISNULL((SELECT COUNT(*) FROM ShippingBox WHERE ISNULL(vcShippingLabelImage,'''') <> '''' AND ISNULL(vcTrackingNumber,'''') <> '''' AND numShippingReportId IN (SELECT numShippingReportId FROM ShippingReport WHERE ShippingReport.numOppID=OpportunityMaster.numOppId AND ShippingReport.numOppBizDocId=OpportunityBizDocs.numOppBizDocsId)),0) = 0 
										THEN 1 
										ELSE 0 
									END)
										
							AND 1 = (CASE WHEN ISNULL(WareHouseItems.numWareHouseItemID,0) > 0 AND ISNULL(OpportunityItems.bitDropship,0) = 0 THEN 1 ELSE 0 END)',
							' AND 1 = (CASE WHEN @numViewID = 2 AND @tintPackingViewMode = 3 THEN (CASE WHEN ISNULL(Item.bitContainer,0)=1 THEN 0 ELSE 1 END) ELSE 1 END)
								AND 1 = (CASE WHEN ISNULL(@numWarehouseID,0) > 0 AND ISNULL(OpportunityItems.numWarehouseItmsID,0) > 0 THEN (CASE WHEN @numWarehouseID=WareHouseItems.numWarehouseID THEN 1 ELSE 0 END) ELSE 1 END)'
								,(CASE WHEN ISNULL(@numShippingZone,0) > 0 THEN ' AND 1 = (CASE 
																				WHEN ISNULL((SELECT TOP 1 numState FROM fn_getOPPAddressDetails(OpportunityMaster.numOppId,OpportunityMaster.numDomainID,2)),0) > 0 
																				THEN
																					CASE 
																						WHEN (SELECT COUNT(*) FROM dbo.State WHERE numDomainID=@numDomainId AND numStateID =  (SELECT TOP 1 numState FROM fn_getOPPAddressDetails(OpportunityMaster.numOppId,OpportunityMaster.numDomainID,2)) AND numShippingZone=@numShippingZone) > 0 
																						THEN 1
																						ELSE 0
																					END
																				ELSE 0 
																			END)' ELSE '' END),
								@vcCustomSearchValue,(CASE WHEN @bitGroupByOrder = 1 THEN ' GROUP BY OpportunityMaster.numOppId,OpportunityBizDocs.numOppBizDocsId,OpportunityBizDocs.monAmountPaid' ELSE '' END));

		EXEC sp_executesql @vcSQL, N'@numDomainID NUMERIC(18,0), @vcOrderStauts VARCHAR(MAX), @vcFilterValue VARCHAR(MAX), @tintFilterBy TINYINT, @numViewID NUMERIC(18,0), @tintPackingViewMode TINYINT, @numShippingServiceItemID NUMERIC(18,0), @numWarehouseID NUMERIC(18,0), @numShippingZone NUMERIC(18,0), @numDefaultSalesShippingDoc NUMERIC(18,0), @tintInvoicingType TINYINT, @tintPackingMode TINYINT, @bitRemoveBO BIT', @numDomainID, @vcOrderStauts, @vcFilterValue, @tintFilterBy, @numViewID, @tintPackingViewMode, @numShippingServiceItemID, @numWarehouseID, @numShippingZone, @numDefaultSalesShippingDoc, @tintInvoicingType, @tintPackingMode, @bitRemoveBO;
	END

	SET @numTotalRecords = (SELECT COUNT(*) FROM #TEMPMSRecords)

	IF @bitGroupByOrder = 1
	BEGIN
		
		SET @vcSQLFinal = CONCAT('

		INSERT INTO #TEMPResult
		(
			numOppID
			,numDivisionID
			,numTerID
			,numOppItemID
			,numOppBizDocID
			,bintCreatedDate
			,vcCompanyName
			,vcPoppName
			,numAssignedTo
			,numRecOwner
			,vcBizDocID
			,numStatus
			,txtComments
			,monAmountPaid
			,monAmountToPay
			,intUsedShippingCompany
			,intShippingCompany
			,numPreferredShipVia
			,numPreferredShipService
			,bitPaidInFull
			,numAge
			,dtAnticipatedDelivery
			,vcShipStatus
			,dtExpectedDate
			,dtExpectedDateOrig
			,vcPaymentStatus
			,numShipRate
		)
		SELECT
			OpportunityMaster.numOppId
			,OpportunityMaster.numDivisionID
			,ISNULL(DivisionMaster.numTerID,0) numTerID
			,0
			,TEMPOrder.numOppBizDocID
			,(CASE WHEN @numViewID = 6 THEN dbo.FormatedDateFromDate(DATEADD(MINUTE,@ClientTimeZoneOffset * -1,OpportunityBizDocs.dtCreatedDate),@numDomainID) ELSE dbo.FormatedDateFromDate(DATEADD(MINUTE,@ClientTimeZoneOffset * -1,OpportunityMaster.bintCreatedDate),@numDomainID) END) 
			,ISNULL(CompanyInfo.vcCompanyName,'''')
			,ISNULL(OpportunityMaster.vcPoppName,'''')
			,dbo.fn_GetContactName(OpportunityMaster.numAssignedTo)
			,dbo.fn_GetContactName(OpportunityMaster.numRecOwner)
			,(CASE WHEN @numViewID = 4 OR @numViewID = 6 THEN ISNULL(OpportunityBizDocs.vcBizDocID,''-'') ELSE '''' END)
			,dbo.GetListIemName(OpportunityMaster.numStatus)
			,ISNULL(OpportunityMaster.txtComments,'''')
			,ISNULL(TempPaid.monAmountPaid,0)
			,ISNULL(OpportunityBizDocs.monDealAmount,0) - ISNULL(OpportunityBizDocs.monAmountPaid,0)
			,CONCAT(ISNULL(LDOpp.vcData,''-''),'', '',ISNULL(SSOpp.vcShipmentService,''-'')) 
			,CONCAT(ISNULL(LDDiv.vcData,''-''),'', '',ISNULL(SSDiv.vcShipmentService,''-''))
			,ISNULL(DivisionMaster.intShippingCompany,0)
			,ISNULL(DivisionMaster.numDefaultShippingServiceID,0)
			,(CASE WHEN ISNULL(TempPaid.monAmountPaid,0) >= ISNULL(OpportunityMaster.monDealAmount,0) THEN 1 ELSE 0 END)
			,CONCAT(''<span style="color:'',(CASE
						WHEN (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24) = 0 AND (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) < 4  
						THEN ''#00ff00''
						WHEN (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24) = 0 AND ((DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) >= 4 AND (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) < 8)
						THEN ''#00b050''
						WHEN (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24) = 0 AND ((DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) >= 8 AND (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) < 12)
						THEN ''#3399ff''
						WHEN (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24) = 0 AND ((DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) >= 12 AND (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) < 24)
						THEN ''#ff9900''
						WHEN (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24) BETWEEN 1 AND 2  
						THEN ''#9b3596''
						ELSE ''#ff0000''
					END),''">'',CONCAT(DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24,''d '',DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24,''h''),''</span>'')
			,'''' dtAnticipatedDelivery
			,'''' vcShipStatus
			,dbo.FormatedDateFromDate(OpportunityMaster.dtExpectedDate,@numDomainID)
			,OpportunityMaster.dtExpectedDate
			,'''' vcPaymentStatus
			,ISNULL(CAST((SELECT SUM(monTotAmount) FROM OpportunityItems OIInner WHERE OIInner.numOppID=OpportunityMaster.numOppId AND OIInner.numItemCode=@numShippingServiceItemID) AS VARCHAR),'''')
		FROM
			#TEMPMSRecords TEMPOrder
		INNER JOIN
			OpportunityMaster
		ON
			TEMPOrder.numOppID = OpportunityMaster.numOppId
		INNER JOIN
			DivisionMaster
		ON
			OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
		INNER JOIN
			CompanyInfo
		ON
			DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
		LEFT JOIN
			OpportunityBizDocs
		ON
			OpportunityMaster.numOppId = OpportunityBizDocs.numOppId
			AND OpportunityBizDocs.numOppBizDocsId = ISNULL(TEMPOrder.numOppBizDocID,0)
		LEFT JOIN
			ListDetails  LDOpp
		ON
			LDOpp.numListID = 82
			AND (LDOpp.numDomainID = @numDomainID OR LDOpp.constFlag=1)
			AND ISNULL(OpportunityMaster.intUsedShippingCompany,0) = LDOpp.numListItemID
		LEFT JOIN
			ShippingService AS SSOpp
		ON
			(SSOpp.numDomainID=@numDomainID OR ISNULL(SSOpp.numDomainID,0)=0) 
			AND OpportunityMaster.numShippingService = SSOpp.numShippingServiceID
		LEFT JOIN
			ListDetails  LDDiv
		ON
			LDDiv.numListID = 82
			AND (LDDiv.numDomainID = @numDomainID OR LDDiv.constFlag=1)
			AND ISNULL(DivisionMaster.intShippingCompany,0) = LDDiv.numListItemID
		LEFT JOIN
			ShippingService AS SSDiv
		ON
			(SSDiv.numDomainID=@numDomainID OR ISNULL(SSDiv.numDomainID,0)=0) 
			AND ISNULL(DivisionMaster.numDefaultShippingServiceID,0) = SSDiv.numShippingServiceID
		OUTER APPLY
		(
			SELECT 
				SUM(OpportunityBizDocs.monAmountPaid) monAmountPaid
			FROM 
				OpportunityBizDocs 
			WHERE 
				OpportunityBizDocs.numOppId=OpportunityMaster.numOppId 
				AND OpportunityBizDocs.numBizDocId=287
		) TempPaid
		ORDER BY ',
		(CASE @vcSortColumn
			WHEN 'CompanyInfo.vcCompanyName' THEN 'CompanyInfo.vcCompanyName '
			WHEN 'OpportunityMaster.numStatus' THEN 'dbo.GetListIemName(OpportunityMaster.numStatus)'
			WHEN 'OpportunityMaster.vcPoppName' THEN 'OpportunityMaster.vcPoppName'
			WHEN 'OpportunityMaster.numAssignedTo' THEN 'dbo.fn_GetContactName(OpportunityMaster.numAssignedTo)'
			WHEN 'OpportunityMaster.numRecOwner' THEN 'dbo.fn_GetContactName(OpportunityMaster.numRecOwner)'
			WHEN 'OpportunityMaster.numAge' THEN 'OpportunityMaster.bintCreatedDate'
			WHEN 'OpportunityBizDocs.monAmountPaid' THEN 'TempPaid.monAmountPaid'
			WHEN 'OpportunityBizDocs.vcBizDocID' THEN 'OpportunityBizDocs.vcBizDocID'
			WHEN 'OpportunityMaster.bitPaidInFull' THEN '(CASE WHEN ISNULL(TempPaid.monAmountPaid,0) >= ISNULL(OpportunityMaster.monDealAmount,0) THEN 1 ELSE 0 END)'
			ELSE (CASE WHEN @numViewID = 6 THEN 'OpportunityBizDocs.dtCreatedDate' ELSE 'OpportunityMaster.bintCreatedDate' END)
		END),' ',(CASE WHEN ISNULL(@vcSortOrder,'') <> '' THEN @vcSortOrder ELSE 'DESC' END),' OFFSET (@numPageIndex-1) * 200 ROWS FETCH NEXT 200 ROWS ONLY')

		--PRINT CAST(@vcSQLFinal AS NTEXT)

		EXEC sp_executesql @vcSQLFinal, N'@numDomainID NUMERIC(18,0), @vcOrderStauts VARCHAR(MAX), @vcFilterValue VARCHAR(MAX), @tintFilterBy TINYINT, @numViewID NUMERIC(18,0), @tintPackingViewMode TINYINT, @numShippingServiceItemID NUMERIC(18,0), @numWarehouseID NUMERIC(18,0), @numShippingZone NUMERIC(18,0), @numDefaultSalesShippingDoc NUMERIC(18,0), @tintInvoicingType TINYINT, @ClientTimeZoneOffset INT, @tintPackingMode TINYINT, @numPageIndex INT', @numDomainID, @vcOrderStauts, @vcFilterValue, @tintFilterBy, @numViewID, @tintPackingViewMode, @numShippingServiceItemID, @numWarehouseID, @numShippingZone, @numDefaultSalesShippingDoc, @tintInvoicingType, @ClientTimeZoneOffset, @tintPackingMode, @numPageIndex;
	END
	ELSE
	BEGIN
		SET @vcSQLFinal = CONCAT('

		INSERT INTO #TEMPResult
		(
			numOppID
			,numDivisionID
			,numTerID
			,numOppItemID
			,numOppBizDocID
			,bintCreatedDate
			,numItemCode
			,vcCompanyName
			,vcPoppName
			,numAssignedTo
			,numRecOwner
			,vcBizDocID
			,numStatus
			,txtComments
			,numBarCodeId
			,vcLocation
			,vcItemName
			,vcItemDesc
			,numQtyShipped
			,numUnitHour
			,monAmountPaid
			,numItemClassification
			,vcSKU
			,charItemType
			,vcAttributes
			,vcNotes
			,vcItemReleaseDate
			,dtItemReceivedDate
			,intUsedShippingCompany
			,intShippingCompany
			,numPreferredShipVia
			,numPreferredShipService
			,vcInvoiced
			,vcInclusionDetails
			,bitPaidInFull
			,numAge
			,dtAnticipatedDelivery
			,vcShipStatus
			,numRemainingQty
			,dtExpectedDate
			,dtExpectedDateOrig
			,numQtyPicked
			,numQtyPacked
			,vcPaymentStatus
			,numShipRate
		)
		SELECT
			OpportunityMaster.numOppId
			,OpportunityMaster.numDivisionID
			,ISNULL(DivisionMaster.numTerID,0) numTerID
			,OpportunityItems.numoppitemtCode
			,TEMPOrder.numOppBizDocID
			,dbo.FormatedDateFromDate(DATEADD(MINUTE,@ClientTimeZoneOffset * -1,OpportunityMaster.bintCreatedDate),@numDomainID)
			,Item.numItemCode
			,ISNULL(CompanyInfo.vcCompanyName,'''')
			,ISNULL(OpportunityMaster.vcPoppName,'''')
			,dbo.fn_GetContactName(OpportunityMaster.numAssignedTo)
			,dbo.fn_GetContactName(OpportunityMaster.numRecOwner)
			,ISNULL(OpportunityBizDocs.vcBizDocID,'''')
			,dbo.GetListIemName(OpportunityMaster.numStatus)
			,ISNULL(OpportunityMaster.txtComments,'''')
			,ISNULL(Item.numBarCodeId,'''')
			,ISNULL(WarehouseLocation.vcLocation,'''')
			,ISNULL(Item.vcItemName,'''')
			,ISNULL(OpportunityItems.vcItemDesc,'''')
			,ISNULL(OpportunityItems.numQtyShipped,0)
			,ISNULL(OpportunityItems.numUnitHour,0)
			,ISNULL(TempPaid.monAmountPaid,0)
			,dbo.GetListIemName(Item.numItemClassification)
			,ISNULL(Item.vcSKU,'''')
			,(CASE 
				WHEN Item.charItemType=''P''  THEN ''Inventory Item''
				WHEN Item.charItemType=''N'' THEN ''Non Inventory Item'' 
				WHEN Item.charItemType=''S'' THEN ''Service'' 
				WHEN Item.charItemType=''A'' THEN ''Accessory'' 
			END)
			,ISNULL(OpportunityItems.vcAttributes,'''')
			,ISNULL(OpportunityItems.vcNotes,'''')
			,dbo.FormatedDateFromDate(OpportunityItems.ItemReleaseDate,@numDomainID)
			,dbo.FormatedDateFromDate((SELECT TOP 1 OMInner.dtItemReceivedDate FROM OpportunityMaster OMInner INNER JOIN OpportunityItems OIInner ON OMInner.numOppID=OIInner.numOppID WHERE OMInner.tintOppType=2 AND OMInner.tintOppStatus=1 AND OIInner.numItemCode=OpportunityItems.numItemCode ORDER BY OMInner.dtItemReceivedDate DESC),@numDomainID)
			,CONCAT(ISNULL(LDOpp.vcData,''-''),'', '',ISNULL(SSOpp.vcShipmentService,''-'')) 
			,CONCAT(ISNULL(LDDiv.vcData,''-''),'', '',ISNULL(SSDiv.vcShipmentService,''-''))
			,ISNULL(DivisionMaster.intShippingCompany,0)
			,ISNULL(DivisionMaster.numDefaultShippingServiceID,0)
			,ISNULL((SELECT SUM(numUnitHour) FROM OpportunityBizDocs OBD INNER JOIN OpportunityBizDocItems OBDI ON OBD.numOppBizDocsId=OBDI.numOppBizDocID  WHERE OBD.numOppId=OpportunityMaster.numOppId AND OBD.numBizDocId=287 AND OBDI.numOppItemID=OpportunityItems.numoppitemtCode),0)
			,ISNULL(OpportunityItems.vcInclusionDetail,'''')
			,(CASE WHEN ISNULL(TempPaid.monAmountPaid,0) >= ISNULL(OpportunityMaster.monDealAmount,0) THEN 1 ELSE 0 END)
			,CONCAT(''<span style="color:'',(CASE
						WHEN (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24) = 0 AND (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) < 4  
						THEN ''#00ff00''
						WHEN (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24) = 0 AND ((DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) >= 4 AND (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) < 8)
						THEN ''#00b050''
						WHEN (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24) = 0 AND ((DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) >= 8 AND (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) < 12)
						THEN ''#3399ff''
						WHEN (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24) = 0 AND ((DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) >= 12 AND (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) < 24)
						THEN ''#ff9900''
						WHEN (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24) BETWEEN 1 AND 2  
						THEN ''#9b3596''
						ELSE ''#ff0000''
					END),''">'',CONCAT(DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24,''d '',DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24,''h''),''</span>'')
			,'''' dtAnticipatedDelivery
			,'''' vcShipStatus
			,(CASE 
				WHEN @numViewID = 1
				THEN ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyPicked,0)
				WHEN @numViewID = 2 AND @tintPackingViewMode = 2
				THEN (CASE WHEN ISNULL(@tintPackingMode,1) = 2 THEN ISNULL(OpportunityItems.numQtyPicked,0) - ISNULL(OpportunityItems.numQtyShipped,0) ELSE ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyShipped,0) END)
				WHEN @numViewID = 2 AND @tintPackingViewMode = 3 AND ISNULL(TEMPOrder.numOppBizDocID,0) = 0
				THEN (CASE WHEN ISNULL(@tintPackingMode,1) = 2 THEN ISNULL(OpportunityItems.numQtyPicked,0) ELSE ISNULL(OpportunityItems.numUnitHour,0) END) - ISNULL((SELECT 
																																											SUM(OpportunityBizDocItems.numUnitHour)
																																										FROM
																																											OpportunityBizDocs
																																										INNER JOIN
																																											OpportunityBizDocItems
																																										ON
																																											OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																																										WHERE
																																											OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
																																											AND OpportunityBizDocs.numBizDocId=@numDefaultSalesShippingDoc
					 																																					AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode),0)
				WHEN @numViewID = 2 AND @tintPackingViewMode = 3 AND ISNULL(OpportunityBizDocs.numOppBizDocsId,0) > 0
				THEN ISNULL(OpportunityBizDocItems.numUnitHour,0)
				WHEN @numViewID = 3
				THEN (CASE 
						WHEN ISNULL(@tintInvoicingType,0)=1 
						THEN ISNULL(OpportunityItems.numQtyShipped,0) 
						ELSE ISNULL(OpportunityItems.numUnitHour,0) 
					END) - ISNULL((SELECT 
										SUM(OpportunityBizDocItems.numUnitHour)
									FROM
										OpportunityBizDocs
									INNER JOIN
										OpportunityBizDocItems
									ON
										OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
									WHERE
										OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
										AND OpportunityBizDocs.numBizDocId=287
										AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode),0)
				ELSE 0
			END) numRemainingQty
			,dbo.FormatedDateFromDate(OpportunityMaster.dtExpectedDate,@numDomainID)
			,OpportunityMaster.dtExpectedDate
			,ISNULL(OpportunityItems.numQtyPicked,0)
			,ISNULL(TempInvoiced.numInvoicedQty,0)
			,'''' vcPaymentStatus
			,ISNULL(CAST((SELECT SUM(monTotAmount) FROM OpportunityItems OIInner WHERE OIInner.numOppID=OpportunityMaster.numOppId AND OIInner.numItemCode=@numShippingServiceItemID) AS VARCHAR),'''')
		FROM
			#TEMPMSRecords TEMPOrder
		INNER JOIN
			OpportunityMaster
		ON
			TEMPOrder.numOppID = OpportunityMaster.numOppId
		INNER JOIN
			DivisionMaster
		ON
			OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
		INNER JOIN
			CompanyInfo
		ON
			DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
		INNER JOIN 
			OpportunityItems
		ON
			OpportunityMaster.numOppId = OpportunityItems.numOppId
			AND TEMPOrder.numOppItemID = OpportunityItems.numoppitemtCode
		LEFT JOIN
			OpportunityBizDocs 
		ON
			TEMPOrder.numOppId = OpportunityBizDocs.numOppID
			AND TEMPOrder.numOppBizDocID = OpportunityBizDocs.numOppBizDocsId
		LEFT JOIN
			OpportunityBizDocItems
		ON
			OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocId
			AND OpportunityItems.numoppitemtCode = OpportunityBizDocItems.numOppItemID
		INNER JOIN
			Item
		ON
			OpportunityItems.numItemCode = Item.numItemCode
		LEFT JOIN
			ListDetails  LDOpp
		ON
			LDOpp.numListID = 82
			AND (LDOpp.numDomainID = @numDomainID OR LDOpp.constFlag=1)
			AND ISNULL(OpportunityMaster.intUsedShippingCompany,0) = LDOpp.numListItemID
		LEFT JOIN
			ShippingService AS SSOpp
		ON
			(SSOpp.numDomainID=@numDomainID OR ISNULL(SSOpp.numDomainID,0)=0) 
			AND OpportunityMaster.numShippingService = SSOpp.numShippingServiceID
		LEFT JOIN
			ListDetails  LDDiv
		ON
			LDDiv.numListID = 82
			AND (LDDiv.numDomainID = @numDomainID OR LDDiv.constFlag=1)
			AND ISNULL(DivisionMaster.intShippingCompany,0) = LDDiv.numListItemID
		LEFT JOIN
			ShippingService AS SSDiv
		ON
			(SSDiv.numDomainID=@numDomainID OR ISNULL(SSDiv.numDomainID,0)=0) 
			AND ISNULL(DivisionMaster.numDefaultShippingServiceID,0) = SSDiv.numShippingServiceID
		LEFT JOIN
			WareHouseItems
		ON
			OpportunityItems.numWarehouseItmsID = WareHouseItems.numWareHouseItemID
		LEFT JOIN
			WarehouseLocation
		ON
			WareHouseItems.numWLocationID = WarehouseLocation.numWLocationID
		OUTER APPLY
		(
			SELECT 
				SUM(OpportunityBizDocItems.numUnitHour) numInvoicedQty
			FROM 
				OpportunityBizDocs
			INNER JOIN
				OpportunityBizDocItems
			ON
				OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
			WHERE 
				OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
				AND OpportunityBizDocs.numBizDocId=@numDefaultSalesShippingDoc
				AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode
		) TempInvoiced
		OUTER APPLY
		(
			SELECT 
				SUM(OpportunityBizDocs.monAmountPaid) monAmountPaid
			FROM 
				OpportunityBizDocs 
			WHERE 
				OpportunityBizDocs.numOppId=OpportunityMaster.numOppId 
				AND OpportunityBizDocs.numBizDocId=287
		) TempPaid
		WHERE
			1 = (CASE WHEN @numViewID = 2 AND @tintPackingViewMode = 3 THEN (CASE WHEN ISNULL(Item.bitContainer,0)=1 THEN 0 ELSE 1 END) ELSE 1 END)
			AND 1 = (CASE WHEN ISNULL(TEMPOrder.numOppBizDocID,0) > 0 THEN (CASE WHEN OpportunityBizDocItems.numOppItemID > 0 THEN 1 ELSE 0 END) ELSE 1 END)',
			(CASE WHEN @numViewID=2 AND @tintPackingViewMode=3 THEN ' AND 1 = (CASE WHEN ISNULL(OpportunityItems.numWarehouseItmsID,0) > 0 AND ISNULL(OpportunityItems.bitDropShip,0) = 0 THEN 1 ELSE 0 END)' ELSE '' END),'
		ORDER BY ',
		(CASE @vcSortColumn
			WHEN 'CompanyInfo.vcCompanyName' THEN 'CompanyInfo.vcCompanyName' 
			WHEN 'Item.charItemType' THEN '(CASE 
												WHEN Item.charItemType=''P''  THEN ''Inventory Item''
												WHEN Item.charItemType=''N'' THEN ''Non Inventory Item'' 
												WHEN Item.charItemType=''S'' THEN ''Service''
												WHEN Item.charItemType=''A'' THEN ''Accessory''
											END)'
			WHEN 'Item.numBarCodeId' THEN 'Item.numBarCodeId'
			WHEN 'Item.numItemClassification' THEN 'dbo.GetListIemName(Item.numItemClassification)'
			WHEN 'Item.vcSKU' THEN 'Item.vcSKU'
			WHEN 'OpportunityItems.vcItemName' THEN 'Item.vcItemName'
			WHEN 'OpportunityMaster.numStatus' THEN 'dbo.GetListIemName(OpportunityMaster.numStatus)'
			WHEN 'OpportunityMaster.vcPoppName' THEN 'OpportunityMaster.vcPoppName'
			WHEN 'OpportunityMaster.numAssignedTo' THEN 'dbo.fn_GetContactName(OpportunityMaster.numAssignedTo)'
			WHEN 'OpportunityMaster.numRecOwner' THEN 'dbo.fn_GetContactName(OpportunityMaster.numRecOwner)'
			WHEN 'WareHouseItems.vcLocation' THEN 'WarehouseLocation.vcLocation'
			WHEN 'OpportunityBizDocs.monAmountPaid' THEN 'TempPaid.monAmountPaid'
			WHEN 'OpportunityBizDocs.vcBizDocID' THEN 'OpportunityBizDocs.vcBizDocID'
			WHEN 'OpportunityItems.numQtyPacked' THEN 'ISNULL((SELECT SUM(numUnitHour) FROM OpportunityBizDocs OBD INNER JOIN OpportunityBizDocItems OBDI ON OBD.numOppBizDocsId=OBDI.numOppBizDocID  WHERE OBD.numOppId=OpportunityMaster.numOppId AND OBD.numBizDocId=29397 AND OBDI.numOppItemID=OpportunityItems.numoppitemtCode),0)'
			WHEN 'OpportunityItems.numQtyPicked' THEN 'OpportunityItems.numQtyPicked'
			WHEN 'OpportunityItems.numQtyShipped' THEN 'OpportunityItems.numQtyShipped'
			WHEN 'OpportunityItems.numRemainingQty' THEN '(CASE 
																WHEN @numViewID = 1
																THEN ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyPicked,0)
																WHEN @numViewID = 2
																THEN ISNULL(OpportunityItems.numUnitHour,0) - ISNULL((SELECT SUM(numUnitHour) FROM OpportunityBizDocs OBD INNER JOIN OpportunityBizDocItems OBDI ON OBD.numOppBizDocsId=OBDI.numOppBizDocID  WHERE OBD.numOppId=OpportunityMaster.numOppId AND OBD.numBizDocId=29397 AND OBDI.numOppItemID=OpportunityItems.numoppitemtCode),0)
																ELSE 0
															END)'
			WHEN 'OpportunityItems.numUnitHour' THEN 'OpportunityItems.numUnitHour'
			WHEN 'OpportunityItems.vcInvoiced' THEN 'ISNULL((SELECT SUM(numUnitHour) FROM OpportunityBizDocs OBD INNER JOIN OpportunityBizDocItems OBDI ON OBD.numOppBizDocsId=OBDI.numOppBizDocID  WHERE OBD.numOppId=OpportunityMaster.numOppId AND OBD.numBizDocId=287 AND OBDI.numOppItemID=OpportunityItems.numoppitemtCode),0)'
			WHEN 'OpportunityMaster.dtExpectedDate' THEN 'OpportunityMaster.dtExpectedDate'
			WHEN 'OpportunityItems.vcItemReleaseDate' THEN 'OpportunityItems.ItemReleaseDate'
			WHEN 'OpportunityMaster.bintCreatedDate' THEN 'OpportunityMaster.bintCreatedDate'
			WHEN 'OpportunityMaster.numAge' THEN 'OpportunityMaster.bintCreatedDate'
			WHEN 'OpportunityMaster.bitPaidInFull' THEN '(CASE WHEN ISNULL(TempPaid.monAmountPaid,0) >= ISNULL(OpportunityMaster.monDealAmount,0) THEN 1 ELSE 0 END)'
			WHEN 'OpportunityMaster.dtItemReceivedDate' THEN '(SELECT TOP 1 OMInner.dtItemReceivedDate FROM OpportunityMaster OMInner INNER JOIN OpportunityItems OIInner ON OMInner.numOppID=OIInner.numOppID WHERE OMInner.tintOppType=2 AND OMInner.tintOppStatus=1 AND OIInner.numItemCode=OpportunityItems.numItemCode ORDER BY OMInner.dtItemReceivedDate)'
			ELSE 'OpportunityMaster.bintCreatedDate'
		END),' ',(CASE WHEN ISNULL(@vcSortOrder,'') <> '' THEN @vcSortOrder ELSE 'ASC' END),' OFFSET (@numPageIndex-1) * 200 ROWS FETCH NEXT 200 ROWS ONLY')

		--PRINT CAST(@vcSQLFinal AS NTEXT)

		EXEC sp_executesql @vcSQLFinal, N'@numDomainID NUMERIC(18,0), @vcOrderStauts VARCHAR(MAX), @vcFilterValue VARCHAR(MAX), @tintFilterBy TINYINT, @numViewID NUMERIC(18,0), @tintPackingViewMode TINYINT, @numShippingServiceItemID NUMERIC(18,0), @numWarehouseID NUMERIC(18,0), @numShippingZone NUMERIC(18,0), @numDefaultSalesShippingDoc NUMERIC(18,0), @tintInvoicingType TINYINT, @ClientTimeZoneOffset INT, @tintPackingMode TINYINT, @numPageIndex INT', @numDomainID, @vcOrderStauts, @vcFilterValue, @tintFilterBy, @numViewID, @tintPackingViewMode, @numShippingServiceItemID, @numWarehouseID, @numShippingZone, @numDefaultSalesShippingDoc, @tintInvoicingType, @ClientTimeZoneOffset, @tintPackingMode, @numPageIndex;
	END

	SET @vcSQLFinal = CONCAT('SELECT T1.*',@vcCustomFields,' FROM #TEMPResult T1',@vcCustomWhere)

	EXEC sp_executesql @vcSQLFinal

	SELECT * FROM @TempFieldsLeft ORDER BY intRowNum

	IF OBJECT_ID('tempdb..#TEMPMSRecords') IS NOT NULL DROP TABLE #TEMPMSRecords
	IF OBJECT_ID('tempdb..#TEMPResult') IS NOT NULL DROP TABLE #TEMPResult
END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_MassSalesFulfillment_UpdateOrderStauts')
DROP PROCEDURE dbo.USP_MassSalesFulfillment_UpdateOrderStauts
GO

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_MassSalesFulfillment_UpdateOrderStauts]
(
	@numDomainID NUMERIC(18,0)
	,@numUserCntID NUMERIC(18,0)
	,@numOppID NUMERIC(18,0)
	,@tintViewID TINYINT
	,@tintPackingMode TINYINT
)
AS
BEGIN
	DECLARE @numStatus NUMERIC(18,0) = 0
	IF EXISTS (SELECT ID FROM MassSalesFulfillmentConfiguration WHERE numDomainID=@numDomainID AND numUserCntID=@numUserCntID)
	BEGIN
		SELECT 
			@numStatus = (CASE 
								WHEN @tintViewID = 1 THEN ISNULL(numOrderStatusPicked,0) 
								WHEN @tintViewID = 2 AND @tintPackingMode = 1 THEN ISNULL(numOrderStatusPacked1,0)
								WHEN @tintViewID = 2 AND @tintPackingMode = 2 THEN ISNULL(numOrderStatusPacked2,0)
								WHEN @tintViewID = 2 AND @tintPackingMode = 3 THEN ISNULL(numOrderStatusPacked3,0)
								WHEN @tintViewID = 2 AND @tintPackingMode = 4 THEN ISNULL(numOrderStatusPacked4,0)
								WHEN @tintViewID = 3 THEN ISNULL(numOrderStatusInvoiced,0)
								WHEN @tintViewID = 4 THEN ISNULL(numOrderStatusPaid,0)
								WHEN @tintViewID = 5 THEN ISNULL(numOrderStatusClosed,0)
								ELSE 0
							END) 
		FROM 
			MassSalesFulfillmentConfiguration 
		WHERE 
			numDomainID=@numDomainID 
			AND numUserCntID=@numUserCntID
	END

	DECLARE @canUpdateStatus AS BIT = 0

	IF @tintViewID = 1 AND NOT EXISTS (SELECT
											numOppItemtcode
										FROM
											OpportunityItems
										WHERE
											numOppId = @numOppID
											AND ISNULL(numWarehouseItmsID,0) > 0
											AND ISNULL(bitDropShip,0) = 0
											AND ISNULL(numUnitHour,0) <> ISNULL(numQtyPicked,0))
	BEGIN
		SET @canUpdateStatus = 1
	END
	ELSE IF @tintViewID = 2 AND @tintPackingMode = 1
	BEGIN
		SET @canUpdateStatus = 1
	END
	ELSE IF @tintViewID = 2 AND @tintPackingMode = 2 AND NOT EXISTS (SELECT
																		numOppItemtcode
																	FROM
																		OpportunityItems
																	WHERE
																		numOppId = @numOppID
																		AND ISNULL(numWarehouseItmsID,0) > 0
																		AND ISNULL(bitDropShip,0) = 0
																		AND ISNULL(numUnitHour,0) <> ISNULL((SELECT 
																												SUM(numUnitHour) 
																											FROM 
																												OpportunityBizDocs OBD 
																											INNER JOIN 
																												OpportunityBizDocItems OBDI 
																											ON
																												OBD.numOppBizDocsId = OBDI.numOppBizDocID  
																											WHERE 
																												OBD.numOppId = OpportunityItems.numOppId 
																						AND OBD.numBizDocId=296
																						AND OBDI.numOppItemID=OpportunityItems.numoppitemtCode),0))
	BEGIN
		SET @canUpdateStatus = 1
	END
	ELSE IF @tintViewID = 2 AND @tintPackingMode = 4 AND NOT EXISTS (SELECT
																		numOppItemtcode
																	FROM
																		OpportunityItems
																	WHERE
																		numOppId = @numOppID
																		AND ISNULL(numWarehouseItmsID,0) > 0
																		AND ISNULL(bitDropShip,0) = 0
																		AND ISNULL(numUnitHour,0) <> ISNULL((SELECT 
																												SUM(SRI.intBoxQty) 
																											FROM 
																												ShippingReport SR
																											INNER JOIN
																												ShippingBox SB
																											ON
																												SR.numShippingReportId = SB.numShippingReportId
																											INNER JOIN
																												ShippingReportItems SRI
																											ON
																												SB.numBoxID = SRI.numBoxID 
																											INNER JOIN 
																												OpportunityBizDocItems OBDI 
																											ON
																												SRI.numOppBizDocItemID = OBDI.numOppBizDocItemID  
																											WHERE 
																												SR.numOppId = OpportunityItems.numOppId 
																												AND OBDI.numOppItemID=OpportunityItems.numoppitemtCode),0))
	BEGIN
		SET @canUpdateStatus = 1
	END
	ELSE IF @tintViewID = 3 AND NOT EXISTS (SELECT
												numOppItemtcode
											FROM
												OpportunityItems
											WHERE
												numOppId = @numOppID
												AND ISNULL(numUnitHour,0) <> ISNULL((SELECT 
																						SUM(numUnitHour) 
																					FROM 
																						OpportunityBizDocs OBD 
																					INNER JOIN 
																						OpportunityBizDocItems OBDI 
																					ON
																						OBD.numOppBizDocsId = OBDI.numOppBizDocID  
																					WHERE 
																						OBD.numOppId = OpportunityItems.numOppId 
																						AND OBD.numBizDocId=287
																						AND OBDI.numOppItemID=OpportunityItems.numoppitemtCode),0))
	BEGIN
		SET @canUpdateStatus = 1
	END
	ELSE IF @tintViewID = 4 AND EXISTS (SELECT 
											numOppId 
										FROM 
											OpportunityMaster 
										WHERE 
											numOppId=@numOppID 
											AND monDealAmount = ISNULL((SELECT SUM(ISNULL(monAmountPaid,0)) FROM OpportunityBizDocs BD WHERE  BD.numOppId = OpportunityMaster.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)) 
	BEGIN
		SET @canUpdateStatus = 1
	END
	ELSE IF @tintViewID = 5
	BEGIN
		SET @canUpdateStatus = 1
	END
	

	IF ISNULL(@numStatus,0) > 0 AND @canUpdateStatus = 1
	BEGIN
		UPDATE
			OpportunityMaster
		SET
			numStatus=@numStatus
		WHERE
			numDomainId=@numDomainID
			AND numOppId = @numOppID
	END
END

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_oppdetailsdtlpl')
DROP PROCEDURE usp_oppdetailsdtlpl
GO
CREATE PROCEDURE [dbo].[USP_OPPDetailsDTLPL]
(
               @OpportunityId        AS NUMERIC(9)  = NULL,
               @numDomainID          AS NUMERIC(9),
               @ClientTimeZoneOffset AS INT)
AS
	DECLARE @tintDecimalPoints TINYINT

	SELECT
		@tintDecimalPoints=ISNULL(tintDecimalPoints,0)
	FROM
		Domain 
	WHERE 
		numDomainId=@numDomainID

  SELECT Opp.numoppid,numCampainID,
		 CAMP.vcCampaignName AS [numCampainIDName],
         dbo.Fn_getcontactname(Opp.numContactId) numContactIdName,
         isnull(ADC.vcEmail,'') AS vcEmail,
         isnull(ADC.numPhone,'') AS Phone,
         isnull(ADC.numPhoneExtension,'') AS PhoneExtension,
		 Opp.numContactId,
         Opp.txtComments,
         Opp.numDivisionID,
         tintOppType,
         Dateadd(MINUTE,-@ClientTimeZoneOffset,bintAccountClosingDate) AS bintAccountClosingDate,
         dbo.fn_GetOpportunitySourceValue(ISNULL(Opp.tintSource,0),ISNULL(Opp.tintSourceType,0),Opp.numDomainID) AS tintSourceName,
          tintSource,
         Opp.VcPoppName,
         intpEstimatedCloseDate,
         [dbo].[getdealamount](@OpportunityId,Getutcdate(),0) AS CalAmount,
         --monPAmount,
         CONVERT(DECIMAL(10,2),Isnull(monPAmount,0)) AS monPAmount,
		lngPConclAnalysis,
         (SELECT vcdata
          FROM   listdetails
          WHERE  numlistitemid = lngPConclAnalysis) AS lngPConclAnalysisName,
         (SELECT vcdata
          FROM   listdetails
          WHERE  numlistitemid = numSalesOrPurType) AS numSalesOrPurTypeName,
		  numSalesOrPurType,
		 Opp.vcOppRefOrderNo,
		 Opp.vcMarketplaceOrderID,
         C2.vcCompanyName,
         D2.tintCRMType,
         Opp.tintActive,
         dbo.Fn_getcontactname(Opp.numCreatedby)
           + ' '
           + CONVERT(VARCHAR(20),Dateadd(MINUTE,-@ClientTimeZoneOffset,Opp.bintCreatedDate)) AS CreatedBy,
         dbo.Fn_getcontactname(Opp.numModifiedBy)
           + ' '
           + CONVERT(VARCHAR(20),Dateadd(MINUTE,-@ClientTimeZoneOffset,Opp.bintModifiedDate)) AS ModifiedBy,
         dbo.Fn_getcontactname(Opp.numRecOwner) AS RecordOwner,
         Opp.numRecOwner,
         Isnull(tintshipped,0) AS tintshipped,
         Isnull(tintOppStatus,0) AS tintOppStatus,
         [dbo].[GetOppStatus](Opp.numOppId) AS vcDealStatus,
         dbo.Opportunitylinkeditems(@OpportunityId) +
         (SELECT COUNT(*) FROM  dbo.Communication Comm JOIN Correspondence Corr ON Comm.numCommId=Corr.numCommID
			WHERE Comm.numDomainId=Opp.numDomainId AND Corr.numDomainID=Comm.numDomainID AND Corr.tintCorrType IN (1,2,3,4) AND Corr.numOpenRecordID=Opp.numOppId) AS NoOfProjects,
         (SELECT A.vcFirstName
                   + ' '
                   + A.vcLastName
          FROM   AdditionalContactsInformation A
          WHERE  A.numcontactid = opp.numAssignedTo) AS numAssignedToName,
		  opp.numAssignedTo,
         Opp.numAssignedBy,
         (SELECT COUNT(* )
          FROM   GenericDocuments
          WHERE  numRecID = @OpportunityId
                 AND vcDocumentSection = 'O') AS DocumentCount,
         CASE 
           WHEN tintOppStatus = 1 THEN 100
           ELSE isnull((SELECT SUM(tintPercentage)
                 FROM   OpportunityStageDetails
                 WHERE  numOppId = @OpportunityId
                        AND bitStageCompleted = 1),0)
         END AS TProgress,
         (SELECT TOP 1 Isnull(varRecurringTemplateName,'')
          FROM   RecurringTemplate
          WHERE  numRecurringId = OPR.numRecurringId) AS numRecurringIdName,
		 OPR.numRecurringId,
         Link.numOppID AS numParentOppID,
         Link.vcPoppName AS ParentOppName,
         Link.vcSource AS Source,
         ISNULL(C.varCurrSymbol,'') varCurrSymbol,
          ISNULL(Opp.fltExchangeRate,0) AS [fltExchangeRate],
           ISNULL(C.chrCurrency,'') AS [chrCurrency],
         ISNULL(C.numCurrencyID,0) AS [numCurrencyID],
         (SELECT COUNT(*) FROM [OpportunityLinking] OL WHERE OL.[numParentOppID] = Opp.numOppId) AS ChildCount,
         (SELECT COUNT(*) FROM [RecurringTransactionReport] RTR WHERE RTR.[numRecTranSeedId] = Opp.numOppId)  AS RecurringChildCount,
         (SELECT COUNT(*) FROM [ShippingReport] WHERE numOppBizDocId IN (SELECT numOppBizDocsId FROM   OpportunityBizDocs BD WHERE  BD.numOppId = @OpportunityId))AS ShippingReportCount,
         ISNULL(Opp.numStatus ,0) numStatus,
         (SELECT vcdata
          FROM   listdetails
          WHERE  numlistitemid = Opp.numStatus) AS numStatusName,
          (SELECT SUBSTRING(
	(SELECT ','+  A.vcFirstName+' ' +A.vcLastName + CASE WHEN ISNULL(SR.numContactType,0)>0 THEN '(' + dbo.fn_GetListItemName(SR.numContactType) + ')' ELSE '' END
	FROM ShareRecord SR JOIN UserMaster UM ON UM.numUserDetailId=SR.numAssignedTo 
							 JOIN AdditionalContactsInformation A ON UM.numUserDetailId=A.numContactID 
		 WHERE SR.numDomainID=Opp.numDomainID AND SR.numModuleID=3 AND SR.numRecordID=Opp.numOppId
				AND UM.numDomainID=Opp.numDomainID and UM.numDomainID=A.numDomainID FOR XML PATH('')),2,200000)) AS numShareWith,
		ISNULL((SELECT SUM(ISNULL(monDealAmount,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = @OpportunityId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0) AS monDealAmount,
		ISNULL((SELECT SUM(ISNULL(monAmountPaid,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = @OpportunityId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0) AS monAmountPaid,
		ISNULL(Opp.tintSourceType,0) AS tintSourceType,
		ISNULL(Opp.tintTaxOperator,0) AS tintTaxOperator,  
             isnull(Opp.intBillingDays,0) AS numBillingDays,isnull(Opp.bitInterestType,0) AS bitInterestType,
             isnull(opp.fltInterest,0) AS fltInterest,  isnull(Opp.fltDiscount,0) AS fltDiscount,
             isnull(Opp.bitDiscountType,0) AS bitDiscountType,ISNULL(Opp.bitBillingTerms,0) AS bitBillingTerms,
             ISNULL(OPP.numDiscountAcntType,0) AS numDiscountAcntType,
              dbo.fn_getOPPAddress(Opp.numOppId,@numDomainID,1) AS BillingAddress,
            dbo.fn_getOPPAddress(Opp.numOppId,@numDomainID,2) AS ShippingAddress,

            -- ISNULL((SELECT SUBSTRING((SELECT '$^$' + ISNULL( (SELECT vcShipFieldValue FROM dbo.ShippingFieldValues WHERE numDomainID=Opp.numDomainID AND intShipFieldID IN ( SELECT intShipFieldID FROM dbo.ShippingFields WHERE numListItemID=OBD.numShipVia  AND vcFieldName='Tracking URL')),'') +'#^#'+ RTRIM(LTRIM(vcTrackingNo) )  FROM dbo.OpportunityBizDocs OBD WHERE OBD.numOppId=Opp.numOppID FOR XML PATH('')),4,200000)),'') AS  vcTrackingNo,
			 (SELECT  
				SUBSTRING(
							(SELECT '$^$' + 
							(SELECT vcShipFieldValue FROM dbo.ShippingFieldValues 
								WHERE numDomainID=Opp.numDomainID AND intShipFieldID IN ( SELECT intShipFieldID FROM dbo.ShippingFields WHERE numListItemID=OBD.numShipVia  AND vcFieldName='Tracking URL')
							) +'#^#'+ RTRIM(LTRIM(vcTrackingNo))						
							FROM dbo.OpportunityBizDocs OBD WHERE OBD.numOppId= Opp.numOppID and ISNULL(OBD.numShipVia,0) <> 0
				FOR XML PATH('')),4,200000)
						
			 ) AS  vcTrackingNo,
			 STUFF((SELECT 
						CONCAT('<br/>',OpportunityBizDocs.vcBizDocID,': ',vcTrackingDetail)
					FROM 
						ShippingReport 
					INNER JOIN 
						OpportunityBizDocs 
					ON 
						ShippingReport.numOppBizDocId=OpportunityBizDocs.numOppBizDocsId 
					WHERE 
						ShippingReport.numOppId=Opp.numOppID
						AND ISNULL(ShippingReport.vcTrackingDetail,'') <> ''
					FOR XML PATH(''), TYPE).value('(./text())[1]','varchar(max)'), 1, 5, '') AS vcTrackingDetail,
            ISNULL(numPercentageComplete,0) numPercentageComplete,
            dbo.GetListIemName(ISNULL(numPercentageComplete,0)) AS PercentageCompleteName,
            ISNULL(numBusinessProcessID,0) AS [numBusinessProcessID],
            CASE WHEN opp.tintOppType=1 THEN dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID) ELSE '' END AS vcInventoryStatus
            ,ISNULL(bitUseShippersAccountNo,0) [bitUseShippersAccountNo], ISNULL(D2.vcShippersAccountNo,'') [vcShippersAccountNo] ,
			(SELECT SUM((ISNULL(monTotAmtBefDiscount, ISNULL(monTotAmount,0))- ISNULL(monTotAmount,0))) FROM OpportunityBizDocItems WHERE numOppBizDocID IN (SELECT numOppBizDocsId FROM OpportunityBizDocs WHERE numOppId = @OpportunityId AND bitAuthoritativeBizDocs = 1)) AS numTotalDiscount,
			ISNULL(Opp.vcRecurrenceType,'') AS vcRecurrenceType,
			ISNULL(Opp.bitRecurred,0) AS bitRecurred,
			(
				CASE 
				WHEN opp.tintOppType=1 AND opp.tintOppStatus = 1 
				THEN  
					dbo.GetOrderShipReceiveStatus(opp.numOppId,1,opp.tintShipped,@tintDecimalPoints)
				ELSE 
						'' 
				END
			) AS vcOrderedShipped,
			(
				CASE 
				WHEN opp.tintOppType = 2 AND opp.tintOppStatus = 1 
				THEN  
					dbo.GetOrderShipReceiveStatus(opp.numOppId,2,opp.tintShipped,@tintDecimalPoints)
				ELSE 
						'' 
				END
			) AS vcOrderedReceived,
			ISNULL(C2.numCompanyType,0) AS numCompanyType,
		ISNULL(C2.vcProfile,0) AS vcProfile,
		ISNULL(Opp.numAccountClass,0) AS numAccountClass,D3.vcPartnerCode+'-'+C3.vcCompanyName as numPartner,
		ISNULL(Opp.numPartner,0) AS numPartnerId,
		ISNULL(Opp.bitIsInitalSalesOrder,0) AS bitIsInitalSalesOrder,
		ISNULL(Opp.dtReleaseDate,'') AS dtReleaseDate,
		ISNULL(Opp.numPartenerContact,0) AS numPartenerContactId,
		A.vcGivenName AS numPartenerContact,
		Opp.numReleaseStatus,
		(CASE Opp.numReleaseStatus WHEN 1 THEN 'Open' WHEN 2 THEN 'Purchased' ELSE '' END) numReleaseStatusName,
		ISNULL(Opp.intUsedShippingCompany,0) intUsedShippingCompany,
		CASE WHEN ISNULL(Opp.intUsedShippingCompany,0) = 0 THEN '' ELSE ISNULL((SELECT vcData FROM ListDetails WHERE numListID = 82 AND numListItemID=ISNULL(Opp.intUsedShippingCompany,0)),'') END AS vcShippingCompany,
		ISNULL(Opp.numShipmentMethod,0) numShipmentMethod
		,CASE WHEN ISNULL(Opp.numShipmentMethod,0) = 0 THEN '' ELSE ISNULL((SELECT vcData FROM ListDetails WHERE numListID = 338 AND numListItemID=ISNULL(Opp.numShipmentMethod,0)),'') END AS vcShipmentMethod
		,ISNULL(Opp.vcCustomerPO#,'') vcCustomerPO#,
		CONCAT(CASE tintEDIStatus 
			--WHEN 1 THEN '850 Received'
			WHEN 2 THEN '850 Acknowledged'
			WHEN 3 THEN '940 Sent'
			WHEN 4 THEN '940 Acknowledged'
			WHEN 5 THEN '856 Received'
			WHEN 6 THEN '856 Acknowledged'
			WHEN 7 THEN '856 Sent'
			WHEN 8 THEN 'Send 940 Failed'
			WHEN 9 THEN 'Send 856 & 810 Failed'
			WHEN 11 THEN '850 Partially Created'
			WHEN 12 THEN '850 SO Created'
			ELSE '' 
		END,'  ','<a onclick="return Open3PLEDIHistory(',Opp.numOppID,')"><img src="../images/GLReport.png" border="0"></a>') tintEDIStatusName,
		ISNULL(Opp.numShippingService,0) numShippingService,
		ISNULL((SELECT vcShipmentService FROM ShippingService WHERE (numDomainID=@numDomainID OR ISNULL(numDomainID,0)=0) AND numShippingServiceID = Opp.numShippingService),'') AS vcShippingService,
		CASE 
			WHEN vcSignatureType = 0 THEN 'Service Default'
			WHEN vcSignatureType = 1 THEN 'Adult Signature Required'
			WHEN vcSignatureType = 2 THEN 'Direct Signature'
			WHEN vcSignatureType = 3 THEN 'InDirect Signature'
			WHEN vcSignatureType = 4 THEN 'No Signature Required'
			ELSE ''
		END AS vcSignatureType
		,dbo.fn_getOPPState(Opp.numOppId,Opp.numDomainID,2) vcShipState
		,Opp.dtExpectedDate
  FROM   OpportunityMaster Opp
         JOIN divisionMaster D2 ON Opp.numDivisionID = D2.numDivisionID
         JOIN CompanyInfo C2 ON C2.numCompanyID = D2.numCompanyID
		 LEFT JOIN DivisionMasterShippingConfiguration ON D2.numDivisionID=DivisionMasterShippingConfiguration.numDivisionID
		 LEFT JOIN divisionMaster D3 ON Opp.numPartner = D3.numDivisionID
		 LEFT JOIN AdditionalContactsInformation A ON Opp.numPartenerContact = A.numContactId
		 LEFT JOIN AdditionalContactsInformation ADC ON Opp.numContactId = ADC.numContactId
         LEFT JOIN CompanyInfo C3 ON C3.numCompanyID = D3.numCompanyID
         LEFT JOIN (SELECT TOP 1 vcPoppName,
                                 numOppID,
                                 numChildOppID,
                                 vcSource,numParentOppID,OpportunityMaster.numDivisionID AS numParentDivisionID 
                    FROM   OpportunityLinking
                          left JOIN OpportunityMaster
                             ON numOppID = numParentOppID
                    WHERE  numChildOppID = @OpportunityId) Link
           ON Link.numChildOppID = Opp.numOppID
           LEFT OUTER JOIN [Currency] C
					ON C.numCurrencyID = Opp.numCurrencyID
			LEFT OUTER JOIN [OpportunityRecurring] OPR
					ON OPR.numOppId = Opp.numOppId
					 LEFT JOIN AddressDetails AD --Billing add
				ON AD.numDomainID=D2.numDomainID AND AD.numRecordID=D2.numDivisionID AND AD.tintAddressOf = 2 AND AD.tintAddressType = 1 AND AD.bitIsPrimary=1
			 LEFT JOIN AddressDetails AD2--Shipping address
				ON AD2.numDomainID=D2.numDomainID AND AD2.numRecordID=D2.numDivisionID AND AD2.tintAddressOf = 2 AND AD2.tintAddressType = 2 AND AD2.bitIsPrimary=1	
		LEFT JOIN dbo.CampaignMaster CAMP ON CAMP.numCampaignID = Opp.numCampainID 
  WHERE  Opp.numOppId = @OpportunityId
         AND Opp.numDomainID = @numDomainID
/****** Object:  StoredProcedure [dbo].[USP_OPPItemDetails]    Script Date: 09/01/2009 18:59:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- exec USP_OPPItemDetails @numItemCode=31455,@OpportunityId=12387,@numDomainId=110
--created by anooop jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityMaster_GetItems')
DROP PROCEDURE USP_OpportunityMaster_GetItems
GO
CREATE PROCEDURE [dbo].[USP_OpportunityMaster_GetItems]
(
    @numUserCntID NUMERIC(9) = 0,
    @OpportunityId NUMERIC(9) = 0,
    @numDomainId NUMERIC(9),
    @ClientTimeZoneOffset  INT
)
AS 
BEGIN
    IF @OpportunityId >0
    BEGIN
        DECLARE @DivisionID AS NUMERIC(9)
        DECLARE @TaxPercentage AS FLOAT
        DECLARE @tintOppType AS TINYINT
        DECLARE @fld_Name AS VARCHAR(500)
        DECLARE @fld_ID AS NUMERIC(9)
        DECLARE @numBillCountry AS NUMERIC(9)
        DECLARE @numBillState AS NUMERIC(9)
    
        SELECT  
			@DivisionID = numDivisionID
			,@tintOppType = tintOpptype
        FROM 
			OpportunityMaster
        WHERE 
			numOppId = @OpportunityId
               

		CREATE TABLE #tempForm 
		(
			ID INT IDENTITY(1,1)
			,tintOrder TINYINT
			,vcDbColumnName NVARCHAR(50)
			,vcOrigDbColumnName NVARCHAR(50)
			,vcFieldName NVARCHAR(50)
			,vcAssociatedControlType NVARCHAR(50)
			,vcListItemType VARCHAR(3)
			,numListID NUMERIC(9)
			,vcLookBackTableName VARCHAR(50)
			,bitCustomField BIT
			,numFieldId NUMERIC
			,bitAllowSorting BIT
			,bitAllowEdit BIT
			,bitIsRequired BIT
			,bitIsEmail BIT
			,bitIsAlphaNumeric BIT
			,bitIsNumeric BIT
			,bitIsLengthValidation BIT
			,intMaxLength INT
			,intMinLength INT
			,bitFieldMessage BIT
			,vcFieldMessage VARCHAR(500)
			,ListRelID NUMERIC(9)
			,intColumnWidth INT
			,intVisible INT
			,vcFieldDataType CHAR(1)
			,numUserCntID NUMERIC(18,0)
		)

		
		IF (
				SELECT 
					COUNT(*)
				FROM 
					View_DynamicColumns DC 
				WHERE 
					DC.numFormId=(CASE WHEN @tintOppType = 2 THEN 129 ELSE 26 END) and DC.numDomainID=@numDomainID AND numUserCntID = @numUserCntID AND
					ISNULL(DC.bitSettingField,0)=1 AND ISNULL(DC.bitDeleted,0)=0 AND ISNULL(DC.bitCustom,0)=0) > 0
			OR
			(
				SELECT 
					COUNT(*)
				FROM 
					View_DynamicCustomColumns
				WHERE 
					numFormId=(CASE WHEN @tintOppType = 2 THEN 129 ELSE 26 END) AND numUserCntID=@numUserCntID AND numDomainID=@numDomainID AND tintPageType=1 AND numRelCntType=0 AND ISNULL(bitCustom,0)=1
			) > 0
		BEGIN
			INSERT INTO 
				#tempForm
			SELECT 
				ISNULL(DC.tintRow,0) + 1 as tintOrder
				,DDF.vcDbColumnName
				,DDF.vcOrigDbColumnName
				,ISNULL(DDF.vcCultureFieldName,DDF.vcFieldName)
				,DDF.vcAssociatedControlType
				,DDF.vcListItemType
				,DDF.numListID,DC.vcLookBackTableName                                                 
				,DDF.bitCustom,DC.numFieldId
				,DDF.bitAllowSorting
				,DDF.bitInlineEdit
				,DDF.bitIsRequired
				,DDF.bitIsEmail
				,DDF.bitIsAlphaNumeric
				,DDF.bitIsNumeric
				,DDF.bitIsLengthValidation
				,DDF.intMaxLength
				,DDF.intMinLength
				,DDF.bitFieldMessage
				,DDF.vcFieldMessage vcFieldMessage
				,DDF.ListRelID
				,DC.intColumnWidth
				,CASE WHEN DC.numFieldID IS NULL THEN 0 ELSE 1 END
				,DDF.vcFieldDataType
				,DC.numUserCntID
			FROM  
				View_DynamicDefaultColumns DDF 
			LEFT JOIN 
				View_DynamicColumns DC 
			ON 
				DC.numFormId=(CASE WHEN @tintOppType = 2 THEN 129 ELSE 26 END) 
				AND DDF.numFieldId=DC.numFieldId 
				AND DC.numUserCntID=@numUserCntID 
				AND DC.numDomainID=@numDomainID 
				AND numRelCntType=0 
				AND tintPageType=1 
			WHERE 
				DDF.numFormId=(CASE WHEN @tintOppType = 2 THEN 129 ELSE 26 END) 
				AND DDF.numDomainID=@numDomainID 
				AND ISNULL(DDF.bitSettingField,0)=1 
				AND ISNULL(DDF.bitDeleted,0)=0 
				AND ISNULL(DDF.bitCustom,0)=0
			UNION
			SELECT 
				tintRow + 1 AS tintOrder
				,vcDbColumnName
				,vcFieldName
				,vcFieldName
				,vcAssociatedControlType
				,'' as vcListItemType
				,numListID
				,''                                                 
				,bitCustom
				,numFieldId
				,bitAllowSorting
				,0 as bitAllowEdit
				,bitIsRequired
				,bitIsEmail
				,bitIsAlphaNumeric
				,bitIsNumeric
				,bitIsLengthValidation
				,intMaxLength
				,intMinLength
				,bitFieldMessage
				,vcFieldMessage
				,ListRelID
				,intColumnWidth
				,1
				,''
				,0
			FROM 
				View_DynamicCustomColumns
			WHERE 
				numFormId=(CASE WHEN @tintOppType = 2 THEN 129 ELSE 26 END) 
				AND numUserCntID=@numUserCntID 
				AND numDomainID=@numDomainID 
				AND tintPageType=1 
				AND numRelCntType=0
				AND ISNULL(bitCustom,0)=1
		END
		ELSE
		BEGIN
			--Check if Master Form Configuration is created by administrator if NoColumns=0
			DECLARE @IsMasterConfAvailable BIT = 0
			DECLARE @numUserGroup NUMERIC(18,0)

			SELECT @numUserGroup = numGroupID FROM UserMaster WHERE numUserDetailId = @numUserCntID 

			IF (SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = @numDomainID AND numFormID = (CASE WHEN @tintOppType = 2 THEN 129 ELSE 26 END) AND bitGridConfiguration = 1 AND numGroupID = @numUserGroup) > 0
			BEGIN
				SET @IsMasterConfAvailable = 1
			END

			--If MasterConfiguration is available then load it otherwise load default columns
			IF @IsMasterConfAvailable = 1
			BEGIN
				INSERT INTO DycFormConfigurationDetails 
				(
					numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth
				)
				SELECT 
					(CASE WHEN @tintOppType = 2 THEN 129 ELSE 26 END),numFieldId,intColumnNum,intRowNum,@numDomainID,@numUserCntID,0,1,0,intColumnWidth
				FROM 
					View_DynamicColumnsMasterConfig
				WHERE
					View_DynamicColumnsMasterConfig.numFormId=(CASE WHEN @tintOppType = 2 THEN 129 ELSE 26 END) AND 
					View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
					View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
					View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
					ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
					ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
				UNION
				SELECT 
					(CASE WHEN @tintOppType = 2 THEN 129 ELSE 26 END),numFieldId,intColumnNum,intRowNum,@numDomainID,@numUserCntID,0,1,1,intColumnWidth
				FROM 
					View_DynamicCustomColumnsMasterConfig
				WHERE 
					View_DynamicCustomColumnsMasterConfig.numFormId=(CASE WHEN @tintOppType = 2 THEN 129 ELSE 26 END) AND 
					View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
					View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
					View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
					ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1


				INSERT INTO #tempForm
				SELECT 
					(intRowNum + 1) as tintOrder, vcDbColumnName, vcOrigDbColumnName,ISNULL(vcCultureFieldName,vcFieldName),                  
					vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName,bitCustom,numFieldId,bitAllowSorting,bitInlineEdit,
					bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength,
					bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth,1,vcFieldDataType,@numUserCntID
				FROM 
					View_DynamicColumnsMasterConfig
				WHERE
					View_DynamicColumnsMasterConfig.numFormId=(CASE WHEN @tintOppType = 2 THEN 129 ELSE 26 END) AND 
					View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
					View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
					View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
					ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
					ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
				UNION
				SELECT 
					tintRow+1 as tintOrder,vcDbColumnName,vcFieldName,vcFieldName,                  
					 vcAssociatedControlType,'' as vcListItemType,numListID,''                                                 
					,bitCustom,numFieldId,bitAllowSorting,0 as bitAllowEdit,
					bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
					intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,'',0
				FROM 
					View_DynamicCustomColumnsMasterConfig
				WHERE 
					View_DynamicCustomColumnsMasterConfig.numFormId=(CASE WHEN @tintOppType = 2 THEN 129 ELSE 26 END) AND 
					View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
					View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
					View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
					ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1
				ORDER BY 
					tintOrder ASC  
			END
		END

		
		DECLARE @strSQL VARCHAR(MAX) = ''

		DECLARE @avgCost INT
		DECLARE @tintCommitAllocation TINYINT
		SELECT @avgCost=ISNULL(numCost,0),@tintCommitAllocation=ISNULL(tintCommitAllocation,1) FROM Domain WHERE numDomainId=@numDomainID

		--------------Make Dynamic Query--------------
		SET @strSQL = @strSQL + 'SELECT 
									Opp.numoppitemtCode
									,Opp.vcNotes
									,WItems.numBackOrder
									,CAST(ISNULL(Opp.numCost,0) AS DECIMAL(20,5)) AS numCost
									,ISNULL(Opp.bitItemPriceApprovalRequired,0) AS bitItemPriceApprovalRequired
									,ISNULL(Opp.numSOVendorID,0) AS numVendorID
									,' 
									+ CONCAT('ISNULL(Opp.monTotAmount,0) - (ISNULL(Opp.numUnitHour,0) * (CASE ',@avgCost,' WHEN 3 THEN ISNULL(V.monCost,0) / dbo.fn_UOMConversion(numPurchaseUnit,I.numItemCode,',@numDomainID,',numBaseUnit) WHEN 2 THEN (CASE WHEN ISNULL(Opp.numSOVendorID,0) > 0 THEN Opp.numCost ELSE ISNULL(V.monCost,0) / dbo.fn_UOMConversion(numPurchaseUnit,I.numItemCode,',@numDomainID,',numBaseUnit) END) ELSE monAverageCost END)) AS vcMargin,') + '
									OM.tintOppType
									,Opp.numItemCode
									,I.charItemType
									,ISNULL(I.bitAsset,0) as bitAsset
									,ISNULL(I.bitRental,0) as bitRental
									,ISNULL(I.bitSerialized,0) bitSerialized
									,ISNULL(I.bitLotNo,0) bitLotNo
									,Opp.numOppId
									,Opp.bitWorkOrder
									,Opp.fltDiscount
									,ISNULL(Opp.bitDiscountType,0) bitDiscountType
									,dbo.fn_UOMConversion(Opp.numUOMId, Opp.numItemCode,' + CAST(@numDomainId AS VARCHAR) +', NULL) AS UOMConversionFactor
									,ISNULL(Opp.bitWorkOrder,0) bitWorkOrder
									,ISNULL(Opp.numUnitHourReceived,0) numUnitHourReceived
									,ISNULL((SELECT COUNT(*) FROM OppWarehouseSerializedItem WHERE numOppId=Opp.numOppID AND numOppItemID=Opp.numoppitemtCode),0) AS numSerialNoAssigned
									,(CASE 
										WHEN (SELECT 
												COUNT(*) 
											FROM 
												OpportunityBizDocs OB 
											JOIN 
												dbo.OpportunityBizDocItems OBI 
											ON 
												ob.numOppBizDocsId=obi.numOppBizDocID 
												AND (ob.bitAuthoritativeBizDocs=1 OR ob.numBizDocId=304) 
												AND obi.numOppItemID=Opp.numoppitemtCode 
												AND ob.numOppId=Opp.numOppId)>0 
											THEN 1 
											ELSE 0 
									END) AS bitIsAuthBizDoc
									,(CASE 
										WHEN (SELECT 
													COUNT(*) 
												FROM 
													OpportunityBizDocs OB 
												JOIN 
													dbo.OpportunityBizDocItems OBI 
												ON 
													OB.numOppBizDocsId=OBI.numOppBizDocID 
												WHERE 
													OB.numOppId = Opp.numOppId 
													AND OBI.numOppItemID=Opp.numoppitemtCode 
													AND OB.numBizDocId=296) > 0 
											THEN 1 
											ELSE 0 
										END) AS bitAddedFulFillmentBizDoc
									,ISNULL(numPromotionID,0) AS numPromotionID
									,ISNULL(opp.numUOMId, 0) AS numUOM
									,(SELECT vcUnitName FROM dbo.UOM WHERE numUOMId = opp.numUOMId AND ISNULL(bitEnabled,0) = 1)  AS  [vcBaseUOMName]
									,Opp.numUnitHour AS numOriginalUnitHour 
									,ISNULL(M.vcPOppName,''Internal'') as Source
									,numSourceID
									,(CASE WHEN ISNULL(bitKitParent,0) = 1 AND ISNULL(bitAssembly,0) = 1 THEN 0 WHEN ISNULL(bitKitParent,0) = 1 THEN 1 ELSE 0 END) as bitKitParent
									,(CASE 
										WHEN OM.tintOppType = 1 AND OM.tintOppStatus = 1 
										THEN 
											(CASE 
												WHEN (I.charItemType = ''p'' AND ISNULL(bitDropShip, 0) = 0 AND ISNULL(bitAsset,0)=0)
												THEN 
													dbo.CheckOrderItemInventoryStatus(Opp.numItemCode,Opp.numUnitHour,Opp.numoppitemtCode,Opp.numWarehouseItmsID,(CASE WHEN ISNULL(bitKitParent,0) = 1 AND ISNULL(bitAssembly,0) = 1 THEN 0  WHEN ISNULL(bitKitParent,0) = 1 THEN 1 ELSE 0 END)) 
												ELSE 0 
											END) 
										ELSE 0 
									END) AS bitBackOrder
									,(SELECT vcData FROM dbo.ListDetails WHERE 1=1 AND numListItemID = numShipClass ) AS [numShipClass]
									,ISNULL(Opp.numQtyShipped,0) numQtyShipped
									,ISNULL(Opp.numUnitHourReceived,0) AS numUnitHourReceived
									,ISNULL(OM.fltExchangeRate,0) AS fltExchangeRate
									,ISNULL(OM.numCurrencyID,0) AS numCurrencyID
									,ISNULL(OM.numDivisionID,0) AS numDivisionID
									,(CASE 
										WHEN OM.tintOppType = 1 AND OM.tintOppStatus = 1
										THEN CAST(ISNULL(Opp.numQtyShipped,0) AS VARCHAR)
										WHEN OM.tintOppType = 2 AND OM.tintOppStatus = 1
										THEN CAST(ISNULL(Opp.numUnitHourReceived,0) AS VARCHAR)
										ELSE '''' 
									END) AS vcShippedReceived
									,ISNULL(I.numIncomeChartAcntId,0) AS itemIncomeAccount
									,ISNULL(I.numAssetChartAcntId,0) AS itemInventoryAsset
									,ISNULL(i.numCOGsChartAcntId,0) AS itemCoGs
									,ISNULL(Opp.numRentalIN,0) as numRentalIN
									,ISNULL(Opp.numRentalLost,0) as numRentalLost
									,ISNULL(Opp.numRentalOut,0) as numRentalOut
									,ISNULL(bitFreeShipping,0) AS [IsFreeShipping]
									,Opp.bitDiscountType,isnull(Opp.monLandedCost,0) as monLandedCost
									, ISNULL(Opp.vcPromotionDetail,'''') AS vcPromotionDetail
									,'''' AS numPurchasedQty
									,Opp.vcPathForTImage
									,Opp.vcItemName
									,Opp.vcModelID
									,vcWarehouse
									,ISNULL(WItems.numWarehouseID,0) AS numWarehouseID
									,ISNULL(Opp.numShipToAddressID,0) AS numShipToAddressID
									,Case when ISNULL(I.bitKitParent,0)=1 then CAST(ISNULL(dbo.fn_GetKitInventoryByWarehouse(I.numItemCode, WItems.numWareHouseID),0) AS DECIMAL(18,2)) ELSE CAST(ISNULL(WItems.numOnHand,0) AS FLOAT) END numOnHand
									,ISNULL(WItems.numAllocation,0) as numAllocation
									,WL.vcLocation
									,ISNULL(Opp.vcType,'''') ItemType
									,ISNULL(Opp.vcType,'''') vcType
									,ISNULL(Opp.vcItemDesc, '''') vcItemDesc
									,Opp.vcAttributes
									,ISNULL(bitDropShip, 0) as DropShip
									,CASE ISNULL(bitDropShip, 0) WHEN 1 THEN ''Yes'' ELSE '''' END AS bitDropShip
									,CAST(( dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), Opp.numItemCode,om.numDomainId, ISNULL(opp.numUOMId, 0))* Opp.numUnitHour ) AS NUMERIC(18, 2)) AS numUnitHour
									,Opp.monPrice
									,CAST(( dbo.fn_UOMConversion(ISNULL(opp.numUOMId, 0), Opp.numItemCode,om.numDomainId, ISNULL(I.numBaseUnit, 0)) * Opp.monPrice) AS NUMERIC(18, 6)) AS monPriceUOM
									,Opp.monTotAmount
									,Opp.monTotAmtBefDiscount
									,ISNULL(WItems.vcWHSKU,ISNULL(I.vcSKU,'''')) [vcSKU]
									,Opp.vcManufacturer
									,dbo.[fn_GetListItemName](I.[numItemClassification]) numItemClassification
									,(SELECT [vcItemGroup] FROM [ItemGroups] WHERE [numItemGroupID] = I.numItemGroup) numItemGroup
									,ISNULL(u.vcUnitName, '''') numUOMId
									,(CASE 
										WHEN ISNULL(I.bitKitParent,0)=1  
												AND (CASE 
														WHEN EXISTS (SELECT ID.numItemDetailID FROM ItemDetails ID INNER JOIN Item IInner ON ID.numChildItemID=IInner.numItemCode WHERE ID.numItemKitID=I.numItemCode AND ISNULL(IInner.bitKitParent,0)=1)
														THEN 1
														ELSE 0
													END) = 0
										THEN 
											dbo.GetKitWeightBasedOnItemSelection(I.numItemCode,Opp.vcChildKitSelectedItems)
										ELSE 
											ISNULL(I.fltWeight,0) 
									END) fltWeight
									,I.fltHeight
									,I.fltWidth
									,I.fltLength
									,I.numBarCodeId
									,I.IsArchieve
									,ISNULL(I.numContainer,0) AS numContainer
									,ISNULL(I.numNoItemIntoContainer,0) AS numContainerQty
									,CASE WHEN Opp.bitDiscountType = 0
										 THEN REPLACE(STR(ISNULL(Opp.monTotAmtBefDiscount, Opp.monTotAmount)
											  - Opp.monTotAmount,20,4), '' '', '''') + '' (''
											  + CAST(FORMAT(Opp.fltDiscount,''0.00##'') AS VARCHAR(50)) + ''%)''
										 ELSE REPLACE(STR(ISNULL(Opp.monTotAmtBefDiscount, Opp.monTotAmount)
											  - Opp.monTotAmount,20,4), '' '', '''')
									END AS DiscAmt
									,ISNULL(Opp.numProjectID, 0) numProjectID
									,CASE WHEN ( SELECT TOP 1
														ISNULL(OBI.numOppBizDocItemID, 0)
												FROM    dbo.OpportunityBizDocs OBD
														INNER JOIN dbo.OpportunityBizDocItems OBI ON OBD.numOppBizDocsID = OBI.numOppBizDocID
												WHERE   OBI.numOppItemID = Opp.numoppitemtCode
														AND ISNULL(OBD.bitAuthoritativeBizDocs, 0) = 1
														and isnull(OBD.numOppID,0)=OM.numOppID
											  ) > 0 THEN 1
										 ELSE 0
									END AS bitItemAddedToAuthBizDoc
									,ISNULL(Opp.numClassID, 0) numClassID
									,ISNULL(Opp.numProjectStageID, 0) numProjectStageID
									,CASE 
										WHEN OPP.numProjectStageID > 0 THEN dbo.fn_GetProjectStageName(OPP.numProjectId,OPP.numProjectStageID)
										WHEN OPP.numProjectStageID = -1 THEN ''T&E''
										WHEN OPP.numProjectId > 0 THEN CASE WHEN OM.tintOppType = 1 THEN ''S.O.'' ELSE ''P.O.'' END
										ELSE ''-''
									END AS vcProjectStageName
									,CAST(Opp.numUnitHour AS FLOAT) numQty
									,ISNULL((SELECT sum(numQty) FROM OppWarehouseSerializedItem WHERE numOppID=OM.numOppID AND numOppItemID=Opp.numoppitemtCode),0) AS SerialLotNo
									,Opp.numWarehouseItmsID,CPN.CustomerPartNo
									,(CASE WHEN ISNULL(Opp.bitMarkupDiscount,0) = 0 THEN ''0'' ELSE ''1'' END) AS bitMarkupDiscount
									,CAST(Opp.ItemRequiredDate AS DATE) AS ItemRequiredDate,ISNULL(bitWinningBid,0) bitWinningBid, ISNULL(numParentOppItemID,0) numParentOppItemID,ISNULL(Opp.numQtyReceived,0) numQtyReceived,'


		
			   IF EXISTS (SELECT * FROM #tempForm WHERE vcOrigDbColumnName='vcWorkOrderStatus' AND ISNULL(numUserCntID,0) = @numUserCntID)
			   BEGIN
					SET @strSQL = @strSQL + CONCAT('dbo.GetWorkOrderStatus(om.numDomainID,',@tintCommitAllocation,',Opp.numOppID,Opp.numoppitemtCode) AS vcWorkOrderStatus,')
			   END     

			   IF EXISTS (SELECT * FROM #tempForm WHERE vcOrigDbColumnName='vcBackordered' AND ISNULL(numUserCntID,0) = @numUserCntID)
			   BEGIN
					SET @strSQL = @strSQL + '(CASE 
												WHEN OM.tintOppType = 1 AND OM.tintOppStatus = 1 
												THEN 
													(CASE 
														WHEN (I.charItemType = ''p'' AND ISNULL(bitDropShip, 0) = 0 AND ISNULL(bitAsset,0)=0)
														THEN 
															CAST(dbo.CheckOrderItemInventoryStatus(Opp.numItemCode,Opp.numUnitHour,Opp.numoppitemtCode,Opp.numWarehouseItmsID,(CASE WHEN ISNULL(bitKitParent,0) = 1 AND ISNULL(bitAssembly,0) = 1 THEN 0  WHEN ISNULL(bitKitParent,0) = 1 THEN 1 ELSE 0 END)) AS VARCHAR)
														ELSE ''''
													END) 
												ELSE ''''
											END) vcBackordered, '
			   END


			   IF EXISTS (SELECT * FROM #tempForm WHERE vcOrigDbColumnName='vcItemReleaseDate' AND ISNULL(numUserCntID,0) = @numUserCntID)
			   BEGIN
					SET @strSQL = @strSQL + '(CASE WHEN I.charItemType = ''p'' AND ISNULL(bitDropShip, 0) = 0 THEN ISNULL(dbo.FormatedDateFromDate(Opp.ItemReleaseDate,om.numDomainId),''Not Available'') ELSE '''' END)  AS vcItemReleaseDate,'
			   END

			   IF EXISTS (SELECT * FROM #tempForm WHERE vcOrigDbColumnName='SerialLotNo' AND ISNULL(numUserCntID,0) = @numUserCntID)
			   BEGIN
					SET @strSQL = @strSQL + 'SUBSTRING((SELECT  
													'', '' + vcSerialNo + CASE WHEN ISNULL(I.bitLotNo, 0) = 1 THEN '' ('' + CONVERT(VARCHAR(15), oppI.numQty) + '')'' ELSE ''''	END
												FROM 
													OppWarehouseSerializedItem oppI
												JOIN 
													WareHouseItmsDTL whi 
												ON 
													oppI.numWareHouseItmsDTLID = whi.numWareHouseItmsDTLID
												WHERE 
													oppI.numOppID = Opp.numOppId
													AND oppI.numOppItemID = Opp.numoppitemtCode
												ORDER BY 
													vcSerialNo
												FOR
													XML PATH('''')), 3, 200000) AS vcSerialLotNo,'
			   END
			   
			   IF EXISTS (SELECT * FROM #tempForm WHERE vcOrigDbColumnName='vcInclusionDetails' AND ISNULL(numUserCntID,0) = @numUserCntID)
			   BEGIN
					SET @strSQL = @strSQL + CONCAT('dbo.GetOrderAssemblyKitInclusionDetails(OM.numOppID,Opp.numoppitemtCode,Opp.numUnitHour,',@tintCommitAllocation,',0) AS vcInclusionDetails,')
			   END    
			   
			   IF EXISTS (SELECT * FROM #tempForm WHERE vcOrigDbColumnName='OppItemShipToAddress' )
			   BEGIN
					SET @strSQL = @strSQL +  '(CASE WHEN Opp.numShipToAddressID > 0 THEN (+ isnull((AD.vcStreet)  +'', '','''') + isnull((AD.vcCity)+ '', '','''')  + '' <br>''
                            + isnull(dbo.fn_GetState(AD.numState) + '', '' ,'''') 
                            + isnull(AD.vcPostalCode + '', '' ,'''')  + '' <br>''  +
                            + isnull(dbo.fn_GetListItemName(AD.numCountry),'''') )
							ELSE '''' END ) AS OppItemShipToAddress,'
			   END   
	
			-- (CASE WHEN @tintOppType=1 THEN 26 ELSE 129 END) --

		--------------Add custom fields to query----------------
		SELECT TOP 1 @fld_Name= vcFieldName,@fld_ID = numFieldId FROM #tempForm WHERE ISNULL(bitCustomField,0)=1 ORDER BY numFieldId
		WHILE @fld_ID > 0
		BEGIN 
			SET @strSQL = @strSQL + CONCAT('dbo.GetCustFldValueOppItems(',@fld_ID,',Opp.numoppitemtCode,Opp.numItemCode) as ''',@fld_Name,''',')
       
			SELECT TOP 1 @fld_Name= vcFieldName,@fld_ID = numFieldId FROM #tempForm WHERE ISNULL(bitCustomField,0)=1 AND numFieldId > @fld_ID ORDER BY numFieldId
		 
			IF @@ROWCOUNT=0
				SET @fld_ID = 0
		END

		 SET @strSQL = left(@strSQL,len(@strSQL)-1) + '   
				FROM    OpportunityItems Opp
				LEFT JOIN OpportunityMaster OM ON Opp.numOppID = oM.numOppID
				INNER JOIN DivisionMaster DM ON OM.numDivisionID=DM.numDivisionID
				JOIN item I ON Opp.numItemCode = i.numItemcode
				LEFT JOIN WareHouseItems WItems ON WItems.numWareHouseItemID = Opp.numWarehouseItmsID
				LEFT JOIN Warehouses W ON W.numWarehouseID = WItems.numWarehouseID
				LEFT JOIN WarehouseLocation WL ON WL.numWLocationID = WItems.numWLocationID
				LEFT JOIN OpportunityMaster M ON Opp.numSourceID = M.numOppID
				LEFT JOIN UOM u ON u.numUOMId = opp.numUOMId
				LEFT JOIN Vendor V ON I.numVendorID = V.numVendorID  AND V.numItemCode = I.numItemCode
				LEFT JOIN CustomerPartNumber CPN ON CPN.numItemCode = Opp.numItemCode AND CPN.numCompanyId=DM.numCompanyID
				LEFT JOIN AddressDetails AD ON Opp.numShipToAddressID = AD.numAddressID
		WHERE   om.numDomainId = '+ CONVERT(VARCHAR(10),@numDomainId) + '
				AND Opp.numOppId = '+ CONVERT(VARCHAR(15),@OpportunityId) +'
		ORDER BY opp.numSortOrder,numoppitemtCode ASC '
  
		--PRINT CAST(@strSQL AS NTEXT);
		EXEC (@strSQL);
		SELECT  vcSerialNo,
			   vcComments AS Comments,
				numOppItemID AS numoppitemtCode,
				W.numWarehouseItmsDTLID,
				numWarehouseItmsID AS numWItmsID,
				dbo.Fn_getattributes(W.numWarehouseItmsDTLID,
									 1) Attributes
		FROM    WareHouseItmsDTL W
				JOIN OppWarehouseSerializedItem O ON O.numWarehouseItmsDTLID = W.numWareHouseItmsDTLID
		WHERE   numOppID = @OpportunityId


		select * from #tempForm order by tintOrder

		drop table #tempForm

    END         
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityMaster_GetShippingDetail')
DROP PROCEDURE USP_OpportunityMaster_GetShippingDetail
GO
CREATE PROCEDURE [dbo].[USP_OpportunityMaster_GetShippingDetail]
	@numDomainId NUMERIC(18,0)
	,@numOppID NUMERIC(18,0)
	,@numShippingReportID NUMERIC(18,0)
AS
BEGIN
	DECLARE @IsCOD BIT,@IsDryIce BIT,@IsHoldSaturday BIT,@IsHomeDelivery BIT,@IsInsideDelevery BIT,@IsInsidePickup BIT,@IsReturnShipment BIT,@IsSaturdayDelivery BIT,@IsSaturdayPickup BIT,@IsAdditionalHandling BIT,@IsLargePackage BIT
	DECLARE @vcCODType VARCHAR(50),@vcDeliveryConfirmation VARCHAR(1000),@vcDescription VARCHAR(MAX)
	DECLARE @tintSignatureType TINYINT
	DECLARE @numOrderShippingCompany NUMERIC(18,0)
	DECLARE @numOrderShippingService NUMERIC(18,0)
	DECLARE @numDivisionID NUMERIC(18,0)
	DECLARE @numDivisionShippingCompany NUMERIC(18,0)
    DECLARE @numDivisionShippingService NUMERIC(18,0)
	DECLARE @numShippingCompany NUMERIC(18,0)
	DECLARE @numShippingService NUMERIC(18,0)
	DECLARE @vcFromName VARCHAR(1000)
    DECLARE @vcFromCompany VARCHAR(1000)
    DECLARE @vcFromPhone VARCHAR(100)
    DECLARE @vcFromAddressLine1 VARCHAR(50)
    DECLARE @vcFromAddressLine2 VARCHAR(50) = ''
    DECLARE @vcFromCity VARCHAR(50)
	DECLARE @vcFromState NUMERIC(18,0)
    DECLARE @vcFromZip VARCHAR(50)
    DECLARE @vcFromCountry NUMERIC(18,0)
	DECLARE @bitFromResidential BIT = 0

	DECLARE @vcToName VARCHAR(1000)
    DECLARE @vcToCompany VARCHAR(1000)
    DECLARE @vcToPhone VARCHAR(100)
    DECLARE @vcToAddressLine1 VARCHAR(50)
    DECLARE @vcToAddressLine2 VARCHAR(50) = ''
	DECLARE @vcToCity VARCHAR(50)
	DECLARE @vcToState NUMERIC(18,0)
    DECLARE @vcToZip VARCHAR(50)
    DECLARE @vcToCountry NUMERIC(18,0)
    DECLARE @bitToResidential BIT = 0
	DECLARE @numOppBizDocID NUMERIC(18,0) = 0
	DECLARE @tintPayorType TINYINT
	DECLARE @vcPayorAccountNo VARCHAR(100)
	DECLARE @numPayorCountry NUMERIC(18,0)
	DECLARE @vcPayorZip VARCHAR(20)


	IF EXISTS (SELECT numShippingReportId FROM ShippingReport WHERE numDomainId = @numDomainId AND numShippingReportId = @numShippingReportID) 
	BEGIN
		SELECT 
			@numOppBizDocID=numOppBizDocId
			,@IsCOD = IsCOD
			,@IsHomeDelivery = IsHomeDelivery
			,@IsInsideDelevery = IsInsideDelevery
			,@IsInsidePickup = IsInsidePickup
			,@IsSaturdayDelivery = IsSaturdayDelivery
			,@IsSaturdayPickup = IsSaturdayPickup
			,@IsAdditionalHandling = IsAdditionalHandling
			,@IsLargePackage=IsLargePackage
			,@vcCODType = vcCODType
			,@vcDeliveryConfirmation = vcDeliveryConfirmation
			,@vcDescription = vcDescription
			,@tintSignatureType = ISNULL(tintSignatureType,0)
			,@vcFromName=vcFromName
			,@vcFromCompany=vcFromCompany
			,@vcFromPhone=vcFromPhone
			,@vcFromAddressLine1=vcFromAddressLine1
			,@vcFromCity=vcFromCity
			,@vcFromState=CAST(vcFromState AS NUMERIC)
			,@vcFromZip=vcFromZip
			,@vcFromCountry=CAST(vcFromCountry AS NUMERIC)
			,@bitFromResidential=bitFromResidential
			,@vcToName=vcToName
			,@vcToCompany=vcToCompany
			,@vcToPhone=vcToPhone
			,@vcToAddressLine1=vcToAddressLine1
			,@vcToCity=vcToCity
			,@vcToState=CAST(vcToState AS NUMERIC)
			,@vcToZip=vcToZip
			,@vcToCountry=CAST(vcToCountry AS NUMERIC)
			,@bitToResidential=bitToResidential
			,@vcCODType=vcCODType
			,@vcDeliveryConfirmation=vcDeliveryConfirmation
			,@vcDescription=vcDescription
			,@numShippingCompany=numShippingCompany
			,@numShippingService=vcValue2
			,@tintPayorType=ISNULL(tintPayorType,0)
			,@vcPayorAccountNo=ISNULL(vcPayorAccountNo,'')
			,@numPayorCountry=ISNULL(numPayorCountry,0)
			,@vcPayorZip=ISNULL(vcPayorAccountNo,'')
		FROM
			ShippingReport
		WHERE
			numDomainId = @numDomainId
			AND numShippingReportId = @numShippingReportID
	END
	ELSE
	BEGIN
		SELECT
			@IsCOD = IsCOD
			,@IsHomeDelivery = IsHomeDelivery
			,@IsInsideDelevery = IsInsideDelevery
			,@IsInsidePickup = IsInsidePickup
			,@IsSaturdayDelivery = IsSaturdayDelivery
			,@IsSaturdayPickup = IsSaturdayPickup
			,@IsAdditionalHandling = IsAdditionalHandling
			,@IsLargePackage=IsLargePackage
			,@vcCODType = vcCODType
			,@vcDeliveryConfirmation = vcDeliveryConfirmation
			,@vcDescription = vcDescription
			,@tintSignatureType = CAST(ISNULL(vcSignatureType,'0') AS TINYINT)
			,@numOrderShippingCompany = ISNULL(intUsedShippingCompany,0)
			,@numOrderShippingService = ISNULL(numShippingService,0)
			,@numDivisionID=DivisionMaster.numDivisionID
			,@numDivisionShippingCompany = ISNULL(intShippingCompany,0)
			,@numDivisionShippingService = ISNULL(numDefaultShippingServiceID,0)
			,@tintPayorType=(CASE WHEN ISNULL(bitUseShippersAccountNo,0) = 1 THEN 2 ELSE 0 END)
		FROM
			OpportunityMaster
		INNER JOIN
			DivisionMaster
		ON
			OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
		LEFT JOIN
			DivisionMasterShippingConfiguration
		ON
			DivisionMasterShippingConfiguration.numDomainID=@numDomainId
			AND DivisionMaster.numDivisionID = DivisionMasterShippingConfiguration.numDivisionID
		WHERE
			OpportunityMaster.numDomainId = @numDomainId
			AND DivisionMaster.numDomainID=@numDomainId
			AND OpportunityMaster.numOppId = @numOppID

		-- GET FROM ADDRESS
		SELECT
			@vcFromName=vcName
			,@vcFromCompany=vcCompanyName
			,@vcFromPhone=vcPhone
			,@vcFromAddressLine1=vcStreet
			,@vcFromCity=vcCity
			,@vcFromState=vcState
			,@vcFromZip=vcZipCode
			,@vcFromCountry=vcCountry
			,@bitFromResidential=bitResidential
		FROM
			dbo.fn_GetShippingReportAddress(1,@numDomainId,@numOppID)

		-- GET TO ADDRESS
		SELECT
			@vcToName=vcName
			,@vcToCompany=vcCompanyName
			,@vcToPhone=vcPhone
			,@vcToAddressLine1=vcStreet
			,@vcToCity=vcCity
			,@vcToState=vcState
			,@vcToZip=vcZipCode
			,@vcToCountry=vcCountry
			,@bitToResidential=bitResidential
		FROM
			dbo.fn_GetShippingReportAddress(2,@numDomainId,@numOppID)

		-- FIRST CHECK IF ORDER LEVEL SHIPPING FIELDS HAVE VALUES
		IF ISNULL(@numOrderShippingCompany,0) > 0
		BEGIN
			IF ISNULL(@numOrderShippingService,0) = 0
			BEGIN
				SET @numOrderShippingService = CASE @numOrderShippingCompany 
										WHEN 91 THEN 15 --FEDEX
										WHEN 88 THEN 43 --UPS
										WHEN 90 THEN 70 --USPS
									 END
			END

			SET @numShippingCompany = @numOrderShippingCompany
			SET @numShippingService = @numOrderShippingService
		END
		ELSE IF ISNULL(@numDivisionShippingCompany,0) > 0 -- IF DIVISION SHIPPIGN SETTING AVAILABLE
		BEGIN
			IF ISNULL(@numDivisionShippingService,0) = 0
			BEGIN
				SET @numDivisionShippingService = CASE @numDivisionShippingCompany 
										WHEN 91 THEN 15 --FEDEX
										WHEN 88 THEN 43 --UPS
										WHEN 90 THEN 70 --USPS
									 END
			END

			SET @numShippingCompany = @numDivisionShippingCompany
			SET @numShippingService = @numDivisionShippingService
		END
		ELSE -- USE DOMAIN DEFAULT
		BEGIN
			SELECT @numShippingCompany=ISNULL(numShipCompany,0) FROM Domain WHERE numDomainId=@numDomainId
		
			IF @numShippingCompany <> 91 OR @numShippingCompany <> 88 OR @numShippingCompany <> 90
			BEGIN
				SET @numShippingCompany = 91
			END

			SET @numShippingService = CASE @numShippingCompany 
										WHEN 91 THEN 15 --FEDEX
										WHEN 88 THEN 43 --UPS
										WHEN 90 THEN 70 --USPS
									 END
		END

		IF @tintPayorType = 2 AND EXISTS (SELECT ID FROM DivisionMasterShippingAccount WHERE numDivisionID=@numDivisionID AND numShipViaID=@numShippingCompany)
		BEGIN
			SELECT
				@vcPayorAccountNo=ISNULL(vcAccountNumber,'')
				,@numPayorCountry=numCountry
				,@vcPayorZip=vcZipCode
			FROM
				DivisionMasterShippingAccount
			WHERE
				numDivisionID=@numDivisionID 
				AND numShipViaID=@numShippingCompany
		END
	END

	SELECT
		@numOppID numOppID
		,ISNULL(@numOppBizDocID,0) numOppBizDocID
		,@numShippingCompany numShippingCompany
		,@numShippingService numShippingService
		,@vcFromName vcFromName
		,@vcFromCompany vcFromCompanyName
		,@vcFromPhone vcFromPhone
		,@vcFromAddressLine1 vcFromStreet
		,@vcFromCity vcFromCity
		,@vcFromState numFromState
		,@vcFromZip vcFromZipCode
		,@vcFromCountry numFromCountry
		,@bitFromResidential bitFromResidential
		,@vcToName vcToName
		,@vcToCompany vcToCompanyName
		,@vcToPhone vcToPhone
		,@vcToAddressLine1 vcToStreet
		,@vcToCity vcToCity
		,@vcToState numToState
		,@vcToZip vcToZipCode
		,@vcToCountry numToCountry
		,@bitToResidential bitToResidential
		,@IsCOD bitCOD
		,@IsDryIce bitDryIce
		,@IsHoldSaturday bitHoldSaturday
		,@IsHomeDelivery bitHomeDelivery
		,@IsInsideDelevery bitInsideDelevery
		,@IsInsidePickup bitIsInsidePickup
		,@IsReturnShipment bitReturnShipment
		,@IsSaturdayDelivery bitSaturdayDelivery
		,@IsSaturdayPickup bitIsSaturdayPickup
		,@IsAdditionalHandling bitAdditionalHandling
		,@IsLargePackage bitLargePackage
		,@vcCODType vcCODType
		,@vcDeliveryConfirmation vcDeliveryConfirmation
		,@vcDescription vcDescription
		,@tintSignatureType tintSignatureType
		,@tintPayorType tintPayorType
		,@vcPayorAccountNo vcPayorAccountNo
		,@numPayorCountry numPayorCountry
		,@vcPayorZip vcPayorZip
END
GO
/****** Object:  StoredProcedure [dbo].[USP_ReOpenOppertunity]    Script Date: 07/26/2008 16:20:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ReOpenOppertunity')
DROP PROCEDURE USP_ReOpenOppertunity
GO
CREATE PROCEDURE [dbo].[USP_ReOpenOppertunity]            
@OppID as numeric(9),
@numUserCntID AS NUMERIC(9),
@numDomainID AS NUMERIC(9)
          
as          
BEGIN TRY
	-- Added by Manish Anjara : 17th Sep,2014
	-- Check whether order is already opened or not. If already opened then show a warning message
	IF (SELECT ISNULL(tintshipped,0) FROM [dbo].[OpportunityMaster] WHERE [OpportunityMaster].[numOppId] = @OppID AND [OpportunityMaster].[numDomainId] = @numDomainID) = 0
	BEGIN
		RAISERROR('ORDER_OPENED', 16, 1)
	END

   BEGIN TRANSACTION       
			declare @status as varchar(2)            
			declare @OppType as varchar(2)  
			DECLARE @fltExchangeRate AS FLOAT 
			          
			select @status=tintOppStatus,@OppType=tintOppType,@fltExchangeRate=(CASE WHEN ISNULL(fltExchangeRate,0)=0 THEN 1 ELSE fltExchangeRate END) from OpportunityMaster            
			where numOppId=@OppID AND [numDomainId] = @numDomainID

			update OpportunityMaster set tintshipped=0,bintAccountClosingDate=NULL,bintClosedDate=NULL where  numOppId=@OppID  AND [numDomainId] = @numDomainID
			UPDATE [OpportunityBizDocs] SET [dtShippedDate] = NULL WHERE [numOppId]=@OppID AND numBizDocID <> 296
        			
			-- WE ARE REVERTING RECEIVED (PURCHASED) QTY ONLY BECAUSE USER HAS TO DELETE FULFILLMENT ORDER TO REVERSE SHIPPED (SALED) QTY
			IF @OppType = 2
			BEGIN
				--SET Received qty to 0	
				UPDATE OpportunityItems SET numUnitHourReceived=0,numQtyShipped=0,numQtyReceived=0 WHERE [numOppId]=@OppID
				UPDATE OpportunityKitItems SET numQtyShipped=0 WHERE [numOppId]=@OppID
			END
			
			-- UnArchive item based on Item's individual setting
			IF @OppType = 1
			BEGIN
				UPDATE Item SET IsArchieve = CASE WHEN EXISTS(SELECT OpportunityItems.numItemCode FROM dbo.OpportunityItems 
															  WHERE OpportunityItems.numItemCode = I.numItemCode
															  AND OpportunityItems.numoppitemtCode = OI.numoppitemtCode) 
												  THEN 0
												  ELSE 1 
											 END 
				FROM Item I JOIN dbo.OpportunityItems OI ON I.numItemCode = OI.numItemCode  
				WHERE numOppId = @OppID 
				AND I.[numDomainId] = @numDomainID
				AND ISNULL(I.bitArchiveItem,0) = 1 
			END     
			----------------------------------------------------------------------------            
			--Updating Item Details            
			-----------------------------------------------------------------------            
            
			---Updating the inventory Items once the deal is won              
			declare @numoppitemtCode as numeric            
			declare @numUnits as FLOAT              
			declare @numWarehouseItemID as numeric       
			declare @itemcode as numeric        
			declare @QtyShipped as FLOAT
			declare @QtyReceived as FLOAT
			Declare @monPrice as DECIMAL(20,5)  
			declare @Kit as bit  
			declare @bitWorkOrder AS BIT
    
			 select top 1 @numoppitemtCode=numoppitemtCode,@itemcode=OI.numItemCode,@numUnits=ISNULL(numUnitHour,0),@qtyShipped=ISNULL([numQtyShipped],0),@qtyReceived=ISNULL([numUnitHourReceived],0),
			 @numWareHouseItemID=ISNULL(numWarehouseItmsID,0),@monPrice=isnull(monPrice,0) * @fltExchangeRate,@Kit= (case when bitKitParent=1 and bitAssembly=1 then 0 when bitKitParent=1 then 1 else 0 end),
			 @bitWorkOrder = bitWorkOrder
			 from OpportunityItems OI join Item I                                                
			 on OI.numItemCode=I.numItemCode and numOppId=@OppID and (bitDropShip=0 or bitDropShip is null)                                            
			 where (charitemtype='P' OR 1=(CASE WHEN @OppType=1 THEN 
							CASE WHEN ISNULL(I.bitKitParent,0) = 1 AND ISNULL(I.bitAssembly,0) = 1 THEN 0
								 WHEN ISNULL(I.bitKitParent,0) = 1 THEN 1
								 ELSE 0 END 
							ELSE 0 END))  
			 AND I.[numDomainId] = @numDomainID
			order by OI.numoppitemtCode      
            
			 while @numoppitemtCode>0                
			 begin   
 
				EXEC usp_ManageInventory @itemcode,@numWareHouseItemID,0,@OppType,@numUnits,@qtyShipped,@qtyReceived,@monPrice,3,@OppID,@numoppitemtCode,@numUserCntID,@bitWorkOrder
        
			  select top 1 @numoppitemtCode=numoppitemtCode,@itemcode=OI.numItemCode,@numUnits=ISNULL(numUnitHour,0),@qtyShipped=ISNULL([numQtyShipped],0),@qtyReceived=ISNULL([numUnitHourReceived],0),
			  @numWareHouseItemID=ISNULL(numWarehouseItmsID,0),@monPrice=isnull(monPrice,0) * @fltExchangeRate,@Kit= (case when bitKitParent=1 and bitAssembly=1 then 0 when bitKitParent=1 then 1 else 0 end) 
			  from OpportunityItems OI                                                
			  join Item I                                                
			  on OI.numItemCode=I.numItemCode and numOppId=@OppID                                              
			  where (charitemtype='P' OR 1=(CASE WHEN @OppType=1 THEN 
							CASE WHEN ISNULL(I.bitKitParent,0) = 1 AND ISNULL(I.bitAssembly,0) = 1 THEN 0
								 WHEN ISNULL(I.bitKitParent,0) = 1 THEN 1
								 ELSE 0 END 
							ELSE 0 END))
					and OI.numoppitemtCode>@numoppitemtCode and (bitDropShip=0 or bitDropShip is null) 
					AND I.[numDomainId] = @numDomainID
					order by OI.numoppitemtCode                                                
			  if @@rowcount=0 set @numoppitemtCode=0     
  
			 END
				

			IF @OppType=2
			BEGIN
				IF (
					SELECT 
						COUNT(*)
					FROM 
						OppWarehouseSerializedItem OWSI 
					INNER JOIN 
						OpportunityMaster OM 
					ON 
						OWSI.numOppID=OM.numOppId 
						AND tintOppType=1 
					WHERE 
						OWSI.numWarehouseItmsDTLID IN (SELECT numWarehouseItmsDTLID FROM OppWarehouseSerializedItem OWSIInner WHERE OWSIInner.numOppID = @OppID)
					) > 0
				BEGIN
					RAISERROR ('SERIAL/LOT#_USED', 16, 1 ) ;
				END
				ELSE
				BEGIN
					-- REMOVE SERIAL/LOT# NUMBER FROM INVENTORY BECAUSE PURCHASE ORDER IS DELETED
					UPDATE WHIDL
						SET WHIDL.numQty = (CASE WHEN Item.bitLotNo = 1 THEN ISNULL(WHIDL.numQty,0) - ISNULL(OWSI.numQty,0) ELSE 0 END)
					FROM 
						WareHouseItmsDTL WHIDL
					INNER JOIN
						OppWarehouseSerializedItem OWSI
					ON
						WHIDL.numWareHouseItmsDTLID = OWSI.numWarehouseItmsDTLID
						AND WHIDL.numWareHouseItemID = OWSI.numWarehouseItmsID
					INNER JOIN
						WareHouseItems
					ON
						WHIDL.numWareHouseItemID = WareHouseItems.numWareHouseItemID
					INNER JOIN
						Item
					ON
						WareHouseItems.numItemID = Item.numItemCode
					WHERE
						OWSI.numOppID = @OppID
				END
				
				DELETE FROM OppWarehouseSerializedItem WHERE numOppID=@OppID
			END
		
			
			IF @OppType=2
			BEGIn
			DELETE FROM dbo.General_Journal_Details WHERE numJournalId IN (SELECT numJournal_Id FROM dbo.General_Journal_Header WHERE numDomainID=@numDomainID AND numOppId=@OppID AND ISNULL(numOppBizDocsId,0)=0 
				AND isnull(numBillID,0)=0 AND isnull(numBillPaymentID,0)=0 AND isnull(numPayrollDetailID,0)=0
				AND isnull(numCheckHeaderID,0)=0 AND isnull(numReturnID,0)=0 
				AND isnull(numDepositId,0)=0)
			
			DELETE FROM General_Journal_Header WHERE numDomainID=@numDomainID AND numOppId=@OppID AND ISNULL(numOppBizDocsId,0)=0 
				AND isnull(numBillID,0)=0 AND isnull(numBillPaymentID,0)=0 AND isnull(numPayrollDetailID,0)=0
				AND isnull(numCheckHeaderID,0)=0 AND isnull(numReturnID,0)=0 
				AND isnull(numDepositId,0)=0
			END
COMMIT
END TRY
BEGIN CATCH
  -- Whoops, there was an error
  IF @@TRANCOUNT > 0
     ROLLBACK

  -- Raise an error with the details of the exception
  DECLARE @ErrMsg nvarchar(4000), @ErrSeverity int
  SELECT @ErrMsg = ERROR_MESSAGE(),
         @ErrSeverity = ERROR_SEVERITY()

  RAISERROR(@ErrMsg, @ErrSeverity, 1)
END CATCH


GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ShippingReport_SaveMassSalesFulfillment')
DROP PROCEDURE USP_ShippingReport_SaveMassSalesFulfillment
GO
CREATE PROCEDURE [dbo].[USP_ShippingReport_SaveMassSalesFulfillment]
	@numDomainId NUMERIC(18,0)
	,@numUserCntID NUMERIC(18,0)
	,@numOppID NUMERIC(18,0)
    ,@numOppBizDocId NUMERIC(18,0)
	,@numShippingCompany NUMERIC(18,0)
	,@numShippingService NUMERIC(18,0)
	,@vcBoxes VARCHAR(MAX)
	,@vcBoxItems VARCHAR(MAX)
	,@vcShippingDetail VARCHAR(MAX)
AS
BEGIN
BEGIN TRY
BEGIN TRANSACTION
	
	DECLARE @numShippingReportID NUMERIC(18,0)
	DECLARE @numOrderShippingCompany NUMERIC(18,0)
	DECLARE @numOrderShippingService NUMERIC(18,0)
	DECLARE @numDivisionShippingCompany NUMERIC(18,0)
    DECLARE @numDivisionShippingService NUMERIC(18,0)

	IF ISNULL(@numShippingCompany,0) = 0 OR ISNULL(@numShippingService,0) = 0
	BEGIN
		SELECT
			@numOrderShippingCompany = ISNULL(intUsedShippingCompany,0)
			,@numOrderShippingService = ISNULL(numShippingService,0)
			,@numDivisionShippingCompany = ISNULL(intShippingCompany,0)
			,@numDivisionShippingService = ISNULL(numDefaultShippingServiceID,0)
		FROM
			OpportunityMaster
		INNER JOIN
			DivisionMaster
		ON
			OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
		WHERE
			OpportunityMaster.numDomainId = @numDomainId
			AND DivisionMaster.numDomainID=@numDomainId
			AND OpportunityMaster.numOppId = @numOppID

	
		-- FIRST CHECK IF ORDER LEVEL SHIPPING FIELDS HAVE VALUES
		IF ISNULL(@numOrderShippingCompany,0) > 0
		BEGIN
			IF ISNULL(@numOrderShippingService,0) = 0
			BEGIN
				SET @numOrderShippingService = CASE @numOrderShippingCompany 
										WHEN 91 THEN 15 --FEDEX
										WHEN 88 THEN 43 --UPS
										WHEN 90 THEN 70 --USPS
									 END
			END

			SET @numShippingCompany = @numOrderShippingCompany
			SET @numShippingService = @numOrderShippingService
		END
		ELSE IF ISNULL(@numDivisionShippingCompany,0) > 0 -- IF DIVISION SHIPPIGN SETTING AVAILABLE
		BEGIN
			IF ISNULL(@numDivisionShippingService,0) = 0
			BEGIN
				SET @numDivisionShippingService = CASE @numDivisionShippingCompany 
										WHEN 91 THEN 15 --FEDEX
										WHEN 88 THEN 43 --UPS
										WHEN 90 THEN 70 --USPS
									 END
			END

			SET @numShippingCompany = @numDivisionShippingCompany
			SET @numShippingService = @numDivisionShippingService
		END
		ELSE -- USE DOMAIN DEFAULT
		BEGIN
			SELECT @numShippingCompany=ISNULL(numShipCompany,0) FROM Domain WHERE numDomainId=@numDomainId
		
			IF @numShippingCompany <> 91 OR @numShippingCompany <> 88 OR @numShippingCompany <> 90
			BEGIN
				SET @numShippingCompany = 91
			END

			SET @numShippingService = CASE @numShippingCompany 
										WHEN 91 THEN 15 --FEDEX
										WHEN 88 THEN 43 --UPS
										WHEN 90 THEN 70 --USPS
									 END
		END
	END

	

	DECLARE @vcFromName VARCHAR(1000)
    DECLARE @vcFromCompany VARCHAR(1000)
    DECLARE @vcFromPhone VARCHAR(100)
    DECLARE @vcFromAddressLine1 VARCHAR(50)
    DECLARE @vcFromAddressLine2 VARCHAR(50) = ''
    DECLARE @vcFromCity VARCHAR(50)
	DECLARE @vcFromState VARCHAR(50)
    DECLARE @vcFromZip VARCHAR(50)
    DECLARE @vcFromCountry VARCHAR(50)
	DECLARE @bitFromResidential BIT = 0

	DECLARE @vcToName VARCHAR(1000)
    DECLARE @vcToCompany VARCHAR(1000)
    DECLARE @vcToPhone VARCHAR(100)
    DECLARE @vcToAddressLine1 VARCHAR(50)
    DECLARE @vcToAddressLine2 VARCHAR(50) = ''
	DECLARE @vcToCity VARCHAR(50)
	DECLARE @vcToState VARCHAR(50)
    DECLARE @vcToZip VARCHAR(50)
    DECLARE @vcToCountry VARCHAR(50)
    DECLARE @bitToResidential BIT = 0

	DECLARE @IsCOD BIT,@IsDryIce BIT,@IsHoldSaturday BIT,@IsHomeDelivery BIT,@IsInsideDelevery BIT,@IsInsidePickup BIT,@IsReturnShipment BIT,@IsSaturdayDelivery BIT,@IsSaturdayPickup BIT,@IsAdditionalHandling BIT,@IsLargePackage BIT
	DECLARE @numCODAmount NUMERIC(18,2),@vcCODType VARCHAR(50),@vcDeliveryConfirmation VARCHAR(1000),@vcDescription VARCHAR(MAX)
	DECLARE @tintSignatureType TINYINT
	DECLARE @tintPayorType TINYINT=0
	DECLARE @vcPayorAccountNo VARCHAR(20)=''
	DECLARE @numPayorCountry NUMERIC(18,0)
	DECLARE @vcPayorZip VARCHAR(50)=''
	DECLARE @numTotalCustomsValue NUMERIC(18,2)
	DECLARE @numTotalInsuredValue NUMERIC(18,2)

	IF LEN(ISNULL(@vcShippingDetail,'')) > 0
	BEGIN
		DECLARE @hDocShippingDetail as INTEGER
		EXEC sp_xml_preparedocument @hDocShippingDetail OUTPUT, @vcShippingDetail

		SELECT 
			@vcFromName=FromContact
			,@vcFromCompany=FromCompany
			,@vcFromPhone=FromPhone
			,@vcFromAddressLine1=FromAddress1
			,@vcFromAddressLine2=FromAddress2
			,@vcFromCity=FromCity
			,@vcFromState=CAST(FromState AS VARCHAR(18))
			,@vcFromZip=FromZip
			,@vcFromCountry=CAST(FromCountry AS VARCHAR(18))
			,@bitFromResidential=IsFromResidential
			,@vcToName=ToContact
			,@vcToCompany=ToCompany
			,@vcToPhone=ToPhone
			,@vcToAddressLine1=ToAddress1
			,@vcToAddressLine2=ToAddress2
			,@vcToCity=ToCity
			,@vcToState=CAST(ToState AS VARCHAR(18))
			,@vcToZip=ToZip
			,@vcToCountry=CAST(ToCountry AS VARCHAR(18))
			,@bitToResidential=IsToResidential
			,@tintPayorType=PayerType
			,@vcPayorAccountNo=AccountNo
			,@numPayorCountry=Country
			,@vcPayorZip=ZipCode
			,@IsCOD = IsCOD
			,@IsHomeDelivery = IsHomeDelivery
			,@IsInsideDelevery = IsInsideDelivery
			,@IsInsidePickup = IsInsidePickup
			,@IsSaturdayDelivery = IsSaturdayDelivery
			,@IsSaturdayPickup = IsSaturdayPickup
			,@IsAdditionalHandling = IsAdditionalHandling
			,@IsLargePackage=IsLargePackage
			,@numTotalInsuredValue = TotalInsuredValue
            ,@numTotalCustomsValue = TotalCustomsValue
			,@vcCODType = CODType
			,@numCODAmount = CODAmount
			,@vcDescription = [Description]
			,@tintSignatureType = CAST(ISNULL(SignatureType,'0') AS TINYINT)
		FROM 
			OPENXML(@hDocShippingDetail,'/NewDataSet/Table1',2)                                                                          
		WITH                       
		(
			FromContact VARCHAR(1000)
            ,FromPhone VARCHAR(100)
            ,FromCompany VARCHAR(1000)
            ,FromAddress1 VARCHAR(50)
            ,FromAddress2 VARCHAR(50)
            ,FromCountry NUMERIC(18,0)
            ,FromState NUMERIC(18,0)
            ,FromCity VARCHAR(50)
            ,FromZip VARCHAR(50)
            ,IsFromResidential BIT
            ,ToContact VARCHAR(1000)
            ,ToPhone VARCHAR(100)
            ,ToCompany VARCHAR(1000)
            ,ToAddress1 VARCHAR(50)
            ,ToAddress2 VARCHAR(50)
            ,ToCountry NUMERIC(18,0)
            ,ToState NUMERIC(18,0)
            ,ToCity VARCHAR(50)
            ,ToZip VARCHAR(50)
            ,IsToResidential BIT
            ,PayerType TINYINT
            ,ReferenceNo VARCHAR(200)
            ,AccountNo VARCHAR(20)
            ,ZipCode VARCHAR(50)
            ,Country NUMERIC(18,0)
            ,IsAdditionalHandling BIT
            ,IsCOD BIT
            ,IsHomeDelivery BIT
            ,IsInsideDelivery BIT
            ,IsInsidePickup BIT
            ,IsLargePackage BIT
            ,IsSaturdayDelivery BIT
            ,IsSaturdayPickup BIT
            ,SignatureType TINYINT
            ,[Description] VARCHAR(MAX)
            ,TotalInsuredValue NUMERIC(18,2)
            ,TotalCustomsValue NUMERIC(18,2)
            ,CODType VARCHAR(50)
            ,CODAmount NUMERIC(18,2)
		)


		EXEC sp_xml_removedocument @hDocShippingDetail
	END
	ELSE
	BEGIN
		-- GET FROM ADDRESS
		SELECT
			@vcFromName=vcName
			,@vcFromCompany=vcCompanyName
			,@vcFromPhone=vcPhone
			,@vcFromAddressLine1=vcStreet
			,@vcFromCity=vcCity
			,@vcFromState=CAST(vcState AS VARCHAR(18))
			,@vcFromZip=vcZipCode
			,@vcFromCountry=CAST(vcCountry AS VARCHAR(18))
			,@bitFromResidential=bitResidential
		FROM
			dbo.fn_GetShippingReportAddress(1,@numDomainId,@numOppID)

		-- GET TO ADDRESS
		SELECT
			@vcToName=vcName
			,@vcToCompany=vcCompanyName
			,@vcToPhone=vcPhone
			,@vcToAddressLine1=vcStreet
			,@vcToCity=vcCity
			,@vcToState=CAST(vcState AS VARCHAR(18))
			,@vcToZip=vcZipCode
			,@vcToCountry=CAST(vcCountry AS VARCHAR(18))
			,@bitToResidential=bitResidential
		FROM
			dbo.fn_GetShippingReportAddress(2,@numDomainId,@numOppID)

		SELECT
			@IsCOD = IsCOD
			,@IsHomeDelivery = IsHomeDelivery
			,@IsInsideDelevery = IsInsideDelevery
			,@IsInsidePickup = IsInsidePickup
			,@IsSaturdayDelivery = IsSaturdayDelivery
			,@IsSaturdayPickup = IsSaturdayPickup
			,@IsAdditionalHandling = IsAdditionalHandling
			,@IsLargePackage=IsLargePackage
			,@vcCODType = vcCODType
			,@vcDeliveryConfirmation = vcDeliveryConfirmation
			,@vcDescription = vcDescription
			,@tintSignatureType = CAST(ISNULL(vcSignatureType,'0') AS TINYINT)
		FROM
			OpportunityMaster
		INNER JOIN
			DivisionMaster
		ON
			OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
		LEFT JOIN
			DivisionMasterShippingConfiguration
		ON
			DivisionMasterShippingConfiguration.numDomainID=@numDomainId
			AND DivisionMaster.numDivisionID = DivisionMasterShippingConfiguration.numDivisionID
		WHERE
			OpportunityMaster.numDomainId = @numDomainId
			AND DivisionMaster.numDomainID=@numDomainId
			AND OpportunityMaster.numOppId = @numOppID

		DECLARE @bitUseBizdocAmount BIT

		SELECT @bitUseBizdocAmount=bitUseBizdocAmount,@numTotalCustomsValue=ISNULL(numTotalInsuredValue,0),@numTotalInsuredValue=ISNULL(numTotalCustomsValue,0) FROM Domain WHERE numDomainId=@numDomainId

		IF @bitUseBizdocAmount = 1
		BEGIN
			SELECT @numTotalCustomsValue=ISNULL(monDealAmount,0),@numTotalInsuredValue=ISNULL(monDealAmount,0),@numCODAmount=ISNULL(monDealAmount,0) FROM OpportunityBizDocs WHERE numOppBizDocsId=@numOppBizDocId 
		END
		ELSE
		BEGIN
			SELECT @numCODAmount=ISNULL(monDealAmount,0) FROM OpportunityBizDocs WHERE numOppBizDocsId=@numOppBizDocId 
		END
	END

	-- MAKE ENTRY IN ShippingReport TABLE
	INSERT INTO ShippingReport
	(
		numDomainId,numCreatedBy,dtCreateDate,numOppID,numOppBizDocId,numShippingCompany,vcValue2
		,vcFromName,vcFromCompany,vcFromPhone,vcFromAddressLine1,vcFromAddressLine2,vcFromCity,vcFromState,vcFromZip,vcFromCountry,bitFromResidential
		,vcToName,vcToCompany,vcToPhone,vcToAddressLine1,vcToAddressLine2,vcToCity,vcToState,vcToZip,vcToCountry,bitToResidential
		,IsCOD,IsDryIce,IsHoldSaturday,IsHomeDelivery,IsInsideDelevery,IsInsidePickup,IsReturnShipment,IsSaturdayDelivery,IsSaturdayPickup
		,numCODAmount,vcCODType,numTotalInsuredValue,IsAdditionalHandling,IsLargePackage,vcDeliveryConfirmation,vcDescription,numTotalCustomsValue,tintSignatureType
		,tintPayorType,vcPayorAccountNo,numPayorCountry,vcPayorZip
	)
	VALUES
	(
		@numDomainId,@numUserCntID,GETUTCDATE(),@numOppID,@numOppBizDocId,@numShippingCompany,CAST(@numShippingService AS VARCHAR)
		,@vcFromName,@vcFromCompany,@vcFromPhone,@vcFromAddressLine1,@vcFromAddressLine2,@vcFromCity,@vcFromState,@vcFromZip,@vcFromCountry,@bitFromResidential
		,@vcToName,@vcToCompany,@vcToPhone,@vcToAddressLine1,@vcToAddressLine2,@vcToCity,@vcToState,@vcToZip,@vcToCountry,@bitToResidential
		,@IsCOD,@IsDryIce,@IsHoldSaturday,@IsHomeDelivery,@IsInsideDelevery,@IsInsidePickup,@IsReturnShipment,@IsSaturdayDelivery,@IsSaturdayPickup
		,@numCODAmount,@vcCODType,@numTotalInsuredValue,@IsAdditionalHandling,@IsLargePackage,@vcDeliveryConfirmation,@vcDescription,@numTotalCustomsValue,@tintSignatureType
		,@tintPayorType,@vcPayorAccountNo,@numPayorCountry,@vcPayorZip
	)

	SET @numShippingReportID = SCOPE_IDENTITY()


	DECLARE @TableShippingBox TABLE
	(
		ID INT IDENTITY(1,1)
		,numBoxID INT
		,vcBoxName VARCHAR(300)
		,fltHeight FLOAT
		,fltWidth FLOAT
		,fltLength FLOAT
		,fltWeight FLOAT
		,numPackageType NUMERIC(18,0)
	)

	DECLARE @hDocBox as INTEGER
	EXEC sp_xml_preparedocument @hDocBox OUTPUT, @vcBoxes

	INSERT INTO @TableShippingBox
	(
		numBoxID
		,vcBoxName
		,fltHeight
		,fltWidth
		,fltLength
		,fltWeight
		,numPackageType
	)
	SELECT 
		ID 
		,ISNULL(Name,'')
		,ISNULL(Height,0)
		,ISNULL(Width,0)
		,ISNULL([Length],0)                             
		,ISNULL([Weight],0)
		,PackageType
	FROM 
		OPENXML(@hDocBox,'/NewDataSet/Table1',2)                                                                          
	WITH                       
	(
		ID NUMERIC(18,0)
		,Name VARCHAR(100)
		,Height FLOAT
		,Width FLOAT
		,[Length] FLOAT                              
		,[Weight] FLOAT
		,PackageType NUMERIC(18,0)
	)

	EXEC sp_xml_removedocument @hDocBox

	DECLARE @TableShippingBoxItems TABLE
	(
		numBoxID NUMERIC(18,0)
		,numOppItemID NUMERIC(18,0)
		,numUnitHour FLOAT
	)

	DECLARE @hDocBoxItem as INTEGER
	EXEC sp_xml_preparedocument @hDocBoxItem OUTPUT, @vcBoxItems

	INSERT INTO @TableShippingBoxItems
	(
		numBoxID
		,numOppItemID
		,numUnitHour
	)
	SELECT 
		BoxID 
		,OppItemID
		,ISNULL(Quantity,0)
	FROM 
		OPENXML(@hDocBoxItem,'/NewDataSet/Table1',2)                                                                          
	WITH                       
	(
		BoxID NUMERIC(18,0)
		,OppItemID NUMERIC(18,0)
		,Quantity FLOAT
	)

	EXEC sp_xml_removedocument @hDocBoxItem

	DECLARE @iBox INT = 1
	DECLARE @iBoxCount INT 
	DECLARE @numUserBoxID NUMERIC(18,0)
	DECLARE @numTempBoxID NUMERIC(18,0)
	SELECT @iBoxCount = COUNT(*) FROM @TableShippingBox

	WHILE @iBox <= @iBoxCount
	BEGIN
		SELECT 
			@numUserBoxID=numBoxID
		FROM 
			@TableShippingBox
		WHERE
			ID=@iBox

		INSERT INTO ShippingBox
		(
			vcBoxName
			,numShippingReportId
			,fltTotalWeight
			,fltHeight
			,fltWidth
			,fltLength
			,dtCreateDate
			,numCreatedBy
			,numPackageTypeID
			,numServiceTypeID
			,fltDimensionalWeight
			,numShipCompany
		)
		SELECT 
			ISNULL(vcBoxName,'')
			,@numShippingReportID
			,(CASE WHEN ISNULL(fltWeight,0) = 0 THEN CEILING(CAST((CASE WHEN ISNULL(fltHeight,0)=0 THEN 1 ELSE fltHeight END) * (CASE WHEN ISNULL(fltWidth,0)=0 THEN 1 ELSE fltWidth END) * (CASE WHEN ISNULL(fltLength,0)=0 THEN 1 ELSE fltLength END) AS FLOAT)/166) ELSE fltWeight END)
			,(CASE WHEN ISNULL(fltHeight,0)=0 THEN 1 ELSE fltHeight END)
			,(CASE WHEN ISNULL(fltWidth,0)=0 THEN 1 ELSE fltWidth END)
			,(CASE WHEN ISNULL(fltLength,0)=0 THEN 1 ELSE fltLength END)
			,GETUTCDATE()
			,@numUserCntID
			,(CASE 
				WHEN ISNULL(numPackageType,0) > 0 AND EXISTS (SELECT 
																	ID 
																FROM 
																	ShippingPackageType 
																WHERE 
																	numShippingCompanyID=ISNULL(@numShippingCompany,0)
																	AND numNsoftwarePackageTypeID=numPackageType
																	AND 1 = (CASE 
																				WHEN (numShippingCompanyID = 88 OR numShippingCompanyID=91) THEN 1 
																				ELSE (CASE WHEN ISNULL(bitEndicia,0) = ISNULL((SELECT vcShipFieldValue FROM ShippingFieldValues WHERE numDomainID=@numDomainID AND numListItemID=90 AND intShipFieldID=21),0) THEN 1 ELSE 0 END) 
																			END))
				THEN (SELECT TOP 1
						ID 
					FROM 
						ShippingPackageType 
					WHERE 
						numShippingCompanyID=ISNULL(@numShippingCompany,0)
						AND numNsoftwarePackageTypeID=numPackageType
						AND 1 = (CASE 
									WHEN (numShippingCompanyID = 88 OR numShippingCompanyID=91) THEN 1 
									ELSE (CASE WHEN ISNULL(bitEndicia,0) = ISNULL((SELECT vcShipFieldValue FROM ShippingFieldValues WHERE numDomainID=@numDomainID AND numListItemID=90 AND intShipFieldID=21),0) THEN 1 ELSE 0 END) 
								END))
				ELSE (CASE 
							WHEN @numShippingCompany = 88 THEN 19
							WHEN @numShippingCompany = 90 THEN (CASE WHEN ISNULL((SELECT TOP 1 vcShipFieldValue FROM ShippingFieldValues WHERE numDomainID=@numDomainId AND intShipFieldID=21 AND numListItemID=90),0) = '1' THEN 69 ELSE 50 END)
							WHEN @numShippingCompany = 91 THEN 7
						END) 
			END)
			,@numShippingService
			,(CASE WHEN ISNULL(fltWeight,0) = 0 THEN CEILING(CAST((CASE WHEN ISNULL(fltHeight,0)=0 THEN 1 ELSE fltHeight END) * (CASE WHEN ISNULL(fltWidth,0)=0 THEN 1 ELSE fltWidth END) * (CASE WHEN ISNULL(fltLength,0)=0 THEN 1 ELSE fltLength END) AS FLOAT)/166) ELSE fltWeight END)
			,@numShippingCompany
		FROM
			@TableShippingBox
		WHERE
			ID = @iBox

		SET @numTempBoxID = SCOPE_IDENTITY()

		-- MAKE ENTRY IN ShippingReportItems Table
		INSERT INTO ShippingReportItems
		(
			numShippingReportId
			,numItemCode
			,tintServiceType
			,monShippingRate
			,fltTotalWeight
			,intNoOfBox
			,fltHeight
			,fltWidth
			,fltLength
			,dtCreateDate
			,numCreatedBy
			,numBoxID
			,numOppBizDocItemID
			,intBoxQty
		)
		SELECT 
			@numShippingReportID
			,OpportunityBizDocItems.numItemCode
			,@numShippingService
			,0
			,(CASE 
				WHEN ISNULL(bitKitParent,0)=1 
						AND (CASE 
								WHEN EXISTS (SELECT ID.numItemDetailID FROM ItemDetails ID INNER JOIN Item IInner ON ID.numChildItemID=IInner.numItemCode WHERE ID.numItemKitID=I.numItemCode AND ISNULL(IInner.bitKitParent,0)=1)
								THEN 1
								ELSE 0
							END) = 0
				THEN 
					dbo.GetKitWeightBasedOnItemSelection(I.numItemCode,OI.vcChildKitSelectedItems)
				ELSE 
					ISNULL(fltWeight,0) 
			END)
			,1
			,ISNULL(I.fltHeight,0)
			,ISNULL(I.fltWidth,0)
			,ISNULL(I.fltLength,0)
			,GETUTCDATE()
			,@numUserCntID
			,@numTempBoxID
			,OpportunityBizDocItems.numOppBizDocItemID
			,TSBI.numUnitHour
		FROM
			OpportunityBizDocs
		INNER JOIN
			OpportunityBizDocItems
		ON
			OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
		INNER JOIN
			OpportunityItems OI
		ON
			OpportunityBizDocItems.numOppItemID=OI.numoppitemtCode
		INNER JOIN
			WareHouseItems WI
		ON
			OpportunityBizDocItems.numWarehouseItmsID=WI.numWareHouseItemID
		INNER JOIN
			Item I
		ON
			OpportunityBizDocItems.numItemCode=I.numItemCode
		INNER JOIN
			@TableShippingBoxItems TSBI
		ON
			OpportunityBizDocItems.numOppItemID = TSBI.numOppItemID
			AND OI.numoppitemtCode = TSBI.numOppItemID
		WHERE
			OpportunityBizDocs.numOppBizDocsId = @numOppBizDocId
			AND TSBI.numBoxID = @numUserBoxID

		UPDATE
			ShippingBox 
		SET 
			fltTotalWeight = ISNULL(fltTotalWeight,0) + ISNULL((SELECT SUM((ISNULL(fltTotalWeight,0) * ISNULL(intBoxQty,0))) FROM ShippingReportItems WHERE numShippingReportId=@numShippingReportID AND numBoxID=@numTempBoxID),0) 
		WHERE 
			numBoxID=@numTempBoxID

		SET @iBox = @iBox + 1
	END

	SELECT @numShippingReportID
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);

END CATCH
END
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_UpdateGoogleRefreshToken')
DROP PROCEDURE USP_UpdateGoogleRefreshToken
GO
CREATE PROCEDURE USP_UpdateGoogleRefreshToken
    @numUserCntId NUMERIC(18,0),
    @numDomainId NUMERIC(18,0),
	@vcGoogleRefreshToken as varchar(MAX)
AS 
    BEGIN

                UPDATE  ImapUserDetails
                SET     vcGoogleRefreshToken = @vcGoogleRefreshToken
                WHERE   numUserCntId = @numUserCntId
                        AND numDomainID = @numDomainId
	
    END
--Created by Anoop Jayaraj

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_UpdatePOItemsForFulfillment')
DROP PROCEDURE USP_UpdatePOItemsForFulfillment
GO
CREATE PROCEDURE  USP_UpdatePOItemsForFulfillment
    @numQtyReceived FLOAT,
    @numOppItemID NUMERIC(9),
    @vcError VARCHAR(200) = '' OUTPUT,
    @numUserCntID AS NUMERIC(9),
    @dtItemReceivedDate AS DATETIME,
	@numSelectedWarehouseItemID AS NUMERIC(18,0) = 0,
	@numVendorInvoiceBizDocID NUMERIC(18,0) = 0,
	@tintMode TINYINT = 0
AS 
BEGIN
--Transaction is invoked from ADO.net (Purchase Fulfillment) Do not add transaction code here.
-- BEGIN TRY
-- BEGIN TRANSACTION
	IF ISNULL(@numVendorInvoiceBizDocID,0) > 0
	BEGIN
		-- IF RECEIVED AGAINST VENDOR INVOICE MAKE SURE ENOUGH QTY IS PENDING TO RECIEVED AGAINST VEDOR INVOICE BECAUSE USER CAN RECEIVE QTY WITHOUT SELECTING VENDOR INVOCIE ALSO
		IF (SELECT
				COUNT(*)
			FROM
				OpportunityBizDocItems OBDI
			INNER JOIN
				OpportunityItems OI
			ON
				OBDI.numOppItemID=OI.numoppitemtCode
			WHERE
				numOppBizDocID=@numVendorInvoiceBizDocID
				AND OBDI.numOppItemID = @numOppItemID
				AND @numQtyReceived > (ISNULL(OI.numUnitHour,0) - ISNULL(OI.numUnitHourReceived,0))
			) > 0
		BEGIN
			RAISERROR('INVALID_VENDOR_INVOICE_RECEIVE_QTY',16,1)
			RETURN
		END
	END


	DECLARE @numDomain AS NUMERIC(18,0)
	          
	DECLARE @numWarehouseItemID AS NUMERIC
	DECLARE @numWLocationID AS NUMERIC(18,0)     
	DECLARE @numOldQtyReceived AS FLOAT       
	DECLARE @numNewQtyReceived AS FLOAT
	              
	DECLARE @bitStockTransfer BIT
	DECLARE @numToWarehouseItemID NUMERIC
	DECLARE @numOppId NUMERIC
	DECLARE @numUnits FLOAT
	DECLARE @monPrice AS DECIMAL(20,5) 
	DECLARE @numItemCode NUMERIC 
		
	SELECT  
		@numWarehouseItemID = ISNULL([numWarehouseItmsID], 0),
		@numWLocationID = ISNULL(WI.numWLocationID,0),
		@numOldQtyReceived = ISNULL([numUnitHourReceived], 0),
		@bitStockTransfer = ISNULL(OM.bitStockTransfer,0),
		@numToWarehouseItemID= ISNULL(numToWarehouseItemID,0),
		@numOppId=OM.numOppId,
		@numUnits=OI.numUnitHour,
		@monPrice=isnull(monPrice,0) * (CASE WHEN ISNULL(OM.fltExchangeRate,0)=0 THEN 1 ELSE OM.fltExchangeRate END),
		@numItemCode=OI.numItemCode,
		@numDomain = OM.[numDomainId]
	FROM 
		[OpportunityItems] OI 
	INNER JOIN 
		dbo.OpportunityMaster OM
	ON 
		OI.numOppId = OM.numOppId
	LEFT JOIN
		WareHouseItems WI
	ON
		OI.numWarehouseItmsID = WI.numWareHouseItemID
	WHERE 
		[numoppitemtCode] = @numOppItemID


	IF @bitStockTransfer = 1 --added by chintan
	BEGIN
	--ship item from FROM warehouse
		DECLARE @p3 VARCHAR(MAX) = ''
		
		EXEC USP_UpdateQtyShipped @numQtyReceived,@numOppItemID,@numUserCntID,@vcError=@p3 OUTPUT

		IF LEN(@p3)>0 
		BEGIN
			RAISERROR ( @p3,16, 1 )
			RETURN ;
		END
		
		-- Receive item from To Warehouse
		SET @numWarehouseItemID=@numToWarehouseItemID
    
	END  
    

	DECLARE @numTotalQuantityReceived FLOAT
	SET @numTotalQuantityReceived = @numQtyReceived + @numOldQtyReceived ;
	SET @numNewQtyReceived = @numQtyReceived;
	DECLARE @description AS VARCHAR(100)

	SET @description=CONCAT('PO Qty Received (Qty:',@numTotalQuantityReceived,')')
	
	IF @numTotalQuantityReceived > @numUnits
	BEGIN
		RAISERROR('RECEIVED_QTY_MUST_BE_LESS_THEN_OR_EQUAL_TO_ORDERED_QTY',16,1)
	END

	IF @numNewQtyReceived <= 0 
		RETURN 
  
	
	IF ISNULL(@bitStockTransfer,0) = 0
	BEGIN
		DECLARE @TotalOnHand AS FLOAT;SET @TotalOnHand=0  
		SELECT @TotalOnHand=(SUM(ISNULL(numOnHand,0)) + SUM(ISNULL(numAllocation,0))) FROM dbo.WareHouseItems WHERE numItemID=@numItemCode 
						
		--Updating the Average Cost
		DECLARE @monAvgCost AS DECIMAL(20,5) 
		SELECT  @monAvgCost = (CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(monAverageCost, 0) END) FROM Item WHERE   numitemcode = @numItemCode  
    
  		SET @monAvgCost = ((@TotalOnHand * @monAvgCost) + (@numNewQtyReceived * @monPrice)) / ( @TotalOnHand + @numNewQtyReceived )
                            
		UPDATE  
			item
		SET 
			monAverageCost = (CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE @monAvgCost END)
		WHERE 
			numItemCode = @numItemCode
    END	

	IF ISNULL(@numSelectedWarehouseItemID,0) > 0 AND ISNULL(@numSelectedWarehouseItemID,0) <> @numWareHouseItemID		
	BEGIN
		DECLARE @vcFromLocation AS VARCHAR(300)
		DECLARE @vcToLocation AS VARCHAR(300)
		SELECT @vcFromLocation=ISNULL(WL.vcLocation,'') FROM WareHouseItems WI LEFT JOIN WarehouseLocation WL ON WI.numWLocationID=WL.numWLocationID WHERE WI.numWareHouseItemID=@numWareHouseItemID
		SELECT @vcToLocation=ISNULL(WL.vcLocation,'') FROM WareHouseItems WI LEFT JOIN WarehouseLocation WL ON WI.numWLocationID=WL.numWLocationID WHERE WI.numWareHouseItemID=@numSelectedWarehouseItemID

		UPDATE
			WareHouseItems
		SET 
			numOnOrder = ISNULL(numOnOrder,0) - @numNewQtyReceived,
			dtModified = GETDATE()
		WHERE
			numWareHouseItemID=@numWareHouseItemID
		
		SET @description = CONCAT('PO Qty Received To Internal Location ',@vcToLocation,' (Qty:',@numNewQtyReceived,')')

		EXEC dbo.USP_ManageWareHouseItems_Tracking
			@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
			@numReferenceID = @numOppId, --  numeric(9, 0)
			@tintRefType = 4, --  tinyint
			@vcDescription = @description,
			@numModifiedBy = @numUserCntID,
			@dtRecordDate =  @dtItemReceivedDate,
			@numDomainID = @numDomain

		-- INCREASE THE OnHand Of Destination Location
		UPDATE
			WareHouseItems
		SET
			numBackOrder = (CASE WHEN numBackOrder >= @numNewQtyReceived THEN ISNULL(numBackOrder,0) - @numNewQtyReceived ELSE 0 END),         
			numAllocation = (CASE WHEN numBackOrder >= @numNewQtyReceived THEN ISNULL(numAllocation,0) + @numNewQtyReceived ELSE ISNULL(numAllocation,0) + ISNULL(numBackOrder,0) END),
			numOnHand = (CASE WHEN numBackOrder >= @numNewQtyReceived THEN numOnHand ELSE ISNULL(numOnHand,0) + @numNewQtyReceived - ISNULL(numBackOrder,0) END),
			dtModified = GETDATE()
		WHERE
			numWareHouseItemID=@numSelectedWarehouseItemID

		SET @description = CONCAT('PO Qty Received From Internal Location ',@vcFromLocation,' (Qty:',@numNewQtyReceived,')')

		EXEC dbo.USP_ManageWareHouseItems_Tracking
			@numWareHouseItemID = @numSelectedWarehouseItemID, --  numeric(9, 0)
			@numReferenceID = @numOppId, --  numeric(9, 0)
			@tintRefType = 4, --  tinyint
			@vcDescription = @description,
			@numModifiedBy = @numUserCntID,
			@dtRecordDate =  @dtItemReceivedDate,
			@numDomainID = @numDomain

		INSERT INTO OpportunityItemsReceievedLocation
		(
			numDomainID,
			numOppID,
			numOppItemID,
			numWarehouseItemID,
			numUnitReceieved
		)
		VALUES
		(
			@numDomain,
			@numOppId,
			@numOppItemID,
			@numSelectedWarehouseItemID,
			@numNewQtyReceived
		)
	END
	ELSE IF ISNULL(@numWareHouseItemID,0) > 0
	BEGIN
		DECLARE @onHand AS FLOAT
		DECLARE @onAllocation AS FLOAT    
		DECLARE @onOrder AS FLOAT            
		DECLARE @onBackOrder AS FLOAT

		SELECT  
			@onHand = ISNULL(numOnHand, 0),
			@onAllocation = ISNULL(numAllocation, 0),
			@onOrder = ISNULL(numOnOrder, 0),
			@onBackOrder = ISNULL(numBackOrder, 0)
		FROM 
			WareHouseItems
		WHERE 
			numWareHouseItemID = @numWareHouseItemID

		IF @onOrder >= @numNewQtyReceived 
		BEGIN
			PRINT '1 case'

			SET @onOrder = @onOrder - @numNewQtyReceived             
			
			IF @onBackOrder >= @numNewQtyReceived 
			BEGIN            
				SET @onBackOrder = @onBackOrder - @numNewQtyReceived             
				SET @onAllocation = @onAllocation + @numNewQtyReceived             
			END            
			ELSE 
			BEGIN            
				SET @onAllocation = @onAllocation + @onBackOrder            
				SET @numNewQtyReceived = @numNewQtyReceived - @onBackOrder            
				SET @onBackOrder = 0            
				SET @onHand = @onHand + @numNewQtyReceived             
			END         
		END            
		ELSE IF @onOrder < @numNewQtyReceived 
		BEGIN
			PRINT '2 case'        
			SET @onHand = @onHand + @onOrder
			SET @onOrder = @numNewQtyReceived - @onOrder
		END   

		SELECT 
			@onHand onHand,
			@onAllocation onAllocation,
			@onBackOrder onBackOrder,
			@onOrder onOrder
                
		UPDATE 
			WareHouseItems
		SET     
			numOnHand = @onHand,
			numAllocation = @onAllocation,
			numBackOrder = @onBackOrder,
			numOnOrder = @onOrder,
			dtModified = GETDATE() 
		WHERE 
			numWareHouseItemID = @numWareHouseItemID
    
		EXEC dbo.USP_ManageWareHouseItems_Tracking
			@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
			@numReferenceID = @numOppId, --  numeric(9, 0)
			@tintRefType = 4, --  tinyint
			@vcDescription = @description, --  varchar(100)
			@numModifiedBy = @numUserCntID,
			@dtRecordDate =  @dtItemReceivedDate,
			@numDomainID = @numDomain
	END
 
	UPDATE  
		[OpportunityItems]
	SET 
		numUnitHourReceived = @numTotalQuantityReceived
		,numQtyReceived = (CASE WHEN ISNULL(@tintMode,0)=1 THEN ISNULL(numQtyReceived,0) + ISNULL(@numQtyReceived,0) ELSE ISNULL(numQtyReceived,0) END)
	WHERE
		[numoppitemtCode] = @numOppItemID


	IF ISNULL(@numVendorInvoiceBizDocID,0) > 0
	BEGIN
		UPDATE  
			OpportunityBizDocItems
		SET 
			numVendorInvoiceUnitReceived = ISNULL(numVendorInvoiceUnitReceived,0) +  ISNULL(@numQtyReceived,0)
		WHERE
			numOppBizDocID=@numVendorInvoiceBizDocID
			AND numOppItemID = @numOppItemID
	END

	UPDATE dbo.OpportunityMaster SET dtItemReceivedDate=@dtItemReceivedDate WHERE numOppId=@numOppId
END	
set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go 
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetWebReferringInfo')
DROP PROCEDURE USP_GetWebReferringInfo
GO
CREATE PROCEDURE [dbo].[USP_GetWebReferringInfo] 
	@numDomainID AS NUMERIC(9) = 0
AS  
BEGIN
	SELECT *
	FROM TrackingVisitorsHDR 
	WHERE numDomainID = @numDomainID AND ISNULL(numListItemID,0) > 0
END
