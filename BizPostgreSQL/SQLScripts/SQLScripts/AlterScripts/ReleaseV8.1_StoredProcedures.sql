/******************************************************************
Project: Release 8.1 Date: 02.OCTOBER.2017
Comments: STORE PROCEDURES
*******************************************************************/

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_UOMConversion')
DROP FUNCTION fn_UOMConversion
GO
CREATE FUNCTION [dbo].[fn_UOMConversion]
(
	@numFromUnit NUMERIC,
	@numItemCode NUMERIC,
	@numDomainID NUMERIC,
	@numToUnit NUMERIC=null
)
RETURNS FLOAT
AS
BEGIN
	DECLARE @bitEnableItemLevelUOM AS BIT = 0
	DECLARE @MultiplicationFactor FLOAT

	SELECT @bitEnableItemLevelUOM=ISNULL(bitEnableItemLevelUOM,0) FROM Domain WHERE numDomainID = @numDomainID
 
	IF ISNULL(@numToUnit,0) = 0
		SELECT @numToUnit=numBaseUnit FROM Item WHERE numDomainID=@numDomainID AND numItemCode=@numItemCode

	IF EXISTS (SELECT @numToUnit WHERE @numToUnit IS null)
		RETURN 1
 
	IF(@numFromUnit=@numToUnit)
		RETURN 1

	IF @bitEnableItemLevelUOM = 0
	BEGIN
		;with recursiveTable(FROMUnit,LevelNum,numUOM1,decConv1,numUOM2,decConv2,multiplicationFactor) as
		(
			select  numUOM1,0 as LevelNum
					,ct.numUOM1,ct.decConv1,ct.numUOM2,ct.decConv2
					,CAST((ct.decConv2 / CAST(ct.decConv1 AS DECIMAL(18,4))) AS DECIMAL(18,4)) as multiplicationFactor
			from UOMConversion ct
			where   ct.numUOM1 = @numFromUnit AND ct.numDomainID=@numDomainID

			union all

			select  FROMUnit,LevelNum + 1
					,rt.numUOM2,rt.decConv2,ct.numUOM2,ct.decConv2
					,CAST((rt.multiplicationFactor *(ct.decConv2 / CAST(ct.decConv1 AS DECIMAL(18,4)))) AS DECIMAL(18,4))
			from UOMConversion ct
			inner join recursiveTable rt on rt.numUOM2 = ct.numUOM1 WHERE ct.numDomainID=@numDomainID
		)

		select top 1 @MultiplicationFactor=multiplicationFactor from recursiveTable where FROMUnit = @numFromUnit and numUOM2 = @numToUnit

		IF EXISTS (SELECT @MultiplicationFactor WHERE @MultiplicationFactor IS NOT null)
		BEGIN
			RETURN @MultiplicationFactor	
		END 
		ELSE
		BEGIN
			;with recursiveTable(FROMUnit,LevelNum,numUOM1,decConv1,numUOM2,decConv2,multiplicationFactor) as
			(
				select  numUOM2,0 as LevelNum
						,ct.numUOM1,ct.decConv1,ct.numUOM2,ct.decConv2
						,CONVERT(float,ct.decConv1 / ct.decConv2) as multiplicationFactor
				from UOMConversion ct
				where   ct.numUOM2 = @numFromUnit AND ct.numDomainID=@numDomainID

				union all

				select  FROMUnit,LevelNum + 1
						,ct.numUOM1,ct.decConv1,ct.numUOM2,ct.decConv2
						,CONVERT(float,rt.multiplicationFactor * CONVERT(float,(ct.decConv1 / ct.decConv2)))
				from UOMConversion ct
				inner join recursiveTable rt on rt.numUOM1 = ct.numUOM2 WHERE ct.numDomainID=@numDomainID
			)

			select top 1 @MultiplicationFactor=multiplicationFactor from recursiveTable where FROMUnit = @numFromUnit and numUOM1 = @numToUnit

			IF EXISTS(SELECT @MultiplicationFactor WHERE @MultiplicationFactor IS NOT null)
			BEGIN
				RETURN @MultiplicationFactor	
			END 
		END
	END
	ELSE
	BEGIN
		;WITH CTE(FromUnit,LevelNum,numSourceUOM,numTargetUOM,numTargetUnit) AS
		(
			SELECT  
				IUOM.numSourceUOM,
				0 AS LevelNum,
				IUOM.numSourceUOM,
				IUOM.numTargetUOM,
				IUOM.numTargetUnit
			FROM 
				ItemUOMConversion IUOM
			WHERE   
				IUOM.numDomainID=@numDomainID
				AND IUOM.numItemCode = @numItemCode
				AND IUOM.numSourceUOM = @numFromUnit
			UNION ALL
			SELECT  
				cte.FROMUnit,
				LevelNum + 1,
				IUOM.numSourceUOM,
				IUOM.numTargetUOM,
				(IUOM.numTargetUnit * cte.numTargetUnit)
			FROM 
				ItemUOMConversion IUOM
			INNER JOIN 
				CTE cte 
			ON 
				IUOM.numSourceUOM = cte.numTargetUOM 
			WHERE 
				IUOM.numDomainID=@numDomainID
				AND IUOM.numItemCode = @numItemCode
		)

		SELECT TOP 1 @MultiplicationFactor=numTargetUnit FROM CTE WHERE FROMUnit = @numFromUnit AND numTargetUOM = @numToUnit

		IF EXISTS (SELECT @MultiplicationFactor WHERE @MultiplicationFactor IS NOT NULL)
		BEGIN
			RETURN @MultiplicationFactor	
		END
		ELSE
		BEGIN
			;WITH CTE(FromUnit,LevelNum,numSourceUOM,numTargetUOM,numTargetUnit) AS
			(
				SELECT  
					IUOM.numTargetUOM,
					0 AS LevelNum,
					IUOM.numSourceUOM,
					IUOM.numTargetUOM,
					(1 / IUOM.numTargetUnit)
				FROM 
					ItemUOMConversion IUOM
				WHERE   
					IUOM.numDomainID=@numDomainID
					AND IUOM.numItemCode = @numItemCode
					AND IUOM.numTargetUOM = @numFromUnit
				UNION ALL
				SELECT  
					cte.FROMUnit,
					LevelNum + 1,
					IUOM.numSourceUOM,
					IUOM.numTargetUOM,
					((1 / IUOM.numTargetUnit) * cte.numTargetUnit)
				FROM 
					ItemUOMConversion IUOM
				INNER JOIN 
					CTE cte 
				ON 
					IUOM.numTargetUOM = cte.numSourceUOM 
				WHERE 
					IUOM.numDomainID=@numDomainID
					AND IUOM.numItemCode = @numItemCode
			)

			select top 1 @MultiplicationFactor=numTargetUnit from cte where FROMUnit = @numFromUnit and numSourceUOM = @numToUnit

			IF EXISTS(SELECT @MultiplicationFactor WHERE @MultiplicationFactor IS NOT null)
			BEGIN
				RETURN @MultiplicationFactor	
			END 
		END
	END

RETURN 1
END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='TF'AND NAME ='GetCommissionPaidCreditMemoOrRefund')
DROP FUNCTION GetCommissionPaidCreditMemoOrRefund
GO
CREATE FUNCTION [dbo].[GetCommissionPaidCreditMemoOrRefund]
(
	 @numDomainID AS NUMERIC(18,0),
	 @ClientTimeZoneOffset INT,
	 @dtStartDate DATE,
	 @dtEndDate DATE
)    
RETURNS @TempCommissionPaidCreditMemoOrRefund TABLE
(
	numUserCntID NUMERIC(18,0),
	numReturnHeaderID NUMERIC(18,0),
	tintReturnType TINYINT,
	numReturnItemID NUMERIC(18,0),
	monCommission MONEY,
	numComRuleID NUMERIC(18,0),
	tintComType TINYINT,
	tintComBasedOn TINYINT,
	decCommission FLOAT,
	tintAssignTo TINYINT
)    
AS    
BEGIN   
	--GET COMMISSION RULE OF DOMAIN
	DECLARE @TempCommissionRule TABLE
	(
		ID INT IDENTITY(1,1),
		numComRuleID NUMERIC(18,0),
		tintComBasedOn TINYINT,
		tintComType TINYINT,
		tintComAppliesTo TINYINT,
		tintComOrgType TINYINT,
		tintAssignTo TINYINT
	)

	INSERT INTO
		@TempCommissionRule
	SELECT
		CR.numComRuleID,
		CR.tintComBasedOn,
		CR.tintComType,
		CR.tintComAppliesTo,
		CR.tintComOrgType,
		CR.tintAssignTo
	FROM
		CommissionRules CR
	LEFT JOIN
		PriceBookPriorities PBP
	ON	
		CR.tintComAppliesTo = PBP.Step2Value 
		AND CR.tintComOrgType = PBP.Step3Value
	WHERE
		CR.numDomainID = @numDomainID
		AND ISNULL(PBP.[Priority],0) > 0
	ORDER BY
		ISNULL(PBP.[Priority],0) ASC

	--GET COMMISSION GLOBAL SETTINGS
	DECLARE @numDiscountServiceItemID NUMERIC(18,0)
	DECLARE @numShippingServiceItemID NUMERIC(18,0)
	DECLARE @tintCommissionType TINYINT
	DECLARE @bitIncludeTaxAndShippingInCommission BIT

	SELECT 
		@numDiscountServiceItemID=numDiscountServiceItemID, 
		@numShippingServiceItemID = numShippingServiceItemID, 
		@tintCommissionType=tintCommissionType, 
		@bitIncludeTaxAndShippingInCommission=bitIncludeTaxAndShippingInCommission 
	FROM 
		Domain 
	WHERE 
		numDomainID=@numDomainID

	DECLARE @TABLEPAID TABLE
	(
		ID INT IDENTITY(1,1),
		numDivisionID NUMERIC(18,0),
		numRelationship NUMERIC(18,0),
		numProfile NUMERIC(18,0),
		numAssignedTo NUMERIC(18,0),
		numRecordOwner NUMERIC(18,0),
		numItemCode NUMERIC(18,0),
		numItemClassification NUMERIC(18,0),
		numReturnHeaderID NUMERIC(18,0),
		tintReturnType TINYINT,
		numReturnItemID NUMERIC(18,0),
		numUnitHour FLOAT,
		monTotAmount MONEY,
		monVendorCost MONEY,
		monAvgCost MONEY,
		numPartner NUMERIC(18,0)
	)

	-- GET FULLY PAID INVOICES WITHIN PAY PERIOD
	INSERT INTO 
		@TABLEPAID
	SELECT 
		DivisionMaster.numDivisionID,
		ISNULL(CompanyInfo.numCompanyType,0),
		ISNULL(CompanyInfo.vcProfile,0),
		(CASE WHEN ISNULL(OMSalesReturn.numAssignedTo,0) > 0 THEN OMSalesReturn.numAssignedTo ELSE OMCreditMemo.numAssignedTo END),
		(CASE WHEN ISNULL(OMSalesReturn.numRecOwner,0) > 0 THEN OMSalesReturn.numRecOwner ELSE OMCreditMemo.numRecOwner END),
		Item.numItemCode,
		Item.numItemClassification,
		ReturnHeader.numReturnHeaderID,
		ReturnHeader.tintReturnType,
		(CASE WHEN tintReturnType IN (1,2) THEN ReturnItems.numReturnItemID ELSE 0 END),
		(CASE WHEN tintReturnType IN (1,2) THEN ISNULL(ReturnItems.numUnitHour,0) ELSE 0 END),
		(CASE WHEN tintReturnType IN (1,2) THEN ISNULL(ReturnItems.monTotAmount,0) ELSE ReturnHeader.monBizDocAmount END),
		(CASE WHEN tintReturnType IN (1,2) THEN (ISNULL(ReturnItems.monVendorCost,ISNULL((SELECT Vendor.monCost FROM Item INNER JOIN Vendor ON Item.numVendorID=Vendor.numVendorID AND Vendor.numItemCode=Item.numItemCode WHERE Item.numItemCode=ReturnItems.numItemCode),0)) * ISNULL(ReturnItems.numUnitHour,0)) ELSE 0 END),
		(CASE WHEN tintReturnType IN (1,2) THEN (ISNULL(ReturnItems.monAverageCost,Item.monAverageCost) * ISNULL(ReturnItems.numUnitHour,0)) ELSE 0 END),
		(CASE WHEN ISNULL(OMSalesReturn.numPartner,0) > 0 THEN OMSalesReturn.numPartner ELSE OMCreditMemo.numPartner END)
	FROM 
		ReturnHeader
	INNER JOIN
		DivisionMaster 
	ON
		ReturnHeader.numDivisionID = DivisionMaster.numDivisionID
	INNER JOIN
		CompanyInfo
	ON
		DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
	LEFT JOIN
		OpportunityMaster OMSalesReturn
	ON
		OMSalesReturn.numOppId = ReturnHeader.numOppId
	LEFT JOIN
		OpportunityMaster OMCreditMemo
	ON
		OMCreditMemo.numOppId = ReturnHeader.numReferenceSalesOrder
	LEFT JOIN
		ReturnItems 
	ON
		ReturnHeader.numReturnHeaderID=ReturnItems.numReturnHeaderID
	LEFT JOIN
		OpportunityItems
	ON
		ReturnItems.numOppItemID=OpportunityItems.numoppitemtCode
	LEFT JOIN
		Item
	ON
		ReturnItems.numItemCode = Item.numItemCode
	OUTER APPLY
	(
		SELECT 
			MAX(datEntry_Date) dtDepositDate
		FROM 
			General_Journal_Header 
		WHERE 
			numDomainId=@numDomainId
			AND numReturnID=ReturnHeader.numReturnHeaderID
	) TempDepositMaster
	WHERE
		ReturnHeader.numDomainId = @numDomainID 
		AND ReturnHeader.numReturnStatus = 303
		AND (ISNULL(ReturnHeader.numOppId,0) > 0 OR ISNULL(ReturnHeader.numReferenceSalesOrder,0) > 0)
		AND (TempDepositMaster.dtDepositDate IS NOT NULL AND (CAST(DATEADD(MINUTE,-@ClientTimeZoneOffset,TempDepositMaster.dtDepositDate) AS DATE) BETWEEN @dtStartDate AND @dtEndDate))
		AND (ReturnItems.numItemCode IS NULL OR ReturnItems.numItemCode NOT IN ( @numDiscountServiceItemID))  /*DO NOT INCLUDE DISCOUNT ITEM*/
		AND 1 = (
					CASE 
						WHEN ReturnItems.numItemCode = @numShippingServiceItemID 
						THEN 
							CASE 
								WHEN ISNULL(@bitIncludeTaxAndShippingInCommission,0) = 1
								THEN	
									1
								ELSE	
									0
							END
						ELSE 
							1 
					END
				)/*CONDITION TO INCLUDE/EXCLUDE INCLUDE SHIPPING ITEM */

		-- LOOP ALL COMMISSION RULES
		DECLARE @i INT = 1
		DECLARE @COUNT INT = 0

		SELECT @COUNT=COUNT(*) FROM @TempCommissionRule

		DECLARE @TEMPITEM TABLE
		(
			ID INT,
			UniqueID INT,
			numUserCntID NUMERIC(18,0),
			numReturnHeaderID NUMERIC(18,0),
			tintReturnType TINYINT,
			numReturnItemID NUMERIC(18,0),
			numItemCode NUMERIC(18,0),
			numUnitHour FLOAT,
			monTotAmount MONEY,
			monVendorCost MONEY,
			monAvgCost MONEY
		)

		WHILE @i <= @COUNT
		BEGIN
			DECLARE @numComRuleID NUMERIC(18,0)
			DECLARE @tintComBasedOn TINYINT
			DECLARE @tintComAppliesTo TINYINT
			DECLARE @tintComOrgType TINYINT
			DECLARE @tintComType TINYINT
			DECLARE @tintAssignTo TINYINT
			DECLARE @decCommission MONEY
			DECLARE @numTotalAmount AS MONEY
			DECLARE @numTotalUnit AS FLOAT
			DECLARE @RowCount AS INT

			--CLEAR PREVIOUS VALUES FROM TEMP TABLE
			DELETE FROM @TEMPITEM

			--FETCH COMMISSION RULE
			SELECT @numComRuleID=numComRuleID,@tintComBasedOn=tintComBasedOn,@tintComType=tintComType,@tintComAppliesTo=tintComAppliesTo,@tintComOrgType=tintComOrgType,@tintAssignTo=tintAssignTo FROM @TempCommissionRule WHERE ID=@i

			--FIND ALL BIZ DOC ITEMS WHERE COMMISSION IS NOT PAID AND CURRENT RULE ITEMS, ORGANIZATION AND ASSINGED TO FILTER CAN BE APPLIED
			INSERT INTO
				@TEMPITEM
			SELECT
				ROW_NUMBER() OVER (ORDER BY ID),
				ID,
				(CASE @tintAssignTo WHEN 1 THEN numAssignedTo WHEN 2 THEN numRecordOwner ELSE numPartner END),
				numReturnHeaderID,
				tintReturnType,
				numReturnItemID,
				numItemCode,
				numUnitHour,
				monTotAmount,
				monVendorCost,
				monAvgCost
			FROM
				@TABLEPAID
			WHERE
				1 = (CASE 
					WHEN tintReturnType = 3 OR tintReturnType = 4 THEN 1
					ELSE
						(CASE @tintComAppliesTo
							--SPECIFIC ITEMS
							WHEN 1 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleItems WHERE numComRuleID = @numComRuleID AND ISNULL(numValue,0) = numItemCode) > 0 THEN 1 ELSE 0 END
							-- ITEM WITH SPECIFIC CLASSIFICATIONS
							WHEN 2 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleItems WHERE numComRuleID = @numComRuleID AND ISNULL(numValue,0) = numItemClassification) > 0 THEN 1 ELSE 0 END
							-- ALL ITEMS
							ELSE 1
						END)
					END)
				AND 1 = (CASE @tintComOrgType
							-- SPECIFIC ORGANIZATION
							WHEN 1 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleOrganization WHERE numComRuleID = @numComRuleID AND ISNULL(numValue,0) = numDivisionID) > 0 THEN 1 ELSE 0 END
							-- ORGANIZATION WITH SPECIFIC RELATIONSHIP AND PROFILE
							WHEN 2 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleOrganization WHERE numComRuleID = @numComRuleID AND ISNULL(numValue,0) = numRelationship AND ISNULL(numProfile,0) = numProfile) > 0 THEN 1 ELSE 0 END
							-- ALL ORGANIZATIONS
							ELSE 1
						END)
				AND 1 = (CASE @tintAssignTo 
							-- ORDER ASSIGNED TO
							WHEN 1 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleContacts WHERE numComRuleID = @numComRuleID AND numValue=numAssignedTo) > 0 THEN 1 ELSE 0 END
							-- ORDER OWNER
							WHEN 2 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleContacts WHERE numComRuleID = @numComRuleID AND numValue=numRecordOwner) > 0 THEN 1 ELSE 0 END
							-- Order Partner
							WHEN 3 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleContacts WHERE numComRuleID = @numComRuleID AND numValue=numPartner) > 0 THEN 1 ELSE 0 END
							-- NO OPTION SELECTED IN RULE
							ELSE 0
						END)

			-- IF BIZDOC ITEMS ARE FOUND AFTER FILTER
			IF (SELECT COUNT(*) FROM @TEMPITEM) > 0
			BEGIN
				--CHECK COMMISSION BASED ON OPTION OF RULE
				--BASED ON AMOUNT SOLD
				IF @tintComBasedOn = 1
				BEGIN
					SELECT @numTotalAmount = SUM(monTotAmount) FROM @TEMPITEM

					IF (SELECT COUNT(*) FROM CommissionRuleDtl WHERE numComRuleID=@numComRuleID AND (@numTotalAmount BETWEEN intFrom AND intTo)) > 0
					BEGIN
						SELECT TOP 1 @decCommission=ISNULL(decCommission,0) FROM CommissionRuleDtl WHERE numComRuleID=@numComRuleID AND (@numTotalAmount BETWEEN intFrom AND intTo)
					END
				END
				--BASED ON UNITS SOLD
				ELSE IF @tintComBasedOn = 2
				BEGIN
					SELECT @numTotalUnit = SUM(numUnitHour) FROM @TEMPITEM

					IF (SELECT COUNT(*) FROM CommissionRuleDtl WHERE numComRuleID=@numComRuleID AND (@numTotalUnit BETWEEN intFrom AND intTo)) > 0
					BEGIN
						SELECT TOP 1 @decCommission=ISNULL(decCommission,0) FROM CommissionRuleDtl WHERE numComRuleID=@numComRuleID AND (@numTotalUnit BETWEEN intFrom AND intTo)
					END
				END

				--IF COMMISSION AMOUNT FOUND AFTER CHECKING COMMISSION FROM AND TO RANGE IN ABOVE STEP
				IF ISNULL(@decCommission,0) > 0
				BEGIN
					SELECT @RowCount=COUNT(*) FROM @TEMPITEM

					--CALCULATE COMMISSION AND STORE IN TEMPARORY TABLE
					INSERT INTO @TempCommissionPaidCreditMemoOrRefund 
					(
						numUserCntID,
						numReturnHeaderID,
						tintReturnType,
						numReturnItemID,
						monCommission,
						numComRuleID,
						tintComType,
						tintComBasedOn,
						decCommission,
						tintAssignTo
					)
					SELECT
						numUserCntID,
						numReturnHeaderID,
						tintReturnType,
						numReturnItemID,
						CASE @tintComType 
							WHEN 1 --PERCENT
								THEN 
									CASE tintReturnType
										WHEN 3 THEN monTotAmount * (@decCommission / CAST(100 AS FLOAT))
										WHEN 4 THEN monTotAmount * (@decCommission / CAST(100 AS FLOAT))
										ELSE
											CASE @tintCommissionType
												--TOTAL AMOUNT PAID
												WHEN 1 THEN monTotAmount * (@decCommission / CAST(100 AS FLOAT))
												--ITEM GROSS PROFIT (VENDOR COST)
												WHEN 2 THEN (ISNULL(monTotAmount,0) - ISNULL(monVendorCost,0)) * (@decCommission / CAST(100 AS FLOAT))
												--ITEM GROSS PROFIT (AVERAGE COST)
												WHEN 3 THEN(ISNULL(monTotAmount,0) - ISNULL(monAvgCost,0)) * (@decCommission / CAST(100 AS FLOAT))
											END
									END
							ELSE  --FLAT: COMMISSION AMOUNT IS EQUALLY DISTRIBUTED AMONG FOUND ENTERIES
								(@decCommission / @RowCount)
						END,
						@numComRuleID,
						@tintComType,
						@tintComBasedOn,
						@decCommission,
						@tintAssignTo
					FROM 
						@TEMPITEM AS T1
					WHERE
						1 = (CASE tintReturnType
								WHEN 3 THEN 1
								WHEN 4 THEN 1
								ELSE
									(CASE @tintCommissionType 
										--TOTAL PAID INVOICE
										WHEN 1 THEN CASE WHEN (ISNULL(monTotAmount,0) > 0) THEN 1 ELSE 0 END 
										-- ITEM GROSS PROFIT (VENDOR COST)
										WHEN 2 THEN CASE WHEN ((ISNULL(monTotAmount,0) - ISNULL(monVendorCost,0)) > 0) THEN 1 ELSE 0 END 
										-- ITEM GROSS PROFIT (AVERAGE COST)
										WHEN 3 THEN CASE WHEN ((ISNULL(monTotAmount,0) - ISNULL(monAvgCost,0)) > 0) THEN 1 ELSE 0 END
										ELSE
											0 
									END)
							END)
						AND (
								SELECT 
									COUNT(*) 
								FROM 
									@TempCommissionPaidCreditMemoOrRefund 
								WHERE 
									numUserCntID=T1.numUserCntID 
									AND numReturnHeaderID=T1.numReturnHeaderID 
									AND tintReturnType = T1.tintReturnType 
									AND numReturnItemID = T1.numReturnItemID 
							) = 0
				END
			END

			SET @i = @i + 1

			SET @numComRuleID = 0
			SET @tintComBasedOn = 0
			SET @tintComAppliesTo = 0
			SET @tintComOrgType = 0
			SET @tintComType = 0
			SET @tintAssignTo = 0
			SET @decCommission = 0
			SET @numTotalAmount = 0
			SET @numTotalUnit = 0
			SET @RowCount = 0
		END
	
	RETURN
END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='TF'AND NAME ='GetCommissionUnPaidCreditMemoOrRefund')
DROP FUNCTION GetCommissionUnPaidCreditMemoOrRefund
GO
CREATE FUNCTION [dbo].[GetCommissionUnPaidCreditMemoOrRefund]
(
	 @numDomainID AS NUMERIC(18,0),
	 @ClientTimeZoneOffset INT,
	 @dtStartDate DATE,
	 @dtEndDate DATE
)    
RETURNS @TempCommissionUnPaidCreditMemoOrRefund TABLE
(
	numUserCntID NUMERIC(18,0),
	numReturnHeaderID NUMERIC(18,0),
	tintReturnType TINYINT,
	numReturnItemID NUMERIC(18,0),
	monCommission MONEY,
	numComRuleID NUMERIC(18,0),
	tintComType TINYINT,
	tintComBasedOn TINYINT,
	decCommission FLOAT,
	tintAssignTo TINYINT
)    
AS    
BEGIN   
	DECLARE @monCommissionAmount AS MONEY

	--GET COMMISSION RULE OF DOMAIN
	DECLARE @TempCommissionRule TABLE
	(
		ID INT IDENTITY(1,1),
		numComRuleID NUMERIC(18,0),
		tintComBasedOn TINYINT,
		tintComType TINYINT,
		tintComAppliesTo TINYINT,
		tintComOrgType TINYINT,
		tintAssignTo TINYINT
	)

	INSERT INTO
		@TempCommissionRule
	SELECT
		CR.numComRuleID,
		CR.tintComBasedOn,
		CR.tintComType,
		CR.tintComAppliesTo,
		CR.tintComOrgType,
		CR.tintAssignTo
	FROM
		CommissionRules CR
	LEFT JOIN
		PriceBookPriorities PBP
	ON	
		CR.tintComAppliesTo = PBP.Step2Value 
		AND CR.tintComOrgType = PBP.Step3Value
	WHERE
		CR.numDomainID = @numDomainID
		AND ISNULL(PBP.[Priority],0) > 0
	ORDER BY
		ISNULL(PBP.[Priority],0) ASC

	--GET COMMISSION GLOBAL SETTINGS
	DECLARE @numDiscountServiceItemID NUMERIC(18,0)
	DECLARE @numShippingServiceItemID NUMERIC(18,0)
	DECLARE @tintCommissionType TINYINT
	DECLARE @bitIncludeTaxAndShippingInCommission BIT

	SELECT 
		@numDiscountServiceItemID=numDiscountServiceItemID, 
		@numShippingServiceItemID = numShippingServiceItemID, 
		@tintCommissionType=tintCommissionType, 
		@bitIncludeTaxAndShippingInCommission=bitIncludeTaxAndShippingInCommission 
	FROM 
		Domain 
	WHERE 
		numDomainID=@numDomainID

	DECLARE @TABLEPAID TABLE
	(
		ID INT IDENTITY(1,1),
		numDivisionID NUMERIC(18,0),
		numRelationship NUMERIC(18,0),
		numProfile NUMERIC(18,0),
		numAssignedTo NUMERIC(18,0),
		numRecordOwner NUMERIC(18,0),
		numItemCode NUMERIC(18,0),
		numItemClassification NUMERIC(18,0),
		numReturnHeaderID NUMERIC(18,0),
		tintReturnType TINYINT,
		numReturnItemID NUMERIC(18,0),
		numUnitHour FLOAT,
		monTotAmount MONEY,
		monVendorCost MONEY,
		monAvgCost MONEY,
		numPartner NUMERIC(18,0)
	)

	-- GET FULLY PAID INVOICES WITHIN PAY PERIOD
	INSERT INTO 
		@TABLEPAID
	SELECT 
		DivisionMaster.numDivisionID,
		ISNULL(CompanyInfo.numCompanyType,0),
		ISNULL(CompanyInfo.vcProfile,0),
		(CASE WHEN ISNULL(OMSalesReturn.numAssignedTo,0) > 0 THEN OMSalesReturn.numAssignedTo ELSE OMCreditMemo.numAssignedTo END),
		(CASE WHEN ISNULL(OMSalesReturn.numRecOwner,0) > 0 THEN OMSalesReturn.numRecOwner ELSE OMCreditMemo.numRecOwner END),
		Item.numItemCode,
		Item.numItemClassification,
		ReturnHeader.numReturnHeaderID,
		ReturnHeader.tintReturnType,
		(CASE WHEN tintReturnType IN (1,2) THEN ReturnItems.numReturnItemID ELSE 0 END),
		(CASE WHEN tintReturnType IN (1,2) THEN ISNULL(ReturnItems.numUnitHour,0) ELSE 0 END),
		(CASE WHEN tintReturnType IN (1,2) THEN ISNULL(ReturnItems.monTotAmount,0) ELSE ReturnHeader.monAmount END),
		(CASE WHEN tintReturnType IN (1,2) THEN (ISNULL(ReturnItems.monVendorCost,ISNULL((SELECT Vendor.monCost FROM Item INNER JOIN Vendor ON Item.numVendorID=Vendor.numVendorID AND Vendor.numItemCode=Item.numItemCode WHERE Item.numItemCode=ReturnItems.numItemCode),0)) * ISNULL(ReturnItems.numUnitHour,0)) ELSE 0 END),
		(CASE WHEN tintReturnType IN (1,2) THEN (ISNULL(ReturnItems.monAverageCost,Item.monAverageCost) * ISNULL(ReturnItems.numUnitHour,0)) ELSE 0 END),
		(CASE WHEN ISNULL(OMSalesReturn.numPartner,0) > 0 THEN OMSalesReturn.numPartner ELSE OMCreditMemo.numPartner END)
	FROM 
		ReturnHeader
	INNER JOIN
		DivisionMaster 
	ON
		ReturnHeader.numDivisionID = DivisionMaster.numDivisionID
	INNER JOIN
		CompanyInfo
	ON
		DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
	LEFT JOIN
		OpportunityMaster OMSalesReturn
	ON
		OMSalesReturn.numOppId = ReturnHeader.numOppId
	LEFT JOIN
		OpportunityMaster OMCreditMemo
	ON
		OMCreditMemo.numOppId = ReturnHeader.numReferenceSalesOrder
	LEFT JOIN
		ReturnItems 
	ON
		ReturnHeader.numReturnHeaderID=ReturnItems.numReturnHeaderID
	LEFT JOIN
		OpportunityItems
	ON
		ReturnItems.numOppItemID=OpportunityItems.numoppitemtCode
	LEFT JOIN
		Item
	ON
		ReturnItems.numItemCode = Item.numItemCode
	WHERE
		ReturnHeader.numDomainId = @numDomainID 
		AND ReturnHeader.numReturnStatus = 301
		AND (ISNULL(ReturnHeader.numOppId,0) > 0 OR ISNULL(ReturnHeader.numReferenceSalesOrder,0) > 0)
		AND (ReturnHeader.dtCreatedDate IS NOT NULL AND (CAST(DATEADD(MINUTE,-@ClientTimeZoneOffset,ReturnHeader.dtCreatedDate) AS DATE) BETWEEN @dtStartDate AND @dtEndDate))
		AND (ReturnItems.numItemCode IS NULL OR ReturnItems.numItemCode NOT IN ( @numDiscountServiceItemID))  /*DO NOT INCLUDE DISCOUNT ITEM*/
		AND 1 = (
					CASE 
						WHEN ReturnItems.numItemCode IS NOT NULL AND ReturnItems.numItemCode = @numShippingServiceItemID 
						THEN 
							CASE 
								WHEN ISNULL(@bitIncludeTaxAndShippingInCommission,0) = 1
								THEN	
									1
								ELSE	
									0
							END
						ELSE 
							1 
					END
				)/*CONDITION TO INCLUDE/EXCLUDE INCLUDE SHIPPING ITEM */

		-- LOOP ALL COMMISSION RULES
		DECLARE @i INT = 1
		DECLARE @COUNT INT = 0

		SELECT @COUNT=COUNT(*) FROM @TempCommissionRule

		DECLARE @TEMPITEM TABLE
		(
			ID INT,
			UniqueID INT,
			numUserCntID NUMERIC(18,0),
			numReturnHeaderID NUMERIC(18,0),
			tintReturnType TINYINT,
			numReturnItemID NUMERIC(18,0),
			numItemCode NUMERIC(18,0),
			numUnitHour FLOAT,
			monTotAmount MONEY,
			monVendorCost MONEY,
			monAvgCost MONEY
		)

		WHILE @i <= @COUNT
		BEGIN
			DECLARE @numComRuleID NUMERIC(18,0)
			DECLARE @tintComBasedOn TINYINT
			DECLARE @tintComAppliesTo TINYINT
			DECLARE @tintComOrgType TINYINT
			DECLARE @tintComType TINYINT
			DECLARE @tintAssignTo TINYINT
			DECLARE @decCommission MONEY
			DECLARE @numTotalAmount AS MONEY
			DECLARE @numTotalUnit AS FLOAT
			DECLARE @RowCount AS INT

			--CLEAR PREVIOUS VALUES FROM TEMP TABLE
			DELETE FROM @TEMPITEM

			--FETCH COMMISSION RULE
			SELECT @numComRuleID=numComRuleID,@tintComBasedOn=tintComBasedOn,@tintComType=tintComType,@tintComAppliesTo=tintComAppliesTo,@tintComOrgType=tintComOrgType,@tintAssignTo=tintAssignTo FROM @TempCommissionRule WHERE ID=@i

			--FIND ALL BIZ DOC ITEMS WHERE COMMISSION IS NOT PAID AND CURRENT RULE ITEMS, ORGANIZATION AND ASSINGED TO FILTER CAN BE APPLIED
			INSERT INTO
				@TEMPITEM
			SELECT
				ROW_NUMBER() OVER (ORDER BY ID),
				ID,
				(CASE @tintAssignTo WHEN 1 THEN numAssignedTo WHEN 2 THEN numRecordOwner ELSE numPartner END),
				numReturnHeaderID,
				tintReturnType,
				numReturnItemID,
				numItemCode,
				numUnitHour,
				monTotAmount,
				monVendorCost,
				monAvgCost
			FROM
				@TABLEPAID
			WHERE
				1 = (CASE 
					WHEN tintReturnType = 3 OR tintReturnType = 4 THEN 1
					ELSE 
						(CASE @tintComAppliesTo
							--SPECIFIC ITEMS
							WHEN 1 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleItems WHERE numComRuleID = @numComRuleID AND ISNULL(numValue,0) = numItemCode) > 0 THEN 1 ELSE 0 END
							-- ITEM WITH SPECIFIC CLASSIFICATIONS
							WHEN 2 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleItems WHERE numComRuleID = @numComRuleID AND ISNULL(numValue,0) = numItemClassification) > 0 THEN 1 ELSE 0 END
							-- ALL ITEMS
							ELSE 1
						END)
					END)
				AND 1 = (CASE @tintComOrgType
							-- SPECIFIC ORGANIZATION
							WHEN 1 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleOrganization WHERE numComRuleID = @numComRuleID AND ISNULL(numValue,0) = numDivisionID) > 0 THEN 1 ELSE 0 END
							-- ORGANIZATION WITH SPECIFIC RELATIONSHIP AND PROFILE
							WHEN 2 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleOrganization WHERE numComRuleID = @numComRuleID AND ISNULL(numValue,0) = numRelationship AND ISNULL(numProfile,0) = numProfile) > 0 THEN 1 ELSE 0 END
							-- ALL ORGANIZATIONS
							ELSE 1
						END)
				AND 1 = (CASE @tintAssignTo 
							-- ORDER ASSIGNED TO
							WHEN 1 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleContacts WHERE numComRuleID = @numComRuleID AND numValue=numAssignedTo) > 0 THEN 1 ELSE 0 END
							-- ORDER OWNER
							WHEN 2 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleContacts WHERE numComRuleID = @numComRuleID AND numValue=numRecordOwner) > 0 THEN 1 ELSE 0 END
							-- Order Partner
							WHEN 3 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleContacts WHERE numComRuleID = @numComRuleID AND numValue=numPartner) > 0 THEN 1 ELSE 0 END
							-- NO OPTION SELECTED IN RULE
							ELSE 0
						END)

			-- IF BIZDOC ITEMS ARE FOUND AFTER FILTER
			IF (SELECT COUNT(*) FROM @TEMPITEM) > 0
			BEGIN
				--CHECK COMMISSION BASED ON OPTION OF RULE
				--BASED ON AMOUNT SOLD
				IF @tintComBasedOn = 1
				BEGIN
					SELECT @numTotalAmount = SUM(monTotAmount) FROM @TEMPITEM

					IF (SELECT COUNT(*) FROM CommissionRuleDtl WHERE numComRuleID=@numComRuleID AND (@numTotalAmount BETWEEN intFrom AND intTo)) > 0
					BEGIN
						SELECT TOP 1 @decCommission=ISNULL(decCommission,0) FROM CommissionRuleDtl WHERE numComRuleID=@numComRuleID AND (@numTotalAmount BETWEEN intFrom AND intTo)
					END
				END
				--BASED ON UNITS SOLD
				ELSE IF @tintComBasedOn = 2
				BEGIN
					SELECT @numTotalUnit = SUM(numUnitHour) FROM @TEMPITEM

					IF (SELECT COUNT(*) FROM CommissionRuleDtl WHERE numComRuleID=@numComRuleID AND (@numTotalUnit BETWEEN intFrom AND intTo)) > 0
					BEGIN
						SELECT TOP 1 @decCommission=ISNULL(decCommission,0) FROM CommissionRuleDtl WHERE numComRuleID=@numComRuleID AND (@numTotalUnit BETWEEN intFrom AND intTo)
					END
				END

				--IF COMMISSION AMOUNT FOUND AFTER CHECKING COMMISSION FROM AND TO RANGE IN ABOVE STEP
				IF ISNULL(@decCommission,0) > 0
				BEGIN
					SELECT @RowCount=COUNT(*) FROM @TEMPITEM

					--CALCULATE COMMISSION AND STORE IN TEMPARORY TABLE
					INSERT INTO @TempCommissionUnPaidCreditMemoOrRefund 
					(
						numUserCntID,
						numReturnHeaderID,
						tintReturnType,
						numReturnItemID,
						monCommission,
						numComRuleID,
						tintComType,
						tintComBasedOn,
						decCommission,
						tintAssignTo
					)
					SELECT
						numUserCntID,
						numReturnHeaderID,
						tintReturnType,
						numReturnItemID,
						CASE @tintComType 
							WHEN 1 --PERCENT
								THEN 
									CASE tintReturnType
										WHEN 3 THEN monTotAmount * (@decCommission / CAST(100 AS FLOAT))
										WHEN 4 THEN monTotAmount * (@decCommission / CAST(100 AS FLOAT))
										ELSE
											CASE @tintCommissionType
												--TOTAL AMOUNT PAID
												WHEN 1 THEN monTotAmount * (@decCommission / CAST(100 AS FLOAT))
												--ITEM GROSS PROFIT (VENDOR COST)
												WHEN 2 THEN (ISNULL(monTotAmount,0) - ISNULL(monVendorCost,0)) * (@decCommission / CAST(100 AS FLOAT))
												--ITEM GROSS PROFIT (AVERAGE COST)
												WHEN 3 THEN(ISNULL(monTotAmount,0) - ISNULL(monAvgCost,0)) * (@decCommission / CAST(100 AS FLOAT))
											END
									END
							ELSE  --FLAT: COMMISSION AMOUNT IS EQUALLY DISTRIBUTED AMONG FOUND ENTERIES
								(@decCommission / @RowCount)
						END,
						@numComRuleID,
						@tintComType,
						@tintComBasedOn,
						@decCommission,
						@tintAssignTo
					FROM 
						@TEMPITEM AS T1
					WHERE
						1 = (CASE tintReturnType
								WHEN 3 THEN 1
								WHEN 4 THEN 1
								ELSE
									(CASE @tintCommissionType 
										--TOTAL PAID INVOICE
										WHEN 1 THEN CASE WHEN (ISNULL(monTotAmount,0) > 0) THEN 1 ELSE 0 END 
										-- ITEM GROSS PROFIT (VENDOR COST)
										WHEN 2 THEN CASE WHEN ((ISNULL(monTotAmount,0) - ISNULL(monVendorCost,0)) > 0) THEN 1 ELSE 0 END 
										-- ITEM GROSS PROFIT (AVERAGE COST)
										WHEN 3 THEN CASE WHEN ((ISNULL(monTotAmount,0) - ISNULL(monAvgCost,0)) > 0) THEN 1 ELSE 0 END
										ELSE
											0 
									END)
							END)
						AND (
								SELECT 
									COUNT(*) 
								FROM 
									@TempCommissionUnPaidCreditMemoOrRefund 
								WHERE 
									numUserCntID=T1.numUserCntID 
									AND numReturnHeaderID=T1.numReturnHeaderID 
									AND tintReturnType = T1.tintReturnType 
									AND numReturnItemID = T1.numReturnItemID 
							) = 0
				END
			END

			SET @i = @i + 1

			SET @numComRuleID = 0
			SET @tintComBasedOn = 0
			SET @tintComAppliesTo = 0
			SET @tintComOrgType = 0
			SET @tintComType = 0
			SET @tintAssignTo = 0
			SET @decCommission = 0
			SET @numTotalAmount = 0
			SET @numTotalUnit = 0
			SET @RowCount = 0
		END


	RETURN
END
GO
GO
--select dbo.GetDealAmount(1133,getdate(),1610)
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='GetOrderProfitAmountOrMargin')
DROP FUNCTION GetOrderProfitAmountOrMargin
GO
CREATE FUNCTION [dbo].[GetOrderProfitAmountOrMargin]
(
	@numDomainID NUMERIC(18,0)
	,@numOppID NUMERIC(18,0)
	,@tintMode TINYINT --1: Profit Amount, 2:Profit Margin
)    
RETURNS MONEY    
AS    
BEGIN   
	DECLARE @Value AS MONEY
	DECLARE @numShippingItemID NUMERIC(18,0)
	DECLARE @numDiscountItemID NUMERIC(18,0)
	DECLARE @ProfitCost INT
	SELECT @numShippingItemID=numShippingServiceItemID,@numDiscountItemID=numDiscountServiceItemID,@ProfitCost=numCost FROM Domain WHERE numDomainId=@numDomainID

	If @tintMode = 1
	BEGIN
		SELECT
			@Value = SUM(ISNULL(monTotAmount,0) - (ISNULL(numUnitHour,0) * (CASE @ProfitCost WHEN 3 THEN ISNULL(V.monCost,0) * dbo.fn_UOMConversion(numBaseUnit,I.numItemCode,@numDomainID,numPurchaseUnit) WHEN 2 THEN numCost * dbo.fn_UOMConversion(numBaseUnit,I.numItemCode,@numDomainID,numUOMId) ELSE monAverageCost END)))
		FROM
			OpportunityItems OI
		INNER JOIN
			OpportunityMaster OM
		ON
			OI.numOppId = OM.numOppID
		INNER JOIN
			Item I
		ON
			OI.numItemCode = I.numItemCode
		Left JOIN 
			Vendor V 
		ON 
			V.numVendorID=I.numVendorID 
			AND V.numItemCode=I.numItemCode
		WHERE
			OM.numDomainId=@numDomainID
			AND OM.numOppId = @numOppID
			AND I.numItemCode NOT IN (@numShippingItemID,@numDiscountItemID)
	END
	ELSE IF @tintMode = 2
	BEGIN
		SELECT
			@Value = (SUM(Profit)/SUM(monTotAmount)) * 100
		FROM
		(
			SELECT
				ISNULL(monTotAmount,0) monTotAmount
				,ISNULL(monTotAmount,0) - (ISNULL(numUnitHour,0) * (CASE @ProfitCost WHEN 3 THEN ISNULL(V.monCost,0) * dbo.fn_UOMConversion(numBaseUnit,I.numItemCode,@numDomainID,numPurchaseUnit) WHEN 2 THEN numCost * dbo.fn_UOMConversion(numBaseUnit,I.numItemCode,@numDomainID,numUOMId) ELSE monAverageCost END)) Profit
			FROM
				OpportunityItems OI
			INNER JOIN
				OpportunityMaster OM
			ON
				OI.numOppId = OM.numOppID
			INNER JOIN
				DivisionMaster DM
			ON
				OM.numDivisionId = DM.numDivisionID
			INNER JOIN
				CompanyInfo CI
			ON
				DM.numCompanyID = CI.numCompanyId
			INNER JOIN
				Item I
			ON
				OI.numItemCode = I.numItemCode
			Left JOIN 
				Vendor V 
			ON 
				V.numVendorID=I.numVendorID 
				AND V.numItemCode=I.numItemCode
			WHERE
				OM.numDomainId=@numDomainID
				AND OM.numOppId = @numOppID
				AND I.numItemCode NOT IN (@numShippingItemID,@numDiscountItemID)
		) TEMP
	END

	RETURN CAST(@Value AS MONEY) -- Set Accuracy of Two precision
END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='GetOrganizationPriceLevelName')
DROP FUNCTION GetOrganizationPriceLevelName
GO
CREATE FUNCTION [dbo].[GetOrganizationPriceLevelName] 
( 
	@numDomainID NUMERIC(18,0)
	,@tintPriceLevel TINYINT 
)
RETURNS VARCHAR(300)
AS
BEGIN
	DECLARE @vcPriceLevelName VARCHAR(300) = ''

	SELECT 
		@vcPriceLevelName = ISNULL(NULLIF(pnt.vcPriceLevelName, ''), CONCAT('Price Level ',pt.Id))
	FROM 
	(
		SELECT 
			DISTINCT ROW_NUMBER() OVER(PARTITION BY pt.numItemCode ORDER BY numPricingID) Id
		FROM 
			[PricingTable] pt
		INNER JOIN 
			Item
		ON 
			Item.numItemCode = pt.numItemCode 
		AND 
			Item.numDomainID = @numDomainID
		WHERE 
			tintRuleType = 3
	) pt
	LEFT JOIN 
		PricingNamesTable pnt
	ON 
		pt.Id = pnt.tintPriceLevel
	AND 
		pnt.numDomainID = @numDomainID
	WHERE
		pt.Id = @tintPriceLevel

	RETURN ISNULL(@vcPriceLevelName,'')
END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='GetPercentageChangeInGrossProfit')
DROP FUNCTION GetPercentageChangeInGrossProfit
GO
CREATE FUNCTION [dbo].[GetPercentageChangeInGrossProfit]
(
	 @numDomainID AS NUMERIC(18,0),
	 @dtStartDate DATE,
	 @dtEndDate DATE,
	 @tintDateRange TINYINT --1:YTD TO SAME PERIOD LAST YEAR, 2:MTD TO SAME PERIOD LAST YEAR
)    
RETURNS NUMERIC(18,2)
AS    
BEGIN   
	DECLARE @GrossProfitChange NUMERIC(18,2) = 0.00
	DECLARE @GrossProfit1 INT
	DECLARE @GrossProfit2 INT
	DECLARE @numShippingItemID NUMERIC(18,0)
	DECLARE @numDiscountItemID NUMERIC(18,0)
	DECLARE @ProfitCost INT

	SELECT 
		@numShippingItemID=numShippingServiceItemID
		,@numDiscountItemID=numDiscountServiceItemID
		,@ProfitCost=numCost 
	FROM 
		Domain 
	WHERE 
		numDomainId=@numDomainID


	SELECT 
		@GrossProfit1 = SUM(Profit)
	FROM
	(
		SELECT
			ISNULL(monTotAmount,0) - (ISNULL(numUnitHour,0) * (CASE @ProfitCost WHEN 3 THEN ISNULL(V.monCost,0) * dbo.fn_UOMConversion(numBaseUnit,I.numItemCode,@numDomainID,numPurchaseUnit) WHEN 2 THEN numCost * dbo.fn_UOMConversion(numBaseUnit,I.numItemCode,@numDomainID,numUOMId) ELSE monAverageCost END)) Profit
		FROM
			OpportunityItems OI
		INNER JOIN
			OpportunityMaster OM
		ON
			OI.numOppId = OM.numOppID
		INNER JOIN
			DivisionMaster DM
		ON
			OM.numDivisionId = DM.numDivisionID
		INNER JOIN
			CompanyInfo CI
		ON
			DM.numCompanyID = CI.numCompanyId
		INNER JOIN
			Item I
		ON
			OI.numItemCode = I.numItemCode
		Left JOIN 
			Vendor V 
		ON 
			V.numVendorID=I.numVendorID 
			AND V.numItemCode=I.numItemCode
		WHERE
			OM.numDomainId=@numDomainID
			AND OM.bintCreatedDate BETWEEN  @dtStartDate AND @dtEndDate
			AND ISNULL(OM.tintOppType,0)=1
			AND ISNULL(OM.tintOppStatus,0)=1
			AND I.numItemCode NOT IN (@numShippingItemID,@numDiscountItemID)
	) TEMP

	SELECT 
		@GrossProfit2 = SUM(Profit)
	FROM
	(
		SELECT
			ISNULL(monTotAmount,0) - (ISNULL(numUnitHour,0) * (CASE @ProfitCost WHEN 3 THEN ISNULL(V.monCost,0) * dbo.fn_UOMConversion(numBaseUnit,I.numItemCode,@numDomainID,numPurchaseUnit) WHEN 2 THEN numCost * dbo.fn_UOMConversion(numBaseUnit,I.numItemCode,@numDomainID,numUOMId) ELSE monAverageCost END)) Profit
		FROM
			OpportunityItems OI
		INNER JOIN
			OpportunityMaster OM
		ON
			OI.numOppId = OM.numOppID
		INNER JOIN
			DivisionMaster DM
		ON
			OM.numDivisionId = DM.numDivisionID
		INNER JOIN
			CompanyInfo CI
		ON
			DM.numCompanyID = CI.numCompanyId
		INNER JOIN
			Item I
		ON
			OI.numItemCode = I.numItemCode
		Left JOIN 
			Vendor V 
		ON 
			V.numVendorID=I.numVendorID 
			AND V.numItemCode=I.numItemCode
		WHERE
			OM.numDomainId=@numDomainID
			AND 1 = (CASE 
						WHEN @tintDateRange=1 
						THEN (CASE WHEN OM.bintCreatedDate BETWEEN DATEADD(YEAR,-1,@dtStartDate) AND DATEADD(YEAR,-1,@dtEndDate) THEN 1 ELSE 0 END)
						WHEN @tintDateRange=2 
						THEN (CASE WHEN OM.bintCreatedDate BETWEEN DATEADD(MONTH,-1,@dtStartDate) AND DATEADD(MONTH,-1,@dtEndDate) THEN 1 ELSE 0 END)
						ELSE 0
					END)
			AND ISNULL(OM.tintOppType,0)=1
			AND ISNULL(OM.tintOppStatus,0)=1
			AND I.numItemCode NOT IN (@numShippingItemID,@numDiscountItemID)
	) TEMP


	SET @GrossProfitChange = 100.0 * (ISNULL(@GrossProfit1,0) - ISNULL(@GrossProfit2,0)) / (CASE WHEN ISNULL(@GrossProfit2,0) = 0 THEN 1 ELSE ISNULL(@GrossProfit2,0) END)


	RETURN ISNULL(@GrossProfitChange,0.00)
END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='GetPercentageChangeInLeadToAccountConversion')
DROP FUNCTION GetPercentageChangeInLeadToAccountConversion
GO
CREATE FUNCTION [dbo].[GetPercentageChangeInLeadToAccountConversion]
(
	 @numDomainID AS NUMERIC(18,0),
	 @dtStartDate DATE,
	 @dtEndDate DATE,
	 @tintDateRange TINYINT --1:YTD TO SAME PERIOD LAST YEAR, 2:MTD TO SAME PERIOD LAST YEAR
)    
RETURNS NUMERIC(18,2)
AS    
BEGIN   
	DECLARE @LeadToAccountConversionChange NUMERIC(18,2) = 0.00
	DECLARE @TotalLeadsConvertedToAccount1 INT
	DECLARE @TotalLeadsConvertedToAccount2 INT


	DECLARE @TEMP TABLE
	(
		numDivisionID NUMERIC(18,0)
		,FirstRecordID NUMERIC(18,0)
		,LastRecordID NUMERIC(18,0)
		,tintFirstCRMType TINYINT
		,tintLastCRMType TINYINT
	)

	INSERT INTO @TEMP
	(
		numDivisionID
		,FirstRecordID
		,LastRecordID
	)
	SELECT 
		DivisionMasterPromotionHistory.numDivisionID
		,MIN(ID)
		,MAX(ID)
	FROM 
		DivisionMasterPromotionHistory 
	INNER JOIN
		DivisionMaster DM
	ON
		DivisionMasterPromotionHistory.numDivisionID = DM.numDivisionID
	WHERE 
		DivisionMasterPromotionHistory.numDomainID=@numDomainID 
		AND DM.numDomainID = @numDomainID
		AND DM.bintCreatedDate BETWEEN @dtStartDate AND @dtEndDate
	GROUP BY
		DivisionMasterPromotionHistory.numDivisionID

	UPDATE
		T1
	SET
		tintFirstCRMType = DMPH1.tintPreviousCRMType
		,tintLastCRMType = DMPH2.tintNewCRMType
	FROM
		@TEMP T1
	INNER JOIN
		DivisionMasterPromotionHistory DMPH1
	ON
		T1.FirstRecordID = DMPH1.ID
	INNER JOIN
		DivisionMasterPromotionHistory DMPH2
	ON
		T1.LastRecordID = DMPH2.ID
	WHERE
		DMPH1.tintPreviousCRMType =0

	SELECT 
		@TotalLeadsConvertedToAccount1 = COUNT(*)
	FROM 
		@TEMP T1
	WHERE
		T1.tintFirstCRMType = 0
		AND T1.tintLastCRMType = 2
	
	
	DELETE FROM @TEMP

	INSERT INTO @TEMP
	(
		numDivisionID
		,FirstRecordID
		,LastRecordID
	)
	SELECT 
		DivisionMasterPromotionHistory.numDivisionID
		,MIN(ID)
		,MAX(ID)
	FROM 
		DivisionMasterPromotionHistory 
	INNER JOIN
		DivisionMaster DM
	ON
		DivisionMasterPromotionHistory.numDivisionID = DM.numDivisionID
	WHERE 
		DivisionMasterPromotionHistory.numDomainID=@numDomainID 
		AND DM.numDomainID = @numDomainID
		AND 1 = (CASE 
					WHEN @tintDateRange=1 
					THEN (CASE WHEN DM.bintCreatedDate BETWEEN DATEADD(YEAR,-1,@dtStartDate) AND DATEADD(YEAR,-1,@dtEndDate) THEN 1 ELSE 0 END)
					WHEN @tintDateRange=2 
					THEN (CASE WHEN DM.bintCreatedDate BETWEEN DATEADD(MONTH,-1,@dtStartDate) AND DATEADD(MONTH,-1,@dtEndDate) THEN 1 ELSE 0 END)
					ELSE 0
				END)
	GROUP BY
		DivisionMasterPromotionHistory.numDivisionID

	UPDATE
		T1
	SET
		tintFirstCRMType = DMPH1.tintPreviousCRMType
		,tintLastCRMType = DMPH2.tintNewCRMType
	FROM
		@TEMP T1
	INNER JOIN
		DivisionMasterPromotionHistory DMPH1
	ON
		T1.FirstRecordID = DMPH1.ID
	INNER JOIN
		DivisionMasterPromotionHistory DMPH2
	ON
		T1.LastRecordID = DMPH2.ID
	WHERE
		DMPH1.tintPreviousCRMType =0

	SELECT 
		@TotalLeadsConvertedToAccount2 = COUNT(*)
	FROM 
		@TEMP T1
	WHERE
		T1.tintFirstCRMType = 0
		AND T1.tintLastCRMType = 2


	SET @LeadToAccountConversionChange = 100.0 * (ISNULL(@TotalLeadsConvertedToAccount1,0) - ISNULL(@TotalLeadsConvertedToAccount2,0)) / (CASE WHEN ISNULL(@TotalLeadsConvertedToAccount2,0) = 0 THEN 1 ELSE ISNULL(@TotalLeadsConvertedToAccount2,0) END)


	RETURN ISNULL(@LeadToAccountConversionChange,0.00)
END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='GetPercentageChangeInSalesOppToOrderConversion')
DROP FUNCTION GetPercentageChangeInSalesOppToOrderConversion
GO
CREATE FUNCTION [dbo].[GetPercentageChangeInSalesOppToOrderConversion]
(
	 @numDomainID AS NUMERIC(18,0),
	 @dtStartDate DATE,
	 @dtEndDate DATE,
	 @tintCompareValueOf TINYINT --1: Number of Conversion, 2: Amount of Conversion
)    
RETURNS NUMERIC(18,2)
AS    
BEGIN   
	DECLARE @SalesOppToOrderConversionChange NUMERIC(18,2) = 0.00
	DECLARE @SalesOppToOrderConversion1 INT
	DECLARE @SalesOppToOrderConversion2 INT

	SELECT
		@SalesOppToOrderConversion1 = (CASE WHEN @tintCompareValueOf=1 THEN COUNT(numOppID) ELSE SUM(monDealAmount) END)
	FROM
		OpportunityMaster OM
	WHERE
		OM.numDomainId=@numDomainID
		AND OM.bintCreatedDate BETWEEN  @dtStartDate AND @dtEndDate
		AND ISNULL(OM.tintOppType,0)=1
		AND ISNULL(OM.tintOppStatus,0)=1
		AND bintOppToOrder IS NOT NULL

	SELECT
		@SalesOppToOrderConversion2 = (CASE WHEN @tintCompareValueOf=1 THEN COUNT(numOppID) ELSE SUM(monDealAmount) END)
	FROM
		OpportunityMaster OM
	WHERE
		OM.numDomainId=@numDomainID
		AND OM.bintCreatedDate BETWEEN DATEADD(YEAR,-1,@dtStartDate) AND DATEADD(YEAR,-1,@dtEndDate)
		AND ISNULL(OM.tintOppType,0)=1
		AND ISNULL(OM.tintOppStatus,0)=1
		AND bintOppToOrder IS NOT NULL


	SET @SalesOppToOrderConversionChange = 100.0 * (ISNULL(@SalesOppToOrderConversion1,0) - ISNULL(@SalesOppToOrderConversion2,0)) / (CASE WHEN ISNULL(@SalesOppToOrderConversion2,0) = 0 THEN 1 ELSE ISNULL(@SalesOppToOrderConversion2,0) END)


	RETURN ISNULL(@SalesOppToOrderConversionChange,0.00)
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p' AND NAME ='USP_BizRecurrence_Order')
DROP PROCEDURE USP_BizRecurrence_Order
GO
  
Create PROCEDURE [dbo].[USP_BizRecurrence_Order]  
	@numRecConfigID NUMERIC(18,0), 
	@numDomainID NUMERIC(18,0),
	@numUserCntID NUMERIC(18,0),
	@numOppID NUMERIC(18,0),
	@numRecurOppID NUMERIC(18,0) OUT,
	@numFrequency INTEGER,
	@dtEndDate DATE,
	@Date AS DATE
AS  
BEGIN  
BEGIN TRANSACTION;

BEGIN TRY

--DECLARE @Date AS DATE = GETDATE()

DECLARE @numDivisionID AS NUMERIC(18,0)
DECLARE @numOppType AS TINYINT
DECLARE @tintOppStatus AS TINYINT
DECLARE @DealStatus AS TINYINT
DECLARE @numStatus AS NUMERIC(9)
DECLARE @numCurrencyID AS NUMERIC(9)

DECLARE @intOppTcode AS NUMERIC(18,0)
DECLARE @numNewOppID AS NUMERIC(18,0)
DECLARE @fltExchangeRate AS FLOAT


--Get Existing Opportunity Detail
SELECT 
	@numDivisionID = numDivisionId, 
	@numOppType=tintOppType, 
	@tintOppStatus = tintOppStatus,
	@numCurrencyID=numCurrencyID,
	@numStatus = numStatus,
	@DealStatus = tintOppStatus
FROM 
	OpportunityMaster 
WHERE 
	numOppId = @numOppID                                            

IF ISNULL(@numCurrencyID,0) = 0 
BEGIN
	SET @fltExchangeRate=1
	SELECT @numCurrencyID= ISNULL(numCurrencyID,0) FROM dbo.Domain WHERE numDomainId=@numDomainId	
END
ELSE 
BEGIN
	SET @fltExchangeRate=dbo.GetExchangeRate(@numDomainID,@numCurrencyID)
END

INSERT INTO OpportunityMaster                                                                          
(                                                                             
	numContactId, numDivisionId, txtComments, numCampainID, bitPublicFlag, tintSource, tintSourceType, vcPOppName, 
	intPEstimatedCloseDate, numCreatedBy, bintCreatedDate, numDomainId, numRecOwner, lngPConclAnalysis, tintOppType, 
	numSalesOrPurType, numCurrencyID, fltExchangeRate, numAssignedTo, numAssignedBy, [tintOppStatus], numStatus, 
	vcOppRefOrderNo, bitStockTransfer,bitBillingTerms, intBillingDays,bitInterestType,fltInterest,vcCouponCode, 
	bitDiscountType,fltDiscount,bitPPVariance, vcMarketplaceOrderID, vcMarketplaceOrderReportId, numPercentageComplete,
	numAccountClass, tintTaxOperator, bitUseShippersAccountNo, bitUseMarkupShippingRate, numMarkupShippingRate, 
	intUsedShippingCompany, bintAccountClosingDate
)                                                                          
SELECT
	numContactId, numDivisionId, txtComments, numCampainID, bitPublicFlag, tintSource, tintSourceType, vcPOppName, 
	@Date, @numUserCntID, @Date, numDomainId, @numUserCntID, lngPConclAnalysis, tintOppType, 
	numSalesOrPurType, @numCurrencyID, @fltExchangeRate, numAssignedTo, numAssignedBy, [tintOppStatus], numStatus, vcOppRefOrderNo, 
	bitStockTransfer,bitBillingTerms, intBillingDays,bitInterestType,fltInterest,vcCouponCode, bitDiscountType, fltDiscount,
	bitPPVariance, vcMarketplaceOrderID, vcMarketplaceOrderReportId, numPercentageComplete, numAccountClass,tintTaxOperator, 
	bitUseShippersAccountNo, bitUseMarkupShippingRate, numMarkupShippingRate, intUsedShippingCompany, @Date 
FROM
	OpportunityMaster
WHERE                                                                        
	numOppId = @numOppID

SET @numNewOppID=SCOPE_IDENTITY() 

EXEC USP_OpportunityMaster_CT @numDomainID=@numDomainID,@numUserCntID=@numUserCntID,@numRecordID=@numNewOppID                                             
  
--Update OppName as per Name Template
EXEC dbo.USP_UpdateNameTemplateValue @numOppType,@numDomainID,@numNewOppID

--Map Custom Field	
DECLARE @tintPageID AS TINYINT;
SET @tintPageID=CASE WHEN @numOppType=1 THEN 2 ELSE 6 END 
  	
EXEC dbo.USP_AddParentChildCustomFieldMap @numDomainID = @numDomainID, @numRecordID = @numNewOppID, @numParentRecId = @numDivisionId, @tintPageID = @tintPageID
 	
IF ISNULL(@numStatus,0) > 0 AND @numOppType=1 AND isnull(@DealStatus,0)=1
BEGIN
	EXEC USP_ManageOpportunityAutomationQueue
			@numOppQueueID = 0, -- numeric(18, 0)
			@numDomainID = @numDomainID, -- numeric(18, 0)
			@numOppId = @numNewOppID, -- numeric(18, 0)
			@numOppBizDocsId = 0, -- numeric(18, 0)
			@numOrderStatus = @numStatus, -- numeric(18, 0)
			@numUserCntID = @numUserCntID, -- numeric(18, 0)
			@tintProcessStatus = 1, -- tinyint
			@tintMode = 1 -- TINYINT
END

IF ISNULL(@numStatus,0) > 0 AND @numOppType=1 AND isnull(@DealStatus,0)=1
BEGIN
	IF @numOppType=1 AND @tintOppStatus=1
	BEGIN
		EXEC USP_SalesFulfillmentQueue_Save @numDomainID,@numUserCntID,@numOppID,@numStatus
	END
END


IF (SELECT COUNT(*) FROM OpportunityItems WHERE numOppId = @numOppID) > 0
BEGIN
	-- INSERT OPPORTUNITY ITEMS

	INSERT INTO OpportunityItems                                                                          
	(
		numOppId,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,bitDropShip,bitDiscountType,
		fltDiscount,monTotAmtBefDiscount,vcItemName,vcModelID,vcManufacturer,vcPathForTImage,monVendorCost,numUOMId,bitWorkOrder,
		numVendorWareHouse,numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,numClassID,numToWarehouseItemID,vcAttributes,
		monAvgCost,bitItemPriceApprovalRequired,numRecurParentOppItemID
	)
	SELECT 
		@numNewOppID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,bitDropShip,bitDiscountType,
		fltDiscount,monTotAmtBefDiscount,vcItemName,vcModelID,vcManufacturer,vcPathForTImage,monVendorCost,numUOMId,bitWorkOrder,
		numVendorWareHouse,numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,numClassID,numToWarehouseItemID,vcAttributes,
		monAvgCost,bitItemPriceApprovalRequired,OpportunityItems.numoppitemtCode
	FROM
		OpportunityItems
	WHERE
		numOppID = @numOppID 
           
	EXEC USP_BizRecurrence_WorkOrder @numDomainId,@numUserCntID,@numNewOppID,@numOppID

	INSERT INTO OpportunityKitItems                                                                          
	(
		numOppId,numOppItemID,numChildItemID,numWareHouseItemId,numQtyItemsReq,numQtyItemsReq_Orig,numUOMId,numQtyShipped,monAvgCost
	)
	SELECT 
		@numNewOppId,OI.numoppitemtCode,ID.numChildItemID,ID.numWareHouseItemId,ID.numQtyItemsReq * OI.numUnitHour,ID.numQtyItemsReq,ID.numUOMId,0,ISNULL(I.monAverageCost,0)
	FROM 
		OpportunityItems OI 
	JOIN 
		ItemDetails ID 
	ON 
		OI.numItemCode=ID.numItemKitID
	INNER JOIN
		Item I
	ON
		I.numItemCode = ID.numChildItemID
	WHERE 
		OI.numOppId=@numNewOppID AND 
		OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityKitItems OKI WHERE OKI.numOppId=@numNewOppID)
END

DECLARE @TotalAmount AS FLOAT = 0                                                           
   
SELECT @TotalAmount = sum(monTotAmount) FROM OpportunityItems WHERE numOppId=@numNewOppID                                                                          
UPDATE OpportunityMaster SET monPamount=@TotalAmount WHERE numOppId=@numNewOppID 

--Updating the warehouse items              
DECLARE @tintShipped AS TINYINT = 0                

IF @tintOppStatus=1               
BEGIN       
	EXEC USP_UpdatingInventoryonCloseDeal @numNewOppID,@numUserCntID
END              

DECLARE @tintCRMType AS NUMERIC(9)
       
DECLARE @AccountClosingDate AS DATETIME 
SELECT @numDivisionID=numDivisionID,@AccountClosingDate=bintAccountClosingDate FROM OpportunityMaster WHERE numOppID=@numNewOppID         
SELECT @tintCRMType=tintCRMType FROM divisionmaster WHERE numDivisionID = @numDivisionID 

                 
--Add/Update Address Details
DECLARE @vcStreet varchar(100), @vcCity varchar(50), @vcPostalCode varchar(15), @vcCompanyName varchar(100), @vcAddressName VARCHAR(50)      
DECLARE @numState numeric(9),@numCountry numeric(9), @numCompanyId numeric(9) 
DECLARE @bitIsPrimary BIT

SELECT  
	@vcCompanyName=vcCompanyName,
	@numCompanyId=div.numCompanyID 
FROM 
	CompanyInfo Com                            
JOIN 
	divisionMaster Div                            
ON 
	div.numCompanyID=com.numCompanyID                            
WHERE 
	div.numdivisionID=@numDivisionId

--Bill Address
IF (SELECT COUNT(*) FROM OpportunityAddress WHERE numOppID = @numOppID) > 0
BEGIN
	SELECT  
		@vcStreet=ISNULL(vcBillStreet,''),
		@vcCity=ISNULL(vcBillCity,''),
		@vcPostalCode=ISNULL(vcBillPostCode,''),
		@numState=ISNULL(numBillState,0),
		@numCountry=ISNULL(numBillCountry,0),
		@vcAddressName=vcAddressName
	FROM  
		dbo.OpportunityAddress 
	WHERE   
		numOppID = @numOppID

  	EXEC dbo.USP_UpdateOppAddress
		@numOppID = @numNewOppID,
		@byteMode = 0,
		@vcStreet = @vcStreet,
		@vcCity = @vcCity,
		@vcPostalCode = @vcPostalCode,
		@numState = @numState,
		@numCountry = @numCountry,
		@vcCompanyName = @vcCompanyName,
		@numCompanyId = 0,
		@vcAddressName = @vcAddressName,
		@bitCalledFromProcedure=1
END
  
--Ship Address
IF (SELECT COUNT(*) FROM OpportunityAddress WHERE numOppID = @numOppID) > 0
BEGIN
	SELECT  
		@vcStreet=ISNULL(vcShipStreet,''),
		@vcCity=ISNULL(vcShipCity,''),
		@vcPostalCode=ISNULL(vcShipPostCode,''),
		@numState=ISNULL(numShipState,0),
		@numCountry=ISNULL(numShipCountry,0),
		@vcAddressName=vcAddressName
    FROM  
		dbo.OpportunityAddress
	WHERE   
		numOppID = @numOppID
 
  	EXEC dbo.USP_UpdateOppAddress
		@numOppID = @numNewOppID,
		@byteMode = 1,
		@vcStreet = @vcStreet,
		@vcCity = @vcCity,
		@vcPostalCode = @vcPostalCode,
		@numState = @numState,
		@numCountry = @numCountry,
		@vcCompanyName = @vcCompanyName,
		@numCompanyId =@numCompanyId,
		@vcAddressName = @vcAddressName,
		@bitCalledFromProcedure=1
END


-- IMPORTANT: KEEP THIS BELOW ADDRESS UPDATE LOGIC
--Insert Tax for Division   
IF @numOppType=1 OR (@numOppType=2 and (select isnull(bitPurchaseTaxCredit,0) from Domain where numDomainId=@numDomainId)=1)
BEGIN  
	INSERT INTO dbo.OpportunityMasterTaxItems 
	(
		numOppId,
		numTaxItemID,
		fltPercentage,
		tintTaxType,
		numTaxID
	) 
	SELECT 
		@numNewOppID,
		TI.numTaxItemID,
		TEMPTax.decTaxValue,
		TEMPTax.tintTaxType,
		0
	FROM 
		TaxItems TI 
	JOIN 
		DivisionTaxTypes DTT 
	ON 
		TI.numTaxItemID = DTT.numTaxItemID
	CROSS APPLY
	(
		SELECT
			decTaxValue,
			tintTaxType
		FROM
			dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,TI.numTaxItemID,@numNewOppID,0,NULL)
	) AS TEMPTax
	WHERE 
		DTT.numDivisionID=@numDivisionId 
		AND DTT.bitApplicable=1
	UNION 
	SELECT 
		@numNewOppID,
		0,
		TEMPTax.decTaxValue,
		TEMPTax.tintTaxType,
		0
	FROM 
		dbo.DivisionMaster 
	CROSS APPLY
	(
		SELECT
			decTaxValue,
			tintTaxType
		FROM
			dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,0,@numNewOppID,1,NULL)
	) AS TEMPTax
	WHERE 
		bitNoTax=0 
		AND numDivisionID=@numDivisionID	
	UNION 
	SELECT
		@numNewOppID
		,1
		,decTaxValue
		,tintTaxType
		,numTaxID
	FROM
		dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,1,@numNewOppID,1,NULL)	
END

--Delete Tax for Opportunity Items if item deleted 
DELETE FROM OpportunityItemsTaxItems WHERE numOppId=@numNewOppID AND numOppItemID NOT IN (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId=@numNewOppID)

--Insert Tax for Opportunity Items
INSERT INTO dbo.OpportunityItemsTaxItems 
(
	numOppId,
	numOppItemID,
	numTaxItemID,
	numTaxID
) 
SELECT 
	@numNewOppID,
	OI.numoppitemtCode,
	TI.numTaxItemID,
	0
FROM 
	dbo.OpportunityItems OI 
JOIN 
	dbo.ItemTax IT 
ON 
	OI.numItemCode=IT.numItemCode 
JOIN
	TaxItems TI 
ON 
	TI.numTaxItemID=IT.numTaxItemID 
WHERE 
	OI.numOppId=@numNewOppID AND 
	IT.bitApplicable=1 AND 
	OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numNewOppID)
UNION
SELECT 
	@numNewOppID,
	OI.numoppitemtCode,
	0,
	0
FROM 
	dbo.OpportunityItems OI 
JOIN 
	dbo.Item I 
ON 
	OI.numItemCode=I.numItemCode
WHERE 
	OI.numOppId=@numNewOppID AND 
	I.bitTaxable=1 AND
	OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numNewOppID)
UNION
SELECT
	@numNewOppID,
	OI.numoppitemtCode,
	1,
	TD.numTaxID
FROM
	dbo.OpportunityItems OI 
INNER JOIN
	ItemTax IT
ON
	IT.numItemCode = OI.numItemCode
INNER JOIN
	TaxDetails TD
ON
	TD.numTaxID = IT.numTaxID
	AND TD.numDomainId = @numDomainId
WHERE
	OI.numOppId = @numNewOppID
	AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numNewOppID)

  
UPDATE OpportunityMaster SET monDealAmount=dbo.GetDealAmount(@numNewOppID,@Date,0) WHERE numOppId=@numNewOppID


IF @numNewOppID > 0
BEGIN
	-- Insert newly created opportunity to transaction
	INSERT INTO RecurrenceTransaction ([numRecConfigID],[numRecurrOppID],[dtCreatedDate]) VALUES (@numRecConfigID,@numNewOppID,GETDATE())

	-- Insert newly created opportunity to transaction history - Records are not deleted in this table when parent record is delete
	INSERT INTO RecurrenceTransactionHistory ([numRecConfigID],[numRecurrOppID],[dtCreatedDate]) VALUES (@numRecConfigID,@numNewOppID,GETDATE())

	--Get next recurrence date for order 
	DECLARE @dtNextRecurrenceDate DATE = NULL

	SET @dtNextRecurrenceDate = (CASE @numFrequency
								WHEN 1 THEN DATEADD(D,1,@Date) --Daily
								WHEN 2 THEN DATEADD(D,7,@Date) --Weekly
								WHEN 3 THEN DATEADD(M,1,@Date) --Monthly
								WHEN 4 THEN DATEADD(M,4,@Date)  --Quarterly
								END)
	
	--Increase value of number of transaction completed by 1
	UPDATE RecurrenceConfiguration SET numTransaction = ISNULL(numTransaction,0) + 1 WHERE numRecConfigID = @numRecConfigID
			
	PRINT @dtNextRecurrenceDate
	PRINT @dtEndDate

	-- Set recurrence status as completed if next recurrence date is greater than end date
	If @dtNextRecurrenceDate > @dtEndDate
	BEGIN
		UPDATE RecurrenceConfiguration SET bitCompleted = 1 WHERE numRecConfigID = @numRecConfigID
	END
	ELSE -- Set next recurrence date
	BEGIN
		UPDATE RecurrenceConfiguration SET dtNextRecurrenceDate = @dtNextRecurrenceDate WHERE numRecConfigID = @numRecConfigID
	END
END


UPDATE OpportunityMaster SET bitRecurred = 1 WHERE numOppId = @numNewOppID

SET @numRecurOppID = @numNewOppID

END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000);
    DECLARE @ErrorSeverity INT;
    DECLARE @ErrorState INT;

    SELECT @ErrorMessage = ERROR_MESSAGE(), @ErrorSeverity = ERROR_SEVERITY(), @ErrorState = ERROR_STATE();

    RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState);

	IF @@TRANCOUNT > 0
        ROLLBACK TRANSACTION;
END CATCH

IF @@TRANCOUNT > 0
    COMMIT TRANSACTION;

END  
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_CalculateCommissionPaidBizDoc')
DROP PROCEDURE USP_CalculateCommissionPaidBizDoc
GO
CREATE PROCEDURE [dbo].[USP_CalculateCommissionPaidBizDoc]
	-- Add the parameters for the stored procedure here
	 @numComPayPeriodID AS NUMERIC(18,0),
	 @numDomainID AS NUMERIC(18,0),
	 @ClientTimeZoneOffset INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
SET NOCOUNT ON;

BEGIN TRY
BEGIN TRANSACTION
	DECLARE @dtPayStart DATE 
	DECLARE @dtPayEnd DATE

	SELECT @dtPayStart=dtStart,@dtPayEnd=dtEnd FROM CommissionPayPeriod WHERE numComPayPeriodID = @numComPayPeriodID

	-- DELETE UNPAID COMMISSION ENTRIES BETWEEN PAY PERIOD DATE RANGE
	DELETE FROM 
		BizDocComission 
	WHERE 
		numDomainId=@numDomainID 
		AND ISNULL(bitCommisionPaid,0)=0 
		AND numOppBizDocItemID IN (SELECT 
										ISNULL(numOppBizDocItemID,0)
									FROM 
										OpportunityBizDocs OppBiz
									INNER JOIN
										OpportunityMaster OppM
									ON
										OppBiz.numOppID = OppM.numOppID
									INNER JOIN
										OpportunityBizDocItems OppBizItems
									ON
										OppBiz.numOppBizDocsId = OppBizItems.numOppBizDocID
									OUTER APPLY
									(
										SELECT 
											MAX(DM.dtDepositDate) dtDepositDate
										FROM 
											DepositMaster DM 
										JOIN 
											dbo.DepositeDetails DD
										ON 
											DM.numDepositId=DD.numDepositID 
										WHERE 
											DM.tintDepositePage=2 
											AND DM.numDomainId=@numDomainId
											AND DD.numOppID=OppBiz.numOppID 
											AND DD.numOppBizDocsID =OppBiz.numOppBizDocsID
									) TempDepositMaster
									WHERE
										OppM.numDomainId = @numDomainID 
										AND OppM.tintOppType = 1
										AND OppM.tintOppStatus = 1
										AND OppBiz.bitAuthoritativeBizDocs = 1
										AND ISNULL(OppBiz.monAmountPaid,0) >= OppBiz.monDealAmount
										AND (TempDepositMaster.dtDepositDate IS NOT NULL AND (CAST(DATEADD(MINUTE,-@ClientTimeZoneOffset,TempDepositMaster.dtDepositDate) AS DATE) BETWEEN @dtPayStart AND @dtPayEnd)))

	--GET COMMISSION RULE OF DOMAIN
	DECLARE @TempCommissionRule TABLE
	(
		ID INT IDENTITY(1,1),
		numComRuleID NUMERIC(18,0),
		tintComBasedOn TINYINT,
		tintComType TINYINT,
		tintComAppliesTo TINYINT,
		tintComOrgType TINYINT,
		tintAssignTo TINYINT
	)

	INSERT INTO
		@TempCommissionRule
	SELECT
		CR.numComRuleID,
		CR.tintComBasedOn,
		CR.tintComType,
		CR.tintComAppliesTo,
		CR.tintComOrgType,
		CR.tintAssignTo
	FROM
		CommissionRules CR
	LEFT JOIN
		PriceBookPriorities PBP
	ON	
		CR.tintComAppliesTo = PBP.Step2Value 
		AND CR.tintComOrgType = PBP.Step3Value
	WHERE
		CR.numDomainID = @numDomainID
		AND ISNULL(PBP.[Priority],0) > 0
	ORDER BY
		ISNULL(PBP.[Priority],0) ASC

	--GET COMMISSION GLOBAL SETTINGS
	DECLARE @numDiscountServiceItemID NUMERIC(18,0)
	DECLARE @numShippingServiceItemID NUMERIC(18,0)
	DECLARE @tintCommissionType TINYINT
	DECLARE @bitIncludeTaxAndShippingInCommission BIT

	SELECT 
		@numDiscountServiceItemID=numDiscountServiceItemID, 
		@numShippingServiceItemID = numShippingServiceItemID, 
		@tintCommissionType=tintCommissionType, 
		@bitIncludeTaxAndShippingInCommission=bitIncludeTaxAndShippingInCommission 
	FROM 
		Domain 
	WHERE 
		numDomainID=@numDomainID

	--TEMPORARY TABLES TO HOLD DATA
	DECLARE @TempBizCommission TABLE
	(
		numUserCntID NUMERIC(18,0),
		numOppID NUMERIC(18,0),
		numOppBizDocID NUMERIC(18,0),
		numOppBizDocItemID NUMERIC(18,0),
		monCommission MONEY,
		numComRuleID NUMERIC(18,0),
		tintComType TINYINT,
		tintComBasedOn TINYINT,
		decCommission FLOAT,
		tintAssignTo TINYINT
	)

	DECLARE @TABLEPAID TABLE
	(
		ID INT IDENTITY(1,1),
		numDivisionID NUMERIC(18,0),
		numRelationship NUMERIC(18,0),
		numProfile NUMERIC(18,0),
		numAssignedTo NUMERIC(18,0),
		numRecordOwner NUMERIC(18,0),
		numItemCode NUMERIC(18,0),
		numItemClassification NUMERIC(18,0),
		numOppID NUMERIC(18,0),
		numOppBizDocID NUMERIC(18,0),
		numOppBizDocItemID NUMERIC(18,0),
		numUnitHour FLOAT,
		monTotAmount MONEY,
		monVendorCost MONEY,
		monAvgCost MONEY,
		numPartner NUMERIC(18,0)
	)

	-- GET FULLY PAID INVOICES WITHIN PAY PERIOD
	INSERT INTO 
		@TABLEPAID
	SELECT 
		OppM.numDivisionID,
		ISNULL(CompanyInfo.numCompanyType,0),
		ISNULL(CompanyInfo.vcProfile,0),
		OppM.numAssignedTo,
		OppM.numRecOwner,
		OppBizItems.numItemCode,
		Item.numItemClassification,
		OppM.numOppID,
		OppBizItems.numOppBizDocID,
		OppBizItems.numOppBizDocItemID,
		ISNULL(OppBizItems.numUnitHour,0),
		ISNULL(OppBizItems.monTotAmount,0),
		(ISNULL(OppMItems.monVendorCost,0) * ISNULL(OppBizItems.numUnitHour,0)),
		(ISNULL(OppMItems.monAvgCost,0) * ISNULL(OppBizItems.numUnitHour,0)),
		OppM.numPartner
	FROM 
		OpportunityBizDocs OppBiz
	INNER JOIN
		OpportunityMaster OppM
	ON
		OppBiz.numOppID = OppM.numOppID
	INNER JOIN
		DivisionMaster 
	ON
		OppM.numDivisionID = DivisionMaster.numDivisionID
	INNER JOIN
		CompanyInfo
	ON
		DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
	INNER JOIN
		OpportunityBizDocItems OppBizItems
	ON
		OppBiz.numOppBizDocsId = OppBizItems.numOppBizDocID
	INNER JOIN
		OpportunityItems OppMItems 
	ON
		OppBizItems.numOppItemID = OppMItems.numoppitemtCode
	INNER JOIN
		Item
	ON
		OppBizItems.numItemCode = Item.numItemCode
	LEFT JOIN
		BizDocComission BDC
	ON
		OppM.numOppId = BDC.numOppID 
		AND OppBiz.numOppBizDocsId = BDC.numOppBizDocId
		AND OppBizItems.numOppBizDocItemID = BDC.numOppBizDocItemID
	OUTER APPLY
	(
		SELECT 
			MAX(DM.dtDepositDate) dtDepositDate
		FROM 
			DepositMaster DM 
		JOIN 
			dbo.DepositeDetails DD
		ON 
			DM.numDepositId=DD.numDepositID 
		WHERE 
			DM.tintDepositePage=2 
			AND DM.numDomainId=@numDomainId
			AND DD.numOppID=OppBiz.numOppID 
			AND DD.numOppBizDocsID =OppBiz.numOppBizDocsID
	) TempDepositMaster
	WHERE
		OppM.numDomainId = @numDomainID 
		AND OppM.tintOppType = 1
		AND OppM.tintOppStatus = 1
		AND OppBiz.bitAuthoritativeBizDocs = 1
		AND ISNULL(OppBiz.monAmountPaid,0) >= OppBiz.monDealAmount
		AND (TempDepositMaster.dtDepositDate IS NOT NULL AND (CAST(DATEADD(MINUTE,-@ClientTimeZoneOffset,TempDepositMaster.dtDepositDate) AS DATE) BETWEEN @dtPayStart AND @dtPayEnd))
		AND OppBizItems.numItemCode NOT IN ( @numDiscountServiceItemID )  /*DO NOT INCLUDE DISCOUNT ITEM*/
		AND 1 = (
					CASE 
						WHEN OppBizItems.numItemCode = @numShippingServiceItemID 
						THEN 
							CASE 
								WHEN ISNULL(@bitIncludeTaxAndShippingInCommission,0) = 1
								THEN	
									1
								ELSE	
									0
							END
						ELSE 
							1 
					END
				)/*CONDITION TO INCLUDE/EXCLUDE INCLUDE SHIPPING ITEM */
		AND BDC.numOppBizDocItemID IS NULL --NO ENTRY AVAILABLE IN BizDocComission TABLE FOR BIZDOC ITEM


		-- LOOP ALL COMMISSION RULES
		DECLARE @i INT = 1
		DECLARE @COUNT INT = 0

		SELECT @COUNT=COUNT(*) FROM @TempCommissionRule

		DECLARE @TEMPITEM TABLE
		(
			ID INT,
			UniqueID INT,
			numUserCntID NUMERIC(18,0),
			numOppID NUMERIC(18,0),
			numOppBizDocID NUMERIC(18,0),
			numOppBizDocItemID NUMERIC(18,0),
			numItemCode NUMERIC(18,0),
			numUnitHour FLOAT,
			monTotAmount MONEY,
			monVendorCost MONEY,
			monAvgCost MONEY
		)

		WHILE @i <= @COUNT
		BEGIN
			DECLARE @numComRuleID NUMERIC(18,0)
			DECLARE @tintComBasedOn TINYINT
			DECLARE @tintComAppliesTo TINYINT
			DECLARE @tintComOrgType TINYINT
			DECLARE @tintComType TINYINT
			DECLARE @tintAssignTo TINYINT
			DECLARE @decCommission MONEY
			DECLARE @numTotalAmount AS MONEY
			DECLARE @numTotalUnit AS FLOAT
			DECLARE @RowCount AS INT

			--CLEAR PREVIOUS VALUES FROM TEMP TABLE
			DELETE FROM @TEMPITEM

			--FETCH COMMISSION RULE
			SELECT @numComRuleID=numComRuleID,@tintComBasedOn=tintComBasedOn,@tintComType=tintComType,@tintComAppliesTo=tintComAppliesTo,@tintComOrgType=tintComOrgType,@tintAssignTo=tintAssignTo FROM @TempCommissionRule WHERE ID=@i

			--FIND ALL BIZ DOC ITEMS WHERE COMMISSION IS NOT PAID AND CURRENT RULE ITEMS, ORGANIZATION AND ASSINGED TO FILTER CAN BE APPLIED
			INSERT INTO
				@TEMPITEM
			SELECT
				ROW_NUMBER() OVER (ORDER BY ID),
				ID,
				(CASE @tintAssignTo WHEN 1 THEN numAssignedTo WHEN 2 THEN numRecordOwner ELSE numPartner END),
				numOppID,
				numOppBizDocID,
				numOppBizDocItemID,
				numItemCode,
				numUnitHour,
				monTotAmount,
				monVendorCost,
				monAvgCost
			FROM
				@TABLEPAID
			WHERE
				1 = (CASE @tintComAppliesTo
						--SPECIFIC ITEMS
						WHEN 1 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleItems WHERE numComRuleID = @numComRuleID AND ISNULL(numValue,0) = numItemCode) > 0 THEN 1 ELSE 0 END
						-- ITEM WITH SPECIFIC CLASSIFICATIONS
						WHEN 2 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleItems WHERE numComRuleID = @numComRuleID AND ISNULL(numValue,0) = numItemClassification) > 0 THEN 1 ELSE 0 END
						-- ALL ITEMS
						ELSE 1
					END)
				AND 1 = (CASE @tintComOrgType
							-- SPECIFIC ORGANIZATION
							WHEN 1 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleOrganization WHERE numComRuleID = @numComRuleID AND ISNULL(numValue,0) = numDivisionID) > 0 THEN 1 ELSE 0 END
							-- ORGANIZATION WITH SPECIFIC RELATIONSHIP AND PROFILE
							WHEN 2 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleOrganization WHERE numComRuleID = @numComRuleID AND ISNULL(numValue,0) = numRelationship AND ISNULL(numProfile,0) = numProfile) > 0 THEN 1 ELSE 0 END
							-- ALL ORGANIZATIONS
							ELSE 1
						END)
				AND 1 = (CASE @tintAssignTo 
							-- ORDER ASSIGNED TO
							WHEN 1 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleContacts WHERE numComRuleID = @numComRuleID AND numValue=numAssignedTo) > 0 THEN 1 ELSE 0 END
							-- ORDER OWNER
							WHEN 2 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleContacts WHERE numComRuleID = @numComRuleID AND numValue=numRecordOwner) > 0 THEN 1 ELSE 0 END
							-- Order Partner
							WHEN 3 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleContacts WHERE numComRuleID = @numComRuleID AND numValue=numPartner) > 0 THEN 1 ELSE 0 END
							-- NO OPTION SELECTED IN RULE
							ELSE 0
						END)

			-- IF BIZDOC ITEMS ARE FOUND AFTER FILTER
			IF (SELECT COUNT(*) FROM @TEMPITEM) > 0
			BEGIN
				--CHECK COMMISSION BASED ON OPTION OF RULE
				--BASED ON AMOUNT SOLD
				IF @tintComBasedOn = 1
				BEGIN
					SELECT @numTotalAmount = SUM(monTotAmount) FROM @TEMPITEM

					IF (SELECT COUNT(*) FROM CommissionRuleDtl WHERE numComRuleID=@numComRuleID AND (@numTotalAmount BETWEEN intFrom AND intTo)) > 0
					BEGIN
						SELECT TOP 1 @decCommission=ISNULL(decCommission,0) FROM CommissionRuleDtl WHERE numComRuleID=@numComRuleID AND (@numTotalAmount BETWEEN intFrom AND intTo)
					END
				END
				--BASED ON UNITS SOLD
				ELSE IF @tintComBasedOn = 2
				BEGIN
					SELECT @numTotalUnit = SUM(numUnitHour) FROM @TEMPITEM

					IF (SELECT COUNT(*) FROM CommissionRuleDtl WHERE numComRuleID=@numComRuleID AND (@numTotalUnit BETWEEN intFrom AND intTo)) > 0
					BEGIN
						SELECT TOP 1 @decCommission=ISNULL(decCommission,0) FROM CommissionRuleDtl WHERE numComRuleID=@numComRuleID AND (@numTotalUnit BETWEEN intFrom AND intTo)
					END
				END

				--IF COMMISSION AMOUNT FOUND AFTER CHECKING COMMISSION FROM AND TO RANGE IN ABOVE STEP
				IF ISNULL(@decCommission,0) > 0
				BEGIN
					SELECT @RowCount=COUNT(*) FROM @TEMPITEM

					--CALCULATE COMMISSION AND STORE IN TEMPARORY TABLE
					INSERT INTO @TempBizCommission 
					(
						numUserCntID,
						numOppID,
						numOppBizDocID,
						numOppBizDocItemID,
						monCommission,
						numComRuleID,
						tintComType,
						tintComBasedOn,
						decCommission,
						tintAssignTo
					)
					SELECT
						numUserCntID,
						numOppID,
						numOppBizDocID,
						numOppBizDocItemID,
						CASE @tintComType 
							WHEN 1 --PERCENT
								THEN 
									CASE @tintCommissionType
										--TOTAL AMOUNT PAID
										WHEN 1 THEN monTotAmount * (@decCommission / CAST(100 AS FLOAT))
										--ITEM GROSS PROFIT (VENDOR COST)
										WHEN 2 THEN (ISNULL(monTotAmount,0) - ISNULL(monVendorCost,0)) * (@decCommission / CAST(100 AS FLOAT))
										--ITEM GROSS PROFIT (AVERAGE COST)
										WHEN 3 THEN(ISNULL(monTotAmount,0) - ISNULL(monAvgCost,0)) * (@decCommission / CAST(100 AS FLOAT))
									END
							ELSE  --FLAT: COMMISSION AMOUNT IS EQUALLY DISTRIBUTED AMONG FOUND ENTERIES
								(@decCommission / @RowCount)
						END,
						@numComRuleID,
						@tintComType,
						@tintComBasedOn,
						@decCommission,
						@tintAssignTo
					FROM 
						@TEMPITEM AS T1
					WHERE
						1 = (CASE @tintCommissionType 
								--TOTAL PAID INVOICE
								WHEN 1 THEN CASE WHEN (ISNULL(monTotAmount,0) > 0) THEN 1 ELSE 0 END 
								-- ITEM GROSS PROFIT (VENDOR COST)
								WHEN 2 THEN CASE WHEN ((ISNULL(monTotAmount,0) - ISNULL(monVendorCost,0)) > 0) THEN 1 ELSE 0 END 
								-- ITEM GROSS PROFIT (AVERAGE COST)
								WHEN 3 THEN CASE WHEN ((ISNULL(monTotAmount,0) - ISNULL(monAvgCost,0)) > 0) THEN 1 ELSE 0 END
								ELSE
									0 
							END) 
						AND (
								SELECT 
									COUNT(*) 
								FROM 
									@TempBizCommission 
								WHERE 
									numUserCntID=T1.numUserCntID 
									AND numOppID=T1.numOppID 
									AND numOppBizDocID = T1.numOppBizDocID 
									AND numOppBizDocItemID = T1.numOppBizDocItemID 
							) = 0
				END
			END

			SET @i = @i + 1

			SET @numComRuleID = 0
			SET @tintComBasedOn = 0
			SET @tintComAppliesTo = 0
			SET @tintComOrgType = 0
			SET @tintComType = 0
			SET @tintAssignTo = 0
			SET @decCommission = 0
			SET @numTotalAmount = 0
			SET @numTotalUnit = 0
			SET @RowCount = 0
		END

		--INSERT CALCULATED ENTERIES IN BIZDOCCOMMISSION TABLE
		INSERT INTO BizDocComission
		(	
			numDomainID,
			numUserCntID,
			numOppBizDocId,
			numComissionAmount,
			numOppBizDocItemID,
			numComRuleID,
			tintComType,
			tintComBasedOn,
			decCommission,
			numOppID,
			bitFullPaidBiz,
			numComPayPeriodID,
			tintAssignTo
		)
		SELECT
			@numDomainID,
			numUserCntID,
			numOppBizDocID,
			monCommission,
			numOppBizDocItemID,
			numComRuleID,
			tintComType,
			tintComBasedOn,
			decCommission,
			numOppID,
			1,
			@numComPayPeriodID,
			tintAssignTo
		FROM
			@TempBizCommission
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
END
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DashboardTemplate_Delete')
DROP PROCEDURE USP_DashboardTemplate_Delete
GO
CREATE PROCEDURE [dbo].[USP_DashboardTemplate_Delete]
	@numDomainID NUMERIC(18,0)
	,@numTemplateID NUMERIC(18,0)
AS
BEGIN 
	BEGIN TRY
	BEGIN TRANSACTION
		DELETE FROM ReportDashboard WHERE numDomainID=@numDomainID AND ISNULL(numDashboardTemplateID,0)=@numTemplateID AND numReportID NOT IN (SELECT numReportID FROM ReportListMaster WHERE ISNULL(bitDefault,0)=1)
		UPDATE UserMaster SET numDashboardTemplateID=0 WHERE numDomainID=@numDomainID AND numDashboardTemplateID=@numTemplateID
		DELETE FROM DashboardTemplate WHERE numDomainID=@numDomainID AND numTemplateID=@numTemplateID
	COMMIT
	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000)
		DECLARE @ErrorNumber INT
		DECLARE @ErrorSeverity INT
		DECLARE @ErrorState INT
		DECLARE @ErrorLine INT
		DECLARE @ErrorProcedure NVARCHAR(200)

		IF @@TRANCOUNT > 0
			ROLLBACK TRANSACTION;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorNumber = ERROR_NUMBER(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE(),
			@ErrorLine = ERROR_LINE(),
			@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

		RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
	END CATCH
END
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DashboardTemplate_GetAllByDomain')
DROP PROCEDURE USP_DashboardTemplate_GetAllByDomain
GO
CREATE PROCEDURE [dbo].[USP_DashboardTemplate_GetAllByDomain]      
@numDomainID AS NUMERIC(18,0)
AS
BEGIN 
	SELECT * FROM DashboardTemplate WHERE numDomainID=@numDomainID
END
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DashboardTemplate_Save')
DROP PROCEDURE USP_DashboardTemplate_Save
GO
CREATE PROCEDURE [dbo].[USP_DashboardTemplate_Save]
	@numDomainID NUMERIC(18,0)
	,@numUserCntID NUMERIC(18,0)
	,@bitUpdate BIT
	,@numTemplateID NUMERIC(18,0)
	,@vcTemplateName VARCHAR(300)
AS
BEGIN 
BEGIN TRY
BEGIN TRANSACTION
	IF @bitUpdate = 0
	BEGIN
		DECLARE @NewTemplateID NUMERIC(18,0)

		INSERT INTO DashboardTemplate
		(
			numDomainID
			,vcTemplateName
			,dtCreatedDate
			,numCreatedBy
		)
		VALUES
		(
			@numDomainID
			,@vcTemplateName
			,GETUTCDATE()
			,@numUserCntID
		)

		SET @NewTemplateID=SCOPE_IDENTITY()


		INSERT INTO DashboardTemplateReports
		(
			numDomainID
			,numDashboardTemplateID
			,numReportID
			,tintReportType
			,tintChartType
			,vcHeaderText
			,vcFooterText
			,tintRow
			,tintColumn
			,vcXAxis
			,vcYAxis
			,vcAggType
			,tintReportCategory
			,intHeight
			,intWidth
		)
		SELECT
			numDomainID
			,@NewTemplateID
			,numReportID
			,tintReportType
			,tintChartType
			,vcHeaderText
			,vcFooterText
			,tintRow
			,tintColumn
			,vcXAxis
			,vcYAxis
			,vcAggType
			,tintReportCategory
			,intHeight
			,intWidth
		FROM
			ReportDashboard
		WHERE
			numDomainID=@numDomainID
			AND numUserCntID=@numUserCntID
			AND ISNULL(numDashboardTemplateID,0)=ISNULL(@numTemplateID,0)

		DELETE FROM ReportDashboard WHERE numDomainID=@numDomainID AND ISNULL(numDashboardTemplateID,0)=ISNULL(@numTemplateID,0) AND numUserCntID = @numUserCntID AND ISNULL(bitNewAdded,0)=1
	END
	ELSE
	BEGIN
		UPDATE DashboardTemplate SET dtModifiedDate=GETUTCDATE(),numModifiedBy=@numUserCntID WHERE numTemplateID=@numTemplateID

		DELETE FROM DashboardTemplateReports WHERE numDashboardTemplateID=@numTemplateID

		INSERT INTO DashboardTemplateReports
		(
			numDomainID
			,numDashboardTemplateID
			,numReportID
			,tintReportType
			,tintChartType
			,vcHeaderText
			,vcFooterText
			,tintRow
			,tintColumn
			,vcXAxis
			,vcYAxis
			,vcAggType
			,tintReportCategory
			,intHeight
			,intWidth
		)
		SELECT
			numDomainID
			,numDashboardTemplateID
			,numReportID
			,tintReportType
			,tintChartType
			,vcHeaderText
			,vcFooterText
			,tintRow
			,tintColumn
			,vcXAxis
			,vcYAxis
			,vcAggType
			,tintReportCategory
			,intHeight
			,intWidth
		FROM
			ReportDashboard
		WHERE
			numDomainID=@numDomainID
			AND numUserCntID=@numUserCntID
			AND numDashboardTemplateID=@numTemplateID

		
		UPDATE ReportDashboard SET bitNewAdded=0 WHERE numDomainID=@numDomainID AND numDashboardTemplateID=@numTemplateID AND numUserCntID = @numUserCntID

		-- DELETE DASHBOARD REPORT CONFIGURATION FOR ALL USERS WHO ARE USING EDITED DASHBOARD TEMPLATE EXCEPT CURRENT USER(@numUserCntID)
		DELETE FROM 
			ReportDashboard 
		WHERE 
			numDomainID=@numDomainID 
			AND numDashboardTemplateID=@numTemplateID 
			AND numUserCntID IN (SELECT numUserDetailId FROM UserMaster WHERE numDomainID=@numDomainID AND numUserDetailId <> @numUserCntID)

		INSERT INTO [dbo].[ReportDashboard]
        (
			numDomainID
			,numReportID
			,numUserCntID
			,tintReportType
			,tintChartType
			,vcHeaderText
			,vcFooterText
			,tintRow
			,tintColumn
			,vcXAxis
			,vcYAxis
			,vcAggType
			,tintReportCategory
			,intHeight
			,intWidth
			,numDashboardTemplateID
		)
		SELECT
			numDomainID
			,numReportID
			,numUserDetailId
			,tintReportType
			,tintChartType
			,vcHeaderText
			,vcFooterText
			,tintRow
			,tintColumn
			,vcXAxis
			,vcYAxis
			,vcAggType
			,tintReportCategory
			,intHeight
			,intWidth
			,numDashboardTemplateID
		FROM
			DashboardTemplateReports
		OUTER APPLY
		(
			SELECT 
				numUserDetailId 
			FROM 
				UserMaster 
			WHERE 
				numDomainID=@numDomainID 
				AND numDashboardTemplateID=@numTemplateID
				 AND numUserDetailId <> @numUserCntID
		) DashboardTemplateAssignee
		WHERE
			numDomainID=@numDomainID
			AND numDashboardTemplateID=@numTemplateID
	END
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
END
GO
/****** Object:  StoredProcedure [dbo].[usp_DemoteAccount]    Script Date: 07/26/2008 16:15:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_demoteaccount')
DROP PROCEDURE usp_demoteaccount
GO
CREATE PROCEDURE [dbo].[usp_DemoteAccount]      
 @numDivisionID numeric=0,      
 @byteMode as tinyint=0,
  @numDomainID as numeric(9)=0,
  @numUserCntID numeric    
AS      
BEGIN
	DECLARE @tintCurrentCRMType TINYINT    
    
	if @byteMode=0    
	begin     
		declare @numGRPID as numeric 
	
		select @numGRPID=isnull(numGrpId,0),@tintCurrentCRMType=tintCRMType from DivisionMaster 
		where numDivisionId =@numDivisionID and numDomainID=@numDomainID
		if @numGRPID=0 
		begin
		 UPDATE dbo.DivisionMaster SET numGrpId=2 ,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate()    
		 WHERE numDivisionId=@numDivisionID  and numDomainID=@numDomainID
		end
	 
		INSERT INTO DivisionMasterPromotionHistory
		(
			numDomainID
			,numDivisionID
			,tintPreviousCRMType
			,tintNewCRMType
			,dtPromotedBy
			,dtPromotionDate
		)
		VALUES
		(
			@numDomainID
			,@numDivisionID
			,@tintCurrentCRMType
			,0
			,@numUserCntID
			,GETUTCDATE()
		)       

		 UPDATE dbo.DivisionMaster SET tintCRMType=0,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate()      
		 WHERE numDivisionId=@numDivisionID   and numDomainID=@numDomainID  
	end     
	if @byteMode=1    
	begin       
		SELECT @tintCurrentCRMType=tintCRMType FROM DivisionMaster WHERE numDivisionID=@numDivisionID     
	
		INSERT INTO DivisionMasterPromotionHistory
		(
			numDomainID
			,numDivisionID
			,tintPreviousCRMType
			,tintNewCRMType
			,dtPromotedBy
			,dtPromotionDate
		)
		VALUES
		(
			@numDomainID
			,@numDivisionID
			,@tintCurrentCRMType
			,1
			,@numUserCntID
			,GETUTCDATE()
		)       

		 UPDATE dbo.DivisionMaster SET tintCRMType=1,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate()     
		 WHERE numDivisionId=@numDivisionID    and numDomainID=@numDomainID 
	end
	if @byteMode=2    
	begin       
		SELECT @tintCurrentCRMType=tintCRMType FROM DivisionMaster WHERE numDivisionID=@numDivisionID     
	
		INSERT INTO DivisionMasterPromotionHistory
		(
			numDomainID
			,numDivisionID
			,tintPreviousCRMType
			,tintNewCRMType
			,dtPromotedBy
			,dtPromotionDate
		)
		VALUES
		(
			@numDomainID
			,@numDivisionID
			,@tintCurrentCRMType
			,2
			,@numUserCntID
			,GETUTCDATE()
		)      


		 UPDATE 
			dbo.DivisionMaster 
		SET 
			tintCRMType=2, 
			bintProsProm = getutcdate()
			,bintProsPromBy = @numUserCntID
			,numModifiedBy=@numUserCntID
			,bintModifiedDate=getutcdate()       
		 WHERE 
			numDivisionId=@numDivisionID 
			AND numDomainID=@numDomainID 
	end
END
GO
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ElasticSearch_GetGeneralLedger')
DROP PROCEDURE dbo.USP_ElasticSearch_GetGeneralLedger
GO
CREATE PROCEDURE [dbo].[USP_ElasticSearch_GetGeneralLedger]
	@numDomainID NUMERIC(18,0)
AS 
BEGIN
	DECLARE @dtStartDate DATETIME
	DECLARE @dtEndDate DATETIME

	SELECT @dtStartDate=dtPeriodFrom,@dtEndDate=dtPeriodTo FROM FinancialYear WHERE numDomainID=@numDomainID AND ISNULL(bitCurrentYear,0) = 1

	IF Not @dtStartDate IS NULL AND  Not @dtEndDate IS NULL
	BEGIN
		SELECT 
			*,
			CONCAT('<b style="color:#7f7f7f">G/L Transaction (',Search_TransactionType,'):</b>',dbo.FormatedDateFromDate(SearchDate_EntryDate,@numDomainID),', ',Search_CompanyName,', ',Search_Memo,', ',Search_CompanyName,', ',SearchNumber_Amount) AS displaytext
		FROM
		(
			SELECT
				GJD.numJournalId as id,
				'generalledger' as module,
				CONCAT('/Accounting/frmNewJournalEntry.aspx?frm=GeneralLedger&JournalId=',GJD.numJournalId) AS url,
				GJD.numTransactionId AS [text],
				GJH.datEntry_Date AS SearchDate_EntryDate,
				ISNULL(CI.vcCompanyName,'') AS Search_CompanyName,
				(CASE WHEN ISNULL(GJD.numCreditAmt,0)=0 THEN GJD.numDebitAmt ELSE GJD.numCreditAmt END) as SearchNumber_Amount,        
				ISNULL(GJD.varDescription,'') AS Search_Memo,
				(CASE WHEN RIGHT(GJH.varDescription,1) = ':' THEN SUBSTRING(GJH.varDescription,0,LEN(GJH.varDescription)) ELSE ISNULL(GJH.varDescription,'') END)  AS Search_Description,
				CASE 
					WHEN isnull(GJH.numCheckHeaderID, 0) <> 0 THEN + 'Check' 
					WHEN isnull(GJH.numDepositId, 0) <> 0 AND DM.tintDepositePage = 1 THEN 'Deposit' 
					WHEN isnull(GJH.numDepositId, 0) <> 0 AND DM.tintDepositePage = 2 THEN 'Received Payment' 
					WHEN isnull(GJH.numOppId, 0) <> 0 AND isnull(GJH.numOppBizDocsId, 0) <> 0 AND isnull(GJH.numBizDocsPaymentDetId, 0) = 0 AND dbo.OpportunityMaster.tintOppType = 1 THEN 'BizDocs Invoice' 
					WHEN isnull(GJH.numOppId, 0) <> 0 AND isnull(GJH.numOppBizDocsId, 0) <> 0 AND isnull(GJH.numBizDocsPaymentDetId, 0) = 0 AND dbo.OpportunityMaster.tintOppType = 2 THEN 'BizDocs Purchase' 
					WHEN isnull(GJH.numCategoryHDRID, 0) <> 0 THEN 'Time And Expenses' 
					WHEN ISNULL(GJH.numBillId,0) <>0 AND isnull(BH.bitLandedCost,0)=1 then 'Landed Cost' 
					WHEN isnull(GJH.numBillID, 0) <> 0 THEN 'Bill'
					WHEN isnull(GJH.numBillPaymentID, 0) <> 0 THEN 'Bill Payment'
					WHEN isnull(GJH.numReturnID, 0) <> 0 THEN 
						CASE 
							WHEN RH.tintReturnType=1 AND RH.tintReceiveType=1 THEN 'Sales Return Refund'
							WHEN RH.tintReturnType=1 AND RH.tintReceiveType=2 THEN 'Sales Return Credit Memo'
							WHEN RH.tintReturnType=2 AND RH.tintReceiveType=2 THEN 'Purchase Return Credit Memo'
							WHEN RH.tintReturnType=3 AND RH.tintReceiveType=2 THEN 'Credit Memo'
							WHEN RH.tintReturnType=4 AND RH.tintReceiveType=1 THEN 'Refund Against Credit Memo'
						END 
					WHEN isnull(GJH.numOppId, 0) <> 0 AND isnull(GJH.numOppBizDocsId, 0) = 0 THEN 'PO Fulfillment'
					WHEN GJH.numJournal_Id <> 0 THEN 'Journal' 
				END AS Search_TransactionType,
				ISNULL(CAST(CH.numCheckNo AS VARCHAR),'') Search_CheckNo
			FROM   
				General_Journal_Header GJH 
			INNER JOIN General_Journal_Details GJD ON GJH.numJournal_Id = GJD.numJournalId 
			INNER JOIN Chart_Of_Accounts ON GJD.numChartAcntId = dbo.Chart_Of_Accounts.numAccountId 
			LEFT OUTER JOIN TimeAndExpense ON GJH.numCategoryHDRID = dbo.TimeAndExpense.numCategoryHDRID 
			LEFT OUTER JOIN DivisionMaster ON GJD.numCustomerId = dbo.DivisionMaster.numDivisionID and GJD.numDomainID = dbo.DivisionMaster.numDomainID
			LEFT OUTER JOIN CompanyInfo CI ON dbo.DivisionMaster.numCompanyID = CI.numCompanyId 
			LEFT OUTER JOIN OpportunityMaster ON GJH.numOppId = dbo.OpportunityMaster.numOppId 
			LEFT OUTER JOIN OpportunityBizDocs ON GJH.numOppBizDocsId = dbo.OpportunityBizDocs.numOppBizDocsId 
			LEFT OUTER JOIN VIEW_BIZPAYMENT ON GJH.numBizDocsPaymentDetId = dbo.VIEW_BIZPAYMENT.numBizDocsPaymentDetId 
			LEFT OUTER JOIN dbo.DepositMaster DM ON DM.numDepositId = GJH.numDepositId 
			LEFT OUTER JOIN dbo.ReturnHeader RH ON RH.numReturnHeaderID = GJH.numReturnID 
			LEFT OUTER JOIN dbo.CheckHeader CH ON GJH.numCheckHeaderID = CH.numCheckHeaderID
			LEFT OUTER JOIN BillHeader BH  ON ISNULL(GJH.numBillId,0) = BH.numBillId
			WHERE  
				   GJH.[datEntry_Date] >= @dtStartDate
				   AND GJH.[datEntry_Date] <= @dtEndDate
				   AND GJD.numDomainId = @numDomainId
				   AND GJD.numChartAcntId IN (SELECT COA.numAccountId FROM Chart_Of_Accounts COA WHERE numDomainID=@numDomainID AND numParntAcntTypeId IN (SELECT numAccountTypeID FROM AccountTypeDetail WHERE numDomainID=@numDomainID  AND vcAccountCode='01010101'))
		) TMEP
		ORDER BY 
			SearchDate_EntryDate
	END
END



			
/*
-- Added Support for Bills in AP Aging summary,by chintan
*/

-- [dbo].[USP_GetAccountPayableAging] 1,null 6719
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getaccountpayableaging')
DROP PROCEDURE usp_getaccountpayableaging
GO
set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

CREATE PROCEDURE [dbo].[USP_GetAccountPayableAging]
    (
      @numDomainId AS NUMERIC(9) = 0,
      @numDivisionId AS NUMERIC(9) = NULL,
      @numAccountClass AS NUMERIC(9)=0,
	  @dtFromDate AS DATETIME,
	  @dtToDate AS DATETIME
    )
AS 
    BEGIN 
  
  		SET @dtFromDate = DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE()), @dtFromDate);
		SET @dtToDate =  DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE()), @dtToDate);

		PRINT @dtFromDate
		PRINT @dtToDate

        CREATE TABLE #TempAPRecord
            (
              [ID] [int] IDENTITY(1, 1)
                         NOT NULL,
              [numUserCntID] [numeric](18, 0) NOT NULL,
              [numOppId] [numeric](18, 0) NULL,
              [numDivisionId] [numeric](18, 0) NULL,
              [numOppBizDocsId] [numeric](18, 0) NULL,
              [DealAmount] [float] NULL,
              [numBizDocId] [numeric](18, 0) NULL,
              dtDueDate DATETIME NULL,
              [numCurrencyID] [numeric](18, 0) NULL,AmountPaid FLOAT NULL,monUnAppliedAmount MONEY NULL 
            ) ;
  
  
        CREATE TABLE #TempAPRecord1
            (
              [numID] [numeric](18, 0) IDENTITY(1, 1)
                                       NOT NULL,
              [numUserCntID] [numeric](18, 0) NOT NULL,
              [numDivisionId] [numeric](18, 0) NULL,
              [tintCRMType] [tinyint] NULL,
              [vcCompanyName] [varchar](200) NULL,
              [vcCustPhone] [varchar](50) NULL,
              [numContactId] [numeric](18, 0) NULL,
              [vcEmail] [varchar](100) NULL,
              [numPhone] [varchar](50) NULL,
              [vcContactName] [varchar](100) NULL,
              [numThirtyDays] [numeric](18, 2) NULL,
              [numSixtyDays] [numeric](18, 2) NULL,
              [numNinetyDays] [numeric](18, 2) NULL,
              [numOverNinetyDays] [numeric](18, 2) NULL,
              [numThirtyDaysOverDue] [numeric](18, 2) NULL,
              [numSixtyDaysOverDue] [numeric](18, 2) NULL,
              [numNinetyDaysOverDue] [numeric](18, 2) NULL,
              [numOverNinetyDaysOverDue] [numeric](18, 2) NULL,
              [numCompanyID] [numeric](18, 2) NULL,
              [numDomainID] [numeric](18, 2) NULL,
              [intThirtyDaysCount] [int] NULL,
              [intThirtyDaysOverDueCount] [int] NULL,
              [intSixtyDaysCount] [int] NULL,
              [intSixtyDaysOverDueCount] [int] NULL,
              [intNinetyDaysCount] [int] NULL,
              [intOverNinetyDaysCount] [int] NULL,
              [intNinetyDaysOverDueCount] [int] NULL,
              [intOverNinetyDaysOverDueCount] [int] NULL,
              [numThirtyDaysPaid] [numeric](18, 2) NULL,
              [numSixtyDaysPaid] [numeric](18, 2) NULL,
              [numNinetyDaysPaid] [numeric](18, 2) NULL,
              [numOverNinetyDaysPaid] [numeric](18, 2) NULL,
              [numThirtyDaysOverDuePaid] [numeric](18, 2) NULL,
              [numSixtyDaysOverDuePaid] [numeric](18, 2) NULL,
              [numNinetyDaysOverDuePaid] [numeric](18, 2) NULL,
              [numOverNinetyDaysOverDuePaid] [numeric](18, 2) NULL
              ,monUnAppliedAmount MONEY NULL,numTotal MONEY NULL 
            ) ;
  
  
  
        DECLARE @AuthoritativePurchaseBizDocId AS INTEGER
        SELECT  @AuthoritativePurchaseBizDocId = isnull(numAuthoritativePurchase,
                                                        0)
        FROM    AuthoritativeBizDocs
        WHERE   numDomainId = @numDomainId ;
        
     
        INSERT  INTO [#TempAPRecord]
                (
                  numUserCntID,
                  [numOppId],
                  [numDivisionId],
                  [numOppBizDocsId],
                  [DealAmount],
                  [numBizDocId],
                  dtDueDate,
                  [numCurrencyID],AmountPaid
                )
                SELECT  @numDomainId,
                        OB.numOppId,
                        Opp.numDivisionId,
                        OB.numOppBizDocsId,
                        ISNULL(OB.monDealAmount --dbo.GetDealAmount(OB.numOppID,getutcdate(),OB.numOppBizDocsId)
                               * ISNULL(Opp.fltExchangeRate, 1), 0)
                        - ISNULL(OB.monAmountPaid
                                 * ISNULL(Opp.fltExchangeRate, 1), 0) DealAmount,
                        OB.numBizDocId,
                        DATEADD(DAY,
                                CASE WHEN Opp.bitBillingTerms = 1
                                     --THEN convert(int, ISNULL(dbo.fn_GetListItemName(isnull(Opp.intBillingDays, 0)), 0))
                                     THEN convert(int, ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(Opp.intBillingDays,0)), 0))
                                     ELSE 0
                                END, dtFromDate) AS dtDueDate,
                        ISNULL(Opp.numCurrencyID, 0) numCurrencyID,
                        ISNULL(OB.monAmountPaid * ISNULL(Opp.fltExchangeRate, 1), 0) AmountPaid
                FROM    OpportunityMaster Opp
                        JOIN OpportunityBizDocs OB ON OB.numOppId = Opp.numOppID
                        LEFT OUTER JOIN [Currency] C ON Opp.[numCurrencyID] = C.[numCurrencyID]
                WHERE   tintOpptype = 2
                        AND tintoppStatus = 1
                --AND dtShippedDate IS NULL
                        AND opp.numDomainID = @numDomainId
                        AND OB.bitAuthoritativeBizDocs = 1
                        AND ISNULL(OB.tintDeferred, 0) <> 1
                        AND numBizDocId = @AuthoritativePurchaseBizDocId
                        AND ( Opp.numDivisionId = @numDivisionId
                              OR @numDivisionId IS NULL
                            )
                        AND (Opp.numAccountClass=@numAccountClass OR @numAccountClass=0)
						AND OB.[dtCreatedDate] BETWEEN @dtFromDate AND @dtToDate
                GROUP BY OB.numOppId,
                        Opp.numDivisionId,
                        OB.numOppBizDocsId,
                        Opp.fltExchangeRate,
                        OB.numBizDocId,
                        OB.dtCreatedDate,
                        Opp.bitBillingTerms,
                        Opp.intBillingDays,
                        OB.monDealAmount,
                        Opp.numCurrencyID,
                        OB.[dtFromDate],OB.monAmountPaid
                HAVING  ( ISNULL(OB.monDealAmount * ISNULL(Opp.fltExchangeRate,
                                                           1), 0)
                          - ISNULL(OB.monAmountPaid
                                   * ISNULL(Opp.fltExchangeRate, 1), 0) ) > 0
                UNION ALL 
		--Add Bill Amount
                SELECT  @numDomainId,
                        0 numOppId,
                        BH.numDivisionId,
                        0 numBizDocsId,
                        ISNULL(SUM(BH.monAmountDue), 0) - ISNULL(SUM(BH.monAmtPaid), 0) DealAmount,
                        0 numBizDocId,
                        BH.dtDueDate AS dtDueDate,
                        0 numCurrencyID,ISNULL(SUM(BH.monAmtPaid), 0) AS AmountPaid
                FROM    BillHeader BH
                WHERE   BH.numDomainID = @numDomainId
                        AND ( BH.numDivisionId = @numDivisionId
                              OR @numDivisionId IS NULL
                            )
                         AND ISNULL(BH.monAmountDue, 0) - ISNULL(BH.monAmtPaid, 0) > 0   
                         AND (BH.numAccountClass=@numAccountClass OR @numAccountClass=0)
						 AND BH.[dtBillDate] BETWEEN @dtFromDate AND @dtToDate
                GROUP BY BH.numDivisionId,
                        BH.dtDueDate
                HAVING  ISNULL(SUM(BH.monAmountDue), 0) - ISNULL(SUM(BH.monAmtPaid), 0) > 0
                UNION ALL
		--Add Write Check entry (As Amount Paid)
                SELECT  @numDomainId,
                        0 numOppId,
                        CD.numCustomerId as numDivisionId,
                        0 numBizDocsId,
                        0 DealAmount,
                        0 numBizDocId,
                        CH.dtCheckDate AS dtDueDate,
                        0 numCurrencyID,ISNULL(SUM(CD.monAmount), 0) AS AmountPaid
                FROM    CheckHeader CH
						JOIN  dbo.CheckDetails CD ON CH.numCheckHeaderID=CD.numCheckHeaderID
                        INNER JOIN dbo.Chart_Of_Accounts COA ON COA.numAccountId = CD.numChartAcntId
                WHERE   CH.numDomainID = @numDomainId AND CH.tintReferenceType=1
						AND COA.vcAccountCode LIKE '01020102%'
                        AND ( CD.numCustomerId = @numDivisionId
                              OR @numDivisionId IS NULL
                            )
                        AND (CH.numAccountClass=@numAccountClass OR @numAccountClass=0)
						AND CH.[dtCheckDate] BETWEEN @dtFromDate AND @dtToDate
                GROUP BY CD.numCustomerId,
                        CH.dtCheckDate
		--Show Impact of AP journal entries in report as well 
                UNION ALL
                SELECT  @numDomainId,
                        0,
                        GJD.numCustomerId,
                        0,
                        CASE WHEN GJD.numDebitAmt > 0 THEN -1 * GJD.numDebitAmt
                             ELSE GJD.numCreditAmt
                        END,
                        0,
                        GJH.datEntry_Date,
                        GJD.[numCurrencyID],0 AS AmountPaid
                FROM    dbo.General_Journal_Header GJH
                        INNER JOIN dbo.General_Journal_Details GJD ON GJH.numJournal_Id = GJD.numJournalId
                                                                      AND GJH.numDomainId = GJD.numDomainId
                        INNER JOIN dbo.Chart_Of_Accounts COA ON COA.numAccountId = GJD.numChartAcntId
                WHERE   GJH.numDomainId = @numDomainId 
                        AND COA.vcAccountCode LIKE '01020102%'
                        AND ISNULL(numOppId, 0) = 0
                        AND ISNULL(numOppBizDocsId, 0) = 0
                        AND ISNULL(GJH.numBillID,0)=0 AND ISNULL(GJH.numBillPaymentID,0)=0 AND ISNULL(GJH.numCheckHeaderID,0)=0 AND ISNULL(GJH.numReturnID,0)=0
                        AND ISNULL(GJH.numPayrollDetailID,0)=0
                         AND ( GJD.numCustomerId = @numDivisionId
                              OR @numDivisionId IS NULL
                            )
                            AND (GJH.numAccountClass=@numAccountClass OR @numAccountClass=0)
						AND [GJH].[datEntry_Date]  BETWEEN @dtFromDate AND @dtToDate
                UNION ALL
	--Add Commission Amount
                SELECT  @numDomainId,
                        OBD.numOppId numOppId,
                        D.numDivisionId,
                        BDC.numOppBizDocId numBizDocsId,
                       isnull(sum(BDC.numComissionAmount - ISNULL(PT.monCommissionAmt,0)),0)  DealAmount,
                        0 numBizDocId,
                        OBD.dtCreatedDate AS dtDueDate,
                        ISNULL(OPP.numCurrencyID, 0) numCurrencyID,0 AS AmountPaid
                FROM    BizDocComission BDC
                        JOIN OpportunityBizDocs OBD ON BDC.numOppBizDocId = OBD.numOppBizDocsId
                        join OpportunityMaster Opp on OPP.numOppId = OBD.numOppId
                        JOIN Domain D on D.numDomainID = BDC.numDomainID
                        LEFT JOIN dbo.PayrollTracking PT ON BDC.numComissionID=PT.numComissionID
                WHERE   OPP.numDomainID = @numDomainId
                        and BDC.numDomainID = @numDomainId
                        AND ( D.numDivisionId = @numDivisionId
                              OR @numDivisionId IS NULL
                            )
                            AND (Opp.numAccountClass=@numAccountClass OR @numAccountClass=0)
						AND OBD.[dtCreatedDate]  BETWEEN @dtFromDate AND @dtToDate
				--AND BDC.bitCommisionPaid=0
                        AND OBD.bitAuthoritativeBizDocs = 1
                GROUP BY D.numDivisionId,
                        OBD.numOppId,
                        BDC.numOppBizDocId,
                        OPP.numCurrencyID,
                        BDC.numComissionAmount,
                        OBD.dtCreatedDate
                HAVING  ISNULL(BDC.numComissionAmount, 0) > 0
			UNION ALL --Standalone Refund against Account Receivable
			 SELECT @numDomainId,
					0 AS numOppID,
					RH.numDivisionId,
					0,
					CASE WHEN GJD.numDebitAmt > 0 THEN GJD.numDebitAmt * -1 ELSE GJD.numCreditAmt END DealAmount,
					0 numBizDocId,
					GJH.datEntry_Date,GJD.[numCurrencyID],0 AS AmountPaid
			 FROM   dbo.ReturnHeader RH JOIN dbo.General_Journal_Header GJH ON RH.numReturnHeaderID = GJH.numReturnID
			JOIN General_Journal_Details GJD ON GJH.numJournal_Id = GJD.numJournalId 
			INNER JOIN dbo.Chart_Of_Accounts COA ON COA.numAccountId = GJD.numChartAcntId
			WHERE RH.tintReturnType=4 AND RH.numDomainId=@numDomainId  AND COA.vcAccountCode LIKE '01020102%'
					AND (RH.numDivisionId = @numDivisionId OR @numDivisionId IS NULL)
					AND (monBizDocAmount > 0) AND ISNULL(RH.numBillPaymentIDRef,0)>0
					AND (RH.numAccountClass=@numAccountClass OR @numAccountClass=0)
					AND RH.[dtCreatedDate]  BETWEEN @dtFromDate AND @dtToDate
					--GROUP BY DM.numDivisionId,DM.dtDepositDate
		
		
---SELECT  SUM(monUnAppliedAmount) FROM    [#TempAPRecord]

   INSERT  INTO [#TempAPRecord]
        (
          numUserCntID,
          [numOppId],
          [numDivisionId],
          [numOppBizDocsId],
          [DealAmount],
          [numBizDocId],
          [numCurrencyID],
          [dtDueDate],AmountPaid,monUnAppliedAmount
        )
SELECT @numDomainId,0, BPH.numDivisionId,0,0,0,0,NULL,0,sum(ISNULL(monPaymentAmount,0) - ISNULL(monAppliedAmount,0))       
 FROM BillPaymentHeader BPH
		WHERE BPH.numDomainId=@numDomainId   
		AND (BPH.numDivisionId = @numDivisionId
                            OR @numDivisionId IS NULL)
		AND (ISNULL(monPaymentAmount,0) - ISNULL(monAppliedAmount,0)) > 0
		AND (BPH.numAccountClass=@numAccountClass OR @numAccountClass=0)
		AND [BPH].[dtPaymentDate]  BETWEEN @dtFromDate AND @dtToDate
		GROUP BY BPH.numDivisionId
		

--SELECT  GETDATE()
        INSERT  INTO [#TempAPRecord1]
                (
                  [numUserCntID],
                  [numDivisionId],
                  [tintCRMType],
                  [vcCompanyName],
                  [vcCustPhone],
                  [numContactId],
                  [vcEmail],
                  [numPhone],
                  [vcContactName],
                  [numThirtyDays],
                  [numSixtyDays],
                  [numNinetyDays],
                  [numOverNinetyDays],
                  [numThirtyDaysOverDue],
                  [numSixtyDaysOverDue],
                  [numNinetyDaysOverDue],
                  [numOverNinetyDaysOverDue],
                  numCompanyID,
                  numDomainID,
                  [intThirtyDaysCount],
                  [intThirtyDaysOverDueCount],
                  [intSixtyDaysCount],
                  [intSixtyDaysOverDueCount],
                  [intNinetyDaysCount],
                  [intOverNinetyDaysCount],
                  [intNinetyDaysOverDueCount],
                  [intOverNinetyDaysOverDueCount],
                  [numThirtyDaysPaid],
                  [numSixtyDaysPaid],
                  [numNinetyDaysPaid],
                  [numOverNinetyDaysPaid],
                  [numThirtyDaysOverDuePaid],
                  [numSixtyDaysOverDuePaid],
                  [numNinetyDaysOverDuePaid],
                  [numOverNinetyDaysOverDuePaid],monUnAppliedAmount
                )
                SELECT DISTINCT
                        @numDomainId,
                        Div.numDivisionID,
                        tintCRMType AS tintCRMType,
                        vcCompanyName AS vcCompanyName,
                        '',
                        NULL,
                        '',
                        '',
                        '',
                        0 numThirtyDays,
                        0 numSixtyDays,
                        0 numNinetyDays,
                        0 numOverNinetyDays,
                        0 numThirtyDaysOverDue,
                        0 numSixtyDaysOverDue,
                        0 numNinetyDaysOverDue,
                        0 numOverNinetyDaysOverDue,
                        C.numCompanyID,
                        Div.numDomainID,
                        0 [intThirtyDaysCount],
                        0 [intThirtyDaysOverDueCount],
                        0 [intSixtyDaysCount],
                        0 [intSixtyDaysOverDueCount],
                        0 [intNinetyDaysCount],
                        0 [intOverNinetyDaysCount],
                        0 [intNinetyDaysOverDueCount],
                        0 [intOverNinetyDaysOverDueCount],
                         0 numThirtyDaysPaid,
                        0 numSixtyDaysPaid,
                        0 numNinetyDaysPaid,
                        0 numOverNinetyDaysPaid,
                        0 numThirtyDaysOverDuePaid,
                        0 numSixtyDaysOverDuePaid,
                        0 numNinetyDaysOverDuePaid,
                        0 numOverNinetyDaysOverDuePaid,0 monUnAppliedAmount
                FROM    Divisionmaster Div
                        INNER JOIN CompanyInfo C ON C.numCompanyId = Div.numCompanyID
                    --JOIN [#TempAPRecord] t ON Div.[numDivisionID] = t.[numDivisionId]
                WHERE   Div.[numDomainID] = @numDomainId
                        AND Div.[numDivisionID] IN ( SELECT DISTINCT
                                                            [numDivisionID]
                                                     FROM   [#TempAPRecord] )



--Update Custome Phone 
        UPDATE  #TempAPRecord1
        SET     [vcCustPhone] = X.[vcCustPhone]
        FROM    ( SELECT    CASE WHEN numPhone IS NULL THEN ''
                                 WHEN numPhone = '' THEN ''
                                 ELSE ', ' + numPhone
                            END AS vcCustPhone,
                            numDivisionId
                  FROM      AdditionalContactsInformation
                  WHERE     bitPrimaryContact = 1
                            AND numDivisionId IN (
                            SELECT DISTINCT
                                    numDivisionID
                            FROM    [#TempAPRecord1]
                            WHERE   numUserCntID = @numDomainId )
                ) X
        WHERE   #TempAPRecord1.numDivisionID = X.numDivisionID

--Update Customer info 
        UPDATE  #TempAPRecord1
        SET     [numContactId] = X.[numContactID],
                [vcEmail] = X.[vcEmail],
                [vcContactName] = X.[vcContactName],
                numPhone = X.numPhone
        FROM    ( SELECT  numContactId AS numContactID,
                            ISNULL(vcEmail, '') AS vcEmail,
                            ISNULL(vcFirstname, '') + ' ' + ISNULL(vcLastName, '') AS vcContactName,
                            CAST(ISNULL(CASE WHEN numPhone IS NULL THEN ''
                                             WHEN numPhone = '' THEN ''
                                             ELSE ', ' + numPhone
                                        END, '') AS VARCHAR) AS numPhone,
                            [numDivisionId]
                  FROM      AdditionalContactsInformation
                  WHERE     (vcPosition = 843 or isnull(bitPrimaryContact,0)=1)
                            AND numDivisionId IN (
                            SELECT DISTINCT
                                    numDivisionID
                            FROM    [#TempAPRecord1]
                            WHERE   numUserCntID = @numDomainId )
                ) X
        WHERE   #TempAPRecord1.numDivisionID = X.numDivisionID

                    
--SELECT  GETDATE()
------------------------------------------      
        DECLARE @baseCurrency AS NUMERIC
        SELECT  @baseCurrency = numCurrencyID
        FROM    dbo.Domain
        WHERE   numDomainId = @numDomainId
/*when any Order is placed in Foreign currency (other than base currency)then show it in purple color */

------------------------------------------
        UPDATE  #TempAPRecord1
        SET     numThirtyDays = X.numThirtyDays
        FROM    ( SELECT    SUM(DealAmount) AS numThirtyDays,
                            [numDivisionId]
                  FROM      [#TempAPRecord]
                  WHERE     numUserCntID = @numDomainId
                            AND DATEDIFF(DAY, dbo.GetUTCDateWithoutTime(),
                                         [dtDueDate]) BETWEEN 0
                                                      AND     30
                  GROUP BY  numDivisionId
                ) X
        WHERE   #TempAPRecord1.numDivisionID = X.numDivisionID
        
         UPDATE  #TempAPRecord1
        SET     numThirtyDaysPaid = X.numThirtyDaysPaid
        FROM    ( SELECT    SUM(AmountPaid) AS numThirtyDaysPaid,
                            [numDivisionId]
                  FROM      [#TempAPRecord]
                  WHERE     numUserCntID = @numDomainId
                            AND DATEDIFF(DAY, dbo.GetUTCDateWithoutTime(),
                                         [dtDueDate]) BETWEEN 0
                                                      AND     30
                  GROUP BY  numDivisionId
                ) X
        WHERE   #TempAPRecord1.numDivisionID = X.numDivisionID
        
        UPDATE  #TempAPRecord1
        SET     intThirtyDaysCount = 2
        WHERE   numDivisionId IN (
                SELECT DISTINCT
                        [numDivisionId]
                FROM    [#TempAPRecord]
                WHERE   numUserCntID = @numDomainId
                        AND DATEDIFF(DAY, dbo.GetUTCDateWithoutTime(),
                                     [dtDueDate]) BETWEEN 0
                                                  AND     30
                        AND numCurrencyID <> @baseCurrency )
------------------------------------------
        UPDATE  #TempAPRecord1
        SET     numSixtyDays = X.numSixtyDays
        FROM    ( SELECT    SUM(DealAmount) AS numSixtyDays,
                            numDivisionId
                  FROM      [#TempAPRecord]
                  WHERE     numUserCntID = @numDomainId
                            AND DATEDIFF(DAY, dbo.GetUTCDateWithoutTime(),
                                         [dtDueDate]) BETWEEN 31
                                                      AND     60
                            AND dbo.GetUTCDateWithoutTime() <= [dtDueDate]
                  GROUP BY  numDivisionId
                ) X
        WHERE   #TempAPRecord1.numDivisionID = X.numDivisionID
        
        UPDATE  #TempAPRecord1
        SET     numSixtyDaysPaid = X.numSixtyDaysPaid
        FROM    ( SELECT    SUM(AmountPaid) AS numSixtyDaysPaid,
                            numDivisionId
                  FROM      [#TempAPRecord]
                  WHERE     numUserCntID = @numDomainId
                            AND DATEDIFF(DAY, dbo.GetUTCDateWithoutTime(),
                                         [dtDueDate]) BETWEEN 31
                                                      AND     60
                            AND dbo.GetUTCDateWithoutTime() <= [dtDueDate]
                  GROUP BY  numDivisionId
                ) X
        WHERE   #TempAPRecord1.numDivisionID = X.numDivisionID
        
        UPDATE  #TempAPRecord1
        SET     intSixtyDaysCount = 2
        WHERE   numDivisionId IN (
                SELECT DISTINCT
                        [numDivisionId]
                FROM    [#TempAPRecord]
                WHERE   numUserCntID = @numDomainId
                        AND DATEDIFF(DAY, dbo.GetUTCDateWithoutTime(),
                                     [dtDueDate]) BETWEEN 31
                                                  AND     60
                        AND dbo.GetUTCDateWithoutTime() <= [dtDueDate]
                        AND numCurrencyID <> @baseCurrency )

------------------------------------------
        UPDATE  #TempAPRecord1
        SET     numNinetyDays = X.numNinetyDays
        FROM    ( SELECT    SUM(DealAmount) AS numNinetyDays,
                            numDivisionId
                  FROM      #TempAPRecord
                  WHERE     numUserCntID = @numDomainId
                            AND DATEDIFF(DAY, dbo.GetUTCDateWithoutTime(),
                                         [dtDueDate]) BETWEEN 61
                                                      AND     90
                            AND dbo.GetUTCDateWithoutTime() <= [dtDueDate]
                  GROUP BY  numDivisionId
                ) X
        WHERE   #TempAPRecord1.numDivisionID = X.numDivisionID
        
         UPDATE  #TempAPRecord1
        SET     numNinetyDaysPaid = X.numNinetyDaysPaid
        FROM    ( SELECT    SUM(AmountPaid) AS numNinetyDaysPaid,
                            numDivisionId
                  FROM      #TempAPRecord
                  WHERE     numUserCntID = @numDomainId
                            AND DATEDIFF(DAY, dbo.GetUTCDateWithoutTime(),
                                         [dtDueDate]) BETWEEN 61
                                                      AND     90
                            AND dbo.GetUTCDateWithoutTime() <= [dtDueDate]
                  GROUP BY  numDivisionId
                ) X
        WHERE   #TempAPRecord1.numDivisionID = X.numDivisionID
        
        UPDATE  #TempAPRecord1
        SET     intNinetyDaysCount = 2
        WHERE   numDivisionId IN (
                SELECT DISTINCT
                        [numDivisionId]
                FROM    [#TempAPRecord]
                WHERE   numUserCntID = @numDomainId
                        AND DATEDIFF(DAY, dbo.GetUTCDateWithoutTime(),
                                     [dtDueDate]) BETWEEN 61
                                                  AND     90
                        AND dbo.GetUTCDateWithoutTime() <= [dtDueDate]
                        AND numCurrencyID <> @baseCurrency )

------------------------------------------
        UPDATE  #TempAPRecord1
        SET     numOverNinetyDays = X.numOverNinetyDays
        FROM    ( SELECT    SUM(DealAmount) AS numOverNinetyDays,
                            numDivisionId
                  FROM      #TempAPRecord
                  WHERE     numUserCntID = @numDomainId
                            AND [dtDueDate] >= DATEADD(DAY, 91,
                                                       dbo.GetUTCDateWithoutTime())
                            AND dbo.GetUTCDateWithoutTime() <= [dtDueDate]
                  GROUP BY  numDivisionId
                ) X
        WHERE   #TempAPRecord1.numDivisionID = X.numDivisionID
        
        UPDATE  #TempAPRecord1
        SET     numOverNinetyDaysPaid = X.numOverNinetyDaysPaid
        FROM    ( SELECT    SUM(AmountPaid) AS numOverNinetyDaysPaid,
                            numDivisionId
                  FROM      #TempAPRecord
                  WHERE     numUserCntID = @numDomainId
                            AND [dtDueDate] >= DATEADD(DAY, 91,
                                                       dbo.GetUTCDateWithoutTime())
                            AND dbo.GetUTCDateWithoutTime() <= [dtDueDate]
                  GROUP BY  numDivisionId
                ) X
        WHERE   #TempAPRecord1.numDivisionID = X.numDivisionID
        
        UPDATE  #TempAPRecord1
        SET     intOverNinetyDaysCount = 2
        WHERE   numDivisionId IN (
                SELECT DISTINCT
                        [numDivisionId]
                FROM    [#TempAPRecord]
                WHERE   numUserCntID = @numDomainId
                        AND [dtDueDate] >= DATEADD(DAY, 91,
                                                   dbo.GetUTCDateWithoutTime())
                        AND dbo.GetUTCDateWithoutTime() <= [dtDueDate]
                        AND numCurrencyID <> @baseCurrency )

------------------------------------------
        UPDATE  #TempAPRecord1
        SET     numThirtyDaysOverDue = X.numThirtyDaysOverDue
        FROM    ( SELECT    SUM(DealAmount) AS numThirtyDaysOverDue,
                            numDivisionId
                  FROM      #TempAPRecord
                  WHERE     numUserCntID = @numDomainId
                            AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
                            AND DATEDIFF(DAY, [dtDueDate],
                                         dbo.GetUTCDateWithoutTime()) BETWEEN 0
                                                                      AND     30
                  GROUP BY  numDivisionId
                ) X
        WHERE   #TempAPRecord1.numDivisionID = X.numDivisionID
        
         UPDATE  #TempAPRecord1
        SET     numThirtyDaysOverDuePaid = X.numThirtyDaysOverDuePaid
        FROM    ( SELECT    SUM(AmountPaid) AS numThirtyDaysOverDuePaid,
                            numDivisionId
                  FROM      #TempAPRecord
                  WHERE     numUserCntID = @numDomainId
                            AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
                            AND DATEDIFF(DAY, [dtDueDate],
                                         dbo.GetUTCDateWithoutTime()) BETWEEN 0
                                                                      AND     30
                  GROUP BY  numDivisionId
                ) X
        WHERE   #TempAPRecord1.numDivisionID = X.numDivisionID
        
        UPDATE  #TempAPRecord1
        SET     intThirtyDaysOverDueCount = 2
        WHERE   numDivisionId IN (
                SELECT DISTINCT
                        [numDivisionId]
                FROM    [#TempAPRecord]
                WHERE   numUserCntID = @numDomainId
                        AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
                        AND DATEDIFF(DAY, [dtDueDate],
                                     dbo.GetUTCDateWithoutTime()) BETWEEN 0
                                                                  AND     30
                        AND numCurrencyID <> @baseCurrency )

----------------------------------
        UPDATE  #TempAPRecord1
        SET     numSixtyDaysOverDue = X.numSixtyDaysOverDue
        FROM    ( SELECT    SUM(DealAmount) AS numSixtyDaysOverDue,
                            numDivisionId
                  FROM      #TempAPRecord
                  WHERE     numUserCntID = @numDomainId
                            AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
                            AND DATEDIFF(DAY, [dtDueDate],
                                         dbo.GetUTCDateWithoutTime()) BETWEEN 31
                                                                      AND     60
                  GROUP BY  numDivisionId
                ) X
        WHERE   #TempAPRecord1.numDivisionID = X.numDivisionID
        
          UPDATE  #TempAPRecord1
        SET     numSixtyDaysOverDuePaid = X.numSixtyDaysOverDuePaid
        FROM    ( SELECT    SUM(AmountPaid) AS numSixtyDaysOverDuePaid,
                            numDivisionId
                  FROM      #TempAPRecord
                  WHERE     numUserCntID = @numDomainId
                            AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
                            AND DATEDIFF(DAY, [dtDueDate],
                                         dbo.GetUTCDateWithoutTime()) BETWEEN 31
                                                                      AND     60
                  GROUP BY  numDivisionId
                ) X
        WHERE   #TempAPRecord1.numDivisionID = X.numDivisionID
        
        UPDATE  #TempAPRecord1
        SET     intSixtyDaysOverDueCount = 2
        WHERE   numDivisionId IN (
                SELECT DISTINCT
                        [numDivisionId]
                FROM    [#TempAPRecord]
                WHERE   numUserCntID = @numDomainId
                        AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
                        AND DATEDIFF(DAY, [dtDueDate],
                                     dbo.GetUTCDateWithoutTime()) BETWEEN 31
                                                                  AND     60
                        AND numCurrencyID <> @baseCurrency )
------------------------------------------
        UPDATE  #TempAPRecord1
        SET     numNinetyDaysOverDue = X.numNinetyDaysOverDue
        FROM    ( SELECT    SUM(DealAmount) AS numNinetyDaysOverDue,
                            numDivisionId
                  FROM      #TempAPRecord
                  WHERE     numUserCntID = @numDomainId
                            AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
                            AND DATEDIFF(DAY, [dtDueDate],
                                         dbo.GetUTCDateWithoutTime()) BETWEEN 61
                                                                      AND     90
                  GROUP BY  numDivisionId
                ) X
        WHERE   #TempAPRecord1.numDivisionID = X.numDivisionID
        
         UPDATE  #TempAPRecord1
        SET     numNinetyDaysOverDuePaid = X.numNinetyDaysOverDuePaid
        FROM    ( SELECT    SUM(AmountPaid) AS numNinetyDaysOverDuePaid,
                            numDivisionId
                  FROM      #TempAPRecord
                  WHERE     numUserCntID = @numDomainId
                            AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
                            AND DATEDIFF(DAY, [dtDueDate],
                                         dbo.GetUTCDateWithoutTime()) BETWEEN 61
                                                                      AND     90
                  GROUP BY  numDivisionId
                ) X
        WHERE   #TempAPRecord1.numDivisionID = X.numDivisionID
        
        UPDATE  #TempAPRecord1
        SET     intNinetyDaysOverDueCount = 2
        WHERE   numDivisionId IN (
                SELECT DISTINCT
                        [numDivisionId]
                FROM    [#TempAPRecord]
                WHERE   numUserCntID = @numDomainId
                        AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
                        AND DATEDIFF(DAY, [dtDueDate],
                                     dbo.GetUTCDateWithoutTime()) BETWEEN 61
                                                                  AND     90
                        AND numCurrencyID <> @baseCurrency )
------------------------------------------
        UPDATE  #TempAPRecord1
        SET     numOverNinetyDaysOverDue = X.numOverNinetyDaysOverDue
        FROM    ( SELECT    SUM(DealAmount) AS numOverNinetyDaysOverDue,
                            numDivisionId
                  FROM      #TempAPRecord
                  WHERE     numUserCntID = @numDomainId
                            AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
                            AND DATEDIFF(DAY, [dtDueDate],
                                         dbo.GetUTCDateWithoutTime()) > 90
                  GROUP BY  numDivisionId
                ) X
        WHERE   #TempAPRecord1.numDivisionID = X.numDivisionID
        
           UPDATE  #TempAPRecord1
        SET     numOverNinetyDaysOverDuePaid = X.numOverNinetyDaysOverDuePaid
        FROM    ( SELECT    SUM(AmountPaid) AS numOverNinetyDaysOverDuePaid,
                            numDivisionId
                  FROM      #TempAPRecord
                  WHERE     numUserCntID = @numDomainId
                            AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
                            AND DATEDIFF(DAY, [dtDueDate],
                                         dbo.GetUTCDateWithoutTime()) > 90
                  GROUP BY  numDivisionId
                ) X
        WHERE   #TempAPRecord1.numDivisionID = X.numDivisionID
        
        UPDATE  #TempAPRecord1
        SET     intOverNinetyDaysOverDueCount = 2
        WHERE   numDivisionId IN (
                SELECT DISTINCT
                        [numDivisionId]
                FROM    [#TempAPRecord]
                WHERE   numUserCntID = @numDomainId
                        AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
                        AND DATEDIFF(DAY, [dtDueDate],
                                     dbo.GetUTCDateWithoutTime()) > 90
                        AND numCurrencyID <> @baseCurrency )
             


UPDATE  #TempAPRecord1
SET     monUnAppliedAmount = ISNULL(X.monUnAppliedAmount,0)
FROM    (SELECT  SUM(ISNULL(monUnAppliedAmount,0)) AS monUnAppliedAmount,numDivisionId
FROM    #TempAPRecord
WHERE   numUserCntID =@numDomainId 
GROUP BY numDivisionId
) X WHERE   #TempAPRecord1.numDivisionID = X.numDivisionID

UPDATE  #TempAPRecord1 SET numTotal=  isnull(numThirtyDays, 0) + isnull(numThirtyDaysOverDue, 0)
                + isnull(numSixtyDays, 0) + isnull(numSixtyDaysOverDue, 0)
                + isnull(numNinetyDays, 0) + isnull(numNinetyDaysOverDue, 0)
                + isnull(numOverNinetyDays, 0)
                + isnull(numOverNinetyDaysOverDue, 0) 			
             
        SELECT  [numDivisionId],
                [numContactId],
                [tintCRMType],
                [vcCompanyName],
                [vcCustPhone],
                [vcContactName] as vcApName,
                [vcEmail],
                [numPhone] as vcPhone,
                [numThirtyDays],
                [numSixtyDays],
                [numNinetyDays],
                [numOverNinetyDays],
                [numThirtyDaysOverDue],
                [numSixtyDaysOverDue],
                [numNinetyDaysOverDue],
                [numOverNinetyDaysOverDue],
             ISNULL(numTotal,0) - ISNULL(monUnAppliedAmount,0) AS numTotal,
                numCompanyID,
                numDomainID,
                [intThirtyDaysCount],
                [intThirtyDaysOverDueCount],
                [intSixtyDaysCount],
                [intSixtyDaysOverDueCount],
                [intNinetyDaysCount],
                [intOverNinetyDaysCount],
                [intNinetyDaysOverDueCount],
                [intOverNinetyDaysOverDueCount],
                [numThirtyDaysPaid],
                [numSixtyDaysPaid],
                [numNinetyDaysPaid],
                [numOverNinetyDaysPaid],
                [numThirtyDaysOverDuePaid],
                [numSixtyDaysOverDuePaid],
                [numNinetyDaysOverDuePaid],
                [numOverNinetyDaysOverDuePaid],ISNULL(monUnAppliedAmount,0) AS monUnAppliedAmount
        FROM    #TempAPRecord1
        WHERE   numUserCntID = @numDomainId AND (numDivisionId = @numDivisionId OR @numDivisionId IS NULL)
        ORDER BY [vcCompanyName]

        drop table #TempAPRecord1
        drop table #TempAPRecord 

        SET NOCOUNT OFF   
    
    END
/****** Object:  StoredProcedure [dbo].[USP_GetAccountPayableAging_Details]    Script Date: 03/06/2009 00:30:26 ******/
    SET ANSI_NULLS ON

GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetBillPaymentDetails' ) 
    DROP PROCEDURE USP_GetBillPaymentDetails
GO
-- exec USP_GetBillPaymentDetails 0,1,0,0
CREATE PROCEDURE [dbo].[USP_GetBillPaymentDetails]
    @numBillPaymentID NUMERIC(18, 0) ,
    @numDomainID NUMERIC ,
    @ClientTimeZoneOffset INT,
    @numDivisionID NUMERIC=0,
    @numCurrencyID NUMERIC=0,
    @CurrentPage INT,
    @PageSize INT,
    @TotRecs INT OUTPUT,
    @numAccountClass NUMERIC(18,0)=0
AS 
    SET NOCOUNT ON
    SET TRANSACTION ISOLATION LEVEL READ COMMITTED
		DECLARE @numBaseCurrencyID NUMERIC
	    DECLARE @BaseCurrencySymbol nVARCHAR(3)
	    
        SELECT @numBaseCurrencyID = ISNULL(numCurrencyID,0) FROM dbo.Domain WHERE numDomainId=@numDomainId
        SELECT @BaseCurrencySymbol = ISNULL(varCurrSymbol,'') FROM dbo.Currency WHERE numCurrencyID = @numBaseCurrencyID
    
		DECLARE @firstRec AS INTEGER
		DECLARE @lastRec AS INTEGER
		
		SET @firstRec = ( @CurrentPage - 1 ) * @PageSize
		SET @lastRec = ( @CurrentPage * @PageSize + 1 )
		
				  
		IF @numBillPaymentID>0
		BEGIN
			SELECT @numDivisionID=numDivisionID FROM [dbo].[BillPaymentHeader] BPH WHERE BPH.numBillPaymentID=@numBillPaymentID
		END
		
	 SELECT BPH.numBillPaymentID,BPH.dtPaymentDate,BPH.numAccountID,BPH.numDivisionID,BPH.numReturnHeaderID,BPH.numPaymentMethod
	 ,ISNULL(BPH.monPaymentAmount,0) AS monPaymentAmount,ISNULL(BPH.monAppliedAmount,0) AS monAppliedAmount,
	 ISNULL(CH.numCheckHeaderID, 0) numCheckHeaderID,
	 ISNULL(CH.numCheckNo,0) AS numCheckNo,ISNULL(BPH.numReturnHeaderID,0) AS numReturnHeaderID,
	 BPH.numCurrencyID,ISNULL(NULLIF(BPH.fltExchangeRate,0),1) fltExchangeRateBillPayment
	,ISNULL(C.varCurrSymbol,'') varCurrSymbol
	,@BaseCurrencySymbol BaseCurrencySymbol,ISNULL(monRefundAmount,0) AS monRefundAmount,ISNULL(BPH.numAccountClass,0) AS numAccountClass
	  FROM  [dbo].[BillPaymentHeader] BPH
			LEFT JOIN dbo.CheckHeader CH ON CH.numReferenceID = BPH.numBillPaymentID AND CH.tintReferenceType = 8
			LEFT JOIN dbo.Currency C ON C.numCurrencyID = BPH.numCurrencyID
			WHERE BPH.numBillPaymentID=@numBillPaymentID
		
		--For Edit Bill Payment Get Details
		SELECT Row_number() OVER ( ORDER BY dtDueDate ASC ) AS row,* INTO #tempBillPayment FROM 
		(
			SELECT  Case When isnull(BH.bitLandedCost,0)=1 then  isnull(BH.numOppId,0) else OBD.numOppId end AS numoppid,
					  BPH.numDivisionID,BPD.[numOppBizDocsID],BPD.[numBillID],
					  CASE WHEN(BPD.numBillID>0) THEN  Case When isnull(BH.bitLandedCost,0)=1 THEN 'Laned Cost-' ELSE 'Bill-' END + CONVERT(VARCHAR(10),BH.numBillID)
					  else OBD.vcbizdocid END AS [Name],
					  CASE BPD.[tintBillType] WHEN 1 THEN OBD.monDealAmount WHEN 2 THEN BH.monAmountDue END monOriginalAmount,
					  CASE BPD.[tintBillType] WHEN 1 THEN ( OBD.monDealAmount - OBD.monAmountPaid + BPD.[monAmount]) WHEN 2 THEN BH.monAmountDue - BH.monAmtPaid + BPD.[monAmount] END monAmountDue,
					  CASE BPD.[tintBillType] WHEN 1 THEN '' WHEN 2 THEN BH.vcReference END Reference,
					  CASE BPD.[tintBillType] WHEN 1 THEN dbo.fn_GetComapnyName(OM.numDivisionId) WHEN 2 THEN dbo.fn_GetComapnyName(BH.numDivisionID) END vcCompanyName,
					  BPD.[tintBillType],
					  CASE BPD.[tintBillType]
						  WHEN 1
						  THEN CASE ISNULL(OM.bitBillingTerms, 0)
								 WHEN 1
								 --THEN [dbo].[FormatedDateFromDate](DATEADD(dd,CONVERT(INT, ISNULL(dbo.fn_GetListItemName(ISNULL(OM.intBillingDays,0)), 0)),OBD.dtFromDate),@numDomainId)
								 THEN [dbo].[FormatedDateFromDate](DATEADD(dd,CONVERT(INT, ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(OM.intBillingDays,0)), 0)),OBD.dtFromDate),@numDomainId)
								 WHEN 0
								 THEN [dbo].[FormatedDateFromDate](OBD.[dtFromDate],
																  @numDomainId)
							   END
						  WHEN 2
						  THEN [dbo].[FormatedDateFromDate](BH.dtDueDate,@numDomainID) END AS DueDate,
						  BPD.[monAmount] AS monAmountPaid,1 bitIsPaid
						  ,ISNULL(NULLIF(OM.fltExchangeRate,0),1) fltExchangeRateOfOrder
						  ,BH.dtDueDate 
				FROM    [dbo].[BillPaymentHeader] BPH
						INNER JOIN dbo.BillPaymentDetails BPD ON BPH.numBillPaymentID = BPD.numBillPaymentID
						LEFT JOIN dbo.OpportunityBizDocs OBD ON BPD.numOppBizDocsID = OBD.numOppBizDocsId
						LEFT JOIN dbo.OpportunityMaster OM ON OBD.numOppId = OM.numOppId
						LEFT JOIN dbo.BillHeader BH ON BH.numBillID = BPD.numBillID
						LEFT JOIN dbo.CheckHeader CH ON CH.numReferenceID = BPH.numBillPaymentID
														AND CH.tintReferenceType = 8
						                                                 
				WHERE   BPH.[numBillPaymentID] = @numBillPaymentID
				AND BPH.numDomainID = @numDomainID
				AND (OM.numCurrencyID = @numCurrencyID OR @numCurrencyID = 0 OR ISNULL(BPD.numOppBizDocsID,0) =0)
	            AND (ISNULL(BPH.numAccountClass,0)=@numAccountClass OR @numAccountClass=0)
	            
				UNION
		--PO Bills For Add Bill Payment
				SELECT  opp.numoppid AS numoppid ,
						opp.numdivisionid AS numDivisionID ,
						OBD.numOppBizDocsId AS numOppBizDocsId ,
						0 AS numBillID ,
						OBD.vcbizdocid AS [name] ,
						OBD.monDealAmount AS monOriginalAmount ,
						( OBD.monDealAmount - OBD.monAmountPaid ) AS monAmountDue ,
	--                    '' AS memo ,
						OBD.vcRefOrderNo AS reference ,
	--                    c.numcompanyid AS numcompanyid ,
						c.vccompanyname AS vccompanyname ,
						1 AS [tintBillType] , -- 1 =PO Bill,2=Regular bill , 3= Bill against liability
						CASE ISNULL(Opp.bitBillingTerms, 0)
						  WHEN 1
						  --THEN [dbo].[FormatedDateFromDate](DATEADD(dd,CONVERT(INT, ISNULL(dbo.fn_GetListItemName(ISNULL(Opp.intBillingDays,0)), 0)),OBD.dtFromDate),@numDomainId)
						  THEN [dbo].[FormatedDateFromDate](DATEADD(dd,CONVERT(INT, ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0)),OBD.dtFromDate),@numDomainId)
						  WHEN 0
						  THEN [dbo].[FormatedDateFromDate](OBD.[dtFromDate],
															@numDomainId)
						END AS DueDate ,
						'' AS monAmountPaid,
						0 bitIsPaid
						,ISNULL(NULLIF(Opp.fltExchangeRate,0),1) fltExchangeRateOfOrder
						,OBD.[dtFromDate] AS dtDueDate
				FROM    OpportunityBizDocs OBD
						INNER JOIN OpportunityMaster Opp ON OBD.numOppId = Opp.numOppId
						INNER JOIN AdditionalContactsInformation ADC ON Opp.numContactId = ADC.numContactId
						INNER JOIN DivisionMaster Div ON Opp.numDivisionId = Div.numDivisionID
														 AND ADC.numDivisionId = Div.numDivisionID
						INNER JOIN CompanyInfo C ON Div.numCompanyID = C.numCompanyId
				WHERE   Opp.numDomainId = @numDomainId
						AND OBD.bitAuthoritativeBizDocs = 1
						AND Opp.tintOppType = 2
						AND ( OBD.monDealAmount - OBD.monAmountPaid ) > 0
						AND (Opp.numDivisionId = @numDivisionID OR ISNULL(@numDivisionID,0)=0)
						AND OBD.numOppBizDocsID NOT IN (SELECT numOppBizDocsID FROM BillPaymentDetails WHERE numBillPaymentID = @numBillPaymentID)
						AND (Opp.numCurrencyID = @numCurrencyID OR @numCurrencyID = 0)
						AND (ISNULL(Opp.numAccountClass,0)=@numAccountClass OR @numAccountClass=0)
				 UNION       
				 --Regular Bills For Add Bill Payment
				SELECT  Case When isnull(BH.bitLandedCost,0)=1 then  isnull(BH.numOppId,0) else 0 end AS numoppid ,
						numDivisionID AS numDivisionID ,
						0 AS numOppBizDocsId ,
						BH.numBillID ,
						Case When isnull(BH.bitLandedCost,0)=1 THEN 'Laned Cost-' ELSE 'Bill-' END + CONVERT(VARCHAR(10),BH.numBillID) AS [name] ,
						BH.monAmountDue AS monOriginalAmount ,
						(BH.monAmountDue - BH.monAmtPaid) AS monAmountDue ,
	--                    '' AS memo ,
						BH.vcReference AS reference ,
	--                    c.numcompanyid AS numcompanyid ,
						dbo.fn_GetComapnyName(BH.numDivisionID) AS vccompanyname ,
						2 AS [tintBillType] , -- 1 =PO Bill,2=Regular bill , 3= Bill against liability
						[dbo].[FormatedDateFromDate](BH.dtDueDate,@numDomainId) AS DueDate ,
						'' AS monAmountPaid,
						0 bitIsPaid
						,ISNULL(BH.fltExchangeRate,1) fltExchangeRateOfOrder
						,BH.dtDueDate
				FROM    dbo.BillHeader BH
				WHERE   BH.numDomainId = @numDomainId
						AND BH.bitIsPaid = 0
						AND (BH.numDivisionId = @numDivisionID OR ISNULL(@numDivisionID,0)=0) 
						AND BH.numBillID NOT IN (SELECT numBillID FROM BillPaymentDetails WHERE numBillPaymentID = @numBillPaymentID)      
						AND (ISNULL(BH.numCurrencyID,@numBaseCurrencyID) = @numCurrencyID OR @numCurrencyID = 0)
						AND (ISNULL(BH.numAccountClass,0)=@numAccountClass OR @numAccountClass=0)
		) [BillPayment]

		SELECT  @TotRecs = COUNT(*)  FROM #tempBillPayment	
			                                                 	
		SELECT * FROM #tempBillPayment	
		WHERE  row > @firstRec 
		   AND row < @lastRec ;				

        DROP TABLE #tempBillPayment	    


GO


/****** Object:  StoredProcedure [dbo].[USP_GetCategory]    Script Date: 08/08/2009 16:37:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                        
-- exec [dbo].[USP_GetCategory] 0,0,'',0,1,3
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcategory')
DROP PROCEDURE usp_getcategory
GO
CREATE PROCEDURE [dbo].[USP_GetCategory]                        
@numCatergoryId as numeric(9),                        
@byteMode as tinyint ,                  
@str as varchar(1000),                  
@numDomainID as numeric(9)=0,
@numSiteID AS NUMERIC(9)=0 ,
@numCurrentPage as numeric(9)=0,
@PageSize AS INT=20,
@numTotalPage as numeric(9) OUT,    
@numItemCode AS NUMERIC(9,0),
@SortChar CHAR(1) = '0',
@numCategoryProfileID NUMERIC(18,0)
               
as   

set nocount on                     
--selecting all categories                     
declare @strsql as varchar(8000)
DECLARE @strRowCount VARCHAR(8000)  


create table #TempCategory
(PKId numeric(9) identity,
numItemCode numeric(9),
vcItemName varchar(250));

                  
if @byteMode=0                        
BEGIN
	 ;WITH CategoryList
As 
( SELECT C1.numCategoryID,C1.vcCategoryName,C1.vcCategoryNameURL,C1.numDepCategory,C1.tintLevel,case when C1.bitLink=0 then 'No' when C1.bitLink=1 then 'Yes' end as Link,C1.vcLink as vcLink,CAST('' AS VARCHAR(1000)) as DepCategory,
	C1.intDisplayOrder,C1.vcDescription,
	CASE WHEN LEN(ISNULL(C1.vcDescription ,'')) > 0 THEN  
	CASE WHEN LEN(C1.vcDescription) > 40 THEN SUBSTRING(C1.vcDescription,0,40) + ' [...]' 
         ELSE C1.vcDescription + ' [...]'	END 
    ELSE
		 ISNULL(C1.vcDescription ,'')   
    END     
         AS vcShortDescription,1 AS [Level],
	CAST (REPLICATE('.',1) + C1.vcCategoryName as varchar(1000) )  as vcNewCategoryName,
	CAST(C1.numCategoryID AS VARCHAR(MAX)) AS [Order]
 FROM dbo.Category C1 WHERE C1.numDepCategory=0 AND  C1.numDomainID=@numDomainID AND C1.numCategoryProfileID = @numCategoryProfileID
 UNION ALL
 SELECT  C1.numCategoryID,C1.vcCategoryName,C1.vcCategoryNameURL,C1.numDepCategory,C1.tintLevel,case when C1.bitLink=0 then 'No' when C1.bitLink=1 then 'Yes' end as Link,C1.vcLink as vcLink,CL.vcCategoryName as DepCategory ,
	C1.intDisplayOrder,C1.vcDescription,
	CASE WHEN LEN(ISNULL(C1.vcDescription ,'')) > 0 THEN  
	CASE WHEN LEN(C1.vcDescription) > 40 THEN SUBSTRING(C1.vcDescription,0,40) + ' [...]' 
         ELSE C1.vcDescription + ' [...]'	END 
    ELSE
		 ISNULL(C1.vcDescription ,'')   
    END     
         AS vcShortDescription,CL.[Level] + 1 AS [Level],
	CAST (REPLICATE('.',[Level] * 3) + C1.vcCategoryName as varchar(1000) )  as vcNewCategoryName,
	[Order] + '.' + CAST(C1.numCategoryID AS VARCHAR(MAX)) AS [Order]
 FROM dbo.Category C1 
 INNER JOIN CategoryList CL
 ON CL.numCategoryID = C1.numDepCategory  WHERE  C1.numDomainID=@numDomainID  AND C1.numCategoryProfileID = @numCategoryProfileID
)   

SELECT *,(SELECT COUNT(*) FROM [SiteCategories] SC WHERE SC.[numSiteID] =@numSiteID AND SC.[numCategoryID]= C1.[numCategoryID]) AS ShowInECommerce,
ISNULL((SELECT COUNT(*) FROM [ItemCategory] WHERE [numCategoryID] = C1.[numCategoryID]),0) AS ItemCount FROM CategoryList C1 ORDER BY [Order]
end                                                
--deleting a category                      
else if @byteMode=1                        
begin     
	delete from SiteCategories where numCategoryID=@numCatergoryId                     
	delete from ItemCategory where numCategoryID=@numCatergoryId
	delete from Category where numCategoryID=@numCatergoryId       
	select 1                        
end                        
--selecting all catories where level is 1                       
else if @byteMode=2                      
begin                        
	select numCategoryID,vcCategoryName,vcCategoryNameURL,intDisplayOrder,vcDescription from Category                      
	where ((tintlevel=1 )  or (tintlevel<>1 and numDepCategory=@numCatergoryId))  and numCategoryID!=@numCatergoryId                      
	and numDomainID=@numDomainID                    
	ORDER BY [vcCategoryName]
end                      
--selecting subCategories                      
else if @byteMode=3                      
begin                        
	select numCategoryID,vcCategoryName,vcCategoryNameURL,intDisplayOrder,vcDescription  from Category                      
	where numDepCategory=@numCatergoryId                      
	ORDER BY [vcCategoryName]
end                      
                      
--selecting Items                      
if @byteMode=4                      
begin                        
	select numItemCode,vcItemName  from Item           
	where numDomainID=@numDomainID                       
end                      
                      
--selecting Item belongs to a category                      
else if @byteMode=5                      
begin    


	insert into #TempCategory
	                       
	select numItemCode,CONCAT(numItemCode,' - ',vcItemName) AS vcItemName  from Item                      
	join ItemCategory                       
	on numItemCode=numItemID                      
	where numCategoryID=@numCatergoryId           
	and numDomainID=@numDomainID                       
	ORDER BY [vcItemName]


	select @numTotalPage =COUNT(*) from #TempCategory

	if @numTotalPage<@PageSize 
		begin
			set @numTotalPage=1
		end
	else	
		begin
			set @numTotalPage=@numTotalPage/@PageSize
		end

	select * from #TempCategory  where PKId between (@numCurrentPage*@PageSize)- (@PageSize-1) and  @numCurrentPage*@PageSize

	select @numTotalPage

end                      
                    
else if @byteMode=6                    
begin                        

	select C1.numCategoryID,c1.vcCategoryName,((select Count(*) from Category C2 where C2.numDepCategory=C1.numCategoryID)+(select count(*) from ItemCategory C3 where C3.numCategoryID=C1.numCategoryID  ) ) as [Count],convert(integer,bitLink) as Type,vcLink,C1.intDisplayOrder,
	C1.vcDescription   
	from Category C1                     
	where tintlevel=1                     
	and numDomainID=@numDomainID   
	AND numCategoryProfileID=@numCategoryProfileID                   
                    
end                     
                    
else if @byteMode=7                    
begin                        

	select C1.numCategoryID,c1.vcCategoryName,((select Count(*) from Category C2 where C2.numDepCategory=C1.numCategoryID)+(select count(*) from ItemCategory C3 where C3.numCategoryID=C1.numCategoryID ) ) as [Count],convert(integer,bitLink) as Type,vcLink,C1.intDisplayOrder,
	C1.vcDescription,numDepCategory as Category
	from Category C1                     
	where C1.numDepCategory=@numCatergoryId and numDomainID=@numDomainID                      
	union                     
	select numItemCode,vcItemName,0 ,2,'',0,'',0  from Item                      
	join ItemCategory                       
	on numItemCode=numItemID                      
	where numCategoryID=@numCatergoryId  and numDomainID=@numDomainID                    
end                     
                
else if @byteMode=8                     
begin                    
     --kishan               
	set @strsql='select numItemCode,vcItemName,txtItemDesc,0,
	(
	SELECT TOP 1 II.vcPathForTImage 
    FROM dbo.ItemImages II 
        WHERE II.numItemCode = Item.numItemCode 
    AND II.bitDefault = 1 , AND II.bitIsImage = 1 
    AND II.numDomainID ='+ cast(@numDomainId as varchar(50))+') 
	vcPathForTImage ,
	1 as Type,monListPrice as price,0 as numQtyOnHand  from Item                    
	join ItemCategory                       
	on numItemCode=numItemID                      
	where numCategoryID in('+@str+')'                  
	exec (@strsql)                   
end                    
                  
else if @byteMode=9                  
begin                    
                    
	select distinct(numItemCode),vcItemName,txtItemDesc,0 , (SELECT TOP 1 vcPathForImage FROM ItemImages WHERE numDomainId = @numDomainId AND bitDefault = 1 AND numItemCode = Item.numItemCode) AS vcPathForImage,1 as Type,monListPrice as price,0 as numQtyOnHand from Item                      
	join ItemCategory                       
	on numItemCode=numItemID                      
	where ( vcItemName like '%'+@str+'%' or txtItemDesc like '%'+@str+'%') and numDomainID= @numDomainID                                    
end        
      
else if @byteMode=10      
begin       
	select ROW_NUMBER() OVER (ORDER BY Category Asc) AS RowNumber,* from (select   C1.numCategoryID,c1.vcCategoryName,convert(integer,bitLink) as Type,vcLink ,numDepCategory as Category,C1.intDisplayOrder,
	C1.vcDescription         
	from Category C1                     
	where  numDomainID=@numDomainID)X       
      
end 


--selecting Items not present in Category                   
else if @byteMode=11                    
begin     

	insert into #TempCategory
	         
	SELECT I.numItemCode, CONCAT(I.numItemCode,' - ',I.vcItemName) AS vcItemName FROM item  I LEFT OUTER JOIN [ItemCategory] IC ON IC.numItemID = I.numItemCode
	WHERE [numDomainID] =@numDomainID AND ISNULL(IC.numCategoryID,0) <> @numCatergoryId
	ORDER BY [vcItemName]
          
--	select numItemCode,vcItemName  from Item LEFT OUTER JOIN [ItemCategory] ON Item.[numItemCode] = [ItemCategory].[numCategoryID]
--	where [ItemCategory].[numCategoryID] <> @numCatergoryId
----numItemCode not in (select numItemID from ItemCategory where numCategoryID=@numCatergoryId)
--	and numDomainID=@numDomainID                      
--	ORDER BY [vcItemName]



	select @numTotalPage =COUNT(*) from #TempCategory

	set @numTotalPage=@numTotalPage/@PageSize

	select * from #TempCategory  where PKId between (@numCurrentPage*@PageSize)-(@PageSize-1) and  @numCurrentPage*@PageSize

	select @numTotalPage


end   


--selecting Items From Item Groups                  
else if @byteMode=12                
begin   

	insert into #TempCategory
	                     
	select numItemCode,vcItemName  from Item 
	where numItemGroup= @numCatergoryId  
	ORDER BY [vcItemName] 



	select @numTotalPage =COUNT(*) from #TempCategory

	set @numTotalPage=@numTotalPage/@PageSize

	select * from #TempCategory  where PKId between (@numCurrentPage*@PageSize)-(@PageSize-1) and  @numCurrentPage*@PageSize

	select @numTotalPage

              
end

-- Selecting Categories Site Wise and Sorted into Sort Order 
else if @byteMode=13
BEGIN
	SELECT  
		@numCategoryProfileID=CPS.numCategoryProfileID
	FROM 
		Sites S
	JOIN
		CategoryProfileSites CPS
	ON
		S.numSiteID=CPS.numSiteID
	JOIN
		CategoryProfile CP
	ON
		CPS.numCategoryProfileID = CP.ID
	WHERE 
		S.numSiteID=@numSiteID


	DECLARE @bParentCatrgory BIT;
	SELECT @bParentCatrgory=vcAttributeValue FROM PageElementDetail WHERE [numSiteID] = @numSiteID AND numAttributeID=39
	SET @bParentCatrgory=ISNULL(@bParentCatrgory,0)

	DECLARE @tintDisplayCategory AS TINYINT
	SELECT @tintDisplayCategory = ISNULL(bitDisplayCategory,0) FROM dbo.eCommerceDTL WHERE numSiteID = @numSiteId

	
	IF ISNULL(@tintDisplayCategory,0) = 0 
	BEGIN
		SELECT C.numCategoryID,
			   C.vcCategoryName,
			   C.tintLevel,
			   convert(integer,C.bitLink) as TYPE,
			   C.vcLink AS vcLink,
			   numDepCategory as Category,C.intDisplayOrder,C.vcDescription 
		FROM   [SiteCategories] SC
			   INNER JOIN [Category] C ON SC.[numCategoryID] = C.[numCategoryID]
		WHERE  SC.[numSiteID] = @numSiteID
			   AND SC.[numCategoryID] = C.[numCategoryID] 
			   AND 1 = ( CASE WHEN @bParentCatrgory = 1 THEN tintLevel ELSE 1 END )
			   ORDER BY tintLevel , 
						ISNULL(C.intDisplayOrder,0),
						Category	
	END	
	ELSE
	BEGIN
		SELECT C.numCategoryID,
			   C.vcCategoryName,
			   C.tintLevel,
			   convert(integer,C.bitLink) as TYPE,
			   C.vcLink AS vcLink,
			   numDepCategory as Category,C.intDisplayOrder,C.vcDescription 
		FROM   [SiteCategories] SC
			   INNER JOIN [Category] C ON SC.[numCategoryID] = C.[numCategoryID]
		WHERE  SC.[numSiteID] = @numSiteID
			   AND SC.[numCategoryID] = C.[numCategoryID] 
			   AND 1 = ( CASE WHEN @bParentCatrgory = 1 THEN tintLevel ELSE 1 END )
			   AND (
					SC.numCategoryID IN (
										SELECT numCategoryID FROM dbo.ItemCategory 
										WHERE numItemID IN (
															SELECT numItemID FROM WareHouseItems WI
															INNER JOIN dbo.Item I ON I.numItemCode = WI.numItemID
															GROUP BY numItemID 
															HAVING SUM(WI.numOnHand) > 0
															UNION ALL
															SELECT numItemID FROM WareHouseItems WI
															INNER JOIN dbo.Item I ON I.numItemCode = WI.numItemID
															GROUP BY numItemID 
															HAVING SUM(WI.numAllocation) > 0
														   )
									   )
					OR tintLevel = 1
					)				   
			   ORDER BY tintLevel , 
						ISNULL(C.intDisplayOrder,0),
						Category
	END			

END

else if @byteMode=14
BEGIN

	SELECT C.numCategoryID,
		   C.vcCategoryName,
		   C.tintLevel,
		   convert(integer,C.bitLink) as TYPE,
		   C.vcLink AS vcLink,
		   numDepCategory as Category,C.intDisplayOrder,C.vcPathForCategoryImage,C.vcDescription 
	FROM   [SiteCategories] SC
		   INNER JOIN [Category] C
			 ON SC.[numCategoryID] = C.[numCategoryID]
	WHERE  SC.[numSiteID] = @numSiteID
		   AND SC.[numCategoryID] = C.[numCategoryID]
		   AND ISNULL(numDepCategory,0) = 0
		 ORDER BY ISNULL(C.intDisplayOrder,0),Category
END

else if @byteMode=15
BEGIN

DECLARE @bSubCatrgory BIT;

SELECT @bSubCatrgory=vcAttributeValue FROM PageElementDetail WHERE [numSiteID] = @numSiteID AND numAttributeID=40
SET @bSubCatrgory=ISNULL(@bSubCatrgory,0)

	SELECT C.numCategoryID,
		   C.vcCategoryName,
		   C.tintLevel,
		   convert(integer,C.bitLink) as TYPE,
		   C.vcLink AS vcLink,
		   numDepCategory as Category,C.intDisplayOrder,C.vcPathForCategoryImage,C.vcDescription 
		   
		   
	FROM   [SiteCategories] SC
		   INNER JOIN [Category] C
			 ON SC.[numCategoryID] = C.[numCategoryID]
	WHERE  SC.[numSiteID] = @numSiteID
		   AND SC.[numCategoryID] = C.[numCategoryID] AND numDepCategory=@numCatergoryId
		   AND 1=(CASE WHEN @bSubCatrgory=1 THEN 1 ELSE 0 END)
		    ORDER BY ISNULL(C.intDisplayOrder,0),Category
END

ELSE if @byteMode=16
BEGIN
 SELECT vcPathForCategoryImage FROM Category WHERE numCategoryID=@numCatergoryId AND numDomainID=@numDomainID
 
END
ELSE if @byteMode=17
BEGIN
 SELECT numCategoryID ,vcCategoryName, vcCategoryNameURL ,vcDescription,intDisplayOrder,vcPathForCategoryImage,numDepCategory,ISNULL(vcMetaTitle,'') vcMetaTitle,ISNULL(vcMetaKeywords,'') vcMetaKeywords,ISNULL(vcMetaDescription,'') vcMetaDescription FROM Category  WHERE numCategoryID = @numCatergoryId AND numDomainID=@numDomainID
END
ELSE IF @byteMode = 18
BEGIN
	SELECT
		*
	FROM
	(
		-- USED FOR HIERARCHICAL CATEGORY LIST
		SELECT 
			ROW_NUMBER() OVER ( ORDER BY vcCategoryName) AS RowNum,
			C1.numCategoryID,
			C1.vcCategoryName,
			C1.vcCategoryNameURL,
			NULLIF(numDepCategory,0) numDepCategory,
			ISNULL(numDepCategory,0) numDepCategory1,
			0 AS tintLevel,
			ISNULL((SELECT COUNT(*) FROM [ItemCategory] WHERE [numCategoryID] = C1.[numCategoryID]),0) AS ItemCount,
			ISNULL((SELECT COUNT(*) FROM [ItemCategory] WHERE [numCategoryID] IN (SELECT numCategoryID FROM category WHERE numDepCategory = C1.[numCategoryID])),0) AS ItemSubcategoryCount
		FROM 
			Category C1 
		WHERE 
			numDomainID =@numDomainID AND numCategoryProfileID = @numCategoryProfileID
	) TEMp
	ORDER BY
		vcCategoryName
END

ELSE IF @byteMode=19
BEGIN
	SELECT  
		@numCategoryProfileID=CPS.numCategoryProfileID
	FROM 
		Sites S
	JOIN
		CategoryProfileSites CPS
	ON
		S.numSiteID=CPS.numSiteID
	JOIN
		CategoryProfile CP
	ON
		CPS.numCategoryProfileID = CP.ID
	WHERE 
		S.numSiteID=@numSiteID

	BEGIN
	 ;WITH CategoryList
As 
( SELECT C1.numCategoryID,C1.vcCategoryName,C1.numDepCategory,C1.tintLevel,case when C1.bitLink=0 then 'No' when C1.bitLink=1 then 'Yes' end as Link,C1.vcLink as vcLink,CAST('' AS VARCHAR(1000)) as DepCategory,
	C1.intDisplayOrder,C1.vcDescription,
	CASE WHEN LEN(ISNULL(C1.vcDescription ,'')) > 0 THEN  
	CASE WHEN LEN(C1.vcDescription) > 40 THEN SUBSTRING(C1.vcDescription,0,40) + ' [...]' 
         ELSE C1.vcDescription + ' [...]'	END 
    ELSE
		 ISNULL(C1.vcDescription ,'')   
    END     
         AS vcShortDescription,1 AS [Level],
	CAST (REPLICATE('.',1) + C1.vcCategoryName as varchar(1000) )  as vcNewCategoryName,
	CAST(C1.numCategoryID AS VARCHAR(MAX)) AS [Order]
 FROM dbo.Category C1 WHERE C1.numDepCategory=0 AND  C1.numDomainID=@numDomainID AND C1.numCategoryProfileID=@numCategoryProfileID
 UNION ALL
 SELECT  C1.numCategoryID,C1.vcCategoryName,C1.numDepCategory,C1.tintLevel,case when C1.bitLink=0 then 'No' when C1.bitLink=1 then 'Yes' end as Link,C1.vcLink as vcLink,CL.vcCategoryName as DepCategory ,
	C1.intDisplayOrder,C1.vcDescription,
	CASE WHEN LEN(ISNULL(C1.vcDescription ,'')) > 0 THEN  
	CASE WHEN LEN(C1.vcDescription) > 40 THEN SUBSTRING(C1.vcDescription,0,40) + ' [...]' 
         ELSE C1.vcDescription + ' [...]'	END 
    ELSE
		 ISNULL(C1.vcDescription ,'')   
    END     
         AS vcShortDescription,CL.[Level] + 1 AS [Level],
	CAST (REPLICATE('.',[Level] * 3) + C1.vcCategoryName as varchar(1000) )  as vcNewCategoryName,
	[Order] + '.' + CAST(C1.numCategoryID AS VARCHAR(MAX)) AS [Order]
 FROM dbo.Category C1 
 INNER JOIN CategoryList CL
 ON CL.numCategoryID = C1.numDepCategory  WHERE  C1.numDomainID=@numDomainID AND C1.numCategoryProfileID=@numCategoryProfileID
)   

SELECT numCategoryID,vcCategoryName,(CASE WHEN  numDepCategory=0 THEN NULL ELSE numDepCategory END) AS numDepCategory ,tintLevel,Link,vcLink,DepCategory,(SELECT COUNT(*) FROM [SiteCategories] SC WHERE SC.[numSiteID] =@numSiteID AND SC.[numCategoryID]= C1.[numCategoryID]) AS ShowInECommerce,
ISNULL((SELECT COUNT(*) FROM [ItemCategory] WHERE [numCategoryID] = C1.[numCategoryID]),0) AS ItemCount FROM CategoryList C1 ORDER BY [Order]
END


DROP TABLE #TempCategory
END
/****** Object:  StoredProcedure [dbo].[usp_GetContactDTlPL]    Script Date: 07/26/2008 16:16:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By                                          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcontactdtlpl')
DROP PROCEDURE usp_getcontactdtlpl
GO
CREATE PROCEDURE [dbo].[usp_GetContactDTlPL]                                                              
@numContactID as numeric(9)=0  ,    
@numDomainID as numeric(9)=0   ,  
@ClientTimeZoneOffset as int                                    
--                                                            
AS                                                              
BEGIN                                                              
                                                              
  SELECT                                      
 A.numContactId,            
A.vcFirstName,           
A.vcLastName,                                                                   
 D.numDivisionID,             
 C.numCompanyId,            
 C.vcCompanyName,             
 D.vcDivisionName,             
 D.numDomainID,        
 D.tintCRMType,            
 A.numPhone,
 A.numPhoneExtension,
 A.vcEmail,  
 A.numTeam, 
 A.numTeam as vcTeam,         
[dbo].[GetListIemName](A.numTeam) as vcTeamName,                                                             
 A.vcFax,   
 A.numContactType as vcContactType,
 A.numContactType,         
[dbo].[GetListIemName](A.numContactType) as vcContactTypeName,                         
 A.charSex,             
 A.bintDOB,            
 dbo.GetAge(A.bintDOB, getutcdate()) as Age,                                                               
 [dbo].[GetListIemName]( A.vcPosition) as vcPositionName, 
 A.vcPosition,            
 A.txtNotes,             
 A.numCreatedBy,                                                              
 A.numCell,            
 A.NumHomePhone,            
 A.vcAsstFirstName,
 A.vcAsstLastName,            
 A.numAsstPhone,
 A.numAsstExtn,                                                              
 A.vcAsstEmail,            
 A.charSex, 
 A.vcLinkedinId,
  A.vcLinkedinUrl,
 case when A.charSex='M' then 'Male' when A.charSex='F' then 'Female' else  '-' end as charSexName,            
[dbo].[GetListIemName]( A.vcDepartment) as vcDepartmentName,  
A.vcDepartment,                                                             
--    case when AddC.vcPStreet is null then '' when AddC.vcPStreet='' then '' else AddC.vcPStreet + ',' end +             
-- case when AddC.vcPCity is null then '' when AddC.vcPCity='' then ''  else AddC.vcPCity end +             
-- case when  AddC.vcPPostalCode is null then '' when  AddC.vcPPostalCode='' then '' else ','+ AddC.vcPPostalCode end +              
-- case when dbo.fn_GetState(AddC.vcPState) is null then '' else  ','+ dbo.fn_GetState(AddC.vcPState) end +              
-- case when dbo.fn_GetListName(AddC.vcPCountry,0) ='' then '' else ',' + dbo.fn_GetListName(AddC.vcPCountry,0) end      
 dbo.getContactAddress(A.numContactId) as [Address],            
-- AddC.vcPState,            
-- AddC.vcPCountry,            
-- AddC.vcContactLocation,                                    
--  AddC.vcStreet,                                                               
--    AddC.vcCity,             
-- AddC.vcState,             
-- AddC.intPostalCode,                                                               
--  AddC.vcCountry,                                                              
    A.bitOptOut,                                                                         
 dbo.fn_GetContactName(a.numManagerId) as ManagerName,
 a.numManagerId   as   Manager,
[dbo].[GetListIemName](A.vcCategory) as vcCategoryName  ,     
 A.vcCategory,        
 dbo.fn_GetContactName(A.numCreatedBy)+' '+ convert(varchar(20),dateadd(minute,-@ClientTimeZoneOffset,A.bintCreatedDate)) CreatedBy,                                      
 dbo.fn_GetContactName(A.numModifiedBy)+' '+ convert(varchar(20),dateadd(minute,-@ClientTimeZoneOffset,A.bintModifiedDate)) ModifiedBy,            
 A.vcTitle,            
 A.vcAltEmail,                                                          
 dbo.fn_GetContactName(A.numRecOwner) as RecordOwner,
 A.numEmpStatus,                  
[dbo].[GetListIemName]( A.numEmpStatus) as numEmpStatusName,                                                          
(select  count(*) from GenericDocuments   where numRecID=A.numContactId and  vcDocumentSection='C') as DocumentCount,                          
(SELECT count(*)from CompanyAssociations where numDivisionID=A.numContactId and bitDeleted=0 ) as AssociateCountFrom,                            
(SELECT count(*)from CompanyAssociations where numAssociateFromDivisionID=A.numContactId and bitDeleted=0 ) as AssociateCountTo,
 ISNULL((SELECT vcECampName FROM [ECampaign] WHERE numECampaignID = A.numECampaignID),'') vcECampName,
 ISNULL(A.numECampaignID,0) numECampaignID,
 ISNULL( (SELECT MAX([numConEmailCampID]) FROM [ConECampaign] WHERE [numContactID] = A.numContactID AND [numECampaignID]= A.numECampaignID AND ISNULL(bitEngaged,0)=1),0) numConEmailCampID,
 ISNULL(A.bitPrimaryContact,0) AS bitPrimaryContact,D.vcPartnerCode AS vcPartnerCode,
 ISNULL(D.numPartenerSource,0) AS numPartenerSourceId,
 (D3.vcPartnerCode+'-'+C3.vcCompanyName) as numPartenerSource,
 ISNULL(D.numPartenerContact,0) AS numPartenerContactId,
 A1.vcFirstName+' '+A1.vcLastName AS numPartenerContact,
 ISNULL((SELECT vcPassword FROM ExtranetAccountsDtl WHERE numContactId=A.numContactId),'') AS vcPassword,ISNULL(A.vcTaxID,'') vcTaxID
 FROM AdditionalContactsInformation A INNER JOIN                                                              
 DivisionMaster D ON A.numDivisionId = D.numDivisionID INNER JOIN                                                              
 CompanyInfo C ON D.numCompanyID = C.numCompanyId  
  LEFT JOIN divisionMaster D3 ON D.numPartenerSource = D3.numDivisionID
  LEFT JOIN CompanyInfo C3 ON C3.numCompanyID = D3.numCompanyID    
  LEFT JOIN AdditionalContactsInformation A1 ON D.numPartenerContact = A1.numContactId                              
-- left join ContactAddress AddC on A.numContactId=AddC.numContactId                                  
 left join UserMaster U on U.numUserDetailId=A.numContactId                                                             
 WHERE A.numContactId = @numContactID and A.numDomainID=@numDomainID                                                     
END
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='Usp_GetDashboardAllowedReports')
DROP PROCEDURE Usp_GetDashboardAllowedReports
GO
CREATE PROCEDURE [dbo].[Usp_GetDashboardAllowedReports]        
@numDomainId as numeric(9) ,
@numGroupId as numeric(9),
@tintMode AS TINYINT=0
as                                                 
      
IF @tintMode=0 --All Custom Reports
BEGIN
	SELECT RLM.numReportID,RLM.vcReportName,RLM.vcReportDescription,RMM.vcModuleName,RMGM.vcGroupName,  
	CASE WHEN ISNULL(DAR.numReportID,0)=0 THEN convert(bit,'false') ELSE convert(bit,'true') END AS bitallowed
	from ReportListMaster RLM JOIN ReportModuleMaster RMM ON RLM.numReportModuleID=RMM.numReportModuleID
	JOIN ReportModuleGroupMaster RMGM ON RLM.numReportModuleGroupID=RMGM.numReportModuleGroupID                                                  
	LEFT join ReportDashboardAllowedReports DAR on RLM.numReportID=DAR.numReportID AND DAR.numDomainID=@numDomainId AND DAR.numGrpID =  @numGroupId AND DAR.tintReportCategory=0
	where RLM.numDomainID=@numDomainID
	order by RLM.numReportID
END    
ELSE IF @tintMode=1 --Only Allowed Custom Reports
BEGIN
	SELECT RLM.numReportID,RLM.vcReportName,RLM.vcReportDescription,RMM.vcModuleName,RMGM.vcGroupName,  
	CASE WHEN ISNULL(DAR.numReportID,0)=0 THEN convert(bit,'false') ELSE convert(bit,'true') END AS bitallowed
	from ReportListMaster RLM JOIN ReportModuleMaster RMM ON RLM.numReportModuleID=RMM.numReportModuleID
	JOIN ReportModuleGroupMaster RMGM ON RLM.numReportModuleGroupID=RMGM.numReportModuleGroupID                                                  
	join ReportDashboardAllowedReports DAR on RLM.numReportID=DAR.numReportID 
	where RLM.numDomainID=@numDomainID AND DAR.numDomainID=@numDomainId AND DAR.numGrpID =  @numGroupId AND DAR.tintReportCategory=0
	order by RLM.numReportID 
END   
ELSE IF @tintMode=2 --All KPI Groups Reports
BEGIN
	SELECT KPI.numReportKPIGroupID,KPI.vcKPIGroupName,KPI.vcKPIGroupDescription,
	CASE KPI.tintKPIGroupReportType WHEN 3 THEN 'KPI' WHEN 4 THEN 'ScoreCard' END AS vcKPIGroupReportType,
	CASE WHEN ISNULL(DAR.numReportID,0)=0 THEN convert(bit,'false') ELSE convert(bit,'true') END AS bitallowed
	from ReportKPIGroupListMaster KPI                                                 
	LEFT join ReportDashboardAllowedReports DAR on KPI.numReportKPIGroupID=DAR.numReportID AND DAR.numDomainID=@numDomainId AND DAR.numGrpID =  @numGroupId AND DAR.tintReportCategory=1
	where KPI.numDomainID=@numDomainID
	order by KPI.numReportKPIGroupID
END    
ELSE IF @tintMode=3 --Only Allowed KPI Groups Reports
BEGIN
	SELECT KPI.numReportKPIGroupID,KPI.vcKPIGroupName,KPI.vcKPIGroupDescription,
	CASE KPI.tintKPIGroupReportType WHEN 3 THEN 'KPI' WHEN 4 THEN 'ScoreCard' END AS vcKPIGroupReportType,
	CASE WHEN ISNULL(DAR.numReportID,0)=0 THEN convert(bit,'false') ELSE convert(bit,'true') END AS bitallowed
	from ReportKPIGroupListMaster KPI                                                  
	join ReportDashboardAllowedReports DAR on KPI.numReportKPIGroupID=DAR.numReportID 
	where KPI.numDomainID=@numDomainID AND DAR.numDomainID=@numDomainId AND DAR.numGrpID =  @numGroupId AND DAR.tintReportCategory=1
	order by KPI.numReportKPIGroupID 
END   
ELSE IF @tintMode=4 --Default Reports
BEGIN
	SELECT 
		RLM.numReportID
		,RLM.vcReportName
	FROM 
		ReportListMaster RLM 
	WHERE 
		ISNULL(RLM.bitDefault,0) = 1
	ORDER BY 
		RLM.numReportID 
END   
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetDataForIndexing' ) 
    DROP PROCEDURE USP_GetDataForIndexing
GO
-- exec USP_GetDataForIndexing @tintMode=0,@numDomainID=172,@numSiteID=90,@bitSchemaOnly=0,@tintAddUpdateMode=1,@bitDeleteAll=1
CREATE PROCEDURE USP_GetDataForIndexing
    @tintMode AS TINYINT = 0,
    @numDomainID AS NUMERIC,
    @numSiteID AS NUMERIC,
    @bitSchemaOnly AS BIT=0,
    @tintAddUpdateMode AS TINYINT, -- 1 = add , 2= update ,3=Deleted items
    @bitDeleteAll BIT=0 --Used for rebuilding all index from scratch
AS
BEGIN
    IF @tintMode = 0 -- items 
        BEGIN
        DECLARE @strSQL VARCHAR(4000)
        DECLARE @Fields VARCHAR(1000)
        DECLARE @numFldID VARCHAR(15)
        DECLARE @vcFldname VARCHAR(200)
        DECLARE @vcDBFldname VARCHAR(200)
        DECLARE @vcLookBackTableName VARCHAR(200)
        DECLARE @intRowNum INT
        DECLARE @strSQLUpdate VARCHAR(4000)
        DECLARE @Custom AS BIT
--SELECT * FROM View_DynamicColumns WHERE numFormid=30


--Select fields set for simple search from e-com settings as well fields from advance search
					SELECT  ROW_NUMBER() 
        OVER (ORDER BY vcFormFieldName) AS tintOrder,*
					INTO    #tempCustomField
					FROM    ( SELECT   distinct numFieldID as numFormFieldId,
										vcDbColumnName,
										vcFieldName as vcFormFieldName,
										0 AS Custom,
										vcLookBackTableName
							  FROM      View_DynamicColumns
							  WHERE     numFormId = 30
										AND numDomainID = @numDomainID
--										AND DFC.tintPageType = 1 --Commented so can agreegate all fields of simple and advance search
										AND isnull(numRelCntType,0) = 0
										AND isnull(bitCustom,0) = 0
										AND ISNULL(bitSettingField, 0) = 1
							  UNION
							  SELECT    numFieldID as numFormFieldId,
										vcFieldName,
										vcFieldName,
										1 AS Custom,
										'' vcLookBackTableName
							  FROM      View_DynamicCustomColumns
							  WHERE     Grp_id = 5
										AND numFormId = 30
										AND numDomainID = @numDomainID
										AND isnull(numRelCntType,0) = 0
--										AND isnull(DFC.bitCustom,0) = 1
--										AND ISNULL(DFC.tintPageType,0) = 1 --Commented so can agreegate all fields of simple and advance search

							) X
       
       IF @bitSchemaOnly =1
       BEGIN
			SELECT * FROM #tempCustomField
	   		RETURN
	   END
	   IF @bitDeleteAll = 1
	   BEGIN
	   		DELETE FROM LuceneItemsIndex WHERE numDomainID = @numDomainID AND numSiteID = @numSiteID
	   END
       
       --Create Fields query 
		 SET @Fields = '';
         SELECT TOP 1  @intRowNum = (tintOrder +1), @numFldID=numFormFieldId,@vcFldname=vcFormFieldName,@vcDBFldName=vcDbColumnName,@vcLookBackTableName=vcLookBackTableName
				FROM #tempCustomField WHERE Custom=0 ORDER BY tintOrder
				while @intRowNum>0                                                                                  
				 BEGIN
--					PRINT @numFldID
					IF(@vcDBFldName='numItemCode')
					set @vcDBFldName ='I.numItemCode'
				 
					SET @Fields = @Fields + ','  + @vcDBFldName + ' as ['  + @vcFldname + ']' 
					 

						SELECT TOP 1 @intRowNum = (tintOrder +1), @numFldID=numFormFieldId,@vcFldname=vcFormFieldName ,@vcDBFldName=vcDbColumnName,@vcLookBackTableName=vcLookBackTableName
						FROM #tempCustomField WHERE tintOrder >=@intRowNum AND Custom=0 ORDER BY tintOrder
							           
						if @@rowcount=0 set @intRowNum=0                                                                                          
				 END
       
--       PRINT @Fields
       
       
       SET @strSQL =  'SELECT ' + (CASE WHEN @tintAddUpdateMode = 1 THEN 'TOP 1000' ELSE '' END) + ' I.numItemCode AS numItemCode
			' + @Fields + '            
            INTO #DataForIndexing
            FROM    dbo.Item I 
            LEFT JOIN dbo.ItemExtendedDetails IED
            ON I.numItemCode = IED.numItemCode
            WHERE   I.numDomainID = ' + convert(varchar(10),@numDomainID) + '
                    AND I.numItemCode IN (
                    SELECT  numItemID
                    FROM    dbo.ItemCategory
                    WHERE ISNULL(I.IsArchieve,0) = 0 AND  numCategoryID IN ( SELECT   numCategoryID
                                               FROM     dbo.SiteCategories
                                               WHERE    numSiteID = ' + convert(varchar(10),@numSiteID) +' )) '
            
        IF @tintAddUpdateMode = 1 -- Add mode
			SET @strSQL = @strSQL +' and I.numItemCode Not in (SELECT numItemCode FROM LuceneItemsIndex WHERE numDomainID = '+ CONVERT(VARCHAR(10),@numDomainID) +  ' and numSiteID = ' + CONVERT(VARCHAR(10),@numSiteID) + ') '
		IF @tintAddUpdateMode = 2 -- Update mode
			SET @strSQL = @strSQL +' and bintModifiedDate BETWEEN  DATEADD( hour,-1,GETUTCDATE()) AND GETUTCDATE() 
			AND I.numItemCode in(SELECT I1.numItemCode FROM dbo.Item I1 INNER JOIN dbo.ItemCategory IC ON I1.numItemCode = IC.numItemID
			INNER JOIN dbo.SiteCategories SC ON IC.numCategoryID = SC.numCategoryID
			WHERE SC.numSiteID = ' + CONVERT(VARCHAR(10),@numSiteID) + ' AND I1.numDomainID= ' + CONVERT(VARCHAR(10),@numDomainID) + ')'
		IF @tintAddUpdateMode = 3 -- Delete mode
		BEGIN
			SET @strSQL = 'SELECT numItemCode FROM LuceneItemsIndex WHERE bitItemDeleted=1 and numDomainID = '+ CONVERT(VARCHAR(10),@numDomainID) +  ' and numSiteID = ' + CONVERT(VARCHAR(10),@numSiteID) 
			EXEC(@strSQL);
			RETURN
		END
			
       
     
       
       

				SET @strSQLUpdate=''
			--Add  custom Fields and data to table
				SELECT TOP 1  @intRowNum = (tintOrder +1), @numFldID=numFormFieldId,@vcFldname=vcFormFieldName
				FROM #tempCustomField WHERE Custom=1 ORDER BY tintOrder
				while @intRowNum>0                                                                                  
				 BEGIN
--				 PRINT @numFldID
					
				 			SET @strSQLUpdate = @strSQLUpdate + 'alter table #DataForIndexing add ['+@vcFldname+'_CustomField] varchar(500);'
							set @strSQLUpdate=@strSQLUpdate+ 'update #DataForIndexing set [' +@vcFldname + '_CustomField]=dbo.GetCustFldValueItem('+@numFldID+',numItemCode) where numItemCode>0'
							
					 

						SELECT TOP 1 @intRowNum = (tintOrder +1), @numFldID=numFormFieldId,@vcFldname=vcFormFieldName
						FROM #tempCustomField WHERE tintOrder >=@intRowNum AND Custom=1 ORDER BY tintOrder
							           
						if @@rowcount=0 set @intRowNum=0
				 END
				 SET @strSQLUpdate = @strSQLUpdate + ' SELECT * FROM #DataForIndexing'
				 
				 
				 IF @bitSchemaOnly=0
				 BEGIN
				 	SET @strSQLUpdate = @strSQLUpdate + ' INSERT INTO LuceneItemsIndex
				 	SELECT numItemCode,1,0,'+ CONVERT(VARCHAR(10), @numDomainID) +','+ CONVERT(VARCHAR(10), @numSiteID) +'  FROM #DataForIndexing where numItemCode not in (select numItemCode from LuceneItemsIndex where numDomainID ='+ CONVERT(VARCHAR(10), @numDomainID) +' and numSiteID = ' + CONVERT(VARCHAR(10),@numSiteID) + ' )'
				 END
				 
				 
				 SET @strSQLUpdate = @strSQLUpdate + ' DROP TABLE #DataForIndexing
												DROP TABLE #tempCustomField '
				 
				 SET @strSQL =@strSQL + @strSQLUpdate;
				 
				 
				 PRINT @strSQL
				 exec (@strSQL)
				 
				 
				 
				 
			
        END
        
	IF @tintMode= 1 
	BEGIN
		RETURN	
	END
	
	IF @tintMode= 2
	BEGIN
		RETURN	
	END
	
END
/****** Object:  StoredProcedure [dbo].[USP_GetDealsList1]    Script Date: 05/07/2009 22:12:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getdealslist1')
DROP PROCEDURE usp_getdealslist1
GO
CREATE PROCEDURE [dbo].[USP_GetDealsList1]
               @numUserCntID              NUMERIC(9)  = 0,
               @numDomainID               NUMERIC(9)  = 0,
               @tintSalesUserRightType    TINYINT  = 0,
               @tintPurchaseUserRightType TINYINT  = 0,
               @tintSortOrder             TINYINT  = 4,
               @CurrentPage               INT,
               @PageSize                  INT,
               @TotRecs                   INT  OUTPUT,
               @columnName                AS VARCHAR(50),
               @columnSortOrder           AS VARCHAR(10),
               @numCompanyID              AS NUMERIC(9)  = 0,
               @bitPartner                AS BIT  = 0,
               @ClientTimeZoneOffset      AS INT,
               @tintShipped               AS TINYINT,
               @intOrder                  AS TINYINT,
               @intType                   AS TINYINT,
               @tintFilterBy              AS TINYINT  = 0,
               @numOrderStatus			  AS NUMERIC,
			   @vcRegularSearchCriteria varchar(MAX)='',
			   @vcCustomSearchCriteria varchar(MAX)='',
			   @SearchText VARCHAR(300) = '',
			   @SortChar char(1)='0',
			   @tintDashboardReminderType TINYINT = 0
AS
	DECLARE  @PageId  AS TINYINT
	DECLARE  @numFormId  AS INT 
  
	SET @PageId = 0

	IF @inttype = 1
	BEGIN
		SET @PageId = 2
		SET @inttype = 3
		SET @numFormId=39
	END
	ELSE IF @inttype = 2
	BEGIN
		SET @PageId = 6
		SET @inttype = 4
		SET @numFormId=41
	END

	CREATE TABLE #tempForm 
	(
		tintOrder TINYINT,vcDbColumnName nvarchar(50),vcOrigDbColumnName nvarchar(50),vcFieldName nvarchar(50),vcAssociatedControlType nvarchar(50),vcListItemType varchar(3),
		numListID numeric(9),vcLookBackTableName varchar(50),bitCustomField BIT,numFieldId NUMERIC,bitAllowSorting BIT,bitAllowEdit BIT,bitIsRequired BIT,bitIsEmail BIT,
		bitIsAlphaNumeric BIT,bitIsNumeric BIT,bitIsLengthValidation BIT,intMaxLength int,intMinLength int,bitFieldMessage BIT,vcFieldMessage VARCHAR(500),
		ListRelID numeric(9),intColumnWidth int,bitAllowFiltering BIT,vcFieldDataType CHAR(1)
	)

	-- FIRST CHECK IF GRID COLUMNS ARE CONFIGURED
	DECLARE  @Nocolumns  AS TINYINT = 0

	SELECT 
		@Nocolumns=ISNULL(SUM(TotalRow),0) 
	FROM
	(            
		SELECT 
			COUNT(*) TotalRow 
		FROM 
			View_DynamicColumns 
		WHERE 
			numFormId=@numFormId 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND numRelCntType = @numFormId
		UNION 
		SELECT 
			COUNT(*) TotalRow 
		FROM 
			View_DynamicCustomColumns 
		WHERE 
			numFormId=@numFormId 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND numRelCntType = @numFormId
	) TotalRows

	-- IF GRID COLUMNS ARE NOT CONFIGURES THEN CHECK IF MASTER FORM CONFIGURATION IS CREATED BY ADMINISTRATOR
	DECLARE @IsMasterConfAvailable BIT = 0
	DECLARE @numUserGroup NUMERIC(18,0)

	IF @Nocolumns=0
	BEGIN
		SELECT @numUserGroup = numGroupID FROM UserMaster WHERE numUserDetailId = @numUserCntID 

		IF (SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = @numDomainID AND numFormID = @numFormId AND numRelCntType=@numFormId AND bitGridConfiguration = 1 AND numGroupID = @numUserGroup) > 0
		BEGIN
			SET @IsMasterConfAvailable = 1
		END
	END

	--IF MASTERCONFIGURATION IS AVAILABLE THEN LOAD IT OTHERWISE LOAD DEFAULT COLUMNS
	IF @IsMasterConfAvailable = 1
	BEGIN
		INSERT INTO DycFormConfigurationDetails 
		(
			numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth
		)
		SELECT 
			@numFormId,numFieldId,intColumnNum,intRowNum,@numDomainID,@numUserCntID,@numFormId,1,0,intColumnWidth
		FROM 
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId=@numFormId AND 
			View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicColumnsMasterConfig.numRelCntType=@numFormId AND 
			View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
			ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
		UNION
		SELECT 
			@numFormId,numFieldId,intColumnNum,intRowNum,@numDomainID,@numUserCntID,@numFormId,1,1,intColumnWidth
		FROM 
			View_DynamicCustomColumnsMasterConfig
		WHERE 
			View_DynamicCustomColumnsMasterConfig.numFormId=@numFormId AND 
			View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicCustomColumnsMasterConfig.numRelCntType=@numFormId AND 
			View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1


		INSERT INTO 
			#tempForm
		SELECT 
			(intRowNum + 1) as tintOrder, vcDbColumnName,	vcOrigDbColumnName,	ISNULL(vcCultureFieldName,vcFieldName), vcAssociatedControlType,
			vcListItemType, numListID, vcLookBackTableName,	bitCustom, numFieldId, bitAllowSorting,	bitInlineEdit, bitIsRequired,
			bitIsEmail,	bitIsAlphaNumeric, bitIsNumeric, bitIsLengthValidation,	intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,
			ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType 
		FROM 
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId=@numFormId AND 
			View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicColumnsMasterConfig.numRelCntType=@numFormId AND 
			View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
			ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
		UNION
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName, vcAssociatedControlType,'' as vcListItemType,numListID,'',
			bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,''
		FROM 
			View_DynamicCustomColumnsMasterConfig
		WHERE 
			View_DynamicCustomColumnsMasterConfig.numFormId=@numFormId AND 
			View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicCustomColumnsMasterConfig.numRelCntType=@numFormId AND 
			View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1
		ORDER BY 
			tintOrder ASC  
	END
	ELSE                                        
	BEGIN
		IF @Nocolumns=0
		BEGIN
			INSERT INTO DycFormConfigurationDetails 
			(
				numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth
			)
			SELECT 
				@numFormId,numFieldId,0,Row_number() over(order by tintRow desc),@numDomainID,@numUserCntID,@numFormId,1,0,intColumnWidth
			FROM 
				View_DynamicDefaultColumns
			WHERE 
				numFormId=@numFormId 
				AND bitDefault=1 
				AND ISNULL(bitSettingField,0)=1 
				AND numDomainID=@numDomainID
			ORDER BY 
				tintOrder asc   
		END

		INSERT INTO 
			#tempForm
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcOrigDbColumnName,ISNULL(vcCultureFieldName,vcFieldName),                  
			vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName,bitCustom,numFieldId,bitAllowSorting,bitInlineEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength,bitFieldMessage,
			vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType  
		FROM 
			View_DynamicColumns 
		WHERE 
			numFormId=@numFormId 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND ISNULL(bitSettingField,0)=1 
			AND ISNULL(bitCustom,0)=0  
			AND numRelCntType = @numFormId
		UNION
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName,vcAssociatedControlType,'' as vcListItemType,numListID,'',bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,'' 
		FROM 
			View_DynamicCustomColumns
		WHERE 
			numFormId=@numFormId 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND ISNULL(bitCustom,0)=1 
			AND numRelCntType = @numFormId
		ORDER BY 
			tintOrder asc  
	END

	DECLARE  @strColumns AS VARCHAR(MAX)
	SET @strColumns = ' ADC.numContactId, Div.numDivisionID,ISNULL(Div.numTerID,0) numTerID, opp.numRecOwner as numRecOwner, Div.tintCRMType, opp.numOppId, opp.monDealAmount, ISNULL(opp.intUsedShippingCompany,0) AS intUsedShippingCompany'


	DECLARE  @tintOrder  AS TINYINT;SET @tintOrder = 0
	DECLARE  @vcFieldName  AS VARCHAR(50)
	DECLARE  @vcListItemType  AS VARCHAR(3)
	DECLARE  @vcListItemType1  AS VARCHAR(1)
	DECLARE  @vcAssociatedControlType VARCHAR(30)
	DECLARE  @numListID  AS NUMERIC(9)
	DECLARE  @vcDbColumnName VARCHAR(40)
	DECLARE  @WhereCondition VARCHAR(2000);SET @WhereCondition = ''
	DECLARE  @vcLookBackTableName VARCHAR(2000)
	DECLARE  @bitCustom  AS BIT
	Declare @numFieldId as numeric  
	DECLARE @bitAllowSorting AS CHAR(1)   
	DECLARE @bitAllowEdit AS CHAR(1)                 
	DECLARE @vcColumnName AS VARCHAR(500)          
	
	Declare @ListRelID as numeric(9) 
	DECLARE @SearchQuery AS VARCHAR(MAX) = ''        

	SELECT TOP 1 
		@tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName,@vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,
		@numListID=numListID,@vcLookBackTableName=vcLookBackTableName ,@bitCustom=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,@bitAllowEdit=bitAllowEdit,
		@ListRelID=ListRelID
	FROM 
		#tempForm 
	ORDER BY 
		tintOrder ASC            
--	SET @strColumns = @strColumns + ' , (SELECT SUBSTRING((SELECT '', '' + cast(I.vcItemName as varchar)
--                      FROM OpportunityItems AS t
--					  LEFT JOIN OpportunityBizDocs as BC
--					  ON t.numOppId=BC.numOppId
--LEFT JOIN Item as I
--ON t.numItemCode=I.numItemCode
--WHERE t.numOppId=Opp.numOppId AND t.numoppItemtCode NOT IN (SELECT numOppItemId FROM OpportunityBizDocItems WHERE numOppBizDocID=BC.numOppBizDocsId)
--                     FOR XML PATH('''')), 2, 200000)  )[List_Item_Approval_UNIT] '
--SET @strColumns = @strColumns + ' , (SELECT 
-- SUBSTRING((SELECT '', '' + cast(I.vcItemName as varchar)
--FROM 
-- OpportunityItems AS t
--LEFT JOIN 
-- Item as I
--ON 
-- t.numItemCode=I.numItemCode
--WHERE 
-- t.numOppId=Opp.numOppId AND t.numoppItemtCode NOT IN (SELECT numOppItemId FROM OpportunityBizDocs JOIN OpportunityBizDocItems ON OpportunityBizDocs.numOppBizDocsId=OpportunityBizDocItems.numOppBizDocID  WHERE OpportunityBizDocs.numOppId=Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1)
--                     FOR XML PATH('''')), 2, 200000)  )[List_Item_Approval_UNIT] '
	WHILE @tintOrder > 0
    BEGIN
		IF @bitCustom = 0
		BEGIN
			DECLARE  @Prefix  AS VARCHAR(5)

			IF @vcLookBackTableName = 'AdditionalContactsInformation'
				SET @Prefix = 'ADC.'
			IF @vcLookBackTableName = 'DivisionMaster'
				SET @Prefix = 'Div.'
			IF @vcLookBackTableName = 'OpportunityMaster'
				SET @PreFix = 'Opp.'
			IF @vcLookBackTableName = 'OpportunityRecurring'
				SET @PreFix = 'OPR.'
			IF @vcLookBackTableName = 'CompanyInfo'
				SET @PreFix = 'cmp.'
			IF @vcLookBackTableName = 'ProjectProgress'
				SET @PreFix = 'PP'
			SET @vcColumnName= @vcDbColumnName+'~'+ convert(varchar(10),@numFieldId) +'~0'

			IF @vcAssociatedControlType = 'SelectBox'
			BEGIN
				IF @vcDbColumnName = 'numPartner'
				BEGIN
					SET @strColumns=@strColumns+ ' ,(Opp.numPartner) '+' ['+ @vcColumnName+']'    
					SET @strColumns=@strColumns+ ' ,(SELECT D.vcPartnerCode+''-''+C.vcCompanyName FROM DivisionMaster AS D LEFT JOIN CompanyInfo AS C ON D.numCompanyID=C.numCompanyID WHERE D.numDivisionID=Opp.numPartner) AS vcPartner' 
				END
				IF @vcDbColumnName = 'numPartenerContact'
				BEGIN
					SET @strColumns=@strColumns+ ' ,(Opp.numPartenerContact) '+' ['+ @vcColumnName+']'    
					SET @strColumns=@strColumns+ ' ,(SELECT A.vcFirstName+'' ''+ A.vcLastName FROM AdditionalContactsInformation AS A WHERE A.numContactId=Opp.numPartenerContact) AS vcPartenerContact' 
				END
				IF @vcDbColumnName = 'numStatus'
				BEGIN
					SET @strColumns=@strColumns+ ' ,(SELECT COUNT(OpportunityItems.numoppitemtCode) FROM OpportunityItems 
WHERE OpportunityItems.bitItemPriceApprovalRequired=1 AND OpportunityItems.numOppId=Opp.numOppId) ApprovalMarginCount'
				END
				IF @vcDbColumnName = 'tintSource'
				BEGIN
					SET @strColumns = @strColumns + ',ISNULL(dbo.fn_GetOpportunitySourceValue(ISNULL(Opp.tintSource,0),ISNULL(Opp.tintSourceType,0),Opp.numDomainID),''Internal Order'')' + ' [' + @vcColumnName + ']'

					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'ISNULL(dbo.fn_GetOpportunitySourceValue(ISNULL(Opp.tintSource,0),ISNULL(Opp.tintSourceType,0),Opp.numDomainID),''Internal Order'') LIKE ''%' + @SearchText + '%'''
					END
				END
				ELSE IF @vcDbColumnName = 'numCampainID'
				BEGIN
					SET @strColumns = @strColumns + ', (SELECT vcCampaignName FROM CampaignMaster WHERE numCampaignID= Opp.numCampainID) ' + ' [' + @vcColumnName + ']'
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'ISNULL((SELECT vcCampaignName FROM CampaignMaster WHERE numCampaignID= Opp.numCampainID),'''') LIKE ''%' + @SearchText + '%'''
					END
				END
				ELSE IF @vcDbColumnName = 'numContactID'
				BEGIN
					SET @strColumns = @strColumns + ',dbo.fn_GetContactName(' + @Prefix + @vcDbColumnName + ') [' + @vcColumnName + ']'
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'dbo.fn_GetContactName(' + @Prefix + @vcDbColumnName + ') LIKE ''%' + @SearchText + '%'''
					END
				END
				ELSE IF @vcListItemType = 'LI'
				BEGIN
					SET @strColumns = @strColumns + ',L' + CONVERT(VARCHAR(3),@tintOrder) + '.vcData' + ' [' + @vcColumnName + ']'
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'L' + CONVERT(VARCHAR(3),@tintOrder) + '.vcData LIKE ''%' + @SearchText + '%'''
					END

					SET @WhereCondition = @WhereCondition + ' left Join ListDetails L' + CONVERT(VARCHAR(3),@tintOrder) + ' on L' + CONVERT(VARCHAR(3),@tintOrder) + '.numListItemID=' + @vcDbColumnName
				END
				ELSE IF @vcListItemType = 'S'
				BEGIN
					SET @strColumns = @strColumns + ',S' + CONVERT(VARCHAR(3),@tintOrder) + '.vcState' + ' [' + @vcColumnName + ']'
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'S' + CONVERT(VARCHAR(3),@tintOrder) + '.vcState LIKE ''%' + @SearchText + '%'''
					END

					SET @WhereCondition = @WhereCondition + ' left join State S' + CONVERT(VARCHAR(3),@tintOrder) + ' on S' + CONVERT(VARCHAR(3),@tintOrder) + '.numStateID=' + @vcDbColumnName
				END
				ELSE IF @vcListItemType = 'BP'
				BEGIN
					SET @strColumns = @strColumns + ',SPLM.Slp_Name' + ' [' + @vcColumnName + ']'
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'SPLM.Slp_Name LIKE ''%' + @SearchText + '%'''
					END

					SET @WhereCondition = @WhereCondition + ' LEFT JOIN Sales_process_List_Master SPLM on SPLM.Slp_Id = OPP.numBusinessProcessID '
				END
				ELSE IF @vcListItemType = 'PP'
				BEGIN
					SET @strColumns = @strColumns + ',ISNULL(PP.intTotalProgress,0)' + ' [' + @vcColumnName + ']'

					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'ISNULL(PP.intTotalProgress,0) LIKE ''%' + @SearchText + '%'''
					END
				END
				ELSE IF @vcListItemType = 'T'
				BEGIN
					SET @strColumns = @strColumns + ',L' + CONVERT(VARCHAR(3),@tintOrder) + '.vcData' + ' [' + @vcColumnName + ']'
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'L' + CONVERT(VARCHAR(3),@tintOrder) + '.vcData LIKE ''%' + @SearchText + '%'''
					END

					SET @WhereCondition = @WhereCondition + ' left Join ListDetails L' + CONVERT(VARCHAR(3),@tintOrder) + ' on L' + CONVERT(VARCHAR(3),@tintOrder) + '.numListItemID=' + @vcDbColumnName
				END
				ELSE IF @vcListItemType = 'U'
				BEGIN
					SET @strColumns = @strColumns + ',dbo.fn_GetContactName(' + @Prefix + @vcDbColumnName + ') [' + @vcColumnName + ']'
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'dbo.fn_GetContactName(' + @Prefix + @vcDbColumnName + ') LIKE ''%' + @SearchText + '%'''
					END
				END
				ELSE IF @vcListItemType = 'WI'
				BEGIN
					SET @strColumns = @strColumns + ', Case when isnull(OL.numSiteID,0)>0 then Sites.vcSiteName else WebAPI.vcProviderName end [' + @vcColumnName + ']'
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + '(Case when isnull(OL.numSiteID,0)>0 then Sites.vcSiteName else WebAPI.vcProviderName end) LIKE ''%' + @SearchText + '%'''
					END

					set @WhereCondition= @WhereCondition +' left JOIN OpportunityLinking OL on OL.numChildOppID=Opp.numOppId left join WebAPI on WebAPI.WebApiId=isnull(OL.numWebApiId,0)
													left join Sites on isnull(OL.numSiteID,0)=Sites.numSIteID and Sites.numDomainID=' + convert(varchar(15),@numDomainID)                                                       
				END 
            END
			ELSE IF @vcAssociatedControlType = 'DateField'
			BEGIN
				IF @Prefix ='OPR.'
				BEGIN
					SET @strColumns = @strColumns + ', (SELECT TOP 1 dbo.FormatedDateFromDate( DateAdd(minute,+'+convert(varchar(15),-@ClientTimeZoneOffset)+', dtRecurringDate)'+ ','+ CONVERT(VARCHAR(10),@numDomainId) +') FROM  dbo.OpportunityRecurring WHERE numOppID=OPP.numOppID AND ISNULL(numOppBizDocID,0)=0 and dtRecurringDate > dbo.GetUTCDateWithoutTime()) AS  ['+ @vcColumnName + ']'

					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + '(SELECT TOP 1 dbo.FormatedDateFromDate( DateAdd(minute,+'+convert(varchar(15),-@ClientTimeZoneOffset)+', dtRecurringDate)'+ ','+ CONVERT(VARCHAR(10),@numDomainId) +') FROM  dbo.OpportunityRecurring WHERE numOppID=OPP.numOppID AND ISNULL(numOppBizDocID,0)=0 and dtRecurringDate > dbo.GetUTCDateWithoutTime()) LIKE ''%' + @SearchText + '%'''
					END
				END
				ELSE IF @vcDbColumnName = 'dtReleaseDate'
				BEGIN
					SET @strColumns = @strColumns
                                + ',case when convert(varchar(11),'
                                + @Prefix
                                + @vcDbColumnName
                                + ')= convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',getdate())) then ''<b><font color=red>Today</font></b>'''
					SET @strColumns = @strColumns
									+ 'when convert(varchar(11),'
									+ @Prefix
									+ @vcDbColumnName
									+ ')= convert(varchar(11),dateadd(day,-1,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',getdate()))) then ''<b><font color=purple>Yesterday</font></b>'''
					SET @strColumns = @strColumns
									+ 'when convert(varchar(11),'
									+ @Prefix
									+ @vcDbColumnName
									+ ')= convert(varchar(11),dateadd(day,1,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',getdate()))) then''<b><font color=orange>Tommorow</font></b>'' '
					SET @strColumns = @strColumns
									+ 'else dbo.FormatedDateFromDate('
									+ @Prefix
									+ @vcDbColumnName
									+ ','
									+ CONVERT(VARCHAR(10),@numDomainId)
									+ ') end  ['
									+ @vcColumnName + ']' 	

					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + @Prefix + @vcDbColumnName + ' LIKE ''%' + @SearchText + '%'''
					END

				END
				ELSE
				BEGIN
					SET @strColumns = @strColumns
                                + ',case when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+','
                                + @Prefix
                                + @vcDbColumnName
                                + ') )= convert(varchar(11),getdate()) then ''<b><font color=red>Today</font></b>'''
					SET @strColumns = @strColumns
									+ 'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+','
									+ @Prefix
									+ @vcDbColumnName
									+ ') )= convert(varchar(11),dateadd(day,-1,getdate())) then ''<b><font color=purple>Yesterday</font></b>'''
					SET @strColumns = @strColumns
									+ 'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+','
									+ @Prefix
									+ @vcDbColumnName
									+ ') )= convert(varchar(11),dateadd(day,1,getdate())) then''<b><font color=orange>Tommorow</font></b>'' '
					SET @strColumns = @strColumns
									+ 'else dbo.FormatedDateFromDate('
									+ @Prefix
									+ @vcDbColumnName
									+ ','
									+ CONVERT(VARCHAR(10),@numDomainId)
									+ ') end  ['
									+ @vcColumnName + ']' 	

					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + @Prefix + @vcDbColumnName + ' LIKE ''%' + @SearchText + '%'''
					END
				 END	
            END
            ELSE
            IF @vcAssociatedControlType = 'TextBox'
            BEGIN
				
                SET @strColumns = @strColumns
                                  + ','
                                  + CASE 
                                      WHEN @vcDbColumnName = 'numAge' THEN 'year(getutcdate())-year(bintDOB)'
                                      WHEN @vcDbColumnName = 'monPAmount' THEN 'isnull((SELECT varCurrSymbol FROM [Currency] WHERE numCurrencyID = Opp.[numCurrencyID]),'''') + '' ''+ CONVERT(VARCHAR,CONVERT(MONEY,monPAmount),1)'
                                      WHEN @vcDbColumnName = 'numDivisionID' THEN 'Div.numDivisionID'
                                      WHEN @vcDbColumnName = 'tintOppStatus' THEN 'CASE tintOppStatus WHEN 0 then ''Open'' WHEN 1 THEN ''Closed-Won'' WHEN 2 THEN ''Closed-Lost'' END '
                                      WHEN @vcDbColumnName = 'tintOppType' THEN 'CASE tintOppType WHEN 1 then ''Sales'' WHEN 2 THEN ''Purchase'' Else ''NA'' END '
                                      WHEN @vcDbColumnName = 'vcPOppName' THEN 'Opp.vcPOppName'
									  WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'vcBizDocsList' THEN 'ISNULL((SELECT SUBSTRING((SELECT ''$^$'' + CAST(numOppBizDocsId AS VARCHAR(18)) +''#^#''+ vcBizDocID
										FROM OpportunityBizDocs WHERE numOppId=Opp.numOppId FOR XML PATH('''')),4,200000)),'''')'
                                      ELSE @Prefix + @vcDbColumnName
                                    END
                                  + ' ['
                                  + @vcColumnName + ']'
				IF(@vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'vcBizDocsList')
				BEGIN
					SET @strColumns=@strColumns+','+' (SELECT COUNT(I.vcItemName) FROM 
													 OpportunityItems AS t LEFT JOIN  Item as I ON 
													 t.numItemCode=I.numItemCode WHERE 
													 t.numOppId=Opp.numOppId AND t.numoppItemtCode NOT IN 
													 (SELECT numOppItemId FROM OpportunityBizDocs JOIN 
													 OpportunityBizDocItems ON OpportunityBizDocs.numOppBizDocsId=OpportunityBizDocItems.numOppBizDocID  
													 WHERE OpportunityBizDocs.numOppId=Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1)) AS [List_Item_Approval_UNIT] '
				END
				
				IF LEN(@SearchText) > 0
				BEGIN
					SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + (CASE 
                                      WHEN @vcDbColumnName = 'numAge' THEN 'year(getutcdate())-year(bintDOB)'
                                      WHEN @vcDbColumnName = 'monPAmount' THEN 'isnull((SELECT varCurrSymbol FROM [Currency] WHERE numCurrencyID = Opp.[numCurrencyID]),'''') + '' ''+ CONVERT(VARCHAR,CONVERT(MONEY,monPAmount),1)'
                                      WHEN @vcDbColumnName = 'numDivisionID' THEN 'Div.numDivisionID'
                                      WHEN @vcDbColumnName = 'tintOppStatus' THEN 'CASE tintOppStatus WHEN 0 then ''Open'' WHEN 1 THEN ''Closed-Won'' WHEN 2 THEN ''Closed-Lost'' END '
                                      WHEN @vcDbColumnName = 'tintOppType' THEN 'CASE tintOppType WHEN 1 then ''Sales'' WHEN 2 THEN ''Purchase'' Else ''NA'' END '
                                      WHEN @vcDbColumnName = 'vcPOppName' THEN 'Opp.vcPOppName'
									  WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'vcBizDocsList' THEN 'ISNULL((SELECT SUBSTRING((SELECT ''$^$'' + CAST(numOppBizDocsId AS VARCHAR(18)) +''#^#''+ vcBizDocID
										FROM OpportunityBizDocs WHERE numOppId=Opp.numOppId FOR XML PATH('''')),4,200000)),'''')'
                                      ELSE @Prefix + @vcDbColumnName
                                    END) + ' LIKE ''%' + @SearchText + '%'''
				END
            END
            ELSE
            IF @vcAssociatedControlType = 'TextArea'
            BEGIN
				SET @strColumns=@strColumns+','+  @Prefix + @vcDbColumnName +' ['+ @vcColumnName+']' 

				IF LEN(@SearchText) > 0
				BEGIN
					SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + @Prefix + @vcDbColumnName + ' LIKE ''%' + @SearchText + '%'''
				END
            END
            ELSE IF  @vcAssociatedControlType='Label'                                              
			BEGIN
				SET @strColumns=@strColumns + ',' + CASE                    
				WHEN @vcDbColumnName = 'CalAmount' THEN 'Opp.monDealAmount'--'[dbo].[getdealamount](Opp.numOppId,Getutcdate(),0)'
				WHEN @vcDbColumnName = 'vcInventoryStatus' THEN 'ISNULL(dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID),'''')'
				WHEN @vcDbColumnName = 'vcOrderedShipped' THEN 'dbo.GetOrderShipReceiveStatus(Opp.numOppId,1,Opp.tintshipped)'
				WHEN @vcDbColumnName = 'vcOrderedReceived' THEN 'dbo.GetOrderShipReceiveStatus(Opp.numOppId,2,Opp.tintshipped)'
				WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'monDealAmount' THEN 'ISNULL((SELECT SUM(ISNULL(monDealAmount,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)'
				WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'monAmountPaid' THEN 'ISNULL((SELECT SUM(ISNULL(monAmountPaid,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)'
				else @Prefix + @vcDbColumnName END +' ['+ @vcColumnName+']'        
				
				IF @vcDbColumnName='vcOrderedShipped'
				BEGIN
				DECLARE @temp VARCHAR(MAX)
				SET @temp = '(SELECT 
				STUFF((SELECT '', '' + CAST((CAST(numOppBizDocsId AS varchar)+''~''+CAST(ISNULL(numShippingReportId,0) AS varchar) ) AS VARCHAR(MAX)) [text()]
				FROM OpportunityBizDocs
				LEFT JOIN ShippingReport
				ON ShippingReport.numOppBizDocId=OpportunityBizDocs.numOppBizDocsId
				WHERE OpportunityBizDocs.numOppId=Opp.numOppId  AND OpportunityBizDocs.bitShippingGenerated=1  FOR XML PATH(''''), TYPE)
						.value(''.'',''NVARCHAR(MAX)''),1,2,'' ''))'
					SET @strColumns = @strColumns + ' , (SELECT SUBSTRING('+ @temp +', 2, 200000))AS ShippingIcons '
					SET @strColumns = @strColumns + ' , CASE WHEN (SELECT COUNT(*) FROM ShippingBox INNER JOIN ShippingReport ON ShippingBox.numShippingReportId = ShippingReport.numShippingReportId WHERE ShippingReport.numOppID = Opp.numOppID AND LEN(vcTrackingNumber) > 0) > 0 THEN 1 ELSE 0 END [ISTrackingNumGenerated]'
				END

				IF LEN(@SearchText) > 0
				BEGIN
					SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + (CASE                    
					WHEN @vcDbColumnName = 'CalAmount' THEN 'Opp.monDealAmount'
					WHEN @vcDbColumnName = 'vcInventoryStatus' THEN 'ISNULL(dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID),'''')'
					WHEN @vcDbColumnName = 'vcOrderedShipped' THEN 'dbo.GetOrderShipReceiveStatus(Opp.numOppId,1,Opp.tintshipped)'
					WHEN @vcDbColumnName = 'vcOrderedReceived' THEN 'dbo.GetOrderShipReceiveStatus(Opp.numOppId,2,Opp.tintshipped)'
					WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'monDealAmount' THEN 'ISNULL((SELECT SUM(ISNULL(monDealAmount,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)'
					WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'monAmountPaid' THEN 'ISNULL((SELECT SUM(ISNULL(monAmountPaid,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)'
					else @Prefix + @vcDbColumnName END) + ' LIKE ''%' + @SearchText + '%'''
				END    
			END
            ELSE IF @vcAssociatedControlType='Popup' and @vcDbColumnName='numShareWith'                                                   
			BEGIN
				SET @strColumns = @strColumns + ',(SELECT SUBSTRING(
								(SELECT '','' + A.vcFirstName+'' ''+A.vcLastName + CASE WHEN ISNULL(SR.numContactType,0)>0 THEN ''('' + dbo.fn_GetListItemName(SR.numContactType) + '')'' ELSE '''' END
								FROM ShareRecord SR JOIN UserMaster UM ON UM.numUserDetailId=SR.numAssignedTo 
								JOIN AdditionalContactsInformation A ON UM.numUserDetailId=A.numContactID 
								WHERE SR.numDomainID=Opp.numDomainID AND SR.numModuleID=3 AND SR.numRecordID=Opp.numOppId
								AND UM.numDomainID=Opp.numDomainID and UM.numDomainID=A.numDomainID FOR XML PATH('''')),2,200000)) ['+ @vcColumnName+']'                
			END 
        END
		ELSE
        BEGIN
			SET @vcColumnName= @vcDbColumnName +'~'+ convert(varchar(10),@numFieldId)+'~1'

			IF @vcAssociatedControlType = 'TextBox' or @vcAssociatedControlType = 'TextArea'
            BEGIN
              SET @strColumns = @strColumns
                              + ',CFW'
                              + CONVERT(VARCHAR(3),@tintOrder)
                              + '.Fld_Value  ['
                              + @vcColumnName + ']'
              SET @WhereCondition = @WhereCondition
                                      + ' left Join CFW_FLD_Values_Opp CFW'
                                      + CONVERT(VARCHAR(3),@tintOrder)
                                      + ' on CFW'
                                      + CONVERT(VARCHAR(3),@tintOrder)
                                      + '.Fld_Id='
                                      + CONVERT(VARCHAR(10),@numFieldId)
                                      + 'and CFW'
                                      + CONVERT(VARCHAR(3),@tintOrder)
                                      + '.RecId=Opp.numOppid   '
			END
			ELSE
            IF @vcAssociatedControlType = 'CheckBox' 
            BEGIN
                SET @strColumns = @strColumns
                                + ',case when isnull(CFW'
                                + CONVERT(VARCHAR(3),@tintOrder)
                                + '.Fld_Value,0)=0 then 0 when isnull(CFW'
                                + CONVERT(VARCHAR(3),@tintOrder)
                                + '.Fld_Value,0)=1 then 1 end   ['
                                + @vcColumnName + ']'
                SET @WhereCondition = @WhereCondition
                                        + ' left Join CFW_FLD_Values_Opp CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + ' on CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.Fld_Id='
                                        + CONVERT(VARCHAR(10),@numFieldId)
                                        + 'and CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.RecId=Opp.numOppid   '
            END
            ELSE
            IF @vcAssociatedControlType = 'DateField'
            BEGIN
                  SET @strColumns = @strColumns
                                  + ',dbo.FormatedDateFromDate(CFW'
                                  + CONVERT(VARCHAR(3),@tintOrder)
                                  + '.Fld_Value,'
                                  + CONVERT(VARCHAR(10),@numDomainId)
                                  + ')  ['
                                  + @vcColumnName + ']'
                  SET @WhereCondition = @WhereCondition
                                          + ' left Join CFW_FLD_Values_Opp CFW'
                                          + CONVERT(VARCHAR(3),@tintOrder)
                                          + ' on CFW'
                                          + CONVERT(VARCHAR(3),@tintOrder)
                                          + '.Fld_Id='
                                          + CONVERT(VARCHAR(10),@numFieldId)
                                          + 'and CFW'
                                          + CONVERT(VARCHAR(3),@tintOrder)
                                          + '.RecId=Opp.numOppid    '
            END
            ELSE
            IF @vcAssociatedControlType = 'SelectBox'
            BEGIN
                SET @vcDbColumnName = 'DCust'
                                            + CONVERT(VARCHAR(10),@numFieldId)
                SET @strColumns = @strColumns
                                + ',L'
                                + CONVERT(VARCHAR(3),@tintOrder)
                                + '.vcData'
                                + ' ['
                                + @vcColumnName + ']'

                SET @WhereCondition = @WhereCondition
                                        + ' left Join CFW_FLD_Values_Opp CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + ' on CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.Fld_Id='
                                        + CONVERT(VARCHAR(10),@numFieldId)
                                        + 'and CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.RecId=Opp.numOppid '

                SET @WhereCondition = @WhereCondition
                                        + ' left Join ListDetails L'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + ' on L'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.numListItemID=CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.Fld_Value'
            END
			ELSE
			BEGIN
				SET @strColumns = @strColumns + CONCAT(',dbo.GetCustFldValueOpp(',@numFieldId,',opp.numOppID)') + ' [' + @vcColumnName + ']'
			END

			IF LEN(@SearchText) > 0
			BEGIN
				SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + CONCAT('dbo.GetCustFldValueOpp(',@numFieldId,',opp.numOppID) LIKE ''%',@SearchText,'%''')
			END
		END
      
     
		SELECT TOP 1 
			@tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName,@vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,
			@numListID=numListID,@vcLookBackTableName=vcLookBackTableName,@bitCustom=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,@bitAllowEdit=bitAllowEdit,
			@ListRelID=ListRelID
		FROM 
			#tempForm 
		WHERE 
			tintOrder > @tintOrder-1 
		ORDER BY 
			tintOrder asc            
 
		IF @@rowcount=0 SET @tintOrder=0 
	END 

	

	DECLARE @strShareRedordWith AS VARCHAR(300);
	SET @strShareRedordWith=' Opp.numOppId in (select SR.numRecordID FROM ShareRecord SR WHERE SR.numDomainID='+ convert(varchar(15),@numDomainID )+' AND SR.numModuleID=3 AND SR.numAssignedTo='+convert(varchar(15),@numUserCntId)+') '


	DECLARE @StrSql AS VARCHAR(MAX) = ''

	SET @StrSql = @StrSql + ' FROM OpportunityMaster Opp            
								 ##PLACEHOLDER##                                                   
                                 INNER JOIN AdditionalContactsInformation ADC ON Opp.numContactId = ADC.numContactId                                                               
                                 INNER JOIN DivisionMaster Div ON Opp.numDivisionId = Div.numDivisionID 
								 INNER JOIN CompanyInfo cmp ON Div.numCompanyID = cmp.numCompanyId 
								 LEFT JOIN ProjectProgress PP On PP.numOppId = Opp.numOppId ' + @WhereCondition

	-------Change Row Color-------
	
	DECLARE @vcCSOrigDbCOlumnName AS VARCHAR(50) = ''
	DECLARE @vcCSLookBackTableName AS VARCHAR(50) = ''
	DECLARE @vcCSAssociatedControlType AS VARCHAR(50)

	CREATE TABLE #tempColorScheme
	(
		vcOrigDbCOlumnName VARCHAR(50),
		vcLookBackTableName VARCHAR(50),
		vcFieldValue VARCHAR(50),
		vcFieldValue1 VARCHAR(50),
		vcColorScheme VARCHAR(50),
		vcAssociatedControlType varchar(50)
	)

	INSERT INTO 
		#tempColorScheme 
	SELECT 
		DFM.vcOrigDbCOlumnName,
		DFM.vcLookBackTableName,
		DFCS.vcFieldValue,
		DFCS.vcFieldValue1,
		DFCS.vcColorScheme,
		DFFM.vcAssociatedControlType 
	FROM 
		DycFieldColorScheme DFCS 
	JOIN 
		DycFormField_Mapping DFFM 
	ON 
		DFCS.numFieldID=DFFM.numFieldID
	JOIN 
		DycFieldMaster DFM 
	ON 
		DFM.numModuleID=DFFM.numModuleID and DFM.numFieldID=DFFM.numFieldID
	WHERE
		DFCS.numDomainID=@numDomainID 
		AND DFFM.numFormID=@numFormId 
		AND DFCS.numFormID=@numFormId 
		AND isnull(DFFM.bitAllowGridColor,0)=1

	IF(SELECT COUNT(*) FROM #tempColorScheme)>0
	BEGIN
		SELECT TOP 1 @vcCSOrigDbCOlumnName=vcOrigDbCOlumnName,@vcCSLookBackTableName=vcLookBackTableName,@vcCSAssociatedControlType=vcAssociatedControlType from #tempColorScheme
	END                        

	IF len(@vcCSOrigDbCOlumnName)>0 and len(@vcCSLookBackTableName)>0
	BEGIN
		SET @strColumns=@strColumns + ',tCS.vcColorScheme'
	END

	IF len(@vcCSOrigDbCOlumnName)>0 and len(@vcCSLookBackTableName)>0
	BEGIN
		IF @vcCSLookBackTableName = 'AdditionalContactsInformation'                  
			SET @Prefix = 'ADC.'                  
		IF @vcCSLookBackTableName = 'DivisionMaster'                  
			SET @Prefix = 'Div.'
		if @vcCSLookBackTableName = 'CompanyInfo'                  
			SET @Prefix = 'CMP.'   
		if @vcCSLookBackTableName = 'OpportunityMaster'                  
			SET @Prefix = 'Opp.'   
 
		IF @vcCSAssociatedControlType='DateField'
		BEGIN
			SET @strSql=@strSql+' LEFT JOIN #tempColorScheme tCS ON CONVERT(VARCHAR(10),' + @Prefix + @vcCSOrigDbCOlumnName + ',111) >= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue as int),GETUTCDATE()),111)
				 and CONVERT(VARCHAR(10),' + @Prefix + @vcCSOrigDbCOlumnName + ',111) <= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue1 as int),GETUTCDATE()),111)'
		END
		ELSE IF @vcCSAssociatedControlType='SelectBox' OR @vcCSAssociatedControlType='ListBox' OR @vcCSAssociatedControlType='CheckBox'
		BEGIN
			SET @strSql=@strSql+' LEFT JOIN #tempColorScheme tCS on tCS.vcFieldValue=' + @Prefix + @vcCSOrigDbCOlumnName
		END
	END
                          
	IF @columnName like 'CFW.Cust%'             
	BEGIN            
		SET @strSql = @strSql + ' LEFT JOIN CFW_Fld_Values_Opp CFW on CFW.RecId=Opp.numOppid and CFW.fld_id= '+ REPLACE(@columnName,'CFW.Cust','') +' '                                                     
		SET @columnName='CFW.Fld_Value'            
	END 

	IF @bitPartner = 1
		SET @strSql = @strSql + ' LEFT JOIN OpportunityContact OppCont ON OppCont.numOppId=Opp.numOppId AND OppCont.bitPartner=1 AND OppCont.numContactId=' + CONVERT(VARCHAR(15),@numUserCntID)
                    
                
	IF @tintFilterBy = 1 --Partially Fulfilled Orders 
		SET @strSql = @strSql + ' LEFT JOIN AdditionalContactsInformation ADC1 ON ADC1.numContactId=Opp.numRecOwner                                                               
                             LEFT OUTER JOIN [OpportunityBizDocs] OB ON OB.[numOppId] = Opp.[numOppId] 
                             WHERE Opp.numDomainID=' + CONVERT(VARCHAR(15),@numDomainID) + ' and Opp.tintOppstatus=1 and (Opp.tintShipped=' + CONVERT(VARCHAR(15),@tintShipped) + ' OR ' + CONVERT(VARCHAR(15),@tintShipped) + '=2)'
	ELSE
		SET @strSql = @strSql + ' LEFT JOIN AdditionalContactsInformation ADC1 on ADC1.numContactId=Opp.numRecOwner                                                               
                             WHERE Opp.numDomainID=' + CONVERT(VARCHAR(15),@numDomainID) + ' and Opp.tintOppstatus=1 and (Opp.tintShipped='+ CONVERT(VARCHAR(15),@tintShipped)  + ' OR ' + CONVERT(VARCHAR(15),@tintShipped) + '=2)'
  
  
	IF @intOrder <> 0
    BEGIN
      IF @intOrder = 1
        SET @strSql = @strSql + ' and Opp.bitOrder = 0 '
      IF @intOrder = 2
        SET @strSql = @strSql + ' and Opp.bitOrder = 1'
    END
    
	IF @numCompanyID <> 0
		SET @strSql = @strSql + ' And Div.numCompanyID ='+ CONVERT(VARCHAR(15),@numCompanyID)

	IF @SortChar <> '0' 
		SET @strSql = @strSql + ' And Opp.vcPOppName like ''' + @SortChar + '%'''
  

	IF @inttype = 3
	BEGIN
		IF @tintSalesUserRightType = 0
			SET @strSql = @strSql + ' AND opp.tintOppType=0'
		ELSE IF @tintSalesUserRightType = 1
			SET @strSql = @strSql + ' AND (Opp.numRecOwner= ' + CONVERT(VARCHAR(15),@numUserCntID)
						+ ' or Opp.numAssignedTo= '+ CONVERT(VARCHAR(15),@numUserCntID)
						+ CASE WHEN @bitPartner = 1 THEN ' or ( OppCont.bitPartner=1 and OppCont.numContactId='+ CONVERT(VARCHAR(15),@numUserCntID) + ')' ELSE '' END
						+ ' or ' + @strShareRedordWith +')'
		ELSE IF @tintSalesUserRightType = 2
			SET @strSql = @strSql + ' AND (Div.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+ CONVERT(VARCHAR(15),@numUserCntID) + ' ) 
									 or Opp.numAssignedTo= '+ CONVERT(VARCHAR(15),@numUserCntID)
									+ ' or ' + @strShareRedordWith +')'
	END
	ELSE IF @inttype = 4
	BEGIN
		IF @tintPurchaseUserRightType = 0
			SET @strSql = @strSql + ' AND opp.tintOppType=0'
		ELSE IF @tintPurchaseUserRightType = 1
			SET @strSql = @strSql + ' AND (Opp.numRecOwner= ' + CONVERT(VARCHAR(15),@numUserCntID)
						+ ' or Opp.numAssignedTo= '+ CONVERT(VARCHAR(15),@numUserCntID)
						+ CASE WHEN @bitPartner = 1 THEN ' or ( OppCont.bitPartner=1 and OppCont.numContactId='+ CONVERT(VARCHAR(15),@numUserCntID) + ')' ELSE '' END
						+ ' or ' + @strShareRedordWith +')'
		ELSE IF @tintPurchaseUserRightType = 2
			SET @strSql = @strSql + ' AND (Div.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+ CONVERT(VARCHAR(15),@numUserCntID) + ' ) 
									 or Opp.numAssignedTo= '+ CONVERT(VARCHAR(15),@numUserCntID)
									+ ' or ' + @strShareRedordWith +')'
	END

	
					
	IF @tintSortOrder <> '0'
		SET @strSql = @strSql + '  AND Opp.tintOppType= '+ CONVERT(VARCHAR(1),@tintSortOrder)
    
    
	IF @tintFilterBy = 1 --Partially Fulfilled Orders
		SET @strSql = @strSql + ' AND OB.bitPartialFulfilment = 1 AND OB.bitAuthoritativeBizDocs = 1 '
	ELSE IF @tintFilterBy = 2 --Fulfilled Orders
		SET @strSql = @strSql + ' AND Opp.[numOppId] IN (SELECT OM.numOppId FROM [OpportunityMaster] OM
                      			INNER JOIN [OpportunityBizDocs] OB ON OB.[numOppId] = OM.[numOppId]
                      			INNER JOIN [OpportunityItems] OI ON OM.[numOppId] = OI.[numOppId]
                      			INNER JOIN [OpportunityBizDocItems] BI ON BI.[numOppBizDocID] = OB.[numOppBizDocsId]
                      			Where OM.numDomainId=' + CONVERT(VARCHAR(15),@numDomainId) + 'AND OB.[bitAuthoritativeBizDocs] = 1 GROUP BY OM.[numOppId]
                      			HAVING   COUNT(OI.[numUnitHour]) = COUNT(BI.[numUnitHour])) '
	ELSE IF @tintFilterBy = 3 --Orders without Authoritative-BizDocs
		SET @strSql = @strSql + ' AND Opp.[numOppId] not IN (SELECT DISTINCT OM.[numOppId]
                      		  FROM [OpportunityMaster] OM JOIN [OpportunityBizDocs] OB ON OM.[numOppId] = OB.[numOppId]
                      		  Where OM.numDomainId=' + CONVERT(VARCHAR(15),@numDomainId) + ' AND OM.tintOppstatus=1 AND (OM.tintShipped=' + CONVERT(VARCHAR(15),@tintShipped) + ' OR ' + CONVERT(VARCHAR(15),@tintShipped) + '=2)' + ' AND OM.tintOppType= '+ CONVERT(VARCHAR(1),@tintSortOrder) + ' AND isnull(OB.[bitAuthoritativeBizDocs],0)=1) '
	ELSE IF @tintFilterBy = 4 --Orders with items yet to be added to Invoices
		SET @strSql = @strSql + ' AND Opp.[numOppId] IN (SELECT distinct OPPM.numOppId FROM [OpportunityMaster] OPPM
                                  INNER JOIN [OpportunityItems] OPPI ON OPPM.[numOppId] = OPPI.[numOppId]
                                  Where OPPM.numDomainId=' + CONVERT(VARCHAR(15),@numDomainId) + ' 
								  and OPPI.numoppitemtCode not in (select numOppItemID from [OpportunityBizDocs] OB INNER JOIN [OpportunityBizDocItems] BI
                      			  ON BI.[numOppBizDocID] = OB.[numOppBizDocsId] where OB.[numOppId] = OPPM.[numOppId]))'      
	ELSE IF @tintFilterBy = 6 --Orders with deferred Income/Invoices
		SET @strSql = @strSql + ' AND Opp.[numOppId] IN (SELECT DISTINCT OM.[numOppId]
                      		 FROM [OpportunityMaster] OM LEFT OUTER JOIN [OpportunityBizDocs] OB ON OM.[numOppId] = OB.[numOppId]
                      		 Where OM.numDomainId=' + CONVERT(VARCHAR(15),@numDomainId) + ' AND OB.[bitAuthoritativeBizDocs] =1 and OB.tintDeferred=1)'

	IF @numOrderStatus <>0 
		SET @strSql = @strSql + ' AND Opp.numStatus = ' + CONVERT(VARCHAR(15),@numOrderStatus);

	IF CHARINDEX('vcInventoryStatus',@vcRegularSearchCriteria) > 0
	BEGIN
		IF CHARINDEX('vcInventoryStatus=1',@vcRegularSearchCriteria) > 0
			SET @strSql = @strSql + ' AND ' + ' CHARINDEX(''Not Applicable'', dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID)) > 0 '
		IF CHARINDEX('vcInventoryStatus=2',@vcRegularSearchCriteria) > 0
			SET @strSql = @strSql + ' AND ' + ' CHARINDEX(''Allocation Cleared'', dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID)) > 0 '
		IF CHARINDEX('vcInventoryStatus=3',@vcRegularSearchCriteria) > 0
			SET @strSql = @strSql + ' AND ' + ' CHARINDEX(''Back Order'', dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID)) > 0 '
		IF CHARINDEX('vcInventoryStatus=4',@vcRegularSearchCriteria) > 0
			SET @strSql = @strSql + ' AND ' + ' CHARINDEX(''Ready to Ship'', dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID)) > 0 '
	END
	ELSE IF @vcRegularSearchCriteria<>'' 
	BEGIN
		SET @strSql=@strSql +' AND ' + @vcRegularSearchCriteria 
	END
	
	IF @vcCustomSearchCriteria<>'' 
	BEGIN
		SET @strSql = @strSql +' AND ' +  @vcCustomSearchCriteria
	END

	IF LEN(@SearchQuery) > 0
	BEGIN
		SET @strSql = @strSql + ' AND (' + @SearchQuery + ') '
	END

	IF ISNULL(@tintDashboardReminderType,0) = 19
	BEGIN
		SET @strSql = CONCAT(@strSql,' AND Opp.numOppID IN (','SELECT DISTINCT
																	OMInner.numOppID
																FROM 
																	StagePercentageDetails SPDInner
																INNER JOIN 
																	OpportunityMaster OMInner
																ON 
																	SPDInner.numOppID=OMInner.numOppId 
																WHERE 
																	SPDInner.numDomainId=',@numDOmainID,'
																	AND OMInner.numDomainId=',@numDOmainID,' 
																	AND OMInner.tintOppType = 1
																	AND ISNULL(OMInner.tintOppStatus,0) = 1
																	AND tinProgressPercentage <> 100',') ')

	END
	ELSE IF ISNULL(@tintDashboardReminderType,0) = 10
	BEGIN
		SET @strSql = CONCAT(@strSql,' AND Opp.numOppID IN (','SELECT DISTINCT
																	OMInner.numOppId
																FROM
																	OpportunityMaster OMInner
																INNER JOIN
																	OpportunityItems OIInner
																ON
																	OIInner.numOppId = OMInner.numOppID
																INNER JOIN
																	Item IInner
																ON
																	OIInner.numItemCode = IInner.numItemCode
																OUTER APPLY
																(
																	SELECT
																		SUM(OpportunityBizDocItems.numUnitHour) AS BilledQty
																	FROM
																		OpportunityBizDocs
																	INNER JOIN
																		OpportunityBizDocItems 
																	ON
																		OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																	WHERE
																		OpportunityBizDocs.numOppId = OMInner.numOppID
																		AND OpportunityBizDocItems.numOppItemID = OIInner.numoppitemtCode
																		AND OpportunityBizDocs.numBizDocId = (SELECT ISNULL(numAuthoritativePurchase,0) FROM AuthoritativeBizDocs WHERE numDomainId=',@numDomainID,')
																) AS TempBilled
																WHERE
																	OMInner.numDomainId = ',@numDomainID,'
																	AND ISNULL(OMInner.bitStockTransfer,0) = 0
																	AND OMInner.tintOppType = 2
																	AND OMInner.tintOppStatus  = 1
																	AND ISNULL(OIInner.numUnitHour,0) <> ISNULL(TempBilled.BilledQty,0)',') ')
	END
	ELSE IF ISNULL(@tintDashboardReminderType,0) = 11
	BEGIN
		SET @strSql = CONCAT(@strSql,' AND Opp.numOppID IN (','SELECT DISTINCT
																	OMInner.numOppId
																FROM
																	OpportunityMaster OMInner
																INNER JOIN
																	OpportunityItems OIInner
																ON
																	OIInner.numOppId = OMInner.numOppId
																INNER JOIN
																	Item IInner
																ON
																	OIInner.numItemCode = IInner.numItemCode
																OUTER APPLY
																(
																	SELECT
																		SUM(OpportunityBizDocItems.numUnitHour) AS PickPacked
																	FROM
																		OpportunityBizDocs
																	INNER JOIN
																		OpportunityBizDocItems 
																	ON
																		OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																	WHERE
																		OpportunityBizDocs.numOppId = OMInner.numOppId
																		AND ISNULL(OpportunityBizDocs.numBizDocId,0) = ',(SELECT ISNULL(numDefaultSalesShippingDoc,0) FROM Domain WHERE numDomainId=@numDomainID),'
																		AND OpportunityBizDocItems.numOppItemID = OIInner.numoppitemtCode
																) AS TempFulFilled
																WHERE
																	OMInner.numDomainId = ',@numDomainID,'
																	AND OMInner.tintOppType = 1
																	AND OMInner.tintOppStatus = 1
																	AND UPPER(IInner.charItemType) = ''P''
																	AND ISNULL(OMInner.tintshipped,0) = 0
																	AND ISNULL(OIInner.bitDropShip,0) = 0
																	AND ISNULL(OIInner.numUnitHour,0) <> ISNULL(TempFulFilled.PickPacked,0)',') ')
	END
	ELSE IF ISNULL(@tintDashboardReminderType,0) = 12
	BEGIN
		SET @strSql = CONCAT(@strSql,' AND Opp.numOppID IN (','SELECT DISTINCT
																	OMInner.numOppId
																FROM
																	OpportunityMaster OMInner
																INNER JOIN
																	OpportunityItems OIInner
																ON
																	OIInner.numOppId = OMInner.numOppId
																INNER JOIN
																	Item IInner
																ON
																	OIInner.numItemCode = IInner.numItemCode
																OUTER APPLY
																(
																	SELECT
																		SUM(OpportunityBizDocItems.numUnitHour) AS FulFilledQty
																	FROM
																		OpportunityBizDocs
																	INNER JOIN
																		OpportunityBizDocItems 
																	ON
																		OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																	WHERE
																		OpportunityBizDocs.numOppId = OMInner.numOppId
																		AND ISNULL(OpportunityBizDocs.numBizDocId,0) = 296
																		AND ISNULL(OpportunityBizDocs.bitFulFilled,0) = 1
																		AND OpportunityBizDocItems.numOppItemID = OIInner.numoppitemtCode
																) AS TempFulFilled
																WHERE
																	OMInner.numDomainId = ',@numDomainID,'
																	AND OMInner.tintOppType = 1
																	AND OMInner.tintOppStatus = 1
																	AND ISNULL(OMInner.tintshipped,0) = 0
																	AND UPPER(IInner.charItemType) = ''P''
																	AND ISNULL(OIInner.bitDropShip,0) = 0
																	AND ISNULL(OIInner.numUnitHour,0) <> ISNULL(TempFulFilled.FulFilledQty,0)',') ')
	END
	ELSE IF ISNULL(@tintDashboardReminderType,0) = 13
	BEGIN
		SET @strSql = CONCAT(@strSql,' AND Opp.numOppID IN (','SELECT DISTINCT
																	OMInner.numOppId
																FROM
																	OpportunityMaster OMInner
																INNER JOIN
																	OpportunityItems OIInner
																ON
																	OIInner.numOppId = OMInner.numOppID
																INNER JOIN
																	Item IInner
																ON
																	OIInner.numItemCode = IInner.numItemCode
																OUTER APPLY
																(
																	SELECT
																		SUM(OpportunityBizDocItems.numUnitHour) AS InvoicedQty
																	FROM
																		OpportunityBizDocs
																	INNER JOIN
																		OpportunityBizDocItems 
																	ON
																		OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																	WHERE
																		OpportunityBizDocs.numOppId = OMInner.numOppID
																		AND OpportunityBizDocItems.numOppItemID = OIInner.numoppitemtCode
																		AND OpportunityBizDocs.numBizDocId = (SELECT ISNULL(numAuthoritativeSales,0) FROM AuthoritativeBizDocs WHERE numDomainId=',@numDomainID,')
																) AS TempInvoice
																WHERE
																	OMInner.numDomainId = ',@numDomainID,'
																	AND OMInner.tintOppType = 1
																	AND OMInner.tintOppStatus  = 1
																	AND ISNULL(OIInner.numUnitHour,0) <> ISNULL(TempInvoice.InvoicedQty,0)',') ')
	END
	ELSE IF ISNULL(@tintDashboardReminderType,0) = 14
	BEGIN
		DECLARE @TEMPOppID TABLE
		(
			numOppID NUMERIC(18,0)
		)

		INSERT INTO @TEMPOppID
		(
			numOppID
		)
		SELECT DISTINCT
			OM.numOppID
		FROM
			OpportunityMaster OM
		INNER JOIN
			OpportunityItems OI
		ON
			OM.numOppId = OI.numOppId
		WHERE   
			OM.numDomainId = @numDomainId
			AND OM.tintOppType = 1
			AND OM.tintOppStatus=1
			AND ISNULL(OI.bitDropShip, 0) = 1
			AND 1 = (CASE WHEN (SELECT COUNT(*) FROM OpportunityLinking OMInner INNER JOIN OpportunityItems OIInner ON OMInner.numChildOppID=OIInner.numOppId WHERE OMInner.numParentOppID = OM.numOppID AND OIInner.numItemCode=OI.numItemCode AND ISNULL(OIInner.numWarehouseItmsID,0)=ISNULL(OI.numWarehouseItmsID,0)) > 0 THEN 0 ELSE 1 END)


		If ISNULL((SELECT bitReOrderPoint FROM Domain WHERE numDomainId=@numDomainID),0) = 1
		BEGIN
			INSERT INTO @TEMPOppID
			(
				numOppID
			)
			SELECT DISTINCT
				OM.numOppID
			FROM
				OpportunityMaster OM
			INNER JOIN
				OpportunityItems OI
			ON
				OI.numOppId = OM.numOppId
			INNER JOIN 
				Item I 
			ON 
				I.numItemCode = OI.numItemCode
			INNER JOIN 
				WarehouseItems WI 
			ON 
				OI.numWarehouseItmsID = WI.numWareHouseItemID
			WHERE   
				OM.numDomainId = @numDomainId
				AND ISNULL((SELECT bitReOrderPoint FROM Domain WHERE numDomainId=@numDomainID),0) = 1
				AND (ISNULL(WI.numOnHand,0) + ISNULL(WI.numOnOrder,0) <= ISNULL(WI.numReOrder,0))
				AND OM.tintOppType = 1
				AND OM.tintOppStatus=1
				AND ISNULL(I.bitAssembly, 0) = 0
				AND ISNULL(I.bitKitParent, 0) = 0
				AND ISNULL(OI.bitDropship,0) = 0
				AND OM.numOppId NOT IN (SELECT numOppID FROM @TEMPOppID)

			IF (SELECT COUNT(*) FROM @TEMPOppID) > 0
			BEGIN
				DECLARE @tmpIds VARCHAR(MAX) = ''
				SELECT @tmpIds = CONCAT(@tmpIds,numOppID,', ') FROM @TEMPOppID
				SET @strSql = CONCAT(@strSql,' AND Opp.numOppID IN (',SUBSTRING(@tmpIds, 0, LEN(@tmpIds)),') ')
			END
			ELSE
			BEGIN
				SET @strSql = CONCAT(@strSql,' AND Opp.numOppID IN (0) ')
			END
		END
	END
	ELSE IF ISNULL(@tintDashboardReminderType,0) = 15
	BEGIN
		SET @strSql = CONCAT(@strSql,' AND Opp.numOppID IN (','SELECT
																	OMInner.numOppID
																FROM
																	OpportunityMaster OMInner
																WHERE
																	numDomainId=',@numDomainID,'
																	AND tintOppType=1
																	AND tintOppStatus=1
																	AND ISNULL(OMInner.tintshipped,0) = 0
																	AND dtReleaseDate IS NOT NULL
																	AND dtReleaseDate < DateAdd(minute,',@ClientTimeZoneOffset * -1,',GETUTCDATE())
																	AND (SELECT 
																			COUNT(*) 
																		FROM 
																		(
																			SELECT
																				OIInner.numoppitemtCode,
																				ISNULL(OIInner.numUnitHour,0) AS OrderedQty,
																				ISNULL(TempFulFilled.FulFilledQty,0) AS FulFilledQty
																			FROM
																				OpportunityItems OIInner
																			INNER JOIN
																				Item IInner
																			ON
																				OIInner.numItemCode = IInner.numItemCode
																			OUTER APPLY
																			(
																				SELECT
																					SUM(OpportunityBizDocItems.numUnitHour) AS FulFilledQty
																				FROM
																					OpportunityBizDocs
																				INNER JOIN
																					OpportunityBizDocItems 
																				ON
																					OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																				WHERE
																					OpportunityBizDocs.numOppId = OMInner.numOppId
																					AND ISNULL(OpportunityBizDocs.numBizDocId,0) = 296
																					AND ISNULL(OpportunityBizDocs.bitFulFilled,0) = 1
																					AND OpportunityBizDocItems.numOppItemID = OIInner.numoppitemtCode
																			) AS TempFulFilled
																			WHERE
																				OIInner.numOppID = OMInner.numOppID
																				AND UPPER(IInner.charItemType) = ''P''
																				AND ISNULL(OIInner.bitDropShip,0) = 0
																		) X
																		WHERE
																			X.OrderedQty <> X.FulFilledQty) > 0',') ')
	END



	DECLARE  @firstRec  AS INTEGER
	DECLARE  @lastRec  AS INTEGER
  
	SET @firstRec = (@CurrentPage - 1) * @PageSize
	SET @lastRec = (@CurrentPage * @PageSize + 1)



	-- EXECUTE FINAL QUERY
	DECLARE @strFinal AS NVARCHAR(MAX)
	SET @strFinal = CONCAT('SELECT ROW_NUMBER() OVER(ORDER BY ',@columnName,' ',@columnSortOrder,') ID, Opp.numOppID INTO #temp2', REPLACE(@strSql,'##PLACEHOLDER##',''),'; SELECT ID,',@strColumns,' INTO #tempTable',REPLACE(@strSql,'##PLACEHOLDER##',' JOIN #temp2 tblAllOrders ON Opp.numOppID = tblAllOrders.numOppID '),' AND tblAllOrders.ID > ',@firstRec,' and tblAllOrders.ID <',@lastRec,' ORDER BY ID; SELECT * FROM  #tempTable; DROP TABLE #tempTable; SELECT @TotalRecords = COUNT(*) FROM #temp2; DROP TABLE #temp2;')
	--SET @strFinal = CONCAT('SELECT ROW_NUMBER() OVER(ORDER BY ',@columnName,' ',@columnSortOrder,') ID, ',@strColumns,' INTO #tempTable',@strSql,'; SELECT * FROM #tempTable WHERE ID > ',@firstRec,' and ID <',@lastRec,' ORDER BY ID; SELECT @TotalRecords = COUNT(*) FROM #tempTable; DROP TABLE #tempTable;')

	PRINT @strFinal
	EXEC sp_executesql @strFinal, N'@TotalRecords INT OUT', @TotRecs OUT

	SELECT * FROM #tempForm

	DROP TABLE #tempForm

	DROP TABLE #tempColorScheme
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetDropDownValue')
DROP PROCEDURE USP_GetDropDownValue
GO
Create PROCEDURE [dbo].[USP_GetDropDownValue]     
    @numDomainID numeric(18, 0),
    @numListID numeric(18, 0),
    @vcListItemType AS VARCHAR(5),
    @vcDbColumnName VARCHAR(50)
as                 

CREATE TABLE #temp(numID VARCHAR(500),vcData VARCHAR(max))

			 IF @vcListItemType='LI' OR @vcListItemType='T'                                                    
				BEGIN  
					INSERT INTO #temp                                                   
					SELECT Ld.numListItemID,vcData from ListDetails LD          
					left join listorder LO on Ld.numListItemID= LO.numListItemID  and lo.numDomainId = @numDomainID
					where (Ld.numDomainID=@numDomainID or Ld.constFlag=1) and Ld.numListID=@numListID order by intSortOrder
				end  
 			 ELSE IF @vcListItemType='C'                                                     
				begin    
		   		    INSERT INTO #temp                                                   
	  			    SELECT  numCampaignID,vcCampaignName + CASE bitIsOnline WHEN 1 THEN ' (Online)' ELSE ' (Offline)' END AS vcCampaignName
					FROM CampaignMaster WHERE numDomainID = @numDomainID
				end  
			 ELSE IF @vcListItemType='AG'                                                     
				begin      
		   		    INSERT INTO #temp                                                   
					SELECT numGrpID, vcGrpName FROM groups       
					ORDER BY vcGrpName                                               
				end   
			else if @vcListItemType='DC'                                                     
				begin    
		   		    INSERT INTO #temp                                                   
					select numECampaignID,vcECampName from ECampaign where numDomainID= @numDomainID                                                       
				end 
			else if @vcListItemType='U' OR @vcDbColumnName='numManagerID'
			    BEGIN
		   		     INSERT INTO #temp                                                   
			    	 SELECT A.numContactID,ISNULL(A.vcFirstName,'') +' '+ ISNULL(A.vcLastName,'') as vcUserName          
					 from UserMaster UM join AdditionalContactsInformation A        
					 on UM.numUserDetailId=A.numContactID          
					 where UM.numDomainID=@numDomainID and UM.numDomainID=A.numDomainID  ORDER BY A.vcFirstName,A.vcLastName 
				END
			else if @vcListItemType='S'
			    BEGIN
		   		     INSERT INTO #temp                                                   
			    	 SELECT S.numStateID,S.vcState          
					 from State S      
					 where S.numDomainID=@numDomainID ORDER BY S.vcState
				END
			ELSE IF @vcListItemType = 'OC' 
				BEGIN
					INSERT INTO #temp   
					SELECT DISTINCT C.numCurrencyID,C.vcCurrencyDesc FROM dbo.Currency C WHERE C.numDomainID=@numDomainID
				END
			ELSE IF @vcListItemType = 'UOM' 
				BEGIN
					INSERT INTO #temp   
					SELECT  numUOMId AS numItemID,vcUnitName AS vcItemName FROM UOM WHERE numDomainID = @numDomainID
				END	
			ELSE IF @vcListItemType = 'IG' 
				BEGIN
					INSERT INTO #temp   
					SELECT numItemGroupID,vcItemGroup FROM dbo.ItemGroups WHERE numDomainID=@numDomainID   	
				END
			ELSE IF @vcListItemType = 'SYS'
				BEGIN
					INSERT INTO #temp
					SELECT 0 AS numItemID,'Lead' AS vcItemName
					UNION ALL
					SELECT 1 AS numItemID,'Prospect' AS vcItemName
					UNION ALL
					SELECT 2 AS numItemID,'Account' AS vcItemName
				END
			ELSE IF @vcListItemType = 'PP' 
				BEGIN
					INSERT INTO #temp 
					SELECT 'P','Inventory Item'
					UNION 
					SELECT 'N','Non-Inventory Item'
					UNION 
					SELECT 'S','Service'
				END

				--<asp:ListItem Value="0" Selected="True">0</asp:ListItem>
    --                                <asp:ListItem Value="25" Selected="True">25%</asp:ListItem>
    --                                <asp:ListItem Value="50">50%</asp:ListItem>
    --                                <asp:ListItem Value="75">75%</asp:ListItem>
    --                                <asp:ListItem Value="100">100%</asp:ListItem>
				ELSE IF @vcListItemType = 'SP' --Progress Percentage
				BEGIN
					INSERT INTO #temp 
					SELECT '0','0%'
					UNION 
					SELECT '25','25%'
					UNION 
					SELECT '50','50%'
					UNION 
					SELECT '75','75%'
					UNION 
					SELECT '100','100%'
				END
				ELSE IF @vcListItemType = 'ST' --Stage Details
				BEGIN
					INSERT INTO #temp 
					SELECT vcStageName ,vcStageName  from StagePercentageDetails where numDomainId=@numDomainID and slp_id=@numListID  AND ISNULL(numProjectID,0)= 0 AND ISNULL(numOppID,0)= 0
				END
				ELSE IF @vcListItemType = 'SG' --Stage Details
				BEGIN
					INSERT INTO #temp 
					SELECT vcStageName ,vcStageName  from StagePercentageDetails where numDomainId=@numDomainID   AND ISNULL(numProjectID,0)= 0 AND ISNULL(numOppID,0)= 0
				END
				ELSE IF @vcListItemType = 'SM' --Reminer,snooze time
				BEGIN
					INSERT INTO #temp 
					SELECT '5','5 minutes'
					UNION 
					SELECT '15','15 minutes'
					UNION 
					SELECT '30','30 Minutes'
					UNION 
					SELECT '60','1 Hour'
					UNION 
					SELECT '240','4 Hour'
					UNION 
					SELECT '480','8 Hour'
					UNION 
					SELECT '1440','24 Hour'

				END
			 ELSE IF @vcListItemType = 'PT' --Pin To
				BEGIN
					INSERT INTO #temp 
					SELECT '1','Sales Opportunity'
					UNION 
					SELECT '2','Purchase Opportunity'
					UNION 
					SELECT '3','Sales Order'
					UNION 
					SELECT '4','Purchase Order'
					UNION 
					SELECT '5','Project'
					UNION 
					SELECT '6','Cases'
					UNION 
					SELECT '7','Item'

				END
				ELSE IF @vcListItemType = 'DV' --Customer
				BEGIN
					--INSERT INTO #temp 
					--SELECT vcStageName ,vcStageName  from StagePercentageDetails where numDomainId=@numDomainID and slp_id=@numListID  AND ISNULL(numProjectID,0)= 0 AND ISNULL(numOppID,0)= 0
				INSERT INTO #temp 
					SELECT d.numDivisionID,a.vcCompanyname + Case when isnull(d.numCompanyDiff,0)>0 then  ' ' + dbo.fn_getlistitemname(d.numCompanyDiff) + ':' + isnull(d.vcCompanyDiff,'') else '' end as vcCompanyname from   companyinfo AS a                      
					join divisionmaster d                      
					on  a.numCompanyid=d.numCompanyid
					WHERE D.numDomainID=@numDomainID
			
				END
				ELSE IF @vcListItemType = 'BD' --Net Days
				BEGIN
					--INSERT INTO #temp 
					--SELECT vcStageName ,vcStageName  from StagePercentageDetails where numDomainId=@numDomainID and slp_id=@numListID  AND ISNULL(numProjectID,0)= 0 AND ISNULL(numOppID,0)= 0
				INSERT INTO #temp 
						SELECT numTermsID,vcTerms	
						FROM dbo.BillingTerms 
						WHERE 
						numDomainID = @numDomainID
						AND ISNULL(bitActive,0) = 1
			
				END
				ELSE IF @vcListItemType = 'BT' --Net Days
				BEGIN
					--INSERT INTO #temp 
					--SELECT vcStageName ,vcStageName  from StagePercentageDetails where numDomainId=@numDomainID and slp_id=@numListID  AND ISNULL(numProjectID,0)= 0 AND ISNULL(numOppID,0)= 0
				INSERT INTO #temp 
							SELECT numBizDocTempID,    
							vcTemplateName
							FROM BizDocTemplate  
							WHERE [numDomainID] = @numDomainID and tintTemplateType=0
			
				END
				
		    ELSE IF @vcDbColumnName='charSex'
				BEGIN
					 INSERT INTO #temp
					 SELECT 'M','Male' UNION
					 SELECT 'F','Female'
				 END
			ELSE IF @vcDbColumnName='tintOppStatus'
			  BEGIN
					 INSERT INTO #temp
					 SELECT 1,'Deal Won' UNION
					 SELECT 2,'Deal Lost'
			  END
			ELSE IF @vcDbColumnName='tintOppType'
			  BEGIN
					 INSERT INTO #temp
					 SELECT 1,'Sales Order' UNION
					 SELECT 2,'Purchase Order' UNION
					 SELECT 3,'Sales Opportunity' UNION
					 SELECT 4,'Purchase Opportunity' 
			  END
			 ELSE IF @vcDbColumnName = 'vcLocation'
			  BEGIN
					INSERT INTO #temp
					SELECT -1,'Global' UNION
					SELECT
						numWLocationID,
						ISNULL(vcLocation,'')
					FROM
						WarehouseLocation
					WHERE
						numDomainID=@numDomainID
				
			  END
			  ELSE IF @vcDbColumnName = 'tintPriceLevel'
			  BEGIN
					DECLARE @Temp TABLE
					(
						Id INT
					)
					INSERT INTO @Temp
					(
						Id
					)
					SELECT DISTINCT 
						ROW_NUMBER() OVER(PARTITION BY pt.numItemCode ORDER BY numPricingID) Id
					FROM 
						[PricingTable] pt
					INNER JOIN 
						Item
					ON 
						Item.numItemCode = pt.numItemCode 
						AND Item.numDomainID = @numDomainID
					WHERE 
						tintRuleType = 3
					ORDER BY [Id] 

					INSERT INTO 
						#temp
					SELECT 
						pt.Id
						,ISNULL(NULLIF(pnt.vcPriceLevelName, ''), CONCAT('Price Level ',pt.Id)) Value
					FROM 
						@Temp pt
					LEFT JOIN
						PricingNamesTable pnt
					ON 
						pt.Id = pnt.tintPriceLevel
					AND 
						pnt.numDomainID = @numDomainID				
			  END
--			Else                                                     
--				BEGIN                                                     
--					select @listName=vcData from ListDetails WHERE numListItemID=@numlistID
--				end 	

 INSERT INTO #temp  SELECT 0,'--None--'				
        
		SELECT * FROM #temp 
		DROP TABLE #temp	
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetItemTransit')
DROP PROCEDURE USP_GetItemTransit
GO
CREATE PROCEDURE [dbo].[USP_GetItemTransit] 
( 
@numDomainID as numeric(9)=0,    
@numItemcode AS NUMERIC(9)=0,
@numVendorID AS NUMERIC(9)=0,
@numAddressID AS NUMERIC(9)=0,
@tintMode AS TINYINT,
@ClientTimeZoneOffset      AS INT
)
AS 
--tintOppStatus = 1 -> Won 
--[tintOppType] = 2 AND [tintOppStatus] = 1 -> Purchase Order 
--tintShipped = 0 

IF @tintMode=1
BEGIN
select dbo.fn_GetItemTransitCount(@numItemcode,@numDomainID) AS Total
--	select COUNT(*) AS Total
--      FROM OpportunityMaster Opp INNER JOIN OpportunityItems OppI ON opp.numOppId=OppI.numOppId 
--		   INNER JOIN item I on OppI.numItemCode=i.numItemcode
--		   LEFT JOIN  UOM u ON u.numUOMId = OppI.numUOMId  
--           WHERE Opp.tintOppType= 2 and Opp.tintOppstatus=1 and Opp.tintShipped=0 
--				 and Opp.numDomainId=@numDomainID AND OppI.numItemcode=@numItemcode
--				 and (isnull(OppI.numUnitHour,0)-isnull(OppI.numUnitHourReceived,0)>0)

END

ELSE IF @tintMode=2
BEGIN 
select opp.numOppId,Opp.vcPOppName,
case when convert(varchar(11),DateAdd(minute, -@ClientTimeZoneOffset,Opp.bintCreatedDate) )= convert(varchar(11),getdate()) then '<b><font color=red>Today</font></b>'when convert(varchar(11),DateAdd(minute, -@ClientTimeZoneOffset,Opp.bintCreatedDate) )= convert(varchar(11),dateadd(day,-1,getdate())) then '<b><font color=purple>YesterDay</font></b>'when convert(varchar(11),DateAdd(minute, -@ClientTimeZoneOffset,Opp.bintCreatedDate) )= convert(varchar(11),dateadd(day,1,getdate())) then'<b><font color=orange>Tommorow</font></b>' else dbo.FormatedDateFromDate(Opp.bintCreatedDate,1) end  bintCreatedDate,
dbo.fn_GetContactName(Opp.numContactId) AS vcOrderedBy,OppI.vcItemName,
CAST((dbo.fn_UOMConversion(ISNULL(I.numBaseUnit,0),OppI.numItemCode,OPP.numDomainId,ISNULL(OppI.numUOMId,0)) * (isnull(OppI.numUnitHour,0)-isnull(OppI.numUnitHourReceived,0))) as numeric(18,0)) as numUnitHour
,ISNULL(u.vcUnitName,'') vcUOMName,OppI.vcManufacturer,ISNULL(OppI.vcItemDesc,'') as vcItemDesc,OppI.vcModelID,
dbo.fn_GetListItemName(Opp.numShipmentMethod) vcShipmentMethod,
CASE WHEN ISNULL(VSM.numListValue,0)>0 THEN 
case when convert(varchar(11),DATEADD(DAY,VSM.numListValue,DateAdd(minute, -@ClientTimeZoneOffset,Opp.bintCreatedDate)))= convert(varchar(11),getdate()) then '<b><font color=red>Today</font></b>'
	 when convert(varchar(11),DATEADD(DAY,VSM.numListValue,DateAdd(minute, -@ClientTimeZoneOffset,Opp.bintCreatedDate)))= convert(varchar(11),dateadd(day,-1,getdate())) then '<b><font color=purple>YesterDay</font></b>'
	 when convert(varchar(11),DATEADD(DAY,VSM.numListValue,DateAdd(minute, -@ClientTimeZoneOffset,Opp.bintCreatedDate)))= convert(varchar(11),dateadd(day,1,getdate())) then'<b><font color=orange>Tommorow</font></b>' 
	 else dbo.FormatedDateFromDate(DATEADD(DAY,VSM.numListValue,DateAdd(minute, -@ClientTimeZoneOffset,Opp.bintCreatedDate)),@numDomainID) end 
	 ELSE '' end bintExpectedDelivery
      FROM OpportunityMaster Opp INNER JOIN OpportunityItems OppI ON opp.numOppId=OppI.numOppId 
		   INNER JOIN item I on OppI.numItemCode=i.numItemcode
		   LEFT JOIN  UOM u ON u.numUOMId = OppI.numUOMId  
		   OUTER APPLY (SELECT TOP 1 * FROM VendorShipmentMethod WHERE numAddressID=Opp.numVendorAddressID AND numVendorID=Opp.numDivisionId ORDER BY ISNULL(bitPrimary,0) DESC, ISNULL(bitPreferredMethod,0) DESC) VSM
           WHERE Opp.tintOppType= 2 and Opp.tintOppstatus=1 and Opp.tintShipped=0 
				 and Opp.numDomainId=@numDomainID AND OppI.numItemcode=@numItemcode
				 and (isnull(OppI.numUnitHour,0)-isnull(OppI.numUnitHourReceived,0)>0)
END

ELSE IF @tintMode=3
BEGIN 
select opp.numOppId,Opp.vcPOppName,case when convert(varchar(11),DateAdd(minute, -@ClientTimeZoneOffset,Opp.bintCreatedDate) )= convert(varchar(11),getdate()) then '<b><font color=red>Today</font></b>'when convert(varchar(11),DateAdd(minute, -@ClientTimeZoneOffset,Opp.bintCreatedDate) )= convert(varchar(11),dateadd(day,-1,getdate())) then '<b><font color=purple>YesterDay</font></b>'when convert(varchar(11),DateAdd(minute, -@ClientTimeZoneOffset,Opp.bintCreatedDate) )= convert(varchar(11),dateadd(day,1,getdate())) then'<b><font color=orange>Tommorow</font></b>' else dbo.FormatedDateFromDate(Opp.bintCreatedDate,1) end  bintCreatedDate,
dbo.fn_GetContactName(Opp.numContactId) AS vcOrderedBy,OppI.vcItemName,
CAST((dbo.fn_UOMConversion(ISNULL(I.numBaseUnit,0),OppI.numItemCode,OPP.numDomainId,ISNULL(OppI.numUOMId,0)) * (isnull(OppI.numUnitHour,0)-isnull(OppI.numUnitHourReceived,0))) as numeric(18,0)) as numUnitHour
,ISNULL(u.vcUnitName,'') vcUOMName,OppI.vcManufacturer,ISNULL(OppI.vcItemDesc,'') as vcItemDesc,OppI.vcModelID,
dbo.fn_GetListItemName(OppI.numShipmentMethod) vcShipmentMethod,
CASE WHEN ISNULL(VSM.numListValue,0)>0 THEN 
case when convert(varchar(11),DATEADD(DAY,VSM.numListValue,DateAdd(minute, -@ClientTimeZoneOffset,Opp.bintCreatedDate)))= convert(varchar(11),getdate()) then '<b><font color=red>Today</font></b>'
	 when convert(varchar(11),DATEADD(DAY,VSM.numListValue,DateAdd(minute, -@ClientTimeZoneOffset,Opp.bintCreatedDate)))= convert(varchar(11),dateadd(day,-1,getdate())) then '<b><font color=purple>YesterDay</font></b>'
	 when convert(varchar(11),DATEADD(DAY,VSM.numListValue,DateAdd(minute, -@ClientTimeZoneOffset,Opp.bintCreatedDate)))= convert(varchar(11),dateadd(day,1,getdate())) then'<b><font color=orange>Tommorow</font></b>' 
	 else dbo.FormatedDateFromDate(DATEADD(DAY,VSM.numListValue,DateAdd(minute, -@ClientTimeZoneOffset,Opp.bintCreatedDate)),1) end 
	 ELSE '' end bintExpectedDelivery
      FROM OpportunityMaster Opp INNER JOIN OpportunityItems OppI ON opp.numOppId=OppI.numOppId 
		   INNER JOIN item I on OppI.numItemCode=i.numItemcode
		   LEFT JOIN  UOM u ON u.numUOMId = OppI.numUOMId  
		   LEFT JOIN  VendorShipmentMethod VSM ON VSM.numAddressID=OppI.numVendorWareHouse AND VSM.numVendorID=Opp.numDivisionId AND VSM.numListItemID=OppI.numShipmentMethod
           WHERE Opp.tintOppType= 2 and Opp.tintOppstatus=1 and Opp.tintShipped=0 
				 and Opp.numDomainId=@numDomainID AND Opp.numDivisionId=@numVendorID AND OppI.numVendorWareHouse=@numAddressID
				 and (isnull(OppI.numUnitHour,0)-isnull(OppI.numUnitHourReceived,0)>0)
END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetMirrorBizDocDetails' ) 
    DROP PROCEDURE USP_GetMirrorBizDocDetails
GO

-- EXEC USP_GetMirrorBizDocDetails 53,8,1,1,0
CREATE PROCEDURE [dbo].[USP_GetMirrorBizDocDetails]
    (
      @numReferenceID NUMERIC(9) = 0,
      @numReferenceType TINYINT,
      @numDomainId NUMERIC(9),
      @numUserCntID NUMERIC(9),
      @ClientTimeZoneOffset INT
    )
AS 
    BEGIN
--        DECLARE @numBizDocID AS NUMERIC
        DECLARE @numBizDocType AS NUMERIC
        
        IF @numReferenceType = 1
			SELECT  @numBizDocType = numListItemID FROM dbo.ListDetails WHERE numListID=27 AND vcData='Sales Order' AND constFlag=1
        ELSE IF @numReferenceType = 2
			SELECT  @numBizDocType = numListItemID FROM dbo.ListDetails WHERE numListID=27 AND vcData='Purchase Order' AND constFlag=1
        ELSE IF @numReferenceType = 3
			SELECT  @numBizDocType = numListItemID FROM dbo.ListDetails WHERE numListID=27 AND vcData='Sales Opportunity' AND constFlag=1
        ELSE IF @numReferenceType = 4
			SELECT  @numBizDocType = numListItemID FROM dbo.ListDetails WHERE numListID=27 AND vcData='Purchase Opportunity' AND constFlag=1
        ELSE IF @numReferenceType = 5 
			SELECT @numBizDocType =  numListItemID FROM dbo.ListDetails WHERE vcData='RMA' AND constFlag=1
		ELSE IF @numReferenceType = 6 
			SELECT @numBizDocType =  numListItemID FROM dbo.ListDetails WHERE vcData='RMA' AND constFlag=1 
		ELSE IF @numReferenceType = 7 
			SELECT @numBizDocType =  numListItemID FROM dbo.ListDetails WHERE vcData='Sales Credit Memo' AND constFlag=1 
		ELSE IF @numReferenceType = 8
			SELECT @numBizDocType =  numListItemID FROM dbo.ListDetails WHERE vcData='Purchase Credit Memo' AND constFlag=1 
		ELSE IF @numReferenceType = 9
			SELECT @numBizDocType =  numListItemID FROM dbo.ListDetails WHERE vcData='Refund Receipt' AND constFlag=1 
		ELSE IF @numReferenceType = 10
			SELECT @numBizDocType =  numListItemID FROM dbo.ListDetails WHERE vcData='Credit Memo' AND constFlag=1 
		ELSE IF @numReferenceType = 11
			SELECT  @numBizDocType = numListItemID FROM dbo.ListDetails WHERE vcData='Invoice' AND constFlag=1
	 
        IF @numReferenceType = 1 OR @numReferenceType = 2 OR @numReferenceType = 3 OR @numReferenceType = 4 
		BEGIN
		      
			---------------------------------------------------------------------------------------
      
				  SELECT Mst.vcPOppName AS vcBizDocID,
						 Mst.vcPOppName AS OppName,Mst.vcPOppName AS BizDcocName,@numBizDocType AS BizDoc,ISNULL(Mst.numRecOwner,0) AS Owner,
						 CMP.VcCompanyName AS CompName,Mst.numContactID,Mst.numCreatedBy AS BizDocOwner,
						 Mst.fltDiscount AS decDiscount,
						 (SELECT SUM(ISNULL(monAmountPaid,0)) FROM OpportunityBizDocs WHERE numOppId=MSt.numOppId) AS monAmountPaid,
						 isnull(Mst.txtComments,'') AS vcComments,
						 CONVERT(VARCHAR(20),DATEADD(minute,-@ClientTimeZoneOffset,Mst.bintCreatedDate)) dtCreatedDate,
						 dbo.fn_GetContactName(Mst.numCreatedby) AS numCreatedby,
						 dbo.fn_GetContactName(Mst.numModifiedBy) AS numModifiedBy,
						 Mst.bintModifiedDate AS dtModifiedDate,
						 '' AS numViewedBy,
						 NULL AS dtViewedDate,
						 isnull(Mst.bitBillingTerms,0) AS tintBillingTerms,
						 isnull(Mst.intBillingDays,0) AS numBillingDays,
						 ISNULL(BTR.numNetDueInDays,0) AS numBillingDaysName,
						 BTR.vcTerms AS vcBillingTermsName,
						 isnull(Mst.bitInterestType,0) AS tintInterestType,
						 isnull(Mst.fltInterest,0) AS fltInterest,
						 tintOPPType,
						 @numBizDocType AS numBizDocId,
						 Mst.bintAccountClosingDate,
						 tintShipToType,
						 tintBillToType,
						 tintshipped,
						 NULL bintShippedDate,
						 ISNULL(Mst.monShipCost ,0) AS monShipCost,
						 ISNULL(BT.vcBizDocFooter,D.vcBizDocFooter) AS BizdocFooter,
						 ISNULL(BT.vcBizDocImagePath,D.vcBizDocImagePath) As vcBizDocImagePath, 
						 Mst.numDivisionID,
						  ISNULL(BT.vcPurBizDocFooter,D.vcPurBizDocFooter) AS PurBizdocFooter,
						 isnull(Mst.fltDiscount,0) AS fltDiscount,
						 isnull(Mst.bitDiscountType,0) AS bitDiscountType,
						 0 AS numShipVia,
						 '' AS vcTrackingURL,
						 0 AS numBizDocStatus,
						 '' AS BizDocStatus,
						 '' AS ShipVia,
						 ISNULL(C.varCurrSymbol,'') varCurrSymbol,
						 0 AS ShippingReportCount,
						 0 bitPartialFulfilment,
						 vcPOppName as vcBizDocName,
						 dbo.[fn_GetContactName](Mst.[numModifiedBy])  + ', '
							+ CONVERT(VARCHAR(20),dateadd(MINUTE,-@ClientTimeZoneOffset,Mst.bintModifiedDate)) AS ModifiedBy,
						 dbo.[fn_GetContactName](Mst.[numRecOwner]) + ', ' +  dbo.[fn_GetContactName](Mst.[numAssignedTo]) AS OrderRecOwner,
						 dbo.[fn_GetContactName](DM.[numRecOwner]) + ', ' +  dbo.[fn_GetContactName](DM.[numAssignedTo]) AS AccountRecOwner,
						 Mst.bintCreatedDate AS dtFromDate,
						 Mst.tintTaxOperator,
						 0 AS monTotalEmbeddedCost,
						 0 AS monTotalAccountedCost,
						 ISNULL(BT.bitEnabled,0) bitEnabled,
						 ISNULL(BT.txtBizDocTemplate,'') txtBizDocTemplate, 
						 ISNULL(BT.txtCSS,'') txtCSS,
						 dbo.fn_GetContactName(Mst.numAssignedTo) AS AssigneeName,
						 ISNULL((SELECT vcEmail FROM dbo.AdditionalContactsInformation WHERE numContactID = Mst.numAssignedTo),'') AS AssigneeEmail,
						 ISNULL((SELECT numPhone FROM dbo.AdditionalContactsInformation WHERE numContactID = Mst.numAssignedTo),'') AS AssigneePhone,
						 dbo.fn_GetComapnyName(Mst.numDivisionId) OrganizationName,
						 dbo.fn_GetContactName(Mst.numContactID)  OrgContactName,
						 case when ACI.numPhone<>'' then ACI.numPhone +case when ACI.numPhoneExtension<>'' then ' - ' + ACI.numPhoneExtension else '' end  else '' END OrgContactPhone,
						 DM.vcComPhone as OrganizationPhone,
						 ISNULL(ACI.vcEmail,'') AS OrgContactEmail,
 						 dbo.[fn_GetContactName](Mst.[numRecOwner]) AS OnlyOrderRecOwner,
 						 0 bitAuthoritativeBizDocs,
						0 tintDeferred,0 AS monCreditAmount,
						 0 as bitRentalBizDoc,isnull(BT.numBizDocTempID,0) as numBizDocTempID,isnull(BT.vcTemplateName,'') as vcTemplateName,
						 [dbo].[GetDealAmount](Mst.numOppId ,getutcdate(),0 ) as monPAmount,
						 --(select top 1 SD.vcSignatureFile from SignatureDetail SD join OpportunityBizDocsDetails OBD on SD.numSignID=OBD.numSignID where SD.numDomainID = @numDomainID and OBD.numDomainID = @numDomainID and OBD.numSignID IS not null and OBD.numBizDocsId=opp.numOppBizDocsId order by OBD.numBizDocsPaymentDetId desc) as vcSignatureFile,
						 isnull(CMP.txtComments,'') as vcOrganizationComments,
						 ISNULL((SELECT SUBSTRING((SELECT '$^$' + ISNULL( (SELECT vcShipFieldValue FROM dbo.ShippingFieldValues WHERE numDomainID=MSt.numDomainID AND intShipFieldID IN ( SELECT intShipFieldID FROM dbo.ShippingFields WHERE numListItemID=OBD.numShipVia  AND vcFieldName='Tracking URL')),'') +'#^#'+ RTRIM(LTRIM(vcTrackingNo) )  FROM dbo.OpportunityBizDocs OBD WHERE OBD.numOppId=Mst.numOppID FOR XML PATH('')),4,200000)),'') AS  vcTrackingNo,'' AS  vcShippingMethod,
						 NULL AS dtDeliveryDate,ISNULL(Mst.vcOppRefOrderNo,'') AS vcOppRefOrderNo,ISNULL(Mst.numDiscountAcntType,0) AS numDiscountAcntType ,
						 isnull(numOppBizDocTempID,0) as numOppBizDocTempID,
						 com1.vcCompanyName AS EmployerOrganizationName,div1.vcComPhone as EmployerOrganizationPhone,dbo.FormatedDateFromDate(Mst.dtReleaseDate,@numDomainID) AS vcReleaseDate
						 ,(CASE WHEN ISNULL(Mst.numPartner,0) > 0 THEN ISNULL((SELECT CONCAT(DivisionMaster.vcPartnerCode,'-',CompanyInfo.vcCompanyName) FROM DivisionMaster INNER JOIN CompanyInfo ON DivisionMaster.numCompanyID=CompanyInfo.numCompanyId WHERE DivisionMaster.numDivisionID=Mst.numPartner),'') ELSE '' END) AS vcPartner,
						 ISNULL(vcCustomerPO#,'') vcCustomerPO#
				  FROM    OpportunityMaster Mst JOIN Domain D ON D.numDomainID = Mst.numDomainID
						  LEFT OUTER JOIN [Currency] C ON C.numCurrencyID = Mst.numCurrencyID
						  LEFT JOIN [DivisionMaster] DM ON DM.numDivisionID = Mst.numDivisionID   
						  LEFT JOIN CompanyInfo CMP ON DM.numCompanyId = CMP.numCompanyId and CMP.numDomainID = @numDomainID  
						  LEFT JOIN dbo.AdditionalContactsInformation ACI ON ACI.numContactID = Mst.numContactID
						  LEFT JOIN [BizDocTemplate] BT ON BT.numDomainID = @numDomainID AND BT.numBizDocID = @numBizDocType 
							 AND ((BT.numBizDocTempID=ISNULL(Mst.numOppBizDocTempID, 0)) OR (ISNULL(BT.bitDefault,0)=1 AND ISNULL(BT.bitEnabled,0)=1))  
						  LEFT JOIN BillingTerms BTR ON BTR.numTermsID = ISNULL(Mst.intBillingDays,0)
						  JOIN divisionmaster div1 ON D.numDivisionID = div1.numDivisionID
						  JOIN companyinfo [Com1] ON com1.numCompanyID = div1.numCompanyID
				  WHERE  Mst.numOppID = @numReferenceID AND Mst.numDomainID = @numDomainID
			             
						 EXEC USP_OPPGetOppAddressDetails @numReferenceID,@numDomainId
             
            END
    
    
        ELSE IF @numReferenceType = 5 OR @numReferenceType = 6 OR @numReferenceType = 7 OR @numReferenceType = 8 
					OR @numReferenceType = 9 OR @numReferenceType = 10
         BEGIN
         
								DECLARE @numBizDocTempID AS NUMERIC(9)
								DECLARE @numDivisionID AS NUMERIC(9)
         
                                SELECT @numDivisionID=numDivisionID,@numBizDocTempID=(CASE WHEN @numReferenceType = 5 OR @numReferenceType = 6 OR @numReferenceType = 9 OR @numReferenceType = 10 THEN numRMATempID ELSE numBizDocTempID END)				
                                FROM    dbo.ReturnHeader RH
                                WHERE   RH.numReturnHeaderID = @numReferenceID
                                
    			---------------------
               
                                SELECT  RH.vcBizDocName AS [vcBizDocID],
                                  RH.vcRMA AS OppName,RH.vcBizDocName AS BizDcocName,@numBizDocType AS BizDoc,ISNULL(RH.numCreatedBy,0) AS Owner,
								   CMP.VcCompanyName AS CompName,RH.numContactID,RH.numCreatedBy AS BizDocOwner,
                                        RH.monTotalDiscount AS decDiscount,
                                        ISNULL(RDA.monAmount + RDA.monTotalTax - RDA.monTotalDiscount, 0) AS monAmountPaid,
                                        ISNULL(RH.vcComments, '') AS vcComments,
                                        CONVERT(VARCHAR(20), DATEADD(minute, -@ClientTimeZoneOffset, RH.dtCreatedDate)) dtCreatedDate,
                                        dbo.fn_GetContactName(RH.numCreatedby) AS numCreatedby,
                                        dbo.fn_GetContactName(RH.numModifiedBy) AS numModifiedBy,
                                        RH.dtModifiedDate,
                                        '' AS numViewedBy,
                                        '' AS dtViewedDate,
                                        0 AS tintBillingTerms,
                                        0 AS numBillingDays,
                                        0 AS [numBillingDaysName],
                                        '' AS vcBillingTermsName,
                                        0 AS tintInterestType,
                                        0 AS fltInterest,
                                        tintReturnType AS tintOPPType,
                                        @numBizDocType AS numBizDocId,
                                        dbo.fn_GetContactName(RH.numCreatedby) AS ApprovedBy,
                                        dtCreatedDate AS dtApprovedDate,
                                        0 AS bintAccountClosingDate,
                                        0 AS tintShipToType,
                                        0 AS tintBillToType,
                                        0 AS tintshipped,
                                        0 AS bintShippedDate,
                                        0 AS monShipCost,
                                        ISNULL(BT.vcBizDocFooter,D.vcBizDocFooter) AS BizdocFooter,
                                        ISNULL(BT.vcBizDocImagePath,D.vcBizDocImagePath) As vcBizDocImagePath,
                                        RH.numDivisionID,
                                         ISNULL(BT.vcPurBizDocFooter,D.vcPurBizDocFooter) AS PurBizdocFooter,
                                        ISNULL(RH.monTotalDiscount, 0) AS fltDiscount,
                                        0 AS bitDiscountType,
                                        0 AS numShipVia,
                                        '' AS vcTrackingURL,
                                        0 AS numBizDocStatus,
                                        '-' AS BizDocStatus,
                                        '-' AS ShipVia,
                                        ISNULL(C.varCurrSymbol, '') varCurrSymbol,
                                        0 AS ShippingReportCount,
                                        0 bitPartialFulfilment,
                                        vcBizDocName,
                                        dbo.[fn_GetContactName](RH.[numModifiedBy])
                                        + ', '
                                        + CONVERT(VARCHAR(20), DATEADD(MINUTE, -@ClientTimeZoneOffset, RH.dtModifiedDate)) AS ModifiedBy,
                                        dbo.[fn_GetContactName](RH.[numCreatedBy]) AS OrderRecOwner,
                                        dbo.[fn_GetContactName](DM.[numRecOwner])
                                        + ', '
                                        + dbo.[fn_GetContactName](DM.[numAssignedTo]) AS AccountRecOwner,
                                        RH.dtcreatedDate AS dtFromDate,
                                        0 AS tintTaxOperator,
                                        1 AS monTotalEmbeddedCost,
                                        2 AS monTotalAccountedCost,
                                        ISNULL(BT.bitEnabled, 0) bitEnabled,
                                        ISNULL(BT.txtBizDocTemplate, '') txtBizDocTemplate,
                                        ISNULL(BT.txtCSS, '') txtCSS,
                                        '' AS AssigneeName,
                                        '' AS AssigneeEmail,
                                        '' AS AssigneePhone,
                                        dbo.fn_GetComapnyName(RH.numDivisionId) OrganizationName,
                                        dbo.fn_GetContactName(RH.numContactID) OrgContactName,
                                        dbo.getCompanyAddress(RH.numDivisionId, 1,RH.numDomainId) CompanyBillingAddress,
                                        CASE WHEN ACI.numPhone <> ''
                                             THEN ACI.numPhone
                                                  + CASE WHEN ACI.numPhoneExtension <> ''
                                                         THEN ' - ' + ACI.numPhoneExtension
                                                         ELSE ''
                                                    END
                                             ELSE ''
                                        END OrgContactPhone,
                                        DM.vcComPhone AS OrganizationPhone,
                                        ISNULL(ACI.vcEmail, '') AS OrgContactEmail,
                                        dbo.[fn_GetContactName](RH.[numCreatedBy]) AS OnlyOrderRecOwner,
                                        1 bitAuthoritativeBizDocs,
                                        0 tintDeferred,
                                        0 AS monCreditAmount,
                                        0 AS bitRentalBizDoc,
                                        ISNULL(BT.numBizDocTempID, 0) AS numBizDocTempID,
                                        ISNULL(BT.vcTemplateName, '') AS vcTemplateName,
                                        0 AS monPAmount,
                                        ISNULL(CMP.txtComments, '') AS vcOrganizationComments,
                                        '' AS vcTrackingNo,
                                        '' AS vcShippingMethod,
                                        '' AS dtDeliveryDate,
                                        '' AS vcOppRefOrderNo,
                                        0 AS numDiscountAcntType ,
										0 AS numOppBizDocTempID
                                        ,com1.vcCompanyName AS EmployerOrganizationName,div1.vcComPhone as EmployerOrganizationPhone
										,'' AS vcReleaseDate
										,'' AS vcPartner
										,'' vcCustomerPO#
                                FROM    dbo.ReturnHeader RH
                                        JOIN Domain D ON D.numDomainID = RH.numDomainID
                                        JOIN [DivisionMaster] DM ON DM.numDivisionID = RH.numDivisionID
                                        LEFT OUTER JOIN [Currency] C ON C.numCurrencyID = DM.numCurrencyID
                                        LEFT JOIN CompanyInfo CMP ON DM.numCompanyId = CMP.numCompanyId
                                                                     AND CMP.numDomainID = @numDomainID
                                        LEFT JOIN dbo.AdditionalContactsInformation ACI ON ACI.numContactID = RH.numContactID
                                        LEFT JOIN [BizDocTemplate] BT ON BT.numDomainID = @numDomainID
                                                                         AND BT.numBizDocID = @numBizDocType	
                                                                         AND ((BT.numBizDocTempID=ISNULL(@numBizDocTempID, 0)) OR (ISNULL(BT.bitDefault,0)=1 AND ISNULL(BT.bitEnabled,0)=1))  
                                         JOIN divisionmaster div1 ON D.numDivisionID = div1.numDivisionID
										 JOIN companyinfo [Com1] ON com1.numCompanyID = div1.numCompanyID,
                                                                         dbo.GetReturnDealAmount(@numReferenceID,@numReferenceType) RDA
                                WHERE   RH.numReturnHeaderID = @numReferenceID
                                
                                	--************Customer/Vendor Billing Address************
                                		SELECT  '<pre>' + isnull(AD.vcStreet,'') + '</pre>' + isnull(AD.vcCity,'') + ' ,' + isnull(dbo.fn_GetState(AD.numState),'') + ' ' + isnull(AD.vcPostalCode,'') + ' <br>' + isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcFullAddress,
											isnull(AD.vcStreet,'') AS vcStreet,
											isnull(AD.vcCity,'') AS vcCity,
											isnull(dbo.fn_GetState(AD.numState),'') AS vcState,
											isnull(AD.vcPostalCode,'') AS vcPostalCode,
											isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcCountry,
											ISNULL(AD.vcAddressName,'') AS vcAddressName,
            								CMP.vcCompanyName
										 FROM AddressDetails AD 
										 JOIN [DivisionMaster] DM ON DM.numDivisionID = AD.numRecordID   
										 JOIN CompanyInfo CMP ON DM.numCompanyId = CMP.numCompanyId and CMP.numDomainID = AD.numDomainID  
										 --LEFT JOIN dbo.AdditionalContactsInformation ACI ON ACI.numContactID = @numContactID AND ACI.numDivisionId=DM.numDivisionID
										 WHERE AD.numDomainID=@numDomainID AND AD.numRecordID=@numDivisionID 
										 AND AD.tintAddressOf = 2 AND AD.tintAddressType = 1 AND AD.bitIsPrimary=1

                                        
                                	--************Customer/Vendor Shipping Address************
                                	SELECT  '<pre>' + isnull(AD.vcStreet,'') + '</pre>' + isnull(AD.vcCity,'') + ' ,' + isnull(dbo.fn_GetState(AD.numState),'') + ' ' + isnull(AD.vcPostalCode,'') + ' <br>' + isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcFullAddress,
											isnull(AD.vcStreet,'') AS vcStreet,
											isnull(AD.vcCity,'') AS vcCity,
											isnull(dbo.fn_GetState(AD.numState),'') AS vcState,
											isnull(AD.vcPostalCode,'') AS vcPostalCode,
											isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcCountry,
											ISNULL(AD.vcAddressName,'') AS vcAddressName,
            								CMP.vcCompanyName
										 FROM AddressDetails AD 
										 JOIN [DivisionMaster] DM ON DM.numDivisionID = AD.numRecordID   
										 JOIN CompanyInfo CMP ON DM.numCompanyId = CMP.numCompanyId and CMP.numDomainID = AD.numDomainID  
										 --LEFT JOIN dbo.AdditionalContactsInformation ACI ON ACI.numContactID = @numContactID AND ACI.numDivisionId=DM.numDivisionID
										 WHERE AD.numDomainID=@numDomainID AND AD.numRecordID=@numDivisionID 
										 AND AD.tintAddressOf = 2 AND AD.tintAddressType = 2 AND AD.bitIsPrimary=1

                                --************Employer Shipping Address************ 
									SELECT  '<pre>' + isnull(AD.vcStreet,'') + '</pre>' + isnull(AD.vcCity,'') + ' ,' + isnull(dbo.fn_GetState(AD.numState),'') + ' ' + isnull(AD.vcPostalCode,'') + ' <br>' + isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcFullAddress,
											isnull(AD.vcStreet,'') AS vcStreet,
											isnull(AD.vcCity,'') AS vcCity,
											isnull(dbo.fn_GetState(AD.numState),'') AS vcState,
											isnull(AD.vcPostalCode,'') AS vcPostalCode,
											isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcCountry,
											ISNULL(AD.vcAddressName,'') AS vcAddressName,vcCompanyName
									 FROM Domain D1 JOIN divisionmaster div1 ON D1.numDivisionID = div1.numDivisionID
										  JOIN companyinfo [Com1] ON com1.numCompanyID = div1.numCompanyID
										  JOIN dbo.AddressDetails AD ON AD.numDomainID = div1.numDomainID AND AD.numRecordID = div1.numDivisionID 
									  WHERE  D1.numDomainID = @numDomainID
										AND AD.tintAddressOf = 2 AND AD.tintAddressType = 1 AND AD.bitIsPrimary=1


								--************Employer Shipping Address************ 
								SELECT  '<pre>' + isnull(AD.vcStreet,'') + '</pre>' + isnull(AD.vcCity,'') + ' ,' + isnull(dbo.fn_GetState(AD.numState),'') + ' ' + isnull(AD.vcPostalCode,'') + ' <br>' + isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcFullAddress,
										isnull(AD.vcStreet,'') AS vcStreet,
										isnull(AD.vcCity,'') AS vcCity,
										isnull(dbo.fn_GetState(AD.numState),'') AS vcState,
										isnull(AD.vcPostalCode,'') AS vcPostalCode,
										isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcCountry,
										ISNULL(AD.vcAddressName,'') AS vcAddressName,vcCompanyName
								 FROM Domain D1 JOIN divisionmaster div1 ON D1.numDivisionID = div1.numDivisionID
									  JOIN companyinfo [Com1] ON com1.numCompanyID = div1.numCompanyID
									  JOIN dbo.AddressDetails AD ON AD.numDomainID = div1.numDomainID AND AD.numRecordID = div1.numDivisionID 
								 WHERE AD.numDomainID=@numDomainID 
								 AND AD.tintAddressOf = 2 AND AD.tintAddressType = 2 AND AD.bitIsPrimary=1
           END
		
        ELSE IF @numReferenceType = 11
		BEGIN
		      
			---------------------------------------------------------------------------------------
      
				  SELECT Mst.vcPOppName AS vcBizDocID,
						 Mst.vcPOppName AS OppName,OBD.vcBizDocID AS BizDcocName,@numBizDocType AS BizDoc,ISNULL(Mst.numRecOwner,0) AS Owner,
						 CMP.VcCompanyName AS CompName,Mst.numContactID,Mst.numCreatedBy AS BizDocOwner,
						 Mst.fltDiscount AS decDiscount,
						 (SELECT SUM(ISNULL(monAmountPaid,0)) FROM OpportunityBizDocs WHERE numOppId=MSt.numOppId) AS monAmountPaid,
						 isnull(Mst.txtComments,'') AS vcComments,
						 CONVERT(VARCHAR(20),DATEADD(minute,-@ClientTimeZoneOffset,Mst.bintCreatedDate)) dtCreatedDate,
						 dbo.fn_GetContactName(Mst.numCreatedby) AS numCreatedby,
						 dbo.fn_GetContactName(Mst.numModifiedBy) AS numModifiedBy,
						 Mst.bintModifiedDate AS dtModifiedDate,
						 '' AS numViewedBy,
						 NULL AS dtViewedDate,
						 isnull(Mst.bitBillingTerms,0) AS tintBillingTerms,
						 isnull(Mst.intBillingDays,0) AS numBillingDays,
						 ISNULL(BTR.numNetDueInDays,0) AS numBillingDaysName,
						 BTR.vcTerms AS vcBillingTermsName,
						 isnull(Mst.bitInterestType,0) AS tintInterestType,
						 isnull(Mst.fltInterest,0) AS fltInterest,
						 tintOPPType,
						 @numBizDocType AS numBizDocId,
						 Mst.bintAccountClosingDate,
						 tintShipToType,
						 tintBillToType,
						 tintshipped,
						 NULL bintShippedDate,
						 ISNULL(Mst.monShipCost ,0) AS monShipCost,
						 ISNULL(BT.vcBizDocFooter,D.vcBizDocFooter) AS BizdocFooter,
						 ISNULL(BT.vcBizDocImagePath,D.vcBizDocImagePath) As vcBizDocImagePath, 
						 Mst.numDivisionID,
						  ISNULL(BT.vcPurBizDocFooter,D.vcPurBizDocFooter) AS PurBizdocFooter,
						 isnull(Mst.fltDiscount,0) AS fltDiscount,
						 isnull(Mst.bitDiscountType,0) AS bitDiscountType,
						 0 AS numShipVia,
						 '' AS vcTrackingURL,
						 0 AS numBizDocStatus,
						 '' AS BizDocStatus,
						 '' AS ShipVia,
						 ISNULL(C.varCurrSymbol,'') varCurrSymbol,
						 0 AS ShippingReportCount,
						 0 bitPartialFulfilment,
						 vcPOppName as vcBizDocName,
						 dbo.[fn_GetContactName](Mst.[numModifiedBy])  + ', '
							+ CONVERT(VARCHAR(20),dateadd(MINUTE,-@ClientTimeZoneOffset,Mst.bintModifiedDate)) AS ModifiedBy,
						 dbo.[fn_GetContactName](Mst.[numRecOwner]) + ', ' +  dbo.[fn_GetContactName](Mst.[numAssignedTo]) AS OrderRecOwner,
						 dbo.[fn_GetContactName](DM.[numRecOwner]) + ', ' +  dbo.[fn_GetContactName](DM.[numAssignedTo]) AS AccountRecOwner,
						 Mst.bintCreatedDate AS dtFromDate,
						 Mst.tintTaxOperator,
						 0 AS monTotalEmbeddedCost,
						 0 AS monTotalAccountedCost,
						 ISNULL(BT.bitEnabled,0) bitEnabled,
						 ISNULL(BT.txtBizDocTemplate,'') txtBizDocTemplate, 
						 ISNULL(BT.txtCSS,'') txtCSS,
						 dbo.fn_GetContactName(Mst.numAssignedTo) AS AssigneeName,
						 ISNULL((SELECT vcEmail FROM dbo.AdditionalContactsInformation WHERE numContactID = Mst.numAssignedTo),'') AS AssigneeEmail,
						 ISNULL((SELECT numPhone FROM dbo.AdditionalContactsInformation WHERE numContactID = Mst.numAssignedTo),'') AS AssigneePhone,
						 dbo.fn_GetComapnyName(Mst.numDivisionId) OrganizationName,
						 dbo.fn_GetContactName(Mst.numContactID)  OrgContactName,
						 case when ACI.numPhone<>'' then ACI.numPhone +case when ACI.numPhoneExtension<>'' then ' - ' + ACI.numPhoneExtension else '' end  else '' END OrgContactPhone,
						 DM.vcComPhone as OrganizationPhone,
						 ISNULL(ACI.vcEmail,'') AS OrgContactEmail,
 						 dbo.[fn_GetContactName](Mst.[numRecOwner]) AS OnlyOrderRecOwner,
 						 0 bitAuthoritativeBizDocs,
						0 tintDeferred,0 AS monCreditAmount,
						 0 as bitRentalBizDoc,isnull(BT.numBizDocTempID,0) as numBizDocTempID,isnull(BT.vcTemplateName,'') as vcTemplateName,
						 [dbo].[GetDealAmount](Mst.numOppId ,getutcdate(),0 ) as monPAmount,
						 --(select top 1 SD.vcSignatureFile from SignatureDetail SD join OpportunityBizDocsDetails OBD on SD.numSignID=OBD.numSignID where SD.numDomainID = @numDomainID and OBD.numDomainID = @numDomainID and OBD.numSignID IS not null and OBD.numBizDocsId=opp.numOppBizDocsId order by OBD.numBizDocsPaymentDetId desc) as vcSignatureFile,
						 isnull(CMP.txtComments,'') as vcOrganizationComments,
						 ISNULL((SELECT SUBSTRING((SELECT '$^$' + ISNULL( (SELECT vcShipFieldValue FROM dbo.ShippingFieldValues WHERE numDomainID=MSt.numDomainID AND intShipFieldID IN ( SELECT intShipFieldID FROM dbo.ShippingFields WHERE numListItemID=OBD.numShipVia  AND vcFieldName='Tracking URL')),'') +'#^#'+ RTRIM(LTRIM(vcTrackingNo) )  FROM dbo.OpportunityBizDocs OBD WHERE OBD.numOppId=Mst.numOppID FOR XML PATH('')),4,200000)),'') AS  vcTrackingNo,'' AS  vcShippingMethod,
						 NULL AS dtDeliveryDate,ISNULL(Mst.vcOppRefOrderNo,'') AS vcOppRefOrderNo,ISNULL(Mst.numDiscountAcntType,0) AS numDiscountAcntType ,
						 isnull(numOppBizDocTempID,0) as numOppBizDocTempID,
						 com1.vcCompanyName AS EmployerOrganizationName,div1.vcComPhone as EmployerOrganizationPhone
						 ,dbo.FormatedDateFromDate(Mst.dtReleaseDate,@numDomainID) AS vcReleaseDate
						 ,(CASE WHEN ISNULL(Mst.numPartner,0) > 0 THEN ISNULL((SELECT CONCAT(DivisionMaster.vcPartnerCode,'-',CompanyInfo.vcCompanyName) FROM DivisionMaster INNER JOIN CompanyInfo ON DivisionMaster.numCompanyID=CompanyInfo.numCompanyId WHERE DivisionMaster.numDivisionID=Mst.numPartner),'') ELSE '' END) AS vcPartner,
						 ISNULL(Mst.vcCustomerPO#,'') vcCustomerPO#
				  FROM    OpportunityMaster Mst JOIN Domain D ON D.numDomainID = Mst.numDomainID
					LEFT JOIN OpportunityBizDocs AS OBD ON Mst.numOppid=OBD.numOppId
						  LEFT OUTER JOIN [Currency] C ON C.numCurrencyID = Mst.numCurrencyID
						  LEFT JOIN [DivisionMaster] DM ON DM.numDivisionID = Mst.numDivisionID   
						  LEFT JOIN CompanyInfo CMP ON DM.numCompanyId = CMP.numCompanyId and CMP.numDomainID = @numDomainID  
						  LEFT JOIN dbo.AdditionalContactsInformation ACI ON ACI.numContactID = Mst.numContactID
						  LEFT JOIN [BizDocTemplate] BT ON BT.numDomainID = @numDomainID AND BT.numBizDocID = @numBizDocType 
							 AND ((BT.numBizDocTempID=ISNULL(Mst.numOppBizDocTempID, 0)) OR (ISNULL(BT.bitDefault,0)=1 AND ISNULL(BT.bitEnabled,0)=1))  
						  LEFT JOIN BillingTerms BTR ON BTR.numTermsID = ISNULL(Mst.intBillingDays,0)
						  JOIN divisionmaster div1 ON D.numDivisionID = div1.numDivisionID
						  JOIN companyinfo [Com1] ON com1.numCompanyID = div1.numCompanyID
				  WHERE  OBD.numOppBizDocsId = @numReferenceID AND  Mst.numDomainID = @numDomainID
				  SET @numReferenceType=1
			      SET @numReferenceID=(SELECT TOP 1 numOppId FROM OpportunityBizDocs WHERE numOppBizDocsId = @numReferenceID)
					EXEC USP_OPPGetOppAddressDetails @numReferenceID,@numDomainId	
             
            END
    END

GO
--exec USP_GetMirrorBizDocItems @numReferenceID=37056,@numReferenceType=1,@numDomainID=1
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetMirrorBizDocItems' ) 
    DROP PROCEDURE USP_GetMirrorBizDocItems
GO
--- EXEC USP_GetMirrorBizDocItems 53,7,1
CREATE PROCEDURE [dbo].[USP_GetMirrorBizDocItems]
    (
      @numReferenceID NUMERIC(9) = NULL,
      @numReferenceType TINYINT = NULL,
      @numDomainID AS NUMERIC(9) = 0      
    )
AS 

  IF @numReferenceType = 1 OR @numReferenceType = 2 OR @numReferenceType = 3 OR @numReferenceType = 4  
            BEGIN
    			EXEC USP_MirrorOPPBizDocItems @numReferenceID,@numDomainID
END
ELSE
BEGIN
    DECLARE @DivisionID AS NUMERIC(9)                                                                                                                                                                                                                                                                                                                                            
    DECLARE @tintReturnType AS NUMERIC(9)                                                                                                                                                                                                                                                                                                                                            
    
    SELECT  @DivisionID = numDivisionID,@tintReturnType=tintReturnType
    FROM    dbo.ReturnHeader
    WHERE   numReturnHeaderID = @numReferenceID                                                                                                                                                                          
--Decimal points to show in bizdoc 
    DECLARE @DecimalPoint AS TINYINT
    SELECT  @DecimalPoint = tintDecimalPoints
    FROM    domain
    WHERE   numDomainID = @numDomainID
                                                                                          
    DECLARE @strSQL AS VARCHAR(MAX)                                                                                          
    DECLARE @strSQL1 AS VARCHAR(8000)                                       
    DECLARE @strSQLCusFields VARCHAR(1000)                      
    DECLARE @strSQLEmpFlds VARCHAR(500)                                                                         
    SET @strSQLCusFields = ''                                                                                          
    SET @strSQLEmpFlds = ''                                                                                          
    DECLARE @tintType AS TINYINT   
    
    DECLARE @intRowNum AS INT                                                  
    DECLARE @numFldID AS VARCHAR(15)                                          
    DECLARE @vcFldname AS VARCHAR(50)                                            
    DECLARE @numBizDocId AS NUMERIC(9)
    DECLARE @numBizDocTempID AS NUMERIC(9) ;
    SET @numBizDocTempID = 0
        
    IF @numReferenceType = 5 OR @numReferenceType = 6 
        BEGIN
            SELECT  @numBizDocTempID = ISNULL(numRMATempID, 0)
            FROM    dbo.ReturnHeader
            WHERE   numReturnHeaderID = @numReferenceID                       
        END 
    ELSE IF @numReferenceType = 7 OR @numReferenceType = 8  OR @numReferenceType = 9  OR @numReferenceType = 10 
            BEGIN
                SELECT  @numBizDocTempID = ISNULL(numBizdocTempID, 0)
                FROM    dbo.ReturnHeader
                WHERE   numReturnHeaderID = @numReferenceID
            END  
	
    PRINT 'numBizdocTempID : ' + CONVERT(VARCHAR(10), @numBizdocTempID)
    SELECT  @numBizDocId = numBizDocId
    FROM    dbo.BizDocTemplate
    WHERE   numBizDocTempID = @numBizDocTempID
            AND numDomainID = @numDomainID
      
     IF @numReferenceType = 5 
     BEGIN
		SELECT @numBizDocID =  numListItemID FROM dbo.ListDetails WHERE vcData='RMA' AND constFlag=1
		
		SET @tintType = 7   
	 END
	 ELSE IF @numReferenceType = 6 
	 BEGIN
		SELECT @numBizDocID =  numListItemID FROM dbo.ListDetails WHERE vcData='RMA' AND constFlag=1 
		
		SET @tintType = 8   
	 END
	 ELSE IF @numReferenceType = 7 
     BEGIN
		SELECT @numBizDocID =  numListItemID FROM dbo.ListDetails WHERE vcData='Sales Credit Memo' AND constFlag=1 
		
		SET @tintType = 7   
	 END
	 ELSE IF @numReferenceType = 8
     BEGIN
		SELECT @numBizDocID =  numListItemID FROM dbo.ListDetails WHERE vcData='Purchase Credit Memo' AND constFlag=1 
		
		SET @tintType = 8 
	 END
	 ELSE IF @numReferenceType = 9
     BEGIN
		SELECT @numBizDocID =  numListItemID FROM dbo.ListDetails WHERE vcData='Refund Receipt' AND constFlag=1 
		
		SET @tintType = 7   
	 END
	 ELSE IF @numReferenceType = 10
     BEGIN
		SELECT @numBizDocID =  numListItemID FROM dbo.ListDetails WHERE vcData='Credit Memo' AND constFlag=1 
		
		SET @tintType = 7   
	 END
														  

    SELECT  *
    INTO    #Temp1
    FROM    ( SELECT    numReturnItemID,I.vcitemname AS vcItemName,
                        0 OppBizDocItemID,
                        CASE WHEN charitemType = 'P' THEN 'Product'
                             WHEN charitemType = 'S' THEN 'Service'
                        END AS charitemType,
                        RI.vcItemDesc AS txtItemDesc,                                      
--vcitemdesc as [desc],                                      
                        dbo.fn_UOMConversion(ISNULL(i.numBaseUnit, 0),
                                             i.numItemCode, @numDomainID,
                                             ISNULL(RI.numUOMId, 0))
                        * (CASE WHEN @numReferenceType=5 OR @numReferenceType=6 THEN RI.numUnitHour ELSE RI.numUnitHourReceived END) AS numUnitHourUOM,
						CASE WHEN @numReferenceType=5 OR @numReferenceType=6 THEN RI.numUnitHour ELSE RI.numUnitHourReceived END AS numUnitHour,
                       RI.monPrice,
					   (CASE WHEN RI.numUnitHour = 0 THEN 0 ELSE CONVERT(DECIMAL(18, 4),ISNULL(RI.monTotAmount,0) / RI.numUnitHour) END)  As monUnitSalePrice,
                        (CASE WHEN @numReferenceType=5 OR @numReferenceType=6 THEN RI.numUnitHour ELSE RI.numUnitHourReceived END) 
                        * (CASE WHEN RI.numUnitHour = 0 THEN 0 ELSE CONVERT(DECIMAL(18, 4),ISNULL(RI.monTotAmount,0) / RI.numUnitHour) END) AS  Amount,
                        (CASE WHEN @numReferenceType=5 OR @numReferenceType=6 THEN RI.numUnitHour ELSE RI.numUnitHourReceived END) 
                         * (CASE WHEN RI.numUnitHour = 0 THEN 0 ELSE CONVERT(DECIMAL(18, 4),ISNULL(RI.monTotAmount,0) / RI.numUnitHour) END) AS monTotAmount/*Fo calculating sum*/,
                        RH.monTotalTax AS [Tax],
                        ISNULL(CONVERT(VARCHAR, i.monListPrice), 0) AS monListPrice,
                        i.numItemCode AS ItemCode,i.numItemCode,
                        RI.numItemCode AS [numoppitemtCode],
                        L.vcdata AS numItemClassification,
                        CASE WHEN bitTaxable = 0 THEN 'No'
                             ELSE 'Yes'
                        END AS Taxable,
                        numIncomeChartAcntId AS itemIncomeAccount,
                        'NI' AS ItemType,
                        0 AS numJournalId,
                        0 AS numTransactionId,
                        ISNULL(i.vcModelID, '') vcModelID,
                        dbo.USP_GetAttributes(RI.numWarehouseItemID,
                                              bitSerialized) AS vcAttributes,
                        '' AS vcPartNo,
                        (CASE WHEN ISNULL(RH.monTotalDiscount,0) > 0 THEN (ISNULL(RH.monTotalDiscount,0) - RI.monTotAmount)  ELSE ((CASE WHEN @numReferenceType=5 OR @numReferenceType=6 THEN RI.numUnitHour ELSE RI.numUnitHourReceived END) * RI.monPrice) - RI.monTotAmount END) AS DiscAmt,
                        '' AS vcNotes,
                        '' AS vcTrackingNo,
                        '' AS vcShippingMethod,
                        0 AS monShipCost,
                        [dbo].[FormatedDateFromDate](RH.dtCreatedDate,
                                                     @numDomainID) dtDeliveryDate,
                        RI.numWarehouseItemID,
                        ISNULL(u.vcUnitName, '') numUOMId,
                        ISNULL(RI.vcManufacturer, '') AS vcManufacturer,
                        ISNULL(V.monCost, 0) AS VendorCost,
                        '' AS vcPathForTImage,
                        i.[fltLength],
                        i.[fltWidth],
                        i.[fltHeight],
                        i.[fltWeight],
                        ISNULL(I.numVendorID, 0) numVendorID,
                        0 AS DropShip,
                        ISNULL(RI.numUOMId, 0) AS numUOM,
                        ISNULL(u.vcUnitName, '') vcUOMName,
                        dbo.fn_UOMConversion(RI.numUOMId, RI.numItemCode,
                                             @numDomainID, NULL) AS UOMConversionFactor,
                        ISNULL(W.vcWareHouse, '') AS Warehouse,
                        CASE WHEN charitemType = 'P' THEN WI.vcWHSKU
                             ELSE i.vcSKU
                        END vcSKU,
                        CASE WHEN charitemType = 'P' THEN WI.vcBarCode
                             ELSE i.numBarCodeId
                        END vcBarcode,
                        i.numShipClass,
                        [dbo].FormatedDateTimeFromDate(RH.dtCreatedDate,
                                                       @numDomainID) dtRentalStartDate,
                        [dbo].FormatedDateTimeFromDate(RH.dtCreatedDate,
                                                       @numDomainID) dtRentalReturnDate,
                       		SUBSTRING(
	(SELECT ',' + vcSerialNo + CASE WHEN isnull(I.bitLotNo,0)=1 THEN ' (' + CONVERT(VARCHAR(15),oppI.numQty) + ')' ELSE '' end 
	FROM OppWarehouseSerializedItem oppI JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID=whi.numWareHouseItmsDTLID where oppI.numReturnHeaderID=RH.numReturnHeaderID and oppI.numReturnItemID=RI.numReturnItemID
	ORDER BY vcSerialNo FOR XML PATH('')),2,200000) AS SerialLotNo
              FROM      dbo.ReturnHeader RH
                        JOIN dbo.ReturnItems RI ON RH.numReturnHeaderID = RI.numReturnHeaderID
                        LEFT JOIN dbo.Item i ON RI.numItemCode = i.numItemCode
                        LEFT JOIN ListDetails L ON i.numItemClassification = L.numListItemID
                        LEFT JOIN Vendor V ON V.numVendorID = @DivisionID
                                              AND V.numItemCode = RI.numItemCode
                        LEFT JOIN UOM u ON u.numUOMId = RI.numUOMId
                        LEFT JOIN dbo.WareHouseItems WI ON RI.numWarehouseItemID = WI.numWareHouseItemID
                                                           AND WI.numDomainID = @numDomainID
                        LEFT JOIN dbo.Warehouses W ON WI.numDomainID = W.numDomainID
                                                      AND WI.numWareHouseID = W.numWareHouseID
              WHERE     RH.numReturnHeaderID = @numReferenceID
            ) X


	--SELECT * FROM #Temp1
	
    IF @DecimalPoint = 1 
        BEGIN
            UPDATE  #Temp1
            SET     monPrice = CONVERT(DECIMAL(18, 1), monPrice),
                    Amount = CONVERT(DECIMAL(18, 1), Amount)
        END
    IF @DecimalPoint = 2 
        BEGIN
            UPDATE  #Temp1
            SET     monPrice = CONVERT(DECIMAL(18, 2), monPrice),
                    Amount = CONVERT(DECIMAL(18, 2), Amount)
        END
    IF @DecimalPoint = 3 
        BEGIN
            UPDATE  #Temp1
            SET     monPrice = CONVERT(DECIMAL(18, 3), monPrice),
                    Amount = CONVERT(DECIMAL(18, 3), Amount)
        END
    IF @DecimalPoint = 4 
        BEGIN
            UPDATE  #Temp1
            SET     monPrice = CONVERT(DECIMAL(18, 4), monPrice),
                    Amount = CONVERT(DECIMAL(18, 4), Amount)
        END

    DECLARE @strSQLUpdate AS VARCHAR(2000)
    DECLARE @vcTaxName AS VARCHAR(100)
    DECLARE @numTaxItemID AS NUMERIC(9)
    SET @numTaxItemID = 0
    SET @strSQLUpdate = ''
    SELECT TOP 1
            @vcTaxName = vcTaxName,
            @numTaxItemID = numTaxItemID
    FROM    TaxItems
    WHERE   numDomainID = @numDomainID

    EXEC ('alter table #Temp1 add [TotalTax] money')
	EXEC ('alter table #Temp1 add [CRV] money')

    IF @tintReturnType = 1
	BEGIN
        SET @strSQLUpdate = @strSQLUpdate
            + ',[TotalTax]= dbo.fn_CalBizDocTaxAmt('
            + CONVERT(VARCHAR(20), @numDomainID) + ',0,'
            + CONVERT(VARCHAR(20), @numReferenceID)
            + ',numReturnItemID,2,Amount,numUnitHourUOM)'

		SET @strSQLUpdate = @strSQLUpdate
            + ',[CRV]= dbo.fn_CalBizDocTaxAmt('
            + CONVERT(VARCHAR(20), @numDomainID) + ',1,'
            + CONVERT(VARCHAR(20), @numReferenceID)
            + ',numReturnItemID,2,Amount,numUnitHourUOM)'
	END
    ELSE 
        IF @tintReturnType = 1
		BEGIN
            SET @strSQLUpdate = @strSQLUpdate + ',[TotalTax]= 0'
			SET @strSQLUpdate = @strSQLUpdate + ',[CRV]= 0'
		END
        ELSE 
            IF @tintReturnType = 1
			BEGIN
                SET @strSQLUpdate = @strSQLUpdate
                    + ',[TotalTax]= dbo.fn_CalBizDocTaxAmt('
                    + CONVERT(VARCHAR(20), @numDomainID) + ',0,'
                     + CONVERT(VARCHAR(20), @numReferenceID)
                    + ',numReturnItemID,2,Amount,numUnitHourUOM)'

				SET @strSQLUpdate = @strSQLUpdate
                    + ',[CRV]= dbo.fn_CalBizDocTaxAmt('
                    + CONVERT(VARCHAR(20), @numDomainID) + ',1,'
                     + CONVERT(VARCHAR(20), @numReferenceID)
                    + ',numReturnItemID,2,Amount,numUnitHourUOM)'
			END
            ELSE 
			BEGIN
                SET @strSQLUpdate = @strSQLUpdate + ',[TotalTax]= 0'
				SET @strSQLUpdate = @strSQLUpdate + ',[CRV]= 0'
			END

    WHILE @numTaxItemID > 0
        BEGIN

            EXEC ( 'alter table #Temp1 add [' + @vcTaxName + '] money'
                )

            IF @tintReturnType = 1
                SET @strSQLUpdate = @strSQLUpdate + ',[' + @vcTaxName
                    + ']= dbo.fn_CalBizDocTaxAmt('
                    + CONVERT(VARCHAR(20), @numDomainID) + ','
                    + CONVERT(VARCHAR(20), @numTaxItemID) + ','
                    + CONVERT(VARCHAR(20), @numReferenceID) + ',numReturnItemID,2,Amount,numUnitHourUOM)'
            ELSE 
                SET @strSQLUpdate = @strSQLUpdate + ',[' + @vcTaxName + ']= 0'

            SELECT TOP 1
                    @vcTaxName = vcTaxName,
                    @numTaxItemID = numTaxItemID
            FROM    TaxItems
            WHERE   numDomainID = @numDomainID
                    AND numTaxItemID > @numTaxItemID

            IF @@rowcount = 0 
                SET @numTaxItemID = 0


        END
	
    PRINT 'QUERY  :' + @strSQLUpdate
	
    IF @strSQLUpdate <> '' 
        BEGIN
            SET @strSQLUpdate = 'update #Temp1 set ItemCode=ItemCode'
                + @strSQLUpdate + ' where ItemCode>0'
            PRINT @strSQLUpdate
            EXEC ( @strSQLUpdate
                )
        END

    DECLARE @vcDbColumnName NVARCHAR(50)
    SELECT TOP 1
            @numFldID = numfieldID,
            @vcFldname = vcFieldName,
            @vcDbColumnName = vcDbColumnName,
            @intRowNum = ( tintRow + 1 )
    FROM    View_DynamicCustomColumns
    WHERE   numFormID = @tintType
            AND Grp_id = 5
            AND numDomainID = @numDomainID
            AND numAuthGroupID = @numBizDocId
            AND bitCustom = 1
            AND ISNULL(numRelCntType, 0) = @numBizDocTempID
    ORDER BY tintRow                                                                                          
    WHILE @intRowNum > 0                                                                                  
        BEGIN  
            PRINT @vcDbColumnName  
            EXEC
                ( 'alter table #Temp1 add [' + @vcDbColumnName
                  + '] varchar(100)'
                )
	
            SET @strSQLUpdate = 'update #Temp1 set ItemCode=ItemCode,['
                + @vcDbColumnName + ']=dbo.GetCustFldValueItem(' + @numFldID
                + ',ItemCode) where ItemCode>0'
            EXEC ( @strSQLUpdate
                )
		                   
            SELECT TOP 1
                    @numFldID = numfieldID,
                    @vcFldname = vcFieldName,
                    @vcDbColumnName = vcDbColumnName,
                    @intRowNum = ( tintRow + 1 )
            FROM    View_DynamicCustomColumns
            WHERE   numFormID = @tintType
                    AND Grp_id = 5
                    AND numDomainID = @numDomainID
                    AND numAuthGroupID = @numBizDocId
                    AND bitCustom = 1
                    AND tintRow >= @intRowNum
                    AND ISNULL(numRelCntType, 0) = @numBizDocTempID
            ORDER BY tintRow                                                                  
		           
            IF @@rowcount = 0 
                SET @intRowNum = 0                                                                                          
        END


    SELECT  Row_NUmber() OVER ( ORDER BY numoppitemtCode ) RowNumber,
            *
    FROM    #Temp1
    ORDER BY numoppitemtCode ASC 

    DROP TABLE #Temp1

IF (SELECT COUNT(*) FROM View_DynamicColumns WHERE   numFormID = @tintType AND numDomainID = @numDomainID
            AND numAuthGroupID = @numBizDocId AND vcFieldType = 'R'AND ISNULL(numRelCntType, 0) = @numBizDocTempID)=0
BEGIN            
	INSERT INTO DycFormConfigurationDetails (numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numAuthGroupID,numRelCntType)
	select numFormId,numFieldId,1,tintRow,@numDomainID,@numBizDocId,@numBizDocTempID
	 FROM View_DynamicDefaultColumns
	 where numFormId=@tintType and bitDefault=1 and numDomainID=@numDomainID order by tintRow ASC	
	 
	 
	  IF(SELECT COUNT(*) FROM DycFrmConfigBizDocsSumm Ds where numBizDocID=@numBizDocId and Ds.numFormID=@tintType 
	and isnull(Ds.numBizDocTempID,0)=@numBizDocTempID and Ds.numDomainID=@numDomainID)=0
	BEGIN
		INSERT INTO DycFrmConfigBizDocsSumm (numFormID,numBizDocID,numFieldID,numDomainID,numBizDocTempID)
		 SELECT @tintType,@numBizDocId,DFM.numFieldID,@numDomainID,@numBizDocTempID FROM
		 DycFormField_Mapping DFFM 
			JOIN DycFieldMaster DFM on DFFM.numModuleID=DFFM.numModuleID and DFFM.numFieldID=DFM.numFieldID
			where numFormID=16 and DFFM.numDomainID is null
	END
	
END 

    SELECT  vcFieldName AS vcFormFieldName,
            vcDbColumnName,
            vcFieldDataType,
            tintRow AS intRowNum
    FROM    View_DynamicColumns
    WHERE   numFormID = @tintType
            AND numDomainID = @numDomainID
            AND numAuthGroupID = @numBizDocId
            AND vcFieldType = 'R'
            AND ISNULL(numRelCntType, 0) = @numBizDocTempID
    UNION
    SELECT  vcFieldName AS vcFormFieldName,
            vcDbColumnName,
            'C' vcFieldDataType,
            tintRow AS intRowNum
    FROM    View_DynamicCustomColumns
    WHERE   numFormID = @tintType
            AND Grp_id = 5
            AND numDomainID = @numDomainID
            AND numAuthGroupID = @numBizDocId
            AND bitCustom = 1
            AND ISNULL(numRelCntType, 0) = @numBizDocTempID
    ORDER BY tintRow
    
    END
GO
--- EXEC USP_GetMirrorBizDocItems 30,5,1

GO
/****** Object:  StoredProcedure [dbo].[USP_GetOpportunityList1]    Script Date: 03/06/2009 00:37:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- created by anoop jayaraj                                                               
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getopportunitylist1')
DROP PROCEDURE usp_getopportunitylist1
GO
CREATE PROCEDURE [dbo].[USP_GetOpportunityList1]                                                                    
	@numUserCntID numeric(9)=0,
	@numDomainID numeric(9)=0,
	@tintUserRightType tinyint=0,
	@tintSortOrder tinyint=4,
	@dtLastDate datetime,
	@OppType as tinyint,
	@CurrentPage int,
	@PageSize int,
	@TotRecs int output,
	@columnName as Varchar(50),
	@columnSortOrder as Varchar(10),
	@numDivisionID as numeric(9)=0,
	@bitPartner as bit=0,
	@ClientTimeZoneOffset as int,
	@inttype as tinyint,
	@vcRegularSearchCriteria varchar(1000)='',
	@vcCustomSearchCriteria varchar(1000)='',
	@SearchText VARCHAR(300) = '',
	@SortChar char(1)='0',
	@tintDashboardReminderType TINYINT = 0                                                              
AS                
BEGIN
         
	DECLARE @PageId as TINYINT = 0
	DECLARE @numFormId  AS INT
           
	IF @inttype = 1 
	BEGIN           
		SET @PageId =2  
		SET @numFormId=38          
	END
	ELSE IF @inttype = 2            
	BEGIN
		SET @PageId =6 
		SET @numFormId=40           
	END

	CREATE TABLE #tempForm 
	(
		tintOrder TINYINT,vcDbColumnName nvarchar(50),vcOrigDbColumnName nvarchar(50),vcFieldName nvarchar(50),vcAssociatedControlType nvarchar(50),vcListItemType varchar(3),
		numListID numeric(9),vcLookBackTableName varchar(50),bitCustomField BIT,numFieldId NUMERIC,bitAllowSorting BIT,bitAllowEdit BIT,bitIsRequired BIT,bitIsEmail BIT,
		bitIsAlphaNumeric BIT,bitIsNumeric BIT,bitIsLengthValidation BIT,intMaxLength int,intMinLength int,bitFieldMessage BIT,vcFieldMessage VARCHAR(500),ListRelID numeric(9),
		intColumnWidth int,bitAllowFiltering BIT,vcFieldDataType CHAR(1)
	)

	-- FIRST CHECK IF GRID COLUMNS ARE CONFIGURED
	DECLARE @Nocolumns AS TINYINT = 0  

	SELECT 
		@Nocolumns = ISNULL(SUM(TotalRow),0)  
	FROM
	(            
		SELECT 
			COUNT(*) TotalRow 
		FROM 
			View_DynamicColumns 
		WHERE 
			numFormId=@numFormId 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND numRelCntType = @numFormId
		UNION 
		SELECT 
			COUNT(*) TotalRow 
		FROM 
			View_DynamicCustomColumns 
		WHERE 
			numFormId=@numFormId 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND numRelCntType = @numFormId
	) TotalRows
	
	DECLARE @IsMasterConfAvailable BIT = 0
	DECLARE @numUserGroup NUMERIC(18,0)

	-- IF NoColumns=0 CHECK IF MASTER FORM CONFIGURATION IS CREATED BY ADMINISTRATOR
	IF @Nocolumns=0
	BEGIN
		SELECT @numUserGroup = numGroupID FROM UserMaster WHERE numUserDetailId = @numUserCntID 

		IF (SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = @numDomainID AND numFormID = @numFormId AND numRelCntType=@numFormId AND bitGridConfiguration = 1 AND numGroupID = @numUserGroup) > 0
		BEGIN
			SET @IsMasterConfAvailable = 1
		END
	END

	--IF MASTERCONFIGURATION IS AVAILABLE THEN LOAD IT OTHERWISE LOAD DEFAULT COLUMNS
	IF @IsMasterConfAvailable = 1
	BEGIN
		INSERT INTO DycFormConfigurationDetails 
		(
			numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth
		)
		SELECT 
			@numFormId,numFieldId,intColumnNum,intRowNum,@numDomainID,@numUserCntID,@numFormId,1,0,intColumnWidth
		FROM 
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId=@numFormId AND 
			View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicColumnsMasterConfig.numRelCntType=@numFormId AND 
			View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
			ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
		UNION
		SELECT 
			@numFormId,numFieldId,intColumnNum,intRowNum,@numDomainID,@numUserCntID,@numFormId,1,1,intColumnWidth
		FROM 
			View_DynamicCustomColumnsMasterConfig
		WHERE 
			View_DynamicCustomColumnsMasterConfig.numFormId=@numFormId AND 
			View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicCustomColumnsMasterConfig.numRelCntType=@numFormId AND 
			View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1

		INSERT INTO 
			#tempForm
		SELECT 
			(intRowNum + 1) as tintOrder, vcDbColumnName,	vcOrigDbColumnName,	ISNULL(vcCultureFieldName,vcFieldName), vcAssociatedControlType,
			vcListItemType, numListID, vcLookBackTableName,	bitCustom, numFieldId, bitAllowSorting,	bitInlineEdit, bitIsRequired,
			bitIsEmail,	bitIsAlphaNumeric, bitIsNumeric, bitIsLengthValidation,	intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,
			ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType 
		FROM 
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId=@numFormId AND 
			View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicColumnsMasterConfig.numRelCntType=@numFormId AND 
			View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
			ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
		UNION
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName, vcAssociatedControlType,'' as vcListItemType,numListID,'',
			bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,0,''
		FROM 
			View_DynamicCustomColumnsMasterConfig
		WHERE 
			View_DynamicCustomColumnsMasterConfig.numFormId=@numFormId AND 
			View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicCustomColumnsMasterConfig.numRelCntType=@numFormId AND 
			View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1
		ORDER BY 
			tintOrder ASC  
	END
	ELSE                                        
	BEGIN 
		IF @Nocolumns=0
		BEGIN
			INSERT INTO DycFormConfigurationDetails 
			(
				numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth
			)
			SELECT 
				@numFormId,numFieldId,0,Row_number() over(order by tintRow desc),@numDomainID,@numUserCntID,@numFormId,1,0,intColumnWidth
			FROM 
				View_DynamicDefaultColumns
			WHERE 
				numFormId=@numFormId 
				AND bitDefault=1 
				AND ISNULL(bitSettingField,0)=1 
				AND numDomainID=@numDomainID
			ORDER BY 
				tintOrder ASC  
		END
  

		INSERT INTO 
			#tempForm
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcOrigDbColumnName,ISNULL(vcCultureFieldName,vcFieldName),vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName,
			bitCustom,numFieldId,bitAllowSorting,bitInlineEdit,bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength,bitFieldMessage,
			vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType  
		FROM 
			View_DynamicColumns 
		WHERE 
			numFormId=@numFormId 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND ISNULL(bitSettingField,0)=1 
			AND ISNULL(bitCustom,0)=0  
			AND numRelCntType = @numFormId
		UNION
		SELECT 
			tintRow+1 AS tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName,vcAssociatedControlType,'' as vcListItemType,numListID,'',bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,0,'' 
		FROM 
			View_DynamicCustomColumns
		WHERE 
			numFormId=@numFormId 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND ISNULL(bitCustom,0)=1 
			AND numRelCntType = @numFormId
		ORDER BY 
			tintOrder ASC             
	END
	
	DECLARE  @strColumns AS VARCHAR(MAX)
	SET @strColumns = ' ADC.numContactId, DM.numDivisionID, ISNULL(DM.numTerID,0) as numTerID, Opp.numRecOwner, DM.tintCRMType, Opp.numOppID, ISNULL(Opp.numBusinessProcessID,0) AS numBusinessProcessID'
        
	
	DECLARE @tintOrder as tinyint  = 0                                                  
	DECLARE @vcFieldName as varchar(50)                                                      
	DECLARE @vcListItemType as varchar(3)                                                 
	DECLARE @vcListItemType1 as varchar(1)                                                     
	DECLARE @vcAssociatedControlType varchar(10)                                                      
	DECLARE @numListID AS numeric(9)                                                      
	DECLARE @vcDbColumnName varchar(30)                          
	DECLARE @WhereCondition varchar(2000) = ''                          
	DECLARE @vcLookBackTableName varchar(2000)                    
	DECLARE @bitCustom as bit              
	DECLARE @numFieldId as numeric  
	DECLARE @bitAllowSorting AS CHAR(1)   
	DECLARE @bitAllowEdit AS CHAR(1)                 
	DECLARE @vcColumnName AS VARCHAR(500)                  
	Declare @ListRelID AS NUMERIC(9) 
	DECLARE @SearchQuery AS VARCHAR(MAX) = ''   

	SELECT TOP 1
		@tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName, @vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,
		@numListID=numListID,@vcLookBackTableName=vcLookBackTableName,@bitCustom=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,@bitAllowEdit=bitAllowEdit,
		@ListRelID=ListRelID
	FROM 
		#tempForm 
	ORDER BY 
		tintOrder asc            
                                                   
	WHILE @tintOrder>0                                                      
	BEGIN 
		IF @bitCustom = 0            
		BEGIN 
			DECLARE @Prefix as varchar(5)                                               
			
			IF @vcLookBackTableName = 'AdditionalContactsInformation'                    
				SET @Prefix = 'ADC.'                    
			IF @vcLookBackTableName = 'DivisionMaster'                    
				SET @Prefix = 'DM.'                    
			IF @vcLookBackTableName = 'OpportunityMaster'                    
				SET @PreFix ='Opp.'      
			IF @vcLookBackTableName = 'OpportunityRecurring'
				SET @PreFix = 'OPR.'
			IF @vcLookBackTableName = 'CompanyInfo'
				SET @PreFix = 'cmp.'
			IF @vcLookBackTableName = 'ProjectProgress'
				SET @PreFix = 'PP'

			SET @vcColumnName= @vcDbColumnName+'~'+ convert(varchar(10),@numFieldId) +'~0'
      
			IF @vcAssociatedControlType='SelectBox'                                                      
			BEGIN  
				IF @vcDbColumnName = 'numStatus'
				BEGIN
					SET @strColumns=@strColumns+ ' ,(SELECT COUNT(OpportunityItems.numoppitemtCode) FROM OpportunityItems 
					WHERE OpportunityItems.bitItemPriceApprovalRequired=1 AND OpportunityItems.numOppId=Opp.numOppId) ApprovalMarginCount,ISNULL(Opp.intPromotionApprovalStatus,0) AS intPromotionApprovalStatus '
				END
				
				IF @vcDbColumnName='numPartenerSource'
				BEGIN
					SET @strColumns=@strColumns+ ' ,(Opp.numPartner) '+' ['+ @vcColumnName+']'    
					SET @strColumns=@strColumns+ ' ,(SELECT D.vcPartnerCode+''-''+C.vcCompanyName FROM DivisionMaster AS D LEFT JOIN CompanyInfo AS C ON D.numCompanyID=C.numCompanyID WHERE D.numDivisionID=Opp.numPartner) AS vcPartenerSource' 
				END
				ELSE IF @vcDbColumnName = 'numPartenerContact'
				BEGIN
					SET @strColumns=@strColumns+ ' ,(Opp.numPartenerContact) '+' ['+ @vcColumnName+']'    
					SET @strColumns=@strColumns+ ' ,(SELECT A.vcFirstName+'' ''+ A.vcLastName FROM AdditionalContactsInformation AS A WHERE A.numContactId=Opp.numPartenerContact) AS vcPartenerContact' 
				END
				ELSE IF @vcDbColumnName = 'tintSource'
				BEGIN
					SET @strColumns = @strColumns + ',dbo.fn_GetOpportunitySourceValue(ISNULL(Opp.tintSource,0),ISNULL(Opp.tintSourceType,0),Opp.numDomainID) ' + ' [' + @vcColumnName + ']'

					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'dbo.fn_GetOpportunitySourceValue(ISNULL(Opp.tintSource,0),ISNULL(Opp.tintSourceType,0),Opp.numDomainID) LIKE ''%' + @SearchText + '%'''
					END
				END
				ELSE IF @vcDbColumnName = 'numContactId'
				BEGIN
					SET @strColumns = @strColumns + ',dbo.fn_GetContactName('+@Prefix+@vcDbColumnName+') ['+ @vcColumnName+']'     
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'dbo.fn_GetContactName('+@Prefix+@vcDbColumnName+') LIKE ''%' + @SearchText + '%'''
					END             
				END
				ELSE if @vcListItemType='LI'                                                       
				BEGIN
					SET @strColumns = @strColumns+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName+']'                                                      
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'L' + CONVERT(VARCHAR(3),@tintOrder) + '.vcData LIKE ''%' + @SearchText + '%'''
					END

					SET @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID='+@vcDbColumnName                                                      
				END                                                      
				ELSE IF @vcListItemType='S'                                                       
				BEGIN
					SET @strColumns = @strColumns+',S'+ convert(varchar(3),@tintOrder)+'.vcState'+' ['+ @vcColumnName+']'   
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'S'+ convert(varchar(3),@tintOrder)+'.vcState LIKE ''%' + @SearchText + '%'''
					END
					                                                   
					SET @WhereCondition= @WhereCondition +' left join State S'+ convert(varchar(3),@tintOrder)+ ' on S'+convert(varchar(3),@tintOrder)+ '.numStateID='+@vcDbColumnName                                                      
				END     
				ELSE IF @vcListItemType = 'BP'
				BEGIN
					SET @strColumns = @strColumns + ',SPLM.Slp_Name' + ' ['	+ @vcColumnName + ']'

					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'SPLM.Slp_Name LIKE ''%' + @SearchText + '%'''
					END

					SET @WhereCondition = @WhereCondition + ' LEFT JOIN Sales_process_List_Master SPLM on SPLM.Slp_Id = OPP.numBusinessProcessID '
				END
				ELSE IF @vcListItemType='PP'
				BEGIN
					SET @strColumns = @strColumns+', CASE WHEN PP.numOppID >0 then ISNULL(PP.intTotalProgress,0) ELSE dbo.GetListIemName(OPP.numPercentageComplete) END '+' ['+ @vcColumnName+']'

					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + '(CASE WHEN PP.numOppID >0 then CONCAT(ISNULL(PP.intTotalProgress,0),''%'') ELSE dbo.GetListIemName(OPP.numPercentageComplete) END) LIKE ''%' + @SearchText + '%'''
					END
				END  
				ELSE IF @vcListItemType='T'                                                       
				BEGIN
					SET @strColumns = @strColumns+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName+']' 
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'L'+ convert(varchar(3),@tintOrder)+'.vcData LIKE ''%' + @SearchText + '%'''
					END
					                                                     
					SET @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID='+@vcDbColumnName                                                      
				END                     
				ELSE IF @vcListItemType='U'                                                   
				BEGIN
					SET @strColumns = @strColumns + ',dbo.fn_GetContactName('+@Prefix+@vcDbColumnName+') ['+ @vcColumnName+']'     
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'dbo.fn_GetContactName('+@Prefix+@vcDbColumnName+') LIKE ''%' + @SearchText + '%'''
					END              
				END  
				ELSE IF @vcListItemType = 'WI'
				BEGIN
					SET @strColumns = @strColumns + ', Case when isnull(OL.numSiteID,0)>0 then Sites.vcSiteName else WebAPI.vcProviderName end [' + @vcColumnName + ']'
				  
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + '(Case when isnull(OL.numSiteID,0)>0 then Sites.vcSiteName else WebAPI.vcProviderName end) LIKE ''%' + @SearchText + '%'''
					END 

					SET @WhereCondition= @WhereCondition +' left join WebAPI on WebAPI.WebApiId=isnull(OL.numWebApiId,0)
															left join Sites on isnull(OL.numSiteID,0)=Sites.numSIteID and Sites.numDomainID=' + convert(varchar(15),@numDomainID)                                                    
			   END                  
			END               
			ELSE IF @vcAssociatedControlType='DateField'                                                      
			BEGIN  
				SET @strColumns=@strColumns+',case when convert(varchar(11),'+@Prefix+@vcDbColumnName+')= convert(varchar(11),getdate()) then ''<b><font color=red>Today</font></b>'''      
				SET @strColumns=@strColumns+'when convert(varchar(11),'+@Prefix+@vcDbColumnName+')= convert(varchar(11),dateadd(day,-1,getdate())) then ''<b><font color=purple>YesterDay</font></b>'''      
				SET @strColumns=@strColumns+'when convert(varchar(11),'+@Prefix+@vcDbColumnName+')= convert(varchar(11),dateadd(day,1,getdate())) then''<b><font color=orange>Tommorow</font></b>'' '      
				SET @strColumns=@strColumns+'else dbo.FormatedDateFromDate('+@Prefix+@vcDbColumnName+','+convert(varchar(10),@numDomainId)+') end  ['+ @vcColumnName+']'               

				IF LEN(@SearchText) > 0
				BEGIN
					SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'dbo.FormatedDateFromDate('+@Prefix+@vcDbColumnName+','+ CONVERT(VARCHAR(10),@numDomainId)+') LIKE ''%' + @SearchText + '%'''
				END
			END              
			ELSE IF @vcAssociatedControlType='TextBox'                                                      
			BEGIN  
				SET @strColumns = @strColumns + ','+ CASE 
														WHEN @vcDbColumnName='numAge' then 'year(getutcdate())-year(bintDOB)'                   
														WHEN @vcDbColumnName='monPAmount' then 'isnull((SELECT varCurrSymbol FROM [Currency] WHERE numCurrencyID = Opp.[numCurrencyID]),'''') + '' ''+ CONVERT(VARCHAR,CONVERT(MONEY,monPAmount),1)'
														WHEN @vcDbColumnName='numDivisionID' then 'DM.numDivisionID'
														WHEN @vcDbColumnName = 'tintOppStatus' THEN 'CASE tintOppStatus WHEN 0 then ''Open'' WHEN 1 THEN ''Closed-Won'' WHEN 2 THEN ''Closed-Lost'' END '
														WHEN @vcDbColumnName = 'tintOppType' THEN 'CASE tintOppType WHEN 1 then ''Sales'' WHEN 2 THEN ''Purchase'' Else ''NA'' END ' 
														ELSE @Prefix + @vcDbColumnName 
													END +' ['+ @vcColumnName+']'  
				
				IF LEN(@SearchText) > 0
				BEGIN
					SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + CASE 
														WHEN @vcDbColumnName='numAge' then 'year(getutcdate())-year(bintDOB)'                   
														WHEN @vcDbColumnName='monPAmount' then 'isnull((SELECT varCurrSymbol FROM [Currency] WHERE numCurrencyID = Opp.[numCurrencyID]),'''') + '' ''+ CONVERT(VARCHAR,CONVERT(MONEY,monPAmount),1)'
														WHEN @vcDbColumnName='numDivisionID' then 'DM.numDivisionID'
														WHEN @vcDbColumnName = 'tintOppStatus' THEN 'CASE tintOppStatus WHEN 0 then ''Open'' WHEN 1 THEN ''Closed-Won'' WHEN 2 THEN ''Closed-Lost'' END '
														WHEN @vcDbColumnName = 'tintOppType' THEN 'CASE tintOppType WHEN 1 then ''Sales'' WHEN 2 THEN ''Purchase'' Else ''NA'' END ' 
														ELSE @Prefix + @vcDbColumnName 
													END + ' LIKE ''%' + @SearchText + '%'''
				END              
			END  
			ELSE IF @vcAssociatedControlType='TextArea'                                              
			BEGIN  
				SET @strColumns=@strColumns+','+  @Prefix + @vcDbColumnName +' ['+ @vcColumnName+']' 

				IF LEN(@SearchText) > 0
				BEGIN
					SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + @Prefix + @vcDbColumnName + ' LIKE ''%' + @SearchText + '%'''
				END     
			END
			ELSE IF @vcAssociatedControlType='Label'                                              
			BEGIN  
				SET @strColumns = @strColumns + ',' + CASE 
												WHEN @vcDbColumnName = 'CalAmount' THEN '[dbo].[getdealamount](Opp.numOppId,Getutcdate(),0)'
												WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'monDealAmount' THEN 'ISNULL((SELECT SUM(ISNULL(monDealAmount,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)'
												WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'monAmountPaid' THEN 'ISNULL((SELECT SUM(ISNULL(monAmountPaid,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)'
												WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'vcBizDocsList' THEN 'ISNULL((SELECT SUBSTRING((SELECT ''$^$'' + CAST(numOppBizDocsId AS VARCHAR(18)) +''#^#''+ vcBizDocID
												FROM OpportunityBizDocs WHERE numOppId=Opp.numOppId FOR XML PATH('''')),4,200000)),'''')'
												else @Prefix + @vcDbColumnName 
											END +' ['+ @vcColumnName+']'     
											
				IF LEN(@SearchText) > 0
				BEGIN
					SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + CASE 
												WHEN @vcDbColumnName = 'CalAmount' THEN '[dbo].[getdealamount](Opp.numOppId,Getutcdate(),0)'
												WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'monDealAmount' THEN 'ISNULL((SELECT SUM(ISNULL(monDealAmount,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)'
												WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'monAmountPaid' THEN 'ISNULL((SELECT SUM(ISNULL(monAmountPaid,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)'
												WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'vcBizDocsList' THEN 'ISNULL((SELECT SUBSTRING((SELECT ''$^$'' + CAST(numOppBizDocsId AS VARCHAR(18)) +''#^#''+ vcBizDocID
												FROM OpportunityBizDocs WHERE numOppId=Opp.numOppId FOR XML PATH('''')),4,200000)),'''')'
												else @Prefix + @vcDbColumnName
											END + ' LIKE ''%' + @SearchText + '%'''
				END           
			END
  			ELSE IF @vcAssociatedControlType='Popup' and @vcDbColumnName='numShareWith'                                                   
			BEGIN  
				SET @strColumns = @strColumns+',(SELECT SUBSTRING(
				(SELECT '','' + A.vcFirstName+'' ''+A.vcLastName + CASE WHEN ISNULL(SR.numContactType,0)>0 THEN ''('' + dbo.fn_GetListItemName(SR.numContactType) + '')'' ELSE '''' END
				FROM ShareRecord SR JOIN UserMaster UM ON UM.numUserDetailId=SR.numAssignedTo 
											JOIN AdditionalContactsInformation A ON UM.numUserDetailId=A.numContactID 
						WHERE SR.numDomainID=Opp.numDomainID AND SR.numModuleID=3 AND SR.numRecordID=Opp.numOppId
							AND UM.numDomainID=Opp.numDomainID and UM.numDomainID=A.numDomainID FOR XML PATH('''')),2,200000)) ['+ @vcColumnName+']'    
				
				IF LEN(@SearchText) > 0
				BEGIN
					SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + '(SELECT SUBSTRING(
						(SELECT '','' + A.vcFirstName+'' ''+A.vcLastName + CASE WHEN ISNULL(SR.numContactType,0)>0 THEN ''('' + dbo.fn_GetListItemName(SR.numContactType) + '')'' ELSE '''' END
						FROM ShareRecord SR JOIN UserMaster UM ON UM.numUserDetailId=SR.numAssignedTo 
													JOIN AdditionalContactsInformation A ON UM.numUserDetailId=A.numContactID 
								WHERE SR.numDomainID=Opp.numDomainID AND SR.numModuleID=3 AND SR.numRecordID=Opp.numOppId
									AND UM.numDomainID=Opp.numDomainID and UM.numDomainID=A.numDomainID FOR XML PATH('''')),2,200000)) LIKE ''%' + @SearchText + '%'''
				END            
			END  
		END               
		Else                          
		BEGIN 
			SET @vcColumnName= @vcDbColumnName +'~'+ convert(varchar(10),@numFieldId)+'~1'
               
			IF @vcAssociatedControlType = 'TextBox' or @vcAssociatedControlType = 'TextArea'          
			BEGIN              
				SET @strColumns= @strColumns+',CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value  ['+ @vcColumnName + ']'               
				SET @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values_Opp CFW'+ convert(varchar(3),@tintOrder)+ '             
				on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=Opp.numOppid   '                                                     
			END            
			ELSE IF @vcAssociatedControlType = 'CheckBox'         
			BEGIN          
				SET @strColumns = @strColumns + ',case when isnull(CFW'+ CONVERT(VARCHAR(3),@tintOrder)+'.Fld_Value,0)=0 then ''No'' when isnull(CFW'+ CONVERT(VARCHAR(3),@tintOrder)+'.Fld_Value,0)=1 then ''Yes'' end   ['+ @vcColumnName + ']'             
				SET @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values_Opp CFW'+ convert(varchar(3),@tintOrder)+ '           
					on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=Opp.numOppid   '                                                   
			END          
			ELSE IF @vcAssociatedControlType = 'DateField'       
			BEGIN            
				set @strColumns= @strColumns+',dbo.FormatedDateFromDate(CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value,'+convert(varchar(10),@numDomainId)+')  ['+ @vcColumnName + ']'               
				set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values_Opp CFW'+ convert(varchar(3),@tintOrder)+ '             
				on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=Opp.numOppid    '                                                     
			END            
			ELSE IF @vcAssociatedControlType = 'SelectBox'         
			BEGIN            
				SET @vcDbColumnName = 'DCust'+Convert(varchar(10),@numFieldId)    
				        
				SET @strColumns = @strColumns+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName + ']'                                                      
				SET @WhereCondition = @WhereCondition +' left Join CFW_FLD_Values_Opp CFW'+ convert(varchar(3),@tintOrder)+ '             
									on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=Opp.numOppid     '                                                     
				SET @WhereCondition = @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID=CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value' 
			END
			ELSE
			BEGIN
				SET @strColumns = @strColumns + CONCAT(',dbo.GetCustFldValueOpp(',@numFieldId,',opp.numOppID)') + ' [' + @vcColumnName + ']'
			END
		   
			IF LEN(@SearchText) > 0
			BEGIN
				SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + CONCAT('dbo.GetCustFldValueOpp(',@numFieldId,',opp.numOppID) LIKE ''%',@SearchText,'%''')
			END        
		END 
  
		SELECT TOP 1 
			@tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName,@vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,
			@numListID=numListID,@vcLookBackTableName=vcLookBackTableName,@bitCustom=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,@bitAllowEdit=bitAllowEdit,
			@ListRelID=ListRelID
		FROM 
			#tempForm 
		WHERE 
			tintOrder > @tintOrder-1 
		ORDER BY 
			tintOrder asc            
 
		IF @@rowcount=0 
			SET @tintOrder=0 
	END                            
		
	 
	DECLARE @strShareRedordWith AS VARCHAR(300);
	SET @strShareRedordWith=' Opp.numOppId in (select SR.numRecordID FROM ShareRecord SR WHERE SR.numDomainID=' + CONVERT(VARCHAR(15),@numDomainID) + ' AND SR.numModuleID=3 AND SR.numAssignedTo=' + CONVERT(VARCHAR(15),@numUserCntId) + ') '
                                             
	DECLARE @strSql AS VARCHAR(MAX) = ''                                       
                                                                                 
	SET @strSql = ' FROM 
						OpportunityMaster Opp                                                                     
					INNER JOIN 
						AdditionalContactsInformation ADC                                                                     
					ON
						Opp.numContactId = ADC.numContactId                                  
					INNER JOIN 
						DivisionMaster DM                                                                     
					ON 
						Opp.numDivisionId = DM.numDivisionID AND ADC.numDivisionId = DM.numDivisionID                                                                     
					INNER JOIN 
						CompanyInfo cmp                                                                     
					ON 
						DM.numCompanyID = cmp.numCompanyId                                                                         
					LEFT JOIN 
						AdditionalContactsInformation ADC1 
					ON 
						ADC1.numContactId=Opp.numRecOwner 
					LEFT JOIN 
						ProjectProgress PP 
					ON 
						PP.numOppID = Opp.numOppID
					LEFT JOIN 
						OpportunityLinking OL 
					on 
						OL.numChildOppID=Opp.numOppId ' + @WhereCondition
                                     

	-------Change Row Color-------
	DECLARE @vcCSOrigDbCOlumnName AS VARCHAR(50) = ''
	DECLARE @vcCSLookBackTableName AS VARCHAR(50) = ''
	DECLARE @vcCSAssociatedControlType AS VARCHAR(50) = ''

	CREATE TABLE #tempColorScheme
	(
		vcOrigDbCOlumnName VARCHAR(50),
		vcLookBackTableName VARCHAR(50),
		vcFieldValue VARCHAR(50),
		vcFieldValue1 VARCHAR(50),
		vcColorScheme VARCHAR(50),
		vcAssociatedControlType VARCHAR(50)
	)

	INSERT INTO 
		#tempColorScheme 
	SELECT 
		DFM.vcOrigDbCOlumnName,
		DFM.vcLookBackTableName,
		DFCS.vcFieldValue,
		DFCS.vcFieldValue1,
		DFCS.vcColorScheme,
		DFFM.vcAssociatedControlType 
	FROM 
		DycFieldColorScheme DFCS 
	JOIN 
		DycFormField_Mapping DFFM 
	ON 
		DFCS.numFieldID=DFFM.numFieldID
	JOIN 
		DycFieldMaster DFM 
	ON 
		DFM.numModuleID=DFFM.numModuleID 
		AND DFM.numFieldID=DFFM.numFieldID
	WHERE 
		DFCS.numDomainID=@numDomainID 
		AND DFFM.numFormID=@numFormId 
		AND DFCS.numFormID=@numFormId 
		AND ISNULL(DFFM.bitAllowGridColor,0)=1

	IF(SELECT COUNT(*) FROM #tempColorScheme)>0
	BEGIN
		SELECT TOP 1 @vcCSOrigDbCOlumnName=vcOrigDbCOlumnName,@vcCSLookBackTableName=vcLookBackTableName,@vcCSAssociatedControlType=vcAssociatedControlType FROM #tempColorScheme
	END   

	IF len(@vcCSOrigDbCOlumnName)>0 and len(@vcCSLookBackTableName)>0
	BEGIN
		SET @strColumns = @strColumns + ',tCS.vcColorScheme'
	END

	IF len(@vcCSOrigDbCOlumnName)>0 and len(@vcCSLookBackTableName)>0
	BEGIN
		IF @vcCSLookBackTableName = 'AdditionalContactsInformation'                  
			SET @Prefix = 'ADC.'                  
		IF @vcCSLookBackTableName = 'DivisionMaster'                  
			SET @Prefix = 'DM.'
		IF @vcCSLookBackTableName = 'CompanyInfo'                  
			SET @Prefix = 'CMP.' 
		IF @vcCSLookBackTableName = 'OpportunityMaster'                  
			SET @Prefix = 'Opp.'
		
		IF @vcCSAssociatedControlType='DateField'
		BEGIN
			SET @strSql=@strSql+' LEFT JOIN #tempColorScheme tCS on CONVERT(VARCHAR(10),' + @Prefix + @vcCSOrigDbCOlumnName + ',111) >= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue as int),GETUTCDATE()),111)
					and CONVERT(VARCHAR(10),' + @Prefix + @vcCSOrigDbCOlumnName + ',111) <= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue1 as int),GETUTCDATE()),111)'
		END
		ELSE IF @vcCSAssociatedControlType='SelectBox' OR @vcCSAssociatedControlType='ListBox' OR @vcCSAssociatedControlType='CheckBox'
		BEGIN
			SET @strSql=@strSql+' LEFT JOIN #tempColorScheme tCS on tCS.vcFieldValue=' + @Prefix + @vcCSOrigDbCOlumnName
		END
	END      

            
	IF @columnName like 'CFW.Cust%'             
	BEGIN
		SET @strSql = @strSql +' LEFT JOIN CFW_Fld_Values_Opp CFW on CFW.RecId=Opp.numOppid  and CFW.fld_id= ' + REPLACE(@columnName,'CFW.Cust','') +' '                                                     
		SET @columnName='CFW.Fld_Value'   
	END
	
	ELSE IF @columnName like 'DCust%'            
	BEGIN            
		SET @strSql = @strSql + ' left Join CFW_Fld_Values_Opp CFW on CFW.RecId=Opp.numOppid   and CFW.fld_id= '+replace(@columnName,'DCust','')                                                                                               
		SET @strSql = @strSql + ' left Join ListDetails LstCF on LstCF.numListItemID=CFW.Fld_Value  '            
		set @columnName='LstCF.vcData'                   
	END
                                        
	IF @bitPartner=1 
	BEGIN
		SET @strSql=@strSql+' LEFT JOIN OpportunityContact OppCont ON OppCont.numOppId=Opp.numOppId'                                                                               
	END

	SET @strSql = @strSql +' WHERE Opp.tintOppstatus=0 AND DM.numDomainID=' + CONVERT(VARCHAR(15),@numDomainID) + ' AND Opp.tintOppType= ' + convert(varchar(1),@OppType)                                                              

	IF @SortChar <> '0' 
		SET @strSql = @strSql + ' And Opp.vcPOppName like ''' + @SortChar + '%'''
                                
	IF @numDivisionID <> 0 
	BEGIN
		SET @strSql = @strSql + ' AND DM.numDivisionID =' + CONVERT(VARCHAR(15),@numDivisionID)                              
    END
	                                                                
	IF @tintUserRightType=1 
	BEGIN
		SET @strSql = @strSql + ' AND (Opp.numRecOwner = '
							+ CONVERT(VARCHAR(15),@numUserCntID) 
							+ ' or Opp.numAssignedTo= '
							+ convert(varchar(15),@numUserCntID)  
							+' or  Opp.numOppId in (select distinct(numOppId) from OpportunityStageDetails where numAssignTo ='
							+ convert(varchar(15),@numUserCntID)+')'
							+ case when @bitPartner=1 then ' or ( OppCont.bitPartner=1 and OppCont.numContactId='+convert(varchar(15),@numUserCntID)+ ')' else '' end 
							+ ' or ' + @strShareRedordWith +')'
    END                                        
	ELSE IF @tintUserRightType=2 
	BEGIN
		SET @strSql = @strSql + ' AND (DM.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '
							+ convert(varchar(15),@numUserCntID)+' ) or DM.numTerID=0 or Opp.numAssignedTo= '
							+ convert(varchar(15),@numUserCntID)  +' or  Opp.numOppId in (select distinct(numOppId) from OpportunityStageDetails where numAssignTo ='
							+ convert(varchar(15),@numUserCntID)+')' + ' or ' + @strShareRedordWith +')'                                                                        
	END           
	
	                                                  
	IF @tintSortOrder=1 
	BEGIN 
		SET @strSql = @strSql + ' AND (Opp.numRecOwner = ' 
							+ CONVERT(VARCHAR(15),@numUserCntID) 
							+ ' or Opp.numAssignedTo= '+ convert(varchar(15),@numUserCntID) 
							+ ' or  Opp.numOppId in (select distinct(numOppId) from OpportunityStageDetails where numAssignTo ='
							+ convert(varchar(15),@numUserCntID)+')'
							+ case when @bitPartner=1 then ' or ( OppCont.bitPartner=1 and OppCont.numContactId='+convert(varchar(15),@numUserCntID)+ ')' else '' end 
							+ ' or ' + @strShareRedordWith +')'                        
    END
	ELSE IF @tintSortOrder=2  
	BEGIN
		SET @strSql = @strSql + 'AND (Opp.numRecOwner in (select A.numContactID from AdditionalContactsInformation A where A.numManagerID='
							+ convert(varchar(15),@numUserCntID) 
							+') or Opp.numAssignedTo in (select A.numContactID from AdditionalContactsInformation A where A.numManagerID='
							+ convert(varchar(15),@numUserCntID)  
							+') or  Opp.numOppId in (select distinct(numOppId) from OpportunityStageDetails where numAssignTo in (select A.numContactID from AdditionalContactsInformation A where A.numManagerID='
							+ convert(varchar(15),@numUserCntID)+'))'
							+ case when @bitPartner=1 then ' or ( OppCont.bitPartner=1 and OppCont.numContactId in (select A.numContactID from AdditionalContactsInformation A where A.numManagerID='+ convert(varchar(15),@numUserCntID)+ '))' else '' end 
							+ ' or ' + @strShareRedordWith +')'                                                                    
	END
	ELSE IF @tintSortOrder=4  
	BEGIN
		SET @strSql=@strSql + ' AND Opp.bintCreatedDate> '''+convert(varchar(20),dateadd(day,-8,@dtLastDate))+''''                                                                    
	END
                                                 

	IF @vcRegularSearchCriteria <> '' 
	BEGIN
		SET @strSql = @strSql + ' AND ' + @vcRegularSearchCriteria 
	END

	IF @vcCustomSearchCriteria <> '' 
	BEGIN
		SET @strSql = @strSql + ' AND Opp.numOppid IN (SELECT DISTINCT CFW.RecId FROM CFW_Fld_Values_Opp CFW where ' + @vcCustomSearchCriteria + ')'
	END

	IF LEN(@SearchQuery) > 0
	BEGIN
		SET @strSql = @strSql + ' AND (' + @SearchQuery + ') '
	END

	IF ISNULL(@tintDashboardReminderType,0) = 4
	BEGIN
		SET @strSql = CONCAT(@strSql,' AND Opp.numOppID IN (','SELECT DISTINCT
																	OMInner.numOppID
																FROM 
																	StagePercentageDetails SPDInner
																INNER JOIN 
																	OpportunityMaster OMInner
																ON 
																	SPDInner.numOppID=OMInner.numOppId 
																WHERE 
																	SPDInner.numDomainId=',@numDOmainID,'
																	AND OMInner.numDomainId=',@numDOmainID,' 
																	AND OMInner.tintOppType = 1
																	AND ISNULL(OMInner.tintOppStatus,0) = 0
																	AND tinProgressPercentage <> 100',') ')

	END
	ELSE IF ISNULL(@tintDashboardReminderType,0) = 18
	BEGIN
		SET @strSql = CONCAT(@strSql,' AND Opp.numOppID IN (','SELECT DISTINCT
																	OMInner.numOppID
																FROM 
																	StagePercentageDetails SPDInner
																INNER JOIN 
																	OpportunityMaster OMInner
																ON 
																	SPDInner.numOppID=OMInner.numOppId 
																WHERE 
																	SPDInner.numDomainId=',@numDOmainID,'
																	AND OMInner.numDomainId=',@numDOmainID,' 
																	AND OMInner.tintOppType = 2
																	AND ISNULL(OMInner.tintOppStatus,0) = 0
																	AND tinProgressPercentage <> 100',') ')

	END

	DECLARE @firstRec AS INTEGER                                                              
	DECLARE @lastRec AS INTEGER    
	                                                 
	SET @firstRec= (@CurrentPage-1) * @PageSize                             
	SET @lastRec= (@CurrentPage*@PageSize+1)                                                                    


    -- GETS ROWS COUNT
	DECLARE @strTotal NVARCHAR(MAX) = 'SELECT @TotalRecords = COUNT(*) ' + @strSql
	exec sp_executesql @strTotal, N'@TotalRecords INT OUT', @TotRecs OUT
               
                              
	-- EXECUTE FINAL QUERY
	DECLARE @strFinal AS VARCHAR(MAX)
	SET @strFinal = CONCAT('SELECT ROW_NUMBER() OVER(ORDER BY ',@columnName,' ',@columnSortOrder,') ID, ',@strColumns,' INTO #tempTable',@strSql,'; SELECT * FROM #tempTable WHERE ID > ',@firstRec,' and ID <',@lastRec,'; DROP TABLE #tempTable;')
	PRINT @strFinal
	EXEC (@strFinal) 

	SELECT * FROM #tempForm

	DROP TABLE #tempForm
	DROP TABLE #tempColorScheme
END

GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetOpportunityModifiedBizDocs' ) 
    DROP PROCEDURE USP_GetOpportunityModifiedBizDocs
GO
-- =============================================  
-- Modified by: <Author,,Neelam Kapila>  
-- Create date: <Create Date,,09/29/2017>  
-- Description: <Description,,To fetch modified BizDocs >  
-- =============================================  

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[USP_GetOpportunityModifiedBizDocs]    
	@numBizDocTempID AS NUMERIC(18,0)        
AS 
BEGIN
	SELECT Top 1 numOppId, numOppBizDocsId FROM OpportunityBizDocs 
	WHERE numBizDocTempID = @numBizDocTempID
	ORDER BY numOppBizDocsId DESC
END
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetOrderItems' ) 
    DROP PROCEDURE USP_GetOrderItems
GO
CREATE PROCEDURE USP_GetOrderItems ( @tintMode TINYINT = 0,
                                     @numOppItemID NUMERIC(9) = 0,
                                     @numOppID NUMERIC(9) = 0,
                                     @numDomainId NUMERIC(9),
                                     @ClientTimeZoneOffset INT,
									 @vcOppItemIDs VARCHAR(2000)='' )
AS 
BEGIN
	
    IF @tintMode = 1 --Add Return grid data & For Recurring Get Original Units to split
        BEGIN
            SELECT  OI.numoppitemtCode,
                    OI.numItemCode,
                    OI.vcItemName,
                    OI.vcItemDesc,
					OI.vcNotes as txtNotes,ISNULL(OI.numCost,0) AS numCost,
                    OI.numUnitHour numOriginalUnitHour,
					CAST(( dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), OI.numItemCode,
                                    OM.numDomainId, ISNULL(OI.numUOMId, 0))
						* OI.numUnitHour ) AS NUMERIC(18, 2)) AS numUnitHour,
					OI.monPrice
            FROM    dbo.OpportunityMaster OM
                    INNER JOIN dbo.OpportunityItems OI ON OM.numOppId = OI.numOppId
                    INNER JOIN dbo.Item I ON I.numItemCode = OI.numItemCode
            WHERE   OM.numDomainId = @numDomainId
                    AND OM.numOppId = @numOppID
                    
        END
 
    IF @tintMode = 2 -- Get single item based on PK, or all items
		-- Used by Sales/Purchase Template Load items in New Sales Order
        BEGIN 
            SELECT  OI.numoppitemtCode,
                    OI.numOppId,
                    OI.numItemCode,
					OI.bitItemPriceApprovalRequired,
                    OI.numUnitHour numOriginalUnitHour,
                    OI.monPrice,
                    OI.monTotAmount,
                    OI.numSourceID,
					ISNULL(OI.numCost,0) AS numCost,
                    OI.vcItemDesc,
					OI.vcNotes as txtNotes,
                    OI.numWarehouseItmsID,
                    OI.vcType,
                    OI.vcAttributes,
                    OI.bitDropShip,
                    ISNULL(OI.numUnitHourReceived,0) AS numUnitHourReceived,
                    OI.bitDiscountType,
                    OI.fltDiscount,
                    OI.monTotAmtBefDiscount,
                    ISNULL(OI.numQtyShipped,0) AS numQtyShipped,
                    OI.vcItemName,
                    OI.vcModelID,
                    OI.vcPathForTImage,
                    OI.vcManufacturer,
                    OI.monVendorCost,
                    ISNULL(OI.numUOMId, 0) AS numUOMId,
                    ISNULL(OI.numUOMId, 0) numUOM, -- used from add edit order
                    ISNULL(U.vcUnitName, '') vcUOMName,
                    dbo.fn_UOMConversion(OI.numUOMId, OI.numItemCode,
                                         OM.numDomainId, NULL) AS UOMConversionFactor,
                    OI.bitWorkOrder,
                    isnull(OI.numVendorWareHouse,0) as numVendorWareHouse,
                    OI.numShipmentMethod,
                    OI.numSOVendorId,
                    OI.numProjectID,
                    OI.numClassID,
                    OI.numProjectStageID,
                    OI.numToWarehouseItemID,
                    WItems.numWarehouseID,
                    W.vcWareHouse AS Warehouse,
					CAST(( dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), OI.numItemCode,
                                    OM.numDomainId, ISNULL(OI.numUOMId, 0))
						* OI.numUnitHour ) AS NUMERIC(18, 2)) AS numUnitHour,
					isnull(OI.numClassID,0) as numClassID,
					isnull(OI.numProjectID,0) as numProjectID,
					dbo.fn_GetUOMName(I.numBaseUnit) AS vcBaseUOMName,
					OI.bitDropShip AS bitAllowDropShip,
					I.numVendorID,
					ISNULL(I.bitFreeShipping,0) AS [bitFreeShipping],
					ISNULL(I.txtItemDesc,'') txtItemDesc,
					ISNULL(I.vcModelID,'') vcModelID,
					ISNULL(I.vcManufacturer,'') vcManufacturer,
					ISNULL(I.numBarCodeId,'') numBarCodeId,
					ISNULL(I.bitSerialized,0) bitSerialized,
					ISNULL(I.bitKitParent,0) bitKitParent,
					ISNULL(I.bitAssembly,0) bitAssembly,
					ISNULL(I.IsArchieve,0) IsArchieve,
					ISNULL(I.bitLotNo,0) bitLotNo,
					ISNULL(WItems.monWListPrice,0) monWListPrice,
					ISNULL(I.bitTaxable, 0) bitTaxable,
					ISNULL(I.numItemClassification,0) AS numItemClassification,
                    I.charItemType,
                    I.bitKitParent,
					I.bitAssembly,
					I.bitSerialized,
					ISNULL(I.fltWeight,0) AS [fltWeight],
					ISNULL(I.fltLength,0) AS [fltLength],
					ISNULL(I.fltWidth,0) AS [fltWidth],
					ISNULL(I.fltHeight,0) AS [fltHeight],
					CASE WHEN I.numItemGroup > 0 AND ISNULL(I.bitMatrix,0)=0 THEN ISNULL(WItems.vcWHSKU,ISNULL(I.vcSKU,'')) ELSE ISNULL(I.vcSKU,'') END AS [vcSKU],
					(CASE WHEN ISNULL(I.bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(I.monAverageCost,0) END)  AS monAverageCost,
					CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numOnHand),0) AS VARCHAR(30)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numOnHand,
					CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numOnOrder),0) AS VARCHAR(30)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numOnOrder,
					CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numAllocation),0) AS VARCHAR(30)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numAllocation,
					CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numBackOrder),0) AS VARCHAR(30)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numBackOrder,
					ISNULL(OI.vcAttrValues,'') AS vcAttrValues,
					ISNULL(OI.numSortOrder,0) numSortOrder
					,(CASE WHEN OM.tintOppType=2 THEN ISNULL(vcNotes,'') ELSE '' END) AS vcVendorNotes
            FROM    dbo.OpportunityItems OI
                    INNER JOIN dbo.OpportunityMaster OM ON OM.numOppId = OI.numOppId
                    INNER JOIN dbo.Item I ON I.numItemCode = OI.numItemCode
                    LEFT JOIN UOM U ON U.numUOMId = OI.numUOMId
                    LEFT JOIN WareHouseItems WItems ON WItems.numWareHouseItemID = OI.numWarehouseItmsID
                    LEFT JOIN Warehouses W ON W.numWarehouseID = WItems.numWarehouseID
            WHERE   OI.numOppId = @numOppID
                    AND ( OI.numoppitemtCode = @numOppItemID
                          OR @numOppItemID = 0
                        )
                    AND OM.numDomainId = @numDomainId
                    
        END
 
    IF @tintMode = 3 -- Get item for SO to PO and vice versa creation
        BEGIN
            DECLARE @tintOppType AS TINYINT
			SELECT @tintOppType=tintOppType FROM OpportunityMaster WHERE numOppId=@numOppID

			IF @tintOppType = 1
			BEGIN
				DECLARE @TempItems TABLE
				(
					ID INT IDENTITY(1,1),numItemCode NUMERIC(18,0),vcItemName VARCHAR(300),txtNotes Varchar(MAX),charItemType char,vcModelID VARCHAR(100),vcItemDesc TEXT,vcType VARCHAR(20),vcPathForTImage VARCHAR(200),
					numWarehouseItmsID NUMERIC(18,0),vcAttributes VARCHAR(500),bitDropShip BIT,numUnitHour FLOAT,numUOM NUMERIC(18,0),vcUOMName VARCHAR(100),UOMConversionFactor DECIMAL(18,5),
					monPrice MONEY,numVendorID NUMERIC(18,0),numCost decimal(30, 16),vcPartNo VARCHAR(300),monVendorCost MONEY,numSOVendorId NUMERIC(18,0),numClassID NUMERIC(18,0),numProjectID NUMERIC(18,0),tintLevel TINYINT, vcAttrValues VARCHAR(500)
				)

				INSERT INTO 
					@TempItems
				SELECT
					OI.numItemCode,OI.vcItemName,OI.vcNotes as txtNotes,I.charItemType,OI.vcModelID,OI.vcItemDesc,OI.vcType,ISNULL(OI.vcPathForTImage, ''),ISNULL(OI.numWarehouseItmsID,0),ISNULL(OI.vcAttributes, ''),
					ISNULL(OI.bitDropShip, 0),OI.numUnitHour * dbo.fn_UOMConversion(I.numBaseUnit, OI.numItemCode,OM.numDomainId, OI.numUOMId),ISNULL(OI.numUOMId, 0),ISNULL(U.vcUnitName, ''),dbo.fn_UOMConversion(OI.numUOMId, OI.numItemCode,OM.numDomainId, NULL) AS UOMConversionFactor,
					OI.monPrice,ISNULL(I.numVendorID, 0),ISNULL(OI.numCost,0),ISNULL(V.vcPartNo, ''),ISNULL(V.monCost, 0),ISNULL(OI.numSOVendorId, 0),0,0,0,ISNULL(vcAttrValues,'')
				FROM    
					dbo.OpportunityItems OI
					INNER JOIN dbo.OpportunityMaster OM ON OM.numOppId = OI.numOppId
					INNER JOIN dbo.Item I ON I.numItemCode = OI.numItemCode
					LEFT JOIN UOM U ON U.numUOMId = OI.numUOMId
					LEFT JOIN Vendor V ON V.numVendorID = I.numVendorID	AND I.numItemCode = V.numItemCode
				WHERE   
					OI.numOppId = @numOppID
					AND OM.numDomainId = @numDomainId
					AND ISNULL(I.bitKitParent,0) = 0
					AND ISNULL(I.bitAssembly,0) = 0
					AND 1 = (CASE WHEN (SELECT COUNT(*) FROM OpportunityLinking OMInner INNER JOIN OpportunityItems OIInner ON OMInner.numChildOppID=OIInner.numOppId WHERE OMInner.numParentOppID = @numOppID AND OIInner.numItemCode=OI.numItemCode AND ISNULL(OIInner.numWarehouseItmsID,0)=ISNULL(OI.numWarehouseItmsID,0)) > 0 THEN 0 ELSE 1 END)


				--INSERT KIT CHILD ITEMS WHICH ARE NOT KIT AND NOT YET ADDED
				INSERT INTO 
					@TempItems
				SELECT
					OKI.numChildItemID,'' as txtNotes,I.vcItemName,I.charItemType,I.vcModelID,I.txtItemDesc,(CASE WHEN I.charitemType = 'P' THEN 'Product' WHEN I.charitemType = 'S' THEN 'Service' END),
					ISNULL((SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = I.numItemCode AND bitDefault=1),''),ISNULL(OKI.numWareHouseItemId,0),
					ISNULL(dbo.USP_GetAttributes(OKI.numWarehouseItemID, bitSerialized),''),0,OKI.numQtyItemsReq,ISNULL(OKI.numUOMId, 0),ISNULL(U.vcUnitName, ''),
					dbo.fn_UOMConversion(OKI.numUOMId, OKI.numChildItemID,OM.numDomainId, NULL),
					(CASE WHEN I.charitemType = 'P' THEN WI.monWListPrice ELSE I.monListPrice END),
					ISNULL(I.numVendorID, 0),ISNULL(t1.numCost,0),ISNULL(V.vcPartNo, ''),ISNULL(V.monCost, 0),0,0,0,1,''
				FROM    
					dbo.OpportunityKitItems OKI
				INNER JOIN @TempItems t1 ON OKI.numChildItemID = t1.numItemCode AND ISNULL(OKI.numWareHouseItemId,0) = ISNULL(t1.numWarehouseItmsID,0)
				LEFT JOIN WarehouseItems WI ON OKI.numWareHouseItemId = WI.numWarehouseItemID
				INNER JOIN dbo.OpportunityMaster OM ON OM.numOppId = OKI.numOppId
				INNER JOIN dbo.Item I ON I.numItemCode = OKI.numChildItemID
				LEFT JOIN UOM U ON U.numUOMId = OKI.numUOMId
				LEFT JOIN Vendor V ON V.numVendorID = I.numVendorID	AND I.numItemCode = V.numItemCode
				WHERE   
					OKI.numOppId = @numOppID
					AND OM.numDomainId = @numDomainId
					AND ISNULL(I.bitKitParent,0) = 0
					AND ISNULL(I.bitAssembly,0) = 0
					AND t1.ID IS NULL


				-- FOR KIT CHILD ITEMS WHICH ARE ALREADY ADDED AT PARENT LEVEL - UPDATE QTY ONLY
				UPDATE 
					t2
				SET 
					t2.numUnitHour = ISNULL(t2.numUnitHour,0) + ISNULL(OKI.numQtyItemsReq,0),
					t2.monPrice = (CASE 
									WHEN ISNULL(t2.monPrice,0) < (CASE WHEN I.charitemType = 'P' THEN ISNULL(WI.monWListPrice,0) ELSE ISNULL(I.monListPrice,0) END)  
									THEN t2.monPrice
									ELSE (CASE WHEN I.charitemType = 'P' THEN ISNULL(WI.monWListPrice,0) ELSE ISNULL(I.monListPrice,0) END)
									END)
				FROM
					@TempItems t2
				INNER JOIN
					OpportunityKitItems OKI 
				ON
					OKI.numChildItemID = t2.numItemCode AND ISNULL(OKI.numWareHouseItemId,0) = ISNULL(t2.numWarehouseItmsID,0)
				LEFT JOIN WarehouseItems WI ON OKI.numWareHouseItemId = WI.numWarehouseItemID
				INNER JOIN dbo.OpportunityMaster OM ON OM.numOppId = OKI.numOppId
				INNER JOIN dbo.Item I ON I.numItemCode = OKI.numChildItemID
				LEFT JOIN UOM U ON U.numUOMId = OKI.numUOMId
				LEFT JOIN Vendor V ON V.numVendorID = I.numVendorID	AND I.numItemCode = V.numItemCode
				WHERE   
					OKI.numOppId = @numOppID
					AND OM.numDomainId = @numDomainId
					AND ISNULL(I.bitKitParent,0) = 0
					AND ISNULL(I.bitAssembly,0) = 0
					AND t2.tintLevel <> 1

				--INSERT SUB KIT CHILD ITEMS WHICH ARE NOT KIT AND NOT YET ADDED
				INSERT INTO 
					@TempItems
				SELECT
					OKCI.numItemID,'' as txtNotes,I.vcItemName,I.charItemType,I.vcModelID,I.txtItemDesc,(CASE WHEN I.charitemType = 'P' THEN 'Product' WHEN I.charitemType = 'S' THEN 'Service' END),
					ISNULL((SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = I.numItemCode AND bitDefault=1),''),ISNULL(OKCI.numWareHouseItemId,0),
					ISNULL(dbo.USP_GetAttributes(OKCI.numWarehouseItemID, bitSerialized),''),0,OKCI.numQtyItemsReq,ISNULL(OKCI.numUOMId, 0),ISNULL(U.vcUnitName, ''),
					dbo.fn_UOMConversion(OKCI.numUOMId, OKCI.numItemID,OM.numDomainId, NULL),
					(CASE WHEN I.charitemType = 'P' THEN WI.monWListPrice ELSE I.monListPrice END),
					ISNULL(I.numVendorID, 0),ISNULL(t1.numCost,0),ISNULL(V.vcPartNo, ''),ISNULL(V.monCost, 0),0,0,0,2,''
				FROM    
					dbo.OpportunityKitChildItems OKCI
				INNER JOIN @TempItems t1 ON OKCI.numItemID = t1.numItemCode AND ISNULL(OKCI.numWareHouseItemId,0) = ISNULL(t1.numWarehouseItmsID,0)
				LEFT JOIN WarehouseItems WI ON OKCI.numWareHouseItemId = WI.numWarehouseItemID
				INNER JOIN dbo.OpportunityMaster OM ON OM.numOppId = OKCI.numOppId
				INNER JOIN dbo.Item I ON I.numItemCode = OKCI.numItemID
				LEFT JOIN UOM U ON U.numUOMId = OKCI.numUOMId
				LEFT JOIN Vendor V ON V.numVendorID = I.numVendorID	AND I.numItemCode = V.numItemCode
				WHERE   
					OKCI.numOppId = @numOppID
					AND OM.numDomainId = @numDomainId
					AND ISNULL(I.bitKitParent,0) = 0
					AND ISNULL(I.bitAssembly,0) = 0
					AND t1.ID IS NULL

				-- FOR SUB KIT CHILD ITEMS WHICH ARE ALREADY ADDED AT PARENT LEVEL - UPDATE QTY ONLY
				UPDATE 
					t2
				SET 
					t2.numUnitHour = ISNULL(t2.numUnitHour,0) + ISNULL(OKCI.numQtyItemsReq,0),
					t2.monPrice = (CASE 
									WHEN ISNULL(t2.monPrice,0) < (CASE WHEN I.charitemType = 'P' THEN ISNULL(WI.monWListPrice,0) ELSE ISNULL(I.monListPrice,0) END)  
									THEN t2.monPrice
									ELSE (CASE WHEN I.charitemType = 'P' THEN ISNULL(WI.monWListPrice,0) ELSE ISNULL(I.monListPrice,0) END)
									END)
				FROM
					@TempItems t2
				INNER JOIN
					OpportunityKitChildItems OKCI 
				ON
					OKCI.numItemID = t2.numItemCode AND ISNULL(OKCI.numWareHouseItemId,0) = ISNULL(t2.numWarehouseItmsID,0)
				LEFT JOIN WarehouseItems WI ON OKCI.numWareHouseItemId = WI.numWarehouseItemID
				INNER JOIN dbo.OpportunityMaster OM ON OM.numOppId = OKCI.numOppId
				INNER JOIN dbo.Item I ON I.numItemCode = OKCI.numItemID
				LEFT JOIN UOM U ON U.numUOMId = OKCI.numUOMId
				LEFT JOIN Vendor V ON V.numVendorID = I.numVendorID	AND I.numItemCode = V.numItemCode
				WHERE   
					OKCI.numOppId = @numOppID
					AND OM.numDomainId = @numDomainId
					AND ISNULL(I.bitKitParent,0) = 0
					AND ISNULL(I.bitAssembly,0) = 0
					AND t2.tintLevel <> 2

				SELECT * FROM @TempItems
			END
			ELSE
			BEGIN
				SELECT  OI.numoppitemtCode,
                    OI.numOppId,
					OI.vcNotes as txtNotes,
					OI.bitItemPriceApprovalRequired,
                    OI.vcItemDesc,
                    ISNULL(OI.vcPathForTImage, '') vcPathForTImage,
                    ISNULL(OI.numWarehouseItmsID, 0) numWarehouseItmsID,
                    OI.vcItemName,
                    OI.vcModelID,
					ISNULL(OI.numCost,0) numCost,
                    ISNULL(OI.vcAttributes, '') vcAttributes,
                    OI.vcType,
                    ISNULL(OI.bitDropShip, 0) bitDropShip,
                    OI.numItemCode,
                    OI.numUnitHour numOriginalUnitHour,
                    ISNULL(OI.numUOMId, 0) AS numUOM,
                    ISNULL(U.vcUnitName, '') vcUOMName,
                    dbo.fn_UOMConversion(OI.numUOMId, OI.numItemCode,
                                         OM.numDomainId, NULL) AS UOMConversionFactor,
                    ISNULL(I.numVendorID, 0) numVendorID,
                    ISNULL(V.monCost, 0) monVendorCost,
                    ISNULL(V.vcPartNo, '') vcPartNo,
                    OI.monPrice,
                    ISNULL(OI.numSOVendorId, 0) numSOVendorId,
                    ISNULL(I.charItemType, 'N') charItemType,
					CAST(( dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), OI.numItemCode,
                                    OM.numDomainId, ISNULL(OI.numUOMId, 0))
						* OI.numUnitHour ) AS NUMERIC(18, 2)) AS numUnitHour,
					isnull(OI.numClassID,0) as numClassID,isnull(OI.numProjectID,0) as numProjectID,ISNULL(I.fltWeight,0) AS [fltWeight],ISNULL(I.bitFreeShipping,0) AS [bitFreeShipping],
					ISNULL(I.fltLength,0) AS [fltLength],
					ISNULL(I.fltWidth,0) AS [fltWidth],
					ISNULL(I.fltHeight,0) AS [fltHeight],
					ISNULL((SELECT vcWHSKU FROM dbo.WareHouseItems WHERE numWarehouseItemID = OI.numWarehouseItmsID ),ISNULL(I.vcSKU,'')) AS [vcSKU],
					ISNULL(vcAttrValues,'') AS vcAttrValues
				FROM    dbo.OpportunityItems OI
						INNER JOIN dbo.OpportunityMaster OM ON OM.numOppId = OI.numOppId
						INNER JOIN dbo.Item I ON I.numItemCode = OI.numItemCode
						LEFT JOIN UOM U ON U.numUOMId = OI.numUOMId
						LEFT JOIN Vendor V ON V.numVendorID = I.numVendorID
												AND I.numItemCode = V.numItemCode
				WHERE   OI.numOppId = @numOppID
						AND OM.numDomainId = @numDomainId
			END
        END
        IF @tintMode = 4 -- Add Edit Order items
        BEGIN 
            SELECT  OI.numoppitemtCode,
                    OI.numOppId,
					OI.vcNotes as txtNotes,
                    OI.numItemCode,
					OI.bitItemPriceApprovalRequired,
					OI.numUnitHour AS numOrigUnitHour,
                    OI.numUnitHour,
                    OI.monPrice,
                    OI.monTotAmount,
                    OI.numSourceID,
                    OI.vcItemDesc,
					OI.vcNotes,
					ISNULL(OI.numCost,0) numCost,
                    OI.numWarehouseItmsID,
                    OI.vcType ItemType,
                    OI.vcAttributes Attributes,
                    OI.bitDropShip DropShip,
                    OI.numUnitHourReceived,
                    OI.bitDiscountType,
                    OI.fltDiscount,
                    OI.monTotAmtBefDiscount,
					ISNULL(OI.monTotAmtBefDiscount,0) - ISNULL(OI.monTotAmount,0) AS TotalDiscountAmount,
                    OI.numQtyShipped,
                    OI.vcItemName,
                    OI.vcModelID,
                    OI.vcPathForTImage,
                    OI.vcManufacturer,
                    OI.monVendorCost,
                    ISNULL(OI.numUOMId, 0) AS numUOM,
                    ISNULL(U.vcUnitName, '') vcUOMName,
                    ISNULL(dbo.fn_UOMConversion(OI.numUOMId, OI.numItemCode,
                                         OM.numDomainId, NULL),1) AS UOMConversionFactor,
                    OI.bitWorkOrder,
                    isnull(OI.numVendorWareHouse,0) as numVendorWareHouse,
                    OI.numShipmentMethod,
                    OI.numSOVendorId,
                    OI.numProjectStageID,
                    OI.numToWarehouseItemID,
                   UPPER(I.charItemType) charItemType,
                    WItems.numWarehouseID,
                    W.vcWareHouse AS Warehouse,
                    0 as Op_Flag,
					CAST(( dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), OI.numItemCode,
                                    OM.numDomainId, ISNULL(OI.numUOMId, 0))
						* OI.numUnitHour ) AS NUMERIC(18, 2)) AS numUnitHour,
					isnull(OI.numClassID,0) as numClassID,isnull(OI.numProjectID,0) as numProjectID,
					CASE WHEN (SELECT COUNT(*) FROM OpportunityBizDocs OB JOIN dbo.OpportunityBizDocItems OBI ON ob.numOppBizDocsId=obi.numOppBizDocID
					AND (ob.bitAuthoritativeBizDocs=1 OR ob.numBizDocId=304) AND obi.numOppItemID=oi.numoppitemtCode AND ob.numOppId=OM.numOppId)>0 THEN 1 ELSE 0 END  AS bitIsAuthBizDoc,
					ISNULL(OI.numUnitHourReceived,0) AS numUnitHourReceived,ISNULL(numQtyShipped,0) AS numQtyShipped,dbo.fn_GetUOMName(I.numBaseUnit) AS vcBaseUOMName,ISNULL(I.fltWeight,0) AS [fltWeight],ISNULL(I.bitFreeShipping,0) AS [bitFreeShipping],
					ISNULL(I.fltLength,0) AS [fltLength],
					ISNULL(I.fltWidth,0) AS [fltWidth],
					ISNULL(I.fltHeight,0) AS [fltHeight],
					ISNULL(WItems.vcWHSKU,ISNULL(I.vcSKU,'')) AS [vcSKU],
					(CASE WHEN ISNULL(I.bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(I.monAverageCost,0) END) AS monAverageCost,
					ISNULL(I.bitFreeShipping,0) AS [bitFreeShipping],
					ISNULL(I.txtItemDesc,'') txtItemDesc,
					ISNULL(I.numBarCodeId,'') numBarCodeId,
					ISNULL(I.bitSerialized,0) bitSerialized,
					ISNULL(I.bitKitParent,0) bitKitParent,
					ISNULL(I.bitAssembly,0) bitAssembly,
					ISNULL(I.IsArchieve,0) IsArchieve,
					ISNULL(I.bitLotNo,0) bitLotNo,
					ISNULL(WItems.monWListPrice,0) monWListPrice,
					ISNULL(I.bitTaxable, 0) bitTaxable,
					ISNULL(I.numItemClassification,0) AS numItemClassification,
					CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numOnHand),0) AS VARCHAR(30)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numOnHand,
					CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numOnOrder),0) AS VARCHAR(30)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numOnOrder,
					CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numAllocation),0) AS VARCHAR(30)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numAllocation,
					CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numBackOrder),0) AS VARCHAR(30)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numBackOrder,
					case when ISNULL(I.bitAssembly,0)=1 then dbo.fn_GetAssemblyPossibleWOQty(I.numItemCode) else 0 end AS numMaxWorkOrderQty,
					ISNULL((SELECT COUNT(*) FROM OppWarehouseSerializedItem WHERE numOppId=@numOppID AND numOppItemID=OI.numoppitemtCode),0) AS numSerialNoAssigned,
					(CASE WHEN (SELECT COUNT(*) FROM OpportunityBizDocs OB JOIN dbo.OpportunityBizDocItems OBI ON OB.numOppBizDocsId=OBI.numOppBizDocID WHERE OB.numOppId = OI.numOppId AND OBI.numOppItemID=OI.numoppitemtCode AND OB.numBizDocId=296) > 0 THEN 1 ELSE 0 END) AS bitAddedFulFillmentBizDoc,
					ISNULL(OI.vcAttrValues,'') AS AttributeIDs,
					ISNULL(I.numContainer,0) AS numContainer
					,ISNULL(I.numNoItemIntoContainer,0) AS numContainerQty
					,ISNULL(numPromotionID,0) AS numPromotionID,
					ISNULL(bitPromotionTriggered,0) AS bitPromotionTriggered,
					ISNULL(vcPromotionDetail,'') AS vcPromotionDetail,
					CASE WHEN ISNULL(OI.numSortOrder,0)=0 THEN row_number() OVER (ORDER BY OI.numOppId) ELSE ISNULL(OI.numSortOrder,0) END AS numSortOrder
					,ISNULL(vcNotes,'') AS vcVendorNotes
--					0 AS [Tax0],0 AS bitTaxable0
            FROM    dbo.OpportunityItems OI
                    INNER JOIN dbo.OpportunityMaster OM ON OM.numOppId = OI.numOppId
                    INNER JOIN dbo.Item I ON I.numItemCode = OI.numItemCode
                    LEFT JOIN UOM U ON U.numUOMId = OI.numUOMId
                    LEFT JOIN WareHouseItems WItems ON WItems.numWareHouseItemID = OI.numWarehouseItmsID
                    LEFT JOIN Warehouses W ON W.numWarehouseID = WItems.numWarehouseID
            WHERE   OI.numOppId = @numOppID
                    AND OM.numDomainId = @numDomainId
			ORDER BY OI.numSortOrder
                    
			SELECT  
				vcSerialNo,
				vcComments AS Comments,
				numOppItemID AS numoppitemtCode,
				W.numWarehouseItmsDTLID,
				numWarehouseItmsID AS numWItmsID,
				dbo.Fn_getattributes(W.numWarehouseItmsDTLID, 1) Attributes
			FROM    
				WareHouseItmsDTL W
			JOIN 
				OppWarehouseSerializedItem O 
			ON 
				O.numWarehouseItmsDTLID = W.numWareHouseItmsDTLID
			WHERE   
				numOppID = @numOppID         
        END
        
        
		IF @tintMode = 5 -- Get WebApi Order Items to enter WebApi Order line Item Details 
		BEGIN 
			SELECT OI.numOppItemtCode,OI.numOppId,OI.numItemCode,OI.vcNotes as txtNotes,ISNULL(OI.numCost,0) numCost,
			ISNULL((SELECT vcWHSKU FROM dbo.WareHouseItems WHERE WareHouseItems.numWarehouseItemID = OI.numWarehouseItmsID),ISNULL(IT.vcSKU,'')) AS [vcSKU],
			OI.vcItemDesc,OMAPI.vcAPIOppId
			FROM OpportunityItems OI 
			INNER JOIN dbo.OpportunityMaster OM ON OI.numOppId = OM.numOppId 
			INNER JOIN dbo.OpportunityMasterAPI OMAPI ON OM.numOppId = OMAPI.numOppId
			INNER JOIN dbo.Item IT ON OI.numItemCode = IT.numItemCode
			WHERE OI.numOppId = @numOppID and OM.numDomainID = @numDomainId 
        END

	IF @tintMode = 6 -- Add Edit Order items
        BEGIN 
            SELECT  OI.numoppitemtCode,
                    OI.numOppId,
					OI.vcNotes as txtNotes,
                    OI.numItemCode,
					OI.bitItemPriceApprovalRequired,
					OI.numUnitHour AS numOrigUnitHour,
                    (OI.numUnitHour-ISNULL((SELECT SUM(numUnitHour) FROM ReturnItems where 
					numReturnHeaderID IN (SELECT numReturnHeaderID FROM ReturnHeader 
					WHERE numOppId=@numOppID) AND numOppItemID=OI.numoppitemtCode),0))
					AS numUnitHour,
                    OI.monPrice,
                    OI.monTotAmount,
                    OI.numSourceID,
                    OI.vcItemDesc,
					OI.numCost,
                    OI.numWarehouseItmsID,
                    OI.vcType ItemType,
                    OI.vcAttributes Attributes,
                    OI.bitDropShip DropShip,
                    OI.numUnitHourReceived,
                    OI.bitDiscountType,
                    OI.fltDiscount,
                    OI.monTotAmtBefDiscount,
                    OI.numQtyShipped,
                    OI.vcItemName,
                    OI.vcModelID,
                    OI.vcPathForTImage,
                    OI.vcManufacturer,
                    OI.monVendorCost,
                    ISNULL(OI.numUOMId, 0) AS numUOM,
                    ISNULL(U.vcUnitName, '') vcUOMName,
                    ISNULL(dbo.fn_UOMConversion(OI.numUOMId, OI.numItemCode,
                                         OM.numDomainId, NULL),1) AS UOMConversionFactor,
                    OI.bitWorkOrder,
                    isnull(OI.numVendorWareHouse,0) as numVendorWareHouse,
                    OI.numShipmentMethod,
                    OI.numSOVendorId,
                    OI.numProjectID,
                    OI.numClassID,
                    OI.numProjectStageID,
                    OI.numToWarehouseItemID,
                   UPPER(I.charItemType) charItemType,
                    WItems.numWarehouseID,
                    W.vcWareHouse AS Warehouse,
                    0 as Op_Flag,
					CAST(( dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), OI.numItemCode,
                                    OM.numDomainId, ISNULL(OI.numUOMId, 0))
						* OI.numUnitHour ) AS NUMERIC(18, 2)) AS numUnitHour,
					isnull(OI.numClassID,0) as numClassID,isnull(OI.numProjectID,0) as numProjectID,
					CASE WHEN (SELECT COUNT(*) FROM OpportunityBizDocs OB JOIN dbo.OpportunityBizDocItems OBI ON ob.numOppBizDocsId=obi.numOppBizDocID
					AND (ob.bitAuthoritativeBizDocs=1 OR ob.numBizDocId=304) AND obi.numOppItemID=oi.numoppitemtCode AND ob.numOppId=OM.numOppId)>0 THEN 1 ELSE 0 END  AS bitIsAuthBizDoc,
					ISNULL(OI.numUnitHourReceived,0) AS numUnitHourReceived,ISNULL(numQtyShipped,0) AS numQtyShipped,dbo.fn_GetUOMName(I.numBaseUnit) AS vcBaseUOMName,ISNULL(I.fltWeight,0) AS [fltWeight],ISNULL(I.bitFreeShipping,0) AS [bitFreeShipping],
					ISNULL(I.fltLength,0) AS [fltLength],
					ISNULL(I.fltWidth,0) AS [fltWidth],
					ISNULL(I.fltHeight,0) AS [fltHeight],
					ISNULL(WItems.vcWHSKU,ISNULL(I.vcSKU,'')) AS [vcSKU],
					(CASE WHEN ISNULL(I.bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(I.monAverageCost,0) END) AS monAverageCost,
					ISNULL(I.bitFreeShipping,0) AS [bitFreeShipping],
					ISNULL(I.txtItemDesc,'') txtItemDesc,
					ISNULL(I.vcModelID,'') vcModelID,
					ISNULL(I.vcManufacturer,'') vcManufacturer,
					ISNULL(I.numBarCodeId,'') numBarCodeId,
					ISNULL(I.bitSerialized,0) bitSerialized,
					ISNULL(I.bitKitParent,0) bitKitParent,
					ISNULL(I.bitAssembly,0) bitAssembly,
					ISNULL(I.IsArchieve,0) IsArchieve,
					ISNULL(I.bitLotNo,0) bitLotNo,
					ISNULL(WItems.monWListPrice,0) monWListPrice,
					ISNULL(I.bitTaxable, 0) bitTaxable,
					CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numOnHand),0) AS VARCHAR(30)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numOnHand,
					CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numOnOrder),0) AS VARCHAR(30)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numOnOrder,
					CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numAllocation),0) AS VARCHAR(30)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numAllocation,
					CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numBackOrder),0) AS VARCHAR(30)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numBackOrder,
					case when ISNULL(I.bitAssembly,0)=1 then dbo.fn_GetAssemblyPossibleWOQty(I.numItemCode) else 0 end AS numMaxWorkOrderQty,
					ISNULL((SELECT COUNT(*) FROM OppWarehouseSerializedItem WHERE numOppId=@numOppID AND numOppItemID=OI.numoppitemtCode),0) AS numSerialNoAssigned,
					(CASE WHEN (SELECT COUNT(*) FROM OpportunityBizDocs OB JOIN dbo.OpportunityBizDocItems OBI ON OB.numOppBizDocsId=OBI.numOppBizDocID WHERE OB.numOppId = OI.numOppId AND OBI.numOppItemID=OI.numoppitemtCode AND OB.numBizDocId=296) > 0 THEN 1 ELSE 0 END) AS bitAddedFulFillmentBizDoc,
					ISNULL(OI.vcAttrValues,'') AS AttributeIDs
					,(CASE WHEN OM.tintOppType=2 THEN ISNULL(vcNotes,'') ELSE '' END) AS vcVendorNotes
--					0 AS [Tax0],0 AS bitTaxable0
            FROM    dbo.OpportunityItems OI
                    INNER JOIN dbo.OpportunityMaster OM ON OM.numOppId = OI.numOppId
                    INNER JOIN dbo.Item I ON I.numItemCode = OI.numItemCode
                    LEFT JOIN UOM U ON U.numUOMId = OI.numUOMId
                    LEFT JOIN WareHouseItems WItems ON WItems.numWareHouseItemID = OI.numWarehouseItmsID
                    LEFT JOIN Warehouses W ON W.numWarehouseID = WItems.numWarehouseID
            WHERE   OI.numOppId = @numOppID
                    AND OM.numDomainId = @numDomainId
					AND (OI.numUnitHour-ISNULL((SELECT SUM(numUnitHour) FROM ReturnItems where 
					numReturnHeaderID IN (SELECT numReturnHeaderID FROM ReturnHeader 
					WHERE numOppId=@numOppID) AND numOppItemID=OI.numoppitemtCode),0))>0
                    
		SELECT  vcSerialNo,
				vcComments AS Comments,
				numOppItemID AS numoppitemtCode,
				W.numWarehouseItmsDTLID,
				numWarehouseItmsID AS numWItmsID,
				dbo.Fn_getattributes(W.numWarehouseItmsDTLID, 1) Attributes
		FROM    WareHouseItmsDTL W
				JOIN OppWarehouseSerializedItem O ON O.numWarehouseItmsDTLID = W.numWareHouseItmsDTLID
		WHERE   numOppID = @numOppID AND numOppItemID IN (SELECT OI.numoppitemtCode FROM OpportunityItems OI WHERE OI.numOppId = @numOppID AND (OI.numUnitHour-ISNULL((SELECT SUM(numUnitHour) FROM ReturnItems where 
					numReturnHeaderID IN (SELECT numReturnHeaderID FROM ReturnHeader 
					WHERE numOppId=@numOppID) AND numOppItemID=OI.numoppitemtCode),0))>0)
					              
                   
END
END
        
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetParentChildCustomFieldMap')
	DROP PROCEDURE USP_GetParentChildCustomFieldMap
GO

CREATE PROCEDURE [dbo].[USP_GetParentChildCustomFieldMap]
	@numDomainID numeric(18, 0),
	@tintChildModule tinyint
AS
BEGIN
	SELECT 
		numParentChildFieldID
		,PCFM.numDomainID
		,tintParentModule
		,numParentFieldID
		,tintChildModule
		,numChildFieldID
		,PM.Loc_name AS vcParentModule
		,CM.Loc_name AS vcChildModule
		,DFMP.vcFieldName AS vcParentField
		,DFMC.vcFieldName AS vcChildField
	FROM 
		ParentChildCustomFieldMap PCFM 
	JOIN CFW_Loc_Master PM ON PCFM.tintParentModule=PM.Loc_id
	JOIN CFW_Loc_Master CM ON PCFM.tintChildModule=CM.Loc_id
	INNER JOIN
		DycFieldMaster DFMP
	ON
		PCFM.numParentFieldID = DFMP.numFieldId
	INNER JOIN
		DycFieldMaster DFMC
	ON
		PCFM.numChildFieldID = DFMC.numFieldId
	WHERE 
		PCFM.numDomainID=@numDomainID 
		AND ISNULL(PCFM.bitCustomField,0) = 0
		AND (PCFM.tintChildModule=@tintChildModule OR @tintChildModule=0) 
	UNION
	SELECT 
		numParentChildFieldID
		,PCFM.numDomainID
		,tintParentModule
		,numParentFieldID
		,tintChildModule
		,numChildFieldID
		,PM.Loc_name AS vcParentModule
		,CM.Loc_name AS vcChildModule
		,PFM.Fld_label AS vcParentField
		,CFM.Fld_label AS vcChildField
	FROM 
		ParentChildCustomFieldMap PCFM 
	JOIN CFW_Loc_Master PM ON PCFM.tintParentModule=PM.Loc_id
	JOIN CFW_Fld_Master PFM ON PCFM.numParentFieldID=PFM.Fld_id AND PCFM.numDomainID=PFM.numDomainID AND PCFM.tintParentModule=PFM.Grp_id
	JOIN CFW_Loc_Master CM ON PCFM.tintChildModule=CM.Loc_id
	JOIN CFW_Fld_Master CFM ON PCFM.numChildFieldID=CFM.Fld_id AND PCFM.numDomainID=CFM.numDomainID AND PCFM.tintChildModule=CFM.Grp_id
	WHERE 
		PCFM.numDomainID=@numDomainID 
		AND ISNULL(PCFM.bitCustomField,0) = 1
		AND (PCFM.tintChildModule=@tintChildModule OR @tintChildModule=0) 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetReportDashBoard')
DROP PROCEDURE USP_GetReportDashBoard
GO
CREATE PROCEDURE [dbo].[USP_GetReportDashBoard]     
	@numDomainID AS NUMERIC(18,0),           
	@numUserCntID AS NUMERIC(18,0),
	@numDashboardTemplateID AS NUMERIC(18,0) --IF VALUE IS -1 THEN FETCH DEFAULT TEMPLATE SET FOR THAT USER           
AS            
BEGIN            
	IF ISNULL(@numDashboardTemplateID,0) = -1
	BEGIN
		SET @numDashboardTemplateID = ISNULL((SELECT numDashboardTemplateID FROM UserMaster WHERE numDomainID=@numDomainID AND numUserDetailId=@numUserCntID),0)
	END

	IF NOT EXISTS(select 1 from ReportDashBoardSize WHERE numDomainID=@numDomainID AND numUserCntID=@numUserCntID)            
	BEGIN            
		INSERT INTO ReportDashBoardSize
		(
			numDomainID,numUserCntID,tintColumn,tintSize
		)            
		SELECT @numDomainID,@numUserCntID,1,1 
		UNION ALL
		SELECT @numDomainID,@numUserCntID,2,1 
		UNION ALL
		SELECT @numDomainID,@numUserCntID,3,1
	END     

	SELECT 
		tintColumn
		,tintSize 
	FROM 
		ReportDashBoardSize 
	WHERE 
		numDomainID=@numDomainID 
		AND numUserCntID=@numUserCntID 
	ORDER BY 
		tintColumn            

	SELECT 
		RD.numDashBoardID
		,RD.numReportID
		,RD.tintColumn
		,RD.tintRow
		,RD.tintReportType
		,RD.tintChartType
		,RD.tintReportCategory
		,RD.intHeight
		,RD.intWidth
		,ISNULL(RLM.bitDefault,0) bitDefault
		,ISNULL(RLM.intDefaultReportID,0) intDefaultReportID
	FROM 
		ReportDashboard RD 
	LEFT JOIN
		DashboardTemplate DT
	ON
		RD.numDashboardTemplateID=DT.numTemplateID
	LEFT JOIN
		ReportListMaster RLM
	ON 
		RD.numReportID=RLM.numReportID
	WHERE 
		RD.numDomainID=@numDomainID 
		AND RD.numUserCntID=@numUserCntID 
		AND ISNULL(RD.numDashboardTemplateID,0)=ISNULL(@numDashboardTemplateID,0)
	ORDER BY 
		tintColumn,tintRow     
		
	SELECT
		numTemplateID
		,vcTemplateName
	FROM
		DashboardTemplate
	WHERE
		numDomainID=@numDomainID
		AND numTemplateID=@numDashboardTemplateID       
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetReportDashboardDTL')
DROP PROCEDURE USP_GetReportDashboardDTL
GO
CREATE PROCEDURE [dbo].[USP_GetReportDashboardDTL]   
@numDomainID AS NUMERIC(9),               
@numDashBoardID AS NUMERIC(9)                 
as  

 SELECT ReportDashboard.*,ISNULL(ReportListMaster.bitDefault,0) bitDefault,ISNULL(ReportListMaster.intDefaultReportID,0) intDefaultReportID FROM ReportDashboard LEFT JOIN ReportListMaster ON ReportDashboard.numReportID=ReportListMaster.numReportID WHERE ReportDashboard.numDomainID=@numDomainID AND numDashBoardID=@numDashBoardID
     
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetReportListMaster')
DROP PROCEDURE USP_GetReportListMaster
GO
CREATE PROCEDURE [dbo].[USP_GetReportListMaster]      
@numDomainId as numeric(9),   
@numReportModuleID as numeric(9),   
@CurrentPage int,                                                        
@PageSize int,                                                        
@TotRecs int output,     
@SortChar char(1)='0' ,                                                       
@columnName as Varchar(50),                                                        
@columnSortOrder as Varchar(50)  ,
@SearchStr AS VARCHAR(50),
@tintReportType INT,
@tintPerspective INT,
@vcTimeline VARCHAR(100)
AS
BEGIN       
    SET NOCOUNT ON
     
	Create table #tempTable
	(
		ID INT IDENTITY PRIMARY KEY,                                                         
		numReportID NUMERIC(18,0)                                                       
	)     
	DECLARE @strSql AS VARCHAR(8000)                                                  
    
	SET  @strSql='Select numReportID from ReportListMaster where numdomainid='+ convert(varchar(15),@numDomainID) 
	IF @numReportModuleID <> 0 
	BEGIN
		SET @strSql=@strSql + ' And numReportModuleID = '+ convert(varchar(15),@numReportModuleID) +''   
	END

	IF @SearchStr <> '' 
	BEGIN
		SET @strSql=@strSql + ' AND LOWER(vcReportName) like ''%'+ LOWER(@SearchStr) +'%'''
	END

	IF @tintReportType <> -1
	BEGIN
		SET @strSql=@strSql + CONCAT(' AND tintReportType = ',@tintReportType)
	END

	IF @tintPerspective <> -1
	BEGIN
		SET @strSql=@strSql + CONCAT(' AND tintRecordFilter = ',@tintPerspective)
	END

	IF LEN(ISNULL(@vcTimeline,'')) > 0 AND ISNULL(@vcTimeline,'') <> '-1'
	BEGIN
		SET @strSql=@strSql + CONCAT(' AND vcDateFieldValue = ''',@vcTimeline,'''')
	END
    
	SET  @strSql=@strSql +'ORDER BY ' + @columnName +' '+ @columnSortOrder    
    
    PRINT @strSql
	INSERT INTO #tempTable(numReportID) EXEC(@strSql)    
    
	DECLARE @firstRec as integer                                                        
	DECLARE @lastRec as integer                                                        
	SET @firstRec= (@CurrentPage-1) * @PageSize                                                        
	SET @lastRec= (@CurrentPage*@PageSize+1)                                                         

	SELECT  @TotRecs = COUNT(*)  FROM #tempTable	
	
	SELECT 
		RLM.numReportID
		,RLM.vcReportName
		,RLM.vcReportDescription
		,RMM.vcModuleName
		,RMGM.vcGroupName
		,(CASE RLM.tintReportType WHEN 0 THEN 'Tabular' WHEN 1 THEN 'Summary' WHEN 2 THEN 'Matrix' WHEN 3 THEN 'KPI' WHEN 4 THEN 'ScoreCard' END) AS vcReportType
		,(CASE RLM.tintRecordFilter WHEN 5 THEN 'User logged in (Record Owner)' WHEN 4 THEN 'User logged in (Assigned-To)' WHEN 3 THEN 'User logged in (Territory Scope)' WHEN 2 THEN 'User logged in (Team Scope)' WHEN 1 THEN 'User logged in (Assigned-To/Record Owner)' ELSE 'All Records' END) AS vcPerspective
		,(CASE 
			WHEN ISNULL(RLM.numDateFieldID,0) > 0 AND LEN(ISNULL(RLM.vcDateFieldValue,'')) > 0
			THEN 
				CONCAT((SELECT vcFieldName FROM DycFieldMaster WHERE numFieldId=numDateFieldID),' (',
				(CASE RLM.vcDateFieldValue
					WHEN 'AllTime' THEN 'All Time'
					WHEN 'Custom' THEN 'Custom'
					WHEN 'CurYear' THEN 'Current CY'
					WHEN 'PreYear' THEN 'Previous CY'
					WHEN 'Pre2Year' THEN 'Previous 2 CY'
					WHEN 'Ago2Year' THEN '2 CY Ago'
					WHEN 'NextYear' THEN 'Next CY'
					WHEN 'CurPreYear' THEN 'Current and Previous CY'
					WHEN 'CurPre2Year' THEN 'Current and Previous 2 CY'
					WHEN 'CurNextYear' THEN 'Current and Next CY'
					WHEN 'CuQur' THEN 'Current CQ'
					WHEN 'CurNextQur' THEN 'Current and Next CQ'
					WHEN 'CurPreQur' THEN 'Current and Previous CQ'
					WHEN 'NextQur' THEN 'Next CQ'
					WHEN 'PreQur' THEN 'Previous CQ'
					WHEN 'LastMonth' THEN 'Last Month'
					WHEN 'ThisMonth' THEN 'This Month'
					WHEN 'NextMonth' THEN 'Next Month'
					WHEN 'CurPreMonth' THEN 'Current and Previous Month'
					WHEN 'CurNextMonth' THEN 'Current and Next Month'
					WHEN 'LastWeek' THEN 'Last Week'
					WHEN 'ThisWeek' THEN 'This Week'
					WHEN 'NextWeek' THEN 'Next Week'
					WHEN 'Yesterday' THEN 'Yesterday'
					WHEN 'Today' THEN 'Today'
					WHEN 'Tomorrow' THEN 'Tomorrow'
					WHEN 'Last7Day' THEN 'Last 7 Days'
					WHEN 'Last30Day' THEN 'Last 30 Days'
					WHEN 'Last60Day' THEN 'Last 60 Days'
					WHEN 'Last90Day' THEN 'Last 90 Days'
					WHEN 'Last120Day' THEN 'Last 120 Days'
					WHEN 'Next7Day' THEN 'Next 7 Days'
					WHEN 'Next30Day' THEN 'Next 30 Days'
					WHEN 'Next60Day' THEN 'Next 60 Days'
					WHEN 'Next90Day' THEN 'Next 90 Days'
					WHEN 'Next120Day' THEN 'Next 120 Days'
					ELSE '-'
				END),')')
			ELSE 
				'' 
		END) AS vcTimeline,
		(CASE WHEN (SELECT COUNT(*) FROM ReportFilterList WHERE numReportID=RLM.numReportID) > 0 THEN STUFF((SELECT DISTINCT ', ' + vcFilterValue FROM ReportFilterList WHERE numReportID=RLM.numReportID AND numFieldID=199 FOR XML PATH('')),1,2,'') ELSE '' END) AS vcLocationSource
	FROM 
		ReportListMaster RLM 
	JOIN 
		#tempTable T 
	ON 
		T.numReportID=RLM.numReportID 
	JOIN 
		ReportModuleMaster RMM 
	ON 
		RLM.numReportModuleID=RMM.numReportModuleID
	JOIN 
		ReportModuleGroupMaster RMGM 
	ON 
		RLM.numReportModuleGroupID=RMGM.numReportModuleGroupID                                                  
	WHERE 
		ID > @firstRec 
		AND ID < @lastRec 
	ORDER BY
		ID
   
	DROP TABLE #tempTable
END
GO
--created by anoop jayaraj
--	EXEC USP_GEtSFItemsForImporting 72,'76,318,538,758,772,1061,11409,11410,11628,11629,17089,18151,18193,18195,18196,18197',1
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getsfitemsforimporting')
DROP PROCEDURE usp_getsfitemsforimporting
GO
CREATE PROCEDURE USP_GEtSFItemsForImporting
    @numDomainID AS NUMERIC(9),
	@numBizDocId	AS BIGINT,
	@vcBizDocsIds		VARCHAR(MAX),
	@tintMode AS TINYINT 
AS
BEGIN
---------------------------------------------------------------------------------
    DECLARE  @strSql  AS VARCHAR(8000)
  DECLARE  @strFrom  AS VARCHAR(2000)

IF @tintMode=1 --PrintPickList
BEGIN
   SELECT
		ISNULL(vcItemName,'') AS vcItemName,
		ISNULL((SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numDomainId=187 AND bitDefault=1 AND bitIsImage=1),'') AS vcImage,
		ISNULL(vcModelID,'') AS vcModelID,
		ISNULL(txtItemDesc,'') AS txtItemDesc,
		ISNULL(vcWareHouse,'') AS vcWareHouse,
		ISNULL(vcLocation,'') AS vcLocation,
		(CASE WHEN ISNULL(Item.numItemGroup,0) > 0 THEN WareHouseItems.vcWHSKU ELSE Item.vcSKU END) AS vcSKU,
		(CASE WHEN ISNULL(Item.numItemGroup,0) > 0 THEN WareHouseItems.vcBarCode ELSE Item.numBarCodeId END) AS vcUPC,
		SUM(numUnitHour) AS numUnitHour
	FROM
		OpportunityBizDocItems
	INNER JOIN
		WareHouseItems
	ON
		OpportunityBizDocItems.numWarehouseItmsID = WareHouseItems.numWareHouseItemID
	INNER JOIN
		Warehouses
	ON
		WareHouseItems.numWareHouseID = Warehouses.numWareHouseID
	INNER JOIN
		Item
	ON
		OpportunityBizDocItems.numItemCode = Item.numItemCode
	WHERE
		numOppBizDocID IN ( SELECT * FROM dbo.splitIds(@vcBizDocsIds,','))
	GROUP BY
		vcItemName,
		vcModelID,
		txtItemDesc,
		vcWareHouse,
		vcLocation,
		(CASE WHEN ISNULL(Item.numItemGroup,0) > 0 THEN WareHouseItems.vcWHSKU ELSE Item.vcSKU END),
		(CASE WHEN ISNULL(Item.numItemGroup,0) > 0 THEN WareHouseItems.vcBarCode ELSE Item.numBarCodeId END)
	ORDER BY
		vcWareHouse ASC,
		vcLocation ASC,
		vcItemName ASC
END
ELSE IF @tintMode=2 --PrintPackingSlip
BEGIN
		  
		SELECT	
				ROW_NUMBER() OVER(ORDER BY OM.numDomainID ASC) AS SRNO,
				OM.numOppId,
				OM.numDomainID,
				OM.tintTaxOperator,
				I.numItemCode,
				I.vcModelID,
				OM.vcPOppName,
				cast((OBDI.monTotAmtBefDiscount - OBDI.monTotAMount) as varchar(20)) + Case When isnull(OM.fltDiscount,0)> 0 then  '(' + cast(OM.fltDiscount as varchar(10)) + '%)' else '' end AS DiscAmt,
				OBDI.dtRentalStartDate,
				OBDI.dtRentalReturnDate,
				OBDI.monShipCost,
				OBDI.vcShippingMethod,
				OBDI.dtDeliveryDate,
				numItemClassification,
				OBDI.vcNotes,
				OBD.vcTrackingNo,
				I.vcManufacturer,
				--vcItemClassification,
				CASE 
					WHEN charItemType='P' 
					THEN 'Inventory Item' 
					WHEN charItemType='S' 
					THEN 'Service' 
					WHEN charItemType='A' 
					THEN 'Accessory' 
					WHEN charItemType='N' 
					THEN 'Non-Inventory Item' 
				END AS charItemType,charItemType AS [Type],
				OBDI.vcAttributes,
				OI.[numoppitemtCode] ,
				OI.vcItemName,
				OBDI.numUnitHour,
				OI.[numQtyShipped],
				OBDI.vcItemDesc AS txtItemDesc,
				vcUnitofMeasure AS numUOMId,
				monListPrice AS [Price],
				CONVERT(VARCHAR, CONVERT(DECIMAL(18, 4), OBDI.monTotAmount)) Amount,
				dbo.fn_GetListItemName(OBD.numShipVia) AS ShipVai,
				OBDI.monPrice,
				OBDI.monTotAmount,
				vcWareHouse,
				vcLocation,
				vcWStreet,
				vcWCity,
				vcWPinCode,
				(SELECT vcState FROM State WHERE numStateID = W.numWState) AS [vcState],
				dbo.fn_GetListItemName(W.numWCountry) AS [vcWCountry],							
				vcWHSKU,
				vcBarCode,
				SUBSTRING((SELECT ',' + vcSerialNo + CASE 
															WHEN isnull(I.bitLotNo,0)=1 
															THEN ' (' + CONVERT(VARCHAR(15),SERIALIZED_ITEM.numQty) + ')' 
															ELSE '' 
													 END 
							FROM OppWarehouseSerializedItem SERIALIZED_ITEM 
							JOIN WareHouseItmsDTL W_ITEM_DETAIL ON SERIALIZED_ITEM.numWareHouseItmsDTLID = W_ITEM_DETAIL.numWareHouseItmsDTLID 
							WHERE SERIALIZED_ITEM.numOppID = OI.numOppId 
							  AND SERIALIZED_ITEM.numOppItemID = OI.numoppitemtCode 
							  and SERIALIZED_ITEM.numOppBizDocsID=OBD.numOppBizDocsId
							ORDER BY vcSerialNo 
						  FOR XML PATH('')),2,200000) AS SerialLotNo,
				isnull(OBD.vcRefOrderNo,'') AS vcRefOrderNo,
				isnull(OM.bitBillingTerms,0) AS tintBillingTerms,
				isnull(intBillingDays,0) AS numBillingDays,
--				CASE 
--					WHEN ISNUMERIC(ISNULL(dbo.fn_GetListItemName(ISNULL(intBillingDays,0)),0)) = 1 
--					THEN ISNULL(dbo.fn_GetListItemName(ISNULL(intBillingDays,0)),0) 
--					ELSE 0 
--				END AS numBillingDaysName,
				CASE WHEN ISNUMERIC((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0))) = 1
					 THEN (SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0))
					 ELSE 0
				END AS numBillingDaysName,	 
				ISNULL(bitInterestType,0) AS tintInterestType,
				tintOPPType,
				dbo.fn_GetContactName(numApprovedBy) AS ApprovedBy,
				dtApprovedDate,
				OM.bintAccountClosingDate,
				tintShipToType,
				tintBillToType,
				tintshipped,
				dtShippedDate bintShippedDate,
				OM.numDivisionID,							
				ISNULL(numShipVia,0) AS numShipVia,
				ISNULL(vcTrackingURL,'') AS vcTrackingURL,
				CASE 
				    WHEN numShipVia IS NULL 
				    THEN '-'
				    ELSE dbo.fn_GetListItemName(numShipVia)
				END AS ShipVia,
				ISNULL(C.varCurrSymbol,'') varCurrSymbol,
				isnull(bitPartialFulfilment,0) bitPartialFulfilment,
				dbo.[fn_GetContactName](OM.[numRecOwner])  AS OrderRecOwner,
				dbo.[fn_GetContactName](DM.[numRecOwner]) AS AccountRecOwner,
				dbo.fn_GetContactName(OM.numAssignedTo) AS AssigneeName,
				ISNULL((SELECT vcEmail FROM dbo.AdditionalContactsInformation WHERE numContactID = OM.numAssignedTo),'') AS AssigneeEmail,
				ISNULL((SELECT numPhone FROM dbo.AdditionalContactsInformation WHERE numContactID = OM.numAssignedTo),'') AS AssigneePhone,
				dbo.fn_GetComapnyName(OM.numDivisionId) OrganizationName,
				DM.vcComPhone as OrganizationPhone,
				dbo.fn_GetContactName(OM.numContactID)  OrgContactName,
				dbo.getCompanyAddress(OM.numDivisionId,1,OM.numDomainId) CompanyBillingAddress,
				CASE 
					 WHEN ACI.numPhone<>'' 
					 THEN ACI.numPhone + CASE 
										    WHEN ACI.numPhoneExtension<>'' 
											THEN ' - ' + ACI.numPhoneExtension 
											ELSE '' 
										 END  
					 ELSE '' 
				END OrgContactPhone,
				ISNULL(ACI.vcEmail,'') AS OrgContactEmail,
				dbo.[fn_GetContactName](OM.[numRecOwner]) AS OnlyOrderRecOwner,
				(SELECT TOP 1 SD.vcSignatureFile FROM SignatureDetail SD 
												 JOIN OpportunityBizDocsDetails OBD ON SD.numSignID = OBD.numSignID 
												WHERE SD.numDomainID = @numDomainID 
												  AND OBD.numDomainID = @numDomainID 
												  AND OBD.numSignID IS not null 
				ORDER BY OBD.numBizDocsPaymentDetId DESC) AS vcSignatureFile,
				ISNULL(CMP.txtComments,'') AS vcOrganizationComments,
				OBD.vcRefOrderNo AS [P.O.No],
				OM.txtComments AS [Comments],
				D.vcBizDocImagePath
				,dbo.FormatedDateFromDate(OM.dtReleaseDate,@numDomainID) AS vcReleaseDate
				,(CASE WHEN ISNULL(OM.numPartner,0) > 0 THEN ISNULL((SELECT CONCAT(DivisionMaster.vcPartnerCode,'-',CompanyInfo.vcCompanyName) FROM DivisionMaster INNER JOIN CompanyInfo ON DivisionMaster.numCompanyID=CompanyInfo.numCompanyId WHERE DivisionMaster.numDivisionID=OM.numPartner),'') ELSE '' END) AS vcPartner,
				ISNULL(vcCustomerPO#,'') vcCustomerPO#
	INTO #temp_Packing_List
	FROM OpportunityMaster AS OM
	join OpportunityBizDocs OBD on OM.numOppID=OBD.numOppID
	JOIN OpportunityBizDocItems OBDI on OBDI.numOppBizDocID=OBD.numOppBizDocsId
	JOIN OpportunityItems AS OI on OI.numoppitemtCode=OBDI.numOppItemID and OI.numoppID=OI.numOppID
	JOIN Item AS I ON OI.numItemCode = I.numItemCode AND I.numDomainID = @numDomainID
	LEFT OUTER JOIN WareHouseItems WI ON I.numItemCode = WI.numItemID AND WI.numWareHouseItemID = OBDI.numWarehouseItmsID
	LEFT OUTER JOIN Warehouses W ON WI.numWareHouseID = W.numWareHouseID 
	JOIN [DivisionMaster] DM ON DM.numDivisionID = OM.numDivisionID   
	JOIN CompanyInfo CMP ON DM.numCompanyId = CMP.numCompanyId and CMP.numDomainID = @numDomainID  
	JOIN dbo.AdditionalContactsInformation ACI ON ACI.numContactID = OM.numContactID
	JOIN Domain D ON D.numDomainID = OM.numDomainID
	LEFT OUTER JOIN [Currency] C ON C.numCurrencyID = OM.numCurrencyID
    WHERE OM.numDomainID = @numDomainID 
	  AND OBD.[numOppBizDocsId] IN ( SELECT * FROM dbo.splitIds(@vcBizDocsIds ,',')) ORDER BY OM.numOppId
	
	DECLARE @numFldID AS INT    
	DECLARE @intRowNum AS INT    	
	DECLARE @vcFldname AS VARCHAR(MAX)	
	
	SET @strSQL = ''
	

	/**** START: Added Sales Tax Column ****/

	DECLARE @strSQLUpdate AS VARCHAR(2000);
	
	EXEC ( 'ALTER TABLE #temp_Packing_List ADD [TotalTax] MONEY'  )
	EXEC ( 'ALTER TABLE #temp_Packing_List ADD [CRV] MONEY'  )


	DECLARE @i AS INT = 1
	DECLARE @Count AS INT 
	SELECT @Count = COUNT(*) FROM #temp_Packing_List

	DECLARE @numOppId AS BIGINT
	DECLARE @numOppItemCode AS BIGINT
	DECLARE @tintTaxOperator AS INT

	WHILE @i <= @Count
	BEGIN
		SET @strSQLUpdate=''

		SELECT 
			@numDomainID = numDomainID,
			@numOppId = numOppId,
			@tintTaxOperator = tintTaxOperator,
			@numOppItemCode = numoppitemtCode
		FROM
			#temp_Packing_List
		WHERE
			SRNO = @i


		IF @tintTaxOperator = 1 
		BEGIN
			 SET @strSQLUpdate = @strSQLUpdate
				+ ',[TotalTax]= dbo.fn_CalBizDocTaxAmt('
				+ CONVERT(VARCHAR(20), @numDomainID) + ',0,'
				+ CONVERT(VARCHAR(20), @numOppId) + ','
				+ CONVERT(VARCHAR(20), @numOppItemCode) + ',1,Amount,numUnitHour)'

			SET @strSQLUpdate = @strSQLUpdate
				+ ',[CRV]= dbo.fn_CalBizDocTaxAmt('
				+ CONVERT(VARCHAR(20), @numDomainID) + ',1,'
				+ CONVERT(VARCHAR(20), @numOppId) + ','
				+ CONVERT(VARCHAR(20), @numOppItemCode) + ',1,Amount,numUnitHour)'
		END
		ELSE IF @tintTaxOperator = 2 -- remove sales tax
		BEGIN
			SET @strSQLUpdate = @strSQLUpdate + ',[TotalTax]= 0'
			SET @strSQLUpdate = @strSQLUpdate + ',[CRV]= 0'
		END
		ELSE
		BEGIN
			 SET @strSQLUpdate = @strSQLUpdate
				+ ',[TotalTax]= dbo.fn_CalBizDocTaxAmt('
				+ CONVERT(VARCHAR(20), @numDomainID) + ',0,'
				+ CONVERT(VARCHAR(20), @numOppId) + ','
				+ CONVERT(VARCHAR(20), @numOppItemCode) + ',1,Amount,numUnitHour)'

			SET @strSQLUpdate = @strSQLUpdate
				+ ',[CRV]= dbo.fn_CalBizDocTaxAmt('
				+ CONVERT(VARCHAR(20), @numDomainID) + ',1,'
				+ CONVERT(VARCHAR(20), @numOppId) + ','
				+ CONVERT(VARCHAR(20), @numOppItemCode) + ',1,Amount,numUnitHour)'
		END

		IF @strSQLUpdate <> '' 
		BEGIN
			SET @strSQLUpdate = 'UPDATE #temp_Packing_List SET numItemCode=numItemCode' + @strSQLUpdate + ' WHERE numItemCode > 0 AND numOppId=' + CONVERT(VARCHAR(20), @numOppId) + ' AND numoppitemtCode=' + CONVERT(VARCHAR(20), @numOppItemCode)
			PRINT @strSQLUpdate
			EXEC (@strSQLUpdate)
		END

		SET @i = @i + 1
	END


	SET @strSQLUpdate = ''
	DECLARE @vcTaxName AS VARCHAR(100)
	DECLARE @numTaxItemID AS NUMERIC(9)
	SET @numTaxItemID = 0
	SELECT TOP 1 @vcTaxName = vcTaxName, @numTaxItemID = numTaxItemID FROM TaxItems WHERE numDomainID = @numDomainID

	WHILE @numTaxItemID > 0
	BEGIN

		EXEC ( 'ALTER TABLE #temp_Packing_List ADD [' + @vcTaxName + '] MONEY' )

		SET @strSQLUpdate = @strSQLUpdate + ',[' + @vcTaxName  + ']= dbo.fn_CalBizDocTaxAmt(numDomainID,'
						+ CONVERT(VARCHAR(20), @numTaxItemID) + ',numOppId,numoppitemtCode,1,Amount,numUnitHour)'
           

		SELECT TOP 1 @vcTaxName = vcTaxName, @numTaxItemID = numTaxItemID FROM TaxItems WHERE numDomainID = @numDomainID AND numTaxItemID > @numTaxItemID

		IF @@rowcount = 0 
			SET @numTaxItemID = 0
	END


	IF @strSQLUpdate <> '' 
	BEGIN
		SET @strSQLUpdate = 'UPDATE #temp_Packing_List SET numItemCode=numItemCode' + @strSQLUpdate + ' WHERE numItemCode > 0' 
		PRINT @strSQLUpdate
		EXEC (@strSQLUpdate)
	END

	/**** END: Added Sales Tax Column ****/
	
	PRINT 'START'                                                                                          
	PRINT 'QUERY : ' + @strSQL

	SELECT TOP 1 @numFldID = numFieldId,
				 @vcFldname = Fld_label,
				 @intRowNum = (intRowNum+1) 
	FROM DycFormConfigurationDetails DTL                                                                                                
	JOIN CFW_Fld_Master FIELD_MASTER ON FIELD_MASTER.Fld_id = DTL.numFieldId                                                                                                 
	WHERE DTL.numFormID = 7 
	  AND FIELD_MASTER.Grp_id = 5 
	  AND DTL.numDomainID = @numDomainID 
	  AND numAuthGroupID = @numBizDocId
	ORDER BY intRowNum

	PRINT @intRowNum
	WHILE @intRowNum > 0                                                                                  
	BEGIN    
			PRINT 'FROM LOOP'
			PRINT @vcFldname
			PRINT @numFldID

			EXEC('ALTER TABLE #temp_Packing_List add [' + @vcFldname + '] varchar(100)')
		
			SET @strSQL = 'UPDATE #temp_Packing_List SET [' + @vcFldname + '] = dbo.GetCustFldValueItem(' + CONVERT(VARCHAR(10),@numFldID) + ',numItemCode) 
						   WHERE numItemCode > 0'

			PRINT 'QUERY : ' + @strSQL
			EXEC (@strSQL)
			                                                                                 	                                                                                       
			SELECT TOP 1 @numFldID = numFieldId,
						 @vcFldname = Fld_label,
						 @intRowNum = (intRowNum + 1) 
			FROM DycFormConfigurationDetails DETAIL                                                                                                
			JOIN CFW_Fld_Master MASTER ON MASTER.Fld_id = DETAIL.numFieldId                                                                                                 
			WHERE DETAIL.numFormID = 7 
			  AND MASTER.Grp_id = 5 
			  AND DETAIL.numDomainID = @numDomainID 
			 AND numAuthGroupID = @numBizDocId                                                 
			  AND intRowNum >= @intRowNum 
			ORDER BY intRowNum                                                                  
			           
			IF @@rowcount=0 SET @intRowNum=0                                                                                                                                                
		END	
	PRINT 'END'	
	SELECT * FROM #temp_Packing_List
END
END
/****** Object:  StoredProcedure [dbo].[usp_GetUsersWithDomains]    Script Date: 07/26/2008 16:18:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj              
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getuserswithdomains')
DROP PROCEDURE usp_getuserswithdomains
GO
CREATE PROCEDURE [dbo].[usp_GetUsersWithDomains]                            
 @numUserID NUMERIC(9 )= 0,
 @numDomainID NUMERIC(9)
--                          
AS                            
BEGIN                            
 IF @numUserID = 0 AND @numDomainID > 0 --Check if request is made while session is on
 BEGIN                            
  SELECT numUserID, vcUserName,UserMaster.numGroupID,isnull(vcGroupName,'-')as vcGroupName,              
 vcUserDesc,ISNULL(ADC.vcfirstname,'') +' '+ ISNULL(ADC.vclastname,'') as Name,               
 UserMaster.numDomainID, vcDomainName, UserMaster.numUserDetailId ,isnull(UserMaster.SubscriptionId,'') as SubscriptionId
,isnull(UserMaster.tintDefaultRemStatus,0) tintDefaultRemStatus,isnull(UserMaster.numDefaultTaskType,0) numDefaultTaskType,
ISNULL(UserMaster.bitOutlook,0) bitOutlook,UserMaster.vcLinkedinId,ISNULL(UserMaster.intAssociate,0) as intAssociate,UserMaster.ProfilePic,UserMaster.numDashboardTemplateID
   FROM UserMaster                      
   join Domain                        
   on UserMaster.numDomainID =  Domain.numDomainID                       
   left join AdditionalContactsInformation ADC                      
   on ADC.numContactid=UserMaster.numUserDetailId                     
   left join AuthenticationGroupMaster GM                     
   on Gm.numGroupID= UserMaster.numGroupID                    
   where tintGroupType=1 -- Application Users                    
   ORDER BY  Domain.numDomainID, vcUserDesc                            
 END   
 ELSE IF @numUserID = -1 AND @numDomainID > 0 --Support Email
 BEGIN                            
  SELECT '' AS vcEmailId,vcImapUserName,isnull(bitUseUserName,0) bitUseUserName, isnull(ImapUserDTL.bitImap,0) bitImap,  
	ImapUserDTL.vcImapServerUrl,  
	isnull(ImapUserDTL.vcImapPassword,'') vcImapPassword,  
	ImapUserDTL.bitSSl,  
	ImapUserDTL.numPort 
FROM [ImapUserDetails] ImapUserDTL   
where ImapUserDTL.numUserCntId = @numUserID 
AND ImapUserDTL.numDomainID = @numDomainID

 END                         
 ELSE                            
 BEGIN                            
  SELECT       
 U.numUserID, vcUserName,numGroupID,vcUserDesc, U.numDomainID,vcDomainName, U.numUserDetailId,bitHourlyRate,bitActivateFlag,      
 monHourlyRate,bitSalary,numDailyHours,bitOverTime,numLimDailHrs,monOverTimeRate,bitMainComm,fltMainCommPer,      
 bitRoleComm,(select count(*) from UserRoles where UserRoles.numUserCntID=U.numUserDetailId)  as Roles ,vcEmailid,      
 vcPassword,isnull(ExcUserDTL.bitExchangeIntegration,0) bitExchangeIntegration, isnull(ExcUserDTL.bitAccessExchange,0) bitAccessExchange,       
 isnull(ExcUserDTL.vcExchPassword,'') as vcExchPassword, isnull(ExcUserDTL.vcExchPath,'') as vcExchPath ,       
 isnull(ExcUserDTL.vcExchDomain,'')  as vcExchDomain   ,isnull(U.SubscriptionId,'') as SubscriptionId ,  
 isnull(ImapUserDTL.bitImap,0) bitImap,  
	ImapUserDTL.vcImapServerUrl,  
	isnull(ImapUserDTL.vcImapPassword,'') vcImapPassword,  
	ImapUserDTL.bitSSl,  
	ImapUserDTL.numPort 
	,isnull(bitSMTPAuth,0) bitSMTPAuth
      ,isnull([vcSmtpPassword],'')vcSmtpPassword
      ,isnull([vcSMTPServer],0) vcSMTPServer
      ,isnull(numSMTPPort,0)numSMTPPort
      ,isnull(bitSMTPSSL,0) bitSMTPSSL
,	isnull(bitSMTPServer,0)bitSMTPServer
    ,isnull(bitUseUserName,0) bitUseUserName
,isnull(U.tintDefaultRemStatus,0) tintDefaultRemStatus,isnull(U.numDefaultTaskType,0) numDefaultTaskType,
ISNULL(U.bitOutlook,0) bitOutlook,
ISNULL(numDefaultClass,0) numDefaultClass,isnull(numDefaultWarehouse,0) numDefaultWarehouse,U.vcLinkedinId,ISNULL(U.intAssociate,0) as intAssociate,U.ProfilePic,U.numDashboardTemplateID
FROM UserMaster U                        
 join Domain D                       
 on   U.numDomainID =  D.numDomainID       
left join  ExchangeUserDetails ExcUserDTL      
 on ExcUserDTL.numUserID=U.numUserID                      
left join  [ImapUserDetails] ImapUserDTL   
on ImapUserDTL.numUserCntId = U.numUserDetailId   
AND ImapUserDTL.numDomainID = D.numDomainID
   WHERE  U.numUserID=@numUserID                            
 END                            
END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetVendorShipmentMethod')
DROP PROCEDURE USP_GetVendorShipmentMethod
GO
CREATE PROCEDURE [dbo].[USP_GetVendorShipmentMethod]         
@numDomainID as numeric(9)=0,    
@numVendorid AS NUMERIC(9)=0,
@numAddressID AS NUMERIC(9)=0,
@tintMode AS TINYINT        
as        

IF @tintMode=1
BEGIN 
	SELECT LD.numListItemID,LD.vcData AS ShipmentMethod,ISNULL(VM.numListValue,0) AS numListValue,VM.bitPrimary,ISNULL(VM.bitPreferredMethod,0) bitPreferredMethod
	from ListDetails LD LEFT JOIN VendorShipmentMethod VM ON VM.numListItemID=LD.numListItemID 
	AND VM.numVendorid=@numVendorid AND VM.numAddressID=@numAddressID AND VM.numDomainID=@numDomainID
	WHERE LD.numListID=338 AND (LD.numDomainID=@numDomainID or Ld.constFlag=1)
END
ELSE IF @tintMode=2
BEGIN 
	SELECT LD.numListItemID,LD.vcData AS ShipmentMethod,ISNULL(VM.numListValue,0) AS numListValue,VM.bitPrimary,
	cast(LD.numListItemID AS VARCHAR(10)) + '~' + cast(ISNULL(VM.numListValue,0) AS VARCHAR(10)) AS vcListValue
		from ListDetails LD JOIN VendorShipmentMethod VM ON VM.numListItemID=LD.numListItemID 
	AND VM.numVendorid=@numVendorid AND VM.numAddressID=@numAddressID AND VM.numDomainID=@numDomainID
	WHERE LD.numListID=338 AND (LD.numDomainID=@numDomainID or Ld.constFlag=1) ORDER BY ISNULL(bitPrimary,0) DESC, ISNULL(bitPreferredMethod,0) DESC
END

GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetWorkFlowQueue' ) 
    DROP PROCEDURE USP_GetWorkFlowQueue
GO
CREATE PROCEDURE USP_GetWorkFlowQueue
AS 
BEGIN
			  --IF Workflow is not active or deleted
			  UPDATE WFQ SET tintProcessStatus=5 FROM WorkFlowQueue WFQ 
				WHERE WFQ.numWFID NOT IN (SELECT numWFID FROM dbo.WorkFlowMaster WF WHERE WF.bitActive=1 AND WF.numFormID=WFQ.numFormID 
				AND WFQ.numDomainID=WF.numDomainID AND WF.tintWFTriggerOn=WFQ.tintWFTriggerOn) 
			  
			  --Select top 25 WorkFlow which are Pending Execution			  	
			  SELECT TOP 25 WFQ.[numWFQueueID],WFQ.[numDomainID],WFQ.[numRecordID],WFQ.[numFormID],
						WFQ.[tintProcessStatus],WFQ.[tintWFTriggerOn],WFQ.[numWFID]
				 FROM WorkFlowQueue WFQ JOIN dbo.WorkFlowMaster WF ON WFQ.numWFID=WF.numWFID and WFQ.numFormID=WF.numFormID
					  AND WFQ.numDomainID=WF.numDomainID AND WF.tintWFTriggerOn=WFQ.tintWFTriggerOn
				 WHERE WFQ.tintProcessStatus IN (1) AND WF.bitActive=1 ORDER BY numWFQueueID
END



--Created By Anoop Jayaraj          
--exec USP_ItemDetailsForEcomm @numItemCode=859064,@numWareHouseID=1074,@numDomainID=172,@numSiteId=90
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_itemdetailsforecomm')
DROP PROCEDURE USP_ItemDetailsForEcomm
GO
CREATE PROCEDURE [dbo].[USP_ItemDetailsForEcomm]
@numItemCode as numeric(9),          
@numWareHouseID as numeric(9),    
@numDomainID as numeric(9),
@numSiteId as numeric(9),
@vcCookieId as VARCHAR(MAX)=null,
@numUserCntID AS NUMERIC(9)=0,
@numDivisionID AS NUMERIC(18,0) = 0                                        
as     
/*Removed warehouse parameter, and taking Default warehouse by default from DB*/
    
declare @bitShowInStock as bit        
declare @bitShowQuantity as BIT
declare @bitAutoSelectWarehouse as bit        
--declare @tintColumns as tinyint        
DECLARE @numDefaultWareHouseID as numeric(9)
DECLARE @numDefaultRelationship AS NUMERIC(18,0)
DECLARE @numDefaultProfile AS NUMERIC(18,0)
DECLARE @tintPreLoginPriceLevel AS TINYINT
DECLARE @tintPriceLevel AS TINYINT
set @bitShowInStock=0        
set @bitShowQuantity=0        
--set @tintColumns=1        
        
IF ISNULL(@numDivisionID,0) > 0
BEGIN
	SELECT @numDefaultRelationship=ISNULL(tintCRMType,0),@numDefaultProfile=ISNULL(vcProfile,0),@tintPriceLevel=ISNULL(tintPriceLevel,0) FROM DivisionMaster INNER JOIN CompanyInfo ON DivisionMaster.numCompanyID=CompanyInfo.numCompanyId WHERE numDivisionID=@numDivisionID AND DivisionMaster.numDomainID=@numDomainID
END     
        
SELECT 
	@bitShowInStock=ISNULL(bitShowInStock,0)
	,@bitShowQuantity=ISNULL(bitShowQOnHand,0)
	,@bitAutoSelectWarehouse=ISNULL(bitAutoSelectWarehouse,0)
	,@numDefaultRelationship=numRelationshipId
	,@numDefaultProfile=numProfileId
	,@tintPreLoginPriceLevel=(CASE WHEN @numDivisionID > 0 THEN (CASE WHEN ISNULL(bitShowPriceUsingPriceLevel,0)=1 THEN ISNULL(@tintPriceLevel,0) ELSE 0 END) ELSE ISNULL(tintPreLoginProceLevel,0) END)
FROM 
	eCommerceDTL 
WHERE 
	numDomainID=@numDomainID

	
    


IF @numWareHouseID=0
BEGIN
	SELECT @numDefaultWareHouseID=ISNULL(numDefaultWareHouseID,0) FROM [eCommerceDTL] WHERE [numDomainID]=@numDomainID	
	SET @numWareHouseID =@numDefaultWareHouseID;
END

DECLARE @vcWarehouseIDs AS VARCHAR(1000)
IF @bitAutoSelectWarehouse = 1
BEGIN		
	SELECT @vcWarehouseIDs = ISNULL( STUFF((SELECT DISTINCT ',' + CAST(numWarehouseID AS VARCHAR(10)) FROM [dbo].[Warehouses] WHERE [Warehouses].[numDomainID] = @numDomainID FOR XML PATH('')),1, 1, '') , '')
END
ELSE
BEGIN
	SET @vcWarehouseIDs = @numWareHouseID
END

/*If qty is not found in default warehouse then choose next warehouse with sufficient qty */
IF @bitAutoSelectWarehouse = 1 AND ( SELECT COUNT(*) FROM dbo.WareHouseItems WHERE  numItemID = @numItemCode AND numWarehouseID= @numWareHouseID ) = 0 
BEGIN
	SELECT TOP 1 @numDefaultWareHouseID = numWareHouseID FROM dbo.WareHouseItems WHERE  numItemID = @numItemCode AND numOnHand>0 /*TODO: Pass actuall qty from cart*/ ORDER BY numOnHand DESC 
	IF (ISNULL(@numDefaultWareHouseID,0)>0)
		SET @numWareHouseID =@numDefaultWareHouseID;
END

      DECLARE @UOMConversionFactor AS DECIMAL(18,5)
      
      Select @UOMConversionFactor= ISNULL(dbo.fn_UOMConversion(ISNULL(I.numSaleUnit,0),I.numItemCode,I.numDomainId,ISNULL(I.numBaseUnit,0)),0)
      FROM Item I where numItemCode=@numItemCode   
	   
	  DECLARE @PromotionOffers AS DECIMAL(18,2)=0     
	  IF(@vcCookieId!=null OR @vcCookieId!='')   
	  BEGIN                    
SET @PromotionOffers=(SELECT TOP 1 CASE WHEN P.tintDiscountType=1  
				THEN (I.monListPrice*fltDiscountValue)/100
				WHEN P.tintDiscountType=2
				THEN fltDiscountValue
				WHEN P.tintDiscountType=3
				THEN fltDiscountValue*I.monListPrice
				ELSE 0 END
FROM CartItems AS C
LEFT JOIN PromotionOffer AS P ON P.numProId=C.PromotionID
LEFT JOIN Item I ON I.numItemCode=C.numItemCode
WHERE I.numItemCode=@numItemCode AND I.numItemCode=@numDomainID AND C.vcCookieId=@vcCookieId AND C.numUserCntId=@numUserCntID AND C.fltDiscount=0 AND 
(I.numItemCode IN (SELECT PIS.numValue FROM PromotionOfferItems AS PIS WHERE PIS.numProId=P.numProId AND PIS.tintType=1 AND PIS.tintRecordType=6) OR
I.numItemClassification IN (SELECT PIS.numValue FROM PromotionOfferItems AS PIS WHERE PIS.numProId=P.numProId AND PIS.tintType=2 AND PIS.tintRecordType=6))
AND P.numDomainId=@numDomainID 
		AND ISNULL(bitEnabled,0)=1 
		AND ISNULL(bitAppliesToSite,0)=1 
		AND 1 = (CASE WHEN bitRequireCouponCode = 1 THEN CASE WHEN ISNULL(intCouponCodeUsed,0) < ISNULL(tintUsageLimit,0) OR ISNULL(tintUsageLimit,0) = 0  THEN 1 ELSE 0 END ELSE 1 END)
		AND 1 = (CASE WHEN bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN CAST(GETDATE() AS DATE)>=dtValidFrom AND CAST(GETDATE() AS DATE)<=dtValidTo THEN 1 ELSE 0 END) END))
		END
		PRINT @PromotionOffers
 
declare @strSql as varchar(MAX)
set @strSql=' With tblItem AS (                  
select I.numItemCode, vcItemName, txtItemDesc, charItemType, ISNULL(I.bitMatrix,0) bitMatrix,' + 
(CASE WHEN @tintPreLoginPriceLevel > 0 THEN 'ISNULL(PricingOption.monPrice,ISNULL(CASE WHEN I.[charItemType]=''P'' 
			THEN '+ convert(varchar(15),@UOMConversionFactor) + ' * W.[monWListPrice]  
			ELSE '+ convert(varchar(15),@UOMConversionFactor) + ' * monListPrice 
	   END,0))' ELSE 'ISNULL(CASE WHEN I.[charItemType]=''P'' 
			THEN '+ convert(varchar(15),@UOMConversionFactor) + ' * W.[monWListPrice]  
			ELSE '+ convert(varchar(15),@UOMConversionFactor) + ' * monListPrice 
	   END,0)' END)
+'
AS monListPrice,ISNULL(CASE WHEN I.[charItemType]=''P'' 
			THEN '+ convert(varchar(15),@UOMConversionFactor) + ' * W.[monWListPrice]  
			ELSE '+ convert(varchar(15),@UOMConversionFactor) + ' * monListPrice 
	   END,0) AS monMSRP,
numItemClassification, bitTaxable, vcSKU, bitKitParent,              
I.numModifiedBy, (SELECT numItemImageId,vcPathForImage,vcPathForTImage,bitDefault,case when bitdefault=1  then -1 else isnull(intDisplayOrder,0) end as intDisplayOrder FROM ItemImages  WHERE numItemCode=I.numItemCode and bitIsImage = 1 order by case when bitdefault=1 then -1 else isnull(intDisplayOrder,0) end  asc FOR XML AUTO,ROOT(''Images''))  vcImages,
(SELECT vcPathForImage FROM ItemImages  WHERE numItemCode=I.numItemCode and bitIsImage = 0 order by intDisplayOrder  asc FOR XML AUTO,ROOT(''Documents''))  vcDocuments ,
 isnull(bitSerialized,0) as bitSerialized, vcModelID,                 
numItemGroup,                   
(isnull(sum(numOnHand),0) / '+ convert(varchar(15),@UOMConversionFactor) + ') as numOnHand,                    
sum(numOnOrder) as numOnOrder,                    
sum(numReorder)  as numReorder,                    
sum(numAllocation)  as numAllocation,                    
sum(numBackOrder)  as numBackOrder,              
isnull(fltWeight,0) as fltWeight,              
isnull(fltHeight,0) as fltHeight,              
isnull(fltWidth,0) as fltWidth,              
isnull(fltLength,0) as fltLength,              
case when charItemType=''P''then isnull(bitFreeShipping,0) else 1 end as bitFreeShipping,
Case When (case when charItemType=''P''then isnull(bitFreeShipping,0) else 1 end )=1 then ''Free shipping'' else ''Calculated at checkout'' end as Shipping,              
isnull(bitAllowBackOrder,0) as bitAllowBackOrder,bitShowInStock,bitShowQOnHand,isnull(numWareHouseItemID,0)  as numWareHouseItemID,
(case when charItemType<>''S'' and charItemType<>''N'' then         
 (CASE 
	WHEN I.bitAllowBackOrder = 1 THEN 1
	WHEN (ISNULL(WAll.numTotalOnHand,0)<=0) THEN 0
	ELSE 1
END) ELSE 1 END ) as bitInStock,
(case when charItemType<>''S'' and charItemType<>''N'' then         
 (Case when '+ convert(varchar(15),@bitShowInStock) + '=1  then 
 (CASE 
	WHEN I.bitAllowBackOrder = 1 AND I.numDomainID <> 172 AND ISNULL(WAll.numTotalOnHand,0)<=0 AND ISNULL(I.numVendorID,0) > 0 AND dbo.GetVendorPreferredMethodLeadTime(I.numVendorID) > 0 THEN CONCAT(''<font class="vendorLeadTime" style="color:green">Usually ships in '',dbo.GetVendorPreferredMethodLeadTime(I.numVendorID),'' days</font>'')
	WHEN I.bitAllowBackOrder = 1 AND ISNULL(WAll.numTotalOnHand,0)<=0 THEN (CASE WHEN I.numDomainID = 172 THEN ''<font style="color:green">AVAILABLE</font>'' ELSE ''<font style="color:green">In Stock</font>'' END)
	WHEN (ISNULL(WAll.numTotalOnHand,0)<=0) THEN (CASE WHEN I.numDomainID = 172 THEN ''<font style="color:red">ON HOLD</font>'' ELSE ''<font style="color:red">Out Of Stock</font>'' END)
	ELSE (CASE WHEN I.numDomainID = 172 THEN ''<font style="color:green">AVAILABLE</font>'' ELSE ''<font style="color:green">In Stock</font>'' END)  
END) else '''' end) ELSE '''' END ) as InStock,
ISNULL(numSaleUnit,0) AS numUOM,
ISNULL(vcUnitName,'''') AS vcUOMName,
 '+ convert(varchar(15),@UOMConversionFactor) + ' AS UOMConversionFactor,
I.numCreatedBy,
I.numBarCodeId,
I.[vcManufacturer],
(SELECT  COUNT(*) FROM  Review where vcReferenceId = CONVERT(VARCHAR(50) ,I.numItemCode) and numDomainId = I.numDomainId and numSiteId = '+ CONVERT(VARCHAR(20) , @numSiteId)  +' AND bitHide = 0 ) as ReviewCount ,
(SELECT  COUNT(*) FROM  Ratings where vcReferenceId = CONVERT(VARCHAR(50) ,I.numItemCode) and numDomainId = I.numDomainId and numSiteId = '+ CONVERT(VARCHAR(20) , @numSiteId)  +'  ) as RatingCount ,
(select top 1 vcMetaKeywords  from MetaTags where numReferenceID = I.numItemCode and tintMetaTagFor =2) as vcMetaKeywords,
(select top 1 vcMetaDescription  from MetaTags where numReferenceID = I.numItemCode and tintMetaTagFor =2) as vcMetaDescription ,
(SELECT vcCategoryName  FROM  dbo.Category WHERE numCategoryID =(SELECT TOP 1 numCategoryID FROM dbo.ItemCategory WHERE numItemID = I.numItemCode)) as vcCategoryName,('+(SELECT CAST(ISNULL(@PromotionOffers,0) AS varchar(20)))+') as PromotionOffers

from Item I ' + 
(CASE WHEN @tintPreLoginPriceLevel > 0 THEN ' OUTER APPLY(SELECT decDiscount AS monPrice FROM PricingTable WHERE tintRuleType=3 AND numItemCode=I.numItemCode ORDER BY numPricingID OFFSET ' + CAST(@tintPreLoginPriceLevel-1 AS VARCHAR) + ' ROWS FETCH NEXT 1 ROWS ONLY) As PricingOption ' ELSE '' END)
+ '                 
left join  WareHouseItems W                
on W.numItemID=I.numItemCode and numWareHouseID= '+ convert(varchar(15),@numWareHouseID) + '
 OUTER APPLY (SELECT SUM(numOnHand) numTotalOnHand FROM WareHouseItems W 
													  WHERE  W.numItemID = I.numItemCode
                                                      AND numWareHouseID IN (SELECT [OutParam] FROM dbo.[SplitString](''' + @vcWarehouseIDs + ''','',''))
                                                      AND W.numDomainID = ' + CONVERT(VARCHAR(10),@numDomainID) + ') AS WAll
left join  eCommerceDTL E          
on E.numDomainID=I.numDomainID     
LEFT JOIN UOM u ON u.numUOMId=I.numSaleUnit    
where ISNULL(I.IsArchieve,0)=0 AND I.numDomainID='+ convert(varchar(15),@numDomainID) + ' AND I.numItemCode='+ convert(varchar(15),@numItemCode) +
CASE WHEN (SELECT COUNT(*) FROM [dbo].[EcommerceRelationshipProfile] WHERE numSiteID=@numSiteID AND numRelationship = @numDefaultRelationship AND numProfile = @numDefaultProfile) > 0
THEN
	CONCAT(' AND I.numItemClassification NOT IN (SELECT numItemClassification FROM EcommerceRelationshipProfile ERP INNER JOIN ECommerceItemClassification EIC ON EIC.numECommRelatiionshipProfileID=ERP.ID WHERE ERP.numSiteID=',@numSiteID,' AND ERP.numRelationship=',@numDefaultRelationship,' AND ERP.numProfile=', @numDefaultProfile,')')
ELSE
	''
END
+ '           
group by I.numItemCode, vcItemName, txtItemDesc, charItemType, monListPrice,numItemClassification, bitTaxable, vcSKU, bitKitParent, WAll.numTotalOnHand,         
I.numDomainID,I.numModifiedBy,  bitSerialized, vcModelID,numItemGroup, fltWeight,fltHeight,fltWidth,          
fltLength,bitFreeShipping,bitAllowBackOrder,bitShowInStock,bitShowQOnHand ,numWareHouseItemID,vcUnitName,I.numCreatedBy,I.numBarCodeId,W.[monWListPrice],I.[vcManufacturer],
numSaleUnit, I.numVendorID, I.bitMatrix ' + (CASE WHEN @tintPreLoginPriceLevel > 0 THEN ',PricingOption.monPrice' ELSE '' END) + ')'

set @strSql=@strSql+' select numItemCode,vcItemName,txtItemDesc,charItemType, bitMatrix, monListPrice,monMSRP,numItemClassification, bitTaxable, vcSKU, bitKitParent,                  
tblItem.numModifiedBy, vcImages,vcDocuments, bitSerialized, vcModelID, numItemGroup,numOnHand,numOnOrder,numReorder,numAllocation, 
numBackOrder, fltWeight, fltHeight, fltWidth, fltLength, bitFreeShipping,bitAllowBackOrder,bitShowInStock,
bitShowQOnHand,numWareHouseItemID,bitInStock,InStock,numUOM,vcUOMName,UOMConversionFactor,tblItem.numCreatedBy,numBarCodeId,vcManufacturer,ReviewCount,RatingCount,Shipping,isNull(vcMetaKeywords,'''') as vcMetaKeywords ,isNull(vcMetaDescription,'''') as vcMetaDescription,isNull(vcCategoryName,'''') as vcCategoryName,PromotionOffers'     

set @strSql=@strSql+ ' from tblItem ORDER BY numOnHand DESC'
PRINT @strSql
exec (@strSql)


declare @tintOrder as tinyint                                                  
declare @vcFormFieldName as varchar(50)                                                  
declare @vcListItemType as varchar(1)                                             
declare @vcAssociatedControlType varchar(10)                                                  
declare @numListID AS numeric(9)                                                  
declare @WhereCondition varchar(2000)                       
Declare @numFormFieldId as numeric  
DECLARE @vcFieldType CHAR(1)
                  
set @tintOrder=0                                                  
set @WhereCondition =''                 
                   
              
  CREATE TABLE #tempAvailableFields(numFormFieldId  numeric(9),vcFormFieldName NVARCHAR(50),
        vcFieldType CHAR(1),vcAssociatedControlType NVARCHAR(50),numListID NUMERIC(18),
        vcListItemType CHAR(1),intRowNum INT,vcItemValue VARCHAR(3000))
   
  INSERT INTO #tempAvailableFields (numFormFieldId,vcFormFieldName,vcFieldType,vcAssociatedControlType
        ,numListID,vcListItemType,intRowNum)                         
            SELECT  Fld_id,REPLACE(Fld_label,' ','') AS vcFormFieldName,
                   CASE GRP_ID WHEN 4 then 'C' WHEN 1 THEN 'D' ELSE 'C' END AS vcFieldType,
                    fld_type as vcAssociatedControlType,
                    ISNULL(C.numListID, 0) numListID,
                    CASE WHEN C.numListID > 0 THEN 'L'
                         ELSE ''
                    END vcListItemType,ROW_NUMBER() OVER (ORDER BY Fld_id asc) AS intRowNum
            FROM    CFW_Fld_Master C LEFT JOIN CFW_Validation V ON V.numFieldID = C.Fld_id
            WHERE   C.numDomainID = @numDomainId
                    AND GRP_ID IN (5)

     select top 1 @tintOrder=intRowNum,@numFormFieldId=numFormFieldId,@vcFormFieldName=vcFormFieldName,
				  @vcFieldType=vcFieldType,@vcAssociatedControlType=vcAssociatedControlType,@numListID=numListID,
				  @vcListItemType=vcListItemType
	from #tempAvailableFields order by intRowNum ASC
   
while @tintOrder>0                                                  
begin                                                  
    if @vcAssociatedControlType = 'TextBox' or @vcAssociatedControlType = 'TextArea'
    begin  
		UPDATE #tempAvailableFields SET vcItemValue=(SELECT Fld_Value FROM CFW_FLD_Values_Item WHERE Fld_Id=@numFormFieldId AND RecId=@numItemCode) WHERE numFormFieldId=@numFormFieldId              
    end   
    else if @vcAssociatedControlType = 'CheckBox'
	begin      
		UPDATE #tempAvailableFields SET vcItemValue=(SELECT case when isnull(Fld_Value,0)=0 then 'No' when isnull(Fld_Value,0)=1 then 'Yes' END FROM CFW_FLD_Values_Item WHERE Fld_Id=@numFormFieldId AND RecId=@numItemCode) WHERE numFormFieldId=@numFormFieldId              
   end                
   else if @vcAssociatedControlType = 'DateField'           
   begin   
		UPDATE #tempAvailableFields SET vcItemValue=(SELECT dbo.FormatedDateFromDate(Fld_Value,@numDomainId) FROM CFW_FLD_Values_Item WHERE Fld_Id=@numFormFieldId AND RecId=@numItemCode) WHERE numFormFieldId=@numFormFieldId              
   end                
    else if @vcAssociatedControlType = 'SelectBox'           
   begin 
		UPDATE #tempAvailableFields SET vcItemValue=(SELECT L.vcData FROM CFW_FLD_Values_Item CFW left Join ListDetails L
			on L.numListItemID=CFW.Fld_Value                
			WHERE CFW.Fld_Id=@numFormFieldId AND CFW.RecId=@numItemCode) WHERE numFormFieldId=@numFormFieldId              
end          
               
 
    select top 1 @tintOrder=intRowNum,@numFormFieldId=numFormFieldId,@vcFormFieldName=vcFormFieldName,
				  @vcFieldType=vcFieldType,@vcAssociatedControlType=vcAssociatedControlType,@numListID=numListID,
				  @vcListItemType=vcListItemType
	from #tempAvailableFields WHERE intRowNum > @tintOrder order by intRowNum ASC
 
   if @@rowcount=0 set @tintOrder=0                                                  
end   


  
SELECT * FROM #tempAvailableFields

DROP TABLE #tempAvailableFields

--exec USP_ItemDetailsForEcomm @numItemCode=197611,@numWareHouseID=58,@numDomainID=1,@numSiteId=18
--exec USP_ItemDetailsForEcomm @numItemCode=735364,@numWareHouseID=1039,@numDomainID=156,@numSiteId=104
GO
/****** Object:  StoredProcedure [dbo].[USP_ItemList1]    Script Date: 05/07/2009 18:14:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Created By Anoop Jayaraj                                        
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'usp_itemlist1' ) 
    DROP PROCEDURE usp_itemlist1
GO
CREATE PROCEDURE [dbo].[USP_ItemList1]
    @ItemClassification AS NUMERIC(9) = 0,
    @IsKit AS TINYINT,
    @SortChar CHAR(1) = '0',
    @CurrentPage INT,
    @PageSize INT,
    @TotRecs INT OUTPUT, 
    @columnName AS VARCHAR(50),
    @columnSortOrder AS VARCHAR(10),
    @numDomainID AS NUMERIC(9) = 0,
    @ItemType AS CHAR(1),
    @bitSerialized AS BIT,
    @numItemGroup AS NUMERIC(9),
    @bitAssembly AS BIT,
    @numUserCntID AS NUMERIC(9),
    @Where	AS VARCHAR(MAX),
    @IsArchive AS BIT = 0,
    @byteMode AS TINYINT,
	@bitAsset as BIT=0,
	@bitRental as BIT=0,
	@bitContainer AS BIT=0,
	@SearchText VARCHAR(300),
	@vcRegularSearchCriteria varchar(MAX)='',
	@vcCustomSearchCriteria varchar(MAX)='',
	@tintDashboardReminderType TINYINT = 0
AS 
BEGIN	
	DECLARE @numWarehouseID NUMERIC(18,0)
	SELECT TOP 1 @numWarehouseID = numWareHouseID FROM Warehouses WHERE numDomainID = @numDomainID


	CREATE TABLE #tempForm 
	(
		tintOrder TINYINT,vcDbColumnName nvarchar(50),vcOrigDbColumnName nvarchar(50),vcFieldName nvarchar(50),vcAssociatedControlType nvarchar(50),vcListItemType varchar(3),
		numListID numeric(9),vcLookBackTableName varchar(50),bitCustomField BIT,numFieldId NUMERIC,bitAllowSorting BIT,bitAllowEdit BIT,bitIsRequired BIT,bitIsEmail BIT,
		bitIsAlphaNumeric BIT,bitIsNumeric BIT,bitIsLengthValidation BIT,intMaxLength int,intMinLength int,bitFieldMessage BIT,vcFieldMessage VARCHAR(500),
		ListRelID numeric(9),intColumnWidth int,bitAllowFiltering BIT,vcFieldDataType CHAR(1),Grp_Id TINYINT
	)

	-- FIRST CHECK IF GRID COLUMNS ARE CONFIGURED
	DECLARE  @Nocolumns  AS TINYINT = 0

	SELECT 
		@Nocolumns=ISNULL(SUM(TotalRow),0) 
	FROM
	(            
		SELECT 
			COUNT(*) TotalRow 
		FROM 
			View_DynamicColumns 
		WHERE 
			numFormId=21 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND numRelCntType = 0
		UNION 
		SELECT 
			COUNT(*) TotalRow 
		FROM 
			View_DynamicCustomColumns 
		WHERE 
			numFormId=21 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND numRelCntType = 0
	) TotalRows

	-- IF GRID COLUMNS ARE NOT CONFIGURES THEN CHECK IF MASTER FORM CONFIGURATION IS CREATED BY ADMINISTRATOR
	DECLARE @IsMasterConfAvailable BIT = 0
	DECLARE @numUserGroup NUMERIC(18,0)

	IF @Nocolumns=0
	BEGIN
		SELECT @numUserGroup = numGroupID FROM UserMaster WHERE numUserDetailId = @numUserCntID 

		IF (SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = @numDomainID AND numFormID = 21 AND numRelCntType=0 AND bitGridConfiguration = 1 AND numGroupID = @numUserGroup) > 0
		BEGIN
			SET @IsMasterConfAvailable = 1
		END
	END

	--IF MASTERCONFIGURATION IS AVAILABLE THEN LOAD IT OTHERWISE LOAD DEFAULT COLUMNS
	IF @IsMasterConfAvailable = 1
	BEGIN
		INSERT INTO DycFormConfigurationDetails 
		(
			numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth
		)
		SELECT 
			21,numFieldId,intColumnNum,intRowNum,@numDomainID,@numUserCntID,0,1,0,intColumnWidth
		FROM 
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId=21 AND 
			View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicColumnsMasterConfig.numRelCntType=0 AND 
			View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
			ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
		UNION
		SELECT 
			21,numFieldId,intColumnNum,intRowNum,@numDomainID,@numUserCntID,0,1,1,intColumnWidth
		FROM 
			View_DynamicCustomColumnsMasterConfig
		WHERE 
			View_DynamicCustomColumnsMasterConfig.numFormId=21 AND 
			View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicCustomColumnsMasterConfig.numRelCntType=0 AND 
			View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1

		INSERT INTO 
			#tempForm
		SELECT 
			(intRowNum + 1) as tintOrder, vcDbColumnName,	vcOrigDbColumnName,	ISNULL(vcCultureFieldName,vcFieldName), vcAssociatedControlType,
			vcListItemType, numListID, vcLookBackTableName,	bitCustom, numFieldId, bitAllowSorting,	bitInlineEdit, bitIsRequired,
			bitIsEmail,	bitIsAlphaNumeric, bitIsNumeric, bitIsLengthValidation,	intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,
			ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType,0
		FROM 
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId=21 AND 
			View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicColumnsMasterConfig.numRelCntType=0 AND 
			View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
			ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
		UNION
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName, vcAssociatedControlType,'' as vcListItemType,numListID,'',
			bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,'',Grp_Id
		FROM 
			View_DynamicCustomColumnsMasterConfig
		WHERE 
			View_DynamicCustomColumnsMasterConfig.numFormId=21 AND 
			View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicCustomColumnsMasterConfig.numRelCntType=0 AND 
			View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1
		ORDER BY 
			tintOrder ASC  
	END
	ELSE                                        
	BEGIN
		IF @Nocolumns=0
		BEGIN
			INSERT INTO DycFormConfigurationDetails 
			(
				numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth
			)
			SELECT 
				21,numFieldId,0,Row_number() over(order by tintRow desc),@numDomainID,@numUserCntID,0,1,0,intColumnWidth
			FROM 
				View_DynamicDefaultColumns
			WHERE 
				numFormId=21 
				AND bitDefault=1 
				AND ISNULL(bitSettingField,0)=1 
				AND numDomainID=@numDomainID
			ORDER BY 
				tintOrder asc   
		END

		INSERT INTO 
			#tempForm
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcOrigDbColumnName,ISNULL(vcCultureFieldName,vcFieldName),                  
			vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName,bitCustom,numFieldId,bitAllowSorting,bitInlineEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength,bitFieldMessage,
			vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType,0
		FROM 
			View_DynamicColumns 
		WHERE 
			numFormId=21 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND ISNULL(bitSettingField,0)=1 
			AND ISNULL(bitCustom,0)=0  
			AND numRelCntType = 0
		UNION
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName,vcAssociatedControlType,'' as vcListItemType,numListID,'',bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,'',Grp_Id
		FROM 
			View_DynamicCustomColumns
		WHERE 
			numFormId=21 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND ISNULL(bitCustom,0)=1 
			AND numRelCntType = 0
		ORDER BY 
			tintOrder asc  
	END   

	IF @byteMode=0
	BEGIN
		DECLARE @strColumns AS VARCHAR(MAX)
		SET @strColumns = ' I.numItemCode,ISNULL(DivisionMaster.numDivisionID,0) numDivisionID,ISNULL(DivisionMaster.tintCRMType,0) tintCRMType, ' + CAST(ISNULL(@numWarehouseID,0) AS VARCHAR) + ' AS numWarehouseID, I.numDomainID,0 AS numRecOwner,0 AS numTerID, I.charItemType, I.numItemCode AS [numItemCode~211~0]'

		DECLARE @tintOrder AS TINYINT                                                  
		DECLARE @vcFieldName AS VARCHAR(50)                                                  
		DECLARE @vcListItemType AS VARCHAR(3)                                             
		DECLARE @vcListItemType1 AS VARCHAR(1)                                                 
		DECLARE @vcAssociatedControlType VARCHAR(10)                                                  
		DECLARE @numListID AS NUMERIC(9)                                                  
		DECLARE @vcDbColumnName VARCHAR(50)                      
		DECLARE @WhereCondition VARCHAR(2000)                       
		DECLARE @vcLookBackTableName VARCHAR(2000)                
		DECLARE @bitCustom AS BIT                  
		DECLARE @numFieldId AS NUMERIC  
		DECLARE @bitAllowSorting AS CHAR(1)   
		DECLARE @bitAllowEdit AS CHAR(1)                   
        DECLARE @vcColumnName AS VARCHAR(200)
		DECLARE @Grp_ID AS TINYINT
		SET @tintOrder = 0                                                  
		SET @WhereCondition = ''                 
                   
		DECLARE @ListRelID AS NUMERIC(9) 
		DECLARE @SearchQuery AS VARCHAR(MAX) = ''

		IF LEN(@SearchText) > 0
		BEGIN
			SET @SearchQuery = ' I.numItemCode LIKE ''%' + @SearchText + '%'' OR I.vcItemName LIKE ''%' + @SearchText + '%'' OR I.vcModelID LIKE ''%' + @SearchText + '%'' OR I.vcSKU LIKE ''%' + @SearchText + '%'' OR cmp.vcCompanyName LIKE ''%' + @SearchText + '%'' OR I.vcManufacturer LIKE ''%' + @SearchText + '%'''
		END


		DECLARE @j INT = 0
  
		SELECT TOP 1
				@tintOrder = tintOrder + 1,
				@vcDbColumnName = vcDbColumnName,
				@vcFieldName = vcFieldName,
				@vcAssociatedControlType = vcAssociatedControlType,
				@vcListItemType = vcListItemType,
				@numListID = numListID,
				@vcLookBackTableName = vcLookBackTableName,
				@bitCustom = bitCustomField,
				@numFieldId = numFieldId,
				@bitAllowSorting = bitAllowSorting,
				@bitAllowEdit = bitAllowEdit,
				@ListRelID = ListRelID
		FROM    #tempForm --WHERE bitCustomField=1
		ORDER BY tintOrder ASC            

		WHILE @tintOrder > 0                                                  
		BEGIN
			IF @bitCustom = 0
			BEGIN
				DECLARE  @Prefix  AS VARCHAR(5)

				IF @vcLookBackTableName = 'Item'
					SET @Prefix = 'I.'
				ELSE IF @vcLookBackTableName = 'CompanyInfo'
					SET @Prefix = 'cmp.'
				ELSE IF @vcLookBackTableName = 'Vendor'
					SET @Prefix = 'V.'

				SET @vcColumnName= @vcDbColumnName+'~'+ convert(varchar(10),@numFieldId) +'~0'

				IF @vcAssociatedControlType='SelectBox' 
				BEGIN
					IF @vcListItemType='LI' 
					BEGIN
						SET @strColumns = @strColumns+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName+']'
						SET @WhereCondition= @WhereCondition +' LEFT JOIN ListDetails L'+ convert(varchar(3),@tintOrder)+ ' ON L'+convert(varchar(3),@tintOrder)+ '.numListItemID='+@vcDbColumnName                                                      
					END
					ELSE IF @vcListItemType = 'UOM'
					BEGIN
						SET @strColumns = @strColumns + ',ISNULL((SELECT vcUnitName FROM UOM WHERE numUOMId = I.' + @vcDbColumnName + '),'''')' + ' ['+ @vcColumnName+']'
					END
					ELSE IF @vcListItemType = 'IG'
					BEGIN
						SET @strColumns = @strColumns+ ',ISNULL((SELECT vcItemGroup FROM ItemGroups WHERE numItemGroupID = I.numItemGroup),'''')' + ' ['+ @vcColumnName+']'  
					END
				END
				ELSE IF @vcDbColumnName = 'vcPathForTImage'
		   		BEGIN
					SET @strColumns = @strColumns + ',ISNULL(II.vcPathForTImage,'''') AS ' + ' ['+ @vcColumnName+']'                  
					SET @WhereCondition = @WhereCondition + ' LEFT JOIN ItemImages II ON II.numItemCode = I.numItemCode AND bitDefault = 1'
				END
				ELSE IF @vcLookBackTableName = 'Item' AND @vcDbColumnName = 'monListPrice'
				BEGIN
					SET @strColumns = @strColumns + ',CASE WHEN I.charItemType=''P'' THEN ISNULL((SELECT TOP 1 monWListPrice FROM WarehouseItems WHERE numDomainID=I.numDomainID AND numItemID=I.numItemCode AND monWListPrice > 0),0.00) ELSE ISNULL(I.monListPrice,0.00) END AS ' + ' ['+ @vcColumnName+']'
				END
				ELSE IF @vcLookBackTableName = 'Item' AND @vcDbColumnName = 'ItemType'
				BEGIN
					SET @strColumns = @strColumns + ',' + '(CASE 
																WHEN I.charItemType=''P'' 
																THEN 
																	CASE 
																		WHEN ISNULL(I.bitAssembly,0) = 1 THEN ''Assembly''
																		WHEN ISNULL(I.bitKitParent,0) = 1 THEN ''Kit''
																		WHEN ISNULL(I.bitAsset,0) = 1 AND ISNULL(I.bitRental,0)=0 THEN ''Asset''
																		WHEN ISNULL(I.bitAsset,0) = 1 AND ISNULL(I.bitRental,0)=1 THEN ''Rental Asset''
																		WHEN ISNULL(I.bitSerialized,0) = 1 AND ISNULL(I.bitAsset,0) = 0  THEN ''Serialized''
																		WHEN ISNULL(I.bitSerialized,0) = 1 AND ISNULL(I.bitAsset,0) = 1 AND ISNULL(I.bitRental,0)=0 THEN ''Serialized Asset''
																		WHEN ISNULL(I.bitSerialized,0) = 1 AND ISNULL(I.bitAsset,0) = 1 AND ISNULL(I.bitRental,0)=1 THEN ''Serialized Rental Asset''
																		WHEN ISNULL(I.bitLotNo,0)=1 THEN ''Lot #''
																		ELSE ''Inventory Item'' 
																	END
																WHEN I.charItemType=''N'' THEN ''Non Inventory Item'' 
																WHEN I.charItemType=''S'' THEN ''Service'' 
																WHEN I.charItemType=''A'' THEN ''Accessory'' 
															END)' + ' AS ' + ' [' + @vcColumnName + ']'  
				END
				ELSE IF @vcLookBackTableName = 'Item' AND @vcDbColumnName = 'monStockValue'
				BEGIN
					SET @strColumns = @strColumns+',WarehouseItems.monStockValue'+' ['+ @vcColumnName+']'                                                      
				END
				ELSE IF @vcLookBackTableName = 'Warehouses' AND @vcDbColumnName = 'vcWareHouse'
				BEGIN
					SET @strColumns = @strColumns + ', (SELECT STUFF((SELECT CONCAT('', '', vcWareHouse) FROM WareHouseItems WI JOIN Warehouses W ON WI.numWareHouseID=W.numWareHouseID WHERE WI.numItemID=I.numItemCode FOR XML PATH('''')), 1, 1, ''''))' + ' AS ' + ' [' + @vcColumnName + ']'
				END
				ELSE IF @vcLookBackTableName = 'WorkOrder' AND @vcDbColumnName = 'WorkOrder'
				BEGIN
					SET @strColumns = @strColumns + ',(CASE WHEN (SELECT SUM(WO.numQtyItemsReq) from WorkOrder WO where WO.numItemCode=I.numItemCode and WO.numWOStatus=0) > 0 THEN 1 ELSE 0 END)' + ' AS ' + ' [' + @vcColumnName + ']'
				END
				ELSE IF @vcDbColumnName = 'vcPriceLevelDetail' 
				BEGIN
					SELECT @strColumns = @strColumns + CONCAT(', dbo.fn_GetItemPriceLevelDetail( ',@numDomainID,',I.numItemCode,',@numWarehouseID,')') +' ['+ @vcColumnName+']' 
				END
				ELSE
				BEGIN
					If @vcDbColumnName <> 'numItemCode' AND @vcDbColumnName <> 'vcPriceLevelDetail' --WE AE ALREADY ADDING COLUMN IN SELECT CLUASE DIRECTLY
					BEGIN
						SET @strColumns = @strColumns + ',' + (CASE @vcLookBackTableName WHEN 'Item' THEN 'I' WHEN 'CompanyInfo' THEN 'cmp' WHEN 'Vendor' THEN 'V' ELSE @vcLookBackTableName END) + '.' + @vcDbColumnName + ' AS ' + ' [' + @vcColumnName + ']' 
					END
				END
			END                              
			ELSE IF @bitCustom = 1 
			BEGIN
				
				SET @vcColumnName= @vcDbColumnName +'~'+ convert(varchar(10),@numFieldId)+'~1'

				SELECT 
					@vcFieldName = FLd_label,
					@vcAssociatedControlType = fld_type,
					@vcDbColumnName = 'Cust' + CONVERT(VARCHAR(10), Fld_Id),
					@Grp_ID = Grp_id
				FROM 
					CFW_Fld_Master
				WHERE 
					CFW_Fld_Master.Fld_Id = @numFieldId                 
        
				IF @Grp_ID = 9
				BEGIN
					SET @strColumns = @strColumns + ',L' + CONVERT(VARCHAR(3), @tintOrder) + '.vcData' + ' [' + @vcColumnName + ']'     
             
					SET @WhereCondition = @WhereCondition + ' left Join ItemAttributes CFW' + CONVERT(VARCHAR(3), @tintOrder)
									+ ' ON CFW' + CONVERT(VARCHAR(3), @tintOrder) + '.numDOmainID=' + CAST(@numDomainID AS VARCHAR) + ' AND CFW' + CONVERT(VARCHAR(3), @tintOrder) + '.Fld_Id='
									+ CONVERT(VARCHAR(10), @numFieldId)
									+ 'and CFW' + CONVERT(VARCHAR(3), @tintOrder)
									+ '.numItemCode=I.numItemCode   '  
					SET @WhereCondition = @WhereCondition
						+ ' left Join ListDetails L'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ ' on L'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ '.numListItemID=CFW'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ '.Fld_Value'      
				END
				ELSE IF @vcAssociatedControlType = 'TextBox' OR @vcAssociatedControlType = 'TextArea' 
				BEGIN
					SET @strColumns = @strColumns + ',CFW'  + CONVERT(VARCHAR(3), @tintOrder)  + '.Fld_Value  [' + @vcColumnName + ']'     
             
					SET @WhereCondition = @WhereCondition + ' left Join CFW_FLD_Values_Item CFW' + CONVERT(VARCHAR(3), @tintOrder)
									+ ' ON CFW' + CONVERT(VARCHAR(3), @tintOrder) + '.Fld_Id='
									+ CONVERT(VARCHAR(10), @numFieldId)
									+ 'and CFW' + CONVERT(VARCHAR(3), @tintOrder)
									+ '.RecId=I.numItemCode   '                                                         
				END   
				ELSE IF @vcAssociatedControlType = 'CheckBox' 
				BEGIN
					SET @strColumns = @strColumns
						+ ',case when isnull(CFW'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ '.Fld_Value,0)=0 then 0 when isnull(CFW'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ '.Fld_Value,0)=1 then 1 end   ['
						+  @vcColumnName
						+ ']'               
 
					SET @WhereCondition = @WhereCondition
						+ ' left Join CFW_FLD_Values_Item CFW'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ ' ON CFW' + CONVERT(VARCHAR(3), @tintOrder) + '.Fld_Id='
						+ CONVERT(VARCHAR(10), @numFieldId)
						+ ' and CFW'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ '.RecId=I.numItemCode   '                                                     
				END                
				ELSE IF @vcAssociatedControlType = 'DateField' 
				BEGIN
					SET @strColumns = @strColumns
						+ ',dbo.FormatedDateFromDate(CFW'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ '.Fld_Value,'
						+ CONVERT(VARCHAR(10), @numDomainId)
						+ ')  [' +@vcColumnName + ']'    
					                  
					SET @WhereCondition = @WhereCondition
						+ ' left Join CFW_FLD_Values_Item CFW'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ ' on CFW' + CONVERT(VARCHAR(3), @tintOrder) + '.Fld_Id='
						+ CONVERT(VARCHAR(10), @numFieldId)
						+ ' and CFW'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ '.RecId=I.numItemCode   '                                                         
				END                
				ELSE IF @vcAssociatedControlType = 'SelectBox' 
				BEGIN
					SET @vcDbColumnName = 'Cust' + CONVERT(VARCHAR(10), @numFieldId)

					SET @strColumns = @strColumns + ',L'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ '.vcData' + ' [' + @vcColumnName + ']'          
					                                                    
					SET @WhereCondition = @WhereCondition
						+ ' left Join CFW_FLD_Values_Item CFW'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ ' on CFW' + CONVERT(VARCHAR(3), @tintOrder) + '.Fld_Id='
						+ CONVERT(VARCHAR(10), @numFieldId)
						+ ' and CFW'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ '.RecId=I.numItemCode    '     
					                                                    
					SET @WhereCondition = @WhereCondition
						+ ' left Join ListDetails L'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ ' on L'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ '.numListItemID=CFW'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ '.Fld_Value'                
				END
				ELSE
				BEGIN
					SET @strColumns = @strColumns + CONCAT(',dbo.GetCustFldValueItem(',@numFieldId,',I.numItemCode)') + ' [' + @vcColumnName + ']'
				END                
			END        
  
			

			SELECT TOP 1
					@tintOrder = tintOrder + 1,
					@vcDbColumnName = vcDbColumnName,
					@vcFieldName = vcFieldName,
					@vcAssociatedControlType = vcAssociatedControlType,
					@vcListItemType = vcListItemType,
					@numListID = numListID,
					@vcLookBackTableName = vcLookBackTableName,
					@bitCustom = bitCustomField,
					@numFieldId = numFieldId,
					@bitAllowSorting = bitAllowSorting,
					@bitAllowEdit = bitAllowEdit,
					@ListRelID = ListRelID
			FROM    #tempForm
			WHERE   tintOrder > @tintOrder - 1 --AND bitCustomField=1
			ORDER BY tintOrder ASC            
 
			IF @@rowcount = 0 
				SET @tintOrder = 0 
		END  

		DECLARE @strSQL AS VARCHAR(MAX)
		SET @strSQL = ' FROM 
							Item I
						LEFT JOIN
							Vendor V
						ON
							I.numItemCode = V.numItemCode
							AND I.numVendorID = V.numVendorID
						LEFT JOIN
							DivisionMaster
						ON
							V.numVendorID = DivisionMaster.numDivisionID
						LEFT JOIN
							CompanyInfo cmp
						ON
							DivisionMaster.numCompanyId = cmp.numCompanyId
						OUTER APPLY
						(
							SELECT
								SUM(numOnHand) AS numOnHand,
								SUM(numBackOrder) AS numBackOrder,
								SUM(numOnOrder) AS numOnOrder,
								SUM(numAllocation) AS numAllocation,
								SUM(numReorder) AS numReorder,
								SUM(numOnHand) * (CASE WHEN ISNULL(I.bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(I.monAverageCost,0) END) AS monStockValue
							FROM
								WareHouseItems
							WHERE
								numItemID = I.numItemCode
						) WarehouseItems ' + @WhereCondition

		IF @columnName = 'OnHand' OR @columnName = 'WHI.numOnHand'
			SET @columnName = 'numOnHand'
		ELSE IF @columnName = 'Backorder' OR @columnName = 'WHI.numBackOrder'
			SET @columnName = 'numBackOrder'
		ELSE IF @columnName = 'OnOrder' OR @columnName = 'WHI.numOnOrder' 
			SET @columnName = 'numOnOrder'
		ELSE IF @columnName = 'OnAllocation' OR @columnName = 'WHI.numAllocation' 
			SET @columnName = 'numAllocation'
		ELSE IF @columnName = 'Reorder' OR @columnName = 'WHI.numReorder' 
			SET @columnName = 'numReorder'
		ELSE IF @columnName = 'monStockValue' 
			SET @columnName = 'sum(numOnHand) * (CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(monAverageCost,0) END)'
		ELSE IF @columnName = 'ItemType' 
			SET @columnName = 'charItemType'
		ELSE IF @columnName = 'numItemCode' 
			SET @columnName = 'I.numItemCode'

		DECLARE @column AS VARCHAR(50)              
		SET @column = @columnName
	         
		IF @columnName LIKE '%Cust%' 
		BEGIN
			DECLARE @CustomFieldType AS VARCHAR(10)
			DECLARE @fldId AS VARCHAR(10)
			
			IF CHARINDEX('CFW.Cust',@columnName) > 0
			BEGIN
				SET @fldId = REPLACE(@columnName, 'CFW.Cust', '')
			END
			ELSE
			BEGIN
				SET @fldId = REPLACE(@columnName, 'Cust', '')
			END
			SELECT TOP 1 @CustomFieldType = ISNULL(vcAssociatedControlType,'') FROM View_DynamicCustomColumns WHERE numFieldID = @fldId
            
			IF ISNULL(@CustomFieldType,'') = 'SelectBox' OR ISNULL(@CustomFieldType,'') = 'ListBox'
			BEGIN
				SET @strSQL = @strSQL + ' left Join CFW_FLD_Values_Item CFW on CFW.RecId=i.numItemCode and CFW.fld_id= ' + @fldId + ' ' 
				SET @strSQL = @strSQL + ' left Join ListDetails LstCF on LstCF.numListItemID = CFW.Fld_Value  '            
				SET @columnName = ' LstCF.vcData ' 	
			END
			ELSE
			BEGIN
				SET @strSQL =  @strSQL + ' left Join CFW_FLD_Values_Item CFW on CFW.RecId=i.numItemCode and CFW.fld_id= ' + @fldId + ' '                                                         
				SET @columnName = 'CFW.Fld_Value'                	
			END
		END                                              
                  
		DECLARE @strWhere AS VARCHAR(8000)
		SET @strWhere = CONCAT(' WHERE I.numDomainID = ',@numDomainID)
    
		IF @ItemType = 'P'
		BEGIN
			SET @strWhere = @strWhere + ' AND I.charItemType= ''P''' 
		END
		ELSE IF @ItemType = 'S'
		BEGIN
			SET @strWhere = @strWhere + ' AND I.charItemType= ''S'''
		END
		ELSE IF @ItemType = 'N'    
		BEGIN
			SET @strWhere = @strWhere + ' AND I.charItemType= ''N'''
		END
	    
		IF @bitContainer= '1'
		BEGIN
			SET @strWhere = @strWhere + ' AND I.bitContainer= ' + CONVERT(VARCHAR(2), @bitContainer) + ''  
		END

		IF @bitAssembly = '1' 
		BEGIN
			SET @strWhere = @strWhere + ' AND I.bitAssembly= ' + CONVERT(VARCHAR(2), @bitAssembly) + ''  
		END 
		ELSE IF @ItemType = 'P'
		BEGIN
			SET @strWhere = @strWhere + ' AND ISNULL(I.bitAssembly,0)=0 '
		END

		IF @IsKit = '1' 
		BEGIN
			SET @strWhere = @strWhere + ' AND ISNULL(I.bitAssembly,0)=0 AND I.bitKitParent= ' + CONVERT(VARCHAR(2), @IsKit) + ''  
		END           
		ELSE IF @ItemType = 'P'
		BEGIN
			SET @strWhere = @strWhere + ' AND ISNULL(I.bitKitParent,0)=0 '
		END

		IF @bitSerialized = 1 
		BEGIN
			SET @strWhere = @strWhere + ' AND (I.bitSerialized=1 OR I.bitLotNo=1)'
		END
		--ELSE IF @ItemType = 'P'
		--BEGIN
		--	SET @strWhere = @strWhere + ' AND ISNULL(Item.bitSerialized,0)=0 AND ISNULL(Item.bitLotNo,0)=0 '
		--END

		IF @bitAsset = 1 
		BEGIN
			SET @strWhere = @strWhere + ' AND ISNULL(I.bitAsset,0) = 1'
		END
		ELSE IF @ItemType = 'P'
		BEGIN
			SET @strWhere = @strWhere + ' AND ISNULL(I.bitAsset,0) = 0 '
		END

		IF @bitRental = 1
		BEGIN
			SET @strWhere = @strWhere + ' AND ISNULL(I.bitRental,0) = 1 AND ISNULL(I.bitAsset,0) = 1 '
		END
		ELSE IF @ItemType = 'P'
		BEGIN
			SET @strWhere = @strWhere + ' AND ISNULL(I.bitRental,0) = 0 '
		END

		IF @SortChar <> '0' 
		BEGIN
			SET @strWhere = @strWhere + ' AND I.vcItemName like ''' + @SortChar + '%'''                                       
		END

		IF @ItemClassification <> '0' 
		BEGIN
			SET @strWhere = @strWhere + ' AND I.numItemClassification=' + CONVERT(VARCHAR(15), @ItemClassification)    
		END

		IF @IsArchive = 0 
		BEGIN
			SET @strWhere = @strWhere + ' AND ISNULL(I.IsArchieve,0) = 0'       
		END
        
		IF @numItemGroup > 0 
		BEGIN
			SET @strWhere = @strWhere + ' AND I.numItemGroup = ' + CONVERT(VARCHAR(20), @numItemGroup)  
        END  
		
		IF @numItemGroup = -1 
		BEGIN
			SET @strWhere = @strWhere + ' AND I.numItemGroup in (select numItemGroupID from ItemGroups where numDomainID=' + CONVERT(VARCHAR(20), @numDomainID) + ')'                                      
        END

		IF @vcRegularSearchCriteria<>'' 
		BEGIN
			SET @strWhere = @strWhere +' AND ' + @vcRegularSearchCriteria 
		END
	
		IF @vcCustomSearchCriteria<>'' 
		BEGIN
			SET @strWhere = @strWhere + ' AND ' + @vcCustomSearchCriteria
		END
		
		IF LEN(@SearchQuery) > 0
		BEGIN
			SET @strWhere = @strWhere + ' AND (' + @SearchQuery + ') '
		END

		SET @strWhere = @strWhere + ISNULL(@Where,'')

		IF ISNULL(@tintDashboardReminderType,0) = 17
		BEGIN
			SET @strWhere = CONCAT(@strWhere,' AND I.numItemCode IN (','SELECT DISTINCT
																		numItemID
																	FROM
																		WareHouseItems WIInner
																	INNER JOIN
																		Item IInner
																	ON
																		WIInner.numItemID = IInner.numItemCode
																	WHERE
																		WIInner.numDomainID=',@numDomainID,'
																		AND IInner.numDomainID = ',@numDomainID,'
																		AND ISNULL(IInner.bitArchiveItem,0) = 0
																		AND ISNULL(IInner.IsArchieve,0) = 0
																		AND ISNULL(WIInner.numReorder,0) > 0 
																		AND ISNULL(WIInner.numOnHand,0) < ISNULL(WIInner.numReorder,0)',') ')

		END
  
		DECLARE @firstRec AS INTEGER                                        
		DECLARE @lastRec AS INTEGER                                        
		SET @firstRec = ( @CurrentPage - 1 ) * @PageSize                                        
		SET @lastRec = ( @CurrentPage * @PageSize + 1 )

		-- EXECUTE FINAL QUERY
		DECLARE @strFinal AS NVARCHAR(MAX)
		
		IF @CurrentPage = -1 AND @PageSize = -1
		BEGIN
			SET @strFinal = CONCAT('SELECT I.numItemCode, I.vcItemName, I.vcModelID INTO #tempTable ',@strSql + @strWhere,'; SELECT * FROM #tempTable; SELECT @TotalRecords = COUNT(*) FROM #tempTable; DROP TABLE #tempTable;')
		END
		ELSE
		BEGIN
			SET @strFinal = CONCAT('SELECT ROW_NUMBER() OVER(ORDER BY ',@columnName,' ',@columnSortOrder,') RowID,I.numItemCode INTO #temp2 ',@strSql, @strWhere,'; SELECT RowID, ',@strColumns,' INTO #tempTable ',@strSql, ' JOIN #temp2 tblAllItems ON I.numItemCode = tblAllItems.numItemCode ',@strWhere,' AND tblAllItems.RowID > ',@firstRec,' and tblAllItems.RowID <',@lastRec,'; SELECT * FROM  #tempTable; DROP TABLE #tempTable; SELECT @TotalRecords = COUNT(*) FROM #temp2; DROP TABLE #temp2;')
		END

		
		PRINT @strFinal
		exec sp_executesql @strFinal, N'@TotalRecords INT OUT', @TotRecs OUT

	END

	--UPDATE  
	--	#tempForm
 --   SET 
	--	vcDbColumnName = (CASE WHEN bitCustomField = 1 THEN vcFieldName + '~' + vcDbColumnName ELSE vcDbColumnName END)
                                  
	SELECT * FROM #tempForm
    DROP TABLE #tempForm
END
/****** Object:  StoredProcedure [dbo].[USP_ItemPricingRecomm]    Script Date: 02/19/2009 01:33:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- exec USP_ItemPricingRecomm @numItemCode=173028,@units=1,@numOppID=0,@numDivisionID=7052,@numDomainID=72,@tintOppType=1,@CalPrice=$50,@numWareHouseItemID=130634,@bitMode=0
--created by anoop jayaraj                          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_itempricingrecomm')
DROP PROCEDURE usp_itempricingrecomm
GO
CREATE PROCEDURE [dbo].[USP_ItemPricingRecomm]                                        
@numItemCode as numeric(9),                                        
@units Float,                                  
@numOppID as numeric(9)=0,                                
@numDivisionID as numeric(9)=0,                  
@numDomainID as numeric(9)=0,            
@tintOppType as tinyint,          
@CalPrice as money,      
@numWareHouseItemID as numeric(9),
@bitMode tinyint=0,
@vcSelectedKitChildItems VARCHAR(MAX) = '',
@numOppItemID NUMERIC(18,0) = 0
as                 
BEGIN TRY

DECLARE @numRelationship AS NUMERIC(9)
DECLARE @numProfile AS NUMERIC(9)             
DECLARE @monListPrice MONEY
DECLARE @bitCalAmtBasedonDepItems BIT
DECLARE @numDefaultSalesPricing TINYINT
DECLARE @tintPriceLevel INT
DECLARE @decmUOMConversion decimal(18,2)
DECLARE @intLeadTimeDays INT
DECLARE @intMinQty INT

/*Profile and relationship id */
SELECT 
	@numRelationship=numCompanyType,
	@numProfile=vcProfile,
	@tintPriceLevel = ISNULL(D.tintPriceLevel,0)
FROM 
	DivisionMaster D                  
JOIN 
	CompanyInfo C 
ON 
	C.numCompanyId=D.numCompanyID                  
WHERE 
	numDivisionID =@numDivisionID       
            
IF @tintOppType=1            
BEGIN
	SELECT
		@bitCalAmtBasedonDepItems=ISNULL(bitCalAmtBasedonDepItems,0) 
	FROM 
		Item 
	WHERE 
		numItemCode = @numItemCode
      
	/*Get List Price for item*/      
	IF((@numWareHouseItemID>0) AND EXISTS(SELECT * FROM Item WHERE numItemCode=@numItemCode AND charItemType='P'))      
	BEGIN      
		SELECT 
			@monListPrice=ISNULL(monWListPrice,0) 
		FROM 
			WareHouseItems 
		WHERE 
			numWareHouseItemID=@numWareHouseItemID      
		
		IF @monListPrice=0 
		BEGIN
			SELECT @monListPrice=monListPrice FROM Item WHERE numItemCode=@numItemCode       
		END
	END 
	ELSE
	BEGIN
		 SELECT 
			@monListPrice=monListPrice 
		FROM 
			Item 
		WHERE 
			numItemCode=@numItemCode      
	END      

	/*Use Calculated Price When item is Assembly/Kit,Calculate price based on sum of dependent items.*/
	IF @bitCalAmtBasedonDepItems = 1 
	BEGIN
		CREATE TABLE #TEMPSelectedKitChilds
		(
			ChildKitItemID NUMERIC(18,0),
			ChildKitWarehouseItemID NUMERIC(18,0),
			ChildKitChildItemID NUMERIC(18,0),
			ChildKitChildWarehouseItemID NUMERIC(18,0)
		)


		IF ISNULL(@numOppItemID,0) > 0
		BEGIN
			INSERT INTO 
				#TEMPSelectedKitChilds
			SELECT 
				OKI.numChildItemID,
				OKI.numWareHouseItemId,
				OKCI.numItemID,
				OKCI.numWareHouseItemId
			FROM
				OpportunityKitItems OKI
			INNER JOIN
				OpportunityKitChildItems OKCI
			ON
				OKI.numOppChildItemID = OKCI.numOppChildItemID
			WHERE
				OKI.numOppId = @numOppID
				AND OKI.numOppItemID = @numOppItemID
		END
		ELSE IF ISNULL(@vcSelectedKitChildItems,'') <> ''
		BEGIN
			INSERT INTO 
				#TEMPSelectedKitChilds
			SELECT 
				SUBSTRING
				(
					SUBSTRING(OutParam,0,CHARINDEX('-',OutParam)),
					0,
					CHARINDEX('#', SUBSTRING(OutParam,0,CHARINDEX('-',OutParam)))
				) AS numChildKitID,
				SUBSTRING
				(
					SUBSTRING(OutParam,0,CHARINDEX('-',OutParam)),
					CHARINDEX('#', SUBSTRING(OutParam,0,CHARINDEX('-',OutParam))) + 1,
					LEN(SUBSTRING(OutParam,0,CHARINDEX('-',OutParam)))
				) AS numChildKitWarehouseItemID,
				SUBSTRING
				(
					SUBSTRING(OutParam,CHARINDEX('-',OutParam) + 1, LEN(OutParam)),
					0,
					CHARINDEX('#', SUBSTRING(OutParam,CHARINDEX('-',OutParam) + 1, LEN(OutParam)))
				) AS numChildKitID,
				SUBSTRING
				(
					SUBSTRING(OutParam,CHARINDEX('-',OutParam) + 1, LEN(OutParam)),
					CHARINDEX('#', SUBSTRING(OutParam,CHARINDEX('-',OutParam) + 1, LEN(OutParam))) + 1,
					LEN(SUBSTRING(OutParam,CHARINDEX('-',OutParam) + 1, LEN(OutParam)))
				) AS numChildKitWarehouseItemID
		
			FROM 
				dbo.SplitString(@vcSelectedKitChildItems,',')
		END

		;WITH CTE (numItemCode, vcItemName, bitKitParent, numWarehouseItemID,numQtyItemsReq,numUOMId) AS
		(
			SELECT
				ID.numChildItemID,
				I.vcItemName,
				ISNULL(I.bitKitParent,0),
				ISNULL(ID.numWarehouseItemId,0),
				(ISNULL(ID.numQtyItemsReq,0) * ISNULL(dbo.fn_UOMConversion(ISNULL(ID.numUOMID, 0), I.numItemCode, @numDomainID, ISNULL(I.numBaseUnit, 0)),1)),
				ISNULL(numUOMId,0)
			FROM 
				[ItemDetails] ID
			INNER JOIN 
				[Item] I 
			ON 
				ID.[numChildItemID] = I.[numItemCode]
			WHERE   
				[numItemKitID] = @numItemCode
			UNION ALL
			SELECT
				ID.numChildItemID,
				I.vcItemName,
				ISNULL(I.bitKitParent,0),
				ISNULL(ID.numWarehouseItemId,0),
				(ISNULL(ID.numQtyItemsReq,0) * ISNULL(dbo.fn_UOMConversion(ISNULL(ID.numUOMID, 0), I.numItemCode, @numDomainID, ISNULL(I.numBaseUnit, 0)),1)),
				ISNULL(ID.numUOMId,0)
			FROM 
				CTE As Temp1
			INNER JOIN
				[ItemDetails] ID
			ON
				ID.numItemKitID = Temp1.numItemCode
			INNER JOIN 
				[Item] I 
			ON 
				ID.[numChildItemID] = I.[numItemCode]
			INNER JOIN
				#TEMPSelectedKitChilds
			ON
				ISNULL(Temp1.bitKitParent,0) = 1
				AND ID.numChildItemID = #TEMPSelectedKitChilds.ChildKitChildItemID
				AND ID.numWareHouseItemId = #TEMPSelectedKitChilds.ChildKitChildWarehouseItemID
		)

		SELECT  
				@CalPrice = ISNULL(  SUM(ISNULL(CASE WHEN I.[charItemType]='P' 
				THEN WI.[monWListPrice] 
				ELSE monListPrice END,0) * ID.[numQtyItemsReq] )  ,0)
		FROM    CTE ID
				INNER JOIN [Item] I ON ID.numItemCode = I.[numItemCode]
				left join  WareHouseItems WI
				on WI.numItemID=I.numItemCode and WI.[numWareHouseItemID]=ID.numWarehouseItemID
		WHERE
			ISNULL(ID.bitKitParent,0) = 0

		DROP TABLE #TEMPSelectedKitChilds
	END


	SELECT 
		@numDefaultSalesPricing = ISNULL(numDefaultSalesPricing,2) 
	FROM 
		Domain 
	WHERE 
		numDomainID = @numDomainID

	IF @numDefaultSalesPricing = 1 -- Use Price Level
	BEGIN
		DECLARE @newPrice MONEY
		DECLARE @finalUnitPrice FLOAT = 0
		DECLARE @tintRuleType INT
		DECLARE @tintDiscountType INT
		DECLARE @decDiscount FLOAT
		DECLARE @ItemPrice FLOAT

		SET @tintRuleType = 0
		SET @tintDiscountType = 0
		SET @decDiscount  = 0
		SET @ItemPrice = 0

		SELECT TOP 1
			@tintRuleType = tintRuleType,
			@tintDiscountType = tintDiscountType,
			@decDiscount = decDiscount
		FROM 
			PricingTable 
		WHERE 
			numItemCode = @numItemCode AND
			((@units BETWEEN intFromQty AND intToQty) OR tintRuleType = 3)

		IF @tintRuleType > 0 AND (@tintDiscountType > 0 OR @tintRuleType = 3)
		BEGIN
			IF @tintRuleType = 1 -- Deduct from List price
			BEGIN
				IF (SELECT ISNULL(charItemType,'') FROM Item WHERE numDomainID = @numDomainID AND numItemCode = @numItemCode) = 'P'
					If (SELECT COUNT(*) FROM Item WHERE numDomainID = @numDomainID AND numItemCode = @numItemCode AND (bitKitParent = 1 OR bitAssembly = 1)) > 0 
						SELECT @ItemPrice = dbo.GetUnitPriceForKitOrAssembly(@numDomainID,@numItemCode,@numWarehouseItemID,@units)
					ELSE
						SELECT @ItemPrice = ISNULL(monWListPrice,0) FROM WareHouseItems WHERE numDomainID = @numDomainID AND numItemID = @numItemCode AND numWareHouseItemID = @numWarehouseItemID
				ELSE
					SELECT @ItemPrice = ISNULL(monListPrice,0) FROM Item WHERE numDomainID = @numDomainID AND numItemCode = @numItemCode

				IF @tintDiscountType = 1 -- Percentage
				BEGIN
					IF @decDiscount > 0
						SELECT @finalUnitPrice = @ItemPrice - (@ItemPrice * (@decDiscount/100))
				END
				ELSE IF @tintDiscountType = 2 -- Flat Amount
				BEGIN
					SELECT @finalUnitPrice = @ItemPrice - @decDiscount
				END
				ELSE IF @tintDiscountType = 3 -- Named Price
				BEGIN
					SELECT @finalUnitPrice = @decDiscount
				END
			END
			ELSE IF @tintRuleType = 2 -- Add to Primary Vendor Cost
			BEGIN
				If (SELECT COUNT(*) FROM Item WHERE numDomainID = @numDomainID AND numItemCode = @numItemCode AND (bitKitParent = 1 OR bitAssembly = 1)) > 0 
					SELECT @ItemPrice = dbo.GetUnitPriceForKitOrAssembly(@numDomainID,@numItemCode,@numWarehouseItemID,@units)
				ELSE
					SELECT @ItemPrice = ISNULL(monCost,0) FROM Vendor WHERE numItemCode = @numItemCode AND numVendorID = (SELECT numVendorID FROM Item WHERE numItemCode = @numItemCode)

				IF @tintDiscountType = 1 AND @ItemPrice > 0 -- Percentage
				BEGIN
					IF @decDiscount > 0
						SELECT @finalUnitPrice = @ItemPrice + (@ItemPrice * (@decDiscount/100))
				END
				ELSE IF @tintDiscountType = 2 AND @ItemPrice > 0 -- Flat Amount
				BEGIN
					SELECT @finalUnitPrice = @ItemPrice + @decDiscount
				END
				ELSE IF @tintDiscountType = 3 -- Named Price
				BEGIN
					SELECT @finalUnitPrice = @decDiscount
				END
			END
			ELSE IF @tintRuleType = 3 -- Named Price
			BEGIN
				IF ISNULL(@tintPriceLevel,0) > 0
				BEGIN
					SET @decDiscount = 0
					SET @tintDiscountType = 2
					SET @finalUnitPrice = 0 
					
					SELECT 
						@finalUnitPrice = ISNULL(decDiscount,0)
					FROM
					(
						SELECT 
							ROW_NUMBER() OVER(PARTITION BY PricingTable.numItemCode ORDER BY numPricingID) Id,
							decDiscount
						FROM 
							PricingTable 
						WHERE 
						    PricingTable.numItemCode = @numItemCode 
							AND tintRuleType = 3 
					) TEMP
					WHERE
						Id = @tintPriceLevel

					IF ISNULL(@finalUnitPrice,0) = 0
					BEGIN
						SET @finalUnitPrice = @monListPrice
					END
					ELSE
					BEGIN
						SET @monListPrice = @finalUnitPrice
					END
				END
				ELSE
				BEGIN
					SET @finalUnitPrice = @decDiscount
					SET @monListPrice = @finalUnitPrice
					-- KEEP THIS LINE AT END
					SET @decDiscount = 0
					SET @tintDiscountType = 2
				END				
			END
		END

		IF @finalUnitPrice = 0
			SET @newPrice = @monListPrice
		ELSE
			SET @newPrice = @finalUnitPrice

		If @tintRuleType = 2
		BEGIN
			IF @finalUnitPrice > 0
			BEGIN
				IF @tintDiscountType = 1 -- Percentage
				BEGIN
					IF @newPrice > @monListPrice
						SET @decDiscount = 0
					ELSE
						If @monListPrice > 0
							SET @decDiscount = ((@monListPrice - @newPrice) * 100) / CAST(@monListPrice AS FLOAT)
						ELSE
							SET @decDiscount = 0
				END
				ELSE IF @tintDiscountType = 2 -- Flat Amount
				BEGIN
					If @newPrice > @monListPrice
						SET @decDiscount = 0
					ELSE
						SET @decDiscount = @monListPrice - @newPrice
				END
			END
			ELSE
			BEGIN
				SET @decDiscount = 0
				SET @tintDiscountType = 0
			END
		END

		SELECT 
			1 as numUintHour, 
			ISNULL(@newPrice,0) AS ListPrice,
			'' AS vcPOppName,
			'' AS bintCreatedDate,
			CASE 
				WHEN @tintRuleType = 0 THEN 'Price - List price'
				ELSE 
				   CASE @tintRuleType
				   WHEN 1 
						THEN 
							'Deduct from List price ' +  CASE 
														   WHEN @tintDiscountType = 1 THEN CONVERT(VARCHAR(20), @decDiscount) + '%'
														   WHEN @tintDiscountType = 2 THEN 'flat ' + CONVERT(VARCHAR(20), CAST(@decDiscount AS int))
														   ELSE 'Name price ' + CONVERT(VARCHAR(20), CAST(@decDiscount AS int))
														   END 
				   WHEN 2 THEN 'Add to primary vendor cost '  +  CASE 
														   WHEN @tintDiscountType = 1 THEN CONVERT(VARCHAR(20), @decDiscount) + '%'
														   WHEN @tintDiscountType = 2 THEN 'flat ' + CONVERT(VARCHAR(20), CAST(@decDiscount AS int))
														   ELSE 'Name price ' + CONVERT(VARCHAR(20), CAST(@decDiscount AS int))
														   END 
				   ELSE 'Flat ' + CONVERT(VARCHAR(20), @decDiscount)
				   END
			END AS OppStatus,
			CAST(0 AS NUMERIC) AS numPricRuleID,
			CAST(1 AS TINYINT) AS tintPricingMethod,
			CAST(@tintDiscountType AS TINYINT) AS tintDiscountTypeOriginal,
			CAST(@decDiscount AS FLOAT) AS decDiscountOriginal,
			CAST(@tintRuleType AS TINYINT) AS tintRuleType
	END
	ELSE -- Use Price Rule
	BEGIN
		/* Checks Pricebook if exist any.. */      
		SELECT TOP 1 1 AS numUnitHour,
				dbo.GetPriceBasedOnPriceBook(P.[numPricRuleID],@monListPrice,@units,I.numItemCode) 
				* (CASE WHEN (P.tintRuleType=2 OR (P.tintRuleType=0 AND ISNULL(PBT.tintRuleType,0) = 2)) THEN dbo.fn_UOMConversion(numBaseUnit,@numItemCode,@numDomainId,CASE WHEN ISNULL(numPurchaseUnit,0)>0 THEN numPurchaseUnit ELSE numBaseUnit END) ELSE 1 END)  AS ListPrice,
				vcRuleName AS vcPOppName,
				CONVERT(VARCHAR(20), NULL) AS bintCreatedDate,
				CASE WHEN numPricRuleID IS NULL THEN 'Price - List price'
					 ELSE 
					  CASE WHEN P.tintPricingMethod = 1
										 THEN 'Price Book Rule'
										 ELSE CASE PBT.tintRuleType
												WHEN 1 THEN 'Deduct from List price '
												WHEN 2 THEN 'Add to primary vendor cost '
												ELSE ''
											  END
											  + CASE 
													WHEN tintDiscountType = 1 THEN CONVERT(VARCHAR(20), decDiscount) + '%'
													WHEN tintDiscountType = 2 THEN 'flat ' + CONVERT(VARCHAR(20), CAST(decDiscount AS int))
													ELSE 'Named price ' + CONVERT(VARCHAR(20), CAST(decDiscount AS int))
												END + ' for every '
											  + CONVERT(VARCHAR(20), CASE WHEN [intQntyItems] = 0 THEN 1
																		  ELSE intQntyItems
																	 END)
											  + ' units, until maximum of '
											  + CONVERT(VARCHAR(20), decMaxDedPerAmt)
											  + CASE WHEN tintDiscountType = 1 THEN ' %' ELSE ' Flat' END END
				END AS OppStatus,P.numPricRuleID,P.tintPricingMethod,PBT.tintDiscountTypeOriginal,CAST(PBT.decDiscountOriginal AS FLOAT) AS decDiscountOriginal,PBT.tintRuleType
		FROM    Item I
				LEFT JOIN PriceBookRules P ON I.numDomainID = P.numDomainID
				LEFT JOIN PriceBookRuleDTL PDTL ON P.numPricRuleID = PDTL.numRuleID
				LEFT JOIN PriceBookRuleItems PBI ON P.numPricRuleID = PBI.numRuleID
				LEFT JOIN [PriceBookPriorities] PP ON PP.[Step2Value] = P.[tintStep2] AND PP.[Step3Value] = P.[tintStep3]
				CROSS APPLY dbo.GetPriceBasedOnPriceBookTable(P.[numPricRuleID],CASE WHEN bitCalAmtBasedonDepItems=1 then @CalPrice ELSE @monListPrice END ,@units,I.numItemCode) as PBT
		WHERE   numItemCode = @numItemCode AND tintRuleFor=@tintOppType
		AND 
		(
		((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 1 AND PDTL.numValue = @numDivisionID)) -- Priority 1
		OR ((tintStep2 = 2 AND PBI.numValue = I.numItemClassification) AND (tintStep3= 1 AND PDTL.numValue = @numDivisionID)) -- Priority 2
		OR ((tintStep2 = 3) AND (tintStep3 = 1 AND PDTL.numValue = @numDivisionID)) -- Priority 3

		OR ((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 4
		OR ((tintStep2 = 2 AND PBI.numValue = I.numItemClassification) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 5
		OR ((tintStep2 = 3) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 6

		OR ((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 3)) -- Priority 7
		OR ((tintStep2 = 2 AND PBI.numValue = I.numItemClassification) AND (tintStep3 = 3)) -- Priority 8
		OR ((tintStep2 = 3) and (tintStep3 = 3)) -- Priority 9
		)
		ORDER BY PP.Priority ASC
	END

	IF @bitMode=2  -- When this procedure is being called from USP_GetPriceOfItem for e-com
		RETURN 

                                   
	select 1 as numUnitHour,convert(money,ISNULL((monTotAmount/(case when numUnitHour< 1 then 1 else numUnitHour end)),0)) as ListPrice,vcPOppName,convert(varchar(20),bintCreatedDate) as bintCreatedDate,case when tintOppStatus=0 then 'Opportunity' when tintOppStatus=1 then 'Deal won'   
	when tintOppStatus=2 then 'Deal Lost' end as OppStatus,CAST(0 AS NUMERIC) AS numPricRuleID,CAST(0 AS TINYINT) AS tintPricingMethod,CAST(0 AS TINYINT) as tintDiscountTypeOriginal,CAST(0 AS FLOAT) as decDiscountOriginal,CAST(0 AS TINYINT) as tintRuleType                         
	from OpportunityItems itm                                        
	join OpportunityMaster mst                                        
	on mst.numOppId=itm.numOppId              
	where  tintOppType=1 and numItemCode=@numItemCode and mst.numDivisionID=@numDivisionID order by OppStatus desc  
  
  
	select 1 as numUnitHour,CASE WHEN @bitCalAmtBasedonDepItems=1 THEN convert(money,ISNULL(@CalPrice,0)) ELSE convert(money,ISNULL(@monListPrice,0)) End as ListPrice,'' as vcPOppName,convert(varchar(20),null) as bintCreatedDate,CASE WHEN @bitCalAmtBasedonDepItems=1 THEN 'Sum of dependent items' ELSE '' END as OppStatus
	,CAST(0 AS NUMERIC) AS numPricRuleID,CAST(0 AS TINYINT) AS tintPricingMethod ,CAST(0 AS TINYINT) as tintDiscountTypeOriginal,CAST(0 AS FLOAT) as decDiscountOriginal,CAST(0 AS TINYINT) as tintRuleType                    
END             
ELSE            
BEGIN

	If @numDivisionID=0
	BEGIN
		SELECT 
			@numDivisionID=V.numVendorID 
		FROM 
			[Vendor] V 
		INNER JOIN 
			Item I 
		ON 
			V.numVendorID = I.numVendorID 
			AND V.numItemCode=I.numItemCode 
		WHERE 
			V.[numItemCode]=@numItemCode 
			AND V.[numDomainID]=@numDomainID 
	END

	IF EXISTS
	(
		SELECT 
			ISNULL([monCost],0) ListPrice 
		FROM 
			[Vendor] V 
		INNER JOIN 
			dbo.Item I 
		ON 
			V.numItemCode = I.numItemCode
		WHERE
			V.[numItemCode]=@numItemCode 
			AND V.[numDomainID]=@numDomainID 
			AND V.numVendorID=@numDivisionID
	)       
	BEGIN      
		SELECT 
			@monListPrice=ISNULL([monCost],0) 
		FROM 
			[Vendor] V 
		INNER JOIN 
			dbo.Item I 
		ON 
			V.numItemCode = I.numItemCode
		WHERE 
			V.[numItemCode]=@numItemCode 
			AND V.[numDomainID]=@numDomainID 
			AND V.numVendorID=@numDivisionID
	END      
	ELSE
	BEGIN   
		SELECT 
			@monListPrice=ISNULL([monCost],0) 
		FROM 
			[Vendor] 
		WHERE 
			[numItemCode]=@numItemCode 
			AND [numDomainID]=@numDomainID
	END      
 

	SELECT TOP 1 
		1 AS numUnitHour,
		ISNULL(dbo.GetPriceBasedOnPriceBook(P.[numPricRuleID],@monListPrice,@units,I.numItemCode),0) AS ListPrice,
		vcRuleName AS vcPOppName,
		CONVERT(VARCHAR(20), NULL) AS bintCreatedDate,
		CASE 
		WHEN numPricRuleID IS NULL THEN 'Price - List price'
		ELSE  
				CASE 
				WHEN P.tintPricingMethod = 1 THEN 'Price Book Rule'
				ELSE 
					(CASE tintRuleType
						WHEN 1 THEN 'Deduct from List price '
						WHEN 2 THEN 'Add to primary vendor cost '
						ELSE ''
					END) + 
					(CASE 
						WHEN tintDiscountType = 1 THEN CONVERT(VARCHAR(20), decDiscount) + '%'
						ELSE 'flat ' + CONVERT(VARCHAR(20), CAST(decDiscount AS int))
					END) + ' for every ' + 
					CONVERT(VARCHAR(20), (CASE WHEN [intQntyItems] = 0 THEN 1 ELSE intQntyItems END))
					+ ' units, until maximum of '
					+ CONVERT(VARCHAR(20), decMaxDedPerAmt)
					+ (CASE WHEN tintDiscountType = 1 THEN ' %' ELSE ' Flat' END)
				END
		END AS OppStatus,
		P.numPricRuleID,
		P.tintPricingMethod,
		@numDivisionID AS numDivisionID
	FROM
		Item I 
	LEFT JOIN PriceBookRules P ON I.numDomainID = P.numDomainID
	LEFT JOIN PriceBookRuleDTL PDTL ON P.numPricRuleID = PDTL.numRuleID
	LEFT JOIN PriceBookRuleItems PBI ON P.numPricRuleID = PBI.numRuleID
	LEFT JOIN [PriceBookPriorities] PP ON PP.[Step2Value] = P.[tintStep2] AND PP.[Step3Value] = P.[tintStep3]
	WHERE   
		numItemCode = @numItemCode 
		AND tintRuleFor=@tintOppType
		AND (
			((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 1 AND PDTL.numValue = @numDivisionID)) -- Priority 1
			OR ((tintStep2 = 2 AND PBI.numValue = I.numItemClassification) AND (tintStep3= 1 AND PDTL.numValue = @numDivisionID)) -- Priority 2
			OR ((tintStep2 = 3) AND (tintStep3 = 1 AND PDTL.numValue = @numDivisionID)) -- Priority 3

			OR ((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 4
			OR ((tintStep2 = 2 AND PBI.numValue = I.numItemClassification) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 5
			OR ((tintStep2 = 3) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 6

			OR ((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 3)) -- Priority 7
			OR ((tintStep2 = 2 AND PBI.numValue = I.numItemClassification) AND (tintStep3 = 3)) -- Priority 8
			OR ((tintStep2 = 3) and (tintStep3 = 3)) -- Priority 9
			)
	ORDER BY PP.Priority ASC

	SET @decmUOMConversion=(SELECT 
								dbo.fn_UOMConversion(ISNULL(I.numPurchaseUnit, 0) , I.numItemCode,I.numDomainId, ISNULL(I.numBaseUnit, 0)) 
							FROM 
								Item I 
							WHERE 
								I.numItemCode=@numItemCode)

	SELECT
		@intLeadTimeDays = ISNULL(VSM.numListValue,0),
		@intMinQty=ISNULL(intMinQty,0)
	FROM
		Item
	JOIN
		Vendor
	ON
		Item.numVendorID = Vendor.numVendorID
		AND Item.numItemCode=Vendor.numItemCode
	LEFT JOIN
		VendorShipmentMethod VSM
	ON
		Vendor.numVendorID = VSM.numVendorID
		AND VSM.numDomainID=@numDomainID
		AND VSM.bitPrimary = 1
		AND VSM.bitPreferredMethod = 1
	WHERE
		Item.numItemCode = @numItemCode

	select 1 as numUnitHour,convert(money,ISNULL((monTotAmount/(case when numUnitHour< 1 then 1 else numUnitHour end)),0)) as ListPrice,
	@decmUOMConversion *(convert(money,ISNULL((monTotAmount/(case when numUnitHour< 1 then 1 else numUnitHour end)),0)))
	as convrsPrice
	,vcPOppName,convert(varchar(20),bintCreatedDate) as bintCreatedDate,case when tintOppStatus=0 then 'Opportunity' 
		 when tintOppStatus=1 then 'Deal won'  
		 when tintOppStatus=2 then 'Deal Lost' end as OppStatus
		 ,CAST(0 AS NUMERIC) AS numPricRuleID,CAST(0 AS TINYINT) AS tintPricingMethod,@numDivisionID as numDivisionID                         
		 from OpportunityItems itm                                        
		 join OpportunityMaster mst                                        
		 on mst.numOppId=itm.numOppId                                        
		 where  tintOppType=2 and numItemCode=@numItemCode and mst.numDivisionID=@numDivisionID order by OppStatus,bintCreatedDate DESC

 		select 1 as numUnitHour,convert(money,ISNULL(@monListPrice,0)) as ListPrice,
		@decmUOMConversion*(Convert(money,ISNULL(@monListPrice,0))) as convrsPrice
		,@intLeadTimeDays as LeadTime,@intMinQty as intMinQty,
		(SELECT DATEADD(day,@intLeadTimeDays,GETDATE())) AS EstimatedArrival
		,'' as vcPOppName,convert(varchar(20),null) as bintCreatedDate,''  as OppStatus,@numDivisionID as numDivisionID
 		,CAST(0 AS NUMERIC) AS numPricRuleID,CAST(0 AS TINYINT) AS tintPricingMethod
END

END TRY
BEGIN CATCH
    SELECT 0 AS ListPrice
    SELECT 0 AS ListPrice
    SELECT 0 AS ListPrice
END CATCH

GO
/****** Object:  StoredProcedure [dbo].[usp_ManageAddlContInfo]    Script Date: 04/02/2009 00:17:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Modified By Anoop jayaraj                  
 GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_manageaddlcontinfo')
DROP PROCEDURE usp_manageaddlcontinfo
GO
CREATE PROCEDURE [dbo].[usp_ManageAddlContInfo]                                    
 @numcontactId numeric=0,                                    
 @numContactType numeric=0,                                    
 @vcDepartment numeric(9)=0,                                    
 @vcCategory numeric(9)=0,                                    
 @vcGivenName varchar(100)='',                                    
 @vcFirstName varchar(50)='',                                    
 @vcLastName varchar(50)='',                                    
 @numDivisionId numeric,                                    
 @numPhone varchar(15)='',                                    
 @numPhoneExtension varchar(7)='',                                    
 @numCell varchar(15)='',                                    
 @NumHomePhone varchar(15)='',                                    
 @vcFax varchar(15)='',                                    
 @vcEmail varchar(50)='',                                    
 @VcAsstFirstName varchar(50)='',                                    
 @vcAsstLastName varchar(50)='',                                    
 @numAsstPhone varchar(15)='',                                    
 @numAsstExtn varchar(6)='',                                    
 @vcAsstEmail varchar(50)='',                                                      
 @charSex char(1)='',                                    
 @bintDOB datetime,                                    
 @vcPosition numeric(9)=0,                                                     
 @txtNotes text='',                                                     
 @numUserCntID numeric,                                                                                              
 @numDomainID numeric=1,                                                   
 @vcPStreet varchar(100)='',                                    
 @vcPCity varchar(50)='',                                    
 @vcPPostalCode varchar(15)='',                                    
 @vcPState numeric(9)=0,                                    
 @vcPCountry numeric(9)=0,                  
 @numManagerID numeric=0,        
 @numTeam as numeric(9)=0,        
 @numEmpStatus as numeric(9)=0,
 @vcTitle AS VARCHAR(100)='',
 @bitPrimaryContact AS BIT=0,
 @bitOptOut AS BIT=0,
 @vcLinkedinId VARCHAR(30)=NULL,
 @vcLinkedinUrl VARCHAR(300)=NULL,
 @numECampaignID NUMERIC(18)=0,
 @vcPassword VARCHAR(50) = '',
 @vcTaxID VARCHAR(100) = ''
AS   


	IF ISNULL(@numContactType,0) = 0
	BEGIN
		SET @numContactType = 70
	END

	If LEN(ISNULL(@vcFirstName,'')) = 0
	BEGIN
		SET @vcFirstName = '-'
	END

	If LEN(ISNULL(@vcLastName,'')) = 0
	BEGIN
		SET @vcLastName = '-'
	END

 SELECT @vcGivenName = @vcLastName + ', ' + @vcFirstName  + '.eml'                                 
   if @bintDOB ='Jan  1 1753 12:00AM' set  @bintDOB=null                      
IF @numContactId=0
     BEGIN   

IF @bitPrimaryContact = 1 
    BEGIN            
        DECLARE @numID AS NUMERIC(9)            
        DECLARE @bitPrimaryContactCheck AS BIT             
             
        SELECT TOP 1
                @numID = numContactID,
                @bitPrimaryContactCheck = ISNULL(bitPrimaryContact,0)
        FROM    AdditionalContactsInformation
        WHERE   numDivisionID = @numDivisionID 
                   
        IF @@rowcount = 0 
            SET @numID = 0             
        WHILE @numID > 0            
            BEGIN            
                IF @bitPrimaryContactCheck = 1 
                    UPDATE  AdditionalContactsInformation
                    SET     bitPrimaryContact = 0
                    WHERE   numContactID = @numID  
                              
                SELECT TOP 1
                        @numID = numContactID,
                        @bitPrimaryContactCheck = ISNULL(bitPrimaryContact,0)
                FROM    AdditionalContactsInformation
                WHERE   numDivisionID = @numDivisionID
                        AND numContactID > @numID    
                                
                IF @@rowcount = 0 
                    SET @numID = 0             
            END            
    END  
    ELSE IF @bitPrimaryContact = 0
    BEGIN
		IF (SELECT COUNT(*) FROM AdditionalContactsInformation WHERE numDivisionID = @numDivisionID AND numContactId!=@numContactId AND ISNULL(bitPrimaryContact,0)=1)=0
			SET @bitPrimaryContact = 1
    END                 
    
    
declare @bitAutoPopulateAddress bit 
declare @tintPoulateAddressTo tinyint 

select @bitAutoPopulateAddress= isnull(bitAutoPopulateAddress,'0'),@tintPoulateAddressTo=isnull(tintPoulateAddressTo,'0') from Domain where numDomainId = @numDomainId ;
if (@bitAutoPopulateAddress = 1)
	if(@tintPoulateAddressTo = 1) --if primary address is not specified then getting billing address    
	begin
		 
		  select                   
			@vcPStreet = AD1.vcStreet,
			@vcPCity= AD1.vcCity,
			@vcPState= AD1.numState,
			@vcPPostalCode= AD1.vcPostalCode,
			@vcPCountry= AD1.numCountry              
		  from divisionMaster  DM              
		   LEFT JOIN dbo.AddressDetails AD1 ON AD1.numDomainID=DM.numDomainID 
			AND AD1.numRecordID=DM.numDivisionID AND AD1.tintAddressOf=2 AND AD1.tintAddressType=1 AND AD1.bitIsPrimary=1
		  where numDivisionID=@numDivisionId    
	end  
	else if (@tintPoulateAddressTo = 2)-- Primary Address is Shipping Address
	begin
		  select                   
			@vcPStreet= AD2.vcStreet,
			@vcPCity=AD2.vcCity,
			@vcPState=AD2.numState,
			@vcPPostalCode=AD2.vcPostalCode,
			@vcPCountry=AD2.numCountry                   
		  from divisionMaster DM
		  LEFT JOIN dbo.AddressDetails AD2 ON AD2.numDomainID=DM.numDomainID 
		  AND AD2.numRecordID= DM.numDivisionID AND AD2.tintAddressOf=2 AND AD2.tintAddressType=2 AND AD2.bitIsPrimary=1
		  where numDivisionID=@numDivisionId    
	
	end                       
                       
 if (@numEmpStatus is null or @numEmpStatus=0) set @numEmpStatus=658        
 
    INSERT into AdditionalContactsInformation                                    
    (                  
   numContactType,                  
   vcDepartment,                  
   vcCategory,                  
   vcGivenName,                  
   vcFirstName,                  
   vcLastName,                   
   numDivisionId ,                  
   numPhone ,                  
   numPhoneExtension,                  
   numCell ,                  
   numHomePhone ,                          
   vcFax ,                  
   vcEmail ,                  
   VcAsstFirstName ,                  
   vcAsstLastName ,                  
   numAsstPhone ,                  
   numAsstExtn ,                  
   vcAsstEmail  ,                  
   charSex ,                  
   bintDOB ,                  
   vcPosition ,                  
   txtNotes ,                  
   numCreatedBy ,                                    
   bintCreatedDate,             
   numModifiedBy,            
   bintModifiedDate,                   
   numDomainID,                  
   numManagerID,                   
   numRecOwner,                  
   numTeam,                  
   numEmpStatus,
   vcTitle,bitPrimaryContact,bitOptOut,
   vcLinkedinId,vcLinkedinUrl,numECampaignID,vcTaxID           
   )                                    
    VALUES                                    
      (                  
   @numContactType,                  
   @vcDepartment,                  
   @vcCategory,                  
   @vcGivenName ,                  
   @vcFirstName ,                  
   @vcLastName,                   
   @numDivisionId ,                  
   @numPhone ,                                    
   @numPhoneExtension,                  
   @numCell ,                  
   @NumHomePhone ,                  
   @vcFax ,                  
   @vcEmail ,                  
   @VcAsstFirstName,                         
   @vcAsstLastName,                  
   @numAsstPhone,                  
   @numAsstExtn,                  
   @vcAsstEmail,                  
   @charSex,                  
   @bintDOB ,                  
   @vcPosition,                                   
   @txtNotes,                  
   @numUserCntID,                  
   getutcdate(),             
   @numUserCntID,                  
   getutcdate(),                   
   @numDomainID,                   
   @numManagerID,                   
   @numUserCntID,                   
   @numTeam,                  
   @numEmpStatus,
   @vcTitle,@bitPrimaryContact,@bitOptOut,
   @vcLinkedinId,@vcLinkedinUrl,@numECampaignID,@vcTaxID           
   )                                    
                     
 set @numcontactId= SCOPE_IDENTITY()
 
 SELECT @numcontactId             

     INSERT INTO dbo.AddressDetails (
	 	vcAddressName,
	 	vcStreet,
	 	vcCity,
	 	vcPostalCode,
	 	numState,
	 	numCountry,
	 	bitIsPrimary,
	 	tintAddressOf,
	 	tintAddressType,
	 	numRecordID,
	 	numDomainID
	 ) VALUES ('Primary', @vcPStreet,@vcPCity,@vcPPostalCode,@vcPState,@vcPCountry,1,1,0,@numcontactId,@numDomainID)
     
	 IF ISNULL(@numECampaignID,0) > 0 
	 BEGIN
		  --Manage ECampiagn When Campaign is changed from inline edit
		DECLARE @Date AS DATETIME 
		SELECT @Date = dbo.[GetUTCDateWithoutTime]()

 		EXEC [USP_ManageConEmailCampaign]
		@numConEmailCampID = 0, --  numeric(9, 0)
		@numContactID = @numcontactId, --  numeric(9, 0)
		@numECampaignID = @numECampaignID, --  numeric(9, 0)
		@intStartDate = @Date, --  datetime
		@bitEngaged = 1, --  bit
		@numUserCntID = @numUserCntID --  numeric(9, 0)     
	END                                         
                                                 
  END                                    
 ELSE if @numContactId>0                                                        
    BEGIN                                    
    UPDATE AdditionalContactsInformation SET                                    
      numContactType=@numContactType,                                    
      vcGivenName=@vcGivenName,                                    
      vcFirstName=@vcFirstName ,                                    
      vcLastName =@vcLastName ,                   
      numDivisionId =@numDivisionId ,                                    
      numPhone=@numPhone ,                                    
      numPhoneExtension=@numPhoneExtension,                                    
      numCell =@numCell ,                                    
      NumHomePhone =@NumHomePhone ,                                    
      vcFax=@vcFax ,                                    
      vcEmail=@vcEmail ,                                    
      VcAsstFirstName=@VcAsstFirstName,                                    
      vcAsstLastName=@vcAsstLastName,                                    
      numAsstPhone=@numAsstPhone,                                    
      numAsstExtn=@numAsstExtn,                                    
      vcAsstEmail=@vcAsstEmail,                                                      
      charSex=@charSex,                            
      bintDOB=@bintDOB ,                                    
      vcPosition=@vcPosition,                                                    
      txtNotes=@txtNotes,                                                     
      numModifiedBy=@numUserCntID,                                    
      bintModifiedDate=getutcdate(),                                    
      numManagerID=@numManagerID,
      bitPrimaryContact=@bitPrimaryContact                              
     WHERE numContactId=@numContactId   
                    
     Update dbo.AddressDetails set                   
       vcStreet=@vcPStreet,                                    
       vcCity =@vcPCity,                                    
       vcPostalCode=@vcPPostalCode,                                     
       numState=@vcPState,                                    
       numCountry=@vcPCountry
	  where numRecordID=@numContactId AND bitIsPrimary=1 AND tintAddressOf=1 AND tintAddressType=0
 
     SELECT @numcontactId   
                                                
  END    
  ------Check the Email id Exist into the Email Master Table

	IF @vcPassword IS NOT NULL AND  LEN(@vcPassword) > 0
	BEGIN
		IF EXISTS (SELECT numExtranetID FROM ExtarnetAccounts WHERE numDivisionID = @numDivisionID)
		BEGIN
			IF EXISTS (SELECT numExtranetDtlID FROM ExtranetAccountsDtl WHERE numContactID=@numContactID)
			BEGIN
				UPDATE
					ExtranetAccountsDtl
				SET 
					vcPassword=@vcPassword
					,tintAccessAllowed=1
				WHERE
					numContactID=@numContactID
			END
			ELSE
			BEGIN
				DECLARE @numExtranetID NUMERIC(18,0)
				SELECT @numExtranetID=numExtranetID FROM ExtarnetAccounts WHERE numDivisionID = @numDivisionID

				INSERT INTO ExtranetAccountsDtl (numExtranetID,numContactID,vcPassword,tintAccessAllowed,numDomainID)
				VALUES (@numExtranetID,@numContactID,@vcPassword,1,@numDomainID)
			END
		END
		ELSE
		BEGIN
			RAISERROR('Organization e-commerce access is not enabled',16,1)
		END
	END



DECLARE @numCount AS NUMERIC(18,0)
SET @numCount = 0
IF @vcEmail <> ''
BEGIN 
SELECT @numCount = COUNT(*) FROM dbo.EmailMaster WHERE vcEmailID =@vcEmail and numDomainId=@numDomainID
IF @numCount = 0 
BEGIN
	INSERT INTO EmailMaster(vcEmailId,vcName,numDomainId,numContactId)
	VALUES(@vcEmail,@vcFirstName,@numDomainID,@numcontactId)
END
else
begin
	update EmailMaster set numContactId=@numcontactId where vcEmailID =@vcEmail and numDomainId=@numDomainID
end
END 
--IF @vcAsstEmail <> ''
--BEGIN
--SELECT @numCount = COUNT(*) FROM dbo.EmailMaster WHERE vcEmailID =@vcAsstEmail
--IF @numCount = 0 
--BEGIN
--	INSERT INTO EmailMaster(vcEmailId,vcName,numDomainId,numContactId)
--	VALUES(@vcAsstEmail,@vcFirstName,@numDomainID,@numcontactId)
--END
--END
---------------------------------------------------------

/****** Object:  StoredProcedure [dbo].[USP_ManageCategory]    Script Date: 07/26/2008 16:19:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj        
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_managecategory')
DROP PROCEDURE usp_managecategory
GO
CREATE PROCEDURE [dbo].[USP_ManageCategory]        
@numCatergoryId as numeric(9),        
@vcCatgoryName as varchar(1000),       
@numDomainID as numeric(9)=0,
@vcDescription as VARCHAR(MAX),
@intDisplayOrder AS INT ,
@vcPathForCategoryImage AS VARCHAR(100),
@numDepCategory AS NUMERIC(9,0),
@numCategoryProfileID AS NUMERIC(18,0),
@vcCatgoryNameURL VARCHAR(1000),
@vcMetaTitle as varchar(1000),
@vcMetaKeywords as varchar(1000),
@vcMetaDescription as varchar(1000)
as 

	DECLARE @tintLevel AS TINYINT
	SET @tintLevel = 1

	IF	@numDepCategory <> 0
	BEGIN
		SELECT @tintLevel = (tintLevel + 1) FROM dbo.Category WHERE numCategoryID = @numDepCategory
	END       
       
	IF @numCatergoryId=0        
	BEGIN        
		INSERT INTO Category 
		(
			vcCategoryName,
			tintLevel,
			numDomainID,
			vcDescription,
			intDisplayOrder,
			vcPathForCategoryImage,
			numDepCategory,
			numCategoryProfileID,
			vcCategoryNameURL,
			vcMetaTitle,
			vcMetaKeywords,
			vcMetaDescription
		)        
		VALUES
		(
			@vcCatgoryName,
			@tintLevel,
			@numDomainID,
			@vcDescription,
			@intDisplayOrder,
			@vcPathForCategoryImage,
			@numDepCategory,
			@numCategoryProfileID,
			@vcCatgoryNameURL,
			@vcMetaTitle,
			@vcMetaKeywords,
			@vcMetaDescription
		) 
		set @numCatergoryId = SCOPE_IDENTITY();   
		insert into SiteCategories 
		(
			numSiteID,
			numCategoryID
		)
		SELECT
			numSiteID
			,@numCatergoryId
		FROM
			CategoryProfileSites
		WHERE
			 numCategoryProfileID=@numCategoryProfileID    
	END        
	ELSE IF @numCatergoryId>0        
	BEGIN        
		UPDATE 
			Category 
		SET 
			vcCategoryName=@vcCatgoryName,
			vcDescription=@vcDescription,
			intDisplayOrder = @intDisplayOrder,
			tintLevel = @tintLevel,
			vcPathForCategoryImage = @vcPathForCategoryImage,
			numDepCategory = @numDepCategory,
			vcCategoryNameURL=@vcCatgoryNameURL,
			vcMetaTitle=@vcMetaTitle,
			vcMetaKeywords=@vcMetaKeywords,
			vcMetaDescription=@vcMetaDescription
		WHERE 
			numCategoryID=@numCatergoryId        
	END
	
GO
/****** Object:  StoredProcedure [dbo].[usp_ManageContInfo]    Script Date: 07/26/2008 16:19:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_managecontinfo')
DROP PROCEDURE usp_managecontinfo
GO
CREATE PROCEDURE [dbo].[usp_ManageContInfo]                  
 @numContactId numeric=0,                  
 @vcDepartment numeric(9)=0,                  
 @vcFirstName varchar(50)='',                  
 @vcLastName varchar(50)='',                  
 @vcEmail varchar(50)='',                  
 @vcSex char(1)='',                  
 @vcPosition  numeric(9)=0,                  
 @numPhone varchar(15)='',                  
 @numPhoneExtension varchar(7)='',                  
 @bintDOB DATETIME=null,                  
 @txtNotes text='',                  
 @numUserCntID numeric=0,                      
 @numCategory numeric=0,                   
 @numCell varchar(15)='',                  
 @NumHomePhone varchar(15)='',                  
 @vcFax varchar(15)='',                  
 @vcAsstFirstName varchar(50)='',                  
 @vcAsstLastName varchar(50)='',                  
 @numAsstPhone varchar(15)='',                  
 @numAsstExtn varchar(6)='',                  
 @vcAsstEmail varchar(50)='',                      
 @numContactType numeric=0 ,               
 @bitOptOut bit=0,                
 @numManagerId numeric=0,                
 @numTeam as numeric(9)=0,                
 @numEmpStatus as numeric(9)=0,        
 @vcAltEmail as varchar(100),        
 @vcTitle as varchar(100),
 @numECampaignID AS NUMERIC(9),
 @bitSelectiveUpdate AS BIT = 0, --Update fields whose value are supplied
 @bitPrimaryContact AS BIT=0,
 @vcTaxID VARCHAR(100) = ''        
AS                  
IF @bitSelectiveUpdate = 0
BEGIN

if @bintDOB ='Jan  1 1753 12:00AM' set  @bintDOB=null                  
       
        DECLARE @numDivisionID AS NUMERIC(9)            
        SELECT  @numDivisionID = numDivisionID FROM AdditionalContactsInformation WHERE numContactID = @numContactId   
           
 IF @bitPrimaryContact = 1 
    BEGIN            
        DECLARE @numID AS NUMERIC(9)            
        DECLARE @bitPrimaryContactCheck AS BIT             
             
        SELECT TOP 1
                @numID = numContactID,
                @bitPrimaryContactCheck = ISNULL(bitPrimaryContact,0)
        FROM    AdditionalContactsInformation
        WHERE   numDivisionID = @numDivisionID 
                   
        IF @@rowcount = 0 
            SET @numID = 0             
        WHILE @numID > 0            
            BEGIN            
                IF @bitPrimaryContactCheck = 1 
                    UPDATE  AdditionalContactsInformation
                    SET     bitPrimaryContact = 0
                    WHERE   numContactID = @numID  
                              
                SELECT TOP 1
                        @numID = numContactID,
                        @bitPrimaryContactCheck = ISNULL(bitPrimaryContact,0)
                FROM    AdditionalContactsInformation
                WHERE   numDivisionID = @numDivisionID
                        AND numContactID > @numID    
                                
                IF @@rowcount = 0 
                    SET @numID = 0             
            END            
    END 
    ELSE IF @bitPrimaryContact = 0
    BEGIN
		IF (SELECT COUNT(*) FROM AdditionalContactsInformation WHERE numDivisionID = @numDivisionID AND numContactId!=@numContactId AND ISNULL(bitPrimaryContact,0)=1)=0
			SET @bitPrimaryContact = 1
    END                 
    
    UPDATE AdditionalContactsInformation SET                  
     vcDepartment=@vcDepartment,                  
     vcFirstName=@vcFirstName ,                  
     vcLastName =@vcLastName ,                  
     numPhone=@numPhone ,                  
     numPhoneExtension=@numPhoneExtension,                  
     vcEmail=@vcEmail ,                  
     charSex=@vcSex ,                  
     vcPosition = @vcPosition,                  
     bintDOB=@bintDOB ,                  
     txtNotes=@txtNotes,                  
     numModifiedBy=@numUserCntID,                  
     bintModifiedDate=getutcdate(),                
     vcCategory = @numCategory,                  
     numCell=@numCell,                  
     NumHomePhone=@NumHomePhone,                  
     vcFax=@vcFax,                  
     vcAsstFirstName=@vcAsstFirstName,                  
     vcAsstLastName=@vcAsstLastName,                  
     numAsstPhone=@numAsstPhone,                  
     numAsstExtn=@numAsstExtn,                  
     vcAsstEmail=@vcAsstEmail,                     
     numContactType=@numContactType,                      
     bitOptOut=@bitOptOut,                
     numManagerId=@numManagerId,                
     numTeam=@numTeam,           
     numEmpStatus = @numEmpStatus,        
     vcTitle=@vcTitle,        
     vcAltEmail= @vcAltEmail,
     numECampaignID = @numECampaignID,
     bitPrimaryContact=@bitPrimaryContact,
	 vcTaxID=@vcTaxID
 WHERE numContactId=@numContactId
 
 /*Added by chintan BugID-262*/
	 DECLARE @Date AS DATETIME 
	 SELECT @Date = dbo.[GetUTCDateWithoutTime]()
	 IF @numECampaignID = -1 
		SET @numECampaignID=0
	 
 	EXEC [USP_ManageConEmailCampaign]
	@numConEmailCampID = 0, --  numeric(9, 0)
	@numContactID = @numContactID, --  numeric(9, 0)
	@numECampaignID = @numECampaignID, --  numeric(9, 0)
	@intStartDate = @Date, --  datetime
	@bitEngaged = 1, --  bit
	@numUserCntID = @numUserCntID --  numeric(9, 0)
END


IF @bitSelectiveUpdate = 1
BEGIN
		DECLARE @sql VARCHAR(1000)
		SET @sql = 'update AdditionalContactsInformation Set '

		IF(LEN(@vcFirstName)>0)
		BEGIN
			SET @sql =@sql + ' vcFirstName=''' + @vcFirstName + ''', '
		END
		IF(LEN(@vcLastName)>0)
		BEGIN
			SET @sql =@sql + ' vcLastName=''' + @vcLastName + ''', '
		END
		
		IF(LEN(@vcEmail)>0)
		BEGIN
			SET @sql =@sql + ' vcEmail=''' + @vcEmail + ''', '
		END
		
		IF(LEN(@numPhone)>0)
		BEGIN
			SET @sql =@sql + ' numPhone=''' + @numPhone + ''', '
		END
		
		IF(LEN(@numPhoneExtension)>0)
		BEGIN
			SET @sql =@sql + ' numPhoneExtension=''' + @numPhoneExtension + ''', '
		END
		
		IF(LEN(@numCell)>0)
		BEGIN
			SET @sql =@sql + ' numCell=''' + @numCell + ''', '
		END
		
		IF(LEN(@vcFax)>0)
		BEGIN
			SET @sql =@sql + ' vcFax=''' + @vcFax + ''', '
		END
		
		SET @sql =@sql + ' bitOptOut=''' + CONVERT(VARCHAR(10),@bitOptOut) + ''', '
		
		IF(@numUserCntID>0)
		BEGIN
			SET @sql =@sql + ' numModifiedBy='+ CONVERT(VARCHAR(10),@numUserCntID) +', '
		END
		
	
	
		SET @sql=SUBSTRING(@sql,0,LEN(@sql)) + ' '
		SET @sql =@sql + 'where numContactId= '+CONVERT(VARCHAR(10),@numContactId)

		PRINT @sql
		EXEC(@sql) 
END 
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManagegeReportDashboard')
DROP PROCEDURE USP_ManagegeReportDashboard
GO
CREATE PROCEDURE [dbo].[USP_ManagegeReportDashboard]
@numDomainID AS NUMERIC(18,0),
@numDashBoardID AS NUMERIC(18,0),
@numReportID AS NUMERIC(18,0),
@numUserCntID AS NUMERIC(9)=0,
@tintReportType AS TINYINT,
@tintChartType AS TINYINT,
@vcHeaderText AS VARCHAR(100)='',
@vcFooterText AS VARCHAR(100)='',
@vcXAxis AS VARCHAR(50),
@vcYAxis AS VARCHAR(50),
@vcAggType AS VARCHAR(50),
@tintReportCategory AS TINYINT,
@numDashboardTemplateID NUMERIC(18,0)
as

	IF @numDashBoardID=0 
	BEGIN
		DECLARE @tintRow AS TINYINT;SET @tintRow=0
		SELECT @tintRow = ISNULL(MAX(tintRow),0) + 1 FROM ReportDashboard WHERE numDomainID=@numDomainID AND numUserCntID=@numUserCntID AND ISNULL(numDashboardTemplateID,0)=ISNULL(@numDashboardTemplateID,0)
		
		INSERT INTO ReportDashboard (numDomainID, numReportID, numUserCntID, tintReportType, tintChartType, vcHeaderText, vcFooterText, tintRow, tintColumn,vcXAxis,vcYAxis,vcAggType,tintReportCategory,intHeight,intWidth,numDashboardTemplateID,bitNewAdded)
		values (@numDomainID, @numReportID, @numUserCntID, @tintReportType, @tintChartType, @vcHeaderText, @vcFooterText, @tintRow, 1,@vcXAxis,@vcYAxis,@vcAggType,@tintReportCategory,7,8,@numDashboardTemplateID,1)
	END
	ELSE IF @numDashBoardID>0
	BEGIN
		update ReportDashboard set
			numReportID=@numReportID, 
			tintReportType=@tintReportType, 
			tintChartType=@tintChartType, 
			vcHeaderText=@vcHeaderText, 
			vcFooterText=@vcFooterText,vcXAxis=@vcXAxis,vcYAxis=@vcYAxis,vcAggType=@vcAggType,tintReportCategory=@tintReportCategory where numDashBoardID=@numDashBoardID AND numDomainID=@numDomainID
	END
	
GO
        
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageParentChildCustomFieldMap')
	DROP PROCEDURE USP_ManageParentChildCustomFieldMap
GO

CREATE PROCEDURE [dbo].[USP_ManageParentChildCustomFieldMap]
    @numParentChildFieldID numeric(18, 0),
	@numDomainID numeric(18, 0),
	@tintParentModule tinyint,
	@numParentFieldID numeric(18, 0),
	@tintChildModule tinyint,
	@numChildFieldID numeric(18, 0),
	@tintMode AS TINYINT,
	@bitCustomField AS BIT = 0
AS

IF @tintMode = 1
BEGIN
	DELETE FROM ParentChildCustomFieldMap WHERE numDomainID=@numDomainID AND numParentChildFieldID=@numParentChildFieldID
END
ELSE
BEGIN
	IF EXISTS(SELECT 1 FROM ParentChildCustomFieldMap WHERE numDomainID=@numDomainID AND tintChildModule=@tintChildModule AND numChildFieldID=@numChildFieldID AND bitCustomField=@bitCustomField)
	BEGIN
		RAISERROR ('AlreadyExists',16,1);
	END
	ELSE
	BEGIN
		DECLARE @ParentFld_type AS VARCHAR(20),@ChildFld_type AS VARCHAR(20)

		IF ISNULL(@bitCustomField,0) = 0
		BEGIN
			SELECT @ParentFld_type=vcAssociatedControlType FROM DycFieldMaster WHERE numFieldId=@numParentFieldID
			SELECT @ChildFld_type=vcAssociatedControlType FROM DycFieldMaster WHERE numFieldId=@numChildFieldID

			IF @ParentFld_type != @ChildFld_type
			BEGIN
				RAISERROR ('FieldTypeMisMatch',16,1);
			END	
		
			ELSE
			BEGIN
				INSERT INTO dbo.ParentChildCustomFieldMap 
				(
					numDomainID,tintParentModule,numParentFieldID,tintChildModule,numChildFieldID,bitCustomField
				) 
				SELECT @numDomainID,@tintParentModule,@numParentFieldID,@tintChildModule,@numChildFieldID,0
			END	
		END
		ELSE
		BEGIN
			SELECT @ParentFld_type=Fld_type FROM CFW_Fld_Master WHERE numDomainID=@numDomainID AND Grp_id=@tintParentModule AND Fld_id=@numParentFieldID
			SELECT @ChildFld_type=Fld_type FROM CFW_Fld_Master WHERE numDomainID=@numDomainID AND Grp_id=@tintChildModule AND Fld_id=@numChildFieldID
			
			IF @ParentFld_type != @ChildFld_type
			BEGIN
				RAISERROR ('FieldTypeMisMatch',16,1);
			END	
		
			ELSE
			BEGIN
				INSERT INTO dbo.ParentChildCustomFieldMap (
					numDomainID,tintParentModule,numParentFieldID,tintChildModule,numChildFieldID,bitCustomField
				) SELECT @numDomainID,@tintParentModule,@numParentFieldID,@tintChildModule,@numChildFieldID,1
			END	
		END
	END		
END
/****** Object:  StoredProcedure [USP_ManageReturnHeaderItems]    ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ManageReturnHeaderItems' ) 
    DROP PROCEDURE USP_ManageReturnHeaderItems
GO
CREATE PROCEDURE [USP_ManageReturnHeaderItems]
    (
      @numReturnHeaderID NUMERIC(18, 0) OUTPUT,
      @vcRMA VARCHAR(100),
      @vcBizDocName VARCHAR(100),
      @numDomainId NUMERIC(18, 0),
      @numUserCntID AS NUMERIC(9) = 0,
      @numDivisionId NUMERIC(18, 0),
      @numContactId NUMERIC(18, 0),
      @numOppId NUMERIC(18, 0),
      @tintReturnType TINYINT,
      @numReturnReason NUMERIC(18, 0),
      @numReturnStatus NUMERIC(18, 0),
      @monAmount MONEY,
      @monTotalTax MONEY,
      @monTotalDiscount MONEY,
      @tintReceiveType TINYINT,
      @vcComments TEXT,
      @strItems TEXT = '',
      @tintMode TINYINT = 0,
      @numBillAddressId NUMERIC(18, 0),
      @numShipAddressId NUMERIC(18, 0),
      @numDepositIDRef NUMERIC(18,0),
      @numBillPaymentIDRef NUMERIC(18,0),
	  @numParentID NUMERIC(18,0) = 0,
	  @numReferenceSalesOrder NUMERIC(18,0) = 0
    )
AS 
BEGIN 
    DECLARE @hDocItem INT                                                                                                                                                                
 
    IF @numReturnHeaderID = 0 
    BEGIN             
		-- GET ACCOUNT CLASS IF ENABLED
		DECLARE @numAccountClass AS NUMERIC(18) = 0

		DECLARE @tintDefaultClassType AS INT = 0
		SELECT @tintDefaultClassType = ISNULL(tintDefaultClassType,0) FROM Domain WHERE numDomainId = @numDomainID 

		IF @tintDefaultClassType = 1 --USER
		BEGIN
			SELECT 
				@numAccountClass=ISNULL(numDefaultClass,0) 
			FROM 
				dbo.UserMaster UM 
			JOIN 
				dbo.Domain D 
			ON 
				UM.numDomainID=D.numDomainId
			WHERE 
				D.numDomainId=@numDomainId 
				AND UM.numUserDetailId=@numUserCntID
		END
		ELSE IF @tintDefaultClassType = 2 --COMPANY
		BEGIN
			SELECT 
				@numAccountClass=ISNULL(numAccountClassID,0) 
			FROM 
				dbo.DivisionMaster DM 
			WHERE 
				DM.numDomainId=@numDomainId 
				AND DM.numDivisionID=@numDivisionID
		END
		ELSE
		BEGIN
			SET @numAccountClass = 0
		END
                   
        INSERT  INTO [ReturnHeader]
		(
			[vcRMA],[vcBizDocName],[numDomainId],[numDivisionId],[numContactId],
            [numOppId],[tintReturnType],[numReturnReason],[numReturnStatus],[monAmount],
            [monTotalTax],[monTotalDiscount],[tintReceiveType],[vcComments],
            [numCreatedBy],[dtCreatedDate],monBizDocAmount,monBizDocUsedAmount,numDepositIDRef,
			numBillPaymentIDRef,numAccountClass,numParentID,IsUnappliedPayment,numReferenceSalesOrder
		)
        SELECT  
			@vcRMA,@vcBizDocName,@numDomainId,@numDivisionId,@numContactId,@numOppId,
            @tintReturnType,@numReturnReason,@numReturnStatus,@monAmount,@monTotalTax,
			@monTotalDiscount,@tintReceiveType,@vcComments,@numUserCntID,GETUTCDATE(),0,0,
			@numDepositIDRef,@numBillPaymentIDRef,@numAccountClass,@numParentID,
			(CASE WHEN (SELECT [DM].[tintDepositePage] FROM [dbo].[DepositMaster] AS DM 
						WHERE DM.[numDepositId] = @numDepositIDRef AND [DM].[numDomainId] = @numDomainId) = 2 THEN 1
					ELSE 0
			END),@numReferenceSalesOrder
                    
        SET @numReturnHeaderID = SCOPE_IDENTITY()                                        
        SELECT  @numReturnHeaderID

		--Update DepositMaster if Refund UnApplied Payment
		IF @numDepositIDRef>0 AND @tintReturnType=4
		BEGIN
			UPDATE dbo.DepositMaster SET monRefundAmount=@monAmount WHERE numDepositId=@numDepositIDRef
		END
						
		--Update BillPaymentHeader if Refund UnApplied Payment
		IF @numBillPaymentIDRef>0 AND @tintReturnType=4
		BEGIN
			UPDATE dbo.BillPaymentHeader SET monRefundAmount=@monAmount WHERE numBillPaymentID=@numBillPaymentIDRef
		END
						
        DECLARE @tintType TINYINT 
        SET @tintType=CASE When @tintReturnType=1 OR @tintReturnType=2 THEN 7 
								WHEN @tintReturnType=3 THEN 6
								WHEN @tintReturnType=4 THEN 5 END
												
        EXEC dbo.USP_UpdateBizDocNameTemplate
				@tintType =  @tintType, --  tinyint
				@numDomainID = @numDomainID, --  numeric(18, 0)
				@RecordID = @numReturnHeaderID --  numeric(18, 0)

        DECLARE @numRMATempID AS NUMERIC(18, 0)
        DECLARE @numBizdocTempID AS NUMERIC(18, 0)

        IF @tintReturnType=1 
        BEGIN
			SELECT TOP 1 @numRMATempID = numBizDocTempID FROM BizDocTemplate
			WHERE   numOppType = 1 AND bitDefault = 1 AND bitEnabled = 1
                AND numDomainID = @numDomainID AND numBizDocID IN ( SELECT TOP 1 numListItemID 
						FROM   dbo.ListDetails WHERE  vcData = 'RMA' AND constFlag = 1 )							
		END
		ELSE IF @tintReturnType=2
        BEGIN
			SELECT TOP 1 @numRMATempID = numBizDocTempID FROM BizDocTemplate
			WHERE   numOppType = 2 AND bitDefault = 1 AND bitEnabled = 1
                AND numDomainID = @numDomainID AND numBizDocID IN ( SELECT TOP 1 numListItemID 
						FROM   dbo.ListDetails WHERE  vcData = 'RMA' AND constFlag = 1 )	
		END			    
        ELSE IF @tintReturnType=3
        BEGIN
			SELECT TOP 1 @numRMATempID = numBizDocTempID FROM    BizDocTemplate
			WHERE   numOppType = 1 AND bitDefault = 1 AND bitEnabled = 1
                AND numDomainID = @numDomainID AND numBizDocID IN ( SELECT TOP 1 numListItemID 
                FROM   dbo.ListDetails WHERE  vcData = 'Credit Memo' AND constFlag = 1 )							
		END
		ELSE IF @tintReturnType=4
        BEGIN
			SELECT TOP 1 @numRMATempID = numBizDocTempID FROM    BizDocTemplate
			WHERE   numOppType = 1 AND bitDefault = 1 AND bitEnabled = 1
                AND numDomainID = @numDomainID AND numBizDocID IN ( SELECT TOP 1 numListItemID 
                FROM   dbo.ListDetails WHERE  vcData = 'Refund Receipt' AND constFlag = 1 )							
                                
            SET @numBizdocTempID=@numRMATempID    
		END
                                               
        UPDATE  dbo.ReturnHeader
        SET     numRMATempID = ISNULL(@numRMATempID,0),
                numBizdocTempID = ISNULL(@numBizdocTempID,0)
        WHERE   numReturnHeaderID = @numReturnHeaderID
			                
		--Add/Update Address Details
        SET @numOppID = NULLIF(@numOppID, 0)
		            
        DECLARE @vcStreet VARCHAR(100),
            @vcCity VARCHAR(50),
            @vcPostalCode VARCHAR(15),
            @vcCompanyName VARCHAR(100),
            @vcAddressName VARCHAR(50)      
        DECLARE @numState NUMERIC(9),
            @numCountry NUMERIC(9),
            @numCompanyId NUMERIC(9) 
        DECLARE @bitIsPrimary BIT ;

        SELECT  @vcCompanyName = vcCompanyName,
                @numCompanyId = div.numCompanyID
        FROM    CompanyInfo Com
                JOIN divisionMaster Div ON div.numCompanyID = com.numCompanyID
        WHERE   div.numdivisionID = @numDivisionId

		--Bill Address
        IF @numBillAddressId > 0 
        BEGIN
            SELECT  @vcStreet = ISNULL(vcStreet, ''),
                    @vcCity = ISNULL(vcCity, ''),
                    @vcPostalCode = ISNULL(vcPostalCode, ''),
                    @numState = ISNULL(numState, 0),
                    @numCountry = ISNULL(numCountry, 0),
                    @bitIsPrimary = bitIsPrimary,
                    @vcAddressName = vcAddressName
            FROM    dbo.AddressDetails
            WHERE   numDomainID = @numDomainID
                    AND numAddressID = @numBillAddressId

            EXEC dbo.USP_UpdateOppAddress @numOppID = @numOppID, --  numeric(9, 0)
                @byteMode = 0, --  tinyint
                @vcStreet = @vcStreet, --  varchar(100)
                @vcCity = @vcCity, --  varchar(50)
                @vcPostalCode = @vcPostalCode, --  varchar(15)
                @numState = @numState, --  numeric(9, 0)
                @numCountry = @numCountry, --  numeric(9, 0)
                @vcCompanyName = @vcCompanyName, --  varchar(100)
                @numCompanyId = 0, --  numeric(9, 0)
                @vcAddressName = @vcAddressName,
                @numReturnHeaderID = @numReturnHeaderID,
				@bitCalledFromProcedure=1
        END
    
		--Ship Address
        IF @numShipAddressId > 0 
        BEGIN
            SELECT  @vcStreet = ISNULL(vcStreet, ''),
                    @vcCity = ISNULL(vcCity, ''),
                    @vcPostalCode = ISNULL(vcPostalCode, ''),
                    @numState = ISNULL(numState, 0),
                    @numCountry = ISNULL(numCountry, 0),
                    @bitIsPrimary = bitIsPrimary,
                    @vcAddressName = vcAddressName
            FROM    dbo.AddressDetails
            WHERE   numDomainID = @numDomainID
                    AND numAddressID = @numShipAddressId
 
            EXEC dbo.USP_UpdateOppAddress @numOppID = @numOppID, --  numeric(9, 0)
                @byteMode = 1, --  tinyint
                @vcStreet = @vcStreet, --  varchar(100)
                @vcCity = @vcCity, --  varchar(50)
                @vcPostalCode = @vcPostalCode, --  varchar(15)
                @numState = @numState, --  numeric(9, 0)
                @numCountry = @numCountry, --  numeric(9, 0)
                @vcCompanyName = @vcCompanyName, --  varchar(100)
                @numCompanyId = @numCompanyId, --  numeric(9, 0)
                @vcAddressName = @vcAddressName,
                @numReturnHeaderID = @numReturnHeaderID,
				@bitCalledFromProcedure=1
        END
           
		   
		-- IMPORTANT: KEEP THIS BELOW ADDRESS UPDATE LOGIC 
		IF @tintReturnType=1 OR (@tintReturnType=2 AND (SELECT ISNULL(bitPurchaseTaxCredit,0) FROM Domain WHERE numDomainId=@numDomainId) = 1) 
		BEGIN    
			IF @numOppId > 0
			BEGIN
				INSERT dbo.OpportunityMasterTaxItems 
				(
					numReturnHeaderID,
					numTaxItemID,
					fltPercentage,
					tintTaxType,
					numTaxID
				) 
				SELECT 
					@numReturnHeaderID,
					numTaxItemID,
					fltPercentage,
					tintTaxType,
					numTaxID
				FROM 
					OpportunityMasterTaxItems 
				WHERE 
					numOppID=@numOppID
			END
			ELSE
			BEGIN 
				--Insert Tax for Division                       
				INSERT dbo.OpportunityMasterTaxItems (
					numReturnHeaderID,
					numTaxItemID,
					fltPercentage,
					tintTaxType,
					numTaxID
				) 
				SELECT 
					@numReturnHeaderID,
					TI.numTaxItemID,
					TEMPTax.decTaxValue,
					TEMPTax.tintTaxType,
					0
				FROM 
					TaxItems TI 
				JOIN 
					DivisionTaxTypes DTT 
				ON 
					TI.numTaxItemID = DTT.numTaxItemID
				CROSS APPLY
				(
					SELECT
						decTaxValue,
						tintTaxType,
						numTaxID
					FROM
						dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,TI.numTaxItemID,NULL,0,@numReturnHeaderID)   
				) AS TEMPTax
				WHERE 
					DTT.numDivisionID=@numDivisionId AND DTT.bitApplicable=1
				UNION 
				SELECT 
					@numReturnHeaderID,
					0,
					TEMPTax.decTaxValue,
					TEMPTax.tintTaxType,
					0
				FROM 
					dbo.DivisionMaster 
				CROSS APPLY
				(
					SELECT
						decTaxValue,
						tintTaxType,
						numTaxID
					FROM
						dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,0,NULL,1,@numReturnHeaderID)  
				) AS TEMPTax
				WHERE 
					bitNoTax=0 
					AND numDivisionID=@numDivisionID
				UNION 
				SELECT
					@numReturnHeaderID
					,1
					,decTaxValue
					,tintTaxType
					,numTaxID
				FROM
					dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,1,NULL,1,@numReturnHeaderID)	
			END			  
		END
          
        IF CONVERT(VARCHAR(10), @strItems) <> '' 
        BEGIN
            EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems
			                                                                                                        
            SELECT  
				*
            INTO 
				#temp
            FROM 
				OPENXML (@hDocItem, '/NewDataSet/Item',2) 
			WITH 
			( 
				numReturnItemID NUMERIC, numItemCode NUMERIC, numUnitHour FLOAT, numUnitHourReceived FLOAT, monPrice DECIMAL(30,16), 
				monTotAmount MONEY, vcItemDesc VARCHAR(1000), numWareHouseItemID NUMERIC, vcModelID VARCHAR(200), 
				vcManufacturer VARCHAR(250), numUOMId NUMERIC,numOppItemID NUMERIC, bitDiscountType BIT, fltDiscount FLOAT
			)

            EXEC sp_xml_removedocument @hDocItem 
                
            INSERT  INTO [ReturnItems]
			(
				[numReturnHeaderID],
				[numItemCode],
				[numUnitHour],
				[numUnitHourReceived],
				[monPrice],
				[monTotAmount],
				[vcItemDesc],
				[numWareHouseItemID],
				[vcModelID],
				[vcManufacturer],
				[numUOMId],numOppItemID,
				bitDiscountType,fltDiscount
			)
			SELECT  
				@numReturnHeaderID,
				numItemCode,
				numUnitHour,
				0,
				monPrice,
				monTotAmount,
				vcItemDesc,
				NULLIF(numWareHouseItemID, 0) numWareHouseItemID,
				vcModelID,
				vcManufacturer,
				numUOMId,numOppItemID,
				bitDiscountType,
				fltDiscount
			FROM 
				#temp
			WHERE 
				numReturnItemID = 0

			UPDATE
				ReturnItems
			SET
				monAverageCost = (CASE 
									WHEN @numOppId > 0 
									THEN ISNULL((SELECT monAvgCost FROM OpportunityItems WHERE numOppId=@numOppId AND numOppItemID=ReturnItems.numOppItemID),0)
									ELSE
										ISNULL((SELECT monAverageCost FROM Item WHERE numItemCode=ReturnItems.numItemCode),0)
								END),
				monVendorCost = (CASE 
									WHEN @numOppId > 0 
									THEN ISNULL((SELECT monVendorCost FROM OpportunityItems WHERE numOppId=@numOppId AND numOppItemID=ReturnItems.numOppItemID),0)
									ELSE
										ISNULL((SELECT Vendor.monCost FROM Item INNER JOIN Vendor ON Item.numVendorID=Vendor.numVendorID AND Vendor.numItemCode=Item.numItemCode WHERE Item.numItemCode=ReturnItems.numItemCode),0)
								END)
			WHERE
				numReturnHeaderID=@numReturnHeaderID
					 
            DROP TABLE #temp
   
			IF @tintReturnType=1 OR (@tintReturnType=2 AND (SELECT ISNULL(bitPurchaseTaxCredit,0) FROM Domain WHERE numDomainId=@numDomainId) = 1) 
			BEGIN               
				IF @numOppId>0
				BEGIN
					INSERT INTO dbo.OpportunityItemsTaxItems 
					(
						numReturnHeaderID,numReturnItemID,numTaxItemID,numTaxID
					)  
					SELECT 
						@numReturnHeaderID,OI.numReturnItemID,IT.numTaxItemID,IT.numTaxID
					FROM 
						dbo.ReturnItems OI 
					JOIN 
						dbo.OpportunityItemsTaxItems IT 
					ON 
						OI.numOppItemID=IT.numOppItemID 
					WHERE 
						OI.numReturnHeaderID=@numReturnHeaderID 
						AND IT.numOppId=@numOppId
				END
				ELSE
				BEGIN
					--Insert Tax for ReturnItems
					INSERT INTO dbo.OpportunityItemsTaxItems 
					(
						numReturnHeaderID,
						numReturnItemID,
						numTaxItemID,
						numTaxID
					) 
					SELECT 
						@numReturnHeaderID,
						OI.numReturnItemID,
						IT.numTaxItemID,
						(CASE WHEN IT.numTaxItemID = 1 THEN numTaxID ELSE 0 END)
					FROM 
						dbo.ReturnItems OI 
					JOIN 
						dbo.ItemTax IT 
					ON 
						OI.numItemCode=IT.numItemCode 
					WHERE 
						OI.numReturnHeaderID=@numReturnHeaderID 
						AND IT.bitApplicable=1  
				END
			END
        END 
            
            -- Update leads,prospects to Accounts
		EXEC dbo.USP_UpdateCRMTypeToAccounts @numDivisionID ,@numDomainID ,@numUserCntID
                          
    END    
    ELSE IF @numReturnHeaderID <> 0 
    BEGIN    
        IF ISNULL(@tintMode, 0) = 0
        BEGIN
			UPDATE
				[ReturnHeader]
			SET 
				[vcRMA] = @vcRMA,
				[numReturnReason] = @numReturnReason,
				[numReturnStatus] = @numReturnStatus,
				[monAmount] = ISNULL(monAmount,0) + ISNULL(monTotalDiscount,0) - ISNULL(@monTotalDiscount,0),
				[monTotalDiscount] = @monTotalDiscount,
				[tintReceiveType] = @tintReceiveType,
				[vcComments] = @vcComments,
				[numModifiedBy] = @numUserCntID,
				[dtModifiedDate] = GETUTCDATE()
			WHERE 
				[numDomainId] = @numDomainId
				AND [numReturnHeaderID] = @numReturnHeaderID
		END                 
        ELSE IF ISNULL(@tintMode, 0) = 1 
        BEGIN
            UPDATE  
				ReturnHeader
			SET 
				numReturnStatus = @numReturnStatus
			WHERE 
				numReturnHeaderID = @numReturnHeaderID
							
			IF CONVERT(VARCHAR(10), @strItems) <> '' 
			BEGIN
				EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems
				                                                                                                    
				SELECT 
					*
				INTO 
					#temp1
				FROM OPENXML 
					(@hDocItem, '/NewDataSet/Item',2) 
				WITH 
				( 
					numReturnItemID NUMERIC, 
					numUnitHourReceived FLOAT, 
					numWareHouseItemID NUMERIC
				)
				
				EXEC sp_xml_removedocument @hDocItem 

				UPDATE  
					[ReturnItems]
				SET 
					[numUnitHourReceived] = X.numUnitHourReceived,
					[numWareHouseItemID] =CASE WHEN X.numWareHouseItemID=0 THEN NULL ELSE X.numWareHouseItemID END 
				FROM 
					#temp1 AS X
				WHERE 
					X.numReturnItemID = ReturnItems.numReturnItemID
								AND ReturnItems.numReturnHeaderID = @numReturnHeaderID
				
				DROP TABLE #temp1
			END 
		END
    END            
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_MirrorOPPBizDocItems' ) 
    DROP PROCEDURE USP_MirrorOPPBizDocItems
GO
CREATE PROCEDURE [dbo].[USP_MirrorOPPBizDocItems]
    (
      @numOppId AS NUMERIC(9) = NULL,
      @numDomainID AS NUMERIC(9) = 0      
    )
AS 
    DECLARE @DivisionID AS NUMERIC(9)      
    DECLARE @tintTaxOperator AS TINYINT   
    DECLARE @tintType AS TINYINT   
    DECLARE @tintOppStatus AS TINYINT   
    
    DECLARE @tintOppType AS TINYINT   
    DECLARE @numBizDocTempID AS NUMERIC(9) ;
      SET @numBizDocTempID = 0
                                                                                                                                                                                                                                                                                                                                          
    SELECT  @DivisionID = numDivisionID, @tintTaxOperator = [tintTaxOperator]  ,
    @tintType = tintOppType,@tintOppStatus=tintOppStatus,
            @tintOppType = tintOppType   ,@numBizDocTempID = ISNULL(numOppBizDocTempID, 0)      
    FROM    OpportunityMaster
    WHERE   numOppId = @numOppId    
                                                                                                                                                                          
--Decimal points to show in bizdoc 
    DECLARE @DecimalPoint AS TINYINT
    SELECT  @DecimalPoint = tintDecimalPoints
    FROM    domain
    WHERE   numDomainID = @numDomainID
                                                                                          
    DECLARE @strSQL AS VARCHAR(MAX)                                                                                          
    DECLARE @strSQL1 AS VARCHAR(8000)                                       
    DECLARE @strSQLCusFields VARCHAR(1000)                      
    DECLARE @strSQLEmpFlds VARCHAR(500)                                                                         
    SET @strSQLCusFields = ''                                                                                          
    SET @strSQLEmpFlds = ''                                                                                          
                                                   
    DECLARE @intRowNum AS INT                                                  
    DECLARE @numFldID AS VARCHAR(15)                                          
    DECLARE @vcFldname AS VARCHAR(50)                                            
    DECLARE @numBizDocId AS NUMERIC(9)
                                          
    
       IF @tintOppType=1 AND @tintOppStatus=0
			SELECT  @numBizDocID = numListItemID FROM dbo.ListDetails WHERE numListID=27 AND vcData='Sales Opportunity' AND constFlag=1
       ELSE IF @tintOppType=1 AND @tintOppStatus=1
			SELECT  @numBizDocID = numListItemID FROM dbo.ListDetails WHERE numListID=27 AND vcData='Sales Order' AND constFlag=1
       ELSE IF @tintOppType=2 AND @tintOppStatus=0
			SELECT  @numBizDocID = numListItemID FROM dbo.ListDetails WHERE numListID=27 AND vcData='Purchase Opportunity' AND constFlag=1
       ELSE IF @tintOppType=2 AND @tintOppStatus=1
	 		SELECT  @numBizDocID = numListItemID FROM dbo.ListDetails WHERE numListID=27 AND vcData='Purchase Order' AND constFlag=1
                                                                              
    IF @tintType = 1 
        SET @tintType = 7
    ELSE 
        SET @tintType = 8                                                                                      
                                                                                
    SELECT  *
    INTO    #Temp1
    FROM    ( SELECT   Opp.vcitemname AS vcItemName,
                        0 OppBizDocItemID,
                        CASE WHEN charitemType = 'P' THEN 'Product'
                             WHEN charitemType = 'S' THEN 'Service'
                        END AS charitemType,
                        CASE WHEN bitShowDeptItemDesc = 0
                             THEN CONVERT(VARCHAR(500), Opp.vcitemDesc)
                             WHEN bitShowDeptItemDesc = 1
                                  AND bitKitParent = 1
                                  AND bitAssembly = 0
                             THEN dbo.fn_PopulateKitDesc(opp.numOppId,
                                                         opp.numoppitemtCode)
                             WHEN bitShowDeptItemDesc = 1
                                  AND bitKitParent = 1
                                  AND bitAssembly = 1
                             THEN dbo.fn_PopulateAssemblyDesc(opp.numoppitemtCode, Opp.numUnitHour)
                             ELSE CONVERT(VARCHAR(500), Opp.vcitemDesc)
                        END AS txtItemDesc,                                      
--vcitemdesc as [desc],                                      
                        dbo.fn_UOMConversion(ISNULL(i.numBaseUnit, 0),
                                             i.numItemCode, @numDomainID,
                                             ISNULL(opp.numUOMId, 0))
                        * Opp.numUnitHour AS numUnitHour,
                        CONVERT(VARCHAR, CONVERT(DECIMAL(18, 4), dbo.fn_UOMConversion(ISNULL(opp.numUOMId, 0), i.numItemCode, @numDomainID, ISNULL(i.numBaseUnit, 0))
                        * Opp.monPrice)) monPrice,
                        CONVERT(VARCHAR, CONVERT(DECIMAL(18, 4), Opp.monTotAmount)) Amount,
                        Opp.monTotAmount/*Fo calculating sum*/,
                        ISNULL(CONVERT(VARCHAR, i.monListPrice), 0) AS monListPrice,
                        i.numItemCode AS ItemCode,i.numItemCode,
                        opp.numoppitemtCode,
                        L.vcdata AS numItemClassification,
                        CASE WHEN bitTaxable = 0 THEN 'No'
                             ELSE 'Yes'
                        END AS Taxable,
                        numIncomeChartAcntId AS itemIncomeAccount,
                        'NI' AS ItemType,
                        0 AS numJournalId,
                        0 AS numTransactionId,
                        ISNULL(Opp.vcModelID, '') vcModelID,
                        dbo.USP_GetAttributes(Opp.numWarehouseItmsID,
                                              bitSerialized) AS vcAttributes,
                        ISNULL(vcPartNo, '') AS vcPartNo,
                        ( ISNULL(Opp.monTotAmtBefDiscount, Opp.monTotAmount)
                          - Opp.monTotAmount ) AS DiscAmt,
						  (CASE WHEN opp.numUnitHour = 0 THEN 0 ELSE CONVERT(DECIMAL(18, 4),(dbo.fn_UOMConversion(ISNULL(opp.numUOMId, 0), i.numItemCode, @numDomainID, ISNULL(i.numBaseUnit, 0)) * (ISNULL(opp.monTotAmount,0) / opp.numUnitHour))) END) As monUnitSalePrice,
                        '' vcNotes,
                        '' vcTrackingNo,
                        '' vcShippingMethod,
                        0 monShipCost,
                        NULL dtDeliveryDate,
                        Opp.numWarehouseItmsID,
--ISNULL(I.vcUnitofMeasure,'Units') vcUnitofMeasure,
                        ISNULL(u.vcUnitName, '') numUOMId,
                        ISNULL(Opp.vcManufacturer, '') AS vcManufacturer,
                        ISNULL(V.monCost, 0) AS VendorCost,
                        Opp.vcPathForTImage,
                        i.[fltLength],
                        i.[fltWidth],
                        i.[fltHeight],
                        i.[fltWeight],
                        ISNULL(I.numVendorID, 0) numVendorID,
                        Opp.bitDropShip AS DropShip,
                        SUBSTRING(( SELECT  ' ,' + vcSerialNo
                                            + CASE WHEN ISNULL(I.bitLotNo, 0) = 1
                                                   THEN ' ('
                                                        + CONVERT(VARCHAR(15), oppI.numQty)
                                                        + ')'
                                                   ELSE ''
                                              END
                                    FROM    OppWarehouseSerializedItem oppI
                                            JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID = whi.numWareHouseItmsDTLID
                                    WHERE   oppI.numOppID = Opp.numOppId
                                            AND oppI.numOppItemID = Opp.numoppitemtCode
                                    ORDER BY vcSerialNo
                                  FOR
                                    XML PATH('')
                                  ), 3, 200000) AS SerialLotNo,
                        ISNULL(opp.numUOMId, 0) AS numUOM,
                        ISNULL(u.vcUnitName, '') vcUOMName,
                        dbo.fn_UOMConversion(opp.numUOMId, Opp.numItemCode,
                                             @numDomainID, NULL) AS UOMConversionFactor,
                        ISNULL(Opp.numSOVendorId, 0) AS numSOVendorId,
                        NULL dtRentalStartDate,
                        NULL dtRentalReturnDate,
                        ISNULL(W.vcWareHouse, '') AS Warehouse,
                        CASE WHEN charitemType = 'P' THEN WI.vcWHSKU
                             ELSE i.vcSKU
                        END vcSKU,
                        CASE WHEN charitemType = 'P' THEN WI.vcBarCode
                             ELSE i.numBarCodeId
                        END vcBarcode,
                        ISNULL(opp.numClassID, 0) AS numClassID,
                        ISNULL(opp.numProjectID, 0) AS numProjectID,
                        i.numShipClass,
						(CASE WHEN opp.numUnitHour > ISNULL(WI.numAllocation,0) THEN (opp.numUnitHour - ISNULL(WI.numAllocation,0)) * dbo.fn_UOMConversion(ISNULL(i.numBaseUnit, 0),i.numItemCode, @numDomainID,ISNULL(opp.numUOMId, 0))  ELSE 0 END) AS numBackOrder,
						ISNULL(opp.vcPromotionDetail,'') AS vcPromotionDetail,
						(CASE WHEN ISNULL(WI.numWareHouseItemID,0) > 0 THEN CONCAT(W.vcWareHouse,CASE WHEN WL.numWLocationID > 0 THEN CONCAT('(',WL.vcLocation ,')') ELSE '' END) ELSE '' END) vcWarehouse,
						opp.numSortOrder
              FROM      OpportunityItems opp
                        LEFT JOIN item i ON opp.numItemCode = i.numItemCode
                        LEFT JOIN ListDetails L ON i.numItemClassification = L.numListItemID
                        LEFT JOIN Vendor V ON V.numVendorID = @DivisionID
                                              AND V.numItemCode = opp.numItemCode
                        LEFT JOIN UOM u ON u.numUOMId = opp.numUOMId                                                                          
                        LEFT JOIN dbo.WareHouseItems WI ON opp.numWarehouseItmsID = WI.numWareHouseItemID
                                                           AND WI.numDomainID = @numDomainID
                        LEFT JOIN dbo.Warehouses W ON WI.numDomainID = W.numDomainID
                                                      AND WI.numWareHouseID = W.numWareHouseID
						LEFT JOIN 
							dbo.WarehouseLocation WL 
						ON 
							WL.numWLocationID = WI.numWLocationID
              WHERE     opp.numOppId = @numOppId
            ) X


    IF @DecimalPoint = 1 
        BEGIN
            UPDATE  #Temp1
            SET     monPrice = CONVERT(DECIMAL(18, 1), monPrice),
                    Amount = CONVERT(DECIMAL(18, 1), Amount)
        END
    IF @DecimalPoint = 2 
        BEGIN
            UPDATE  #Temp1
            SET     monPrice = CONVERT(DECIMAL(18, 2), monPrice),
                    Amount = CONVERT(DECIMAL(18, 2), Amount)
        END
    IF @DecimalPoint = 3 
        BEGIN
            UPDATE  #Temp1
            SET     monPrice = CONVERT(DECIMAL(18, 3), monPrice),
                    Amount = CONVERT(DECIMAL(18, 3), Amount)
        END
    IF @DecimalPoint = 4 
        BEGIN
            UPDATE  #Temp1
            SET     monPrice = CONVERT(DECIMAL(18, 4), monPrice),
                    Amount = CONVERT(DECIMAL(18, 4), Amount)
        END

    DECLARE @strSQLUpdate AS VARCHAR(2000);SET @strSQLUpdate=''
   
    EXEC ('ALTER TABLE #Temp1 ADD [TotalTax] MONEY')
	EXEC ('ALTER TABLE #Temp1 ADD [CRV] MONEY')

    IF @tintOppType = 1 AND @tintTaxOperator = 1 --add Sales tax
	BEGIN
        SET @strSQLUpdate = @strSQLUpdate
            + ',[TotalTax]= dbo.fn_CalBizDocTaxAmt('
            + CONVERT(VARCHAR(20), @numDomainID) + ',0,'
            + CONVERT(VARCHAR(20), @numOppId) +','+
            + 'numoppitemtCode,1,Amount,numUnitHour)'

		SET @strSQLUpdate = @strSQLUpdate
            + ',[CRV]= dbo.fn_CalBizDocTaxAmt('
            + CONVERT(VARCHAR(20), @numDomainID) + ',1,'
            + CONVERT(VARCHAR(20), @numOppId) +','+
            + 'numoppitemtCode,1,Amount,numUnitHour)'
	END
    ELSE IF @tintOppType = 1 AND @tintTaxOperator = 2 -- remove sales tax
	BEGIN
        SET @strSQLUpdate = @strSQLUpdate + ',[TotalTax]= 0'
		SET @strSQLUpdate = @strSQLUpdate + ',[CRV]= 0'
	END
    ELSE IF @tintOppType = 1
	BEGIN
        SET @strSQLUpdate = @strSQLUpdate
            + ',[TotalTax]= dbo.fn_CalBizDocTaxAmt('
            + CONVERT(VARCHAR(20), @numDomainID) + ',0,'
            + CONVERT(VARCHAR(20), @numOppId) +','+
            + 'numoppitemtCode,1,Amount,numUnitHour)'

		SET @strSQLUpdate = @strSQLUpdate
            + ',[CRV]= dbo.fn_CalBizDocTaxAmt('
            + CONVERT(VARCHAR(20), @numDomainID) + ',1,'
            + CONVERT(VARCHAR(20), @numOppId) +','+
            + 'numoppitemtCode,1,Amount,numUnitHour)'
	END
    ELSE
	BEGIN 
        SET @strSQLUpdate = @strSQLUpdate + ',[TotalTax]= 0'
		SET @strSQLUpdate = @strSQLUpdate + ',[CRV]= 0'
	END

	DECLARE @vcTaxName AS VARCHAR(100)
    DECLARE @numTaxItemID AS NUMERIC(9)
    SET @numTaxItemID = 0
    SELECT TOP 1
            @vcTaxName = vcTaxName,
            @numTaxItemID = numTaxItemID
    FROM    TaxItems
    WHERE   numDomainID = @numDomainID


    WHILE @numTaxItemID > 0
        BEGIN

            EXEC ( 'alter table #Temp1 add [' + @vcTaxName + '] money'
                )

            IF @tintOppType = 1 
                SET @strSQLUpdate = @strSQLUpdate + ',[' + @vcTaxName
                    + ']= dbo.fn_CalBizDocTaxAmt('
                    + CONVERT(VARCHAR(20), @numDomainID) + ','
                    + CONVERT(VARCHAR(20), @numTaxItemID) + ','
                    + CONVERT(VARCHAR(20), @numOppId) +','+
                    + 'numoppitemtCode,1,Amount,numUnitHour)'
            ELSE 
                SET @strSQLUpdate = @strSQLUpdate + ',[' + @vcTaxName + ']= 0'

            SELECT TOP 1
                    @vcTaxName = vcTaxName,
                    @numTaxItemID = numTaxItemID
            FROM    TaxItems
            WHERE   numDomainID = @numDomainID
                    AND numTaxItemID > @numTaxItemID

            IF @@rowcount = 0 
                SET @numTaxItemID = 0


        END

    IF @strSQLUpdate <> '' 
        BEGIN
            SET @strSQLUpdate = 'update #Temp1 set ItemCode=ItemCode'
                + @strSQLUpdate + ' where ItemCode>0'
            PRINT @strSQLUpdate
            EXEC ( @strSQLUpdate
                )
        END

    DECLARE @vcDbColumnName NVARCHAR(50)
    SELECT TOP 1
            @numFldID = numfieldID,
            @vcFldname = vcFieldName,
            @vcDbColumnName = vcDbColumnName,
            @intRowNum = ( tintRow + 1 )
    FROM    View_DynamicCustomColumns
    WHERE   numFormID = @tintType
            AND Grp_id = 5
            AND numDomainID = @numDomainID
            AND numAuthGroupID = @numBizDocId
            AND bitCustom = 1
            AND ISNULL(numRelCntType, 0) = @numBizDocTempID
    ORDER BY tintRow                                                                                          
    WHILE @intRowNum > 0                                                                                  
        BEGIN    
            EXEC
                ( 'alter table #Temp1 add [' + @vcDbColumnName
                  + '] varchar(100)'
                )
	
            SET @strSQLUpdate = 'update #Temp1 set ItemCode=ItemCode,['
                + @vcDbColumnName + ']=dbo.GetCustFldValueItem(' + @numFldID
                + ',ItemCode) where ItemCode>0'
            EXEC ( @strSQLUpdate
                )
		                   
            SELECT TOP 1
                    @numFldID = numfieldID,
                    @vcFldname = vcFieldName,
                    @vcDbColumnName = vcDbColumnName,
                    @intRowNum = ( tintRow + 1 )
            FROM    View_DynamicCustomColumns
            WHERE   numFormID = @tintType
                    AND Grp_id = 5
                    AND numDomainID = @numDomainID
                    AND numAuthGroupID = @numBizDocId
                    AND bitCustom = 1
                    AND tintRow >= @intRowNum
                    AND ISNULL(numRelCntType, 0) = @numBizDocTempID
            ORDER BY tintRow                                                                  
		           
            IF @@rowcount = 0 
                SET @intRowNum = 0                                                                                          
        END


    SELECT  Row_NUmber() OVER ( ORDER BY numoppitemtCode ) RowNumber,
            *
    FROM    #Temp1
    ORDER BY numSortOrder ASC 

    DROP TABLE #Temp1

--    SELECT  ISNULL(tintOppStatus, 0)
--    FROM    OpportunityMaster
--    WHERE   numOppID = @numOppId                                                                                                 

IF (SELECT COUNT(*) FROM View_DynamicColumns WHERE   numFormID = @tintType AND numDomainID = @numDomainID
            AND numAuthGroupID = @numBizDocId AND vcFieldType = 'R'AND ISNULL(numRelCntType, 0) = @numBizDocTempID)=0
BEGIN            
	INSERT INTO DycFormConfigurationDetails (numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numAuthGroupID,numRelCntType)
	select numFormId,numFieldId,1,tintRow,@numDomainID,@numBizDocId,@numBizDocTempID
	 FROM View_DynamicDefaultColumns
	 where numFormId=@tintType AND bitDefault=1 AND numDomainID=@numDomainID AND vcLookBackTableName = (CASE @numBizDocId WHEN 40911 THEN 'CSGrid' ELSE REPLACE(vcLookBackTableName,'CSGrid','') END) order by tintRow ASC	
	 
	 
	  IF(SELECT COUNT(*) FROM DycFrmConfigBizDocsSumm Ds where numBizDocID=@numBizDocId and Ds.numFormID=@tintType 
	and isnull(Ds.numBizDocTempID,0)=@numBizDocTempID and Ds.numDomainID=@numDomainID)=0
	BEGIN
		INSERT INTO DycFrmConfigBizDocsSumm (numFormID,numBizDocID,numFieldID,numDomainID,numBizDocTempID)
		 SELECT @tintType,@numBizDocId,DFM.numFieldID,@numDomainID,@numBizDocTempID FROM
		 DycFormField_Mapping DFFM 
			JOIN DycFieldMaster DFM on DFFM.numModuleID=DFFM.numModuleID and DFFM.numFieldID=DFM.numFieldID
			where numFormID=16 and DFFM.numDomainID is null
	END
	
END      

    SELECT  vcFieldName AS vcFormFieldName,
            vcDbColumnName,
            vcFieldDataType,
            tintRow AS intRowNum
    FROM    View_DynamicColumns
    WHERE   numFormID = @tintType
            AND numDomainID = @numDomainID
            AND numAuthGroupID = @numBizDocId
            AND vcFieldType = 'R'
            AND ISNULL(numRelCntType, 0) = @numBizDocTempID
    UNION
    SELECT  vcFieldName AS vcFormFieldName,
            vcDbColumnName,
            'C' vcFieldDataType,
            tintRow AS intRowNum
    FROM    View_DynamicCustomColumns
    WHERE   numFormID = @tintType
            AND Grp_id = 5
            AND numDomainID = @numDomainID
            AND numAuthGroupID = @numBizDocId
            AND bitCustom = 1
            AND ISNULL(numRelCntType, 0) = @numBizDocTempID
    ORDER BY tintRow
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_OPPBizDocItems_GetWithKitChilds' ) 
    DROP PROCEDURE USP_OPPBizDocItems_GetWithKitChilds
GO
CREATE PROCEDURE [dbo].[USP_OPPBizDocItems_GetWithKitChilds]
(
    @numOppId AS NUMERIC(9) = NULL,
    @numOppBizDocsId AS NUMERIC(9) = NULL,
    @numDomainID AS NUMERIC(9) = 0      
)
AS 
BEGIN
    DECLARE @DivisionID AS NUMERIC(9)      
    DECLARE @tintTaxOperator AS TINYINT   
                                                                                                                                                                                                                                                                                                                                          
    SELECT  
		@DivisionID = numDivisionID, 
		@tintTaxOperator = [tintTaxOperator]           
    FROM    
		OpportunityMaster
    WHERE   
		numOppId = @numOppId                                                                                                                                                                          

    
                                                                                          
    DECLARE @strSQL AS VARCHAR(MAX)                                                                                          
    DECLARE @strSQL1 AS VARCHAR(8000)                                       
    DECLARE @strSQLCusFields VARCHAR(1000) = ''
    DECLARE @strSQLEmpFlds VARCHAR(500) = ''                                                                         
                                                                                              
    DECLARE @tintType AS TINYINT   
    DECLARE @tintOppType AS TINYINT                                                      
    DECLARE @intRowNum AS INT                                                  
    DECLARE @numFldID AS VARCHAR(15)                                          
    DECLARE @vcFldname AS VARCHAR(50)                                            
    DECLARE @numBizDocId AS NUMERIC(9)
    DECLARE @numBizDocTempID AS NUMERIC(9) = 0
	DECLARE @bitDisplayKitChild AS BIT = 0
        
                                           
    SELECT  
		@numBizDocTempID = ISNULL(numBizDocTempID, 0),
        @numBizDocId = numBizDocId
    FROM    
		OpportunityBizDocs
    WHERE   
		numOppBizDocsId = @numOppBizDocsId

	
	SELECT
		@bitDisplayKitChild = ISNULL(bitDisplayKitChild,0)
	FROM	
		BizDocTemplate
	WHERE
		numDomainID = @numDomainID
		AND numBizDocTempID = @numBizDocTempID


    SELECT  
		@tintType = tintOppType,
        @tintOppType = tintOppType
    FROM    
		OpportunityMaster
    WHERE   
		numOppID = @numOppId                                                                                          
                                                                                   
    IF @tintType = 1 
        SET @tintType = 7
    ELSE 
        SET @tintType = 8     
		
		
	IF (SELECT 
			COUNT(*) 
		FROM 
			View_DynamicColumns 
		WHERE   
			numFormID = @tintType 
			AND numDomainID = @numDomainID
            AND numAuthGroupID = @numBizDocId 
			AND vcFieldType = 'R'
			AND ISNULL(numRelCntType, 0) = @numBizDocTempID
		)=0
	BEGIN            
		INSERT INTO DycFormConfigurationDetails 
		(
			numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numAuthGroupID,numRelCntType
		)
		SELECT 
			numFormId,numFieldId,1,tintRow,@numDomainID,@numBizDocId,@numBizDocTempID
		FROM 
			View_DynamicDefaultColumns
		WHERE 
			numFormId=@tintType and bitDefault=1 and numDomainID=@numDomainID order by tintRow ASC	
	 
	 
		IF (SELECT 
				COUNT(*) 
			FROM 
				DycFrmConfigBizDocsSumm Ds 
			WHERE 
				numBizDocID=@numBizDocId 
				AND Ds.numFormID=@tintType 
				AND ISNULL(Ds.numBizDocTempID,0)=@numBizDocTempID 
				AND Ds.numDomainID=@numDomainID
			)=0
		BEGIN
			INSERT INTO DycFrmConfigBizDocsSumm 
			(
				numFormID,numBizDocID,numFieldID,numDomainID,numBizDocTempID
			)
			SELECT 
				@tintType,@numBizDocId,DFM.numFieldID,@numDomainID,@numBizDocTempID 
			FROM
				DycFormField_Mapping DFFM 
			JOIN 
				DycFieldMaster DFM 
			ON 
				DFFM.numModuleID=DFFM.numModuleID and DFFM.numFieldID=DFM.numFieldID
			WHERE 
				numFormID=16 
				AND DFFM.numDomainID IS NULL
		END
	END      

	IF OBJECT_ID('tempdb..#TempBizDocProductGridColumns') IS NOT NULL DROP TABLE #TempBizDocProductGridColumns
	SELECT * INTO #TempBizDocProductGridColumns FROM (
    SELECT  
		vcFieldName AS vcFormFieldName,
        vcDbColumnName,
        vcFieldDataType,
        tintRow AS intRowNum
    FROM    
		View_DynamicColumns
    WHERE   
		numFormID = @tintType
        AND numDomainID = @numDomainID
        AND numAuthGroupID = @numBizDocId
        AND vcFieldType = 'R'
        AND ISNULL(numRelCntType, 0) = @numBizDocTempID
    UNION
    SELECT  
		vcFieldName AS vcFormFieldName,
        vcDbColumnName,
        'C' vcFieldDataType,
        tintRow AS intRowNum
    FROM    
		View_DynamicCustomColumns
    WHERE   
		numFormID = @tintType
        AND Grp_id = 5
        AND numDomainID = @numDomainID
        AND numAuthGroupID = @numBizDocId
        AND bitCustom = 1
        AND ISNULL(numRelCntType, 0) = @numBizDocTempID
    ) t1   
	ORDER BY 
		intRowNum                                                                              
                                                                                
    SELECT  
		*
    INTO    
		#Temp1
    FROM    
	( 
		SELECT   
			Opp.vcitemname AS vcItemName,
            ISNULL(OBD.numOppBizDocItemID, 0) OppBizDocItemID,
			(CASE WHEN OBD.numUnitHour > ISNULL(WI.numAllocation,0) THEN (OBD.numUnitHour - ISNULL(WI.numAllocation,0)) * dbo.fn_UOMConversion(ISNULL(i.numBaseUnit, 0),i.numItemCode, @numDomainID,ISNULL(opp.numUOMId, 0))  ELSE 0 END) AS numBackOrder,
            CASE WHEN charitemType = 'P' THEN 'Product'
                    WHEN charitemType = 'S' THEN 'Service'
            END AS charitemType,
            CASE WHEN charitemType = 'P' THEN 'Inventory'
	                WHEN charitemType = 'N' THEN 'Non-Inventory'
                    WHEN charitemType = 'S' THEN 'Service'
            END AS vcItemType,
			CONVERT(VARCHAR(2500), OBD.vcitemDesc) AS txtItemDesc, 
			opp.numUnitHour AS numOrgTotalUnitHour,
			dbo.fn_UOMConversion(ISNULL(i.numBaseUnit, 0),i.numItemCode, @numDomainID,ISNULL(opp.numUOMId, 0)) * opp.numUnitHour AS numTotalUnitHour,                                   
            dbo.fn_UOMConversion(ISNULL(i.numBaseUnit, 0),i.numItemCode, @numDomainID,ISNULL(opp.numUOMId, 0)) * OBD.numUnitHour AS numUnitHour,
            CONVERT(DECIMAL(18, 4), (dbo.fn_UOMConversion(ISNULL(opp.numUOMId, 0), i.numItemCode, @numDomainID, ISNULL(i.numBaseUnit, 0)) * OBD.monPrice)) monPrice,
            CONVERT(VARCHAR, CONVERT(DECIMAL(18, 4), OBD.monTotAmount)) Amount,
            OBD.monTotAmount/*Fo calculating sum*/,
            ISNULL(CONVERT(VARCHAR, i.monListPrice), 0) AS monListPrice,
            i.numItemCode AS ItemCode,
            opp.numoppitemtCode,
            L.vcdata AS numItemClassification,
            CASE WHEN bitTaxable = 0 THEN 'No'
                    ELSE 'Yes'
            END AS Taxable,
            numIncomeChartAcntId AS itemIncomeAccount,
            'NI' AS ItemType,
            ISNULL(GJH.numJournal_Id, 0) AS numJournalId,
            ( SELECT TOP 1
                        ISNULL(GJD.numTransactionId, 0)
                FROM      General_Journal_Details GJD
                WHERE     GJH.numJournal_Id = GJD.numJournalId
                        AND GJD.chBizDocItems = 'NI'
                        AND GJD.numoppitemtCode = opp.numoppitemtCode
            ) AS numTransactionId,
            ISNULL(Opp.vcModelID, '') vcModelID,
            (CASE WHEN charitemType = 'P' THEN dbo.USP_GetAttributes(OBD.numWarehouseItmsID, bitSerialized) ELSE ISNULL(OBD.vcAttributes,0) END) AS vcAttributes,
            ISNULL(vcPartNo, '') AS vcPartNo,
            (ISNULL(OBD.monTotAmtBefDiscount, OBD.monTotAmount) - OBD.monTotAmount ) AS DiscAmt,
			(CASE WHEN OBD.numUnitHour = 0 THEN 0 ELSE CONVERT(DECIMAL(18, 4),(dbo.fn_UOMConversion(ISNULL(opp.numUOMId, 0), i.numItemCode, @numDomainID, ISNULL(i.numBaseUnit, 0)) * (ISNULL(OBD.monTotAmount,0) / OBD.numUnitHour))) END) As monUnitSalePrice,
            ISNULL(OBD.vcNotes,ISNULL(opp.vcNotes,'')) vcNotes,
            OBD.vcTrackingNo,
            OBD.vcShippingMethod,
            OBD.monShipCost,
            [dbo].[FormatedDateFromDate](OBD.dtDeliveryDate,@numDomainID) dtDeliveryDate,
            Opp.numWarehouseItmsID,
            ISNULL(u.vcUnitName, '') numUOMId,
            ISNULL(Opp.vcManufacturer, '') AS vcManufacturer,
            ISNULL(V.monCost, 0) AS VendorCost,
            Opp.vcPathForTImage,
            i.[fltLength],
            i.[fltWidth],
            i.[fltHeight],
            i.[fltWeight],
            ISNULL(I.numVendorID, 0) numVendorID,
            Opp.bitDropShip AS DropShip,
            SUBSTRING(( SELECT  ' ,' + vcSerialNo
                                + CASE WHEN ISNULL(I.bitLotNo, 0) = 1
                                        THEN ' ('
                                            + CONVERT(VARCHAR(15), oppI.numQty)
                                            + ')'
                                        ELSE ''
                                    END
                        FROM    OppWarehouseSerializedItem oppI
                                JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID = whi.numWareHouseItmsDTLID
                        WHERE   oppI.numOppID = Opp.numOppId
                                AND oppI.numOppItemID = Opp.numoppitemtCode
                        ORDER BY vcSerialNo
                        FOR
                        XML PATH('')
                        ), 3, 200000) AS SerialLotNo,
            ISNULL(opp.numUOMId, 0) AS numUOM,
            ISNULL(u.vcUnitName, '') vcUOMName,
            dbo.fn_UOMConversion(opp.numUOMId, Opp.numItemCode,
                                    @numDomainID, NULL) AS UOMConversionFactor,
            ISNULL(Opp.numSOVendorId, 0) AS numSOVendorId,
            [dbo].FormatedDateTimeFromDate(OBD.dtRentalStartDate,
                                            @numDomainID) dtRentalStartDate,
            [dbo].FormatedDateTimeFromDate(OBD.dtRentalReturnDate,
                                            @numDomainID) dtRentalReturnDate,
            ISNULL(W.vcWareHouse, '') AS Warehouse,
            CASE WHEN charitemType = 'P' AND ISNULL(numItemGroup,0) > 0 AND ISNULL(bitMatrix,0)=0 THEN ISNULL(WI.vcWHSKU,I.vcSKU) ELSE I.vcSKU END vcSKU,
            CASE WHEN charitemType = 'P' AND ISNULL(numItemGroup,0) > 0 AND ISNULL(bitMatrix,0)=0 THEN ISNULL(WI.vcBarCode,I.numBarCodeId) ELSE I.numBarCodeId END vcBarcode,
			CASE WHEN charitemType = 'P' AND ISNULL(numItemGroup,0) > 0 AND ISNULL(bitMatrix,0)=0 THEN ISNULL(WI.vcBarCode,I.numBarCodeId) ELSE I.numBarCodeId END numBarCodeId,
            ISNULL(opp.numClassID, 0) AS numClassID,
            ISNULL(opp.numProjectID, 0) AS numProjectID,
            ISNULL(I.[numShipClass],0) AS numShipClass,OBD.numOppBizDocID,WL.vcLocation,ISNULL(bitSerialized,0) AS bitSerialized,ISNULL(bitLotNo,0) AS bitLotNo,
            opp.numOppId,OBD.numUnitHour AS numUnitHourOrig,ISNULL(I.numItemCode,0) AS [numItemCode],
			ISNULL(I.bitKitParent,0) AS bitKitParent,
			0 AS numOppChildItemID, 
			0 AS numOppKitChildItemID,
			0 AS bitChildItem,
			0 AS tintLevel,
			ISNULL(opp.vcPromotionDetail,'') AS vcPromotionDetail,
			(CASE WHEN ISNULL(WI.numWareHouseItemID,0) > 0 THEN CONCAT(W.vcWareHouse,CASE WHEN WL.numWLocationID > 0 THEN CONCAT('(',WL.vcLocation ,')') ELSE '' END) ELSE '' END) vcWarehouse,
			(CASE WHEN (SELECT COUNT(*) FROM #TempBizDocProductGridColumns WHERE vcDbColumnName='vcInclusionDetails') > 0 THEN dbo.GetOrderAssemblyKitInclusionDetails(opp.numOppID,Opp.numoppitemtCode,OBD.numUnitHour) ELSE '' END) AS vcInclusionDetails,
			Opp.numSortOrder
        FROM      
			OpportunityItems opp
        JOIN 
			OpportunityBizDocItems OBD 
		ON 
			OBD.numOppItemID = opp.numoppitemtCode
        LEFT JOIN 
			item i 
		ON 
			opp.numItemCode = i.numItemCode
        LEFT JOIN 
			ListDetails L 
		ON 
			i.numItemClassification = L.numListItemID
        LEFT JOIN 
			Vendor V 
		ON 
			V.numVendorID = @DivisionID
            AND V.numItemCode = opp.numItemCode
        LEFT JOIN 
			General_Journal_Header GJH 
		ON 
			opp.numoppid = GJH.numOppId
            AND GJH.numBizDocsPaymentDetId IS NULL
            AND GJH.numOppBizDocsId = @numOppBizDocsId
        LEFT JOIN 
			UOM u 
		ON 
			u.numUOMId = opp.numUOMId                                      
        LEFT JOIN 
			dbo.WareHouseItems WI 
		ON 
			opp.numWarehouseItmsID = WI.numWareHouseItemID
			AND WI.numDomainID = @numDomainID
        LEFT JOIN 
			dbo.Warehouses W 
		ON 
			WI.numDomainID = W.numDomainID
            AND WI.numWareHouseID = W.numWareHouseID
        LEFT JOIN 
			dbo.WarehouseLocation WL 
		ON 
			WL.numWLocationID = WI.numWLocationID
        WHERE     
			opp.numOppId = @numOppId
            AND (bitShowDeptItem IS NULL OR bitShowDeptItem = 0)
            AND OBD.numOppBizDocID = @numOppBizDocsId
    ) X
	ORDER BY
		numSortOrder

	--GET KIT CHILD ITEMS (NOW WE ARE SUPPORTING KIT WITHIN KIT BUT ONLY 1 LEVEL MEANS CHILD KIT INSIDE PARENT KIT CAN NOT HAVE ANOTHER KIT AS CHILD)
	IF (@bitDisplayKitChild=1 AND (SELECT COUNT(*) FROM OpportunityKitItems WHERE numOppId = @numOppId ) > 0)
	BEGIN
		DECLARE @TEMPTABLE TABLE(
			numOppItemCode NUMERIC(18,0),
			numOppChildItemID NUMERIC(18,0), 
			numOppKitChildItemID NUMERIC(18,0),
			numItemCode NUMERIC(18,0),
			numWarehouseItemID NUMERIC(18,0),
			numTotalQty NUMERIC(18,2),
			numQty NUMERIC(18,2),
			numUOMID NUMERIC(18,0),
			tintLevel TINYINT
		);

		--FIRST LEVEL CHILD
		INSERT INTO @TEMPTABLE
		(
			numOppItemCode,numOppChildItemID,numOppKitChildItemID,numItemCode,numWarehouseItemID,numTotalQty,numQty,numUOMID,tintLevel
		)
		SELECT 
			OKI.numOppItemID,
			OKI.numOppChildItemID,
			CAST(0 AS NUMERIC(18,0)),
			OKI.numChildItemID,
			ISNULL(OKI.numWareHouseItemId,0),
			CAST((t1.numOrgTotalUnitHour * OKI.numQtyItemsReq_Orig) AS NUMERIC(18,2)),
			CAST((t1.numUnitHour * OKI.numQtyItemsReq_Orig) AS NUMERIC(18,2)),
			ISNULL(OKI.numUOMID,0),
			1
		FROM 
			#Temp1 t1
		INNER JOIN
			OpportunityKitItems OKI
		ON
			t1.numoppitemtCode = OKI.numOppItemID
		WHERE
			t1.bitKitParent = 1

		--SECOND LEVEL CHILD
		INSERT INTO @TEMPTABLE
		(
			numOppItemCode,numOppChildItemID,numOppKitChildItemID,numItemCode,numWarehouseItemID,numTotalQty,numQty,numUOMID,tintLevel
		)
		SELECT 
			OKCI.numOppItemID,
			OKCI.numOppChildItemID,
			OKCI.numOppKitChildItemID,
			OKCI.numItemID,
			ISNULL(OKCI.numWareHouseItemId,0),
			CAST((c.numTotalQty * OKCI.numQtyItemsReq_Orig) AS NUMERIC(18,2)),
			CAST((c.numQty * OKCI.numQtyItemsReq_Orig) AS NUMERIC(18,2)),
			ISNULL(OKCI.numUOMID,0),
			2
		FROM 
			OpportunityKitChildItems OKCI
		INNER JOIN
			@TEMPTABLE c
		ON
			OKCI.numOppID = @numOppId
			AND OKCI.numOppItemID = c.numOppItemCode
			AND OKCI.numOppChildItemID = c.numOppChildItemID

		INSERT INTO 
			#Temp1
		SELECT   
			(CASE WHEN t2.tintLevel = 1 THEN CONCAT('&nbsp;&nbsp;',I.vcItemName) WHEN t2.tintLevel = 2 THEN CONCAT('&nbsp;&nbsp;&nbsp;&nbsp;',I.vcItemName) ELSE I.vcItemName END) AS vcItemName,
            0 OppBizDocItemID,ISNULL(WI.numBackOrder,0) AS numBackOrder,
            (CASE WHEN I.charitemType = 'P' THEN 'Product' WHEN charitemType = 'S' THEN 'Service' END) AS charitemType,
            (CASE WHEN charitemType = 'P' THEN 'Inventory'
	                WHEN charitemType = 'N' THEN 'Non-Inventory'
                    WHEN charitemType = 'S' THEN 'Service'
            END) AS vcItemType,
			CONVERT(VARCHAR(2500), I.txtItemDesc) AS txtItemDesc,   
			t2.numTotalQty AS numOrigTotalUnitHour,
			t2.numTotalQty AS numTotalUnitHour,
            t2.numQty AS numUnitHour,
            (ISNULL(dbo.fn_UOMConversion(ISNULL(t2.numUOMID, 0), i.numItemCode, @numDomainID, ISNULL(i.numBaseUnit, 0)),1) * (CASE WHEN I.charitemType = 'P' THEN ISNULL(WI.monWListPrice,0) ELSE ISNULL(I.monListPrice,0) END)) monPrice,
            CONVERT(VARCHAR, CONVERT(DECIMAL(18, 4),((t2.numQty * ISNULL(dbo.fn_UOMConversion(ISNULL(t2.numUOMID, 0), i.numItemCode, @numDomainID, ISNULL(i.numBaseUnit, 0)),1)) * (CASE WHEN I.charitemType = 'P' THEN ISNULL(WI.monWListPrice,0) ELSE ISNULL(I.monListPrice,0) END)))) Amount,
            ((t2.numQty * ISNULL(dbo.fn_UOMConversion(ISNULL(t2.numUOMID, 0), i.numItemCode, @numDomainID, ISNULL(i.numBaseUnit, 0)),1)) * (CASE WHEN I.charitemType = 'P' THEN ISNULL(WI.monWListPrice,0) ELSE ISNULL(I.monListPrice,0) END)) AS monTotAmount/*Fo calculating sum*/,
            ISNULL(CONVERT(VARCHAR, (CASE WHEN I.charitemType = 'P' THEN ISNULL(WI.monWListPrice,0) ELSE ISNULL(I.monListPrice,0) END)), 0) AS monListPrice,
            I.numItemCode AS ItemCode,
            t2.numOppItemCode,
            L.vcdata AS numItemClassification,
            CASE WHEN bitTaxable = 0 THEN 'No' ELSE 'Yes' END AS Taxable,
            numIncomeChartAcntId AS itemIncomeAccount,
            'NI' AS ItemType,
            0 AS numJournalId,
            0 AS numTransactionId,
            ISNULL(I.vcModelID, '') vcModelID,
            dbo.USP_GetAttributes(t2.numWarehouseItemID, bitSerialized) AS vcAttributes,
            ISNULL(vcPartNo, '') AS vcPartNo,
            0 AS DiscAmt,
			(ISNULL(dbo.fn_UOMConversion(ISNULL(t2.numUOMID, 0), i.numItemCode, @numDomainID, ISNULL(i.numBaseUnit, 0)),1) * (CASE WHEN I.charitemType = 'P' THEN ISNULL(WI.monWListPrice,0) ELSE ISNULL(I.monListPrice,0) END)) As monUnitSalePrice,
            '' AS vcNotes,'' AS vcTrackingNo,'' AS vcShippingMethod,0 AS monShipCost,NULL AS dtDeliveryDate,t2.numWarehouseItemID,
            ISNULL(u.vcUnitName, '') numUOMId,ISNULL(I.vcManufacturer, '') AS vcManufacturer,ISNULL(V.monCost, 0) AS VendorCost,
            ISNULL((SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = I.numItemCode AND bitDefault=1),'') AS vcPathForTImage,
            I.[fltLength],I.[fltWidth],I.[fltHeight],I.[fltWeight],ISNULL(I.numVendorID, 0) numVendorID,0 AS DropShip,
            '' AS SerialLotNo,
            ISNULL(t2.numUOMId, 0) AS numUOM,
            ISNULL(u.vcUnitName, '') vcUOMName,
            dbo.fn_UOMConversion(t2.numUOMId, t2.numItemCode,@numDomainID, NULL) AS UOMConversionFactor,
            0 AS numSOVendorId,
            NULL AS dtRentalStartDate,
            NULL dtRentalReturnDate,
            ISNULL(W.vcWareHouse, '') AS Warehouse,
            CASE WHEN charitemType = 'P' AND ISNULL(numItemGroup,0) > 0 AND ISNULL(bitMatrix,0)=0 THEN ISNULL(WI.vcWHSKU,I.vcSKU) ELSE I.vcSKU END vcSKU,
            CASE WHEN charitemType = 'P' AND ISNULL(numItemGroup,0) > 0 AND ISNULL(bitMatrix,0)=0 THEN ISNULL(WI.vcBarCode,I.numBarCodeId) ELSE I.numBarCodeId END vcBarcode,
			CASE WHEN charitemType = 'P' AND ISNULL(numItemGroup,0) > 0 AND ISNULL(bitMatrix,0)=0 THEN ISNULL(WI.vcBarCode,I.numBarCodeId) ELSE I.numBarCodeId END numBarCodeId,
            ISNULL(I.numItemClass, 0) AS numClassID,
            0 AS numProjectID,
            ISNULL(I.[numShipClass],0) AS numShipClass,
			@numOppBizDocsId AS numOppBizDocID,WL.vcLocation,ISNULL(bitSerialized,0) AS bitSerialized,ISNULL(bitLotNo,0) AS bitLotNo,
            @numOppId AS numOppId,t2.numQty AS numUnitHourOrig,
			ISNULL(I.numItemCode,0) AS [numItemCode],
			ISNULL(I.bitKitParent,0) AS bitKitParent,
			ISNULL(t2.numOppChildItemID,0) AS numOppChildItemID, 
			ISNULL(t2.numOppKitChildItemID,0) AS numOppKitChildItemID,
			1 AS bitChildItem,
			t2.tintLevel AS tintLevel,
			'' AS vcPromotionDetail,
			(CASE WHEN ISNULL(WI.numWareHouseItemID,0) > 0 THEN CONCAT(W.vcWareHouse,CASE WHEN ISNULL(WL.numWLocationID,0) > 0 THEN CONCAT('(',WL.vcLocation ,')') ELSE '' END) ELSE '' END) vcWarehouse,
			'' vcInclusionDetails
			,0
		FROM
			@TEMPTABLE t2
		INNER JOIN
			Item I
		ON
			t2.numItemCode = I.numItemCode
		LEFT JOIN
			WareHouseItems WI
		ON
			WI.numWareHouseItemID = t2.numWarehouseItemID
		LEFT JOIN 
			dbo.Warehouses W 
		ON 
			WI.numDomainID = W.numDomainID
            AND WI.numWareHouseID = W.numWareHouseID
        LEFT JOIN 
			dbo.WarehouseLocation WL 
		ON 
			WL.numWLocationID = WI.numWLocationID
		LEFT JOIN 
			ListDetails L 
		ON 
			I.numItemClassification = L.numListItemID
		LEFT JOIN 
			Vendor V 
		ON 
			V.numVendorID = @DivisionID
            AND V.numItemCode = t2.numItemCode
		LEFT JOIN 
			UOM u 
		ON 
			u.numUOMId = t2.numUOMId 
	END


    DECLARE @strSQLUpdate AS VARCHAR(2000);SET @strSQLUpdate=''
   
    EXEC ('ALTER TABLE #Temp1 ADD [TotalTax] MONEY')

    IF @tintTaxOperator = 1 --add Sales tax
	BEGIN
		SET @strSQLUpdate = @strSQLUpdate
            + ',[TotalTax]= (CASE WHEN ISNULL(bitChildItem,0) = 0 THEN dbo.fn_CalBizDocTaxAmt('
            + CONVERT(VARCHAR(20), @numDomainID) + ',0,'
            + CONVERT(VARCHAR(20), @numOppId) +','+
            + 'numoppitemtCode,1,Amount,numUnitHour) ELSE 0 END)'
	END        
    ELSE IF @tintTaxOperator = 2 -- remove sales tax
	BEGIN
        SET @strSQLUpdate = @strSQLUpdate + ',[TotalTax]= 0'
	END
    ELSE 
	BEGIN
        SET @strSQLUpdate = @strSQLUpdate
            + ',[TotalTax]= (CASE WHEN ISNULL(bitChildItem,0) = 0 THEN dbo.fn_CalBizDocTaxAmt('
            + CONVERT(VARCHAR(20), @numDomainID) + ',0,'
            + CONVERT(VARCHAR(20), @numOppId) +','+
            + 'numoppitemtCode,1,Amount,numUnitHour) ELSE 0 END)'
    END

	-- CRV TAX TYPE
	EXEC ('ALTER TABLE #Temp1 ADD [CRV] MONEY')

	IF @tintTaxOperator = 2
	BEGIN
		SET @strSQLUpdate = @strSQLUpdate
				+ ',[CRV]= (CASE WHEN ISNULL(bitChildItem,0) = 0 THEN dbo.fn_CalBizDocTaxAmt('
				+ CONVERT(VARCHAR(20), @numDomainID) + ',1,'
				+ CONVERT(VARCHAR(20), @numOppId) +','+
				+ 'numoppitemtCode,1,Amount,numUnitHour) ELSE 0 END)'
	END
	ELSE
	BEGIN
		SET @strSQLUpdate = @strSQLUpdate + ',[CRV]= 0'
	END

	DECLARE @vcTaxName AS VARCHAR(100)
    DECLARE @numTaxItemID AS NUMERIC(9)
    SET @numTaxItemID = 0
    
	SELECT TOP 1
        @vcTaxName = vcTaxName,
        @numTaxItemID = numTaxItemID
    FROM    
		TaxItems
    WHERE   
		numDomainID = @numDomainID

    WHILE @numTaxItemID > 0
    BEGIN
        EXEC ('ALTER TABLE #Temp1 ADD [' + @vcTaxName + '] MONEY')

		IF @tintTaxOperator = 2 -- remove sales tax
		BEGIN
			SET @strSQLUpdate = @strSQLUpdate + ',[' + @vcTaxName + ']=0' 
		END
		ELSE
		BEGIN
			SET @strSQLUpdate = @strSQLUpdate + ',[' + @vcTaxName
				+ ']= (CASE WHEN ISNULL(bitChildItem,0) = 0 THEN dbo.fn_CalBizDocTaxAmt('
				+ CONVERT(VARCHAR(20), @numDomainID) + ','
				+ CONVERT(VARCHAR(20), @numTaxItemID) + ','
				+ CONVERT(VARCHAR(20), @numOppId) +','+
				+ 'numoppitemtCode,1,Amount,numUnitHour) ELSE 0 END)'
		END

        SELECT TOP 1
			@vcTaxName = vcTaxName,
            @numTaxItemID = numTaxItemID
        FROM    
			TaxItems
        WHERE   
			numDomainID = @numDomainID
            AND numTaxItemID > @numTaxItemID

        IF @@rowcount = 0 
            SET @numTaxItemID = 0
    END

    IF @strSQLUpdate <> '' 
    BEGIN
        SET @strSQLUpdate = 'UPDATE #Temp1 SET ItemCode=ItemCode' + @strSQLUpdate + ' WHERE ItemCode > 0 AND bitChildItem=0'
        EXEC (@strSQLUpdate)
    END

    DECLARE @vcDbColumnName NVARCHAR(50)

    SELECT TOP 1
            @numFldID = numfieldID,
            @vcFldname = vcFieldName,
            @vcDbColumnName = vcDbColumnName,
            @intRowNum = ( tintRow + 1 )
    FROM    View_DynamicCustomColumns
    WHERE   numFormID = @tintType
            AND Grp_id = 5
            AND numDomainID = @numDomainID
            AND numAuthGroupID = @numBizDocId
            AND bitCustom = 1
            AND ISNULL(numRelCntType, 0) = @numBizDocTempID
    ORDER BY tintRow     
	                                                                                     
    WHILE @intRowNum > 0                                                                                  
    BEGIN    
        EXEC('ALTER TABLE #Temp1 ADD [' + @vcDbColumnName + '] varchar(100)')
	
        SET @strSQLUpdate = 'UPDATE #Temp1 set ItemCode=ItemCode,[' + @vcDbColumnName + ']= dbo.GetCustFldValueItem(' + @numFldID  + ',ItemCode) WHERE ItemCode > 0 AND bitChildItem=0'
            
		EXEC (@strSQLUpdate)
		                   
        SELECT 
			TOP 1
            @numFldID = numfieldID,
            @vcFldname = vcFieldName,
            @vcDbColumnName = vcDbColumnName,
            @intRowNum = ( tintRow + 1 )
        FROM    
			View_DynamicCustomColumns
        WHERE   
			numFormID = @tintType
            AND Grp_id = 5
            AND numDomainID = @numDomainID
            AND numAuthGroupID = @numBizDocId
            AND bitCustom = 1
            AND tintRow >= @intRowNum
            AND ISNULL(numRelCntType, 0) = @numBizDocTempID
        ORDER BY 
			tintRow                                                                  
		           
        IF @@rowcount = 0 
            SET @intRowNum = 0                                                                                          
    END


    SELECT  
		ROW_NUMBER() OVER ( ORDER BY numoppitemtCode ) RowNumber,
        *
    FROM    
		#Temp1
    ORDER BY 
		numoppitemtCode ASC, numOppChildItemID ASC, numOppKitChildItemID ASC

    DROP TABLE #Temp1

    SELECT  
		ISNULL(tintOppStatus, 0)
    FROM    
		OpportunityMaster
    WHERE   
		numOppID = @numOppId                                                                                                 


	SELECT * FROM #TempBizDocProductGridColumns

	IF OBJECT_ID('tempdb..#TempBizDocProductGridColumns') IS NOT NULL DROP TABLE #TempBizDocProductGridColumns
END
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_oppdetailsdtlpl')
DROP PROCEDURE usp_oppdetailsdtlpl
GO
CREATE PROCEDURE [dbo].[USP_OPPDetailsDTLPL]
(
               @OpportunityId        AS NUMERIC(9)  = NULL,
               @numDomainID          AS NUMERIC(9),
               @ClientTimeZoneOffset AS INT)
AS
  SELECT Opp.numoppid,numCampainID,
--         (SELECT vcdata
--          FROM   listdetails
--          WHERE  numlistitemid = numCampainID) AS numCampainIDName,
		 CAMP.vcCampaignName AS [numCampainIDName],
         dbo.Fn_getcontactname(Opp.numContactId) numContactIdName,
		 Opp.numContactId,
         Opp.txtComments,
         Opp.numDivisionID,
         tintOppType,
         Dateadd(MINUTE,-@ClientTimeZoneOffset,bintAccountClosingDate) AS bintAccountClosingDate,
         dbo.fn_GetOpportunitySourceValue(ISNULL(Opp.tintSource,0),ISNULL(Opp.tintSourceType,0),Opp.numDomainID) AS tintSourceName,
          tintSource,
         Opp.VcPoppName,
         intpEstimatedCloseDate,
         [dbo].[getdealamount](@OpportunityId,Getutcdate(),0) AS CalAmount,
         --monPAmount,
         CONVERT(DECIMAL(10,2),Isnull(monPAmount,0)) AS monPAmount,
		lngPConclAnalysis,
         (SELECT vcdata
          FROM   listdetails
          WHERE  numlistitemid = lngPConclAnalysis) AS lngPConclAnalysisName,
         (SELECT vcdata
          FROM   listdetails
          WHERE  numlistitemid = numSalesOrPurType) AS numSalesOrPurTypeName,
		  numSalesOrPurType,
		 Opp.vcOppRefOrderNo,
		 Opp.vcMarketplaceOrderID,
         C2.vcCompanyName,
         D2.tintCRMType,
         Opp.tintActive,
         dbo.Fn_getcontactname(Opp.numCreatedby)
           + ' '
           + CONVERT(VARCHAR(20),Dateadd(MINUTE,-@ClientTimeZoneOffset,Opp.bintCreatedDate)) AS CreatedBy,
         dbo.Fn_getcontactname(Opp.numModifiedBy)
           + ' '
           + CONVERT(VARCHAR(20),Dateadd(MINUTE,-@ClientTimeZoneOffset,Opp.bintModifiedDate)) AS ModifiedBy,
         dbo.Fn_getcontactname(Opp.numRecOwner) AS RecordOwner,
         Opp.numRecOwner,
         Isnull(tintshipped,0) AS tintshipped,
         Isnull(tintOppStatus,0) AS tintOppStatus,
         [dbo].[GetOppStatus](Opp.numOppId) AS vcDealStatus,
         dbo.Opportunitylinkeditems(@OpportunityId) +
         (SELECT COUNT(*) FROM  dbo.Communication Comm JOIN Correspondence Corr ON Comm.numCommId=Corr.numCommID
			WHERE Comm.numDomainId=Opp.numDomainId AND Corr.numDomainID=Comm.numDomainID AND Corr.tintCorrType IN (1,2,3,4) AND Corr.numOpenRecordID=Opp.numOppId) AS NoOfProjects,
         (SELECT A.vcFirstName
                   + ' '
                   + A.vcLastName
          FROM   AdditionalContactsInformation A
          WHERE  A.numcontactid = opp.numAssignedTo) AS numAssignedToName,
		  opp.numAssignedTo,
         Opp.numAssignedBy,
         (SELECT COUNT(* )
          FROM   GenericDocuments
          WHERE  numRecID = @OpportunityId
                 AND vcDocumentSection = 'O') AS DocumentCount,
         CASE 
           WHEN tintOppStatus = 1 THEN 100
           ELSE isnull((SELECT SUM(tintPercentage)
                 FROM   OpportunityStageDetails
                 WHERE  numOppId = @OpportunityId
                        AND bitStageCompleted = 1),0)
         END AS TProgress,
         (SELECT TOP 1 Isnull(varRecurringTemplateName,'')
          FROM   RecurringTemplate
          WHERE  numRecurringId = OPR.numRecurringId) AS numRecurringIdName,
		 OPR.numRecurringId,
         Link.numOppID AS numParentOppID,
         Link.vcPoppName AS ParentOppName,
         Link.vcSource AS Source,
         ISNULL(C.varCurrSymbol,'') varCurrSymbol,
          ISNULL(Opp.fltExchangeRate,0) AS [fltExchangeRate],
           ISNULL(C.chrCurrency,'') AS [chrCurrency],
         ISNULL(C.numCurrencyID,0) AS [numCurrencyID],
         (SELECT COUNT(*) FROM [OpportunityLinking] OL WHERE OL.[numParentOppID] = Opp.numOppId) AS ChildCount,
         (SELECT COUNT(*) FROM [RecurringTransactionReport] RTR WHERE RTR.[numRecTranSeedId] = Opp.numOppId)  AS RecurringChildCount,
         (SELECT COUNT(*) FROM [ShippingReport] WHERE numOppBizDocId IN (SELECT numOppBizDocsId FROM   OpportunityBizDocs BD WHERE  BD.numOppId = @OpportunityId))AS ShippingReportCount,
         ISNULL(Opp.numStatus ,0) numStatus,
         (SELECT vcdata
          FROM   listdetails
          WHERE  numlistitemid = Opp.numStatus) AS numStatusName,
          (SELECT SUBSTRING(
	(SELECT ','+  A.vcFirstName+' ' +A.vcLastName + CASE WHEN ISNULL(SR.numContactType,0)>0 THEN '(' + dbo.fn_GetListItemName(SR.numContactType) + ')' ELSE '' END
	FROM ShareRecord SR JOIN UserMaster UM ON UM.numUserDetailId=SR.numAssignedTo 
							 JOIN AdditionalContactsInformation A ON UM.numUserDetailId=A.numContactID 
		 WHERE SR.numDomainID=Opp.numDomainID AND SR.numModuleID=3 AND SR.numRecordID=Opp.numOppId
				AND UM.numDomainID=Opp.numDomainID and UM.numDomainID=A.numDomainID FOR XML PATH('')),2,200000)) AS numShareWith,
		ISNULL((SELECT SUM(ISNULL(monDealAmount,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = @OpportunityId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0) AS monDealAmount,
		ISNULL((SELECT SUM(ISNULL(monAmountPaid,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = @OpportunityId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0) AS monAmountPaid,
		ISNULL(Opp.tintSourceType,0) AS tintSourceType,
		ISNULL(Opp.tintTaxOperator,0) AS tintTaxOperator,  
             isnull(Opp.intBillingDays,0) AS numBillingDays,isnull(Opp.bitInterestType,0) AS bitInterestType,
             isnull(opp.fltInterest,0) AS fltInterest,  isnull(Opp.fltDiscount,0) AS fltDiscount,
             isnull(Opp.bitDiscountType,0) AS bitDiscountType,ISNULL(Opp.bitBillingTerms,0) AS bitBillingTerms,
             ISNULL(OPP.numDiscountAcntType,0) AS numDiscountAcntType,
              dbo.fn_getOPPAddress(Opp.numOppId,@numDomainID,1) AS BillingAddress,
            dbo.fn_getOPPAddress(Opp.numOppId,@numDomainID,2) AS ShippingAddress,
             ISNULL((SELECT SUBSTRING((SELECT '$^$' + ISNULL( (SELECT vcShipFieldValue FROM dbo.ShippingFieldValues WHERE numDomainID=Opp.numDomainID AND intShipFieldID IN ( SELECT intShipFieldID FROM dbo.ShippingFields WHERE numListItemID=OBD.numShipVia  AND vcFieldName='Tracking URL')),'') +'#^#'+ RTRIM(LTRIM(vcTrackingNo) )  FROM dbo.OpportunityBizDocs OBD WHERE OBD.numOppId=Opp.numOppID FOR XML PATH('')),4,200000)),'') AS  vcTrackingNo,
            ISNULL(numPercentageComplete,0) numPercentageComplete,
            dbo.GetListIemName(ISNULL(numPercentageComplete,0)) AS PercentageCompleteName,
            ISNULL(numBusinessProcessID,0) AS [numBusinessProcessID],
            CASE WHEN opp.tintOppType=1 THEN dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID) ELSE '' END AS vcInventoryStatus
            ,ISNULL(bitUseShippersAccountNo,0) [bitUseShippersAccountNo], ISNULL(D2.vcShippersAccountNo,'') [vcShippersAccountNo] ,
			(SELECT SUM((ISNULL(monTotAmtBefDiscount, ISNULL(monTotAmount,0))- ISNULL(monTotAmount,0))) FROM OpportunityBizDocItems WHERE numOppBizDocID IN (SELECT numOppBizDocsId FROM OpportunityBizDocs WHERE numOppId = @OpportunityId AND bitAuthoritativeBizDocs = 1)) AS numTotalDiscount,
			ISNULL(Opp.vcRecurrenceType,'') AS vcRecurrenceType,
			ISNULL(Opp.bitRecurred,0) AS bitRecurred,
			(
				CASE 
				WHEN opp.tintOppType=1 AND opp.tintOppStatus = 1 
				THEN  
					dbo.GetOrderShipReceiveStatus(opp.numOppId,1,opp.tintShipped)
				ELSE 
						'' 
				END
			) AS vcOrderedShipped,
			(
				CASE 
				WHEN opp.tintOppType = 2 AND opp.tintOppStatus = 1 
				THEN  
					dbo.GetOrderShipReceiveStatus(opp.numOppId,2,opp.tintShipped)
				ELSE 
						'' 
				END
			) AS vcOrderedReceived,
			ISNULL(C2.numCompanyType,0) AS numCompanyType,
		ISNULL(C2.vcProfile,0) AS vcProfile,
		ISNULL(Opp.numAccountClass,0) AS numAccountClass,D3.vcPartnerCode+'-'+C3.vcCompanyName as numPartner,
		ISNULL(Opp.numPartner,0) AS numPartnerId,
		ISNULL(Opp.bitIsInitalSalesOrder,0) AS bitIsInitalSalesOrder,
		ISNULL(Opp.dtReleaseDate,'') AS dtReleaseDate,
		ISNULL(Opp.numPartenerContact,0) AS numPartenerContactId,
		A.vcGivenName AS numPartenerContact,
		Opp.numReleaseStatus,
		(CASE Opp.numReleaseStatus WHEN 1 THEN 'Open' WHEN 2 THEN 'Purchased' ELSE '' END) numReleaseStatusName,
		ISNULL(Opp.numShipmentMethod,0) numShipmentMethod
		,CASE WHEN ISNULL(Opp.numShipmentMethod,0) = 0 THEN '' ELSE ISNULL((SELECT vcData FROM ListDetails WHERE numListID = (CASE WHEN Opp.tintOpptype =2 THEN 338 ELSE 82 END) AND numListItemID=ISNULL(Opp.numShipmentMethod,0)),'') END AS vcShipmentMethod
		,ISNULL(Opp.vcCustomerPO#,'') vcCustomerPO#
  FROM   OpportunityMaster Opp
         JOIN divisionMaster D2 ON Opp.numDivisionID = D2.numDivisionID
         JOIN CompanyInfo C2 ON C2.numCompanyID = D2.numCompanyID
		 LEFT JOIN divisionMaster D3 ON Opp.numPartner = D3.numDivisionID
		 LEFT JOIN AdditionalContactsInformation A ON Opp.numPartenerContact = A.numContactId
         LEFT JOIN CompanyInfo C3 ON C3.numCompanyID = D3.numCompanyID
         LEFT JOIN (SELECT TOP 1 vcPoppName,
                                 numOppID,
                                 numChildOppID,
                                 vcSource,numParentOppID,OpportunityMaster.numDivisionID AS numParentDivisionID 
                    FROM   OpportunityLinking
                          left JOIN OpportunityMaster
                             ON numOppID = numParentOppID
                    WHERE  numChildOppID = @OpportunityId) Link
           ON Link.numChildOppID = Opp.numOppID
           LEFT OUTER JOIN [Currency] C
					ON C.numCurrencyID = Opp.numCurrencyID
			LEFT OUTER JOIN [OpportunityRecurring] OPR
					ON OPR.numOppId = Opp.numOppId
					 LEFT JOIN AddressDetails AD --Billing add
				ON AD.numDomainID=D2.numDomainID AND AD.numRecordID=D2.numDivisionID AND AD.tintAddressOf = 2 AND AD.tintAddressType = 1 AND AD.bitIsPrimary=1
			 LEFT JOIN AddressDetails AD2--Shipping address
				ON AD2.numDomainID=D2.numDomainID AND AD2.numRecordID=D2.numDivisionID AND AD2.tintAddressOf = 2 AND AD2.tintAddressType = 2 AND AD2.bitIsPrimary=1	
		LEFT JOIN dbo.CampaignMaster CAMP ON CAMP.numCampaignID = Opp.numCampainID 
  WHERE  Opp.numOppId = @OpportunityId
         AND Opp.numDomainID = @numDomainID
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OPPGetINItems')
DROP PROCEDURE USP_OPPGetINItems
GO
-- =============================================  
-- Modified by: <Author,,Sachin Sadhu>  
-- Create date: <Create Date,,24thDec2013>  
-- Description: <Description,,add 3 fields:vcBizDocImagePath,vcBizDocFooter,vcPurBizDocFooter>  
-- =============================================  
CREATE PROCEDURE [dbo].[USP_OPPGetINItems]
(
          @byteMode        AS TINYINT  = NULL,
          @numOppId        AS NUMERIC(9)  = NULL,
          @numOppBizDocsId AS NUMERIC(9)  = NULL,
          @numDomainID     AS NUMERIC(9)  = 0,
          @numUserCntID    AS NUMERIC(9)  = 0,
          @ClientTimeZoneOffset INT=0)
AS
  IF @byteMode = 1
    BEGIN
      DECLARE  @BizDcocName  AS VARCHAR(100)
      DECLARE  @OppName  AS VARCHAR(100)
      DECLARE  @Contactid  AS VARCHAR(100)
      DECLARE  @shipAmount  AS MONEY
      DECLARE  @tintOppType  AS TINYINT
      DECLARE  @numlistitemid  AS VARCHAR(15)
      DECLARE  @RecOwner  AS VARCHAR(15)
      DECLARE  @MonAmount  AS VARCHAR(15)
      DECLARE  @OppBizDocID  AS VARCHAR(100)
      DECLARE  @bizdocOwner  AS VARCHAR(15)
      DECLARE  @tintBillType  AS TINYINT
      DECLARE  @tintShipType  AS TINYINT
      DECLARE  @numCustomerDivID AS NUMERIC 
      
      SELECT @RecOwner = numRecOwner,@MonAmount = CONVERT(VARCHAR(20),monPAmount),
             @Contactid = numContactId,@OppName = vcPOppName,
             @tintOppType = tintOppType,@tintBillType = tintBillToType,@tintShipType = tintShipToType
      FROM   OpportunityMaster WHERE  numOppId = @numOppId
      
      -- When Creating PO from SO and Bill type is Customer selected 
      SELECT @numCustomerDivID = ISNULL(numDivisionID,0) FROM dbo.OpportunityMaster WHERE 
			numOppID IN (SELECT  ISNULL(numParentOppID,0) FROM dbo.OpportunityLinking WHERE numChildOppID = @numOppId)
			
      --select @BizDcocName=vcBizDocID from OpportunityBizDocs where numOppBizDocsId=@numOppBizDocsId
      SELECT @BizDcocName = vcdata,
             @shipAmount = monShipCost,
             @numlistitemid = numlistitemid,
             @OppBizDocID = vcBizDocID,
             @bizdocOwner = OpportunityBizDocs.numCreatedBy
      FROM   listdetails JOIN OpportunityBizDocs ON numBizDocId = numlistitemid
      WHERE  numOppBizDocsId = @numOppBizDocsId
      
      SELECT @bizdocOwner AS BizDocOwner,
             @OppBizDocID AS BizDocID,
             @MonAmount AS Amount,
             isnull(@RecOwner,1) AS Owner,
             @BizDcocName AS BizDcocName,
             @numlistitemid AS BizDoc,
             @OppName AS OppName,
             VcCompanyName AS CompName,
             @shipAmount AS ShipAmount,
             dbo.fn_getOPPAddress(@numOppId,@numDomainID,1) AS BillAdd,
             dbo.fn_getOPPAddress(@numOppId,@numDomainID,2) AS ShipAdd,
             AD.VcStreet,
             AD.VcCity,
             isnull(dbo.fn_GetState(AD.numState),'') AS vcBilState,
             AD.vcPostalCode,
             isnull(dbo.fn_GetListItemName(AD.numCountry),
                    '') AS vcBillCountry,
             con.vcFirstName AS ConName,
             ISNULL(con.numPhone,'') numPhone,
             con.vcFax,
             isnull(con.vcEmail,'') AS vcEmail,
             div.numDivisionID,
             numContactid,
             isnull(vcFirstFldValue,'') vcFirstFldValue,
             isnull(vcSecndFldValue,'') vcSecndFldValue,
             isnull(bitTest,0) AS bitTest,
			dbo.fn_getOPPAddress(@numOppId,@numDomainID,1) AS BillToAddressName,
			dbo.fn_getOPPAddress(@numOppId,@numDomainID,1) AS ShipToAddressName,
             CASE 
               WHEN @tintBillType IS NULL THEN Com.vcCompanyName
             	-- When Sales order and bill to is set to customer 
               WHEN @tintBillType = 1 AND @tintOppType = 1 THEN Com.vcCompanyName
             -- When Create PO from SO and Bill to is set to Customer 
			   WHEN @tintBillType = 1 AND @tintOppType = 2 THEN dbo.fn_GetComapnyName(@numCustomerDivID)
               WHEN @tintBillType = 0 THEN (SELECT TOP 1 Com1.vcCompanyName
                                            FROM   companyinfo [Com1]
                                                   JOIN divisionmaster div1
                                                     ON com1.numCompanyID = div1.numCompanyID
                                                   JOIN Domain D1
                                                     ON D1.numDivisionID = div1.numDivisionID
                                                    JOIN dbo.AddressDetails AD1
														ON AD1.numDomainID = div1.numDomainID AND AD1.numRecordID = div1.numDivisionID AND tintAddressOf = 2 AND tintAddressType = 1 AND bitIsPrimary=1
                                            WHERE  D1.numDomainID = @numDomainID)
               WHEN @tintBillType = 2 THEN (SELECT vcBillCompanyName
                                            FROM   OpportunityAddress
                                            WHERE  numOppID = @numOppId)
				WHEN @tintBillType = 3 THEN  Com.vcCompanyName
              END AS BillToCompanyName,
             CASE 
               WHEN @tintShipType IS NULL THEN Com.vcCompanyName
               WHEN @tintShipType = 1 AND @tintOppType = 1 THEN Com.vcCompanyName
				-- When Create PO from SO and Ship to is set to Customer 
   			   WHEN @tintShipType = 1 AND @tintOppType = 2 THEN dbo.fn_GetComapnyName(@numCustomerDivID)
               WHEN @tintShipType = 0 THEN (SELECT TOP 1 Com1.vcCompanyName
                                            FROM   companyinfo [Com1]
                                                   JOIN divisionmaster div1
                                                     ON com1.numCompanyID = div1.numCompanyID
                                                   JOIN Domain D1
                                                     ON D1.numDivisionID = div1.numDivisionID
                                                    JOIN dbo.AddressDetails AD1
													 ON AD1.numDomainID = div1.numDomainID AND AD1.numRecordID = div1.numDivisionID AND tintAddressOf = 2 AND tintAddressType = 2 AND bitIsPrimary=1
                                            WHERE  D1.numDomainID = @numDomainID)
               WHEN @tintShipType = 2 THEN (SELECT vcShipCompanyName
                                            FROM   OpportunityAddress
                                            WHERE  numOppID = @numOppId)
				WHEN @tintShipType = 3 THEN (SELECT vcShipCompanyName
														FROM   OpportunityAddress
														WHERE  numOppID = @numOppId)
             END AS ShipToCompanyName
      FROM   companyinfo [Com] JOIN divisionmaster div ON com.numCompanyID = div.numCompanyID
             JOIN AdditionalContactsInformation con ON con.numdivisionId = div.numdivisionId
             JOIN Domain D ON D.numDomainID = div.numDomainID
             LEFT JOIN PaymentGatewayDTLID PGDTL ON PGDTL.intPaymentGateWay = D.intPaymentGateWay AND D.numDomainID = PGDTL.numDomainID
             LEFT JOIN AddressDetails AD --Billing add
				ON AD.numDomainID=DIV.numDomainID AND AD.numRecordID=div.numDivisionID AND AD.tintAddressOf = 2 AND AD.tintAddressType = 1 AND AD.bitIsPrimary=1
			 LEFT JOIN AddressDetails AD2--Shipping address
				ON AD2.numDomainID=DIV.numDomainID AND AD2.numRecordID=div.numDivisionID AND AD2.tintAddressOf = 2 AND AD2.tintAddressType = 2 AND AD2.bitIsPrimary=1	
      WHERE  numContactid = @Contactid AND Div.numDomainID = @numDomainID
    END
  IF @byteMode = 2
    BEGIN
      SELECT opp.vcBizDocID,
             Mst.fltDiscount AS decDiscount,
             isnull(opp.monAmountPaid,0) AS monAmountPaid,
             isnull(opp.vcComments,'') AS vcComments,
             CONVERT(VARCHAR(20),DATEADD(minute,-@ClientTimeZoneOffset,opp.dtCreatedDate)) dtCreatedDate,
             dbo.fn_GetContactName(opp.numCreatedby) AS numCreatedby,
             dbo.fn_GetContactName(opp.numModifiedBy) AS numModifiedBy,
             CONVERT(VARCHAR(20),DATEADD(minute,-@ClientTimeZoneOffset,opp.dtModifiedDate)) dtModifiedDate,
             CASE 
               WHEN isnull(opp.numViewedBy,0) = 0 THEN ''
               ELSE dbo.fn_GetContactName(opp.numViewedBy)
             END AS numViewedBy,
             CASE 
               WHEN opp.numViewedBy = 0 THEN NULL
               ELSE opp.dtViewedDate
             END AS dtViewedDate,
             (CASE WHEN isnull(Mst.intBillingDays,0) > 0 THEN CAST(1 AS BIT) ELSE isnull(Mst.bitBillingTerms,0) END) AS tintBillingTerms,
             isnull(Mst.intBillingDays,0) AS numBillingDays,
			 ISNULL(BTR.numNetDueInDays,0) AS numBillingDaysName,
			 BTR.vcTerms AS vcBillingTermsName,
             isnull(Mst.bitInterestType,0) AS tintInterestType,
             isnull(Mst.fltInterest,0) AS fltInterest,
             tintOPPType,
             Opp.numBizDocId,
             dbo.fn_GetContactName(numApprovedBy) AS ApprovedBy,
             dtApprovedDate,
             Mst.bintAccountClosingDate,
             tintShipToType,
             tintBillToType,
             tintshipped,
             dtShippedDate bintShippedDate,
             isnull(opp.monShipCost,0) AS monShipCost,
             (SELECT COUNT(* )
              FROM   DocumentWorkflow
              WHERE  numDocID = @numOppBizDocsId
                     AND numContactID = @numUserCntID
                     AND cDocType = 'B'
                     AND tintApprove = 0) AS AppReq,
             (SELECT COUNT(* )
              FROM   DocumentWorkflow
              WHERE  numDocID = @numOppBizDocsId
                     AND cDocType = 'B'
                     AND tintApprove = 1) AS Approved,
             (SELECT COUNT(* )
              FROM   DocumentWorkflow
              WHERE  numDocID = @numOppBizDocsId
                     AND cDocType = 'B'
                     AND tintApprove = 2) AS Declined,
             (SELECT COUNT(* )
              FROM   DocumentWorkflow
              WHERE  numDocID = @numOppBizDocsId
                     AND cDocType = 'B'
                     AND tintApprove = 0) AS Pending,
             ISNULL(BT.vcBizDocFooter,D.vcBizDocFooter) AS BizdocFooter,
             ISNULL(BT.vcBizDocImagePath,D.vcBizDocImagePath) As vcBizDocImagePath, 
             Mst.numDivisionID,
             ISNULL(BT.vcPurBizDocFooter,D.vcPurBizDocFooter) AS PurBizdocFooter, 
             isnull(Mst.fltDiscount,0) AS fltDiscount,
             isnull(Mst.bitDiscountType,0) AS bitDiscountType,
             isnull(Opp.numShipVia,ISNULL(Mst.numShipmentMethod,0)) AS numShipVia,
--             isnull(vcTrackingURL,'') AS vcTrackingURL,
			ISNULL( (SELECT vcShipFieldValue FROM dbo.ShippingFieldValues WHERE numDomainID=MSt.numDomainID AND intShipFieldID IN ( SELECT intShipFieldID FROM dbo.ShippingFields WHERE numListItemID=Opp.numShipVia  AND vcFieldName='Tracking URL')),'')		AS vcTrackingURL,
             isnull(Opp.numBizDocStatus,0) AS numBizDocStatus,
             CASE 
               WHEN Opp.numBizDocStatus IS NULL THEN '-'
               ELSE dbo.fn_GetListItemName(Opp.numBizDocStatus)
             END AS BizDocStatus,
             CASE 
               WHEN ISNULL(Opp.numShipVia,0) = 0 THEN (CASE WHEN Mst.numShipmentMethod IS NULL THEN '-' WHEN Mst.numShipmentMethod = -1 THEN 'Will-call' ELSE dbo.fn_GetListItemName(Mst.numShipmentMethod) END)
			   WHEN Opp.numShipVia = -1 THEN 'Will-call'
               ELSE dbo.fn_GetListItemName(Opp.numShipVia)
             END AS ShipVia,
             ISNULL(C.varCurrSymbol,'') varCurrSymbol,
             (SELECT COUNT(*) FROM [ShippingReport] WHERE numOppBizDocId = opp.numOppBizDocsId )AS ShippingReportCount,
			 isnull(bitPartialFulfilment,0) bitPartialFulfilment,
			 ISNULL(Opp.vcBizDocName,'') vcBizDocName,
			 dbo.[fn_GetContactName](opp.[numModifiedBy])  + ', '
				+ CONVERT(VARCHAR(20),dateadd(MINUTE,-@ClientTimeZoneOffset,Opp.dtModifiedDate)) AS ModifiedBy,
			 dbo.[fn_GetContactName](Mst.[numRecOwner]) + ', ' +  dbo.[fn_GetContactName](Mst.[numAssignedTo]) AS OrderRecOwner,
			 dbo.[fn_GetContactName](DM.[numRecOwner]) + ', ' +  dbo.[fn_GetContactName](DM.[numAssignedTo]) AS AccountRecOwner,
			 Opp.dtFromDate,
			 Mst.tintTaxOperator,
			 dbo.GetAccountedEmbeddedCost(opp.numOppBizDocsID,1) AS monTotalEmbeddedCost,
			 dbo.GetAccountedEmbeddedCost(opp.numOppBizDocsID,2) AS monTotalAccountedCost,
			 ISNULL(BT.bitEnabled,0) bitEnabled,
			 ISNULL(BT.txtBizDocTemplate,'') txtBizDocTemplate, 
			 ISNULL(BT.txtCSS,'') txtCSS,
			 ISNULL(BT.numOrientation,0) numOrientation,
			 ISNULL(BT.bitKeepFooterBottom,0) bitKeepFooterBottom,
			 dbo.fn_GetContactName(Mst.numAssignedTo) AS AssigneeName,
			 ISNULL((SELECT vcEmail FROM dbo.AdditionalContactsInformation WHERE numContactID = Mst.numAssignedTo),'') AS AssigneeEmail,
			 ISNULL((SELECT numPhone FROM dbo.AdditionalContactsInformation WHERE numContactID = Mst.numAssignedTo),'') AS AssigneePhone,
			 dbo.fn_GetComapnyName(Mst.numDivisionId) OrganizationName,
			 dbo.fn_GetContactName(Mst.numContactID)  OrgContactName,

--CASE WHEN Mst.tintOppType = 1 THEN 
--dbo.getCompanyAddress((select ISNULL(DM.numDivisionID,0)  as numDivisionID from AdditionalContactsInformation AC INNER JOIN DivisionMaster DM 
--on AC.numDivisionID = DM.numDivisionID where numContactID = Mst.numCreatedBy),1,Mst.numDomainId)
--               ELSE dbo.getCompanyAddress(Mst.numDivisionId,1,Mst.numDomainId)
--             END AS CompanyBillingAddress1,

CASE WHEN Mst.tintOppType = 1 THEN 
dbo.getCompanyAddress((select ISNULL(AC.numDivisionID,0)  as numDivisionID from AdditionalContactsInformation AC
INNER JOIN Domain D ON D.numDomainID = AC.numDomainID where AC.numContactID = D.numAdminID AND D.numDomainID = Mst.numDomainId),1,Mst.numDomainId)
               ELSE dbo.getCompanyAddress(Mst.numDivisionId,1,Mst.numDomainId)
             END AS CompanyBillingAddress,

CASE WHEN Mst.tintOppType = 1 THEN 
dbo.getCompanyAddress((select ISNULL(AC.numDivisionID,0)  as numDivisionID from AdditionalContactsInformation AC
INNER JOIN Domain D ON D.numDomainID = AC.numDomainID where AC.numContactID = D.numAdminID AND D.numDomainID = Mst.numDomainId),1,Mst.numDomainId)
               ELSE dbo.getCompanyAddress(Mst.numDivisionId,2,Mst.numDomainId)
             END AS CompanyShippingAddress,
--
--			 dbo.getCompanyAddress(Mst.numDivisionId,1,Mst.numDomainId) CompanyBillingAddress,
--			 dbo.getCompanyAddress(Mst.numDivisionId,2,Mst.numDomainId) CompanyShippingAddress,
			 case when ACI.numPhone<>'' then ACI.numPhone +case when ACI.numPhoneExtension<>'' then ' - ' + ACI.numPhoneExtension else '' end  else '' END OrgContactPhone,
			 DM.vcComPhone as OrganizationPhone,
             ISNULL(ACI.vcEmail,'') AS OrgContactEmail,
 			 dbo.[fn_GetContactName](Mst.[numRecOwner]) AS OnlyOrderRecOwner,
 			 ISNULL(Opp.bitAuthoritativeBizDocs,0) bitAuthoritativeBizDocs,
			 ISNULL(Opp.tintDeferred,0) tintDeferred,isnull(Opp.monCreditAmount,0) AS monCreditAmount,
			 isnull(Opp.bitRentalBizDoc,0) as bitRentalBizDoc,isnull(BT.numBizDocTempID,0) as numBizDocTempID,isnull(BT.vcTemplateName,'') as vcTemplateName,
		     [dbo].[GetDealAmount](opp.numOppId ,getutcdate(),opp.numOppBizDocsId ) as monPAmount,
			 --(select top 1 SD.vcSignatureFile from SignatureDetail SD join OpportunityBizDocsDetails OBD on SD.numSignID=OBD.numSignID where SD.numDomainID = @numDomainID and OBD.numDomainID = @numDomainID and OBD.numSignID IS not null and OBD.numBizDocsId=opp.numOppBizDocsId order by OBD.numBizDocsPaymentDetId desc) as vcSignatureFile,
			 isnull(CMP.txtComments,'') as vcOrganizationComments,
			 isnull(Opp.vcTrackingNo,'') AS  vcTrackingNo,isnull(Opp.vcShippingMethod,'') AS  vcShippingMethod,
			 Opp.dtDeliveryDate,
			 CASE WHEN Opp.vcRefOrderNo IS NULL OR LEN(Opp.vcRefOrderNo)=0 THEN ISNULL(Mst.vcOppRefOrderNo,'') ELSE 
			  isnull(Opp.vcRefOrderNo,'') END AS vcRefOrderNo,ISNULL(Mst.numDiscountAcntType,0) AS numDiscountAcntType,
			 ISNULL(opp.numSequenceId,'') AS numSequenceId,
			 CASE WHEN ISNULL(Opp.numBizDocId,0)=296 THEN ISNULL((SELECT SUBSTRING((SELECT '$^$' + dbo.fn_GetListItemName(numBizDocStatus) +'#^#'+ dbo.fn_GetContactName(numUserCntID) +'#^#'+ dbo.FormatedDateTimeFromDate(DateAdd(minute, -@ClientTimeZoneOffset , dtCreatedDate),@numDomainId) + '#^#'+ isnull(DFCS.vcColorScheme,'NoForeColor')
					FROM OppFulfillmentBizDocsStatusHistory OFBS left join DycFieldColorScheme DFCS on OFBS.numBizDocStatus=DFCS.vcFieldValue AND DFCS.numDomainID=@numDomainID
					WHERE numOppId=opp.numOppId and numOppBizDocsId=opp.numOppBizDocsId FOR XML PATH('')),4,200000)),'') ELSE '' END AS vcBizDocsStatusList,ISNULL(Mst.numCurrencyID,0) AS numCurrencyID,
			ISNULL(Opp.fltExchangeRateBizDoc,Mst.fltExchangeRate) AS fltExchangeRateBizDoc,
			ISNULL(TBLEmployer.vcCompanyName,'') AS EmployerOrganizationName,ISNULL(TBLEmployer.vcComPhone,'') as EmployerOrganizationPhone,ISNULL(opp.bitAutoCreated,0) AS bitAutoCreated,
			ISNULL(RecurrenceConfiguration.numRecConfigID,0) AS numRecConfigID,
			(CASE WHEN RecurrenceConfiguration.numRecConfigID IS NULL THEN 0 ELSE 1 END) AS bitRecur,
			(CASE WHEN RecurrenceTransaction.numRecTranID IS NULL THEN 0 ELSE 1 END) AS bitRecurred,
			ISNULL(Mst.bitRecurred,0) AS bitOrderRecurred,
			dbo.FormatedDateFromDate(RecurrenceConfiguration.dtStartDate,@numDomainID) AS dtStartDate,
			RecurrenceConfiguration.vcFrequency AS vcFrequency,
			ISNULL(RecurrenceConfiguration.bitDisabled,0) AS bitDisabled,
			ISNULL(opp.bitFulfilled,0) AS bitFulfilled,
			ISNULL(opp.numDeferredBizDocID,0) AS numDeferredBizDocID,
			dbo.GetTotalQtyByUOM(opp.numOppBizDocsId) AS vcTotalQtybyUOM,
			ISNULL(Mst.intUsedShippingCompany,0) AS intUsedShippingCompany,
			dbo.FormatedDateFromDate(Mst.dtReleaseDate,@numDomainID) AS vcReleaseDate,
			(CASE WHEN ISNULL(Mst.numPartner,0) > 0 THEN ISNULL((SELECT CONCAT(DivisionMaster.vcPartnerCode,'-',CompanyInfo.vcCompanyName) FROM DivisionMaster INNER JOIN CompanyInfo ON DivisionMaster.numCompanyID=CompanyInfo.numCompanyId WHERE DivisionMaster.numDivisionID=Mst.numPartner),'') ELSE '' END) AS vcPartner,
			dbo.FormatedDateFromDate(mst.dtReleaseDate,@numDomainID) AS dtReleaseDate,
			(select TOP 1 vcBizDocID from OpportunityBizDocs where numOppId=opp.numOppId and numBizDocId=296) as vcFulFillment,
			(select TOP 1 vcOppRefOrderNo from OpportunityMaster WHERE numOppId=opp.numOppId) AS vcPOName,
			ISNULL(vcCustomerPO#,'') AS vcCustomerPO#
		FROM   OpportunityBizDocs opp JOIN OpportunityMaster Mst ON Mst.numOppId = opp.numOppId
             JOIN Domain D ON D.numDomainID = Mst.numDomainID
             LEFT OUTER JOIN [Currency] C ON C.numCurrencyID = Mst.numCurrencyID
             LEFT JOIN [DivisionMaster] DM ON DM.numDivisionID = Mst.numDivisionID   
			 LEFT JOIN CompanyInfo CMP ON DM.numCompanyId = CMP.numCompanyId and CMP.numDomainID = @numDomainID  
             LEFT JOIN dbo.AdditionalContactsInformation ACI ON ACI.numContactID = Mst.numContactID
             LEFT JOIN [BizDocTemplate] BT ON BT.numDomainID = @numDomainID AND BT.numBizDocID = Opp.numBizDocID AND BT.numOppType = Mst.tintOppType AND BT.tintTemplateType=0 and BT.numBizDocTempID=opp.numBizDocTempID
             LEFT JOIN BillingTerms BTR ON BTR.numTermsID = ISNULL(Mst.intBillingDays,0)
             JOIN divisionmaster div1 ON Mst.numDivisionID = div1.numDivisionID
			 JOIN companyinfo [Com1] ON com1.numCompanyID = div1.numCompanyID
			 LEFT JOIN RecurrenceConfiguration ON Opp.numOppBizDocsId = RecurrenceConfiguration.numOppBizDocID
			 LEFT JOIN RecurrenceTransaction ON Opp.numOppBizDocsId = RecurrenceTransaction.numRecurrOppBizDocID
			 OUTER APPLY
			 (
				SELECT TOP 1 
					Com1.vcCompanyName, div1.vcComPhone
                FROM   companyinfo [Com1]
                        JOIN divisionmaster div1
                            ON com1.numCompanyID = div1.numCompanyID
                        JOIN Domain D1
                            ON D1.numDivisionID = div1.numDivisionID
                        JOIN dbo.AddressDetails AD1
							ON AD1.numDomainID = div1.numDomainID AND AD1.numRecordID = div1.numDivisionID AND tintAddressOf = 2 AND tintAddressType = 1 AND bitIsPrimary=1
                WHERE  D1.numDomainID = @numDomainID
			 ) AS TBLEmployer
      WHERE  opp.numOppBizDocsId = @numOppBizDocsId
             AND Mst.numDomainID = @numDomainID
    END
/****** Object:  StoredProcedure [dbo].[USP_OPPGetINItemsForAuthorizativeAccounting]    Script Date: 07/26/2008 16:20:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by Siva                                                                                                            
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_oppgetinitemsforauthorizativeaccounting')
DROP PROCEDURE usp_oppgetinitemsforauthorizativeaccounting
GO
CREATE PROCEDURE  [dbo].[USP_OPPGetINItemsForAuthorizativeAccounting]                                                                                                                                                
(                                                                                                                                                
 @numOppId as numeric(9)=null,                                                                                                                                                
 @numOppBizDocsId as numeric(9)=null,                                                                
 @numDomainID as numeric(9)=0 ,                                                      
 @numUserCntID as numeric(9)=0                                                                                                                                               
)                                                                                                                                                
                                                                                                                                            
as                           
begin

declare @numCurrencyID as numeric(9)
declare @fltExchangeRate as float

declare @fltCurrentExchangeRate as float
declare @tintType as tinyint                                                          
declare @vcPOppName as VARCHAR(100)                                                          
DECLARE @bitPPVariance AS BIT
DECLARE @numDivisionID AS NUMERIC(9)

select  @numCurrencyID=numCurrencyID, @fltExchangeRate=fltExchangeRate,@tintType=tintOppType,@vcPOppName=vcPOppName,@bitPPVariance=ISNULL(bitPPVariance,0),@numDivisionID=numDivisionID from  OpportunityMaster
where numOppID=@numOppId 

DECLARE @vcBaseCurrency AS VARCHAR(100);SET  @vcBaseCurrency=''
DECLARE @bitAutolinkUnappliedPayment AS BIT 
SELECT @vcBaseCurrency=ISNULL(C.varCurrSymbol,''),@bitAutolinkUnappliedPayment=ISNULL(bitAutolinkUnappliedPayment,0) FROM  dbo.Domain D LEFT JOIN Currency C ON D.numCurrencyID=C.numCurrencyID 
WHERE D.numDomainID=@numDomainID

DECLARE @vcForeignCurrency AS VARCHAR(100) ;SET  @vcForeignCurrency=''
SELECT @vcForeignCurrency=ISNULL(C.varCurrSymbol,'') FROM  Currency C WHERE numCurrencyID=@numCurrencyID

DECLARE @fltExchangeRateBizDoc AS FLOAT 
declare @numBizDocId as numeric(9)                                                           

DECLARE @vcBizDocID AS VARCHAR(100)
select @numBizDocId=numBizDocId,@fltExchangeRateBizDoc=fltExchangeRateBizDoc,@vcBizDocID=vcBizDocID  from OpportunityBizDocs where numOppBizDocsId=@numOppBizDocsId                                                         

if @numCurrencyID>0 set @fltCurrentExchangeRate=dbo.GetExchangeRate(@numDomainID,@numCurrencyID)
else set @fltCurrentExchangeRate=@fltExchangeRate                                                                                                                                          

declare @strSQL as varchar(8000)                                                          
declare @strSQLCusFields varchar(1000)                                                          
declare @strSQLEmpFlds varchar(500)                                                          
set @strSQLCusFields=''                                                          
set @strSQLEmpFlds=''                                                          
declare @intRowNum as int                                                          
declare @numFldID as varchar(15)                                                          
declare @vcFldname as varchar(50)                                                                                                                                          
                                                                
if @tintType=1 set @tintType=7                                                                
else set @tintType=8                                                                                                                      
select top 1 @numFldID=numfieldID,@vcFldname=vcFieldName,@intRowNum=(tintRow+1) from View_DynamicCustomColumns                                       
where Grp_id=@tintType and numDomainID=@numDomainID and numAuthGroupID=@numBizDocId and bitCustom=1 order by tintRow                                                          
 while @intRowNum>0                                               
 begin                                                          
  set @strSQLCusFields=@strSQLCusFields+',  dbo.GetCustFldValueBizdoc('+@numFldID+','+convert(varchar(2),@tintType)+',opp.numoppitemtCode) as ['+ @vcFldname+']'                                                     
  set @strSQLEmpFlds=@strSQLEmpFlds+',''-'' as ['+ @vcFldname+']'         
                                                 
  select top 1 @numFldID=numfieldID,@vcFldname=vcFieldName,@intRowNum=(tintRow+1) from View_DynamicCustomColumns                                                               
  where Grp_id=@tintType and numDomainID=@numDomainID and numAuthGroupID=@numBizDocId                                                          
  and bitCustom=1 and tintRow>@intRowNum order by tintRow                                                          
                                                            
  if @@rowcount=0 set @intRowNum=0                                                          
                                                           
 end                                                           
                                                       
       
         
select I.[vcItemName] as Item,        
charitemType as type,        
OBI.vcitemdesc as [desc],        
OBI.numUnitHour as Unit,        
ISNULL(OBI.monPrice,0) as Price,        
ISNULL(OBI.monTotAmount,0) as Amount
,ISNULL((opp.monTotAmount/opp.numUnitHour)*OBI.numUnitHour,0) AS  ItemTotalAmount,       
isnull(monListPrice,0) as listPrice,convert(varchar,i.numItemCode) as ItemCode,numoppitemtCode,        
L.vcdata as vcItemClassification,case when bitTaxable=0 then 'No' else 'Yes' end as Taxable,monListPrice +' '+@strSQLCusFields,isnull(i.numIncomeChartAcntId,0) as itemIncomeAccount,isnull(i.numAssetChartAcntId,0) as itemInventoryAsset,'NI' as ItemType        
,isnull(i.numCOGsChartAcntId,0) as itemCoGs,(CASE WHEN ISNULL(i.bitVirtualInventory,0) = 1 THEN 0 ELSE isnull(Opp.monAvgCost,'0') END) as AverageCost ,isnull(OBI.bitDropShip,0) as  bitDropShip ,  (OBI.monTotAmtBefDiscount-OBI.monTotAmount) as DiscAmt,
NULLIF(Opp.numProjectID,0) numProjectID ,NULLIF(Opp.numClassID,0) numClassID,ISNULL(i.bitKitParent,0) AS bitKitParent,ISNULL(i.bitAssembly,0) AS bitAssembly
from  OpportunityItems opp
join  OpportunityBizDocItems OBI
on OBI.numOppItemID=Opp.numoppitemtCode       
left join item i on opp.numItemCode=i.numItemCode        
left join ListDetails L on i.numItemClassification=L.numListItemID        
where Opp.numOppId=@numOppId  and OBI.numOppBizDocID=@numOppBizDocsId
                 
                                                                                                                  
                                                         
  select  isnull(@numCurrencyID,0) as numCurrencyID, isnull(@fltExchangeRate,1) as fltExchangeRate,isnull(@fltCurrentExchangeRate,1) as CurrfltExchangeRate,isnull(@fltExchangeRateBizDoc,1) AS fltExchangeRateBizDoc ,@vcBaseCurrency AS vcBaseCurrency,@vcForeignCurrency AS vcForeignCurrency,ISNULL(@vcPOppName,'') AS vcPOppName,ISNULL(@vcBizDocID,'') AS vcBizDocID,ISNULL(@bitPPVariance,0) AS bitPPVariance,@numDivisionID AS numDivisionID,ISNULL(@bitAutolinkUnappliedPayment,0) AS bitAutolinkUnappliedPayment                  
     
     
                                                  
End
GO
/****** Object:  StoredProcedure [dbo].[USP_OppManage]    Script Date: 07/26/2008 16:20:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OppManage')
DROP PROCEDURE usp_oppmanage
GO
CREATE PROCEDURE [dbo].[USP_OppManage] 
(                                                                          
  @numOppID as numeric=0 OUTPUT,                                                                          
  @numContactId numeric(9)=null,                                                                          
  @numDivisionId numeric(9)=null,                                                                          
  @tintSource numeric(9)=null,                                                                          
  @vcPOppName Varchar(100)='' OUTPUT,                                                                          
  @Comments varchar(1000)='',                                                                          
  @bitPublicFlag bit=0,                                                                          
  @numUserCntID numeric(9)=0,                                                                                 
  @monPAmount money =0,                                                 
  @numAssignedTo as numeric(9)=0,                                                                                                        
  @numDomainId numeric(9)=0,                                                                                                                                                                                   
  @strItems as text=null,                                                                          
  @strMilestone as text=null,                                                                          
  @dtEstimatedCloseDate datetime,                                                                          
  @CampaignID as numeric(9)=null,                                                                          
  @lngPConclAnalysis as numeric(9)=null,                                                                         
  @tintOppType as tinyint,                                                                                                                                             
  @tintActive as tinyint,                                                              
  @numSalesOrPurType as numeric(9),              
  @numRecurringId as numeric(9),
  @numCurrencyID as numeric(9)=0,
  @DealStatus as tinyint =0,-- 0=Open,1=Won,2=Lost
  @numStatus AS NUMERIC(9),
  @vcOppRefOrderNo VARCHAR(100),
  @numBillAddressId numeric(9)=0,
  @numShipAddressId numeric(9)=0,
  @bitStockTransfer BIT=0,
  @WebApiId INT = 0,
  @tintSourceType TINYINT=0,
  @bitDiscountType as bit,
  @fltDiscount  as float,
  @bitBillingTerms as bit,
  @intBillingDays as integer,
  @bitInterestType as bit,
  @fltInterest as float,
  @tintTaxOperator AS TINYINT,
  @numDiscountAcntType AS NUMERIC(9),
  @vcWebApiOrderNo VARCHAR(100)=NULL,
  @vcCouponCode		VARCHAR(100) = '',
  @vcMarketplaceOrderID VARCHAR(100)=NULL,
  @vcMarketplaceOrderDate  datetime=NULL,
  @vcMarketplaceOrderReportId VARCHAR(100)=NULL,
  @numPercentageComplete NUMERIC(9),
  @bitUseShippersAccountNo BIT = 0,
  @bitUseMarkupShippingRate BIT = 0,
  @numMarkupShippingRate NUMERIC(19,2) = 0,
  @intUsedShippingCompany INT = 0,
  @numShipmentMethod NUMERIC(18,0)=0,
  @dtReleaseDate DATE = NULL,
  @numPartner NUMERIC(18,0)=0,
  @tintClickBtn INT=0,
  @numPartenerContactId NUMERIC(18,0)=0,
  @numAccountClass NUMERIC(18,0) = 0,
  @numWillCallWarehouseID NUMERIC(18,0) = 0,
  @numVendorAddressID NUMERIC(18,0) = 0
)                                                                          
                                                                          
as  
BEGIN TRY
BEGIN TRANSACTION
	DECLARE @fltExchangeRate float                                 
	DECLARE @hDocItem int                                                                                                                            
	DECLARE @TotalAmount as money                                                                          
	DECLARE @tintOppStatus as tinyint
	DECLARE @bitNewOrder AS BIT = (CASE WHEN ISNULL(@numOppID,0) = 0 THEN 1 ELSE 0 END)
	
	  
	
	IF ISNULL(@tintOppType,0) = 0
	BEGIN
		RAISERROR('OPP_TYPE_CAN_NOT_BE_0',16,1)
		RETURN
	END             

	IF CONVERT(VARCHAR(10),@strItems) <> ''
	BEGIN
		EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems

		--VALIDATE PROMOTION OF COUPON CODE
		DECLARE @TEMP TABLE
		(
			ID INT IDENTITY(1,1),
			numOppItemID NUMERIC(18,0),
			numPromotionID NUMERIC(18,0)
		)

		INSERT INTO @TEMP
		(
			numOppItemID,
			numPromotionID
		)
		SELECT
			numoppitemtCode,
			numPromotionID
		FROM
			OPENXML (@hDocItem,'/NewDataSet/Item',2)
		WITH
		(
			numoppitemtCode NUMERIC(18,0),
			numPromotionID NUMERIC(18,0)
		)

		IF (SELECT
				COUNT(*)
			FROM
				PromotionOffer PO
			WHERE
				numDomainId=@numDomainId
				AND bitRequireCouponCode = 1
				AND ISNULL(bitEnabled,0) = 1
				AND ISNULL(tintUsageLimit,0) > 0
				AND (intCouponCodeUsed 
					- (CASE WHEN ISNULL((SELECT COUNT(*) FROM OpportunityItems WHERE numOppID=ISNULL(@numOppID,0) AND numPromotionID=PO.numProId),0) > 0 THEN 1 ELSE 0 END) 
					+ (CASE WHEN ISNULL((SELECT COUNT(*) FROM @TEMP WHERE numPromotionID=PO.numProId),0) > 0 THEN 1 ELSE 0 END)) > tintUsageLimit) > 0
		BEGIN
			RAISERROR('USAGE_OF_COUPON_EXCEED',16,1)
			RETURN
		END
		ELSE
		BEGIN
			UPDATE
				PO
			SET 
				intCouponCodeUsed = ISNULL(intCouponCodeUsed,0) 
									- (CASE WHEN ISNULL((SELECT COUNT(*) FROM OpportunityItems WHERE numOppID=ISNULL(@numOppID,0) AND numPromotionID=PO.numProId),0) > 0 THEN 1 ELSE 0 END) 
									+ (CASE WHEN ISNULL(T1.intUsed,0) > 0 THEN 1 ELSE 0 END) 
			FROM
				PromotionOffer PO
			INNER JOIN
				(
					SELECT
						numPromotionID,
						COUNT(*) AS intUsed
					FROM
						@TEMP
					GROUP BY
						numPromotionID
				) AS T1
			ON
				PO.numProId = T1.numPromotionID
			WHERE
				numDomainId=@numDomainId
				AND bitRequireCouponCode = 1
				AND ISNULL(bitEnabled,0) = 1
		END
	END

	--IF NEW OPPERTUNITY
	IF @numOppID = 0                                                                          
	BEGIN
		DECLARE @bitIsInitalSalesOrder BIT
		IF(@DealStatus=1 AND @tintOppType=1)
		BEGIN
			SET @bitIsInitalSalesOrder=1
		END

		DECLARE @intOppTcode AS NUMERIC(9)  
  
		SELECT @intOppTcode = MAX(numOppId) FROM OpportunityMaster                

		SET @intOppTcode = ISNULL(@intOppTcode,0) + 1                                                
		
		SET @vcPOppName = @vcPOppName + '-'+ CONVERT(varchar(4),YEAR(GETUTCDATE()))  +'-A'+ CONVERT(varchar(10),@intOppTcode)                                  
  
		IF ISNULL(@numCurrencyID,0) = 0 
		BEGIN
			SET @fltExchangeRate=1
			SELECT @numCurrencyID= ISNULL(numCurrencyID,0) FROM dbo.Domain WHERE numDomainId=@numDomainId	
		END
		ELSE
		BEGIN 
			SET @fltExchangeRate = dbo.GetExchangeRate(@numDomainID,@numCurrencyID)
        END  
		                                                        
		IF ISNULL(@numAccountClass,0) = 0
		BEGIN                                                  
		DECLARE @tintDefaultClassType AS INT = 0
		SELECT @tintDefaultClassType = ISNULL(tintDefaultClassType,0) FROM Domain WHERE numDomainId = @numDomainID 

		IF @tintDefaultClassType = 1 --USER
		BEGIN
			SELECT 
				@numAccountClass=ISNULL(numDefaultClass,0) 
			FROM 
				dbo.UserMaster UM 
			JOIN 
				dbo.Domain D 
			ON 
				UM.numDomainID=D.numDomainId
			WHERE 
				D.numDomainId=@numDomainId 
				AND UM.numUserDetailId=@numUserCntID
		END
		ELSE IF @tintDefaultClassType = 2 --COMPANY
		BEGIN
			SELECT 
				@numAccountClass=ISNULL(numAccountClassID,0) 
			FROM 
				dbo.DivisionMaster DM 
			WHERE 
				DM.numDomainId=@numDomainId 
				AND DM.numDivisionID=@numDivisionID
		END
		ELSE
		BEGIN
			SET @numAccountClass = 0
		END
		END

        IF(@DealStatus=1 AND @tintOppType=1 AND @tintClickBtn=1)
		BEGIN
			SET @DealStatus=0
		END         
		--PRINT ('Deal Staus ' +@DealStatus)                                                      
		INSERT INTO OpportunityMaster
		(                                                                             
			numContactId,numDivisionId,txtComments,numCampainID,bitPublicFlag,tintSource,tintSourceType,vcPOppName,                                                                          
			intPEstimatedCloseDate,monPAmount,numCreatedBy,bintCreatedDate,numModifiedBy,bintModifiedDate,numDomainId,                                                                                                             
			numRecOwner,lngPConclAnalysis,tintOppType,numSalesOrPurType,numCurrencyID,fltExchangeRate,numAssignedTo,
			numAssignedBy,[tintOppStatus],numStatus,vcOppRefOrderNo,bitStockTransfer,bitBillingTerms,intBillingDays,
			bitInterestType,fltInterest,vcCouponCode,bitDiscountType,fltDiscount,bitPPVariance,vcMarketplaceOrderID,
			vcMarketplaceOrderReportId,numPercentageComplete,numAccountClass,tintTaxOperator,bitUseShippersAccountNo,
			bitUseMarkupShippingRate,numMarkupShippingRate,intUsedShippingCompany,numShipmentMethod,numPartner,numPartenerContact,dtReleaseDate,bitIsInitalSalesOrder,numVendorAddressID
		)                                                                          
		Values                                                                          
		(                                                                          
			@numContactId,@numDivisionId,ISNULL(@Comments,''),@CampaignID,@bitPublicFlag,@tintSource,@tintSourceType,@vcPOppName,                                                                          
			@dtEstimatedCloseDate,@monPAmount,@numUserCntID,(CASE @tintSourceType WHEN 3 THEN @vcMarketplaceOrderDate ELSE GETUTCDATE() END),                                                                          
			@numUserCntID,GETUTCDATE(),@numDomainId,@numUserCntID,@lngPConclAnalysis,@tintOppType,@numSalesOrPurType,@numCurrencyID, 
			@fltExchangeRate,(CASE WHEN @numAssignedTo > 0 THEN @numAssignedTo ELSE NULL END),(CASE WHEN @numAssignedTo>0 THEN @numUserCntID ELSE NULL END),
			@DealStatus,@numStatus,@vcOppRefOrderNo,@bitStockTransfer,@bitBillingTerms,@intBillingDays,@bitInterestType,@fltInterest,
			@vcCouponCode,@bitDiscountType,@fltDiscount,1,@vcMarketplaceOrderID,@vcMarketplaceOrderReportId,@numPercentageComplete,
			@numAccountClass,@tintTaxOperator,@bitUseShippersAccountNo,@bitUseMarkupShippingRate,@numMarkupShippingRate,@intUsedShippingCompany,@numShipmentMethod,@numPartner,@numPartenerContactId,@dtReleaseDate,@bitIsInitalSalesOrder,@numVendorAddressID
		)                                                                                                                      
		
		SET @numOppID=(SELECT SCOPE_IDENTITY())                
                            
		
		--UPDATE OPPNAME AS PER NAME TEMPLATE
		EXEC dbo.USP_UpdateNameTemplateValue @tintOppType,@numDomainID,@numOppID

		--MAP CUSTOM FIELD	
		DECLARE @tintPageID AS TINYINT;
		SET @tintPageID = CASE WHEN @tintOppType=1 THEN 2 ELSE 6 END 
  	
		EXEC dbo.USP_AddParentChildCustomFieldMap
				@numDomainID = @numDomainID, --  numeric(18, 0)
				@numRecordID = @numOppID, --  numeric(18, 0)
				@numParentRecId = @numDivisionId, --  numeric(18, 0)
				@tintPageID = @tintPageID --  tinyint
 	
		IF ISNULL(@numStatus,0) > 0 AND @tintOppType=1 AND isnull(@DealStatus,0)=1
		BEGIN
	   		EXEC USP_ManageOpportunityAutomationQueue
					@numOppQueueID = 0, -- numeric(18, 0)
					@numDomainID = @numDomainID, -- numeric(18, 0)
					@numOppId = @numOppID, -- numeric(18, 0)
					@numOppBizDocsId = 0, -- numeric(18, 0)
					@numOrderStatus = @numStatus, -- numeric(18, 0)
					@numUserCntID = @numUserCntID, -- numeric(18, 0)
					@tintProcessStatus = 1, -- tinyint
					@tintMode = 1 -- TINYINT
		END
  
		-- INSERTING ITEMS                                                                                                                    
		IF CONVERT(VARCHAR(10),@strItems) <> '' AND @numOppID > 0
		BEGIN
			EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems       
	
			INSERT INTO OpportunityItems
			(
				numOppId,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,bitDropShip,bitDiscountType,fltDiscount,
				monTotAmtBefDiscount,vcItemName,vcModelID,vcManufacturer,vcPathForTImage,monVendorCost,numUOMId,bitWorkOrder,numVendorWareHouse,
				numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,numClassID,numToWarehouseItemID,vcAttributes,vcAttrValues,monAvgCost,bitItemPriceApprovalRequired,
				numPromotionID,bitPromotionTriggered,vcPromotionDetail,numSortOrder,numCost,vcNotes
			)
			SELECT 
				@numOppID,X.numItemCode,dbo.fn_UOMConversion(X.numUOM,numItemCode,@numDomainId,null) * x.numUnitHour,x.monPrice,x.monTotAmount,X.vcItemDesc,
				(CASE X.numWarehouseItmsID 
				WHEN -1 
				THEN 
					CASE 
						WHEN ((SELECT COUNT(*) FROM Item WHERE numDomainID=@numDomainId AND vcSKU= X.vcSKU) > 0)
							THEN
								(SELECT TOP 1 
									numWareHouseItemID 
								FROM 
									WareHouseItems 
								WHERE 
									[numItemID] = X.numItemCode 
									AND numWareHouseID IN (SELECT TOP 1 ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail WHERE numDomainID =@numDomainId AND WebApiId = @WebApiId))
						ELSE
							(SELECT 
								[numWareHouseItemID] 
							FROM 
								[WareHouseItems] 
							WHERE 
								[numItemID] = X.numItemCode 
								AND ISNULL(WareHouseItems.vcWHSKU,'') = ISNULL(X.vcSKU,'')
								AND numWareHouseID IN (SELECT TOP 1 ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail WHERE numDomainID =@numDomainId AND WebApiId = @WebApiId)) 
					END
				WHEN 0 
				THEN 
					CASE 
					WHEN ((SELECT COUNT(*) FROM Item WHERE numDomainID=@numDomainId AND vcSKU=X.vcSKU) > 0)
						THEN
							(SELECT TOP 1 
								numWareHouseItemID 
							FROM 
								WareHouseItems 
							WHERE 
								[numItemID] = X.numItemCode 
								AND numWareHouseID IN (SELECT TOP 1 ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail WHERE numDomainID =@numDomainId AND WebApiId = @WebApiId))
					ELSE
						(SELECT 
							[numWareHouseItemID] 
						FROM 
							[WareHouseItems] 
						WHERE 
							[numItemID] = X.numItemCode 
							AND ISNULL(WareHouseItems.vcWHSKU,'') = ISNULL(X.vcSKU,'')
							AND numWareHouseID IN (SELECT TOP 1 ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail WHERE numDomainID =@numDomainId AND WebApiId = @WebApiId)) 
					END
				ELSE  
					X.numWarehouseItmsID 
				END) AS numWarehouseItmsID,
				X.ItemType,X.DropShip,X.bitDiscountType,X.fltDiscount,X.monTotAmtBefDiscount,X.vcItemName,(SELECT vcModelID FROM item WHERE numItemCode = X.numItemCode),
				(SELECT vcManufacturer FROM item WHERE numItemCode = X.numItemCode),(SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = X.numItemCode AND BitDefault = 1 AND numdomainId = @numdomainId),
				(SELECT isnull(VN.monCost,0) FROM Vendor VN INNER JOIN Item IT ON IT.numItemCode=VN.numItemCode AND VN.numVendorID=IT.numVendorID WHERE  VN.numItemCode=X.numItemCode),
				X.numUOM,X.bitWorkOrder,X.numVendorWareHouse,X.numShipmentMethod,X.numSOVendorId,X.numProjectID,X.numProjectStageID,
				0,
				X.numToWarehouseItemID,X.Attributes,X.AttributeIDs,(SELECT (CASE WHEN ISNULL(I.bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(I.monAverageCost,0) END) FROM Item I WHERE I.numDomainId=@numDomainId AND I.numItemCode=X.numItemCode),
				X.bitItemPriceApprovalRequired,X.numPromotionID,X.bitPromotionTriggered,X.vcPromotionDetail,X.numSortOrder,ISNULL(X.numCost,0),ISNULL(X.vcVendorNotes,'')
			FROM
				(
					SELECT 
						*
					FROM 
						OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=1]',2)                                                                          
					WITH                       
					(                                                                          
						numoppitemtCode NUMERIC(9),numItemCode NUMERIC(9), numUnitHour FLOAT, monPrice DECIMAL(30,16), monTotAmount MONEY,vcItemDesc VARCHAR(1000),                                    
						numWarehouseItmsID NUMERIC(9), ItemType VARCHAR(30), DropShip BIT, bitDiscountType BIT,	fltDiscount DECIMAL(30,16), monTotAmtBefDiscount MONEY,
						vcItemName VARCHAR(300),numUOM NUMERIC(9),bitWorkOrder BIT,numVendorWareHouse NUMERIC(9),numShipmentMethod NUMERIC(9),numSOVendorId NUMERIC(9),
						numProjectID numeric(9),numProjectStageID numeric(9),numToWarehouseItemID NUMERIC(9),Attributes varchar(500),AttributeIDs VARCHAR(500),vcSKU VARCHAR(100),bitItemPriceApprovalRequired BIT,
						numPromotionID NUMERIC(18,0),bitPromotionTriggered BIT,vcPromotionDetail VARCHAR(2000),numSortOrder NUMERIC(18,0),numCost MONEY,vcVendorNotes VARCHAR(300)
					)
				)X    
			ORDER BY 
				X.numoppitemtCode
    
			UPDATE 
				OpportunityItems                       
			SET 
				numItemCode=X.numItemCode,numOppId=@numOppID,numUnitHour=dbo.fn_UOMConversion(X.numUOM,X.numItemCode,@numDomainId,null) * X.numUnitHour,                      
				monPrice=x.monPrice,monTotAmount=x.monTotAmount,vcItemDesc=X.vcItemDesc,numWarehouseItmsID=X.numWarehouseItmsID,bitDropShip=X.DropShip,
				bitDiscountType=X.bitDiscountType,fltDiscount=X.fltDiscount,monTotAmtBefDiscount=X.monTotAmtBefDiscount,vcItemName=X.vcItemName,numUOMId=numUOM,
				bitWorkOrder=X.bitWorkOrder,numVendorWareHouse=X.numVendorWareHouse,numShipmentMethod=X.numShipmentMethod,numSOVendorId=X.numSOVendorId,
				numProjectId=X.numProjectID,numProjectStageId=X.numProjectStageID,numToWarehouseItemID = X.numToWarehouseItemID,vcAttributes=X.Attributes,
				numClassID=0,vcModelID=X.vcModelID,
				bitItemPriceApprovalRequired = X.bitItemPriceApprovalRequired,vcAttrValues=X.AttributeIDs,
				numPromotionID=X.numPromotionID,bitPromotionTriggered=X.bitPromotionTriggered,vcPromotionDetail=X.vcPromotionDetail,numSortOrder=X.numSortOrder,numCost=X.numCost,vcNotes=X.vcVendorNotes
			FROM 
			(
				SELECT 
					numoppitemtCode as numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,vcModelID,numWarehouseItmsID,DropShip,bitDiscountType,fltDiscount,
					monTotAmtBefDiscount,vcItemName,numUOM,bitWorkOrder,numVendorWareHouse,numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,numToWarehouseItemID,
					Attributes,bitItemPriceApprovalRequired,AttributeIDs,numPromotionID,bitPromotionTriggered,vcPromotionDetail,numSortOrder,ISNULL(numCost,0) numCost,ISNULL(vcVendorNotes,'') vcVendorNotes
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=2]',2)
				WITH  
					(                      
						numoppitemtCode NUMERIC(9),numItemCode NUMERIC(9),numUnitHour FLOAT,monPrice DECIMAL(30,16),monTotAmount MONEY,vcItemDesc VARCHAR(1000), vcModelID VARCHAR(300),
						numWarehouseItmsID NUMERIC(9),DropShip BIT,bitDiscountType BIT,fltDiscount DECIMAL(30,16),monTotAmtBefDiscount MONEY,vcItemName VARCHAR(300),
						numUOM NUMERIC(9),bitWorkOrder BIT,numVendorWareHouse NUMERIC(9),numShipmentMethod NUMERIC(9),numSOVendorId NUMERIC(9),numProjectID NUMERIC(9),
						numProjectStageID numeric(9),numToWarehouseItemID NUMERIC(9),Attributes varchar(500),bitItemPriceApprovalRequired BIT,AttributeIDs VARCHAR(500),
						numPromotionID NUMERIC(18,0),bitPromotionTriggered BIT,vcPromotionDetail VARCHAR(2000),numSortOrder NUMERIC(18,0),numCost MONEY,vcVendorNotes VARCHAR(300)
					)
			)X 
			WHERE 
				numoppitemtCode=X.numOppItemID AND numOppID=@numOppID
				
			--INSERT KIT ITEMS                 
			INSERT INTO OpportunityKitItems
			(
				numOppId,
				numOppItemID,
				numChildItemID,
				numWareHouseItemId,
				numQtyItemsReq,
				numQtyItemsReq_Orig,
				numUOMId,
				numQtyShipped,
				monAvgCost
			)
			SELECT
				@numOppId,
				OI.numoppitemtCode,
				ID.numChildItemID,
				ID.numWareHouseItemId,
				(ID.numQtyItemsReq * ISNULL(dbo.fn_UOMConversion(ID.numUOMId,ID.numChildItemID,@numDomainID,I.numBaseUnit),1)) * OI.numUnitHour,
				ID.numQtyItemsReq,
				ID.numUOMId,
				0,
				ISNULL(I.monAverageCost,0)
			FROM 
				OpportunityItems OI 
			JOIN 
				ItemDetails ID 
			ON 
				OI.numItemCode=ID.numItemKitID 
			JOIN
				Item I
			ON
				ID.numChildItemID = I.numItemCode
			WHERE 
				OI.numOppId=@numOppId 

			IF (SELECT COUNT(*) FROM OPENXML (@hDocItem,'/NewDataSet/Item[bitHasKitAsChild=''true'']',2)) > 0
			BEGIN
				--INSERT KIT ITEM HAS ANOTHER KIT AS CHILD THEN SAVE CONFIGURATION OF KIT
				DECLARE @TempKitConfiguration TABLE
				(
					numItemCode NUMERIC(18,0),
					numWarehouseItemID NUMERIC(18,0),
					KitChildItems VARCHAR(MAX)
				)

				INSERT INTO 
					@TempKitConfiguration
				SELECT
					numItemCode,
					numWarehouseItmsID,
					KitChildItems
				FROM
					OPENXML (@hDocItem,'/NewDataSet/Item[bitHasKitAsChild=''true'']',2)
				WITH                       
				(
					numItemCode NUMERIC(9),numWarehouseItmsID NUMERIC(9), KitChildItems VARCHAR(MAX)
				) 

				INSERT INTO OpportunityKitChildItems
				(
					numOppID,
					numOppItemID,
					numOppChildItemID,
					numItemID,
					numWareHouseItemId,
					numQtyItemsReq,
					numQtyItemsReq_Orig,
					numUOMId,
					numQtyShipped,
					monAvgCost
				)
				SELECT 
					@numOppID,
					OKI.numOppItemID,
					OKI.numOppChildItemID,
					ItemDetails.numChildItemID,
					ItemDetails.numWareHouseItemId,
					(ItemDetails.numQtyItemsReq * ISNULL(dbo.fn_UOMConversion(ItemDetails.numUOMId,ItemDetails.numChildItemID,@numDomainID,Item.numBaseUnit),1)) * OKI.numQtyItemsReq,
					ItemDetails.numQtyItemsReq,
					ItemDetails.numUOMId,
					0,
					ISNULL(Item.monAverageCost,0)
				FROM
					(
						SELECT 
							t1.numItemCode,
							t1.numWarehouseItemID,
							SUBSTRING(
										SUBSTRING(items,0,CHARINDEX('-',items)),
										0,
										CHARINDEX('#',SUBSTRING(items,0,CHARINDEX('-',items)))
									  ) AS numKitItemID,
							SUBSTRING(
										SUBSTRING(items,0,CHARINDEX('-',items)),
										CHARINDEX('#',SUBSTRING(items,0,CHARINDEX('-',items))) + 1, 
										LEN(SUBSTRING(items,0,CHARINDEX('-',items))) - CHARINDEX('#',SUBSTRING(items,0,CHARINDEX('-',items)))
									  ) AS numKitWarehouseItemID,
							SUBSTRING(
										SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)), 
										0, 
										CHARINDEX('#',SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)))
									  ) As numKitChildItemID,
							SUBSTRING(
										SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)), 
										CHARINDEX('#',SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items))) + 1, 
										LEN(SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items))) - CHARINDEX('#',SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)))
									  ) As numKitChildItemWarehouseItemID
						FROM 
							@TempKitConfiguration t1
						CROSS APPLY
						(
							SELECT
								*
							FROM 
								dbo.Split((SELECT KitChildItems FROM @TempKitConfiguration WHERE numItemCode=t1.numItemCode AND numWarehouseItemID=t1.numWarehouseItemID),',') 
						) as t2
					) TempChildItems
				INNER JOIN
					OpportunityKitItems OKI
				ON
					OKI.numOppId=@numOppId
					AND OKI.numChildItemID=TempChildItems.numKitItemID
					AND OKI.numWarehouseItemId=TempChildItems.numKitWarehouseItemID
				INNER JOIN
					OpportunityItems OI
				ON
					OI.numItemCode = TempChildItems.numItemCode
					AND OI.numWarehouseItmsID = TempChildItems.numWarehouseItemID
					AND OI.numoppitemtcode = OKI.numOppItemID
				INNER JOIN
					ItemDetails
				ON
					TempChildItems.numKitItemID = ItemDetails.numItemKitID
					AND TempChildItems.numKitChildItemID = ItemDetails.numChildItemID
					AND (TempChildItems.numKitChildItemWarehouseItemID = ItemDetails.numWareHouseItemId  OR ISNULL(TempChildItems.numKitChildItemWarehouseItemID,0) = 0)
				INNER JOIN
					Item 
				ON
					ItemDetails.numChildItemID = Item.numItemCode
			END

			-- UPDATE UOM OF OPPORTUNITY ITEMS HAVE NOT UOM SET WHILE EXPORTED FROM MARKETPLACE (MEANS TINTSOURCETYPE = 3)
			IF ISNULL(@tintSourceType,0) = 3
			BEGIN
				UPDATE 
					OI 
				SET 
					numUOMID = ISNULL(I.numSaleUnit,0), 
					numUnitHour = dbo.fn_UOMConversion(ISNULL(I.numSaleUnit,0),OI.numItemCode,@numDomainId,null) * OI.numUnitHour
				FROM 
					OpportunityItems OI
				JOIN 
					Item I 
				ON 
					OI.numItemCode = I.numItemCode
				WHERE 
					numOppId = @numOppID
					AND I.numDomainID = @numDomainId
			END	
                                     
			INSERT INTO OppWarehouseSerializedItem
			(
				numOppID,
				numWarehouseItmsDTLID,
				numOppItemID,
				numWarehouseItmsID
			)                      
			SELECT 
				@numOppID,
				X.numWarehouseItmsDTLID,
				X.numoppitemtCode,
				X.numWItmsID 
			FROM
			(                                                                          
				SELECT
					* 
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/SerialNo',2)                                                                          
				WITH                        
				(                                               
				numWarehouseItmsDTLID numeric(9),                      
				numoppitemtCode numeric(9),                      
				numWItmsID numeric(9)                                                     
				)
			)X                                    
                                   
			UPDATE 
				OppWarehouseSerializedItem                       
			SET 
				numOppItemID=X.numoppitemtCode                                
			FROM                       
			(
				SELECT 
					numWarehouseItmsDTLID AS DTLID,
					O.numoppitemtCode 
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/SerialNo',2)                                                                         
				WITH
				(
					numWarehouseItmsDTLID NUMERIC(9),numoppitemtCode NUMERIC(9),numWItmsID NUMERIC(9)
				)                                
				JOIN 
					OpportunityItems O 
				ON 
					O.numWarehouseItmsID=numWItmsID and O.numOppID=@numOppID
			)X                 
			WHERE 
				numOppID=@numOppID 
				AND numWarehouseItmsDTLID=X.DTLID      
    
			EXEC USP_ManageSOWorkOrder @numDomainId,@numOppID,@strItems,@numUserCntID
                     
			EXEC sp_xml_removedocument @hDocItem                                     
		END          
	END
	ELSE                                                                                                                          
	BEGIN
		DECLARE @tempAssignedTo AS NUMERIC(9) = NULL                                                    
		SELECT 
			@tintOppStatus = tintOppStatus,
			@tempAssignedTo=ISNULL(numAssignedTo,0) 
		FROM   
			OpportunityMaster 
		WHERE  
			numOppID = @numOppID

		IF @tempAssignedTo <> @numAssignedTo
		BEGIN
			IF (SELECT COUNT(*) FROM BizDocComission WHERE ISNULL(numOppID,0) = @numOppID AND ISNULL(bitCommisionPaid,0) = 1) > 0
			BEGIN
				RAISERROR('COMMISSION_PAID_AGAINST_BIZDOC',16,1)
			END
		END
 
		--REVERTING BACK THE WAREHOUSE ITEMS                  
		IF @tintOppStatus = 1 
		BEGIN               
			EXEC USP_RevertDetailsOpp @numOppID,0,@numUserCntID                  
		END                  

		IF LEN(ISNULL(@vcOppRefOrderNo,''))>0
		BEGIN
			UPDATE dbo.OpportunityBizDocs SET vcRefOrderNo=@vcOppRefOrderNo WHERE numOppId=@numOppID
		END
		
		IF EXISTS (SELECT 1 FROM dbo.OpportunityMaster WHERE numOppId = @numOppID AND tintOppType=1 AND isnull(tintOppStatus,0)=1 AND ISNULL(numStatus,0)!= @numStatus)
		BEGIN
			EXEC USP_ManageOpportunityAutomationQueue
					@numOppQueueID = 0, -- numeric(18, 0)
					@numDomainID = @numDomainID, -- numeric(18, 0)
					@numOppId = @numOppID, -- numeric(18, 0)
					@numOppBizDocsId = 0, -- numeric(18, 0)
					@numOrderStatus = @numStatus, -- numeric(18, 0)
					@numUserCntID = @numUserCntID, -- numeric(18, 0)
					@tintProcessStatus = 1, -- tinyint
					@tintMode = 1 -- TINYINT
		END 
						
		UPDATE   
			OpportunityMaster
		SET      
			vcPOppName = @vcPOppName,txtComments = ISNULL(@Comments,''),bitPublicFlag = @bitPublicFlag,numCampainID = @CampaignID,tintSource = @tintSource,tintSourceType=@tintSourceType,
			intPEstimatedCloseDate = @dtEstimatedCloseDate,numModifiedBy = @numUserCntID,bintModifiedDate = GETUTCDATE(),lngPConclAnalysis = @lngPConclAnalysis,monPAmount = @monPAmount,
			tintActive = @tintActive,numSalesOrPurType = @numSalesOrPurType,numStatus = @numStatus,vcOppRefOrderNo=@vcOppRefOrderNo,
			bintOppToOrder=(CASE WHEN @tintOppStatus=0 AND @DealStatus=1 THEN GETUTCDATE() ELSE bintOppToOrder END),bitDiscountType=@bitDiscountType,fltDiscount=@fltDiscount,
			bitBillingTerms=@bitBillingTerms,intBillingDays=@intBillingDays,bitInterestType=@bitInterestType,fltInterest=@fltInterest,tintTaxOperator=@tintTaxOperator,
			numDiscountAcntType=@numDiscountAcntType,vcCouponCode = @vcCouponCode,numPercentageComplete =@numPercentageComplete,bitUseShippersAccountNo = @bitUseShippersAccountNo,
			bitUseMarkupShippingRate = @bitUseMarkupShippingRate,numMarkupShippingRate = @numMarkupShippingRate,intUsedShippingCompany = @intUsedShippingCompany, numContactID=@numContactId,numShipmentMethod=@numShipmentMethod,numPartner=@numPartner,numPartenerContact=@numPartenerContactId
		WHERE
			numOppId = @numOppID   
	   
		-- UPDATING IF ORGANIZATION IS ASSIGNED TO SOMEONE                                                      
		IF ( @tempAssignedTo <> @numAssignedTo AND @numAssignedTo <> '0' ) 
		BEGIN
			UPDATE
				OpportunityMaster 
			SET	
				numAssignedTo = @numAssignedTo, 
				numAssignedBy = @numUserCntID 
			WHERE 
				numOppId = @numOppID                                                    
		END                                                     
		ELSE IF(@numAssignedTo = 0) 
		BEGIN
			UPDATE 
				OpportunityMaster 
			SET 
				numAssignedTo = 0, 
				numAssignedBy = 0 
			WHERE
				numOppId = @numOppID
		END  
			                                                                    
		-- UPDATING OPP ITEMS
		IF CONVERT(VARCHAR(10),@strItems) <> '' AND @numOppID > 0                                                                         
		BEGIN
			EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems                                      
			   
			IF EXISTS 
			(
				SELECT 
					OI.numoppitemtCode
				FROM 
					OpportunityItems OI
				WHERE 
					numOppID=@numOppID 
					AND OI.numoppitemtCode NOT IN (SELECT numoppitemtCode FROM OPENXML (@hDocItem,'/NewDataSet/Item',2) WITH (numoppitemtCode NUMERIC(9)))
					AND ISNULL((SELECT COUNT(*) FROM OppWarehouseSerializedItem WHERE numOppId=@numOppID AND numOppItemID=OI.numoppitemtCode),0) > 0
			)
			BEGIN
				RAISERROR('SERIAL/LOT#_ASSIGNED_TO_ITEM_DELETED',16,1)
			END

			--DELETE CHILD ITEMS OF KIT WITHIN KIT
			DELETE FROM 
				OpportunityKitChildItems 
			WHERE 
				numOppID=@numOppID AND 
				numOppItemID NOT IN (SELECT ISNULL(numoppitemtCode,0) FROM OPENXML (@hDocItem,'/NewDataSet/Item',2) WITH (numoppitemtCode NUMERIC(9)))   

			--DELETE ITEMS
			DELETE FROM 
				OpportunityKitItems 
			WHERE 
				numOppID=@numOppID AND 
				numOppItemID NOT IN (SELECT ISNULL(numoppitemtCode,0) FROM OPENXML (@hDocItem,'/NewDataSet/Item',2) WITH (numoppitemtCode NUMERIC(9)))                      
			                            
			DELETE FROM 
				OpportunityItems 
			WHERE 
				numOppID=@numOppID 
				AND numoppitemtCode NOT IN (SELECT numoppitemtCode FROM OPENXML (@hDocItem,'/NewDataSet/Item',2) WITH (numoppitemtCode NUMERIC(9)))   
	          
			--INSERT NEW ADDED ITEMS                        
			INSERT INTO OpportunityItems
			(
				numOppId,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount,
				[vcItemName],[vcModelID],[vcManufacturer],vcPathForTImage,monVendorCost,numUOMId,bitWorkOrder,numVendorWareHouse,numToWarehouseItemID,numShipmentMethod,
				numSOVendorId,numProjectID,numProjectStageID,vcAttributes,numClassID,monAvgCost,bitItemPriceApprovalRequired,vcAttrValues,numPromotionID,bitPromotionTriggered,vcPromotionDetail,numSortOrder,numCost,vcNotes
			)
			SELECT 
				@numOppID,X.numItemCode,dbo.fn_UOMConversion(X.numUOM,numItemCode,@numDomainId,null) * x.numUnitHour,x.monPrice,x.monTotAmount,X.vcItemDesc,X.numWarehouseItmsID,
				X.ItemType,X.DropShip,X.bitDiscountType,X.fltDiscount,X.monTotAmtBefDiscount,X.vcItemName,(SELECT vcModelID FROM item WHERE numItemCode = X.numItemCode),
				(SELECT vcManufacturer FROM item WHERE numItemCode = X.numItemCode),
				(SELECT TOP 1 vcPathForTImage FROM dbo.ItemImages WHERE numItemCode = X.numItemCode AND BitDefault = 1 AND numDomainId = @numDomainId),
				(select isnull(VN.monCost,0) from Vendor VN INNER JOIN Item IT ON IT.numItemCode=VN.numItemCode and VN.numVendorID=IT.numVendorID where VN.numItemCode=X.numItemCode),
				X.numUOM,X.bitWorkOrder,X.numVendorWareHouse,X.numToWarehouseItemID,X.numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,Attributes,
				0,
				(SELECT (CASE WHEN ISNULL(I.bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(I.monAverageCost,0) END) FROM Item I WHERE I.numDomainId=@numDomainId AND I.numItemCode=X.numItemCode), X.bitItemPriceApprovalRequired, X.AttributeIDs,
				X.numPromotionID,X.bitPromotionTriggered,X.vcPromotionDetail,X.numSortOrder,ISNULL(X.numCost,0),ISNULL(vcVendorNotes,'')
			FROM
			(
				SELECT 
					*
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=1]',2)                                                                          
				WITH  
				(                      
					numoppitemtCode numeric(9),numItemCode numeric(9),numUnitHour FLOAT,monPrice DECIMAL(30,16),monTotAmount money,vcItemDesc varchar(1000),                                    
					numWarehouseItmsID numeric(9),numToWarehouseItemID numeric(9),Op_Flag tinyint,ItemType varchar(30),DropShip bit,bitDiscountType bit,
					fltDiscount DECIMAL(30,16),monTotAmtBefDiscount MONEY,vcItemName VARCHAR(300),numUOM NUMERIC(9),bitWorkOrder BIT,numVendorWareHouse NUMERIC(9),
					numShipmentMethod NUMERIC(9),numSOVendorId numeric(9),numProjectID numeric(9),numProjectStageID numeric(9),Attributes varchar(500), 
					bitItemPriceApprovalRequired BIT,AttributeIDs VARCHAR(500),numPromotionID NUMERIC(18,0),bitPromotionTriggered BIT,vcPromotionDetail VARCHAR(2000)
					,numSortOrder NUMERIC(18,0),numCost MONEY,vcVendorNotes VARCHAR(300)
				)
			)X 
			
			-- UPDATE ITEMS
			UPDATE 
				OpportunityItems                       
			SET 
				numItemCode=X.numItemCode,numOppId=@numOppID,numUnitHour=dbo.fn_UOMConversion(X.numUOM,X.numItemCode,@numDomainId,null) * x.numUnitHour,                      
				monPrice=x.monPrice,monTotAmount=x.monTotAmount,vcItemDesc=X.vcItemDesc,vcModelID=X.vcModelID,numWarehouseItmsID=X.numWarehouseItmsID,numToWarehouseItemID = X.numToWarehouseItemID,             
				bitDropShip=X.DropShip,bitDiscountType=X.bitDiscountType,fltDiscount=X.fltDiscount,monTotAmtBefDiscount=X.monTotAmtBefDiscount,vcItemName=X.vcItemName,
				numUOMId=numUOM,numVendorWareHouse=X.numVendorWareHouse,numShipmentMethod=X.numShipmentMethod,numSOVendorId=X.numSOVendorId,numProjectId=X.numProjectID,
				numProjectStageId=X.numProjectStageID,vcAttributes=X.Attributes,bitItemPriceApprovalRequired=X.bitItemPriceApprovalRequired,bitWorkOrder=X.bitWorkOrder,vcAttrValues=AttributeIDs,
				numPromotionID=X.numPromotionID,bitPromotionTriggered=X.bitPromotionTriggered,vcPromotionDetail=X.vcPromotionDetail,numSortOrder=X.numSortOrder,numCost=X.numCost,vcNotes=vcVendorNotes
			FROM 
			(
				SELECT 
					numoppitemtCode as numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,vcModelID,numWarehouseItmsID,numToWarehouseItemID,DropShip,bitDiscountType,
					fltDiscount,monTotAmtBefDiscount,vcItemName,numUOM,numVendorWareHouse,numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,Attributes,
					bitItemPriceApprovalRequired,bitWorkOrder,AttributeIDs,numPromotionID,bitPromotionTriggered,vcPromotionDetail,numSortOrder,ISNULL(numCost,0) AS numCost,ISNULL(vcVendorNotes,'') vcVendorNotes
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=2]',2)                                                                
				WITH  
				(                      
					numoppitemtCode NUMERIC(9),numItemCode NUMERIC(9),numUnitHour FLOAT,monPrice DECIMAL(30,16),monTotAmount MONEY,vcItemDesc VARCHAR(1000),vcModelID VARCHAR(300), numWarehouseItmsID NUMERIC(9), 
					numToWarehouseItemID NUMERIC(18,0),DropShip BIT,bitDiscountType BIT,fltDiscount DECIMAL(30,16),monTotAmtBefDiscount MONEY,vcItemName VARCHAR(300),numUOM NUMERIC(9),
					numVendorWareHouse NUMERIC(9),numShipmentMethod NUMERIC(9),numSOVendorId NUMERIC(9),numProjectID NUMERIC(9),numProjectStageID NUMERIC(9),Attributes VARCHAR(500),
					bitItemPriceApprovalRequired BIT, bitWorkOrder BIT,AttributeIDs VARCHAR(500),numPromotionID NUMERIC(18,0),bitPromotionTriggered BIT,vcPromotionDetail VARCHAR(2000),numSortOrder NUMERIC(18,0),numCost MONEY,vcVendorNotes VARCHAR(300)                           
				)
			)X 
			WHERE 
				numoppitemtCode=X.numOppItemID AND numOppId=@numOppID

			--INSERT NEW ADDED KIT ITEMS                 
			INSERT INTO OpportunityKitItems                                                                          
			(
				numOppId,
				numOppItemID,
				numChildItemID,
				numWareHouseItemId,
				numQtyItemsReq,
				numQtyItemsReq_Orig,
				numUOMId,
				numQtyShipped,
				monAvgCost
			)
			SELECT
				@numOppId,
				OI.numoppitemtCode,
				ID.numChildItemID,
				ID.numWareHouseItemId,
				ID.numQtyItemsReq * OI.numUnitHour,
				ID.numQtyItemsReq,
				ID.numUOMId,
				0,
				ISNULL(I.monAverageCost,0)
			FROM 
				OpportunityItems OI 
			JOIN 
				ItemDetails ID 
			ON 
				OI.numItemCode=ID.numItemKitID
			INNER JOIN
				Item I
			ON
				ID.numChildItemID = I.numItemCode
			WHERE 
				OI.numOppId=@numOppId 
				AND OI.numoppitemtCode NOT IN (SELECT ISNULL(numOppItemID,0) FROM OpportunityKitItems OKI WHERE OKI.numOppId=@numOppID)  
				
			IF (SELECT COUNT(*) FROM OPENXML (@hDocItem,'/NewDataSet/Item[bitHasKitAsChild=''true'']',2)) > 0
			BEGIN
				--INSERT KIT ITEM HAS ANOTHER KIT AS CHILD THEN SAVE CONFIGURATION OF KIT
				DELETE FROM @TempKitConfiguration

				INSERT INTO 
					@TempKitConfiguration
				SELECT
					numItemCode,
					numWarehouseItmsID,
					KitChildItems
				FROM
					OPENXML (@hDocItem,'/NewDataSet/Item[bitHasKitAsChild=''true'']',2)
				WITH                       
				(
					numItemCode NUMERIC(9),numWarehouseItmsID NUMERIC(9), KitChildItems VARCHAR(MAX)
				) 

				INSERT INTO OpportunityKitChildItems
				(
					numOppID,
					numOppItemID,
					numOppChildItemID,
					numItemID,
					numWareHouseItemId,
					numQtyItemsReq,
					numQtyItemsReq_Orig,
					numUOMId,
					numQtyShipped,
					monAvgCost
				)
				SELECT 
					@numOppID,
					OKI.numOppItemID,
					OKI.numOppChildItemID,
					ItemDetails.numChildItemID,
					ItemDetails.numWareHouseItemId,
					ItemDetails.numQtyItemsReq * OKI.numQtyItemsReq,
					ItemDetails.numQtyItemsReq,
					ItemDetails.numUOMId,
					0,
					ISNULL(Item.monAverageCost,0)
				FROM
					(
						SELECT 
							t1.numItemCode,
							t1.numWarehouseItemID,
							SUBSTRING(
										SUBSTRING(items,0,CHARINDEX('-',items)),
										0,
										CHARINDEX('#',SUBSTRING(items,0,CHARINDEX('-',items)))
									  ) AS numKitItemID,
							SUBSTRING(
										SUBSTRING(items,0,CHARINDEX('-',items)),
										CHARINDEX('#',SUBSTRING(items,0,CHARINDEX('-',items))) + 1, 
										LEN(SUBSTRING(items,0,CHARINDEX('-',items))) - CHARINDEX('#',SUBSTRING(items,0,CHARINDEX('-',items)))
									  ) AS numKitWarehouseItemID,
							SUBSTRING(
										SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)), 
										0, 
										CHARINDEX('#',SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)))
									  ) As numKitChildItemID,
							SUBSTRING(
										SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)), 
										CHARINDEX('#',SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items))) + 1, 
										LEN(SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items))) - CHARINDEX('#',SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)))
									  ) As numKitChildItemWarehouseItemID
						FROM 
							@TempKitConfiguration t1
						CROSS APPLY
						(
							SELECT
								*
							FROM 
								dbo.Split((SELECT KitChildItems FROM @TempKitConfiguration WHERE numItemCode=t1.numItemCode AND numWarehouseItemID=t1.numWarehouseItemID),',') 
						) as t2
					) TempChildItems
				INNER JOIN
					OpportunityKitItems OKI
				ON
					OKI.numOppId=@numOppId
					AND OKI.numChildItemID=TempChildItems.numKitItemID
					AND OKI.numWarehouseItemId=TempChildItems.numKitWarehouseItemID
				INNER JOIN
					OpportunityItems OI
				ON
					OI.numItemCode = TempChildItems.numItemCode
					AND OI.numWarehouseItmsID = TempChildItems.numWarehouseItemID
					AND OI.numoppitemtcode = OKI.numOppItemID
				INNER JOIN
					ItemDetails
				ON
					TempChildItems.numKitItemID = ItemDetails.numItemKitID
					AND TempChildItems.numKitChildItemID = ItemDetails.numChildItemID
					AND (TempChildItems.numKitChildItemWarehouseItemID = ItemDetails.numWareHouseItemId  OR ISNULL(TempChildItems.numKitChildItemWarehouseItemID,0) = 0)
				INNER JOIN
					Item 
				ON
					ItemDetails.numChildItemID = Item.numItemCode
			END                                  
			
			--UPDATE KIT ITEMS                 
			UPDATE 
				OKI 
			SET 
				numQtyItemsReq = numQtyItemsReq_Orig * OI.numUnitHour
			FROM 
				OpportunityKitItems OKI 
			JOIN 
				OpportunityItems OI 
			ON 
				OKI.numOppItemID=OI.numoppitemtCode 
				AND OKI.numOppId=OI.numOppId  
			WHERE 
				OI.numOppId=@numOppId 

			-- UPDATE CHILD ITEMS OF KIT WITHIN KIT
			UPDATE
				OKCI
			SET
				OKCI.numQtyItemsReq = (OKI.numQtyItemsReq * OKCI.numQtyItemsReq_Orig)
			FROM
				OpportunityKitChildItems OKCI
			INNER JOIN
				OpportunityKitItems OKI
			ON
				OKCI.numOppID = @numOppId
				AND OKCI.numOppItemID = OKI.numOppItemID
				AND OKCI.numOppChildItemID = OKI.numOppChildItemID                     
			                                    
			EXEC USP_ManageSOWorkOrder @numDomainId,@numOppID,@strItems,@numUserCntID
			   		                                                                      
			EXEC sp_xml_removedocument @hDocItem                                               
		END     

		--FOLLOWING DELETE STATEMENT IS COMMENTED BECUASE NOW WE ARE NOT ALLOWING USER TO EIDT SERIAL/LOT ITEM UNTILL USER REMOVED ASSIGNED SERIAL/LOT NUMBERS
	    --Delete previously added serial lot number added for opportunity
		--DELETE FROM OppWarehouseSerializedItem WHERE numOppID = @numOppID                                                         
	END


	SET @TotalAmount=0                                                           
	SELECT @TotalAmount = SUM(monTotAmount) FROM OpportunityItems WHERE numOppId=@numOppID                                                                          
  
	UPDATE OpportunityMaster SET monPamount=@TotalAmount WHERE numOppId=@numOppID

/*Added by chintan - While creating SO from PO it wasn't puting Item Qty to Allocation. this segment was being called from USP_OppSaveMilestone which No longer in use after changes anoop did. */     
--Updating the warehouse items              
            
			declare @bitApprovalforOpportunity bit
			declare @intOpportunityApprovalProcess int
			SELECT @bitApprovalforOpportunity=bitApprovalforOpportunity,@intOpportunityApprovalProcess=intOpportunityApprovalProcess 
					FROM Domain WHERE numDomainId=@numDomainID
			DECLARE @bitViolatePrice BIT
			DECLARE @intPendingApprovePending INT

			SET @bitIsInitalSalesOrder=(SELECT bitIsInitalSalesOrder FROM OpportunityMaster WHERE numOppId=@numOppID AND numDomainId=@numDomainID)
			SET @bitViolatePrice=(SELECT bitMarginPriceViolated FROM Domain WHERE numDomainId=@numDomainID)
			
			IF(@bitViolatePrice=1 AND @bitIsInitalSalesOrder=1 AND @tintClickBtn = 1)
			BEGIN
				UPDATE OpportunityMaster SET tintOppStatus=@DealStatus WHERE numOppId=@numOppID
			END
			ELSE
			BEGIN
				IF(@bitApprovalforOpportunity=1)
				BEGIN
					IF(@DealStatus='1' OR @DealStatus='2')
					BEGIN
						IF(@intOpportunityApprovalProcess=1)
						BEGIN
							UPDATE OpportunityMaster SET intPromotionApprovalStatus=6,intChangePromotionStatus=@DealStatus
							WHERE numOppId=@numOppId
						END
						IF(@intOpportunityApprovalProcess=2)
						BEGIN
							UPDATE OpportunityMaster SET intPromotionApprovalStatus=1,intChangePromotionStatus=@DealStatus
							WHERE numOppId=@numOppId
						END
					END
				END

				UPDATE OpportunityMaster SET tintOppStatus = @DealStatus WHERE numOppId=@numOppID
				declare @tintShipped as tinyint               
				select @tintOppStatus=tintOppStatus,@tintOppType=tintOppType,@tintShipped=tintShipped from OpportunityMaster where numOppID=@numOppID              
				if @tintOppStatus=1               
					begin         
					PRINT 'inside USP_UpdatingInventoryonCloseDeal'
						exec USP_UpdatingInventoryonCloseDeal @numOppID,@numUserCntID
					end  
				declare @tintCRMType as numeric(9)        
				declare @AccountClosingDate as datetime        

				select @numDivisionID=numDivisionID,@AccountClosingDate=bintAccountClosingDate from  OpportunityMaster where numOppID=@numOppID         
				select @tintCRMType=tintCRMType from divisionmaster where numDivisionID =@numDivisionID 

				--When deal is won                                         
				IF @DealStatus = 1 
				BEGIN
					IF @AccountClosingDate IS NULL                   
					BEGIN
						UPDATE OpportunityMaster SET bintAccountClosingDate=GETUTCDATE() WHERE numOppID=@numOppID              
					END

					-- Promote Lead to Account when Sales/Purchase Order is created against it
					IF @tintCRMType=0 --Lead & Order
					BEGIN
						UPDATE 
							divisionmaster 
						SET 
							tintCRMType=2
							,bintLeadProm=getutcdate()
							,bintLeadPromBy=@numUserCntID
							,bintProsProm=getutcdate()
							,bintProsPromBy=@numUserCntID        
						WHERE 
							numDivisionID=@numDivisionID        
					END
					--Promote Prospect to Account
					ELSE IF @tintCRMType=1 
					BEGIN
						UPDATE 
							divisionmaster 
						SET 
							tintCRMType=2
							,bintProsProm=getutcdate()
							,bintProsPromBy=@numUserCntID        
						WHERE 
							numDivisionID=@numDivisionID        
					END
				END         
				--When Deal is Lost
				ELSE IF @DealStatus = 2
				BEGIN
					SELECT @AccountClosingDate=bintAccountClosingDate FROM OpportunityMaster WHERE numOppID=@numOppID         
					
					IF @AccountClosingDate IS NULL                   
					BEGIN
						UPDATE OpportunityMaster SET bintAccountClosingDate=GETUTCDATE() WHERE numOppID=@numOppID
					END
				END 
			END

			/*Added by chintan -  Biz usually automatically promotes the record from Lead/Prospect to Account When a Sales Opp/Order is made against it*/  
			-- Promote Lead to Prospect when Sales/Purchase Opp is created against it
			if @tintCRMType=0 AND @DealStatus = 0 --Lead & Open Opp
			begin        
				update divisionmaster set tintCRMType=1,bintLeadProm=getutcdate(),bintLeadPromBy=@numUserCntID,bintProsProm=getutcdate(),bintProsPromBy=@numUserCntID        
				where numDivisionID=@numDivisionID        
			end        

--Add/Update Address Details
DECLARE @vcStreet  varchar(100),@vcCity  varchar(50),@vcPostalCode  varchar(15),@vcCompanyName  varchar(100),@vcAddressName VARCHAR(50)      
DECLARE @numState  numeric(9),@numCountry  numeric(9), @numCompanyId  numeric(9) 
DECLARE @bitIsPrimary BIT;
DECLARE @tintAddressOf TINYINT

select  @vcCompanyName=vcCompanyName,@numCompanyId=div.numCompanyID from CompanyInfo Com                            
join divisionMaster Div                            
on div.numCompanyID=com.numCompanyID                            
where div.numdivisionID=@numDivisionId

--Bill Address
IF @numBillAddressId>0
BEGIN
 SELECT  @vcStreet=ISNULL(vcStreet,''),@vcCity=ISNULL(vcCity,''),@vcPostalCode=ISNULL(vcPostalCode,''),
		@numState=ISNULL(numState,0),@numCountry=ISNULL(numCountry,0),@bitIsPrimary=bitIsPrimary,
		@vcAddressName=vcAddressName,@tintAddressOf=ISNULL(tintAddressOf,0)
            FROM  dbo.AddressDetails WHERE   numDomainID = @numDomainID AND numAddressID = @numBillAddressId

	IF @tintAddressOf = 2 AND @tintOppType = 2
	BEGIN
		SELECT
			@vcCompanyName = ISNULL(vcCompanyName,'')
			,@numCompanyId = ISNULL(CompanyInfo.numCompanyId,0)
		FROM
			AddressDetails
		INNER JOIN
			DivisionMaster 
		ON
			AddressDetails.numRecordID = DivisionMaster.numDivisionID
		INNER JOIN
			CompanyInfo
		ON
			DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
		WHERE 
			AddressDetails.numDomainID = @numDomainID 
			AND numAddressID = @numBillAddressId
	END

  	EXEC dbo.USP_UpdateOppAddress
	@numOppID = @numOppID, --  numeric(9, 0)
	@byteMode = 0, --  tinyint
	@vcStreet = @vcStreet, --  varchar(100)
	@vcCity = @vcCity, --  varchar(50)
	@vcPostalCode = @vcPostalCode, --  varchar(15)
	@numState = @numState, --  numeric(9, 0)
	@numCountry = @numCountry, --  numeric(9, 0)
	@vcCompanyName = @vcCompanyName, --  varchar(100)
	@numCompanyId = 0, --  numeric(9, 0)
	@vcAddressName = @vcAddressName,
	@bitCalledFromProcedure = 1
END
  
  
--Ship Address
IF @numShipmentMethod = 92 -- WILL CALL (PICK UP FROM Warehouse)
BEGIN
	SELECT  
		@vcStreet=ISNULL(vcWStreet,'')
		,@vcCity=ISNULL(vcWCity,'')
		,@vcPostalCode=ISNULL(vcWPinCode,'')
		,@numState=ISNULL(numWState,0)
		,@numCountry=ISNULL(numWCountry,0)
		,@vcAddressName=''
	FROM 
		dbo.Warehouses 
	WHERE 
		numDomainID = @numDomainID 
		AND numWareHouseID = @numWillCallWarehouseID

	UPDATE OpportunityMaster SET tintShipToType=2 WHERE numOppId=@numOppID
 
  	EXEC dbo.USP_UpdateOppAddress
	@numOppID = @numOppID, --  numeric(9, 0)
	@byteMode = 1, --  tinyint
	@vcStreet = @vcStreet, --  varchar(100)
	@vcCity = @vcCity, --  varchar(50)
	@vcPostalCode = @vcPostalCode, --  varchar(15)
	@numState = @numState, --  numeric(9, 0)
	@numCountry = @numCountry, --  numeric(9, 0)
	@vcCompanyName = '', --  varchar(100)
	@numCompanyId =0, --  numeric(9, 0)
	@vcAddressName = @vcAddressName,
	@bitCalledFromProcedure = 1
END
ELSE IF @numShipAddressId>0
BEGIN
	SELECT  @vcStreet=ISNULL(vcStreet,''),@vcCity=ISNULL(vcCity,''),@vcPostalCode=ISNULL(vcPostalCode,''),
		@numState=ISNULL(numState,0),@numCountry=ISNULL(numCountry,0),@bitIsPrimary=bitIsPrimary,
		@vcAddressName=vcAddressName,@tintAddressOf=ISNULL(tintAddressOf,0)
            FROM  dbo.AddressDetails WHERE   numDomainID = @numDomainID AND numAddressID = @numShipAddressId
 

	IF @tintAddressOf = 2 AND @tintOppType = 2
	BEGIN
		SELECT
			@vcCompanyName = ISNULL(vcCompanyName,'')
			,@numCompanyId = ISNULL(CompanyInfo.numCompanyId,0)
		FROM
			AddressDetails
		INNER JOIN
			DivisionMaster 
		ON
			AddressDetails.numRecordID = DivisionMaster.numDivisionID
		INNER JOIN
			CompanyInfo
		ON
			DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
		WHERE 
			AddressDetails.numDomainID = @numDomainID 
			AND numAddressID = @numShipAddressId
	END


  	EXEC dbo.USP_UpdateOppAddress
	@numOppID = @numOppID, --  numeric(9, 0)
	@byteMode = 1, --  tinyint
	@vcStreet = @vcStreet, --  varchar(100)
	@vcCity = @vcCity, --  varchar(50)
	@vcPostalCode = @vcPostalCode, --  varchar(15)
	@numState = @numState, --  numeric(9, 0)
	@numCountry = @numCountry, --  numeric(9, 0)
	@vcCompanyName = @vcCompanyName, --  varchar(100)
	@numCompanyId =@numCompanyId, --  numeric(9, 0)
	@vcAddressName = @vcAddressName,
	@bitCalledFromProcedure = 1
END

-- IMPORTANT:KEEP THIS BELOW ADDRESS UPDATE LOGIC
-- GET TAX APPLICABLE TO DIVISION WHEN ORDER IS CREATED AFTER THAT NOT NEED TO CHANGE IT
IF @bitNewOrder = 1
BEGIN
	--INSERT TAX FOR DIVISION   
	IF @tintOppType=1 OR (@tintOppType=2 and (select isnull(bitPurchaseTaxCredit,0) from Domain where numDomainId=@numDomainId)=1)
	BEGIN
		INSERT INTO dbo.OpportunityMasterTaxItems 
		(
			numOppId,
			numTaxItemID,
			fltPercentage,
			tintTaxType,
			numTaxID
		) 
		SELECT 
			@numOppID,
			TI.numTaxItemID,
			TEMPTax.decTaxValue,
			TEMPTax.tintTaxType,
			0
		FROM 
			TaxItems TI 
		JOIN 
			DivisionTaxTypes DTT 
		ON 
			TI.numTaxItemID = DTT.numTaxItemID
		CROSS APPLY
		(
			SELECT
				decTaxValue,
				tintTaxType
			FROM
				dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,TI.numTaxItemID,@numOppId,0,NULL)
		) AS TEMPTax
		WHERE 
			DTT.numDivisionID=@numDivisionId 
			AND DTT.bitApplicable=1
		UNION 
		SELECT 
			@numOppID,
			0,
			TEMPTax.decTaxValue,
			TEMPTax.tintTaxType,
			0
		FROM 
			dbo.DivisionMaster 
		CROSS APPLY
		(
			SELECT
				decTaxValue,
				tintTaxType
			FROM
				dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,0,@numOppId,1,NULL)
		) AS TEMPTax
		WHERE 
			bitNoTax=0 
			AND numDivisionID=@numDivisionID	
		UNION 
		SELECT
			@numOppId
			,1
			,decTaxValue
			,tintTaxType
			,numTaxID
		FROM
			dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,1,@numOppId,1,NULL)		
	END	
END

--INSERT TAX FOR DIVISION   
IF @tintOppType=1 OR (@tintOppType=2 and (select isnull(bitPurchaseTaxCredit,0) from Domain where numDomainId=@numDomainId)=1)
BEGIN
--Delete Tax for Opportunity Items if item deleted 
DELETE FROM OpportunityItemsTaxItems WHERE numOppId=@numOppId AND numOppItemID NOT IN (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId=@numOppID)

--Insert Tax for Opportunity Items
INSERT INTO dbo.OpportunityItemsTaxItems 
(
	numOppId,
	numOppItemID,
	numTaxItemID,
	numTaxID
) 
SELECT 
	@numOppId,
	OI.numoppitemtCode,
	TI.numTaxItemID,
	0
FROM 
	dbo.OpportunityItems OI 
JOIN 
	dbo.ItemTax IT 
ON 
	OI.numItemCode=IT.numItemCode 
JOIN
	TaxItems TI 
ON 
	TI.numTaxItemID = IT.numTaxItemID 
WHERE 
	OI.numOppId=@numOppID 
	AND IT.bitApplicable=1 
	AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)
UNION
SELECT 
	@numOppId,
	OI.numoppitemtCode,
	0,
	0
FROM 
	dbo.OpportunityItems OI 
JOIN 
	dbo.Item I 
ON 
	OI.numItemCode=I.numItemCode
WHERE 
	OI.numOppId=@numOppID 
	AND I.bitTaxable=1 
	AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)
UNION
SELECT
	@numOppId,
	OI.numoppitemtCode,
	1,
	TD.numTaxID
FROM
	dbo.OpportunityItems OI 
INNER JOIN
	ItemTax IT
ON
	IT.numItemCode = OI.numItemCode
INNER JOIN
	TaxDetails TD
ON
	TD.numTaxID = IT.numTaxID
	AND TD.numDomainId = @numDomainId
WHERE
	OI.numOppId = @numOppID
	AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)

END
  
 UPDATE OpportunityMaster SET monDealAmount=dbo.GetDealAmount(@numOppID,getdate(),0) WHERE numOppId=@numOppID

	IF @tintOppType=1 AND @tintOppStatus=1 
	BEGIN
		EXEC USP_SalesFulfillmentQueue_Save @numDomainID,@numUserCntID,@numOppID,@numStatus
	END
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SET @numOppID = 0

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityBizDocs_FulFill')
DROP PROCEDURE USP_OpportunityBizDocs_FulFill
GO
CREATE PROCEDURE [dbo].[USP_OpportunityBizDocs_FulFill] 
	@numDomainID NUMERIC(18,0),
	@numUserCntID NUMERIC(18,0),
	@numOppId NUMERIC(18,0),
	@numOppBizDocID NUMERIC(18,0),
	@vcItems VARCHAR(MAX),
	@dtShippedDate DATETIME
AS  
BEGIN 
BEGIN TRY
	DECLARE @vcItemType AS CHAR(1)
	DECLARE @bitDropShip AS BIT
	DECLARE @numOppItemID AS INT
	DECLARE @numWarehouseItemID AS INT
	DECLARE @numQty AS FLOAT
	DECLARE @numQtyShipped AS FLOAT
	DECLARE @numXmlQty AS FLOAT
	DECLARE @vcSerialLot AS VARCHAR(MAX)
	DECLARE @numSerialCount AS INT
	DECLARE @bitSerial AS BIT
	DECLARE @bitLot AS BIT
	DECLARE @bitAssembly AS BIT
	DECLARE @bitKit AS BIT

	DECLARE @i AS INT = 1
	DECLARE @COUNT AS INT  
	DECLARE @numNewQtyShipped AS FLOAT


	/* VERIFY BIZDOC IS EXISTS OR NOT */
	IF NOT EXISTS (SELECT numOppBizDocsId FROM OpportunityBizDocs WHERE numOppBizDocsId = @numOppBizDocID)
	BEGIN
		RAISERROR('FULFILLMENT_BIZDOC_IS_DELETED',16,1)
	END

	/* VERIFY WHETHER BIZDOC IS ALREADY FULLFILLED BY SOMEBODY ELSE */
	IF EXISTS (SELECT numOppBizDocsId FROM OpportunityBizDocs WHERE numOppBizDocsId = @numOppBizDocID AND bitFulfilled = 1) 
	BEGIN
		RAISERROR('FULFILLMENT_BIZDOC_ALREADY_FULFILLED',16,1)
	END

	IF EXISTS(SELECT numWOId FROm WorkOrder WHERE numOppId=@numOppId AND numWOStatus <> 23184) --WORK ORDER NOT YET COMPLETED
	BEGIN
		RAISERROR('WORK_ORDER_NOT_COMPLETED',16,1)
	END

	BEGIN TRANSACTION

		BEGIN /* REVERT SEIRAL/LOT# NUMBER ASSIGNED FROM PRODCUT&SERVICES TAB */
			UPDATE WHIDL
				SET WHIDL.numQty = (CASE WHEN Item.bitLotNo = 1 THEN ISNULL(WHIDL.numQty,0) + ISNULL(OWSI.numQty,0) ELSE 1 END)
			FROM 
				WareHouseItmsDTL WHIDL
			INNER JOIN
				OppWarehouseSerializedItem OWSI
			ON
				WHIDL.numWareHouseItmsDTLID = OWSI.numWarehouseItmsDTLID
				AND WHIDL.numWareHouseItemID = OWSI.numWarehouseItmsID
			INNER JOIN
				WareHouseItems
			ON
				WHIDL.numWareHouseItemID = WareHouseItems.numWareHouseItemID
			INNER JOIN
				Item
			ON
				WareHouseItems.numItemID = Item.numItemCode
			WHERE
				OWSI.numOppID=@numOppId
				AND OWSI.numOppBizDocsId IS NULL
		END

		BEGIN /* GET FULFILLMENT BIZDOC ITEMS */
			DECLARE @TempFinalTable TABLE
			(
				ID INT IDENTITY(1,1),
				numItemCode NUMERIC(18,0),
				vcItemType CHAR(1),
				bitSerial BIT,
				bitLot BIT,
				bitAssembly BIT,
				bitKit BIT,
				numOppItemID NUMERIC(18,0),
				numWarehouseItemID NUMERIC(18,0),
				numQtyShipped FLOAT,
				numQty FLOAT,
				bitDropShip BIT,
				vcSerialLot VARCHAR(MAX)
			)

			INSERT INTO @TempFinalTable
			(
				numItemCode,
				vcItemType,
				bitSerial,
				bitLot,
				bitAssembly,
				bitKit,
				numOppItemID,
				numWarehouseItemID,
				numQtyShipped,
				numQty,
				bitDropShip,
				vcSerialLot
			)
			SELECT
				OpportunityBizDocItems.numItemCode,
				ISNULL(Item.charItemType,''),
				ISNULL(Item.bitSerialized,0),
				ISNULL(Item.bitLotNo,0),
				ISNULL(Item.bitAssembly,0),
				ISNULL(Item.bitKitParent,0),
				ISNULL(OpportunityItems.numoppitemtcode,0),
				ISNULL(OpportunityBizDocItems.numWarehouseItmsID,0),
				ISNULL(OpportunityItems.numQtyShipped,0),
				ISNULL(OpportunityBizDocItems.numUnitHour,0),
				ISNULL(OpportunityItems.bitDropShip,0),
				''
			FROM
				OpportunityBizDocs
			INNER JOIN
				OpportunityBizDocItems
			ON
				OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
			INNER JOIN
				OpportunityItems
			ON
				OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtCode
			INNER JOIN
				Item
			ON
				OpportunityBizDocItems.numItemCode = Item.numItemCode
			WHERE
				numOppBizDocsId = @numOppBizDocID
				AND ISNULL(OpportunityItems.bitDropShip,0) = 0
				AND ISNULL(OpportunityItems.numWarehouseItmsID,0) > 0
		END

		BEGIN /* CONVERT vcItems XML to TABLE */
			DECLARE @TempItemXML TABLE
			(
				numOppItemID NUMERIC(18,0),
				numQty FLOAT,
				numWarehouseItemID NUMERIC(18,0),
				vcSerialLot VARCHAR(MAX)
			)

			DECLARE @idoc int
			EXEC sp_xml_preparedocument @idoc OUTPUT, @vcItems;

			INSERT INTO
				@TempItemXML
			SELECT
				numOppItemID,
				numQty,
				numWarehouseItemID,
				vcSerialLot
			FROM 
				OPENXML (@idoc, '/Items/Item',2)
			WITH 
			(
				numOppItemID NUMERIC(18,0),
				numQty FLOAT,
				numWarehouseItemID NUMERIC(18,0),
				vcSerialLot VARCHAR(MAX)
			);
		END

		BEGIN /* VALIDATE ALL THE DETAILS BEFORE FULFILLING ITEMS */
		
			SELECT @COUNT=COUNT(*) FROM @TempFinalTable

			DECLARE @TempLot TABLE
			(
				vcLotNo VARCHAR(100),
				numQty FLOAT 
			)

			WHILE @i <= @COUNT
			BEGIN
				SELECT
					@numOppItemID=numOppItemID,
					@numWarehouseItemID=numWarehouseItemID,
					@numQty=numQty,
					@bitSerial=bitSerial,
					@bitLot=bitLot 
				FROM 
					@TempFinalTable 
				WHERE 
					ID = @i

				--GET QUENTITY RECEIVED FROM XML PARAMETER
				SELECT 
					@numXmlQty = numQty, 
					@vcSerialLot=vcSerialLot, 
					@numSerialCount = (CASE WHEN (@bitSerial = 1 OR @bitLot = 1) THEN (SELECT COUNT(*) FROM dbo.SplitString(vcSerialLot,',')) ELSE 0 END)  
				FROM 
					@TempItemXML 
				WHERE 
					numOppItemID = @numOppItemID 

				--CHECK IF QUANTITY OF ITEM IN OpportunityBizDocItems TBALE IS SAME AS QUANTITY SUPPLIED FROM XML
				IF @numQty <> @numXmlQty
				BEGIN
					RAISERROR('QTY_MISMATCH',16,1)
				END

				--IF ITEM IS SERIAL ITEM CHEKC THAT VALID AND REQUIRED NUMBER OF SERIALS ARE PROVIDED BY USER
				IF @bitSerial = 1 
				BEGIN
					IF @numSerialCount <> @numQty
					BEGIN
						RAISERROR('REQUIRED_SERIALS_NOT_PROVIDED',16,1)
					END
					ELSE
					BEGIN
						IF (SELECT COUNT(*) FROM WareHouseItmsDTL WHERE numWareHouseItemID = @numWarehouseItemID AND numQty > 0 AND vcSerialNo IN (SELECT OutParam FROM dbo.SplitString(@vcSerialLot,','))) <> @numSerialCount
						BEGIN
							RAISERROR('INVALID_SERIAL_NUMBERS',16,1)
						END
					END
				END

				IF @bitLot = 1
				BEGIN
					-- DELETE DATA OF PREVIOUS ITERATION FROM TABLE
					DELETE FROM @TempLot

					--CONVERTS COMMA SEPERATED STRING TO LOTNo AND QTY 
					INSERT INTO
						@TempLot
					SELECT
						SUBSTRING(OutParam,0,PATINDEX('%(%', OutParam)),
						CAST(SUBSTRING(OutParam,(PATINDEX('%(%', OutParam) + 1),(PATINDEX('%)%', OutParam) - PATINDEX('%(%', OutParam) - 1)) AS INT)
					FROM
						dbo.SplitString(@vcSerialLot,',')

					--CHECKS WHETHER PROVIDED QTY OF LOT NO IS SAME IS REQUIRED QTY
					IF (SELECT SUM(numQty) FROM @TempLot) <> @numQty
					BEGIN
						RAISERROR('REQUIRED_LOTNO_NOT_PROVIDED',16,1)
					END
					ELSE
					BEGIN
						-- CHECKS WHETHER LOT NO EXISTS IN WareHouseItmsDTL
						IF (SELECT COUNT(*) FROM WareHouseItmsDTL WHERE numWareHouseItemID = @numWarehouseItemID AND vcSerialNo IN (SELECT vcLotNo FROM @TempLot)) <> (SELECT COUNT(*) FROM @TempLot)
						BEGIN
							RAISERROR('INVALID_LOT_NUMBERS',16,1)
						END
						ELSE
						BEGIN
							-- CHECKS WHETHER LOT NO HAVE ENOUGH QTY TO FULFILL ITEM QTY
							IF (SELECT 
									COUNT(*) 
								FROM 
									WareHouseItmsDTL 
								INNER JOIN
									@TempLot TEMPLOT
								ON
									WareHouseItmsDTL.vcSerialNo = TEMPLOT.vcLotNo
								WHERE 
									numWareHouseItemID = @numWarehouseItemID
									AND TEMPLOT.numQty > WareHouseItmsDTL.numQty) > 0
							BEGIN
								RAISERROR('SOME_LOTNO_DO_NOT_HAVE_ENOUGH_QTY',16,1)
							END
						END
					END
				END

		
				UPDATE @TempFinalTable SET vcSerialLot = @vcSerialLot WHERE ID=@i

				SET @i = @i + 1
			END
		END
	
		SET @i = 1
		SELECT @COUNT = COUNT(*) FROM @TempFinalTable
		DECLARE @vcDescription AS VARCHAR(300)
		DECLARE @numAllocation AS FLOAT

		DECLARE @j INT = 0
		DECLARE @ChildCount AS INT
		DECLARE @vcChildDescription AS VARCHAR(300)
		DECLARE @numChildAllocation AS FLOAT
		DECLARE @numOppChildItemID AS INT
		DECLARE @numChildItemCode AS INT
		DECLARE @bitChildIsKit AS BIT
		DECLARE @numChildWarehouseItemID AS INT
		DECLARE @numChildQty AS FLOAT
		DECLARE @numChildQtyShipped AS FLOAT

		DECLARE @TempKitSubItems TABLE
		(
			ID INT,
			numOppChildItemID NUMERIC(18,0),
			numChildItemCode NUMERIC(18,0),
			bitChildIsKit BIT,
			numChildWarehouseItemID NUMERIC(18,0),
			numChildQty FLOAT,
			numChildQtyShipped FLOAT
		)


		DECLARE @k AS INT = 1
		DECLARE @CountKitChildItems AS INT
		DECLARE @vcKitChildDescription AS VARCHAR(300)
		DECLARE @numKitChildAllocation AS FLOAT
		DECLARE @numOppKitChildItemID AS NUMERIC(18,0)
		DECLARE @numKitChildItemCode AS NUMERIC(18,0)
		DECLARE @numKitChildWarehouseItemID AS NUMERIC(18,0)
		DECLARE @numKitChildQty AS FLOAT
		DECLARE @numKitChildQtyShipped AS FLOAT

		DECLARE @TempKitChildKitSubItems TABLE
		(
			ID INT,
			numOppKitChildItemID NUMERIC(18,0),
			numKitChildItemCode NUMERIC(18,0),
			numKitChildWarehouseItemID NUMERIC(18,0),
			numKitChildQty FLOAT,
			numKitChildQtyShipped FLOAT
		)

		WHILE @i <= @COUNT
		BEGIN
			SELECT 
				@vcItemType=vcItemType,
				@bitSerial=bitSerial,
				@bitLot=bitLot,
				@bitAssembly=bitAssembly,
				@bitKit=bitKit,
				@bitDropShip=bitDropShip,
				@numOppItemID=numOppItemID,
				@numQty=numQty,
				@numQtyShipped = numQtyShipped,
				@numWarehouseItemID=numWarehouseItemID,
				@vcSerialLot = vcSerialLot
			FROM 
				@TempFinalTable 
			WHERE 
				ID = @i

			--MAKE INVENTIRY CHANGES IF INVENTORY ITEM
			IF (UPPER(@vcItemType) = 'P' AND @bitDropShip = 0)
			BEGIN
				SET @vcDescription='SO Qty Shipped (Qty:' + CAST(@numQty AS VARCHAR(10)) + ' Shipped:' +  CAST(@numQtyShipped AS VARCHAR(10)) + ')'
						 
				IF @bitKit = 1
				BEGIN
					-- CLEAR DATA OF PREVIOUS ITERATION
					DELETE FROM @TempKitSubItems

					-- GET KIT SUB ITEMS DETAIL
					INSERT INTO @TempKitSubItems
					(
						ID,
						numOppChildItemID,
						numChildItemCode,
						bitChildIsKit,
						numChildWarehouseItemID,
						numChildQty,
						numChildQtyShipped
					)
					SELECT 
						ROW_NUMBER() OVER(ORDER BY numOppChildItemID),
						ISNULL(numOppChildItemID,0),
						ISNULL(I.numItemCode,0),
						ISNULL(I.bitKitParent,0),
						ISNULL(numWareHouseItemId,0),
						((ISNULL(numQtyItemsReq_Orig,0) * ISNULL(dbo.fn_UOMConversion(OKI.numUOMId,OKI.numChildItemID,@numDomainID,I.numBaseUnit),1)) * @numQty),
						ISNULL(numQtyShipped,0)
					FROM 
						OpportunityKitItems OKI 
					JOIN 
						Item I 
					ON 
						OKI.numChildItemID=I.numItemCode    
					WHERE 
						charitemtype='P' 
						AND ISNULL(numWareHouseItemId,0) > 0 
						AND OKI.numOppID=@numOppID 
						AND OKI.numOppItemID=@numOppItemID 

					SET @j = 1
					SELECT @ChildCount=COUNT(*) FROM @TempKitSubItems

					--LOOP OVER ALL CHILD ITEMS AND RELESE ALLOCATION
					WHILE @j <= @ChildCount
					BEGIN
						SELECT
							@numOppChildItemID=numOppChildItemID,
							@numChildItemCode=numChildItemCode,
							@bitChildIsKit=bitChildIsKit,
							@numChildWarehouseItemID=numChildWarehouseItemID,
							@numChildQty=numChildQty,
							@numChildQtyShipped=numChildQtyShipped
						FROM
							@TempKitSubItems
						WHERE 
							ID = @j

						SELECT @numChildAllocation=ISNULL(numAllocation,0) FROM WareHouseItems WHERE numWareHouseItemID=@numChildWarehouseItemID

						-- IF CHILD ITEM IS KIT THEN LOOK OUT FOR ITS CHILD ITEMS WHICH ARE STORED IN OpportunityKitChildItems TABLE
						IF @bitChildIsKit = 1
						BEGIN
							-- IF KIT IS ADDED WITHIN KIT THEN THIS SUB KIT HAS ITS OWN ITEM CONFIGURATION WHICH ID STORED IN OpportunityKitChildItems
							IF EXISTS (SELECT numOppKitChildItemID FROM OpportunityKitChildItems WHERE numOppID=@numOppId AND numOppItemID=@numOppItemID AND numOppChildItemID=@numOppChildItemID)
							BEGIN
								-- CLEAR DATA OF PREVIOUS ITERATION
								DELETE FROM @TempKitChildKitSubItems

								-- GET SUB KIT SUB ITEMS DETAIL
								INSERT INTO @TempKitChildKitSubItems
								(
									ID,
									numOppKitChildItemID,
									numKitChildItemCode,
									numKitChildWarehouseItemID,
									numKitChildQty,
									numKitChildQtyShipped
								)
								SELECT 
									ROW_NUMBER() OVER(ORDER BY numOppKitChildItemID),
									ISNULL(numOppKitChildItemID,0),
									ISNULL(OKCI.numItemID,0),
									ISNULL(OKCI.numWareHouseItemId,0),
									((ISNULL(OKCI.numQtyItemsReq_Orig,0) * ISNULL(dbo.fn_UOMConversion(OKCI.numUOMId,OKCI.numItemID,@numDomainID,I.numBaseUnit),1)) * @numChildQty),
									ISNULL(numQtyShipped,0)
								FROM 
									OpportunityKitChildItems OKCI 
								JOIN 
									Item I 
								ON 
									OKCI.numItemID=I.numItemCode    
								WHERE 
									charitemtype='P' 
									AND ISNULL(numWareHouseItemId,0) > 0 
									AND OKCI.numOppID=@numOppID 
									AND OKCI.numOppItemID=@numOppItemID 
									AND OKCI.numOppChildItemID = @numOppChildItemID

								SET @k = 1
								SELECT @CountKitChildItems=COUNT(*) FROM @TempKitChildKitSubItems

								WHILE @k <= @CountKitChildItems
								BEGIN
									SELECT
										@numOppKitChildItemID=numOppKitChildItemID,
										@numKitChildItemCode=numKitChildItemCode,
										@numKitChildWarehouseItemID=numKitChildWarehouseItemID,
										@numKitChildQty=numKitChildQty,
										@numKitChildQtyShipped=numKitChildQtyShipped
									FROM
										@TempKitChildKitSubItems
									WHERE 
										ID = @k

									SELECT @numKitChildAllocation=ISNULL(numAllocation,0) FROM WareHouseItems WHERE numWareHouseItemID=@numKitChildWarehouseItemID

									--REMOVE QTY FROM ALLOCATION
									SET @numKitChildAllocation = @numKitChildAllocation - @numKitChildQty

									IF @numKitChildAllocation >= 0
									BEGIN
										UPDATE  
											WareHouseItems
										SET     
											numAllocation = @numKitChildAllocation,
											dtModified = GETDATE() 
										WHERE   
											numWareHouseItemID = @numKitChildWarehouseItemID      	
							
										UPDATE  
											OpportunityKitChildItems
										SET 
											numQtyShipped = ISNULL(numQtyShipped,0) + ISNULL(@numKitChildQty,0)
										WHERE   
											numOppKitChildItemID = @numOppKitChildItemID
											AND numOppChildItemID = @numOppChildItemID 
											AND numOppId=@numOppId
											AND numOppItemID=@numOppItemID

										SET @vcKitChildDescription = 'SO CHILD KIT Qty Shipped (Qty:' + CAST(@numKitChildQty AS VARCHAR(10)) + ' Shipped:' +  CAST(@numKitChildQtyShipped AS VARCHAR(10)) + ')'

										EXEC dbo.USP_ManageWareHouseItems_Tracking
											@numWareHouseItemID = @numKitChildWarehouseItemID,
											@numReferenceID = @numOppId,
											@tintRefType = 3,
											@vcDescription = @vcKitChildDescription,
											@numModifiedBy = @numUserCntID,
											@numDomainID = @numDomainID	 
									END
									ELSE
									BEGIN
										RAISERROR ('NOTSUFFICIENTQTY_ALLOCATION',16,1);
									END

									SET @k = @k + 1
								END
							END

							UPDATE  
								OpportunityKitItems
							SET 
								numQtyShipped = ISNULL(numQtyShipped,0) + ISNULL(@numChildQty,0)
							WHERE   
								numOppChildItemID = @numOppChildItemID 
								AND numOppId=@numOppId
								AND numOppItemID=@numOppItemID

							SET @vcChildDescription = 'SO KIT Qty Shipped (Qty:' + CAST(@numChildQty AS VARCHAR(10)) + ' Shipped:' +  CAST(@numChildQtyShipped AS VARCHAR(10)) + ')'

							EXEC dbo.USP_ManageWareHouseItems_Tracking
								@numWareHouseItemID = @numChildWarehouseItemID,
								@numReferenceID = @numOppId,
								@tintRefType = 3,
								@vcDescription = @vcChildDescription,
								@numModifiedBy = @numUserCntID,
								@numDomainID = @numDomainID	 
						END
						ELSE 
						BEGIN
							--REMOVE QTY FROM ALLOCATION
							SET @numChildAllocation = @numChildAllocation - @numChildQty
							IF @numChildAllocation >= 0
							BEGIN
								UPDATE  
									WareHouseItems
								SET     
									numAllocation = @numChildAllocation,
									dtModified = GETDATE() 
								WHERE   
									numWareHouseItemID = @numChildWarehouseItemID      	
							
								UPDATE  
									OpportunityKitItems
								SET 
									numQtyShipped = ISNULL(numQtyShipped,0) + ISNULL(@numChildQty,0)
								WHERE   
									numOppChildItemID = @numOppChildItemID 
									AND numOppId=@numOppId
									AND numOppItemID=@numOppItemID

								SET @vcChildDescription = 'SO KIT Qty Shipped (Qty:' + CAST(@numChildQty AS VARCHAR(10)) + ' Shipped:' +  CAST(@numChildQtyShipped AS VARCHAR(10)) + ')'

								EXEC dbo.USP_ManageWareHouseItems_Tracking
									@numWareHouseItemID = @numChildWarehouseItemID,
									@numReferenceID = @numOppId,
									@tintRefType = 3,
									@vcDescription = @vcChildDescription,
									@numModifiedBy = @numUserCntID,
									@numDomainID = @numDomainID	 
							END
							ELSE
							BEGIN
								RAISERROR ('NOTSUFFICIENTQTY_ALLOCATION',16,1);
							END
						END

						SET @j = @j + 1
					END
				END
				ELSE
				BEGIN
					SELECT @numAllocation = ISNULL(numAllocation, 0) FROM WareHouseItems WHERE numWareHouseItemID = @numWareHouseItemID
				       
					--REMOVE QTY FROM ALLOCATION
					SET @numAllocation = @numAllocation - @numQty	
				       
					IF (@numAllocation >= 0 )
					BEGIN
						UPDATE  
							WareHouseItems
						SET     
							numAllocation = @numAllocation,
							dtModified = GETDATE() 
						WHERE   
							numWareHouseItemID = @numWareHouseItemID      	

						IF @bitSerial = 1
						BEGIN								
							DELETE FROM OppWarehouseSerializedItem WHERE numOppID=@numOppID AND numOppItemID=@numOppItemID AND numWarehouseItmsID=@numWarehouseItemID

							INSERT INTO OppWarehouseSerializedItem
							(
								numWarehouseItmsDTLID,
								numOppID,
								numOppItemID,
								numWarehouseItmsID,
								numQty,
								numOppBizDocsId	
							)
							SELECT
								(SELECT TOP 1 numWarehouseItmsDTLID FROM WareHouseItmsDTL WHERE numWareHouseItemID=@numWarehouseItemID AND vcSerialNo=OutParam AND  numQty = 1),
								@numOppID,
								@numOppItemID,
								@numWarehouseItemID,
								1,
								@numOppBizDocID
							FROM
								dbo.SplitString(@vcSerialLot,',')


							UPDATE WareHouseItmsDTL SET numQty=0 WHERE numWareHouseItemID=@numWarehouseItemID AND vcSerialNo IN (SELECT OutParam FROM dbo.SplitString(@vcSerialLot,','))
						END

						IF @bitLot = 1
						BEGIN
							DELETE FROM @TempLot

							INSERT INTO
								@TempLot
							SELECT
								SUBSTRING(OutParam,0,PATINDEX('%(%', OutParam)),
								CAST(SUBSTRING(OutParam,(PATINDEX('%(%', OutParam) + 1),(PATINDEX('%)%', OutParam) - PATINDEX('%(%', OutParam) - 1)) AS INT)
							FROM
								dbo.SplitString(@vcSerialLot,',')

							DELETE FROM OppWarehouseSerializedItem WHERE numOppID=@numOppID AND numOppItemID=@numOppItemID AND numWarehouseItmsID=@numWarehouseItemID

							INSERT INTO OppWarehouseSerializedItem
							(
								numWarehouseItmsDTLID,
								numOppID,
								numOppItemID,
								numWarehouseItmsID,
								numQty,
								numOppBizDocsId	
							)
							SELECT
								(SELECT TOP 1 numWarehouseItmsDTLID FROM WareHouseItmsDTL WHERE numWareHouseItemID=@numWarehouseItemID AND vcSerialNo=TEMPLOT.vcLotNo AND numQty >= TEMPLOT.numQty),
								@numOppID,
								@numOppItemID,
								@numWarehouseItemID,
								TEMPLOT.numQty,
								@numOppBizDocID
							FROM
								@TempLot TEMPLOT


							UPDATE	TEMPWareHouseItmsDTL
								SET TEMPWareHouseItmsDTL.numQty = ISNULL(TEMPWareHouseItmsDTL.numQty,0) - ISNULL(TEMPLOT.numQty,0)
							FROM
								WareHouseItmsDTL TEMPWareHouseItmsDTL
							INNER JOIN
								@TempLot TEMPLOT
							ON
								TEMPWareHouseItmsDTL.vcSerialNo = TEMPLOT.vcLotNo
							WHERE
								TEMPWareHouseItmsDTL.numWareHouseItemID = @numWarehouseItemID
						END
					END
					ELSE
					BEGIN
						PRINT 'PARENT: ' + CAST(@numChildWarehouseItemID AS VARCHAR)
						RAISERROR ('NOTSUFFICIENTQTY_ALLOCATION',16,1);
					END
				END

				EXEC dbo.USP_ManageWareHouseItems_Tracking
							@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
							@numReferenceID = @numOppId, --  numeric(9, 0)
							@tintRefType = 3, --  tinyint
							@vcDescription = @vcDescription, --  varchar(100)
							@numModifiedBy = @numUserCntID,
							@numDomainID = @numDomainID	 
			END

			UPDATE  
				OpportunityItems
			SET 
				numQtyShipped = ISNULL(numQtyShipped,0) + ISNULL(@numQty,0)
			WHERE   
				numoppitemtCode = @numOppItemID 
				AND numOppId=@numOppId

			SET @i = @i + 1
		END

		UPDATE OpportunityBizDocs SET bitFulfilled = 1,dtShippedDate=@dtShippedDate WHERE numOppBizDocsId=@numOppBizDocID
	COMMIT TRANSACTION
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
END

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityBizDocs_RevertFulFillment')
DROP PROCEDURE USP_OpportunityBizDocs_RevertFulFillment
GO
CREATE PROCEDURE [dbo].[USP_OpportunityBizDocs_RevertFulFillment] 
	@numDomainID NUMERIC(18,0),
	@numUserCntID NUMERIC(18,0),
	@numOppId NUMERIC(18,0),
	@numOppBizDocID NUMERIC(18,0)
AS  
BEGIN 
BEGIN TRY

	DECLARE @vcItemType AS CHAR(1)
	DECLARE @bitDropShip AS BIT
	DECLARE @bitSerial AS BIT
	DECLARE @bitLot AS BIT
	DECLARE @bitAssembly AS BIT
	DECLARE @bitKit AS BIT
	DECLARE @numOppItemID AS INT
	DECLARE @numWarehouseItemID AS INT
	DECLARE @numQty AS FLOAT
	DECLARE @numQtyShipped AS FLOAT
	DECLARE @vcDescription AS VARCHAR(300)
	DECLARE @numAllocation AS FLOAT


	DECLARE @j INT = 0
	DECLARE @ChildCount AS INT
	DECLARE @vcChildDescription AS VARCHAR(300)
	DECLARE @numChildAllocation AS FLOAT
	DECLARE @numOppChildItemID AS INT
	DECLARE @numChildItemCode AS INT
	DECLARE @bitChildIsKit AS BIT
	DECLARE @numChildWarehouseItemID AS INT
	DECLARE @numChildQty AS FLOAT
	DECLARE @numChildQtyShipped AS FLOAT

	DECLARE @TempKitSubItems TABLE
	(
		ID INT,
		numOppChildItemID NUMERIC(18,0),
		numChildItemCode NUMERIC(18,0),
		bitChildIsKit BIT,
		numChildWarehouseItemID NUMERIC(18,0),
		numChildQty FLOAT,
		numChildQtyShipped FLOAT
	)

	DECLARE @k AS INT = 1
	DECLARE @CountKitChildItems AS INT
	DECLARE @vcKitChildDescription AS VARCHAR(300)
	DECLARE @numKitChildAllocation AS FLOAT
	DECLARE @numOppKitChildItemID AS NUMERIC(18,0)
	DECLARE @numKitChildItemCode AS NUMERIC(18,0)
	DECLARE @numKitChildWarehouseItemID AS NUMERIC(18,0)
	DECLARE @numKitChildQty AS FLOAT
	DECLARE @numKitChildQtyShipped AS FLOAT

	DECLARE @TempKitChildKitSubItems TABLE
	(
		ID INT,
		numOppKitChildItemID NUMERIC(18,0),
		numKitChildItemCode NUMERIC(18,0),
		numKitChildWarehouseItemID NUMERIC(18,0),
		numKitChildQty FLOAT,
		numKitChildQtyShipped FLOAT
	)

	DECLARE @TempFinalTable TABLE
	(
		ID INT IDENTITY(1,1),
		numItemCode NUMERIC(18,0),
		vcItemType CHAR(1),
		bitSerial BIT,
		bitLot BIT,
		bitAssembly BIT,
		bitKit BIT,
		numOppItemID NUMERIC(18,0),
		numWarehouseItemID NUMERIC(18,0),
		numQtyShipped FLOAT,
		numQty FLOAT,
		bitDropShip BIT,
		vcSerialLot VARCHAR(MAX)
	)

	INSERT INTO @TempFinalTable
	(
		numItemCode,
		vcItemType,
		bitSerial,
		bitLot,
		bitAssembly,
		bitKit,
		numOppItemID,
		numWarehouseItemID,
		numQtyShipped,
		numQty,
		bitDropShip,
		vcSerialLot
	)
	SELECT
		OpportunityBizDocItems.numItemCode,
		ISNULL(Item.charItemType,''),
		ISNULL(Item.bitSerialized,0),
		ISNULL(Item.bitLotNo,0),
		ISNULL(Item.bitAssembly,0),
		ISNULL(Item.bitKitParent,0),
		ISNULL(OpportunityItems.numoppitemtcode,0),
		ISNULL(OpportunityBizDocItems.numWarehouseItmsID,0),
		ISNULL(OpportunityItems.numQtyShipped,0),
		ISNULL(OpportunityBizDocItems.numUnitHour,0),
		ISNULL(OpportunityItems.bitDropShip,0),
		''
	FROM
		OpportunityBizDocs
	INNER JOIN
		OpportunityBizDocItems
	ON
		OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
	INNER JOIN
		OpportunityItems
	ON
		OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtCode
	INNER JOIN
		Item
	ON
		OpportunityBizDocItems.numItemCode = Item.numItemCode
	WHERE
		numOppBizDocsId = @numOppBizDocID
		AND ISNULL(OpportunityItems.bitDropShip,0) = 0
		AND ISNULL(OpportunityItems.numWarehouseItmsID,0) > 0

	BEGIN TRANSACTION
		DECLARE @i INT = 1
		DECLARE @COUNT INT = 0
		SELECT @COUNT = COUNT(*) FROM @TempFinalTable

		WHILE @i <= @COUNT
		BEGIN
			SELECT 
				@vcItemType=vcItemType,
				@bitSerial=bitSerial,
				@bitLot=bitLot,
				@bitAssembly=bitAssembly,
				@bitKit=bitKit,
				@bitDropShip=bitDropShip,
				@numOppItemID=numOppItemID,
				@numQty=numQty,
				@numQtyShipped = numQtyShipped,
				@numWarehouseItemID=numWarehouseItemID
			FROM 
				@TempFinalTable 
			WHERE 
				ID = @i

			IF @numQty > @numQtyShipped
			BEGIN
				RAISERROR ('INVALID_SHIPPED_QTY',16,1);
			END

			--REVERT QUANTITY TO ON ALLOCATION IF INVENTORY ITEM
			IF (UPPER(@vcItemType) = 'P' AND @bitDropShip = 0)
			BEGIN
				SET @vcDescription='SO Qty Shipped Return (Qty:' + CAST(@numQty AS VARCHAR(10)) + ' Shipped:' +  CAST(@numQtyShipped AS VARCHAR(10)) + ')'
				
				IF @bitKit = 1
				BEGIN
					-- CLEAR DATA OF PREVIOUS ITERATION
					DELETE FROM @TempKitSubItems

					-- GET KIT SUB ITEMS DETAIL
					INSERT INTO @TempKitSubItems
					(
						ID,
						numOppChildItemID,
						numChildItemCode,
						bitChildIsKit,
						numChildWarehouseItemID,
						numChildQty,
						numChildQtyShipped
					)
					SELECT 
						ROW_NUMBER() OVER(ORDER BY numOppChildItemID),
						ISNULL(numOppChildItemID,0),
						ISNULL(I.numItemCode,0),
						ISNULL(I.bitKitParent,0),
						ISNULL(OKI.numWareHouseItemId,0),
						((ISNULL(OKI.numQtyItemsReq_Orig,0) * ISNULL(dbo.fn_UOMConversion(OKI.numUOMId,OKI.numChildItemID,@numDomainID,I.numBaseUnit),1)) * @numQty),
						ISNULL(OKI.numQtyShipped,0)
					FROM 
						OpportunityKitItems OKI 
					JOIN 
						Item I 
					ON 
						OKI.numChildItemID=I.numItemCode    
					WHERE 
						charitemtype='P' 
						AND ISNULL(numWareHouseItemId,0) > 0 
						AND OKI.numOppID=@numOppID 
						AND OKI.numOppItemID=@numOppItemID 

					SET @j = 1
					SELECT @ChildCount=COUNT(*) FROM @TempKitSubItems

					--LOOP OVER ALL CHILD ITEMS AND REVERT ALLOCATION
					WHILE @j <= @ChildCount
					BEGIN
					
						SELECT
							@numOppChildItemID=numOppChildItemID,
							@numChildItemCode=numChildItemCode,
							@bitChildIsKit=bitChildIsKit,
							@numChildWarehouseItemID=numChildWarehouseItemID,
							@numChildQty=numChildQty,
							@numChildQtyShipped=numChildQtyShipped
						FROM
							@TempKitSubItems
						WHERE 
							ID = @j

						IF @numChildQty > @numChildQtyShipped
						BEGIN
							RAISERROR ('INVALID_KIT_SUB_ITEM_SHIPPED_QTY',16,1);
						END

						SELECT @numChildAllocation=ISNULL(numAllocation,0) FROM WareHouseItems WHERE numWareHouseItemID=@numChildWarehouseItemID

						IF @bitChildIsKit = 1
						BEGIN
							-- IF KIT IS ADDED WITHIN KIT THEN THIS SUB KIT HAS ITS OWN ITEM CONFIGURATION WHICH ID STORED IN OpportunityKitChildItems
							IF EXISTS (SELECT numOppKitChildItemID FROM OpportunityKitChildItems WHERE numOppID=@numOppId AND numOppItemID=@numOppItemID AND numOppChildItemID=@numOppChildItemID)
							BEGIN
								-- CLEAR DATA OF PREVIOUS ITERATION
								DELETE FROM @TempKitChildKitSubItems

								-- GET SUB KIT SUB ITEMS DETAIL
								INSERT INTO @TempKitChildKitSubItems
								(
									ID,
									numOppKitChildItemID,
									numKitChildItemCode,
									numKitChildWarehouseItemID,
									numKitChildQty,
									numKitChildQtyShipped
								)
								SELECT 
									ROW_NUMBER() OVER(ORDER BY numOppKitChildItemID),
									ISNULL(numOppKitChildItemID,0),
									ISNULL(OKCI.numItemID,0),
									ISNULL(OKCI.numWareHouseItemId,0),
									((ISNULL(OKCI.numQtyItemsReq_Orig,0) * ISNULL(dbo.fn_UOMConversion(OKCI.numUOMId,OKCI.numItemID,@numDomainID,I.numBaseUnit),1)) * @numChildQty),
									ISNULL(numQtyShipped,0)
								FROM 
									OpportunityKitChildItems OKCI 
								JOIN 
									Item I 
								ON 
									OKCI.numItemID=I.numItemCode    
								WHERE 
									charitemtype='P' 
									AND ISNULL(numWareHouseItemId,0) > 0 
									AND OKCI.numOppID=@numOppID 
									AND OKCI.numOppItemID=@numOppItemID 
									AND OKCI.numOppChildItemID = @numOppChildItemID

								SET @k = 1
								SELECT @CountKitChildItems=COUNT(*) FROM @TempKitChildKitSubItems


								--LOOP OVER ALL CHILD ITEMS AND REVERT ALLOCATION
								WHILE @k <= @CountKitChildItems
								BEGIN
									SELECT
										@numOppKitChildItemID=numOppKitChildItemID,
										@numKitChildItemCode=numKitChildItemCode,
										@numKitChildWarehouseItemID=numKitChildWarehouseItemID,
										@numKitChildQty=numKitChildQty,
										@numKitChildQtyShipped=numKitChildQtyShipped
									FROM
										@TempKitChildKitSubItems
									WHERE 
										ID = @k

									IF @numKitChildQty > @numKitChildQtyShipped
									BEGIN
										RAISERROR ('INVALID_KIT_SUB_ITEM_SHIPPED_QTY',16,1);
									END

									SELECT @numKitChildAllocation=ISNULL(numAllocation,0) FROM WareHouseItems WHERE numWareHouseItemID=@numKitChildWarehouseItemID

									--PLACE QTY BACK ON ALLOCATION
									SET @numKitChildAllocation = @numKitChildAllocation + @numKitChildQty
						
									-- UPDATE WAREHOUSE INVENTORY
									UPDATE  
										WareHouseItems
									SET     
										numAllocation = @numKitChildAllocation,
										dtModified = GETDATE() 
									WHERE   
										numWareHouseItemID = @numKitChildWarehouseItemID      	
						
									--DECREASE QTY SHIPPED OF ORDER ITEM	
									UPDATE  
										OpportunityKitChildItems
									SET 
										numQtyShipped = ISNULL(numQtyShipped,0) - ISNULL(@numKitChildQty,0)
									WHERE   
										numOppKitChildItemID = @numOppKitChildItemID
										AND numOppChildItemID = @numOppChildItemID 
										AND numOppId=@numOppId
										AND numOppItemID=@numOppItemID

									SET @vcKitChildDescription = 'SO CHILD KIT Qty Shipped Return (Qty:' + CAST(@numKitChildQty AS VARCHAR(10)) + ' Shipped:' +  CAST(@numKitChildQtyShipped AS VARCHAR(10)) + ')'

									-- CREATE WAREHOUSE INVENTORY TRACKING ENTRY
									EXEC dbo.USP_ManageWareHouseItems_Tracking
										@numWareHouseItemID = @numKitChildWarehouseItemID,
										@numReferenceID = @numOppId,
										@tintRefType = 3,
										@vcDescription = @vcKitChildDescription,
										@numModifiedBy = @numUserCntID,
										@numDomainID = @numDomainID	 
									
									SET @k = @k + 1
								END
							END

							--DECREASE QTY SHIPPED OF ORDER ITEM	
							UPDATE  
								OpportunityKitItems
							SET 
								numQtyShipped = ISNULL(numQtyShipped,0) - ISNULL(@numChildQty,0)
							WHERE   
								numOppChildItemID = @numOppChildItemID 
								AND numOppId=@numOppId
								AND numOppItemID=@numOppItemID

							SET @vcChildDescription = 'SO KIT Qty Shipped Return (Qty:' + CAST(@numChildQty AS VARCHAR(10)) + ' Shipped:' +  CAST(@numChildQtyShipped AS VARCHAR(10)) + ')'

							-- CREATE WAREHOUSE INVENTORY TRACKING ENTRY
							EXEC dbo.USP_ManageWareHouseItems_Tracking
								@numWareHouseItemID = @numChildWarehouseItemID,
								@numReferenceID = @numOppId,
								@tintRefType = 3,
								@vcDescription = @vcChildDescription,
								@numModifiedBy = @numUserCntID,
								@numDomainID = @numDomainID	 
						END
						ELSE
						BEGIN
							
							--PLACE QTY BACK ON ALLOCATION
							SET @numChildAllocation = @numChildAllocation + @numChildQty
						
							-- UPDATE WAREHOUSE INVENTORY
							UPDATE  
								WareHouseItems
							SET     
								numAllocation = @numChildAllocation,
								dtModified = GETDATE() 
							WHERE   
								numWareHouseItemID = @numChildWarehouseItemID      	
						
							--DECREASE QTY SHIPPED OF ORDER ITEM	
							UPDATE  
								OpportunityKitItems
							SET 
								numQtyShipped = ISNULL(numQtyShipped,0) - ISNULL(@numChildQty,0)
							WHERE   
								numOppChildItemID = @numOppChildItemID 
								AND numOppId=@numOppId
								AND numOppItemID=@numOppItemID

							SET @vcChildDescription = 'SO KIT Qty Shipped Return (Qty:' + CAST(@numChildQty AS VARCHAR(10)) + ' Shipped:' +  CAST(@numChildQtyShipped AS VARCHAR(10)) + ')'

							-- CREATE WAREHOUSE INVENTORY TRACKING ENTRY
							EXEC dbo.USP_ManageWareHouseItems_Tracking
								@numWareHouseItemID = @numChildWarehouseItemID,
								@numReferenceID = @numOppId,
								@tintRefType = 3,
								@vcDescription = @vcChildDescription,
								@numModifiedBy = @numUserCntID,
								@numDomainID = @numDomainID	
						END

						SET @j = @j + 1
					END
				END
				ELSE
				BEGIN
					
					SELECT @numAllocation = ISNULL(numAllocation, 0) FROM WareHouseItems WHERE numWareHouseItemID = @numWareHouseItemID
				       
					--PLACE QTY BACK ON ALLOCATION
					SET @numAllocation = @numAllocation + @numQty	

					-- UPDATE WAREHOUSE INVENTORY
					UPDATE  
						WareHouseItems
					SET     
						numAllocation = @numAllocation
						,dtModified = GETDATE() 
					WHERE   
						numWareHouseItemID = @numWareHouseItemID      	

					IF @bitSerial = 1 OR @bitLot = 1
					BEGIN 
						UPDATE OppWarehouseSerializedItem SET numOppBizDocsId=NULL WHERE numOppID=@numOppID AND numOppItemID= @numOppItemID AND numWarehouseItmsID=@numWarehouseItemID AND numOppBizDocsId=@numOppBizDocID
					END
				END

				-- CREATE WAREHOUSE INVENTORY TRACKING ENTRY
				EXEC dbo.USP_ManageWareHouseItems_Tracking
							@numWareHouseItemID = @numWareHouseItemID,
							@numReferenceID = @numOppId,
							@tintRefType = 3,
							@vcDescription = @vcDescription,
							@numModifiedBy = @numUserCntID,
							@numDomainID = @numDomainID	 
				
			END

			--DECREASE QTY SHIPPED OF ORDER ITEM	
			UPDATE 
				OpportunityItems
			SET     
				numQtyShipped = (ISNULL(numQtyShipped,0) - @numQty)
			WHERE   
				numoppitemtCode = @numOppItemID 
				AND numOppId=@numOppId

			SET @i = @i + 1
		END
	COMMIT TRANSACTION 
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRAN

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by Siva                                                                                                            
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityKitItemsForAuthorizativeAccounting')
DROP PROCEDURE USP_OpportunityKitItemsForAuthorizativeAccounting
GO
CREATE PROCEDURE  [dbo].[USP_OpportunityKitItemsForAuthorizativeAccounting]                                                                                                                                                
(                                                                                                                                                
 @numOppId as numeric(9)=null,                                                                                                                                                
 @numOppBizDocsId as numeric(9)=null                                                              
)                                                                                                                                                
                                                                                                                                            
as                           
BEGIN
     --OpportunityKitItems
    SELECT 
		I.[vcItemName] as Item,        
		I.charitemType as type,        
		'' as [desc],        
		(ISNULL(OBI.numUnitHour,0) * (OKI.numQtyItemsReq_Orig * ISNULL(dbo.fn_UOMConversion(OKI.numUOMId,OKI.numChildItemID,i.numDomainID,i.numBaseUnit),1))) as Unit,        
		0 as Price,        
		0 as Amount,0 AS ItemTotalAmount,       
		0 as listPrice,
		CONVERT(VARCHAR,i.numItemCode) as ItemCode,
		numOppChildItemID AS numoppitemtCode,        
		L.vcdata as vcItemClassification,
		CASE WHEN I.bitTaxable=0 THEN 'No' ELSE 'Yes' END AS Taxable,
		0 AS monListPrice,
		ISNULL(i.numIncomeChartAcntId,0) AS itemIncomeAccount,
		ISNULL(i.numAssetChartAcntId,0) as itemInventoryAsset,
		'NI' as ItemType,
		ISNULL(i.numCOGsChartAcntId,0) AS itemCoGs,
		(CASE WHEN ISNULL(i.bitVirtualInventory,0) = 1 THEN '0' ELSE ISNULL(OKI.monAvgCost,'0') END) AS AverageCost,
		ISNULL(OBI.bitDropShip,0) AS bitDropShip,  
		0 as DiscAmt,
		NULLIF(Opp.numProjectID,0) numProjectID,
		NULLIF(Opp.numClassID,0) numClassID 
	FROM  
		OpportunityItems opp 
	JOIN 
		OpportunityBizDocItems OBI 
	ON 
		OBI.numOppItemID=Opp.numoppitemtCode       
	JOIN 
		OpportunityKitItems OKI 
	ON 
		OKI.numOppItemID=opp.numoppitemtCode 
		AND Opp.numOppId=OKI.numOppId
	LEFT JOIN 
		Item iMain
	ON 
		opp.numItemCode=iMain.numItemCode
	LEFT JOIN 
		Item i 
	ON 
		OKI.numChildItemID=i.numItemCode        
	LEFT JOIN 
		ListDetails L 
	ON 
		i.numItemClassification=L.numListItemID        
	WHERE 
		Opp.numOppId=@numOppId 
		AND OBI.numOppBizDocID=@numOppBizDocsId
		AND ISNULL(I.bitKitParent,0) = 0
		AND ISNULL(iMain.bitAssembly,0) = 0
	UNION ALL
	SELECT 
		I.[vcItemName] as Item,        
		charitemType as type,        
		'' as [desc],        
		(ISNULL(OBI.numUnitHour,0) * ISNULL(OKI.numQtyItemsReq_Orig,0) * (ISNULL(OKCI.numQtyItemsReq_Orig,0) * ISNULL(dbo.fn_UOMConversion(OKCI.numUOMId,OKCI.numItemID,i.numDomainID,i.numBaseUnit),1))) as Unit,        
		0 as Price,        
		0 as Amount,0 AS ItemTotalAmount,       
		0 as listPrice,
		CONVERT(VARCHAR,i.numItemCode) as ItemCode,
		OKI.numOppChildItemID AS numoppitemtCode,        
		L.vcdata as vcItemClassification,
		CASE WHEN bitTaxable=0 THEN 'No' ELSE 'Yes' END AS Taxable,
		0 AS monListPrice,
		ISNULL(i.numIncomeChartAcntId,0) AS itemIncomeAccount,
		ISNULL(i.numAssetChartAcntId,0) as itemInventoryAsset,
		'NI' as ItemType,
		ISNULL(i.numCOGsChartAcntId,0) AS itemCoGs,
		(CASE WHEN ISNULL(i.bitVirtualInventory,0) = 1 THEN '0' ELSE ISNULL(OKCI.monAvgCost,'0') END) AS AverageCost,
		ISNULL(OBI.bitDropShip,0) AS bitDropShip,  
		0 as DiscAmt,
		NULLIF(Opp.numProjectID,0) numProjectID,
		NULLIF(Opp.numClassID,0) numClassID 
	FROM  
		OpportunityKitChildItems OKCI 
	INNER JOIN
		OpportunityKitItems OKI
	ON
		OKCI.numOppChildItemID = OKI.numOppChildItemID
	INNER JOIN
		OpportunityItems opp 
	ON
		OKCI.numOppItemID = opp.numoppitemtCode
		AND OKCI.numOppID = @numOppId 
	INNER JOIN 
		OpportunityBizDocItems OBI 
	ON 
		OBI.numOppItemID=Opp.numoppitemtCode       	
	LEFT JOIN 
		Item i 
	ON 
		OKCI.numItemID=i.numItemCode        
	LEFT JOIN 
		ListDetails L 
	ON 
		i.numItemClassification=L.numListItemID        
	WHERE 
		OKCI.numOppId=@numOppId 
		AND OBI.numOppBizDocID=@numOppBizDocsId
END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityMaster_MergeVendorPO')
DROP PROCEDURE USP_OpportunityMaster_MergeVendorPO
GO
CREATE PROCEDURE [dbo].[USP_OpportunityMaster_MergeVendorPO]  
	@numDomainID NUMERIC(18,0),
	@numUserCntID NUMERIC(18,0),
	@vcXML VARCHAR(4000)
AS  
BEGIN  
BEGIN TRY
BEGIN TRANSACTION
	DECLARE @docHandle int;

	DECLARE @Vendor TABLE
	(
		ID INT IDENTITY(1,1),
		numVendorID NUMERIC(18,0),
		numMasterPOID NUMERIC(18,0)
	)

	DECLARE @VendorPOToMerger TABLE
	(
		numVendorID NUMERIC(18,0),
		numPOID NUMERIC(18,0)
	)

	DECLARE @TEMPMasterItems TABLE
	(
		numoppitemtCode NUMERIC(18,0),numItemCode NUMERIC(18,0),numUnitHour FLOAT,monPrice MONEY,
		monTotAmount MONEY,vcItemDesc VARCHAR(1000),numWarehouseItmsID NUMERIC(18,0),numToWarehouseItemID NUMERIC(18,0),
		Op_Flag INT, ItemType VARCHAR(50),DropShip BIT,bitDiscountType BIT, fltDiscount FLOAT, monTotAmtBefDiscount MONEY,
		vcItemName VARCHAR(300),numUOM NUMERIC(18,0), bitWorkOrder BIT,numVendorWareHouse NUMERIC(18,0),
		numShipmentMethod NUMERIC(18,0),numSOVendorId NUMERIC(18,0),numProjectID NUMERIC(18,0),
		numProjectStageID NUMERIC(18,0),Attributes VARCHAR(200),bitItemPriceApprovalRequired BIT
	)

	DECLARE @TEMPChildItems TABLE
	(
		numRowID INT, numoppitemtCode NUMERIC(18,0),numItemCode NUMERIC(18,0),numUnitHour FLOAT,monPrice MONEY,
		monTotAmount MONEY,vcItemDesc VARCHAR(1000),numWarehouseItmsID NUMERIC(18,0),numToWarehouseItemID NUMERIC(18,0),
		Op_Flag INT, ItemType VARCHAR(50),DropShip BIT,bitDiscountType BIT, fltDiscount FLOAT, monTotAmtBefDiscount MONEY,
		vcItemName VARCHAR(300),numUOM NUMERIC(18,0), bitWorkOrder BIT,numVendorWareHouse NUMERIC(18,0),
		numShipmentMethod NUMERIC(18,0),numSOVendorId NUMERIC(18,0),numProjectID NUMERIC(18,0),
		numProjectStageID NUMERIC(18,0),Attributes VARCHAR(200),bitItemPriceApprovalRequired BIT
	)

	DECLARE @TEMPPO TABLE
	(
		ID INT,
		numPOID NUMERIC(18,0)
	)

	EXEC sp_xml_preparedocument @docHandle OUTPUT, @vcXML;

	--RETRIVE VENDOR AND MASTER PURCHASE ORDER IDS
	INSERT INTO 
		@Vendor
	SELECT 
		VendorID,
		MasterPOID
	FROM 
		OPENXML(@docHandle, N'/Vendors/Vendor',2) 
	WITH 
		(
			VendorID NUMERIC(18,0),
			MasterPOID NUMERIC(18,0)
		);  
	
	--RETIRIVE ORDERS OF EACH VENDOR
	INSERT INTO 
		@VendorPOToMerger
	SELECT 
		VendorID,
		OrderID
	FROM 
		OPENXML(@docHandle, N'/Vendors/Vendor/Orders/OrderID',2) 
	WITH 
		(
			VendorID NUMERIC(18,0) '../../VendorID',
			OrderID NUMERIC(18,0) '.'
		);  

	DECLARE @i AS INT = 1
	DECLARE @iCOUNT AS INT
	SELECT @iCOUNT=COUNT(*) FROM @Vendor

	--LOOP ALL VENDORS
	WHILE @i <= @iCOUNT
	BEGIN
		DECLARE @VendorID AS NUMERIC(18,0) = 0
		DECLARE @MasterPOID AS NUMERIC(18,0) = 0
		DECLARE @numOppID  AS NUMERIC(18,0) = NULL                                                                        
		DECLARE @numContactId NUMERIC(18,0)=null                                                                        
		DECLARE @numDivisionId numeric(9)=null                                                                          
		DECLARE @tintSource numeric(9)=null                                                                          
		DECLARE @vcPOppName Varchar(100)=''                                                                 
		DECLARE @Comments varchar(1000)=''                                                                          
		DECLARE @bitPublicFlag bit=0                                                                                                                                                         
		DECLARE @monPAmount money =0                                                 
		DECLARE @numAssignedTo as numeric(9)=0                                                                                                                                                                                                                                                                                         
		DECLARE @strItems as VARCHAR(MAX)=null                                                                          
		DECLARE @strMilestone as VARCHAR(100)=null                                                                          
		DECLARE @dtEstimatedCloseDate datetime                                                                          
		DECLARE @CampaignID as numeric(9)=null                                                                          
		DECLARE @lngPConclAnalysis as numeric(9)=null                                                                         
		DECLARE @tintOppType as tinyint                                                                                                                                             
		DECLARE @tintActive as tinyint                                                              
		DECLARE @numSalesOrPurType as numeric(9)              
		DECLARE @numRecurringId as numeric(9)
		DECLARE @numCurrencyID as numeric(9)=0
		DECLARE @DealStatus as tinyint =0
		DECLARE @numStatus AS NUMERIC(9)
		DECLARE @vcOppRefOrderNo VARCHAR(100)
		DECLARE @numBillAddressId numeric(9)=0
		DECLARE @numShipAddressId numeric(9)=0
		DECLARE @bitStockTransfer BIT=0
		DECLARE @WebApiId INT = 0
		DECLARE @tintSourceType TINYINT=0
		DECLARE @bitDiscountType as bit
		DECLARE @fltDiscount  as float
		DECLARE @bitBillingTerms as bit
		DECLARE @intBillingDays as integer
		DECLARE @bitInterestType as bit
		DECLARE @fltInterest as float
		DECLARE @tintTaxOperator AS TINYINT
		DECLARE @numDiscountAcntType AS NUMERIC(9)
		DECLARE @vcWebApiOrderNo VARCHAR(100)=NULL
		DECLARE @vcCouponCode		VARCHAR(100) = ''
		DECLARE @vcMarketplaceOrderID VARCHAR(100)=NULL
		DECLARE @vcMarketplaceOrderDate  datetime=NULL
		DECLARE @vcMarketplaceOrderReportId VARCHAR(100)=NULL
		DECLARE @numPercentageComplete NUMERIC(9)
		DECLARE @bitUseShippersAccountNo BIT = 0
		DECLARE @bitUseMarkupShippingRate BIT = 0
		DECLARE @numMarkupShippingRate NUMERIC(19,2) = 0
		DECLARE @intUsedShippingCompany INT = 0
		DECLARE @numShipmentMethod NUMERIC(18,0)=0
		DECLARE @dtReleaseDate DATE = NULL
		DECLARE @numPartner NUMERIC(18,0)=0
		DECLARE @tintClickBtn INT=0
		DECLARE @numPartenerContactId NUMERIC(18,0)=0
		DECLARE @numAccountClass NUMERIC(18,0) = 0
		DECLARE @numWillCallWarehouseID NUMERIC(18,0) = 0
		DECLARE @numVendorAddressID NUMERIC(18,0) = 0

		--GET VENDOR AND MASTER PO ID
		SELECT @VendorID=numVendorID, @MasterPOID=numMasterPOID FROM @Vendor WHERE ID = @i

		--GET MASTER PO FIELDS 
		SELECT
			@numOppID=numOppId,@numContactId =numContactId,@numDivisionId=numDivisionId,@tintSource=tintSource,@vcPOppName=vcPOppName,@Comments=txtComments,
			@bitPublicFlag=bitPublicFlag,@monPAmount=monPAmount,@numAssignedTo=numAssignedTo,@strMilestone=null,@dtEstimatedCloseDate=intPEstimatedCloseDate,
			@CampaignID=numCampainID,@lngPConclAnalysis=lngPConclAnalysis,@tintOppType=tintOppType,@tintActive=tintActive,@numSalesOrPurType=numSalesOrPurType,
			@numRecurringId=null,@numCurrencyID=numCurrencyID,@DealStatus=tintOppStatus,@numStatus=numStatus,@vcOppRefOrderNo=vcOppRefOrderNo,@numBillAddressId=0,
			@numShipAddressId=0,@bitStockTransfer=0,@WebApiId=0,@tintSourceType=tintSourceType,@bitDiscountType=bitDiscountType,@fltDiscount=fltDiscount,
			@bitBillingTerms=bitBillingTerms,@intBillingDays=intBillingDays,@bitInterestType=bitInterestType,@fltInterest=fltInterest,@tintTaxOperator=tintTaxOperator,
			@numDiscountAcntType=numDiscountAcntType,@vcWebApiOrderNo=vcWebApiOrderNo,@vcCouponCode=vcCouponCode,@vcMarketplaceOrderID=vcMarketplaceOrderID,
			@vcMarketplaceOrderDate=bintCreatedDate,@vcMarketplaceOrderReportId=vcMarketplaceOrderReportId,@numPercentageComplete= numPercentageComplete,
			@bitUseShippersAccountNo=bitUseShippersAccountNo,@bitUseMarkupShippingRate=bitUseMarkupShippingRate,@numMarkupShippingRate=numMarkupShippingRate,
			@intUsedShippingCompany= intUsedShippingCompany,@numShipmentMethod=numShipmentMethod,@dtReleaseDate=dtReleaseDate,@numPartner=numPartner,@numPartenerContactId=numPartenerContact
			,@numAccountClass=numAccountClass,@numVendorAddressID=numVendorAddressID
		FROM
			OpportunityMaster
		WHERE
			numDomainId = @numDomainID
			AND numOppId = @MasterPOID 

		--CLEAR DATA OF PREVIOUS ITERATION
		DELETE FROM @TEMPMasterItems
		DELETE FROM @TEMPChildItems

		
		--GET ITEMS OF MASTER PO
		INSERT INTO 
			@TEMPMasterItems
		SELECT 
			numoppitemtCode,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,numToWarehouseItemID,
			2,vcType,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount,vcItemName,numUOMId,bitWorkOrder,numVendorWareHouse,
			numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,vcAttributes,bitItemPriceApprovalRequired
		FROM 
			OpportunityItems 
		WHERE 
			numOppId = @MasterPOID

		--GET ITEMS OF PO NEEDS TO BE MERGED TO MASTER PO
		INSERT INTO 
			@TEMPChildItems
		SELECT 
			ROW_NUMBER() OVER(ORDER BY numoppitemtCode),numoppitemtCode,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,numToWarehouseItemID,
			1,vcType,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount,vcItemName,numUOMId,bitWorkOrder,numVendorWareHouse,
			numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,vcAttributes,bitItemPriceApprovalRequired
		FROM 
			OpportunityItems 
		WHERE 
			numOppId IN (SELECT numPOID FROM @VendorPOToMerger WHERE numVendorID=@VendorID AND numPOID <> @MasterPOID)

		DECLARE @numChildItemCode NUMERIC(18,0) = 0
		DECLARE @numChildWaerehouseItemID NUMERIC(18,0) = 0
		DECLARE @numChildUnitHour FLOAT = 0
		DECLARE @monChildPrice MONEY = 0
		DECLARE @K AS INT = 1
		DECLARE @KCOUNT AS INT
		SELECT @KCOUNT=COUNT(*) FROM @TEMPChildItems

		-- LOOP ITEMS OF EACH PO NEEDS TO MERGED
		WHILE @K <= @KCOUNT
		BEGIN
			SELECT 
				@numChildItemCode=numItemCode,
				@numChildWaerehouseItemID=ISNULL(numWarehouseItmsID,0),
				@numChildUnitHour=ISNULL(numUnitHour,0), 
				@monChildPrice=ISNULL(monPrice,0) 
			FROM 
				@TEMPChildItems 
			WHERE 
				numRowID = @K

			-- IF ITEM WITH SAME ITEM ID AND WAREHOUSE IS EXIST IN MASTER ORDER THEN UPDATE QTY IN MASTER PO AND SET PRICE TO WHICHEVER LOWER
			-- ELSE ADD ITEMS MASTER PO ITEMS
			IF EXISTS(SELECT numoppitemtCode FROM @TEMPMasterItems WHERE numItemCode=@numChildItemCode AND ISNULL(numWarehouseItmsID,0)=@numChildWaerehouseItemID)
			BEGIN
				UPDATE 
					@TEMPMasterItems
				SET 
					numUnitHour = ISNULL(numUnitHour,0) + @numChildUnitHour,
					monPrice = (CASE WHEN ISNULL(monPrice,0) > @monChildPrice THEN @monChildPrice ELSE monPrice END),
					monTotAmount = (ISNULL(numUnitHour,0) + @numChildUnitHour) * (CASE WHEN ISNULL(monPrice,0) > @monChildPrice THEN @monChildPrice ELSE monPrice END),
					monTotAmtBefDiscount = (ISNULL(numUnitHour,0) + @numChildUnitHour) * (CASE WHEN ISNULL(monPrice,0) > @monChildPrice THEN @monChildPrice ELSE monPrice END)
				WHERE
					numItemCode=@numChildItemCode 
					AND ISNULL(numWarehouseItmsID,0)=@numChildWaerehouseItemID
			END
			ELSE
			BEGIN
				INSERT INTO 
					@TEMPMasterItems
				SELECT 
					numoppitemtCode,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,numToWarehouseItemID,
					1,ItemType,DropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount,vcItemName,numUOM,bitWorkOrder,numVendorWareHouse,
					numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,Attributes,bitItemPriceApprovalRequired
				FROM 
					@TEMPChildItems 
				WHERE 
					numRowID = @K

			END

			SET @K = @K + 1
		END
		
		-- Op_Flag is 2(UPDATE) FOR MASTER PO AND 1(INSERT) FOR OTHER
		SET @strItems =(
							SELECT 
								numoppitemtCode,numItemCode,CAST(numUnitHour AS DECIMAL(18,8)) AS numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,numToWarehouseItemID,
								Op_Flag, ItemType,DropShip,	bitDiscountType, CAST(fltDiscount AS DECIMAL(18,8)) AS fltDiscount, monTotAmtBefDiscount,	vcItemName,numUOM, bitWorkOrder,
								numVendorWareHouse,numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,Attributes,bitItemPriceApprovalRequired
							FROM 
								@TEMPMasterItems
							FOR XML RAW ('Item'), ROOT ('NewDataSet'), ELEMENTS
						)
		
		--UPDATE MASTER PO AND ADD MERGE PO ITEMS
		EXEC USP_OppManage @numOppID=@numOppID OUTPUT,@numContactId=@numContactId,@numDivisionId=@numDivisionId,@tintSource=@tintSource,@vcPOppName=@vcPOppName output,
							@Comments=@Comments,@bitPublicFlag=@bitPublicFlag,@numUserCntID=@numUserCntID,@monPAmount=@monPAmount,@numAssignedTo=@numAssignedTo,
							@numDomainId=@numDomainId,@strItems=@strItems,@strMilestone=@strMilestone,@dtEstimatedCloseDate=@dtEstimatedCloseDate,@CampaignID=@CampaignID,
							@lngPConclAnalysis=@lngPConclAnalysis,@tintOppType=@tintOppType,@tintActive=@tintActive,@numSalesOrPurType=@numSalesOrPurType,
							@numRecurringId=@numRecurringId,@numCurrencyID=@numCurrencyID,@DealStatus=@DealStatus,@numStatus=@numStatus,@vcOppRefOrderNo=@vcOppRefOrderNo,
							@numBillAddressId=@numBillAddressId,@numShipAddressId=@numShipAddressId,@bitStockTransfer=@bitStockTransfer,@WebApiId=@WebApiId,
							@tintSourceType=@tintSourceType,@bitDiscountType=@bitDiscountType,@fltDiscount=@fltDiscount,@bitBillingTerms=@bitBillingTerms,
							@intBillingDays=@intBillingDays,@bitInterestType=@bitInterestType,@fltInterest=@fltInterest,@tintTaxOperator=@tintTaxOperator,
							@numDiscountAcntType=@numDiscountAcntType,@vcWebApiOrderNo=@vcWebApiOrderNo,@vcCouponCode=@vcCouponCode,@vcMarketplaceOrderID=@vcMarketplaceOrderID,
							@vcMarketplaceOrderDate=@vcMarketplaceOrderDate,@vcMarketplaceOrderReportId=@vcMarketplaceOrderReportId,@numPercentageComplete=@numPercentageComplete,
							@bitUseShippersAccountNo=@bitUseShippersAccountNo,@bitUseMarkupShippingRate=@bitUseMarkupShippingRate,
							@numMarkupShippingRate=@numMarkupShippingRate,@intUsedShippingCompany=@intUsedShippingCompany,@numShipmentMethod=@numShipmentMethod,@dtReleaseDate=@dtReleaseDate,@numPartner=@numPartner,@numPartenerContactId=@numPartenerContactId
							,@numAccountClass=@numAccountClass,@numVendorAddressID=@numVendorAddressID
		
		-- DELETE MERGED PO

		---- CLEAR DATA OF PREVIOUS ITERATION
		DELETE FROM @TEMPPO

		---- GET ALL POS EXCEPT MASTER PO
		INSERT INTO @TEMPPO SELECT Row_Number() OVER(ORDER BY numPOID), numPOID FROM @VendorPOToMerger WHERE numVendorID=@VendorID AND numPOID <> @MasterPOID

		DECLARE @j AS INT = 1
		DECLARE @jCOUNT AS INT
		SELECT @jCOUNT=COUNT(*) FROM @TEMPPO

		WHILE @j <= @jCOUNT
		BEGIN
			DECLARE @POID AS INT = 0
			SELECT @POID=numPOID FROM @TEMPPO WHERE ID = @j
			
			EXEC USP_DeleteOppurtunity @numOppId=@POID,@numDomainID=@numDomainID,@numUserCntID=@numUserCntID

			SET @j = @j + 1
		END

		SET @i = @i + 1
	END
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH	
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityMaster_SearchSalesOrder')
DROP PROCEDURE USP_OpportunityMaster_SearchSalesOrder
GO
CREATE PROCEDURE [dbo].[USP_OpportunityMaster_SearchSalesOrder]
	 @numDomainID AS NUMERIC(18,0),
	 @vcOppName VARCHAR(300)
AS
BEGIN
	SELECT
		numOppId
		,vcPOppName
	FROM
		OpportunityMaster
	WHERE
		numDomainId=@numDomainID
		AND tintOppType=1
		AND tintOppStatus=1
		AND vcPOppName LIKE CONCAT('%',@vcOppName,'%')
END
GO
        
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ParentChildCustomFieldMap_GetDynamicFields')
	DROP PROCEDURE USP_ParentChildCustomFieldMap_GetDynamicFields
GO

CREATE PROCEDURE [dbo].[USP_ParentChildCustomFieldMap_GetDynamicFields]
	@tintModule TINYINT
AS
BEGIN
	DECLARE @TEMP TABLE
	(
		fld_id NUMERIC(18,0)
		,Fld_label VARCHAR(300)
	)

	If @tintModule = 2 OR @tintModule = 6
	BEGIN
		INSERT INTO @TEMP
		(
			fld_id
			,Fld_label
		)
		VALUES
		(100,'Assigned To')
		,(122,'Comments')
		,(771,'Ship Via')
	END

	SELECT * FROM @TEMP
END
GO

GO
/****** Object:  StoredProcedure [dbo].[USP_PayrollLiabilityList]    Script Date: 02/28/2009 13:55:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Siva
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_payrollliabilitylist')
DROP PROCEDURE usp_payrollliabilitylist
GO
CREATE PROCEDURE [dbo].[USP_PayrollLiabilityList]
(
    @numDomainId          AS NUMERIC(9)  = 0,
    @numDepartmentId      AS NUMERIC(9)  = 0,
    @dtStartDate          AS DATETIME,
    @dtEndDate            AS DATETIME,
    @ClientTimeZoneOffset INT,
	@numOrgUserCntID AS NUMERIC(18,0),
	@tintUserRightType INT = 3,
	@tintUserRightForUpdate INT = 3
)
AS
BEGIN
	DECLARE @TempCommissionPaidCreditMemoOrRefund TABLE
	(
		numUserCntID NUMERIC(18,0),
		numReturnHeaderID NUMERIC(18,0),
		tintReturnType TINYINT,
		numReturnItemID NUMERIC(18,0),
		monCommission MONEY,
		numComRuleID NUMERIC(18,0),
		tintComType TINYINT,
		tintComBasedOn TINYINT,
		decCommission FLOAT,
		tintAssignTo TINYINT
	)    

	INSERT INTO 
		@TempCommissionPaidCreditMemoOrRefund 
	SELECT 
		numUserCntID,
		numReturnHeaderID,
		tintReturnType,
		numReturnItemID,
		monCommission,
		numComRuleID,
		tintComType,
		tintComBasedOn,
		decCommission,
		tintAssignTo
	FROM 
		dbo.GetCommissionPaidCreditMemoOrRefund(@numDomainId,@ClientTimeZoneOffset,@dtStartDate,@dtEndDate)

	DECLARE @TempCommissionUnPaidCreditMemoOrRefund TABLE
	(
		numUserCntID NUMERIC(18,0),
		numReturnHeaderID NUMERIC(18,0),
		tintReturnType TINYINT,
		numReturnItemID NUMERIC(18,0),
		monCommission MONEY,
		numComRuleID NUMERIC(18,0),
		tintComType TINYINT,
		tintComBasedOn TINYINT,
		decCommission FLOAT,
		tintAssignTo TINYINT
	)    

	INSERT INTO 
		@TempCommissionUnPaidCreditMemoOrRefund 
	SELECT 
		numUserCntID,
		numReturnHeaderID,
		tintReturnType,
		numReturnItemID,
		monCommission,
		numComRuleID,
		tintComType,
		tintComBasedOn,
		decCommission,
		tintAssignTo
	FROM 
		dbo.GetCommissionUnPaidCreditMemoOrRefund(@numDomainId,@ClientTimeZoneOffset,@dtStartDate,@dtEndDate)

	
	-- TEMPORARY TABLE TO HOLD DATA
	CREATE TABLE #tempHrs 
	(
		ID INT IDENTITY(1,1),
		numUserId NUMERIC,
		vcteritorries VARCHAR(300),
		vcTaxID VARCHAR(100),
		vcUserName VARCHAR(100),
		vcdepartment VARCHAR(100),
		bitLinkVisible BIT,
		bitpayroll BIT,
		numUserCntID NUMERIC,
		vcEmployeeId VARCHAR(50),
		monHourlyRate MONEY,
		decRegularHrs decimal(10,2),
		decPaidLeaveHrs decimal(10,2),
		monExpense MONEY,
		monReimburse MONEY,
		monCommPaidInvoice MONEY,
		monCommPaidInvoiceDeposited MONEY,
		monCommUNPaidInvoice MONEY,
		monCommPaidCreditMemoOrRefund MONEY,
		monCommUnPaidCreditMemoOrRefund MONEY,
		monTotalAmountDue MONEY,
		monAmountPaid MONEY, 
		bitCommissionContact BIT,
		numDivisionID NUMERIC(18,0),
		bitPartner BIT,
		tintCRMType TINYINT
	)

	-- GET EMPLOYEE OF DOMAIN
	INSERT INTO #tempHrs
	(
		numUserId,
		vcteritorries,
		vcTaxID,
		vcUserName,
		vcdepartment,
		bitLinkVisible,
		bitpayroll,
		numUserCntID,
		vcEmployeeId,
		monHourlyRate,
		bitCommissionContact
	)
	SELECT 
		UM.numUserId,
		CONCAT(',',STUFF((SELECT CONCAT(',',numTerritoryID) FROM UserTerritory WHERE numUserCntID=UM.numUserDetailId AND numDomainID=@numDomainId FOR XML PATH('')),1,1,''),','),
		ISNULL(adc.vcTaxID,''),
		Isnull(ADC.vcfirstname + ' ' + adc.vclastname,'-') vcUserName,
		dbo.fn_GetListItemName(adc.vcdepartment),
		(CASE @tintUserRightForUpdate
			WHEN 0 THEN 0
			WHEN 1 THEN CASE WHEN UM.numUserDetailId = @numOrgUserCntID THEN 1 ELSE 0 END
			WHEN 2 THEN CASE WHEN UM.numUserDetailId IN (SELECT numUserCntID FROM UserTerritory WHERE numUserCntID <> @numOrgUserCntID AND numTerritoryID IN (SELECT numTerritoryID FROM UserTerritory WHERE numUserCntID = @numOrgUserCntID)) THEN 1 ELSE 0 END
			ELSE 1
		END),
		Isnull(um.bitpayroll,0) AS bitpayroll,
		adc.numcontactid AS numUserCntID,
		Isnull(um.vcEmployeeId,'') AS vcEmployeeId,
		ISNULL(UM.monHourlyRate,0),
		0
	FROM  
		dbo.UserMaster UM 
	JOIN 
		dbo.AdditionalContactsInformation adc 
	ON 
		adc.numDomainID=@numDomainId 
		AND adc.numcontactid = um.numuserdetailid
	WHERE 
		UM.numDomainId=@numDomainId AND (adc.vcdepartment=@numDepartmentId OR @numDepartmentId=0)
		AND 1 = (CASE @tintUserRightType
					WHEN 1 THEN CASE WHEN UM.numUserDetailId = @numOrgUserCntID THEN 1 ELSE 0 END
					WHEN 2 THEN CASE WHEN UM.numUserDetailId IN (SELECT numUserCntID FROM UserTerritory WHERE numUserCntID <> @numOrgUserCntID AND numTerritoryID IN (SELECT numTerritoryID FROM UserTerritory WHERE numUserCntID = @numOrgUserCntID)) THEN 1 ELSE 0 END
					ELSE 1
				END)

	-- GET COMMISSION CONTACTS OF DOMAIN
	INSERT INTO #tempHrs 
	(
		numUserId,
		vcTaxID,
		vcUserName,
		numUserCntID,
		vcEmployeeId,
		vcdepartment,
		bitLinkVisible,
		bitpayroll,
		monHourlyRate,
		bitCommissionContact
	)
	SELECT  
		0,
		ISNULL(A.vcTaxID,''),
		ISNULL(A.vcFirstName +' '+ A.vcLastName + '(' + C.vcCompanyName +')','-'),
		A.numContactId,
		'',
		'-',
		(CASE WHEN @tintUserRightForUpdate = 0 OR @tintUserRightForUpdate = 1 OR @tintUserRightForUpdate = 2 THEN 0 ELSE 1 END),
		0,
		0,
		1
	FROM 
		CommissionContacts CC 
	JOIN DivisionMaster D ON CC.numDivisionID=D.numDivisionID
	JOIN AdditionalContactsInformation A ON D.numDivisionID=A.numDivisionID
	JOIN CompanyInfo C ON D.numCompanyID = C.numCompanyId
	WHERE 
		CC.numDomainID=@numDomainID
		AND 1 = (CASE WHEN @tintUserRightType = 1 OR @tintUserRightType = 2 THEN 0 ELSE 1 END)


	-- GET PARTNERS OF DOMAIN
	INSERT INTO #tempHrs 
	(
		numUserId,
		vcTaxID,
		vcUserName,
		numUserCntID,
		vcEmployeeId,
		vcdepartment,
		bitLinkVisible,
		bitpayroll,
		monHourlyRate,
		numDivisionID,
		bitPartner,
		bitCommissionContact,
		tintCRMType
	)
	SELECT  
		0,
		ISNULL(A.vcTaxID,''),
		ISNULL(C.vcCompanyName,'-'),
		0,
		'',
		'-',
		(CASE WHEN @tintUserRightForUpdate = 0 OR @tintUserRightForUpdate = 1 OR @tintUserRightForUpdate = 2 THEN 0 ELSE 1 END),
		0,
		0,
		D.numDivisionID,
		1,
		1,
		tintCRMType
	FROM  
		DivisionMaster AS D 
	LEFT JOIN 
		CompanyInfo AS C
	ON 
		D.numCompanyID=C.numCompanyID
	LEFT JOIN
		AdditionalContactsInformation A
	ON
		A.numDivisionID = D.numDivisionID
		AND ISNULL(A.bitPrimaryContact,1) = 1
	WHERE 
		D.numDomainID=@numDomainID 
		AND D.vcPartnerCode <> ''
		AND 1 = (CASE WHEN @tintUserRightType = 1 OR @tintUserRightType = 2 THEN 0 ELSE 1 END)
	ORDER BY
		vcCompanyName

	-- FETCH ALLREADY PAID COMMISSION DATA AND STORE IN TEMPORARY TABLE
	SELECT * INTO #tempPayrollTracking FROM PayrollTracking WHERE numDomainId=@numDomainId

	Declare @decTotalHrsWorked as decimal(10,2)
	DECLARE @decTotalPaidLeaveHrs AS DECIMAL(10,2)
	DECLARE @monExpense MONEY,@monReimburse MONEY,@monCommPaidInvoice MONEY,@monCommPaidInvoiceDepositedToBank MONEY,@monCommUNPaidInvoice MONEY,@monCommPaidCreditMemoOrRefund MONEY,@monCommUnPaidCreditMemoOrRefund MONEY

	DECLARE @i INT = 1
	DECLARE @COUNT INT = 0
	DECLARE @numUserCntID NUMERIC(18,0)
	DECLARE @numDivisionID NUMERIC(18,0)
	DECLARE @bitCommissionContact BIT
    
	SELECT @COUNT=COUNT(*) FROM #tempHrs

	-- LOOP OVER EACH EMPLOYEE
	WHILE @i <= @COUNT
	BEGIN
		SELECT @decTotalHrsWorked=0,@decTotalPaidLeaveHrs=0,@monExpense=0,@monReimburse=0,@monCommPaidInvoice=0,@monCommPaidInvoiceDepositedToBank=0,@monCommUNPaidInvoice=0, @numUserCntID=0,@bitCommissionContact=0,@monCommPaidCreditMemoOrRefund=0,@monCommUnPaidCreditMemoOrRefund=0
		
		SELECT
			@numUserCntID = ISNULL(numUserCntID,0),
			@bitCommissionContact = ISNULL(bitCommissionContact,0),
			@numDivisionID=ISNULL(numDivisionID,0)
		FROM
			#tempHrs
		WHERE
			ID = @i
		
		-- EMPLOYEE (ONLY COMMISSION AMOUNT IS NEEDED FOR COMMISSION CONTACTS)
		IF @bitCommissionContact = 0
		BEGIN
			-- CALCULATE REGULAR HRS
			SELECT 
				@decTotalHrsWorked=sum(x.Hrs) 
			FROM 
				(
					SELECT  
						ISNULL(SUM(CAST(DATEDIFF(MINUTE,dtfromdate,dttodate) AS FLOAT)/CAST(60 AS FLOAT)),0) AS Hrs            
					FROM 
						TimeAndExpense 
					WHERE 
						(numType=1 OR numType=2) 
						AND numCategory=1                 
						AND (
								(DATEADD(MINUTE,-@ClientTimeZoneOffset,dtFromDate) BETWEEN @dtStartDate AND @dtEndDate) 
								OR 
								(DATEADD(MINUTE,-@ClientTimeZoneOffset,dtToDate) BETWEEN @dtStartDate AND @dtEndDate)
							) 
						AND numUserCntID=@numUserCntID  
						AND numDomainID=@numDomainId 
						AND numCategoryHDRID NOT IN (SELECT numCategoryHDRID FROM #tempPayrollTracking WHERE ISNULL(numCategoryHDRID,0)>0)
				) x

			-- CALCULATE PAID LEAVE HRS
			SELECT 
				@decTotalPaidLeaveHrs=ISNULL(
												SUM( 
														CASE 
														WHEN dtfromdate = dttodate 
														THEN 
															24 - (CASE WHEN bitfromfullday = 0 THEN 12 ELSE 0 END)
														ELSE 
															(Datediff(DAY,dtfromdate,dttodate) + 1) * 24 - (CASE WHEN bitfromfullday = 0 THEN 12 ELSE 0 END) - (CASE WHEN bittofullday = 0 THEN 12 ELSE 0 END)
														 END
													)
											 ,0)
            FROM   
				TimeAndExpense
            WHERE  
				numtype = 3 
				AND numcategory = 3 
				AND numusercntid = @numUserCntID 
				AND numdomainid = @numDomainId 
                AND (
						(DATEADD(MINUTE,-@ClientTimeZoneOffset,dtFromDate) BETWEEN @dtStartDate AND @dtEndDate)
						Or 
						(DATEADD(MINUTE,-@ClientTimeZoneOffset,dtToDate) BETWEEN @dtStartDate AND @dtEndDate)
					)
				AND numCategoryHDRID NOT IN (SELECT numCategoryHDRID FROM #tempPayrollTracking WHERE ISNULL(numCategoryHDRID,0)>0)

			-- CALCULATE EXPENSES
			SELECT 
				@monExpense=ISNULL((SUM(CAST(monAmount AS FLOAT))),0)    
			FROM 
				TimeAndExpense 
			WHERE 
				numCategory=2 
				AND numType in (1,2) 
				AND numUserCntID=@numUserCntID 
				AND numDomainID=@numDomainId 
				AND ((dtFromDate BETWEEN @dtStartDate And @dtEndDate) Or (dtToDate BETWEEN @dtStartDate And @dtEndDate)) 
				AND numCategoryHDRID NOT IN (SELECT numCategoryHDRID FROM #tempPayrollTracking WHERE ISNULL(numCategoryHDRID,0)>0)

			-- CALCULATE REIMBURSABLE EXPENSES
			SELECT 
				@monReimburse=Isnull((SUM(CAST(monamount AS FLOAT))),0)
            FROM   
				TimeAndExpense 
            WHERE  
				bitreimburse = 1 
				AND numcategory = 2 
                AND numusercntid = @numUserCntID 
				AND numdomainid = @numDomainId
                AND dtfromdate BETWEEN @dtStartDate AND @dtEndDate
                AND numCategoryHDRID NOT IN (SELECT numCategoryHDRID FROM #tempPayrollTracking WHERE ISNULL(numCategoryHDRID,0)>0)

		END

		-- CALCULATE COMMISSION PAID INVOICE
		SELECT 
			@monCommPaidInvoice=ISNULL(SUM(Amount),0) 
		FROM 
			(
				SELECT 
					oppBiz.numOppID,
					oppBiz.numOppBizDocsID,
					ISNULL(SUM(BC.numComissionAmount - ISNULL(PT.monCommissionAmt,0)),0) AS Amount
				FROM 
					OpportunityMaster Opp 
				INNER JOIN OpportunityBizDocs oppBiz on oppBiz.numOppId=opp.numOppId 
				INNER JOIN BizDocComission BC on BC.numOppBizDocId=oppBiz.numOppBizDocsId 
				LEFT JOIN #tempPayrollTracking PT ON PT.numComissionID=BC.numComissionID
				CROSS APPLY
				(
					SELECT 
						MAX(DM.dtDepositDate) dtDepositDate
					FROM 
						DepositMaster DM 
					JOIN dbo.DepositeDetails DD	ON DM.numDepositId=DD.numDepositID 
					WHERE 
						DM.tintDepositePage = 2
						AND DM.numDomainId=@numDomainId
						AND DD.numOppID=oppBiz.numOppID 
						AND DD.numOppBizDocsID =oppBiz.numOppBizDocsID
				) TEMPDeposit
				WHERE 
					Opp.numDomainId=@numDomainId 
					AND ((BC.numUserCntId=@numUserCntID AND ISNULL(BC.tintAssignTo,0) <> 3) OR (BC.numUserCntID=@numDivisionID AND BC.tintAssignTo=3))
					AND BC.bitCommisionPaid=0 
					AND oppBiz.bitAuthoritativeBizDocs = 1 
					AND isnull(oppBiz.monAmountPaid,0) >=oppBiz.monDealAmount 
					AND CAST(DATEADD(MINUTE,-@ClientTimeZoneOffset,TEMPDeposit.dtDepositDate) AS DATE) BETWEEN CAST(@dtStartDate AS DATE) AND CAST(@dtEndDate AS DATE)
				GROUP BY 
					oppBiz.numOppID,
					oppBiz.numOppBizDocsID
			) A

		-- CALCULATE COMMISSION PAID INVOICE (DEPOSITED TO BANK)
		SELECT 
			@monCommPaidInvoiceDepositedToBank=ISNULL(SUM(Amount),0) 
		FROM 
			(
				SELECT 
					oppBiz.numOppID,
					oppBiz.numOppBizDocsID,
					ISNULL(SUM(BC.numComissionAmount - ISNULL(PT.monCommissionAmt,0)),0) AS Amount
				FROM 
					OpportunityMaster Opp 
				INNER JOIN OpportunityBizDocs oppBiz on oppBiz.numOppId=opp.numOppId 
				INNER JOIN BizDocComission BC on BC.numOppBizDocId=oppBiz.numOppBizDocsId 
				LEFT JOIN #tempPayrollTracking PT ON PT.numComissionID=BC.numComissionID
				CROSS APPLY
				(
					SELECT 
						MAX(DM.dtDepositDate) dtDepositDate
					FROM 
						DepositMaster DM 
					JOIN dbo.DepositeDetails DD	ON DM.numDepositId=DD.numDepositID 
					WHERE 
						DM.tintDepositePage = 2
						AND DM.numDomainId=@numDomainId
						AND DD.numOppID=oppBiz.numOppID 
						AND DD.numOppBizDocsID =oppBiz.numOppBizDocsID
				) TEMPDeposit
				WHERE 
					Opp.numDomainId=@numDomainId 
					AND ((BC.numUserCntId=@numUserCntID AND ISNULL(BC.tintAssignTo,0) <> 3) OR (BC.numUserCntID=@numDivisionID AND BC.tintAssignTo=3))
					AND BC.bitCommisionPaid=0 
					AND oppBiz.bitAuthoritativeBizDocs = 1 
					AND isnull(oppBiz.monAmountPaid,0) >=oppBiz.monDealAmount 
					AND CAST(DATEADD(MINUTE,-@ClientTimeZoneOffset,TEMPDeposit.dtDepositDate) AS DATE) BETWEEN CAST(@dtStartDate AS DATE) AND CAST(@dtEndDate AS DATE)
					AND 1 = (CASE WHEN EXISTS 
								(SELECT 
									DM.numDepositId 
								FROM 
									DepositeDetails DD 
								INNER JOIN 
									DepositMaster DM 
								ON 
									DD.numDepositID=DM.numDepositId 
								WHERE 
									numDomainId=@numDomainId 
									AND DD.numOppID=Opp.numOppId 
									AND DD.numOppBizDocsID=oppBiz.numOppBizDocsId 
									AND DM.tintDepositePage=2 
									AND 1 = (CASE WHEN DM.tintDepositeToType=2 THEN (CASE WHEN ISNULL(DM.bitDepositedToAcnt,0)=1 THEN 1 ELSE 0 END) ELSE 1 END)
								)
							 THEN 1 ELSE 0 END)
				GROUP BY 
					oppBiz.numOppID,
					oppBiz.numOppBizDocsID
			) A
		

		-- CALCULATE COMMISSION UNPAID INVOICE 
		SELECT 
			@monCommUNPaidInvoice=isnull(sum(BC.numComissionAmount - ISNULL(PT.monCommissionAmt,0)),0) 
		FROM 
			OpportunityMaster Opp 
		INNER JOIN OpportunityBizDocs oppBiz on oppBiz.numOppId=opp.numOppId 
        INNER JOIN BizDocComission BC on BC.numOppBizDocId=oppBiz.numOppBizDocsId
        LEFT JOIN #tempPayrollTracking PT ON PT.numComissionID=BC.numComissionID 
		WHERE 
			Opp.numDomainId=@numDomainId 
			AND ((BC.numUserCntId=@numUserCntID AND ISNULL(BC.tintAssignTo,0) <> 3) OR (BC.numUserCntID=@numDivisionID AND BC.tintAssignTo=3))
			AND BC.bitCommisionPaid=0 
			AND oppBiz.bitAuthoritativeBizDocs = 1 
			AND ISNULL(oppBiz.monAmountPaid,0) <oppBiz.monDealAmount
			AND ((DATEADD(MINUTE,-@ClientTimeZoneOffset,oppBiz.dtFromDate) BETWEEN @dtStartDate AND @dtEndDate))	

		-- CALCULATE COMMISSION FOR PAID CREDIT MEMO/REFUND
		SET @monCommPaidCreditMemoOrRefund = ISNULL((SELECT SUM(monCommission) FROM @TempCommissionPaidCreditMemoOrRefund WHERE ((numUserCntId=@numUserCntID AND ISNULL(tintAssignTo,0) <> 3) OR (numUserCntID=@numDivisionID AND tintAssignTo=3))),0)		
		-- CALCULATE COMMISSION FOR UN-PAID CREDIT MEMO/REFUND
		SET @monCommUnPaidCreditMemoOrRefund = ISNULL((SELECT SUM(monCommission) FROM @TempCommissionUnPaidCreditMemoOrRefund WHERE ((numUserCntId=@numUserCntID AND ISNULL(tintAssignTo,0) <> 3) OR (numUserCntID=@numDivisionID AND tintAssignTo=3))),0)
	
		UPDATE 
			#tempHrs 
		SET    
			decRegularHrs=@decTotalHrsWorked,
			decPaidLeaveHrs=@decTotalPaidLeaveHrs,
			monExpense=@monExpense,
			monReimburse=@monReimburse,
			monCommPaidInvoice=@monCommPaidInvoice,
			monCommPaidInvoiceDeposited=@monCommPaidInvoiceDepositedToBank,
			monCommUNPaidInvoice=@monCommUNPaidInvoice,
			monCommPaidCreditMemoOrRefund=@monCommPaidCreditMemoOrRefund,
			monCommUnPaidCreditMemoOrRefund=@monCommUnPaidCreditMemoOrRefund,
			monTotalAmountDue= @decTotalHrsWorked * monHourlyRate + @monReimburse + @monCommPaidInvoice + @monCommUNPaidInvoice - ISNULL(@monCommPaidCreditMemoOrRefund,0)
        WHERE 
			(numUserCntID=@numUserCntID AND ISNULL(bitPartner,0) = 0) 
			OR (numDivisionID=@numDivisionID AND ISNULL(bitPartner,0) = 1)

		SET @i = @i + 1
	END

	-- CALCULATE PAID AMOUNT
	UPDATE 
		temp 
	SET 
		monAmountPaid=ISNULL(PD.monTotalAmt,0) 
	FROM 
		#tempHrs temp  
	JOIN dbo.PayrollDetail PD ON PD.numUserCntID=temp.numUserCntID
	JOIN dbo.PayrollHeader PH ON PH.numPayrollHeaderID=PD.numPayrollHeaderID
	WHERE 
		ISNULL(PD.numCheckStatus,0)=2  
		AND ((dtFromDate BETWEEN @dtStartDate AND @dtEndDate) OR (dtToDate BETWEEN @dtStartDate And @dtEndDate))
 
	SELECT * FROM #tempHrs
	DROP TABLE #tempPayrollTracking
	DROP TABLE #tempHrs
END
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ProjectList1')
DROP PROCEDURE USP_ProjectList1
GO
CREATE PROCEDURE [dbo].[USP_ProjectList1]                                                                
			 @numUserCntID numeric(9)=0,                                              
			 @numDomainID numeric(9)=0,                                              
			 @tintUserRightType tinyint=0,                                              
			 @tintSortOrder tinyint=4,                                                    
			 @SortChar char(1)='0',                                                                                           
			 @CurrentPage int,                                              
			 @PageSize int,                                              
			 @TotRecs int output,                                              
			 @columnName as Varchar(50),                                              
			 @columnSortOrder as Varchar(10),                                              
			 @numDivisionID as numeric(9) ,                                             
			 @bitPartner as bit=0,          
			 @ClientTimeZoneOffset as int,
			 @numProjectStatus as numeric(9),
			 @vcRegularSearchCriteria varchar(MAX)='',
			 @vcCustomSearchCriteria varchar(MAX)='' ,
			 @SearchText VARCHAR(100) = '',
			 @tintDashboardReminderType TINYINT = 0                                     
as                                              
     BEGIN TRY       
	CREATE TABLE #tempForm (tintOrder TINYINT,vcDbColumnName nvarchar(50),vcOrigDbColumnName nvarchar(50),vcFieldName nvarchar(50),vcAssociatedControlType nvarchar(50),
	vcListItemType varchar(3),numListID numeric(9),vcLookBackTableName varchar(50),bitCustomField BIT,
	numFieldId NUMERIC,bitAllowSorting BIT,bitAllowEdit BIT,bitIsRequired BIT,bitIsEmail BIT,bitIsAlphaNumeric BIT,bitIsNumeric BIT,
	bitIsLengthValidation BIT,intMaxLength int,intMinLength int,bitFieldMessage BIT,vcFieldMessage VARCHAR(500),ListRelID numeric(9),intColumnWidth INT,bitAllowFiltering BIT,vcFieldDataType CHAR(1))
                
	-- FIRST CHECK IF GRID COLUMNS ARE CONFIGURED
	DECLARE  @Nocolumns  AS TINYINT = 0		             
	SET @Nocolumns=0                  
	SELECT 
		@Nocolumns=isnull(sum(TotalRow),0)  
	FROM
	(            
		SELECT 
			count(*) TotalRow 
		FROM 
			View_DynamicColumns 
		WHERE 
			numFormId=13 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1
		UNION
		SELECT 
			COUNT(*) TotalRow 
		FROM 
			View_DynamicCustomColumns 
		WHERE 
			numFormId=13 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1
		) TotalRows
		
	-- IF GRID COLUMNS ARE NOT CONFIGURES THEN CHECK IF MASTER FORM CONFIGURATION IS CREATED BY ADMINISTRATOR
	DECLARE @IsMasterConfAvailable BIT = 0
	DECLARE @numUserGroup NUMERIC(18,0)

	IF @Nocolumns=0
	BEGIN
		SELECT @numUserGroup = numGroupID FROM UserMaster WHERE numUserDetailId = @numUserCntID 

		IF (SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = @numDomainID AND numFormID = 13 AND bitGridConfiguration = 1 AND numGroupID = @numUserGroup) > 0
		BEGIN
			SET @IsMasterConfAvailable = 1
		END
	END


	--IF MASTERCONFIGURATION IS AVAILABLE THEN LOAD IT OTHERWISE LOAD DEFAULT COLUMNS
	IF @IsMasterConfAvailable = 1
BEGIN
	INSERT INTO DycFormConfigurationDetails 
	(
		numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth
	)
	SELECT 
		13,numFieldId,intColumnNum,intRowNum,@numDomainID,@numUserCntID,NULL,1,0,intColumnWidth 
	FROM 
		View_DynamicColumnsMasterConfig
	WHERE
		View_DynamicColumnsMasterConfig.numFormId=13 AND 
		View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
		View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
		View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
		ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
		ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
	UNION
    SELECT 
		13,numFieldId,intColumnNum,intRowNum,@numDomainID,@numUserCntID,NULL,1,1,intColumnWidth
	FROM 
		View_DynamicCustomColumnsMasterConfig
	WHERE 
		View_DynamicCustomColumnsMasterConfig.numFormId=13 AND 
		View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
		View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
		View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
		ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1

	INSERT INTO #tempForm
	SELECT 
		(intRowNum + 1) as tintOrder, vcDbColumnName,	vcOrigDbColumnName,	ISNULL(vcCultureFieldName,vcFieldName), vcAssociatedControlType,
		vcListItemType, numListID, vcLookBackTableName,	bitCustom, numFieldId, bitAllowSorting,	bitInlineEdit, bitIsRequired,
		bitIsEmail,	bitIsAlphaNumeric, bitIsNumeric, bitIsLengthValidation,	intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,
		ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType 
	FROM 
		View_DynamicColumnsMasterConfig
	WHERE
		View_DynamicColumnsMasterConfig.numFormId=13 AND 
		View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
		View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
		View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
		ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
		ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
	UNION
    SELECT 
		tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName, vcAssociatedControlType,'' as vcListItemType,numListID,'',
		bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
		intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,''
	FROM 
		View_DynamicCustomColumnsMasterConfig
	WHERE 
		View_DynamicCustomColumnsMasterConfig.numFormId=13 AND 
		View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
		View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
		View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
		ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1
	ORDER BY 
		tintOrder ASC  
END
ELSE                                        
BEGIN
	if @Nocolumns=0
BEGIN
INSERT INTO DycFormConfigurationDetails (numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth)
select 13,numFieldId,0,Row_number() over(order by tintRow desc),@numDomainID,@numUserCntID,NULL,1,0,intColumnWidth
 FROM View_DynamicDefaultColumns
  where numFormId=13 and bitDefault=1 AND ISNULL(bitSettingField,0)=1 and numDomainID=@numDomainID                
  order by tintOrder asc 

END
             
	INSERT INTO #tempForm
select tintRow+1 as tintOrder,vcDbColumnName,vcOrigDbColumnName,ISNULL(vcCultureFieldName,vcFieldName),                  
 vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName                                                 
,bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType  
 FROM View_DynamicColumns 
 where numFormId=13 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 
  AND ISNULL(bitSettingField,0)=1 AND ISNULL(bitCustom,0)=0
       
       UNION
    
      select tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName,                  
 vcAssociatedControlType,'' as vcListItemType,numListID,''                                                 
,bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,'' 
 from View_DynamicCustomColumns
 where numFormId=13 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 
 AND ISNULL(bitCustom,0)=1
 
  order by tintOrder asc  
END 	

	DECLARE  @strColumns AS VARCHAR(MAX)
	SET @strColumns=' addCon.numContactId,Pro.vcProjectName,Div.numDivisionID,isnull(Div.numTerID,0) as numTerID,pro.numCustPrjMgr,Div.tintCRMType,pro.numProId,Pro.numRecOwner'
	
	declare @tintOrder as tinyint                                                    
	declare @vcFieldName as varchar(50)                                                    
	declare @vcListItemType as varchar(3)                                               
	declare @vcListItemType1 as varchar(1)                                                   
	declare @vcAssociatedControlType varchar(20)                                                    
	declare @numListID AS numeric(9)                                                    
	declare @vcDbColumnName varchar(20)                        
                      
	declare @vcLookBackTableName varchar(2000)                  
	Declare @bitCustom as bit  
	DECLARE @bitAllowEdit AS CHAR(1)                 
	Declare @numFieldId as numeric  
	DECLARE @bitAllowSorting AS CHAR(1) 
	declare @vcColumnName AS VARCHAR(500)    
                  
	set @tintOrder=0                         
	Declare @ListRelID as numeric(9) 
	DECLARE @SearchQuery AS VARCHAR(MAX) = '' 

	SELECT TOP 1 
		@tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName,                
		@vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,@numListID=numListID,@vcLookBackTableName=vcLookBackTableName                                               
		,@bitCustom=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,@bitAllowEdit=bitAllowEdit,
		@ListRelID=ListRelID
	FROM  
		#tempForm 
	ORDER BY 
		tintOrder asc  
	Declare @WhereCondition varchar(MAX)  =''
	while @tintOrder>0                                                    
	BEGIN         
     PRINT @vcDbColumnName
	 PRINT @vcListItemType   
	 PRINT @vcAssociatedControlType                                       
	IF @bitCustom = 0          
	BEGIN          
--print @vcAssociatedControlType        
--print @vcFieldName  --print @vcDbColumnName        
  declare @Prefix as varchar(5)                  
      if @vcLookBackTableName = 'AdditionalContactsInformation'                  
    set @Prefix = 'ADC.'                  
      if @vcLookBackTableName = 'DivisionMaster'                  
    set @Prefix = 'DM.'                  
      if @vcLookBackTableName = 'ProjectsMaster'                  
    set @PreFix ='Pro.'        
    
    SET @vcColumnName= @vcDbColumnName+'~'+ convert(varchar(10),@numFieldId) +'~0'

    IF @vcAssociatedControlType='SelectBox'                                                    
    BEGIN      
		                                                           
		IF @vcListItemType='LI'                                                     
		BEGIN                                                    
		  SET @strColumns= @strColumns + ',L' + convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName+']'
		  
		  IF LEN(@SearchText) > 0
		  BEGIN 
			SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'L' + CONVERT(VARCHAR(3),@tintOrder) + '.vcData LIKE ''%' + @SearchText + '%'' '     
		  END   
		                                          
		  SET @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID='+@vcDbColumnName                                                    
			PRINT @WhereCondition   
		END                                                    
		ELSE IF @vcListItemType='S'                                                     
		BEGIN                                                    
		  SET @strColumns=@strColumns + ',S' + convert(varchar(3),@tintOrder)+'.vcState'+' ['+ @vcColumnName+']'     
		    
		  IF LEN(@SearchText) > 0
		  BEGIN 
			SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'S' + CONVERT(VARCHAR(3),@tintOrder) + '.vcState LIKE ''%' + @SearchText + '%'' '     
		  END                
		                                  
		  SET @WhereCondition= @WhereCondition +' left join State S'+ convert(varchar(3),@tintOrder)+ ' on S'+convert(varchar(3),@tintOrder)+ '.numStateID='+@vcDbColumnName                                                    
		END   
		ELSE IF @vcListItemType='PP'
		BEGIN                                                    
		  SET @strColumns=@strColumns + ',ISNULL(PP.intTotalProgress,0)'+' ['+ @vcColumnName+']'   
		  
		  IF LEN(@SearchText) > 0
		  BEGIN 
			SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'ISNULL(PP.intTotalProgress,0) LIKE ''%' + @SearchText + '%'' '
		  END
		END   
		ELSE IF @vcListItemType='T'                                                     
		BEGIN        
		  SET @strColumns=@strColumns + ',L' + convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName+']'  
		                
		  IF LEN(@SearchText) > 0
		  BEGIN
			SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'L' + CONVERT(VARCHAR(3),@tintOrder) + '.vcData LIKE ''%' + @SearchText + '%'' '
		  END                                       
		                                           
		  SET @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID='+@vcDbColumnName                                                    
		END                   
		ELSE IF   @vcListItemType='U'                                                 
		BEGIN                     
			SET @strColumns=@strColumns+',dbo.fn_GetContactName('+@Prefix+@vcDbColumnName+') ['+ @vcColumnName+']'  
		  IF LEN(@SearchText) > 0
		  BEGIN
			SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'dbo.fn_GetContactName(' + @Prefix + @vcDbColumnName + ') LIKE ''%' + @SearchText + '%'' '
		  END                                                  
		END                  
	END             
    
	ELSE IF @vcAssociatedControlType='DateField'                                                    
	BEGIN             
	IF @vcDbColumnName='intDueDate'
	BEGIN	
		set @strColumns=@strColumns+',case when convert(varchar(11),'+@Prefix+@vcDbColumnName+' )= convert(varchar(11),getdate()) then ''<b><font color=red>Today</font></b>'''      
		set @strColumns=@strColumns+'when convert(varchar(11),'+@Prefix+@vcDbColumnName+'  )= convert(varchar(11),dateadd(day,-1,getdate())) then ''<b><font color=purple>YesterDay</font></b>'''      
		set @strColumns=@strColumns+'when convert(varchar(11),'+@Prefix+@vcDbColumnName+'  )= convert(varchar(11),dateadd(day,1,getdate())) then''<b><font color=orange>Tommorow</font></b>'' ' 
		set @strColumns=@strColumns+'else dbo.FormatedDateFromDate(' +@Prefix+@vcDbColumnName+','+convert(varchar(10),@numDomainId)+') end  ['+ @vcColumnName+']'
		
		IF LEN(@SearchText) > 0
		BEGIN
			SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + @Prefix + @vcDbColumnName + ' LIKE ''%' + @SearchText + '%'' '
		END
	END
	ELSE
	BEGIN
        SET @strColumns=@strColumns+',case when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+','+@Prefix+@vcDbColumnName+') )= convert(varchar(11),getdate()) then ''<b><font color=red>Today</font></b>'''    
		SET @strColumns=@strColumns+'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+','+@Prefix+@vcDbColumnName+') )= convert(varchar(11),dateadd(day,-1,getdate())) then ''<b><font color=purple>YesterDay</font></b>'''    
		SET @strColumns=@strColumns+'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+','+@Prefix+@vcDbColumnName+') )= convert(varchar(11),dateadd(day,1,getdate())) then''<b><font color=orange>Tommorow</font></b>'' '    
		SET @strColumns=@strColumns+'else dbo.FormatedDateFromDate(DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+','+@Prefix+@vcDbColumnName+'),'+convert(varchar(10),@numDomainId)+') end  ['+ @vcColumnName+']'             
		
		IF LEN(@SearchText) > 0
		BEGIN
			SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + @Prefix + @vcDbColumnName + ' LIKE ''%' + @SearchText + '%'' '
		END

	END  
      
	END            
    ELSE IF @vcAssociatedControlType='TextBox'                                                    
	   BEGIN             
		 
		 SET @strColumns=@strColumns+','+ case                  
		 WHEN @vcDbColumnName='numAge' then 'year(getutcdate())-year(bintDOB)'                 
		 WHEN @vcDbColumnName='numDivisionID' then 'DM.numDivisionID'                 
		 WHEN @vcDbColumnName='textSubject' then 'convert(varchar(max),textSubject)'                 
		 ELSE @PreFix+@vcDbColumnName end+' ['+@vcColumnName+']'
		 
		 IF LEN(@SearchText) > 0
		 BEGIN
			 SET @SearchQuery = @SearchQuery +  (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + (case                  
			 WHEN @vcDbColumnName='numAge' then 'year(getutcdate())-year(bintDOB)'                 
			 WHEN @vcDbColumnName='numDivisionID' then 'DM.numDivisionID'                 
			 WHEN @vcDbColumnName='textSubject' then 'convert(varchar(max),textSubject)'                 
			 ELSE @PreFix+@vcDbColumnName end) + ' LIKE ''%' + @SearchText + '%'' '
		 END
		               
	   END  
 ELSE IF  @vcAssociatedControlType='TextArea'                                              
 BEGIN  
  SET @strColumns=@strColumns+', '''' ' +' ['+ @vcColumnName+']'            
 END 
 ELSE  IF @vcAssociatedControlType='CheckBox'                                                
 BEGIN      
	SET @strColumns=@strColumns+', isnull('+ @vcDbColumnName +',0) ['+ @vcColumnName+']'         
	IF LEN(@SearchText) > 0
	BEGIN
		SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + @vcDbColumnName + ' LIKE ''%' + @SearchText + '%'''            
	END
 END                                               
 ELSE IF @vcAssociatedControlType='Popup' and @vcDbColumnName='numShareWith'                                                   
 BEGIN      
	 SET @strColumns=@strColumns+',(SELECT SUBSTRING(
					(SELECT '','' + A.vcFirstName+'' ''+A.vcLastName + CASE WHEN ISNULL(SR.numContactType,0)>0 THEN ''('' + dbo.fn_GetListItemName(SR.numContactType) + '')'' ELSE '''' END
					FROM ShareRecord SR JOIN UserMaster UM ON UM.numUserDetailId=SR.numAssignedTo 
					JOIN AdditionalContactsInformation A ON UM.numUserDetailId=A.numContactID 
					WHERE SR.numDomainID=pro.numDomainID AND SR.numModuleID=5 AND SR.numRecordID=pro.numProId
					AND UM.numDomainID=pro.numDomainID and UM.numDomainID=A.numDomainID FOR XML PATH('''')),2,200000)) ['+ @vcColumnName+']'		           
 END       
 END                                              
 ELSE IF @bitCustom = 1        
 BEGIN          
           
--   select @vcFieldName=FLd_label,@vcAssociatedControlType=fld_type,@vcDbColumnName='Cust'+Convert(varchar(10),Fld_Id)  from CFW_Fld_Master                                           
--   where CFW_Fld_Master.Fld_Id = @numFieldId                                      
      
      SET @vcColumnName= @vcDbColumnName +'~'+ convert(varchar(10),@numFieldId)+'~1'
    
    if @vcAssociatedControlType = 'TextBox' or @vcAssociatedControlType = 'TextArea'
   begin          
             
    set @strColumns= @strColumns+',CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value  ['+ @vcColumnName + ']'             
    set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values_Pro CFW'+ convert(varchar(3),@tintOrder)+ '           
    on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=pro.numProid   '                                                   
   end          
             
   else if @vcAssociatedControlType = 'Checkbox'         
   begin          
             
    set @strColumns= @strColumns+',case when isnull(CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value,0)=0 then 0 when isnull(CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value,0)=1 then 1 end   ['+ @vcColumnName + ']'             
    set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values_Pro CFW'+ convert(varchar(3),@tintOrder)+ '           
    on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=pro.numProid   '                                                   
   end          
   else if @vcAssociatedControlType = 'DateField'         
   begin           
             
    set @strColumns= @strColumns+',dbo.FormatedDateFromDate(CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value,'+convert(varchar(10),@numDomainId)+')  ['+ @vcColumnName + ']'             
    set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values_Pro CFW'+ convert(varchar(3),@tintOrder)+ '           
    on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=pro.numProid   '                                                   
   end          
   else if @vcAssociatedControlType = 'SelectBox'       
   begin        
    set @vcDbColumnName = 'DCust'+Convert(varchar(10),@numFieldId)          
    set @strColumns=@strColumns+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName + ']'                                                    
    set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values_Pro CFW'+ convert(varchar(3),@tintOrder)+ '           
     on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=pro.numProid   '                                                   
    set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID=CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value'          
   end
   ELSE
	BEGIN
		SET @strColumns = @strColumns + CONCAT(',dbo.GetCustFldValuePro(',@numFieldId,',pro.numProID)') + ' [' + @vcColumnName + ']'
	END             
 End          
                                                  
	SELECT TOP 1 
		@tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName,                
		@vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,@numListID=numListID,@vcLookBackTableName=vcLookBackTableName,                                             
		@bitCustom=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,@bitAllowEdit=bitAllowEdit,
		@ListRelID=ListRelID
	FROM 
		#tempForm 
	WHERE 
		tintOrder > @tintOrder-1 
	ORDER BY 
		tintOrder asc            
 
	IF @@rowcount=0 
		SET @tintOrder=0 

	
	         
	END
	

	DECLARE @strShareRedordWith AS VARCHAR(MAX);
	SET @strShareRedordWith=' pro.numProId in (select SR.numRecordID FROM ShareRecord SR WHERE SR.numDomainID='+ convert(varchar(15),@numDomainID )+' AND SR.numModuleID=5 AND SR.numAssignedTo='+convert(varchar(15),@numUserCntId)+') '
 
	DECLARE @StrSql AS VARCHAR(MAX) = ''
	
	SET @StrSql = @StrSql + ' FROM ProjectsMaster pro                                                               
                                 LEFT JOIN ProjectsContacts ProCont on ProCont.numProId=Pro.numProId and ProCont.bitPartner=1 and ProCont.numContactId='+convert(varchar(15),@numUserCntID)+ '                                                              
                                 JOIN additionalContactsinformation addCon on addCon.numContactId=pro.numCustPrjMgr
								 JOIN DivisionMaster Div on Div.numDivisionID=addCon.numDivisionID    
								 JOIN CompanyInfo cmp on cmp.numCompanyID=div.numCompanyID    
								  left join ProjectProgress PP on PP.numProID=pro.numProID ' + ISNULL(@WhereCondition,'')
								

	if @columnName like 'CFW.Cust%'           
	begin                    
		set  @StrSql = @StrSql + ' left Join CFW_FLD_Values_Pro CFW on CFW.RecId=pro.numProid  and CFW.fld_id= '+replace(@columnName,'CFW.Cust','')
		set @columnName='CFW.Fld_Value'                                                            
	end                                   
	if @columnName like 'DCust%'          
	begin                                  
		set  @StrSql = @StrSql + ' left Join CFW_FLD_Values_Pro CFW on CFW.RecId=pro.numProid and CFW.fld_id= '+replace(@columnName,'DCust','')                                                                                             
		set  @StrSql = @StrSql + ' left Join ListDetails LstCF on LstCF.numListItemID=CFW.Fld_Value'
		set @columnName='LstCF.vcData'
	end

	  if @bitPartner=1 
		set @strSql=@strSql+' left join ProjectsContacts ProCont on ProCont.numProId=Pro.numProId and ProCont.bitPartner=1 and ProCont.numContactId='+convert(varchar(15),@numUserCntID)+ ''   


 -------Change Row Color-------
Declare @vcCSOrigDbCOlumnName as varchar(50),@vcCSLookBackTableName as varchar(50),@vcCSAssociatedControlType as varchar(50)
SET @vcCSOrigDbCOlumnName=''
SET @vcCSLookBackTableName=''

Create table #tempColorScheme(vcOrigDbCOlumnName varchar(50),vcLookBackTableName varchar(50),vcFieldValue varchar(50),vcFieldValue1 varchar(50),vcColorScheme varchar(50),vcAssociatedControlType varchar(50))

insert into #tempColorScheme select DFM.vcOrigDbCOlumnName,DFM.vcLookBackTableName,DFCS.vcFieldValue,DFCS.vcFieldValue1,DFCS.vcColorScheme,DFFM.vcAssociatedControlType 
from DycFieldColorScheme DFCS join DycFormField_Mapping DFFM on DFCS.numFieldID=DFFM.numFieldID
join DycFieldMaster DFM on DFM.numModuleID=DFFM.numModuleID and DFM.numFieldID=DFFM.numFieldID
where DFCS.numDomainID=@numDomainID and DFFM.numFormID=13 AND DFCS.numFormID=13 and isnull(DFFM.bitAllowGridColor,0)=1

IF(SELECT COUNT(*) FROM #tempColorScheme)>0
BEGIN
   select top 1 @vcCSOrigDbCOlumnName=vcOrigDbCOlumnName,@vcCSLookBackTableName=vcLookBackTableName,@vcCSAssociatedControlType=vcAssociatedControlType from #tempColorScheme
END   
----------------------------             
    
IF len(@vcCSOrigDbCOlumnName)>0 and len(@vcCSLookBackTableName)>0
BEGIN
set @strColumns=@strColumns+',tCS.vcColorScheme'
END
                                                                
 --set @strSql=@strSql+' ,TotalRowCount                                                                  
 --  FROM ProjectsMaster pro                          
 -- join additionalContactsinformation ADC                                              
 --  on ADC.numContactId=pro.numCustPrjMgr                                              
 -- join DivisionMaster DM                                               
 --  on DM.numDivisionID=ADC.numDivisionID                                              
 -- join CompanyInfo cmp                                             
 --  on cmp.numCompanyID=DM.numCompanyID                                            
 --    '+@WhereCondition+  '                                
 --join tblProjects T on T.numProID=Pro.numProID'                                                              
  
--' union select count(*),null,null,null,null,null,null,null '+@strColumns+' from tblProjects order by RowNumber'                 
                                                           
 
IF len(@vcCSOrigDbCOlumnName)>0 and len(@vcCSLookBackTableName)>0
BEGIN
	 if @vcCSLookBackTableName = 'AdditionalContactsInformation'                  
		set @Prefix = 'ADC.'                  
     if @vcCSLookBackTableName = 'DivisionMaster'                  
		set @Prefix = 'DM.'
	 if @vcCSLookBackTableName = 'CompanyInfo'                  
		set @Prefix = 'CMP.' 
	 if @vcCSLookBackTableName = 'ProjectsMaster'                  
		set @Prefix = 'pro.'   
 
	IF @vcCSAssociatedControlType='DateField'
	BEGIN
			set @StrSql=@StrSql+' left join #tempColorScheme tCS on CONVERT(VARCHAR(10),' + @Prefix + @vcCSOrigDbCOlumnName + ',111) >= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue as int),GETUTCDATE()),111)
			 and CONVERT(VARCHAR(10),' + @Prefix + @vcCSOrigDbCOlumnName + ',111) <= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue1 as int),GETUTCDATE()),111)'
	END
	ELSE IF @vcCSAssociatedControlType='SelectBox' OR @vcCSAssociatedControlType='ListBox' OR @vcCSAssociatedControlType='CheckBox'
	BEGIN
		set @StrSql=@StrSql+' left join #tempColorScheme tCS on tCS.vcFieldValue=' + @Prefix + @vcCSOrigDbCOlumnName
	END
END


 set @strSql=@strSql+ CONCAT(' where tintProStatus=0 and Div.numDomainID= ',@numDomainID)
                                
                                             
 
set @strSql=@strSql+ 'and (isnull(pro.numProjectStatus,0) = '+ convert(varchar(10),@numProjectStatus) + ' OR (' + convert(varchar(10),@numProjectStatus) +'=0 and isnull(pro.numProjectStatus,0) <> 27492))'
                                       
if @numDivisionID <>0 set @strSql=@strSql + ' And div.numDivisionID =' + convert(varchar(15),@numDivisionID)                                               
if @SortChar<>'0' set @strSql=@strSql + ' And Pro.vcProjectName like '''+@SortChar+'%'''                                               
if @tintUserRightType=1 set @strSql=@strSql + ' AND (Pro.numRecOwner = '+convert(varchar(15),@numUserCntID)+' or Pro.numAssignedTo = '+convert(varchar(15),@numUserCntID) +' or  Pro.numProId in (select distinct(numProID) from ProjectsStageDetails         
where numAssignTo ='+ convert(varchar(15),@numUserCntID)+')' + case when @bitPartner=1 then ' or ( ProCont.bitPartner=1 and ProCont.numContactId='+convert(varchar(15),@numUserCntID)+ ')' else '' end  + ' or ' + @strShareRedordWith +')'                                                
else if @tintUserRightType=2 set @strSql=@strSql + ' and  (Div.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+convert(varchar(15),@numUserCntID)+' ) or Pro.numAssignedTo= '+ convert(varchar(15),@numUserCntID
)  +' or  Pro.numProId in (select distinct(numProID) from ProjectsStageDetails                     
where numAssignTo ='+ convert(varchar(15),@numUserCntID)+')' + ' or ' + @strShareRedordWith +')'                                             
                  
if @tintSortOrder=1  set @strSql=@strSql + ' AND (Pro.numRecOwner= '+ convert(varchar(15),@numUserCntID)  +' or Pro.numAssignedTo= '+ convert(varchar(15),@numUserCntID)  +' or  Pro.numProId in (select distinct(numProID) from ProjectsStageDetails          
where numAssignTo ='+ convert(varchar(15),@numUserCntID)+')' + ' or ' + @strShareRedordWith +')'                   
else if @tintSortOrder=2  set @strSql=@strSql  + ' AND (Pro.numRecOwner in (select A.numContactID from AdditionalContactsInformation A                                                         
where A.numManagerID='+ convert(varchar(15),@numUserCntID)+') or Pro.numAssignedTo in (select A.numContactID from AdditionalContactsInformation A                                                         
where A.numManagerID='+ convert(varchar(15),@numUserCntID)+') or  Pro.numProId in (select distinct(numProID) from ProjectsStageDetails                     
where numAssignTo in (select A.numContactID from AdditionalContactsInformation A                                                         
where A.numManagerID='+ convert(varchar(15),@numUserCntID)+'))' + ' or ' + @strShareRedordWith +')'                                           
else if @tintSortOrder=4  set @strSql=@strSql + ' AND Pro.bintCreatedDate > '''+convert(varchar(20),dateadd(day,-8,getutcdate()))+''''                                              
else if @tintSortOrder=6  set @strSql=@strSql + ' and Pro.numModifiedBy= '+ convert(varchar(15),@numUserCntID)        
                                          
                                                
else if @tintSortOrder=4  set @strSql=@strSql + ' and Pro.numRecOwner= '+ convert(varchar(15),@numUserCntID)                                            
else if @tintSortOrder=5  set @strSql=@strSql + ' and Pro.numRecOwner= '+ convert(varchar(15),@numUserCntID)                                             
 
IF @vcRegularSearchCriteria<>'' set @strSql=@strSql+' and ' + @vcRegularSearchCriteria 
IF @vcCustomSearchCriteria<>'' set @strSql=@strSql+' AND ' + @vcCustomSearchCriteria


IF LEN(@SearchQuery) > 0
	BEGIN
		SET @StrSql = @StrSql + ' AND (' + @SearchQuery + ') '
	END	
	

	IF ISNULL(@tintDashboardReminderType,0) = 5
	BEGIN
		SET @strSql = CONCAT(@strSql,' AND Pro.numProId IN (','SELECT DISTINCT
																	ProjectsMaster.numProId 
																FROM 
																	StagePercentageDetails SPD 
																INNER JOIN 
																	ProjectsMaster 
																ON 
																	SPD.numProjectID=ProjectsMaster.numProId 
																WHERE 
																	SPD.numDomainId=',@numDOmainID ,'
																	AND ProjectsMaster.numDomainId=',@numDOmainID,'
																	AND tinProgressPercentage <> 100',') ')

	END


DECLARE  @firstRec  AS INTEGER=0
DECLARE  @lastRec  AS INTEGER=0
SET @firstRec = (@CurrentPage - 1) * @PageSize
	SET @lastRec = (@CurrentPage * @PageSize + 1)
 --set @StrSql=@StrSql+' WHERE RowNumber > '+convert(varchar(10),@firstRec)+ ' and RowNumber <'+ convert(varchar(10),@lastRec) + ' order by RowNumber'

	-- EXECUTE FINAL QUERY
	DECLARE @strFinal AS NVARCHAR(MAX)
	SET @strFinal = CONCAT('SELECT ROW_NUMBER() OVER(ORDER BY ',@columnName,' ',@columnSortOrder,') ID, ',@strColumns,' INTO #tempTable',@StrSql,'; SELECT @TotalRecords=COUNT(*) FROM #tempTable; SELECT * FROM #tempTable WHERE ID > ',@firstRec,' and ID <',@lastRec,'; DROP TABLE #tempTable;')
	
	PRINT @strFinal
	EXEC sp_executesql @strFinal, N'@TotalRecords INT OUT', @TotRecs OUT 

	SELECT * FROM #tempForm

	DROP TABLE #tempForm

	DROP TABLE #tempColorScheme
	END TRY
  BEGIN CATCH
  IF @@TRANCOUNT > 0
    ROLLBACK TRANSACTION;

    DECLARE @ErrorNumber INT = ERROR_NUMBER();
    DECLARE @ErrorLine INT = ERROR_LINE();
    DECLARE @ErrorMessage NVARCHAR(4000) = ERROR_MESSAGE();
    DECLARE @ErrorSeverity INT = ERROR_SEVERITY();
    DECLARE @ErrorState INT = ERROR_STATE();

    PRINT 'Actual error number: ' + CAST(@ErrorNumber AS VARCHAR(10));
    PRINT 'Actual line number: ' + CAST(@ErrorLine AS VARCHAR(10));

    RAISERROR(@ErrorMessage, @ErrorSeverity, @ErrorState);

  END CATCH
GO 
/****** Object:  StoredProcedure [dbo].[USP_PromoteLead]    Script Date: 07/26/2008 16:20:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--modified by anoop jayaraj    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_promotelead')
DROP PROCEDURE usp_promotelead
GO
CREATE PROCEDURE [dbo].[USP_PromoteLead]          
 @numDivisionID numeric,          
 @numContactID numeric,          
 @numUserCntID numeric        
         
--          
AS          
         
BEGIN          
	DECLARE @numDomainID NUMERIC(18,0)
	DECLARE @tintCurrentCRMType TINYINT
	
	SELECT @numDomainID=numDomainID,@tintCurrentCRMType=tintCRMType FROM DivisionMaster WHERE numDivisionID=@numDivisionID     
	
	INSERT INTO DivisionMasterPromotionHistory
	(
		numDomainID
		,numDivisionID
		,tintPreviousCRMType
		,tintNewCRMType
		,dtPromotedBy
		,dtPromotionDate
	)
	VALUES
	(
		@numDomainID
		,@numDivisionID
		,@tintCurrentCRMType
		,1
		,@numUserCntID
		,GETUTCDATE()
	)


   UPDATE DivisionMaster           
   SET tintCRMType=1,          
   numModifiedBy=@numUserCntID,          
   bintModifiedDate=getutcdate(),          
   bintLeadProm = getutcdate(),          
   bintLeadPromBy = @numUserCntID          
   WHERE numDivisionID=@numDivisionID          
    declare @numCompanyID as numeric(9)        
   select @numCompanyID=numCompanyID from DivisionMaster where numDivisionID=@numDivisionID        
   UPDATE AdditionalContactsInformation       
   set numModifiedBy=@numUserCntID,          
   bintModifiedDate=getutcdate()          
   WHERE numContactId=@numContactID       
   
  
   	DECLARE @numGroupID NUMERIC       
	SELECT TOP 1 @numGroupID=numGroupID FROM dbo.AuthenticationGroupMaster WHERE numDomainID=@numDomainID AND tintGroupType=2
	IF @numGroupID IS NULL SET @numGroupID = 0    
	  
   insert into ExtarnetAccounts(numCompanyID,numDivisionID,numGroupID,numDomainID)         
   values(@numCompanyID,@numDivisionID,@numGroupID,@numDomainID)        
      
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ReportListMaster_ActionItemPreBuildReport')
DROP PROCEDURE USP_ReportListMaster_ActionItemPreBuildReport
GO
CREATE PROCEDURE [dbo].[USP_ReportListMaster_ActionItemPreBuildReport]
	@numDomainID NUMERIC(18,0)
	,@numUserCntID NUMERIC(18,0)
	,@ClientTimeZoneOffset INT
AS
BEGIN 
	DECLARE @TEMP TABLE
	(
		ID NUMERIC(18,0)
		,vcOrganizationName VARCHAR(300)
		,vcComments NVARCHAR(MAX)
		,vcType VARCHAR(50)
		,dtDate DATETIME
		,tintType TINYINT
	)

	INSERT INTO @TEMP
	(
		ID
		,vcOrganizationName
		,vcComments
		,vcType
		,dtDate
		,tintType
	)
	SELECT
		Communication.numCommId
		,CompanyInfo.vcCompanyName
		,ISNULL(Communication.textDetails,'')
		,ISNULL(LD.vcData,'')
		,DATEADD(MINUTE,-@ClientTimeZoneOffset,dtStartTime)
		,1
	FROM
		Communication                               
	INNER JOIN 
		AdditionalContactsInformation
	ON 
		Communication.numContactId = AdditionalContactsInformation.numContactId                                  
	INNER JOIN 
		DivisionMaster                                                                         
	ON 
		AdditionalContactsInformation.numDivisionId = DivisionMaster.numDivisionID                                   
	INNER JOIN 
		CompanyInfo                                                                         
	ON 
		DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
	LEFT JOIN
		ListDetails LD
	ON
		LD.numListID = 73
		AND (LD.numDomainID=@numDomainID OR LD.constFlag = 1)
		AND LD.numListItemID = Communication.bitTask
	WHERE
		Communication.numDomainID = @numDomainID
		AND AdditionalContactsInformation.numDomainID=@numDomainID
		AND DivisionMaster.numDomainID=@numDomainID
		AND CompanyInfo.numDomainID=@numDomainID
		AND Communication.bitclosedflag=0                                                
		AND Communication.bitTask <> 973
		AND CAST(DATEADD(MINUTE,-@ClientTimeZoneOffset,dtEndTime) AS DATE) = CAST(DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()) AS DATE)
		AND 1= (CASE WHEN ISNULL(@numUserCntID,0) > 0 THEN (CASE WHEN (Communication.numAssign  = @numUserCntID  OR Communication.numCreatedBy = @numUserCntID) THEN 1 ELSE 0 END) ELSE 1 END)


	INSERT INTO @TEMP
	(
		ID
		,vcOrganizationName
		,vcComments
		,vcType
		,dtDate
		,tintType
	)
	SELECT
		Activity.Activityid
		,Comp.vcCompanyName
		,ISNULL(Activity.ActivityDescription,'')
		,'Calendar'
		,DATEADD(MINUTE,-@ClientTimeZoneOffset,startdatetimeutc)
		,2
	FROM 
		Activity 
	INNER JOIN             
		ActivityResource 
	ON 
		Activity.ActivityID=ActivityResource.ActivityID
	INNER JOIN 
		[Resource] Res 
	ON 
		ActivityResource.ResourceID=Res.ResourceID                                   
	INNER JOIN 
		AdditionalContactsInformation ACI 
	ON 
		ACI.numContactId = Res.numUserCntId                
	INNER JOIN 
		DivisionMaster Div                                                          
	ON 
		ACI.numDivisionId = Div.numDivisionID                                   
	INNER JOIN 
		CompanyInfo Comp                             
	ON 
		Div.numCompanyID = Comp.numCompanyId
	WHERE
		Res.numDomainId =@numDomainID
		AND 1 = (CASE WHEN ISNULL(@numUserCntID,0) > 0 THEN (CASE WHEN Res.numUserCntId = @numUserCntID THEN 1 ELSE 0 END) ELSE 1 END)
		AND ACI.numDomainID=@numDomainID
		AND Div.numDomainID=@numDomainID
		AND Comp.numDomainID=@numDomainID
		AND CAST(DATEADD(MINUTE,-@ClientTimeZoneOffset,startdatetimeutc) AS DATE) = CAST(DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()) AS DATE)
		--AND DATEADD(MINUTE,-@ClientTimeZoneOffset,startdatetimeutc) >= DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()) AND DATEADD(MINUTE,-@ClientTimeZoneOffset,startdatetimeutc) < CAST(CONVERT(CHAR(8), GETUTCDATE(), 112) + ' 00:00:00.00' AS DATETIME)
		AND [status] <> -1                                                     
		AND Activity.activityid NOT IN (SELECT DISTINCT(numActivityId) FROM Communication WHERE numDomainID=@numDomainID AND 1 = (CASE WHEN ISNULL(@numUserCntID,0) > 0 THEN (CASE WHEN (numAssign =@numUserCntID OR numCreatedBy=@numUserCntID) THEN 1 ELSE 0 END) ELSE 1 END))

	--INSERT INTO @TEMP
	--(
	--	ID
	--	,vcOrganizationName
	--	,vcComments
	--	,vcType
	--	,dtDate
	--	,tintType
	--)
	--SELECT
	--	(CASE BAD.btDocType WHEN 2 THEN BAD.numOppBizDocsId WHEN 3 THEN BAD.numOppBizDocsId ELSE BDA.numBizActionID END)
	--	,CI.vcCompanyName
	--	,ISNULL(BAD.vcComment,'')
	--	,(CASE BAD.btDocType WHEN 2  THEN  'Document Approval Request' ELSE 'BizDoc Approval Request' END)
	--	,DATEADD(MINUTE,-@ClientTimeZoneOffset,dtCreatedDate)
	--	,2
	--FROM
	--	BizDocAction BDA 
	--INNER JOIN
	--	AdditionalContactsInformation ACI
	--ON
	--	BDA.numContactId = ACI.numContactId
	--INNER JOIN
	--	DivisionMaster DM
	--ON
	--	ACI.numDivisionId = DM.numDivisionID
	--INNER JOIN
	--	CompanyInfo CI
	--ON
	--	DM.numCompanyID = CI.numCompanyId
	--LEFT JOIN 
	--	dbo.BizActionDetails BAD
	--ON 
	--	BDA.numBizActionId = BAD.numBizActionId
	--WHERE
	--	BDA.numDomainId =@numDomainID
	--	AND 1 = (CASE WHEN ISNULL(@numUserCntID,0) > 0 THEN (CASE WHEN BDA.numContactId = @numUserCntID THEN 1 ELSE 0 END) ELSE 1 END)
	--	AND ACI.numDomainID=@numDomainID
	--	AND DM.numDomainID=@numDomainID
	--	AND CI.numDomainID=@numDomainID
	--	AND BDA.numStatus=0 
	--	AND CAST(DATEADD(MINUTE,-@ClientTimeZoneOffset,dtCreatedDate) AS DATE) = CAST(DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()) AS DATE)
	
	SELECT *,dbo.FormatedDateTimeFromDate(dtDate,@numDOmainID) AS dtFormatted FROM @TEMP ORDER BY dtDate ASC
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ReportListMaster_APPreBuildReport')
DROP PROCEDURE USP_ReportListMaster_APPreBuildReport
GO
CREATE PROCEDURE [dbo].[USP_ReportListMaster_APPreBuildReport]
	@numDomainID NUMERIC(18,0)
AS
BEGIN 
	DECLARE @TotalAP MONEY = (SELECT numOpeningBal FROM Chart_Of_Accounts WHERE numDomainId=@numDomainID AND numAccountId IN (SELECT numAccountID FROM AccountingCharges WHERE numDomainID=@numDomainID AND numChargeTypeId=8))
	DECLARE @APWithin30Days MONEY = 0
	DECLARE @APWithin31to60Days MONEY = 0
	DECLARE @APWithin61to90Days MONEY = 0
	DECLARE @APPastDue MONEY = 0
	DECLARE @dtFromDate DATETIME = '1990-01-01 00:00:00.000'
	DECLARE @dtToDate DATETIME = GETUTCDATE()

	SET @dtFromDate = DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE()), @dtFromDate);
	SET @dtToDate =  DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE()), @dtToDate);

	DECLARE @AuthoritativePurchaseBizDocId AS INTEGER
    SELECT 
		@AuthoritativePurchaseBizDocId = ISNULL(numAuthoritativePurchase,0)
    FROM 
		AuthoritativeBizDocs
    WHERE 
		numDomainId = @numDomainId
  
	DECLARE @TEMPAPRecord TABLE
	(
		DealAmount MONEY,
		dtDueDate DATETIME,
		AmountPaid MONEY,
		monUnAppliedAmount MONEY
	)

	BEGIN TRY
		INSERT INTO @TEMPAPRecord
		(
			DealAmount
			,dtDueDate
			,AmountPaid
		)
		SELECT  
			ISNULL(OB.monDealAmount * ISNULL(Opp.fltExchangeRate, 1), 0) - ISNULL(OB.monAmountPaid * ISNULL(Opp.fltExchangeRate, 1), 0) DealAmount
			,DATEADD(DAY,CASE WHEN Opp.bitBillingTerms = 1 THEN convert(int, ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(Opp.intBillingDays,0)), 0)) ELSE 0 END, dtFromDate) AS dtDueDate
			,ISNULL(OB.monAmountPaid * ISNULL(Opp.fltExchangeRate, 1), 0) AmountPaid
        FROM 
			OpportunityMaster Opp
        INNER JOIN 
			OpportunityBizDocs OB 
		ON 
			OB.numOppId = Opp.numOppID
        LEFT OUTER JOIN 
			[Currency] C 
		ON 
			Opp.[numCurrencyID] = C.[numCurrencyID]
        WHERE 
			tintOpptype = 2
            AND tintoppStatus = 1
            AND opp.numDomainID = @numDomainId
            AND OB.bitAuthoritativeBizDocs = 1
            AND ISNULL(OB.tintDeferred, 0) <> 1
            AND numBizDocId = @AuthoritativePurchaseBizDocId
			AND OB.[dtCreatedDate] BETWEEN @dtFromDate AND @dtToDate
        GROUP BY 
			OB.numOppId,
            OB.numOppBizDocsId,
            Opp.fltExchangeRate,
            OB.numBizDocId,
            OB.dtCreatedDate,
            Opp.bitBillingTerms,
            Opp.intBillingDays,
            OB.monDealAmount,
            Opp.numCurrencyID,
            OB.[dtFromDate],
			OB.monAmountPaid
        HAVING 
			(ISNULL(OB.monDealAmount * ISNULL(Opp.fltExchangeRate,1), 0) - ISNULL(OB.monAmountPaid * ISNULL(Opp.fltExchangeRate, 1), 0) ) > 0
        UNION ALL 
		--Add Bill Amount
        SELECT
            ISNULL(SUM(BH.monAmountDue), 0) - ISNULL(SUM(BH.monAmtPaid), 0) DealAmount,
            BH.dtDueDate AS dtDueDate,
            ISNULL(SUM(BH.monAmtPaid), 0) AS AmountPaid
        FROM 
			BillHeader BH
        WHERE 
			BH.numDomainID = @numDomainId
            AND ISNULL(BH.monAmountDue, 0) - ISNULL(BH.monAmtPaid, 0) > 0
			AND BH.[dtBillDate] BETWEEN @dtFromDate AND @dtToDate
        GROUP BY 
			BH.numDivisionId
			,BH.dtDueDate
        HAVING 
			ISNULL(SUM(BH.monAmountDue), 0) - ISNULL(SUM(BH.monAmtPaid), 0) > 0
        UNION ALL
		--Add Write Check entry (As Amount Paid)
        SELECT  
            0 DealAmount,
            CH.dtCheckDate AS dtDueDate,
            ISNULL(SUM(CD.monAmount), 0) AS AmountPaid
        FROM 
			CheckHeader CH
		INNER JOIN 
			dbo.CheckDetails CD 
		ON 
			CH.numCheckHeaderID=CD.numCheckHeaderID
        INNER JOIN 
			dbo.Chart_Of_Accounts COA 
		ON 
			COA.numAccountId = CD.numChartAcntId
        WHERE 
			CH.numDomainID = @numDomainId 
			AND CH.tintReferenceType=1
			AND COA.vcAccountCode LIKE '01020102%'
			AND CH.[dtCheckDate] BETWEEN @dtFromDate AND @dtToDate
        GROUP BY 
			CD.numCustomerId,
            CH.dtCheckDate
		--Show Impact of AP journal entries in report as well 
        UNION ALL
        SELECT 
			CASE WHEN GJD.numDebitAmt > 0 THEN -1 * GJD.numDebitAmt ELSE GJD.numCreditAmt END,
            GJH.datEntry_Date,
            0 AS AmountPaid
        FROM 
			dbo.General_Journal_Header GJH
        INNER JOIN 
			dbo.General_Journal_Details GJD 
		ON 
			GJH.numJournal_Id = GJD.numJournalId
            AND GJH.numDomainId = GJD.numDomainId
		INNER JOIN 
			dbo.Chart_Of_Accounts COA ON COA.numAccountId = GJD.numChartAcntId
        WHERE 
			GJH.numDomainId = @numDomainId 
			AND COA.vcAccountCode LIKE '01020102%'
            AND ISNULL(numOppId, 0) = 0
            AND ISNULL(numOppBizDocsId, 0) = 0
			AND ISNULL(GJD.numCustomerId,0) > 0
            AND ISNULL(GJH.numBillID,0)=0 AND ISNULL(GJH.numBillPaymentID,0)=0 AND ISNULL(GJH.numCheckHeaderID,0)=0 AND ISNULL(GJH.numReturnID,0)=0
            AND ISNULL(GJH.numPayrollDetailID,0)=0
			AND [GJH].[datEntry_Date] BETWEEN @dtFromDate AND @dtToDate
        UNION ALL
		--Add Commission Amount
        SELECT 
			ISNULL(SUM(BDC.numComissionAmount - ISNULL(PT.monCommissionAmt,0)),0)  DealAmount,
            OBD.dtCreatedDate AS dtDueDate,
            0 AS AmountPaid
        FROM 
			BizDocComission BDC
        INNER JOIN 
			OpportunityBizDocs OBD 
		ON 
			BDC.numOppBizDocId = OBD.numOppBizDocsId
        INNER JOIN 
			OpportunityMaster Opp 
		ON 
			OPP.numOppId = OBD.numOppId
        INNER JOIN 
			Domain D 
		ON 
			D.numDomainID = BDC.numDomainID
        LEFT JOIN 
			dbo.PayrollTracking PT 
		ON 
			BDC.numComissionID=PT.numComissionID
        WHERE 
			OPP.numDomainID = @numDomainId
            AND BDC.numDomainID = @numDomainId
			AND OBD.[dtCreatedDate] BETWEEN @dtFromDate AND @dtToDate
            AND OBD.bitAuthoritativeBizDocs = 1
        GROUP BY 
			D.numDivisionId,
            OBD.numOppId,
            BDC.numOppBizDocId,
            OPP.numCurrencyID,
            BDC.numComissionAmount,
            OBD.dtCreatedDate
        HAVING 
			ISNULL(BDC.numComissionAmount, 0) > 0
		UNION ALL --Standalone Refund against Account Receivable
		SELECT 
			CASE WHEN GJD.numDebitAmt > 0 THEN GJD.numDebitAmt * -1 ELSE GJD.numCreditAmt END DealAmount,			
			GJH.datEntry_Date
			,0 AS AmountPaid
		FROM 
			dbo.ReturnHeader RH 
		JOIN 
			dbo.General_Journal_Header GJH 
		ON 
			RH.numReturnHeaderID = GJH.numReturnID
		JOIN 
			General_Journal_Details GJD 
		ON 
			GJH.numJournal_Id = GJD.numJournalId 
		INNER JOIN 
			dbo.Chart_Of_Accounts COA 
		ON 
			COA.numAccountId = GJD.numChartAcntId
		WHERE 
			RH.tintReturnType=4 
			AND RH.numDomainId=@numDomainId 
			AND COA.vcAccountCode LIKE '01020102%'
			AND (monBizDocAmount > 0) 
			AND ISNULL(RH.numBillPaymentIDRef,0)>0
			AND RH.[dtCreatedDate]  BETWEEN @dtFromDate AND @dtToDate
			
		
		INSERT INTO @TEMPAPRecord
        (
			[DealAmount],
			[dtDueDate],
			AmountPaid,
			monUnAppliedAmount
        )
		SELECT 
			0,
			NULL,
			0,
			SUM(ISNULL(monPaymentAmount,0) - ISNULL(monAppliedAmount,0))       
		FROM 
			BillPaymentHeader BPH
		WHERE 
			BPH.numDomainId=@numDomainId   
			AND (ISNULL(monPaymentAmount,0) - ISNULL(monAppliedAmount,0)) > 0
			AND [BPH].[dtPaymentDate]  BETWEEN @dtFromDate AND @dtToDate
		GROUP BY 
			BPH.numDivisionId

		SET @APWithin30Days = (SELECT SUM(DealAmount) FROM @TEMPAPRecord WHERE DATEDIFF(DAY, dbo.GetUTCDateWithoutTime(),[dtDueDate]) BETWEEN 0 AND 30)
		SET @APWithin31to60Days = (SELECT SUM(DealAmount) FROM @TEMPAPRecord WHERE DATEDIFF(DAY, dbo.GetUTCDateWithoutTime(),[dtDueDate]) BETWEEN 31 AND 60)
		SET @APWithin61to90Days = (SELECT SUM(DealAmount) FROM @TEMPAPRecord WHERE DATEDIFF(DAY, dbo.GetUTCDateWithoutTime(),[dtDueDate]) BETWEEN 61 AND 90)
		SET @APPastDue = (SELECT SUM(DealAmount) FROM @TEMPAPRecord WHERE dbo.GetUTCDateWithoutTime() > [dtDueDate])
	END TRY
	BEGIN CATCH
		-- DO NOT RAISE ERROR
	END CATCH

	SELECT 
		ISNULL(@TotalAP,0) AS TotalAP
		,ISNULL(@APWithin30Days,0) AS APWithin30Days
		,ISNULL(@APWithin31to60Days,0) AS APWithin31to60Days
		,ISNULL(@APWithin61to90Days,0) AS APWithin61to90Days
		,ISNULL(@APPastDue,0) AS APPastDue
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ReportListMaster_ARPreBuildReport')
DROP PROCEDURE USP_ReportListMaster_ARPreBuildReport
GO
CREATE PROCEDURE [dbo].[USP_ReportListMaster_ARPreBuildReport]
	@numDomainID NUMERIC(18,0)
AS
BEGIN 
	DECLARE @TotalAR MONEY = (SELECT numOpeningBal FROM Chart_Of_Accounts WHERE numDomainId=@numDomainID AND numAccountId IN (SELECT numAccountID FROM AccountingCharges WHERE numDomainID=@numDomainID AND numChargeTypeId=4))
	DECLARE @ARWithin30Days MONEY = 0
	DECLARE @ARWithin31to60Days MONEY = 0
	DECLARE @ARWithin61to90Days MONEY = 0
	DECLARE @ARPastDue MONEY = 0
	DECLARE @dtFromDate DATETIME = '1990-01-01 00:00:00.000'
	DECLARE @dtToDate DATETIME = GETUTCDATE()

	SET @dtFromDate = DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE()), @dtFromDate);
	SET @dtToDate =  DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE()), @dtToDate);

	DECLARE @AuthoritativeSalesBizDocId INTEGER
    SELECT 
		@AuthoritativeSalesBizDocId = ISNULL(numAuthoritativeSales,0)
    FROM 
		AuthoritativeBizDocs
    WHERE 
		numDomainId = @numDomainId;
  
	DECLARE @TEMPARRecord TABLE
	(
		numOppId NUMERIC(18,0),
		numOppBizDocsId NUMERIC(18,0),
		DealAmount MONEY,
		numBizDocId NUMERIC(18,0),
		numCurrencyID NUMERIC(18,0),
		dtDueDate DATETIME,
		AmountPaid MONEY,
		monUnAppliedAmount MONEY
	)

	BEGIN TRY
		INSERT INTO @TEMPARRecord
		(
			numOppId
			,numOppBizDocsId
			,DealAmount
			,numBizDocId
			,numCurrencyID
			,dtDueDate
			,AmountPaid
		)
		SELECT  
			OB.numOppId,
			OB.numOppBizDocsId,
			ISNULL(OB.monDealAmount * ISNULL(Opp.fltExchangeRate, 1), 0) - ISNULL(OB.monAmountPaid * ISNULL(Opp.fltExchangeRate, 1),0) DealAmount,
			OB.numBizDocId,
			ISNULL(Opp.numCurrencyID, 0) numCurrencyID,
			DATEADD(DAY,CASE WHEN Opp.bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(Opp.intBillingDays,0)), 0)) ELSE 0 END, dtFromDate) AS dtDueDate,
			ISNULL(OB.monAmountPaid * ISNULL(Opp.fltExchangeRate, 1),0) as AmountPaid
		FROM    
			OpportunityMaster Opp
		INNER JOIN 
			[dbo].[DivisionMaster] AS DIV 
		ON 
			DIV.[numDivisionID] = Opp.[numDivisionId]
		INNER JOIN 
			OpportunityBizDocs OB 
		ON 
			OB.numOppId = Opp.numOppID
		LEFT OUTER JOIN 
			[Currency] C 
		ON 
			Opp.[numCurrencyID] = C.[numCurrencyID]
		WHERE 
			tintOpptype = 1
			AND tintoppStatus = 1        
			AND opp.numDomainID = @numDomainId
			AND numBizDocId = @AuthoritativeSalesBizDocId
			AND OB.bitAuthoritativeBizDocs=1
			AND ISNULL(OB.tintDeferred,0) <> 1
			AND CONVERT(DATE,OB.[dtCreatedDate]) BETWEEN @dtFromDate AND @dtToDate
		GROUP BY 
			OB.numOppId,
			OB.numOppBizDocsId,
			Opp.fltExchangeRate,
			OB.numBizDocId,
			OB.dtCreatedDate,
			Opp.bitBillingTerms,
			Opp.intBillingDays,
			OB.monDealAmount,
			Opp.numCurrencyID,
			OB.dtFromDate,
			OB.monAmountPaid
		HAVING
			(ISNULL(OB.monDealAmount * ISNULL(Opp.fltExchangeRate, 1), 0) - ISNULL(OB.monAmountPaid * ISNULL(Opp.fltExchangeRate, 1),0) ) != 0
		--Show Impact of AR journal entries in report as well 
		UNION 
		SELECT 
			0,
			0,
			CASE WHEN GJD.numDebitAmt > 0 THEN GJD.numDebitAmt ELSE -1*GJD.numCreditAmt END,
			0,
			GJD.[numCurrencyID],
			GJH.datEntry_Date,
			0 AS AmountPaid
		FROM  
			dbo.General_Journal_Header GJH
		INNER JOIN 
			dbo.General_Journal_Details GJD 
		ON 
			GJH.numJournal_Id = GJD.numJournalId
			AND GJH.numDomainId = GJD.numDomainId
		INNER JOIN
			dbo.Chart_Of_Accounts COA 
		ON 
			COA.numAccountId = GJD.numChartAcntId
		INNER JOIN 
			[dbo].[DivisionMaster] AS DIV 
		ON 
			DIV.[numDivisionID] = [GJD].[numCustomerId]
		WHERE 
			GJH.numDomainId = @numDomainId
			AND COA.vcAccountCode LIKE '0101010501'
			AND ISNULL(numOppId,0)=0 AND ISNULL(numOppBizDocsId,0)=0
			AND ISNULL(GJH.numDepositID,0)=0 AND ISNULL(GJH.numReturnID,0)=0
			AND CONVERT(DATE,GJH.[datEntry_Date]) BETWEEN @dtFromDate AND @dtToDate
		UNION ALL
		SELECT
			0,
			0,
			(ISNULL(monDepositAmount,0) - ISNULL(monAppliedAmount,0)) * -1,
			0,
			DM.numCurrencyID,
			DM.dtDepositDate,
			0 AS AmountPaid
		FROM 
			DepositMaster DM 
		INNER JOIN 
			[dbo].[DivisionMaster] AS DIV 
		ON 
			[DM].[numDivisionID] = DIV.[numDivisionID]
		WHERE 
			DM.numDomainId=@numDomainId 
			AND tintDepositePage IN(2,3)  
			AND ((ISNULL(monDepositAmount,0) - ISNULL(monAppliedAmount,0)) > 0)
			AND CONVERT(DATE,DM.[dtDepositDate]) BETWEEN @dtFromDate AND @dtToDate
		UNION ALL --Standalone Refund against Account Receivable
		SELECT 
			0,
			0,
			CASE WHEN GJD.numDebitAmt > 0 THEN GJD.numDebitAmt ELSE -1*GJD.numCreditAmt END,
			0,
			GJD.[numCurrencyID],
			GJH.datEntry_Date,
			0 AS AmountPaid
		FROM 
			dbo.ReturnHeader RH 
		INNER JOIN 
			dbo.General_Journal_Header GJH 
		ON 
			RH.numReturnHeaderID = GJH.numReturnID
		JOIN 
			[dbo].[DivisionMaster] AS DIV 
		ON 
			DIV.[numDivisionID] = RH.[numDivisionId]
		JOIN 
			General_Journal_Details GJD 
		ON 
			GJH.numJournal_Id = GJD.numJournalId 
		JOIN 
			dbo.Chart_Of_Accounts COA 
		ON 
			COA.numAccountId = GJD.numChartAcntId 
			AND COA.vcAccountCode LIKE '0101010501'
		WHERE 
			RH.tintReturnType=4 
			AND RH.numDomainId=@numDomainId  
			AND ISNULL(RH.numParentID,0) = 0
			AND ISNULL(RH.IsUnappliedPayment,0) = 0
			AND (monBizDocAmount > 0) AND ISNULL(RH.numBillPaymentIDRef,0)=0 --AND ISNULL(RH.numDepositIDRef,0) > 0
			AND CONVERT(DATE,GJH.[datEntry_Date])  BETWEEN @dtFromDate AND @dtToDate
		

		INSERT INTO @TEMPARRecord
		(
			numOppId
			,numOppBizDocsId
			,DealAmount
			,numBizDocId
			,numCurrencyID
			,dtDueDate
			,AmountPaid
			,monUnAppliedAmount
		)
		SELECT 
			0
			,0
			,0
			,0
			,0
			,NULL
			,0
			,SUM(ISNULL(monDepositAmount,0) - ISNULL(monAppliedAmount,0))       
		FROM 
			DepositMaster DM 
		INNER JOIN 
			[dbo].[DivisionMaster] AS DIV 
		ON 
			DIV.[numDivisionID] = DM.[numDivisionID]
 		WHERE 
			DM.numDomainId=@numDomainId 
			AND tintDepositePage IN (2,3)  
			AND ((ISNULL(monDepositAmount,0) - ISNULL(monAppliedAmount,0)) > 0)
			AND CONVERT(DATE,DM.[dtDepositDate]) BETWEEN @dtFromDate AND @dtToDate
		GROUP BY 
			DM.numDivisionId

	

		SET @ARWithin30Days = (SELECT SUM(DealAmount) FROM @TEMPARRecord WHERE DATEDIFF(DAY,dbo.GetUTCDateWithoutTime(),[dtDueDate]) BETWEEN 0 AND 30)
		SET @ARWithin31to60Days = (SELECT SUM(DealAmount) FROM @TEMPARRecord WHERE DATEDIFF(DAY,dbo.GetUTCDateWithoutTime(),[dtDueDate]) BETWEEN 31 AND 60)
		SET @ARWithin61to90Days = (SELECT SUM(DealAmount) FROM @TEMPARRecord WHERE DATEDIFF(DAY,dbo.GetUTCDateWithoutTime(),[dtDueDate]) BETWEEN 61 AND 90)
		SET @ARPastDue = (SELECT SUM(DealAmount) FROM @TEMPARRecord WHERE dbo.GetUTCDateWithoutTime() > [dtDueDate])
	END TRY
	BEGIN CATCH
		-- DO NOT RAISE ERROR
	END CATCH

	SELECT ISNULL(@TotalAR,0) AS TotalAR, ISNULL(@ARWithin30Days,0) AS ARWithin30Days,ISNULL(@ARWithin31to60Days,0) AS ARWithin31to60Days,ISNULL(@ARWithin61to90Days,0) AS ARWithin61to90Days,ISNULL(@ARPastDue,0) AS ARPastDue
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ReportListMaster_BankPreBuildReport')
DROP PROCEDURE USP_ReportListMaster_BankPreBuildReport
GO
CREATE PROCEDURE [dbo].[USP_ReportListMaster_BankPreBuildReport]
	@numDomainID NUMERIC(18,0)
AS
BEGIN 
	SELECT 
		CAST(SUM(numOpeningBal) AS MONEY) AS MoneyInBank
	FROM 
		Chart_Of_Accounts 
	WHERE 
		numDomainId=@numDomainID 
		AND numParntAcntTypeId IN (SELECT numAccountTypeID FROM AccountTypeDetail WHERE numDomainID=@numDomainID AND vcAccountCode='01010101')
		AND numAccountId NOT IN (SELECT numAccountID FROM AccountingCharges WHERE numDomainID=@numDomainID AND numChargeTypeId=9)
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ReportListMaster_ItemClassificationProfitPreBuildReport')
DROP PROCEDURE USP_ReportListMaster_ItemClassificationProfitPreBuildReport
GO
CREATE PROCEDURE [dbo].[USP_ReportListMaster_ItemClassificationProfitPreBuildReport]
	@numDomainID NUMERIC(18,0)
AS
BEGIN 
	DECLARE @numShippingItemID NUMERIC(18,0)
	DECLARE @numDiscountItemID NUMERIC(18,0)
	DECLARE @ProfitCost INT
	SELECT @numShippingItemID=ISNULL(numShippingServiceItemID,0),@numDiscountItemID=ISNULL(numDiscountServiceItemID,0),@ProfitCost=numCost FROM Domain WHERE numDomainId=@numDomainID


	SELECT 
		vcItemClassification
		,(SUM(Profit)/SUM(monTotAmount)) * 100 BlendedProfit
	FROM
	(
		SELECT
			LD.vcData AS vcItemClassification
			,ISNULL(monTotAmount,0) monTotAmount
			,ISNULL(monTotAmount,0) - (ISNULL(numUnitHour,0) * (CASE @ProfitCost WHEN 3 THEN ISNULL(V.monCost,0) * dbo.fn_UOMConversion(numBaseUnit,I.numItemCode,@numDomainID,numPurchaseUnit) WHEN 2 THEN numCost * dbo.fn_UOMConversion(numBaseUnit,I.numItemCode,@numDomainID,numUOMId) ELSE monAverageCost END)) Profit
		FROM
			OpportunityItems OI
		INNER JOIN
			OpportunityMaster OM
		ON
			OI.numOppId = OM.numOppID
		INNER JOIN
			Item I
		ON
			OI.numItemCode = I.numItemCode
		INNER JOIN
			ListDetails LD
		ON
			I.numItemClassification = LD.numListItemID
			AND LD.numListID=36
			AND LD.numDomainID=@numDomainID
		Left JOIN 
			Vendor V 
		ON 
			V.numVendorID=I.numVendorID 
			AND V.numItemCode=I.numItemCode
		WHERE
			OM.numDomainId=@numDomainID
			AND ISNULL(OI.monTotAmount,0) <> 0
			AND ISNULL(OI.numUnitHour,0) <> 0
			AND ISNULL(OM.tintOppType,0)=1
			AND ISNULL(OM.tintOppStatus,0)=1
			AND OM.bintCreatedDate >= DATEADD(MONTH, -12, GETUTCDATE())
			AND I.numItemCode NOT IN (@numShippingItemID,@numDiscountItemID)
	) TEMP
	GROUP BY
		vcItemClassification
	HAVING 
		(SUM(Profit)/SUM(monTotAmount)) * 100 > 0
	ORDER BY
		(SUM(Profit)/SUM(monTotAmount)) * 100 DESC
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ReportListMaster_PrebuildScoreCard')
DROP PROCEDURE USP_ReportListMaster_PrebuildScoreCard
GO
CREATE PROCEDURE [dbo].[USP_ReportListMaster_PrebuildScoreCard]
	@numDomainID NUMERIC(18,0)
AS
BEGIN 
	DECLARE @FinancialYearStartDate AS DATETIME
	DECLARE @dtCYFromDate AS DATETIME                                       
	DECLARE @dtCYToDate AS DATETIME
	DECLARE @dtMonthFromDate AS DATETIME                                       
	DECLARE @dtMonthToDate AS DATETIME

	SELECT @FinancialYearStartDate = dbo.GetFiscalStartDate(YEAR(GETDATE()),@numDomainID)
	SELECT @dtCYFromDate = @FinancialYearStartDate,@dtCYToDate = dateadd(ms, -3, dateadd(day, datediff(day, 0, getdate())+1, 0));

	SELECT 
		@dtMonthFromDate = DATEADD(m, DATEDIFF(m, 0, GETUTCDATE()), 0)
		,@dtMonthToDate = GETUTCDATE();
	
	

	DECLARE @Table Table
	(
		ID INT
		,vcReport VARCHAR(100)
		,vcObjectsOfMeasure VARCHAR(100)
		,vcPeriod VARCHAR(100)
		,numChange NUMERIC(18,2)
	)

	INSERT INTO @Table (ID) VALUES (1),(2),(3),(4),(5),(6),(7)

	UPDATE @Table SET numChange=dbo.GetPercentageChangeInLeadToAccountConversion(@numDomainID,@dtCYFromDate,@dtCYToDate,1) WHERE ID=1
	UPDATE @Table SET numChange=dbo.GetPercentageChangeInLeadToAccountConversion(@numDomainID,@dtMonthFromDate,@dtMonthToDate,2) WHERE ID=2
	UPDATE @Table SET numChange=dbo.GetPercentageChangeInGrossProfit(@numDomainID,@dtCYFromDate,@dtCYToDate,1) WHERE ID=3
	UPDATE @Table SET numChange=dbo.GetPercentageChangeInGrossProfit(@numDomainID,@dtMonthFromDate,@dtMonthToDate,2) WHERE ID=4
	UPDATE @Table SET numChange=dbo.GetPercentageChangeInSalesOppToOrderConversion(@numDomainID,@dtCYFromDate,@dtCYToDate,1) WHERE ID=5
	UPDATE @Table SET numChange=dbo.GetPercentageChangeInSalesOppToOrderConversion(@numDomainID,@dtCYFromDate,@dtCYToDate,2) WHERE ID=6
	UPDATE @Table SET numChange=0.00 WHERE ID=7

	SELECT * FROM @Table
END
GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ReportListMaster_RemindersPreBuildReport')
DROP PROCEDURE USP_ReportListMaster_RemindersPreBuildReport
GO
CREATE PROCEDURE [dbo].[USP_ReportListMaster_RemindersPreBuildReport]
	@numDomainID NUMERIC(18,0)
	,@ClientTimeZoneOffset INT
AS
BEGIN 
	DECLARE @bitReOrderPoint AS BIT
	DECLARE @numDefaultSalesShippingDoc AS NUMERIC(18,0)

	DECLARE @TimeAndExpenseToReview INT
	DECLARE @PriceMarginToReview INT
	DECLARE @PromotionApproval INT
	DECLARE @SalesOppStagesToComplete INT
	DECLARE @PurchaseOppStagesToComplete INT
	DECLARE @SalesOrderStagesToComplete INT
	DECLARE @ProjectStagesToComplete INT
	DECLARE @CasesToSolve INT
	DECLARE @PendingSalesRMA INT
	DECLARE @PaymentsToDeposite INT
	DECLARE @BillsToPay INT
	DECLARE @PurchaseOrderToBill INT
	DECLARE @SalesOrderToPickAndPack INT
	DECLARE @SalesOrderToShip INT
	DECLARE @SalesOrderToInvoice INT
	DECLARE @SalesOrderWithItemsToPurchase INT
	DECLARE @SalesOrderPastTheirReleaseDate INT
	DECLARE @ItemOnHandLessThenReorder INT

	SELECT 
		@bitReOrderPoint=ISNULL(bitReOrderPoint,0)
		,@numDefaultSalesShippingDoc = ISNULL(numDefaultSalesShippingDoc,0)
	FROM 
		Domain 
	WHERE 
		numDomainId = @numDomainID

	DECLARE @TEMP TABLE
	(
		ID INT
		,Value INT
	)

	---------------------------- 1 Billable time & expense entries to review -----------------------------------

	SELECT 
		@TimeAndExpenseToReview = COUNT(numCategoryHDRID)
	FROM 
		TimeAndExpense TE
	WHERE 
		TE.numDomainID = @numDOmainID
		AND TE.numApprovalComplete NOT IN (0)

	---------------------------- 2 Minimum price margin violations to review -----------------------------------

	SELECT 
		@PriceMarginToReview = COUNT(M.numOppID)
	FROM 
		OpportunityMaster AS M
	WHERE 
		M.numDomainId=@numDomainID
		AND (SELECT 
				COUNT(OpportunityItems.numoppitemtCode) 
			FROM 
				OpportunityItems 
			WHERE 
				OpportunityItems.bitItemPriceApprovalRequired=1 
				AND OpportunityItems.numOppId=M.numOppId
			) > 0

	---------------------------- 3 Promotions in sales orders to approve -----------------------------------

	SELECT 
		@PromotionApproval = COUNT(numOppID)
	FROM 
		OpportunityMaster
	WHERE 
		numDomainID=@numDomainId
		AND tintOppType = 1
		AND ISNULL(intPromotionApprovalStatus,0) NOT IN (0,-1) 

	---------------------------- 4 Sales OpportunityOrder Stages to complete -----------------------------------

	SELECT 
		@SalesOppStagesToComplete = COUNT(*) 
	FROM 
		StagePercentageDetails SPD 
	INNER JOIN 
		OpportunityMaster OM 
	ON 
		SPD.numOppID=OM.numOppId 
	WHERE 
		SPD.numDomainId=@numDOmainID 
		AND OM.numDomainId=@numDOmainID 
		AND OM.tintOppType = 1
		AND ISNULL(OM.tintOppStatus,0) = 0
		AND tinProgressPercentage <> 100

	---------------------------- 18 Purchase Opportunities Stages to complete -----------------------------------

	SELECT 
		@PurchaseOppStagesToComplete = COUNT(*) 
	FROM 
		StagePercentageDetails SPD 
	INNER JOIN 
		OpportunityMaster OM 
	ON 
		SPD.numOppID=OM.numOppId 
	WHERE 
		SPD.numDomainId=@numDOmainID 
		AND OM.numDomainId=@numDOmainID 
		AND OM.tintOppType = 2
		AND ISNULL(OM.tintOppStatus,0) = 0
		AND tinProgressPercentage <> 100

	---------------------------- 19 Sales Order Stages to complete -----------------------------------

	SELECT 
		@SalesOrderStagesToComplete = COUNT(*) 
	FROM 
		StagePercentageDetails SPD 
	INNER JOIN 
		OpportunityMaster OM 
	ON 
		SPD.numOppID=OM.numOppId
	INNER JOIN
		DivisionMaster DM
	ON
		OM.numDivisionID = DM.numDivisionID
	INNER JOIN
		AdditionalContactsInformation ACI
	ON
		OM.numContactId = ACI.numContactId
	WHERE 
		SPD.numDomainId=@numDOmainID 
		AND OM.numDomainId=@numDOmainID 
		AND OM.tintOppType = 1
		AND ISNULL(OM.tintOppStatus,0) = 1
		AND tinProgressPercentage <> 100

	---------------------------- 5 Project Stages to complete -----------------------------------

	SELECT 
		@ProjectStagesToComplete = COUNT(*) 
	FROM 
		StagePercentageDetails SPD 
	INNER JOIN 
		ProjectsMaster 
	ON 
		SPD.numProjectID=ProjectsMaster.numProId 
	WHERE 
		SPD.numDomainId=@numDOmainID 
		AND ProjectsMaster.numDomainId=@numDOmainID 
		AND tinProgressPercentage <> 100

	---------------------------- 6 Cases to solve -----------------------------------

	SELECT 
		@CasesToSolve = COUNT(*) 
	FROM 
		Cases 
	INNER JOIN
		AdditionalContactsInformation
	ON
		Cases.numContactId = AdditionalContactsInformation.numContactId
	WHERE 
		Cases.numDomainID=@numDOmainID 
		AND numStatus <> 136
	
	---------------------------- 7 Pending Sales RMAs -----------------------------------

	SELECT 
		@PendingSalesRMA = COUNT(*) 
	FROM 
		ReturnHeader 
	WHERE 
		numDomainId=@numDomainID 
		AND tintReturnType = 1 
		AND numReturnStatus = 301

	---------------------------- 8 Payments to deposit -----------------------------------

	SELECT 
		@PaymentsToDeposite = COUNT(numDepositId)
	FROM 
		dbo.DepositMaster DM
	WHERE 
		DM.numDomainId = @numDomainID
		AND tintDepositeToType = 2
		AND ISNULL(bitDepositedToAcnt,0) = 0 

	---------------------------- 9 Bills to pay -----------------------------------

	SELECT 
		@BillsToPay = COUNT(*)
	FROM
	(
		SELECT  
			OBD.numOppBizDocsId
		FROM    
			OpportunityBizDocs OBD
		INNER JOIN 
			OpportunityMaster Opp 
		ON 
			OBD.numOppId = Opp.numOppId
		INNER JOIN 
			AdditionalContactsInformation ADC 
		ON 
			Opp.numContactId = ADC.numContactId
		INNER JOIN 
			DivisionMaster Div 
		ON 
			Opp.numDivisionId = Div.numDivisionID 
			AND ADC.numDivisionId = Div.numDivisionID
		INNER JOIN 
			CompanyInfo C 
		ON 
			Div.numCompanyID = C.numCompanyId
		WHERE 
			Opp.numDomainId = @numDomainId
			AND OBD.bitAuthoritativeBizDocs = 1
			AND Opp.tintOppType = 2
			AND ( OBD.monDealAmount - OBD.monAmountPaid ) > 0
			UNION       
			--Regular Bills For Add Bill Payment
		SELECT  BH.numBillID FROM dbo.BillHeader BH	WHERE BH.numDomainId = @numDomainId AND BH.bitIsPaid = 0
	) TEMP

	---------------------------- 10 Purchase Orders to bill -----------------------------------

	SELECT
		@PurchaseOrderToBill = COUNT(numOppID)
	FROM
	(
		SELECT DISTINCT
			OM.numOppId
		FROM
			OpportunityMaster OM
		INNER JOIN
			OpportunityItems OI
		ON
			OI.numOppId = OM.numOppID
		INNER JOIN
			Item I
		ON
			OI.numItemCode = I.numItemCode
		OUTER APPLY
		(
			SELECT
				SUM(OpportunityBizDocItems.numUnitHour) AS BilledQty
			FROM
				OpportunityBizDocs
			INNER JOIN
				OpportunityBizDocItems 
			ON
				OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
			WHERE
				OpportunityBizDocs.numOppId = OM.numOppID
				AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
				AND OpportunityBizDocs.numBizDocId = (SELECT ISNULL(numAuthoritativePurchase,0) FROM AuthoritativeBizDocs WHERE numDomainId=@numDomainID)
		) AS TempBilled
		WHERE
			OM.numDomainId = @numDomainID
			AND ISNULL(OM.bitStockTransfer,0) = 0
			AND OM.tintOppType = 2
			AND OM.tintOppStatus  = 1
			AND ISNULL(OI.numUnitHour,0) <> ISNULL(TempBilled.BilledQty,0)
	) TEMP


	---------------------------- 11 Sales Orders to Pick & Pack -----------------------------------

	IF @numDefaultSalesShippingDoc > 0
	BEGIN
		SELECT
			@SalesOrderToPickAndPack = COUNT(numOppId)
		FROM
		(
			SELECT DISTINCT
				OM.numOppId
			FROM
				OpportunityMaster OM
			INNER JOIN
				OpportunityItems OI
			ON
				OI.numOppId = OM.numOppId
			INNER JOIN
				Item I
			ON
				OI.numItemCode = I.numItemCode
			OUTER APPLY
			(
				SELECT
					SUM(OpportunityBizDocItems.numUnitHour) AS PickPacked
				FROM
					OpportunityBizDocs
				INNER JOIN
					OpportunityBizDocItems 
				ON
					OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
				WHERE
					OpportunityBizDocs.numOppId = OM.numOppId
					AND ISNULL(OpportunityBizDocs.numBizDocId,0) = @numDefaultSalesShippingDoc
					AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
			) AS TempFulFilled
			WHERE
				OM.numDomainId = @numDomainID
				AND OM.tintOppType = 1
				AND OM.tintOppStatus = 1
				AND ISNULL(OM.tintshipped,0) = 0
				AND UPPER(I.charItemType) = 'P'
				AND ISNULL(OI.bitDropShip,0) = 0
				AND ISNULL(OI.numUnitHour,0) <> ISNULL(TempFulFilled.PickPacked,0)
		) TEMP
	END

	---------------------------- 12 Sales Orders to ship -----------------------------------

	SELECT
		@SalesOrderToShip = COUNT(numOppId)
	FROM
	(
		SELECT DISTINCT
			OM.numOppId
		FROM
			OpportunityMaster OM
		INNER JOIN
			OpportunityItems OI
		ON
			OI.numOppId = OM.numOppId
		INNER JOIN
			Item I
		ON
			OI.numItemCode = I.numItemCode
		OUTER APPLY
		(
			SELECT
				SUM(OpportunityBizDocItems.numUnitHour) AS FulFilledQty
			FROM
				OpportunityBizDocs
			INNER JOIN
				OpportunityBizDocItems 
			ON
				OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
			WHERE
				OpportunityBizDocs.numOppId = OM.numOppId
				AND ISNULL(OpportunityBizDocs.numBizDocId,0) = 296
				AND ISNULL(OpportunityBizDocs.bitFulFilled,0) = 1
				AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
		) AS TempFulFilled
		WHERE
			OM.numDomainId = @numDomainID
			AND OM.tintOppType = 1
			AND OM.tintOppStatus = 1
			AND ISNULL(OM.tintshipped,0) = 0
			AND UPPER(I.charItemType) = 'P'
			AND ISNULL(OI.bitDropShip,0) = 0
			AND ISNULL(OI.numUnitHour,0) <> ISNULL(TempFulFilled.FulFilledQty,0)
	) TEMP

	---------------------------- 13 Sales Orders to invoice -----------------------------------

	SELECT
		@SalesOrderToInvoice = COUNT(numOppID)
	FROM
	(
		SELECT DISTINCT
			OM.numOppId
		FROM
			OpportunityMaster OM
		INNER JOIN
			OpportunityItems OI
		ON
			OI.numOppId = OM.numOppID
		INNER JOIN
			Item I
		ON
			OI.numItemCode = I.numItemCode
		OUTER APPLY
		(
			SELECT
				SUM(OpportunityBizDocItems.numUnitHour) AS InvoicedQty
			FROM
				OpportunityBizDocs
			INNER JOIN
				OpportunityBizDocItems 
			ON
				OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
			WHERE
				OpportunityBizDocs.numOppId = OM.numOppID
				AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
				AND OpportunityBizDocs.numBizDocId = (SELECT ISNULL(numAuthoritativeSales,0) FROM AuthoritativeBizDocs WHERE numDomainId=@numDomainID)
		) AS TempInvoice
		WHERE
			OM.numDomainId = @numDomainID
			AND OM.tintOppType = 1
			AND OM.tintOppStatus  = 1
			AND ISNULL(OI.numUnitHour,0) <> ISNULL(TempInvoice.InvoicedQty,0)
	) TEMP

	---------------------------- 14 Sales Orders with items to purchase -----------------------------------

	DECLARE @TEMPOppID TABLE
	(
		numOppID NUMERIC(18,0)
	)

	INSERT INTO @TEMPOppID
	(
		numOppID
	)
	SELECT DISTINCT
		OM.numOppID
	FROM
		OpportunityMaster OM
	INNER JOIN
		OpportunityItems OI
	ON
		OM.numOppId = OI.numOppId
	WHERE   
		OM.numDomainId = @numDomainId
		AND OM.tintOppType = 1
		AND OM.tintOppStatus=1
		AND ISNULL(OI.bitDropShip, 0) = 1
		AND 1 = (CASE WHEN (SELECT COUNT(*) FROM OpportunityLinking OMInner INNER JOIN OpportunityItems OIInner ON OMInner.numChildOppID=OIInner.numOppId WHERE OMInner.numParentOppID = OM.numOppID AND OIInner.numItemCode=OI.numItemCode AND ISNULL(OIInner.numWarehouseItmsID,0)=ISNULL(OI.numWarehouseItmsID,0)) > 0 THEN 0 ELSE 1 END)


	If ISNULL(@bitReOrderPoint,0) = 1
	BEGIN
		INSERT INTO @TEMPOppID
		(
			numOppID
		)
		SELECT DISTINCT
			OM.numOppID
		FROM
			OpportunityMaster OM
		INNER JOIN
			OpportunityItems OI
		ON
			OI.numOppId = OM.numOppId
		INNER JOIN 
			Item I 
		ON 
			I.numItemCode = OI.numItemCode
		INNER JOIN 
			WarehouseItems WI 
		ON 
			OI.numWarehouseItmsID = WI.numWareHouseItemID
		WHERE   
			OM.numDomainId = @numDomainId
			AND ISNULL(@bitReOrderPoint,0) = 1
			AND (ISNULL(WI.numOnHand,0) + ISNULL(WI.numOnOrder,0) <= ISNULL(WI.numReOrder,0))
			AND OM.tintOppType = 1
			AND OM.tintOppStatus=1
			AND ISNULL(I.bitAssembly, 0) = 0
			AND ISNULL(I.bitKitParent, 0) = 0
			AND ISNULL(OI.bitDropship,0) = 0
			AND OM.numOppId NOT IN (SELECT numOppID FROM @TEMPOppID)
	END
	
	---------------------------- 15 Sales Orders past their release date -----------------------------------

	SELECT
		@SalesOrderPastTheirReleaseDate = COUNT(*)
	FROM
		OpportunityMaster OM
	WHERE
		numDomainId=@numDomainID
		AND tintOppType=1
		AND tintOppStatus=1
		AND ISNULL(OM.tintshipped,0) = 0
		AND dtReleaseDate IS NOT NULL
		AND dtReleaseDate < DateAdd(minute, -@ClientTimeZoneOffset,GETUTCDATE())
		AND (SELECT 
				COUNT(*) 
			FROM 
			(
				SELECT
					OI.numoppitemtCode,
					ISNULL(OI.numUnitHour,0) AS OrderedQty,
					ISNULL(TempFulFilled.FulFilledQty,0) AS FulFilledQty
				FROM
					OpportunityItems OI
				INNER JOIN
					Item I
				ON
					OI.numItemCode = I.numItemCode
				OUTER APPLY
				(
					SELECT
						SUM(OpportunityBizDocItems.numUnitHour) AS FulFilledQty
					FROM
						OpportunityBizDocs
					INNER JOIN
						OpportunityBizDocItems 
					ON
						OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
					WHERE
						OpportunityBizDocs.numOppId = OM.numOppId
						AND ISNULL(OpportunityBizDocs.numBizDocId,0) = 296
						AND ISNULL(OpportunityBizDocs.bitFulFilled,0) = 1
						AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
				) AS TempFulFilled
				WHERE
					OI.numOppID = OM.numOppID
					AND UPPER(I.charItemType) = 'P'
					AND ISNULL(OI.bitDropShip,0) = 0
			) X
			WHERE
				X.OrderedQty <> X.FulFilledQty) > 0

	---------------------------- 17 Items to buy where qty-on-hand is =< the reorder qty -----------------------------------
	 
	SELECT 
		@ItemOnHandLessThenReorder = COUNT(numItemID)
	FROM
	(
		SELECT DISTINCT
			numItemID
		FROM
			WareHouseItems
		INNER JOIN
			Item
		ON
			WareHouseItems.numItemID = Item.numItemCode
		WHERE
			WareHouseItems.numDomainID=@numDomainID
			AND Item.numDomainID = @numDomainID
			AND ISNULL(Item.bitArchiveItem,0) = 0
			AND ISNULL(Item.IsArchieve,0) = 0
			AND ISNULL(numReorder,0) > 0 
			AND ISNULL(numOnHand,0) < ISNULL(numReorder,0)
	) TEMP


	INSERT INTO @TEMP (ID,Value) VALUES (1,ISNULL(@TimeAndExpenseToReview,0))
	INSERT INTO @TEMP (ID,Value) VALUES (2,ISNULL(@PriceMarginToReview,0))
	INSERT INTO @TEMP (ID,Value) VALUES (3,ISNULL(@PromotionApproval,0))
	INSERT INTO @TEMP (ID,Value) VALUES (4,ISNULL(@SalesOppStagesToComplete,0))
	INSERT INTO @TEMP (ID,Value) VALUES (5,ISNULL(@ProjectStagesToComplete,0))
	INSERT INTO @TEMP (ID,Value) VALUES (6,ISNULL(@CasesToSolve,0))
	INSERT INTO @TEMP (ID,Value) VALUES (7,ISNULL(@PendingSalesRMA,0))
	INSERT INTO @TEMP (ID,Value) VALUES (8,ISNULL(@PaymentsToDeposite,0))
	INSERT INTO @TEMP (ID,Value) VALUES (9,ISNULL(@BillsToPay,0))
	INSERT INTO @TEMP (ID,Value) VALUES (10,ISNULL(@PurchaseOrderToBill,0))
	INSERT INTO @TEMP (ID,Value) VALUES (11,ISNULL(@SalesOrderToPickAndPack,0))
	INSERT INTO @TEMP (ID,Value) VALUES (12,ISNULL(@SalesOrderToShip,0))
	INSERT INTO @TEMP (ID,Value) VALUES (13,ISNULL(@SalesOrderToInvoice,0))
	INSERT INTO @TEMP (ID,Value) VALUES (14,ISNULL((SELECT COUNT(*) FROM @TEMPOppID),0))
	INSERT INTO @TEMP (ID,Value) VALUES (15,ISNULL(@SalesOrderPastTheirReleaseDate,0))
	INSERT INTO @TEMP (ID,Value) VALUES (16,0)
	INSERT INTO @TEMP (ID,Value) VALUES (17,ISNULL(@ItemOnHandLessThenReorder,0))
	INSERT INTO @TEMP (ID,Value) VALUES (18,ISNULL(@PurchaseOppStagesToComplete,0))
	INSERT INTO @TEMP (ID,Value) VALUES (19,ISNULL(@SalesOrderStagesToComplete,0))

	SELECT * FROM @TEMP
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ReportListMaster_RPELastMonthAndSamePeriodLastYear')
DROP PROCEDURE USP_ReportListMaster_RPELastMonthAndSamePeriodLastYear
GO
CREATE PROCEDURE [dbo].[USP_ReportListMaster_RPELastMonthAndSamePeriodLastYear]
	@numDomainID NUMERIC(18,0)
AS
BEGIN 
	DECLARE @ExpenseLastMonth MONEY
	DECLARE @ExpenseSamePeriodLastYear MONEY
	DECLARE @ProfitLastMonth MONEY
	DECLARE @ProfitSamePeriodLastYear MONEY
	DECLARE @RevenueLastMonth MONEY
	DECLARE @RevenueSamePeriodLastYear MONEY
	DECLARE @numShippingItemID NUMERIC(18,0)
	DECLARE @numDiscountItemID NUMERIC(18,0)
	DECLARE @ProfitCost INT
	DECLARE @dtLastMonthFromDate AS DATETIME                                       
	DECLARE @dtLastMonthToDate AS DATETIME
	DECLARE @dtSameMonthLYFromDate AS DATETIME                                       
	DECLARE @dtSameMonthLYToDate AS DATETIME

	DECLARE @FinancialYearStartDate AS DATETIME
	SELECT @FinancialYearStartDate = dbo.GetFiscalStartDate(YEAR(GETDATE()),@numDomainID)

	SELECT @dtLastMonthFromDate = DATEADD(mm,-1,DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0)),@dtLastMonthToDate = DATEADD(ms,-3,DATEADD(mm,0,DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0)));
	SELECT @dtSameMonthLYFromDate = DATEADD(YEAR,-1,@dtLastMonthFromDate),@dtSameMonthLYToDate = DATEADD(YEAR,-1,@dtLastMonthToDate);

	SELECT 
		@numShippingItemID=numShippingServiceItemID
		,@numDiscountItemID=numDiscountServiceItemID
		,@ProfitCost=numCost 
	FROM 
		Domain 
	WHERE 
		numDomainId=@numDomainID


	-- GET EXPENSE YTD VS SAME PERIOD LAST YEAR
	SELECT @ExpenseLastMonth = SUM(numDebitAmt) - SUM(numCreditAmt) FROM General_Journal_Details INNER JOIN General_Journal_Header ON General_Journal_Details.numJournalId=General_Journal_Header.numJournal_Id WHERE General_Journal_Details.numDomainId=@numDomainID AND datEntry_Date BETWEEN  @dtLastMonthFromDate AND @dtLastMonthToDate AND General_Journal_Details.numChartAcntId IN (SELECT numAccountId FROM Chart_Of_Accounts WHERE numDomainId=@numDomainID AND bitActive = 1 AND numAcntTypeId IN (SELECT numAccountTypeID FROM AccountTypeDetail WHERE numDomainID=@numDomainID AND vcAccountCode LIKE '0104%'))
	SELECT @ExpenseSamePeriodLastYear = SUM(numDebitAmt) - SUM(numCreditAmt) FROM General_Journal_Details INNER JOIN General_Journal_Header ON General_Journal_Details.numJournalId=General_Journal_Header.numJournal_Id WHERE General_Journal_Details.numDomainId=@numDomainID AND datEntry_Date BETWEEN  @dtSameMonthLYFromDate AND @dtSameMonthLYToDate AND General_Journal_Details.numChartAcntId IN (SELECT numAccountId FROM Chart_Of_Accounts WHERE numDomainId=@numDomainID AND bitActive = 1 AND numAcntTypeId IN (SELECT numAccountTypeID FROM AccountTypeDetail WHERE numDomainID=@numDomainID AND vcAccountCode LIKE '0104%'))

	-- GET PROFIT YTD VS SAME PERIOD LAST YEAR
	SELECT 
		@ProfitLastMonth = SUM(Profit)
	FROM
	(
		SELECT
			ISNULL(monTotAmount,0) - (ISNULL(numUnitHour,0) * (CASE @ProfitCost WHEN 3 THEN ISNULL(V.monCost,0) * dbo.fn_UOMConversion(numBaseUnit,I.numItemCode,@numDomainID,numPurchaseUnit) WHEN 2 THEN numCost * dbo.fn_UOMConversion(numBaseUnit,I.numItemCode,@numDomainID,numUOMId) ELSE monAverageCost END)) Profit
		FROM
			OpportunityItems OI
		INNER JOIN
			OpportunityMaster OM
		ON
			OI.numOppId = OM.numOppID
		INNER JOIN
			DivisionMaster DM
		ON
			OM.numDivisionId = DM.numDivisionID
		INNER JOIN
			CompanyInfo CI
		ON
			DM.numCompanyID = CI.numCompanyId
		INNER JOIN
			Item I
		ON
			OI.numItemCode = I.numItemCode
		Left JOIN 
			Vendor V 
		ON 
			V.numVendorID=I.numVendorID 
			AND V.numItemCode=I.numItemCode
		WHERE
			OM.numDomainId=@numDomainID
			AND OM.bintCreatedDate BETWEEN  @dtLastMonthFromDate AND @dtLastMonthToDate
			AND ISNULL(OM.tintOppType,0)=1
			AND ISNULL(OM.tintOppStatus,0)=1
			AND I.numItemCode NOT IN (@numShippingItemID,@numDiscountItemID)
	) TEMP

	SELECT 
		@ProfitSamePeriodLastYear = SUM(Profit)
	FROM
	(
		SELECT
			ISNULL(monTotAmount,0) - (ISNULL(numUnitHour,0) * (CASE @ProfitCost WHEN 3 THEN ISNULL(V.monCost,0) * dbo.fn_UOMConversion(numBaseUnit,I.numItemCode,@numDomainID,numPurchaseUnit) WHEN 2 THEN numCost * dbo.fn_UOMConversion(numBaseUnit,I.numItemCode,@numDomainID,numUOMId) ELSE monAverageCost END)) Profit
		FROM
			OpportunityItems OI
		INNER JOIN
			OpportunityMaster OM
		ON
			OI.numOppId = OM.numOppID
		INNER JOIN
			DivisionMaster DM
		ON
			OM.numDivisionId = DM.numDivisionID
		INNER JOIN
			CompanyInfo CI
		ON
			DM.numCompanyID = CI.numCompanyId
		INNER JOIN
			Item I
		ON
			OI.numItemCode = I.numItemCode
		Left JOIN 
			Vendor V 
		ON 
			V.numVendorID=I.numVendorID 
			AND V.numItemCode=I.numItemCode
		WHERE
			OM.numDomainId=@numDomainID
			AND OM.bintCreatedDate BETWEEN  @dtSameMonthLYFromDate AND @dtSameMonthLYToDate
			AND ISNULL(OM.tintOppType,0)=1
			AND ISNULL(OM.tintOppStatus,0)=1
			AND I.numItemCode NOT IN (@numShippingItemID,@numDiscountItemID)
	) TEMP

	-- GET REVENUE YTD VS SAME PERIOD LAST YEAR
	SELECT 
		@RevenueLastMonth = SUM(monTotAmount)
	FROM
	(
		SELECT
			ISNULL(monTotAmount,0) monTotAmount
		FROM
			OpportunityItems OI
		INNER JOIN
			OpportunityMaster OM
		ON
			OI.numOppId = OM.numOppID
		INNER JOIN
			DivisionMaster DM
		ON
			OM.numDivisionId = DM.numDivisionID
		INNER JOIN
			CompanyInfo CI
		ON
			DM.numCompanyID = CI.numCompanyId
		INNER JOIN
			Item I
		ON
			OI.numItemCode = I.numItemCode
		Left JOIN 
			Vendor V 
		ON 
			V.numVendorID=I.numVendorID 
			AND V.numItemCode=I.numItemCode
		WHERE
			OM.numDomainId=@numDomainID
			AND OM.bintCreatedDate BETWEEN  @dtLastMonthFromDate AND @dtLastMonthToDate
			AND ISNULL(OM.tintOppType,0)=1
			AND ISNULL(OM.tintOppStatus,0)=1
	) TEMP

	SELECT 
		@RevenueSamePeriodLastYear = SUM(monTotAmount)
	FROM
	(
		SELECT
			ISNULL(monTotAmount,0) monTotAmount
		FROM
			OpportunityItems OI
		INNER JOIN
			OpportunityMaster OM
		ON
			OI.numOppId = OM.numOppID
		INNER JOIN
			DivisionMaster DM
		ON
			OM.numDivisionId = DM.numDivisionID
		INNER JOIN
			CompanyInfo CI
		ON
			DM.numCompanyID = CI.numCompanyId
		INNER JOIN
			Item I
		ON
			OI.numItemCode = I.numItemCode
		Left JOIN 
			Vendor V 
		ON 
			V.numVendorID=I.numVendorID 
			AND V.numItemCode=I.numItemCode
		WHERE
			OM.numDomainId=@numDomainID
			AND OM.bintCreatedDate BETWEEN  @dtSameMonthLYFromDate AND @dtSameMonthLYToDate
			AND ISNULL(OM.tintOppType,0)=1
			AND ISNULL(OM.tintOppStatus,0)=1
	) TEMP

	SELECT
		100.0 * (ISNULL(@RevenueLastMonth,0) - ISNULL(@RevenueSamePeriodLastYear,0)) / ISNULL(@RevenueSamePeriodLastYear,0) As RevenueDifference
		,100.0 * (ISNULL(@ProfitLastMonth,0) - ISNULL(@ProfitSamePeriodLastYear,0)) / ISNULL(@ProfitSamePeriodLastYear,0) As ProfitDifference
		,100.0 * (ISNULL(@ExpenseLastMonth,0) - ISNULL(@ExpenseSamePeriodLastYear,0)) / ISNULL(@ExpenseSamePeriodLastYear,0) As ExpenseDifference
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ReportListMaster_RPEYTDAndSamePeriodLastYear')
DROP PROCEDURE USP_ReportListMaster_RPEYTDAndSamePeriodLastYear
GO
CREATE PROCEDURE [dbo].[USP_ReportListMaster_RPEYTDAndSamePeriodLastYear]
	@numDomainID NUMERIC(18,0)
AS
BEGIN 
	DECLARE @ExpenseYTD MONEY
	DECLARE @ExpenseSamePeriodLastYear MONEY
	DECLARE @ProfitYTD MONEY
	DECLARE @ProfitSamePeriodLastYear MONEY
	DECLARE @RevenueYTD MONEY
	DECLARE @RevenueSamePeriodLastYear MONEY
	DECLARE @numShippingItemID NUMERIC(18,0)
	DECLARE @numDiscountItemID NUMERIC(18,0)
	DECLARE @ProfitCost INT
	DECLARE @dtCYFromDate AS DATETIME                                       
	DECLARE @dtCYToDate AS DATETIME
	DECLARE @dtLYFromDate AS DATETIME                                       
	DECLARE @dtLYToDate AS DATETIME

	DECLARE @FinancialYearStartDate AS DATETIME
	SELECT @FinancialYearStartDate = dbo.GetFiscalStartDate(YEAR(GETDATE()),@numDomainID)

	SELECT @dtCYFromDate = @FinancialYearStartDate,@dtCYToDate = dateadd(ms, -3, dateadd(day, datediff(day, 0, getdate())+1, 0));
	SELECT @dtLYFromDate = DATEADD(YEAR,-1,@FinancialYearStartDate),@dtLYToDate = DATEADD(MS,-1,DATEADD(YEAR,-1,DATEADD(MS, -3, DATEADD(DAY, DATEDIFF(DAY, 0, GETDATE())+1, 0))));

	SELECT 
		@numShippingItemID=numShippingServiceItemID
		,@numDiscountItemID=numDiscountServiceItemID
		,@ProfitCost=numCost 
	FROM 
		Domain 
	WHERE 
		numDomainId=@numDomainID


	-- GET EXPENSE YTD VS SAME PERIOD LAST YEAR
	SELECT @ExpenseYTD = SUM(numDebitAmt) - SUM(numCreditAmt) FROM General_Journal_Details INNER JOIN General_Journal_Header ON General_Journal_Details.numJournalId=General_Journal_Header.numJournal_Id WHERE General_Journal_Details.numDomainId=@numDomainID AND datEntry_Date BETWEEN  @dtCYFromDate AND @dtCYToDate AND General_Journal_Details.numChartAcntId IN (SELECT numAccountId FROM Chart_Of_Accounts WHERE numDomainId=@numDomainID AND bitActive = 1 AND numAcntTypeId IN (SELECT numAccountTypeID FROM AccountTypeDetail WHERE numDomainID=@numDomainID AND vcAccountCode LIKE '0104%'))
	SELECT @ExpenseSamePeriodLastYear = SUM(numDebitAmt) - SUM(numCreditAmt) FROM General_Journal_Details INNER JOIN General_Journal_Header ON General_Journal_Details.numJournalId=General_Journal_Header.numJournal_Id WHERE General_Journal_Details.numDomainId=@numDomainID AND datEntry_Date BETWEEN  @dtLYFromDate AND @dtLYToDate AND General_Journal_Details.numChartAcntId IN (SELECT numAccountId FROM Chart_Of_Accounts WHERE numDomainId=@numDomainID AND bitActive = 1 AND numAcntTypeId IN (SELECT numAccountTypeID FROM AccountTypeDetail WHERE numDomainID=@numDomainID AND vcAccountCode LIKE '0104%'))

	-- GET PROFIT YTD VS SAME PERIOD LAST YEAR
	SELECT 
		@ProfitYTD = SUM(Profit)
	FROM
	(
		SELECT
			ISNULL(monTotAmount,0) - (ISNULL(numUnitHour,0) * (CASE @ProfitCost WHEN 3 THEN ISNULL(V.monCost,0) * dbo.fn_UOMConversion(numBaseUnit,I.numItemCode,@numDomainID,numPurchaseUnit) WHEN 2 THEN numCost * dbo.fn_UOMConversion(numBaseUnit,I.numItemCode,@numDomainID,numUOMId) ELSE monAverageCost END)) Profit
		FROM
			OpportunityItems OI
		INNER JOIN
			OpportunityMaster OM
		ON
			OI.numOppId = OM.numOppID
		INNER JOIN
			DivisionMaster DM
		ON
			OM.numDivisionId = DM.numDivisionID
		INNER JOIN
			CompanyInfo CI
		ON
			DM.numCompanyID = CI.numCompanyId
		INNER JOIN
			Item I
		ON
			OI.numItemCode = I.numItemCode
		Left JOIN 
			Vendor V 
		ON 
			V.numVendorID=I.numVendorID 
			AND V.numItemCode=I.numItemCode
		WHERE
			OM.numDomainId=@numDomainID
			AND OM.bintCreatedDate BETWEEN  @dtCYFromDate AND @dtCYToDate
			AND ISNULL(OM.tintOppType,0)=1
			AND ISNULL(OM.tintOppStatus,0)=1
			AND I.numItemCode NOT IN (@numShippingItemID,@numDiscountItemID)
	) TEMP

	SELECT 
		@ProfitSamePeriodLastYear = SUM(Profit)
	FROM
	(
		SELECT
			ISNULL(monTotAmount,0) - (ISNULL(numUnitHour,0) * (CASE @ProfitCost WHEN 3 THEN ISNULL(V.monCost,0) * dbo.fn_UOMConversion(numBaseUnit,I.numItemCode,@numDomainID,numPurchaseUnit) WHEN 2 THEN numCost * dbo.fn_UOMConversion(numBaseUnit,I.numItemCode,@numDomainID,numUOMId) ELSE monAverageCost END)) Profit
		FROM
			OpportunityItems OI
		INNER JOIN
			OpportunityMaster OM
		ON
			OI.numOppId = OM.numOppID
		INNER JOIN
			DivisionMaster DM
		ON
			OM.numDivisionId = DM.numDivisionID
		INNER JOIN
			CompanyInfo CI
		ON
			DM.numCompanyID = CI.numCompanyId
		INNER JOIN
			Item I
		ON
			OI.numItemCode = I.numItemCode
		Left JOIN 
			Vendor V 
		ON 
			V.numVendorID=I.numVendorID 
			AND V.numItemCode=I.numItemCode
		WHERE
			OM.numDomainId=@numDomainID
			AND OM.bintCreatedDate BETWEEN  @dtLYFromDate AND @dtLYToDate
			AND ISNULL(OM.tintOppType,0)=1
			AND ISNULL(OM.tintOppStatus,0)=1
			AND I.numItemCode NOT IN (@numShippingItemID,@numDiscountItemID)
	) TEMP

	-- GET REVENUE YTD VS SAME PERIOD LAST YEAR
	SELECT 
		@RevenueYTD = SUM(monTotAmount)
	FROM
	(
		SELECT
			ISNULL(monTotAmount,0) monTotAmount
		FROM
			OpportunityItems OI
		INNER JOIN
			OpportunityMaster OM
		ON
			OI.numOppId = OM.numOppID
		INNER JOIN
			DivisionMaster DM
		ON
			OM.numDivisionId = DM.numDivisionID
		INNER JOIN
			CompanyInfo CI
		ON
			DM.numCompanyID = CI.numCompanyId
		INNER JOIN
			Item I
		ON
			OI.numItemCode = I.numItemCode
		Left JOIN 
			Vendor V 
		ON 
			V.numVendorID=I.numVendorID 
			AND V.numItemCode=I.numItemCode
		WHERE
			OM.numDomainId=@numDomainID
			AND OM.bintCreatedDate BETWEEN  @dtCYFromDate AND @dtCYToDate
			AND ISNULL(OM.tintOppType,0)=1
			AND ISNULL(OM.tintOppStatus,0)=1
	) TEMP

	SELECT 
		@RevenueSamePeriodLastYear = SUM(monTotAmount)
	FROM
	(
		SELECT
			ISNULL(monTotAmount,0) monTotAmount
		FROM
			OpportunityItems OI
		INNER JOIN
			OpportunityMaster OM
		ON
			OI.numOppId = OM.numOppID
		INNER JOIN
			DivisionMaster DM
		ON
			OM.numDivisionId = DM.numDivisionID
		INNER JOIN
			CompanyInfo CI
		ON
			DM.numCompanyID = CI.numCompanyId
		INNER JOIN
			Item I
		ON
			OI.numItemCode = I.numItemCode
		Left JOIN 
			Vendor V 
		ON 
			V.numVendorID=I.numVendorID 
			AND V.numItemCode=I.numItemCode
		WHERE
			OM.numDomainId=@numDomainID
			AND OM.bintCreatedDate BETWEEN  @dtLYFromDate AND @dtLYToDate
			AND ISNULL(OM.tintOppType,0)=1
			AND ISNULL(OM.tintOppStatus,0)=1
	) TEMP

	SELECT
		100.0 * (ISNULL(@RevenueYTD,0) - ISNULL(@RevenueSamePeriodLastYear,0)) / ISNULL(@RevenueSamePeriodLastYear,0) As RevenueDifference
		,100.0 * (ISNULL(@ProfitYTD,0) - ISNULL(@ProfitSamePeriodLastYear,0)) / ISNULL(@ProfitSamePeriodLastYear,0) As ProfitDifference
		,100.0 * (ISNULL(@ExpenseYTD,0) - ISNULL(@ExpenseSamePeriodLastYear,0)) / ISNULL(@ExpenseSamePeriodLastYear,0) As ExpenseDifference
END
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ReportListMaster_RunPrebuildReport')
DROP PROCEDURE USP_ReportListMaster_RunPrebuildReport
GO
CREATE PROCEDURE [dbo].[USP_ReportListMaster_RunPrebuildReport]   
@numDomainID AS NUMERIC(18,0),               
@numUserCntID AS NUMERIC(18,0),
@ClientTimeZoneOffset AS INT,
@intDefaultReportID INT               
AS  
BEGIN
	IF @intDefaultReportID = 1 --1 - A/R
	BEGIN
		EXEC USP_ReportListMaster_ARPreBuildReport @numDomainID
	END
	ELSE IF @intDefaultReportID = 2 -- A/P
	BEGIN
		EXEC USP_ReportListMaster_APPreBuildReport @numDomainID
	END
	ELSE IF @intDefaultReportID = 3 -- MONEY IN THE BANK
	BEGIN
		EXEC USP_ReportListMaster_BankPreBuildReport @numDomainID
	END
	ELSE IF @intDefaultReportID = 5 -- TOP 10 ITEMS BY PROFIT AMOUNT (LAST 12 MONTHS)
	BEGIN
		EXEC USP_ReportListMaster_Top10ItemByProfitPreBuildReport @numDomainID
	END
	ELSE IF @intDefaultReportID = 6 -- TOP 10 ITEMS BY REVENUE SOLD (LAST 12 MONTHS)
	BEGIN
		EXEC USP_ReportListMaster_Top10ItemByRevenuePreBuildReport @numDomainID
	END
	ELSE IF @intDefaultReportID = 7  -- PROFIT MARGIN BY ITEM CLASSIFICATION (LAST 12 MONTHS)
	BEGIN
		EXEC USP_ReportListMaster_ItemClassificationProfitPreBuildReport @numDomainID
	END
	ELSE IF @intDefaultReportID = 8  -- TOP 10 CUSTOMERS BY PROFIT MARGIN
	BEGIN
		EXEC USP_ReportListMaster_Top10CustomerByProfitPercentPreBuildReport @numDomainID
	END
	ELSE IF @intDefaultReportID = 9  -- TOP 10 CUSTOMERS BY PROFIT AMOUNT
	BEGIN
		EXEC USP_ReportListMaster_Top10CustomerByProfitAmountPreBuildReport @numDomainID
	END
	ELSE IF @intDefaultReportID = 10  -- ACTION ITEMS & MEETINGS DUE TODAY
	BEGIN
		EXEC USP_ReportListMaster_ActionItemPreBuildReport @numDomainID,0,@ClientTimeZoneOffset
	END
	ELSE IF @intDefaultReportID = 11  -- YTD VS SAME PERIOD LAST YEAR
	BEGIN
		EXEC USP_ReportListMaster_RPEYTDAndSamePeriodLastYear @numDomainID
	END
	ELSE IF @intDefaultReportID = 12  -- ACTION ITEMS & MEETINGS DUE TODAY
	BEGIN
		EXEC USP_ReportListMaster_RPELastMonthAndSamePeriodLastYear @numDomainID
	END
	ELSE IF @intDefaultReportID = 13  --  SALES VS EXPENSES (LAST 12 MONTHS)
	BEGIN
		EXEC USP_ReportListMaster_SalesVsExpenseLast12Months @numDomainID
	END
	ELSE IF @intDefaultReportID = 14  --  TOP SOURCES OF SALES ORDERS (LAST 12 MONTHS)
	BEGIN
		EXEC USP_ReportListMaster_TopSourceOfSalesOrder @numDomainID
	END
	ELSE IF @intDefaultReportID = 15  --  TOP 10 ITEMS BY PROFIT MARGIN
	BEGIN
		EXEC USP_ReportListMaster_Top10ItemByProfitMargin @numDomainID
	END
	ELSE IF @intDefaultReportID = 16  --  TOP 10 SALES OPPORTUNITIES BY REVENUE (LAST 12 MONTHS)
	BEGIN
		EXEC USP_ReportListMaster_Top10SalesOpportunityByRevenue @numDomainID
	END
	ELSE IF @intDefaultReportID = 17  --  TOP 10 SALES OPPORTUNITIES BY TOTAL PROGRESS
	BEGIN
		EXEC USP_ReportListMaster_Top10SalesOpportunityByTotalProgress @numDomainID
	END
	ELSE IF @intDefaultReportID = 18  --  TOP 10 ITEMS RETURNED VS QTY SOLD
	BEGIN
		EXEC USP_ReportListMaster_Top10ItemsByReturnedVsSold @numDomainID
	END
	ELSE IF @intDefaultReportID = 19  --  LARGEST 10 SALES OPPORTUNITIES PAST THEIR DUE DATE
	BEGIN
		EXEC USP_ReportListMaster_Top10SalesOpportunityByPastDue @numDomainID,@ClientTimeZoneOffset
	END
	ELSE IF @intDefaultReportID = 20  --  Top 10 Campaigns by ROI
	BEGIN
		EXEC USP_ReportListMaster_Top10CampaignsByROI @numDomainID
	END
	ELSE IF @intDefaultReportID = 21  --  MARKET FRAGMENTATION � TOP 10 CUSTOMERS, AS A PORTION TOTAL SALES
	BEGIN
		EXEC USP_ReportListMaster_TopCustomersByPortionOfTotalSales @numDomainID
	END
	ELSE IF @intDefaultReportID = 22  --  SCORE CARD
	BEGIN
		EXEC USP_ReportListMaster_PrebuildScoreCard @numDomainID
	END
	ELSE IF @intDefaultReportID = 23  --  TOP 10 LEAD SOURCES (NEW LEADS)
	BEGIN
		EXEC USP_ReportListMaster_Top10LeadSource @numDomainID
	END
	ELSE IF @intDefaultReportID = 24  --  LAST 10 EMAIL MESSAGES FROM PEOPLE I DO BUSINESS WITH
	BEGIN
		EXEC USP_ReportListMaster_Top10Email @numDomainID,@numUserCntID
	END
	ELSE IF @intDefaultReportID = 25  --  Reminders
	BEGIN
		EXEC USP_ReportListMaster_RemindersPreBuildReport @numDomainID,@ClientTimeZoneOffset
	END
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ReportListMaster_SalesVsExpenseLast12Months')
DROP PROCEDURE USP_ReportListMaster_SalesVsExpenseLast12Months
GO
CREATE PROCEDURE [dbo].[USP_ReportListMaster_SalesVsExpenseLast12Months]
	@numDomainID NUMERIC(18,0)
AS
BEGIN 
	DECLARE @StartDate DATETIME
	DECLARE @EndDate DATETIME
	SET @EndDate = EOMONTH (GETDATE()) 

	SET @StartDate = DATEADD(MONTH, DATEDIFF(MONTH, 0, @EndDate), 0)



	DECLARE @TABLE TABLE
	(
		ID INT IDENTITY(1,1)
		,MonthStartDate DATETIME
		,MonthEndDate DATETIME
		,MonthLabel VARCHAR(10)
		,MonthSales MONEY
		,MonthExpense MONEY
	)

	INSERT INTO @TABLE
	(
		MonthStartDate,MonthEndDate,MonthLabel
	)
	VALUES
	(
		DATEADD(MONTH,-11,@StartDate),DATEADD(MONTH,-11,@EndDate),CONCAT(CONVERT(VARCHAR(3), DATEADD(MONTH,-11,@EndDate), 100),' ',YEAR(DATEADD(MONTH,-11,@EndDate)))
	),
	(
		DATEADD(MONTH,-10,@StartDate),DATEADD(MONTH,-10,@EndDate),CONCAT(CONVERT(VARCHAR(3), DATEADD(MONTH,-10,@EndDate), 100),' ',YEAR(DATEADD(MONTH,-10,@EndDate)))
	),
	(
		DATEADD(MONTH,-9,@StartDate),DATEADD(MONTH,-9,@EndDate),CONCAT(CONVERT(VARCHAR(3), DATEADD(MONTH,-9,@EndDate), 100),' ',YEAR(DATEADD(MONTH,-9,@EndDate)))
	),
	(
		DATEADD(MONTH,-8,@StartDate),DATEADD(MONTH,-8,@EndDate),CONCAT(CONVERT(VARCHAR(3), DATEADD(MONTH,-8,@EndDate), 100),' ',YEAR(DATEADD(MONTH,-8,@EndDate)))
	),
	(
		DATEADD(MONTH,-7,@StartDate),DATEADD(MONTH,-7,@EndDate),CONCAT(CONVERT(VARCHAR(3), DATEADD(MONTH,-7,@EndDate), 100),' ',YEAR(DATEADD(MONTH,-7,@EndDate)))
	),
	(
		DATEADD(MONTH,-6,@StartDate),DATEADD(MONTH,-6,@EndDate),CONCAT(CONVERT(VARCHAR(3), DATEADD(MONTH,-6,@EndDate), 100),' ',YEAR(DATEADD(MONTH,-6,@EndDate)))
	),
	(
		DATEADD(MONTH,-5,@StartDate),DATEADD(MONTH,-5,@EndDate),CONCAT(CONVERT(VARCHAR(3), DATEADD(MONTH,-5,@EndDate), 100),' ',YEAR(DATEADD(MONTH,-5,@EndDate)))
	),
	(
		DATEADD(MONTH,-4,@StartDate),DATEADD(MONTH,-4,@EndDate),CONCAT(CONVERT(VARCHAR(3), DATEADD(MONTH,-4,@EndDate), 100),' ',YEAR(DATEADD(MONTH,-4,@EndDate)))
	),
	(
		DATEADD(MONTH,-3,@StartDate),DATEADD(MONTH,-3,@EndDate),CONCAT(CONVERT(VARCHAR(3), DATEADD(MONTH,-3,@EndDate), 100),' ',YEAR(DATEADD(MONTH,-3,@EndDate)))
	),
	(
		DATEADD(MONTH,-2,@StartDate),DATEADD(MONTH,-3,@EndDate),CONCAT(CONVERT(VARCHAR(3), DATEADD(MONTH,-2,@EndDate), 100),' ',YEAR(DATEADD(MONTH,-2,@EndDate)))
	),
	(
		DATEADD(MONTH,-1,@StartDate),DATEADD(MONTH,-2,@EndDate),CONCAT(CONVERT(VARCHAR(3), DATEADD(MONTH,-1,@EndDate), 100),' ',YEAR(DATEADD(MONTH,-1,@EndDate)))
	),
	(
		@StartDate,@EndDate,CONCAT(CONVERT(VARCHAR(3), @EndDate, 100),' ',YEAR(@EndDate))
	)


	DECLARE @i AS INT = 1
	DECLARE @iCount AS INT
	DECLARE @TempSales MONEY = 0
	DECLARE @TempExpense MONEY = 0


	SELECT @iCount=COUNT(*) FROM @TABLE

	WHILE @i <= @iCount
	BEGIN
		SET @TempSales = 0
		SET @TempExpense = 0

		SELECT @StartDate=MonthStartDate,@EndDate=MonthEndDate FROM @TABLE WHERE ID=@i

		SELECT 
			@TempSales = SUM(monTotAmount)
		FROM
		(
			SELECT
				ISNULL(monTotAmount,0) monTotAmount
			FROM
				OpportunityItems OI
			INNER JOIN
				OpportunityMaster OM
			ON
				OI.numOppId = OM.numOppID
			INNER JOIN
				Item I
			ON
				OI.numItemCode = I.numItemCode
			WHERE
				OM.numDomainId=@numDomainID
				AND OM.bintCreatedDate BETWEEN  @StartDate AND @EndDate
				AND ISNULL(OM.tintOppType,0)=1
				AND ISNULL(OM.tintOppStatus,0)=1
		) TEMP
	
		SELECT @TempExpense = SUM(numDebitAmt) - SUM(numCreditAmt) FROM General_Journal_Details INNER JOIN General_Journal_Header ON General_Journal_Details.numJournalId=General_Journal_Header.numJournal_Id WHERE General_Journal_Details.numDomainId=@numDomainID AND datEntry_Date BETWEEN  @StartDate AND @EndDate AND General_Journal_Details.numChartAcntId IN (SELECT numAccountId FROM Chart_Of_Accounts WHERE numDomainId=@numDomainID AND bitActive = 1 AND numAcntTypeId IN (SELECT numAccountTypeID FROM AccountTypeDetail WHERE numDomainID=@numDomainID AND vcAccountCode LIKE '0104%'))

		UPDATE @TABLE SET MonthSales=ISNULL(@TempSales,0), MonthExpense=ISNULL(@TempExpense,0) WHERE ID=@i

		SET @i = @i + 1
	END

	SELECT * FROM @TABLE
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ReportListMaster_Top10CampaignsByROI')
DROP PROCEDURE USP_ReportListMaster_Top10CampaignsByROI
GO
CREATE PROCEDURE [dbo].[USP_ReportListMaster_Top10CampaignsByROI]
	@numDomainID NUMERIC(18,0)
AS
BEGIN 
	SELECT TOP 10
		[vcCampaignName],
		CONCAT('~/Marketing/frmCampaignDetails.aspx?CampID=',numCampaignId) URL,
		dbo.GetIncomeforCampaign(numCampaignId,4,NULL,NULL) as ROI
	FROM 
		CampaignMaster
	WHERE
		numDomainID=@numDomainID
		AND dbo.GetIncomeforCampaign(numCampaignId,4,NULL,NULL) <> 0
	ORDER BY
		dbo.GetIncomeforCampaign(numCampaignId,4,NULL,NULL) DESC
END
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ReportListMaster_Top10CustomerByProfitAmountPreBuildReport')
DROP PROCEDURE USP_ReportListMaster_Top10CustomerByProfitAmountPreBuildReport
GO
CREATE PROCEDURE [dbo].[USP_ReportListMaster_Top10CustomerByProfitAmountPreBuildReport]
	@numDomainID NUMERIC(18,0)
AS
BEGIN 
	DECLARE @numShippingItemID NUMERIC(18,0)
	DECLARE @numDiscountItemID NUMERIC(18,0)
	DECLARE @ProfitCost INT
	SELECT @numShippingItemID=numShippingServiceItemID,@numDiscountItemID=numDiscountServiceItemID,@ProfitCost=numCost FROM Domain WHERE numDomainId=@numDomainID


	SELECT TOP 10
		numDivisionID
		,(CASE tintCRMType WHEN 1 THEN CONCAT('~/prospects/frmProspects.aspx?DivID=',numDivisionID) WHEN 2 THEN CONCAT('~/Account/frmAccounts.aspx?DivID=',numDivisionID) ELSE CONCAT('~/Leads/frmLeads.aspx?DivID=',numDivisionID) END) AS URL
		,vcCompanyName
		,SUM(Profit) Profit
	FROM
	(
		SELECT
			DM.numDivisionID
			,tintCRMType
			,CI.vcCompanyName
			,ISNULL(monTotAmount,0) - (ISNULL(numUnitHour,0) * (CASE @ProfitCost WHEN 3 THEN ISNULL(V.monCost,0) * dbo.fn_UOMConversion(numBaseUnit,I.numItemCode,@numDomainID,numPurchaseUnit) WHEN 2 THEN numCost * dbo.fn_UOMConversion(numBaseUnit,I.numItemCode,@numDomainID,numUOMId) ELSE monAverageCost END)) Profit
		FROM
			OpportunityItems OI
		INNER JOIN
			OpportunityMaster OM
		ON
			OI.numOppId = OM.numOppID
		INNER JOIN
			DivisionMaster DM
		ON
			OM.numDivisionId = DM.numDivisionID
		INNER JOIN
			CompanyInfo CI
		ON
			DM.numCompanyID = CI.numCompanyId
		INNER JOIN
			Item I
		ON
			OI.numItemCode = I.numItemCode
		Left JOIN 
			Vendor V 
		ON 
			V.numVendorID=I.numVendorID 
			AND V.numItemCode=I.numItemCode
		WHERE
			OM.numDomainId=@numDomainID
			AND ISNULL(OI.monTotAmount,0) <> 0
			AND ISNULL(OI.numUnitHour,0) <> 0
			AND ISNULL(OM.tintOppType,0)=1
			AND ISNULL(OM.tintOppStatus,0)=1
			AND I.numItemCode NOT IN (@numShippingItemID,@numDiscountItemID)
	) TEMP
	GROUP BY
		numDivisionID
		,tintCRMType
		,vcCompanyName
	HAVING
		SUM(Profit) > 0
	ORDER BY
		SUM(Profit) DESC
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ReportListMaster_Top10CustomerByProfitPercentPreBuildReport')
DROP PROCEDURE USP_ReportListMaster_Top10CustomerByProfitPercentPreBuildReport
GO
CREATE PROCEDURE [dbo].[USP_ReportListMaster_Top10CustomerByProfitPercentPreBuildReport]
	@numDomainID NUMERIC(18,0)
AS
BEGIN 
	DECLARE @numShippingItemID NUMERIC(18,0)
	DECLARE @numDiscountItemID NUMERIC(18,0)
	DECLARE @ProfitCost INT
	SELECT @numShippingItemID=numShippingServiceItemID,@numDiscountItemID=numDiscountServiceItemID,@ProfitCost=numCost FROM Domain WHERE numDomainId=@numDomainID


	SELECT TOP 10
		numDivisionID
		,vcCompanyName
		,(CASE tintCRMType WHEN 1 THEN CONCAT('~/prospects/frmProspects.aspx?DivID=',numDivisionID) WHEN 2 THEN CONCAT('~/Account/frmAccounts.aspx?DivID=',numDivisionID) ELSE CONCAT('~/Leads/frmLeads.aspx?DivID=',numDivisionID) END) AS URL
		,(SUM(Profit)/SUM(monTotAmount)) * 100 BlendedProfit
	FROM
	(
		SELECT
			DM.numDivisionID
			,tintCRMType
			,CI.vcCompanyName
			,ISNULL(monTotAmount,0) monTotAmount
			,ISNULL(monTotAmount,0) - (ISNULL(numUnitHour,0) * (CASE @ProfitCost WHEN 3 THEN ISNULL(V.monCost,0) * dbo.fn_UOMConversion(numBaseUnit,I.numItemCode,@numDomainID,numPurchaseUnit) WHEN 2 THEN numCost * dbo.fn_UOMConversion(numBaseUnit,I.numItemCode,@numDomainID,numUOMId) ELSE monAverageCost END)) Profit
		FROM
			OpportunityItems OI
		INNER JOIN
			OpportunityMaster OM
		ON
			OI.numOppId = OM.numOppID
		INNER JOIN
			DivisionMaster DM
		ON
			OM.numDivisionId = DM.numDivisionID
		INNER JOIN
			CompanyInfo CI
		ON
			DM.numCompanyID = CI.numCompanyId
		INNER JOIN
			Item I
		ON
			OI.numItemCode = I.numItemCode
		Left JOIN 
			Vendor V 
		ON 
			V.numVendorID=I.numVendorID 
			AND V.numItemCode=I.numItemCode
		WHERE
			OM.numDomainId=@numDomainID
			AND ISNULL(monTotAmount,0) <> 0
			AND ISNULL(numUnitHour,0) <> 0
			AND ISNULL(OM.tintOppType,0)=1
			AND ISNULL(OM.tintOppStatus,0)=1
			AND I.numItemCode NOT IN (@numShippingItemID,@numDiscountItemID)
	) TEMP
	GROUP BY
		numDivisionID
		,tintCRMType
		,vcCompanyName
	HAVING
		(SUM(Profit)/SUM(monTotAmount)) * 100 > 0
	ORDER BY
		(SUM(Profit)/SUM(monTotAmount)) * 100 DESC
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ReportListMaster_Top10Email')
DROP PROCEDURE USP_ReportListMaster_Top10Email
GO
CREATE PROCEDURE [dbo].[USP_ReportListMaster_Top10Email]
	@numDomainID NUMERIC(18,0)
	,@numUserCntId NUMERIC(18,0)
AS
BEGIN 
	DECLARE @Temp TABLE
	(
		numEmailHstrID NUMERIC(18,0)
		,vcCompanyName VARCHAR(300)
	)

	INSERT INTO @Temp
	(
		numEmailHstrID
		,vcCompanyName
	)
	SELECT TOP 10
		EH.numEmailHstrID
		,CI.vcCompanyName
	From 
		EmailHistory EH 
	INNER JOIN 
		EmailMaster EM 
	ON 
		EM.numEMailId=EH.numEmailId
	INNER JOIN
		AdditionalContactsInformation ACI
	ON 
		ACI.numContactId = EM.numContactId
	INNER JOIN
		DivisionMaster DM
	ON
		ACI.numDivisionId = DM.numDivisionID
	INNER JOIN
		CompanyInfo CI
	ON
		DM.numCompanyID = CI.numCompanyId
	WHERE
		EH.numDomainID = @numDomainID
		AND EH.numUserCntId = @numUserCntId
	ORDER BY 
		bintCreatedOn DESC

	SELECT 
		T1.*
		,CASE 
			WHEN LEN(Cast(EH.vcBodyText AS VARCHAR(1000)))>150 
			THEN SUBSTRING(EH.vcBodyText,0,150) + '...' 
			ELSE EH.vcBodyText END 
		AS vcBodyText
		,EH.vcSubject
		,REVERSE(SUBSTRING(REVERSE(EH.vcFrom),0,CHARINDEX('$^$',REVERSE(EH.vcFrom)))) vcFrom
	FROM
		EmailHistory EH
	INNER JOIN
		@Temp T1
	ON
		EH.numEmailHstrID = T1.numEmailHstrID
	WHERE
		EH.numDomainID = @numDomainID
		AND EH.numUserCntId = @numUserCntId
END
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ReportListMaster_Top10ItemByProfitMargin')
DROP PROCEDURE USP_ReportListMaster_Top10ItemByProfitMargin
GO
CREATE PROCEDURE [dbo].[USP_ReportListMaster_Top10ItemByProfitMargin]
	@numDomainID NUMERIC(18,0)
AS
BEGIN 
	DECLARE @numShippingItemID NUMERIC(18,0)
	DECLARE @numDiscountItemID NUMERIC(18,0)
	DECLARE @ProfitCost INT
	SELECT @numShippingItemID=numShippingServiceItemID,@numDiscountItemID=numDiscountServiceItemID,@ProfitCost=numCost FROM Domain WHERE numDomainId=@numDomainID


	SELECT TOP 10
		numItemCode
		,vcItemName
		,CONCAT('~/Items/frmKitDetails.aspx?ItemCode=',numItemCode,'&frm=All Items') AS URL
		,(SUM(Profit)/SUM(monTotAmount)) * 100 BlendedProfit
	FROM
	(
		SELECT
			I.numItemCode
			,I.vcItemName
			,ISNULL(monTotAmount,0) monTotAmount
			,ISNULL(monTotAmount,0) - (ISNULL(numUnitHour,0) * (CASE @ProfitCost WHEN 3 THEN ISNULL(V.monCost,0) * dbo.fn_UOMConversion(numBaseUnit,I.numItemCode,@numDomainID,numPurchaseUnit) WHEN 2 THEN numCost * dbo.fn_UOMConversion(numBaseUnit,I.numItemCode,@numDomainID,numUOMId) ELSE monAverageCost END)) Profit
		FROM
			OpportunityItems OI
		INNER JOIN
			OpportunityMaster OM
		ON
			OI.numOppId = OM.numOppID
		INNER JOIN
			DivisionMaster DM
		ON
			OM.numDivisionId = DM.numDivisionID
		INNER JOIN
			CompanyInfo CI
		ON
			DM.numCompanyID = CI.numCompanyId
		INNER JOIN
			Item I
		ON
			OI.numItemCode = I.numItemCode
		Left JOIN 
			Vendor V 
		ON 
			V.numVendorID=I.numVendorID 
			AND V.numItemCode=I.numItemCode
		WHERE
			OM.numDomainId=@numDomainID
			AND ISNULL(monTotAmount,0) <> 0
			AND ISNULL(numUnitHour,0) <> 0
			AND ISNULL(OM.tintOppType,0)=1
			AND ISNULL(OM.tintOppStatus,0)=1
			AND I.numItemCode NOT IN (@numShippingItemID,@numDiscountItemID)
	) TEMP
	GROUP BY
		numItemCode
		,vcItemName
	HAVING
		(SUM(Profit)/SUM(monTotAmount)) * 100 > 0
	ORDER BY
		(SUM(Profit)/SUM(monTotAmount)) * 100 DESC
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ReportListMaster_Top10ItemByProfitPreBuildReport')
DROP PROCEDURE USP_ReportListMaster_Top10ItemByProfitPreBuildReport
GO
CREATE PROCEDURE [dbo].[USP_ReportListMaster_Top10ItemByProfitPreBuildReport]
	@numDomainID NUMERIC(18,0)
AS
BEGIN 
	DECLARE @numShippingItemID NUMERIC(18,0)
	DECLARE @numDiscountItemID NUMERIC(18,0)
	DECLARE @ProfitCost INT
	SELECT @numShippingItemID=numShippingServiceItemID,@numDiscountItemID=numDiscountServiceItemID,@ProfitCost=numCost FROM Domain WHERE numDomainId=@numDomainID


	SELECT TOP 10
		numItemCode
		,vcItemName
		,SUM(Profit) Profit
	FROM
	(
		SELECT
			I.numItemCode
			,I.vcItemName
			,ISNULL(monTotAmount,0) - (ISNULL(numUnitHour,0) * (CASE @ProfitCost WHEN 3 THEN ISNULL(V.monCost,0) * dbo.fn_UOMConversion(numBaseUnit,I.numItemCode,@numDomainID,numPurchaseUnit) WHEN 2 THEN numCost * dbo.fn_UOMConversion(numBaseUnit,I.numItemCode,@numDomainID,numUOMId) ELSE monAverageCost END)) Profit
		FROM
			OpportunityItems OI
		INNER JOIN
			OpportunityMaster OM
		ON
			OI.numOppId = OM.numOppID
		INNER JOIN
			Item I
		ON
			OI.numItemCode = I.numItemCode
		Left JOIN 
			Vendor V 
		ON 
			V.numVendorID=I.numVendorID 
			AND V.numItemCode=I.numItemCode
		WHERE
			OM.numDomainId=@numDomainID
			AND ISNULL(OM.tintOppType,0)=1
			AND ISNULL(OM.tintOppStatus,0)=1
			AND OM.bintCreatedDate >= DATEADD(MONTH, -12, GETUTCDATE())
			AND I.numItemCode NOT IN (@numShippingItemID,@numDiscountItemID)
	) TEMP	
	GROUP BY
		numItemCode
		,vcItemName
	HAVING
		SUM(Profit) > 0
	ORDER BY
		SUM(Profit) DESC
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ReportListMaster_Top10ItemByRevenuePreBuildReport')
DROP PROCEDURE USP_ReportListMaster_Top10ItemByRevenuePreBuildReport
GO
CREATE PROCEDURE [dbo].[USP_ReportListMaster_Top10ItemByRevenuePreBuildReport]
	@numDomainID NUMERIC(18,0)
AS
BEGIN 
	SELECT TOP 10
		numItemCode
		,vcItemName
		,SUM(monTotAmount) TotalAmount
	FROM
	(
		SELECT
			I.numItemCode
			,I.vcItemName
			,ISNULL(monTotAmount,0) monTotAmount
		FROM
			OpportunityItems OI
		INNER JOIN
			OpportunityMaster OM
		ON
			OI.numOppId = OM.numOppID
		INNER JOIN
			Item I
		ON
			OI.numItemCode = I.numItemCode
		WHERE
			OM.numDomainId=@numDomainID
			AND ISNULL(OM.tintOppType,0)=1
			AND ISNULL(OM.tintOppStatus,0)=1
			AND OM.bintCreatedDate >= DATEADD(MONTH, -12, GETUTCDATE())
	) TEMP
	GROUP BY
		numItemCode
		,vcItemName
	HAVING
		SUM(monTotAmount) > 0
	ORDER BY
		SUM(monTotAmount) DESC
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ReportListMaster_Top10ItemsByReturnedVsSold')
DROP PROCEDURE USP_ReportListMaster_Top10ItemsByReturnedVsSold
GO
CREATE PROCEDURE [dbo].[USP_ReportListMaster_Top10ItemsByReturnedVsSold]
	@numDomainID NUMERIC(18,0)
AS
BEGIN 
	SELECT TOP 10
		I.vcItemName
		,CONCAT('~/Items/frmKitDetails.aspx?ItemCode=',I.numItemCode,'&frm=All Items') AS URL
		,SUM(RI.numUnitHour)
		,SUM(OI.numUnitHour)
		,(SUM(RI.numUnitHour) * 100)/ (CASE WHEN SUM(OI.numUnitHour) = 0 THEN 1.0 ELSE SUM(OI.numUnitHour) END) ReturnPercent
	FROM 
		OpportunityItems OI
	INNER JOIN
		OpportunityMaster OM
	ON
		OI.numOppId = OM.numOppId
	INNER JOIN
		Item I
	ON
		OI.numItemCode = I.numItemCode
	LEFT JOIN
		ReturnItems RI
	ON
		(RI.numOppItemID = OI.numoppitemtCode
		OR RI.numReturnItemID = I.numItemCode)
	LEFT JOIN
		ReturnHeader RH
	ON
		RI.numReturnHeaderID = RH.numReturnHeaderID
	WHERE
		OM.numDomainId=@numDomainID
		AND RH.numDomainId=@numDomainID
		AND RH.tintReturnType IN (1,3)
		AND ISNULL(tintOppType,0) = 1
		AND ISNULL(tintOppStatus,0) = 1
	GROUP BY
		I.numItemCode
		,I.vcItemName
	ORDER BY
		(SUM(RI.numUnitHour) * 100)/ (CASE WHEN SUM(OI.numUnitHour) = 0 THEN 1.0 ELSE SUM(OI.numUnitHour) END) DESC,I.numItemCode ASC
END
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ReportListMaster_Top10LeadSource')
DROP PROCEDURE USP_ReportListMaster_Top10LeadSource
GO
CREATE PROCEDURE [dbo].[USP_ReportListMaster_Top10LeadSource]
	@numDomainID NUMERIC(18,0)
AS
BEGIN 
	DECLARE @TEMP TABLE
	(
		numDivisionID NUMERIC(18,0)
		,RowID NUMERIC(18,0)
	)

	INSERT INTO @TEMP
	(
		numDivisionID
		,RowID
	)
	SELECT 
		numDivisionID
		,MIN(ID) 
	FROM 
		DivisionMasterPromotionHistory
	WHERE
		numDomainID = @numDomainID
	GROUP BY
		numDivisionID


	DECLARE @TotalLeads NUMERIC(18,2) 

	SELECT
		@TotalLeads = COUNT(DM.numDivisionID)
	FROM
		DivisionMaster DM
	INNER JOIN	
		CompanyInfo CI
	ON
		DM.numCompanyID = CI.numCompanyId
	LEFT JOIN
		ListDetails LD
	ON
		LD.numListID=18
		AND CI.vcHow = LD.numListItemID
		AND (LD.constFlag=1 OR LD.numDomainID=@numDomainID)
	LEFT JOIN
		@TEMP T1
	ON
		DM.numDivisionID = T1.numDivisionID
	LEFT JOIN
		DivisionMasterPromotionHistory DMPH
	ON
		T1.RowID = DMPH.ID
	WHERE
		DM.numDomainID = @numDomainID
		AND ((ISNULL(DM.tintCRMType,0) = 0 AND DMPH.ID IS NULL) OR ISNULL(DMPH.tintPreviousCRMType,0) = 0)

	SELECT TOP 10
		LD.vcData
		,CAST((COUNT(*) * 100) / @TotalLeads AS NUMERIC(18,2)) LeadSourcePercent
	FROM
		DivisionMaster DM
	INNER JOIN	
		CompanyInfo CI
	ON
		DM.numCompanyID = CI.numCompanyId
	LEFT JOIN
		ListDetails LD
	ON
		LD.numListID=18
		AND CI.vcHow = LD.numListItemID
		AND (LD.constFlag=1 OR LD.numDomainID=@numDomainID)
	LEFT JOIN
		@TEMP T1
	ON
		DM.numDivisionID = T1.numDivisionID
	LEFT JOIN
		DivisionMasterPromotionHistory DMPH
	ON
		T1.RowID = DMPH.ID
	WHERE
		DM.numDomainID = @numDomainID
		AND ((ISNULL(DM.tintCRMType,0) = 0 AND DMPH.ID IS NULL) OR ISNULL(DMPH.tintPreviousCRMType,0) = 0)
		AND LD.numListItemID IS NOT NULL
	GROUP BY
		LD.numListItemID
		,LD.vcData
	ORDER BY
		(COUNT(*) * 100) / @TotalLeads DESC
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ReportListMaster_Top10SalesOpportunityByPastDue')
DROP PROCEDURE USP_ReportListMaster_Top10SalesOpportunityByPastDue
GO
CREATE PROCEDURE [dbo].[USP_ReportListMaster_Top10SalesOpportunityByPastDue]
	@numDomainID NUMERIC(18,0)
	,@ClientTimeZoneOffset INT
AS
BEGIN 
	SELECT TOP 10
		OM.vcPOppName
		,CONCAT('~/opportunity/frmOpportunities.aspx?opId=',OM.numOppId) AS URL
		,ISNULL(monTotAmount,0) monTotAmount
		,ISNULL(OM.numPercentageComplete,0) numPercentageComplete
	FROM
		OpportunityItems OI
	INNER JOIN
		OpportunityMaster OM
	ON
		OI.numOppId = OM.numOppID
	LEFT JOIN
		ListDetails LD
	ON
		LD.numListID=50 
		AND (ISNULL(LD.constFlag,0)=1 OR LD.numDomainID=@numDomainID)
		AND LD.numListItemID = OM.numPercentageComplete
	WHERE
		OM.numDomainId=@numDomainID
		AND ISNULL(OM.tintOppType,0)=1
		AND ISNULL(OM.tintOppStatus,0)=0
		AND OM.intPEstimatedCloseDate IS NOT NULL
		AND OM.intPEstimatedCloseDate < DateAdd(minute, -@ClientTimeZoneOffset,GETUTCDATE())
	ORDER BY
		ISNULL(monTotAmount,0) DESC
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ReportListMaster_Top10SalesOpportunityByRevenue')
DROP PROCEDURE USP_ReportListMaster_Top10SalesOpportunityByRevenue
GO
CREATE PROCEDURE [dbo].[USP_ReportListMaster_Top10SalesOpportunityByRevenue]
	@numDomainID NUMERIC(18,0)
AS
BEGIN 
	SELECT TOP 10
		OM.vcPOppName
		,CONCAT('~/opportunity/frmOpportunities.aspx?opId=',OM.numOppId) AS URL
		,ISNULL(monTotAmount,0) monTotAmount
		,ISNULL(OM.numPercentageComplete,0) numPercentageComplete
	FROM
		OpportunityItems OI
	INNER JOIN
		OpportunityMaster OM
	ON
		OI.numOppId = OM.numOppID
	LEFT JOIN
		ListDetails LD
	ON
		LD.numListID=50 
		AND (ISNULL(LD.constFlag,0)=1 OR LD.numDomainID=@numDomainID)
		AND LD.numListItemID = OM.numPercentageComplete
	WHERE
		OM.numDomainId=@numDomainID
		AND ISNULL(OM.tintOppType,0)=1
		AND ISNULL(OM.tintOppStatus,0)=0
		AND OM.bintCreatedDate >= DATEADD(MONTH, -12, GETUTCDATE())
	ORDER BY
		ISNULL(monTotAmount,0) DESC
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ReportListMaster_Top10SalesOpportunityByTotalProgress')
DROP PROCEDURE USP_ReportListMaster_Top10SalesOpportunityByTotalProgress
GO
CREATE PROCEDURE [dbo].[USP_ReportListMaster_Top10SalesOpportunityByTotalProgress]
	@numDomainID NUMERIC(18,0)
AS
BEGIN 
	SELECT TOP 10
		OM.vcPOppName
		,CONCAT('~/opportunity/frmOpportunities.aspx?opId=',OM.numOppId) AS URL
		,ISNULL(monTotAmount,0) monTotAmount
		,ISNULL(OM.numPercentageComplete,0) numPercentageComplete
	FROM
		OpportunityItems OI
	INNER JOIN
		OpportunityMaster OM
	ON
		OI.numOppId = OM.numOppID
	LEFT JOIN
		ListDetails LD
	ON
		LD.numListID=50 
		AND (ISNULL(LD.constFlag,0)=1 OR LD.numDomainID=@numDomainID)
		AND LD.numListItemID = OM.numPercentageComplete
	WHERE
		OM.numDomainId=@numDomainID
		AND ISNULL(OM.tintOppType,0)=1
		AND ISNULL(OM.tintOppStatus,0)=0
	ORDER BY
		ISNULL(OM.numPercentageComplete,0) DESC
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ReportListMaster_TopCustomersByPortionOfTotalSales')
DROP PROCEDURE USP_ReportListMaster_TopCustomersByPortionOfTotalSales
GO
CREATE PROCEDURE [dbo].[USP_ReportListMaster_TopCustomersByPortionOfTotalSales]
	@numDomainID NUMERIC(18,0)
AS
BEGIN 
	DECLARE @TotalSalesOrderAmount AS MONEY


	SELECT DISTINCT
		@TotalSalesOrderAmount = SUM(monDealAmount)
	FROM 
		OpportunityMaster
	WHERE 
		numDomainId=@numDomainID 
		AND tintOppType=1 
		AND tintOppStatus=1 

	SELECT TOP 10 
		DM.numDivisionID
		,vcCompanyName
		,(SUM(monDealAmount) * 100)/ (CASE WHEN @TotalSalesOrderAmount = 0 THEN 1 ELSE @TotalSalesOrderAmount END) AS TotalSalesPercent
	FROM
		OpportunityMaster OM
	INNER JOIN
		DivisionMaster DM
	ON
		OM.numDivisionId = DM.numDivisionID
	INNER JOIN
		CompanyInfo CI
	ON
		DM.numCompanyID = CI.numCompanyId
	WHERE
		OM.numDomainId = @numDomainID
		AND DM.numDomainID = @numDomainID
		AND CI.numDomainID=@numDomainID
		AND tintOppType=1 
		AND tintOppStatus=1
	GROUP BY
		DM.numDivisionID
		,vcCompanyName
	HAVING
		SUM(monDealAmount) > 0
	ORDER BY 
		SUM(monDealAmount) DESC
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ReportListMaster_TopSourceOfSalesOrder')
DROP PROCEDURE USP_ReportListMaster_TopSourceOfSalesOrder
GO
CREATE PROCEDURE [dbo].[USP_ReportListMaster_TopSourceOfSalesOrder]
	@numDomainID NUMERIC(18,0)
AS
BEGIN 
	DECLARE @TotalSalesOrderCount AS NUMERIC(18,4)

	SELECT DISTINCT
		@TotalSalesOrderCount = COUNT(*)
	FROM 
		OpportunityMaster
	WHERE 
		numDomainId=@numDomainID 
		AND tintOppType=1 
		AND tintOppStatus=1 
		AND bintCreatedDate >= DATEADD(MONTH, -12, GETUTCDATE())

	DECLARE @TEMP TABLE
	(
		ID INT IDENTITY(1,1)
		,tintSource VARCHAR(100)
		,bitPartner BIT
		,numPartner NUMERIC(18,0)
		,vcSource VARCHAR(300)
		,TotalOrdersCount INT
		,TotalOrdersPercent NUMERIC(18,2)
		,TotalOrdersAmount MONEY
	)

	INSERT INTO @TEMP
	(
		tintSource
		,vcSource
		,bitPartner
		,numPartner
	)
	SELECT
		'0~0'
		,'-'
		,0
		,0
	UNION
	SELECT 
		'0~1'
		,'Internal Order'
		,0
		,0
	UNION
	SELECT 
		CONCAT(numSiteID,'~2')
		,vcSiteName
		,0
		,0
	FROM 
		Sites 
	WHERE 
		numDomainID = @numDomainID
	UNION
	SELECT DISTINCT 
		CONCAT(WebApiId,'~3')
		,vcProviderName
		,0
		,0
	FROM 
		dbo.WebAPI
	UNION
	SELECT 
		CONCAT(Ld.numListItemID,'~1')
		,ISNULL(vcRenamedListName,vcData)
		,0
		,0 
	FROM 
		ListDetails Ld  
	LEFT JOIN 
		listorder LO 
	ON 
		Ld.numListItemID= LO.numListItemID 
		AND Lo.numDomainId = @numDomainID 
	WHERE 
		Ld.numListID=9 
		AND (constFlag=1 OR Ld.numDomainID=@numDomainID)  
	UNION
	SELECT DISTINCT
		''
		,''
		,1
		,numPartner
	FROM 
		OpportunityMaster
	WHERE 
		numDomainId=@numDomainID 
		AND tintOppType=1 
		AND tintOppStatus=1
		AND ISNULL(numPartner,0) > 0
		AND bintCreatedDate >= DATEADD(MONTH, -12, GETUTCDATE())

	DECLARE @i AS INT = 1
	DECLARE @iCount AS INT
	DECLARE @tintSource AS VARCHAR(100)
	DECLARE @bitPartner BIT
	DECLARE @numPartner NUMERIC(18,0)
	DECLARE @TotalSalesOrderBySource AS INT
	DECLARE @TotalSalesOrderAmountBySource AS MONEY


	SELECT @iCount = COUNT(*) FROM @TEMP

	WHILE @i <= @iCount
	BEGIN
		SELECT @tintSource=ISNULL(tintSource,''),@bitPartner=bitPartner,@numPartner=numPartner FROM @TEMP WHERE ID=@i

		SELECT 
			@TotalSalesOrderBySource = COUNT(numOppId)
		FROM 
			OpportunityMaster
		WHERE 
			numDomainId=@numDomainID 
			AND tintOppType=1
			AND tintOppStatus=1
			AND 1 = (CASE 
						WHEN @bitPartner=1 
						THEN CASE WHEN numPartner=@numPartner THEN 1 ELSE 0 END
						ELSE CASE WHEN CONCAT(ISNULL(tintSource,0),'~',ISNULL(tintSourceType,0))=@tintSource AND ISNULL(numPartner,0)=0 THEN 1 ELSE 0 END
					END)
			AND bintCreatedDate >= DATEADD(MONTH, -12, GETUTCDATE())

		SELECT 
			@TotalSalesOrderAmountBySource = SUM(monTotAmount)
		FROM
			OpportunityItems OI
		INNER JOIN
			OpportunityMaster OM
		ON
			OI.numOppId = OM.numOppID
		INNER JOIN
			Item I
		ON
			OI.numItemCode = I.numItemCode
		WHERE 
			OM.numDomainId=@numDomainID 
			AND I.numDomainID = @numDomainID
			AND tintOppType=1 
			AND tintOppStatus=1
			AND 1 = (CASE 
						WHEN @bitPartner=1 
						THEN CASE WHEN numPartner=@numPartner THEN 1 ELSE 0 END
						ELSE CASE WHEN CONCAT(ISNULL(tintSource,0),'~',ISNULL(tintSourceType,0))=@tintSource AND ISNULL(numPartner,0)=0 THEN 1 ELSE 0 END
					END)
			AND OM.bintCreatedDate >= DATEADD(MONTH, -12, GETUTCDATE())

		UPDATE
			@TEMP
		SET
			vcSource = (CASE 
						WHEN @bitPartner=1 
						THEN ISNULL((SELECT CI.vcCompanyName FROM DivisionMaster DM INNER JOIN CompanyInfo CI ON DM.numCompanyID=CI.numCompanyId WHERE DM.numDomainID=@numDomainID AND numDivisionID=@numPartner),'-')
						ELSE vcSource
					END)
			,TotalOrdersPercent = (@TotalSalesOrderBySource * 100)/ISNULL(@TotalSalesOrderCount,1)
			,TotalOrdersAmount = ISNULL(@TotalSalesOrderAmountBySource,0)
			,TotalOrdersCount = @TotalSalesOrderBySource
		WHERE
			ID=@i

		SET @i = @i + 1
	END

	SELECT vcSource,TotalOrdersPercent,TotalOrdersAmount FROM @TEMP WHERE ISNULL(TotalOrdersPercent,0) > 0 ORDER BY TotalOrdersPercent DESC
END
GO
/****** Object:  StoredProcedure [dbo].[USP_RevertDetailsOpp]    Script Date: 07/26/2008 16:20:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj      
/* deducts Qty from allocation and Adds back to onhand */
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_RevertDetailsOpp')
DROP PROCEDURE USP_RevertDetailsOpp
GO
CREATE PROCEDURE [dbo].[USP_RevertDetailsOpp] 
@numOppId NUMERIC,
@tintMode AS TINYINT=0, -- 0:Add/Edit 1:Delete
@numUserCntID AS NUMERIC(9)
AS ---reverting back to previous state if deal is being edited 
	DECLARE @numDomain AS NUMERIC(18,0)
	SELECT @numDomain = numDomainID FROM [dbo].[OpportunityMaster] AS OM WHERE [OM].[numOppId] = @numOppID
    
	DECLARE @OppType AS VARCHAR(2)              
    DECLARE @itemcode AS NUMERIC       
    DECLARE @numWareHouseItemID AS NUMERIC
	DECLARE @numWLocationID NUMERIC(18,0)                     
    DECLARE @numToWarehouseItemID AS NUMERIC 
    DECLARE @numUnits AS FLOAT                                              
    DECLARE @onHand AS FLOAT                                            
    DECLARE @onOrder AS FLOAT                                            
    DECLARE @onBackOrder AS FLOAT                                              
    DECLARE @onAllocation AS FLOAT
    DECLARE @numQtyShipped AS FLOAT
    DECLARE @numUnitHourReceived AS FLOAT
	DECLARE @numDeletedReceievedQty AS FLOAT
    DECLARE @numoppitemtCode AS NUMERIC(9) 
    DECLARE @monAmount AS MONEY 
    DECLARE @monAvgCost AS MONEY   
    DECLARE @Kit AS BIT                                        
    DECLARE @bitKitParent BIT
    DECLARE @bitStockTransfer BIT 
    DECLARE @numOrigUnits AS FLOAT			
    DECLARE @description AS VARCHAR(100)
	DECLARE @bitWorkOrder AS BIT
	--Added by :Sachin Sadhu||Date:18thSept2014
	--For Rental/Asset Project
	Declare @numRentalIN as FLOAT
	Declare @numRentalOut as FLOAT
	Declare @numRentalLost as FLOAT
	DECLARE @bitAsset as BIT
	--end sachin

    						
    SELECT TOP 1
            @numoppitemtCode = numoppitemtCode,
            @itemcode = OI.numItemCode,
            @numUnits = ISNULL(numUnitHour,0),
            @numWareHouseItemID = ISNULL(numWarehouseItmsID,0),
			@numWLocationID = ISNULL(numWLocationID,0),
            @Kit = ( CASE WHEN bitKitParent = 1
                               AND bitAssembly = 1 THEN 0
                          WHEN bitKitParent = 1 THEN 1
                          ELSE 0
                     END ),
            @monAmount = ISNULL(monTotAmount,0) * (CASE WHEN ISNULL(fltExchangeRate,0)=0 THEN 1 ELSE fltExchangeRate END),
            @monAvgCost = ISNULL(monAverageCost,0),
            @numQtyShipped = ISNULL(numQtyShipped,0),
            @numUnitHourReceived = ISNULL(numUnitHourReceived,0),
			@numDeletedReceievedQty = ISNULL(numDeletedReceievedQty,0),
            @bitKitParent=ISNULL(bitKitParent,0),
            @numToWarehouseItemID =OI.numToWarehouseItemID,
            @bitStockTransfer = ISNULL(OM.bitStockTransfer,0),
            @OppType = tintOppType,
		    @numRentalIN=ISNULL(oi.numRentalIN,0),
			@numRentalOut=Isnull(oi.numRentalOut,0),
			@numRentalLost=Isnull(oi.numRentalLost,0),
			@bitAsset =ISNULL(I.bitAsset,0),
			@bitWorkOrder = ISNULL(OI.bitWorkOrder,0)
    FROM    OpportunityItems OI
			LEFT JOIN WareHouseItems WI ON OI.numWarehouseItmsID=WI.numWareHouseItemID
			JOIN dbo.OpportunityMaster OM ON OI.numOppId = OM.numOppId
            JOIN Item I ON OI.numItemCode = I.numItemCode
    WHERE   (charitemtype='P' OR 1=(CASE WHEN tintOppType=1 THEN 
							CASE WHEN ISNULL(I.bitKitParent,0) = 1 AND ISNULL(I.bitAssembly,0) = 1 THEN 0
								 WHEN ISNULL(I.bitKitParent,0) = 1 THEN 1
								 ELSE 0 END 
							ELSE 0 END)) AND OI.numOppId = @numOppId
                           AND ( bitDropShip = 0
                                 OR bitDropShip IS NULL
                               ) 
    ORDER BY OI.numoppitemtCode

	
    WHILE @numoppitemtCode > 0                                  
    BEGIN    
        SET @numOrigUnits=@numUnits
            
        IF @bitStockTransfer=1
        BEGIN
			SET @OppType = 1
		END
                 
        IF @numWareHouseItemID>0
        BEGIN                                
			SELECT  @onHand = ISNULL(numOnHand, 0),
					@onAllocation = ISNULL(numAllocation, 0),
					@onOrder = ISNULL(numOnOrder, 0),
					@onBackOrder = ISNULL(numBackOrder, 0)
			FROM    WareHouseItems
			WHERE   numWareHouseItemID = @numWareHouseItemID                                               
        END
            
        IF @OppType = 1 
        BEGIN
			SET @description='SO Deleted (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@numQtyShipped AS VARCHAR(10)) + ')'
                
            IF @Kit = 1
			BEGIN
				exec USP_UpdatingInventoryForEmbeddedKits @numoppitemtCode,@numOrigUnits,1,@numOppID,@tintMode,@numUserCntID
			END
			ELSE IF @bitWorkOrder = 1
			BEGIN
					
				IF @tintMode=0 -- EDIT
				BEGIN
					SET @description='SO-WO Edited (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@numQtyShipped AS VARCHAR(10)) + ')'
					-- UPDATE WORK ORDER - REVERT INVENTOY OF ASSEMBLY ITEMS
					EXEC USP_ManageInventoryAssemblyWO @numUserCntID,@numOppID,@itemcode,@numWareHouseItemID,@numQtyShipped,5
				END
				ELSE IF @tintMode=1 -- DELETE
				BEGIN
					DECLARE @numWOID AS NUMERIC(18,0)
					DECLARE @numWOStatus AS NUMERIC(18,0)
					SELECT @numWOID=numWOId,@numWOStatus=numWOStatus FROM WorkOrder WHERE numOppId=@numOppId AND numItemCode=@itemcode AND numWareHouseItemId=@numWareHouseItemID

					SET @description='SO-WO Deleted (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@numQtyShipped AS VARCHAR(10)) + ')'

					--IF WORK ORDER IS NOT COMPLETED THAN REMOVE ON ORDER QUANTITY
					IF @numWOStatus <> 23184
					BEGIN
						IF @onOrder >= @numUnits
							SET @onOrder = @onOrder - @numUnits
						ELSE
							SET @onOrder = 0
					END

					-- IF BACKORDER QTY IS GREATER THEN QTY TO REVERT THEN
					-- DECREASE BACKORDER QTY BY QTY TO REVERT
					IF @numUnits < @onBackOrder 
					BEGIN                  
						SET @onBackOrder = @onBackOrder - @numUnits
					END 
					-- IF BACKORDER QTY IS LESS THEN OR EQUAL TO QTY TO REVERT THEN
					-- DECREASE QTY TO REVERT BY QTY OF BACK ORDER AND
					-- SET BACKORDER QTY TO 0
					ELSE IF @numUnits >= @onBackOrder 
					BEGIN
						SET @numUnits = @numUnits - @onBackOrder
						SET @onBackOrder = 0
                        
						--REMOVE ITEM FROM ALLOCATION 
						IF (@onAllocation - @numUnits) >= 0
							SET @onAllocation = @onAllocation - @numUnits
						
						--ADD QTY TO ONHAND
						SET @onHand = @onHand + @numUnits
					END

					UPDATE  
						WareHouseItems
					SET 
						numOnHand = @onHand ,
						numAllocation = @onAllocation,
						numBackOrder = @onBackOrder,
						numOnOrder = @onOrder,
						dtModified = GETDATE()
					WHERE 
						numWareHouseItemID = @numWareHouseItemID   

					--IF WORK ORDER IS NOT COMPLETED THAN DELETE WORK ORDER AND ITS CHILDS WORK ORDER
					IF @numWOStatus <> 23184
					BEGIN
						EXEC USP_DeleteAssemblyWOAndInventory  @numDomain,@numUserCntID,@numWOID,@numOppID
					END
				END
			END	
			ELSE
			BEGIN	
				IF @numQtyShipped>0
				BEGIN
					IF @tintMode=0
						SET @numUnits = @numUnits - @numQtyShipped
					ELSE IF @tintmode=1
						SET @onAllocation = @onAllocation + @numQtyShipped 
				END 
								                    
                IF @numUnits >= @onBackOrder 
                BEGIN
                    SET @numUnits = @numUnits - @onBackOrder
                    SET @onBackOrder = 0
                            
                    IF (@onAllocation - @numUnits >= 0)
						SET @onAllocation = @onAllocation - @numUnits

					IF @bitAsset=1--Not Asset
					BEGIN
						SET @onHand = @onHand +@numRentalIN+@numRentalLost+@numRentalOut     
					END
					ELSE
					BEGIN
						SET @onHand = @onHand + @numUnits     
					END                                         
                END                                            
                ELSE IF @numUnits < @onBackOrder 
                BEGIN                  
					IF (@onBackOrder - @numUnits >0)
						SET @onBackOrder = @onBackOrder - @numUnits
                END 
                 	
				UPDATE  
					WareHouseItems
				SET 
					numOnHand = @onHand ,
					numAllocation = @onAllocation,
					numBackOrder = @onBackOrder,
					dtModified = GETDATE()
				WHERE 
					numWareHouseItemID = @numWareHouseItemID              
			END
				
			IF @numWareHouseItemID>0
			BEGIN 
					EXEC dbo.USP_ManageWareHouseItems_Tracking
					@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
					@numReferenceID = @numOppId, --  numeric(9, 0)
					@tintRefType = 3, --  tinyint
					@vcDescription = @description, --  varchar(100)
					@numModifiedBy = @numUserCntID,
					@numDomainID = @numDomain 
			END		
		END      
          
        IF @bitStockTransfer=1
        BEGIN
			SET @numWareHouseItemID = @numToWarehouseItemID;
			SET @OppType = 2
			SET @numUnits = @numOrigUnits

			SELECT
				@onHand = ISNULL(numOnHand, 0),
                @onAllocation = ISNULL(numAllocation, 0),
                @onOrder = ISNULL(numOnOrder, 0),
                @onBackOrder = ISNULL(numBackOrder, 0)
			FROM 
				WareHouseItems
			WHERE
				numWareHouseItemID = @numWareHouseItemID   
		END
          
        IF @OppType = 2 
        BEGIN 
			SET @description='PO Deleted (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Received:' +  CAST(@numUnitHourReceived AS VARCHAR(10)) + ' Deleted:' + CAST(@numDeletedReceievedQty AS VARCHAR(10)) + ')'

			IF @numWLocationID = -1 AND ISNULL(@bitStockTransfer,0)=0
			BEGIN
				IF @tintmode = 1
				BEGIN
					DECLARE @TEMPReceievedItems TABLE
					(
						ID INT,
						numOIRLID NUMERIC(18,0),
						numWarehouseItemID NUMERIC(18,0),
						numUnitReceieved FLOAT,
						numDeletedReceievedQty FLOAT
					)

					DELETE FROM @TEMPReceievedItems

					INSERT INTO
						@TEMPReceievedItems
					SELECT
						ROW_NUMBER() OVER(ORDER BY ID),
						ID,
						numWarehouseItemID,
						numUnitReceieved,
						ISNULL(numDeletedReceievedQty,0)
					FROM
						OpportunityItemsReceievedLocation 
					WHERE
						numDomainID=@numDomain
						AND numOppID=@numOppId
						AND numOppItemID=@numoppitemtCode

					DECLARE @i AS INT = 1
					DECLARE @COUNT AS INT
					DECLARE @numTempOIRLID NUMERIC(18,0)
					DECLARE @numTempOnHand FLOAT
					DECLARE @numTempWarehouseItemID NUMERIC(18,0)
					DECLARE @numTempUnitReceieved FLOAT
					DECLARE @numTempDeletedReceievedQty FLOAT

					SELECT @COUNT=COUNT(*) FROM @TEMPReceievedItems

					WHILE @i <= @COUNT
					BEGIN
						SELECT 
							@numTempOIRLID=TRI.numOIRLID,
							@numTempOnHand= ISNULL(numOnHand,0),
							@numTempWarehouseItemID=ISNULL(TRI.numWarehouseItemID,0),
							@numTempUnitReceieved=ISNULL(numUnitReceieved,0),
							@numTempDeletedReceievedQty=ISNULL(numDeletedReceievedQty,0)
						FROM 
							@TEMPReceievedItems TRI
						INNER JOIN
							WareHouseItems WI
						ON
							TRI.numWarehouseItemID=WI.numWareHouseItemID
						WHERE 
							ID=@i

						IF @numTempOnHand >= (@numTempUnitReceieved - @numTempDeletedReceievedQty)
						BEGIN
							UPDATE
								WareHouseItems
							SET
								numOnHand = ISNULL(numOnHand,0) - (@numTempUnitReceieved - @numTempDeletedReceievedQty),
								dtModified = GETDATE()
							WHERE
								numWareHouseItemID=@numTempWarehouseItemID
						END
						ELSE
						BEGIN
							RAISERROR('INSUFFICIENT_ONHAND_QTY',16,1)
							RETURN
						END

						SET @description='PO Deleted (Total Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Deleted Qty: ' + CAST((@numTempUnitReceieved - @numTempDeletedReceievedQty) AS VARCHAR(10)) + ' Received:' +  CAST(@numUnitHourReceived AS VARCHAR(10)) + ')'

						EXEC dbo.USP_ManageWareHouseItems_Tracking
							@numWareHouseItemID = @numTempWarehouseItemID,
							@numReferenceID = @numOppId,
							@tintRefType = 4,
							@vcDescription = @description,
							@numModifiedBy = @numUserCntID,
							@numDomainID = @numDomain

						DELETE FROM OpportunityItemsReceievedLocation WHERE ID=@numTempOIRLID

						SET @i = @i + 1
					END
				END

				SET @numUnits = @numUnits - @numUnitHourReceived

				IF (@onOrder - @numUnits)>=0
				BEGIN
					UPDATE
						WareHouseItems
					SET 
						numOnOrder = ISNULL(numOnOrder,0) - @numUnits,
						dtModified = GETDATE()
					WHERE 
						numWareHouseItemID = @numWareHouseItemID

					EXEC dbo.USP_ManageWareHouseItems_Tracking
						@numWareHouseItemID = @numWareHouseItemID,
						@numReferenceID = @numOppId,
						@tintRefType = 4,
						@vcDescription = @description,
						@numModifiedBy = @numUserCntID,
						@numDomainID = @numDomain
				END
				ELSE
				BEGIN
					RAISERROR('INSUFFICIENT_ONORDER_QTY',16,1)
					RETURN
				END
			END
			ELSE
			BEGIN
				--Partial Fulfillment
				IF @tintmode=1 and  @onHand >= (@numUnitHourReceived - @numDeletedReceievedQty)
				BEGIN
					SET @onHand= @onHand - (@numUnitHourReceived - @numDeletedReceievedQty)
				END
						
				SET @numUnits = @numUnits - @numUnitHourReceived

				IF (@onOrder - @numUnits)>=0
				BEGIN
					--Causing Negative Inventory Bug ID:494
					SET @onOrder = @onOrder - @numUnits	
				END
				ELSE IF (@onHand + @onOrder) - @numUnits >= 0
				BEGIN						
					SET @onHand = @onHand - (@numUnits-@onOrder)
					SET @onOrder = 0
				END
				ELSE IF  (@onHand + @onOrder + @onAllocation) - @numUnits >= 0
				BEGIN
					Declare @numDiff numeric
	
					SET @numDiff = @numUnits - @onOrder
					SET @onOrder = 0

					SET @numDiff = @numDiff - @onHand
					SET @onHand = 0

					SET @onAllocation = @onAllocation - @numDiff
					SET @onBackOrder = @onBackOrder + @numDiff
				END
					    
				UPDATE
					WareHouseItems
				SET 
					numOnHand = @onHand,
					numAllocation = @onAllocation,
					numBackOrder = @onBackOrder,
					numOnOrder = @onOrder,
					dtModified = GETDATE()
				WHERE 
					numWareHouseItemID = @numWareHouseItemID   
                        
						

				EXEC dbo.USP_ManageWareHouseItems_Tracking
				@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
				@numReferenceID = @numOppId, --  numeric(9, 0)
				@tintRefType = 4, --  tinyint
				@vcDescription = @description, --  varchar(100)
				@numModifiedBy = @numUserCntID,
				@numDomainID = @numDomain
			END                                        
        END   
                                                                
        SELECT TOP 1
                @numoppitemtCode = numoppitemtCode,
                @itemcode = OI.numItemCode,
                @numUnits = numUnitHour,
                @numWareHouseItemID = ISNULL(numWarehouseItmsID,0),
				@numWLocationID = ISNULL(numWLocationID,0),
                @Kit = ( CASE WHEN bitKitParent = 1
                                    AND bitAssembly = 1 THEN 0
                                WHEN bitKitParent = 1 THEN 1
                                ELSE 0
                            END ),
                @monAmount = ISNULL(monTotAmount,0)  * (CASE WHEN ISNULL(fltExchangeRate,0)=0 THEN 1 ELSE fltExchangeRate END),
                @monAvgCost = monAverageCost,
                @numQtyShipped = ISNULL(numQtyShipped,0),
				@numUnitHourReceived = ISNULL(numUnitHourReceived,0),
				@numDeletedReceievedQty = ISNULL(numDeletedReceievedQty,0),
				@bitKitParent=ISNULL(bitKitParent,0),
				@numToWarehouseItemID =OI.numToWarehouseItemID,
				@bitStockTransfer = ISNULL(OM.bitStockTransfer,0),
				@OppType = tintOppType
        FROM    OpportunityItems OI
				LEFT JOIN WareHouseItems WI ON OI.numWarehouseItmsID=WI.numWareHouseItemID
				JOIN dbo.OpportunityMaster OM ON OI.numOppId = OM.numOppId
                JOIN Item I ON OI.numItemCode = I.numItemCode
                                   
        WHERE   (charitemtype='P' OR 1=(CASE WHEN tintOppType=1 THEN 
						CASE WHEN ISNULL(I.bitKitParent,0) = 1 AND ISNULL(I.bitAssembly,0) = 1 THEN 0
								WHEN ISNULL(I.bitKitParent,0) = 1 THEN 1
								ELSE 0 END 
						ELSE 0 END))  
						AND OI.numOppId = @numOppId 
                AND OI.numoppitemtCode > @numoppitemtCode
                AND ( bitDropShip = 0
                        OR bitDropShip IS NULL
                    )
        ORDER BY OI.numoppitemtCode                                              
        IF @@rowcount = 0 
            SET @numoppitemtCode = 0      
    END

/****** Object:  StoredProcedure [dbo].[USP_ShoppingCart]    Script Date: 05/07/2009 22:09:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_shoppingcart')
DROP PROCEDURE usp_shoppingcart
GO
CREATE PROCEDURE [dbo].[USP_ShoppingCart]                            
@numDivID as numeric(18),                            
@numOppID as numeric(18)=null output ,                                              
@vcPOppName as varchar(200)='' output,  
@numContactId as numeric(18),                            
@numDomainId as numeric(18),
@vcSource VARCHAR(50)=NULL,
@numCampainID NUMERIC(9)=0,
@numCurrencyID as numeric(9),
@numBillAddressId numeric(9),
@numShipAddressId numeric(9),
@bitDiscountType BIT,
@fltDiscount FLOAT,
@numProId NUMERIC(9),
@numSiteID NUMERIC(9),
@tintOppStatus INT,
@monShipCost MONEY,
@tintSource AS BIGINT ,
@tintSourceType AS TINYINT ,                          
@numPaymentMethodId NUMERIC(9),
@txtComments varchar(1000),
@numPartner NUMERIC(18,0),
@txtFuturePassword VARCHAR(50) = NULL,
@intUsedShippingCompany NUMERIC(18,0)=0,
@vcPartenerContact VARCHAR(200)='',
@numShipmentMethod NUMERIC(18,0)=0,
@vcOppRefOrderNo VARCHAR(200) = '',
@bitBillingTerms as bit,
@intBillingDays as integer,
@bitInterestType as bit,
@fltInterest as float
as                            
BEGIN TRY
BEGIN TRANSACTION          
declare @CRMType as integer               
declare @numRecOwner as integer       
DECLARE @numPartenerContact NUMERIC(18,0)=0
	
SET @numPartenerContact=(SELECT Top 1 numContactId FROM AdditionalContactsInformation WHERE numDomainId=@numDomainID AND numDivisionId=@numPartner AND vcEmail=@vcPartenerContact)
	
if @numOppID=  0                             
begin                            
select @CRMType=tintCRMType,@numRecOwner=numRecOwner from DivisionMaster where numDivisionID=@numDivID                            
if @CRMType= 0                             
begin                            
UPDATE DivisionMaster                               
   SET tintCRMType=1                              
   WHERE numDivisionID=@numDivID                              
		
end                            
declare @TotalAmount as FLOAT
--declare @intOppTcode as numeric(9) 
--declare @numCurrencyID as numeric(9)
--declare @fltExchangeRate as float                          
--Select @intOppTcode=max(numOppId)from OpportunityMaster                              
--set @intOppTcode =isnull(@intOppTcode,0) + 1        

		IF LEN(ISNULL(@txtFuturePassword,'')) > 0
		BEGIN
			DECLARE @numExtranetID NUMERIC(18,0)

			SELECT @numExtranetID=numExtranetID FROM ExtarnetAccounts WHERE numDivisionID=@numDivID

			UPDATE ExtranetAccountsDtl SET vcPassword=@txtFuturePassword WHERE numExtranetID=@numExtranetID
		END                      

		DECLARE @numTemplateID NUMERIC
		SELECT  @numTemplateID= numMirrorBizDocTemplateId FROM eCommercePaymentConfig WHERE  numSiteId=@numSiteID AND numPaymentMethodId = @numPaymentMethodId AND numDomainID = @numDomainID   

		Declare @fltExchangeRate float                                 
    
   IF @numCurrencyID=0 select @numCurrencyID=isnull(numCurrencyID,0) from Domain where numDomainID=@numDomainId                            
   if @numCurrencyID=0 set   @fltExchangeRate=1
  else set @fltExchangeRate=dbo.GetExchangeRate(@numDomainID,@numCurrencyID)                          
			                       
                            
		-- GET ACCOUNT CLASS IF ENABLED
		DECLARE @numAccountClass AS NUMERIC(18) = 0
	-- GET ACCOUNT CLASS IF ENABLED
		DECLARE @tintDefaultClassType AS INT = 0
		SELECT @tintDefaultClassType = ISNULL(tintDefaultClassType,0) FROM Domain WHERE numDomainId = @numDomainID 

	IF @tintDefaultClassType <> 0
		BEGIN
			SELECT 
				@numAccountClass=ISNULL(numDefaultClass,0) 
			FROM 
				dbo.UserMaster UM 
			JOIN 
				dbo.Domain D 
			ON 
				UM.numDomainID=D.numDomainId
			WHERE 
				D.numDomainId=@numDomainId 
				AND UM.numUserDetailId=@numContactId
		END
		ELSE IF @tintDefaultClassType = 2 --COMPANY
		BEGIN
			SELECT 
				@numAccountClass=ISNULL(numAccountClassID,0) 
			FROM 
				dbo.DivisionMaster DM 
			WHERE 
				DM.numDomainId=@numDomainId 
				AND DM.numDivisionID=@numDivID
		END
		ELSE
		BEGIN
			SET @numAccountClass = 0
		END
		SET @numAccountClass=(SELECT 
								  TOP 1 
								  numDefaultClass from dbo.eCommerceDTL 
								  where 
								  numSiteId=@numSiteID 
								  and numDomainId=@numDomainId)                  
--set @vcPOppName=@vcPOppName+ '-'+convert(varchar(4),year(getutcdate()))  +'-A'+ convert(varchar(10),@intOppTcode)                              
  --SELECT tintSource , tintSourceType FROM dbo.OpportunityMaster
		insert into OpportunityMaster                              
		  (                              
		  numContactId,                              
		  numDivisionId,                              
		  txtComments,                              
		  numCampainID,                              
		  bitPublicFlag,                              
		  tintSource, 
		  tintSourceType ,                               
		  vcPOppName,                              
		  monPAmount,                              
		  numCreatedBy,                              
		  bintCreatedDate,                               
		  numModifiedBy,                              
		  bintModifiedDate,                              
		  numDomainId,                               
		  tintOppType, 
		  tintOppStatus,                   
		  intPEstimatedCloseDate,              
		  numRecOwner,
		  bitOrder,
		  numCurrencyID,
		  fltExchangeRate,fltDiscountTotal,bitDiscountTypeTotal,
		  monShipCost,
  numOppBizDocTempID,numAccountClass,numPartner,intUsedShippingCompany,numPartenerContact,numShipmentMethod,dtReleaseDate,vcOppRefOrderNo,
  bitBillingTerms,intBillingDays,bitInterestType,fltInterest
		  )                              
		 Values                              
		  (                              
		  @numContactId,                              
		  @numDivID,                              
		  @txtComments,                              
		  @numCampainID,--  0,
		  0,                              
		  @tintSource,   
		  @tintSourceType,                           
		  ISNULL(@vcPOppName,'SO'),                             
		  0,                                
		  @numContactId,                              
		  getutcdate(),                              
		  @numContactId,                              
		  getutcdate(),        
		  @numDomainId,                              
		  1,             
		  @tintOppStatus,       
		  getutcdate(),                                 
		  @numRecOwner ,
		  1,
		  @numCurrencyID,
		  @fltExchangeRate,@fltDiscount,@bitDiscountType
		  ,@monShipCost,
  @numTemplateID,@numAccountClass,@numPartner,@intUsedShippingCompany,@numPartenerContact,@numShipmentMethod,GETDATE(),@vcOppRefOrderNo
  ,@bitBillingTerms,@intBillingDays,@bitInterestType,@fltInterest
		  )        
set @numOppID=scope_identity()                             
--Update OppName as per Name Template
		EXEC dbo.USP_UpdateNameTemplateValue 1,@numDomainId,@numOppID
		SELECT @vcPOppName = vcPOppName FROM OpportunityMaster WHERE numOppID= @numOppID
end
delete  from    OpportunityItems where   numOppId= @numOppID                               

--if convert(varchar(10),@strItems) <>''                
--begin               
-- DECLARE @hDocItem int                
-- EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems                

	UPDATE 
		P
	SET
		intCouponCodeUsed=ISNULL(intCouponCodeUsed,0)+1
	FROM 
		PromotionOffer AS P
	LEFT JOIN
		CartItems AS C
	ON
		P.numProId=C.PromotionID
	WHERE
	C.vcCoupon=P.txtCouponCode 
	AND (C.fltDiscount>0 OR C.numCartId IN (SELECT numCartId FROM CartItems AS CG WHERE CG.PromotionID=P.numProId AND CG.fltDiscount>0))
	AND numUserCntId =@numContactId
	AND ISNULL(bitEnabled,0)=1


insert into OpportunityItems                                                        
  (numOppId,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,vcAttributes,vcAttrValues,[vcItemName],[vcModelID],[vcManufacturer],vcPathForTImage, monVendorCost,numUOMId,bitDiscountType,fltDiscount,monTotAmtBefDiscount,numPromotionID,bitPromotionTriggered,bitDropShip,vcPromotionDetail)                                                        
  --kishan                                                       
 select @numOppID,X.numItemCode,x.numUnitHour * x.decUOMConversionFactor,x.monPrice/x.decUOMConversionFactor,x.monTotAmount,X.vcItemDesc, NULLIF(X.numWarehouseItmsID,0),X.vcItemType,X.vcAttributes,X.vcAttrValues,(SELECT vcItemName FROM item WHERE numItemCode = X.numItemCode),(SELECT vcModelID FROM item WHERE numItemCode = X.numItemCode),(SELECT vcManufacturer FROM item WHERE numItemCode = X.numItemCode),
		(SELECT TOP 1  vcPathForTImage FROM dbo.ItemImages WHERE numItemCode = X.numItemCode AND bitDefault = 1 AND numDomainId = @numDomainId),   (select isnull(VN.monCost,0) from Vendor VN INNER JOIN Item IT ON IT.numItemCode=VN.numItemCode and VN.numVendorID=IT.numVendorID where VN.numItemCode=X.numItemCode), 
		numUOM,bitDiscountType,fltDiscount,monTotAmtBefDiscount,X.PromotionID,X.bitParentPromotion,ISNULL((SELECT bitAllowDropShip FROM Item WHERE numItemCode=X.numItemCode),0),(CASE WHEN ISNULL(X.PromotionID,0) > 0 THEN ISNULL((SELECT CONCAT('Buy '
					,CASE WHEN tintOfferTriggerValueType=1 THEN CAST(fltOfferTriggerValue AS VARCHAR) ELSE CONCAT('$',fltOfferTriggerValue) END
					,CASE tintOfferBasedOn
							WHEN 1 THEN CONCAT(' of "',(SELECT Stuff((SELECT CONCAT(', ',Item.vcItemName) FROM PromotionOfferItems INNER JOIN Item ON PromotionOfferItems.numValue = Item.numItemCode WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=1 FOR XML PATH('')), 1, 2, '')), '"')
							WHEN 2 THEN CONCAT(' of items belonging to classification(s) "',(SELECT Stuff((SELECT CONCAT(', ',ListDetails.vcData) FROM PromotionOfferItems INNER JOIN ListDetails ON PromotionOfferItems.numValue = ListDetails.numListItemID WHERE numListID=36 AND numProId=PO.numProId AND tintRecordType=5 AND tintType=2 FOR XML PATH('')), 1, 2, '')), '"')
						END
					,' & get '
					, CASE tintDiscountType WHEN 1 THEN CONCAT(fltDiscountValue,'% off on') WHEN 2 THEN CONCAT('$',fltDiscountValue, ' off on') WHEN 3 THEN CONCAT(fltDiscountValue,' of') END
					, CASE 
						WHEN tintDiscoutBaseOn = 1 THEN 
							CONCAT
							(
								CONCAT(' "',(SELECT Stuff((SELECT CONCAT(',',Item.vcItemName) FROM PromotionOfferItems INNER JOIN Item ON PromotionOfferItems.numValue = Item.numItemCode WHERE numProId=PO.numProId AND tintRecordType=6 AND tintType=1 FOR XML PATH('')), 1, 1, '')), '"')
								,(CASE WHEN PO.tintDiscountType = 3 THEN ' free' ELSE '' END)
								,'.'
							)
						WHEN tintDiscoutBaseOn = 2 THEN 
							CONCAT
							(
								CONCAT(' items belonging to classification(s) "',(SELECT Stuff((SELECT CONCAT(',',ListDetails.vcData) FROM PromotionOfferItems INNER JOIN ListDetails ON PromotionOfferItems.numValue = ListDetails.numListItemID WHERE numListID=36 AND numProId=PO.numProId AND tintRecordType=6 AND tintType=2 FOR XML PATH('')), 1, 1, '')), '"')
								,(CASE WHEN PO.tintDiscountType = 3 THEN ' free' ELSE '' END)
								,'.'
							)
						WHEN tintDiscoutBaseOn = 3 THEN ' related items.'
						ELSE '' 
					END
				) FROM PromotionOffer PO WHERE numProId=X.PromotionID AND numDomainId=@numDomainID),'') ELSE '' END)
FROM dbo.CartItems X WHERE numUserCntId =@numContactId

	declare @tintShipped as tinyint      
	DECLARE @tintOppType AS TINYINT
	
select @tintOppStatus=tintOppStatus,@tintOppType=tintOppType,@tintShipped=tintShipped from OpportunityMaster where numOppID=@numOppID              
	if @tintOppStatus=1               
	begin         
	PRINT 'inside USP_UpdatingInventoryonCloseDeal'
		exec USP_UpdatingInventoryonCloseDeal @numOppID,@numContactId
	end              

/*Commented by Chintan..reason:tobe executed only when order is confirmed*/
--exec USP_UpdatingInventoryonCloseDeal @numOppID,@numContactId

-- SELECT * FROM OPENXML (@hDocItem,'/NewDataSet/Table',2)                                                        
-- WITH  (                                                        
--  numoppitemtCode numeric(9),                   
--  numItemCode numeric(9),                                                        
--  numUnitHour numeric(9),                                                        
--  monPrice money,                                                     
--  monTotAmount money,                                                        
--  vcItemDesc varchar(1000),                  
--  numWarehouseItmsID numeric(9),        
--  vcItemType varchar(30),    
--  vcAttrValues varchar(50) ,
--  numUOM NUMERIC(18,0),
--  decUOMConversionFactor DECIMAL(18,5),bitDiscountType BIT,fltDiscount FLOAT,monTotAmtBefDiscount MONEY,
--  vcShippingMethod VARCHAR(100),decShippingCharge MONEY,dtDeliveryDate datetime
--  ))X                
--                
                
-- EXEC sp_xml_removedocument @hDocItem                
                
--end
                             
                  
	select @TotalAmount=sum(monTotAmount)from OpportunityItems where numOppId=@numOppID                            
 update OpportunityMaster set  monPamount=@TotalAmount - @fltDiscount                           
 where numOppId=@numOppID                           
                          
--set @vcPOppName=(select  ISNULL(vcPOppName,numOppId) from OpportunityMaster where numOppId=@numOppID )
                          
	IF(@vcSource IS NOT NULL)
	BEGIN
		insert into OpportunityLinking ([numParentOppID],[numChildOppID],[vcSource],[numParentOppBizDocID],numPromotionId,numSiteID)  values(null,@numOppID,@vcSource,NULL,@numProId,@numSiteID);
	END

	--Add/Update Address Details
	DECLARE @vcStreet  varchar(100),@vcCity  varchar(50),@vcPostalCode  varchar(15),@vcCompanyName  varchar(100),@vcAddressName VARCHAR(50)      
	DECLARE @numState  numeric(9),@numCountry  numeric(9), @numCompanyId  numeric(9) 
	DECLARE @bitIsPrimary BIT;

	--Bill Address
	IF @numBillAddressId>0
	BEGIN
	 SELECT  @vcStreet=ISNULL(vcStreet,''),@vcCity=ISNULL(vcCity,''),@vcPostalCode=ISNULL(vcPostalCode,''),
			@numState=ISNULL(numState,0),@numCountry=ISNULL(numCountry,0),@bitIsPrimary=bitIsPrimary,
			@vcAddressName=vcAddressName
				FROM  dbo.AddressDetails WHERE   numDomainID = @numDomainID AND numAddressID = @numBillAddressId 
            
  
  		EXEC dbo.USP_UpdateOppAddress
		@numOppID = @numOppID, --  numeric(9, 0)
		@byteMode = 0, --  tinyint
		@vcStreet = @vcStreet, --  varchar(100)
		@vcCity = @vcCity, --  varchar(50)
		@vcPostalCode = @vcPostalCode, --  varchar(15)
		@numState = @numState, --  numeric(9, 0)
		@numCountry = @numCountry, --  numeric(9, 0)
		@vcCompanyName = '', --  varchar(100)
		@numCompanyId = 0, --  numeric(9, 0)
		@vcAddressName =@vcAddressName,
		@bitCalledFromProcedure=1
        
	END
  
  
	--Ship Address
	IF @numShipAddressId>0
	BEGIN
   		SELECT  @vcStreet=ISNULL(vcStreet,''),@vcCity=ISNULL(vcCity,''),@vcPostalCode=ISNULL(vcPostalCode,''),
			@numState=ISNULL(numState,0),@numCountry=ISNULL(numCountry,0),@bitIsPrimary=bitIsPrimary,
			@vcAddressName=vcAddressName
				FROM  dbo.AddressDetails WHERE   numDomainID = @numDomainID AND numAddressID = @numShipAddressId 
  
  
		select  @vcCompanyName=vcCompanyName,@numCompanyId=div.numCompanyID from CompanyInfo Com                            
		join divisionMaster Div                            
		on div.numCompanyID=com.numCompanyID                            
		where div.numdivisionID=@numDivID

  		EXEC dbo.USP_UpdateOppAddress
		@numOppID = @numOppID, --  numeric(9, 0)
		@byteMode = 1, --  tinyint
		@vcStreet = @vcStreet, --  varchar(100)
		@vcCity = @vcCity, --  varchar(50)
		@vcPostalCode = @vcPostalCode, --  varchar(15)
		@numState = @numState, --  numeric(9, 0)
		@numCountry = @numCountry, --  numeric(9, 0)
		@vcCompanyName = @vcCompanyName, --  varchar(100)
		@numCompanyId =@numCompanyId, --  numeric(9, 0)
		@vcAddressName =@vcAddressName,
		@bitCalledFromProcedure=1
   	
	END

	-- IMPORTANT: KEEP THIS BELOW ADDRESS UPDATE LOGIC
	--Insert Tax for Division                       
	INSERT dbo.OpportunityMasterTaxItems 
	(
		numOppId,
		numTaxItemID,
		fltPercentage,
		tintTaxType,
		numTaxID
	) 
	SELECT 
		@numOppID,
		TI.numTaxItemID,
		TEMPTax.decTaxValue,
		TEMPTax.tintTaxType,
		0
	FROM 
		TaxItems TI 
	JOIN 
		DivisionTaxTypes DTT 
	ON 
		TI.numTaxItemID = DTT.numTaxItemID
	CROSS APPLY
	(
		SELECT
			decTaxValue,
			tintTaxType,
			numTaxID
		FROM
			dbo.fn_CalItemTaxAmt(@numDomainID,@numDivID,0,TI.numTaxItemID,@numOppId,0,NULL)
	) AS TEMPTax
	WHERE 
		DTT.numDivisionID=@numDivID AND DTT.bitApplicable=1
	UNION 
	SELECT 
		@numOppID,
		0,
		TEMPTax.decTaxValue,
		TEMPTax.tintTaxType,
		0
	FROM 
		dbo.DivisionMaster 
	CROSS APPLY
	(
		SELECT
			decTaxValue,
			tintTaxType,
			numTaxID
		FROM
			dbo.fn_CalItemTaxAmt(@numDomainID,@numDivID,0,0,@numOppId,1,NULL)
	) AS TEMPTax
	WHERE 
		bitNoTax=0 
		AND numDivisionID=@numDivID
	UNION 
	SELECT
		@numOppId
		,1
		,decTaxValue
		,tintTaxType
		,numTaxID
	FROM
		dbo.fn_CalItemTaxAmt(@numDomainID,@numDivID,0,1,@numOppId,1,NULL)
  
	--Delete Tax for Opportunity Items if item deleted 
	DELETE FROM OpportunityItemsTaxItems WHERE numOppId=@numOppId AND numOppItemID NOT IN (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId=@numOppID)

	--Insert Tax for Opportunity Items
	INSERT INTO dbo.OpportunityItemsTaxItems 
	(
		numOppId,
		numOppItemID,
		numTaxItemID,
		numTaxID
	) 
	SELECT 
		@numOppId,
		OI.numoppitemtCode,
		TI.numTaxItemID,
		0 
	FROM 
		dbo.OpportunityItems OI 
	JOIN 
		dbo.ItemTax IT 
	ON 
		OI.numItemCode=IT.numItemCode 
	JOIN
		TaxItems TI 
	ON 
		TI.numTaxItemID=IT.numTaxItemID 
	WHERE 
		OI.numOppId=@numOppID 
		AND IT.bitApplicable=1 
		AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)
	UNION
	SELECT 
		@numOppId,
		OI.numoppitemtCode,
		0,
		0 
	FROM 
		dbo.OpportunityItems OI 
	JOIN 
		dbo.Item I 
	ON 
		OI.numItemCode=I.numItemCode
	WHERE 
		OI.numOppId=@numOppID 
		AND I.bitTaxable=1 
		AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)
	UNION
	SELECT
		@numOppId,
		OI.numoppitemtCode,
		1,
		TD.numTaxID
	FROM
		dbo.OpportunityItems OI 
	INNER JOIN
		ItemTax IT
	ON
		IT.numItemCode = OI.numItemCode
	INNER JOIN
		TaxDetails TD
	ON
		TD.numTaxID = IT.numTaxID
		AND TD.numDomainId = @numDomainId
	WHERE
		OI.numOppId = @numOppID
		AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)

COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

  -- Whoops, there was an error
  IF @@TRANCOUNT > 0
     ROLLBACK

  SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
GO
/****** Object:  StoredProcedure [dbo].[USP_UpdateOppLinkingDTls]    Script Date: 05/07/2009 22:12:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_updateopplinkingdtls')
DROP PROCEDURE usp_updateopplinkingdtls
GO
CREATE PROCEDURE [dbo].[USP_UpdateOppLinkingDTls]        
@numOppID as numeric(9)=0,            
@tintOppType as tinyint,        
@tintBillToType as tinyint,        
@tintShipToType as tinyint,        
@vcBillStreet as varchar(50),                  
@vcBillCity as varchar(50),                  
@numBillState as numeric(9),                  
@vcBillPostCode as varchar(15),                  
@numBillCountry as numeric(9),                          
@vcShipStreet as varchar(50),                  
@vcShipCity as varchar(50),                  
@numShipState as numeric(9),                  
@vcShipPostCode as varchar(15),                  
@numShipCountry as numeric(9),    
@vcBillCompanyName as varchar(100),    
@vcShipCompanyName as varchar(100),
@numParentOppID as numeric(9),
@numParentOppBizDocID AS NUMERIC(9),
@numBillToAddressID AS NUMERIC(18,0) = 0,
@numShipToAddressID AS NUMERIC(18,0) = 0
AS        
BEGIN                    
	UPDATE 
		OpportunityMaster 
	SET 
		tintBillToType=@tintBillToType,
		numBillToAddressID = @numBillToAddressID,
		tintShipToType=@tintShipToType,
		numShipToAddressID = @numShipToAddressID
	WHERE 
		numOppID=@numOppID        
        
	IF @tintBillToType = 2 OR @tintShipToType = 2 
	BEGIN
		IF (SELECT COUNT(*) FROM OpportunityAddress WHERE [numOppID]=@numOppID) = 0
		BEGIN
			INSERT  INTO OpportunityAddress ( numOppID )
			VALUES  ( @numOppID )      
		END
	END

	IF @tintBillToType = 2 
	BEGIN
		UPDATE  OpportunityAddress
		SET     vcBillCompanyName = @vcBillCompanyName,
				vcBillStreet = @vcBillStreet,
				vcBillCity = @vcBillCity,
				numBillState = @numBillState,
				vcBillPostCode = @vcBillPostCode,
				numBillCountry = @numBillCountry
		WHERE   numOppID = @numOppID
	
	END
    
	IF @tintShipToType = 2 
	BEGIN
	PRINT 'hi'
		UPDATE  OpportunityAddress
		SET     vcShipCompanyName = @vcShipCompanyName,
				vcShipStreet = @vcShipStreet,
				vcShipCity = @vcShipCity,
				numShipState = @numShipState,
				vcShipPostCode = @vcShipPostCode,
				numShipCountry = @numShipCountry
		WHERE   numOppID = @numOppID

	END


	if @numParentOppID>0 
	BEGIN
		IF @numParentOppBizDocID = 0 
			SET @numParentOppBizDocID = NULL       

		insert into OpportunityLinking (numParentOppID,numChildOppID,numParentOppBizDocID) values(@numParentOppID,@numOppID,@numParentOppBizDocID)



		If @tintOppType = 2
		BEGIN
			DECLARE @numDOmainID NUMERIC(18,0) = ISNULL((SELECT numDOmainID FROM OpportunityMaster WHERE numOppID=@numOppID),0)

			UPDATE 
				OpportunityMaster 
			SET 
				vcCustomerPO#=ISNULL((SELECT vcOppRefOrderNo FROM OpportunityMaster WHERE numOppId=@numParentOppID),'')
			WHERE 
				numOppID=@numOppID 


			IF EXISTS (SELECT numParentChildFieldID FROM ParentChildCustomFieldMap WHERE numDomainID=@numDomainID AND numParentFieldID=100 AND tintParentModule=2 AND numChildFieldID=100 AND tintChildModule=6)
			BEGIN
				UPDATE 
					OpportunityMaster 
				SET 
					numAssignedTo=ISNULL((SELECT numAssignedTo FROM OpportunityMaster WHERE numOppId=@numParentOppID),0)
				WHERE 
					numOppID=@numOppID 
			END

			IF EXISTS (SELECT numParentChildFieldID FROM ParentChildCustomFieldMap WHERE numDomainID=@numDomainID AND numParentFieldID=122 AND tintParentModule=2 AND numChildFieldID=122 AND tintChildModule=6)
			BEGIN
				UPDATE 
					OpportunityMaster 
				SET 
					txtComments=ISNULL((SELECT txtComments FROM OpportunityMaster WHERE numOppId=@numParentOppID),'')
				WHERE 
					numOppID=@numOppID 
			END

			IF EXISTS (SELECT numParentChildFieldID FROM ParentChildCustomFieldMap WHERE numDomainID=@numDomainID AND numParentFieldID=771 AND tintParentModule=2 AND numChildFieldID=771 AND tintChildModule=6)
			BEGIN
				UPDATE 
					OpportunityMaster 
				SET 
					numShipmentMethod=ISNULL((SELECT numShipmentMethod FROM OpportunityMaster WHERE numOppId=@numParentOppID),0)
				WHERE 
					numOppID=@numOppID 
			END

			
			EXEC dbo.USP_AddParentChildCustomFieldMap
						@numDomainID = @numDOmainID, --  numeric(18, 0)
						@numRecordID = @numOppID, --  numeric(18, 0)
						@numParentRecId = @numParentOppID, --  numeric(18, 0)
						@tintPageID = 6 --  tinyin
		END
	end
END
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_UserMaster_ChangeDashboardTemplate')
DROP PROCEDURE USP_UserMaster_ChangeDashboardTemplate
GO
CREATE PROCEDURE [dbo].[USP_UserMaster_ChangeDashboardTemplate]
	@numDomainID NUMERIC(18,0)
	,@numUserID AS NUMERIC(18,0)
	,@numTemplateID AS NUMERIC(18,0)
AS
BEGIN 
	UPDATE 
		UserMaster
	SET
		numDashboardTemplateID=@numTemplateID
	WHERE
		numUserId=@numUserID

	IF ISNULL(@numTemplateID,0) > 0
	BEGIN
		-- DELETE OLD DASHBOARD CONFIGURATION FOR USER BECAUSE IT MAY BE CHANGED FROM LAST ASSIGNMENT 
		DELETE FROM ReportDashboard WHERE numDomainID=@numDomainID AND numUserCntID=ISNULL((SELECT numUserDetailId FROM UserMaster WHERE numUserId=@numUserID),0) AND numDashboardTemplateID=@numTemplateID

		-- INSERT LATEST CONFIGURATION OF DASHBOARD TEMPLATE
		INSERT INTO [dbo].[ReportDashboard]
		(
			numDomainID
			,numReportID
			,numUserCntID
			,tintReportType
			,tintChartType
			,vcHeaderText
			,vcFooterText
			,tintRow
			,tintColumn
			,vcXAxis
			,vcYAxis
			,vcAggType
			,tintReportCategory
			,intHeight
			,intWidth
			,numDashboardTemplateID
		)
		SELECT
			DashboardTemplateReports.numDomainID
			,DashboardTemplateReports.numReportID
			,ISNULL((SELECT numUserDetailId FROM UserMaster WHERE numUserId=@numUserID),0)
			,DashboardTemplateReports.tintReportType
			,tintChartType
			,vcHeaderText
			,vcFooterText
			,tintRow
			,tintColumn
			,vcXAxis
			,vcYAxis
			,vcAggType
			,tintReportCategory
			,intHeight
			,intWidth
			,numDashboardTemplateID
		FROM
			DashboardTemplateReports
		INNER JOIN
			ReportListMaster
		ON
			DashboardTemplateReports.numReportID=ReportListMaster.numReportID
			AND (ReportListMaster.numDomainID=@numDomainID OR bitDefault=1)
		WHERE
			DashboardTemplateReports.numDomainID=@numDOmainID
			AND numDashboardTemplateID=@numTemplateID
	END
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetOrderSelectedItemDetail')
DROP PROCEDURE USP_GetOrderSelectedItemDetail
GO
CREATE PROCEDURE [dbo].[USP_GetOrderSelectedItemDetail]
	@numDomainID NUMERIC(18,0),
	@numDivisionID NUMERIC(18,0),
	@tintOppType AS TINYINT,
	@numItemCode NUMERIC(18,0),
	@numUOMID NUMERIC(18,0),
	@numWarehouseItemID AS NUMERIC(18,0),
	@numQty INT,
	@monPrice FLOAT,
	@numOppID NUMERIC(18,0),
	@numOppItemID NUMERIC(18,0),
	@vcSelectedKitChildItems VARCHAR(MAX),
	@numShippingCountry AS NUMERIC(18,0),
	@numWarehouseID AS NUMERIC(18,0),
	@vcCoupon AS VARCHAR(200)
AS
BEGIN
	DECLARE @vcItemName AS VARCHAR(500)
	DECLARE @bitDropship AS BIT
	DECLARE @bitMatrix AS BIT
	DECLARE @chrItemType AS CHAR(1)
	DECLARE @numItemGroup AS NUMERIC(18,0)
	DECLARE @numBaseUnit AS NUMERIC(18,0)
	DECLARE @numSaleUnit AS NUMERIC(18,0)
	DECLARE @fltUOMConversionFactor AS FLOAT
	DECLARE @numPurchaseUnit AS NUMERIC(18,0)
	DECLARE @bitAssemblyOrKit BIT
	DECLARE @bitAssembly AS BIT
	DECLARE @bitCalAmtBasedonDepItems AS BIT
	DECLARE @ItemDesc AS VARCHAR(1000)
	DECLARE @numItemClassification AS NUMERIC(18,0)

	DECLARE @monListPrice AS MONEY = 0.0000
	DECLARE @monVendorCost AS MONEY = 0.0000
	DECLARE @tintPricingBasedOn AS TINYINT = 0.0000

	DECLARE @tintRuleType AS TINYINT
	DECLARE @monPriceLevelPrice AS MONEY = 0.0000
	DECLARE @monPriceFinalPrice AS MONEY = 0.0000
	DECLARE @monPriceRulePrice AS MONEY = 0.0000
	DECLARE @monPriceRuleFinalPrice AS MONEY = 0.0000
	DECLARE @monLastPrice AS MONEY = 0.0000
	DECLARE @dtLastOrderDate VARCHAR(200) = ''

	DECLARE @tintDisountType TINYINT
	DECLARE @decDiscount AS FLOAT

	DECLARE @numVendorID AS NUMERIC(18,0)
	DECLARE @vcMinOrderQty AS VARCHAR(5) = '-'
	DECLARE @vcVendorNotes AS VARCHAR(300) = ''
	DECLARE @numRelationship NUMERIC(18,0)
	DECLARE @numProfile NUMERIC(18,0)
	DECLARE @tintPriceLevel AS TINYINT
	DECLARE @tintDefaultSalesPricing AS TINYINT
	DECLARE @tintPriceBookDiscount AS TINYINT

	IF ISNULL(@numDivisionID,0) > 0
	BEGIN
		SELECT @vcMinOrderQty=ISNULL(CAST(intMinQty AS VARCHAR),'-'),@vcVendorNotes=ISNULL(vcNotes,'') FROM Vendor WHERE numVendorID=@numDivisionID AND numItemCode=@numItemCode
	END

	SELECT @tintDefaultSalesPricing = numDefaultSalesPricing,@tintPriceBookDiscount=tintPriceBookDiscount FROM Domain WHERE numDomainId=@numDomainID

	IF ISNULL(@numWarehouseID,0) > 0
	BEGIN
		SELECT TOP 1 @numWarehouseItemID=numWareHouseItemID FROM WareHouseItems WHERE numItemID=@numItemCode AND numWareHouseID=@numWarehouseID ORDER BY numWareHouseItemID ASC
	END

	-- GET ORGANIZATION DETAL
	SELECT 
		@numRelationship=numCompanyType,
		@numProfile=vcProfile,
		@tintPriceLevel = ISNULL(D.tintPriceLevel,0)
	FROM 
		DivisionMaster D                  
	JOIN 
		CompanyInfo C 
	ON 
		C.numCompanyId=D.numCompanyID                  
	WHERE 
		numDivisionID =@numDivisionID       

	-- GET ITEM DETAILS
	SELECT
		@vcItemName = vcItemName,
		@bitDropship = bitAllowDropShip,
		@ItemDesc = Item.txtItemDesc,
		@bitMatrix = ISNULL(Item.bitMatrix,0),
		@chrItemType = charItemType,
		@numItemGroup = numItemGroup,
		@bitAssembly = ISNULL(bitAssembly,0),
		@numBaseUnit = ISNULL(numBaseUnit,0),
		@numSaleUnit = (CASE WHEN ISNULL(@numUOMID,0) > 0 THEN @numUOMID ELSE ISNULL(numSaleUnit,0) END),
		@numPurchaseUnit = (CASE WHEN ISNULL(@numUOMID,0) > 0 THEN @numUOMID ELSE ISNULL(numPurchaseUnit,0) END),
		@numItemClassification = numItemClassification,
		@bitAssemblyOrKit = (CASE WHEN ISNULL(bitKitParent,0)=1 OR ISNULL(bitAssembly,0)=1 THEN 1 ELSE 0 END),
		@monListPrice = (CASE WHEN Item.charItemType='P' THEN ISNULL(WareHouseItems.monWListPrice,0) ELSE ISNULL(Item.monListPrice,0) END),
		@monVendorCost = ISNULL((SELECT monCost FROM Vendor WHERE numVendorID=Item.numVendorID AND Vendor.numItemCode=Item.numItemCode),0),
		@numVendorID = ISNULL(Item.numVendorID,0),
		@numWarehouseItemID = (CASE WHEN ISNULL(@numWarehouseItemID,0) > 0 THEN @numWarehouseItemID ELSE WareHouseItems.numWareHouseItemID END),
		@bitCalAmtBasedonDepItems = ISNULL(bitCalAmtBasedonDepItems,0)
	FROM
		Item
	OUTER APPLY
	(
		SELECT TOP 1 numWareHouseItemID,monWListPrice FROM WareHouseItems WHERE numItemID=@numItemCode AND (numWareHouseItemID = @numWarehouseItemID OR ISNULL(@numWarehouseItemID,0) = 0)
	) AS WareHouseItems
	WHERE
		Item.numItemCode = @numItemCode

	IF ISNULL(@numDivisionID,0) > 0 AND EXISTS (SELECT * FROM Vendor WHERE numItemCode=@numItemCode AND numVendorID=@numDivisionID)
	BEGIN
		SELECT @monVendorCost=ISNULL(monCost,0) FROM Vendor WHERE numVendorID=@numDivisionID AND numItemCode=@numItemCode
	END


	IF @chrItemType = 'P' AND ISNULL(@numWarehouseItemID,0) = 0
	BEGIN
		RAISERROR('WAREHOUSE_DOES_NOT_EXISTS',16,1)
	END

	SET @fltUOMConversionFactor = dbo.fn_UOMConversion((CASE WHEN  @tintOppType = 1 THEN @numSaleUnit ELSE @numPurchaseUnit END),@numItemCode,@numDomainId,@numBaseUnit)

	IF @tintOppType = 1
	BEGIN
		/*Use Calculated Price When item is Assembly/Kit,Calculate price based on sum of dependent items.*/
		IF @bitCalAmtBasedonDepItems = 1 
		BEGIN
			PRINT 'IN DEPENDED ITEM'

			CREATE TABLE #TEMPSelectedKitChilds
			(
				ChildKitItemID NUMERIC(18,0),
				ChildKitWarehouseItemID NUMERIC(18,0),
				ChildKitChildItemID NUMERIC(18,0),
				ChildKitChildWarehouseItemID NUMERIC(18,0)
			)

			IF ISNULL(@numOppItemID,0) > 0
			BEGIN
				INSERT INTO 
					#TEMPSelectedKitChilds
				SELECT 
					OKI.numChildItemID,
					OKI.numWareHouseItemId,
					OKCI.numItemID,
					OKCI.numWareHouseItemId
				FROM
					OpportunityKitItems OKI
				INNER JOIN
					OpportunityKitChildItems OKCI
				ON
					OKI.numOppChildItemID = OKCI.numOppChildItemID
				WHERE
					OKI.numOppId = @numOppID
					AND OKI.numOppItemID = @numOppItemID
			END
			ELSE IF ISNULL(@vcSelectedKitChildItems,'') <> ''
			BEGIN
				INSERT INTO 
					#TEMPSelectedKitChilds
				SELECT 
					SUBSTRING
					(
						SUBSTRING(OutParam,0,CHARINDEX('-',OutParam)),
						0,
						CHARINDEX('#', SUBSTRING(OutParam,0,CHARINDEX('-',OutParam)))
					) AS numChildKitID,
					SUBSTRING
					(
						SUBSTRING(OutParam,0,CHARINDEX('-',OutParam)),
						CHARINDEX('#', SUBSTRING(OutParam,0,CHARINDEX('-',OutParam))) + 1,
						LEN(SUBSTRING(OutParam,0,CHARINDEX('-',OutParam)))
					) AS numChildKitWarehouseItemID,
					SUBSTRING
					(
						SUBSTRING(OutParam,CHARINDEX('-',OutParam) + 1, LEN(OutParam)),
						0,
						CHARINDEX('#', SUBSTRING(OutParam,CHARINDEX('-',OutParam) + 1, LEN(OutParam)))
					) AS numChildKitID,
					SUBSTRING
					(
						SUBSTRING(OutParam,CHARINDEX('-',OutParam) + 1, LEN(OutParam)),
						CHARINDEX('#', SUBSTRING(OutParam,CHARINDEX('-',OutParam) + 1, LEN(OutParam))) + 1,
						LEN(SUBSTRING(OutParam,CHARINDEX('-',OutParam) + 1, LEN(OutParam)))
					) AS numChildKitWarehouseItemID
		
				FROM 
					dbo.SplitString(@vcSelectedKitChildItems,',')
			END

			;WITH CTE (numItemCode, vcItemName, bitKitParent, numWarehouseItemID,numQtyItemsReq,numUOMId) AS
			(
				SELECT
					ID.numChildItemID,
					I.vcItemName,
					ISNULL(I.bitKitParent,0),
					ISNULL(ID.numWarehouseItemId,0),
					CAST((ISNULL(ID.numQtyItemsReq,0) * ISNULL(dbo.fn_UOMConversion(ISNULL(ID.numUOMID, 0), I.numItemCode, @numDomainID, ISNULL(I.numBaseUnit, 0)),1)) AS FLOAT),
					ISNULL(numUOMId,0)
				FROM 
					[ItemDetails] ID
				INNER JOIN 
					[Item] I 
				ON 
					ID.[numChildItemID] = I.[numItemCode]
				WHERE   
					[numItemKitID] = @numItemCode
				UNION ALL
				SELECT
					ID.numChildItemID,
					I.vcItemName,
					ISNULL(I.bitKitParent,0),
					ISNULL(ID.numWarehouseItemId,0),
					CAST((ISNULL(Temp1.numQtyItemsReq,0) * ISNULL(ID.numQtyItemsReq,0) * ISNULL(dbo.fn_UOMConversion(ISNULL(ID.numUOMID, 0), I.numItemCode, @numDomainID, ISNULL(I.numBaseUnit, 0)),1)) AS FLOAT),
					ISNULL(ID.numUOMId,0)
				FROM 
					CTE As Temp1
				INNER JOIN
					[ItemDetails] ID
				ON
					ID.numItemKitID = Temp1.numItemCode
				INNER JOIN 
					[Item] I 
				ON 
					ID.[numChildItemID] = I.[numItemCode]
				INNER JOIN
					#TEMPSelectedKitChilds
				ON
					ISNULL(Temp1.bitKitParent,0) = 1
					AND ID.numChildItemID = #TEMPSelectedKitChilds.ChildKitChildItemID
					AND ID.numWareHouseItemId = #TEMPSelectedKitChilds.ChildKitChildWarehouseItemID
			)

			SELECT  
					@monListPrice = ISNULL(  SUM(ISNULL(CASE WHEN I.[charItemType]='P' 
					THEN WI.[monWListPrice] 
					ELSE monListPrice END,0) * ID.[numQtyItemsReq] )  ,0)
			FROM    CTE ID
					INNER JOIN [Item] I ON ID.numItemCode = I.[numItemCode]
					left join  WareHouseItems WI
					on WI.numItemID=I.numItemCode and WI.[numWareHouseItemID]=ID.numWarehouseItemID
			WHERE
				ISNULL(ID.bitKitParent,0) = 0

			DROP TABLE #TEMPSelectedKitChilds
		END

		IF @tintDefaultSalesPricing = 1 AND (SELECT COUNT(*) FROM PricingTable WHERE numItemCode=@numItemCode AND ISNULL(numPriceRuleID,0) = 0) > 0 -- PRICE LEVEL
		BEGIN
			SELECT
				@tintDisountType = tintDiscountType,
				@decDiscount = ISNULL(decDiscount,0),
				@tintRuleType = tintRuleType,
				@monPriceLevelPrice = (CASE tintRuleType WHEN 1 THEN @monListPrice WHEN 2 THEN @monVendorCost ELSE ISNULL(decDiscount,0) END),
				@monPriceFinalPrice = (CASE tintRuleType
										WHEN 1 -- Deduct From List Price
										THEN
											CASE tintDiscountType 
												WHEN 1 -- PERCENT
												THEN (CASE WHEN @monListPrice > 0 THEN @monListPrice - (@monListPrice * (ISNULL(decDiscount,0)/100)) ELSE 0 END)
												WHEN 2 -- FLAT AMOUNT
												THEN (CASE WHEN @monListPrice > 0 THEN (CASE WHEN (@monListPrice * @numQty * @fltUOMConversionFactor) - ISNULL(decDiscount,0) > 0 THEN ((@monListPrice * @numQty * @fltUOMConversionFactor) - ISNULL(decDiscount,0)) / (@numQty * @fltUOMConversionFactor) ELSE 0 END) ELSE 0 END)
												WHEN 3 -- NAMED PRICE
												THEN ISNULL(decDiscount,0)
											END
										WHEN 2 -- Add to primary vendor cost
										THEN
											CASE tintDiscountType 
												WHEN 1  -- PERCENT
												THEN @monVendorCost + (@monVendorCost * (ISNULL(decDiscount,0)/100))  * @fltUOMConversionFactor
												WHEN 2 -- FLAT AMOUNT
												THEN (CASE WHEN (@monVendorCost * @numQty * @fltUOMConversionFactor) + ISNULL(decDiscount,0) > 0 THEN ((@monVendorCost * @numQty * @fltUOMConversionFactor) + ISNULL(decDiscount,0)) / (@numQty * @fltUOMConversionFactor)  ELSE 0 END)
												WHEN 3 -- NAMED PRICE
												THEN ISNULL(decDiscount,0)
											END
										WHEN 3 -- Named price
										THEN
											ISNULL(decDiscount,0)
										END
									) 
			FROM
			(
				SELECT 
					ROW_NUMBER() OVER(PARTITION BY PricingTable.numItemCode ORDER BY numPricingID) Id,
					*
				FROM
					PricingTable
				WHERE 
					PricingTable.numItemCode = @numItemCode
					
			) TEMP
			WHERE
				numItemCode=@numItemCode AND ((tintRuleType = 3 AND Id=@tintPriceLevel) OR @numQty * @fltUOMConversionFactor >= intFromQty AND @numQty * @fltUOMConversionFactor <= intToQty)
		END
		ELSE IF @tintDefaultSalesPricing = 2 AND (SELECT COUNT(*) FROM PriceBookRules WHERE numDomainID=@numDomainID AND tintRuleFor=@tintOppType) > 0 -- PRICE RULE
		BEGIN
			DECLARE @numPriceRuleID NUMERIC(18,0)
			DECLARE @tintPriceRuleType AS TINYINT
			DECLARE @tintPricingMethod TINYINT
			DECLARE @tintPriceBookDiscountType TINYINT
			DECLARE @decPriceBookDiscount FLOAT
			DECLARE @intQntyItems INT
			DECLARE @decMaxDedPerAmt FLOAT

			SELECT 
				TOP 1
				@numPriceRuleID = numPricRuleID,
				@tintPricingMethod = tintPricingMethod,
				@tintPriceRuleType = p.tintRuleType,
				@tintPriceBookDiscountType = P.[tintDiscountType],
				@decPriceBookDiscount = P.[decDiscount],
				@intQntyItems = P.[intQntyItems],
				@decMaxDedPerAmt = P.[decMaxDedPerAmt]
			FROM   
				PriceBookRules P 
			LEFT JOIN 
				PriceBookRuleDTL PDTL 
			ON 
				P.numPricRuleID = PDTL.numRuleID
			LEFT JOIN 
				PriceBookRuleItems PBI 
			ON 
				P.numPricRuleID = PBI.numRuleID
			LEFT JOIN 
				[PriceBookPriorities] PP 
			ON 
				PP.[Step2Value] = P.[tintStep2] 
				AND PP.[Step3Value] = P.[tintStep3]
			WHERE  
				P.numDomainID=@numDomainID 
				AND tintRuleFor=@tintOppType
				AND (
						((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 1 AND PDTL.numValue = @numDivisionID)) -- Priority 1
						OR ((tintStep2 = 2 AND PBI.numValue = @numItemClassification) AND (tintStep3= 1 AND PDTL.numValue = @numDivisionID)) -- Priority 2
						OR ((tintStep2 = 3) AND (tintStep3 = 1 AND PDTL.numValue = @numDivisionID)) -- Priority 3

						OR ((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 4
						OR ((tintStep2 = 2 AND PBI.numValue = @numItemClassification) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 5
						OR ((tintStep2 = 3) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 6

						OR ((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 3)) -- Priority 7
						OR ((tintStep2 = 2 AND PBI.numValue = @numItemClassification) AND (tintStep3 = 3)) -- Priority 8
						OR ((tintStep2 = 3) and (tintStep3 = 3)) -- Priority 9
					)
			ORDER BY 
				PP.Priority ASC


			IF ISNULL(@numPriceRuleID,0) > 0
			BEGIN
				IF @tintPricingMethod = 1 -- BASED ON PRICE TABLE
				BEGIN
					SELECT
						@tintDisountType = tintDiscountType,
						@decDiscount = ISNULL(decDiscount,0),
						@tintRuleType = tintRuleType,
						@monPriceRulePrice = (CASE tintRuleType WHEN 1 THEN @monListPrice WHEN 2 THEN @monVendorCost END) ,
						@monPriceRuleFinalPrice = (CASE tintRuleType
												WHEN 1 -- Deduct From List Price
												THEN
													CASE tintDiscountType 
														WHEN 1 -- PERCENT
														THEN (CASE WHEN @monListPrice > 0 THEN @monListPrice - (@monListPrice * (ISNULL(decDiscount,0)/100)) ELSE 0 END)
														WHEN 2 -- FLAT AMOUNT
														THEN (CASE WHEN @monListPrice > 0 THEN (CASE WHEN (@monListPrice * @numQty * @fltUOMConversionFactor) - ISNULL(decDiscount,0) > 0 THEN ((@monListPrice * @numQty * @fltUOMConversionFactor) - ISNULL(decDiscount,0)) / (@numQty * @fltUOMConversionFactor) ELSE 0 END) ELSE 0 END)
														WHEN 3 -- NAMED PRICE
														THEN ISNULL(decDiscount,0)
													END
												WHEN 2 -- Add to primary vendor cost
												THEN
													CASE tintDiscountType 
														WHEN 1  -- PERCENT
														THEN @monVendorCost + (@monVendorCost * (ISNULL(decDiscount,0)/100))
														WHEN 2 -- FLAT AMOUNT
														THEN (CASE WHEN (@monVendorCost * @numQty * @fltUOMConversionFactor) + ISNULL(decDiscount,0) > 0 THEN ((@monVendorCost * @numQty * @fltUOMConversionFactor) + ISNULL(decDiscount,0)) / (@numQty * @fltUOMConversionFactor)  ELSE 0 END)
														WHEN 3 -- NAMED PRICE
														THEN ISNULL(decDiscount,0)
													END
												WHEN 3 -- Named price
												THEN
													CASE 
													WHEN ISNULL(@tintPriceLevel,0) > 0
													THEN
														(SELECT 
															 ISNULL(decDiscount,0)
														FROM
														(
															SELECT 
																ROW_NUMBER() OVER(PARTITION BY PricingTable.numItemCode ORDER BY numPricingID) Id,
																decDiscount
															FROM 
																PricingTable 
															WHERE 
																PricingTable.numItemCode = @numItemCode 
																AND tintRuleType = 3 
														) TEMP
														WHERE
															Id = @tintPriceLevel)
													ELSE
														ISNULL(decDiscount,0)
													END
												END
											) 
					FROM
						PricingTable
					WHERE
						numPriceRuleID=@numPriceRuleID AND @numQty * @fltUOMConversionFactor >= intFromQty AND @numQty * @fltUOMConversionFactor <= intToQty
				END
				ELSE IF @tintPricingMethod = 2 -- BASED ON RULE
				BEGIN
					SET @tintRuleType = @tintPriceRuleType
					SET @monPriceRulePrice = (CASE @tintPriceRuleType WHEN 1 THEN @monListPrice WHEN 2 THEN @monVendorCost END) 

					IF (@tintPriceBookDiscountType = 1 ) -- Percentage 
					BEGIN
						SET @tintDisountType = @tintPriceBookDiscountType
						SET @decDiscount = @decDiscount * (@numQty/@intQntyItems);
						SET @decDiscount = @decDiscount * (CASE WHEN @tintPriceRuleType = 2 THEN @monVendorCost ELSE @monListPrice END) /100;
						SET @decMaxDedPerAmt = @decMaxDedPerAmt * (CASE WHEN @tintPriceRuleType = 2 THEN @monVendorCost ELSE @monListPrice END) /100;
					
						IF (@decDiscount > @decMaxDedPerAmt)
							SET @decDiscount = @decMaxDedPerAmt;
				
						IF ( @tintPriceRuleType = 1 )
								SET @monPriceRuleFinalPrice = (@monListPrice - @decDiscount);
						IF ( @tintPriceRuleType = 2 )
								SET @monPriceRuleFinalPrice = (@monVendorCost + @decDiscount);
					END
					IF ( @tintPriceBookDiscountType = 2 ) -- Flat discount 
					BEGIN
						SET @decDiscount = @decDiscount * ((@numQty * @fltUOMConversionFactor)/@intQntyItems);

						IF (@decDiscount > @decMaxDedPerAmt)
							SET @decDiscount = @decMaxDedPerAmt;
					
						IF ( @tintPriceRuleType = 1 )
							SET @monPriceRuleFinalPrice =  ((@monListPrice * @numQty * @fltUOMConversionFactor) - @decDiscount)/(@numQty * @fltUOMConversionFactor);
						IF ( @tintPriceRuleType = 2 )
							SET @monPriceRuleFinalPrice = ((@monVendorCost * @numQty * @fltUOMConversionFactor) + @decDiscount)/(@numQty * @fltUOMConversionFactor);
					END
				END
			END
		
		END
	
		-- GET Last Price
		DECLARE @LastDiscountType TINYINT
		DECLARE @LastDiscount FLOAT

		SELECT 
			TOP 1 
			@monLastPrice = monPrice
			,@dtLastOrderDate = dbo.FormatedDateFromDate(OpportunityMaster.bintCreatedDate,@numDomainID)
			,@LastDiscountType = ISNULL(OpportunityItems.bitDiscountType,1)
			,@LastDiscount = ISNULL(OpportunityItems.fltDiscount,0)
		FROM 
			OpportunityItems 
		JOIN 
			OpportunityMaster 
		ON 
			OpportunityItems.numOppId = OpportunityMaster.numOppId 
		WHERE 
			OpportunityMaster.numDomainId= @numDomainID
			AND tintOppType = @tintOppType
			AND tintOppStatus = 1
			AND numItemCode=@numItemCode
		ORDER BY
			OpportunityMaster.numOppId DESC

		IF @tintDefaultSalesPricing = 1 AND ISNULL(@monPriceFinalPrice,0) > 0
		BEGIN
			-- WE ARE NOT DISPLAYING DISCOUNT SEPERATELY IN PRICE LEVEL
			SET @tintPricingBasedOn = 1
			SET @monPrice = @monPriceFinalPrice
			SET @tintDisountType = 1
			SET @decDiscount = 0
		END
		ELSE IF @tintDefaultSalesPricing = 2 AND ISNULL(@monPriceRuleFinalPrice,0) > 0
		BEGIN
			SET @tintPricingBasedOn = 2
		
			IF @tintPriceBookDiscountType = 1 -- Display Disocunt Price As Unit Price
			BEGIN
				SET @monPrice = @monPriceRuleFinalPrice
				SET @tintDisountType = 1
				SET @decDiscount = 0
			END
			ELSE -- Display Unit Price And Disocunt Price Seperately
			BEGIN
				SET @monPrice = @monPriceRulePrice
			END
		
		END
		ELSE IF @tintDefaultSalesPricing = 3 AND ISNULL(@monLastPrice,0) > 0
		BEGIN
			SET @tintPricingBasedOn = 3
			SET @monPrice = @monLastPrice
			SET @tintDisountType = @LastDiscountType
			SET @decDiscount = @LastDiscount
		END
		ELSE
		BEGIN
			SET @tintPricingBasedOn = 0
			SET @tintDisountType = 1
			SET @decDiscount = 0
			SET @tintRuleType = 0

			IF @tintOppType = 1
				SET @monPrice = @monListPrice
			ELSE
				SET @monPrice = @monVendorCost
		END
	END
	ELSE IF @tintOppType = 2
	BEGIN
		SELECT 
			TOP 1
			@numPriceRuleID = numPricRuleID,
			@tintPricingMethod = tintPricingMethod,
			@tintPriceRuleType = p.tintRuleType,
			@tintPriceBookDiscountType = P.[tintDiscountType],
			@decPriceBookDiscount = P.[decDiscount],
			@intQntyItems = P.[intQntyItems],
			@decMaxDedPerAmt = P.[decMaxDedPerAmt]
		FROM   
			PriceBookRules P 
		LEFT JOIN 
			PriceBookRuleDTL PDTL 
		ON 
			P.numPricRuleID = PDTL.numRuleID
		LEFT JOIN 
			PriceBookRuleItems PBI 
		ON 
			P.numPricRuleID = PBI.numRuleID
		LEFT JOIN 
			[PriceBookPriorities] PP 
		ON 
			PP.[Step2Value] = P.[tintStep2] 
			AND PP.[Step3Value] = P.[tintStep3]
		WHERE  
			P.numDomainID=@numDomainID 
			AND tintRuleFor=@tintOppType
			AND (
					((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 1 AND PDTL.numValue = @numDivisionID)) -- Priority 1
					OR ((tintStep2 = 2 AND PBI.numValue = @numItemClassification) AND (tintStep3= 1 AND PDTL.numValue = @numDivisionID)) -- Priority 2
					OR ((tintStep2 = 3) AND (tintStep3 = 1 AND PDTL.numValue = @numDivisionID)) -- Priority 3

					OR ((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 4
					OR ((tintStep2 = 2 AND PBI.numValue = @numItemClassification) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 5
					OR ((tintStep2 = 3) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 6

					OR ((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 3)) -- Priority 7
					OR ((tintStep2 = 2 AND PBI.numValue = @numItemClassification) AND (tintStep3 = 3)) -- Priority 8
					OR ((tintStep2 = 3) and (tintStep3 = 3)) -- Priority 9
				)
		ORDER BY 
			PP.Priority ASC


		IF ISNULL(@numPriceRuleID,0) > 0
		BEGIN
			IF @tintPricingMethod = 1 -- BASED ON PRICE TABLE
			BEGIN
				SELECT
					@tintDisountType = tintDiscountType,
					@decDiscount = ISNULL(decDiscount,0),
					@tintRuleType = tintRuleType,
					@monPriceRulePrice = (CASE tintRuleType WHEN 1 THEN @monListPrice WHEN 2 THEN @monVendorCost END) ,
					@monPriceRuleFinalPrice = (CASE tintRuleType
											WHEN 1 -- Deduct From List Price
											THEN
												CASE tintDiscountType 
													WHEN 1 -- PERCENT
													THEN (CASE WHEN @monListPrice > 0 THEN @monListPrice - (@monListPrice * (ISNULL(decDiscount,0)/100)) ELSE 0 END)
													WHEN 2 -- FLAT AMOUNT
													THEN (CASE WHEN @monListPrice > 0 THEN (CASE WHEN (@monListPrice * @numQty * @fltUOMConversionFactor) - ISNULL(decDiscount,0) > 0 THEN ((@monListPrice * @numQty * @fltUOMConversionFactor) - ISNULL(decDiscount,0)) / (@numQty * @fltUOMConversionFactor) ELSE 0 END) ELSE 0 END)
													WHEN 3 -- NAMED PRICE
													THEN ISNULL(decDiscount,0)
												END
											WHEN 2 -- Add to primary vendor cost
											THEN
												CASE tintDiscountType 
													WHEN 1  -- PERCENT
													THEN @monVendorCost + (@monVendorCost * (ISNULL(decDiscount,0)/100))
													WHEN 2 -- FLAT AMOUNT
													THEN (CASE WHEN (@monVendorCost * @numQty * @fltUOMConversionFactor) + ISNULL(decDiscount,0) > 0 THEN ((@monVendorCost * @numQty * @fltUOMConversionFactor) + ISNULL(decDiscount,0)) / (@numQty * @fltUOMConversionFactor)  ELSE 0 END)
													WHEN 3 -- NAMED PRICE
													THEN ISNULL(decDiscount,0)
												END
											WHEN 3 -- Named price
											THEN
												CASE 
												WHEN ISNULL(@tintPriceLevel,0) > 0
												THEN
													(SELECT 
															ISNULL(decDiscount,0)
													FROM
													(
														SELECT 
															ROW_NUMBER() OVER(PARTITION BY PricingTable.numItemCode ORDER BY numPricingID) Id,
															decDiscount
														FROM 
															PricingTable 
														WHERE 
															PricingTable.numItemCode = @numItemCode 
															AND tintRuleType = 3 
													) TEMP
													WHERE
														Id = @tintPriceLevel)
												ELSE
													ISNULL(decDiscount,0)
												END
											END
										) 
				FROM
					PricingTable
				WHERE
					numPriceRuleID=@numPriceRuleID AND @numQty * @fltUOMConversionFactor >= intFromQty AND @numQty * @fltUOMConversionFactor <= intToQty
			END
			ELSE IF @tintPricingMethod = 2 -- BASED ON RULE
			BEGIN
				SET @tintRuleType = @tintPriceRuleType
				SET @monPriceRulePrice = (CASE @tintPriceRuleType WHEN 1 THEN @monListPrice WHEN 2 THEN @monVendorCost END) 

				IF (@tintPriceBookDiscountType = 1 ) -- Percentage 
				BEGIN
					SET @tintDisountType = @tintPriceBookDiscountType
					SET @decDiscount = @decDiscount * (@numQty/@intQntyItems);
					SET @decDiscount = @decDiscount * (CASE WHEN @tintPriceRuleType = 2 THEN @monVendorCost ELSE @monListPrice END) /100;
					SET @decMaxDedPerAmt = @decMaxDedPerAmt * (CASE WHEN @tintPriceRuleType = 2 THEN @monVendorCost ELSE @monListPrice END) /100;
					
					IF (@decDiscount > @decMaxDedPerAmt)
						SET @decDiscount = @decMaxDedPerAmt;
				
					IF ( @tintPriceRuleType = 1 )
							SET @monPriceRuleFinalPrice = (@monListPrice - @decDiscount);
					IF ( @tintPriceRuleType = 2 )
							SET @monPriceRuleFinalPrice = (@monVendorCost + @decDiscount);
				END
				IF ( @tintPriceBookDiscountType = 2 ) -- Flat discount 
				BEGIN
					SET @decDiscount = @decDiscount * ((@numQty * @fltUOMConversionFactor)/@intQntyItems);

					IF (@decDiscount > @decMaxDedPerAmt)
						SET @decDiscount = @decMaxDedPerAmt;
					
					IF ( @tintPriceRuleType = 1 )
						SET @monPriceRuleFinalPrice =  ((@monListPrice * @numQty * @fltUOMConversionFactor) - @decDiscount)/(@numQty * @fltUOMConversionFactor);
					IF ( @tintPriceRuleType = 2 )
						SET @monPriceRuleFinalPrice = ((@monVendorCost * @numQty * @fltUOMConversionFactor) + @decDiscount)/(@numQty * @fltUOMConversionFactor);
				END
			END
		END

		If @monPriceRuleFinalPrice > 0
		BEGIN
			SET @monPrice = @monPriceRuleFinalPrice
		END
		ELSE
		BEGIN
			SET @monPrice = @monVendorCost
		END


		SET @monPriceLevelPrice = 0
		SET @monPriceFinalPrice = 0
		SET @monPriceRulePrice = 0
		SET @monPriceRuleFinalPrice = 0
		SET @monLastPrice = 0
		SET @dtLastOrderDate = NULL
		SET @tintDisountType = 1
		SET @decDiscount =0 
	END


	

	SELECT
		@vcItemName AS vcItemName,
		@bitDropship AS bitDropship,
		@chrItemType AS ItemType,
		@bitMatrix AS bitMatrix,
		@numItemClassification AS ItemClassification,
		@numItemGroup AS ItemGroup,
		@ItemDesc AS ItemDescription,
		@bitAssembly AS bitAssembly,
		case when ISNULL(@bitAssembly,0)=1 then dbo.fn_GetAssemblyPossibleWOQty(@numItemCode) else 0 end AS MaxWorkOrderQty,
		@tintPricingBasedOn AS tintPriceBaseOn,
		@monPrice AS monPrice,
		@fltUOMConversionFactor AS UOMConversionFactor,
		(CASE WHEN @tintOppType=1 THEN @monListPrice ELSE @monVendorCost END) AS monListPrice,
		@monVendorCost AS VendorCost,
		@numVendorID AS numVendorID,
		@tintRuleType AS tintRuleType,
		@monPriceLevelPrice AS monPriceLevel,
		@monPriceFinalPrice AS monPriceLevelFinalPrice,
		@monPriceRulePrice AS monPriceRule,
		@monPriceRuleFinalPrice AS monPriceRuleFinalPrice,
		@monLastPrice AS monLastPrice,
		@dtLastOrderDate AS LastOrderDate,
		@tintDisountType AS DiscountType,
		@decDiscount AS Discount,
		@numBaseUnit AS numBaseUnit,
		@numSaleUnit AS numSaleUnit,
		@numPurchaseUnit AS numPurchaseUnit,
		dbo.fn_GetUOMName(@numBaseUnit) AS vcBaseUOMName,
		dbo.fn_GetUOMName(@numSaleUnit) AS vcSaleUOMName,
		@numPurchaseUnit AS numPurchaseUnit,
		@numWarehouseItemID AS numWarehouseItemID,
		dbo.fn_GetItemTransitCount(@numItemcode,@numDomainID) AS OrdersInTransist,
		(SELECT COUNT(*) FROM SimilarItems SI INNER JOIN Item I ON I.numItemCode = SI.numItemCode WHERE SI.numDomainId = @numDomainID AND SI.numParentItemCode = @numItemCode) AS RelatedItemsCount,
		@tintPriceLevel AS tintPriceLevel,
		@vcMinOrderQty AS vcMinOrderQty,
		@vcVendorNotes AS vcVendorNotes


	-- CHECK IF PROMOTION CAN BE TRIGGERED FROM THIS ITEM
	DECLARE @numPromotionID AS NUMERIC(18,0)

	SELECT TOP 1
		@numPromotionID = numProId 
	FROM 
		PromotionOffer PO
	WHERE 
		numDomainId=@numDomainID 
		AND ISNULL(bitEnabled,0)=1
		AND ISNULL(bitApplyToInternalOrders,0)=1 
		AND 1 = (CASE WHEN bitRequireCouponCode = 1 THEN CASE WHEN ISNULL(intCouponCodeUsed,0) < ISNULL(tintUsageLimit,0) OR ISNULL(tintUsageLimit,0) = 0  THEN 1 ELSE 0 END ELSE 1 END)
		AND 1 = (CASE WHEN bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN CAST(GETDATE() AS DATE)>=dtValidFrom AND CAST(GETDATE() AS DATE)<=dtValidTo THEN 1 ELSE 0 END) END)
		AND 1 = (CASE 
					WHEN tintOfferBasedOn = 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=1 AND numValue=@numItemCode) > 0 THEN 1 ELSE 0 END)
					WHEN tintOfferBasedOn = 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=2 AND numValue=@numItemClassification) > 0 THEN 1 ELSE 0 END)
					ELSE 0
				END)
	ORDER BY
		CASE 
			WHEN ISNULL(PO.bitRequireCouponCode,0)=1 AND PO.txtCouponCode=@vcCoupon THEN 1
			WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 1 THEN 2
			WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 1 THEN 3
			WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 2 THEN 4
			WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 2 THEN 5
			WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 3 THEN 6
			WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 3 THEN 7
		END


	IF ISNULL(@numPromotionID,0) > 0
	BEGIN
		-- Get Top 1 Promotion Based on priority

		SELECT 
			numProId
			,vcProName
			,bitNeverExpires
			,bitRequireCouponCode
			,txtCouponCode
			,tintUsageLimit
			,intCouponCodeUsed
			,bitFreeShiping
			,monFreeShippingOrderAmount
			,numFreeShippingCountry
			,bitFixShipping1
			,monFixShipping1OrderAmount
			,monFixShipping1Charge
			,bitFixShipping2
			,monFixShipping2OrderAmount
			,monFixShipping2Charge
			,tintOfferTriggerValueType
			,fltOfferTriggerValue
			,tintOfferBasedOn
			,fltDiscountValue
			,tintDiscountType
			,tintDiscoutBaseOn
			,dbo.FormatedDateFromDate(dtValidTo,@numDomainID) AS dtExpire
			,(CASE tintOfferBasedOn
				WHEN 1 THEN (SELECT Stuff((SELECT CONCAT(',',numValue) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=1 FOR XML PATH('')), 1, 1, ''))
				WHEN 2 THEN (SELECT Stuff((SELECT CONCAT(',',numValue) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=2 FOR XML PATH('')), 1, 1, ''))
			END) As vcPromotionItems
			,(CASE tintDiscoutBaseOn
				WHEN 1 THEN (SELECT Stuff((SELECT CONCAT(',',numValue) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=6 AND tintType=1 FOR XML PATH('')), 1, 1, ''))
				WHEN 2 THEN (SELECT Stuff((SELECT CONCAT(',',numValue) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=6 AND tintType=2 FOR XML PATH('')), 1, 1, ''))
				WHEN 3 THEN (SELECT Stuff((SELECT CONCAT(',',SimilarItems.numItemCode) FROM SimilarItems WHERE 1 = (CASE 
																														WHEN tintOfferBasedOn=1 THEN CASE WHEN numParentItemCode IN (SELECT numValue FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=1) THEN 1 ELSE 0 END
																														WHEN tintOfferBasedOn=2 THEN CASE WHEN numParentItemCode IN (SELECT numItemCode FROM Item WHERE numDomainID=@numDomainID AND numItemClassification IN (SELECT numValue FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=2)) THEN 1 ELSE 0 END
																														ELSE 0
																													END) FOR XML PATH('')), 1, 1, ''))
			END) As vcItems
			,CONCAT('Buy '
					,CASE WHEN tintOfferTriggerValueType=1 THEN CAST(fltOfferTriggerValue AS VARCHAR) ELSE CONCAT('$',fltOfferTriggerValue) END
					,CASE tintOfferBasedOn
							WHEN 1 THEN CONCAT(' of "',(SELECT Stuff((SELECT CONCAT(', ',Item.vcItemName) FROM PromotionOfferItems INNER JOIN Item ON PromotionOfferItems.numValue = Item.numItemCode WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=1 FOR XML PATH('')), 1, 2, '')), '"')
							WHEN 2 THEN CONCAT(' of items belonging to classification(s) "',(SELECT Stuff((SELECT CONCAT(', ',ListDetails.vcData) FROM PromotionOfferItems INNER JOIN ListDetails ON PromotionOfferItems.numValue = ListDetails.numListItemID WHERE numListID=36 AND numProId=PO.numProId AND tintRecordType=5 AND tintType=2 FOR XML PATH('')), 1, 2, '')), '"')
						END
					,' & get '
					, CASE tintDiscountType WHEN 1 THEN CONCAT(fltDiscountValue,'% off on') WHEN 2 THEN CONCAT('$',fltDiscountValue, ' off on') WHEN 3 THEN CONCAT(fltDiscountValue,' of') END
					, CASE 
						WHEN tintDiscoutBaseOn = 1 THEN 
							CONCAT
							(
								CONCAT(' "',(SELECT Stuff((SELECT CONCAT(',',Item.vcItemName) FROM PromotionOfferItems INNER JOIN Item ON PromotionOfferItems.numValue = Item.numItemCode WHERE numProId=PO.numProId AND tintRecordType=6 AND tintType=1 FOR XML PATH('')), 1, 1, '')), '"')
								,(CASE WHEN PO.tintDiscountType = 3 THEN ' free' ELSE '' END)
								,'.'
							)
						WHEN tintDiscoutBaseOn = 2 THEN 
							CONCAT
							(
								CONCAT(' items belonging to classification(s) "',(SELECT Stuff((SELECT CONCAT(',',ListDetails.vcData) FROM PromotionOfferItems INNER JOIN ListDetails ON PromotionOfferItems.numValue = ListDetails.numListItemID WHERE numListID=36 AND numProId=PO.numProId AND tintRecordType=6 AND tintType=2 FOR XML PATH('')), 1, 1, '')), '"')
								,(CASE WHEN PO.tintDiscountType = 3 THEN ' free' ELSE '' END)
								,'.'
							)
						WHEN tintDiscoutBaseOn = 3 THEN ' related items.'
						ELSE '' 
					END
				) AS vcPromotionDescription
				,CONCAT
				(
					(CASE WHEN ISNULL(bitFixShipping1,0)=1 THEN CONCAT('Spend $',monFixShipping1OrderAmount,' and get $',monFixShipping1Charge,' shipping. ') ELSE '' END)
					,(CASE WHEN ISNULL(bitFixShipping2,0)=1  THEN CONCAT('Spend $',monFixShipping2OrderAmount,' and get $',monFixShipping2Charge,' shipping. ') ELSE '' END)
					,(CASE WHEN (bitFreeShiping=1 AND numFreeShippingCountry=@numShippingCountry) THEN CONCAT('Spend $',monFreeShippingOrderAmount,' and shipping is FREE! ') ELSE '' END) 
				) AS vcShippingDescription,
				CASE 
					WHEN ISNULL(PO.bitRequireCouponCode,0)=1 AND PO.txtCouponCode=@vcCoupon THEN 1
					WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 1 THEN 2
					WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 1 THEN 3
					WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 2 THEN 4
					WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 2 THEN 5
					WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 3 THEN 6
					WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 3 THEN 7
				END AS tintPriority
		FROM
			PromotionOffer PO
		WHERE
			numDomainId=@numDomainID 
			AND numProId = @numPromotionID
	END
END 
GO
