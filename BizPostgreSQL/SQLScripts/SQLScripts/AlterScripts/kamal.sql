/***************************************************************************/
/*********************** LAST UPDATE ON 04 SEPTEMBER 2015 ******************/
/***************************************************************************/


----SELECT * FROM [dbo].[DycFieldMaster] AS DFM WHERE [DFM].[vcFieldName] LIKE '%Compliation%'
--UPDATE [DycFieldMaster] SET [vcFieldName]='Completion Date' WHERE [vcFieldName] LIKE '%Compliation%'


----SELECT * FROM [dbo].DycFormField_Mapping AS DFM WHERE [DFM].[vcFieldName] LIKE '%Compliation%'
--UPDATE DycFormField_Mapping SET [vcFieldName]='Completion Date' WHERE [vcFieldName] LIKE '%Compliation%'



--ALTER TABLE dbo.Domain ADD
--	numPODropShipBizDoc numeric(18, 0) NULL,
--	numPODropShipBizDocTemplate numeric(18, 0) NULL



------/************************************************************************************************/
------/************************27_Dec_2013*******************************************************************/
------/************************************************************************************************/


--exec USP_GetPageNavigation @numModuleID=35,@numDomainID=1,@numGroupID=1


BEGIN TRANSACTION
	DECLARE @numPageNavID numeric 
	SELECT @numPageNavID = MAX(numPageNavID) + 1  FROM dbo.PageNavigationDTL 
	INSERT INTO dbo.PageNavigationDTL
			( numPageNavID ,
			  numModuleID ,
			  numParentID ,
			  vcPageNavName ,
			  vcNavURL ,
			  vcImageURL ,
			  bitVisible ,
			  numTabID
			)
	SELECT @numPageNavID ,35,93,'Other Reports',NULL,NULL,1,45 
	EXEC USP_CreateTreeNavigationForDomain 0,@numPageNavID --Give permission to tree node.
END TRANSACTION

BEGIN TRANSACTION
	DECLARE @numPageNavID numeric 
	SELECT @numPageNavID = MAX(numPageNavID) + 1  FROM dbo.PageNavigationDTL 
	INSERT INTO dbo.PageNavigationDTL
			( numPageNavID ,
			  numModuleID ,
			  numParentID ,
			  vcPageNavName ,
			  vcNavURL ,
			  vcImageURL ,
			  bitVisible ,
			  numTabID
			)
	SELECT @numPageNavID ,35,222,'Check Register','../Accounting/frmAccountsReports.aspx?RType=1',NULL,1,45 
	EXEC USP_CreateTreeNavigationForDomain 0,@numPageNavID --Give permission to tree node.
	
	SELECT @numPageNavID = MAX(numPageNavID) + 1  FROM dbo.PageNavigationDTL 
	INSERT INTO dbo.PageNavigationDTL
			( numPageNavID ,
			  numModuleID ,
			  numParentID ,
			  vcPageNavName ,
			  vcNavURL ,
			  vcImageURL ,
			  bitVisible ,
			  numTabID
			)
	SELECT @numPageNavID ,35,222,'Sales Journal Detail (By GL Account)','../Accounting/frmAccountsReports.aspx?RType=2',NULL,1,45 
	EXEC USP_CreateTreeNavigationForDomain 0,@numPageNavID --Give permission to tree node.
	
	SELECT @numPageNavID = MAX(numPageNavID) + 1  FROM dbo.PageNavigationDTL 
	INSERT INTO dbo.PageNavigationDTL
			( numPageNavID ,
			  numModuleID ,
			  numParentID ,
			  vcPageNavName ,
			  vcNavURL ,
			  vcImageURL ,
			  bitVisible ,
			  numTabID
			)
	SELECT @numPageNavID ,35,222,'Purchase Journal Detail (By GL Account)','../Accounting/frmAccountsReports.aspx?RType=3',NULL,1,45 
	EXEC USP_CreateTreeNavigationForDomain 0,@numPageNavID --Give permission to tree node.
	
	SELECT @numPageNavID = MAX(numPageNavID) + 1  FROM dbo.PageNavigationDTL 
	INSERT INTO dbo.PageNavigationDTL
			( numPageNavID ,
			  numModuleID ,
			  numParentID ,
			  vcPageNavName ,
			  vcNavURL ,
			  vcImageURL ,
			  bitVisible ,
			  numTabID
			)
	SELECT @numPageNavID ,35,222,'Invoice Register','../Accounting/frmAccountsReports.aspx?RType=4',NULL,1,45 
	EXEC USP_CreateTreeNavigationForDomain 0,@numPageNavID --Give permission to tree node.
	
	SELECT @numPageNavID = MAX(numPageNavID) + 1  FROM dbo.PageNavigationDTL 
	INSERT INTO dbo.PageNavigationDTL
			( numPageNavID ,
			  numModuleID ,
			  numParentID ,
			  vcPageNavName ,
			  vcNavURL ,
			  vcImageURL ,
			  bitVisible ,
			  numTabID
			)
	SELECT @numPageNavID ,35,222,'Receipt Journal (Summary)','../Accounting/frmAccountsReports.aspx?RType=5',NULL,1,45 
	EXEC USP_CreateTreeNavigationForDomain 0,@numPageNavID --Give permission to tree node.
	
	SELECT @numPageNavID = MAX(numPageNavID) + 1  FROM dbo.PageNavigationDTL 
	INSERT INTO dbo.PageNavigationDTL
			( numPageNavID ,
			  numModuleID ,
			  numParentID ,
			  vcPageNavName ,
			  vcNavURL ,
			  vcImageURL ,
			  bitVisible ,
			  numTabID
			)
	SELECT @numPageNavID ,35,222,'Receipt Journal (Detailed)','../Accounting/frmAccountsReports.aspx?RType=6',NULL,1,45 
	EXEC USP_CreateTreeNavigationForDomain 0,@numPageNavID --Give permission to tree node.
	
	SELECT @numPageNavID = MAX(numPageNavID) + 1  FROM dbo.PageNavigationDTL 
	INSERT INTO dbo.PageNavigationDTL
			( numPageNavID ,
			  numModuleID ,
			  numParentID ,
			  vcPageNavName ,
			  vcNavURL ,
			  vcImageURL ,
			  bitVisible ,
			  numTabID
			)
	SELECT @numPageNavID ,35,222,'Total Disbursement Journal (Summary)','../Accounting/frmAccountsReports.aspx?RType=7',NULL,1,45 
	EXEC USP_CreateTreeNavigationForDomain 0,@numPageNavID --Give permission to tree node.
	
	SELECT @numPageNavID = MAX(numPageNavID) + 1  FROM dbo.PageNavigationDTL 
	INSERT INTO dbo.PageNavigationDTL
			( numPageNavID ,
			  numModuleID ,
			  numParentID ,
			  vcPageNavName ,
			  vcNavURL ,
			  vcImageURL ,
			  bitVisible ,
			  numTabID
			)
	SELECT @numPageNavID ,35,222,'Total Disbursement Journal (Detailed)','../Accounting/frmAccountsReports.aspx?RType=8',NULL,1,45 
	EXEC USP_CreateTreeNavigationForDomain 0,@numPageNavID --Give permission to tree node.
END TRANSACTION


------/************************************************************************************************/
------/************************4_Dec_2013 and 16_Dec_2013_Workflow*******************************************************************/
------/************************************************************************************************/

--UPDATE dbo.DycFieldMaster SET vcFieldDataType='M' WHERE numFieldId=99
--
--ALTER TABLE dbo.ReturnHeader ADD
--	numDepositIDRef numeric(18, 0) NULL,
--	numBillPaymentIDRef numeric(18, 0) NULL
--	
--
--ALTER TABLE dbo.BillPaymentHeader ADD
--	monRefundAmount money NULL	
--	
--	
--ALTER TABLE dbo.DepositMaster ADD
--	monRefundAmount money NULL	
--	
--	
--	
--
--DECLARE @v sql_variant 
--SET @v = N'0: BizForm Wizards
--1: Regular Form (Grid Settings)
--2: Other Form
--3: WorkFlow Form'
--EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'DynamicFormMaster', N'COLUMN', N'tintFlag'
--
--
--
--INSERT INTO dbo.DynamicFormMaster (numFormId,vcFormName,cCustomFieldsAssociated,
--	cAOIAssociated,bitDeleted,tintFlag,bitWorkFlow,vcLocationID,bitAllowGridColor)
--SELECT 68,'Organization','Y','N',0,3,0,'1,12,13,14',0 UNION
--SELECT 69,'Contacts','Y','N',0,3,0,'4',0 UNION
--SELECT 70,'Opportunities & Orders','Y','N',0,3,0,'2,6',0 UNION
--SELECT 71,'BizDocs','Y','N',0,3,0,'',0 UNION
--SELECT 72,'Cases','Y','N',0,3,0,'3',0 UNION
--SELECT 73,'Projects','Y','N',0,3,0,'11',0 UNION
--SELECT 74,'Items','Y','N',0,3,0,'5',0
--
--
--
--
--CREATE TABLE [dbo].[WorkFlowMaster](
--	[numWFID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
--	[numDomainID] [numeric](18, 0) NULL,
--	[vcWFName] [varchar](200) NULL,
--	[vcWFDescription] [varchar](500) NULL,
--	[numCreatedBy] [numeric](18, 0) NULL,
--	[numModifiedBy] [numeric](18, 0) NULL,
--	[dtCreatedDate] [datetime] NULL,
--	[dtModifiedDate] [datetime] NULL,
--	[bitActive] [bit] NULL,
--	[numFormID] [numeric](18, 0) NULL,
--	[tintWFTriggerOn] [tinyint] NULL,
-- CONSTRAINT [PK_WorkFlowMaster] PRIMARY KEY CLUSTERED 
--(
--	[numWFID] ASC
--)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
--) ON [PRIMARY]
--
--GO
--SET ANSI_PADDING ON
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1:Create
--2:Edit
--3:Create/Edit
--4:Field Update
--5:Delete
--6:Date Field Condition' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WorkFlowMaster', @level2type=N'COLUMN',@level2name=N'tintWFTriggerOn'
--
--
--
--CREATE TABLE [dbo].[WorkFlowTriggerFieldList](
--	[numWFTriggerFieldID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
--	[numWFID] [numeric](18, 0) NULL,
--	[numFieldID] [numeric](18, 0) NULL,
--	[bitCustom] [bit] NOT NULL,
-- CONSTRAINT [PK_WorkFlowTriggerFieldList] PRIMARY KEY CLUSTERED 
--(
--	[numWFTriggerFieldID] ASC
--)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
--) ON [PRIMARY]
--
--GO
--ALTER TABLE [dbo].[WorkFlowTriggerFieldList]  WITH CHECK ADD  CONSTRAINT [FK_WorkFlowTriggerFieldList_WorkFlowMaster] FOREIGN KEY([numWFID])
--REFERENCES [dbo].[WorkFlowMaster] ([numWFID])
--GO
--ALTER TABLE [dbo].[WorkFlowTriggerFieldList] CHECK CONSTRAINT [FK_WorkFlowTriggerFieldList_WorkFlowMaster]
--
--
--
--CREATE TABLE [dbo].[WorkFlowConditionList](
--	[numWFConditionID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
--	[numWFID] [numeric](18, 0) NULL,
--	[numFieldID] [numeric](18, 0) NULL,
--	[bitCustom] [bit] NULL,
--	[vcFilterValue] [varchar](500) NULL,
--	[vcFilterOperator] [varchar](10) NULL,
--	[vcFilterANDOR] [varchar](10) NULL,
-- CONSTRAINT [PK_WorkFlowConditionList] PRIMARY KEY CLUSTERED 
--(
--	[numWFConditionID] ASC
--)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
--) ON [PRIMARY]
--
--GO
--ALTER TABLE [dbo].[WorkFlowConditionList]  WITH CHECK ADD  CONSTRAINT [FK_WorkFlowConditionList_WorkFlowMaster] FOREIGN KEY([numWFID])
--REFERENCES [dbo].[WorkFlowMaster] ([numWFID])
--GO
--ALTER TABLE [dbo].[WorkFlowConditionList] CHECK CONSTRAINT [FK_WorkFlowConditionList_WorkFlowMaster]
--
--
--
--CREATE TABLE [dbo].[WorkFlowActionList](
--	[numWFActionID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
--	[numWFID] [numeric](18, 0) NULL,
--	[tintActionType] [tinyint] NULL,
--	[numTemplateID] [numeric](18, 0) NULL,
--	[vcEmailToType] [varchar](50) NULL,
--	[tintTicklerActionAssignedTo] [tinyint] NULL,
--	[vcAlertMessage] [ntext] NULL,
--	[tintActionOrder] [tinyint] NULL,
-- CONSTRAINT [PK_WorkFlowActionList] PRIMARY KEY CLUSTERED 
--(
--	[numWFActionID] ASC
--)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
--) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
--
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1 : Email Alerts
--2 : Action Items
--3 : Update Fields
--4:  Alert Message' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WorkFlowActionList', @level2type=N'COLUMN',@level2name=N'tintActionType'
--GO
--ALTER TABLE [dbo].[WorkFlowActionList]  WITH CHECK ADD  CONSTRAINT [FK_WorkFlowActionList_WorkFlowMaster] FOREIGN KEY([numWFID])
--REFERENCES [dbo].[WorkFlowMaster] ([numWFID])
--GO
--ALTER TABLE [dbo].[WorkFlowActionList] CHECK CONSTRAINT [FK_WorkFlowActionList_WorkFlowMaster]
--
--
--
--CREATE TABLE [dbo].[WorkFlowActionUpdateFields](
--	[numWFActionUFID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
--	[numWFActionID] [numeric](18, 0) NULL,
--	[tintModuleType] [numeric](18, 0) NULL,
--	[numFieldID] [numeric](18, 0) NULL,
--	[bitCustom] [bit] NULL,
--	[vcValue] [varchar](500) NULL,
-- CONSTRAINT [PK_WorkFlowConditionUpdateFields] PRIMARY KEY CLUSTERED 
--(
--	[numWFActionUFID] ASC
--)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
--) ON [PRIMARY]
--
--GO
--ALTER TABLE [dbo].[WorkFlowActionUpdateFields]  WITH CHECK ADD  CONSTRAINT [FK_WorkFlowActionUpdateFields_WorkFlowActionList] FOREIGN KEY([numWFActionID])
--REFERENCES [dbo].[WorkFlowActionList] ([numWFActionID])
--GO
--ALTER TABLE [dbo].[WorkFlowActionUpdateFields] CHECK CONSTRAINT [FK_WorkFlowActionUpdateFields_WorkFlowActionList]
--
--
--
--CREATE TABLE [dbo].[WorkFlowQueue](
--	[numWFQueueID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
--	[numDomainID] [numeric](18, 0) NOT NULL,
--	[numCreatedBy] [numeric](18, 0) NOT NULL,
--	[dtCreatedDate] [datetime] NOT NULL,
--	[numRecordID] [numeric](18, 0) NOT NULL,
--	[numFormID] [numeric](18, 0) NOT NULL,
--	[tintProcessStatus] [tinyint] NOT NULL,
--	[tintWFTriggerOn] [tinyint] NULL,
--	[numWFID] [numeric](18, 0) NULL,
-- CONSTRAINT [PK_WorkFlowQueue] PRIMARY KEY CLUSTERED 
--(
--	[numWFQueueID] ASC
--)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
--) ON [PRIMARY]
--
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1:Pending Execution 2:In Progress 3:Success 4:Failed 5:No WF Found' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WorkFlowQueue', @level2type=N'COLUMN',@level2name=N'tintProcessStatus'
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1:Create
--2:Edit
--3:Create/Edit
--4:Field Update
--5:Delete
--6:Date Field Condition' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'WorkFlowQueue', @level2type=N'COLUMN',@level2name=N'tintWFTriggerOn'
--
--
--
--CREATE TABLE [dbo].[WorkFlowQueueExecution](
--	[numWFQueueExeID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
--	[numWFQueueID] [numeric](18, 0) NOT NULL,
--	[numWFID] [numeric](18, 0) NOT NULL,
--	[dtExecutionDate] [datetime] NOT NULL,
--	[bitSuccess] [bit] NOT NULL,
--	[vcDescription] [varchar](1000) NULL,
-- CONSTRAINT [PK_WorkFlowQueueExecution] PRIMARY KEY CLUSTERED 
--(
--	[numWFQueueExeID] ASC
--)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
--) ON [PRIMARY]
--
--
--
--CREATE TABLE [dbo].[Audit](
--	[AuditID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
--	[Type] [char](1) NULL,
--	[TableName] [varchar](128) NULL,
--	[PK] [varchar](1000) NULL,
--	[FieldName] [varchar](128) NULL,
--	[OldValue] [varchar](1000) NULL,
--	[NewValue] [varchar](1000) NULL,
--	[UpdateDate] [datetime] NULL,
--	[UserName] [varchar](128) NULL,
--	[numRecordID] [numeric](18, 0) NULL,
--	[numDomainID] [numeric](18, 0) NULL,
--	[AuditGUID] [uniqueidentifier] NULL,
-- CONSTRAINT [PK_Audit] PRIMARY KEY CLUSTERED 
--(
--	[AuditID] ASC
--)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
--) ON [PRIMARY]
--
--
--SELECT * FROM dbo.DycFormField_Mapping WHERE numFormID=70
--
--
--INSERT INTO [dbo].[DycFormField_Mapping]([numModuleID], [numFieldID], [numDomainID], [numFormID], [bitAllowEdit], [bitInlineEdit], [vcFieldName], [vcAssociatedControlType], [vcPropertyName], [PopupFunctionName], [order], [tintRow], [tintColumn], [bitInResults], [bitDeleted], [bitDefault], [bitSettingField], [bitAddField], [bitDetailField], [bitAllowSorting], [bitWorkFlowField], [bitImport], [bitExport], [bitAllowFiltering], [bitRequired], [numFormFieldID], [intSectionID], [bitAllowGridColor])
--SELECT 3, 96, NULL, 70, 1, 0, N'Sales Order Name', N'TextBox', N'OpportunityName', NULL, 1, 1, 1, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, NULL, 1876, NULL, 0 UNION ALL
--SELECT 3, 97, NULL, 70, 1, 0, N'Source', N'SelectBox', N'Source', NULL, 2, 5, 1, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, NULL, 1877, NULL, 0 UNION ALL
--SELECT 3, 99, NULL, 70, 0, 0, N'Amount', N'TextBox', N'Amount', NULL, 4, 10, 1, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, NULL, 1879, NULL, 0 UNION ALL
--SELECT 3, 100, NULL, 70, 1, 0, N'Assigned To', N'SelectBox', N'AssignedTo', NULL, 5, 2, 2, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, NULL, 1880, NULL, 0 UNION ALL
--SELECT 3, 101, NULL, 70, 1, 0, N'Order Status', N'SelectBox', N'OrderStatus', NULL, 6, 13, 1, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, NULL, 1881, NULL, 0 UNION ALL
--SELECT 3, 103, NULL, 70, 1, 0, N'Conclusion Reason', N'SelectBox', N'ConAnalysis', NULL, 8, 7, 1, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, NULL, 1883, NULL, 0 UNION ALL
--SELECT 3, 104, NULL, 70, 0, 0, N'Order Sub-total', N'TextBox', N'CalcAmount', NULL, 9, 9, 2, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, NULL, 1884, NULL, 0 UNION ALL
--SELECT 3, 105, NULL, 70, 1, 0, N'Active', N'CheckBox', N'Active', NULL, 10, 8, 1, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, NULL, 1885, NULL, 0 UNION ALL
--SELECT 3, 108, NULL, 70, 1, 0, N'Estimated close date', N'DateField', N'EstimatedCloseDate', NULL, 13, 4, 1, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, NULL, 1888, NULL, 0 UNION ALL
--SELECT 3, 109, NULL, 70, 0, 0, N'Deal Status', N'SelectBox', N'DealStatus', NULL, 14, 15, 1, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, NULL, 1889, NULL, 0 UNION ALL
--SELECT 3, 111, NULL, 70, 1, 0, N'Record Owner', N'SelectBox', NULL, NULL, 16, NULL, NULL, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, NULL, 1891, NULL, 0 UNION ALL
--SELECT 3, 112, NULL, 70, 0, 0, N'Assigned By', N'SelectBox', NULL, NULL, 17, NULL, NULL, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, NULL, 1892, NULL, 0 UNION ALL
--SELECT 3, 116, NULL, 70, 1, 0, N'Campaign', N'SelectBox', NULL, NULL, 1, 1, 1, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, NULL, 0, NULL, 0 UNION ALL
--SELECT 3, 117, NULL, 70, 1, 0, N'Closing Date', N'DateField', NULL, NULL, 22, NULL, NULL, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, NULL, 1897, NULL, 0 UNION ALL
--SELECT 3, 118, NULL, 70, 0, 0, N'Opp Type', N'SelectBox', NULL, NULL, 23, NULL, NULL, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, NULL, 1898, NULL, 0 UNION ALL
--SELECT 3, 119, NULL, 70, 0, 0, N'Created Date', N'DateField', NULL, NULL, 24, NULL, NULL, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, NULL, 1899, NULL, 0 UNION ALL
--SELECT 3, 122, NULL, 70, 1, 0, N'Comments', N'TextArea', N'OppComments', NULL, 28, 27, 1, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, NULL, 1903, NULL, 0 UNION ALL
--SELECT 3, 230, NULL, 70, 0, 0, N'Sales Type', N'SelectBox', N'SalesorPurType', NULL, 3, 6, 2, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, NULL, 1878, NULL, 0 UNION ALL
--SELECT 3, 268, NULL, 70, 0, 0, N'Invoice Grand-total', N'TextBox', N'InvoiceGrandTotal', NULL, 31, 31, 1, NULL, NULL, NULL, 0, 0, 0, 0, 1, 0, 0, 0, NULL, NULL, NULL, 0 UNION ALL
--SELECT 3, 269, NULL, 70, 0, 0, N'Total Amount Paid', N'TextBox', N'TotalAmountPaid', NULL, 32, 31, 2, NULL, NULL, NULL, 0, 0, 0, 0, 1, 0, 0, 0, NULL, NULL, NULL, 0 UNION ALL
--SELECT 3, 448, NULL, 70, 1, 0, N'Customer PO#', N'TextBox', N'OppRefOrderNo', NULL, 15, 13, 2, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, NULL, 1890, NULL, 0 UNION ALL
--SELECT 3, 464, NULL, 70, 0, 0, N'Marketplace Order ID', N'TextBox', N'MarketplaceOrderID', N'', 1, 1, 2, NULL, NULL, NULL, 0, 0, 0, 0, 1, 0, 0, 1, NULL, NULL, NULL, 0


------/************************************************************************************************/
------/************************30_Oct_2013*******************************************************************/
------/************************************************************************************************/

--ALTER TABLE dbo.CFW_Loc_Master ADD
--	vcCustomLookBackTableName varchar(50) NULL
--
----select * from CFW_Loc_Master
--UPDATE CFW_Loc_Master SET vcCustomLookBackTableName='CFW_FLD_Values' WHERE Loc_Id=1
--UPDATE CFW_Loc_Master SET vcCustomLookBackTableName='CFW_Fld_Values_Opp' WHERE Loc_Id=2
--UPDATE CFW_Loc_Master SET vcCustomLookBackTableName='CFW_FLD_Values_Case' WHERE Loc_Id=3
--UPDATE CFW_Loc_Master SET vcCustomLookBackTableName='CFW_FLD_Values_Cont' WHERE Loc_Id=4
--UPDATE CFW_Loc_Master SET vcCustomLookBackTableName='CFW_FLD_Values_Item' WHERE Loc_Id=5
--UPDATE CFW_Loc_Master SET vcCustomLookBackTableName='CFW_Fld_Values_Opp' WHERE Loc_Id=6
--UPDATE CFW_Loc_Master SET vcCustomLookBackTableName='CFW_Fld_Values_Serialized_Items' WHERE Loc_Id=9
--UPDATE CFW_Loc_Master SET vcCustomLookBackTableName='CFW_FLD_Values_Pro' WHERE Loc_Id=11
--UPDATE CFW_Loc_Master SET vcCustomLookBackTableName='CFW_FLD_Values' WHERE Loc_Id=12
--UPDATE CFW_Loc_Master SET vcCustomLookBackTableName='CFW_FLD_Values' WHERE Loc_Id=13
--UPDATE CFW_Loc_Master SET vcCustomLookBackTableName='CFW_FLD_Values' WHERE Loc_Id=14

------/************************************************************************************************/
------/************************26_Oct_2013*******************************************************************/
------/************************************************************************************************/
----------Portal--------
--
----11	Commission Report	11
--DELETE FROM PortalDashboardDtl WHERE numReportID=11
--DELETE FROM PortalDashboardReports WHERE numreportid=11
--
----6	Items awaiting return	5
--DELETE FROM PortalDashboardDtl WHERE numReportID=6
--DELETE FROM PortalDashboardReports WHERE numreportid=6
--
--
--
----------Move to New Reports & Dashboard--------
--
----SELECT * FROM TabMaster
----DashBoard/frmDashBoard.aspx -> ReportDashboard/frmNewDashBoard.aspx
--UPDATE TabMaster SET vcURL='ReportDashboard/frmNewDashBoard.aspx' WHERE numTabId=68
--
----SELECT * FROM  ShortCutBar WHERE numTabId=6
----../reports/frmCustomRptList.aspx -> ../reports/frmCustomReportList.aspx
--UPDATE ShortCutBar SET link='../reports/frmCustomReportList.aspx' WHERE id=130 AND numTabId=6
--
----../DashBoard/frmDashBoard.aspx -> ../ReportDashboard/frmNewDashBoard.aspx
--UPDATE ShortCutBar SET link='../ReportDashboard/frmNewDashBoard.aspx' WHERE id=135 AND numTabId=68
--
----SELECT * FROM PageNavigationDTL WHERE numModuleID=13
----Remove Administration -> User Administration -> Dashboard
--UPDATE PageNavigationDTL SET bitVisible=0 WHERE numPageNavID=91 AND numModuleID=13
--
----SELECT * FROM PageNavigationDTL WHERE numModuleID=8
----../DashBoard/frmDashBoard.aspx -> ../ReportDashboard/frmNewDashBoard.aspx
--UPDATE PageNavigationDTL SET vcNavURL='../ReportDashboard/frmNewDashBoard.aspx' WHERE numPageNavID=23 AND numModuleID=8
----../reports/frmCustomRptList.aspx -> ../reports/frmCustomReportList.aspx
--UPDATE PageNavigationDTL SET vcNavURL='../reports/frmCustomReportList.aspx' WHERE numPageNavID=92 AND numModuleID=8

----/************************************************************************************************/
----/************************23_Aug_2013*******************************************************************/
----/************************************************************************************************/
--
--ALTER TABLE dbo.DivisionMaster ADD
--	numDefaultPaymentMethod numeric(18, 0) NULL,
--	numDefaultCreditCard numeric(18, 0) NULL,
--	bitOnCreditHold bit NULL
--	
--	
----SELECT * FROM dbo.ReturnHeader WHERE numDomainId=172 AND tintReturnType=1 and numBizDocTempID=978
----UPDATE ReturnHeader SET numBizDocTempID=974 WHERE numDomainId=172 AND tintReturnType=1 and numBizDocTempID=978
--	
--	
--
----SELECT * FROM ReportFieldGroupMappingMaster where vcFieldName like '%Date%' AND numReportFieldGroupID=3
--
--UPDATE ReportFieldGroupMappingMaster SET vcFieldName='Order Created Date' WHERE vcFieldName like '%Created Date%' AND numReportFieldGroupID=3
--UPDATE ReportFieldGroupMappingMaster SET vcFieldName='Order Due Date' WHERE vcFieldName like '%Due Date%' AND numReportFieldGroupID=3
--UPDATE ReportFieldGroupMappingMaster SET vcFieldName='Order Estimated Created Date' WHERE vcFieldName like '%Closing Date%' AND numReportFieldGroupID=3
--	
--UPDATE ReportFieldGroupMappingMaster SET vcFieldName='BizDoc Billing Date' WHERE vcFieldName like '%Billing Date%' AND numReportFieldGroupID=5
--UPDATE ReportFieldGroupMappingMaster SET vcFieldName='BizDoc Created Date' WHERE vcFieldName like '%Created Date%' AND numReportFieldGroupID=5
--UPDATE ReportFieldGroupMappingMaster SET vcFieldName='BizDoc Due Date' WHERE vcFieldName like '%Due Date%' AND numReportFieldGroupID=5
--UPDATE ReportFieldGroupMappingMaster SET vcFieldName='BizDoc Modified Date' WHERE vcFieldName like '%Modified Date%' AND numReportFieldGroupID=5
--	
--	
--	
----SELECT * FROM ReportFieldGroupMaster
--INSERT INTO [dbo].[ReportFieldGroupMaster]([vcFieldGroupName], [bitActive], [bitCustomFieldGroup], [numGroupID], [vcCustomTableName])
--SELECT N'Leads Custom Field', 1, 1, 14, N'CFW_FLD_Values' UNION ALL
--SELECT N'Prospect Custom Field', 1, 1, 12, N'CFW_FLD_Values' UNION ALL
--SELECT N'Account Custom Field', 1, 1, 13, N'CFW_FLD_Values'	
--
--
----SELECT * FROM ReportModuleGroupFieldMappingMaster WHERE numReportFieldGroupID=15
--INSERT INTO [dbo].[ReportModuleGroupFieldMappingMaster]([numReportModuleGroupID], [numReportFieldGroupID])
--SELECT 1, 27 UNION ALL
--SELECT 5, 27 UNION ALL
--SELECT 14, 27 UNION ALL
--SELECT 16, 27 UNION ALL
--SELECT 1, 28 UNION ALL
--SELECT 5, 28 UNION ALL
--SELECT 14, 28 UNION ALL
--SELECT 16, 28 UNION ALL
--SELECT 1, 29 UNION ALL
--SELECT 5, 29 UNION ALL
--SELECT 14, 29 UNION ALL
--SELECT 16, 29 
--
-----Add Custom Fields to Other Module(Orders)
--INSERT INTO [dbo].[ReportModuleGroupFieldMappingMaster]([numReportModuleGroupID], [numReportFieldGroupID])
--SELECT 8, 15 UNION ALL
--SELECT 8, 27 UNION ALL
--SELECT 8, 28 UNION ALL
--SELECT 8, 29 UNION ALL
--SELECT 9, 15 UNION ALL
--SELECT 9, 27 UNION ALL
--SELECT 9, 28 UNION ALL
--SELECT 9, 29 UNION ALL
--SELECT 10, 15 UNION ALL
--SELECT 10, 27 UNION ALL
--SELECT 10, 28 UNION ALL
--SELECT 10, 29 UNION ALL
--SELECT 13, 15 UNION ALL
--SELECT 13, 27 UNION ALL
--SELECT 13, 28 UNION ALL
--SELECT 13, 29
----=========================================================================================	
--DECLARE @v sql_variant 
--SET @v = N'0: BizForm Wizards
--1: Regular Form (Grid Settings)
--2: Other Form
--3: WorkFlow Form'
--EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'DynamicFormMaster', N'COLUMN', N'tintFlag'
--
--
--INSERT INTO dbo.DynamicFormMaster (numFormId,vcFormName,cCustomFieldsAssociated,
--	cAOIAssociated,bitDeleted,tintFlag,bitWorkFlow,vcLocationID,bitAllowGridColor)
--SELECT 68,'Organization','Y','N',0,3,0,'1,12,13,14',0 UNION
--SELECT 69,'Contacts','Y','N',0,3,0,'4',0 UNION
--SELECT 70,'Opportunities & Orders','Y','N',0,3,0,'2,6',0 UNION
--SELECT 71,'BizDocs','Y','N',0,3,0,'',0 UNION
--SELECT 72,'Cases','Y','N',0,3,0,'3',0 UNION
--SELECT 73,'Projects','Y','N',0,3,0,'11',0 UNION
--SELECT 74,'Items','Y','N',0,3,0,'5',0
--
----=========================================================================================
--
--ALTER TABLE dbo.OpportunityBizDocs ADD
--	bitAutoCreated bit NULL	 
--	
--UPDATE OpportunityBizDocs SET bitAutoCreated=0
--
--UPDATE dbo.OpportunityBizDocs SET bitAutoCreated=1 WHERE numOppBizDocsId IN (
--SELECT numOppBizDocsId FROM dbo.OpportunityAutomationQueueExecution WHERE numRuleID IN (1,2))
--
--
----=========================================================================================
--
----SELECT * FROM dbo.ReportFieldGroupMaster
--INSERT INTO [dbo].[ReportFieldGroupMaster]([vcFieldGroupName], [bitActive], [bitCustomFieldGroup], [numGroupID], [vcCustomTableName])
--SELECT N'Item Price Level', 1, NULL, NULL, NULL
--
----SELECT *FROM dbo.ReportModuleGroupMaster
--INSERT INTO [dbo].[ReportModuleGroupFieldMappingMaster]([numReportModuleGroupID], [numReportFieldGroupID])
--SELECT 19, 30 
--
--INSERT INTO [dbo].[ReportModuleGroupFieldMappingMaster]([numReportModuleGroupID], [numReportFieldGroupID])
--SELECT 23, 30 
--
--
--
--
--
--SELECT * FROM dbo.DycFieldMaster WHERE numModuleID=4 AND vcLookBackTableName='PricingRuleTable'
--
--INSERT INTO [dbo].[DycFieldMaster]([numModuleID], [numDomainID], [vcFieldName], [vcDbColumnName], [vcOrigDbColumnName], [vcPropertyName], [vcLookBackTableName], [vcFieldDataType], [vcFieldType], [vcAssociatedControlType], [vcToolTip], [vcListItemType], [numListID], [PopupFunctionName], [order], [tintRow], [tintColumn], [bitInResults], [bitDeleted], [bitAllowEdit], [bitDefault], [bitSettingField], [bitAddField], [bitDetailField], [bitAllowSorting], [bitWorkFlowField], [bitImport], [bitExport], [bitAllowFiltering], [bitInlineEdit], [bitRequired], [intColumnWidth], [intFieldMaxLength])
--SELECT 4, NULL, N'Qty From', N'intFromQty', N'intFromQty', NULL, N'PricingRuleTable', N'N', N'R', N'TextBox', NULL, NULL, 0, NULL, 18, NULL, NULL, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL
--UNION
--SELECT 4, NULL, N'Qty To', N'intToQty', N'intToQty', NULL, N'PricingRuleTable', N'N', N'R', N'TextBox', NULL, NULL, 0, NULL, 18, NULL, NULL, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL
--UNION
--SELECT 4, NULL, N'Rule Type', N'vcRuleType', N'vcRuleType', NULL, N'PricingRuleTable', N'V', N'R', N'TextBox', NULL, NULL, 0, NULL, 18, NULL, NULL, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL
--UNION
--SELECT 4, NULL, N'Discount Type', N'vcDiscountType', N'vcDiscountType', NULL, N'PricingRuleTable', N'V', N'R', N'TextBox', NULL, NULL, 0, NULL, 18, NULL, NULL, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL
--UNION
--SELECT 4, NULL, N'Discount', N'decDiscount', N'decDiscount', NULL, N'PricingRuleTable', N'M', N'R', N'TextBox', NULL, NULL, 0, NULL, 18, NULL, NULL, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL
--UNION
--SELECT 4, NULL, N'Price Level Price', N'monPriceLevelPrice', N'monPriceLevelPrice', NULL, N'PricingRuleTable', N'M', N'R', N'TextBox', NULL, NULL, 0, NULL, 18, NULL, NULL, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL
--UNION
--SELECT 4, NULL, N'Price Level Type', N'vcPriceLevelType', N'vcPriceLevelType', NULL, N'PricingRuleTable', N'V', N'R', N'TextBox', NULL, NULL, 0, NULL, 18, NULL, NULL, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL
--
--
--INSERT INTO [dbo].[ReportFieldGroupMappingMaster]([numReportFieldGroupID], [numFieldID], [vcFieldName], 
--[vcAssociatedControlType], [vcFieldDataType], [bitAllowSorting], [bitAllowGrouping], [bitAllowAggregate], [bitAllowFiltering])
--SELECT 30, numFieldId, vcFieldName, vcDbColumnName, vcFieldDataType, 0, 0, 0, 0 FROM DycFieldMaster 
--WHERE numModuleID=4 AND vcLookBackTableName='PricingRuleTable'


----/************************************************************************************************/
----/************************25_Sep_2013*******************************************************************/
----/************************************************************************************************/
--
----Add Item & WareHouse into Opportunity Items
--INSERT INTO [dbo].[ReportModuleGroupFieldMappingMaster]([numReportModuleGroupID], [numReportFieldGroupID])
--SELECT 10, 7 UNION ALL
--SELECT 10, 11 
--
--
----Financial Transaction Search (frmAddAcc.aspx)
--UPDATE PageMaster SET bitIsExportApplicable=1 WHERE numModuleID=9 AND numPageID=12
--
--
----numIncomeChartAcntId,numAssetChartAcntId,numCOGsChartAcntId
--INSERT INTO [dbo].[ReportFieldGroupMappingMaster]([numReportFieldGroupID], [numFieldID], [vcFieldName], 
--[vcAssociatedControlType], [vcFieldDataType], [bitAllowSorting], [bitAllowGrouping], [bitAllowAggregate], [bitAllowFiltering])
--SELECT 7, numFieldId, vcFieldName, vcAssociatedControlType, vcFieldDataType, 0, 0, 0, 0 FROM DycFieldMaster 
--WHERE numModuleID=4 AND numFieldId IN (270,271,272)
--
--
--
----Primary Vendor
--INSERT INTO [dbo].[ReportFieldGroupMappingMaster]([numReportFieldGroupID], [numFieldID], [vcFieldName], 
--[vcAssociatedControlType], [vcFieldDataType], [bitAllowSorting], [bitAllowGrouping], [bitAllowAggregate], [bitAllowFiltering])
--SELECT 7, numFieldId, vcFieldName, vcAssociatedControlType, vcFieldDataType, 0, 0, 0, 0 FROM DycFieldMaster 
--WHERE numModuleID=4 AND numFieldId IN (289,290,291,349)
--
----Vendor to Primary Vendor
--UPDATE ReportFieldGroupMappingMaster SET vcFieldName='Primary Vendor' WHERE numReportFieldGroupID=7 AND numFieldID=349
-- 
-- 
--ALTER TABLE dbo.ReportListMaster ADD
--	bitHideSummaryDetail bit NULL 
--	
--	
--	
----====Set bitAllowAggregate=0===
--
--UPDATE ReportFieldGroupMappingMaster SET bitAllowAggregate=0 WHERE vcFieldDataType='V'	
--
----SELECT * FROM dbo.ReportFieldGroupMappingMaster RFGM JOIN dbo.DycFieldMaster DFM ON RFGM.numFieldID = DFM.numFieldId
---- WHERE bitAllowAggregate=1 AND ISNULL(DFM.vcListItemType,'')!=''
--
--UPDATE RFGM SET bitAllowAggregate=0 FROM dbo.ReportFieldGroupMappingMaster RFGM JOIN dbo.DycFieldMaster DFM ON RFGM.numFieldID = DFM.numFieldId
-- WHERE bitAllowAggregate=1 AND ISNULL(DFM.vcListItemType,'')!=''
--
--
----SELECT * FROM dbo.ReportFieldGroupMappingMaster RFGM JOIN dbo.DycFieldMaster DFM ON RFGM.numFieldID = DFM.numFieldId
---- WHERE bitAllowAggregate=1 AND ISNULL(RFGM.vcFieldDataType,'') IN ('D','Y')
-- 
-- 
-- UPDATE RFGM SET bitAllowAggregate=0 FROM dbo.ReportFieldGroupMappingMaster RFGM JOIN dbo.DycFieldMaster DFM ON RFGM.numFieldID = DFM.numFieldId
-- WHERE bitAllowAggregate=1 AND ISNULL(RFGM.vcFieldDataType,'') IN ('D','Y')
--
----Item ID,Opp Type,Project
-- UPDATE RFGM SET bitAllowAggregate=0 FROM dbo.ReportFieldGroupMappingMaster RFGM 
-- WHERE bitAllowAggregate=1 AND numFieldID IN (118,211,263)


--/************************************************************************************************/
--/************************23_Aug_2013*******************************************************************/
--/************************************************************************************************/
--
--ALTER TABLE dbo.DivisionMaster ADD
--	numDefaultPaymentMethod numeric(18, 0) NULL,
--	numDefaultCreditCard numeric(18, 0) NULL,
--	bitOnCreditHold bit NULL
--	
--	
----SELECT * FROM dbo.ReturnHeader WHERE numDomainId=172 AND tintReturnType=1 and numBizDocTempID=978
----UPDATE ReturnHeader SET numBizDocTempID=974 WHERE numDomainId=172 AND tintReturnType=1 and numBizDocTempID=978
--	
--	
--
----SELECT * FROM ReportFieldGroupMappingMaster where vcFieldName like '%Date%' AND numReportFieldGroupID=5
--
--UPDATE ReportFieldGroupMappingMaster SET vcFieldName='Order Created Date' WHERE vcFieldName like '%Created Date%' AND numReportFieldGroupID=3
--UPDATE ReportFieldGroupMappingMaster SET vcFieldName='Order Due Date' WHERE vcFieldName like '%Due Date%' AND numReportFieldGroupID=3
--UPDATE ReportFieldGroupMappingMaster SET vcFieldName='Order Estimated Created Date' WHERE vcFieldName like '%Closing Date%' AND numReportFieldGroupID=3
--	
--UPDATE ReportFieldGroupMappingMaster SET vcFieldName='BizDoc Billing Date' WHERE vcFieldName like '%Billing Date%' AND numReportFieldGroupID=5
--UPDATE ReportFieldGroupMappingMaster SET vcFieldName='BizDoc Created Date' WHERE vcFieldName like '%Created Date%' AND numReportFieldGroupID=5
--UPDATE ReportFieldGroupMappingMaster SET vcFieldName='BizDoc Due Date' WHERE vcFieldName like '%Due Date%' AND numReportFieldGroupID=5
--UPDATE ReportFieldGroupMappingMaster SET vcFieldName='BizDoc Modified Date' WHERE vcFieldName like '%Modified Date%' AND numReportFieldGroupID=5
--	
--	
--	
----SELECT * FROM ReportFieldGroupMaster
--INSERT INTO [dbo].[ReportFieldGroupMaster]([vcFieldGroupName], [bitActive], [bitCustomFieldGroup], [numGroupID], [vcCustomTableName])
--SELECT N'Leads Custom Field', 1, 1, 14, N'CFW_FLD_Values' UNION ALL
--SELECT N'Prospect Custom Field', 1, 1, 12, N'CFW_FLD_Values' UNION ALL
--SELECT N'Account Custom Field', 1, 1, 13, N'CFW_FLD_Values'	
--
--
----SELECT * FROM ReportModuleGroupFieldMappingMaster WHERE numReportFieldGroupID=15
--INSERT INTO [dbo].[ReportModuleGroupFieldMappingMaster]([numReportModuleGroupID], [numReportFieldGroupID])
--SELECT 1, 27 UNION ALL
--SELECT 5, 27 UNION ALL
--SELECT 14, 27 UNION ALL
--SELECT 16, 27 UNION ALL
--SELECT 1, 28 UNION ALL
--SELECT 5, 28 UNION ALL
--SELECT 14, 28 UNION ALL
--SELECT 16, 28 UNION ALL
--SELECT 1, 29 UNION ALL
--SELECT 5, 29 UNION ALL
--SELECT 14, 29 UNION ALL
--SELECT 16, 29 
--
-----Add Custom Fields to Other Module(Orders)
--INSERT INTO [dbo].[ReportModuleGroupFieldMappingMaster]([numReportModuleGroupID], [numReportFieldGroupID])
--SELECT 8, 15 UNION ALL
--SELECT 8, 27 UNION ALL
--SELECT 8, 28 UNION ALL
--SELECT 8, 29 UNION ALL
--SELECT 9, 15 UNION ALL
--SELECT 9, 27 UNION ALL
--SELECT 9, 28 UNION ALL
--SELECT 9, 29 UNION ALL
--SELECT 10, 15 UNION ALL
--SELECT 10, 27 UNION ALL
--SELECT 10, 28 UNION ALL
--SELECT 10, 29 UNION ALL
--SELECT 13, 15 UNION ALL
--SELECT 13, 27 UNION ALL
--SELECT 13, 28 UNION ALL
--SELECT 13, 29
----=========================================================================================	
--DECLARE @v sql_variant 
--SET @v = N'0: BizForm Wizards
--1: Regular Form (Grid Settings)
--2: Other Form
--3: WorkFlow Form'
--EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'DynamicFormMaster', N'COLUMN', N'tintFlag'
--
--
--INSERT INTO dbo.DynamicFormMaster (numFormId,vcFormName,cCustomFieldsAssociated,
--	cAOIAssociated,bitDeleted,tintFlag,bitWorkFlow,vcLocationID,bitAllowGridColor)
--SELECT 68,'Organization','Y','N',0,3,0,'1,12,13,14',0 UNION
--SELECT 69,'Contacts','Y','N',0,3,0,'4',0 UNION
--SELECT 70,'Opportunities & Orders','Y','N',0,3,0,'2,6',0 UNION
--SELECT 71,'BizDocs','Y','N',0,3,0,'',0 UNION
--SELECT 72,'Cases','Y','N',0,3,0,'3',0 UNION
--SELECT 73,'Projects','Y','N',0,3,0,'11',0 UNION
--SELECT 74,'Items','Y','N',0,3,0,'5',0
--
--
----SELECT * FROM dbo.DynamicFormMaster WHERE tintFlag=3
--
----SELECT * FROM dbo.DycFormField_Mapping WHERE numFormID=36
--
----Organization
--INSERT INTO dbo.DycFormField_Mapping (
--	numModuleID,numFieldID,numDomainID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,
--	PopupFunctionName,[order],tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,
--	bitDetailField,bitAllowSorting,bitWorkFlowField,bitImport,bitExport,bitAllowFiltering,bitRequired,numFormFieldID,intSectionID,bitAllowGridColor)
--SELECT numModuleID,numFieldID,numDomainID,68,0,0,vcFieldName,vcAssociatedControlType,vcPropertyName,
--	PopupFunctionName,[order],tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,0,0,
--	0,0,1,0,0,0,bitRequired,numFormFieldID,intSectionID,0
--	 FROM dbo.DycFormField_Mapping WHERE numFormID=36
--
--
----Contacts
--INSERT INTO dbo.DycFormField_Mapping (
--	numModuleID,numFieldID,numDomainID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,
--	PopupFunctionName,[order],tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,
--	bitDetailField,bitAllowSorting,bitWorkFlowField,bitImport,bitExport,bitAllowFiltering,bitRequired,numFormFieldID,intSectionID,bitAllowGridColor)
--SELECT numModuleID,numFieldID,numDomainID,69,0,0,vcFieldName,vcAssociatedControlType,vcPropertyName,
--	PopupFunctionName,[order],tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,0,0,
--	0,0,1,0,0,0,bitRequired,numFormFieldID,intSectionID,0
--	 FROM dbo.DycFormField_Mapping WHERE numFormID=10
--
--
----Opportunities & Orders
--INSERT INTO dbo.DycFormField_Mapping (
--	numModuleID,numFieldID,numDomainID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,
--	PopupFunctionName,[order],tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,
--	bitDetailField,bitAllowSorting,bitWorkFlowField,bitImport,bitExport,bitAllowFiltering,bitRequired,numFormFieldID,intSectionID,bitAllowGridColor)
--SELECT numModuleID,numFieldID,numDomainID,70,0,0,vcFieldName,vcAssociatedControlType,vcPropertyName,
--	PopupFunctionName,[order],tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,0,0,
--	0,0,1,0,0,0,bitRequired,numFormFieldID,intSectionID,0
--	 FROM dbo.DycFormField_Mapping WHERE numFormID=39
--
----BizDocs
--INSERT INTO dbo.DycFormField_Mapping (
--	numModuleID,numFieldID,numDomainID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,
--	PopupFunctionName,[order],tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,
--	bitDetailField,bitAllowSorting,bitWorkFlowField,bitImport,bitExport,bitAllowFiltering,bitRequired,numFormFieldID,intSectionID,bitAllowGridColor)
--SELECT numModuleID,numFieldID,numDomainID,71,0,0,vcFieldName,vcAssociatedControlType,vcPropertyName,
--	PopupFunctionName,[order],tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,0,0,
--	0,0,1,0,0,0,bitRequired,numFormFieldID,intSectionID,0
--	 FROM dbo.DycFormField_Mapping WHERE numFormID=7
--	 
--
----Cases
--INSERT INTO dbo.DycFormField_Mapping (
--	numModuleID,numFieldID,numDomainID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,
--	PopupFunctionName,[order],tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,
--	bitDetailField,bitAllowSorting,bitWorkFlowField,bitImport,bitExport,bitAllowFiltering,bitRequired,numFormFieldID,intSectionID,bitAllowGridColor)
--SELECT numModuleID,numFieldID,numDomainID,72,0,0,vcFieldName,vcAssociatedControlType,vcPropertyName,
--	PopupFunctionName,[order],tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,0,0,
--	0,0,1,0,0,0,bitRequired,numFormFieldID,intSectionID,0
--	 FROM dbo.DycFormField_Mapping WHERE numFormID=12
--	 
--
----Projects
--INSERT INTO dbo.DycFormField_Mapping (
--	numModuleID,numFieldID,numDomainID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,
--	PopupFunctionName,[order],tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,
--	bitDetailField,bitAllowSorting,bitWorkFlowField,bitImport,bitExport,bitAllowFiltering,bitRequired,numFormFieldID,intSectionID,bitAllowGridColor)
--SELECT numModuleID,numFieldID,numDomainID,73,0,0,vcFieldName,vcAssociatedControlType,vcPropertyName,
--	PopupFunctionName,[order],tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,0,0,
--	0,0,1,0,0,0,bitRequired,numFormFieldID,intSectionID,0
--	 FROM dbo.DycFormField_Mapping WHERE numFormID=13
--	 
--
----Items
--INSERT INTO dbo.DycFormField_Mapping (
--	numModuleID,numFieldID,numDomainID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,
--	PopupFunctionName,[order],tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,
--	bitDetailField,bitAllowSorting,bitWorkFlowField,bitImport,bitExport,bitAllowFiltering,bitRequired,numFormFieldID,intSectionID,bitAllowGridColor)
--SELECT numModuleID,numFieldID,numDomainID,74,0,0,vcFieldName,vcAssociatedControlType,vcPropertyName,
--	PopupFunctionName,[order],tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,0,0,
--	0,0,1,0,0,0,bitRequired,numFormFieldID,intSectionID,0
--	 FROM dbo.DycFormField_Mapping WHERE numFormID=21	 	 	 
--	 
--	 
----=========================================================================================
--
--ALTER TABLE dbo.OpportunityBizDocs ADD
--	bitAutoCreated bit NULL	 
--	
--UPDATE OpportunityBizDocs SET bitAutoCreated=0
--
--UPDATE dbo.OpportunityBizDocs SET bitAutoCreated=1 WHERE numOppBizDocsId IN (
--SELECT numOppBizDocsId FROM dbo.OpportunityAutomationQueueExecution WHERE numRuleID IN (1,2))
--
--
----=========================================================================================
--
----SELECT * FROM dbo.ReportFieldGroupMaster
--INSERT INTO [dbo].[ReportFieldGroupMaster]([vcFieldGroupName], [bitActive], [bitCustomFieldGroup], [numGroupID], [vcCustomTableName])
--SELECT N'Item Price Level', 1, NULL, NULL, NULL
--
----SELECT *FROM dbo.ReportModuleGroupMaster
--INSERT INTO [dbo].[ReportModuleGroupFieldMappingMaster]([numReportModuleGroupID], [numReportFieldGroupID])
--SELECT 19, 30 
--
--INSERT INTO [dbo].[ReportModuleGroupFieldMappingMaster]([numReportModuleGroupID], [numReportFieldGroupID])
--SELECT 23, 30 
--
--
--
--
--
--SELECT * FROM dbo.DycFieldMaster WHERE numModuleID=4 AND vcLookBackTableName='PricingRuleTable'
--
--INSERT INTO [dbo].[DycFieldMaster]([numModuleID], [numDomainID], [vcFieldName], [vcDbColumnName], [vcOrigDbColumnName], [vcPropertyName], [vcLookBackTableName], [vcFieldDataType], [vcFieldType], [vcAssociatedControlType], [vcToolTip], [vcListItemType], [numListID], [PopupFunctionName], [order], [tintRow], [tintColumn], [bitInResults], [bitDeleted], [bitAllowEdit], [bitDefault], [bitSettingField], [bitAddField], [bitDetailField], [bitAllowSorting], [bitWorkFlowField], [bitImport], [bitExport], [bitAllowFiltering], [bitInlineEdit], [bitRequired], [intColumnWidth], [intFieldMaxLength])
--SELECT 4, NULL, N'Qty From', N'intFromQty', N'intFromQty', NULL, N'PricingRuleTable', N'N', N'R', N'TextBox', NULL, NULL, 0, NULL, 18, NULL, NULL, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL
--UNION
--SELECT 4, NULL, N'Qty To', N'intToQty', N'intToQty', NULL, N'PricingRuleTable', N'N', N'R', N'TextBox', NULL, NULL, 0, NULL, 18, NULL, NULL, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL
--UNION
--SELECT 4, NULL, N'Rule Type', N'vcRuleType', N'vcRuleType', NULL, N'PricingRuleTable', N'V', N'R', N'TextBox', NULL, NULL, 0, NULL, 18, NULL, NULL, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL
--UNION
--SELECT 4, NULL, N'Discount Type', N'vcDiscountType', N'vcDiscountType', NULL, N'PricingRuleTable', N'V', N'R', N'TextBox', NULL, NULL, 0, NULL, 18, NULL, NULL, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL
--UNION
--SELECT 4, NULL, N'Discount', N'decDiscount', N'decDiscount', NULL, N'PricingRuleTable', N'M', N'R', N'TextBox', NULL, NULL, 0, NULL, 18, NULL, NULL, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL
--UNION
--SELECT 4, NULL, N'Price Level Price', N'monPriceLevelPrice', N'monPriceLevelPrice', NULL, N'PricingRuleTable', N'M', N'R', N'TextBox', NULL, NULL, 0, NULL, 18, NULL, NULL, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL
--UNION
--SELECT 4, NULL, N'Price Level Type', N'vcPriceLevelType', N'vcPriceLevelType', NULL, N'PricingRuleTable', N'V', N'R', N'TextBox', NULL, NULL, 0, NULL, 18, NULL, NULL, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL
--
--
--INSERT INTO [dbo].[ReportFieldGroupMappingMaster]([numReportFieldGroupID], [numFieldID], [vcFieldName], 
--[vcAssociatedControlType], [vcFieldDataType], [bitAllowSorting], [bitAllowGrouping], [bitAllowAggregate], [bitAllowFiltering])
--SELECT 30, numFieldId, vcFieldName, vcDbColumnName, vcFieldDataType, 0, 0, 0, 0 FROM DycFieldMaster 
--WHERE numModuleID=4 AND vcLookBackTableName='PricingRuleTable'


--/************************************************************************************************/
--/************************26_July_2013*******************************************************************/
--/************************************************************************************************/
--
--ALTER TABLE dbo.Domain ADD
--	bitDiscountOnUnitPrice bit NULL
--	
--
----SELECT * FROM dbo.ReportFieldGroupMaster
--INSERT INTO ReportFieldGroupMaster SELECT 'BizDocs Items Tax Field',1,NULL,NULL,NULL	
--
--INSERT INTO ReportFieldGroupMaster SELECT 'BizDocs Tax Field',1,NULL,NULL,NULL	
--
----SELECT * FROM dbo.ReportModuleGroupMaster
----SELECT * FROM dbo.ReportModuleGroupFieldMappingMaster
--INSERT INTO dbo.ReportModuleGroupFieldMappingMaster SELECT 9,25 
--
--INSERT INTO dbo.ReportModuleGroupFieldMappingMaster SELECT 8,26 
--



----*******Union National Bank(6016) to Undeposited Funds(6653)*********************************************************************
----SELECT vcAccountName,* FROM dbo.Chart_Of_Accounts WHERE numDomainId=169
--
----SELECT * FROM dbo.DepositMaster WHERE tintDepositeToType=2 AND numDomainId=169
--
----SELECT GJD.* FROM dbo.General_Journal_Header GJH JOIN dbo.General_Journal_Details GJD ON GJH.numJournal_Id = GJD.numJournalId
----JOIN dbo.DepositMaster DM ON DM.numDepositId=GJH.numDepositId WHERE DM.tintDepositeToType=2 AND DM.numDomainId=169
----AND GJD.varDescription='Payment received to Undeposited Fund' AND GJD.numChartAcntId=6016
--
--UPDATE GJD SET numChartAcntId=6653 FROM dbo.General_Journal_Header GJH JOIN dbo.General_Journal_Details GJD ON GJH.numJournal_Id = GJD.numJournalId
--JOIN dbo.DepositMaster DM ON DM.numDepositId=GJH.numDepositId WHERE DM.tintDepositeToType=2 AND DM.numDomainId=169
--AND GJD.varDescription='Payment received to Undeposited Fund' AND GJD.numChartAcntId=6016
--
----SELECT GJD.* FROM dbo.General_Journal_Header GJH JOIN dbo.General_Journal_Details GJD ON GJH.numJournal_Id = GJD.numJournalId
----JOIN dbo.DepositMaster DM ON DM.numDepositId=GJH.numDepositId WHERE DM.tintDepositeToType=1 AND DM.numDomainId=169
----AND GJD.varDescription='Amount Deposited from UnDeposited Funds' AND GJD.numChartAcntId=6016
--
--UPDATE GJD SET numChartAcntId=6653 FROM dbo.General_Journal_Header GJH JOIN dbo.General_Journal_Details GJD ON GJH.numJournal_Id = GJD.numJournalId
--JOIN dbo.DepositMaster DM ON DM.numDepositId=GJH.numDepositId WHERE DM.tintDepositeToType=1 AND DM.numDomainId=169
--AND GJD.varDescription='Amount Deposited from UnDeposited Funds' AND GJD.numChartAcntId=6016
----****************************************************************************


--*******Change Opportunities to Transactions (Also SP:USP_ManageTabsInCuSFields)*********************************************************************
--exec USP_GetCustomFieldsTab @LocID=13,@numDomainID=169,@Type=-1,@numGroupID=1
--SELECT * FROM CFw_Grp_Master

----SELECT * FROM CFw_Grp_Master WHERE Loc_Id=13 AND grp_name='Opportunities'
--UPDATE CFw_Grp_Master SET grp_name='Transactions' WHERE Loc_Id=13 AND grp_name='Opportunities'
--
----SELECT * FROM CFw_Grp_Master WHERE Loc_Id=12 AND grp_name='Opportunities'
--UPDATE CFw_Grp_Master SET grp_name='Transactions' WHERE Loc_Id=12 AND grp_name='Opportunities'


--****************************************************************************
--
--DECLARE  @maxDomainID NUMERIC(9)
--DECLARE  @minDomainID NUMERIC(9)
--
--SELECT @maxDomainID = max([numDomainId]),@minDomainID = min([numDomainId]) FROM   [Domain] where numDomainId>0
--WHILE @minDomainID <= @maxDomainID
--  BEGIN
--  
--	 DECLARE @numAccountsPayable AS NUMERIC(9)
--	 SELECT @numAccountsPayable = numAccountTypeID FROM dbo.AccountTypeDetail WHERE vcAccountCode = '01020102'  AND numDomainID = @minDomainID  
--
--	 IF NOT EXISTS (SELECT 1 FROM AccountTypeDetail WHERE numDomainID=@minDomainID AND vcAccountCode='0102010201')
--	 BEGIN
--		INSERT INTO [AccountTypeDetail] ([vcAccountCode],[vcAccountType],[numParentID],[numDomainID],[dtCreateDate],[dtModifiedDate],[bitSystem]) Values('0102010201','Accounts Payable Clearing Acct',@numAccountsPayable,@minDomainID,GETUTCDATE(),GETUTCDATE(),1)
--	 END
--
--	 DECLARE @numAccountsReceivable AS NUMERIC(9)
--	 SELECT @numAccountsReceivable = numAccountTypeID FROM dbo.AccountTypeDetail WHERE vcAccountCode = '01010105'  AND numDomainID = @minDomainID  
--
--	 IF NOT EXISTS (SELECT 1 FROM AccountTypeDetail WHERE numDomainID=@minDomainID AND vcAccountCode='0101010501')
--	 BEGIN
--		INSERT INTO [AccountTypeDetail] ([vcAccountCode],[vcAccountType],[numParentID],[numDomainID],[dtCreateDate],[dtModifiedDate],[bitSystem]) Values('0101010501','Accounts Receivable Clearing Acct',@numAccountsReceivable,@minDomainID,GETUTCDATE(),GETUTCDATE(),1)
--	 END
--	 
--	 SELECT @minDomainID = min([numDomainId]) FROM   [Domain] WHERE  [numDomainId]> @minDomainID 
--  END

--****************************************************************************

--SELECT vcAccountName,* FROM dbo.Chart_Of_Accounts WHERE numDomainId=169
----Account Receivable	6020
----Accounts Payable	6027
--
--SELECT GJD.*/*,SUM(GJD.numDebitAmt)- SUM(GJD.numCreditAmt)*/ FROM dbo.General_Journal_Header GJH JOIN dbo.General_Journal_Details GJD ON GJH.numJournal_Id = GJD.numJournalId
--WHERE GJH.numDomainId=169
--AND GJD.varDescription='Standalone Debit' AND GJD.numChartAcntId=6020
--
--UPDATE GJD SET numChartAcntId=6027 FROM dbo.General_Journal_Header GJH JOIN dbo.General_Journal_Details GJD ON GJH.numJournal_Id = GJD.numJournalId
--WHERE GJH.numDomainId=169
--AND GJD.varDescription='Standalone Debit' AND GJD.numChartAcntId=6020


--****************************************************************************

--
--ALTER TABLE dbo.General_Journal_Details ADD
--	numEntryDateSortOrder1 numeric(18, 0) NULL
--	
--UPDATE GJD SET numEntryDateSortOrder1 = CONVERT(numeric,
--RIGHT('00000'+ CONVERT(VARCHAR,DATEPART(year, GJH.datEntry_Date)),4)+
--RIGHT('00000'+ CONVERT(VARCHAR,DATEPART(month, GJH.datEntry_Date)),2)+
--RIGHT('00000'+ CONVERT(VARCHAR,DATEPART(day, GJH.datEntry_Date)),2)+
--RIGHT('00000'+ CONVERT(VARCHAR,DATEPART(hour, GJH.datEntry_Date)),2)+
--RIGHT('00000'+ CONVERT(VARCHAR,DATEPART(minute, GJH.datEntry_Date)),2)+
--RIGHT('00000'+ CONVERT(VARCHAR,DATEPART(second, GJH.datEntry_Date)),2)+
--RIGHT('00000'+ CONVERT(VARCHAR,DATEPART(millisecond, GJH.datEntry_Date)),3))
--FROM dbo.General_Journal_Header GJH JOIN dbo.General_Journal_Details GJD ON GJH.numJournal_Id = GJD.numJournalId
--
--
----=============================================================================================================
--DECLARE @RowsToProcess int
--DECLARE @CurrentRow int
-- 
--DECLARE @DuplicateVersions TABLE
--(
--    row_id int NOT NULL PRIMARY KEY IDENTITY(1,1),
--    duplicate_count int,
--    numEntryDateSortOrder1 NUMERIC
--)
-- 
--INSERT INTO @DuplicateVersions 
--            (   
--                duplicate_count,
--                numEntryDateSortOrder1
--            )
--         SELECT COUNT(*) AS duplicate_count,
--               GJD.numEntryDateSortOrder1
--           FROM General_Journal_Details GJD
--       GROUP BY GJD.numEntryDateSortOrder1
--         HAVING COUNT(*) > 1
--       ORDER BY duplicate_count DESC
--
--SELECT @RowsToProcess = COUNT(*) FROM @DuplicateVersions
--SET @CurrentRow = 1
-- 
--WHILE @CurrentRow <= @RowsToProcess
--BEGIN   
--
--Select GJD.numTransactionId,GJD.numEntryDateSortOrder1,IDENTITY( int,1,1 ) as NewSort INTO #temp123 
--from General_Journal_Details GJD
--   INNER JOIN @DuplicateVersions AS DuplicateVersions 
--ON GJD.numEntryDateSortOrder1 = DuplicateVersions.numEntryDateSortOrder1 
--WHERE DuplicateVersions.row_id = @CurrentRow
--
--UPDATE  GJD
--SET GJD.numEntryDateSortOrder1 = (Temp1.numEntryDateSortOrder1 + Temp1.NewSort)  from General_Journal_Details GJD
--   INNER JOIN #temp123 AS Temp1  
--ON GJD.numTransactionId = Temp1.numTransactionId
--
--DROP TABLE #temp123
--
--SET @CurrentRow = @CurrentRow + 1      
--END 


--****************************************************************************

--CREATE NONCLUSTERED INDEX [Index_OpportunityItems_OppID] ON [dbo].[OpportunityItems] 
--(
--	[numOppId] ASC
--)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
--
--
--
--
--CREATE NONCLUSTERED INDEX [Index_OpportunityBizDocItems_numOppBizDocID] ON [dbo].[OpportunityBizDocItems] 
--(
--	[numOppBizDocID] ASC
--)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]



--CREATE TABLE [dbo].[ParentChildCustomFieldMap](
--	[numParentChildFieldID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
--	[numDomainID] [numeric](18, 0) NULL,
--	[tintParentModule] [tinyint] NULL,
--	[numParentFieldID] [nchar](10) NULL,
--	[tintChildModule] [tinyint] NULL,
--	[numChildFieldID] [tinyint] NULL,
-- CONSTRAINT [PK_ParentChildCustomFieldMap] PRIMARY KEY CLUSTERED 
--(
--	[numParentChildFieldID] ASC
--)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
--) ON [PRIMARY]
--
--
--
--INSERT INTO [dbo].[ReportFieldGroupMappingMaster]([numReportFieldGroupID], [numFieldID], [vcFieldName], [vcAssociatedControlType], [vcFieldDataType], [bitAllowSorting], [bitAllowGrouping], [bitAllowAggregate], [bitAllowFiltering])
--SELECT 7, numFieldId, N'Item ID', N'TextBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster 
--WHERE numModuleID=4 AND vcOrigDbColumnName='numItemCode' AND numFieldId=211 
--
--
--INSERT INTO [dbo].[ReportFieldGroupMappingMaster]([numReportFieldGroupID], [numFieldID], [vcFieldName], [vcAssociatedControlType], [vcFieldDataType], [bitAllowSorting], [bitAllowGrouping], [bitAllowAggregate], [bitAllowFiltering])
--SELECT 7, numFieldId, N'Description', N'TextBox', N'V', 1, 1, 1, 1 FROM DycFieldMaster 
--WHERE numModuleID=4 AND vcOrigDbColumnName='txtItemDesc' AND numFieldId=191 

--/************************************************************************************************/
--/************************11_July_2013*******************************************************************/
--/************************************************************************************************/

--ALTER TABLE dbo.OpportunityItems ADD
--	monAvgCost money NULL
--	
--	
--UPDATE OI SET OI.monAvgCost=I.monAverageCost FROM dbo.OpportunityItems OI JOIN Item I 
--	      ON OI.numItemCode=I.numItemCode WHERE I.charItemType='P'
--	      
---------******Opportunity to Opportunity/Order******------------
----SELECT * FROM EmailMergeModule
--UPDATE EmailMergeModule SET vcModuleName='Opportunity/Order' WHERE numModuleID=2 	      
--
--
---------******Add Tracking Number to Email Template******------------
----SELECT EMF.* FROM   EmailMergeFields EMF
----        INNER JOIN dbo.EmailMergeModule EMM ON EMF.numModuleID = EMM.numModuleID
----WHERE   EMF.numModuleID = 2 AND EMM.tintModuleType=0 AND EMM.tintModuleType=ISNULL(EMF.tintModuleType,0)
--
----exec USP_GetMergeFieldsByModuleId @numModuleID=2,@tintMode=0,@tintModuleType=0
--
--INSERT INTO dbo.EmailMergeFields (
--	vcMergeField,
--	vcMergeFieldValue,
--	numModuleID,
--	tintModuleType
--) VALUES ( 
--	/* vcMergeField - varchar(100) */ 'Tracking Numbers',
--	/* vcMergeFieldValue - varchar(2000) */ '##TrackingNo##',
--	/* numModuleID - numeric(18, 0) */ 2,
--	/* tintModuleType - tinyint */ 0 ) 
--	
----SELECT * FROM dbo.DycFieldMaster WHERE vcDbColumnName='numManagerID'
--UPDATE DycFieldMaster SET vcListItemType='U' WHERE vcDbColumnName='numManagerID'
--	
-----------------BizDoc Template---------------------------------------------------------
--
----#OrganizationComments# -> #Customer/VendorOrganizationComments#
----#OrganizationContactName# -> #Customer/VendorOrganizationContactName#
----#OrganizationContactEmail# -> #Customer/VendorOrganizationContactEmail#
----#OrganizationContactPhone# -> #Customer/VendorOrganizationContactPhone#
----#OrganizationName# -> #Customer/VendorOrganizationName#
----#OrganizationPhone# -> #Customer/VendorOrganizationPhone#
----
----#OrganizationBillingAddress# -> SO : #EmployerBillToAddress#       PO : #Customer/VendorBillToAddress#
----#BillTo# -> SO : #Customer/VendorBillToAddress#       PO : #EmployerBillToAddress#
----#ChangeBillToAddress(Bill To)# -> SO : #Customer/VendorChangeBillToHeader       PO : #EmployerChangeBillToHeader
----#ShipTo# -> SO : #Customer/VendorShipToAddress#       PO : #EmployerShipToAddress#
----#ChangeShipToAddress(Ship To)# -> SO : #Customer/VendorChangeShipToHeader       PO : #EmployerChangeShipToHeader
----#BillToAddressName# -> SO : #Customer/VendorBillToAddressName#       PO : #EmployerBillToAddressName#
----#ShipToAddressName# -> SO : #Customer/VendorShipToAddressName#       PO : #EmployerShipToAddressName#
----#BillToCompanyName# -> SO : #Customer/VendorBillToCompanyName#       PO : #EmployerBillToCompanyName#
----#ShipToCompanyName# -> SO : #Customer/VendorShipToCompanyName#       PO : #EmployerShipToCompanyName#
--
--ALTER TABLE BizDocTemplate ADD
--txtNewBizDocTemplate TEXT NULL
--
--
----UPDATE BizDocTemplate SET txtBizDocTemplate= ISNULL(txtNewBizDocTemplate,'')
--UPDATE BizDocTemplate SET txtNewBizDocTemplate = txtBizDocTemplate
--
--------------------------------------------------------------------------
--
--UPDATE BizDocTemplate SET txtBizDocTemplate = REPLACE(CAST(txtBizDocTemplate AS NVARCHAR(MAX)),
--'#OrganizationComments#','#Customer/VendorOrganizationComments#')
--WHERE txtBizDocTemplate LIKE '%#OrganizationComments#%' --AND numOppType = 1
--
--UPDATE BizDocTemplate SET txtBizDocTemplate = REPLACE(CAST(txtBizDocTemplate AS NVARCHAR(MAX)),
--'#OrganizationContactName#','#Customer/VendorOrganizationContactName#')
--WHERE txtBizDocTemplate LIKE '%#OrganizationContactName#%' --AND numOppType = 1
--
--UPDATE BizDocTemplate SET txtBizDocTemplate = REPLACE(CAST(txtBizDocTemplate AS NVARCHAR(MAX)),
--'#OrganizationContactEmail#','#Customer/VendorOrganizationContactEmail#')
--WHERE txtBizDocTemplate LIKE '%#OrganizationContactEmail#%' --AND numOppType = 1
--
--UPDATE BizDocTemplate SET txtBizDocTemplate = REPLACE(CAST(txtBizDocTemplate AS NVARCHAR(MAX)),
--'#OrganizationContactPhone#','#Customer/VendorOrganizationContactPhone#')
--WHERE txtBizDocTemplate LIKE '%#OrganizationContactPhone#%' --AND numOppType = 1
--
--UPDATE BizDocTemplate SET txtBizDocTemplate = REPLACE(CAST(txtBizDocTemplate AS NVARCHAR(MAX)),
--'#OrganizationName#','#Customer/VendorOrganizationName#')
--WHERE txtBizDocTemplate LIKE '%#OrganizationName#%' --AND numOppType = 1
--
--UPDATE BizDocTemplate SET txtBizDocTemplate = REPLACE(CAST(txtBizDocTemplate AS NVARCHAR(MAX)),
--'#OrganizationPhone#','#Customer/VendorOrganizationPhone#')
--WHERE txtBizDocTemplate LIKE '%#OrganizationPhone#%' --AND numOppType = 1
--
--------------------------------------------------------------------------
--
--UPDATE BizDocTemplate SET txtBizDocTemplate = REPLACE(CAST(txtBizDocTemplate AS NVARCHAR(MAX)),
--'#BillTo#','#Customer/VendorBillToAddress#')
--WHERE txtBizDocTemplate LIKE '%#BillTo#%' AND numOppType = 1
--
--UPDATE BizDocTemplate SET txtBizDocTemplate = REPLACE(CAST(txtBizDocTemplate AS NVARCHAR(MAX)),
--'#ShipTo#','#Customer/VendorShipToAddress#')
--WHERE txtBizDocTemplate LIKE '%#ShipTo#%' AND numOppType = 1
--
--UPDATE BizDocTemplate SET txtBizDocTemplate = REPLACE(CAST(txtBizDocTemplate AS NVARCHAR(MAX)),
--'#ChangeBillToAddress','#Customer/VendorChangeBillToHeader')
--WHERE txtBizDocTemplate LIKE '%#ChangeBillToAddress%' AND numOppType = 1
--
--UPDATE BizDocTemplate SET txtBizDocTemplate = REPLACE(CAST(txtBizDocTemplate AS NVARCHAR(MAX)),
--'#ChangeShipToAddress','#Customer/VendorChangeShipToHeader')
--WHERE txtBizDocTemplate LIKE '%#ChangeShipToAddress%' AND numOppType = 1
--
--UPDATE BizDocTemplate SET txtBizDocTemplate = REPLACE(CAST(txtBizDocTemplate AS NVARCHAR(MAX)),
--'#BillToAddressName#','#Customer/VendorBillToAddressName#')
--WHERE txtBizDocTemplate LIKE '%#BillToAddressName#%' AND numOppType = 1
--
--UPDATE BizDocTemplate SET txtBizDocTemplate = REPLACE(CAST(txtBizDocTemplate AS NVARCHAR(MAX)),
--'#ShipToAddressName#','#Customer/VendorShipToAddressName#')
--WHERE txtBizDocTemplate LIKE '%#ShipToAddressName#%' AND numOppType = 1
--
--UPDATE BizDocTemplate SET txtBizDocTemplate = REPLACE(CAST(txtBizDocTemplate AS NVARCHAR(MAX)),
--'#BillToCompanyName#','#Customer/VendorBillToCompanyName#')
--WHERE txtBizDocTemplate LIKE '%#BillToCompanyName#%' AND numOppType = 1
--
--UPDATE BizDocTemplate SET txtBizDocTemplate = REPLACE(CAST(txtBizDocTemplate AS NVARCHAR(MAX)),
--'#ShipToCompanyName#','#Customer/VendorShipToCompanyName#')
--WHERE txtBizDocTemplate LIKE '%#ShipToCompanyName#%' AND numOppType = 1
--
--
--UPDATE BizDocTemplate SET txtBizDocTemplate = REPLACE(CAST(txtBizDocTemplate AS NVARCHAR(MAX)),
--'#OrganizationBillingAddress#','#EmployerBillToAddress#')
--WHERE txtBizDocTemplate LIKE '%#OrganizationBillingAddress#%' AND numOppType = 1
--
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--
--UPDATE BizDocTemplate SET txtBizDocTemplate = REPLACE(CAST(txtBizDocTemplate AS NVARCHAR(MAX)),
--'#BillTo#','#EmployerBillToAddress#')
--WHERE txtBizDocTemplate LIKE '%#BillTo#%' AND numOppType = 2
--
--UPDATE BizDocTemplate SET txtBizDocTemplate = REPLACE(CAST(txtBizDocTemplate AS NVARCHAR(MAX)),
--'#ShipTo#','#EmployerShipToAddress#')
--WHERE txtBizDocTemplate LIKE '%#ShipTo#%' AND numOppType = 2
--
--UPDATE BizDocTemplate SET txtBizDocTemplate = REPLACE(CAST(txtBizDocTemplate AS NVARCHAR(MAX)),
--'#ChangeBillToAddress','#EmployerChangeBillToHeader')
--WHERE txtBizDocTemplate LIKE '%#ChangeBillToAddress%' AND numOppType = 2
--
--UPDATE BizDocTemplate SET txtBizDocTemplate = REPLACE(CAST(txtBizDocTemplate AS NVARCHAR(MAX)),
--'#ChangeShipToAddress','#EmployerChangeShipToHeader')
--WHERE txtBizDocTemplate LIKE '%#ChangeShipToAddress%' AND numOppType = 2
--
--UPDATE BizDocTemplate SET txtBizDocTemplate = REPLACE(CAST(txtBizDocTemplate AS NVARCHAR(MAX)),
--'#BillToAddressName#','#EmployerBillToAddressName#')
--WHERE txtBizDocTemplate LIKE '%#BillToAddressName#%' AND numOppType = 2
--
--UPDATE BizDocTemplate SET txtBizDocTemplate = REPLACE(CAST(txtBizDocTemplate AS NVARCHAR(MAX)),
--'#ShipToAddressName#','#EmployerShipToAddressName#')
--WHERE txtBizDocTemplate LIKE '%#ShipToAddressName#%' AND numOppType = 2
--
--UPDATE BizDocTemplate SET txtBizDocTemplate = REPLACE(CAST(txtBizDocTemplate AS NVARCHAR(MAX)),
--'#BillToCompanyName#','#EmployerBillToCompanyName#')
--WHERE txtBizDocTemplate LIKE '%#BillToCompanyName#%' AND numOppType = 2
--
--UPDATE BizDocTemplate SET txtBizDocTemplate = REPLACE(CAST(txtBizDocTemplate AS NVARCHAR(MAX)),
--'#ShipToCompanyName#','#EmployerShipToCompanyName#')
--WHERE txtBizDocTemplate LIKE '%#ShipToCompanyName#%' AND numOppType = 2
--
--
--UPDATE BizDocTemplate SET txtBizDocTemplate = REPLACE(CAST(txtBizDocTemplate AS NVARCHAR(MAX)),
--'#OrganizationBillingAddress#','#Customer/VendorBillToAddress#')
--WHERE txtBizDocTemplate LIKE '%#OrganizationBillingAddress#%' AND numOppType = 2

--/************************************************************************************************/
--/************************13_June_2013*******************************************************************/
--/************************************************************************************************/
--
----CalAmount to monDealAmount
--UPDATE DycFieldMaster SET vcOrigDbColumnName='monDealAmount' WHERE numModuleID=3 AND numFieldId=104
--
--
----ADC.vcFirstName to vcFirstName
--UPDATE DycFieldMaster SET vcOrigDbColumnName='vcFirstName' WHERE numModuleID=2 AND numFieldId=51
--
--INSERT INTO [dbo].[DycFieldMaster]([numModuleID], [numDomainID], [vcFieldName], [vcDbColumnName], [vcOrigDbColumnName], [vcPropertyName], [vcLookBackTableName], [vcFieldDataType], [vcFieldType], [vcAssociatedControlType], [vcToolTip], [vcListItemType], [numListID], [PopupFunctionName], [order], [tintRow], [tintColumn], [bitInResults], [bitDeleted], [bitAllowEdit], [bitDefault], [bitSettingField], [bitAddField], [bitDetailField], [bitAllowSorting], [bitWorkFlowField], [bitImport], [bitExport], [bitAllowFiltering], [bitInlineEdit], [bitRequired], [intColumnWidth], [intFieldMaxLength])
--SELECT 3, NULL, N'Manufacturer', N'vcManufacturer', N'vcManufacturer', NULL, N'OpportunityItems', N'N', N'R', N'TextBox', NULL, NULL, 0, NULL, 18, NULL, NULL, 1, 0, 0, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL
--
--
----ItemType to vcType
--UPDATE DycFieldMaster SET vcOrigDbColumnName='vcType' WHERE numModuleID=3 AND numFieldId=256
--
--
----Add ListId into Item Class
--UPDATE DycFieldMaster SET vcListItemType='LI',numListID=381 WHERE numModuleID=3 AND numFieldId=264
--
--
----OpportunityBizDocs Fields
--INSERT INTO [dbo].[DycFieldMaster]([numModuleID], [numDomainID], [vcFieldName], [vcDbColumnName], [vcOrigDbColumnName], [vcPropertyName], [vcLookBackTableName], [vcFieldDataType], [vcFieldType], [vcAssociatedControlType], [vcToolTip], [vcListItemType], [numListID], [PopupFunctionName], [order], [tintRow], [tintColumn], [bitInResults], [bitDeleted], [bitAllowEdit], [bitDefault], [bitSettingField], [bitAddField], [bitDetailField], [bitAllowSorting], [bitWorkFlowField], [bitImport], [bitExport], [bitAllowFiltering], [bitInlineEdit], [bitRequired], [intColumnWidth], [intFieldMaxLength])
--SELECT 3, NULL, N'Is Authoritative', N'bitAuthoritativeBizDocs', N'bitAuthoritativeBizDocs', NULL, N'OpportunityBizDocs', N'Y', N'R', N'CheckBox', NULL, NULL, 0, NULL, 18, NULL, NULL, 1, 0, 0, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL
--UNION
--SELECT 3, NULL, N'Created By', N'numCreatedBy', N'numCreatedBy', NULL, N'OpportunityBizDocs', N'N', N'R', N'SelectBox', NULL, 'U', 0, NULL, 18, NULL, NULL, 1, 0, 0, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL
--UNION
--SELECT 3, NULL, N'Created Date', N'dtCreatedDate', N'dtCreatedDate', NULL, N'OpportunityBizDocs', N'V', N'R', N'DateField', NULL, NULL, 0, NULL, 18, NULL, NULL, 1, 0, 0, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL
--UNION
--SELECT 3, NULL, N'Modified By', N'numModifiedBy', N'numModifiedBy', NULL, N'OpportunityBizDocs', N'N', N'R', N'SelectBox', NULL, 'U', 0, NULL, 18, NULL, NULL, 1, 0, 0, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL
--UNION
--SELECT 3, NULL, N'Modified Date', N'dtModifiedDate', N'dtModifiedDate', NULL, N'OpportunityBizDocs', N'V', N'R', N'DateField', NULL, NULL, 0, NULL, 18, NULL, NULL, 1, 0, 0, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL
--UNION
--SELECT 3, NULL, N'Deferred', N'tintDeferred', N'tintDeferred', NULL, N'OpportunityBizDocs', N'Y', N'R', N'CheckBox', NULL, NULL, 0, NULL, 18, NULL, NULL, 1, 0, 0, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL
--UNION
--SELECT 3, NULL, N'Billing Date', N'dtFromDate', N'dtFromDate', NULL, N'OpportunityBizDocs', N'V', N'R', N'DateField', NULL, NULL, 0, NULL, 18, NULL, NULL, 1, 0, 0, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL
--UNION
--SELECT 3, NULL, N'Due Date', N'dtDueDate', N'dtDueDate', NULL, N'OpportunityBizDocs', N'V', N'R', N'DateField', NULL, NULL, 0, NULL, 18, NULL, NULL, 1, 0, 0, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL
--UNION
--SELECT 3, NULL, N'Amount Due', N'monAmountDue', N'monAmountDue', NULL, N'OpportunityBizDocs', N'M', N'R', N'TextBox', NULL, NULL, 0, NULL, 18, NULL, NULL, 1, 0, 0, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL
--
--SELECT * FROM dbo.DycFieldMaster WHERE numModuleID=3
----Item Field
--INSERT INTO [dbo].[DycFieldMaster]([numModuleID], [numDomainID], [vcFieldName], [vcDbColumnName], [vcOrigDbColumnName], [vcPropertyName], [vcLookBackTableName], [vcFieldDataType], [vcFieldType], [vcAssociatedControlType], [vcToolTip], [vcListItemType], [numListID], [PopupFunctionName], [order], [tintRow], [tintColumn], [bitInResults], [bitDeleted], [bitAllowEdit], [bitDefault], [bitSettingField], [bitAddField], [bitDetailField], [bitAllowSorting], [bitWorkFlowField], [bitImport], [bitExport], [bitAllowFiltering], [bitInlineEdit], [bitRequired], [intColumnWidth], [intFieldMaxLength])
--SELECT 4, NULL, N'Allow DropShip', N'bitAllowDropShip', N'bitAllowDropShip', N'bitAllowDropShip', N'Item', N'Y', N'R', N'CheckBox', NULL, N'', 0, NULL, NULL, NULL, NULL, 1, 0, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL UNION ALL
--SELECT 4, NULL, N'Ship Class', N'numShipClass', N'numShipClass', N'numShipClass', N'Item', N'V', N'R', N'SelectBox', NULL, N'LI', 381, NULL, 0, NULL, NULL, 1, 0, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL
--
--
----Projects to ProjectsMaster
--UPDATE DycFieldMaster SET vcLookBackTableName='ProjectsMaster' WHERE numModuleID=5 AND vcLookBackTableName='Projects'
--
----numReason to numCreatedBy
--UPDATE DycFieldMaster SET vcOrigDbColumnName='numCreatedBy' WHERE numModuleID=5 AND vcOrigDbColumnName='numReason'
--
----Case Field
--INSERT INTO [dbo].[DycFieldMaster]([numModuleID], [numDomainID], [vcFieldName], [vcDbColumnName], [vcOrigDbColumnName], [vcPropertyName], [vcLookBackTableName], [vcFieldDataType], [vcFieldType], [vcAssociatedControlType], [vcToolTip], [vcListItemType], [numListID], [PopupFunctionName], [order], [tintRow], [tintColumn], [bitInResults], [bitDeleted], [bitAllowEdit], [bitDefault], [bitSettingField], [bitAddField], [bitDetailField], [bitAllowSorting], [bitWorkFlowField], [bitImport], [bitExport], [bitAllowFiltering], [bitInlineEdit], [bitRequired], [intColumnWidth], [intFieldMaxLength])
--SELECT 7, NULL, N'Created By', N'numCreatedBy', N'numCreatedBy', N'', N'Cases', N'N', N'R', N'SelectBox', NULL, N'U', 0, NULL, 16, NULL, NULL, 1, 0, 0, 0, 1, NULL, 0, 1, 1, NULL, NULL, 1, NULL, NULL, NULL, NULL UNION ALL
--SELECT 7, NULL, N'Modified By', N'numModifiedBy', N'numModifiedBy', N'', N'Cases', N'N', N'R', N'SelectBox', NULL, N'U', 0, NULL, 16, NULL, NULL, 1, 0, 0, 0, 1, NULL, 0, 1, 1, NULL, NULL, 1, NULL, NULL, NULL, NULL UNION ALL
--SELECT 7, NULL, N'Modified Date', N'bintModifiedDate', N'bintModifiedDate', N'', N'Cases', N'V', N'R', N'DateField', NULL, N'', 0, NULL, 19, NULL, NULL, 1, 0, 0, 0, 1, NULL, 0, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL
--
--
--///**************************************************************************************///
--
--
--GO
--CREATE TABLE [dbo].[ReportModuleMaster](
--	[numReportModuleID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
--	[vcModuleName] [varchar](100) NULL,
--	[bitActive] [bit] NULL,
-- CONSTRAINT [PK_ReportModuleMaster] PRIMARY KEY CLUSTERED 
--(
--	[numReportModuleID] ASC
--)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
--) ON [PRIMARY]
--
--GO
--
--
--SET IDENTITY_INSERT [dbo].[ReportModuleMaster] ON;
--
--INSERT INTO [dbo].[ReportModuleMaster]([numReportModuleID], [vcModuleName], [bitActive])
--SELECT 1, N'Organization', 1 UNION ALL
--SELECT 2, N'Contacts', 1 UNION ALL
--SELECT 3, N'Opportunities/Orders', 1 UNION ALL
--SELECT 4, N'Projects', 1 UNION ALL
--SELECT 5, N'Cases', 1 UNION ALL
--SELECT 6, N'Items', 1
--
--SET IDENTITY_INSERT [dbo].[ReportModuleMaster] OFF;
--
--
--///*****************************************///
--
--
--GO
--CREATE TABLE [dbo].[ReportModuleGroupMaster](
--	[numReportModuleGroupID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
--	[numReportModuleID] [numeric](18, 0) NOT NULL,
--	[vcGroupName] [varchar](50) NULL,
--	[bitActive] [bit] NULL,
-- CONSTRAINT [PK_ReportModuleGroupMaster] PRIMARY KEY CLUSTERED 
--(
--	[numReportModuleGroupID] ASC
--)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
--) ON [PRIMARY]
--
--GO
--
--
--SET IDENTITY_INSERT [dbo].[ReportModuleGroupMaster] ON;
--
--INSERT INTO [dbo].[ReportModuleGroupMaster]([numReportModuleGroupID], [numReportModuleID], [vcGroupName], [bitActive])
--SELECT 1, 1, N'Organization', 1 UNION ALL
--SELECT 2, 1, N'Organization Association', 0 UNION ALL
--SELECT 3, 1, N'Organization With Assets', 0 UNION ALL
--SELECT 4, 1, N'Orgnization Action Items', 0 UNION ALL
--SELECT 5, 2, N'Contacts', 1 UNION ALL
--SELECT 6, 2, N'Contacts & Action Items', 0 UNION ALL
--SELECT 7, 2, N'Contacts & Organization', 0 UNION ALL
--SELECT 8, 3, N'Opportunities BizDocs', 1 UNION ALL
--SELECT 9, 3, N'Opportunities BizDocs Items', 1 UNION ALL
--SELECT 10, 3, N'Opportunities Items', 1 UNION ALL
--SELECT 11, 3, N'Opportunities Items BizDocs', 0 UNION ALL
--SELECT 12, 3, N'Opportunities MileStones', 0 UNION ALL
--SELECT 13, 3, N'Opportunities/Orders', 1 UNION ALL
--SELECT 14, 4, N'Projects', 1 UNION ALL
--SELECT 15, 4, N'Projects and Stages ', 0 UNION ALL
--SELECT 16, 5, N'Cases', 1 UNION ALL
--SELECT 17, 5, N'Cases Tasks', 0 UNION ALL
--SELECT 18, 5, N'Cases with Contracts', 0 UNION ALL
--SELECT 19, 6, N'Items', 1 UNION ALL
--SELECT 20, 6, N'Items Maintenance & Repair', 0 UNION ALL
--SELECT 21, 6, N'Items with Attributes', 0 UNION ALL
--SELECT 22, 6, N'Items with Options & Accesories', 0 UNION ALL
--SELECT 23, 6, N'WareHouse', 1
--
--SET IDENTITY_INSERT [dbo].[ReportModuleGroupMaster] OFF;
--
--///*****************************************///
--
--
--GO
--CREATE TABLE [dbo].[ReportFieldGroupMaster](
--	[numReportFieldGroupID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
--	[vcFieldGroupName] [varchar](100) NOT NULL,
--	[bitActive] [bit] NOT NULL,
--	[bitCustomFieldGroup] [bit] NULL,
--	[numGroupID] [numeric](18, 0) NULL,
--	[vcCustomTableName] [varchar](50) NULL,
-- CONSTRAINT [PK_ReportFieldGroupMaster] PRIMARY KEY CLUSTERED 
--(
--	[numReportFieldGroupID] ASC
--)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
--) ON [PRIMARY]
--
--GO
--
--SET IDENTITY_INSERT [dbo].[ReportFieldGroupMaster] ON;
--
--INSERT INTO [dbo].[ReportFieldGroupMaster]([numReportFieldGroupID], [vcFieldGroupName], [bitActive], [bitCustomFieldGroup], [numGroupID], [vcCustomTableName])
--SELECT 1, N'Organization', 1, NULL, NULL, NULL UNION ALL
--SELECT 2, N'Contacts', 1, NULL, NULL, NULL UNION ALL
--SELECT 3, N'Opportunities/Orders', 1, NULL, NULL, NULL UNION ALL
--SELECT 4, N'Opportunities Items', 1, NULL, NULL, NULL UNION ALL
--SELECT 5, N'BizDocs', 1, NULL, NULL, NULL UNION ALL
--SELECT 6, N'BizDocs Items', 1, NULL, NULL, NULL UNION ALL
--SELECT 7, N'Items', 1, NULL, NULL, NULL UNION ALL
--SELECT 10, N'Items with Options & Accesories', 1, NULL, NULL, NULL UNION ALL
--SELECT 11, N'WareHouses', 1, NULL, NULL, NULL UNION ALL
--SELECT 15, N'Organization Custom Field', 1, 1, 1, N'CFW_FLD_Values' UNION ALL
--SELECT 16, N'Contacts Custom Field', 1, 1, 4, N'CFW_FLD_Values_Cont' UNION ALL
--SELECT 17, N'Sales Custom Field', 1, 1, 2, N'CFW_Fld_Values_Opp' UNION ALL
--SELECT 18, N'Purchase Custom Field', 1, 1, 6, N'CFW_Fld_Values_Opp' UNION ALL
--SELECT 19, N'Items Custom Field', 1, 1, 5, N'CFW_FLD_Values_Item' UNION ALL
--SELECT 20, N'Items Attributes', 1, 1, 9, N'CFW_Fld_Values_Serialized_Items' UNION ALL
--SELECT 21, N'Projects', 1, NULL, NULL, NULL UNION ALL
--SELECT 22, N'Projects Custom Field', 1, 1, 11, N'CFW_FLD_Values_Pro' UNION ALL
--SELECT 23, N'Cases', 1, NULL, NULL, NULL UNION ALL
--SELECT 24, N'Cases Custom Field', 1, 1, 3, N'CFW_FLD_Values_Case'
--
--SET IDENTITY_INSERT [dbo].[ReportFieldGroupMaster] OFF;
--
--
--///*****************************************///
--
--
--CREATE TABLE [dbo].[ReportModuleGroupFieldMappingMaster](
--	[numReportModuleGroupID] [numeric](18, 0) NOT NULL,
--	[numReportFieldGroupID] [numeric](18, 0) NOT NULL,
-- CONSTRAINT [PK_ReportGroupFieldMaster] PRIMARY KEY CLUSTERED 
--(
--	[numReportModuleGroupID] ASC,
--	[numReportFieldGroupID] ASC
--)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
--) ON [PRIMARY]
--
--
--INSERT INTO [dbo].[ReportModuleGroupFieldMappingMaster]([numReportModuleGroupID], [numReportFieldGroupID])
--SELECT 1, 1 UNION ALL
--SELECT 1, 15 UNION ALL
--SELECT 5, 1 UNION ALL
--SELECT 5, 2 UNION ALL
--SELECT 5, 15 UNION ALL
--SELECT 5, 16 UNION ALL
--SELECT 8, 1 UNION ALL
--SELECT 8, 2 UNION ALL
--SELECT 8, 3 UNION ALL
--SELECT 8, 5 UNION ALL
--SELECT 8, 17 UNION ALL
--SELECT 8, 18 UNION ALL
--SELECT 9, 1 UNION ALL
--SELECT 9, 2 UNION ALL
--SELECT 9, 3 UNION ALL
--SELECT 9, 4 UNION ALL
--SELECT 9, 5 UNION ALL
--SELECT 9, 6 UNION ALL
--SELECT 9, 17 UNION ALL
--SELECT 9, 18 UNION ALL
--SELECT 10, 1 UNION ALL
--SELECT 10, 2 UNION ALL
--SELECT 10, 3 UNION ALL
--SELECT 10, 4 UNION ALL
--SELECT 10, 17 UNION ALL
--SELECT 10, 18 UNION ALL
--SELECT 13, 1 UNION ALL
--SELECT 13, 2 UNION ALL
--SELECT 13, 3 UNION ALL
--SELECT 13, 17 UNION ALL
--SELECT 13, 18 UNION ALL
--SELECT 14, 1 UNION ALL
--SELECT 14, 15 UNION ALL
--SELECT 14, 21 UNION ALL
--SELECT 14, 22 UNION ALL
--SELECT 16, 1 UNION ALL
--SELECT 16, 15 UNION ALL
--SELECT 16, 23 UNION ALL
--SELECT 16, 24 UNION ALL
--SELECT 19, 7 UNION ALL
--SELECT 19, 19 UNION ALL
--SELECT 23, 7 UNION ALL
--SELECT 23, 11 UNION ALL
--SELECT 23, 19 UNION ALL
--SELECT 23, 20
--
--
--///*****************************************///
--
--
--GO
--CREATE TABLE [dbo].[ReportFieldGroupMappingMaster](
--	[numReportFieldGroupID] [numeric](18, 0) NOT NULL,
--	[numFieldID] [numeric](18, 0) NOT NULL,
--	[vcFieldName] [nvarchar](50) NULL,
--	[vcAssociatedControlType] [nvarchar](50) NULL,
--	[vcFieldDataType] [char](1) NULL,
--	[bitAllowSorting] [bit] NULL,
--	[bitAllowGrouping] [bit] NULL,
--	[bitAllowAggregate] [bit] NULL,
--	[bitAllowFiltering] [bit] NULL,
-- CONSTRAINT [PK_ReportFieldGroupMappingMaster] PRIMARY KEY CLUSTERED 
--(
--	[numReportFieldGroupID] ASC,
--	[numFieldID] ASC
--)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
--) ON [PRIMARY]
--
--GO
--
--
--///*****************************************///
--
--GO
--CREATE TABLE [dbo].[ReportListMaster](
--	[numReportID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
--	[vcReportName] [varchar](200) NULL,
--	[vcReportDescription] [varchar](500) NULL,
--	[numDomainID] [numeric](18, 0) NULL,
--	[numReportModuleID] [numeric](18, 0) NULL,
--	[numReportModuleGroupID] [numeric](18, 0) NULL,
--	[tintReportType] [tinyint] NULL,
--	[numCreatedBy] [numeric](18, 0) NULL,
--	[numModifiedBy] [numeric](18, 0) NULL,
--	[dtCreatedDate] [datetime] NULL,
--	[dtModifiedDate] [datetime] NULL,
--	[bitActive] [bit] NULL,
--	[textQuery] [text] NULL,
--	[numSortColumnmReportFieldGroupID] [numeric](18, 0) NULL,
--	[numSortColumnFieldID] [numeric](18, 0) NULL,
--	[vcSortColumnDirection] [varchar](10) NULL,
--	[bitSortColumnCustom] [bit] NULL,
--	[vcFilterLogic] [varchar](200) NULL,
--	[numDateReportFieldGroupID] [numeric](18, 0) NULL,
--	[numDateFieldID] [numeric](18, 0) NULL,
--	[bitDateFieldColumnCustom] [bit] NULL,
--	[vcDateFieldValue] [varchar](50) NULL,
--	[dtFromDate] [datetime] NULL,
--	[dtToDate] [datetime] NULL,
--	[tintRecordFilter] [tinyint] NULL,
--	[vcKPIMeasureFieldID] [varchar](50) NULL,
--	[intNoRows] [int] NULL,
-- CONSTRAINT [PK_ReportListMaster] PRIMARY KEY CLUSTERED 
--(
--	[numReportID] ASC
--)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
--) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
--
--GO
--SET ANSI_PADDING ON
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0: Tabular, 1: Summary, 2:Matrix, 3:KPI, 4:ScoreCard' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ReportListMaster', @level2type=N'COLUMN',@level2name=N'tintReportType'
--
--
--///*****************************************///
--
--
--GO
--CREATE TABLE [dbo].[ReportFieldsList](
--	[numReportFieldID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
--	[numReportID] [numeric](18, 0) NOT NULL,
--	[numFieldID] [numeric](18, 0) NOT NULL,
--	[bitCustom] [bit] NOT NULL,
--	[numReportFieldGroupID] [numeric](18, 0) NOT NULL,
-- CONSTRAINT [PK_ReportFieldsMaster] PRIMARY KEY CLUSTERED 
--(
--	[numReportID] ASC,
--	[numFieldID] ASC,
--	[bitCustom] ASC,
--	[numReportFieldGroupID] ASC
--)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
--) ON [PRIMARY]
--
--
--///*****************************************///
--
--
--GO
--CREATE TABLE [dbo].[ReportFilterList](
--	[numReportFilterID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
--	[numReportID] [numeric](18, 0) NOT NULL,
--	[numFieldID] [numeric](18, 0) NOT NULL,
--	[bitCustom] [bit] NOT NULL,
--	[vcFilterValue] [varchar](500) NOT NULL,
--	[vcFilterOperator] [varchar](50) NOT NULL,
--	[vcFilterText] [varchar](2000) NULL,
--	[numReportFieldGroupID] [numeric](18, 0) NULL
--) ON [PRIMARY]
--
--GO
--
--///*****************************************///
--
--GO
--CREATE TABLE [dbo].[ReportSummaryGroupList](
--	[numReportGroupID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
--	[numReportID] [numeric](18, 0) NOT NULL,
--	[numFieldID] [numeric](18, 0) NOT NULL,
--	[bitCustom] [bit] NOT NULL,
--	[numReportFieldGroupID] [numeric](18, 0) NOT NULL,
--	[tintBreakType] [tinyint] NOT NULL,
-- CONSTRAINT [PK_ReportGroupListMaster] PRIMARY KEY CLUSTERED 
--(
--	[numReportID] ASC,
--	[numFieldID] ASC,
--	[bitCustom] ASC,
--	[numReportFieldGroupID] ASC,
--	[tintBreakType] ASC
--)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
--) ON [PRIMARY]
--
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0:Group Summary 1:Matrix Column, 2:Matrix Row' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ReportSummaryGroupList', @level2type=N'COLUMN',@level2name=N'tintBreakType'
--
--///*****************************************///
--
--GO
--CREATE TABLE [dbo].[ReportMatrixAggregateList](
--	[numReportAggregateID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
--	[numReportID] [numeric](18, 0) NOT NULL,
--	[numFieldID] [numeric](18, 0) NOT NULL,
--	[bitCustom] [bit] NOT NULL,
--	[vcAggType] [varchar](50) NOT NULL,
--	[numReportFieldGroupID] [numeric](18, 0) NULL
--) ON [PRIMARY]
--
--GO
--
--///*****************************************///
--
--GO
--CREATE TABLE [dbo].[ReportKPIThresold](
--	[numReportThresoldID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
--	[numReportID] [numeric](18, 0) NOT NULL,
--	[vcThresoldType] [varchar](50) NULL,
--	[decValue1] [decimal](18, 2) NULL,
--	[decValue2] [decimal](18, 2) NULL,
--	[decGoalValue] [decimal](18, 2) NULL,
-- CONSTRAINT [PK_ReportKPIThresold] PRIMARY KEY CLUSTERED 
--(
--	[numReportThresoldID] ASC
--)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
--) ON [PRIMARY]
--
--GO
--
--///*****************************************///
--
--GO
--CREATE TABLE [dbo].[ReportKPIGroupListMaster](
--	[numReportKPIGroupID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
--	[numDomainID] [numeric](18, 0) NOT NULL,
--	[vcKPIGroupName] [varchar](200) NULL,
--	[vcKPIGroupDescription] [varchar](500) NULL,
--	[tintKPIGroupReportType] [tinyint] NULL,
-- CONSTRAINT [PK_ReportKPIList] PRIMARY KEY CLUSTERED 
--(
--	[numReportKPIGroupID] ASC
--)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
--) ON [PRIMARY]
--
--GO
--SET ANSI_PADDING ON
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'3:KPI 4:ScoreCard' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ReportKPIGroupListMaster', @level2type=N'COLUMN',@level2name=N'tintKPIGroupReportType'
--
--///*****************************************///
--
--GO
--CREATE TABLE [dbo].[ReportKPIGroupDetailList](
--	[numReportKPIGroupID] [numeric](18, 0) NULL,
--	[numReportID] [numeric](18, 0) NULL
--) ON [PRIMARY]
--
--
--///*****************************************///
--
--
--GO
--CREATE TABLE [dbo].[UserReportList](
--	[numUserReportID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
--	[numReportID] [numeric](18, 0) NOT NULL,
--	[numDomainID] [numeric](18, 0) NOT NULL,
--	[numUserCntID] [numeric](18, 0) NOT NULL,
--	[tintReportType] [tinyint] NOT NULL,
-- CONSTRAINT [PK_UserReportList] PRIMARY KEY CLUSTERED 
--(
--	[numUserReportID] ASC
--)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
--) ON [PRIMARY]
--
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0:Pre-defined 1:Custom Reports' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'UserReportList', @level2type=N'COLUMN',@level2name=N'tintReportType'
--
--
--///*****************************************///
--
--
--GO
--CREATE TABLE [dbo].[ReportDashboard](
--	[numDashBoardID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
--	[numDomainID] [numeric](18, 0) NULL,
--	[numReportID] [numeric](18, 0) NOT NULL,
--	[numUserCntID] [numeric](18, 0) NOT NULL,
--	[tintReportType] [tinyint] NOT NULL,
--	[tintChartType] [tinyint] NOT NULL,
--	[vcHeaderText] [varchar](100) NULL,
--	[vcFooterText] [varchar](100) NULL,
--	[tintRow] [tinyint] NOT NULL,
--	[tintColumn] [tinyint] NOT NULL,
--	[vcXAxis] [varchar](50) NULL,
--	[vcYAxis] [varchar](50) NULL,
--	[vcAggType] [varchar](50) NULL,
--	[tintReportCategory] [tinyint] NULL,
-- CONSTRAINT [PK_ReportDashboard] PRIMARY KEY CLUSTERED 
--(
--	[numDashBoardID] ASC
--)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
--) ON [PRIMARY]
--
--GO
--SET ANSI_PADDING ON
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1:Table 2:Chart' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ReportDashboard', @level2type=N'COLUMN',@level2name=N'tintReportType'
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0:Custom Report 1:KPI/ScoreCard Group Report' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ReportDashboard', @level2type=N'COLUMN',@level2name=N'tintReportCategory'
--
--
--ALTER TABLE dbo.ReportDashboard ADD
--	intHeight int NULL,
--	intWidth int NULL
--///*****************************************///
--
--
--GO
--CREATE TABLE [dbo].[ReportDashboardAllowedReports](
--	[numReportID] [numeric](18, 0) NULL,
--	[numDomainID] [numeric](18, 0) NULL,
--	[numGrpID] [numeric](18, 0) NULL,
--	[tintReportCategory] [tinyint] NULL
--) ON [PRIMARY]
--
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0:Custom Report 1:KPI/ScoreCard Group Report' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ReportDashboardAllowedReports', @level2type=N'COLUMN',@level2name=N'tintReportCategory'
--
--
--///*****************************************///
--
--GO
--CREATE TABLE [dbo].[ReportDashBoardSize](
--	[numDomainID] [numeric](18, 0) NULL,
--	[numUserCntID] [numeric](18, 0) NULL,
--	[tintColumn] [tinyint] NULL,
--	[tintSize] [tinyint] NULL
--) ON [PRIMARY]
--
--///*****************************************///
--
--
--INSERT INTO [dbo].[ReportFieldGroupMappingMaster]([numReportFieldGroupID], [numFieldID], [vcFieldName], [vcAssociatedControlType], [vcFieldDataType], [bitAllowSorting], [bitAllowGrouping], [bitAllowAggregate], [bitAllowFiltering])
--SELECT 1, numFieldId, N'Company Differentiation', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=1 AND vcOrigDbColumnName='numCompanyDiff' UNION ALL
--SELECT 1, numFieldId, N'Company Differentiation Value', N'TextBox', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=1 AND vcOrigDbColumnName='vcCompanyDiff' UNION ALL
--SELECT 1, numFieldId, N'Organization Name', N'TextBox', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=1 AND vcOrigDbColumnName='vcCompanyName' UNION ALL
--SELECT 1, numFieldId, N'Organization Profile', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=1 AND vcOrigDbColumnName='vcProfile' UNION ALL
--SELECT 1, numFieldId, N'Organization Relationship', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=1 AND vcOrigDbColumnName='numCompanyType' UNION ALL
--SELECT 1, numFieldId, N'Campaign', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=1 AND vcOrigDbColumnName='numCampaignID' UNION ALL
--SELECT 1, numFieldId, N'Organization Phone', N'TextBox', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=1 AND vcOrigDbColumnName='vcComPhone' UNION ALL
--SELECT 1, numFieldId, N'Assigned To', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=1 AND vcOrigDbColumnName='numAssignedTo' UNION ALL
--SELECT 1, numFieldId, N'Employees', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=1 AND vcOrigDbColumnName='numNoOfEmployeesId' UNION ALL
--SELECT 1, numFieldId, N'Annual Revenue', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=1 AND vcOrigDbColumnName='numAnnualRevID' UNION ALL
--SELECT 1, numFieldId, N'Web', N'TextBox', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=1 AND vcOrigDbColumnName='vcWebSite' UNION ALL
--SELECT 1, numFieldId, N'Follow-up Status', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=1 AND vcOrigDbColumnName='numFollowUpStatus' UNION ALL
--SELECT 1, numFieldId, N'Group', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=1 AND vcOrigDbColumnName='numGrpID' UNION ALL
--SELECT 1, numFieldId, N'Territory', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=1 AND vcOrigDbColumnName='numTerID' UNION ALL
--SELECT 1, numFieldId, N'Lead Source', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=1 AND vcOrigDbColumnName='vcHow' UNION ALL
--SELECT 1, numFieldId, N'Industry', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=1 AND vcOrigDbColumnName='numCompanyIndustry' UNION ALL
--SELECT 1, numFieldId, N'Organization Rating', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=1 AND vcOrigDbColumnName='numCompanyRating' UNION ALL
--SELECT 1, numFieldId, N'Organization Status', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=1 AND vcOrigDbColumnName='numStatusID' UNION ALL
--SELECT 1, numFieldId, N'Organization Credit Limit', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=1 AND vcOrigDbColumnName='numCompanyCredit' UNION ALL
--SELECT 1, numFieldId, N'Organization Fax', N'TextBox', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=1 AND vcOrigDbColumnName='vcComFax' UNION ALL
--SELECT 1, numFieldId, N'Assigned By', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=1 AND vcOrigDbColumnName='numAssignedBy' UNION ALL
--SELECT 1, numFieldId, N'Organization Created Date', N'DateField', N'D', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=1 AND vcOrigDbColumnName='bintCreatedDate' UNION ALL
--SELECT 1, numFieldId, N'Organization Comments', N'TextArea', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=1 AND vcOrigDbColumnName='txtComments' UNION ALL
--SELECT 1, numFieldId, N'Private', N'CheckBox', N'Y', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=1 AND vcOrigDbColumnName='bitPublicFlag' UNION ALL
--SELECT 1, numFieldId, N'Active', N'CheckBox', N'Y', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=1 AND vcOrigDbColumnName='bitActiveInActive' UNION ALL
--SELECT 1, numFieldId, N'Web Link 1', N'TextBox', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=1 AND vcOrigDbColumnName='vcWebLink1' UNION ALL
--SELECT 1, numFieldId, N'Web Link 2', N'TextBox', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=1 AND vcOrigDbColumnName='vcWebLink2' UNION ALL
--SELECT 1, numFieldId, N'Web Link 3', N'TextBox', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=1 AND vcOrigDbColumnName='vcWebLink3' UNION ALL
--SELECT 1, numFieldId, N'Web Link 4', N'TextBox', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=1 AND vcOrigDbColumnName='vcWebLink4' UNION ALL
--SELECT 1, numFieldId, N'Ship To Street', N'TextBox', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=1 AND vcOrigDbColumnName='vcStreet' AND vcDbColumnName='vcShipStreet' UNION ALL
--SELECT 1, numFieldId, N'Ship To City', N'TextBox', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=1 AND vcOrigDbColumnName='vcCity' AND vcDbColumnName='vcShipCity' UNION ALL
--SELECT 1, numFieldId, N'Ship To State', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=1 AND vcOrigDbColumnName='numState' AND vcDbColumnName='numShipState' UNION ALL
--SELECT 1, numFieldId, N'Ship To Postal Code', N'TextBox', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=1 AND vcOrigDbColumnName='vcPostalCode' AND vcDbColumnName='vcShipPostCode' UNION ALL
--SELECT 1, numFieldId, N'Ship To Country', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=1 AND vcOrigDbColumnName='numCountry' AND vcDbColumnName='numShipCountry' UNION ALL
--SELECT 1, numFieldId, N'Bill To Street', N'TextBox', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=1 AND vcOrigDbColumnName='vcStreet' AND vcDbColumnName='vcBillStreet' UNION ALL
--SELECT 1, numFieldId, N'Bill To City', N'TextBox', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=1 AND vcOrigDbColumnName='vcCity' AND vcDbColumnName='vcBillCity' UNION ALL
--SELECT 1, numFieldId, N'Bill To State', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=1 AND vcOrigDbColumnName='numState' AND vcDbColumnName='numBillState' UNION ALL
--SELECT 1, numFieldId, N'Bill To Postal Code', N'TextBox', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=1 AND vcOrigDbColumnName='vcPostalCode' AND vcDbColumnName='vcBillPostCode' UNION ALL
--SELECT 1, numFieldId, N'Bill To Country', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=1 AND vcOrigDbColumnName='numCountry' AND vcDbColumnName='numBillCountry' UNION ALL
--SELECT 1, numFieldId, N'Record Owner', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=1 AND vcOrigDbColumnName='numRecOwner' UNION ALL
--SELECT 1, numFieldId, N'Relationship Type', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=2 AND vcOrigDbColumnName='tintCRMType' UNION ALL
--SELECT 2, numFieldId, N'Contact First Name', N'TextBox', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=2 AND vcOrigDbColumnName='vcFirstName' UNION ALL
--SELECT 2, numFieldId, N'Contact Last Name', N'TextBox', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=2 AND vcOrigDbColumnName='vcLastName' UNION ALL
--SELECT 2, numFieldId, N'Contact Email', N'TextBox', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=2 AND vcOrigDbColumnName='vcEmail' UNION ALL
--SELECT 2, numFieldId, N'Contact Alternate Email', N'TextBox', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=2 AND vcOrigDbColumnName='vcAltEmail' UNION ALL
--SELECT 2, numFieldId, N'Date of Birth', N'DateField', N'D', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=2 AND vcOrigDbColumnName='bintDOB' UNION ALL
--SELECT 2, numFieldId, N'Contact Type', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=2 AND vcOrigDbColumnName='numContactType' UNION ALL
--SELECT 2, numFieldId, N'Manager', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=2 AND vcOrigDbColumnName='numManagerID' UNION ALL
--SELECT 2, numFieldId, N'Contact Phone', N'TextBox', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=2 AND vcOrigDbColumnName='numPhone' UNION ALL
--SELECT 2, numFieldId, N'Contact Phone Ext', N'TextBox', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=2 AND vcOrigDbColumnName='numPhoneExtension' UNION ALL
--SELECT 2, numFieldId, N'Contact Cell Phone', N'TextBox', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=2 AND vcOrigDbColumnName='numCell' UNION ALL
--SELECT 2, numFieldId, N'Contact Home Phone', N'TextBox', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=2 AND vcOrigDbColumnName='numHomePhone' UNION ALL
--SELECT 2, numFieldId, N'Contact Fax', N'TextBox', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=2 AND vcOrigDbColumnName='vcFax' UNION ALL
--SELECT 2, numFieldId, N'Contact Category', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=2 AND vcOrigDbColumnName='vcCategory' UNION ALL
--SELECT 2, numFieldId, N'Contact Team', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=2 AND vcOrigDbColumnName='numTeam' UNION ALL
--SELECT 2, numFieldId, N'Contact Position', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=2 AND vcOrigDbColumnName='vcPosition' UNION ALL
--SELECT 2, numFieldId, N'Contact Title', N'TextBox', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=2 AND vcOrigDbColumnName='vcTitle' UNION ALL
--SELECT 2, numFieldId, N'Department Name', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=2 AND vcOrigDbColumnName='vcDepartment' UNION ALL
--SELECT 2, numFieldId, N'Gender', N'SelectBox', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=2 AND vcOrigDbColumnName='charSex' UNION ALL
--SELECT 2, numFieldId, N'Opt-Out', N'CheckBox', N'Y', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=2 AND vcOrigDbColumnName='bitOptOut' UNION ALL
--SELECT 2, numFieldId, N'Contact Status', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=2 AND vcOrigDbColumnName='numEmpStatus' UNION ALL
--SELECT 2, numFieldId, N'Drip Campaign', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=2 AND vcOrigDbColumnName='numECampaignID' UNION ALL
--SELECT 2, numFieldId, N'Assistant First Name', N'TextBox', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=2 AND vcOrigDbColumnName='vcAsstFirstName' UNION ALL
--SELECT 2, numFieldId, N'Assistant Last Name', N'TextBox', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=2 AND vcOrigDbColumnName='vcAsstLastName' UNION ALL
--SELECT 2, numFieldId, N'Assistant Email', N'TextBox', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=2 AND vcOrigDbColumnName='vcAsstEmail' UNION ALL
--SELECT 2, numFieldId, N'Assistant Phone', N'TextBox', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=2 AND vcOrigDbColumnName='numAsstPhone' UNION ALL
--SELECT 2, numFieldId, N'Assistant Phone Ext', N'TextBox', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=2 AND vcOrigDbColumnName='numAsstExtn' UNION ALL
--SELECT 2, numFieldId, N'Contact Street', N'TextBox', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=2 AND vcOrigDbColumnName='vcStreet' UNION ALL
--SELECT 2, numFieldId, N'Contact City', N'TextBox', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=2 AND vcOrigDbColumnName='vcCity' UNION ALL
--SELECT 2, numFieldId, N'Contact State', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=2 AND vcOrigDbColumnName='numState' UNION ALL
--SELECT 2, numFieldId, N'Contact Country', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=2 AND vcOrigDbColumnName='numCountry' UNION ALL
--SELECT 2, numFieldId, N'Contact Postal Code', N'TextBox', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=2 AND vcOrigDbColumnName='vcPostalCode' UNION ALL
--SELECT 2, numFieldId, N'Contact Record Owner', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=2 AND vcOrigDbColumnName='numRecOwner' UNION ALL
--SELECT 2, numFieldId, N'Contact Created Date', N'DateField', N'D', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=2 AND vcOrigDbColumnName='bintCreatedDate' UNION ALL
--SELECT 2, numFieldId, N'Comments', N'TextArea', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=2 AND vcOrigDbColumnName='txtNotes' UNION ALL
--SELECT 2, numFieldId, N'Primary Contact', N'CheckBox', N'Y', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=2 AND vcOrigDbColumnName='bitPrimaryContact' UNION ALL
--SELECT 3, numFieldId, N'Opp Name', N'TextBox', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=3 AND vcOrigDbColumnName='vcPoppName' UNION ALL
--SELECT 3, numFieldId, N'Source', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=3 AND vcOrigDbColumnName='tintSource' UNION ALL
--SELECT 3, numFieldId, N'Assigned To', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=3 AND vcOrigDbColumnName='numAssignedTo' UNION ALL
--SELECT 3, numFieldId, N'Order Status', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=3 AND vcOrigDbColumnName='numStatus' UNION ALL
--SELECT 3, numFieldId, N'Conclusion Reason', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=3 AND vcOrigDbColumnName='lngPConclAnalysis' UNION ALL
--SELECT 3, numFieldId, N'Order Sub-total', N'TextBox', N'M', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=3 AND vcOrigDbColumnName='monDealAmount' AND vcDbColumnName='CalAmount' UNION ALL
--SELECT 3, numFieldId, N'Active', N'CheckBox', N'Y', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=3 AND vcOrigDbColumnName='tintActive' UNION ALL
--SELECT 3, numFieldId, N'Due Date', N'DateField', N'D', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=3 AND vcOrigDbColumnName='intpEstimatedCloseDate' UNION ALL
--SELECT 3, numFieldId, N'Record Owner', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=3 AND vcOrigDbColumnName='numRecOwner' UNION ALL
--SELECT 3, numFieldId, N'Assigned By', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=3 AND vcOrigDbColumnName='numAssignedBy' UNION ALL
--SELECT 3, numFieldId, N'Campaign', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=3 AND vcOrigDbColumnName='numCampainID' UNION ALL
--SELECT 3, numFieldId, N'Closing Date', N'DateField', N'D', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=3 AND vcOrigDbColumnName='bintAccountClosingDate' UNION ALL
--SELECT 3, numFieldId, N'Opp Type', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=3 AND vcOrigDbColumnName='tintOppType' UNION ALL
--SELECT 3, numFieldId, N'Created Date', N'DateField', N'D', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=3 AND vcOrigDbColumnName='bintCreatedDate' UNION ALL
--SELECT 3, numFieldId, N'Comments', N'TextArea', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=3 AND vcOrigDbColumnName='txtComments' UNION ALL
--SELECT 3, numFieldId, N'Invoice Grand-total', N'TextBox', N'M', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=3 AND vcOrigDbColumnName='monDealAmount' AND vcDbColumnName='monDealAmount' AND vcLookBackTableName='OpportunityBizDocs' UNION ALL
--SELECT 3, numFieldId, N'Total Amount Paid', N'TextBox', N'M', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=3 AND vcOrigDbColumnName='monAmountPaid' UNION ALL
--SELECT 3, numFieldId, N'Order Currency', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=3 AND vcOrigDbColumnName='numCurrencyID' UNION ALL
--SELECT 3, numFieldId, N'Customer PO# / Vendor Invoice#', N'TextBox', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=3 AND vcOrigDbColumnName='vcOppRefOrderNo' UNION ALL
--SELECT 3, numFieldId, N'Percentage Complete', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=3 AND vcOrigDbColumnName='numPercentageComplete' UNION ALL
--SELECT 3, numFieldId, N'Marketplace Order ID', N'TextBox', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=3 AND vcOrigDbColumnName='vcMarketplaceOrderID' UNION ALL
--SELECT 4, numFieldId, N'Item Name', N'TextBox', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=3 AND vcOrigDbColumnName='vcItemName' UNION ALL
--SELECT 4, numFieldId, N'Model ID', N'TextBox', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=3 AND vcOrigDbColumnName='vcModelID' UNION ALL
--SELECT 4, numFieldId, N'Description', N'TextBox', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=3 AND vcOrigDbColumnName='vcItemDesc' AND vcLookBackTableName='OpportunityItems' UNION ALL
--SELECT 4, numFieldId, N'Type', N'TextBox', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=3 AND vcOrigDbColumnName='vcType' UNION ALL
--SELECT 4, numFieldId, N'Drop Ship', N'CheckBox', N'Y', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=3 AND vcOrigDbColumnName='bitDropShip' UNION ALL
--SELECT 4, numFieldId, N'Units', N'TextBox', N'M', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=3 AND vcOrigDbColumnName='numUnitHour' AND vcLookBackTableName='OpportunityItems' UNION ALL
--SELECT 4, numFieldId, N'Unit Price', N'TextBox', N'M', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=3 AND vcOrigDbColumnName='monPrice' AND vcLookBackTableName='OpportunityItems' UNION ALL
--SELECT 4, numFieldId, N'Amount', N'TextBox', N'M', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=3 AND vcOrigDbColumnName='monTotAmount' AND vcLookBackTableName='OpportunityItems' UNION ALL
--SELECT 4, numFieldId, N'Project', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=3 AND vcOrigDbColumnName='numProjectID' UNION ALL
--SELECT 4, numFieldId, N'Class', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=3 AND vcOrigDbColumnName='numClassID' UNION ALL
--SELECT 4, numFieldId, N'Unit of Measure', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=3 AND vcOrigDbColumnName='numUOMId' UNION ALL
--SELECT 4, numFieldId, N'Manufacturer', N'TextBox', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=3 AND vcOrigDbColumnName='vcManufacturer' UNION ALL
--SELECT 5, numFieldId, N'Customer PO# / Vendor Invoice#', N'TextBox', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=3 AND vcOrigDbColumnName='vcRefOrderNo' UNION ALL
--SELECT 5, numFieldId, N'Shipping Company', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=3 AND vcOrigDbColumnName='numShipVia' UNION ALL
--SELECT 5, numFieldId, N'BizDoc ID', N'TextBox', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=3 AND vcOrigDbColumnName='vcBizDocID' UNION ALL
--SELECT 5, numFieldId, N'BizDoc Type', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=3 AND vcOrigDbColumnName='numBizDocId' UNION ALL
--SELECT 5, numFieldId, N'BizDoc Status', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=3 AND vcOrigDbColumnName='numBizDocStatus' UNION ALL
--SELECT 5, numFieldId, N'Deal Amount', N'TextBox', N'M', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=3 AND vcOrigDbColumnName='monDealAmount' AND vcLookBackTableName='OpportunityBizDocs' UNION ALL
--SELECT 5, numFieldId, N'Amount Paid', N'TextBox', N'M', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=3 AND vcOrigDbColumnName='monAmountPaid' UNION ALL
--SELECT 5, numFieldId, N'Tracking No', N'Label', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=3 AND vcOrigDbColumnName='vcTrackingNo' AND vcLookBackTableName='OpportunityBizDocs' UNION ALL
--SELECT 5, numFieldId, N'Amount Due', N'TextBox', N'M', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=3 AND vcOrigDbColumnName='monAmountDue' UNION ALL
--SELECT 5, numFieldId, N'Billing Date', N'DateField', N'D', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=3 AND vcOrigDbColumnName='dtFromDate' UNION ALL
--SELECT 5, numFieldId, N'Created By', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=3 AND vcOrigDbColumnName='numCreatedBy' UNION ALL
--SELECT 5, numFieldId, N'Created Date', N'DateField', N'D', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=3 AND vcOrigDbColumnName='dtCreatedDate' UNION ALL
--SELECT 5, numFieldId, N'Deferred', N'CheckBox', N'Y', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=3 AND vcOrigDbColumnName='tintDeferred' UNION ALL
--SELECT 5, numFieldId, N'Due Date', N'DateField', N'D', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=3 AND vcOrigDbColumnName='dtDueDate' UNION ALL
--SELECT 5, numFieldId, N'Is Authoritative', N'CheckBox', N'Y', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=3 AND vcOrigDbColumnName='bitAuthoritativeBizDocs' UNION ALL
--SELECT 5, numFieldId, N'Modified By', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=3 AND vcOrigDbColumnName='numModifiedBy' UNION ALL
--SELECT 5, numFieldId, N'Modified Date', N'DateField', N'D', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=3 AND vcOrigDbColumnName='dtModifiedDate' UNION ALL
--SELECT 6, numFieldId, N'Units', N'TextBox', N'M', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=3 AND vcOrigDbColumnName='numUnitHour' AND vcLookBackTableName='OpportunityBizDocItems' UNION ALL
--SELECT 6, numFieldId, N'Unit Price', N'TextBox', N'M', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=3 AND vcOrigDbColumnName='monPrice' AND vcLookBackTableName='OpportunityBizDocItems' UNION ALL
--SELECT 6, numFieldId, N'Amount', N'TextBox', N'M', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=3 AND vcOrigDbColumnName='monTotAmount' AND vcLookBackTableName='OpportunityBizDocItems' UNION ALL
--SELECT 7, numFieldId, N'Item', N'TextBox', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=4 AND vcOrigDbColumnName='vcItemName' UNION ALL
--SELECT 7, numFieldId, N'List Price (NI)', N'TextBox', N'M', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=4 AND vcOrigDbColumnName='monListPrice' UNION ALL
--SELECT 7, numFieldId, N'Model ID', N'TextBox', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=4 AND vcOrigDbColumnName='vcModelID' UNION ALL
--SELECT 7, numFieldId, N'Manufacturer', N'TextBox', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=4 AND vcOrigDbColumnName='vcManufacturer' UNION ALL
--SELECT 7, numFieldId, N'UPC(NI)', N'TextBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=4 AND vcOrigDbColumnName='numBarCodeId' UNION ALL
--SELECT 7, numFieldId, N'Width', N'TextBox', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=4 AND vcOrigDbColumnName='fltWidth' UNION ALL
--SELECT 7, numFieldId, N'Height', N'TextBox', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=4 AND vcOrigDbColumnName='fltHeight' UNION ALL
--SELECT 7, numFieldId, N'Weight', N'TextBox', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=4 AND vcOrigDbColumnName='fltWeight' UNION ALL
--SELECT 7, numFieldId, N'Length', N'TextBox', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=4 AND vcOrigDbColumnName='fltLength' UNION ALL
--SELECT 7, numFieldId, N'Average Cost', N'TextBox', N'M', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=4 AND vcOrigDbColumnName='monAverageCost' UNION ALL
--SELECT 7, numFieldId, N'Is Serialized', N'CheckBox', N'Y', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=4 AND vcOrigDbColumnName='bitSerialized' UNION ALL
--SELECT 7, numFieldId, N'Is Kit', N'CheckBox', N'Y', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=4 AND vcOrigDbColumnName='bitKitParent' UNION ALL
--SELECT 7, numFieldId, N'Is Assembly', N'CheckBox', N'Y', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=4 AND vcOrigDbColumnName='bitAssembly' UNION ALL
--SELECT 7, numFieldId, N'Is Archive', N'CheckBox', N'Y', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=4 AND vcOrigDbColumnName='IsArchieve' UNION ALL
--SELECT 7, numFieldId, N'Is Lot No', N'CheckBox', N'Y', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=4 AND vcOrigDbColumnName='bitLotNo' UNION ALL
--SELECT 7, numFieldId, N'Allow Back Order', N'CheckBox', N'Y', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=4 AND vcOrigDbColumnName='bitAllowBackOrder' UNION ALL
--SELECT 7, numFieldId, N'Item Classification', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=4 AND vcOrigDbColumnName='numItemClassification' UNION ALL
--SELECT 7, numFieldId, N'Item Group', N'SelectBox', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=4 AND vcOrigDbColumnName='numItemGroup' UNION ALL
--SELECT 7, numFieldId, N'Purchase Unit', N'SelectBox', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=4 AND vcOrigDbColumnName='numPurchaseUnit' UNION ALL
--SELECT 7, numFieldId, N'Sale Unit', N'SelectBox', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=4 AND vcOrigDbColumnName='numSaleUnit' UNION ALL
--SELECT 7, numFieldId, N'Item Class', N'SelectBox', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=4 AND vcOrigDbColumnName='numItemClass' UNION ALL
--SELECT 7, numFieldId, N'Labour Cost', N'TextBox', N'M', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=4 AND vcOrigDbColumnName='monCampaignLabourCost' UNION ALL
--SELECT 7, numFieldId, N'Base Unit', N'SelectBox', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=4 AND vcOrigDbColumnName='numBaseUnit' UNION ALL
--SELECT 7, numFieldId, N'SKU(NI)', N'TextBox', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=4 AND vcOrigDbColumnName='vcSKU' UNION ALL
--SELECT 7, numFieldId, N'Item Type', N'SelectBox', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=4 AND vcOrigDbColumnName='charItemType' UNION ALL
--SELECT 7, numFieldId, N'Free Shipping', N'CheckBox', N'Y', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=4 AND vcOrigDbColumnName='bitFreeShipping' UNION ALL
--SELECT 7, numFieldId, N'Has Sales Tax', N'CheckBox', N'Y', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=4 AND vcOrigDbColumnName='bitTaxable' UNION ALL
--SELECT 7, numFieldId, N'Allow DropShip', N'CheckBox', N'Y', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=4 AND vcOrigDbColumnName='bitAllowDropShip' UNION ALL
--SELECT 7, numFieldId, N'Ship Class', N'SelectBox', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=4 AND vcOrigDbColumnName='numShipClass' UNION ALL
--SELECT 11, numFieldId, N'On Hand', N'TextBox', N'M', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=4 AND vcOrigDbColumnName='numOnHand' UNION ALL
--SELECT 11, numFieldId, N'On Order', N'TextBox', N'M', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=4 AND vcOrigDbColumnName='numOnOrder' UNION ALL
--SELECT 11, numFieldId, N'On Allocation', N'TextBox', N'M', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=4 AND vcOrigDbColumnName='numAllocation' UNION ALL
--SELECT 11, numFieldId, N'On Backorder', N'TextBox', N'M', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=4 AND vcOrigDbColumnName='numBackOrder' UNION ALL
--SELECT 11, numFieldId, N'WareHouse', N'TextBox', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=4 AND vcOrigDbColumnName='vcWareHouse' UNION ALL
--SELECT 11, numFieldId, N'Reorder', N'TextBox', N'M', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=4 AND vcOrigDbColumnName='numReorder' UNION ALL
--SELECT 11, numFieldId, N'Warehouse Location', N'SelectBox', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=4 AND vcOrigDbColumnName='vcLocation' UNION ALL
--SELECT 11, numFieldId, N'List Price (I)', N'TextBox', N'M', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=4 AND vcOrigDbColumnName='monWListPrice' UNION ALL
--SELECT 11, numFieldId, N'UPC (M)', N'TextBox', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=4 AND vcOrigDbColumnName='vcBarCode' UNION ALL
--SELECT 11, numFieldId, N'SKU (M)', N'TextBox', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=4 AND vcOrigDbColumnName='vcWHSKU' UNION ALL
--SELECT 11, numFieldId, N'Street', N'TextBox', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=4 AND vcOrigDbColumnName='vcWStreet' UNION ALL
--SELECT 11, numFieldId, N'City', N'TextBox', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=4 AND vcOrigDbColumnName='vcWCity' UNION ALL
--SELECT 11, numFieldId, N'PinCode', N'TextBox', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=4 AND vcOrigDbColumnName='vcWPinCode' UNION ALL
--SELECT 11, numFieldId, N'Country', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=4 AND vcOrigDbColumnName='numWCountry' UNION ALL
--SELECT 11, numFieldId, N'State', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=4 AND vcOrigDbColumnName='numWState' UNION ALL
--SELECT 21, numFieldId, N'Project ID', N'TextBox', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=5 AND vcOrigDbColumnName='vcProjectID' UNION ALL
--SELECT 21, numFieldId, N'Project Name', N'TextBox', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=5 AND vcOrigDbColumnName='vcProjectName' UNION ALL
--SELECT 21, numFieldId, N'Assigned To', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=5 AND vcOrigDbColumnName='numAssignedTo' UNION ALL
--SELECT 21, numFieldId, N'Internal Project Manager', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=5 AND vcOrigDbColumnName='numintPrjMgr' UNION ALL
--SELECT 21, numFieldId, N'Customer Side Project Manager', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=5 AND vcOrigDbColumnName='numCustPrjMgr' UNION ALL
--SELECT 21, numFieldId, N'Due Date', N'DateField', N'D', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=5 AND vcOrigDbColumnName='intDueDate' UNION ALL
--SELECT 21, numFieldId, N'Contract', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=5 AND vcOrigDbColumnName='numContractId' UNION ALL
--SELECT 21, numFieldId, N'Created By', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=5 AND vcOrigDbColumnName='numCreatedBy' UNION ALL
--SELECT 21, numFieldId, N'Created Date', N'DateField', N'D', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=5 AND vcOrigDbColumnName='bintCreatedDate' UNION ALL
--SELECT 21, numFieldId, N'Record Owner', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=5 AND vcOrigDbColumnName='numRecOwner' UNION ALL
--SELECT 21, numFieldId, N'Assigned By', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=5 AND vcOrigDbColumnName='numAssignedBy' UNION ALL
--SELECT 21, numFieldId, N'ClosingDate', N'DateField', N'D', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=5 AND vcOrigDbColumnName='bintProClosingDate' UNION ALL
--SELECT 21, numFieldId, N'Project Type', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=5 AND vcOrigDbColumnName='numProjectType' UNION ALL
--SELECT 21, numFieldId, N'Project Status', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=5 AND vcOrigDbColumnName='numProjectStatus' UNION ALL
--SELECT 23, numFieldId, N'Subject', N'TextBox', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=7 AND vcOrigDbColumnName='textSubject' UNION ALL
--SELECT 23, numFieldId, N'Priority', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=7 AND vcOrigDbColumnName='numPriority' UNION ALL
--SELECT 23, numFieldId, N'Origin', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=7 AND vcOrigDbColumnName='numOrigin' UNION ALL
--SELECT 23, numFieldId, N'Resolve Date', N'DateField', N'D', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=7 AND vcOrigDbColumnName='intTargetResolveDate' UNION ALL
--SELECT 23, numFieldId, N'Type', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=7 AND vcOrigDbColumnName='numType' UNION ALL
--SELECT 23, numFieldId, N'Assigned To', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=7 AND vcOrigDbColumnName='numAssignedTo' UNION ALL
--SELECT 23, numFieldId, N'Reason', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=7 AND vcOrigDbColumnName='numReason' UNION ALL
--SELECT 23, numFieldId, N'Status', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=7 AND vcOrigDbColumnName='numStatus' UNION ALL
--SELECT 23, numFieldId, N'Case Number', N'TextBox', N'V', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=7 AND vcOrigDbColumnName='vcCaseNumber' UNION ALL
--SELECT 23, numFieldId, N'Record Owner', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=7 AND vcOrigDbColumnName='numRecOwner' UNION ALL
--SELECT 23, numFieldId, N'Assigned By', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=7 AND vcOrigDbColumnName='numAssignedBy' UNION ALL
--SELECT 23, numFieldId, N'Created Date', N'DateField', N'D', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=7 AND vcOrigDbColumnName='bintCreatedDate' UNION ALL
--SELECT 23, numFieldId, N'Contract', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=7 AND vcOrigDbColumnName='numContractId' UNION ALL
--SELECT 23, numFieldId, N'Created By', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=7 AND vcOrigDbColumnName='numCreatedBy' UNION ALL
--SELECT 23, numFieldId, N'Modified By', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=7 AND vcOrigDbColumnName='numModifiedBy' UNION ALL
--SELECT 23, numFieldId, N'Modified Date', N'DateField', N'D', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=7 AND vcOrigDbColumnName='bintModifiedDate'
--
--
--------------------------------------------
--
--INSERT INTO [dbo].[DycFieldMaster]([numModuleID], [numDomainID], [vcFieldName], [vcDbColumnName], [vcOrigDbColumnName], [vcPropertyName], [vcLookBackTableName], [vcFieldDataType], [vcFieldType], [vcAssociatedControlType], [vcToolTip], [vcListItemType], [numListID], [PopupFunctionName], [order], [tintRow], [tintColumn], [bitInResults], [bitDeleted], [bitAllowEdit], [bitDefault], [bitSettingField], [bitAddField], [bitDetailField], [bitAllowSorting], [bitWorkFlowField], [bitImport], [bitExport], [bitAllowFiltering], [bitInlineEdit], [bitRequired], [intColumnWidth], [intFieldMaxLength])
--SELECT 4, NULL, N'Created By', N'numCreatedBy', N'numCreatedBy', NULL, N'Item', N'N', N'R', N'SelectBox', NULL, 'U', 0, NULL, 18, NULL, NULL, 1, 0, 0, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL
--UNION
--SELECT 4, NULL, N'Created Date', N'bintCreatedDate', N'bintCreatedDate', NULL, N'Item', N'V', N'R', N'DateField', NULL, NULL, 0, NULL, 18, NULL, NULL, 1, 0, 0, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL
--UNION
--SELECT 4, NULL, N'Modified By', N'numModifiedBy', N'numModifiedBy', NULL, N'Item', N'N', N'R', N'SelectBox', NULL, 'U', 0, NULL, 18, NULL, NULL, 1, 0, 0, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL
--UNION
--SELECT 4, NULL, N'Modified Date', N'bintModifiedDate', N'bintModifiedDate', NULL, N'Item', N'V', N'R', N'DateField', NULL, NULL, 0, NULL, 18, NULL, NULL, 1, 0, 0, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL
--
--
--INSERT INTO [dbo].[ReportFieldGroupMappingMaster]([numReportFieldGroupID], [numFieldID], [vcFieldName], [vcAssociatedControlType], [vcFieldDataType], [bitAllowSorting], [bitAllowGrouping], [bitAllowAggregate], [bitAllowFiltering])
--SELECT 7, numFieldId, N'Created By', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=4 AND vcOrigDbColumnName='numCreatedBy' UNION ALL
--SELECT 7, numFieldId, N'Created Date', N'DateField', N'D', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=4 AND vcOrigDbColumnName='bintCreatedDate' UNION ALL
--SELECT 7, numFieldId, N'Modified By', N'SelectBox', N'N', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=4 AND vcOrigDbColumnName='numModifiedBy' UNION ALL
--SELECT 7, numFieldId, N'Modified Date', N'DateField', N'D', 1, 1, 1, 1 FROM DycFieldMaster WHERE numModuleID=4 AND vcOrigDbColumnName='bintModifiedDate'
--

/************************************************************************************************/
/************************29_Apr_2013*******************************************************************/
/************************************************************************************************/

----List Price (NI+I) to List Price
----SELECT * FROM dbo.DycFormField_Mapping WHERE numFormID=29
--UPDATE DycFormField_Mapping SET vcFieldName='List Price' WHERE numFieldID=190 AND numFormID=29
--
--
----SELECT * FROM BillHeader WHERE monAmountDue=monAmtPaid AND bitIsPaid=0
--UPDATE BillHeader SET bitIsPaid=1 WHERE monAmountDue=monAmtPaid AND bitIsPaid=0
--


/************************************************************************************************/
/************************16_Mar_2013*******************************************************************/
/************************************************************************************************/


----SELECT * FROM CFw_Grp_Master WHERE Loc_Id=13 AND grp_name='Orders'
--UPDATE CFw_Grp_Master SET grp_name='Transactions' WHERE Loc_Id=13 AND grp_name='Orders'
--
----SELECT * FROM CFw_Grp_Master WHERE Loc_Id=12 AND grp_name='Orders'
--UPDATE CFw_Grp_Master SET grp_name='Transactions' WHERE Loc_Id=12 AND grp_name='Orders'
--
--
--ALTER TABLE BizDocComission
--ALTER COLUMN decCommission DECIMAL(18,2)
--
--ALTER TABLE CommissionRuleDtl
--ALTER COLUMN intFrom DECIMAL(18,2)
--
--ALTER TABLE CommissionRuleDtl
--ALTER COLUMN intTo DECIMAL(18,2)
--
--
--DECLARE @numPageID NUMERIC 
--SELECT @numPageID = MAX(numPageID) + 1 FROM dbo.PageMaster WHERE numModuleID=10
--INSERT INTO PageMaster VALUES (@numPageID,10,'frmOpportunities.aspx','Re-open deal',1,0,0,0,0,NULL,NULL)
--
--
--INSERT INTO dbo.GroupAuthorization
--        ( numGroupID ,numModuleID ,numPageID ,intExportAllowed ,intPrintAllowed ,
--          intViewAllowed ,intAddAllowed ,intUpdateAllowed ,intDeleteAllowed ,numDomainID
--        )
--
-- SELECT   AGM.numGroupID ,PM.numModuleID ,PM.numPageID ,PM.bitIsExportApplicable  AS intExportAllowed ,
--                 0 AS intPrintAllowed ,PM.bitIsViewApplicable  AS  intViewAllowed ,PM.bitIsAddApplicable  AS  intAddAllowed ,
--                PM.bitIsUpdateApplicable  AS  intUpdateAllowed ,PM.bitIsDeleteApplicable  AS  intDeleteAllowed,AGM.numDomainID
--FROM dbo.AuthenticationGroupMaster AGM CROSS JOIN dbo.PageMaster PM 
--WHERE numPageID = 30 AND numModuleID=10


/************************************************************************************************/
/************************21_Feb_2013*******************************************************************/
/************************************************************************************************/


--DROP TABLE OpportunityFulfillmentBizDocs
--DROP TABLE OpportunityFulfillmentRules
--
--
--CREATE TABLE [dbo].[OpportunityAutomationRules](
--	[numRuleID] [numeric](18, 0) NOT NULL,
--	[numBizDocStatus1] [numeric](18, 0) NULL,
--	[numBizDocStatus2] [numeric](18, 0) NULL,
--	[numDomainID] [numeric](18, 0) NOT NULL,
--	[tintOppType] [tinyint] NOT NULL,
--	[numOrderStatus] [numeric](18, 0) NULL
--) ON [PRIMARY]
--
--
--
--CREATE TABLE [dbo].[OpportunityAutomationQueue](
--	[numOppQueueID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
--	[numDomainID] [numeric](18, 0) NULL,
--	[numOppId] [numeric](18, 0) NULL,
--	[numOppBizDocsId] [numeric](18, 0) NULL,
--	[numBizDocStatus] [numeric](18, 0) NULL,
--	[numCreatedBy] [numeric](18, 0) NULL,
--	[dtCreatedDate] [datetime] NULL,
--	[tintProcessStatus] [tinyint] NULL,
--	[numRuleID] [numeric](18, 0) NULL,
--	[numOrderStatus] [numeric](18, 0) NULL,
--	[numShippingReportID] [numeric](18, 0) NULL,
-- CONSTRAINT [PK_OpportunityAutomationQueue] PRIMARY KEY CLUSTERED 
--(
--	[numOppQueueID] ASC
--)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
--) ON [PRIMARY]
--
--GO
--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1:Pending Execution 2:In Progress 3:Success 4:Failed 5:Change Status' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'OpportunityAutomationQueue', @level2type=N'COLUMN',@level2name=N'tintProcessStatus'
--
--
--
--CREATE TABLE [dbo].[OpportunityAutomationQueueExecution](
--	[numOppQueueExeID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
--	[numOppQueueID] [numeric](18, 0) NULL,
--	[bitSuccess] [bit] NULL,
--	[vcDescription] [varchar](1000) NULL,
--	[dtExecutionDate] [datetime] NULL,
--	[numRuleID] [numeric](18, 0) NULL,
--	[numOppId] [numeric](18, 0) NULL,
--	[numOppBizDocsId] [numeric](18, 0) NULL,
--	[numOrderStatus] [numeric](18, 0) NULL,
--	[numBizDocStatus] [numeric](18, 0) NULL,
-- CONSTRAINT [PK_OpportunityAutomationQueueExecution] PRIMARY KEY CLUSTERED 
--(
--	[numOppQueueExeID] ASC
--)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
--) ON [PRIMARY]
--
-------Add to app.congif of Service------------------------
--
--    <add key="OpportunityAutomation" value="1"/>
--    
-----------------------------------------------------------
--    
--    ALTER TABLE dbo.Domain ADD
--	numDiscountServiceItemID NUMERIC(18,0) NULL	
--
--    
----------Return hide discount column-------------------------------------------------
--   
----SELECT * FROM dbo.DynamicFormMaster WHERE numFormId IN (60,61,62,63)
----SELECT * FROM dbo.DycFormField_Mapping WHERE numFormId IN (60,61,62,63) AND vcPropertyName='TotalDiscount'
--
--UPDATE DycFormField_Mapping SET bitSettingField=0,bitAddField=0,bitDetailField=0 WHERE numFormId IN (60,61,62,63) AND vcPropertyName='TotalDiscount'
--
----------Item Price Level-------------------------------------------------
--
----ALTER TABLE dbo.Domain ADD
----	bitItemPriceLevel bit NULL
--
--ALTER TABLE dbo.PricingTable ADD
--	numItemCode numeric(18, 0) NULL
--	
--	
--ALTER TABLE dbo.PricingTable
--	DROP CONSTRAINT FK_PricingTable_PriceBookRules	
--	
--	
----ALTER TABLE [dbo].[PricingTable]  WITH CHECK ADD  CONSTRAINT [FK_PricingTable_PriceBookRules] FOREIGN KEY([numPriceRuleID])
----REFERENCES [dbo].[PriceBookRules] ([numPricRuleID])
----GO
----ALTER TABLE [dbo].[PricingTable] CHECK CONSTRAINT [FK_PricingTable_PriceBookRules]	
--
--
-----------Project Commission------------------------------------------------
--
----SELECT tintComAppliesTo,* FROM domain WHERE ISNULL(tintComAppliesTo,0)=0
--UPDATE domain SET tintComAppliesTo=3 WHERE ISNULL(tintComAppliesTo,0)=0
--
----SELECT tintCommissionType,* FROM domain WHERE ISNULL(tintCommissionType,0)=0
--UPDATE domain SET tintCommissionType=1 WHERE ISNULL(tintCommissionType,0)=0
--
--
--ALTER TABLE dbo.BizDocComission ADD
--	numProId numeric(18, 0) NULL
--	
--
--ALTER TABLE dbo.ProjectsMaster ADD
--	dtCompletionDate datetime NULL,
--	monTotalExpense money NULL,
--	monTotalIncome money NULL,
--	monTotalGrossProfit money NULL	
--	
--
--ALTER TABLE dbo.BizDocComission ADD
--	dtProCompletionDate datetime NULL,
--	monProTotalExpense money NULL,
--	monProTotalIncome money NULL,
--	monProTotalGrossProfit money NULL	


/************************************************************************************************/
/************************28_Jan_2013*******************************************************************/
/************************************************************************************************/

--ALTER TABLE dbo.Domain  ALTER COLUMN
--	bitCreateInvoice TINYINT NULL
--
--
--ALTER TABLE dbo.Domain ADD
--	numSOBizDocStatus numeric(18, 0) NULL
--	
----------------------------------------------------------------------------	
--
--ALTER TABLE dbo.Domain ADD
--	bitAutolinkUnappliedPayment bit NULL	
--	
--
--INSERT INTO dbo.AccountingChargeTypes (
--	vcChageType,
--	vcChargeAccountCode,
--	chChargeCode
--) VALUES ( 
--	/* vcChageType - varchar(100) */ 'Purchase Clearing',
--	/* vcChargeAccountCode - varchar(50) */ '010201',
--	/* chChargeCode - char(10) */ 'PC' ) 	
--
--
--INSERT INTO dbo.AccountingChargeTypes (
--	vcChageType,
--	vcChargeAccountCode,
--	chChargeCode
--) VALUES ( 
--	/* vcChageType - varchar(100) */ 'Purchase Price Variance',
--	/* vcChargeAccountCode - varchar(50) */ '0106',
--	/* chChargeCode - char(10) */ 'PV' ) 	
--	
----------------------------------------------------------------------------	
--
--ALTER TABLE dbo.OppWarehouseSerializedItem ADD
--	numReturnHeaderID numeric(18, 0) NULL,
--	numReturnItemID numeric(18, 0) NULL	
--
----------------------------------------------------------------------------	
--
--ALTER TABLE dbo.OpportunityBizDocs ADD
--	fltExchangeRateBizDoc float(53) NULL	
--
--
----ALTER TABLE dbo.Domain ADD
----	bitAllowPPVariance bit NULL
--
----------------------------------------------------------------------------	
--
----SELECT * FROM dbo.General_Journal_Details WHERE tintReferenceType=6 and numCustomerID=0
--
--UPDATE GJD SET numCustomerID=(SELECT TOP 1 numCustomerID FROM General_Journal_Details WHERE tintReferenceType=7 AND GJD.numJournalID=numJournalID) 
--FROM General_Journal_Details GJD
--WHERE tintReferenceType=6 and numCustomerID=0
--
----------------------------------------------------------------------------	
--ALTER TABLE dbo.TempARRecord ADD
--	monUnAppliedAmount money NULL
--
--ALTER TABLE dbo.TempARRecord1 ADD
--	monUnAppliedAmount money NULL
--	
--ALTER TABLE dbo.TempARRecord1 ADD
--	numTotal money NULL
--
--	
--	
----------------------------------------------------------------------------	
----SELECT OM.numDomainId,OM.tintSourceType,monAmountPaid,OBD.monDealAmount,OBD.* FROM dbo.OpportunityBizDocs OBD JOIN dbo.OpportunityMaster OM ON OBD.numOppId = OM.numOppId
---- WHERE monAmountPaid > OBD.monDealAmount and OM.tintOppType=1 
-- 
-- UPDATE OBD SET monAmountPaid=OBD.monDealAmount FROM dbo.OpportunityBizDocs OBD JOIN dbo.OpportunityMaster OM ON OBD.numOppId = OM.numOppId
-- WHERE monAmountPaid > OBD.monDealAmount and OM.tintOppType=1  
-- 	
-- 
-- --------------------------------------------------------------------------	
-- --SELECT * FROM BillPaymentHeader BPH WHERE monPaymentAmount IS NULL
--
-- UPDATE BillPaymentHeader SET monAppliedAmount=(SELECT ISNULL(SUM(ISNULL(monAmount,0)),0) FROM BillPaymentDetails WHERE numBillPaymentID=BillPaymentHeader.numBillPaymentID),
-- 								monPaymentAmount=(SELECT ISNULL(SUM(ISNULL(monAmount,0)),0) FROM BillPaymentDetails WHERE numBillPaymentID=BillPaymentHeader.numBillPaymentID)
--  							WHERE monPaymentAmount IS NULL 
-- 
-- --------------------------------------------------------------------------	
-- --SELECT * FROM dbo.General_Journal_Header WHERE numBillPaymentID>0 AND numOppId>0 AND numOppBizDocsId>0
-- UPDATE dbo.General_Journal_Header SET numOppId=NULL,numOppBizDocsId=NULL WHERE numBillPaymentID>0 AND numOppId>0 AND numOppBizDocsId>0
--  	
-- 
--  --------------------------------------------------------------------------	
--  	SELECT GJH.numAmount,OBD.monDealAmount,OM.tintOppType
--	 FROM  dbo.General_Journal_Header GJH 
--	INNER JOIN dbo.OpportunityBizDocs OBD ON GJH.numOppBizDocsId = OBD.numOppBizDocsId AND GJH.numOppId = OBD.numOppId
--	JOIN dbo.OpportunityMaster OM ON OM.numOppId = OBD.numOppId 
--	WHERE GJH.numAmount!=OBD.monDealAmount  AND OBD.bitAuthoritativeBizDocs=1
--	
--	
-- UPDATE GJH SET numAmount=(SELECT SUM(numDebitAmt) FROM dbo.General_Journal_Details WHERE numJournalID=GJH.numJournal_ID)
--	 FROM  dbo.General_Journal_Header GJH 
--	INNER JOIN dbo.OpportunityBizDocs OBD ON GJH.numOppBizDocsId = OBD.numOppBizDocsId AND GJH.numOppId = OBD.numOppId
--	JOIN dbo.OpportunityMaster OM ON OM.numOppId = OBD.numOppId 
--	WHERE GJH.numAmount!=OBD.monDealAmount  AND OBD.bitAuthoritativeBizDocs=1				
--						
--
--  --------------------------------------------------------------------------	
--ALTER TABLE dbo.OpportunityMaster ADD
--	dtItemReceivedDate datetime NULL				 	
--	
--	
--
--  --------------------------------------------------------------------------	
--UPDATE BPD SET dtAppliedDate=BPH.dtPaymentDate FROM BillPaymentHeader BPH JOIN BillPaymentDetails BPD 
--ON BPH.numBillPaymentID = BPD.numBillPaymentID
--WHERE BPD.dtAppliedDate IS NULL
--	
--	
--  --------------------------------------------------------------------------	
--ALTER TABLE dbo.OpportunityMaster ADD
--	bitPPVariance bit NULL	
--	
--	
--  --------------------------------------------------------------------------
--  ALTER TABLE dbo.WareHouseItems_Tracking ADD
--	numWareHouseItemTrackingID numeric(18, 0) NOT NULL IDENTITY (1, 1)
--GO
--ALTER TABLE dbo.WareHouseItems_Tracking ADD CONSTRAINT
--	PK_WareHouseItems_Tracking PRIMARY KEY CLUSTERED 
--	(
--	numWareHouseItemTrackingID
--	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
--
--	
--	ALTER TABLE dbo.WareHouseItems_Tracking ADD
--	monAverageCost money NULL	
--	
--	ALTER TABLE dbo.WareHouseItems_Tracking ADD
--	numTotalOnHand numeric(18, 0) NULL,
--	dtRecordDate datetime NULL
--	
----	INSERT INTO dbo.WareHouseItems_Tracking (
----	numWareHouseItemID,numOnHand,numOnOrder,numReorder,numAllocation,numBackOrder,numDomainID,
----	vcDescription,tintRefType,numReferenceID,dtCreatedDate,numModifiedBy,monAverageCost) 
----SELECT numWareHouseItemID,numOnHand,numOnOrder,numReorder,numAllocation,numBackOrder,I.numDomainID,
----	'Average Cost',1,numItemID,GETUTCDATE(),1,I.monAverageCost
---- 	FROM Item I JOIN WareHouseItems WHI ON I.numItemCode=WHI.numItemID
-- 	
-- 	
-- --------------------------------------------------------------------------	
--
---- 	UPDATE DycFormConfigurationDetails SET numRelCntType=0,numVIewID=0 WHERE numFormID=21 AND numRelCntType>0
---- 	
---- 	
---- 	select numUserCntID,numDomainID,numFieldID,COUNT(*) from DycFormConfigurationDetails where numFormId=21 AND tintPageType = 1
----GROUP BY numUserCntID,numDomainID,numFieldID HAVING COUNT(*)>1
----
----
----WITH [CTE] AS
----(
----select numUserCntID,numDomainID,numFieldID,ROW_NUMBER() OVER(PARTITION BY numUserCntID,numDomainID,numFieldID 
----ORDER BY numFieldID) AS NUm  from DycFormConfigurationDetails where numFormId=21 AND tintPageType = 1
----)
----DELETE FROM CTE WHERE NUm > 1
--
-- --------------------------------------------------------------------------	
--
----SELECT * FROM dbo.Chart_Of_Accounts WHERE vcAccountName LIKE 'Price Variance%'
--
----SELECT * FROM dbo.General_Journal_Details WHERE numChartAcntId IN(SELECT numAccountId FROM dbo.Chart_Of_Accounts WHERE vcAccountName LIKE 'Price Variance%')
--
----SELECT * FROM ChartAccountOpening WHERE numAccountId IN(SELECT numAccountId FROM dbo.Chart_Of_Accounts WHERE vcAccountName LIKE 'Price Variance%')
--
--DELETE FROM ChartAccountOpening WHERE numAccountId IN(SELECT numAccountId FROM dbo.Chart_Of_Accounts WHERE vcAccountName LIKE 'Price Variance%')
--DELETE FROM Chart_Of_Accounts WHERE vcAccountName LIKE 'Price Variance%'
--
--  -------------Set Default Account for existing domain-------------------------------------------------------------	
--
--BEGIN TRANSACTION
--
--DECLARE @IA AS VARCHAR(10)       
--SELECT @IA = ISNULL(numChargeTypeId,0) FROM dbo.AccountingChargeTypes WHERE chChargeCode='IA'
--
--DECLARE @OE AS VARCHAR(10)       
--SELECT @OE = ISNULL(numChargeTypeId,0) FROM dbo.AccountingChargeTypes WHERE chChargeCode='OE'
--
--DECLARE @PC AS VARCHAR(10)       
--SELECT @PC = ISNULL(numChargeTypeId,0) FROM dbo.AccountingChargeTypes WHERE chChargeCode='PC'
--
--DECLARE @PV AS VARCHAR(10)       
--SELECT @PV = ISNULL(numChargeTypeId,0) FROM dbo.AccountingChargeTypes WHERE chChargeCode='PV'
--
--DECLARE  @maxDomainID NUMERIC(9)
--DECLARE  @minDomainID NUMERIC(9)
--
--DECLARE @IAAccountId NUMERIC(9),@OEAccountId NUMERIC(9),@PCAccountId NUMERIC(9),@PVAccountId NUMERIC(9)
--DECLARE @numExpenseAccountTypeID  AS NUMERIC(9,0),@numEquityAccountTypeID  AS NUMERIC(9,0),@numLiabilityAccountTypeID  AS NUMERIC(9,0),@numCOGSAccountTypeID  AS NUMERIC(9,0)
--DECLARE @numOtherDirectExpenseID AS NUMERIC(9),@numRSccountTypeID AS NUMERIC(9),@numCLAccountTypeID AS NUMERIC(9),@numCOGSExpenseID AS NUMERIC(9)
--
--DECLARE @dtTodayDate AS DATETIME
--        SET @dtTodayDate = GETDATE()
--        
--SELECT @maxDomainID = max([numDomainId]),@minDomainID = min([numDomainId]) FROM   [Domain] where numDomainId>0
--WHILE @minDomainID <= @maxDomainID
--  BEGIN
--	 PRINT 'DomainID=' + CONVERT(VARCHAR(20),@minDomainID)
--	 
--	 SELECT @IAAccountId=0,@PCAccountId=0,@PVAccountId=0,@OEAccountId=0
--	 SELECT @numExpenseAccountTypeID =0,@numEquityAccountTypeID=0,@numLiabilityAccountTypeID=0,@numCOGSAccountTypeID=0
--	 SELECT @numOtherDirectExpenseID =0,@numRSccountTypeID =0,@numCLAccountTypeID =0,@numCOGSExpenseID =0
--
--	 IF NOT EXISTS (SELECT 1 FROM AccountingCharges WHERE numDomainID=@minDomainID AND numChargeTypeId=@IA)
--	 BEGIN
--		 SELECT @numExpenseAccountTypeID = numAccountTypeId FROM dbo.AccountTypeDetail WHERE vcAccountCode ='0104' AND numDomainID = @minDomainID 
--		 SELECT @numOtherDirectExpenseID = numAccountTypeID FROM dbo.AccountTypeDetail WHERE numDomainID = @minDomainID   AND vcAccountCode = '01040101' 
--
--	 			EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numExpenseAccountTypeID,
--			@numParntAcntTypeID = @numOtherDirectExpenseID, @vcAccountName = 'Inventory Adjustment',
--			@vcAccountDescription = 'Inventory Adjustment',
--			@monOriginalOpeningBal = $0, @monOpeningBal = $0,
--			@dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = @IAAccountId output,
--			@bitFixed = 0, @numDomainID = @minDomainID   , @numListItemID = 0, @numUserCntId = 1,
--			@bitProfitLoss = 0, @bitDepreciation = 0,
--			@dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
--			@vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0
--			
--			INSERT INTO dbo.AccountingCharges (numChargeTypeId,numAccountID,numDomainID) 
--				VALUES (@IA,@IAAccountId,@minDomainID) 
--	 END
--
--	 IF NOT EXISTS (SELECT 1 FROM AccountingCharges WHERE numDomainID=@minDomainID AND numChargeTypeId=@OE)
--	 BEGIN
--		 SELECT @numEquityAccountTypeID = numAccountTypeId FROM dbo.AccountTypeDetail WHERE vcAccountCode ='0105' AND numDomainID = @minDomainID  
--		 SELECT @numRSccountTypeID = numAccountTypeID FROM dbo.AccountTypeDetail WHERE vcAccountCode = '01050101'  AND numDomainID = @minDomainID  
--
--	 		EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numEquityAccountTypeID,
--		@numParntAcntTypeID = @numRSccountTypeID, @vcAccountName = 'Opening Balance Equity',
--		@vcAccountDescription = 'Opening Balance Equity', @monOriginalOpeningBal = $0,
--		@monOpeningBal = $0, @dtOpeningDate = @dtTodayDate, @bitActive = 1,
--		@numAccountId = @OEAccountId output, @bitFixed = 0, @numDomainID = @minDomainID   , @numListItemID = 0,
--		@numUserCntId = 1, @bitProfitLoss = 0, @bitDepreciation = 0,
--		@dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
--		@vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0
--		
--		INSERT INTO dbo.AccountingCharges (numChargeTypeId,numAccountID,numDomainID) 
--				VALUES (@OE,@OEAccountId,@minDomainID)
--	 END
--	 
--	 IF NOT EXISTS (SELECT 1 FROM AccountingCharges WHERE numDomainID=@minDomainID AND numChargeTypeId=@PC)
--	 BEGIN
--		SELECT @numLiabilityAccountTypeID = numAccountTypeId FROM dbo.AccountTypeDetail WHERE vcAccountCode ='0102' AND numDomainID = @minDomainID 
--		SELECT @numCLAccountTypeID = numAccountTypeID FROM dbo.AccountTypeDetail WHERE vcAccountCode = '01020102'  AND numDomainID = @minDomainID  
--
--	 			EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numLiabilityAccountTypeID,
--			@numParntAcntTypeID =  @numCLAccountTypeID, @vcAccountName = 'Purchase Clearing',
--			@vcAccountDescription = 'Purchase Clearing',
--			@monOriginalOpeningBal = $0, @monOpeningBal = $0,
--			@dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = @PCAccountId output,
--			@bitFixed = 0, @numDomainID = @minDomainID   , @numListItemID = 0, @numUserCntId = 1,
--			@bitProfitLoss = 0, @bitDepreciation = 0,
--			@dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
--			@vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0
--			
--			INSERT INTO dbo.AccountingCharges (numChargeTypeId,numAccountID,numDomainID) 
--				VALUES (@PC,@PCAccountId,@minDomainID)
--	 END
--	 
--	 IF NOT EXISTS (SELECT 1 FROM AccountingCharges WHERE numDomainID=@minDomainID AND numChargeTypeId=@PV)
--	 BEGIN
--		SELECT @numCOGSAccountTypeID = numAccountTypeId FROM dbo.AccountTypeDetail WHERE vcAccountCode ='0106' AND numDomainID = @minDomainID  
--		SELECT @numCOGSExpenseID = numAccountTypeID FROM dbo.AccountTypeDetail WHERE numDomainID = @minDomainID   AND vcAccountCode = '010601' 
--
--	 			EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numCOGSAccountTypeID,
--		@numParntAcntTypeID = @numCOGSExpenseID, @vcAccountName = 'Purchase Price Variance',
--		@vcAccountDescription = 'Stores difference of purchase price when Cost of Product is different then what is entered in Purchase Order.',
--		@monOriginalOpeningBal = $0, @monOpeningBal = $0,
--		@dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = @PVAccountId output,
--		@bitFixed = 0, @numDomainID = @minDomainID   , @numListItemID = 0, @numUserCntId = 1,
--		@bitProfitLoss = 0, @bitDepreciation = 0,
--		@dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
--		@vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0
--		
--		INSERT INTO dbo.AccountingCharges (numChargeTypeId,numAccountID,numDomainID) 
--				VALUES (@PV,@PVAccountId,@minDomainID)
--	 END
--	 
--	 SELECT @minDomainID = min([numDomainId]) FROM   [Domain] WHERE  [numDomainId]> @minDomainID 
--  END
--
--ROLLBACK TRANSACTION
--
--
--  -------------Average Cost : SO/PO Audit-------------------------------------------------------------	
--
--BEGIN TRANSACTION
--
--SELECT OI.numItemCode,OM.numDomainId,ROW_NUMBER() OVER(ORDER BY OI.numItemCode,OM.numDomainId) RowNO INTO #tempItem FROM OpportunityMaster OM JOIN OpportunityItems OI ON OM.numOppId = OI.numOppId
--JOIN item I ON OI.numItemCode=I.numItemCOde WHERE tintOppType=2 AND tintshipped=1 AND I.charItemType='P' AND ISNULL(OI.numWarehouseItmsID,0)>0
--GROUP BY OI.numItemCode,OM.numDomainId
--
--DECLARE @minNo AS NUMERIC,@maxNo AS NUMERIC;
--SELECT @minNo=MIN(RowNO),@maxNo=MAX(RowNO) FROM #tempItem
--
--DECLARE @numOnHand AS NUMERIC,@monAvgCost AS MONEY
--DECLARE @numItemCode AS NUMERIC,@numDomainId AS NUMERIC,@tintOppType AS NUMERIC
--DECLARE @minOpp AS NUMERIC,@maxOpp AS NUMERIC;
--DECLARE @numOppId AS NUMERIC,@numUnitHour AS NUMERIC,@numWarehouseItmsID AS NUMERIC,@monPrice AS MONEY,@bintAccountClosingDate AS DATETIME;
--
--WHILE @minNo <= @maxNo
--  BEGIN
--	SELECT @numOnHand=0,@monAvgCost=0,@minOpp=0,@maxOpp=0,@numOppId=0,@numUnitHour=0,@numWarehouseItmsID=0,@monPrice=0,
--	@numItemCode=0,@numDomainId=0,@tintOppType=0
--	
--	SELECT @numItemCode=numItemCode,@numDomainId=numDomainId FROM #tempItem WHERE RowNO=@minNo 	
--	
--	SELECT OM.tintOppType,OM.numOppId,numUnitHour,numWarehouseItmsID,isnull(monPrice,0) * (CASE WHEN ISNULL(fltExchangeRate,0)=0 THEN 1 ELSE fltExchangeRate END) AS monPrice,
--	bintAccountClosingDate,ROW_NUMBER() OVER(ORDER BY bintAccountClosingDate) RowNO INTO #temp1 FROM dbo.OpportunityMaster OM JOIN dbo.OpportunityItems OI ON OM.numOppId = OI.numOppId
--		WHERE tintshipped=1 AND numDomainId=@numDomainId AND numItemCode=@numItemCode AND ISNULL(OI.numWarehouseItmsID,0)>0
--		ORDER BY bintAccountClosingDate 
--
--	SELECT @minOpp=MIN(RowNO),@maxOpp=MAX(RowNO) FROM #temp1
--
--	WHILE @minOpp <= @maxOpp
--     BEGIN
--		SELECT @numOppId=numOppId,@numUnitHour=numUnitHour,@numWarehouseItmsID=numWarehouseItmsID,@monPrice=monPrice,
--			@bintAccountClosingDate=bintAccountClosingDate,@tintOppType=tintOppType FROM #temp1 WHERE RowNO=@minOpp
--		
--		IF @numUnitHour>0
--		BEGIN
--			PRINT CAST(@tintOppType AS VARCHAR(2)) + '::' + CAST(@numOnHand AS VARCHAR(10)) + '::'+ CAST(@numUnitHour AS VARCHAR(10))
--			
--			IF @tintOppType=1
--			BEGIN
--  			     IF @numOnHand>=@numUnitHour
--  			     BEGIN
--				   SET @numOnHand = @numOnHand - @numUnitHour	
--  			     
--  			      INSERT INTO dbo.WareHouseItems_Tracking (
--						numWareHouseItemID,numOnHand,numOnOrder,numReorder,numAllocation,numBackOrder,
--						numDomainID,vcDescription,tintRefType,numReferenceID,dtCreatedDate,numModifiedBy,
--						monAverageCost,numTotalOnHand,dtRecordDate
--					) VALUES ( @numWarehouseItmsID,@numOnHand,0,0,@numUnitHour,0,@numDomainId,'SO Created (Qty:'+ CAST(@numUnitHour AS VARCHAR(15)) +' Price:' + CAST(@monPrice AS VARCHAR(15)) +')',6,@numOppId,@bintAccountClosingDate,1,
--					@monAvgCost,@numOnHand,@bintAccountClosingDate)  
--					
--				 INSERT INTO dbo.WareHouseItems_Tracking (
--						numWareHouseItemID,numOnHand,numOnOrder,numReorder,numAllocation,numBackOrder,
--						numDomainID,vcDescription,tintRefType,numReferenceID,dtCreatedDate,numModifiedBy,
--						monAverageCost,numTotalOnHand,dtRecordDate
--					) VALUES ( @numWarehouseItmsID,@numOnHand,0,0,0,0,@numDomainId,'SO Closed (Qty:'+ CAST(@numUnitHour AS VARCHAR(15)) +' Price:' + CAST(@monPrice AS VARCHAR(15)) +')',6,@numOppId,@bintAccountClosingDate,1,
--					@monAvgCost,@numOnHand,@bintAccountClosingDate)  
--				 END
--			END
--			ELSE
--			BEGIN
--					INSERT INTO dbo.WareHouseItems_Tracking (
--						numWareHouseItemID,numOnHand,numOnOrder,numReorder,numAllocation,numBackOrder,
--						numDomainID,vcDescription,tintRefType,numReferenceID,dtCreatedDate,numModifiedBy,
--						monAverageCost,numTotalOnHand,dtRecordDate
--					) VALUES ( @numWarehouseItmsID,@numOnHand,@numUnitHour,0,0,0,@numDomainId,'PO Created (Qty:'+ CAST(@numUnitHour AS VARCHAR(15)) +' Cost:' + CAST(@monPrice AS VARCHAR(15)) +')',6,@numOppId,@bintAccountClosingDate,1,
--					@monAvgCost,@numOnHand,@bintAccountClosingDate) 
--							
--					SET @monAvgCost = ( ( @numOnHand * @monAvgCost )
--													+ (@numUnitHour * @monPrice))
--									/ ( @numOnHand + @numUnitHour )
--				
--					SET @numOnHand = @numOnHand + @numUnitHour	
--				
--					INSERT INTO dbo.WareHouseItems_Tracking (
--						numWareHouseItemID,numOnHand,numOnOrder,numReorder,numAllocation,numBackOrder,
--						numDomainID,vcDescription,tintRefType,numReferenceID,dtCreatedDate,numModifiedBy,
--						monAverageCost,numTotalOnHand,dtRecordDate
--					) VALUES ( @numWarehouseItmsID,@numOnHand,0,0,0,0,@numDomainId,'PO Closed (Qty:'+ CAST(@numUnitHour AS VARCHAR(15)) +' Cost:' + CAST(@monPrice AS VARCHAR(15)) +')',6,@numOppId,@bintAccountClosingDate,1,
--					@monAvgCost,@numOnHand,@bintAccountClosingDate) 
--									
--			END
--		END
--		
--		SET @minOpp = @minOpp + 1
--	END
--           
--    INSERT INTO dbo.WareHouseItems_Tracking (
--	numWareHouseItemID,numOnHand,numOnOrder,numReorder,numAllocation,numBackOrder,numDomainID,
--	vcDescription,tintRefType,numReferenceID,dtCreatedDate,numModifiedBy,monAverageCost) 
--	SELECT numWareHouseItemID,numOnHand,numOnOrder,numReorder,numAllocation,numBackOrder,I.numDomainID,
--	'Biz: Calculated avg cost, old avg cost:' + CAST(CAST(ISNULL(I.monAverageCost,0) AS DECIMAL(18,4)) AS VARCHAR(15)),1,numItemID,GETUTCDATE(),1,@monAvgCost
-- 	FROM Item I JOIN WareHouseItems WHI ON I.numItemCode=WHI.numItemID WHERE I.numItemCode=@numItemCode
-- 	       
-- 	--UPDATE Item SET monAverageCost=@monAvgCost WHERE numItemCode=@numItemCode 
-- 	
--    DROP TABLE #temp1                 
--    
--    SET @minNo = @minNo + 1
--END
--  
--DROP TABLE #tempItem
--
--ROLLBACK TRANSACTION 


/************************21_Jan_2013*******************/

--ALTER TABLE dbo.Item ADD
--	bitAllowDropShip bit NULL
--	
-----------------------------------------------------------------------------------------------		
--	
--
--INSERT INTO [dbo].[DycFieldMaster]([numModuleID], [numDomainID], [vcFieldName], [vcDbColumnName], [vcOrigDbColumnName], [vcPropertyName], [vcLookBackTableName], [vcFieldDataType], [vcFieldType], [vcAssociatedControlType], 
--[vcToolTip], [vcListItemType], [numListID], [PopupFunctionName], [order], [tintRow], [tintColumn], 
--[bitInResults], [bitDeleted], [bitAllowEdit], [bitDefault], [bitSettingField], [bitAddField], 
--[bitDetailField], [bitAllowSorting],bitWorkFlowField,bitAllowFiltering )
--SELECT 3, NULL, N'Tracking No', N'vcTrackingNo', N'vcTrackingNo', 'TrackingNo', N'OpportunityBizDocs', N'V', N'R',
-- N'Label', NULL, N'', 0, N'', 16, 14, 1, 1, 0, 0, 0,0, 0, 1, 0,0,0
--		
--	
--		
--insert into DycFormField_Mapping (numModuleID,numFieldID,numFormID,vcFieldName,vcAssociatedControlType,PopupFunctionName,bitSettingField,bitAddField,bitDetailField,bitAllowEdit,bitInlineEdit,vcPropertyName,[order],tintRow,tintColumn)
--select numModuleID,numFieldID,38,'Tracking No',vcAssociatedControlType,PopupFunctionName,bitSettingField,0,bitDetailField,0,0,vcPropertyName,36,36,1
--from DycFieldMaster where [numModuleID]=3 and  [vcDbColumnName]='vcTrackingNo' AND vcLookBackTableName='OpportunityBizDocs'
--UNION 
--select numModuleID,numFieldID,39,'Tracking No',vcAssociatedControlType,PopupFunctionName,bitSettingField,0,bitDetailField,0,0,vcPropertyName,36,36,1
--from DycFieldMaster where [numModuleID]=3 and  [vcDbColumnName]='vcTrackingNo' AND vcLookBackTableName='OpportunityBizDocs'
--UNION 
--select numModuleID,numFieldID,40,'Tracking No',vcAssociatedControlType,PopupFunctionName,bitSettingField,0,bitDetailField,0,0,vcPropertyName,36,36,1
--from DycFieldMaster where [numModuleID]=3 and  [vcDbColumnName]='vcTrackingNo' AND vcLookBackTableName='OpportunityBizDocs'
--UNION 
--select numModuleID,numFieldID,41,'Tracking No',vcAssociatedControlType,PopupFunctionName,bitSettingField,0,bitDetailField,0,0,vcPropertyName,36,36,1
--from DycFieldMaster where [numModuleID]=3 and  [vcDbColumnName]='vcTrackingNo' AND vcLookBackTableName='OpportunityBizDocs'
--		
--		
---------------Authoritative bizdoc's journal entries are not being created on Billing Date---------------------------------------------------------------------------------		
--
--
--BEGIN TRANSACTION 
--
--
--SELECT CONVERT(DATETIME , CONVERT(VARCHAR(10),obd.dtFromDate,111)) dtFromDate,
--GJH.numJournal_Id INTO #temp
--FROM dbo.OpportunityBizDocs OBD JOIN dbo.General_Journal_Header GJH ON OBD.numOppId=GJH.numOppId
--AND obd.numOppBizDocsId=GJH.numOppBizDocsId WHERE 
--CONVERT(VARCHAR(10),obd.dtFromDate,111)!=CONVERT(VARCHAR(10),GJH.datEntry_Date,111)
--AND isnull(Gjh.numBillID,0)=0 AND isnull(gjh.numBillPaymentID,0)=0 AND isnull(GJH.numPayrollDetailID,0)=0
--AND isnull(Gjh.numCheckHeaderID,0)=0 AND isnull(gjh.numReturnID,0)=0 
--AND isnull(GJH.numDepositId,0)=0 and monDealAMount=numAmount 
--AND OBD.bitAuthoritativeBizDocs=1 AND numJournal_Id!=12264
--order by GJH.datEntry_Date 
--
----SELECT *,CAST(dtFromDate AS SMALLDATETIME) FROM #temp 
--
--UPDATE GJH SET datEntry_Date=CAST(dtFromDate AS SMALLDATETIME)
--FROM #temp OBD JOIN dbo.General_Journal_Header GJH ON OBD.numJournal_Id=GJH.numJournal_Id
--
--SELECT CONVERT(VARCHAR(10),obd.dtCreatedDate,111) dtCreatedDate,CONVERT(VARCHAR(10),obd.dtFromDate,111) dtFromDate,CONVERT(VARCHAR(10),GJH.datEntry_Date,111) datEntry_Date
--,GJH.* 
--FROM dbo.OpportunityBizDocs OBD JOIN dbo.General_Journal_Header GJH ON OBD.numOppId=GJH.numOppId
--AND obd.numOppBizDocsId=GJH.numOppBizDocsId WHERE 
--CONVERT(VARCHAR(10),obd.dtFromDate,111)!=CONVERT(VARCHAR(10),GJH.datEntry_Date,111)
--AND isnull(Gjh.numBillID,0)=0 AND isnull(gjh.numBillPaymentID,0)=0 AND isnull(GJH.numPayrollDetailID,0)=0
--AND isnull(Gjh.numCheckHeaderID,0)=0 AND isnull(gjh.numReturnID,0)=0 
--AND isnull(GJH.numDepositId,0)=0 and monDealAMount=numAmount 
--AND OBD.bitAuthoritativeBizDocs=1 AND numJournal_Id!=12264
--order by GJH.datEntry_Date 
--
--DROP TABLE #temp
--
--ROLLBACK TRANSACTION 		
--
--
-----------------------------------------------------------------------------------------------		
--
----SELECT * FROM dbo.DycFormConfigurationDetails WHERE numformid=56 AND intColumnNum=0
--UPDATE dbo.DycFormConfigurationDetails SET intColumnNum=1 WHERE numformid=56 AND intColumnNum=0
--
-----------------------------------------------------------------------------------------------		


/*
--SELECT * FROM dbo.ShortCutBar WHERE Link='../opportunity/frmAmtPaid.aspx'
UPDATE ShortCutBar SET bitNew=1 WHERE Link='../opportunity/frmAmtPaid.aspx' 

ALTER TABLE dbo.OpportunityMaster ADD
	vcOppRefOrderNo VARCHAR(100) NULL
	


INSERT INTO [dbo].[DycFieldMaster]([numModuleID], [numDomainID], [vcFieldName], [vcDbColumnName], [vcOrigDbColumnName], [vcPropertyName], [vcLookBackTableName], [vcFieldDataType], [vcFieldType], [vcAssociatedControlType], 
[vcToolTip], [vcListItemType], [numListID], [PopupFunctionName], [order], [tintRow], [tintColumn], 
[bitInResults], [bitDeleted], [bitAllowEdit], [bitDefault], [bitSettingField], [bitAddField], 
[bitDetailField], [bitAllowSorting],bitWorkFlowField,bitAllowFiltering )
SELECT 3, NULL, N'Customer PO# / Vendor Invoice#', N'vcOppRefOrderNo', N'vcOppRefOrderNo', 'OppRefOrderNo', N'OpportunityMaster', N'V', N'R',
 N'TextBox', NULL, N'', 0, N'', 15, 13, 2, 1, 0, 1, 0,1, 0, 1, 1,1,1
	
	

--SELECT * FROM dbo.DycFieldMaster WHERE vcDbColumnName='vcRefOrderNo'
--SELECT numFieldID FROM dbo.DycFieldMaster WHERE vcDbColumnName='vcOppRefOrderNo'

--SELECT * FROM dbo.DycFormField_Mapping WHERE numFieldID=110

UPDATE DycFormField_Mapping SET numFieldID=(SELECT numFieldID FROM dbo.DycFieldMaster 
WHERE vcDbColumnName='vcOppRefOrderNo'),vcPropertyName='OppRefOrderNo' WHERE numFormID IN (38,39,40,41) AND numFieldID=110

--select * FROM dbo.DycFormConfigurationDetails

UPDATE DycFormConfigurationDetails SET numFieldID=(SELECT numFieldID FROM dbo.DycFieldMaster 
WHERE vcDbColumnName='vcOppRefOrderNo') WHERE numFormID IN (38,39,40,41) AND numFieldID=110
	

UPDATE OM SET vcOppRefOrderNo=OBD.vcRefOrderNo FROM 
OpportunityBizDocs OBD JOIN dbo.OpportunityMaster OM ON OBD.numOppId = OM.numOppId
WHERE OBD.vcRefOrderNo IS NOT NULL 
	

--------------------------------------------------------------------------------------------------------

--SELECT * FROM CustRptDefaultFields WHERE vcScrFieldName='Terms' AND numFieldGroupID=25
UPDATE CustRptDefaultFields SET vcDbFieldName='case when OppMas.bitBillingTerms=1 then  ''Net '' + convert(varchar(10),ISNULL(dbo.fn_GetListItemName(isnull(OppMas.intBillingDays,0)),0)) + '' , ''  + case when OppMas.bitInterestType=0  then ''-'' else ''+'' end + convert(varchar(10),OppMas.fltInterest) + ''%''  else '''' end'
WHERE vcScrFieldName='Terms' AND numFieldGroupID=25

--SELECT * FROM CustRptDefaultFields WHERE vcScrFieldName='Due Date' AND numFieldGroupID=25
UPDATE CustRptDefaultFields SET vcDbFieldName='dateadd(day,case when OppMas.bitBillingTerms=1 then convert(int,ISNULL(dbo.fn_GetListItemName(isnull(OppMas.intBillingdays,0)),0)) else 0 end ,convert(varchar(11),OppMas.dtFromDate))'
WHERE vcScrFieldName='Due Date' AND numFieldGroupID=25
	

--SELECT * FROM CustReportFields WHERE vcFieldName LIKE '%intBillingdays%'
UPDATE CustReportFields SET vcFieldName=REPLACE(CAST(vcFieldName AS VARCHAR(8000)),'OppBDoc.bitBillingTerms','OppMas.bitBillingTerms') WHERE vcFieldName LIKE '%OppBDoc.bitBillingTerms%'

--SELECT * FROM CustReportFields WHERE vcFieldName LIKE '%bitBillingTerms%'
UPDATE CustReportFields SET vcFieldName=REPLACE(CAST(vcFieldName AS VARCHAR(8000)),'OppBDoc.bitBillingTerms','OppMas.bitBillingTerms') WHERE vcFieldName LIKE '%OppBDoc.bitBillingTerms%'


--SELECT * FROM CustomReport WHERE textQuery LIKE '%OppBDoc.intBillingdays%'
UPDATE CustomReport SET textQuery=REPLACE(CAST(textQuery AS VARCHAR(8000)),'OppBDoc.intBillingdays','OppMas.intBillingdays') WHERE textQuery LIKE '%OppBDoc.intBillingdays%'

--SELECT * FROM CustomReport WHERE textQuery LIKE '%OppBDoc.bitBillingTerms%'
UPDATE CustomReport SET textQuery=REPLACE(CAST(textQuery AS VARCHAR(8000)),'OppBDoc.bitBillingTerms','OppMas.bitBillingTerms') WHERE textQuery LIKE '%OppBDoc.bitBillingTerms%'

	
--------------------------------------------------------------------------------------------------------
--SELECT OBDD.numDivisionId,tintPaymentType,OPD.numCheckNo,BPH.*,OPD.monAmount AS monCheckAmount
--FROM OpportunityBizDocsDetails OBDD 
--INNER JOIN dbo.OpportunityBizDocsPaymentDetails OPD  ON OBDD.numBizDocsPaymentDetId = OPD.numBizDocsPaymentDetId
--JOIN dbo.BillPaymentHeader BPH ON BPH.numBizDocsPaymentDetId_Ref=OBDD.numBizDocsPaymentDetId 
--WHERE tintPaymentType IN(2,4) AND
--ISNULL(numCheckNo,0)>0


UPDATE BPH SET numDivisionId=OBDD.numDivisionId FROM OpportunityBizDocsDetails OBDD 
INNER JOIN dbo.OpportunityBizDocsPaymentDetails OPD  ON OBDD.numBizDocsPaymentDetId = OPD.numBizDocsPaymentDetId
JOIN dbo.BillPaymentHeader BPH ON BPH.numBizDocsPaymentDetId_Ref=OBDD.numBizDocsPaymentDetId 
WHERE tintPaymentType IN(2,4) AND
ISNULL(numCheckNo,0)>0
	
--SELECT BPH.numDivisionID,CH.numDivisionID,CH.tintReferenceType,* FROM dbo.CheckHeader CH JOIN dbo.BillPaymentHeader BPH ON CH.numReferenceID=BPH.numBillPaymentID
--AND CH.tintReferenceType=8 WHERE BPH.numDivisionID IS NOT NULL  AND CH.numDivisionID=0

UPDATE CH SET numDivisionID=BPH.numDivisionID FROM dbo.CheckHeader CH JOIN dbo.BillPaymentHeader BPH ON CH.numReferenceID=BPH.numBillPaymentID
AND CH.tintReferenceType=8 WHERE BPH.numDivisionID IS NOT NULL  AND ISNULL(CH.numDivisionID,0)=0



--------------------------------------------------------------------------------------------------------
ALTER TABLE dbo.OpportunityBizDocs ADD
	numBizDocStatusOLD numeric(18, 0) NULL
	
UPDATE OpportunityBizDocs SET numBizDocStatusOLD=numBizDocStatus


--------------------------------------------------------------------------------------------------------
SELECT GJD.numCustomerId,CH.numDivisionID FROM dbo.CheckHeader CH LEFT JOIN dbo.General_Journal_Header GJH ON CH.numCheckHeaderID=GJH.numCheckHeaderID
JOIN dbo.General_Journal_Details GJD ON GJD.numJournalId=GJH.numJournal_Id WHERE CH.tintReferenceType=1

UPDATE GJD SET numCustomerId=CH.numDivisionID FROM dbo.CheckHeader CH LEFT JOIN dbo.General_Journal_Header GJH ON CH.numCheckHeaderID=GJH.numCheckHeaderID
JOIN dbo.General_Journal_Details GJD ON GJD.numJournalId=GJH.numJournal_Id WHERE CH.tintReferenceType=1




--------------------------------------------------------------------------------------------------------
DECLARE @numFieldID AS NUMERIC;
SELECT @numFieldID=numFieldID FROM DycFieldMaster WHERE vcDbColumnName='monShipCost' AND vcLookBackTableName='OpportunityBizDocItems'
delete FROM dbo.DycFormField_Mapping WHERE numFieldID=@numFieldID
delete FROM dbo.DycFormConfigurationDetails WHERE numFieldID=@numFieldID

---------------------------------
DECLARE @numFieldID AS NUMERIC;
SELECT @numFieldID=numFieldID FROM DycFieldMaster WHERE vcDbColumnName='vcTrackingNo' AND vcLookBackTableName='OpportunityBizDocItems'
delete FROM dbo.DycFormField_Mapping WHERE numFieldID=@numFieldID
delete FROM dbo.DycFormConfigurationDetails WHERE numFieldID=@numFieldID

---------------------------------
--DECLARE @numFieldID AS NUMERIC;
--SELECT @numFieldID=numFieldID FROM DycFieldMaster WHERE vcDbColumnName='ShippingAmount' AND vcLookBackTableName='TaxItems'
--delete FROM dbo.DycFormField_Mapping WHERE numFieldID=@numFieldID
--delete FROM dbo.DycFormConfigurationDetails WHERE numFieldID=@numFieldID

---------------------------------
DECLARE @numFieldID AS NUMERIC;
SELECT @numFieldID=numFieldID FROM DycFieldMaster WHERE vcDbColumnName='vcShippingMethod' AND vcLookBackTableName='OpportunityBizDocItems'
delete FROM dbo.DycFormField_Mapping WHERE numFieldID=@numFieldID
delete FROM dbo.DycFormConfigurationDetails WHERE numFieldID=@numFieldID

---------------------------------
DECLARE @numFieldID AS NUMERIC;
SELECT @numFieldID=numFieldID FROM DycFieldMaster WHERE vcDbColumnName='dtDeliveryDate' AND vcLookBackTableName='OpportunityBizDocItems'
delete FROM dbo.DycFormField_Mapping WHERE numFieldID=@numFieldID
delete FROM dbo.DycFormConfigurationDetails WHERE numFieldID=@numFieldID

--------------------------------------------------------------------------------------------------------


GO
/****** Object:  Table [dbo].[OpportunityFulfillmentBizDocs]    Script Date: 01/11/2013 15:19:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OpportunityFulfillmentBizDocs](
	[numBizDocTypeID] [numeric](18, 0) NOT NULL,
	[numDomainID] [numeric](18, 0) NOT NULL,
	[ItemType] [char](2) NOT NULL,
 CONSTRAINT [PK_FulfillmentOrderBizDocs] PRIMARY KEY CLUSTERED 
(
	[numBizDocTypeID] ASC,
	[numDomainID] ASC,
	[ItemType] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING ON



GO
/****** Object:  Table [dbo].[OpportunityFulfillmentRules]    Script Date: 01/11/2013 15:19:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OpportunityFulfillmentRules](
	[numRuleID] [numeric](18, 0) NOT NULL,
	[numBizDocTypeID] [numeric](18, 0) NOT NULL,
	[numBizDocStatus] [numeric](18, 0) NOT NULL,
	[numDomainID] [numeric](18, 0) NOT NULL,
 CONSTRAINT [PK_OpportunityFulfillmentRules] PRIMARY KEY CLUSTERED 
(
	[numRuleID] ASC,
	[numBizDocTypeID] ASC,
	[numBizDocStatus] ASC,
	[numDomainID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]


--------------------------------------------------------------------------------------------------------

UPDATE ShortCutBar SET link='../opportunity/frmSalesFulfillmentOrder.aspx' 
WHERE link='../Opportunity/frmSalesFulfullment.aspx'

UPDATE PageNavigationDTL SET vcNavURL='../opportunity/frmSalesFulfillmentOrder.aspx' 
WHERE vcNavURL='../Opportunity/frmSalesFulfullment.aspx'


--------------------------------------------------------------------------------------------------------

GO
/****** Object:  Table [dbo].[OppFulfillmentBizDocsStatusHistory]    Script Date: 01/13/2013 13:15:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OppFulfillmentBizDocsStatusHistory](
	[numOppBizDocStatus] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numDomainId] [numeric](18, 0) NULL,
	[numOppId] [numeric](18, 0) NULL,
	[numOppBizDocsId] [numeric](18, 0) NULL,
	[numBizDocStatus] [numeric](18, 0) NULL,
	[numUserCntID] [numeric](18, 0) NULL,
	[dtCreatedDate] [datetime] NULL,
 CONSTRAINT [PK_OppFulfillmentBizDocsStatusHistory] PRIMARY KEY CLUSTERED 
(
	[numOppBizDocStatus] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]


--------------------------------------------------------------------------------------------------------

INSERT INTO dbo.DynamicFormMaster (
	numFormId,vcFormName,cCustomFieldsAssociated,cAOIAssociated,bitDeleted,
	tintFlag,bitWorkFlow,vcLocationID,bitAllowGridColor
) VALUES ( 
	64,'Sales Fulfillment','N','N',0,2,0,'',0 )  


*/

