/******************************************************************
Project: Release 8.6 Date: 11.DECEMBER.2017
Comments: ALTER SCRIPTS
*******************************************************************/

/*************************   NEELAM  *****************************/


ALTER TABLE PromotionOffer DROP COLUMN [bitFreeShiping]
ALTER TABLE PromotionOffer DROP COLUMN [monFreeShippingOrderAmount]
ALTER TABLE PromotionOffer DROP COLUMN [numFreeShippingCountry]
ALTER TABLE PromotionOffer DROP COLUMN [bitFixShipping1]
ALTER TABLE PromotionOffer DROP COLUMN [monFixShipping1OrderAmount]
ALTER TABLE PromotionOffer DROP COLUMN [monFixShipping1Charge]
ALTER TABLE PromotionOffer DROP COLUMN [bitFixShipping2]
ALTER TABLE PromotionOffer DROP COLUMN [monFixShipping2OrderAmount]
ALTER TABLE PromotionOffer DROP COLUMN [monFixShipping2Charge]

ALTER TABLE SimilarItems ADD bitRequired bit

ALTER TABLE PromotionOffer ADD tintCustomersBasedOn TINYINT

ALTER TABLE PromotionOffer ADD tintOfferTriggerValueTypeRange TINYINT
ALTER TABLE PromotionOffer ADD fltOfferTriggerValueRange FLOAT



ALTER TABLE PromotionOffer ADD tintOfferTriggerValueType TINYINT
ALTER TABLE PromotionOffer ADD fltOfferTriggerValue FLOAT
ALTER TABLE PromotionOffer ADD tintOfferBasedOn TINYINT
ALTER TABLE PromotionOffer ADD tintDiscountType TINYINT
ALTER TABLE PromotionOffer ADD fltDiscountValue FLOAT

ALTER TABLE PromotionOffer ADD tintDiscoutBaseOn TINYINT
ALTER TABLE PromotionOffer ADD tintDiscountType TINYINT

/************************************************SHIPPING PROMOTIONS***************************************************************************************************/



BEGIN TRY
BEGIN TRANSACTION

	INSERT INTO PageNavigationDTL 
		(numPageNavID,numModuleID,numParentID,vcPageNavName,vcNavURL,bitVisible,numTabID)
	VALUES
		(259,13,237,'Shipping Promotions','../ECommerce/frmShippingCriteria.aspx',1,-1)

	INSERT INTO dbo.TreeNavigationAuthorization
	(
		numGroupID,
		numTabID,
		numPageNavID,
		bitVisible,
		numDomainID,
		tintType
	)
	SELECT  
		numGroupID,
		-1,
		259,
		1,
		numDomainID,
		1
	FROM    
		AuthenticationGroupMaster
	WHERE
		tintGroupType=1
COMMIT
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		ERROR_MESSAGE(),
		ERROR_NUMBER(),
		ERROR_SEVERITY(),
		ERROR_STATE(),
		ERROR_LINE(),
		ISNULL(ERROR_PROCEDURE(), '-');
END CATCH


/************************************************SHIPPING PROMOTIONS***************************************************************************************************/

INSERT INTO CFW_Loc_Master VALUES('Opportunity Item Details','O','CFW_Fld_Values_OppItems')


ALTER TABLE CartItems ADD numParentItem NUMERIC(18,0)


USE [Production.2014]
GO

/****** Object:  Table [dbo].[CFW_Fld_Values_OppItems]    Script Date: 11-Dec-17 9:36:58 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[CFW_Fld_Values_OppItems](
	[FldDTLID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Fld_ID] [numeric](18, 0) NULL,
	[Fld_Value] [varchar](max) NULL,
	[RecId] [numeric](18, 0) NULL,
 CONSTRAINT [PK_CFW_Fld_Values_OppItems] PRIMARY KEY CLUSTERED 
(
	[FldDTLID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING ON
GO

ALTER TABLE [dbo].[CFW_Fld_Values_OppItems]  WITH CHECK ADD  CONSTRAINT [FK_CFW_Fld_Values_OppItems_CFW_Fld_Master] FOREIGN KEY([Fld_ID])
REFERENCES [dbo].[CFW_Fld_Master] ([Fld_id])
GO

ALTER TABLE [dbo].[CFW_Fld_Values_OppItems] CHECK CONSTRAINT [FK_CFW_Fld_Values_OppItems_CFW_Fld_Master]
GO

