/******************************************************************
Project: Release 13.9 Date: 26.AUG.2020
Comments: STORE PROCEDURES
*******************************************************************/

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_IsKitCanBeBuild')
DROP FUNCTION fn_IsKitCanBeBuild
GO
CREATE FUNCTION [dbo].[fn_IsKitCanBeBuild]
(
	@numDomainID NUMERIC(18,0)
	,@numItemCode NUMERIC(18,0)
	,@numQty FLOAT
	,@numWarehouseID NUMERIC(18,0)
	,@vcSelectedKitChildItems VARCHAR(MAX)
)
RETURNS BIT
AS    
BEGIN 
	DECLARE @TEMPitems TABLE
	(
		numItemCode NUMERIC(18,0)
		,numQtyItemsReq FLOAT
		,numUOMId NUMERIC(18,0)
		,bitKitParent BIT
		,bitFirst BIT
		,numOnHand FLOAT
	)
  
	DECLARE @TempExistingItems TABLE
	(
		vcItem VARCHAR(100)
	)

	INSERT INTO @TempExistingItems
	(
		vcItem
	)
	SELECT 
		OutParam 
	FROM 
		SplitString(@vcSelectedKitChildItems,',')

	DECLARE @TEMPSelectedKitChilds TABLE
	(
		ChildKitItemID NUMERIC(18,0),
		ChildKitWarehouseItemID NUMERIC(18,0),
		ChildKitChildItemID NUMERIC(18,0),
		ChildKitChildWarehouseItemID NUMERIC(18,0),
		ChildQty FLOAT
	)

	INSERT INTO @TEMPSelectedKitChilds
	(
		ChildKitItemID
		,ChildKitChildItemID
		,ChildQty
	)
	SELECT 
		Reverse(ParseName(Replace(Reverse(vcItem), '-', '.'), 1))
		,Reverse(ParseName(Replace(Reverse(vcItem), '-', '.'), 2))
		,Reverse(ParseName(Replace(Reverse(vcItem), '-', '.'), 3))
	FROM  
	(
		SELECT 
			vcItem 
		FROM 
			@TempExistingItems
	) As [x]

	UPDATE 
		@TEMPSelectedKitChilds 
	SET 
		ChildKitWarehouseItemID=(SELECT TOP 1 numWareHouseItemID FROM WareHouseItems INNER JOIN WarehouseLocation ON WareHouseItems.numWLocationID = WarehouseLocation.numWLocationID WHERE numItemID=ChildKitItemID AND WareHouseItems.numWareHouseID=@numWarehouseID ORDER BY ISNULL(WarehouseLocation.numWLocationID,0) ASC)
		,ChildKitChildWarehouseItemID=(SELECT TOP 1 numWareHouseItemID FROM WareHouseItems INNER JOIN WarehouseLocation ON WareHouseItems.numWLocationID = WarehouseLocation.numWLocationID WHERE numItemID=ChildKitChildItemID AND WareHouseItems.numWareHouseID=@numWarehouseID ORDER BY ISNULL(WarehouseLocation.numWLocationID,0) ASC)


	IF (SELECT COUNT(*) FROM @TEMPSelectedKitChilds WHERE ISNULL(ChildKitItemID,0)=0) > 0
	BEGIN
		INSERT INTO @TEMPitems
		(
			numItemCode
			,numQtyItemsReq
			,numUOMId
			,bitKitParent
			,numOnHand
		)
		SELECT
			I.numItemCode
			,ChildQty
			,I.numBaseUnit
			,0
			,ISNULL((SELECT SUM(WIInner.numOnHand) FROM WarehouseItems WIInner WHERE numItemID=I.numItemCode AND numWareHouseID=WI.numWareHouseID),0)
		FROM
			@TEMPSelectedKitChilds T1
		INNER JOIN
			Item I
		ON
			T1.ChildKitChildItemID = I.numItemCOde
		INNER JOIN
			WareHouseItems WI
		ON
			T1.ChildKitChildWarehouseItemID = WI.numWareHouseItemID
		WHERE
			ISNULL(ChildKitItemID,0)=0
	END
	ELSE
	BEGIN
		;WITH CTE (numItemCode, vcItemName, bitKitParent, bitCalAmtBasedonDepItems, numQtyItemsReq,numUOMId) AS
		(
			SELECT
				ID.numChildItemID,
				I.vcItemName,
				ISNULL(I.bitKitParent,0),
				ISNULL(I.bitCalAmtBasedonDepItems,0),
				CAST((@numQty * ISNULL(ID.numQtyItemsReq,0) * ISNULL(dbo.fn_UOMConversion(ISNULL(ID.numUOMID, 0), I.numItemCode, @numDomainID, ISNULL(I.numBaseUnit, 0)),1)) AS FLOAT),
				ISNULL(numUOMId,0)
			FROM 
				[ItemDetails] ID
			INNER JOIN 
				[Item] I 
			ON 
				ID.[numChildItemID] = I.[numItemCode]
			WHERE   
				[numItemKitID] = @numItemCode
				AND ISNULL(I.charItemType,0) = 'P'
				AND 1 = (CASE 
							WHEN ISNULL(I.bitKitParent,0) = 1 AND LEN(ISNULL(@vcSelectedKitChildItems,'')) > 0 THEN 
								(CASE 
									WHEN numChildItemID IN (SELECT ChildKitItemID FROM @TEMPSelectedKitChilds) 
									THEN 1 
									ELSE 0 
								END) 
							ELSE (CASE 
									WHEN (SELECT COUNT(*) FROM @TEMPSelectedKitChilds WHERE ISNULL(ChildKitItemID,0)=0) > 0
									THEN	
										(CASE 
											WHEN numChildItemID IN (SELECT ChildKitChildItemID FROM @TEMPSelectedKitChilds WHERE ISNULL(ChildKitItemID,0)=0) 
											THEN 1 
											ELSE 0 
										END) 
									ELSE 1
								END)
						END)
			UNION ALL
			SELECT
				ID.numChildItemID,
				I.vcItemName,
				ISNULL(I.bitKitParent,0),
				ISNULL(I.bitCalAmtBasedonDepItems,0),
				CAST((ISNULL(Temp1.numQtyItemsReq,0) * ISNULL(ID.numQtyItemsReq,0) * ISNULL(dbo.fn_UOMConversion(ISNULL(ID.numUOMID, 0), I.numItemCode, @numDomainID, ISNULL(I.numBaseUnit, 0)),1)) AS FLOAT),
				ISNULL(ID.numUOMId,0)
			FROM 
				CTE As Temp1
			INNER JOIN
				[ItemDetails] ID
			ON
				ID.numItemKitID = Temp1.numItemCode
			INNER JOIN 
				[Item] I 
			ON 
				ID.[numChildItemID] = I.[numItemCode]
			WHERE
				Temp1.bitKitParent = 1
				AND ISNULL(I.charItemType,0) = 'P'
				AND EXISTS (SELECT ChildKitItemID FROM @TEMPSelectedKitChilds T2 WHERE ISNULL(Temp1.bitKitParent,0) = 1 AND t2.ChildKitItemID=ID.numItemKitID AND ID.numChildItemID = T2.ChildKitChildItemID)
		)

		INSERT INTO @TEMPitems
		(
			numItemCode
			,numQtyItemsReq
			,numUOMId
			,bitKitParent
			,numOnHand
		)
		SELECT
			numItemCode
			,numQtyItemsReq
			,numUOMId
			,bitKitParent
			,ISNULL((SELECT SUM(WIInner.numOnHand) FROM WarehouseItems WIInner WHERE numItemID=numItemCode AND numWareHouseID=@numWarehouseID),0)
		FROM
			CTE
	END

	DECLARE @bitKitCandBeBuild BIT

	IF EXISTS (SELECT * FROM @TEMPitems WHERE ISNULL(bitKitParent,0) = 0 AND numQtyItemsReq > numOnHand)
	BEGIN
		SET @bitKitCandBeBuild = 0
	END
	ELSE 
	BEGIN
		SET @bitKitCandBeBuild = 1
	END

	RETURN @bitKitCandBeBuild
END
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'FN'
                    AND NAME = 'GetHighestPriceLevelDiscountForEcommerceItem' ) 
    DROP FUNCTION GetHighestPriceLevelDiscountForEcommerceItem
GO
CREATE FUNCTION GetHighestPriceLevelDiscountForEcommerceItem
--Below function applies Price Rule on given Price and Qty and returns discount
    (
      @numItemCode AS NUMERIC(9),
      @numDomainID AS NUMERIC(9) = 0,
      @numWareHouseItemID AS NUMERIC(9)
    )
RETURNS DECIMAL(20,5)
AS BEGIN
	DECLARE @monHighestDiscount AS DECIMAL(20,5);
	
    DECLARE @monListPrice AS DECIMAL(20,5) ;
        SET @monListPrice = 0
    DECLARE @monVendorCost AS DECIMAL(20,5) ;
        SET @monVendorCost = 0

    IF ( ( @numWareHouseItemID > 0 )
         AND EXISTS ( SELECT    *
                      FROM      item
                      WHERE     numItemCode = @numItemCode
                                AND bitSerialized = 0
                                AND charItemType = 'P' )
       ) 
        BEGIN      
            SELECT  @monListPrice = ISNULL(monWListPrice, 0)
            FROM    WareHouseItems
            WHERE   numWareHouseItemID = @numWareHouseItemID      
            IF @monListPrice = 0 
                SELECT  @monListPrice = monListPrice
                FROM    Item
                WHERE   numItemCode = @numItemCode       
        END      
    ELSE 
        BEGIN      
            SELECT  @monListPrice = monListPrice
            FROM    Item
            WHERE   numItemCode = @numItemCode      
        END 

    SELECT  @monVendorCost = dbo.[fn_GetVendorCost](@numItemCode)

    SELECT TOP 1 @monHighestDiscount = CASE WHEN tintRuleType = 1 AND tintDiscountType = 1 --Deduct from List price & Percentage
										 THEN decDiscount 
										 WHEN tintRuleType = 1 AND tintDiscountType = 2 --Deduct from List price & Flat discount
										 THEN (decDiscount * 100) / (@monListPrice - decDiscount)
										 WHEN tintRuleType = 2 AND tintDiscountType = 1 --Add to Primary Vendor Cost & Percentage
										 THEN decDiscount
										 WHEN tintRuleType = 2 AND tintDiscountType = 2 --Add to Primary Vendor Cost & Flat discount
										 THEN (decDiscount * 100) / (@monVendorCost + decDiscount)
										 WHEN tintRuleType = 3 --Named Price 
										 THEN 0
							 END
    FROM    [PricingTable]
    WHERE   ISNULL(numItemCode, 0) = @numItemCode AND ISNULL(numCurrencyID,0) = 0
    ORDER BY [numPricingID] 
    
    RETURN ISNULL(@monHighestDiscount,0)
    
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p' AND NAME ='USP_BizRecurrence_Order')
DROP PROCEDURE USP_BizRecurrence_Order
GO
  
Create PROCEDURE [dbo].[USP_BizRecurrence_Order]  
	@numRecConfigID NUMERIC(18,0), 
	@numDomainID NUMERIC(18,0),
	@numUserCntID NUMERIC(18,0),
	@numOppID NUMERIC(18,0),
	@numRecurOppID NUMERIC(18,0) OUT,
	@numFrequency INTEGER,
	@dtEndDate DATE,
	@Date AS DATE
AS  
BEGIN  
BEGIN TRANSACTION;

BEGIN TRY

--DECLARE @Date AS DATE = GETDATE()

DECLARE @numDivisionID AS NUMERIC(18,0)
DECLARE @numOppType AS TINYINT
DECLARE @tintOppStatus AS TINYINT
DECLARE @DealStatus AS TINYINT
DECLARE @numStatus AS NUMERIC(9)
DECLARE @numCurrencyID AS NUMERIC(9)

DECLARE @intOppTcode AS NUMERIC(18,0)
DECLARE @numNewOppID AS NUMERIC(18,0)
DECLARE @fltExchangeRate AS FLOAT


--Get Existing Opportunity Detail
SELECT 
	@numDivisionID = numDivisionId, 
	@numOppType=tintOppType, 
	@tintOppStatus = tintOppStatus,
	@numCurrencyID=numCurrencyID,
	@numStatus = numStatus,
	@DealStatus = tintOppStatus
FROM 
	OpportunityMaster 
WHERE 
	numOppId = @numOppID                                            

IF ISNULL(@numCurrencyID,0) = 0 
BEGIN
	SET @fltExchangeRate=1
	SELECT @numCurrencyID= ISNULL(numCurrencyID,0) FROM dbo.Domain WHERE numDomainId=@numDomainId	
END
ELSE 
BEGIN
	SET @fltExchangeRate=dbo.GetExchangeRate(@numDomainID,@numCurrencyID)
END

INSERT INTO OpportunityMaster                                                                          
(                                                                             
	numContactId, numDivisionId, txtComments, numCampainID, bitPublicFlag, tintSource, tintSourceType, vcPOppName, 
	intPEstimatedCloseDate, numCreatedBy, bintCreatedDate, numDomainId, numRecOwner, lngPConclAnalysis, tintOppType, 
	numSalesOrPurType, numCurrencyID, fltExchangeRate, numAssignedTo, numAssignedBy, [tintOppStatus], numStatus, 
	vcOppRefOrderNo, bitStockTransfer,bitBillingTerms, intBillingDays,bitInterestType,fltInterest,vcCouponCode, 
	bitDiscountType,fltDiscount,bitPPVariance, vcMarketplaceOrderID, vcMarketplaceOrderReportId, numPercentageComplete,
	numAccountClass, tintTaxOperator, bitUseShippersAccountNo, bitUseMarkupShippingRate, numMarkupShippingRate, 
	intUsedShippingCompany, bintAccountClosingDate
)                                                                          
SELECT
	numContactId, numDivisionId, txtComments, numCampainID, bitPublicFlag, tintSource, tintSourceType, vcPOppName, 
	@Date, @numUserCntID, @Date, numDomainId, @numUserCntID, lngPConclAnalysis, tintOppType, 
	numSalesOrPurType, @numCurrencyID, @fltExchangeRate, numAssignedTo, numAssignedBy, [tintOppStatus], numStatus, vcOppRefOrderNo, 
	bitStockTransfer,bitBillingTerms, intBillingDays,bitInterestType,fltInterest,vcCouponCode, bitDiscountType, fltDiscount,
	bitPPVariance, vcMarketplaceOrderID, vcMarketplaceOrderReportId, numPercentageComplete, numAccountClass,tintTaxOperator, 
	bitUseShippersAccountNo, bitUseMarkupShippingRate, numMarkupShippingRate, intUsedShippingCompany, @Date 
FROM
	OpportunityMaster
WHERE                                                                        
	numOppId = @numOppID

SET @numNewOppID=SCOPE_IDENTITY() 

EXEC USP_OpportunityMaster_CT @numDomainID=@numDomainID,@numUserCntID=@numUserCntID,@numRecordID=@numNewOppID                                             
  
--Update OppName as per Name Template
EXEC dbo.USP_UpdateNameTemplateValue @numOppType,@numDomainID,@numNewOppID

--Map Custom Field	
DECLARE @tintPageID AS TINYINT;
SET @tintPageID=CASE WHEN @numOppType=1 THEN 2 ELSE 6 END 
  	
EXEC dbo.USP_AddParentChildCustomFieldMap @numDomainID = @numDomainID, @numRecordID = @numNewOppID, @numParentRecId = @numDivisionId, @tintPageID = @tintPageID
 	
IF ISNULL(@numStatus,0) > 0 AND @numOppType=1 AND isnull(@DealStatus,0)=1
BEGIN
	EXEC USP_ManageOpportunityAutomationQueue
			@numOppQueueID = 0, -- numeric(18, 0)
			@numDomainID = @numDomainID, -- numeric(18, 0)
			@numOppId = @numNewOppID, -- numeric(18, 0)
			@numOppBizDocsId = 0, -- numeric(18, 0)
			@numOrderStatus = @numStatus, -- numeric(18, 0)
			@numUserCntID = @numUserCntID, -- numeric(18, 0)
			@tintProcessStatus = 1, -- tinyint
			@tintMode = 1 -- TINYINT
END

IF ISNULL(@numStatus,0) > 0 AND @numOppType=1 AND isnull(@DealStatus,0)=1
BEGIN
	IF @numOppType=1 AND @tintOppStatus=1
	BEGIN
		EXEC USP_SalesFulfillmentQueue_Save @numDomainID,@numUserCntID,@numOppID,@numStatus
	END
END


IF (SELECT COUNT(*) FROM OpportunityItems WHERE numOppId = @numOppID) > 0
BEGIN
	-- INSERT OPPORTUNITY ITEMS

	INSERT INTO OpportunityItems                                                                          
	(
		numOppId,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,bitDropShip,bitDiscountType,
		fltDiscount,monTotAmtBefDiscount,vcItemName,vcModelID,vcManufacturer,vcPathForTImage,monVendorCost,numUOMId,bitWorkOrder,
		numVendorWareHouse,numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,numClassID,numToWarehouseItemID,vcAttributes,
		monAvgCost,bitItemPriceApprovalRequired,numRecurParentOppItemID
	)
	SELECT 
		@numNewOppID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,bitDropShip,bitDiscountType,
		fltDiscount,monTotAmtBefDiscount,vcItemName,vcModelID,vcManufacturer,vcPathForTImage,monVendorCost,numUOMId,bitWorkOrder,
		numVendorWareHouse,numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,numClassID,numToWarehouseItemID,vcAttributes,
		monAvgCost,bitItemPriceApprovalRequired,OpportunityItems.numoppitemtCode
	FROM
		OpportunityItems
	WHERE
		numOppID = @numOppID 
           
	EXEC USP_BizRecurrence_WorkOrder @numDomainId,@numUserCntID,@numNewOppID,@numOppID

	INSERT INTO OpportunityKitItems                                                                          
	(
		numOppId,numOppItemID,numChildItemID,numWareHouseItemId,numQtyItemsReq,numQtyItemsReq_Orig,numUOMId,numQtyShipped,monAvgCost
	)
	SELECT 
		@numNewOppId,OI.numoppitemtCode,OKI.numChildItemID,OKI.numWarehouseItemID,OKI.numQtyItemsReq,OKI.numQtyItemsReq,OKI.numUOMId,0,ISNULL(I.monAverageCost,0)
	FROM 
		OpportunityKitItems OKI
	INNER JOIN
		OpportunityItems OI 
	ON
		OKI.numOppItemID=OI.numRecurParentOppItemID
	INNER JOIN
		Item I
	ON
		OKI.numCHildItemID=I.numItemCode
	WHERE 
		OKI.numOppID=@numOppID
		AND OI.numOppId=@numNewOppID  

	INSERT INTO OpportunityKitChildItems                                                                          
	(
		numOppId,numOppItemID,numOppChildItemID,numItemID,numWareHouseItemId,numQtyItemsReq,numQtyItemsReq_Orig,numUOMId,numQtyShipped,monAvgCost
	)
	SELECT 
		@numNewOppId,OI.numoppitemtCode,OKI.numOppChildItemID,OKCI.numItemID,OKCI.numWarehouseItemID,OKCI.numQtyItemsReq,OKCI.numQtyItemsReq,OKCI.numUOMId,0,ISNULL(I.monAverageCost,0)
	FROM 
		OpportunityKitChildItems OKCI
	INNER JOIN
		OpportunityItems OI 
	ON
		OKCI.numOppItemID=OI.numRecurParentOppItemID
	INNER JOIN
		OpportuntiyKitItems OKI
	ON
		OKI.numOppItemID=OI.numOppItemtCOde
	INNER JOIN
		Item I
	ON
		OKCI.numItemID=I.numItemCode
	WHERE 
		OKCI.numOppID=@numOppID
		AND OI.numOppId=@numNewOppID 
END

DECLARE @TotalAmount AS FLOAT = 0       
DECLARE @tintCommitAllocation TINYINT                                                    
   
SELECT @TotalAmount = sum(monTotAmount) FROM OpportunityItems WHERE numOppId=@numNewOppID                                                                          
UPDATE OpportunityMaster SET monPamount=@TotalAmount WHERE numOppId=@numNewOppID 
SELECT @tintCommitAllocation=ISNULL(tintCommitAllocation,1) FROM Domain WHERE numDomainId=@numDomainId
--Updating the warehouse items              
DECLARE @tintShipped AS TINYINT = 0                

IF @tintOppStatus=1 AND @tintCommitAllocation=1
BEGIN       
	EXEC USP_UpdatingInventoryonCloseDeal @numNewOppID,0,@numUserCntID
END              

DECLARE @tintCRMType AS NUMERIC(9)
       
DECLARE @AccountClosingDate AS DATETIME 
SELECT @numDivisionID=numDivisionID,@AccountClosingDate=bintAccountClosingDate FROM OpportunityMaster WHERE numOppID=@numNewOppID         
SELECT @tintCRMType=tintCRMType FROM divisionmaster WHERE numDivisionID = @numDivisionID 

                 
--Add/Update Address Details
DECLARE @vcStreet varchar(100), @vcCity varchar(50), @vcPostalCode varchar(15), @vcCompanyName varchar(100), @vcAddressName VARCHAR(50), @vcAltContact VARCHAR(200)      
DECLARE @numState numeric(9),@numCountry numeric(9), @numCompanyId numeric(9), @numContact NUMERIC(18,0)
DECLARE @bitIsPrimary BIT, @bitAltContact BIT

SELECT  
	@vcCompanyName=vcCompanyName,
	@numCompanyId=div.numCompanyID 
FROM 
	CompanyInfo Com                            
JOIN 
	divisionMaster Div                            
ON 
	div.numCompanyID=com.numCompanyID                            
WHERE 
	div.numdivisionID=@numDivisionId

--Bill Address
IF (SELECT COUNT(*) FROM OpportunityAddress WHERE numOppID = @numOppID) > 0
BEGIN
	SELECT  
		@vcStreet=ISNULL(vcBillStreet,''),
		@vcCity=ISNULL(vcBillCity,''),
		@vcPostalCode=ISNULL(vcBillPostCode,''),
		@numState=ISNULL(numBillState,0),
		@numCountry=ISNULL(numBillCountry,0),
		@vcAddressName=vcAddressName,
		@numContact=numBillingContact,
		@bitAltContact=bitAltBillingContact,
		@vcAltContact=vcAltBillingContact
	FROM  
		dbo.OpportunityAddress 
	WHERE   
		numOppID = @numOppID

  	EXEC dbo.USP_UpdateOppAddress
		@numOppID = @numNewOppID,
		@byteMode = 0,
		@vcStreet = @vcStreet,
		@vcCity = @vcCity,
		@vcPostalCode = @vcPostalCode,
		@numState = @numState,
		@numCountry = @numCountry,
		@vcCompanyName = @vcCompanyName,
		@numCompanyId = 0,
		@vcAddressName = @vcAddressName,
		@bitCalledFromProcedure=1,
		@numContact = @numContact,
		@bitAltContact = @bitAltContact,
		@vcAltContact = @vcAltContact
END
  
--Ship Address
IF (SELECT COUNT(*) FROM OpportunityAddress WHERE numOppID = @numOppID) > 0
BEGIN
	SELECT  
		@vcStreet=ISNULL(vcShipStreet,''),
		@vcCity=ISNULL(vcShipCity,''),
		@vcPostalCode=ISNULL(vcShipPostCode,''),
		@numState=ISNULL(numShipState,0),
		@numCountry=ISNULL(numShipCountry,0),
		@vcAddressName=vcAddressName,
		@numContact=numShippingContact,
		@bitAltContact=bitAltShippingContact,
		@vcAltContact=vcAltShippingContact
    FROM  
		dbo.OpportunityAddress
	WHERE   
		numOppID = @numOppID
 
  	EXEC dbo.USP_UpdateOppAddress
		@numOppID = @numNewOppID,
		@byteMode = 1,
		@vcStreet = @vcStreet,
		@vcCity = @vcCity,
		@vcPostalCode = @vcPostalCode,
		@numState = @numState,
		@numCountry = @numCountry,
		@vcCompanyName = @vcCompanyName,
		@numCompanyId =@numCompanyId,
		@vcAddressName = @vcAddressName,
		@bitCalledFromProcedure=1,
		@numContact = @numContact,
		@bitAltContact = @bitAltContact,
		@vcAltContact = @vcAltContact
END


-- IMPORTANT: KEEP THIS BELOW ADDRESS UPDATE LOGIC
--Insert Tax for Division   
IF @numOppType=1 OR (@numOppType=2 and (select isnull(bitPurchaseTaxCredit,0) from Domain where numDomainId=@numDomainId)=1)
BEGIN  
	INSERT INTO dbo.OpportunityMasterTaxItems 
	(
		numOppId,
		numTaxItemID,
		fltPercentage,
		tintTaxType,
		numTaxID
	) 
	SELECT 
		@numNewOppID,
		TI.numTaxItemID,
		TEMPTax.decTaxValue,
		TEMPTax.tintTaxType,
		0
	FROM 
		TaxItems TI 
	JOIN 
		DivisionTaxTypes DTT 
	ON 
		TI.numTaxItemID = DTT.numTaxItemID
	CROSS APPLY
	(
		SELECT
			decTaxValue,
			tintTaxType
		FROM
			dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,TI.numTaxItemID,@numNewOppID,0,NULL)
	) AS TEMPTax
	WHERE 
		DTT.numDivisionID=@numDivisionId 
		AND DTT.bitApplicable=1
	UNION 
	SELECT 
		@numNewOppID,
		0,
		TEMPTax.decTaxValue,
		TEMPTax.tintTaxType,
		0
	FROM 
		dbo.DivisionMaster 
	CROSS APPLY
	(
		SELECT
			decTaxValue,
			tintTaxType
		FROM
			dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,0,@numNewOppID,1,NULL)
	) AS TEMPTax
	WHERE 
		bitNoTax=0 
		AND numDivisionID=@numDivisionID	
	UNION 
	SELECT
		@numNewOppID
		,1
		,decTaxValue
		,tintTaxType
		,numTaxID
	FROM
		dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,1,@numNewOppID,1,NULL)	
END

--Delete Tax for Opportunity Items if item deleted 
DELETE FROM OpportunityItemsTaxItems WHERE numOppId=@numNewOppID AND numOppItemID NOT IN (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId=@numNewOppID)

--Insert Tax for Opportunity Items
INSERT INTO dbo.OpportunityItemsTaxItems 
(
	numOppId,
	numOppItemID,
	numTaxItemID,
	numTaxID
) 
SELECT 
	@numNewOppID,
	OI.numoppitemtCode,
	TI.numTaxItemID,
	0
FROM 
	dbo.OpportunityItems OI 
JOIN 
	dbo.ItemTax IT 
ON 
	OI.numItemCode=IT.numItemCode 
JOIN
	TaxItems TI 
ON 
	TI.numTaxItemID=IT.numTaxItemID 
WHERE 
	OI.numOppId=@numNewOppID AND 
	IT.bitApplicable=1 AND 
	OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numNewOppID)
UNION
SELECT 
	@numNewOppID,
	OI.numoppitemtCode,
	0,
	0
FROM 
	dbo.OpportunityItems OI 
JOIN 
	dbo.Item I 
ON 
	OI.numItemCode=I.numItemCode
WHERE 
	OI.numOppId=@numNewOppID AND 
	I.bitTaxable=1 AND
	OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numNewOppID)
UNION
SELECT
	@numNewOppID,
	OI.numoppitemtCode,
	1,
	TD.numTaxID
FROM
	dbo.OpportunityItems OI 
INNER JOIN
	ItemTax IT
ON
	IT.numItemCode = OI.numItemCode
INNER JOIN
	TaxDetails TD
ON
	TD.numTaxID = IT.numTaxID
	AND TD.numDomainId = @numDomainId
WHERE
	OI.numOppId = @numNewOppID
	AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numNewOppID)

  
UPDATE OpportunityMaster SET monDealAmount=dbo.GetDealAmount(@numNewOppID,@Date,0) WHERE numOppId=@numNewOppID


IF @numNewOppID > 0
BEGIN
	-- Insert newly created opportunity to transaction
	INSERT INTO RecurrenceTransaction ([numRecConfigID],[numRecurrOppID],[dtCreatedDate]) VALUES (@numRecConfigID,@numNewOppID,GETDATE())

	-- Insert newly created opportunity to transaction history - Records are not deleted in this table when parent record is delete
	INSERT INTO RecurrenceTransactionHistory ([numRecConfigID],[numRecurrOppID],[dtCreatedDate]) VALUES (@numRecConfigID,@numNewOppID,GETDATE())

	--Get next recurrence date for order 
	DECLARE @dtNextRecurrenceDate DATE = NULL

	SET @dtNextRecurrenceDate = (CASE @numFrequency
								WHEN 1 THEN DATEADD(D,1,@Date) --Daily
								WHEN 2 THEN DATEADD(D,7,@Date) --Weekly
								WHEN 3 THEN DATEADD(M,1,@Date) --Monthly
								WHEN 4 THEN DATEADD(M,4,@Date)  --Quarterly
								END)
	
	--Increase value of number of transaction completed by 1
	UPDATE RecurrenceConfiguration SET numTransaction = ISNULL(numTransaction,0) + 1 WHERE numRecConfigID = @numRecConfigID
			
	PRINT @dtNextRecurrenceDate
	PRINT @dtEndDate

	-- Set recurrence status as completed if next recurrence date is greater than end date
	If @dtNextRecurrenceDate > @dtEndDate
	BEGIN
		UPDATE RecurrenceConfiguration SET bitCompleted = 1 WHERE numRecConfigID = @numRecConfigID
	END
	ELSE -- Set next recurrence date
	BEGIN
		UPDATE RecurrenceConfiguration SET dtNextRecurrenceDate = @dtNextRecurrenceDate WHERE numRecConfigID = @numRecConfigID
	END
END


UPDATE OpportunityMaster SET bitRecurred = 1 WHERE numOppId = @numNewOppID

SET @numRecurOppID = @numNewOppID

END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000);
    DECLARE @ErrorSeverity INT;
    DECLARE @ErrorState INT;

    SELECT @ErrorMessage = ERROR_MESSAGE(), @ErrorSeverity = ERROR_SEVERITY(), @ErrorState = ERROR_STATE();

    RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState);

	IF @@TRANCOUNT > 0
        ROLLBACK TRANSACTION;
END CATCH

IF @@TRANCOUNT > 0
    COMMIT TRANSACTION;

END  
--modified by anoop jayaraj              
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_deleteoppurtunity')
DROP PROCEDURE usp_deleteoppurtunity
GO
CREATE PROCEDURE [dbo].[USP_DeleteOppurtunity]
 @numOppId numeric(9) ,        
 @numDomainID as numeric(9),
 @numUserCntID AS NUMERIC(9)                       
AS                          
             
       
if exists(select * from OpportunityMaster where numOppID=@numOppId and numDomainID=@numDomainID)         
BEGIN 
	DECLARE @tintCommitAllocation AS TINYINT
	DECLARE @bitAllocateInventoryOnPickList AS BIT

	SELECT @tintCommitAllocation=ISNULL(tintCommitAllocation,1) FROM Domain WHERE numDomainId=@numDomainID
	SELECT @bitAllocateInventoryOnPickList=ISNULL(bitAllocateInventoryOnPickList,0) FROM OpportunityMaster INNER JOIN DivisionMaster ON OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID WHERE numOppID=@numOppID

SET XACT_ABORT ON;

BEGIN TRY 
BEGIN TRAN

	IF (SELECT COUNT(*) FROM MassSalesOrderQueue WHERE numOppID = @numOppId AND ISNULL(bitExecuted,0) = 0) > 0
	BEGIN
		RAISERROR ( 'MASS SALES ORDER', 16, 1 )
		RETURN
	END
	IF (SELECT COUNT(*) FROM RecurrenceConfiguration WHERE numOppID = @numOppId AND numDomainID=@numDomainID AND ISNULL(bitMarkForDelete,0) = 0) > 0
	BEGIN
		RAISERROR ( 'RECURRING ORDER OR BIZDOC', 16, 1 )
		RETURN
	END
	ELSE IF (SELECT COUNT(*) FROM RecurrenceTransaction INNER JOIN RecurrenceConfiguration ON RecurrenceTransaction.numRecConfigID = RecurrenceConfiguration.numRecConfigID WHERE RecurrenceTransaction.numRecurrOppID = @numOppId AND ISNULL(RecurrenceConfiguration.bitDisabled,0) = 0) > 0
	BEGIN	
		RAISERROR ( 'RECURRED ORDER', 16, 1 ) ;
		RETURN ;
	END
	ELSE IF (SELECT COUNT(*) FROM OpportunityBizdocs WHERE numOppId=@numOppId AND numBizDocId=296 AND ISNULL(bitFulfilled,0) = 1) > 0
	BEGIN
		RAISERROR ( 'FULFILLED_ITEMS', 16, 1 ) ;
		RETURN ;
	END
	ELSE IF (SELECT COUNT(*) FROM WorkOrder WHERE numDomainID=@numDomainID AND numOppId=@numOppId AND ISNULL(numWOStatus,0) <> 23184) > 0
	BEGIN
		RAISERROR ( 'WORK_ORDER_EXISTS', 16, 1 ) ;
		RETURN ;
	END

      IF EXISTS ( SELECT  *
                FROM    dbo.CaseOpportunities
                WHERE   numOppID = @numOppId
                        AND numDomainID = @numDomainID )         
        BEGIN
            RAISERROR ( 'CASE DEPENDANT', 16, 1 ) ;
            RETURN ;
        END	
	 IF EXISTS ( SELECT  *
			FROM    dbo.ReturnHeader
			WHERE   numOppId = @numOppId
					AND numDomainId= @numDomainID )         
	BEGIN
		RAISERROR ( 'RETURN_EXIST', 16, 1 ) ;
		RETURN ;
	END	

      
      
      

DECLARE @tintError TINYINT;SET @tintError=0

EXECUTE USP_CheckCanbeDeleteOppertunity @numOppId,@tintError OUTPUT
  
  IF @tintError=1
  BEGIN
  	 RAISERROR ( 'OppItems DEPENDANT', 16, 1 ) ;
     RETURN ;
  END
  
  EXEC dbo.USP_ValidateFinancialYearClosingDate
	@numDomainID = @numDomainID, --  numeric(18, 0)
	@tintRecordType = 2, --  tinyint
	@numRecordID = @numOppId --  numeric(18, 0)
	
  --Credit Balance
   Declare @monCreditAmount as DECIMAL(20,5);Set @monCreditAmount=0
   Declare @monCreditBalance as DECIMAL(20,5);Set @monCreditBalance=0
   Declare @numDivisionID as numeric(9);Set @numDivisionID=0
   Declare @tintOppType as tinyint

   Select @numDivisionID=numDivisionID,@tintOppType=tintOppType from OpportunityMaster WHERE  numOppId=@numOppId  and numDomainID= @numDomainID 
   
    IF @tintOppType=1 --Sales
      Select @monCreditBalance=isnull(monSCreditBalance,0) from DivisionMaster WHERE  numDivisionID=@numDivisionID  and numDomainID= @numDomainID 
    else IF @tintOppType=2 --Purchase
		UPDATE OpportunityMaster SET numReleaseStatus = 1 WHERE numDomainId=@numDomainID AND numOppId IN (SELECT DISTINCT numOppId FROM OpportunityItemsReleaseDates WHERE  numPurchasedOrderID = @numOppId)
		UPDATE OpportunityItemsReleaseDates SET tintStatus = 1, numPurchasedOrderID=0 WHERE numPurchasedOrderID = @numOppId


      Select @monCreditBalance=isnull(monPCreditBalance,0) from DivisionMaster WHERE  numDivisionID=@numDivisionID  and numDomainID= @numDomainID 
   
   Select @monCreditAmount=sum(monAmount) from CreditBalanceHistory where numReturnID in (select numReturnID from [Returns] WHERE [numOppId] = @numOppId)
	
	IF ( @monCreditAmount > @monCreditBalance )         
        BEGIN
            RAISERROR ( 'CreditBalance DEPENDANT', 16, 1 ) ;
            RETURN ;
        END	
        
  declare @tintOppStatus as tinyint    
 declare @tintShipped as tinyint                
  select @tintOppStatus=tintOppStatus,@tintShipped=tintShipped from OpportunityMaster where numOppID=@numOppId                
  
  DECLARE @isOrderItemsAvailable AS INT = 0

	SELECT 
		@isOrderItemsAvailable = COUNT(*) 
	FROM    
		OpportunityItems OI
	JOIN 
		dbo.OpportunityMaster OM ON OI.numOppId = OM.numOppId
	JOIN 
		Item I ON OI.numItemCode = I.numItemCode
	WHERE   
		(charitemtype='P' OR 1=(CASE WHEN tintOppType=1 THEN 
								CASE WHEN ISNULL(I.bitKitParent,0) = 1 AND ISNULL(I.bitAssembly,0) = 1 THEN 0
									 WHEN ISNULL(I.bitKitParent,0) = 1 THEN 1
									 ELSE 0 END 
								ELSE 0 END)) AND OI.numOppId = @numOppId
							   AND ( bitDropShip = 0
									 OR bitDropShip IS NULL
								   ) 

  if ((@tintOppStatus=1 and @tintShipped=0) OR  @tintShipped=1) AND @tintCommitAllocation <> 2            
  begin    
    exec USP_RevertDetailsOpp @numOppId,0,1,@numUserCntID                
  end                                 
                        
  DELETE FROM [OpportunitySalesTemplate] WHERE [numOppId] =@numOppId 
  delete OpportunityLinking where   numChildOppID=  @numOppId or   numParentOppID=    @numOppId          
  delete RECENTITEMS where numRecordID =  @numOppId and chrRecordType='O'             
                

	IF @tintOppType = 1 AND @tintOppStatus = 1 -- SALES ORDER
	BEGIN
		-- MAKE SERIAL/LOT# NUUMBER AVAILABLE FOR USE AS SALES ORDER IS DELETED
		UPDATE WHIDL
			SET WHIDL.numQty = (CASE WHEN Item.bitLotNo = 1 THEN ISNULL(WHIDL.numQty,0) + ISNULL(OWSI.numQty,0) ELSE 1 END)
		FROM 
			WareHouseItmsDTL WHIDL
		INNER JOIN
			OppWarehouseSerializedItem OWSI
		ON
			WHIDL.numWareHouseItmsDTLID = OWSI.numWarehouseItmsDTLID
			AND WHIDL.numWareHouseItemID = OWSI.numWarehouseItmsID
		INNER JOIN
			WareHouseItems
		ON
			WHIDL.numWareHouseItemID = WareHouseItems.numWareHouseItemID
		INNER JOIN
			Item
		ON
			WareHouseItems.numItemID = Item.numItemCode
		WHERE
			OWSI.numOppID = @numOppId
	END
	ELSE IF @tintOppType = 2 AND @tintOppStatus = 1 -- PURCHASE ORDER
	BEGIN
		IF (
			SELECT 
				COUNT(*)
			FROM 
				OppWarehouseSerializedItem OWSI 
			INNER JOIN 
				OpportunityMaster OM 
			ON 
				OWSI.numOppID=OM.numOppId 
				AND tintOppType=1 
			WHERE 
				OWSI.numWarehouseItmsDTLID IN (SELECT numWarehouseItmsDTLID FROM OppWarehouseSerializedItem OWSIInner WHERE OWSIInner.numOppID = @numOppID)
			) > 0
		BEGIN
			RAISERROR ('SERIAL/LOT#_USED', 16, 1 ) ;
		END
		ELSE
		BEGIN
			-- REMOVE SERIAL/LOT# NUMBER FROM INVENTORY BECAUSE PURCHASE ORDER IS DELETED
			UPDATE WHIDL
				SET WHIDL.numQty = (CASE WHEN Item.bitLotNo = 1 THEN ISNULL(WHIDL.numQty,0) - ISNULL(OWSI.numQty,0) ELSE 0 END)
			FROM 
				WareHouseItmsDTL WHIDL
			INNER JOIN
				OppWarehouseSerializedItem OWSI
			ON
				WHIDL.numWareHouseItmsDTLID = OWSI.numWarehouseItmsDTLID
				AND WHIDL.numWareHouseItemID = OWSI.numWarehouseItmsID
			INNER JOIN
				WareHouseItems
			ON
				WHIDL.numWareHouseItemID = WareHouseItems.numWareHouseItemID
			INNER JOIN
				Item
			ON
				WareHouseItems.numItemID = Item.numItemCode
			WHERE
				OWSI.numOppID = @numOppId
		END
	END
									        
  delete from OppWarehouseSerializedItem where numOppID=  @numOppId                  
                  
                        
  delete from OpportunityItemLinking where numNewOppID=@numOppId                      
                         
  delete from OpportunityAddress  where numOppID=@numOppId


  DELETE FROM OpportunityBizDocKitItems WHERE [numOppBizDocItemID] IN 
  (SELECT [numOppBizDocItemID] FROM [OpportunityBizDocItems] WHERE [numOppBizDocID] IN 
	(select numOppBizDocsId from OpportunityBizDocs where numOppId=@numOppId))
	
  delete from OpportunityBizDocItems where numOppBizDocID in                        
  (select numOppBizDocsId from OpportunityBizDocs where numOppId=@numOppId)

  delete from OpportunityItemsTaxItems WHERE numOppID=@numOppID
  delete from OpportunityMasterTaxItems WHERE numOppID=@numOppID
  
  delete from OpportunityBizDocDtl where numBizDocID in                        
  (select numOppBizDocsId from OpportunityBizDocs where numOppId=@numOppId)                        
           
   DELETE FROM OpportunityBizDocsPaymentDetails WHERE [numBizDocsPaymentDetId] IN 
	 (SELECT [numBizDocsPaymentDetId] FROM [OpportunityBizDocsDetails] where numBizDocsId in                        
		(select numOppBizDocsId from OpportunityBizDocs where numOppId=@numOppId)) 
               
  delete from OpportunityBizDocsDetails where numBizDocsId in                        
  (select numOppBizDocsId from OpportunityBizDocs where numOppId=@numOppId)  

DELETE FROM [ShippingReportItems] WHERE [numShippingReportId] IN  (SELECT numShippingReportId from [ShippingReport] WHERE [numOppBizDocId] IN (select numOppBizDocsId from OpportunityBizDocs where numOppId=@numOppId))
DELETE FROM [ShippingBox] WHERE [numShippingReportId] IN  (SELECT numShippingReportId from [ShippingReport] WHERE [numOppBizDocId] IN (select numOppBizDocsId from OpportunityBizDocs where numOppId=@numOppId))
DELETE FROM [ShippingReport] WHERE [numOppBizDocId] IN (select numOppBizDocsId from OpportunityBizDocs where numOppId=@numOppId)

delete from DocumentWorkflow where numDocID in 
(select numOppBizDocsId from OpportunityBizDocs where numOppId=@numOppId) and cDocType='B'      

delete from BizDocAction where numBizActionId in(select numBizActionId from BizActionDetails where numOppBizDocsId in
(select numOppBizDocsId from OpportunityBizDocs where numOppId=@numOppId) and btDocType=1)

delete from BizActionDetails where numOppBizDocsId in
(select numOppBizDocsId from OpportunityBizDocs where numOppId=@numOppId) and btDocType=1

  delete from OpportunityBizDocs where numOppId=@numOppId                        
                          
  delete from OpportunityContact where numOppId=@numOppId                        
                          
  delete from OpportunityDependency where numOpportunityId=@numOppId                        
                          
  delete from TimeAndExpense where numOppId=@numOppId   and numDomainID= @numDomainID                     
  
  --Credit Balance
  delete from CreditBalanceHistory where numReturnID in (select numReturnID from [Returns] WHERE [numOppId] = @numOppId)

    IF @tintOppType=1 --Sales
		update DivisionMaster set monSCreditBalance=isnull(monSCreditBalance,0) - isnull(@monCreditAmount,0) where numDivisionID=@numDivisionID and numDomainID=@numDomainID
    else IF @tintOppType=2 --Purchase
		update DivisionMaster set monPCreditBalance=isnull(monPCreditBalance,0) - isnull(@monCreditAmount,0) where numDivisionID=@numDivisionID and numDomainID=@numDomainID
  
	UPDATE
		DCU
	SET 
		intCodeUsed = ISNULL(intCodeUsed,0) - 1
	FROM
		DiscountCodeUsage DCU
	INNER JOIN
		(
			SELECT
				numDivisionId
				,numDiscountID
			FROM
				OpportunityMaster
			WHERE
				OpportunityMaster.numOppId=@numOppId
		) AS T1
	ON
		DCU.numDiscountId = T1.numDiscountID
		AND DCU.numDivisionId = T1.numDivisionId

  DELETE FROM [Returns] WHERE [numOppId] = @numOppId
                         
  delete from OpportunityKitItems where numOppId=@numOppId                        
                          
  delete from OpportunityItems where numOppId=@numOppId                        
                          
  DELETE FROM dbo.ProjectProgress WHERE numOppId =@numOppId AND numDomainId=@numDomainID
                          
  delete from OpportunityStageDetails where numOppId=@numOppId
                          
  delete from OpportunitySubStageDetails where numOppId=@numOppId
                                               
  DELETE FROM [OpportunityRecurring] WHERE [numOppId] = @numOppId
  
  DELETE FROM [OpportunityMasterAPI] WHERE [numOppId] = @numOppId
  
  DELETE FROM [WebApiOrderDetails] WHERE [numOppId] = @numOppId
  
  DELETE FROM [WebApiOppItemDetails] WHERE [numOppId] = @numOppId
  
  /*
  added by chintan: reason: when bizdoc is deleted and journal entries are not deleted in exceptional case, 
  it throws Foreign key reference error when deleting order.
  */
  DELETE FROM dbo.General_Journal_Details WHERE numJournalId IN (SELECT numJournal_Id FROM dbo.General_Journal_Header WHERE numOppId=@numOppId AND numDomainId=@numDomainID)
  DELETE FROM dbo.General_Journal_Header WHERE numOppId=@numOppId AND numDomainId= @numDomainID
 
	IF @tintCommitAllocation = 2
	BEGIN
		DELETE FROM OpportunityMaster WHERE numOppId=@numOppId AND numDomainID= @numDomainID
	END
	ELSE
	BEGIN
		IF ISNULL(@tintOppStatus,0)=0 OR ISNULL(@isOrderItemsAvailable,0) = 0 OR (SELECT COUNT(*) FROM WareHouseItems_Tracking WHERE numDomainID=@numDomainID AND numReferenceID=@numOppId AND tintRefType IN (3,4) AND vcDescription LIKE '%Deleted%' AND CAST(dtCreatedDate AS DATE) = CAST(GETUTCDATE() AS DATE)) > 0
			DELETE FROM OpportunityMaster WHERE numOppId=@numOppId and numDomainID= @numDomainID             
		ELSE
			RAISERROR ('INVENTORY IM-BALANCE', 16, 1);
	END
COMMIT TRAN 

    END TRY 
    BEGIN CATCH		
        DECLARE @strMsg VARCHAR(200)
        SET @strMsg = ERROR_MESSAGE()
        IF ( @@TRANCOUNT > 0 ) 
            BEGIN
             RAISERROR ( @strMsg, 16, 1 ) ;
                ROLLBACK TRAN
                RETURN 1
            END
    END CATCH	

SET XACT_ABORT OFF;
END 
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DemandForecast_UpdateInventory')
DROP PROCEDURE dbo.USP_DemandForecast_UpdateInventory
GO
CREATE PROCEDURE [dbo].[USP_DemandForecast_UpdateInventory]
	@numOppID NUMERIC (18,0),
	@numUserCntID NUMERIC(18,0)
AS 
BEGIN

	-- Update Inventory when order is created from demand forecast
	exec USP_UpdatingInventoryonCloseDeal @numOppID,0,@numUserCntID
	
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetDropDownValue')
DROP PROCEDURE USP_GetDropDownValue
GO
Create PROCEDURE [dbo].[USP_GetDropDownValue]     
    @numDomainID numeric(18, 0),
    @numListID numeric(18, 0),
    @vcListItemType AS VARCHAR(5),
    @vcDbColumnName VARCHAR(100)
as                 

CREATE TABLE #temp(numID VARCHAR(500),vcData VARCHAR(max))

INSERT INTO #temp  SELECT 0,'-- Select One --'

			 IF @vcListItemType='LI' OR @vcListItemType='T'                                                    
				BEGIN  
					INSERT INTO #temp                                                   
					SELECT Ld.numListItemID,vcData from ListDetails LD          
					left join listorder LO on Ld.numListItemID= LO.numListItemID  and lo.numDomainId = @numDomainID
					where (Ld.numDomainID=@numDomainID or Ld.constFlag=1) and Ld.numListID=@numListID order by intSortOrder
				end  
 			 ELSE IF @vcListItemType='C'                                                     
				begin    
		   		    INSERT INTO #temp                                                   
	  			    SELECT  numCampaignID,vcCampaignName + CASE bitIsOnline WHEN 1 THEN ' (Online)' ELSE ' (Offline)' END AS vcCampaignName
					FROM CampaignMaster WHERE numDomainID = @numDomainID
				end  
			 ELSE IF @vcListItemType='AG'                                                     
				begin      
		   		    INSERT INTO #temp                                                   
					SELECT numGrpID, vcGrpName FROM groups       
					ORDER BY vcGrpName                                               
				end   
			else if @vcListItemType='DC'                                                     
				begin    
		   		    INSERT INTO #temp                                                   
					select numECampaignID,vcECampName from ECampaign where numDomainID= @numDomainID                                                       
				end 
			else if @vcListItemType='U' OR @vcDbColumnName='numManagerID'
			    BEGIN
		   		     INSERT INTO #temp                                                   
			    	 SELECT A.numContactID,ISNULL(A.vcFirstName,'') +' '+ ISNULL(A.vcLastName,'') as vcUserName          
					 from UserMaster UM join AdditionalContactsInformation A        
					 on UM.numUserDetailId=A.numContactID          
					 where UM.numDomainID=@numDomainID and UM.numDomainID=A.numDomainID  ORDER BY A.vcFirstName,A.vcLastName 
				END
			else if @vcListItemType='S'
			    BEGIN
		   		     INSERT INTO #temp                                                   
			    	 SELECT S.numStateID,S.vcState          
					 from State S      
					 where S.numDomainID=@numDomainID ORDER BY S.vcState
				END
			ELSE IF @vcListItemType = 'OC' 
				BEGIN
					INSERT INTO #temp   
					SELECT DISTINCT C.numCurrencyID,C.vcCurrencyDesc FROM dbo.Currency C WHERE C.numDomainID=@numDomainID
				END
			ELSE IF @vcListItemType = 'UOM' 
				BEGIN
					INSERT INTO #temp   
					SELECT  numUOMId AS numItemID,vcUnitName AS vcItemName FROM UOM WHERE numDomainID = @numDomainID
				END	
			ELSE IF @vcListItemType = 'IG' 
				BEGIN
					INSERT INTO #temp   
					SELECT numItemGroupID,vcItemGroup FROM dbo.ItemGroups WHERE numDomainID=@numDomainID   	
				END
			ELSE IF @vcListItemType = 'SYS'
				BEGIN
					INSERT INTO #temp
					SELECT 0 AS numItemID,'Lead' AS vcItemName
					UNION ALL
					SELECT 1 AS numItemID,'Prospect' AS vcItemName
					UNION ALL
					SELECT 2 AS numItemID,'Account' AS vcItemName
				END
			ELSE IF @vcListItemType = 'PP' 
				BEGIN
					INSERT INTO #temp 
					SELECT 'P','Inventory Item'
					UNION 
					SELECT 'N','Non-Inventory Item'
					UNION 
					SELECT 'S','Service'
				END

				--<asp:ListItem Value="0" Selected="True">0</asp:ListItem>
    --                                <asp:ListItem Value="25" Selected="True">25%</asp:ListItem>
    --                                <asp:ListItem Value="50">50%</asp:ListItem>
    --                                <asp:ListItem Value="75">75%</asp:ListItem>
    --                                <asp:ListItem Value="100">100%</asp:ListItem>
				ELSE IF @vcListItemType = 'SP' --Progress Percentage
				BEGIN
					INSERT INTO #temp 
					SELECT '0','0%'
					UNION 
					SELECT '25','25%'
					UNION 
					SELECT '50','50%'
					UNION 
					SELECT '75','75%'
					UNION 
					SELECT '100','100%'
				END
				ELSE IF @vcListItemType = 'ST' --Stage Details
				BEGIN
					INSERT INTO #temp 
					SELECT vcStageName ,vcStageName  from StagePercentageDetails where numDomainId=@numDomainID and slp_id=@numListID  AND ISNULL(numProjectID,0)= 0 AND ISNULL(numOppID,0)= 0
				END
				ELSE IF @vcListItemType = 'SG' --Stage Details
				BEGIN
					INSERT INTO #temp 
					SELECT vcStageName ,vcStageName  from StagePercentageDetails where numDomainId=@numDomainID   AND ISNULL(numProjectID,0)= 0 AND ISNULL(numOppID,0)= 0
				END
				ELSE IF @vcListItemType = 'SM' --Reminer,snooze time
				BEGIN
					INSERT INTO #temp 
					SELECT '5','5 minutes'
					UNION 
					SELECT '15','15 minutes'
					UNION 
					SELECT '30','30 Minutes'
					UNION 
					SELECT '60','1 Hour'
					UNION 
					SELECT '240','4 Hour'
					UNION 
					SELECT '480','8 Hour'
					UNION 
					SELECT '1440','24 Hour'

				END
			 ELSE IF @vcListItemType = 'PT' --Pin To
				BEGIN
					INSERT INTO #temp 
					SELECT '1','Sales Opportunity'
					UNION 
					SELECT '2','Purchase Opportunity'
					UNION 
					SELECT '3','Sales Order'
					UNION 
					SELECT '4','Purchase Order'
					UNION 
					SELECT '5','Project'
					UNION 
					SELECT '6','Cases'
					UNION 
					SELECT '7','Item'

				END
				ELSE IF @vcListItemType = 'DV' --Customer
				BEGIN
					--INSERT INTO #temp 
					--SELECT vcStageName ,vcStageName  from StagePercentageDetails where numDomainId=@numDomainID and slp_id=@numListID  AND ISNULL(numProjectID,0)= 0 AND ISNULL(numOppID,0)= 0
				INSERT INTO #temp 
					SELECT d.numDivisionID,a.vcCompanyname + Case when isnull(d.numCompanyDiff,0)>0 then  ' ' + dbo.fn_getlistitemname(d.numCompanyDiff) + ':' + isnull(d.vcCompanyDiff,'') else '' end as vcCompanyname from   companyinfo AS a                      
					join divisionmaster d                      
					on  a.numCompanyid=d.numCompanyid
					WHERE D.numDomainID=@numDomainID
			
				END
				ELSE IF @vcListItemType = 'BD' --Net Days
				BEGIN
					--INSERT INTO #temp 
					--SELECT vcStageName ,vcStageName  from StagePercentageDetails where numDomainId=@numDomainID and slp_id=@numListID  AND ISNULL(numProjectID,0)= 0 AND ISNULL(numOppID,0)= 0
				INSERT INTO #temp 
						SELECT numTermsID,vcTerms	
						FROM dbo.BillingTerms 
						WHERE 
						numDomainID = @numDomainID
						AND ISNULL(bitActive,0) = 1
			
				END
				ELSE IF @vcListItemType = 'BT' --Net Days
				BEGIN
					--INSERT INTO #temp 
					--SELECT vcStageName ,vcStageName  from StagePercentageDetails where numDomainId=@numDomainID and slp_id=@numListID  AND ISNULL(numProjectID,0)= 0 AND ISNULL(numOppID,0)= 0
				INSERT INTO #temp 
							SELECT numBizDocTempID,    
							vcTemplateName
							FROM BizDocTemplate  
							WHERE [numDomainID] = @numDomainID and tintTemplateType=0
			
				END
				ELSE IF @vcListItemType = 'PSS' --Parcel Shipping Service
				BEGIN
					INSERT INTO #temp (numID,vcData) 
					SELECT
						numShippingServiceID
						,vcShipmentService
					FROM 
						ShippingService
					WHERE
						numDomainID=@numDomainID OR ISNULL(numDomainID,0)=0
					UNION
					SELECT
						-1
						,'Set Manually'
				END
				ELSE IF @vcListItemType = 'IC'  -- Item Category
				BEGIN
					INSERT INTO #temp (numID,vcData) 
					SELECT 							
							C1.numCategoryID,
							C1.vcCategoryName							
						FROM 
							Category C1 
						WHERE 
							numDomainID =@numDomainID				
					ORDER BY
						vcCategoryName
					
				END
				ELSE IF @vcDbColumnName = 'vcInventoryStatus'
				BEGIN
					INSERT INTO #temp
							SELECT 1,'Not Applicable' UNION
							SELECT 2,'Shipped' UNION
							SELECT 3,'Back Order' UNION
							SELECT 4,'Shippable' 
				END
				ELSE IF @vcDbColumnName='charSex'
					BEGIN
							INSERT INTO #temp
							SELECT 'M','Male' UNION
							SELECT 'F','Female'
						END
				ELSE IF @vcDbColumnName='tintOppStatus'
					BEGIN
							INSERT INTO #temp
							SELECT 1,'Deal Won' UNION
							SELECT 2,'Deal Lost'
					END
				ELSE IF @vcDbColumnName='tintOppType'
					BEGIN
							INSERT INTO #temp
							SELECT 1,'Sales Order' UNION
							SELECT 2,'Purchase Order' UNION
							SELECT 3,'Sales Opportunity' UNION
							SELECT 4,'Purchase Opportunity' 
					END
					ELSE IF @vcDbColumnName = 'vcWarehouse'
					BEGIN
						INSERT INTO #temp
						SELECT
							numWareHouseID,
							ISNULL(vcWareHouse,'')
						FROM
							Warehouses
						WHERE
							numDomainID=@numDomainID
					END
					ELSE IF @vcDbColumnName = 'vcLocation'
					BEGIN
						INSERT INTO #temp
						SELECT -1,'Global' UNION
						SELECT
							numWLocationID,
							ISNULL(vcLocation,'')
						FROM
							WarehouseLocation
						WHERE
							numDomainID=@numDomainID
				
					END
					ELSE IF @vcDbColumnName = 'tintPriceLevel'
					BEGIN
						DECLARE @Temp TABLE
						(
							Id INT
						)
						INSERT INTO @Temp
						(
							Id
						)
						SELECT DISTINCT 
							ROW_NUMBER() OVER(PARTITION BY pt.numItemCode ORDER BY numPricingID) Id
						FROM 
							[PricingTable] pt
						INNER JOIN 
							Item
						ON 
							Item.numItemCode = pt.numItemCode 
							AND Item.numDomainID = @numDomainID
						WHERE 
							tintRuleType = 3
							AND ISNULL(pt.numCurrencyID,0) = 0
						ORDER BY [Id] 

						INSERT INTO 
							#temp
						SELECT 
							pt.Id
							,ISNULL(NULLIF(pnt.vcPriceLevelName, ''), CONCAT('Price Level ',pt.Id)) Value
						FROM 
							@Temp pt
						LEFT JOIN
							PricingNamesTable pnt
						ON 
							pt.Id = pnt.tintPriceLevel
						AND 
							pnt.numDomainID = @numDomainID				
					END
--			Else                                                     
--				BEGIN                                                     
--					select @listName=vcData from ListDetails WHERE numListItemID=@numlistID
--				end 	

 				
        
		SELECT * FROM #temp 
		DROP TABLE #temp	
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
         
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetEstimateShipping')
DROP PROCEDURE USP_GetEstimateShipping
GO
CREATE PROCEDURE [dbo].[USP_GetEstimateShipping]
	@numDomainID NUMERIC(18,0)
	,@numShipVia INT
	,@numShipService INT
AS 
BEGIN
	SELECT 
		numServiceTypeID
		,0 AS numRuleID
		,numShippingCompanyID
		,vcServiceName
		,monRate
		,intNsoftEnum
		,intFrom
		,intTo
		,fltMarkup
		,bitMarkUpType
		,bitEnabled
		,1 AS tintFixedShippingQuotesMode
		,L.vcData AS vcShippingCompanyName
	FROM 
		ShippingServiceTypes S
	LEFT JOIN 
		ListDetails AS L
	ON
		S.numShippingCompanyID=L.numListItemID
	WHERE 
		S.numDomainID=@numDomainID 
		AND 1 = (CASE 
					WHEN ISNULL(@numShipVia,0) > 0 AND ISNULL(@numShipService,0) > 0 
					THEN (CASE WHEN ISNULL(S.numShippingCompanyID,0)=@numShipVia AND ISNULL(S.intNsoftEnum,0) = @numShipService THEN 1 ELSE 0 END)
					ELSE (CASE WHEN ISNULL(bitEnabled,0)=1 THEN 1 ELSE 0 END) 
				END)
END      




SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
-- USP_GetOpportunitySource 1
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetOpportunitySource')
DROP PROCEDURE USP_GetOpportunitySource 
GO
CREATE PROCEDURE [dbo].[USP_GetOpportunitySource]
(
	@numDomainID NUMERIC(18,0)
)
AS
BEGIN
	CREATE TABLE #temp 
	(
		numItemID VARCHAR(50)
		,vcItemName VARCHAR(100)
		,constFlag BIT
		,bitDelete BIT
		,intSortOrder INT
		,numListItemID NUMERIC(9)
		,vcData VARCHAR(100)
		,vcListType VARCHAR(1000)
		,vcOrderType VARCHAR(50)
		,vcOppOrOrder VARCHAR(20)
		,numListType NUMERIC(18,0)
		,tintOppOrOrder TINYINT
		,bitEnforceMinOrderAmount BIT
		,fltMinOrderAmount DECIMAL(20,5)
		,vcOriginalBizDocName VARCHAR(100)
		,numListItemGroupId NUMERIC(18,0)
		,vcListItemGroupName VARCHAR(100)
		,vcColorScheme VARCHAR(100)
		,tintSourceType TINYINT
	)

	INSERT INTO 
		#temp
	SELECT 
		'0~1','Internal Order',1 AS constFlag,0 AS bitDelete,0 intSortOrder,-1 AS numListItemID,'Internal Order','Internal Order','','',0,0,0,0,'',0,'','',1
	UNION
	SELECT 
		CAST(Ld.numListItemID  AS VARCHAR(250)) +'~1'
		,ISNULL(vcRenamedListName,vcData) AS vcData
		,Ld.constFlag,bitDelete
		,1 intSortOrder
		,CAST(Ld.numListItemID  AS VARCHAR(18))
		,ISNULL(vcRenamedListName,vcData) AS vcData
		,(SELECT vcData FROM ListDetails WHERE numListId=9 AND numListItemId = ld.numListType AND [ListDetails].[numDomainID] = @numDomainID)
		,(CASE when ld.numListType=1 then 'Sales' when ld.numListType=2 then 'Purchase' else 'All' END)
		,(CASE WHEN ld.tintOppOrOrder=1 THEN 'Opportunity' WHEN ld.tintOppOrOrder=2 THEN 'Order' ELSE 'All' END)
		,numListType
		,tintOppOrOrder
		,0
		,0
		,''
		,ISNULL(numListItemGroupId,0)
		,''
		,''
		,1
	FROM 
		ListDetails Ld  
	LEFT JOIN 
		ListOrder LO 
	ON 
		Ld.numListItemID=LO.numListItemID 
		AND Lo.numDomainId = @numDomainID 
	WHERE 
		Ld.numListID=9 AND (constFlag=1 OR Ld.numDomainID=@numDomainID)
	UNION
	SELECT 
		CONCAT(numSiteID,'~2')
		,[vcSiteName]
		,1 AS constFlag
		,0 AS bitDelete
		,2 intSortOrder
		,CAST([numSiteID] AS VARCHAR(18)) AS numListItemID
		,[vcSiteName]
		,[Sites].[vcSiteName]
		,''
		,''
		,0
		,0
		,0
		,0
		,''
		,0
		,''
		,''
		,2
	FROM
		Sites
	WHERE 
		[numDomainID] = @numDomainID
	UNION
	SELECT DISTINCT 
		CONCAT(WebApiId,'~3')
		,vcProviderName
		,1 constFlag
		,0 bitDelete
		,3 intSortOrder
		,CAST(WebApiId AS VARCHAR(18)) AS numListItemID
		,vcProviderName
		,[WebAPI].[vcProviderName]
		,''
		,''
		,0
		,0
		,0
		,0
		,''
		,0
		,''
		,''
		,3
	FROM 
		dbo.WebAPI
		 
	SELECT * FROM #temp ORDER BY intSortOrder
	
	DROP TABLE #temp		 	
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_InLineEditDataSave')
DROP PROCEDURE USP_InLineEditDataSave
GO
CREATE PROCEDURE [dbo].[USP_InLineEditDataSave] 
( 
 @numDomainID NUMERIC(18,0),
 @numUserCntID NUMERIC(18,0),
 @numFormFieldId NUMERIC(18,0),
 @bitCustom AS BIT = 0,
 @InlineEditValue VARCHAR(8000),
 @numContactID NUMERIC(18,0),
 @numDivisionID NUMERIC(18,0),
 @numWOId NUMERIC(18,0),
 @numProId NUMERIC(18,0),
 @numOppId NUMERIC(18,0),
 @numOppItemId NUMERIC(18,0),
 @numRecId NUMERIC(18,0),
 @vcPageName NVARCHAR(100),
 @numCaseId NUMERIC(18,0),
 @numWODetailId NUMERIC(18,0),
 @numCommID NUMERIC(18,0)
 )
AS 

declare @strSql as nvarchar(4000)
DECLARE @vcDbColumnName AS VARCHAR(100),@vcOrigDbColumnName AS VARCHAR(100),@vcLookBackTableName AS varchar(100)
DECLARE @vcListItemType as VARCHAR(3),@numListID AS numeric(9),@vcAssociatedControlType varchar(20)
declare @tempAssignedTo as numeric(9);set @tempAssignedTo=null           
DECLARE @Value1 AS VARCHAR(18),@Value2 AS VARCHAR(18)

IF  @bitCustom =0
BEGIN
	SELECT @vcDbColumnName=vcDbColumnName,@vcLookBackTableName=vcLookBackTableName,
		   @vcListItemType=vcListItemType,@numListID=numListID,@vcAssociatedControlType=vcAssociatedControlType,
		   @vcOrigDbColumnName=vcOrigDbColumnName
	 FROM DycFieldMaster WHERE numFieldId=@numFormFieldId
	 
	IF @vcAssociatedControlType = 'DateField' AND  @InlineEditValue=''
	BEGIN
		SET @InlineEditValue = null
	END

	DECLARE @intExecuteDiv INT=0
	if @vcLookBackTableName = 'DivisionMaster' 
	 BEGIN 
		
	   IF @vcDbColumnName='numFollowUpStatus' --Add to FollowUpHistory
	    Begin
			IF(@numCommID>0 AND @numDivisionID=0)
			BEGIN
				SET @numDivisionID=(SELECT TOP 1 numDivisionId FROM Communication WHERE numCommId=@numCommID)
			END
			declare @numFollow as varchar(10),@binAdded as varchar(20),@PreFollow as varchar(10),@RowCount as varchar(2)                            
			                      
			set @PreFollow=(select count(*)  numFollowUpStatus from FollowUpHistory where numDivisionID= @numDivisionID and numDomainId= @numDomainID)                          
			set @RowCount=@@rowcount  
			                        
			select   @numFollow=numFollowUpStatus, @binAdded=bintModifiedDate from divisionmaster where numDivisionID=@numDivisionID and numDomainId= @numDomainID                    
			
			if @numFollow <>'0' and @numFollow <> @InlineEditValue                          
			begin                          
				if @PreFollow<>0                          
					begin                          
			                   
						if @numFollow <> (select top 1  numFollowUpStatus from FollowUpHistory where numDivisionID= @numDivisionID and numDomainId= @numDomainID order by numFollowUpStatusID desc)                          
							begin                          
			                      	insert into FollowUpHistory(numFollowUpstatus,numDivisionID,bintAddedDate,numDomainId)                          
									values(@numFollow,@numDivisionID,@binAdded,@numDomainID)                          
							end                          
					end                          
				else                          
					begin                          
						insert into FollowUpHistory(numFollowUpstatus,numDivisionID,bintAddedDate,numDomainId)                          
						 values(@numFollow,@numDivisionID,@binAdded,@numDomainID)                          
					end                          
			   end 
			   
			
			UPDATE  DivisionMaster SET  dtLastFollowUp=getutcdate() where numDivisionID=@numDivisionID and numDomainID=@numDomainID
		END
		 IF @vcDbColumnName='numAssignedTo' ---Updating if organization is assigned to someone
			BEGIN
				select @tempAssignedTo=isnull(numAssignedTo,0) from DivisionMaster where numDivisionID = @numDivisionID and numDomainId= @numDomainID           
			
				if (@tempAssignedTo<>@InlineEditValue and  @InlineEditValue<>'0')          
					begin            
						update DivisionMaster set numAssignedTo=@InlineEditValue ,numAssignedBy=@numUserCntID where numDivisionID = @numDivisionID  and numDomainId= @numDomainID  
						UPDATE CompanyInfo SET bintModifiedDate=GETUTCDATE() WHERE numCompanyId IN (SELECT numCompanyId 
																									FROM 
																										DivisionMaster
																									WHERE 
																										numDivisionID = @numDivisionID  and numDomainId= @numDomainID)         
					end					
				else if  (@InlineEditValue ='0')          
					begin          
						update DivisionMaster set numAssignedTo=0 ,numAssignedBy=0 where numDivisionID = @numDivisionID  and numDomainId= @numDomainID
						UPDATE CompanyInfo SET bintModifiedDate=GETUTCDATE() WHERE numCompanyId IN (SELECT numCompanyId 
																									FROM 
																										DivisionMaster
																									WHERE 
																										numDivisionID = @numDivisionID  and numDomainId= @numDomainID)          
					end    
			END
	    UPDATE CompanyInfo SET numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() WHERE numCompanyId=(SELECT TOP 1 numCompanyId FROM DivisionMaster where numDivisionID=@numDivisionID and numDomainID=@numDomainID) and numDomainID=@numDomainID
		
		IF @vcDbColumnName='vcPartnerCode'
		BEGIN
			IF(@InlineEditValue<>'')
			BEGIN
				IF EXISTS(SELECT vcPartnerCode FROM DivisionMaster where numDivisionID<>@numDivisionID and numDomainID=@numDomainID AND vcPartnerCode=@InlineEditValue)
				BEGIN
					SET @intExecuteDiv=1
					SET @InlineEditValue='-#12'
				END
			END
		END
		IF @intExecuteDiv=0
		BEGIN
			SET @strSql='update DivisionMaster set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numDivisionID=@numDivisionID and numDomainID=@numDomainID'
			
		END
	 END 
	 ELSE if @vcLookBackTableName = 'AdditionalContactsInformation' 
	 BEGIN 
			IF @vcDbColumnName='bitPrimaryContact' AND @InlineEditValue='True'
			BEGIN
				DECLARE @bitPrimaryContactCheck AS BIT             
				DECLARE @numID AS NUMERIC(9) 
				
				SELECT TOP 1 @numID = numContactID,@bitPrimaryContactCheck = ISNULL(bitPrimaryContact,0)
					FROM AdditionalContactsInformation WHERE numDivisionID = @numDivisionID 
                   
					IF @@rowcount = 0 SET @numID = 0             
				
					WHILE @numID > 0            
					BEGIN            
						IF @bitPrimaryContactCheck = 1 
							UPDATE  AdditionalContactsInformation SET bitPrimaryContact = 0 WHERE numContactID = @numID  
                              
						SELECT TOP 1 @numID = numContactID,@bitPrimaryContactCheck = ISNULL(bitPrimaryContact,0)
						FROM AdditionalContactsInformation WHERE numDivisionID = @numDivisionID AND numContactID > @numID    
                                
						IF @@rowcount = 0 SET @numID = 0             
					END 
				END
				ELSE IF @vcDbColumnName='bitPrimaryContact' AND @InlineEditValue='False'
				BEGIN
					IF (SELECT COUNT(*) FROM AdditionalContactsInformation WHERE numDivisionID = @numDivisionID AND numContactId!=@numContactId AND ISNULL(bitPrimaryContact,0)=1) = 0
						SET @InlineEditValue='True'
				END 
				-- Added by Priya(to check no Campaign got engaged wothout contacts email
				ELSE IF @vcDbColumnName = 'numECampaignID' 
				BEGIN
					
					IF EXISTS (select 1 from AdditionalContactsInformation WHERE numContactId = @numContactID AND (vcEmail IS NULL OR vcEmail = ''))
					BEGIN
						RAISERROR('EMAIL_ADDRESS_REQUIRED',16,1)
								RETURN
					END

				END
				
		SET @strSql='update AdditionalContactsInformation set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numDivisionID=@numDivisionID and numDomainID=@numDomainID and numContactId=@numContactId'
	 END 
	 ELSE if @vcLookBackTableName = 'CompanyInfo' 
	 BEGIN 
		SET @strSql='update CompanyInfo set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numCompanyId=(select numCompanyId from divisionmaster where numDivisionID=@numDivisionID and numDomainID=@numDomainID) and numDomainID=@numDomainID'
	 END 
	 ELSE if @vcLookBackTableName = 'WorkOrder' 
	 BEGIN 
		DECLARE @numItemCode AS NUMERIC(9)
	 
		IF @numWODetailId>0 AND @vcDbColumnName <> 'numQtyItemsReq'
		BEGIN
			SET @strSql='update WorkOrderDetails set ' + @vcDbColumnName + '=@InlineEditValue where numWOId=@numWOId and numWODetailId=@numWODetailId' 
		END 	
		ELSE
		BEGIN
			SET @strSql='update WorkOrder set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numWOId=@numWOId and numDomainID=@numDomainID'
			
			IF @vcDbColumnName='numQtyItemsReq'
			BEGIN 
				DECLARE @numPickedQty FLOAT = 0
				
				SET @numPickedQty = ISNULL((SELECT
												MAX(numPickedQty)
											FROM
											(
												SELECT 
													WorkOrderDetails.numWODetailId
													,CEILING(SUM(WorkOrderPickedItems.numPickedQty)/numQtyItemsReq_Orig) numPickedQty
												FROM
													WorkOrderDetails
												INNER JOIN
													WorkOrderPickedItems
												ON
													WorkOrderDetails.numWODetailId=WorkOrderPickedItems.numWODetailId
												WHERE
													WorkOrderDetails.numWOId=@numWOID
													AND ISNULL(WorkOrderDetails.numWareHouseItemId,0) > 0
												GROUP BY
													WorkOrderDetails.numWODetailId
													,WorkOrderDetails.numQtyItemsReq_Orig
											) TEMP),0)

				IF @InlineEditValue < ISNULL((@numPickedQty),0)
				BEGIN
					RAISERROR('WORKORDR_QUANITY_CAN_NOT_BE_LESS_THEN_PICKED_QTY',16,1)
					RETURN
				END

				IF EXISTS (SELECT 
								SPDTTL.ID 
							FROM 
								StagePercentageDetailsTask SPDT
							INNER JOIN
								StagePercentageDetailsTaskTimeLog SPDTTL
							ON
								SPDT.numTaskId = SPDTTL.numTaskID 
							WHERE 
								SPDT.numDomainID=@numDomainID 
								AND SPDT.numWorkOrderId = @numWOId)
				BEGIN
					RAISERROR('TASKS_ARE_ALREADY_STARTED',16,1)
					RETURN
				END

				IF EXISTS (SELECT numWOId FROM WorkOrder WHERE numDomainID=@numDomainID AND numWOId=@numWOId AND numWOStatus=23184)
				BEGIN
					RAISERROR('WORKORDR_COMPLETED',16,1)
					RETURN
				END

				SELECT @numItemCode=numItemCode FROM WorkOrder WHERE numWOId=@numWOId and numDomainID=@numDomainID
			
				EXEC USP_UpdateWorkOrderDetailQuantity @numDomainID,@numUserCntID,@numItemCode,@InlineEditValue,@numWOId
			END 
		END	
	 END
	 ELSE if @vcLookBackTableName = 'ProjectsMaster' 
	 BEGIN 
		IF @vcDbColumnName='numAssignedTo' ---Updating if Project is assigned to someone 
			BEGIN
				select @tempAssignedTo=isnull(numAssignedTo,0) from ProjectsMaster where numProId=@numProId and numDomainId= @numDomainID           
			
				if (@tempAssignedTo<>@InlineEditValue and  @InlineEditValue<>'0')          
					begin            
						update ProjectsMaster set numAssignedTo=@InlineEditValue ,numAssignedBy=@numUserCntID,bintModifiedDate=GETUTCDATE() where numProId = @numProId  and numDomainId= @numDomainID           
					end           
				else if  (@InlineEditValue ='0')          
					begin          
						update ProjectsMaster set numAssignedTo=0 ,numAssignedBy=0,bintModifiedDate=GETUTCDATE() where numProId = @numProId  and numDomainId= @numDomainID        
					end    
			END
		else IF @vcDbColumnName='numProjectStatus'  ---Updating All Statge to 100% if Completed
			BEGIN
				IF @InlineEditValue='27492'
				BEGIN
					UPDATE PM SET dtCompletionDate=GETUTCDATE(),monTotalExpense=PIE.monExpense,
					monTotalIncome=PIE.monIncome,monTotalGrossProfit=PIE.monBalance,numProjectStatus=27492
					FROM ProjectsMaster PM,dbo.GetProjectIncomeExpense(@numDomainId,@numProId) PIE
					WHERE PM.numProId=@numProId
					
					EXEC USP_ManageProjectCommission @numDomainId,@numProId
						
					Update StagePercentageDetails set tinProgressPercentage=100,bitclose=1 where numProjectID=@numProId

					EXEC dbo.USP_GetProjectTotalProgress
							@numDomainID = @numDomainID, --  numeric(9, 0)
							@numProId = @numProId, --  numeric(9, 0)
							@tintMode = 1 --  tinyint
				END			
				ELSE
				BEGIN
					DELETE from BizDocComission WHERE numDomainID = @numDomainID AND numProId=@numProId AND numComissionID 
						NOT IN (SELECT BC.numComissionID FROM dbo.BizDocComission BC JOIN dbo.PayrollTracking PT 
							ON BC.numComissionID = PT.numComissionID AND BC.numDomainId = PT.numDomainId WHERE BC.numDomainID = @numDomainID AND ISNULL(BC.numProId,0) = @numProId)
				END
			END
			

		SET @strSql='update ProjectsMaster set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numProId=@numProId and numDomainID=@numDomainID'
	 END 
	 ELSE if @vcLookBackTableName = 'OpportunityMaster' 
	 BEGIN 
		IF @vcDbColumnName='numAssignedTo' ---Updating if organization is assigned to someone  
			BEGIN
				select @tempAssignedTo=isnull(numAssignedTo,0) from OpportunityMaster where numOppId=@numOppId and numDomainId= @numDomainID           
			
				if (@tempAssignedTo<>@InlineEditValue and  @InlineEditValue<>'0')          
					begin            
						update OpportunityMaster set numAssignedTo=@InlineEditValue ,numAssignedBy=@numUserCntID,bintModifiedDate=GETUTCDATE() where numOppId = @numOppId  and numDomainId= @numDomainID           
					end           
				else if  (@InlineEditValue ='0')          
					begin          
						update OpportunityMaster set numAssignedTo=0 ,numAssignedBy=0,bintModifiedDate=GETUTCDATE() where numOppId = @numOppId  and numDomainId= @numDomainID        
					end    
			END
		ELSE IF @vcDbColumnName='numPartner' OR @vcDbColumnName='numPartenerSource'
		 BEGIN
			update OpportunityMaster set numPartner=@InlineEditValue ,bintModifiedDate=GETUTCDATE() where numOppId = @numOppId  and numDomainId= @numDomainID           
		 END
		
		ELSE IF @vcDbColumnName='tintActive'
		 BEGIN
		 	SET @InlineEditValue= CASE @InlineEditValue WHEN 'True' THEN 1 ELSE 0 END 
		 END
		 ELSE IF @vcDbColumnName='tintSource'
		 BEGIN
			IF CHARINDEX('~', @InlineEditValue)>0
			BEGIN
				SET @Value1=SUBSTRING(@InlineEditValue, 1,CHARINDEX('~', @InlineEditValue)-1) 
				SET @Value2=SUBSTRING(@InlineEditValue, CHARINDEX('~', @InlineEditValue)+1,LEN(@InlineEditValue)) 
			END	
			ELSE
			BEGIN
				SET @Value1=@InlineEditValue
				SET @Value2=0
			END
			
		 	update OpportunityMaster set tintSourceType=@Value2 where numOppId = @numOppId  and numDomainId= @numDomainID        
		 	
		 	SET @InlineEditValue= @Value1
		 END
		ELSE IF @vcDbColumnName='tintOppStatus' --Update Inventory  0=Open,1=Won,2=Lost
		 BEGIN			
			DECLARE @bitViolatePrice BIT
			DECLARE @intPendingApprovePending INT
			DECLARE @tintCommitAllocation1 TINYINT
			SELECT @tintCommitAllocation1=ISNULL(tintCommitAllocation,1) FROM Domain WHERE numDomainId=@numDomainId

			IF EXISTS (SELECT numApprovalRuleID FROM ApprovalProcessItemsClassification WHERE numDomainId=@numDomainID AND ISNULL(bitMarginPriceViolated,0) = 1)
			BEGIN
				SET @bitViolatePrice = 1
			END
			SET @intPendingApprovePending=(SELECT COUNT(*) FROM OpportunityItems WHERE numOppId=@numOppId AND ISNULL(bitItemPriceApprovalRequired,0)=1)

			IF(@intPendingApprovePending > 0 AND @bitViolatePrice=1 AND @InlineEditValue='1')
			BEGIN
				SET @intExecuteDiv=1
				SET @InlineEditValue='-#123'
			END

			IF ISNULL(@intExecuteDiv,0)=0
			BEGIN
				DECLARE @tintOppStatus AS TINYINT
				SELECT @tintOppStatus=tintOppStatus FROM OpportunityMaster WHERE numOppID=@numOppID              

				IF (@tintOppStatus=0 OR @tintOppStatus=2) AND @InlineEditValue='1' --Open to Won
				BEGIN
					SELECT @numDivisionID=numDivisionID FROM OpportunityMaster WHERE numOppID=@numOppID   
					
					DECLARE @tintCRMType AS TINYINT      
					SELECT @tintCRMType=tintCRMType FROM divisionmaster WHERE numDivisionID =@numDivisionID 

					-- Promote Lead to Account when Sales/Purchase Order is created against it
					IF @tintCRMType=0 --Lead & Order
					BEGIN        
						UPDATE 
							divisionmaster 
						SET 
							tintCRMType=2
							,bintLeadProm=GETUTCDATE()
							,bintLeadPromBy=@numUserCntID
							,bintProsProm=GETUTCDATE()
							,bintProsPromBy=@numUserCntID        
						WHERE 
							numDivisionID=@numDivisionID        
					END
					--Promote Prospect to Account
					ELSE IF @tintCRMType=1 
					BEGIN        
						UPDATE 
							divisionmaster 
						SET 
							tintCRMType=2
							,bintProsProm=GETUTCDATE()
							,bintProsPromBy=@numUserCntID        
						WHERE 
							numDivisionID=@numDivisionID        
					END    

					IF @tintCommitAllocation1=1
					BEGIN
		 				EXEC USP_UpdatingInventoryonCloseDeal @numOppID,0,@numUserCntID
					END

					UPDATE OpportunityMaster SET bintOppToOrder=GETUTCDATE() WHERE numOppId=@numOppId and numDomainID=@numDomainID
					UPDATE OpportunityItems SET bitRequiredWarehouseCorrection=NULL WHERE numOppId=@numOppId
					EXEC USP_OpportunityItems_CheckWarehouseMapping @numDomainID,@numOppId
				END
				ELSE IF (@InlineEditValue='2') -- Win to Lost
				BEGIN
					UPDATE OpportunityMaster SET dtDealLost=GETUTCDATE() WHERE numOppId=@numOppId and numDomainID=@numDomainID
				END
				
				IF ((@tintOppStatus=1 and @InlineEditValue='2') or (@tintOppStatus=1 and @InlineEditValue='0')) AND @tintCommitAllocation1=1 --Won to Lost or Won to Open
						EXEC USP_RevertDetailsOpp @numOppID,0,0,@numUserCntID 

				UPDATE OpportunityMaster SET tintOppStatus = @InlineEditValue WHERE numOppId=@numOppID
				
			END
		 END
		 ELSE IF @vcDbColumnName='vcOppRefOrderNo' --Update vcRefOrderNo OpportunityBizDocs
		 BEGIN
			 IF LEN(ISNULL(@InlineEditValue,''))>0
			 BEGIN
	   			UPDATE dbo.OpportunityBizDocs SET vcRefOrderNo=@InlineEditValue WHERE numOppId=@numOppID
			 END
		 END
		 ELSE IF @vcDbColumnName='numStatus' 
		 BEGIN
			DECLARE @tintOppType AS TINYINT
			select @tintOppStatus=tintOppStatus,@tintOppType=tintOppType from OpportunityMaster where numOppID=@numOppID

			IF @tintOppType=1 AND @tintOppStatus=1
			BEGIN
				DECLARE @numOppStatus AS NUMERIC(18,0) = CAST(@InlineEditValue AS NUMERIC(18,0))
				EXEC USP_SalesFulfillmentQueue_Save @numDomainID,@numUserCntID,@numOppID,@numOppStatus
			END

		 	 IF EXISTS (SELECT 1 FROM dbo.OpportunityMaster WHERE numOppId = @numOppID AND tintOppType=1 AND isnull(tintOppStatus,0)=1 AND ISNULL(numStatus,0)!= @InlineEditValue)
			 BEGIN
	   				EXEC USP_ManageOpportunityAutomationQueue
							@numOppQueueID = 0, -- numeric(18, 0)
							@numDomainID = @numDomainID, -- numeric(18, 0)
							@numOppId = @numOppID, -- numeric(18, 0)
							@numOppBizDocsId = 0, -- numeric(18, 0)
							@numOrderStatus = @InlineEditValue, -- numeric(18, 0)
							@numUserCntID = @numUserCntID, -- numeric(18, 0)
							@tintProcessStatus = 1, -- tinyint
							@tintMode = 1 -- TINYINT
			END 
		 END	
		 ELSE IF @vcDbColumnName = 'dtReleaseDate'
		 BEGIN
			IF EXISTS (SELECT 1 FROM OpportunityMaster WHERE numDomainId=@numDomainID AND numOppId=@numOppId AND tintOppType=1 AND tintOppStatus=0)
			BEGIN
				IF (SELECT dbo.GetListIemName(ISNULL(numPercentageComplete,0)) FROM OpportunityMaster WHERE numDomainId=@numDomainID AND numOppId=@numOppId) = '100'
				BEGIN
					SET @strSql='update OpportunityMaster set ' + @vcDbColumnName + '=DATEADD(hh, DATEDIFF(hh, GETDATE(), GETUTCDATE()), @InlineEditValue),numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numOppId=@numOppId and numDomainID=@numDomainID'
				END
				ELSE
				BEGIN
					RAISERROR('PERCENT_COMPLETE_MUST_BE_100',16,1)
					RETURN
				END
			END
			ELSE
			BEGIN
				SET @strSql='update OpportunityMaster set ' + @vcDbColumnName + '=DATEADD(hh, DATEDIFF(hh, GETDATE(), GETUTCDATE()), @InlineEditValue),numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numOppId=@numOppId and numDomainID=@numDomainID'
			END
		END
		ELSE IF @vcDbColumnName = 'numPercentageComplete'
		BEGIN
			IF EXISTS (SELECT 1 FROM OpportunityMaster WHERE numDomainId=@numDomainID AND numOppId=@numOppId AND tintOppType=1 AND tintOppStatus=0 AND dbo.GetListIemName(ISNULL(numPercentageComplete,0))='100')
			BEGIN
				IF EXISTS(SELECT dtReleaseDate FROM OpportunityMaster WHERE numDomainId=@numDomainID AND numOppId=@numOppId AND dtReleaseDate IS NULL)
				BEGIN
					SET @strSql='update OpportunityMaster set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numOppId=@numOppId and numDomainID=@numDomainID'
				END
				ELSE
				BEGIN
					RAISERROR('REMOVE_RELEASE_DATE',16,1)
					RETURN
				END
			END
			ELSE
			BEGIN
				SET @strSql='update OpportunityMaster set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numOppId=@numOppId and numDomainID=@numDomainID'
			END
		END
		ELSE IF @vcDbColumnName = 'numProjectID'
		BEGIN
			UPDATE OpportunityItems SET numProjectID=@InlineEditValue WHERE numOppId=@numOppId
		END
		ELSE IF @vcDbColumnName = 'intUsedShippingCompany'
		BEGIN
			UPDATE OpportunityMaster SET numShippingService=0 WHERE numOppId=@numOppId
		END

		IF(@vcDbColumnName!='tintOppStatus')
		BEGIN
			SET @strSql='update OpportunityMaster set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numOppId=@numOppId and numDomainID=@numDomainID'
		END
	 END
	 ELSE if @vcLookBackTableName = 'Cases' 
	 BEGIN 
	 		IF @vcDbColumnName='numAssignedTo' ---Updating if Case is assigned to someone  
			BEGIN
				select @tempAssignedTo=isnull(numAssignedTo,0) from Cases where numCaseId=@numCaseId and numDomainId= @numDomainID           
			
				if (@tempAssignedTo<>@InlineEditValue and  @InlineEditValue<>'0')          
					begin            
						update Cases set numAssignedTo=@InlineEditValue ,numAssignedBy=@numUserCntID,bintModifiedDate=GETUTCDATE() where numCaseId = @numCaseId  and numDomainId= @numDomainID           
					end           
				else if  (@InlineEditValue ='0')          
					begin          
						update Cases set numAssignedTo=0 ,numAssignedBy=0,bintModifiedDate=GETUTCDATE() where numCaseId = @numCaseId  and numDomainId= @numDomainID        
					end    
			END
		UPDATE Cases SET bintModifiedDate=GETUTCDATE() WHERE numCaseId = @numCaseId  and numDomainId= @numDomainID    
		SET @strSql='update Cases set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numCaseId=@numCaseId and numDomainID=@numDomainID'
	 END 
	 ELSE if @vcLookBackTableName = 'Tickler' 
	 BEGIN 	
		IF(@vcOrigDbColumnName = 'textDetails' AND EXISTS(SELECT * FROM Communication WHERE numCommId=@numCommID and numDomainID=@numDomainID))
		BEGIN
			SET @strSql='update Communication set ' + @vcOrigDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,dtModifiedDate=getutcdate() where numCommId=@numCommId and numDomainID=@numDomainID'
		END
		ELSE IF(@vcOrigDbColumnName = 'textDetails' AND EXISTS(SELECT * FROM Activity WHERE ActivityID=@numCommID))
		BEGIN
			SET @strSql='update Activity set Comments=@InlineEditValue where ActivityID=@numCommId'
		END
		ELSE
		BEGIN
			SET @strSql='update Communication set ' + @vcOrigDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,dtModifiedDate=getutcdate() where numCommId=@numCommId and numDomainID=@numDomainID'
		END
	 END
	 ELSE IF @vcLookBackTableName = 'ExtarnetAccounts'
	 BEGIN
		IF @vcDbColumnName='bitEcommerceAccess'
		BEGIN
			IF LOWER(@InlineEditValue) = 'yes' OR LOWER(@InlineEditValue) = 'true'
			BEGIN
				IF NOT EXISTS (SELECT * FROM ExtarnetAccounts where numDivisionID=@numDivisionID) 
				BEGIN
					DECLARE @numGroupID NUMERIC
					DECLARE @numCompanyID NUMERIC(18,0)
					SELECT @numCompanyID=numCompanyID FROM DivisionMaster WHERE numDivisionID=@numDivisionID

					SELECT TOP 1 @numGroupID=numGroupID FROM dbo.AuthenticationGroupMaster WHERE numDomainID=@numDomainID AND tintGroupType=2
				
					IF @numGroupID IS NULL SET @numGroupID = 0 
					INSERT INTO ExtarnetAccounts (numCompanyID,numDivisionID,numGroupID,numDomainID)                                                
					VALUES(@numCompanyID,@numDivisionID,@numGroupID,@numDomainID)
				END
			END
			ELSE
			BEGIN
				DELETE FROM ExtranetAccountsDtl WHERE numExtranetID IN (SELECT ISNULL(numExtranetID,0) FROM ExtarnetAccounts where numDivisionID=@numDivisionID)
				DELETE FROM ExtarnetAccounts where numDivisionID=@numDivisionID
			END

			SELECT (CASE WHEN LOWER(@InlineEditValue) = 'yes' OR LOWER(@InlineEditValue) = 'true' THEN 'Yes' ELSE 'No' END) AS vcData

			RETURN
		END
	 END
	ELSE IF @vcLookBackTableName = 'ExtranetAccountsDtl'
	BEGIN
		IF @vcDbColumnName='vcPassword'
		BEGIN
			IF LEN(@InlineEditValue) = 0
			BEGIN
				DELETE FROM ExtranetAccountsDtl WHERE numContactID=@numContactID
			END
			ELSE
			BEGIN
				IF EXISTS (SELECT numExtranetID FROM ExtarnetAccounts WHERE numDivisionID = @numDivisionID)
				BEGIN
					IF EXISTS (SELECT numExtranetDtlID FROM ExtranetAccountsDtl WHERE numContactID=@numContactID)
					BEGIN
						UPDATE
							ExtranetAccountsDtl
						SET 
							vcPassword=@InlineEditValue
							,tintAccessAllowed=1
						WHERE
							numContactID=@numContactID
					END
					ELSE
					BEGIN
						DECLARE @numExtranetID NUMERIC(18,0)
						SELECT @numExtranetID=numExtranetID FROM ExtarnetAccounts WHERE numDivisionID = @numDivisionID

						INSERT INTO ExtranetAccountsDtl (numExtranetID,numContactID,vcPassword,tintAccessAllowed,numDomainID)
						VALUES (@numExtranetID,@numContactID,@InlineEditValue,1,@numDomainID)
					END
				END
				ELSE
				BEGIN
					RAISERROR('Organization e-commerce access is not enabled',16,1)
				END
			END

			SELECT @InlineEditValue AS vcData

			RETURN
		END
	END
	ELSE IF @vcLookBackTableName = 'CustomerPartNumber'
	BEGIN
		IF @vcDbColumnName='CustomerPartNo'
		BEGIN
			SELECT @numCompanyID=numCompanyID FROM DivisionMaster WHERE numDivisionID=@numDivisionID
			
			IF EXISTS(SELECT CustomerPartNoID FROM CustomerPartNumber WHERE numItemCode = @numRecId AND numCompanyID = @numCompanyID)
			BEGIN
				UPDATE CustomerPartNumber SET CustomerPartNo = @InlineEditValue WHERE numItemCode = @numRecId AND numCompanyId = @numCompanyID AND numDomainID = @numDomainId 
			END
			ELSE
			BEGIN
				INSERT INTO CustomerPartNumber
				(numItemCode, numCompanyId, numDomainID, CustomerPartNo)
				VALUES
				(@numRecId, @numCompanyID, @numDomainID, @InlineEditValue)
			END

			SELECT @InlineEditValue AS vcData

			RETURN
		END
		
	END
	ELSE IF @vcLookBackTableName = 'CheckHeader'
	BEGIN
		IF EXISTS (SELECT 1 FROM CheckHeader WHERE numDomainID=@numDomainID AND numCheckNo=@InlineEditValue AND numCheckHeaderID <> @numRecId)
		BEGIN
			RAISERROR('DUPLICATE_CHECK_NUMBER',16,1)
			RETURN
		END

		SET @strSql='UPDATE CheckHeader SET ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,dtModifiedDate=getutcdate() WHERE numCheckHeaderID=@numRecId and numDomainID=@numDomainID'
	END
	ELSE IF @vcLookBackTableName = 'Item'
	BEGIN
		IF @vcDbColumnName = 'monListPrice'
		BEGIN
			SET @InlineEditValue = REPLACE(@InlineEditValue,',','')

			IF EXISTS (SELECT numItemCode FROM Item WHERE numDomainID=@numDomainID AND numItemCode=@numRecId AND 1 = (CASE WHEN ISNULL(numItemGroup,0) > 0 THEN (CASE WHEN ISNULL(bitMatrix,0)=1 THEN 1 ELSE 0 END) ELSE 1 END))
			BEGIN
				UPDATE WareHouseItems SET monWListPrice=@InlineEditValue WHERE numDomainID=@numDomainID AND numItemID=@numRecId
				SELECT @InlineEditValue AS vcData
			END
			ELSE
			BEGIN
				RAISERROR('You are not allowed to edit list price for warehouse level matrix item.',16,1)
			END

			RETURN
		END

		SET @strSql='UPDATE Item set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numItemCode=@numRecId AND numDomainID=@numDomainID'
	END
	ELSE IF @vcLookBackTableName = 'Warehouses'
	BEGIN
		IF EXISTS (SELECT numOppId FROM OpportunityMaster WHERE numDomainId=@numDomainID AND numOppID=@numOppId AND ISNULL(tintOppStatus,0)=1 AND ISNULL(tintshipped,0)=0)
		BEGIN
			IF EXISTS (SELECT numWareHouseID FROM Warehouses WHERE numDomainID=@numDomainID AND numWareHouseID=@InlineEditValue)
			BEGIN
				DECLARE @numTempOppItemID NUMERIC(18,0)
				DECLARE @numTempItemCode NUMERIC(18,0)
				DECLARE @numOldWarehouseItemID NUMERIC(18,0)
				DECLARE @numNewWarehouseItemID NUMERIC(18,0)
				DECLARE @tintCommitAllocation TINYINT 

				SELECT @tintCommitAllocation=ISNULL(tintCommitAllocation,1) FROM Domain WHERE numDomainId=@numDomainID


				DECLARE @TEMPItems TABLE
				(
					ID INT IDENTITY(1,1)
					,numOppItemID NUMERIC(18,0)
					,numItemCode NUMERIC(18,0)
					,numWarehouseItemID NUMERIC(18,0)
				)
				INSERT INTO @TEMPItems
				(
					numOppItemID
					,numItemCode
					,numWarehouseItemID
				)
				SELECT
					numoppitemtCode
					,numItemCode
					,numWarehouseItmsID
				FROM 
					OpportunityItems
				WHERE
					numOppID=@numOppID
					AND (numOppItemtcode=@numOppItemId OR ISNULL(@numOppItemId,0) = 0)

				IF EXISTS (SELECT TI.numItemCode FROM @TEMPItems TI LEFT JOIN WareHouseItems WI ON TI.numItemCode=WI.numItemID AND WI.numWareHouseID=@InlineEditValue WHERE WI.numWareHouseItemID IS NULL)
				BEGIN
					RAISERROR('WAREHOUSE_DOES_NOT_EXISTS_IN_ITEM',16,1)
				END
				ELSE
				BEGIN
					DECLARE @j INT = 1
					DECLARE @jCount INT 

					SELECT @jCount=COUNT(*) FROM @TEMPItems

					WHILE @j <= @jCount
					BEGIN
						SELECT 
							@numTempOppItemID = numOppItemID
							,@numTempItemCode = numItemCode
							,@numOldWarehouseItemID = numWarehouseItemID
						FROM 
							@TEMPItems TI 
						WHERE 
							ID=@j

						SELECT TOP 1
							@numNewWarehouseItemID = numWareHouseItemID
						FROM
							WareHouseItems 
						WHERE 
							numDomainID=@numDomainID 
							AND numItemID=@numTempItemCode 
							AND numWareHouseID=@InlineEditValue
						ORDER BY
							ISNULL(numWLocationID,0) ASC

						IF @numOldWarehouseItemID <> @numNewWarehouseItemID
						BEGIN
							BEGIN TRY
							BEGIN TRANSACTION 
								--REVERTING BACK THE WAREHOUSE ITEMS                  
								IF @tintCommitAllocation=1
								BEGIN               
									EXEC USP_RevertDetailsOpp @numOppID,@numTempOppItemID,0,@numUserCntID                  
								END  

								UPDATE OpportunityItems SET numWarehouseItmsID=@numNewWarehouseItemID WHERE numoppitemtCode=@numTempOppItemID

								IF @tintCommitAllocation=1
								BEGIN               
									EXEC USP_UpdatingInventoryonCloseDeal @numOppID,@numTempOppItemID,@numUserCntID
								END  

								UPDATE OpportunityItems SET bitRequiredWarehouseCorrection=0 WHERE numoppitemtCode=@numTempOppItemID
							COMMIT
							END TRY
							BEGIN CATCH
								IF @@TRANCOUNT > 0
									ROLLBACK

								-- Raise an error with the details of the exception
								DECLARE @ErrMsg nvarchar(4000), @ErrSeverity int
								SELECT @ErrMsg = ERROR_MESSAGE(), @ErrSeverity = ERROR_SEVERITY()

								RAISERROR(@ErrMsg, @ErrSeverity, 1)
							END CATCH
						END

						SET @j = @j + 1
					END
				END
			END
			ELSE
			BEGIN
				RAISERROR('WAREHOUSE_DOES_NOT_EXISTS',16,1)
			END
		END
	END

	EXECUTE sp_executeSQL @strSql, N'@numDomainID NUMERIC(18,0),@numUserCntID NUMERIC(18,0),@numDivisionID NUMERIC(18,0),@numContactID NUMERIC(18,0),@InlineEditValue VARCHAR(8000),@numWOId NUMERIC(18,0),@numProId NUMERIC(18,0),@numOppId NUMERIC(18,0),@numOppItemId NUMERIC(18,0),@numCaseId NUMERIC(18,0),@numWODetailId NUMERIC(18,0),@numCommID Numeric(18,0),@numRecId NUMERIC(18,0)',
		     @numDomainID,@numUserCntID,@numDivisionID,@numContactID,@InlineEditValue,@numWOId,@numProId,@numOppId,@numOppItemId,@numCaseId,@numWODetailId,@numCommID,@numRecId;
		     
	IF @vcAssociatedControlType='SelectBox'                                                    
       BEGIN       
			IF @vcDbColumnName='tintSource' AND @vcLookBackTableName = 'OpportunityMaster'
			BEGIN
				SELECT dbo.fn_GetOpportunitySourceValue(@Value1,@Value2,@numDomainID) AS vcData                                             
			END
			ELSE IF @vcDbColumnName='numContactID'
			BEGIN
				SELECT dbo.fn_GetContactName(@InlineEditValue)
			END
			ELSE IF @vcDbColumnName='numPartner' OR @vcDbColumnName='numPartenerSource'
			BEGIN
				SET @intExecuteDiv=1
				SELECT D.vcPartnerCode+'-'+C.vcCompanyName  AS vcData FROM DivisionMaster AS D 
				LEFT JOIN CompanyInfo AS C
				ON D.numCompanyID=C.numCompanyID WHERE D.numDivisionID=@InlineEditValue
			END
			ELSE IF @vcDbColumnName='tintOppStatus' AND @intExecuteDiv=1
			BEGIN
				SELECT @InlineEditValue AS vcData
			END
			ELSE IF @vcDbColumnName='numReleaseStatus'
			BEGIN
				If @InlineEditValue = '1'
				BEGIN
					SELECT 'Open'
				END
				ELSE IF @InlineEditValue = '2'
				BEGIN
					SELECT 'Purchased'
				END
				ELSE
				BEGIN
					SELECT ''
				END
			END
			ELSE IF @vcDbColumnName='numPartenerContact'
			BEGIN
				SET @InlineEditValue=(SELECT TOP 1 A.vcGivenName FROM AdditionalContactsInformation AS A 
										LEFT JOIN DivisionMaster AS D 
										ON D.numDivisionID=A.numDivisionId
										WHERE D.numDomainID=@numDomainID AND A.numCOntactId=@InlineEditValue )
				SELECT @InlineEditValue AS vcData
			END
			ELSE IF @vcDbColumnName='numPartenerSource'
			BEGIN
				
				SET @InlineEditValue=(SELECT D.vcPartnerCode+'-'+C.vcCompanyName  AS vcData FROM DivisionMaster AS D 
				LEFT JOIN CompanyInfo AS C
				ON D.numCompanyID=C.numCompanyID WHERE D.numDivisionID=@InlineEditValue)

				SELECT @InlineEditValue AS vcData
			END
			ELSE IF @vcDbColumnName='numShippingService'
			BEGIN
				SELECT ISNULL((SELECT vcShipmentService FROM ShippingService WHERE (numDomainID=@numDomainID OR ISNULL(numDomainID,0)=0) AND numShippingServiceID = @InlineEditValue),'') AS vcData
			END
			ELSE IF @vcDbColumnName='intDropShip'
			BEGIN
				 SELECT CASE WHEN ISNULL(@InlineEditValue,0)=1 THEN 'Not Available'
						 WHEN ISNULL(@InlineEditValue,0)=2 THEN 'Blank Available'
						 WHEN ISNULL(@InlineEditValue,0)=3 THEN 'Vendor Label'
						 WHEN ISNULL(@InlineEditValue,0)=4 THEN 'Private Label'
						 ELSE 'Select One'
					END AS vcData
			END
			else
			BEGIN
				SELECT dbo.fn_getSelectBoxValue(@vcListItemType,@InlineEditValue,@vcDbColumnName) AS vcData                                             
			END
		end 
		ELSE
		BEGIN
			IF @vcDbColumnName='tintActive'
		  	BEGIN
				SET @InlineEditValue= CASE @InlineEditValue WHEN 1 THEN 'True' ELSE 'False' END 
			END
			ELSE IF @vcDbColumnName='vcWareHouse' AND ISNUMERIC(@InlineEditValue) = 1
			BEGIN
				SELECT @InlineEditValue=vcWareHouse FROM Warehouses WHERE numDomainID=@numDomainID AND numWareHouseID=@InlineEditValue
			END

			IF @vcAssociatedControlType = 'Check box' or @vcAssociatedControlType = 'Checkbox'
			BEGIN
	 			SET @InlineEditValue= Case @InlineEditValue WHEN 'True' THEN 'Yes' ELSE 'No' END
			END
			
		    SELECT @InlineEditValue AS vcData
		END  
		
	IF @vcLookBackTableName = 'AdditionalContactsInformation' AND @vcDbColumnName = 'numECampaignID'
	BEGIN
		--Manage ECampiagn When Campaign is changed from inline edit
		DECLARE @Date AS DATETIME 
		SELECT @Date = dbo.[GetUTCDateWithoutTime]()
		DECLARE @numECampaignID AS NUMERIC(18,0)
		SET @numECampaignID = @InlineEditValue

		IF @numECampaignID = -1 
		SET @numECampaignID=0
	 
 		EXEC [USP_ManageConEmailCampaign]
		@numConEmailCampID = 0, --  numeric(9, 0)
		@numContactID = @numContactID, --  numeric(9, 0)
		@numECampaignID = @numECampaignID, --  numeric(9, 0)
		@intStartDate = @Date, --  datetime
		@bitEngaged = 1, --  bit
		@numUserCntID = @numUserCntID --  numeric(9, 0)
	END  
END
ELSE
BEGIN	 

	SET @InlineEditValue=Cast(@InlineEditValue as varchar(1000))
	SELECT @vcAssociatedControlType=Fld_type
	 FROM CFW_Fld_Master WHERE Fld_Id=@numFormFieldId 

	IF @vcAssociatedControlType = 'CheckBox'
	BEGIN
	    SET @InlineEditValue=(case when @InlineEditValue='False' then 0 ELSE 1 END)
	END

	IF @vcPageName='organization'
	BEGIN 
		IF (SELECT COUNT(*) FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)>0
			BEGIN
				UPDATE CFW_FLD_Values SET Fld_Value=@InlineEditValue WHERE FldDTLID=(SELECT CFW.FldDTLID FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)
			END	
		ELSE
			BEGIN
				insert into CFW_FLD_Values (Fld_ID,Fld_Value,RecId) VALUES (@numFormFieldId,@InlineEditValue,@numRecId)         
			END

			--Added By :Sachin Sadhu ||Date:13thOct2014
			--Purpose  :To manage CustomFields in Queue
			EXEC USP_CFW_FLD_Values_CT
					@numDomainID =@numDomainID,
					@numUserCntID =@numUserCntID,
					@numRecordID =@numRecId ,
					@numFormFieldId  =@numFormFieldId
  
	  END
	ELSE IF @vcPageName='contact'
	BEGIN 
		IF (SELECT COUNT(*) FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Cont CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)>0
			BEGIN
				UPDATE CFW_FLD_Values_Cont SET Fld_Value=@InlineEditValue WHERE FldDTLID=(SELECT CFW.FldDTLID FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Cont CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)
			END	
		ELSE
			BEGIN
				insert into CFW_FLD_Values_Cont (Fld_ID,Fld_Value,RecId) VALUES (@numFormFieldId,@InlineEditValue,@numRecId)         
			END
			
			--Added By :Sachin Sadhu ||Date:13thOct2014
			--Purpose  :To manage CustomFields in Queue
			EXEC USP_CFW_FLD_Values_Cont_CT
					@numDomainID =@numDomainID,
					@numUserCntID =@numUserCntID,
					@numRecordID =@numRecId ,
					@numFormFieldId  =@numFormFieldId

	END
	ELSE IF @vcPageName='project'
	BEGIN 
		IF (SELECT COUNT(*) FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Pro CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)>0
			BEGIN
				UPDATE CFW_FLD_Values_Pro SET Fld_Value=@InlineEditValue WHERE FldDTLID=(SELECT CFW.FldDTLID FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Pro CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)
			END	
		ELSE
			BEGIN
				insert into CFW_FLD_Values_Pro (Fld_ID,Fld_Value,RecId) VALUES (@numFormFieldId,@InlineEditValue,@numRecId)         
			END

			
			--Added By :Sachin Sadhu ||Date:14thOct2014
			--Purpose  :To manage CustomFields in Queue
			EXEC USP_CFW_FLD_Values_Pro_CT
					@numDomainID =@numDomainID,
					@numUserCntID =@numUserCntID,
					@numRecordID =@numRecId ,
					@numFormFieldId  =@numFormFieldId
	END
	ELSE IF @vcPageName='opp'
	BEGIN 
		DECLARE @dtDate AS DATETIME = GETUTCDATE()

		IF (SELECT COUNT(*) FROM CFW_Fld_Master CFM JOIN CFW_Fld_Values_Opp CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)>0
		BEGIN
			UPDATE 
				CFW_Fld_Values_Opp 
			SET 
				Fld_Value=@InlineEditValue ,
				numModifiedBy = @numUserCntID,
				bintModifiedDate = @dtDate
			WHERE 
				FldDTLID= (SELECT CFW.FldDTLID FROM CFW_Fld_Master CFM JOIN CFW_Fld_Values_Opp CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)
		END	
		ELSE
		BEGIN
			INSERT INTO CFW_Fld_Values_Opp (Fld_ID,Fld_Value,RecId,numModifiedBy,bintModifiedDate) VALUES (@numFormFieldId,@InlineEditValue,@numRecId,@numUserCntID,@dtDate)         
		END

		UPDATE OpportunityMaster SET numModifiedBy=@numUserCntID,bintModifiedDate=@dtDate WHERE numOppID = @numRecId 

		--Added By :Sachin Sadhu ||Date:14thOct2014
		--Purpose  :To manage CustomFields in Queue
		EXEC USP_CFW_Fld_Values_Opp_CT
				@numDomainID =@numDomainID,
				@numUserCntID =@numUserCntID,
				@numRecordID =@numRecId ,
				@numFormFieldId  =@numFormFieldId

	END
	ELSE IF @vcPageName='oppitems'
	BEGIN 
		IF (SELECT COUNT(*) FROM CFW_Fld_Master CFM JOIN CFW_Fld_Values_OppItems CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)>0
		BEGIN
			UPDATE 
				CFW_Fld_Values_OppItems 
			SET 
				Fld_Value=@InlineEditValue
			WHERE 
				FldDTLID= (SELECT CFW.FldDTLID FROM CFW_Fld_Master CFM JOIN CFW_Fld_Values_OppItems CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)
		END	
		ELSE
		BEGIN
			INSERT INTO CFW_Fld_Values_OppItems(Fld_ID,Fld_Value,RecId) VALUES (@numFormFieldId,@InlineEditValue,@numRecId)         
		END
	END
	ELSE IF @vcPageName='case'
	BEGIN 
		IF (SELECT COUNT(*) FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Case CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)>0
			BEGIN
				UPDATE CFW_FLD_Values_Case SET Fld_Value=@InlineEditValue WHERE FldDTLID=(SELECT CFW.FldDTLID FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Case CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)
			END	
		ELSE
			BEGIN
				insert into CFW_FLD_Values_Case (Fld_ID,Fld_Value,RecId) VALUES (@numFormFieldId,@InlineEditValue,@numRecId)         
			END

			
			--Added By :Sachin Sadhu ||Date:14thOct2014
			--Purpose  :To manage CustomFields in Queue
			EXEC USP_CFW_FLD_Values_Case_CT
					@numDomainID =@numDomainID,
					@numUserCntID =@numUserCntID,
					@numRecordID =@numRecId ,
					@numFormFieldId  =@numFormFieldId
	END
	ELSE IF @vcPageName='item'
	BEGIN
		IF (SELECT COUNT(*) FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Item CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)>0
		BEGIN
			UPDATE CFW_FLD_Values_Item SET Fld_Value=@InlineEditValue WHERE FldDTLID=(SELECT CFW.FldDTLID FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Item CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)
		END	
		ELSE
		BEGIN
			INSERT INTO CFW_FLD_Values_Item (Fld_ID,Fld_Value,RecId) VALUES (@numFormFieldId,@InlineEditValue,@numRecId)         
		END
	END


	IF @vcAssociatedControlType = 'SelectBox'            
	BEGIN            
		    
		 SELECT vcData FROM ListDetails WHERE numListItemID=@InlineEditValue            
	END
	ELSE IF @vcAssociatedControlType = 'CheckBoxList'
	BEGIN
		DECLARE @str AS VARCHAR(MAX)
		SET @str = 'SELECT STUFF((SELECT CONCAT('','', vcData) FROM ListDetails WHERE numlistitemid IN (' + (CASE WHEN LEN(@InlineEditValue) > 0 THEN @InlineEditValue ELSE '0' END) + ') FOR XML PATH('''')), 1, 1, '''')'
		EXEC(@str)
	END
	ELSE IF @vcAssociatedControlType = 'CheckBox'
	BEGIN
	 	select CASE @InlineEditValue WHEN 1 THEN 'Yes' ELSE 'No' END AS vcData
	END
	ELSE
	BEGIN
		SELECT @InlineEditValue AS vcData
	END 
END

 
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_MassSalesFulfillment_GetConfiguration')
DROP PROCEDURE dbo.USP_MassSalesFulfillment_GetConfiguration
GO

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_MassSalesFulfillment_GetConfiguration]
(
	@numDomainID NUMERIC(18,0)
)
AS
BEGIN
	DECLARE @numDefaultSalesShippingDoc NUMERIC(18,0)

	SELECT @numDefaultSalesShippingDoc=ISNULL(numDefaultSalesShippingDoc,0) FROM Domain WHERE numDomainId=@numDomainID

	IF EXISTS (SELECT ID FROM MassSalesFulfillmentConfiguration WHERE numDomainID=@numDomainID)
	BEGIN
		SELECT 
			ISNULL(bitGroupByOrderForPick,0) bitGroupByOrderForPick
			,ISNULL(bitGroupByOrderForShip,0) bitGroupByOrderForShip
			,ISNULL(bitGroupByOrderForInvoice,0) bitGroupByOrderForInvoice
			,ISNULL(bitGroupByOrderForPay,0) bitGroupByOrderForPay
			,ISNULL(bitGroupByOrderForClose,0) bitGroupByOrderForClose
			,ISNULL(tintInvoicingType,1) tintInvoicingType
			,ISNULL(tintScanValue,1) tintScanValue
			,ISNULL(tintPackingMode,1) tintPackingMode
			,@numDefaultSalesShippingDoc numDefaultSalesShippingDoc
			,ISNULL(tintPendingCloseFilter,1) tintPendingCloseFilter
			,ISNULL(bitGeneratePickListByOrder,0) bitGeneratePickListByOrder
			,ISNULL(bitPickWithoutInventoryCheck,0) bitPickWithoutInventoryCheck
			,ISNULL(bitEnablePickListMapping,0) bitEnablePickListMapping
			,ISNULL(bitEnableFulfillmentBizDocMapping,0) bitEnableFulfillmentBizDocMapping
		FROM	
			MassSalesFulfillmentConfiguration
		WHERE
			numDomainID=@numDomainID
	END
	ELSE 
	BEGIN
		SELECT 
			0 bitGroupByOrderForPick
			,0 bitGroupByOrderForShip
			,0 bitGroupByOrderForInvoice
			,0 bitGroupByOrderForPay
			,0 bitGroupByOrderForClose
			,2 tintInvoicingType
			,1 tintScanValue
			,1 tintPackingMode
			,@numDefaultSalesShippingDoc numDefaultSalesShippingDoc
			,1 tintPendingCloseFilter
			,0 bitGeneratePickListByOrder
			,0 bitPickWithoutInventoryCheck
			,0 bitEnablePickListMapping
			,0 bitEnableFulfillmentBizDocMapping
	END
END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_MassSalesFulfillment_GetOrderItems')
DROP PROCEDURE dbo.USP_MassSalesFulfillment_GetOrderItems
GO

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_MassSalesFulfillment_GetOrderItems]
(
	@numDomainID NUMERIC(18,0)
	,@numUserCntID NUMERIC(18,0)
	,@numOppID NUMERIC(18,0)
	,@tintMode TINYINT
	,@vcSelectedRecords VARCHAR(MAX)
)
AS 
BEGIN
	DECLARE @tintPackingMode TINYINT
	DECLARE @tintInvoicingType TINYINT
	DECLARE @numDefaultShippingBizDoc NUMERIC(18,0)

	SELECT
		@numDefaultShippingBizDoc = ISNULL(numDefaultSalesShippingDoc,0)
	FROM
		Domain
	WHERE
		numDomainID=@numDomainID

	SELECT
		@tintPackingMode=tintPackingMode
		,@tintInvoicingType=tintInvoicingType
	FROM
		MassSalesFulfillmentConfiguration
	WHERE
		MassSalesFulfillmentConfiguration.numDomainID = @numDomainID

	IF ISNULL(@vcSelectedRecords,'') <> ''
	BEGIN
		DECLARE @TempItems TABLE
		(	
			numOppItemID NUMERIC(18,0),
			numUnitHour FLOAT
		)

		INSERT INTO @TempItems
		(
			numOppItemID,
			numUnitHour
		)
		SELECT
			CAST(SUBSTRING(OutParam,0,CHARINDEX('-',OutParam)) AS NUMERIC(18,0))
			,CAST(SUBSTRING(OutParam,CHARINDEX('-',OutParam) + 1,LEN(OutParam)) AS FLOAT)
		FROM
			dbo.SplitString(@vcSelectedRecords,',')

		IF @tintMode = 1 -- For ship orders
		BEGIN
			SELECT
				numoppitemtCode
				,ISNULL(T1.numUnitHour,0) numUnitHour
				,monPrice
			FROM
				OpportunityItems
			INNER JOIN
				@TempItems T1
			ON
				OpportunityItems.numoppitemtCode=T1.numOppItemID
			WHERE
				numOppId = @numOppID
				AND (CASE 
						WHEN ISNULL(@tintPackingMode,1) = 3 
						THEN ISNULL(OpportunityItems.numQtyPicked,0) - ISNULL((SELECT 
																					SUM(SRI.intBoxQty)
																				FROM 
																					ShippingReport SR 
																				INNER JOIN 
																					ShippingBox SB 
																				ON 
																					SR.numShippingReportId=SB.numShippingReportId 
																				INNER JOIN 
																					ShippingReportItems SRI 
																				ON 
																					SB.numBoxID = SRI.numBoxID 
																				INNER JOIN
																					OpportunityBizDocItems OBDI
																				ON
																					 SRI.numOppBizDocItemID = OBDI.numOppBizDocItemID
																				WHERE 
																					SR.numDomainId = @numDomainID
																					AND SR.numOppID = @numOppID
																					AND LEN(ISNULL(SB.vcTrackingNumber,'')) > 0
																					AND OBDI.numOppItemID = OpportunityItems.numoppitemtCode),0) 
						WHEN ISNULL(@tintPackingMode,1) = 2 
						THEN ISNULL(OpportunityItems.numQtyPicked,0) - ISNULL(OpportunityItems.numQtyShipped,0) 
						ELSE ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyShipped,0) 
					END) > 0				
		END
		ELSE IF @tintMode = 2 -- For invoicing orders
		BEGIN
			SELECT
				numoppitemtCode
				,ISNULL(T1.numUnitHour,0) numUnitHour
				,monPrice
			FROM
				OpportunityItems
			INNER JOIN
				@TempItems T1
			ON
				OpportunityItems.numoppitemtCode=T1.numOppItemID
			WHERE
				numOppId = @numOppID
				AND (CASE 
							WHEN ISNULL(@tintInvoicingType,0)=1 
							THEN ISNULL(OpportunityItems.numQtyShipped,0) 
							ELSE ISNULL(OpportunityItems.numUnitHour,0) 
						END) - ISNULL((SELECT 
											SUM(OpportunityBizDocItems.numUnitHour)
										FROM
											OpportunityBizDocs
										INNER JOIN
											OpportunityBizDocItems
										ON
											OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
										WHERE
											OpportunityBizDocs.numOppId = @numOppID
											AND OpportunityBizDocs.numBizDocId=287
											AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode),0) > 0
		END
		ELSE IF @tintMode = 3 -- For deffault shipping bizdoc
		BEGIN
			IF ISNULL(@numDefaultShippingBizDoc,0) > 0
			BEGIN
				SELECT
					numoppitemtCode
					,ISNULL(T1.numUnitHour,0) numUnitHour
					,monPrice
				FROM
					OpportunityItems
				INNER JOIN
					@TempItems T1
				ON
					OpportunityItems.numoppitemtCode=T1.numOppItemID
				WHERE
					numOppId = @numOppID
					AND ISNULL(OpportunityItems.numUnitHour,0)  - ISNULL((SELECT 
																		SUM(OpportunityBizDocItems.numUnitHour)
																	FROM
																		OpportunityBizDocs
																	INNER JOIN
																		OpportunityBizDocItems
																	ON
																		OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																	WHERE
																		OpportunityBizDocs.numOppId = @numOppID
																		AND OpportunityBizDocs.numBizDocId=@numDefaultShippingBizDoc
																		AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode),0) > 0
			END
			ELSE
			BEGIN
				RAISERROR('DEFAULT_SHIPPING_BIZDOC_NOT_CONFIGURED',16,1)
			END
			
		END
	END
	ELSE
	BEGIN
		IF @tintMode = 1 -- For ship orders
		BEGIN
			SELECT
				numoppitemtCode
				,(CASE WHEN ISNULL(@tintPackingMode,1) = 2 THEN ISNULL(OpportunityItems.numQtyPicked,0) - ISNULL(OpportunityItems.numQtyShipped,0) ELSE ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyShipped,0) END) numUnitHour
				,monPrice
			FROM
				OpportunityItems
			WHERE
				numOppId = @numOppID
				AND (CASE WHEN ISNULL(@tintPackingMode,1) = 2 THEN ISNULL(OpportunityItems.numQtyPicked,0) - ISNULL(OpportunityItems.numQtyShipped,0) ELSE ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyShipped,0) END) > 0
		END
		ELSE IF @tintMode = 2 -- For invoicing orders
		BEGIN
			SELECT
				numoppitemtCode
				,(CASE 
						WHEN ISNULL(@tintInvoicingType,0)=1 
						THEN ISNULL(OpportunityItems.numQtyShipped,0) 
						ELSE ISNULL(OpportunityItems.numUnitHour,0) 
					END) - ISNULL((SELECT 
										SUM(OpportunityBizDocItems.numUnitHour)
									FROM
										OpportunityBizDocs
									INNER JOIN
										OpportunityBizDocItems
									ON
										OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
									WHERE
										OpportunityBizDocs.numOppId = @numOppID
										AND OpportunityBizDocs.numBizDocId=287
										AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode),0) numUnitHour
				,monPrice
			FROM
				OpportunityItems
			WHERE
				numOppId = @numOppID
				AND (CASE 
							WHEN ISNULL(@tintInvoicingType,0)=1 
							THEN ISNULL(OpportunityItems.numQtyShipped,0) 
							ELSE ISNULL(OpportunityItems.numUnitHour,0) 
						END) - ISNULL((SELECT 
											SUM(OpportunityBizDocItems.numUnitHour)
										FROM
											OpportunityBizDocs
										INNER JOIN
											OpportunityBizDocItems
										ON
											OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
										WHERE
											OpportunityBizDocs.numOppId = @numOppID
											AND OpportunityBizDocs.numBizDocId=287
											AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode),0) > 0
		END
		ELSE IF @tintMode = 3 -- For deffault shipping bizdoc
		BEGIN
			IF ISNULL(@numDefaultShippingBizDoc,0) > 0
			BEGIN
				SELECT
					numoppitemtCode
					,ISNULL(OpportunityItems.numUnitHour,0) - ISNULL((SELECT 
																		SUM(OpportunityBizDocItems.numUnitHour)
																	FROM
																		OpportunityBizDocs
																	INNER JOIN
																		OpportunityBizDocItems
																	ON
																		OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																	WHERE
																		OpportunityBizDocs.numOppId = @numOppID
																		AND OpportunityBizDocs.numBizDocId=@numDefaultShippingBizDoc
																		AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode),0) numUnitHour
					,monPrice
				FROM
					OpportunityItems
				WHERE
					numOppId = @numOppID
					AND (CASE 
								WHEN ISNULL(@tintInvoicingType,0)=1 
								THEN ISNULL(OpportunityItems.numQtyShipped,0) 
								ELSE ISNULL(OpportunityItems.numUnitHour,0) 
							END) - ISNULL((SELECT 
												SUM(OpportunityBizDocItems.numUnitHour)
											FROM
												OpportunityBizDocs
											INNER JOIN
												OpportunityBizDocItems
											ON
												OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
											WHERE
												OpportunityBizDocs.numOppId = @numOppID
												AND OpportunityBizDocs.numBizDocId=@numDefaultShippingBizDoc
												AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode),0) > 0
			END
			ELSE
			BEGIN
				RAISERROR('DEFAULT_SHIPPING_BIZDOC_NOT_CONFIGURED',16,1)
			END
		END
	END

END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_MassSalesFulfillment_GetRecords')
DROP PROCEDURE dbo.USP_MassSalesFulfillment_GetRecords
GO

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_MassSalesFulfillment_GetRecords]
(
	@numDomainID NUMERIC(18,0)
	,@numUserCntID NUMERIC(18,0)
	,@ClientTimeZoneOffset INT
	,@numViewID NUMERIC(18,0)
	,@numWarehouseID NUMERIC(18,0)
	,@bitGroupByOrder BIT
	,@bitRemoveBO BIT
	,@numShippingZone NUMERIC(18,0)
	,@bitIncludeSearch BIT
	,@vcOrderStauts VARCHAR(MAX)
	,@tintFilterBy TINYINT
	,@vcFilterValue VARCHAR(MAX)
	,@vcCustomSearchValue VARCHAR(MAX)
	,@numBatchID NUMERIC(18,0)
	,@tintPackingViewMode TINYINT
	,@tintPrintBizDocViewMode TINYINT
	,@tintPendingCloseFilter TINYINT
	,@numPageIndex INT
	,@vcSortColumn VARCHAR(100) OUTPUT
	,@vcSortOrder VARCHAR(4) OUTPUT
	,@numTotalRecords NUMERIC(18,0) OUTPUT
)
AS 
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	IF @numViewID = 4 OR @numViewID = 5 OR @numViewID = 6 OR (@numViewID = 2 AND @tintPackingViewMode=1) 
	BEGIN
		SET @bitGroupByOrder = 1
	END
	
	SET @vcCustomSearchValue = REPLACE(@vcCustomSearchValue,'OpportunityItems.vcItemReleaseDate','OpportunityItems.ItemReleaseDate')

	If @numViewID = 6 AND CHARINDEX('OpportunityMaster.bintCreatedDate',@vcCustomSearchValue) > 0
	BEGIN
		SET @vcCustomSearchValue = REPLACE(@vcCustomSearchValue,'OpportunityMaster.bintCreatedDate','DATEADD(MINUTE,@ClientTimeZoneOffset * -1,OpportunityBizDocs.dtCreatedDate)')
	END
	ELSE IF CHARINDEX('OpportunityMaster.bintCreatedDate',@vcCustomSearchValue) > 0
	BEGIN
		SET @vcCustomSearchValue = REPLACE(@vcCustomSearchValue,'OpportunityMaster.bintCreatedDate','DATEADD(MINUTE,@ClientTimeZoneOffset * -1,OpportunityMaster.bintCreatedDate)')
	END

	IF @bitGroupByOrder = 1
	BEGIN
		IF @vcSortColumn NOT IN ('CompanyInfo.vcCompanyName','OpportunityBizDocs.monAmountPaid','OpportunityBizDocs.vcBizDocID','OpportunityMaster.bintCreatedDate','OpportunityMaster.bitPaidInFull','OpportunityMaster.numAge','OpportunityMaster.numStatus','OpportunityMaster.vcPoppName','Item.dtItemReceivedDate','OpportunityMaster.numAssignedTo','OpportunityMaster.numRecOwner')
		BEGIN
			SET @vcSortColumn = 'OpportunityMaster.bintCreatedDate'
			SET @vcSortOrder = 'DESC'
		END
	END
	ELSE 
	BEGIN
		IF @vcSortColumn NOT IN ('CompanyInfo.vcCompanyName'
								,'Item.charItemType'
								,'Item.numBarCodeId'
								,'Item.numItemClassification'
								,'Item.numItemGroup'
								,'Item.vcSKU'
								,'OpportunityBizDocs.monAmountPaid'
								,'OpportunityMaster.dtExpectedDate'
								,'OpportunityItems.numQtyPacked'
								,'OpportunityItems.numQtyPicked'
								,'OpportunityItems.numQtyShipped'
								,'OpportunityItems.numRemainingQty'
								,'OpportunityItems.numUnitHour'
								,'OpportunityItems.vcInvoiced'
								,'OpportunityItems.vcItemName'
								,'OpportunityItems.vcItemReleaseDate'
								,'OpportunityMaster.bintCreatedDate'
								,'OpportunityMaster.bitPaidInFull'
								,'OpportunityMaster.numAge'
								,'OpportunityMaster.numStatus'
								,'OpportunityMaster.vcPoppName'
								,'WareHouseItems.vcLocation'
								,'Item.dtItemReceivedDate'
								,'OpportunityBizDocs.vcBizDocID'
								,'Warehouses.vcWareHouse')
		BEGIN
			SET @vcSortColumn = 'OpportunityMaster.bintCreatedDate'
			SET @vcSortOrder = 'DESC'
		END
	END

	DECLARE @tintPackingMode TINYINT
	DECLARE @tintInvoicingType TINYINT
	DECLARE @tintCommitAllocation TINYINT
	DECLARE @bitEnablePickListMapping BIT
	DECLARE @bitEnableFulfillmentBizDocMapping BIT
	DECLARE @numShippingServiceItemID NUMERIC(18,0)
	DECLARE @numDefaultSalesShippingDoc NUMERIC(18,0)
	SELECT 
		@tintCommitAllocation=ISNULL(tintCommitAllocation,1) 
		,@numShippingServiceItemID=ISNULL(numShippingServiceItemID,0)
		,@numDefaultSalesShippingDoc=ISNULL(numDefaultSalesShippingDoc,0)
	FROM 
		Domain 
	WHERE 
		numDomainId=@numDomainID 

	IF @numViewID = 2 AND @tintPackingViewMode=3 AND @numDefaultSalesShippingDoc = 0
	BEGIN
		RAISERROR('DEFAULT_SHIPPING_BIZDOC_NOT_CONFIGURED',16,1)
		RETURN
	END

	SELECT
		@tintPackingMode=tintPackingMode
		,@tintInvoicingType=tintInvoicingType
		,@bitEnablePickListMapping=bitEnablePickListMapping
		,@bitEnableFulfillmentBizDocMapping=bitEnableFulfillmentBizDocMapping
	FROM
		MassSalesFulfillmentConfiguration
	WHERE
		numDomainID = @numDomainID 

	DECLARE @TempFieldsLeft TABLE
	(
		numPKID INT IDENTITY(1,1)
		,ID VARCHAR(50)
		,numFieldID NUMERIC(18,0)
		,vcFieldName VARCHAR(100)
		,vcOrigDbColumnName VARCHAR(100)
		,vcListItemType VARCHAR(10)
		,numListID NUMERIC(18,0)
		,bitAllowSorting BIT
		,bitAllowFiltering BIT
		,vcAssociatedControlType VARCHAR(20)
		,vcLookBackTableName VARCHAR(100)
		,bitCustomField BIT
		,intRowNum INT
		,intColumnWidth FLOAT
		,bitIsRequired BIT
		,bitIsEmail BIT
		,bitIsAlphaNumeric BIT
		,bitIsNumeric BIT
		,bitIsLengthValidation BIT
		,intMaxLength INT
		,intMinLength INT
		,bitFieldMessage BIT
		,vcFieldMessage VARCHAR(MAX)
		,Grp_id INT
	)

	INSERT INTO @TempFieldsLeft
	(
		ID
		,numFieldID
		,vcFieldName
		,vcOrigDbColumnName
		,vcListItemType
		,numListID
		,bitAllowSorting
		,bitAllowFiltering
		,vcAssociatedControlType
		,vcLookBackTableName
		,bitCustomField
		,intRowNum
		,intColumnWidth
		,bitIsRequired
		,bitIsEmail
		,bitIsAlphaNumeric
		,bitIsNumeric
		,bitIsLengthValidation
		,intMaxLength
		,intMinLength
		,bitFieldMessage
		,vcFieldMessage
		,Grp_id
	)
	SELECT
		CONCAT(141,'~',DFM.numFieldID,'~',0)
		,DFM.numFieldID
		,(CASE WHEN @numViewID=6 AND DFM.numFieldID=248 THEN 'BizDoc' ELSE DFFM.vcFieldName END)
		,DFM.vcOrigDbColumnName
		,ISNULL(DFM.vcListItemType,'')
		,ISNULL(DFM.numListID,0)
		,DFFM.bitAllowSorting
		,DFFM.bitAllowFiltering
		,DFFM.vcAssociatedControlType
		,DFM.vcLookBackTableName
		,0
		,DFCD.intRowNum
		,ISNULL(DFCD.intColumnWidth,0)
		,0
		,0
		,0
		,(CASE WHEN vcFieldDataType IN ('N','M') THEN 1 ELSE 0 END)
		,0
		,0
		,0
		,0
		,''
		,0
	FROM
		DycFormConfigurationDetails DFCD
	INNER JOIN
		DycFormField_Mapping DFFM
	ON
		DFCD.numFormId = DFFM.numFormID
		AND DFCD.numFieldId = DFFM.numFieldID
	INNER JOIN
		DycFieldMaster DFM
	ON
		DFFM.numFieldID = DFM.numFieldId
	WHERE 
		DFCD.numDomainId=@numDomainID 
		AND DFCD.numUserCntID=@numUserCntID 
		AND DFCD.numFormId=141 
		AND DFCD.numViewId=@numViewID
		AND ISNULL(DFFM.bitDeleted,0) = 0
		AND ISNULL(DFFM.bitSettingField,0) = 1
		AND ISNULL(DFCD.bitCustom,0) = 0
	UNION
	SELECT
		CONCAT(141,'~',CFM.Fld_id,'~',0)
		,CFM.Fld_id
		,CFM.Fld_label
		,CONCAT('CUST',CFM.Fld_id)
		,''
		,ISNULL(CFM.numListID,0)
		,0
		,0
		,CFM.Fld_type
		,''
		,1
		,DFCD.intRowNum
		,ISNULL(DFCD.intColumnWidth,0)
		,ISNULL(bitIsRequired,0)
		,ISNULL(bitIsEmail,0)
		,ISNULL(bitIsAlphaNumeric,0)
		,ISNULL(bitIsNumeric,0)
		,ISNULL(bitIsLengthValidation,0)
		,ISNULL(intMaxLength,0)
		,ISNULL(intMinLength,0)
		,ISNULL(bitFieldMessage,0)
		,ISNULL(vcFieldMessage,'')
		,CFM.Grp_id
	FROM
		DycFormConfigurationDetails DFCD
	INNER JOIN
		CFW_Fld_Master CFM
	ON
		DFCD.numFieldId = CFM.Fld_id
	LEFT JOIN
		CFW_Validation CV
	ON
		CFM.Fld_id = CV.numFieldID
	WHERE
		DFCD.numDomainId=@numDomainID 
		AND DFCD.numUserCntID=@numUserCntID 
		AND DFCD.numFormId=141 
		AND DFCD.numViewId=@numViewID
		AND ISNULL(DFCD.bitCustom,0) = 1

	IF (SELECT COUNT(*) FROM @TempFieldsLeft) = 0
	BEGIN
		INSERT INTO @TempFieldsLeft
		(
			ID
			,numFieldID
			,vcFieldName
			,vcOrigDbColumnName
			,vcListItemType
			,numListID
			,bitAllowSorting
			,bitAllowFiltering
			,vcAssociatedControlType
			,vcLookBackTableName
			,bitCustomField
			,intRowNum
			,intColumnWidth
		)
		SELECT
			CONCAT(141,'~',DFM.numFieldID,'~',0)
			,DFM.numFieldID
			,DFFM.vcFieldName
			,DFM.vcOrigDbColumnName
			,ISNULL(DFM.vcListItemType,'')
			,ISNULL(DFM.numListID,0)
			,DFFM.bitAllowSorting
			,DFFM.bitAllowFiltering
			,DFFM.vcAssociatedControlType
			,DFM.vcLookBackTableName
			,0
			,DFFM.[order]
			,100
		FROM
			DycFormField_Mapping DFFM
		INNER JOIN
			DycFieldMaster DFM
		ON
			DFFM.numFieldID = DFM.numFieldId
		WHERE 
			DFFM.numFormId=141 
			AND ISNULL(DFFM.bitDeleted,0) = 0
			AND ISNULL(DFFM.bitSettingField,0) = 1
			AND ISNULL(DFFM.bitDefault,0) = 1
	END

	DECLARE @vcSelect NVARCHAR(MAX) = CONCAT('SELECT 
												OpportunityMaster.numOppID
												,OpportunityMaster.dtExpectedDate AS dtExpectedDateOrig
												,DivisionMaster.numDivisionID
												,ISNULL(DivisionMaster.numTerID,0) numTerID
												,ISNULL(OpportunityMaster.intUsedShippingCompany,0) numOppShipVia
												,ISNULL(OpportunityMaster.numShippingService,0) numOppShipService
												,ISNULL(DivisionMaster.intShippingCompany,0) AS numPreferredShipVia
												,ISNULL(DivisionMaster.numDefaultShippingServiceID,0) numPreferredShipService
												,ISNULL(OpportunityBizDocs.numOppBizDocsId,0) AS numOppBizDocID
												,ISNULL(OpportunityBizDocs.vcBizDocID,'''') vcBizDocID
												,ISNULL(OpportunityBizDocs.monDealAmount,0) - ISNULL(OpportunityBizDocs.monAmountPaid,0) AS monAmountToPay
												,',(CASE WHEN ISNULL(@bitGroupByOrder,0)=1 THEN '0' ELSE 'ISNULL(OpportunityItems.numoppitemtCode,0)' END),' AS numOppItemID
												',(CASE WHEN ISNULL(@bitGroupByOrder,0)=1
														THEN ',0 AS numRemainingQty'
														ELSE ',(CASE 
															WHEN @numViewID = 1
															THEN ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyPicked,0)
															WHEN @numViewID = 2 AND @tintPackingViewMode = 2
															THEN (CASE WHEN ISNULL(@tintPackingMode,1) = 2 THEN ISNULL(OpportunityItems.numQtyPicked,0) - ISNULL(OpportunityItems.numQtyShipped,0) ELSE ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyShipped,0) END)
															WHEN @numViewID = 2 AND @tintPackingViewMode = 3 AND ISNULL(OpportunityBizDocs.numOppBizDocsId,0) = 0
															THEN (CASE WHEN ISNULL(@tintPackingMode,1) = 2 THEN ISNULL(OpportunityItems.numQtyPicked,0) ELSE ISNULL(OpportunityItems.numUnitHour,0) END) - ISNULL((SELECT 
																																																						SUM(OBIInner.numUnitHour)
																																																					FROM
																																																						OpportunityBizDocs OBInner
																																																					INNER JOIN
																																																						OpportunityBizDocItems OBIInner
																																																					ON
																																																						OBInner.numOppBizDocsId = OBIInner.numOppBizDocID
																																																					WHERE
																																																						OBInner.numOppId = OpportunityMaster.numOppID
																																																						AND OBInner.numBizDocId=@numDefaultSalesShippingDoc
					 																																																	AND OBIInner.numOppItemID = OpportunityItems.numoppitemtcode),0)
															WHEN @numViewID = 2 AND @tintPackingViewMode = 3 AND ISNULL(OpportunityBizDocs.numOppBizDocsId,0) > 0
															THEN ISNULL(OpportunityBizDocItems.numUnitHour,0)
															WHEN @numViewID = 3
															THEN (CASE 
																	WHEN ISNULL(@tintInvoicingType,0)=1 
																	THEN ISNULL(OpportunityItems.numQtyShipped,0) 
																	ELSE ISNULL(OpportunityItems.numUnitHour,0) 
																END) - ISNULL((SELECT 
																					SUM(OpportunityBizDocItems.numUnitHour)
																				FROM
																					OpportunityBizDocs
																				INNER JOIN
																					OpportunityBizDocItems
																				ON
																					OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																				WHERE
																					OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
																					AND OpportunityBizDocs.numBizDocId=287
																					AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode),0)
															ELSE 0
														END) numRemainingQty'
													END)
													,','''' dtAnticipatedDelivery,'''' vcShipStatus,'''' vcPaymentStatus,0 numShipRate
													,(CASE 
														WHEN @numViewID = 2 AND @tintPackingViewMode = 2 AND ISNULL(@bitEnablePickListMapping,0) = 1 
														THEN CONCAT(''['',STUFF((SELECT 
																	CONCAT('', {"numOppBizDocsId":'',OB.numOppBizDocsId,'', "vcBizDocName":"'',OB.vcBizDocID,'' '',ISNULL(BT.vcTemplateName,''''),''"}'') 
																FROM 
																	OpportunityBizDocs OB
																LEFT JOIN
																	BizDocTemplate AS BT
																ON
																	OB.numBizDocTempID=BT.numBizDocTempID
																WHERE 
																	OB.numOppId = OpportunityMaster.numOppID
																	AND OB.numBizDocId=29397
																ORDER BY
																	OB.numOppBizDocsId
																FOR XML PATH('''')),1,1,''''),'']'')
														WHEN @numViewID = 3 AND ISNULL(@bitEnableFulfillmentBizDocMapping,0) = 1 
														THEN  CONCAT(''['',STUFF((SELECT 
																	CONCAT('', {"numOppBizDocsId":'',OB.numOppBizDocsId,'', "vcBizDocName":"'',OB.vcBizDocID,'' '',ISNULL(BT.vcTemplateName,''''),''"}'') 
																FROM 
																	OpportunityBizDocs OB
																LEFT JOIN
																	BizDocTemplate AS BT
																ON
																	OB.numBizDocTempID=BT.numBizDocTempID
																WHERE 
																	OB.numOppId = OpportunityMaster.numOppID
																	AND OB.numBizDocId IN (29397,296)
																ORDER BY
																	OB.numOppBizDocsId
																FOR XML PATH('''')),1,1,''''),'']'')
														ELSE ''''
													END) vcLinkingBizDocs,
													(CASE WHEN EXISTS (SELECT OIInner.numOppItemtcode FROM OpportunityItems OIInner WHERE OIInner.numOppID=OpportunityMaster.numOppId AND ISNULL(OIInner.bitRequiredWarehouseCorrection,0)=1) THEN 1 ELSE 0 END) bitRequiredWarehouseCorrection',
													(CASE WHEN @numViewID=2 AND @tintPackingViewMode = 1 THEN ',ISNULL(OpportunityItems.ItemReleaseDate,CAST(OpportunityMaster.dtReleaseDate AS DATE)) dtShipByDate' ELSE ',CAST(OpportunityMaster.dtReleaseDate AS DATE) AS dtShipByDate' END))

	DECLARE @vcFrom NVARCHAR(MAX) = CONCAT(' FROM
												OpportunityMaster
											INNER JOIN
												DivisionMaster
											ON
												OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
											INNER JOIN
												CompanyInfo
											ON
												DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
											INNER JOIN
												OpportunityItems
											ON
												OpportunityMaster.numOppId = OpportunityItems.numOppId
											LEFT JOIN 
												MassSalesFulfillmentBatchOrders
											ON
												MassSalesFulfillmentBatchOrders.numBatchID = @numBatchID
												AND OpportunityMaster.numOppId = MassSalesFulfillmentBatchOrders.numOppID
												AND (OpportunityItems.numOppItemtCode = MassSalesFulfillmentBatchOrders.numOppItemID OR ISNULL(MassSalesFulfillmentBatchOrders.numOppItemID,0) = 0)
											LEFT JOIN
												WareHouseItems
											ON
												OpportunityItems.numWarehouseItmsID = WareHouseItems.numWareHouseItemID
											LEFT JOIN
												WareHouses
											ON
												WareHouseItems.numWarehouseID = WareHouses.numWarehouseID
											LEFT JOIN
												WarehouseLocation
											ON
												WareHouseItems.numWLocationID = WarehouseLocation.numWLocationID
											INNER JOIN
												Item
											ON
												OpportunityItems.numItemCode = Item.numItemCode
											',(CASE WHEN @numViewID=6 THEN ' INNER JOIN ' ELSE ' LEFT JOIN ' END),'
												OpportunityBizDocs
											ON
												OpportunityMaster.numOppId = OpportunityBizDocs.numOppId
												AND OpportunityBizDocs.numBizDocID= ',(CASE 
																						WHEN @numViewID=4 THEN 287 
																						WHEN @numViewID=6 AND @tintPrintBizDocViewMode=1 THEN 55206 
																						WHEN @numViewID=6 AND @tintPrintBizDocViewMode=2 THEN 29397 
																						WHEN @numViewID=6 AND @tintPrintBizDocViewMode=3 THEN 296 
																						WHEN @numViewID=6 AND @tintPrintBizDocViewMode=4 THEN 287 
																						ELSE 0
																					END),'
												AND @numViewID IN (4,6) 
											LEFT JOIN
												OpportunityBizDocItems
											ON
												OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocId
												AND OpportunityItems.numoppitemtCode = OpportunityBizDocItems.numOppItemID')

	DECLARE @vcFrom1 NVARCHAR(MAX) = ' FROM
										OpportunityMaster
									INNER JOIN
										DivisionMaster
									ON
										OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
									INNER JOIN
										CompanyInfo
									ON
										DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
									INNER JOIN
										OpportunityBizDocs
									ON
										OpportunityMaster.numOppId = OpportunityBizDocs.numOppId
										AND OpportunityBizDocs.numBizDocID= @numDefaultSalesShippingDoc
									INNER JOIN
										OpportunityBizDocItems
									ON
										OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocId
									INNER JOIN
										OpportunityItems
									ON
										OpportunityMaster.numOppId = OpportunityItems.numOppId
										AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtCode 
									LEFT JOIN 
										MassSalesFulfillmentBatchOrders
									ON
										MassSalesFulfillmentBatchOrders.numBatchID = @numBatchID
										AND OpportunityMaster.numOppId = MassSalesFulfillmentBatchOrders.numOppID
										AND (OpportunityItems.numOppItemtCode = MassSalesFulfillmentBatchOrders.numOppItemID OR ISNULL(MassSalesFulfillmentBatchOrders.numOppItemID,0) = 0)
									LEFT JOIN
										WareHouseItems
									ON
										OpportunityItems.numWarehouseItmsID = WareHouseItems.numWareHouseItemID
									LEFT JOIN
										WareHouses
									ON
										WareHouseItems.numWarehouseID = WareHouses.numWarehouseID
									LEFT JOIN
										WarehouseLocation
									ON
										WareHouseItems.numWLocationID = WarehouseLocation.numWLocationID
									INNER JOIN
										Item
									ON
										OpportunityItems.numItemCode = Item.numItemCode
										AND 1 = (CASE WHEN @numViewID = 2 AND @tintPackingViewMode = 3 THEN (CASE WHEN ISNULL(Item.bitContainer,0)=1 THEN 0 ELSE 1 END) ELSE 1 END)'

	DECLARE @vcWhere VARCHAR(MAX) = CONCAT(' WHERE
												OpportunityMaster.numDomainId=@numDomainID
												AND OpportunityMaster.tintOppType = 1
												AND OpportunityMaster.tintOppStatus = 1'
												,(CASE 
													WHEN @numViewID=6 AND @tintPendingCloseFilter=1 THEN ' AND ISNULL(OpportunityMaster.tintshipped,0) = 0'
													WHEN @numViewID=6 AND @tintPendingCloseFilter=2 THEN ' AND ISNULL(OpportunityMaster.tintshipped,0) = 1'
													WHEN @numViewID=6 AND @tintPendingCloseFilter=3 THEN ''
													ELSE ' AND ISNULL(OpportunityMaster.tintshipped,0) = 0'
												END)
												,(CASE WHEN @numViewID <> 6 AND ISNULL(@numWarehouseID,0) > 0 THEN ' AND 1 = (CASE WHEN ISNULL(OpportunityItems.numWarehouseItmsID,0) > 0 THEN (CASE WHEN @numWarehouseID=WareHouseItems.numWarehouseID OR ISNULL(OpportunityItems.bitRequiredWarehouseCorrection,0) = 1 THEN 1 ELSE 0 END) ELSE 1 END)' ELSE '' END)
												,(CASE 
													WHEN @numViewID IN (1,2) AND @tintPackingViewMode <> 1 AND ISNULL(@bitRemoveBO,0) = 1
													THEN ' AND 1 = (CASE 
																		WHEN (Item.charItemType = ''p'' AND ISNULL(OpportunityItems.bitDropShip, 0) = 0 AND ISNULL(Item.bitAsset,0)=0)
																		THEN 
																			(CASE 
																				WHEN dbo.CheckOrderItemInventoryStatus(OpportunityItems.numItemCode,(CASE 
																																						WHEN @numViewID = 1
																																						THEN ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyPicked,0)
																																						WHEN @numViewID = 2 AND @tintPackingViewMode = 2
																																						THEN (CASE WHEN ISNULL(@tintPackingMode,1) = 2 THEN ISNULL(OpportunityItems.numQtyPicked,0) - ISNULL(OpportunityItems.numQtyShipped,0) ELSE ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyShipped,0) END)
																																						WHEN @numViewID = 2 AND @tintPackingViewMode = 3
																																						THEN (CASE WHEN ISNULL(@tintPackingMode,1) = 2 THEN ISNULL(OpportunityItems.numQtyPicked,0) ELSE ISNULL(OpportunityItems.numUnitHour,0) END) - ISNULL((SELECT 
																																																																													SUM(OpportunityBizDocItems.numUnitHour)
																																																																												FROM
																																																																													OpportunityBizDocs
																																																																												INNER JOIN
																																																																													OpportunityBizDocItems
																																																																												ON
																																																																													OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																																																																												WHERE
																																																																													OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
																																																																													AND OpportunityBizDocs.numBizDocId=@numDefaultSalesShippingDoc
					 																																																																							AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode),0)
				
																																						ELSE OpportunityItems.numUnitHour
																																					END),OpportunityItems.numoppitemtCode,OpportunityItems.numWarehouseItmsID,(CASE WHEN ISNULL(Item.bitKitParent,0) = 1 THEN 1 ELSE 0 END)) > 0 THEN 0 ELSE 1 END)
																							ELSE 1
																				END)'
														ELSE ''
												END)
												,(CASE 
													WHEN LEN(ISNULL(@vcOrderStauts,'')) > 0 
													THEN (CASE 
															WHEN ISNULL(@bitIncludeSearch,0) = 0 
															THEN ' AND 1 = (CASE WHEN OpportunityMaster.numStatus NOT IN (SELECT Id FROM dbo.SplitIDs(@vcOrderStauts,'','')) THEN 1 ELSE 0 END)'
															ELSE ' AND 1 = (CASE WHEN OpportunityMaster.numStatus IN (SELECT Id FROM dbo.SplitIDs(@vcOrderStauts,'','')) THEN 1 ELSE 0 END)'
														END)
													ELSE ''
												END)
												,(CASE 
													WHEN LEN(ISNULL(@vcFilterValue,'')) > 0
													THEN
														CASE 
															WHEN @tintFilterBy=1 --Item Classification
															THEN ' AND 1 = (CASE 
																				WHEN ISNULL(@bitIncludeSearch,0) = 0 
																				THEN (CASE WHEN Item.numItemClassification NOT IN (SELECT Id FROM dbo.SplitIDs(@vcFilterValue,'','')) THEN 1 ELSE 0 END) 
																				ELSE (CASE WHEN Item.numItemClassification IN (SELECT Id FROM dbo.SplitIDs(@vcFilterValue,'','')) THEN 1 ELSE 0 END) 
																			END)'
															ELSE ''
														END
													ELSE ''
												END)
												,(CASE
													WHEN @numViewID = 1 THEN ' AND 1 = (CASE WHEN ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyPicked,0) > 0 THEN 1 ELSE 0 END)'
													WHEN @numViewID = 2 THEN ' AND 1 = (CASE 
																							WHEN @tintPackingViewMode = 1 
																							THEN (CASE 
																									WHEN ISNULL((SELECT COUNT(*) FROM OpportunityItems OIInner WHERE OIInner.numOppID=OpportunityMaster.numOppId AND OIInner.numItemCode=ISNULL(@numShippingServiceItemID,0) AND OIInner.numShipFromWarehouse=@numWarehouseID AND OIInner.ItemReleaseDate=ISNULL(OpportunityItems.ItemReleaseDate,CAST(OpportunityMaster.dtReleaseDate AS DATE))),0) = 0 
																									THEN 1 
																									ELSE 0 
																								END)
																							WHEN @tintPackingViewMode = 2
																							THEN (CASE 
																									WHEN (CASE WHEN ISNULL(@tintPackingMode,1) = 2 THEN ISNULL(OpportunityItems.numQtyPicked,0) ELSE ISNULL(OpportunityItems.numUnitHour,0) END) - ISNULL(OpportunityItems.numQtyShipped,0) > 0
																									THEN 1 
																									ELSE 0 
																								END)
																							WHEN @tintPackingViewMode = 3
																							THEN (CASE 
																									WHEN (CASE WHEN ISNULL(@tintPackingMode,1) = 2 THEN ISNULL(OpportunityItems.numQtyPicked,0) ELSE ISNULL(OpportunityItems.numUnitHour,0) END) - ISNULL((SELECT 
																																																																SUM(OBIInner.numUnitHour)
																																																															FROM
																																																																OpportunityBizDocs OBInner
																																																															INNER JOIN
																																																																OpportunityBizDocItems OBIInner
																																																															ON
																																																																OBInner.numOppBizDocsId = OBIInner.numOppBizDocID
																																																															WHERE
																																																																OBInner.numOppId = OpportunityMaster.numOppID
																																																																AND OBInner.numBizDocId=@numDefaultSalesShippingDoc
																																																																AND OBIInner.numOppItemID = OpportunityItems.numoppitemtcode),0) > 0
																									THEN 1 
																									ELSE 0 
																								END)
																							ELSE 0
																						END)'
													WHEN @numViewID = 3
													THEN ' AND 1 = (CASE 
																		WHEN (CASE 
																				WHEN ISNULL(@tintInvoicingType,0)=1 
																				THEN ISNULL(OpportunityItems.numQtyShipped,0) 
																				ELSE ISNULL(OpportunityItems.numUnitHour,0) 
																			END) - ISNULL((SELECT 
																								SUM(OpportunityBizDocItems.numUnitHour)
																							FROM
																								OpportunityBizDocs
																							INNER JOIN
																								OpportunityBizDocItems
																							ON
																								OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																							WHERE
																								OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
																								AND OpportunityBizDocs.numBizDocId=287
																								AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode),0) > 0 
																		THEN 1
																		ELSE 0 
																	END)'
												END)
												,(CASE
															WHEN @numViewID = 1 THEN 'AND 1 = (CASE WHEN ISNULL(WareHouseItems.numWareHouseItemID,0) > 0 AND ISNULL(OpportunityItems.bitDropship,0) = 0 THEN 1 ELSE 0 END)'
															WHEN @numViewID = 2 AND @tintPackingViewMode <> 2 THEN ' AND 1 = (CASE WHEN ISNULL(WareHouseItems.numWareHouseItemID,0) > 0 AND ISNULL(OpportunityItems.bitDropship,0) = 0 THEN 1 ELSE 0 END)'
															WHEN @numViewID = 2 AND @tintPackingViewMode = 2 THEN ' AND 1=1'
															WHEN @numViewID = 3 THEN ' AND 1=1'
															WHEN @numViewID = 4 THEN ' AND 1=1'
															WHEN @numViewID = 5 THEN ' AND 1=1'
															WHEN @numViewID = 6 THEN ' AND 1=1'
															ELSE ' AND 1=0'
													END)
												,(CASE WHEN ISNULL(@numBatchID,0) > 0 THEN ' AND 1 = (CASE WHEN MassSalesFulfillmentBatchOrders.ID IS NOT NULL THEN 1 ELSE 0 END)' ELSE '' END)
												,(CASE WHEN @numViewID = 4 OR @numViewID=6 THEN ' AND 1= (CASE WHEN OpportunityBizDocs.numOppBizDocsId IS NOT NULL THEN 1 ELSE 0 END)' ELSE '' END)
												,(CASE WHEN @numViewID = 4 THEN ' AND 1 = (CASE WHEN ISNULL(OpportunityBizDocs.monDealAmount,0) - ISNULL(OpportunityBizDocs.monAmountPaid,0) > 0 THEN 1 ELSE 0 END)' ELSE '' END)
												,(CASE 
													WHEN @numViewID = 5 AND ISNULL(@tintPendingCloseFilter,0) = 2
													THEN 
														' AND 1 = (CASE 
																	WHEN (SELECT 
																				COUNT(*) 
																			FROM 
																			(
																				SELECT
																					OI.numoppitemtCode,
																					ISNULL(OI.numUnitHour,0) AS OrderedQty,
																					ISNULL(TempFulFilled.FulFilledQty,0) AS FulFilledQty
																				FROM
																					OpportunityItems OI
																				INNER JOIN
																					Item I
																				ON
																					OI.numItemCode = I.numItemCode
																				OUTER APPLY
																				(
																					SELECT
																						SUM(OpportunityBizDocItems.numUnitHour) AS FulFilledQty
																					FROM
																						OpportunityBizDocs
																					INNER JOIN
																						OpportunityBizDocItems 
																					ON
																						OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																					WHERE
																						OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
																						AND ISNULL(OpportunityBizDocs.numBizDocId,0) = 296
																						AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
																				) AS TempFulFilled
																				WHERE
																					OI.numOppID = OpportunityMaster.numOppID
																					AND ISNULL(OI.numUnitHour,0) > 0
																					AND ISNULL(OI.bitDropShip,0) = 0
																			) X
																			WHERE
																				X.OrderedQty <> X.FulFilledQty) = 0
																		AND (SELECT 
																				COUNT(*) 
																			FROM 
																			(
																				SELECT
																					OI.numoppitemtCode,
																					ISNULL(OI.numUnitHour,0) AS OrderedQty,
																					ISNULL(TempInvoice.InvoicedQty,0) AS InvoicedQty
																				FROM
																					OpportunityItems OI
																				INNER JOIN
																					Item I
																				ON
																					OI.numItemCode = I.numItemCode
																				OUTER APPLY
																				(
																					SELECT
																						SUM(OpportunityBizDocItems.numUnitHour) AS InvoicedQty
																					FROM
																						OpportunityBizDocs
																					INNER JOIN
																						OpportunityBizDocItems 
																					ON
																						OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																					WHERE
																						OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
																						AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
																						AND ISNULL(OpportunityBizDocs.bitAuthoritativeBizDocs,0) = 1
																				) AS TempInvoice
																				WHERE
																					OI.numOppID = OpportunityMaster.numOppID
																					AND ISNULL(OI.numUnitHour,0) > 0
																			) X
																			WHERE
																				X.OrderedQty > X.InvoicedQty) = 0
																	THEN 1 
																	ELSE 0 
																END)'
															ELSE ''
														END)
												,(CASE 
													WHEN ISNULL(@numShippingZone,0) > 0 
													THEN ' AND 1 = (CASE 
																		WHEN ISNULL((SELECT TOP 1 numState FROM fn_getOPPAddressDetails(OpportunityMaster.numOppId,OpportunityMaster.numDomainID,2)),0) > 0 
																		THEN
																			CASE 
																				WHEN (SELECT COUNT(*) FROM dbo.State WHERE numDomainID=@numDomainId AND numStateID =  (SELECT TOP 1 numState FROM fn_getOPPAddressDetails(OpportunityMaster.numOppId,OpportunityMaster.numDomainID,2)) AND numShippingZone=@numShippingZone) > 0 
																				THEN 1
																				ELSE 0
																			END
																		ELSE 0 
																	END)'
													ELSE ''
												END)
												,(CASE WHEN ISNULL(@bitGroupByOrder,0)=0 THEN ' AND 1 = (CASE WHEN ISNULL(OpportunityBizDocs.numOppBizDocsId,0) > 0 THEN (CASE WHEN OpportunityBizDocItems.numOppItemID > 0 THEN 1 ELSE 0 END) ELSE 1 END)' ELSE '' END))

	DECLARE @vcWhere1 VARCHAR(MAX) = CONCAT(' WHERE
												OpportunityMaster.numDomainId=@numDomainID
												AND OpportunityMaster.tintOppType = 1
												AND OpportunityMaster.tintOppStatus = 1
												AND ISNULL(OpportunityMaster.tintshipped,0) = 0
												AND 1 = (CASE 
															WHEN LEN(ISNULL(@vcOrderStauts,'''')) > 0 
															THEN (CASE 
																	WHEN ISNULL(@bitIncludeSearch,0) = 0 
																	THEN (CASE WHEN OpportunityMaster.numStatus NOT IN (SELECT Id FROM dbo.SplitIDs(@vcOrderStauts,'','')) THEN 1 ELSE 0 END) 
																	ELSE (CASE WHEN OpportunityMaster.numStatus IN (SELECT Id FROM dbo.SplitIDs(@vcOrderStauts,'','')) THEN 1 ELSE 0 END) 
																END)
															ELSE 1 
														END)
												AND 1 = (CASE 
															WHEN LEN(ISNULL(@vcFilterValue,'''')) > 0
															THEN
																CASE 
																	WHEN @tintFilterBy=1 --Item Classification
																	THEN (CASE 
																			WHEN ISNULL(@bitIncludeSearch,0) = 0 
																			THEN (CASE WHEN Item.numItemClassification NOT IN (SELECT Id FROM dbo.SplitIDs(@vcFilterValue,'','')) THEN 1 ELSE 0 END) 
																			ELSE (CASE WHEN Item.numItemClassification IN (SELECT Id FROM dbo.SplitIDs(@vcFilterValue,'','')) THEN 1 ELSE 0 END) 
																		END)
																	ELSE 1
																END
															ELSE 1
														END)
												AND 1 = (CASE 
															WHEN ISNULL((SELECT COUNT(*) FROM ShippingBox WHERE ISNULL(vcShippingLabelImage,'''') <> '''' AND ISNULL(vcTrackingNumber,'''') <> '''' AND numShippingReportId IN (SELECT numShippingReportId FROM ShippingReport WHERE ShippingReport.numOppID=OpportunityMaster.numOppId AND ShippingReport.numOppBizDocId=OpportunityBizDocs.numOppBizDocsId)),0) = 0 
															THEN 1 
															ELSE 0 
														END)
												AND 1 = (CASE WHEN ISNULL(WareHouseItems.numWareHouseItemID,0) > 0 AND ISNULL(OpportunityItems.bitDropship,0) = 0 THEN 1 ELSE 0 END)
												AND 1 = (CASE WHEN ISNULL(@numWarehouseID,0) > 0 AND ISNULL(OpportunityItems.numWarehouseItmsID,0) > 0 THEN (CASE WHEN @numWarehouseID=WareHouseItems.numWarehouseID OR ISNULL(OpportunityItems.bitRequiredWarehouseCorrection,0) = 1 THEN 1 ELSE 0 END) ELSE 1 END)
												AND 1 = (CASE WHEN ISNULL(@numBatchID,0) > 0 THEN (CASE WHEN MassSalesFulfillmentBatchOrders.ID IS NOT NULL THEN 1 ELSE 0 END) ELSE 1 END)'
												,(CASE 
													WHEN ISNULL(@numShippingZone,0) > 0 
													THEN ' AND 1 = (CASE 
																		WHEN ISNULL((SELECT TOP 1 numState FROM fn_getOPPAddressDetails(OpportunityMaster.numOppId,OpportunityMaster.numDomainID,2)),0) > 0 
																		THEN
																			CASE 
																				WHEN (SELECT COUNT(*) FROM dbo.State WHERE numDomainID=@numDomainId AND numStateID =  (SELECT TOP 1 numState FROM fn_getOPPAddressDetails(OpportunityMaster.numOppId,OpportunityMaster.numDomainID,2)) AND numShippingZone=@numShippingZone) > 0 
																				THEN 1
																				ELSE 0
																			END
																		ELSE 0 
																	END)'
													ELSE ''
												END)
												,(CASE WHEN ISNULL(@bitGroupByOrder,0)=0 THEN ' AND 1 = (CASE WHEN ISNULL(OpportunityBizDocs.numOppBizDocsId,0) > 0 THEN (CASE WHEN OpportunityBizDocItems.numOppItemID > 0 THEN 1 ELSE 0 END) ELSE 1 END)' ELSE '' END))

	SET @vcWhere = CONCAT(@vcWhere,' ',@vcCustomSearchValue)
	SET @vcWhere1 = CONCAT(@vcWhere1,' ',@vcCustomSearchValue)
	
	DECLARE @vcGroupBy NVARCHAR(MAX) = (CASE 
											WHEN @bitGroupByOrder = 1 
											THEN CONCAT('GROUP BY OpportunityMaster.numOppID
																	,DivisionMaster.numDivisionID
																	,DivisionMaster.numTerID
																	,DivisionMaster.intShippingCompany
																	,DivisionMaster.numDefaultShippingServiceID
																	,OpportunityBizDocs.numOppBizDocsId
																	,OpportunityBizDocs.monDealAmount
																	,OpportunityBizDocs.monAmountPaid
																	,CompanyInfo.vcCompanyName
																	,OpportunityMaster.vcPoppName
																	,OpportunityMaster.numAssignedTo
																	,OpportunityMaster.numStatus
																	,OpportunityMaster.numRecOwner
																	,OpportunityMaster.bintCreatedDate
																	,OpportunityMaster.txtComments
																	,OpportunityBizDocs.vcBizDocID
																	,OpportunityMaster.intUsedShippingCompany
																	,OpportunityMaster.numShippingService
																	,DivisionMaster.intShippingCompany
																	,OpportunityBizDocs.dtCreatedDate
																	,OpportunityMaster.dtReleaseDate
																	,OpportunityMaster.dtExpectedDate',(CASE WHEN @numViewID = 2 AND @tintPackingViewMode = 1 THEN ',OpportunityItems.ItemReleaseDate' ELSE '' END))
											ELSE '' 
										END)

	DECLARE @vcOrderBy NVARCHAR(MAX) = CONCAT(' ORDER BY ',
					(CASE @vcSortColumn
						WHEN 'CompanyInfo.vcCompanyName' THEN 'CompanyInfo.vcCompanyName' 
						WHEN 'Item.charItemType' THEN '(CASE 
															WHEN Item.charItemType=''P''  THEN ''Inventory Item''
															WHEN Item.charItemType=''N'' THEN ''Non Inventory Item'' 
															WHEN Item.charItemType=''S'' THEN ''Service''
															WHEN Item.charItemType=''A'' THEN ''Accessory''
														END)'
						WHEN 'Item.numBarCodeId' THEN 'Item.numBarCodeId'
						WHEN 'Item.numItemClassification' THEN 'dbo.GetListIemName(Item.numItemClassification)'
						WHEN 'Item.numItemGroup' THEN 'ISNULL((SELECT vcItemGroup FROM ItemGroups WHERE numItemGroupID = Item.numItemGroup),'''')'
						WHEN 'Item.vcSKU' THEN 'Item.vcSKU'
						WHEN 'OpportunityItems.vcItemName' THEN 'Item.vcItemName'
						WHEN 'OpportunityMaster.numStatus' THEN 'dbo.GetListIemName(OpportunityMaster.numStatus)'
						WHEN 'OpportunityMaster.vcPoppName' THEN 'OpportunityMaster.vcPoppName'
						WHEN 'OpportunityMaster.numAssignedTo' THEN 'dbo.fn_GetContactName(OpportunityMaster.numAssignedTo)'
						WHEN 'OpportunityMaster.numRecOwner' THEN 'dbo.fn_GetContactName(OpportunityMaster.numRecOwner)'
						WHEN 'WareHouseItems.vcLocation' THEN 'WarehouseLocation.vcLocation'
						WHEN 'OpportunityBizDocs.monAmountPaid' THEN 'TempPaid.monAmountPaid'
						WHEN 'OpportunityBizDocs.vcBizDocID' THEN 'LEN(OpportunityBizDocs.vcBizDocID) DESC, OpportunityBizDocs.vcBizDocID'
						WHEN 'OpportunityItems.numQtyPacked' THEN 'ISNULL((SELECT SUM(numUnitHour) FROM OpportunityBizDocs OBD INNER JOIN OpportunityBizDocItems OBDI ON OBD.numOppBizDocsId=OBDI.numOppBizDocID  WHERE OBD.numOppId=OpportunityMaster.numOppId AND OBD.numBizDocId=29397 AND OBDI.numOppItemID=OpportunityItems.numoppitemtCode),0)'
						WHEN 'OpportunityItems.numQtyPicked' THEN 'OpportunityItems.numQtyPicked'
						WHEN 'OpportunityItems.numQtyShipped' THEN 'OpportunityItems.numQtyShipped'
						WHEN 'OpportunityItems.numRemainingQty' THEN '(CASE 
																			WHEN @numViewID = 1
																			THEN ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyPicked,0)
																			WHEN @numViewID = 2
																			THEN ISNULL(OpportunityItems.numUnitHour,0) - ISNULL((SELECT SUM(numUnitHour) FROM OpportunityBizDocs OBD INNER JOIN OpportunityBizDocItems OBDI ON OBD.numOppBizDocsId=OBDI.numOppBizDocID  WHERE OBD.numOppId=OpportunityMaster.numOppId AND OBD.numBizDocId=29397 AND OBDI.numOppItemID=OpportunityItems.numoppitemtCode),0)
																			ELSE 0
																		END)'
						WHEN 'OpportunityItems.numUnitHour' THEN 'OpportunityItems.numUnitHour'
						WHEN 'OpportunityItems.vcInvoiced' THEN 'ISNULL((SELECT SUM(numUnitHour) FROM OpportunityBizDocs OBD INNER JOIN OpportunityBizDocItems OBDI ON OBD.numOppBizDocsId=OBDI.numOppBizDocID  WHERE OBD.numOppId=OpportunityMaster.numOppId AND OBD.numBizDocId=287 AND OBDI.numOppItemID=OpportunityItems.numoppitemtCode),0)'
						WHEN 'OpportunityMaster.dtExpectedDate' THEN 'OpportunityMaster.dtExpectedDate'
						WHEN 'OpportunityItems.vcItemReleaseDate' THEN 'OpportunityItems.ItemReleaseDate'
						WHEN 'OpportunityMaster.bintCreatedDate' THEN 'OpportunityMaster.bintCreatedDate'
						WHEN 'OpportunityMaster.numAge' THEN 'OpportunityMaster.bintCreatedDate'
						WHEN 'OpportunityMaster.bitPaidInFull' THEN '(CASE WHEN ISNULL(TempPaid.monAmountPaid,0) >= ISNULL(OpportunityMaster.monDealAmount,0) THEN 1 ELSE 0 END)'
						WHEN 'Warehouses.vcWareHouse' THEN '(CASE WHEN ISNULL(OpportunityItems.bitRequiredWarehouseCorrection,0) = 1 THEN '''' ELSE Warehouses.vcWareHouse END)'
						WHEN 'Item.dtItemReceivedDate' THEN 'Item.dtItemReceivedDate'
						ELSE 'OpportunityMaster.bintCreatedDate'
					END),' ',(CASE WHEN ISNULL(@vcSortOrder,'') <> '' THEN @vcSortOrder ELSE 'ASC' END))

	DECLARE @vcOrderBy1 NVARCHAR(MAX) = CONCAT(' ORDER BY ',
					(CASE @vcSortColumn
						WHEN 'CompanyInfo.vcCompanyName' THEN 'TEMP.vcCompanyNameOrig' 
						WHEN 'Item.charItemType' THEN '(CASE 
															WHEN TEMP.charItemTypeOrig=''P''  THEN ''Inventory Item''
															WHEN TEMP.charItemTypeOrig=''N'' THEN ''Non Inventory Item'' 
															WHEN TEMP.charItemTypeOrig=''S'' THEN ''Service''
															WHEN TEMP.charItemTypeOrig=''A'' THEN ''Accessory''
														END)'
						WHEN 'Item.numBarCodeId' THEN 'TEMP.numBarCodeIdOrig'
						WHEN 'Item.numItemClassification' THEN 'dbo.GetListIemName(TEMP.numItemClassificationOrig)'
						WHEN 'Item.numItemGroup' THEN 'ISNULL((SELECT vcItemGroup FROM ItemGroups WHERE numItemGroupID = TEMP.numItemGroupOrig),'''')'
						WHEN 'Item.vcSKU' THEN 'TEMP.vcSKUOrig'
						WHEN 'OpportunityItems.vcItemName' THEN 'TEMP.vcItemNameOrig'
						WHEN 'OpportunityMaster.numStatus' THEN 'dbo.GetListIemName(TEMP.numStatusOrig)'
						WHEN 'OpportunityMaster.vcPoppName' THEN 'TEMP.vcPoppNameOrig'
						WHEN 'OpportunityMaster.numAssignedTo' THEN 'dbo.fn_GetContactName(TEMP.numAssignedToOrig)'
						WHEN 'OpportunityMaster.numRecOwner' THEN 'dbo.fn_GetContactName(TEMP.numRecOwnerOrig)'
						WHEN 'WareHouseItems.vcLocation' THEN 'TEMP.vcLocationOrig'
						WHEN 'OpportunityBizDocs.monAmountPaid' THEN 'TEMP.monAmountPaidOrig'
						WHEN 'OpportunityBizDocs.vcBizDocID' THEN 'LEN(TEMP.vcBizDocID) DESC, TEMP.vcBizDocID'
						WHEN 'OpportunityItems.numQtyPacked' THEN 'ISNULL((SELECT SUM(numUnitHour) FROM OpportunityBizDocs OBD INNER JOIN OpportunityBizDocItems OBDI ON OBD.numOppBizDocsId=OBDI.numOppBizDocID  WHERE OBD.numOppId=TEMP.numOppId AND OBD.numBizDocId=29397 AND OBDI.numOppItemID=TEMP.numOppItemID),0)'
						WHEN 'OpportunityItems.numQtyPicked' THEN 'TEMP.numQtyPickedOrig'
						WHEN 'OpportunityItems.numQtyShipped' THEN 'TEMP.numQtyShippedOrig'
						WHEN 'OpportunityItems.numRemainingQty' THEN '(CASE 
																			WHEN @numViewID = 1
																			THEN ISNULL(TEMP.numUnitHourOrig,0) - ISNULL(OpportunityItems.numQtyPickedOrig,0)
																			WHEN @numViewID = 2
																			THEN ISNULL(TEMP.numUnitHourOrig,0) - ISNULL((SELECT SUM(numUnitHour) FROM OpportunityBizDocs OBD INNER JOIN OpportunityBizDocItems OBDI ON OBD.numOppBizDocsId=OBDI.numOppBizDocID  WHERE OBD.numOppId=TEMP.numOppId AND OBD.numBizDocId=29397 AND OBDI.numOppItemID=TEMP.numOppItemID),0)
																			ELSE 0
																		END)'
						WHEN 'OpportunityItems.numUnitHour' THEN 'TEMP.numUnitHourOrig'
						WHEN 'OpportunityItems.vcInvoiced' THEN 'ISNULL((SELECT SUM(numUnitHour) FROM OpportunityBizDocs OBD INNER JOIN OpportunityBizDocItems OBDI ON OBD.numOppBizDocsId=OBDI.numOppBizDocID  WHERE OBD.numOppId=TEMP.numOppId AND OBD.numBizDocId=287 AND OBDI.numOppItemID=TEMP.numOppItemID),0)'
						WHEN 'OpportunityMaster.dtExpectedDate' THEN 'TEMP.dtExpectedDateOrig'
						WHEN 'OpportunityItems.vcItemReleaseDate' THEN 'TEMP.ItemReleaseDateOrig'
						WHEN 'OpportunityMaster.bintCreatedDate' THEN 'TEMP.bintCreatedDateOrig'
						WHEN 'OpportunityMaster.numAge' THEN 'TEMP.bintCreatedDateOrig'
						WHEN 'OpportunityMaster.bitPaidInFull' THEN '(CASE WHEN ISNULL(TEMP.monAmountPaidOrig,0) >= ISNULL(TEMP.monDealAmountOrig,0) THEN 1 ELSE 0 END)'
						WHEN 'Item.dtItemReceivedDate' THEN 'TEMP.dtItemReceivedDate'
						WHEN 'Warehouses.vcWareHouse' THEN '(CASE WHEN ISNULL(OpportunityItems.bitRequiredWarehouseCorrection,0) = 1 THEN '''' ELSE Warehouses.vcWareHouse END)'
						ELSE 'TEMP.bintCreatedDateOrig'
					END),' ',(CASE WHEN ISNULL(@vcSortOrder,'') <> '' THEN @vcSortOrder ELSE 'ASC' END))

	DECLARE @vcText NVARCHAR(MAX)

	IF EXISTS (SELECT ID FROM @TempFieldsLeft WHERE vcOrigDbColumnName='bitPaidInFull') OR @vcSortColumn='OpportunityMaster.bitPaidInFull'
	BEGIN
		SET @vcText = ' OUTER APPLY
						(
							SELECT 
								SUM(OBInner.monAmountPaid) monAmountPaid
							FROM 
								OpportunityBizDocs OBInner 
							WHERE 
								OBInner.numOppId=OpportunityMaster.numOppId 
								AND OBInner.numBizDocId=287
						) TempPaid'

		SET @vcFrom = CONCAT(@vcFrom,@vcText)
		SET @vcFrom1 = CONCAT(@vcFrom1,@vcText)
	END

	IF EXISTS (SELECT ID FROM @TempFieldsLeft WHERE vcOrigDbColumnName='numQtyPacked') OR @vcSortColumn='OpportunityItems.numQtyPacked'
	BEGIN
		SET @vcText = ' OUTER APPLY
						(
							SELECT 
								SUM(OpportunityBizDocItems.numUnitHour) numPackedQty
							FROM 
								OpportunityBizDocs
							INNER JOIN
								OpportunityBizDocItems
							ON
								OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
							WHERE 
								OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
								AND OpportunityBizDocs.numBizDocId=@numDefaultSalesShippingDoc
								AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode
						) TempPacked'

		SET @vcFrom = CONCAT(@vcFrom,@vcText)
		SET @vcFrom1 = CONCAT(@vcFrom1,@vcText)
	END

	DECLARE @j INT = 0
	DECLARE @jCount INT
	DECLARE @numFieldId NUMERIC(18,0)
	DECLARE @vcListItemType VARCHAR(10)
	DECLARE @vcFieldName AS VARCHAR(50)                                                                                                
	DECLARE @vcAssociatedControlType VARCHAR(10)  
	DECLARE @vcLookBackTableName VARCHAR(100)
	DECLARE @vcDbColumnName VARCHAR(100)
	DECLARE @Grp_id INT                                               
	DECLARE @bitCustomField BIT      
	DECLARE @bitIsNumeric BIT
	SET @jCount = (SELECT COUNT(*) FROM @TempFieldsLeft)


	WHILE @j <= @jCount
	BEGIN
		SELECT
			@numFieldId=numFieldID,
			@vcFieldName = vcFieldName,
			@vcAssociatedControlType = vcAssociatedControlType,
			@vcDbColumnName = (CASE WHEN ISNULL(bitCustomField,0) = 1 THEN CONCAT('Cust',numFieldID) ELSE vcOrigDbColumnName END),
			@vcLookBackTableName = vcLookBackTableName,
			@bitCustomField=ISNULL(bitCustomField,0),
			@Grp_id = ISNULL(Grp_id,0),
			@bitIsNumeric=ISNULL(bitIsNumeric,0),
			@vcListItemType=ISNULL(vcListItemType,'')
		FROM 
			@TempFieldsLeft
		WHERE 
			numPKID = @j

		IF @vcDbColumnName NOT IN ('vcBizDocID','numRemainingQty','dtAnticipatedDelivery','vcShipStatus','vcPaymentStatus','numShipRate')
		BEGIN
			IF ISNULL(@bitCustomField,0) = 0
			BEGIN
				IF @vcDbColumnName = 'vcItemReleaseDate'
				BEGIN
					SET @vcSelect = CONCAT(@vcSelect,',',(CASE WHEN @numViewID=2 AND @tintPackingViewMode = 1 THEN 'ISNULL(OpportunityItems.ItemReleaseDate,CAST(OpportunityMaster.dtReleaseDate AS DATE))' ELSE 'CAST(OpportunityMaster.dtReleaseDate AS DATE)' END),' [',@vcDbColumnName,']')
				END
				ELSE IF (@vcLookBackTableName = 'Item' OR @vcLookBackTableName = 'WareHouseItems' OR @vcLookBackTableName = 'OpportunityItems' OR @vcLookBackTableName='Warehouses') AND ISNULL(@bitGroupByOrder,0)=1
				BEGIN
					SET @vcSelect = CONCAT(@vcSelect,',',(CASE WHEN @bitIsNumeric = 1 THEN '0' ELSE '''''' END),' [',@vcDbColumnName,']')
				END
				ELSE
				BEGIN
					IF @vcDbColumnName = 'numAge'
					BEGIN
						SET @vcSelect = CONCAT(@vcSelect,',CONCAT(''<span style="color:'',(CASE
																							WHEN (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24) = 0 AND (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) < 4  
																							THEN ''#00ff00''
																							WHEN (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24) = 0 AND ((DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) >= 4 AND (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) < 8)
																							THEN ''#00b050''
																							WHEN (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24) = 0 AND ((DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) >= 8 AND (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) < 12)
																							THEN ''#3399ff''
																							WHEN (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24) = 0 AND ((DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) >= 12 AND (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) < 24)
																							THEN ''#ff9900''
																							WHEN (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24) BETWEEN 1 AND 2  
																							THEN ''#9b3596''
																							ELSE ''#ff0000''
																						END),''">'',CONCAT(DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24,''d '',DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24,''h''),''</span>'')',' [',@vcDbColumnName,']')
					END
					ELSE IF @vcDbColumnName = 'dtItemReceivedDate'
					BEGIN
						SET @vcSelect = CONCAT(@vcSelect,',dbo.FormatedDateFromDate(Item.dtItemReceivedDate,@numDomainID) [',@vcDbColumnName,']')
					END
					ELSE IF @vcDbColumnName = 'bitPaidInFull'
					BEGIN
						SET @vcSelect = CONCAT(@vcSelect,',(CASE WHEN ISNULL(TempPaid.monAmountPaid,0) >= ISNULL(OpportunityMaster.monDealAmount,0) THEN 1 ELSE 0 END) [',@vcDbColumnName,']')
					END
					ELSE IF @vcDbColumnName = 'dtExpectedDate'
					BEGIN
						SET @vcSelect = CONCAT(@vcSelect,',dbo.FormatedDateFromDate(OpportunityMaster.dtExpectedDate,@numDomainID) [',@vcDbColumnName,']')
					END
					ELSE IF @vcDbColumnName = 'bintCreatedDate'
					BEGIN
						SET @vcSelect = CONCAT(@vcSelect,',(CASE WHEN @numViewID = 6 THEN dbo.FormatedDateFromDate(DATEADD(MINUTE,@ClientTimeZoneOffset * -1,OpportunityBizDocs.dtCreatedDate),@numDomainID) ELSE dbo.FormatedDateFromDate(DATEADD(MINUTE,@ClientTimeZoneOffset * -1,OpportunityMaster.bintCreatedDate),@numDomainID) END) [',@vcDbColumnName,']')
					END
					ELSE IF @vcDbColumnName = 'numQtyPacked'
					BEGIN
						SET @vcSelect = CONCAT(@vcSelect,',ISNULL(TempPacked.numPackedQty,0) [',@vcDbColumnName,']')
					END
					ELSE IF @vcDbColumnName = 'vcInvoiced'
					BEGIN
						SET @vcSelect = CONCAT(@vcSelect,',ISNULL((SELECT SUM(numUnitHour) FROM OpportunityBizDocs OBD INNER JOIN OpportunityBizDocItems OBDI ON OBD.numOppBizDocsId=OBDI.numOppBizDocID  WHERE OBD.numOppId=OpportunityMaster.numOppId AND OBD.numBizDocId=287 AND OBDI.numOppItemID=OpportunityItems.numoppitemtCode),0) [',@vcDbColumnName,']')
					END
					ELSE IF @vcDbColumnName = 'vcLocation'
					BEGIN
						SET @vcSelect = CONCAT(@vcSelect,',ISNULL(WarehouseLocation.vcLocation,'''') [',@vcDbColumnName,']')
					END
					ELSE IF @vcDbColumnName = 'charItemType'
					BEGIN
						SET @vcSelect = CONCAT(@vcSelect,',(CASE 
																	WHEN Item.charItemType=''P'' 
																	THEN 
																		CASE 
																			WHEN ISNULL(Item.bitAssembly,0) = 1 THEN ''Assembly''
																			WHEN ISNULL(Item.bitKitParent,0) = 1 THEN ''Kit''
																			WHEN ISNULL(Item.bitAsset,0) = 1 AND ISNULL(Item.bitRental,0)=0 THEN ''Asset''
																			WHEN ISNULL(Item.bitAsset,0) = 1 AND ISNULL(Item.bitRental,0)=1 THEN ''Rental Asset''
																			WHEN ISNULL(Item.bitSerialized,0) = 1 AND ISNULL(Item.bitAsset,0) = 0  THEN ''Serialized''
																			WHEN ISNULL(Item.bitSerialized,0) = 1 AND ISNULL(Item.bitAsset,0) = 1 AND ISNULL(Item.bitRental,0)=0 THEN ''Serialized Asset''
																			WHEN ISNULL(Item.bitSerialized,0) = 1 AND ISNULL(Item.bitAsset,0) = 1 AND ISNULL(Item.bitRental,0)=1 THEN ''Serialized Rental Asset''
																			WHEN ISNULL(Item.bitLotNo,0)=1 THEN ''Lot #''
																			ELSE ''Inventory Item'' 
																		END
																	WHEN Item.charItemType=''N'' THEN ''Non Inventory Item'' 
																	WHEN Item.charItemType=''S'' THEN ''Service'' 
																	WHEN Item.charItemType=''A'' THEN ''Accessory'' 
																END)',' [',@vcDbColumnName,']') 
					END
					ELSE IF @vcDbColumnName = 'vcItemReleaseDate'
					BEGIN
						SET @vcSelect = CONCAT(@vcSelect,',dbo.FormatedDateFromDate(CAST(OpportunityItems.ItemReleaseDate AS DATETIME),@numDomainID) [',@vcDbColumnName,']')
					END
					ELSE IF @vcDbColumnName = 'numStatus'
					BEGIN
						SET @vcSelect = CONCAT(@vcSelect,',dbo.GetListIemName(',@vcLookBackTableName,'.',@vcDbColumnName,') [',@vcDbColumnName,']')
					END
					ELSE IF @vcDbColumnName = 'tintSource'
					BEGIN
						SET @vcSelect = CONCAT(@vcSelect,',dbo.fn_GetOpportunitySourceValue(ISNULL(OpportunityMaster.tintSource,0),ISNULL(OpportunityMaster.tintSourceType,0),OpportunityMaster.numDomainID) [',@vcDbColumnName,']')
					END
					ELSE IF @vcDbColumnName = 'vcWareHouse'
					BEGIN
						SET @vcSelect = CONCAT(@vcSelect,',(CASE WHEN ISNULL(OpportunityItems.bitRequiredWarehouseCorrection,0) = 1 THEN '''' ELSE ISNULL(Warehouses.vcWarehouse,''-'') END) [',@vcDbColumnName,']')
					END
					ELSE IF @vcDbColumnName = 'intUsedShippingCompany'
					BEGIN
						SET @vcSelect = CONCAT(@vcSelect,',CONCAT(ISNULL(LDOpp.vcData,''-''),'', '',ISNULL(SSOpp.vcShipmentService,''-''))  [',@vcDbColumnName,']')

						SET @vcGroupBy = (CASE 
											WHEN @bitGroupByOrder = 1 
											THEN CONCAT(@vcGroupBy,',LDOpp.vcData,SSOpp.vcShipmentService')
											ELSE '' 
										END)

						SET @vcFrom = CONCAT(@vcFrom ,' LEFT JOIN
															ListDetails  LDOpp
														ON
															LDOpp.numListID = 82
															AND (LDOpp.numDomainID = @numDomainID OR LDOpp.constFlag=1)
															AND ISNULL(OpportunityMaster.intUsedShippingCompany,0) = LDOpp.numListItemID
														LEFT JOIN
															ShippingService AS SSOpp
														ON
															(SSOpp.numDomainID=@numDomainID OR ISNULL(SSOpp.numDomainID,0)=0) 
															AND OpportunityMaster.numShippingService = SSOpp.numShippingServiceID')
						SET @vcFrom1 = CONCAT(@vcFrom1 ,' LEFT JOIN
															ListDetails  LDOpp
														ON
															LDOpp.numListID = 82
															AND (LDOpp.numDomainID = @numDomainID OR LDOpp.constFlag=1)
															AND ISNULL(OpportunityMaster.intUsedShippingCompany,0) = LDOpp.numListItemID
														LEFT JOIN
															ShippingService AS SSOpp
														ON
															(SSOpp.numDomainID=@numDomainID OR ISNULL(SSOpp.numDomainID,0)=0) 
															AND OpportunityMaster.numShippingService = SSOpp.numShippingServiceID')
					END
					ELSE IF @vcDbColumnName = 'intShippingCompany'
					BEGIN
						SET @vcSelect = CONCAT(@vcSelect,',CONCAT(ISNULL(LDDiv.vcData,''-''),'', '',ISNULL(SSDiv.vcShipmentService,''-''))  [',@vcDbColumnName,']')

						SET @vcGroupBy = (CASE 
											WHEN @bitGroupByOrder = 1 
											THEN CONCAT(@vcGroupBy,',LDDiv.vcData,SSDiv.vcShipmentService')
											ELSE '' 
										END)

						SET @vcFrom = CONCAT(@vcFrom ,' LEFT JOIN
															ListDetails  LDDiv
														ON
															LDDiv.numListID = 82
															AND (LDDiv.numDomainID = @numDomainID OR LDDiv.constFlag=1)
															AND ISNULL(DivisionMaster.intShippingCompany,0) = LDDiv.numListItemID
														LEFT JOIN
															ShippingService AS SSDiv
														ON
															(SSDiv.numDomainID=@numDomainID OR ISNULL(SSDiv.numDomainID,0)=0) 
															AND ISNULL(DivisionMaster.numDefaultShippingServiceID,0) = SSDiv.numShippingServiceID')

						SET @vcFrom1 = CONCAT(@vcFrom1 ,' LEFT JOIN
															ListDetails  LDDiv
														ON
															LDDiv.numListID = 82
															AND (LDDiv.numDomainID = @numDomainID OR LDDiv.constFlag=1)
															AND ISNULL(DivisionMaster.intShippingCompany,0) = LDDiv.numListItemID
														LEFT JOIN
															ShippingService AS SSDiv
														ON
															(SSDiv.numDomainID=@numDomainID OR ISNULL(SSDiv.numDomainID,0)=0) 
															AND ISNULL(DivisionMaster.numDefaultShippingServiceID,0) = SSDiv.numShippingServiceID')
					END
					ELSE IF @vcAssociatedControlType = 'SelectBox'
					BEGIN
						IF @vcListItemType = 'LI'
						BEGIN
							SET @vcSelect = CONCAT(@vcSelect,',L',CONVERT(VARCHAR(3),@j),'.vcData',' [',@vcDbColumnName,']')

							SET @vcFrom = CONCAT(@vcFrom ,' LEFT JOIN ListDetails L',@j,' ON L',@j,'.numListItemID=',@vcLookBackTableName,'.',@vcDbColumnName)
							SET @vcFrom1 = CONCAT(@vcFrom1 ,' LEFT JOIN ListDetails L',@j,' ON L',@j,'.numListItemID=',@vcLookBackTableName,'.',@vcDbColumnName)
						END
						ELSE IF @vcListItemType = 'IG'
						BEGIN
							SET @vcSelect = CONCAT(@vcSelect,',ISNULL((SELECT vcItemGroup FROM ItemGroups WHERE numItemGroupID = I.numItemGroup),'''')',' [',@vcDbColumnName,']') 
						END
						ELSE IF @vcListItemType = 'U'
						BEGIN
							SET @vcSelect = CONCAT(@vcSelect,',dbo.fn_GetContactName(',@vcLookBackTableName,'.',@vcDbColumnName,') [',@vcDbColumnName,']')
						END
						ELSE
						BEGIN
							SET @vcSelect = CONCAT(@vcSelect,',',(CASE WHEN @bitIsNumeric = 1 THEN '0' ELSE '''''' END),' [',@vcDbColumnName,']')
						END
					END
					ELSE IF @vcAssociatedControlType = 'DateField'
					BEGIN
						SET @vcSelect = CONCAT(@vcSelect,',dbo.FormatedDateFromDate(DATEADD(MINUTE,',-@ClientTimeZoneOffset,',',@vcLookBackTableName,'.',@vcDbColumnName,'),',@numDomainId,') [', @vcDbColumnName,']')
					END
					ELSE IF @vcAssociatedControlType = 'TextBox'
					BEGIN
						SET @vcSelect = CONCAT(@vcSelect,',ISNULL(',@vcLookBackTableName,'.',@vcDbColumnName,(CASE WHEN @bitIsNumeric = 1 THEN ',0' ELSE ',''''' END),') [',@vcDbColumnName,']')
					END
					ELSE IF @vcAssociatedControlType = 'TextArea'
					BEGIN
						SET @vcSelect = CONCAT(@vcSelect,',ISNULL(',@vcLookBackTableName,'.',@vcDbColumnName,(CASE WHEN @bitIsNumeric = 1 THEN ',0' ELSE ',''''' END),') [',@vcDbColumnName,']')
					END
					ELSE IF @vcAssociatedControlType = 'Label'
					BEGIN
						SET @vcSelect = CONCAT(@vcSelect,',ISNULL(',@vcLookBackTableName,'.',@vcDbColumnName,(CASE WHEN @bitIsNumeric = 1 THEN ',0' ELSE '''''' END),') [',@vcDbColumnName,']')
					END
					ELSE
					BEGIN
						SET @vcSelect = CONCAT(@vcSelect,',ISNULL(',@vcLookBackTableName,'.',@vcDbColumnName,(CASE WHEN @bitIsNumeric = 1 THEN ',0' ELSE '''''' END),') [',@vcDbColumnName,']')
					END
				END
			END
			ELSE IF @bitCustomField = 1
			BEGIN
				IF @Grp_id = 5
				BEGIN
					IF ISNULL(@bitGroupByOrder,0) = 1
					BEGIN
						SET @vcSelect = CONCAT(@vcSelect,',''''',' [',@vcDbColumnName,']')
					END
					ELSE
					BEGIN
						SET @vcSelect = CONCAT(@vcSelect,CONCAT(',dbo.GetCustFldValueOppItems(',@numFieldId,',OpportunityItems.numoppitemtCode,Item.numItemCode)'),' [',@vcDbColumnName,']')
					END
				END
				ELSE
				BEGIN
					IF @vcAssociatedControlType = 'TextBox' OR @vcAssociatedControlType = 'TextArea' 
					BEGIN
						SET @vcSelect = CONCAT(@vcSelect,',CFW',@numFieldId,'.Fld_Value  [',@vcDbColumnName,']')
             
						SET @vcText = CONCAT(' left Join CFW_FLD_Values_Opp CFW',@numFieldId
										,' ON CFW' , @numFieldId , '.Fld_Id='
										,@numFieldId
										, ' and CFW' , @numFieldId
										, '.RecId=OpportunityMaster.numOppID')

						SET @vcFrom = CONCAT(@vcFrom,@vcText)
						SET @vcFrom1 = CONCAT(@vcFrom1,@vcText)
					END   
					ELSE IF @vcAssociatedControlType = 'CheckBox' 
					BEGIN
						SET @vcSelect =CONCAT( @vcSelect
							, ',case when isnull(CFW'
							, @numFieldId
							, '.Fld_Value,0)=0 then 0 when isnull(CFW'
							, @numFieldId
							, '.Fld_Value,0)=1 then 1 end   ['
							,  @vcDbColumnName
							, ']')
							
						SET @vcText = CONCAT(' left Join CFW_FLD_Values_Opp CFW'
							, @numFieldId
							, ' ON CFW',@numFieldId,'.Fld_Id='
							, @numFieldId
							, ' and CFW'
							, @numFieldId
							, '.RecId=OpportunityMaster.numOppID')

						SET @vcFrom = CONCAT(@vcFrom,@vcText)
						SET @vcFrom1 = CONCAT(@vcFrom1,@vcText)                                                 
					END                
					ELSE IF @vcAssociatedControlType = 'DateField' 
					BEGIN
						SET @vcSelect = CONCAT(@vcSelect
							, ',dbo.FormatedDateFromDate(CFW'
							, @numFieldId
							, '.Fld_Value,'
							, @numDomainId
							, ')  [',@vcDbColumnName ,']' )  
					                  
						SET @vcText = CONCAT(' left Join CFW_FLD_Values_Opp CFW'
							, @numFieldId
							, ' on CFW', @numFieldId, '.Fld_Id='
							, @numFieldId
							, ' and CFW'
							, @numFieldId
							, '.RecId=OpportunityMaster.numOppID')

						SET @vcFrom = CONCAT(@vcFrom,@vcText)
						SET @vcFrom1 = CONCAT(@vcFrom1,@vcText)                                                 
					END                
					ELSE IF @vcAssociatedControlType = 'SelectBox' 
					BEGIN
						SET @vcSelect = CONCAT(@vcSelect,',L',@numFieldId,'.vcData',' [',@vcDbColumnName,']')
					        
						SET @vcText = CONCAT(' left Join CFW_FLD_Values_Opp CFW'
							, @numFieldId
							, ' on CFW',@numFieldId ,'.Fld_Id='
							, @numFieldId
							, ' and CFW'
							, @numFieldId
							, '.RecId=OpportunityMaster.numOppID')

						SET @vcFrom = CONCAT(@vcFrom,@vcText)
						SET @vcFrom1 = CONCAT(@vcFrom1,@vcText)

						SET @vcText = CONCAT(' left Join ListDetails L'
							, @numFieldId
							, ' on L'
							, @numFieldId
							, '.numListItemID=CFW'
							, @numFieldId
							, '.Fld_Value')

						SET @vcFrom = CONCAT(@vcFrom,@vcText)
						SET @vcFrom1 = CONCAT(@vcFrom1,@vcText)            
					END
					ELSE
					BEGIN
						SET @vcSelect = CONCAT(@vcSelect,CONCAT(',dbo.GetCustFldValueOpp(',@numFieldId,',OpportunityMaster.numOppID)'),' [',@vcDbColumnName,']')
					END 
				END
			END
		END

		SET @j = @j + 1
	END

	PRINT CAST(@vcSelect AS NTEXT)
	PRINT CAST(@vcFrom AS NTEXT)
	PRINT CAST(@vcWhere AS NTEXT)
	PRINT CAST(@vcGroupBy AS NTEXT)
	PRINT CAST(@vcOrderBy AS NTEXT)

	DECLARE @vcFinal NVARCHAR(MAX) = ''

	If @numViewID = 2 AND @tintPackingViewMode = 3
	BEGIN
		SET @vcSelect = CONCAT(@vcSelect,',',(CASE @vcSortColumn
													WHEN 'CompanyInfo.vcCompanyName' THEN 'CompanyInfo.vcCompanyName AS vcCompanyNameOrig' 
													WHEN 'Item.charItemType' THEN 'Item.charItemType AS charItemTypeOrig'
													WHEN 'Item.numBarCodeId' THEN 'Item.numBarCodeId AS numBarCodeIdOrig'
													WHEN 'Item.numItemClassification' THEN 'Item.numItemClassification AS numItemClassificationOrig'
													WHEN 'Item.numItemGroup' THEN 'Item.numItemGroup AS numItemGroupOrig'
													WHEN 'Item.vcSKU' THEN 'Item.vcSKU AS vcSKUOrig'
													WHEN 'OpportunityItems.vcItemName' THEN 'Item.vcItemName AS vcItemNameOrig'
													WHEN 'OpportunityMaster.numStatus' THEN 'OpportunityMaster.numStatus AS numStatusOrig '
													WHEN 'OpportunityMaster.vcPoppName' THEN 'OpportunityMaster.vcPoppName AS vcPoppNameOrig'
													WHEN 'OpportunityMaster.numAssignedTo' THEN 'OpportunityMaster.numAssignedTo AS numAssignedToOrig'
													WHEN 'OpportunityMaster.numRecOwner' THEN 'OpportunityMaster.numRecOwner AS numRecOwnerOrig'
													WHEN 'WareHouseItems.vcLocation' THEN 'WarehouseLocation.vcLocation AS vcLocationOrig'
													WHEN 'OpportunityBizDocs.monAmountPaid' THEN 'TempPaid.monAmountPaid AS monAmountPaidOrig'
													WHEN 'OpportunityItems.numQtyPicked' THEN 'OpportunityItems.numQtyPicked AS numQtyPickedOrig'
													WHEN 'OpportunityItems.numQtyShipped' THEN 'OpportunityItems.numQtyShipped AS numQtyShippedOrig'
													WHEN 'OpportunityItems.numRemainingQty' THEN 'OpportunityItems.numUnitHour AS numUnitHourOrig,OpportunityItems.numQtyPicked AS numQtyPickedOrig'
													WHEN 'OpportunityItems.numUnitHour' THEN 'OpportunityItems.numUnitHour AS numUnitHourOrig'
													WHEN 'OpportunityMaster.dtExpectedDate' THEN 'OpportunityMaster.dtExpectedDate AS dtExpectedDateOrig'
													WHEN 'OpportunityItems.vcItemReleaseDate' THEN 'OpportunityItems.ItemReleaseDate AS ItemReleaseDateOrig'
													WHEN 'OpportunityMaster.bintCreatedDate' THEN 'OpportunityMaster.bintCreatedDate AS bintCreatedDateOrig'
													WHEN 'OpportunityMaster.numAge' THEN 'OpportunityMaster.bintCreatedDate AS bintCreatedDateOrig'
													WHEN 'OpportunityMaster.bitPaidInFull' THEN 'TempPaid.monAmountPaid AS monAmountPaidOrig, OpportunityMaster.monDealAmount AS monDealAmountOrig'
													WHEN 'Item.dtItemReceivedDate' THEN 'Item.dtItemReceivedDate AS dtItemReceivedDateOrig'
													WHEN 'Warehouses.vcWareHouse' THEN '(CASE WHEN ISNULL(OpportunityItems.bitRequiredWarehouseCorrection,0) = 1 THEN '''' ELSE Warehouses.vcWareHouse END)'
													ELSE 'OpportunityMaster.bintCreatedDate AS bintCreatedDateOrig'
												END))

		DECLARE @vcGroupBy1 NVARCHAR(MAX) = (CASE 
											WHEN @bitGroupByOrder = 1 
											THEN CONCAT('GROUP BY TEMP.numOppId
																	,TEMP.numDivisionID
																	,TEMP.numTerID
																	,TEMP.intShippingCompany
																	,TEMP.numDefaultShippingServiceID
																	,TEMP.numOppBizDocsId
																	,TEMP.monDealAmount
																	,TEMP.monAmountPaid
																	,',(CASE WHEN EXISTS (SELECT ID FROM @TempFieldsLeft WHERE vcLookBackTableName='CompanyInfo' AND vcOrigDbColumnName='vcCompanyName') THEN 'TEMP.vcCompanyName' ELSE '' END),'
																	,',(CASE WHEN EXISTS (SELECT ID FROM @TempFieldsLeft WHERE vcLookBackTableName='OpportunityMaster' AND vcOrigDbColumnName='vcPoppName') THEN 'TEMP.vcPoppName' ELSE '' END),'
																	,',(CASE WHEN EXISTS (SELECT ID FROM @TempFieldsLeft WHERE vcLookBackTableName='OpportunityMaster' AND vcOrigDbColumnName='numAssignedTo') THEN 'TEMP.numAssignedTo' ELSE '' END),'.
																	,',(CASE WHEN EXISTS (SELECT ID FROM @TempFieldsLeft WHERE vcLookBackTableName='OpportunityMaster' AND vcOrigDbColumnName='numRecOwner') THEN 'TEMP.numRecOwner' ELSE '' END),'.
																	,',(CASE WHEN EXISTS (SELECT ID FROM @TempFieldsLeft WHERE vcLookBackTableName='OpportunityMaster' AND vcOrigDbColumnName='numStatus') THEN 'TEMP.numStatus' ELSE '' END),'.
																	,',(CASE WHEN EXISTS (SELECT ID FROM @TempFieldsLeft WHERE vcLookBackTableName='OpportunityMaster' AND vcOrigDbColumnName='bintCreatedDate') THEN 'TEMP.bintCreatedDate' ELSE '' END),'.
																	,',(CASE WHEN EXISTS (SELECT ID FROM @TempFieldsLeft WHERE vcLookBackTableName='OpportunityMaster' AND vcOrigDbColumnName='txtComments') THEN 'TEMP.txtComments' ELSE '' END),'.
																	,',(CASE WHEN EXISTS (SELECT ID FROM @TempFieldsLeft WHERE vcLookBackTableName='OpportunityBizDocs' AND vcOrigDbColumnName='vcBizDocID') THEN 'TEMP.vcBizDocID' ELSE '' END),'.
																	,',(CASE WHEN EXISTS (SELECT ID FROM @TempFieldsLeft WHERE vcLookBackTableName='OpportunityMaster' AND vcOrigDbColumnName='intUsedShippingCompany') THEN 'TEMP.intUsedShippingCompany' ELSE '' END),'.
																	,',(CASE WHEN EXISTS (SELECT ID FROM @TempFieldsLeft WHERE vcLookBackTableName='DivisionMaster' AND vcOrigDbColumnName='intShippingCompany') THEN 'TEMP.intShippingCompany' ELSE '' END),'.
																	,',(CASE WHEN EXISTS (SELECT ID FROM @TempFieldsLeft WHERE vcLookBackTableName='OpportunityBizDocs' AND vcOrigDbColumnName='dtCreatedDate') THEN 'TEMP.dtCreatedDate' ELSE '' END),'.
																	,',(CASE WHEN EXISTS (SELECT ID FROM @TempFieldsLeft WHERE vcLookBackTableName='OpportunityMaster' AND vcOrigDbColumnName='dtExpectedDate') THEN 'TEMP.dtExpectedDate' ELSE '' END),'.
																	,',(CASE WHEN EXISTS (SELECT ID FROM @TempFieldsLeft WHERE vcLookBackTableName='Item' AND vcOrigDbColumnName='dtItemReceivedDate') THEN 'TEMP.dtItemReceivedDateOrig' ELSE '' END),'.')
											ELSE '' 
										END)
		SET @vcFinal= CONCAT('SELECT COUNT(*) OVER() AS TotalRecords,* FROM (',@vcSelect,@vcFrom,@vcWhere,' UNION ',@vcSelect,@vcFrom1,@vcWhere1,') TEMP',@vcGroupBy1,@vcOrderBy1,' OFFSET (@numPageIndex-1) * 200 ROWS FETCH NEXT 200 ROWS ONLY OPTION (OPTIMIZE FOR (@numDomainID UNKNOWN))')
	END
	ELSE
	BEGIN
		SET @vcFinal = CONCAT(@vcSelect,',COUNT(*) OVER() AS TotalRecords',@vcFrom,@vcWhere,@vcGroupBy,@vcOrderBy,' OFFSET (@numPageIndex-1) * 200 ROWS FETCH NEXT 200 ROWS ONLY OPTION (OPTIMIZE FOR (@numDomainID UNKNOWN))')
	END

	EXEC sp_executesql @vcFinal, N'@numDomainID NUMERIC(18,0), @vcOrderStauts VARCHAR(MAX), @vcFilterValue VARCHAR(MAX), @tintFilterBy TINYINT, @numViewID NUMERIC(18,0), @tintPackingViewMode TINYINT, @numShippingServiceItemID NUMERIC(18,0), @numWarehouseID NUMERIC(18,0), @numShippingZone NUMERIC(18,0), @numDefaultSalesShippingDoc NUMERIC(18,0), @tintInvoicingType TINYINT, @tintPackingMode TINYINT, @bitRemoveBO BIT, @ClientTimeZoneOffset INT,@numBatchID NUMERIC(18,0),@tintPrintBizDocViewMode TINYINT,@tintPendingCloseFilter TINYINT, @bitIncludeSearch BIT, @bitEnablePickListMapping BIT, @bitEnableFulfillmentBizDocMapping BIT,@numPageIndex INT', @numDomainID, @vcOrderStauts, @vcFilterValue, @tintFilterBy, @numViewID, @tintPackingViewMode, @numShippingServiceItemID, @numWarehouseID, @numShippingZone, @numDefaultSalesShippingDoc, @tintInvoicingType, @tintPackingMode, @bitRemoveBO, @ClientTimeZoneOffset,@numBatchID,@tintPrintBizDocViewMode,@tintPendingCloseFilter,@bitIncludeSearch,@bitEnablePickListMapping, @bitEnableFulfillmentBizDocMapping,@numPageIndex;

	SELECT * FROM @TempFieldsLeft ORDER BY intRowNum
END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_MassSalesFulfillment_GetRecordsForPacking')
DROP PROCEDURE dbo.USP_MassSalesFulfillment_GetRecordsForPacking
GO

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_MassSalesFulfillment_GetRecordsForPacking]
(
	@numDomainID NUMERIC(18,0)
	,@numUserCntID NUMERIC(18,0)
	,@numOppID NUMERIC(18,0)
	,@numOppItemID NUMERIC(18,0)
)
AS
BEGIN
	DECLARE @tintPackingMode TINYINT
	DECLARE @numDefaultSalesShippingDoc NUMERIC(18,0)
	DECLARE @numShippingServiceItemID NUMERIC(18,0)

	SELECT 
		@numDefaultSalesShippingDoc=ISNULL(numDefaultSalesShippingDoc,0)
		,@numShippingServiceItemID = ISNULL(numShippingServiceItemID,0)
	FROM 
		Domain 
	WHERE 
		numDomainId=@numDomainID 

	SELECT
		@tintPackingMode=tintPackingMode
	FROM
		MassSalesFulfillmentConfiguration
	WHERE
		numDomainID = @numDomainID

	SELECT 
		OM.numOppID
		,ISNULL(OM.intUsedShippingCompany,ISNULL(DM.intShippingCompany,0)) numShipVia
		,ISNULL(OM.numShippingService,ISNULL(DM.numDefaultShippingServiceID,0)) numShipService
		,OM.vcPOppName
		,I.numContainer
		,ISNULL(IContainer.vcItemName,'') vcContainer
		,(CASE WHEN ISNULL(I.numContainer,0) > 0 THEN I.numNoItemIntoContainer ELSE OI.numUnitHour END) AS numNoItemIntoContainer
		,ISNULL(IContainer.fltWeight,0) fltContainerWeight
		,ISNULL(IContainer.fltHeight,0) fltContainerHeight
		,ISNULL(IContainer.fltWidth,0) fltContainerWidth
		,ISNULL(IContainer.fltLength,0) fltContainerLength
		,OI.numoppitemtCode
		,I.numItemCode
		,I.vcItemName
		,ISNULL(I.fltWeight,0) fltItemWeight
		,ISNULL(I.fltHeight,0) fltItemHeight
		,ISNULL(I.fltWidth,0) fltItemWidth
		,ISNULL(I.fltLength,0) fltItemLength
		,ISNULL((SELECT TOP 1 monPrice FROM OpportunityItems OIInner WHERE OIInner.numOppID=OM.numOppID AND numItemCode=@numShippingServiceItemID),-1) monCurrentShipRate
		,(CASE WHEN ISNULL(@tintPackingMode,1) = 2 THEN ISNULL(OI.numQtyPicked,0) ELSE ISNULL(OI.numUnitHour,0) END) - ISNULL((SELECT 
																																	SUM(OpportunityBizDocItems.numUnitHour)
																																FROM
																																	OpportunityBizDocs
																																INNER JOIN
																																	OpportunityBizDocItems
																																ON
																																	OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																																WHERE
																																	OpportunityBizDocs.numOppId = OI.numOppID
																																	AND OpportunityBizDocs.numBizDocId=@numDefaultSalesShippingDoc
																																	AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtcode),0) numTotalQty
	FROM 
		OpportunityMaster OM
	INNER JOIN
		DivisionMaster DM
	ON
		OM.numDivisionId = DM.numDivisionID
	INNER JOIN
		OpportunityItems OI
	ON
		OM.numOppId = OI.numOppId
	INNER JOIN 
		Item AS I
	ON 
		OI.numItemCode = I.numItemCode
	LEFT JOIN
		Item AS IContainer
	ON
		I.numContainer = IContainer.numItemCode
	WHERE
		OM.numOppId=@numOppID
		AND I.charItemType ='P'
		AND ISNULL(I.bitContainer,0) = 0
		AND (OI.numoppitemtCode=@numOppItemID OR ISNULL(@numOppItemID,0) = 0)
END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_MassSalesFulfillment_UpdateOrderStauts')
DROP PROCEDURE dbo.USP_MassSalesFulfillment_UpdateOrderStauts
GO

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_MassSalesFulfillment_UpdateOrderStauts]
(
	@numDomainID NUMERIC(18,0)
	,@numUserCntID NUMERIC(18,0)
	,@numOppID NUMERIC(18,0)
	,@tintViewID TINYINT
	,@tintPackingMode TINYINT
)
AS
BEGIN
	DECLARE @numStatus NUMERIC(18,0) = 0
	IF EXISTS (SELECT ID FROM MassSalesFulfillmentConfiguration WHERE numDomainID=@numDomainID)
	BEGIN
		SELECT 
			@numStatus = (CASE 
								WHEN @tintViewID = 1 THEN ISNULL(numOrderStatusPicked,0) 
								WHEN @tintViewID = 2 AND @tintPackingMode = 1 THEN ISNULL(numOrderStatusPacked1,0)
								WHEN @tintViewID = 2 AND @tintPackingMode = 2 THEN ISNULL(numOrderStatusPacked2,0)
								WHEN @tintViewID = 2 AND @tintPackingMode = 3 THEN ISNULL(numOrderStatusPacked3,0)
								WHEN @tintViewID = 2 AND @tintPackingMode = 4 THEN ISNULL(numOrderStatusPacked4,0)
								WHEN @tintViewID = 3 THEN ISNULL(numOrderStatusInvoiced,0)
								WHEN @tintViewID = 4 THEN ISNULL(numOrderStatusPaid,0)
								WHEN @tintViewID = 5 THEN ISNULL(numOrderStatusClosed,0)
								ELSE 0
							END) 
		FROM 
			MassSalesFulfillmentConfiguration 
		WHERE 
			numDomainID=@numDomainID
	END

	DECLARE @canUpdateStatus AS BIT = 0

	IF @tintViewID = 1 AND NOT EXISTS (SELECT
											numOppItemtcode
										FROM
											OpportunityItems
										WHERE
											numOppId = @numOppID
											AND ISNULL(numWarehouseItmsID,0) > 0
											AND ISNULL(bitDropShip,0) = 0
											AND ISNULL(numUnitHour,0) <> ISNULL(numQtyPicked,0))
	BEGIN
		SET @canUpdateStatus = 1
	END
	ELSE IF @tintViewID = 2 AND @tintPackingMode = 1
	BEGIN
		SET @canUpdateStatus = 1
	END
	ELSE IF @tintViewID = 2 AND @tintPackingMode = 2 AND NOT EXISTS (SELECT
																		numOppItemtcode
																	FROM
																		OpportunityItems
																	WHERE
																		numOppId = @numOppID
																		AND ISNULL(numWarehouseItmsID,0) > 0
																		AND ISNULL(bitDropShip,0) = 0
																		AND ISNULL(numUnitHour,0) <> ISNULL((SELECT 
																												SUM(numUnitHour) 
																											FROM 
																												OpportunityBizDocs OBD 
																											INNER JOIN 
																												OpportunityBizDocItems OBDI 
																											ON
																												OBD.numOppBizDocsId = OBDI.numOppBizDocID  
																											WHERE 
																												OBD.numOppId = OpportunityItems.numOppId 
																						AND OBD.numBizDocId=296
																						AND OBDI.numOppItemID=OpportunityItems.numoppitemtCode),0))
	BEGIN
		SET @canUpdateStatus = 1
	END
	ELSE IF @tintViewID = 2 AND @tintPackingMode = 4 AND NOT EXISTS (SELECT
																		numOppItemtcode
																	FROM
																		OpportunityItems
																	WHERE
																		numOppId = @numOppID
																		AND ISNULL(numWarehouseItmsID,0) > 0
																		AND ISNULL(bitDropShip,0) = 0
																		AND ISNULL(numUnitHour,0) <> ISNULL((SELECT 
																												SUM(SRI.intBoxQty) 
																											FROM 
																												ShippingReport SR
																											INNER JOIN
																												ShippingBox SB
																											ON
																												SR.numShippingReportId = SB.numShippingReportId
																											INNER JOIN
																												ShippingReportItems SRI
																											ON
																												SB.numBoxID = SRI.numBoxID 
																											INNER JOIN 
																												OpportunityBizDocItems OBDI 
																											ON
																												SRI.numOppBizDocItemID = OBDI.numOppBizDocItemID  
																											WHERE 
																												SR.numOppId = OpportunityItems.numOppId 
																												AND OBDI.numOppItemID=OpportunityItems.numoppitemtCode),0))
	BEGIN
		SET @canUpdateStatus = 1
	END
	ELSE IF @tintViewID = 3 AND NOT EXISTS (SELECT
												numOppItemtcode
											FROM
												OpportunityItems
											WHERE
												numOppId = @numOppID
												AND ISNULL(numUnitHour,0) <> ISNULL((SELECT 
																						SUM(numUnitHour) 
																					FROM 
																						OpportunityBizDocs OBD 
																					INNER JOIN 
																						OpportunityBizDocItems OBDI 
																					ON
																						OBD.numOppBizDocsId = OBDI.numOppBizDocID  
																					WHERE 
																						OBD.numOppId = OpportunityItems.numOppId 
																						AND OBD.numBizDocId=287
																						AND OBDI.numOppItemID=OpportunityItems.numoppitemtCode),0))
	BEGIN
		SET @canUpdateStatus = 1
	END
	ELSE IF @tintViewID = 4 AND EXISTS (SELECT 
											numOppId 
										FROM 
											OpportunityMaster 
										WHERE 
											numOppId=@numOppID 
											AND monDealAmount = ISNULL((SELECT SUM(ISNULL(monAmountPaid,0)) FROM OpportunityBizDocs BD WHERE  BD.numOppId = OpportunityMaster.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)) 
	BEGIN
		SET @canUpdateStatus = 1
	END
	ELSE IF @tintViewID = 5
	BEGIN
		SET @canUpdateStatus = 1
	END
	

	IF ISNULL(@numStatus,0) > 0 AND @canUpdateStatus = 1
	BEGIN
		UPDATE
			OpportunityMaster
		SET
			numStatus=@numStatus
		WHERE
			numDomainId=@numDomainID
			AND numOppId = @numOppID
	END
END
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_MassSalesFulfillment_UpdateShippingRate')
DROP PROCEDURE dbo.USP_MassSalesFulfillment_UpdateShippingRate
GO

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_MassSalesFulfillment_UpdateShippingRate]
(
	@numDomainID NUMERIC(18,0)
	,@numUserCntID NUMERIC(18,0)
	,@vcSelectedRecords VARCHAR(MAX)
)
AS
BEGIN
	DECLARE @numStatus NUMERIC(18,0) = 0
	IF EXISTS (SELECT ID FROM MassSalesFulfillmentConfiguration WHERE numDomainID=@numDomainID)
	BEGIN
		SELECT @numStatus = ISNULL(numOrderStatusPacked1,0) FROM MassSalesFulfillmentConfiguration WHERE numDomainID=@numDomainID
	END
	
	DECLARE @bitSuccessful BIT = 1
	DECLARE @hDocItem INT
	DECLARE @numShippingServiceItemID NUMERIC(18,0)

	SELECT 
		@numShippingServiceItemID=ISNULL(numShippingServiceItemID,0)
	FROM 
		Domain 
	WHERE 
		numDomainId=@numDomainID 

	IF NOT EXISTS (SELECT numItemCode FROM Item WHERE numDomainID=@numDomainID AND numItemCode=@numShippingServiceItemID)
	BEGIN
		RAISERROR('SHIPPING_ITEM_NOT_SET',16,1)
		RETURN
	END

	DECLARE @Temp TABLE
	(
		ID INT IDENTITY(1,1)
		,numOppID NUMERIC(18,0)
		,numShipVia NUMERIC(18,0)
		,numShipService NUMERIC(18,0)
		,monShipRate DECIMAL(20,5)
	)

	EXEC sp_xml_preparedocument @hDocItem OUTPUT, @vcSelectedRecords

	INSERT INTO @Temp
	(
		numOppID
		,numShipVia
		,numShipService
		,monShipRate
	)
	SELECT
		ISNULL(OppID,0)
		,ISNULL(PreferredShipVia,0)
		,ISNULL(PreferredShipService,0)
		,ISNULL(ShipRate,0)
	FROM
		OPENXML (@hDocItem,'/NewDataSet/Table1',2)
	WITH
	(
		OppID  NUMERIC(18,0)
		,PreferredShipVia NUMERIC(18,0)
		,PreferredShipService NUMERIC(18,0)
		,ShipRate DECIMAL(20,5)
	)

	EXEC sp_xml_removedocument @hDocItem 


	DECLARE @i INT = 1
	DECLARE @iCount INT
	DECLARE @numOppID NUMERIC(18,0)
	DECLARE @numShipVia NUMERIC(18,0)
	DECLARE @numShipService NUMERIC(18,0)
	DECLARE @vcShipService VARCHAR(300)
	DECLARE @monShipRate DECIMAL(20,5)

	SET @iCount = (SELECT COUNT(*) FROM @TEMP)

	WHILE @i <= @iCount
	BEGIN
		SELECT
			@numOppID=numOppID
			,@numShipVia=numShipVia
			,@numShipService=numShipService
			,@monShipRate=monShipRate
		FROM 
			@TEMP 
		WHERE 
			ID=@i

		SET @vcShipService = ISNULL((SELECT TOP 1 vcShipmentService FROM ShippingService WHERE (numDomainID=@numDomainID OR ISNULL(numDomainID,0)=0) AND numShippingServiceID = @numShipService),'')

		BEGIN TRY
		BEGIN TRANSACTION
			IF EXISTS(SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId=@numOppID AND numItemCode=@numShippingServiceItemID)
			BEGIN
				UPDATE 
					OpportunityItems 
				SET 
					numUnitHour=1
					,monPrice=@monShipRate
					,monTotAmount=@monShipRate
					,monTotAmtBefDiscount=@monShipRate
					,fltDiscount=0
					,vcItemDesc = @vcShipService
				WHERE 
					numOppId=@numOppID 
					AND numItemCode=@numShippingServiceItemID
			END
			ELSE 
			BEGIN
				INSERT INTO OpportunityItems
				(
					numOppId
					,numItemCode
					,numUnitHour
					,monPrice
					,monTotAmount
					,monTotAmtBefDiscount
					,bitDiscountType
					,fltDiscount
					,vcItemName
					,vcItemDesc
					,vcSKU
					,vcManufacturer
					,vcModelID
				)
				SELECT 
					@numOppID
					,@numShippingServiceItemID
					,1
					,@monShipRate
					,@monShipRate
					,@monShipRate
					,1
					,0
					,vcItemName
					,@vcShipService
					,vcSKU
					,vcManufacturer
					,vcModelID
				FROM
					Item
				WHERE
					numDomainID=@numDomainID
					AND numItemCode = @numShippingServiceItemID
			END

			UPDATE 
				OpportunityMaster 
			SET 
				intUsedShippingCompany=@numShipVia
				,numShippingService=@numShipService
				,numModifiedBy=@numUserCntID
				,bintModifiedDate=GETUTCDATE()
				,monPAmount = (SELECT SUM(monTotAmount) FROM OpportunityItems WHERE numOppId=@numOppID)
				,numStatus = (CASE WHEN ISNULL(@numStatus,0) > 0 THEN @numStatus ELSE numStatus END)
			WHERE 
				numDomainID=@numDomainID 
				AND numOppID=@numOppID

		COMMIT
		END TRY
		BEGIN CATCH
			IF @@TRANCOUNT > 0
				ROLLBACK TRANSACTION;

			SET @bitSuccessful = 0
		END CATCH
			
		SET @i = @i + 1
	END

	SELECT @bitSuccessful
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_MassSalesFulfillmentConfiguration_Save')
DROP PROCEDURE dbo.USP_MassSalesFulfillmentConfiguration_Save
GO
CREATE PROCEDURE [dbo].[USP_MassSalesFulfillmentConfiguration_Save]
(
	@numDomainID NUMERIC(18,0)
	,@bitGroupByOrderForPick BIT
	,@bitGroupByOrderForShip BIT
	,@bitGroupByOrderForInvoice BIT
	,@bitGroupByOrderForPay BIT
	,@bitGroupByOrderForClose BIT
	,@tintInvoicingType TINYINT
	,@tintScanValue TINYINT
	,@tintPackingMode TINYINT
	,@tintPendingCloseFilter TINYINT
	,@bitGeneratePickListByOrder BIT
	,@numOrderStatusPicked NUMERIC(18,0)
	,@numOrderStatusPacked1 NUMERIC(18,0)
	,@numOrderStatusPacked2 NUMERIC(18,0)
	,@numOrderStatusPacked3 NUMERIC(18,0)
	,@numOrderStatusPacked4 NUMERIC(18,0)
	,@numOrderStatusInvoiced NUMERIC(18,0)
	,@numOrderStatusPaid NUMERIC(18,0)
	,@numOrderStatusClosed NUMERIC(18,0)
	,@bitPickWithoutInventoryCheck BIT
	,@bitEnablePickListMapping BIT
	,@bitEnableFulfillmentBizDocMapping BIT
	,@bitAutoWarehouseSelection BIT
	,@bitBOOrderStatus BIT
)
AS 
BEGIN
	IF EXISTS (SELECT ID FROM MassSalesFulfillmentConfiguration WHERE numDomainID=@numDomainID)
	BEGIN
		UPDATE
			MassSalesFulfillmentConfiguration
		SET
			bitGroupByOrderForPick=@bitGroupByOrderForPick
			,bitGroupByOrderForShip=@bitGroupByOrderForShip
			,bitGroupByOrderForInvoice=@bitGroupByOrderForInvoice
			,bitGroupByOrderForPay=@bitGroupByOrderForPay
			,bitGroupByOrderForClose=@bitGroupByOrderForClose
			,tintInvoicingType=@tintInvoicingType
			,tintScanValue=@tintScanValue
			,tintPackingMode=@tintPackingMode
			,tintPendingCloseFilter=@tintPendingCloseFilter
			,bitGeneratePickListByOrder=@bitGeneratePickListByOrder
			,numOrderStatusPicked=@numOrderStatusPicked
			,numOrderStatusPacked1=@numOrderStatusPacked1
			,numOrderStatusPacked2=@numOrderStatusPacked2
			,numOrderStatusPacked3=@numOrderStatusPacked3
			,numOrderStatusPacked4=@numOrderStatusPacked4
			,numOrderStatusInvoiced=@numOrderStatusInvoiced
			,numOrderStatusPaid=@numOrderStatusPaid
			,numOrderStatusClosed=@numOrderStatusClosed
			,bitPickWithoutInventoryCheck=@bitPickWithoutInventoryCheck
			,bitEnablePickListMapping=@bitEnablePickListMapping
			,bitEnableFulfillmentBizDocMapping=@bitEnableFulfillmentBizDocMapping
			,bitAutoWarehouseSelection=@bitAutoWarehouseSelection
			,bitBOOrderStatus=@bitBOOrderStatus
		WHERE
			numDomainID=@numDomainID 
	END
	ELSE
	BEGIN
		INSERT INTO MassSalesFulfillmentConfiguration
		(
			numDomainID 
			,bitGroupByOrderForPick
			,bitGroupByOrderForShip
			,bitGroupByOrderForInvoice
			,bitGroupByOrderForPay
			,bitGroupByOrderForClose
			,tintInvoicingType
			,tintScanValue
			,tintPackingMode
			,tintPendingCloseFilter
			,bitGeneratePickListByOrder
			,numOrderStatusPicked
			,numOrderStatusPacked1
			,numOrderStatusPacked2
			,numOrderStatusPacked3
			,numOrderStatusPacked4
			,numOrderStatusInvoiced
			,numOrderStatusPaid
			,numOrderStatusClosed
			,bitPickWithoutInventoryCheck
			,bitEnablePickListMapping
			,bitEnableFulfillmentBizDocMapping
			,bitAutoWarehouseSelection
			,bitBOOrderStatus
		)
		VALUES
		(
			@numDomainID 
			,@bitGroupByOrderForPick
			,@bitGroupByOrderForShip
			,@bitGroupByOrderForInvoice
			,@bitGroupByOrderForPay
			,@bitGroupByOrderForClose
			,@tintInvoicingType
			,@tintScanValue
			,@tintPackingMode
			,@tintPendingCloseFilter
			,@bitGeneratePickListByOrder
			,@numOrderStatusPicked
			,@numOrderStatusPacked1
			,@numOrderStatusPacked2
			,@numOrderStatusPacked3
			,@numOrderStatusPacked4
			,@numOrderStatusInvoiced
			,@numOrderStatusPaid
			,@numOrderStatusClosed
			,@bitPickWithoutInventoryCheck
			,@bitEnablePickListMapping
			,@bitEnableFulfillmentBizDocMapping
			,@bitAutoWarehouseSelection
			,@bitBOOrderStatus
		)
	END
END
GO
/****** Object:  StoredProcedure [dbo].[USP_OPPDetails]    Script Date: 03/25/2009 15:10:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Created By Anoop Jayaraj
-- [dbo].[USP_OPPDetails] 1362,1,-333
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_oppdetails')
DROP PROCEDURE usp_oppdetails
GO
CREATE PROCEDURE [dbo].[USP_OPPDetails]
(
               @numOppID             AS NUMERIC(9)  = NULL,
               @numDomainID          AS NUMERIC(9)  = 0,
               @ClientTimeZoneOffset AS INT)
AS
BEGIN	
  SELECT Opp.numoppid,
         numCampainID,
         ADC.vcFirstname + ' ' + ADC.vcLastName AS numContactId,
         isnull(ADC.vcEmail,'') AS vcEmail,
         isnull(ADC.numPhone,'') AS Phone,
         isnull(ADC.numPhoneExtension,'') AS PhoneExtension,
         Opp.txtComments,
         Opp.numDivisionID,
         tintOppType,
         ISNULL(dateadd(MINUTE,-@ClientTimeZoneOffset,bintAccountClosingDate),'') AS bintAccountClosingDate,
         Opp.numContactID AS ContactID,
         tintSource,
		 tintSourceType,
         C2.vcCompanyName + Case when isnull(D2.numCompanyDiff,0)>0 then  '  ' + dbo.fn_getlistitemname(D2.numCompanyDiff) + ':' + isnull(D2.vcCompanyDiff,'') else '' end as vcCompanyname,
		 (SELECT TOP 1 ISNULL(vcData,'') FROM ListDetails WHERE numListID=5 AND numListItemID=C2.numCompanyType AND numDomainId=@numDomainID) AS vcCompanyType,
         D2.tintCRMType,
         Opp.vcPoppName,
         intpEstimatedCloseDate,
         [dbo].[GetDealAmount](@numOppID,getutcdate(),0) AS monPAmount,
         lngPConclAnalysis,
         monPAmount AS OppAmount,
         numSalesOrPurType,
         Opp.tintActive,
		  dbo.fn_GetContactName(Opp.numCreatedby) AS CreatedBy,
         --dbo.fn_GetContactName(Opp.numCreatedby)
         --  + ' '
         --  + CONVERT(VARCHAR(20),dateadd(MINUTE,-@ClientTimeZoneOffset,Opp.bintCreatedDate)) AS CreatedBy,
         dbo.fn_GetContactName(Opp.numModifiedBy)
           + ' '
           + CONVERT(VARCHAR,dateadd(MINUTE,-@ClientTimeZoneOffset,Opp.bintModifiedDate),0) AS ModifiedBy,
         dbo.fn_GetContactName(Opp.numRecOwner) AS RecordOwner,
         Opp.numRecOwner,
         isnull(tintshipped,0) AS tintshipped,
         isnull(tintOppStatus,0) AS tintOppStatus,
         dbo.OpportunityLinkedItems(@numOppID) AS NoOfProjects,
         Opp.numAssignedTo,
         Opp.numAssignedBy,
         (SELECT COUNT(* )
          FROM   dbo.GenericDocuments
          WHERE  numRecID = @numOppID
                 AND vcDocumentSection = 'O') AS DocumentCount,
         isnull(OPR.numRecurringId,0) AS numRecurringId,
		 OPR.dtRecurringDate AS dtLastRecurringDate,
         D2.numTerID,
         Link.numOppID AS numParentOppID,
         Link.vcPoppName AS ParentOppName,
         Link.vcSource AS Source,
         Link.numParentProjectID,
         Link.vcProjectName,
         ISNULL(C.varCurrSymbol,'') varCurrSymbol,
         ISNULL(Opp.fltExchangeRate,0) AS [fltExchangeRate],
         ISNULL(C.chrCurrency,'') AS [chrCurrency],
         ISNULL(C.numCurrencyID,0) AS [numCurrencyID],
		 (Select Count(*) from OpportunityBizDocs 
		 where numOppId=@numOppID and bitAuthoritativeBizDocs=1) As AuthBizDocCount,
		 (SELECT COUNT(distinct numChildOppID) FROM [OpportunityLinking] OL WHERE OL.[numParentOppID] = Opp.numOppId) AS ChildCount,
		 (SELECT COUNT(*) FROM [RecurringTransactionReport] RTR WHERE RTR.[numRecTranSeedId] = Opp.numOppId)  AS RecurringChildCount
		 ,Opp.bintCreatedDate,
		 ISNULL(Opp.bitStockTransfer ,0) bitStockTransfer,	ISNULL(Opp.tintTaxOperator,0) AS tintTaxOperator, 
         isnull(Opp.intBillingDays,0) AS numBillingDays,isnull(Opp.bitInterestType,0) AS bitInterestType,
         isnull(opp.fltInterest,0) AS fltInterest,  isnull(Opp.fltDiscount,0) AS fltDiscount,
         isnull(Opp.bitDiscountType,0) AS bitDiscountType,ISNULL(Opp.bitBillingTerms,0) AS bitBillingTerms,ISNULL(OPP.numDiscountAcntType,0) AS numDiscountAcntType,
         ISNULL((SELECT TOP 1 numCountry FROM fn_getOPPAddressDetails(Opp.numOppId,Opp.numDomainID,2)),0) AS [numShipCountry],
		 ISNULL((SELECT TOP 1 numState FROM fn_getOPPAddressDetails(Opp.numOppId,Opp.numDomainID,2)),0) AS [numShipState],
		 ISNULL((SELECT TOP 1 vcState FROM fn_getOPPAddressDetails(Opp.numOppId,Opp.numDomainID,2)),'') AS [vcShipState],
		 ISNULL((SELECT TOP 1 vcCountry FROM fn_getOPPAddressDetails(Opp.numOppId,Opp.numDomainID,2)),'') AS [vcShipCountry],
		 ISNULL((SELECT TOP 1 vcPostalCode FROM fn_getOPPAddressDetails(Opp.numOppId,Opp.numDomainID,2)),'') AS [vcPostalCode],
		 ISNULL(vcCouponCode,'') AS [vcCouponCode],ISNULL(Opp.bitPPVariance,0) AS bitPPVariance,
		 Opp.dtItemReceivedDate,ISNULL(bitUseShippersAccountNo,0) [bitUseShippersAccountNo], ISNULL((SELECT DMSA.vcAccountNumber FROm [dbo].[DivisionMasterShippingAccount] DMSA WHERE DMSA.numDivisionID=D2.numDivisionID AND DMSA.numShipViaID=opp.intUsedShippingCompany),'') [vcShippersAccountNo],
		 ISNULL(bitUseMarkupShippingRate,0) [bitUseMarkupShippingRate],
		 ISNULL(numMarkupShippingRate,0) [numMarkupShippingRate],
		 ISNULL(Opp.intUsedShippingCompany,0) AS intUsedShippingCompany,
		 ISNULL(Opp.numShippingService,0) numOrderShippingService,
		 ISNULL(SR.[vcValue2],ISNULL(numShippingService,
		 (SELECT TOP 1 ISNULL([SST].[intNsoftEnum],0) FROM [dbo].[ShippingServiceTypes] AS SST 
		 WHERE [SST].[numDomainID] = @numDomainID 
		 AND [SST].[numRuleID] = 0
		 AND [SST].[vcServiceName] IN (SELECT ISNULL([OI].[vcItemDesc],'') FROM [dbo].[OpportunityItems] AS OI 
									 WHERE [OI].[numOppId] = [Opp].[numOppId] 
									 AND [OI].[vcType] = 'Service Item')))) AS numShippingService,
		 ISNULL(Opp.[monLandedCostTotal],0) AS monLandedCostTotal,opp.[vcLanedCost],
		 ISNULL(Opp.vcRecurrenceType,'') AS vcRecurrenceType,
		 ISNULL(RecurrenceConfiguration.numRecConfigID,0) AS numRecConfigID,
		 (CASE WHEN ISNULL(Opp.vcRecurrenceType,'') = '' THEN 0 ELSE 1 END) AS bitRecur,
		 (CASE WHEN RecurrenceTransaction.numRecTranID IS NULL THEN 0 ELSE 1 END) AS bitRecurred,
		 dbo.FormatedDateFromDate(RecurrenceConfiguration.dtStartDate,@numDomainID) AS dtStartDate,
		 dbo.FormatedDateFromDate(RecurrenceConfiguration.dtEndDate,@numDomainID) AS dtEndDate,
		 RecurrenceConfiguration.vcFrequency AS vcFrequency,
		 ISNULL(RecurrenceConfiguration.bitDisabled,0) AS bitDisabled,
		 ISNULL(Opp.dtReleaseDate,'') AS dtReleaseDate,
		 ISNULL(Opp.dtExpectedDate,'') AS dtExpectedDate,
		 ISNULL(Opp.numAccountClass,0) AS numAccountClass,D3.vcPartnerCode+'-'+C3.vcCompanyName as numPartenerSource,
		ISNULL(Opp.numPartner,0) AS numPartenerSourceId,
		ISNULL(Opp.numPartenerContact,0) AS numPartenerContactId,
		A.vcFirstName+' '+A.vcLastName AS numPartenerContact,
		numShipFromWarehouse, CPN.CustomerPartNo,
		Opp.numBusinessProcessID,
		SLP.Slp_Name AS vcProcessName,
		(CASE WHEN ISNULL((SELECT COUNT(*) FROM OpportunityBizDocs WHERE OpportunityBizDocs.numOppID=Opp.numOppId AND numBizDocId IN (287,296,644)),0) > 0 THEN 1 ELSE 0 END) bitAuthorativeBizDocAdded
  FROM   OpportunityMaster Opp 
         JOIN divisionMaster D2
           ON Opp.numDivisionID = D2.numDivisionID
		   LEFT JOIN divisionMaster D3 ON Opp.numPartner = D3.numDivisionID
		LEFT JOIN CompanyInfo C3 ON C3.numCompanyID = D3.numCompanyID
		LEFT JOIN AdditionalContactsInformation A ON Opp.numPartenerContact = A.numContactId
		LEFT JOIN Sales_process_List_Master AS SLP ON Opp.numBusinessProcessID=SLP.Slp_Id
         LEFT JOIN CompanyInfo C2
           ON C2.numCompanyID = D2.numCompanyID
         LEFT JOIN (SELECT TOP 1 OM.vcPoppName,
                                 OM.numOppID,
                                 numChildOppID,
                                 vcSource,
                                 numParentProjectID,
                                 vcProjectName
                    FROM   OpportunityLinking
                          LEFT JOIN OpportunityMaster OM
                             ON numOppID = numParentOppID
                          LEFT JOIN [ProjectsMaster]
							 ON numParentProjectID = numProId   
                    WHERE  numChildOppID = @numOppID) Link
           ON Link.numChildOppID = Opp.numOppID
         LEFT OUTER JOIN [Currency] C
					ON C.numCurrencyID = Opp.numCurrencyID
		 LEFT OUTER JOIN [OpportunityRecurring] OPR
					ON OPR.numOppId = Opp.numOppId
		 LEFT OUTER JOIN additionalContactsinformation ADC ON
		 ADC.numdivisionId = D2.numdivisionId AND  ADC.numContactId = Opp.numContactId	
		 LEFT JOIN RecurrenceConfiguration ON Opp.numOppId = RecurrenceConfiguration.numOppID AND RecurrenceConfiguration.numType = 1
		 LEFT JOIN RecurrenceTransaction ON Opp.numOppId = RecurrenceTransaction.numRecurrOppID
		 LEFT JOIN [dbo].[ShippingReport] AS SR ON SR.[numOppID] = [Opp].[numOppId] AND SR.[numDomainID] = [Opp].[numDomainId]
		LEFT JOIN [dbo].CustomerPartNumber CPN ON C3.numCompanyID = CPN.numCompanyID
		WHERE  Opp.numOppId = @numOppID
         AND Opp.numDomainID = @numDomainID
END
GO
/****** Object:  StoredProcedure [dbo].[USP_OppManage]    Script Date: 07/26/2008 16:20:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OppManage')
DROP PROCEDURE usp_oppmanage
GO
CREATE PROCEDURE [dbo].[USP_OppManage] 
(                                                                          
  @numOppID as numeric=0 OUTPUT,                                                                          
  @numContactId numeric(9)=null,                                                                          
  @numDivisionId numeric(9)=null,                                                                          
  @tintSource numeric(9)=null,                                                                          
  @vcPOppName Varchar(100)='' OUTPUT,                                                                          
  @Comments varchar(1000)='',                                                                          
  @bitPublicFlag bit=0,                                                                          
  @numUserCntID numeric(9)=0,                                                                                 
  @monPAmount DECIMAL(20,5) =0,                                                 
  @numAssignedTo as numeric(9)=0,                                                                                                        
  @numDomainId numeric(9)=0,                                                                                                                                                                                   
  @strItems as text=null,                                                                          
  @strMilestone as text=null,                                                                          
  @dtEstimatedCloseDate datetime,                                                                          
  @CampaignID as numeric(9)=null,                                                                          
  @lngPConclAnalysis as numeric(9)=null,                                                                         
  @tintOppType as tinyint,                                                                                                                                             
  @tintActive as tinyint,                                                              
  @numSalesOrPurType as numeric(9),              
  @numRecurringId as numeric(9),
  @numCurrencyID as numeric(9)=0,
  @DealStatus as tinyint =0,-- 0=Open,1=Won,2=Lost
  @numStatus AS NUMERIC(18,0),
  @vcOppRefOrderNo VARCHAR(100),
  @numBillAddressId numeric(9)=0,
  @numShipAddressId numeric(9)=0,
  @bitStockTransfer BIT=0,
  @WebApiId INT = 0,
  @tintSourceType TINYINT=0,
  @bitDiscountType as bit,
  @fltDiscount  as float,
  @bitBillingTerms as bit,
  @intBillingDays as integer,
  @bitInterestType as bit,
  @fltInterest as float,
  @tintTaxOperator AS TINYINT,
  @numDiscountAcntType AS NUMERIC(9),
  @vcWebApiOrderNo VARCHAR(100)=NULL,
  @vcCouponCode		VARCHAR(100) = '',
  @vcMarketplaceOrderID VARCHAR(100)=NULL,
  @vcMarketplaceOrderDate  datetime=NULL,
  @vcMarketplaceOrderReportId VARCHAR(100)=NULL,
  @numPercentageComplete NUMERIC(9),
  @bitUseShippersAccountNo BIT = 0,
  @bitUseMarkupShippingRate BIT = 0,
  @numMarkupShippingRate NUMERIC(19,2) = 0,
  @intUsedShippingCompany INT = 0,
  @numShipmentMethod NUMERIC(18,0)=0,
  @dtReleaseDate DATE = NULL,
  @numPartner NUMERIC(18,0)=0,
  @tintClickBtn INT=0,
  @numPartenerContactId NUMERIC(18,0)=0,
  @numAccountClass NUMERIC(18,0) = 0,
  @numWillCallWarehouseID NUMERIC(18,0) = 0,
  @numVendorAddressID NUMERIC(18,0) = 0,
  @numShipFromWarehouse NUMERIC(18,0) = 0,
  @numShippingService NUMERIC(18,0) = 0,
  @ClientTimeZoneOffset INT = 0,
  @vcCustomerPO VARCHAR(100)=NULL,
  @bitDropShipAddress BIT = 0,
  @PromCouponCode AS VARCHAR(100) = NULL,
  @numPromotionId AS NUMERIC(18,0) = 0,
  @dtExpectedDate AS DATETIME = null,
  @numProjectID NUMERIC(18,0) = 0
  -- USE PARAMETER WITH OPTIONAL VALUE BECAUSE PROCEDURE IS CALLED FROM OTHER PROCEDURE WHICH WILL NOT PASS NEW PARAMETER VALUE
)                                                                          
                                                                          
as  
BEGIN TRY
BEGIN TRANSACTION
	DECLARE @fltExchangeRate float                                 
	DECLARE @hDocItem int                                                                                                                            
	DECLARE @TotalAmount as DECIMAL(20,5)                                                                          
	DECLARE @tintOppStatus as tinyint
	DECLARE @bitNewOrder AS BIT = (CASE WHEN ISNULL(@numOppID,0) = 0 THEN 1 ELSE 0 END)
	
	IF ISNULL(@numContactId,0) = 0
	BEGIN
		RAISERROR('CONTACT_REQUIRED',16,1)
		RETURN
	END  
	
	IF ISNULL(@tintOppType,0) = 0
	BEGIN
		RAISERROR('OPP_TYPE_CAN_NOT_BE_0',16,1)
		RETURN
	END             

	IF ISNULL(@numOppID,0) > 0 AND EXISTS (SELECT numOppId FROM OpportunityMaster WHERE numDomainId=@numDomainId AND numOppId=@numOppID AND ISNULL(tintshipped,0) = 1) 
	BEGIN	 
		RAISERROR('OPP/ORDER_IS_CLOSED',16,1)
		RETURN
	END

	DECLARE @dtItemRelease AS DATE
	SET @dtItemRelease = DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE())

	DECLARE @numCompany AS NUMERIC(18)
	DECLARE @numRelationship NUMERIC(18,0)
	DECLARE @numProfile NUMERIC(18,0)
	DECLARE @tintCommitAllocation AS TINYINT
	DECLARE @bitAllocateInventoryOnPickList AS BIT

	SELECT @tintCommitAllocation=ISNULL(tintCommitAllocation,1) FROM Domain WHERE numDomainId=@numDomainId

	SELECT 
		@numCompany = D.numCompanyID
		,@bitAllocateInventoryOnPickList=ISNULL(bitAllocateInventoryOnPickList,0)
		,@numRelationship=numCompanyType
		,@numProfile=vcProfile
	FROM 
		DivisionMaster D
	JOIN 
		CompanyInfo C 
	ON 
		C.numCompanyId=D.numCompanyID
	WHERE 
		D.numDivisionID = @numDivisionId

	IF @numOppID = 0 AND CONVERT(VARCHAR(10),@strItems) <> ''
	BEGIN
		EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems

		--VALIDATE PROMOTION OF COUPON CODE
		DECLARE @TEMP TABLE
		(
			ID INT IDENTITY(1,1),
			numOppItemID NUMERIC(18,0),
			numPromotionID NUMERIC(18,0)
		)

		INSERT INTO @TEMP
		(
			numOppItemID,
			numPromotionID
		)
		SELECT
			numoppitemtCode,
			numPromotionID
		FROM
			OPENXML (@hDocItem,'/NewDataSet/Item',2)
		WITH
		(
			numoppitemtCode NUMERIC(18,0),
			numPromotionID NUMERIC(18,0)
		)
		WHERE
			numPromotionID > 0

		EXEC sp_xml_removedocument @hDocItem 

		IF (SELECT COUNT(*) FROM @TEMP) > 0
		BEGIN
			-- IF ITEM PROMOTION USED IN ORDER EXIPIRES THAN RAISE ERROR
			IF  (SELECT 
						COUNT(PO.numProId)
					FROM 
						PromotionOffer PO
					LEFT JOIN
						PromotionOffer POOrder
					ON
						PO.numOrderPromotionID = POOrder.numProId
					INNER JOIN
						@TEMP T1
					ON
						PO.numProId = T1.numPromotionID
					WHERE 
						PO.numDomainId=@numDomainID 
						AND ISNULL(PO.IsOrderBasedPromotion,0) = 0
						AND ISNULL(PO.bitEnabled,0) = 1
						AND 1 = (CASE 
									WHEN ISNULL(PO.numOrderPromotionID,0) > 0 
									THEN (CASE WHEN POOrder.bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN GETUTCDATE()>=POOrder.dtValidFrom AND GETUTCDATE()<=POOrder.dtValidTo THEN 1 ELSE 0 END) END)
									ELSE (CASE WHEN PO.bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN GETUTCDATE()>=PO.dtValidFrom AND GETUTCDATE()<=PO.dtValidTo THEN 1 ELSE 0 END) END)
								END)
						AND 1 = (CASE 
									WHEN ISNULL(PO.numOrderPromotionID,0) > 0 
									THEN
										(CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=PO.numOrderPromotionID AND tintType=2 AND numRelationship=@numRelationship AND numProfile=@numProfile) > 0 THEN 1 ELSE 0 END)
									ELSE
										(CASE PO.tintCustomersBasedOn 
											WHEN 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=PO.numProId AND tintType=1 AND numDivisionID=@numDivisionID) > 0 THEN 1 ELSE 0 END)
											WHEN 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=PO.numProId AND tintType=2 AND numRelationship=@numRelationship AND numProfile=@numProfile) > 0 THEN 1 ELSE 0 END)
											WHEN 3 THEN 1
											ELSE 0
										END)
								END)
				) <> (SELECT COUNT(*) FROM @TEMP)
			BEGIN
				RAISERROR('ITEM_PROMOTION_EXPIRED',16,1)
				RETURN
			END

			-- NOW IF COUPON BASED ITEM PROMOTION IS USED THEN VALIDATE COUPON AND ITS USAGE
			IF 
			(
				SELECT 
					COUNT(*)
				FROM
					PromotionOffer PO
				INNER JOIN
					@TEMP T1
				ON
					PO.numProId = T1.numPromotionID
				WHERE
					PO.numDomainId = @numDomainId
					AND ISNULL(IsOrderBasedPromotion,0) = 0
					AND ISNULL(numOrderPromotionID,0) > 0 
			) = 1
			BEGIN
				IF ISNULL(@PromCouponCode,0) <> ''
				BEGIN
					IF (SELECT 
							COUNT(*)
						FROM
							PromotionOffer PO
						INNER JOIN 
							PromotionOffer POOrder
						ON
							PO.numOrderPromotionID = POOrder.numProId
						INNER JOIN
							@TEMP T1
						ON
							PO.numProId = T1.numPromotionID
						INNER JOIN
							DiscountCodes DC
						ON
							POOrder.numProId = DC.numPromotionID
						WHERE
							PO.numDomainId = @numDomainId
							AND ISNULL(PO.IsOrderBasedPromotion,0) = 0
							AND ISNULL(DC.vcDiscountCode,0) = @PromCouponCode) = 0
					BEGIN
						RAISERROR('INVALID_COUPON_CODE_ITEM_PROMOTION',16,1)
						RETURN
					END
					ELSE
					BEGIN
						-- IF THERE IS COUPON CODE USAGE LIMIT THAN VALIDATE USAGE LIMIT
						IF (SELECT 
								COUNT(*)
							FROM
								PromotionOffer PO
							INNER JOIN 
								PromotionOffer POOrder
							ON
								PO.numOrderPromotionID = POOrder.numProId
							INNER JOIN
								@TEMP T1
							ON
								PO.numProId = T1.numPromotionID
							INNER JOIN
								DiscountCodes DC
							ON
								POOrder.numProId = DC.numPromotionID
							WHERE
								PO.numDomainId = @numDomainId
								AND ISNULL(PO.IsOrderBasedPromotion,0) = 0
								AND ISNULL(DC.vcDiscountCode,0) = @PromCouponCode
								AND 1 = (CASE WHEN ISNULL(DC.CodeUsageLimit,0) <> 0 THEN 1 ELSE 0 END)
								AND ISNULL((SELECT intCodeUsed FROM DiscountCodeUsage WHERE DiscountCodeUsage.numDiscountId=DC.numDiscountId AND numDivisionId=@numDivisionId),0) >= DC.CodeUsageLimit) > 0
						BEGIN
							RAISERROR('ITEM_PROMOTION_COUPON_CODE_USAGE_LIMIT_EXCEEDED',16,1)
							RETURN
						END
					END
				END
				ELSE
				BEGIN
					RAISERROR('COUPON_CODE_REQUIRED_ITEM_PROMOTION',16,1)
					RETURN
				END
			END
		END
	END

	--IF NEW OPPERTUNITY
	IF @numOppID = 0                                                                          
	BEGIN
		DECLARE @bitIsInitalSalesOrder BIT
		IF(@DealStatus=1 AND @tintOppType=1)
		BEGIN
			SET @bitIsInitalSalesOrder=1
		END

		DECLARE @intOppTcode AS NUMERIC(9)  
  
		SELECT @intOppTcode = MAX(numOppId) FROM OpportunityMaster                

		SET @intOppTcode = ISNULL(@intOppTcode,0) + 1                                                
		
		SET @vcPOppName = @vcPOppName + '-'+ CONVERT(varchar(4),YEAR(GETUTCDATE()))  +'-A'+ CONVERT(varchar(10),@intOppTcode)                                  
  
		IF ISNULL(@numCurrencyID,0) = 0 
		BEGIN
			SET @fltExchangeRate=1
			SELECT @numCurrencyID= ISNULL(numCurrencyID,0) FROM dbo.Domain WHERE numDomainId=@numDomainId	
		END
		ELSE
		BEGIN 
			SET @fltExchangeRate = dbo.GetExchangeRate(@numDomainID,@numCurrencyID)
        END  
		                                                        
		IF ISNULL(@numAccountClass,0) = 0
		BEGIN                                                  
		DECLARE @tintDefaultClassType AS INT = 0
		SELECT @tintDefaultClassType = ISNULL(tintDefaultClassType,0) FROM Domain WHERE numDomainId = @numDomainID 

		IF @tintDefaultClassType = 1 --USER
		BEGIN
			SELECT 
				@numAccountClass=ISNULL(numDefaultClass,0) 
			FROM 
				dbo.UserMaster UM 
			JOIN 
				dbo.Domain D 
			ON 
				UM.numDomainID=D.numDomainId
			WHERE 
				D.numDomainId=@numDomainId 
				AND UM.numUserDetailId=@numUserCntID
		END
		ELSE IF @tintDefaultClassType = 2 --COMPANY
		BEGIN
			SELECT 
				@numAccountClass=ISNULL(numAccountClassID,0) 
			FROM 
				dbo.DivisionMaster DM 
			WHERE 
				DM.numDomainId=@numDomainId 
				AND DM.numDivisionID=@numDivisionID
		END
		ELSE
		BEGIN
			SET @numAccountClass = 0
		END
		END

        IF(@DealStatus=1 AND @tintOppType=1 AND @tintClickBtn=1)
		BEGIN
			SET @DealStatus=0
		END         
		--PRINT ('Deal Staus ' +@DealStatus)  

		-------- VALIDATE ORDER BASED PROMOTION -------------

		DECLARE @numDiscountID NUMERIC(18,0) = NULL
		IF @numPromotionId > 0 
		BEGIN
			-- VALIDTE IF ITS ORDER BASED PROMOTION (BOTH ITEM AND ORDER BASED PROMOTION CAN HAVE COUPON CODE)
			IF EXISTS (SELECT numProId FROM PromotionOffer WHERE numDomainId=@numDomainId AND numProId=@numPromotionId AND ISNULL(IsOrderBasedPromotion,0) = 1)
			BEGIN
				-- CHECK IF ORDER PROMOTION IS STILL VALID
				IF NOT EXISTS (SELECT 
									PO.numProId
								FROM 
									PromotionOffer PO
								INNER JOIN 
									PromotionOfferOrganizations PORG
								ON 
									PO.numProId = PORG.numProId
								WHERE 
									numDomainId=@numDomainID 
									AND PO.numProId=@numPromotionId
									AND ISNULL(IsOrderBasedPromotion,0) = 1
									AND ISNULL(bitEnabled,0) = 1
									AND 1 = (CASE WHEN bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN GETUTCDATE()>=dtValidFrom AND GETUTCDATE()<=dtValidTo THEN 1 ELSE 0 END) END)
									AND numRelationship=@numRelationship 
									AND numProfile=@numProfile
				)
				BEGIN
					RAISERROR('ORDER_PROMOTION_EXPIRED',16,1)
					RETURN
				END
				ELSE IF ISNULL((SELECT ISNULL(bitRequireCouponCode,0) FROM PromotionOffer WHERE numDomainId=@numDomainId AND numProId=@numPromotionId),0) = 1
				BEGIN
					IF ISNULL(@PromCouponCode,0) <> ''
					BEGIN
						IF (SELECT 
								COUNT(*)
							FROM
								PromotionOffer PO
							INNER JOIN
								DiscountCodes DC
							ON
								PO.numProId = DC.numPromotionID
							WHERE
								PO.numDomainId = @numDomainId
								AND PO.numProId = @numPromotionId
								AND ISNULL(IsOrderBasedPromotion,0) = 1
								AND ISNULL(bitRequireCouponCode,0) = 1
								AND ISNULL(DC.vcDiscountCode,0) = @PromCouponCode) = 0
						BEGIN
							RAISERROR('INVALID_COUPON_CODE_ORDER_PROMOTION',16,1)
							RETURN
						END
						ELSE
						BEGIN
							-- IF THERE IS COUPON CODE USAGE LIMIT THAN VALIDATE USAGE LIMIT
							IF (SELECT 
									COUNT(*)
								FROM
									PromotionOffer PO
								INNER JOIN
									DiscountCodes DC
								ON
									PO.numProId = DC.numPromotionID
								WHERE
									PO.numDomainId = @numDomainId
									AND PO.numProId = @numPromotionId
									AND ISNULL(IsOrderBasedPromotion,0) = 1
									AND ISNULL(bitRequireCouponCode,0) = 1
									AND ISNULL(DC.vcDiscountCode,0) = @PromCouponCode
									AND 1 = (CASE WHEN ISNULL(DC.CodeUsageLimit,0) <> 0 THEN 1 ELSE 0 END)
									AND ISNULL((SELECT intCodeUsed FROM DiscountCodeUsage WHERE DiscountCodeUsage.numDiscountId=DC.numDiscountId AND numDivisionId=@numDivisionId),0) >= DC.CodeUsageLimit) > 0
							BEGIN
								RAISERROR('ORDER_PROMOTION_COUPON_CODE_USAGE_LIMIT_EXCEEDED',16,1)
								RETURN
							END
							ELSE
							BEGIN			
								SELECT
									@numDiscountID=numDiscountId
								FROM
									DiscountCodes DC
								WHERE
									DC.numPromotionID=@numPromotionId
									AND ISNULL(DC.vcDiscountCode,'')=@PromCouponCode
									
								IF EXISTS (SELECT DC.numDiscountID FROM DiscountCodes DC INNER JOIN DiscountCodeUsage DCU ON DC.numDiscountID=DCU.numDIscountID WHERE DC.numPromotionID=@numPromotionId AND DCU.numDivisionID=@numDivisionID AND ISNULL(DC.vcDiscountCode,'')=@PromCouponCode)
								BEGIN
									UPDATE
										DCU
									SET
										DCU.intCodeUsed = ISNULL(DCU.intCodeUsed,0) + 1
									FROM 
										DiscountCodes DC 
									INNER JOIN 
										DiscountCodeUsage DCU 
									ON 
										DC.numDiscountID=DCU.numDIscountID 
									WHERE 
										DC.numPromotionID=@numPromotionId 
										AND DCU.numDivisionID=@numDivisionID 
										AND ISNULL(DC.vcDiscountCode,'')=@PromCouponCode
								END
								ELSE
								BEGIN
									INSERT INTO DiscountCodeUsage
									(
										numDiscountId
										,numDivisionId
										,intCodeUsed
									)
									SELECT 
										DC.numDiscountId
										,@numDivisionId
										,1
									FROM
										DiscountCodes DC
									WHERE
										DC.numPromotionID=@numPromotionId
										AND ISNULL(DC.vcDiscountCode,'')=@PromCouponCode
								END
							END
						END
					END
					ELSE
					BEGIN
						RAISERROR('COUPON_CODE_REQUIRED_ORDER_PROMOTION',16,1)
						RETURN
					END
				END
			END
		END

		-------- ORDER BASED Promotion Logic-------------
		                                                    
		INSERT INTO OpportunityMaster
		(                                                                             
			numContactId,numDivisionId,txtComments,numCampainID,bitPublicFlag,tintSource,tintSourceType,vcPOppName,                                                                          
			intPEstimatedCloseDate,monPAmount,numCreatedBy,bintCreatedDate,numModifiedBy,bintModifiedDate,numDomainId,                                                                                                             
			numRecOwner,lngPConclAnalysis,tintOppType,numSalesOrPurType,numCurrencyID,fltExchangeRate,numAssignedTo,
			numAssignedBy,[tintOppStatus],numStatus,vcOppRefOrderNo,bitStockTransfer,bitBillingTerms,intBillingDays,
			bitInterestType,fltInterest,vcCouponCode,bitDiscountType,fltDiscount,bitPPVariance,vcMarketplaceOrderID,
			vcMarketplaceOrderReportId,numPercentageComplete,numAccountClass,tintTaxOperator,bitUseShippersAccountNo,
			bitUseMarkupShippingRate,numMarkupShippingRate,intUsedShippingCompany,numShipmentMethod,numShippingService,numPartner,numPartenerContact,dtReleaseDate,bitIsInitalSalesOrder,numVendorAddressID,numShipFromWarehouse
			,vcCustomerPO#,bitDropShipAddress,numDiscountID,dtExpectedDate,numProjectID
		)                                                                          
		Values                                                                          
		(                                                                          
			@numContactId,@numDivisionId,ISNULL(@Comments,''),@CampaignID,@bitPublicFlag,@tintSource,@tintSourceType,@vcPOppName,                                                                          
			@dtEstimatedCloseDate,@monPAmount,@numUserCntID,(CASE @tintSourceType WHEN 3 THEN @vcMarketplaceOrderDate ELSE GETUTCDATE() END),                                                                          
			@numUserCntID,GETUTCDATE(),@numDomainId,@numUserCntID,@lngPConclAnalysis,@tintOppType,@numSalesOrPurType,@numCurrencyID, 
			@fltExchangeRate,(CASE WHEN @numAssignedTo > 0 THEN @numAssignedTo ELSE NULL END),(CASE WHEN @numAssignedTo>0 THEN @numUserCntID ELSE NULL END),
			@DealStatus,@numStatus,@vcOppRefOrderNo,@bitStockTransfer,@bitBillingTerms,@intBillingDays,@bitInterestType,@fltInterest,
			@PromCouponCode,@bitDiscountType,@fltDiscount,1,@vcMarketplaceOrderID,@vcMarketplaceOrderReportId,@numPercentageComplete,
			@numAccountClass,@tintTaxOperator,@bitUseShippersAccountNo,@bitUseMarkupShippingRate,@numMarkupShippingRate,@intUsedShippingCompany,@numShipmentMethod,@numShippingService,@numPartner,@numPartenerContactId,@dtReleaseDate,@bitIsInitalSalesOrder,@numVendorAddressID,@numShipFromWarehouse
			,@vcCustomerPO,@bitDropShipAddress,@numDiscountID,@dtExpectedDate,@numProjectID
		)                                                                                                                      
		
		SET @numOppID=(SELECT SCOPE_IDENTITY())                
                            
        EXEC dbo.usp_OppDefaultAssociateContacts @numOppId=@numOppID,@numDomainId=@numDomainId                    
		--UPDATE OPPNAME AS PER NAME TEMPLATE
		EXEC dbo.USP_UpdateNameTemplateValue @tintOppType,@numDomainID,@numOppID

		SELECT @vcPOppName=ISNULL(vcPOppName,'') FROM OpportunityMaster WHERE numOppId=@numOppID

		--MAP CUSTOM FIELD	
		DECLARE @tintPageID AS TINYINT;
		SET @tintPageID = CASE WHEN @tintOppType=1 THEN 2 ELSE 6 END 
  	
		EXEC dbo.USP_AddParentChildCustomFieldMap
				@numDomainID = @numDomainID, --  numeric(18, 0)
				@numRecordID = @numOppID, --  numeric(18, 0)
				@numParentRecId = @numDivisionId, --  numeric(18, 0)
				@tintPageID = @tintPageID --  tinyint
 	
		IF ISNULL(@numStatus,0) > 0 AND @tintOppType=1 AND isnull(@DealStatus,0)=1
		BEGIN
	   		EXEC USP_ManageOpportunityAutomationQueue
					@numOppQueueID = 0, -- numeric(18, 0)
					@numDomainID = @numDomainID, -- numeric(18, 0)
					@numOppId = @numOppID, -- numeric(18, 0)
					@numOppBizDocsId = 0, -- numeric(18, 0)
					@numOrderStatus = @numStatus, -- numeric(18, 0)
					@numUserCntID = @numUserCntID, -- numeric(18, 0)
					@tintProcessStatus = 1, -- tinyint
					@tintMode = 1 -- TINYINT
		END
  
		-- INSERTING ITEMS                                                                                                                    
		IF CONVERT(VARCHAR(10),@strItems) <> '' AND @numOppID > 0
		BEGIN
			EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems       
	
			INSERT INTO OpportunityItems
			(
				numOppId,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,bitDropShip,bitDiscountType,fltDiscount,
				monTotAmtBefDiscount,vcItemName,vcModelID,vcManufacturer,vcPathForTImage,monVendorCost,numUOMId,bitWorkOrder,numVendorWareHouse,
				numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,numClassID,numToWarehouseItemID,vcAttributes,vcAttrValues,monAvgCost,bitItemPriceApprovalRequired,
				numPromotionID,bitPromotionTriggered,vcPromotionDetail,numSortOrder,numCost,vcNotes,vcInstruction,bintCompliationDate,dtPlannedStart,numWOAssignedTo,numShipToAddressID,ItemReleaseDate
				,bitMarkupDiscount,vcChildKitSelectedItems,numWOQty
			)
			SELECT 
				@numOppID,X.numItemCode,dbo.fn_UOMConversion(X.numUOM,numItemCode,@numDomainId,null) * x.numUnitHour,x.monPrice,x.monTotAmount,X.vcItemDesc,
				(CASE X.numWarehouseItmsID 
				WHEN -1 
				THEN 
					CASE 
						WHEN ((SELECT COUNT(*) FROM Item WHERE numDomainID=@numDomainId AND vcSKU= X.vcSKU) > 0)
							THEN
								(SELECT TOP 1 
									numWareHouseItemID 
								FROM 
									WareHouseItems 
								WHERE 
									[numItemID] = X.numItemCode 
									AND numWareHouseID IN (SELECT TOP 1 ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail WHERE numDomainID =@numDomainId AND WebApiId = @WebApiId))
						ELSE
							(SELECT 
								[numWareHouseItemID] 
							FROM 
								[WareHouseItems] 
							WHERE 
								[numItemID] = X.numItemCode 
								AND ISNULL(WareHouseItems.vcWHSKU,'') = ISNULL(X.vcSKU,'')
								AND numWareHouseID IN (SELECT TOP 1 ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail WHERE numDomainID =@numDomainId AND WebApiId = @WebApiId)) 
					END
				WHEN 0 
				THEN 
					CASE 
					WHEN ((SELECT COUNT(*) FROM Item WHERE numDomainID=@numDomainId AND vcSKU=X.vcSKU) > 0)
						THEN
							(SELECT TOP 1 
								numWareHouseItemID 
							FROM 
								WareHouseItems 
							WHERE 
								[numItemID] = X.numItemCode 
								AND numWareHouseID IN (SELECT TOP 1 ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail WHERE numDomainID =@numDomainId AND WebApiId = @WebApiId))
					ELSE
						(SELECT 
							[numWareHouseItemID] 
						FROM 
							[WareHouseItems] 
						WHERE 
							[numItemID] = X.numItemCode 
							AND ISNULL(WareHouseItems.vcWHSKU,'') = ISNULL(X.vcSKU,'')
							AND numWareHouseID IN (SELECT TOP 1 ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail WHERE numDomainID =@numDomainId AND WebApiId = @WebApiId)) 
					END
				ELSE  
					X.numWarehouseItmsID 
				END) AS numWarehouseItmsID,
				X.ItemType,X.DropShip,X.bitDiscountType,X.fltDiscount,X.monTotAmtBefDiscount,X.vcItemName,(SELECT vcModelID FROM item WHERE numItemCode = X.numItemCode),
				(SELECT vcManufacturer FROM item WHERE numItemCode = X.numItemCode),(SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = X.numItemCode AND BitDefault = 1 AND numdomainId = @numdomainId),
				(SELECT isnull(VN.monCost,0) FROM Vendor VN INNER JOIN Item IT ON IT.numItemCode=VN.numItemCode AND VN.numVendorID=IT.numVendorID WHERE  VN.numItemCode=X.numItemCode),
				X.numUOM,X.bitWorkOrder,X.numVendorWareHouse,X.numShipmentMethod,X.numSOVendorId,X.numProjectID,X.numProjectStageID,
				0,
				X.numToWarehouseItemID,X.Attributes,X.AttributeIDs,(SELECT (CASE WHEN ISNULL(I.bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(I.monAverageCost,0) END) FROM Item I WHERE I.numDomainId=@numDomainId AND I.numItemCode=X.numItemCode),
				X.bitItemPriceApprovalRequired,X.numPromotionID,X.bitPromotionTriggered,X.vcPromotionDetail,X.numSortOrder,ISNULL(X.numCost,0),ISNULL(X.vcVendorNotes,'')
				,X.vcInstruction,DATEADD(MINUTE,@ClientTimeZoneOffset,X.bintCompliationDate),DATEADD(MINUTE,@ClientTimeZoneOffset,X.dtPlannedStart),X.numWOAssignedTo,X.numShipToAddressID,ISNULL(ItemReleaseDate,@dtItemRelease),X.bitMarkupDiscount,ISNULL(X.KitChildItems,''),X.numWOQty
			FROM
				(
					SELECT 
						*
					FROM 
						OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=1]',2)                                                                          
					WITH                       
					(                                                                          
						numoppitemtCode NUMERIC(9),numItemCode NUMERIC(9), numUnitHour FLOAT, monPrice DECIMAL(30,16), monTotAmount DECIMAL(20,5),vcItemDesc VARCHAR(2000),                                    
						numWarehouseItmsID NUMERIC(9), ItemType VARCHAR(30), DropShip BIT, bitDiscountType BIT,	fltDiscount DECIMAL(30,16), monTotAmtBefDiscount DECIMAL(20,5),
						vcItemName VARCHAR(300),numUOM NUMERIC(9),bitWorkOrder BIT,numVendorWareHouse NUMERIC(9),numShipmentMethod NUMERIC(9),numSOVendorId NUMERIC(9),
						numProjectID numeric(9),numProjectStageID numeric(9),numToWarehouseItemID NUMERIC(9),Attributes varchar(500),AttributeIDs VARCHAR(500),vcSKU VARCHAR(100)
						,bitItemPriceApprovalRequired BIT,numPromotionID NUMERIC(18,0),bitPromotionTriggered BIT,vcPromotionDetail VARCHAR(2000),numSortOrder NUMERIC(18,0)
						,numCost DECIMAL(20,5),vcVendorNotes VARCHAR(1000),vcInstruction VARCHAR(1000),bintCompliationDate DATETIME,dtPlannedStart DATETIME,numWOAssignedTo NUMERIC(18,0)
						,numShipToAddressID  NUMERIC(18,0),ItemReleaseDate DATETIME,bitMarkupDiscount BIT,KitChildItems VARCHAR(MAX),numWOQty FLOAT
					)
				)X    
			ORDER BY 
				X.numoppitemtCode
    
			-- UPDATE EXISTING CUSTOMER PART NO
			UPDATE
				CPN
			SET
				CPN.CustomerPartNo = ISNULL(TEMPCPN.CustomerPartNo,'')
			FROM
				CustomerPartNumber CPN
			INNER JOIN
			(
				SELECT 
					numItemCode
					,MIN(CustomerPartNo) AS CustomerPartNo
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=1]',2)                                                                          
				WITH                       
				(                                                                          
					CustomerPartNo VARCHAR(300), numItemCode NUMERIC(18,0)
				)
				GROUP BY
					numItemCode
			) AS TEMPCPN
			ON
				CPN.numCompanyId = @numCompany
				AND CPN.numItemCode = TEMPCPN.numItemCode
			WHERE
				CPN.numDomainID=@numDomainID
				AND CPN.numCompanyId = @numCompany
				AND LEN(ISNULL(TEMPCPN.CustomerPartNo,'')) > 0
				AND ISNULL(TEMPCPN.CustomerPartNo,'') <> '0'
			
			-- INSERT NEW CUSTOMER PART NO
			INSERT INTO CustomerPartNumber
			(
				numCompanyId, numDomainID, CustomerPartNo, numItemCode 
			)
			SELECT 
				@numCompany, @numDomainID, TEMPCPN.CustomerPartNo,TEMPCPN.numItemCode
			FROM
			(
				SELECT 
					numItemCode
					,MIN(CustomerPartNo) AS CustomerPartNo
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=1]',2)                                                                          
				WITH                       
				(                                                                          
					CustomerPartNo VARCHAR(300), numItemCode NUMERIC(18,0)
				)
				GROUP BY
					numItemCode
			) TEMPCPN 
			LEFT JOIN
				CustomerPartNumber CPN
			ON
				CPN.numCompanyId = @numCompany
				AND CPN.numItemCode = TEMPCPN.numItemCode
			WHERE
				CPN.CustomerPartNoID IS NULL
				AND LEN(ISNULL(TEMPCPN.CustomerPartNo,'')) > 0
				AND ISNULL(TEMPCPN.CustomerPartNo,'') <> '0'

			UPDATE 
				OpportunityItems                       
			SET 
				numItemCode=X.numItemCode,numOppId=@numOppID,numUnitHour=dbo.fn_UOMConversion(X.numUOM,X.numItemCode,@numDomainId,null) * X.numUnitHour,                      
				monPrice=x.monPrice,monTotAmount=x.monTotAmount,vcItemDesc=X.vcItemDesc,numWarehouseItmsID=X.numWarehouseItmsID,bitDropShip=X.DropShip,
				bitDiscountType=X.bitDiscountType,fltDiscount=X.fltDiscount,monTotAmtBefDiscount=X.monTotAmtBefDiscount,vcItemName=X.vcItemName,numUOMId=numUOM,
				bitWorkOrder=X.bitWorkOrder,numVendorWareHouse=X.numVendorWareHouse,numShipmentMethod=X.numShipmentMethod,numSOVendorId=X.numSOVendorId,
				numProjectId=X.numProjectID,numProjectStageId=X.numProjectStageID,numToWarehouseItemID = X.numToWarehouseItemID,vcAttributes=X.Attributes,
				numClassID=0,vcModelID=X.vcModelID,
				bitItemPriceApprovalRequired = X.bitItemPriceApprovalRequired,vcAttrValues=X.AttributeIDs,
				numPromotionID=X.numPromotionID,bitPromotionTriggered=X.bitPromotionTriggered,vcPromotionDetail=X.vcPromotionDetail,numSortOrder=X.numSortOrder,numCost=X.numCost
				,vcNotes=X.vcVendorNotes,vcInstruction=X.vcInstruction,bintCompliationDate=DATEADD(MINUTE,@ClientTimeZoneOffset,X.bintCompliationDate),dtPlannedStart=DATEADD(MINUTE,@ClientTimeZoneOffset,X.dtPlannedStart),numWOAssignedTo=X.numWOAssignedTo,ItemRequiredDate=X.ItemRequiredDate
				,bitMarkupDiscount=X.bitMarkupDiscount,vcChildKitSelectedItems=ISNULL(X.KitChildItems,'')
			FROM 
			(
				SELECT 
					numoppitemtCode as numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,vcModelID,numWarehouseItmsID,DropShip,bitDiscountType,fltDiscount,
					monTotAmtBefDiscount,vcItemName,numUOM,bitWorkOrder,numVendorWareHouse,numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,numToWarehouseItemID,
					Attributes,bitItemPriceApprovalRequired,AttributeIDs,numPromotionID,bitPromotionTriggered,vcPromotionDetail,numSortOrder,ISNULL(numCost,0) numCost
					,ISNULL(vcVendorNotes,'') vcVendorNotes,vcInstruction,bintCompliationDate,dtPlannedStart,numWOAssignedTo,ItemRequiredDate,bitMarkupDiscount,KitChildItems
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=2]',2)
				WITH  
					(                      
						numoppitemtCode NUMERIC(9),numItemCode NUMERIC(9),numUnitHour FLOAT,monPrice DECIMAL(30,16),monTotAmount DECIMAL(20,5),vcItemDesc VARCHAR(2000), vcModelID VARCHAR(300),
						numWarehouseItmsID NUMERIC(9),DropShip BIT,bitDiscountType BIT,fltDiscount DECIMAL(30,16),monTotAmtBefDiscount DECIMAL(20,5),vcItemName VARCHAR(300),
						numUOM NUMERIC(9),bitWorkOrder BIT,numVendorWareHouse NUMERIC(9),numShipmentMethod NUMERIC(9),numSOVendorId NUMERIC(9),numProjectID NUMERIC(9),
						numProjectStageID numeric(9),numToWarehouseItemID NUMERIC(9),Attributes varchar(500),bitItemPriceApprovalRequired BIT,AttributeIDs VARCHAR(500),
						numPromotionID NUMERIC(18,0),bitPromotionTriggered BIT,vcPromotionDetail VARCHAR(2000),numSortOrder NUMERIC(18,0),numCost DECIMAL(20,5),vcVendorNotes VARCHAR(1000)
						,vcInstruction VARCHAR(1000),bintCompliationDate DATETIME,dtPlannedStart DATETIME,numWOAssignedTo NUMERIC(18,0),ItemRequiredDate DATETIME,bitMarkupDiscount BIT,KitChildItems VARCHAR(MAX)
					)
			)X 
			WHERE 
				numoppitemtCode=X.numOppItemID AND numOppID=@numOppID
				
			--INSERT KIT ITEM HAS ANOTHER KIT AS CHILD THEN SAVE CONFIGURATION OF KIT
			DECLARE @TempKitConfiguration TABLE
			(
				numOppItemID NUMERIC(18,0),
				numItemCode NUMERIC(18,0),
				numKitItemID NUMERIC(18,0),
				numKitChildItemID NUMERIC(18,0),
				numQty FLOAT,
				numSequence INT
			)

			INSERT INTO @TempKitConfiguration
			(
				numOppItemID,
				numItemCode,
				numKitItemID,
				numKitChildItemID,
				numQty,
				numSequence
			)
			SELECT
				numoppitemtCode,
				numItemCode,
				numKitItemID,
				numChildItemID,
				numQty,
				numSequence
			FROM
				OpportunityItems
			CROSS APPLY
			(
				SELECT 
					Reverse(ParseName(Replace(Reverse(OutParam), '-', '.'), 1)) As numKitItemID
					,Reverse(ParseName(Replace(Reverse(OutParam), '-', '.'), 2)) As numChildItemID
					,Reverse(ParseName(Replace(Reverse(OutParam), '-', '.'), 3)) As numQty
					,Reverse(ParseName(Replace(Reverse(OutParam), '-', '.'), 4)) As numSequence
				FROM  
				(
					SELECT 
						OutParam 
					FROM 
						SplitString(vcChildKitSelectedItems,',')
				) X
			) Y
			WHERE
				numOppId = @numOppID
				AND LEN(ISNULL(vcChildKitSelectedItems,'')) > 0
			ORDER BY
				ISNULL(Y.numSequence,0)

			--INSERT KIT ITEMS 
			INSERT INTO OpportunityKitItems
			(
				numOppId,
				numOppItemID,
				numChildItemID,
				numWareHouseItemId,
				numQtyItemsReq,
				numQtyItemsReq_Orig,
				numUOMId,
				numQtyShipped,
				monAvgCost
			)
			SELECT
				@numOppId,
				OI.numoppitemtCode,
				T1.numKitChildItemID,
				ISNULL((SELECT TOP 1 numWarehouseItemID FROM WareHouseItems WHERE numItemID=T1.numKitChildItemID AND WareHouseItems.numWareHouseID = W.numWarehouseID ORDER BY numWareHouseItemID),0),
				T1.numQty * OI.numUnitHour,
				T1.numQty,
				I.numBaseUnit,
				0,
				ISNULL(I.monAverageCost,0)
			FROM 
				OpportunityItems OI 
			LEFT JOIN
				WarehouseItems WI
			ON
				OI.numWarehouseItmsID = WI.numWarehouseItemID
			LEFT JOIN
				Warehouses W
			ON
				WI.numWarehouseID = W.numWarehouseID
			INNER JOIN
				@TempKitConfiguration T1
			ON
				T1.numOppItemID=OI.numoppitemtCode
				AND T1.numItemCode = OI.numItemCode
			JOIN
				Item I
			ON
				T1.numKitChildItemID = I.numItemCode
			WHERE 
				OI.numOppId=@numOppId
				AND (SELECT COUNT(*) FROM @TempKitConfiguration t2 WHERE t2.numOppItemID=OI.numoppitemtCode AND t2.numItemCode=OI.numItemCode AND ISNULL(t2.numKitItemID,0) = 0) > 0
			ORDER BY
				ISNULL(T1.numSequence,0)

			INSERT INTO OpportunityKitItems
			(
				numOppId,
				numOppItemID,
				numChildItemID,
				numWareHouseItemId,
				numQtyItemsReq,
				numQtyItemsReq_Orig,
				numUOMId,
				numQtyShipped,
				monAvgCost
			)
			SELECT
				@numOppId,
				OI.numoppitemtCode,
				ID.numChildItemID,
				ISNULL((SELECT TOP 1 numWarehouseItemID FROM WareHouseItems WHERE numItemID=ID.numChildItemID AND WareHouseItems.numWareHouseID = W.numWarehouseID ORDER BY numWareHouseItemID),0),
				(ID.numQtyItemsReq * ISNULL(dbo.fn_UOMConversion(ID.numUOMId,ID.numChildItemID,@numDomainID,I.numBaseUnit),1)) * OI.numUnitHour,
				ID.numQtyItemsReq,
				ID.numUOMId,
				0,
				ISNULL(I.monAverageCost,0)
			FROM 
				OpportunityItems OI 
			LEFT JOIN
				WarehouseItems WI
			ON
				OI.numWarehouseItmsID = WI.numWarehouseItemID
			LEFT JOIN
				Warehouses W
			ON
				WI.numWarehouseID = W.numWarehouseID
			JOIN 
				ItemDetails ID 
			ON 
				OI.numItemCode=ID.numItemKitID 
			JOIN
				Item I
			ON
				ID.numChildItemID = I.numItemCode
			WHERE 
				OI.numOppId=@numOppId
				AND (SELECT COUNT(*) FROM @TempKitConfiguration t2 WHERE t2.numOppItemID=OI.numoppitemtCode AND t2.numItemCode=OI.numItemCode AND ISNULL(t2.numKitItemID,0) = 0) = 0
			ORDER BY
				ISNULL(ID.sintOrder,0)

			IF (SELECT 
					COUNT(*) 
				FROM 
					OpportunityKitItems OI 
				INNER JOIN
					Item I
				ON
					OI.numChildItemID = I.numItemCode
				WHERE 
					numOppId = @numOppID
					AND ISNULL(charItemType,0) = 'P'
					AND numWareHouseItemId = 0) > 0
			BEGIN
				RAISERROR('SELECTED_WAREHOUSE_IS_NOT_AVAILABLE_FOR_ALL_CHILD_ITEMS',16,1)
			END

			IF (SELECT COUNT(*) FROM OPENXML (@hDocItem,'/NewDataSet/Item[bitHasKitAsChild=''true'']',2)) > 0
			BEGIN
				IF (SELECT 
						COUNT(*) 
					FROM 
					(
						SELECT 
							t1.numOppItemID
							,t1.numItemCode
							,numKitItemID
							,numKitChildItemID
						FROM 
							@TempKitConfiguration t1
						WHERE
							ISNULL(t1.numKitItemID,0) > 0
					) TempChildItems
					INNER JOIN
						OpportunityKitItems OKI
					ON
						OKI.numOppId=@numOppId
						AND OKI.numOppItemID=TempChildItems.numOppItemID
						AND OKI.numChildItemID=TempChildItems.numKitItemID
					INNER JOIN
						OpportunityItems OI
					ON
						OI.numItemCode = TempChildItems.numItemCode
						AND OI.numoppitemtcode = OKI.numOppItemID
					LEFT JOIN
						WarehouseItems WI
					ON
						OI.numWarehouseItmsID = WI.numWarehouseItemID
					LEFT JOIN
						Warehouses W
					ON
						WI.numWarehouseID = W.numWarehouseID
					INNER JOIN
						ItemDetails
					ON
						TempChildItems.numKitItemID = ItemDetails.numItemKitID
						AND TempChildItems.numKitChildItemID = ItemDetails.numChildItemID
					INNER JOIN
						Item 
					ON
						ItemDetails.numChildItemID = Item.numItemCode
					INNER JOIN
						Item IMain
					ON
						OKI.numChildItemID = IMain.numItemCode
					WHERE
						Item.charItemType = 'P'
						AND ISNULL(IMain.bitKitParent,0) = 1
						AND ISNULL((SELECT TOP 1 numWarehouseItemID FROM WareHouseItems WHERE numItemID=ItemDetails.numChildItemID AND numWareHouseID = W.numWarehouseID),0) = 0) > 0
				BEGIN
					RAISERROR('SELECTED_WAREHOUSE_IS_NOT_AVAILABLE_FOR_ALL_CHILD_ITEMS',16,1)
				END

				INSERT INTO OpportunityKitChildItems
				(
					numOppID,
					numOppItemID,
					numOppChildItemID,
					numItemID,
					numWareHouseItemId,
					numQtyItemsReq,
					numQtyItemsReq_Orig,
					numUOMId,
					numQtyShipped,
					monAvgCost
				)
				SELECT 
					@numOppID,
					OKI.numOppItemID,
					OKI.numOppChildItemID,
					ItemDetails.numChildItemID,
					(SELECT TOP 1 numWarehouseItemID FROM WareHouseItems WHERE numItemID=ItemDetails.numChildItemID AND WareHouseItems.numWareHouseID = W.numWarehouseID ORDER BY numWareHouseItemID),
					(ItemDetails.numQtyItemsReq * ISNULL(dbo.fn_UOMConversion(ItemDetails.numUOMId,ItemDetails.numChildItemID,@numDomainID,Item.numBaseUnit),1)) * OKI.numQtyItemsReq,
					ItemDetails.numQtyItemsReq,
					ItemDetails.numUOMId,
					0,
					ISNULL(Item.monAverageCost,0)
				FROM
				(
					SELECT 
						t1.numOppItemID
						,t1.numItemCode
						,numKitItemID
						,numKitChildItemID
					FROM 
						@TempKitConfiguration t1
					WHERE
						ISNULL(t1.numKitItemID,0) > 0
				) TempChildItems
				INNER JOIN
					OpportunityKitItems OKI
				ON
					OKI.numOppId=@numOppId
					AND OKI.numOppItemID=TempChildItems.numOppItemID
					AND OKI.numChildItemID=TempChildItems.numKitItemID
				INNER JOIN
					OpportunityItems OI
				ON
					OI.numItemCode = TempChildItems.numItemCode
					AND OI.numoppitemtcode = OKI.numOppItemID
				LEFT JOIN
					WarehouseItems WI
				ON
					OI.numWarehouseItmsID = WI.numWarehouseItemID
				LEFT JOIN
					Warehouses W
				ON
					WI.numWarehouseID = W.numWarehouseID
				INNER JOIN
					ItemDetails
				ON
					TempChildItems.numKitItemID = ItemDetails.numItemKitID
					AND TempChildItems.numKitChildItemID = ItemDetails.numChildItemID
				INNER JOIN
					Item 
				ON
					ItemDetails.numChildItemID = Item.numItemCode
			END

			-- UPDATE UOM OF OPPORTUNITY ITEMS HAVE NOT UOM SET WHILE EXPORTED FROM MARKETPLACE (MEANS TINTSOURCETYPE = 3)
			IF ISNULL(@tintSourceType,0) = 3
			BEGIN
				UPDATE 
					OI 
				SET 
					numUOMID = ISNULL(I.numSaleUnit,0), 
					numUnitHour = dbo.fn_UOMConversion(ISNULL(I.numSaleUnit,0),OI.numItemCode,@numDomainId,null) * OI.numUnitHour
				FROM 
					OpportunityItems OI
				JOIN 
					Item I 
				ON 
					OI.numItemCode = I.numItemCode
				WHERE 
					numOppId = @numOppID
					AND I.numDomainID = @numDomainId
			END	
                                     
			INSERT INTO OppWarehouseSerializedItem
			(
				numOppID,
				numWarehouseItmsDTLID,
				numOppItemID,
				numWarehouseItmsID
			)                      
			SELECT 
				@numOppID,
				X.numWarehouseItmsDTLID,
				X.numoppitemtCode,
				X.numWItmsID 
			FROM
			(                                                                          
				SELECT
					* 
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/SerialNo',2)                                                                          
				WITH                        
				(                                               
				numWarehouseItmsDTLID numeric(9),                      
				numoppitemtCode numeric(9),                      
				numWItmsID numeric(9)                                                     
				)
			)X                                    
                                   
			UPDATE 
				OppWarehouseSerializedItem                       
			SET 
				numOppItemID=X.numoppitemtCode                                
			FROM                       
			(
				SELECT 
					numWarehouseItmsDTLID AS DTLID,
					O.numoppitemtCode 
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/SerialNo',2)                                                                         
				WITH
				(
					numWarehouseItmsDTLID NUMERIC(9),numoppitemtCode NUMERIC(9),numWItmsID NUMERIC(9)
				)                                
				JOIN 
					OpportunityItems O 
				ON 
					O.numWarehouseItmsID=numWItmsID and O.numOppID=@numOppID
			)X                 
			WHERE 
				numOppID=@numOppID 
				AND numWarehouseItmsDTLID=X.DTLID      
    
			IF NOT (@tintCommitAllocation=2 AND @bitAllocateInventoryOnPickList=1)
			BEGIN
				EXEC USP_ManageSOWorkOrder @numDomainId,@numOppID,@numUserCntID
			END
                     
			EXEC sp_xml_removedocument @hDocItem                                     
		END          
	END
	ELSE                                                                                                                          
	BEGIN

		DECLARE @tempAssignedTo AS NUMERIC(9) = NULL                                                    
		SELECT 
			@tintOppStatus = tintOppStatus,
			@tempAssignedTo=ISNULL(numAssignedTo,0) 
		FROM   
			OpportunityMaster 
		WHERE  
			numOppID = @numOppID

		IF @tempAssignedTo <> @numAssignedTo
		BEGIN
			IF (SELECT COUNT(*) FROM BizDocComission WHERE ISNULL(numOppID,0) = @numOppID AND ISNULL(bitCommisionPaid,0) = 1) > 0
			BEGIN
				RAISERROR('COMMISSION_PAID_AGAINST_BIZDOC',16,1)
			END
		END
 
		--REVERTING BACK THE WAREHOUSE ITEMS                  
		IF @tintOppStatus = 1 AND @tintCommitAllocation=1
		BEGIN               
			EXEC USP_RevertDetailsOpp @numOppID,0,0,@numUserCntID                  
		END                  

		IF LEN(ISNULL(@vcOppRefOrderNo,''))>0
		BEGIN
			UPDATE dbo.OpportunityBizDocs SET vcRefOrderNo=@vcOppRefOrderNo WHERE numOppId=@numOppID
		END
		
		IF EXISTS (SELECT 1 FROM dbo.OpportunityMaster WHERE numOppId = @numOppID AND tintOppType=1 AND isnull(tintOppStatus,0)=1 AND ISNULL(numStatus,0)!= @numStatus)
		BEGIN
			EXEC USP_ManageOpportunityAutomationQueue
					@numOppQueueID = 0, -- numeric(18, 0)
					@numDomainID = @numDomainID, -- numeric(18, 0)
					@numOppId = @numOppID, -- numeric(18, 0)
					@numOppBizDocsId = 0, -- numeric(18, 0)
					@numOrderStatus = @numStatus, -- numeric(18, 0)
					@numUserCntID = @numUserCntID, -- numeric(18, 0)
					@tintProcessStatus = 1, -- tinyint
					@tintMode = 1 -- TINYINT
		END 
						
		UPDATE   
			OpportunityMaster
		SET      
			vcPOppName = @vcPOppName,txtComments = ISNULL(@Comments,''),bitPublicFlag = @bitPublicFlag,numCampainID = @CampaignID,tintSource = @tintSource,tintSourceType=@tintSourceType,
			intPEstimatedCloseDate = @dtEstimatedCloseDate,numModifiedBy = @numUserCntID,bintModifiedDate = GETUTCDATE(),lngPConclAnalysis = @lngPConclAnalysis,monPAmount = @monPAmount,
			tintActive = @tintActive,numSalesOrPurType = @numSalesOrPurType,numStatus = @numStatus,vcOppRefOrderNo=@vcOppRefOrderNo,
			bintOppToOrder=(CASE WHEN @tintOppStatus=0 AND @DealStatus=1 THEN GETUTCDATE() ELSE bintOppToOrder END),bitDiscountType=@bitDiscountType,fltDiscount=@fltDiscount,
			bitBillingTerms=@bitBillingTerms,intBillingDays=@intBillingDays,bitInterestType=@bitInterestType,fltInterest=@fltInterest,tintTaxOperator=@tintTaxOperator,
			numDiscountAcntType=@numDiscountAcntType,numPercentageComplete =@numPercentageComplete,bitUseShippersAccountNo = @bitUseShippersAccountNo,
			intUsedShippingCompany = @intUsedShippingCompany, numContactID=@numContactId,numShipmentMethod=@numShipmentMethod,numShippingService=@numShippingService
			,numPartner=@numPartner,numPartenerContact=@numPartenerContactId,numProjectID=@numProjectID
		WHERE
			numOppId = @numOppID   
	   
		-- UPDATING IF ORGANIZATION IS ASSIGNED TO SOMEONE                                                      
		IF ( @tempAssignedTo <> @numAssignedTo AND @numAssignedTo <> '0' ) 
		BEGIN
			UPDATE
				OpportunityMaster 
			SET	
				numAssignedTo = @numAssignedTo, 
				numAssignedBy = @numUserCntID 
			WHERE 
				numOppId = @numOppID                                                    
		END                                                     
		ELSE IF(@numAssignedTo = 0) 
		BEGIN
			UPDATE 
				OpportunityMaster 
			SET 
				numAssignedTo = 0, 
				numAssignedBy = 0 
			WHERE
				numOppId = @numOppID
		END  
			                                                                    
		-- UPDATING OPP ITEMS
		IF CONVERT(VARCHAR(10),@strItems) <> '' AND @numOppID > 0                                                                         
		BEGIN
			EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems                                      
			   
			IF EXISTS 
			(
				SELECT 
					OI.numoppitemtCode
				FROM 
					OpportunityItems OI
				WHERE 
					numOppID=@numOppID 
					AND OI.numoppitemtCode NOT IN (SELECT numoppitemtCode FROM OPENXML (@hDocItem,'/NewDataSet/Item',2) WITH (numoppitemtCode NUMERIC(9)))
					AND ISNULL((SELECT COUNT(*) FROM OppWarehouseSerializedItem WHERE numOppId=@numOppID AND numOppItemID=OI.numoppitemtCode),0) > 0
			)
			BEGIN
				RAISERROR('SERIAL/LOT#_ASSIGNED_TO_ITEM_DELETED',16,1)
			END

			DELETE FROM OpportunityKitChildItems WHERE numOppID=@numOppID   
			DELETE FROM OpportunityKitItems WHERE numOppID=@numOppID                  
			DELETE FROM OpportunityItems WHERE numOppID=@numOppID AND numoppitemtCode NOT IN (SELECT numoppitemtCode FROM OPENXML (@hDocItem,'/NewDataSet/Item',2) WITH (numoppitemtCode NUMERIC(9))) 
	          
			-- DELETE CORRESPONSING TIME AND EXPENSE ENTRIES OF DELETED ITEMS
			DELETE FROM
				TimeAndExpense
			WHERE
				numDomainID=@numDomainId
				AND numOppId=@numOppID
				AND numOppItemID NOT IN (SELECT numoppitemtCode FROM OPENXML (@hDocItem,'/NewDataSet/Item',2) WITH (numoppitemtCode NUMERIC(9))) 

			--INSERT NEW ADDED ITEMS                        
			INSERT INTO OpportunityItems
			(
				numOppId,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount,
				[vcItemName],[vcModelID],[vcManufacturer],vcPathForTImage,monVendorCost,numUOMId,bitWorkOrder,numVendorWareHouse,numToWarehouseItemID,numShipmentMethod,
				numSOVendorId,numProjectID,numProjectStageID,vcAttributes,numClassID,monAvgCost,bitItemPriceApprovalRequired,vcAttrValues,numPromotionID,bitPromotionTriggered
				,vcPromotionDetail,numSortOrder,numCost,vcNotes,vcInstruction,bintCompliationDate,dtPlannedStart,numWOAssignedTo,bitMarkupDiscount,ItemReleaseDate,vcChildKitSelectedItems,numWOQty
			)
			SELECT 
				@numOppID,X.numItemCode,dbo.fn_UOMConversion(X.numUOM,numItemCode,@numDomainId,null) * x.numUnitHour,x.monPrice,x.monTotAmount,X.vcItemDesc,X.numWarehouseItmsID,
				X.ItemType,X.DropShip,X.bitDiscountType,X.fltDiscount,X.monTotAmtBefDiscount,X.vcItemName,(SELECT vcModelID FROM item WHERE numItemCode = X.numItemCode),
				(SELECT vcManufacturer FROM item WHERE numItemCode = X.numItemCode),
				(SELECT TOP 1 vcPathForTImage FROM dbo.ItemImages WHERE numItemCode = X.numItemCode AND BitDefault = 1 AND numDomainId = @numDomainId),
				(select isnull(VN.monCost,0) from Vendor VN INNER JOIN Item IT ON IT.numItemCode=VN.numItemCode and VN.numVendorID=IT.numVendorID where VN.numItemCode=X.numItemCode),
				X.numUOM,X.bitWorkOrder,X.numVendorWareHouse,X.numToWarehouseItemID,X.numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,Attributes,
				0,
				(SELECT (CASE WHEN ISNULL(I.bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(I.monAverageCost,0) END) FROM Item I WHERE I.numDomainId=@numDomainId AND I.numItemCode=X.numItemCode), X.bitItemPriceApprovalRequired, X.AttributeIDs,
				X.numPromotionID,X.bitPromotionTriggered,X.vcPromotionDetail,X.numSortOrder,ISNULL(X.numCost,0),ISNULL(vcVendorNotes,'')
				,X.vcInstruction,DATEADD(MINUTE,@ClientTimeZoneOffset,X.bintCompliationDate),DATEADD(MINUTE,@ClientTimeZoneOffset,X.dtPlannedStart),X.numWOAssignedTo,X.bitMarkupDiscount,@dtItemRelease,ISNULL(KitChildItems,''),X.numWOQty
			FROM
			(
				SELECT 
					*
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=1]',2)                                                                          
				WITH  
				(                      
					numoppitemtCode numeric(9),numItemCode numeric(9),numUnitHour FLOAT,monPrice DECIMAL(30,16),monTotAmount DECIMAL(20,5),vcItemDesc varchar(2000),                                    
					numWarehouseItmsID numeric(9),numToWarehouseItemID numeric(9),Op_Flag tinyint,ItemType varchar(30),DropShip bit,bitDiscountType bit,
					fltDiscount DECIMAL(30,16),monTotAmtBefDiscount DECIMAL(20,5),vcItemName VARCHAR(300),numUOM NUMERIC(9),bitWorkOrder BIT,numVendorWareHouse NUMERIC(9),
					numShipmentMethod NUMERIC(9),numSOVendorId numeric(9),numProjectID numeric(9),numProjectStageID numeric(9),Attributes varchar(500), 
					bitItemPriceApprovalRequired BIT,AttributeIDs VARCHAR(500),numPromotionID NUMERIC(18,0),bitPromotionTriggered BIT,vcPromotionDetail VARCHAR(2000)
					,numSortOrder NUMERIC(18,0),numCost DECIMAL(20,5),vcVendorNotes VARCHAR(1000),vcInstruction VARCHAR(1000),bintCompliationDate DATETIME,dtPlannedStart DATETIME
					,numWOAssignedTo NUMERIC(18,0),bitMarkupDiscount BIT,KitChildItems VARCHAR(MAX),numWOQty FLOAT
				)
			)X 
			
			-- UPDATE EXISTING CUSTOMER PART NO
			UPDATE
				CPN
			SET
				CPN.CustomerPartNo = ISNULL(TEMPCPN.CustomerPartNo,'')
			FROM
				CustomerPartNumber CPN
			INNER JOIN
			(
				SELECT 
					numItemCode
					,MIN(CustomerPartNo) AS CustomerPartNo
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item',2)                                                                          
				WITH                       
				(                                                                          
					CustomerPartNo VARCHAR(300), numItemCode NUMERIC(18,0)
				)
				GROUP BY
					numItemCode
			) AS TEMPCPN
			ON
				CPN.numCompanyId = @numCompany
				AND CPN.numItemCode = TEMPCPN.numItemCode
			WHERE
				CPN.numDomainID=@numDomainID
				AND CPN.numCompanyId = @numCompany
				AND LEN(ISNULL(TEMPCPN.CustomerPartNo,'')) > 0
				AND ISNULL(TEMPCPN.CustomerPartNo,'') <> '0'
			
			-- INSERT NEW CUSTOMER PART NO
			INSERT INTO CustomerPartNumber
			(
				numCompanyId, numDomainID, CustomerPartNo, numItemCode 
			)
			SELECT 
				@numCompany, @numDomainID, TEMPCPN.CustomerPartNo,TEMPCPN.numItemCode
			FROM
			(
				SELECT 
					numItemCode
					,MIN(CustomerPartNo) AS CustomerPartNo
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item',2)                                                                          
				WITH                       
				(                                                                          
					CustomerPartNo VARCHAR(300), numItemCode NUMERIC(18,0)
				)
				GROUP BY
					numItemCode
			) TEMPCPN 
			LEFT JOIN
				CustomerPartNumber CPN
			ON
				CPN.numCompanyId = @numCompany
				AND CPN.numItemCode = TEMPCPN.numItemCode
			WHERE
				CPN.CustomerPartNoID IS NULL
				AND LEN(ISNULL(TEMPCPN.CustomerPartNo,'')) > 0
				AND ISNULL(TEMPCPN.CustomerPartNo,'') <> '0'
			
			-- UPDATE ITEMS
			UPDATE 
				OpportunityItems                       
			SET 
				numItemCode=X.numItemCode,numOppId=@numOppID,numUnitHour=dbo.fn_UOMConversion(X.numUOM,X.numItemCode,@numDomainId,null) * x.numUnitHour,                      
				monPrice=x.monPrice,monTotAmount=x.monTotAmount,vcItemDesc=X.vcItemDesc,vcModelID=X.vcModelID,numWarehouseItmsID=X.numWarehouseItmsID,numToWarehouseItemID = X.numToWarehouseItemID,             
				bitDropShip=X.DropShip,bitDiscountType=X.bitDiscountType,fltDiscount=X.fltDiscount,monTotAmtBefDiscount=X.monTotAmtBefDiscount,vcItemName=X.vcItemName,
				numUOMId=numUOM,numVendorWareHouse=X.numVendorWareHouse,numShipmentMethod=X.numShipmentMethod,numSOVendorId=X.numSOVendorId,numProjectId=X.numProjectID,
				numProjectStageId=X.numProjectStageID,vcAttributes=X.Attributes,bitItemPriceApprovalRequired=X.bitItemPriceApprovalRequired,bitWorkOrder=X.bitWorkOrder,vcAttrValues=AttributeIDs,
				numPromotionID=X.numPromotionID,bitPromotionTriggered=X.bitPromotionTriggered,vcPromotionDetail=X.vcPromotionDetail,numSortOrder=X.numSortOrder,numCost=X.numCost,vcNotes=vcVendorNotes
				,bitMarkupDiscount=X.bitMarkupDiscount,ItemRequiredDate=X.ItemRequiredDate,vcChildKitSelectedItems=ISNULL(KitChildItems,'')
			FROM 
			(
				SELECT 
					numoppitemtCode as numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,vcModelID,numWarehouseItmsID,numToWarehouseItemID,DropShip,bitDiscountType,
					fltDiscount,monTotAmtBefDiscount,vcItemName,numUOM,numVendorWareHouse,numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,Attributes,
					bitItemPriceApprovalRequired,bitWorkOrder,AttributeIDs,numPromotionID,bitPromotionTriggered,vcPromotionDetail,numSortOrder,ISNULL(numCost,0) AS numCost,ISNULL(vcVendorNotes,'') vcVendorNotes,
					ItemRequiredDate,bitMarkupDiscount,KitChildItems
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=2]',2)                                                                
				WITH  
				(                      
					numoppitemtCode NUMERIC(9),numItemCode NUMERIC(9),numUnitHour FLOAT,monPrice DECIMAL(30,16),monTotAmount DECIMAL(20,5),vcItemDesc VARCHAR(2000),vcModelID VARCHAR(300), numWarehouseItmsID NUMERIC(9), 
					numToWarehouseItemID NUMERIC(18,0),DropShip BIT,bitDiscountType BIT,fltDiscount DECIMAL(30,16),monTotAmtBefDiscount DECIMAL(20,5),vcItemName VARCHAR(300),numUOM NUMERIC(9),
					numVendorWareHouse NUMERIC(9),numShipmentMethod NUMERIC(9),numSOVendorId NUMERIC(9),numProjectID NUMERIC(9),numProjectStageID NUMERIC(9),Attributes VARCHAR(500),
					bitItemPriceApprovalRequired BIT, bitWorkOrder BIT,AttributeIDs VARCHAR(500),numPromotionID NUMERIC(18,0),bitPromotionTriggered BIT,vcPromotionDetail VARCHAR(2000),numSortOrder NUMERIC(18,0),numCost DECIMAL(20,5),vcVendorNotes VARCHAR(1000),
					ItemRequiredDate DATETIME, bitMarkupDiscount BIT,KitChildItems VARCHAR(MAX)
				)
			)X 
			WHERE 
				numoppitemtCode=X.numOppItemID AND numOppId=@numOppID

			--INSERT KIT ITEM HAS ANOTHER KIT AS CHILD THEN SAVE CONFIGURATION OF KIT
			DELETE FROM @TempKitConfiguration

			INSERT INTO @TempKitConfiguration
			(
				numOppItemID,
				numItemCode,
				numKitItemID,
				numKitChildItemID,
				numQty,
				numSequence
			)
			SELECT
				numoppitemtCode,
				numItemCode,
				numKitItemID,
				numChildItemID,
				numQty,
				numSequence
			FROM
				OpportunityItems
			CROSS APPLY
			(
				SELECT 
					Reverse(ParseName(Replace(Reverse(OutParam), '-', '.'), 1)) As numKitItemID
					,Reverse(ParseName(Replace(Reverse(OutParam), '-', '.'), 2)) As numChildItemID
					,Reverse(ParseName(Replace(Reverse(OutParam), '-', '.'), 3)) As numQty
					,Reverse(ParseName(Replace(Reverse(OutParam), '-', '.'), 4)) As numSequence
				FROM  
				(
					SELECT 
						OutParam 
					FROM 
						SplitString(vcChildKitSelectedItems,',')
				) X
			) Y
			WHERE
				numOppId = @numOppID
				AND LEN(ISNULL(vcChildKitSelectedItems,'')) > 0
			ORDER BY
				ISNULL(Y.numSequence,0)

			--INSERT KIT ITEMS 
			INSERT INTO OpportunityKitItems
			(
				numOppId,
				numOppItemID,
				numChildItemID,
				numWareHouseItemId,
				numQtyItemsReq,
				numQtyItemsReq_Orig,
				numUOMId,
				numQtyShipped,
				monAvgCost
			)
			SELECT
				@numOppId,
				OI.numoppitemtCode,
				T1.numKitChildItemID,
				ISNULL((SELECT TOP 1 numWarehouseItemID FROM WareHouseItems WHERE numItemID=T1.numKitChildItemID AND WareHouseItems.numWareHouseID = W.numWarehouseID ORDER BY numWareHouseItemID),0),
				T1.numQty * OI.numUnitHour,
				T1.numQty,
				I.numBaseUnit,
				0,
				ISNULL(I.monAverageCost,0)
			FROM 
				OpportunityItems OI 
			LEFT JOIN
				WarehouseItems WI
			ON
				OI.numWarehouseItmsID = WI.numWarehouseItemID
			LEFT JOIN
				Warehouses W
			ON
				WI.numWarehouseID = W.numWarehouseID
			INNER JOIN
				@TempKitConfiguration T1
			ON
				T1.numOppItemID=OI.numoppitemtCode
				AND T1.numItemCode = OI.numItemCode
			JOIN
				Item I
			ON
				T1.numKitChildItemID = I.numItemCode
			WHERE 
				OI.numOppId=@numOppId
				AND (SELECT COUNT(*) FROM @TempKitConfiguration t2 WHERE t2.numOppItemID=OI.numoppitemtCode AND t2.numItemCode=OI.numItemCode AND ISNULL(t2.numKitItemID,0) = 0) > 0
			ORDER BY
				ISNULL(T1.numSequence,0)

			INSERT INTO OpportunityKitItems
			(
				numOppId,
				numOppItemID,
				numChildItemID,
				numWareHouseItemId,
				numQtyItemsReq,
				numQtyItemsReq_Orig,
				numUOMId,
				numQtyShipped,
				monAvgCost
			)
			SELECT
				@numOppId,
				OI.numoppitemtCode,
				ID.numChildItemID,
				ISNULL((SELECT TOP 1 numWarehouseItemID FROM WareHouseItems WHERE numItemID=ID.numChildItemID AND WareHouseItems.numWareHouseID = W.numWarehouseID ORDER BY numWareHouseItemID),0),
				(ID.numQtyItemsReq * ISNULL(dbo.fn_UOMConversion(ID.numUOMId,ID.numChildItemID,@numDomainID,I.numBaseUnit),1)) * OI.numUnitHour,
				ID.numQtyItemsReq,
				ID.numUOMId,
				0,
				ISNULL(I.monAverageCost,0)
			FROM 
				OpportunityItems OI 
			LEFT JOIN
				WarehouseItems WI
			ON
				OI.numWarehouseItmsID = WI.numWarehouseItemID
			LEFT JOIN
				Warehouses W
			ON
				WI.numWarehouseID = W.numWarehouseID
			JOIN 
				ItemDetails ID 
			ON 
				OI.numItemCode=ID.numItemKitID 
			JOIN
				Item I
			ON
				ID.numChildItemID = I.numItemCode
			WHERE 
				OI.numOppId=@numOppId
				AND (SELECT COUNT(*) FROM @TempKitConfiguration t2 WHERE t2.numOppItemID=OI.numoppitemtCode AND t2.numItemCode=OI.numItemCode AND ISNULL(t2.numKitItemID,0) = 0) = 0
			ORDER BY
				ISNULL(ID.sintOrder,0)

			IF (SELECT 
					COUNT(*) 
				FROM 
					OpportunityKitItems OI 
				INNER JOIN
					Item I
				ON
					OI.numChildItemID = I.numItemCode
				WHERE 
					numOppId=@numOppID
					AND ISNULL(charItemType,0) = 'P'
					AND numWareHouseItemId = 0) > 0
			BEGIN
				RAISERROR('SELECTED_WAREHOUSE_IS_NOT_AVAILABLE_FOR_ALL_CHILD_ITEMS',16,1)
			END

			IF (SELECT 
					COUNT(*) 
				FROM 
				(
					SELECT 
						t1.numOppItemID
						,t1.numItemCode
						,numKitItemID
						,numKitChildItemID
					FROM 
						@TempKitConfiguration t1
					WHERE
						ISNULL(t1.numKitItemID,0) > 0
				) TempChildItems
				INNER JOIN
					OpportunityKitItems OKI
				ON
					OKI.numOppId=@numOppId
					AND OKI.numOppItemID=TempChildItems.numOppItemID
					AND OKI.numChildItemID=TempChildItems.numKitItemID
				INNER JOIN
					OpportunityItems OI
				ON
					OI.numItemCode = TempChildItems.numItemCode
					AND OI.numoppitemtcode = OKI.numOppItemID
				LEFT JOIN
					WarehouseItems WI
				ON
					OI.numWarehouseItmsID = WI.numWarehouseItemID
				LEFT JOIN
					Warehouses W
				ON
					WI.numWarehouseID = W.numWarehouseID
				INNER JOIN
					ItemDetails
				ON
					TempChildItems.numKitItemID = ItemDetails.numItemKitID
					AND TempChildItems.numKitChildItemID = ItemDetails.numChildItemID
				INNER JOIN
					Item 
				ON
					ItemDetails.numChildItemID = Item.numItemCode
				INNER JOIN
					Item IMain
				ON
					OKI.numChildItemID = IMain.numItemCode
				WHERE
					Item.charItemType = 'P'
					AND ISNULL(IMain.bitKitParent,0) = 1
					AND ISNULL((SELECT TOP 1 numWarehouseItemID FROM WareHouseItems WHERE numItemID=ItemDetails.numChildItemID AND numWareHouseID = W.numWarehouseID),0) = 0) > 0
			BEGIN
				RAISERROR('SELECTED_WAREHOUSE_IS_NOT_AVAILABLE_FOR_ALL_CHILD_ITEMS',16,1)
			END

			INSERT INTO OpportunityKitChildItems
			(
				numOppID,
				numOppItemID,
				numOppChildItemID,
				numItemID,
				numWareHouseItemId,
				numQtyItemsReq,
				numQtyItemsReq_Orig,
				numUOMId,
				numQtyShipped,
				monAvgCost
			)
			SELECT 
				@numOppID,
				OKI.numOppItemID,
				OKI.numOppChildItemID,
				ItemDetails.numChildItemID,
				(SELECT TOP 1 numWarehouseItemID FROM WareHouseItems WHERE numItemID=ItemDetails.numChildItemID AND WareHouseItems.numWareHouseID = W.numWarehouseID ORDER BY numWareHouseItemID),
				(ItemDetails.numQtyItemsReq * ISNULL(dbo.fn_UOMConversion(ItemDetails.numUOMId,ItemDetails.numChildItemID,@numDomainID,Item.numBaseUnit),1)) * OKI.numQtyItemsReq,
				ItemDetails.numQtyItemsReq,
				ItemDetails.numUOMId,
				0,
				ISNULL(Item.monAverageCost,0)
			FROM
			(
				SELECT 
					t1.numOppItemID
					,t1.numItemCode
					,numKitItemID
					,numKitChildItemID
				FROM 
					@TempKitConfiguration t1
				WHERE
					ISNULL(t1.numKitItemID,0) > 0
			) TempChildItems
			INNER JOIN
				OpportunityKitItems OKI
			ON
				OKI.numOppId=@numOppId
				AND OKI.numOppItemID=TempChildItems.numOppItemID
				AND OKI.numChildItemID=TempChildItems.numKitItemID
			INNER JOIN
				OpportunityItems OI
			ON
				OI.numItemCode = TempChildItems.numItemCode
				AND OI.numoppitemtcode = OKI.numOppItemID
			LEFT JOIN
				WarehouseItems WI
			ON
				OI.numWarehouseItmsID = WI.numWarehouseItemID
			LEFT JOIN
				Warehouses W
			ON
				WI.numWarehouseID = W.numWarehouseID
			INNER JOIN
				ItemDetails
			ON
				TempChildItems.numKitItemID = ItemDetails.numItemKitID
				AND TempChildItems.numKitChildItemID = ItemDetails.numChildItemID
			INNER JOIN
				Item 
			ON
				ItemDetails.numChildItemID = Item.numItemCode                                                  
			                                    
			IF NOT (@tintCommitAllocation=2 AND @bitAllocateInventoryOnPickList=1)
			BEGIN
				EXEC USP_ManageSOWorkOrder @numDomainId,@numOppID,@numUserCntID
			END
			   		                                                                      
			EXEC sp_xml_removedocument @hDocItem                                               
		END     

		IF @tintOppType=1 AND @tintOppStatus=1 --Sales Order
		BEGIN
			IF EXISTS (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId=@numOppID AND ISNULL(numWarehouseItmsID,0) > 0 AND ISNULL(bitDropShip,0) = 0 AND (ISNULL(numUnitHour,0) - ISNULL(numQtyShipped,0)) < 0)
			BEGIN
				RAISERROR('EDITED_ITEMS_QTY_IS_LESS_THEN_SHIPPED_QTY',16,1)
				RETURN
			END

			IF EXISTS (SELECT numoppitemtCode FROM OpportunityItems INNER JOIN WorkOrder ON OpportunityItems.numOppID=WorkOrder.numOppID AND OpportunityItems.numoppitemtCode=WorkOrder.numOppItemID AND ISNULL(WorkOrder.numParentWOID,0)=0 AND WorkOrder.numWOStatus=23184 AND ISNULL(OpportunityItems.numUnitHour,0) > ISNULL(WorkOrder.numQtyItemsReq,0) WHERE OpportunityItems.numOppID=@numOppID)
			BEGIN
				RAISERROR('EDITED_ITEMS_QTY_CAN_NOT_BE_GREATER_THEN_COMPLETED_WORK_ORDER',16,1)
				RETURN
			END
		END
		ELSE IF @tintOppType=2 AND @tintOppStatus=1 --Purchase Order
		BEGIN
			IF EXISTS (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId=@numOppID AND ISNULL(numWarehouseItmsID,0) > 0 AND ISNULL(bitDropShip,0) = 0 AND (ISNULL(numUnitHour,0) - ISNULL(numUnitHourReceived,0)) < 0)
			BEGIN
				RAISERROR('EDITED_ITEMS_QTY_IS_LESS_THEN_RECEIVED_QTY',16,1)
				RETURN
			END
		END

		--FOLLOWING DELETE STATEMENT IS COMMENTED BECUASE NOW WE ARE NOT ALLOWING USER TO EIDT SERIAL/LOT ITEM UNTILL USER REMOVED ASSIGNED SERIAL/LOT NUMBERS
	    --Delete previously added serial lot number added for opportunity
		--DELETE FROM OppWarehouseSerializedItem WHERE numOppID = @numOppID                                                         
	END

	IF @tintOppStatus = 1
	BEGIN
		IF (SELECT COUNT(*) FROM OpportunityItems WHERE numOppId=@numOppID AND ISNULL(numWarehouseItmsID,0) > 0 AND ISNULL(bitDropShip,0)=0) <>
			(SELECT COUNT(*) FROM OpportunityItems INNER JOIN WareHouseItems ON OpportunityItems.numWarehouseItmsID=WareHouseItems.numWareHouseItemID AND OpportunityItems.numItemCode=WareHouseItems.numItemID WHERE numOppId=@numOppID AND OpportunityItems.numWarehouseItmsID > 0 AND ISNULL(bitDropShip,0)=0)
		BEGIN
			RAISERROR('INVALID_ITEM_WAREHOUSES',16,1)
			RETURN
		END
	END

	IF(SELECT 
			COUNT(*) 
		FROM 
			OpportunityBizDocItems OBDI 
		INNER JOIN 
			OpportunityBizDocs OBD 
		ON 
			OBDI.numOppBizDocID=OBD.numOppBizDocsID 
		WHERE 
			OBD.numOppId=@numOppID AND OBDI.numOppItemID NOT IN (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId=@numOppID)) > 0
	BEGIN
		RAISERROR('ITEM_USED_IN_BIZDOC',16,1)
		RETURN
	END

	IF(SELECT 
			COUNT(*)
		FROM
			OpportunityItems OI
		INNER JOIN
		(
			SELECT
				numOppItemID
				,SUM(numUnitHour) numInvoiceBillQty
			FROM 
				OpportunityBizDocItems OBDI 
			INNER JOIN 
				OpportunityBizDocs OBD 
			ON 
				OBDI.numOppBizDocID=OBD.numOppBizDocsID 
			WHERE 
				OBD.numOppId=@numOppID
				AND ISNULL(OBD.bitAuthoritativeBizDocs,0) = 1
			GROUP BY
				numOppItemID
		) AS TEMP
		ON
			TEMP.numOppItemID = OI.numoppitemtCode
		WHERE
			OI.numOppId = @numOppID
			AND OI.numUnitHour < Temp.numInvoiceBillQty
		) > 0
	BEGIN
		RAISERROR('ORDERED_QUANTITY_CAN_NOT_BE_LESS_THEN_INVOICE/BILL_QTY',16,1)
		RETURN
	END

	IF @tintOppType=1 AND @tintCommitAllocation=2 AND @bitAllocateInventoryOnPickList=1
	BEGIN
		IF(SELECT 
				COUNT(*)
			FROM
			(
				SELECT
					numOppItemID
					,SUM(numUnitHour) PackingSlipUnits
				FROM 
					OpportunityBizDocItems OBDI 
				INNER JOIN 
					OpportunityBizDocs OBD 
				ON 
					OBDI.numOppBizDocID=OBD.numOppBizDocsID 
				WHERE 
					OBD.numOppId=@numOppID
					AND OBD.numBizDocId=29397 --Picking Slip
				GROUP BY
					numOppItemID
			) AS TEMP
			INNER JOIN
				OpportunityItems OI
			ON
				TEMP.numOppItemID = OI.numoppitemtCode
			WHERE
				OI.numUnitHour < PackingSlipUnits
			) > 0
		BEGIN
			RAISERROR('ORDERED_QUANTITY_CAN_NOT_BE_LESS_THEN_PACKING_SLIP_QTY',16,1)
			RETURN
		END
	END

	SET @TotalAmount=0                                                           
	SELECT @TotalAmount = SUM(monTotAmount) FROM OpportunityItems WHERE numOppId=@numOppID                                                                          
  
	UPDATE OpportunityMaster SET monPamount=@TotalAmount WHERE numOppId=@numOppID

/*Added by chintan - While creating SO from PO it wasn't puting Item Qty to Allocation. this segment was being called from USP_OppSaveMilestone which No longer in use after changes anoop did. */     
--Updating the warehouse items              
            
			
			DECLARE @bitViolatePrice BIT
			DECLARE @intPendingApprovePending INT

			SET @bitIsInitalSalesOrder=(SELECT bitIsInitalSalesOrder FROM OpportunityMaster WHERE numOppId=@numOppID AND numDomainId=@numDomainID)
			--SET @bitViolatePrice=(SELECT bitMarginPriceViolated FROM Domain WHERE numDomainId=@numDomainID)
			IF EXISTS (SELECT numApprovalRuleID FROM ApprovalProcessItemsClassification WHERE numDomainId=@numDomainID AND ISNULL(bitMarginPriceViolated,0) = 1)
			BEGIN
				SET @bitViolatePrice = 1
			END
			
			IF(@bitViolatePrice=1 AND @bitIsInitalSalesOrder=1 AND @tintClickBtn = 1)
			BEGIN
				UPDATE OpportunityMaster SET tintOppStatus=@DealStatus WHERE numOppId=@numOppID
			END
			ELSE
			BEGIN
				UPDATE OpportunityMaster SET tintOppStatus = @DealStatus WHERE numOppId=@numOppID
				DECLARE @tintShipped AS TINYINT               
				SELECT @tintOppStatus=tintOppStatus,@tintOppType=tintOppType,@tintShipped=tintShipped from OpportunityMaster where numOppID=@numOppID               
				
				IF @tintOppStatus=1 AND @tintCommitAllocation=1             
				BEGIN         
					EXEC USP_UpdatingInventoryonCloseDeal @numOppID,0,@numUserCntID
				END  
				declare @tintCRMType as numeric(9)        
				declare @AccountClosingDate as datetime        

				select @numDivisionID=numDivisionID,@AccountClosingDate=bintAccountClosingDate from OpportunityMaster where numOppID=@numOppID         
				select @tintCRMType=tintCRMType from divisionmaster where numDivisionID =@numDivisionID 

				--When deal is won                                         
				IF @DealStatus = 1 
				BEGIN
					IF @AccountClosingDate IS NULL                   
					BEGIN
						UPDATE OpportunityMaster SET bintAccountClosingDate=GETUTCDATE() WHERE numOppID=@numOppID              
					END

					-- Promote Lead to Account when Sales/Purchase Order is created against it
					IF @tintCRMType=0 --Lead & Order
					BEGIN
						UPDATE 
							divisionmaster 
						SET 
							tintCRMType=2
							,bintLeadProm=getutcdate()
							,bintLeadPromBy=@numUserCntID
							,bintProsProm=getutcdate()
							,bintProsPromBy=@numUserCntID        
						WHERE 
							numDivisionID=@numDivisionID        
					END
					--Promote Prospect to Account
					ELSE IF @tintCRMType=1 
					BEGIN
						UPDATE 
							divisionmaster 
						SET 
							tintCRMType=2
							,bintProsProm=getutcdate()
							,bintProsPromBy=@numUserCntID        
						WHERE 
							numDivisionID=@numDivisionID        
					END
				END         
				--When Deal is Lost
				ELSE IF @DealStatus = 2
				BEGIN
					SELECT @AccountClosingDate=bintAccountClosingDate FROM OpportunityMaster WHERE numOppID=@numOppID         
					
					IF @AccountClosingDate IS NULL                   
					BEGIN
						UPDATE OpportunityMaster SET bintAccountClosingDate=GETUTCDATE() WHERE numOppID=@numOppID
					END
				END 
			END

			/*Added by chintan -  Biz usually automatically promotes the record from Lead/Prospect to Account When a Sales Opp/Order is made against it*/  
			-- Promote Lead to Prospect when Sales/Purchase Opp is created against it
			if @tintCRMType=0 AND @DealStatus = 0 --Lead & Open Opp
			begin        
				update divisionmaster set tintCRMType=1,bintLeadProm=getutcdate(),bintLeadPromBy=@numUserCntID,bintProsProm=getutcdate(),bintProsPromBy=@numUserCntID        
				where numDivisionID=@numDivisionID        
			end        

--Add/Update Address Details
DECLARE @vcStreet  varchar(100),@vcCity  varchar(50),@vcPostalCode  varchar(15),@vcCompanyName  varchar(100),@vcAddressName VARCHAR(50), @vcAltContact VARCHAR(200)      
DECLARE @numState  numeric(9),@numCountry  numeric(9), @numCompanyId  numeric(9), @numContact NUMERIC(18,0)
DECLARE @bitIsPrimary BIT, @bitAltContact BIT;
DECLARE @tintAddressOf TINYINT

select  @vcCompanyName=vcCompanyName,@numCompanyId=div.numCompanyID from CompanyInfo Com                            
join divisionMaster Div                            
on div.numCompanyID=com.numCompanyID                            
where div.numdivisionID=@numDivisionId

IF EXISTS (SELECT numOppId FROM OpportunityMaster WHERE numOppId=@numOppID AND ISNULL(tintBillToType,0) = 0 AND ISNULL(numBillToAddressID,0) = 0)
BEGIN
	SELECT 
		@vcStreet=ISNULL(vcStreet,'')
		,@vcCity=ISNULL(vcCity,'')
		,@vcPostalCode=ISNULL(vcPostalCode,'')
		,@numState=ISNULL(numState,0)
		,@numCountry=ISNULL(numCountry,0)
		,@bitIsPrimary=bitIsPrimary
		,@vcAddressName=vcAddressName
		,@tintAddressOf=ISNULL(tintAddressOf,0)
		,@numContact=numContact
		,@bitAltContact=bitAltContact
		,@vcAltContact=vcAltContact
    FROM 
		dbo.AddressDetails AD
	WHERE 
		AD.numDomainID=@numDomainID 
		AND AD.numRecordID=@numDivisionID 
		AND AD.tintAddressOf = 2 
		AND AD.tintAddressType = 1 
		AND AD.bitIsPrimary=1       

	UPDATE OpportunityMaster SET tintBillToType=2,numBillToAddressID=0 WHERE numOppId=@numOppID

  	EXEC dbo.USP_UpdateOppAddress
	@numOppID = @numOppID, --  numeric(9, 0)
	@byteMode = 0, --  tinyint
	@vcStreet = @vcStreet, --  varchar(100)
	@vcCity = @vcCity, --  varchar(50)
	@vcPostalCode = @vcPostalCode, --  varchar(15)
	@numState = @numState, --  numeric(9, 0)
	@numCountry = @numCountry, --  numeric(9, 0)
	@vcCompanyName = @vcCompanyName, --  varchar(100)
	@numCompanyId = 0, --  numeric(9, 0)
	@vcAddressName = @vcAddressName,
	@bitCalledFromProcedure = 1,
	@numContact = @numContact,
	@bitAltContact = @bitAltContact,
	@vcAltContact = @vcAltContact
END

IF EXISTS (SELECT numOppId FROM OpportunityMaster WHERE numOppId=@numOppID AND ISNULL(tintShipToType,0) = 0 AND ISNULL(numShipToAddressID,0) = 0)
BEGIN
	SELECT 
		@vcStreet=ISNULL(vcStreet,'')
		,@vcCity=ISNULL(vcCity,'')
		,@vcPostalCode=ISNULL(vcPostalCode,'')
		,@numState=ISNULL(numState,0)
		,@numCountry=ISNULL(numCountry,0)
		,@bitIsPrimary=bitIsPrimary
		,@vcAddressName=vcAddressName
		,@tintAddressOf=ISNULL(tintAddressOf,0)
		,@numContact=numContact
		,@bitAltContact=bitAltContact
		,@vcAltContact=vcAltContact
    FROM 
		dbo.AddressDetails AD
	WHERE 
		AD.numDomainID=@numDomainID 
		AND AD.numRecordID=@numDivisionID 
		AND AD.tintAddressOf = 2 
		AND AD.tintAddressType = 2 
		AND AD.bitIsPrimary=1       

	UPDATE OpportunityMaster SET tintShipToType=2,numShipToAddressID=0 WHERE numOppId=@numOppID

  	EXEC dbo.USP_UpdateOppAddress
	@numOppID = @numOppID, --  numeric(9, 0)
	@byteMode = 1, --  tinyint
	@vcStreet = @vcStreet, --  varchar(100)
	@vcCity = @vcCity, --  varchar(50)
	@vcPostalCode = @vcPostalCode, --  varchar(15)
	@numState = @numState, --  numeric(9, 0)
	@numCountry = @numCountry, --  numeric(9, 0)
	@vcCompanyName = @vcCompanyName, --  varchar(100)
	@numCompanyId =0, --  numeric(9, 0)
	@vcAddressName = @vcAddressName,
	@bitCalledFromProcedure = 1,
	@numContact = @numContact,
	@bitAltContact = @bitAltContact,
	@vcAltContact = @vcAltContact
END

--Bill Address
IF @numBillAddressId>0
BEGIN
	SELECT 
		@vcStreet=ISNULL(vcStreet,'')
		,@vcCity=ISNULL(vcCity,'')
		,@vcPostalCode=ISNULL(vcPostalCode,'')
		,@numState=ISNULL(numState,0)
		,@numCountry=ISNULL(numCountry,0)
		,@bitIsPrimary=bitIsPrimary
		,@vcAddressName=vcAddressName
		,@tintAddressOf=ISNULL(tintAddressOf,0)
		,@numContact=numContact
		,@bitAltContact=bitAltContact
		,@vcAltContact=vcAltContact
    FROM 
		dbo.AddressDetails 
	WHERE 
		numDomainID = @numDomainID 
		AND numAddressID = @numBillAddressId

	IF @tintAddressOf = 2 AND @tintOppType = 2
	BEGIN
		SELECT
			@vcCompanyName = ISNULL(vcCompanyName,'')
			,@numCompanyId = ISNULL(CompanyInfo.numCompanyId,0)
		FROM
			AddressDetails
		INNER JOIN
			DivisionMaster 
		ON
			AddressDetails.numRecordID = DivisionMaster.numDivisionID
		INNER JOIN
			CompanyInfo
		ON
			DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
		WHERE 
			AddressDetails.numDomainID = @numDomainID 
			AND numAddressID = @numBillAddressId
	END

	UPDATE OpportunityMaster SET tintBillToType=2,numBillToAddressID=0 WHERE numOppId=@numOppID

  	EXEC dbo.USP_UpdateOppAddress
	@numOppID = @numOppID, --  numeric(9, 0)
	@byteMode = 0, --  tinyint
	@vcStreet = @vcStreet, --  varchar(100)
	@vcCity = @vcCity, --  varchar(50)
	@vcPostalCode = @vcPostalCode, --  varchar(15)
	@numState = @numState, --  numeric(9, 0)
	@numCountry = @numCountry, --  numeric(9, 0)
	@vcCompanyName = @vcCompanyName, --  varchar(100)
	@numCompanyId = 0, --  numeric(9, 0)
	@vcAddressName = @vcAddressName,
	@bitCalledFromProcedure = 1,
	@numContact = @numContact,
	@bitAltContact = @bitAltContact,
	@vcAltContact = @vcAltContact
END
  
--Ship Address
IF @intUsedShippingCompany = 92 -- WILL CALL (PICK UP FROM Warehouse)
BEGIN
	IF EXISTS (SELECT numWareHouseID FROM Warehouses WHERE Warehouses.numDomainID = @numDomainID AND numWareHouseID = @numWillCallWarehouseID AND ISNULL(numAddressID,0) > 0)
	BEGIN
		SELECT  
			@vcStreet=ISNULL(vcStreet,'')
			,@vcCity=ISNULL(vcCity,'')
			,@vcPostalCode=ISNULL(AddressDetails.vcPostalCode,'')
			,@numState=ISNULL(numState,0)
			,@numCountry=ISNULL(numCountry,0)
			,@vcAddressName=vcAddressName
		FROM 
			dbo.Warehouses 
		INNER JOIN
			AddressDetails
		ON
			Warehouses.numAddressID = AddressDetails.numAddressID
		WHERE 
			Warehouses.numDomainID = @numDomainID 
			AND numWareHouseID = @numWillCallWarehouseID
	END
	ELSE
	BEGIN
	SELECT  
		@vcStreet=ISNULL(vcWStreet,'')
		,@vcCity=ISNULL(vcWCity,'')
		,@vcPostalCode=ISNULL(vcWPinCode,'')
		,@numState=ISNULL(numWState,0)
		,@numCountry=ISNULL(numWCountry,0)
		,@vcAddressName=''
	FROM 
		dbo.Warehouses 
	WHERE 
		numDomainID = @numDomainID 
		AND numWareHouseID = @numWillCallWarehouseID
	END

	UPDATE OpportunityMaster SET tintShipToType=2 WHERE numOppId=@numOppID
 
  	EXEC dbo.USP_UpdateOppAddress
	@numOppID = @numOppID, --  numeric(9, 0)
	@byteMode = 1, --  tinyint
	@vcStreet = @vcStreet, --  varchar(100)
	@vcCity = @vcCity, --  varchar(50)
	@vcPostalCode = @vcPostalCode, --  varchar(15)
	@numState = @numState, --  numeric(9, 0)
	@numCountry = @numCountry, --  numeric(9, 0)
	@vcCompanyName = '', --  varchar(100)
	@numCompanyId =0, --  numeric(9, 0)
	@vcAddressName = @vcAddressName,
	@bitCalledFromProcedure = 1,
	@numContact = 0,
	@bitAltContact = 0,
	@vcAltContact = ''
END
ELSE IF @numShipAddressId>0
BEGIN
	SELECT
		@vcStreet=ISNULL(vcStreet,'')
		,@vcCity=ISNULL(vcCity,'')
		,@vcPostalCode=ISNULL(vcPostalCode,'')
		,@numState=ISNULL(numState,0)
		,@numCountry=ISNULL(numCountry,0)
		,@bitIsPrimary=bitIsPrimary
		,@vcAddressName=vcAddressName
		,@tintAddressOf=ISNULL(tintAddressOf,0)
		,@numContact=numContact
		,@bitAltContact=bitAltContact
		,@vcAltContact=vcAltContact
    FROM  
		dbo.AddressDetails 
	WHERE 
		numDomainID = @numDomainID 
		AND numAddressID = @numShipAddressId
 

	IF @tintAddressOf = 2 AND @tintOppType = 2
	BEGIN
		SELECT
			@vcCompanyName = ISNULL(vcCompanyName,'')
			,@numCompanyId = ISNULL(CompanyInfo.numCompanyId,0)
		FROM
			AddressDetails
		INNER JOIN
			DivisionMaster 
		ON
			AddressDetails.numRecordID = DivisionMaster.numDivisionID
		INNER JOIN
			CompanyInfo
		ON
			DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
		WHERE 
			AddressDetails.numDomainID = @numDomainID 
			AND numAddressID = @numShipAddressId
	END

	UPDATE OpportunityMaster SET tintShipToType=2,numShipToAddressID=0 WHERE numOppId=@numOppID

  	EXEC dbo.USP_UpdateOppAddress
	@numOppID = @numOppID, --  numeric(9, 0)
	@byteMode = 1, --  tinyint
	@vcStreet = @vcStreet, --  varchar(100)
	@vcCity = @vcCity, --  varchar(50)
	@vcPostalCode = @vcPostalCode, --  varchar(15)
	@numState = @numState, --  numeric(9, 0)
	@numCountry = @numCountry, --  numeric(9, 0)
	@vcCompanyName = @vcCompanyName, --  varchar(100)
	@numCompanyId =@numCompanyId, --  numeric(9, 0)
	@vcAddressName = @vcAddressName,
	@bitCalledFromProcedure = 1,
	@numContact = @numContact,
	@bitAltContact = @bitAltContact,
	@vcAltContact = @vcAltContact
END

-- IMPORTANT:KEEP THIS BELOW ADDRESS UPDATE LOGIC
-- GET TAX APPLICABLE TO DIVISION WHEN ORDER IS CREATED AFTER THAT NOT NEED TO CHANGE IT
IF @bitNewOrder = 1
BEGIN
	--INSERT TAX FOR DIVISION   
	IF @tintOppType=1 OR (@tintOppType=2 and (select isnull(bitPurchaseTaxCredit,0) from Domain where numDomainId=@numDomainId)=1)
	BEGIN
		INSERT INTO dbo.OpportunityMasterTaxItems 
		(
			numOppId,
			numTaxItemID,
			fltPercentage,
			tintTaxType,
			numTaxID
		) 
		SELECT 
			@numOppID,
			TI.numTaxItemID,
			TEMPTax.decTaxValue,
			TEMPTax.tintTaxType,
			0
		FROM 
			TaxItems TI 
		JOIN 
			DivisionTaxTypes DTT 
		ON 
			TI.numTaxItemID = DTT.numTaxItemID
		CROSS APPLY
		(
			SELECT
				decTaxValue,
				tintTaxType
			FROM
				dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,TI.numTaxItemID,@numOppId,0,NULL)
		) AS TEMPTax
		WHERE 
			DTT.numDivisionID=@numDivisionId 
			AND DTT.bitApplicable=1
		UNION 
		SELECT 
			@numOppID,
			0,
			TEMPTax.decTaxValue,
			TEMPTax.tintTaxType,
			0
		FROM 
			dbo.DivisionMaster 
		CROSS APPLY
		(
			SELECT
				decTaxValue,
				tintTaxType
			FROM
				dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,0,@numOppId,1,NULL)
		) AS TEMPTax
		WHERE 
			bitNoTax=0 
			AND numDivisionID=@numDivisionID	
		UNION 
		SELECT
			@numOppId
			,1
			,decTaxValue
			,tintTaxType
			,numTaxID
		FROM
			dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,1,@numOppId,1,NULL)		
	END	
END

--INSERT TAX FOR DIVISION   
IF @tintOppType=1 OR (@tintOppType=2 and (select isnull(bitPurchaseTaxCredit,0) from Domain where numDomainId=@numDomainId)=1)
BEGIN
--Delete Tax for Opportunity Items if item deleted 
DELETE FROM OpportunityItemsTaxItems WHERE numOppId=@numOppId AND numOppItemID NOT IN (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId=@numOppID)

--Insert Tax for Opportunity Items
INSERT INTO dbo.OpportunityItemsTaxItems 
(
	numOppId,
	numOppItemID,
	numTaxItemID,
	numTaxID
) 
SELECT 
	@numOppId,
	OI.numoppitemtCode,
	TI.numTaxItemID,
	0
FROM 
	dbo.OpportunityItems OI 
JOIN 
	dbo.ItemTax IT 
ON 
	OI.numItemCode=IT.numItemCode 
JOIN
	TaxItems TI 
ON 
	TI.numTaxItemID = IT.numTaxItemID 
WHERE 
	OI.numOppId=@numOppID 
	AND IT.bitApplicable=1 
	AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)
UNION
SELECT 
	@numOppId,
	OI.numoppitemtCode,
	0,
	0
FROM 
	dbo.OpportunityItems OI 
JOIN 
	dbo.Item I 
ON 
	OI.numItemCode=I.numItemCode
WHERE 
	OI.numOppId=@numOppID 
	AND I.bitTaxable=1 
	AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)
UNION
SELECT
	@numOppId,
	OI.numoppitemtCode,
	1,
	TD.numTaxID
FROM
	dbo.OpportunityItems OI 
INNER JOIN
	ItemTax IT
ON
	IT.numItemCode = OI.numItemCode
INNER JOIN
	TaxDetails TD
ON
	TD.numTaxID = IT.numTaxID
	AND TD.numDomainId = @numDomainId
WHERE
	OI.numOppId = @numOppID
	AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)

END

	UPDATE 
		OI
	SET
		OI.vcInclusionDetail = [dbo].[GetOrderAssemblyKitInclusionDetails](OI.numOppID,OI.numoppitemtCode,OI.numUnitHour,1,1)
	FROM 
		OpportunityItems OI
	INNER JOIN
		Item I
	ON
		OI.numItemCode = I.numItemCode
	WHERE
		numOppId=@numOppID
		AND ISNULL(I.bitKitParent,0) = 1
  
	UPDATE OpportunityMaster SET monDealAmount=dbo.GetDealAmount(@numOppID,getdate(),0) WHERE numOppId=@numOppID

	UPDATE OpportunityItems SET numProjectID=@numProjectID WHERE numOppId=@numOppId

	IF @tintOppType=1 AND @tintOppStatus=1 
	BEGIN
		EXEC USP_SalesFulfillmentQueue_Save @numDomainID,@numUserCntID,@numOppID,@numStatus
		EXEC USP_OpportunityMaster_CreateBackOrderPO @numDomainID,@numUserCntID,@numOppID
	END

	IF EXISTS (SELECT 
					numoppitemtCode 
				FROM 
					OpportunityItems 
				INNER JOIN 
					Item 
				ON 
					OpportunityItems.numItemCode=Item.numItemCode 
				WHERE 
					numOppId=@numOppID 
					AND charItemType='P' 
					AND ISNULL(numWarehouseItmsID,0) = 0 
					AND ISNULL(bitDropShip,0) = 0)
	BEGIN
		RAISERROR('WAREHOUSE_REQUIRED',16,1)
	END

	IF (SELECT COUNT(*) FROM WorkOrder WHERE numDomainID=@numDomainID AND numOppId=@numOppId AND ISNULL(numOppItemID,0) > 0 AND numOppItemID NOT IN (SELECT OIInner.numoppitemtCode FROM OpportunityItems OIInner WHERE OIInner.numOppId=@numOppID)) > 0
	BEGIN
		RAISERROR ( 'WORK_ORDER_EXISTS', 16, 1 ) ;
	END

	IF (SELECT 
			COUNT(*) 
		FROM 
			WorkOrder 
		INNER JOIN 
			OpportunityItems 
		ON 
			OpportunityItems.numOppId=@numOppID
			AND OpportunityItems.numoppitemtCode=WorkOrder.numOppItemID
		WHERE 
			WorkOrder.numDomainID=@numDomainID 
			AND WorkOrder.numOppId=@numOppID
			AND WorkOrder.numWareHouseItemId <> OpportunityItems.numWarehouseItmsID) > 0
	BEGIN
		RAISERROR ('CAN_NOT_CHANGE_ASSEMBLY_WAREHOUSE_WORK_ORDER_EXISTS', 16, 1 ) ;
	END

	IF @tintOppType = 1 AND @tintOppStatus=1
	BEGIN
		EXEC USP_OpportunityItems_CheckWarehouseMapping @numDomainID,@numOppID
	END
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SET @numOppID = 0

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
GO
/****** Object:  StoredProcedure [dbo].[USP_OppManage_API]    Script Date: 06/01/2009 23:49:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OppManage_API')
DROP PROCEDURE USP_OppManage_API
GO
CREATE PROCEDURE [dbo].[USP_OppManage_API]
(                                                                          
  @numOppID as numeric=0 OUTPUT,                                                                          
  @numContactId numeric(9)=null,                                                                          
  @numDivisionId numeric(9)=null,                                                                          
  @tintSource numeric(9)=null,                                                                          
  @vcPOppName Varchar(100)='' OUTPUT,                                                                          
  @Comments varchar(1000)='',                                                                          
  @bitPublicFlag bit=0,                                                                          
  @numUserCntID numeric(9)=0,                                                                                 
  @monPAmount DECIMAL(20,5) =0,                                                 
  @numAssignedTo as numeric(9)=0,                                                                                                        
  @numDomainId numeric(9)=0,                                                                                                                                                                                   
  @strItems as text=null,                                                                          
  @strMilestone as text=null,                                                                          
  @dtEstimatedCloseDate datetime,                                                                          
  @CampaignID as numeric(9)=null,                                                                          
  @lngPConclAnalysis as numeric(9)=null,                                                                         
  @tintOppType as tinyint,                                                                                                                                             
  @tintActive as tinyint,                                                              
  @numSalesOrPurType as numeric(9),              
  @numRecurringId as numeric(9),
  @numCurrencyID as numeric(9)=0,
  @vcSource VARCHAR(50)=NULL                                                                   
)                                                                          
                                                                          
as  
 Declare @fltExchangeRate float                                 
 DECLARE @hDocItem int                                                                                                                            
 declare @TotalAmount as DECIMAL(20,5)  
 DECLARE @WebApiId AS int                                                                        
 if @numOppID = 0                                                                          
 begin                                                                          
  declare @intOppTcode as numeric(9)                           
  Select @intOppTcode=max(numOppId)from OpportunityMaster                
  set @intOppTcode =isnull(@intOppTcode,0) + 1                                                
  set @vcPOppName=@vcPOppName+ '-'+convert(varchar(4),year(getutcdate()))  +'-A'+ convert(varchar(10),@intOppTcode)                                  
  
  if @numCurrencyID=0
  BEGIN
  	 set   @fltExchangeRate=1
  	 SELECT @numCurrencyID = CASE [bitMultiCurrency] WHEN 1 THEN numCurrencyID ELSE 0 END  FROM [Domain] WHERE [numDomainId] = @numDomainID 
  END
  else set @fltExchangeRate=dbo.GetExchangeRate(@numDomainID,@numCurrencyID)

  insert into OpportunityMaster                                                                 
  (                                                                             
  numContactId,                                                                   
  numDivisionId,                                                                          
  txtComments,                                             
  numCampainID,                                                                          
  bitPublicFlag,                                                                          
  tintSource,                                                             
  vcPOppName,                                                                          
  intPEstimatedCloseDate,                                                                          
  monPAmount,                                                                           
  numCreatedBy,                                                                          
  bintCreatedDate,                                                                           
  numModifiedBy,                                                                          
  bintModifiedDate,                                                                          
  numDomainId,                                                                                                             
  numRecOwner,                                                                          
  lngPConclAnalysis,                                                                         
  tintOppType,                                                              
  numSalesOrPurType,              
  numCurrencyID,
  fltExchangeRate                                                                   
  )                                                                          
  Values                                                                          
  (                                                                          
  @numContactId,                                                                          
  @numDivisionId,                                                                          
  ISNULL(@Comments,''),
  @CampaignID,                                                                          
  @bitPublicFlag,                     
  @tintSource,                         
  @vcPOppName,                                                                          
  @dtEstimatedCloseDate,                                                                          
  @monPAmount,                                                                                
  @numUserCntID,                                                                          
  getutcdate(),                                                                          
  @numUserCntID,                                                           
  getutcdate(),                                                                          
  @numDomainId,                                                                                   
  @numUserCntID,                                                                          
  @lngPConclAnalysis,                                                                                                                                                                      
  @tintOppType,                                                                                                                                      
  @numSalesOrPurType,              
  @numCurrencyID, 
  @fltExchangeRate                                                                 
  )                                                                                                                      
  set @numOppID=scope_identity()  
  ---- insert recurring id 
  if (@numRecurringId >0)
  BEGIN
  	EXEC [dbo].[USP_RecurringAddOpportunity] @numOppID, @numRecurringId , null, 0, null,null
  END                                              
  ---- inserting Items                                                                           
                                                         
  if convert(varchar(10),@strItems) <>''                                                                          
  begin                      
   EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems                       
      insert into                       
   OpportunityItems                                                                          
   (numOppId,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,bitDropShip,[vcItemName],[vcModelID],[vcManufacturer],vcPathForTImage,monVendorCost)                      
   select 
   @numOppID,
   X.numItemCode AS numItemCode,
   x.numUnitHour,x.monPrice,x.monTotAmount,X.vcItemDesc,
   CASE X.numWarehouseItmsID WHEN -1 THEN (SELECT [numWareHouseItemID] FROM [WareHouseItems] WHERE [numItemID] = X.numItemCode AND numWareHouseID IN (SELECT numDefaultWareHouseID FROM [eCommerceDTL] WHERE numDomainID =@numDomainId))  WHEN 0 THEN (SELECT TOP 1 [numWareHouseItemID] FROM [WareHouseItems] WHERE [numItemID] = X.numItemCode) ELSE  X.numWarehouseItmsID END AS numWarehouseItmsID,
   X.ItemType,X.DropShip,(SELECT vcItemName FROM item WHERE numItemCode = X.numItemCode),(SELECT vcModelID FROM item WHERE numItemCode = X.numItemCode),(SELECT vcManufacturer FROM item WHERE numItemCode = X.numItemCode),(SELECT TOP 1 vcPathForTImage FROM dbo.ItemImages II WHERE II.numItemCode = X.numItemCode AND II.numDomainId= @numDomainID AND II.bitDefault=1),
   (select isnull(VN.monCost,0) from Vendor VN INNER JOIN Item IT ON IT.numItemCode=VN.numItemCode AND VN.numVendorID=IT.numVendorID WHERE VN.numItemCode=X.numItemCode )
    from(                                                                          
   SELECT *FROM OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=1]',2)                                                                          
   WITH                       
   (                                                                          
   numoppitemtCode numeric(9),                                     
   numItemCode numeric(9),                                                                          
   numUnitHour FLOAT,                                                                          
   monPrice DECIMAL(20,5),                                                                       
   monTotAmount DECIMAL(20,5),                                                                          
   vcItemDesc varchar(1000),                                    
   numWarehouseItmsID numeric(9),                          
   ItemType varchar(30),      
   DropShip BIT,
   WebApiId int
   ))X    
    
 SELECT TOP 1 @WebApiId=WebApiId FROM OPENXML (@hDocItem,'/NewDataSet/Item',2)                                                        
 WITH  
 (                                                        
  WebApiId int                                
 )
		IF(@WebApiId =1)  
		BEGIN
			UPDATE [WebAPIDetail] SET [vcNinthFldValue] = GETUTCDATE() WHERE [numDomainId]=@numDomainID AND [WebApiId]=@WebApiId
		END
		IF(@vcSource IS NOT NULL)
		BEGIN
			insert into OpportunityLinking(	[numParentOppID],[numChildOppID],[vcSource],[numParentOppBizDocID]) values(null,@numOppID,@vcSource,null);
		END
	
    
   Update OpportunityItems                       
   set numItemCode=X.numItemCode,    
   numOppId=@numOppID,                     
   numUnitHour=x.numUnitHour,                      
   monPrice=x.monPrice,                      
   monTotAmount=x.monTotAmount,                                  
   vcItemDesc=X.vcItemDesc,                      
   numWarehouseItmsID=X.numWarehouseItmsID,                 
   bitDropShip=X.DropShip                 
   from (SELECT numoppitemtCode as numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,DropShip  FROM OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=2]',2)                                                                
  
    
   WITH  (                      
    numoppitemtCode numeric(9),                                     
    numItemCode numeric(9),                                                                          
    numUnitHour FLOAT,                                                                          
    monPrice DECIMAL(20,5),                                         
    monTotAmount DECIMAL(20,5),                                                                          
    vcItemDesc varchar(1000),                                    
    numWarehouseItmsID numeric(9),      
    DropShip bit                                                      
   ))X where numoppitemtCode=X.numOppItemID                                          
                                     
   insert into OppWarehouseSerializedItem                                                                          
   (numOppID,numWarehouseItmsDTLID,numOppItemID,numWarehouseItmsID)                      
   select @numOppID,X.numWarehouseItmsDTLID,X.numoppitemtCode,X.numWItmsID from(                                                                          
   SELECT * FROM OPENXML (@hDocItem,'/NewDataSet/SerialNo',2)                                                                          
   WITH                        
   (                                               
   numWarehouseItmsDTLID numeric(9),                      
   numoppitemtCode numeric(9),                      
   numWItmsID numeric(9)                                                     
   ))X                                    
               
                                 
   update OppWarehouseSerializedItem                       
   set numOppItemID=X.numoppitemtCode                                
   from                       
   (SELECT numWarehouseItmsDTLID as DTLID,O.numoppitemtCode FROM OPENXML (@hDocItem,'/NewDataSet/SerialNo',2)                                                                         
   WITH(numWarehouseItmsDTLID numeric(9),numoppitemtCode numeric(9),numWItmsID numeric(9))                                
   join OpportunityItems O on O.numWarehouseItmsID=numWItmsID and O.numOppID=@numOppID)X                 
   where numOppID=@numOppID and numWarehouseItmsDTLID=X.DTLID      
    
--   insert into OpportunityKitItems(numOppKitItem,numChildItem,intQuantity,numWareHouseItemId)    
--   SELECT * FROM OPENXML (@hDocItem,'/NewDataSet/ChildItems',2)                                                                          
--   WITH                        
--   (                                               
--   numoppitemtCode numeric(9),                      
--   numItemCode numeric(9),    
--   numQtyItemsReq integer,  
--   numWarehouseItmsID numeric(9)                                                       
--   )    
      
    
    
    
    
                              
                          
   EXEC sp_xml_removedocument @hDocItem                                     
        end          
   set @TotalAmount=0                                                           
   select @TotalAmount=sum(monTotAmount)from OpportunityItems where numOppId=@numOppID                                                                          
  update OpportunityMaster set  monPamount=@TotalAmount                                                          
  where numOppId=@numOppID                                                 
                       
                
                                                                   
  insert into OpportunityStageDetails                                                  
  (                                                                    
  numOppId,                                                                    
  numstagepercentage,                                                                    
  vcStageDetail,                                                                    
  vcComments,                            
  numDomainId,                                                                    
  numCreatedBy,                                                                    
  bintCreatedDate,                                                                    
  numModifiedBy,                                                                    
  bintModifiedDate,                                                                    
  numAssignTo,                                                                    
  bintStageComDate,                                                                    
  bintDueDate,                                                                    
  bitAlert,                                                                    
  bitStageCompleted                                                                    
  )                                                                          
  values                      
  (                      
  @numOppID,                                                                    
  100,                      
  'Deal Closed',                      
  '',                                                                    
  @numDomainId,                                                                    
  @numUserCntID,                                                                    
  getutcdate(),                                   
  @numUserCntID,                                                                    
  getutcdate(),                                                                    
  0,                                                                    
  0,                                                                    
  0,                                                                    
  0,                                                                    
  0                                                                    
  )                                                                     
  insert into OpportunityStageDetails                                  
  (                                                             
  numOppId,                                                                    
  numstagepercentage,                                                                    
  vcStageDetail,                                                                    
  vcComments,                                                                
  numDomainId,                                                                    
  numCreatedBy,                                                                    
  bintCreatedDate,                                                                    
  numModifiedBy,                                                            
  bintModifiedDate,                                                                    
  numAssignTo,                                                                  
  bintStageComDate,                                                                    
  bintDueDate,                                                                    
  bitAlert,                                                                    
  bitStageCompleted                                                                    
  )                                 
  values                      
  (                  
  @numOppID,                                                                    
  0,                                                                    
  'Deal Lost',                                 
  '',                                                                    
 @numDomainId,                                                                    
  @numUserCntID,                                                                    
  getutcdate(),                                                                    
  @numUserCntID,                                                                    
  getutcdate(),                        
   0,                                                                    
   0,                                                                
  0,                                                                    
  0,                                                                    
  0                                                                    
  )                                                                                                     
 end                       
 else                                                                                                                          
 begin                  
  --Reverting back the warehouse items                  
  declare @tintOppStatus as tinyint                   
  select @tintOppStatus=tintOppStatus from OpportunityMaster where numOppID=@numOppID                  
  if @tintOppStatus=1                   
        begin                  
   exec USP_RevertDetailsOpp @numOppID,0,0,@numUserCntID        
  end                  
                     
  update OpportunityMaster                      
  set                       
  vcPOppName=@vcPOppName,                                              
  txtComments=ISNULL(@Comments,''),                                                           
  bitPublicFlag=@bitPublicFlag,                                                                          
  numCampainID=@CampaignID,                                                                          
  tintSource=@tintSource,                                                                          
  intPEstimatedCloseDate=@dtEstimatedCloseDate,                                                                           
  numModifiedBy=@numUserCntID,                                                          
   bintModifiedDate=getutcdate(),                                                                                                                            
  lngPConclAnalysis=@lngPConclAnalysis,          
  monPAmount=@monPAmount,                                                                                                                                                                                           
  tintActive=@tintActive,                                                              
  numSalesOrPurType=@numSalesOrPurType              
  where numOppId=@numOppID               
        ---Updating if organization is assigned to someone                                                      
  declare @tempAssignedTo as numeric(9)                                                    
  set @tempAssignedTo=null                                      
  select @tempAssignedTo=isnull(numAssignedTo,0) from OpportunityMaster where numOppId=@numOppID                                                                          
  if (@tempAssignedTo<>@numAssignedTo and  @numAssignedTo<>'0')                                                    
  begin                                   
   update OpportunityMaster set numAssignedTo=@numAssignedTo ,numAssignedBy=@numUserCntID where numOppId=@numOppID                                                    
  end                                                     
  else if  (@numAssignedTo =0)                                                    
  begin                
   update OpportunityMaster set numAssignedTo=0 ,numAssignedBy=0 where numOppId = @numOppID                                                    
  end       
  ---- update recurring id 
  if (@numRecurringId >0)
  BEGIN
  	EXEC [dbo].[USP_RecurringAddOpportunity] @numOppID, @numRecurringId , null, 0, null,null
  END                                                               
  ---- Updating Items                                                                     
  if convert(varchar(10),@strItems) <>''                                                                          
  begin                                                                          
   EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems                                      
   Delete From OppWarehouseSerializedItem WHERE numOppID=@numOppID                                           
 delete from OpportunityItems where numOppID=@numOppID and numoppitemtCode not in                       
   (SELECT numoppitemtCode FROM OPENXML (@hDocItem,'/NewDataSet/Item',2)                                                                          
   WITH  (numoppitemtCode numeric(9)))                                   
   insert into OpportunityItems                                                                          
   (numOppId,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,bitDropShip)                                                                          
   select 
	   @numOppID,
	   X.numItemCode AS numItemCode,
	   x.numUnitHour,x.monPrice,x.monTotAmount,X.vcItemDesc,
	   CASE X.numWarehouseItmsID WHEN 0 THEN (SELECT TOP 1 [numWareHouseItemID] FROM [WareHouseItems] WHERE [numItemID] = X.numItemCode) ELSE  X.numWarehouseItmsID END AS numWarehouseItmsID,
	   X.ItemType,X.DropShip from(                                                                          
   SELECT *FROM OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=1]',2)                                                                          
   WITH                       
   (                                                                          
   numoppitemtCode numeric(9),                                     
   numItemCode numeric(9),                                                                          
   numUnitHour FLOAT,                                                                          
   monPrice DECIMAL(20,5),                                                                       
   monTotAmount DECIMAL(20,5),                                                                          
   vcItemDesc varchar(1000),                                    
   numWarehouseItmsID numeric(9),                          
   ItemType varchar(30),      
   DropShip BIT,
   WebApiId int
   ))X  
--   select @numOppID,X.numItemCode,x.numUnitHour,x.monPrice,x.monTotAmount,X.vcItemDesc,X.numWarehouseItmsID,X.ItemType,X.DropShip from(                                                                          
--   SELECT *FROM OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=1]',2)                                                                          
--   WITH  (                      
--    numoppitemtCode numeric(9),                                     
--    numItemCode numeric(9),                                                                          
--    numUnitHour numeric(9),                                                                          
--    monPrice DECIMAL(20,5),                                                                       
--    monTotAmount DECIMAL(20,5),                                                                          
--    vcItemDesc varchar(1000),                                    
--    numWarehouseItmsID numeric(9),                              
--    Op_Flag tinyint,                            
--    ItemType varchar(30),      
-- DropShip bit                  
--     ))X                                     
                                  
   Update OpportunityItems                       
   set numItemCode=X.numItemCode,    
   numOppId=@numOppID,                       
   numUnitHour=x.numUnitHour,                      
   monPrice=x.monPrice,                      
   monTotAmount=x.monTotAmount,                                  
   vcItemDesc=X.vcItemDesc,                      
   numWarehouseItmsID=X.numWarehouseItmsID,                 
   bitDropShip=X.DropShip                 
   from (SELECT numoppitemtCode as numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,DropShip  FROM OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=2]',2)                                                                
  
    
      
              
                
   WITH  (                      
    numoppitemtCode numeric(9),                                     
    numItemCode numeric(9),                                                                          
    numUnitHour FLOAT,                                                                          
    monPrice DECIMAL(20,5),                                         
    monTotAmount DECIMAL(20,5),                                                                          
    vcItemDesc varchar(1000),                                    
    numWarehouseItmsID numeric(9),      
    DropShip bit                                                      
   ))X where numoppitemtCode=X.numOppItemID                                  
                                    
         
-- delete from OpportunityKitItems where numOppKitItem in (select numoppitemtCode from OpportunityItems where numOppId=@numOppID)     
-- and numOppChildItemID not in                       
-- (SELECT numOppChildItemID FROM OPENXML (@hDocItem,'/NewDataSet/ChildItems',2)                                                                          
-- WITH  (numOppChildItemID numeric(9)))    
                             
   insert into OppWarehouseSerializedItem                                                                          
   (numOppID,numWarehouseItmsDTLID,numOppItemID,numWarehouseItmsID)                                                                   
   select @numOppID,X.numWarehouseItmsDTLID,X.numoppitemtCode,X.numWItmsID from(                                                                          
   SELECT * FROM OPENXML (@hDocItem,'/NewDataSet/SerialNo',2)                                                                          
   WITH  (                                                                          
   numWarehouseItmsDTLID numeric(9),numoppitemtCode numeric(9),numWItmsID numeric(9)                                                     
   ))X                                    
                     
   update OppWarehouseSerializedItem set numOppItemID=X.numoppitemtCode                                
   from (SELECT numWarehouseItmsDTLID as DTLID,O.numoppitemtCode FROM OPENXML (@hDocItem,'/NewDataSet/SerialNo',2)                                                                         
   WITH(numWarehouseItmsDTLID numeric(9),numoppitemtCode numeric(9),numWItmsID numeric(9))                                
   join OpportunityItems O on O.numWarehouseItmsID=numWItmsID and O.numOppID=@numOppID)X                                
   where numOppID=@numOppID and numWarehouseItmsDTLID=X.DTLID       
    
--   insert into OpportunityKitItems(numOppKitItem,numChildItem,intQuantity,numWareHouseItemId)    
--   SELECT * FROM OPENXML (@hDocItem,'/NewDataSet/ChildItems[Op_Flag=1]',2)                                                                          
--   WITH                        
--   (                                               
--   numoppitemtCode numeric(9),                      
--   numItemCode numeric(9),    
--   numQtyItemsReq integer,  
--   numWarehouseItmsID numeric(9)                                                       
--   )    
    
--   update OpportunityKitItems set numOppKitItem=X.numoppitemtCode,numWareHouseItemId=X.numWarehouseItmsID,numChildItem=X.numItemCode, intQuantity=X.numQtyItemsReq                               
--   from (SELECT numoppitemtCode,numItemCode,numQtyItemsReq,numOppChildItemID as OppChildItemID,numWarehouseItmsID FROM OPENXML (@hDocItem,'/NewDataSet/ChildItems[Op_Flag=2]',2)                                                                         
--   WITH(numOppChildItemID numeric(9),numoppitemtCode numeric(9),numItemCode numeric(9),numQtyItemsReq numeric(9),numWarehouseItmsID numeric(9)))X                                
--   where numOppChildItemID=X.OppChildItemID     
       
                                                                      
   EXEC sp_xml_removedocument @hDocItem                                               
end                                                                  
                                                                                                                               
                                             
 end
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityMaster_CloneOrderLineItem')
DROP PROCEDURE dbo.USP_OpportunityMaster_CloneOrderLineItem
GO
CREATE PROCEDURE [dbo].[USP_OpportunityMaster_CloneOrderLineItem]
(
	@numDomainID NUMERIC(18,0),
	@numUserCntID NUMERIC(18,0),
    @numOppID NUMERIC(18,0),
    @numOppItemID NUMERIC(18,0)
)
AS
BEGIN
	IF EXISTS(SELECT numOppId FROM OpportunityMaster WHERE numOppId=@numOppID AND ISNULL(tintshipped,0)=1)
	BEGIN
		RAISERROR('CLOSED_ORDER',16,1)
	END
	ELSE
	BEGIN
		BEGIN TRY
		BEGIN TRANSACTION
			DECLARE @tintCommitAllocation TINYINT
			DECLARE @tintOppType TINYINT
			DECLARE @tintOppStatus TINYINT
			DECLARE @numDivisionId NUMERIC(18,0)
			DECLARE @bitAllocateInventoryOnPickList BIT

			SELECT @tintCommitAllocation=ISNULL(tintCommitAllocation,1) FROM Domain WHERE numDomainId=@numDomainId
			SELECT @numDivisionId=numDivisionID, @tintOppType=tintOppType,@tintOppStatus=ISNULL(tintOppStatus,0) FROM OpportunityMaster WHERE numOppId=@numOppID

			SELECT 
				@bitAllocateInventoryOnPickList=ISNULL(bitAllocateInventoryOnPickList,0)
			FROM 
				DivisionMaster D
			JOIN 
				CompanyInfo C 
			ON 
				C.numCompanyId=D.numCompanyID
			WHERE 
				D.numDivisionID = @numDivisionId

			--REVERTING BACK THE WAREHOUSE ITEMS                  
			IF @tintOppStatus = 1 AND @tintCommitAllocation=1
			BEGIN               
				EXEC USP_RevertDetailsOpp @numOppID,0,0,@numUserCntID                  
			END       

			DECLARE @vcOppItemsColumns VARCHAR(MAX) = STUFF((SELECT ',' + COLUMN_NAME
															FROM INFORMATION_SCHEMA.COLUMNS
															WHERE TABLE_NAME = N'OpportunityItems' AND COLUMN_NAME <> 'numoppitemtCode'
															FOR XML PATH('')),1,1,'')

			DECLARE @vcOppKitItemsColumns VARCHAR(MAX) = STUFF((SELECT ',' + COLUMN_NAME
															FROM INFORMATION_SCHEMA.COLUMNS
															WHERE TABLE_NAME = N'OpportunityKitItems' AND COLUMN_NAME <> 'numOppChildItemID' AND COLUMN_NAME <> 'numOppItemID'
															FOR XML PATH('')),1,1,'')

			DECLARE @vcOppKitChildItemsColumns VARCHAR(MAX) = STUFF((SELECT ',' + COLUMN_NAME
																	FROM INFORMATION_SCHEMA.COLUMNS
																	WHERE TABLE_NAME = N'OpportunityKitChildItems' AND COLUMN_NAME <> 'numOppKitChildItemID' AND COLUMN_NAME <> 'numOppItemID' AND COLUMN_NAME <> 'numOppChildItemID' 
																	FOR XML PATH('')),1,1,'')

			DECLARE @numNewOppItemID NUMERIC(18,0)
			DECLARE @numNewOppChildItemID NUMERIC(18,0)
			DECLARE @numNewOppKitChildItemID NUMERIC(18,0)
			DECLARE @sqlText AS NVARCHAR(MAX) = CONCAT('INSERT INTO OpportunityItems (',@vcOppItemsColumns,') SELECT ',@vcOppItemsColumns,' FROM OpportunityItems WHERE numOppID=',@numOppID,' AND numoppitemtCode=',@numOppItemID,'; SELECT @numNewOppItemID = SCOPE_IDENTITY()')

			EXEC sp_executesql @sqlText, N'@numNewOppItemID NUMERIC(18,0) OUTPUT', @numNewOppItemID OUTPUT

			DECLARE @TEMP TABLE
			(
				ID INT IDENTITY(1,1)
				,numOppChildItemID NUMERIC(18,0)
			)

			INSERT INTO @TEMP
			(
				numOppChildItemID
			)
			SELECT 
				numOppChildItemID 
			FROM 
				OpportunityKitItems 
			WHERE 
				numOppId=@numOppID 
				AND numOppItemID=@numOppItemID

			DECLARE @i AS INT = 1
			DECLARE @iCount AS INT
			DECLARE @numTempOppChildItemID NUMERIC(18,0)
			SELECT @iCount=COUNT(*) FROM @TEMP

			WHILE @i <= @iCount
			BEGIN
				SELECT @numTempOppChildItemID=numOppChildItemID FROM @TEMP WHERE ID=@i

				SET @sqlText = CONCAT('INSERT INTO OpportunityKitItems (',@vcOppKitItemsColumns,',numOppItemID) SELECT ',@vcOppKitItemsColumns,',',@numNewOppItemID,' FROM OpportunityKitItems WHERE numOppID=',@numOppID,' AND numOppItemID=',@numOppItemID,' AND numOppChildItemID=',@numTempOppChildItemID,'; SELECT @numNewOppChildItemID = SCOPE_IDENTITY()')
				EXEC sp_executesql @sqlText, N'@numNewOppChildItemID NUMERIC(18,0) OUTPUT',@numNewOppChildItemID OUTPUT

				SET @sqlText = CONCAT('INSERT INTO OpportunityKitChildItems (',@vcOppKitChildItemsColumns,',numOppItemID,numOppChildItemID) SELECT ',@vcOppKitChildItemsColumns,',',@numNewOppItemID,',',@numNewOppChildItemID,' FROM OpportunityKitChildItems WHERE numOppID=',@numOppID,' AND numOppItemID=',@numOppItemID,' AND numOppChildItemID=',@numTempOppChildItemID,';')
				EXEC sp_executesql @sqlText

				SET @i = @i + 1
			END

			IF NOT (@tintCommitAllocation=2 AND @bitAllocateInventoryOnPickList=1)
			BEGIN
				EXEC USP_ManageSOWorkOrder @numDomainId,@numOppID,@numUserCntID
			END

			IF @tintOppStatus=1 AND @tintCommitAllocation=1             
			BEGIN         
				EXEC USP_UpdatingInventoryonCloseDeal @numOppID,0,@numUserCntID
			END  

			IF @tintOppStatus = 1
			BEGIN
				IF (SELECT COUNT(*) FROM OpportunityItems WHERE numOppId=@numOppID AND ISNULL(numWarehouseItmsID,0) > 0 AND ISNULL(bitDropShip,0)=0) <>
					(SELECT COUNT(*) FROM OpportunityItems INNER JOIN WareHouseItems ON OpportunityItems.numWarehouseItmsID=WareHouseItems.numWareHouseItemID AND OpportunityItems.numItemCode=WareHouseItems.numItemID WHERE numOppId=@numOppID AND OpportunityItems.numWarehouseItmsID > 0 AND ISNULL(bitDropShip,0)=0)
				BEGIN
					RAISERROR('INVALID_ITEM_WAREHOUSES',16,1)
					RETURN
				END
			END

			IF(SELECT 
					COUNT(*) 
				FROM 
					OpportunityBizDocItems OBDI 
				INNER JOIN 
					OpportunityBizDocs OBD 
				ON 
					OBDI.numOppBizDocID=OBD.numOppBizDocsID 
				WHERE 
					OBD.numOppId=@numOppID AND OBDI.numOppItemID NOT IN (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId=@numOppID)) > 0
			BEGIN
				RAISERROR('ITEM_USED_IN_BIZDOC',16,1)
				RETURN
			END

			IF(SELECT 
					COUNT(*)
				FROM
					OpportunityItems OI
				INNER JOIN
				(
					SELECT
						numOppItemID
						,SUM(numUnitHour) numInvoiceBillQty
					FROM 
						OpportunityBizDocItems OBDI 
					INNER JOIN 
						OpportunityBizDocs OBD 
					ON 
						OBDI.numOppBizDocID=OBD.numOppBizDocsID 
					WHERE 
						OBD.numOppId=@numOppID
						AND ISNULL(OBD.bitAuthoritativeBizDocs,0) = 1
					GROUP BY
						numOppItemID
				) AS TEMP
				ON
					TEMP.numOppItemID = OI.numoppitemtCode
				WHERE
					OI.numOppId = @numOppID
					AND OI.numUnitHour < Temp.numInvoiceBillQty
				) > 0
			BEGIN
				RAISERROR('ORDERED_QUANTITY_CAN_NOT_BE_LESS_THEN_INVOICE/BILL_QTY',16,1)
				RETURN
			END

			IF @tintOppType=1 AND @tintCommitAllocation=2 AND @bitAllocateInventoryOnPickList=1
			BEGIN
				IF(SELECT 
						COUNT(*)
					FROM
					(
						SELECT
							numOppItemID
							,SUM(numUnitHour) PackingSlipUnits
						FROM 
							OpportunityBizDocItems OBDI 
						INNER JOIN 
							OpportunityBizDocs OBD 
						ON 
							OBDI.numOppBizDocID=OBD.numOppBizDocsID 
						WHERE 
							OBD.numOppId=@numOppID
							AND OBD.numBizDocId=29397 --Picking Slip
						GROUP BY
							numOppItemID
					) AS TEMP
					INNER JOIN
						OpportunityItems OI
					ON
						TEMP.numOppItemID = OI.numoppitemtCode
					WHERE
						OI.numUnitHour < PackingSlipUnits
					) > 0
				BEGIN
					RAISERROR('ORDERED_QUANTITY_CAN_NOT_BE_LESS_THEN_PACKING_SLIP_QTY',16,1)
					RETURN
				END
			END

			IF EXISTS (SELECT 
					numoppitemtCode 
				FROM 
					OpportunityItems 
				INNER JOIN 
					Item 
				ON 
					OpportunityItems.numItemCode=Item.numItemCode 
				WHERE 
					numOppId=@numOppID 
					AND charItemType='P' 
					AND ISNULL(numWarehouseItmsID,0) = 0 
					AND ISNULL(bitDropShip,0) = 0)
			BEGIN
				RAISERROR('WAREHOUSE_REQUIRED',16,1)
			END

			IF (SELECT COUNT(*) FROM WorkOrder WHERE numDomainID=@numDomainID AND numOppId=@numOppId AND ISNULL(numOppItemID,0) > 0 AND numOppItemID NOT IN (SELECT OIInner.numoppitemtCode FROM OpportunityItems OIInner WHERE OIInner.numOppId=@numOppID)) > 0
			BEGIN
				RAISERROR ( 'WORK_ORDER_EXISTS', 16, 1 ) ;
			END

			IF (SELECT 
					COUNT(*) 
				FROM 
					WorkOrder 
				INNER JOIN 
					OpportunityItems 
				ON 
					OpportunityItems.numOppId=@numOppID
					AND OpportunityItems.numoppitemtCode=WorkOrder.numOppItemID
				WHERE 
					WorkOrder.numDomainID=@numDomainID 
					AND WorkOrder.numOppId=@numOppID
					AND WorkOrder.numWareHouseItemId <> OpportunityItems.numWarehouseItmsID) > 0
			BEGIN
				RAISERROR ('CAN_NOT_CHANGE_ASSEMBLY_WAREHOUSE_WORK_ORDER_EXISTS', 16, 1 ) ;
			END

			--Delete Tax for Opportunity Items if item deleted 
			DELETE FROM OpportunityItemsTaxItems WHERE numOppId=@numOppId AND numOppItemID NOT IN (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId=@numOppID)

			--Insert Tax for Opportunity Items
			INSERT INTO dbo.OpportunityItemsTaxItems 
			(
				numOppId,
				numOppItemID,
				numTaxItemID,
				numTaxID
			) 
			SELECT 
				@numOppId,
				OI.numoppitemtCode,
				TI.numTaxItemID,
				0
			FROM 
				dbo.OpportunityItems OI 
			JOIN 
				dbo.ItemTax IT 
			ON 
				OI.numItemCode=IT.numItemCode 
			JOIN
				TaxItems TI 
			ON 
				TI.numTaxItemID = IT.numTaxItemID 
			WHERE 
				OI.numOppId=@numOppID 
				AND IT.bitApplicable=1 
				AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)
			UNION
			SELECT 
				@numOppId,
				OI.numoppitemtCode,
				0,
				0
			FROM 
				dbo.OpportunityItems OI 
			JOIN 
				dbo.Item I 
			ON 
				OI.numItemCode=I.numItemCode
			WHERE 
				OI.numOppId=@numOppID 
				AND I.bitTaxable=1 
				AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)
			UNION
			SELECT
				@numOppId,
				OI.numoppitemtCode,
				1,
				TD.numTaxID
			FROM
				dbo.OpportunityItems OI 
			INNER JOIN
				ItemTax IT
			ON
				IT.numItemCode = OI.numItemCode
			INNER JOIN
				TaxDetails TD
			ON
				TD.numTaxID = IT.numTaxID
				AND TD.numDomainId = @numDomainId
			WHERE
				OI.numOppId = @numOppID
				AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)

			UPDATE 
				OI
			SET
				OI.vcInclusionDetail = [dbo].[GetOrderAssemblyKitInclusionDetails](OI.numOppID,OI.numoppitemtCode,OI.numUnitHour,1,1)
			FROM 
				OpportunityItems OI
			INNER JOIN
				Item I
			ON
				OI.numItemCode = I.numItemCode
			WHERE
				numOppId=@numOppID
				AND ISNULL(I.bitKitParent,0) = 1
  
			UPDATE OpportunityMaster SET monDealAmount=dbo.GetDealAmount(@numOppID,getdate(),0),numModifiedBy=@numUserCntID,bintModifiedDate=GETUTCDATE() WHERE numOppId=@numOppID
		COMMIT
		END TRY
		BEGIN CATCH
			DECLARE @ErrorMessage NVARCHAR(4000)
			DECLARE @ErrorNumber INT
			DECLARE @ErrorSeverity INT
			DECLARE @ErrorState INT
			DECLARE @ErrorLine INT
			DECLARE @ErrorProcedure NVARCHAR(200)

			IF @@TRANCOUNT > 0
				ROLLBACK TRANSACTION;

			SELECT 
				@ErrorMessage = ERROR_MESSAGE(),
				@ErrorNumber = ERROR_NUMBER(),
				@ErrorSeverity = ERROR_SEVERITY(),
				@ErrorState = ERROR_STATE(),
				@ErrorLine = ERROR_LINE(),
				@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

			RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
		END CATCH
	END

END
GO
--modified by anoop jayaraj              
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityMaster_DemoteToOpportunity')
DROP PROCEDURE USP_OpportunityMaster_DemoteToOpportunity
GO
CREATE PROCEDURE [dbo].[USP_OpportunityMaster_DemoteToOpportunity]
	@numDomainID NUMERIC(18,0),
	@numUserCntID NUMERIC(18,0),
	@numOppId NUMERIC(18,0)                   
AS                          
BEGIN             
	IF EXISTS(SELECT * FROM OpportunityMaster WHERE numOppID=@numOppId and numDomainID=@numDomainID)         
	BEGIN 
		SET XACT_ABORT ON;

		BEGIN TRY 
		BEGIN TRAN

			IF (SELECT COUNT(*) FROM MassSalesOrderQueue WHERE numOppID = @numOppId AND ISNULL(bitExecuted,0) = 0) > 0
			BEGIN
				RAISERROR ( 'MASS SALES ORDER', 16, 1 )
				RETURN
			END
			IF (SELECT COUNT(*) FROM RecurrenceConfiguration WHERE numOppID = @numOppId AND numDomainID=@numDomainID AND ISNULL(bitMarkForDelete,0) = 0) > 0
			BEGIN
				RAISERROR ( 'RECURRING ORDER OR BIZDOC', 16, 1 )
				RETURN
			END
			ELSE IF (SELECT COUNT(*) FROM RecurrenceTransaction INNER JOIN RecurrenceConfiguration ON RecurrenceTransaction.numRecConfigID = RecurrenceConfiguration.numRecConfigID WHERE RecurrenceTransaction.numRecurrOppID = @numOppId AND ISNULL(RecurrenceConfiguration.bitDisabled,0) = 0) > 0
			BEGIN	
				RAISERROR ( 'RECURRED ORDER', 16, 1 ) ;
				RETURN ;
			END
			ELSE IF (SELECT COUNT(*) FROM OpportunityBizdocs WHERE numOppId=@numOppId AND numBizDocId=296 AND ISNULL(bitFulfilled,0) = 1) > 0
			BEGIN
				RAISERROR ( 'FULFILLED_ITEMS', 16, 1 ) ;
				RETURN ;
			END

			IF EXISTS (SELECT * FROM dbo.ReturnHeader WHERE numOppId = @numOppId AND numDomainId= @numDomainID)         
			BEGIN
				RAISERROR ( 'RETURN_EXIST', 16, 1 ) ;
				RETURN ;
			END	

     
			DECLARE @tintError TINYINT;SET @tintError=0

			EXECUTE USP_CheckCanbeDeleteOppertunity @numOppId,@tintError OUTPUT
  
			IF @tintError=1
			BEGIN
  				RAISERROR ( 'OppItems DEPENDANT', 16, 1 ) ;
				RETURN ;
			END
	
			--Credit Balance
			Declare @monCreditAmount as DECIMAL(20,5);Set @monCreditAmount=0
			Declare @monCreditBalance as DECIMAL(20,5);Set @monCreditBalance=0
			Declare @numDivisionID as numeric(9);Set @numDivisionID=0
			Declare @tintOppType as tinyint

			Select @numDivisionID=numDivisionID,@tintOppType=tintOppType from OpportunityMaster WHERE  numOppId=@numOppId  and numDomainID= @numDomainID 
   
			IF @tintOppType=1 --Sales
			BEGIN
			  Select @monCreditBalance=isnull(monSCreditBalance,0) from DivisionMaster WHERE  numDivisionID=@numDivisionID  and numDomainID= @numDomainID 
			END
			ELSE IF @tintOppType=2 --Purchase
			BEGIN
				UPDATE OpportunityMaster SET numReleaseStatus = 1 WHERE numDomainId=@numDomainID AND numOppId IN (SELECT DISTINCT numOppId FROM OpportunityItemsReleaseDates WHERE  numPurchasedOrderID = @numOppId)
				UPDATE OpportunityItemsReleaseDates SET tintStatus = 1, numPurchasedOrderID=0 WHERE numPurchasedOrderID = @numOppId
			END

			SELECT @monCreditBalance=isnull(monPCreditBalance,0) from DivisionMaster WHERE  numDivisionID=@numDivisionID  and numDomainID= @numDomainID 
   
			SELECT @monCreditAmount=sum(monAmount) from CreditBalanceHistory where numReturnID in (select numReturnID from [Returns] WHERE [numOppId] = @numOppId)
	
			IF (@monCreditAmount > @monCreditBalance)         
			BEGIN
				RAISERROR ( 'CreditBalance DEPENDANT', 16, 1 ) ;
				RETURN ;
			END	
        
			DECLARE @tintOppStatus AS TINYINT
			DECLARE @tintShipped AS TINYINT                
			SELECT @tintOppStatus=tintOppStatus,@tintShipped=tintShipped from OpportunityMaster where numOppID=@numOppId                
  
			DECLARE @isOrderItemsAvailable AS INT = 0

			SELECT 
				@isOrderItemsAvailable = COUNT(*) 
			FROM    
				OpportunityItems OI
			JOIN 
				dbo.OpportunityMaster OM ON OI.numOppId = OM.numOppId
			JOIN 
				Item I ON OI.numItemCode = I.numItemCode
			WHERE   
				(charitemtype='P' OR 1=(CASE WHEN tintOppType=1 THEN 
										CASE WHEN ISNULL(I.bitKitParent,0) = 1 AND ISNULL(I.bitAssembly,0) = 1 THEN 0
											 WHEN ISNULL(I.bitKitParent,0) = 1 THEN 1
											 ELSE 0 END 
										ELSE 0 END)) AND OI.numOppId = @numOppId
									   AND ( bitDropShip = 0
											 OR bitDropShip IS NULL
										   ) 

			IF (@tintOppStatus=1 and @tintShipped=0) OR  @tintShipped=1               
			BEGIN    
				EXEC USP_RevertDetailsOpp @numOppId,0,2,@numUserCntID                
			END                                 
                        
			DELETE FROM [OpportunitySalesTemplate] WHERE [numOppId] =@numOppId 
			DELETE OpportunityLinking WHERE numChildOppID=@numOppId OR numParentOppID=@numOppId                    
                

			IF @tintOppType = 1 AND @tintOppStatus = 1 -- SALES ORDER
			BEGIN
				-- MAKE SERIAL/LOT# NUUMBER AVAILABLE FOR USE AS SALES ORDER IS DELETED
				UPDATE WHIDL
					SET WHIDL.numQty = (CASE WHEN Item.bitLotNo = 1 THEN ISNULL(WHIDL.numQty,0) + ISNULL(OWSI.numQty,0) ELSE 1 END)
				FROM 
					WareHouseItmsDTL WHIDL
				INNER JOIN
					OppWarehouseSerializedItem OWSI
				ON
					WHIDL.numWareHouseItmsDTLID = OWSI.numWarehouseItmsDTLID
					AND WHIDL.numWareHouseItemID = OWSI.numWarehouseItmsID
				INNER JOIN
					WareHouseItems
				ON
					WHIDL.numWareHouseItemID = WareHouseItems.numWareHouseItemID
				INNER JOIN
					Item
				ON
					WareHouseItems.numItemID = Item.numItemCode
				WHERE
					OWSI.numOppID = @numOppId
			END
			ELSE IF @tintOppType = 2 AND @tintOppStatus = 1 -- PURCHASE ORDER
			BEGIN
				IF (
					SELECT 
						COUNT(*)
					FROM 
						OppWarehouseSerializedItem OWSI 
					INNER JOIN 
						OpportunityMaster OM 
					ON 
						OWSI.numOppID=OM.numOppId 
						AND tintOppType=1 
					WHERE 
						OWSI.numWarehouseItmsDTLID IN (SELECT numWarehouseItmsDTLID FROM OppWarehouseSerializedItem OWSIInner WHERE OWSIInner.numOppID = @numOppID)
					) > 0
				BEGIN
					RAISERROR ('SERIAL/LOT#_USED', 16, 1 ) ;
				END
				ELSE
				BEGIN
					-- REMOVE SERIAL/LOT# NUMBER FROM INVENTORY BECAUSE PURCHASE ORDER IS DELETED
					UPDATE WHIDL
						SET WHIDL.numQty = (CASE WHEN Item.bitLotNo = 1 THEN ISNULL(WHIDL.numQty,0) - ISNULL(OWSI.numQty,0) ELSE 0 END)
					FROM 
						WareHouseItmsDTL WHIDL
					INNER JOIN
						OppWarehouseSerializedItem OWSI
					ON
						WHIDL.numWareHouseItmsDTLID = OWSI.numWarehouseItmsDTLID
						AND WHIDL.numWareHouseItemID = OWSI.numWarehouseItmsID
					INNER JOIN
						WareHouseItems
					ON
						WHIDL.numWareHouseItemID = WareHouseItems.numWareHouseItemID
					INNER JOIN
						Item
					ON
						WareHouseItems.numItemID = Item.numItemCode
					WHERE
						OWSI.numOppID = @numOppId
				END
			END
									        
	                
		  DELETE FROM OpportunityItemLinking where numNewOppID=@numOppId                      
                         
	  


		  DELETE FROM OpportunityBizDocKitItems WHERE [numOppBizDocItemID] IN 
		  (SELECT [numOppBizDocItemID] FROM [OpportunityBizDocItems] WHERE [numOppBizDocID] IN 
			(select numOppBizDocsId from OpportunityBizDocs where numOppId=@numOppId))
	
		  delete from OpportunityBizDocItems where numOppBizDocID in                        
		  (select numOppBizDocsId from OpportunityBizDocs where numOppId=@numOppId)

		  DELETE FROM OpportunityBizDocDtl WHERE numBizDocID in                        
		  (select numOppBizDocsId from OpportunityBizDocs where numOppId=@numOppId)                        
           
		   DELETE FROM OpportunityBizDocsPaymentDetails WHERE [numBizDocsPaymentDetId] IN 
			 (SELECT [numBizDocsPaymentDetId] FROM [OpportunityBizDocsDetails] where numBizDocsId in                        
				(select numOppBizDocsId from OpportunityBizDocs where numOppId=@numOppId)) 
               
		  delete from OpportunityBizDocsDetails where numBizDocsId in                        
		  (select numOppBizDocsId from OpportunityBizDocs where numOppId=@numOppId)  

		DELETE FROM [ShippingReportItems] WHERE [numShippingReportId] IN  (SELECT numShippingReportId from [ShippingReport] WHERE [numOppBizDocId] IN (select numOppBizDocsId from OpportunityBizDocs where numOppId=@numOppId))
		DELETE FROM [ShippingBox] WHERE [numShippingReportId] IN  (SELECT numShippingReportId from [ShippingReport] WHERE [numOppBizDocId] IN (select numOppBizDocsId from OpportunityBizDocs where numOppId=@numOppId))
		DELETE FROM [ShippingReport] WHERE [numOppBizDocId] IN (select numOppBizDocsId from OpportunityBizDocs where numOppId=@numOppId)

		delete from DocumentWorkflow where numDocID in 
		(select numOppBizDocsId from OpportunityBizDocs where numOppId=@numOppId) and cDocType='B'      

		delete from BizDocAction where numBizActionId in(select numBizActionId from BizActionDetails where numOppBizDocsId in
		(select numOppBizDocsId from OpportunityBizDocs where numOppId=@numOppId) and btDocType=1)

		delete from BizActionDetails where numOppBizDocsId in
		(select numOppBizDocsId from OpportunityBizDocs where numOppId=@numOppId) and btDocType=1

		  delete from OpportunityBizDocs where numOppId=@numOppId                        
                                          
		  delete from OpportunityDependency where numOpportunityId=@numOppId                        
                          
	  
  
		  --Credit Balance
		  delete from CreditBalanceHistory where numReturnID in (select numReturnID from [Returns] WHERE [numOppId] = @numOppId)

			IF @tintOppType=1 --Sales
				update DivisionMaster set monSCreditBalance=isnull(monSCreditBalance,0) - isnull(@monCreditAmount,0) where numDivisionID=@numDivisionID and numDomainID=@numDomainID
			else IF @tintOppType=2 --Purchase
				update DivisionMaster set monPCreditBalance=isnull(monPCreditBalance,0) - isnull(@monCreditAmount,0) where numDivisionID=@numDivisionID and numDomainID=@numDomainID
  
			UPDATE
				PO
			SET 
				intCouponCodeUsed = ISNULL(intCouponCodeUsed,0) - (CASE WHEN ISNULL(T1.intUsed,0) > 0 THEN 1 ELSE 0 END)
			FROM
				PromotionOffer PO
			INNER JOIN
				(
					SELECT
						numPromotionID,
						COUNT(*) AS intUsed
					FROM
						OpportunityMaster
					INNER JOIN	
						OpportunityItems
					ON
						OpportunityMaster.numOppId=OpportunityItems.numOppId
					WHERE
						OpportunityMaster.numOppId=@numOppId
					GROUP BY
						numPromotionID
				) AS T1
			ON
				PO.numProId = T1.numPromotionID
			WHERE
				numDomainId=@numDomainId
				AND bitRequireCouponCode = 1
  
		  /*
		  added by chintan: reason: when bizdoc is deleted and journal entries are not deleted in exceptional case, 
		  it throws Foreign key reference error when deleting order.
		  */
		  DELETE FROM dbo.General_Journal_Details WHERE numJournalId IN (SELECT numJournal_Id FROM dbo.General_Journal_Header WHERE numOppId=@numOppId AND numDomainId=@numDomainID)
		  DELETE FROM dbo.General_Journal_Header WHERE numOppId=@numOppId AND numDomainId= @numDomainID
 
		  IF ISNULL(@tintOppStatus,0)=0 OR ISNULL(@isOrderItemsAvailable,0) = 0 OR (SELECT COUNT(*) FROM WareHouseItems_Tracking WHERE numDomainID=@numDomainID AND numReferenceID=@numOppId AND tintRefType IN (3,4) AND vcDescription LIKE '%Deleted%' AND CAST(dtCreatedDate AS DATE) = CAST(GETUTCDATE() AS DATE)) > 0
			UPDATE OpportunityMaster SET tintOppStatus=0,bintModifiedDate=GETUTCDATE(),numModifiedBy=@numUserCntID where numOppId=@numOppId and numDomainID= @numDomainID             
		  ELSE
			RAISERROR ( 'INVENTORY IM-BALANCE', 16, 1 ) ;


		COMMIT TRAN 

		END TRY 
		BEGIN CATCH		
			DECLARE @strMsg VARCHAR(200)
			SET @strMsg = ERROR_MESSAGE()
			IF ( @@TRANCOUNT > 0 ) 
			BEGIN
				RAISERROR ( @strMsg, 16, 1 ) ;
				ROLLBACK TRAN
				RETURN 1
			END
		END CATCH	

		SET XACT_ABORT OFF;
	END 
END
GO
/****** Object:  StoredProcedure [dbo].[USP_OppSaveMilestone]    Script Date: 07/26/2008 16:20:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                                            
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_oppsavemilestone')
DROP PROCEDURE usp_oppsavemilestone
GO
CREATE PROCEDURE [dbo].[USP_OppSaveMilestone]                                              
(                                              
 @numOppID as numeric(9)=null,                                              
 @strMilestone as text='' ,                        
 @numUserCntID as numeric(9)=0                                           
)                                              
as 
BEGIN                                             
declare @status as tinyint                                              
declare @hDoc3  int                                              
declare @divId as numeric(9)                                              
declare @ConId as numeric(9)                                              
declare @tintOpptype as tinyint                                              
declare @numItemCode as numeric(9)                                              
declare @numOppItemCode as numeric(9)                                              
declare @numUnit as numeric(9)                                              
declare @numQtyOnHand as numeric(9)                                          
                                          
                                            
                                            
if convert(varchar(10),@strMilestone) <>''                                           
 begin                                              
  EXEC sp_xml_preparedocument @hDoc3 OUTPUT, @strMilestone  


   delete from OpportunityStageDetails                                               
   where numOppStageID not in (SELECT StageID FROM OPENXML(@hDoc3,'/NewDataSet/Table',2) with(StageID numeric,Op_Flag numeric))                                             
      and numOppID=  @numOppID                                             
                                                
    update OpportunityStageDetails set                                               
     vcstageDetail=X.vcstageDetail,                                              
     numModifiedBy=X.numModifiedBy,                                    
     bintModifiedDate=GETUTCDATE(),
     bintDueDate=CONVERT(DATETIME,replace(X.bintDueDate,'T',' ')),
     bintStageComDate= CONVERT(DATETIME,REPLACE(X.bintStageComDate,'T',' ')),
     vcComments=X.vcComments,                                              
     numAssignTo=X.numAssignTo,                                              
     bitAlert=X.bitAlert,                                               
     bitStageCompleted=X.bitStageCompleted,numStage=X.numStage ,          
  vcStagePercentageDtl =x.vcStagePercentageDtl, numTemplateId =  x.numTemplateId,          
  tintPercentage=X.tintPercentage  ,      
 numEvent=X.numEvent,      
 numReminder=X.numReminder,      
 numET=X.numET,      
 numActivity =X.numActivity ,      
 numStartDate=X.numStartDate,      
 numStartTime=X.numStartTime,      
 numStartTimePeriod=X.numStartTimePeriod,      
 numEndTime=X.numEndTime,      
 numEndTimePeriod=X.numEndTimePeriod,      
 txtCom=X.txtCom     
 ,numChgStatus =x.numChgStatus ,bitChgStatus=x.bitChgStatus    
,bitClose =x.bitClose,numType =x.numType  ,numCommActId= x.numCommActId  
      From (SELECT *                                              
     FROM OPENXML(@hDoc3,'/NewDataSet/Table[StageID>0][Op_Flag=0]',2)                                  
      with(StageID numeric,                                              
      vcstageDetail varchar(100),                                              
      numModifiedBy numeric,                                   
      bintModifiedDate VARCHAR(23),                                              
      bintDueDate VARCHAR(23),
      bintStageComDate VARCHAR(23),
      vcComments varchar(1000),                                    
      numAssignTo numeric,                                              
      bitAlert bit,                                              
      bitStageCompleted bit,                                              
      Op_Flag numeric,                     
      numStage numeric,            
    vcStagePercentageDtl varchar(500),            
    numTemplateId numeric,          
  tintPercentage tinyint,      
  numEvent tinyint,      
  numReminder numeric,      
  numET numeric,      
  numActivity numeric ,      
  numStartDate tinyint,      
  numStartTime tinyint,      
  numStartTimePeriod tinyint ,      
  numEndTime tinyint ,      
  numEndTimePeriod tinyint,       
  txtCom varchar(1000) ,numChgStatus numeric(9),bitChgStatus bit ,bitClose bit,numType numeric(9),numCommActId numeric(9) ))X                                               
    where  numOppStageID=X.StageID                                              
                                                
                                                
   insert into OpportunityStageDetails                                              
    (numOppId,vcStageDetail,numStagePercentage,numDomainId,                                        
    numCreatedBy,bintCreatedDate,numModifiedBy,bintModifiedDate,                                              
    bintDueDate,bintStageComDate,vcComments,numAssignTo,bitAlert,                                              
    bitStageCompleted,numStage,vcStagePercentageDtl,numTemplateId,numEvent,numReminder,numET,numActivity ,numStartDate,numStartTime,numStartTimePeriod ,numEndTime ,      
numEndTimePeriod, txtCom ,numChgStatus ,bitChgStatus  ,bitClose,numType,numCommActId,tintPercentage )                                              
                                                 
   select @numOppID,                                              
    X.vcstageDetail ,                                              
    X.numStagePercentage ,                              
    X.numDomainId,                                              
    X.numCreatedBy ,                                              
    GETUTCDATE(),--replace(X.bintCreatedDate,'T',' ') ,                                              
	X.numModifiedBy ,                                              
    GETUTCDATE(),--replace(X.bintModifiedDate,'T',' ') ,                                              
    replace(X.bintDueDate,'T',' ') ,
    replace(X.bintStageComDate,'T',' ') ,
    X.vcComments ,                                              
    X.numAssignTo,                                    
    X.bitAlert ,                                              
    X.bitStageCompleted,                                        
    X.numStage,x.vcStagePercentageDtl,x.numTemplateId,X.numEvent,      
 X.numReminder,      
 X.numET,      
 X.numActivity ,      
 X.numStartDate,      
 X.numStartTime,      
 X.numStartTimePeriod ,      
 X.numEndTime ,      
 X.numEndTimePeriod,       
 X.txtCom  ,x.numChgStatus ,x.bitChgStatus  ,x.bitClose ,x.numType,x.numCommActId,x.tintPercentage from(SELECT *FROM OPENXML (@hDoc3,'/NewDataSet/Table[StageID=0][Op_Flag=0]',2)                                              
   WITH  (StageID numeric,                                              
    vcstageDetail varchar(100),                                              
    numStagePercentage numeric,                                              
    numDomainId numeric,                                              
    numCreatedBy numeric,                                              
    bintCreatedDate VARCHAR(23),                                              
    numModifiedBy numeric,                                              
    bintModifiedDate VARCHAR(23),                                              
    bintDueDate VARCHAR(23),
    bintStageComDate VARCHAR(23),                                              
    vcComments varchar(1000),                                              
    numAssignTo numeric,                                              
    bitAlert bit,                                              
    bitStageCompleted bit,                                              
    Op_Flag numeric,                                        
    numStage numeric ,            
 vcStagePercentageDtl varchar(500),            
    numTemplateId numeric ,      
 numEvent tinyint,      
 numReminder numeric,      
 numET numeric,      
 numActivity numeric ,      
 numStartDate tinyint,      
 numStartTime tinyint,      
 numStartTimePeriod tinyint ,      
 numEndTime tinyint ,      
 numEndTimePeriod tinyint,       
 txtCom varchar(1000)  ,numChgStatus numeric(9),bitChgStatus bit ,bitClose bit ,numType numeric(9),numCommActId numeric(9),tintPercentage tinyint
    ))X 

         
                                        
                                                
                                                
                                                
                                                
   EXEC sp_xml_removedocument @hDoc3                                              
                                              
   end                                          
         
                                            
declare @numDivisionID as numeric(9)        
declare @tintCRMType as numeric(9)        
declare @AccountClosingDate as datetime        
                                        
if exists(select * from OpportunityStageDetails where numOppId=@numOppID and   numStagePercentage=100 and bitStageCompleted=1)              
begin           
 select @numDivisionID=numDivisionID,@AccountClosingDate=bintAccountClosingDate from  OpportunityMaster where numOppID=@numOppID         
 select @tintCRMType=tintCRMType from divisionmaster where numDivisionID =@numDivisionID         
           
 if @AccountClosingDate is null                   
  update OpportunityMaster set tintOppStatus=1,bintAccountClosingDate=getutcdate() where numOppID=@numOppID              
    else         
  update OpportunityMaster set tintOppStatus=1 where numOppID=@numOppID         
 if @tintCRMType=0        
 begin        
  update divisionmaster set tintCRMType=2,bintLeadProm=getutcdate(),bintLeadPromBy=@numUserCntID,bintProsProm=getutcdate(),bintProsPromBy=@numUserCntID        
  where numDivisionID=@numDivisionID        
 end        
 else if @tintCRMType=1        
 begin        
  update divisionmaster set tintCRMType=2,bintProsProm=getutcdate(),bintProsPromBy=@numUserCntID        
  where numDivisionID=@numDivisionID        
 end        
        
end         
else if exists(select * from OpportunityStageDetails where numOppId=@numOppID and   numStagePercentage=0 and bitStageCompleted=1)              
begin         
 select @numDivisionID=numDivisionID,@AccountClosingDate=bintAccountClosingDate from  OpportunityMaster where numOppID=@numOppID         
 select @tintCRMType=tintCRMType from divisionmaster where numDivisionID =@numDivisionID         
 if @AccountClosingDate is null                   
  update OpportunityMaster set tintOppStatus=2,bintAccountClosingDate=getutcdate() where numOppID=@numOppID              
    else         
  update OpportunityMaster set tintOppStatus=2 where numOppID=@numOppID         
end                    
                            
 select numoppitemtCode from OpportunityItems where numOppid= @numOppID                      
              
              
--Updating the warehouse items              
	declare @tintOppStatus as tinyint               
	declare @tintShipped as tinyint   
	DECLARE @tintCommitAllocation TINYINT      
	
	SELECT 
		@tintOppStatus=tintOppStatus
		,@tintOppType=tintOppType
		,@tintShipped=tintShipped
		,@tintCommitAllocation=ISNULL(tintCommitAllocation,1)
	FROM 
		OpportunityMaster 
	INNER JOIN
		Domain
	ON
		OpportunityMaster.numDomainId = Domain.numDomainId
	WHERE 
		numOppID=@numOppID              
	
	IF @tintOppStatus=1 AND @tintCommitAllocation=1        
	BEGIN
		exec USP_UpdatingInventoryonCloseDeal @numOppID,0,@numUserCntID        
	END
END
GO
/****** Object:  StoredProcedure [dbo].[USP_RevertDetailsOpp]    Script Date: 07/26/2008 16:20:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj      
/* deducts Qty from allocation and Adds back to onhand */
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_RevertDetailsOpp')
DROP PROCEDURE USP_RevertDetailsOpp
GO
CREATE PROCEDURE [dbo].[USP_RevertDetailsOpp] 
@numOppId NUMERIC(18,0),
@numOppItemId NUMERIC(18,0),
@tintMode AS TINYINT=0, -- 0:Add/Edit, 1:Delete, 2: Demoted to opportunity
@numUserCntID AS NUMERIC(9)
AS ---reverting back to previous state if deal is being edited
BEGIN 
	DECLARE @numDomain AS NUMERIC(18,0)
	DECLARE @tintCommitAllocation AS TINYINT
	SELECT @numDomain = OM.numDomainID, @tintCommitAllocation=ISNULL(tintCommitAllocation,1)  FROM [dbo].[OpportunityMaster] AS OM INNER JOIN Domain D ON OM.numDomainId = D.numDomainId WHERE [OM].[numOppId] = @numOppID
    
	DECLARE @OppType AS VARCHAR(2)              
    DECLARE @itemcode AS NUMERIC       
    DECLARE @numWareHouseItemID AS NUMERIC
	DECLARE @numWLocationID NUMERIC(18,0)                     
    DECLARE @numToWarehouseItemID AS NUMERIC 
    DECLARE @numUnits AS FLOAT                                              
    DECLARE @onHand AS FLOAT                                            
    DECLARE @onOrder AS FLOAT                                            
    DECLARE @onBackOrder AS FLOAT                                              
    DECLARE @onAllocation AS FLOAT
    DECLARE @numQtyShipped AS FLOAT
    DECLARE @numUnitHourReceived AS FLOAT
	DECLARE @numDeletedReceievedQty AS FLOAT
    DECLARE @numoppitemtCode AS NUMERIC(9) 
    DECLARE @monAmount AS DECIMAL(20,5) 
    DECLARE @monAvgCost AS DECIMAL(20,5)   
    DECLARE @Kit AS BIT                                        
    DECLARE @bitKitParent BIT
    DECLARE @bitStockTransfer BIT 
    DECLARE @numOrigUnits AS FLOAT			
    DECLARE @description AS VARCHAR(100)
	DECLARE @bitWorkOrder AS BIT
	--Added by :Sachin Sadhu||Date:18thSept2014
	--For Rental/Asset Project
	Declare @numRentalIN as FLOAT
	Declare @numRentalOut as FLOAT
	Declare @numRentalLost as FLOAT
	DECLARE @bitAsset as BIT
	--end sachin

    						
    SELECT TOP 1
            @numoppitemtCode = numoppitemtCode,
            @itemcode = OI.numItemCode,
            @numUnits = ISNULL(numUnitHour,0),
            @numWareHouseItemID = ISNULL(numWarehouseItmsID,0),
			@numWLocationID = ISNULL(numWLocationID,0),
            @Kit = ( CASE WHEN bitKitParent = 1
                               AND bitAssembly = 1 THEN 0
                          WHEN bitKitParent = 1 THEN 1
                          ELSE 0
                     END ),
            @monAmount = ISNULL(monTotAmount,0) * (CASE WHEN ISNULL(fltExchangeRate,0)=0 THEN 1 ELSE fltExchangeRate END),
            @monAvgCost = ISNULL(monAverageCost,0),
            @numQtyShipped = ISNULL(numQtyShipped,0),
            @numUnitHourReceived = ISNULL(numUnitHourReceived,0),
			@numDeletedReceievedQty = ISNULL(numDeletedReceievedQty,0),
            @bitKitParent=ISNULL(bitKitParent,0),
            @numToWarehouseItemID =OI.numToWarehouseItemID,
            @bitStockTransfer = ISNULL(OM.bitStockTransfer,0),
            @OppType = tintOppType,
		    @numRentalIN=ISNULL(oi.numRentalIN,0),
			@numRentalOut=Isnull(oi.numRentalOut,0),
			@numRentalLost=Isnull(oi.numRentalLost,0),
			@bitAsset =ISNULL(I.bitAsset,0),
			@bitWorkOrder = ISNULL(OI.bitWorkOrder,0)
    FROM    OpportunityItems OI
			LEFT JOIN WareHouseItems WI ON OI.numWarehouseItmsID=WI.numWareHouseItemID
			JOIN dbo.OpportunityMaster OM ON OI.numOppId = OM.numOppId
            JOIN Item I ON OI.numItemCode = I.numItemCode
    WHERE 
		(ISNULL(@numOppItemId,0) = 0 OR OI.numoppitemtCode=@numOppItemId) 
		AND (charitemtype='P' OR 1=(CASE WHEN tintOppType=1 THEN 
							CASE WHEN ISNULL(I.bitKitParent,0) = 1 AND ISNULL(I.bitAssembly,0) = 1 THEN 0
								 WHEN ISNULL(I.bitKitParent,0) = 1 THEN 1
								 ELSE 0 END 
							ELSE 0 END)) AND OI.numOppId = @numOppId
                           AND ( bitDropShip = 0
                                 OR bitDropShip IS NULL
                               ) 
    ORDER BY OI.numoppitemtCode

	
    WHILE @numoppitemtCode > 0                                  
    BEGIN    
        SET @numOrigUnits=@numUnits
            
        IF @bitStockTransfer=1
        BEGIN
			SET @OppType = 1
		END
                 
        IF @numWareHouseItemID>0
        BEGIN                                
			SELECT  @onHand = ISNULL(numOnHand, 0),
					@onAllocation = ISNULL(numAllocation, 0),
					@onOrder = ISNULL(numOnOrder, 0),
					@onBackOrder = ISNULL(numBackOrder, 0)
			FROM    WareHouseItems
			WHERE   numWareHouseItemID = @numWareHouseItemID                                               
        END
            
        IF @OppType = 1 AND @tintCommitAllocation=1
        BEGIN
			IF @tintMode = 2
			BEGIN
				SET @description=CONCAT('SO DEMOTED TO OPPORTUNITY (Qty:',@numUnits,' Shipped:',@numQtyShipped,')')
			END
			ELSE
			BEGIN
				SET @description=CONCAT('SO Deleted (Qty:',@numUnits,' Shipped:',@numQtyShipped,')')
			END
                
            IF @Kit = 1
			BEGIN
				exec USP_UpdatingInventoryForEmbeddedKits @numoppitemtCode,@numOrigUnits,1,@numOppID,@tintMode,@numUserCntID
			END	
			ELSE
			BEGIN
				IF @numQtyShipped>0
				BEGIN
					IF @tintMode=0
						SET @numUnits = @numUnits - @numQtyShipped
					ELSE IF @tintmode=1 OR @tintMode=2
						SET @onAllocation = @onAllocation + @numQtyShipped 
				END 
								                    
                IF @numUnits >= @onBackOrder 
                BEGIN
                    SET @numUnits = @numUnits - @onBackOrder
                    SET @onBackOrder = 0
                            
                    IF (@onAllocation - @numUnits >= 0)
						SET @onAllocation = @onAllocation - @numUnits

					IF @bitAsset=1--Not Asset
					BEGIN
						SET @onHand = @onHand +@numRentalIN+@numRentalLost+@numRentalOut     
					END
					ELSE
					BEGIN
						SET @onHand = @onHand + @numUnits     
					END                                         
                END                                            
                ELSE IF @numUnits < @onBackOrder 
                BEGIN
					IF (@onBackOrder - @numUnits >0)
						SET @onBackOrder = @onBackOrder - @numUnits
                END 
                 	
				UPDATE
					WareHouseItems
				SET 
					numOnHand = @onHand ,
					numAllocation = @onAllocation,
					numBackOrder = @onBackOrder,
					dtModified = GETDATE()
				WHERE 
					numWareHouseItemID = @numWareHouseItemID              
			END
				

			IF @numWareHouseItemID>0 AND @description <> 'DO NOT ADD WAREHOUSE TRACKING'
			BEGIN 
					EXEC dbo.USP_ManageWareHouseItems_Tracking
					@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
					@numReferenceID = @numOppId, --  numeric(9, 0)
					@tintRefType = 3, --  tinyint
					@vcDescription = @description, --  varchar(100)
					@numModifiedBy = @numUserCntID,
					@numDomainID = @numDomain 
			END		
		END      
          
        IF @bitStockTransfer=1
        BEGIN
			SET @numWareHouseItemID = @numToWarehouseItemID;
			SET @OppType = 2
			SET @numUnits = @numOrigUnits

			SELECT
				@onHand = ISNULL(numOnHand, 0),
                @onAllocation = ISNULL(numAllocation, 0),
                @onOrder = ISNULL(numOnOrder, 0),
                @onBackOrder = ISNULL(numBackOrder, 0)
			FROM 
				WareHouseItems
			WHERE
				numWareHouseItemID = @numWareHouseItemID   
		END
          
        IF @OppType = 2 
        BEGIN 
			IF (@tintmode = 1 OR @tintMode=2) AND ISNULL(@bitStockTransfer,0)=0 AND EXISTS (SELECT ID FROM OpportunityItemsReceievedLocation WHERE numDomainID=@numDomain AND numOppID=@numOppId AND numOppItemID=@numoppitemtCode)
			BEGIN
				DECLARE @TEMPReceievedItems TABLE
				(
					ID INT,
					numOIRLID NUMERIC(18,0),
					numWarehouseItemID NUMERIC(18,0),
					numUnitReceieved FLOAT,
					numDeletedReceievedQty FLOAT
				)

				DELETE FROM @TEMPReceievedItems

				INSERT INTO
					@TEMPReceievedItems
				SELECT
					ROW_NUMBER() OVER(ORDER BY ID),
					ID,
					numWarehouseItemID,
					numUnitReceieved,
					ISNULL(numDeletedReceievedQty,0)
				FROM
					OpportunityItemsReceievedLocation 
				WHERE
					numDomainID=@numDomain
					AND numOppID=@numOppId
					AND numOppItemID=@numoppitemtCode

				DECLARE @i AS INT = 1
				DECLARE @COUNT AS INT
				DECLARE @numTempOIRLID NUMERIC(18,0)
				DECLARE @numTempOnHand FLOAT
				DECLARE @numTempWarehouseItemID NUMERIC(18,0)
				DECLARE @numTempUnitReceieved FLOAT
				DECLARE @numTempDeletedReceievedQty FLOAT

				SELECT @COUNT=COUNT(*) FROM @TEMPReceievedItems

				WHILE @i <= @COUNT
				BEGIN
					SELECT 
						@numTempOIRLID=TRI.numOIRLID,
						@numTempOnHand= ISNULL(numOnHand,0),
						@numTempWarehouseItemID=ISNULL(TRI.numWarehouseItemID,0),
						@numTempUnitReceieved=ISNULL(numUnitReceieved,0),
						@numTempDeletedReceievedQty=ISNULL(numDeletedReceievedQty,0)
					FROM 
						@TEMPReceievedItems TRI
					INNER JOIN
						WareHouseItems WI
					ON
						TRI.numWarehouseItemID=WI.numWareHouseItemID
					WHERE 
						ID=@i

					IF @numTempOnHand >= (@numTempUnitReceieved - @numTempDeletedReceievedQty)
					BEGIN
						UPDATE
							WareHouseItems
						SET
							numOnHand = ISNULL(numOnHand,0) - (@numTempUnitReceieved - @numTempDeletedReceievedQty),
							dtModified = GETDATE()
						WHERE
							numWareHouseItemID=@numTempWarehouseItemID

						UPDATE
							OpportunityItems
						SET
							numDeletedReceievedQty = ISNULL(numDeletedReceievedQty,0) + (@numTempUnitReceieved - @numTempDeletedReceievedQty)
						WHERE
							numoppitemtCode=@numoppitemtCode					
					END
					ELSE
					BEGIN
						RAISERROR('INSUFFICIENT_ONHAND_QTY',16,1)
						RETURN
					END

					IF @tintMode = 2
					BEGIN
						SET @description=CONCAT('PO DEMOTED TO OPPORTUNITY (Total Qty:',@numUnits,' Deleted Qty: ',(@numTempUnitReceieved - @numTempDeletedReceievedQty),' Received:',@numTempUnitReceieved,')')
					END
					ELSE
					BEGIN
						SET @description=CONCAT('PO Deleted (Total Qty:',@numUnits,' Deleted Qty: ',(@numTempUnitReceieved - @numTempDeletedReceievedQty),' Received:',@numTempUnitReceieved,')')
					END

					EXEC dbo.USP_ManageWareHouseItems_Tracking
						@numWareHouseItemID = @numTempWarehouseItemID,
						@numReferenceID = @numOppId,
						@tintRefType = 4,
						@vcDescription = @description,
						@numModifiedBy = @numUserCntID,
						@numDomainID = @numDomain

					DELETE FROM OpportunityItemsReceievedLocation WHERE ID=@numTempOIRLID

					SET @i = @i + 1
				END
			END
		
			--WE ARE FETCHING VALUE AGAIN BECAUSE IF ITEMS ARE RECEIVED TO DIFFERENT LOCATION THAN IT'S CHANGE VALUE FROM CODE ABOVE
			SELECT @numDeletedReceievedQty=ISNULL(numDeletedReceievedQty,0) FROM OpportunityItems WHERE	numoppitemtCode=@numoppitemtCode

			IF @tintMode = 2
			BEGIN
				SET @description=CONCAT('PO DEMOTED TO OPPORTUNITY (Qty:',@numUnits,' Received:',@numUnitHourReceived,' Deleted:',@numDeletedReceievedQty,')')
			END
			ELSE
			BEGIN
				SET @description=CONCAT('PO Deleted (Qty:',@numUnits,' Received:',@numUnitHourReceived,' Deleted:',@numDeletedReceievedQty,')')
			END

			--Partial Fulfillment
			IF (@tintmode=1 OR @tintMode=2) and  @onHand >= (@numUnitHourReceived - @numDeletedReceievedQty)
			BEGIN
				SET @onHand= @onHand - (@numUnitHourReceived - @numDeletedReceievedQty)
			END
						
			SET @numUnits = @numUnits - @numUnitHourReceived

			IF (@onOrder - @numUnits)>=0
			BEGIN
				--Causing Negative Inventory Bug ID:494
				SET @onOrder = @onOrder - @numUnits	
			END
			ELSE IF (@onHand + @onOrder) - @numUnits >= 0
			BEGIN						
				SET @onHand = @onHand - (@numUnits-@onOrder)
				SET @onOrder = 0
			END
			ELSE IF  (@onHand + @onOrder + @onAllocation) - @numUnits >= 0
			BEGIN
				Declare @numDiff numeric
	
				SET @numDiff = @numUnits - @onOrder
				SET @onOrder = 0

				SET @numDiff = @numDiff - @onHand
				SET @onHand = 0

				SET @onAllocation = @onAllocation - @numDiff
				SET @onBackOrder = @onBackOrder + @numDiff
			END
					    
			UPDATE
				WareHouseItems
			SET 
				numOnHand = @onHand,
				numAllocation = @onAllocation,
				numBackOrder = @onBackOrder,
				numOnOrder = @onOrder,
				dtModified = GETDATE()
			WHERE 
				numWareHouseItemID = @numWareHouseItemID 
			
			IF (@tintmode = 1 OR @tintMode=2)
			BEGIN
				UPDATE
					OpportunityItems
				SET
					numUnitHourReceived = 0
					,numDeletedReceievedQty = 0
				WHERE
					numoppitemtCode=@numoppitemtCode
			END
				       
			EXEC dbo.USP_ManageWareHouseItems_Tracking
			@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
			@numReferenceID = @numOppId, --  numeric(9, 0)
			@tintRefType = 4, --  tinyint
			@vcDescription = @description, --  varchar(100)
			@numModifiedBy = @numUserCntID,
			@numDomainID = @numDomain                                      
		END   
                                                                
        SELECT TOP 1
                @numoppitemtCode = numoppitemtCode,
                @itemcode = OI.numItemCode,
                @numUnits = numUnitHour,
                @numWareHouseItemID = ISNULL(numWarehouseItmsID,0),
				@numWLocationID = ISNULL(numWLocationID,0),
                @Kit = ( CASE WHEN bitKitParent = 1
                                    AND bitAssembly = 1 THEN 0
                                WHEN bitKitParent = 1 THEN 1
                                ELSE 0
                            END ),
                @monAmount = ISNULL(monTotAmount,0)  * (CASE WHEN ISNULL(fltExchangeRate,0)=0 THEN 1 ELSE fltExchangeRate END),
                @monAvgCost = monAverageCost,
                @numQtyShipped = ISNULL(numQtyShipped,0),
				@numUnitHourReceived = ISNULL(numUnitHourReceived,0),
				@numDeletedReceievedQty = ISNULL(numDeletedReceievedQty,0),
				@bitKitParent=ISNULL(bitKitParent,0),
				@numToWarehouseItemID =OI.numToWarehouseItemID,
				@bitStockTransfer = ISNULL(OM.bitStockTransfer,0),
				@OppType = tintOppType,
				@numRentalIN=ISNULL(oi.numRentalIN,0),
				@numRentalOut=Isnull(oi.numRentalOut,0),
				@numRentalLost=Isnull(oi.numRentalLost,0),
				@bitAsset =ISNULL(I.bitAsset,0),
				@bitWorkOrder = ISNULL(OI.bitWorkOrder,0)
        FROM    OpportunityItems OI
				LEFT JOIN WareHouseItems WI ON OI.numWarehouseItmsID=WI.numWareHouseItemID
				JOIN dbo.OpportunityMaster OM ON OI.numOppId = OM.numOppId
                JOIN Item I ON OI.numItemCode = I.numItemCode
                                   
        WHERE 
			(ISNULL(@numOppItemId,0) = 0 OR OI.numoppitemtCode=@numOppItemId) 
			AND (charitemtype='P' OR 1=(CASE WHEN tintOppType=1 THEN 
						CASE WHEN ISNULL(I.bitKitParent,0) = 1 AND ISNULL(I.bitAssembly,0) = 1 THEN 0
								WHEN ISNULL(I.bitKitParent,0) = 1 THEN 1
								ELSE 0 END 
						ELSE 0 END))  
						AND OI.numOppId = @numOppId 
                AND OI.numoppitemtCode > @numoppitemtCode
                AND ( bitDropShip = 0
                        OR bitDropShip IS NULL
                    )
        ORDER BY OI.numoppitemtCode                                              
        
		IF @@rowcount = 0 
            SET @numoppitemtCode = 0      
	END
END
GO
/****** Object:  StoredProcedure [dbo].[USP_ShoppingCart]    Script Date: 05/07/2009 22:09:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_shoppingcart')
DROP PROCEDURE usp_shoppingcart
GO
CREATE PROCEDURE [dbo].[USP_ShoppingCart]                            
@numDivID as numeric(18),                            
@numOppID as numeric(18)=null output ,                                              
@vcPOppName as varchar(200)='' output,  
@numContactId as numeric(18),                            
@numDomainId as numeric(18),
@vcSource VARCHAR(50)=NULL,
@numCampainID NUMERIC(9)=0,
@numCurrencyID as numeric(9),
@numBillAddressId numeric(9),
@numShipAddressId numeric(9),
@bitDiscountType BIT,
@fltDiscount FLOAT,
@numProId NUMERIC(9),
@numSiteID NUMERIC(9),
@tintOppStatus INT,
@monShipCost DECIMAL(20,5),
@tintSource AS BIGINT ,
@tintSourceType AS TINYINT ,                          
@numPaymentMethodId NUMERIC(9),
@txtComments varchar(1000),
@numPartner NUMERIC(18,0),
@txtFuturePassword VARCHAR(50) = NULL,
@intUsedShippingCompany NUMERIC(18,0)=0,
@vcPartenerContact VARCHAR(200)='',
@numShipmentMethod NUMERIC(18,0)=0,
@vcOppRefOrderNo VARCHAR(200) = '',
@bitBillingTerms as bit,
@intBillingDays as integer,
@bitInterestType as bit,
@fltInterest as float,
@numOrderPromotionID NUMERIC(18,0),
@numOrderPromotionDiscountID NUMERIC(18,0)
as                            
BEGIN TRY
BEGIN TRANSACTION          
	DECLARE @CRMType AS INTEGER               
	DECLARE @numRecOwner AS INTEGER       
	DECLARE @numPartenerContact NUMERIC(18,0)=0
	DECLARE @bitAllocateInventoryOnPickList AS BIT
	DECLARE @numDiscountItemID NUMERIC(18,0)
	DECLARE @numRelationship NUMERIC(18,0) = 0
	DECLARE @numProfile NUMERIC(18,0) = 0
	DECLARE @tintCommitAllocation TINYINT
	DECLARE @tintDefaultClassType AS INT = 0
	
	SELECT 
		@numDiscountItemID=ISNULL(numDiscountServiceItemID,0)
		,@tintCommitAllocation=ISNULL(tintCommitAllocation,1)
		,@tintDefaultClassType = ISNULL(tintDefaultClassType,0) 
	FROM 
		Domain 
	WHERE 
		numDomainId=@numDomainId

	SET @numPartenerContact=(SELECT TOP 1 numContactId FROM AdditionalContactsInformation WHERE numDomainId=@numDomainID AND numDivisionId=@numPartner AND vcEmail=@vcPartenerContact)
	
	SELECT 
		@CRMType=tintCRMType
		,@numRecOwner=numRecOwner 
		,@bitAllocateInventoryOnPickList=ISNULL(bitAllocateInventoryOnPickList,0)
		,@numRelationship=ISNULL(numCompanyType,0)
		,@numProfile=ISNULL(vcProfile,0) 
	FROM 
		DivisionMaster
	INNER JOIN 
		CompanyInfo 
	ON 
		DivisionMaster.numCompanyID=CompanyInfo.numCompanyId
	WHERE 
		numDivisionID=@numDivID


	--VALIDATE PROMOTION OF COUPON CODE
	DECLARE @TEMP TABLE
	(
		ID INT IDENTITY(1,1),
		numCartID NUMERIC(18,0),
		numPromotionID NUMERIC(18,0)
	)

	INSERT INTO @TEMP
	(
		numCartID,
		numPromotionID
	)
	SELECT
		numCartId,
		PromotionID
	FROM
		CartItems 
	WHERE 
		numUserCntId=@numContactId
		AND ISNULL(PromotionID,0) > 0

	IF (SELECT COUNT(*) FROM @TEMP) > 0
	BEGIN
		-- IF ITEM PROMOTION USED IN ORDER EXIPIRES THAN RAISE ERROR
		IF  (SELECT 
					COUNT(PO.numProId)
				FROM 
					PromotionOffer PO
				LEFT JOIN
					PromotionOffer POOrder
				ON
					PO.numOrderPromotionID = POOrder.numProId
				INNER JOIN
					@TEMP T1
				ON
					PO.numProId = T1.numPromotionID
				WHERE 
					PO.numDomainId=@numDomainID 
					AND ISNULL(PO.IsOrderBasedPromotion,0) = 0
					AND ISNULL(PO.bitEnabled,0) = 1
					AND 1 = (CASE 
								WHEN ISNULL(PO.numOrderPromotionID,0) > 0 
								THEN (CASE WHEN POOrder.bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN GETUTCDATE()>=POOrder.dtValidFrom AND GETUTCDATE()<=POOrder.dtValidTo THEN 1 ELSE 0 END) END)
								ELSE (CASE WHEN PO.bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN GETUTCDATE()>=PO.dtValidFrom AND GETUTCDATE()<=PO.dtValidTo THEN 1 ELSE 0 END) END)
							END)
					AND 1 = (CASE 
								WHEN ISNULL(PO.numOrderPromotionID,0) > 0 
								THEN
									(CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=PO.numOrderPromotionID AND tintType=2 AND numRelationship=@numRelationship AND numProfile=@numProfile) > 0 THEN 1 ELSE 0 END)
								ELSE
									(CASE PO.tintCustomersBasedOn 
										WHEN 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=PO.numProId AND tintType=1 AND numDivisionID=@numDivID) > 0 THEN 1 ELSE 0 END)
										WHEN 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=PO.numProId AND tintType=2 AND numRelationship=@numRelationship AND numProfile=@numProfile) > 0 THEN 1 ELSE 0 END)
										WHEN 3 THEN 1
										ELSE 0
									END)
							END)
			) <> (SELECT COUNT(*) FROM @TEMP)
		BEGIN
			RAISERROR('ITEM_PROMOTION_EXPIRED',16,1)
			RETURN
		END

		-- NOW IF COUPON BASED ITEM PROMOTION IS USED THEN VALIDATE COUPON AND ITS USAGE
		IF 
		(
			SELECT 
				COUNT(*)
			FROM
				PromotionOffer PO
			INNER JOIN
				@TEMP T1
			ON
				PO.numProId = T1.numPromotionID
			WHERE
				PO.numDomainId = @numDomainId
				AND ISNULL(IsOrderBasedPromotion,0) = 0
				AND ISNULL(numOrderPromotionID,0) > 0 
		) = 1
		BEGIN
			IF ISNULL(@numOrderPromotionDiscountID,0) > 0
			BEGIN
				IF (SELECT 
						COUNT(*)
					FROM
						DiscountCodes DC
					WHERE
						Dc.numDiscountId = @numOrderPromotionDiscountID
					) = 0
				BEGIN
					RAISERROR('INVALID_COUPON_CODE_ITEM_PROMOTION',16,1)
					RETURN
				END
				ELSE
				BEGIN
					-- IF THERE IS COUPON CODE USAGE LIMIT THAN VALIDATE USAGE LIMIT
					IF (SELECT 
							COUNT(*)
						FROM
							PromotionOffer PO
						INNER JOIN 
							PromotionOffer POOrder
						ON
							PO.numOrderPromotionID = POOrder.numProId
						INNER JOIN
							@TEMP T1
						ON
							PO.numProId = T1.numPromotionID
						INNER JOIN
							DiscountCodes DC
						ON
							POOrder.numProId = DC.numPromotionID
						WHERE
							PO.numDomainId = @numDomainId
							AND ISNULL(PO.IsOrderBasedPromotion,0) = 0
							AND DC.numDiscountId = @numOrderPromotionDiscountID
							AND 1 = (CASE WHEN ISNULL(DC.CodeUsageLimit,0) <> 0 THEN 1 ELSE 0 END)
							AND ISNULL((SELECT intCodeUsed FROM DiscountCodeUsage WHERE DiscountCodeUsage.numDiscountId=DC.numDiscountId AND numDivisionId=@numDivID),0) >= DC.CodeUsageLimit) > 0
					BEGIN
						RAISERROR('ITEM_PROMOTION_COUPON_CODE_USAGE_LIMIT_EXCEEDED',16,1)
						RETURN
					END
				END
			END
			ELSE
			BEGIN
				RAISERROR('COUPON_CODE_REQUIRED_ITEM_PROMOTION',16,1)
				RETURN
			END
		END
	END

	IF ISNULL(@numOrderPromotionID,0) > 0
	BEGIN
		-- VALIDTE IF ITS ORDER BASED PROMOTION (BOTH ITEM AND ORDER BASED PROMOTION CAN HAVE COUPON CODE)
		IF EXISTS (SELECT numProId FROM PromotionOffer WHERE numDomainId=@numDomainId AND numProId=@numOrderPromotionID AND ISNULL(IsOrderBasedPromotion,0) = 1)
		BEGIN
			-- CHECK IF ORDER PROMOTION IS STILL VALID
			IF NOT EXISTS (SELECT 
								PO.numProId
							FROM 
								PromotionOffer PO
							INNER JOIN 
								PromotionOfferOrganizations PORG
							ON 
								PO.numProId = PORG.numProId
							WHERE 
								numDomainId=@numDomainID 
								AND PO.numProId=@numOrderPromotionID
								AND ISNULL(IsOrderBasedPromotion,0) = 1
								AND ISNULL(bitEnabled,0) = 1
								AND 1 = (CASE WHEN bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN GETUTCDATE()>=dtValidFrom AND GETUTCDATE()<=dtValidTo THEN 1 ELSE 0 END) END)
								AND numRelationship=@numRelationship 
								AND numProfile=@numProfile
			)
			BEGIN
				RAISERROR('ORDER_PROMOTION_EXPIRED',16,1)
				RETURN
			END
			ELSE IF ISNULL((SELECT ISNULL(bitRequireCouponCode,0) FROM PromotionOffer WHERE numDomainId=@numDomainId AND numProId=@numOrderPromotionID),0) = 1
			BEGIN
				IF ISNULL(@numOrderPromotionDiscountID,0) > 0
				BEGIN
					IF (SELECT 
							COUNT(*)
						FROM
							PromotionOffer PO
						INNER JOIN
							DiscountCodes DC
						ON
							PO.numProId = DC.numPromotionID
						WHERE
							PO.numDomainId = @numDomainId
							AND PO.numProId = @numOrderPromotionID
							AND ISNULL(IsOrderBasedPromotion,0) = 1
							AND ISNULL(bitRequireCouponCode,0) = 1
							AND ISNULL(DC.numDiscountId,0) = @numOrderPromotionDiscountID) = 0
					BEGIN
						RAISERROR('INVALID_COUPON_CODE_ORDER_PROMOTION',16,1)
						RETURN
					END
					ELSE
					BEGIN
						-- IF THERE IS COUPON CODE USAGE LIMIT THAN VALIDATE USAGE LIMIT
						IF (SELECT 
								COUNT(*)
							FROM
								PromotionOffer PO
							INNER JOIN
								DiscountCodes DC
							ON
								PO.numProId = DC.numPromotionID
							WHERE
								PO.numDomainId = @numDomainId
								AND PO.numProId = @numOrderPromotionID
								AND ISNULL(IsOrderBasedPromotion,0) = 1
								AND ISNULL(bitRequireCouponCode,0) = 1
								AND ISNULL(DC.numDiscountId,0) = @numOrderPromotionDiscountID
								AND 1 = (CASE WHEN ISNULL(DC.CodeUsageLimit,0) <> 0 THEN 1 ELSE 0 END)
								AND ISNULL((SELECT intCodeUsed FROM DiscountCodeUsage WHERE DiscountCodeUsage.numDiscountId=DC.numDiscountId AND numDivisionId=@numDivID),0) >= DC.CodeUsageLimit) > 0
						BEGIN
							RAISERROR('ORDER_PROMOTION_COUPON_CODE_USAGE_LIMIT_EXCEEDED',16,1)
							RETURN
						END
						ELSE
						BEGIN	
							IF EXISTS (SELECT DC.numDiscountID FROM DiscountCodes DC INNER JOIN DiscountCodeUsage DCU ON DC.numDiscountID=DCU.numDIscountID WHERE DC.numPromotionID=@numOrderPromotionID AND DCU.numDivisionID=@numDivID AND DC.numDiscountId=@numOrderPromotionDiscountID)
							BEGIN
								UPDATE
									DCU
								SET
									DCU.intCodeUsed = ISNULL(DCU.intCodeUsed,0) + 1
								FROM 
									DiscountCodes DC 
								INNER JOIN 
									DiscountCodeUsage DCU 
								ON 
									DC.numDiscountID=DCU.numDIscountID 
								WHERE 
									DC.numPromotionID=@numOrderPromotionID 
									AND DCU.numDivisionID=@numDivID 
									AND DC.numDiscountId=@numOrderPromotionDiscountID
							END
							ELSE
							BEGIN
								INSERT INTO DiscountCodeUsage
								(
									numDiscountId
									,numDivisionId
									,intCodeUsed
								)
								SELECT 
									DC.numDiscountId
									,@numDivID
									,1
								FROM
									DiscountCodes DC
								WHERE
									DC.numPromotionID=@numOrderPromotionID
									AND DC.numDiscountId=@numOrderPromotionDiscountID
							END
						END
					END
				END
				ELSE
				BEGIN
					RAISERROR('COUPON_CODE_REQUIRED_ORDER_PROMOTION',16,1)
					RETURN
				END
			END
		END
	END
	ELSE
	BEGIN
		-- IF PROMOTION ID NOT AVAILABLE THAN REMOVE ORDER PROMOTION DISCOUNT ITEM
		DELETE FROM CartItems WHERE numUserCntId =@numContactId AND numItemCode=@numDiscountItemID
	END                        
		
	IF @CRMType= 0                             
	BEGIN
		UPDATE 
			DivisionMaster                               
		SET 
			tintCRMType=1                              
		WHERE 
			numDivisionID=@numDivID
	END                            

	DECLARE @TotalAmount AS FLOAT
       

	IF LEN(ISNULL(@txtFuturePassword,'')) > 0
	BEGIN
		DECLARE @numExtranetID NUMERIC(18,0)

		SELECT @numExtranetID=numExtranetID FROM ExtarnetAccounts WHERE numDivisionID=@numDivID

		UPDATE ExtranetAccountsDtl SET vcPassword=@txtFuturePassword WHERE numExtranetID=@numExtranetID
	END                      

	DECLARE @numTemplateID NUMERIC
	SELECT 
		@numTemplateID= numMirrorBizDocTemplateId 
	FROM 
		eCommercePaymentConfig 
	WHERE 
		numSiteId=@numSiteID 
		AND numPaymentMethodId = @numPaymentMethodId 
		AND numDomainID = @numDomainID   

	DECLARE @fltExchangeRate FLOAT                                 
    
	IF @numCurrencyID=0 
	BEGIN
		SELECT 
			@numCurrencyID=ISNULL(numCurrencyID,0) 
		FROM 
			Domain 
		WHERE 
			numDomainID=@numDomainId                            
	END
		
	IF @numCurrencyID=0 
	BEGIN
		SET @fltExchangeRate=1
	END
	ELSE 
	BEGIN
		SET @fltExchangeRate=dbo.GetExchangeRate(@numDomainID,@numCurrencyID)                          
	END                      
                           
	DECLARE @numAccountClass AS NUMERIC(18) = 0

	IF @tintDefaultClassType <> 0
	BEGIN
		SELECT 
			@numAccountClass=ISNULL(numDefaultClass,0) 
		FROM 
			dbo.UserMaster UM 
		JOIN 
			dbo.Domain D 
		ON 
			UM.numDomainID=D.numDomainId
		WHERE 
			D.numDomainId=@numDomainId 
			AND UM.numUserDetailId=@numContactId
	END
	ELSE IF @tintDefaultClassType = 2 --COMPANY
	BEGIN
		SELECT 
			@numAccountClass=ISNULL(numAccountClassID,0) 
		FROM 
			dbo.DivisionMaster DM 
		WHERE 
			DM.numDomainId=@numDomainId 
			AND DM.numDivisionID=@numDivID
	END
	ELSE
	BEGIN
		SET @numAccountClass = 0
	END

	SET @numAccountClass=(SELECT TOP 1 numDefaultClass FROM dbo.eCommerceDTL WHERE numSiteId=@numSiteID AND numDomainId=@numDomainId)                  

	INSERT INTO OpportunityMaster
	(                              
		numContactId,numDivisionId,txtComments,numCampainID,bitPublicFlag,tintSource,tintSourceType,vcPOppName,monPAmount,                              
		numCreatedBy,bintCreatedDate,numModifiedBy,bintModifiedDate,numDomainId,tintOppType,tintOppStatus,intPEstimatedCloseDate,              
		numRecOwner,bitOrder,numCurrencyID,fltExchangeRate,fltDiscountTotal,bitDiscountTypeTotal,monShipCost,numOppBizDocTempID
		,numAccountClass,numPartner,intUsedShippingCompany,numPartenerContact,numShippingService,dtReleaseDate,vcOppRefOrderNo,
		bitBillingTerms,intBillingDays,bitInterestType,fltInterest,numDiscountID
	)                              
	VALUES                              
	(                              
		@numContactId,@numDivID,@txtComments,@numCampainID,0,@tintSource,@tintSourceType,ISNULL(@vcPOppName,'SO'),0,                                
		@numContactId,GETUTCDATE(),@numContactId,GETUTCDATE(),@numDomainId,1,@tintOppStatus,GETUTCDATE(),@numRecOwner,
		1,@numCurrencyID,@fltExchangeRate,@fltDiscount,@bitDiscountType,@monShipCost,@numTemplateID,@numAccountClass,@numPartner
		,@intUsedShippingCompany,@numPartenerContact,@numShipmentMethod,GETDATE(),@vcOppRefOrderNo,@bitBillingTerms,@intBillingDays,
		@bitInterestType,@fltInterest,@numOrderPromotionDiscountID
	)
		
	SET @numOppID=scope_identity()                             

	EXEC dbo.USP_UpdateNameTemplateValue 1,@numDomainId,@numOppID
	SELECT @vcPOppName = vcPOppName FROM OpportunityMaster WHERE numOppID= @numOppID

	INSERT INTO OpportunityItems
	(
		numOppId,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,vcAttributes,vcAttrValues,[vcItemName],
		[vcModelID],[vcManufacturer],vcPathForTImage, monVendorCost,numUOMId,bitDiscountType,fltDiscount,monTotAmtBefDiscount,numPromotionID,
		bitPromotionTriggered,bitDropShip,vcPromotionDetail,vcChildKitSelectedItems
	)                                                                                                              
	SELECT 
		@numOppID,X.numItemCode,x.numUnitHour * x.decUOMConversionFactor,x.monPrice/x.decUOMConversionFactor,x.monTotAmount,X.vcItemDesc,
		NULLIF(X.numWarehouseItmsID,0),X.vcItemType,X.vcAttributes,X.vcAttrValues,(SELECT vcItemName FROM item WHERE numItemCode = X.numItemCode),
		(SELECT vcModelID FROM item WHERE numItemCode = X.numItemCode),(SELECT vcManufacturer FROM item WHERE numItemCode = X.numItemCode),
		(SELECT TOP 1  vcPathForTImage FROM dbo.ItemImages WHERE numItemCode = X.numItemCode AND bitDefault = 1 AND numDomainId = @numDomainId),
		(SELECT ISNULL(VN.monCost,0) FROM Vendor VN INNER JOIN Item IT ON IT.numItemCode=VN.numItemCode and VN.numVendorID=IT.numVendorID WHERE VN.numItemCode=X.numItemCode), 
		numUOM,bitDiscountType,fltDiscount,monTotAmtBefDiscount,X.PromotionID,X.bitPromotionTrigerred
		,ISNULL((SELECT bitAllowDropShip FROM Item WHERE numItemCode=X.numItemCode),0),x.PromotionDesc,ISNULL(vcChildKitItemSelection,'')
	FROM 
		dbo.CartItems X 
	WHERE 
		numUserCntId =@numContactId


	--INSERT KIT ITEMS 
	IF (SELECT 
			COUNT(*) 
		FROM 
			OpportunityItems OI 
		LEFT JOIN
			WarehouseItems WI
		ON
			OI.numWarehouseItmsID = WI.numWarehouseItemID
		LEFT JOIN
			Warehouses W
		ON
			WI.numWarehouseID = W.numWarehouseID
		JOIN 
			ItemDetails ID 
		ON 
			OI.numItemCode=ID.numItemKitID 
		JOIN
			Item I
		ON
			ID.numChildItemID = I.numItemCode
		WHERE 
			OI.numOppId=@numOppId  
			AND ISNULL(OI.bitDropShip,0) = 0
			AND ISNULL(OI.numWarehouseItmsID,0) > 0
			AND I.charItemType = 'P'
			AND ISNULL((SELECT TOP 1 numWarehouseItemID FROM WareHouseItems WHERE numItemID=ID.numChildItemID AND numWareHouseID = W.numWarehouseID),0) = 0) > 0
	BEGIN
		RAISERROR('SELECTED_WAREHOUSE_IS_NOT_AVAILABLE_FOR_ALL_CHILD_ITEMS',16,1)
	END

			                
	INSERT INTO OpportunityKitItems
	(
		numOppId,
		numOppItemID,
		numChildItemID,
		numWareHouseItemId,
		numQtyItemsReq,
		numQtyItemsReq_Orig,
		numUOMId,
		numQtyShipped,
		monAvgCost
	)
	SELECT
		@numOppId,
		OI.numoppitemtCode,
		ID.numChildItemID,
		(SELECT TOP 1 numWarehouseItemID FROM WareHouseItems WHERE numItemID=ID.numChildItemID AND WareHouseItems.numWareHouseID = W.numWarehouseID ORDER BY numWareHouseItemID),
		(ID.numQtyItemsReq * ISNULL(dbo.fn_UOMConversion(ID.numUOMId,ID.numChildItemID,@numDomainID,I.numBaseUnit),1)) * OI.numUnitHour,
		ID.numQtyItemsReq,
		ID.numUOMId,
		0,
		ISNULL(I.monAverageCost,0)
	FROM 
		OpportunityItems OI 
	LEFT JOIN
		WarehouseItems WI
	ON
		OI.numWarehouseItmsID = WI.numWarehouseItemID
	LEFT JOIN
		Warehouses W
	ON
		WI.numWarehouseID = W.numWarehouseID
	JOIN 
		ItemDetails ID 
	ON 
		OI.numItemCode=ID.numItemKitID 
	JOIN
		Item I
	ON
		ID.numChildItemID = I.numItemCode
	WHERE 
		OI.numOppId=@numOppId 

	--INSERT KIT ITEM HAS ANOTHER KIT AS CHILD THEN SAVE CONFIGURATION OF KIT
	DECLARE @TempKitConfiguration TABLE
	(
		numOppItemID NUMERIC(18,0),
		numItemCode NUMERIC(18,0),
		KitChildItems VARCHAR(MAX)
	)

	INSERT INTO 
		@TempKitConfiguration
	SELECT
		numoppitemtCode,
		numItemCode,
		vcChildKitSelectedItems
	FROM
		OpportunityItems
	WHERE
		numOppId = @numOppID
		AND LEN(ISNULL(vcChildKitSelectedItems,'')) > 0
					

	IF (SELECT 
			COUNT(*) 
		FROM 
		(
			SELECT 
				t1.numOppItemID
				,t1.numItemCode
				,SUBSTRING(items,0,CHARINDEX('-',items)) AS numKitItemID
				,SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)) As numKitChildItemID
			FROM 
				@TempKitConfiguration t1
			CROSS APPLY
			(
				SELECT
					*
				FROM 
					dbo.Split((SELECT KitChildItems FROM @TempKitConfiguration t3 WHERE t3.numOppItemID=t1.numOppItemID AND t3.numItemCode=t1.numItemCode),',') 
			) as t2
		) TempChildItems
		INNER JOIN
			OpportunityKitItems OKI
		ON
			OKI.numOppId=@numOppId
			AND OKI.numOppItemID=TempChildItems.numOppItemID
			AND OKI.numChildItemID=TempChildItems.numKitItemID
		INNER JOIN
			OpportunityItems OI
		ON
			OI.numItemCode = TempChildItems.numItemCode
			AND OI.numoppitemtcode = OKI.numOppItemID
		LEFT JOIN
			WarehouseItems WI
		ON
			OI.numWarehouseItmsID = WI.numWarehouseItemID
		LEFT JOIN
			Warehouses W
		ON
			WI.numWarehouseID = W.numWarehouseID
		INNER JOIN
			ItemDetails
		ON
			TempChildItems.numKitItemID = ItemDetails.numItemKitID
			AND TempChildItems.numKitChildItemID = ItemDetails.numChildItemID
		INNER JOIN
			Item 
		ON
			ItemDetails.numChildItemID = Item.numItemCode
		WHERE
			Item.charItemType = 'P'
			AND ISNULL((SELECT TOP 1 numWarehouseItemID FROM WareHouseItems WHERE numItemID=ItemDetails.numChildItemID AND WareHouseItems.numWareHouseID = W.numWarehouseID),0) = 0) > 0
	BEGIN
		RAISERROR('SELECTED_WAREHOUSE_IS_NOT_AVAILABLE_FOR_ALL_CHILD_ITEMS',16,1)
	END

	INSERT INTO OpportunityKitChildItems
	(
		numOppID,
		numOppItemID,
		numOppChildItemID,
		numItemID,
		numWareHouseItemId,
		numQtyItemsReq,
		numQtyItemsReq_Orig,
		numUOMId,
		numQtyShipped,
		monAvgCost
	)
	SELECT 
		@numOppID,
		OKI.numOppItemID,
		OKI.numOppChildItemID,
		ItemDetails.numChildItemID,
		(SELECT TOP 1 numWarehouseItemID FROM WareHouseItems WHERE numItemID=ItemDetails.numChildItemID AND WareHouseItems.numWareHouseID = W.numWarehouseID ORDER BY numWareHouseItemID),
		(ItemDetails.numQtyItemsReq * ISNULL(dbo.fn_UOMConversion(ItemDetails.numUOMId,ItemDetails.numChildItemID,@numDomainID,Item.numBaseUnit),1)) * OKI.numQtyItemsReq,
		ItemDetails.numQtyItemsReq,
		ItemDetails.numUOMId,
		0,
		ISNULL(Item.monAverageCost,0)
	FROM
	(
		SELECT 
			t1.numOppItemID
			,t1.numItemCode
			,SUBSTRING(items,0,CHARINDEX('-',items)) AS numKitItemID
			,SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)) As numKitChildItemID
		FROM 
			@TempKitConfiguration t1
		CROSS APPLY
		(
			SELECT
				*
			FROM 
				dbo.Split((SELECT KitChildItems FROM @TempKitConfiguration t3 WHERE t3.numOppItemID=t1.numOppItemID AND t3.numItemCode=t1.numItemCode),',') 
		) as t2
	) TempChildItems
	INNER JOIN
		OpportunityKitItems OKI
	ON
		OKI.numOppId=@numOppId
		AND OKI.numOppItemID=TempChildItems.numOppItemID
		AND OKI.numChildItemID=TempChildItems.numKitItemID
	INNER JOIN
		OpportunityItems OI
	ON
		OI.numItemCode = TempChildItems.numItemCode
		AND OI.numoppitemtcode = OKI.numOppItemID
	LEFT JOIN
		WarehouseItems WI
	ON
		OI.numWarehouseItmsID = WI.numWarehouseItemID
	LEFT JOIN
		Warehouses W
	ON
		WI.numWarehouseID = W.numWarehouseID
	INNER JOIN
		ItemDetails
	ON
		TempChildItems.numKitItemID = ItemDetails.numItemKitID
		AND TempChildItems.numKitChildItemID = ItemDetails.numChildItemID
	INNER JOIN
		Item 
	ON
		ItemDetails.numChildItemID = Item.numItemCode

	BEGIN TRY
		DECLARE @TEMPContainer TABLE
		(
			numConainer NUMERIC(18,0)
			,numNoItemIntoContainer NUMERIC(18,0)
			,numWarehouseID NUMERIC(18,0)
			,monPrice DECIMAL(20,5)
			,monAverageCost DECIMAL(20,5)
			,numQtyToFit FLOAT
			,vcItemName VARCHAR(300)
			,vcItemDesc VARCHAR(1000)
			,vcModelID VARCHAR(300)
		)

		INSERT INTO @TEMPContainer
		(
			numConainer
			,numNoItemIntoContainer
			,numWarehouseID
			,monPrice
			,monAverageCost
			,numQtyToFit
			,vcItemName
			,vcItemDesc
			,vcModelID
		)
		SELECT
			I.numContainer
			,I.numNoItemIntoContainer
			,WI.numWarehouseID
			,ISNULL(IContainer.monListPrice,0)
			,ISNULL(IContainer.monAverageCost,0)
			,SUM(numUnitHour)
			,ISNULL(IContainer.vcItemName,'')
			,ISNULL(IContainer.txtItemDesc,'')
			,ISNULL(IContainer.vcModelID,'')
		FROM
			OpportunityItems OI
		INNER JOIN
			Item I
		ON
			OI.numItemCode = I.numItemCode
		INNER JOIN
			Item IContainer
		ON
			I.numContainer = IContainer.numItemCode
		LEFT JOIN
			WareHouseItems WI
		ON
			OI.numWarehouseItmsID = WI.numWareHouseItemID
		WHERE
			numOppID = @numOppID
			AND ISNULL(OI.bitDropShip,0) = 0
			AND ISNULL(I.bitContainer,0) = 0
			AND ISNULL(IContainer.bitContainer,0) = 1
			AND ISNULL(I.numContainer,0) > 0
			AND ISNULL(I.numNoItemIntoContainer,0) > 0
		GROUP BY
			I.numContainer
			,I.numNoItemIntoContainer
			,IContainer.monListPrice
			,IContainer.monAverageCost
			,WI.numWarehouseID
			,IContainer.vcItemName
			,IContainer.txtItemDesc
			,IContainer.vcModelID

		INSERT INTO OpportunityItems
		(
			numOppId
			,numItemCode
			,numWarehouseItmsID
			,numUnitHour
			,monPrice
			,fltDiscount
			,bitDiscountType
			,monTotAmount
			,monTotAmtBefDiscount
			,monAvgCost
			,vcItemName
			,vcItemDesc
			,vcModelID
		)
		SELECT
			@numOppID
			,numConainer
			,(SELECT TOP 1 numWarehouseItemID FROM WarehouseItems WHERE numItemID=numConainer AND WarehouseItems.numWarehouseID=T1.numWarehouseID ORDER BY numWareHouseItemID)
			,(CASE WHEN numQtyToFit > numNoItemIntoContainer THEN CEILING(numQtyToFit/numNoItemIntoContainer) ELSE 1 END)
			,monPrice
			,0
			,1
			,(CASE WHEN numQtyToFit > numNoItemIntoContainer THEN CEILING(numQtyToFit/numNoItemIntoContainer) ELSE 1 END) * monPrice
			,(CASE WHEN numQtyToFit > numNoItemIntoContainer THEN CEILING(numQtyToFit/numNoItemIntoContainer) ELSE 1 END) * monPrice
			,monAverageCost
			,vcItemName
			,vcItemDesc
			,vcModelID
		FROM
			@TEMPContainer T1

		DECLARE @tintShipped AS TINYINT      
		DECLARE @tintOppType AS TINYINT
	
		SELECT 
			@tintOppStatus=tintOppStatus
			,@tintOppType=tintOppType
			,@tintShipped=tintShipped 
		FROM 
			OpportunityMaster 
		WHERE 
			numOppID=@numOppID
	END TRY
	BEGIN CATCH
		-- DO NOT RAISE ERROR
	END CATCH           
	
	EXEC USP_OpportunityItems_UpdateWarehouseMapping @numDomainID,@numOppID

	IF @tintOppStatus=1 AND @tintCommitAllocation=1               
	BEGIN         
		EXEC USP_UpdatingInventoryonCloseDeal @numOppID,0,@numContactId
	END
	
	EXEC USP_OpportunityItems_CheckWarehouseMapping @numDomainID,@numOppID                       
                  
	SELECT
		@TotalAmount=SUM(monTotAmount)
	FROM 
		OpportunityItems 
	WHERE 
		numOppId=@numOppID                            
 
	UPDATE 
		OpportunityMaster 
	SET 
		monPamount = @TotalAmount - @fltDiscount                           
	WHERE 
		numOppId=@numOppID                           
                                               
	IF(@vcSource IS NOT NULL)
	BEGIN
		insert into OpportunityLinking ([numParentOppID],[numChildOppID],[vcSource],[numParentOppBizDocID],numPromotionId,numSiteID)  values(null,@numOppID,@vcSource,NULL,@numProId,@numSiteID);
	END

	--Add/Update Address Details
	DECLARE @vcStreet  varchar(100),@vcCity  varchar(50),@vcPostalCode  varchar(15),@vcCompanyName  varchar(100),@vcAddressName VARCHAR(50), @vcAltContact VARCHAR(200)     
	DECLARE @numState  numeric(9),@numCountry  numeric(9), @numCompanyId  numeric(9), @numContact NUMERIC(18,0)
	DECLARE @bitIsPrimary BIT, @bitAltContact BIT;

	--Bill Address
	IF @numBillAddressId>0
	BEGIN
		SELECT
			@vcStreet=ISNULL(vcStreet,'')
			,@vcCity=ISNULL(vcCity,'')
			,@vcPostalCode=ISNULL(vcPostalCode,'')
			,@numState=ISNULL(numState,0)
			,@numCountry=ISNULL(numCountry,0)
			,@bitIsPrimary=bitIsPrimary
			,@vcAddressName=vcAddressName
			,@numContact=numContact
			,@bitAltContact=bitAltContact
			,@vcAltContact=vcAltContact
		FROM 
			dbo.AddressDetails 
		WHERE 
			numDomainID = @numDomainID 
			AND numAddressID = @numBillAddressId 
            
  
  		EXEC dbo.USP_UpdateOppAddress
		@numOppID = @numOppID, --  numeric(9, 0)
		@byteMode = 0, --  tinyint
		@vcStreet = @vcStreet, --  varchar(100)
		@vcCity = @vcCity, --  varchar(50)
		@vcPostalCode = @vcPostalCode, --  varchar(15)
		@numState = @numState, --  numeric(9, 0)
		@numCountry = @numCountry, --  numeric(9, 0)
		@vcCompanyName = '', --  varchar(100)
		@numCompanyId = 0, --  numeric(9, 0)
		@vcAddressName =@vcAddressName,
		@bitCalledFromProcedure=1,
		@numContact = @numContact,
		@bitAltContact = @bitAltContact,
		@vcAltContact = @vcAltContact
        
	END
  
  
	--Ship Address
	IF @numShipAddressId>0
	BEGIN
   		SELECT 
			@vcStreet=ISNULL(vcStreet,'')
			,@vcCity=ISNULL(vcCity,'')
			,@vcPostalCode=ISNULL(vcPostalCode,'')
			,@numState=ISNULL(numState,0)
			,@numCountry=ISNULL(numCountry,0)
			,@bitIsPrimary=bitIsPrimary
			,@vcAddressName=vcAddressName
			,@numContact=numContact
			,@bitAltContact=bitAltContact
			,@vcAltContact=vcAltContact
		FROM 
			dbo.AddressDetails 
		WHERE 
			numDomainID = @numDomainID 
			AND numAddressID = @numShipAddressId 
  
		select  @vcCompanyName=vcCompanyName,@numCompanyId=div.numCompanyID from CompanyInfo Com                            
		join divisionMaster Div                            
		on div.numCompanyID=com.numCompanyID                            
		where div.numdivisionID=@numDivID

  		EXEC dbo.USP_UpdateOppAddress
		@numOppID = @numOppID, --  numeric(9, 0)
		@byteMode = 1, --  tinyint
		@vcStreet = @vcStreet, --  varchar(100)
		@vcCity = @vcCity, --  varchar(50)
		@vcPostalCode = @vcPostalCode, --  varchar(15)
		@numState = @numState, --  numeric(9, 0)
		@numCountry = @numCountry, --  numeric(9, 0)
		@vcCompanyName = @vcCompanyName, --  varchar(100)
		@numCompanyId =@numCompanyId, --  numeric(9, 0)
		@vcAddressName =@vcAddressName,
		@bitCalledFromProcedure=1,
		@numContact = @numContact,
		@bitAltContact = @bitAltContact,
		@vcAltContact = @vcAltContact
   	
	END

	-- IMPORTANT: KEEP THIS BELOW ADDRESS UPDATE LOGIC
	--Insert Tax for Division                       
	INSERT dbo.OpportunityMasterTaxItems 
	(
		numOppId,
		numTaxItemID,
		fltPercentage,
		tintTaxType,
		numTaxID
	) 
	SELECT 
		@numOppID,
		TI.numTaxItemID,
		TEMPTax.decTaxValue,
		TEMPTax.tintTaxType,
		0
	FROM 
		TaxItems TI 
	JOIN 
		DivisionTaxTypes DTT 
	ON 
		TI.numTaxItemID = DTT.numTaxItemID
	CROSS APPLY
	(
		SELECT
			decTaxValue,
			tintTaxType,
			numTaxID
		FROM
			dbo.fn_CalItemTaxAmt(@numDomainID,@numDivID,0,TI.numTaxItemID,@numOppId,0,NULL)
	) AS TEMPTax
	WHERE 
		DTT.numDivisionID=@numDivID AND DTT.bitApplicable=1
	UNION 
	SELECT 
		@numOppID,
		0,
		TEMPTax.decTaxValue,
		TEMPTax.tintTaxType,
		0
	FROM 
		dbo.DivisionMaster 
	CROSS APPLY
	(
		SELECT
			decTaxValue,
			tintTaxType,
			numTaxID
		FROM
			dbo.fn_CalItemTaxAmt(@numDomainID,@numDivID,0,0,@numOppId,1,NULL)
	) AS TEMPTax
	WHERE 
		bitNoTax=0 
		AND numDivisionID=@numDivID
	UNION 
	SELECT
		@numOppId
		,1
		,decTaxValue
		,tintTaxType
		,numTaxID
	FROM
		dbo.fn_CalItemTaxAmt(@numDomainID,@numDivID,0,1,@numOppId,1,NULL)
  
	--Delete Tax for Opportunity Items if item deleted 
	DELETE FROM OpportunityItemsTaxItems WHERE numOppId=@numOppId AND numOppItemID NOT IN (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId=@numOppID)

	--Insert Tax for Opportunity Items
	INSERT INTO dbo.OpportunityItemsTaxItems 
	(
		numOppId,
		numOppItemID,
		numTaxItemID,
		numTaxID
	) 
	SELECT 
		@numOppId,
		OI.numoppitemtCode,
		TI.numTaxItemID,
		0 
	FROM 
		dbo.OpportunityItems OI 
	JOIN 
		dbo.ItemTax IT 
	ON 
		OI.numItemCode=IT.numItemCode 
	JOIN
		TaxItems TI 
	ON 
		TI.numTaxItemID=IT.numTaxItemID 
	WHERE 
		OI.numOppId=@numOppID 
		AND IT.bitApplicable=1 
		AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)
	UNION
	SELECT 
		@numOppId,
		OI.numoppitemtCode,
		0,
		0 
	FROM 
		dbo.OpportunityItems OI 
	JOIN 
		dbo.Item I 
	ON 
		OI.numItemCode=I.numItemCode
	WHERE 
		OI.numOppId=@numOppID 
		AND I.bitTaxable=1 
		AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)
	UNION
	SELECT
		@numOppId,
		OI.numoppitemtCode,
		1,
		TD.numTaxID
	FROM
		dbo.OpportunityItems OI 
	INNER JOIN
		ItemTax IT
	ON
		IT.numItemCode = OI.numItemCode
	INNER JOIN
		TaxDetails TD
	ON
		TD.numTaxID = IT.numTaxID
		AND TD.numDomainId = @numDomainId
	WHERE
		OI.numOppId = @numOppID
		AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)

	UPDATE 
		OI
	SET
		OI.vcInclusionDetail = [dbo].[GetOrderAssemblyKitInclusionDetails](OI.numOppID,OI.numoppitemtCode,OI.numUnitHour,1,1)
	FROM 
		OpportunityItems OI
	INNER JOIN
		Item I
	ON
		OI.numItemCode = I.numItemCode
	WHERE
		numOppId=@numOppID
		AND ISNULL(I.bitKitParent,0) = 1
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

  -- Whoops, there was an error
  IF @@TRANCOUNT > 0
     ROLLBACK

  SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_UpdateSingleFieldValue' ) 
    DROP PROCEDURE USP_UpdateSingleFieldValue
GO

CREATE PROCEDURE USP_UpdateSingleFieldValue
	@tintMode AS TINYINT,
	@numUpdateRecordID NUMERIC,
	@numUpdateValueID NUMERIC=0,
	@vcText AS TEXT,
	@numDomainID AS NUMERIC= 0 
AS 
BEGIN
	 -- For 26 and 37
     DECLARE @Status AS NUMERIC(9,0)
     DECLARE @SiteID       AS NUMERIC(9,0)
     DECLARE @OrderStatus  AS NUMERIC(9,0)
	 DECLARE @BizdocStatus AS NUMERIC(9,0)  
	 DECLARE @numDomain AS NUMERIC(18,0)
	 SET @numDomain = @numDomainID 

	IF @tintMode=1
	BEGIN
		UPDATE  dbo.OpportunityBizDocsDetails
		SET     bitAmountCaptured = @numUpdateValueID
		WHERE   numBizDocsPaymentDetId = @numUpdateRecordID
	END
--	IF @tintMode = 2 
--		BEGIN
--			UPDATE  dbo.OpportunityMaster
--			SET     numStatus = @numUpdateValueID
--			WHERE   numOppId = @numUpdateRecordID
--		END
	IF @tintMode = 3
		BEGIN
			 UPDATE AdditionalContactsInformation SET txtNotes=ISNULL(CAST(txtNotes AS VARCHAR(8000)),'') + ' ' +  ISNULL(CAST(@vcText AS VARCHAR(8000)),'') WHERE numContactId=@numUpdateRecordID
		END
	IF @tintMode = 4
		BEGIN
			 UPDATE dbo.OpportunityBizDocsDetails SET numBillPaymentJournalID=@numUpdateValueID WHERE numBizDocsPaymentDetId=@numUpdateRecordID
		END
		
	IF @tintMode = 5
		BEGIN
			 UPDATE dbo.BizDocComission SET bitCommisionPaid=@numUpdateValueID WHERE numOppBizDocID=@numUpdateRecordID
		END
		
	IF @tintMode = 6
		BEGIN
			 UPDATE dbo.OpportunityRecurring SET numOppBizDocID=@numUpdateValueID WHERE numOppRecID=@numUpdateRecordID
		END
		
	IF @tintMode = 7
		BEGIN
			 UPDATE dbo.OpportunityRecurring SET dtRecurringDate=CAST( CAST( @vcText AS VARCHAR) AS DATETIME) WHERE numOppRecID=@numUpdateRecordID
		END
		
	IF @tintMode = 8
		BEGIN
			 UPDATE dbo.OpportunityRecurring SET fltBreakupPercentage=CAST( CAST( @vcText AS VARCHAR) AS float) WHERE numOppRecID=@numUpdateRecordID
		END
		/*Commented by chintan.. this is moved to USP_ShoppingCart SP*/
--	IF @tintMode = 9
--		BEGIN
--			UPDATE  dbo.OpportunityMaster
--			SET     txtComments = CAST(@vcText AS VARCHAR(1000))
--			WHERE   numOppId = @numUpdateRecordID
--		END    
--	IF @tintMode = 10
--		BEGIN
--			UPDATE  dbo.OpportunityMaster
--			SET     vcRefOrderNo = CAST(@vcText AS VARCHAR(100))
--			WHERE   numOppId = @numUpdateRecordID
--		END    
	IF @tintMode = 11
		BEGIN
			DELETE FROM dbo.LuceneItemsIndex WHERE numItemCode=@numUpdateRecordID
		END    
	IF @tintMode = 12
		BEGIN
			UPDATE dbo.Domain SET vcPortalName = CAST(@vcText AS VARCHAR(50))
			WHERE numDomainId=@numUpdateRecordID
		END 
	IF @tintMode = 13
		BEGIN
			UPDATE dbo.WareHouseItems SET numOnHand =numOnHand + CAST(CAST(@vcText AS VARCHAR(50)) AS numeric(18,0)),dtModified = GETDATE() 
			WHERE numWareHouseItemID = @numUpdateRecordID
			
			DECLARE @numItemCode NUMERIC(18)

			SELECT @numItemCode=numItemID from WareHouseItems where numWareHouseItemID = @numUpdateRecordID

			EXEC dbo.USP_ManageWareHouseItems_Tracking
				@numWareHouseItemID = @numUpdateRecordID, --  numeric(9, 0)
				@numReferenceID = @numItemCode, --  numeric(9, 0)
				@tintRefType = 1, --  tinyint
				@vcDescription = 'Inventory Adjustment', --  varchar(100)
				@numModifiedBy = @numUpdateValueID,
				@numDomainID = @numDomain
			 
		END
	IF @tintMode = 14
		BEGIN
			UPDATE dbo.Item SET monAverageCost=CAST(CAST(@vcText AS VARCHAR(50)) AS numeric(20,5))
			WHERE numItemCode = @numUpdateRecordID
		END
		IF @tintMode = 15
		BEGIN
			UPDATE dbo.EmailHistory  SET numListItemId = @numUpdateValueID  WHERE numEmailHstrID=@numUpdateRecordID
		END
		IF @tintMode = 16
		BEGIN
			UPDATE dbo.EmailHistory SET bitIsRead=@numUpdateValueID WHERE numEmailHstrID IN ( SELECT Id FROM dbo.SplitIDs(CAST(@vcText AS VARCHAR(200)) ,','))
		END
		IF @tintMode = 17
		BEGIN
			UPDATE CartItems SET numUserCntId = @numUpdateValueID WHERE vcCookieId =CAST(@vcText AS VARCHAR(MAX)) AND numUserCntId = 0
		END
		IF @tintMode = 18
		BEGIN
			UPDATE OpportunityBizDocs SET tintDeferred = 0 WHERE numOppBizDocsId=@numUpdateRecordID
		END
		IF @tintMode = 19
		BEGIN
			UPDATE dbo.OpportunityMaster SET vcPOppName=CAST(@vcText AS VARCHAR(100)) WHERE numOppId=@numUpdateRecordID
		END
	    IF @tintMode = 20
		BEGIN
			UPDATE UserMaster SET tintTabEvent=@numUpdateValueID WHERE numUserId=@numUpdateRecordID
		END
		IF @tintMode = 21
		BEGIN
			UPDATE OpportunityBizDocs SET numBizDocTempID = @numUpdateValueID WHERE numOppBizDocsId=@numUpdateRecordID
		END
		IF @tintMode = 22
		BEGIN
			UPDATE OpportunityBizDocItems SET tintTrackingStatus = 1 WHERE numOppBizDocItemID =@numUpdateRecordID
		END
		IF	 @tintMode = 23
		BEGIN
			UPDATE item SET bintModifiedDate = GETUTCDATE() WHERE numItemCode=@numUpdateRecordID			
		END
		IF @tintMode = 24
		BEGIN
			UPDATE dbo.OpportunityMaster SET numStatus=@numUpdateValueID WHERE numOppId in (select Items from dbo.split(@vcText,','))
		END
		
		IF @tintMode = 25
		BEGIN
			UPDATE dbo.WebAPIDetail SET vcEighthFldValue = @vcText WHERE WebApiId = @numUpdateRecordID AND numDomainId = @numDomainID
		END
		IF @tintMode = 26
		BEGIN
			
		     SELECT  @Status = ISNULL(numStatus ,0)  FROM dbo.OpportunityMaster WHERE numOppId = @numUpdateRecordID
		     
		     IF @Status = 0
		      BEGIN
				SELECT @SiteID  = CONVERT(NUMERIC(9,0),tintSource)   FROM	dbo.OpportunityMaster WHERE numOppId = @numUpdateRecordID
			    SELECT @OrderStatus = ISNULL(numOrderStatus,0) ,@BizdocStatus = ISNULL(numBizDocStatus,0)  FROM dbo.eCommercePaymentConfig WHERE numSiteId = @SiteID AND numPaymentMethodId = 31488
		       
			    UPDATE dbo.OpportunityMaster SET numStatus = ISNULL(@OrderStatus,0) WHERE numOppId = @numUpdateRecordID AND numDomainId = @numDomainID 
			    Update OpportunityBizDocs set monAmountPaid = ISNULL(monDealAmount,0),numBizDocStatus = @BizdocStatus  WHERE  numOppId = @numUpdateRecordID and bitAuthoritativeBizDocs =1
			  END   
		END
		IF @tintMode = 27
		BEGIN
			UPDATE OpportunityBizDocs SET numBizDocStatusOLD=numBizDocStatus,numBizDocStatus = @numUpdateValueID WHERE numOppBizDocsId=@numUpdateRecordID 
		END
	    IF @tintMode = 28
		BEGIN
			UPDATE General_Journal_Details SET bitReconcile=(CASE WHEN CAST(@vcText AS VARCHAR(5))='R' THEN 1 ELSE 0 END),
			bitCleared=(CASE WHEN CAST(@vcText AS VARCHAR(5))='C' THEN 1 ELSE 0 END),numReconcileID=0
			WHERE numDomainId=@numDomainID AND numTransactionId=@numUpdateRecordID 
		END
		
		IF @tintMode = 29
		BEGIN
			UPDATE dbo.ReturnHeader SET numRMATempID = @numUpdateValueID WHERE numReturnHeaderID = @numUpdateRecordID 
		END
		
		IF @tintMode = 30
		BEGIN
			UPDATE dbo.ReturnHeader SET numBizdocTempID = @numUpdateValueID WHERE numReturnHeaderID = @numUpdateRecordID 
		END
		
		IF @tintMode = 31
		BEGIN
			UPDATE dbo.OpportunityMaster SET numOppBizDocTempID = @numUpdateValueID WHERE numOppId = @numUpdateRecordID 
		END
		/*Commented by chintan.. this is moved to USP_ShoppingCart SP*/
--		IF @tintMode = 32
--		BEGIN
--			DECLARE @numBizDocId AS NUMERIC(9,0)
--			SELECT @numBizDocId = numBizDocId FROM eCommercePaymentConfig WHERE  numPaymentMethodId = @numUpdateValueId AND numDomainID = @numDomainID   
--			UPDATE dbo.OpportunityMaster SET numOppBizDocTempID = @numBizDocId WHERE numOppId = @numUpdateRecordID 
--		END
		
		IF @tintMode = 33
		BEGIN
			UPDATE dbo.ReturnHeader SET numBizdocTempID = @numUpdateValueID,numRMATempID = @numUpdateValueID WHERE numReturnHeaderID = @numUpdateRecordID 
		END
		
			
		IF @tintMode = 34
		BEGIN
			UPDATE dbo.WebAPIDetail SET vcFourteenthFldValue = @numUpdateValueID WHERE WebApiId = @numUpdateRecordID AND numDomainId = @numDomainID
		END
		
		IF @tintMode = 35
		BEGIN
			UPDATE OpportunityMasterTaxItems SET fltPercentage = CAST(CAST(@vcText AS VARCHAR(10)) AS FLOAT)
			WHERE numOppId = @numUpdateRecordID AND numTaxItemID = 0
		END
		
		IF @tintMode = 36
		BEGIN
			UPDATE dbo.Import_File_Master SET numProcessedCSVRowNumber = @numUpdateValueID
			WHERE intImportFileID = @numUpdateRecordID AND numDomainID = @numDomainID
		END
		IF @tintMode = 37
		BEGIN
		     SELECT  @Status = ISNULL(numStatus ,0)  FROM dbo.OpportunityMaster WHERE numOppId = @numUpdateRecordID
		     
		     IF @Status = 0
		      BEGIN
			    SELECT @SiteID  = CONVERT(NUMERIC(9,0),tintSource)   FROM	dbo.OpportunityMaster WHERE numOppId = @numUpdateRecordID
			    SELECT @OrderStatus = ISNULL(numFailedOrderStatus,0)    FROM dbo.eCommercePaymentConfig WHERE numSiteId = @SiteID AND numPaymentMethodId = 31488
		        
			    UPDATE dbo.OpportunityMaster SET numStatus = ISNULL(@OrderStatus,0) WHERE numOppId = @numUpdateRecordID AND numDomainId = @numDomainID 
			    
			  END   
		END
		IF @tintMode = 38
		BEGIN
			UPDATE [WebAPIDetail] SET    vcNinthFldValue = CAST(CAST(@vcText AS VARCHAR(30)) AS DATETIME)
			WHERE  [WebApiId] =  @numUpdateRecordID AND [numDomainId] = @numDomainId
		END
		
		IF @tintMode = 39
		BEGIN
			UPDATE [WebAPIDetail] SET    vcSixteenthFldValue = CAST(CAST(@vcText AS VARCHAR(30)) AS DATETIME)
			WHERE  [WebApiId] =  @numUpdateRecordID AND [numDomainId] = @numDomainId
		END

		IF @tintMode = 40
		BEGIN
			UPDATE dbo.WebAPIDetail SET bitEnableAPI = @numUpdateValueID WHERE WebApiId = @numUpdateRecordID AND numDomainId = @numDomainID
		END

		IF @tintMode = 41
		BEGIN
			UPDATE dbo.domain SET numShippingServiceItemID = @numUpdateValueID WHERE numDomainId = @numDomainID
		END
		
		IF @tintMode = 42
		BEGIN
			UPDATE dbo.domain SET numDiscountServiceItemID = @numUpdateValueID WHERE numDomainId = @numDomainID
		END
		
		IF @tintMode = 43
		BEGIN
			UPDATE dbo.Cases SET numStatus = @numUpdateValueID WHERE numCaseId = @numUpdateRecordID AND numDomainId = @numDomainID
		END
		
		IF @tintMode = 44
		BEGIN
			UPDATE dbo.UserMaster SET numDefaultClass = @numUpdateValueID WHERE numUserId = @numUpdateRecordID AND numDomainId = @numDomainID
		END

		IF @tintMode = 46
		BEGIN
			UPDATE dbo.Communication SET bitClosedFlag = @numUpdateValueID WHERE numCommId = @numUpdateRecordID AND numDomainId = @numDomainID
		END

		IF @tintMode = 47
		BEGIN
			UPDATE dbo.Domain SET bitCloneLocationWithItem = @numUpdateValueID WHERE numDomainId = @numDomainID
		END

		IF @tintMode = 48
		BEGIN
			UPDATE dbo.Domain SET vcMarketplaces = @vcText WHERE numDomainId = @numDomainID
		END

		IF @tintMode = 49
		BEGIN
			UPDATE dbo.OpportunityMaster SET intUsedShippingCompany=@numUpdateValueID,numShippingService=-1 WHERE numDomainId = @numDomainID AND numOppId=@numUpdateRecordID
		END
		
    SELECT @@ROWCOUNT
END
--Exec  USP_UpdateSingleFieldValue 26,52205,0,'',156
--UPDATE dbo.OpportunityMaster SET numStatus = 0 where numOppId = 52185
--SELECT numStatus FROM dbo.OpportunityMaster where numOppId = 52203
--SELECT * FROM dbo.OpportunityMaster 
--SELECT * FROM dbo.OpportunityBizDocs where numOppId = 52203
/* FOR SO:Adds Qty on allocation and deducts from onhand*/
/* FOR PO:Adds Qty on Order and updates average cost */
--exec USP_UpdatingInventoryonCloseDeal 5833      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_updatinginventoryonclosedeal')
DROP PROCEDURE usp_updatinginventoryonclosedeal
GO
CREATE PROCEDURE [dbo].[USP_UpdatingInventoryonCloseDeal]
@numOppID NUMERIC(18,0),
@numOppItemId NUMERIC(18,0),
@numUserCntID AS NUMERIC(18,0)        
AS
BEGIN TRY
   BEGIN TRANSACTION      
		
		declare @tintOpptype as tinyint        
		declare @itemcode as numeric                                    
		declare @numUnits as FLOAT                                      
		declare @numoppitemtCode as numeric(9)         
		declare @numWareHouseItemID as numeric(9) 
		declare @numToWarehouseItemID as numeric(9) --to be used with stock transfer
		declare @monAmount as DECIMAL(20,5) 
		declare @bitStockTransfer as bit
		declare @QtyShipped as FLOAT
		declare @QtyReceived as FLOAT
		DECLARE @fltExchangeRate AS FLOAT 
		DECLARE @bitWorkOrder AS BIT

		DECLARE @numDomain AS NUMERIC(18,0)
		SELECT @numDomain = OM.numDomainID FROM [dbo].[OpportunityMaster] AS OM WHERE [OM].[numOppId] = @numOppID
		select @tintOpptype=tintOppType,@bitStockTransfer=ISNULL(bitStockTransfer,0),@fltExchangeRate=(CASE WHEN ISNULL(fltExchangeRate,0)=0 THEN 1 ELSE fltExchangeRate END) from OpportunityMaster where numOppId=@numOppID
               
		 select top 1 @numoppitemtCode=numoppitemtCode,@itemcode=OI.numItemCode,@numUnits=numUnitHour,@qtyShipped=ISNULL([numQtyShipped],0),@qtyReceived=ISNULL([numUnitHourReceived],0),        
		 @numWareHouseItemID=ISNULL(numWarehouseItmsID,0),@numToWarehouseItemID=numToWarehouseItemID,@monAmount=monTotAmount * @fltExchangeRate,@bitWorkOrder=OI.bitWorkOrder
		 FROM OpportunityItems OI join Item I                                                
		 on OI.numItemCode=I.numItemCode   and numOppId=@numOppId                                          
		 where (charitemtype='P' OR 1=(CASE WHEN @tintOpptype=1 THEN 
							CASE WHEN ISNULL(I.bitKitParent,0) = 1 AND ISNULL(I.bitAssembly,0) = 1 THEN 0
								 WHEN ISNULL(I.bitKitParent,0) = 1 THEN 1
								 ELSE 0 END 
							ELSE 0 END)) and (bitDropShip=0 or bitDropShip is null)
		 AND I.[numDomainID] = @numDomain
		 AND (ISNULL(@numOppItemId,0) = 0 OR OI.numoppitemtCode=@numOppItemId)
		 ORDER by OI.numoppitemtCode                                       
		 while @numoppitemtCode>0                                        
		  begin   
	
	
				IF @bitStockTransfer = 1
				BEGIN
					--Make inventory changes as if sales order was placed , using OpportunityItems.numWarehouseItmsID
					EXEC usp_ManageInventory @itemcode,@numWareHouseItemID,@monAmount,1,@numUnits,@QtyShipped,@qtyReceived,0,1,@numOppID,@numoppitemtCode,@numUserCntID,@bitWorkOrder
					--Make inventory changes as if purchase order was placed , using OpportunityItems.numToWarehouseItemID
					EXEC usp_ManageInventory @itemcode,@numToWarehouseItemID,@monAmount,2,@numUnits,@QtyShipped,@qtyReceived,0,1,@numOppID,@numoppitemtCode,@numUserCntID,@bitWorkOrder
				END
				ELSE
				BEGIN
					EXEC usp_ManageInventory @itemcode,@numWareHouseItemID,@monAmount,@tintOpptype,@numUnits,@QtyShipped,@qtyReceived,0,1,@numOppID,@numoppitemtCode,@numUserCntID,@bitWorkOrder
				END
                                                                                                                                      
		   select top 1 @numoppitemtCode=numoppitemtCode,@itemcode=OI.numItemCode,@numUnits=numUnitHour,@qtyShipped=ISNULL([numQtyShipped],0),@qtyReceived=ISNULL([numUnitHourReceived],0),        
		 @numWareHouseItemID=ISNULL(numWarehouseItmsID,0),@numToWarehouseItemID=numToWarehouseItemID,@monAmount=monTotAmount * @fltExchangeRate,@bitWorkOrder=OI.bitWorkOrder
			from OpportunityItems OI join Item I         
		   on OI.numItemCode=I.numItemCode and numOppId=@numOppId               
		   where (charitemtype='P' OR 1=(CASE WHEN @tintOpptype=1 THEN 
							CASE WHEN ISNULL(I.bitKitParent,0) = 1 AND ISNULL(I.bitAssembly,0) = 1 THEN 0
								 WHEN ISNULL(I.bitKitParent,0) = 1 THEN 1
								 ELSE 0 END 
							ELSE 0 END)) and OI.numoppitemtCode>@numoppitemtCode and (bitDropShip=0 or bitDropShip is null)  
			AND I.[numDomainID] = @numDomain
			AND (ISNULL(@numOppItemId,0) = 0 OR OI.numoppitemtCode=@numOppItemId)
			order by OI.numoppitemtCode                                                
		   if @@rowcount=0 set @numoppitemtCode=0         
		  END

COMMIT
END TRY
BEGIN CATCH
  -- Whoops, there was an error
  IF @@TRANCOUNT > 0
     ROLLBACK

  -- Raise an error with the details of the exception
  DECLARE @ErrMsg nvarchar(4000), @ErrSeverity int
  SELECT @ErrMsg = ERROR_MESSAGE(),
         @ErrSeverity = ERROR_SEVERITY()

  RAISERROR(@ErrMsg, @ErrSeverity, 1)
END CATCH
        
GO

IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='GetRecordCountForSalesFulFillment')
DROP FUNCTION GetRecordCountForSalesFulFillment
GO
CREATE FUNCTION [dbo].[GetRecordCountForSalesFulFillment] 
 (
@numUserCntID as numeric(9)=null,                                                                        
@numDomainID as numeric(9)=null,                                                                        
@startDate as datetime,                                                                        
@endDate as datetime,                                                                    
@ClientTimeZoneOffset Int,
@numViewID AS NUMERIC(18,0)
)
RETURNS NVARCHAR(MAX)
AS
BEGIN
DECLARE @bitGroupByOrder AS BIT =0
	DECLARE @numBatchID AS NUMERIC(18,0)=0
	DECLARE @bitIncludeSearch AS BIT =1
	DECLARE @tintPrintBizDocViewMode AS INT =1
	DECLARE @tintPendingCloseFilter AS INT =1
	DECLARE @numShippingZone AS NUMERIC(18,0)=0
	DECLARE @numTotalRecords AS NUMERIC(18,0)=0
	DECLARE @vcOrderStauts VARCHAR(MAX) ='' 
	DECLARE @tintFilterBy TINYINT =0
	DECLARE @vcFilterValue VARCHAR(MAX)=''
	,@numPageIndex INT=1
	,@vcSortColumn VARCHAR(100)=''
	,@vcSortOrder VARCHAR(4)=''
	,@numWarehouseID VARCHAR(100)
	,@bitRemoveBO BIT =0


	IF @numViewID = 4 OR @numViewID = 5 OR @numViewID = 6
	BEGIN
		SET @bitGroupByOrder = 1
	END
	DECLARE @tintPackingViewMode AS INT =1
	DECLARE @tintPackingMode AS INT
	DECLARE @tintInvoicingType AS INT 
	SELECT
		@tintPackingMode=tintPackingMode
		,@tintInvoicingType=tintInvoicingType
	FROM
		MassSalesFulfillmentConfiguration
	WHERE
		numDomainID = @numDomainID 

	DECLARE @tintCommitAllocation TINYINT
	DECLARE @numShippingServiceItemID NUMERIC(18,0)
	DECLARE @numDefaultSalesShippingDoc NUMERIC(18,0)
	SELECT 
		@tintCommitAllocation=ISNULL(tintCommitAllocation,1) 
		,@numShippingServiceItemID=ISNULL(numShippingServiceItemID,0)
		,@numDefaultSalesShippingDoc=ISNULL(numDefaultSalesShippingDoc,0)
	FROM 
		Domain 
	WHERE 
		numDomainId=@numDomainID 

	DECLARE @vcCustomSearchValue AS VARCHAR(MAX) ='' 
	SET @vcCustomSearchValue = REPLACE(@vcCustomSearchValue,'OpportunityMaster.dtItemReceivedDate','(SELECT TOP 1 OMInner.dtItemReceivedDate FROM OpportunityMaster OMInner INNER JOIN OpportunityItems OIInner ON OMInner.numOppID=OIInner.numOppID WHERE OMInner.tintOppType=2 AND OMInner.tintOppStatus=1 AND OIInner.numItemCode=OpportunityItems.numItemCode ORDER BY OMInner.dtItemReceivedDate DESC)')

DECLARE  @TEMPMSRecords TABLE
	(
		numOppID NUMERIC(18,0)
		,numOppItemID NUMERIC(18,0)
		,numOppBizDocID NUMERIC(18,0)
	)
DECLARE  @TempWareHouse TABLE
(
	ID INT IDENTITY(1,1) PRIMARY KEY,
	numWareHouseId NUMERIC(18,0)
)

	DECLARE @vcSQL NVARCHAR(MAX)
	DECLARE @vcSQLFinal NVARCHAR(MAX)
	DECLARE @WarehouseCount AS INT = 0
	DECLARE @I AS INT = 1
INSERT INTO @TempWareHouse 	SELECT 
		[numWareHouseID]         
   FROM [dbo].[Warehouses] W 
   LEFT JOIN
		AddressDetails AD
	ON
        W.numAddressID = AD.numAddressID
   WHERE 
		W.numDomainID=@numDomainID

	SELECT @WarehouseCount = COUNT(*) FROM @TempWareHouse
	WHILE(@I<=@WarehouseCount)
	BEGIN
		SELECT @numWarehouseID = numWareHouseId FROM @TempWareHouse WHERE ID=@I
		
	SET @vcSQL = CONCAT('INSERT INTO @TEMPMSRecords
						(
							numOppID
							,numOppItemID
							,numOppBizDocID
						)
						SELECT
							OpportunityMaster.numOppId
							,',(CASE WHEN @bitGroupByOrder = 1 THEN '0' ELSE 'OpportunityItems.numoppitemtCode' END),'
							,',(CASE 
									WHEN @numViewID = 4 OR @numViewID = 6
									THEN 'OpportunityBizDocs.numOppBizDocsId'  
									ELSE '0' 
								END),'
						FROM
							OpportunityMaster
						INNER JOIN
							DivisionMaster
						ON
							OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
						INNER JOIN
							CompanyInfo
						ON
							DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
						INNER JOIN 
							OpportunityItems
						ON
							OpportunityMaster.numOppId = OpportunityItems.numOppId
						INNER JOIN
							Item
						ON
							OpportunityItems.numItemCode = Item.numItemCode
						LEFT JOIN
							WareHouseItems
						ON
							OpportunityItems.numWarehouseItmsID = WareHouseItems.numWareHouseItemID
						',
						(CASE 
							WHEN ISNULL(@numBatchID,0) > 0 
							THEN 
								' INNER JOIN 
										MassSalesFulfillmentBatchOrders
								ON
									OpportunityMaster.numOppId = MassSalesFulfillmentBatchOrders.numOppID
									AND (OpportunityItems.numOppItemtCode = MassSalesFulfillmentBatchOrders.numOppItemID OR ISNULL(MassSalesFulfillmentBatchOrders.numOppItemID,0) = 0)'
							ELSE 
								'' 
						END)	
						,
						(CASE 
							WHEN @numViewID = 4 
							THEN
								'INNER JOIN 
									OpportunityBizDocs
								ON
									OpportunityMaster.numOppId = OpportunityBizDocs.numOppId
									AND OpportunityBizDocs.numBizDocID=287'
							WHEN @numViewID = 6
							THEN
								CONCAT('INNER JOIN 
											OpportunityBizDocs
										ON
											OpportunityMaster.numOppId = OpportunityBizDocs.numOppId
											AND OpportunityBizDocs.numBizDocID=',(CASE @tintPrintBizDocViewMode WHEN 2 THEN 29397 WHEN 3 THEN 296 WHEN 4 THEN 287 ELSE 55206 END))
							ELSE ''

						END)
						,'
						WHERE
							OpportunityMaster.numDomainId=@numDomainID
							AND OpportunityMaster.tintOppType = 1
							AND OpportunityMaster.tintOppStatus = 1
							AND ISNULL(OpportunityMaster.tintshipped,0) = 0
							AND 1 = (CASE 
										WHEN @numViewID IN (1,2) AND ISNULL(@bitRemoveBO,0) = 1
										THEN (CASE 
												WHEN (Item.charItemType = ''p'' AND ISNULL(OpportunityItems.bitDropShip, 0) = 0 AND ISNULL(Item.bitAsset,0)=0)
												THEN 
													(CASE WHEN dbo.CheckOrderItemInventoryStatus(OpportunityItems.numItemCode,(CASE 
				WHEN @numViewID = 1
				THEN ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyPicked,0)
				WHEN @numViewID = 2 AND @tintPackingViewMode = 2
				THEN (CASE WHEN ISNULL(@tintPackingMode,1) = 2 THEN ISNULL(OpportunityItems.numQtyPicked,0) - ISNULL(OpportunityItems.numQtyShipped,0) ELSE ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyShipped,0) END)
				WHEN @numViewID = 2 AND @tintPackingViewMode = 3
				THEN (CASE WHEN ISNULL(@tintPackingMode,1) = 2 THEN ISNULL(OpportunityItems.numQtyPicked,0) ELSE ISNULL(OpportunityItems.numUnitHour,0) END) - ISNULL((SELECT 
																																											SUM(OpportunityBizDocItems.numUnitHour)
																																										FROM
																																											OpportunityBizDocs
																																										INNER JOIN
																																											OpportunityBizDocItems
																																										ON
																																											OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																																										WHERE
																																											OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
																																											AND OpportunityBizDocs.numBizDocId=@numDefaultSalesShippingDoc
					 																																					AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode),0)
				
				ELSE OpportunityItems.numUnitHour
			END),OpportunityItems.numoppitemtCode,OpportunityItems.numWarehouseItmsID,(CASE WHEN ISNULL(Item.bitKitParent,0) = 1 THEN 1 ELSE 0 END)) = 1 THEN 0 ELSE 1 END)
												ELSE 1
											END) 
										ELSE 1
									END)		
							AND 1 = (CASE WHEN LEN(ISNULL(@vcOrderStauts,'''')) > 0 THEN (CASE WHEN OpportunityMaster.numStatus ',(CASE WHEN ISNULL(@bitIncludeSearch,0) = 0 THEN 'NOT IN' ELSE 'IN' END),' (SELECT Id FROM dbo.SplitIDs(@vcOrderStauts,'','')) THEN 1 ELSE 0 END) ELSE 1 END)
							AND 1 = (CASE 
										WHEN LEN(ISNULL(@vcFilterValue,'''')) > 0
										THEN
											CASE 
												WHEN @tintFilterBy=1 --Item Classification
												THEN (CASE WHEN Item.numItemClassification ',(CASE WHEN ISNULL(@bitIncludeSearch,0) = 0 THEN 'NOT IN' ELSE 'IN' END),' (SELECT Id FROM dbo.SplitIDs(@vcFilterValue,'','')) THEN 1 ELSE 0 END)
												ELSE 1
											END
										ELSE 1
									END)
							AND 1 = (CASE
										WHEN @numViewID = 1 THEN (CASE WHEN ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyPicked,0) > 0 THEN 1 ELSE 0 END)
										WHEN @numViewID = 2 THEN (CASE 
																		WHEN @tintPackingViewMode = 1 
																		THEN (CASE 
																				WHEN ISNULL((SELECT COUNT(*) FROM OpportunityItems OIInner WHERE OIInner.numOppID=OpportunityMaster.numOppId AND OIInner.numItemCode=ISNULL(@numShippingServiceItemID,0)),0) = 0 
																				THEN 1 
																				ELSE 0 
																			END)
																		WHEN @tintPackingViewMode = 2
																		THEN (CASE 
																				WHEN ',(CASE WHEN ISNULL(@tintPackingMode,1) = 2 THEN 'ISNULL(OpportunityItems.numQtyPicked,0)' ELSE 'ISNULL(OpportunityItems.numUnitHour,0)' END),' - ISNULL(OpportunityItems.numQtyShipped,0) > 0
																				THEN 1 
																				ELSE 0 
																			END)
																		WHEN @tintPackingViewMode = 3
																		THEN (CASE 
																				WHEN ',(CASE WHEN ISNULL(@tintPackingMode,1) = 2 THEN 'ISNULL(OpportunityItems.numQtyPicked,0)' ELSE 'ISNULL(OpportunityItems.numUnitHour,0)' END),' - ISNULL((SELECT 
																																																													SUM(OpportunityBizDocItems.numUnitHour)
																																																												FROM
																																																													OpportunityBizDocs
																																																												INNER JOIN
																																																													OpportunityBizDocItems
																																																												ON
																																																													OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																																																												WHERE
																																																													OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
																																																													AND OpportunityBizDocs.numBizDocId=@numDefaultSalesShippingDoc
																																																													AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode),0) > 0
																				THEN 1 
																				ELSE 0 
																			END)','
																		ELSE 0
																	END)
										WHEN @numViewID = 3
										THEN (CASE 
												WHEN (CASE 
														WHEN ISNULL(@tintInvoicingType,0)=1 
														THEN ISNULL(OpportunityItems.numQtyShipped,0) 
														ELSE ISNULL(OpportunityItems.numUnitHour,0) 
													END) - ISNULL((SELECT 
																		SUM(OpportunityBizDocItems.numUnitHour)
																	FROM
																		OpportunityBizDocs
																	INNER JOIN
																		OpportunityBizDocItems
																	ON
																		OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																	WHERE
																		OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
																		AND OpportunityBizDocs.numBizDocId=287
																		AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode),0) > 0 
												THEN 1
												ELSE 0 
											END)
										WHEN @numViewID = 4 THEN 1
										WHEN @numViewID = 5 THEN 1
										WHEN @numViewID = 6 THEN 1
									END)
							AND 1 = (CASE
										WHEN @numViewID = 1 THEN (CASE WHEN ISNULL(WareHouseItems.numWareHouseItemID,0) > 0 AND ISNULL(OpportunityItems.bitDropship,0) = 0 THEN 1 ELSE 0 END)
										WHEN @numViewID = 2 AND @tintPackingViewMode <> 2 THEN (CASE WHEN ISNULL(WareHouseItems.numWareHouseItemID,0) > 0 AND ISNULL(OpportunityItems.bitDropship,0) = 0 THEN 1 ELSE 0 END)
										WHEN @numViewID = 2 AND @tintPackingViewMode = 2 THEN 1
										WHEN @numViewID = 3 THEN 1
										WHEN @numViewID = 4 THEN 1
										WHEN @numViewID = 5 THEN 1
										WHEN @numViewID = 6 THEN 1
										ELSE 0
									END)',
							(CASE 
								WHEN @numViewID = 5 AND ISNULL(@tintPendingCloseFilter,0) = 2
								THEN ' AND (SELECT 
												COUNT(*) 
											FROM 
											(
												SELECT
													OI.numoppitemtCode,
													ISNULL(OI.numUnitHour,0) AS OrderedQty,
													ISNULL(TempFulFilled.FulFilledQty,0) AS FulFilledQty
												FROM
													OpportunityItems OI
												INNER JOIN
													Item I
												ON
													OI.numItemCode = I.numItemCode
												OUTER APPLY
												(
													SELECT
														SUM(OpportunityBizDocItems.numUnitHour) AS FulFilledQty
													FROM
														OpportunityBizDocs
													INNER JOIN
														OpportunityBizDocItems 
													ON
														OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
													WHERE
														OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
														AND ISNULL(OpportunityBizDocs.numBizDocId,0) = 296
														AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
												) AS TempFulFilled
												WHERE
													OI.numOppID = OpportunityMaster.numOppID
													AND ISNULL(OI.numUnitHour,0) > 0
													AND ISNULL(OI.bitDropShip,0) = 0
											) X
											WHERE
												X.OrderedQty <> X.FulFilledQty) = 0
										AND (SELECT 
												COUNT(*) 
											FROM 
											(
												SELECT
													OI.numoppitemtCode,
													ISNULL(OI.numUnitHour,0) AS OrderedQty,
													ISNULL(TempInvoice.InvoicedQty,0) AS InvoicedQty
												FROM
													OpportunityItems OI
												INNER JOIN
													Item I
												ON
													OI.numItemCode = I.numItemCode
												OUTER APPLY
												(
													SELECT
														SUM(OpportunityBizDocItems.numUnitHour) AS InvoicedQty
													FROM
														OpportunityBizDocs
													INNER JOIN
														OpportunityBizDocItems 
													ON
														OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
													WHERE
														OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
														AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
														AND ISNULL(OpportunityBizDocs.bitAuthoritativeBizDocs,0) = 1
												) AS TempInvoice
												WHERE
													OI.numOppID = OpportunityMaster.numOppID
													AND ISNULL(OI.numUnitHour,0) > 0
											) X
											WHERE
												X.OrderedQty > X.InvoicedQty) = 0'
								ELSE '' 
							END)
							,(CASE WHEN @numViewID = 4 THEN 'AND 1 = (CASE WHEN ISNULL(OpportunityBizDocs.monDealAmount,0) - ISNULL(OpportunityBizDocs.monAmountPaid,0) > 0 THEN 1 ELSE 0 END)' ELSE '' END)
							,' AND 1 = (CASE WHEN @numViewID = 2 AND @tintPackingViewMode = 3 THEN (CASE WHEN ISNULL(Item.bitContainer,0)=1 THEN 0 ELSE 1 END) ELSE 1 END)
							AND 1 = (CASE WHEN ISNULL(@numWarehouseID,'''') <>'''' AND ISNULL(OpportunityItems.numWarehouseItmsID,0) > 0 THEN (CASE WHEN WareHouseItems.numWarehouseID IN (SELECT Items FROM Split(@numWarehouseID,'','') WHERE Items<>'''') THEN 1 ELSE 0 END) ELSE 1 END)	'
								,(CASE WHEN ISNULL(@numShippingZone,0) > 0 THEN ' AND 1 = (CASE 
																				WHEN ISNULL((SELECT TOP 1 numState FROM fn_getOPPAddressDetails(OpportunityMaster.numOppId,OpportunityMaster.numDomainID,2)),0) > 0 
																				THEN
																					CASE 
																						WHEN (SELECT COUNT(*) FROM dbo.State WHERE numDomainID=@numDomainId AND numStateID =  (SELECT TOP 1 numState FROM fn_getOPPAddressDetails(OpportunityMaster.numOppId,OpportunityMaster.numDomainID,2)) AND numShippingZone=@numShippingZone) > 0 
																						THEN 1
																						ELSE 0
																					END
																				ELSE 0 
																			END)' ELSE '' END),
								@vcCustomSearchValue,(CASE WHEN @bitGroupByOrder = 1 THEN CONCAT('GROUP BY OpportunityMaster.numOppId',(CASE WHEN @numViewID = 4 THEN ',OpportunityBizDocs.numOppBizDocsId,OpportunityBizDocs.monAmountPaid' WHEN @numViewID = 6 THEN ',OpportunityBizDocs.numOppBizDocsId'  ELSE '' END)) ELSE '' END));
	EXEC sp_executesql @vcSQL, N'@numDomainID NUMERIC(18,0), @vcOrderStauts VARCHAR(MAX), @vcFilterValue VARCHAR(MAX), @tintFilterBy TINYINT, @numViewID NUMERIC(18,0), @tintPackingViewMode TINYINT, @numShippingServiceItemID NUMERIC(18,0), @numShippingZone NUMERIC(18,0), @numDefaultSalesShippingDoc NUMERIC(18,0), @tintInvoicingType TINYINT, @tintPackingMode TINYINT, @bitRemoveBO BIT,@numWarehouseID VARCHAR(100)', @numDomainID, @vcOrderStauts, @vcFilterValue, @tintFilterBy, @numViewID, @tintPackingViewMode, @numShippingServiceItemID, @numShippingZone, @numDefaultSalesShippingDoc, @tintInvoicingType, @tintPackingMode, @bitRemoveBO,@numWarehouseID;

	If @numViewID = 2 AND @tintPackingViewMode = 3
	BEGIN
		SET @vcSQL = CONCAT('INSERT INTO @TEMPMSRecords
						(
							numOppID
							,numOppItemID
							,numOppBizDocID
						)
						SELECT
							OpportunityMaster.numOppId
							,',(CASE WHEN @bitGroupByOrder = 1 THEN '0' ELSE 'OpportunityItems.numoppitemtCode' END),'
							,OpportunityBizDocs.numOppBizDocsId
						FROM
							OpportunityMaster
						INNER JOIN
							DivisionMaster
						ON
							OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
						INNER JOIN
							CompanyInfo
						ON
							DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
						INNER JOIN 
							OpportunityBizDocs
						ON
							OpportunityMaster.numOppId = OpportunityBizDocs.numOppId
							AND OpportunityBizDocs.numBizDocID=@numDefaultSalesShippingDoc
						INNER JOIN 
							OpportunityItems
						ON
							OpportunityMaster.numOppId = OpportunityItems.numOppId
						INNER JOIN
							Item
						ON
							OpportunityItems.numItemCode = Item.numItemCode
						LEFT JOIN
							WareHouseItems
						ON
							OpportunityItems.numWarehouseItmsID = WareHouseItems.numWareHouseItemID
						
						',
						(CASE 
							WHEN ISNULL(@numBatchID,0) > 0 
							THEN 
								' INNER JOIN 
										MassSalesFulfillmentBatchOrders
								ON
									OpportunityMaster.numOppId = MassSalesFulfillmentBatchOrders.numOppID
									AND (OpportunityItems.numOppItemtCode = MassSalesFulfillmentBatchOrders.numOppItemID OR ISNULL(MassSalesFulfillmentBatchOrders.numOppItemID,0) = 0)'
							ELSE 
								'' 
						END)	
						,'
						WHERE
							OpportunityMaster.numDomainId=@numDomainID
							AND OpportunityMaster.tintOppType = 1
							AND OpportunityMaster.tintOppStatus = 1
							AND ISNULL(OpportunityMaster.tintshipped,0) = 0		
							AND 1 = (CASE WHEN LEN(ISNULL(@vcOrderStauts,'''')) > 0 THEN (CASE WHEN OpportunityMaster.numStatus ',(CASE WHEN ISNULL(@bitIncludeSearch,0) = 0 THEN 'NOT IN' ELSE 'IN' END),' (SELECT Id FROM dbo.SplitIDs(@vcOrderStauts,'','')) THEN 1 ELSE 0 END) ELSE 1 END)
							AND 1 = (CASE 
										WHEN LEN(ISNULL(@vcFilterValue,'''')) > 0
										THEN
											CASE 
												WHEN @tintFilterBy=1 --Item Classification
												THEN (CASE WHEN Item.numItemClassification ',(CASE WHEN ISNULL(@bitIncludeSearch,0) = 0 THEN 'NOT IN' ELSE 'IN' END),' (SELECT Id FROM dbo.SplitIDs(@vcFilterValue,'','')) THEN 1 ELSE 0 END)
												ELSE 1
											END
										ELSE 1
									END)
							AND 1 = (CASE 
										WHEN ISNULL((SELECT COUNT(*) FROM ShippingBox WHERE ISNULL(vcShippingLabelImage,'''') <> '''' AND ISNULL(vcTrackingNumber,'''') <> '''' AND numShippingReportId IN (SELECT numShippingReportId FROM ShippingReport WHERE ShippingReport.numOppID=OpportunityMaster.numOppId AND ShippingReport.numOppBizDocId=OpportunityBizDocs.numOppBizDocsId)),0) = 0 
										THEN 1 
										ELSE 0 
									END)
										
							AND 1 = (CASE WHEN ISNULL(WareHouseItems.numWareHouseItemID,0) > 0 AND ISNULL(OpportunityItems.bitDropship,0) = 0 THEN 1 ELSE 0 END)',
							' AND 1 = (CASE WHEN @numViewID = 2 AND @tintPackingViewMode = 3 THEN (CASE WHEN ISNULL(Item.bitContainer,0)=1 THEN 0 ELSE 1 END) ELSE 1 END)
							AND 1 = (CASE WHEN ISNULL(@numWarehouseID,'''') <>'''' AND ISNULL(OpportunityItems.numWarehouseItmsID,0) > 0 THEN (CASE WHEN WareHouseItems.numWarehouseID IN (SELECT Items FROM Split(@numWarehouseID,'','') WHERE Items<>'''') THEN 1 ELSE 0 END) ELSE 1 END)	'
								,(CASE WHEN ISNULL(@numShippingZone,0) > 0 THEN ' AND 1 = (CASE 
																				WHEN ISNULL((SELECT TOP 1 numState FROM fn_getOPPAddressDetails(OpportunityMaster.numOppId,OpportunityMaster.numDomainID,2)),0) > 0 
																				THEN
																					CASE 
																						WHEN (SELECT COUNT(*) FROM dbo.State WHERE numDomainID=@numDomainId AND numStateID =  (SELECT TOP 1 numState FROM fn_getOPPAddressDetails(OpportunityMaster.numOppId,OpportunityMaster.numDomainID,2)) AND numShippingZone=@numShippingZone) > 0 
																						THEN 1
																						ELSE 0
																					END
																				ELSE 0 
																			END)' ELSE '' END),
								@vcCustomSearchValue,(CASE WHEN @bitGroupByOrder = 1 THEN ' GROUP BY OpportunityMaster.numOppId,OpportunityBizDocs.numOppBizDocsId,OpportunityBizDocs.monAmountPaid' ELSE '' END));

		EXEC sp_executesql @vcSQL, N'@numDomainID NUMERIC(18,0), @vcOrderStauts VARCHAR(MAX), @vcFilterValue VARCHAR(MAX), @tintFilterBy TINYINT, @numViewID NUMERIC(18,0), @tintPackingViewMode TINYINT, @numShippingServiceItemID NUMERIC(18,0), @numShippingZone NUMERIC(18,0), @numDefaultSalesShippingDoc NUMERIC(18,0), @tintInvoicingType TINYINT, @tintPackingMode TINYINT, @bitRemoveBO BIT,@numWarehouseID NUMERIC(18,0)', @numDomainID, @vcOrderStauts, @vcFilterValue, @tintFilterBy, @numViewID, @tintPackingViewMode, @numShippingServiceItemID, @numShippingZone, @numDefaultSalesShippingDoc, @tintInvoicingType, @tintPackingMode, @bitRemoveBO,@numWarehouseID;
	END

	--PRINT @numDomainID 
	--PRINT @vcOrderStauts 
	--PRINT  @vcFilterValue 
	--PRINT @tintFilterBy 
	--PRINT  @numViewID 
	--PRINT  @tintPackingViewMode 
	--PRINT  @numShippingServiceItemID 
	--PRINT @numShippingZone 
	--PRINT  @numDefaultSalesShippingDoc 
	--PRINT  @tintInvoicingType 
	--PRINT  @tintPackingMode 
	--PRINT @bitRemoveBO 

	--SELECT * FROM @TEMPMSRecords
	SET @I = @I +1
	END
	
	SET @numTotalRecords = (SELECT COUNT(*) FROM @TEMPMSRecords) 
	RETURN @numTotalRecords

END

GO
/****** Object:  StoredProcedure [dbo].[USP_GetTimeAndExpense]    Script Date: 07/26/2008 16:18:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj    
-- SELECT TOP 1 * FROM [TimeAndExpense] ORDER BY [numCategoryHDRID] DESC 
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ApprovalProcess_Transaction')
DROP PROCEDURE USP_ApprovalProcess_Transaction
GO
CREATE PROCEDURE [dbo].[USP_ApprovalProcess_Transaction]
	@chrAction char(10)=null,
	@numDomainID AS NUMERIC(9) = 0,
    @numUserCntID AS NUMERIC(9) = 0,
    @numModuleId AS NUMERIC(9) = 0,
    @numRecordId AS NUMERIC(9) = 0,
    @numApprovedBy AS NUMERIC(9) = 0,
	@numApprovalComplete AS NUMERIC(9)=0,
	
	@vchoutput varchar(100)=null OUTPUT
AS
BEGIN 
	DECLARE @numRUserId AS NUMERIC
	DECLARE @num1stApproval AS NUMERIC
	DECLARE @num2ndApproval AS NUMERIC
	DECLARE @num3rdApproval AS NUMERIC
	DECLARE @num4thApproval AS NUMERIC
	DECLARE @num5thApproval AS NUMERIC

	IF(@chrAction='UT')--Time Expense Approval
	BEGIN
		UPDATE
			TimeAndExpense
		SET
			numApprovalComplete=@numApprovalComplete
		WHERE
			numCategoryHDRID=@numRecordId
		INSERT INTO Approval_transaction_log
			(numDomainID,numModuleId,numRecordId,numApprovedBy,dtmApprovedOn)
		VALUES
			(@numDomainID,@numModuleId,@numRecordId,@numApprovedBy,GETDATE())
	END

	IF(@chrAction='UTP')--Promotion Approval
	BEGIN
		UPDATE
			OpportunityMaster
		SET
			intPromotionApprovalStatus=@numApprovalComplete
		WHERE
			numOppId=@numRecordId
		IF(@numApprovalComplete=0)
		BEGIN
				DECLARE @DealStatus AS NUMERIC(18,0)  
				DECLARE @tintOppStatus AS tinyint
				DECLARE @tintOppType AS tinyint
				SET @DealStatus=(SELECT TOP 1 intChangePromotionStatus FROM OpportunityMaster WHERE numOppId=@numRecordId)    
				UPDATE OpportunityMaster SET tintOppStatus = @DealStatus WHERE numOppId=@numRecordId
				declare @tintShipped as tinyint 
				DECLARE @numDivisionID AS NUMERIC(18,0)  
				DECLARE @tintCommitAllocation TINYINT
				      
				select @tintOppStatus=tintOppStatus,@tintOppType=tintOppType,@tintShipped=tintShipped,@tintCommitAllocation=ISNULL(tintCommitAllocation,1) from OpportunityMaster INNER JOIN Domain ON OpportunityMaster.numDomainId=Domain.numDomainId where numOppID=@numRecordId              
				if @tintOppStatus=1 AND @tintCommitAllocation=1         
				begin         
					exec USP_UpdatingInventoryonCloseDeal @numRecordId,0,@numUserCntID
				end  
				declare @tintCRMType as numeric(9)        
				declare @AccountClosingDate as datetime        

				select @numDivisionID=numDivisionID,@AccountClosingDate=bintAccountClosingDate from  OpportunityMaster where numOppID=@numRecordId         
				select @tintCRMType=tintCRMType from divisionmaster where numDivisionID =@numDivisionID 

				--When deal is won                                         
				if @DealStatus = 1 
				begin           
					 if @AccountClosingDate is null                   
					  update OpportunityMaster set bintAccountClosingDate=getutcdate() where numOppID=@numRecordId              
				end         
				--When Deal is Lost
				else if @DealStatus = 2
				begin         
					 select @AccountClosingDate=bintAccountClosingDate from  OpportunityMaster where numOppID=@numRecordId         
					 if @AccountClosingDate is null                   
					  update OpportunityMaster set bintAccountClosingDate=getutcdate() where numOppID=@numRecordId
				end                    

				/*Added by chintan -  Biz usually automatically promotes the record from Lead/Prospect to Account When a Sales Opp/Order is made against it*/  
				-- Promote Lead to Prospect when Sales/Purchase Opp is created against it
				if @tintCRMType=0 AND @DealStatus = 0 --Lead & Open Opp
				begin        
					update divisionmaster set tintCRMType=1,bintLeadProm=getutcdate(),bintLeadPromBy=@numUserCntID,bintProsProm=getutcdate(),bintProsPromBy=@numUserCntID        
					where numDivisionID=@numDivisionID        
				end        
				-- Promote Lead to Account when Sales/Purchase Order is created against it
				ELSE if @tintCRMType=0 AND @DealStatus = 1 --Lead & Order
				begin        
					update divisionmaster set tintCRMType=2,bintLeadProm=getutcdate(),bintLeadPromBy=@numUserCntID,bintProsProm=getutcdate(),bintProsPromBy=@numUserCntID        
					where numDivisionID=@numDivisionID        
				end
				--Promote Prospect to Account
				else if @tintCRMType=1 AND @DealStatus = 1 
				begin        
					update divisionmaster set tintCRMType=2,bintProsProm=getutcdate(),bintProsPromBy=@numUserCntID        
					where numDivisionID=@numDivisionID        
				end
		END
		INSERT INTO Approval_transaction_log
			(numDomainID,numModuleId,numRecordId,numApprovedBy,dtmApprovedOn)
		VALUES
			(@numDomainID,@numModuleId,@numRecordId,@numApprovedBy,GETDATE())
	END

	IF(@chrAction='CF')
	BEGIN
		IF(@numRecordId>0)
		BEGIN
			SET @numRUserId=(SELECT TOP 1 numUserCntID FROM TimeAndExpense WHERE numCategoryHDRID=@numRecordId)
		END
		ELSE
		BEGIN
			SET @numRUserId=@numUserCntID
		END
		
		SELECT TOP 1 
			@num1stApproval=numLevel1Authority,
			@num2ndApproval=numLevel2Authority,
			@num3rdApproval=numLevel3Authority,
			@num4thApproval=numLevel4Authority,
			@num5thApproval=numLevel5Authority 
		FROM 
			ApprovalConfig 
		WHERE 
			numUserId=@numRUserId AND numModule=1
		
		IF(ISNULL(@num1stApproval,0)=0 and ISNULL(@num2ndApproval,0)=0 and ISNULL(@num3rdApproval,0)=0 and ISNULL(@num4thApproval,0)=0 and ISNULL(@num5thApproval,0)=0)
		BEGIN
			SET @vchoutput='INVALID'
		END
		ELSE
		BEGIN
			SET @vchoutput='VALID'
		END
	END

	IF(@chrAction='CH')
	BEGIN
		IF(@numRecordId>0)
		BEGIN
			SET @numRUserId=(SELECT TOP 1 numUserCntID FROM TimeAndExpense WHERE numCategoryHDRID=@numRecordId)
		END
		ELSE
		BEGIN
			SET @numRUserId=@numUserCntID
		END
		SELECT TOP 1 
			@num1stApproval=numLevel1Authority,
			@num2ndApproval=numLevel2Authority,
			@num3rdApproval=numLevel3Authority,
			@num4thApproval=numLevel4Authority,
			@num5thApproval=numLevel5Authority 
		FROM 
			ApprovalConfig 
		WHERE 
			numUserId=@numRUserId AND numModule=1
		
		IF(ISNULL(@num1stApproval,0)=0 and @numApprovalComplete=1)
		BEGIN
			SET @vchoutput='INVALID'
		END
		ELSE IF(ISNULL(@num2ndApproval,0)=0 and @numApprovalComplete=2)
		BEGIN
			SET @vchoutput='INVALID'
		END
		ELSE IF(ISNULL(@num3rdApproval,0)=0 and @numApprovalComplete=3)
		BEGIN
			SET @vchoutput='INVALID'
		END
		ELSE IF(ISNULL(@num4thApproval,0)=0 and @numApprovalComplete=4)
		BEGIN
			SET @vchoutput='INVALID'
		END
		ELSE IF(ISNULL(@num5thApproval,0)=0 and @numApprovalComplete=5)
		BEGIN
			SET @vchoutput='INVALID'
		END
		ELSE
		BEGIN
			SET @vchoutput='VALID'
		END
	END

	IF(@chrAction='CHP')--Check Promotion Approver Authority
	BEGIN
		IF(@numRecordId>0)
		BEGIN
			SET @numRUserId=(SELECT TOP 1 numCreatedby FROM OpportunityMaster WHERE numOppId=@numRecordId)
		END
		ELSE
		BEGIN
			SET @numRUserId=@numUserCntID
		END
		SELECT TOP 1 
			@num1stApproval=numLevel1Authority,
			@num2ndApproval=numLevel2Authority,
			@num3rdApproval=numLevel3Authority,
			@num4thApproval=numLevel4Authority,
			@num5thApproval=numLevel5Authority 
		FROM 
			ApprovalConfig 
		WHERE 
			numUserId=@numRUserId AND numModule=2
		
		IF(ISNULL(@num1stApproval,0)=0 and @numApprovalComplete=1)
		BEGIN
			SET @vchoutput='INVALID'
		END
		ELSE IF(ISNULL(@num2ndApproval,0)=0 and @numApprovalComplete=2)
		BEGIN
			SET @vchoutput='INVALID'
		END
		ELSE IF(ISNULL(@num3rdApproval,0)=0 and @numApprovalComplete=3)
		BEGIN
			SET @vchoutput='INVALID'
		END
		ELSE IF(ISNULL(@num4thApproval,0)=0 and @numApprovalComplete=4)
		BEGIN
			SET @vchoutput='INVALID'
		END
		ELSE IF(ISNULL(@num5thApproval,0)=0 and @numApprovalComplete=5)
		BEGIN
			SET @vchoutput='INVALID'
		END
		ELSE
		BEGIN
			SET @vchoutput='VALID'
		END
	END



END
GO

GO
/****** Object:  StoredProcedure [dbo].[Activity_Add]    Script Date: 07/26/2008 16:14:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*  
**  Inserts a new Activity added by a given Resource.  
**  
**  If no Resource is specified, then the Activity is related to  
**  the Unassigned Resource.  
*/  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ContractsLog')
DROP PROCEDURE USP_ContractsLog
GO
CREATE PROCEDURE [dbo].[USP_ContractsLog] 
@numContractId AS NUMERIC(18,0)=0,
@numDivisionId AS NUMERIC(18,0)=0,
@numDomainID AS NUMERIC(18,0)=0
AS 
BEGIN
	DECLARE @dynamicQuery AS NVARCHAR(MAX)=''
	SET @dynamicQuery = @dynamicQuery+ ' SELECT 
		C.intType,
		CASE WHEN C.intType=3 THEN ''Incidents'' WHEN C.intType=1 THEN ''Task'' ELSE '''' END AS vcType,
		CP.vcCaseNumber AS CaseNumber,
		CC.numReferenceId,
		CC.dtmCreatedOn,
		CC.numUsedTime,
		CC.numBalance,
		CC.numContractId,
		COM.numCommId,
		SP.numProjectId,
		dbo.fn_SecondsConversion(CAST(ISNULL(CC.numBalance,''0'') AS INT)) AS [timeLeft],
		CASE WHEN COM.numCommId>0 THEN COM.dtStartTime 
		WHEN SP.numTaskId>0 THEN (SELECT TOP 1 dtActionTime FROM StagePercentageDetailsTaskTimeLog where numTaskId=SP.numTaskId AND tintAction=1) ELSE NULL END AS dtStartTime ,
		CASE WHEN COM.numCommId>0 THEN COM.dtEndTime
		WHEN SP.numTaskId>0 THEN (SELECT TOP 1 dtActionTime FROM StagePercentageDetailsTaskTimeLog where numTaskId=SP.numTaskId AND tintAction=4) ELSE NULL END AS dtEndTime
	FROM 
		ContractsLog AS CC 
	LEFT JOIN 
		Contracts AS C ON CC.numContractId=C.numContractId
	LEFT JOIN 
		DivisionMaster AS D ON C.numDivisonId=D.numDivisionID
	LEFT JOIN
		Cases AS CP ON CC.numReferenceId=CP.numCaseId 
	LEFT JOIN 
		Communication AS COM ON CC.numReferenceId=COM.numCommId 
	LEFT JOIN 
		StagePercentageDetailsTask AS SP ON CC.numReferenceId=SP.numTaskId
	 WHERE C.numDomainId='+CAST(@numDomainID AS VARCHAR(200))+' '
	IF(@numDivisionId>0)
	BEGIN
	SET @dynamicQuery = @dynamicQuery+' AND CC.numDivisionID='+CAST(@numDivisionId AS VARCHAR(300))+' '
	END
	IF(@numContractId>0)
	BEGIN
	SET @dynamicQuery = @dynamicQuery+' AND CC.numContractId='+CAST(@numContractId AS VARCHAR(300))+' '
	END
	PRINT @dynamicQuery
	EXEC(@dynamicQuery)
END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetOrderSelectedItemDetail')
DROP PROCEDURE USP_GetOrderSelectedItemDetail
GO
CREATE PROCEDURE [dbo].[USP_GetOrderSelectedItemDetail]
	@numDomainID NUMERIC(18,0),
	@numDivisionID NUMERIC(18,0),
	@tintOppType AS TINYINT,
	@numItemCode NUMERIC(18,0),
	@numUOMID NUMERIC(18,0),
	@numWarehouseItemID AS NUMERIC(18,0),
	@numQty INT,
	@monPrice FLOAT,
	@numOppID NUMERIC(18,0),
	@numOppItemID NUMERIC(18,0),
	@vcSelectedKitChildItems VARCHAR(MAX),
	@numWarehouseID AS NUMERIC(18,0),
	@vcCoupon AS VARCHAR(200),
	@numCurrencyID NUMERIC(18,0)
AS
BEGIN
	DECLARE @vcItemName AS VARCHAR(500)
	DECLARE @bitDropship AS BIT
	DECLARE @bitMatrix AS BIT
	DECLARE @chrItemType AS CHAR(1)
	DECLARE @numItemGroup AS NUMERIC(18,0)
	DECLARE @numBaseUnit AS NUMERIC(18,0)
	DECLARE @numSaleUnit AS NUMERIC(18,0)
	DECLARE @fltUOMConversionFactor AS FLOAT
	DECLARE @numPurchaseUnit AS NUMERIC(18,0)
	DECLARE @bitAssemblyOrKit BIT
	DECLARE @bitAssembly AS BIT
	DECLARE @bitKit AS BIT
	DECLARE @bitHasChildKits BIT
	DECLARE @bitSOWorkOrder BIT
	DECLARE @bitCalAmtBasedonDepItems AS BIT
	DECLARE @ItemDesc AS VARCHAR(1000)
	DECLARE @numItemClassification AS NUMERIC(18,0)

	DECLARE @monListPrice AS DECIMAL(20,5) = 0.0000
	DECLARE @monVendorCost AS DECIMAL(20,5) = 0.0000
	DECLARE @tintPricingBasedOn AS TINYINT = 0.0000

	DECLARE @tintRuleType AS TINYINT
	DECLARE @monPriceLevelPrice AS DECIMAL(20,5) = 0.0000
	DECLARE @monPriceFinalPrice AS DECIMAL(20,5) = 0.0000
	DECLARE @monPriceRulePrice AS DECIMAL(20,5) = 0.0000
	DECLARE @monPriceRuleFinalPrice AS DECIMAL(20,5) = 0.0000
	DECLARE @monLastPrice AS DECIMAL(20,5) = 0.0000
	DECLARE @dtLastOrderDate VARCHAR(200) = ''

	DECLARE @tintDisountType TINYINT
	DECLARE @decDiscount AS FLOAT

	DECLARE @numVendorID AS NUMERIC(18,0)
	DECLARE @vcMinOrderQty AS VARCHAR(5) = '-'
	DECLARE @vcVendorNotes AS VARCHAR(300) = ''
	DECLARE @numRelationship NUMERIC(18,0)
	DECLARE @numProfile NUMERIC(18,0)
	DECLARE @tintPriceLevel AS TINYINT
	DECLARE @tintDefaultSalesPricing AS TINYINT
	DECLARE @tintPriceBookDiscount AS TINYINT
	DECLARE @tintKitAssemblyPriceBasedOn TINYINT
	DECLARE @numBaseCurrency NUMERIC(18,0)
	DECLARE @fltExchangeRate FLOAT

	IF ISNULL(@numDivisionID,0) > 0
	BEGIN
		SELECT @vcMinOrderQty=ISNULL(CAST(intMinQty AS VARCHAR),'-'),@vcVendorNotes=ISNULL(vcNotes,'') FROM Vendor WHERE numVendorID=@numDivisionID AND numItemCode=@numItemCode
	END

	SELECT 
		@tintDefaultSalesPricing = numDefaultSalesPricing
		,@tintPriceBookDiscount=ISNULL(tintPriceBookDiscount,0) 
		,@numBaseCurrency=ISNULL(numCurrencyID,0)
	FROM 
		Domain 
	WHERE 
		numDomainId=@numDomainID

	IF @numBaseCurrency = @numCurrencyID
	BEGIN
		SET @fltExchangeRate = 1
	END
	ELSE
	BEGIN
		SET @fltExchangeRate = ISNULL((SELECT fltExchangeRate FROM Currency WHERE numDomainID=@numDomainID AND numCurrencyID=@numCurrencyID),1)

		IF ISNULL(@fltExchangeRate,0) = 0
		BEGIN
			SET @fltExchangeRate = 1
		END
	END

	IF ISNULL(@numWarehouseID,0) > 0
	BEGIN
		SELECT TOP 1 @numWarehouseItemID=numWareHouseItemID FROM WareHouseItems WHERE numItemID=@numItemCode AND numWareHouseID=@numWarehouseID ORDER BY numWareHouseItemID ASC
	END

	-- GET ORGANIZATION DETAL
	SELECT 
		@numRelationship=numCompanyType,
		@numProfile=vcProfile,
		@tintPriceLevel = ISNULL(D.tintPriceLevel,0)
	FROM 
		DivisionMaster D                  
	JOIN 
		CompanyInfo C 
	ON 
		C.numCompanyId=D.numCompanyID                  
	WHERE 
		numDivisionID =@numDivisionID       

	-- GET ITEM DETAILS
	SELECT
		@vcItemName = vcItemName,
		@bitDropship = bitAllowDropShip,
		@ItemDesc = Item.txtItemDesc,
		@bitMatrix = ISNULL(Item.bitMatrix,0),
		@chrItemType = charItemType,
		@numItemGroup = numItemGroup,
		@bitAssembly = ISNULL(bitAssembly,0),
		@bitSOWorkOrder = ISNULL(bitSOWorkOrder,0),
		@numBaseUnit = ISNULL(numBaseUnit,0),
		@numSaleUnit = (CASE WHEN ISNULL(@numUOMID,0) > 0 THEN @numUOMID ELSE ISNULL(numSaleUnit,0) END),
		@numPurchaseUnit = (CASE WHEN ISNULL(@numUOMID,0) > 0 THEN @numUOMID ELSE ISNULL(numPurchaseUnit,0) END),
		@numItemClassification = numItemClassification,
		@bitAssemblyOrKit = (CASE WHEN ISNULL(bitKitParent,0)=1 OR ISNULL(bitAssembly,0)=1 THEN 1 ELSE 0 END),
		@monListPrice = (CASE 
							WHEN EXISTS (SELECT ID FROM ItemCurrencyPrice WHERE numDomainID=@numDomainID AND numItemCode=@numItemCode AND numCurrencyID=ISNULL(@numCurrencyID,0))  
							THEN ISNULL((SELECT monListPrice FROM ItemCurrencyPrice WHERE numDomainID=@numDomainID AND numItemCode=@numItemCode AND numCurrencyID=ISNULL(@numCurrencyID,0)),0)
							ELSE (CASE WHEN @fltExchangeRate <> 1 THEN CAST((ISNULL(Item.monListPrice,0) / @fltExchangeRate) AS INT) ELSE ISNULL(Item.monListPrice,0) END)
						END),
		@monVendorCost = ISNULL((SELECT (CASE WHEN @fltExchangeRate <> 1 THEN CAST((ISNULL(monCost,0) / @fltExchangeRate) AS INT) ELSE ISNULL(monCost,0) END) * dbo.fn_UOMConversion(Item.numBaseUnit,Item.numItemCode,Item.numDomainID,Item.numPurchaseUnit) FROM Vendor WHERE numVendorID=Item.numVendorID AND Vendor.numItemCode=Item.numItemCode),0),
		@numVendorID = ISNULL(Item.numVendorID,0),
		@numWarehouseItemID = (CASE WHEN ISNULL(@numWarehouseItemID,0) > 0 THEN @numWarehouseItemID ELSE WareHouseItems.numWareHouseItemID END),
		@bitCalAmtBasedonDepItems = ISNULL(bitCalAmtBasedonDepItems,0),
		@tintKitAssemblyPriceBasedOn=ISNULL(tintKitAssemblyPriceBasedOn,1),
		@bitKit=ISNULL(bitKitParent,0),
		@bitHasChildKits = (CASE 
								WHEN ISNULL(bitKitParent,0) = 1 
								THEN 
									(CASE 
										WHEN EXISTS (SELECT ID.numItemDetailID FROM ItemDetails ID INNER JOIN Item IInner ON ID.numChildItemID=IInner.numItemCode WHERE ID.numItemKitID=Item.numItemCode AND ISNULL(IInner.bitKitParent,0)=1)
										THEN 1
										ELSE 0
									END)
								ELSE 0 
							END)
	FROM
		Item
	OUTER APPLY
	(
		SELECT TOP 1 numWareHouseItemID,monWListPrice FROM WareHouseItems WHERE numItemID=@numItemCode AND (numWareHouseItemID = @numWarehouseItemID OR ISNULL(@numWarehouseItemID,0) = 0)
	) AS WareHouseItems
	WHERE
		Item.numItemCode = @numItemCode

	IF ISNULL(@numDivisionID,0) > 0 AND EXISTS (SELECT * FROM Vendor WHERE numItemCode=@numItemCode AND numVendorID=@numDivisionID)
	BEGIN
		SELECT @monVendorCost= (CASE WHEN @fltExchangeRate <> 1 THEN CAST((ISNULL(monCost,0) / @fltExchangeRate) AS INT) ELSE ISNULL(monCost,0) END) FROM Vendor WHERE numVendorID=@numDivisionID AND numItemCode=@numItemCode
	END


	IF @chrItemType = 'P' AND ISNULL(@numWarehouseItemID,0) = 0
	BEGIN
		RAISERROR('WAREHOUSE_DOES_NOT_EXISTS',16,1)
	END

	SET @fltUOMConversionFactor = dbo.fn_UOMConversion((CASE WHEN  @tintOppType = 1 THEN @numSaleUnit ELSE @numPurchaseUnit END),@numItemCode,@numDomainId,@numBaseUnit)

	IF @tintOppType = 1
	BEGIN
		/*Use Calculated Price When item is Assembly/Kit,Calculate price based on sum of dependent items.*/
		IF @bitCalAmtBasedonDepItems = 1 
		BEGIN
			PRINT 'IN DEPENDED ITEM'

			DECLARE @TEMPPrice TABLE
			(
				bitSuccess BIT
				,monPrice DECIMAL(20,5)
			)
			INSERT INTO @TEMPPrice
			(
				bitSuccess
				,monPrice
			)
			SELECT
				bitSuccess
				,monMSRPPrice
			FROM
				dbo.fn_GetKitAssemblyCalculatedPrice(@numDomainID,@numDivisionID,@numItemCode,@numQty,@numWarehouseItemID,@tintKitAssemblyPriceBasedOn,@numOppID,@numOppItemID,@vcSelectedKitChildItems,@numCurrencyID,@fltExchangeRate)

			IF (SELECT bitSuccess FROM @TEMPPrice) = 1
			BEGIN
				SET @monListPrice = (SELECT monPrice FROM @TEMPPrice)
			END
			ELSE
			BEGIN
				RAISERROR('SELECTED_WAREHOUSE_IS_NOT_AVAILABLE_FOR_ALL_CHILD_ITEMS',16,1)
			END
		END

		IF @tintDefaultSalesPricing = 1 AND (SELECT 
												COUNT(*) 
											FROM 
												PricingTable 
											WHERE 
												numItemCode=@numItemCode 
												AND ISNULL(numPriceRuleID,0) = 0
												AND 1 = (CASE 
													WHEN EXISTS (SELECT * FROM PricingTable WHERE numItemCode=@numItemCode AND ISNULL(numPriceRuleID,0) = 0 AND numCurrencyID=@numCurrencyID)
													THEN (CASE WHEN ISNULL(numCurrencyID,0) = @numCurrencyID THEN 1 ELSE 0 END)
													ELSE (CASE WHEN ISNULL(numCurrencyID,0) = 0 THEN 1 ELSE 0 END)
												END)) > 0 -- PRICE LEVEL
		BEGIN
			SELECT
				@tintDisountType = tintDiscountType,
				@decDiscount = ISNULL(decDiscount,0),
				@tintRuleType = tintRuleType,
				@monPriceLevelPrice = (CASE tintRuleType WHEN 1 THEN @monListPrice WHEN 2 THEN @monVendorCost ELSE ISNULL(decDiscount,0) END),
				@monPriceFinalPrice = (CASE tintRuleType
										WHEN 1 -- Deduct From List Price
										THEN
											(CASE tintDiscountType 
												WHEN 1 -- PERCENT
												THEN (CASE WHEN @monListPrice > 0 THEN @monListPrice - (@monListPrice * (ISNULL(decDiscount,0)/100)) ELSE 0 END)
												WHEN 2 -- FLAT AMOUNT
												THEN (CASE WHEN @monListPrice > 0 THEN (CASE WHEN (@monListPrice * @numQty * @fltUOMConversionFactor) - (CASE WHEN @numCurrencyID <> numCurrencyID THEN CAST((ISNULL(decDiscount,0) / @fltExchangeRate) AS INT) ELSE ISNULL(decDiscount,0) END) > 0 THEN ((@monListPrice * @numQty * @fltUOMConversionFactor) - (CASE WHEN @numCurrencyID <> numCurrencyID THEN CAST((ISNULL(decDiscount,0) / @fltExchangeRate) AS INT) ELSE ISNULL(decDiscount,0) END)) / (@numQty * @fltUOMConversionFactor) ELSE 0 END) ELSE 0 END)
												WHEN 3 -- NAMED PRICE
												THEN (CASE WHEN @numCurrencyID <> numCurrencyID THEN CAST((ISNULL(decDiscount,0) / @fltExchangeRate) AS INT) ELSE ISNULL(decDiscount,0) END)
											END)
										WHEN 2 -- Add to primary vendor cost
										THEN
											(CASE tintDiscountType 
												WHEN 1  -- PERCENT
												THEN @monVendorCost + (@monVendorCost * (ISNULL(decDiscount,0)/100))  * @fltUOMConversionFactor
												WHEN 2 -- FLAT AMOUNT
												THEN (CASE WHEN (@monVendorCost * @numQty * @fltUOMConversionFactor) + (CASE WHEN @numCurrencyID <> numCurrencyID THEN CAST((ISNULL(decDiscount,0) / @fltExchangeRate) AS INT) ELSE ISNULL(decDiscount,0) END) > 0 THEN ((@monVendorCost * @numQty * @fltUOMConversionFactor) + (CASE WHEN @numCurrencyID <> numCurrencyID THEN CAST((ISNULL(decDiscount,0) / @fltExchangeRate) AS INT) ELSE ISNULL(decDiscount,0) END)) / (@numQty * @fltUOMConversionFactor)  ELSE 0 END)
												WHEN 3 -- NAMED PRICE
												THEN (CASE WHEN @numCurrencyID <> numCurrencyID THEN CAST((ISNULL(decDiscount,0) / @fltExchangeRate) AS INT) ELSE ISNULL(decDiscount,0) END)
											END)
										WHEN 3 -- Named price
										THEN
											(CASE WHEN @numCurrencyID <> numCurrencyID THEN CAST((ISNULL(decDiscount,0) / @fltExchangeRate) AS INT) ELSE ISNULL(decDiscount,0) END)
										END
									) 
			FROM
			(
				SELECT 
					ROW_NUMBER() OVER(PARTITION BY PricingTable.numItemCode ORDER BY numPricingID) Id,
					*
				FROM
					PricingTable
				WHERE 
					PricingTable.numItemCode = @numItemCode
					AND ISNULL(PricingTable.numPriceRuleID,0) = 0
					AND 1 = (CASE 
								WHEN EXISTS (SELECT * FROM PricingTable WHERE numItemCode=@numItemCode AND ISNULL(numPriceRuleID,0) = 0 AND numCurrencyID=@numCurrencyID)
								THEN (CASE WHEN ISNULL(numCurrencyID,0) = @numCurrencyID THEN 1 ELSE 0 END)
								ELSE (CASE WHEN ISNULL(numCurrencyID,0) = 0 THEN 1 ELSE 0 END)
							END)
			) TEMP
			WHERE
				numItemCode=@numItemCode 
				AND ISNULL(numPriceRuleID,0) = 0
				AND ((tintRuleType = 3 AND Id=@tintPriceLevel) OR (tintRuleType <> 3 AND @numQty * @fltUOMConversionFactor >= intFromQty AND @numQty * @fltUOMConversionFactor <= intToQty))
		END
		ELSE IF @tintDefaultSalesPricing = 2 AND (SELECT COUNT(*) FROM PriceBookRules WHERE numDomainID=@numDomainID AND tintRuleFor=@tintOppType) > 0 -- PRICE RULE
		BEGIN
			DECLARE @numPriceRuleID NUMERIC(18,0)
			DECLARE @tintPriceRuleType AS TINYINT
			DECLARE @tintPricingMethod TINYINT
			DECLARE @tintPriceBookDiscountType TINYINT
			DECLARE @decPriceBookDiscount FLOAT
			DECLARE @intQntyItems INT
			DECLARE @decMaxDedPerAmt FLOAT

			SELECT 
				TOP 1
				@numPriceRuleID = numPricRuleID,
				@tintPricingMethod = tintPricingMethod,
				@tintPriceRuleType = p.tintRuleType,
				@tintPriceBookDiscountType = P.[tintDiscountType],
				@decPriceBookDiscount = P.[decDiscount],
				@intQntyItems = P.[intQntyItems],
				@decMaxDedPerAmt = P.[decMaxDedPerAmt]
			FROM   
				PriceBookRules P 
			LEFT JOIN 
				PriceBookRuleDTL PDTL 
			ON 
				P.numPricRuleID = PDTL.numRuleID
			LEFT JOIN 
				PriceBookRuleItems PBI 
			ON 
				P.numPricRuleID = PBI.numRuleID
			LEFT JOIN 
				[PriceBookPriorities] PP 
			ON 
				PP.[Step2Value] = P.[tintStep2] 
				AND PP.[Step3Value] = P.[tintStep3]
			WHERE  
				P.numDomainID=@numDomainID 
				AND tintRuleFor=@tintOppType
				AND (
						((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 1 AND PDTL.numValue = @numDivisionID)) -- Priority 1
						OR ((tintStep2 = 2 AND PBI.numValue = @numItemClassification) AND (tintStep3= 1 AND PDTL.numValue = @numDivisionID)) -- Priority 2
						OR ((tintStep2 = 3) AND (tintStep3 = 1 AND PDTL.numValue = @numDivisionID)) -- Priority 3

						OR ((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 4
						OR ((tintStep2 = 2 AND PBI.numValue = @numItemClassification) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 5
						OR ((tintStep2 = 3) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 6

						OR ((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 3)) -- Priority 7
						OR ((tintStep2 = 2 AND PBI.numValue = @numItemClassification) AND (tintStep3 = 3)) -- Priority 8
						OR ((tintStep2 = 3) and (tintStep3 = 3)) -- Priority 9
					)
			ORDER BY 
				PP.Priority ASC


			IF ISNULL(@numPriceRuleID,0) > 0
			BEGIN
				IF @tintPricingMethod = 1 -- BASED ON PRICE TABLE
				BEGIN
					SELECT
						@tintDisountType = tintDiscountType,
						@decDiscount = (CASE WHEN tintDiscountType=3 THEN 0 ELSE ISNULL(decDiscount,0) END),
						@tintRuleType = tintRuleType,
						@monPriceRulePrice = (CASE tintRuleType WHEN 1 THEN @monListPrice WHEN 2 THEN @monVendorCost END) ,
						@monPriceRuleFinalPrice = (CASE tintRuleType
												WHEN 1 -- Deduct From List Price
												THEN
													(CASE tintDiscountType 
														WHEN 1 -- PERCENT
														THEN (CASE WHEN @monListPrice > 0 THEN @monListPrice - (@monListPrice * (ISNULL(decDiscount,0)/100)) ELSE 0 END)
														WHEN 2 -- FLAT AMOUNT
														THEN (CASE WHEN @monListPrice > 0 THEN (CASE WHEN (@monListPrice * @numQty * @fltUOMConversionFactor) - (CASE WHEN @numCurrencyID <> numCurrencyID THEN CAST((ISNULL(decDiscount,0) / @fltExchangeRate) AS INT) ELSE ISNULL(decDiscount,0) END) > 0 THEN ((@monListPrice * @numQty * @fltUOMConversionFactor) - (CASE WHEN @numCurrencyID <> numCurrencyID THEN CAST((ISNULL(decDiscount,0) / @fltExchangeRate) AS INT) ELSE ISNULL(decDiscount,0) END)) / (@numQty * @fltUOMConversionFactor) ELSE 0 END) ELSE 0 END)
														WHEN 3 -- NAMED PRICE
														THEN (CASE WHEN @numCurrencyID <> numCurrencyID THEN CAST((ISNULL(decDiscount,0) / @fltExchangeRate) AS INT) ELSE ISNULL(decDiscount,0) END)
													END)
												WHEN 2 -- Add to primary vendor cost
												THEN
													(CASE tintDiscountType 
														WHEN 1  -- PERCENT
														THEN @monVendorCost + (@monVendorCost * (ISNULL(decDiscount,0)/100))
														WHEN 2 -- FLAT AMOUNT
														THEN (CASE WHEN (@monVendorCost * @numQty * @fltUOMConversionFactor) + (CASE WHEN @numCurrencyID <> numCurrencyID THEN CAST((ISNULL(decDiscount,0) / @fltExchangeRate) AS INT) ELSE ISNULL(decDiscount,0) END) > 0 THEN ((@monVendorCost * @numQty * @fltUOMConversionFactor) + (CASE WHEN @numCurrencyID <> numCurrencyID THEN CAST((ISNULL(decDiscount,0) / @fltExchangeRate) AS INT) ELSE ISNULL(decDiscount,0) END)) / (@numQty * @fltUOMConversionFactor)  ELSE 0 END)
														WHEN 3 -- NAMED PRICE
														THEN (CASE WHEN @numCurrencyID <> numCurrencyID THEN CAST((ISNULL(decDiscount,0) / @fltExchangeRate) AS INT) ELSE ISNULL(decDiscount,0) END)
													END)
												WHEN 3 -- Named price
												THEN
													(CASE 
														WHEN ISNULL(@tintPriceLevel,0) > 0
														THEN
															(SELECT 
																 (CASE WHEN @numCurrencyID <> numCurrencyID THEN CAST((ISNULL(decDiscount,0) / @fltExchangeRate) AS INT) ELSE ISNULL(decDiscount,0) END)
															FROM
															(
																SELECT 
																	ROW_NUMBER() OVER(PARTITION BY PricingTable.numItemCode ORDER BY numPricingID) Id,
																	decDiscount
																FROM 
																	PricingTable 
																WHERE 
																	PricingTable.numItemCode = @numItemCode 
																	AND tintRuleType = 3 
															) TEMP
															WHERE
																Id = @tintPriceLevel)
														ELSE (CASE WHEN @numCurrencyID <> numCurrencyID THEN CAST((ISNULL(decDiscount,0) / @fltExchangeRate) AS INT) ELSE ISNULL(decDiscount,0) END)
													END)
												END
											) 
					FROM
						PricingTable
					WHERE
						numPriceRuleID=@numPriceRuleID 
						AND 1 = (CASE 
									WHEN EXISTS (SELECT * FROM PricingTable WHERE numItemCode=@numItemCode AND ISNULL(numPriceRuleID,0) = 0 AND numCurrencyID=@numCurrencyID)
									THEN (CASE WHEN ISNULL(numCurrencyID,0) = @numCurrencyID THEN 1 ELSE 0 END)
									ELSE (CASE WHEN ISNULL(numCurrencyID,0) = 0 THEN 1 ELSE 0 END)
								END)
						AND @numQty * @fltUOMConversionFactor >= intFromQty 
						AND @numQty * @fltUOMConversionFactor <= intToQty
				END
				ELSE IF @tintPricingMethod = 2 -- BASED ON RULE
				BEGIN
					SET @tintRuleType = @tintPriceRuleType
					SET @monPriceRulePrice = (CASE @tintPriceRuleType WHEN 1 THEN @monListPrice WHEN 2 THEN @monVendorCost END) 

					IF (@tintPriceBookDiscountType = 1 ) -- Percentage 
					BEGIN
						SET @tintDisountType = @tintPriceBookDiscountType
						SET @decDiscount = @decPriceBookDiscount * (@numQty/@intQntyItems);
						SET @decDiscount = @decPriceBookDiscount * (CASE WHEN @tintPriceRuleType = 2 THEN @monVendorCost ELSE @monListPrice END) /100;
						SET @decMaxDedPerAmt = @decMaxDedPerAmt * (CASE WHEN @tintPriceRuleType = 2 THEN @monVendorCost ELSE @monListPrice END) /100;
						
						IF (@decDiscount > @decMaxDedPerAmt)
							SET @decDiscount = @decMaxDedPerAmt;
				
						IF ( @tintPriceRuleType = 1 )
								SET @monPriceRuleFinalPrice = (@monListPrice - @decDiscount);
						IF ( @tintPriceRuleType = 2 )
								SET @monPriceRuleFinalPrice = (@monVendorCost + @decDiscount);						
					
						SET @monPriceRuleFinalPrice = (CASE WHEN @numBaseCurrency <> @numCurrencyID THEN CAST((ISNULL(@monPriceRuleFinalPrice,0) / @fltExchangeRate) AS INT) ELSE ISNULL(@monPriceRuleFinalPrice,0) END)
					END
					IF ( @tintPriceBookDiscountType = 2 ) -- Flat discount 
					BEGIN
						SET @decDiscount = (CASE WHEN @numBaseCurrency <> @numCurrencyID THEN CAST((ISNULL(@decPriceBookDiscount,0) / @fltExchangeRate) AS INT) ELSE ISNULL(@decPriceBookDiscount,0) END) * ((@numQty * @fltUOMConversionFactor)/@intQntyItems);

						IF (@decDiscount > @decMaxDedPerAmt)
							SET @decDiscount = @decMaxDedPerAmt;
					
						IF ( @tintPriceRuleType = 1 )
							SET @monPriceRuleFinalPrice =  ((@monListPrice * @numQty * @fltUOMConversionFactor) - @decDiscount)/(@numQty * @fltUOMConversionFactor);
						IF ( @tintPriceRuleType = 2 )
							SET @monPriceRuleFinalPrice = ((@monVendorCost * @numQty * @fltUOMConversionFactor) + @decDiscount)/(@numQty * @fltUOMConversionFactor);
					END
				END
			END
		
		END
	
		-- GET Last Price
		DECLARE @LastDiscountType TINYINT
		DECLARE @LastDiscount FLOAT

		SELECT 
			TOP 1 
			@monLastPrice = ((ISNULL(monPrice,0) * ISNULL(OpportunityMaster.fltExchangeRate,1)) / @fltExchangeRate)
			,@LastDiscountType = ISNULL(OpportunityItems.bitDiscountType,1)
			,@LastDiscount = (CASE 
								WHEN ISNULL(OpportunityItems.bitDiscountType,1) = 1 
								THEN ((ISNULL(OpportunityItems.fltDiscount,0) * ISNULL(OpportunityMaster.fltExchangeRate,1)) / @fltExchangeRate)
								ELSE ISNULL(OpportunityItems.fltDiscount,0) END) 
			,@dtLastOrderDate = dbo.FormatedDateFromDate(OpportunityMaster.bintCreatedDate,@numDomainID)
		FROM 
			OpportunityItems 
		JOIN 
			OpportunityMaster 
		ON 
			OpportunityItems.numOppId = OpportunityMaster.numOppId 
		WHERE 
			OpportunityMaster.numDomainId= @numDomainID
			AND tintOppType = @tintOppType
			AND tintOppStatus = 1
			AND numItemCode=@numItemCode
		ORDER BY
			OpportunityMaster.numOppId DESC

		IF @tintDefaultSalesPricing = 1 AND ISNULL(@monPriceFinalPrice,0) > 0
		BEGIN
			-- WE ARE NOT DISPLAYING DISCOUNT SEPERATELY IN PRICE LEVEL
			SET @tintPricingBasedOn = 1
			SET @monPrice = @monPriceFinalPrice
			SET @tintDisountType = 1
			SET @decDiscount = 0
		END
		ELSE IF @tintDefaultSalesPricing = 2 AND ISNULL(@monPriceRuleFinalPrice,0) > 0
		BEGIN
			SET @tintPricingBasedOn = 2
		
			IF @tintDisountType = 3
			BEGIN
				SET @monPrice = @monPriceRuleFinalPrice
				SET @tintDisountType = 1
				SET @decDiscount = 0
			END
			ELSE
			BEGIN
				IF @tintPriceBookDiscount = 1 -- Display Unit Price And Disocunt Price Seperately
				BEGIN
					SET @monPrice = @monPriceRulePrice
				END
				ELSE -- Display Disocunt Price As Unit Price
				BEGIN
					SET @monPrice = @monPriceRuleFinalPrice
					SET @tintDisountType = 1
					SET @decDiscount = 0
				END
			END
		END
		ELSE IF @tintDefaultSalesPricing = 3 AND ISNULL(@monLastPrice,0) > 0
		BEGIN
			SET @tintPricingBasedOn = 3
			SET @monPrice = @monLastPrice
			SET @tintDisountType = @LastDiscountType
			SET @decDiscount = @LastDiscount
		END
		ELSE
		BEGIN
			SET @tintPricingBasedOn = 0
			SET @tintDisountType = 1
			SET @decDiscount = 0
			SET @tintRuleType = 0

			IF @tintOppType = 1
				SET @monPrice = @monListPrice
			ELSE
				SET @monPrice = @monVendorCost
		END
	END
	ELSE IF @tintOppType = 2
	BEGIN
		SELECT 
			TOP 1
			@numPriceRuleID = numPricRuleID,
			@tintPricingMethod = tintPricingMethod,
			@tintPriceRuleType = p.tintRuleType,
			@tintPriceBookDiscountType = P.[tintDiscountType],
			@decPriceBookDiscount = P.[decDiscount],
			@intQntyItems = P.[intQntyItems],
			@decMaxDedPerAmt = P.[decMaxDedPerAmt]
		FROM   
			PriceBookRules P 
		LEFT JOIN 
			PriceBookRuleDTL PDTL 
		ON 
			P.numPricRuleID = PDTL.numRuleID
		LEFT JOIN 
			PriceBookRuleItems PBI 
		ON 
			P.numPricRuleID = PBI.numRuleID
		LEFT JOIN 
			[PriceBookPriorities] PP 
		ON 
			PP.[Step2Value] = P.[tintStep2] 
			AND PP.[Step3Value] = P.[tintStep3]
		WHERE  
			P.numDomainID=@numDomainID 
			AND tintRuleFor=@tintOppType
			AND (
					((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 1 AND PDTL.numValue = @numDivisionID)) -- Priority 1
					OR ((tintStep2 = 2 AND PBI.numValue = @numItemClassification) AND (tintStep3= 1 AND PDTL.numValue = @numDivisionID)) -- Priority 2
					OR ((tintStep2 = 3) AND (tintStep3 = 1 AND PDTL.numValue = @numDivisionID)) -- Priority 3

					OR ((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 4
					OR ((tintStep2 = 2 AND PBI.numValue = @numItemClassification) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 5
					OR ((tintStep2 = 3) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 6

					OR ((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 3)) -- Priority 7
					OR ((tintStep2 = 2 AND PBI.numValue = @numItemClassification) AND (tintStep3 = 3)) -- Priority 8
					OR ((tintStep2 = 3) and (tintStep3 = 3)) -- Priority 9
				)
		ORDER BY 
			PP.Priority ASC


		IF ISNULL(@numPriceRuleID,0) > 0
		BEGIN
			IF @tintPricingMethod = 1 -- BASED ON PRICE TABLE
			BEGIN
				SELECT
					@tintDisountType = tintDiscountType,
					@decDiscount = (CASE WHEN tintDiscountType=3 THEN 0 ELSE ISNULL(decDiscount,0) END),
					@tintRuleType = tintRuleType,
					@monPriceRulePrice = (CASE tintRuleType WHEN 1 THEN @monListPrice WHEN 2 THEN @monVendorCost END) ,
					@monPriceRuleFinalPrice = (CASE tintRuleType
											WHEN 1 -- Deduct From List Price
											THEN
												CASE tintDiscountType 
													WHEN 1 -- PERCENT
													THEN (CASE WHEN @monListPrice > 0 THEN @monListPrice - (@monListPrice * (ISNULL(decDiscount,0)/100)) ELSE 0 END)
													WHEN 2 -- FLAT AMOUNT
													THEN (CASE WHEN @monListPrice > 0 THEN (CASE WHEN (@monListPrice * @numQty * @fltUOMConversionFactor) - ISNULL(decDiscount,0) > 0 THEN ((@monListPrice * @numQty * @fltUOMConversionFactor) - ISNULL(decDiscount,0)) / (@numQty * @fltUOMConversionFactor) ELSE 0 END) ELSE 0 END)
													WHEN 3 -- NAMED PRICE
													THEN ISNULL(decDiscount,0)
												END
											WHEN 2 -- Add to primary vendor cost
											THEN
												CASE tintDiscountType 
													WHEN 1  -- PERCENT
													THEN @monVendorCost + (@monVendorCost * (ISNULL(decDiscount,0)/100))
													WHEN 2 -- FLAT AMOUNT
													THEN (CASE WHEN (@monVendorCost * @numQty * @fltUOMConversionFactor) + ISNULL(decDiscount,0) > 0 THEN ((@monVendorCost * @numQty * @fltUOMConversionFactor) + ISNULL(decDiscount,0)) / (@numQty * @fltUOMConversionFactor)  ELSE 0 END)
													WHEN 3 -- NAMED PRICE
													THEN ISNULL(decDiscount,0)
												END
											WHEN 3 -- Named price
											THEN
												CASE 
												WHEN ISNULL(@tintPriceLevel,0) > 0
												THEN
													(SELECT 
															ISNULL(decDiscount,0)
													FROM
													(
														SELECT 
															ROW_NUMBER() OVER(PARTITION BY PricingTable.numItemCode ORDER BY numPricingID) Id,
															decDiscount
														FROM 
															PricingTable 
														WHERE 
															PricingTable.numItemCode = @numItemCode 
															AND tintRuleType = 3 
													) TEMP
													WHERE
														Id = @tintPriceLevel)
												ELSE
													ISNULL(decDiscount,0)
												END
											END
										) 
				FROM
					PricingTable
				WHERE
					numPriceRuleID=@numPriceRuleID AND @numQty * @fltUOMConversionFactor >= intFromQty AND @numQty * @fltUOMConversionFactor <= intToQty
			END
			ELSE IF @tintPricingMethod = 2 -- BASED ON RULE
			BEGIN
				SET @tintRuleType = @tintPriceRuleType
				SET @monPriceRulePrice = (CASE @tintPriceRuleType WHEN 1 THEN @monListPrice WHEN 2 THEN @monVendorCost END) 

				IF (@tintPriceBookDiscountType = 1 ) -- Percentage 
				BEGIN
					SET @tintDisountType = @tintPriceBookDiscountType
					SET @decDiscount = @decDiscount * (@numQty/@intQntyItems);
					SET @decDiscount = @decDiscount * (CASE WHEN @tintPriceRuleType = 2 THEN @monVendorCost ELSE @monListPrice END) /100;
					SET @decMaxDedPerAmt = @decMaxDedPerAmt * (CASE WHEN @tintPriceRuleType = 2 THEN @monVendorCost ELSE @monListPrice END) /100;
					
					IF (@decDiscount > @decMaxDedPerAmt)
						SET @decDiscount = @decMaxDedPerAmt;
				
					IF ( @tintPriceRuleType = 1 )
							SET @monPriceRuleFinalPrice = (@monListPrice - @decDiscount);
					IF ( @tintPriceRuleType = 2 )
							SET @monPriceRuleFinalPrice = (@monVendorCost + @decDiscount);
				END
				IF ( @tintPriceBookDiscountType = 2 ) -- Flat discount 
				BEGIN
					SET @decDiscount = @decDiscount * ((@numQty * @fltUOMConversionFactor)/@intQntyItems);

					IF (@decDiscount > @decMaxDedPerAmt)
						SET @decDiscount = @decMaxDedPerAmt;
					
					IF ( @tintPriceRuleType = 1 )
						SET @monPriceRuleFinalPrice =  ((@monListPrice * @numQty * @fltUOMConversionFactor) - @decDiscount)/(@numQty * @fltUOMConversionFactor);
					IF ( @tintPriceRuleType = 2 )
						SET @monPriceRuleFinalPrice = ((@monVendorCost * @numQty * @fltUOMConversionFactor) + @decDiscount)/(@numQty * @fltUOMConversionFactor);
				END
			END
		END

		If @monPriceRuleFinalPrice > 0
		BEGIN
			SET @monPrice = @monPriceRuleFinalPrice
		END
		ELSE
		BEGIN
			SET @monPrice = @monVendorCost
		END


		SET @monPriceLevelPrice = 0
		SET @monPriceFinalPrice = 0
		SET @monPriceRulePrice = 0
		SET @monPriceRuleFinalPrice = 0
		SET @monLastPrice = 0
		SET @dtLastOrderDate = NULL
		SET @tintDisountType = 1
		SET @decDiscount =0 
	END

	DECLARE @numTempWarehouseID NUMERIC(18,0)
	IF ISNULL(@numWarehouseItemID,0) > 0
	BEGIN
		SELECT @numTempWarehouseID=numWareHouseID FROM WareHouseItems WHERE numWareHouseItemID=@numWarehouseItemID
	END
	ELSE
	BEGIN
		SET @numTempWarehouseID = @numWarehouseID
	END

	SELECT
		@vcItemName AS vcItemName,
		@bitDropship AS bitDropship,
		@chrItemType AS ItemType,
		@bitMatrix AS bitMatrix,
		@numItemClassification AS ItemClassification,
		@numItemGroup AS ItemGroup,
		@ItemDesc AS ItemDescription,
		@bitAssembly AS bitAssembly,
		@bitKit AS bitKitParent,
		@bitHasChildKits AS bitHasChildKits,
		@bitSOWorkOrder AS bitSOWorkOrder,
		case when ISNULL(@bitAssembly,0)=1 then dbo.fn_GetAssemblyPossibleWOQty(@numItemCode,@numTempWarehouseID) else 0 end AS MaxWorkOrderQty,
		@tintPricingBasedOn AS tintPriceBaseOn,
		@monPrice AS monPrice,
		@fltUOMConversionFactor AS UOMConversionFactor,
		(CASE WHEN @tintOppType=1 THEN @monListPrice ELSE @monVendorCost END) AS monListPrice,
		@monVendorCost AS VendorCost,
		@numVendorID AS numVendorID,
		@tintRuleType AS tintRuleType,
		@monPriceLevelPrice AS monPriceLevel,
		@monPriceFinalPrice AS monPriceLevelFinalPrice,
		@monPriceRulePrice AS monPriceRule,
		@monPriceRuleFinalPrice AS monPriceRuleFinalPrice,
		@monLastPrice AS monLastPrice,
		@dtLastOrderDate AS LastOrderDate,
		@tintDisountType AS DiscountType,
		@decDiscount AS Discount,
		@numBaseUnit AS numBaseUnit,
		@numSaleUnit AS numSaleUnit,
		@numPurchaseUnit AS numPurchaseUnit,
		dbo.fn_GetUOMName(@numBaseUnit) AS vcBaseUOMName,
		dbo.fn_GetUOMName(@numSaleUnit) AS vcSaleUOMName,
		@numPurchaseUnit AS numPurchaseUnit,
		@numWarehouseItemID AS numWarehouseItemID,
		ISNULL((SELECT numOnHand FROM WareHouseItems WI WHERE WI.numWareHouseItemID=@numWarehouseItemID),0) numOnhand,
		dbo.fn_GetItemTransitCount(@numItemcode,@numDomainID) AS OrdersInTransist,
		(SELECT COUNT(*) FROM SimilarItems SI INNER JOIN Item I ON I.numItemCode = SI.numItemCode WHERE SI.numDomainId = @numDomainID AND SI.numParentItemCode = @numItemCode) AS RelatedItemsCount,
		@tintPriceLevel AS tintPriceLevel,
		@vcMinOrderQty AS vcMinOrderQty,
		@vcVendorNotes AS vcVendorNotes,
		@bitCalAmtBasedonDepItems AS bitCalAmtBasedonDepItems,
		@tintKitAssemblyPriceBasedOn AS tintKitAssemblyPriceBasedOn
END 
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
     
GO

IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getwarehouseitemidfor850')
DROP PROCEDURE usp_getwarehouseitemidfor850
GO
CREATE PROCEDURE [dbo].[USP_GetWarehouseItemIdFor850]        
	@numDomainID NUMERIC(18,0),        
	@numItemCode NUMERIC(18,0),
	@numQty FLOAT,
	@numOrderSource NUMERIC(18,0),
	@tintSourceType TINYINT,
	@numShipToCountry NUMERIC(18,0),
	@numShipToState NUMERIC(18,0)
AS
BEGIN         
	DECLARE @TEMP TABLE
	(	
		ID INT IDENTITY(1,1)
		,numWareHouseItemID NUMERIC(18,0)
		,bitWarehouseMapped BIT
	)

	IF EXISTS (SELECT ID FROM MassSalesFulfillmentConfiguration WHERE numDomainID=@numDomainID AND ISNULL(bitAutoWarehouseSelection,0)= 1)
	BEGIN
		DECLARE @bitKitParent BIT
		SELECT @bitKitParent=ISNULL(bitKitParent,0) FROM Item WHERE numDomainID=@numDomainID AND numItemCode=@numItemCode

		DECLARE @TEMPWarehouse TABLE
		(
			numWarehouseItemID NUMERIC(18,0)
			,numWarehouseID NUMERIC(18,0)
			,numOnHand FLOAT
		)

		INSERT INTO @TEMPWarehouse
		(
			numWarehouseItemID
			,numWarehouseID
			,numOnHand
		)
		SELECT
			WareHouseItems.numWarehouseItemID
			,WareHouseItems.numWareHouseID
			,ISNULL((SELECT SUM(WIInner.numOnHand) FROM WareHouseItems WIInner WHERE WIInner.numItemID = WareHouseItems.numItemID AND WIInner.numWareHouseID=WareHouseItems.numWareHouseID),0)
		FROM
			WareHouseItems
		LEFT JOIN
			WarehouseLocation
		ON
			WareHouseItems.numWLocationID = WarehouseLocation.numWLocationID
		WHERE
			WareHouseItems.numItemID = @numItemCode
			AND WarehouseLocation.numWLocationID IS NULL

		INSERT INTO @TEMP
		(
			numWareHouseItemID
			,bitWarehouseMapped
		)
		SELECT
			numWareHouseItemID
			,1
		FROM
		(
			SELECT 
				ROW_NUMBER() OVER(PARTITION BY titOnHandOrder ORDER BY titOnHandOrder ASC) numRowIndex	
				,*
			FROM
			(
				SELECT
					TW.numWareHouseItemID
					,(CASE 
						WHEN @bitKitParent = 1 
						THEN dbo.fn_IsKitCanBeBuild(@numDomainID,@numItemCode,@numQty,TW.numWarehouseID,'')
						ELSE (CASE WHEN TW.numOnHand >= @numQty THEN 1 ELSE 0 END) 
					END) titOnHandOrder
					,MassSalesFulfillmentWP.intOrder
				FROM
					MassSalesFulfillmentWM
				INNER JOIN
					MassSalesFulfillmentWMState
				ON
					MassSalesFulfillmentWM.ID = MassSalesFulfillmentWMState.numMSFWMID
				INNER JOIN
					MassSalesFulfillmentWP
				ON
					MassSalesFulfillmentWM.ID = MassSalesFulfillmentWP.numMSFWMID
				INNER JOIN
					@TEMPWarehouse TW
				ON
					MassSalesFulfillmentWP.numWarehouseID	= TW.numWarehouseID
				WHERE
					MassSalesFulfillmentWM.numDomainID = @numDomainID
					AND MassSalesFulfillmentWM.numOrderSource = @numOrderSource
					AND MassSalesFulfillmentWM.tintSourceType = @tintSourceType
					AND MassSalesFulfillmentWM.numCountryID = @numShipToCountry
					AND MassSalesFulfillmentWMState.numStateID = @numShipToState
					
			) TEMP
		) T1
		ORDER BY
			titOnHandOrder DESC, intOrder ASC	
	END
	
	IF EXISTS (SELECT ID FROM @TEMP)
	BEGIN
		SELECT TOP 1 numWareHouseItemID FROM @TEMP ORDER BY ID
	END
	ELSE
	BEGIN
		SELECT TOP 1 numWarehouseItemID FROM WarehouseItems WHERE WarehouseItems.numItemID= @numItemCode AND numDomainID = @numDomainID
	END
END
GO
GO

/****** Object:  StoredProcedure [dbo].[USP_GetWarehousesForSelectedItem]    Script Date: 04-05-2018 10:59:19 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetWarehousesForSelectedItem')
DROP PROCEDURE USP_GetWarehousesForSelectedItem
GO

CREATE PROCEDURE [dbo].[USP_GetWarehousesForSelectedItem]    
(
	@numItemCode AS VARCHAR(20)=''  ,
	@numWareHouseID AS NUMERIC(18,0) = 0,
	@numQty FLOAT,
	@numOrderSource NUMERIC(18,0),
	@tintSourceType TINYINT,
	@numShipToCountry NUMERIC(18,0) = 0,
	@numShipToState NUMERIC(18,0) = 0,
	@vcSelectedKitChildItems VARCHAR(MAX) = ''
)
AS
BEGIN  
	DECLARE @numDomainID AS NUMERIC(18,0)
	DECLARE @numBaseUnit AS NUMERIC(18,0)
	DECLARE @vcUnitName as VARCHAR(100) 
	DECLARE @numSaleUnit AS NUMERIC(18,0)
	DECLARE @vcSaleUnitName as VARCHAR(100) 
	DECLARE @numPurchaseUnit AS NUMERIC(18,0)
	DECLARE @vcPurchaseUnitName as VARCHAR(100) 
	declare @bitSerialize as bit     
	DECLARE @bitKitParent BIT
	DECLARE @bitAssembly BIT
 
	SELECT 
		@numDomainID = numDomainID,
		@numBaseUnit = ISNULL(numBaseUnit,0),
		@numSaleUnit = ISNULL(numSaleUnit,0),
		@numPurchaseUnit = ISNULL(numPurchaseUnit,0),
		@bitSerialize=bitSerialized,
		@bitAssembly=ISNULL(bitAssembly,0),
		@bitKitParent = ( CASE WHEN ISNULL(bitKitParent,0) = 1
								   AND ISNULL(bitAssembly,0) = 1 THEN 0
							  WHEN ISNULL(bitKitParent,0) = 1 THEN 1
							  ELSE 0
						 END ) 
	FROM 
		Item 
	WHERE 
		numItemCode=@numItemCode    

	DECLARE @TEMPWarehouse TABLE
	(
		numWarehouseItemID NUMERIC(18,0)
		,numWarehouseID NUMERIC(18,0)
		,numOnHand FLOAT
	)

	INSERT INTO @TEMPWarehouse
	(
		numWarehouseItemID
		,numWarehouseID
		,numOnHand
	)
	SELECT
		WareHouseItems.numWarehouseItemID
		,WareHouseItems.numWareHouseID
		,ISNULL((SELECT SUM(WIInner.numOnHand) FROM WareHouseItems WIInner WHERE WIInner.numItemID = WareHouseItems.numItemID AND WIInner.numWareHouseID=WareHouseItems.numWareHouseID),0)
	FROM
		WareHouseItems
	LEFT JOIN
		WarehouseLocation
	ON
		WareHouseItems.numWLocationID = WarehouseLocation.numWLocationID
	WHERE
		WareHouseItems.numItemID = @numItemCode
		AND WarehouseLocation.numWLocationID IS NULL


	DECLARE @TEMP TABLE
	(	
		ID INT IDENTITY(1,1)
		,numWareHouseItemID NUMERIC(18,0)
		,bitWarehouseMapped BIT
	)

	IF EXISTS (SELECT ID FROM MassSalesFulfillmentConfiguration WHERE numDomainID=@numDomainID AND ISNULL(bitAutoWarehouseSelection,0)= 1)
	BEGIN
		INSERT INTO @TEMP
		(
			numWareHouseItemID
			,bitWarehouseMapped
		)
		SELECT 
			numWareHouseItemID
			,1
		FROM
		(
			SELECT 
				ROW_NUMBER() OVER(PARTITION BY titOnHandOrder ORDER BY titOnHandOrder ASC) numRowIndex	
				,*
			FROM
			(
				SELECT
					TW.numWareHouseItemID
					,(CASE 
						WHEN @bitKitParent = 1 
						THEN dbo.fn_IsKitCanBeBuild(@numDomainID,@numItemCode,@numQty,TW.numWarehouseID,@vcSelectedKitChildItems)
						ELSE (CASE WHEN TW.numOnHand >= @numQty THEN 1 ELSE 0 END) 
					END) titOnHandOrder
					,MassSalesFulfillmentWP.intOrder
				FROM
					MassSalesFulfillmentWM
				INNER JOIN
					MassSalesFulfillmentWMState
				ON
					MassSalesFulfillmentWM.ID = MassSalesFulfillmentWMState.numMSFWMID
				INNER JOIN
					MassSalesFulfillmentWP
				ON
					MassSalesFulfillmentWM.ID = MassSalesFulfillmentWP.numMSFWMID
				INNER JOIN
					@TEMPWarehouse TW
				ON
					MassSalesFulfillmentWP.numWarehouseID	= TW.numWarehouseID
				WHERE
					MassSalesFulfillmentWM.numDomainID = @numDomainID
					AND MassSalesFulfillmentWM.numOrderSource = @numOrderSource
					AND MassSalesFulfillmentWM.tintSourceType = @tintSourceType
					AND MassSalesFulfillmentWM.numCountryID = @numShipToCountry
					AND MassSalesFulfillmentWMState.numStateID = @numShipToState
					
			) TEMP
		) T1
		ORDER BY
			titOnHandOrder DESC, intOrder ASC
	END

	SELECT @vcUnitName=ISNULL(vcUnitName,'-') FROM UOM u WHERE u.numUOMId=@numBaseUnit
	SELECT @vcSaleUnitName=ISNULL(vcUnitName,'-') FROM UOM u WHERE u.numUOMId=@numSaleUnit
	SELECT @vcPurchaseUnitName=ISNULL(vcUnitName,'-') FROM UOM u WHERE u.numUOMId=@numPurchaseUnit

	DECLARE @numSaleUOMFactor AS DECIMAL(28,14)
	DECLARE @numPurchaseUOMFactor AS DECIMAL(28,14)

	SELECT @numSaleUOMFactor = dbo.fn_UOMConversion(@numSaleUnit,@numItemCode,@numDOmainID,@numBaseUnit)
	SELECT @numPurchaseUOMFactor = dbo.fn_UOMConversion(@numPurchaseUnit,@numItemCode,@numDOmainID,@numBaseUnit)

	SELECT 
		(WareHouseItems.numWareHouseItemId),
		ISNULL(WareHouseItems.numWLocationID,0) numWLocationID,
		ISNULL(vcWareHouse,'') AS vcWareHouse, 
		dbo.fn_GetAttributes(WareHouseItems.numWareHouseItemId,@bitSerialize) as Attr, 
		WareHouseItems.monWListPrice,
		CASE 
			WHEN @bitKitParent=1 
			THEN dbo.fn_GetKitInventory(@numItemCode,WareHouseItems.numWareHouseID) 
			ELSE ISNULL((SELECT SUM(WI.numOnHand) FROM WareHouseItems WI INNER JOIN WarehouseLocation WL ON WI.numWLocationID = WL.numWLocationID WHERE WI.numItemID=WareHouseItems.numItemID AND WI.numWarehouseID=WareHouseItems.numWarehouseID),0) + ISNULL(numOnHand,0) 
		END numOnHand,
		CASE WHEN @bitKitParent=1 THEN 0 ELSE ISNULL(numOnOrder,0) END numOnOrder,
		CASE WHEN @bitKitParent=1 THEN 0 ELSE ISNULL(numReorder,0) END numReorder,
		CASE WHEN @bitKitParent=1 THEN 0 ELSE ISNULL(numAllocation,0) END numAllocation,
		CASE WHEN @bitKitParent=1 THEN 0 ELSE ISNULL(numBackOrder,0) END numBackOrder,
		Warehouses.numWareHouseID,
		@vcUnitName AS vcUnitName,
		@vcSaleUnitName AS vcSaleUnitName,
		@vcPurchaseUnitName AS vcPurchaseUnitName,
		CASE WHEN @numBaseUnit > 0 AND @numSaleUnit > 0
		THEN
			CASE WHEN @bitKitParent=1 THEN CAST(CAST(dbo.fn_GetKitInventory(@numItemCode,WareHouseItems.numWareHouseID)/@numSaleUOMFactor AS INT) AS VARCHAR) else CAST(CAST(ISNULL((SELECT SUM(WI.numOnHand) FROM WareHouseItems WI INNER JOIN WarehouseLocation WL ON WI.numWLocationID = WL.numWLocationID WHERE WI.numItemID=WareHouseItems.numItemID AND WI.numWarehouseID=WareHouseItems.numWarehouseID),0) + ISNULL(numOnHand,0) /@numSaleUOMFactor AS INT) AS VARCHAR) END
		ELSE
			'-'
		END AS numOnHandUOM,
		CASE WHEN @numBaseUnit > 0 AND @numSaleUnit > 0
		THEN
			CASE WHEN @bitKitParent=1 THEN '0' else CAST(CAST(ISNULL(numOnOrder,0)/@numPurchaseUOMFactor AS INT) AS VARCHAR) END
		ELSE
			'-'
		END AS numOnOrderUOM,
		CASE WHEN @numBaseUnit > 0 AND @numSaleUnit > 0
		THEN
			CASE WHEN @bitKitParent=1 THEN '0' else CAST(CAST(ISNULL(numReorder,0)/@numSaleUOMFactor AS INT) AS VARCHAR) END
		ELSE
			'-'
		END AS numReorderUOM,
		CASE WHEN @numBaseUnit > 0 AND @numSaleUnit > 0
		THEN
			CASE WHEN @bitKitParent=1 THEN '0' else CAST(CAST(ISNULL(numAllocation,0)/@numSaleUOMFactor AS INT) AS VARCHAR) END
		ELSE
			'-'
		END AS numAllocationUOM,
		CASE WHEN @numBaseUnit > 0 AND @numSaleUnit > 0
		THEN
			CASE WHEN @bitKitParent=1 THEN '0' else CAST(CAST(ISNULL(numBackOrder,0)/@numSaleUOMFactor AS INT) AS VARCHAR) END
		ELSE
			'-'
		END AS numBackOrderUOM
		,T.bitWarehouseMapped
	FROM 
		WareHouseItems
	JOIN 
		Warehouses 
	ON 
		Warehouses.numWareHouseID=WareHouseItems.numWareHouseID  
	LEFT JOIN
		@TEMP T
	ON
		WareHouseItems.numWareHouseItemID = T.numWareHouseItemID
	WHERE 
		numItemID=@numItemCode
		AND ISNULL(WareHouseItems.numWLocationID,0) = 0
	ORDER BY
		(CASE 
			WHEN T.ID IS NULL 
			THEN (CASE 
					WHEN WareHouseItems.numWareHouseID = @numWareHouseID then 99991
					ELSE 99992
				END)
			ELSE T.ID 
		END) ASC
END
GO


GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_TicklerActItemsV2TopCount')
DROP PROCEDURE USP_TicklerActItemsV2TopCount
GO
CREATE Proc [dbo].[USP_TicklerActItemsV2TopCount]     
@numUserCntID as numeric(9)=null,                                                                        
@numDomainID as numeric(9)=null,                                                                        
@startDate as datetime,                                                                        
@endDate as datetime,                                                                    
@ClientTimeZoneOffset Int,
@numViewID AS NUMERIC(18,0),
@numTotalRecords AS NUMERIC(18,0) OUTPUT
AS
BEGIN
	DECLARE @bitGroupByOrder AS BIT =0
	DECLARE @numBatchID AS NUMERIC(18,0)=0
	DECLARE @bitIncludeSearch AS BIT =1
	DECLARE @tintPrintBizDocViewMode AS INT =1
	DECLARE @tintPendingCloseFilter AS INT =1
	DECLARE @numShippingZone AS NUMERIC(18,0)=0
	DECLARE @vcOrderStauts VARCHAR(MAX) ='' 
	DECLARE @tintFilterBy TINYINT =0
	DECLARE @vcFilterValue VARCHAR(MAX)=''
	,@numPageIndex INT=1
	,@vcSortColumn VARCHAR(100)=''
	,@vcSortOrder VARCHAR(4)=''
	,@numWarehouseID VARCHAR(100)
	,@bitRemoveBO BIT =0


	IF @numViewID = 4 OR @numViewID = 5 OR @numViewID = 6
	BEGIN
		SET @bitGroupByOrder = 1
	END
	DECLARE @tintPackingViewMode AS INT =1
	DECLARE @tintPackingMode AS INT
	DECLARE @tintInvoicingType AS INT 
	SELECT
		@tintPackingMode=tintPackingMode
		,@tintInvoicingType=tintInvoicingType
	FROM
		MassSalesFulfillmentConfiguration
	WHERE
		numDomainID = @numDomainID 

	DECLARE @tintCommitAllocation TINYINT
	DECLARE @numShippingServiceItemID NUMERIC(18,0)
	DECLARE @numDefaultSalesShippingDoc NUMERIC(18,0)
	SELECT 
		@tintCommitAllocation=ISNULL(tintCommitAllocation,1) 
		,@numShippingServiceItemID=ISNULL(numShippingServiceItemID,0)
		,@numDefaultSalesShippingDoc=ISNULL(numDefaultSalesShippingDoc,0)
	FROM 
		Domain 
	WHERE 
		numDomainId=@numDomainID 

	DECLARE @vcCustomSearchValue AS VARCHAR(MAX) ='' 
	SET @vcCustomSearchValue = REPLACE(@vcCustomSearchValue,'OpportunityMaster.dtItemReceivedDate','(SELECT TOP 1 OMInner.dtItemReceivedDate FROM OpportunityMaster OMInner INNER JOIN OpportunityItems OIInner ON OMInner.numOppID=OIInner.numOppID WHERE OMInner.tintOppType=2 AND OMInner.tintOppStatus=1 AND OIInner.numItemCode=OpportunityItems.numItemCode ORDER BY OMInner.dtItemReceivedDate DESC)')

CREATE TABLE #TEMPMSRecords
	(
		numOppID NUMERIC(18,0)
		,numOppItemID NUMERIC(18,0)
		,numOppBizDocID NUMERIC(18,0)
	)
CREATE TABLE #TempWareHouse
(
	ID INT IDENTITY(1,1) PRIMARY KEY,
	numWareHouseId NUMERIC(18,0)
)

	DECLARE @vcSQL NVARCHAR(MAX)
	DECLARE @vcSQLFinal NVARCHAR(MAX)
	DECLARE @WarehouseCount AS INT = 0
	DECLARE @I AS INT = 1
INSERT INTO #TempWareHouse 	SELECT 
		[numWareHouseID]         
   FROM [dbo].[Warehouses] W 
   LEFT JOIN
		AddressDetails AD
	ON
        W.numAddressID = AD.numAddressID
   WHERE 
		W.numDomainID=@numDomainID

	SELECT @WarehouseCount = COUNT(*) FROM #TempWareHouse
	WHILE(@I<=@WarehouseCount)
	BEGIN
		SELECT @numWarehouseID = numWareHouseId FROM #TempWareHouse WHERE ID=@I
		
	SET @vcSQL = CONCAT('INSERT INTO #TEMPMSRecords
						(
							numOppID
							,numOppItemID
							,numOppBizDocID
						)
						SELECT
							OpportunityMaster.numOppId
							,',(CASE WHEN @bitGroupByOrder = 1 THEN '0' ELSE 'OpportunityItems.numoppitemtCode' END),'
							,',(CASE 
									WHEN @numViewID = 4 OR @numViewID = 6
									THEN 'OpportunityBizDocs.numOppBizDocsId'  
									ELSE '0' 
								END),'
						FROM
							OpportunityMaster
						INNER JOIN
							DivisionMaster
						ON
							OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
						INNER JOIN
							CompanyInfo
						ON
							DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
						INNER JOIN 
							OpportunityItems
						ON
							OpportunityMaster.numOppId = OpportunityItems.numOppId
						INNER JOIN
							Item
						ON
							OpportunityItems.numItemCode = Item.numItemCode
						LEFT JOIN
							WareHouseItems
						ON
							OpportunityItems.numWarehouseItmsID = WareHouseItems.numWareHouseItemID
						',
						(CASE 
							WHEN ISNULL(@numBatchID,0) > 0 
							THEN 
								' INNER JOIN 
										MassSalesFulfillmentBatchOrders
								ON
									OpportunityMaster.numOppId = MassSalesFulfillmentBatchOrders.numOppID
									AND (OpportunityItems.numOppItemtCode = MassSalesFulfillmentBatchOrders.numOppItemID OR ISNULL(MassSalesFulfillmentBatchOrders.numOppItemID,0) = 0)'
							ELSE 
								'' 
						END)	
						,
						(CASE 
							WHEN @numViewID = 4 
							THEN
								'INNER JOIN 
									OpportunityBizDocs
								ON
									OpportunityMaster.numOppId = OpportunityBizDocs.numOppId
									AND OpportunityBizDocs.numBizDocID=287'
							WHEN @numViewID = 6
							THEN
								CONCAT('INNER JOIN 
											OpportunityBizDocs
										ON
											OpportunityMaster.numOppId = OpportunityBizDocs.numOppId
											AND OpportunityBizDocs.numBizDocID=',(CASE @tintPrintBizDocViewMode WHEN 2 THEN 29397 WHEN 3 THEN 296 WHEN 4 THEN 287 ELSE 55206 END))
							ELSE ''

						END)
						,'
						WHERE
							OpportunityMaster.numDomainId=@numDomainID
							AND OpportunityMaster.tintOppType = 1
							AND OpportunityMaster.tintOppStatus = 1
							AND ISNULL(OpportunityMaster.tintshipped,0) = 0
							AND 1 = (CASE 
										WHEN @numViewID IN (1,2) AND ISNULL(@bitRemoveBO,0) = 1
										THEN (CASE 
												WHEN (Item.charItemType = ''p'' AND ISNULL(OpportunityItems.bitDropShip, 0) = 0 AND ISNULL(Item.bitAsset,0)=0)
												THEN 
													(CASE WHEN dbo.CheckOrderItemInventoryStatus(OpportunityItems.numItemCode,(CASE 
				WHEN @numViewID = 1
				THEN ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyPicked,0)
				WHEN @numViewID = 2 AND @tintPackingViewMode = 2
				THEN (CASE WHEN ISNULL(@tintPackingMode,1) = 2 THEN ISNULL(OpportunityItems.numQtyPicked,0) - ISNULL(OpportunityItems.numQtyShipped,0) ELSE ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyShipped,0) END)
				WHEN @numViewID = 2 AND @tintPackingViewMode = 3
				THEN (CASE WHEN ISNULL(@tintPackingMode,1) = 2 THEN ISNULL(OpportunityItems.numQtyPicked,0) ELSE ISNULL(OpportunityItems.numUnitHour,0) END) - ISNULL((SELECT 
																																											SUM(OpportunityBizDocItems.numUnitHour)
																																										FROM
																																											OpportunityBizDocs
																																										INNER JOIN
																																											OpportunityBizDocItems
																																										ON
																																											OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																																										WHERE
																																											OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
																																											AND OpportunityBizDocs.numBizDocId=@numDefaultSalesShippingDoc
					 																																					AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode),0)
				
				ELSE OpportunityItems.numUnitHour
			END),OpportunityItems.numoppitemtCode,OpportunityItems.numWarehouseItmsID,(CASE WHEN ISNULL(Item.bitKitParent,0) = 1 THEN 1 ELSE 0 END)) = 1 THEN 0 ELSE 1 END)
												ELSE 1
											END) 
										ELSE 1
									END)		
							AND 1 = (CASE WHEN LEN(ISNULL(@vcOrderStauts,'''')) > 0 THEN (CASE WHEN OpportunityMaster.numStatus ',(CASE WHEN ISNULL(@bitIncludeSearch,0) = 0 THEN 'NOT IN' ELSE 'IN' END),' (SELECT Id FROM dbo.SplitIDs(@vcOrderStauts,'','')) THEN 1 ELSE 0 END) ELSE 1 END)
							AND 1 = (CASE 
										WHEN LEN(ISNULL(@vcFilterValue,'''')) > 0
										THEN
											CASE 
												WHEN @tintFilterBy=1 --Item Classification
												THEN (CASE WHEN Item.numItemClassification ',(CASE WHEN ISNULL(@bitIncludeSearch,0) = 0 THEN 'NOT IN' ELSE 'IN' END),' (SELECT Id FROM dbo.SplitIDs(@vcFilterValue,'','')) THEN 1 ELSE 0 END)
												ELSE 1
											END
										ELSE 1
									END)
							AND 1 = (CASE
										WHEN @numViewID = 1 THEN (CASE WHEN ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyPicked,0) > 0 THEN 1 ELSE 0 END)
										WHEN @numViewID = 2 THEN (CASE 
																		WHEN @tintPackingViewMode = 1 
																		THEN (CASE 
																				WHEN ISNULL((SELECT COUNT(*) FROM OpportunityItems OIInner WHERE OIInner.numOppID=OpportunityMaster.numOppId AND OIInner.numItemCode=ISNULL(@numShippingServiceItemID,0)),0) = 0 
																				THEN 1 
																				ELSE 0 
																			END)
																		WHEN @tintPackingViewMode = 2
																		THEN (CASE 
																				WHEN ',(CASE WHEN ISNULL(@tintPackingMode,1) = 2 THEN 'ISNULL(OpportunityItems.numQtyPicked,0)' ELSE 'ISNULL(OpportunityItems.numUnitHour,0)' END),' - ISNULL(OpportunityItems.numQtyShipped,0) > 0
																				THEN 1 
																				ELSE 0 
																			END)
																		WHEN @tintPackingViewMode = 3
																		THEN (CASE 
																				WHEN ',(CASE WHEN ISNULL(@tintPackingMode,1) = 2 THEN 'ISNULL(OpportunityItems.numQtyPicked,0)' ELSE 'ISNULL(OpportunityItems.numUnitHour,0)' END),' - ISNULL((SELECT 
																																																													SUM(OpportunityBizDocItems.numUnitHour)
																																																												FROM
																																																													OpportunityBizDocs
																																																												INNER JOIN
																																																													OpportunityBizDocItems
																																																												ON
																																																													OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																																																												WHERE
																																																													OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
																																																													AND OpportunityBizDocs.numBizDocId=@numDefaultSalesShippingDoc
																																																													AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode),0) > 0
																				THEN 1 
																				ELSE 0 
																			END)','
																		ELSE 0
																	END)
										WHEN @numViewID = 3
										THEN (CASE 
												WHEN (CASE 
														WHEN ISNULL(@tintInvoicingType,0)=1 
														THEN ISNULL(OpportunityItems.numQtyShipped,0) 
														ELSE ISNULL(OpportunityItems.numUnitHour,0) 
													END) - ISNULL((SELECT 
																		SUM(OpportunityBizDocItems.numUnitHour)
																	FROM
																		OpportunityBizDocs
																	INNER JOIN
																		OpportunityBizDocItems
																	ON
																		OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																	WHERE
																		OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
																		AND OpportunityBizDocs.numBizDocId=287
																		AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode),0) > 0 
												THEN 1
												ELSE 0 
											END)
										WHEN @numViewID = 4 THEN 1
										WHEN @numViewID = 5 THEN 1
										WHEN @numViewID = 6 THEN 1
									END)
							AND 1 = (CASE
										WHEN @numViewID = 1 THEN (CASE WHEN ISNULL(WareHouseItems.numWareHouseItemID,0) > 0 AND ISNULL(OpportunityItems.bitDropship,0) = 0 THEN 1 ELSE 0 END)
										WHEN @numViewID = 2 AND @tintPackingViewMode <> 2 THEN (CASE WHEN ISNULL(WareHouseItems.numWareHouseItemID,0) > 0 AND ISNULL(OpportunityItems.bitDropship,0) = 0 THEN 1 ELSE 0 END)
										WHEN @numViewID = 2 AND @tintPackingViewMode = 2 THEN 1
										WHEN @numViewID = 3 THEN 1
										WHEN @numViewID = 4 THEN 1
										WHEN @numViewID = 5 THEN 1
										WHEN @numViewID = 6 THEN 1
										ELSE 0
									END)',
							(CASE 
								WHEN @numViewID = 5 AND ISNULL(@tintPendingCloseFilter,0) = 2
								THEN ' AND (SELECT 
												COUNT(*) 
											FROM 
											(
												SELECT
													OI.numoppitemtCode,
													ISNULL(OI.numUnitHour,0) AS OrderedQty,
													ISNULL(TempFulFilled.FulFilledQty,0) AS FulFilledQty
												FROM
													OpportunityItems OI
												INNER JOIN
													Item I
												ON
													OI.numItemCode = I.numItemCode
												OUTER APPLY
												(
													SELECT
														SUM(OpportunityBizDocItems.numUnitHour) AS FulFilledQty
													FROM
														OpportunityBizDocs
													INNER JOIN
														OpportunityBizDocItems 
													ON
														OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
													WHERE
														OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
														AND ISNULL(OpportunityBizDocs.numBizDocId,0) = 296
														AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
												) AS TempFulFilled
												WHERE
													OI.numOppID = OpportunityMaster.numOppID
													AND ISNULL(OI.numUnitHour,0) > 0
													AND ISNULL(OI.bitDropShip,0) = 0
											) X
											WHERE
												X.OrderedQty <> X.FulFilledQty) = 0
										AND (SELECT 
												COUNT(*) 
											FROM 
											(
												SELECT
													OI.numoppitemtCode,
													ISNULL(OI.numUnitHour,0) AS OrderedQty,
													ISNULL(TempInvoice.InvoicedQty,0) AS InvoicedQty
												FROM
													OpportunityItems OI
												INNER JOIN
													Item I
												ON
													OI.numItemCode = I.numItemCode
												OUTER APPLY
												(
													SELECT
														SUM(OpportunityBizDocItems.numUnitHour) AS InvoicedQty
													FROM
														OpportunityBizDocs
													INNER JOIN
														OpportunityBizDocItems 
													ON
														OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
													WHERE
														OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
														AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
														AND ISNULL(OpportunityBizDocs.bitAuthoritativeBizDocs,0) = 1
												) AS TempInvoice
												WHERE
													OI.numOppID = OpportunityMaster.numOppID
													AND ISNULL(OI.numUnitHour,0) > 0
											) X
											WHERE
												X.OrderedQty > X.InvoicedQty) = 0'
								ELSE '' 
							END)
							,(CASE WHEN @numViewID = 4 THEN 'AND 1 = (CASE WHEN ISNULL(OpportunityBizDocs.monDealAmount,0) - ISNULL(OpportunityBizDocs.monAmountPaid,0) > 0 THEN 1 ELSE 0 END)' ELSE '' END)
							,' AND 1 = (CASE WHEN @numViewID = 2 AND @tintPackingViewMode = 3 THEN (CASE WHEN ISNULL(Item.bitContainer,0)=1 THEN 0 ELSE 1 END) ELSE 1 END)
							AND 1 = (CASE WHEN ISNULL(@numWarehouseID,'''') <>'''' AND ISNULL(OpportunityItems.numWarehouseItmsID,0) > 0 THEN (CASE WHEN WareHouseItems.numWarehouseID IN (SELECT Items FROM Split(@numWarehouseID,'','') WHERE Items<>'''') THEN 1 ELSE 0 END) ELSE 1 END)	'
								,(CASE WHEN ISNULL(@numShippingZone,0) > 0 THEN ' AND 1 = (CASE 
																				WHEN ISNULL((SELECT TOP 1 numState FROM fn_getOPPAddressDetails(OpportunityMaster.numOppId,OpportunityMaster.numDomainID,2)),0) > 0 
																				THEN
																					CASE 
																						WHEN (SELECT COUNT(*) FROM dbo.State WHERE numDomainID=@numDomainId AND numStateID =  (SELECT TOP 1 numState FROM fn_getOPPAddressDetails(OpportunityMaster.numOppId,OpportunityMaster.numDomainID,2)) AND numShippingZone=@numShippingZone) > 0 
																						THEN 1
																						ELSE 0
																					END
																				ELSE 0 
																			END)' ELSE '' END),
								@vcCustomSearchValue,(CASE WHEN @bitGroupByOrder = 1 THEN CONCAT('GROUP BY OpportunityMaster.numOppId',(CASE WHEN @numViewID = 4 THEN ',OpportunityBizDocs.numOppBizDocsId,OpportunityBizDocs.monAmountPaid' WHEN @numViewID = 6 THEN ',OpportunityBizDocs.numOppBizDocsId'  ELSE '' END)) ELSE '' END));
	EXEC sp_executesql @vcSQL, N'@numDomainID NUMERIC(18,0), @vcOrderStauts VARCHAR(MAX), @vcFilterValue VARCHAR(MAX), @tintFilterBy TINYINT, @numViewID NUMERIC(18,0), @tintPackingViewMode TINYINT, @numShippingServiceItemID NUMERIC(18,0), @numShippingZone NUMERIC(18,0), @numDefaultSalesShippingDoc NUMERIC(18,0), @tintInvoicingType TINYINT, @tintPackingMode TINYINT, @bitRemoveBO BIT,@numWarehouseID VARCHAR(100)', @numDomainID, @vcOrderStauts, @vcFilterValue, @tintFilterBy, @numViewID, @tintPackingViewMode, @numShippingServiceItemID, @numShippingZone, @numDefaultSalesShippingDoc, @tintInvoicingType, @tintPackingMode, @bitRemoveBO,@numWarehouseID;

	If @numViewID = 2 AND @tintPackingViewMode = 3
	BEGIN
		SET @vcSQL = CONCAT('INSERT INTO #TEMPMSRecords
						(
							numOppID
							,numOppItemID
							,numOppBizDocID
						)
						SELECT
							OpportunityMaster.numOppId
							,',(CASE WHEN @bitGroupByOrder = 1 THEN '0' ELSE 'OpportunityItems.numoppitemtCode' END),'
							,OpportunityBizDocs.numOppBizDocsId
						FROM
							OpportunityMaster
						INNER JOIN
							DivisionMaster
						ON
							OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
						INNER JOIN
							CompanyInfo
						ON
							DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
						INNER JOIN 
							OpportunityBizDocs
						ON
							OpportunityMaster.numOppId = OpportunityBizDocs.numOppId
							AND OpportunityBizDocs.numBizDocID=@numDefaultSalesShippingDoc
						INNER JOIN 
							OpportunityItems
						ON
							OpportunityMaster.numOppId = OpportunityItems.numOppId
						INNER JOIN
							Item
						ON
							OpportunityItems.numItemCode = Item.numItemCode
						LEFT JOIN
							WareHouseItems
						ON
							OpportunityItems.numWarehouseItmsID = WareHouseItems.numWareHouseItemID
						
						',
						(CASE 
							WHEN ISNULL(@numBatchID,0) > 0 
							THEN 
								' INNER JOIN 
										MassSalesFulfillmentBatchOrders
								ON
									OpportunityMaster.numOppId = MassSalesFulfillmentBatchOrders.numOppID
									AND (OpportunityItems.numOppItemtCode = MassSalesFulfillmentBatchOrders.numOppItemID OR ISNULL(MassSalesFulfillmentBatchOrders.numOppItemID,0) = 0)'
							ELSE 
								'' 
						END)	
						,'
						WHERE
							OpportunityMaster.numDomainId=@numDomainID
							AND OpportunityMaster.tintOppType = 1
							AND OpportunityMaster.tintOppStatus = 1
							AND ISNULL(OpportunityMaster.tintshipped,0) = 0		
							AND 1 = (CASE WHEN LEN(ISNULL(@vcOrderStauts,'''')) > 0 THEN (CASE WHEN OpportunityMaster.numStatus ',(CASE WHEN ISNULL(@bitIncludeSearch,0) = 0 THEN 'NOT IN' ELSE 'IN' END),' (SELECT Id FROM dbo.SplitIDs(@vcOrderStauts,'','')) THEN 1 ELSE 0 END) ELSE 1 END)
							AND 1 = (CASE 
										WHEN LEN(ISNULL(@vcFilterValue,'''')) > 0
										THEN
											CASE 
												WHEN @tintFilterBy=1 --Item Classification
												THEN (CASE WHEN Item.numItemClassification ',(CASE WHEN ISNULL(@bitIncludeSearch,0) = 0 THEN 'NOT IN' ELSE 'IN' END),' (SELECT Id FROM dbo.SplitIDs(@vcFilterValue,'','')) THEN 1 ELSE 0 END)
												ELSE 1
											END
										ELSE 1
									END)
							AND 1 = (CASE 
										WHEN ISNULL((SELECT COUNT(*) FROM ShippingBox WHERE ISNULL(vcShippingLabelImage,'''') <> '''' AND ISNULL(vcTrackingNumber,'''') <> '''' AND numShippingReportId IN (SELECT numShippingReportId FROM ShippingReport WHERE ShippingReport.numOppID=OpportunityMaster.numOppId AND ShippingReport.numOppBizDocId=OpportunityBizDocs.numOppBizDocsId)),0) = 0 
										THEN 1 
										ELSE 0 
									END)
										
							AND 1 = (CASE WHEN ISNULL(WareHouseItems.numWareHouseItemID,0) > 0 AND ISNULL(OpportunityItems.bitDropship,0) = 0 THEN 1 ELSE 0 END)',
							' AND 1 = (CASE WHEN @numViewID = 2 AND @tintPackingViewMode = 3 THEN (CASE WHEN ISNULL(Item.bitContainer,0)=1 THEN 0 ELSE 1 END) ELSE 1 END)
							AND 1 = (CASE WHEN ISNULL(@numWarehouseID,'''') <>'''' AND ISNULL(OpportunityItems.numWarehouseItmsID,0) > 0 THEN (CASE WHEN WareHouseItems.numWarehouseID IN (SELECT Items FROM Split(@numWarehouseID,'','') WHERE Items<>'''') THEN 1 ELSE 0 END) ELSE 1 END)	'
								,(CASE WHEN ISNULL(@numShippingZone,0) > 0 THEN ' AND 1 = (CASE 
																				WHEN ISNULL((SELECT TOP 1 numState FROM fn_getOPPAddressDetails(OpportunityMaster.numOppId,OpportunityMaster.numDomainID,2)),0) > 0 
																				THEN
																					CASE 
																						WHEN (SELECT COUNT(*) FROM dbo.State WHERE numDomainID=@numDomainId AND numStateID =  (SELECT TOP 1 numState FROM fn_getOPPAddressDetails(OpportunityMaster.numOppId,OpportunityMaster.numDomainID,2)) AND numShippingZone=@numShippingZone) > 0 
																						THEN 1
																						ELSE 0
																					END
																				ELSE 0 
																			END)' ELSE '' END),
								@vcCustomSearchValue,(CASE WHEN @bitGroupByOrder = 1 THEN ' GROUP BY OpportunityMaster.numOppId,OpportunityBizDocs.numOppBizDocsId,OpportunityBizDocs.monAmountPaid' ELSE '' END));

		EXEC sp_executesql @vcSQL, N'@numDomainID NUMERIC(18,0), @vcOrderStauts VARCHAR(MAX), @vcFilterValue VARCHAR(MAX), @tintFilterBy TINYINT, @numViewID NUMERIC(18,0), @tintPackingViewMode TINYINT, @numShippingServiceItemID NUMERIC(18,0), @numShippingZone NUMERIC(18,0), @numDefaultSalesShippingDoc NUMERIC(18,0), @tintInvoicingType TINYINT, @tintPackingMode TINYINT, @bitRemoveBO BIT,@numWarehouseID NUMERIC(18,0)', @numDomainID, @vcOrderStauts, @vcFilterValue, @tintFilterBy, @numViewID, @tintPackingViewMode, @numShippingServiceItemID, @numShippingZone, @numDefaultSalesShippingDoc, @tintInvoicingType, @tintPackingMode, @bitRemoveBO,@numWarehouseID;
	END

	--PRINT @numDomainID 
	--PRINT @vcOrderStauts 
	--PRINT  @vcFilterValue 
	--PRINT @tintFilterBy 
	--PRINT  @numViewID 
	--PRINT  @tintPackingViewMode 
	--PRINT  @numShippingServiceItemID 
	--PRINT @numShippingZone 
	--PRINT  @numDefaultSalesShippingDoc 
	--PRINT  @tintInvoicingType 
	--PRINT  @tintPackingMode 
	--PRINT @bitRemoveBO 
	PRINT @numWarehouseID
	--SELECT * FROM #TEMPMSRecords
	SET @I = @I +1
	END
	
	SET @numTotalRecords = (SELECT COUNT(*) FROM #TEMPMSRecords) 
END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_UpdateOpportunityDealStatusConclusion')
DROP PROCEDURE USP_UpdateOpportunityDealStatusConclusion
GO
CREATE PROCEDURE [dbo].[USP_UpdateOpportunityDealStatusConclusion] 
( 
 @numDomainID NUMERIC(9),
 @numOppId NUMERIC(9),
 @tintOppStatus AS INT = 0,
 @lngPConclAnalysis NUMERIC(18,0)=0,
 @numUserCntID NUMERIC(18,0)=0
)
AS 
BEGIN
	IF(@tintOppStatus>0)
	BEGIN
		DECLARE @bitViolatePrice BIT
		DECLARE @intPendingApprovePending INT
		DECLARE @intExecuteDiv INT=0
		DECLARE @numDivisionID NUMERIC=0
		DECLARE @tintCommitAllocation TINYINT

		SELECT @tintCommitAllocation=ISNULL(tintCommitAllocation,1) FROM Domain WHERE numDomainId=@numDomainId
		
		IF EXISTS (SELECT numApprovalRuleID FROM ApprovalProcessItemsClassification WHERE numDomainId=@numDomainID AND ISNULL(bitMarginPriceViolated,0) = 1)
		BEGIN
			SET @bitViolatePrice = 1
		END
		SET @intPendingApprovePending=(SELECT COUNT(*) FROM OpportunityItems WHERE numOppId=@numOppId AND ISNULL(bitItemPriceApprovalRequired,0)=1)

		IF(@intPendingApprovePending > 0 AND @bitViolatePrice=1 AND @tintOppStatus='1')
		BEGIN
			SET @intExecuteDiv=1
		END

		IF ISNULL(@intExecuteDiv,0)=0
		BEGIN
			DECLARE @tintOldOppStatus AS TINYINT
			SELECT @tintOldOppStatus=tintOppStatus FROM OpportunityMaster WHERE numOppID=@numOppID              

			IF @tintOldOppStatus=0 AND @tintOppStatus='1' --Open to Won
			BEGIN
				SELECT @numDivisionID=numDivisionID FROM OpportunityMaster WHERE numOppID=@numOppID   
					
				DECLARE @tintCRMType AS TINYINT      
				SELECT @tintCRMType=tintCRMType FROM divisionmaster WHERE numDivisionID =@numDivisionID 

				-- Promote Lead to Account when Sales/Purchase Order is created against it
				IF @tintCRMType=0 --Lead & Order
				BEGIN        
					UPDATE 
						divisionmaster 
					SET 
						tintCRMType=2
						,bintLeadProm=GETUTCDATE()
						,bintLeadPromBy=@numUserCntID
						,bintProsProm=GETUTCDATE()
						,bintProsPromBy=@numUserCntID        
					WHERE 
						numDivisionID=@numDivisionID        
				END
				--Promote Prospect to Account
				ELSE IF @tintCRMType=1 
				BEGIN        
					UPDATE 
						divisionmaster 
					SET 
						tintCRMType=2
						,bintProsProm=GETUTCDATE()
						,bintProsPromBy=@numUserCntID        
					WHERE 
						numDivisionID=@numDivisionID        
				END    

				IF @tintCommitAllocation=1
				BEGIN
		 			EXEC USP_UpdatingInventoryonCloseDeal @numOppID,0,@numUserCntID
				END
				UPDATE OpportunityMaster SET bintOppToOrder=GETUTCDATE() WHERE numOppId=@numOppId and numDomainID=@numDomainID
				UPDATE OpportunityItems SET bitRequiredWarehouseCorrection=NULL WHERE numOppId=@numOppId
				EXEC USP_OpportunityItems_CheckWarehouseMapping @numDomainID,@numOppId
			END
			ELSE IF (@tintOppStatus='2') -- Win to Lost
			BEGIN
				UPDATE OpportunityMaster SET dtDealLost=GETUTCDATE() WHERE numOppId=@numOppId and numDomainID=@numDomainID
			END
				
			IF ((@tintOldOppStatus=1 and @tintOppStatus='2') or (@tintOldOppStatus=1 and @tintOppStatus='0')) AND @tintCommitAllocation=1 --Won to Lost or Won to Open
					EXEC USP_RevertDetailsOpp @numOppID,0,0,@numUserCntID 

			UPDATE OpportunityMaster SET tintOppStatus = @tintOppStatus WHERE numOppId=@numOppID
				
		END
		--UPDATE 
		--	OpportunityMaster
		--SET
		--	tintOppStatus=@tintOppStatus
		--WHERE
		--	numDomainId=@numDomainID AND
		--	numOppId=@numOppId
	END
	IF(@lngPConclAnalysis>0)
	BEGIN
		UPDATE 
			OpportunityMaster
		SET
			lngPConclAnalysis=@lngPConclAnalysis
		WHERE
			numDomainId=@numDomainID AND
			numOppId=@numOppId
	END
END
GO

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_eChannelHub_GetAsHtml')
DROP PROCEDURE USP_eChannelHub_GetAsHtml
GO
CREATE PROCEDURE [dbo].[USP_eChannelHub_GetAsHtml]
	@numDomainID NUMERIC(18,0)
AS
BEGIN	
	DECLARE @vcMarketplaces VARCHAR(MAX)

	SELECT @vcMarketplaces=ISNULL(vcMarketplaces,'') FROM Domain WHERE numDOmainID=@numDomainID


	DECLARE @vchtml VARCHAR(MAX) = '<div class="row">'
	SET @vchtml = CONCAT(@vchtml,'<div class="col-xs-12 col-md-6"><b>Marketplaces</b><hr style="margin-top:6px;margin-bottom:10px;border-color:#ddd;"/>')
	SET @vchtml = CONCAT(@vchtml,'<div class="row">')
	SET @vchtml = CONCAT(@vchtml,STUFF((SELECT
												CONCAT('<div class="col-sm-12 col-md-4"><div class="checkbox-inline"><input type="checkbox"',(CASE WHEN ISNULL(TEMP.Id,0) > 0 THEN ' checked' ELSE '' END),' id="chkMarketplace',eChannelHub.ID,'" /><label style="font-weight:normal;">',vcMarketplace,'</label></div></div>')
											FROM 
												eChannelHub 
											LEFT JOIN
											(
												SELECT
													Id
												FROM
													dbo.SplitIDs(@vcMarketplaces,',')
											) TEMP
											ON
												eChannelHub.ID = TEMP.Id
											WHERE 
												vcType='Marketplaces'
											ORDER BY
												vcMarketplace
											FOR XML PATH(''), TYPE).value('.', 'varchar(max)'),1,0,''))
	SET @vchtml = CONCAT(@vchtml,'</div>')
	SET @vchtml = CONCAT(@vchtml,'</div>')
	SET @vchtml = CONCAT(@vchtml,'<div class="col-xs-12 col-md-3"><b>Comparison Shopping</b><hr style="margin-top:6px;margin-bottom:10px;border-color:#ddd;"/>')
	SET @vchtml = CONCAT(@vchtml,'<div class="row">')
	SET @vchtml = CONCAT(@vchtml,STUFF((SELECT
												CONCAT('<div class="col-sm-12"><div class="checkbox-inline"><input type="checkbox"',(CASE WHEN ISNULL(TEMP.Id,0) > 0 THEN ' checked="checked"' ELSE '' END),' id="chkMarketplace',eChannelHub.ID,'" /><label style="font-weight:normal;">',vcMarketplace,'</label></div></div>')
											FROM 
												eChannelHub 
											LEFT JOIN
											(
												SELECT
													Id
												FROM
													dbo.SplitIDs(@vcMarketplaces,',')
											) TEMP
											ON
												eChannelHub.ID = TEMP.Id
											WHERE 
												vcType='Comparison Shopping'
											ORDER BY
												vcMarketplace
											FOR XML PATH(''), TYPE).value('.', 'varchar(max)'),1,0,''))
	SET @vchtml = CONCAT(@vchtml,'</div>')
	SET @vchtml = CONCAT(@vchtml,'</div>')
	SET @vchtml = CONCAT(@vchtml,'<div class="col-xs-12 col-md-3"><b>3rd Party Shopping Carts</b><hr style="margin-top:6px;margin-bottom:10px;border-color:#ddd;"/>')
	SET @vchtml = CONCAT(@vchtml,'<div class="row">')
	SET @vchtml = CONCAT(@vchtml,STUFF((SELECT
												CONCAT('<div class="col-sm-12"><div class="checkbox-inline"><input type="checkbox"',(CASE WHEN ISNULL(TEMP.Id,0) > 0 THEN ' checked="checked"' ELSE '' END),' id="chkMarketplace',eChannelHub.ID,'" /><label style="font-weight:normal;">',vcMarketplace,'</label></div></div>')
											FROM 
												eChannelHub 
											LEFT JOIN
											(
												SELECT
													Id
												FROM
													dbo.SplitIDs(@vcMarketplaces,',')
											) TEMP
											ON
												eChannelHub.ID = TEMP.Id
											WHERE 
												vcType='3rd Party Shopping Carts'
											ORDER BY
												vcMarketplace
											FOR XML PATH(''), TYPE).value('.', 'varchar(max)'),1,0,''))
	SET @vchtml = CONCAT(@vchtml,'</div>')
	SET @vchtml = CONCAT(@vchtml,'</div>')
	SET @vchtml = CONCAT(@vchtml,'</div>')

	SELECT @vchtml
END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetOrderItemsForAutoCalculatedShippingRate')
DROP PROCEDURE dbo.USP_GetOrderItemsForAutoCalculatedShippingRate
GO

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_GetOrderItemsForAutoCalculatedShippingRate]
(
	@numDomainID NUMERIC(18,0)
	,@numOppID NUMERIC(18,0)
	,@numWarehouseID NUMERIC(18,0)
	,@dtShipByDate DATE
)
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	-- GET FROM ADDRESS
	SELECT
		AD.vcStreet
		,AD.vcCity
		,ISNULL((SELECT vcStateCode FROM ShippingStateMaster WHERE numShipCompany=91 AND vcStateName=dbo.fn_GetState(AD.numState)),'') vcStateFedex
		,ISNULL((SELECT vcStateCode FROM ShippingStateMaster WHERE numShipCompany=88 AND vcStateName=dbo.fn_GetState(AD.numState)),'') vcStateUPS
		,ISNULL((SELECT vcStateCode FROM ShippingStateMaster WHERE numShipCompany=90 AND vcStateName=dbo.fn_GetState(AD.numState)),'') vcStateUSPS
		,AD.vcPostalCode AS vcZipCode
		,ISNULL((SELECT vcCountryCode FROM ShippingCountryMaster WHERE numShipCompany=91 AND vcCountryName=dbo.fn_GetListItemName(AD.numCountry)),'') vcCountryFedex
		,ISNULL((SELECT vcCountryCode FROM ShippingCountryMaster WHERE numShipCompany=91 AND vcCountryName=dbo.fn_GetListItemName(AD.numCountry)),'') vcCountryUPS
		,ISNULL((SELECT vcCountryCode FROM ShippingCountryMaster WHERE numShipCompany=91 AND vcCountryName=dbo.fn_GetListItemName(AD.numCountry)),'') vcCountryUSPS
	FROM
		Warehouses W
	LEFT JOIN
		AddressDetails AD
	ON
		W.numAddressID = AD.numAddressID
	WHERE
		W.numDomainID = @numDomainID
		AND W.numWareHouseID = @numWarehouseID

	-- GET TO ADDRESS
	SELECT
		vcStreet
		,vcCity
		,ISNULL((SELECT vcStateCode FROM ShippingStateMaster WHERE numShipCompany=91 AND vcStateName=dbo.fn_GetState(vcState)),'') vcStateFedex
		,ISNULL((SELECT vcStateCode FROM ShippingStateMaster WHERE numShipCompany=88 AND vcStateName=dbo.fn_GetState(vcState)),'') vcStateUPS
		,ISNULL((SELECT vcStateCode FROM ShippingStateMaster WHERE numShipCompany=90 AND vcStateName=dbo.fn_GetState(vcState)),'') vcStateUSPS
		,vcZipCode
		,ISNULL((SELECT vcCountryCode FROM ShippingCountryMaster WHERE numShipCompany=91 AND vcCountryName=dbo.fn_GetListItemName(vcCountry)),'') vcCountryFedex
		,ISNULL((SELECT vcCountryCode FROM ShippingCountryMaster WHERE numShipCompany=88 AND vcCountryName=dbo.fn_GetListItemName(vcCountry)),'') vcCountryUPS
		,ISNULL((SELECT vcCountryCode FROM ShippingCountryMaster WHERE numShipCompany=90 AND vcCountryName=dbo.fn_GetListItemName(vcCountry)),'') vcCountryUSPS
	FROM
		dbo.fn_GetShippingReportAddress(2,@numDomainId,@numOppID)

	-- GET CONTAINERS/PACKAGES
	DECLARE @TempContiner TABLE
	(
		ID INT IDENTITY(1,1)
		,fltContainerWeight FLOAT
		,fltContainerHeight FLOAT
		,fltContainerWidth FLOAT
		,fltContainerLength FLOAT
		,numQty FLOAT
	)

	DECLARE @TEMPItems TABLE
	(
		ID INT IDENTITY(1,1)
		,numContainer NUMERIC(18,0)
		,numNoItemIntoContainer FLOAT
		,fltItemWeight FLOAT
		,fltItemHeight FLOAT
		,fltItemWidth FLOAT
		,fltItemLength FLOAT
		,fltContainerWeight FLOAT
		,fltContainerHeight FLOAT
		,ltContainerWidth FLOAT
		,fltContainerLength FLOAT
		,numTotalQty FLOAT
	)

	INSERT INTO 
		@TEMPItems
	SELECT
		T1.numContainer
		,T1.numNoItemIntoContainer
		,T1.fltItemWeight
		,T1.fltItemHeight
		,T1.fltItemWidth
		,T1.fltItemLength
		,ISNULL(IContainer.fltWeight,0) fltContainerWeight
		,ISNULL(IContainer.fltHeight,0) fltContainerHeight
		,ISNULL(IContainer.fltWidth,0) fltContainerWidth
		,ISNULL(IContainer.fltLength,0) fltContainerLength
		,T1.numTotalQty
	FROM
	(
		SELECT 
			I.numContainer
			,(CASE WHEN ISNULL(I.numContainer,0) > 0 THEN I.numNoItemIntoContainer ELSE SUM(OI.numUnitHour) END) AS numNoItemIntoContainer
			,ISNULL(I.fltWeight,0) fltItemWeight
			,ISNULL(I.fltHeight,0) fltItemHeight
			,ISNULL(I.fltWidth,0) fltItemWidth
			,ISNULL(I.fltLength,0) fltItemLength
			,SUM(OI.numUnitHour) numTotalQty
		FROM 
			OpportunityMaster OM
		INNER JOIN
			OpportunityItems OI
		ON
			OM.numOppID = OI.numOppID
		INNER JOIN
			WareHouseItems WI
		ON
			OI.numWarehouseItmsID = WI.numWareHouseItemID
		INNER JOIN 
			Item AS I
		ON 
			OI.numItemCode = I.numItemCode
		WHERE
			OM.numOppId=@numOppID
			AND WI.numWareHouseID = @numWarehouseID
			AND ISNULL(OI.ItemReleaseDate,CAST(OM.dtReleaseDate AS DATE)) = @dtShipByDate
			AND ISNULL(I.bitContainer,0) = 0
		GROUP BY
			I.numContainer
			,I.numNoItemIntoContainer
			,I.fltWeight
			,I.fltHeight
			,I.fltWidth
			,I.fltLength
	) AS T1
	LEFT JOIN
		Item IContainer
	ON
		T1.numContainer = IContainer.numItemCode

	DECLARE @i AS INT = 1
	DECLARE @iCount AS INT
	DECLARE @numContainer NUMERIC(18,0)
	DECLARE @numNoItemIntoContainer FLOAT
	DECLARE @fltItemWeight FLOAT
	DECLARE @fltItemHeight FLOAT
	DECLARE @fltItemWidth FLOAT
	DECLARE @fltItemLength FLOAT
	DECLARE @fltContainerWeight FLOAT
	DECLARE @fltContainerHeight FLOAT
	DECLARE @ltContainerWidth FLOAT
	DECLARE @fltContainerLength FLOAT
	DECLARE @numTotalQty FLOAT

	SELECT @iCount = COUNT(*) FROM @TEMPItems

	WHILE @i <= @iCount
	BEGIN
		SELECT 
			@numContainer=numContainer
			,@numNoItemIntoContainer=numNoItemIntoContainer
			,@fltItemWeight=fltItemWeight
			,@fltItemHeight=fltItemHeight
			,@fltItemWidth=fltItemWidth
			,@fltItemLength=fltItemLength
			,@fltContainerWeight=fltContainerWeight
			,@fltContainerHeight=fltContainerHeight
			,@ltContainerWidth=ltContainerWidth
			,@fltContainerLength=fltContainerLength
			,@numTotalQty=numTotalQty
		FROM
			@TEMPItems
		WHERE
			ID=@i

		IF @numTotalQty <= @numNoItemIntoContainer
		BEGIN
			INSERT INTO @TempContiner
			(
				fltContainerWeight
				,fltContainerHeight
				,fltContainerWidth
				,fltContainerLength
				,numQty
			)
			VALUES
			(
				ISNULL(@fltContainerWeight,0) + (ISNULL(@numTotalQty,0) * ISNULL(@fltItemWeight,0))
				,@fltContainerHeight
				,@ltContainerWidth
				,@fltContainerLength
				,@numTotalQty
			)
		END
		ELSE
		BEGIN
			WHILE @numTotalQty > @numNoItemIntoContainer
			BEGIN
				INSERT INTO @TempContiner
				(
					fltContainerWeight
					,fltContainerHeight
					,fltContainerWidth
					,fltContainerLength
					,numQty
				)
				VALUES
				(
					ISNULL(@fltContainerWeight,0) + (ISNULL(@numNoItemIntoContainer,0) * ISNULL(@fltItemWeight,0))
					,@fltContainerHeight
					,@ltContainerWidth
					,@fltContainerLength
					,@numNoItemIntoContainer
				)
			
				SET @numTotalQty = @numTotalQty - @numNoItemIntoContainer
			END

			IF ISNULL(@numTotalQty,0) > 0
			BEGIN
				INSERT INTO @TempContiner
				(
					fltContainerWeight
					,fltContainerHeight
					,fltContainerWidth
					,fltContainerLength
					,numQty
				)
				VALUES
				(
					ISNULL(@fltContainerWeight,0) + (ISNULL(@numTotalQty,0) * ISNULL(@fltItemWeight,0))
					,@fltContainerHeight
					,@ltContainerWidth
					,@fltContainerLength
					,@numTotalQty
				)
			END
		END


		SET @i = @i + 1
	END

	SELECT * FROm @TempContiner
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_MassSalesFulfillmentConfiguration_Get')
DROP PROCEDURE dbo.USP_MassSalesFulfillmentConfiguration_Get
GO
CREATE PROCEDURE [dbo].[USP_MassSalesFulfillmentConfiguration_Get]
(
	@numDomainID NUMERIC(18,0)
)
AS 
BEGIN
	SELECT * FROM MassSalesFulfillmentConfiguration WHERE numDomainID=@numDomainID
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_MassSalesFulfillmentConfiguration_GetColumnValue')
DROP PROCEDURE USP_MassSalesFulfillmentConfiguration_GetColumnValue
GO
CREATE PROCEDURE [dbo].[USP_MassSalesFulfillmentConfiguration_GetColumnValue]
(
    @numDomainID AS NUMERIC(18,0),
	@vcDbColumnName AS VARCHAR(300)
)
AS 
BEGIN
	IF @vcDbColumnName = 'bitAutoWarehouseSelection'
	BEGIN
		SELECT ISNULL(bitAutoWarehouseSelection,0) AS bitAutoWarehouseSelection FROM MassSalesFulfillmentConfiguration WHERE numDomainId=@numDomainID
	END
	ELSE IF @vcDbColumnName = 'bitBOOrderStatus'
	BEGIN
		SELECT ISNULL(bitBOOrderStatus,0) AS bitBOOrderStatus FROM MassSalesFulfillmentConfiguration WHERE numDomainId=@numDomainID
	END
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_MassSalesFulfillmentShippingConfig_Get')
DROP PROCEDURE USP_MassSalesFulfillmentShippingConfig_Get
GO
CREATE PROCEDURE [dbo].[USP_MassSalesFulfillmentShippingConfig_Get]
(
	@numDomainID NUMERIC(18,0)
	,@numOrderSource NUMERIC(18,0)
	,@tintSourceType TINYINT
)
AS
BEGIN
	SELECT 
		* 
	FROM 
		MassSalesFulfillmentShippingConfig 
	WHERE 
		numDomainID=@numDomainID 
		AND numOrderSource=@numOrderSource 
		AND tintSourceType=@tintSourceType
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_MassSalesFulfillmentShippingConfig_Save')
DROP PROCEDURE USP_MassSalesFulfillmentShippingConfig_Save
GO
CREATE PROCEDURE [dbo].[USP_MassSalesFulfillmentShippingConfig_Save]
(
	@numDomainID NUMERIC(18,0)
	,@numOrderSource NUMERIC(18,0)
	,@tintSourceType TINYINT
	,@tintType TINYINT
	,@vcPriorities VARCHAR(50)
	,@numShipVia NUMERIC(18,0)
	,@numShipService NUMERIC(18,0)
	,@bitOverride BIT
	,@fltWeight FLOAT
	,@numShipViaOverride NUMERIC(18,0)
)
AS
BEGIN
	IF EXISTS (SELECT ID FROM MassSalesFulfillmentShippingConfig WHERE numDomainID=@numDomainID AND numOrderSource=@numOrderSource AND tintSourceType=@tintSourceType)
	BEGIN
		UPDATE
			MassSalesFulfillmentShippingConfig
		SET
			tintType=@tintType
			,vcPriorities=@vcPriorities
			,numShipVia=@numShipVia
			,numShipService=@numShipService
			,bitOverride=@bitOverride
			,fltWeight=@fltWeight
			,numShipViaOverride=@numShipViaOverride
		WHERE
			numDomainID=@numDomainID 
			AND numOrderSource=@numOrderSource 
			AND tintSourceType=@tintSourceType
	END
	ELSE
	BEGIN
		INSERT INTO MassSalesFulfillmentShippingConfig
		(
			numDomainID
			,numOrderSource
			,tintSourceType
			,tintType
			,vcPriorities
			,numShipVia
			,numShipService
			,bitOverride
			,fltWeight
			,numShipViaOverride
		)
		VALUES
		(
			@numDomainID
			,@numOrderSource
			,@tintSourceType
			,@tintType
			,@vcPriorities
			,@numShipVia
			,@numShipService
			,@bitOverride
			,@fltWeight
			,@numShipViaOverride
		)
	END
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_MassSalesFulfillmentWM_Delete')
DROP PROCEDURE dbo.USP_MassSalesFulfillmentWM_Delete
GO
CREATE PROCEDURE [dbo].[USP_MassSalesFulfillmentWM_Delete]
(
	@numDomainID NUMERIC(18,0)
	,@numID NUMERIC(18,0)
)
AS 
BEGIN
	DELETE FROM MassSalesFulfillmentWM WHERE numDomainID=@numDomainID AND ID=@numID
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_MassSalesFulfillmentWM_Get')
DROP PROCEDURE dbo.USP_MassSalesFulfillmentWM_Get
GO
CREATE PROCEDURE [dbo].[USP_MassSalesFulfillmentWM_Get]
(
	@numDomainID NUMERIC(18,0)
	,@numPageIndex INT
	,@numPageSize INT
	,@vcSources VARCHAR(MAX)
	,@vcCountries VARCHAR(MAX)
	,@vcStates VARCHAR(MAX)
)
AS 
BEGIN
	SELECT
		COUNT(*) OVER() numTotalRecords
		,MSFWM.ID
		,(CASE 
			WHEN ISNULL(numOrderSource,0) = 0 AND tintSourceType=1 THEN 'Internal Order'
			WHEN ISNULL(numOrderSource,0) > 0 AND tintSourceType=1 THEN ISNULL(LDOS.vcData,'-') 
			WHEN tintSourceType=2 THEN ISNULL(S.vcSiteName,'-') 
			ELSE '-'
		END) vcOrderSource
		,ISNULL(LDCountry.vcData,'-') vcCountry
		,STUFF((SELECT 
					CONCAT(', ',(CASE WHEN LEN(ISNULL(LDS.vcAbbreviations,'')) > 0 THEN LDS.vcAbbreviations ELSE ISNULL(LDS.vcState,'') END))
				FROM 
					MassSalesFulfillmentWMState MSFWMS
				INNER JOIN
					[State] LDS
				ON
					LDS.numDomainID = @numDomainID
					AND MSFWMS.numStateID = LDS.numStateID
				WHERE 
					MSFWMS.numMSFWMID = MSFWM.ID
				ORDER BY 
					(CASE WHEN LEN(ISNULL(LDS.vcAbbreviations,'')) > 0 THEN LDS.vcAbbreviations ELSE ISNULL(LDS.vcState,'') END)
				FOR XML PATH('')),1,1,'') vcStates
		,STUFF((SELECT 
					CONCAT(', (',intOrder,') ',ISNULL(vcWareHouse,'-'))
				FROM 
					MassSalesFulfillmentWP MSFWWP
				INNER JOIN
					Warehouses W
				ON
					W.numDomainID = @numDomainID
					AND MSFWWP.numWareHouseID = W.numWareHouseID
				WHERE 
					MSFWWP.numMSFWMID = MSFWM.ID
				ORDER BY 
					MSFWWP.intOrder
				FOR XML PATH('')),1,1,'') vcWarehouses
	FROM
		MassSalesFulfillmentWM MSFWM
	LEFT JOIN
		ListDetails LDOS
	ON
		LDOS.numListID = 9
		AND MSFWM.numOrderSource = LDOS.numListItemID
		AND MSFWM.tintSourceType = 1
	LEFT JOIN
		Sites S
	ON
		S.numDomainID = @numDomainID
		AND MSFWM.numOrderSource = S.numSiteID
		AND MSFWM.tintSourceType = 2
	LEFT JOIN
		ListDetails LDCountry
	ON
		LDCountry.numListID = 40
		AND MSFWM.numCountryID = LDCountry.numListItemID
	WHERE
		MSFWM.numDomainID=@numDomainID
		AND (LEN(ISNULL(@vcSources,'')) = 0 OR CONCAT(numOrderSource,'~',tintSourceType) IN (SELECT OutParam FROM dbo.SplitString(@vcSources,',')))
		AND (LEN(ISNULL(@vcCountries,'')) = 0 OR numCountryID IN (SELECT Id FROM dbo.SplitIDs(@vcCountries,',')))
		AND (LEN(ISNULL(@vcStates,'')) = 0 OR EXISTS (SELECT MSFWMS.ID FROM MassSalesFulfillmentWMState MSFWMS WHERE MSFWMS.numMSFWMID=MSFWM.ID AND MSFWMS.numStateID IN (SELECT Id FROM dbo.SplitIDs(@vcStates,','))))
	ORDER BY
		LDOS.vcData
	OFFSET (@numPageIndex-1) ROWS FETCH NEXT @numPageSize ROWS ONLY
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_MassSalesFulfillmentWM_GetWarehousePriority')
DROP PROCEDURE USP_MassSalesFulfillmentWM_GetWarehousePriority
GO
CREATE PROCEDURE [dbo].[USP_MassSalesFulfillmentWM_GetWarehousePriority]
(
    @numDomainID NUMERIC(18,0)
	,@numOrderSource NUMERIC(18,0)
	,@tintSourceType TINYINT
	,@numCountryID NUMERIC(18,0)
	,@numStateID NUMERIC(18,0)
)
AS 
BEGIN
	SELECT
		MassSalesFulfillmentWP.numWarehouseID
	FROM
		MassSalesFulfillmentWM
	INNER JOIN
		MassSalesFulfillmentWMState
	ON
		MassSalesFulfillmentWM.ID = MassSalesFulfillmentWMState.numMSFWMID
	INNER JOIN
		MassSalesFulfillmentWP
	ON
		MassSalesFulfillmentWM.ID = MassSalesFulfillmentWP.numMSFWMID
	WHERE
		MassSalesFulfillmentWM.numDomainID=@numDomainID
		AND MassSalesFulfillmentWM.numOrderSource=@numOrderSource
		AND MassSalesFulfillmentWM.tintSourceType=@tintSourceType
		AND MassSalesFulfillmentWM.numCountryID=@numCountryID
		AND MassSalesFulfillmentWMState.numStateID = @numStateID
	ORDER BY
		MassSalesFulfillmentWP.intOrder
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_MassSalesFulfillmentWM_Save')
DROP PROCEDURE dbo.USP_MassSalesFulfillmentWM_Save
GO
CREATE PROCEDURE [dbo].[USP_MassSalesFulfillmentWM_Save]
(
	@numDomainID NUMERIC(18,0)
	,@numOrderSource NUMERIC(18,0)
	,@tintSourceType TINYINT
	,@numCountryID NUMERIC(18,0)
	,@vcStateIDs VARCHAR(MAX)
	,@vcWarehousePriorities VARCHAR(MAX)
)
AS 
BEGIN
	DECLARE @TEMP TABLE
	(
		numStateID NUMERIC(18,0)
	)

	DECLARE @TEMPWarehouse TABLE
	(
		ID INT IDENTITY(1,1)
		,numWarehouseID NUMERIC(18,0)
	)

	INSERT INTO @TEMP
	(
		numStateID
	)
	SELECT
		Id
	FROM
		dbo.SplitIDs(@vcStateIDs,',')

	INSERT INTO @TEMPWarehouse
	(
		numWarehouseID
	)
	SELECT
		Id
	FROM
		dbo.SplitIDs(@vcWarehousePriorities,',')


	IF EXISTS (SELECT 
					MassSalesFulfillmentWMState.ID
				FROM 
					MassSalesFulfillmentWM
				INNER JOIN
					MassSalesFulfillmentWMState
				ON
					MassSalesFulfillmentWM.ID=MassSalesFulfillmentWMState.numMSFWMID
				WHERE 
					numDomainID = @numDomainID 
					AND numOrderSource=@numOrderSource
					AND numStateID IN (SELECT numStateID FROM @TEMP))
	BEGIN
		RAISERROR('DUPLICATE_STATE',16,1)
		RETURN
	END
	ELSE
	BEGIN
		INSERT INTO MassSalesFulfillmentWM
		(
			numDomainID
			,numOrderSource
			,numCountryID
			,tintSourceType
		)
		VALUES
		(
			@numDomainID
			,@numOrderSource
			,@numCountryID
			,@tintSourceType
		)
	
		DECLARE @ID NUMERIC(18,0)
		SET @ID = SCOPE_IDENTITY()

		INSERT INTO MassSalesFulfillmentWMState
		(
			numMSFWMID
			,numStateID
		)
		SELECT
			@ID
			,numStateID
		FROM
			@TEMP

		INSERT INTO MassSalesFulfillmentWP
		(
			numMSFWMID
			,numWarehouseID
			,intOrder
		)
		SELECT
			@ID
			,numWarehouseID
			,ID
		FROM
			@TEMPWarehouse
	END
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityItems_CheckWarehouseMapping')
DROP PROCEDURE USP_OpportunityItems_CheckWarehouseMapping
GO
CREATE PROCEDURE [dbo].[USP_OpportunityItems_CheckWarehouseMapping]
(
	@numDomainID NUMERIC(18,0)
	,@numOppID NUMERIC(18,0)
)
AS
BEGIN
	IF EXISTS (SELECT * FROM MassSalesFulfillmentConfiguration WHERE numDomainID=@numDomainID AND ISNULL(bitAutoWarehouseSelection,0)=1)
	BEGIN
		DECLARE @numOrderSource NUMERIC(18,0)
		DECLARE @tintSourceType TINYINT
		DECLARE @numShipToState NUMERIC(18,0)
		DECLARE @numShipToCountry NUMERIC(18,0)

		SELECT
			@numOrderSource = ISNULL(OpportunityMaster.tintSource,0)
			,@tintSourceType = ISNULL(OpportunityMaster.tintSourceType,0)
		FROM
			OpportunityMaster
		WHERE
			numDomainId=@numDomainID
			AND numOppId=@numOppID

		SELECT
			@numShipToState=ISNULL(numState,0)
			,@numShipToCountry=ISNULL(numCountry,0)
		FROM
			fn_getOPPAddressDetails(@numOppID,@numDomainID,2)

		DECLARE @bitBOOrderStatus BIT
		SELECT @bitBOOrderStatus=ISNULL(bitBOOrderStatus,0) FROM MassSalesFulfillmentConfiguration WHERE numDomainID=@numDomainID

		DECLARE @TEMP TABLE
		(
			numWarehouseID NUMERIC(18,0)
		)

		INSERT INTO @TEMP
		(
			numWarehouseID
		)
		SELECT  
			MSFWP.numWarehouseID
		FROM 
			MassSalesFulfillmentWM MSFWM
		INNER JOIN
			MassSalesFulfillmentWMState MSFWMS
		ON
			MSFWM.ID = MSFWMS.numMSFWMID
		INNER JOIN
			MassSalesFulfillmentWP MSFWP
		ON
			MSFWM.ID = MSFWP.numMSFWMID
		WHERE 
			numDomainID=@numDomainID 
			AND numOrderSource=@numOrderSource 
			AND tintSourceType=@tintSourceType 
			AND numCountryID=@numShipToCountry
			AND numStateID=@numShipToState
		ORDER BY
			intOrder

		IF (SELECT COUNT(*) FROM @TEMP) > 0
		BEGIN
			DECLARE @tintCommitAllocation AS TINYINT	
			SELECT @tintCommitAllocation=ISNULL(tintCommitAllocation,1) FROM Domain WHERE numDomainId=@numDomainId

			UPDATE
				OI
			SET
				bitRequiredWarehouseCorrection = 1
			FROM
				OpportunityItems OI
			INNER JOIN
				Item I
			ON
				OI.numItemCode = I.numItemCode
			INNER JOIN 
				WareHouseItems WI
			ON
				OI.numWarehouseItmsID = WI.numWareHouseItemID
			WHERE
				numOppId=@numOppID 
				AND ISNULL(bitDropShip,0)=0
				AND (ISNULL(bitWorkOrder,0)=0 OR (ISNULL(bitWorkOrder,0)=1 AND ISNULL(numUnitHour,0) - ISNULL(numWOQty,0) > 0))
				AND bitRequiredWarehouseCorrection IS NULL
				AND 1 = (CASE 
							WHEN @bitBOOrderStatus = 0 
							THEN (CASE WHEN WI.numWareHouseID NOT IN (SELECT T.numWarehouseID FROM @TEMP T) THEN 1 ELSE 0 END)
							ELSE (CASE 
									WHEN WI.numWareHouseID NOT IN (SELECT T.numWarehouseID FROM @TEMP T)
											OR 1 = (CASE 
														WHEN @tintCommitAllocation=1 
														THEN (CASE 
																WHEN ISNULL(I.bitKitParent,0) = 1 
																THEN (CASE 
																		WHEN EXISTS(SELECT OKI.numOppChildItemID FROM OpportunityKitItems OKI INNER JOIN WareHouseItems WIInner ON OKI.numWareHouseItemId=WIInner.numWareHouseItemID WHERE OKI.numOppId=@numOppID AND OKI.numOppItemID=OI.numoppitemtCode AND ISNULL(WIInner.numBackOrder,0) > 0)
																				OR EXISTS(SELECT OKCI.numOppKitChildItemID FROM OpportunityKitChildItems OKCI INNER JOIN WareHouseItems WIInner ON OKCI.numWareHouseItemId=WIInner.numWareHouseItemID WHERE OKCI.numOppId=@numOppID AND OKCI.numOppItemID=OI.numoppitemtCode AND ISNULL(WIInner.numBackOrder,0) > 0)
																		THEN 1
																		ELSE 0
																	END)
																ELSE (CASE WHEN ISNULL(WI.numBackOrder,0) > 0 THEN 1 ELSE 0 END)
															END) 
														ELSE (CASE 
																WHEN ISNULL(I.bitKitParent,0) = 1 
																THEN (CASE 
																		WHEN EXISTS(SELECT OKI.numOppChildItemID FROM OpportunityKitItems OKI INNER JOIN WareHouseItems WIInner ON OKI.numWareHouseItemId=WIInner.numWareHouseItemID WHERE OKI.numOppId=@numOppID AND OKI.numOppItemID=OI.numoppitemtCode AND ISNULL((SELECT SUM(WIInner.numOnHand) FROM WareHouseItems WIInner WHERE WIInner.numItemID = WIInner.numItemID AND WIInner.numWareHouseID=WIInner.numWareHouseID),0) < ISNULL(OKI.numQtyItemsReq,0))
																				OR EXISTS(SELECT OKCI.numOppKitChildItemID FROM OpportunityKitChildItems OKCI INNER JOIN WareHouseItems WIInner ON OKCI.numWareHouseItemId=WIInner.numWareHouseItemID WHERE OKCI.numOppId=@numOppID AND OKCI.numOppItemID=OI.numoppitemtCode AND ISNULL((SELECT SUM(WIInner.numOnHand) FROM WareHouseItems WIInner WHERE WIInner.numItemID = WIInner.numItemID AND WIInner.numWareHouseID=WIInner.numWareHouseID),0) < ISNULL(OKCI.numQtyItemsReq,0))
																		THEN 1
																		ELSE 0
																	END)
																ELSE (CASE WHEN ISNULL((SELECT SUM(WIInner.numOnHand) FROM WareHouseItems WIInner WHERE WIInner.numItemID = WI.numItemID AND WIInner.numWareHouseID=WI.numWareHouseID),0) < (ISNULL(numUnitHour,0) - ISNULL(numWOQty,0)) THEN 1 ELSE 0 END)
															END)
													END) 
									THEN 1 
									ELSE 0 
								END)
						END)
		END
		ELSE
		BEGIN
			UPDATE OpportunityItems SET bitRequiredWarehouseCorrection = 1 WHERE numOppId=@numOppID AND numWarehouseItmsID > 0 AND bitRequiredWarehouseCorrection IS NULL
		END

		UPDATE OpportunityItems SET bitRequiredWarehouseCorrection = 0 WHERE numOppId=@numOppID AND bitRequiredWarehouseCorrection IS NULL

		IF EXISTS (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId=@numOppID AND bitRequiredWarehouseCorrection = 1)
		BEGIN
			UPDATE OpportunityMaster SET numStatus=15448 WHERE numDomainId=@numDomainID AND numOppId=@numOppID
		END
	END
	ELSE
	BEGIN
		UPDATE OpportunityItems SET bitRequiredWarehouseCorrection = 0 WHERE numOppId=@numOppID AND bitRequiredWarehouseCorrection IS NULL
	END
END
GO

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityItems_UpdateWarehouseMapping')
DROP PROCEDURE USP_OpportunityItems_UpdateWarehouseMapping
GO
CREATE PROCEDURE [dbo].[USP_OpportunityItems_UpdateWarehouseMapping]
(
	@numDomainID NUMERIC(18,0)
	,@numOppID NUMERIC(18,0)
)
AS
BEGIN
	DECLARE @numOrderSource NUMERIC(18,0)
	DECLARE @tintSourceType TINYINT
	DECLARE @numShipToState NUMERIC(18,0)
	DECLARE @numShipToCountry NUMERIC(18,0)

	SELECT
		@numOrderSource = ISNULL(OpportunityMaster.tintSource,0)
		,@tintSourceType = ISNULL(OpportunityMaster.tintSourceType,0)
	FROM
		OpportunityMaster
	WHERE
		numDomainId=@numDomainID
		AND numOppId=@numOppID

	SELECT
		@numShipToState=ISNULL(numState,0)
		,@numShipToCountry=ISNULL(numCountry,0)
	FROM
		fn_getOPPAddressDetails(@numOppID,@numDomainID,2)

	DECLARE @TEMPWarehouse TABLE
	(
		ID INT IDENTITY(1,1)
		,numWarehouseID NUMERIC(18,0)
	)

	INSERT INTO @TEMPWarehouse
	(	
		numWarehouseID
	)
	SELECT  
		MSFWP.numWarehouseID
	FROM 
		MassSalesFulfillmentWM MSFWM
	INNER JOIN
		MassSalesFulfillmentWMState MSFWMS
	ON
		MSFWM.ID = MSFWMS.numMSFWMID
	INNER JOIN
		MassSalesFulfillmentWP MSFWP
	ON
		MSFWM.ID = MSFWP.numMSFWMID
	WHERE 
		numDomainID=@numDomainID 
		AND numOrderSource=@numOrderSource 
		AND tintSourceType=@tintSourceType 
		AND numCountryID=@numShipToCountry
		AND numStateID=@numShipToState
	ORDER BY
		intOrder

	IF EXISTS (SELECT ID FROM @TEMPWarehouse)
	BEGIN
		DECLARE @TEMPItems TABLE
		(
			ID INT IDENTITY(1,1)
			,numOppItemID NUMERIC(18,0)
			,numItemCode NUMERIC(18,0)
			,bitKitParent BIT
			,numWarehouseItemID NUMERIC(18,0)
			,numUnitHour FLOAT
		)

		INSERT INTO @TEMPItems
		(
			numOppItemID
			,numItemCode
			,bitKitParent
			,numWarehouseItemID
			,numUnitHour
		)
		SELECT
			OI.numoppitemtCode
			,OI.numItemCode
			,ISNULL(I.bitKitParent,0)
			,OI.numWarehouseItmsID
			,ISNULL(OI.numUnitHour,0) - ISNULL(OI.numWOQty,0)
		FROM
			OpportunityItems OI
		INNER JOIN
			Item I
		ON
			OI.numItemCode = I.numItemCode
		WHERE
			OI.numOppId = @numOppID
			AND ISNULL(OI.numWarehouseItmsID,0) > 0
			AND ISNULL(OI.bitDropShip,0) = 0

		DECLARE @i INT = 1
		DECLARE @iCount INT
		DECLARE @numOppItemID NUMERIC(18,0)
		DECLARE @numItemCode NUMERIC(18,0)
		DECLARE @bitKitParent BIT
		DECLARE @numUnitHour FLOAT
		DECLARE @numWarehouseItemID  NUMERIC(18,0)
		
		SELECT @iCount = COUNT(*) FROM @TEMPItems

		WHILE @i <= @iCount
		BEGIN
			SELECT  
				@numOppItemID=numOppItemID
				,@numItemCode=numItemCode
				,@bitKitParent=bitKitParent
				,@numUnitHour=numUnitHour
				,@numWarehouseItemID=numWarehouseItemID
			FROM 
				@TEMPItems 
			WHERE 
				ID = @i

			SELECT TOP 1 
				@numWarehouseItemID = numWareHouseItemID
			FROM
			(
				SELECT 
					ROW_NUMBER() OVER(PARTITION BY titOnHandOrder ORDER BY titOnHandOrder ASC) numRowIndex	
					,*
				FROM
				(
					SELECT
						WI.numWareHouseItemID
						,(CASE 
							WHEN @bitKitParent=1 
							THEN (CASE 
									WHEN NOT EXISTS (SELECT OKI.numOppChildItemID FROM OpportunityKitItems OKI INNER JOIN WareHouseItems WIInner ON OKI.numChildItemID=WIInner.numItemID WHERE OKI.numOppId=@numOppID AND OKI.numOppItemID=@numOppItemID AND WIInner.numWareHouseID = TW.numWarehouseID GROUP BY OKI.numOppChildItemID,OKI.numQtyItemsReq,WIInner.numWareHouseID HAVING SUM(numOnHand) < OKI.numQtyItemsReq)
										AND  NOT EXISTS (SELECT OKCI.numOppKitChildItemID FROM OpportunityKitChildItems OKCI INNER JOIN WareHouseItems WIInner ON OKCI.numItemID=WIInner.numItemID WHERE OKCI.numOppId=@numOppID AND OKCI.numOppItemID=@numOppItemID AND WIInner.numWareHouseID = TW.numWarehouseID GROUP BY OKCI.numOppKitChildItemID,OKCI.numQtyItemsReq,WIInner.numWareHouseID HAVING SUM(numOnHand) < OKCI.numQtyItemsReq)
									THEN 1
									ELSE 0 
								END)
							ELSE (CASE WHEN (SELECT SUM(WIInner.numOnHand) FROM WareHouseItems WIInner WHERE WIInner.numItemID = WI.numItemID AND WIInner.numWareHouseID=WI.numWareHouseID) >= @numUnitHour THEN 1 ELSE 0 END) 
						END) titOnHandOrder
						,TW.ID intOrder
					FROM
						WareHouseItems WI
					INNER JOIN
						@TEMPWarehouse TW
					ON
						WI.numWareHouseID = TW.numWarehouseID
					WHERE
						WI.numItemID = @numItemCode
						AND ISNULL(WI.numWareHouseID,0) = 0
				) TEMP
			) T1
			ORDER BY
				titOnHandOrder DESC, intOrder ASC			

			IF ISNULL(@numWarehouseItemID,0) > 0
			BEGIN
				UPDATE OpportunityItems SET numWarehouseItmsID=@numWarehouseItemID WHERE numOppId=@numOppID AND numoppitemtCode=@numOppItemID
			END

			SET @i = @i + 1
		END
	END
END
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityMaster_AddAutoCalculatedShippingRate')
DROP PROCEDURE dbo.USP_OpportunityMaster_AddAutoCalculatedShippingRate
GO

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_OpportunityMaster_AddAutoCalculatedShippingRate]
(
	@numDomainID NUMERIC(18,0)
	,@numOppID NUMERIC(18,0)
	,@dtItemReleaseDate DATE
	,@numShipFromWarehouse NUMERIC(18,0)
	,@monShipRate DECIMAL(20,5)
	,@vcDescription VARCHAR(300)
)
AS
BEGIN
	DECLARE @numShippingServiceItemID NUMERIC(18,0)

	SELECT 
		@numShippingServiceItemID=ISNULL(numShippingServiceItemID,0)
	FROM 
		 Domain
	WHERE 
		numDomainId=@numDomainID 

	IF NOT EXISTS (SELECT numItemCode FROM  Item WHERE numDomainID=@numDomainID AND numItemCode=@numShippingServiceItemID)
	BEGIN
		RAISERROR('SHIPPING_ITEM_NOT_SET',16,1)
		RETURN
	END

	DECLARE @numOppItemID NUMERIC(18,0)

	SET @numOppItemID = ISNULL((SELECT 
									OI.numoppitemtCode 
								FROM 
									OpportunityItems OI 
								INNER JOIN 
									OpportunityMaster OM 
								ON 
									OI.numOppId=OM.numOppId 
								WHERE 
									OI.numOppId=@numOppID 
									AND OI.numItemCode=@numShippingServiceItemID 
									AND ISNULL(OI.numShipFromWarehouse,0)=@numShipFromWarehouse 
									AND ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate)=@dtItemReleaseDate),0)

	IF ISNULL(@numOppItemID,0) > 0
	BEGIN
		IF NOT EXISTS (SELECT OBI.numOppBizDocItemID FROM OpportunityBizDocs OB INNER JOIN OpportunityBizDocItems OBI ON OB.numOppBizDocsId=OBI.numOppBizDocID WHERE OB.numOppId=@numOppID AND OBI.numOppItemID=@numOppItemID)
		BEGIN
			UPDATE 
				OpportunityItems
			SET
				numUnitHour = 1
				,monPrice = @monShipRate
				,monTotAmount = @monShipRate
				,monTotAmtBefDiscount = @monShipRate
			FROM
				OpportunityItems OI 
			WHERE 
				numoppitemtCode=@numOppItemID

			UPDATE 
				OpportunityBizDocItems 
			SET 
				numUnitHour=1
				,monPrice=@monShipRate
				,monTotAmount=@monShipRate
				,monTotAmtBefDiscount=@monShipRate 
			WHERE 
				numOppItemID=@numOppItemID
		END
	END
	ELSE
	BEGIN
		INSERT INTO OpportunityItems
		(
			numOppId
			,numItemCode
			,numUnitHour
			,monPrice
			,monTotAmount
			,monTotAmtBefDiscount
			,bitDiscountType
			,fltDiscount
			,vcItemName
			,vcItemDesc
			,vcSKU
			,vcManufacturer
			,vcModelID
			,numShipFromWarehouse
			,ItemReleaseDate
		)
		SELECT 
			@numOppID
			,@numShippingServiceItemID
			,1
			,@monShipRate
			,@monShipRate
			,@monShipRate
			,1
			,0
			,vcItemName
			,@vcDescription
			,vcSKU
			,vcManufacturer
			,vcModelID
			,@numShipFromWarehouse
			,@dtItemReleaseDate
		FROM
			Item
		WHERE
			numDomainID=@numDomainID
			AND numItemCode = @numShippingServiceItemID

		SET @numOppItemID = SCOPE_IDENTITY()

		--Insert Tax for Opportunity Items
		INSERT INTO dbo.OpportunityItemsTaxItems 
		(
			numOppId,
			numOppItemID,
			numTaxItemID,
			numTaxID
		) 
		SELECT 
			@numOppId,
			OI.numoppitemtCode,
			TI.numTaxItemID,
			0
		FROM 
			dbo.OpportunityItems OI 
		JOIN 
			dbo.ItemTax IT 
		ON 
			OI.numItemCode=IT.numItemCode 
		JOIN
			TaxItems TI 
		ON 
			TI.numTaxItemID = IT.numTaxItemID 
		WHERE 
			OI.numOppId=@numOppID 
			AND OI.numoppitemtCode = @numOppItemID
			AND IT.bitApplicable=1 
		UNION
		SELECT 
			@numOppId,
			OI.numoppitemtCode,
			0,
			0
		FROM 
			dbo.OpportunityItems OI 
		JOIN 
			dbo.Item I 
		ON 
			OI.numItemCode=I.numItemCode
		WHERE 
			OI.numOppId=@numOppID 
			AND OI.numoppitemtCode = @numOppItemID
			AND I.bitTaxable=1 
		UNION
		SELECT
			@numOppId,
			OI.numoppitemtCode,
			1,
			TD.numTaxID
		FROM
			dbo.OpportunityItems OI 
		INNER JOIN
			ItemTax IT
		ON
			IT.numItemCode = OI.numItemCode
		INNER JOIN
			TaxDetails TD
		ON
			TD.numTaxID = IT.numTaxID
			AND TD.numDomainId = @numDomainId
		WHERE
			OI.numOppId = @numOppID
			AND OI.numoppitemtCode = @numOppItemID
	END
	
	UPDATE 
		OpportunityMaster 
	SET 
		monDealAmount=dbo.GetDealAmount(@numOppID,getdate(),0)
	WHERE 
		numDomainID=@numDomainID 
		AND numOppID=@numOppID
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityMasterShippingRateError_Save')
DROP PROCEDURE USP_OpportunityMasterShippingRateError_Save
GO
CREATE PROCEDURE [dbo].[USP_OpportunityMasterShippingRateError_Save]
(
	@numDomainID NUMERIC(18,0)
	,@numOppID NUMERIC(18,0)
	,@vcErrorMessage VARCHAR(MAX)
)
AS
BEGIN
	IF EXISTS (SELECT ID FROM OpportunityMasterShippingRateError WHERE numDomainID=@numDomainID AND numOppID=@numOppID)
	BEGIN
		UPDATE
			OpportunityMasterShippingRateError
		SET
			intNoOfTimeFailed = ISNULL(intNoOfTimeFailed,0) + 1
			,dtLastExecutedDate = GETUTCDATE()
		WHERE
			numDomainID=@numDomainID 
			AND numOppID=@numOppID
	END
	ELSE
	BEGIN
		INSERT INTO OpportunityMasterShippingRateError
		(
			numDomainID
			,numOppID
			,intNoOfTimeFailed
			,vcErrorMessage
			,dtLastExecutedDate
		)
		VALUES
		(
			@numDomainID
			,@numOppID
			,1
			,@vcErrorMessage
			,GETUTCDATE()
		)
	END

END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpporutnityMaster_GetForShippingRate')
DROP PROCEDURE USP_OpporutnityMaster_GetForShippingRate
GO
CREATE PROCEDURE [dbo].[USP_OpporutnityMaster_GetForShippingRate]
(
	@numDomainID NUMERIC(18,0)
	,@numOppID NUMERIC(18,0)
	,@numWareHouseID NUMERIC(18,0)
	,@dtShipByDate DATE
	,@bitFromService BIT
)
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	SELECT TOP 15
		OM.numDomainId
		,OM.numOppId
		,ISNULL(OM.monMarketplaceShippingCost,0) monMarketplaceShippingCost
		,ISNULL(OM.intUsedShippingCompany,0) numShipVia
		,ISNULL(OM.numShippingService,0) numShipService
		,ISNULL(DM.intShippingCompany,0) numPreferredShipVia
		,ISNULL(DM.numDefaultShippingServiceID,0) numPreferredShipService
		,W.numWareHouseID
		,OM.dtExpectedDate
		,ISNULL(OI.ItemReleaseDate,CAST(OM.dtReleaseDate AS DATE)) dtShipByDate
		,MSFSC.tintType
		,MSFSC.vcPriorities
		,MSFSC.numShipVia
		,MSFSC.numShipService
		,MSFSC.bitOverride
		,MSFSC.fltWeight
		,MSFSC.numShipViaOverride
	FROM
		OpportunityMaster OM
	INNER JOIN
		DivisionMaster DM
	ON
		OM.numDivisionId = DM.numDivisionID
	INNER JOIN
		Domain D
	ON
		OM.numDomainId = D.numDomainId
	INNER JOIN
		MassSalesFulfillmentShippingConfig MSFSC
	ON
		OM.numDomainId = MSFSC.numDomainID
		AND OM.tintSource = MSFSC.numOrderSource
		AND OM.tintSourceType = MSFSC.tintSourceType
	INNER JOIN
		OpportunityItems OI
	ON
		OM.numOppId = OI.numOppId
	INNER JOIN
		WareHouseItems WI
	ON
		OI.numWarehouseItmsID = WI.numWareHouseItemID
	INNER JOIN
		Warehouses W
	ON
		WI.numWareHouseID = W.numWareHouseID
	INNER JOIN
		Item I
	ON
		OI.numItemCode = I.numItemCode
	LEFT JOIN
		OpportunityMasterShippingRateError OMSRE
	ON
		OM.numDomainId = OMSRE.numDomainID
		AND OM.numOppId = OMSRE.numOppID
	WHERE
		(ISNULL(@numDomainID,0) = 0 OR OM.numDomainId = @numDomainID)
		AND (ISNULL(@numOppID,0) = 0 OR OM.numOppId = @numOppID)
		AND (ISNULL(@numWareHouseID,0) = 0 OR W.numWareHouseID = @numWareHouseID)
		AND 1 = (CASE WHEN @bitFromService = 1 THEN 1 ELSE (CASE WHEN ISNULL(OI.ItemReleaseDate,CAST(OM.dtReleaseDate AS DATE))=@dtShipByDate THEN 1 ELSE 0 END) END)
		AND OM.tintOppType=1
		AND OM.tintOppStatus=1
		AND ISNULL(OM.tintshipped,0) = 0
		AND 1 = (CASE WHEN ISNULL(@bitFromService,0) = 1 THEN (CASE WHEN DATEDIFF(DAY,OM.bintCreatedDate,GETUTCDATE()) <= 1 THEN 1 ELSE 0 END) ELSE 1 END)
		AND ISNULL(D.numShippingServiceItemID,0) > 0
		AND ISNULL(OMSRE.intNoOfTimeFailed,0) < 3
		AND NOT EXISTS (SELECT OIInner.numoppitemtCode FROM OpportunityItems OIInner WHERE OIInner.numOppId=OM.numOppId AND OIInner.numItemCode=D.numShippingServiceItemID AND OIInner.numShipFromWarehouse=W.numWareHouseID AND OIInner.ItemReleaseDate=ISNULL(OI.ItemReleaseDate,CAST(OM.dtReleaseDate AS DATE)))
	GROUP BY
		OM.numDomainId
		,OM.numOppId
		,OM.monMarketplaceShippingCost
		,OM.dtExpectedDate
		,OM.intUsedShippingCompany
		,OM.numShippingService
		,DM.intShippingCompany
		,DM.numDefaultShippingServiceID
		,OM.dtReleaseDate
		,OI.ItemReleaseDate
		,W.numWareHouseID
		,MSFSC.tintType
		,MSFSC.vcPriorities
		,MSFSC.numShipVia
		,MSFSC.numShipService
		,MSFSC.bitOverride
		,MSFSC.fltWeight
		,MSFSC.numShipViaOverride
		,OMSRE.intNoOfTimeFailed
	ORDER BY
		ISNULL(OMSRE.intNoOfTimeFailed,0) ASC
		,OM.numOppId
END
GO

