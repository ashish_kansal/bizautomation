/******************************************************************
Project: Release 11.1 Date: 20.FEB.2019
Comments: ALTER SCRIPTS
*******************************************************************/

/******************************************** SANDEEP *********************************************/

ALTER TABLE PromotionOffer ADD bitError BIT
ALTER TABLE PromotionOffer ADD bitUseForCouponManagement BIT
ALTER TABLE PromotionOffer ADD bitUseOrderPromotion BIT
ALTER TABLE PromotionOffer ADD numOrderPromotionID NUMERIC(18,0)

/******************************************** PRASANT *********************************************/

ALTER TABLE Sales_process_List_Master ADD numOppId NUMERIC DEFAULT 0
ALTER TABLE Sales_process_List_Master ADD numTaskValidatorId NUMERIC DEFAULT 0

ALTER TABLE StagePercentageDetailsTask ADD numOrder NUMERIC DEFAULT 0
ALTER TABLE StagePercentageDetails ADD numStageOrder NUMERIC DEFAULT 0