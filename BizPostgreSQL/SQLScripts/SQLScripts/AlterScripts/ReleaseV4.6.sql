/******************************************************************
Project: Release 4.6 Date: 12.June.2015
Comments: ALTER SCRIPTS
*******************************************************************/

ALTER TABLE OpportunityMaster ADD
numBillToAddressID NUMERIC(18,0) NULL,
numShipToAddressID NUMERIC(18,0) NULL

---------------------------------------------------------------------------------------------------------------------------------

ALTER TABLE BizDocTemplate ADD
numRelationship NUMERIC(18,0) NULL,
numProfile NUMERIC(18,0) NULL

----------------------------------------------------------------------------------------------------------------------------------

INSERT INTO PageNavigationDTL 
(numPageNavID,numModuleID,numParentID,vcPageNavName,vcNavURL,bitVisible,numTabID)
VALUES
(
242,35,93,'Profit & Loss A/c - New','../Accounting/frmProfitLossReport.aspx',1,45
)



BEGIN TRANSACTION

DECLARE @numPageNavID NUMERIC(18,0) = 242 

DECLARE @TEMPDomain TABLE
(
	ID INT IDENTITY(1,1),
	numDomainID NUMERIC(18,0)
)

INSERT INTO @TEMPDomain SELECT numDomainId FROM Domain WHERE numDomainId <> -255



DECLARE @i AS INT = 1
DECLARE @COUNT AS INT
DECLARE @numDomainID AS NUMERIC(18,0)

SELECT @COUNT=COUNT(*) FROM @TEMPDomain


WHILE @i <= @COUNT
BEGIN
	SELECT @numDomainID=numDomainID FROM @TEMPDomain WHERE ID = @i

	DELETE FROM TreeNavigationAuthorization WHERE numPageNavID=@numPageNavID AND numDomainID = @numDomainId

	INSERT  INTO dbo.TreeNavigationAuthorization
    (
        numGroupID,
        numTabID,
        numPageNavID,
        bitVisible,
        numDomainID,
        tintType
    )
    SELECT    
		A.numGroupID,
		45,
		@numPageNavID,
		1,
		A.numDomainID,
		1
	FROM      
		AuthenticationGroupMaster A
	WHERE    
		A.numDomainId = @numDomainID


	SET @i = @i + 1
END

ROLLBACK


--------------------------------------------------------------------------------------------------------


INSERT INTO PageNavigationDTL 
(numPageNavID,numModuleID,numParentID,vcPageNavName,vcNavURL,bitVisible,numTabID)
VALUES
(
243,35,93,'Balance Sheet - New','../Accounting/frmBalanceSheetReport.aspx',1,45
)



BEGIN TRANSACTION

DECLARE @numPageNavID NUMERIC(18,0) = 243 

DECLARE @TEMPDomain TABLE
(
	ID INT IDENTITY(1,1),
	numDomainID NUMERIC(18,0)
)

INSERT INTO @TEMPDomain SELECT numDomainId FROM Domain WHERE numDomainId <> -255



DECLARE @i AS INT = 1
DECLARE @COUNT AS INT
DECLARE @numDomainID AS NUMERIC(18,0)

SELECT @COUNT=COUNT(*) FROM @TEMPDomain


WHILE @i <= @COUNT
BEGIN
	SELECT @numDomainID=numDomainID FROM @TEMPDomain WHERE ID = @i

	DELETE FROM TreeNavigationAuthorization WHERE numPageNavID=@numPageNavID AND numDomainID = @numDomainId

	INSERT  INTO dbo.TreeNavigationAuthorization
    (
        numGroupID,
        numTabID,
        numPageNavID,
        bitVisible,
        numDomainID,
        tintType
    )
    SELECT    
		A.numGroupID,
		45,
		@numPageNavID,
		1,
		A.numDomainID,
		1
	FROM      
		AuthenticationGroupMaster A
	WHERE    
		A.numDomainId = @numDomainID


	SET @i = @i + 1
END

ROLLBACK


--------------------------------------------------------------------------------------------------------


INSERT INTO PageNavigationDTL 
(numPageNavID,numModuleID,numParentID,vcPageNavName,vcNavURL,bitVisible,numTabID)
VALUES
(
244,35,93,'Trial Balance - New','../Accounting/frmTrialBalanceReport.aspx',1,45
)



BEGIN TRANSACTION

DECLARE @numPageNavID NUMERIC(18,0) = 244

DECLARE @TEMPDomain TABLE
(
	ID INT IDENTITY(1,1),
	numDomainID NUMERIC(18,0)
)

INSERT INTO @TEMPDomain SELECT numDomainId FROM Domain WHERE numDomainId <> -255



DECLARE @i AS INT = 1
DECLARE @COUNT AS INT
DECLARE @numDomainID AS NUMERIC(18,0)

SELECT @COUNT=COUNT(*) FROM @TEMPDomain


WHILE @i <= @COUNT
BEGIN
	SELECT @numDomainID=numDomainID FROM @TEMPDomain WHERE ID = @i

	DELETE FROM TreeNavigationAuthorization WHERE numPageNavID=@numPageNavID AND numDomainID = @numDomainId

	INSERT  INTO dbo.TreeNavigationAuthorization
    (
        numGroupID,
        numTabID,
        numPageNavID,
        bitVisible,
        numDomainID,
        tintType
    )
    SELECT    
		A.numGroupID,
		45,
		@numPageNavID,
		1,
		A.numDomainID,
		1
	FROM      
		AuthenticationGroupMaster A
	WHERE    
		A.numDomainId = @numDomainID


	SET @i = @i + 1
END
COMMIT
ROLLBACK

---------------------------------------------------------------------------------------

ALTER TABLE Domain ADD
bitReOrderPoint BIT NULL,
numReOrderPointOrderStatus NUMERIC(18,0) NULL

-----------------------------------------------------------------------------------------------

INSERT INTO DycFormField_Mapping 
(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],bitImport,bitAllowFiltering,intSectionID)
VALUES
(
	4,668,55,0,0,'Lead Days','TextBox','LeadTimeDays',8,1,1,1
)


--------------------------------------------------------------------------------------------

ALTER TABLE [dbo].[AdditionalContactsInformation]
ADD [vcImageName] VARCHAR(200)