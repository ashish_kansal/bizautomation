/******************************************************************
Project: Release 12.8 Date: 16.OCT.2019
Comments: ALTER SCRIPTS
*******************************************************************/


/******************************************** SANDEEP *********************************************/

ALTER TABLE ImapUserDetails ALTER COLUMN vcGoogleRefreshToken VARCHAR(MAX)
ALTER TABLE Activity ALTER COLUMN GoogleEventId VARCHAR(MAX)
ALTER TABLE MassPurchaseFulfillmentConfiguration ADD bitReceiveBillOnClose BIT
ALTER TABLE MassPurchaseFulfillmentConfiguration ADD tintBillType TINYINT 
ALTER TABLE ShippingReport ADD vcPayerStreet VARCHAR(300)
ALTER TABLE ShippingReport ADD vcPayerCity VARCHAR(100)
ALTER TABLE ShippingReport ADD numPayerState NUMERIC(18,0)

UPDATE MassPurchaseFulfillmentConfiguration SET tintBillType=1


DECLARE @numFieldID NUMERIC(18,0)
SELECT @numFieldID=numFieldID FROM DycFieldMaster WHERE vcDBColumnName='dtReleaseDate'

INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,bitAddField,intSectionID
)
VALUES
(
	3,@numFieldID,90,1,1,1,1
)


UPDATE DycFormField_Mapping SET vcFieldName='Shipped' WHERE numFOrmID=26 AND vcPropertyName='vcShippedReceived'
UPDATE DycFormField_Mapping SET vcFieldName='Put Away' WHERE numFOrmID=129 AND vcPropertyName='vcShippedReceived'
UPDATE DycFormField_Mapping SET vcFieldName='WH On-Hand' WHERE numFormID=139 AND numFieldID=900
-----------------------------------------

DECLARE @numFieldID NUMERIC(18,0)
SELECT @numFieldID=numFieldId FROM DycFieldMaster WHERE vcDbColumnName='numQtyReceived' AND vcLookBackTableName='OpportunityItems'

INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField
)
VALUES
(
	4,@numFieldID,129,0,0,'Received','Label',1,0,0,1,0,0
)

-----------------------------------------

UPDATE DycFormField_Mapping SET vcFieldName='Qty Put Away' WHERE numFormID=135 AND numFieldID=493

-------------------------------------------

UPDATE DycFormField_Mapping SET vcFieldName='Item' WHERE numFormID=135 AND numFieldID=189

---------------------------------------------

DECLARE @numFieldID NUMERIC(18,0)
SELECT @numFieldID=numFieldId FROM DycFieldMaster WHERE vcOrigDbColumnName  = 'vcBilled'

INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField
)
VALUES
(
	4,@numFieldID,135,0,0,'Qty Billed','Label',1,0,0,1,0,0
)

--------------------------------------------------

USE [Production.2014]
GO

/****** Object:  Table [dbo].[DivisionMasterShippingAccount]    Script Date: 10-Oct-19 5:14:02 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DivisionMasterShippingAccount](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[numDivisionID] [numeric](18, 0) NOT NULL,
	[numShipViaID] [numeric](18, 0) NOT NULL,
	[vcAccountNumber] [varchar](100) NOT NULL,
	[vcStreet] [varchar](300) NOT NULL,
	[vcCity] [varchar](100) NOT NULL,
	[numCountry] [numeric](18, 0) NOT NULL,
	[numState] [numeric](18, 0) NOT NULL,
	[vcZipCode] [varchar](50) NOT NULL,
	[numCreatedBy] [numeric](18, 0) NOT NULL,
	[dtCreatedDate] [datetime] NOT NULL,
	[numModifiedBy] [numeric](18, 0) NULL,
	[dtModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_DivisionMasterShippingAccount] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING ON
GO

------------------------------------

DECLARE @numFieldID NUMERIC(18,0)
INSERT INTO DycFieldMaster
(
	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcListItemType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitAllowFiltering
)
VALUES
(
	4,'Total On-Hand','numTotalOnHand','numTotalOnHand','TotalOnHand','WareHouseItems','N','R','Label','',0,1,0,0,0,1,0,0,1,0,0
)

SET @numFieldID = SCOPE_IDENTITY()

INSERT INTO DycFormField_Mapping
(
	numModuleID,numFormID,numFieldID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering
)
VALUES
(
	4,139,@numFieldID,0,0,'Total On-Hand','Label','TotalOnHand',1,0,0,1,0,0,0,0
)