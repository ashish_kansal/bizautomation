/******************************************************************
Project: Release 6.2 Date: 10.OCT.2016
Comments: ALTER SCRIPTS
*******************************************************************/

/*************************** SANDEEP ****************************/

ALTER TABLE dbo.State ADD numShippingZone NUMERIC(18,0)

------------------------------------------------------------------

ALTER TABLE Domain ADD vcPrinterIPAddress VARCHAR(15)
ALTER TABLE Domain ADD vcPrinterPort VARCHAR(5)

------------------------------------------------------------------

INSERT INTO ShippingFields (vcFieldName,numListItemID,vcToolTip) VALUES ('Shipping Label Type',91,'Generate shipping label as PNG or ZPL')
INSERT INTO ShippingFields (vcFieldName,numListItemID,vcToolTip) VALUES ('Shipping Label Type',88,'Generate shipping label as GIF or ZPL')

------------------------------------------------------------------

BEGIN TRY
BEGIN TRANSACTION
	DECLARE @numListID AS INT = 833

	SET IDENTITY_INSERT dbo.ListMaster ON;  

	INSERT INTO ListMaster
	(
		numListID
		,vcListName
		,numCreatedBy
		,bitDeleted
		,bitFixed
		,numDomainID
		,bitFlag
		,numModuleID
	)
	VALUES
	(
		@numListID
		,'Shipping Zone'
		,1
		,0
		,0
		,1
		,1
		,1
	)

	SET IDENTITY_INSERT dbo.ListMaster OFF; 

	DECLARE @numAlaska NUMERIC(18,0)
	DECLARE @numCentral NUMERIC(18,0)
	DECLARE @numEastern NUMERIC(18,0)
	DECLARE @numHawaii NUMERIC(18,0)
	DECLARE @numMountain NUMERIC(18,0)
	DECLARE @numPacific NUMERIC(18,0)


	INSERT INTO ListDetails (numListID,vcData,numCreatedBY,numModifiedBy,bitDelete,numDomainID,constFlag,sintOrder) VALUES (@numListID,'Alaska',1,1,0,1,1,1)
	SELECT @numAlaska = SCOPE_IDENTITY()
	INSERT INTO ListDetails (numListID,vcData,numCreatedBY,numModifiedBy,bitDelete,numDomainID,constFlag,sintOrder) VALUES (@numListID,'Central',1,1,0,1,1,1)
	SELECT @numCentral = SCOPE_IDENTITY()
	INSERT INTO ListDetails (numListID,vcData,numCreatedBY,numModifiedBy,bitDelete,numDomainID,constFlag,sintOrder) VALUES (@numListID,'Eastern',1,1,0,1,1,1)
	SELECT @numEastern = SCOPE_IDENTITY()
	INSERT INTO ListDetails (numListID,vcData,numCreatedBY,numModifiedBy,bitDelete,numDomainID,constFlag,sintOrder) VALUES (@numListID,'Hawaii',1,1,0,1,1,1)
	SELECT @numHawaii = SCOPE_IDENTITY()
	INSERT INTO ListDetails (numListID,vcData,numCreatedBY,numModifiedBy,bitDelete,numDomainID,constFlag,sintOrder) VALUES (@numListID,'Mountain',1,1,0,1,1,1)
	SELECT @numMountain = SCOPE_IDENTITY()
	INSERT INTO ListDetails (numListID,vcData,numCreatedBY,numModifiedBy,bitDelete,numDomainID,constFlag,sintOrder) VALUES (@numListID,'Pacific',1,1,0,1,1,1)
	SELECT @numPacific = SCOPE_IDENTITY()

	UPDATE dbo.State SET numShippingZone=@numCentral WHERE vcState LIKE '%Alabama%'
	UPDATE dbo.State SET numShippingZone=@numAlaska WHERE vcState LIKE '%Alaska%'
	UPDATE dbo.State SET numShippingZone=@numMountain WHERE vcState LIKE '%Arizona%'
	UPDATE dbo.State SET numShippingZone=@numCentral WHERE vcState LIKE '%Arkansas%'
	UPDATE dbo.State SET numShippingZone=@numPacific WHERE vcState LIKE '%California%'
	UPDATE dbo.State SET numShippingZone=@numMountain WHERE vcState LIKE '%Colorado%'
	UPDATE dbo.State SET numShippingZone=@numEastern WHERE vcState LIKE '%Connecticut%'
	UPDATE dbo.State SET numShippingZone=@numEastern WHERE vcState LIKE '%Delaware%'
	UPDATE dbo.State SET numShippingZone=@numEastern WHERE vcState LIKE '%Florida%'
	UPDATE dbo.State SET numShippingZone=@numEastern WHERE vcState LIKE '%Georgia%'
	UPDATE dbo.State SET numShippingZone=@numHawaii WHERE vcState LIKE '%Hawaii%'
	UPDATE dbo.State SET numShippingZone=@numMountain WHERE vcState LIKE '%Idaho%'
	UPDATE dbo.State SET numShippingZone=@numCentral WHERE vcState LIKE '%Illinois%'
	UPDATE dbo.State SET numShippingZone=@numEastern WHERE vcState LIKE '%Indiana%'
	UPDATE dbo.State SET numShippingZone=@numCentral WHERE vcState LIKE '%Iowa%'
	UPDATE dbo.State SET numShippingZone=@numCentral WHERE vcState LIKE '%Kansas%'
	UPDATE dbo.State SET numShippingZone=@numEastern WHERE vcState LIKE '%Kentucky%'
	UPDATE dbo.State SET numShippingZone=@numCentral WHERE vcState LIKE '%Louisiana%'
	UPDATE dbo.State SET numShippingZone=@numEastern WHERE vcState LIKE '%Maine%'
	UPDATE dbo.State SET numShippingZone=@numEastern WHERE vcState LIKE '%Maryland%'
	UPDATE dbo.State SET numShippingZone=@numEastern WHERE vcState LIKE '%Massachusetts%'
	UPDATE dbo.State SET numShippingZone=@numEastern WHERE vcState LIKE '%Michigan%'
	UPDATE dbo.State SET numShippingZone=@numCentral WHERE vcState LIKE '%Minnesota%'
	UPDATE dbo.State SET numShippingZone=@numCentral WHERE vcState LIKE '%Mississippi%'
	UPDATE dbo.State SET numShippingZone=@numCentral WHERE vcState LIKE '%Missouri%'
	UPDATE dbo.State SET numShippingZone=@numMountain WHERE vcState LIKE '%Montana%'
	UPDATE dbo.State SET numShippingZone=@numCentral WHERE vcState LIKE '%Nebraska%'
	UPDATE dbo.State SET numShippingZone= @numPacific WHERE vcState LIKE '%Nevada%'
	UPDATE dbo.State SET numShippingZone=@numEastern WHERE vcState LIKE '%New Hampshire%'
	UPDATE dbo.State SET numShippingZone=@numEastern WHERE vcState LIKE '%New Jersey%'
	UPDATE dbo.State SET numShippingZone=@numMountain WHERE vcState LIKE '%New Mexico%'
	UPDATE dbo.State SET numShippingZone=@numEastern WHERE vcState LIKE '%New York%'
	UPDATE dbo.State SET numShippingZone=@numEastern WHERE vcState LIKE '%North Carolina%'
	UPDATE dbo.State SET numShippingZone=@numCentral WHERE vcState LIKE '%North Dakota%'
	UPDATE dbo.State SET numShippingZone=@numEastern WHERE vcState LIKE '%Ohio%'
	UPDATE dbo.State SET numShippingZone=@numCentral WHERE vcState LIKE '%Oklahoma%'
	UPDATE dbo.State SET numShippingZone= @numPacific WHERE vcState LIKE '%Oregon%'
	UPDATE dbo.State SET numShippingZone=@numEastern WHERE vcState LIKE '%Pennsylvania%'
	UPDATE dbo.State SET numShippingZone=@numEastern WHERE vcState LIKE '%Rhode Island%'
	UPDATE dbo.State SET numShippingZone=@numEastern WHERE vcState LIKE '%South Carolina%'
	UPDATE dbo.State SET numShippingZone=@numCentral WHERE vcState LIKE '%South Dakota%'
	UPDATE dbo.State SET numShippingZone=@numCentral WHERE vcState LIKE '%Tennessee%'
	UPDATE dbo.State SET numShippingZone=@numCentral WHERE vcState LIKE '%Texas%'
	UPDATE dbo.State SET numShippingZone=@numMountain WHERE vcState LIKE '%Utah%'
	UPDATE dbo.State SET numShippingZone=@numEastern WHERE vcState LIKE '%Vermont%'
	UPDATE dbo.State SET numShippingZone=@numEastern WHERE vcState LIKE '%Virginia%'
	UPDATE dbo.State SET numShippingZone=@numPacific WHERE vcState LIKE '%Washington%'
	UPDATE dbo.State SET numShippingZone=@numEastern WHERE vcState LIKE '%West Virginia%'
	UPDATE dbo.State SET numShippingZone=@numCentral WHERE vcState LIKE '%Wisconsin%'
	UPDATE dbo.State SET numShippingZone=@numMountain WHERE vcState LIKE '%Wyoming%'

COMMIT
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		ERROR_MESSAGE(),
		ERROR_NUMBER(),
		ERROR_SEVERITY(),
		ERROR_STATE(),
		ERROR_LINE(),
		ISNULL(ERROR_PROCEDURE(), '-');
END CATCH


