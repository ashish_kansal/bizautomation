/******************************************************************
Project: Release 11.0 Date: 06.FEB.2019
Comments: ALTER SCRIPTS
*******************************************************************/

/******************************************** SANDEEP *********************************************/

ALTER TABLE PromotionOffer ADD vcShortDesc VARCHAR(300)
ALTER TABLE PromotionOffer ADD vcLongDesc VARCHAR(MAX)
ALTER TABLE PromotionOffer ADD tintItemCalDiscount TINYINT

--------------------------------------------

USE [Production.2014]
GO

/****** Object:  Table [dbo].[ShippingPackageType]    Script Date: 07-Feb-19 9:18:08 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[ShippingPackageType](
	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numShippingCompanyID] [numeric](18, 0) NOT NULL,
	[numNsoftwarePackageTypeID] [numeric](18, 0) NOT NULL,
	[vcPackageName] [varchar](100) NOT NULL,
	[bitEndicia] [bit] NOT NULL,
 CONSTRAINT [PK_ShippingPackageType] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING ON
GO

-----------------------

USE [Production.2014]
GO
SET IDENTITY_INSERT [dbo].[ShippingPackageType] ON 

GO
INSERT [dbo].[ShippingPackageType] ([ID], [numShippingCompanyID], [numNsoftwarePackageTypeID], [vcPackageName], [bitEndicia]) VALUES (CAST(1 AS Numeric(18, 0)), CAST(91 AS Numeric(18, 0)), CAST(2 AS Numeric(18, 0)), N'FedEx Letter', 0)
GO
INSERT [dbo].[ShippingPackageType] ([ID], [numShippingCompanyID], [numNsoftwarePackageTypeID], [vcPackageName], [bitEndicia]) VALUES (CAST(2 AS Numeric(18, 0)), CAST(91 AS Numeric(18, 0)), CAST(11 AS Numeric(18, 0)), N'FedEx Pak', 0)
GO
INSERT [dbo].[ShippingPackageType] ([ID], [numShippingCompanyID], [numNsoftwarePackageTypeID], [vcPackageName], [bitEndicia]) VALUES (CAST(3 AS Numeric(18, 0)), CAST(91 AS Numeric(18, 0)), CAST(12 AS Numeric(18, 0)), N'FedEx Box', 0)
GO
INSERT [dbo].[ShippingPackageType] ([ID], [numShippingCompanyID], [numNsoftwarePackageTypeID], [vcPackageName], [bitEndicia]) VALUES (CAST(4 AS Numeric(18, 0)), CAST(91 AS Numeric(18, 0)), CAST(13 AS Numeric(18, 0)), N'FedEx 10kg Box', 0)
GO
INSERT [dbo].[ShippingPackageType] ([ID], [numShippingCompanyID], [numNsoftwarePackageTypeID], [vcPackageName], [bitEndicia]) VALUES (CAST(5 AS Numeric(18, 0)), CAST(91 AS Numeric(18, 0)), CAST(14 AS Numeric(18, 0)), N'FedEx 25kg Box', 0)
GO
INSERT [dbo].[ShippingPackageType] ([ID], [numShippingCompanyID], [numNsoftwarePackageTypeID], [vcPackageName], [bitEndicia]) VALUES (CAST(6 AS Numeric(18, 0)), CAST(91 AS Numeric(18, 0)), CAST(28 AS Numeric(18, 0)), N'FedEx Tube', 0)
GO
INSERT [dbo].[ShippingPackageType] ([ID], [numShippingCompanyID], [numNsoftwarePackageTypeID], [vcPackageName], [bitEndicia]) VALUES (CAST(7 AS Numeric(18, 0)), CAST(91 AS Numeric(18, 0)), CAST(31 AS Numeric(18, 0)), N'Your Packaging', 0)
GO
INSERT [dbo].[ShippingPackageType] ([ID], [numShippingCompanyID], [numNsoftwarePackageTypeID], [vcPackageName], [bitEndicia]) VALUES (CAST(8 AS Numeric(18, 0)), CAST(88 AS Numeric(18, 0)), CAST(0 AS Numeric(18, 0)), N'None', 0)
GO
INSERT [dbo].[ShippingPackageType] ([ID], [numShippingCompanyID], [numNsoftwarePackageTypeID], [vcPackageName], [bitEndicia]) VALUES (CAST(9 AS Numeric(18, 0)), CAST(88 AS Numeric(18, 0)), CAST(2 AS Numeric(18, 0)), N'UPS letter', 0)
GO
INSERT [dbo].[ShippingPackageType] ([ID], [numShippingCompanyID], [numNsoftwarePackageTypeID], [vcPackageName], [bitEndicia]) VALUES (CAST(10 AS Numeric(18, 0)), CAST(88 AS Numeric(18, 0)), CAST(11 AS Numeric(18, 0)), N'UPS Pak', 0)
GO
INSERT [dbo].[ShippingPackageType] ([ID], [numShippingCompanyID], [numNsoftwarePackageTypeID], [vcPackageName], [bitEndicia]) VALUES (CAST(11 AS Numeric(18, 0)), CAST(88 AS Numeric(18, 0)), CAST(12 AS Numeric(18, 0)), N'UPS Box', 0)
GO
INSERT [dbo].[ShippingPackageType] ([ID], [numShippingCompanyID], [numNsoftwarePackageTypeID], [vcPackageName], [bitEndicia]) VALUES (CAST(12 AS Numeric(18, 0)), CAST(88 AS Numeric(18, 0)), CAST(13 AS Numeric(18, 0)), N'UPS 10kg Box', 0)
GO
INSERT [dbo].[ShippingPackageType] ([ID], [numShippingCompanyID], [numNsoftwarePackageTypeID], [vcPackageName], [bitEndicia]) VALUES (CAST(13 AS Numeric(18, 0)), CAST(88 AS Numeric(18, 0)), CAST(14 AS Numeric(18, 0)), N'UPS 25kg Box', 0)
GO
INSERT [dbo].[ShippingPackageType] ([ID], [numShippingCompanyID], [numNsoftwarePackageTypeID], [vcPackageName], [bitEndicia]) VALUES (CAST(14 AS Numeric(18, 0)), CAST(88 AS Numeric(18, 0)), CAST(15 AS Numeric(18, 0)), N'UPS Small Express Box', 0)
GO
INSERT [dbo].[ShippingPackageType] ([ID], [numShippingCompanyID], [numNsoftwarePackageTypeID], [vcPackageName], [bitEndicia]) VALUES (CAST(15 AS Numeric(18, 0)), CAST(88 AS Numeric(18, 0)), CAST(16 AS Numeric(18, 0)), N'UPS Medium Express Box', 0)
GO
INSERT [dbo].[ShippingPackageType] ([ID], [numShippingCompanyID], [numNsoftwarePackageTypeID], [vcPackageName], [bitEndicia]) VALUES (CAST(16 AS Numeric(18, 0)), CAST(88 AS Numeric(18, 0)), CAST(17 AS Numeric(18, 0)), N'UPS Large Express Box', 0)
GO
INSERT [dbo].[ShippingPackageType] ([ID], [numShippingCompanyID], [numNsoftwarePackageTypeID], [vcPackageName], [bitEndicia]) VALUES (CAST(17 AS Numeric(18, 0)), CAST(88 AS Numeric(18, 0)), CAST(28 AS Numeric(18, 0)), N'UPS Tube', 0)
GO
INSERT [dbo].[ShippingPackageType] ([ID], [numShippingCompanyID], [numNsoftwarePackageTypeID], [vcPackageName], [bitEndicia]) VALUES (CAST(18 AS Numeric(18, 0)), CAST(88 AS Numeric(18, 0)), CAST(30 AS Numeric(18, 0)), N'UPS Pallet', 0)
GO
INSERT [dbo].[ShippingPackageType] ([ID], [numShippingCompanyID], [numNsoftwarePackageTypeID], [vcPackageName], [bitEndicia]) VALUES (CAST(19 AS Numeric(18, 0)), CAST(88 AS Numeric(18, 0)), CAST(31 AS Numeric(18, 0)), N'Your Packaging', 0)
GO
INSERT [dbo].[ShippingPackageType] ([ID], [numShippingCompanyID], [numNsoftwarePackageTypeID], [vcPackageName], [bitEndicia]) VALUES (CAST(20 AS Numeric(18, 0)), CAST(88 AS Numeric(18, 0)), CAST(33 AS Numeric(18, 0)), N'UPS Flats', 0)
GO
INSERT [dbo].[ShippingPackageType] ([ID], [numShippingCompanyID], [numNsoftwarePackageTypeID], [vcPackageName], [bitEndicia]) VALUES (CAST(21 AS Numeric(18, 0)), CAST(88 AS Numeric(18, 0)), CAST(34 AS Numeric(18, 0)), N'UPS Parcels', 0)
GO
INSERT [dbo].[ShippingPackageType] ([ID], [numShippingCompanyID], [numNsoftwarePackageTypeID], [vcPackageName], [bitEndicia]) VALUES (CAST(22 AS Numeric(18, 0)), CAST(88 AS Numeric(18, 0)), CAST(35 AS Numeric(18, 0)), N'UPS BPM', 0)
GO
INSERT [dbo].[ShippingPackageType] ([ID], [numShippingCompanyID], [numNsoftwarePackageTypeID], [vcPackageName], [bitEndicia]) VALUES (CAST(23 AS Numeric(18, 0)), CAST(88 AS Numeric(18, 0)), CAST(36 AS Numeric(18, 0)), N'UPS First Class', 0)
GO
INSERT [dbo].[ShippingPackageType] ([ID], [numShippingCompanyID], [numNsoftwarePackageTypeID], [vcPackageName], [bitEndicia]) VALUES (CAST(24 AS Numeric(18, 0)), CAST(88 AS Numeric(18, 0)), CAST(37 AS Numeric(18, 0)), N'UPS Priority', 0)
GO
INSERT [dbo].[ShippingPackageType] ([ID], [numShippingCompanyID], [numNsoftwarePackageTypeID], [vcPackageName], [bitEndicia]) VALUES (CAST(25 AS Numeric(18, 0)), CAST(88 AS Numeric(18, 0)), CAST(38 AS Numeric(18, 0)), N'UPS Machinables', 0)
GO
INSERT [dbo].[ShippingPackageType] ([ID], [numShippingCompanyID], [numNsoftwarePackageTypeID], [vcPackageName], [bitEndicia]) VALUES (CAST(26 AS Numeric(18, 0)), CAST(88 AS Numeric(18, 0)), CAST(39 AS Numeric(18, 0)), N'UPS Irregulars', 0)
GO
INSERT [dbo].[ShippingPackageType] ([ID], [numShippingCompanyID], [numNsoftwarePackageTypeID], [vcPackageName], [bitEndicia]) VALUES (CAST(27 AS Numeric(18, 0)), CAST(88 AS Numeric(18, 0)), CAST(40 AS Numeric(18, 0)), N'UPS Parcel Post', 0)
GO
INSERT [dbo].[ShippingPackageType] ([ID], [numShippingCompanyID], [numNsoftwarePackageTypeID], [vcPackageName], [bitEndicia]) VALUES (CAST(28 AS Numeric(18, 0)), CAST(88 AS Numeric(18, 0)), CAST(41 AS Numeric(18, 0)), N'UPS BPM Parcel', 0)
GO
INSERT [dbo].[ShippingPackageType] ([ID], [numShippingCompanyID], [numNsoftwarePackageTypeID], [vcPackageName], [bitEndicia]) VALUES (CAST(29 AS Numeric(18, 0)), CAST(88 AS Numeric(18, 0)), CAST(42 AS Numeric(18, 0)), N'UPS Media Mail', 0)
GO
INSERT [dbo].[ShippingPackageType] ([ID], [numShippingCompanyID], [numNsoftwarePackageTypeID], [vcPackageName], [bitEndicia]) VALUES (CAST(30 AS Numeric(18, 0)), CAST(88 AS Numeric(18, 0)), CAST(43 AS Numeric(18, 0)), N'UPS BPM Flat', 0)
GO
INSERT [dbo].[ShippingPackageType] ([ID], [numShippingCompanyID], [numNsoftwarePackageTypeID], [vcPackageName], [bitEndicia]) VALUES (CAST(31 AS Numeric(18, 0)), CAST(88 AS Numeric(18, 0)), CAST(44 AS Numeric(18, 0)), N'UPS Standard Flat', 0)
GO
INSERT [dbo].[ShippingPackageType] ([ID], [numShippingCompanyID], [numNsoftwarePackageTypeID], [vcPackageName], [bitEndicia]) VALUES (CAST(32 AS Numeric(18, 0)), CAST(90 AS Numeric(18, 0)), CAST(0 AS Numeric(18, 0)), N'None', 0)
GO
INSERT [dbo].[ShippingPackageType] ([ID], [numShippingCompanyID], [numNsoftwarePackageTypeID], [vcPackageName], [bitEndicia]) VALUES (CAST(33 AS Numeric(18, 0)), CAST(90 AS Numeric(18, 0)), CAST(1 AS Numeric(18, 0)), N'USPS Postcards', 0)
GO
INSERT [dbo].[ShippingPackageType] ([ID], [numShippingCompanyID], [numNsoftwarePackageTypeID], [vcPackageName], [bitEndicia]) VALUES (CAST(34 AS Numeric(18, 0)), CAST(90 AS Numeric(18, 0)), CAST(3 AS Numeric(18, 0)), N'USPS Large Envelope', 0)
GO
INSERT [dbo].[ShippingPackageType] ([ID], [numShippingCompanyID], [numNsoftwarePackageTypeID], [vcPackageName], [bitEndicia]) VALUES (CAST(35 AS Numeric(18, 0)), CAST(90 AS Numeric(18, 0)), CAST(4 AS Numeric(18, 0)), N'USPS Flat Rate Envelope', 0)
GO
INSERT [dbo].[ShippingPackageType] ([ID], [numShippingCompanyID], [numNsoftwarePackageTypeID], [vcPackageName], [bitEndicia]) VALUES (CAST(36 AS Numeric(18, 0)), CAST(90 AS Numeric(18, 0)), CAST(5 AS Numeric(18, 0)), N'USPS Flat Rate Legal Envelope', 0)
GO
INSERT [dbo].[ShippingPackageType] ([ID], [numShippingCompanyID], [numNsoftwarePackageTypeID], [vcPackageName], [bitEndicia]) VALUES (CAST(37 AS Numeric(18, 0)), CAST(90 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), N'USPS Flat Rate Padded Envelope', 0)
GO
INSERT [dbo].[ShippingPackageType] ([ID], [numShippingCompanyID], [numNsoftwarePackageTypeID], [vcPackageName], [bitEndicia]) VALUES (CAST(38 AS Numeric(18, 0)), CAST(90 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), N'USPS Flat Rate GiftCard Envelope', 0)
GO
INSERT [dbo].[ShippingPackageType] ([ID], [numShippingCompanyID], [numNsoftwarePackageTypeID], [vcPackageName], [bitEndicia]) VALUES (CAST(39 AS Numeric(18, 0)), CAST(90 AS Numeric(18, 0)), CAST(8 AS Numeric(18, 0)), N'USPS Flat Rate Window Envelope', 0)
GO
INSERT [dbo].[ShippingPackageType] ([ID], [numShippingCompanyID], [numNsoftwarePackageTypeID], [vcPackageName], [bitEndicia]) VALUES (CAST(40 AS Numeric(18, 0)), CAST(90 AS Numeric(18, 0)), CAST(10 AS Numeric(18, 0)), N'USPS Small Flat Rate Envelope', 0)
GO
INSERT [dbo].[ShippingPackageType] ([ID], [numShippingCompanyID], [numNsoftwarePackageTypeID], [vcPackageName], [bitEndicia]) VALUES (CAST(41 AS Numeric(18, 0)), CAST(90 AS Numeric(18, 0)), CAST(18 AS Numeric(18, 0)), N'USPS Flat Rate Box', 0)
GO
INSERT [dbo].[ShippingPackageType] ([ID], [numShippingCompanyID], [numNsoftwarePackageTypeID], [vcPackageName], [bitEndicia]) VALUES (CAST(42 AS Numeric(18, 0)), CAST(90 AS Numeric(18, 0)), CAST(19 AS Numeric(18, 0)), N'USPS Small Flat Rate Box', 0)
GO
INSERT [dbo].[ShippingPackageType] ([ID], [numShippingCompanyID], [numNsoftwarePackageTypeID], [vcPackageName], [bitEndicia]) VALUES (CAST(43 AS Numeric(18, 0)), CAST(90 AS Numeric(18, 0)), CAST(20 AS Numeric(18, 0)), N'USPS Medium Flat Rate Box', 0)
GO
INSERT [dbo].[ShippingPackageType] ([ID], [numShippingCompanyID], [numNsoftwarePackageTypeID], [vcPackageName], [bitEndicia]) VALUES (CAST(44 AS Numeric(18, 0)), CAST(90 AS Numeric(18, 0)), CAST(21 AS Numeric(18, 0)), N'USPS Large Flat Rate Box', 0)
GO
INSERT [dbo].[ShippingPackageType] ([ID], [numShippingCompanyID], [numNsoftwarePackageTypeID], [vcPackageName], [bitEndicia]) VALUES (CAST(45 AS Numeric(18, 0)), CAST(90 AS Numeric(18, 0)), CAST(24 AS Numeric(18, 0)), N'USPS Regional Rate Box A', 0)
GO
INSERT [dbo].[ShippingPackageType] ([ID], [numShippingCompanyID], [numNsoftwarePackageTypeID], [vcPackageName], [bitEndicia]) VALUES (CAST(46 AS Numeric(18, 0)), CAST(90 AS Numeric(18, 0)), CAST(25 AS Numeric(18, 0)), N'USPS Regional Rate Box B', 0)
GO
INSERT [dbo].[ShippingPackageType] ([ID], [numShippingCompanyID], [numNsoftwarePackageTypeID], [vcPackageName], [bitEndicia]) VALUES (CAST(47 AS Numeric(18, 0)), CAST(90 AS Numeric(18, 0)), CAST(26 AS Numeric(18, 0)), N'USPS Rectangular', 0)
GO
INSERT [dbo].[ShippingPackageType] ([ID], [numShippingCompanyID], [numNsoftwarePackageTypeID], [vcPackageName], [bitEndicia]) VALUES (CAST(48 AS Numeric(18, 0)), CAST(90 AS Numeric(18, 0)), CAST(27 AS Numeric(18, 0)), N'USPS Non Rectangular', 0)
GO
INSERT [dbo].[ShippingPackageType] ([ID], [numShippingCompanyID], [numNsoftwarePackageTypeID], [vcPackageName], [bitEndicia]) VALUES (CAST(49 AS Numeric(18, 0)), CAST(90 AS Numeric(18, 0)), CAST(29 AS Numeric(18, 0)), N'USPS Matter For The Blind', 0)
GO
INSERT [dbo].[ShippingPackageType] ([ID], [numShippingCompanyID], [numNsoftwarePackageTypeID], [vcPackageName], [bitEndicia]) VALUES (CAST(50 AS Numeric(18, 0)), CAST(90 AS Numeric(18, 0)), CAST(31 AS Numeric(18, 0)), N'Your Packaging', 0)
GO
INSERT [dbo].[ShippingPackageType] ([ID], [numShippingCompanyID], [numNsoftwarePackageTypeID], [vcPackageName], [bitEndicia]) VALUES (CAST(51 AS Numeric(18, 0)), CAST(90 AS Numeric(18, 0)), CAST(45 AS Numeric(18, 0)), N'USPS Regional Rate Box C', 0)
GO
INSERT [dbo].[ShippingPackageType] ([ID], [numShippingCompanyID], [numNsoftwarePackageTypeID], [vcPackageName], [bitEndicia]) VALUES (CAST(52 AS Numeric(18, 0)), CAST(90 AS Numeric(18, 0)), CAST(1 AS Numeric(18, 0)), N'USPS Postcards', 1)
GO
INSERT [dbo].[ShippingPackageType] ([ID], [numShippingCompanyID], [numNsoftwarePackageTypeID], [vcPackageName], [bitEndicia]) VALUES (CAST(53 AS Numeric(18, 0)), CAST(90 AS Numeric(18, 0)), CAST(2 AS Numeric(18, 0)), N'USPS Letter', 1)
GO
INSERT [dbo].[ShippingPackageType] ([ID], [numShippingCompanyID], [numNsoftwarePackageTypeID], [vcPackageName], [bitEndicia]) VALUES (CAST(54 AS Numeric(18, 0)), CAST(90 AS Numeric(18, 0)), CAST(3 AS Numeric(18, 0)), N'USPS Large Envelope', 1)
GO
INSERT [dbo].[ShippingPackageType] ([ID], [numShippingCompanyID], [numNsoftwarePackageTypeID], [vcPackageName], [bitEndicia]) VALUES (CAST(55 AS Numeric(18, 0)), CAST(90 AS Numeric(18, 0)), CAST(4 AS Numeric(18, 0)), N'USPS Flat Rate Envelope', 1)
GO
INSERT [dbo].[ShippingPackageType] ([ID], [numShippingCompanyID], [numNsoftwarePackageTypeID], [vcPackageName], [bitEndicia]) VALUES (CAST(56 AS Numeric(18, 0)), CAST(90 AS Numeric(18, 0)), CAST(5 AS Numeric(18, 0)), N'USPS Flat Rate Legal Envelope', 1)
GO
INSERT [dbo].[ShippingPackageType] ([ID], [numShippingCompanyID], [numNsoftwarePackageTypeID], [vcPackageName], [bitEndicia]) VALUES (CAST(57 AS Numeric(18, 0)), CAST(90 AS Numeric(18, 0)), CAST(6 AS Numeric(18, 0)), N'USPS Flat Rate Padded Envelope', 1)
GO
INSERT [dbo].[ShippingPackageType] ([ID], [numShippingCompanyID], [numNsoftwarePackageTypeID], [vcPackageName], [bitEndicia]) VALUES (CAST(58 AS Numeric(18, 0)), CAST(90 AS Numeric(18, 0)), CAST(7 AS Numeric(18, 0)), N'USPS Flat Rate GiftCard Envelope', 1)
GO
INSERT [dbo].[ShippingPackageType] ([ID], [numShippingCompanyID], [numNsoftwarePackageTypeID], [vcPackageName], [bitEndicia]) VALUES (CAST(59 AS Numeric(18, 0)), CAST(90 AS Numeric(18, 0)), CAST(8 AS Numeric(18, 0)), N'USPS Flat Rate Window Envelope', 1)
GO
INSERT [dbo].[ShippingPackageType] ([ID], [numShippingCompanyID], [numNsoftwarePackageTypeID], [vcPackageName], [bitEndicia]) VALUES (CAST(60 AS Numeric(18, 0)), CAST(90 AS Numeric(18, 0)), CAST(9 AS Numeric(18, 0)), N'USPS Flat Rate Cardboard Envelope', 1)
GO
INSERT [dbo].[ShippingPackageType] ([ID], [numShippingCompanyID], [numNsoftwarePackageTypeID], [vcPackageName], [bitEndicia]) VALUES (CAST(61 AS Numeric(18, 0)), CAST(90 AS Numeric(18, 0)), CAST(10 AS Numeric(18, 0)), N'USPS Small Flat Rate Envelope', 1)
GO
INSERT [dbo].[ShippingPackageType] ([ID], [numShippingCompanyID], [numNsoftwarePackageTypeID], [vcPackageName], [bitEndicia]) VALUES (CAST(62 AS Numeric(18, 0)), CAST(90 AS Numeric(18, 0)), CAST(19 AS Numeric(18, 0)), N'USPS Small Flat Rate Box', 1)
GO
INSERT [dbo].[ShippingPackageType] ([ID], [numShippingCompanyID], [numNsoftwarePackageTypeID], [vcPackageName], [bitEndicia]) VALUES (CAST(63 AS Numeric(18, 0)), CAST(90 AS Numeric(18, 0)), CAST(20 AS Numeric(18, 0)), N'USPS Medium Flat Rate Box', 1)
GO
INSERT [dbo].[ShippingPackageType] ([ID], [numShippingCompanyID], [numNsoftwarePackageTypeID], [vcPackageName], [bitEndicia]) VALUES (CAST(64 AS Numeric(18, 0)), CAST(90 AS Numeric(18, 0)), CAST(21 AS Numeric(18, 0)), N'USPS Large Flat Rate Box', 1)
GO
INSERT [dbo].[ShippingPackageType] ([ID], [numShippingCompanyID], [numNsoftwarePackageTypeID], [vcPackageName], [bitEndicia]) VALUES (CAST(65 AS Numeric(18, 0)), CAST(90 AS Numeric(18, 0)), CAST(22 AS Numeric(18, 0)), N'USPS DVD Flat Rate Box', 1)
GO
INSERT [dbo].[ShippingPackageType] ([ID], [numShippingCompanyID], [numNsoftwarePackageTypeID], [vcPackageName], [bitEndicia]) VALUES (CAST(66 AS Numeric(18, 0)), CAST(90 AS Numeric(18, 0)), CAST(23 AS Numeric(18, 0)), N'USPS Large Video Flat Rate Box', 1)
GO
INSERT [dbo].[ShippingPackageType] ([ID], [numShippingCompanyID], [numNsoftwarePackageTypeID], [vcPackageName], [bitEndicia]) VALUES (CAST(67 AS Numeric(18, 0)), CAST(90 AS Numeric(18, 0)), CAST(24 AS Numeric(18, 0)), N'USPS Regional Rate Box A', 1)
GO
INSERT [dbo].[ShippingPackageType] ([ID], [numShippingCompanyID], [numNsoftwarePackageTypeID], [vcPackageName], [bitEndicia]) VALUES (CAST(68 AS Numeric(18, 0)), CAST(90 AS Numeric(18, 0)), CAST(25 AS Numeric(18, 0)), N'USPS Regional Rate Box B', 1)
GO
INSERT [dbo].[ShippingPackageType] ([ID], [numShippingCompanyID], [numNsoftwarePackageTypeID], [vcPackageName], [bitEndicia]) VALUES (CAST(69 AS Numeric(18, 0)), CAST(90 AS Numeric(18, 0)), CAST(31 AS Numeric(18, 0)), N'Your Packaging', 1)
GO
INSERT [dbo].[ShippingPackageType] ([ID], [numShippingCompanyID], [numNsoftwarePackageTypeID], [vcPackageName], [bitEndicia]) VALUES (CAST(70 AS Numeric(18, 0)), CAST(90 AS Numeric(18, 0)), CAST(45 AS Numeric(18, 0)), N'USPS Regional Rate Box C', 1)
GO
SET IDENTITY_INSERT [dbo].[ShippingPackageType] OFF
GO



