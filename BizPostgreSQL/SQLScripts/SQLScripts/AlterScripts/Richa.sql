-------------------------------------------------------------------------------------------------------------------------
------------------------------------------ PLECE NEW SCRIPTS ABOVE IT ---------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------

--GO

--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

--SET ANSI_PADDING ON
--GO

--CREATE TABLE [dbo].[ActivityAttendees](
--	[AttendeeID] [int] IDENTITY(1,1) NOT NULL,
--	[ActivityID] [int] NOT NULL,
--	[ResponseStatus] [varchar](20) NULL,
--	[AttendeeEmail] [varchar](50) NULL,
--	[AttendeeFirstName] [varchar](50) NULL,
--	[AttendeeLastName] [varchar](50) NULL,
--	[AttendeePhone] [varchar](15) NULL,
--	[AttendeePhoneExtension] [varchar](7) NULL,
--	[CompanyNameinBiz] [varchar](100) NULL,
--	[DivisionID] [numeric](18, 0) NULL,
--	[tintCRMType] [tinyint] NULL,
--	[ContactID] [numeric](18, 0) NULL,
-- CONSTRAINT [PK_ActivityAttendees] PRIMARY KEY CLUSTERED 
--(
--	[AttendeeID] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
--) ON [PRIMARY]

--GO

--SET ANSI_PADDING ON
--GO

--ALTER TABLE [dbo].[ActivityAttendees]  WITH CHECK ADD  CONSTRAINT [FK_ActivityID] FOREIGN KEY([ActivityID])
--REFERENCES [dbo].[Activity] ([ActivityID])
--GO

--ALTER TABLE [dbo].[ActivityAttendees] CHECK CONSTRAINT [FK_ActivityID]
--GO

----------------------------------------------------------

--GO

--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

--CREATE TABLE [dbo].[ActivityDisplayConfiguration](
--	[numDisplayId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
--	[numDomainId] [numeric](18, 0) NULL,
--	[numUserCntId] [numeric](18, 0) NULL,
--	[bitTitle] [bit] NULL,
--	[bitLocation] [bit] NULL,
--	[bitDescription] [bit] NULL,
-- CONSTRAINT [PK_ActivityDisplayConfiguration] PRIMARY KEY CLUSTERED 
--(
--	[numDisplayId] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
--) ON [PRIMARY]

--GO

----------------------------------------------------------------


--alter table Activity
--ADD HtmlLink varchar(max)

--Go

--alter table Activity
--ADD Priority numeric(18,0)

--Go

--alter table Activity
--ADD [Activity] numeric(18,0)

--Go

--alter table Activity
--ADD FollowUpStatus numeric(18,0)

--Go

--alter table Activity
--ADD Comments varchar(max)

--Go

----------------------------------------

--DECLARE @numFieldID NUMERIC(18,0)

--INSERT INTO DycFieldMaster
--(
--	numFieldID,
--	numModuleID,
--	vcFieldName,
--	vcDbColumnName,
--	vcOrigDbColumnName,
--	vcLookBackTableName,
--	vcFieldDataType,
--	vcFieldType,
--	vcAssociatedControlType,
--	vcListItemType,
--	numListID,
--	bitInResults,
--	bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering
--)
--VALUES
--(
--	50921,
--	1,
--	'Action-Item Participants',
--	'Action-Item Participants',
--	'Action-Item Participants',
--	'',
--	'V',
--	'R',
--	'TextArea',
--	'',
--	0,
--	0,
--	0,0,0,1,0,0,1,1
--)


--SET @numFieldID = SCOPE_IDENTITY()

--Select * from DycFormConfigurationDetails where numFieldId=50921
--	IF NOT EXISTS (SELECT * FROM DycFormConfigurationDetails WHERE numFormId = 43 AND numFieldId = 189 AND numDomainId = 72)
--	BEGIN
--		INSERT INTO DycFormConfigurationDetails ([numFormId],[numFieldId],[intColumnNum],[intRowNum],[numDomainId],[numUserCntID],[numRelCntType],[tintPageType],[bitCustom])
--		 VALUES (43,@numFieldID,1,2,72,17,0,1,0)
--	END

	

--INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],vcFieldName,vcAssociatedControlType,[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit]) 
--VALUES (1,@numFieldID,43,'Action-Item Participants','TextArea',1,1,1,1)