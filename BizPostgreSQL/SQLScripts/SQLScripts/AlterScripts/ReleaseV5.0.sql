/******************************************************************
Project: Release 5.0 Date: 09.October.2015
Comments: ALTER SCRIPTS
*******************************************************************/


ALTER TABLE BizDocTemplate ADD
bitDisplayKitChild BIT

-------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------


/****** Object:  Table [dbo].[OpportunityKitChildItems]    Script Date: 22-Sep-15 10:26:31 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[OpportunityKitChildItems](
	[numOppKitChildItemID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numOppID] [numeric](18, 0) NOT NULL,
	[numOppItemID] [numeric](18, 0) NOT NULL,
	[numOppChildItemID] [numeric](18, 0) NOT NULL,
	[numItemID] [numeric](18, 0) NOT NULL,
	[numWareHouseItemId] [numeric](18, 0) NULL,
	[numQtyItemsReq] [int] NULL,
	[numQtyItemsReq_Orig] [int] NULL,
	[numUOMId] [numeric](18, 0) NULL,
	[numQtyShipped] [int] NULL,
 CONSTRAINT [PK_OpportunityKitChildItems] PRIMARY KEY CLUSTERED 
(
	[numOppKitChildItemID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[OpportunityKitChildItems]  WITH CHECK ADD  CONSTRAINT [FK_OpportunityKitChildItems_OpportunityKitItems] FOREIGN KEY([numOppChildItemID])
REFERENCES [dbo].[OpportunityKitItems] ([numOppChildItemID])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[OpportunityKitChildItems] CHECK CONSTRAINT [FK_OpportunityKitChildItems_OpportunityKitItems]
GO

-------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------

INSERT INTO PageNavigationDTl 
(
	numPageNavID,numModuleID,numParentID,vcPageNavName,vcNavURL,bitVisible,numTabID
)
VALUES
(
	(SELECT MAX(numPageNavID) + 1 FROM PageNavigationDTl),36,86,'Custom Report','../Service/frmCustomReports.aspx',1,-2
)

-------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------

GO

/****** Object:  Table [dbo].[CustomQueryReport]    Script Date: 30-Sep-15 10:49:43 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[CustomQueryReport](
	[numReportID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numDomainID] [numeric](18, 0) NOT NULL,
	[vcReportName] [varchar](300) NOT NULL,
	[vcReportDescription] [varchar](300) NULL,
	[vcEmailTo] [varchar](1000) NULL,
	[tintEmailFrequency] [tinyint] NULL,
	[vcQuery] [text] NOT NULL,
	[vcCSS] [varchar](max) NULL,
	[dtCreatedDate] [datetime] NULL,
 CONSTRAINT [PK_CustomQueryReport] PRIMARY KEY CLUSTERED 
(
	[numReportID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING ON
GO

-------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------

GO

/****** Object:  Index [CQR_NCI_numReportID_numDomainID]    Script Date: 30-Sep-15 10:51:16 AM ******/
CREATE NONCLUSTERED INDEX [CQR_NCI_numReportID_numDomainID] ON [dbo].[CustomQueryReport]
(
	[numReportID] ASC,
	[numDomainID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

-------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------

GO

/****** Object:  Table [dbo].[CustomQueryReportEmail]    Script Date: 30-Sep-15 10:49:49 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[CustomQueryReportEmail](
	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numReportID] [numeric](18, 0) NOT NULL,
	[tintEmailFrequency] [tinyint] NOT NULL,
	[dtDateToSend] [date] NOT NULL,
	[dtSentDate] [date] NOT NULL,
 CONSTRAINT [PK_CustomQueryReportEmail] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[CustomQueryReportEmail]  WITH CHECK ADD  CONSTRAINT [FK_CustomQueryReportEmail_CustomQueryReport] FOREIGN KEY([numReportID])
REFERENCES [dbo].[CustomQueryReport] ([numReportID])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[CustomQueryReportEmail] CHECK CONSTRAINT [FK_CustomQueryReportEmail_CustomQueryReport]
GO

-------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------


INSERT INTO ReportList 
(
	RptID,NumId,RptHeading,RptDesc,rptGroup,rptSequence,bitEnable
)
VALUES
(
	 0,0,'Custom Reports','',0,26,1
)

-------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------

--ADD KEY IN BizService Config
<add key="EmailCustomQueryReport" value="360" />


-------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------


SET IDENTITY_INSERT AccountingChargeTypes ON 

INSERT INTO AccountingChargeTypes
(
	numChargeTypeId,
	vcChageType,
	vcChargeAccountCode,
	chChargeCode
)
VALUES
(
	26,
	'Deferred Income',
	'',
	'DI'
)

SET IDENTITY_INSERT AccountingChargeTypes OFF


-------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------

BEGIN TRANSACTION

DECLARE @TMEP TABLE
(
	ID INT IDENTITY(1,1),
	numDomainID NUMERIC(18,0)
)

INSERT INTO @TMEP SELECT numDomainID FROM Domain WHERE numDomainId > 0 ORDER BY numDomainId


DECLARE @i AS INT = 1
DECLARE @numDomainID NUMERIC(18,0)
DECLARE @Count AS INT
SELECT @Count=COUNT(*) FROM @TMEP

DECLARE @dtTodayDate AS DATETIME
SET @dtTodayDate = GETDATE()    

WHILE @i <= @Count
BEGIN
	SELECT @numDomainID = numDomainID FROM @TMEP WHERE ID=@i

	DECLARE @numLiabilityAccountTypeID  AS NUMERIC(9,0) = 0
	SELECT @numLiabilityAccountTypeID = ISNULL(numAccountTypeId,0) FROM dbo.AccountTypeDetail WHERE vcAccountCode ='0102' AND numDomainID = @numDomainID 

	DECLARE @numCurrentLiabilitiesAccountTypeID AS NUMERIC(9) = 0
	SELECT @numCurrentLiabilitiesAccountTypeID = ISNULL(numAccountTypeID,0) FROM dbo.AccountTypeDetail WHERE vcAccountCode = '010201'  AND numDomainID = @numDomainID  


	EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numLiabilityAccountTypeID,
		@numParntAcntTypeID = @numCurrentLiabilitiesAccountTypeID, @vcAccountName = 'Deferred Income',
		@vcAccountDescription = 'Deferred Income',
		@monOriginalOpeningBal = $0, @monOpeningBal = $0,
		@dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
		@bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
		@bitProfitLoss = 0, @bitDepreciation = 0,
		@dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
		@vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0

	DECLARE @DI AS NUMERIC(18,0) = 0
	SELECT @DI = numAccountId FROM dbo.Chart_Of_Accounts WHERE numDomainId = @numDomainId AND vcAccountName = 'Deferred Income'

	PRINT @DI

	IF ISNULL(@DI,0) > 0
	BEGIN
		INSERT INTO AccountingCharges
		 (
			[numChargeTypeId],
			[numAccountID],
			[numDomainID]
		)
		VALUES
		(
			26,
			@DI,
			@numDomainID
		)
	END

	SET @i = @i + 1
END


COMMIT

-------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------

SET IDENTITY_INSERT ListDetails ON
INSERT INTO ListDetails
( numListItemID,numListID,vcData,numCreatedBY,bitDelete,numDomainID,constFlag,sintOrder)
VALUES
(304,27,'Deferred Income',1,0,1,1,1)
SET IDENTITY_INSERT ListDetails OFF

-------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------

ALTER TABLE Domain ADD
IsEnableDeferredIncome BIT

-------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------

ALTER TABLE OpportunityBizDocs ADD
numDeferredBizDocID NUMERIC(18,0)

-------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------- KAMAL ----------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------

ALTER TABLE dbo.GenericDocuments ADD
	vcContactPosition varchar(200) NULL


UPDATE ShortCutBar SET link='../opportunity/frmAmtPaid.aspx?frm=topTab' WHERE link LIKE '%amtpaid%'


Update PageNavigationDTL SET [vcNavURL]='../Opportunity/frmAmtPaid.aspx?frm=topTab' WHERE [vcNavURL] LIKE '%amtpaid%'
