/******************************************************************
Project: Release 8.5 Date: 5.DECEMBER.2017
Comments: ALTER SCRIPTS
*******************************************************************/

/******************   SANDEEP  *****************/

ALTER TABLE OpportunityBizDocItems ALTER COLUMN vcNotes VARCHAR(1000)


UPDATE ListMaster SET vcListName='Status' WHERE numListID=176

ALTER TABLE ListDetails ADD tintOppOrOrder TINYINT

UPDATE DycFormField_Mapping SET bitWorkFlowField=0 WHERE numFormID=68 AND numFieldID IN (82,83,84,85,86,222,473,24,386,38,32,10,387,217,17,47,48,49,50)
UPDATE DycFormField_Mapping SET bitWorkFlowField=0 WHERE numFormID=69 AND numFieldID IN (58,77,75,76,78,79,94,54,61,92,53,63,51,62,52,59,60,67,71,74,80,82,83,84,85,86)
UPDATE DycFormField_Mapping SET bitWorkFlowField=0 WHERE numFormID=70 AND numFieldID IN (99,122,448,464,691,96,694)
UPDATE DycFormField_Mapping SET bitWorkFlowField=0 WHERE numFormID=124 AND numFieldID IN (547,546)

UPDATE DycFormField_Mapping SET bitWorkFlowField=0 WHERE numFormID=72 AND numFieldID IN (139,128,369,145,494,144,132)
UPDATE DycFormField_Mapping SET bitWorkFlowField=0 WHERE numFormID=73 AND numFieldID IN (163,155,152,154,149,146,148,147,162)
UPDATE DycFormField_Mapping SET bitWorkFlowField=0 WHERE numFormID=49 AND numFieldID IN (268)





UPDATE DycFieldMaster SET vcGroup='Org. | Accounting Fields' WHERE numFieldId IN (31,417,418)


UPDATE DycFieldMaster SET vcGroup='Org/Accounting sub-tab' WHERE vcGroup='Org. | Accounting Fields'
UPDATE DycFieldMaster SET vcGroup='Organization Detail Fields' WHERE vcGroup='Org.Details Fields'
UPDATE DycFieldMaster SET bitWorkFlowField=1,vcGroup='Org/Accounting sub-tab' WHERE vcFieldName='Class' AND numModuleID = 1 AND vcOrigDbColumnName='numAccountClassID' AND vcLookBackTableName='DivisionMaster'
UPDATE DycFieldMaster SET bitWorkFlowField=1,vcGroup='Org/Accounting sub-tab' WHERE vcFieldName='Price Level' AND numModuleID = 1 AND vcOrigDbColumnName='tintPriceLevel' AND vcLookBackTableName='DivisionMaster'
UPDATE DycFieldMaster SET bitWorkFlowField=1,vcGroup='Org/Accounting sub-tab' WHERE numFieldId=538
UPDATE DycFieldMaster SET bitWorkFlowField=1,vcGroup='Org/Accounting sub-tab' WHERE vcFieldName='Ship Via' AND numModuleID = 1 AND vcOrigDbColumnName='intShippingCompany' AND vcLookBackTableName='DivisionMaster'



-------------------------------------

DECLARE @numFieldID NUMERIC(18,0)
SELECT @numFieldID=numFieldId FROM DycFieldMaster WHERE vcFieldName='Class' AND numModuleID = 1 AND vcOrigDbColumnName='numAccountClassID' AND vcLookBackTableName='DivisionMaster'

INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitImport,bitExport,bitAllowFiltering,intSectionID,bitAllowGridColor
)
VALUES
(
	1,@numFieldID,68,1,0,'Class','SelectBox','AccountClass',1,1,1,0,0,0,0,0,0,1,0,0,0,1,0
)

-------------------------------------

INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitImport,bitExport,bitAllowFiltering,intSectionID,bitAllowGridColor
)
VALUES
(
	3,573,68,1,0,'Net Terms','SelectBox','BillingDays',1,1,1,0,0,0,0,0,0,1,0,0,0,1,0
)

-------------------------------------

DECLARE @numFieldID NUMERIC(18,0)
SELECT @numFieldID=numFieldId FROM DycFieldMaster WHERE vcFieldName='Price Level' AND numModuleID = 1 AND vcOrigDbColumnName='tintPriceLevel' AND vcLookBackTableName='DivisionMaster'

INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitImport,bitExport,bitAllowFiltering,intSectionID,bitAllowGridColor
)
VALUES
(
	1,@numFieldID,68,1,0,'Item Price Level','SelectBox','PriceLevel',1,1,1,0,0,0,0,0,0,1,0,0,0,1,0
)

-------------------------------------

INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitImport,bitExport,bitAllowFiltering,intSectionID,bitAllowGridColor
)
VALUES
(
	1,538,68,1,0,'Preferred Payment Method','SelectBox','DefaultPaymentMethod',1,1,1,0,0,0,0,0,0,1,0,0,0,1,0
)
------------------------------------

DECLARE @numFieldID NUMERIC(18,0)
SELECT @numFieldID=numFieldId FROM DycFieldMaster WHERE vcFieldName='Ship Via' AND numModuleID = 1 AND vcOrigDbColumnName='intShippingCompany' AND vcLookBackTableName='DivisionMaster'

INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitImport,bitExport,bitAllowFiltering,intSectionID,bitAllowGridColor
)
VALUES
(
	1,@numFieldID,68,1,0,'Preferred Ship Via','SelectBox','ShippingCompany',1,1,1,0,0,0,0,0,0,1,0,0,0,1,0
)

-----------------------------

DECLARE @numFieldID NUMERIC(18,0)
INSERT INTO DycFieldMaster
(
	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcListItemType,numListID,[order],bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitWorkFlowField,bitAllowFiltering,vcGroup
)
VALUES
(
	1,'Preferred Parcel Shipping Service','numDefaultShippingServiceID','numDefaultShippingServiceID','DefaultShippingService','DivisionMaster','N','R','SelectBox','PSS',0,1,1,0,1,0,1,1,1,'Org/Accounting sub-tab'
)
SELECT @numFieldID = SCOPE_IDENTITY()


INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitImport,bitExport,bitAllowFiltering,intSectionID,bitAllowGridColor
)
VALUES
(
	1,@numFieldID,68,1,0,'Preferred Parcel Shipping Service','SelectBox','DefaultShippingService',1,1,1,0,0,0,0,0,0,1,0,0,0,1,0
)

---------------------------
DECLARE @numClassFieldID NUMERIC(18,0)
DECLARE @numShipViaFieldID NUMERIC(18,0)
DECLARE @numParcelServiceFieldID NUMERIC(18,0)
DECLARE @numPriceLevelFieldID NUMERIC(18,0)
SELECT @numClassFieldID=numFieldId FROM DycFieldMaster WHERE vcFieldName='Class' AND numModuleID = 1 AND vcOrigDbColumnName='numAccountClassID' AND vcLookBackTableName='DivisionMaster'
SELECT @numShipViaFieldID=numFieldId FROM DycFieldMaster WHERE vcFieldName='Ship Via' AND numModuleID = 1 AND vcOrigDbColumnName='intShippingCompany' AND vcLookBackTableName='DivisionMaster'
SELECT @numParcelServiceFieldID=numFieldId FROM DycFieldMaster WHERE vcFieldName='Preferred Parcel Shipping Service' AND numModuleID = 1 AND vcOrigDbColumnName='numDefaultShippingServiceID' AND vcLookBackTableName='DivisionMaster'
SELECT @numPriceLevelFieldID=numFieldId FROM DycFieldMaster WHERE vcFieldName='Price Level' AND numModuleID = 1 AND vcOrigDbColumnName='tintPriceLevel' AND vcLookBackTableName='DivisionMaster'



DELETE D1 FROM DycFormField_Mapping D1 INNER JOIN DycFieldMaster ON D1.numFieldID=DycFieldMaster.numFieldId WHERE numFormID IN (69,70,72,73,124,49) AND vcGroup IN ('Organization Detail Fields','Org/Accounting sub-tab')

INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitImport,bitExport,bitAllowFiltering
)
SELECT
	1,numFieldID,69,1,0,vcFieldName,vcAssociatedControlType,1,1,0,0,0,0,0,0,1,0,0,0
FROM
	DycFormField_Mapping
WHERE
	numFormID=68
	AND numFieldID IN (16,14,224,226,7,15,18,19,28,22,3,30,29,5,6,451,221,219,677,677,@numClassFieldID,418,573,538,@numShipViaFieldID,@numParcelServiceFieldID,678,417,@numPriceLevelFieldID)

INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitImport,bitExport,bitAllowFiltering
)
SELECT
	1,numFieldID,70,1,0,vcFieldName,vcAssociatedControlType,1,1,0,0,0,0,0,0,1,0,0,0
FROM
	DycFormField_Mapping
WHERE
	numFormID=68
	AND numFieldID IN (16,14,224,226,7,15,18,19,28,22,3,30,29,5,6,451,221,219,677,677,@numClassFieldID,418,573,538,@numShipViaFieldID,@numParcelServiceFieldID,678,417,@numPriceLevelFieldID)

INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitImport,bitExport,bitAllowFiltering
)
SELECT
	1,numFieldID,72,1,0,vcFieldName,vcAssociatedControlType,1,1,0,0,0,0,0,0,1,0,0,0
FROM
	DycFormField_Mapping
WHERE
	numFormID=68
	AND numFieldID IN (16,14,224,226,7,15,18,19,28,22,3,30,29,5,6,451,221,219,677,677,@numClassFieldID,418,573,538,@numShipViaFieldID,@numParcelServiceFieldID,678,417,@numPriceLevelFieldID)

INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitImport,bitExport,bitAllowFiltering
)
SELECT
	1,numFieldID,73,1,0,vcFieldName,vcAssociatedControlType,1,1,0,0,0,0,0,0,1,0,0,0
FROM
	DycFormField_Mapping
WHERE
	numFormID=68
	AND numFieldID IN (16,14,224,226,7,15,18,19,28,22,3,30,29,5,6,451,221,219,677,677,@numClassFieldID,418,573,538,@numShipViaFieldID,@numParcelServiceFieldID,678,417,@numPriceLevelFieldID)

INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitImport,bitExport,bitAllowFiltering
)
SELECT
	1,numFieldID,124,1,0,vcFieldName,vcAssociatedControlType,1,1,0,0,0,0,0,0,1,0,0,0
FROM
	DycFormField_Mapping
WHERE
	numFormID=68
	AND numFieldID IN (16,14,224,226,7,15,18,19,28,22,3,30,29,5,6,451,221,219,677,677,@numClassFieldID,418,573,538,@numShipViaFieldID,@numParcelServiceFieldID,678,417,@numPriceLevelFieldID)

INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitImport,bitExport,bitAllowFiltering
)
SELECT
	1,numFieldID,49,1,0,vcFieldName,vcAssociatedControlType,1,1,0,0,0,0,0,0,1,0,0,0
FROM
	DycFormField_Mapping
WHERE
	numFormID=68
	AND numFieldID IN (16,14,224,226,7,15,18,19,28,22,3,30,29,5,6,451,221,219,677,677,@numClassFieldID,418,573,538,@numShipViaFieldID,@numParcelServiceFieldID,678,417,@numPriceLevelFieldID)

----------------------

DECLARE @numFieldPartner NUMERIC(18,0)
DECLARE @numFieldPartnerContact NUMERIC(18,0)
DECLARE @numFieldReleaseDate NUMERIC(18,0)
DECLARE @numFieldReleaseStatus NUMERIC(18,0)
DECLARE @numFieldShipVia NUMERIC(18,0)
DECLARE @numFieldInventoryStatus NUMERIC(18,0)
DECLARE @numFieldPerComplete NUMERIC(18,0)

SELECT @numFieldPartner=numFieldId FROM DycFieldMaster WHERE vcOrigDbColumnName='numPartner' AND vcLookBackTableName='OpportunityMaster'
SELECT @numFieldPartnerContact=numFieldId FROM DycFieldMaster WHERE vcOrigDbColumnName='numPartenerContact' AND vcLookBackTableName='OpportunityMaster'
SELECT @numFieldReleaseDate=numFieldId FROM DycFieldMaster WHERE vcOrigDbColumnName='dtReleaseDate' AND vcLookBackTableName='OpportunityMaster'
SELECT @numFieldReleaseStatus=numFieldId FROM DycFieldMaster WHERE vcOrigDbColumnName='numReleaseStatus' AND vcLookBackTableName='OpportunityMaster'
SELECT @numFieldShipVia=numFieldId FROM DycFieldMaster WHERE vcOrigDbColumnName='numShipmentMethod' AND vcLookBackTableName='OpportunityMaster' AND numListID=82
SELECT @numFieldInventoryStatus=numFieldId FROM DycFieldMaster WHERE vcOrigDbColumnName='vcInventoryStatus' AND vcLookBackTableName='OpportunityItems'
SELECT @numFieldPerComplete=numFieldId FROM DycFieldMaster WHERE vcFieldName='Percentage Complete' ANd vcLookBackTableName='OpportunityMaster'

UPDATE DycFieldMaster SET bitWorkFlowField=1,vcGroup='Order Fields' WHERE numFieldId IN (@numFieldPartner,@numFieldPartnerContact,@numFieldReleaseDate,@numFieldReleaseStatus,@numFieldShipVia,@numFieldInventoryStatus,@numFieldPerComplete)

INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitImport,bitExport,bitAllowFiltering
)
VALUES
(
	3,@numFieldPartner,70,1,0,'Partner Source','SelectBox',1,1,0,0,0,0,0,0,1,0,0,0
),
(
	3,@numFieldPartnerContact,70,1,0,'Partner Contact','SelectBox',1,1,0,0,0,0,0,0,1,0,0,0
),
(
	3,@numFieldReleaseDate,70,1,0,'Release Date','DateField',1,1,0,0,0,0,0,0,1,0,0,0
),
(
	3,@numFieldReleaseStatus,70,1,0,'Release Status','SelectBox',1,1,0,0,0,0,0,0,1,0,0,0
),
(
	3,@numFieldShipVia,70,1,0,'Ship Via','SelectBox',1,1,0,0,0,0,0,0,1,0,0,0
),
(
	3,@numFieldInventoryStatus,70,1,0,'Inventory Status','SelectBox',1,1,0,0,0,0,0,0,1,0,0,0
),
(
	3,@numFieldPerComplete,70,1,0,'Percentage Complete','SelectBox',1,1,0,0,0,0,0,0,1,0,0,0
)

----------------------------

UPDATE DycFormField_Mapping SET vcFieldName='Opportunity/Order Type' WHERE numFormID=70 AND vcFieldName='Opp Type'
UPDATE DycFormField_Mapping SET vcFieldName='Opportunity/Order Status' WHERE numFormID=70 AND vcFieldName='Order Status'
UPDATE DycFormField_Mapping SET vcFieldName='Opportunity/Order Sub-total' WHERE numFormID=70 AND vcFieldName='Order Sub-total'

--------------------------------

BEGIN TRY
BEGIN TRANSACTION
	INSERT INTO PageMaster
	(
		numPageID,numModuleID,vcFileName,vcPageDesc,bitIsViewApplicable,bitIsAddApplicable,bitIsUpdateApplicable,bitIsDeleteApplicable,bitIsExportApplicable
	)
	VALUES
	(
		135,2,'frmCorrespondence.ascx','Correspondance',1,0,0,1,0
	),
	(
		135,3,'frmCorrespondence.ascx','Correspondance',1,0,0,1,0
	),
	(
		135,4,'frmCorrespondence.ascx','Correspondance',1,0,0,1,0
	),
	(
		135,11,'frmCorrespondence.ascx','Correspondance',1,0,0,1,0
	),
	(
		135,7,'frmCorrespondence.ascx','Correspondance',1,0,0,1,0
	),
	(
		135,12,'frmCorrespondence.ascx','Correspondance',1,0,0,1,0
	),
	(
		135,10,'frmCorrespondence.ascx','Correspondance',1,0,0,1,0
	)


	SELECT 
		* 
	INTO 
		#temp 
	FROM
	(
		SELECT 
			ROW_NUMBER() OVER(ORDER BY numDomainId DESC) AS Row, 
			numDomainId 
		FROM 
			Domain 
		WHERE 
			numDomainId <> -255
	) TABLE2
	
	DECLARE @RowCount INT
	SET @RowCount = (SELECT COUNT(Row) FROM #temp) 
	
	DECLARE @I INT
	DECLARE @numDomainId NUMERIC(18,0)
	
	SET @I = 1

	WHILE (@I <= @RowCount)
	BEGIN
			
		SELECT @numDomainId = numDomainID FROM #temp WHERE Row = @I
	
		-- 1. Leads - ModileID = 2
		INSERT INTO dbo.GroupAuthorization
			(numDomainID,numModuleID,numPageID,numGroupID,intExportAllowed,intPrintAllowed,intViewAllowed,intAddAllowed,intUpdateAllowed,intDeleteAllowed)
		SELECT
			@numDomainId
			,2
			,135
			,numGroupID
			,0
			,0
			,ISNULL((SELECT intViewAllowed FROM GroupAuthorization WHERE numDomainID=@numDomainId AND GroupAuthorization.numGroupID=AuthenticationGroupMaster.numGroupID AND numModuleID=13 AND numPageID=24),1)
			,0
			,0
			,ISNULL((SELECT intDeleteAllowed FROM GroupAuthorization WHERE numDomainID=@numDomainId AND GroupAuthorization.numGroupID=AuthenticationGroupMaster.numGroupID AND numModuleID=13 AND numPageID=24),1)
		FROM
			AuthenticationGroupMaster
		WHERE
			numDomainID = @numDomainId AND
			tintGroupType = 1

		-- 2. Prospects - ModileID = 3
		INSERT INTO dbo.GroupAuthorization
			(numDomainID,numModuleID,numPageID,numGroupID,intExportAllowed,intPrintAllowed,intViewAllowed,intAddAllowed,intUpdateAllowed,intDeleteAllowed)
		SELECT
			@numDomainId,3,135,numGroupID
			,0
			,0
			,ISNULL((SELECT intViewAllowed FROM GroupAuthorization WHERE numDomainID=@numDomainId AND GroupAuthorization.numGroupID=AuthenticationGroupMaster.numGroupID AND numModuleID=13 AND numPageID=24),1)
			,0
			,0
			,ISNULL((SELECT intDeleteAllowed FROM GroupAuthorization WHERE numDomainID=@numDomainId AND GroupAuthorization.numGroupID=AuthenticationGroupMaster.numGroupID AND numModuleID=13 AND numPageID=24),1)
		FROM
			AuthenticationGroupMaster
		WHERE
			numDomainID = @numDomainId AND
			tintGroupType = 1

		-- 3. Accounts - ModileID = 4
		INSERT INTO dbo.GroupAuthorization
			(numDomainID,numModuleID,numPageID,numGroupID,intExportAllowed,intPrintAllowed,intViewAllowed,intAddAllowed,intUpdateAllowed,intDeleteAllowed)
		SELECT
			@numDomainId,4,135,numGroupID
			,0
			,0
			,ISNULL((SELECT intViewAllowed FROM GroupAuthorization WHERE numDomainID=@numDomainId AND GroupAuthorization.numGroupID=AuthenticationGroupMaster.numGroupID AND numModuleID=13 AND numPageID=24),1)
			,0
			,0
			,ISNULL((SELECT intDeleteAllowed FROM GroupAuthorization WHERE numDomainID=@numDomainId AND GroupAuthorization.numGroupID=AuthenticationGroupMaster.numGroupID AND numModuleID=13 AND numPageID=24),1)
		FROM
			AuthenticationGroupMaster
		WHERE
			numDomainID = @numDomainId AND
			tintGroupType = 1

		-- 4. Contacts - ModileID = 11
		INSERT INTO dbo.GroupAuthorization
			(numDomainID,numModuleID,numPageID,numGroupID,intExportAllowed,intPrintAllowed,intViewAllowed,intAddAllowed,intUpdateAllowed,intDeleteAllowed)
		SELECT
			@numDomainId,11,135,numGroupID
			,0
			,0
			,ISNULL((SELECT intViewAllowed FROM GroupAuthorization WHERE numDomainID=@numDomainId AND GroupAuthorization.numGroupID=AuthenticationGroupMaster.numGroupID AND numModuleID=13 AND numPageID=24),1)
			,0
			,0
			,ISNULL((SELECT intDeleteAllowed FROM GroupAuthorization WHERE numDomainID=@numDomainId AND GroupAuthorization.numGroupID=AuthenticationGroupMaster.numGroupID AND numModuleID=13 AND numPageID=24),1)
		FROM
			AuthenticationGroupMaster
		WHERE
			numDomainID = @numDomainId AND
			tintGroupType = 1

		-- 5. Cases - ModileID = 7
		INSERT INTO dbo.GroupAuthorization
			(numDomainID,numModuleID,numPageID,numGroupID,intExportAllowed,intPrintAllowed,intViewAllowed,intAddAllowed,intUpdateAllowed,intDeleteAllowed)
		SELECT
			@numDomainId,7,135,numGroupID
			,0
			,0
			,ISNULL((SELECT intViewAllowed FROM GroupAuthorization WHERE numDomainID=@numDomainId AND GroupAuthorization.numGroupID=AuthenticationGroupMaster.numGroupID AND numModuleID=13 AND numPageID=24),1)
			,0
			,0
			,ISNULL((SELECT intDeleteAllowed FROM GroupAuthorization WHERE numDomainID=@numDomainId AND GroupAuthorization.numGroupID=AuthenticationGroupMaster.numGroupID AND numModuleID=13 AND numPageID=24),1)
		FROM
			AuthenticationGroupMaster
		WHERE
			numDomainID = @numDomainId AND
			tintGroupType = 1

		-- 6. Projects - ModileID = 12
		INSERT INTO dbo.GroupAuthorization
			(numDomainID,numModuleID,numPageID,numGroupID,intExportAllowed,intPrintAllowed,intViewAllowed,intAddAllowed,intUpdateAllowed,intDeleteAllowed)
		SELECT
			@numDomainId,12,135,numGroupID
			,0
			,0
			,ISNULL((SELECT intViewAllowed FROM GroupAuthorization WHERE numDomainID=@numDomainId AND GroupAuthorization.numGroupID=AuthenticationGroupMaster.numGroupID AND numModuleID=13 AND numPageID=24),1)
			,0
			,0
			,ISNULL((SELECT intDeleteAllowed FROM GroupAuthorization WHERE numDomainID=@numDomainId AND GroupAuthorization.numGroupID=AuthenticationGroupMaster.numGroupID AND numModuleID=13 AND numPageID=24),1)
		FROM
			AuthenticationGroupMaster
		WHERE
			numDomainID = @numDomainId AND
			tintGroupType = 1

		-- 7. Opportunity/Orders - ModileID = 10
		INSERT INTO dbo.GroupAuthorization
			(numDomainID,numModuleID,numPageID,numGroupID,intExportAllowed,intPrintAllowed,intViewAllowed,intAddAllowed,intUpdateAllowed,intDeleteAllowed)
		SELECT
			@numDomainId,10,135,numGroupID
			,0
			,0
			,ISNULL((SELECT intViewAllowed FROM GroupAuthorization WHERE numDomainID=@numDomainId AND GroupAuthorization.numGroupID=AuthenticationGroupMaster.numGroupID AND numModuleID=13 AND numPageID=24),1)
			,0
			,0
			,ISNULL((SELECT intDeleteAllowed FROM GroupAuthorization WHERE numDomainID=@numDomainId AND GroupAuthorization.numGroupID=AuthenticationGroupMaster.numGroupID AND numModuleID=13 AND numPageID=24),1)
		FROM
			AuthenticationGroupMaster
		WHERE
			numDomainID = @numDomainId AND
			tintGroupType = 1

		SET @I = @I  + 1
	END
	
	DROP TABLE #temp
COMMIT
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		ERROR_MESSAGE(),
		ERROR_NUMBER(),
		ERROR_SEVERITY(),
		ERROR_STATE(),
		ERROR_LINE(),
		ISNULL(ERROR_PROCEDURE(), '-');
END CATCH

---------------------------------------------

INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintColumn,tintRow,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering
)
VALUES
(
	2,56,34,1,1,'Contact Type','SelectBox','ContactType',1,1,1,1,0,0,1,1,0,1,1
),
(
	2,56,35,1,1,'Contact Type','SelectBox','ContactType',1,1,1,1,0,0,1,1,0,1,1
)


UPDATE DycFormField_Mapping SET bitInResults=1,bitInlineEdit=1,bitAllowEdit=1,bitDefault=0,bitDeleted=0,bitAllowFiltering=1,bitAllowSorting=1,bitAddField=1,bitDetailField=0 WHERE numFormID=36 AND numFieldID=56

------------------------------------------------------------------------------------------------------------

INSERT INTO ReportDashboardAllowedReports
(
	numReportID
	,numDomainID
	,numGrpID
	,tintReportCategory
)
SELECT intDefaultReportID,numDomainID,numGroupID,2 FROM AuthenticationGroupMaster OUTER APPLY (
SELECT
	intDefaultReportID
FROM
	ReportListMaster
WHERE
	ISNULL(numDomainID,0) = 0
	AND bitDefault=1) AS Test

-----------------------------------------------------

BEGIN TRY
BEGIN TRANSACTION

DECLARE @numFieldID NUMERIC(18,0)

INSERT INTO DycFieldMaster
(
	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcListItemType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField
)
VALUES
(
	1,'Modified By','numModifiedBy','numModifiedBy','Communication','N','R','SelectBox','U',0,1,0,0,0,1
)

SET @numFieldID = SCOPE_IDENTITY()

INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField
)
VALUES
(
	1,@numFieldID,43,0,0,'Modified By','SelectBox',1,0,0,1,0,1
)

COMMIT
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		ERROR_MESSAGE(),
		ERROR_NUMBER(),
		ERROR_SEVERITY(),
		ERROR_STATE(),
		ERROR_LINE(),
		ISNULL(ERROR_PROCEDURE(), '-');
END CATCH