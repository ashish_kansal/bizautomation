/******************************************************************
Project: Release 8.2 Date: 16.OCTOBER.2017
Comments: ALTER SCRIPTS
*******************************************************************/

/******************   SANDEEP  *****************/

UPDATE DynamicFormMaster SET numBizFormModuleID=4,tintFlag=0 WHERE numFormId IN (26,129)
UPDATE DynamicFormMaster SET numBizFormModuleID=NULL WHERE numFormId IN (122)
UPDATE BizFormWizardMasterConfiguration SET tintPageType=1 WHERE numFormId = 26

ALTER TABLE AddressDetails ADD numContact NUMERIC(18,0)
ALTER TABLE AddressDetails ADD bitAltContact BIT
ALTER TABLE AddressDetails ADD vcAltContact VARCHAR(200)

ALTER TABLE OpportunityAddress ADD numBillingContact NUMERIC(18,0)
ALTER TABLE OpportunityAddress ADD bitAltBillingContact BIT
ALTER TABLE OpportunityAddress ADD vcAltBillingContact VARCHAR(200)
ALTER TABLE OpportunityAddress ADD numShippingContact NUMERIC(18,0)
ALTER TABLE OpportunityAddress ADD bitAltShippingContact BIT
ALTER TABLE OpportunityAddress ADD vcAltShippingContact VARCHAR(200)

ALTER TABLE ShippingReport ADD tintSignatureType TINYINT


USE [Production.2014]
GO

/****** Object:  Table [dbo].[DivisionMasterShippingConfiguration]    Script Date: 13-Oct-17 10:19:14 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[DivisionMasterShippingConfiguration](
	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numDomainID] [numeric](18, 0) NOT NULL,
	[numDivisionID] [numeric](18, 0) NOT NULL,
	[IsAdditionalHandling] [bit] NULL,
	[IsCOD] [bit] NULL,
	[IsHomeDelivery] [bit] NULL,
	[IsInsideDelevery] [bit] NULL,
	[IsInsidePickup] [bit] NULL,
	[IsSaturdayDelivery] [bit] NULL,
	[IsSaturdayPickup] [bit] NULL,
	[IsLargePackage] [bit] NULL,
	[vcDeliveryConfirmation] [nvarchar](1000) NULL,
	[vcDescription] [nvarchar](max) NULL,
	[vcSignatureType] [nvarchar](300) NULL,
	[vcCODType] [nvarchar](50) NULL,
 CONSTRAINT [PK_DivisionMasterShippingConfiguration] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO


USE [Production.2014]
GO

/****** Object:  Index [NonClusteredIndex-20171012-140021]    Script Date: 12-Oct-17 2:01:12 PM ******/
CREATE NONCLUSTERED INDEX [NonClusteredIndex-20171012-140021] ON [dbo].[DivisionMasterShippingConfiguration]
(
	[numDomainID] ASC
)
INCLUDE ( 	[numDivisionID]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO



ALTER TABLE ElasticSearchLastReindex ADD vcModule VARCHAR(300)

UPDATE ElasticSearchLastReindex SET vcModule = 'ElasticSearchReindex' WHERE ID=1

INSERT INTO ElasticSearchLastReindex
(
	ID
	,LastReindexDate
	,vcModule
)
VALUES
(
	2
	,DATEADD(DAY,-1,GETDATE())
	,'DashboardCommissonReportCalculation'
)

INSERT INTO ReportListMaster
(
	vcReportName,vcReportDescription,numDomainID,tintReportType,textQuery,bitDefault,intDefaultReportID
)
VALUES
(
	'Top reasons why we�re wining deals','Top reasons why we�re wining deals',0,1,'EXEC USP_ReportListMaster_Top10ReasonsDealWins @numDomainID',1,26
),
(
	'Top reasons why we�re loosing deals','Top reasons why we�re loosing deals',0,1,'EXEC USP_ReportListMaster_Top10ReasonsDealLost @numDomainID',1,27
),
(
	'Top 10 Reasons for Sales Returns','Top 10 Reasons for Sales Returns',0,1,'EXEC USP_ReportListMaster_Top10ReasonsForSalesReturns @numDomainID',1,28
),
(
	'Employee Sales Performance Panel 1','Employee Sales Performance Panel 1',0,1,'EXEC USP_ReportListMaster_EmployeeSalesPerformancePanel1 @numDomainID',1,29
),
(
	'Partner Revenues, Margins & Profits (Last 12 months)','Partner Revenues, Margins & Profits (Last 12 months)',0,1,'EXEC USP_ReportListMaster_PartnerRMPLast12Months @numDomainID',1,30
),
(
	'Employee Sales Performance pt 1 (Last 12 months)','Employee Sales Performance pt 1 (Last 12 months)',0,1,'EXEC USP_ReportListMaster_EmployeeSalesPerformance1 @numDomainID',1,31
),
(
	'Employee benefit to company (Last 12 months)','Employee benefit to company (Last 12 months)',0,1,'EXEC USP_ReportListMaster_EmployeeBenefitToCompany @numDomainID,@ClientTimeZoneOffset',1,32
),
(
	'First 10 Saved Searches','First 10 Saved Searches',0,1,'EXEC USP_ReportListMaster_First10SavedSearch @numDomainID',1,33
)