/******************************************************************
Project: Release 3.6 Date: 22.SEP.2014
Comments: 
*******************************************************************/

------------------- SACHIN ------------------


	INSERT INTO DycFieldMaster
                         (numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, 
                         vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, 
                         bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, 
                         intColumnWidth, intFieldMaxLength,vcGroup)
VALUES        (3,Null,'Rental Status','bitAsset','bitAsset',Null,'Item','N','R','',Null,Null,0,Null,1,Null,Null,1,0,0,1,1,Null,Null,Null,1,Null,Null,1,Null,Null,Null,Null,Null)

--10542



INSERT INTO DycFormField_Mapping
                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
VALUES        (3,(select MAX(numFieldID) from DycFieldMaster),Null,26,1,0,'Rental Status','','bitAsset',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)

Update DycFieldMaster set bitAllowFiltering=1,vcFieldDataType='T',bitAllowEdit=1,bitDefault=1,bitSettingField=1  where numFieldId=80549
Update DycFormField_Mapping set bitInlineEdit=1,bitAllowEdit=1,bitDefault=1,bitSettingField=1 ,bitAllowFiltering=1,numFormFieldID=1382 where numFieldId=80549



Update PageNavigationDTL set vcNavURL='../Items/frmItemList.aspx?Page=Asset Items&ItemGroup=0' where numPageNavID=171 and vcPageNavName='Asset Items'
--select * from ShortCutBar where id=150
update ShortCutBar set  Link='../Items/frmItemList.aspx?Page=Asset Items&ItemGroup=0' where id=150

ALTER TABLE Item ADD
bitAsset BIT,
bitRental BIT


ALTER TABLE dbo.OpportunityItems ADD
	numRentalIN numeric(18,0) NULL,
	numRentalOut numeric(18,0) NULL,
	numRentalLost numeric(18,0) NULL

---------------------------------------------

------------------- SANDEEP ------------------


 BEGIN TRANSACTION

 DECLARE @numPageNavID numeric 
 SELECT @numPageNavID = MAX(numPageNavID) + 1  FROM dbo.PageNavigationDTL 
 INSERT INTO dbo.PageNavigationDTL
   ( numPageNavID ,
     numModuleID ,
     numParentID ,
     vcPageNavName ,
     vcNavURL ,
     vcImageURL ,
     bitVisible ,
     numTabID,
     intSortOrder
   )
 SELECT @numPageNavID ,13,73,'Shipping',NULL,NULL,1,-1,4


 DECLARE CursorTreeNavigation CURSOR FOR
 SELECT numDomainId FROM Domain WHERE numDomainId <> -255
 
 OPEN CursorTreeNavigation;

 DECLARE @numDomainId NUMERIC(18,0);

 FETCH NEXT FROM CursorTreeNavigation INTO @numDomainId;
 WHILE (@@FETCH_STATUS = 0)
 BEGIN
   
    ---- Give permission for each domain to tree node
    
	DELETE FROM TreeNavigationAuthorization WHERE numPageNavID=@numPageNavID AND numDomainID = @numDomainId
 
	SELECT * INTO #tempData1 FROM
  (
   SELECT    T.numTabID,
                        CASE WHEN bitFixed = 1
                             THEN CASE WHEN EXISTS ( SELECT numTabName
                                                     FROM   TabDefault
                                                     WHERE  numTabId = T.numTabId
                                                            AND tintTabType = T.tintTabType AND numDomainID =D.numDomainID)
                                       THEN ( SELECT TOP 1
                                                        numTabName
                                              FROM      TabDefault
                                              WHERE     numTabId = T.numTabId
                                                        AND tintTabType = T.tintTabType AND numDomainID = D.numDomainID
                                            )
                                       ELSE T.numTabName
                                  END
                             ELSE T.numTabName
                        END numTabname,
                        vcURL,
                        bitFixed,
                        1 AS tintType,
                        T.vcImage,
                        D.numDomainID,
                        A.numGroupID
              FROM      TabMaster T
                        CROSS JOIN Domain D --ON D.numDomainID = T.numDomainID
                        LEFT JOIN AuthenticationGroupMaster A ON A.numDomainID = D.numDomainId
              WHERE     bitFixed = 1
                        AND D.numDomainId <> -255
                        AND t.tintTabType = 1
              
              UNION ALL
              SELECT    -1 AS [numTabID],
                        'Administration' numTabname,
                        '' vcURL,
                        1 bitFixed,
                        1 AS tintType,
                        '' AS vcImage,
                        D.numDomainID,
                        A.numGroupID
              FROM      Domain D
                        JOIN AuthenticationGroupMaster A ON A.numDomainID = D.numDomainId
                        WHERE D.numDomainID = @numDomainID
     
     UNION ALL
     SELECT    -3 AS [numTabID],
      'Advanced Search' numTabname,
      '' vcURL,
      1 bitFixed,
      1 AS tintType,
      '' AS vcImage,
      D.numDomainID,
      A.numGroupID
     FROM      Domain D
      JOIN AuthenticationGroupMaster A ON A.numDomainID = D.numDomainId          
            ) TABLE2         
                        
	INSERT  INTO dbo.TreeNavigationAuthorization
            (
              numGroupID,
              numTabID,
              numPageNavID,
              bitVisible,
              numDomainID,
              tintType
            )
            SELECT  numGroupID,
                    PND.numTabID,
                    numPageNavID,
                    bitVisible,
                    numDomainID,
                    tintType
            FROM    dbo.PageNavigationDTL PND
                    JOIN #tempData1 TD ON TD.numTabID = PND.numTabID 
                    WHERE PND.numPageNavID= @numPageNavID
  
    DROP TABLE #tempData1

    FETCH NEXT FROM CursorTreeNavigation INTO @numDomainId;
END;

CLOSE CursorTreeNavigation;
DEALLOCATE CursorTreeNavigation;


UPDATE PageNavigationDTL SET bitVisible=0, vcPageNavName='Global Settings',intsortOrder=1 where numPageNavID=120 --Ecommerce Settings
UPDATE PageNavigationDTL SET bitVisible=0, intsortOrder=2 where numPageNavID=103 --Categories
UPDATE PageNavigationDTL SET bitVisible=0, vcPageNavName='Location Mapping',intsortOrder=3 where numPageNavID=119 --Warehouse Mapping
UPDATE PageNavigationDTL SET bitVisible=0, vcPageNavName='Payment Gateways',intsortOrder=5 where numPageNavID=143 --Payment Gateways
UPDATE PageNavigationDTL SET bitVisible=0, vcPageNavName='Marketplaces',intsortOrder=6 where numPageNavID=150 --Market Places(ol-e-Commerce API)
UPDATE PageNavigationDTL SET bitVisible=0, vcPageNavName='Promotions',intsortOrder=7 where numPageNavID=187 --Promotions & Offers
UPDATE PageNavigationDTL SET bitVisible=0, vcPageNavName='Error Codes',intsortOrder=8 where numPageNavID=197 --Error Message 
UPDATE PageNavigationDTL SET bitVisible=0, vcPageNavName='Reviews & Ratings',intsortOrder=9 where numPageNavID=217 --Reviews & Ratings
UPDATE PageNavigationDTL SET bitVisible=0, vcPageNavName='Design',intsortOrder=10 where numPageNavID=157 --Design Cart


UPDATE PageNavigationDTL SET numParentID=@numPageNavID where numPageNavID in (121,193) --under Shipping

UPDATE PageNavigationDTL SET vcPageNavName='Shipping Carrier Setup' where numPageNavID in (121)
UPDATE PageNavigationDTL SET vcPageNavName='Shipping Rules' where numPageNavID in (193)

----Ecommerce settings main
UPDATE PageNavigationDTL SET vcNavURL='../Items/frmECommerceSettings.aspx',vcPageNavName='e-Commerce' where numPageNavID=73


ROLLBACK TRANSACTION

--New Tables
/****** Object:  Table [dbo].[DemandForecast]    Script Date: 19-Sep-14 2:18:04 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[DemandForecast](
	[numDFID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numDomainID] [numeric](18, 0) NOT NULL,
	[numDFAPID] [numeric](18, 0) NOT NULL,
	[bitLastWeek] [bit] NOT NULL,
	[bitLastYear] [bit] NULL,
	[numDFDaysID] [numeric](18, 0) NOT NULL,
	[bitIncludeSalesOpp] [bit] NOT NULL,
	[dtCreatedDate] [datetime] NOT NULL,
	[dtModifiedDate] [datetime] NULL,
	[numCreatedBy] [numeric](18, 0) NULL,
	[numModifiedBy] [numeric](18, 0) NULL,
	[dtExecutionDate] [datetime] NULL,
 CONSTRAINT [PK_DemandForecast] PRIMARY KEY CLUSTERED 
(
	[numDFID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

/****** Object:  Table [dbo].[DemandForecastAnalysisPattern]    Script Date: 19-Sep-14 2:18:32 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DemandForecastAnalysisPattern](
	[numDFAPID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numDomainID] [numeric](18, 0) NULL,
	[vcAnalysisPattern] [varchar](500) NOT NULL,
	[numDays] [int] NOT NULL,
	[bitNextWeekLastYear] [bit] NOT NULL,
 CONSTRAINT [PK_DemandForecastAnalysisPattern] PRIMARY KEY CLUSTERED 
(
	[numDFAPID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING ON
GO

/****** Object:  Table [dbo].[DemandForecastDays]    Script Date: 19-Sep-14 2:19:05 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DemandForecastDays](
	[numDFDaysID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numDomainID] [numeric](18, 0) NULL,
	[vcDays] [varchar](500) NOT NULL,
	[numDays] [int] NOT NULL,
 CONSTRAINT [PK_DemandForecastDays] PRIMARY KEY CLUSTERED 
(
	[numDFDaysID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING ON
GO

/****** Object:  Table [dbo].[DemandForecastItemClassification]    Script Date: 19-Sep-14 2:19:35 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[DemandForecastItemClassification](
	[numDFICID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numDFID] [numeric](18, 0) NOT NULL,
	[numItemClassificationID] [numeric](18, 0) NOT NULL,
 CONSTRAINT [PK_DemandForecastItemClassification] PRIMARY KEY CLUSTERED 
(
	[numDFICID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[DemandForecastItemClassification]  WITH CHECK ADD  CONSTRAINT [FK_DemandForecastItemClassification_DemandForecast] FOREIGN KEY([numDFID])
REFERENCES [dbo].[DemandForecast] ([numDFID])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[DemandForecastItemClassification] CHECK CONSTRAINT [FK_DemandForecastItemClassification_DemandForecast]
GO

/****** Object:  Table [dbo].[DemandForecastItemGroup]    Script Date: 19-Sep-14 2:20:06 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[DemandForecastItemGroup](
	[numDFIGID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numDFID] [numeric](18, 0) NOT NULL,
	[numItemGroupID] [numeric](18, 0) NOT NULL,
 CONSTRAINT [PK_DemandForecastItemGroup] PRIMARY KEY CLUSTERED 
(
	[numDFIGID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[DemandForecastItemGroup]  WITH CHECK ADD  CONSTRAINT [FK_DemandForecastItemGroup_DemandForecast] FOREIGN KEY([numDFID])
REFERENCES [dbo].[DemandForecast] ([numDFID])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[DemandForecastItemGroup] CHECK CONSTRAINT [FK_DemandForecastItemGroup_DemandForecast]
GO


/****** Object:  Table [dbo].[DemandForecastWarehouse]    Script Date: 19-Sep-14 2:20:34 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[DemandForecastWarehouse](
	[numDFWID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numDFID] [numeric](18, 0) NOT NULL,
	[numWarehouseID] [numeric](18, 0) NOT NULL,
 CONSTRAINT [PK_DemandForecastWarehouse] PRIMARY KEY CLUSTERED 
(
	[numDFWID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[DemandForecastWarehouse]  WITH CHECK ADD  CONSTRAINT [FK_DemandForecastWarehouse_DemandForecast] FOREIGN KEY([numDFID])
REFERENCES [dbo].[DemandForecast] ([numDFID])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[DemandForecastWarehouse] CHECK CONSTRAINT [FK_DemandForecastWarehouse_DemandForecast]
GO

-- Change Planning and Procurement link in orders tab tree navigation
UPDATE 
	PageNavigationDTL
SET
	vcNavURL = '../DemandForecast/frmDemandForecastList.aspx'
WHERE
	numPageNavID = 128 AND
	numModuleID = 10 AND
	numParentID = 122


/* OLD URL  - DO NOT EXECUTE ON SERVER
UPDATE 
	PageNavigationDTL
SET
	vcNavURL = '../Opportunity/frmPlangProcurement.aspx?R=1'
WHERE
	numPageNavID = 128 AND
	numModuleID = 10 AND
	numParentID = 122
*/

-- Insert master detail of analysis pattern 
INSERT INTO DemandForecastAnalysisPattern 
	([numDomainID],[vcAnalysisPattern],[numDays],[bitNextWeekLastYear]) 
VALUES
(NULL,'Unit sales for the last week (7 days)',7,0),
(NULL,'Unit sales for the last 2 weeks (14 days)',14,0),
(NULL,'Unit sales for the last 1 month (30 days)',30,0),
(NULL,'Unit sales for the last 3 months (90 days)',90,0),
(NULL,'Unit sales from next week (7 days) based on last year',7,1),
(NULL,'Unit sales from next 2 weeks (14 days) based on last year',14,1),
(NULL,'Unit sales from next 1 month (30 days) based on last year',30,1),
(NULL,'Unit sales for next 3 months (90 days) based on last year',90,1)


-- Insert master detail of forcast days 
INSERT INTO DemandForecastDays
	([numDomainID],[vcDays],[numDays])
VALUES	
	(NULL,'Enough supply to last us 1 week (7 days)',7),
	(NULL,'Enough supply to last us 2 weeks (14 days)',14),
	(NULL,'Enough supply to last us 1 month (30 days)',30),
	(NULL,'Enough supply to last us 2 months (60 days)',60),
	(NULL,'Enough supply to last us 3 months (90 days)',90)

--Add column intLeadTimeDays to vendor table
ALTER TABLE dbo.Vendor ADD
intLeadTimeDays NUMERIC(18,0) NULL

BEGIN TRANSACTION

--Add Field to DycFieldMaster
DECLARE @NewFieldID AS INT

INSERT INTO DycFieldMaster 
(numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,bitInlineEdit,bitDeleted,bitAllowEdit,bitDefault,numListID,bitInResults) 
VALUES
(4,'Lead Time (Days)','intLeadTimeDays','intLeadTimeDays','Vendor','N','R','TextBox',1,0,1,0,0,0)
SELECT @NewFieldID = SCOPE_IDENTITY()

DECLARE @numQtySoldID AS INT
INSERT INTO DycFieldMaster 
(numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,bitInlineEdit,bitDeleted,bitAllowEdit,bitDefault,numListID,bitInResults) 
VALUES
(5,'Qty Sold','numQuantitySold','numQuantitySold','','V','R','TextArea',1,0,0,0,0,0)
SELECT @numQtySoldID = SCOPE_IDENTITY()

DECLARE @numOrderDate AS INT
INSERT INTO DycFieldMaster 
(numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,bitInlineEdit,bitDeleted,bitAllowEdit,bitDefault,numListID,bitInResults) 
VALUES
(5,'Order Date','vcOrderDate','vcOrderDate','','V','R','TextArea',1,0,1,0,0,0)
SELECT @numOrderDate = SCOPE_IDENTITY()

--Planning & Procurement Grid Columns Setting
DECLARE @numFormID AS INT = 125
INSERT INTO DynamicFormMaster ([numFormId],[vcFormName],[cCustomFieldsAssociated],[cAOIAssociated],[bitDeleted],[tintFlag]) VALUES (@numFormID,'Planning & Procurement Grid Columns Setting','Y','N',0,1)

IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 189 AND numFormID = @numFormID)
BEGIN
	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField],[bitDefault],[order]) VALUES (4,189,@numFormID,'Item Name',0,1,0,0,1,1,1)
END
IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 211 AND numFormID = @numFormID)
BEGIN
	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField],[bitDefault]) VALUES (4,211,@numFormID,'Item Code',0,0,0,0,1,0)
END
IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 193 AND numFormID = @numFormID)
BEGIN
	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField],[bitDefault]) VALUES (4,193,@numFormID,'Model ID',0,0,0,0,1,0)
END
IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 195 AND numFormID = @numFormID)
BEGIN
	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField],[bitDefault]) VALUES (4,195,@numFormID,'OnHand',0,0,0,0,1,0)
END
IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 196 AND numFormID = @numFormID)
BEGIN
	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField],[bitDefault]) VALUES (4,196,@numFormID,'OnOrder',0,0,0,0,1,0)
END
IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 197 AND numFormID = @numFormID)
BEGIN
	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField],[bitDefault]) VALUES (4,197,@numFormID,'OnAllocation',0,0,0,0,1,0)
END
IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 198 AND numFormID = @numFormID)
BEGIN
	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField],[bitDefault]) VALUES (4,198,@numFormID,'BackOrder',0,0,0,0,1,0)
END
IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 200 AND numFormID = @numFormID)
BEGIN
	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField],[bitDefault]) VALUES (4,200,@numFormID,'ReOrder',0,0,0,0,1,0)
END
IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 273 AND numFormID = @numFormID)
BEGIN
	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField],[bitDefault]) VALUES (4,273,@numFormID,'Item Classification',0,0,0,0,1,0)
END
IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 274 AND numFormID = @numFormID)
BEGIN
	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField],[bitDefault]) VALUES (4,274,@numFormID,'Item Group',0,0,0,0,1,0)
END
IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 281 AND numFormID = @numFormID)
BEGIN
	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField],[bitDefault]) VALUES (4,281,@numFormID,'SKU',0,0,0,0,1,0)
END
IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 3 AND numFieldID = 298 AND numFormID = @numFormID)
BEGIN
	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField],[bitDefault],[order]) VALUES (3,298,@numFormID,'Warehouse',0,1,0,0,1,1,2)
END
IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 290 AND numFormID = @numFormID)
BEGIN
	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField],[bitDefault]) VALUES (4,290,@numFormID,'Min Order Quantity',0,0,0,0,1,0)
END
IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = @NewFieldID AND numFormID = @numFormID)
BEGIN
	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField],[bitDefault],[order]) VALUES (4,@NewFieldID,@numFormID,'Lead Time (Days)',0,0,0,0,1,1,3)
END
IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 5 AND numFieldID = @numQtySoldID AND numFormID = @numFormID)
BEGIN
	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField],[bitDefault],[order]) VALUES (5,@numQtySoldID,@numFormID,'Qty Sold',0,0,0,0,1,1,4)
END
IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 5 AND numFieldID = @numOrderDate AND numFormID = @numFormID)
BEGIN
	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField],[bitDefault],[order]) VALUES (5,@numOrderDate,@numFormID,'Order Date',0,0,0,0,1,1,5)
END

ROLLBACK

--StoreProcedures
-----------------

--USP_GetColumnConfiguration1
--USP_GetVendors
--USP_TicklerActItems
--USP_UpdateVendors

--NewStoreProcedures
--------------------
--USP_DemandForecast_Delete
--USP_DemandForecast_DeleteOpp
--USP_DemandForecast_Execute
--USP_DemandForecast_Get
--USP_DemandForecast_GetByID
--USP_DemandForecast_GetByItemVendor
--USP_DemandForecast_Save
--USP_DemandForecast_UpdateInventory
--USP_DemandForecastAnalysisPattern_GetAll
--USP_DemandForecastDays_GetAll

--Functions
-----------

--GetDFHistoricalSalesOppData
--GetDFHistoricalSalesOrderData
--GetItemPurchaseVendorPrice
--GetItemQuantityToOreder




---------------------------------------------

------------------- MANISH ------------------

ALTER TABLE [dbo].[General_Journal_Header] ADD numAmountWith4Decimals MONEY
UPDATE [dbo].[General_Journal_Header] SET numAmountWith4Decimals = ISNULL([numAmount],0)
UPDATE [dbo].[General_Journal_Header] SET [numAmount] = ROUND(ISNULL([numAmount],0),2)

ALTER TABLE [dbo].[General_Journal_Details] ADD numDebitAmtWith4Decimals MONEY, numCreditAmtWith4Decimals MONEY
UPDATE [dbo].[General_Journal_Details] SET numDebitAmtWith4Decimals = ISNULL([numDebitAmt],0)
UPDATE [dbo].[General_Journal_Details] SET numCreditAmtWith4Decimals = ISNULL(numCreditAmt,0)

UPDATE [dbo].[General_Journal_Details] SET [numDebitAmt] = ROUND(ISNULL([numDebitAmt],0),2)
UPDATE [dbo].[General_Journal_Details] SET [numCreditAmt] = ROUND(ISNULL(numCreditAmt,0),2)

---------------------------------------------
