/******************************************************************
Project:    Date: 08-03-2013
Note : 
*******************************************************************/

-- Note : add item into ItemTax whose bitTaxable = 1 and not inserted into ItemTax table .
 
BEGIN TRANSACTION

INSERT INTO dbo.ItemTax (
	numItemCode,
	numTaxItemID,
	bitApplicable
) 
SELECT  numItemCode,0,1  FROM dbo.Item WHERE ISNULL(bitTaxable,0) = 1 and numItemCode NOT in
(
   SELECT numItemCode FROM dbo.ItemTax WHERE numTaxItemID = 0 
)
GO
ROLLBACK TRANSACTION

--Note this script is not for this release
BEGIN TRANSACTION

INSERT INTO dbo.EmailMergeFields (
	vcMergeField,
	vcMergeFieldValue,
	numModuleID,
	tintModuleType
) VALUES ( 
	/* vcMergeField - varchar(100) */ 'Item Documents',
	/* vcMergeFieldValue - varchar(100) */ 
	'{##ProductDocument##}
                                <div>
                                    <ul style="list-style:none;">
                                        <##FOR_Document##>
                                        <li style=" background:url(''##DocumentImage##'') no-repeat;line-height: 32px;vertical-align: middle;padding-left:36px;" >
                                            <a href ="##DocumentPath##"  target="_blank" >##DocumentName##</a>
                                        </li>
                                        <##ENDFOR##>
                                    </ul>
                                </div>
                   {/##ProductDocument##}'
	,
	/* numModuleID - numeric(18, 0) */ 3,
	/* tintModuleType - tinyint */ 1 ) 
GO
ROLLBACK TRANSACTION


BEGIN TRANSACTION

ALTER TABLE dbo.ItemImages 
ADD bitIsImage BIT NULL DEFAULT 0

GO
ROLLBACK TRANSACTION


--Note before uploading any document set all images' IsImage field = 1
BEGIN TRANSACTION

UPDATE dbo.ItemImages SET bitIsImage = 1 

GO
ROLLBACK TRANSACTION




/******************************************************************
Project:    Date: 22-2-2013
Note :  for release 16-03-2013
*******************************************************************/
BEGIN TRANSACTION
ALTER TABLE dbo.ItemImages ALTER COLUMN vcPathForTImage VARCHAR(300) null
GO
ROLLBACK TRANSACTION


BEGIN TRANSACTION
DELETE FROM dbo.PageMaster WHERE numModuleID = 13 AND numPageID = 52
GO
ROLLBACK TRANSACTION
 
BEGIN TRANSACTION
DELETE FROM dbo.GroupAuthorization  WHERE numModuleID = 13 AND numPageID = 52
GO
ROLLBACK TRANSACTION
 


--07-03-2013 add page for Authentication frmSiteCategory
BEGIN TRANSACTION
--Set Identity_Insert dbo.PageMaster  Off
INSERT INTO dbo.PageMaster (
	numPageID,
	numModuleID,
	vcFileName,
	vcPageDesc,
	bitIsViewApplicable,
	bitIsAddApplicable,
	bitIsUpdateApplicable,
	bitIsDeleteApplicable,
	bitIsExportApplicable
) VALUES ( 
	/* numPageID - numeric(18, 0) */ 53,
	/* numModuleID - numeric(18, 0) */ 13,
	/* vcFileName - varchar(50) */ 'frmSiteCategory.aspx',
	/* vcPageDesc - varchar(100) */ 'Site Category ',
	/* bitIsViewApplicable - int */ 1,
	/* bitIsAddApplicable - int */ 1,
	/* bitIsUpdateApplicable - int */ 0,
	/* bitIsDeleteApplicable - int */ 0,
	/* bitIsExportApplicable - int */ 0
	 ) 
--Set Identity_Insert dbo.PageMaster  On
GO
ROLLBACK TRANSACTION


BEGIN TRANSACTION

alter table dbo.MetaTags alter COLUMN vcMetaTag  VARCHAR(4000)  NULL

GO
ROLLBACK TRANSACTION


--13-3-2013 
BEGIN TRANSACTION

INSERT INTO dbo.ErrorMaster (
	vcErrorCode,
	vcErrorDesc,
	tintApplication
) VALUES ( 
	/* vcErrorCode - varchar(6) */ 'ERR072',
	/* vcErrorDesc - nvarchar(max) */ N'Service item for Coupen is not selected,Please contact your merchant.',
	/* tintApplication - tinyint */ 3 )  

GO
ROLLBACK TRANSACTION

--BEGIN TRANSACTION
--DECLARE @PageID AS NUMERIC(9,0)
--SELECT @PageID = (MAX(numPageID)+1) FROM dbo.PageMaster WHERE  numModuleID = 13
--
--INSERT INTO dbo.PageMaster (
--	numPageID,
--	numModuleID,
--	vcFileName,
--	vcPageDesc,
--	bitIsViewApplicable,
--	bitIsAddApplicable,
--	bitIsUpdateApplicable,
--	bitIsDeleteApplicable,
--	bitIsExportApplicable
--) VALUES ( 
--	/* numPageID - numeric(18, 0) */ @PageID,
--	/* numModuleID - numeric(18, 0) */ 13,
--	/* vcFileName - varchar(50) */ 'frmCategoryAdd.aspx',
--	/* vcPageDesc - varchar(100) */ 'Category Add',
--	/* bitIsViewApplicable - int */ 1,
--	/* bitIsAddApplicable - int */ 1,
--	/* bitIsUpdateApplicable - int */ 1,
--	/* bitIsDeleteApplicable - int */ 1,
--	/* bitIsExportApplicable - int */ 0
--	 ) 
--GO
--ROLLBACK TRANSACTION

--07-03-2013
BEGIN TRANSACTION

UPDATE dbo.PageNavigationDTL SET bitVisible = 0 WHERE numPageNavID = 165

GO
ROLLBACK TRANSACTION

--5-3-2013
--Note there should be a script for moving data of vcMetaTags to vcKeyword and vcDescription
BEGIN TRANSACTION

ALTER TABLE MetaTags 
ADD vcMetaKeywords VARCHAR(4000) NULL

ALTER TABLE MetaTags 
ADD vcMetaDescription VARCHAR(4000) NULL

GO
ROLLBACK TRANSACTION

--4-3-2013 
--note check for max of numPageNavID in local it is 168 and parentID is 157 
BEGIN TRANSACTION

DECLARE @numPageNavID NUMERIC(9,0)
SET @numPageNavID = 0
SELECT @numPageNavID = (MAX(numPageNavID)+1) FROM dbo.PageNavigationDTL WHERE numParentID = 157 
--PRINT(@numPageNavID)
INSERT INTO dbo.PageNavigationDTL (
	numPageNavID,
	numModuleID,
	numParentID,
	vcPageNavName,
	vcNavURL,
	vcImageURL,
	bitVisible,
	numTabID
) VALUES ( 
	/* numPageNavID - numeric(18, 0) */ @numPageNavID,
	/* numModuleID - numeric(18, 0) */ 13,
	/* numParentID - numeric(18, 0) */ 157,
	/* vcPageNavName - varchar(300) */ 'Site Category',
	/* vcNavURL - varchar(1000) */ '../ECommerce/frmSiteCategory.aspx',
	/* vcImageURL - varchar(100) */ '',
	/* bitVisible - bit */ 1,
	/* numTabID - numeric(18, 0) */ -1 ) 
		
GO
ROLLBACK TRANSACTION
/******************************************************************
Project:    Date: 12-2-2013
Note : Issue found when testing on demo server.
*******************************************************************/

--Note its added in demo server 
BEGIN TRANSACTION

INSERT INTO dbo.ErrorMaster (
	vcErrorCode,
	vcErrorDesc,
	tintApplication
) VALUES ( 
	/* vcErrorCode - varchar(6) */ 'ERR071',
	/* vcErrorDesc - nvarchar(max) */ N'Please enter a valid postal code in the address.',
	/* tintApplication - tinyint */ 3 ) 
GO
ROLLBACK TRANSACTION

--19-02-2013
BEGIN TRANSACTION

ALTER TABLE ExtranetAccountsDtl 
ADD vcGoogleID VARCHAR(100) NULL

GO
ROLLBACK TRANSACTION

--21-02-2013
BEGIN TRANSACTION

UPDATE dbo.PageNavigationDTL SET numParentID = 73 WHERE numPageNavID = 187 AND vcPageNavName LIKE 'Promotions & Offers'
UPDATE dbo.PageNavigationDTL SET numParentID = 73 WHERE numPageNavID = 193 AND vcPageNavName LIKE 'Shipping Rule List'
GO
ROLLBACK TRANSACTION


/******************************************************************
Project:    Date: 23-1-2013
Comments: Note : After adding Error message it requires to click get Error Message button on Maintanance Page .
*******************************************************************/
--AppleNv web.config replace SMTPLicense with this for sending email of Mirror Bizdoc
<add key="SMTPLicense" value="MN700-D686805186065845A8EFDF190775-AA3C" />

BEGIN TRANSACTION
        ALTER TABLE dbo.SitePages
		ADD bitIsMaintainScroll bit NOT NULL
		CONSTRAINT MaintainScroll_Default_Constraint  DEFAULT 1
GO
ROLLBACK TRANSACTION


BEGIN TRANSACTION
UPDATE dbo.SitePages SET bitIsMaintainScroll = 1 WHERE numSiteID = 60 AND numDomainID = 156
GO
ROLLBACK TRANSACTION

BEGIN TRANSACTION
UPDATE dbo.SitePages SET bitIsMaintainScroll = 0 WHERE numSiteID <> 60 AND numDomainID <> 156
GO
ROLLBACK TRANSACTION

BEGIN TRANSACTION
ALTER TABLE eCommercePaymentConfig
ADD  numFailedOrderStatus NUMERIC(18,0) null
GO
ROLLBACK TRANSACTION

BEGIN TRANSACTION
ALTER TABLE eCommercePaymentConfig
ADD  numBizDocStatus NUMERIC(18,0) null
GO
ROLLBACK TRANSACTION


--Note Fire only this script for 25-1-2013 Demo
BEGIN TRANSACTION
DELETE FROM dbo.ShippingServiceTypes WHERE numDomainID = 0 AND numRuleID IS NOT NULL
GO
ROLLBACK TRANSACTION
  
/******************************************************************
Project:    Date: 18-1-2013
Comments: Note : After adding Error message it requires to click get Error Message button on Maintanance Page .
*******************************************************************/
--Note I think you have inserted Error message upto ERR068 ,so you need to insert ERR069 ,ERR070 and update script 
BEGIN TRANSACTION
UPDATE dbo.ErrorMaster SET vcErrorDesc = 'Please make sure your review contains at least 100 characters.' WHERE vcErrorCode = 'ERR064'
GO
ROLLBACK TRANSACTION

BEGIN TRANSACTION
INSERT INTO dbo.ErrorMaster (
	vcErrorCode,
	vcErrorDesc,
	tintApplication
) VALUES ( 
	/* vcErrorCode - varchar(6) */ 'ERR070',
	/* vcErrorDesc - nvarchar(max) */ N' There is a problem to redirect to Google Checkout.',
	/* tintApplication - tinyint */ 3 ) 
GO
ROLLBACK TRANSACTION




BEGIN TRANSACTION
INSERT INTO dbo.ErrorMaster (
	vcErrorCode,
	vcErrorDesc,
	tintApplication
) VALUES ( 
	/* vcErrorCode - varchar(6) */ 'ERR069',
	/* vcErrorDesc - nvarchar(max) */ N' your merchant need to provide valid ship from warehouse address to calculate shipping amount,please contact site administrator.',
	/* tintApplication - tinyint */ 3 ) 
GO
ROLLBACK TRANSACTION



BEGIN TRANSACTION
INSERT INTO dbo.ErrorMaster (
	vcErrorCode,
	vcErrorDesc,
	tintApplication
) VALUES ( 
	/* vcErrorCode - varchar(6) */ 'ERR068',
	/* vcErrorDesc - nvarchar(max) */ N' Shipping information you provided does not seem valid.Please re enter details or contact site administrator.',
	/* tintApplication - tinyint */ 3 ) 
GO
ROLLBACK TRANSACTION
/******************************************************************
Project:    Date:
Comments: 
*******************************************************************/
BEGIN TRANSACTION
INSERT INTO dbo.ErrorMaster (		
	vcErrorCode,	
	vcErrorDesc,	
	tintApplication	
) VALUES (  		
	/* vcErrorCode - varchar(6) */ 'ERR067',	
	/* vcErrorDesc - nvarchar(max) */ N'Service item for shipping charge is not selected,Please contact your merchant.',	
	/* tintApplication - tinyint */ 3 ) 	

GO
ROLLBACK TRANSACTION





