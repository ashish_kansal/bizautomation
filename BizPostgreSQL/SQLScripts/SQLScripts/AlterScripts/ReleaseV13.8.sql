/******************************************************************
Project: Release 13.8 Date: 05.AUG.2020
Comments: ALTER SCRIPTS
*******************************************************************/

/******************************************** PRASHANT *********************************************/

INSERT INTO CFw_Grp_Master( Grp_Name, Loc_Id, numDomainID, tintType, vcURLF) SELECT 'Collaboration',3,numDomainId,2,'' FROM Domain
INSERT INTO CFw_Grp_Master( Grp_Name, Loc_Id, numDomainID, tintType, vcURLF) SELECT 'Collaboration',2,numDomainId,2,'' FROM Domain
CREATE TABLE [dbo].[TopicMessageAttachments](
	[numAttachmentId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numTopicId] [numeric](18, 0) NULL,
	[numMessageId] [numeric](18, 0) NULL,
	[vcAttachmentName] [varchar](500) NULL,
	[vcAttachmentUrl] [varchar](500) NULL,
	[numCreatedBy] [numeric](18, 0) NULL,
	[dtmCreatedOn] [datetime] NULL,
	[numUpdatedBy] [numeric](18, 0) NULL,
	[dtmUpdatedOn] [datetime] NULL,
	[numDomainId] [numeric](18, 0) NULL,
 CONSTRAINT [PK_TopicMessageAttachments] PRIMARY KEY CLUSTERED 
(
	[numAttachmentId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[MessageMaster](
	[numMessageId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numTopicId] [numeric](18, 0) NULL,
	[numParentMessageId] [numeric](18, 0) NULL,
	[intRecordType] [int] NULL,
	[numRecordId] [numeric](18, 0) NULL,
	[vcMessage] [varchar](max) NULL,
	[numCreatedBy] [numeric](18, 0) NULL,
	[dtmCreatedOn] [datetime] NULL,
	[numUpdatedBy] [numeric](18, 0) NULL,
	[dtmUpdatedOn] [datetime] NULL,
	[bitIsInternal] [bit] NULL,
	[numDomainId] [numeric](18, 0) NULL,
 CONSTRAINT [PK_MessageMaster] PRIMARY KEY CLUSTERED 
(
	[numMessageId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

CREATE TABLE [dbo].[TopicMaster](
	[numTopicId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[intRecordType] [int] NULL,
	[numRecordId] [numeric](18, 0) NULL,
	[vcTopicTitle] [varchar](max) NULL,
	[numCreateBy] [numeric](18, 0) NULL,
	[dtmCreatedOn] [datetime] NULL,
	[numUpdatedBy] [numeric](18, 0) NULL,
	[dtmUpdatedOn] [datetime] NULL,
	[bitIsInternal] [bit] NULL,
	[numDomainId] [numeric](18, 0) NULL,
 CONSTRAINT [PK_TopicMaster] PRIMARY KEY CLUSTERED 
(
	[numTopicId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE ContractsLog ADD vcDetails VARCHAR(500) DEFAULT NULL
ALTER TABLE ContractsLog ADD numUsedTime NUMERIC(18,2) DEFAULT 0
ALTER TABLE ContractsLog ADD numBalance NUMERIC(18,2) DEFAULT 0
ALTER TABLE ContractsLog ADD numContractId NUMERIC(18,2) DEFAULT 0


/******************************************** SANDEEP *********************************************/

ALTER TABLE PurchaseIncentives ADD tintDiscountType TINYINT
ALTER TABLE PurchaseIncentives ALTER COLUMN vcIncentives FLOAT
ALTER TABLE PurchaseIncentives ALTER COLUMN vcbuyingqty FLOAT
ALTER TABLE VendorShipmentMethod ADD numWarehouseID NUMERIC(18,0)
UPDATE PurchaseIncentives SET tintDiscountType = 0
ALTER TABLE Domain ADD bitUseDeluxeCheckStock BIT DEFAULT 1
ALTER TABLE SalesOrderConfiguration ADD vcPOHiddenColumns VARCHAR(200)

INSERT INTO PageMaster
(
	numPageID,numModuleID,vcFileName,vcPageDesc,bitIsViewApplicable,bitIsAddApplicable,bitIsUpdateApplicable,bitIsDeleteApplicable,bitIsExportApplicable,bitViewPermission1Applicable,bitViewPermission2Applicable,bitViewPermission3Applicable
)
VALUES
(
	1,46,'frmPOCostOptimizationAndMerge.aspx','Merging & Cost Optimization',1,0,0,0,0,0,0,1
)

BEGIN TRY
BEGIN TRANSACTION

	DECLARE @numMAXPageNavID INT
	SELECT @numMAXPageNavID = MAX(numPageNavID) FROM PageNavigationDTL


	INSERT INTO PageNavigationDTL
	(
		numPageNavID,numModuleID,numParentID,vcPageNavName,vcNavURL,bitVisible,numTabID
	)
	VALUES
	(
		(@numMAXPageNavID + 1),(SELECT TOP 1 numModuleID FROM ModuleMaster WHERE vcModuleName='Procurement'),(SELECT TOP 1 numPageNavID FROM PageNavigationDTL WHERE vcPageNavName='Procurement'),'Merging & Cost Optimization','../Opportunity/frmPOCostOptimizationAndMerge.aspx',1,(SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Procurement' AND numDomainID=1)
	)

	INSERT INTO dbo.TreeNavigationAuthorization
	(
		numGroupID,
		numTabID,
		numPageNavID,
		bitVisible,
		numDomainID,
		tintType
	)
	SELECT  
		numGroupID,
		(SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Procurement' AND numDomainID=1),
		(@numMAXPageNavID + 1),
		1,
		numDomainID,
		1
	FROM    
		AuthenticationGroupMaster
	WHERE
		vcGroupName='System Administrator'

	INSERT INTO dbo.GroupAuthorization
	(numDomainID,numModuleID,numPageID,numGroupID,intExportAllowed,intPrintAllowed,intViewAllowed,intAddAllowed,intUpdateAllowed,intDeleteAllowed)
	SELECT
		numDomainID,46,1,numGroupID
		,0
		,0
		,3
		,0
		,0
		,0
	FROM
		AuthenticationGroupMaster
	WHERE
		vcGroupName='System Administrator'
COMMIT
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		ERROR_MESSAGE(),
		ERROR_NUMBER(),
		ERROR_SEVERITY(),
		ERROR_STATE(),
		ERROR_LINE(),
		ISNULL(ERROR_PROCEDURE(), '-');
END CATCH


--------------------------------------------------------

-- FOLLOWING ALREAY EXECUTED ON LIVE
ALTER TABLE MassSalesFulfillmentConfiguration ADD bitEnablePickListMapping BIT DEFAULT 0
ALTER TABLE MassSalesFulfillmentConfiguration ADD bitEnableFulfillmentBizDocMapping BIT DEFAULT 0

DECLARE @numFieldID NUMERIC(18,0)

INSERT INTO DycFieldMaster
(
	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcLookBackTableName,vcPropertyName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcListItemType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitDetailField,bitAllowSorting,bitAllowFiltering,bitWorkFlowField
)
VALUES
(
	3,'Remaining qty for pick','numRemainingQtyToPick','numRemainingQtyToPick','OpportunityItems','RemainingQtyToPick','N','R','Label','',0,1,0,0,0,1,0,1,1,0
)

SET @numFieldID = SCOPE_IDENTITY()

INSERT INTO ReportFieldGroupMappingMaster
(
	numReportFieldGroupID,numFieldID,vcFieldName,vcAssociatedControlType,vcFieldDataType,bitAllowSorting,bitAllowGrouping,bitAllowAggregate,bitAllowFiltering
)
VALUES
(
	4,@numFieldID,'Remaining qty for pick','TextBox','N',1,0,0,1
)

----------------------------------------------

DECLARE @numFieldID NUMERIC(18,0)

INSERT INTO DycFieldMaster
(
	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcLookBackTableName,vcPropertyName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcListItemType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitDetailField,bitAllowSorting,bitAllowFiltering,bitWorkFlowField
)
VALUES
(
	3,'Remaining qty for ship','numRemainingQtyToShip','numRemainingQtyToShip','OpportunityItems','RemainingQtyToShip','N','R','Label','',0,1,0,0,0,1,0,1,1,0
)

SET @numFieldID = SCOPE_IDENTITY()

INSERT INTO ReportFieldGroupMappingMaster
(
	numReportFieldGroupID,numFieldID,vcFieldName,vcAssociatedControlType,vcFieldDataType,bitAllowSorting,bitAllowGrouping,bitAllowAggregate,bitAllowFiltering
)
VALUES
(
	4,@numFieldID,'Remaining qty for ship','TextBox','N',1,0,0,1
)

----------------------------------------------

DECLARE @numFieldID NUMERIC(18,0)

INSERT INTO DycFieldMaster
(
	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcLookBackTableName,vcPropertyName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcListItemType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitDetailField,bitAllowSorting,bitAllowFiltering,bitWorkFlowField
)
VALUES
(
	3,'Remaining qty for invoicing','numRemainingQtyToInvoice','numRemainingQtyToInvoice','OpportunityItems','RemainingQtyToInvoice','N','R','Label','',0,1,0,0,0,1,0,1,1,0
)

SET @numFieldID = SCOPE_IDENTITY()

INSERT INTO ReportFieldGroupMappingMaster
(
	numReportFieldGroupID,numFieldID,vcFieldName,vcAssociatedControlType,vcFieldDataType,bitAllowSorting,bitAllowGrouping,bitAllowAggregate,bitAllowFiltering
)
VALUES
(
	4,@numFieldID,'Remaining qty for invoicing','TextBox','N',1,0,0,1
)

-----------------------------------------------------

ALTER TABLE MassSalesFulfillmentConfiguration ADD bitPickWithoutInventoryCheck BIT DEFAULT 0
