/******************************************************************
Project: Release 6.8 Date: 26.JAN.2017
Comments: ALTER SCRIPTS
*******************************************************************/

/************************** SANDEEP ************************/

UPDATE DycFormField_Mapping SET bitAllowFiltering=0,bitAllowSorting=0 WHERE numFormID=43 AND numFieldID IN (15,120,121)
UPDATE DycFormField_Mapping SET bitAllowSorting=0 WHERE numFormID=43 AND numFieldID=60


INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,bitSettingField,bitAddField,bitRequired
)
VALUES
(
	1,3,96,0,0,'Organization Name',1,0,0
)


BEGIN TRY
BEGIN TRANSACTION

	
 -- 1. Leads - ModileID = 2

	INSERT INTO PageMaster 
		(numPageID, numModuleID, vcFileName, vcPageDesc, bitIsViewApplicable, bitIsAddApplicable, bitIsUpdateApplicable, bitIsDeleteApplicable, bitIsExportApplicable)
	VALUES
		(132,37,'frmInventoryAdjustmentInBatch.aspx','Manage Inventory',1,0,0,0,0)


	SELECT 
		* 
	INTO 
		#temp 
	FROM
	(
		SELECT 
			ROW_NUMBER() OVER(ORDER BY numDomainId DESC) AS Row, 
			numDomainId 
		FROM 
			Domain 
		WHERE 
			numDomainId <> -255
	) TABLE2
	
	DECLARE @RowCount INT
	SET @RowCount = (SELECT COUNT(Row) FROM #temp) 
	
	DECLARE @I INT
	DECLARE @numDomainId NUMERIC(18,0)
	
	SET @I = 1

	WHILE (@I <= @RowCount)
	BEGIN
			
		SELECT @numDomainId = numDomainID FROM #temp WHERE Row = @I
	
		INSERT INTO dbo.GroupAuthorization
			(numDomainID,numModuleID,numPageID,numGroupID,intExportAllowed,intPrintAllowed,intViewAllowed,intAddAllowed,intUpdateAllowed,intDeleteAllowed)
		SELECT
			@numDomainId,37,132,numGroupID,0,0,3,0,0,0
		FROM
			AuthenticationGroupMaster
		WHERE
			numDomainID = @numDomainId AND
			tintGroupType = 1


		SET @I = @I  + 1
	END
	
	DROP TABLE #temp
COMMIT
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		ERROR_MESSAGE(),
		ERROR_NUMBER(),
		ERROR_SEVERITY(),
		ERROR_STATE(),
		ERROR_LINE(),
		ISNULL(ERROR_PROCEDURE(), '-');
END CATCH


BEGIN TRY
BEGIN TRANSACTION

	
 -- 1. Leads - ModileID = 2

	INSERT INTO PageMaster 
		(numPageID, numModuleID, vcFileName, vcPageDesc, bitIsViewApplicable, bitIsAddApplicable, bitIsUpdateApplicable, bitIsDeleteApplicable, bitIsExportApplicable)
	VALUES
		(133,8,'frmItemInventoryAndOppOrderReport.aspx','Items Inventory and Total Sales/Purchase Orders Quantity',1,0,0,0,0)


	SELECT 
		* 
	INTO 
		#temp 
	FROM
	(
		SELECT 
			ROW_NUMBER() OVER(ORDER BY numDomainId DESC) AS Row, 
			numDomainId 
		FROM 
			Domain 
		WHERE 
			numDomainId <> -255
	) TABLE2
	
	DECLARE @RowCount INT
	SET @RowCount = (SELECT COUNT(Row) FROM #temp) 
	
	DECLARE @I INT
	DECLARE @numDomainId NUMERIC(18,0)
	
	SET @I = 1

	WHILE (@I <= @RowCount)
	BEGIN
			
		SELECT @numDomainId = numDomainID FROM #temp WHERE Row = @I
	
		INSERT INTO dbo.GroupAuthorization
			(numDomainID,numModuleID,numPageID,numGroupID,intExportAllowed,intPrintAllowed,intViewAllowed,intAddAllowed,intUpdateAllowed,intDeleteAllowed)
		SELECT
			@numDomainId,8,133,numGroupID,0,0,3,0,0,0
		FROM
			AuthenticationGroupMaster
		WHERE
			numDomainID = @numDomainId AND
			tintGroupType = 1


		SET @I = @I  + 1
	END
	
	DROP TABLE #temp

	INSERT INTO ReportList
	( 
		RptId,NumId,RptHeading,RptDesc,rptGroup,rptSequence,bitEnable
	)
	VALUES
	(
		(SELECT MAX(RptId) FROM ReportList) + 1,133,'Items Inventory and Total Opp/Orders Quantity','Shows item total inventoty and also quantity sold and purchased.',32,7,1
	)
COMMIT
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		ERROR_MESSAGE(),
		ERROR_NUMBER(),
		ERROR_SEVERITY(),
		ERROR_STATE(),
		ERROR_LINE(),
		ISNULL(ERROR_PROCEDURE(), '-');
END CATCH


---------------------------------------------------------------


BEGIN TRY
BEGIN TRANSACTION

	
 -- 1. Leads - ModileID = 2

	INSERT INTO PageMaster 
		(numPageID, numModuleID, vcFileName, vcPageDesc, bitIsViewApplicable, bitIsAddApplicable, bitIsUpdateApplicable, bitIsDeleteApplicable, bitIsExportApplicable)
	VALUES
		(134,11,'','Layout Button',1,0,0,0,0)


	SELECT 
		* 
	INTO 
		#temp 
	FROM
	(
		SELECT 
			ROW_NUMBER() OVER(ORDER BY numDomainId DESC) AS Row, 
			numDomainId 
		FROM 
			Domain 
		WHERE 
			numDomainId <> -255
	) TABLE2
	
	DECLARE @RowCount INT
	SET @RowCount = (SELECT COUNT(Row) FROM #temp) 
	
	DECLARE @I INT
	DECLARE @numDomainId NUMERIC(18,0)
	
	SET @I = 1

	WHILE (@I <= @RowCount)
	BEGIN
			
		SELECT @numDomainId = numDomainID FROM #temp WHERE Row = @I
	
		INSERT INTO dbo.GroupAuthorization
			(numDomainID,numModuleID,numPageID,numGroupID,intExportAllowed,intPrintAllowed,intViewAllowed,intAddAllowed,intUpdateAllowed,intDeleteAllowed)
		SELECT
			@numDomainId,11,134,numGroupID,0,0,3,0,0,0
		FROM
			AuthenticationGroupMaster
		WHERE
			numDomainID = @numDomainId AND
			tintGroupType = 1


		SET @I = @I  + 1
	END
	
	DROP TABLE #temp
COMMIT
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		ERROR_MESSAGE(),
		ERROR_NUMBER(),
		ERROR_SEVERITY(),
		ERROR_STATE(),
		ERROR_LINE(),
		ISNULL(ERROR_PROCEDURE(), '-');
END CATCH