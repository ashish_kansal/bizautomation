/******************************************************************
Project: Release 2.9 Date: 19.02.2014
Comments: Changes for adding ItemID searchable in bizcart
*******************************************************************/

BEGIN TRANSACTION




--25.02.2014 updated on demo and production server do not run again
ALTER TABLE dbo.UserMaster
ALTER column txtSignature varchar(8000)
INSERT INTO dbo.EmailMergeFields (
	vcMergeField,
	vcMergeFieldValue,
	numModuleID,
	tintModuleType
) VALUES ( 
	/* vcMergeField - varchar(100) */ 'Item Attributes',
	/* vcMergeFieldValue - varchar(2000) */ '##ItemAttributes##',
	/* numModuleID - numeric(18, 0) */ 7,
	/* tintModuleType - tinyint */ 1 ) 



usp_UpdateSignature
USP_ItemDetailsForEcomm
USP_EcommerceSettings
USP_GetECommerceDetails

/*Below script is already executed on production DB as on 19.2.2014*/
INSERT INTO dbo.DycFormField_Mapping (
	numModuleID,
	numFieldID,
	numDomainID,
	numFormID,
	bitAllowEdit,
	bitInlineEdit,
	vcFieldName,
	vcAssociatedControlType,
	vcPropertyName,
	PopupFunctionName,
	[order],
	tintRow,
	tintColumn,
	bitInResults,
	bitDeleted,
	bitDefault,
	bitSettingField,
	bitAddField,
	bitDetailField,
	bitAllowSorting,
	bitWorkFlowField,
	bitImport,
	bitExport,
	bitAllowFiltering,
	bitRequired,
	numFormFieldID,
	intSectionID,
	bitAllowGridColor
) 
SELECT numModuleID,numFieldId,numDomainID,30,0 AS bitAllowEdit,0 AS bitInlineEdit,
vcFieldName,vcAssociatedControlType,vcPropertyName,PopupFunctionName,6 AS [Order],NULL,NULL,
1 AS bitInResults,0,0,1,NULL,1,NULL,NULL,NULL,
NULL,1,NULL,null,NULL,NULL  FROM dbo.DycFieldMaster WHERE numModuleID=4 AND vcFieldName like 'Item ID'
------------------------------


GO
ALTER TABLE dbo.eCommerceDTL ADD
	bitAutoSelectWarehouse bit NULL
GO
ALTER TABLE dbo.eCommerceDTL ADD CONSTRAINT
	DF_eCommerceDTL_bitAutoSelectWarehouse DEFAULT 0 FOR bitAutoSelectWarehouse
GO

ROLLBACK 
/******************************************************************
Project: Release 1.5 Date: 16.03.2013
Comments: includes location management, Sales Opp Percentage completed and other bug fixes
*******************************************************************/

BEGIN TRANSACTION

GO
/****** Object:  Table [dbo].[WarehouseLocation]    Script Date: 04/16/2013 13:02:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[WarehouseLocation](
	[numWLocationID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numWarehouseID] [numeric](18, 0) NOT NULL,
	[vcAisle] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[vcRack] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[vcShelf] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[vcBin] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[vcLocation] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[bitDefault] [bit] NOT NULL CONSTRAINT [DF_WarehouseLocation_bitDefault]  DEFAULT ((0)),
	[bitSystem] [bit] NOT NULL CONSTRAINT [DF_WarehouseLocation_bitSystem]  DEFAULT ((0)),
	[intQty] [int] NULL,
	[numDomainID] [numeric](18, 0) NOT NULL,
 CONSTRAINT [PK_WarehouseLocation] PRIMARY KEY CLUSTERED 
(
	[numWLocationID] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING ON

--Migrate existing warehouse location data
INSERT INTO dbo.WarehouseLocation
        ( numWarehouseID ,
          vcAisle ,
          vcRack ,
          vcShelf ,
          vcBin ,
          vcLocation ,
          numDomainID
        )
SELECT  numWareHouseID ,
      LTRIM(RTRIM(ISNULL( NULLIF( ISNULL( vcLocation,''),'0') ,''))) ,
        '',
        '',
        '',
        '',
        numDomainID FROM dbo.WareHouseItems WHERE LEN(RTRIM(LTRIM(ISNULL( NULLIF( ISNULL( vcLocation,''),'0') ,''))) )>0
        
-- Update vcLocation 
UPDATE dbo.WarehouseLocation SET vcLocation = 
RTRIM(LTRIM(ISNULL(vcAisle,''))) + '.'
+ RTRIM(LTRIM(ISNULL(vcRack,''))) + '.'
+ RTRIM(LTRIM(ISNULL(vcShelf,''))) + '.'
+ RTRIM(LTRIM(ISNULL(vcBin,''))) 

--Remove trailing dot ...
UPDATE  dbo.WarehouseLocation SET vcLocation = REVERSE(SUBSTRING(REVERSE(vcLocation), PATINDEX('%[^.]%',REVERSE(vcLocation)), LEN(vcLocation) - PATINDEX('%[^.]%', REVERSE(vcLocation)) + 1))

UPDATE dbo.WarehouseLocation SET bitDefault = 0 
UPDATE dbo.WarehouseLocation SET bitSystem = 0 

--SELECT * FROM dbo.PageMaster 
--
--SELECT * FROM dbo.PageNavigationDTL WHERE vcPageNavName LIKE '%Group%'
--SELECT * FROM dbo.ModuleMaster
--SELECT * FROM dbo.WarehouseLocation
--SELECT * FROM dbo.WarehouseLocationTransactions
--SELECT * FROM dbo.WareHouseItems
--SELECT * FROM dbo.WareHouseItmsDTL

BEGIN TRANSACTION
	DECLARE @numPageNavID numeric 
	SELECT @numPageNavID = MAX(numPageNavID) + 1  FROM dbo.PageNavigationDTL 
	INSERT INTO dbo.PageNavigationDTL
			( numPageNavID ,
			  numModuleID ,
			  numParentID ,
			  vcPageNavName ,
			  vcNavURL ,
			  vcImageURL ,
			  bitVisible ,
			  numTabID
			)
	SELECT TOP 1 @numPageNavID ,37,173,'Warehouse Location','../Items/frmWarehouseLocation.aspx',NULL,1,80 FROM dbo.PageNavigationDTL 
	EXEC USP_CreateTreeNavigationForDomain 0,@numPageNavID --Give permission to tree node.
END TRANSACTION


Begin TRANSACTION

DECLARE @numMaxPageID NUMERIC
SELECT @numMaxPageID =  MAX(numPageID) + 1  FROM dbo.PageMaster
INSERT INTO dbo.PageMaster
        ( numPageID ,
          numModuleID ,
          vcFileName ,
          vcPageDesc ,
          bitIsViewApplicable ,
          bitIsAddApplicable ,
          bitIsUpdateApplicable ,
          bitIsDeleteApplicable ,
          bitIsExportApplicable ,
          vcToolTip ,
          bitDeleted
        )
SELECT @numMaxPageID,37,'frmWarehouseLocation.aspx','Warehouse Locations',1,1,0,1,1,'',0 
INSERT INTO dbo.GroupAuthorization
        ( numGroupID ,
          numModuleID ,
          numPageID ,
          intExportAllowed ,
          intPrintAllowed ,
          intViewAllowed ,
          intAddAllowed ,
          intUpdateAllowed ,
          intDeleteAllowed ,
          numDomainID
        )

 SELECT   
                AGM.numGroupID ,
                PM.numModuleID ,
                PM.numPageID ,
                PM.bitIsExportApplicable  AS intExportAllowed ,
                 0 AS intPrintAllowed ,
                PM.bitIsViewApplicable  AS  intViewAllowed ,
                PM.bitIsAddApplicable  AS  intAddAllowed ,
                PM.bitIsUpdateApplicable  AS  intUpdateAllowed ,
                PM.bitIsDeleteApplicable  AS  intDeleteAllowed,
                AGM.numDomainID
                /*,
				PM.vcPageDesc,
				(SELECT MM.vcModuleName FROM dbo.ModuleMaster MM WHERE numModuleID=PM.numModuleID) AS vcModuleName*/
FROM dbo.AuthenticationGroupMaster AGM CROSS JOIN dbo.PageMaster PM 
WHERE numPageID = @numMaxPageID



SELECT * FROM  GroupAuthorization WHERE numPageID=@numMaxPageID 


-------------------
-- Create default template for  all domain
FrmMaintanance.aspx -> click EXECUTE button 



ROLLBACK TRANSACTION

--------------------------------------------------------------------
/*updated on production on 20-03-2013*/
--Get category whose parent category does not exist
  SELECT numCategoryID,vcCategoryName,numDepCategory FROM dbo.Category WHERE ISNULL(numDepCategory,0) >0  AND ISNULL(numDepCategory,0) NOT IN (SELECT numCategoryID FROM dbo.Category) 
-- update such categories 
  UPDATE dbo.Category SET numDepCategory = NULL WHERE numCategoryID IN (
	SELECT numCategoryID FROM dbo.Category WHERE ISNULL(numDepCategory,0) >0  AND ISNULL(numDepCategory,0) NOT IN (SELECT numCategoryID FROM dbo.Category)   
  )
	
/* Item search result Export- updated on production on 21-03-2013*/	
UPDATE dbo.PageMaster SET bitIsExportApplicable=1 WHERE numModuleID=9 AND numPageID=11


ALTER TABLE dbo.WareHouseItems ADD
	numWLocationID numeric(18, 0) NULL
	
	

SET IDENTITY_INSERT dbo.ListMaster ON 
INSERT INTO dbo.ListMaster
        ( numListID,vcListName ,
          numCreatedBy ,
          bintCreatedDate ,
          numModifiedBy ,
          bintModifiedDate ,
          bitDeleted ,
          bitFixed ,
          numDomainID ,
          bitFlag ,
          vcDataType ,
          numModuleID
        )
VALUES  ( 50,'Percentage Complete' , -- vcListName - varchar(50)
          1 , -- numCreatedBy - numeric
          '2013-04-15 10:13:42' , -- bintCreatedDate - datetime
          NULL , -- numModifiedBy - numeric
          '2013-04-15 10:13:42' , -- bintModifiedDate - datetime
          0, -- bitDeleted - bit
          1 , -- bitFixed - bit
          1 , -- numDomainID - numeric
          1 , -- bitFlag - bit
          'numeric' , -- vcDataType - varchar(15)
          2  -- numModuleID - numeric
        )
SET IDENTITY_INSERT  dbo.ListMaster OFF  


INSERT INTO dbo.ListDetails 
        ( numListID ,
          vcData ,
          numCreatedBY ,
          bintCreatedDate ,
          numModifiedBy ,
          bintModifiedDate ,
          bitDelete ,
          numDomainID ,
          constFlag ,
          sintOrder
        )
       SELECT 50,Id,1,GETDATE(),1,GETDATE(),0,1,1,2 FROM dbo.SplitIDs('2, 5,10,15,20,25,30,40,50,60,70,80,85,90,95,100',',')   
	
	
ALTER TABLE dbo.OpportunityMaster
ADD numPercentageComplete numeric(9)

SET IDENTITY_INSERT dbo.DycFieldMaster ON 
INSERT INTO dbo.DycFieldMaster
        ( numFieldID,numModuleID ,
          numDomainID ,
          vcFieldName ,
          vcDbColumnName ,
          vcOrigDbColumnName ,
          vcPropertyName ,
          vcLookBackTableName ,
          vcFieldDataType ,
          vcFieldType ,
          vcAssociatedControlType ,
          vcToolTip ,
          vcListItemType ,
          numListID ,
          PopupFunctionName ,
          [order] ,
          tintRow ,
          tintColumn ,
          bitInResults ,
          bitDeleted ,
          bitAllowEdit ,
          bitDefault ,
          bitSettingField ,
          bitAddField ,
          bitDetailField ,
          bitAllowSorting ,
          bitWorkFlowField ,
          bitImport ,
          bitExport ,
          bitAllowFiltering ,
          bitInlineEdit ,
          bitRequired ,
          intColumnWidth ,
          intFieldMaxLength
        )
VALUES  ( 465,3 , -- numModuleID - numeric
          NULL , -- numDomainID - numeric
          N'Percentage Complete' , -- vcFieldName - nvarchar(50)
          N'numPercentageComplete' , -- vcDbColumnName - nvarchar(50)
          N'numPercentageComplete' , -- vcOrigDbColumnName - nvarchar(50)
          'PercentageComplete' , -- vcPropertyName - varchar(100)
          N'OpportunityMaster' , -- vcLookBackTableName - nvarchar(50)
          'N' , -- vcFieldDataType - char(1)
          'N' , -- vcFieldType - char(1)
          N'SelectBox' , -- vcAssociatedControlType - nvarchar(50)
          Null , -- vcToolTip - nvarchar(1000)
          'LI' , -- vcListItemType - varchar(3)
          50, -- numListID - numeric
          NULL , -- PopupFunctionName - varchar(100)
          7 , -- order - tinyint
          15 , -- tintRow - tinyint
          1 , -- tintColumn - tinyint
          1 , -- bitInResults - bit
          0 , -- bitDeleted - bit
          1 , -- bitAllowEdit - bit
          0, -- bitDefault - bit
          1, -- bitSettingField - bit
          0, -- bitAddField - bit
          1 , -- bitDetailField - bit
          1 , -- bitAllowSorting - bit
          1 , -- bitWorkFlowField - bit
          null , -- bitImport - bit
          NULL , -- bitExport - bit
          1 , -- bitAllowFiltering - bit
          NULL , -- bitInlineEdit - bit
          NULL , -- bitRequired - bit
          NULL , -- intColumnWidth - int
          NULL  -- intFieldMaxLength - int
        )
SET IDENTITY_INSERT  dbo.DycFieldMaster OFF 

INSERT INTO dbo.DycFormField_Mapping
        ( numModuleID ,
          numFieldID ,
          numDomainID ,
          numFormID ,
          bitAllowEdit ,
          bitInlineEdit ,
          vcFieldName ,
          vcAssociatedControlType ,
          vcPropertyName ,
          PopupFunctionName ,
          [order] ,
          tintRow ,
          tintColumn ,
          bitInResults ,
          bitDeleted ,
          bitDefault ,
          bitSettingField ,
          bitAddField ,
          bitDetailField ,
          bitAllowSorting ,
          bitWorkFlowField ,
          bitImport ,
          bitExport ,
          bitAllowFiltering ,
          bitRequired ,
          numFormFieldID ,
          intSectionID ,
          bitAllowGridColor
        )
VALUES  ( 3 , -- numModuleID - numeric
          465 , -- numFieldID - numeric
          NULL , -- numDomainID - numeric
          38 , -- numFormID - numeric
          1 , -- bitAllowEdit - bit
          1 , -- bitInlineEdit - bit
          N'Percentage Complete' , -- vcFieldName - nvarchar(50)
          N'SelectBox' , -- vcAssociatedControlType - nvarchar(50)
          'PercentageComplete' , -- vcPropertyName - varchar(100)
          null , -- PopupFunctionName - varchar(100)
          0 , -- order - tinyint
         NULL , -- tintRow - tinyint
          NULL , -- tintColumn - tinyint
          1 , -- bitInResults - bit
          0, -- bitDeleted - bit
          0 , -- bitDefault - bit
          1 , -- bitSettingField - bit
          0 , -- bitAddField - bit
          1, -- bitDetailField - bit
          1, -- bitAllowSorting - bit
          1, -- bitWorkFlowField - bit
          NULL , -- bitImport - bit
          NULL , -- bitExport - bit
          1 , -- bitAllowFiltering - bit
          NULL , -- bitRequired - bit
          NULL , -- numFormFieldID - numeric
          NULL , -- intSectionID - int
          1  -- bitAllowGridColor - bit
        )
        
        


ROLLBACK TRANSACTION



/******************************************************************
Project: Release 1.4 Date: 16.03.2013
Comments: 
*******************************************************************/
--BEGIN TRANSACTION
--GO
--
--/*KV matz trial balance issue*/
--SELECT * FROM dbo.General_Journal_Details WHERE numChartAcntId IS NULL AND numDomainId=153
--update General_Journal_Details set numChartAcntID = 4529 where numTransactionID in (392200,392153,392152,392131)
--
--/*For GIS already updated on production */
--UPDATE dbo.OpportunityMaster SET tintSourceType = 2 WHERE tintSource=21 AND tintSourceType<>2 AND numDomainId=135
--
--
--/*Upadate connection string in UI portal and BizCart*/
--"Pooling=true; Max pool size=200; Min pool size=0;"
--
--DECLARE @v sql_variant 
--SET @v = N'0 = Lead ,1 = Prospect , 2 = Account'
--EXECUTE sp_updateextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'DivisionMaster', N'COLUMN', N'tintCRMType'
--
--
--SELECT * FROM dbo.ListMaster WHERE vcListName LIKE '%billing term%'
--
--UPDATE dbo.ListMaster SET bitDeleted = 1 WHERE numListID=296
--
--
--ROLLBACK TRANSACTION
--
--/******************************************************************
--Project: Inventory Adjustment Date: 19.01.2013
--Comments: 
--*******************************************************************/
--BEGIN TRANSACTION
--GO
--
--
----SELECT TOP 50 * FROM dbo.General_Journal_Details WHERE numDomainId=171 ORDER BY numTransactionId DESC 
----SELECT TOP 50 * FROM dbo.General_Journal_Header  ORDER BY numJournal_Id DESC 
--
--
--IF NOT EXISTS(SELECT * FROM dbo.AccountingChargeTypes WHERE chChargeCode='IA' )
--BEGIN
--INSERT INTO dbo.AccountingChargeTypes
--        ( vcChageType ,
--          vcChargeAccountCode ,
--          chChargeCode
--        )
--VALUES  ( 'Inventory Adjustment' , -- vcChageType - varchar(100)
--          '' , -- vcChargeAccountCode - varchar(50)
--          'IA'  -- chChargeCode - char(10)
--        )
--	
--END
--
--
--UPDATE dbo.General_Journal_Details SET varDescription= 'Inventory_Adjustment',chBizDocItems='IA1'  WHERE numItemID>0 AND numJournalId IN (SELECT numJournal_Id FROM dbo.General_Journal_Header WHERE varDescription LIKE 'Stock Adjustment:%') 
--SELECT * FROM dbo.General_Journal_Details WHERE varDescription <> 'Inventory_Adjustment' AND numJournalId IN (SELECT numJournal_Id FROM dbo.General_Journal_Header WHERE varDescription LIKE 'Stock Adjustment:%') 
--
--
--DECLARE @v sql_variant 
--SET @v = N'Project Expenses->PE   Project Time ->PT  Opportunity Expenses->OE Opportunity Time->OT,Inventory Adjustment->IA1, COGS->IA'
--EXECUTE sp_updateextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'General_Journal_Details', N'COLUMN', N'chBizDocItems'
--GO
--
--
--IF NOT EXISTS(SELECT * FROM dbo.AccountingChargeTypes WHERE chChargeCode='OE' )
--BEGIN
--INSERT INTO dbo.AccountingChargeTypes
--        ( vcChageType ,
--          vcChargeAccountCode ,
--          chChargeCode
--        )
--VALUES  ( 'Opening Balance Equity' , -- vcChageType - varchar(100)
--          '' , -- vcChargeAccountCode - varchar(50)
--          'OE'  -- chChargeCode - char(10)
--        )
--	
--END
--
--/*Profit and loss formula */
--DECLARE @numDomainID NUMERIC(9)
--DECLARE @TotalIncome MONEY;
--DECLARE @TotalExpense MONEY;
--DECLARE @TotalCOGS MONEY;
--DECLARE @PLCHARTID NUMERIC(9)
--DECLARE @PLOPENING MONEY;
--DECLARE @CURRENTPL MONEY ;
--
--SET @numDomainID = 174 
--
--SELECT @TotalIncome = ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM view_journal VJ WHERE numDomainID=@numDomainID AND vcAccountCode  LIKE '0103%'
--SELECT @TotalExpense = ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM view_journal VJ WHERE numDomainID=@numDomainID AND vcAccountCode  LIKE '0104%'
--SELECT @TotalCOGS =  ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM view_journal VJ WHERE numDomainID=@numDomainID AND vcAccountCode  LIKE '0106%'
--PRINT 'TotalIncome=									' + CAST(@TotalIncome AS VARCHAR(20))
--PRINT 'TotalExpense=									'+ CAST(@TotalExpense AS VARCHAR(20))
--PRINT 'TotalCOGS=										'+ CAST(@TotalCOGS AS VARCHAR(20))
--SET @CURRENTPL = (@TotalIncome + @TotalExpense + @TotalCOGS)
--PRINT 'Current Profit/Loss = (Income - expense - cogs)= ' + CAST( @CURRENTPL AS VARCHAR(20))
--
--
--SELECT @PLCHARTID = numAccountId FROM dbo.Chart_Of_Accounts WHERE numDomainId=@numDomainID AND bitProfitLoss=1
----PRINT 'PL Account ID=' +CAST( @PLCHARTID AS VARCHAR(20))
--SELECT @PLOPENING = ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM view_journal VJ WHERE numDomainID=@numDomainID
--/* AND numAccountId=@PLCHARTID*/
--( vcAccountCode  LIKE '0103' or  vcAccountCode  LIKE '0104%' or  vcAccountCode  LIKE '0106%')
--AND datEntry_Date <=  DATEADD(Minute,-1,@dtFromDate);
--PRINT 'PL Account Opening=' +CAST( @PLOPENING AS VARCHAR(20))
--PRINT 'PL = ' + CAST( @PLOPENING + @CURRENTPL AS VARCHAR(20))
--
--
--
--
--
-------Maintanance task
--
--/*Step 1: Opening balance equity account must be created befre this task TO executed
--
--step 2* disable validation on fy closing */
--GO
--IF EXISTS ( SELECT  *
--            FROM    sysobjects
--            WHERE   xtype = 'p'
--                    AND NAME = 'USP_ValidateFinancialYearClosingDate' ) 
--    DROP PROCEDURE USP_ValidateFinancialYearClosingDate
--GO
--
--CREATE PROCEDURE USP_ValidateFinancialYearClosingDate
--@numDomainID NUMERIC,
--@dtEntryDate DATETIME -- pass date on which journal will be passed to accounting 
--AS 
--BEGIN
----	IF EXISTS(SELECT * FROM dbo.FinancialYear WHERE numDomainId=@numDomainID AND ( ISNULL(bitCloseStatus,0)=1 OR ISNULL(bitAuditStatus,0)=1 )
----	AND @dtEntryDate BETWEEN dtPeriodFrom AND dtPeriodTo)
----	BEGIN
----			RAISERROR ('FY_CLOSED',16,1);
--            RETURN -1
----	END
--	
--END
----> Step 3 :Run Opening balance journal entry creating task USP_GetCOAwithOpeningBalance_Temp
--
----> Step 4 :
--GO
--IF EXISTS ( SELECT  *
--            FROM    sysobjects
--            WHERE   xtype = 'p'
--                    AND NAME = 'USP_ValidateFinancialYearClosingDate' ) 
--    DROP PROCEDURE USP_ValidateFinancialYearClosingDate
--GO
--
--CREATE PROCEDURE USP_ValidateFinancialYearClosingDate
--@numDomainID NUMERIC,
--@dtEntryDate DATETIME=NULL, -- pass date on which journal will be passed to accounting 
--@tintRecordType AS TINYINT=0,
--@numRecordID AS NUMERIC=0
--AS 
--BEGIN
--	IF @tintRecordType=0 --General for Date
--	BEGIN
--		IF EXISTS(SELECT * FROM dbo.FinancialYear WHERE numDomainId=@numDomainID AND ( ISNULL(bitCloseStatus,0)=1 OR ISNULL(bitAuditStatus,0)=1 )
--			AND @dtEntryDate BETWEEN dtPeriodFrom AND dtPeriodTo)
--			BEGIN
--				RAISERROR ('FY_CLOSED',16,1);
--				RETURN -1
--			END
--	END
--	ELSE IF @tintRecordType=1 --For Item Delete
--	BEGIN
--		IF EXISTS(SELECT GJD.numJournalId FROM dbo.General_Journal_Header GJH JOIN dbo.General_Journal_Details GJD ON GJH.numJournal_Id = GJD.numJournalId
--	       JOIN FinancialYear FY ON GJH.numDomainId = FY.numDomainId WHERE GJH.numDomainId=@numDomainID AND GJD.numItemID=@numRecordID AND ( ISNULL(FY.bitCloseStatus,0)=1 OR ISNULL(FY.bitAuditStatus,0)=1 )
--			AND GJH.datEntry_Date BETWEEN FY.dtPeriodFrom AND FY.dtPeriodTo )
--			BEGIN
--				RAISERROR ('FY_CLOSED',16,1);
--				RETURN -1
--			END
--	END	
--	ELSE IF @tintRecordType=2 --For Opportunity Delete
--	BEGIN
--		IF EXISTS(SELECT GJD.numJournalId FROM dbo.General_Journal_Header GJH JOIN dbo.General_Journal_Details GJD ON GJH.numJournal_Id = GJD.numJournalId
--	       JOIN FinancialYear FY ON GJH.numDomainId = FY.numDomainId WHERE GJH.numDomainId=@numDomainID AND GJH.numOppId=@numRecordID AND ( ISNULL(FY.bitCloseStatus,0)=1 OR ISNULL(FY.bitAuditStatus,0)=1 )
--			AND GJH.datEntry_Date BETWEEN FY.dtPeriodFrom AND FY.dtPeriodTo )
--			BEGIN
--				RAISERROR ('FY_CLOSED',16,1);
--				RETURN -1
--			END
--	END	
--	
--	
--END
--
--

--UPDATE dbo.General_Journal_Details SET varDescription= 'Inventory_Adjustment' WHERE numItemID>0 AND numJournalId IN (SELECT numJournal_Id FROM dbo.General_Journal_Header WHERE varDescription LIKE 'Stock Adjustment:%') 
ROLLBACK TRANSACTION

/******************************************************************
Project: Bug fixing Date: 14.01.2013
Comments: 
*******************************************************************/
/*
/*1 index was modified to include non-key columns*/
DROP INDEX [IX_General_Journal_Details] ON [dbo].[General_Journal_Details] 
CREATE NONCLUSTERED INDEX [IX_General_Journal_Details] ON [dbo].[General_Journal_Details] 
(
 [numTransactionId] ASC,
 [numDebitAmt] ASC,
 [numChartAcntId] ASC,
 [numCustomerId] ASC,
 [numJournalId] ASC
)
INCLUDE ( [vcReference],
[bitReconcile],
[bitCleared],
[varDescription],
[numCreditAmt])
/*2nd index was on a small table which was involved in multiple scans*/
CREATE NONCLUSTERED INDEX [IX_CheckHeader_numReferenceID] ON [dbo].[CheckHeader] 
(
 [numReferenceID] ASC,
 [tintReferenceType] ASC,
 [numCheckHeaderID] ASC
)
INCLUDE ( [vcMemo],
[monAmount],
[numCheckNo])


*/

/******************************************************************
Project: Shipping Management   Date: 07.01.2013
Comments: 
*******************************************************************/
/*BEGIN TRANSACTION
GO
--Install E-Payment integrator V5 license on server
DELETE FROM dbo.DycFormConfigurationDetails WHERE numFormId=39 AND numFieldId=250
DELETE FROM dbo.DycFormField_Mapping WHERE vcFieldName='Shipping Amount' AND numFormID=39
UPDATE dbo.DycFormField_Mapping SET bitImport =1 WHERE numFormID=36 AND vcFieldName LIKE 'Web Link%'
--monShipCost Field has been obsolete from OpportunityMaster table
/*ALTER TABLE dbo.OpportunityMaster
DROP COLUMN numShipVia ,
vcTrackingNo ,
vcShippingMethod ,
dtDeliveryDate 

ALTER TABLE dbo.OpportunityMaster
ADD numShipVia numeric(18,0) NULL,
vcTrackingNo varchar(500) NULL,
vcShippingMethod varchar(100) NULL,
dtDeliveryDate DATETIME NULL
*/

ROLLBACK TRANSACTION
*/

/******************************************************************
Project: Accounting   Date: 31.12.2012
Comments: Deposit Table modifications
*******************************************************************/
/*
BEGIN TRANSACTION
GO
ALTER TABLE dbo.DepositMaster ADD
	bitDepositedToAcnt bit NULL
GO
ALTER TABLE dbo.DepositMaster ADD CONSTRAINT
	DF_DepositMaster_bitDepositedToAcnt DEFAULT 0 FOR bitDepositedToAcnt
GO
UPDATE dbo.DepositMaster SET bitDepositedToAcnt =0 

ALTER TABLE dbo.DepositeDetails ADD
	numChildDepositID numeric(18, 0) NULL
GO
ROLLBACK TRANSACTION

*/


