/******************************************************************
Project: Release 2.4 Date: 25.11.2013
Comments: 
*******************************************************************/

/*******************Kamal Script******************/
--/************************************************************************************************/
--/************************************************************************************************/


ALTER TABLE dbo.CFW_Loc_Master ADD
	vcCustomLookBackTableName varchar(50) NULL

--select * from CFW_Loc_Master
UPDATE CFW_Loc_Master SET vcCustomLookBackTableName='CFW_FLD_Values' WHERE Loc_Id=1
UPDATE CFW_Loc_Master SET vcCustomLookBackTableName='CFW_Fld_Values_Opp' WHERE Loc_Id=2
UPDATE CFW_Loc_Master SET vcCustomLookBackTableName='CFW_FLD_Values_Case' WHERE Loc_Id=3
UPDATE CFW_Loc_Master SET vcCustomLookBackTableName='CFW_FLD_Values_Cont' WHERE Loc_Id=4
UPDATE CFW_Loc_Master SET vcCustomLookBackTableName='CFW_FLD_Values_Item' WHERE Loc_Id=5
UPDATE CFW_Loc_Master SET vcCustomLookBackTableName='CFW_Fld_Values_Opp' WHERE Loc_Id=6
UPDATE CFW_Loc_Master SET vcCustomLookBackTableName='CFW_Fld_Values_Serialized_Items' WHERE Loc_Id=9
UPDATE CFW_Loc_Master SET vcCustomLookBackTableName='CFW_FLD_Values_Pro' WHERE Loc_Id=11
UPDATE CFW_Loc_Master SET vcCustomLookBackTableName='CFW_FLD_Values' WHERE Loc_Id=12
UPDATE CFW_Loc_Master SET vcCustomLookBackTableName='CFW_FLD_Values' WHERE Loc_Id=13
UPDATE CFW_Loc_Master SET vcCustomLookBackTableName='CFW_FLD_Values' WHERE Loc_Id=14


/*******************Manish Script******************/
/******************************************************************/
/******************************************************************/

/******************************************************************
Project: BizCart   Date: 12.Nov.2013
Comments: Cart Item secondary sorting
*******************************************************************/
BEGIN TRANSACTION

---SELECT * FROM DynamicFormMaster 
INSERT dbo.DynamicFormMaster 
(numFormId,vcFormName,cCustomFieldsAssociated,cAOIAssociated,bitDeleted,tintFlag,bitWorkFlow,vcLocationID,bitAllowGridColor )
SELECT 84 ,'Secondary Sort Order For Cart Item', 'Y', 'N',0,0, NULL,'5', NULL 

INSERT INTO dbo.DycFormField_Mapping 
	(numModuleID,numFieldID,numDomainID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,PopupFunctionName,[order],
	 tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitImport,bitExport,
	 bitAllowFiltering,bitRequired,numFormFieldID,intSectionID,bitAllowGridColor)
SELECT numModuleID,numFieldID,numDomainID,84,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,PopupFunctionName,1,
	 tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,1,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,0,bitExport,
	 1,bitRequired,numFormFieldID,intSectionID,bitAllowGridColor 
	 FROM dbo.DycFormField_Mapping 
	 WHERE numFormID = 20 
	 AND vcFieldName IN ('Item Name','Model ID','Manufacturer')	

UPDATE DynamicFormMaster SET vcFormName = 'Secondary Sort Order For Cart Item' WHERE vcFormName = 'Sort Order For Cart Item'

-- ADD new column for enabling Secondary sorting
ALTER TABLE EcommerceDTL ADD bitEnableSecSorting BIT NULL

SELECT * FROM DynamicFormMaster 
INSERT dbo.DynamicFormMaster 
(numFormId,vcFormName,cCustomFieldsAssociated,cAOIAssociated,bitDeleted,tintFlag,bitWorkFlow,vcLocationID,bitAllowGridColor )
SELECT 85 ,'Priamry Sort Order For Cart Item', 'Y', 'N',0,0, NULL,'5', NULL 

INSERT INTO dbo.DycFormField_Mapping 
	(numModuleID,numFieldID,numDomainID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,PopupFunctionName,[order],
	 tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitImport,bitExport,
	 bitAllowFiltering,bitRequired,numFormFieldID,intSectionID,bitAllowGridColor)
SELECT numModuleID,numFieldID,numDomainID,85,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,PopupFunctionName,1,
	 tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,1,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,0,bitExport,
	 1,bitRequired,numFormFieldID,intSectionID,bitAllowGridColor 
	 FROM dbo.DycFormField_Mapping 
	 WHERE numFormID = 20 
	 AND vcFieldName IN ('Item Name','Model ID','Manufacturer')	
---SELECT * FROM dbo.DycFormField_Mapping WHERE numFormID = 20 AND vcAssociatedControlType = 'TextBox'
---DELETE FROM dbo.DycFormField_Mapping WHERE numFormID = 84
---SELECT * FROM DycFieldMaster WHERE numFieldID IN (SELECT numFieldID FROM dbo.DycFormField_Mapping WHERE numFormID = 84)
-- SELECT * FROM EcommerceDTL

ROLLBACK

/******************************************************************
Project: BACRM / PORTAL   Date: 28.Oct.2013
Comments: Add field to Use Bizdoc amount into Total Insured Value & Total Customs Value
*******************************************************************/
BEGIN TRANSACTION
ALTER TABLE Domain ADD bitUseBizdocAmount BIT
ROLLBACK

/******************************************************************
Project: BACRM / PORTAL   Date: 26.Oct.2013
Comments: Remove 2 forms from grid column config / Change in column configuration
*******************************************************************/
BEGIN TRANSACTION
UPDATE DynamicFormMaster SET bitDeleted = 1 WHERE vcFormName IN ('Knowledge Base (Portal)','Documents (Portal)') 

SELECT * FROM dycFormField_Mapping WHERE numFormID = 82  AND vcAssociatedControlType = 'Popup'
UPDATE  dycFormField_Mapping SET bitSettingField = 0 WHERE numFormID = 82  AND vcAssociatedControlType = 'Popup'

UPDATE  dycFormField_Mapping SET bitSettingField = 0 WHERE numFormID = 78 AND vcFieldName IN ('Active','Billing Address','Linked Items','Tracking No','Shipping Address','Contact','Deal Status','Documents','Recurring Template','Share With')

UPDATE  dycFormField_Mapping SET bitSettingField = 0 WHERE numFormID = 79 AND vcFieldName IN ('Active','Billing Address','Linked Items','Tracking No','Shipping Address','Contact','Deal Status','Documents','Recurring Template','Share With')

SELECT * FROM dycFormField_Mapping WHERE numFormID = 80  AND vcAssociatedControlType = 'Popup'
UPDATE  dycFormField_Mapping SET bitSettingField = 0 WHERE numFormID = 80  AND vcAssociatedControlType = 'Popup'

UPDATE  dycFormField_Mapping SET bitSettingField = 0 
WHERE numFormID = 80 
 AND vcFieldName IN ('Email','Phone','')
ROLLBACK

/******************************************************************
Project: BACRM / PORTAL   Date: 17.Oct.2013
Comments: Add New Field to Hide Tabs from Portal
*******************************************************************/
BEGIN TRANSACTION
 ALTER TABLE DOmain ADD vcHideTabs VARCHAR(1000)
ROLLBACK

/******************************************************************
Project: PORTAL   Date: 11.Oct.2013
Comments: Added 3 new fields for Item List
*******************************************************************/
BEGIN TRANSACTION

INSERT INTO dbo.EmailMergeFields (vcMergeField,vcMergeFieldValue,numModuleID,tintModuleType)
SELECT 'ModelID','##ModelID##',2,1
UNION 
SELECT 'FirstPriceLevelValue','##FirstPriceLevelValue##',2,1
UNION 
SELECT 'FirstPriceLevelDiscount','##FirstPriceLevelDiscount##',2,1
	
ROLLBACK

/******************************************************************
Project: PORTAL   Date: 07.Oct.2013
Comments: Removed Default PRM from AuthenticationGroupMaster
*******************************************************************/
BEGIN TRANSACTION
DELETE FROM AuthenticationGroupMaster WHERE tintGroupType = 3
ROLLBACK


/******************************************************************
Project: PORTAL   Date: 30.Sep.2013
Comments: Portal Column settings, adding form ids, form fields 
*******************************************************************/
BEGIN TRANSACTION
ALTER TABLE dbo.eCommerceDTL ADD bitShowPriceUsingPriceLevel BIT
ROLLBACK
