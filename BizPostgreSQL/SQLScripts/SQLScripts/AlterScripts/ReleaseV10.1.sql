/******************************************************************
Project: Release 10.1 Date: 13.AUG.2018
Comments: ALTER SCRIPTS
*******************************************************************/


USE [Production.2014]
GO

/****** Object:  Table [dbo].[ShippingService]    Script Date: 13-Aug-18 1:47:44 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[ShippingService](
	[numShippingServiceID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numDomainID] [numeric](18, 0) NOT NULL,
	[numShipViaID] [numeric](18, 0) NOT NULL,
	[vcShipmentService] [varchar](300) NOT NULL,
	[numShipmentServiceID] [numeric](18, 0) NULL,
	[bitResidential] [bit] NULL,
 CONSTRAINT [PK_ShippingService] PRIMARY KEY CLUSTERED 
(
	[numShippingServiceID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING ON
GO

-----------------------------------------

USE [Production.2014]
GO

/****** Object:  Table [dbo].[ShippingServiceAbbreviations]    Script Date: 13-Aug-18 1:46:02 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[ShippingServiceAbbreviations](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[numDomainID] [numeric](18, 0) NOT NULL,
	[numShippingServiceID] [numeric](18, 0) NOT NULL,
	[vcAbbreviation] [varchar](300) NULL,
 CONSTRAINT [PK__Shipping__3214EC2725DC48AD] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING ON
GO

ALTER TABLE [dbo].[ShippingServiceAbbreviations]  WITH CHECK ADD  CONSTRAINT [FK_ShippingServiceAbbreviation] FOREIGN KEY([numShippingServiceID])
REFERENCES [dbo].[ShippingService] ([numShippingServiceID])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[ShippingServiceAbbreviations] CHECK CONSTRAINT [FK_ShippingServiceAbbreviation]
GO




-------------------

SET IDENTITY_INSERT ShippingService ON

INSERT INTO ShippingService
(
	numShippingServiceID,
	numShipViaID,
	numDomainId,
	vcShipmentService,
	numShipmentServiceID,
	bitResidential
)
VALUES
--- 91 FeDEX
(10,91,0,'FedEx Priority Overnight',10,0),
(11,91,0,'FedEx Standard Overnight',11,0),
(12,91,0,'FedEx Overnight',12,0),
(13,91,0,'FedEx 2nd Day',13,0),
(14,91,0,'FedEx Express Saver',14,0),
(15,91,0,'FedEx Ground',15,0),
(16,91,0,'FedEx Ground Home Delivery',16,0),
(17,91,0,'FedEx 1 Day Freight',17,0),
(18,91,0,'FedEx 2 Day Freight',18,0),
(19,91,0,'FedEx 3 Day Freight',19,0),
(20,91,0,'FedEx International Priority',20,0),
(21,91,0,'FedEx International Priority Distribution',21,0),
(22,91,0,'FedEx International Economy',22,0),
(23,91,0,'FedEx International Economy Distribution',23,0),
(24,91,0,'FedEx International First',24,0),
(25,91,0,'FedEx International Priority Freight',25,0),
(26,91,0,'FedEx International Economy Freight',26,0),
(27,91,0,'FedEx International Distribution Freight',27,0),
(28,91,0,'FedEx Europe International Priority',28,0),
--- 88 UPS
(40,88,0,'UPS Next Day Air',40,0),
(42,88,0,'UPS 2nd Day Air',42,0),
(43,88,0,'UPS Ground',43,0),
(48,88,0,'UPS 3Day Select',48,0),
(49,88,0,'UPS Next Day Air Saver',49,0),
(50,88,0,'UPS Saver',50,0),
(51,88,0,'UPS Next Day Air Early A.M.',51,0),
(55,88,0,'UPS 2nd Day Air AM',55,0),
--- 90 USPS
(70,90,0,'USPS Express',70,0),
(71,90,0,'USPS First Class',71,0),
(72,90,0,'USPS Priority',72,0),
(73,90,0,'USPS Parcel Post',73,0),
(74,90,0,'USPS Bound Printed Matter',74,0),
(75,90,0,'USPS Media',75,0),
(76,90,0,'USPS Library',76,0)

SET IDENTITY_INSERT ShippingService OFF




DBCC CHECKIDENT ('ShippingService', RESEED, 301)