/******************************************************************
Project: Release 4.5 Date: 04.May.2015
Comments: 
*******************************************************************/


---------------  Sandeep  ----------------------

--Change <authentication mode="Windows" />

--to

--<authentication mode="Forms">
--    <forms loginUrl="Login.aspx" timeout="20" />
--</authentication>

--in web.config

--ADD DDL TO GAC
--==============

--iTextShart
--AspNetPager in BIZCART

INSERT INTO DycFormField_Mapping 
(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,
bitDetailField,bitAllowSorting,bitAllowFiltering)
VALUES
(
	1,18,43,0,0,'Follow-up Status','SelectBox','FollowUpStatus',26,1,0,0,1,0,0,1,1
)

---------------------------------------------------------------------------------------------------

INSERT INTO PageNavigationDTL 
(numPageNavID,numModuleID,numParentID,vcPageNavName,vcNavURL,bitVisible,numTabID)
VALUES
(
240,37,62,'Rental Items','../Items/frmItemList.aspx?Page=Rental Items&ItemGroup=0',1,80
)

BEGIN TRANSACTION

DECLARE @numPageNavID NUMERIC(18,0) = 240 

DECLARE @TEMPDomain TABLE
(
	ID INT IDENTITY(1,1),
	numDomainID NUMERIC(18,0)
)

INSERT INTO @TEMPDomain SELECT numDomainId FROM Domain WHERE numDomainId <> -255

DECLARE @i AS INT = 1
DECLARE @COUNT AS INT
DECLARE @numDomainID AS NUMERIC(18,0)

SELECT @COUNT=COUNT(*) FROM @TEMPDomain


WHILE @i <= @COUNT
BEGIN
	SELECT @numDomainID=numDomainID FROM @TEMPDomain WHERE ID = @i

	DELETE FROM TreeNavigationAuthorization WHERE numPageNavID=@numPageNavID AND numDomainID = @numDomainId

	INSERT  INTO dbo.TreeNavigationAuthorization
    (
        numGroupID,
        numTabID,
        numPageNavID,
        bitVisible,
        numDomainID,
        tintType
    )
    SELECT    
		A.numGroupID,
		80,
		@numPageNavID,
		1,
		A.numDomainID,
		1
	FROM      
		AuthenticationGroupMaster A
	WHERE    
		A.numDomainId = @numDomainID

	SET @i = @i + 1
END

ROLLBACK

-----------------------------------------------------------------------------------------------------------

UPDATE PageNavigationDTL SET intSortOrder=10000 WHERE numPageNavID=173

-----------------------------------------------------------------------------------------------------------

INSERT INTO DynamicFormMaster
(numFormId,vcFormName,cCustomFieldsAssociated,cAOIAssociated,bitDeleted)
VALUES
(126,'Inventory Adjustment Grid Columns','Y','N',0)


INSERT INTO DycFormField_Mapping
(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,
vcPropertyName,PopupFunctionName,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,
bitDetailField,bitAllowSorting,bitImport,bitExport,bitAllowFiltering,numFormFieldID)
SELECT
	numModuleID,numFieldID,126,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,
	vcPropertyName,PopupFunctionName,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,
	bitDetailField,bitAllowSorting,bitImport,bitExport,bitAllowFiltering,numFormFieldID
FROM
	DycFormField_Mapping
WHERE
	numFormID = 21
	AND numFieldID NOT IN (200,198,197,196,195,210,469,351,295,199,211,189,192)

UPDATE DycFormField_Mapping SET bitDefault = 0 WHERE ISNULL(numFormID,0)=126

-------------------------------------------------------------------------------------------

UPDATE DycFormField_Mapping SET vcAssociatedControlType='TextBox' WHERE numFieldID=419 AND numFormID=39
UPDATE DycFormField_Mapping SET vcAssociatedControlType='TextBox' WHERE numFieldID=419 AND numFormID=41

-------------------------------------------------------------------------------------------

INSERT INTO DycFormField_Mapping 
(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,bitInResults,bitDeleted,bitDefault,bitAllowFiltering,intSectionID)
VALUES
(4,236,29,1,1,'UPC (M)','TextBox',1,0,0,1,1),
(4,237,29,1,1,'SKU (M)','TextBox',1,0,0,1,1)
