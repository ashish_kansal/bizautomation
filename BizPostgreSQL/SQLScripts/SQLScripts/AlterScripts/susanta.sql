/***************************************************************************/
/*********************** LAST UPDATE ON 24 APRIL 2016 ******************/
/***************************************************************************/

--UPDATE Domain SET monAmountPastDue=1 WHERE monAmountPastDue=NULL OR monAmountPastDue=0

--ALTER TABLE UserMaster
--ADD ProfilePic varchar(100)


--Alter table PaymentGatewayDTLID add numSiteId int


--UPDATE PaymentGatewayDTLID SET numSiteId=0 WHERE numSiteId IS NULL

--BEGIN TRANSACTION

--DECLARE @numFieldID AS NUMERIC(18,0)
--DECLARE @numFormFieldID AS NUMERIC(18,0)

--INSERT INTO DycFieldMaster
--(numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,[order],tintRow,tintColumn,
--bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numListID)
--VALUES
--(4,'Back Order','numBackOrder','numBackOrder','BackOrder','WareHouseItems','V','R','Label',46,1,1,1,0,0,0,1,0,1,1,1,0)

--SELECT @numFieldID = SCOPE_IDENTITY()

--INSERT INTO DynamicFormFieldMaster
--(numFormID,vcFormFieldName,vcFieldType,vcAssociatedControlType,vcDbColumnName,numListID,bitDeleted,vcFieldDataType,vcOrigDbColumnName,bitAllowEdit,vcLookBackTableName,
-- bitDefault,[order],bitInResults,vcPropertyName,tintRow,tintColumn,bitSettingField,bitAddField,bitDetailField,bitAllowSorting)
--VALUES
--(26,'Back Order','R','Label','numBackOrder',0,0,'V','numBackOrder',0,'WareHouseItems',0,7,1,'BackOrder',1,7,1,0,1,0)

--SELECT @numFormFieldID = SCOPE_IDENTITY()

--INSERT INTO DycFormField_Mapping
--(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,bitInResults,bitDeleted,
--bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numFormFieldID)
--VALUES
--(4,@numFieldID,26,1,1,'Back Order','Label','BackOrder',7,1,7,1,0,0,1,0,1,0,1,@numFormFieldID)

--ROLLBACK

--BEGIN TRANSACTION

--DECLARE @numFieldID AS NUMERIC(18,0)
--DECLARE @numFormFieldID AS NUMERIC(18,0)

--INSERT INTO DycFieldMaster
--(numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,[order],tintRow,tintColumn,
--bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numListID)
--VALUES
--(4,'Back Order','numBackOrder','numBackOrder','BackOrder','WareHouseItems','V','R','Label',46,1,1,1,0,0,0,1,0,1,1,1,0)

--SELECT @numFieldID = SCOPE_IDENTITY()

--INSERT INTO DynamicFormFieldMaster
--(numFormID,vcFormFieldName,vcFieldType,vcAssociatedControlType,vcDbColumnName,numListID,bitDeleted,vcFieldDataType,vcOrigDbColumnName,bitAllowEdit,vcLookBackTableName,
-- bitDefault,[order],bitInResults,vcPropertyName,tintRow,tintColumn,bitSettingField,bitAddField,bitDetailField,bitAllowSorting)
--VALUES
--(7,'Back Order','R','Label','numBackOrder',0,0,'V','numBackOrder',0,'WareHouseItems',0,7,1,'BackOrder',1,7,1,0,1,0)

--SELECT @numFormFieldID = SCOPE_IDENTITY()

--INSERT INTO DycFormField_Mapping
--(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,bitInResults,bitDeleted,
--bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numFormFieldID)
--VALUES
--(4,@numFieldID,7,1,1,'Back Order','Label','BackOrder',7,1,7,1,0,0,1,0,1,0,1,@numFormFieldID)

--ROLLBACK


--BEGIN TRANSACTION

--DECLARE @numFieldID AS NUMERIC(18,0)
--DECLARE @numFormFieldID AS NUMERIC(18,0)

--INSERT INTO DycFieldMaster
--(numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,[order],tintRow,tintColumn,
--bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numListID)
--VALUES
--(4,'Back Order','numBackOrder','numBackOrder','BackOrder','WareHouseItems','V','R','Label',46,1,1,1,0,0,0,1,0,1,1,1,0)

--SELECT @numFieldID = SCOPE_IDENTITY()

--INSERT INTO DynamicFormFieldMaster
--(numFormID,vcFormFieldName,vcFieldType,vcAssociatedControlType,vcDbColumnName,numListID,bitDeleted,vcFieldDataType,vcOrigDbColumnName,bitAllowEdit,vcLookBackTableName,
-- bitDefault,[order],bitInResults,vcPropertyName,tintRow,tintColumn,bitSettingField,bitAddField,bitDetailField,bitAllowSorting)
--VALUES
--(8,'Back Order','R','Label','numBackOrder',0,0,'V','numBackOrder',0,'WareHouseItems',0,7,1,'BackOrder',1,7,1,0,1,0)

--SELECT @numFormFieldID = SCOPE_IDENTITY()

--INSERT INTO DycFormField_Mapping
--(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,bitInResults,bitDeleted,
--bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numFormFieldID)
--VALUES
--(4,@numFieldID,8,1,1,'Back Order','Label','BackOrder',7,1,7,1,0,0,1,0,1,0,1,@numFormFieldID)

--ROLLBACK

--ALTER TABLE domain
--ADD numCost numeric

--ALTER TABLE OpportunityItems
--ADD numCost numeric

--ALTER TABLE domain ADD DEFAULT '1' FOR numCost

--BEGIN TRANSACTION

--DECLARE @numFieldID AS NUMERIC(18,0)
--DECLARE @numFormFieldID AS NUMERIC(18,0)

--INSERT INTO DycFieldMaster
--(numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,[order],tintRow,tintColumn,
--bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numListID)
--VALUES
--(3,'Cost','numCost','numCost','Cost','OpportunityItems','N','R','TextBox',46,1,1,1,0,0,0,1,0,1,1,1,0)

--SELECT @numFieldID = SCOPE_IDENTITY()

--INSERT INTO DynamicFormFieldMaster
--(numFormID,vcFormFieldName,vcFieldType,vcAssociatedControlType,vcDbColumnName,numListID,bitDeleted,vcFieldDataType,vcOrigDbColumnName,bitAllowEdit,vcLookBackTableName,
-- bitDefault,[order],bitInResults,vcPropertyName,tintRow,tintColumn,bitSettingField,bitAddField,bitDetailField,bitAllowSorting)
--VALUES
--(26,'Cost','R','TextBox','numCost',0,0,'V','N',0,'OpportunityItems',0,7,1,'Cost',1,7,1,0,1,0)

--SELECT @numFormFieldID = SCOPE_IDENTITY()

--INSERT INTO DycFormField_Mapping
--(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,bitInResults,bitDeleted,
--bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numFormFieldID)
--VALUES
--(3,@numFieldID,26,1,1,'Cost','TextBox','Cost',7,1,7,1,0,0,1,0,1,0,1,@numFormFieldID)

ROLLBACK

update domain set numCost=1

update PageNavigationDTL set vcPageNavName = 'Import & Update Wizard' where numPageNavID =205


update PageNavigationDTL set vcImageURL = '../images/Dashboard-25.png' where numPageNavID = 23

update PageNavigationDTL set vcImageURL = '../images/icon-recurring1.gif' where numPageNavID = 151

update PageNavigationDTL set vcImageURL = '../images/moneyup-25.png' where numPageNavID = 29

update PageNavigationDTL set vcImageURL = '../images/moneydown-25.png' where numPageNavID = 54

update PageNavigationDTL set vcImageURL = '../images/fileup-25.png' where numPageNavID = 81

update PageNavigationDTL set vcImageURL = '../images/filedown-25.png' where numPageNavID = 82

update PageNavigationDTL set bitVisible = 1 where numPageNavID = 249

update PageNavigationDTL set bitVisible = 1 where numPageNavID = 255

update PageNavigationDTL set vcImageURL = '../images/GreenMan-24.gif' where numPageNavID = 84

update PageNavigationDTL set vcImageURL = '../images/Admin-List1.png' where numPageNavID =61