/******************************************************************
Project: Release 4.2 Date: 03.FEB.2015
Comments: 
*******************************************************************/


------------------ SANDEEP ---------------------

ALTER TABLE dbo.SalesOrderConfiguration ADD
bitDisplayCreateInvoice BIT NULL 

UPDATE SalesOrderConfiguration SET bitDisplayCreateInvoice = 1

INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
)
VALUES
(	
	4,274,21,1,1,'Item Group','SelectBox',17,1,0,1,1,1,1
)

INSERT INTO ListDetails 
(numListID,vcData,numCreatedBY,bitDelete,numDomainID,constFlag,sintOrder)
VALUES
(302,'Not Ready to Build',1,1,1,1,0)

INSERT INTO ListDetails 
(numListID,vcData,numCreatedBY,bitDelete,numDomainID,constFlag,sintOrder)
VALUES
(302,'Ready to Build',1,1,1,1,0)


ALTER TABLE WorkOrderDetails ADD
numPOID NUMERIC(18,0)

ALTER TABLE WorkOrder ADD
numParentWOID NUMERIC(18,0)

ALTER TABLE OpportunityMaster ADD
bitFromWorkOrder BIT


------------------------------------------------


------------------ MANISH ---------------------

------/******************************************************************
------Project: BACRMUI   Date: 31.JAN.2015
------Comments: Added new control for onepagecheckout
------*******************************************************************/

BEGIN TRANSACTION

SELECT * FROM [PageElementAttributes] WHERE [PageElementAttributes].[numElementID] = 35
UPDATE [PageElementAttributes] SET [bitEditor] = 1 WHERE [PageElementAttributes].[numElementID] = 35

SELECT * FROM dbo.PageElementMaster 
SELECT vcMergeField,vcMergeFieldValue,numModuleID,tintModuleType FROM EmailMergeFields WHERE [numModuleID] = 44

DECLARE @MaxElementID AS NUMERIC(18,0)
SELECT @MaxElementID = MAX([numElementID]) + 1 FROM [PageElementMaster] 
PRINT @MaxElementID

INSERT INTO dbo.PageElementMaster 
(numElementID,vcElementName,vcUserControlPath,vcTagName,bitCustomization,bitAdd,bitDelete)
SELECT @MaxElementID,'NewOnePageCheckout','~/UserControls/NewOnePageCheckout.ascx','{#NewOnePageCheckout#}',1,NULL,NULL
--------------------------------
INSERT INTO dbo.EmailMergeModule (numModuleID,vcModuleName,tintModuleType) 
SELECT @MaxElementID,'NewOnePageCheckout',1
--------------------------------
--SELECT * FROM dbo.EmailMergeFields
INSERT INTO dbo.EmailMergeFields (vcMergeField,vcMergeFieldValue,numModuleID,tintModuleType)
SELECT vcMergeField,vcMergeFieldValue,@MaxElementID,tintModuleType FROM EmailMergeFields WHERE [numModuleID] = 10
UNION ALL 
SELECT 'Sub Total','##SubTotalLabel##',@MaxElementID,1
UNION ALL 
SELECT 'Discount','##DiscountLabel##',@MaxElementID,1
UNION ALL 
SELECT 'Tax','##TaxLabel##',@MaxElementID,1
UNION ALL 
SELECT 'Show Shipping Amount With Method','##ShowShippingAmountWithMethod##',@MaxElementID,1
UNION ALL 
SELECT 'Shipping Method','##ShippingMethodDropdown##',@MaxElementID,1
UNION ALL 
SELECT 'Shipping Charge','##ShippingChargeLabel##',@MaxElementID,1
UNION ALL 
SELECT 'Total Amount','##TotalAmountLabel##',@MaxElementID,1
UNION ALL 
SELECT 'Total Amount','##TotalAmount##',@MaxElementID,1
UNION ALL 
SELECT 'Comments','##Comments##',@MaxElementID,1
UNION ALL 
SELECT 'View Past Orders','##ViewPastOrdersButton##',@MaxElementID,1
UNION ALL
SELECT 'Shipping FirstName','##ShippingFirstName##',@MaxElementID,1
UNION ALL
SELECT 'Shipping LastName','##ShippingLastName##',@MaxElementID,1
UNION ALL
SELECT 'CurrencySymbol','##CurrencySymbol##',@MaxElementID,1


--------------------------------
INSERT INTO dbo.PageElementDetail (numElementID,numAttributeID,vcAttributeValue,numSiteID,numDomainID,vcHtml)
SELECT @MaxElementID,96,'',82,170,''
--------------------------------
INSERT INTO dbo.PageElementAttributes (numElementID,vcAttributeName,vcControlType,vcControlValues,bitEditor) 
SELECT @MaxElementID,'Html Customize','HtmlEditor','',1
--------------------------------

SELECT * FROM [dbo].[EmailMergeFields] AS EMF WHERE [EMF].[numModuleID] = 44
UPDATE [dbo].[EmailMergeFields] SET [vcMergeField] = 'Billing FirstName', [vcMergeFieldValue] = '##BillingFirstName##' WHERE [numModuleID] = 44 AND [vcMergeField] = 'Customer FirstName'
UPDATE [dbo].[EmailMergeFields] SET [vcMergeField] = 'Billing LastName', [vcMergeFieldValue] = '##BillingLastName##' WHERE [numModuleID] = 44 AND [vcMergeField] = 'Customer LastName'

ROLLBACK


------/******************************************************************
------Project: BACRMUI   Date: 21.JAN.2015
------Comments: Make ItemID available to Custom Report module : Opportunity Bizdoc Items
------*******************************************************************/
BEGIN TRANSACTION

INSERT INTO [dbo].[ReportModuleGroupFieldMappingMaster]
        ( [numReportModuleGroupID] ,
          [numReportFieldGroupID]
        )
VALUES  ( 9 , -- numReportModuleGroupID - numeric
          7  -- numReportFieldGroupID - numeric
        )
ROLLBACK


------/******************************************************************
------Project: BizService   Date: 20.JAN.2015
------Comments: Add New Field to Organization Import
------*******************************************************************/


BEGIN TRANSACTION

DECLARE @numMaxFieldId AS NUMERIC(18,0)
SELECT @numMaxFieldId = MAX(numFieldID) + 1 FROM [dbo].[DycFieldMaster] AS DFM
PRINT @numMaxFieldId

SET IDENTITY_INSERT [dbo].[DycFieldMaster] ON

INSERT INTO [dbo].[DycFieldMaster]
        ( [numFieldId],
		  [numModuleID] ,
          [numDomainID] ,
          [vcFieldName] ,
          [vcDbColumnName] ,
          [vcOrigDbColumnName] ,
          [vcPropertyName] ,
          [vcLookBackTableName] ,
          [vcFieldDataType] ,
          [vcFieldType] ,
          [vcAssociatedControlType] ,
          [vcToolTip] ,
          [vcListItemType] ,
          [numListID] ,
          [PopupFunctionName] ,
          [order] ,
          [tintRow] ,
          [tintColumn] ,
          [bitInResults] ,
          [bitDeleted] ,
          [bitAllowEdit] ,
          [bitDefault] ,
          [bitSettingField] ,
          [bitAddField] ,
          [bitDetailField] ,
          [bitAllowSorting] ,
          [bitWorkFlowField] ,
          [bitImport] ,
          [bitExport] ,
          [bitAllowFiltering] ,
          [bitInlineEdit] ,
          [bitRequired] ,
          [intColumnWidth] ,
          [intFieldMaxLength] ,
          [intWFCompare] ,
          [vcGroup] ,
          [vcWFCompareField]
        )
VALUES  ( @numMaxFieldId , -- numFieldID - numeric
		  4 , -- numModuleID - numeric
          NULL , -- numDomainID - numeric
          N'Default Expense Account' , -- vcFieldName - nvarchar(50)
          N'numDefaultExpenseAccountID' , -- vcDbColumnName - nvarchar(50)
          N'numDefaultExpenseAccountID' , -- vcOrigDbColumnName - nvarchar(50)
          'DefaultExpenseAccountID' , -- vcPropertyName - varchar(100)
          N'DivisionMaster' , -- vcLookBackTableName - nvarchar(50)
          'N' , -- vcFieldDataType - char(1)
          'R' , -- vcFieldType - char(1)
          N'SelectBox' , -- vcAssociatedControlType - nvarchar(50)
          N'Default Expense Account' , -- vcToolTip - nvarchar(1000)
          '' , -- vcListItemType - varchar(3)
          0 , -- numListID - numeric
          NULL , -- PopupFunctionName - varchar(100)
          63 , -- order - tinyint
          0 , -- tintRow - tinyint
          1 , -- tintColumn - tinyint
          1 , -- bitInResults - bit
          0 , -- bitDeleted - bit
          1 , -- bitAllowEdit - bit
          0 , -- bitDefault - bit
          1 , -- bitSettingField - bit
          1 , -- bitAddField - bit
          1 , -- bitDetailField - bit
          1 , -- bitAllowSorting - bit
          1 , -- bitWorkFlowField - bit
          1 , -- bitImport - bit
          NULL , -- bitExport - bit
          1 , -- bitAllowFiltering - bit
          NULL , -- bitInlineEdit - bit
          NULL , -- bitRequired - bit
          NULL , -- intColumnWidth - int
          NULL , -- intFieldMaxLength - int
          NULL , -- intWFCompare - int
          'Org.Details Fields' , -- vcGroup - varchar(100)
          NULL  -- vcWFCompareField - varchar(150)
        )

SET IDENTITY_INSERT [dbo].[DycFieldMaster] OFF
INSERT INTO [dbo].[DycFormField_Mapping]
        ( [numModuleID] ,
          [numFieldID] ,
          [numDomainID] ,
          [numFormID] ,
          [bitAllowEdit] ,
          [bitInlineEdit] ,
          [vcFieldName] ,
          [vcAssociatedControlType] ,
          [vcPropertyName] ,
          [PopupFunctionName] ,
          [order] ,
          [tintRow] ,
          [tintColumn] ,
          [bitInResults] ,
          [bitDeleted] ,
          [bitDefault] ,
          [bitSettingField] ,
          [bitAddField] ,
          [bitDetailField] ,
          [bitAllowSorting] ,
          [bitWorkFlowField] ,
          [bitImport] ,
          [bitExport] ,
          [bitAllowFiltering] ,
          [bitRequired] ,
          [numFormFieldID] ,
          [intSectionID] ,
          [bitAllowGridColor]
        )
VALUES  ( 4 , -- numModuleID - numeric
          @numMaxFieldId , -- numFieldID - numeric
          NULL , -- numDomainID - numeric
          36 , -- numFormID - numeric
          0 , -- bitAllowEdit - bit
          0 , -- bitInlineEdit - bit
          N'Default Expense Account' , -- vcFieldName - nvarchar(50)
          N'SelectBox' , -- vcAssociatedControlType - nvarchar(50)
          'DefaultExpenseAccountID' , -- vcPropertyName - varchar(100)
          NULL , -- PopupFunctionName - varchar(100)
          63 , -- order - tinyint
          NULL , -- tintRow - tinyint
          NULL , -- tintColumn - tinyint
          NULL , -- bitInResults - bit
          NULL , -- bitDeleted - bit
          NULL , -- bitDefault - bit
          NULL , -- bitSettingField - bit
          NULL , -- bitAddField - bit
          NULL , -- bitDetailField - bit
          NULL , -- bitAllowSorting - bit
          NULL , -- bitWorkFlowField - bit
          1 , -- bitImport - bit
          NULL , -- bitExport - bit
          NULL , -- bitAllowFiltering - bit
          NULL , -- bitRequired - bit
          NULL , -- numFormFieldID - numeric
          1 , -- intSectionID - int
          NULL  -- bitAllowGridColor - bit
        )

SELECT * FROM [dbo].[DycFormField_Mapping] AS DFFM WHERE [DFFM].[numFormID] = 36
ROLLBACK

------------------------------------------------