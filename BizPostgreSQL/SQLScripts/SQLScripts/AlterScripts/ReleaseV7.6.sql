/******************************************************************
Project: Release 7.6 Date: 03.JUNE.2017
Comments: ALTER SCRIPTS
*******************************************************************/

/************************** SANDEEP ************************/

-- CHECK FORM CONFIGURATION FOR ITEM DISPLAY AND SEARCH SETTING FOR ALL DOMAIN AND REMOVE DUPLICATE BY USERCNTID BECAUSE ITS A DOMAIN LEVEL SETTING AND SET USERCNTID TO 0
SELECT Distinct numDomainId,numFormId,numUserCntID,numRelCntType FROM DycFormConfigurationDetails WHERE numFormId=22 ORDER BY numDomainId



/************************** ANUSHA ************************/

Update DycFieldMaster set vcFieldName = 'Contact First Name' WHERE numFieldId  = 51
Update DycFieldMaster set vcFieldName = 'Contact Last Name' WHERE numFieldId  = 52

Update DynamicFormFieldMaster set vcFormFieldName = 'Contact First Name' where numFormFieldId in (SELECT numFormFieldID FROM DycFormField_Mapping WHERE numFieldID IN (51) and numFormID in  (1,10,56,36))
Update DynamicFormFieldMaster set vcFormFieldName = 'Contact Last Name' where numFormFieldId in (SELECT numFormFieldID FROM DycFormField_Mapping WHERE numFieldID IN (52) and numFormID in  (1,10,56,36))

UPDATE DynamicFormField_Validation SET vcNewFormFieldName = 'Contact First Name' WHERE numFormFieldId = 51 AND vcNewFormFieldName = 'First Name'
UPDATE DynamicFormField_Validation SET vcNewFormFieldName = 'Contact Last Name' WHERE numFormFieldId = 52 AND vcNewFormFieldName = 'Last Name'


UPDATE DycFormField_Mapping SET vcFieldName = 'Contact First Name' WHERE numFieldID = 51 AND vcFieldName = 'First Name'
UPDATE DycFormField_Mapping SET vcFieldName = 'Contact Last Name' WHERE numFieldID = 52 AND vcFieldName = 'Last Name'


/************************** PRASANT ************************/


ALTER TABLE eCommerceDTL ADD bitHideAddtoCart BIT DEFAULT 0

ALTER TABLE OpportunityBizDocs ADD dtExpectedDeliveryDate datetime


INSERT INTO PageElementMaster
	(
		 [numElementID],
		 [vcElementName],
		 [vcUserControlPath],
		 vcTagName,
		 bitCustomization,
		 bitAdd,
		 bitDelete
	) 
	VALUES
	(
		57,
		'ManufacturerList',
		'~/UserControls/ManufacturerList.ascx',
		'{#ManufacturerList#}',
		1,
		0,
		0
	)

	INSERT INTO PageElementAttributes
	(
		numElementID, 
		vcAttributeName, 
		vcControlType, 
		vcControlValues, 
		bitEditor
	)
	VALUES
	(
		57,
		'Html Customize',
		'HtmlEditor',
		NULL,
		1
	)

INSERT INTO PageElementMaster
	(
		 [numElementID],
		 [vcElementName],
		 [vcUserControlPath],
		 vcTagName,
		 bitCustomization,
		 bitAdd,
		 bitDelete
	) 
	VALUES
	(
		56,
		'Fulfilment',
		'~/UserControls/fullfilment.ascx',
		'{#fulfilment#}',
		1,
		0,
		0
	)

	INSERT INTO PageElementAttributes
	(
		numElementID, 
		vcAttributeName, 
		vcControlType, 
		vcControlValues, 
		bitEditor
	)
	VALUES
	(
		56,
		'Html Customize',
		'HtmlEditor',
		NULL,
		1
	)


INSERT INTO PageElementMaster
	(
		 [numElementID],
		 [vcElementName],
		 [vcUserControlPath],
		 vcTagName,
		 bitCustomization,
		 bitAdd,
		 bitDelete
	) 
	VALUES
	(
		55,
		'Support',
		'~/UserControls/Support.ascx',
		'{#Support#}',
		1,
		0,
		0
	)

	INSERT INTO PageElementAttributes
	(
		numElementID, 
		vcAttributeName, 
		vcControlType, 
		vcControlValues, 
		bitEditor
	)
	VALUES
	(
		55,
		'Html Customize',
		'HtmlEditor',
		NULL,
		1
	)

ALTER TABLE Domain ADD vcSupportTabs VARCHAR(300) DEFAULT 'Support'
ALTER TABLE Domain ADD bitSupportTabs BIT DEFAULT 0

UPDATE 
	Domain 
SET 
	vcSupportTabs='Support',
	bitSupportTabs=0

INSERT INTO PageElementMaster
	(
		 [numElementID],
		 [vcElementName],
		 [vcUserControlPath],
		 vcTagName,
		 bitCustomization,
		 bitAdd,
		 bitDelete
	) 
	VALUES
	(
		54,
		'ShippingStatus',
		'~/UserControls/ShippingStatus.ascx',
		'{#ShippingStatus#}',
		1,
		0,
		0
	)

	INSERT INTO PageElementAttributes
	(
		numElementID, 
		vcAttributeName, 
		vcControlType, 
		vcControlValues, 
		bitEditor
	)
	VALUES
	(
		54,
		'Html Customize',
		'HtmlEditor',
		NULL,
		1
	)