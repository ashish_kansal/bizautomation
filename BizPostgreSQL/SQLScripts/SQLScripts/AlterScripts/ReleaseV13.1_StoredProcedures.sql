/******************************************************************
Project: Release 13.1 Date: 17.FEB.2020
Comments: STORE PROCEDURES
*******************************************************************/

/****** Object:  UserDefinedFunction [dbo].[fn_GetContactsAOINamesInCSV]    Script Date: 07/26/2008 18:12:32 ******/

GO
SET QUOTED_IDENTIFIER OFF
GO
--Created By: Debasish Nag    
--Created On: 1th Sept 2005    
--This function returns the comma separated list of AOI Names is used in Adv Search
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_getcontactsaoinamesincsv')
DROP FUNCTION fn_getcontactsaoinamesincsv
GO
CREATE FUNCTION [dbo].[fn_GetContactsAOINamesInCSV](@numContactID numeric)
	RETURNS varchar(250) 
AS
BEGIN
	DECLARE @AOINames varchar(250)
	SELECT 
		@AOINames = COALESCE(@AOINames + ',', '') + AOIM.vcAOIName
	FROM 
		AOIContactLink AOIC
	INNER JOIN
		AOIMaster AOIM
	ON
		AOIC.numCOntactID=@numContactID
		AND AOIC.numAOIID = AOIM.numAOIId
	WHERE 
		AOIC.tintSelected = 1
	RETURN @AOINames
END
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_GetFollowUpDetails')
DROP FUNCTION fn_GetFollowUpDetails
GO
-- =============================================
-- Author:		<Priya>
-- Create date: <15 Jan 2018>
-- Description:	<Fetch Last and Next Follow ups>
-- =============================================
CREATE FUNCTION [dbo].[fn_GetFollowUpDetails](@numContactID NUMERIC, @numFollowup INT, @numDomainID NUMERIC) 

RETURNS VARCHAR(MAX)
AS
BEGIN

DECLARE @RetFollowup Varchar(MAX)
SET @RetFollowup=''

 IF(@numFollowup = 1)
  BEGIN
	SELECT @RetFollowup =
	CASE 
		WHEN (SELECT COUNT(*) FROM ConECampaignDTL CEDInner WHERE CEDInner.numConECampID=ConECampaign.numConEmailCampID ANd ISNULL(bitSend,0)=1) > 0 THEN
			(SELECT (CASE 
					 WHEN GenericDocuments.numGenericDocID IS NOT NULL
					 THEN CONCAT(GenericDocuments.vcDocName + ' ',
								 ISNULL(dbo.FormatedDateFromDate(ConECampaignDTL.bintSentON,@numDomainID),''),
								 ' <img alt="Emails Read" height="16px" width="16px" title="EmailsRead" src="../images/email_read.png"> (',
								 (SELECT COUNT(*) FROM ConECampaignDTL WHERE ISNULL(tintDeliveryStatus,0) = 1 AND ISNULL(bitEmailRead,0) = 1) ,')' )
					 WHEN tblActionItemData.RowID IS NOT NULL 
					 THEN CONCAT(tblActionItemData.Activity + ' ',
								ISNULL(dbo.FormatedDateFromDate(ConECampaignDTL.bintSentON,@numDomainID),''),
								' <img alt="Action Items" height="16px" width="16px" title="Action Items" src="../images/MasterList-16.gif">')
					ELSE ''
					END)
			FROM
				ConECampaignDTL
			LEFT JOIN
				ECampaignDTLs
			ON
				ConECampaignDTL.numECampDTLID = ECampaignDTLs.numECampDTLId
			LEFT JOIN
				GenericDocuments 
			ON
				ECampaignDTLs.numEmailTemplate = GenericDocuments.numGenericDocID
			LEFT JOIN
				tblActionItemData
			ON
				ECampaignDTLs.numActionItemTemplate = tblActionItemData.RowID
			WHERE
				ConECampaignDTL.numConECampDTLID = (SELECT MAX(ConECampaignDTL.numConECampDTLID) FROM ConECampaignDTL WHERE ConECampaignDTL.numConECampID=ConECampaign.numConEmailCampID AND ISNULL(bitSend,0)=1)
				)

		ELSE ''
	END 
	FROM
		ConECampaign
	WHERE
		ConECampaign.numContactId=@numContactID
		AND ISNULL(bitEngaged,0)=1
	--SET @RetFollowup= vcLastFollowup
END

ELSE IF (@numFollowup = 2)
BEGIN
	SELECT @RetFollowup = 
		CASE 
			WHEN (SELECT COUNT(*) FROM ConECampaignDTL CEDInner WHERE CEDInner.numConECampID=ConECampaign.numConEmailCampID ANd ISNULL(bitSend,0)=0) > 0 THEN
				(SELECT (CASE 
						WHEN GenericDocuments.numGenericDocID IS NOT NULL THEN CONCAT(GenericDocuments.vcDocName, +' '+ ISNULL(dbo.FormatedDateFromDate(ConECampaignDTL.dtExecutionDate,@numDomainID),''))
						WHEN tblActionItemData.RowID IS NOT NULL THEN CONCAT(tblActionItemData.Activity,  +' '+ ISNULL(dbo.FormatedDateFromDate(ConECampaignDTL.dtExecutionDate,@numDomainID),''))
						ELSE ''
						END)
				FROM
					ConECampaignDTL
				LEFT JOIN
					ECampaignDTLs
				ON
					ConECampaignDTL.numECampDTLID = ECampaignDTLs.numECampDTLId
				LEFT JOIN
					GenericDocuments 
				ON
					ECampaignDTLs.numEmailTemplate = GenericDocuments.numGenericDocID
				LEFT JOIN
					tblActionItemData
				ON
					ECampaignDTLs.numActionItemTemplate = tblActionItemData.RowID
				WHERE
					ConECampaignDTL.numConECampDTLID = (SELECT MIN(ConECampaignDTL.numConECampDTLID) FROM ConECampaignDTL WHERE ConECampaignDTL.numConECampID=ConECampaign.numConEmailCampID AND ISNULL(bitSend,0)=0)
					)

			ELSE ''
		END 
	FROM
		ConECampaign
	WHERE
		ConECampaign.numContactId=@numContactID
		AND ISNULL(bitEngaged,0)=1
--SET @RetFollowup = vcNextFollowup
END

ELSE IF(@numFollowup = 3)
  BEGIN
	SELECT @RetFollowup =
	CASE 
		WHEN (SELECT COUNT(*) FROM ConECampaignDTL CEDInner WHERE CEDInner.numConECampID=ConECampaign.numConEmailCampID ANd ISNULL(bitSend,0)=1) > 0 THEN
			(SELECT (CASE 
					 WHEN GenericDocuments.numGenericDocID IS NOT NULL THEN
											(SELECT vcEmailLog FROM ConECampaignDTL WHERE ISNULL(tintDeliveryStatus,0) = 1 AND ISNULL(bitEmailRead,0) = 1)
				ELSE ''
				END)
					
			FROM
				ConECampaignDTL
			LEFT JOIN
				ECampaignDTLs
			ON
				ConECampaignDTL.numECampDTLID = ECampaignDTLs.numECampDTLId
			LEFT JOIN
				GenericDocuments 
			ON
				ECampaignDTLs.numEmailTemplate = GenericDocuments.numGenericDocID
			LEFT JOIN
				tblActionItemData
			ON
				ECampaignDTLs.numActionItemTemplate = tblActionItemData.RowID
			WHERE
				ConECampaignDTL.numConECampDTLID = (SELECT MAX(ConECampaignDTL.numConECampDTLID) FROM ConECampaignDTL WHERE ConECampaignDTL.numConECampID=ConECampaign.numConEmailCampID AND ISNULL(bitSend,0)=1)
				
			)
		ELSE ''
	END 
	FROM
		ConECampaign
	WHERE
		ConECampaign.numContactId=@numContactID
		AND ISNULL(bitEngaged,0)=1   --- LastEmailStatus
END

RETURN @RetFollowup
END

GO


GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='tf'AND NAME ='fn_GetPayrollEmployeeExpense')
DROP FUNCTION fn_GetPayrollEmployeeExpense
GO
CREATE FUNCTION [dbo].[fn_GetPayrollEmployeeExpense] 
(
    @numDomainID NUMERIC(18,0)
	,@numUserCntID INT
	,@dtFromDate DATE
	,@dtToDate DATE
	,@ClientTimeZoneOffset INT
)
RETURNS @ItemPrice TABLE
(	
	numRecordID NUMERIC(18,0)
	,tintRecordType TINYINT
	,vcSource VARCHAR(200)
	,dtDate DATE
	,vcDate VARCHAR(50)
	,numType TINYINT
	,vcType VARCHAR(50)
	,monExpense DECIMAL(20,5)
	,vcDetails VARCHAR(MAX)
	,numApprovalComplete INT
)
AS 
BEGIN

	-- Time & Expense
	INSERT INTO @ItemPrice
	(
		numRecordID
		,tintRecordType
		,vcSource
		,dtDate
		,vcDate
		,numType
		,vcType
		,monExpense
		,vcDetails
		,numApprovalComplete
	)
	SELECT
		TimeAndExpense.numCategoryHDRID
		,1
		,'Expense' AS vcSource
		,dtFromDate
		,dbo.FormatedDateFromDate(DATEADD(MINUTE,@ClientTimeZoneOffset * -1,dtFromDate),@numDomainID) AS vcDate
		,numType
		,(CASE 
			WHEN TimeAndExpense.numType = 1 THEN 'Billable' 
			WHEN TimeAndExpense.numType = 2 THEN 'Reimbursable' 
			WHEN TimeAndExpense.numType = 6 Then 'Billable + Reimbursable' 
			ELSE '' 
		END) AS vcType
		,ISNULL(monAmount,0)
		,ISNULL(TimeAndExpense.txtDesc,'') vcDetails
		,ISNULL(numApprovalComplete,0) numApprovalComplete
	FROM
		TimeAndExpense 
	LEFT JOIN
		OpportunityMaster
	ON
		TimeAndExpense.numOppID=OpportunityMaster.numOppID
	WHERE
		TimeAndExpense.numDomainID = @numDomainID
		AND TimeAndExpense.numUserCntID=@numUserCntID
		AND ISNULL(numCategory,0) = 2
		AND CAST(DATEADD(MINUTE,@ClientTimeZoneOffset * -1,dtFromDate) AS Date) BETWEEN @dtFromDate AND @dtToDate
	ORDER BY
		dtFromDate

	
	RETURN 
END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='tf'AND NAME ='fn_GetPayrollEmployeeTime')
DROP FUNCTION fn_GetPayrollEmployeeTime
GO
CREATE FUNCTION [dbo].[fn_GetPayrollEmployeeTime] 
(
    @numDomainID NUMERIC(18,0)
	,@numUserCntID INT
	,@dtFromDate DATE
	,@dtToDate DATE
	,@ClientTimeZoneOffset INT
)
RETURNS @EmployeeTime TABLE
(	
	numRecordID NUMERIC(18,0)
	,tintRecordType TINYINT
	,vcSource VARCHAR(200)
	,dtDate DATE
	,vcDate VARCHAR(50)
	,numType TINYINT
	,vcType VARCHAR(500)
	,vcHours VARCHAR(20)
	,numMinutes INT
	,vcDetails VARCHAR(MAX)
	,numApprovalComplete INT
)
AS 
BEGIN

	-- Time & Expense
	INSERT INTO @EmployeeTime
	(
		numRecordID
		,tintRecordType
		,vcSource
		,dtDate
		,vcDate
		,numType
		,vcType
		,vcHours
		,numMinutes
		,vcDetails
		,numApprovalComplete
	)
	SELECT
		TimeAndExpense.numCategoryHDRID
		,1
		,'Time & Expense' AS vcSource
		,dtFromDate
		,dbo.FormatedDateFromDate(DATEADD(MINUTE,@ClientTimeZoneOffset * -1,dtFromDate),@numDomainID) AS vcDate
		,TimeAndExpense.numType
		,(CASE 
			WHEN TimeAndExpense.numType = 4 THEN 'Absence (Un-Paid)' 
			WHEN TimeAndExpense.numType = 3 THEN 'Absence (Paid)' 
			WHEN TimeAndExpense.numType = 2 Then 'Non-Billable Time' 
			WHEN TimeAndExpense.numType = 1 Then 'Billable' 
			ELSE '' 
		END) AS vcType
		,CONVERT(varchar(5),DATEADD(MINUTE,DATEDIFF(MINUTE,TimeAndExpense.dtFromDate,TimeAndExpense.dtToDate),0),108) vcHours
		,DATEDIFF(MINUTE,TimeAndExpense.dtFromDate,TimeAndExpense.dtToDate) numMinutes
		,ISNULL(TimeAndExpense.txtDesc,'') vcDetails
		,ISNULL(numApprovalComplete,0) numApprovalComplete
	FROM
		TimeAndExpense 
	LEFT JOIN
		OpportunityMaster
	ON
		TimeAndExpense.numOppID=OpportunityMaster.numOppID
	WHERE
		TimeAndExpense.numDomainID = @numDomainID
		AND TimeAndExpense.numUserCntID=@numUserCntID
		AND ISNULL(numCategory,0) = 1
		AND numType NOT IN (4) -- DO NOT INCLUDE UNPAID LEAVE
		AND CAST(DATEADD(MINUTE,@ClientTimeZoneOffset * -1,dtFromDate) AS Date) BETWEEN @dtFromDate AND @dtToDate
	ORDER BY
		dtFromDate

	-- Action Item
	INSERT INTO @EmployeeTime
	(
		numRecordID
		,tintRecordType
		,vcSource
		,dtDate
		,vcDate
		,numType
		,vcType
		,vcHours
		,numMinutes
		,vcDetails
		,numApprovalComplete
	)
	SELECT
		Communication.numCommId
		,2
		,CONCAT('<a target="_blank" href="../admin/actionitemdetails.aspx?CommId=',Communication.numCommId,'&lngType=0">'
				,dbo.GetListIemName(bitTask),'</a>',(CASE WHEN DM.numDivisionID > 0 THEN CONCAT(' (<a target="_blank" href="',(CASE WHEN DM.tintCRMType = 2 THEN '../account/frmAccounts.aspx?DivID=' WHEN DM.tintCRMType = 1 THEN '../prospects/frmProspects.aspx?DivID=' ELSE '../Leads/frmLeads.aspx?DivID=' END),Dm.numDivisionID,'">',ISNULL(CI.vcCompanyName,'-'),'</a>)') ELSE '' END)
		) AS vcSource
		,dtStartTime
		,dbo.FormatedDateFromDate(DATEADD(MINUTE,@ClientTimeZoneOffset * -1,Communication.dtStartTime),@numDomainID) AS vcDate
		,0
		,dbo.GetListIemName(bitTask) AS vcType
		,CONVERT(varchar(5),DATEADD(MINUTE,DATEDIFF(MINUTE,Communication.dtStartTime,Communication.dtEndTime),0),108) vcHours
		,DATEDIFF(MINUTE,Communication.dtStartTime,Communication.dtEndTime) numMinutes
		,ISNULL(Communication.textDetails,'') vcDetails
		,0
	FROM
		Communication 
	LEFT JOIN
		DivisionMaster DM
	ON
		Communication.numDivisionId = DM.numDivisionID
	LEFT JOIN
		CompanyInfo CI
	ON
		DM.numCompanyID = CI.numCompanyId
	WHERE
		Communication.numDomainID = @numDomainID
		AND Communication.numAssign=@numUserCntID
		AND CAST(DATEADD(MINUTE,@ClientTimeZoneOffset * -1,Communication.dtStartTime) AS Date) BETWEEN @dtFromDate AND @dtToDate
		AND ISNULL(bitClosedFlag,0) = 1
	ORDER BY
		dtStartTime

	-- Calendar
	INSERT INTO @EmployeeTime
	(
		numRecordID
		,tintRecordType
		,vcSource
		,dtDate
		,vcDate
		,numType
		,vcType
		,vcHours
		,numMinutes
		,vcDetails
		,numApprovalComplete
	)
	SELECT
		Activity.ActivityID
		,3
		,CONCAT('<a target="_blank" href="../admin/actionitemdetails.aspx?CommId=',Activity.ActivityID,'&lngType=1">','Action Item','</a>') AS vcSource
		,Activity.StartDateTimeUtc
		,dbo.FormatedDateFromDate(DATEADD(MINUTE,@ClientTimeZoneOffset * -1,Activity.StartDateTimeUtc),@numDomainID) AS vcDate
		,0
		,'Calendar' AS vcType
		,CONVERT(varchar(5),DATEADD(MINUTE,DATEDIFF(MINUTE,Activity.StartDateTimeUtc,DATEADD(SECOND,Activity.Duration,Activity.StartDateTimeUtc)),0),108) vcHours
		,DATEDIFF(MINUTE,Activity.StartDateTimeUtc,DATEADD(SECOND,Activity.Duration,Activity.StartDateTimeUtc)) numMinutes
		,ISNULL(Activity.ActivityDescription,'') vcDetails
		,0
	FROM
		[Resource]
	INNER JOIN
		ActivityResource 
	ON
		[Resource].ResourceID = ActivityResource.ResourceID
	INNER JOIN
		Activity 
	ON
		ActivityResource.ActivityID=Activity.ActivityID
	WHERE
		[Resource].numUserCntId = @numUserCntID
		AND CAST(DATEADD(MINUTE,@ClientTimeZoneOffset * -1, Activity.StartDateTimeUtc) AS Date) BETWEEN @dtFromDate AND @dtToDate
	ORDER BY
		Activity.StartDateTimeUtc

	-- Work Order Tasks
	INSERT INTO @EmployeeTime
	(
		numRecordID
		,tintRecordType
		,vcSource
		,dtDate
		,vcDate
		,numType
		,vcType
		,vcHours
		,numMinutes
		,vcDetails
		,numApprovalComplete
	)
	SELECT 
		WO.numWOId
		,4
		,CONCAT('<a target="_blank" href="../Items/frmWorkOrder.aspx?WOID=',WO.numWOId,'">',ISNULL(WO.vcWorkOrderName,'-'),'</a>'
				,(CASE WHEN OM.numOppID > 0 THEN CONCAT(' (<a target="_blank" href="../opportunity/frmOpportunities.aspx?opId=',OM.numOppId,'">',ISNULL(OM.vcPOppName,'-'),'</a>)') ELSE '' END)
				,(CASE WHEN DM.numDivisionID > 0 THEN CONCAT(' (<a target="_blank" href="',(CASE WHEN DM.tintCRMType = 2 THEN '../account/frmAccounts.aspx?DivID=' WHEN DM.tintCRMType = 1 THEN '../prospects/frmProspects.aspx?DivID=' ELSE '../Leads/frmLeads.aspx?DivID=' END),Dm.numDivisionID,'">',ISNULL(CI.vcCompanyName,'-'),'</a>)') ELSE '' END)
			) vcWorkOrderName
		,SPDTTLStart.dtActionTime
		,dbo.FormatedDateFromDate(DATEADD(MINUTE,@ClientTimeZoneOffset * -1,SPDTTLStart.dtActionTime),@numDomainID) AS vcDate
		,0
		,'Work Order' AS vcType
		,CONCAT(FORMAT(ISNULL(DATEDIFF(MINUTE,SPDTTLStart.dtActionTime,TEMP.dtActionTime),0) / 60,'00'),':',FORMAT(ISNULL(DATEDIFF(MINUTE,SPDTTLStart.dtActionTime,TEMP.dtActionTime),0) % 60,'')) vcHours
		,ISNULL(DATEDIFF(MINUTE,SPDTTLStart.dtActionTime,TEMP.dtActionTime),0) numMinutes
		,ISNULL(SPDT.vcTaskName,'') vcDetails
		,0
	FROM 
		StagePercentageDetailsTask SPDT
	INNER JOIN
		WorkOrder WO
	ON
		SPDT.numWorkOrderId = WO.numWOId
	LEFT JOIN
		OpportunityMaster OM
	ON
		WO.numOppId = OM.numOppId
	LEFT JOIN
		DivisionMaster DM
	ON
		OM.numDivisionId = DM.numDivisionID
	LEFT JOIN
		CompanyInfo CI
	ON
		DM.numCompanyID = CI.numCompanyId
	INNER JOIN
		StagePercentageDetailsTaskTimeLog  SPDTTLStart
	ON
		SPDT.numTaskId = SPDTTLStart.numTaskID
	CROSS APPLY
	(
		SELECT
			TEMP.dtActionTime
		FROM
		(
			SELECT TOP 1
				SPDTTLEnd.dtActionTime
			FROM
				StagePercentageDetailsTaskTimeLog  SPDTTLEnd
			WHERE
				SPDTTLEnd.numDomainID = @numDomainID
				AND SPDTTLEnd.numUserCntID = @numUserCntID
				AND SPDTTLStart.numTaskID = SPDTTLEnd.numTaskID
				AND SPDTTLEnd.tintAction IN (2,4)
				AND SPDTTLEnd.dtActionTime >= SPDTTLStart.dtActionTime
			ORDER BY	
				dtActionTime
		) TEMP
		WHERE
			DATEDIFF(HOUR,SPDTTLStart.dtActionTime,TEMP.dtActionTime) < 24
	) TEMP
	WHERE 
		SPDT.numDomainID = @numDomainID
		AND ISNULL(SPDT.numWorkOrderId,0) > 0
		AND SPDTTLStart.numDomainID = @numDomainID 
		AND SPDTTLStart.numUserCntID=@numUserCntID 
		AND SPDTTLStart.tintAction IN (1,3) 
		AND CAST(DATEADD(MINUTE,@ClientTimeZoneOffset * -1,SPDTTLStart.dtActionTime) AS Date) BETWEEN @dtFromDate AND @dtToDate

	RETURN 
END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='GetCapacityLoad')
DROP FUNCTION GetCapacityLoad
GO
CREATE FUNCTION [dbo].[GetCapacityLoad]
(
	@numDomainID NUMERIC(18,0)
	,@numUsercntID NUMERIC(18,0)
	,@numTeamID NUMERIC(18,0)
	,@numWorkOrderID NUMERIC(18,0)
	,@numTaskID NUMERIC(18,0)
	,@tintDateRange TINYINT -- 1:Today, 2:Week, 3:Month, 4:Upto Workorder projected finish
	,@dtFromDate DATETIME
	,@ClientTimeZoneOffset INT
)    
RETURNS INT   
AS
BEGIN
	SET @dtFromDate = DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,@dtFromDate)

	DECLARE @numDivisionID NUMERIC(18,0)
	SELECT @numDivisionID=numDivisionID FROM Domain WHERE numDomainId=@numDomainID
	
	DECLARE @numQtyToBuild FLOAT
	DECLARE @dtPlannedStartDate DATETIME
	DECLARE @dtActualStartDate DATETIME = NULL

	SELECT 
		@numQtyToBuild=numQtyItemsReq
		,@dtPlannedStartDate=dtmStartDate 
	FROM 
		WorkOrder WHERE numWOId=@numWorkOrderID


	IF EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID IN (SELECT numTaskId FROM StagePercentageDetailsTask WHERE numWorkOrderId=@numWorkOrderID) AND tintAction=1)
	BEGIN
		SELECT @dtActualStartDate = MIN(dtActionTime) FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID IN (SELECT numTaskId FROM StagePercentageDetailsTask WHERE numWorkOrderId=@numWorkOrderID) AND tintAction=1
	END

	DECLARE @TEMPTaskTimeSequence TABLE
	(
		numAssignedTo NUMERIC(18,0)
		,dtDate DATE
		,numTaskHours NUMERIC(18,0)
	)

	DECLARE @TempTaskAssignee TABLE
	(
		numAssignedTo NUMERIC(18,0)
		,dtLastTaskCompletionTime DATETIME
	)

	DECLARE @TempTasks TABLE
	(
		ID INT IDENTITY(1,1)
		,numTaskID NUMERIC(18,0)
		,numTaskTimeInMinutes NUMERIC(18,0)
		,numTaskAssignee NUMERIC(18,0)
		,intTaskType INT --1:Parallel, 2:Sequential
		,dtPlannedStartDate DATETIME
	)

	INSERT INTO @TempTasks
	(
		numTaskID
		,numTaskTimeInMinutes
		,intTaskType
		,numTaskAssignee
	)
	SELECT 
		SPDT.numTaskID
		,((ISNULL(SPDT.numHours,0) * 60) + ISNULL(SPDT.numMinutes,0)) * (@numQtyToBuild - ISNULL((SELECT SUM(numProcessedQty) FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID = SPDT.numTaskId),0))
		,ISNULL(SPDT.intTaskType,1)
		,SPDT.numAssignTo
	FROM 
		StagePercentageDetailsTask SPDT
	INNER JOIN
		WorkOrder WO
	ON
		SPDT.numWorkOrderId = WO.numWOId
	WHERE 
		SPDT.numDomainId=@numDomainID
		AND SPDT.numWorkOrderId = @numWorkOrderID
		AND NOT EXISTS (SELECT * FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID = SPDT.numTaskId AND tintAction=4) 

	UPDATE @TempTasks SET dtPlannedStartDate=@dtPlannedStartDate

	INSERT INTO @TempTaskAssignee
	(
		numAssignedTo
	)
	SELECT DISTINCT
		numTaskAssignee
	FROM
		@TempTasks

	DECLARE @i INT = 1
	DECLARE @iCount INT 	
	SELECT @iCount = COUNT(*) FROM @TempTasks

	DECLARE @numTaskAssignee NUMERIC(18,0)
	DECLARE @intTaskType INT
	DECLARE @numWorkScheduleID NUMERIC(18,0)
	DECLARE @numTempUserCntID NUMERIC(18,0)
	DECLARE @dtStartDate DATETIME
	DECLARE @numTotalTaskInMinutes NUMERIC(18,0)
	DECLARE @numProductiveTimeInMinutes NUMERIC(18,0)
	DECLARE @tmStartOfDay TIME(7)
	DECLARE @numTimeLeftForDay NUMERIC(18,0)
	DECLARE @vcWorkDays VARCHAR(20)
	DECLARE @bitParallelStartSet BIT = 0

	WHILE @i <= @iCount
	BEGIN
		SELECT
			@numTaskAssignee=numTaskAssignee
			,@numTotalTaskInMinutes=numTaskTimeInMinutes
			,@intTaskType = intTaskType
		FROM
			@TempTasks 
		WHERE
			ID=@i

		-- GET TASK COMPLETION TIME WHICH WILL BE START DATETIME FOR NEXT TASK
		SELECT
			@numWorkScheduleID = WS.ID
			,@numProductiveTimeInMinutes=(ISNULL(WS.numProductiveHours,0) * 60) + ISNULL(WS.numProductiveMinutes,0)
			,@tmStartOfDay = tmStartOfDay 
			,@vcWorkDays=vcWorkDays
			,@dtStartDate = (CASE WHEN @dtActualStartDate IS NULL THEN DATEADD(MINUTE,@ClientTimeZoneOffset,CONVERT(DATETIME,CONVERT(VARCHAR(8),DATEADD(MINUTE,-@ClientTimeZoneOffset,@dtPlannedStartDate),112)) + tmStartOfDay) ELSE @dtActualStartDate END)
		FROM
			WorkSchedule WS
		INNER JOIN
			UserMaster
		ON
			WS.numUserCntID = UserMaster.numUserDetailId
		WHERE 
			WS.numUserCntID = @numTaskAssignee

		IF @intTaskType=1 AND EXISTS (SELECT numAssignedTo FROM @TempTaskAssignee WHERE numAssignedTo = @numTaskAssignee AND dtLastTaskCompletionTime IS NOT NULL)
		BEGIN
			SELECT @dtStartDate = dtLastTaskCompletionTime FROM @TempTaskAssignee WHERE numAssignedTo = @numTaskAssignee
		END
		ELSE IF EXISTS (SELECT numAssignedTo FROM @TempTaskAssignee WHERE numAssignedTo = @numTaskAssignee AND dtLastTaskCompletionTime IS NOT NULL)
		BEGIN
			SELECT @dtStartDate = dtLastTaskCompletionTime FROM @TempTaskAssignee WHERE numAssignedTo = @numTaskAssignee
		END

		UPDATE @TempTasks SET dtPlannedStartDate=@dtStartDate WHERE ID=@i 

		IF @numProductiveTimeInMinutes > 0 AND @numTotalTaskInMinutes > 0 AND LEN(ISNULL(@vcWorkDays,'')) > 0
		BEGIN
			WHILE @numTotalTaskInMinutes > 0
			BEGIN
				-- IF ITS WORKING DAY AND ALSO NOT HOLIDAY THEN PROCEED OR INCRESE DATE TO NEXT DAY
				IF DATEPART(WEEKDAY,@dtStartDate) IN (SELECT Id FROM dbo.SplitIDs(@vcWorkDays,',')) AND NOT EXISTS (SELECT ID FROM WorkScheduleDaysOff WHERE numWorkScheduleID=@numWorkScheduleID AND CAST(DATEADD(MINUTE,@ClientTimeZoneOffset * -1,@dtStartDate) AS DATE) BETWEEN dtDayOffFrom AND dtDayOffTo)
				BEGIN
					IF CAST(@dtStartDate AS DATE) = CAST(GETUTCDATE() AS DATE)
					BEGIN
						SET @numTimeLeftForDay = DATEDIFF(MINUTE,@dtStartDate,DATEADD(MINUTE,@numProductiveTimeInMinutes,DATEADD(MINUTE,@ClientTimeZoneOffset,CONVERT(DATETIME,CONVERT(VARCHAR(8),DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()),112)) + @tmStartOfDay)))

						-- CHECK TIME LEFT FOR DAY BASED
						IF @numTimeLeftForDay >= @numProductiveTimeInMinutes
						BEGIN
							SET @numTimeLeftForDay = @numProductiveTimeInMinutes;
						END
					END
					ELSE
					BEGIN
						SET @numTimeLeftForDay = @numProductiveTimeInMinutes
					END

					IF @numTimeLeftForDay > 0
					BEGIN
						INSERT INTO @TEMPTaskTimeSequence
						(
							numAssignedTo
							,dtDate
							,numTaskHours
						)
						VALUES
						(
							@numTaskAssignee
							,@dtStartDate
							,(CASE WHEN @numTimeLeftForDay > @numTotalTaskInMinutes THEN @numTotalTaskInMinutes ELSE @numTimeLeftForDay END)
						)

						IF @numTimeLeftForDay > @numTotalTaskInMinutes
						BEGIN
							SET @dtStartDate = DATEADD(MINUTE,@numTotalTaskInMinutes,@dtStartDate)
						END
						ELSE
						BEGIN
							SET @dtStartDate = DATEADD(DAY,1,@dtStartDate)
						END

						SET @numTotalTaskInMinutes = @numTotalTaskInMinutes - @numTimeLeftForDay
					END
					ELSE
					BEGIN
						SET @dtStartDate = DATEADD(DAY,1,@dtStartDate)
					END
				END
				ELSE
				BEGIN
					SET @dtStartDate = DATEADD(DAY,1,@dtStartDate)					
				END				
			END
		END	

		UPDATE @TempTaskAssignee SET dtLastTaskCompletionTime=@dtStartDate WHERE numAssignedTo=@numTaskAssignee

		SET @i = @i + 1
	END


	DECLARE @TEMPEmployee TABLE
	(
		ID INT IDENTITY(1,1)
		,numContactID NUMERIC(18,0)
		,numProductiveMinutes NUMERIC(18,0)
		,tmStartOfDay TIME(7)
		,vcWorkDays VARCHAR(20)
		,numCapacityLoad NUMERIC(18,0)
	)

	INSERT INTO @TEMPEmployee
	(
		numContactID
		,numProductiveMinutes
		,tmStartOfDay
		,vcWorkDays
	)
	SELECT 
		AdditionalContactsInformation.numContactID
		,(ISNULL(WorkSchedule.numProductiveHours,0) * 60) + ISNULL(WorkSchedule.numProductiveMinutes,0)
		,WorkSchedule.tmStartOfDay
		,WorkSchedule.vcWorkDays
	FROM
		AdditionalContactsInformation
	LEFT JOIN
		WorkSchedule
	ON
		AdditionalContactsInformation.numContactId = WorkSchedule.numUserCntID
	WHERE
		AdditionalContactsInformation.numDomainID=@numDomainID
		AND AdditionalContactsInformation.numDivisionId = @numDivisionID
		AND 1 = (CASE 
					WHEN @tintDateRange = 4 
					THEN (CASE WHEN AdditionalContactsInformation.numContactId IN (SELECT numAssignedTo FROM @TempTaskAssignee) THEN 1 ELSE 0 END)
					ELSE (CASE 
							WHEN ((@numUsercntID > 0 AND AdditionalContactsInformation.numContactId=@numUsercntID) OR (@numTeamID > 0 AND AdditionalContactsInformation.numTeam=@numTeamID))
							THEN 1
							ELSE 0
						END)
				END)

	DECLARE @j INT 
	DECLARE @jCount INT
	DECLARE @numTimeAndExpenseMinues NUMERIC(18,0)
	DECLARE @numAssemblyBuildMinues NUMERIC(18,0)
	DECLARE @dtTaskStartTime DATETIME
	DECLARE @dtLastTaskCompletionTime DATETIME

	SET @j = 1
	SELECT @jCount = COUNT(*) FROM @TEMPEmployee
	DECLARE @numContactID NUMERIC(18,0)

	WHILE @j <= @jCount
	BEGIN
		SELECT 
			@numContactID=numContactID
			,@numProductiveTimeInMinutes = numProductiveMinutes
		FROM 
			@TEMPEmployee 
		WHERE 
			ID=@j

		SELECT 
			@dtLastTaskCompletionTime=dtLastTaskCompletionTime 
			,@dtTaskStartTime= (SELECT MIN(dtDate) FROM @TEMPTaskTimeSequence WHERE numAssignedTo = @numContactID)
		FROM 
			@TempTaskAssignee 
		WHERE 
			numAssignedTo = @numContactID

		SET @numTimeAndExpenseMinues = ISNULL((SELECT 
														SUM(DATEDIFF(MINUTE,TimeAndExpense.dtFromDate,TimeAndExpense.dtToDate))
													FROM 
														TimeAndExpense 
													WHERE 
														numDomainID=@numDomainID 
														AND numUserCntID = @numContactID
														AND numCategory=1 
														AND numType IN (1,2)
														AND 1 = (CASE 
																	WHEN @tintDateRange = 1 -- Day
																	THEN (CASE WHEN CAST(dtFromDate AS DATE) = CAST(@dtFromDate AS DATE) THEN 1 ELSE 0 END)
																	WHEN @tintDateRange = 2 -- Week
																	THEN (CASE WHEN CAST(dtFromDate AS DATE) BETWEEN CAST(@dtFromDate AS DATE) AND CAST(DATEADD(DAY,7,@dtFromDate) AS DATE) THEN 1 ELSE 0 END) --DATEADD(DAY,2 - DATEPART(WEEKDAY, GETDATE()), CAST(GETDATE() AS DATE))
																	WHEN @tintDateRange = 3 -- Month
																	THEN (CASE WHEN CAST(dtFromDate AS DATE) BETWEEN CAST(@dtFromDate AS DATE) AND CAST(DATEADD(MILLISECOND,-3,DATEADD(MONTH,1,@dtFromDate)) AS DATE) THEN 1 ELSE 0 END) --CAST(DATEADD(DAY, 1-DAY(GETUTCDATE()), GETUTCDATE()) AS DATE)
																	WHEN @tintDateRange = 4 -- Upto Workorder projected finish
																	THEN (CASE WHEN CAST(dtFromDate AS DATE) BETWEEN CAST(@dtFromDate AS DATE) AND CAST(@dtLastTaskCompletionTime AS DATE) THEN 1 ELSE 0 END)
																	ELSE 0
																END)),0)

		SET @numAssemblyBuildMinues = ISNULL((SELECT 
												SUM(numTaskHours)
											FROM
												@TEMPTaskTimeSequence 
											WHERE
												numAssignedTo = @numContactID
												AND 1 = (CASE 
															WHEN @tintDateRange = 1 -- Day
															THEN (CASE WHEN CAST(dtDate AS DATE) = CAST(@dtFromDate AS DATE) THEN 1 ELSE 0 END)
															WHEN @tintDateRange = 2 -- Week
															THEN (CASE WHEN CAST(dtDate AS DATE) BETWEEN CAST(@dtFromDate AS DATE) AND CAST(DATEADD(DAY,7,@dtFromDate) AS DATE) THEN 1 ELSE 0 END)
															WHEN @tintDateRange = 3 -- Month
															THEN (CASE WHEN CAST(dtDate AS DATE) BETWEEN CAST(@dtFromDate AS DATE) AND CAST(DATEADD(MILLISECOND,-3,DATEADD(MONTH,1,@dtFromDate)) AS DATE) THEN 1 ELSE 0 END)
															WHEN @tintDateRange = 4 -- Upto Workorder projected finish
															THEN 1
															ELSE 0
														END)),0)


		SET @numProductiveTimeInMinutes = @numProductiveTimeInMinutes * (CASE WHEN @tintDateRange=1 THEN 1 WHEN @tintDateRange=2 THEN 7 WHEN @tintDateRange=3 THEN 30 WHEN @tintDateRange = 4 THEN DATEDIFF(DAY,@dtTaskStartTime,@dtLastTaskCompletionTime) END) 
		
		UPDATE @TEMPEmployee SET numCapacityLoad = (CASE WHEN @numProductiveTimeInMinutes > 0 THEN (((@numAssemblyBuildMinues + @numTimeAndExpenseMinues) * 100) / @numProductiveTimeInMinutes) ELSE 0 END) WHERE ID = @j


		SET @j = @j + 1
	END

	RETURN ISNULL((SELECT AVG(numCapacityLoad) FROM @TEMPEmployee),0)
END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='GetCustFldValueOppItems')
DROP FUNCTION GetCustFldValueOppItems
GO
CREATE FUNCTION GetCustFldValueOppItems
(
 @numFldId NUMERIC(18,0),
 @numRecordId NUMERIC(18,0),
 @numItemCode NUMERIC(18,0)
)
RETURNS VARCHAR(MAX) 
AS
BEGIN
	DECLARE @vcValue AS VARCHAR(MAX) = ''
	DECLARE @fld_Type AS VARCHAR(50)
	DECLARE @numListID AS NUMERIC(18,0)

    SELECT 
		@numListID=numListID,
		@fld_Type=Fld_type 
	FROM 
		CFW_Fld_Master 
	WHERE 
		Fld_id=@numFldId
       
	IF EXISTS (SELECT Fld_Value FROM CFW_Fld_Values_OppItems WHERE Fld_ID = @numFldId  AND RecId = @numRecordId)
	BEGIN
		SELECT 
			@vcValue = ISNULL(Fld_Value,0) 
		FROM 
			CFW_Fld_Values_OppItems
		WHERE 
			Fld_ID = @numFldId 
			AND RecId = @numRecordId
	END
	ELSE
	BEGIN
		SELECT 
			@vcValue = ISNULL(Fld_Value,0) 
		FROM 
			CFW_Fld_Values_Item 
		WHERE 
			Fld_ID = @numFldId 
			AND RecId = @numItemCode
	END

	IF (@fld_Type='TextBox' OR @fld_Type='TextArea')
	BEGIN
		IF @vcValue='' SET @vcValue='-'
	END
	ELSE IF @fld_Type='SelectBox'
	BEGIN
		IF @vcValue='' 
			SET @vcValue='-'
		ELSE 
			SET @vcValue = dbo.GetListIemName(@vcValue)
	END 
	ELSE IF @fld_Type = 'CheckBox' 
	BEGIN
		SET @vcValue= (CASE WHEN @vcValue=0 THEN 'No' ELSE 'Yes' END)
	END
	ELSE IF @fld_type = 'CheckBoxList'
	BEGIN
		SELECT 
			@vcValue = STUFF((SELECT CONCAT(',', vcData) 
		FROM 
			ListDetails 
		WHERE 
			numlistitemid IN (SELECT ISNULL(Id,0) FROM dbo.SplitIDs(@vcValue,',')) FOR XML PATH('')), 1, 1, '')
	END
	
	IF @fld_Type = 'DateField'
	BEGIN
		IF @vcValue = '0' OR @vcValue=''
			SET @vcValue = NULL
	END
	ELSE
	BEGIN
		IF @vcValue IS NULL 
			SET @vcValue=''
	END

	RETURN @vcValue
END
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='tf'AND NAME ='GetEmployeeAvailabilityPercent')
DROP FUNCTION GetEmployeeAvailabilityPercent
GO
CREATE FUNCTION [dbo].[GetEmployeeAvailabilityPercent]
(
	@numDomainID NUMERIC(18,0)
	,@numUserCntID NUMERIC(18,0)
	,@dtFromDate DATETIME
	,@dtToDate DATETIME
	,@ClientTimeZoneOffset INT
)    
RETURNS @EmployeeAvailability TABLE
(	
	numTotalAvailableMinutes NUMERIC(18,0)
	,numAvailabilityPercent NUMERIC(18,0)
	,numProductiveMinutes NUMERIC(18,0)
	,numProductivityPercent NUMERIC(18,0)
)
AS
BEGIN
	DECLARE @numWorkScheduleID NUMERIC(18,0)
	DECLARE @numProductiveTimeInMinutes NUMERIC(18,0)
	DECLARE @vcWorkDays VARCHAR(20)
	
	SELECT 
		@numWorkScheduleID=WS.ID
		,@numProductiveTimeInMinutes=(ISNULL(WS.numProductiveHours,0) * 60) + ISNULL(WS.numProductiveMinutes,0)
		,@vcWorkDays=vcWorkDays
	FROM 
		UserMaster 
	INNER JOIN
		WorkSchedule WS
	ON
		WS.numUserCntID = UserMaster.numUserDetailId
	WHERE 
		UserMaster.numDomainID = @numDomainID 
		AND UserMaster.numUserDetailID=@numUserCntID

	DECLARE @numTotalProductiveMinutes NUMERIC(18,0) = 0
	DECLARE @numTotalAbsentMinutes NUMERIC(18,0) = 0
	DECLARE @numActualProductiveMinutes NUMERIC(18,0) = ISNULL(dbo.GetEmployeeProductiveMinutes(@numDomainID,@numUserCntID,@dtFromDate,@dtToDate,@ClientTimeZoneOffset),0)

	WHILE @dtFromDate <= @dtToDate
	BEGIN
		IF EXISTS (SELECT numCategoryHDRID FROM TimeAndExpense WHERE TimeAndExpense.numDomainID = @numDomainID AND TimeAndExpense.numUserCntID=@numUserCntID AND ISNULL(numCategory,0) = 1 AND numType = 4)
		BEGIN
			SET @numTotalAbsentMinutes = ISNULL(@numTotalAbsentMinutes,0) + @numProductiveTimeInMinutes
		END 
		ELSE IF DATEPART(WEEKDAY,@dtFromDate) IN (SELECT Id FROM dbo.SplitIDs(@vcWorkDays,',')) AND NOT EXISTS (SELECT ID FROM WorkScheduleDaysOff WHERE numWorkScheduleID=@numWorkScheduleID AND DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,@dtFromDate) BETWEEN dtDayOffFrom AND dtDayOffTo)
		BEGIN
			SET @numTotalProductiveMinutes = ISNULL(@numTotalProductiveMinutes,0) + @numProductiveTimeInMinutes
		END

		 SET @dtFromDate = DATEADD(DAY,1,@dtFromDate)
	END

	INSERT INTO @EmployeeAvailability
	(
		numTotalAvailableMinutes
		,numAvailabilityPercent
		,numProductiveMinutes
		,numProductivityPercent
	)
	VALUES
	(
		@numTotalProductiveMinutes - @numTotalAbsentMinutes
		,(CASE WHEN ISNULL(@numTotalProductiveMinutes,0) > 0 THEN (((@numTotalProductiveMinutes - @numTotalAbsentMinutes) * 100) / @numTotalProductiveMinutes) ELSE 0 END)
		,@numActualProductiveMinutes
		,(CASE WHEN ISNULL(@numTotalProductiveMinutes,0) > 0 THEN (@numActualProductiveMinutes * 100) / @numTotalProductiveMinutes ELSE 0 END)
	)

	RETURN 
END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='GetEmployeeProductiveMinutes')
DROP FUNCTION GetEmployeeProductiveMinutes
GO
CREATE FUNCTION [dbo].[GetEmployeeProductiveMinutes]
(
	@numDomainID NUMERIC(18,0)
	,@numUserCntID NUMERIC(18,0)
	,@dtFromDate DATETIME
	,@dtToDate DATETIME
	,@ClientTimeZoneOffset INT
)    
RETURNS INT  
AS
BEGIN
	DECLARE @numProductiveMinutes NUMERIC(18,0) = 0

	SELECT
		@numProductiveMinutes = SUM(DATEDIFF(MINUTE,TimeAndExpense.dtFromDate,TimeAndExpense.dtToDate))
	FROM
		TimeAndExpense 
	LEFT JOIN
		OpportunityMaster
	ON
		TimeAndExpense.numOppID=OpportunityMaster.numOppID
	WHERE
		TimeAndExpense.numDomainID = @numDomainID
		AND TimeAndExpense.numUserCntID=@numUserCntID
		AND ISNULL(TimeAndExpense.numCategory,0) = 1
		AND numType NOT IN (4) -- DO NOT INCLUDE UNPAID LEAVE
		AND dtFromDate BETWEEN @dtFromDate AND @dtToDate

	SELECT
		@numProductiveMinutes = ISNULL(@numProductiveMinutes,0) + SUM(DATEDIFF(MINUTE,Communication.dtStartTime,Communication.dtEndTime))
	FROM
		Communication 
	LEFT JOIN
		DivisionMaster DM
	ON
		Communication.numDivisionId = DM.numDivisionID
	LEFT JOIN
		CompanyInfo CI
	ON
		DM.numCompanyID = CI.numCompanyId
	WHERE
		Communication.numDomainID = @numDomainID
		AND Communication.numAssign=@numUserCntID
		AND Communication.dtStartTime BETWEEN @dtFromDate AND @dtToDate
		AND ISNULL(bitClosedFlag,0) = 1

	SELECT
		@numProductiveMinutes = ISNULL(@numProductiveMinutes,0) + SUM(DATEDIFF(MINUTE,Activity.StartDateTimeUtc,DATEADD(SECOND,Activity.Duration,Activity.StartDateTimeUtc)))
	FROM
		[Resource]
	INNER JOIN
		ActivityResource 
	ON
		[Resource].ResourceID = ActivityResource.ResourceID
	INNER JOIN
		Activity 
	ON
		ActivityResource.ActivityID=Activity.ActivityID
	WHERE
		[Resource].numUserCntId = @numUserCntID
		AND Activity.StartDateTimeUtc BETWEEN @dtFromDate AND @dtToDate

	SELECT 
		@numProductiveMinutes = ISNULL(@numProductiveMinutes,0) + dbo.GetTimeSpendOnTaskInMinutesDateRange(@numDomainID,@numUserCntID,SPDTTL.numTaskID,@dtFromDate,@dtToDate)
	FROM
		StagePercentageDetailsTaskTimeLog SPDTTL
	WHERE
		SPDTTL.numUserCntID = @numUserCntID
		AND SPDTTL.dtActionTime BETWEEN @dtFromDate AND @dtToDate
	GROUP BY
		SPDTTL.numTaskID

	RETURN @numProductiveMinutes
END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='GetProjectedFinish')
DROP FUNCTION GetProjectedFinish
GO
CREATE FUNCTION [dbo].[GetProjectedFinish]
(
	@numDomainID NUMERIC(18,0)
	,@numWOID NUMERIC(18,0)
	,@ClientTimeZoneOffset INT
)    
RETURNS DATETIME    
AS
BEGIN
	DECLARE @dtProjectedFinishDate DATETIME
	
	DECLARE @numQtyToBuild FLOAT
	DECLARE @dtPlannedStartDate DATETIME
	SELECT 
		@numQtyToBuild=numQtyItemsReq
		,@dtPlannedStartDate=ISNULL(WorkOrder.dtmStartDate,WorkOrder.bintCreatedDate) 
	FROM 
		WorkOrder 
	WHERE
		WorkOrder.numDomainID=@numDomainID 
		AND WorkOrder.numWOId=@numWOID

	IF EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID IN (SELECT SPDT.numTaskID FROM StagePercentageDetailsTask SPDT WHERE SPDT.numWorkOrderId=@numWOID) AND tintAction=1)
	BEGIN
		SELECT @dtPlannedStartDate = MIN(SPDTTL.dtActionTime) FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID IN (SELECT SPDT.numTaskID FROM StagePercentageDetailsTask SPDT WHERE SPDT.numWorkOrderId=@numWOID) AND tintAction=1
	END

	SET @dtProjectedFinishDate = @dtPlannedStartDate

	

	DECLARE @TempTaskAssignee TABLE
	(
		numAssignedTo NUMERIC(18,0)
		,dtLastTaskCompletionTime DATETIME
	)

	DECLARE @TempTasks TABLE
	(
		ID INT IDENTITY(1,1)
		,numTaskID NUMERIC(18,0)
		,numTaskTimeInMinutes NUMERIC(18,0)
		,numTaskAssignee NUMERIC(18,0)
		,intTaskType INT --1:Parallel, 2:Sequential
		,dtPlannedStartDate DATETIME
		,dtFinishDate DATETIME
	)

	INSERT INTO @TempTasks
	(
		numTaskID
		,numTaskTimeInMinutes
		,intTaskType
		,numTaskAssignee
	)
	SELECT 
		SPDT.numTaskID
		,((ISNULL(SPDT.numHours,0) * 60) + ISNULL(SPDT.numMinutes,0)) * @numQtyToBuild
		,ISNULL(SPDT.intTaskType,1)
		,SPDT.numAssignTo
	FROM 
		StagePercentageDetailsTask SPDT
	INNER JOIN
		WorkOrder WO
	ON
		SPDT.numWorkOrderId = WO.numWOId
	WHERE 
		SPDT.numDomainId=@numDomainID
		AND SPDT.numWorkOrderId = @numWOID

	UPDATE @TempTasks SET dtPlannedStartDate=@dtPlannedStartDate

	INSERT INTO @TempTaskAssignee
	(
		numAssignedTo
	)
	SELECT DISTINCT
		numTaskAssignee
	FROM
		@TempTasks

	DECLARE @i INT = 1
	DECLARE @iCount INT 	
	SELECT @iCount = COUNT(*) FROM @TempTasks

	DECLARE @numTaskAssignee NUMERIC(18,0)
	DECLARE @intTaskType INT
	DECLARE @numWorkScheduleID NUMERIC(18,0)
	DECLARE @numTempUserCntID NUMERIC(18,0)
	DECLARE @dtStartDate DATETIME
	DECLARE @numTotalTaskInMinutes NUMERIC(18,0)
	DECLARE @numProductiveTimeInMinutes NUMERIC(18,0)
	DECLARE @tmStartOfDay TIME(7)
	DECLARE @numTimeLeftForDay NUMERIC(18,0)
	DECLARE @vcWorkDays VARCHAR(20)
	DECLARE @bitParallelStartSet BIT = 0

	WHILE @i <= @iCount
	BEGIN
		SELECT
			@numTaskAssignee=numTaskAssignee
			,@numTotalTaskInMinutes=numTaskTimeInMinutes
			,@intTaskType = intTaskType
		FROM
			@TempTasks 
		WHERE
			ID=@i

		-- GET TASK COMPLETION TIME WHICH WILL BE START DATETIME FOR NEXT TASK
		SELECT
			@numWorkScheduleID = WS.ID
			,@numProductiveTimeInMinutes=(ISNULL(WS.numProductiveHours,0) * 60) + ISNULL(WS.numProductiveMinutes,0)
			,@tmStartOfDay = tmStartOfDay
			,@vcWorkDays=vcWorkDays
			,@dtStartDate = DATEADD(MINUTE,@ClientTimeZoneOffset,CONVERT(DATETIME,CONVERT(VARCHAR(8),DATEADD(MINUTE,-@ClientTimeZoneOffset,@dtPlannedStartDate),112)) + tmStartOfDay)
		FROM
			WorkSchedule WS
		INNER JOIN
			UserMaster
		ON
			WS.numUserCntID = UserMaster.numUserDetailId
		WHERE 
			WS.numUserCntID = @numTaskAssignee

		IF @intTaskType=1 AND EXISTS (SELECT numAssignedTo FROM @TempTaskAssignee WHERE numAssignedTo = @numTaskAssignee AND dtLastTaskCompletionTime IS NOT NULL)
		BEGIN
			SELECT @dtStartDate = dtLastTaskCompletionTime FROM @TempTaskAssignee WHERE numAssignedTo = @numTaskAssignee
		END
		ELSE IF EXISTS (SELECT numAssignedTo FROM @TempTaskAssignee WHERE numAssignedTo = @numTaskAssignee AND dtLastTaskCompletionTime IS NOT NULL)
		BEGIN
			SELECT @dtStartDate = dtLastTaskCompletionTime FROM @TempTaskAssignee WHERE numAssignedTo = @numTaskAssignee
		END

		UPDATE @TempTasks SET dtPlannedStartDate=@dtStartDate WHERE ID=@i 

		IF @numProductiveTimeInMinutes > 0 AND @numTotalTaskInMinutes > 0
		BEGIN
			WHILE @numTotalTaskInMinutes > 0
			BEGIN
				-- IF ITS WORKING DAY AND ALSO NOT HOLIDAY THEN PROCEED OR INCRESE DATE TO NEXT DAY
				IF DATEPART(WEEKDAY,@dtStartDate) IN (SELECT Id FROM dbo.SplitIDs(@vcWorkDays,',')) AND NOT EXISTS (SELECT ID FROM WorkScheduleDaysOff WHERE numWorkScheduleID=@numWorkScheduleID AND CAST(DATEADD(MINUTE,@ClientTimeZoneOffset * -1,@dtStartDate) AS DATE) BETWEEN dtDayOffFrom AND dtDayOffTo)
				BEGIN
					IF CAST(@dtStartDate AS DATE) = CAST(GETUTCDATE() AS DATE)
					BEGIN
						-- CHECK TIME LEFT FOR DAY BASED
						SET @numTimeLeftForDay = DATEDIFF(MINUTE,@dtStartDate,DATEADD(MINUTE,@numProductiveTimeInMinutes,DATEADD(MINUTE,@ClientTimeZoneOffset,CONVERT(DATETIME,CONVERT(VARCHAR(8),DATEADD(MINUTE,-@ClientTimeZoneOffset,@dtStartDate),112)) + @tmStartOfDay)))
					END
					ELSE
					BEGIN
						SET @numTimeLeftForDay = @numProductiveTimeInMinutes
					END

					IF @numTimeLeftForDay > 0
					BEGIN
						IF @numTimeLeftForDay > @numTotalTaskInMinutes
						BEGIN
							SET @dtStartDate = DATEADD(MINUTE,@numTotalTaskInMinutes,@dtStartDate)
						END
						ELSE
						BEGIN
							SET @dtStartDate = DATEADD(MINUTE,@ClientTimeZoneOffset,CONVERT(DATETIME,CONVERT(VARCHAR(8),DATEADD(MINUTE,-@ClientTimeZoneOffset,DATEADD(DAY,1,@dtStartDate)),112)) + @tmStartOfDay)
						END

						SET @numTotalTaskInMinutes = @numTotalTaskInMinutes - @numTimeLeftForDay
					END
					ELSE
					BEGIN
						SET @dtStartDate = DATEADD(MINUTE,@ClientTimeZoneOffset,CONVERT(DATETIME,CONVERT(VARCHAR(8),DATEADD(MINUTE,-@ClientTimeZoneOffset,DATEADD(DAY,1,@dtStartDate)),112)) + @tmStartOfDay)
					END
				END
				ELSE
				BEGIN
					SET @dtStartDate = DATEADD(MINUTE,@ClientTimeZoneOffset,CONVERT(DATETIME,CONVERT(VARCHAR(8),DATEADD(MINUTE,-@ClientTimeZoneOffset,DATEADD(DAY,1,@dtStartDate)),112)) + @tmStartOfDay)					
				END				
			END
		END	

		UPDATE @TempTasks SET dtFinishDate=@dtStartDate WHERE ID=@i 
		UPDATE @TempTaskAssignee SET dtLastTaskCompletionTime=@dtStartDate WHERE numAssignedTo=@numTaskAssignee

		SET @i = @i + 1
	END

	SELECT @dtProjectedFinishDate=MAX(dtFinishDate) FROM @TempTasks

	RETURN DATEADD(MINUTE,-@ClientTimeZoneOffset,@dtProjectedFinishDate)
END 
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='GetTimeSpendOnTask')
DROP FUNCTION GetTimeSpendOnTask
GO
CREATE FUNCTION [dbo].[GetTimeSpendOnTask]
(
	@numDomainID NUMERIC(18,0)
	,@numTaskID NUMERIC(18,0)
)    
RETURNS VARCHAR(50)    
AS
BEGIN
	DECLARE @vcTotalTimeSpendOnTask VARCHAR(50) = ''
	DECLARE @numTotalMinutes INT = 0

	DECLARE @TEMTIMELong TABLE
	(
		ID INT IDENTITY(1,1)
		,tintAction TINYINT
		,dtActionTime DATETIME
	)

	INSERT INTO @TEMTIMELong
	(
		tintAction
		,dtActionTime
	)
	SELECT
		tintAction
		,dtActionTime
	FROM
		StagePercentageDetailsTaskTimeLog SPDTTL
	WHERE
		SPDTTL.numDomainID=@numDomainID
		AND SPDTTL.numTaskID=@numTaskID
	ORDER BY
		dtActionTime
		,tintAction

	DECLARE @i INT = 1
	DECLARE @iCount INT
	DECLARE @tintLastAction TINYINT
	DECLARE @dtLastActionTime DATETIME

	DECLARE @tintAction TINYINT
	DECLARE @dtActionTime DATETIME
	SELECT @iCount = COUNT(*) FROM @TEMTIMELong

	WHILE @i <= @iCount
	BEGIN
		SELECT
			@tintAction=tintAction
			,@dtActionTime=dtActionTime
		FROM
			@TEMTIMELong
		WHERE
			ID = @i

		IF @i = 1 
		BEGIN
			IF @tintAction <> 1
			BEGIN
				SET @vcTotalTimeSpendOnTask = 'Invalid time sequence'
				BREAK;
			END
			ELSE
			BEGIN
				SET @tintLastAction = @tintAction
				SET @dtLastActionTime = @dtActionTime
			END
		END
		ELSE
		BEGIN
			IF @tintLastAction = 1 AND @tintAction NOT IN (2,4)
			BEGIN
				SET @vcTotalTimeSpendOnTask = 'Invalid time sequence'
				BREAK;
			END
			ELSE IF @tintLastAction = 2 AND @tintAction <> 3
			BEGIN
				SET @vcTotalTimeSpendOnTask = 'Invalid time sequence'
				BREAK;
			END
			ELSE IF @tintLastAction = 3 AND @tintAction NOT IN (2,4)
			BEGIN
				SET @vcTotalTimeSpendOnTask = 'Invalid time sequence'
				BREAK;
			END
			ELSE IF @tintLastAction = 4
			BEGIN
				SET @vcTotalTimeSpendOnTask = 'Invalid time sequence'
				BREAK;
			END
			ELSE
			BEGIN
				IF @tintAction IN (2,4)
				BEGIN
					SET @numTotalMinutes = @numTotalMinutes + DATEDIFF(MINUTE,@dtLastActionTime,@dtActionTime)
				END

				SET @tintLastAction = @tintAction
				SET @dtLastActionTime = @dtActionTime
			END
		END

		SET @i = @i + 1
	END

	IF @vcTotalTimeSpendOnTask <> 'Invalid time sequence'
	BEGIN
		RETURN FORMAT(@numTotalMinutes / 60,'00') + ':' + FORMAT(@numTotalMinutes % 60,'00')
	END
	
	RETURN @vcTotalTimeSpendOnTask
END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='GetTimeSpendOnTaskInMinutes')
DROP FUNCTION GetTimeSpendOnTaskInMinutes
GO
CREATE FUNCTION [dbo].[GetTimeSpendOnTaskInMinutes]
(
	@numDomainID NUMERIC(18,0)
	,@numTaskID NUMERIC(18,0)
)    
RETURNS INT  
AS
BEGIN
	DECLARE @numTotalMinutes INT = 0

	DECLARE @TEMTIMELong TABLE
	(
		ID INT IDENTITY(1,1)
		,tintAction TINYINT
		,dtActionTime DATETIME
	)

	INSERT INTO @TEMTIMELong
	(
		tintAction
		,dtActionTime
	)
	SELECT
		tintAction
		,dtActionTime
	FROM
		StagePercentageDetailsTaskTimeLog SPDTTL
	WHERE
		SPDTTL.numDomainID=@numDomainID
		AND SPDTTL.numTaskID=@numTaskID
	ORDER BY
		dtActionTime

	DECLARE @i INT = 1
	DECLARE @iCount INT
	DECLARE @tintLastAction TINYINT
	DECLARE @dtLastActionTime DATETIME

	DECLARE @tintAction TINYINT
	DECLARE @dtActionTime DATETIME
	SELECT @iCount = COUNT(*) FROM @TEMTIMELong

	WHILE @i <= @iCount
	BEGIN
		SELECT
			@tintAction=tintAction
			,@dtActionTime=dtActionTime
		FROM
			@TEMTIMELong
		WHERE
			ID = @i

		IF @i = 1 
		BEGIN
			IF @tintAction <> 1
			BEGIN
				SET @numTotalMinutes = 0
				BREAK;
			END
			ELSE
			BEGIN
				SET @tintLastAction = @tintAction
				SET @dtLastActionTime = @dtActionTime
			END
		END
		ELSE
		BEGIN
			IF @tintLastAction = 1 AND @tintAction NOT IN (2,4)
			BEGIN
				SET @numTotalMinutes = 0
				BREAK;
			END
			ELSE IF @tintLastAction = 2 AND @tintAction <> 3
			BEGIN
				SET @numTotalMinutes = 0
				BREAK;
			END
			ELSE IF @tintLastAction = 3 AND @tintAction NOT IN (2,4)
			BEGIN
				SET @numTotalMinutes = 0
				BREAK;
			END
			ELSE IF @tintLastAction = 4
			BEGIN
				SET @numTotalMinutes = 0
				BREAK;
			END
			ELSE
			BEGIN
				IF @tintAction IN (2,4)
				BEGIN
					SET @numTotalMinutes = @numTotalMinutes + DATEDIFF(MINUTE,@dtLastActionTime,@dtActionTime)
				END

				SET @tintLastAction = @tintAction
				SET @dtLastActionTime = @dtActionTime
			END
		END

		SET @i = @i + 1
	END

	
	RETURN @numTotalMinutes
END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='GetTimeSpendOnTaskInMinutesDateRange')
DROP FUNCTION GetTimeSpendOnTaskInMinutesDateRange
GO
CREATE FUNCTION [dbo].[GetTimeSpendOnTaskInMinutesDateRange]
(
	@numDomainID NUMERIC(18,0)
	,@numUserCntID NUMERIC(18,0)
	,@numTaskID NUMERIC(18,0)
	,@dtFromDate DATETIME
	,@dtToDate DATETIME
)    
RETURNS INT  
AS
BEGIN
	DECLARE @numTotalMinutes INT = 0

	DECLARE @TEMTIMELong TABLE
	(
		ID INT IDENTITY(1,1)
		,tintAction TINYINT
		,dtActionTime DATETIME
	)

	INSERT INTO @TEMTIMELong
	(
		tintAction
		,dtActionTime
	)
	SELECT
		tintAction
		,dtActionTime
	FROM
		StagePercentageDetailsTaskTimeLog SPDTTL
	WHERE
		SPDTTL.numDomainID=@numDomainID
		AND SPDTTL.numTaskID=@numTaskID
		AND SPDTTL.numUserCntID = @numUserCntID
		AND SPDTTL.dtActionTime BETWEEN @dtFromDate AND @dtToDate
	ORDER BY
		dtActionTime

	DECLARE @i INT = 1
	DECLARE @iCount INT
	DECLARE @tintLastAction TINYINT
	DECLARE @dtLastActionTime DATETIME

	DECLARE @tintAction TINYINT
	DECLARE @dtActionTime DATETIME
	SELECT @iCount = COUNT(*) FROM @TEMTIMELong

	WHILE @i <= @iCount
	BEGIN
		SELECT
			@tintAction=tintAction
			,@dtActionTime=dtActionTime
		FROM
			@TEMTIMELong
		WHERE
			ID = @i

		IF @i = 1 
		BEGIN
			IF @tintAction NOT IN (1,3)
			BEGIN
				SET @numTotalMinutes = 0
				BREAK;
			END
			ELSE
			BEGIN
				SET @tintLastAction = @tintAction
				SET @dtLastActionTime = @dtActionTime
			END
		END
		ELSE
		BEGIN
			IF @tintLastAction = 1 AND @tintAction NOT IN (2,4)
			BEGIN
				SET @numTotalMinutes = 0
				BREAK;
			END
			ELSE IF @tintLastAction = 2 AND @tintAction <> 3
			BEGIN
				SET @numTotalMinutes = 0
				BREAK;
			END
			ELSE IF @tintLastAction = 3 AND @tintAction NOT IN (2,4)
			BEGIN
				SET @numTotalMinutes = 0
				BREAK;
			END
			ELSE IF @tintLastAction = 4
			BEGIN
				SET @numTotalMinutes = 0
				BREAK;
			END
			ELSE
			BEGIN
				IF @tintAction IN (2,4)
				BEGIN
					SET @numTotalMinutes = @numTotalMinutes + DATEDIFF(MINUTE,@dtLastActionTime,@dtActionTime)
				END

				SET @tintLastAction = @tintAction
				SET @dtLastActionTime = @dtActionTime
			END
		END

		SET @i = @i + 1
	END

	
	RETURN @numTotalMinutes
END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='GetTotalProgress')
DROP FUNCTION GetTotalProgress
GO
CREATE FUNCTION [dbo].[GetTotalProgress] 
(
	@numDomainID NUMERIC(18,0)
	,@numRecordID NUMERIC(18,0)
	,@tintRecordType TINYINT --1:WorkOrder, 2:Project
	,@tintProgressFor TINYINT -- 1:Record, 2:Milestome, 3:Stage
	,@vcMileStone VARCHAR(200)
	,@numStageDetailsId NUMERIC(18,0)	
)
RETURNS INT
AS   
BEGIN  
	DECLARE @numTotalProgress INT = 0

	DECLARE @TEMPStages TABLE
	(
		vcMileStoneName VARCHAR(200)
		,numMilestonePercentage INT
		,numStageDetailsId NUMERIC(18,0)
		,vcStageName VARCHAR(200)
		,numStagePercentage INT
		,numTotalStageTask INT
		,numCompletedStageTask INT
		,numStageProgress INT
		,numMilestoneProgress INT
	)

	INSERT INTO @TEMPStages
	(
		vcMileStoneName
		,numMilestonePercentage
		,numStageDetailsId
		,vcStageName
		,numStagePercentage
		,numTotalStageTask
		,numCompletedStageTask
	)
	SELECT
		StagePercentageDetails.vcMileStoneName
		,StagePercentageMaster.numStagePercentage AS numMilestonePercentage
		,StagePercentageDetails.numStageDetailsId
		,StagePercentageDetails.vcStageName
		,StagePercentageDetails.tintPercentage
		,ISNULL((SELECT COUNT(*) FROM StagePercentageDetailsTask SPDT WHERE SPDT.numStageDetailsId=StagePercentageDetails.numStageDetailsId),0) numTotalStageTask
		,ISNULL((SELECT COUNT(*) FROM StagePercentageDetailsTask SPDT WHERE SPDT.numStageDetailsId=StagePercentageDetails.numStageDetailsId AND EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID=SPDT.numTaskId AND tintAction=4)),0) numCompletedStageTask
	FROM
		StagePercentageDetails
	INNER JOIN
		StagePercentageMaster
	ON
		StagePercentageDetails.numStagePercentageId = StagePercentageMaster.numStagePercentageId
	WHERE
		numDomainId=@numDomainID
		AND 1 = (CASE 
					WHEN @tintRecordType = 1 THEN (CASE WHEN numWorkOrderId=@numRecordID THEN 1 ELSE 0 END)
					WHEN @tintRecordType = 2 THEN (CASE WHEN numOppID=@numRecordID THEN 1 ELSE 0 END)
					ELSE 0
				END)
	  
	UPDATE @TEMPStages SET numStageProgress = (CASE WHEN numTotalStageTask > 0 THEN (numCompletedStageTask * 100) / numTotalStageTask ELSE 0 END)

	UPDATE @TEMPStages SET numMilestoneProgress = (numStageProgress * numStagePercentage) / 100

	IF @tintProgressFor = 1
	BEGIN
		SET @numTotalProgress = ISNULL((SELECT AVG(numMilestoneProgress) FROM (SELECT SUM(numMilestoneProgress) numMilestoneProgress FROM @TEMPStages GROUP BY vcMileStoneName) TEMP),0)
	END
	ELSE IF @tintProgressFor = 2
	BEGIN
		SET @numTotalProgress = ISNULL((SELECT SUM(numMilestoneProgress) FROM @TEMPStages WHERE vcMileStoneName=@vcMileStone),0)
	END
	ELSE IF @tintProgressFor = 3
	BEGIN
		SET @numTotalProgress = ISNULL((SELECT numStageProgress FROM @TEMPStages WHERE numStageDetailsId=@numStageDetailsId),0)
	END
  
	RETURN @numTotalProgress
END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='GetWorkOrderLabourCost')
DROP FUNCTION GetWorkOrderLabourCost
GO
CREATE FUNCTION [dbo].[GetWorkOrderLabourCost]
(
	@numDomainID NUMERIC(18,0)
	,@numWOID NUMERIC(18,0)
)    
RETURNS DECIMAL(20,5)    
AS
BEGIN
	DECLARE @monLabourCost DECIMAL(20,5)
	
	SET @monLabourCost = ISNULL((SELECT
									SUM(dbo.GetTimeSpendOnTaskInMinutes(@numDomainID,SPDT.numTaskId) * (ISNULL(SPDT.monHourlyRate,0)/60))
								FROM
									StagePercentageDetailsTask SPDT
								WHERE
									SPDT.numDomainID = @numDomainID
									AND SPDT.numWorkOrderId = @numWOID),0)

	RETURN @monLabourCost
END
GO
/****** Object:  StoredProcedure [dbo].[USP_AssCntCase]    Script Date: 07/26/2008 16:14:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--modified by anoop jayaraj      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_asscntcase')
DROP PROCEDURE usp_asscntcase
GO
CREATE PROCEDURE [dbo].[USP_AssCntCase]            
@numCaseId numeric(9)=0 ,
@numDomainID as numeric(9)=0           
--          
AS          
      
        
       
 SELECT  D.numDivisionID,a.numContactId,vcCompanyName+', '+isnull(Lst.vcData,'-') as Company,       
  a.vcFirstname +' '+ a.vcLastName as [Name],        
  --a.numPhone +', '+ a.numPhoneExtension as Phone,  
  case when a.numPhone<>'' then + a.numPhone +case when a.numPhoneExtension<>'' then ' - ' + a.numPhoneExtension else '' end  else '' end as [Phone],  
  a.vcEmail as Email,        
  b.vcData as ContactRole,        
  CCont.numRole as ContRoleId,       
  convert(integer,isnull(bitPartner,0)) as bitPartner,       
  a.numcontacttype FROM CaseContacts CCont        
  join additionalContactsinformation a on        
  a.numContactId=CCont.numContactId      
  join DivisionMaster D      
  on a.numDivisionID =D.numDivisionID      
  join CompanyInfo C      
  on D.numCompanyID=C.numCompanyID       
  left join listdetails b         
  on b.numlistitemid=CCont.numRole      
  left join listdetails Lst      
  on Lst.numlistitemid=C.numCompanyType      
  WHERE CCont.numCaseID=@numCaseId    and A.numDomainID= @numDomainID
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_BizFormWizardModule_GetAll' ) 
    DROP PROCEDURE USP_BizFormWizardModule_GetAll
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Sandeep Patel
-- Create date: 21 July 2014
-- Description:	Gets list of bizform wizard modules
-- =============================================
CREATE PROCEDURE USP_BizFormWizardModule_GetAll
	@numDomainID NUMERIC(18,0)
AS
BEGIN
	
	SET NOCOUNT ON;

	IF (SELECT COUNT(*) FROM ListDetails WHERE numListID = 5 AND numDomainID = @numDomainID AND constFlag <> 1) > 0
		SELECT * FROM BizFormWizardModule WHERE ISNULL(bitActive,1)=1 ORDER BY tintSortOrder
	ELSE
		SELECT * FROM BizFormWizardModule WHERE ISNULL(bitActive,1)=1 ORDER BY tintSortOrder	
END
GO
/****** Object:  StoredProcedure [dbo].[usp_BusinessProcessList]    Script Date: 07/26/2008 16:14:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_businessprocesslist')
DROP PROCEDURE usp_businessprocesslist
GO
CREATE PROCEDURE [dbo].[usp_BusinessProcessList] 
@numDomainId as numeric(9),
@Mode as int
as 
IF(@Mode=-1)
BEGIN
select Slp_Id,Slp_Name from Sales_process_List_Master where numDomainID= @numDomainId AND ISNULL(numOppId,0)=0 AND ISNULL(numProjectId,0)=0 AND ISNULL(numWorkOrderId,0)=0 order by slp_name
END
ELSE
BEGIN
select Slp_Id,Slp_Name from Sales_process_List_Master where Pro_Type=@Mode and numDomainID= @numDomainId AND ISNULL(numOppId,0)=0 AND ISNULL(numProjectId,0)=0 AND ISNULL(numWorkOrderId,0)=0 order by slp_name
END
GO
--Created by anoop jayaraj                                                          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_checkuseratlogin')
DROP PROCEDURE usp_checkuseratlogin
GO
CREATE PROCEDURE [dbo].[USP_CheckUserAtLogin]                                                              
(                                                                                    
	@UserName as varchar(100)='',                                                                        
	@vcPassword as varchar(100)='',
	@vcLinkedinId as varchar(300)=''                                                                           
)                                                              
AS             
BEGIN
	DECLARE @listIds VARCHAR(MAX)

	SELECT 
		@listIds = COALESCE(@listIds+',' ,'') + CAST(UPA.numUserID AS VARCHAR(100)) 
	FROM 
		UserMaster U                              
	Join 
		Domain D                              
	on 
		D.numDomainID=U.numDomainID
	Join  
		Subscribers S                            
	on 
		S.numTargetDomainID=D.numDomainID
	join
		UnitPriceApprover UPA
	ON
		D.numDomainID = UPA.numDomainID
	WHERE 
		1 = (CASE 
			WHEN @vcLinkedinId='N' THEN (CASE WHEN vcEmailID=@UserName and vcPassword=@vcPassword THEN 1 ELSE 0 END)
			ELSE (CASE WHEN vcLinkedinId=@vcLinkedinId THEN 1 ELSE 0 END)
			END)
       
	IF (SELECT 
			COUNT(*)
		FROM 
			UserMaster U                              
		JOIN 
			Domain D                              
		ON 
			D.numDomainID=U.numDomainID
		JOIN 
			Subscribers S                            
		ON 
			S.numTargetDomainID=D.numDomainID
		WHERE 
			1 = (CASE 
					WHEN @vcLinkedinId='N' THEN (CASE WHEN vcEmailID=@UserName and vcPassword=@vcPassword THEN 1 ELSE 0 END)
					ELSE (CASE WHEN vcLinkedinId=@vcLinkedinId THEN 1 ELSE 0 END)
				END) 
			AND bitActive=1) > 1
	BEGIN
		RAISERROR('MULTIPLE_RECORDS_WITH_SAME_EMAIL',16,1)
		RETURN;
	END   
	                                          
	/*check subscription validity */
	IF EXISTS(SELECT 
					*
			FROM 
				UserMaster U                              
			JOIN 
				Domain D                              
			ON 
				D.numDomainID=U.numDomainID
			JOIN 
				Subscribers S                            
			ON 
				S.numTargetDomainID=D.numDomainID
			WHERE 
				1 = (CASE 
						WHEN @vcLinkedinId='N' THEN (CASE WHEN vcEmailID=@UserName and vcPassword=@vcPassword THEN 1 ELSE 0 END)
						ELSE (CASE WHEN vcLinkedinId=@vcLinkedinId THEN 1 ELSE 0 END)
					END) 
				AND bitActive=1 
				AND getutcdate() > dtSubEndDate)
	BEGIN
		RAISERROR('SUB_RENEW',16,1)
		RETURN;
	END

	SELECT TOP 1 
		U.numUserID
		,numUserDetailId
		,D.numCost
		,isnull(vcEmailID,'') vcEmailID
		,ISNULL(vcEmailAlias,'') vcEmailAlias
		,ISNULL(vcEmailAliasPassword,'') vcEmailAliasPassword
		,isnull(numGroupID,0) numGroupID
		,bitActivateFlag
		,isnull(vcMailNickName,'') vcMailNickName
		,isnull(txtSignature,'') txtSignature
		,isnull(U.numDomainID,0) numDomainID
		,isnull(Div.numDivisionID,0) numDivisionID
		,isnull(vcCompanyName,'') vcCompanyName
		,isnull(E.bitExchangeIntegration,0) bitExchangeIntegration
		,isnull(E.bitAccessExchange,0) bitAccessExchange
		,isnull(E.vcExchPath,'') vcExchPath
		,isnull(E.vcExchDomain,'') vcExchDomain
		,isnull(D.vcExchUserName,'') vcExchUserName
		,isnull(vcFirstname,'') + ' ' + isnull(vcLastName,'') as ContactName
		,Case when E.bitAccessExchange=0 then  E.vcExchPassword when E.bitAccessExchange=1 then D.vcExchPassword end as vcExchPassword
		,tintCustomPagingRows
		,vcDateFormat
		,numDefCountry
		,tintComposeWindow
		,dateadd(day,-sintStartDate,getutcdate()) as StartDate
		,dateadd(day,sintNoofDaysInterval,dateadd(day,-sintStartDate,getutcdate())) as EndDate
		,tintAssignToCriteria
		,bitIntmedPage
		,tintFiscalStartMonth
		,numAdminID
		,isnull(A.numTeam,0) numTeam
		,isnull(D.vcCurrency,'') as vcCurrency
		,isnull(D.numCurrencyID,0) as numCurrencyID, 
		isnull(D.bitMultiCurrency,0) as bitMultiCurrency,              
		bitTrial,isnull(tintChrForComSearch,0) as tintChrForComSearch,  
		isnull(tintChrForItemSearch,0) as tintChrForItemSearch,              
		case when bitSMTPServer= 1 then vcSMTPServer else '' end as vcSMTPServer  ,          
		case when bitSMTPServer= 1 then  isnull(numSMTPPort,0)  else 0 end as numSMTPPort      
		,isnull(bitSMTPAuth,0) bitSMTPAuth    
		,isnull([vcSmtpPassword],'') vcSmtpPassword    
		,isnull(bitSMTPSSL,0) bitSMTPSSL   ,isnull(bitSMTPServer,0) bitSMTPServer,       
		isnull(IM.bitImap,0) bitImapIntegration, isnull(charUnitSystem,'E')   UnitSystem, datediff(day,getutcdate(),dtSubEndDate) as NoOfDaysLeft,
		ISNULL(bitCreateInvoice,0) bitCreateInvoice,
		ISNULL([numDefaultSalesBizDocId],0) numDefaultSalesBizDocId,
		ISNULL([numDefaultPurchaseBizDocId],0) numDefaultPurchaseBizDocId,
		vcDomainName,
		S.numSubscriberID,
		D.[tintBaseTax],
		u.ProfilePic,
		ISNULL(D.[numShipCompany],0) numShipCompany,
		ISNULL(D.[bitEmbeddedCost],0) bitEmbeddedCost,
		(Select isnull(numAuthoritativeSales,0) From AuthoritativeBizDocs Where numDomainId=D.numDomainID) numAuthoritativeSales,
		(Select isnull(numAuthoritativePurchase,0) From AuthoritativeBizDocs Where numDomainId=D.numDomainID) numAuthoritativePurchase,
		ISNULL(D.[numDefaultSalesOppBizDocId],0) numDefaultSalesOppBizDocId,
		ISNULL(tintDecimalPoints,2) tintDecimalPoints,
		ISNULL(D.tintBillToForPO,0) tintBillToForPO, 
		ISNULL(D.tintShipToForPO,0) tintShipToForPO,
		ISNULL(D.tintOrganizationSearchCriteria,0) tintOrganizationSearchCriteria,
		ISNULL(D.tintLogin,0) tintLogin,
		ISNULL(bitDocumentRepositary,0) bitDocumentRepositary,
		ISNULL(D.vcPortalName,'') vcPortalName,
		(SELECT COUNT(*) FROM dbo.Subscribers WHERE getutcdate() between dtSubStartDate and dtSubEndDate and bitActive=1),
		ISNULL(D.bitGtoBContact,0) bitGtoBContact,
		ISNULL(D.bitBtoGContact,0) bitBtoGContact,
		ISNULL(D.bitGtoBCalendar,0) bitGtoBCalendar,
		ISNULL(D.bitBtoGCalendar,0) bitBtoGCalendar,
		ISNULL(bitExpenseNonInventoryItem,0) bitExpenseNonInventoryItem,
		ISNULL(D.bitInlineEdit,0) bitInlineEdit,
		ISNULL(U.numDefaultClass,0) numDefaultClass,
		ISNULL(D.bitRemoveVendorPOValidation,0) bitRemoveVendorPOValidation,
		ISNULL(D.bitTrakDirtyForm,1) bitTrakDirtyForm,
		isnull(D.tintBaseTaxOnArea,0) tintBaseTaxOnArea,
		ISNULL(D.bitAmountPastDue,0) bitAmountPastDue,
		CONVERT(DECIMAL(18,2),ISNULL(D.monAmountPastDue,0)) monAmountPastDue,
		ISNULL(D.bitSearchOrderCustomerHistory,0) bitSearchOrderCustomerHistory,
		isnull(U.tintTabEvent,0) as tintTabEvent,
		isnull(D.bitRentalItem,0) as bitRentalItem,
		isnull(D.numRentalItemClass,0) as numRentalItemClass,
		isnull(D.numRentalHourlyUOM,0) as numRentalHourlyUOM,
		isnull(D.numRentalDailyUOM,0) as numRentalDailyUOM,
		isnull(D.tintRentalPriceBasedOn,0) as tintRentalPriceBasedOn,
		isnull(D.tintCalendarTimeFormat,24) as tintCalendarTimeFormat,
		isnull(D.tintDefaultClassType,0) as tintDefaultClassType,
		ISNULL(U.numDefaultWarehouse,0) numDefaultWarehouse,
		ISNULL(D.tintPriceBookDiscount,0) tintPriceBookDiscount,
		ISNULL(D.bitAutoSerialNoAssign,0) bitAutoSerialNoAssign,
		ISNULL(IsEnableProjectTracking,0) AS [IsEnableProjectTracking],
		ISNULL(IsEnableCampaignTracking,0) AS [IsEnableCampaignTracking],
		ISNULL(IsEnableResourceScheduling,0) AS [IsEnableResourceScheduling],
		ISNULL(D.numSOBizDocStatus,0) AS numSOBizDocStatus,
		ISNULL(D.bitAutolinkUnappliedPayment,0) AS bitAutolinkUnappliedPayment,
		ISNULL(numShippingServiceItemID,0) AS [numShippingServiceItemID],
		ISNULL(D.numDiscountServiceItemID,0) AS numDiscountServiceItemID,
		ISNULL(D.tintCommissionType,1) AS tintCommissionType,
		ISNULL(D.bitDiscountOnUnitPrice,0) AS bitDiscountOnUnitPrice,
		ISNULL(D.intShippingImageWidth,0) AS intShippingImageWidth ,
		ISNULL(D.intShippingImageHeight,0) AS intShippingImageHeight,
		ISNULL(D.numTotalInsuredValue,0) AS numTotalInsuredValue,
		ISNULL(D.numTotalCustomsValue,0) AS numTotalCustomsValue,
		ISNULL(D.bitUseBizdocAmount,1) AS [bitUseBizdocAmount],
		ISNULL(D.bitDefaultRateType,1) AS [bitDefaultRateType],
		ISNULL(D.bitIncludeTaxAndShippingInCommission,1) AS [bitIncludeTaxAndShippingInCommission],
		ISNULL(D.[bitLandedCost],0) AS bitLandedCost,
		ISNULL(D.vcLanedCostDefault,'') AS vcLanedCostDefault,
		ISNULL(D.bitMinUnitPriceRule,0) AS bitMinUnitPriceRule,
		ISNULL(@listIds,'') AS vcUnitPriceApprover,
		ISNULL(D.numPODropShipBizDoc,0) AS numPODropShipBizDoc,
		ISNULL(D.numPODropShipBizDocTemplate,0) AS numPODropShipBizDocTemplate,
		ISNULL(D.IsEnableDeferredIncome,0) AS IsEnableDeferredIncome,
		ISNULL(D.bitEnableItemLevelUOM,0) AS bitEnableItemLevelUOM,
		ISNULL(D.bitPurchaseTaxCredit,0) AS bitPurchaseTaxCredit,
		ISNULL(U.bintCreatedDate,0) AS bintCreatedDate,
		(CASE WHEN LEN(ISNULL(TempDefaultPage.vcDefaultNavURL,'')) = 0 THEN ISNULL(TempDefaultTab.vcDefaultNavURL,'Items/frmItemList.aspx?Page=All Items&ItemGroup=0') ELSE ISNULL(TempDefaultPage.vcDefaultNavURL,'') END) vcDefaultNavURL,
		ISNULL(D.bitApprovalforTImeExpense,0) AS bitApprovalforTImeExpense,
		ISNULL(D.intTimeExpApprovalProcess,0) AS intTimeExpApprovalProcess,ISNULL(D.bitApprovalforOpportunity,0) AS bitApprovalforOpportunity,
		ISNULL(D.intOpportunityApprovalProcess,0) AS intOpportunityApprovalProcess,
		ISNULL(D.numDefaultSalesShippingDoc,0) AS numDefaultSalesShippingDoc,
		ISNULL(D.numDefaultPurchaseShippingDoc,0) AS numDefaultPurchaseShippingDoc,
		ISNULL((SELECT TOP 1 bitMarginPriceViolated FROM ApprovalProcessItemsClassification  WHERE numDomainID=D.numDomainID AND ISNULL(bitMarginPriceViolated,0) = 1),0) AS bitMarginPriceViolated,
		ISNULL((SELECT TOP 1 intViewAllowed FROM GroupAuthorization WHERE numGroupID=U.numGroupID AND numModuleID=2 AND numPageID=2),0) AS tintLeadRights,
		ISNULL((SELECT TOP 1 intViewAllowed FROM GroupAuthorization WHERE numGroupID=U.numGroupID AND numModuleID=3 AND numPageID=2),0) AS tintProspectRights,
		ISNULL((SELECT TOP 1 intViewAllowed FROM GroupAuthorization WHERE numGroupID=U.numGroupID AND numModuleID=4 AND numPageID=2),0) AS tintAccountRights,
		ISNULL((SELECT TOP 1 intViewAllowed FROM GroupAuthorization WHERE numGroupID=U.numGroupID AND numModuleID=10 AND numPageID=10),0) AS tintSalesOrderListRights,
		ISNULL((SELECT TOP 1 intViewAllowed FROM GroupAuthorization WHERE numGroupID=U.numGroupID AND numModuleID=10 AND numPageID=11),0) AS tintPurchaseOrderListRights,
		ISNULL((SELECT TOP 1 intViewAllowed FROM GroupAuthorization WHERE numGroupID=U.numGroupID AND numModuleID=10 AND numPageID=2),0) AS tintSalesOpportunityListRights,
		ISNULL((SELECT TOP 1 intViewAllowed FROM GroupAuthorization WHERE numGroupID=U.numGroupID AND numModuleID=10 AND numPageID=8),0) AS tintPurchaseOpportunityListRights,
		ISNULL((SELECT TOP 1 intViewAllowed FROM GroupAuthorization WHERE numGroupID=U.numGroupID AND numModuleID=11 AND numPageID=2),0) AS tintContactListRights,
		ISNULL((SELECT TOP 1 intViewAllowed FROM GroupAuthorization WHERE numGroupID=U.numGroupID AND numModuleID=7 AND numPageID=2),0) AS tintCaseListRights,
		ISNULL((SELECT TOP 1 intViewAllowed FROM GroupAuthorization WHERE numGroupID=U.numGroupID AND numModuleID=12 AND numPageID=2),0) AS tintProjectListRights,
		ISNULL((SELECT TOP 1 numCriteria FROM TicklerListFilterConfiguration WHERE CHARINDEX(CONCAT(',',U.numUserDetailID,','),CONCAT(',',vcSelectedEmployee,',')) > 0),0) AS tintActionItemRights,
		ISNULL(D.numAuthorizePercentage,0) AS numAuthorizePercentage,
		ISNULL(D.bitAllowDuplicateLineItems,0) bitAllowDuplicateLineItems,
		ISNULL(D.bitLogoAppliedToBizTheme,0) bitLogoAppliedToBizTheme,
		ISNULL(D.vcLogoForBizTheme,'') vcLogoForBizTheme,
		ISNULL(D.bitLogoAppliedToLoginBizTheme,0) bitLogoAppliedToLoginBizTheme,
		ISNULL(D.vcLogoForLoginBizTheme,'') vcLogoForLoginBizTheme,
		ISNULL(D.vcThemeClass,'1473b4') vcThemeClass,
		ISNULL(D.vcLoginURL,'') AS vcLoginURL
	FROM 
		UserMaster U                              
	left join ExchangeUserDetails E                              
	on E.numUserID=U.numUserID                              
	left join ImapUserDetails IM                              
	on IM.numUserCntID=U.numUserDetailID          
	Join Domain D                              
	on D.numDomainID=U.numDomainID                                           
	left join DivisionMaster Div                                            
	on D.numDivisionID=Div.numDivisionID                               
	left join CompanyInfo C                              
	on C.numCompanyID=Div.numCompanyID                               
	left join AdditionalContactsInformation A                              
	on  A.numContactID=U.numUserDetailId                            
	Join  Subscribers S                            
	on S.numTargetDomainID=D.numDomainID    
	 OUTER APPLY
	 (
		SELECT  
			TOP 1 REPLACE(SCB.Link,'../','') vcDefaultNavURL
		FROM    
			TabMaster T
		JOIN 
			GroupTabDetails G ON G.numTabId = T.numTabId
		LEFT JOIN
			ShortCutGrpConf SCUC ON G.numTabId=SCUC.numTabId AND SCUC.numDomainId=U.numDomainID AND SCUC.numGroupId=U.numGroupID AND bitDefaultTab=1
		LEFT JOIN
			ShortCutBar SCB
		ON
			SCB.Id=SCUC.numLinkId
		WHERE   
			(T.numDomainID = U.numDomainID OR bitFixed = 1)
			AND G.numGroupID = U.numGroupID
			AND ISNULL(G.[tintType], 0) <> 1
			AND tintTabType =1
			AND T.numTabID NOT IN (2,68)
		ORDER BY 
			SCUC.bitInitialPage DESC
	 )  TempDefaultPage 
	 OUTER APPLY
	 (
		SELECT  
			TOP 1 REPLACE(T.vcURL,'../','') vcDefaultNavURL
		FROM    
			TabMaster T
		JOIN 
			GroupTabDetails G 
		ON 
			G.numTabId = T.numTabId
		WHERE   
			(T.numDomainID = U.numDomainID OR bitFixed = 1)
			AND G.numGroupID = U.numGroupID
			AND ISNULL(G.[tintType], 0) <> 1
			AND tintTabType =1
			AND T.numTabID NOT IN (2,68)
			AND ISNULL(G.bitInitialTab,0) = 1 
			AND ISNULL(bitallowed,0)=1
		ORDER BY 
			G.bitInitialTab DESC
	 ) TEMPDefaultTab                   
	WHERE 1 = (CASE 
		WHEN @vcLinkedinId='N' THEN (CASE WHEN vcEmailID=@UserName and vcPassword=@vcPassword THEN 1 ELSE 0 END)
		ELSE (CASE WHEN U.vcLinkedinId=@vcLinkedinId THEN 1 ELSE 0 END)
		END) and bitActive IN (1,2) AND getutcdate() between dtSubStartDate and dtSubEndDate
             
	SELECT 
		numTerritoryId 
	FROM
		UserTerritory UT                              
	JOIN 
		UserMaster U                              
	ON 
		numUserDetailId =UT.numUserCntID                                         
	WHERE 
		1 = (CASE 
			WHEN @vcLinkedinId='N' THEN (CASE WHEN vcEmailID=@UserName and vcPassword=@vcPassword THEN 1 ELSE 0 END)
			ELSE (CASE WHEN vcLinkedinId=@vcLinkedinId THEN 1 ELSE 0 END)
			END)

END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_CreateSalesFulfillmentOrder')
DROP PROCEDURE dbo.USP_CreateSalesFulfillmentOrder
GO
CREATE PROCEDURE [dbo].[USP_CreateSalesFulfillmentOrder]
    (
	  @numDomainID NUMERIC(9),
      @numOppID NUMERIC(9),
      @numUserCntID NUMERIC(9),
      @numBizDocStatus NUMERIC(9),
      @tintMode AS TINYINT,
	  @numOppAuthBizDocsId as numeric(9)=NULL OUTPUT
    )
AS 
    BEGIN
BEGIN TRY		
        DECLARE @numOppBizDocsId NUMERIC(9);SET @numOppBizDocsId=0
		DECLARE @numPackingSlipBizDocId NUMERIC(9),@numAuthInvoice NUMERIC(9)
		DECLARE @UTCDATE AS DATETIME;SET @UTCDATE=GETUTCDATE()
		
		--Create FulfillmentBizDocs
		DECLARE @numInventoryInvoice NUMERIC(9),@numNonInventoryFOInvoice NUMERIC(9),@numServiceFOInvoice NUMERIC(9)
		
		SELECT @numInventoryInvoice=ISNULL(numBizDocTypeID,0) FROM OpportunityFulfillmentBizDocs WHERE numDomainID=@numDomainID AND ItemType='P'
		SELECT @numNonInventoryFOInvoice=ISNULL(numBizDocTypeID,0) FROM OpportunityFulfillmentBizDocs WHERE numDomainID=@numDomainID AND ItemType='N'
		SELECT @numServiceFOInvoice=ISNULL(numBizDocTypeID,0) FROM OpportunityFulfillmentBizDocs WHERE numDomainID=@numDomainID AND ItemType='S'
		
		--Create Auth BizDocs
		SELECT @numAuthInvoice=isnull(AuthoritativeBizDocs.numAuthoritativeSales,0) From dbo.AuthoritativeBizDocs Where AuthoritativeBizDocs.numDomainId=@numDomainId 
			
		--Create Packing Slip
		SELECT @numPackingSlipBizDocId=ListDetails.numListItemID FROM dbo.ListDetails WHERE ListDetails.vcData='Packing Slip' AND ListDetails.constFlag=1 AND ListDetails.numListID=27
								
		IF @numInventoryInvoice>0
		BEGIN
				EXEC dbo.USP_CreateBizDocs
							@numOppId = @numOppID, --  numeric(9, 0)
							@numBizDocId = @numInventoryInvoice, --  numeric(9, 0)
							@numUserCntID = @numUserCntID, --  numeric(9, 0)
							@numOppBizDocsId =@numOppBizDocsId output,
							@vcComments = '', --  varchar(1000)
							@bitPartialFulfillment = 1, --  bit
							@strBizDocItems = '', --  text
							--@monShipCost = 0, --  DECIMAL(20,5)
							@numShipVia = 0, --  numeric(9, 0)
							@vcTrackingURL = '', --  varchar(1000)
							@dtFromDate = @UTCDATE, --  datetime
							@numBizDocStatus = 0, --  numeric(9, 0)
							@bitRecurringBizDoc = 0, --  bit
							@numSequenceId = '', --  varchar(50)
							@tintDeferred = 0, --  tinyint
							@monCreditAmount =0,
							@ClientTimeZoneOffset = 0, --  int
							@monDealAmount =0,
							@bitRentalBizDoc = 0, --  bit
							@numBizDocTempID = 0, --  numeric(9, 0)
							@vcTrackingNo = '', --  varchar(500)
							--@vcShippingMethod = '', --  varchar(100)
							@vcRefOrderNo = '', --  varchar(100)
							--@dtDeliveryDate = @UTCDATE, --  datetime
							@OMP_SalesTaxPercent = 0, --  float
							@numFromOppBizDocsId = 0, --  numeric(9, 0)
							@bitTakeSequenceId = 1  --bit
			END
			
			IF @numInventoryInvoice != @numNonInventoryFOInvoice AND @numNonInventoryFOInvoice>0
			BEGIN
				EXEC dbo.USP_CreateBizDocs
							@numOppId = @numOppID, --  numeric(9, 0)
							@numBizDocId = @numNonInventoryFOInvoice, --  numeric(9, 0)
							@numUserCntID = @numUserCntID, --  numeric(9, 0)
							@numOppBizDocsId =0,
							@vcComments = '', --  varchar(1000)
							@bitPartialFulfillment = 1, --  bit
							@strBizDocItems = '', --  text
							--@monShipCost = 0, --  DECIMAL(20,5)
							@numShipVia = 0, --  numeric(9, 0)
							@vcTrackingURL = '', --  varchar(1000)
							@dtFromDate = @UTCDATE, --  datetime
							@numBizDocStatus = 0, --  numeric(9, 0)
							@bitRecurringBizDoc = 0, --  bit
							@numSequenceId = '', --  varchar(50)
							@tintDeferred = 0, --  tinyint
							@monCreditAmount =0,
							@ClientTimeZoneOffset = 0, --  int
							@monDealAmount =0,
							@bitRentalBizDoc = 0, --  bit
							@numBizDocTempID = 0, --  numeric(9, 0)
							@vcTrackingNo = '', --  varchar(500)
							--@vcShippingMethod = '', --  varchar(100)
							@vcRefOrderNo = '', --  varchar(100)
							--@dtDeliveryDate = @UTCDATE, --  datetime
							@OMP_SalesTaxPercent = 0, --  float
							@numFromOppBizDocsId = 0, --  numeric(9, 0)
							@bitTakeSequenceId = 1  --bit
				END
				
				IF @numInventoryInvoice != @numServiceFOInvoice AND @numNonInventoryFOInvoice != @numServiceFOInvoice AND @numServiceFOInvoice>0
				BEGIN			
						EXEC dbo.USP_CreateBizDocs
							@numOppId = @numOppID, --  numeric(9, 0)
							@numBizDocId = @numServiceFOInvoice, --  numeric(9, 0)
							@numUserCntID = @numUserCntID, --  numeric(9, 0)
							@numOppBizDocsId =0,
							@vcComments = '', --  varchar(1000)
							@bitPartialFulfillment = 1, --  bit
							@strBizDocItems = '', --  text
							--@monShipCost = 0, --  DECIMAL(20,5)
							@numShipVia = 0, --  numeric(9, 0)
							@vcTrackingURL = '', --  varchar(1000)
							@dtFromDate = @UTCDATE, --  datetime
							@numBizDocStatus = 0, --  numeric(9, 0)
							@bitRecurringBizDoc = 0, --  bit
							@numSequenceId = '', --  varchar(50)
							@tintDeferred = 0, --  tinyint
							@monCreditAmount =0,
							@ClientTimeZoneOffset = 0, --  int
							@monDealAmount =0,
							@bitRentalBizDoc = 0, --  bit
							@numBizDocTempID = 0, --  numeric(9, 0)
							@vcTrackingNo = '', --  varchar(500)
							--@vcShippingMethod = '', --  varchar(100)
							@vcRefOrderNo = '', --  varchar(100)
							--@dtDeliveryDate = @UTCDATE, --  datetime
							@OMP_SalesTaxPercent = 0, --  float
							@numFromOppBizDocsId = 0, --  numeric(9, 0)
							@bitTakeSequenceId = 1  --bit
					END		
							
					IF @tintMode=1
					BEGIN
						--Upadte FulfillmentBizDocs Status ro Release Allocation
						DECLARE @numBizDocStatusReleaseInventory NUMERIC(9);SET @numBizDocStatusReleaseInventory=0
						SELECT @numBizDocStatusReleaseInventory=ISNULL(numBizDocStatus,0) FROM dbo.OpportunityFulfillmentRules WHERE numDomainID=@numDomainID AND numRuleID=3 
					 			
				 		IF ISNULL(@numBizDocStatusReleaseInventory,0)>0
				 		BEGIN
							EXEC dbo.USP_SalesFulfillmentWorkflow
									@numDomainID = @numDomainID, --  numeric(9, 0)
									@numOppID = @numOppID, --  numeric(9, 0)
									@numOppBizDocsId = @numOppBizDocsId, --  numeric(9, 0)
									@numUserCntID = @numUserCntID, --  numeric(9, 0)
									@numBizDocStatus = @numBizDocStatusReleaseInventory --  numeric(9, 0)
							
							EXEC dbo.USP_UpdateSingleFieldValue
									@tintMode = 27, --  tinyint
									@numUpdateRecordID = @numOppBizDocsId, --  numeric(18, 0)
									@numUpdateValueID = @numBizDocStatusReleaseInventory, --  numeric(18, 0)
									@vcText = '', --  text
									@numDomainID = @numDomainID --  numeric(18, 0)
						END	
					END
					ELSE IF @tintMode=2
					BEGIN
							EXEC dbo.USP_SalesFulfillmentWorkflow
									@numDomainID = @numDomainID, --  numeric(9, 0)
									@numOppID = @numOppID, --  numeric(9, 0)
									@numOppBizDocsId = @numOppBizDocsId, --  numeric(9, 0)
									@numUserCntID = @numUserCntID, --  numeric(9, 0)
									@numBizDocStatus = @numBizDocStatus --  numeric(9, 0)
							
							EXEC dbo.USP_UpdateSingleFieldValue
									@tintMode = 27, --  tinyint
									@numUpdateRecordID = @numOppBizDocsId, --  numeric(18, 0)
									@numUpdateValueID = @numBizDocStatus, --  numeric(18, 0)
									@vcText = '', --  text
									@numDomainID = @numDomainID --  numeric(18, 0)
					END
					
					
					EXEC dbo.USP_CreateBizDocs
							@numOppId = @numOppID, --  numeric(9, 0)
							@numBizDocId = @numAuthInvoice, --  numeric(9, 0)
							@numUserCntID = @numUserCntID, --  numeric(9, 0)
							@numOppBizDocsId = @numOppAuthBizDocsId OUTPUT,
							@vcComments = '', --  varchar(1000)
							@bitPartialFulfillment = 1, --  bit
							@strBizDocItems = '', --  text
							--@monShipCost = 0, --  DECIMAL(20,5)
							@numShipVia = 0, --  numeric(9, 0)
							@vcTrackingURL = '', --  varchar(1000)
							@dtFromDate = '2013-1-2 10:55:29.610', --  datetime
							@numBizDocStatus = 0, --  numeric(9, 0)
							@bitRecurringBizDoc = 0, --  bit
							@numSequenceId = '', --  varchar(50)
							@tintDeferred = 0, --  tinyint
							@monCreditAmount =0,
							@ClientTimeZoneOffset = 0, --  int
							@monDealAmount =0,
							@bitRentalBizDoc = 0, --  bit
							@numBizDocTempID = 0, --  numeric(9, 0)
							@vcTrackingNo = '', --  varchar(500)
							--@vcShippingMethod = '', --  varchar(100)
							@vcRefOrderNo = '', --  varchar(100)
							--@dtDeliveryDate = '2013-1-2 10:55:29.611', --  datetime
							@OMP_SalesTaxPercent = 0, --  float
							@numFromOppBizDocsId = @numOppBizDocsId, --  numeric(9, 0)
							@bitTakeSequenceId = 1,  --bit
							@bitAllItems=1 --bit
					
					EXEC dbo.USP_CreateBizDocs
								@numOppId = @numOppID, --  numeric(9, 0)
								@numBizDocId = @numPackingSlipBizDocId, --  numeric(9, 0)
								@numUserCntID = @numUserCntID, --  numeric(9, 0)
								@numOppBizDocsId =0,
								@vcComments = '', --  varchar(1000)
								@bitPartialFulfillment = 1, --  bit
								@strBizDocItems = '', --  text
								--@monShipCost = 0, --  DECIMAL(20,5)
								@numShipVia = 0, --  numeric(9, 0)
								@vcTrackingURL = '', --  varchar(1000)
								@dtFromDate = '2013-1-2 10:56:4.477', --  datetime
								@numBizDocStatus = 0, --  numeric(9, 0)
								@bitRecurringBizDoc = 0, --  bit
								@numSequenceId = '', --  varchar(50)
								@tintDeferred = 0, --  tinyint
								@monCreditAmount =0,
								@ClientTimeZoneOffset = 0, --  int
								@monDealAmount =0,
								@bitRentalBizDoc = 0, --  bit
								@numBizDocTempID = 0, --  numeric(9, 0)
								@vcTrackingNo = '', --  varchar(500)
								--@vcShippingMethod = '', --  varchar(100)
								@vcRefOrderNo = '', --  varchar(100)
								--@dtDeliveryDate = '2013-1-2 10:56:4.477', --  datetime
								@OMP_SalesTaxPercent = 0, --  float
								@numFromOppBizDocsId = @numOppBizDocsId, --  numeric(9, 0)
								@bitTakeSequenceId = 1,  --bit
								@bitAllItems=0 --bit
				
		
			 END TRY
BEGIN CATCH
    DECLARE @ErrorMessage NVARCHAR(4000);
    DECLARE @ErrorSeverity INT;
    DECLARE @ErrorState INT;

    SELECT 
        @ErrorMessage = ERROR_MESSAGE(),
        @ErrorSeverity = ERROR_SEVERITY(),
        @ErrorState = ERROR_STATE();

    -- Use RAISERROR inside the CATCH block to return error
    -- information about the original error that caused
    -- execution to jump to the CATCH block.
    RAISERROR (@ErrorMessage, -- Message text.
               @ErrorSeverity, -- Severity.
               @ErrorState -- State.
               );
END CATCH;
		
    END
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DeleteDomain')
DROP PROCEDURE USP_DeleteDomain
GO
-- exec USP_DeleteDomain 82
-- exec USP_DeleteDomain 153
-- 72,97,103,104,107,110
CREATE PROCEDURE USP_DeleteDomain
@numDomainID NUMERIC(9)
AS 
BEGIN
IF @numDomainID=1
BEGIN
	raiserror('Verry funny.. can''t delete yourself',16,1);
	RETURN ;
END

/*Allow deletion only for suspended subscription*/
IF exists (SELECT * FROM [Subscribers] WHERE bitActive = 1 AND numTargetDomainID = @numDomainID)
BEGIN
	raiserror('Can not DELETE active subscription',16,1);
	RETURN ;
END

BEGIN TRY 
	BEGIN TRANSACTION

			--DELETE FROM [OpportunityAddress] WHERE [numDomainId]=@numDomainID;

			DELETE FROM [General_Journal_Details] WHERE numJournalId IN (SELECT numJournal_Id FROM dbo.General_Journal_Header WHERE numDomainId = @numDomainID)
			DELETE FROM [General_Journal_Header] WHERE [numDomainId]=@numDomainID;


			--Project Management
			---------------------------------------------------------
			DELETE FROM ProjectProgress WHERE numDomainId=@numDomainID
			 DELETE FROM [ProjectsOpportunities] WHERE [numDomainId]=@numDomainID
			 delete RECENTITEMS where numRecordID IN (SELECT [numProId] FROM [ProjectsMaster] WHERE numDomainID = @numDomainID) and chrRecordType='P'       
			 delete from ProjectsStageDetails where numProId IN (SELECT [numProId] FROM [ProjectsMaster] WHERE numDomainID = @numDomainID)        
			 delete from ProjectsSubStageDetails where numProId IN (SELECT [numProId] FROM [ProjectsMaster] WHERE numDomainID = @numDomainID)              
			 delete from TimeAndExpense where numProId IN (SELECT [numProId] FROM [ProjectsMaster] WHERE numDomainID = @numDomainID)        
			 delete from ProjectsDependency where numProId IN (SELECT [numProId] FROM [ProjectsMaster] WHERE numDomainID = @numDomainID)        
			 delete from ProjectsContacts where numProId IN (SELECT [numProId] FROM [ProjectsMaster] WHERE numDomainID = @numDomainID)         
			 delete from ProjectsMaster WHERE numDomainID = @numDomainID  


			--Case Management
			---------------------------------------------------------
			 DELETE FROM [CaseOpportunities] WHERE [numDomainId] =@numDomainID
			 delete recentItems where numRecordID IN ( SELECT numCaseID FROM Cases WHERE numDomainID=@numDomainID) and chrRecordType = 'S'
			 delete from TimeAndExpense where  numCaseId IN ( SELECT numCaseID FROM Cases WHERE numDomainID=@numDomainID) and numDomainID=   @numDomainID  
			 delete from CaseContacts where numCaseID IN ( SELECT numCaseID FROM Cases WHERE numDomainID=@numDomainID)      
			 delete from CaseSolutions where numCaseID IN ( SELECT numCaseID FROM Cases WHERE numDomainID=@numDomainID)      
			 DELETE from Cases where numDomainID=@numDomainID

			--Contract 
			---------------------------------------------------------

			DELETE FROM [ContractsContact] WHERE [numDomainId]=@numDomainID
			delete contractManagement where numDomainID=@numDomainID

			--Solution/KnowledgeBase
			---------------------------------------------------------
			delete from CaseSolutions where [numSolnID] IN ( SELECT numSolnID FROM [SolutionMaster] WHERE numDomainID=@numDomainID)      
			DELETE FROM [SolutionMaster] WHERE [numDomainID]=@numDomainID


			--Document Management
			---------------------------------------------------------
			DELETE FROM 	DocumentWorkflow	 WHERE [numContactID] IN (SELECT [numContactId] FROM [AdditionalContactsInformation] WHERE [numDomainID]=@numDomainID)
			DELETE FROM [GenericDocuments] WHERE [numDomainID]=@numDomainID
			delete from SpecificDocuments where [numDomainID]=@numDomainID

			--Outlook
			---------------------------------------------------------
			DELETE FROM 	ProfileEGroupDTL	 WHERE [numContactID] IN (SELECT [numContactId] FROM [AdditionalContactsInformation] WHERE [numDomainID]=@numDomainID)
			DELETE FROM 	ProfileEGroupDTL	 WHERE [numEmailGroupID] IN (SELECT [numEmailGroupID] FROM  ProfileEmailGroup	 WHERE numDomainID = @numDomainID)
			DELETE FROM 	ProfileEmailGroup	 WHERE numDomainID = @numDomainID
			DELETE FROM [Correspondence] WHERE [numDomainID]=@numDomainID /***/
			DELETE FROM [Correspondence] WHERE [numEmailHistoryID] IN(select numEmailHSTRID from emailhistory where [numDomainID] =@numDomainID)
			DELETE FROM [EmailMaster] WHERE [numEmailId] IN (select [numEmailId] from EmailHStrToBCCAndCC where [numEmailHstrID] in(select numEmailHSTRID from emailhistory where [numDomainID] =@numDomainID) )
			DELETE FROM [EmailHstrAttchDtls] WHERE [numEmailHstrID] IN (select numEmailHSTRID from emailhistory where [numDomainID] =@numDomainID)
			delete EmailHStrToBCCAndCC where numEmailHSTRID in(select numEmailHSTRID from emailhistory where [numDomainID] =@numDomainID)
			delete [EmailHistory] where [numDomainID] =@numDomainID

			--Marketing/Campaign Mgmt
			---------------------------------------------------------
			DELETE FROM [CampaignDetails] WHERE [numCampaignId] IN (SELECT [numCampaignId] FROM [CampaignMaster] WHERE [numDomainID]=@numDomainID)
			delete from CampaignMaster where numDomainID= @numDomainID

			delete from  ConECampaignDTL where numECampDTLID in (select numECampDTLId from ECampaignDTLs where numECampID IN (SELECT [numECampaignID] FROM [ECampaign] WHERE [numDomainID]=@numDomainID) )
			delete from  ConECampaign where numECampaignID IN (SELECT [numECampaignID] FROM [ECampaign] WHERE [numDomainID]=@numDomainID)
			DELETE FROM [ECampaignAssignee] WHERE [numDomainID]=@numDomainID
			DELETE FROM [ECampaignDTLs] WHERE [numECampID] IN (SELECT [numECampaignID] FROM [ECampaign] WHERE [numDomainID]=@numDomainID)
			DELETE FROM [ECampaign] WHERE [numDomainID]=@numDomainID
			
			DELETE FROM 	BroadCastDtls	 WHERE [numBroadCastID] IN (SELECT [numBroadCastId] FROM [Broadcast] WHERE [numDomainID]=@numDomainID)
			DELETE FROM 	Broadcast	 WHERE numDomainID = @numDomainID

			--Survey
			---------------------------------------------------------
			 --First Delete All Survey Responses related to this Survey.      
			 DELETE FROM dbo.SurveyTemplate WHERE numDomainId=@numDomainID
			 
			 DELETE FROM SurveyResponse       
			  WHERE numRespondantID IN       
			   (SELECT numRespondantID       
				FROM SurveyRespondentsMaster       
				 WHERE numSurID IN (SELECT [numSurID] FROM [SurveyMaster] WHERE [numDomainID]=@numDomainID) )      
			    
			 --Delete all Survey Resposen for the Current Survey   
			 DELETE FROM  SurveyResponse  
			  WHERE numSurID IN (SELECT [numSurID] FROM [SurveyMaster] WHERE [numDomainID]=@numDomainID)
			  
			 --Then Delete All Survey Respondants related to this Survey.      
			 DELETE FROM  SurveyRespondentsChild  
			  WHERE numSurID IN (SELECT [numSurID] FROM [SurveyMaster] WHERE [numDomainID]=@numDomainID)
			    
			 DELETE FROM  SurveyRespondentsMaster  
			  WHERE numSurID IN (SELECT [numSurID] FROM [SurveyMaster] WHERE [numDomainID]=@numDomainID)    
			  
			 --Deletes the Rules for the Survey Answers    
			 DELETE FROM SurveyWorkflowRules    
			  WHERE numSurID IN (SELECT [numSurID] FROM [SurveyMaster] WHERE [numDomainID]=@numDomainID)
			    
			 --Then Delete All Survey Ans Master related to this Survey.      
			 DELETE FROM SurveyAnsMaster       
			  WHERE numSurID IN (SELECT [numSurID] FROM [SurveyMaster] WHERE [numDomainID]=@numDomainID)
			    
			 --Then Delete All Survey Question Master related to this Survey.      
			 DELETE FROM SurveyQuestionMaster       
			  WHERE numSurID IN (SELECT [numSurID] FROM [SurveyMaster] WHERE [numDomainID]=@numDomainID)
			    
			 --Then Delete The Survey from the Survey Master.      
			 DELETE FROM SurveyMaster       
			  WHERE  numDomainId = @numDomainID  


			--E-Commerce
			---------------------------------------------------------
			DELETE FROM dbo.ReviewDetail WHERE numReviewId IN (SELECT numReviewId FROM dbo.Review WHERE numDomainId = @numDomainID)
			DELETE FROM dbo.Review WHERE numDomainId = @numDomainID
			
			DELETE FROM Ratings WHERE numDomainId = @numDomainID
			
			DELETE FROM 	WebAPIDetail	 WHERE numDomainID = @numDomainID
			DELETE FROM 	WebService	 WHERE numDomainID = @numDomainID
			DELETE FROM 	SiteWiseCategories	 WHERE [numSiteID] IN (SELECT [numSiteID] FROM [Sites] WHERE [numDomainID]=@numDomainID)
			DELETE FROM 	MetaTags	 WHERE [tintMetatagFor]=1 AND [numReferenceID] IN (SELECT numitemcode FROM item WHERE [numDomainID]=@numDomainID)
			DELETE FROM SiteSubscriberDetails WHERE [numSiteID] IN (SELECT [numSiteID] FROM [Sites] WHERE [numDomainID]=@numDomainID)
			DELETE FROM [SiteBreadCrumb] WHERE [numDomainID] = @numDomainID
			DELETE FROM [StyleSheetDetails] WHERE [numCssID] IN (SELECT numCssID FROM   [StyleSheets] WHERE  [numDomainID] = @numDomainID)
			DELETE FROM [StyleSheets] WHERE [numDomainID] = @numDomainID
			DELETE FROM [SiteTemplates] WHERE numSiteID IN (SELECT numSiteID FROM Sites WHERE numDomainID = @numDomainID)
			DELETE FROM [PageElementDetail] WHERE   [numDomainID] = @numDomainID
			DELETE FROM [SiteMenu] WHERE [numDomainID] = @numDomainID
			DELETE FROM [SitePages] WHERE numSiteID IN (SELECT numSiteID FROM Sites WHERE numDomainID = @numDomainID)
			DELETE FROM [SiteCategories] WHERE [numSiteID] IN (SELECT [numSiteID] FROM [Sites] WHERE [numDomainID]=@numDomainID)

			delete from ShippingRules WHERE numDomainID = @numDomainID
			DELETE FROM Sites WHERE numDomainID = @numDomainID
			
			DELETE FROM dbo.PromotionOfferItems WHERE numProId IN (SELECT numProId FROM dbo.PromotionOffer WHERE numDomainId=@numDomainID)
			DELETE FROM dbo.PromotionOfferContacts WHERE numProId IN (SELECT numProId FROM dbo.PromotionOffer WHERE numDomainId=@numDomainID)
			DELETE FROM PromotionOffer WHERE numDomainId=@numDomainID


			--Module-Opportunity,Shipping 
			---------------------------------------------------------
			
			delete from CreditBalanceHistory WHERE numDomainId = @numDomainID
--			where numReturnID IN (SELECT numReturnID FROM dbo.[Returns] WHERE numOppId IN (SELECT [numOppId] FROM [OpportunityMaster] WHERE [numDomainId]=@numDomainID))
--			AND numOppBizDocsId in (select numOppBizDocsId FROM OpportunityBizDocs where numOppId IN (SELECT [numOppId] FROM [OpportunityMaster] WHERE [numDomainId]=@numDomainID))
			DELETE FROM TransactionHistory WHERE numDomainID = @numDomainID
			
			DELETE FROM 	OrderAutoRuleDetails	 WHERE [numRuleID] IN (SELECT [numRuleID] FROM [OrderAutoRule] WHERE [numDomainID]=@numDomainID)
			DELETE FROM 	OrderAutoRule	 WHERE numDomainID = @numDomainID

			DELETE FROM 	InventroyReportDTL	 WHERE numDomainID = @numDomainID
			DELETE FROM 	BizDocStatusApprove	 WHERE numDomainID = @numDomainID
			DELETE FROM 	BizDocApprovalRuleEmployee	 WHERE [numBizDocAppId] IN (SELECT [numBizDocAppId] FROM [BizDocApprovalRule] WHERE [numDomainID]=@numDomainID)
			DELETE FROM 	BizDocApprovalRule	 WHERE numDomainID = @numDomainID
			DELETE FROM 	BizActionDetails	 WHERE [numBizActionId] IN (SELECT [numBizActionId] FROM  BizDocAction	 WHERE numDomainID = @numDomainID)
			DELETE FROM 	BizDocAction	 WHERE numDomainID = @numDomainID
			DELETE FROM 	BizDocComission	 WHERE numDomainID = @numDomainID

			delete from Comments where numStageID in (select numStageDetailsId from StagePercentageDetails	 WHERE numDomainID = @numDomainID)
			delete from StageDependency where numDependantOnID in (select numStageDetailsId from StagePercentageDetails	 WHERE numDomainID = @numDomainID)

			  DELETE FROM 	StagePercentageDetails	 WHERE numDomainID = @numDomainID
			  DELETE FROM 	[SubStageDetails]	 WHERE [numSubStageHdrID] IN (SELECT [numSubStageHdrID] FROM [SubStageHDR]	 WHERE [numProcessId] IN (SELECT [Slp_Id] FROM  Sales_process_List_Master	 WHERE numDomainID = @numDomainID))
			  DELETE FROM 	[SubStageHDR]	 WHERE [numProcessId] IN (SELECT [Slp_Id] FROM  Sales_process_List_Master	 WHERE numDomainID = @numDomainID)


			
			DELETE FROM dbo.EmbeddedCostItems WHERE numDomainID=@numDomainID
			  DELETE FROM dbo.EmbeddedCostDefaults WHERE numDomainID=@numDomainID	
			  DELETE FROM EmbeddedCostConfig WHERE numDomainID=@numDomainID
				DELETE FROM EmbeddedCost WHERE numDomainID=@numDomainID
			  DELETE FROM  ShippingProviderForEComerce WHERE [numDomainID]=@numDomainID 
			  DELETE FROM [ShippingFieldValues] WHERE [numDomainID]=@numDomainID 
			 
			  DELETE FROM [ShippingReportItems] WHERE [numItemCode] IN (SELECT numItemCode FROM Item WHERE numDomainID =@numDomainID)/***/
			  DELETE FROM [ShippingReportItems] WHERE [numShippingReportId] IN (select numShippingReportId from [ShippingReport] WHERE numOppBizDocId in (select numOppBizDocId FROM OpportunityBizDocs where numOppId IN (SELECT [numOppId] FROM [OpportunityMaster] WHERE [numDomainId]=@numDomainID)))

			  DELETE FROM [ShippingBox] WHERE [numShippingReportId] IN (SELECT numShippingReportId FROM [ShippingReport] WHERE [numDomainId] =@numDomainID)
			  DELETE FROM [ShippingBox] WHERE [numShippingReportId] IN (select numShippingReportId from [ShippingReport] WHERE numOppBizDocId in (select numOppBizDocId FROM OpportunityBizDocs where numOppId IN (SELECT [numOppId] FROM [OpportunityMaster] WHERE [numDomainId]=@numDomainID)))
			
			  DELETE FROM [ShippingReport] WHERE [numDomainId] =@numDomainID	/***/
			  DELETE FROM [ShippingReport] WHERE numOppBizDocId in (select numOppBizDocId FROM OpportunityBizDocs where numOppId IN (SELECT [numOppId] FROM [OpportunityMaster] WHERE [numDomainId]=@numDomainID))

			  DELETE FROM SalesTemplateItems WHERE [numDomainID] =@numDomainID /***/
			  DELETE FROM [OpportunitySalesTemplate] WHERE [numDomainID]=@numDomainID
			  DELETE FROM [OpportunityLinking] WHERE [numParentOppBizDocID] IN (SELECT [numOppBizDocsId] FROM [OpportunityMaster] INNER JOIN [OpportunityBizDocs] ON [OpportunityMaster].[numOppId] = [OpportunityBizDocs].[numOppId] WHERE [numDomainId]=@numDomainID)
			  DELETE FROM [OpportunityLinking] WHERE [numParentOppID] IN (SELECT [numOppId] FROM [OpportunityMaster] WHERE [numDomainId]=@numDomainID)
			  DELETE FROM [OpportunityLinking] WHERE [numChildOppID] IN (SELECT [numOppId] FROM [OpportunityMaster] WHERE [numDomainId]=@numDomainID)
			  DELETE FROM [OpportunityLinking] WHERE [numParentProjectID] IN (SELECT [numProId] FROM [ProjectsMaster] WHERE [numDomainId]=@numDomainID)
			  
			  DELETE FROM dbo.WorkOrderDetails WHERE numWOId IN (SELECT numWOId FROM dbo.WorkOrder WHERE numDomainID=@numDomainID)
			  DELETE FROM WorkOrder WHERE numDomainID=@numDomainID
			  
			  DELETE FROM RECENTITEMS where numRecordID in  (SELECT [numOppId] FROM [OpportunityMaster] WHERE [numDomainId]=@numDomainID) and chrRecordType='O'
			      
			 
			  DELETE FROM 	PortalBizDocs	 WHERE numDomainID = @numDomainID
			  DELETE FROM 	PortalWorkflow	 WHERE numDomainID = @numDomainID
			  DELETE FROM 	BizDocAttachments	 WHERE numDomainID = @numDomainID
			  DELETE FROM 	WareHouseForSalesFulfillment	 WHERE numDomainID = @numDomainID
			  DELETE FROM 	OpportunityBizDocsPaymentDetails	 WHERE [numBizDocsPaymentDetId] IN (SELECT [numBizDocsPaymentDetId] FROM [OpportunityBizDocsDetails] WHERE [numDomainId]=@numDomainID)
			  DELETE FROM 	OpportunityBizDocsDetails	 WHERE numDomainID = @numDomainID
			  DELETE FROM OppWarehouseSerializedItem where numOppID IN(SELECT [numOppId] FROM [OpportunityMaster] WHERE [numDomainId]=@numDomainID)
			  DELETE FROM OpportunityItemLinking where numNewOppID IN(SELECT [numOppId] FROM [OpportunityMaster] WHERE [numDomainId]=@numDomainID)
			  DELETE FROM OpportunityAddress  where numOppID IN(SELECT [numOppId] FROM [OpportunityMaster] WHERE [numDomainId]=@numDomainID)
			                      
			  DELETE FROM OpportunityBizDocItems where numOppBizDocID in                        
			  (select numOppBizDocsId from OpportunityBizDocs where numOppId IN (SELECT [numOppId] FROM [OpportunityMaster] WHERE [numDomainId]=@numDomainID))

--			  DELETE FROM OpportunityBizDocTaxItems where numOppBizDocID in                        
--			  (select numOppBizDocsId from OpportunityBizDocs where numOppId IN (SELECT [numOppId] FROM [OpportunityMaster] WHERE [numDomainId]=@numDomainID))
--			
			  DELETE FROM OpportunityItemsTaxItems WHERE numOppId IN (SELECT [numOppId] FROM [OpportunityMaster] WHERE [numDomainId]=@numDomainID)                    
			  DELETE FROM OpportunityMasterTaxItems WHERE numOppId IN (SELECT [numOppId] FROM [OpportunityMaster] WHERE [numDomainId]=@numDomainID)                    

			  DELETE FROM OpportunityBizDocDtl where numBizDocID in                        
			  (select numOppBizDocsId from OpportunityBizDocs where numOppId IN (SELECT [numOppId] FROM [OpportunityMaster] WHERE [numDomainId]=@numDomainID))                        
			  
			  DELETE FROM 	OpportunityRecurring	 WHERE [numOppId] IN (SELECT numOppID FROM [OpportunityMaster] WHERE numDomainID = @numDomainID)
			  DELETE FROM 	RecurringTransactionReport	 WHERE [numRecTranSeedId] IN (SELECT numOppID FROM [OpportunityMaster] WHERE numDomainID = @numDomainID)
			  DELETE FROM 	RecurringTransactionReport	 WHERE [numRecTranOppID] IN (SELECT numOppID FROM [OpportunityMaster] WHERE numDomainID = @numDomainID)
			  --DELETE FROM OpportunityKitItems WHERE [numChildItem] IN (SELECT [numItemCode] FROM item WHERE [numDomainID]=@numDomainID)/***/
			  --DELETE FROM [OpportunityBizDocTaxItems] WHERE [numOppBizDocID] IN (SELECT [numOppBizDocsId] FROM [OpportunityBizDocs] WHERE [numOppId] IN (SELECT [numOppId] FROM [OpportunityMaster] WHERE [numDomainId]=@numDomainID) ) /***/
			  
			  --'OpportunityOrderStatus','OpportunityMasterTaxItems','OpportunityItemsTaxItems'
			  DELETE FROM [OpportunityOrderStatus] WHERE numDomainID = @numDomainID
			  DELETE FROM [OpportunityMasterTaxItems] WHERE [numOppId] IN (SELECT numOppID FROM [OpportunityMaster] WHERE numDomainID = @numDomainID)
			  DELETE FROM [OpportunityItemsTaxItems]  WHERE [numOppItemID] IN (SELECT [numOppItemID] FROM [dbo].[OpportunityItems] WHERE [numOppId] IN (SELECT [numOppId] FROM [OpportunityMaster] WHERE [numDomainId]=@numDomainID) )
			  
			  DELETE FROM OpportunityBizDocKitItems WHERE [numOppBizDocItemID] IN (SELECT [numOppBizDocItemID] FROM [OpportunityBizDocItems] WHERE [numItemCode] IN (SELECT numItemCode FROM item WHERE [numDomainID]=@numDomainID) ) /***/
			  DELETE FROM OpportunityBizDocs where numOppId IN (SELECT [numOppId] FROM [OpportunityMaster] WHERE [numDomainId]=@numDomainID)
			  DELETE FROM OpportunityContact where numOppId IN (SELECT [numOppId] FROM [OpportunityMaster] WHERE [numDomainId]=@numDomainID)
			  DELETE FROM OpportunityDependency where numOpportunityId IN (SELECT [numOppId] FROM [OpportunityMaster] WHERE [numDomainId]=@numDomainID)
			  DELETE FROM 	TimeExpAddAmount	 WHERE [numUserCntID] IN (SELECT [numContactId] FROM [AdditionalContactsInformation] WHERE [numDomainID]=@numDomainID)
			  DELETE FROM TimeAndExpense where numDomainID= @numDomainID
			  --delete from CreditBalanceHistory where numDomainID= @numDomainID 
			   delete from OpportunityKitChildItems where numOppId IN (SELECT [numOppId] FROM [OpportunityMaster] WHERE [numDomainId]=@numDomainID)
			  delete from OpportunityKitItems where numOppId IN (SELECT [numOppId] FROM [OpportunityMaster] WHERE [numDomainId]=@numDomainID)                        
 
			  DELETE FROM [Returns] WHERE [numOppId] IN (SELECT [numOppId] FROM [OpportunityMaster] WHERE [numDomainId]=@numDomainID)
			  DELETE FROM OpportunityItems where numOppId IN (SELECT [numOppId] FROM [OpportunityMaster] WHERE [numDomainId]=@numDomainID)
			  DELETE FROM OpportunityItems where numItemCode IN (SELECT numItemCode FROM item WHERE [numDomainID]=@numDomainID)
			  
			  DELETE FROM OpportunityStageDetails WHERE numDomainId=@numDomainID
			  DELETE FROM OpportunityStageDetails where numOppId IN (SELECT [numOppId] FROM [OpportunityMaster] WHERE [numDomainId]=@numDomainID)
			  DELETE FROM OpportunitySubStageDetails where numOppId IN (SELECT [numOppId] FROM [OpportunityMaster] WHERE [numDomainId]=@numDomainID)
			  DELETE FROM [OpportunityRecurring] WHERE [numOppId] IN (SELECT [numOppId] FROM [OpportunityMaster] WHERE [numDomainId]=@numDomainID)
			  DELETE FROM [OpportunityMasterAPI] WHERE [numOppId] IN (SELECT [numOppId] FROM [OpportunityMaster] WHERE [numDomainId]=@numDomainID)
			  DELETE FROM [OpportunityMaster] WHERE [numDomainId]=@numDomainID;
			  DELETE FROM [OpportunityMaster] WHERE [numDivisionId] IN (SELECT numDivisionID FROM [DivisionMaster] WHERE [numDomainID]=@numDomainID)
			  DELETE FROM 	Sales_process_List_Master	 WHERE numDomainID = @numDomainID

			--Item Management,Pricebook,Category,Group,Assets
			---------------------------------------------------------
			DELETE FROM WebApiOppItemDetails WHERE numDomainId = @numDomainID
			
			DELETE FROM [PricingTable] WHERE [numPriceRuleID] IN (SELECT [numPricRuleID] FROM [PriceBookRules] WHERE [numDomainID]=@numDomainID)
			DELETE FROM [PriceBookRuleDTL] WHERE [numRuleID] IN (SELECT [numPricRuleID] FROM [PriceBookRules] WHERE [numDomainID]=@numDomainID)
			DELETE FROM [PriceBookRuleItems] WHERE [numRuleID] IN (SELECT [numPricRuleID] FROM [PriceBookRules] WHERE [numDomainID]=@numDomainID)
			DELETE FROM [PriceBookRules] WHERE [numDomainID]=@numDomainID         

			DELETE FROM [CompanyAssetSerial] WHERE [numAssetItemID] IN (SELECT [numAItemCode] FROM [CompanyAssets] WHERE [numDomainId]=@numDomainID)
			DELETE FROM [CompanyAssets] WHERE [numDomainId]=@numDomainID

			DELETE FROM [ItemCategory] WHERE [numItemID] IN (SELECT numItemCode FROM item WHERE [numDomainID]=@numDomainID)
			DELETE FROM [Category] WHERE [numDomainID] =@numDomainID

			DELETE FROM [ItemGroupsDTL] WHERE [numItemGroupID] IN (SELECT [numItemGroupID] FROM [ItemGroups] WHERE [numDomainID]=@numDomainID)
			DELETE FROM [ItemGroups] WHERE [numDomainID]=@numDomainID

			
			DELETE FROM [Vendor] WHERE [numDomainID] =@numDomainID
			DELETE FROM [Vendor] WHERE numItemCode IN (SELECT numItemCode FROM item WHERE [numDomainID]=@numDomainID)
			DELETE FROM [ItemDiscountProfile] WHERE [numItemID] IN (SELECT numItemCode FROM item WHERE [numDomainID]=@numDomainID)
			DELETE FROM [ItemExtendedDetails] WHERE [numItemCode] IN (SELECT numItemCode FROM item WHERE [numDomainID]=@numDomainID)/***/
			DELETE FROM [ItemDetails] WHERE [numItemKitID] IN (SELECT numItemCode FROM item WHERE [numDomainID]=@numDomainID)
			DELETE FROM [ItemDetails] WHERE [numChildItemID] IN (SELECT numItemCode FROM item WHERE [numDomainID]=@numDomainID) /***/
			DELETE FROM [ItemOppAccAttrDTL] WHERE [numOptAccAttrID] IN(SELECT [numItemAttrOptAccID] FROM  [ItemOptionAccAttr] WHERE [numDomainID]=@numDomainID)/***/
			DELETE FROM [ItemOptionAccAttr] WHERE [numDomainID]=@numDomainID
			DELETE FROM [ItemTax] WHERE [numItemCode] IN (SELECT numItemCode FROM item WHERE [numDomainID]=@numDomainID) /***/
			DELETE FROM [TaxItems] WHERE [numDomainID]=@numDomainID

			DELETE FROM WareHouseItems_Tracking WHERE numDomainID=@numDomainID

			DELETE FROM warehouseItmsDTL WHERE numWareHouseItemID IN (SELECT numWareHouseItemID FROM WareHouseItems WHERE [numWareHouseID] IN (SELECT [numWareHouseID] FROM [Warehouses] WHERE [numDomainID]=@numDomainID)) /***/
			DELETE FROM [WareHouseDetails] WHERE [numDomainId]=@numDomainID
			delete from WareHouseItems WHERE [numDomainID]=@numDomainID
			DELETE FROM [Warehouses] WHERE [numDomainID]=@numDomainID

			Delete from ItemImages where [numDomainID]=@numDomainID
			delete from item where [numDomainID] = @numDomainID
			DELETE FROM [ItemAPI] WHERE  [numDomainId] = @numDomainID
			DELETE FROM LuceneItemsIndex WHERE [numDomainId] = @numDomainID

			--Administration (custom fiels,master lists,alerts)
			---------------------------------------------------------
			DELETE FROM 	FieldRelationshipDTL	 WHERE [numFieldRelID] IN (SELECT [numFieldRelID] FROM [FieldRelationship] WHERE [numDomainID] = @numDomainID)
			DELETE FROM 	FieldRelationship	 WHERE numDomainID = @numDomainID
			DELETE FROM 	Currency	 WHERE numDomainID = CASE @numDomainID WHEN 0 THEN -1 ELSE @numDomainID END 
			delete from VendorShipmentMethod where numDomainID = @numDomainID
			DELETE FROM 	ListMaster	 WHERE numDomainID = @numDomainID
			DELETE FROM 	ListDetails	 WHERE numDomainID = @numDomainID
			DELETE FROM 	TaxDetails	 WHERE numDomainID = @numDomainID
			DELETE FROM 	ExceptionLog	 WHERE numDomainID = @numDomainID
			DELETE FROM 	PaymentGatewayDTLID	 WHERE numDomainID = @numDomainID
			--DELETE FROM 	DomainWiseSort	 WHERE numDomainID = @numDomainID
			DELETE FROM 	ShortCutBar	 WHERE numDomainID = @numDomainID
			DELETE FROM 	TabDefault	 WHERE numDomainID = @numDomainID
			DELETE FROM 	AuthoritativeBizDocs	 WHERE numDomainID = @numDomainID
			DELETE FROM 	TabMaster	 WHERE numDomainID = @numDomainID
			DELETE FROM 	ListOrder	 WHERE numDomainID = @numDomainID
			DELETE FROM 	InboxTree	 WHERE numDomainID = @numDomainID
			DELETE FROM 	ImapUserDetails	 WHERE numDomainID = @numDomainID
			DELETE FROM		NameTemplate WHERE numDomainID = CASE @numDomainID WHEN 0 THEN -1 ELSE @numDomainID END

			DELETE FROM 	ShortCutGrpConf	 WHERE numDomainID = @numDomainID			
			DELETE FROM 	ShortCutUsrCnf	 WHERE numDomainID = @numDomainID
			DELETE FROM 	DefaultTabs	 WHERE numDomainID = @numDomainID
			DELETE FROM 	GroupTabDetails	 WHERE [numGroupId] IN (SELECT [numGroupID] FROM AuthenticationGroupMaster WHERE [numDomainID]=@numDomainID )
			
		
			
			DELETE FROM 	RecImportConfg	 WHERE numDomainID = @numDomainID
			DELETE FROM 	PageLayoutDTL	 WHERE numDomainID = @numDomainID
			DELETE FROM 	RoutinLeadsValues	 WHERE [numRoutID] IN (SELECT [numRoutID] FROM RoutingLeads	 WHERE numDomainID = @numDomainID)
			DELETE FROM 	RoutingLeadDetails	 WHERE [numRoutID] IN (SELECT [numRoutID] FROM RoutingLeads	 WHERE numDomainID = @numDomainID)
			DELETE FROM 	RoutingLeads	 WHERE numDomainID = @numDomainID
			
			DELETE FROM 	SavedSearch WHERE numDomainID=@numDomainID
			DELETE FROM 	AdvSearchCriteria WHERE numDomainID =@numDomainID
			DELETE FROM 	AdvSerViewConf	 WHERE numDomainID = @numDomainID
			DELETE FROM 	UserRoles	 WHERE [numUserCntID] IN (SELECT [numContactId] FROM [AdditionalContactsInformation] WHERE [numDomainID]=@numDomainID)
			DELETE FROM 	UserTeams	 WHERE numDomainID = @numDomainID
			DELETE FROM 	UserTerritory	 WHERE [numDomainID]= @numDomainID
			DELETE FROM 	State	 WHERE numDomainID = @numDomainID
			--DELETE FROM 	HomePage	 WHERE numDomainID = @numDomainID
			DELETE FROM [Favorites] WHERE [numContactID] IN (SELECT [numContactId] FROM [AdditionalContactsInformation] WHERE [numDomainID]=@numDomainID)
			DELETE FROM Favorites	WHERE [numUserCntID] IN (SELECT [numContactId] FROM [AdditionalContactsInformation] WHERE [numDomainID]=@numDomainID)
			DELETE FROM HTMLFormURL WHERE numDomainId=@numDomainID
			
			DELETE FROM 	CFW_FLD_Values	 WHERE [Fld_ID] IN (SELECT [Fld_id] FROM CFW_Fld_Master	 WHERE numDomainID = @numDomainID)
			DELETE FROM 	CFW_Fld_Values_Attributes	 WHERE [Fld_ID] IN (SELECT [Fld_id] FROM CFW_Fld_Master	 WHERE numDomainID = @numDomainID)
			DELETE FROM 	CFW_FLD_Values_Case	 WHERE [Fld_ID] IN (SELECT [Fld_id] FROM CFW_Fld_Master	 WHERE numDomainID = @numDomainID)
			DELETE FROM 	CFW_FLD_Values_Cont	 WHERE [Fld_ID] IN (SELECT [Fld_id] FROM CFW_Fld_Master	 WHERE numDomainID = @numDomainID)
			DELETE FROM 	CFW_FLD_Values_Item	 WHERE [Fld_ID] IN (SELECT [Fld_id] FROM CFW_Fld_Master	 WHERE numDomainID = @numDomainID)
			DELETE FROM 	CFW_Fld_Values_Opp	 WHERE [Fld_ID] IN (SELECT [Fld_id] FROM CFW_Fld_Master	 WHERE numDomainID = @numDomainID)
			DELETE FROM 	CFW_Fld_Values_OppItems WHERE [Fld_ID] IN (SELECT [Fld_id] FROM CFW_Fld_Master	 WHERE numDomainID = @numDomainID)
			DELETE FROM 	CFW_FLD_Values_Pro	 WHERE [Fld_ID] IN (SELECT [Fld_id] FROM CFW_Fld_Master	 WHERE numDomainID = @numDomainID)
			DELETE FROM 	CFW_Fld_Values_Product	 WHERE [Fld_ID] IN (SELECT [Fld_id] FROM CFW_Fld_Master	 WHERE numDomainID = @numDomainID)
			DELETE FROM 	CFW_Fld_Values_Serialized_Items	 WHERE [Fld_ID] IN (SELECT [Fld_id] FROM CFW_Fld_Master	 WHERE numDomainID = @numDomainID)
			--DELETE FROM 	InitialListColumnConf	 WHERE numDomainID = @numDomainID
			DELETE FROM 	RelationsDefaultFilter	 WHERE numDomainID = @numDomainID
			DELETE FROM 	DycFrmConfigBizDocsSumm	 WHERE numDomainID = @numDomainID
			DELETE FROM 	DynamicFormMasterParam	 WHERE numDomainID = @numDomainID
			
			DELETE FROM     DycField_Globalization WHERE numDomainID = @numDomainID
			DELETE FROM     DycField_Validation WHERE numDomainID = @numDomainID
			DELETE FROM 	DycFormConfigurationDetails	 WHERE numDomainID = @numDomainID
			DELETE FROM 	DycFormField_Mapping	 WHERE numDomainID = @numDomainID -- deletes custom field
			DELETE FROM		DycFieldMaster WHERE numDomainID = @numDomainID 

			DELETE FROM 	CFw_Grp_Master	 WHERE numDomainID = @numDomainID
			DELETE FROM 	CFW_Fld_Dtl	 WHERE [numFieldId] IN (SELECT [Fld_id] FROM CFW_Fld_Master	 WHERE numDomainID = @numDomainID)
			DELETE FROM 	CFW_Fld_Master	 WHERE numDomainID = @numDomainID

			DELETE TreeNavigationAuthorization WHERE numDomainID = @numDomainID
			
			DELETE FROM 	GroupAuthorization	 WHERE [numGroupID] IN (SELECT [numGroupID] from AuthenticationGroupMaster	 WHERE numDomainID = @numDomainID)
			DELETE FROM 	AuthenticationGroupMaster	 WHERE numDomainID = @numDomainID
			
			DELETE FROM 	AlertEmailDTL	 WHERE numDomainID = @numDomainID
			DELETE FROM 	AlertDomainDtl	 WHERE [numDomainId] = @numDomainID
			DELETE FROM 	AlertContactsAndCompany	 WHERE [numContactID] IN (SELECT numContactId FROM [AdditionalContactsInformation] WHERE [numDomainID]=@numDomainID)
			-- Wil need FK to DOmainID 
			--DELETE FROM 	AlertDTL	 WHERE numDomainID = @numDomainID 
			-- table need column domainID
			DELETE FROM 	BizDocAlerts	 WHERE numDomainID = @numDomainID
			DELETE FROM 	BizDocFilter	 WHERE numDomainID = @numDomainID

			DELETE FROM UOMConversion WHERE numDomainID=@numDomainID
			DELETE FROM UOM WHERE numDomainID = CASE @numDomainID WHEN 0 THEN -1 ELSE @numDomainID END
			
			/*
			SELECT * FROM [AlertHDR]
			SELECT * FROM AlertDTL
			SELECT * FROM [AlertEmailDTL]
			SELECT * FROM [AlertDomainDtl]
			SELECT * FROM [AlertContactsAndCompany]
			SELECT * FROM [AlertEmailMergeFlds]
			SELECT * FROM [BizDocAlerts] 
			*/
			-- Web Analytics,Tracking
			---------------------------------------------------------
			DELETE FROM 	TrackingCampaignData	 WHERE numDomainID = @numDomainID
			DELETE FROM 	TrackingVisitorsDTL	 WHERE [numTracVisitorsHDRID] IN (SELECT [numTrackingID] FROM [TrackingVisitorsHDR] WHERE [numDomainID]=@numDomainID)
			DELETE FROM 	TrackingVisitorsHDR	 WHERE numDomainID = @numDomainID
			

			
			--Action Item, Time & expense,TIckler
			---------------------------------------------------------
			DELETE FROM 	Recurrence	 WHERE  [RecurrenceID] IN (SELECT [RecurrenceID] FROM [Activity] WHERE [ActivityID] IN (SELECT [ActivityID] FROM ActivityResource WHERE [ResourceID] IN (SELECT ResourceID FROM Resource WHERE numDomainID = @numDomainID)) ) 
			--DELETE FROM 	tmpCalandarTbl	 WHERE numDomainID = @numDomainID
			DELETE FROM 	ActivityResource	 WHERE [ResourceID] IN (SELECT ResourceID FROM Resource WHERE numDomainID = @numDomainID)
			DELETE FROM 	ResourcePreference	 WHERE [ResourceID] IN (SELECT [ResourceID] FROM Resource WHERE numDomainID = @numDomainID)
			DELETE FROM 	Resource	 WHERE numDomainID = @numDomainID
			DELETE FROM 	Communication	 WHERE numDomainID = @numDomainID
			DELETE FROM 	Activity	 WHERE [ActivityID] IN (SELECT [ActivityID] FROM ActivityResource WHERE [ResourceID] IN (SELECT ResourceID FROM Resource WHERE numDomainID = @numDomainID))
		

			DELETE FROM 	TimeAndExpense	 WHERE numDomainID = @numDomainID
			DELETE FROM 	tblActionItemData	 WHERE numDomainID = @numDomainID
			DELETE FROM 	FollowUpHistory	 WHERE numDomainID = @numDomainID
			DELETE FROM 	Forecast	 WHERE numDomainID = @numDomainID


			--Reports
			---------------------------------------------------------
			DELETE FROM     DashboardAllowedReports WHERE [numGrpId] IN (SELECT [numGroupID] FROM AuthenticationGroupMaster WHERE [numDomainID]=@numDomainID )
			DELETE FROM 	MyReportList	 WHERE [numUserCntID] IN (SELECT [numContactId] FROM [AdditionalContactsInformation] WHERE [numDomainID]=@numDomainID)
			DELETE FROM 	ForReportsByTeam	 WHERE numDomainID = @numDomainID
			DELETE FROM 	ForReportsByTerritory	 WHERE numDomainID = @numDomainID
			DELETE FROM 	ForReportsByCampaign	 WHERE [numUserCntID] IN (SELECT [numContactId] FROM [AdditionalContactsInformation] WHERE [numDomainID]=@numDomainID)
			DELETE FROM 	ForReportsByAsItsType	 WHERE numDomainID = @numDomainID
			DELETE FROM 	CustRptSchContacts	 WHERE [numScheduleId] IN (SELECT numScheduleId FROM CustRptScheduler	 WHERE [numReportId] IN (SELECT [numCustomReportID] FROM [CustomReport] WHERE [numDomainID]=@numDomainID))
			DELETE FROM 	CustRptScheduler	 WHERE [numReportId] IN (SELECT [numCustomReportID] FROM [CustomReport] WHERE [numDomainID]=@numDomainID)
			DELETE FROM 	CustReportSummationlist	 WHERE [numCustomReportId] IN (SELECT [numCustomReportID] FROM [CustomReport] WHERE [numDomainID]=@numDomainID)
			DELETE FROM 	CustReportOrderlist	 WHERE [numCustomReportId] IN (SELECT [numCustomReportID] FROM [CustomReport] WHERE [numDomainID]=@numDomainID)
			DELETE FROM 	CustReportFilterlist	 WHERE [numCustomReportId] IN (SELECT [numCustomReportID] FROM [CustomReport] WHERE [numDomainID]=@numDomainID)
			DELETE FROM CustReportFields WHERE [numCustomReportId] IN (SELECT [numCustomReportID] FROM [CustomReport] WHERE [numDomainID]=@numDomainID)
			DELETE FROM [CustomReport] WHERE [numDomainID]=@numDomainID;
			--Accounting
			---------------------------------------------------------
			DELETE FROM dbo.ReturnItems WHERE [numReturnHeaderID] IN ( SELECT [numReturnHeaderID] FROM ReturnHeader WHERE numDomainID = @numDomainID)
			DELETE FROM ReturnPaymentHistory WHERE numReturnHeaderID IN ( SELECT [numReturnHeaderID] FROM ReturnHeader WHERE numDomainID = @numDomainID)
			DELETE FROM	dbo.ReturnHeader WHERE numDomainID = @numDomainID
			
			DELETE FROM BankReconcileMaster WHERE numDomainID = @numDomainID
			
			DELETE FROM dbo.BillDetails WHERE numBillID IN (SELECT numBillID FROM dbo.BillHeader WHERE numDomainID = @numDomainID)
			DELETE FROM dbo.BillHeader WHERE numDomainID = @numDomainID
			
			DELETE FROM dbo.BillPaymentDetails WHERE numBillID IN (SELECT numBillPaymentID FROM BillPaymentHeader WHERE numDomainID = @numDomainID)
			DELETE FROM BillPaymentHeader WHERE numDomainID = @numDomainID
			
			DELETE FROM OnlineBillPayeeDetails WHERE numBankDetailId IN (SELECT numBankDetailID FROM dbo.BankDetails WHERE numDomainID = @numDomainID)
			DELETE FROM BankTransactionMapping WHERE numTransactionID IN (SELECT numTransactionID FROM BankStatementTransactions WHERE numDomainID = @numDomainID)
			DELETE FROM BankStatementTransactions WHERE numDomainID = @numDomainID
			DELETE FROM BankStatementHeader WHERE numDomainID = numDomainID
			DELETE FROM dbo.BankDetails WHERE numDomainID = @numDomainID
								
			DELETE FROM 	CashCreditCardRecurringDetails	 WHERE  [numCashCreditCardId] IN ( SELECT [numCashCreditId] FROM CashCreditCardDetails	 WHERE numDomainID = @numDomainID)
			DELETE FROM 	CashCreditCardDetails	 WHERE numDomainID = @numDomainID
			DELETE FROM 	ProcurementBudgetDetails	 WHERE [numProcurementId] IN (SELECT [numProcurementBudgetId] FROM ProcurementBudgetMaster	 WHERE numDomainID = @numDomainID)
			DELETE FROM 	FixedAssetDetails	 WHERE numDomainID = @numDomainID
			
--			DELETE FROM 	ChecksRecurringDetails	 WHERE [numCheckId] IN (SELECT [numCheckId] FROM [CheckDetails] WHERE [numDomainId]=@numDomainID)
--			DELETE FROM 	CheckDetails	 WHERE numDomainID = @numDomainID
			DELETE FROM dbo.CheckDetails WHERE numCheckHeaderID IN (SELECT numCheckHeaderID FROM dbo.CheckHeader WHERE numDomainID = @numDomainID)
			--DELETE FROM dbo.CheckCompanyDetails	 WHERE numDomainID = @numDomainID
			DELETE FROM dbo.CheckHeader WHERE numDomainID = @numDomainID
						
			DELETE FROM 	MarketBudgetDetails	 WHERE [numMarketId] IN (SELECT [numMarketBudgetId] FROM  MarketBudgetMaster	 WHERE numDomainID = @numDomainID)
			DELETE FROM 	OperationBudgetDetails	 WHERE numDomainID = @numDomainID
			DELETE FROM 	OperationBudgetDetails	 WHERE [numBudgetId] IN (SELECT [numBudgetId] FROM [OperationBudgetMaster] WHERE [numDomainId]=@numDomainID)
			
			DELETE FROM 	DepositeDetails	 WHERE numDepositID IN (SELECT numDepositID FROM dbo.DepositMaster WHERE numDomainID = @numDomainID)
			DELETE FROM 	dbo.DepositMaster WHERE numDomainID = @numDomainID
			
			DELETE FROM 	OperationBudgetMaster	 WHERE numDomainID = @numDomainID
			DELETE FROM 	MarketBudgetMaster	 WHERE numDomainID = @numDomainID
			DELETE FROM 	ProcurementBudgetMaster	 WHERE numDomainID = @numDomainID
			DELETE FROM 	AccountingCharges	 WHERE numDomainID = @numDomainID
			DELETE FROM 	ChartAccountOpening	 WHERE numDomainID = @numDomainID
			DELETE FROM 	RecurringTemplate	 WHERE numDomainID = @numDomainID
			DELETE FROM 	FinancialYear	 WHERE numDomainID = @numDomainID
			DELETE FROM 	AccountTypeDetail	 WHERE numDomainID = @numDomainID
			DELETE FROM 	COARelationships	 WHERE numDomainID = @numDomainID
			DELETE FROM 	COAShippingMapping	 WHERE numDomainID = @numDomainID
			DELETE FROM 	COACreditCardCharge	 WHERE numDomainID = @numDomainID
			DELETE FROM 	ContactTypeMapping	 WHERE numDomainID = @numDomainID
			DELETE FROM 	Chart_Of_Accounts	 WHERE numDomainID = @numDomainID

			--Commission
			---------------------------------------------------------
			DELETE FROM 	CommissionContacts	 WHERE numDomainID = @numDomainID
			DELETE FROM 	CommissionReports	 WHERE numDomainID = @numDomainID
			DELETE FROM 	CommissionRuleContacts WHERE numComRuleID IN (SELECT numComRuleID FROM CommissionRules WHERE numDomainID=@numDomainID)
			DELETE FROM 	CommissionRuleItems WHERE numComRuleID IN (SELECT numComRuleID FROM CommissionRules WHERE numDomainID=@numDomainID)
			DELETE FROM 	CommissionRuleDtl WHERE numComRuleID IN (SELECT numComRuleID FROM CommissionRules WHERE numDomainID=@numDomainID)
			DELETE FROM 	CommissionRules	 WHERE numDomainID = @numDomainID
			

			--Relationship
			---------------------------------------------------------
			 
			DELETE FROM 	DynamicFormAOIConf	 WHERE numDomainID = @numDomainID
			delete  RECENTITEMS  where numRecordID IN (SELECT [numDivisionID] FROM [DivisionMaster] WHERE [numDomainID]=@numDomainID) and chrRecordType='C'
			DELETE FROM CommunicationAttendees WHERE numCommId IN (SELECT numCommId FROM Communication WHERE [numDomainID]=@numDomainID)
			DELETE FROM Communication WHERE [numDomainID]=@numDomainID
			delete from ExtranetAccountsDtl WHERE [numDomainID] =@numDomainID
			delete from ExtranetAccountsDtl WHERE [numExtranetID] IN (SELECT [numExtranetID] FROM  ExtarnetAccounts where [numDomainID]=@numDomainID)
			delete from ExtarnetAccounts where [numDomainID]=@numDomainID
			DELETE FROM [ExtarnetAccounts] WHERE [numDivisionID] IN (SELECT [numDivisionID] FROM [DivisionMaster] WHERE [numDomainID]=@numDomainID)
			delete from CaseContacts WHERE numContactID in (select numContactID from AdditionalContactsInformation where numDivisionID IN (SELECT [numDivisionID] FROM [DivisionMaster] WHERE [numDomainID]=@numDomainID))                     
			delete from ProjectsContacts where numContactID in (select numContactID from AdditionalContactsInformation where numDivisionID  IN (SELECT [numDivisionID] FROM [DivisionMaster] WHERE [numDomainID]=@numDomainID))                   
			delete from OpportunityContact where numContactID in (select numContactID from AdditionalContactsInformation where numDivisionID  IN (SELECT [numDivisionID] FROM [DivisionMaster] WHERE [numDomainID]=@numDomainID))                     
			delete AOIContactLink where [numContactID] IN (SELECT numContactId FROM [AdditionalContactsInformation] WHERE [numDomainID]=@numDomainID)
			delete UserTeams WHERE [numDomainID]=@numDomainID
			delete UserTerritory where  [numDomainID]=@numDomainID
			DELETE FROM CustomerCreditCardInfo	 WHERE numContactID IN (SELECT numContactId FROM [AdditionalContactsInformation] WHERE [numDomainID]=@numDomainID)
			
			DELETE FROM AddressDetails	 WHERE numDomainID=@numDomainID 
			DELETE FROM AdditionalContactsInformation WHERE [numDomainID]=@numDomainID
			DELETE FROM [CompanyAssociations] WHERE [numDomainID]=@numDomainID/***/
			DELETE FROM [Vendor] WHERE [numDomainID] =@numDomainID
			DELETE FROM 	DivisionTaxTypes	 WHERE numDivisionID IN (SELECT numDivisionID FROM [DivisionMaster] WHERE [numDomainID] =@numDomainID)
			
			DELETE FROM [DivisionMaster] WHERE [numDomainID] =@numDomainID
			DELETE FROM [CompanyInfo] WHERE [numDomainID]=@numDomainID


			
	
			
			
			DELETE FROM UserAccessedDTL	 WHERE numDomainID = @numDomainID
			DELETE FROM [UserMaster] WHERE [numDomainID] =@numDomainID; 
			DELETE FROM [AccountTypeDetail] WHERE [numDomainID]=@numDomainID;
			DELETE FROM [ExportDataSettings] WHERE [numDomainID]=@numDomainID;
			DELETE FROM TaxCountryConfi WHERE [numDomainID]=@numDomainID
			Delete from AlertConfig where [numDomainID]=@numDomainID
			
			DELETE FROM [Domain] WHERE [numDomainId]=@numDomainID;
			DELETE FROM 	SubscriberHstr	 WHERE [numTargetDomainID] = @numDomainID
			DELETE FROM 	Subscribers	 WHERE [numTargetDomainID] = @numDomainID


			PRINT 'Domain Deleted successfully.'
/*---------------------------------------------------------------------------------*/

/* Not sure about following table 

DELETE FROM 	EformConfiguration	 WHERE numDomainID = @numDomainID
*/

/*---------------------------------------------------------------------------------*/
	--ROLLBACK
	COMMIT
END TRY 
BEGIN CATCH		
PRINT @@ERROR
PRINT ERROR_MESSAGE() 
    IF (@@TRANCOUNT > 0) BEGIN
        PRINT 'Unexpected error occurred!'
        ROLLBACK TRAN
        RETURN 1
    END
END CATCH	
END
/*
UPDATE [Subscribers] SET [bitActive]=0 WHERE [numTargetDomainID]=70
*/

/****** Object:  StoredProcedure [dbo].[USP_EcommerceSettings]    Script Date: 03/25/2009 16:46:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj              
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_ecommercesettings')
DROP PROCEDURE usp_ecommercesettings
GO
CREATE PROCEDURE [dbo].[USP_EcommerceSettings]              
@numDomainID as numeric(9),              
@vcPaymentGateWay as int ,                
@vcPGWUserId as varchar(100)='' ,                
@vcPGWPassword as varchar(100),              
@bitShowInStock as bit,              
@bitShowQOnHand as bit,
@bitCheckCreditStatus as BIT,
@numDefaultWareHouseID as numeric(9),
@numRelationshipId as numeric(9),
@numProfileId as numeric(9),
@bitHidePriceBeforeLogin	BIT,
@bitAuthOnlyCreditCard BIT=0,
@bitSendMail BIT = 1,
@vcGoogleMerchantID VARCHAR(1000)  = '',
@vcGoogleMerchantKey VARCHAR(1000) = '',
@IsSandbox BIT ,
@numAuthrizedOrderStatus NUMERIC,
@numSiteID numeric(18, 0),
@vcPaypalUserName VARCHAR(50)  = '',
@vcPaypalPassword VARCHAR(50)  = '',
@vcPaypalSignature VARCHAR(500)  = '',
@IsPaypalSandbox BIT,
@bitSkipStep2 BIT = 0,
@bitDisplayCategory BIT = 0,
@bitShowPriceUsingPriceLevel BIT = 0,
@bitEnableSecSorting BIT = 0,
@bitSortPriceMode   BIT=0,
@numPageSize    INT=15,
@numPageVariant  INT=6,
@bitAutoSelectWarehouse BIT =0,
@bitPreSellUp BIT = 0, 
@bitPostSellUp BIT = 0, 
@numDefaultClass NUMERIC(18,0) = 0,
@vcPreSellUp VARCHAR(200)=NULL,
@vcPostSellUp VARCHAR(200)=NULL,
@vcRedirectThankYouUrl VARCHAR(300)=NULL,
@tintPreLoginProceLevel TINYINT = 0,
@bitHideAddtoCart BIT=1,
@bitShowPromoDetailsLink BIT = 1,
@tintWarehouseAvailability TINYINT = 1,
@bitDisplayQtyAvailable BIT = 0,
@vcSalesOrderTabs VARCHAR(200)='',
@vcSalesQuotesTabs VARCHAR(200)='',
@vcItemPurchaseHistoryTabs VARCHAR(200)='',
@vcItemsFrequentlyPurchasedTabs VARCHAR(200)='',
@vcOpenCasesTabs VARCHAR(200)='',
@vcOpenRMATabs VARCHAR(200)='',
@bitSalesOrderTabs BIT=0,
@bitSalesQuotesTabs BIT=0,
@bitItemPurchaseHistoryTabs BIT=0,
@bitItemsFrequentlyPurchasedTabs BIT=0,
@bitOpenCasesTabs BIT=0,
@bitOpenRMATabs BIT=0,
@bitSupportTabs BIT=0,
@vcSupportTabs VARCHAR(200)=''
as              
BEGIN	        
	IF NOT EXISTS(select 'col1' from eCommerceDTL where numDomainID=@numDomainID AND [numSiteId] = @numSiteID)
	BEGIN
		INSERT INTO eCommerceDTL
        (
			numDomainId,
            vcPaymentGateWay,
            vcPGWManagerUId,
            vcPGWManagerPassword,
            bitShowInStock,
            bitShowQOnHand,
            numDefaultWareHouseID,
            bitCheckCreditStatus,
            [numRelationshipId],
            [numProfileId],
            [bitHidePriceBeforeLogin],
            bitAuthOnlyCreditCard,
            bitSendEmail,
            vcGoogleMerchantID,
            vcGoogleMerchantKey,
            IsSandbox ,
            numAuthrizedOrderStatus,
			numSiteId,
			vcPaypalUserName,
			vcPaypalPassword,
			vcPaypalSignature,
			IsPaypalSandbox,
			bitSkipStep2,
			bitDisplayCategory,
			bitShowPriceUsingPriceLevel,
			bitEnableSecSorting,
			bitSortPriceMode,
			numPageSize,
			numPageVariant,
			bitAutoSelectWarehouse,
			[bitPreSellUp],
			[bitPostSellUp],
			numDefaultClass,
			vcPreSellUp,
			vcPostSellUp,
			vcRedirectThankYouUrl,
			tintPreLoginProceLevel,
			bitHideAddtoCart,
			bitShowPromoDetailsLink,
			tintWarehouseAvailability,
			bitDisplayQtyAvailable
		)
		VALUES
		(
			@numDomainID,
            @vcPaymentGateWay,
            @vcPGWUserId,
            @vcPGWPassword,
            @bitShowInStock,
            @bitShowQOnHand,
            @numDefaultWareHouseID,
            @bitCheckCreditStatus,
            @numRelationshipId,
            @numProfileId,
            @bitHidePriceBeforeLogin,
            @bitAuthOnlyCreditCard,
            @bitSendMail,
            @vcGoogleMerchantID,
            @vcGoogleMerchantKey ,
            @IsSandbox,
            @numAuthrizedOrderStatus,
			@numSiteID,
			@vcPaypalUserName,
			@vcPaypalPassword,
			@vcPaypalSignature,
			@IsPaypalSandbox,
			@bitSkipStep2,
			@bitDisplayCategory,
			@bitShowPriceUsingPriceLevel,
			@bitEnableSecSorting,
			@bitSortPriceMode,
			@numPageSize,
			@numPageVariant,
			@bitAutoSelectWarehouse,
			@bitPreSellUp,
			@bitPostSellUp,
			@numDefaultClass,
			@vcPreSellUp,
			@vcPostSellUp,
			@vcRedirectThankYouUrl,
			@tintPreLoginProceLevel,
			@bitHideAddtoCart,
			@bitShowPromoDetailsLink,
			@tintWarehouseAvailability,
			@bitDisplayQtyAvailable
        )

		IF ISNULL((SELECT numDefaultSiteID FROM DOmain WHERE numDomainId=@numDomainID ),0) = 0
		BEGIN
			UPDATE Domain SET numDefaultSiteID=SCOPE_IDENTITY() WHERE numDomainId=@numDomainID
		END
	END
	ELSE
	BEGIN
		UPDATE 
			eCommerceDTL                                   
		SET              
			vcPaymentGateWay=@vcPaymentGateWay,                
			vcPGWManagerUId=@vcPGWUserId ,                
			vcPGWManagerPassword= @vcPGWPassword,              
			bitShowInStock=@bitShowInStock ,               
			bitShowQOnHand=@bitShowQOnHand,        
			bitCheckCreditStatus=@bitCheckCreditStatus ,
			[bitHidePriceBeforeLogin]=@bitHidePriceBeforeLogin,
			bitAuthOnlyCreditCard=@bitAuthOnlyCreditCard,
			bitSendEmail = @bitSendMail,
			vcGoogleMerchantID = @vcGoogleMerchantID ,
			vcGoogleMerchantKey =  @vcGoogleMerchantKey ,
			IsSandbox = @IsSandbox,
			numAuthrizedOrderStatus = @numAuthrizedOrderStatus,
			numSiteId = @numSiteID,
			vcPaypalUserName = @vcPaypalUserName ,
			vcPaypalPassword = @vcPaypalPassword,
			vcPaypalSignature = @vcPaypalSignature,
			IsPaypalSandbox = @IsPaypalSandbox,
			bitSkipStep2 = @bitSkipStep2,
			bitDisplayCategory = @bitDisplayCategory,
			bitShowPriceUsingPriceLevel = @bitShowPriceUsingPriceLevel,
			bitEnableSecSorting = @bitEnableSecSorting,
			bitSortPriceMode=@bitSortPriceMode,
			numPageSize=@numPageSize,
			numPageVariant=@numPageVariant,
			bitAutoSelectWarehouse=@bitAutoSelectWarehouse,
			[bitPreSellUp] = @bitPreSellUp,
			[bitPostSellUp] = @bitPostSellUp,
			vcPreSellUp=@vcPreSellUp,
			vcPostSellUp=@vcPostSellUp,
			vcRedirectThankYouUrl=@vcRedirectThankYouUrl,
			bitHideAddtoCart=@bitHideAddtoCart,
			bitShowPromoDetailsLink=@bitShowPromoDetailsLink,
			tintWarehouseAvailability=@tintWarehouseAvailability,
			bitDisplayQtyAvailable=@bitDisplayQtyAvailable
		WHERE 
			numDomainId=@numDomainID 
			AND [numSiteId] = @numSiteID
		IF(@numDefaultWareHouseID>0)
		BEGIN
			UPDATE 
				eCommerceDTL                                   
			SET 
				numDefaultWareHouseID =@numDefaultWareHouseID
			WHERE 
				numDomainId=@numDomainID 
				AND [numSiteId] = @numSiteID
		END
		IF(@numRelationshipId>0)
		BEGIN
			UPDATE 
				eCommerceDTL                                   
			SET 
				numRelationshipId =@numRelationshipId
			WHERE 
				numDomainId=@numDomainID 
				AND [numSiteId] = @numSiteID
		END
		IF(@numDefaultClass>0)
		BEGIN
			UPDATE 
				eCommerceDTL                                   
			SET 
				numDefaultClass =@numDefaultClass
			WHERE 
				numDomainId=@numDomainID 
				AND [numSiteId] = @numSiteID
		END
		IF(@numProfileId>0)
		BEGIN
			UPDATE 
				eCommerceDTL                                   
			SET 
				numProfileId =@numProfileId
			WHERE 
				numDomainId=@numDomainID 
				AND [numSiteId] = @numSiteID
		END
		IF(@tintPreLoginProceLevel>0)
		BEGIN
			UPDATE 
				eCommerceDTL                                   
			SET 
				tintPreLoginProceLevel =@tintPreLoginProceLevel
			WHERE 
				numDomainId=@numDomainID 
				AND [numSiteId] = @numSiteID
		END

	END

	UPDATE
		Domain
	SET
		vcSalesOrderTabs=@vcSalesOrderTabs,
		vcSalesQuotesTabs=@vcSalesQuotesTabs,
		vcItemPurchaseHistoryTabs=@vcItemPurchaseHistoryTabs,
		vcItemsFrequentlyPurchasedTabs=@vcItemsFrequentlyPurchasedTabs,
		vcOpenCasesTabs=@vcOpenCasesTabs,
		vcOpenRMATabs=@vcOpenRMATabs,
		vcSupportTabs=@vcSupportTabs,
		bitSalesOrderTabs=@bitSalesOrderTabs,
		bitSalesQuotesTabs=@bitSalesQuotesTabs,
		bitItemPurchaseHistoryTabs=@bitItemPurchaseHistoryTabs,
		bitItemsFrequentlyPurchasedTabs=@bitItemsFrequentlyPurchasedTabs,
		bitOpenCasesTabs=@bitOpenCasesTabs,
		bitOpenRMATabs=@bitOpenRMATabs,
		bitSupportTabs=@bitSupportTabs
	WHERE
		numDomainId=@numDomainID
END
GO
/****** Object:  StoredProcedure [dbo].[usp_GeExceltAdvancedSearch]    Script Date: 07/26/2008 16:16:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_geexceltadvancedsearch')
DROP PROCEDURE usp_geexceltadvancedsearch
GO
CREATE PROCEDURE [dbo].[usp_GeExceltAdvancedSearch]  
 @numDomainID numeric,  
 @bitPublicPrivate tinyint=2,  
 @vcFirstName varchar(50)='',  
 @vcLastName varchar(50)='',  
 @vcCompanyName varchar(50)='',  
 @vcDivisionName varchar(50)='',  
 @vcCountry varchar(50)='',  
 @numContactType numeric=0,  
 @numCredit numeric=0,  
 @numCompanyType numeric=0,  
 @numRating numeric=0,  
 @numIndustry numeric=0,  
 @numCloseProb numeric=0,  
 @monOpptAmt DECIMAL(20,5)=0,  
 @tintStage tinyint=0,  
 @numGroup numeric=0,  
 @vcProfile varchar(50)='',  
 @numTeriD numeric=0,  
 @vcAOIList varchar(250)='',  
 @SortChar char(1) ='',  
 @numUserID numeric=0,  
 @numUserTerID numeric=0,  
 @tintRights tinyint=0,  
 @numCustomerID numeric=0,  
 @vcState varchar(50)='',  
 @vcCity varchar(50)=''   --
  
AS  
BEGIN  
 IF @bitPublicPrivate=1  --ONLY PRIVATE RECORDS  
 BEGIN  
  SELECT DISTINCT(VCD.numContactID), VCD.numCompanyID, VCD.numDivisionID,   
   --VCD.vcFirstName, VCD.vcLastName, VCD.vcCompanyName, VCD.vcDivisionName,   
   '<A href="prospects-display.aspx?frm=search&CmpID=' + cast(VCD.numCompanyID as varchar) + '&DivID=' + cast(VCD.numDivisionID as varchar) + '&CRMType=' + cast(VCD.tintCRMType AS varchar) + '&CntID=' + cast(VCD.numContactID AS varchar) + '">' + VCD.vcCompanyName + ' - (<I>' +  VCD.vcDivisionName + '</I>)</A>' AS vcCompName,  
   '<A href="contactdetails.aspx?frm=search&CntID=' + cast(VCD.numContactID AS varchar) + '">' + VCD.vcFirstname + ' ' + VCD.vcLastName + '</A>' AS vcContactName,   
   '<a Onclick="window.open(''callemail.aspx?LsEmail=' + VCD.vcEmail + ''',''NewAction'',''height=500,width=700,scrollbars=Auto'');return false;" href="callemail.aspx?Lsemail=nileshl@kaba.com&fromwhere=searchdisplay">' +  VCD.vcEmail + '</A>' AS vcEMail, 
 
   VCD.vcCompanyName as vcCompanyName,  
   VCD.vcDivisionName as vcDivName,  
   VCD.vcEmail  AS vcMail,   
   VCD.vcBillStreet + ',' + ' ' + VCD.vcBillCity + ',' + ' ' + VCD.vcBilState + ',' + ' ' + VCD.vcBillPostCode + ',' + ' ' + VCD.vcBillCountry as BillingAddress,  
   VCD.vcShipStreet + ',' + ' ' + VCD.vcShipCity + ',' + ' ' + VCD.vcShipState + ',' + ' ' + VCD.vcShipPostCode + ',' + ' ' + VCD.vcShipCountry as ShippingAddress,       
   VCD.vcFirstname + ' ' + VCD.vcLastName  AS vcContactName,  
  
   CASE   
    WHEN VCD.numPhone<>'' THEN VCD.numPhone +   
     CASE  
      WHEN VCD.numPhoneExtension<>'' THEN ' (' +  VCD.numPhoneExtension + ')'   
      ELSE ''  
     END  
    ELSE ''  
   END as vcPhone,   
   VCD.tintCRMType, --, OM.numPClosingPercent, OM.monPAmount, OM.tintPStage  
   VCD.numContCreatedBy, VCD.numTerID  
  FROM vwContactDetails VCD, OpportunityMaster OM  
  WHERE VCD.vcCompanyName LIKE  
    CASE   
     WHEN @vcCompanyName<>'' THEN @vcCompanyName + '%'  
     ELSE '%'  
    END   
   AND VCD.vcDivisionName LIKE  
    CASE   
     WHEN @vcDivisionName<>'' THEN @vcDivisionName + '%'  
     ELSE '%'  
    END  
   AND VCD.vcFirstName LIKE   
    CASE   
     WHEN @vcFirstName<>'' THEN @vcFirstName + '%'  
     ELSE '%'  
    END  
   AND VCD.vcLastName LIKE   
    CASE   
     WHEN @vcLastName<>'' THEN @vcLastName + '%'  
     ELSE '%'  
    END  
   AND VCD.vcProfile LIKE   
    CASE   
     WHEN @vcProfile<>'' THEN @vcProfile + '%'  
     ELSE '%'  
    END  
   AND VCD.vcBillCountry LIKE   
    CASE   
     WHEN @vcCountry<>'' THEN @vcCountry + '%'  
     ELSE '%'  
    END  
   AND (OM.numPClosingPercent =   
    CASE   
     WHEN @numCloseProb>0 THEN @numCloseProb  
    END  
    OR  
    OM.numPClosingPercent >=   
     CASE   
      WHEN @numCloseProb=0 THEN 0  
     END  
    )  
   AND (OM.monPAmount =   
    CASE   
     WHEN @monOpptAmt>0 THEN @monOpptAmt  
    END  
    OR  
    OM.monPAmount >=   
     CASE   
      WHEN @monOpptAmt=0 THEN 0  
     END  
    )  
   AND (OM.tintPStage =   
    CASE   
     WHEN @tintStage>0 THEN @tintStage  

    END  

    OR  
    OM.tintPStage >=   
     CASE   
      WHEN @tintStage=0 THEN 0  
     END  
    )  
   AND (VCD.numGrpID =   
    CASE   
     WHEN @numGroup>0 THEN @numGroup  
    END  
    OR  
    VCD.numGrpID >=   
     CASE   
      WHEN @numGroup=0 THEN 0  
     END  
    )  
   AND (VCD.numCompanyRating =   
    CASE   
     WHEN @numRating>0 THEN @numRating  
    END  
    OR  
    VCD.numCompanyRating >=   
     CASE   
      WHEN @numRating=0 THEN 0  
     END  
    )  
   AND (VCD.numCompanyType =   
    CASE   
     WHEN @numCompanyType>0 THEN @numCompanyType  
    END  
    OR  
    VCD.numCompanyType >=   
     CASE   
      WHEN @numCompanyType=0 THEN 0  
     END  
    )  
   AND (VCD.numCompanyIndustry =   
    CASE   
     WHEN @numIndustry>0 THEN @numIndustry  
    END  
    OR  
    VCD.numCompanyIndustry >=   
     CASE   
      WHEN @numIndustry=0 THEN 0  
     END  
    )  
   AND (VCD.numCompanyCredit =   
    CASE   
     WHEN @numCredit>0 THEN @numCredit  
    END  
    OR  
    VCD.numCompanyCredit >=   
     CASE   
      WHEN @numCredit=0 THEN 0  
     END  
    )  
   AND VCD.vcFirstName LIKE   
    CASE  
     WHEN @SortChar<>'' THEN @SortChar + '%'  
     ELSE '%'  
    END  
   AND (VCD.numContactType =   
    CASE   
     WHEN @numContactType>0 THEN @numContactType  
    END  
    OR  
    VCD.numContactType >=   
     CASE   
      WHEN @numContactType=0 THEN 0  
     END  
    )  
   AND VCD.bitPublicFlag = 1  
   AND (VCD.numTerID =   
    CASE   
     WHEN @numTerID>0 THEN @numTerID  
    END  
    OR  
    VCD.numTerID >=   
     CASE   
      WHEN @numTerID=0 THEN 0  
     END  
    )  
   AND (VCD.numTerID=CASE  
     WHEN @tintRights=0 THEN 0  
     WHEN @tintRights=2 THEN @numUserTerID  
          END  
        OR VCD.numTerID>=CASE  
     WHEN @tintRights=3 THEN 0  
     WHEN @tintRights=1 THEN 0  
          END  
    )  
   --Sridhar -- Start  
   AND (VCD.numDivisionID =   
    CASE   
     WHEN @numCustomerID>0 THEN @numCustomerID  
    END  
    OR  
    VCD.numDivisionID >=   
     CASE   
      WHEN @numCustomerID=0 THEN 0  
     END  
    )  
   AND (VCD.vcBillCity LIKE CASE  
     WHEN @vcCity <> '' THEN @vcCity + '%'  
     ELSE '%'  
          END  
        OR VCD.vcShipCity LIKE CASE  
     WHEN @vcCity <> '' THEN @vcCity + '%'  
     ELSE '%'  
          END  
       )  
  
   AND (VCD.vcBilState LIKE CASE  
     WHEN @vcState <> '' THEN @vcState + '%'  
     ELSE '%'  
          END  
        OR VCD.vcShipState LIKE CASE  
     WHEN @vcState <> '' THEN @vcState + '%'  
     ELSE '%'  
          END  
       )  
   --Sridhar -- End  
   AND VCD.numContCreatedBy =@numUserID  
   --AND dbo.fn_IsAOIExists(VCD.vcAOIList, @vcAOIList)=1  
   AND VCD.numDivDomainID=@numDomainID  
   --AND OM.numContactID=VCD.numContactID --By Madhan  
   AND (OM.numContactID =   
    CASE   
     WHEN @monOpptAmt>0 THEN VCD.numContactID  
     WHEN @tintStage>0 THEN VCD.numContactID  
    END  
    OR  
    OM.numContactID >=   
     CASE   
      WHEN @tintStage=0 AND @monOpptAmt=0 THEN 0  
     END  
    )  
   AND (VCD.tintCRMType=1 OR VCD.tintCRMType=2)  
  --ORDER BY VCD.vcCompanyName, VCD.vcDivisionName, VCD.vcFirstName, VCD.vcLastName  
  ORDER BY vcCompName, vcContactName  
 END  
 ELSE  --Public & Private records.  
 BEGIN  
  SELECT DISTINCT(VCD.numContactID), VCD.numCompanyID, VCD.numDivisionID,   
   --VCD.vcFirstName, VCD.vcLastName, VCD.vcCompanyName, VCD.vcDivisionName,   
   '<A href="prospects-display.aspx?frm=search&CmpID=' + cast(VCD.numCompanyID as varchar) + '&DivID=' + cast(VCD.numDivisionID as varchar) + '&CRMType=' + cast(VCD.tintCRMType AS varchar) + '&CntID=' + cast(VCD.numContactID AS varchar) + '">' + VCD.vcCompanyName + ' - (<I>' +  VCD.vcDivisionName + '</I>)</A>' AS vcCompName,  
   '<A href="contactdetails.aspx?frm=search&CntID=' + cast(VCD.numContactID AS varchar) + '">' + VCD.vcFirstname + ' ' + VCD.vcLastName + '</A>' AS vcContactName,   
   '<a Onclick="window.open(''callemail.aspx?LsEmail=' + VCD.vcEmail + ''',''NewAction'',''height=500,width=700,scrollbars=Auto'');return false;" href="callemail.aspx?Lsemail=nileshl@kaba.com&fromwhere=searchdisplay">' +  VCD.vcEmail + '</A>' AS vcEMail, 
 
   VCD.vcCompanyName as vcCompanyName,  
   VCD.vcDivisionName as vcDivName,  
   VCD.vcEmail  AS vcMail,   
   VCD.vcBillStreet + ',' + ' ' + VCD.vcBillCity + ',' + ' ' + VCD.vcBilState + ',' + ' ' + VCD.vcBillPostCode + ',' + ' ' + VCD.vcBillCountry as BillingAddress,  
   VCD.vcShipStreet + ',' + ' ' + VCD.vcShipCity + ',' + ' ' + VCD.vcShipState + ',' + ' ' + VCD.vcShipPostCode + ',' + ' ' + VCD.vcShipCountry as ShippingAddress,       
   VCD.vcFirstname + ' ' + VCD.vcLastName  AS vcContactName,  
  
  
   CASE   
    WHEN VCD.numPhone<>'' THEN VCD.numPhone +   
     CASE  
      WHEN VCD.numPhoneExtension<>'' THEN ' (' +  VCD.numPhoneExtension + ')'   
      ELSE ''  
     END  
    ELSE ''  
   END as vcPhone,   
   VCD.tintCRMType, --, OM.numPClosingPercent, OM.monPAmount, OM.tintPStage  
   VCD.numContCreatedBy, VCD.numTerID  
  FROM vwContactDetails VCD, OpportunityMaster OM  
  WHERE VCD.vcCompanyName LIKE  
    CASE   
     WHEN @vcCompanyName<>'' THEN @vcCompanyName + '%'  
     ELSE '%'  
    END   
   AND VCD.vcDivisionName LIKE  
    CASE   
     WHEN @vcDivisionName<>'' THEN @vcDivisionName + '%'  
     ELSE '%'  
    END  
   AND VCD.vcFirstName LIKE   
    CASE   
     WHEN @vcFirstName<>'' THEN @vcFirstName + '%'  
     ELSE '%'  
    END  
   AND VCD.vcLastName LIKE   
    CASE   
     WHEN @vcLastName<>'' THEN @vcLastName + '%'  
     ELSE '%'  
    END  
   AND VCD.vcProfile LIKE   
    CASE   
     WHEN @vcProfile<>'' THEN @vcProfile + '%'  
     ELSE '%'  
    END  
   AND VCD.vcBillCountry LIKE   
    CASE   
     WHEN @vcCountry<>'' THEN @vcCountry + '%'  
     ELSE '%'  
    END  
   AND (OM.numPClosingPercent =   
    CASE   
     WHEN @numCloseProb>0 THEN @numCloseProb  
    END  
    OR  
    OM.numPClosingPercent >=   
     CASE   
      WHEN @numCloseProb=0 THEN 0  
     END  
    )  
   AND (OM.monPAmount =   
    CASE   
     WHEN @monOpptAmt>0 THEN @monOpptAmt  
    END  
    OR  
    OM.monPAmount >=   
     CASE   
      WHEN @monOpptAmt=0 THEN 0  
     END  
    )  
   AND (OM.tintPStage =   
    CASE   
     WHEN @tintStage>0 THEN @tintStage  
    END  
    OR  
    OM.tintPStage >=   
     CASE   
      WHEN @tintStage=0 THEN 0  
     END  
    )  
   AND (VCD.numGrpID =   
    CASE   
     WHEN @numGroup>0 THEN @numGroup  
    END  
    OR  
    VCD.numGrpID >=   
     CASE   
      WHEN @numGroup=0 THEN 0  
     END  
    )  
   AND (VCD.numCompanyRating =   
    CASE   
     WHEN @numRating>0 THEN @numRating  
    END  
    OR  
    VCD.numCompanyRating >=   
     CASE   
      WHEN @numRating=0 THEN 0  
     END  
    )  
   AND (VCD.numCompanyType =   
    CASE   
     WHEN @numCompanyType>0 THEN @numCompanyType  
    END  
    OR  
    VCD.numCompanyType >=   
     CASE   
      WHEN @numCompanyType=0 THEN 0  
     END  
    )  
   AND (VCD.numCompanyIndustry =   
    CASE   
     WHEN @numIndustry>0 THEN @numIndustry  
    END  
    OR  
    VCD.numCompanyIndustry >=   
     CASE   
      WHEN @numIndustry=0 THEN 0  
     END  
    )  
   AND (VCD.numCompanyCredit =   
    CASE   
     WHEN @numCredit>0 THEN @numCredit  
    END  
    OR  
    VCD.numCompanyCredit >=   
     CASE   
      WHEN @numCredit=0 THEN 0  
     END  
    )  
   AND VCD.vcFirstName LIKE   
    CASE  
     WHEN @SortChar<>'' THEN @SortChar + '%'  
     ELSE '%'  
    END  
   AND (VCD.numContactType =   
    CASE   
     WHEN @numContactType>0 THEN @numContactType  
    END  
    OR  

    VCD.numContactType >=   
     CASE   
      WHEN @numContactType=0 THEN 0  
     END  
    )  
   AND (VCD.bitPublicFlag = 0  
    OR  
    (VCD.bitPublicFlag = 1  
     AND  
     VCD.numContCreatedBy =@numUserID  
    )  
       )  
   AND (VCD.numTerID =   
    CASE   
     WHEN @numTerID>0 THEN @numTerID  
    END  
    OR  
    VCD.numTerID >=   
     CASE   
      WHEN @numTerID=0 THEN 0  
     END  
    )  
   AND (VCD.numTerID=CASE  
     WHEN @tintRights=0 THEN 0  
     WHEN @tintRights=2 THEN @numUserTerID  
          END  
        OR VCD.numTerID>=CASE  
     WHEN @tintRights=3 THEN 0  
     WHEN @tintRights=1 THEN 0  
          END  
    )  
  
   AND (VCD.numContCreatedBy =  
    CASE  
     WHEN @tintRights=1 THEN @numUserID  
    END  
    OR  
    VCD.numContCreatedBy >  
     CASE  
      WHEN @tintRights<>1 THEN 0  
     END  
    )  
  
   --Sridhar -- Start  
   AND (VCD.numDivisionID =   
    CASE   
     WHEN @numCustomerID>0 THEN @numCustomerID  
    END  
    OR  
    VCD.numDivisionID >=   
     CASE   
      WHEN @numCustomerID=0 THEN 0  
     END  
    )  
   AND (VCD.vcBillCity LIKE CASE  
     WHEN @vcCity<>'' THEN @vcCity + '%'  
     ELSE '%'  
          END  
        OR VCD.vcShipCity LIKE CASE  
     WHEN @vcCity <>'' THEN @vcCity + '%'  
     ELSE '%'  
          END  
       )  
  
   AND (VCD.vcBilState LIKE CASE  
     WHEN @vcState<>'' THEN @vcState + '%'  
     ELSE '%'  
          END  
        OR VCD.vcShipState LIKE CASE  
     WHEN @vcState <>'' THEN @vcState + '%'  
     ELSE '%'  
          END  
       )  
   --Sridhar -- End  
  
 --  AND dbo.fn_IsAOIExists(VCD.vcAOIList, @vcAOIList)=1  
   AND VCD.numDivDomainID=@numDomainID  
   --AND OM.numContactID=VCD.numContactID --By Madhan  
   AND (OM.numContactID =   
    CASE   
     WHEN @monOpptAmt>0 THEN VCD.numContactID  
     WHEN @tintStage>0 THEN VCD.numContactID  
    END  
    OR  
    OM.numContactID >=   
     CASE   
      WHEN @tintStage=0 AND @monOpptAmt=0 THEN 0  
     END  
    )  
   AND (VCD.tintCRMType=1 OR VCD.tintCRMType=2)  
  --ORDER BY VCD.vcCompanyName, VCD.vcDivisionName, VCD.vcFirstName, VCD.vcLastName  
  ORDER BY vcCompName, vcContactName  
 END  
  
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetAdminUserForDomain')
DROP PROCEDURE dbo.USP_GetAdminUserForDomain
GO

CREATE PROCEDURE [dbo].[USP_GetAdminUserForDomain]
	@numDomainID NUMERIC
AS 
BEGIN
		SELECT numAdminID FROM Domain WHERE numDomainID=@numDomainID

	END

GO
/****** Object:  StoredProcedure [dbo].[USP_GetChildItemsForKitsAss]    Script Date: 07/26/2008 16:16:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetChildItemsForKitsAss')
DROP PROCEDURE USP_GetChildItemsForKitsAss
GO
CREATE PROCEDURE [dbo].[USP_GetChildItemsForKitsAss]                             
	@numKitId AS NUMERIC(18,0)                            
	,@numWarehouseItemID NUMERIC(18,0)
	,@bitApplyFilter BIT=0
AS                            
BEGIN

	WITH CTE
	(
		ID
		,numItemDetailID
		,numParentID
		,numItemKitID
		,numItemCode
		,vcItemName
		,vcSKU
		,bitKitParent
		,monAverageCost
		,numAssetChartAcntId
		,txtItemDesc
		,numQtyItemsReq
		,numOppChildItemID
		,charItemType
		,ItemType
		,StageLevel
		,monListPrice
		,numBaseUnit
		,numCalculatedQty
		,numIDUOMId,sintOrder
		,numRowNumber
		,RStageLevel
		,numUOMQuantity
	)
	AS
	(
		SELECT 
			CAST(CONCAT('#',ISNULL(Dtl.numItemDetailID,'0'),'#') AS VARCHAR(1000))
			,Dtl.numItemDetailID
			,CAST(0 AS NUMERIC(18,0))
			,convert(NUMERIC(18,0),0)
			,numItemCode
			,vcItemName
			,vcSKU
			,ISNULL(Item.bitKitParent,0)
			,(CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE monAverageCost END)
			,numAssetChartAcntId
			,ISNULL(Dtl.vcItemDesc,txtItemDesc) AS txtItemDesc
			,DTL.numQtyItemsReq
			,0 as numOppChildItemID
			,case when charItemType='P' then CONCAT('Inventory',CASE WHEN ISNULL(item.bitKitParent,0)=1 THEN ' (Kit)' WHEN ISNULL(item.bitAssembly,0)=1 THEN ' (Assembly)' WHEN ISNULL(item.numItemGroup,0) > 0 AND (ISNULL(item.bitMatrix,0)=1 OR EXISTS (SELECT numItemGroupDTL FROM ItemGroupsDTL WHERE numItemGroupID=item.numItemGroup AND tintType=2)) THEN ' (Matrix)' ELSE '' END) when charItemType='S' then 'Service' when charItemType='A' then 'Accessory' when charItemType='N' then 'Non-Inventory' end as charItemType
			,charItemType as ItemType                            
			,1
			,Item.monListPrice
			,ISNULL(numBaseUnit,0)
			,CAST(DTL.numQtyItemsReq AS FLOAT) AS numCalculatedQty
			,ISNULL(Dtl.numUOMId,ISNULL(numBaseUnit,0)) AS numIDUOMId
			,ISNULL(sintOrder,0) sintOrder
			,ROW_NUMBER() OVER (ORDER BY ISNULL(sintOrder,0)) AS numRowNumber
			,0
			,(DTL.numQtyItemsReq * ISNULL(dbo.fn_UOMConversion(DTL.numUOMID,item.numItemCode,item.numDomainID,item.numBaseUnit),1))
		FROM 
			Item                                
		INNER JOIN 
			ItemDetails Dtl 
		ON 
			numChildItemID=numItemCode
		WHERE 
			numItemKitID=@numKitId 
		UNION ALL
		SELECT 
			CAST(CONCAT(c.ID,'-#',ISNULL(Dtl.numItemDetailID,'0'),'#') AS VARCHAR(1000))
			,Dtl.numItemDetailID
			,C.numItemDetailID
			,Dtl.numItemKitID
			,i.numItemCode
			,i.vcItemName
			,i.vcSKU
			,ISNULL(i.bitKitParent,0)
			,(CASE WHEN ISNULL(i.bitVirtualInventory,0) = 1 THEN 0 ELSE i.monAverageCost END)
			,i.numAssetChartAcntId
			,ISNULL(Dtl.vcItemDesc,i.txtItemDesc) AS txtItemDesc
			,DTL.numQtyItemsReq
			,0 as numOppChildItemID
			,CASE WHEN i.charItemType='P' THEN CONCAT('Inventory',CASE WHEN ISNULL(i.bitKitParent,0)=1 THEN ' (Kit)' WHEN ISNULL(i.bitAssembly,0)=1 THEN ' (Assembly)' WHEN ISNULL(i.numItemGroup,0) > 0 AND (ISNULL(i.bitMatrix,0)=1 OR EXISTS (SELECT numItemGroupDTL FROM ItemGroupsDTL WHERE numItemGroupID=i.numItemGroup AND tintType=2)) THEN ' (Matrix)' ELSE '' END) when i.charItemType='S' then 'Service' when i.charItemType='A' then 'Accessory' when i.charItemType='N' then 'Non-Inventory' end as charItemType
			,i.charItemType as ItemType                            
			,c.StageLevel + 1
			,i.monListPrice,ISNULL(i.numBaseUnit,0),CAST((DTL.numQtyItemsReq * c.numCalculatedQty) AS FLOAT) AS numCalculatedQty
			,ISNULL(Dtl.numUOMId,ISNULL(i.numBaseUnit,0)) AS numIDUOMId
			,ISNULL(Dtl.sintOrder,0) sintOrder
			,ROW_NUMBER() OVER (ORDER BY ISNULL(Dtl.sintOrder,0)) AS numRowNumber
			,0
			,(DTL.numQtyItemsReq * ISNULL(dbo.fn_UOMConversion(DTL.numUOMID,i.numItemCode,i.numDomainID,i.numBaseUnit),1))
		FROM
			Item i                               
		INNER JOIN 
			ItemDetails Dtl
		ON 
			Dtl.numChildItemID=i.numItemCode
		INNER JOIN 
			CTE c 
		ON 
			Dtl.numItemKitID = c.numItemCode 
		WHERE 
			Dtl.numChildItemID!=@numKitId
	)

	SELECT * INTO #temp FROM CTE

	;WITH Final AS 
	(
		SELECT 
			*
			,1 AS RStageLevel1 
		FROM 
			#temp 
		WHERE 
			numitemcode NOT IN (SELECT numItemKitID FROM #temp)
		UNION ALL
		SELECT 
			t.*
			,c.RStageLevel1 + 1 AS RStageLevel1 
		FROM 
			#temp t 
		JOIN 
			Final c 
		ON 
			t.numitemcode=c.numItemKitID
	)


	UPDATE 
		t 
	SET 
		t.RStageLevel=f.RStageLevel 
	FROM 
		#temp t,(SELECT numitemcode,numItemKitID,MAX(RStageLevel1) AS RStageLevel FROM Final GROUP BY numitemcode,numItemKitID) f
	WHERE 
		t.numitemcode=f.numitemcode 
		AND t.numItemKitID=f.numItemKitID 


	IF ISNULL(@numWarehouseItemID,0) > 0
	BEGIN
		DECLARE @numWarehouseID NUMERIC(18,0)
		SELECT @numWarehouseID = numWareHouseID FROM WareHouseItems WHERE numWareHouseItemID=@numWarehouseItemID
		IF(@bitApplyFilter=0)
		BEGIN
		IF EXISTS 
		(
			SELECT
				c.numItemCode
			FROM
				#temp c 
			WHERE
				c.ItemType='P'
				AND (SELECT COUNT(*) FROM WareHouseItems WHERE numItemID = c.numItemCode AND numWareHouseID=@numWarehouseID) = 0
		)
		BEGIN
			RAISERROR('SELECTED_WAREHOUSE_IS_NOT_AVAILABLE_FOR_ALL_CHILD_ITEMS',16,1)
			RETURN
		END
		END

		SELECT DISTINCT 
			c.*
			,CASE WHEN isnull(WI.numWareHouseItemID,0)>0 THEN ISNULL(WI.[monWListPrice],0) ELSE c.monListPrice END monListPrice
			,CONVERT(VARCHAR(30),CASE WHEN isnull(WI.numWareHouseItemID,0)>0 THEN ISNULL(WI.[monWListPrice],0) ELSE c.monListPrice END)+'/'+isnull(UOM.vcUnitName,'Units') as UnitPrice
			,UOM.vcUnitName
			,ISNULL(UOM.numUOMId,0) numUOMId
			,WI.numItemID,vcWareHouse
			,IDUOM.vcUnitName AS vcIDUnitName
			,dbo.fn_UOMConversion(IDUOM.numUOMId,0,IDUOM.numDomainId,UOM.numUOMId) AS UOMConversionFactor
			,CAST(c.numQtyItemsReq * dbo.fn_UOMConversion(UOM.numUOMId,0,IDUOM.numDomainId,IDUOM.numUOMId) AS FLOAT) AS numConQty
			,(c.numQtyItemsReq * dbo.fn_UOMConversion(UOM.numUOMId,0,IDUOM.numDomainId,IDUOM.numUOMId)) * c.monAverageCost AS monAverageCostTotalForQtyRequired
			,c.sintOrder
			,ISNULL(WI.[numWareHouseItemID],0) AS numWarehouseItmsID
			,ISNULL(WI.[numWareHouseItemID],0) AS numWareHouseItemID
			,ISNULL(numOnHand,0) numOnHand
			,ISNULL(numOnHand,0) + isnull(numAllocation,0) AS numAvailable
			,isnull(numOnOrder,0) numOnOrder
			,isnull(numReorder,0) numReorder
			,isnull(numAllocation,0) numAllocation
			,isnull(numBackOrder,0) numBackOrder
			,c.numUOMQuantity
		FROM 
			#temp c  
		LEFT JOIN 
			UOM 
		ON 
			UOM.numUOMId=c.numBaseUnit
		LEFT JOIN 
			UOM IDUOM 
		ON 
			IDUOM.numUOMId=c.numIDUOMId
		OUTER APPLY
		(
			SELECT TOP 1
				numWareHouseItemID
				,numOnHand
				,numOnOrder
				,numReorder
				,numAllocation
				,numBackOrder
				,numItemID
				,vcWareHouse
				,monWListPrice
			FROM
				WareHouseItems
			LEFT JOIN
				Warehouses W 
			ON
				W.numWareHouseID=WareHouseItems.numWareHouseID
			WHERE 
				WareHouseItems.numItemID = c.numItemCode 
				AND WareHouseItems.numWareHouseID=@numWarehouseID
			ORDER BY
				numWareHouseItemID
		) WI
		ORDER BY 
			c.StageLevel
	END
	ELSE IF @bitApplyFilter=1
	BEGIN
		--IF EXISTS 
		--(
		--	SELECT
		--		c.numItemCode
		--	FROM
		--		#temp c 
		--	WHERE
		--		c.ItemType='P'
		--		AND (SELECT COUNT(*) FROM WareHouseItems WHERE numItemID = c.numItemCode) = 0
		--)
		--BEGIN
		--	RAISERROR('SELECTED_WAREHOUSE_IS_NOT_AVAILABLE_FOR_ALL_CHILD_ITEMS',16,1)
		--	RETURN
		--END

		SELECT DISTINCT 
			c.*
			,CASE WHEN isnull(WI.numWareHouseItemID,0)>0 THEN ISNULL(WI.[monWListPrice],0) ELSE c.monListPrice END monListPrice
			,CONVERT(VARCHAR(30),CASE WHEN isnull(WI.numWareHouseItemID,0)>0 THEN ISNULL(WI.[monWListPrice],0) ELSE c.monListPrice END)+'/'+isnull(UOM.vcUnitName,'Units') as UnitPrice
			,UOM.vcUnitName
			,ISNULL(UOM.numUOMId,0) numUOMId
			,WI.numItemID,vcWareHouse
			,IDUOM.vcUnitName AS vcIDUnitName
			,dbo.fn_UOMConversion(IDUOM.numUOMId,0,IDUOM.numDomainId,UOM.numUOMId) AS UOMConversionFactor
			,CAST(c.numQtyItemsReq * dbo.fn_UOMConversion(UOM.numUOMId,0,IDUOM.numDomainId,IDUOM.numUOMId) AS FLOAT) AS numConQty
			,(c.numQtyItemsReq * dbo.fn_UOMConversion(UOM.numUOMId,0,IDUOM.numDomainId,IDUOM.numUOMId)) * c.monAverageCost AS monAverageCostTotalForQtyRequired
			,c.sintOrder
			,ISNULL(WI.[numWareHouseItemID],0) AS numWarehouseItmsID
			,ISNULL(WI.[numWareHouseItemID],0) AS numWareHouseItemID
			,ISNULL(numOnHand,0) numOnHand
			,ISNULL(numOnHand,0) + isnull(numAllocation,0) AS numAvailable
			,isnull(numOnOrder,0) numOnOrder
			,isnull(numReorder,0) numReorder
			,isnull(numAllocation,0) numAllocation
			,isnull(numBackOrder,0) numBackOrder
			,c.numUOMQuantity
		FROM 
			#temp c  
		LEFT JOIN 
			UOM 
		ON 
			UOM.numUOMId=c.numBaseUnit
		LEFT JOIN 
			UOM IDUOM 
		ON 
			IDUOM.numUOMId=c.numIDUOMId
		OUTER APPLY
		(
			SELECT TOP 1
				1 AS numWareHouseItemID
				,SUM(numOnHand) AS numOnHand
				,SUM(numOnOrder) AS numOnOrder
				,0 AS numReorder
				,0 AS numAllocation
				,0 AS numBackOrder
				,0 AS numItemID
				,'' AS vcWareHouse
				,0 AS monWListPrice
			FROM
				WareHouseItems
			LEFT JOIN
				Warehouses W 
			ON
				W.numWareHouseID=WareHouseItems.numWareHouseID
			WHERE 
				WareHouseItems.numItemID = c.numItemCode 
				--AND WareHouseItems.numWareHouseID=@numWarehouseID
			ORDER BY
				numWareHouseItemID
			
		) WI
		ORDER BY 
			c.StageLevel
	END
	ELSE
	BEGIN
		SELECT DISTINCT 
			c.*
			,UOM.vcUnitName
			,ISNULL(UOM.numUOMId,0) numUOMId
			,IDUOM.vcUnitName AS vcIDUnitName
			,dbo.fn_UOMConversion(IDUOM.numUOMId,0,IDUOM.numDomainId,UOM.numUOMId) AS UOMConversionFactor
			,c.numQtyItemsReq * dbo.fn_UOMConversion(UOM.numUOMId,0,IDUOM.numDomainId,IDUOM.numUOMId) AS numConQty
			,(c.numQtyItemsReq * dbo.fn_UOMConversion(UOM.numUOMId,0,IDUOM.numDomainId,IDUOM.numUOMId)) * c.monAverageCost AS monAverageCostTotalForQtyRequired
			,c.sintOrder
			,c.numUOMQuantity
		FROM 
			#temp c  
		LEFT JOIN 
			UOM 
		ON 
			UOM.numUOMId=c.numBaseUnit
		LEFT JOIN 
			UOM IDUOM 
		ON 
			IDUOM.numUOMId=c.numIDUOMId
		ORDER BY 
			c.StageLevel
	END
END
GO
/****** Object:  StoredProcedure [dbo].[USP_GetDealsList1]    Script Date: 05/07/2009 22:12:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getdealslist1')
DROP PROCEDURE usp_getdealslist1
GO
CREATE PROCEDURE [dbo].[USP_GetDealsList1]
               @numUserCntID              NUMERIC(9)  = 0,
               @numDomainID               NUMERIC(9)  = 0,
               @tintSalesUserRightType    TINYINT  = 0,
               @tintPurchaseUserRightType TINYINT  = 0,
               @tintSortOrder             TINYINT  = 4,
               @CurrentPage               INT,
               @PageSize                  INT,
               @TotRecs                   INT  OUTPUT,
               @columnName                AS VARCHAR(300),
               @columnSortOrder           AS VARCHAR(10),
               @numCompanyID              AS NUMERIC(9)  = 0,
               @bitPartner                AS BIT  = 0,
               @ClientTimeZoneOffset      AS INT,
               @tintShipped               AS TINYINT,
               @intOrder                  AS TINYINT,
               @intType                   AS TINYINT,
               @tintFilterBy              AS TINYINT  = 0,
               @numOrderStatus			  AS NUMERIC,
			   @vcRegularSearchCriteria varchar(MAX)='',
			   @vcCustomSearchCriteria varchar(MAX)='',
			   @SearchText VARCHAR(300) = '',
			   @SortChar char(1)='0',
			   @tintDashboardReminderType TINYINT = 0
AS
BEGIN
	DECLARE @PageId  AS TINYINT
	DECLARE @numFormId  AS INT 
	DECLARE @tintDecimalPoints TINYINT

	SELECT
		@tintDecimalPoints=ISNULL(tintDecimalPoints,0)
	FROM
		Domain 
	WHERE 
		numDomainId=@numDomainID

	SET @PageId = 0

	IF @inttype = 1
	BEGIN
		SET @PageId = 2
		SET @inttype = 3
		SET @numFormId=39
	END
	ELSE IF @inttype = 2
	BEGIN
		SET @PageId = 6
		SET @inttype = 4
		SET @numFormId=41
	END

	CREATE TABLE #tempForm 
	(
		tintOrder TINYINT,vcDbColumnName nvarchar(200),vcOrigDbColumnName nvarchar(50),vcFieldName nvarchar(50),vcAssociatedControlType nvarchar(50),vcListItemType varchar(3),
		numListID numeric(9),vcLookBackTableName varchar(50),bitCustomField BIT,numFieldId NUMERIC,bitAllowSorting BIT,bitAllowEdit BIT,bitIsRequired BIT,bitIsEmail BIT,
		bitIsAlphaNumeric BIT,bitIsNumeric BIT,bitIsLengthValidation BIT,intMaxLength int,intMinLength int,bitFieldMessage BIT,vcFieldMessage VARCHAR(500),
		ListRelID numeric(9),intColumnWidth int,bitAllowFiltering BIT,vcFieldDataType CHAR(1)
	)

	-- FIRST CHECK IF GRID COLUMNS ARE CONFIGURED
	DECLARE  @Nocolumns  AS TINYINT = 0

	SELECT 
		@Nocolumns=ISNULL(SUM(TotalRow),0) 
	FROM
	(            
		SELECT 
			COUNT(*) TotalRow 
		FROM 
			View_DynamicColumns 
		WHERE 
			numFormId=@numFormId 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND numRelCntType = @numFormId
		UNION 
		SELECT 
			COUNT(*) TotalRow 
		FROM 
			View_DynamicCustomColumns 
		WHERE 
			numFormId=@numFormId 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND numRelCntType = @numFormId
	) TotalRows

	-- IF GRID COLUMNS ARE NOT CONFIGURES THEN CHECK IF MASTER FORM CONFIGURATION IS CREATED BY ADMINISTRATOR
	DECLARE @IsMasterConfAvailable BIT = 0
	DECLARE @numUserGroup NUMERIC(18,0)

	IF @Nocolumns=0
	BEGIN
		SELECT @numUserGroup = numGroupID FROM UserMaster WHERE numUserDetailId = @numUserCntID 

		IF (SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = @numDomainID AND numFormID = @numFormId AND numRelCntType=@numFormId AND bitGridConfiguration = 1 AND numGroupID = @numUserGroup) > 0
		BEGIN
			SET @IsMasterConfAvailable = 1
		END
	END

	--IF MASTERCONFIGURATION IS AVAILABLE THEN LOAD IT OTHERWISE LOAD DEFAULT COLUMNS
	IF @IsMasterConfAvailable = 1
	BEGIN
		INSERT INTO DycFormConfigurationDetails 
		(
			numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth
		)
		SELECT 
			@numFormId,numFieldId,intColumnNum,intRowNum,@numDomainID,@numUserCntID,@numFormId,1,0,intColumnWidth
		FROM 
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId=@numFormId AND 
			View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicColumnsMasterConfig.numRelCntType=@numFormId AND 
			View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
			ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
		UNION
		SELECT 
			@numFormId,numFieldId,intColumnNum,intRowNum,@numDomainID,@numUserCntID,@numFormId,1,1,intColumnWidth
		FROM 
			View_DynamicCustomColumnsMasterConfig
		WHERE 
			View_DynamicCustomColumnsMasterConfig.numFormId=@numFormId AND 
			View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicCustomColumnsMasterConfig.numRelCntType=@numFormId AND 
			View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1


		INSERT INTO 
			#tempForm
		SELECT 
			(intRowNum + 1) as tintOrder, vcDbColumnName,	vcOrigDbColumnName,	ISNULL(vcCultureFieldName,vcFieldName), vcAssociatedControlType,
			vcListItemType, numListID, vcLookBackTableName,	bitCustom, numFieldId, bitAllowSorting,	bitInlineEdit, bitIsRequired,
			bitIsEmail,	bitIsAlphaNumeric, bitIsNumeric, bitIsLengthValidation,	intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,
			ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType 
		FROM 
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId=@numFormId AND 
			View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicColumnsMasterConfig.numRelCntType=@numFormId AND 
			View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
			ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
		UNION
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName, vcAssociatedControlType,'' as vcListItemType,numListID,'',
			bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,''
		FROM 
			View_DynamicCustomColumnsMasterConfig
		WHERE 
			View_DynamicCustomColumnsMasterConfig.numFormId=@numFormId AND 
			View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicCustomColumnsMasterConfig.numRelCntType=@numFormId AND 
			View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1
		ORDER BY 
			tintOrder ASC  
	END
	ELSE                                        
	BEGIN
		IF @Nocolumns=0
		BEGIN
			INSERT INTO DycFormConfigurationDetails 
			(
				numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth
			)
			SELECT 
				@numFormId,numFieldId,0,Row_number() over(order by tintRow desc),@numDomainID,@numUserCntID,@numFormId,1,0,intColumnWidth
			FROM 
				View_DynamicDefaultColumns
			WHERE 
				numFormId=@numFormId 
				AND bitDefault=1 
				AND ISNULL(bitSettingField,0)=1 
				AND numDomainID=@numDomainID
			ORDER BY 
				tintOrder asc   
		END

		INSERT INTO 
			#tempForm
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcOrigDbColumnName,ISNULL(vcCultureFieldName,vcFieldName),                  
			vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName,bitCustom,numFieldId,bitAllowSorting,bitInlineEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength,bitFieldMessage,
			vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType  
		FROM 
			View_DynamicColumns 
		WHERE 
			numFormId=@numFormId 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND ISNULL(bitSettingField,0)=1 
			AND ISNULL(bitCustom,0)=0  
			AND numRelCntType = @numFormId
		UNION
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName,vcAssociatedControlType,'' as vcListItemType,numListID,'',bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,'' 
		FROM 
			View_DynamicCustomColumns
		WHERE 
			numFormId=@numFormId 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND ISNULL(bitCustom,0)=1 
			AND numRelCntType = @numFormId
		ORDER BY 
			tintOrder asc  
	END

	DECLARE  @strColumns AS VARCHAR(MAX)
	SET @strColumns = ' ADC.numContactId, ADC.vcEmail, Div.numDivisionID,ISNULL(Div.numTerID,0) numTerID, opp.numRecOwner as numRecOwner, Div.tintCRMType, opp.numOppId, ISNULL(Opp.monDealAmount,0) monDealAmount 
	,ISNULL(opp.numShippingService,0) AS numShippingService,ADC.vcFirstName+'' ''+ADC.vcLastName AS vcContactName,ADC.numPhone AS numContactPhone,ADC.numPhoneExtension AS numContactPhoneExtension,ADC.vcEmail AS vcContactEmail,Opp.vcPOppName,cmp.vcCompanyName '
	--Corr.bitIsEmailSent,

	DECLARE  @tintOrder  AS TINYINT;SET @tintOrder = 0
	DECLARE  @vcFieldName  AS VARCHAR(50)
	DECLARE  @vcListItemType  AS VARCHAR(3)
	DECLARE  @vcListItemType1  AS VARCHAR(1)
	DECLARE  @vcAssociatedControlType VARCHAR(30)
	DECLARE  @numListID  AS NUMERIC(9)
	DECLARE  @vcDbColumnName VARCHAR(200)
	DECLARE  @WhereCondition VARCHAR(2000);SET @WhereCondition = ''
	DECLARE  @vcLookBackTableName VARCHAR(2000)
	DECLARE  @bitCustom  AS BIT
	Declare @numFieldId as numeric  
	DECLARE @bitAllowSorting AS CHAR(1)   
	DECLARE @bitAllowEdit AS CHAR(1)                 
	DECLARE @vcColumnName AS VARCHAR(500)          
	
	Declare @ListRelID as numeric(9) 
	DECLARE @SearchQuery AS VARCHAR(MAX) = ''        

	SELECT TOP 1 
		@tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName,@vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,
		@numListID=numListID,@vcLookBackTableName=vcLookBackTableName ,@bitCustom=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,@bitAllowEdit=bitAllowEdit,
		@ListRelID=ListRelID
	FROM 
		#tempForm 
	ORDER BY 
		tintOrder ASC            

	WHILE @tintOrder > 0
    BEGIN
		IF @bitCustom = 0
		BEGIN
			DECLARE  @Prefix  AS VARCHAR(10)

			IF @vcLookBackTableName = 'AdditionalContactsInformation'
				SET @Prefix = 'ADC.'
			IF @vcLookBackTableName = 'DivisionMaster'
				SET @Prefix = 'Div.'
			IF @vcLookBackTableName = 'OpportunityMaster'
				SET @PreFix = 'Opp.'
			IF @vcLookBackTableName = 'OpportunityRecurring'
				SET @PreFix = 'OPR.'
			IF @vcLookBackTableName = 'CompanyInfo'
				SET @PreFix = 'cmp.'
			IF @vcLookBackTableName = 'ProjectProgress'
				SET @PreFix = 'PP.'
			SET @vcColumnName= @vcDbColumnName+'~'+ convert(varchar(10),@numFieldId) +'~0'

			IF @vcAssociatedControlType = 'SelectBox'
			BEGIN
				-- KEEP THIS IF CONDITION SEPERATE FROM IF ELSE
				IF @vcDbColumnName = 'numStatus'
				BEGIN
					SET @strColumns=@strColumns+ ' ,(SELECT COUNT(OpportunityItems.numoppitemtCode) FROM OpportunityItems WHERE OpportunityItems.bitItemPriceApprovalRequired=1 AND OpportunityItems.numOppId=Opp.numOppId) ApprovalMarginCount'
				END


				IF @vcDbColumnName = 'numPartner'
				BEGIN
					SET @strColumns=@strColumns+ ' ,(Opp.numPartner) '+' ['+ @vcColumnName+']'    
					SET @strColumns=@strColumns+ ' ,(SELECT D.vcPartnerCode+''-''+C.vcCompanyName FROM DivisionMaster AS D LEFT JOIN CompanyInfo AS C ON D.numCompanyID=C.numCompanyID WHERE D.numDivisionID=Opp.numPartner) AS vcPartner' 
				END
				ELSE IF @vcDbColumnName = 'vcSignatureType'
				BEGIN
					SET @strColumns=@strColumns+ ' ,(SELECT CASE WHEN vcSignatureType = 0 THEN ''Service Default''
																	WHEN vcSignatureType = 1 THEN ''Adult Signature Required''
																	WHEN vcSignatureType = 2 THEN ''Direct Signature''
																	WHEN vcSignatureType = 3 THEN ''InDirect Signature''
																	WHEN vcSignatureType = 4 THEN ''No Signature Required''
																ELSE '''' END 
					FROM DivisionMaster AS D INNER JOIN DivisionMasterShippingConfiguration AS DMSC ON DMSC.numDivisionID=D.numDivisionID 
					WHERE D.numDivisionID=Div.numDivisionID) '  + ' [' + @vcColumnName + ']'
				END
				ELSE IF @vcDbColumnName = 'numShippingService'
				BEGIN
					SET @strColumns=@strColumns+ ' ,ISNULL((SELECT vcShipmentService FROM ShippingService WHERE (numDomainID=Opp.numDomainID OR ISNULL(numDomainID,0)=0) AND numShipmentServiceID = ISNULL(Opp.numShippingService,0)),'''')' + ' [' + @vcColumnName + ']'
				END
				ELSE IF @vcDbColumnName = 'tintEDIStatus'
				BEGIN
					--SET @strColumns = @strColumns + ',(CASE Opp.tintEDIStatus WHEN 1 THEN ''850 Received'' WHEN 2 THEN ''850 Acknowledged'' WHEN 3 THEN ''940 Sent'' WHEN 4 THEN ''940 Acknowledged'' WHEN 5 THEN ''856 Received'' WHEN 6 THEN ''856 Acknowledged'' WHEN 7 THEN ''856 Sent'' ELSE '''' END)' + ' [' + @vcColumnName + ']'
					SET @strColumns = @strColumns + ',(CASE Opp.tintEDIStatus WHEN 2 THEN ''850 Acknowledged'' WHEN 3 THEN ''940 Sent'' WHEN 4 THEN ''940 Acknowledged'' WHEN 5 THEN ''856 Received'' WHEN 6 THEN ''856 Acknowledged'' WHEN 7 THEN ''856 Sent'' WHEN 11 THEN ''850 Partially Created'' WHEN 12 THEN ''850 SO Created'' ELSE '''' END)' + ' [' + @vcColumnName + ']'
				END
				ELSE IF @vcDbColumnName = 'numPartenerContact'
				BEGIN
					SET @strColumns=@strColumns+ ' ,(Opp.numPartenerContact) '+' ['+ @vcColumnName+']'    
					SET @strColumns=@strColumns+ ' ,(SELECT A.vcFirstName+'' ''+ A.vcLastName FROM AdditionalContactsInformation AS A WHERE A.numContactId=Opp.numPartenerContact) AS vcPartenerContact' 
				END
				ELSE IF @vcDbColumnName = 'tintInvoicing'
				BEGIN
					SET @strColumns=CONCAT(@strColumns,', CONCAT(ISNULL((SELECT SUBSTRING((SELECT ''$^$'' + CONCAT(numOppBizDocsId,''#^#'',vcBizDocID,''#^#'',ISNULL(bitisemailsent,0),''#^#'')
										FROM OpportunityBizDocs WHERE numOppId=Opp.numOppId AND numBizDocId = ISNULL((SELECT TOP 1 numAuthoritativeSales FROM AuthoritativeBizDocs WHERE numDomainId=' + CAST (@numDomainID AS VARCHAR) + '),287) FOR XML PATH('''')),4,200000)),'''')',
										CASE 
										WHEN CHARINDEX('tintInvoicing=3',@vcRegularSearchCriteria) > 0
											THEN ', (SELECT 
													CONCAT(''('',SUM(X.InvoicedQty),'' Out of '',SUM(X.OrderedQty),'')'')
												FROM 
												(
													SELECT
														OI.numoppitemtCode,
														ISNULL(OI.numUnitHour,0) AS OrderedQty,
														ISNULL(TempInvoice.InvoicedQty,0) AS InvoicedQty
													FROM
														OpportunityItems OI
													INNER JOIN
														Item I
													ON
														OI.numItemCode = I.numItemCode
													OUTER APPLY
													(
														SELECT
															SUM(OpportunityBizDocItems.numUnitHour) AS InvoicedQty
														FROM
															OpportunityBizDocs
														INNER JOIN
															OpportunityBizDocItems 
														ON
															OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
														WHERE
															OpportunityBizDocs.numOppId = Opp.numOppId
															AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
															AND ISNULL(OpportunityBizDocs.bitAuthoritativeBizDocs,0) = 1
													) AS TempInvoice
													WHERE
														OI.numOppID = Opp.numOppId
												) X)'

											WHEN CHARINDEX('tintInvoicing=4',@vcRegularSearchCriteria) > 0
											THEN ', (SELECT 
													CONCAT(''('',(X.InvoicedPercentage),'' % '','')'')
												FROM 
												(

													SELECT fltBreakupPercentage  AS InvoicedPercentage
													FROM dbo.OpportunityRecurring OPR 
														INNER JOIN dbo.OpportunityMaster OM ON OPR.numOppID = OM.numOppID
														LEFT JOIN dbo.OpportunityBizDocs OBD ON OBD.numOppBizDocsID = OPR.numOppBizDocID
													WHERE OPR.numOppId = Opp.numOppId 
														and tintRecurringType <> 4
												) X)'

											ELSE 
													','''''
											END,') [', @vcColumnName,']')


				END
				ELSE IF @vcDbColumnName = 'tintSource'
				BEGIN
					SET @strColumns = @strColumns + ',ISNULL(dbo.fn_GetOpportunitySourceValue(ISNULL(Opp.tintSource,0),ISNULL(Opp.tintSourceType,0),Opp.numDomainID),''Internal Order'')' + ' [' + @vcColumnName + ']'

					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'ISNULL(dbo.fn_GetOpportunitySourceValue(ISNULL(Opp.tintSource,0),ISNULL(Opp.tintSourceType,0),Opp.numDomainID),''Internal Order'') LIKE ''%' + @SearchText + '%'''
					END
				END
				ELSE IF @vcDbColumnName = 'numCampainID'
				BEGIN
					SET @strColumns = @strColumns + ', (SELECT vcCampaignName FROM CampaignMaster WHERE numCampaignID= Opp.numCampainID) ' + ' [' + @vcColumnName + ']'
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'ISNULL((SELECT vcCampaignName FROM CampaignMaster WHERE numCampaignID= Opp.numCampainID),'''') LIKE ''%' + @SearchText + '%'''
					END
				END
				ELSE IF @vcDbColumnName = 'numContactID'
				BEGIN
					SET @strColumns = @strColumns + ',dbo.fn_GetContactName(' + @Prefix + @vcDbColumnName + ') [' + @vcColumnName + ']'
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'dbo.fn_GetContactName(' + @Prefix + @vcDbColumnName + ') LIKE ''%' + @SearchText + '%'''
					END
				END
				ELSE IF @vcListItemType = 'LI'
				BEGIN
					SET @strColumns = @strColumns + ',L' + CONVERT(VARCHAR(3),@tintOrder) + '.vcData' + ' [' + @vcColumnName + ']'
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'L' + CONVERT(VARCHAR(3),@tintOrder) + '.vcData LIKE ''%' + @SearchText + '%'''
					END

					SET @WhereCondition = @WhereCondition + ' left Join ListDetails L' + CONVERT(VARCHAR(3),@tintOrder) + ' on L' + CONVERT(VARCHAR(3),@tintOrder) + '.numListItemID=' + @vcDbColumnName
				END
				ELSE IF @vcListItemType = 'S'
				BEGIN
					SET @strColumns = @strColumns + ',S' + CONVERT(VARCHAR(3),@tintOrder) + '.vcState' + ' [' + @vcColumnName + ']'
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'S' + CONVERT(VARCHAR(3),@tintOrder) + '.vcState LIKE ''%' + @SearchText + '%'''
					END

					SET @WhereCondition = @WhereCondition + ' left join State S' + CONVERT(VARCHAR(3),@tintOrder) + ' on S' + CONVERT(VARCHAR(3),@tintOrder) + '.numStateID=' + @vcDbColumnName
				END
				ELSE IF @vcListItemType = 'BP'
				BEGIN
					SET @strColumns = @strColumns + ',SPLM.Slp_Name' + ' [' + @vcColumnName + ']'
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'SPLM.Slp_Name LIKE ''%' + @SearchText + '%'''
					END

					SET @WhereCondition = @WhereCondition + ' LEFT JOIN Sales_process_List_Master SPLM on SPLM.Slp_Id = OPP.numBusinessProcessID '
				END
				ELSE IF @vcListItemType = 'PP'
				BEGIN
					SET @strColumns = @strColumns + ',ISNULL(PP.intTotalProgress,0)' + ' [' + @vcColumnName + ']'

					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'ISNULL(PP.intTotalProgress,0) LIKE ''%' + @SearchText + '%'''
					END
				END
				ELSE IF @vcListItemType = 'T'
				BEGIN
					SET @strColumns = @strColumns + ',L' + CONVERT(VARCHAR(3),@tintOrder) + '.vcData' + ' [' + @vcColumnName + ']'
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'L' + CONVERT(VARCHAR(3),@tintOrder) + '.vcData LIKE ''%' + @SearchText + '%'''
					END

					SET @WhereCondition = @WhereCondition + ' left Join ListDetails L' + CONVERT(VARCHAR(3),@tintOrder) + ' on L' + CONVERT(VARCHAR(3),@tintOrder) + '.numListItemID=' + @vcDbColumnName
				END
				ELSE IF @vcListItemType = 'U'
				BEGIN
					SET @strColumns = @strColumns + ',dbo.fn_GetContactName(' + @Prefix + @vcDbColumnName + ') [' + @vcColumnName + ']'
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'dbo.fn_GetContactName(' + @Prefix + @vcDbColumnName + ') LIKE ''%' + @SearchText + '%'''
					END
				END
				ELSE IF @vcListItemType = 'WI'
				BEGIN
					SET @strColumns = @strColumns + ', Case when isnull(OL.numSiteID,0)>0 then Sites.vcSiteName else WebAPI.vcProviderName end [' + @vcColumnName + ']'
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + '(Case when isnull(OL.numSiteID,0)>0 then Sites.vcSiteName else WebAPI.vcProviderName end) LIKE ''%' + @SearchText + '%'''
					END

					set @WhereCondition= @WhereCondition +' left JOIN OpportunityLinking OL on OL.numChildOppID=Opp.numOppId left join WebAPI on WebAPI.WebApiId=isnull(OL.numWebApiId,0)
													left join Sites on isnull(OL.numSiteID,0)=Sites.numSIteID and Sites.numDomainID=' + convert(varchar(15),@numDomainID)                                                       
				END 
            END
			ELSE IF @vcAssociatedControlType = 'DateField'
			BEGIN
				IF @Prefix ='OPR.'
				BEGIN
					SET @strColumns = @strColumns + ', (SELECT TOP 1 dbo.FormatedDateFromDate( DateAdd(minute,+'+convert(varchar(15),-@ClientTimeZoneOffset)+', dtRecurringDate)'+ ','+ CONVERT(VARCHAR(10),@numDomainId) +') FROM  dbo.OpportunityRecurring WHERE numOppID=OPP.numOppID AND ISNULL(numOppBizDocID,0)=0 and dtRecurringDate > dbo.GetUTCDateWithoutTime()) AS  ['+ @vcColumnName + ']'

					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + '(SELECT TOP 1 dbo.FormatedDateFromDate( DateAdd(minute,+'+convert(varchar(15),-@ClientTimeZoneOffset)+', dtRecurringDate)'+ ','+ CONVERT(VARCHAR(10),@numDomainId) +') FROM  dbo.OpportunityRecurring WHERE numOppID=OPP.numOppID AND ISNULL(numOppBizDocID,0)=0 and dtRecurringDate > dbo.GetUTCDateWithoutTime()) LIKE ''%' + @SearchText + '%'''
					END
				END
				ELSE IF @vcDbColumnName = 'dtReleaseDate'
				BEGIN
					SET @strColumns = @strColumns
                                + ',case when convert(varchar(11),'
                                + @Prefix
                                + @vcDbColumnName
                                + ')= convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',getdate())) then ''<b><font color=red>Today</font></b>'''
					SET @strColumns = @strColumns
									+ 'when convert(varchar(11),'
									+ @Prefix
									+ @vcDbColumnName
									+ ')= convert(varchar(11),dateadd(day,-1,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',getdate()))) then ''<b><font color=purple>Yesterday</font></b>'''
					SET @strColumns = @strColumns
									+ 'when convert(varchar(11),'
									+ @Prefix
									+ @vcDbColumnName
									+ ')= convert(varchar(11),dateadd(day,1,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',getdate()))) then''<b><font color=orange>Tommorow</font></b>'' '
					SET @strColumns = @strColumns
									+ 'else dbo.FormatedDateFromDate('
									+ @Prefix
									+ @vcDbColumnName
									+ ','
									+ CONVERT(VARCHAR(10),@numDomainId)
									+ ') end  ['
									+ @vcColumnName + ']' 	

					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + @Prefix + @vcDbColumnName + ' LIKE ''%' + @SearchText + '%'''
					END

				END
				ELSE
				BEGIN
					SET @strColumns = @strColumns
                                + ',case when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+','
                                + @Prefix
                                + @vcDbColumnName
                                + ') )= convert(varchar(11),getdate()) then ''<b><font color=red>Today</font></b>'''
					SET @strColumns = @strColumns
									+ 'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+','
									+ @Prefix
									+ @vcDbColumnName
									+ ') )= convert(varchar(11),dateadd(day,-1,getdate())) then ''<b><font color=purple>Yesterday</font></b>'''
					SET @strColumns = @strColumns
									+ 'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+','
									+ @Prefix
									+ @vcDbColumnName
									+ ') )= convert(varchar(11),dateadd(day,1,getdate())) then''<b><font color=orange>Tommorow</font></b>'' '
					SET @strColumns = @strColumns
									+ 'else dbo.FormatedDateFromDate('
									+ @Prefix
									+ @vcDbColumnName
									+ ','
									+ CONVERT(VARCHAR(10),@numDomainId)
									+ ') end  ['
									+ @vcColumnName + ']' 	

					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + @Prefix + @vcDbColumnName + ' LIKE ''%' + @SearchText + '%'''
					END
				 END	
            END
            ELSE
            IF @vcAssociatedControlType = 'TextBox'
            BEGIN
				IF @vcDbColumnName = 'vcCompactContactDetails'
				BEGIN
					SET @strColumns=@strColumns+ ' ,'''' AS vcCompactContactDetails'   
				END 
				ELSE
				BEGIN 
                SET @strColumns = @strColumns
                                  + ','
                                  + CASE 
                                      WHEN @vcDbColumnName = 'numAge' THEN 'year(getutcdate())-year(bintDOB)'
                                      WHEN @vcDbColumnName = 'monPAmount' THEN 'isnull((SELECT varCurrSymbol FROM [Currency] WHERE numCurrencyID = Opp.[numCurrencyID]),'''') + '' ''+ CONVERT(VARCHAR,CONVERT(DECIMAL(20,5),monPAmount),1)'
                                      WHEN @vcDbColumnName = 'numDivisionID' THEN 'Div.numDivisionID'
                                      WHEN @vcDbColumnName = 'tintOppStatus' THEN 'CASE tintOppStatus WHEN 0 then ''Open'' WHEN 1 THEN ''Closed-Won'' WHEN 2 THEN ''Closed-Lost'' END '
                                      WHEN @vcDbColumnName = 'tintOppType' THEN 'CASE tintOppType WHEN 1 then ''Sales'' WHEN 2 THEN ''Purchase'' Else ''NA'' END '
									  WHEN @vcDbColumnName = 'vcPOppName' THEN 'dbo.CheckChildRecord(Opp.numOppId,Opp.numDomainID)'
									  WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'vcBizDocsList' THEN 'ISNULL((SELECT SUBSTRING((SELECT ''$^$'' + CAST(numOppBizDocsId AS VARCHAR(18)) +''#^#''+ vcBizDocID
										FROM OpportunityBizDocs WHERE numOppId=Opp.numOppId AND 1 = (Case When bitAuthoritativeBizDocs=1 AND 1=' + (CASE WHEN @inttype=1 THEN '1' ELSE '0' END) + '  Then 0 ELSE 1 END) FOR XML PATH('''')),4,200000)),'''')'
                                      ELSE @Prefix + @vcDbColumnName
                                    END
                                  + ' ['
                                  + @vcColumnName + ']'
				END
				IF(@vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'vcBizDocsList')
				BEGIN
					SET @strColumns=@strColumns+','+' (SELECT COUNT(I.vcItemName) FROM 
													 OpportunityItems AS t LEFT JOIN  Item as I ON 
													 t.numItemCode=I.numItemCode WHERE 
													 t.numOppId=Opp.numOppId AND t.numoppItemtCode NOT IN 
													 (SELECT numOppItemId FROM OpportunityBizDocs JOIN 
													 OpportunityBizDocItems ON OpportunityBizDocs.numOppBizDocsId=OpportunityBizDocItems.numOppBizDocID  
													 WHERE OpportunityBizDocs.numOppId=Opp.numOppId 
													 AND 1 = (Case When bitAuthoritativeBizDocs=1 Or numBizDocID=293 Then 0 ELSE 1 END) 
													 AND ISNULL(bitAuthoritativeBizDocs,0)=1)) AS [List_Item_Approval_UNIT] '
				END
				
				IF LEN(@SearchText) > 0
				BEGIN
					SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + (CASE 
                                      WHEN @vcDbColumnName = 'numAge' THEN 'year(getutcdate())-year(bintDOB)'
                                      WHEN @vcDbColumnName = 'monPAmount' THEN 'isnull((SELECT varCurrSymbol FROM [Currency] WHERE numCurrencyID = Opp.[numCurrencyID]),'''') + '' ''+ CONVERT(VARCHAR,CONVERT(DECIMAL(20,5),monPAmount),1)'
                                      WHEN @vcDbColumnName = 'numDivisionID' THEN 'Div.numDivisionID'
                                      WHEN @vcDbColumnName = 'tintOppStatus' THEN 'CASE tintOppStatus WHEN 0 then ''Open'' WHEN 1 THEN ''Closed-Won'' WHEN 2 THEN ''Closed-Lost'' END '
                                      WHEN @vcDbColumnName = 'tintOppType' THEN 'CASE tintOppType WHEN 1 then ''Sales'' WHEN 2 THEN ''Purchase'' Else ''NA'' END '
									  WHEN @vcDbColumnName = 'vcPOppName' THEN 'dbo.CheckChildRecord(Opp.numOppId,Opp.numDomainID)'
									  WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'vcBizDocsList' THEN 'ISNULL((SELECT SUBSTRING((SELECT ''$^$'' + CAST(numOppBizDocsId AS VARCHAR(18)) +''#^#''+ vcBizDocID
										FROM OpportunityBizDocs WHERE numOppId=Opp.numOppId AND 1 = (Case When bitAuthoritativeBizDocs=1 Or numBizDocID=293 Then 0 ELSE 1 END)  FOR XML PATH('''')),4,200000)),'''')'
                                      ELSE @Prefix + @vcDbColumnName
                                    END) + ' LIKE ''%' + @SearchText + '%'''
				END
            END
            ELSE
            IF @vcAssociatedControlType = 'TextArea'
            BEGIN
				SET @strColumns=@strColumns+','+  @Prefix + @vcDbColumnName +' ['+ @vcColumnName+']' 

				IF LEN(@SearchText) > 0
				BEGIN
					SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + @Prefix + @vcDbColumnName + ' LIKE ''%' + @SearchText + '%'''
				END
            END
            ELSE IF  @vcAssociatedControlType='Label'                                              
			BEGIN
				SET @strColumns=@strColumns + ',' + CASE               
				WHEN @vcDbColumnName = 'vcTrackingDetail' THEN 'STUFF((SELECT 
																			CONCAT(''<br/>'',OpportunityBizDocs.vcBizDocID,'': '',vcTrackingDetail)
																		FROM 
																			ShippingReport 
																		INNER JOIN 
																			OpportunityBizDocs 
																		ON 
																			ShippingReport.numOppBizDocId=OpportunityBizDocs.numOppBizDocsId 
																		WHERE 
																			ShippingReport.numOppId=Opp.numOppID
																			AND ISNULL(ShippingReport.vcTrackingDetail,'''') <> ''''
																		FOR XML PATH(''''), TYPE).value(''(./text())[1]'',''varchar(max)''), 1, 5, '''')'     
				WHEN @vcDbColumnName = 'CalAmount' THEN 'Opp.monDealAmount'--'[dbo].[getdealamount](Opp.numOppId,Getutcdate(),0)'
				WHEN @vcDbColumnName = 'vcInventoryStatus' THEN 'ISNULL(dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID),'''')'
				WHEN @vcDbColumnName = 'vcOrderedShipped' THEN CONCAT('dbo.GetOrderShipReceiveStatus(Opp.numOppId,1,Opp.tintshipped,',@tintDecimalPoints,')')
				WHEN @vcDbColumnName = 'vcOrderedReceived' THEN CONCAT('dbo.GetOrderShipReceiveStatus(Opp.numOppId,2,Opp.tintshipped,',@tintDecimalPoints,')')
				WHEN @vcDbColumnName = 'vcShipStreet' THEN 'dbo.fn_getOPPState(Opp.numOppId,Opp.numDomainID,2)'
				WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'monDealAmount' THEN 'ISNULL((SELECT SUM(ISNULL(monDealAmount,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)'
				WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'monAmountPaid' THEN 'ISNULL((SELECT SUM(ISNULL(monAmountPaid,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)'
				ELSE @Prefix + @vcDbColumnName END +' ['+ @vcColumnName+']'   
				
				IF @vcDbColumnName='vcOrderedShipped'
				BEGIN
				DECLARE @temp VARCHAR(MAX)
				SET @temp = '(SELECT 
				STUFF((SELECT '', '' + CAST((CAST(numOppBizDocsId AS varchar)+''~''+CAST(ISNULL(numShippingReportId,0) AS varchar) ) AS VARCHAR(MAX)) [text()]
				FROM OpportunityBizDocs
				LEFT JOIN ShippingReport
				ON ShippingReport.numOppBizDocId=OpportunityBizDocs.numOppBizDocsId
				WHERE OpportunityBizDocs.numOppId=Opp.numOppId  AND OpportunityBizDocs.bitShippingGenerated=1  FOR XML PATH(''''), TYPE)
						.value(''.'',''NVARCHAR(MAX)''),1,2,'' ''))'
					SET @strColumns = @strColumns + ' , (SELECT SUBSTRING('+ @temp +', 2, 200000))AS ShippingIcons '
					SET @strColumns = @strColumns + ' , CASE WHEN (SELECT COUNT(*) FROM ShippingBox INNER JOIN ShippingReport ON ShippingBox.numShippingReportId = ShippingReport.numShippingReportId WHERE ShippingReport.numOppID = Opp.numOppID AND LEN(vcTrackingNumber) > 0) > 0 THEN 1 ELSE 0 END [ISTrackingNumGenerated]'
				END

				IF LEN(@SearchText) > 0
				BEGIN
					SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + (CASE                    
					WHEN @vcDbColumnName = 'CalAmount' THEN 'Opp.monDealAmount'
					WHEN @vcDbColumnName = 'vcInventoryStatus' THEN 'ISNULL(dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID),'''')'
					WHEN @vcDbColumnName = 'vcOrderedShipped' THEN CONCAT('dbo.GetOrderShipReceiveStatus(Opp.numOppId,1,Opp.tintshipped,',@tintDecimalPoints,')')
					WHEN @vcDbColumnName = 'vcOrderedReceived' THEN CONCAT('dbo.GetOrderShipReceiveStatus(Opp.numOppId,2,Opp.tintshipped,',@tintDecimalPoints,')')
					WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'monDealAmount' THEN 'ISNULL((SELECT SUM(ISNULL(monDealAmount,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)'
					WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'monAmountPaid' THEN 'ISNULL((SELECT SUM(ISNULL(monAmountPaid,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)'
					else @Prefix + @vcDbColumnName END) + ' LIKE ''%' + @SearchText + '%'''
				END    
			END
            ELSE IF @vcAssociatedControlType='Popup' and @vcDbColumnName='numShareWith'                                                   
			BEGIN
				SET @strColumns = @strColumns + ',(SELECT SUBSTRING(
								(SELECT '','' + A.vcFirstName+'' ''+A.vcLastName + CASE WHEN ISNULL(SR.numContactType,0)>0 THEN ''('' + dbo.fn_GetListItemName(SR.numContactType) + '')'' ELSE '''' END
								FROM ShareRecord SR JOIN UserMaster UM ON UM.numUserDetailId=SR.numAssignedTo 
								JOIN AdditionalContactsInformation A ON UM.numUserDetailId=A.numContactID 
								WHERE SR.numDomainID=Opp.numDomainID AND SR.numModuleID=3 AND SR.numRecordID=Opp.numOppId
								AND UM.numDomainID=Opp.numDomainID and UM.numDomainID=A.numDomainID FOR XML PATH('''')),2,200000)) ['+ @vcColumnName+']'                
			END 
        END
		ELSE
        BEGIN
			SET @vcColumnName= @vcDbColumnName +'~'+ convert(varchar(10),@numFieldId)+'~1'

			IF @vcAssociatedControlType = 'TextBox' or @vcAssociatedControlType = 'TextArea'
            BEGIN
              SET @strColumns = @strColumns
                              + ',CFW'
                              + CONVERT(VARCHAR(3),@tintOrder)
                              + '.Fld_Value  ['
                              + @vcColumnName + ']'
              SET @WhereCondition = @WhereCondition
                                      + ' left Join CFW_FLD_Values_Opp CFW'
                                      + CONVERT(VARCHAR(3),@tintOrder)
                                      + ' on CFW'
                                      + CONVERT(VARCHAR(3),@tintOrder)
                                      + '.Fld_Id='
                                      + CONVERT(VARCHAR(10),@numFieldId)
                                      + 'and CFW'
                                      + CONVERT(VARCHAR(3),@tintOrder)
                                      + '.RecId=Opp.numOppid   '
			END
			ELSE
            IF @vcAssociatedControlType = 'CheckBox' 
            BEGIN
                SET @strColumns = @strColumns
                                + ',case when isnull(CFW'
                                + CONVERT(VARCHAR(3),@tintOrder)
                                + '.Fld_Value,0)=0 then 0 when isnull(CFW'
                                + CONVERT(VARCHAR(3),@tintOrder)
                                + '.Fld_Value,0)=1 then 1 end   ['
                                + @vcColumnName + ']'
                SET @WhereCondition = @WhereCondition
                                        + ' left Join CFW_FLD_Values_Opp CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + ' on CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.Fld_Id='
                                        + CONVERT(VARCHAR(10),@numFieldId)
                                        + 'and CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.RecId=Opp.numOppid   '
            END
            ELSE
            IF @vcAssociatedControlType = 'DateField'
            BEGIN
                  SET @strColumns = @strColumns
                                  + ',dbo.FormatedDateFromDate(CFW'
                                  + CONVERT(VARCHAR(3),@tintOrder)
                                  + '.Fld_Value,'
                                  + CONVERT(VARCHAR(10),@numDomainId)
                                  + ')  ['
                                  + @vcColumnName + ']'
                  SET @WhereCondition = @WhereCondition
                                          + ' left Join CFW_FLD_Values_Opp CFW'
                                          + CONVERT(VARCHAR(3),@tintOrder)
                                          + ' on CFW'
                                          + CONVERT(VARCHAR(3),@tintOrder)
                                          + '.Fld_Id='
                                          + CONVERT(VARCHAR(10),@numFieldId)
                                          + 'and CFW'
                                          + CONVERT(VARCHAR(3),@tintOrder)
                                          + '.RecId=Opp.numOppid    '
            END
            ELSE
            IF @vcAssociatedControlType = 'SelectBox'
            BEGIN
                SET @vcDbColumnName = 'DCust'
                                            + CONVERT(VARCHAR(10),@numFieldId)
                SET @strColumns = @strColumns
                                + ',L'
                                + CONVERT(VARCHAR(3),@tintOrder)
                                + '.vcData'
                                + ' ['
                                + @vcColumnName + ']'

                SET @WhereCondition = @WhereCondition
                                        + ' left Join CFW_FLD_Values_Opp CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + ' on CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.Fld_Id='
                                        + CONVERT(VARCHAR(10),@numFieldId)
                                        + 'and CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.RecId=Opp.numOppid '

                SET @WhereCondition = @WhereCondition
                                        + ' left Join ListDetails L'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + ' on L'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.numListItemID=CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.Fld_Value'
            END
			ELSE
			BEGIN
				SET @strColumns = @strColumns + CONCAT(',dbo.GetCustFldValueOpp(',@numFieldId,',opp.numOppID)') + ' [' + @vcColumnName + ']'
			END

			IF LEN(@SearchText) > 0
			BEGIN
				SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + CONCAT('dbo.GetCustFldValueOpp(',@numFieldId,',opp.numOppID) LIKE ''%',@SearchText,'%''')
			END
		END
      
     
		SELECT TOP 1 
			@tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName,@vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,
			@numListID=numListID,@vcLookBackTableName=vcLookBackTableName,@bitCustom=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,@bitAllowEdit=bitAllowEdit,
			@ListRelID=ListRelID
		FROM 
			#tempForm 
		WHERE 
			tintOrder > @tintOrder-1 
		ORDER BY 
			tintOrder asc            
 
		IF @@rowcount=0 SET @tintOrder=0 
	END 

	

	DECLARE @strShareRedordWith AS VARCHAR(300);
	SET @strShareRedordWith=' Opp.numOppId in (select SR.numRecordID FROM ShareRecord SR WHERE SR.numDomainID='+ convert(varchar(15),@numDomainID )+' AND SR.numModuleID=3 AND SR.numAssignedTo='+convert(varchar(15),@numUserCntId)+') '


	DECLARE @StrSql AS VARCHAR(MAX) = ''

	SET @StrSql = @StrSql + ' FROM OpportunityMaster Opp                                               
                                 LEFT JOIN AdditionalContactsInformation ADC ON Opp.numContactId = ADC.numContactId                                                               
                                 INNER JOIN DivisionMaster Div ON Opp.numDivisionId = Div.numDivisionID 
								 INNER JOIN CompanyInfo cmp ON Div.numCompanyID = cmp.numCompanyId 
								 LEFT JOIN ProjectProgress PP On PP.numOppId = Opp.numOppId ' + @WhereCondition
								 --INNER JOIN Correspondence Corr ON opp.numOppId = Corr.numOpenRecordID 
	-------Change Row Color-------
	
	DECLARE @vcCSOrigDbCOlumnName AS VARCHAR(50) = ''
	DECLARE @vcCSLookBackTableName AS VARCHAR(50) = ''
	DECLARE @vcCSAssociatedControlType AS VARCHAR(50)

	CREATE TABLE #tempColorScheme
	(
		vcOrigDbCOlumnName VARCHAR(50),
		vcLookBackTableName VARCHAR(50),
		vcFieldValue VARCHAR(50),
		vcFieldValue1 VARCHAR(50),
		vcColorScheme VARCHAR(50),
		vcAssociatedControlType varchar(50)
	)

	INSERT INTO 
		#tempColorScheme 
	SELECT 
		DFM.vcOrigDbCOlumnName,
		DFM.vcLookBackTableName,
		DFCS.vcFieldValue,
		DFCS.vcFieldValue1,
		DFCS.vcColorScheme,
		DFFM.vcAssociatedControlType 
	FROM 
		DycFieldColorScheme DFCS 
	JOIN 
		DycFormField_Mapping DFFM 
	ON 
		DFCS.numFieldID=DFFM.numFieldID
	JOIN 
		DycFieldMaster DFM 
	ON 
		DFM.numModuleID=DFFM.numModuleID and DFM.numFieldID=DFFM.numFieldID
	WHERE
		DFCS.numDomainID=@numDomainID 
		AND DFFM.numFormID=@numFormId 
		AND DFCS.numFormID=@numFormId 
		AND isnull(DFFM.bitAllowGridColor,0)=1

	IF(SELECT COUNT(*) FROM #tempColorScheme)>0
	BEGIN
		SELECT TOP 1 @vcCSOrigDbCOlumnName=vcOrigDbCOlumnName,@vcCSLookBackTableName=vcLookBackTableName,@vcCSAssociatedControlType=vcAssociatedControlType from #tempColorScheme
	END                        

	IF len(@vcCSOrigDbCOlumnName)>0 and len(@vcCSLookBackTableName)>0
	BEGIN
		SET @strColumns=@strColumns + ',tCS.vcColorScheme'
	END

	IF len(@vcCSOrigDbCOlumnName)>0 and len(@vcCSLookBackTableName)>0
	BEGIN
		IF @vcCSLookBackTableName = 'AdditionalContactsInformation'                  
			SET @Prefix = 'ADC.'                  
		IF @vcCSLookBackTableName = 'DivisionMaster'                  
			SET @Prefix = 'Div.'
		if @vcCSLookBackTableName = 'CompanyInfo'                  
			SET @Prefix = 'CMP.'   
		if @vcCSLookBackTableName = 'OpportunityMaster'                  
			SET @Prefix = 'Opp.'   
 
		IF @vcCSAssociatedControlType='DateField'
		BEGIN
			SET @strSql=@strSql+' LEFT JOIN #tempColorScheme tCS ON CONVERT(VARCHAR(10),' + @Prefix + @vcCSOrigDbCOlumnName + ',111) >= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue as int),GETUTCDATE()),111)
				 and CONVERT(VARCHAR(10),' + @Prefix + @vcCSOrigDbCOlumnName + ',111) <= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue1 as int),GETUTCDATE()),111)'
		END
		ELSE IF @vcCSAssociatedControlType='SelectBox' OR @vcCSAssociatedControlType='ListBox' OR @vcCSAssociatedControlType='CheckBox'
		BEGIN
			SET @strSql=@strSql+' LEFT JOIN #tempColorScheme tCS on tCS.vcFieldValue=' + @Prefix + @vcCSOrigDbCOlumnName
		END
	END
                          
	IF @columnName like 'CFW.Cust%'             
	BEGIN            
		SET @strSql = @strSql + ' LEFT JOIN CFW_Fld_Values_Opp CFW on CFW.RecId=Opp.numOppid and CFW.fld_id= '+ REPLACE(@columnName,'CFW.Cust','') +' '                                                     
		DECLARE @vcSortFieldType VARCHAR(50)

		SELECT @vcSortFieldType = Fld_type FROM CFW_Fld_Master WHERE Fld_id=CAST(REPLACE(@columnName,'CFW.Cust','') AS INT)

		IF ISNULL(@vcSortFieldType,'') = 'DateField'
		BEGIN
			SET @columnName='(CASE WHEN ISDATE(CFW.Fld_Value) = 1 THEN CAST(CFW.Fld_Value AS DATE) ELSE NULL END)'
		END
		ELSE IF CAST(REPLACE(@columnName,'CFW.Cust','') AS INT) IN (12745,12846)
		BEGIN
			SET @columnName='(CASE WHEN ISNUMERIC(CFW.Fld_Value) = 1 THEN CAST(CFW.Fld_Value AS FLOAT) ELSE NULL END)'
		END
		ELSE
		BEGIN
			SET @columnName='CFW.Fld_Value'
		END            
	END 

	IF @bitPartner = 1
		SET @strSql = @strSql + ' LEFT JOIN OpportunityContact OppCont ON OppCont.numOppId=Opp.numOppId AND OppCont.bitPartner=1 AND OppCont.numContactId=' + CONVERT(VARCHAR(15),@numUserCntID)
                    
                
	IF @tintFilterBy = 1 --Partially Fulfilled Orders 
		SET @strSql = @strSql + ' LEFT JOIN AdditionalContactsInformation ADC1 ON ADC1.numContactId=Opp.numRecOwner                                                               
                             LEFT OUTER JOIN [OpportunityBizDocs] OB ON OB.[numOppId] = Opp.[numOppId] 
                             WHERE Opp.numDomainID=' + CONVERT(VARCHAR(15),@numDomainID) + ' and Opp.tintOppstatus=1 and (Opp.tintShipped=' + CONVERT(VARCHAR(15),@tintShipped) + ' OR ' + CONVERT(VARCHAR(15),@tintShipped) + '=2)'
	ELSE
		SET @strSql = @strSql + ' LEFT JOIN AdditionalContactsInformation ADC1 on ADC1.numContactId=Opp.numRecOwner                                                               
                             WHERE Opp.numDomainID=' + CONVERT(VARCHAR(15),@numDomainID) + ' and Opp.tintOppstatus=1 and (Opp.tintShipped='+ CONVERT(VARCHAR(15),@tintShipped)  + ' OR ' + CONVERT(VARCHAR(15),@tintShipped) + '=2)'
  
  
	IF @intOrder <> 0
    BEGIN
      IF @intOrder = 1
        SET @strSql = @strSql + ' and Opp.bitOrder = 0 '
      IF @intOrder = 2
        SET @strSql = @strSql + ' and Opp.bitOrder = 1'
    END
    
	IF @numCompanyID <> 0
		SET @strSql = @strSql + ' And Div.numCompanyID ='+ CONVERT(VARCHAR(15),@numCompanyID)

	IF @SortChar <> '0' 
		SET @strSql = @strSql + ' And Opp.vcPOppName like ''' + @SortChar + '%'''
  

	IF @inttype = 3
	BEGIN
		IF @tintSalesUserRightType = 0
			SET @strSql = @strSql + ' AND opp.tintOppType=0'
		ELSE IF @tintSalesUserRightType = 1
			SET @strSql = @strSql + ' AND (Opp.numRecOwner= ' + CONVERT(VARCHAR(15),@numUserCntID)
						+ ' or Opp.numAssignedTo= '+ CONVERT(VARCHAR(15),@numUserCntID)
						+ CASE WHEN @bitPartner = 1 THEN ' or ( OppCont.bitPartner=1 and OppCont.numContactId='+ CONVERT(VARCHAR(15),@numUserCntID) + ')' ELSE '' END
						+ ' or ' + @strShareRedordWith +')'
		ELSE IF @tintSalesUserRightType = 2
			SET @strSql = @strSql + ' AND (Div.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+ CONVERT(VARCHAR(15),@numUserCntID) + ' ) 
									 or Opp.numAssignedTo= '+ CONVERT(VARCHAR(15),@numUserCntID)
									+ ' or ' + @strShareRedordWith +')'
	END
	ELSE IF @inttype = 4
	BEGIN
		IF @tintPurchaseUserRightType = 0
			SET @strSql = @strSql + ' AND opp.tintOppType=0'
		ELSE IF @tintPurchaseUserRightType = 1
			SET @strSql = @strSql + ' AND (Opp.numRecOwner= ' + CONVERT(VARCHAR(15),@numUserCntID)
						+ ' or Opp.numAssignedTo= '+ CONVERT(VARCHAR(15),@numUserCntID)
						+ CASE WHEN @bitPartner = 1 THEN ' or ( OppCont.bitPartner=1 and OppCont.numContactId='+ CONVERT(VARCHAR(15),@numUserCntID) + ')' ELSE '' END
						+ ' or ' + @strShareRedordWith +')'
		ELSE IF @tintPurchaseUserRightType = 2
			SET @strSql = @strSql + ' AND (Div.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+ CONVERT(VARCHAR(15),@numUserCntID) + ' ) 
									 or Opp.numAssignedTo= '+ CONVERT(VARCHAR(15),@numUserCntID)
									+ ' or ' + @strShareRedordWith +')'
	END

	
					
	IF @tintSortOrder <> '0'
		SET @strSql = @strSql + '  AND Opp.tintOppType= '+ CONVERT(VARCHAR(1),@tintSortOrder)
    
    
	IF @tintFilterBy = 1 --Partially Fulfilled Orders
		SET @strSql = @strSql + ' AND OB.bitPartialFulfilment = 1 AND OB.bitAuthoritativeBizDocs = 1 '
	ELSE IF @tintFilterBy = 2 --Fulfilled Orders
		SET @strSql = @strSql + ' AND Opp.[numOppId] IN (SELECT OM.numOppId FROM [OpportunityMaster] OM
                      			INNER JOIN [OpportunityBizDocs] OB ON OB.[numOppId] = OM.[numOppId]
                      			INNER JOIN [OpportunityItems] OI ON OM.[numOppId] = OI.[numOppId]
                      			INNER JOIN [OpportunityBizDocItems] BI ON BI.[numOppBizDocID] = OB.[numOppBizDocsId]
                      			Where OM.numDomainId=' + CONVERT(VARCHAR(15),@numDomainId) + 'AND OB.[bitAuthoritativeBizDocs] = 1 GROUP BY OM.[numOppId]
                      			HAVING   COUNT(OI.[numUnitHour]) = COUNT(BI.[numUnitHour])) '
	ELSE IF @tintFilterBy = 3 --Orders without Authoritative-BizDocs
		SET @strSql = @strSql + ' AND Opp.[numOppId] not IN (SELECT DISTINCT OM.[numOppId]
                      		  FROM [OpportunityMaster] OM JOIN [OpportunityBizDocs] OB ON OM.[numOppId] = OB.[numOppId]
                      		  Where OM.numDomainId=' + CONVERT(VARCHAR(15),@numDomainId) + ' AND OM.tintOppstatus=1 AND (OM.tintShipped=' + CONVERT(VARCHAR(15),@tintShipped) + ' OR ' + CONVERT(VARCHAR(15),@tintShipped) + '=2)' + ' AND OM.tintOppType= '+ CONVERT(VARCHAR(1),@tintSortOrder) + ' AND isnull(OB.[bitAuthoritativeBizDocs],0)=1) '
	ELSE IF @tintFilterBy = 4 --Orders with items yet to be added to Invoices
		SET @strSql = @strSql + ' AND Opp.[numOppId] IN (SELECT distinct OPPM.numOppId FROM [OpportunityMaster] OPPM
                                  INNER JOIN [OpportunityItems] OPPI ON OPPM.[numOppId] = OPPI.[numOppId]
                                  Where OPPM.numDomainId=' + CONVERT(VARCHAR(15),@numDomainId) + ' 
								  and OPPI.numoppitemtCode not in (select numOppItemID from [OpportunityBizDocs] OB INNER JOIN [OpportunityBizDocItems] BI
                      			  ON BI.[numOppBizDocID] = OB.[numOppBizDocsId] where OB.[numOppId] = OPPM.[numOppId]))'      
	ELSE IF @tintFilterBy = 6 --Orders with deferred Income/Invoices
		SET @strSql = @strSql + ' AND Opp.[numOppId] IN (SELECT DISTINCT OM.[numOppId]
                      		 FROM [OpportunityMaster] OM LEFT OUTER JOIN [OpportunityBizDocs] OB ON OM.[numOppId] = OB.[numOppId]
                      		 Where OM.numDomainId=' + CONVERT(VARCHAR(15),@numDomainId) + ' AND OB.[bitAuthoritativeBizDocs] =1 and OB.tintDeferred=1)'

	IF @numOrderStatus <>0 
		SET @strSql = @strSql + ' AND Opp.numStatus = ' + CONVERT(VARCHAR(15),@numOrderStatus);

	IF CHARINDEX('OI.vcInventoryStatus',@vcRegularSearchCriteria) > 0
	BEGIN
		IF CHARINDEX('OI.vcInventoryStatus=1',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'OI.vcInventoryStatus=1',' CHARINDEX(''Not Applicable'', dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID)) > 0 ')
		IF CHARINDEX('OI.vcInventoryStatus=2',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'OI.vcInventoryStatus=2',' CHARINDEX(''Shipped'', dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID)) > 0 ')
		IF CHARINDEX('OI.vcInventoryStatus=3',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'OI.vcInventoryStatus=3',' CHARINDEX(''BO'', dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID)) > 0 AND CHARINDEX(''Shippable'', dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID)) = 0 ')
		IF CHARINDEX('OI.vcInventoryStatus=4',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'OI.vcInventoryStatus=4',' CHARINDEX(''Shippable'', dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID)) > 0 ')
	END
	
	IF CHARINDEX('Opp.tintInvoicing',@vcRegularSearchCriteria) > 0
	BEGIN
		IF CHARINDEX('Opp.tintInvoicing=0',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.tintInvoicing=0',' CHARINDEX(''All'', dbo.CheckOrderInvoicingStatus(Opp.numOppID,Opp.numDomainID)) > 0 ')
		IF CHARINDEX('Opp.tintInvoicing=1',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.tintInvoicing=1',' (SELECT (CASE WHEN SUM(ISNULL(InvoicedQty,0)) = SUM(ISNULL(OrderedQty,0)) THEN 1 ELSE 0 END)
	FROM 
	(
		SELECT OI.numoppitemtCode, ISNULL(OI.numUnitHour,0) AS OrderedQty, ISNULL(TempInvoice.InvoicedQty,0) AS InvoicedQty
		FROM OpportunityItems OI
		INNER JOIN Item I ON OI.numItemCode = I.numItemCode
		OUTER APPLY
		(
			SELECT SUM(OpportunityBizDocItems.numUnitHour) AS InvoicedQty
			FROM OpportunityBizDocs
				INNER JOIN OpportunityBizDocItems ON OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
			WHERE OpportunityBizDocs.numOppId = Opp.numOppID
				AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
				AND numBizDocId IN (SELECT numAuthoritativeSales FROM AuthoritativeBizDocs WHERE numDomainId = Opp.numDomainID)
		) AS TempInvoice
		WHERE
		OI.numOppID = Opp.numOppID
	) X) > 0 ')
		IF CHARINDEX('Opp.tintInvoicing=2',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.tintInvoicing=2',' (SELECT COUNT(*) 
		FROM OpportunityBizDocs 
		WHERE numOppId = Opp.numOppID 
			AND numBizDocId IN (SELECT numAuthoritativeSales FROM AuthoritativeBizDocs WHERE numDomainId = Opp.numDomainID)) = 0 ')
		IF CHARINDEX('Opp.tintInvoicing=3',@vcRegularSearchCriteria) > 0 
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.tintInvoicing=3',' (SELECT (CASE WHEN SUM(ISNULL(InvoicedQty,0)) > 0 THEN SUM(ISNULL(OrderedQty,0)) - SUM(ISNULL(InvoicedQty,0)) ELSE 0 END)
	FROM 
	(
		SELECT OI.numoppitemtCode, ISNULL(OI.numUnitHour,0) AS OrderedQty, ISNULL(TempInvoice.InvoicedQty,0) AS InvoicedQty
		FROM OpportunityItems OI
		INNER JOIN Item I ON OI.numItemCode = I.numItemCode
		OUTER APPLY
		(
			SELECT SUM(OpportunityBizDocItems.numUnitHour) AS InvoicedQty
			FROM OpportunityBizDocs
				INNER JOIN OpportunityBizDocItems ON OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
			WHERE OpportunityBizDocs.numOppId = Opp.numOppID
				AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
				AND numBizDocId IN (SELECT numAuthoritativeSales FROM AuthoritativeBizDocs WHERE numDomainId = Opp.numDomainID)
		) AS TempInvoice
		WHERE
		OI.numOppID = Opp.numOppID
	) X) > 0 ')
		
		IF CHARINDEX('Opp.tintInvoicing=4',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.tintInvoicing=4',' (SELECT (CASE WHEN SUM(ISNULL(InvoicedQty,0)) > 0 THEN SUM(ISNULL(OrderedQty,0)) - SUM(ISNULL(InvoicedQty,0)) ELSE 0 END)
	FROM 
	(
		SELECT OI.numoppitemtCode, ISNULL(OI.numUnitHour,0) AS OrderedQty, ISNULL(TempInvoice.InvoicedQty,0) AS InvoicedQty
		FROM OpportunityItems OI
		INNER JOIN Item I ON OI.numItemCode = I.numItemCode
		OUTER APPLY
		(
			SELECT SUM(OpportunityBizDocItems.numUnitHour) AS InvoicedQty
			FROM OpportunityBizDocs
				INNER JOIN OpportunityBizDocItems ON OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
			WHERE OpportunityBizDocs.numOppId = Opp.numOppID
				AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
				AND numBizDocId IN (SELECT numAuthoritativeSales FROM AuthoritativeBizDocs WHERE numDomainId = Opp.numDomainID)
		) AS TempInvoice
		WHERE
		OI.numOppID = Opp.numOppID
	) X) > 0 ')

		IF CHARINDEX('Opp.tintInvoicing=5',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.tintInvoicing=5',' 
				Opp.[numOppId] IN (SELECT DISTINCT OM.[numOppId]
                      		 FROM [OpportunityMaster] OM INNER JOIN [OpportunityBizDocs] OB ON OM.[numOppId] = OB.[numOppId]
                      		 Where OM.numDomainId = Opp.numDomainID AND OB.[bitAuthoritativeBizDocs] = 1 and OB.tintDeferred=1) ')
	END
	
	IF CHARINDEX('Opp.tintEDIStatus',@vcRegularSearchCriteria) > 0
	BEGIN
		--IF CHARINDEX('Opp.tintEDIStatus=1',@vcRegularSearchCriteria) > 0
		--	SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.tintEDIStatus=1',' 1 = (SELECT CASE WHEN Opp.tintEDIStatus = 1 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.tintEDIStatus=2',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.tintEDIStatus=2',' 1 = (SELECT CASE WHEN Opp.tintEDIStatus = 2 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.tintEDIStatus=3',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.tintEDIStatus=3',' 1 = (SELECT CASE WHEN Opp.tintEDIStatus = 3 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.tintEDIStatus=4',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.tintEDIStatus=4',' 1 = (SELECT CASE WHEN Opp.tintEDIStatus = 4 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.tintEDIStatus=5',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.tintEDIStatus=5',' 1 = (SELECT CASE WHEN Opp.tintEDIStatus = 5 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.tintEDIStatus=6',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.tintEDIStatus=6',' 1 = (SELECT CASE WHEN Opp.tintEDIStatus = 6 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.tintEDIStatus=7',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.tintEDIStatus=7',' 1 = (SELECT CASE WHEN Opp.tintEDIStatus = 7 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.tintEDIStatus=7',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.tintEDIStatus=11',' 1 = (SELECT CASE WHEN Opp.tintEDIStatus = 11 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.tintEDIStatus=7',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.tintEDIStatus=12',' 1 = (SELECT CASE WHEN Opp.tintEDIStatus = 12 THEN 1 ELSE 0 END) ')
	END

	IF CHARINDEX('Opp.vcSignatureType',@vcRegularSearchCriteria) > 0
	BEGIN
		IF CHARINDEX('Opp.vcSignatureType=0',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.vcSignatureType=0',' 1 = (SELECT CASE WHEN (SELECT COUNT(*) FROM DivisionMaster AS D 
																			LEFT JOIN DivisionMasterShippingConfiguration AS DMSC ON DMSC.numDivisionID=D.numDivisionID 
																			WHERE D.numDivisionID=Div.numDivisionID AND vcSignatureType=0 ) > 0 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.vcSignatureType=1',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.vcSignatureType=1','  1 = (SELECT CASE WHEN (SELECT COUNT(*) FROM DivisionMaster AS D 
																			LEFT JOIN DivisionMasterShippingConfiguration AS DMSC ON DMSC.numDivisionID=D.numDivisionID 
																			WHERE D.numDivisionID=Div.numDivisionID AND vcSignatureType=1 ) > 0 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.vcSignatureType=2',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.vcSignatureType=2',' 1 = (SELECT CASE WHEN (SELECT COUNT(*) FROM DivisionMaster AS D 
																			LEFT JOIN DivisionMasterShippingConfiguration AS DMSC ON DMSC.numDivisionID=D.numDivisionID 
																			WHERE D.numDivisionID=Div.numDivisionID AND vcSignatureType=2 ) > 0 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.vcSignatureType=3',@vcRegularSearchCriteria) > 0								   
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.vcSignatureType=3',' 1 = (SELECT CASE WHEN (SELECT COUNT(*) FROM DivisionMaster AS D 
																			LEFT JOIN DivisionMasterShippingConfiguration AS DMSC ON DMSC.numDivisionID=D.numDivisionID 
																			WHERE D.numDivisionID=Div.numDivisionID AND vcSignatureType=3 ) > 0 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.vcSignatureType=4',@vcRegularSearchCriteria) > 0								   
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.vcSignatureType=4',' 1 = (SELECT CASE WHEN (SELECT COUNT(*) FROM DivisionMaster AS D 
																			LEFT JOIN DivisionMasterShippingConfiguration AS DMSC ON DMSC.numDivisionID=D.numDivisionID 
																			WHERE D.numDivisionID=Div.numDivisionID AND vcSignatureType=4 ) > 0 THEN 1 ELSE 0 END) ')
	END

	IF CHARINDEX('OBD.monAmountPaid',@vcRegularSearchCriteria) > 0
	BEGIN
		IF CHARINDEX('OBD.monAmountPaid like ''%0%''',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'OBD.monAmountPaid like ''%0%''',' 1 = (SELECT ISNULL((SELECT SUM(ISNULL(monAmountPaid,0)) FROM  OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)) ')
		IF CHARINDEX('OBD.monAmountPaid like ''%1%''',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'OBD.monAmountPaid like ''%1%''','  1 = (SELECT (CASE WHEN (SELECT (CASE WHEN (SUM(ISNULL(monDealAmount,0)) = SUM(ISNULL(monAmountPaid,0)) AND SUM(ISNULL(monAmountPaid,0)) <> 0) THEN 1 ELSE 0 END) FROM OpportunityBizDocs BD WHERE BD.numOppId =  Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0) = 1) > 0 THEN 1 ELSE 0 END))  ')
		IF CHARINDEX('OBD.monAmountPaid like ''%2%''',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'OBD.monAmountPaid like ''%2%''',' 1 = (SELECT CASE WHEN (SELECT (CASE WHEN ((SUM(ISNULL(monDealAmount,0)) - SUM(ISNULL(monAmountPaid,0))) > 0 AND SUM(ISNULL(monAmountPaid,0)) <> 0) THEN 1 ELSE 0 END) FROM OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0) = 1) > 0 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('OBD.monAmountPaid like ''%3%''',@vcRegularSearchCriteria) > 0								   
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'OBD.monAmountPaid like ''%3%''',' 1 = (SELECT CASE WHEN (SELECT (CASE WHEN (SUM(ISNULL(monAmountPaid,0)) = 0) THEN 1 ELSE 0 END) FROM OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0) = 1) > 0 THEN 1 ELSE 0 END) ')
	END
	
	
	IF @vcRegularSearchCriteria<>'' 
	BEGIN
		SET @strSql=@strSql +' AND ' + @vcRegularSearchCriteria 
	END
	
	


	IF @vcCustomSearchCriteria<>'' 
	BEGIN
		SET @strSql = @strSql +' AND ' +  @vcCustomSearchCriteria
	END

	IF LEN(@SearchQuery) > 0
	BEGIN
		SET @strSql = @strSql + ' AND (' + @SearchQuery + ') '
	END

	IF ISNULL(@tintDashboardReminderType,0) = 19
	BEGIN
		SET @strSql = CONCAT(@strSql,' AND Opp.numOppID IN (','SELECT DISTINCT
																	OMInner.numOppID
																FROM 
																	StagePercentageDetails SPDInner
																INNER JOIN 
																	OpportunityMaster OMInner
																ON 
																	SPDInner.numOppID=OMInner.numOppId 
																WHERE 
																	SPDInner.numDomainId=',@numDOmainID,'
																	AND OMInner.numDomainId=',@numDOmainID,' 
																	AND OMInner.tintOppType = 1
																	AND ISNULL(OMInner.tintOppStatus,0) = 1
																	AND tinProgressPercentage <> 100',') ')

	END
	ELSE IF ISNULL(@tintDashboardReminderType,0) = 10
	BEGIN
		SET @strSql = CONCAT(@strSql,' AND Opp.numOppID IN (','SELECT DISTINCT
																	OMInner.numOppId
																FROM
																	OpportunityMaster OMInner
																INNER JOIN
																	OpportunityItems OIInner
																ON
																	OIInner.numOppId = OMInner.numOppID
																INNER JOIN
																	Item IInner
																ON
																	OIInner.numItemCode = IInner.numItemCode
																OUTER APPLY
																(
																	SELECT
																		SUM(OpportunityBizDocItems.numUnitHour) AS BilledQty
																	FROM
																		OpportunityBizDocs
																	INNER JOIN
																		OpportunityBizDocItems 
																	ON
																		OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																	WHERE
																		OpportunityBizDocs.numOppId = OMInner.numOppID
																		AND OpportunityBizDocItems.numOppItemID = OIInner.numoppitemtCode
																		AND OpportunityBizDocs.numBizDocId = (SELECT ISNULL(numAuthoritativePurchase,0) FROM AuthoritativeBizDocs WHERE numDomainId=',@numDomainID,')
																) AS TempBilled
																WHERE
																	OMInner.numDomainId = ',@numDomainID,'
																	AND ISNULL(OMInner.bitStockTransfer,0) = 0
																	AND OMInner.tintOppType = 2
																	AND OMInner.tintOppStatus  = 1
																	AND ISNULL(OIInner.numUnitHour,0) <> ISNULL(TempBilled.BilledQty,0)',') ')
	END
	ELSE IF ISNULL(@tintDashboardReminderType,0) = 11
	BEGIN
		SET @strSql = CONCAT(@strSql,' AND Opp.numOppID IN (','SELECT DISTINCT
																	OMInner.numOppId
																FROM
																	OpportunityMaster OMInner
																INNER JOIN
																	OpportunityItems OIInner
																ON
																	OIInner.numOppId = OMInner.numOppId
																INNER JOIN
																	Item IInner
																ON
																	OIInner.numItemCode = IInner.numItemCode
																OUTER APPLY
																(
																	SELECT
																		SUM(OpportunityBizDocItems.numUnitHour) AS PickPacked
																	FROM
																		OpportunityBizDocs
																	INNER JOIN
																		OpportunityBizDocItems 
																	ON
																		OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																	WHERE
																		OpportunityBizDocs.numOppId = OMInner.numOppId
																		AND ISNULL(OpportunityBizDocs.numBizDocId,0) = ',(SELECT ISNULL(numDefaultSalesShippingDoc,0) FROM Domain WHERE numDomainId=@numDomainID),'
																		AND OpportunityBizDocItems.numOppItemID = OIInner.numoppitemtCode
																) AS TempFulFilled
																WHERE
																	OMInner.numDomainId = ',@numDomainID,'
																	AND OMInner.tintOppType = 1
																	AND OMInner.tintOppStatus = 1
																	AND UPPER(IInner.charItemType) = ''P''
																	AND ISNULL(OMInner.tintshipped,0) = 0
																	AND ISNULL(OIInner.bitDropShip,0) = 0
																	AND ISNULL(OIInner.numUnitHour,0) <> ISNULL(TempFulFilled.PickPacked,0)',') ')
	END
	ELSE IF ISNULL(@tintDashboardReminderType,0) = 12
	BEGIN
		SET @strSql = CONCAT(@strSql,' AND Opp.numOppID IN (','SELECT DISTINCT
																	OMInner.numOppId
																FROM
																	OpportunityMaster OMInner
																INNER JOIN
																	OpportunityItems OIInner
																ON
																	OIInner.numOppId = OMInner.numOppId
																INNER JOIN
																	Item IInner
																ON
																	OIInner.numItemCode = IInner.numItemCode
																OUTER APPLY
																(
																	SELECT
																		SUM(OpportunityBizDocItems.numUnitHour) AS FulFilledQty
																	FROM
																		OpportunityBizDocs
																	INNER JOIN
																		OpportunityBizDocItems 
																	ON
																		OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																	WHERE
																		OpportunityBizDocs.numOppId = OMInner.numOppId
																		AND ISNULL(OpportunityBizDocs.numBizDocId,0) = 296
																		AND ISNULL(OpportunityBizDocs.bitFulFilled,0) = 1
																		AND OpportunityBizDocItems.numOppItemID = OIInner.numoppitemtCode
																) AS TempFulFilled
																WHERE
																	OMInner.numDomainId = ',@numDomainID,'
																	AND OMInner.tintOppType = 1
																	AND OMInner.tintOppStatus = 1
																	AND ISNULL(OMInner.tintshipped,0) = 0
																	AND UPPER(IInner.charItemType) = ''P''
																	AND ISNULL(OIInner.bitDropShip,0) = 0
																	AND ISNULL(OIInner.numUnitHour,0) <> ISNULL(TempFulFilled.FulFilledQty,0)',') ')
	END
	ELSE IF ISNULL(@tintDashboardReminderType,0) = 13
	BEGIN
		SET @strSql = CONCAT(@strSql,' AND Opp.numOppID IN (','SELECT DISTINCT
																	OMInner.numOppId
																FROM
																	OpportunityMaster OMInner
																INNER JOIN
																	OpportunityItems OIInner
																ON
																	OIInner.numOppId = OMInner.numOppID
																INNER JOIN
																	Item IInner
																ON
																	OIInner.numItemCode = IInner.numItemCode
																OUTER APPLY
																(
																	SELECT
																		SUM(OpportunityBizDocItems.numUnitHour) AS InvoicedQty
																	FROM
																		OpportunityBizDocs
																	INNER JOIN
																		OpportunityBizDocItems 
																	ON
																		OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																	WHERE
																		OpportunityBizDocs.numOppId = OMInner.numOppID
																		AND OpportunityBizDocItems.numOppItemID = OIInner.numoppitemtCode
																		AND OpportunityBizDocs.numBizDocId = ISNULL((SELECT TOP 1 numAuthoritativeSales FROM AuthoritativeBizDocs WHERE numDomainId=',@numDomainID,'),287)
																) AS TempInvoice
																WHERE
																	OMInner.numDomainId = ',@numDomainID,'
																	AND OMInner.tintOppType = 1
																	AND OMInner.tintOppStatus  = 1
																	AND ISNULL(OIInner.numUnitHour,0) <> ISNULL(TempInvoice.InvoicedQty,0)',') ')
	END
	ELSE IF ISNULL(@tintDashboardReminderType,0) = 14
	BEGIN
		DECLARE @TEMPOppID TABLE
		(
			numOppID NUMERIC(18,0)
		)

		INSERT INTO @TEMPOppID
		(
			numOppID
		)
		SELECT DISTINCT
			OM.numOppID
		FROM
			OpportunityMaster OM
		INNER JOIN
			OpportunityItems OI
		ON
			OM.numOppId = OI.numOppId
		WHERE   
			OM.numDomainId = @numDomainId
			AND OM.tintOppType = 1
			AND OM.tintOppStatus=1
			AND ISNULL(OI.bitDropShip, 0) = 1
			AND 1 = (CASE WHEN (SELECT COUNT(*) FROM OpportunityLinking OMInner INNER JOIN OpportunityItems OIInner ON OMInner.numChildOppID=OIInner.numOppId WHERE OMInner.numParentOppID = OM.numOppID AND OIInner.numItemCode=OI.numItemCode AND ISNULL(OIInner.numWarehouseItmsID,0)=ISNULL(OI.numWarehouseItmsID,0)) > 0 THEN 0 ELSE 1 END)


		If ISNULL((SELECT bitReOrderPoint FROM Domain WHERE numDomainId=@numDomainID),0) = 1
		BEGIN
			INSERT INTO @TEMPOppID
			(
				numOppID
			)
			SELECT DISTINCT
				OM.numOppID
			FROM
				OpportunityMaster OM
			INNER JOIN
				OpportunityItems OI
			ON
				OI.numOppId = OM.numOppId
			INNER JOIN 
				Item I 
			ON 
				I.numItemCode = OI.numItemCode
			INNER JOIN 
				WarehouseItems WI 
			ON 
				OI.numWarehouseItmsID = WI.numWareHouseItemID
			WHERE   
				OM.numDomainId = @numDomainId
				AND ISNULL((SELECT bitReOrderPoint FROM Domain WHERE numDomainId=@numDomainID),0) = 1
				AND (ISNULL(WI.numOnHand,0) + ISNULL(WI.numOnOrder,0) <= ISNULL(WI.numReOrder,0))
				AND OM.tintOppType = 1
				AND OM.tintOppStatus=1
				AND ISNULL(I.bitAssembly, 0) = 0
				AND ISNULL(I.bitKitParent, 0) = 0
				AND ISNULL(OI.bitDropship,0) = 0
				AND OM.numOppId NOT IN (SELECT numOppID FROM @TEMPOppID)

			IF (SELECT COUNT(*) FROM @TEMPOppID) > 0
			BEGIN
				DECLARE @tmpIds VARCHAR(MAX) = ''
				SELECT @tmpIds = CONCAT(@tmpIds,numOppID,', ') FROM @TEMPOppID
				SET @strSql = CONCAT(@strSql,' AND Opp.numOppID IN (',SUBSTRING(@tmpIds, 0, LEN(@tmpIds)),') ')
			END
			ELSE
			BEGIN
				SET @strSql = CONCAT(@strSql,' AND Opp.numOppID IN (0) ')
			END
		END
	END
	ELSE IF ISNULL(@tintDashboardReminderType,0) = 15
	BEGIN
		SET @strSql = CONCAT(@strSql,' AND Opp.numOppID IN (','SELECT
																	OMInner.numOppID
																FROM
																	OpportunityMaster OMInner
																WHERE
																	numDomainId=',@numDomainID,'
																	AND tintOppType=1
																	AND tintOppStatus=1
																	AND ISNULL(OMInner.tintshipped,0) = 0
																	AND dtReleaseDate IS NOT NULL
																	AND dtReleaseDate < DateAdd(minute,',@ClientTimeZoneOffset * -1,',GETUTCDATE())
																	AND (SELECT 
																			COUNT(*) 
																		FROM 
																		(
																			SELECT
																				OIInner.numoppitemtCode,
																				ISNULL(OIInner.numUnitHour,0) AS OrderedQty,
																				ISNULL(TempFulFilled.FulFilledQty,0) AS FulFilledQty
																			FROM
																				OpportunityItems OIInner
																			INNER JOIN
																				Item IInner
																			ON
																				OIInner.numItemCode = IInner.numItemCode
																			OUTER APPLY
																			(
																				SELECT
																					SUM(OpportunityBizDocItems.numUnitHour) AS FulFilledQty
																				FROM
																					OpportunityBizDocs
																				INNER JOIN
																					OpportunityBizDocItems 
																				ON
																					OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																				WHERE
																					OpportunityBizDocs.numOppId = OMInner.numOppId
																					AND ISNULL(OpportunityBizDocs.numBizDocId,0) = 296
																					AND ISNULL(OpportunityBizDocs.bitFulFilled,0) = 1
																					AND OpportunityBizDocItems.numOppItemID = OIInner.numoppitemtCode
																			) AS TempFulFilled
																			WHERE
																				OIInner.numOppID = OMInner.numOppID
																				AND UPPER(IInner.charItemType) = ''P''
																				AND ISNULL(OIInner.bitDropShip,0) = 0
																		) X
																		WHERE
																			X.OrderedQty <> X.FulFilledQty) > 0',') ')
	END



	DECLARE  @firstRec  AS INTEGER
	DECLARE  @lastRec  AS INTEGER
  
	SET @firstRec = (@CurrentPage - 1) * @PageSize
	SET @lastRec = (@CurrentPage * @PageSize + 1)



	-- EXECUTE FINAL QUERY
	DECLARE @strFinal AS NVARCHAR(MAX)
	IF @CurrentPage = -1 AND @PageSize = -1
	BEGIN
		SET @strFinal = CONCAT('SELECT COUNT(*) OVER() AS TotalRecords, ',@strColumns,@strSql,' ORDER BY ',@columnName,' ',@columnSortOrder,' OFFSET ',0,' ROWS FETCH NEXT ',25,' ROWS ONLY;')
	END
	ELSE
	BEGIN
		SET @strFinal = CONCAT('SELECT COUNT(*) OVER() AS TotalRecords, ',@strColumns,@strSql,' ORDER BY ',@columnName,' ',@columnSortOrder,' OFFSET ',(@CurrentPage - 1) * @PageSize,' ROWS FETCH NEXT ',@PageSize,' ROWS ONLY;')
	END
	PRINT CAST(@strFinal AS NTEXT)
	EXEC sp_executesql @strFinal

	SELECT * FROM #tempForm

	DROP TABLE #tempForm

	DROP TABLE #tempColorScheme
END
GO
/****** Object:  StoredProcedure [dbo].[USP_GetDealsListForPortal]    Script Date: 09/10/2013  ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetDealsListForPortal' ) 
    DROP PROCEDURE USP_GetDealsListForPortal
GO
CREATE PROCEDURE [dbo].[USP_GetDealsListForPortal]
    @numUserCntID NUMERIC(9) = 0,
    @numDomainID NUMERIC(9) = 0,
    @tintSortOrder TINYINT = 4,
    @SortChar CHAR(1) = '0',
    @CurrentPage INT,
    @PageSize INT,
    @TotRecs INT OUTPUT,
    @columnName AS VARCHAR(50),
    @columnSortOrder AS VARCHAR(10),
    @ClientTimeZoneOffset AS INT,
    @tintShipped AS TINYINT,
    @intOrder AS TINYINT,
    @intType AS TINYINT,
    @tintFilterBy AS TINYINT = 0,
    @numOrderStatus AS NUMERIC,
    @vcRegularSearchCriteria VARCHAR(1000) = '',
    @vcCustomSearchCriteria VARCHAR(1000) = '',
    @numDivisionID AS NUMERIC(9) = 0
AS 
    DECLARE @PageId AS TINYINT
    DECLARE @numFormId AS INT 
  
    SET @PageId = 0
    IF @inttype = 1 
        BEGIN
            SET @PageId = 2
            SET @inttype = 3
            SET @numFormId = 78
        END
    IF @inttype = 2 
        BEGIN
            SET @PageId = 6
            SET @inttype = 4
            SET @numFormId = 79
        END
        
  --Create a Temporary table to hold data
    CREATE TABLE #tempTable
        (
          ID INT IDENTITY
                 PRIMARY KEY,
          numOppId NUMERIC(9),
          intTotalProgress INT
        )
    
    DECLARE @strShareRedordWith AS VARCHAR(300) ;
    SET @strShareRedordWith = ' 1=1 '
--    SET @strShareRedordWith = ' Opp.numOppId in (select SR.numRecordID FROM ShareRecord SR WHERE SR.numDomainID='
--        + CONVERT(VARCHAR(15), @numDomainID)
--        + ' AND SR.numModuleID = 3 AND SR.numAssignedTo = '
--        + CONVERT(VARCHAR(15), @numUserCntId) + ') '

    DECLARE @strSql AS VARCHAR(8000)
    SET @strSql = 'SELECT  Opp.numOppId,PP.intTotalProgress '
  
    DECLARE @strOrderColumn AS VARCHAR(100) ;
    SET @strOrderColumn = @columnName

    SET @strSql = @strSql
        + ' FROM OpportunityMaster Opp                                                               
                                 INNER JOIN AdditionalContactsInformation ADC ON Opp.numContactId = ADC.numContactId                                                               
                                 INNER JOIN DivisionMaster Div ON Opp.numDivisionId = Div.numDivisionID AND ADC.numDivisionId = Div.numDivisionID
								 INNER JOIN CompanyInfo cmp ON Div.numCompanyID = cmp.numCompanyId 
								 LEFT JOIN ProjectProgress PP On PP.numOppId = Opp.numOppId '
								 
      
    IF @columnName LIKE 'CFW.Cust%' 
        BEGIN            
            SET @strSql = @strSql
                + ' left Join CFW_Fld_Values_Opp CFW on CFW.RecId=Opp.numOppid  and CFW.fld_id= '
                + REPLACE(@columnName, 'CFW.Cust', '') 
            SET @strOrderColumn = 'CFW.Fld_Value'   
        END                                     
    ELSE 
        IF @columnName LIKE 'DCust%' 
            BEGIN            
                SET @strSql = @strSql
                    + ' left Join CFW_Fld_Values_Opp CFW on CFW.RecId=Opp.numOppid   and CFW.fld_id= '
                    + REPLACE(@columnName, 'DCust', '')                                                                                               
                SET @strSql = @strSql
                    + ' left Join ListDetails LstCF on LstCF.numListItemID=CFW.Fld_Value  '            
                SET @strOrderColumn = 'LstCF.vcData'  
            END       

             
    IF @tintFilterBy = 1 --Partially Fulfilled Orders 
        SET @strSql = @strSql
            + ' left join AdditionalContactsInformation ADC1 on ADC1.numContactId=Opp.numRecOwner                                                               
                             LEFT OUTER JOIN [OpportunityBizDocs] OB ON OB.[numOppId] = Opp.[numOppId] 
                             WHERE Opp.numDomainID='
            + CONVERT(VARCHAR(15), @numDomainID)
            + ' and Opp.tintOppstatus = 1 and Opp.tintShipped = '
            + CONVERT(VARCHAR(15), @tintShipped)
    ELSE 
        SET @strSql = @strSql
            + ' left join AdditionalContactsInformation ADC1 on ADC1.numContactId=Opp.numRecOwner                                                               
                             WHERE Opp.numDomainID='
            + CONVERT(VARCHAR(15), @numDomainID)
            + ' and Opp.tintOppstatus = 1 and Opp.tintShipped = '
            + CONVERT(VARCHAR(15), @tintShipped) 
     
    IF @intOrder <> 0 
        BEGIN
            IF @intOrder = 1 
                SET @strSql = @strSql + ' and Opp.bitOrder = 0 '
            IF @intOrder = 2 
                SET @strSql = @strSql + ' and Opp.bitOrder = 1'
        END
        
    if @numDivisionID <> 0 
		SET @strSql = @strSql + ' and Div.numDivisionID = ' + CONVERT(VARCHAR(15),@numDivisionID)                        
    
    IF @SortChar <> '0' 
        SET @strSql = @strSql + ' And Opp.vcPOppName like ''' + @SortChar
            + '%'''
 
   IF @tintSortOrder <> '0' 
        SET @strSql = @strSql + '  AND Opp.tintOppType = '
            + CONVERT(VARCHAR(1), @tintSortOrder)
    
    
    IF @tintFilterBy = 1 --Partially Fulfilled Orders
        SET @strSql = @strSql
            + ' AND OB.bitPartialFulfilment = 1 AND OB.bitAuthoritativeBizDocs = 1 '
    ELSE 
        IF @tintFilterBy = 2 --Fulfilled Orders
            SET @strSql = @strSql
                + ' AND Opp.[numOppId] IN (SELECT OM.numOppId FROM [OpportunityMaster] OM
                      			INNER JOIN [OpportunityBizDocs] OB ON OB.[numOppId] = OM.[numOppId]
                      			INNER JOIN [OpportunityItems] OI ON OM.[numOppId] = OI.[numOppId]
                      			INNER JOIN [OpportunityBizDocItems] BI ON BI.[numOppBizDocID] = OB.[numOppBizDocsId]
                      			Where OM.numDomainId='
                + CONVERT(VARCHAR(15), @numDomainId)
                + 'AND OB.[bitAuthoritativeBizDocs] = 1 GROUP BY OM.[numOppId]
                      			HAVING   COUNT(OI.[numUnitHour]) = COUNT(BI.[numUnitHour])) '
        ELSE 
            IF @tintFilterBy = 3 --Orders without Authoritative-BizDocs
                SET @strSql = @strSql
                    + ' AND Opp.[numOppId] not IN (SELECT DISTINCT OM.[numOppId]
                      		  FROM [OpportunityMaster] OM JOIN [OpportunityBizDocs] OB ON OM.[numOppId] = OB.[numOppId]
                      		  Where OM.numDomainId='
                    + CONVERT(VARCHAR(15), @numDomainId)
                    + ' AND OM.tintOppstatus=1 
                      		    AND OM.tintShipped = '
                    + CONVERT(VARCHAR(15), @tintShipped)
                    + ' AND OM.tintOppType = '
                    + CONVERT(VARCHAR(1), @tintSortOrder)
                    + ' AND isnull(OB.[bitAuthoritativeBizDocs],0)=1) '

            ELSE 
                IF @tintFilterBy = 4 --Orders with items yet to be added to Invoices
                    SET @strSql = @strSql
                        + ' AND Opp.[numOppId] IN (SELECT distinct OPPM.numOppId FROM [OpportunityMaster] OPPM
                                  INNER JOIN [OpportunityItems] OPPI ON OPPM.[numOppId] = OPPI.[numOppId]
                                  Where OPPM.numDomainId='
                        + CONVERT(VARCHAR(15), @numDomainId)
                        + ' 
								  and OPPI.numoppitemtCode not in (select numOppItemID from [OpportunityBizDocs] OB INNER JOIN [OpportunityBizDocItems] BI
                      			  ON BI.[numOppBizDocID] = OB.[numOppBizDocsId] where OB.[numOppId] = OPPM.[numOppId]))'      
                ELSE 
                    IF @tintFilterBy = 6 --Orders with deferred Income/Invoices
                        SET @strSql = @strSql
                            + ' AND Opp.[numOppId] IN (SELECT DISTINCT OM.[numOppId]
                      		 FROM [OpportunityMaster] OM LEFT OUTER JOIN [OpportunityBizDocs] OB ON OM.[numOppId] = OB.[numOppId]
                      		 Where OM.numDomainId='
                            + CONVERT(VARCHAR(15), @numDomainId)
                            + ' AND OB.[bitAuthoritativeBizDocs] =1 and OB.tintDeferred=1)'

    IF @numOrderStatus <> 0 
        SET @strSql = @strSql + ' AND Opp.numStatus = '
            + CONVERT(VARCHAR(15), @numOrderStatus) ;


    IF @vcRegularSearchCriteria <> '' 
        SET @strSql = @strSql + ' and ' + @vcRegularSearchCriteria 
	
    IF @vcCustomSearchCriteria <> '' 
        SET @strSql = @strSql
            + ' and Opp.numOppid in (select distinct CFW.RecId from CFW_Fld_Values_Opp CFW where '
            + @vcCustomSearchCriteria + ')'

    IF LEN(@strOrderColumn) > 0 
        SET @strSql = @strSql + ' ORDER BY  ' + @strOrderColumn + ' '
            + @columnSortOrder    

	
    PRINT @strSql
    INSERT  INTO #tempTable
            (
              numOppId,
              intTotalProgress
            )
            EXEC ( @strSql
                )
	
	DECLARE @firstRec AS INTEGER
    DECLARE @lastRec AS INTEGER
    SET @firstRec = ( @CurrentPage - 1 ) * @PageSize
    SET @lastRec = ( @CurrentPage * @PageSize + 1 )
  
    SET @TotRecs = ( SELECT COUNT(*)
                     FROM   #tempTable
                   )
  
    DECLARE @tintOrder AS TINYINT ;
    SET @tintOrder = 0
    DECLARE @vcFieldName AS VARCHAR(50)
    DECLARE @vcListItemType AS VARCHAR(3)
    DECLARE @vcListItemType1 AS VARCHAR(1)
    DECLARE @vcAssociatedControlType VARCHAR(30)
    DECLARE @numListID AS NUMERIC(9)
    DECLARE @vcDbColumnName VARCHAR(40)
    DECLARE @WhereCondition VARCHAR(2000) ;
    SET @WhereCondition = ''
    DECLARE @vcLookBackTableName VARCHAR(2000)
    DECLARE @bitCustom AS BIT
    DECLARE @numFieldId AS NUMERIC  
    DECLARE @bitAllowSorting AS CHAR(1)   
    DECLARE @bitAllowEdit AS CHAR(1)                 
    DECLARE @vcColumnName AS VARCHAR(500)                  
    DECLARE @Nocolumns AS TINYINT ;
    SET @Nocolumns = 0

    SELECT  @Nocolumns = ISNULL(SUM(TotalRow), 0)
    FROM    ( SELECT    COUNT(*) TotalRow
              FROM      View_DynamicColumns
              WHERE     numFormId = @numFormId
                        --AND numUserCntID = @numUserCntID
                        AND numDomainID = @numDomainID
                        AND tintPageType = 1
                        --AND numRelCntType = @numFormId
              UNION
              SELECT    COUNT(*) TotalRow
              FROM      View_DynamicCustomColumns
              WHERE     numFormId = @numFormId
                        --AND numUserCntID = @numUserCntID
                        AND numDomainID = @numDomainID
                        AND tintPageType = 1
                        --AND numRelCntType = @numFormId
            ) TotalRows

	PRINT '@Nocolumns: ' + CAST(@Nocolumns AS VARCHAR(10))
	
    CREATE TABLE #tempForm
        (
          tintOrder TINYINT,
          vcDbColumnName NVARCHAR(50),
          vcOrigDbColumnName NVARCHAR(50),
          vcFieldName NVARCHAR(50),
          vcAssociatedControlType NVARCHAR(50),
          vcListItemType VARCHAR(3),
          numListID NUMERIC(9),
          vcLookBackTableName VARCHAR(50),
          bitCustomField BIT,
          numFieldId NUMERIC,
          bitAllowSorting BIT,
          bitAllowEdit BIT,
          bitIsRequired BIT,
          bitIsEmail BIT,
          bitIsAlphaNumeric BIT,
          bitIsNumeric BIT,
          bitIsLengthValidation BIT,
          intMaxLength INT,
          intMinLength INT,
          bitFieldMessage BIT,
          vcFieldMessage VARCHAR(500),
          ListRelID NUMERIC(9),
          intColumnWidth INT,
          bitAllowFiltering BIT,
          vcFieldDataType CHAR(1)
        )
    
    SET @strSql = ''
    SET @strSql = 'select ADC.numContactId,DM.numDivisionID,isnull(DM.numTerID,0) numTerID,opp.numRecOwner as numRecOwner,DM.tintCRMType,opp.numOppId'

    IF @Nocolumns = 0 
        BEGIN
            INSERT  INTO DycFormConfigurationDetails
                    (
                      numFormID,
                      numFieldID,
                      intColumnNum,
                      intRowNum,
                      numDomainID,
                      numUserCntID,
                      numRelCntType,
                      tintPageType,
                      bitCustom,
                      intColumnWidth
                    )
                    SELECT  @numFormId,
                            numFieldId,
                            0,
                            Row_number() OVER ( ORDER BY tintRow DESC ),
                            @numDomainID,
                            0,
                            0,
                            1,
                            0,
                            intColumnWidth
                    FROM    View_DynamicDefaultColumns
                    WHERE   numFormId = @numFormId
                            AND bitDefault = 1
                            AND ISNULL(bitSettingField, 0) = 1
                            AND numDomainID = @numDomainID
                    ORDER BY tintOrder ASC   

        END

    INSERT  INTO #tempForm
            SELECT  tintRow + 1 AS tintOrder,
                    vcDbColumnName,
                    vcOrigDbColumnName,
                    ISNULL(vcCultureFieldName, vcFieldName),
                    vcAssociatedControlType,
                    vcListItemType,
                    numListID,
                    vcLookBackTableName,
                    bitCustom,
                    numFieldId,
                    bitAllowSorting,
                    bitInlineEdit,
                    bitIsRequired,
                    bitIsEmail,
                    bitIsAlphaNumeric,
                    bitIsNumeric,
                    bitIsLengthValidation,
                    intMaxLength,
                    intMinLength,
                    bitFieldMessage,
                    vcFieldMessage vcFieldMessage,
                    ListRelID,
                    intColumnWidth,
                    bitAllowFiltering,
                    vcFieldDataType
            FROM    View_DynamicColumns
            WHERE   numFormId = @numFormId
                    --AND numUserCntID = @numUserCntID
                    AND numDomainID = @numDomainID
                    AND tintPageType = 1
                    AND ISNULL(bitSettingField, 0) = 1
                    AND ISNULL(bitCustom, 0) = 0
                    --AND numRelCntType = @numFormId
            UNION
            SELECT  tintRow + 1 AS tintOrder,
                    vcDbColumnName,
                    vcDbColumnName,
                    vcFieldName,
                    vcAssociatedControlType,
                    '' AS vcListItemType,
                    numListID,
                    '',
                    bitCustom,
                    numFieldId,
                    bitAllowSorting,
                    bitAllowEdit,
                    bitIsRequired,
                    bitIsEmail,
                    bitIsAlphaNumeric,
                    bitIsNumeric,
                    bitIsLengthValidation,
                    intMaxLength,
                    intMinLength,
                    bitFieldMessage,
                    vcFieldMessage,
                    ListRelID,
                    intColumnWidth,
                    0,
                    ''
            FROM    View_DynamicCustomColumns
            WHERE   numFormId = @numFormId
                    --AND numUserCntID = @numUserCntID
                    AND numDomainID = @numDomainID
                    AND tintPageType = 1
                    AND ISNULL(bitCustom, 0) = 1
                    --AND numRelCntType = @numFormId
            ORDER BY tintOrder ASC  
    
    DECLARE @ListRelID AS NUMERIC(9) 

    SELECT TOP 1
            @tintOrder = tintOrder + 1,
            @vcDbColumnName = vcDbColumnName,
            @vcFieldName = vcFieldName,
            @vcAssociatedControlType = vcAssociatedControlType,
            @vcListItemType = vcListItemType,
            @numListID = numListID,
            @vcLookBackTableName = vcLookBackTableName,
            @bitCustom = bitCustomField,
            @numFieldId = numFieldId,
            @bitAllowSorting = bitAllowSorting,
            @bitAllowEdit = bitAllowEdit,
            @ListRelID = ListRelID
    FROM    #tempForm
--    ORDER BY tintOrder ASC            

	--SELECT TOP 1 * FROM    #tempForm ORDER BY tintOrder ASC            

    WHILE @tintOrder > 0
        BEGIN
            IF @bitCustom = 0 
                BEGIN
                    DECLARE @Prefix AS VARCHAR(5)
                    IF @vcLookBackTableName = 'AdditionalContactsInformation' 
                        SET @Prefix = 'ADC.'
                    IF @vcLookBackTableName = 'DivisionMaster' 
                        SET @Prefix = 'DM.'
                    IF @vcLookBackTableName = 'OpportunityMaster' 
                        SET @PreFix = 'Opp.'
                    IF @vcLookBackTableName = 'OpportunityRecurring' 
                        SET @PreFix = 'OPR.'
            
                    SET @vcColumnName = @vcDbColumnName + '~'
                        + CONVERT(VARCHAR(10), @numFieldId) + '~0'
            
                    IF @vcAssociatedControlType = 'SelectBox' 
                        BEGIN
                            IF @vcDbColumnName = 'tintSource' 
                                BEGIN
                                    SET @strSql = @strSql
                                        + ',ISNULL(dbo.fn_GetOpportunitySourceValue(ISNULL(Opp.tintSource,0),ISNULL(Opp.tintSourceType,0),Opp.numDomainID),''Internal Order'')'
                                        + ' [' + @vcColumnName + ']'
                                END
            
                            ELSE 
                                IF @vcDbColumnName = 'numCampainID' 
                                    BEGIN
                                        SET @strSql = @strSql
                                            + ', (SELECT vcCampaignName FROM CampaignMaster WHERE numCampaignID= Opp.numCampainID) '
                                            + ' [' + @vcColumnName + ']'
                                    END
            
                                ELSE 
                                    IF @vcListItemType = 'LI' 
                                        BEGIN
                                            SET @strSql = @strSql + ',L'
                                                + CONVERT(VARCHAR(3), @tintOrder)
                                                + '.vcData' + ' ['
                                                + @vcColumnName + ']'
                                            SET @WhereCondition = @WhereCondition
                                                + ' left Join ListDetails L'
                                                + CONVERT(VARCHAR(3), @tintOrder)
                                                + ' on L'
                                                + CONVERT(VARCHAR(3), @tintOrder)
                                                + '.numListItemID='
                                                + @vcDbColumnName
                                        END
                                    ELSE 
                                        IF @vcListItemType = 'S' 
                                            BEGIN
                                                SET @strSql = @strSql + ',S'
                                                    + CONVERT(VARCHAR(3), @tintOrder)
                                                    + '.vcState' + ' ['
                                                    + @vcColumnName + ']'
                                                SET @WhereCondition = @WhereCondition
                                                    + ' left join State S'
                                                    + CONVERT(VARCHAR(3), @tintOrder)
                                                    + ' on S'
                                                    + CONVERT(VARCHAR(3), @tintOrder)
                                                    + '.numStateID='
                                                    + @vcDbColumnName
                                            END
                                        ELSE 
                                            IF @vcListItemType = 'BP' 
                                                BEGIN
                                                    SET @strSql = @strSql
                                                        + ',SPLM.Slp_Name'
                                                        + ' [' + @vcColumnName
                                                        + ']'
                                                    SET @WhereCondition = @WhereCondition
                                                        + ' LEFT JOIN Sales_process_List_Master SPLM on SPLM.Slp_Id = OPP.numBusinessProcessID '
                                                END
                                            ELSE 
                                                IF @vcListItemType = 'PP' 
                                                    BEGIN
                                                        SET @strSql = @strSql
                                                            + ',ISNULL(T.intTotalProgress,0)'
                                                            + ' ['
                                                            + @vcColumnName
                                                            + ']'

                                                    END
                                                ELSE 
                                                    IF @vcListItemType = 'T' 
                                                        BEGIN
                                                            SET @strSql = @strSql + ',L' + CONVERT(VARCHAR(3), @tintOrder) + '.vcData' + ' [' + @vcColumnName + ']'
                                                            SET @WhereCondition = @WhereCondition + ' left Join ListDetails L' + CONVERT(VARCHAR(3), @tintOrder) + ' on L' + CONVERT(VARCHAR(3), @tintOrder) + '.numListItemID=' + @vcDbColumnName
                                                        END
                                                    ELSE 
                                                        IF @vcListItemType = 'U' 
                                                            BEGIN
                                                                SET @strSql = @strSql + ',dbo.fn_GetContactName(' + @Prefix + @vcDbColumnName + ') [' + @vcColumnName + ']'
                                                            END
                                                        ELSE 
                                                            IF @vcListItemType = 'WI' 
                                                                BEGIN
                                                                    SET @strSql = @strSql + ', Case when isnull(OL.numSiteID,0)>0 then Sites.vcSiteName else WebAPI.vcProviderName end [' + @vcColumnName + ']'
                                                                    SET @WhereCondition = @WhereCondition + ' left JOIN OpportunityLinking OL on OL.numChildOppID=Opp.numOppId left join WebAPI on WebAPI.WebApiId=isnull(OL.numWebApiId,0)
													left join Sites on isnull(OL.numSiteID,0)=Sites.numSIteID and Sites.numDomainID=' + CONVERT(VARCHAR(15), @numDomainID)                                                       
                                                                END 
                        END
                    ELSE 
                        IF @vcAssociatedControlType = 'DateField' 
                            BEGIN
                                IF @Prefix = 'OPR.' 
                                    BEGIN
                                        SET @strSql = @strSql
                                            + ', (SELECT TOP 1 dbo.FormatedDateFromDate( DateAdd(minute,+'
                                            + CONVERT(VARCHAR(15), -@ClientTimeZoneOffset)
                                            + ', dtRecurringDate)' + ','
                                            + CONVERT(VARCHAR(10), @numDomainId)
                                            + ') FROM  dbo.OpportunityRecurring WHERE numOppID=OPP.numOppID AND ISNULL(numOppBizDocID,0)=0 and dtRecurringDate > dbo.GetUTCDateWithoutTime()) AS  ['
                                            + @vcColumnName + ']'
                                    END
                                ELSE 
                                    BEGIN
                                        SET @strSql = @strSql
                                            + ',case when convert(varchar(11),DateAdd(minute, '
                                            + CONVERT(VARCHAR(15), -@ClientTimeZoneOffset)
                                            + ',' + @Prefix + @vcDbColumnName
                                            + ') )= convert(varchar(11),getdate()) then ''<b><font color=red>Today</font></b>'''
                                        SET @strSql = @strSql
                                            + 'when convert(varchar(11),DateAdd(minute, '
                                            + CONVERT(VARCHAR(15), -@ClientTimeZoneOffset)
                                            + ',' + @Prefix + @vcDbColumnName
                                            + ') )= convert(varchar(11),dateadd(day,-1,getdate())) then ''<b><font color=purple>YesterDay</font></b>'''
                                        SET @strSql = @strSql
                                            + 'when convert(varchar(11),DateAdd(minute, '
                                            + CONVERT(VARCHAR(15), -@ClientTimeZoneOffset)
                                            + ',' + @Prefix + @vcDbColumnName
                                            + ') )= convert(varchar(11),dateadd(day,1,getdate())) then''<b><font color=orange>Tommorow</font></b>'' '
                                        SET @strSql = @strSql
                                            + 'else dbo.FormatedDateFromDate('
                                            + @Prefix + @vcDbColumnName + ','
                                            + CONVERT(VARCHAR(10), @numDomainId)
                                            + ') end  [' + @vcColumnName + ']' 	
                                    END
                            END
                        ELSE 
                            IF @vcAssociatedControlType = 'TextBox' 
                                BEGIN
                                    SET @strSql = @strSql + ','
                                        + CASE WHEN @vcDbColumnName = 'numAge'
                                               THEN 'year(getutcdate())-year(bintDOB)'
                                               WHEN @vcDbColumnName = 'monPAmount'
                                               THEN 'isnull((SELECT varCurrSymbol FROM [Currency] WHERE numCurrencyID = Opp.[numCurrencyID]),'''') + '' ''+ CONVERT(VARCHAR,CONVERT(DECIMAL(20,5),monPAmount),1)'
                                               WHEN @vcDbColumnName = 'numDivisionID'
                                               THEN 'DM.numDivisionID'
                                               WHEN @vcDbColumnName = 'tintOppStatus'
                                               THEN 'CASE tintOppStatus WHEN 0 then ''Open'' WHEN 1 THEN ''Closed-Won'' WHEN 2 THEN ''Closed-Lost'' END '
                                               WHEN @vcDbColumnName = 'tintOppType'
                                               THEN 'CASE tintOppType WHEN 1 then ''Sales'' WHEN 2 THEN ''Purchase'' Else ''NA'' END '
                                               WHEN @vcDbColumnName = 'vcPOppName'
                                               THEN 'Opp.vcPOppName'
                                               ELSE @vcDbColumnName
                                          END + ' [' + @vcColumnName + ']'
                                END
                            ELSE 
                                IF @vcAssociatedControlType = 'TextArea' 
                                    BEGIN
                                        SET @strSql = @strSql + ',' + @Prefix
                                            + @vcDbColumnName + ' ['
                                            + @vcColumnName + ']'  
                                    END
                                ELSE 
                                    IF @vcAssociatedControlType = 'Label' 
                                        BEGIN  
                                            SET @strSql = @strSql + ','
                                                + CASE WHEN @vcDbColumnName = 'CalAmount'
                                                       THEN 'Opp.monDealAmount'--'[dbo].[getdealamount](Opp.numOppId,Getutcdate(),0)'
                                                       WHEN @vcLookBackTableName = 'OpportunityBizDocs'
                                                            AND @vcDbColumnName = 'monDealAmount'
                                                       THEN 'ISNULL((SELECT SUM(ISNULL(monDealAmount,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)'
                                                       WHEN @vcLookBackTableName = 'OpportunityBizDocs'
                                                            AND @vcDbColumnName = 'monAmountPaid'
                                                       THEN 'ISNULL((SELECT SUM(ISNULL(monAmountPaid,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)'
                                                       WHEN @vcLookBackTableName = 'OpportunityBizDocs'
                                                            AND @vcDbColumnName = 'vcBizDocsList'
                                                       THEN 'ISNULL((SELECT SUBSTRING((SELECT ''$^$'' + CAST(numOppBizDocsId AS VARCHAR(18)) +''#^#''+ vcBizDocID
	FROM OpportunityBizDocs WHERE numOppId=Opp.numOppId FOR XML PATH('''')),4,200000)),'''')'
                                                       ELSE @vcDbColumnName
                                                  END + ' [' + @vcColumnName
                                                + ']'            
                                        END

                                    ELSE 
                                        IF @vcAssociatedControlType = 'Popup'
                                            AND @vcDbColumnName = 'numShareWith' 
                                            BEGIN      
                                                SET @strSql = @strSql
                                                    + ',(SELECT SUBSTRING(
	(SELECT '','' + A.vcFirstName+'' ''+A.vcLastName + CASE WHEN ISNULL(SR.numContactType,0)>0 THEN ''('' + dbo.fn_GetListItemName(SR.numContactType) + '')'' ELSE '''' END
	FROM ShareRecord SR JOIN UserMaster UM ON UM.numUserDetailId=SR.numAssignedTo 
							 JOIN AdditionalContactsInformation A ON UM.numUserDetailId=A.numContactID 
		 WHERE SR.numDomainID=Opp.numDomainID AND SR.numModuleID=3 AND SR.numRecordID=Opp.numOppId
				AND UM.numDomainID=Opp.numDomainID and UM.numDomainID=A.numDomainID FOR XML PATH('''')),2,200000)) ['
                                                    + @vcColumnName + ']'                
                                            END		

                END
            ELSE 
                BEGIN
        
                    SET @vcColumnName = @vcDbColumnName + '~'
                        + CONVERT(VARCHAR(10), @numFieldId) + '~1'

                    IF @vcAssociatedControlType = 'TextBox' OR @vcAssociatedControlType = 'TextArea' 
                        BEGIN
                            SET @strSql = @strSql + ',CFW'
                                + CONVERT(VARCHAR(3), @tintOrder)
                                + '.Fld_Value  [' + @vcColumnName + ']'
                            SET @WhereCondition = @WhereCondition
                                + ' left Join CFW_FLD_Values_Opp CFW'
                                + CONVERT(VARCHAR(3), @tintOrder)
                                + '               
                                                                                                                            on CFW'
                                + CONVERT(VARCHAR(3), @tintOrder) + '.Fld_Id='
                                + CONVERT(VARCHAR(10), @numFieldId)
                                + 'and CFW' + CONVERT(VARCHAR(3), @tintOrder)
                                + '.RecId=Opp.numOppid   '
                        END
                    ELSE 
                        IF @vcAssociatedControlType = 'CheckBox' 
                            BEGIN
							
                                SET @strSql = @strSql 
                                    + ',case when isnull(CFW' + CONVERT(VARCHAR(3), @tintOrder) 
                                    + '.Fld_Value,0)=0 then ''No'' when isnull(CFW' + CONVERT(VARCHAR(3), @tintOrder) 
									+ '.Fld_Value,0)=1 then ''Yes'' end   [' + @vcColumnName + ']'

                                SET @WhereCondition = @WhereCondition
                                    + ' left Join CFW_FLD_Values_Opp CFW'
                                    + CONVERT(VARCHAR(3), @tintOrder) + ' on CFW'
                                    + CONVERT(VARCHAR(3), @tintOrder) + '.Fld_Id='
                                    + CONVERT(VARCHAR(10), @numFieldId) + 'and CFW'
                                    + CONVERT(VARCHAR(3), @tintOrder) + '.RecId=Opp.numOppid   '
                            END
                        ELSE 
                            IF @vcAssociatedControlType = 'DateField' 
                                BEGIN
                                    SET @strSql = @strSql
                                        + ',dbo.FormatedDateFromDate(CFW'
                                        + CONVERT(VARCHAR(3), @tintOrder)
                                        + '.Fld_Value,'
                                        + CONVERT(VARCHAR(10), @numDomainId)
                                        + ')  [' + @vcColumnName + ']'
                                    SET @WhereCondition = @WhereCondition
                                        + ' left Join CFW_FLD_Values_Opp CFW'
                                        + CONVERT(VARCHAR(3), @tintOrder)
                                        + '               
                                                                                                                                        on CFW'
                                        + CONVERT(VARCHAR(3), @tintOrder)
                                        + '.Fld_Id='
                                        + CONVERT(VARCHAR(10), @numFieldId)
                                        + 'and CFW'
                                        + CONVERT(VARCHAR(3), @tintOrder)
                                        + '.RecId=Opp.numOppid    '
                                END
                            ELSE 
                                IF @vcAssociatedControlType = 'SelectBox' 
                                    BEGIN
                                        SET @vcDbColumnName = 'DCust'
                                            + CONVERT(VARCHAR(10), @numFieldId)
                                        SET @strSql = @strSql + ',L'
                                            + CONVERT(VARCHAR(3), @tintOrder)
                                            + '.vcData' + ' [' + @vcColumnName
                                            + ']'
                                        SET @WhereCondition = @WhereCondition
                                            + ' left Join CFW_FLD_Values_Opp CFW'
                                            + CONVERT(VARCHAR(3), @tintOrder)
                                            + '               
                                                                                                                                               on CFW'
                                            + CONVERT(VARCHAR(3), @tintOrder)
                                            + '.Fld_Id='
                                            + CONVERT(VARCHAR(10), @numFieldId)
                                            + 'and CFW'
                                            + CONVERT(VARCHAR(3), @tintOrder)
                                            + '.RecId=Opp.numOppid     '
                                        SET @WhereCondition = @WhereCondition
                                            + ' left Join ListDetails L'
                                            + CONVERT(VARCHAR(3), @tintOrder)
                                            + ' on L'
                                            + CONVERT(VARCHAR(3), @tintOrder)
                                            + '.numListItemID=CFW'
                                            + CONVERT(VARCHAR(3), @tintOrder)
                                            + '.Fld_Value'
                                    END
                END
      
     
            SELECT TOP 1
                    @tintOrder = tintOrder + 1,
                    @vcDbColumnName = vcDbColumnName,
                    @vcFieldName = vcFieldName,
                    @vcAssociatedControlType = vcAssociatedControlType,
                    @vcListItemType = vcListItemType,
                    @numListID = numListID,
                    @vcLookBackTableName = vcLookBackTableName,
                    @bitCustom = bitCustomField,
                    @numFieldId = numFieldId,
                    @bitAllowSorting = bitAllowSorting,
                    @bitAllowEdit = bitAllowEdit,
                    @ListRelID = ListRelID
            FROM    #tempForm
            WHERE   tintOrder > @tintOrder - 1
            ORDER BY tintOrder ASC            
 
            IF @@rowcount = 0 
                SET @tintOrder = 0 
        END 
  
  -------Change Row Color-------
    DECLARE @vcCSOrigDbCOlumnName AS VARCHAR(50),
        @vcCSLookBackTableName AS VARCHAR(50),
        @vcCSAssociatedControlType AS VARCHAR(50)
    SET @vcCSOrigDbCOlumnName = ''
    SET @vcCSLookBackTableName = ''

    CREATE TABLE #tempColorScheme
        (
          vcOrigDbCOlumnName VARCHAR(50),
          vcLookBackTableName VARCHAR(50),
          vcFieldValue VARCHAR(50),
          vcFieldValue1 VARCHAR(50),
          vcColorScheme VARCHAR(50),
          vcAssociatedControlType VARCHAR(50)
        )

    INSERT  INTO #tempColorScheme
            SELECT  DFM.vcOrigDbCOlumnName,
                    DFM.vcLookBackTableName,
                    DFCS.vcFieldValue,
                    DFCS.vcFieldValue1,
                    DFCS.vcColorScheme,
                    DFFM.vcAssociatedControlType
            FROM    DycFieldColorScheme DFCS
                    JOIN DycFormField_Mapping DFFM ON DFCS.numFieldID = DFFM.numFieldID
                    JOIN DycFieldMaster DFM ON DFM.numModuleID = DFFM.numModuleID
                                               AND DFM.numFieldID = DFFM.numFieldID
            WHERE   DFCS.numDomainID = @numDomainID
                    AND DFFM.numFormID = @numFormId
                    AND DFCS.numFormID = @numFormId
                    AND ISNULL(DFFM.bitAllowGridColor, 0) = 1

    IF ( SELECT COUNT(*)
         FROM   #tempColorScheme
       ) > 0 
        BEGIN
            SELECT TOP 1
                    @vcCSOrigDbCOlumnName = vcOrigDbCOlumnName,
                    @vcCSLookBackTableName = vcLookBackTableName,
                    @vcCSAssociatedControlType = vcAssociatedControlType
            FROM    #tempColorScheme
        END   
----------------------------                       

    IF LEN(@vcCSOrigDbCOlumnName) > 0
        AND LEN(@vcCSLookBackTableName) > 0 
        BEGIN
            SET @strSql = @strSql + ',tCS.vcColorScheme'
        END

  
    SET @strSql = @strSql
        + ' FROM OpportunityMaster Opp                                                               
                            INNER JOIN AdditionalContactsInformation ADC ON Opp.numContactId = ADC.numContactId                                                               
							INNER JOIN DivisionMaster DM ON Opp.numDivisionId = DM.numDivisionID AND ADC.numDivisionId = DM.numDivisionID                                                               
                            INNER JOIN CompanyInfo cmp ON DM.numCompanyID = cmp.numCompanyId '
  
    IF LEN(@vcCSOrigDbCOlumnName) > 0
        AND LEN(@vcCSLookBackTableName) > 0 
        BEGIN
            IF @vcCSLookBackTableName = 'AdditionalContactsInformation' 
                SET @Prefix = 'ADC.'                  
            IF @vcCSLookBackTableName = 'DivisionMaster' 
                SET @Prefix = 'DM.'
            IF @vcCSLookBackTableName = 'CompanyInfo' 
                SET @Prefix = 'CMP.'   
            IF @vcCSLookBackTableName = 'OpportunityMaster' 
                SET @Prefix = 'Opp.'   
 
            IF @vcCSAssociatedControlType = 'DateField' 
                BEGIN
                    SET @strSql = @strSql
                        + ' left join #tempColorScheme tCS on CONVERT(VARCHAR(10),'
                        + @Prefix + @vcCSOrigDbCOlumnName
                        + ',111) >= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue as int),GETUTCDATE()),111)
			 and CONVERT(VARCHAR(10),' + @Prefix + @vcCSOrigDbCOlumnName
                        + ',111) <= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue1 as int),GETUTCDATE()),111)'
                END
            ELSE 
                IF @vcCSAssociatedControlType = 'SelectBox'
                    OR @vcCSAssociatedControlType = 'ListBox'
                    OR @vcCSAssociatedControlType = 'CheckBox' 
                    BEGIN
                        SET @strSql = @strSql
                            + ' left join #tempColorScheme tCS on tCS.vcFieldValue='
                            + @Prefix + @vcCSOrigDbCOlumnName
                    END
        END
                          
    IF @columnName LIKE 'CFW.Cust%' 
        BEGIN            
            SET @strSql = @strSql
                + ' left Join CFW_Fld_Values_Opp CFW on CFW.RecId=Opp.numOppid and CFW.fld_id= '
                + REPLACE(@columnName, 'CFW.Cust', '') + ' '                                                     
            SET @columnName = 'CFW.Fld_Value'            
        END 

    SET @strSql = @strSql + @WhereCondition
        + ' join (select ID,numOppId,intTotalProgress FROM  #tempTable )T on T.numOppId=Opp.numOppId                                              
                          WHERE ID > ' + CONVERT(VARCHAR(10), @firstRec)
        + ' and ID <' + CONVERT(VARCHAR(10), @lastRec)
    
    IF ( @tintFilterBy = 5 ) --Sort alphabetically
        SET @strSql = @strSql + ' order by ID '
    ELSE 
        IF ( @columnName = 'numAssignedBy'
             OR @columnName = 'numAssignedTo'
           ) 
            SET @strSql = @strSql + ' ORDER BY  opp.' + @columnName + ' '
                + @columnSortOrder 
        ELSE 
            IF ( @columnName = 'PP.intTotalProgress' ) 
                SET @strSql = @strSql + ' ORDER BY  T.intTotalProgress '
                    + @columnSortOrder 
            ELSE 
                IF @columnName <> 'opp.bintCreatedDate'
                    AND @columnName <> 'vcPOppName'
                    AND @columnName <> 'bintCreatedDate'
                    AND @columnName <> 'vcCompanyName' 
                    SET @strSql = @strSql + ' ORDER BY  ' + @columnName + ' '
                        + @columnSortOrder
                ELSE 
                    SET @strSql = @strSql + ' order by ID '

                                             
    PRINT @strSql                      
              
    EXEC ( @strSql
        )  

    SELECT  *
    FROM    #tempForm

    DROP TABLE #tempForm

    DROP TABLE #tempColorScheme

    DROP TABLE #tempTable
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetDocumentFilesLink')
	DROP PROCEDURE USP_GetDocumentFilesLink
GO
CREATE PROCEDURE [dbo].[USP_GetDocumentFilesLink]    
	@numRecID AS NUMERIC(18) = 0,    
	@numDomainID AS NUMERIC(18) = 0,
	@calledFrom AS CHAR = NULL
AS
BEGIN   
	IF @calledFrom = 'P'
		SELECT 
			numGenericDocID
			,VcFileName
			,cUrlType
			,(CASE WHEN ISNULL(VcDocName,'') = '' THEN '-' ELSE VcDocName END) VcDocName
		FROM 
			GenericDocuments
		WHERE 
			numDomainID = @numDomainID
			AND numRecID = @numRecID 
			AND ISNULL(@numRecID,0) > 0
			AND ISNULL(vcFileName,'') <> ''
	ELSE IF @calledFrom = 'T'
		SELECT 
			numGenericDocID
			,VcFileName
			,cUrlType
			,(CASE WHEN ISNULL(VcDocName,'') = '' THEN '-' ELSE VcDocName END) VcDocName
		FROM 
			GenericDocuments
		WHERE 
			numDomainID = @numDomainID
			AND numRecID = @numRecID 
			AND ISNULL(@numRecID,0) > 0
			AND vcDocumentSection = 'T'
			AND ISNULL(vcFileName,'') <> ''
	ELSE IF @calledFrom = 'O'
		SELECT 
			numGenericDocID
			,VcFileName
			,cUrlType
			,(CASE WHEN ISNULL(VcDocName,'') = '' THEN '-' ELSE VcDocName END) VcDocName
		FROM 
			GenericDocuments
		WHERE 
			numDomainID = @numDomainID
			AND numRecID = @numRecID 
			AND ISNULL(@numRecID,0) > 0
			AND vcDocumentSection = 'O'
			AND ISNULL(vcFileName,'') <> ''
	ELSE IF @calledFrom = 'E'
		SELECT 
			numGenericDocID
			,VcFileName
			,cUrlType
			,VcDocName
		FROM 
			GenericDocuments
		WHERE 
			numDomainID = @numDomainID
			AND numRecID = @numRecID
			AND cURLType='L' 

	
END
GO
/****** Object:  StoredProcedure [dbo].[usp_GetDocumentsForOpp]    Script Date: 07/26/2008 16:17:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getdocumentsforopp')
DROP PROCEDURE usp_getdocumentsforopp
GO
CREATE PROCEDURE [dbo].[usp_GetDocumentsForOpp]
@numOppID numeric(9)=0,
@numDocID numeric(9)=0   
--
AS
BEGIN

	if @numDocID=0
	begin
	select vcFileName,vcDisplayName,numSize,bintDateOfUpload,numDocID,numuserid from opptdocuments OD INNER JOIN DivisionMaster DM ON OD.numDivisionID=DM.numDivisionID
	 where OD.numOppID=@numOppID 
	Order By OD.vcDisplayName
	end
	else
	begin
		select vcFileName,vcDisplayName,numSize,bintDateOfUpload,numDocID,numuserid from opptdocuments OD INNER JOIN DivisionMaster DM ON OD.numDivisionID=DM.numDivisionID
		 where OD.numDocID=@numDocID
		 
	end
	
END
GO
--created by anoop jayaraj                                          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getdomaindetails')
DROP PROCEDURE usp_getdomaindetails
GO
CREATE PROCEDURE [dbo].[USP_GetDomainDetails]                                          
@numDomainID as numeric(9)=0                                          
as           

DECLARE @listIds VARCHAR(MAX)
SELECT @listIds = COALESCE(@listIds+',' ,'') + CAST(numUserID AS VARCHAR(100)) FROM UnitPriceApprover WHERE numDomainID = @numDomainID
   
DECLARE @canChangeDiferredIncomeSelection AS BIT = 1

IF EXISTS(SELECT 
			OB.numOppBizDocsId 
		FROM 
			OpportunityBizDocs OB 
		INNER JOIN 
			OpportunityMaster OM 
		ON 
			OB.numOppId=OM.numOppId 
		WHERE 
			numBizDocId=304 
			AND OM.numDomainId=@numDomainID)
BEGIN
	SET @canChangeDiferredIncomeSelection = 0
END   
   
                               
select                                           
vcDomainName,                                           
isnull(vcDomainDesc,'') vcDomainDesc,                                           
isnull(bitExchangeIntegration,0) bitExchangeIntegration,                                           
isnull(bitAccessExchange,0) bitAccessExchange,                                           
isnull(vcExchUserName,'') vcExchUserName,                                           
isnull(vcExchPassword,'') vcExchPassword,                                           
isnull(vcExchPath,'') vcExchPath,                                           
isnull(vcExchDomain,'') vcExchDomain,                                         
tintCustomPagingRows,                                         
vcDateFormat,                                         
numDefCountry,                                         
tintComposeWindow,                                         
sintStartDate,                                         
sintNoofDaysInterval,                                         
tintAssignToCriteria,                                         
bitIntmedPage,
ISNULL(D.numCost,0) as numCost,                                         
tintFiscalStartMonth,                                    
--isnull(bitDeferredIncome,0) as bitDeferredIncome,                                  
isnull(tintPayPeriod,0) as tintPayPeriod,                                
isnull(numCurrencyID,0) as numCurrencyID,                            
isnull(vcCurrency,'') as vcCurrency,                      
isnull(charUnitSystem,'') as charUnitSystem ,                    
isnull(vcPortalLogo,'') as vcPortalLogo,                  
isnull(tintChrForComSearch,2) as tintChrForComSearch,                
isnull(tintChrForItemSearch,'') as tintChrForItemSearch,            
isnull(intPaymentGateWay,0) as intPaymentGateWay  ,        
 case when bitPSMTPServer= 1 then vcPSMTPServer else '' end as vcPSMTPServer  ,              
 case when bitPSMTPServer= 1 then  isnull(numPSMTPPort,0)  else 0 end as numPSMTPPort          
,isnull(bitPSMTPAuth,0) bitPSMTPAuth        
,isnull([vcPSmtpPassword],'')vcPSmtpPassword        
,isnull(bitPSMTPSSL,0) bitPSMTPSSL     
,isnull(bitPSMTPServer,0) bitPSMTPServer  ,isnull(vcPSMTPUserName,'') vcPSMTPUserName,      
--isnull(numDefaultCategory,0) numDefaultCategory,
isnull(numShipCompany,0)   numShipCompany,
isnull(bitAutoPopulateAddress,0) bitAutoPopulateAddress,
isnull(tintPoulateAddressTo,0) tintPoulateAddressTo,
isnull(bitMultiCompany,0) bitMultiCompany,
isnull(bitMultiCurrency,0) bitMultiCurrency,
ISNULL(bitCreateInvoice,0) bitCreateInvoice,
ISNULL([numDefaultSalesBizDocId],0) numDefaultSalesBizDocId,
ISNULL([bitSaveCreditCardInfo],0) bitSaveCreditCardInfo,
ISNULL([intLastViewedRecord],10) intLastViewedRecord,
ISNULL([tintCommissionType],1) tintCommissionType,
ISNULL([tintBaseTax],2) tintBaseTax,
ISNULL([numDefaultPurchaseBizDocId],0) numDefaultPurchaseBizDocId,
ISNULL(bitEmbeddedCost,0) bitEmbeddedCost,
ISNULL(numDefaultSalesOppBizDocId,0) numDefaultSalesOppBizDocId,
ISNULL(tintSessionTimeOut,1) tintSessionTimeOut,
ISNULL(tintDecimalPoints,2) tintDecimalPoints,
ISNULL(bitCustomizePortal,0) bitCustomizePortal,
ISNULL(tintBillToForPO,0) tintBillToForPO,
ISNULL(tintShipToForPO,0) tintShipToForPO,
ISNULL(tintOrganizationSearchCriteria,1) tintOrganizationSearchCriteria,
ISNULL(tintLogin,0) tintLogin,
lower(ISNULL(vcPortalName,'')) vcPortalName,
ISNULL(bitDocumentRepositary,0) bitDocumentRepositary,
D.numDomainID,
ISNULL(vcPSMTPDisplayName,'') vcPSMTPDisplayName,
ISNULL(bitGtoBContact,0) bitGtoBContact,
ISNULL(bitBtoGContact,0) bitBtoGContact,
ISNULL(bitGtoBCalendar,0) bitGtoBCalendar,
ISNULL(bitBtoGCalendar,0) bitBtoGCalendar,
ISNULL(bitExpenseNonInventoryItem,0) bitExpenseNonInventoryItem,
ISNULL(bitInlineEdit,0) bitInlineEdit,
ISNULL(bitRemoveVendorPOValidation,0) bitRemoveVendorPOValidation,
isnull(D.tintBaseTaxOnArea,0) tintBaseTaxOnArea,
ISNULL(D.bitAmountPastDue,0) bitAmountPastDue,
CONVERT(DECIMAL(18,2),ISNULL(D.monAmountPastDue,0)) monAmountPastDue,
ISNULL(bitSearchOrderCustomerHistory,0) bitSearchOrderCustomerHistory,
isnull(D.bitRentalItem,0) as bitRentalItem,
isnull(D.numRentalItemClass,0) as numRentalItemClass,
isnull(D.numRentalHourlyUOM,0) as numRentalHourlyUOM,
isnull(D.numRentalDailyUOM,0) as numRentalDailyUOM,
isnull(D.tintRentalPriceBasedOn,0) as tintRentalPriceBasedOn,
isnull(D.tintCalendarTimeFormat,24) as tintCalendarTimeFormat,
isnull(D.tintDefaultClassType,0) as tintDefaultClassType,
ISNULL(D.tintPriceBookDiscount,0) tintPriceBookDiscount,
ISNULL(D.bitAutoSerialNoAssign,0) as bitAutoSerialNoAssign,
ISNULL(D.bitIsShowBalance,0) AS [bitIsShowBalance],
ISNULL(numIncomeAccID,0) AS [numIncomeAccID],
ISNULL(numCOGSAccID,0) AS [numCOGSAccID],
ISNULL(numAssetAccID,0) AS [numAssetAccID],
ISNULL(IsEnableProjectTracking,0) AS [IsEnableProjectTracking],
ISNULL(IsEnableCampaignTracking,0) AS [IsEnableCampaignTracking],
ISNULL(IsEnableResourceScheduling,0) AS [IsEnableResourceScheduling],
ISNULL(numShippingServiceItemID,0) AS [numShippingServiceItemID],
ISNULL(D.numSOBizDocStatus,0) AS numSOBizDocStatus,
ISNULL(D.bitAutolinkUnappliedPayment,0) AS bitAutolinkUnappliedPayment,
ISNULL(D.numDiscountServiceItemID,0) AS numDiscountServiceItemID,
ISNULL(D.vcShipToPhoneNo,0) AS vcShipToPhoneNo,
ISNULL(D.vcGAUserEMail,'') AS vcGAUserEMail,
ISNULL(D.vcGAUserPassword,'') AS vcGAUserPassword,
ISNULL(D.vcGAUserProfileId,0) AS vcGAUserProfileId,
ISNULL(D.bitDiscountOnUnitPrice,0) AS bitDiscountOnUnitPrice,
ISNULL(D.intShippingImageWidth,0) AS intShippingImageWidth ,
ISNULL(D.intShippingImageHeight,0) AS intShippingImageHeight,
ISNULL(D.numTotalInsuredValue,0) AS numTotalInsuredValue,
ISNULL(D.numTotalCustomsValue,0) AS numTotalCustomsValue,
ISNULL(D.vcHideTabs,'') AS vcHideTabs,
ISNULL(D.bitUseBizdocAmount,0) AS bitUseBizdocAmount,
ISNULL(D.bitDefaultRateType,0) AS bitDefaultRateType,
ISNULL(D.bitIncludeTaxAndShippingInCommission,0) AS bitIncludeTaxAndShippingInCommission,
/*,ISNULL(D.bitAllowPPVariance,0) AS bitAllowPPVariance*/
ISNULL(D.bitPurchaseTaxCredit,0) AS bitPurchaseTaxCredit,
ISNULL(D.bitLandedCost,0) AS bitLandedCost,
ISNULL(D.vcLanedCostDefault,'') AS vcLanedCostDefault,
ISNULL(D.bitMinUnitPriceRule,0) AS bitMinUnitPriceRule,
ISNULL(AP.numAbovePercent,0) AS numAbovePercent,
ISNULL(AP.numBelowPercent,0) AS numBelowPercent,
ISNULL(AP.numBelowPriceField,0) AS numBelowPriceField,
@listIds AS vcUnitPriceApprover,
ISNULL(D.numDefaultSalesPricing,2) AS numDefaultSalesPricing,
ISNULL(D.bitReOrderPoint,0) AS bitReOrderPoint,
ISNULL(D.numReOrderPointOrderStatus,0) AS numReOrderPointOrderStatus,
ISNULL(D.numPODropShipBizDoc,0) AS numPODropShipBizDoc,
ISNULL(D.numPODropShipBizDocTemplate,0) AS numPODropShipBizDocTemplate,
ISNULL(D.IsEnableDeferredIncome,0) AS IsEnableDeferredIncome,
ISNULL(D.bitApprovalforTImeExpense,0) AS bitApprovalforTImeExpense,
ISNULL(D.intTimeExpApprovalProcess,0) AS intTimeExpApprovalProcess,
ISNULL(D.bitApprovalforOpportunity,0) AS bitApprovalforOpportunity,
ISNULL(D.intOpportunityApprovalProcess,0) AS intOpportunityApprovalProcess,
@canChangeDiferredIncomeSelection AS canChangeDiferredIncomeSelection,
ISNULL(CAST(D.numDefaultSalesShippingDoc AS int),0) AS numDefaultSalesShippingDoc,
ISNULL(CAST(D.numDefaultPurchaseShippingDoc AS int),0) AS numDefaultPurchaseShippingDoc,
ISNULL(D.bitchkOverRideAssignto,0) AS bitchkOverRideAssignto,
ISNULL(D.vcPrinterIPAddress,'') vcPrinterIPAddress,
ISNULL(D.vcPrinterPort,'') vcPrinterPort,
ISNULL(AP.bitCostApproval,0) bitCostApproval
,ISNULL(AP.bitListPriceApproval,0) bitListPriceApproval
,ISNULL(AP.bitMarginPriceViolated,0) bitMarginPriceViolated
,ISNULL(vcSalesOrderTabs,'Sales Orders') AS vcSalesOrderTabs
,ISNULL(vcSalesQuotesTabs,'Sales Quotes') AS vcSalesQuotesTabs
,ISNULL(vcItemPurchaseHistoryTabs,'Item Purchase History') AS vcItemPurchaseHistoryTabs
,ISNULL(vcItemsFrequentlyPurchasedTabs,'Items Frequently Purchased') AS vcItemsFrequentlyPurchasedTabs
,ISNULL(vcOpenCasesTabs,'Open Cases') AS vcOpenCasesTabs
,ISNULL(vcOpenRMATabs,'Open RMAs') AS vcOpenRMATabs
,ISNULL(bitSalesOrderTabs,0) AS bitSalesOrderTabs
,ISNULL(bitSalesQuotesTabs,0) AS bitSalesQuotesTabs
,ISNULL(bitItemPurchaseHistoryTabs,0) AS bitItemPurchaseHistoryTabs
,ISNULL(bitItemsFrequentlyPurchasedTabs,0) AS bitItemsFrequentlyPurchasedTabs
,ISNULL(bitOpenCasesTabs,0) AS bitOpenCasesTabs
,ISNULL(bitOpenRMATabs,0) AS bitOpenRMATabs
,ISNULL(bitSupportTabs,0) AS bitSupportTabs
,ISNULL(vcSupportTabs,'Support') AS vcSupportTabs
,ISNULL(D.numDefaultSiteID,0) AS numDefaultSiteID
,ISNULL(tintOppStautsForAutoPOBackOrder,0) AS tintOppStautsForAutoPOBackOrder
,ISNULL(tintUnitsRecommendationForAutoPOBackOrder,1) AS tintUnitsRecommendationForAutoPOBackOrder
,ISNULL(bitRemoveGlobalLocation,0) AS bitRemoveGlobalLocation
,ISNULL(numAuthorizePercentage,0) AS numAuthorizePercentage
,ISNULL(bitEDI,0) AS bitEDI
,ISNULL(bit3PL,0) AS bit3PL
,ISNULL(numListItemID,0) AS numListItemID
,ISNULL(bitAllowDuplicateLineItems,0) bitAllowDuplicateLineItems
,ISNULL(tintCommitAllocation,1) tintCommitAllocation
,ISNULL(tintInvoicing,1) tintInvoicing
,ISNULL(tintMarkupDiscountOption,1) tintMarkupDiscountOption
,ISNULL(tintMarkupDiscountValue,1) tintMarkupDiscountValue
,ISNULL(bitIncludeRequisitions,0) bitIncludeRequisitions
,ISNULL(bitCommissionBasedOn,0) bitCommissionBasedOn
,ISNULL(tintCommissionBasedOn,0) tintCommissionBasedOn
,ISNULL(bitDoNotShowDropshipPOWindow,0) bitDoNotShowDropshipPOWindow
,ISNULL(tintReceivePaymentTo,2) tintReceivePaymentTo
,ISNULL(numReceivePaymentBankAccount,0) numReceivePaymentBankAccount
,ISNULL(numOverheadServiceItemID,0) numOverheadServiceItemID
from Domain D  
LEFT JOIN eCommerceDTL eComm  on eComm.numDomainID=D.numDomainID  
LEFT JOIN ApprovalProcessItemsClassification AP ON AP.numDomainID = D.numDomainID
where (D.numDomainID=@numDomainID OR @numDomainID=-1)


/****** Object:  StoredProcedure [dbo].[usp_getDynamicFormList]    Script Date: 07/26/2008 16:17:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Modified By: Anoop Jayaraj
   -- Created By: Debasish Nag              
-- Created On: 07/08/2005              
-- Purpose: To get a list of dynamic form names and their details              
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getdynamicformlist')
DROP PROCEDURE usp_getdynamicformlist
GO
CREATE PROCEDURE [dbo].[usp_getDynamicFormList]              
@numDomainID as numeric(9)=0,
@tintFlag AS TINYINT=0,
@bitWorkFlow as bit=0,
@bitAllowGridColor as bit=0,
@numBizFormModuleID as INT=0
as              
              
begin     
IF @bitAllowGridColor=1
BEGIn
	Select DM.numFormId, vcFormName, cCustomFieldsAssociated, cAOIAssociated
			from DynamicFormMaster DM where isnull(bitAllowGridColor,0)=@bitAllowGridColor order by DM.numFormId
END

ELSE IF @bitWorkFlow=1
BEGIn
	Select DM.numFormId, vcFormName, cCustomFieldsAssociated, cAOIAssociated
			from DynamicFormMaster DM where isnull(bitWorkFlow,0)=@bitWorkFlow order by DM.numFormId
END

Else IF  @tintFlag =1 OR @tintFlag=3
BEGIN
	Select DM.numFormId, vcFormName, cCustomFieldsAssociated, cAOIAssociated
from DynamicFormMaster DM 
where tintFlag=@tintFlag 
order by DM.numFormId
END 
Else IF  @tintFlag =6
BEGIN
	Select DM.numFormId, vcFormName, cCustomFieldsAssociated, cAOIAssociated
from DynamicFormMaster DM 
where tintFlag=1  AND DM.numFormId IN (34,35,12,36,10,13,40,41,38,39)
order by DM.numFormId
END
ELSE IF(@tintFlag=5)
BEGIN
	Select DM.numFormId, vcFormName, cCustomFieldsAssociated, cAOIAssociated, isNull(vcAdditionalParam,'') as vcAdditionalParam, numGrpId    
from DynamicFormMaster DM 
left join DynamicFormMasterParam  DMP 
on DMP.numFormID=DM.numFormID and DMP.numDomainID=@numDomainID            
where bitDeleted = 0 AND (numBizFormModuleID = @numBizFormModuleID OR ISNULL(@numBizFormModuleID,0) = 0) 
AND tintFlag=0 /*tintFlag=1 are internal forms need not to show in bizform wizard*/
AND DM.numFormId IN (34,35,12,36,10,13,40,41,38,39)
order by DM.numFormId
END
ELSE
BEGIN       
Select DM.numFormId, vcFormName, cCustomFieldsAssociated, cAOIAssociated, isNull(vcAdditionalParam,'') as vcAdditionalParam, numGrpId    
from DynamicFormMaster DM 
left join DynamicFormMasterParam  DMP 
on DMP.numFormID=DM.numFormID and DMP.numDomainID=@numDomainID            
where bitDeleted = 0 AND (numBizFormModuleID = @numBizFormModuleID OR ISNULL(@numBizFormModuleID,0) = 0) 
AND tintFlag=0 /*tintFlag=1 are internal forms need not to show in bizform wizard*/
order by DM.numFormId
END              
end
GO
/****** Object:  StoredProcedure [dbo].[usp_GetFieldType]    Script Date: 07/26/2008 16:17:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getfieldtype')
DROP PROCEDURE usp_getfieldtype
GO
CREATE PROCEDURE [dbo].[usp_GetFieldType]
	@vcTableName varchar(100)=''   
--
AS
	/*select  syscolumns.NAme ,sysproperties.Value,systypes.name as DataType   from syscolumns , sysobjects  ,sysproperties,systypes where
 syscolumns.id  = sysobjects.id 
and sysproperties.id =  sysobjects.id 
and sysproperties.SmallId = syscolumns.Colid
and systypes.xtype=syscolumns.xtype
and sysobjects.Name  =  @vcTableName */
SELECT  
	syscolumns.NAme,
	systypes.name as DataType   
FROM 
	syscolumns 
INNER JOIN 
	sysobjects 
ON
	syscolumns.id  = sysobjects.id 
INNER JOIN 
	systypes 
ON
	systypes.xtype=syscolumns.xtype
where
	sysobjects.Name  =@vcTableName
	and systypes.xtype in (106,62,60,108,59,52,122,48)
GO

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetItemOrderDependencies')
DROP PROCEDURE USP_GetItemOrderDependencies
GO
CREATE PROCEDURE [dbo].[USP_GetItemOrderDependencies]         
	@numDomainID as numeric(9)=0,    
	@numItemCode as numeric(9)=0,
	@ClientTimeZoneOffset Int,
	@tintOppType as tinyint=0,
	@byteMode as tinyint=0,
	@numWareHouseId as numeric(9)=0
as        
IF @byteMode=0 
BEGIN

	DECLARE @vcItemName AS VARCHAR(100)
	DECLARE @OnOrder AS numeric(18)
	DECLARE @vcShippedReceivedStatus VARCHAR(100) = ''

	Create table #temp (numoppid numeric(18), vcPOppName varchar(100), tintOppType tinyint, bintCreatedDate datetime, bintAccountClosingDate datetime, OppType varchar(20),
		OppStatus varchar(20), vcCompanyname varchar(200), vcItemName varchar(200), numReturnHeaderID numeric(18), vcWareHouse varchar(100), 
		numQtyOrdered FLOAT, numQtyReleasedReceived FLOAT, numQtyOnAllocationOnOrder FLOAT)

	-- NOT Sales and Purchase Return
	IF @tintOppType <> 5 AND @tintOppType <> 6 AND ISNULL(@tintOppType,0) <> 11
	BEGIN
		--Regular Item
		INSERT INTO 
			#temp
		SELECT 
			OM.numoppid
			,OM.vcPoppName
			,OM.tintOppType
			,OM.bintCreatedDate
			,OM.bintAccountClosingDate
			,CASE 
				WHEN OM.tintOppType=1 
				THEN CASE WHEN OM.tintOppStatus=0 THEN 'Sales Opportunity' ELSE 'Sales Order' END 
				ELSE CASE WHEN OM.tintOppStatus=0 THEN 'Purchase Opportunity' ELSE 'Purchase Order' END 
			END as OppType
			,CASE ISNULL(OM.tintshipped, 0) WHEN 0 THEN 'Open' WHEN 1 THEN 'Closed' END OppStatus
			,CI.vcCompanyName + Case when isnull(DM.numCompanyDiff,0)>0 then  '  ' + dbo.fn_getlistitemname(DM.numCompanyDiff) + ':' + isnull(DM.vcCompanyDiff,'') else '' end as vcCompanyname
			,''
			,0
			,W.vcWareHouse 
			,ISNULL(dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), Opp.numItemCode,I.numDomainId, ISNULL(Opp.numUOMId, 0)),1) * numUnitHour AS numQtyOrdered
			,ISNULL(dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), Opp.numItemCode,I.numDomainId, ISNULL(Opp.numUOMId, 0)),1) * (CASE WHEN OM.tintOppType=1 THEN ISNULL(numQtyShipped,0) ELSE ISNULL(numUnitHourReceived,0) END) AS numQtyReleasedReceived
			,(ISNULL(dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), Opp.numItemCode,I.numDomainId, ISNULL(Opp.numUOMId, 0)),1) * numUnitHour) - (ISNULL(dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), Opp.numItemCode,I.numDomainId, ISNULL(Opp.numUOMId, 0)),1) * (CASE WHEN OM.tintOppType=1 THEN ISNULL(numQtyShipped,0) ELSE ISNULL(numUnitHourReceived,0) END)) As numQtyOnAllocationOnOrder
		FROM 
			OpportunityItems Opp
		INNER JOIN OpportunityMaster OM ON Opp.numOppID = oM.numOppID
		INNER JOIN item I ON Opp.numItemCode = i.numItemcode
		INNER JOIN divisionMaster DM ON OM.numDivisionID = DM.numDivisionID
		LEFT JOIN CompanyInfo CI ON CI.numCompanyID = DM.numCompanyID
		LEFT JOIN WareHouseItems WHI ON WHI.[numWareHouseItemID] = Opp.numWarehouseItmsID
		LEFT JOIN Warehouses W ON W.numWareHouseID = WHI.numWareHouseID
		WHERE   
			OM.numDomainId = @numDomainID
			AND I.numDomainId = @numDomainID 
			AND I.numItemCode=@numItemCode
			AND 1 = (CASE WHEN @numWareHouseId > 0 THEN 
							Case WHEN W.numWareHouseID = @numWareHouseId then 1 else 0 end
						Else 1 end) 
			AND 1=(CASE 
						WHEN ISNULL(@tintOppType,0) = 1 -- Sales Order
						THEN CASE WHEN ISNULL(OM.tintOppType,0) = 1 AND ISNULL(OM.tintOppStatus,0)=1 THEN 1 ELSE 0 END
						WHEN ISNULL(@tintOppType,0) = 2 -- Purchase Order
						THEN CASE WHEN ISNULL(OM.tintOppType,0) = 2 AND ISNULL(OM.tintOppStatus,0)=1 THEN 1 ELSE 0 END
						WHEN ISNULL(@tintOppType,0) = 3 -- Sales Opportunity
						THEN CASE WHEN ISNULL(OM.tintOppType,0) = 1 AND ISNULL(OM.tintOppStatus,0)=0 THEN 1 ELSE 0 END
						WHEN ISNULL(@tintOppType,0) = 4 -- Purchase Opportunity
						THEN CASE WHEN ISNULL(OM.tintOppType,0) = 2 AND ISNULL(OM.tintOppStatus,0)=0 THEN 1 ELSE 0 END
						WHEN ISNULL(@tintOppType,0) = 7 -- Open Sales Order
						THEN CASE WHEN ISNULL(OM.tintOppType,0) = 1 AND ISNULL(OM.tintOppStatus,0)=1 AND ISNULL(OM.tintshipped,0) = 0 THEN 1 ELSE 0 END
						WHEN ISNULL(@tintOppType,0) = 8 -- Closed Sales Order
						THEN CASE WHEN ISNULL(OM.tintOppType,0) = 1 AND ISNULL(OM.tintOppStatus,0)=1 AND ISNULL(OM.tintshipped,0) = 1 THEN 1 ELSE 0 END
						WHEN ISNULL(@tintOppType,0) = 9 -- Open Purchase Order
						THEN CASE WHEN ISNULL(OM.tintOppType,0) = 2 AND ISNULL(OM.tintOppStatus,0)=1 AND ISNULL(OM.tintshipped,0) = 0 THEN 1 ELSE 0 END
						WHEN ISNULL(@tintOppType,0) = 10 -- Closed Purchase Order
						THEN CASE WHEN ISNULL(OM.tintOppType,0) = 2 AND ISNULL(OM.tintOppStatus,0)=1 AND ISNULL(OM.tintshipped,0) = 1 THEN 1 ELSE 0 END 
						ELSE 1 
					END)
		ORDER BY 
			OM.bintCreatedDate desc
	END
	

	DECLARE @strSQL AS NVARCHAR(MAX) = ''
	DECLARE @strWHERE AS NVARCHAR(MAX)
	SET @strWHERE = ' WHERE 1=1 '

	IF @tintOppType = 5 
	BEGIN
	 SET @strWHERE = @strWHERE + ' AND [RH].[tintReturnType] = 1 '
	END
	ELSE IF @tintOppType = 6
	BEGIN
	 SET @strWHERE = @strWHERE + ' AND [RH].[tintReturnType] = 2 '
	END
	ELSE IF @tintOppType = 0
	BEGIN
	 SET @strWHERE = @strWHERE + ' AND [RH].[tintReturnType] IN (1,2) '
	END


	-- NOT Sales and Purchase Return
	IF @tintOppType <> 5 AND @tintOppType <> 6 AND ISNULL(@tintOppType,0) <> 11
	BEGIN
		SET @strSQL = CONCAT('INSERT INTO 
								#temp
							SELECT 
								OM.numoppid
								,OM.vcPoppName
								,OM.tintOppType
								,OM.bintCreatedDate
								,OM.bintAccountClosingDate
								,CASE 
									WHEN OM.tintOppType=1 
									THEN CASE WHEN OM.tintOppStatus=0 THEN ''Sales Opportunity'' ELSE ''Sales Order'' END 
									ELSE CASE WHEN OM.tintOppStatus=0 THEN ''Purchase Opportunity'' ELSE ''Purchase Order'' END 
								END as OppType
								,CASE ISNULL(OM.tintshipped, 0) WHEN 0 THEN ''Open'' WHEN 1 THEN ''Closed'' END OppStatus
								,CI.vcCompanyName + Case when isnull(DM.numCompanyDiff,0)>0 then  ''  '' + dbo.fn_getlistitemname(DM.numCompanyDiff) + '':'' + isnull(DM.vcCompanyDiff,'''') else '''' end as vcCompanyname
								,IParent.vcItemName + Case when IParent.bitAssembly=1 then '' (Assembly Member)'' When IParent.bitKitParent=1 then '' (Kit Member)'' else '''' end
								,0 [numReturnHeaderID]
								,W.vcWareHouse
								,ISNULL(dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), I.numItemCode,I.numDomainId, ISNULL(OKI.numUOMId, 0)),1) * OKI.numQtyItemsReq AS numQtyOrdered
								,ISNULL(dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), I.numItemCode,I.numDomainId, ISNULL(OKI.numUOMId, 0)),1) * (CASE WHEN OM.tintOppType=1 THEN ISNULL(OKI.numQtyShipped,0) ELSE 0 END) AS numQtyReleasedReceived
								,(ISNULL(dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), I.numItemCode,I.numDomainId, ISNULL(OKI.numUOMId, 0)),1) * OKI.numQtyItemsReq) - (ISNULL(dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), I.numItemCode,I.numDomainId, ISNULL(OKI.numUOMId, 0)),1) * (CASE WHEN OM.tintOppType=1 THEN ISNULL(OKI.numQtyShipped,0) ELSE 0 END)) As numQtyOnAllocationOnOrder
							FROM 
								OpportunityKitItems OKI 
							INNER JOIN OpportunityItems OI
							ON OKI.numOppItemID=OI.numoppitemtcode
							INNER JOIN Item IParent on OI.numItemCode =IParent.numItemCode 
							INNER JOIN Item I on OKI.numChildItemID =I.numItemCode  
							INNER JOIN OpportunityMaster OM ON OKI.numOppID = oM.numOppID
							INNER JOIN divisionMaster DM ON OM.numDivisionID = DM.numDivisionID
							LEFT JOIN CompanyInfo CI ON CI.numCompanyID = DM.numCompanyID
							LEFT JOIN WareHouseItems WHI ON WHI.numWarehouseItemID = OKI.numWarehouseItemID
							LEFT JOIN Warehouses W ON W.numWareHouseID = WHI.numWareHouseID
							WHERE 
								ISNULL(I.bitAssembly,0)=0 
								AND OKI.numChildItemID=',@numItemCode,'  
								AND om.numDomainId = ',@numDomainID,' 
								AND 1 = (CASE WHEN ',@numWareHouseId,' > 0 THEN 
												Case WHEN W.numWareHouseID = ',@numWareHouseId,' then 1 else 0 end
											Else 1 end) 
								AND I.numDomainId = ',@numDomainID,'  
								AND 1=(CASE 
										WHEN ',ISNULL(@tintOppType,0),' = 1
										THEN CASE WHEN ISNULL(OM.tintOppType,0) = 1 AND ISNULL(OM.tintOppStatus,0)=1 THEN 1 ELSE 0 END
										WHEN ',ISNULL(@tintOppType,0),' = 2
										THEN CASE WHEN ISNULL(OM.tintOppType,0) = 2 AND ISNULL(OM.tintOppStatus,0)=1 THEN 1 ELSE 0 END
										WHEN ',ISNULL(@tintOppType,0),' = 3
										THEN CASE WHEN ISNULL(OM.tintOppType,0) = 1 AND ISNULL(OM.tintOppStatus,0)=0 THEN 1 ELSE 0 END
										WHEN ',ISNULL(@tintOppType,0),' = 4
										THEN CASE WHEN ISNULL(OM.tintOppType,0) = 2 AND ISNULL(OM.tintOppStatus,0)=0 THEN 1 ELSE 0 END
										WHEN ',ISNULL(@tintOppType,0),' = 7
										THEN CASE WHEN ISNULL(OM.tintOppType,0) = 1 AND ISNULL(OM.tintOppStatus,0)=1 AND ISNULL(OM.tintshipped,0) = 0 THEN 1 ELSE 0 END
										WHEN ',ISNULL(@tintOppType,0),' = 8
										THEN CASE WHEN ISNULL(OM.tintOppType,0) = 1 AND ISNULL(OM.tintOppStatus,0)=1 AND ISNULL(OM.tintshipped,0) = 1 THEN 1 ELSE 0 END
										WHEN ',ISNULL(@tintOppType,0),' = 9
										THEN CASE WHEN ISNULL(OM.tintOppType,0) = 2 AND ISNULL(OM.tintOppStatus,0)=1 AND ISNULL(OM.tintshipped,0) = 0 THEN 1 ELSE 0 END
										WHEN ',ISNULL(@tintOppType,0),' = 10
										THEN CASE WHEN ISNULL(OM.tintOppType,0) = 2 AND ISNULL(OM.tintOppStatus,0)=1 AND ISNULL(OM.tintshipped,0) = 1 THEN 1 ELSE 0 END 
										ELSE 1 
									END)')

		SET @strSQL = CONCAT(@strSQL,' INSERT INTO 
								#temp
							SELECT 
								OM.numoppid
								,OM.vcPoppName
								,OM.tintOppType
								,OM.bintCreatedDate
								,OM.bintAccountClosingDate
								,CASE 
									WHEN OM.tintOppType=1 
									THEN CASE WHEN OM.tintOppStatus=0 THEN ''Sales Opportunity'' ELSE ''Sales Order'' END 
									ELSE CASE WHEN OM.tintOppStatus=0 THEN ''Purchase Opportunity'' ELSE ''Purchase Order'' END 
								END as OppType
								,CASE ISNULL(OM.tintshipped, 0) WHEN 0 THEN ''Open'' WHEN 1 THEN ''Closed'' END OppStatus
								,CI.vcCompanyName + Case when isnull(DM.numCompanyDiff,0)>0 then  ''  '' + dbo.fn_getlistitemname(DM.numCompanyDiff) + '':'' + isnull(DM.vcCompanyDiff,'''') else '''' end as vcCompanyname
								,IParent.vcItemName + Case when IParent.bitAssembly=1 then '' (Assembly Member)'' When IParent.bitKitParent=1 then '' (Kit Member)'' else '''' end
								,0 [numReturnHeaderID]
								,W.vcWareHouse
								,ISNULL(dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), I.numItemCode,I.numDomainId, ISNULL(OKI.numUOMId, 0)),1) * OKI.numQtyItemsReq AS numQtyOrdered
								,ISNULL(dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), I.numItemCode,I.numDomainId, ISNULL(OKI.numUOMId, 0)),1) * (CASE WHEN OM.tintOppType=1 THEN ISNULL(OKI.numQtyShipped,0) ELSE 0 END) AS numQtyReleasedReceived
								,(ISNULL(dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), I.numItemCode,I.numDomainId, ISNULL(OKI.numUOMId, 0)),1) * OKI.numQtyItemsReq) - (ISNULL(dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), I.numItemCode,I.numDomainId, ISNULL(OKI.numUOMId, 0)),1) * (CASE WHEN OM.tintOppType=1 THEN ISNULL(OKI.numQtyShipped,0) ELSE 0 END)) As numQtyOnAllocationOnOrder
							FROM 
								OpportunityKitChildItems OKI 
							INNER JOIN OpportunityItems OI
							ON OKI.numOppItemID=OI.numoppitemtcode
							INNER JOIN Item IParent on OI.numItemCode =IParent.numItemCode 
							INNER JOIN Item I on OKI.numItemID =I.numItemCode  
							INNER JOIN OpportunityMaster OM ON OKI.numOppID = oM.numOppID
							INNER JOIN divisionMaster DM ON OM.numDivisionID = DM.numDivisionID
							LEFT JOIN CompanyInfo CI ON CI.numCompanyID = DM.numCompanyID
							LEFT JOIN WareHouseItems WHI ON WHI.numWarehouseItemID = OKI.numWarehouseItemID
							LEFT JOIN Warehouses W ON W.numWareHouseID = WHI.numWareHouseID
							WHERE 
								ISNULL(I.bitAssembly,0)=0 
								AND OKI.numItemID=',@numItemCode,'  
								AND om.numDomainId = ',@numDomainID,' 
								AND 1 = (CASE WHEN ',@numWareHouseId,' > 0 THEN 
												Case WHEN W.numWareHouseID = ',@numWareHouseId,' then 1 else 0 end
											Else 1 end) 
								AND I.numDomainId = ',@numDomainID,'  
								AND 1=(CASE 
										WHEN ',ISNULL(@tintOppType,0),' = 1
										THEN CASE WHEN ISNULL(OM.tintOppType,0) = 1 AND ISNULL(OM.tintOppStatus,0)=1 THEN 1 ELSE 0 END
										WHEN ',ISNULL(@tintOppType,0),' = 2
										THEN CASE WHEN ISNULL(OM.tintOppType,0) = 2 AND ISNULL(OM.tintOppStatus,0)=1 THEN 1 ELSE 0 END
										WHEN ',ISNULL(@tintOppType,0),' = 3
										THEN CASE WHEN ISNULL(OM.tintOppType,0) = 1 AND ISNULL(OM.tintOppStatus,0)=0 THEN 1 ELSE 0 END
										WHEN ',ISNULL(@tintOppType,0),' = 4
										THEN CASE WHEN ISNULL(OM.tintOppType,0) = 2 AND ISNULL(OM.tintOppStatus,0)=0 THEN 1 ELSE 0 END
										WHEN ',ISNULL(@tintOppType,0),' = 7
										THEN CASE WHEN ISNULL(OM.tintOppType,0) = 1 AND ISNULL(OM.tintOppStatus,0)=1 AND ISNULL(OM.tintshipped,0) = 0 THEN 1 ELSE 0 END
										WHEN ',ISNULL(@tintOppType,0),' = 8
										THEN CASE WHEN ISNULL(OM.tintOppType,0) = 1 AND ISNULL(OM.tintOppStatus,0)=1 AND ISNULL(OM.tintshipped,0) = 1 THEN 1 ELSE 0 END
										WHEN ',ISNULL(@tintOppType,0),' = 9
										THEN CASE WHEN ISNULL(OM.tintOppType,0) = 2 AND ISNULL(OM.tintOppStatus,0)=1 AND ISNULL(OM.tintshipped,0) = 0 THEN 1 ELSE 0 END
										WHEN ',ISNULL(@tintOppType,0),' = 10
										THEN CASE WHEN ISNULL(OM.tintOppType,0) = 2 AND ISNULL(OM.tintOppStatus,0)=1 AND ISNULL(OM.tintshipped,0) = 1 THEN 1 ELSE 0 END 
										ELSE 1 
									END)')
	END

	IF @tintOppType = 5 AND @tintOppType = 6 OR @tintOppType = 0
	BEGIN
		SET @strSQL = @strSQL + CONCAT(' INSERT INTO #temp
										SELECT 
											RH.[numReturnHeaderID] numoppid
											,RH.[vcRMA] vcPoppName
											,[RH].[tintReturnType] tintOppType
											,RH.[dtCreatedDate] bintCreatedDate
											,NULL bintAccountClosingDate
											,CASE [RH].[tintReturnType] WHEN 1 THEN ''Sales Return'' WHEN 2 THEN ''Purchase Return'' ELSE '''' END OppType
											,'''' OppStatus
											,CI.vcCompanyName + CASE WHEN ISNULL(DM.numCompanyDiff,0) > 0 THEN ''  '' + dbo.fn_getlistitemname(DM.numCompanyDiff) + '':'' + ISNULL(DM.vcCompanyDiff,'''') ELSE '''' END AS vcCompanyname
											,I.vcItemName + CASE WHEN I.bitAssembly = 1 THEN '' (Assembly)'' WHEN I.bitKitParent = 1 THEN '' (Kit)'' ELSE '''' END
											,RH.[numReturnHeaderID]
											,W.vcWareHouse
											,0
											,0
											,0
										FROM [dbo].[ReturnHeader] AS RH 
										INNER JOIN [dbo].[ReturnItems] AS RI  ON [RH].[numReturnHeaderID] = [RI].[numReturnHeaderID] AND [RH].[tintReturnType] IN (1,2)
										INNER JOIN [dbo].[Item] AS I ON [RI].[numItemCode] = [I].[numItemCode] AND [I].[numDomainID] = RH.[numDomainId]
										INNER JOIN divisionMaster DM ON RH.numDivisionID = DM.numDivisionID
										LEFT JOIN CompanyInfo CI ON CI.numCompanyID = DM.numCompanyID 
										LEFT JOIN WareHouseItems WHI ON WHI.numWareHouseItemID = RI.numWareHouseItemID  
										LEFT JOIN Warehouses W ON W.numWareHouseID = WHI.numWareHouseID
										', @strWHERE,' AND RH.[numDomainId] = ',@numDomainID,' AND [RI].[numItemCode] = ',@numItemCode,' ORDER BY bintCreatedDate desc')
	END

	-- Stock Transfer
	IF ISNULL(@tintOppType,0) = 0 OR ISNULL(@tintOppType,0) = 11
	BEGIN
		INSERT INTO 
			#temp
		SELECT 
			MST.ID
			,CONCAT('Stock Transfer (To Warehouse:',CONCAT(WTo.vcWareHouse, CASE WHEN LEN(ISNULL(WLTo.vcLocation,'')) > 0 THEN CONCAT(' (',WLTo.vcLocation,')') ELSE '' END),')')
			,0
			,MST.dtTransferredDate
			,NULL
			,'Stock Transfer' as OppType
			,'Closed' OppStatus
			,'' as vcCompanyname
			,''
			,0
			,CONCAT(WFrom.vcWareHouse, CASE WHEN LEN(ISNULL(WLFrom.vcLocation,'')) > 0 THEN CONCAT(' (',WLFrom.vcLocation,')') ELSE '' END)
			,MST.numQty AS numQtyOrdered
			,MST.numQty AS numQtyReleasedReceived
			,0 As numQtyOnAllocationOnOrder
		FROM 
			MassStockTransfer MST
		INNER JOIN 
			Item I 
		ON 
			MST.numItemCode = I.numItemcode
		INNER JOIN 
			WareHouseItems WHIFrom 
		ON 
			WHIFrom.[numWareHouseItemID] = MST.numFromWarehouseItemID
		INNER JOIN 
			Warehouses WFrom 
		ON 
			WFrom.numWareHouseID = WHIFrom.numWareHouseID
		LEFT JOIN
			WarehouseLocation WLFrom
		ON
			WHIFrom.numWLocationID = WLFrom.numWLocationID
		INNER JOIN 
			WareHouseItems WHITo 
		ON 
			WHITo.[numWareHouseItemID] = MST.numToWarehouseItemID
		INNER JOIN 
			Warehouses WTo 
		ON 
			WTo.numWareHouseID = WHITo.numWareHouseID
		LEFT JOIN
			WarehouseLocation WLTo
		ON
			WHITo.numWLocationID = WLTo.numWLocationID
		WHERE   
			MST.numDomainId = @numDomainID
			AND MST.numItemCode=@numItemCode
			AND I.numDomainId = @numDomainID 
	END


PRINT CAST(@strSQL AS NTEXT)
EXEC (@strSQL)


select numoppid,vcPoppName,tintOppType,
case when convert(varchar(11),DateAdd(minute, -@ClientTimeZoneOffset,bintCreatedDate) )= convert(varchar(11),getdate()) then '<b><font color=red>Today</font></b>'
	 when convert(varchar(11),DateAdd(minute, -@ClientTimeZoneOffset,bintCreatedDate) )= convert(varchar(11),dateadd(day,-1,getdate())) then '<b><font color=purple>YesterDay</font></b>'
	 when convert(varchar(11),DateAdd(minute, -@ClientTimeZoneOffset,bintCreatedDate) )= convert(varchar(11),dateadd(day,1,getdate())) then'<b><font color=orange>Tommorow</font></b>' 
	else dbo.FormatedDateFromDate(DateAdd(minute, -@ClientTimeZoneOffset,bintCreatedDate ),1) end  CreatedDate,
case when convert(varchar(11),DateAdd(minute, -@ClientTimeZoneOffset,bintAccountClosingDate) )= convert(varchar(11),getdate()) then '<b><font color=red>Today</font></b>'
	 when convert(varchar(11),DateAdd(minute, -@ClientTimeZoneOffset,bintAccountClosingDate) )= convert(varchar(11),dateadd(day,-1,getdate())) then '<b><font color=purple>YesterDay</font></b>'
	 when convert(varchar(11),DateAdd(minute, -@ClientTimeZoneOffset,bintAccountClosingDate) )= convert(varchar(11),dateadd(day,1,getdate())) then'<b><font color=orange>Tommorow</font></b>' 
	else dbo.FormatedDateFromDate(DateAdd(minute, -@ClientTimeZoneOffset,bintAccountClosingDate ),1) end  AccountClosingDate,
 OppType,OppStatus,vcCompanyName,vcItemName,numReturnHeaderID, vcWareHouse, ISNULL(numQtyOrdered,0) AS numOnOrder, ISNULL(numQtyOnAllocationOnOrder,0) AS numAllocation,ISNULL(numQtyReleasedReceived,0) numQtyReleasedReceived  from #temp 
  ORDER by bintCreatedDate desc 

drop table #temp
END


ELSE IF @byteMode=1 --Parent Items of selected item
BEGIN

;WITH CTE(numItemCode,vcItemName,bitAssembly,bitKitParent)
AS
(
select numItemKitID,vcItemName,bitAssembly,bitKitParent
from item                                
INNER join ItemDetails Dtl on numItemKitID=numItemCode
where  numChildItemID=@numItemCode

UNION ALL

select dtl.numItemKitID ,i.vcItemName,i.bitAssembly,i.bitKitParent
from item i                               
INNER JOIN ItemDetails Dtl on Dtl.numItemKitID=i.numItemCode
INNER JOIN CTE c ON Dtl.numChildItemID = c.numItemCode
where Dtl.numChildItemID!=@numItemCode
)

select numItemCode,vcItemName,Case when bitAssembly=1 then 'Assembly' When bitKitParent=1 then 'Kit' end as ItemType
from CTE                                
END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetMutltiCashBank')
DROP PROCEDURE USP_GetMutltiCashBank
GO
Create PROCEDURE [dbo].[USP_GetMutltiCashBank]
(@numParentDomainID  int,
 @dtFromDate datetime,
 @dtToDate datetime,
 @numSubscriberID INT)
as
begin

CREATE TABLE #TempCashBank
(numDomainID numeric,
vcDomainName varchar(200),
vcDomainCode varchar(50),
Debit DECIMAL(20,5),
Credit DECIMAL(20,5),
Total DECIMAL(20,5));


INSERT INTO #TempCashBank 

select DN.numDomainID,DN.vcDomainName,DN.vcDomainCode, Sum(Debit) as Debit, Sum(Credit) as Credit, Sum(Total) as Total
from VIEW_CASHBANKDAILYSUMMARY VARD
INNER JOIN 
(select * from DOMAIN DN where numSubscriberID=@numSubscriberID) DN ON VARD.numDomainID  =  DN.numDomainID 
INNER JOIN (select * from DOMAIN DNV where numSubscriberID=@numSubscriberID) DNV ON VARD.numDomainID = DNV.numDomainID INNER JOIN 
financialyear FY ON FY.numDomainID=DN.numDomainID AND 
FY.dtPeriodFrom <= @dtFromDate and dtPeriodTo >= @dtFromDate 
AND ( DN.numDomainID=@numParentDomainID)
AND datEntry_Date between FY.dtPeriodFrom and @dtFromDate - 1
GROUP BY DN.numDomainID,DN.vcDomainName,DN.vcDomainCode
union
select DN.numDomainID,DN.vcDomainName,DN.vcDomainCode, Sum(Debit) as Debit, Sum(Credit) as Credit, Sum(Total) as Total
from VIEW_CASHBANKDAILYSUMMARY VARD
INNER JOIN 
(select * from DOMAIN DN where numSubscriberID=@numSubscriberID) DN ON VARD.numDomainID  =  DN.numDomainID 
INNER JOIN (select * from DOMAIN DNV where numSubscriberID=@numSubscriberID) DNV ON VARD.numDomainID = DNV.numDomainID
AND (DN.numDomainID = @numParentDomainID  )
AND datEntry_Date between @dtFromDate and @dtToDate  
GROUP BY DN.numDomainID,DN.vcDomainName,DN.vcDomainCode

----------------------------------------
union 
--------------------------
select DN.numDomainID,DN.vcDomainName,DN.vcDomainCode, Sum(Debit) as Debit, Sum(Credit) as Credit, Sum(Total) as Total
from VIEW_CASHBANKDAILYSUMMARY VARD
INNER JOIN 
(select * from DOMAIN DN where numSubscriberID=@numSubscriberID) DN ON VARD.vcDomainCode  like  DN.vcDomainCode + '%'
INNER JOIN (select * from DOMAIN DNV where numSubscriberID=@numSubscriberID) DNV ON VARD.numDomainID = DNV.numDomainID INNER JOIN 
financialyear FY ON FY.numDomainID=DN.numDomainID AND 
FY.dtPeriodFrom <= @dtFromDate and dtPeriodTo >= @dtFromDate 
AND (DN.numParentDomainID = @numParentDomainID )
AND datEntry_Date between FY.dtPeriodFrom and @dtFromDate - 1
GROUP BY DN.numDomainID,DN.vcDomainName,DN.vcDomainCode
union
select DN.numDomainID,DN.vcDomainName,DN.vcDomainCode, Sum(Debit) as Debit, Sum(Credit) as Credit, Sum(Total) as Total
from VIEW_CASHBANKDAILYSUMMARY VARD
INNER JOIN 
(select * from DOMAIN DN where numSubscriberID=@numSubscriberID) DN ON VARD.vcDomainCode  like  DN.vcDomainCode + '%'
INNER JOIN (select * from DOMAIN DNV where numSubscriberID=@numSubscriberID) DNV ON VARD.numDomainID = DNV.numDomainID
AND (DN.numParentDomainID = @numParentDomainID  )
AND datEntry_Date between @dtFromDate and @dtToDate  
GROUP BY DN.numDomainID,DN.vcDomainName,DN.vcDomainCodE


SELECT vcDomainName ,isnull(SUM(isnull(TOTAL,0)),0) as Total  FROM #TempCashBank GROUP BY vcDomainName;

DROP TABLE #TempCashBank;
end
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetNavigation')
DROP PROCEDURE dbo.USP_GetNavigation
GO
CREATE PROCEDURE [dbo].[USP_GetNavigation]
(
	@numDomainID NUMERIC(18,0),
	@numGroupID NUMERIC(18,0)
)
AS 
BEGIN

	/********  MAIN MENU  ********/

	SELECT  
		T.numTabID,
		ISNULL((SELECT TOP 1 numTabName FROM TabDefault WHERE numDomainid = @numDomainID AND numTabId = T.numTabId AND tintTabType = T.tintTabType),T.numTabName) vcTabName,
		CASE 
			WHEN (SELECT COUNT(*) FROM ShortCutGrpConf WHERE numDomainID=@numDomainID AND numGroupId=@numGroupID AND numTabId=T.numTabId AND ISNULL(bitInitialPage,0)=1) > 0
			THEN
				REPLACE(ISNULL((SELECT TOP 1 Link FROM ShortCutBar WHERE (numDomainid = @numDomainID OR bitDefault=1) AND numTabId = T.numTabId AND id=(SELECT TOP 1 numLinkId FROM ShortCutGrpConf WHERE numDomainID=@numDomainID AND numGroupId=@numGroupID AND numTabId=T.numTabId AND ISNULL(bitInitialPage,0)=1)),''),'../','')
			ELSE 
				ISNULL(vcURL,'')
		END vcURL,
		ISNULL(vcAddURL,'') vcAddURL,
		ISNULL(bitAddIsPopUp,0) AS bitAddIsPopUp,
		numOrder
	FROM    
		TabMaster T
	JOIN 
		GroupTabDetails G ON G.numTabId = T.numTabId
	WHERE   
		(numDomainID = @numDomainID OR bitFixed = 1)
		AND numGroupID = @numGroupID
		AND ISNULL(G.[tintType], 0) <> 1
		AND tintTabType =1
		AND T.numTabID NOT IN (2,68)
	ORDER BY 
		numOrder   


	/********  SUB MENU  ********/

	DECLARE @numTabID NUMERIC(18,0)
	DECLARE @numModuleID AS NUMERIC(18, 0)

	DECLARE @TEMP TABLE
	(
		ID INT IDENTITY(1,1),
		numOrigParentID NUMERIC(18,0),
		numParentID NUMERIC(18,0),
		numListItemID NUMERIC(18,0),
		numTabID NUMERIC(18,0),
		numPageNavID NUMERIC(18,0),
		vcNodeName VARCHAR(500),
		vcURL VARCHAR(1000),
		vcAddURL VARCHAR(1000),
		bitAddIsPopUp BIT,
		[order] int,
		bitVisible BIT,
		vcImageURL VARCHAR(1000)
	)

	BEGIN /******************** OPPORTUNITY/ORDER ***********************/

		SET @numTabID = 1
		SELECT TOP 1 @numModuleID = numModuleID FROM dbo.PageNavigationDTL WHERE numTabID = @numTabID

		INSERT INTO
			@TEMP
		SELECT
			0,
			0,
			0,
			@numTabID,
			PND.numPageNavID,
			vcPageNavName,
			ISNULL(vcNavURL,'') AS vcNavURL,
			ISNULL(PND.vcAddURL,''),
			ISNULL(PND.bitAddIsPopUp,0),
			0,
			1,
			ISNULL(PND.vcImageURL,'')
		FROM 
			PageNavigationDTL PND
		JOIN 
			dbo.TreeNavigationAuthorization TNA 
		ON 
			PND.numPageNavID = TNA.numPageNavID  
			AND PND.numTabID = TNA.numTabID       
		WHERE  
			ISNULL(TNA.bitVisible, 0) = 1
			AND ISNULL(PND.bitVisible, 0) = 1
			AND numModuleID = @numModuleID
			AND TNA.numDomainID = @numDomainID
			AND numGroupID = @numGroupID
			AND PND.numParentID <> 0
		ORDER BY 
			numPageNavID 

		INSERT INTO
			@TEMP
		SELECT  
			0,
			0,
			0,
			@numTabID,
			0,
			ISNULL(vcData,'') + 's' [vcPageNavName],
			'../Opportunity/frmOpenBizDocs.aspx?BizDocName=' + vcData + '&BizDocID=' + CONVERT(VARCHAR(10),LD.numListItemID) AS [vcNavURL],
			'',
			0,
			0,
			1 ,
			''
		FROM 
			dbo.ListDetails LD
		JOIN 
			dbo.TreeNavigationAuthorization TNA 
		ON 
			LD.numListItemID = TNA.numPageNavID 
			AND ISNULL(bitVisible,0) = 1  
		WHERE 
			numListID = 27 
			AND ISNULL(TNA.[numDomainID], 0) = @numDomainID
			AND ISNULL(TNA.[numGroupID], 0) = @numGroupID
		ORDER BY 
			numPageNavID 

	END

	BEGIN /******************** Email ***************************/
		SET @numTabID = 44
		SELECT TOP 1 @numModuleID = numModuleID FROM dbo.PageNavigationDTL WHERE numTabID = @numTabID 	

		INSERT INTO
			@TEMP
		SELECT 
		    ISNULL(PND.numParentID,0),
			PND.numParentID,
			0,
			@numTabID,
			PND.numPageNavID AS numPageNavID,
			vcPageNavName,
			ISNULL(vcNavURL, '') as vcNavURL,
			ISNULL(PND.vcAddURL,''),
			ISNULL(PND.bitAddIsPopUp,0),
			0,
			1,
			ISNULL(PND.vcImageURL,'')
		FROM   
			PageNavigationDTL PND
		JOIN 
			dbo.TreeNavigationAuthorization TNA 
		ON 
			PND.numPageNavID = TNA.numPageNavID
			AND TNA.numTabID=@numTabID
		WHERE 
			ISNULL(TNA.bitVisible, 0) = 1      
			AND ISNULL(PND.bitVisible, 0) = 1
			AND PND.numParentID <> 0
			AND PND.numModuleID = @numModuleID
			AND numGroupID = @numGroupID
		ORDER BY 
			numParentID,
			intSortOrder,
			numPageNavID

		UPDATE 
			t1
		SET 
			t1.numParentID = ISNULL((SELECT ID FROM @TEMP t2 WHERE t2.numPageNavID=t1.numParentID AND t2.numTabID=@numTabID),0)
		FROM
			@TEMP t1
		WHERE 
			numTabID=@numTabID
	END

	BEGIN /******************** MARKETING ***************************/
		SET @numTabID = 3
		SELECT TOP 1 @numModuleID = numModuleID FROM dbo.PageNavigationDTL WHERE numTabID = @numTabID 	

		INSERT INTO
			@TEMP
		SELECT 
		    ISNULL(PND.numParentID,0),
			PND.numParentID,
			0,
			@numTabID,
			PND.numPageNavID AS numPageNavID,
			vcPageNavName,
			ISNULL(vcNavURL, '') as vcNavURL,
			ISNULL(PND.vcAddURL,''),
			ISNULL(PND.bitAddIsPopUp,0),
			0,
			1,
			ISNULL(PND.vcImageURL,'')
		FROM   
			PageNavigationDTL PND
		JOIN 
			dbo.TreeNavigationAuthorization TNA 
		ON 
			PND.numPageNavID = TNA.numPageNavID
			AND TNA.numTabID=@numTabID
		WHERE 
			ISNULL(TNA.bitVisible, 0) = 1      
			AND ISNULL(PND.bitVisible, 0) = 1
			AND PND.numParentID <> 0
			AND PND.numModuleID = @numModuleID
			AND numGroupID = @numGroupID
		ORDER BY 
			numParentID,
			intSortOrder,
			numPageNavID

		UPDATE 
			t1
		SET 
			t1.numParentID = ISNULL((SELECT ID FROM @TEMP t2 WHERE t2.numPageNavID=t1.numParentID AND t2.numTabID=@numTabID),0)
		FROM
			@TEMP t1
		WHERE 
			numTabID=@numTabID
	END

	BEGIN /******************** SUPPORT/CASE ***************************/

		SET @numTabID = 4
		SELECT TOP 1 @numModuleID = numModuleID FROM dbo.PageNavigationDTL WHERE numTabID = @numTabID 	

		INSERT INTO
			@TEMP
		SELECT 
			ISNULL(PND.numParentID,0),
			PND.numParentID,
			0,
			@numTabID,
			PND.numPageNavID AS numPageNavID,
			vcPageNavName,
			ISNULL(vcNavURL, '') as vcNavURL,
			ISNULL(PND.vcAddURL,''),
			ISNULL(PND.bitAddIsPopUp,0),
			0,
			1,
			ISNULL(PND.vcImageURL,'')
		FROM   
			PageNavigationDTL PND
		LEFT JOIN 
			dbo.TreeNavigationAuthorization TNA 
		ON 
			PND.numPageNavID = TNA.numPageNavID
			AND TNA.numTabID=@numTabID
		WHERE 
			ISNULL(TNA.bitVisible, 1) = 1
			AND ISNULL(PND.bitVisible, 1) = 1
			AND PND.numParentID <> 0
			AND PND.numModuleID = @numModuleID
			AND numGroupID = @numGroupID
		ORDER BY 
			numParentID,
			intSortOrder,
			numPageNavID

		UPDATE 
			t1
		SET 
			t1.numParentID = ISNULL((SELECT ID FROM @TEMP t2 WHERE t2.numPageNavID=t1.numParentID AND t2.numTabID=@numTabID),0)
		FROM
			@TEMP t1
		WHERE 
			numTabID=@numTabID

	END

	BEGIN /******************** REPORTS ***************************/

		SET @numTabID = 6
		SELECT TOP 1 @numModuleID = numModuleID FROM dbo.PageNavigationDTL WHERE numTabID = @numTabID 	

		INSERT INTO
			@TEMP
		SELECT 
		    ISNULL(PND.numParentID,0),
			PND.numParentID,
			0,
			@numTabID,
			PND.numPageNavID AS numPageNavID,
			vcPageNavName,
			ISNULL(vcNavURL, '') as vcNavURL,
			ISNULL(PND.vcAddURL,''),
			ISNULL(PND.bitAddIsPopUp,0),
			0,
			1,
			ISNULL(PND.vcImageURL,'')
		FROM   
			PageNavigationDTL PND
		LEFT JOIN 
			dbo.TreeNavigationAuthorization TNA 
		ON 
			PND.numPageNavID = TNA.numPageNavID
			AND TNA.numTabID=@numTabID
		WHERE 
			ISNULL(TNA.bitVisible, 1) = 1
			AND ISNULL(PND.bitVisible, 1) = 1
			AND PND.numParentID <> 0
			AND PND.numModuleID = @numModuleID
			AND numGroupID = @numGroupID
		ORDER BY 
			numParentID,
			intSortOrder,
			numPageNavID

		UPDATE 
			t1
		SET 
			t1.numParentID = ISNULL((SELECT ID FROM @TEMP t2 WHERE t2.numPageNavID=t1.numParentID AND t2.numTabID=@numTabID),0)
		FROM
			@TEMP t1
		WHERE 
			numTabID=@numTabID

	END

	BEGIN /******************** RELATIONSHIP ***********************/

		SET @numTabID = 7
		SELECT TOP 1 @numModuleID = numModuleID FROM dbo.PageNavigationDTL WHERE numTabID = @numTabID 	

		INSERT INTO 
			@TEMP
		SELECT    
			ISNULL(PND.numParentID,0),
			PND.numParentID AS numParentID,
			0,
			@numTabID,
			PND.numPageNavID AS numPageNavID,
			ISNULL(PND.vcPageNavName, '') AS [vcNodeName],
			ISNULL(vcNavURL, '') AS vcNavURL,
			ISNULL(PND.vcAddURL,''),
			ISNULL(PND.bitAddIsPopUp,0),
			ISNULL(TreeNodeOrder.numOrder,1000) AS numOrder,
			ISNULL(TNA.bitVisible,1) AS bitVisible,
			ISNULL(PND.vcImageURL,'')
		FROM      
			PageNavigationDTL PND
		LEFT JOIN 
			dbo.TreeNavigationAuthorization TNA 
		ON 
			TNA.numPageNavID = PND.numPageNavID AND
			TNA.numDomainID = @numDomainID AND 
			TNA.numGroupID = @numGroupID
			AND TNA.numTabID=@numTabID
		LEFT JOIN
			dbo.TreeNodeOrder 
		ON
			dbo.TreeNodeOrder.numTabID = @numTabID AND
			dbo.TreeNodeOrder.numDomainID = @numDomainID AND
			dbo.TreeNodeOrder.numGroupID = @numGroupID AND
			dbo.TreeNodeOrder.numPageNavID = PND.numPageNavID
		WHERE     
			ISNULL(PND.bitVisible, 0) = 1
			AND ISNULL(TNA.bitVisible, 0) = 1
			AND PND.numParentID <> 0
			AND numModuleID = @numModuleID

		UPDATE 
			t1
		SET 
			t1.numParentID = ISNULL((SELECT ID FROM @TEMP t2 WHERE t2.numPageNavID=t1.numParentID AND t2.numTabID=@numTabID),0)
		FROM
			@TEMP t1
		WHERE 
			numTabID=@numTabID

		INSERT INTO 
			@TEMP
		SELECT
			0,
			0 AS numParentID,
			ListDetails.numListItemID,
			@numTabID,
			0 AS numPageNavID,
			ListDetails.vcData AS [vcNodeName],
			'../prospects/frmCompanyList.aspx?RelId='+ convert(varchar(10),ListDetails.numListItemID),
			CASE WHEN ListDetails.numListItemID = 93 THEN '' ELSE CONCAT('~/include/frmAddOrganization.aspx?RelID=',ListDetails.numListItemID,'&FormID=36') END,
			CASE WHEN ListDetails.numListItemID = 93 THEN 0 ELSE 1 END,
			ISNULL(TreeNodeOrder.numOrder,2000) AS numOrder,
			ISNULL(dbo.TreeNodeOrder.bitVisible,1) AS bitVisible,
			''
		FROM
			ListDetails
		LEFT JOIN 
			dbo.TreeNodeOrder 
		ON
			dbo.TreeNodeOrder.numListItemID = ListDetails.numListItemID AND
			dbo.TreeNodeOrder.numParentID = 6 AND
			dbo.TreeNodeOrder.numTabID = @numTabID AND
			dbo.TreeNodeOrder.numDomainID = @numDomainID AND
			dbo.TreeNodeOrder.numGroupID = @numGroupID AND
			dbo.TreeNodeOrder.numPageNavID IS NULL
		WHERE
			numListID = 5 AND
			(ListDetails.numDomainID = @numDomainID  OR constFlag = 1) AND
			(ISNULL(bitDelete,0) = 0 OR constFlag = 1) AND
			ListDetails.numListItemID <> 46 

		INSERT INTO 
			@TEMP
		SELECT 
			0,
			(SELECT ID FROM @TEMP WHERE numListItemID = FRDTL.numPrimaryListItemID),
			L2.numListItemID,   
			@numTabID,
			0,
			L2.vcData AS [vcNodeName],
			CONCAT('../prospects/frmCompanyList.aspx?RelId=',FRDTL.numPrimaryListItemID,'&profileid=',numSecondaryListItemID),
			--CONCAT('~/include/frmAddOrganization.aspx?RelID=',FRDTL.numPrimaryListItemID,'&profileid=',numSecondaryListItemID,'&FormID=36'),
			CASE WHEN L2.numListItemID = 37257 THEN '' ELSE CONCAT('~/include/frmAddOrganization.aspx?RelID=',FRDTL.numPrimaryListItemID,'&profileid=',numSecondaryListItemID,'&FormID=36') END,
			--1,
			CASE WHEN L2.numListItemID = 37257 THEN 0 ELSE 1 END,
			ISNULL(TreeNodeOrder.numOrder,3000) AS numOrder,
			ISNULL(dbo.TreeNodeOrder.bitVisible,1) AS bitVisible,
			''
		FROM      
			FieldRelationship FR
		join 
			FieldRelationshipDTL FRDTL on FRDTL.numFieldRelID = FR.numFieldRelID
		join 
			ListDetails L1 on numPrimaryListItemID = L1.numListItemID
		join 
			ListDetails L2 on numSecondaryListItemID = L2.numListItemID
		LEFT JOIN 
			dbo.TreeNodeOrder 
		ON
			dbo.TreeNodeOrder.numListItemID = L2.numListItemID AND
			dbo.TreeNodeOrder.numParentID = FRDTL.numPrimaryListItemID AND
			dbo.TreeNodeOrder.numTabID = @numTabID AND
			dbo.TreeNodeOrder.numDomainID = @numDomainID AND
			dbo.TreeNodeOrder.numGroupID = @numGroupID AND
			dbo.TreeNodeOrder.numPageNavID IS NULL
		WHERE     
			numPrimaryListItemID <> 46
			and FR.numPrimaryListID = 5
			and FR.numSecondaryListID = 21
			and FR.numDomainID = @numDomainID
			and L2.numDomainID = @numDomainID

		INSERT INTO
			@TEMP
		SELECT 
			0,
			(SELECT ID FROM @TEMP WHERE numPageNavID=11),
			L2.numListItemID,
			@numTabID,
			11,
			L2.vcData AS [vcNodeName],
			'../prospects/frmProspectList.aspx?numProfile=' + CONVERT(VARCHAR(10), L2.numListItemID) AS vcNavURL,
			CONCAT('~/include/frmAddOrganization.aspx?RelID=3&profileid=',numSecondaryListItemID,'&FormID=35'),
			1,
			ISNULL(TreeNodeOrder.numOrder,4000) AS numOrder,
			ISNULL(dbo.TreeNodeOrder.bitVisible,1) AS bitVisible,
			''
		FROM    
			FieldRelationship AS FR
		INNER JOIN 
			FieldRelationshipDTL AS FRD ON FRD.numFieldRelID = FR.numFieldRelID
		INNER JOIN 
			ListDetails AS L1 ON L1.numListItemID = FRD.numPrimaryListItemID
		INNER JOIN 
			ListDetails AS L2 ON L2.numListItemID = FRD.numSecondaryListItemID
		LEFT JOIN 
			dbo.TreeNodeOrder 
		ON
			dbo.TreeNodeOrder.numListItemID = L2.numListItemID AND
			dbo.TreeNodeOrder.numParentID = 11 AND
			dbo.TreeNodeOrder.numTabID = @numTabID AND
			dbo.TreeNodeOrder.numDomainID = @numDomainID AND
			dbo.TreeNodeOrder.numGroupID = @numGroupID AND
			dbo.TreeNodeOrder.numPageNavID IS NULL
		WHERE 
			FRD.numPrimaryListItemID = 46
			AND FR.numDomainID = @numDomainID

		INSERT INTO
			@TEMP
		SELECT 
			0,
			(SELECT ID FROM @TEMP WHERE numPageNavID=12),
			L2.numListItemID,
			@numTabID,
			12,
			L2.vcData AS [vcNodeName],
			'../account/frmAccountList.aspx?numProfile=' + CONVERT(VARCHAR(10), L2.numListItemID) AS vcNavURL,
			CONCAT('~/include/frmAddOrganization.aspx?RelID=2&profileid=',numSecondaryListItemID,'&FormID=36'),
			1,
			ISNULL(TreeNodeOrder.numOrder,5000) AS numOrder,
			ISNULL(dbo.TreeNodeOrder.bitVisible,1) AS bitVisible,
			''
		FROM    
			FieldRelationship AS FR
		INNER JOIN 
			FieldRelationshipDTL AS FRD ON FRD.numFieldRelID = FR.numFieldRelID
		INNER JOIN 
			ListDetails AS L1 ON L1.numListItemID = FRD.numPrimaryListItemID
		INNER JOIN 
			ListDetails AS L2 ON L2.numListItemID = FRD.numSecondaryListItemID
		LEFT JOIN 
			dbo.TreeNodeOrder 
		ON
			dbo.TreeNodeOrder.numListItemID = L2.numListItemID AND
			dbo.TreeNodeOrder.numParentID = 12 AND
			dbo.TreeNodeOrder.numTabID = @numTabID AND
			dbo.TreeNodeOrder.numDomainID = @numDomainID AND
			dbo.TreeNodeOrder.numGroupID = @numGroupID AND
			dbo.TreeNodeOrder.numPageNavID IS NULL
		WHERE 
			FRD.numPrimaryListItemID = 46
			AND FR.numDomainID = @numDomainID

		INSERT INTO 
			@TEMP
		SELECT 
			0,
			(SELECT ID FROM @TEMP WHERE numListItemID=FRD.numPrimaryListItemID AND numPageNavID=11),
			L2.numListItemID,
			@numTabID,
			0,
			L2.vcData AS [vcNodeName],
			'../account/frmProspectList.aspx?numProfile=' + CONVERT(VARCHAR(10), FRD.numPrimaryListItemID) + '&numTerritoryID=' + CONVERT(VARCHAR(10), L2.numListItemID) AS vcNavURL,
			CONCAT('~/include/frmAddOrganization.aspx?RelID=3&profileid=',FRD.numPrimaryListItemID,'&FormID=35','&numTerritoryID=',L2.numListItemID),
			1,
			ISNULL(TreeNodeOrder.numOrder,7000) AS numOrder,
			ISNULL(dbo.TreeNodeOrder.bitVisible,1) AS bitVisible,
			''
		FROM    
			FieldRelationship AS FR
		INNER JOIN 
			FieldRelationshipDTL AS FRD ON FRD.numFieldRelID = FR.numFieldRelID
		INNER JOIN 
			ListDetails AS L1 ON L1.numListItemID = FRD.numPrimaryListItemID
		INNER JOIN 
			ListDetails AS L2 ON L2.numListItemID = FRD.numSecondaryListItemID
		LEFT JOIN 
			dbo.TreeNodeOrder 
		ON
			dbo.TreeNodeOrder.numListItemID = L2.numListItemID AND
			dbo.TreeNodeOrder.numParentID = 11 AND
			dbo.TreeNodeOrder.numTabID = @numTabID AND
			dbo.TreeNodeOrder.numDomainID = @numDomainID AND
			dbo.TreeNodeOrder.numGroupID = @numGroupID AND
			dbo.TreeNodeOrder.numPageNavID IS NULL
		WHERE   
			FRD.numPrimaryListItemID IN (SELECT numSecondaryListItemID FROM FieldRelationshipDTL FRDL JOIN FieldRelationship FRP ON FRDL.numFieldRelID = FRP.numFieldRelID WHERE numDomainID=@numDomainID AND numPrimaryListItemID=46)
			AND FR.numDomainID = @numDomainID

		INSERT INTO 
			@TEMP
		SELECT 
			0,
			(SELECT ID FROM @TEMP WHERE numListItemID=FRD.numPrimaryListItemID AND numPageNavID=12),
			L2.numListItemID,
			@numTabID,
			0,
			L2.vcData AS [vcNodeName],
			'../account/frmAccountList.aspx?numProfile=' + CONVERT(VARCHAR(10), FRD.numPrimaryListItemID) + '&numTerritoryID=' + CONVERT(VARCHAR(10), L2.numListItemID) AS vcNavURL,
			CONCAT('~/include/frmAddOrganization.aspx?RelID=2&profileid=',FRD.numPrimaryListItemID,'&FormID=36','&numTerritoryID=',L2.numListItemID),
			1,
			ISNULL(TreeNodeOrder.numOrder,8000) AS numOrder,
			ISNULL(dbo.TreeNodeOrder.bitVisible,1) AS bitVisible,
			''
		FROM    
			FieldRelationship AS FR
		INNER JOIN 
			FieldRelationshipDTL AS FRD ON FRD.numFieldRelID = FR.numFieldRelID
		INNER JOIN 
			ListDetails AS L1 ON L1.numListItemID = FRD.numPrimaryListItemID
		INNER JOIN 
			ListDetails AS L2 ON L2.numListItemID = FRD.numSecondaryListItemID
		LEFT JOIN 
			dbo.TreeNodeOrder 
		ON
			dbo.TreeNodeOrder.numListItemID = L2.numListItemID AND
			dbo.TreeNodeOrder.numParentID = 12 AND
			dbo.TreeNodeOrder.numTabID = @numTabID AND
			dbo.TreeNodeOrder.numDomainID = @numDomainID AND
			dbo.TreeNodeOrder.numGroupID = @numGroupID AND
			dbo.TreeNodeOrder.numPageNavID IS NULL
		WHERE   
			FRD.numPrimaryListItemID IN (SELECT numSecondaryListItemID FROM FieldRelationshipDTL FRDL JOIN FieldRelationship FRP ON FRDL.numFieldRelID = FRP.numFieldRelID WHERE numDomainID=@numDomainID AND numPrimaryListItemID=46)
			AND FR.numDomainID = @numDomainID

		

	END

	BEGIN /******************** DOCUMENTS ***********************/
		SET @numTabID = 8

		INSERT INTO 
			@TEMP
		SELECT 
			0,
			0,
			0,
			@numTabID,
			0,
			'Regular Documents' as vcPageNavName,
			'../Documents/frmRegularDocList.aspx?Category=0',
			'~/Documents/frmGenDocPopUp.aspx',
			1,
			1,
			1,
			''


		INSERT INTO 
			@TEMP
		SELECT  
			0,
			SCOPE_IDENTITY(),
			Ld.numListItemId,
			@numTabID,
			0,
			vcData vcPageNavName,
			'../Documents/frmRegularDocList.aspx?Category=' + cast(Ld.numListItemId as varchar(10)),
			CASE WHEN vcData = 'Email Template' THEN '~/Marketing/frmEmailTemplate.aspx' ELSE '' END,
			CASE WHEN vcData = 'Email Template' THEN 1 ELSE 0 END,
			ISNULL(intSortOrder, LD.sintOrder) SortOrder,
			1,
			''
		FROM    
			ListDetails LD
		LEFT JOIN 
			ListOrder LO 
		ON 
			LD.numListItemID = LO.numListItemID
			AND LO.numDomainId = @numDomainID
		WHERE   
			Ld.numListID = 29
			AND (constFlag = 1 OR Ld.numDomainID = @numDomainID)

	END

	BEGIN /******************** ACCOUNTING **********************/
	
		SET @numTabID = 45
		SELECT TOP 1 @numModuleID = numModuleID FROM dbo.PageNavigationDTL WHERE numTabID = @numTabID

		INSERT INTO 
			@TEMP
		SELECT 
		    ISNULL(PND.numParentID,0),
			PND.numParentID,
			0,
			@numTabID,
			PND.numPageNavID,
			vcPageNavName,
			ISNULL(vcNavURL,'') AS vcNavURL,
			ISNULL(PND.vcAddURL,''),
			ISNULL(PND.bitAddIsPopUp,0),
			0,
			1 ,
			ISNULL(PND.vcImageURL,'')     
		FROM 
			PageNavigationDTL PND
		JOIN 
			dbo.TreeNavigationAuthorization TNA 
		ON 
			PND.numPageNavID = TNA.numPageNavID
			AND TNA.numTabID=@numTabID
		WHERE  
			ISNULL(TNA.bitVisible, 1) = 1
			AND ISNULL(PND.bitVisible, 1) = 1
			AND numModuleID=@numModuleID
			AND TNA.numDomainID = @numDomainID
			AND numGroupID = @numGroupID
			AND numParentID <> 0
		ORDER BY 
			numParentID,PND.numPageNavID 
	
		INSERT INTO 
			@TEMP
		SELECT 
			0,
			(CASE numFinancialViewID 
				WHEN 26770 THEN 96 /*Profit & Loss*/
				WHEN 26769 THEN 176 /*Income & Expense*/
				WHEN 26768 THEN 98 /*Cash Flow Statement*/
				WHEN 26767 THEN 97 /*Balance Sheet*/
				WHEN 26766 THEN 81 /*A/R & A/P Aging*/
				WHEN 26766 THEN 82 /*A/R & A/P Aging*/
				ELSE 0
			END),
			numFRID,
			@numTabID,
			0,
			vcReportName,
			CASE numFinancialViewID 
				WHEN 26770 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =96 AND numModuleID=35)  /*Profit & Loss*/
				WHEN 26769 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =176 AND numModuleID=35) /*Income & Expense*/
				WHEN 26768 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =98 AND numModuleID=35) /*Cash Flow Statement*/
				WHEN 26767 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =97 AND numModuleID=35) /*Balance Sheet*/
				WHEN 26766 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =81 AND numModuleID=35) /*A/R & A/P Aging*/
				WHEN 26766 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =82 AND numModuleID=35) /*A/R & A/P Aging*/
				ELSE ''
			END + 
			CASE CHARINDEX('?',(CASE numFinancialViewID 
				WHEN 26770 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =96 AND numModuleID=35)  /*Profit & Loss*/
				WHEN 26769 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =176 AND numModuleID=35) /*Income & Expense*/
				WHEN 26768 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =98 AND numModuleID=35) /*Cash Flow Statement*/
				WHEN 26767 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =97 AND numModuleID=35) /*Balance Sheet*/
				WHEN 26766 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =81 AND numModuleID=35) /*A/R & A/P Aging*/
				WHEN 26766 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =82 AND numModuleID=35) /*A/R & A/P Aging*/
				ELSE ''
			END)) 
				WHEN 0 THEN '?FRID='
				ELSE '&FRID='
			END + CAST(numFRID AS VARCHAR),
			'',
			0,
			0,
			1,
			''
		FROM 
			dbo.FinancialReport 
		WHERE 
			numDomainID=@numDomainID
	
		UPDATE 
			t1
		SET 
			t1.numParentID = ISNULL((SELECT ID FROM @TEMP t2 WHERE t2.numPageNavID=t1.numParentID AND t2.numTabID=@numTabID),0)
		FROM
			@TEMP t1
		WHERE 
			numTabID=@numTabID
	  
	END

	BEGIN /******************** ITEMS **********************/

		SET @numTabID = 80
		SELECT TOP 1 @numModuleID = numModuleID FROM dbo.PageNavigationDTL WHERE numTabID = @numTabID 	

		INSERT INTO
			@TEMP
		SELECT 
			ISNULL(PND.numParentID,0),
			numParentID,
			0,
			@numTabID,
			PND.numPageNavID,
			vcPageNavName,
			isnull(vcNavURL, '') as vcNavURL,
			ISNULL(PND.vcAddURL,''),
			ISNULL(PND.bitAddIsPopUp,0),
			0,
			1,
			ISNULL(PND.vcImageURL,'')
		FROM 
			PageNavigationDTL PND
		JOIN 
			dbo.TreeNavigationAuthorization TNA 
		ON 
			PND.numPageNavID = TNA.numPageNavID
			AND TNA.numTabID=@numTabID
		WHERE
			ISNULL(TNA.bitVisible, 0) = 1
			AND ISNULL(PND.bitVisible, 0) = 1
			AND PND.numModuleID = @numModuleID
			AND numGroupID = @numGroupID
			AND numParentID <> 0
		ORDER BY 
			numParentID,
			intSortOrder,
			numPageNavID

		UPDATE 
			t1
		SET 
			t1.numParentID = ISNULL((SELECT ID FROM @TEMP t2 WHERE t2.numPageNavID=t1.numParentID AND t2.numTabID=@numTabID),0)
		FROM
			@TEMP t1
		WHERE 
			numTabID=@numTabID
	END

	BEGIN /******************** Activities ***************************/

		SET @numTabID = 36
		SELECT TOP 1 @numModuleID = numModuleID FROM dbo.PageNavigationDTL WHERE numTabID = @numTabID 	

		INSERT INTO
			@TEMP
		SELECT 
			ISNULL(PND.numParentID,0),
			PND.numParentID,
			0,
			@numTabID,
			PND.numPageNavID AS numPageNavID,
			vcPageNavName,
			ISNULL(vcNavURL, '') as vcNavURL,
			ISNULL(PND.vcAddURL,''),
			ISNULL(PND.bitAddIsPopUp,0),
			0,
			1,
			ISNULL(PND.vcImageURL,'')
		FROM   
			PageNavigationDTL PND
		LEFT JOIN 
			dbo.TreeNavigationAuthorization TNA 
		ON 
			PND.numPageNavID = TNA.numPageNavID
			AND TNA.numTabID=@numTabID
		WHERE 
			ISNULL(TNA.bitVisible, 1) = 1
			AND ISNULL(PND.bitVisible, 1) = 1
			AND PND.numParentID <> 0
			AND PND.numModuleID = @numModuleID
			AND numGroupID = @numGroupID
		ORDER BY 
			numParentID,
			intSortOrder,
			numPageNavID

		UPDATE 
			t1
		SET 
			t1.numParentID = ISNULL((SELECT ID FROM @TEMP t2 WHERE t2.numPageNavID=t1.numParentID AND t2.numTabID=@numTabID),0)
		FROM
			@TEMP t1
		WHERE 
			numTabID=@numTabID

	END

	BEGIN /******************** HUMAN RESOURCE ***************************/

		SET @numTabID = (SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Human Resources' AND numDomainID=1)
		SELECT TOP 1 @numModuleID = numModuleID FROM dbo.PageNavigationDTL WHERE numTabID = @numTabID 	

		INSERT INTO
			@TEMP
		SELECT 
			ISNULL(PND.numParentID,0),
			PND.numParentID,
			0,
			@numTabID,
			PND.numPageNavID AS numPageNavID,
			vcPageNavName,
			ISNULL(vcNavURL, '') as vcNavURL,
			ISNULL(PND.vcAddURL,''),
			ISNULL(PND.bitAddIsPopUp,0),
			0,
			1,
			ISNULL(PND.vcImageURL,'')
		FROM   
			PageNavigationDTL PND
		LEFT JOIN 
			dbo.TreeNavigationAuthorization TNA 
		ON 
			PND.numPageNavID = TNA.numPageNavID
			AND TNA.numTabID=@numTabID
		WHERE 
			ISNULL(TNA.bitVisible, 1) = 1
			AND ISNULL(PND.bitVisible, 1) = 1
			AND PND.numParentID <> 0
			AND PND.numModuleID = @numModuleID
			AND numGroupID = @numGroupID
		ORDER BY 
			numParentID,
			intSortOrder,
			numPageNavID

		UPDATE 
			t1
		SET 
			t1.numParentID = ISNULL((SELECT ID FROM @TEMP t2 WHERE t2.numPageNavID=t1.numParentID AND t2.numTabID=@numTabID),0)
		FROM
			@TEMP t1
		WHERE 
			numTabID=@numTabID

	END

	BEGIN /******************** Procurement ***************************/

		SET @numTabID = (SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Procurement' AND numDomainID=1)
		SELECT TOP 1 @numModuleID = numModuleID FROM dbo.PageNavigationDTL WHERE numTabID = @numTabID 	

		INSERT INTO
			@TEMP
		SELECT 
			ISNULL(PND.numParentID,0),
			PND.numParentID,
			0,
			@numTabID,
			PND.numPageNavID AS numPageNavID,
			vcPageNavName,
			ISNULL(vcNavURL, '') as vcNavURL,
			ISNULL(PND.vcAddURL,''),
			ISNULL(PND.bitAddIsPopUp,0),
			0,
			1,
			ISNULL(PND.vcImageURL,'')
		FROM   
			PageNavigationDTL PND
		LEFT JOIN 
			dbo.TreeNavigationAuthorization TNA 
		ON 
			PND.numPageNavID = TNA.numPageNavID
			AND TNA.numTabID=@numTabID
		WHERE 
			ISNULL(TNA.bitVisible, 1) = 1
			AND ISNULL(PND.bitVisible, 1) = 1
			AND PND.numParentID <> 0
			AND PND.numModuleID = @numModuleID
			AND numGroupID = @numGroupID
		ORDER BY 
			numParentID,
			intSortOrder,
			numPageNavID

		UPDATE 
			t1
		SET 
			t1.numParentID = ISNULL((SELECT ID FROM @TEMP t2 WHERE t2.numPageNavID=t1.numParentID AND t2.numTabID=@numTabID),0)
		FROM
			@TEMP t1
		WHERE 
			numTabID=@numTabID

	END
	BEGIN /******************** CONTACTS ***************************/

		SET @numTabID = (SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Contacts' AND numDomainID=1 AND tintTabType=1)
		SELECT TOP 1 @numModuleID = numModuleID FROM dbo.PageNavigationDTL WHERE numTabID = @numTabID 	

		INSERT INTO
			@TEMP
		SELECT 
			ISNULL(PND.numParentID,0),
			PND.numParentID,
			0,
			@numTabID,
			PND.numPageNavID AS numPageNavID,
			vcPageNavName,
			ISNULL(vcNavURL, '') as vcNavURL,
			ISNULL(PND.vcAddURL,''),
			ISNULL(PND.bitAddIsPopUp,0),
			0,
			1,
			ISNULL(PND.vcImageURL,'')
		FROM   
			PageNavigationDTL PND
		LEFT JOIN 
			dbo.TreeNavigationAuthorization TNA 
		ON 
			PND.numPageNavID = TNA.numPageNavID
			AND TNA.numTabID=@numTabID
		WHERE 
			ISNULL(TNA.bitVisible, 1) = 1
			AND ISNULL(PND.bitVisible, 1) = 1
			AND PND.numParentID <> 0
			AND PND.numModuleID = @numModuleID
			AND numGroupID = @numGroupID
		ORDER BY 
			numParentID,
			intSortOrder,
			numPageNavID

		UPDATE 
			t1
		SET 
			t1.numParentID = ISNULL((SELECT ID FROM @TEMP t2 WHERE t2.numPageNavID=t1.numParentID AND t2.numTabID=@numTabID),0)
		FROM
			@TEMP t1
		WHERE 
			numTabID=@numTabID

		INSERT INTO
			@TEMP
		SELECT 
			0,
			(SELECT ISNULL(ID,0) FROM @TEMP WHERE numPageNavID=13),
			listdetails.numListItemID,
			@numTabID,
			0,
			listdetails.vcData AS [vcNodeName],
			'../contact/frmContactList.aspx?ContactType=' + CONVERT(VARCHAR(10), listdetails.numListItemID) AS vcNavURL,
		   '~/Contact/newcontact.aspx',
		   1,
			ISNULL(TreeNodeOrder.numOrder,6000) AS numOrder,
			ISNULL(dbo.TreeNodeOrder.bitVisible,1) AS bitVisible,
			''
		FROM    
			listdetails
		LEFT JOIN 
			dbo.TreeNodeOrder 
		ON
			dbo.TreeNodeOrder.numListItemID = listdetails.numListItemID AND
			dbo.TreeNodeOrder.numParentID = 13 AND
			dbo.TreeNodeOrder.numTabID = @numTabID AND
			dbo.TreeNodeOrder.numDomainID = @numDomainID AND
			dbo.TreeNodeOrder.numGroupID = @numGroupID AND
			dbo.TreeNodeOrder.numPageNavID IS NULL
		WHERE   
			numListID = 8
			AND (constFlag = 1 OR listdetails.numDomainID = @numDomainID)

		INSERT INTO
			@TEMP
		SELECT 
			0,
			(SELECT ISNULL(ID,0) FROM @TEMP WHERE numPageNavID=13),
			101 AS numListItemID,
			@numTabID,
			0,
			'Primary Contact' AS [vcNodeName],
			'../Contact/frmcontactList.aspx?ContactType=101',
		 '~/Contact/newcontact.aspx',
		   1,
			ISNULL((
						SELECT 
							numOrder 
						FROM 
							dbo.TreeNodeOrder 
						WHERE
							dbo.TreeNodeOrder.numListItemID = 101 AND
							dbo.TreeNodeOrder.numParentID = 13 AND
							dbo.TreeNodeOrder.numTabID = @numTabID AND
							dbo.TreeNodeOrder.numDomainID = @numDomainID AND
							dbo.TreeNodeOrder.numGroupID = @numGroupID AND
							dbo.TreeNodeOrder.numPageNavID IS NULL
					),7000) AS numOrder,
			ISNULL((
						SELECT 
							bitVisible 
						FROM 
							dbo.TreeNodeOrder 
						WHERE
							dbo.TreeNodeOrder.numListItemID = 101 AND
							dbo.TreeNodeOrder.numParentID = 13 AND
							dbo.TreeNodeOrder.numTabID = @numTabID AND
							dbo.TreeNodeOrder.numDomainID = @numDomainID AND
							dbo.TreeNodeOrder.numGroupID = @numGroupID AND
							dbo.TreeNodeOrder.numPageNavID IS NULL
					),1) AS bitVisible,
					''

		UPDATE 
			t1
		SET 
			t1.numParentID = 13
		FROM
			@TEMP t1
		WHERE 
			numTabID=@numTabID AND numParentID IS NULL

	END

	BEGIN /******************** Manufacturing ***************************/

		SET @numTabID = (SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Manufacturing' AND numDomainID=1)
		SELECT TOP 1 @numModuleID = numModuleID FROM dbo.PageNavigationDTL WHERE numTabID = @numTabID 	

		INSERT INTO
			@TEMP
		SELECT 
			ISNULL(PND.numParentID,0),
			PND.numParentID,
			0,
			@numTabID,
			PND.numPageNavID AS numPageNavID,
			vcPageNavName,
			ISNULL(vcNavURL, '') as vcNavURL,
			ISNULL(PND.vcAddURL,''),
			ISNULL(PND.bitAddIsPopUp,0),
			0,
			1,
			ISNULL(PND.vcImageURL,'')
		FROM   
			PageNavigationDTL PND
		LEFT JOIN 
			dbo.TreeNavigationAuthorization TNA 
		ON 
			PND.numPageNavID = TNA.numPageNavID
			AND TNA.numTabID=@numTabID
		WHERE 
			ISNULL(TNA.bitVisible, 1) = 1
			AND ISNULL(PND.bitVisible, 1) = 1
			AND PND.numParentID <> 0
			AND PND.numModuleID = @numModuleID
			AND numGroupID = @numGroupID
		ORDER BY 
			numParentID,
			intSortOrder,
			numPageNavID

		UPDATE 
			t1
		SET 
			t1.numParentID = ISNULL((SELECT ID FROM @TEMP t2 WHERE t2.numPageNavID=t1.numParentID AND t2.numTabID=@numTabID),0)
		FROM
			@TEMP t1
		WHERE 
			numTabID=@numTabID

	END

	BEGIN /******************** Projects ***************************/

		SET @numTabID = (SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Projects' AND numDomainID=1 AND tintTabType=1)
		SELECT TOP 1 @numModuleID = numModuleID FROM dbo.PageNavigationDTL WHERE numTabID = @numTabID 	

		INSERT INTO
			@TEMP
		SELECT 
			ISNULL(PND.numParentID,0),
			PND.numParentID,
			0,
			@numTabID,
			PND.numPageNavID AS numPageNavID,
			vcPageNavName,
			ISNULL(vcNavURL, '') as vcNavURL,
			ISNULL(PND.vcAddURL,''),
			ISNULL(PND.bitAddIsPopUp,0),
			0,
			1,
			ISNULL(PND.vcImageURL,'')
		FROM   
			PageNavigationDTL PND
		LEFT JOIN 
			dbo.TreeNavigationAuthorization TNA 
		ON 
			PND.numPageNavID = TNA.numPageNavID
			AND TNA.numTabID=@numTabID
		WHERE 
			ISNULL(TNA.bitVisible, 1) = 1
			AND ISNULL(PND.bitVisible, 1) = 1
			AND PND.numParentID <> 0
			AND PND.numModuleID = @numModuleID
			AND numGroupID = @numGroupID
		ORDER BY 
			numParentID,
			intSortOrder,
			numPageNavID

		UPDATE 
			t1
		SET 
			t1.numParentID = ISNULL((SELECT ID FROM @TEMP t2 WHERE t2.numPageNavID=t1.numParentID AND t2.numTabID=@numTabID),0)
		FROM
			@TEMP t1
		WHERE 
			numTabID=@numTabID

	END


	;WITH CTE(ID) AS 
	(
		SELECT
			ID
		FROM
			@TEMP t1
		WHERE 
			ISNULL(numOrigParentID,0) > 0
			AND numOrigParentID NOT IN (SELECT numPageNavID FROM PageNavigationDTL WHERE numParentID=0 AND numTabID=t1.numTabID)
			AND numOrigParentID NOT IN (SELECT numPageNavID FROM @TEMP t2 WHERE t2.numTabID = t1.numTabID)
		UNION ALL
		SELECT 
			t2.ID
		FROM
			@TEMP t2
		JOIN
			CTE c
		ON
			t2.numParentID=c.ID
	)

	DELETE FROM @TEMP WHERE ID IN (SELECT ID FROM CTE)

	
	/** Admin Menu **/
	INSERT INTO 
		@TEMP
	SELECT 
		numParentID,
		numParentID,
		0,
		-1,
		PND.numPageNavID,
		vcPageNavName,
		ISNULL(vcNavURL, '') as vcNavURL,
		'',
		0,
		ISNULL(PND.intSortOrder,0) sintOrder,
		1,
		ISNULL(vcImageURL,'')
	FROM   
		PageNavigationDTL PND
	JOIN 
		dbo.TreeNavigationAuthorization TNA 
	ON 
		PND.numPageNavID = TNA.numPageNavID
		AND PND.numTabID = TNA.numTabID
	WHERE
		ISNULL(TNA.bitVisible, 0) = 1
		AND ISNULL(PND.bitVisible, 0) = 1
		AND PND.numModuleID = 13 --AND bitVisible=1  
		AND TNA.numDomainID = @numDomainID
		AND TNA.numGroupID = @numGroupID
	ORDER BY
		PND.numParentID, 
		PND.intSortOrder,
		(CASE WHEN PND.numPageNavID=79 THEN 2000 ELSE 0 END),	
		numPageNavID   

	-- MAKE FIRST LEVEL CHILD AS PARENT BECUASE WE ARE NOW NOT USING TREE STRUCTURE AND DIPLAYING ITEMS IN MENU
	UPDATE 
		@TEMP
	SET 
		numParentID = 0
	WHERE
		numTabID = -1
		AND numPageNavID = (SELECT numPageNavID FROM @TEMP WHERE numTabID=-1 AND numParentID = 0)

	-- REMOVE ALL CHILD WHOSE PARENT IS HIDDEN
	DELETE FROM @TEMP WHERE numTabID = -1 AND ISNULL(numPageNavID,0) <> 0 AND numParentID NOT IN (SELECT numPageNavID FROM @TEMP WHERE numTabID = -1)

	-- REMOVE ALL CHILD WHOSE PARENT IS HIDDEN
	DELETE FROM @TEMP WHERE numParentID = 0 AND numTabID=-1

	-- UPDATE PARENT DETAIL
	UPDATE 
		t1
	SET 
		t1.numParentID = ISNULL((SELECT ID FROM @TEMP t2 WHERE t2.numTabID=-1 AND t2.numPageNavID=t1.numParentID),0)
	FROM
		@TEMP t1
	WHERE
		t1.numTabID = -1


	/** Advance Search **/
	INSERT INTO 
		@TEMP
	SELECT 
		numParentID,
		numParentID,
		0,
		-3,
		PND.numPageNavID,
		vcPageNavName,
		ISNULL(vcNavURL, '') as vcNavURL,
		'',
		0,
		ISNULL(PND.intSortOrder,0) sintOrder,
		1,
		ISNULL(vcImageURL,'')
	FROM   
		PageNavigationDTL PND
	JOIN 
		dbo.TreeNavigationAuthorization TNA 
	ON 
		PND.numPageNavID = TNA.numPageNavID
		AND PND.numTabID = TNA.numTabID
	WHERE
		ISNULL(TNA.bitVisible, 0) = 1
		AND ISNULL(PND.bitVisible, 0) = 1
		AND PND.numModuleID = 9
		AND TNA.numDomainID = @numDomainID
		AND TNA.numGroupID = @numGroupID
	ORDER BY
		PND.numParentID, 
		sintOrder,
		numPageNavID   

	-- MAKE FIRST LEVEL CHILD AS PARENT BECUASE WE ARE NOW NOT USING TREE STRUCTURE AND DIPLAYING ITEMS IN MENU
	UPDATE 
		@TEMP
	SET 
		numParentID = 0
	WHERE
		numTabID = -3
		AND numPageNavID = (SELECT numPageNavID FROM @TEMP WHERE numTabID=-3 AND numParentID = 0)

	-- REMOVE ALL CHILD WHOSE PARENT IS HIDDEN
	DELETE FROM @TEMP WHERE numTabID = -3 AND ISNULL(numPageNavID,0) <> 0 AND numParentID NOT IN (SELECT numPageNavID FROM @TEMP WHERE numTabID = -3)

	-- REMOVE ALL CHILD WHOSE PARENT IS HIDDEN
	DELETE FROM @TEMP WHERE numParentID = 0 AND numTabID=-3

	-- UPDATE PARENT DETAIL
	UPDATE 
		t1
	SET 
		t1.numParentID = ISNULL((SELECT ID FROM @TEMP t2 WHERE t2.numTabID=-3 AND t2.numPageNavID=t1.numParentID),0)
	FROM
		@TEMP t1
	WHERE
		t1.numTabID = -3

	UPDATE 
		t1
	SET 
		t1.bitVisible = (SELECT bitVisible FROM @TEMP t2 WHERE t2.ID = t1.numParentID)
	FROM
		@TEMP t1
	WHERE
		ISNULL(t1.numParentID,0) > 0

	SELECT * FROM @TEMP WHERE bitVisible=1 ORDER BY numTabID, numParentID, [order]
END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getoppitemcodesfor850')
DROP PROCEDURE dbo.USP_GetOppItemCodesFor850
GO

/****** Author :  Priya Sharma
		Date : 26 June 2018
 ******/
CREATE PROCEDURE [dbo].[USP_GetOppItemCodesFor850]  

	@numOppId AS NUMERIC(18,0)
  AS

  BEGIN

	 SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId = @numOppId	

  END

GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetReportDashBoard')
DROP PROCEDURE USP_GetReportDashBoard
GO
CREATE PROCEDURE [dbo].[USP_GetReportDashBoard]     
	@numDomainID AS NUMERIC(18,0),           
	@numUserCntID AS NUMERIC(18,0),
	@numDashboardTemplateID AS NUMERIC(18,0) --IF VALUE IS -1 THEN FETCH DEFAULT TEMPLATE SET FOR THAT USER           
AS            
BEGIN            
	IF ISNULL(@numDashboardTemplateID,0) = -1
	BEGIN
		SET @numDashboardTemplateID = ISNULL((SELECT numDashboardTemplateID FROM UserMaster WHERE numDomainID=@numDomainID AND numUserDetailId=@numUserCntID),0)
	END

	IF NOT EXISTS(select 1 from ReportDashBoardSize WHERE numDomainID=@numDomainID AND numUserCntID=@numUserCntID)            
	BEGIN            
		INSERT INTO ReportDashBoardSize
		(
			numDomainID,numUserCntID,tintColumn,tintSize
		)            
		SELECT @numDomainID,@numUserCntID,1,1 
		UNION ALL
		SELECT @numDomainID,@numUserCntID,2,1 
		UNION ALL
		SELECT @numDomainID,@numUserCntID,3,1
	END     

	SELECT 
		tintColumn
		,tintSize 
	FROM 
		ReportDashBoardSize 
	WHERE 
		numDomainID=@numDomainID 
		AND numUserCntID=@numUserCntID 
	ORDER BY 
		tintColumn            

	SELECT 
		RD.numDashBoardID
		,RD.numReportID
		,RD.tintColumn
		,RD.tintRow
		,RD.tintReportType
		,RD.tintChartType
		,RD.tintReportCategory
		,RD.intHeight
		,RD.intWidth
		,ISNULL(RLM.bitDefault,0) bitDefault
		,ISNULL(RLM.intDefaultReportID,0) intDefaultReportID
	FROM 
		ReportDashboard RD 
	LEFT JOIN
		DashboardTemplate DT
	ON
		RD.numDashboardTemplateID=DT.numTemplateID
	LEFT JOIN
		ReportListMaster RLM
	ON 
		RD.numReportID=RLM.numReportID
	WHERE 
		RD.numDomainID=@numDomainID 
		AND RD.numUserCntID=@numUserCntID 
		AND ISNULL(RD.numDashboardTemplateID,0)=ISNULL(@numDashboardTemplateID,0)
	ORDER BY 
		tintColumn,tintRow     
		
	SELECT
		numTemplateID
		,vcTemplateName
	FROM
		DashboardTemplate
	WHERE
		numDomainID=@numDomainID
		AND numTemplateID=@numDashboardTemplateID       
END
GO
--Created By                                                          
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'usp_gettableinfodefault' ) 
    DROP PROCEDURE usp_gettableinfodefault
GO
CREATE PROCEDURE [dbo].[usp_GetTableInfoDefault]                                                                        
@numUserCntID numeric=0,                        
@numRecordID numeric=0,                                    
@numDomainId numeric=0,                                    
@charCoType char(1) = 'a',                        
@pageId as numeric,                        
@numRelCntType as NUMERIC,                        
@numFormID as NUMERIC,                        
@tintPageType TINYINT             
                             
                                    
--                                                           
                                               
AS                                      
  
                                                 
IF (
	SELECT 
		ISNULL(sum(TotalRow),0)  
	FROM
		(            
			Select count(*) TotalRow FROM View_DynamicColumns WHERE numUserCntId=@numuserCntid AND numDomainId = @numDomainId AND numFormID=@numFormID AND numRelCntType=@numRelCntType AND tintPageType=@tintPageType
			Union 
			Select count(*) TotalRow from View_DynamicCustomColumns WHERE numUserCntId=@numuserCntid AND numDomainId = @numDomainId AND numFormID=@numFormID AND numRelCntType=@numRelCntType AND tintPageType=@tintPageType
		) TotalRows
	) <> 0              
BEGIN                           
          
IF @pageid=1 or @pageid=4 or @pageid= 12 or  @pageid= 13 or  @pageid= 14
BEGIN
    SELECT 
		numFieldId,ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName,'' as vcURL,vcAssociatedControlType as fld_type
		,tintRow,tintcolumn as intcoulmn,'' as vcValue,bitCustom AS bitCustomField,vcPropertyName,ISNULL(bitAllowEdit,0) AS bitCanBeUpdated
		,numListID,0 numListItemID,PopupFunctionName
		,CASE 
			WHEN vcAssociatedControlType='SelectBox' 
			THEN ISNULL((SELECT numPrimaryListID FROM FieldRelationship WHERE numSecondaryListID=numListID AND numDomainID=@numDomainId),0) 
			ELSE 0 
		END AS ListRelID
		,CASE 
			WHEN vcAssociatedControlType='SelectBox' 
			THEN (SELECT COUNT(*) FROM FieldRelationship WHERE numPrimaryListID=numListID AND numDomainID=@numDomainId) 
			ELSE 0 
		END AS DependentFields,
		bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
		bitFieldMessage,intMaxLength,intMinLength,bitFieldMessage,ISNULL(vcFieldMessage,'') vcFieldMessage
		,(SELECT TOP 1 FldDTLID from CFW_FLD_Values where  RecId=@numRecordID and Fld_Id=numFieldId) as FldDTLID
		,'' as Value,vcDbColumnName,vcToolTip,intFieldMaxLength,0 AS numFormFieldGroupId,'' AS vcGroupName,0 AS numOrder
	FROM 
		View_DynamicColumns
	WHERE 
		numFormId=@numFormID 
		AND numUserCntID=@numUserCntID 
		AND numDomainID=@numDomainID  
		AND ISNULL(bitCustom,0)=0 
		AND numRelCntType=@numRelCntType 
		AND tintPageType=@tintPageType 
		AND 1=(CASE WHEN @tintPageType=2 THEN CASE WHEN ISNULL(bitAddField,0)=1 THEN 1 ELSE 0 end 
				WHEN @tintPageType=3 THEN CASE WHEN ISNULL(bitDetailField,0)=1 THEN 1 ELSE 0 END 
				ELSE 0 END) 
	UNION 
	SELECT 
		numFieldId,vcfieldName ,vcURL,vcAssociatedControlType as fld_type,tintRow,tintcolumn as intcoulmn,
		CASE 
			WHEN vcAssociatedControlType = 'SelectBox' 
			THEN (SELECT vcData FROM listdetails WHERE numlistitemid = dbo.GetCustFldValue(numFieldID,@PageId,@numRecordID))
			WHEN vcAssociatedControlType = 'CheckBoxList' 
			THEN STUFF((SELECT CONCAT(',', vcData) FROM ListDetails WHERE numlistitemid IN (SELECT ISNULL(Id,0) FROM dbo.SplitIDs(dbo.GetCustFldValue(numFieldID,@PageId,@numRecordID),',')) FOR XML PATH('')), 1, 1, '')
			ELSE
				dbo.GetCustFldValue(numFieldID,@PageId,@numRecordID) 
		END AS vcValue,                    
		CONVERT(BIT,1) bitCustomField,'' vcPropertyName, CONVERT(BIT,1) AS bitCanBeUpdated,numListID,
		CASE 
			WHEN vcAssociatedControlType = 'SelectBox' 
			THEN dbo.GetCustFldValue(numFieldID,@PageId,@numRecordID) 
			ELSE 0 
		END numListItemID,'' PopupFunctionName,
		CASE 
			WHEN vcAssociatedControlType='SelectBox' 
			THEN ISNULL((SELECT numPrimaryListID FROM FieldRelationship WHERE numSecondaryListID=numListID AND numDomainID=@numDomainId),0) 
			ELSE 0 
		END AS ListRelID ,
		CASE 
			WHEN vcAssociatedControlType='SelectBox' 
			THEN (SELECT COUNT(*) FROM FieldRelationship where numPrimaryListID=numListID AND numDomainID=@numDomainId) 
			ELSE 0 
		END AS DependentFields,
		bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,bitFieldMessage,intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage
		,isnull((SELECT TOP 1 FldDTLID from CFW_FLD_Values where  RecId=@numRecordID and Fld_Id=numFieldID),0) as FldDTLID
		,dbo.GetCustFldValue(numFieldID,@PageId,@numRecordID) as Value,'' AS vcDbColumnName,vcToolTip,0 intFieldMaxLength,0 AS numFormFieldGroupId,'' AS vcGroupName,0 AS numOrder
	FROM 
		View_DynamicCustomColumns_RelationShip
	WHERE 
		grp_id=@PageId 
		AND numRelation=@numRelCntType 
		AND numDomainID=@numDomainID 
		AND subgrp=0
		AND numUserCntID=@numUserCntID 
		AND numFormId=@numFormID 
		AND numRelCntType=@numRelCntType 
		AND tintPageType=@tintPageType
	ORDER BY 
		tintRow,intcoulmn   
END                
                
if @PageId= 2 or  @PageId= 3 or @PageId= 5 or @PageId= 6 or @PageId= 7 or @PageId= 8   or @PageId= 11                          
 begin            
		IF @numFormID = 123 -- Add/Edit Order - Item Grid Column Settings
		BEGIN
			SELECT numFieldId,ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName
				,'' as vcURL,vcAssociatedControlType as fld_type,tintRow,tintcolumn as intcoulmn,          
					'' as vcValue,ISNULL(bitCustom,0) AS bitCustomField,vcPropertyName,ISNULL(bitAllowEdit,0) AS bitCanBeUpdated,
					numListID,0 numListItemID,PopupFunctionName,
		Case when vcAssociatedControlType='SelectBox' then isnull((Select numPrimaryListID from FieldRelationship where numSecondaryListID=numListID and numDomainID=@numDomainId  ),0) else 0 end as ListRelID, 
		Case when vcAssociatedControlType='SelectBox' then (Select count(*) from FieldRelationship where numPrimaryListID=numListID and numDomainID=@numDomainId  ) else 0 end as DependentFields,
		bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
		bitFieldMessage,intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,vcDbColumnName,vcToolTip,intFieldMaxLength,0 AS numFormFieldGroupId,'' AS vcGroupName,0 AS numOrder
		FROM View_DynamicColumns
			where numFormId=@numFormID and numUserCntID=@numUserCntID and  numDomainID=@numDomainID  
			and ISNULL(bitCustom,0)=0 and numRelCntType=@numRelCntType AND tintPageType=@tintPageType 

			union          
           
			select numFieldId,vcfieldName ,vcURL,vcAssociatedControlType as fld_type,               
			tintRow,tintcolumn as intcoulmn,
			case when vcAssociatedControlType = 'SelectBox' then (select vcData from listdetails where numlistitemid=dbo.GetCustFldValue(numFieldID,@PageId,@numRecordID))
			when vcAssociatedControlType = 'CheckBoxList' then STUFF((SELECT CONCAT(',', vcData) FROM ListDetails WHERE numlistitemid IN (SELECT ISNULL(Id,0) FROM dbo.SplitIDs(dbo.GetCustFldValue(numFieldID,@PageId,@numRecordID),',')) FOR XML PATH('')), 1, 1, '')
			else dbo.GetCustFldValue(numFieldId,@PageId,@numRecordID) end as vcValue,                    
			convert(bit,1) bitCustomField,'' vcPropertyName,  
			convert(bit,1) as bitCanBeUpdated,numListID,case when vcAssociatedControlType = 'SelectBox' THEN dbo.GetCustFldValue(numFieldId,@PageId,@numRecordID) ELSE 0 END numListItemID,'' PopupFunctionName,
		Case when vcAssociatedControlType='SelectBox' then isnull((Select numPrimaryListID from FieldRelationship where numSecondaryListID=numListID and numDomainID=@numDomainId  ),0) else 0 end as ListRelID ,
		Case when vcAssociatedControlType='SelectBox' then (Select count(*) from FieldRelationship where numPrimaryListID=numListID and numDomainID=@numDomainId  ) else 0 end as DependentFields,
		bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,bitFieldMessage,intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,'' AS vcDbColumnName
		,vcToolTip,0 intFieldMaxLength,0 AS numFormFieldGroupId,'' AS vcGroupName,0 AS numOrder
		from View_DynamicCustomColumns
			where grp_id=@PageId and numDomainID=@numDomainID and subgrp=0
			and numUserCntID=@numUserCntID  
			AND numFormId=@numFormID and ISNULL(bitCustom,0)=1 and numRelCntType=@numRelCntType AND tintPageType=@tintPageType
    
			order by tintrow,intcoulmn      
		END
		ELSE
		BEGIN    
				SELECT numFieldId,ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName
				,'' as vcURL,vcAssociatedControlType as fld_type,tintRow,tintcolumn as intcoulmn,          
					'' as vcValue,ISNULL(bitCustom,0) AS bitCustomField,vcPropertyName,
					--DO NOT ALLOW CHANGE OF ASSIGNED TO FIELD AFTER COMMISSION IS PAID
					CAST((CASE 
					WHEN @charCoType='O' AND numFieldID=100
					THEN 
						CASE 
						WHEN (SELECT COUNT(*) FROM BizDocComission WHERE numOppID=@numRecordID AND ISNULL(bitCommisionPaid,0)=1) > 0 
						THEN 
							0 
						ELSE 
							ISNULL(bitAllowEdit,0) 
						END 
					ELSE 
						ISNULL(bitAllowEdit,0) 
					END) AS BIT) AS bitCanBeUpdated,
					numListID,0 numListItemID,PopupFunctionName,
		Case when vcAssociatedControlType='SelectBox' then isnull((Select numPrimaryListID from FieldRelationship where numSecondaryListID=numListID and numDomainID=@numDomainId  ),0) else 0 end as ListRelID, 
		Case when vcAssociatedControlType='SelectBox' then (Select count(*) from FieldRelationship where numPrimaryListID=numListID and numDomainID=@numDomainId  ) else 0 end as DependentFields,
		bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
		bitFieldMessage,intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,vcDbColumnName,vcToolTip,intFieldMaxLength,0 AS numFormFieldGroupId,'' AS vcGroupName,0 AS numOrder
		FROM View_DynamicColumns
			where numFormId=@numFormID and numUserCntID=@numUserCntID and  numDomainID=@numDomainID  
			and ISNULL(bitCustom,0)=0 and numRelCntType=@numRelCntType AND tintPageType=@tintPageType 
			AND  1=(CASE WHEN @tintPageType=2 THEN CASE WHEN ISNULL(bitAddField,0)=1 THEN 1 ELSE 0 end 
					WHEN @tintPageType=3 THEN CASE WHEN ISNULL(bitDetailField,0)=1 THEN 1 ELSE 0 END 
					ELSE 0 END) 

			union          
           
			select numFieldId,vcfieldName ,vcURL,vcAssociatedControlType as fld_type,               
			tintRow,tintcolumn as intcoulmn,
			case WHEN vcAssociatedControlType = 'SelectBox' then (select vcData from listdetails where numlistitemid=dbo.GetCustFldValue(numFieldID,@PageId,@numRecordID))
			WHEN vcAssociatedControlType = 'CheckBoxList' then STUFF((SELECT CONCAT(',', vcData) FROM ListDetails WHERE numlistitemid IN (SELECT ISNULL(Id,0) FROM dbo.SplitIDs(dbo.GetCustFldValue(numFieldID,@PageId,@numRecordID),',')) FOR XML PATH('')), 1, 1, '')
			ELSE dbo.GetCustFldValue(numFieldId,@PageId,@numRecordID) end as vcValue,                    
			convert(bit,1) bitCustomField,'' vcPropertyName,  
			convert(bit,1) as bitCanBeUpdated,numListID,case when vcAssociatedControlType = 'SelectBox' THEN dbo.GetCustFldValue(numFieldId,@PageId,@numRecordID) ELSE 0 END numListItemID,'' PopupFunctionName,
		Case when vcAssociatedControlType='SelectBox' then isnull((Select numPrimaryListID from FieldRelationship where numSecondaryListID=numListID and numDomainID=@numDomainId  ),0) else 0 end as ListRelID ,
		Case when vcAssociatedControlType='SelectBox' then (Select count(*) from FieldRelationship where numPrimaryListID=numListID and numDomainID=@numDomainId  ) else 0 end as DependentFields,
		bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,bitFieldMessage,intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,'' AS vcDbColumnName
		,vcToolTip,0 intFieldMaxLength,0 AS numFormFieldGroupId,'' AS vcGroupName,0 AS numOrder
		from View_DynamicCustomColumns
			where grp_id=@PageId and numDomainID=@numDomainID and subgrp=0
			and numUserCntID=@numUserCntID  
			AND numFormId=@numFormID and ISNULL(bitCustom,0)=1 and numRelCntType=@numRelCntType AND tintPageType=@tintPageType
    
			order by tintrow,intcoulmn      
	END                          
 end  

	IF @PageId = 153
	BEGIN
		SELECT 
			numFieldId
			,ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName
			,'' as vcURL
			,vcAssociatedControlType as fld_type
			,tintRow
			,tintcolumn as intcoulmn
			,'' as vcValue
			,ISNULL(bitCustom,0) AS bitCustomField
			,vcPropertyName
			,ISNULL(bitAllowEdit,0) AS bitCanBeUpdated
			,numListID
			,0 numListItemID
			,PopupFunctionName
			,Case when vcAssociatedControlType='SelectBox' then isnull((Select numPrimaryListID from FieldRelationship where numSecondaryListID=numListID and numDomainID=@numDomainId  ),0) else 0 end as ListRelID
			,Case when vcAssociatedControlType='SelectBox' then (Select count(*) from FieldRelationship where numPrimaryListID=numListID and numDomainID=@numDomainId  ) else 0 end as DependentFields,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			bitFieldMessage,intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,vcDbColumnName,vcToolTip,intFieldMaxLength,0 AS numFormFieldGroupId,'' AS vcGroupName
		FROM 
			View_DynamicColumns
		WHERE 
			numFormId=@numFormID 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID  
			AND ISNULL(bitCustom,0)=0 
			AND numRelCntType=@numRelCntType 
			AND tintPageType=@tintPageType 
		ORDER BY 
			tintrow,intcoulmn   
	END 
               
if @PageId= 0            
  begin         
  SELECT HDR.numFieldId,HDR.vcFieldName,'' as vcURL,'' as fld_type,DTL.tintRow,DTL.intcoulmn,          
  HDR.vcDBColumnName,convert(char(1),DTL.bitCustomField)as bitCustomField,
  0 as bitIsRequired,0 bitIsEmail,0 bitIsAlphaNumeric,0 bitIsNumeric, 0 bitIsLengthValidation,0 bitFieldMessage,0 intMaxLength,0 intMinLength, 0 bitFieldMessage,'' vcFieldMessage
   from PageLayoutdtl DTL                                    
  join pagelayout HDR on DTl.numFieldId= HDR.numFieldId                                    
   where HDR.Ctype = @charCoType and numUserCntId=@numUserCntId  and bitCustomField=0 and            
  numDomainId = @numDomainId and DTL.numRelCntType=@numRelCntType   order by DTL.tintrow,intcoulmn          
  end                
end                 



ELSE IF @tintPageType=2 and @numRelCntType>3 and 
((select isnull(sum(TotalRow),0) from (Select count(*) TotalRow from View_DynamicColumns where numFormId=@numFormID and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=@tintPageType AND numFormID IN(36) AND numRelCntType=2 
Union 
Select count(*) TotalRow from View_DynamicCustomColumns where numFormId=@numFormID and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=@tintPageType AND numFormID IN(36) AND numRelCntType=2) TotalRows
) <> 0 )
BEGIN

	exec usp_GetTableInfoDefault @numUserCntID=@numUserCntID,@numRecordID=@numRecordID,@numDomainId=@numDomainId,@charCoType=@charCoType,@pageId=@pageId,
			@numRelCntType=2,@numFormID=@numFormID,@tintPageType=@tintPageType
END
                           
ELSE IF @charCoType<>'b'/*added by kamal to prevent showing of all  fields on checkout by default*/                  
BEGIN                 
	DECLARE @IsMasterConfAvailable BIT = 0
	DECLARE @numUserGroup NUMERIC(18,0)                                    

	SELECT @numUserGroup = numGroupID FROM UserMaster WHERE numUserDetailId = @numUserCntID   
       
	IF @pageid=1 or @pageid=4 or @pageid= 12 or  @pageid= 13 or  @pageid= 14                
	BEGIN   
		--Check if Master Form Configuration is created by administrator if NoColumns=0

		IF (SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = @numDomainID AND numFormID = @numFormID AND numRelCntType=@numRelCntType AND bitGridConfiguration = 0 AND numGroupID = @numUserGroup) > 0
	BEGIN
		SET @IsMasterConfAvailable = 1
	END

		--If MasterConfiguration is available then load it otherwise load default columns
		IF @IsMasterConfAvailable = 1
		BEGIN
			SELECT 
				numFieldId,ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName
				,'' as vcURL,vcAssociatedControlType as fld_type,tintRow,tintcolumn as intcoulmn,          
				 '' as vcValue,convert(bit,0) AS bitCustomField,vcPropertyName,ISNULL(bitAllowEdit,0) AS bitCanBeUpdated,
				 0 Tabletype,numListID,0 numListItemID,PopupFunctionName,
				Case when vcAssociatedControlType='SelectBox' then isnull((Select numPrimaryListID from FieldRelationship where numSecondaryListID=numListID and numDomainID=@numDomainId  ),0) else 0 end as ListRelID, 
				Case when vcAssociatedControlType='SelectBox' then (Select count(*) from FieldRelationship where numPrimaryListID=numListID and numDomainID=@numDomainId  ) else 0 end as DependentFields,
				bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
				bitFieldMessage,intMaxLength,intMinLength,bitFieldMessage,ISNULL(vcFieldMessage,'') vcFieldMessage
				,vcDbColumnName,vcToolTip,intFieldMaxLength,ISNULL(numFormFieldGroupId,0) AS numFormFieldGroupId,ISNULL(vcGroupName,'') AS vcGroupName,ISNULL(numOrder,0) AS numOrder
			FROM 
				View_DynamicColumnsMasterConfig
			WHERE
				View_DynamicColumnsMasterConfig.numFormId=@numFormID AND 
				View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
				View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
				View_DynamicColumnsMasterConfig.numRelCntType=@numRelCntType AND 
				View_DynamicColumnsMasterConfig.bitGridConfiguration = 0 AND
				ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
			UNION
			SELECT 
				numFieldId as numFieldId,vcFieldName as vcfieldName ,vcURL,vcAssociatedControlType,               
				tintrow,tintColumn,  
				case when vcAssociatedControlType = 'SelectBox' then (select vcData from listdetails where numlistitemid=dbo.GetCustFldValue(numFieldId,@PageId,@numRecordID))
				WHEN vcAssociatedControlType = 'CheckBoxList' then STUFF((SELECT CONCAT(',', vcData) FROM ListDetails WHERE numlistitemid IN (SELECT ISNULL(Id,0) FROM dbo.SplitIDs(dbo.GetCustFldValue(numFieldID,@PageId,@numRecordID),',')) FOR XML PATH('')), 1, 1, '')
				 else dbo.GetCustFldValue(numFieldId,@PageId,@numRecordID) end as vcValue,                    
				 convert(bit,1) bitCustomField,'' vcPropertyName,  
				 convert(bit,1) as bitCanBeUpdated,1 Tabletype,numListID,case when vcAssociatedControlType = 'SelectBox' THEN dbo.GetCustFldValue(numFieldId,@PageId,@numRecordID) ELSE 0 END numListItemID,'' PopupFunctionName,
				Case when vcAssociatedControlType='SelectBox' then isnull((Select numPrimaryListID from FieldRelationship where numSecondaryListID=numListID and numDomainID=@numDomainId  ),0) else 0 end as ListRelID ,
				Case when vcAssociatedControlType='SelectBox' then (Select count(*) from FieldRelationship where numPrimaryListID=numListID and numDomainID=@numDomainId  ) else 0 end as DependentFields,
				ISNULL(bitIsRequired,0) bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,bitFieldMessage,intMaxLength,intMinLength,bitFieldMessage,ISNULL(vcFieldMessage,'') vcFieldMessage
				,'' AS vcDbColumnName,vcToolTip,0 intFieldMaxLength,ISNULL(numFormFieldGroupId,0) AS numFormFieldGroupId,ISNULL(vcGroupName,'') AS vcGroupName,ISNULL(numOrder,0) AS numOrder
			FROM 
				View_DynamicCustomColumnsMasterConfig
			WHERE 
				View_DynamicCustomColumnsMasterConfig.numFormId=@numFormID AND 
				View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
				View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
				View_DynamicCustomColumnsMasterConfig.numRelCntType=@numRelCntType AND 
				View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 0 AND
				ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1
			ORDER BY 
				tintrow,intcoulmn  
		END
		ELSE                                        
		BEGIN
			SELECT numFieldId,ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName
        ,'' as vcURL,vcAssociatedControlType as fld_type,tintRow,tintcolumn as intcoulmn,          
         '' as vcValue,convert(bit,0) AS bitCustomField,vcPropertyName,ISNULL(bitAllowEdit,0) AS bitCanBeUpdated,
         0 Tabletype,numListID,0 numListItemID,PopupFunctionName,
Case when vcAssociatedControlType='SelectBox' then isnull((Select numPrimaryListID from FieldRelationship where numSecondaryListID=numListID and numDomainID=@numDomainId  ),0) else 0 end as ListRelID, 
Case when vcAssociatedControlType='SelectBox' then (Select count(*) from FieldRelationship where numPrimaryListID=numListID and numDomainID=@numDomainId  ) else 0 end as DependentFields,
bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
bitFieldMessage,intMaxLength,intMinLength,bitFieldMessage,ISNULL(vcFieldMessage,'') vcFieldMessage
,vcDbColumnName,vcToolTip,intFieldMaxLength,0 AS numFormFieldGroupId,'' AS vcGroupName,0 AS numOrder
FROM View_DynamicDefaultColumns 
  where numFormId=@numFormID AND numDomainID=@numDomainID AND
   1=(CASE WHEN @tintPageType=2 THEN CASE WHEN ISNULL(bitAddField,0)=1 THEN 1 ELSE 0 end 
		  WHEN @tintPageType=3 THEN CASE WHEN ISNULL(bitDetailField,0)=1 THEN 1 ELSE 0 END 
		  ELSE 0 END) 
                   
   union     
  
     select fld_id as numFieldId,fld_label as vcfieldName ,vcURL,fld_type,               
   convert(tinyint,0) as tintrow,convert(int,0) as intcoulmn,  
 case when fld_type = 'SelectBox' then (select vcData from listdetails where numlistitemid=dbo.GetCustFldValue(fld_id,@PageId,@numRecordID))                
 WHEN fld_type = 'CheckBoxList' then STUFF((SELECT CONCAT(',', vcData) FROM ListDetails WHERE numlistitemid IN (SELECT ISNULL(Id,0) FROM dbo.SplitIDs(dbo.GetCustFldValue(fld_id,@PageId,@numRecordID),',')) FOR XML PATH('')), 1, 1, '')
 else dbo.GetCustFldValue(fld_id,@PageId,@numRecordID) end as vcValue,                    
 convert(bit,1) bitCustomField,'' vcPropertyName,  
 convert(bit,1) as bitCanBeUpdated,1 Tabletype,CFM.numListID,case when fld_type = 'SelectBox' THEN dbo.GetCustFldValue(fld_id,@PageId,@numRecordID) ELSE 0 END numListItemID,'' PopupFunctionName,
Case when fld_type='SelectBox' then isnull((Select numPrimaryListID from FieldRelationship where numSecondaryListID=CFM.numListID and numDomainID=@numDomainId  ),0) else 0 end as ListRelID ,
Case when fld_type='SelectBox' then (Select count(*) from FieldRelationship where numPrimaryListID=CFM.numListID and numDomainID=@numDomainId  ) else 0 end as DependentFields,
ISNULL(V.bitIsRequired,0) bitIsRequired,V.bitIsEmail,V.bitIsAlphaNumeric,V.bitIsNumeric,V.bitIsLengthValidation,V.bitFieldMessage,V.intMaxLength,V.intMinLength,V.bitFieldMessage,ISNULL(V.vcFieldMessage,'') vcFieldMessage
,'' AS vcDbColumnName,CFM.vcToolTip,0 intFieldMaxLength,0 AS numFormFieldGroupId,'' AS vcGroupName,0 AS numOrder
from CFW_Fld_Master CFM join CFW_Fld_Dtl CFD on Fld_id=numFieldId                                          
 left join CFw_Grp_Master CFG on subgrp=CFG.Grp_id                                          
 LEFT JOIN CFW_Validation V ON V.numFieldID = CFM.Fld_id
 where CFM.grp_id=@PageId and CFD.numRelation=@numRelCntType and CFM.numDomainID=@numDomainID and subgrp=0

 order by tintrow,intcoulmn                        
		END     
	END      
             
	IF @PageId= 2 or  @PageId= 3 or @PageId= 5 or @PageId= 6 or @PageId= 7 or @PageId= 8  or @PageId= 11                          
	BEGIN  
		--Check if Master Form Configuration is created by administrator if NoColumns=0

		IF (SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = @numDomainID AND numFormID = @numFormID AND numRelCntType=@numRelCntType AND bitGridConfiguration = 0 AND numGroupID = @numUserGroup) > 0
		BEGIN
			SET @IsMasterConfAvailable = 1
		END

		--If MasterConfiguration is available then load it otherwise load default columns
		IF @IsMasterConfAvailable = 1
		BEGIN
			SELECT 
				numFieldId,ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName
				,'' as vcURL,vcAssociatedControlType as fld_type,tintRow,tintcolumn as intcoulmn,          
				 '' as vcValue,convert(bit,0) AS bitCustomField,vcPropertyName,ISNULL(bitAllowEdit,0) AS bitCanBeUpdated,
				 0 Tabletype,numListID,0 numListItemID,PopupFunctionName,
				Case when vcAssociatedControlType='SelectBox' then isnull((Select numPrimaryListID from FieldRelationship where numSecondaryListID=numListID and numDomainID=@numDomainId  ),0) else 0 end as ListRelID, 
				Case when vcAssociatedControlType='SelectBox' then (Select count(*) from FieldRelationship where numPrimaryListID=numListID and numDomainID=@numDomainId  ) else 0 end as DependentFields,
				bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
				bitFieldMessage,intMaxLength,intMinLength,bitFieldMessage,ISNULL(vcFieldMessage,'') vcFieldMessage
				,vcDbColumnName,vcToolTip,intFieldMaxLength,ISNULL(numFormFieldGroupId,0) AS numFormFieldGroupId,ISNULL(vcGroupName,'') AS vcGroupName,ISNULL(numOrder,0) AS numOrder
			FROM 
				View_DynamicColumnsMasterConfig
			WHERE
				View_DynamicColumnsMasterConfig.numFormId=@numFormID AND 
				View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
				View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
				View_DynamicColumnsMasterConfig.numRelCntType=@numRelCntType AND 
				View_DynamicColumnsMasterConfig.bitGridConfiguration = 0 AND
				ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
			UNION
			SELECT 
				numFieldId as numFieldId,vcFieldName as vcfieldName ,vcURL,vcAssociatedControlType,               
				tintrow,tintColumn,  
				case when vcAssociatedControlType = 'SelectBox' then (select vcData from listdetails where numlistitemid=dbo.GetCustFldValue(numFieldId,@PageId,@numRecordID))
				WHEN vcAssociatedControlType = 'CheckBoxList' then STUFF((SELECT CONCAT(',', vcData) FROM ListDetails WHERE numlistitemid IN (SELECT ISNULL(Id,0) FROM dbo.SplitIDs(dbo.GetCustFldValue(numFieldId,@PageId,@numRecordID),',')) FOR XML PATH('')), 1, 1, '')
				 else dbo.GetCustFldValue(numFieldId,@PageId,@numRecordID) end as vcValue,                    
				 convert(bit,1) bitCustomField,'' vcPropertyName,  
				 convert(bit,1) as bitCanBeUpdated,1 Tabletype,numListID,case when vcAssociatedControlType = 'SelectBox' THEN dbo.GetCustFldValue(numFieldId,@PageId,@numRecordID) ELSE 0 END numListItemID,'' PopupFunctionName,
				Case when vcAssociatedControlType='SelectBox' then isnull((Select numPrimaryListID from FieldRelationship where numSecondaryListID=numListID and numDomainID=@numDomainId  ),0) else 0 end as ListRelID ,
				Case when vcAssociatedControlType='SelectBox' then (Select count(*) from FieldRelationship where numPrimaryListID=numListID and numDomainID=@numDomainId  ) else 0 end as DependentFields,
				ISNULL(bitIsRequired,0) bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,bitFieldMessage,intMaxLength,intMinLength,bitFieldMessage,ISNULL(vcFieldMessage,'') vcFieldMessage
				,'' AS vcDbColumnName,vcToolTip,0 intFieldMaxLength,ISNULL(numFormFieldGroupId,0) AS numFormFieldGroupId,ISNULL(vcGroupName,'') AS vcGroupName,ISNULL(numOrder,0) AS numOrder
			FROM 
				View_DynamicCustomColumnsMasterConfig
			WHERE 
				View_DynamicCustomColumnsMasterConfig.numFormId=@numFormID AND 
				View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
				View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
				View_DynamicCustomColumnsMasterConfig.numRelCntType=@numRelCntType AND 
				View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 0 AND
				ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1
			ORDER BY 
				tintrow,intcoulmn  
		END
		ELSE                                        
		BEGIN
			SELECT numFieldId,ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName
			,'' as vcURL,vcAssociatedControlType as fld_type,tintRow,tintColumn as intcoulmn,          
			 '' as vcValue,convert(bit,0) AS bitCustomField,vcPropertyName,ISNULL(bitAllowEdit,0) AS bitCanBeUpdated,
			 0 Tabletype,numListID,0 numListItemID,PopupFunctionName,
			Case when vcAssociatedControlType='SelectBox' then isnull((Select numPrimaryListID from FieldRelationship where numSecondaryListID=numListID and numDomainID=@numDomainId  ),0) else 0 end as ListRelID, 
			Case when vcAssociatedControlType='SelectBox' then (Select count(*) from FieldRelationship where numPrimaryListID=numListID and numDomainID=@numDomainId  ) else 0 end as DependentFields,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			bitFieldMessage,intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage
			,vcDbColumnName,vcToolTip,intFieldMaxLength,0 AS numFormFieldGroupId,'' AS vcGroupName,0 AS numOrder
			FROM View_DynamicDefaultColumns DFV
			where numFormId=@numFormID AND numDomainID=@numDomainID AND
			1=(CASE WHEN @tintPageType=2 THEN CASE WHEN ISNULL(bitAddField,0)=1 THEN 1 ELSE 0 end 
			  WHEN @tintPageType=3 THEN CASE WHEN ISNULL(bitDetailField,0)=1 THEN 1 ELSE 0 END 
			  ELSE 0 END)   
			UNION 
			select fld_id as numFieldId,fld_label as vcfieldName ,vcURL,fld_type,               
			convert(tinyint,0) as tintRow,convert(int,0) as intcoulmn,
			case when fld_type = 'SelectBox' then (select vcData from listdetails where numlistitemid=dbo.GetCustFldValue(fld_id,@PageId,@numRecordID))
			WHEN fld_type = 'CheckBoxList' then STUFF((SELECT CONCAT(',', vcData) FROM ListDetails WHERE numlistitemid IN (SELECT ISNULL(Id,0) FROM dbo.SplitIDs(dbo.GetCustFldValue(fld_id,@PageId,@numRecordID),',')) FOR XML PATH('')), 1, 1, '')
			else dbo.GetCustFldValue(fld_id,@PageId,@numRecordID) end as vcValue,                    
			convert(bit,1) bitCustomField,'' vcPropertyName,  
			convert(bit,1) as bitCanBeUpdated,1 Tabletype,CFM.numListID,case when fld_type = 'SelectBox' THEN dbo.GetCustFldValue(fld_id,@PageId,@numRecordID) ELSE 0 END numListItemID,'' PopupFunctionName,
			Case when fld_type='SelectBox' then isnull((Select numPrimaryListID from FieldRelationship where numSecondaryListID=CFM.numListID and numDomainID=@numDomainId  ),0) else 0 end as ListRelID ,
			Case when fld_type='SelectBox' then (Select count(*) from FieldRelationship where numPrimaryListID=CFM.numListID and numDomainID=@numDomainId  ) else 0 end as DependentFields,
			ISNULL(V.bitIsRequired,0) bitIsRequired,V.bitIsEmail,V.bitIsAlphaNumeric,V.bitIsNumeric,V.bitIsLengthValidation,V.bitFieldMessage,V.intMaxLength,V.intMinLength,V.bitFieldMessage,ISNULL(V.vcFieldMessage,'') vcFieldMessage,'' AS vcDbColumnName
			,CFM.vcToolTip,0 intFieldMaxLength,0 AS numFormFieldGroupId,'' AS vcGroupName,0 AS numOrder
			FROM CFW_Fld_Master CFM left join CFW_Fld_Dtl CFD on Fld_id=numFieldId  
			left join CFw_Grp_Master CFG on subgrp=CFG.Grp_id                                    
			LEFT JOIN CFW_Validation V ON V.numFieldID = CFM.Fld_id
			WHERE CFM.grp_id=@PageId and CFM.numDomainID=@numDomainID   and subgrp=0           
			ORDER BY tintrow,intcoulmn                             
		END
	END   
	      
	IF @PageId= 153                      
	BEGIN  
		--Check if Master Form Configuration is created by administrator if NoColumns=0
		IF (SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = @numDomainID AND numFormID = @numFormID AND numRelCntType=@numRelCntType AND bitGridConfiguration = 0 AND numGroupID = @numUserGroup) > 0
		BEGIN
			SET @IsMasterConfAvailable = 1
		END

		--If MasterConfiguration is available then load it otherwise load default columns
		IF @IsMasterConfAvailable = 1
		BEGIN
			SELECT 
				numFieldId,ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName
				,'' as vcURL,vcAssociatedControlType as fld_type,tintRow,tintcolumn as intcoulmn,          
				 '' as vcValue,convert(bit,0) AS bitCustomField,vcPropertyName,ISNULL(bitAllowEdit,0) AS bitCanBeUpdated,
				 0 Tabletype,numListID,0 numListItemID,PopupFunctionName,
				Case when vcAssociatedControlType='SelectBox' then isnull((Select numPrimaryListID from FieldRelationship where numSecondaryListID=numListID and numDomainID=@numDomainId  ),0) else 0 end as ListRelID, 
				Case when vcAssociatedControlType='SelectBox' then (Select count(*) from FieldRelationship where numPrimaryListID=numListID and numDomainID=@numDomainId  ) else 0 end as DependentFields,
				bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
				bitFieldMessage,intMaxLength,intMinLength,bitFieldMessage,ISNULL(vcFieldMessage,'') vcFieldMessage
				,vcDbColumnName,vcToolTip,intFieldMaxLength,ISNULL(numFormFieldGroupId,0) AS numFormFieldGroupId,ISNULL(vcGroupName,'') AS vcGroupName
			FROM 
				View_DynamicColumnsMasterConfig
			WHERE
				View_DynamicColumnsMasterConfig.numFormId=@numFormID AND 
				View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
				View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
				View_DynamicColumnsMasterConfig.numRelCntType=@numRelCntType AND 
				View_DynamicColumnsMasterConfig.bitGridConfiguration = 0 AND
				ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0 
		END
		ELSE                                        
		BEGIN
			SELECT numFieldId,ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName
			,'' as vcURL,vcAssociatedControlType as fld_type,tintRow,tintColumn as intcoulmn,          
			 '' as vcValue,convert(bit,0) AS bitCustomField,vcPropertyName,ISNULL(bitAllowEdit,0) AS bitCanBeUpdated,
			 0 Tabletype,numListID,0 numListItemID,PopupFunctionName,
			Case when vcAssociatedControlType='SelectBox' then isnull((Select numPrimaryListID from FieldRelationship where numSecondaryListID=numListID and numDomainID=@numDomainId  ),0) else 0 end as ListRelID, 
			Case when vcAssociatedControlType='SelectBox' then (Select count(*) from FieldRelationship where numPrimaryListID=numListID and numDomainID=@numDomainId  ) else 0 end as DependentFields,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			bitFieldMessage,intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage
			,vcDbColumnName,vcToolTip,intFieldMaxLength,0 AS numFormFieldGroupId,'' AS vcGroupName
			FROM View_DynamicDefaultColumns DFV
			where numFormId=@numFormID AND numDomainID=@numDomainID AND bitDefault=1 AND
			1=(CASE WHEN @tintPageType=2 THEN CASE WHEN ISNULL(bitAddField,0)=1 THEN 1 ELSE 0 end 
			  WHEN @tintPageType=3 THEN CASE WHEN ISNULL(bitDetailField,0)=1 THEN 1 ELSE 0 END 
			  ELSE 0 END)             
			ORDER BY tintrow,intcoulmn                             
		END
	END  

	IF @PageId= 0            
	BEGIN         
	   SELECT numFieldID,vcFieldName,'' as vcURL,'' as fld_type,vcDBColumnName,tintRow,intcolumn as intcoulmn,          
		'0' as bitCustomField,Ctype,'0' as tabletype,
		0 as bitIsRequired,0 bitIsEmail,0 bitIsAlphaNumeric,0 bitIsNumeric, 0 bitIsLengthValidation,0 bitFieldMessage,0 intMaxLength,0 intMinLength, 0 bitFieldMessage,'' vcFieldMessage
		from PageLayout where Ctype = @charCoType    order by tintrow,intcoulmn           
	END                       
END  
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj              
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getuserswithdomains')
DROP PROCEDURE usp_getuserswithdomains
GO
CREATE PROCEDURE [dbo].[usp_GetUsersWithDomains]                            
 @numUserID NUMERIC(18,0)=0,
 @numDomainID NUMERIC(18,0)                       
AS                            
BEGIN                            
	IF @numUserID = 0 AND @numDomainID > 0 --Check if request is made while session is on
	BEGIN                            
		SELECT 
			numUserID
			,vcUserName
			,UserMaster.numGroupID
			,ISNULL(vcGroupName,'-') AS vcGroupName
			,vcUserDesc
			,ISNULL(ADC.vcfirstname,'') +' '+ ISNULL(ADC.vclastname,'') as Name
			,UserMaster.numDomainID
			,vcDomainName
			,UserMaster.numUserDetailId
			,ISNULL(UserMaster.SubscriptionId,'') AS SubscriptionId
			,ISNULL(UserMaster.tintDefaultRemStatus,0) tintDefaultRemStatus
			,ISNULL(UserMaster.numDefaultTaskType,0) numDefaultTaskType
			,ISNULL(UserMaster.bitOutlook,0) bitOutlook
			,UserMaster.vcLinkedinId
			,ISNULL(UserMaster.intAssociate,0) AS intAssociate
			,UserMaster.ProfilePic
			,UserMaster.numDashboardTemplateID
			,UserMaster.vcDashboardTemplateIDs
			,UserMaster.vcEmailAlias
		FROM 
			UserMaster                      
		JOIN 
			Domain                        
		ON 
			UserMaster.numDomainID =  Domain.numDomainID                       
		LEFT JOIN 
			AdditionalContactsInformation ADC                      
		ON 
			ADC.numContactid=UserMaster.numUserDetailId                     
		LEFT JOIN 
			AuthenticationGroupMaster GM                     
		ON 
			Gm.numGroupID= UserMaster.numGroupID                    
		WHERE 
			tintGroupType=1 -- Application Users                    
		ORDER BY 
			Domain.numDomainID, vcUserDesc                            
	END   
	ELSE IF @numUserID = -1 AND @numDomainID > 0 --Support Email
	BEGIN                            
		SELECT 
			'' AS vcEmailId
			,vcImapUserName
			,ISNULL(bitUseUserName,0) bitUseUserName
			,ISNULL(ImapUserDTL.bitImap,0) bitImap
			,ImapUserDTL.vcImapServerUrl
			,ISNULL(ImapUserDTL.vcImapPassword,'') vcImapPassword
			,ImapUserDTL.bitSSl
			,ImapUserDTL.numPort 
			,'' AS vcEmailAlias
		FROM 
			[ImapUserDetails] ImapUserDTL   
		WHERE 
			ImapUserDTL.numUserCntId = @numUserID 
			AND ImapUserDTL.numDomainID = @numDomainID
	END                         
	ELSE                            
	BEGIN   
		DECLARE @vcSMTPServer VARCHAR(100)
			,@numSMTPPort NUMERIC(18,0)
			,@bitSMTPSSL BIT
			,@bitSMTPAuth BIT
			,@vcIMAPServer  VARCHAR(100)
			,@numIMAPPort NUMERIC(18,0)
			,@bitIMAPSSL BIT
			,@bitIMAPAuth BIT
	    
		SELECT
			@vcSMTPServer=vcSMTPServer
			,@numSMTPPort=numSMTPPort
			,@bitSMTPSSL=bitSMTPSSL
			,@bitSMTPAuth=bitSMTPAuth
			,@vcIMAPServer=vcIMAPServer
			,@numIMAPPort=numIMAPPort
			,@bitIMAPSSL=bitIMAPSSL
			,@bitIMAPAuth=bitIMAPAuth
		FROM
			UniversalSMTPIMAP
	    WHERE
			numDomainID=@numDomainID
		             
		SELECT       
			U.numUserID
			,vcUserName
			,numGroupID
			,vcUserDesc
			,U.numDomainID
			,vcDomainName
			,U.numUserDetailId
			,bitHourlyRate
			,bitActivateFlag
			,bitSalary
			,numDailyHours
			,bitOverTime
			,numLimDailHrs
			,ISNULL(monOverTimeRate,0) monOverTimeRate
			,bitMainComm
			,fltMainCommPer
			,bitRoleComm
			,(SELECT COUNT(*) FROM UserRoles WHERE UserRoles.numUserCntID=U.numUserDetailId) AS Roles
			,vcEmailid
			,ISNULL(vcEmailAlias,'') vcEmailAlias
			,vcPassword
			,ISNULL(ExcUserDTL.bitExchangeIntegration,0) bitExchangeIntegration
			,ISNULL(ExcUserDTL.bitAccessExchange,0) bitAccessExchange
			,ISNULL(ExcUserDTL.vcExchPassword,'') as vcExchPassword
			,ISNULL(ExcUserDTL.vcExchPath,'') as vcExchPath
			,ISNULL(ExcUserDTL.vcExchDomain,'') as vcExchDomain
			,ISNULL(U.SubscriptionId,'') as SubscriptionId
			,ISNULL(ImapUserDTL.bitImap,0) bitImap
			,ISNULL(ImapUserDTL.vcImapServerUrl,ISNULL(@vcIMAPServer,'')) vcImapServerUrl
			,ISNULL(ImapUserDTL.vcImapPassword,'') vcImapPassword
			,ISNULL(ImapUserDTL.bitSSl,ISNULL(@bitIMAPSSL,0)) bitSSl
			,ISNULL(ImapUserDTL.numPort,ISNULL(@numIMAPPort,0)) numPort
			,ISNULL(ImapUserDTL.bitUseUserName,ISNULL(@bitIMAPAuth,0)) bitUseUserName
			,(CASE WHEN ISNULL([vcSMTPServer],'') <> '' AND ISNULL(numSMTPPort,0) <> 0 THEN ISNULL(bitSMTPAuth,0) ELSE ISNULL(@bitSMTPAuth,0) END) bitSMTPAuth
			,ISNULL([vcSmtpPassword],'')vcSmtpPassword
			,(CASE WHEN ISNULL([vcSMTPServer],'') = '' THEN ISNULL(@vcSMTPServer,'') ELSE [vcSMTPServer] END) vcSMTPServer
			,(CASE WHEN ISNULL(numSMTPPort,0) = 0 THEN ISNULL(@numSMTPPort,0) ELSE numSMTPPort END) numSMTPPort
			,(CASE WHEN ISNULL([vcSMTPServer],'') <> '' AND ISNULL(numSMTPPort,0) <> 0 THEN ISNULL(bitSMTPSSL,0) ELSE ISNULL(@bitSMTPSSL,0) END) bitSMTPSSL
			,ISNULL(bitSMTPServer,0) bitSMTPServer
			,ISNULL(U.tintDefaultRemStatus,0) tintDefaultRemStatus
			,ISNULL(U.numDefaultTaskType,0) numDefaultTaskType
			,ISNULL(U.bitOutlook,0) bitOutlook
			,ISNULL(numDefaultClass,0) numDefaultClass
			,ISNULL(numDefaultWarehouse,0) numDefaultWarehouse
			,U.vcLinkedinId
			,ISNULL(U.intAssociate,0) as intAssociate
			,U.ProfilePic
			,U.numDashboardTemplateID
			,U.vcDashboardTemplateIDs
			,ISNULL(bitPayroll,0) bitPayroll
			,ISNULL(monHourlyRate,0) monHourlyRate
			,ISNULL(tintPayrollType,1) tintPayrollType
			,ISNULL(tintHourType,1) tintHourType
		FROM 
			UserMaster U                        
		JOIN 
			Domain D                       
		ON 
			U.numDomainID =  D.numDomainID       
		LEFT JOIN 
			ExchangeUserDetails ExcUserDTL      
		ON 
			ExcUserDTL.numUserID=U.numUserID                      
		LEFT JOIN 
			[ImapUserDetails] ImapUserDTL   
		ON 
			ImapUserDTL.numUserCntId = U.numUserDetailId   
			AND ImapUserDTL.numDomainID = D.numDomainID
		WHERE 
			U.numUserID=@numUserID                            
	 END                            
END

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
     
GO

IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getwarehouseidfor850')
DROP PROCEDURE usp_getwarehouseidfor850
GO
CREATE PROCEDURE [dbo].[USP_GetWarehouseIdFor850]        
@numDomainID as numeric(9)=0,        
@numWareHouseID as numeric(9)=0        
as
BEGIN         
		SELECT TOP 1 numWareHouseID

	   FROM [dbo].[Warehouses] W 
	   LEFT JOIN
			AddressDetails AD
		ON
			W.numAddressID = AD.numAddressID
  
	   WHERE 
			W.numDomainID=@numDomainID
			AND (numWareHouseID=@numWareHouseID  OR ISNULL(@numWareHouseID,0) = 0)
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON


GO

IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetWareHouseItems')
DROP PROCEDURE USP_GetWareHouseItems
GO



CREATE PROCEDURE [dbo].[USP_GetWareHouseItems]                              
@numItemCode as numeric(9)=0,
@byteMode as tinyint=0,
@numWarehouseItemID AS NUMERIC(18,0) = NULL,
@numWarehouseID numeric(18,0)=0,
@numWLocationID AS NUMERIC(18,0)=0 --05052018 Change: Warehouse Location added

                         
as                              
                              
                          
                              
declare @bitSerialize as bit                      
declare @str as nvarchar(max)                 
declare @str1 as nvarchar(max)               
declare @ColName as varchar(500)                      
set @str=''                       

DECLARE @numDomainID AS INT 
DECLARE @numBaseUnit AS NUMERIC(18,0)
DECLARE @vcUnitName as VARCHAR(100) 
DECLARE @numSaleUnit AS NUMERIC(18,0)
DECLARE @vcSaleUnitName as VARCHAR(100) 
DECLARE @numPurchaseUnit AS NUMERIC(18,0)
DECLARE @vcPurchaseUnitName as VARCHAR(100)
DECLARE @bitLot AS BIT                     
DECLARE @bitKitParent BIT
DECLARE @bitMatrix BIT
declare @numItemGroupID as numeric(9)                        
                        
select 
	@numDomainID=numDomainID,
	@numBaseUnit = ISNULL(numBaseUnit,0),
	@numSaleUnit = ISNULL(numSaleUnit,0),
	@numPurchaseUnit = ISNULL(numPurchaseUnit,0),
	@numItemGroupID=numItemGroup,
	@bitLot = bitLotNo,
	@bitSerialize=CASE WHEN bitSerialized=0 THEN bitLotNo ELSE bitSerialized END,
	@bitKitParent = ( CASE 
						WHEN ISNULL(bitKitParent,0) = 1 AND ISNULL(bitAssembly,0) = 1 THEN 0
                        WHEN ISNULL(bitKitParent,0) = 1 THEN 1
                        ELSE 0
                     END )
	,@bitMatrix=bitMatrix
FROM 
	Item 
WHERE 
	numItemCode=@numItemCode     
	
SELECT @vcUnitName=ISNULL(vcUnitName,'-') FROM UOM u WHERE u.numUOMId=@numBaseUnit
SELECT @vcSaleUnitName=ISNULL(vcUnitName,'-') FROM UOM u WHERE u.numUOMId=@numSaleUnit
SELECT @vcPurchaseUnitName=ISNULL(vcUnitName,'-') FROM UOM u WHERE u.numUOMId=@numPurchaseUnit
	
DECLARE @numSaleUOMFactor AS DECIMAL(28,14)
DECLARE @numPurchaseUOMFactor AS DECIMAL(28,14)

SELECT @numSaleUOMFactor = dbo.fn_UOMConversion(@numBaseUnit,@numItemCode,@numDOmainID,@numSaleUnit)
SELECT @numPurchaseUOMFactor = dbo.fn_UOMConversion(@numBaseUnit,@numItemCode,@numDOmainID,@numPurchaseUnit)


DECLARE @ID AS NUMERIC(18,0)  = 0                       
DECLARE @numCusFlDItemID AS VARCHAR(20)                        
DECLARE @fld_label AS VARCHAR(100),@fld_type AS VARCHAR(100)                        
DECLARE @Fld_ValueMatrix NUMERIC(18,0)
DECLARE @Fld_ValueNameMatrix VARCHAR(300)

IF @bitMatrix = 1
BEGIN
	SET @ColName='I.numItemCode,1' 

	SELECT 
		ROW_NUMBER() OVER(ORDER BY CFW_Fld_Master.fld_id) ID
		,CFW_Fld_Master.Fld_label
		,CFW_Fld_Master.fld_id
		,CFW_Fld_Master.fld_type
		,CFW_Fld_Master.numlistid
		,CFW_Fld_Master.vcURL
		,ItemAttributes.FLD_Value
		,CASE 
			WHEN CFW_Fld_Master.fld_type='SelectBox' THEN dbo.GetListIemName(Fld_Value)
			WHEN CFW_Fld_Master.fld_type='CheckBox' THEN CASE WHEN isnull(Fld_Value,0)=1 THEN 'Yes' ELSE 'No' END
			ELSE CAST(Fld_Value AS VARCHAR)
		END AS FLD_ValueName
	INTO 
		#tempTableMatrix
	FROM
		CFW_Fld_Master 
	INNER JOIN
		ItemGroupsDTL 
	ON 
		CFW_Fld_Master.Fld_id = ItemGroupsDTL.numOppAccAttrID
		AND ItemGroupsDTL.tintType = 2
	LEFT JOIN
		ItemAttributes
	ON
		CFW_Fld_Master.Fld_id = ItemAttributes.FLD_ID
		AND ItemAttributes.numItemCode = @numItemCode
	LEFT JOIN
		ListDetails LD
	ON
		CFW_Fld_Master.numlistid = LD.numListID
	WHERE
		CFW_Fld_Master.numDomainID = @numDomainId
		AND ItemGroupsDTL.numItemGroupID = @numItemGroupID
	GROUP BY
		CFW_Fld_Master.Fld_label
		,CFW_Fld_Master.fld_id
		,CFW_Fld_Master.fld_type
		,CFW_Fld_Master.bitAutocomplete
		,CFW_Fld_Master.numlistid
		,CFW_Fld_Master.vcURL
		,ItemAttributes.FLD_Value 

	
	DECLARE @i AS INT = 1
	DECLARE @iCount AS INT
	SELECT @iCount = COUNT(*) FROM #tempTableMatrix
                         
	WHILE @i <= @iCount
	 BEGIN                        
		SELECT @fld_label=Fld_label,@Fld_ValueMatrix=FLD_Value,@Fld_ValueNameMatrix=FLD_ValueName FROM #tempTableMatrix WHERE ID=@i
                          
		SET @str = @str+ CONCAT (',',@Fld_ValueMatrix,' as [',@fld_label,']')
	
		IF @byteMode=1                                        
			SET @str = @str+ CONCAT (',''',@Fld_ValueNameMatrix,''' as [',@fld_label,'Value]')                                   
                          
		SET @i = @i + 1
	 END
END
ELSE
BEGIN
	SET @ColName='WareHouseItems.numWareHouseItemID,0' 

	--Create a Temporary table to hold data                                                            
	create table #tempTable 
	( 
		ID INT IDENTITY PRIMARY KEY,                                                                      
		numCusFlDItemID NUMERIC(9),
		Fld_Value VARCHAR(20)                                                         
	)

	INSERT INTO #tempTable                         
	(
		numCusFlDItemID
		,Fld_Value
	)                                                            
	SELECT DISTINCT
		numOppAccAttrID
		,''
	FROM 
		ItemGroupsDTL 
	WHERE 
		numItemGroupID=@numItemGroupID 
		AND tintType=2    

	SELECT TOP 1 
		@ID=ID
		,@numCusFlDItemID=numCusFlDItemID
		,@fld_label=fld_label
		,@fld_type=fld_type 
	FROM 
		#tempTable                         
	JOIN 
		CFW_Fld_Master 
	ON 
		numCusFlDItemID=Fld_ID                        
                         
	 WHILE @ID>0                        
	 BEGIN                        
                          
		SET @str = @str+',  dbo.GetCustFldItems('+@numCusFlDItemID+',9,'+@ColName+') as ['+ @fld_label+']'
	
		IF @byteMode=1                                        
			SET @str = @str+',  dbo.GetCustFldItemsValue('+@numCusFlDItemID+',9,'+@ColName+','''+@fld_type+''') as ['+ @fld_label+'Value]'                                        
                          
	   select top 1 @ID=ID,@numCusFlDItemID=numCusFlDItemID,@fld_label=fld_label from #tempTable                         
	   join CFW_Fld_Master on numCusFlDItemID=Fld_ID and ID >@ID                        
	   if @@rowcount=0 set @ID=0                        
                          
	 END
END          
                   
                          
                 
                      
                         
                       
--      

DECLARE @KitOnHand AS NUMERIC(9);SET @KitOnHand=0

--IF @bitKitParent=1
	--SELECT @KitOnHand=ISNULL(dbo.fn_GetKitInventory(@numItemCode),0)          
  
set @str1='select '

IF @byteMode=1                                        
		set @str1 =@str1 +'I.numItemCode,'
	
set @str1 =@str1 + 'numWareHouseItemID,WareHouseItems.numWareHouseID,
ISNULL(vcWarehouse,'''') + (CASE WHEN ISNULL(WL.vcLocation,'''') = '''' THEN '''' ELSE '', '' + isnull(WL.vcLocation,'''') END) vcWarehouse,
ISNULL(vcWarehouse,'''') AS vcExternalLocation,
(CASE WHEN WareHouseItems.numWLocationID = -1 THEN ''Global'' ELSE ISNULL(WL.vcLocation,'''') END) AS vcInternalLocation,
W.numWareHouseID,
ISNULL(WareHouseItems.numOnHand,0) + ISNULL(WareHouseItems.numAllocation,0) AS TotalOnHand,
(ISNULL(WareHouseItems.numOnHand,0) * (CASE WHEN ISNULL(I.bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(I.monAverageCost,0) END)) AS monCurrentValue,
Case when @bitKitParent=1 then CAST(ISNULL(dbo.fn_GetKitInventoryByWarehouse(I.numItemCode, W.numWareHouseID),0) AS DECIMAL(18,2)) ELSE CAST(ISNULL(numOnHand,0) AS FLOAT) END AS [OnHand],
CAST((@numPurchaseUOMFactor * Case when @bitKitParent=1 THEN ISNULL(dbo.fn_GetKitInventoryByWarehouse(I.numItemCode, W.numWareHouseID),0) ELSE isnull(numOnHand,0) end) as FLOAT) as PurchaseOnHand,
CAST((@numSaleUOMFactor * Case when @bitKitParent=1 THEN ISNULL(dbo.fn_GetKitInventoryByWarehouse(I.numItemCode, W.numWareHouseID),0) ELSE isnull(numOnHand,0) end) as FLOAT) as SalesOnHand,
Case when @bitKitParent=1 THEN 0 ELSE CAST(ISNULL(numReorder,0) AS FLOAT) END as [Reorder] 
,Case when @bitKitParent=1 THEN 0 ELSE CAST(ISNULL(numOnOrder,0) AS FLOAT) END as [OnOrder]
,Case when @bitKitParent=1 THEN 0 ELSE CAST(ISNULL(numAllocation,0) AS FLOAT) END as [Allocation] 
,Case when @bitKitParent=1 THEN 0 ELSE CAST(ISNULL(numBackOrder,0) AS FLOAT) END as [BackOrder],
CAST((@numSaleUOMFactor * Case when @bitKitParent=1 THEN ISNULL(dbo.fn_GetKitInventoryByWarehouse(I.numItemCode, W.numWareHouseID),0) ELSE isnull(numOnHand,0) end) as numeric(18,2)) as OnHandUOM,
CAST((@numPurchaseUOMFactor * Case when @bitKitParent=1 THEN 0 ELSE isnull(numOnOrder,0) END) AS DECIMAL(18,2)) as OnOrderUOM,
CAST((@numPurchaseUOMFactor * Case when @bitKitParent=1 THEN 0 ELSE isnull(numReorder,0) END) AS DECIMAL(18,2)) as ReorderUOM,
CAST((@numSaleUOMFactor * Case when @bitKitParent=1 THEN 0 ELSE isnull(numAllocation,0) END) AS DECIMAL(18,2)) as AllocationUOM,
CAST((@numSaleUOMFactor * Case when @bitKitParent=1 THEN 0 ELSE isnull(numBackOrder,0) END) AS DECIMAL(18,2)) as BackOrderUOM,
@vcUnitName As vcBaseUnit,
@vcSaleUnitName As vcSaleUnit,
@vcPurchaseUnitName As vcPurchaseUnit
,0 as Op_Flag , isnull(WL.vcLocation,'''') Location,isnull(WL.numWLocationID,0) as numWLocationID,
round(isnull(monWListPrice,0),2) Price,isnull(vcWHSKU,'''') as SKU,isnull(vcBarCode,'''') as BarCode '+ @str +'                   
,(SELECT CASE WHEN ISNULL(subI.bitKitParent,0) = 1 AND ISNULL((SELECT COUNT(*) FROM OpportunityItems 
															   WHERE OpportunityItems.numItemCode = WareHouseItems.numItemID
															   AND WareHouseItems.numWareHouseItemID = OpportunityItems.numWarehouseItmsID),0) = 0 THEN 1
			ELSE 0 
	    END 
FROM Item subI WHERE subI.numItemCode = WareHouseItems.numItemID) [IsDeleteKitWarehouse],@bitKitParent [bitKitParent],
(CASE WHEN (SELECT COUNT(*) FROM ItemDetails WHERE numChildItemID=WareHouseItems.numItemID AND numWareHouseItemId = WareHouseItems.numWareHouseItemID) > 0 THEN 1 ELSE 0 END) AS bitChildItemWarehouse
,' + (CASE WHEN @bitSerialize=1 THEN CONCAT('dbo.GetWarehouseSerialLot(',@numDomainID,',WareHouseItems.numWareHouseItemID,',ISNULL(@bitLot,0),')') ELSE '''''' END) + ' AS vcSerialLot,
CASE 
	WHEN ISNULL(I.numItemGroup,0) > 0 
	THEN dbo.fn_GetAttributes(WareHouseItems.numWareHouseItemID,0)
	ELSE ''''
END AS vcAttribute,
I.numItemcode,
ISNULL(I.bitKitParent,0) AS bitKitParent,
ISNULL(I.bitSerialized,0) AS bitSerialized,
ISNULL(I.bitLotNo,0) as bitLotNo,
ISNULL(I.bitAssembly,0) as bitAssembly,
ISNULL((SELECT vcData FROM ListDetails WHERE numListID = 461 AND numListItemID = I.numItemClass) ,'''') AS numShipClass,
		(
			CASE 
				WHEN ISNULL(I.bitKitParent,0) = 1 AND ISNULL((SELECT COUNT(*) FROM OpportunityItems WHERE OpportunityItems.numItemCode = WareHouseItems.numItemID AND WareHouseItems.numWareHouseItemID = OpportunityItems.numWarehouseItmsID),0) = 0 
				THEN 1
			ELSE 
					0 
			END 
		) [IsDeleteKitWarehouse],
CASE WHEN (SELECT COUNT(*) FROM ItemDetails WHERE numChildItemID=WareHouseItems.numItemID AND numWareHouseItemId = WareHouseItems.numWareHouseItemID) > 0 THEN 1 ELSE 0 END AS bitChildItemWarehouse,
I.numAssetChartAcntId,
(CASE WHEN ISNULL(I.bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(I.monAverageCost,0) END) monAverageCost 
from WareHouseItems                           
join Warehouses W                             
on W.numWareHouseID=WareHouseItems.numWareHouseID 
left join WarehouseLocation WL on WL.numWLocationID =WareHouseItems.numWLocationID
join Item I on I.numItemCode=WareHouseItems.numItemID and WareHouseItems.numDomainId=I.numDomainId
where numItemID='+convert(varchar(15),@numItemCode) 
+ ' AND (WareHouseItems.numWarehouseItemID = ' + CAST(@numWarehouseItemID AS VARCHAR(20)) + ' OR ' + CAST(ISNULL(@numWarehouseItemID,0) AS VARCHAR(20)) + ' = 0 )'    
+ ' AND (WareHouseItems.numWareHouseID='+ CONVERT(VARCHAR(20),@numWarehouseID) + ' OR ' + CONVERT(VARCHAR(20),@numWarehouseID) + ' = 0)' 
+ ' AND (WareHouseItems.numWLocationID='+ CONVERT(VARCHAR(20),@numWLocationID) + ' OR ' + CONVERT(VARCHAR(20),@numWLocationID) + ' = 0)'        
   
print CAST(@str1 AS NTEXT)           

EXECUTE sp_executeSQL @str1, N'@bitKitParent bit,@numSaleUOMFactor DECIMAL(28,14), @numPurchaseUOMFactor DECIMAL(28,14), @vcUnitName VARCHAR(100), @vcSaleUnitName VARCHAR(100), @vcPurchaseUnitName VARCHAR(100)', @bitKitParent, @numSaleUOMFactor, @numPurchaseUOMFactor, @vcUnitName, @vcSaleUnitName, @vcPurchaseUnitName
                       
                        
set @str1='select numWareHouseItmsDTLID,WDTL.numWareHouseItemID, vcSerialNo,WDTL.vcComments as Comments,WDTL.numQty, WDTL.numQty as OldQty,ISNULL(W.vcWarehouse,'''') + (CASE WHEN ISNULL(WL.vcLocation,'''') = '''' THEN '''' ELSE '', '' + isnull(WL.vcLocation,'''') END) vcWarehouse '+ case when @bitSerialize=1 then @str else '' end +'
,WDTL.dExpirationDate, WDTL.bitAddedFromPO
from WareHouseItmsDTL WDTL                             
join WareHouseItems                             
on WDTL.numWareHouseItemID=WareHouseItems.numWareHouseItemID                              
join Warehouses W                             
on W.numWareHouseID=WareHouseItems.numWareHouseID  
left join WarehouseLocation WL on WL.numWLocationID =WareHouseItems.numWLocationID
where ISNULL(numQty,0) > 0 and numItemID='+ convert(varchar(15),@numItemCode) 
+ ' AND (WareHouseItems.numWarehouseItemID = ' + CAST(@numWarehouseItemID AS VARCHAR(20)) + ' OR ' + CAST(ISNULL(@numWarehouseItemID,0) AS VARCHAR(20)) + ' = 0 )' 
+ ' AND (WareHouseItems.numWareHouseID='+ CONVERT(VARCHAR(20),@numWarehouseID) + ' OR ' + CONVERT(VARCHAR(20),@numWarehouseID) + ' = 0)' 
+ ' AND (WareHouseItems.numWLocationID='+ CONVERT(VARCHAR(20),@numWLocationID) + ' OR ' + CONVERT(VARCHAR(20),@numWLocationID) + ' = 0)'   
                         
print CAST(@str1 AS NTEXT)                      
exec (@str1)                       
                      
IF @bitMatrix = 1
BEGIN
	SELECT
		Fld_label
		,fld_id
		,fld_type
		,numlistid
		,vcURL
		,Fld_Value 
	FROM 
		#tempTableMatrix

	DROP TABLE #tempTableMatrix
END
ELSE
BEGIN                      
	SELECT 
		Fld_label
		,fld_id
		,fld_type
		,numlistid
		,vcURL
		,Fld_Value 
	FROM 
		#tempTable                         
	JOIN 
		CFW_Fld_Master
	ON 
		numCusFlDItemID=Fld_ID                      

	DROP TABLE #tempTable
END

                      


-----------------For Kendo UI testting purpose
--create table #tempColumnConfig ( field varchar(100),                                                                      
-- title varchar(100) , format varchar(100)                                                      
-- )  
--
--insert into #tempColumnConfig
--	select 'vcWarehouse','WareHouse','' union all
--	select 'OnHand','On Hand','' union all
--	select 'Reorder','Re Order','' union all
--	select 'Allocation','Allocation','' union all
--	select 'BackOrder','Back Order','' union all
--	select 'Price','Price','{0:c}'
--
--select * from #tempColumnConfig
--
--drop table #tempColumnConfig

-----------------

--set @str1='select case when COUNT(*)=1 then MIN(numWareHouseItmsDTLID) else 0 end as numWareHouseItmsDTLID,MIN(WDTL.numWareHouseItemID) as numWareHouseItemID, vcSerialNo,0 as Op_Flag,WDTL.vcComments as Comments'+ case when @bitSerialize=1 then @str else'' end +',W.vcWarehouse,COUNT(*) Qty
--from WareHouseItmsDTL WDTL                             
--join WareHouseItems WI                             
--on WDTL.numWareHouseItemID=WI.numWareHouseItemID                              
--join Warehouses W                             
--on W.numWareHouseID=WI.numWareHouseID  
--where (tintStatus is null or tintStatus=0)  and  numItemID='+ convert(varchar(15),@numItemCode) +'
--GROUP BY vcSerialNo,WDTL.vcComments,W.vcWarehouse'
--                          
--print @str1                       
--exec (@str1)                       
              
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetWorkFlowMasterList')
DROP PROCEDURE USP_GetWorkFlowMasterList
GO
CREATE PROCEDURE [dbo].[USP_GetWorkFlowMasterList]      
@numDomainId as numeric(9),  
@numFormID NUMERIC(18,0), 
@CurrentPage int,                                                        
@PageSize int,                                                        
@TotRecs int output,     
@SortChar char(1)='0' ,                                                       
@columnName as Varchar(50),                                                        
@columnSortOrder as Varchar(50)  ,
@SearchStr  as Varchar(50)   
    
as       
    SET NOCOUNT ON
     
Create table #tempTable ( ID INT IDENTITY PRIMARY KEY,                                                         
      numWFID NUMERIC(18,0)                                                       
 )     
declare @strSql as varchar(8000)                                                  
    
set  @strSql='Select numWFID from WorkFlowMaster where numdomainid='+ convert(varchar(15),@numDomainID) 
    
if @SortChar<>'0' set @strSql=@strSql + ' And vcWFName like '''+@SortChar+'%'''     

if @numFormID <> 0 set @strSql=@strSql + ' And numFormID = '+ convert(varchar(15),@numFormID) +''   

if @SearchStr<>'' set @strSql=@strSql + ' And (vcWFName like ''%'+@SearchStr+'%'' or 
vcWFDescription like ''%'+@SearchStr+'%'') ' 
    
set  @strSql=@strSql +'ORDER BY ' + @columnName +' '+ @columnSortOrder    
    
    PRINT @strSql
insert into #tempTable(numWFID) exec(@strSql)    
    
 declare @firstRec as integer                                                        
 declare @lastRec as integer                                                        
 set @firstRec= (@CurrentPage-1) * @PageSize                                                        
 set @lastRec= (@CurrentPage*@PageSize+1)                                                         

 SELECT  @TotRecs = COUNT(*)  FROM #tempTable	
	
Select ROW_NUMBER()  OVER (ORDER BY ID ASC) AS 'RowNo', WM.numWFID,WM.numDomainID,WM.vcWFName,WM.vcWFDescription,dbo.fn_StripHTML(WM.vcWFAction) AS vcWFAction ,CASE WHEN wm.bitActive=1 THEN 'Active' ELSE 'InActive' END AS Status,WM.numCreatedBy,
		WM.vcDateField,WM.intDays,WM.intActionOn,WM.numModifiedBy,WM.dtCreatedDate,WM.dtModifiedDate,WM.bitActive,WM.numFormID,WM.tintWFTriggerOn,DFM.vcFormName,CASE WHEN (WM.tintWFTriggerOn=1 AND wm.intDays=0)THEN 'Create' WHEN WM.tintWFTriggerOn=2 THEN 'Edit' WHEN WM.tintWFTriggerOn=3 THEN 'Date Field' WHEN WM.tintWFTriggerOn=4 THEN 'Fields Update' WHEN WM.tintWFTriggerOn=5 THEN 'Delete' WHEN WM.tintWFTriggerOn=6 THEN 'A/R Aging' ELSE 'NA' end AS TriggeredOn
from WorkFlowMaster WM join #tempTable T on T.numWFID=WM.numWFID
JOIN DynamicFormMaster DFM ON WM.numFormID=DFM.numFormID AND DFM.tintFlag=3
   WHERE ID > @firstRec and ID < @lastRec order by ID
   
   DROP TABLE #tempTable
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ImportFileReport_Insert' ) 
    DROP PROCEDURE USP_ImportFileReport_Insert
GO

CREATE PROCEDURE USP_ImportFileReport_Insert
	@intImportFileID BIGINT,
	@intRowNumber INT,
	@vcMessage varchar(1000),
	@numRecordID NUMERIC(18,0),
	@vcErrorMessage VARCHAR(MAX),
	@vcStackStrace VARCHAR(MAX)
AS 
BEGIN
	INSERT INTO Import_File_Report
	(
		intImportFileID
		,intRowNumber
		,vcMessage
		,numRecordID
		,vcErrorMessage
		,vcStackStrace
	)
	VALUES
	(
		@intImportFileID
		,@intRowNumber
		,@vcMessage
		,@numRecordID
		,@vcErrorMessage
		,@vcStackStrace
	)
END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_InLineEditDataSave')
DROP PROCEDURE USP_InLineEditDataSave
GO
CREATE PROCEDURE [dbo].[USP_InLineEditDataSave] 
( 
 @numDomainID NUMERIC(9),
 @numUserCntID NUMERIC(9),
 @numFormFieldId NUMERIC(9),
 @bitCustom AS BIT = 0,
 @InlineEditValue VARCHAR(8000),
 @numContactID NUMERIC(9),
 @numDivisionID NUMERIC(9),
 @numWOId NUMERIC(9),
 @numProId NUMERIC(9),
 @numOppId NUMERIC(9),
 @numRecId NUMERIC(9),
 @vcPageName NVARCHAR(100),
 @numCaseId NUMERIC(9),
 @numWODetailId NUMERIC(9),
 @numCommID NUMERIC(9)
 )
AS 

declare @strSql as nvarchar(4000)
DECLARE @vcDbColumnName AS VARCHAR(100),@vcOrigDbColumnName AS VARCHAR(100),@vcLookBackTableName AS varchar(100)
DECLARE @vcListItemType as VARCHAR(3),@numListID AS numeric(9),@vcAssociatedControlType varchar(20)
declare @tempAssignedTo as numeric(9);set @tempAssignedTo=null           
DECLARE @Value1 AS VARCHAR(18),@Value2 AS VARCHAR(18)

IF  @bitCustom =0
BEGIN
	SELECT @vcDbColumnName=vcDbColumnName,@vcLookBackTableName=vcLookBackTableName,
		   @vcListItemType=vcListItemType,@numListID=numListID,@vcAssociatedControlType=vcAssociatedControlType,
		   @vcOrigDbColumnName=vcOrigDbColumnName
	 FROM DycFieldMaster WHERE numFieldId=@numFormFieldId
	 
	IF @vcAssociatedControlType = 'DateField' AND  @InlineEditValue=''
	BEGIN
		SET @InlineEditValue = null
	END

	DECLARE @intExecuteDiv INT=0
	if @vcLookBackTableName = 'DivisionMaster' 
	 BEGIN 
		
	   IF @vcDbColumnName='numFollowUpStatus' --Add to FollowUpHistory
	    Begin
			IF(@numCommID>0 AND @numDivisionID=0)
			BEGIN
				SET @numDivisionID=(SELECT TOP 1 numDivisionId FROM Communication WHERE numCommId=@numCommID)
			END
			declare @numFollow as varchar(10),@binAdded as varchar(20),@PreFollow as varchar(10),@RowCount as varchar(2)                            
			                      
			set @PreFollow=(select count(*)  numFollowUpStatus from FollowUpHistory where numDivisionID= @numDivisionID and numDomainId= @numDomainID)                          
			set @RowCount=@@rowcount  
			                        
			select   @numFollow=numFollowUpStatus, @binAdded=bintModifiedDate from divisionmaster where numDivisionID=@numDivisionID and numDomainId= @numDomainID                    
			
			if @numFollow <>'0' and @numFollow <> @InlineEditValue                          
			begin                          
				if @PreFollow<>0                          
					begin                          
			                   
						if @numFollow <> (select top 1  numFollowUpStatus from FollowUpHistory where numDivisionID= @numDivisionID and numDomainId= @numDomainID order by numFollowUpStatusID desc)                          
							begin                          
			                      	insert into FollowUpHistory(numFollowUpstatus,numDivisionID,bintAddedDate,numDomainId)                          
									values(@numFollow,@numDivisionID,@binAdded,@numDomainID)                          
							end                          
					end                          
				else                          
					begin                          
						insert into FollowUpHistory(numFollowUpstatus,numDivisionID,bintAddedDate,numDomainId)                          
						 values(@numFollow,@numDivisionID,@binAdded,@numDomainID)                          
					end                          
			   end 
			   
			
			UPDATE  DivisionMaster SET  dtLastFollowUp=getutcdate() where numDivisionID=@numDivisionID and numDomainID=@numDomainID
		END
		 IF @vcDbColumnName='numAssignedTo' ---Updating if organization is assigned to someone
			BEGIN
				select @tempAssignedTo=isnull(numAssignedTo,0) from DivisionMaster where numDivisionID = @numDivisionID and numDomainId= @numDomainID           
			
				if (@tempAssignedTo<>@InlineEditValue and  @InlineEditValue<>'0')          
					begin            
						update DivisionMaster set numAssignedTo=@InlineEditValue ,numAssignedBy=@numUserCntID where numDivisionID = @numDivisionID  and numDomainId= @numDomainID  
						UPDATE CompanyInfo SET bintModifiedDate=GETUTCDATE() WHERE numCompanyId IN (SELECT numCompanyId 
																									FROM 
																										DivisionMaster
																									WHERE 
																										numDivisionID = @numDivisionID  and numDomainId= @numDomainID)         
					end					
				else if  (@InlineEditValue ='0')          
					begin          
						update DivisionMaster set numAssignedTo=0 ,numAssignedBy=0 where numDivisionID = @numDivisionID  and numDomainId= @numDomainID
						UPDATE CompanyInfo SET bintModifiedDate=GETUTCDATE() WHERE numCompanyId IN (SELECT numCompanyId 
																									FROM 
																										DivisionMaster
																									WHERE 
																										numDivisionID = @numDivisionID  and numDomainId= @numDomainID)          
					end    
			END
	    UPDATE CompanyInfo SET numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() WHERE numCompanyId=(SELECT TOP 1 numCompanyId FROM DivisionMaster where numDivisionID=@numDivisionID and numDomainID=@numDomainID) and numDomainID=@numDomainID
		
		IF @vcDbColumnName='vcPartnerCode'
		BEGIN
			IF(@InlineEditValue<>'')
			BEGIN
				IF EXISTS(SELECT vcPartnerCode FROM DivisionMaster where numDivisionID<>@numDivisionID and numDomainID=@numDomainID AND vcPartnerCode=@InlineEditValue)
				BEGIN
					SET @intExecuteDiv=1
					SET @InlineEditValue='-#12'
				END
			END
		END
		IF @intExecuteDiv=0
		BEGIN
			SET @strSql='update DivisionMaster set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numDivisionID=@numDivisionID and numDomainID=@numDomainID'
			
		END
	 END 
	 ELSE if @vcLookBackTableName = 'AdditionalContactsInformation' 
	 BEGIN 
			IF @vcDbColumnName='bitPrimaryContact' AND @InlineEditValue='True'
			BEGIN
				DECLARE @bitPrimaryContactCheck AS BIT             
				DECLARE @numID AS NUMERIC(9) 
				
				SELECT TOP 1 @numID = numContactID,@bitPrimaryContactCheck = ISNULL(bitPrimaryContact,0)
					FROM AdditionalContactsInformation WHERE numDivisionID = @numDivisionID 
                   
					IF @@rowcount = 0 SET @numID = 0             
				
					WHILE @numID > 0            
					BEGIN            
						IF @bitPrimaryContactCheck = 1 
							UPDATE  AdditionalContactsInformation SET bitPrimaryContact = 0 WHERE numContactID = @numID  
                              
						SELECT TOP 1 @numID = numContactID,@bitPrimaryContactCheck = ISNULL(bitPrimaryContact,0)
						FROM AdditionalContactsInformation WHERE numDivisionID = @numDivisionID AND numContactID > @numID    
                                
						IF @@rowcount = 0 SET @numID = 0             
					END 
				END
				ELSE IF @vcDbColumnName='bitPrimaryContact' AND @InlineEditValue='False'
				BEGIN
					IF (SELECT COUNT(*) FROM AdditionalContactsInformation WHERE numDivisionID = @numDivisionID AND numContactId!=@numContactId AND ISNULL(bitPrimaryContact,0)=1) = 0
						SET @InlineEditValue='True'
				END 
				-- Added by Priya(to check no Campaign got engaged wothout contacts email
				ELSE IF @vcDbColumnName = 'numECampaignID' 
				BEGIN
					
					IF EXISTS (select 1 from AdditionalContactsInformation WHERE numContactId = @numContactID AND (vcEmail IS NULL OR vcEmail = ''))
					BEGIN
						RAISERROR('EMAIL_ADDRESS_REQUIRED',16,1)
								RETURN
					END

				END
				
		SET @strSql='update AdditionalContactsInformation set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numDivisionID=@numDivisionID and numDomainID=@numDomainID and numContactId=@numContactId'
	 END 
	 ELSE if @vcLookBackTableName = 'CompanyInfo' 
	 BEGIN 
		SET @strSql='update CompanyInfo set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numCompanyId=(select numCompanyId from divisionmaster where numDivisionID=@numDivisionID and numDomainID=@numDomainID) and numDomainID=@numDomainID'
	 END 
	 ELSE if @vcLookBackTableName = 'WorkOrder' 
	 BEGIN 
		DECLARE @numItemCode AS NUMERIC(9)
	 
		IF @numWODetailId>0
		BEGIN
			SET @strSql='update WorkOrderDetails set ' + @vcDbColumnName + '=@InlineEditValue where numWOId=@numWOId and numWODetailId=@numWODetailId'
			
			IF @vcDbColumnName='numQtyItemsReq'
			BEGIN 
				SELECT @numItemCode=numChildItemID FROM WorkOrderDetails WHERE numWOId=@numWOId and numWODetailId=@numWODetailId
				
				EXEC USP_UpdateWorkOrderDetailQuantity @numItemCode,@InlineEditValue,@numWOId
			END 
		END 	
		ELSE
		BEGIN
			SET @strSql='update WorkOrder set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numWOId=@numWOId and numDomainID=@numDomainID'
			
			IF @vcDbColumnName='numQtyItemsReq'
			BEGIN 
				SELECT @numItemCode=numItemCode FROM WorkOrder WHERE numWOId=@numWOId and numDomainID=@numDomainID
			
				EXEC USP_UpdateWorkOrderDetailQuantity @numItemCode,@InlineEditValue,@numWOId
			END 
		END	
	 END
	 ELSE if @vcLookBackTableName = 'ProjectsMaster' 
	 BEGIN 
		IF @vcDbColumnName='numAssignedTo' ---Updating if Project is assigned to someone 
			BEGIN
				select @tempAssignedTo=isnull(numAssignedTo,0) from ProjectsMaster where numProId=@numProId and numDomainId= @numDomainID           
			
				if (@tempAssignedTo<>@InlineEditValue and  @InlineEditValue<>'0')          
					begin            
						update ProjectsMaster set numAssignedTo=@InlineEditValue ,numAssignedBy=@numUserCntID,bintModifiedDate=GETUTCDATE() where numProId = @numProId  and numDomainId= @numDomainID           
					end           
				else if  (@InlineEditValue ='0')          
					begin          
						update ProjectsMaster set numAssignedTo=0 ,numAssignedBy=0,bintModifiedDate=GETUTCDATE() where numProId = @numProId  and numDomainId= @numDomainID        
					end    
			END
		else IF @vcDbColumnName='numProjectStatus'  ---Updating All Statge to 100% if Completed
			BEGIN
				IF @InlineEditValue='27492'
				BEGIN
					UPDATE PM SET dtCompletionDate=GETUTCDATE(),monTotalExpense=PIE.monExpense,
					monTotalIncome=PIE.monIncome,monTotalGrossProfit=PIE.monBalance,numProjectStatus=27492
					FROM ProjectsMaster PM,dbo.GetProjectIncomeExpense(@numDomainId,@numProId) PIE
					WHERE PM.numProId=@numProId
					
					EXEC USP_ManageProjectCommission @numDomainId,@numProId
						
					Update StagePercentageDetails set tinProgressPercentage=100,bitclose=1 where numProjectID=@numProId

					EXEC dbo.USP_GetProjectTotalProgress
							@numDomainID = @numDomainID, --  numeric(9, 0)
							@numProId = @numProId, --  numeric(9, 0)
							@tintMode = 1 --  tinyint
				END			
				ELSE
				BEGIN
					DELETE from BizDocComission WHERE numDomainID = @numDomainID AND numProId=@numProId AND numComissionID 
						NOT IN (SELECT BC.numComissionID FROM dbo.BizDocComission BC JOIN dbo.PayrollTracking PT 
							ON BC.numComissionID = PT.numComissionID AND BC.numDomainId = PT.numDomainId WHERE BC.numDomainID = @numDomainID AND ISNULL(BC.numProId,0) = @numProId)
				END
			END
			

		SET @strSql='update ProjectsMaster set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numProId=@numProId and numDomainID=@numDomainID'
	 END 
	 ELSE if @vcLookBackTableName = 'OpportunityMaster' 
	 BEGIN 
		IF @vcDbColumnName='numAssignedTo' ---Updating if organization is assigned to someone  
			BEGIN
				select @tempAssignedTo=isnull(numAssignedTo,0) from OpportunityMaster where numOppId=@numOppId and numDomainId= @numDomainID           
			
				if (@tempAssignedTo<>@InlineEditValue and  @InlineEditValue<>'0')          
					begin            
						update OpportunityMaster set numAssignedTo=@InlineEditValue ,numAssignedBy=@numUserCntID,bintModifiedDate=GETUTCDATE() where numOppId = @numOppId  and numDomainId= @numDomainID           
					end           
				else if  (@InlineEditValue ='0')          
					begin          
						update OpportunityMaster set numAssignedTo=0 ,numAssignedBy=0,bintModifiedDate=GETUTCDATE() where numOppId = @numOppId  and numDomainId= @numDomainID        
					end    
			END
		ELSE IF @vcDbColumnName='numPartner' OR @vcDbColumnName='numPartenerSource'
		 BEGIN
			update OpportunityMaster set numPartner=@InlineEditValue ,bintModifiedDate=GETUTCDATE() where numOppId = @numOppId  and numDomainId= @numDomainID           
		 END
		
		ELSE IF @vcDbColumnName='tintActive'
		 BEGIN
		 	SET @InlineEditValue= CASE @InlineEditValue WHEN 'True' THEN 1 ELSE 0 END 
		 END
		 ELSE IF @vcDbColumnName='tintSource'
		 BEGIN
			IF CHARINDEX('~', @InlineEditValue)>0
			BEGIN
				SET @Value1=SUBSTRING(@InlineEditValue, 1,CHARINDEX('~', @InlineEditValue)-1) 
				SET @Value2=SUBSTRING(@InlineEditValue, CHARINDEX('~', @InlineEditValue)+1,LEN(@InlineEditValue)) 
			END	
			ELSE
			BEGIN
				SET @Value1=@InlineEditValue
				SET @Value2=0
			END
			
		 	update OpportunityMaster set tintSourceType=@Value2 where numOppId = @numOppId  and numDomainId= @numDomainID        
		 	
		 	SET @InlineEditValue= @Value1
		 END
		ELSE IF @vcDbColumnName='tintOppStatus' --Update Inventory  0=Open,1=Won,2=Lost
		 BEGIN			
			DECLARE @bitViolatePrice BIT
			DECLARE @intPendingApprovePending INT
		
			IF EXISTS (SELECT numApprovalRuleID FROM ApprovalProcessItemsClassification WHERE numDomainId=@numDomainID AND ISNULL(bitMarginPriceViolated,0) = 1)
			BEGIN
				SET @bitViolatePrice = 1
			END
			SET @intPendingApprovePending=(SELECT COUNT(*) FROM OpportunityItems WHERE numOppId=@numOppId AND ISNULL(bitItemPriceApprovalRequired,0)=1)

			IF(@intPendingApprovePending > 0 AND @bitViolatePrice=1 AND @InlineEditValue='1')
			BEGIN
				SET @intExecuteDiv=1
				SET @InlineEditValue='-#123'
			END

			IF ISNULL(@intExecuteDiv,0)=0
			BEGIN
				DECLARE @tintOppStatus AS TINYINT
				SELECT @tintOppStatus=tintOppStatus FROM OpportunityMaster WHERE numOppID=@numOppID              

				IF @tintOppStatus=0 AND @InlineEditValue='1' --Open to Won
				BEGIN
					SELECT @numDivisionID=numDivisionID FROM OpportunityMaster WHERE numOppID=@numOppID   
					
					DECLARE @tintCRMType AS TINYINT      
					SELECT @tintCRMType=tintCRMType FROM divisionmaster WHERE numDivisionID =@numDivisionID 

					-- Promote Lead to Account when Sales/Purchase Order is created against it
					IF @tintCRMType=0 --Lead & Order
					BEGIN        
						UPDATE 
							divisionmaster 
						SET 
							tintCRMType=2
							,bintLeadProm=GETUTCDATE()
							,bintLeadPromBy=@numUserCntID
							,bintProsProm=GETUTCDATE()
							,bintProsPromBy=@numUserCntID        
						WHERE 
							numDivisionID=@numDivisionID        
					END
					--Promote Prospect to Account
					ELSE IF @tintCRMType=1 
					BEGIN        
						UPDATE 
							divisionmaster 
						SET 
							tintCRMType=2
							,bintProsProm=GETUTCDATE()
							,bintProsPromBy=@numUserCntID        
						WHERE 
							numDivisionID=@numDivisionID        
					END    

		 			EXEC USP_UpdatingInventoryonCloseDeal @numOppID,@numUserCntID

					UPDATE OpportunityMaster SET bintOppToOrder=GETUTCDATE() WHERE numOppId=@numOppId and numDomainID=@numDomainID
				END
				ELSE IF (@InlineEditValue='2') -- Win to Lost
				BEGIN
					UPDATE OpportunityMaster SET dtDealLost=GETUTCDATE() WHERE numOppId=@numOppId and numDomainID=@numDomainID
				END
				
				IF (@tintOppStatus=1 and @InlineEditValue='2') or (@tintOppStatus=1 and @InlineEditValue='0') --Won to Lost or Won to Open
						EXEC USP_RevertDetailsOpp @numOppID,0,@numUserCntID 

				UPDATE OpportunityMaster SET tintOppStatus = @InlineEditValue WHERE numOppId=@numOppID
				
			END
		 END
		 ELSE IF @vcDbColumnName='vcOppRefOrderNo' --Update vcRefOrderNo OpportunityBizDocs
		 BEGIN
			 IF LEN(ISNULL(@InlineEditValue,''))>0
			 BEGIN
	   			UPDATE dbo.OpportunityBizDocs SET vcRefOrderNo=@InlineEditValue WHERE numOppId=@numOppID
			 END
		 END
		 ELSE IF @vcDbColumnName='numStatus' 
		 BEGIN
			DECLARE @tintOppType AS TINYINT
			select @tintOppStatus=tintOppStatus,@tintOppType=tintOppType from OpportunityMaster where numOppID=@numOppID

			IF @tintOppType=1 AND @tintOppStatus=1
			BEGIN
				DECLARE @numOppStatus AS NUMERIC(18,0) = CAST(@InlineEditValue AS NUMERIC(18,0))
				EXEC USP_SalesFulfillmentQueue_Save @numDomainID,@numUserCntID,@numOppID,@numOppStatus
			END

		 	 IF EXISTS (SELECT 1 FROM dbo.OpportunityMaster WHERE numOppId = @numOppID AND tintOppType=1 AND isnull(tintOppStatus,0)=1 AND ISNULL(numStatus,0)!= @InlineEditValue)
			 BEGIN
	   				EXEC USP_ManageOpportunityAutomationQueue
							@numOppQueueID = 0, -- numeric(18, 0)
							@numDomainID = @numDomainID, -- numeric(18, 0)
							@numOppId = @numOppID, -- numeric(18, 0)
							@numOppBizDocsId = 0, -- numeric(18, 0)
							@numOrderStatus = @InlineEditValue, -- numeric(18, 0)
							@numUserCntID = @numUserCntID, -- numeric(18, 0)
							@tintProcessStatus = 1, -- tinyint
							@tintMode = 1 -- TINYINT
			END 
		 END	
		 ELSE IF @vcDbColumnName = 'dtReleaseDate'
		 BEGIN
			IF EXISTS (SELECT 1 FROM OpportunityMaster WHERE numDomainId=@numDomainID AND numOppId=@numOppId AND tintOppType=1 AND tintOppStatus=0)
			BEGIN
				IF (SELECT dbo.GetListIemName(ISNULL(numPercentageComplete,0)) FROM OpportunityMaster WHERE numDomainId=@numDomainID AND numOppId=@numOppId) = '100'
				BEGIN
					SET @strSql='update OpportunityMaster set ' + @vcDbColumnName + '=DATEADD(hh, DATEDIFF(hh, GETDATE(), GETUTCDATE()), @InlineEditValue),numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numOppId=@numOppId and numDomainID=@numDomainID'
				END
				ELSE
				BEGIN
					RAISERROR('PERCENT_COMPLETE_MUST_BE_100',16,1)
					RETURN
				END
			END
			ELSE
			BEGIN
				SET @strSql='update OpportunityMaster set ' + @vcDbColumnName + '=DATEADD(hh, DATEDIFF(hh, GETDATE(), GETUTCDATE()), @InlineEditValue),numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numOppId=@numOppId and numDomainID=@numDomainID'
			END
		END
		ELSE IF @vcDbColumnName = 'numPercentageComplete'
		BEGIN
			IF EXISTS (SELECT 1 FROM OpportunityMaster WHERE numDomainId=@numDomainID AND numOppId=@numOppId AND tintOppType=1 AND tintOppStatus=0 AND dbo.GetListIemName(ISNULL(numPercentageComplete,0))='100')
			BEGIN
				IF EXISTS(SELECT dtReleaseDate FROM OpportunityMaster WHERE numDomainId=@numDomainID AND numOppId=@numOppId AND dtReleaseDate IS NULL)
				BEGIN
					SET @strSql='update OpportunityMaster set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numOppId=@numOppId and numDomainID=@numDomainID'
				END
				ELSE
				BEGIN
					RAISERROR('REMOVE_RELEASE_DATE',16,1)
					RETURN
				END
			END
			ELSE
			BEGIN
				SET @strSql='update OpportunityMaster set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numOppId=@numOppId and numDomainID=@numDomainID'
			END
		END
		ELSE IF @vcDbColumnName = 'numProjectID'
		BEGIN
			UPDATE OpportunityItems SET numProjectID=@InlineEditValue WHERE numOppId=@numOppId
		END

		IF(@vcDbColumnName!='tintOppStatus')
		BEGIN
			SET @strSql='update OpportunityMaster set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numOppId=@numOppId and numDomainID=@numDomainID'
		END
	 END
	 ELSE if @vcLookBackTableName = 'Cases' 
	 BEGIN 
	 		IF @vcDbColumnName='numAssignedTo' ---Updating if Case is assigned to someone  
			BEGIN
				select @tempAssignedTo=isnull(numAssignedTo,0) from Cases where numCaseId=@numCaseId and numDomainId= @numDomainID           
			
				if (@tempAssignedTo<>@InlineEditValue and  @InlineEditValue<>'0')          
					begin            
						update Cases set numAssignedTo=@InlineEditValue ,numAssignedBy=@numUserCntID,bintModifiedDate=GETUTCDATE() where numCaseId = @numCaseId  and numDomainId= @numDomainID           
					end           
				else if  (@InlineEditValue ='0')          
					begin          
						update Cases set numAssignedTo=0 ,numAssignedBy=0,bintModifiedDate=GETUTCDATE() where numCaseId = @numCaseId  and numDomainId= @numDomainID        
					end    
			END
		UPDATE Cases SET bintModifiedDate=GETUTCDATE() WHERE numCaseId = @numCaseId  and numDomainId= @numDomainID    
		SET @strSql='update Cases set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numCaseId=@numCaseId and numDomainID=@numDomainID'
	 END 
	 ELSE if @vcLookBackTableName = 'Tickler' 
	 BEGIN 	
		SET @strSql='update Communication set ' + @vcOrigDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,dtModifiedDate=getutcdate() where numCommId=@numCommId and numDomainID=@numDomainID'
	 END
	 ELSE IF @vcLookBackTableName = 'ExtarnetAccounts'
	 BEGIN
		IF @vcDbColumnName='bitEcommerceAccess'
		BEGIN
			IF LOWER(@InlineEditValue) = 'yes' OR LOWER(@InlineEditValue) = 'true'
			BEGIN
				IF NOT EXISTS (SELECT * FROM ExtarnetAccounts where numDivisionID=@numDivisionID) 
				BEGIN
					DECLARE @numGroupID NUMERIC
					DECLARE @numCompanyID NUMERIC(18,0)
					SELECT @numCompanyID=numCompanyID FROM DivisionMaster WHERE numDivisionID=@numDivisionID

					SELECT TOP 1 @numGroupID=numGroupID FROM dbo.AuthenticationGroupMaster WHERE numDomainID=@numDomainID AND tintGroupType=2
				
					IF @numGroupID IS NULL SET @numGroupID = 0 
					INSERT INTO ExtarnetAccounts (numCompanyID,numDivisionID,numGroupID,numDomainID)                                                
					VALUES(@numCompanyID,@numDivisionID,@numGroupID,@numDomainID)
				END
			END
			ELSE
			BEGIN
				DELETE FROM ExtranetAccountsDtl WHERE numExtranetID IN (SELECT ISNULL(numExtranetID,0) FROM ExtarnetAccounts where numDivisionID=@numDivisionID)
				DELETE FROM ExtarnetAccounts where numDivisionID=@numDivisionID
			END

			SELECT (CASE WHEN LOWER(@InlineEditValue) = 'yes' OR LOWER(@InlineEditValue) = 'true' THEN 'Yes' ELSE 'No' END) AS vcData

			RETURN
		END
	 END
	ELSE IF @vcLookBackTableName = 'ExtranetAccountsDtl'
	BEGIN
		IF @vcDbColumnName='vcPassword'
		BEGIN
			IF LEN(@InlineEditValue) = 0
			BEGIN
				DELETE FROM ExtranetAccountsDtl WHERE numContactID=@numContactID
			END
			ELSE
			BEGIN
				IF EXISTS (SELECT numExtranetID FROM ExtarnetAccounts WHERE numDivisionID = @numDivisionID)
				BEGIN
					IF EXISTS (SELECT numExtranetDtlID FROM ExtranetAccountsDtl WHERE numContactID=@numContactID)
					BEGIN
						UPDATE
							ExtranetAccountsDtl
						SET 
							vcPassword=@InlineEditValue
							,tintAccessAllowed=1
						WHERE
							numContactID=@numContactID
					END
					ELSE
					BEGIN
						DECLARE @numExtranetID NUMERIC(18,0)
						SELECT @numExtranetID=numExtranetID FROM ExtarnetAccounts WHERE numDivisionID = @numDivisionID

						INSERT INTO ExtranetAccountsDtl (numExtranetID,numContactID,vcPassword,tintAccessAllowed,numDomainID)
						VALUES (@numExtranetID,@numContactID,@InlineEditValue,1,@numDomainID)
					END
				END
				ELSE
				BEGIN
					RAISERROR('Organization e-commerce access is not enabled',16,1)
				END
			END

			SELECT @InlineEditValue AS vcData

			RETURN
		END
	END
	ELSE IF @vcLookBackTableName = 'CustomerPartNumber'
	BEGIN
		IF @vcDbColumnName='CustomerPartNo'
		BEGIN
			SELECT @numCompanyID=numCompanyID FROM DivisionMaster WHERE numDivisionID=@numDivisionID
			
			IF EXISTS(SELECT CustomerPartNoID FROM CustomerPartNumber WHERE numItemCode = @numRecId AND numCompanyID = @numCompanyID)
			BEGIN
				UPDATE CustomerPartNumber SET CustomerPartNo = @InlineEditValue WHERE numItemCode = @numRecId AND numCompanyId = @numCompanyID AND numDomainID = @numDomainId 
			END
			ELSE
			BEGIN
				INSERT INTO CustomerPartNumber
				(numItemCode, numCompanyId, numDomainID, CustomerPartNo)
				VALUES
				(@numRecId, @numCompanyID, @numDomainID, @InlineEditValue)
			END

			SELECT @InlineEditValue AS vcData

			RETURN
		END
		
	END
	ELSE IF @vcLookBackTableName = 'CheckHeader'
	BEGIN
		IF EXISTS (SELECT 1 FROM CheckHeader WHERE numDomainID=@numDomainID AND numCheckNo=@InlineEditValue AND numCheckHeaderID <> @numRecId)
		BEGIN
			RAISERROR('DUPLICATE_CHECK_NUMBER',16,1)
			RETURN
		END

		SET @strSql='UPDATE CheckHeader SET ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,dtModifiedDate=getutcdate() WHERE numCheckHeaderID=@numRecId and numDomainID=@numDomainID'
	END
	--ELSE IF @vcLookBackTableName = 'Item'
	--BEGIN
	--	IF @vcDbColumnName = 'monListPrice'
	--	BEGIN
	--		IF EXISTS (SELECT numItemCode FROM Item WHERE numDomainID=@numDomainID AND numItemCode=@numRecId AND 1 = (CASE WHEN ISNULL(numItemGroup,0) > 0 THEN (CASE WHEN ISNULL(bitMatrix,0)=1 THEN 1 ELSE 0 END) ELSE 1 END))
	--		BEGIN
	--			UPDATE WareHouseItems SET monWListPrice=@InlineEditValue WHERE numDomainID=@numDomainID AND numItemID=@numRecId
	--			UPDATE Item SET monListPrice=@InlineEditValue WHERE numDomainID=@numDomainID AND numItemCode=@numRecId

	--			SELECT @InlineEditValue AS vcData
	--		END
	--		ELSE
	--		BEGIN
	--			RAISERROR('You are not allowed to edit list price for warehouse level matrix item.',16,1)
	--		END

	--		RETURN
	--	END

	--	SET @strSql='UPDATE Item set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numItemCode=@numRecId AND numDomainID=@numDomainID'
	--END

	EXECUTE sp_executeSQL @strSql, N'@numDomainID NUMERIC(9),@numUserCntID NUMERIC(9),@numDivisionID NUMERIC(9),@numContactID NUMERIC(9),@InlineEditValue VARCHAR(8000),@numWOId NUMERIC(9),@numProId NUMERIC(9),@numOppId NUMERIC(9),@numCaseId NUMERIC(9),@numWODetailId NUMERIC(9),@numCommID Numeric(9),@numRecId NUMERIC(18,0)',
		     @numDomainID,@numUserCntID,@numDivisionID,@numContactID,@InlineEditValue,@numWOId,@numProId,@numOppId,@numCaseId,@numWODetailId,@numCommID,@numRecId;
		     
	IF @vcAssociatedControlType='SelectBox'                                                    
       BEGIN       
			IF @vcDbColumnName='tintSource' AND @vcLookBackTableName = 'OpportunityMaster'
			BEGIN
				SELECT dbo.fn_GetOpportunitySourceValue(@Value1,@Value2,@numDomainID) AS vcData                                             
			END
			ELSE IF @vcDbColumnName='numContactID'
			BEGIN
				SELECT dbo.fn_GetContactName(@InlineEditValue)
			END
			ELSE IF @vcDbColumnName='numPartner' OR @vcDbColumnName='numPartenerSource'
			BEGIN
				SET @intExecuteDiv=1
				SELECT D.vcPartnerCode+'-'+C.vcCompanyName  AS vcData FROM DivisionMaster AS D 
				LEFT JOIN CompanyInfo AS C
				ON D.numCompanyID=C.numCompanyID WHERE D.numDivisionID=@InlineEditValue
			END
			
			ELSE IF @vcDbColumnName='tintOppStatus' AND @intExecuteDiv=1
			BEGIN
				SELECT @InlineEditValue AS vcData
			END
			ELSE IF @vcDbColumnName='numReleaseStatus'
			BEGIN
				If @InlineEditValue = '1'
				BEGIN
					SELECT 'Open'
				END
				ELSE IF @InlineEditValue = '2'
				BEGIN
					SELECT 'Purchased'
				END
				ELSE
				BEGIN
					SELECT ''
				END
			END
			ELSE IF @vcDbColumnName='numPartenerContact'
			BEGIN
				SET @InlineEditValue=(SELECT TOP 1 A.vcGivenName FROM AdditionalContactsInformation AS A 
										LEFT JOIN DivisionMaster AS D 
										ON D.numDivisionID=A.numDivisionId
										WHERE D.numDomainID=@numDomainID AND A.numCOntactId=@InlineEditValue )
				SELECT @InlineEditValue AS vcData
			END
			ELSE IF @vcDbColumnName='numPartenerSource'
			BEGIN
				
				SET @InlineEditValue=(SELECT D.vcPartnerCode+'-'+C.vcCompanyName  AS vcData FROM DivisionMaster AS D 
				LEFT JOIN CompanyInfo AS C
				ON D.numCompanyID=C.numCompanyID WHERE D.numDivisionID=@InlineEditValue)

				SELECT @InlineEditValue AS vcData
			END
			ELSE IF @vcDbColumnName='numShippingService'
			BEGIN
				SELECT ISNULL((SELECT vcShipmentService FROM ShippingService WHERE (numDomainID=@numDomainID OR ISNULL(numDomainID,0)=0) AND numShippingServiceID = @InlineEditValue),'') AS vcData
			END
			else
			BEGIN
				SELECT dbo.fn_getSelectBoxValue(@vcListItemType,@InlineEditValue,@vcDbColumnName) AS vcData                                             
			END
		end 
		ELSE
		BEGIN
			 IF @vcDbColumnName='tintActive'
		  	 BEGIN
			 	SET @InlineEditValue= CASE @InlineEditValue WHEN 1 THEN 'True' ELSE 'False' END 
			 END

			IF @vcAssociatedControlType = 'Check box' or @vcAssociatedControlType = 'Checkbox'
			BEGIN
	 			SET @InlineEditValue= Case @InlineEditValue WHEN 'True' THEN 'Yes' ELSE 'No' END
			END
			
		    SELECT @InlineEditValue AS vcData
		END  
		
	IF @vcLookBackTableName = 'AdditionalContactsInformation' AND @vcDbColumnName = 'numECampaignID'
	BEGIN
		--Manage ECampiagn When Campaign is changed from inline edit
		DECLARE @Date AS DATETIME 
		SELECT @Date = dbo.[GetUTCDateWithoutTime]()
		DECLARE @numECampaignID AS NUMERIC(18,0)
		SET @numECampaignID = @InlineEditValue

		IF @numECampaignID = -1 
		SET @numECampaignID=0
	 
 		EXEC [USP_ManageConEmailCampaign]
		@numConEmailCampID = 0, --  numeric(9, 0)
		@numContactID = @numContactID, --  numeric(9, 0)
		@numECampaignID = @numECampaignID, --  numeric(9, 0)
		@intStartDate = @Date, --  datetime
		@bitEngaged = 1, --  bit
		@numUserCntID = @numUserCntID --  numeric(9, 0)
	END  
END
ELSE
BEGIN	 

	SET @InlineEditValue=Cast(@InlineEditValue as varchar(1000))
	SELECT @vcAssociatedControlType=Fld_type
	 FROM CFW_Fld_Master WHERE Fld_Id=@numFormFieldId 

	IF @vcAssociatedControlType = 'CheckBox'
	BEGIN
	    SET @InlineEditValue=(case when @InlineEditValue='False' then 0 ELSE 1 END)
	END

	IF @vcPageName='organization'
	BEGIN 
		IF (SELECT COUNT(*) FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)>0
			BEGIN
				UPDATE CFW_FLD_Values SET Fld_Value=@InlineEditValue WHERE FldDTLID=(SELECT CFW.FldDTLID FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)
			END	
		ELSE
			BEGIN
				insert into CFW_FLD_Values (Fld_ID,Fld_Value,RecId) VALUES (@numFormFieldId,@InlineEditValue,@numRecId)         
			END

			--Added By :Sachin Sadhu ||Date:13thOct2014
			--Purpose  :To manage CustomFields in Queue
			EXEC USP_CFW_FLD_Values_CT
					@numDomainID =@numDomainID,
					@numUserCntID =@numUserCntID,
					@numRecordID =@numRecId ,
					@numFormFieldId  =@numFormFieldId
  
	  END
	ELSE IF @vcPageName='contact'
	BEGIN 
		IF (SELECT COUNT(*) FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Cont CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)>0
			BEGIN
				UPDATE CFW_FLD_Values_Cont SET Fld_Value=@InlineEditValue WHERE FldDTLID=(SELECT CFW.FldDTLID FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Cont CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)
			END	
		ELSE
			BEGIN
				insert into CFW_FLD_Values_Cont (Fld_ID,Fld_Value,RecId) VALUES (@numFormFieldId,@InlineEditValue,@numRecId)         
			END
			
			--Added By :Sachin Sadhu ||Date:13thOct2014
			--Purpose  :To manage CustomFields in Queue
			EXEC USP_CFW_FLD_Values_Cont_CT
					@numDomainID =@numDomainID,
					@numUserCntID =@numUserCntID,
					@numRecordID =@numRecId ,
					@numFormFieldId  =@numFormFieldId

	END
	ELSE IF @vcPageName='project'
	BEGIN 
		IF (SELECT COUNT(*) FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Pro CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)>0
			BEGIN
				UPDATE CFW_FLD_Values_Pro SET Fld_Value=@InlineEditValue WHERE FldDTLID=(SELECT CFW.FldDTLID FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Pro CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)
			END	
		ELSE
			BEGIN
				insert into CFW_FLD_Values_Pro (Fld_ID,Fld_Value,RecId) VALUES (@numFormFieldId,@InlineEditValue,@numRecId)         
			END

			
			--Added By :Sachin Sadhu ||Date:14thOct2014
			--Purpose  :To manage CustomFields in Queue
			EXEC USP_CFW_FLD_Values_Pro_CT
					@numDomainID =@numDomainID,
					@numUserCntID =@numUserCntID,
					@numRecordID =@numRecId ,
					@numFormFieldId  =@numFormFieldId
	END
	ELSE IF @vcPageName='opp'
	BEGIN 
		DECLARE @dtDate AS DATETIME = GETUTCDATE()

		IF (SELECT COUNT(*) FROM CFW_Fld_Master CFM JOIN CFW_Fld_Values_Opp CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)>0
		BEGIN
			UPDATE 
				CFW_Fld_Values_Opp 
			SET 
				Fld_Value=@InlineEditValue ,
				numModifiedBy = @numUserCntID,
				bintModifiedDate = @dtDate
			WHERE 
				FldDTLID= (SELECT CFW.FldDTLID FROM CFW_Fld_Master CFM JOIN CFW_Fld_Values_Opp CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)
		END	
		ELSE
		BEGIN
			INSERT INTO CFW_Fld_Values_Opp (Fld_ID,Fld_Value,RecId,numModifiedBy,bintModifiedDate) VALUES (@numFormFieldId,@InlineEditValue,@numRecId,@numUserCntID,@dtDate)         
		END

		UPDATE OpportunityMaster SET numModifiedBy=@numUserCntID,bintModifiedDate=@dtDate WHERE numOppID = @numRecId 

		--Added By :Sachin Sadhu ||Date:14thOct2014
		--Purpose  :To manage CustomFields in Queue
		EXEC USP_CFW_Fld_Values_Opp_CT
				@numDomainID =@numDomainID,
				@numUserCntID =@numUserCntID,
				@numRecordID =@numRecId ,
				@numFormFieldId  =@numFormFieldId

	END
	ELSE IF @vcPageName='oppitems'
	BEGIN 
		IF (SELECT COUNT(*) FROM CFW_Fld_Master CFM JOIN CFW_Fld_Values_OppItems CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)>0
		BEGIN
			UPDATE 
				CFW_Fld_Values_OppItems 
			SET 
				Fld_Value=@InlineEditValue
			WHERE 
				FldDTLID= (SELECT CFW.FldDTLID FROM CFW_Fld_Master CFM JOIN CFW_Fld_Values_OppItems CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)
		END	
		ELSE
		BEGIN
			INSERT INTO CFW_Fld_Values_OppItems(Fld_ID,Fld_Value,RecId) VALUES (@numFormFieldId,@InlineEditValue,@numRecId)         
		END
	END
	ELSE IF @vcPageName='case'
	BEGIN 
		IF (SELECT COUNT(*) FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Case CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)>0
			BEGIN
				UPDATE CFW_FLD_Values_Case SET Fld_Value=@InlineEditValue WHERE FldDTLID=(SELECT CFW.FldDTLID FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Case CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)
			END	
		ELSE
			BEGIN
				insert into CFW_FLD_Values_Case (Fld_ID,Fld_Value,RecId) VALUES (@numFormFieldId,@InlineEditValue,@numRecId)         
			END

			
			--Added By :Sachin Sadhu ||Date:14thOct2014
			--Purpose  :To manage CustomFields in Queue
			EXEC USP_CFW_FLD_Values_Case_CT
					@numDomainID =@numDomainID,
					@numUserCntID =@numUserCntID,
					@numRecordID =@numRecId ,
					@numFormFieldId  =@numFormFieldId
	END
	ELSE IF @vcPageName='item'
	BEGIN
		IF (SELECT COUNT(*) FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Item CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)>0
		BEGIN
			UPDATE CFW_FLD_Values_Item SET Fld_Value=@InlineEditValue WHERE FldDTLID=(SELECT CFW.FldDTLID FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Item CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)
		END	
		ELSE
		BEGIN
			INSERT INTO CFW_FLD_Values_Item (Fld_ID,Fld_Value,RecId) VALUES (@numFormFieldId,@InlineEditValue,@numRecId)         
		END
	END


	IF @vcAssociatedControlType = 'SelectBox'            
	BEGIN            
		    
		 SELECT vcData FROM ListDetails WHERE numListItemID=@InlineEditValue            
	END
	ELSE IF @vcAssociatedControlType = 'CheckBoxList'
	BEGIN
		DECLARE @str AS VARCHAR(MAX)
		SET @str = 'SELECT STUFF((SELECT CONCAT('','', vcData) FROM ListDetails WHERE numlistitemid IN (' + (CASE WHEN LEN(@InlineEditValue) > 0 THEN @InlineEditValue ELSE '0' END) + ') FOR XML PATH('''')), 1, 1, '''')'
		EXEC(@str)
	END
	ELSE IF @vcAssociatedControlType = 'CheckBox'
	BEGIN
	 	select CASE @InlineEditValue WHEN 1 THEN 'Yes' ELSE 'No' END AS vcData
	END
	ELSE
	BEGIN
		SELECT @InlineEditValue AS vcData
	END 
END

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_InLineEditPropertyName')
DROP PROCEDURE USP_InLineEditPropertyName
GO
CREATE PROCEDURE [dbo].[USP_InLineEditPropertyName] 
( 
 @numDomainID NUMERIC(9),
 @numUserCntID NUMERIC(9),
 @numFormFieldId NUMERIC(9),
 @bitCustom AS BIT = 0
 )
AS 
BEGIN
	IF @bitCustom =0
	BEGIN
		SELECT 
			vcDbColumnName
			,vcOrigDbColumnName
			,ISNULL(vcPropertyName,'') vcPropertyName
			,numListID
			,CASE 
				WHEN vcAssociatedControlType='SelectBox' 
				THEN ISNULL((SELECT numPrimaryListID FROM FieldRelationship WHERE numSecondaryListID=numListID and numDomainID=@numDomainId),0) 
				ELSE 0 
			END AS ListRelID
			,vcListItemType
		 FROM 
			DycFieldMaster 
		WHERE 
			numFieldId=@numFormFieldId
	END 
	ELSE
	BEGIN
		SELECT 
			'' AS vcDbColumnName
			,'' AS vcOrigDbColumnName
			,'' vcPropertyName
			,numListID
			,CASE 
				WHEN fld_type='SelectBox' 
				THEN ISNULL((SELECT numPrimaryListID FROM FieldRelationship WHERE numSecondaryListID=numListID AND numDomainID=@numDomainId),0) 
				ELSE 0 
			END AS ListRelID
		 FROM 
			CFW_Fld_Master 
		WHERE 
			Fld_id=@numFormFieldId 
	END
END
GO
GO
/****** Object:  StoredProcedure [dbo].[USP_ItemDetails]    Script Date: 02/07/2010 15:31:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Anoop Jayaraj                                          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_itemdetails')
DROP PROCEDURE usp_itemdetails
GO
CREATE PROCEDURE [dbo].[USP_ItemDetails]                                                  
@numItemCode as numeric(9) ,          
@ClientTimeZoneOffset as int,
@numDivisionId as numeric(18)                                               
AS
BEGIN 
   DECLARE @bitItemIsUsedInOrder AS BIT=0
   DECLARE @bitOppOrderExists AS BIT = 0
   
   DECLARE @numCompanyID NUMERIC(18,0)=0
   IF ISNULL(@numDivisionId,0) > 0
   BEGIN
		SELECT @numCompanyID=numCompanyID FROM DivisionMaster WHERE numDivisionID=@numDivisionId
   END

   DECLARE @numDomainId AS NUMERIC
   DECLARE @numItemGroup AS NUMERIC(18,0)
   SELECT @numDomainId=numDomainId,@numItemGroup=numItemGroup FROM Item WHERE numItemCode=@numItemCode
   
	
	IF (SELECT COUNT(OI.numOppId) FROM dbo.OpportunityItems OI WHERE OI.numItemCode =@numItemCode) > 0
	BEGIN
		SET @bitItemIsUsedInOrder=1
		SET @bitOppOrderExists = 1
	END
	
	IF (SELECT COUNT(OI.numOppId) FROM dbo.OpportunityKitItems OI WHERE OI.numChildItemID =@numItemCode) > 0
	BEGIN
		SET @bitItemIsUsedInOrder=1
		SET @bitOppOrderExists = 1
	END
	
	IF (SELECT COUNT(OI.numOppId) FROM dbo.OpportunityKitChildItems OI WHERE OI.numItemID =@numItemCode) > 0
	BEGIN
		SET @bitItemIsUsedInOrder=1
		SET @bitOppOrderExists = 1
	END
	
	IF EXISTS(SELECT numItemID FROM dbo.General_Journal_Details WHERE numItemID= @numItemCode and numDomainId=@numDomainId AND chBizDocItems='IA1')
	BEGIN
		SET @bitItemIsUsedInOrder =1
    END
	                                      
	SELECT 
		I.numItemCode, vcItemName, ISNULL(txtItemDesc,'') txtItemDesc,CAST(ISNULL(IED.txtDesc,'')  AS VARCHAR(MAX)) vcExtendedDescToAPI, charItemType, 
		CASE 
			WHEN charItemType='P' AND ISNULL(bitAssembly,0) = 1 then 'Assembly'
			WHEN charItemType='P' AND ISNULL(bitKitParent,0) = 1 then 'Kit'
			WHEN charItemType='P' AND ISNULL(bitAsset,0) = 1 AND ISNULL(bitRental,0)=0 then 'Asset'
			WHEN charItemType='P' AND ISNULL(bitAsset,0) = 1 AND ISNULL(bitRental,0)=1 then 'Rental Asset'
			WHEN charItemType='P' AND ISNULL(bitSerialized,0) = 1 AND ISNULL(bitAsset,0) = 0  then 'Serialized'
			WHEN charItemType='P' AND ISNULL(bitSerialized,0) = 1 AND ISNULL(bitAsset,0) = 1 AND ISNULL(bitRental,0)=0 then 'Serialized Asset'
			WHEN charItemType='P' AND ISNULL(bitSerialized,0) = 1 AND ISNULL(bitAsset,0) = 1 AND ISNULL(bitRental,0)=1 then 'Serialized Rental Asset'
			WHEN charItemType='P' AND ISNULL(bitLotNo,0)=1 THEN 'Lot #'
			ELSE CASE WHEN charItemType='P' THEN 'Inventory' ELSE '' END
		END AS InventoryItemType
		,dbo.fn_GetItemChildMembershipCount(@numDomainId,I.numItemCode) AS numChildMembershipCount
		,CASE WHEN ISNULL(bitAssembly,0) = 1 THEN dbo.fn_GetAssemblyPossibleWOQty(I.numItemCode,ISNULL(W.numWareHouseID,0)) ELSE 0 END AS numWOQty
		,CASE WHEN charItemType='P' THEN ISNULL((SELECT TOP 1 monWListPrice FROM WarehouseItems WHERE numDomainID=@numDomainID AND numItemID=@numItemCode AND ISNULL(monWListPrice,0) > 0),0) ELSE monListPrice END monListPrice,                   
		numItemClassification, isnull(bitTaxable,0) as bitTaxable, ISNULL(vcSKU,'') AS vcItemSKU, 
		(CASE WHEN I.numItemGroup > 0 THEN ISNULL(W.[vcWHSKU],'') ELSE ISNULL(vcSKU,'') END) AS vcSKU, 
		ISNULL(bitKitParent,0) as bitKitParent, V.numVendorID, V.monCost, I.numDomainID, I.numCreatedBy, I.bintCreatedDate, I.bintModifiedDate,I.numModifiedBy,
		(SELECT TOP 1 vcPathForImage FROM ItemImages WHERE numItemCode = @numItemCode AND bitDefault = 1 AND bitIsImage = 1 ) AS vcPathForImage ,
		(SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = @numItemCode AND bitDefault = 1 AND bitIsImage = 1) AS  vcPathForTImage
		,ISNULL(bitSerialized,0) as bitSerialized, vcModelID,                   
		(SELECT 
			numItemImageId
			,vcPathForImage
			,vcPathForTImage
			,bitDefault
			,CASE WHEN bitdefault=1 THEN -1 ELSE ISNULL(intDisplayOrder,0) END AS intDisplayOrder 
		FROM 
			ItemImages  
		WHERE 
			numItemCode=@numItemCode 
		ORDER BY 
			CASE WHEN bitdefault=1 THEN -1 ELSE isnull(intDisplayOrder,0) END ASC 
		FOR XML AUTO,ROOT('Images')) AS xmlItemImages,
		(SELECT STUFF((SELECT ',' + CONVERT(VARCHAR(50) , numCategoryID)  FROM dbo.ItemCategory WHERE numItemID = I.numItemCode FOR XML PATH('')), 1, 1, '')) AS vcItemCategories ,
		numItemGroup, 
		(CASE WHEN ISNULL(numItemGroup,0) > 0 AND (SELECT COUNT(*) FROM CFW_Fld_Master CFW JOIN ItemGroupsDTL ON numOppAccAttrID=Fld_id WHERE numItemGroupID=numItemGroup AND tintType=2) > 0 THEN 1 ELSE 0 END) bitItemGroupHasAttributes,
		numCOGsChartAcntId, numAssetChartAcntId, numIncomeChartAcntId, ISNULL(bitExpenseItem,0) bitExpenseItem, ISNULL(numExpenseChartAcntId,0) numExpenseChartAcntId, (CASE WHEN ISNULL(I.bitVirtualInventory,0) = 1 THEN 0 ELSE monAverageCost END) AS monAverageCost,                   
		monCampaignLabourCost,dbo.fn_GetContactName(I.numCreatedBy)+' ,'+convert(varchar(20),DateAdd(minute, -@ClientTimeZoneOffset,I.bintCreatedDate )) as CreatedBy ,                                      
		dbo.fn_GetContactName(I.numModifiedBy)+' ,'+convert(varchar(20),DateAdd(minute, -@ClientTimeZoneOffset,I.bintModifiedDate )) as ModifiedBy,                      
		sum(numOnHand) as numOnHand,                      
		sum(numOnOrder) as numOnOrder,                      
		MAX(numReorder)  as numReOrder,                      
		sum(numAllocation)  as numAllocation,                      
		sum(numBackOrder)  as numBackOrder,                   
		isnull(fltWeight,0) as fltWeight,                
		isnull(fltHeight,0) as fltHeight,                
		isnull(fltWidth,0) as fltWidth,                
		isnull(fltLength,0) as fltLength,                
		isnull(bitFreeShipping,0) as bitFreeShipping,              
		isnull(bitAllowBackOrder,0) as bitAllowBackOrder,              
		isnull(vcUnitofMeasure,'Units') as vcUnitofMeasure,      
		isnull(bitShowDeptItem,0) bitShowDeptItem,      
		isnull(bitShowDeptItemDesc,0) bitShowDeptItemDesc,  
		isnull(bitCalAmtBasedonDepItems ,0) bitCalAmtBasedonDepItems,
		isnull(bitAssembly ,0) bitAssembly ,
		isnull(cast(numBarCodeId as varchar(50)),'') as  numBarCodeId ,
		(CASE WHEN ISNULL(I.numManufacturer,0) > 0 THEN ISNULL((SELECT vcCompanyName FROM DivisionMaster INNER JOIN CompanyInfo ON DivisionMaster.numCompanyID=CompanyInfo.numCompanyId WHERE numDivisionID=I.numManufacturer),'')  ELSE ISNULL(I.vcManufacturer,'') END) AS vcManufacturer,
		ISNULL(I.numManufacturer,0) numManufacturer,
		ISNULL(I.numBaseUnit,0) AS numBaseUnit,ISNULL(I.numPurchaseUnit,0) AS numPurchaseUnit,ISNULL(I.numSaleUnit,0) AS numSaleUnit,
		dbo.fn_GetUOMName(ISNULL(I.numBaseUnit,0)) AS vcBaseUnit,dbo.fn_GetUOMName(ISNULL(I.numPurchaseUnit,0)) AS vcPurchaseUnit,dbo.fn_GetUOMName(ISNULL(I.numSaleUnit,0)) AS vcSaleUnit,
		isnull(bitLotNo,0) as bitLotNo,
		ISNULL(IsArchieve,0) AS IsArchieve,
		case when charItemType='P' then 'Inventory Item' when charItemType='S' then 'Service' when charItemType='A' then 'Accessory' when charItemType='N' then 'Non-Inventory Item' end as ItemType,
		isnull(I.numItemClass,0) as numItemClass,
		ISNULL(tintStandardProductIDType,0) tintStandardProductIDType,
		ISNULL(vcExportToAPI,'') vcExportToAPI,
		ISNULL(numShipClass,0) numShipClass,ISNULL(I.bitAllowDropShip,0) AS bitAllowDropShip,
		ISNULL(@bitItemIsUsedInOrder,0) AS bitItemIsUsedInOrder,
		ISNULL((SELECT COUNT(*) FROM ItemDetails WHERE numChildItemID = I.numItemCode),0) AS intParentKitItemCount,
		ISNULL(I.bitArchiveItem,0) AS [bitArchiveItem],
		ISNULL(W.numWareHouseID,0) AS [numWareHouseID],
		ISNULL(W.numWareHouseItemID,0) AS [numWareHouseItemID],
		ISNULL((SELECT COUNT(WI.[numWareHouseItemID]) FROM [dbo].[WareHouseItems] WI WHERE WI.[numItemID] = I.numItemCode AND WI.[numItemID] = @numItemCode),0) AS [intTransCount],
		ISNULL(I.bitAsset,0) AS [bitAsset],
		ISNULL(I.bitRental,0) AS [bitRental],
		ISNULL(W.vcBarCode,ISNULL(I.[numBarCodeId],'')) AS [vcBarCode],
		ISNULL((SELECT COUNT(*) FROM ItemUOMConversion WHERE numItemCode=I.numItemCode),0) AS tintUOMConvCount,
		ISNULL(I.bitVirtualInventory,0) bitVirtualInventory ,
		ISNULL(I.bitContainer,0) bitContainer
		,ISNULL(I.numContainer,0) numContainer
		,ISNULL(I.numNoItemIntoContainer,0) numNoItemIntoContainer
		,ISNULL(I.bitMatrix,0) AS bitMatrix
		,ISNULL(@bitOppOrderExists,0) AS bitOppOrderExists
		,CPN.CustomerPartNo
		,ISNULL(I.vcASIN,'') AS vcASIN
		,ISNULL(I.tintKitAssemblyPriceBasedOn,1) AS tintKitAssemblyPriceBasedOn
		,ISNULL(I.fltReorderQty,0) AS fltReorderQty
		,ISNULL(bitSOWorkOrder,0) bitSOWorkOrder
		,ISNULL(bitKitSingleSelect,0) bitKitSingleSelect
		,ISNULL(I.numBusinessProcessId,0) numBusinessProcessId
	FROM 
		Item I       
	left join  
		WareHouseItems W                  
	on 
		W.numItemID=I.numItemCode                
	LEFT JOIN 
		ItemExtendedDetails IED 
	ON 
		I.numItemCode = IED.numItemCode
	LEFT JOIN 
		Vendor V 
	ON 
		I.numVendorID = V.numVendorID AND I.numItemCode = V.numItemCode  
	LEFT JOIN 
		CustomerPartNumber CPN 
	ON 
		I.numItemCode=CPN.numItemCode 
		AND CPN.numCompanyID=@numCompanyID
	WHERE 
		I.numItemCode=@numItemCode  
	GROUP BY 
		I.numItemCode, vcItemName, txtItemDesc, charItemType, monListPrice,                   
		numItemClassification, bitTaxable,vcSKU,[W].[vcWHSKU] , bitKitParent,V.numVendorID, V.monCost, I.numDomainID,               
		I.numCreatedBy, I.bintCreatedDate, I.bintModifiedDate,numContainer,   numNoItemIntoContainer,                
		I.numModifiedBy,bitAllowBackOrder, bitSerialized, vcModelID,                   
		numItemGroup, numCOGsChartAcntId, numAssetChartAcntId, numIncomeChartAcntId, monAverageCost, I.bitVirtualInventory, bitExpenseItem, numExpenseChartAcntId,                  
		monCampaignLabourCost,fltWeight,fltHeight,fltWidth,fltLength,bitFreeShipping,vcUnitofMeasure,bitShowDeptItemDesc,bitShowDeptItem,bitCalAmtBasedonDepItems,bitAssembly,numBarCodeId,I.vcManufacturer
		,I.numBaseUnit,I.numPurchaseUnit,I.numSaleUnit,I.bitContainer,I.bitLotNo,I.IsArchieve,I.numItemClass,I.tintStandardProductIDType,I.vcExportToAPI,I.numShipClass,
		CAST( ISNULL(IED.txtDesc,'')  AS VARCHAR(MAX) ),I.bitAllowDropShip,I.bitArchiveItem,I.bitAsset,I.bitRental,W.[numWarehouseID],W.numWareHouseItemID,W.[vcBarCode],I.bitMatrix
		,I.numManufacturer,CPN.CustomerPartNo,I.vcASIN,I.tintKitAssemblyPriceBasedOn,I.fltReorderQty,bitSOWorkOrder,bitKitSingleSelect,I.numBusinessProcessId
END
--SELECT * FROM Category
--- Alterd By Chintan Prajapati
-- EXEC dbo.USP_ItemsForECommerce 9,60,1,1,15,0,''
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ItemsForECommerce' ) 
    DROP PROCEDURE USP_ItemsForECommerce
GO
CREATE PROCEDURE [dbo].[USP_ItemsForECommerce]
    @numSiteID AS NUMERIC(9) = 0,
    @numCategoryID AS NUMERIC(18) = 0,
    @numWareHouseID AS NUMERIC(9) = 0,
    @SortBy NVARCHAR(MAX) = 0,
    @CurrentPage INT,
    @PageSize INT,
    @TotRecs INT = 0 OUTPUT,
    @SearchText AS NVARCHAR(MAX) = '',--Comma seperated item ids
    @FilterQuery AS NVARCHAR(MAX) = '',----Comma seperated Attributes Ids
    @FilterRegularCondition AS NVARCHAR(MAX),
    @FilterCustomFields AS NVARCHAR(MAX),
    @FilterCustomWhere AS NVARCHAR(MAX),
	@numDivisionID AS NUMERIC(18,0) = 0,
	@numManufacturerID AS NUMERIC(18,0)=0,
	@FilterItemAttributes AS NVARCHAR(MAX)
AS 
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	DECLARE @numDomainID AS NUMERIC(9)
	DECLARE @numDefaultRelationship AS NUMERIC(18,0)
	DECLARE @numDefaultProfile AS NUMERIC(18,0)
	DECLARE @tintPreLoginPriceLevel AS TINYINT
	DECLARE @tintPriceLevel AS TINYINT
	DECLARE @tintDisplayCategory AS TINYINT
	DECLARE @bitAutoSelectWarehouse AS BIT

	SELECT  
		@numDomainID = numDomainID
    FROM 
		[Sites]
    WHERE 
		numSiteID = @numSiteID
        
	IF ISNULL(@numDivisionID,0) > 0
	BEGIN
		SELECT 
			@numDefaultRelationship=ISNULL(numCompanyType,0)
			,@numDefaultProfile=ISNULL(vcProfile,0)
			,@tintPriceLevel=ISNULL(tintPriceLevel,0) 
		FROM 
			DivisionMaster 
		INNER JOIN 
			CompanyInfo 
		ON 
			DivisionMaster.numCompanyID=CompanyInfo.numCompanyId 
		WHERE 
			numDivisionID=@numDivisionID 
			AND DivisionMaster.numDomainID=@numDomainID
	END

	--KEEP THIS BELOW ABOVE IF CONDITION
	SELECT 
		@tintDisplayCategory = ISNULL(bitDisplayCategory,0)
		,@numDefaultRelationship=(CASE WHEN ISNULL(@numDivisionID,0) > 0 THEN @numDefaultRelationship ELSE numRelationshipId END)
		,@numDefaultProfile=(CASE WHEN ISNULL(@numDivisionID,0) > 0 THEN @numDefaultProfile ELSE numProfileId END)
		,@tintPreLoginPriceLevel=(CASE WHEN ISNULL(bitShowPriceUsingPriceLevel,0)=1 THEN (CASE WHEN @numDivisionID > 0 THEN (CASE WHEN ISNULL(bitShowPriceUsingPriceLevel,0)=1 THEN ISNULL(@tintPriceLevel,0) ELSE 0 END) ELSE ISNULL(tintPreLoginProceLevel,0) END) ELSE 0 END)
		,@bitAutoSelectWarehouse = ISNULL(bitAutoSelectWarehouse,0)
	FROM 
		dbo.eCommerceDTL 
	WHERE 
		numSiteID = @numSiteId
		
	CREATE TABLE #tmpItemCat(numCategoryID NUMERIC(18,0))
	CREATE TABLE #tmpItemCode(numItemCode NUMERIC(18,0))
		
	IF LEN(@FilterCustomFields) > 0 AND LEN(@FilterCustomWhere) > 0
	BEGIN			
		DECLARE @FilterCustomFieldIds AS NVARCHAR(MAX)
		SET @FilterCustomFieldIds = Replace(@FilterCustomFields,'[','')
		SET @FilterCustomFieldIds = Replace(@FilterCustomFieldIds,']','')
			
		DECLARE @strCustomSql AS NVARCHAR(MAX)
		SET @strCustomSql = 'INSERT INTO #tmpItemCode(numItemCode)
								SELECT DISTINCT T.RecId FROM 
														(
															SELECT * FROM 
																(
																	SELECT  t2.RecId, t1.fld_id, t2.Fld_Value FROM CFW_Fld_Master t1 
																	JOIN CFW_FLD_Values_Item AS t2 ON t1.Fld_ID = t2.Fld_ID 
																	WHERE t1.Grp_id  In (5,9)
																	AND t1.numDomainID = ' + CONVERT(VARCHAR(10),@numDomainID) + '
																	AND t1.fld_type <> ''Link'' 
																	AND t1.fld_id IN (' + @FilterCustomFieldIds + ')
																) p  PIVOT (MAX([Fld_Value]) FOR fld_id IN (' + @FilterCustomFields + ')) AS pvt 
														) T  WHERE 1=1 AND ' + @FilterCustomWhere
								 
		EXEC SP_EXECUTESQL @strCustomSql								 					 
	END
		
    DECLARE @strSQL NVARCHAR(MAX)
    DECLARE @firstRec AS INTEGER
    DECLARE @lastRec AS INTEGER
    DECLARE @Where NVARCHAR(MAX)
    DECLARE @SortString NVARCHAR(MAX)
    DECLARE @searchPositionColumn NVARCHAR(MAX)
	DECLARE @searchPositionColumnGroupBy NVARCHAR(MAX)
    DECLARE @row AS INTEGER
    DECLARE @data AS VARCHAR(100)
    DECLARE @dynamicFilterQuery NVARCHAR(MAX)
    DECLARE @checkFldType VARCHAR(100)
    DECLARE @count NUMERIC(9,0)
    DECLARE @whereNumItemCode NVARCHAR(MAX)
    DECLARE @filterByNumItemCode VARCHAR(100)
        
	SET @SortString = ''
    set @searchPositionColumn = ''
	SET @searchPositionColumnGroupBy = ''
    SET @strSQL = ''
	SET @whereNumItemCode = ''
	SET @dynamicFilterQuery = ''
		 
    SET @firstRec = ( @CurrentPage - 1 ) * @PageSize
    SET @lastRec = ( @CurrentPage * @PageSize + 1 )
                
	DECLARE @vcWarehouseIDs AS VARCHAR(1000)
    IF (@numWareHouseID = 0 OR @bitAutoSelectWarehouse = 1)
    BEGIN
		IF @bitAutoSelectWarehouse = 1
		BEGIN
			SELECT @vcWarehouseIDs = ISNULL( STUFF((SELECT DISTINCT ',' + CAST(numWarehouseID AS VARCHAR(10)) FROM [dbo].[Warehouses] WHERE [Warehouses].[numDomainID] = @numDomainID FOR XML PATH('')),1, 1, '') , '') 
		END
		ELSE
		BEGIN
			SELECT 
				@numWareHouseID = ISNULL(numDefaultWareHouseID,0)
			FROM 
				[eCommerceDTL]
			WHERE 
				[numDomainID] = @numDomainID	

			SET @vcWarehouseIDs = @numWareHouseID
		END
    END
	ELSE
	BEGIN
		SET @vcWarehouseIDs = @numWareHouseID
	END

	--PRIAMRY SORTING USING CART DROPDOWN
	DECLARE @Join AS NVARCHAR(MAX)
	DECLARE @SortFields AS NVARCHAR(MAX)
	DECLARE @vcSort AS VARCHAR(MAX)

	SET @Join = ''
	SET @SortFields = ''
	SET @vcSort = ''

	IF @SortBy = ''
	BEGIN
		SET @SortString = ' vcItemName Asc '
	END
	ELSE IF CHARINDEX('~0',@SortBy) > 0 OR CHARINDEX('~1',@SortBy) > 0
	BEGIN
		DECLARE @fldID AS NUMERIC(18,0)
		DECLARE @RowNumber AS VARCHAR(MAX)
				
		SELECT * INTO #fldValues FROM dbo.SplitIDsWithPosition(@SortBy,'~')
		SELECT @RowNumber = RowNumber, @fldID = id FROM #fldValues WHERE RowNumber = 1
		SELECT @vcSort = CASE WHEN Id = 0 THEN 'ASC' ELSE 'DESC' END FROM #fldValues WHERE RowNumber = 2
				
		SET @Join = @Join + ' LEFT JOIN CFW_FLD_Values_Item CFVI' + @RowNumber + ' ON I.numItemCode = CFVI' + @RowNumber + '.RecID AND CFVI' + @RowNumber + '.fld_Id = ' + CONVERT(VARCHAR(10),@fldID)
		---SET @SortString = 'CFVI' + @RowNumber + '.fld_Value' 
		SET @SortString = '[fld_Value' + @RowNumber + '] ' --+ @vcSort
		SET @SortFields = ', CFVI' + @RowNumber + '.fld_Value [fld_Value' + @RowNumber + ']'
	END
	ELSE
	BEGIN	
		IF CHARINDEX('monListPrice',@SortBy) > 0
		BEGIN
			DECLARE @bitSortPriceMode AS BIT
			SELECT @bitSortPriceMode = ISNULL(bitSortPriceMode,0) FROM dbo.eCommerceDTL WHERE numSiteId = @numSiteID
			
			IF @bitSortPriceMode = 1
			BEGIN
				SET @SortString = ' '
			END
			ELSE
			BEGIN
				SET @SortString = @SortBy	
			END
		END
		ELSE
		BEGIN
			SET @SortString = @SortBy	
		END	
	END	

	--SECONDARY SORTING USING DEFAULT SETTINGS FROM ECOMMERCE
	SELECT 
		ROW_NUMBER() OVER (ORDER BY Custom,vcFieldName) AS [RowID]
		,* 
	INTO 
		#tempSort 
	FROM 
	(
		SELECT 
			CONVERT(VARCHAR(9),numFieldID)+'~0' AS numFieldID,
			ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName,
			0 AS Custom,
			vcDbColumnName 
		FROM 
			View_DynamicColumns
		WHERE 
			numFormID = 84 
			AND ISNULL(bitSettingField,0) = 1 
			AND ISNULL(bitDeleted,0)=0 
			AND ISNULL(bitCustom,0) = 0
			AND numDomainID = @numDomainID	       
		UNION     
		SELECT 
			CONVERT(VARCHAR(9),numFieldID)+'~1' AS numFieldID,
			vcFieldName ,
			1 AS Custom,
			'' AS vcDbColumnName
		FROM 
			View_DynamicCustomColumns          
		WHERE 
			numFormID = 84
			AND numDomainID = @numDomainID
			AND grp_id IN (5,9)
			AND vcAssociatedControlType = 'TextBox' 
			AND ISNULL(bitCustom,0) = 1 
			AND tintPageType=1  
	) TABLE1 
	ORDER BY 
		Custom,vcFieldName
		
	--SELECT * FROM #tempSort
	DECLARE @DefaultSort AS NVARCHAR(MAX)
	DECLARE @DefaultSortFields AS NVARCHAR(MAX)
	DECLARE @DefaultSortJoin AS NVARCHAR(MAX)
	DECLARE @intCnt AS INT
	DECLARE @intTotalCnt AS INT	
	DECLARE @strColumnName AS VARCHAR(100)
	DECLARE @bitCustom AS BIT
	DECLARE @FieldID AS VARCHAR(100)				

	SET @DefaultSort = ''
	SET @DefaultSortFields = ''
	SET @DefaultSortJoin = ''		
	SET @intCnt = 0
	SET @intTotalCnt = ( SELECT COUNT(*) FROM #tempSort )
	SET @strColumnName = ''
		
	IF (SELECT ISNULL(bitEnableSecSorting,0) FROM EcommerceDtl WHERE numSiteID = @numSiteID ) = 1
	BEGIN
		CREATE TABLE #fldDefaultValues(RowNumber INT,Id VARCHAR(1000))

		WHILE(@intCnt < @intTotalCnt)
		BEGIN
			SET @intCnt = @intCnt + 1
			SELECT @FieldID = numFieldID, @strColumnName = vcDbColumnName, @bitCustom = Custom FROM #tempSort WHERE RowID = @intCnt

			IF @bitCustom = 0
			BEGIN
				SET @DefaultSort =  @strColumnName + ',' + @DefaultSort 
			END
			ELSE
			BEGIN
				DECLARE @fldID1 AS NUMERIC(18,0)
				DECLARE @RowNumber1 AS VARCHAR(MAX)
				DECLARE @vcSort1 AS VARCHAR(10)
				DECLARE @vcSortBy AS VARCHAR(1000)
				SET @vcSortBy = CONVERT(VARCHAR(100),@FieldID) + '~' + '0'

				INSERT INTO #fldDefaultValues(RowNumber,Id) SELECT RowNumber, id FROM dbo.SplitIDsWithPosition(@vcSortBy,'~')
				SELECT @RowNumber1 = RowNumber + 100 + @intCnt, @fldID1 = id FROM #fldDefaultValues WHERE RowNumber = 1
				SELECT @vcSort1 = 'ASC' 
						
				SET @DefaultSortJoin = @DefaultSortJoin + ' LEFT JOIN CFW_FLD_Values_Item CFVI' + @RowNumber1 + ' ON I.numItemCode = CFVI' + @RowNumber1 + '.RecID AND CFVI' + @RowNumber1 + '.fld_Id = ' + CONVERT(VARCHAR(10),@fldID1)
				SET @DefaultSort = '[fld_Value' + @RowNumber1 + '] ' + ',' + @DefaultSort 
				SET @DefaultSortFields = @DefaultSortFields + ', CFVI' + @RowNumber1 + '.fld_Value [fld_Value' + @RowNumber1 + ']'
			END
		END

		IF LEN(@DefaultSort) > 0
		BEGIN
			SET  @DefaultSort = LEFT(@DefaultSort,LEN(@DefaultSort) - 1)
			SET @SortString = @SortString + ',' 
		END
	END
                                		
    IF LEN(@SearchText) > 0
	BEGIN
		IF CHARINDEX('SearchOrder',@SortString) > 0
		BEGIN
			SET @searchPositionColumn = ' ,SearchItems.RowNumber AS SearchOrder ' 
			SET @searchPositionColumnGroupBy = ' ,SearchItems.RowNumber ' 
		END

		SET @Where = ' INNER JOIN dbo.SplitIDsWithPosition('''+ @SearchText +''','','') AS SearchItems 
                        ON SearchItems.Id = I.numItemCode  and SC.numSiteID = ' + CONVERT(VARCHAR(15), @numSiteID) + ' WHERE ISNULL(IsArchieve,0) = 0'
	END
	ELSE IF @SearchText = ''
	BEGIN
		SET @Where = ' WHERE 1=2 AND I.numDomainID=' + CONVERT(VARCHAR(15), @numDomainID)
		+ ' and SC.numSiteID = ' + CONVERT(VARCHAR(15), @numSiteID)
		+ ' AND ISNULL(IsArchieve,0) = 0'
	END
	ELSE
	BEGIN
		SET @Where = CONCAT(' WHERE 1=1 AND I.numDomainID=',@numDomainID,' and SC.numSiteID = ',@numSiteID,' AND ISNULL(IsArchieve,0) = 0')

		IF @tintDisplayCategory = 1
		BEGIN
					
			;WITH CTE AS(
			SELECT C.numCategoryID,C.tintLevel,C.numDepCategory,CASE WHEN tintLevel=1 THEN 1 ELSE 0 END AS bitChild 
				FROM SiteCategories SC INNER JOIN Category C ON SC.numCategoryID=C.numCategoryID WHERE SC.numSiteID=@numSiteID AND (C.numCategoryID = @numCategoryID OR ISNULL(@numCategoryID,0) = 0) AND numDomainID = @numDomainID
			UNION ALL
			SELECT  C.numCategoryID,C.tintLevel,C.numDepCategory,CTE.bitChild FROM Category C JOIN CTE ON 
			C.numDepCategory=CTE.numCategoryID AND CTE.bitChild=1 AND numDomainID = @numDomainID)
					
			INSERT INTO #tmpItemCat(numCategoryID)												 
			SELECT DISTINCT C.numCategoryID FROM CTE C JOIN dbo.ItemCategory IC ON C.numCategoryID = IC.numCategoryID 
												WHERE numItemID IN ( 
																						
													 				SELECT numItemID FROM WareHouseItems WI
																	INNER JOIN dbo.Item I ON I.numItemCode = WI.numItemID AND I.numDomainID = @numDomainID
																	GROUP BY numItemID 
																	HAVING SUM(WI.numOnHand) > 0 OR SUM(WI.numAllocation) > 0
																	--UNION ALL
													 			--	SELECT numItemID FROM WareHouseItems WI
																	--INNER JOIN dbo.Item I ON I.numItemCode = WI.numItemID AND I.numDomainID = @numDomainID
																	--GROUP BY numItemID 
																	--HAVING SUM(WI.numAllocation) > 0
																	)
		END

	END

	SET @strSQL = @strSQL + CONCAT(' WITH Items AS 
									( 
											SELECT
											I.numDomainID
											,I.numItemCode
											,ROW_NUMBER() OVER ( Partition by I.numItemCode Order by I.numItemCode) RowID
											,I.bitMatrix
											,I.bitKitParent
											,I.numItemGroup
											,vcItemName
											,I.bintCreatedDate
											,vcManufacturer
											,ISNULL(txtItemDesc,'''') txtItemDesc
											,vcSKU
											,fltHeight
											,fltWidth
											,fltLength
											,vcModelID
											,fltWeight
											,numBaseUnit
											,numSaleUnit
											,numPurchaseUnit
											,bitFreeShipping
											,C.vcCategoryName
											,C.vcDescription as CategoryDesc
											,charItemType
											,monListPrice
											,bitAllowBackOrder
											,bitShowInStock
											,numVendorID
											,numItemClassification',@searchPositionColumn,@SortFields,@DefaultSortFields,'
											,SUM(numOnHand) numOnHand
											,SUM(numAllocation) numAllocation
											FROM      
											Item AS I
											INNER JOIN eCommerceDTL E ON E.numDomainID = I.numDomainID AND E.numSiteId=',@numSiteID,'
											LEFT JOIN WareHouseItems WI ON WI.numDomainID=' + CONVERT(VARCHAR(10),@numDomainID) + ' AND WI.numItemID=I.numItemCode AND numWareHouseID IN (SELECT [OutParam] FROM dbo.[SplitString](''' + @vcWarehouseIDs + ''','',''))
											INNER JOIN ItemCategory IC ON I.numItemCode = IC.numItemID
											INNER JOIN Category C ON IC.numCategoryID = C.numCategoryID
											INNER JOIN SiteCategories SC ON IC.numCategoryID = SC.numCategoryID 
											',@Join,' ',@DefaultSortJoin)

	IF LEN(ISNULL(@FilterItemAttributes,'')) > 0
	BEGIN
		SET @strSQL = @strSQL + @FilterItemAttributes
	END

	IF LEN(@FilterCustomFields) > 0 AND LEN(@FilterCustomWhere) > 0
	BEGIN
		SET @strSQL = @strSQL + ' INNER JOIN #tmpItemCode TP ON TP.numItemCode = I.numItemCode ' 
	END
			
	IF (SELECT COUNT(*) FROM #tmpItemCat) > 0
	BEGIN
		SET @strSQL = @strSQL + ' INNER JOIN #tmpItemCat TC ON TC.numCategoryID = IC.numCategoryID '
	END
	ELSE IF @numManufacturerID>0
	BEGIN
		SET @Where = REPLACE(@Where,'WHERE',' WHERE I.numManufacturer = ' + CONVERT(VARCHAR(10),@numManufacturerID) + ' AND ')
	END 
	ELSE IF @numCategoryID>0
	BEGIN
		SET @Where = REPLACE(@Where,'WHERE',' WHERE IC.numCategoryID = ' + CONVERT(VARCHAR(10),@numCategoryID) + ' AND ')
	END 
			
	IF (SELECT COUNT(*) FROM [dbo].[EcommerceRelationshipProfile] WHERE numSiteID=@numSiteID AND numRelationship = @numDefaultRelationship AND numProfile = @numDefaultProfile) > 0
	BEGIN
		SET @Where = @Where + CONCAT(' AND I.numItemClassification NOT IN (SELECT numItemClassification FROM EcommerceRelationshipProfile ERP INNER JOIN ECommerceItemClassification EIC ON EIC.numECommRelatiionshipProfileID=ERP.ID WHERE ERP.numSiteID=',@numSiteID,' AND ERP.numRelationship=',@numDefaultRelationship,' AND ERP.numProfile=', @numDefaultProfile,')')
	END

	SET @strSQL = @strSQL + @Where
	
	IF LEN(@FilterRegularCondition) > 0
	BEGIN
		SET @strSQL = @strSQL + @FilterRegularCondition
	END                                         

    IF LEN(@whereNumItemCode) > 0
    BEGIN
			SET @strSQL = @strSQL + @filterByNumItemCode	
	END

    SET @strSQL = CONCAT(@strSQL,' GROUP BY
										I.numDomainID
										,I.numItemCode
										,I.bitMatrix
										,I.bitKitParent
										,I.numItemGroup
										,vcItemName
										,I.bintCreatedDate
										,vcManufacturer
										,txtItemDesc
										,vcSKU
										,fltHeight
										,fltWidth
										,fltLength
										,vcModelID
										,fltWeight
										,numBaseUnit
										,numSaleUnit
										,numPurchaseUnit
										,bitFreeShipping
										,C.vcCategoryName
										,C.vcDescription
										,charItemType
										,monListPrice
										,bitAllowBackOrder
										,bitShowInStock
										,numVendorID
										,numItemClassification',@searchPositionColumnGroupBy,@SortFields,@DefaultSortFields,(CASE WHEN ISNULL(@tintDisplayCategory,0) = 1 THEN ' HAVING 1 = (CASE WHEN (I.charItemType=''P'' AND ISNULL(I.bitKitParent,0) = 0) THEN CASE WHEN (ISNULL(SUM(numOnHand), 0) > 0 OR ISNULL(SUM(numAllocation), 0) > 0) THEN 1 ELSE 0 END ELSE 1 END) ' ELSE '' END), ') SELECT * INTO #TEMPItems FROM Items;')
                                        
	IF @numDomainID IN (204,214)
	BEGIN
		SET @strSQL = @strSQL + 'DELETE FROM 
									#TEMPItems
								WHERE 
									numItemCode IN (
														SELECT 
															F.numItemCode
														FROM 
															#TEMPItems AS F
														WHERE 
															ISNULL(F.bitMatrix,0) = 1 
															AND EXISTS (
																		SELECT 
																			t1.vcItemName, t1.bitMatrix, t1.numItemGroup, Count(t1.numItemCode)
																		FROM 
																			#TEMPItems t1
																		WHERE 
																			t1.vcItemName = F.vcItemName
																			AND t1.bitMatrix = 1
																			AND t1.numItemGroup = F.numItemGroup
																		GROUP BY 
																			t1.vcItemName, t1.bitMatrix, t1.numItemGroup
																		HAVING 
																			Count(t1.numItemCode) > 1
																	)
													)
									AND numItemCode NOT IN (
																			SELECT 
																				Min(numItemCode)
																			FROM 
																				#TEMPItems AS F
																			WHERE
																				ISNULL(F.bitMatrix,0) = 1 
																				AND Exists (
																							SELECT 
																								t2.vcItemName, t2.bitMatrix, t2.numItemGroup, Count(t2.numItemCode)
																							FROM 
																								#TEMPItems t2
																							WHERE 
																								t2.vcItemName = F.vcItemName
																								AND t2.bitMatrix = 1
																								AND t2.numItemGroup = F.numItemGroup
																							GROUP BY 
																								t2.vcItemName, t2.bitMatrix, t2.numItemGroup
																							HAVING 
																								Count(t2.numItemCode) > 1
																						)
																			GROUP BY 
																				vcItemName, bitMatrix, numItemGroup
																		);'

	END
									  
									  
	SET @strSQL = @strSQL + ' WITH ItemSorted AS (SELECT  ROW_NUMBER() OVER ( ORDER BY  ' + @SortString + @DefaultSort + ' ' + @vcSort + ' ) AS Rownumber,* FROM  #TEMPItems WHERE RowID = 1)'

    DECLARE @fldList AS NVARCHAR(MAX)
	DECLARE @fldList1 AS NVARCHAR(MAX)
	SELECT @fldList=COALESCE(@fldList + ',','') + QUOTENAME(REPLACE(Fld_label,' ','') + '_C') FROM dbo.CFW_Fld_Master WHERE numDomainID=@numDomainID AND Grp_id=5
	SELECT @fldList1=COALESCE(@fldList1 + ',','') + QUOTENAME(REPLACE(Fld_label,' ','') + '_' + CAST(Fld_Id AS VARCHAR)) FROM dbo.CFW_Fld_Master WHERE numDomainID=@numDomainID AND Grp_id=9
            
    SET @strSQL = @strSQL + 'SELECT * INTO #tmpPagedItems FROM ItemSorted WHERE Rownumber > ' + CONVERT(VARCHAR(10), @firstRec) + '
                                AND   Rownumber < ' + CONVERT(VARCHAR(10), @lastRec) + '
                                order by Rownumber '
                                                                  

	SET @strSQL = CONCAT(@strSQL,'SELECT  
								Rownumber
								,I.numItemCode
								,vcCategoryName
								,CategoryDesc
								,vcItemName
								,txtItemDesc
								,vcManufacturer
								,vcSKU
								,fltHeight
								,fltWidth
								,fltLength
								,vcModelID
								,fltWeight
								,bitFreeShipping
								,', 
									(CASE 
									WHEN @tintPreLoginPriceLevel > 0 
									THEN 'ISNULL(PricingOption.monPrice,ISNULL(CASE WHEN I.[charItemType] = ''P'' THEN ( UOM * ISNULL(W1.[monWListPrice],0)) ELSE (UOM * monListPrice) END, 0))' 
									ELSE 'ISNULL(CASE WHEN I.[charItemType] = ''P'' THEN ( UOM * ISNULL(W1.[monWListPrice],0)) ELSE (UOM * monListPrice) END, 0)' 
									END)
									,' AS monListPrice,
									ISNULL(CASE WHEN I.[charItemType] = ''P'' THEN ( UOM * ISNULL(W1.[monWListPrice],0)) ELSE (UOM * monListPrice) END, 0) monMSRP					   
									',(CASE WHEN @numDomainID = 172 THEN ',ISNULL(CASE 
											WHEN tintRuleType = 1 AND tintDiscountType = 1
											THEN ISNULL((CASE WHEN I.[charItemType] = ''P'' THEN ( UOM * ISNULL(W1.[monWListPrice],0)) ELSE (UOM * monListPrice) END),0) - (ISNULL((CASE WHEN I.[charItemType] = ''P'' THEN ( UOM * ISNULL(W1.[monWListPrice],0)) ELSE (UOM * monListPrice) END),0) * ( decDiscount / 100 ) )
											WHEN tintRuleType = 1 AND tintDiscountType = 2
											THEN ISNULL((CASE WHEN I.[charItemType] = ''P'' THEN ( UOM * ISNULL(W1.[monWListPrice],0)) ELSE (UOM * monListPrice) END),0) - decDiscount
											WHEN tintRuleType = 2 AND tintDiscountType = 1
											THEN ISNULL(Vendor.monCost,0) + (ISNULL(Vendor.monCost,0) * ( decDiscount / 100 ) )
											WHEN tintRuleType = 2 AND tintDiscountType = 2
											THEN ISNULL(Vendor.monCost,0) + decDiscount
											WHEN tintRuleType = 3
											THEN decDiscount
										END, 0) AS monFirstPriceLevelPrice,ISNULL(CASE 
										WHEN tintRuleType = 1 AND tintDiscountType = 1 
										THEN decDiscount 
										WHEN tintRuleType = 1 AND tintDiscountType = 2
										THEN (decDiscount * 100) / (ISNULL((CASE WHEN I.[charItemType] = ''P'' THEN ( UOM * ISNULL(W1.[monWListPrice],0)) ELSE (UOM * monListPrice) END),0) - decDiscount)
										WHEN tintRuleType = 2 AND tintDiscountType = 1
										THEN decDiscount
										WHEN tintRuleType = 2 AND tintDiscountType = 2
										THEN (decDiscount * 100) / (ISNULL(Vendor.monCost,0) + decDiscount)
										WHEN tintRuleType = 3
										THEN 0
									END,'''') AS fltFirstPriceLevelDiscount' ELSE ',0 AS monFirstPriceLevelPrice, 0 AS fltFirstPriceLevelDiscount' END), '  
								,vcPathForImage
								,vcpathForTImage
								,UOM AS UOMConversionFactor
								,UOMPurchase AS UOMPurchaseConversionFactor
								,(CASE WHEN charItemType <> ''S'' AND charItemType <> ''N'' AND ISNULL(bitKitParent,0)=0
											THEN (CASE 
													WHEN bitAllowBackOrder = 1 THEN 1
													WHEN (ISNULL(I.numOnHand,0)<=0) THEN 0
													ELSE 1
												END)
											ELSE 1
								END) AS bitInStock,
								(
									CASE WHEN ISNULL(bitKitParent,0) = 1
									THEN
										(CASE 
											WHEN ((SELECT COUNT(*) FROM ItemDetails ID INNER JOIN Item IInner ON ID.numChildItemID = IInner.numItemCode WHERE ID.numItemKitID = I.numItemCode AND ISNULL(IInner.bitKitParent,0) = 1)) > 0 
											THEN 1 
											ELSE 0 
										END)
									ELSE
										0
									END
								) bitHasChildKits
								,(CASE WHEN charItemType <> ''S'' AND charItemType <> ''N''
											THEN ( CASE WHEN bitShowInStock = 1
														THEN (CASE 
																WHEN bitAllowBackOrder = 1 AND I.numDomainID <> 172 AND ISNULL(I.numOnHand,0)<=0 AND ISNULL(I.numVendorID,0) > 0 AND dbo.GetVendorPreferredMethodLeadTime(I.numVendorID) > 0 THEN CONCAT(''<font class="vendorLeadTime" style="color:green">Usually ships in '',dbo.GetVendorPreferredMethodLeadTime(I.numVendorID),'' days</font>'')
																WHEN bitAllowBackOrder = 1 AND ISNULL(I.numOnHand,0)<=0 THEN (CASE WHEN I.numDomainID = 172 THEN ''<font style="color:green">AVAILABLE</font>'' ELSE ''<font style="color:green">In Stock</font>'' END)
																WHEN (ISNULL(I.numOnHand,0)<=0) THEN (CASE WHEN I.numDomainID = 172 THEN ''<font style="color:red">ON HOLD</font>'' ELSE ''<font style="color:red">Out Of Stock</font>'' END)
																ELSE (CASE WHEN I.numDomainID = 172 THEN ''<font style="color:green">AVAILABLE</font>'' ELSE ''<font style="color:green">In Stock</font>'' END)  
															END)
														ELSE ''''
													END )
											ELSE ''''
										END ) AS InStock
								, ISNULL(W1.numWareHouseItemID,0) [numWareHouseItemID]
								,ISNULL(TablePromotion.numTotalPromotions,0) numTotalPromotions
								,ISNULL(TablePromotion.vcPromoDesc,'''') vcPromoDesc
								,ISNULL(TablePromotion.bitRequireCouponCode,0) bitRequireCouponCode
								',(CASE WHEN LEN(ISNULL(@fldList,'')) > 0 THEN CONCAT(',',@fldList) ELSE '' END),(CASE WHEN LEN(ISNULL(@fldList1,'')) > 0 THEN CONCAT(',',@fldList1) ELSE '' END),'
                            FROM 
								#tmpPagedItems I
							LEFT JOIN ItemImages IM ON I.numItemCode = IM.numItemCode AND IM.bitdefault=1 AND IM.bitIsImage = 1
							OUTER APPLY (SELECT * FROM dbo.fn_GetItemPromotions(',@numDomainID,',',@numDivisionID,',I.numItemCode,I.numItemClassification',',',@numDefaultRelationship,',',@numDefaultProfile,',1)) TablePromotion ',
							(CASE 
								WHEN @tintPreLoginPriceLevel > 0 
								THEN CONCAT(' OUTER APPLY(SELECT decDiscount AS monPrice FROM PricingTable WHERE tintRuleType=3 AND numItemCode=I.numItemCode ORDER BY numPricingID OFFSET ',CAST(@tintPreLoginPriceLevel-1 AS VARCHAR),' ROWS FETCH NEXT 1 ROWS ONLY) As PricingOption ' )
								ELSE '' 
							END),(CASE 
								WHEN @numDomainID = 172
								THEN ' OUTER APPLY (SELECT TOP 1 * FROM PricingTable WHERE numItemCode=I.numItemCode) AS PT LEFT JOIN Vendor ON Vendor.numVendorID =I.numVendorID AND Vendor.numItemCode=I.numItemCode'
								ELSE '' 
							END),' 
							OUTER APPLY (SELECT TOP 1 numWareHouseItemID,monWListPrice FROM WareHouseItems W 
											WHERE  W.numItemID = I.numItemCode 
											AND W.numDomainID = ',CONVERT(VARCHAR(10),@numDomainID),'
											AND numWareHouseID IN (SELECT [OutParam] FROM dbo.[SplitString](''' + @vcWarehouseIDs + ''','',''))) AS W1 			  
								CROSS APPLY (SELECT dbo.fn_UOMConversion(ISNULL(I.numSaleUnit, 0),I.numItemCode, I.numDomainId,ISNULL(I.numBaseUnit, 0)) AS UOM) AS UOM
								CROSS APPLY (SELECT dbo.fn_UOMConversion(ISNULL(I.numPurchaseUnit, 0),I.numItemCode, I.numDomainId,ISNULL(I.numBaseUnit, 0)) AS UOMPurchase) AS UOMPurchase')
			
	IF LEN(ISNULL(@fldList,'')) > 0
	BEGIN
		SET @strSQL = @strSQL +' left join (
											SELECT  t2.RecId, REPLACE(t1.Fld_label,'' '','''') + ''_C'' as Fld_label, 
												CASE t1.fld_type WHEN ''SelectBox'' THEN isnull(dbo.fn_GetListItemName(t2.Fld_Value),'''')
																	WHEN ''CheckBox'' THEN isnull(dbo.fn_getCustomDataValue(t2.Fld_Value,9),'''')
																	WHEN ''DateField'' THEN dbo.FormatedDateFromDate(t2.Fld_Value,' + CAST(@numDomainId AS VARCHAR(18)) +')
																	ELSE Replace(ISNULL(t2.Fld_Value,''0''),''$'',''$$'') end AS Fld_Value FROM CFW_Fld_Master t1 
											JOIN CFW_FLD_Values_Item AS t2 ON t1.Fld_ID = t2.Fld_ID where t1.Grp_id = 5 and t1.numDomainID=' + CAST(@numDomainId AS VARCHAR(18)) +' AND t1.fld_type <> ''Link''
											AND t2.RecId IN (SELECT numItemCode FROM #tmpPagedItems )
											) p PIVOT (MAX([Fld_Value]) FOR Fld_label IN (' + @fldList + ')) AS pvt on I.numItemCode=pvt.RecId' 
	END

	IF LEN(ISNULL(@fldList1,'')) > 0
	BEGIN
					
		SET @strSQL = @strSQL +' left join (
											SELECT  t2.numItemCode AS RecId, REPLACE(t1.Fld_label,'' '','''') + ''_'' + CAST(t1.Fld_Id AS VARCHAR) as Fld_label, 
												CASE t1.fld_type WHEN ''SelectBox'' THEN isnull(dbo.fn_GetListItemName(t2.Fld_Value),'''')
																	WHEN ''CheckBox'' THEN isnull(dbo.fn_getCustomDataValue(t2.Fld_Value,9),'''')
																	WHEN ''DateField'' THEN dbo.FormatedDateFromDate(t2.Fld_Value,' + CAST(@numDomainId AS VARCHAR(18)) +')
																	ELSE Replace(ISNULL(t2.Fld_Value,''0''),''$'',''$$'') end AS Fld_Value FROM CFW_Fld_Master t1 
											JOIN ItemAttributes AS t2 ON t1.Fld_ID = t2.Fld_ID where t1.Grp_id = 9 and t1.numDomainID=' + CAST(@numDomainId AS VARCHAR(18)) +' AND t1.fld_type <> ''Link''
											AND t2.numItemCode IN (SELECT numItemCode FROM #tmpPagedItems )
											) p PIVOT (MAX([Fld_Value]) FOR Fld_label IN (' + @fldList1 + ')) AS pvt1 on I.numItemCode=pvt1.RecId'
	END
	ELSE
	BEGIN
		SET @strSQL = REPLACE(@strSQL, ',##FLDLIST1##', '');
	END

	SET  @strSQL = @strSQL +' order by Rownumber';                
    SET @strSQL = @strSQL + ' SELECT @TotRecs = COUNT(*) FROM #TEMPItems WHERE RowID=1 '                            


	DECLARE @tmpSQL AS NVARCHAR(MAX)
	SET @tmpSQL = @strSQL
	
	PRINT CAST(@tmpSQL AS NTEXT)
	EXEC sp_executeSQL @strSQL, N'@TotRecs INT OUTPUT', @TotRecs OUTPUT
             
             
    DECLARE @tintOrder AS TINYINT                                                  
	DECLARE @vcFormFieldName AS VARCHAR(50)                                                  
	DECLARE @vcListItemType AS VARCHAR(1)                                             
	DECLARE @vcAssociatedControlType VARCHAR(10)                                                  
	DECLARE @numListID AS NUMERIC(9)                                                  
	DECLARE @WhereCondition VARCHAR(MAX)                       
	DECLARE @numFormFieldId AS NUMERIC  
	--DECLARE @vcFieldType CHAR(1)
	DECLARE @vcFieldType VARCHAR(15)
		                  
	SET @tintOrder=0                                                  
	SET @WhereCondition =''                 
                   
              
	CREATE TABLE #tempAvailableFields
	(
		numFormFieldId  NUMERIC(9)
		,vcFormFieldName NVARCHAR(50)
		,vcFieldType VARCHAR(15)
		,vcAssociatedControlType NVARCHAR(50)
		,numListID NUMERIC(18)
		,vcListItemType CHAR(1)
		,intRowNum INT
		,vcItemValue VARCHAR(3000)
	)
		   
	INSERT INTO #tempAvailableFields
	(
		numFormFieldId
		,vcFormFieldName
		,vcFieldType
		,vcAssociatedControlType
		,numListID
		,vcListItemType
		,intRowNum
	)                         
	SELECT 
		Fld_id
		,REPLACE(Fld_label,' ','') AS vcFormFieldName
		,CASE GRP_ID 
			WHEN 4 then 'C' 
			WHEN 1 THEN 'D'
			WHEN 9 THEN CONVERT(VARCHAR(15), Fld_id)
			ELSE 'C' 
		END AS vcFieldType
		,fld_type as vcAssociatedControlType
		,ISNULL(C.numListID, 0) numListID
		,CASE 
			WHEN C.numListID > 0 THEN 'L'
			ELSE ''
		END vcListItemType
		,ROW_NUMBER() OVER (ORDER BY Fld_id ASC) AS intRowNum
	FROM  
		CFW_Fld_Master C 
	LEFT JOIN 
		CFW_Validation V 
	ON 
		V.numFieldID = C.Fld_id
	WHERE 
		C.numDomainID = @numDomainId
		AND GRP_ID IN (5,9)

		  
	SELECT * FROM #tempAvailableFields
	
	IF OBJECT_ID('tempdb..#tempAvailableFields') IS NOT NULL
	DROP TABLE #tempAvailableFields
		
	IF OBJECT_ID('tempdb..#tmpItemCode') IS NOT NULL
	DROP TABLE #tmpItemCode
		
	IF OBJECT_ID('tempdb..#tmpItemCat') IS NOT NULL
	DROP TABLE #tmpItemCat
		
	IF OBJECT_ID('tempdb..#fldValues') IS NOT NULL
	DROP TABLE #fldValues
		
	IF OBJECT_ID('tempdb..#tempSort') IS NOT NULL
	DROP TABLE #tempSort
		
	IF OBJECT_ID('tempdb..#fldDefaultValues') IS NOT NULL
	DROP TABLE #fldDefaultValues

	IF OBJECT_ID('tempdb..#tmpPagedItems') IS NOT NULL
	DROP TABLE #tmpPagedItems


	IF OBJECT_ID('tempdb..#TEMPItems') IS NOT NULL
	DROP TABLE #TEMPItems
END
GO



GO
/****** Object:  StoredProcedure [dbo].[USP_LoadWarehouseBasedOnItem]    Script Date: 09/01/2009 19:12:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec USP_LoadWarehouseBasedOnItem 14    
--created by anoop jayaraj    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_loadwarehousebasedonitem')
DROP PROCEDURE usp_loadwarehousebasedonitem
GO
CREATE PROCEDURE [dbo].[USP_LoadWarehouseBasedOnItem]    
@numItemCode as varchar(20)=''  
as    
BEGIN
DECLARE @numDomainID AS NUMERIC(18,0)
DECLARE @numBaseUnit AS NUMERIC(18,0)
DECLARE @vcUnitName as VARCHAR(100) 
DECLARE @numSaleUnit AS NUMERIC(18,0)
DECLARE @vcSaleUnitName as VARCHAR(100) 
DECLARE @numPurchaseUnit AS NUMERIC(18,0)
DECLARE @vcPurchaseUnitName as VARCHAR(100) 
declare @bitSerialize as bit     
DECLARE @bitKitParent BIT
 
SELECT 
	@numDomainID = numDomainID,
	@numBaseUnit = ISNULL(numBaseUnit,0),
	@numSaleUnit = ISNULL(numSaleUnit,0),
	@numPurchaseUnit = ISNULL(numPurchaseUnit,0),
	@bitSerialize=bitSerialized,
	@bitKitParent = ( CASE WHEN ISNULL(bitKitParent,0) = 1
                               AND ISNULL(bitAssembly,0) = 1 THEN 0
                          WHEN ISNULL(bitKitParent,0) = 1 THEN 1
                          ELSE 0
                     END ) 
FROM 
	Item 
WHERE 
	numItemCode=@numItemCode    


SELECT @vcUnitName=ISNULL(vcUnitName,'-') FROM UOM u WHERE u.numUOMId=@numBaseUnit
SELECT @vcSaleUnitName=ISNULL(vcUnitName,'-') FROM UOM u WHERE u.numUOMId=@numSaleUnit
SELECT @vcPurchaseUnitName=ISNULL(vcUnitName,'-') FROM UOM u WHERE u.numUOMId=@numPurchaseUnit

DECLARE @numSaleUOMFactor AS DECIMAL(28,14)
DECLARE @numPurchaseUOMFactor AS DECIMAL(28,14)

SELECT @numSaleUOMFactor = dbo.fn_UOMConversion(@numSaleUnit,@numItemCode,@numDOmainID,@numBaseUnit)
SELECT @numPurchaseUOMFactor = dbo.fn_UOMConversion(@numPurchaseUnit,@numItemCode,@numDOmainID,@numBaseUnit)

SELECT 
	DISTINCT(WareHouseItems.numWareHouseItemId),
	ISNULL(WareHouseItems.numWLocationID,0) numWLocationID,
	CONCAT(vcWareHouse, (CASE WHEN WareHouseItems.numWLocationID = -1 THEN ' (Global)' ELSE (CASE WHEN ISNULL(WarehouseLocation.numWLocationID,0) > 0 THEN CONCAT(' (',WarehouseLocation.vcLocation,')') ELSE '' END) END)) AS vcWareHouse, 
	dbo.fn_GetAttributes(WareHouseItems.numWareHouseItemId,@bitSerialize) as Attr, 
	WareHouseItems.monWListPrice,
	CASE WHEN @bitKitParent=1 THEN dbo.fn_GetKitInventory(@numItemCode,WareHouseItems.numWareHouseID) else ISNULL(numOnHand,0) END numOnHand,
	CASE WHEN @bitKitParent=1 THEN 0 ELSE ISNULL(numOnOrder,0) END numOnOrder,
	CASE WHEN @bitKitParent=1 THEN 0 ELSE ISNULL(numReorder,0) END numReorder,
	CASE WHEN @bitKitParent=1 THEN 0 ELSE ISNULL(numAllocation,0) END numAllocation,
	CASE WHEN @bitKitParent=1 THEN 0 ELSE ISNULL(numBackOrder,0) END numBackOrder,
	Warehouses.numWareHouseID,
	@vcUnitName AS vcUnitName,
	@vcSaleUnitName AS vcSaleUnitName,
	@vcPurchaseUnitName AS vcPurchaseUnitName,
	CASE WHEN @numBaseUnit > 0 AND @numSaleUnit > 0
	THEN
		CASE WHEN @bitKitParent=1 THEN CAST(CAST(dbo.fn_GetKitInventory(@numItemCode,WareHouseItems.numWareHouseID)/@numSaleUOMFactor AS INT) AS VARCHAR) else CAST(CAST(ISNULL(numOnHand,0)/@numSaleUOMFactor AS INT) AS VARCHAR) END
	ELSE
		'-'
	END AS numOnHandUOM,
	CASE WHEN @numBaseUnit > 0 AND @numSaleUnit > 0
	THEN
		CASE WHEN @bitKitParent=1 THEN '0' else CAST(CAST(ISNULL(numOnOrder,0)/@numPurchaseUOMFactor AS INT) AS VARCHAR) END
	ELSE
		'-'
	END AS numOnOrderUOM,
	CASE WHEN @numBaseUnit > 0 AND @numSaleUnit > 0
	THEN
		CASE WHEN @bitKitParent=1 THEN '0' else CAST(CAST(ISNULL(numReorder,0)/@numSaleUOMFactor AS INT) AS VARCHAR) END
	ELSE
		'-'
	END AS numReorderUOM,
	CASE WHEN @numBaseUnit > 0 AND @numSaleUnit > 0
	THEN
		CASE WHEN @bitKitParent=1 THEN '0' else CAST(CAST(ISNULL(numAllocation,0)/@numSaleUOMFactor AS INT) AS VARCHAR) END
	ELSE
		'-'
	END AS numAllocationUOM,
	CASE WHEN @numBaseUnit > 0 AND @numSaleUnit > 0
	THEN
		CASE WHEN @bitKitParent=1 THEN '0' else CAST(CAST(ISNULL(numBackOrder,0)/@numSaleUOMFactor AS INT) AS VARCHAR) END
	ELSE
		'-'
	END AS numBackOrderUOM
FROM 
	WareHouseItems    
JOIN 
	Warehouses 
ON 
	Warehouses.numWareHouseID=WareHouseItems.numWareHouseID  
LEFT JOIN
	WarehouseLocation
ON
	WareHouseItems.numWLocationID = WarehouseLocation.numWLocationID
WHERE 
	WareHouseItems.numDomainID = @numDomainID
	AND numItemID=@numItemCode
ORDER BY
	numWareHouseItemId ASC
END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageBankReconcileMaster')
DROP PROCEDURE USP_ManageBankReconcileMaster
GO
CREATE PROCEDURE USP_ManageBankReconcileMaster
	@tintMode AS TINYINT, 
    @numDomainID numeric(18, 0),
    @numReconcileID numeric(18, 0),
    @numCreatedBy numeric(18, 0),
    @dtStatementDate datetime,
    @monBeginBalance DECIMAL(20,5),
    @monEndBalance DECIMAL(20,5),
    @numChartAcntId numeric(18, 0),
    @monServiceChargeAmount DECIMAL(20,5),
    @numServiceChargeChartAcntId numeric(18, 0),
    @dtServiceChargeDate datetime,
    @monInterestEarnedAmount DECIMAL(20,5),
    @numInterestEarnedChartAcntId numeric(18, 0),
    @dtInterestEarnedDate DATETIME,
    @bitReconcileComplete BIT=0,
	@vcFileName VARCHAR(300)='',
	@vcFileData VARCHAR(MAX) = ''
AS

--Validation of closed financial year
EXEC dbo.USP_ValidateFinancialYearClosingDate @numDomainID,@dtStatementDate
	
	IF @tintMode=1 --SELECT
	BEGIN
		DECLARE @numCreditAmt AS DECIMAL(20,5),@numDebitAmt AS DECIMAL(20,5) 
		
		SELECT 
			@numCreditAmt=ISNULL(SUM(numCreditAmt),0)
			,@numDebitAmt=ISNULL(SUM(numDebitAmt),0) 
		FROM 
			General_Journal_Details 
		WHERE 
			numDomainID=@numDomainID 
			AND [numReconcileID] = @numReconcileID 
			AND numChartAcntId=(SELECT numChartAcntId FROM [BankReconcileMaster] WHERE  numDomainID=@numDomainID AND [numReconcileID] = @numReconcileID)
			AND numJournalId NOT IN (SELECT numJournal_Id FROM General_Journal_Header WHERE numDomainId=@numDomainID AND numReconcileID=@numReconcileID AND ISNULL(bitReconcileInterest,0) = 0)
		OPTION (OPTIMIZE FOR (@numDomainID UNKNOWN))
	
		SELECT 
			[numReconcileID]
			,[numDomainID]
			,[numCreatedBy]
			,[dtCreatedDate]
			,[dtStatementDate]
			,[monBeginBalance]
			,[monEndBalance]
			,[numChartAcntId]
			,[monServiceChargeAmount]
			,[numServiceChargeChartAcntId]
			,[dtServiceChargeDate]
			,[monInterestEarnedAmount]
			,[numInterestEarnedChartAcntId]
			,[dtInterestEarnedDate]
			,[bitReconcileComplete]
			,[dtReconcileDate]
			,@numCreditAmt AS Payment
			,@numDebitAmt AS Deposit
			,dbo.fn_GetContactName(ISNULL(numCreatedBy,0)) AS vcReconciledby
			,dbo.fn_GetChart_Of_AccountsName(ISNULL(numChartAcntId,0)) AS vcAccountName,ISNULL(vcFileName,'') AS vcFileName
		FROM 
			[BankReconcileMaster] 
		WHERE 
			numDomainID=@numDomainID 
			AND [numReconcileID] = @numReconcileID
	END
	ELSE IF @tintMode=2 --Insert/Update
	BEGIN
		IF @numReconcileID>0 --Update
			BEGIN
				UPDATE [BankReconcileMaster]
					SET    [dtStatementDate] = @dtStatementDate, [monBeginBalance]=@monBeginBalance, [monEndBalance]=@monEndBalance, [numChartAcntId] = @numChartAcntId, [monServiceChargeAmount] = @monServiceChargeAmount, [numServiceChargeChartAcntId] = @numServiceChargeChartAcntId,dtServiceChargeDate=@dtServiceChargeDate, [monInterestEarnedAmount] = @monInterestEarnedAmount, [numInterestEarnedChartAcntId] = @numInterestEarnedChartAcntId,dtInterestEarnedDate=@dtInterestEarnedDate
					WHERE  [numDomainID] = @numDomainID AND [numReconcileID] = @numReconcileID
			END
		ELSE --Insert
			BEGIN
				INSERT INTO [BankReconcileMaster] ([numDomainID], [numCreatedBy], [dtCreatedDate], [dtStatementDate], [monBeginBalance], [monEndBalance], [numChartAcntId], [monServiceChargeAmount], [numServiceChargeChartAcntId],dtServiceChargeDate,[monInterestEarnedAmount], [numInterestEarnedChartAcntId],dtInterestEarnedDate,vcFileName)
				SELECT @numDomainID, @numCreatedBy, GETUTCDATE(), @dtStatementDate, @monBeginBalance, @monEndBalance, @numChartAcntId, @monServiceChargeAmount, @numServiceChargeChartAcntId,@dtServiceChargeDate,@monInterestEarnedAmount, @numInterestEarnedChartAcntId,@dtInterestEarnedDate,@vcFileName
			
				SET @numReconcileID=SCOPE_IDENTITY()


				IF LEN(@vcFileName) > 0
				BEGIN
					DECLARE @hDocItem int
					EXEC sp_xml_preparedocument @hDocItem OUTPUT, @vcFileData

					INSERT INTO BankReconcileFileData
					(
						[numDOmainID],
						[numReconcileID],
						[dtEntryDate],
						[vcReference],
						[fltAmount],
						[vcPayee],
						[vcDescription]
					)
					SELECT 
						@numDomainID,
						@numReconcileID,
						dtEntryDate,
						vcReference,
						fltAmount,
						vcPayee,
						vcDescription
					FROM 
						OPENXML (@hDocItem,'/BankStatement/Trasactions',2)                                                                          
					WITH                       
					(                                                                          
						dtEntryDate Date, vcReference VARCHAR(500), fltAmount FLOAT, vcPayee VARCHAR(1000),vcDescription VARCHAR(1000)
					)
				END
			END	
			
			SELECT [numReconcileID], [numDomainID], [numCreatedBy], [dtCreatedDate], [dtStatementDate], [monBeginBalance], [monEndBalance], [numChartAcntId], [monServiceChargeAmount], [numServiceChargeChartAcntId], [monInterestEarnedAmount], [numInterestEarnedChartAcntId] 
			FROM   [BankReconcileMaster] WHERE  numDomainID=@numDomainID AND [numReconcileID] = @numReconcileID
	   END
	 ELSE IF @tintMode=3 --Delete
	 BEGIN
	    --Delete Service Charge,Interest Earned and Adjustment entry
	    DELETE FROM General_Journal_Details WHERE numJournalId IN (SELECT numJournal_Id FROM General_Journal_Header WHERE numDomainID=@numDomainID AND ISNULL(numReconcileID,0) = @numReconcileID)
	    DELETE FROM General_Journal_Header WHERE numDomainID=@numDomainID AND ISNULL(numReconcileID,0) = @numReconcileID
	 
	    --Set Journal_Details entry to bitReconcile=0 & bitCleared=0
        UPDATE dbo.General_Journal_Details SET bitReconcile=0,bitCleared=0,numReconcileID=NULL WHERE numDomainID=@numDomainID AND [numReconcileID] = @numReconcileID
        
		DELETE FROM [BankReconcileFileData] WHERE numDomainID=@numDomainID AND [numReconcileID] = @numReconcileID

	 	DELETE FROM [BankReconcileMaster] WHERE numDomainID=@numDomainID AND [numReconcileID] = @numReconcileID
	 END
	 ELSE IF @tintMode=4 -- Select based on numChartAcntId and bitReconcileComplete
	 BEGIN
		SELECT *,(SELECT ISNULL(SUM(numCreditAmt),0) FROM General_Journal_Details WHERE numDomainID=@numDomainID AND [numReconcileID] = BRM.numReconcileID /*AND ISNULL(bitReconcile,0)=1*/ AND numChartAcntId=BRM.numChartAcntId) AS monPayment
		,(SELECT ISNULL(SUM(numDebitAmt),0) FROM General_Journal_Details WHERE numDomainID=@numDomainID AND [numReconcileID] = BRM.numReconcileID /*AND ISNULL(bitReconcile,0)=1*/ AND numChartAcntId=BRM.numChartAcntId) AS monDeposit
	      FROM BankReconcileMaster BRM WHERE  numDomainID=@numDomainID AND numChartAcntId=@numChartAcntId 
			AND ISNULL(bitReconcileComplete,0)=@bitReconcileComplete ORDER BY numReconcileID DESC 
	 END
	 ELSE IF @tintMode=5 -- Complete Bank Reconcile
	 BEGIN
	    UPDATE General_Journal_Details SET bitReconcile=1,bitCleared=0 WHERE numDomainID=@numDomainID AND [numReconcileID] = @numReconcileID

		UPDATE BankReconcileFileData SET bitReconcile=1,bitCleared=0 WHERE numDomainID=@numDomainID AND [numReconcileID] = @numReconcileID AND ISNULL(bitCleared,0)=1
	 
		UPDATE BankReconcileMaster SET bitReconcileComplete=1,dtReconcileDate=GETUTCDATE(),numCreatedBy=@numCreatedBy 
			WHERE numDomainID=@numDomainID AND [numReconcileID] = @numReconcileID
	 END
	ELSE IF @tintMode=6 -- Get Last added entry for numChartAcntId
	BEGIN
	SELECT TOP 1 *
	    FROM BankReconcileMaster BRM WHERE  numDomainID=@numDomainID AND numChartAcntId=@numChartAcntId 
		AND ISNULL(bitReconcileComplete,0)=1 ORDER BY numReconcileID DESC 
	END
	ELSE IF @tintMode = 7 -- Select based on numChartAcntId
	BEGIN
		SELECT *,(SELECT ISNULL(SUM(numCreditAmt),0) FROM General_Journal_Details WHERE numDomainID=@numDomainID AND [numReconcileID] = BRM.numReconcileID /*AND ISNULL(bitReconcile,0)=1*/ AND numChartAcntId=BRM.numChartAcntId) AS monPayment
		,(SELECT ISNULL(SUM(numDebitAmt),0) FROM General_Journal_Details WHERE numDomainID=@numDomainID AND [numReconcileID] = BRM.numReconcileID /*AND ISNULL(bitReconcile,0)=1*/ AND numChartAcntId=BRM.numChartAcntId) AS monDeposit
	      FROM BankReconcileMaster BRM WHERE  numDomainID=@numDomainID AND numChartAcntId=@numChartAcntId 
			ORDER BY numReconcileID DESC 
	END
    
GO
/****** Object:  StoredProcedure [dbo].[USP_ManageItemsAndKits]    Script Date: 02/15/2010 21:44:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Anoop Jayaraj                                                          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_manageitemsandkits')
DROP PROCEDURE usp_manageitemsandkits
GO
CREATE PROCEDURE [dbo].[USP_ManageItemsAndKits]                                                                
@numItemCode as numeric(9) output,                                                                
@vcItemName as varchar(300),                                                                
@txtItemDesc as varchar(2000),                                                                
@charItemType as char(1),                                                                
@monListPrice as DECIMAL(20,5),                                                                
@numItemClassification as numeric(9),                                                                
@bitTaxable as bit,                                                                
@vcSKU as varchar(50),                                                                                                                                      
@bitKitParent as bit,                                                                                   
--@dtDateEntered as datetime,                                                                
@numDomainID as numeric(9),                                                                
@numUserCntID as numeric(9),                                                                                                  
@numVendorID as numeric(9),                                                            
@bitSerialized as bit,                                               
@strFieldList as text,                                            
@vcModelID as varchar(200)='' ,                                            
@numItemGroup as numeric(9)=0,                                      
@numCOGSChartAcntId as numeric(9)=0,                                      
@numAssetChartAcntId as numeric(9)=0,                                      
@numIncomeChartAcntId as numeric(9)=0,                            
@monAverageCost as DECIMAL(20,5),                          
@monLabourCost as DECIMAL(20,5),                  
@fltWeight as float,                  
@fltHeight as float,                  
@fltLength as float,                  
@fltWidth as float,                  
@bitFreeshipping as bit,                
@bitAllowBackOrder as bit,              
@UnitofMeasure as varchar(30)='',              
@strChildItems as text,            
@bitShowDeptItem as bit,              
@bitShowDeptItemDesc as bit,        
@bitCalAmtBasedonDepItems as bit,    
@bitAssembly as BIT,
@intWebApiId INT=null,
@vcApiItemId AS VARCHAR(50)=null,
@numBarCodeId as VARCHAR(25)=null,
@vcManufacturer as varchar(250)=NULL,
@numBaseUnit AS NUMERIC(18)=0,
@numPurchaseUnit AS NUMERIC(18)=0,
@numSaleUnit AS NUMERIC(18)=0,
@bitLotNo as BIT,
@IsArchieve AS BIT,
@numItemClass as numeric(9)=0,
@tintStandardProductIDType AS TINYINT=0,
@vcExportToAPI VARCHAR(50),
@numShipClass NUMERIC,
@ProcedureCallFlag SMALLINT =0,
@vcCategories AS VARCHAR(2000) = '',
@bitAllowDropShip AS BIT=0,
@bitArchiveItem AS BIT = 0,
@bitAsset AS BIT = 0,
@bitRental AS BIT = 0,
@numCategoryProfileID AS NUMERIC(18,0) = 0,
@bitVirtualInventory BIT,
@bitContainer BIT,
@numContainer NUMERIC(18,0)=0,
@numNoItemIntoContainer NUMERIC(18,0),
@bitMatrix BIT = 0 ,
@vcItemAttributes VARCHAR(2000) = '',
@numManufacturer NUMERIC(18,0),
@vcASIN as varchar(50),
@tintKitAssemblyPriceBasedOn TINYINT = 1,
@fltReorderQty FLOAT = 0,
@bitSOWorkOrder BIT = 0,
@bitKitSingleSelect BIT = 0,
@bitExpenseItem BIT = 0,
@numExpenseChartAcntId NUMERIC(18,0) = 0,
@numBusinessProcessId NUMERIC(18,0)=0
AS
BEGIN     
	SET @vcManufacturer = CASE WHEN ISNULL(@numManufacturer,0) > 0 THEN ISNULL((SELECT vcCompanyName FROM DivisionMaster INNER JOIN CompanyInfo ON DivisionMaster.numCompanyID=CompanyInfo.numCompanyId WHERE numDivisionID=@numManufacturer),'')  ELSE ISNULL(@vcManufacturer,'') END

	DECLARE @bitAttributesChanged AS BIT  = 1
	DECLARE @bitOppOrderExists AS BIT = 0

	IF ISNULL(@numItemCode,0) > 0
	BEGIN
		IF (SELECT COUNT(OI.numOppId) FROM dbo.OpportunityItems OI WHERE OI.numItemCode =@numItemCode) > 0
		BEGIN
			SET @bitOppOrderExists = 1
		END
		ELSE IF (SELECT COUNT(OI.numOppId) FROM dbo.OpportunityKitItems OI WHERE OI.numChildItemID =@numItemCode) > 0
		BEGIN
			SET @bitOppOrderExists = 1
		END
		ELSE IF (SELECT COUNT(OI.numOppId) FROM dbo.OpportunityKitChildItems OI WHERE OI.numItemID =@numItemCode) > 0
		BEGIN
			SET @bitOppOrderExists = 1
		END
	END
	ELSE 
	BEGIN
		IF (ISNULL(@vcItemName,'') = '')
		BEGIN
			RAISERROR('ITEM_NAME_NOT_SELECTED',16,1)
			RETURN
		END
        ELSE IF (ISNULL(@charItemType,'') = '0')
		BEGIN
			RAISERROR('ITEM_TYPE_NOT_SELECTED',16,1)
			RETURN
		END
        ELSE
		BEGIN 
		    If (@charItemType = 'P' OR (@charItemType = 'S' AND ISNULL(@bitExpenseItem,0) = 1))
			BEGIN
				IF ((SELECT COUNT(*) FROM Chart_Of_Accounts WHERE numDomainId=@numDomainID AND numAccountId=@numAssetChartAcntId) = 0)
				BEGIN
					RAISERROR('INVALID_ASSET_ACCOUNT',16,1)
					RETURN
				END
			END

			-- This is common check for CharItemType P and Other
			IF ((SELECT COUNT(*) FROM Chart_Of_Accounts WHERE numDomainId=@numDomainID AND numAccountId=@numCOGSChartAcntId) = 0)
			BEGIN
				RAISERROR('INVALID_COGS_ACCOUNT',16,1)
				RETURN
			END

			IF ((SELECT COUNT(*) FROM Chart_Of_Accounts WHERE numDomainId=@numDomainID AND numAccountId=@numIncomeChartAcntId) = 0)
			BEGIN
				RAISERROR('INVALID_INCOME_ACCOUNT',16,1)
				RETURN
			END

			IF ISNULL(@bitExpenseItem,0) = 1
			BEGIN
				IF ((SELECT COUNT(*) FROM Chart_Of_Accounts WHERE numDomainId=@numDomainID AND numAccountId=@numExpenseChartAcntId) = 0)
				BEGIN
					RAISERROR('INVALID_EXPENSE_ACCOUNT',16,1)
					RETURN
				END
			END
		END
	END

	--INSERT CUSTOM FIELDS BASE ON WAREHOUSEITEMS
	DECLARE @hDoc AS INT     
	                                                                        	
	DECLARE @TempTable TABLE 
	(
		ID INT IDENTITY(1,1),
		Fld_ID NUMERIC(18,0),
		Fld_Value NUMERIC(18,0)
	)   

	IF ISNULL(@bitMatrix,0) = 1 AND ISNULL(@numItemGroup,0) = 0
	BEGIN
		RAISERROR('ITEM_GROUP_NOT_SELECTED',16,1)
		RETURN
	END
	ELSE IF ISNULL(@bitMatrix,0) = 1 AND LEN(ISNULL(@vcItemAttributes,'')) = 0 
	BEGIN
		RAISERROR('ITEM_ATTRIBUTES_NOT_SELECTED',16,1)
		RETURN
	END
	ELSE IF ISNULL(@bitMatrix,0) = 1 
	BEGIN
		DECLARE @bitDuplicate AS BIT = 0
	
		IF DATALENGTH(ISNULL(@vcItemAttributes,''))>2
		BEGIN
			SET @vcItemAttributes = CONCAT('<?xml version="1.0" encoding="iso-8859-1"?>',@vcItemAttributes)

			EXEC sp_xml_preparedocument @hDoc OUTPUT, @vcItemAttributes
	                                      
			INSERT INTO @TempTable 
			(
				Fld_ID,
				Fld_Value
			)    
			SELECT
				*          
			FROM 
				OPENXML (@hDoc,'/NewDataSet/CusFlds',2)                                                  
			WITH 
				(Fld_ID NUMERIC(18,0),Fld_Value NUMERIC(18,0)) 
			ORDER BY 
				Fld_ID       

			DECLARE @strAttribute VARCHAR(500) = ''
			DECLARE @i AS INT = 1
			DECLARE @COUNT AS INT 
			DECLARE @numFldID AS NUMERIC(18,0)
			DECLARE @numFldValue AS NUMERIC(18,0)
			SELECT @COUNT = COUNT(*) FROM @TempTable

			WHILE @i <= @COUNT
			BEGIN
				SELECT @numFldID=Fld_ID,@numFldValue=Fld_Value FROM @TempTable WHERE ID=@i

				IF ISNUMERIC(@numFldValue)=1
				BEGIN
					SELECT @strAttribute=@strAttribute+Fld_label FROM CFW_Fld_Master WHERE Fld_id=@numFldID AND Grp_id = 9
                
					IF LEN(@strAttribute) > 0
					BEGIN
						SELECT @strAttribute=@strAttribute+':'+ vcData +',' FROM ListDetails WHERE numListItemID=@numFldValue
					END
				END

				SET @i = @i + 1
			END

			SET @strAttribute = SUBSTRING(@strAttribute,0,LEN(@strAttribute))

			IF
				(SELECT
					COUNT(*)
				FROM
					Item
				INNER JOIN
					ItemAttributes
				ON
					Item.numItemCode = ItemAttributes.numItemCode
					AND ItemAttributes.numDomainID = @numDomainID                     
				WHERE
					Item.numItemCode <> @numItemCode
					AND Item.vcItemName = @vcItemName
					AND Item.numItemGroup = @numItemGroup
					AND Item.numDomainID = @numDomainID
					AND dbo.fn_GetItemAttributes(@numDomainID,Item.numItemCode) = @strAttribute
				) > 0
			BEGIN
				RAISERROR('ITEM_WITH_ATTRIBUTES_ALREADY_EXISTS',16,1)
				RETURN
			END

			If ISNULL(@numItemCode,0) > 0 AND dbo.fn_GetItemAttributes(@numDomainID,@numItemCode) = @strAttribute
			BEGIN
				SET @bitAttributesChanged = 0
			END

			EXEC sp_xml_removedocument @hDoc 
		END	
	END
	ELSE IF ISNULL(@bitMatrix,0) = 0
	BEGIN
		-- REMOVE ATTRIBUTES CONFIGURATION OF ITEM AND WAREHOUSE
		DELETE FROM ItemAttributes WHERE numDomainID=@numDomainID AND numItemCode=@numItemCode
	END

	IF ISNULL(@numItemGroup,0) > 0 AND ISNULL(@bitMatrix,0) = 1
	BEGIN
		IF (SELECT COUNT(*) FROM ItemGroupsDTL WHERE numItemGroupID = @numItemGroup AND tintType=2) = 0
		BEGIN
			RAISERROR('ITEM_GROUP_NOT_CONFIGURED',16,1)
			RETURN
		END
		ELSE IF EXISTS (SELECT 
							CFM.Fld_id
						FROM 
							CFW_Fld_Master CFM
						LEFT JOIN
							ListDetails LD
						ON
							CFM.numlistid = LD.numListID
						WHERE
							CFM.numDomainID = @numDomainID
							AND CFM.Fld_id IN (SELECT numOppAccAttrID FROM ItemGroupsDTL WHERE numItemGroupID=@numItemGroup AND tintType = 2)
						GROUP BY
							CFM.Fld_id HAVING COUNT(LD.numListItemID) = 0)
		BEGIN
			RAISERROR('ITEM_GROUP_NOT_CONFIGURED',16,1)
			RETURN
		END
	END

BEGIN TRY
BEGIN TRANSACTION

	

	IF @numCOGSChartAcntId=0 SET @numCOGSChartAcntId=NULL
	IF @numAssetChartAcntId=0 SET @numAssetChartAcntId=NULL  
	IF @numIncomeChartAcntId=0 SET @numIncomeChartAcntId=NULL     
	IF @numExpenseChartAcntId=0 SET @numExpenseChartAcntId=NULL

	DECLARE @ParentSKU VARCHAR(50) = @vcSKU                        
	DECLARE @ItemID AS NUMERIC(9)
	DECLARE @cnt AS INT

	IF ISNULL(@charItemType,'') = '' OR ISNULL(@charItemType,'') = '0'  
		SET @charItemType = 'N'

	IF @intWebApiId = 2 AND LEN(ISNULL(@vcApiItemId, '')) > 0 AND ISNULL(@numItemGroup, 0) > 0 
    BEGIN 
        -- check wether this id already Mapped in ITemAPI 
        SELECT 
			@cnt = COUNT([numItemID])
        FROM 
			[ItemAPI]
        WHERE 
			[WebApiId] = @intWebApiId
            AND [numDomainId] = @numDomainID
            AND [vcAPIItemID] = @vcApiItemId

        IF @cnt > 0 
        BEGIN
            SELECT 
				@ItemID = [numItemID]
            FROM 
				[ItemAPI]
            WHERE 
				[WebApiId] = @intWebApiId
                AND [numDomainId] = @numDomainID
                AND [vcAPIItemID] = @vcApiItemId
        
			--Update Existing Product coming from API
            SET @numItemCode = @ItemID
        END
    END
	ELSE IF @intWebApiId > 1 AND LEN(ISNULL(@vcSKU, '')) > 0 
    BEGIN
        SET @ParentSKU = @vcSKU
		 
        SELECT 
			@cnt = COUNT([numItemCode])
        FROM 
			[Item]
        WHERE 
			[numDomainId] = @numDomainID
            AND (vcSKU = @vcSKU OR @vcSKU IN (SELECT vcWHSKU FROM dbo.WareHouseItems WHERE numItemID = Item.[numItemCode] AND vcWHSKU = @vcSKU))
            
		IF @cnt > 0 
        BEGIN
            SELECT 
				@ItemID = [numItemCode],
                @ParentSKU = vcSKU
            FROM 
				[Item]
            WHERE 
				[numDomainId] = @numDomainID
                AND (vcSKU = @vcSKU OR @vcSKU IN (SELECT vcWHSKU FROM dbo.WareHouseItems WHERE numItemID = Item.[numItemCode] AND vcWHSKU = @vcSKU))

            SET @numItemCode = @ItemID
        END
		ELSE 
		BEGIN
			-- check wether this id already Mapped in ITemAPI 
			SELECT 
				@cnt = COUNT([numItemID])
			FROM 
				[ItemAPI]
			WHERE 
				[WebApiId] = @intWebApiId
				AND [numDomainId] = @numDomainID
				AND [vcAPIItemID] = @vcApiItemId
                    
			IF @cnt > 0 
			BEGIN
				SELECT 
					@ItemID = [numItemID]
				FROM 
					[ItemAPI]
				WHERE 
					[WebApiId] = @intWebApiId
					AND [numDomainId] = @numDomainID
					AND [vcAPIItemID] = @vcApiItemId
        
				--Update Existing Product coming from API
				SET @numItemCode = @ItemID
			END
		END
    END
                                                         
	IF @numItemCode=0 OR @numItemCode IS NULL
	BEGIN
		INSERT INTO Item 
		(                                             
			vcItemName,txtItemDesc,charItemType,monListPrice,numItemClassification,bitTaxable,vcSKU,bitKitParent,
			numVendorID,numDomainID,numCreatedBy,bintCreatedDate,bintModifiedDate,numModifiedBy,bitSerialized,
			vcModelID,numItemGroup,numCOGsChartAcntId,numAssetChartAcntId,numIncomeChartAcntId,monAverageCost,                          
			monCampaignLabourCost,fltWeight,fltHeight,fltWidth,fltLength,bitFreeShipping,bitAllowBackOrder,vcUnitofMeasure,
			bitShowDeptItem,bitShowDeptItemDesc,bitCalAmtBasedonDepItems,bitAssembly,numBarCodeId,vcManufacturer,numBaseUnit,
			numPurchaseUnit,numSaleUnit,bitLotNo,IsArchieve,numItemClass,tintStandardProductIDType,vcExportToAPI,numShipClass,
			bitAllowDropShip,bitArchiveItem,bitAsset,bitRental,bitVirtualInventory,bitContainer,numContainer,numNoItemIntoContainer
			,bitMatrix,numManufacturer,vcASIN,tintKitAssemblyPriceBasedOn,fltReorderQty,bitSOWorkOrder,bitKitSingleSelect,bitExpenseItem,numExpenseChartAcntId,numBusinessProcessId
		)                                                              
		VALUES                                                                
		(                                                                
			@vcItemName,@txtItemDesc,@charItemType,@monListPrice,@numItemClassification,@bitTaxable,@ParentSKU,@bitKitParent,
			@numVendorID,@numDomainID,@numUserCntID,getutcdate(),getutcdate(),@numUserCntID,@bitSerialized,@vcModelID,@numItemGroup,                                      
			@numCOGsChartAcntId,@numAssetChartAcntId,@numIncomeChartAcntId,@monAverageCost,@monLabourCost,@fltWeight,@fltHeight,@fltWidth,                  
			@fltLength,@bitFreeshipping,@bitAllowBackOrder,@UnitofMeasure,@bitShowDeptItem,@bitShowDeptItemDesc,@bitCalAmtBasedonDepItems,    
			@bitAssembly,@numBarCodeId,@vcManufacturer,@numBaseUnit,@numPurchaseUnit,@numSaleUnit,@bitLotNo,@IsArchieve,@numItemClass,
			@tintStandardProductIDType,@vcExportToAPI,@numShipClass,@bitAllowDropShip,@bitArchiveItem ,@bitAsset,@bitRental,
			@bitVirtualInventory,@bitContainer,@numContainer,@numNoItemIntoContainer
			,@bitMatrix,@numManufacturer,@vcASIN,@tintKitAssemblyPriceBasedOn,@fltReorderQty,@bitSOWorkOrder,@bitKitSingleSelect,@bitExpenseItem,@numExpenseChartAcntId,@numBusinessProcessId
		)                                             
 
		SET @numItemCode = SCOPE_IDENTITY()
  
		IF  @intWebApiId > 1 
		BEGIN
			 -- insert new product
			 --insert this id into linking table
			 INSERT INTO [ItemAPI] (
	 			[WebApiId],
	 			[numDomainId],
	 			[numItemID],
	 			[vcAPIItemID],
	 			[numCreatedby],
	 			[dtCreated],
	 			[numModifiedby],
	 			[dtModified],vcSKU
			 ) VALUES     
			 (
				@intWebApiId,
				@numDomainID,
				@numItemCode,
				@vcApiItemId,
	 			@numUserCntID,
	 			GETUTCDATE(),
	 			@numUserCntID,
	 			GETUTCDATE(),@vcSKU
	 		) 
		END
 
		IF @charItemType = 'P'
		BEGIN
			
			DECLARE @TEMPWarehouse TABLE
			(
				ID INT IDENTITY(1,1)
				,numWarehouseID NUMERIC(18,0)
			)
		
			INSERT INTO @TEMPWarehouse (numWarehouseID) SELECT numWareHouseID FROM Warehouses WHERE numDomainID=@numDomainID

			DECLARE @j AS INT = 1
			DECLARE @jCount AS INT
			DECLARE @numTempWarehouseID NUMERIC(18,0)
			SELECT @jCount = COUNT(*) FROM @TEMPWarehouse

			WHILE @j <= @jCount
			BEGIN
				SELECT @numTempWarehouseID=numWarehouseID FROM @TEMPWarehouse WHERE ID=@j

				EXEC USP_WarehouseItems_Save @numDomainID,
											@numUserCntID,
											0,
											@numTempWarehouseID,
											0,
											@numItemCode,
											'',
											@monListPrice,
											0,
											0,
											'',
											'',
											'',
											'',
											0
 
				SET @j = @j + 1
			END
		END 
	END         
	ELSE IF @numItemCode>0 AND @intWebApiId > 0 
	BEGIN 
		IF @ProcedureCallFlag = 1 
		BEGIN
			DECLARE @ExportToAPIList AS VARCHAR(30)
			SELECT @ExportToAPIList = vcExportToAPI FROM dbo.Item WHERE numItemCode = @numItemCode

			IF @ExportToAPIList != ''
			BEGIN
				IF NOT EXISTS(select * from (SELECT * FROM dbo.Split(@ExportToAPIList ,','))as A where items= @vcExportToAPI ) 
				BEGIN
					SET @vcExportToAPI = @ExportToAPIList + ',' + @vcExportToAPI
				End
				ELSE
				BEGIN
					SET @vcExportToAPI = @ExportToAPIList 
				End
			END

			--update ExportToAPI String value
			UPDATE dbo.Item SET vcExportToAPI=@vcExportToAPI where numItemCode=@numItemCode  AND numDomainID=@numDomainID
					
			SELECT 
				@cnt = COUNT([numItemID]) 
			FROM 
				[ItemAPI] 
			WHERE  
				[WebApiId] = @intWebApiId 
				AND [numDomainId] = @numDomainID
				AND [vcAPIItemID] = @vcApiItemId
				AND numItemID = @numItemCode

			IF @cnt > 0
			BEGIN
				--update lastUpdated Date 
				UPDATE [ItemAPI] SET [dtModified] =GETUTCDATE() WHERE [WebApiId] =@intWebApiId AND [numDomainId] = @numDomainID
			END
			ELSE
			--Insert ItemAPI mapping
			BEGIN
				INSERT INTO [ItemAPI] 
				(
					[WebApiId],
					[numDomainId],
					[numItemID],
					[vcAPIItemID],
					[numCreatedby],
					[dtCreated],
					[numModifiedby],
					[dtModified],vcSKU
				) VALUES 
				(
					@intWebApiId,
					@numDomainID,
					@numItemCode,
					@vcApiItemId,
					@numUserCntID,
					GETUTCDATE(),
					@numUserCntID,
					GETUTCDATE(),@vcSKU
				)	
			END
		END   
	END                                            
	ELSE IF @numItemCode > 0 AND @intWebApiId <= 0                                                           
	BEGIN
		DECLARE @OldGroupID NUMERIC 
		SELECT @OldGroupID = ISNULL(numItemGroup,0) FROM [Item] WHERE numitemcode = @numItemCode AND [numDomainId] = @numDomainID
	
		DECLARE @monOldAverageCost AS DECIMAL(18,4);SET @monOldAverageCost=0
		SELECT @monOldAverageCost=(CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(monAverageCost,0) END) FROM Item WHERE numItemCode =@numItemCode AND [numDomainId] = @numDomainID
	   
		UPDATE 
			Item 
		SET 
			vcItemName=@vcItemName,txtItemDesc=@txtItemDesc,charItemType=@charItemType,monListPrice= @monListPrice,numItemClassification=@numItemClassification,                                             
			bitTaxable=@bitTaxable,vcSKU = (CASE WHEN ISNULL(@ParentSKU,'') = '' THEN @vcSKU ELSE @ParentSKU END),bitKitParent=@bitKitParent,                                             
			numVendorID=@numVendorID,numDomainID=@numDomainID,bintModifiedDate=getutcdate(),numModifiedBy=@numUserCntID,bitSerialized=@bitSerialized,                                                         
			vcModelID=@vcModelID,numItemGroup=@numItemGroup,numCOGsChartAcntId=@numCOGsChartAcntId,numAssetChartAcntId=@numAssetChartAcntId,                                      
			numIncomeChartAcntId=@numIncomeChartAcntId,monCampaignLabourCost=@monLabourCost,fltWeight=@fltWeight,fltHeight=@fltHeight,fltWidth=@fltWidth,                  
			fltLength=@fltLength,bitFreeShipping=@bitFreeshipping,bitAllowBackOrder=@bitAllowBackOrder,vcUnitofMeasure=@UnitofMeasure,bitShowDeptItem=@bitShowDeptItem,            
			bitShowDeptItemDesc=@bitShowDeptItemDesc,bitCalAmtBasedonDepItems=@bitCalAmtBasedonDepItems,bitAssembly=@bitAssembly,numBarCodeId=@numBarCodeId,
			vcManufacturer=@vcManufacturer,numBaseUnit=@numBaseUnit,numPurchaseUnit=@numPurchaseUnit,numSaleUnit=@numSaleUnit,bitLotNo=@bitLotNo,
			IsArchieve = @IsArchieve,numItemClass=@numItemClass,tintStandardProductIDType=@tintStandardProductIDType,vcExportToAPI=@vcExportToAPI,
			numShipClass=@numShipClass,bitAllowDropShip=@bitAllowDropShip, bitArchiveItem = @bitArchiveItem,bitAsset=@bitAsset,bitRental=@bitRental,
			bitVirtualInventory = @bitVirtualInventory,numContainer=@numContainer,numNoItemIntoContainer=@numNoItemIntoContainer,bitMatrix=@bitMatrix,numManufacturer=@numManufacturer,
			vcASIN = @vcASIN,tintKitAssemblyPriceBasedOn=@tintKitAssemblyPriceBasedOn,fltReorderQty=@fltReorderQty,bitSOWorkOrder=@bitSOWorkOrder,bitKitSingleSelect=@bitKitSingleSelect
			,bitExpenseItem=@bitExpenseItem,numExpenseChartAcntId=@numExpenseChartAcntId,numBusinessProcessId=@numBusinessProcessId
		WHERE 
			numItemCode=@numItemCode 
			AND numDomainID=@numDomainID

		IF NOT EXISTS(SELECT numItemCode FROM dbo.OpportunityItems WHERE numItemCode = @numItemCode)
		BEGIN
			IF (((SELECT ISNULL(SUM(numOnHand),0) FROM WareHouseItems WHERE numItemID = @numItemCode AND numDomainId=@numDomainID) = 0) AND ((SELECT ISNULL(bitVirtualInventory,0) FROM Item  WHERE numItemCode = @numItemCode AND numDomainId=@numDomainID) = 0))
			BEGIN
				UPDATE Item SET monAverageCost=@monAverageCost  where numItemCode=@numItemCode  AND numDomainID=@numDomainID	
			END
		END
	
		--If Average Cost changed then add in Tracking
		IF ISNULL(@monAverageCost,0) != ISNULL(@monOldAverageCost,0)
		BEGIN
			DECLARE @numWareHouseItemID NUMERIC(9);SET @numWareHouseItemID=0
			DECLARE @numTotal int;SET @numTotal=0
			SET @i=0
			DECLARE @vcDescription AS VARCHAR(100);SET @vcDescription=''
		
			SELECT @numTotal=COUNT(*) FROM WareHouseItems where numItemID=@numItemCode AND numDomainId=@numDomainId
	     
			WHILE @i < @numTotal
			BEGIN
				SELECT @numWareHouseItemID = min(numWareHouseItemID) FROM WareHouseItems 
					WHERE numItemID=@numItemCode AND numDomainId=@numDomainId and numWareHouseItemID > @numWareHouseItemID 
				
				IF @numWareHouseItemID>0
				BEGIN
					SET @vcDescription='Average Cost Changed Manually OLD : ' + CAST(ISNULL(@monOldAverageCost,0) AS VARCHAR(20))
				
					DECLARE @numDomain AS NUMERIC(18,0)
					SET @numDomain = @numDomainID
					EXEC dbo.USP_ManageWareHouseItems_Tracking
						@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)		
						@numReferenceID = @numItemCode, --  numeric(9, 0)
						@tintRefType = 1, --  tinyint
						@vcDescription = @vcDescription, --  varchar(100)
						@tintMode = 0, --  tinyint
						@numModifiedBy = @numUserCntID, --  numeric(9, 0)
						@numDomainID = @numDomain
				END
		    
				SET @i = @i + 1
			END
		END
 
		DECLARE @bitCartFreeShipping as  bit 
		SELECT @bitCartFreeShipping = bitFreeShipping FROM dbo.CartItems where numItemCode = @numItemCode  AND numDomainID = @numDomainID 
 
		IF @bitCartFreeShipping <> @bitFreeshipping
		BEGIN
			UPDATE dbo.CartItems SET bitFreeShipping = @bitFreeshipping WHERE numItemCode = @numItemCode  AND numDomainID=@numDomainID
		END 
 
		IF @intWebApiId > 1 
		BEGIN
			SELECT 
				@cnt = COUNT([numItemID])
			FROM 
				[ItemAPI]
			WHERE 
				[WebApiId] = @intWebApiId
				AND [numDomainId] = @numDomainID
				AND [vcAPIItemID] = @vcApiItemId

			IF @cnt > 0
			BEGIN
				--update lastUpdated Date 
				UPDATE [ItemAPI] SET [dtModified] =GETUTCDATE() WHERE [WebApiId] =@intWebApiId AND [numDomainId] = @numDomainID
			END
			ELSE --Insert ItemAPI mapping
			BEGIN
				INSERT INTO [ItemAPI] 
				(
	 				[WebApiId],
	 				[numDomainId],
	 				[numItemID],
	 				[vcAPIItemID],
	 				[numCreatedby],
	 				[dtCreated],
	 				[numModifiedby],
	 				[dtModified],vcSKU
				 )
				 VALUES 
				 (
					@intWebApiId,
					@numDomainID,
					@numItemCode,
					@vcApiItemId,
	 				@numUserCntID,
	 				GETUTCDATE(),
	 				@numUserCntID,
	 				GETUTCDATE(),@vcSKU
	 			) 
			END
		END   
		                                                                            
		-- kishan It is necessary to add item into itemTax table . 
		IF	@bitTaxable = 1
		BEGIN
			IF NOT EXISTS(SELECT * FROM dbo.ItemTax WHERE numItemCode = @numItemCode AND numTaxItemID = 0)
			BEGIN
			    INSERT INTO dbo.ItemTax 
				(
					numItemCode,
					numTaxItemID,
					bitApplicable
				)
				VALUES 
				( 
					@numItemCode,
					0,
					1 
				) 						
			END
		END	 
      
		IF @charItemType='S' or @charItemType='N'                                                                       
		BEGIN
			DELETE FROM WareHouseItmsDTL where numWareHouseItemID in (select numWareHouseItemID from WareHouseItems where numItemID=@numItemCode AND [WareHouseItems].[numDomainID] = @numDomainID)
			DELETE FROM WareHouseItems where numItemID=@numItemCode AND [WareHouseItems].[numDomainID] = @numDomainID
		END                                      
   
		IF @bitKitParent=1 OR @bitAssembly = 1              
		BEGIN              
			IF DATALENGTH(ISNULL(@strChildItems,'')) > 0
			BEGIN
				SET @strChildItems = CONCAT('<?xml version="1.0" encoding="iso-8859-1"?>',@strChildItems)
			END

			DECLARE @hDoc1 AS INT                                            
			EXEC sp_xml_preparedocument @hDoc1 OUTPUT, @strChildItems                                                                        
                                             
			UPDATE 
				ItemDetails 
			SET
				numQtyItemsReq=X.QtyItemsReq
				,numUOMId=X.numUOMId
				,vcItemDesc=X.vcItemDesc
				,sintOrder=X.sintOrder                                                                                          
			From 
			(
				SELECT 
					QtyItemsReq,ItemKitID,ChildItemID,numUOMId,vcItemDesc,sintOrder, numItemDetailID As ItemDetailID                             
				FROM 
					OPENXML(@hDoc1,'/NewDataSet/Table1',2)                                                                         
				WITH
				(
					ItemKitID NUMERIC(9),                                                          
					ChildItemID NUMERIC(9),                                                                        
					QtyItemsReq DECIMAL(30, 16),
					numUOMId NUMERIC(9),
					vcItemDesc VARCHAR(1000),
					sintOrder int,
					numItemDetailID NUMERIC(18,0)
				)
			)X                                                                         
			WHERE 
				numItemKitID=X.ItemKitID 
				AND numChildItemID=ChildItemID 
				AND numItemDetailID = X.ItemDetailID

			EXEC sp_xml_removedocument @hDoc1
		 END   
		 ELSE
		 BEGIN
 			DELETE FROM ItemDetails WHERE numItemKitID=@numItemCode
		 END                                                
	END

	IF ISNULL(@numItemGroup,0) = 0 OR ISNULL(@bitMatrix,0) = 1
	BEGIN
		UPDATE WareHouseItems SET monWListPrice = @monListPrice, vcWHSKU=@vcSKU, vcBarCode=@numBarCodeId WHERE numDomainID = @numDomainID AND numItemID = @numItemCode
	END

	-- SAVE MATRIX ITEM ATTRIBUTES
	IF @numItemCode > 0 AND ISNULL(@bitAttributesChanged,0)=1 AND ISNULL(@bitMatrix,0) = 1 AND LEN(ISNULL(@vcItemAttributes,'')) > 0 AND ISNULL(@bitOppOrderExists,0) <> 1 AND (SELECT COUNT(*) FROM @TempTable) > 0
	BEGIN
		-- DELETE PREVIOUS ITEM ATTRIBUTES
		DELETE FROM ItemAttributes WHERE numDomainID=@numDomainID AND numItemCode=@numItemCode

		-- INSERT NEW ITEM ATTRIBUTES
		INSERT INTO ItemAttributes
		(
			numDomainID
			,numItemCode
			,FLD_ID
			,FLD_Value
		)
		SELECT
			@numDomainID
			,@numItemCode
			,Fld_ID
			,Fld_Value
		FROM
			@TempTable

		IF (SELECT COUNT(*) FROM WarehouseItems WHERE numDomainID=@numDomainID AND numItemID=@numItemCode) > 0
		BEGIN

			-- DELETE PREVIOUS WAREHOUSE ATTRIBUTES
			DELETE FROM CFW_Fld_Values_Serialized_Items WHERE RecId IN (SELECT ISNULL(numWarehouseItemID,0) FROM WarehouseItems WHERE numDomainID=@numDomainID AND numItemID=@numItemCode)

			-- INSERT NEW WAREHOUSE ATTRIBUTES
			INSERT INTO CFW_Fld_Values_Serialized_Items
			(
				Fld_ID,
				Fld_Value,
				RecId,
				bitSerialized
			)                                                  
			SELECT 
				Fld_ID,
				ISNULL(CAST(Fld_Value AS VARCHAR),'') AS Fld_Value,
				TEMPWarehouse.numWarehouseItemID,
				0 
			FROM 
				@TempTable  
			OUTER APPLY
			(
				SELECT
					numWarehouseItemID
				FROM 
					WarehouseItems 
				WHERE 
					numDomainID=@numDomainID 
					AND numItemID=@numItemCode
			) AS TEMPWarehouse
		END
	END	  
COMMIT

	IF @numItemCode > 0
	BEGIN
		DELETE 
			IC
		FROM 
			dbo.ItemCategory IC
		INNER JOIN 
			Category 
		ON 
			IC.numCategoryID = Category.numCategoryID
		WHERE 
			numItemID = @numItemCode		
			AND numCategoryProfileID = @numCategoryProfileID

		IF @vcCategories <> ''	
		BEGIN
			INSERT INTO ItemCategory( numItemID , numCategoryID )  SELECT  @numItemCode AS numItemID,ID from dbo.SplitIDs(@vcCategories,',')	
		END
	END
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH        
END
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageProjectCommission')
DROP PROCEDURE USP_ManageProjectCommission
GO
CREATE PROCEDURE USP_ManageProjectCommission
    (     
      @numDomainId NUMERIC,
      @numProId numeric(9)
    )

AS BEGIN

DECLARE @tintCommissionType AS TINYINT
DECLARE @tintComAppliesTo AS TINYINT 

--Get CommissionType (total amounts paid : 1 ,gross profit : 2 ,Project Gross Profit : 3)
SELECT @tintCommissionType=ISNULL(tintCommissionType, 1),@tintComAppliesTo = ISNULL(tintComAppliesTo,3) FROM dbo.Domain WHERE numDomainId=@numDomainId

--If Commission set for Project Gross Profit
IF @tintCommissionType=3
BEGIN
	
	--If Project Status set as Completed (27492 : from ListDetails) 
	IF EXISTS (SELECT * FROM dbo.ProjectsMaster WHERE numdomainid = @numDomainId AND numProId=@numProId AND numProjectStatus=27492)
	BEGIN
		DECLARE @monTotalGrossProfit AS DECIMAL(20,5);SET @monTotalGrossProfit=0
		DECLARE @monTotalExpense AS DECIMAL(20,5);SET @monTotalExpense=0
		DECLARE @monTotalIncome AS DECIMAL(20,5);SET @monTotalIncome=0
		DECLARE @dtCompletionDate AS DATETIME;SET @dtCompletionDate=GETUTCDATE()
		
--		SELECT @monProjectGrossProfit=ISNULL(monBalance,0) FROM dbo.GetProjectIncomeExpense(@numDomainId,@numProId)
		
		SELECT @monTotalGrossProfit=ISNULL(monTotalGrossProfit,0),@monTotalExpense=ISNULL(monTotalExpense,0),
		 @monTotalIncome=ISNULL(monTotalIncome,0),@dtCompletionDate=dtCompletionDate FROM ProjectsMaster 
		 WHERE numdomainid = @numDomainId AND numProId=@numProId
		
		--If Project Gross Profit (+)
		IF @monTotalGrossProfit>0
		BEGIN
			DECLARE @numassignedto NUMERIC,@numrecowner NUMERIC
			DECLARE @numDivisionID NUMERIC,@bitCommContact AS BIT,@numUserCntID NUMERIC
			DECLARE @decCommission DECIMAL(18,2),@monCommission AS DECIMAL(20,5),@numComRuleID NUMERIC,@tintComBasedOn TINYINT,@tinComDuration TINYINT,@tintComType TINYINT
			DECLARE @dFrom DATETIME,@dTo DATETIME,@monTotalProjectGrossProfit AS DECIMAL(20,5),@monTotalProjectIncome AS DECIMAL(20,5)  
			
			--Get AssignTo and Record Owner of Oppertunity
			SELECT @numassignedto=numassignedto,@numrecowner=numrecowner FROM ProjectsMaster 
			    WHERE numdomainid = @numDomainId AND numProId=@numProId

				    DECLARE @i AS INT;SET @i=1
					
					--Loop for AssignTo i=1 and RecordOwner i=2 (set tintAssignTo=i)
					WHILE @i<3
					BEGIN
						
					IF @i=1
						SET @numUserCntID=@numassignedto
					ELSE IF @i=2
						SET @numUserCntID=@numrecowner
						
					DECLARE @monProTotalGrossProfit AS DECIMAL(20,5);SET @monProTotalGrossProfit=0
					DECLARE @monProTotalIncome AS DECIMAL(20,5);SET @monProTotalIncome=0
					
					SELECT @monProTotalGrossProfit=ISNULL(monProTotalGrossProfit,0),@monProTotalIncome=ISNULL(monProTotalIncome,0) FROM dbo.BizDocComission WHERE numdomainid = @numDomainId AND numProId=@numProId AND numUserCntID=@numUserCntID	
					
					SET @monProTotalGrossProfit=@monTotalGrossProfit - @monProTotalGrossProfit
					SET @monProTotalIncome = @monTotalIncome - @monProTotalIncome
					
					IF @monProTotalGrossProfit>0
					BEGIN
						--Check For Commission Contact	
						IF EXISTS(SELECT UM.numUserId from UserMaster UM  join AdditionalContactsInformation A  on UM.numUserDetailId=A.numContactID          
								where UM.numDomainID=@numDomainID and UM.numDomainID=A.numDomainID AND A.numContactID =@numUserCntID)
							BEGIN
								SET @bitCommContact=0
								SET @numDivisionID=0
							END	
						 ELSE
							BEGIN
								 SET @bitCommContact=1
								 SELECT @numDivisionID=A.numDivisionId from AdditionalContactsInformation A  where A.numContactId =@numUserCntID 
							END  		
							
						--All Items	 tintComAppliesTo=3
						IF EXISTS (SELECT CR.numComRuleID FROM CommissionRules CR JOIN CommissionRuleContacts CRC ON CR.numComRuleId=CRC.numComRuleId  
										WHERE CR.tintComAppliesTo=3 AND CR.numDomainID=@numDomainID AND CR.tintAssignTo=@i AND CRC.numValue=(CASE @bitCommContact WHEN 0 THEN @numUserCntID WHEN 1 THEN @numDivisionID END) AND CRC.bitCommContact=@bitCommContact) AND @tintComAppliesTo=3
							BEGIN
								SELECT TOP 1 @numComRuleID=CR.numComRuleID,@tintComBasedOn=CR.tintComBasedOn,@tinComDuration=CR.tinComDuration,@tintComType=CR.tintComType 
								FROM CommissionRules CR JOIN CommissionRuleContacts CRC ON CR.numComRuleId=CRC.numComRuleId  WHERE CR.tintComAppliesTo=3 AND CR.numDomainID=@numDomainID 
								AND CR.tintAssignTo=@i AND CRC.numValue=(CASE @bitCommContact WHEN 0 THEN @numUserCntID WHEN 1 THEN @numDivisionID END) AND CRC.bitCommContact=@bitCommContact
							END
						
						IF @numComRuleID>0
						BEGIN
							--Get From and To date for Commission Calculation
							IF @tinComDuration=1 --Month
								SELECT @dFrom=DATEADD(m, DATEDIFF(m, 0, GETDATE()), 0),
										@dTo=DATEADD(d, -1, DATEADD(m, DATEDIFF(m, 0, GETDATE()) + 1, 0)) 
		
							ELSE IF @tinComDuration=2 --Quarter
								SELECT @dFrom=DATEADD(q, DATEDIFF(q, 0, GETDATE()), 0),
										@dTo=DATEADD(d, -1, DATEADD(q, DATEDIFF(q, 0, GETDATE()) + 1, 0)) 
										
							ELSE IF @tinComDuration=3 --Year
								SELECT @dFrom=DATEADD(yy, DATEDIFF(yy, 0, GETDATE()), 0),
										@dTo=DATEADD(d, -1, DATEADD(yy, DATEDIFF(yy, 0, GETDATE()) + 1, 0)) 
							
							
							--Total of Units and Amount for all oppertunity belogs to selected Item and selected AssignTo/RecordOwner
							SELECT @monTotalProjectGrossProfit=ISNULL(SUM(PM.monTotalGrossProfit),0),
								   @monTotalProjectIncome=ISNULL(SUM(PM.monTotalIncome),0)
								FROM dbo.ProjectsMaster PM 
								WHERE PM.numdomainid = @numDomainId AND PM.numProId<>@numProId AND PM.numProjectStatus=27492
								AND 1= (CASE @i WHEN 1 THEN CASE @bitCommContact WHEN 0 THEN CASE WHEN numassignedto=@numUserCntID THEN 1 ELSE 0 END 
																				 WHEN 1 THEN CASE WHEN numassignedto IN (SELECT numContactId FROM AdditionalContactsInformation AC WHERE AC.numDivisionID = @numDivisionID) THEN 1 ELSE 0 END  END
												WHEN 2 THEN CASE @bitCommContact WHEN 0 THEN CASE WHEN numrecowner=@numUserCntID THEN 1 ELSE 0 END 
																				WHEN 1 THEN CASE WHEN numrecowner IN (SELECT numContactId FROM AdditionalContactsInformation AC WHERE AC.numDivisionID = @numDivisionID) THEN 1 ELSE 0 END END END)	 				 	 
								AND PM.dtCompletionDate BETWEEN @dFrom AND @dTo
							
							
							IF @tintComBasedOn=3 --Project Gross Profit Amt
							BEGIN
								SET @monTotalProjectGrossProfit = @monTotalGrossProfit + @monTotalProjectGrossProfit
							END
							ELSE IF @tintComBasedOn=4 --Project Total Income %
							BEGIN
								SET @monTotalProjectGrossProfit = ((@monTotalGrossProfit + @monTotalProjectGrossProfit) / (@monTotalIncome + @monTotalProjectIncome)) * 100
							END
							
							DECLARE @intFromNextTier AS DECIMAL(18,2)
							
							SELECT TOP 1 @decCommission = decCommission,@intFromNextTier=intTo + 1 FROM CommissionRuleDtl 
										WHERE numComRuleID=@numComRuleID AND @monTotalProjectGrossProfit BETWEEN intFrom AND intTo
					
					
							DECLARE @nexttier AS varchar(100)

							SELECT TOP 1 @nexttier = CAST(numComRuleDtlID AS VARCHAR(10))  + ',' + CAST(numComRuleID AS VARCHAR(10)) + ',' + CAST(intFrom AS VARCHAR(10)) + ',' + CAST(intTo AS VARCHAR(10)) + ',' + CAST(decCommission AS VARCHAR(10)) 
							FROM CommissionRuleDtl WHERE numComRuleID=@numComRuleID AND intFrom >= @intFromNextTier	ORDER BY intFrom 
								
							IF @tintComBasedOn=3 --Project Gross Profit Amt
							BEGIN
								SET @monCommission=@monProTotalGrossProfit * @decCommission / 100
							END
							ELSE IF @tintComBasedOn=4 --Project Total Income %
							BEGIN
								SET @monCommission=@monProTotalIncome * @decCommission / 100
							END
							
							if @monCommission>0
							begin
	--							IF NOT exists (SELECT * FROM [BizDocComission] WHERE numDomainId=@numDomainId AND numUserCntID=@numUserCntID AND numProId=@numProId)
	--							BEGIN
									INSERT INTO [BizDocComission]([numDomainId],[numUserCntID],[numProId],[numComissionAmount],
											[numComRuleID],[tintComType],[tintComBasedOn],[decCommission],vcnexttier,dtProCompletionDate,monProTotalExpense,monProTotalIncome,monProTotalGrossProfit) 
											values (@numDomainId,@numUserCntID,@numProId,@monCommission,
												   @numComRuleID,@tintComType,@tintComBasedOn,@decCommission,@nexttier,@dtCompletionDate,@monTotalExpense,@monTotalIncome,@monTotalGrossProfit);
								--END
							END
						END
						END	
						SELECT @numComRuleID=0,@tintComBasedOn=0,@tinComDuration=0,@tintComType=0,@monCommission=0,@bitCommContact=0,@numDivisionID=0,@monTotalProjectGrossProfit=0,@monTotalProjectIncome=0
							SET @i = @i + 1
					END
		END
	END
	
END
    
END

GO


/****** Object:  StoredProcedure [USP_ManageReturnHeaderItems]    ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageReturnHeaderItems')
DROP PROCEDURE USP_ManageReturnHeaderItems
GO
CREATE PROCEDURE [USP_ManageReturnHeaderItems]                              
(
	@numReturnHeaderID numeric(18, 0),
    @vcRMA varchar(100),
    @vcBizDocName varchar(100),
    @numDomainId numeric(18, 0),
    @numUserCntID AS NUMERIC(9)=0,
    @numDivisionId numeric(18, 0),
    @numContactId numeric(18, 0),
    @numOppId numeric(18, 0),
    @tintReturnType tinyint,
    @numReturnReason numeric(18, 0),
    @numReturnStatus numeric(18, 0),
    @monAmount money,
    @monAmountUsed money,
    @monTotalTax money,
    @monTotalDiscount money,
    @tintReceiveType tinyint,
    @vcComments text,
    @strItems TEXT=''
)            
AS                                           
BEGIN 

DECLARE  @hDocItem INT
 
 IF @numReturnHeaderID=0                                  
  BEGIN                              
     
    INSERT INTO .[ReturnHeader] ([vcRMA], [vcBizDocName], [numDomainId], [numDivisionId], [numContactId], [numOppId], [tintReturnType], [numReturnReason], [numReturnStatus], [monAmount], [monAmountUsed], [monTotalTax], [monTotalDiscount], [tintReceiveType], [vcComments], [numCreatedBy], [dtCreatedDate])
		SELECT @vcRMA, @vcBizDocName, @numDomainId, @numDivisionId, @numContactId, @numOppId, @tintReturnType, @numReturnReason, @numReturnStatus, @monAmount, @monAmountUsed, @monTotalTax, @monTotalDiscount, @tintReceiveType, @vcComments, @numUserCntID,GETUTCDATE()
                    
	SET @numReturnHeaderID = @@IDENTITY                                        
	SELECT @numReturnHeaderID
  END                            
 ELSE IF @numReturnHeaderID<>0                        
  BEGIN                        
   	UPDATE [ReturnHeader]
	SET    [vcRMA] = @vcRMA, [vcBizDocName] = @vcBizDocName, [numDivisionId] = @numDivisionId, [numContactId] = @numContactId,
		   [numOppId] = @numOppId, [tintReturnType] = @tintReturnType, [numReturnReason] = @numReturnReason, [numReturnStatus] = @numReturnStatus,
		   [monAmount] = @monAmount, [monAmountUsed] = @monAmountUsed, [monTotalTax] = @monTotalTax, [monTotalDiscount] = @monTotalDiscount,
		   [tintReceiveType] = @tintReceiveType, [vcComments] = @vcComments, [numModifiedBy] = @numUserCntID, [dtModifiedDate] = GETUTCDATE()
	WHERE  [numDomainId] = @numDomainId AND [numReturnHeaderID] = @numReturnHeaderID
  END            
  
  
  IF CONVERT(VARCHAR(10),@strItems) <> ''
        BEGIN
           DECLARE @hDoc3 INT                                                                                                                                                                
                EXEC sp_xml_preparedocument @hDoc3 OUTPUT, @strItems                                                                                                             
                    SELECT * INTO #temp FROM      OPENXML (@hdoc3,'/NewDataSet/Item',2)
												WITH (
												numReturnItemID NUMERIC, 				
								numItemCode NUMERIC,
								numUnitHour NUMERIC,
								numUnitHourReceived NUMERIC,
								monPrice MONEY,
								monTotAmount MONEY,
								vcItemDesc VARCHAR(1000),
								numWareHouseItemID	NUMERIC,	
								vcModelID	VARCHAR(200),
								vcManufacturer	VARCHAR(250),
								numUOMId NUMERIC,
								bitMarkupDiscount BIT)

                 EXEC sp_xml_removedocument @hDoc3 
                 
            
              DELETE FROM ReturnItems WHERE numReturnHeaderID = @numReturnHeaderID
              AND numReturnItemID NOT IN (SELECT numReturnItemID FROM #temp WHERE numReturnItemID>0)
			
			
			  INSERT INTO [ReturnItems] ([numReturnHeaderID], [numItemCode], [numUnitHour], [numUnitHourReceived], 
				[monPrice], [monTotAmount],[vcItemDesc], [numWareHouseItemID], [vcModelID], [vcManufacturer], [numUOMId], [bitMarkupDiscount])
			  SELECT @numReturnHeaderID,
					 numItemCode ,
					 numUnitHour ,
					 numUnitHourReceived,
					 monPrice,
					 monTotAmount,
					 vcItemDesc,
					 numWareHouseItemID,
					 vcModelID,
					 vcManufacturer,
					 numUOMId,
					 bitMarkupDiscount
			  FROM  #temp WHERE numReturnItemID=0
					
					
				UPDATE [ReturnItems]
					SET    [numItemCode] = X.numItemCode, [numUnitHour] = X.numUnitHour, [numUnitHourReceived] = X.numUnitHourReceived, 
						   [monPrice] = X.monPrice, [monTotAmount] = X.monTotAmount, [vcItemDesc] = X.vcItemDesc, [numWareHouseItemID] = X.numWareHouseItemID,
						   [vcModelID] = X.vcModelID, [vcManufacturer] = X.vcManufacturer, [numUOMId] = X.numUOMId, [bitMarkupDiscount] = X.bitMarkupDiscount
				 FROM    #temp as X 
					  WHERE X.numReturnItemID =ReturnItems.numReturnItemID AND ReturnItems.numReturnHeaderID = @numReturnHeaderID

          drop table #temp
  END 
END
GO
/****** Object:  StoredProcedure [dbo].[USP_ManageTimeAndExpense]    Script Date: 07/26/2008 16:20:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_managetimeandexpense')
DROP PROCEDURE usp_managetimeandexpense
GO
CREATE PROCEDURE [dbo].[USP_ManageTimeAndExpense]
    @numCategoryHDRID AS NUMERIC(18,0) = 0 OUTPUT,
    @tintTEType AS TINYINT = 0,
    @numCategory AS NUMERIC(9) = 0,
    @dtFromDate AS DATETIME,
    @dtToDate AS DATETIME,
    @bitFromFullDay AS BIT,
    @bitToFullDay AS BIT,
    @monAmount AS DECIMAL(20,5),
    @numOppId AS NUMERIC(9) = 0,
    @numDivisionID AS NUMERIC(9) = 0,
    @txtDesc AS TEXT = '',
    @numType AS NUMERIC(9) = 0,
    @numUserCntID AS NUMERIC(9) = 0,
    @numDomainID AS NUMERIC(9) = 0,
    @numCaseId AS NUMERIC = 0,
    @numProId AS NUMERIC = 0,
    @numContractId AS NUMERIC = 0,
    @numOppBizDocsId AS NUMERIC = 0,
    @numStageId AS NUMERIC = 0,
    @bitReimburse AS BIT = 0,
    @bitLeaveTaken AS BIT = 0 OUTPUT,
    @numOppItemID AS NUMERIC = 0,
	@numExpId AS NUMERIC=0,
	@numApprovalComplete AS INT=0,
	@numServiceItemID AS INT=0,
	@numClassID AS INT=0,
	@numCreatedBy AS INT=0
AS 
BEGIN
	DECLARE @records AS NUMERIC(10)      
    DECLARE @monTotAmount AS DECIMAL(20,5)
    DECLARE @numJournalId AS NUMERIC(18,0)   
	
	EXEC dbo.USP_ValidateFinancialYearClosingDate @numDomainID,@dtFromDate


	IF @numCategory=1 AND  @numType NOT IN (3,4) AND EXISTS (SELECT 
																numCategoryHDRID
															FROM 
																TimeAndExpense 
															WHERE 
																numDomainID=@numDomainID 
																AND numUserCntID=@numUserCntID 
																AND numCategory IN (3,4) 
																AND CAST(dtFromDate AS DATE) = CAST(@dtFromDate AS DATE))
	BEGIN
		RAISERROR('LEAVE_ENTRY_EXISTS',16,1)
	END
	ELSE IF @numCategory <> 1 OR (@numCategory=1 AND (@numType NOT IN (3,4) OR (@numCategory IN (3,4) AND NOT EXISTS (SELECT 
																										numCategoryHDRID
																									FROM 
																										TimeAndExpense 
																									WHERE 
																										numDomainID=@numDomainID 
																										AND numUserCntID=@numUserCntID 
																										AND numCategory IN (3,4) 
																										AND CAST(dtFromDate AS DATE) = CAST(@dtFromDate AS DATE)))))
	BEGIN
		INSERT INTO TimeAndExpense
        (
            tintTEType,
            numCategory,
            dtFromDate,
            dtToDate,
            bitFromFullDay,
            bitToFullDay,
            monAmount,
            numOppId,
            numDivisionID,
            txtDesc,
            numType,
            numUserCntID,
            numDomainID,
            numCaseId,
            numProId,
            numContractId,
            numOppBizDocsId,
            numtCreatedBy,
            numtModifiedBy,
            dtTCreatedOn,
            dtTModifiedOn,
            numStageId,
            bitReimburse,
			numOppItemID,
			numExpId,
			numApprovalComplete,
			numServiceItemID,
			numClassID    
        )
        VALUES  
		(
            @tintTEType,
            @numCategory,
            @dtFromDate,
            @dtToDate,
            @bitFromFullDay,
            @bitToFullDay,
            @monAmount,
            @numOppId,
            @numDivisionID,
            @txtDesc,
            @numType,
            @numUserCntID,
            @numDomainID,
            @numCaseId,
            @numProId,
            @numContractId,
            @numOppBizDocsId,
            @numCreatedBy,
            @numCreatedBy,
            GETUTCDATE(),
            GETUTCDATE(),
            @numStageId,
            @bitReimburse,
			@numOppItemID,
			@numExpId,
			@numApprovalComplete,
			@numServiceItemID,
			@numClassID         
        )             
         
		SET @numCategoryHDRID=SCOPE_IDENTITY()
	END 

END
GO

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageWorkOrder')
DROP PROCEDURE USP_ManageWorkOrder
GO
CREATE PROCEDURE [dbo].[USP_ManageWorkOrder]
@numItemCode AS NUMERIC(18,0),
@numWareHouseItemID AS NUMERIC(18,0),
@Units AS INT,
@numDomainID AS NUMERIC(18,0),
@numUserCntID AS NUMERIC(18,0),
@vcInstruction AS VARCHAR(1000),
@bintCompliationDate AS DATETIME,
@numAssignedTo AS NUMERIC(9),
@numSalesOrderId AS NUMERIC(18,0)=0,
@numBuildProcessId AS NUMERIC(18,0)=0,
@dtmProjectFinishDate AS DATETIME=NULL,
@dtmEndDate AS DATETIME=NULL,
@dtmStartDate AS DATETIME=NULL
AS
BEGIN
BEGIN TRY
BEGIN TRAN
	DECLARE @numWarehouseID NUMERIC(18,0)
	SET @numWarehouseID = ISNULL((SELECT numWareHouseID FROM WareHouseItems WHERE numWareHouseItemID = @numWareHouseItemID),0)

	IF EXISTS (SELECT 
					item.numItemCode
				FROM 
					item                                
				INNER JOIN 
					ItemDetails Dtl 
				ON 
					numChildItemID=numItemCode
				WHERE 
					numItemKitID=@numItemCode
					AND charItemType='P'
					AND ISNULL((SELECT TOP 1 numWareHouseItemId FROM WareHouseItems WHERE numItemID=item.numItemCode AND numWareHouseID=@numWarehouseID ORDER BY numWareHouseItemID),0) = 0)
	BEGIN
		RAISERROR('SELECTED_WAREHOUSE_IS_NOT_AVAILABLE_FOR_ALL_CHILD_ITEMS',16,1)
		RETURN
	END

	DECLARE @numWOId AS NUMERIC(9),@numDivisionId AS NUMERIC(9) 
	DECLARE @txtItemDesc AS varchar(1000),@vcItemName AS VARCHAR(300)
	
	SELECT @txtItemDesc=ISNULL(txtItemDesc,''),@vcItemName=ISNULL(vcItemName,'') FROM Item WHERE numItemCode=@numItemCode

	INSERT INTO WorkOrder
	(
		numItemCode,numQtyItemsReq,numWareHouseItemId,numCreatedBy,
		bintCreatedDate,numDomainID,numWOStatus,vcItemDesc,vcInstruction,
		bintCompliationDate,numAssignedTo,numOppId,numBuildProcessId,dtmStartDate,dtmEndDate
	)
	VALUES
	(
		@numItemCode,@Units,@numWareHouseItemID,@numUserCntID,
		getutcdate(),@numDomainID,0,@txtItemDesc,@vcInstruction,
		@bintCompliationDate,@numAssignedTo,@numSalesOrderId,@numBuildProcessId,@dtmStartDate,@dtmEndDate
	)
	
	SET @numWOId=@@IDENTITY

	EXEC dbo.USP_UpdateNameTemplateValueForWorkOrder 3,@numDomainID,@numWOId
	IF(@numBuildProcessId>0)
	BEGIN
			INSERT  INTO Sales_process_List_Master ( Slp_Name,
                                                 numdomainid,
                                                 pro_type,
                                                 numCreatedby,
                                                 dtCreatedon,
                                                 numModifedby,
                                                 dtModifiedOn,tintConfiguration,bitAutomaticStartTimer,bitAssigntoTeams,numOppId,numTaskValidatorId,
												 numProjectId,numWorkOrderId)
		 SELECT Slp_Name,
				numdomainid,
				pro_type,
				numCreatedby,
				dtCreatedon,
				numModifedby,
				dtModifiedOn,tintConfiguration,bitAutomaticStartTimer,bitAssigntoTeams,0,numTaskValidatorId,0,@numWOId FROM Sales_process_List_Master WHERE Slp_Id=@numBuildProcessId    
			DECLARE @numNewProcessId AS NUMERIC(18,0)=0
			SET @numNewProcessId=(SELECT SCOPE_IDENTITY())  
			INSERT INTO StagePercentageDetails(
			numStagePercentageId, 
			tintConfiguration, 
			vcStageName, 
			numDomainId, 
			numCreatedBy, 
			bintCreatedDate, 
			numModifiedBy, 
			bintModifiedDate, 
			slp_id, 
			numAssignTo, 
			vcMileStoneName, 
			tintPercentage, 
			tinProgressPercentage, 
			dtStartDate, 
			numParentStageID, 
			intDueDays, 
			numProjectID, 
			numOppID, 
			vcDescription, 
			bitIsDueDaysUsed,
			numTeamId, 
			bitRunningDynamicMode,
			numStageOrder,
			numWorkOrderId
		)
		SELECT
			numStagePercentageId, 
			tintConfiguration, 
			vcStageName, 
			numDomainId, 
			@numUserCntID, 
			GETDATE(), 
			@numUserCntID, 
			GETDATE(), 
			@numNewProcessId, 
			numAssignTo, 
			vcMileStoneName, 
			tintPercentage, 
			tinProgressPercentage, 
			GETDATE(), 
			numParentStageID, 
			intDueDays, 
			0, 
			0, 
			vcDescription, 
			bitIsDueDaysUsed,
			numTeamId, 
			bitRunningDynamicMode,
			numStageOrder,
			@numWOId
		FROM
			StagePercentageDetails
		WHERE
			slp_id=@numBuildProcessId	

			INSERT INTO StagePercentageDetailsTask(
			numStageDetailsId, 
			vcTaskName, 
			numHours, 
			numMinutes, 
			numAssignTo, 
			numDomainID, 
			numCreatedBy, 
			dtmCreatedOn,
			numOppId,
			numProjectId,
			numParentTaskId,
			bitDefaultTask,
			bitSavedTask,
			numOrder,
			numReferenceTaskId,
			numWorkOrderId
		)
		SELECT 
			(SELECT TOP 1 SFD.numStageDetailsId FROM StagePercentageDetails AS SFD WHERE SFD.vcStageName=SP.vcStageName AND SFD.numWorkOrderId=@numWOId),
			vcTaskName,
			numHours,
			numMinutes,
			ST.numAssignTo,
			@numDomainID,
			@numUserCntID,
			GETDATE(),
			0,
			0,
			0,
			1,
			CASE WHEN SP.bitRunningDynamicMode=1 THEN 0 ELSE 1 END,
			numOrder,
			numTaskId,
			@numWOId
		FROM 
			StagePercentageDetailsTask AS ST
		LEFT JOIN
			StagePercentageDetails As SP
		ON
			ST.numStageDetailsId=SP.numStageDetailsId
		WHERE
			ISNULL(ST.numOppId,0)=0 AND ISNULL(ST.numProjectId,0)=0 AND
			ST.numStageDetailsId IN(SELECT numStageDetailsId FROM 
			StagePercentageDetails As ST
		WHERE
			ST.slp_id=@numBuildProcessId)
		ORDER BY ST.numOrder
	END
	IF @numAssignedTo>0
	BEGIN

		SET @vcItemName= 'Work Order for Item : ' + @vcItemName
		SELECT @numDivisionId=numDivisionId FROM AdditionalContactsInformation WHERE numContactId=@numAssignedTo

		EXEC dbo.usp_InsertCommunication
		@numCommId = 0, --  numeric(18, 0)
		@bitTask = 972, --  numeric(18, 0)
		@numContactId = @numUserCntID, --  numeric(18, 0)
		@numDivisionId = @numDivisionId, --  numeric(18, 0)
		@txtDetails = @vcItemName, --  text
		@numOppId = 0, --  numeric(18, 0)
		@numAssigned = @numAssignedTo, --  numeric(18, 0)
		@numUserCntID = @numUserCntID, --  numeric(18, 0)
		@numDomainId = @numDomainID, --  numeric(18, 0)
		@bitClosed = 0, --  tinyint
		@vcCalendarName = '', --  varchar(100)
		@dtStartTime = @bintCompliationDate, --  datetime
		@dtEndtime = @bintCompliationDate, --  datetime
		@numActivity = 0, --  numeric(9, 0)
		@numStatus = 0, --  numeric(9, 0)
		@intSnoozeMins = 0, --  int
		@tintSnoozeStatus = 0, --  tinyint
		@intRemainderMins = 0, --  int
		@tintRemStaus = 0, --  tinyint
		@ClientTimeZoneOffset = 0, --  int
		@bitOutLook = 0, --  tinyint
		@bitSendEmailTemplate = 0, --  bit
		@bitAlert = 0, --  bit
		@numEmailTemplate = 0, --  numeric(9, 0)
		@tintHours = 0, --  tinyint
		@CaseID = 0, --  numeric(18, 0)
		@CaseTimeId = 0, --  numeric(18, 0)
		@CaseExpId = 0, --  numeric(18, 0)
		@ActivityId = 0, --  numeric(18, 0)
		@bitFollowUpAnyTime = 0, --  bit
		@strAttendee=''
END
	
	INSERT INTO [WorkOrderDetails] 
	(
		numWOId,numItemKitID,numChildItemID,numQtyItemsReq,numWareHouseItemId,vcItemDesc,sintOrder,numQtyItemsReq_Orig,numUOMId
	)
	SELECT 
		@numWOId,numItemKitID,numItemCode,
		CAST(((DTL.numQtyItemsReq * ISNULL(dbo.fn_UOMConversion(Dtl.numUOMID,item.numItemCode,item.numDomainID,item.numBaseUnit),1)) * @Units) AS FLOAT),
		isnull((SELECT TOP 1 numWareHouseItemId FROM WareHouseItems WHERE numItemID=item.numItemCode AND numWareHouseID=@numWarehouseID ORDER BY numWareHouseItemID),0),
		ISNULL(Dtl.vcItemDesc,txtItemDesc),
		ISNULL(sintOrder,0),
		DTL.numQtyItemsReq,
		Dtl.numUOMId 
	FROM 
		item                                
	INNER JOIN 
		ItemDetails Dtl 
	ON 
		numChildItemID=numItemCode
	WHERE 
		numItemKitID=@numItemCode

	--UPDATE ON ORDER OF ASSEMBLY
	UPDATE 
		WareHouseItems
	SET    
		numOnOrder= ISNULL(numOnOrder,0) + @Units,
		dtModified = GETDATE() 
	WHERE   
		numWareHouseItemID = @numWareHouseItemID 
	
	--MAKE ENTRY IN WAREHOUSE TRACKING TABLE
	DECLARE @Description AS VARCHAR(1000)
	SET @Description=CONCAT('Work Order Created (Qty:',@Units,')')

	EXEC dbo.USP_ManageWareHouseItems_Tracking
	@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
	@numReferenceID = @numWOId, --  numeric(9, 0)
	@tintRefType = 2, --  tinyint
	@vcDescription = @Description, --  varchar(100)
	@numModifiedBy = @numUserCntID,
	@numDomainID = @numDomainID

	EXEC USP_ManageInventoryWorkOrder @numWOId,@numDomainID,@numUserCntID
COMMIT TRAN 
END TRY 
BEGIN CATCH		
    IF ( @@TRANCOUNT > 0 ) 
    BEGIN
        ROLLBACK TRAN
    END

	DECLARE @ErrorMessage NVARCHAR(4000);
    DECLARE @ErrorSeverity INT;
    DECLARE @ErrorState INT;

    SELECT @ErrorMessage = ERROR_MESSAGE(), @ErrorSeverity = ERROR_SEVERITY(), @ErrorState = ERROR_STATE();

    RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState);
END CATCH
END
/****** Object:  StoredProcedure [dbo].[USP_OPPContactInfo]    Script Date: 07/26/2008 16:20:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--modified by anoop jayaraj        
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_oppcontactinfo')
DROP PROCEDURE usp_oppcontactinfo
GO
CREATE PROCEDURE [dbo].[USP_OPPContactInfo]                     
@numOppId as numeric(9)=0,
@numDomainId as numeric(9)           
AS                     
                   
 SELECT  D.numDivisionID,a.numContactId,vcCompanyName+', '+isnull(Lst.vcData,'-') as Company,         
  a.vcFirstname +' '+ a.vcLastName as [Name],          
  case when a.numPhone<>'' then + a.numPhone +case when a.numPhoneExtension<>'' then ' - ' + a.numPhoneExtension else '' end  else '' end as [Phone],                                                                                                          
 a.vcEmail as Email,          
  b.vcData as ContactRole,          
  opp.numRole as ContRoleId,         
  convert(integer,isnull(bitPartner,0)) as bitPartner,         
  a.numcontacttype FROM OpportunityContact opp          
  join additionalContactsinformation a on          
  a.numContactId=opp.numContactId        
  join DivisionMaster D        
  on a.numDivisionID =D.numDivisionID        
  join CompanyInfo C        
  on D.numCompanyID=C.numCompanyID         
  left join listdetails b           
  on b.numlistitemid=opp.numRole        
  left join listdetails Lst        
  on Lst.numlistitemid=C.numCompanyType        
  WHERE opp.numOppId=@numOppId and A.numDomainID=@numDomainId
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityMaster_GetContactDetails')
DROP PROCEDURE USP_OpportunityMaster_GetContactDetails
GO
CREATE PROCEDURE [dbo].[USP_OpportunityMaster_GetContactDetails]  
 @cntIds as varchar(1000)
as
begin
	select  ADC.numDivisionId,adc.numContactId,vcGivenName,vcFirstName,vcLastName, c.vcCompanyName,dm.vcDivisionName 
		FROM AdditionalContactsInformation ADC                                                   
		JOIN DivisionMaster DM ON ADC.numDivisionId = DM.numDivisionID                                                  
		JOIN CompanyInfo C ON DM.numCompanyID = C.numCompanyId
	 where ADC.numContactId in (select * from dbo.SplitString(@cntIds,','))
	
end
/****** Object:  StoredProcedure [dbo].[USP_PortalAddItem]    Script Date: 07/26/2008 16:20:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj        
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_portaladditem')
DROP PROCEDURE usp_portaladditem
GO
CREATE PROCEDURE [dbo].[USP_PortalAddItem]                  
@numDivID as numeric(9),                  
@numOppID as numeric(9)=null output ,                  
@numItemCode as numeric(9),                  
@vcPOppName as varchar(200)='' output,                  
@numContactId as numeric(9),                  
@numDomainId as numeric(9),                  
@numUnits as numeric(9)                  
                  
as                  
declare @CRMType as integer     
declare @numRecOwner as integer                  
if @numOppID=  0                   
begin                  
select @CRMType=tintCRMType,@numRecOwner=numRecOwner from DivisionMaster where numDivisionID=@numDivID                  
if @CRMType= 0                   
begin                  
UPDATE DivisionMaster                     
   SET tintCRMType=1                    
   WHERE numDivisionID=@numDivID                    
                                        
end                  
declare @TotalAmount as integer                     
declare @intOppTcode as numeric(9)                    
Select @intOppTcode=max(numOppId)from OpportunityMaster                    
set @intOppTcode =isnull(@intOppTcode,0) + 1                    
set  @vcPOppName=(select  vcCompanyName from CompanyInfo Com                  
join divisionMaster Div                  
on div.numCompanyID=com.numCompanyID                  
where div.numdivisionID=@numDivID)+'-' + @vcPOppName                   
                  
                  
set @vcPOppName=@vcPOppName+ '-'+convert(varchar(4),year(getutcdate()))  +'-A'+ convert(varchar(10),@intOppTcode)                          
insert into OpportunityMaster                    
  (                    
  numContactId,                    
  numDivisionId,                    
  txtComments,                    
  numCampainID,                    
  bitPublicFlag,                    
  tintSource,                    
  vcPOppName,                    
  monPAmount,                    
  numCreatedBy,                    
  bintCreatedDate,                     
  numModifiedBy,                    
  bintModifiedDate,                    
  numDomainId,                     
  tintOppType,          
  intPEstimatedCloseDate,      
  numRecOwner,
  bitOrder                    
  )                    
 Values                    
  (                    
  @numContactId,                    
  @numDivID,                    
  '',                    
  0,                    
  0,                    
  0,                    
  @vcPOppName,                   
  0,                      
  @numContactId,                    
  getutcdate(),                    
  @numContactId,                    
  getutcdate(),                    
  @numDomainId,                    
  3,          
  getutcdate(),         
  @numRecOwner,
  1                
  )                     
set @numOppID=scope_identity()                    
end                   
                  
declare @monPrice as DECIMAL(20,5)             
declare @TotalmonPrice as DECIMAL(20,5)                 

select @monPrice=convert(DECIMAL(20,5),dbo.GetPriceBasedonOrgDisc(@numItemCode,1,@numDivID))   
select @TotalmonPrice=@monPrice*@numUnits                       
                 

insert into OpportunityItems(numOppId,numItemCode,numUnitHour,monPrice,monTotAmount)                  
values(@numOppID,@numItemCode,@numUnits,isnull(@monPrice,0),isnull(@TotalmonPrice,0))                  
                   
                  
select @TotalAmount=sum(monTotAmount)from OpportunityItems where numOppId=@numOppID                  
 update OpportunityMaster set  monPamount=@TotalAmount                  
 where numOppId=@numOppID                 
                
set @vcPOppName=(select  vcPOppName from OpportunityMaster where numOppId=@numOppID )
GO
/****** Object:  StoredProcedure [dbo].[USP_ProContacts]    Script Date: 07/26/2008 16:20:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_procontacts')
DROP PROCEDURE usp_procontacts
GO
CREATE PROCEDURE [dbo].[USP_ProContacts]     
@numProId numeric(9)=0 ,
@numDomainId as numeric(9)=0       
--      
AS      
begin    
    
SELECT  D.numDivisionID,a.numContactId,vcCompanyName+', '+isnull(Lst.vcData,'-') as Company,     
  a.vcFirstname +' '+ a.vcLastName as [Name],      
  a.numPhone +', '+ a.numPhoneExtension as Phone,a.vcEmail as Email,      
  b.vcData as ContactRole,      
  pro.numRole as ContRoleId,     
  convert(integer,isnull(bitPartner,0)) as bitPartner,     
  a.numcontacttype FROM ProjectsContacts pro    
  join additionalContactsinformation a on      
  a.numContactId=pro.numContactId    
  join DivisionMaster D    
  on a.numDivisionID =D.numDivisionID    
  join CompanyInfo C    
  on D.numCompanyID=C.numCompanyID     
  left join listdetails b       
  on b.numlistitemid=pro.numRole    
  left join listdetails Lst    
  on Lst.numlistitemid=C.numCompanyType    
  WHERE pro.numProId=@numProId and a.numDomainID=@numDomainId      
end
GO
/****** Object:  StoredProcedure [dbo].[USP_Projects]    Script Date: 07/26/2008 16:20:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_projects')
DROP PROCEDURE usp_projects
GO
CREATE PROCEDURE [dbo].[USP_Projects]                          
(                          
@numProId numeric(9)=null ,  
@numDomainID as numeric(9),  
@ClientTimeZoneOffset as int         
                         
)                          
as                          
                          
begin                          
                                          
 select  pro.numProId,                          
  pro.vcProjectName,                         
  pro.numintPrjMgr,                          
  pro.numOppId,                    
  pro.intDueDate,                          
  pro.numCustPrjMgr,                          
  pro.numDivisionId,                          
  pro.txtComments,      
  Div.tintCRMType,                          
  div.vcDivisionName,                          
  com.vcCompanyName,      
pro.numcontractId,                       
  dbo.fn_GetContactName(pro.numCreatedBy)+' '+convert(varchar(20),dateadd(minute,-@ClientTimeZoneOffset,pro.bintCreatedDate)) as vcCreatedBy ,                          
  dbo.fn_GetContactName(pro.numModifiedBy)+' '+convert(varchar(20),dateadd(minute,-@ClientTimeZoneOffset,pro.bintModifiedDate)) as vcModifiedby,                        
  dbo.fn_GetContactName(pro.numCompletedBy)+' '+convert(varchar(20),dateadd(minute,-@ClientTimeZoneOffset,pro.dtCompletionDate)) as vcFinishedby,                        
  pro.numDomainId,                          
  dbo.fn_GetContactName(pro.numRecOwner) as vcRecOwner,  pro.numRecOwner,   div.numTerID,       
  pro.numAssignedby,pro.numAssignedTo,          
(select  count(*) from dbo.GenericDocuments where numRecID=@numProId and  vcDocumentSection='P') as DocumentCount,
	
 numAccountID,
 '' TimeAndMaterial,
isnull((select sum(isnull(monTimeBudget,0)) from StagePercentageDetails where numProjectID=@numProId),0) TimeBudget,
isnull((select sum(isnull(monExpenseBudget,0)) from StagePercentageDetails where numProjectID=@numProId),0) ExpenseBudget,
isnull((select min(isnull(dtStartDate,getdate()))-1 from StagePercentageDetails where numProjectID=@numProId),getdate()) dtStartDate,
isnull((select max(isnull(dtEndDate,getdate()))+1 from StagePercentageDetails where numProjectID=@numProId),getdate()) dtEndDate,
ISNULL(numProjectType,0) AS numProjectType,
isnull(dbo.fn_GetExpenseDtlsbyProStgID(pro.numProId,0,0),0) as UsedExpense,                        
isnull(dbo.fn_GeTimeDtlsbyProStgID_BT_NBT(pro.numProId,0,1),'Billable Time (0)  Non Billable Time (0)') as UsedTime,
isnull(Pro.numProjectStatus,0) as numProjectStatus,
(SELECT ISNULL(vcStreet,'') + ',<pre>' + ISNULL(vcCity,'') + ',<pre>' +  dbo.[fn_GetState]([numState]) + ',<pre>' + dbo.fn_GetListItemName(numCountry) + ',<pre>' + vcPostalCode 
 FROM dbo.AddressDetails WHERE numDomainID = pro.numDomainID AND numRecordID = pro.numAddressID AND tintAddressOf = 4 AND tintAddressType = 2 AND bitIsPrimary = 1 )as ShippingAddress,
Pro.numBusinessProcessID,
SLP.Slp_Name AS vcProcessName,
pro.dtCompletionDate AS vcProjectedFinish,
NULL AS dtActualStartDate,
pro.dtmEndDate,
pro.dtmStartDate,
ISNULL(pro.numClientBudget,0) AS numClientBudget,
0 AS numClientBudgetBalance
from ProjectsMaster pro                          
  left join DivisionMaster div                          
  on pro.numDivisionID=div.numDivisionID                          
  left join CompanyInfo com                          
  on div.numCompanyID=com.numCompanyId         
  LEFT JOIN dbo.AddressDetails AD ON AD.numAddressID = pro.numAddressID       
		LEFT JOIN Sales_process_List_Master AS SLP ON pro.numBusinessProcessID=SLP.Slp_Id          
  where numProId=@numProId    and pro.numDomainID=@numDomainID                      
                          
end
GO
/****** Object:  StoredProcedure [dbo].[USP_ProjectsDTLPL]    Script Date: 07/26/2008 16:20:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_projectsdtlpl')
DROP PROCEDURE usp_projectsdtlpl
GO
CREATE PROCEDURE [dbo].[USP_ProjectsDTLPL]                                   
(                                    
@numProId numeric(9)=null  ,                  
@numDomainID numeric(9),    
@ClientTimeZoneOffset as int                                     
)                                    
as                                    
                                    
begin                                    
                                                    
                                    
 select  pro.vcProjectName,
		 pro.numintPrjMgr,
		 dbo.fn_GetContactName(pro.numintPrjMgr) as numintPrjMgrName,                              
		 pro.intDueDate,                                    
		 dbo.fn_GetContactName(pro.numCustPrjMgr) as numCustPrjMgrName,
		 pro.numCustPrjMgr,                                    
         pro.txtComments,
		 [dbo].[fn_GetContactName](pro.numAssignedTo ) as numAssignedToName, 
		 pro.numAssignedTo,                  
        (select  count(*) from GenericDocuments   where numRecID=@numProId and  vcDocumentSection='P') as DocumentCount,
		CASE 
           WHEN tintProStatus = 1 THEN 100
           ELSE isnull((SELECT SUM(tintPercentage)
                 FROM   ProjectsStageDetails
                 WHERE  numProId = @numProId
                        AND bitStageCompleted = 1),0)
         END AS TProgress,
         (select  count(*) from [ProjectsOpportunities] PO Left JOIN [OpportunityMaster] OM ON OM.[numOppId] = PO.[numOppId] WHERE [numProId]=@numProId) as LinkedOrderCount,
         vcProjectID,
         '' TimeAndMaterial,
         numContractId,
         ISNULL((SELECT vcContractName FROM dbo.ContractManagement WHERE numContractId=pro.numContractId),'') numContractIdName,
         isnull(numProjectType,0) numProjectType,
         dbo.fn_GetListItemName(numProjectType) vcProjectTypeName,isnull(Pro.numProjectStatus,0) as numProjectStatus,
dbo.fn_GetListItemName(numProjectStatus) vcProjectStatusName,
(SELECT SUBSTRING(
	(SELECT ','+  A.vcFirstName+' ' +A.vcLastName + CASE WHEN ISNULL(SR.numContactType,0)>0 THEN '(' + dbo.fn_GetListItemName(SR.numContactType) + ')' ELSE '' END
	FROM ShareRecord SR JOIN UserMaster UM ON UM.numUserDetailId=SR.numAssignedTo 
							 JOIN AdditionalContactsInformation A ON UM.numUserDetailId=A.numContactID 
		 WHERE SR.numDomainID=pro.numDomainID AND SR.numModuleID=5 AND SR.numRecordID=pro.numProId
				AND UM.numDomainID=pro.numDomainID and UM.numDomainID=A.numDomainID FOR XML PATH('')),2,200000)) AS numShareWith ,
	'<pre>'+ isnull(AD.vcStreet,'')+ 
	' </pre>'+ isnull(AD.vcCity,'')+ ' ,'
	         + isnull(dbo.fn_GetState(AD.numState),'')+ ' '
	         + isnull(AD.vcPostalCode,'')+ ' <br>' 
	         + isnull(dbo.fn_GetListItemName(AD.numCountry),'') as ShippingAddress,
pro.dtCompletionDate AS vcProjectedFinish,
NULL AS dtActualStartDate,
pro.dtmEndDate,
pro.dtmStartDate,
ISNULL(pro.numClientBudget,0) AS numClientBudget,
0 AS numClientBudgetBalance
from ProjectsMaster pro    
LEFT JOIN dbo.AddressDetails AD ON AD.numAddressID = pro.numAddressID                                 
where numProId=@numProId     and pro.numdomainID=  @numDomainID                             
                                    
end
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ReportListMaster_RunPrebuildReport')
DROP PROCEDURE USP_ReportListMaster_RunPrebuildReport
GO
CREATE PROCEDURE [dbo].[USP_ReportListMaster_RunPrebuildReport]   
@numDomainID AS NUMERIC(18,0),               
@numUserCntID AS NUMERIC(18,0),
@ClientTimeZoneOffset AS INT,
@intDefaultReportID INT,
@numDashBoardID NUMERIC(18,0)
AS  
BEGIN
	DECLARE @vcTimeLine VARCHAR(50)
		,@vcGroupBy VARCHAR(50)
		,@vcTeritorry VARCHAR(MAX)
		,@vcFilterBy TINYINT
		,@vcFilterValue VARCHAR(MAX)
		,@dtFromDate DATE
		,@dtToDate DATE
		,@numRecordCount INT
		,@tintControlField INT
		,@vcDealAmount VARCHAR(MAX)
		,@tintOppType INT
		,@lngPConclAnalysis VARCHAR(MAX)
		,@vcTeams VARCHAR(MAX)
		,@vcEmploees VARCHAR(MAX)
		,@bitTask VARCHAR(MAX)
		,@tintTotalProgress INT
		,@tintMinNumber INT
		,@tintMaxNumber INT
		,@tintQtyToDisplay INT
		,@vcDueDate VARCHAR(MAX)

	IF @intDefaultReportID = 1 --1 - A/R
	BEGIN
		EXEC USP_ReportListMaster_ARPreBuildReport @numDomainID
	END
	ELSE IF @intDefaultReportID = 2 -- A/P
	BEGIN
		EXEC USP_ReportListMaster_APPreBuildReport @numDomainID
	END
	ELSE IF @intDefaultReportID = 3 -- MONEY IN THE BANK
	BEGIN
		EXEC USP_ReportListMaster_BankPreBuildReport @numDomainID
	END
	ELSE IF @intDefaultReportID = 5 -- TOP 10 ITEMS BY PROFIT AMOUNT (LAST 12 MONTHS)
	BEGIN

		SELECT
			@vcTimeLine=vcTimeLine
			,@vcGroupBy=vcGroupBy
			,@vcTeritorry=vcTeritorry
			,@vcFilterBy=vcFilterBy
			,@vcFilterValue=vcFilterValue
			,@dtFromDate=dtFromDate
			,@dtToDate=dtToDate
			,@numRecordCount = ISNULL(numRecordCount,10)
			,@tintControlField = tintControlField
		FROM
			ReportDashboard
		WHERE
			numDashBoardID = @numDashBoardID

		EXEC USP_ReportListMaster_Top10ItemByProfitPreBuildReport @numDomainID,@vcTimeLine,@vcGroupBy,@vcTeritorry,@vcFilterBy,@vcFilterValue,@dtFromDate,@dtToDate,@numRecordCount,@tintControlField
	
		--EXEC USP_ReportListMaster_Top10ItemByProfitPreBuildReport @numDomainID
	END
	ELSE IF @intDefaultReportID = 6 -- TOP 10 ITEMS BY REVENUE SOLD (LAST 12 MONTHS)
	BEGIN
		EXEC USP_ReportListMaster_Top10ItemByRevenuePreBuildReport @numDomainID
	END
	ELSE IF @intDefaultReportID = 7  -- PROFIT MARGIN BY ITEM CLASSIFICATION (LAST 12 MONTHS)
	BEGIN
		EXEC USP_ReportListMaster_ItemClassificationProfitPreBuildReport @numDomainID
	END
	ELSE IF @intDefaultReportID = 8  -- TOP 10 CUSTOMERS BY PROFIT MARGIN
	BEGIN

		SELECT
			@vcTimeLine=vcTimeLine
			,@tintControlField=tintControlField
			,@numRecordCount=ISNULL(numRecordCount,10)
		FROM
			ReportDashboard
		WHERE
			numDashBoardID = @numDashBoardID

		EXEC USP_ReportListMaster_Top10CustomerByProfitPercentPreBuildReport @numDomainID,@ClientTimeZoneOffset,@vcTimeLine,@tintControlField,@numRecordCount
	
		--EXEC USP_ReportListMaster_Top10CustomerByProfitPercentPreBuildReport @numDomainID
	END
	ELSE IF @intDefaultReportID = 9  -- TOP 10 CUSTOMERS BY PROFIT AMOUNT
	BEGIN

	SELECT
			@vcTimeLine=vcTimeLine
			,@tintControlField=tintControlField
			,@numRecordCount=ISNULL(numRecordCount,10)
		FROM
			ReportDashboard
		WHERE
			numDashBoardID = @numDashBoardID

		EXEC USP_ReportListMaster_Top10CustomerByProfitAmountPreBuildReport @numDomainID,@ClientTimeZoneOffset,@vcTimeLine,@tintControlField,@numRecordCount

		--EXEC USP_ReportListMaster_Top10CustomerByProfitAmountPreBuildReport @numDomainID
	END
	ELSE IF @intDefaultReportID = 10  -- ACTION ITEMS & MEETINGS DUE TODAY
	BEGIN
		SELECT
			@vcTimeLine=vcTimeLine
			,@vcTeritorry=vcTeritorry
			,@vcFilterBy=vcFilterBy
			,@vcFilterValue=vcFilterValue
			,@bitTask=bitTask
		FROM
			ReportDashboard
		WHERE
			numDashBoardID = @numDashBoardID

		EXEC USP_ReportListMaster_ActionItemPreBuildReport @numDomainID,0,@ClientTimeZoneOffset,@vcTimeLine,@vcTeritorry,@vcFilterBy,@vcFilterValue,@bitTask
		
		--EXEC USP_ReportListMaster_ActionItemPreBuildReport @numDomainID,0,@ClientTimeZoneOffset
	END
	ELSE IF @intDefaultReportID = 11  -- YTD VS SAME PERIOD LAST YEAR
	BEGIN
		EXEC USP_ReportListMaster_RPEYTDAndSamePeriodLastYear @numDomainID
	END
	ELSE IF @intDefaultReportID = 12  -- ACTION ITEMS & MEETINGS DUE TODAY
	BEGIN

		SELECT
			@vcFilterValue=vcFilterValue
			
		FROM
			ReportDashboard
		WHERE
			numDashBoardID = @numDashBoardID
		
		EXEC USP_ReportListMaster_RPELastMonthAndSamePeriodLastYear @numDomainID,@ClientTimeZoneOffset,@vcFilterValue
		--EXEC USP_ReportListMaster_RPELastMonthAndSamePeriodLastYear @numDomainID
	END
	ELSE IF @intDefaultReportID = 13  --  SALES VS EXPENSES (LAST 12 MONTHS)
	BEGIN
		SELECT
			@vcTimeLine=vcTimeLine
			,@vcGroupBy=vcGroupBy
			,@vcTeritorry=vcTeritorry
			,@vcFilterBy=vcFilterBy
			,@vcFilterValue=vcFilterValue
			,@dtFromDate=dtFromDate
			,@dtToDate=dtToDate
		FROM
			ReportDashboard
		WHERE
			numDashBoardID = @numDashBoardID

		EXEC USP_ReportListMaster_SalesVsExpenseLast12Months @numDomainID,@ClientTimeZoneOffset,@vcTimeLine,@vcGroupBy,@vcTeritorry,@vcFilterBy,@vcFilterValue,@dtFromDate,@dtToDate
	END
	ELSE IF @intDefaultReportID = 14  --  TOP SOURCES OF SALES ORDERS (LAST 12 MONTHS)
	BEGIN

		SELECT
			@vcDealAmount=vcDealAmount
		FROM
			ReportDashboard
		WHERE
			numDashBoardID = @numDashBoardID

		EXEC USP_ReportListMaster_TopSourceOfSalesOrder @numDomainID,@ClientTimeZoneOffset,@vcDealAmount
	
		--EXEC USP_ReportListMaster_TopSourceOfSalesOrder @numDomainID
	END
	ELSE IF @intDefaultReportID = 15  --  TOP 10 ITEMS BY PROFIT MARGIN
	BEGIN
		EXEC USP_ReportListMaster_Top10ItemByProfitMargin @numDomainID
	END
	ELSE IF @intDefaultReportID = 16  --  TOP 10 SALES OPPORTUNITIES BY REVENUE (LAST 12 MONTHS)
	BEGIN

		SELECT
			@numRecordCount=ISNULL(numRecordCount,10)
			,@vcTimeLine=vcTimeLine
			,@vcDealAmount=vcDealAmount
		FROM
			ReportDashboard
		WHERE
			numDashBoardID = @numDashBoardID

		EXEC USP_ReportListMaster_Top10SalesOpportunityByRevenue @numDomainID,@ClientTimeZoneOffset, @numRecordCount, @vcTimeLine, @vcDealAmount
		--EXEC USP_ReportListMaster_Top10SalesOpportunityByRevenue @numDomainID
	END
	ELSE IF @intDefaultReportID = 17  --  TOP 10 SALES OPPORTUNITIES BY TOTAL PROGRESS
	BEGIN
		EXEC USP_ReportListMaster_Top10SalesOpportunityByTotalProgress @numDomainID
	END
	ELSE IF @intDefaultReportID = 18  --  TOP 10 ITEMS RETURNED VS QTY SOLD
	BEGIN

		SELECT
			 @numRecordCount=ISNULL(numRecordCount,10)
		FROM
			ReportDashboard
		WHERE
			numDashBoardID = @numDashBoardID

		EXEC USP_ReportListMaster_Top10ItemsByReturnedVsSold @numDomainID, @numRecordCount
	END
	ELSE IF @intDefaultReportID = 19  --  LARGEST 10 SALES OPPORTUNITIES PAST THEIR DUE DATE
	BEGIN

		SELECT
			@vcTimeLine = vcTimeLine,
			@vcDealAmount = vcDealAmount,
			@vcTeritorry = vcTeritorry,
			@tintOppType=tintOppType,
			@vcFilterBy = vcFilterBy,
			@vcFilterValue = vcFilterValue,
			@tintTotalProgress = tintTotalProgress,
			@tintMinNumber = tintMinNumber,
			@tintMaxNumber = tintMaxNumber,
			@tintQtyToDisplay = tintQtyToDisplay,
			@vcDueDate = vcDueDate
			,@dtFromDate = dtFromDate
			,@dtToDate = dtToDate
		FROM
			ReportDashboard
		WHERE
			numDashBoardID = @numDashBoardID

		EXEC USP_ReportListMaster_Top10SalesOpportunityByPastDue @numDomainID,@ClientTimeZoneOffset,@vcTimeLine,@vcTeritorry,@tintOppType,@vcDealAmount,@vcFilterBy,@vcFilterValue,@tintTotalProgress,@tintMinNumber,@tintMaxNumber,@tintQtyToDisplay,@vcDueDate,@dtFromDate,@dtToDate
	
		--EXEC USP_ReportListMaster_Top10SalesOpportunityByPastDue @numDomainID,@ClientTimeZoneOffset
	END
	ELSE IF @intDefaultReportID = 20  --  Top 10 Campaigns by ROI
	BEGIN
		EXEC USP_ReportListMaster_Top10CampaignsByROI @numDomainID
	END
	ELSE IF @intDefaultReportID = 21  --  MARKET FRAGMENTATION � TOP 10 CUSTOMERS, AS A PORTION TOTAL SALES
	BEGIN
		EXEC USP_ReportListMaster_TopCustomersByPortionOfTotalSales @numDomainID
	END
	ELSE IF @intDefaultReportID = 22  --  SCORE CARD
	BEGIN
		EXEC USP_ReportListMaster_PrebuildScoreCard @numDomainID
	END
	ELSE IF @intDefaultReportID = 23  --  TOP 10 LEAD SOURCES (NEW LEADS)
	BEGIN

		SELECT
			@vcTimeLine=vcTimeLine
			,@vcGroupBy=vcGroupBy
			,@vcTeritorry=vcTeritorry
			,@vcFilterBy=vcFilterBy
			,@vcFilterValue=vcFilterValue
			,@dtFromDate=dtFromDate
			,@dtToDate=dtToDate
			,@numRecordCount=ISNULL(numRecordCount,10)
		FROM
			ReportDashboard
		WHERE
			numDashBoardID = @numDashBoardID

		EXEC USP_ReportListMaster_Top10LeadSource @numDomainID,@ClientTimeZoneOffset,@vcTimeLine,@vcGroupBy,@vcTeritorry,@vcFilterBy,@vcFilterValue,@dtFromDate,@dtToDate,@numRecordCount
	
		--EXEC USP_ReportListMaster_Top10LeadSource @numDomainID
	END
	ELSE IF @intDefaultReportID = 24  --  LAST 10 EMAIL MESSAGES FROM PEOPLE I DO BUSINESS WITH
	BEGIN
		EXEC USP_ReportListMaster_Top10Email @numDomainID,@numUserCntID
	END
	ELSE IF @intDefaultReportID = 25  --  Reminders
	BEGIN
		EXEC USP_ReportListMaster_RemindersPreBuildReport @numDomainID,@ClientTimeZoneOffset
	END
	ELSE IF @intDefaultReportID = 26  --  TOP REASONS WHY WE�RE WINING DEALS
	BEGIN
		
		SELECT
			@tintOppType=tintOppType
			,@vcDealAmount=vcDealAmount
		FROM
			ReportDashboard
		WHERE
			numDashBoardID = @numDashBoardID

		EXEC USP_ReportListMaster_Top10ReasonsDealWins @numDomainID,@ClientTimeZoneOffset, @tintOppType, @vcDealAmount
		--EXEC USP_ReportListMaster_Top10ReasonsDealWins @numDomainID
	END
	ELSE IF @intDefaultReportID = 27  --  TOP REASONS WHY WE�RE LOOSING DEALS
	BEGIN

		SELECT
			@tintOppType=tintOppType
			,@vcDealAmount=vcDealAmount
			,@lngPConclAnalysis= lngPConclAnalysis
		FROM
			ReportDashboard
		WHERE
			numDashBoardID = @numDashBoardID

		EXEC USP_ReportListMaster_Top10ReasonsDealLost @numDomainID,@ClientTimeZoneOffset, @tintOppType, @vcDealAmount, @lngPConclAnalysis
		--EXEC USP_ReportListMaster_Top10ReasonsDealLost @numDomainID
	END
	ELSE IF @intDefaultReportID = 28  --  TOP 10 REASONS FOR SALES RETURNS
	BEGIN

		SELECT
			 @numRecordCount=ISNULL(numRecordCount,10)
		FROM
			ReportDashboard
		WHERE
			numDashBoardID = @numDashBoardID
		EXEC USP_ReportListMaster_Top10ReasonsForSalesReturns @numDomainID, @numDashBoardID
	END
	ELSE IF @intDefaultReportID = 29  --  EMPLOYEE SALES PERFORMANCE PANEL 1
	BEGIN

		SELECT
			@vcTimeLine=vcTimeLine
			,@vcFilterBy=vcFilterBy
			,@vcFilterValue=vcFilterValue
		FROM
			ReportDashboard
		WHERE
			numDashBoardID = @numDashBoardID

		EXEC USP_ReportListMaster_EmployeeSalesPerformancePanel1 @numDomainID,@ClientTimeZoneOffset, @vcTimeLine, @vcFilterBy,@vcFilterValue
		--EXEC USP_ReportListMaster_EmployeeSalesPerformancePanel1 @numDomainID
	END
	ELSE IF @intDefaultReportID = 30  --  PARTNER REVENUES, MARGINS & PROFITS (LAST 12 MONTHS)
	BEGIN
		EXEC USP_ReportListMaster_PartnerRMPLast12Months @numDomainID
	END
	ELSE IF @intDefaultReportID = 31  --  EMPLOYEE SALES PERFORMANCE PT 1 (LAST 12 MONTHS)
	BEGIN
		
		SELECT
			@vcTimeLine=vcTimeLine
			,@vcFilterBy=vcFilterBy
			,@vcFilterValue=vcFilterValue
		FROM
			ReportDashboard
		WHERE
			numDashBoardID = @numDashBoardID

		EXEC USP_ReportListMaster_EmployeeSalesPerformance1 @numDomainID,@ClientTimeZoneOffset, @vcTimeLine, @vcFilterBy,@vcFilterValue
		--EXEC USP_ReportListMaster_EmployeeSalesPerformance1 @numDomainID
	END
	ELSE IF @intDefaultReportID = 32  --  EMPLOYEE BENEFIT TO COMPANY (LAST 12 MONTHS)
	BEGIN

		SELECT
			@vcTimeLine=vcTimeLine
			,@vcFilterBy=vcFilterBy
			,@vcFilterValue=vcFilterValue
		FROM
			ReportDashboard
		WHERE
			numDashBoardID = @numDashBoardID

		EXEC USP_ReportListMaster_EmployeeBenefitToCompany @numDomainID,@ClientTimeZoneOffset, @vcTimeLine, @vcFilterBy,@vcFilterValue
	END
	ELSE IF @intDefaultReportID = 33  --  FIRST 10 SAVED SEARCHES
	BEGIN
		EXEC USP_ReportListMaster_First10SavedSearch @numDomainID
	END
	ELSE IF @intDefaultReportID = 34  --  SALES OPPORTUNITY WON/LOST REPORT
	BEGIN
		SELECT
			@vcTimeLine=vcTimeLine
			,@vcGroupBy=vcGroupBy
			,@vcTeritorry=vcTeritorry
			,@vcFilterBy=vcFilterBy
			,@vcFilterValue=vcFilterValue
			,@dtFromDate=dtFromDate
			,@dtToDate=dtToDate
		FROM
			ReportDashboard
		WHERE
			numDashBoardID = @numDashBoardID

		EXEC USP_ReportListMaster_SalesOppWonLostPreBuildReport @numDomainID,@ClientTimeZoneOffset,@vcTimeLine,@vcGroupBy,@vcTeritorry,@vcFilterBy,@vcFilterValue,@dtFromDate,@dtToDate
	END
	ELSE IF @intDefaultReportID = 35  --  SALES OPPORTUNITY PIPELINE
	BEGIN
		SELECT
			@vcTimeLine=vcTimeLine
			,@vcGroupBy=vcGroupBy
			,@vcTeritorry=vcTeritorry
			,@vcFilterBy=vcFilterBy
			,@vcFilterValue=vcFilterValue
			,@dtFromDate=dtFromDate
			,@dtToDate=dtToDate
		FROM
			ReportDashboard
		WHERE
			numDashBoardID = @numDashBoardID

		EXEC USP_ReportListMaster_SalesOppPipeLine @numDomainID,@ClientTimeZoneOffset,@vcTimeLine,@vcGroupBy,@vcTeritorry,@vcFilterBy,@vcFilterValue,@dtFromDate,@dtToDate
	END
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ReportQueryExecute')
DROP PROCEDURE USP_ReportQueryExecute
GO
CREATE PROCEDURE [dbo].[USP_ReportQueryExecute]     
    @numDomainID NUMERIC(18,0),
    @numUserCntID NUMERIC(18,0),
	@numReportID NUMERIC(18,0),
    @ClientTimeZoneOffset INT, 
    @textQuery NTEXT,
	@numCurrentPage NUMERIC(18,0)
AS                 
BEGIN
	DECLARE @tintReportType INT
	SELECT @tintReportType=ISNULL(tintReportType,0) FROM ReportListMaster WHERE numReportID=@numReportID

	DECLARE @avgCost int
	SET @avgCost =(select TOP 1 numCost from Domain where numDOmainId=@numDomainID)

	IF CHARINDEX('AND WareHouseItems.vcLocation',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'AND WareHouseItems.vcLocation','AND WareHouseItems.numWLocationID')
	END

	IF CHARINDEX('ReturnItems.monPrice',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'ReturnItems.monPrice','CAST(ReturnItems.monPrice AS NUMERIC(18,4))')
	END

	IF CHARINDEX('OpportunityItems.monPrice',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'OpportunityItems.monPrice','CAST(OpportunityItems.monPrice AS NUMERIC(18,4))')
	END

	IF CHARINDEX('isnull(category.numCategoryID,0)',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'isnull(category.numCategoryID,0)' ,'(SELECT ISNULL(STUFF((SELECT '','' + CONVERT(VARCHAR(50) , Category.vcCategoryName) FROM ItemCategory LEFT JOIN Category ON ItemCategory.numCategoryID = Category.numCategoryID WHERE ItemCategory.numItemID = Item.numItemCode FOR XML PATH('''')), 1, 1, ''''),''''))')
	END

	IF CHARINDEX('dbo.FormatedDateFromDate(dateadd(minute,-@ClientTimeZoneOffset,OpportunityBizDocs.dtFullPaymentDate),@numDomainId)',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'dbo.FormatedDateFromDate(dateadd(minute,-@ClientTimeZoneOffset,OpportunityBizDocs.dtFullPaymentDate),@numDomainId)' ,'(CASE 
																																															WHEN ISNULL(OpportunityMaster.tintOppType,0) = 2 AND ISNULL(OpportunityBizDocs.monDealAmount,0) > 0 AND ISNULL(OpportunityBizDocs.monAmountPaid,0) >= ISNULL(OpportunityBizDocs.monDealAmount,0)
																																															THEN dbo.FormatedDateFromDate((SELECT MAX(BPH.dtPaymentDate) FROM BillPaymentDetails BPD INNER JOIN BillPaymentHeader BPH ON BPD.numBillPaymentID=BPH.numBillPaymentID  WHERE BPD.numOppBizDocsID=OpportunityBizDocs.numOppBizDocsId),@numDomainID)
																																															WHEN ISNULL(OpportunityMaster.tintOppType,0) = 1 AND ISNULL(OpportunityBizDocs.monDealAmount,0) > 0 AND ISNULL(OpportunityBizDocs.monAmountPaid,0) >= ISNULL(OpportunityBizDocs.monDealAmount,0)
																																															THEN dbo.FormatedDateFromDate((SELECT MAX(dtCreatedDate) FROM DepositeDetails WHERE DepositeDetails.numOppBizDocsID=OpportunityBizDocs.numOppBizDocsId),@numDomainID)
																																															ELSE ''''
																																														END)')
	END

	--KEEP IT BELOW ABOVE REPLACEMENT
	IF CHARINDEX('OpportunityBizDocs.dtFullPaymentDate',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'OpportunityBizDocs.dtFullPaymentDate' ,'CAST((CASE 
																												WHEN ISNULL(OpportunityMaster.tintOppType,0) = 2 AND ISNULL(OpportunityBizDocs.monDealAmount,0) > 0 AND ISNULL(OpportunityBizDocs.monAmountPaid,0) >= ISNULL(OpportunityBizDocs.monDealAmount,0)
																												THEN (SELECT MAX(BPH.dtPaymentDate) FROM BillPaymentDetails BPD INNER JOIN BillPaymentHeader BPH ON BPD.numBillPaymentID=BPH.numBillPaymentID  WHERE BPD.numOppBizDocsID=OpportunityBizDocs.numOppBizDocsId)
																												WHEN ISNULL(OpportunityMaster.tintOppType,0) = 1 AND ISNULL(OpportunityBizDocs.monDealAmount,0) > 0 AND ISNULL(OpportunityBizDocs.monAmountPaid,0) >= ISNULL(OpportunityBizDocs.monDealAmount,0)
																												THEN (SELECT MAX(dtCreatedDate) FROM DepositeDetails WHERE DepositeDetails.numOppBizDocsID=OpportunityBizDocs.numOppBizDocsId)
																												ELSE ''''
																											END) AS DATE)')
	END

	IF CHARINDEX('isnull(OpportunityMaster.monProfit,''0'')',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'isnull(OpportunityMaster.monProfit,''0'')' ,CONCAT('CAST(ISNULL(OpportunityItems.monTotAmount,0) - (ISNULL(OpportunityItems.numUnitHour,0) * (CASE ',ISNULL(@avgCost,1),' WHEN 3 THEN ISNULL(OpportunityItems.monVendorCost,0) * dbo.fn_UOMConversion(Item.numBaseUnit,Item.numItemCode,@numDomainID,Item.numPurchaseUnit) WHEN 2 THEN ISNULL(OpportunityItems.numCost,0) * dbo.fn_UOMConversion(Item.numBaseUnit,Item.numItemCode,@numDomainID,OpportunityItems.numUOMId) ELSE ISNULL(OpportunityItems.monAvgCost,0) END)) AS DECIMAL(20,5))'))
	END

	-- THIS ONE SHOULD BE AFTER ABOVE ONE
	IF CHARINDEX('OpportunityMaster.monProfit',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'OpportunityMaster.monProfit' ,CONCAT('CAST(ISNULL(OpportunityItems.monTotAmount,0) - (ISNULL(OpportunityItems.numUnitHour,0) * (CASE ',ISNULL(@avgCost,1),' WHEN 3 THEN ISNULL(OpportunityItems.monVendorCost,0) * dbo.fn_UOMConversion(Item.numBaseUnit,Item.numItemCode,@numDomainID,Item.numPurchaseUnit) WHEN 2 THEN ISNULL(OpportunityItems.numCost,0) * dbo.fn_UOMConversion(Item.numBaseUnit,Item.numItemCode,@numDomainID,OpportunityItems.numUOMId) ELSE ISNULL(OpportunityItems.monAvgCost,0) END)) AS DECIMAL(20,5))'))
	END

	IF CHARINDEX('isnull(OpportunityBizDocItems.monProfit,''0'')',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'isnull(OpportunityBizDocItems.monProfit,''0'')' ,CONCAT('CAST(ISNULL(OpportunityBizDocItems.monTotAmount,0) - (ISNULL(OpportunityBizDocItems.numUnitHour,0) * (CASE ',ISNULL(@avgCost,1),' WHEN 3 THEN ISNULL(OpportunityItems.monVendorCost,0) * dbo.fn_UOMConversion(Item.numBaseUnit,Item.numItemCode,@numDomainID,Item.numPurchaseUnit) WHEN 2 THEN ISNULL(OpportunityItems.numCost,0) * dbo.fn_UOMConversion(Item.numBaseUnit,Item.numItemCode,@numDomainID,OpportunityItems.numUOMId) ELSE ISNULL(OpportunityBizDocItems.monAverageCost,ISNULL(OpportunityItems.monAvgCost,0)) END)) AS DECIMAL(20,5))'))
	END

	-- THIS ONE SHOULD BE AFTER ABOVE ONE
	IF CHARINDEX('OpportunityBizDocItems.monProfit',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'OpportunityBizDocItems.monProfit' ,CONCAT('CAST(ISNULL(OpportunityBizDocItems.monTotAmount,0) - (ISNULL(OpportunityBizDocItems.numUnitHour,0) * (CASE ',ISNULL(@avgCost,1),' WHEN 3 THEN ISNULL(OpportunityItems.monVendorCost,0) * dbo.fn_UOMConversion(Item.numBaseUnit,Item.numItemCode,@numDomainID,Item.numPurchaseUnit) WHEN 2 THEN ISNULL(OpportunityItems.numCost,0) * dbo.fn_UOMConversion(Item.numBaseUnit,Item.numItemCode,@numDomainID,OpportunityItems.numUOMId) ELSE ISNULL(OpportunityBizDocItems.monAverageCost,ISNULL(OpportunityItems.monAvgCost,0)) END)) AS DECIMAL(20,5))'))
	END

	IF CHARINDEX('ISNULL(PT.numItemCode,0)=Item.numItemCode',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'ISNULL(PT.numItemCode,0)=Item.numItemCode' ,'Item.numItemCode=PT.numItemCode')
	END

	IF CHARINDEX('isnull(OpportunityMaster.numProfitPercent,0)',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'isnull(OpportunityMaster.numProfitPercent,0)' ,CONCAT('CAST((((ISNULL(OpportunityItems.monTotAmount,0)/ISNULL(NULLIF(OpportunityItems.numUnitHour,0),1)) - (CASE ',ISNULL(@avgCost,1),' WHEN 3 THEN ISNULL(OpportunityItems.monVendorCost,0) * dbo.fn_UOMConversion(Item.numBaseUnit,Item.numItemCode,@numDomainID,Item.numPurchaseUnit) WHEN 2 THEN ISNULL(OpportunityItems.numCost,0) * dbo.fn_UOMConversion(Item.numBaseUnit,Item.numItemCode,@numDomainID,OpportunityItems.numUOMId) ELSE ISNULL(OpportunityItems.monAvgCost,0) END)) / (CASE WHEN (ISNULL(OpportunityItems.monTotAmount,0)/ISNULL(NULLIF(OpportunityItems.numUnitHour,0),1))=0 then 1 ELSE (ISNULL(OpportunityItems.monTotAmount,0)/ISNULL(NULLIF(OpportunityItems.numUnitHour,0),1)) end) * 100) AS NUMERIC(18,2))'))
	END
	
	-- THIS ONE SHOULD BE AFTER ABOVE ONE
	IF CHARINDEX('OpportunityMaster.numProfitPercent',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'OpportunityMaster.numProfitPercent' ,CONCAT('CAST((((ISNULL(OpportunityItems.monTotAmount,0)/ISNULL(NULLIF(OpportunityItems.numUnitHour,0),1)) - (CASE ',ISNULL(@avgCost,1),' WHEN 3 THEN ISNULL(OpportunityItems.monVendorCost,0) * dbo.fn_UOMConversion(Item.numBaseUnit,Item.numItemCode,@numDomainID,Item.numPurchaseUnit) WHEN 2 THEN ISNULL(OpportunityItems.numCost,0) * dbo.fn_UOMConversion(Item.numBaseUnit,Item.numItemCode,@numDomainID,OpportunityItems.numUOMId) ELSE ISNULL(OpportunityItems.monAvgCost,0) END)) / (CASE WHEN (ISNULL(OpportunityItems.monTotAmount,0)/ISNULL(NULLIF(OpportunityItems.numUnitHour,0),1))=0 then 1 ELSE (ISNULL(OpportunityItems.monTotAmount,0)/ISNULL(NULLIF(OpportunityItems.numUnitHour,0),1)) end) * 100) AS NUMERIC(18,2))'))
	END

	IF CHARINDEX('isnull(OpportunityBizDocItems.numProfitPercent,0)',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'isnull(OpportunityBizDocItems.numProfitPercent,0)' ,CONCAT('CAST((((ISNULL(OpportunityBizDocItems.monTotAmount,0)/ISNULL(NULLIF(OpportunityBizDocItems.numUnitHour,0),1)) - (CASE ',ISNULL(@avgCost,1),' WHEN 3 THEN ISNULL(OpportunityItems.monVendorCost,0) * dbo.fn_UOMConversion(Item.numBaseUnit,Item.numItemCode,@numDomainID,Item.numPurchaseUnit) WHEN 2 THEN ISNULL(OpportunityItems.numCost,0) * dbo.fn_UOMConversion(Item.numBaseUnit,Item.numItemCode,@numDomainID,OpportunityItems.numUOMId) ELSE ISNULL(OpportunityBizDocItems.monAverageCost,ISNULL(OpportunityItems.monAvgCost,0)) END)) / (CASE WHEN (ISNULL(OpportunityBizDocItems.monTotAmount,0)/ISNULL(NULLIF(OpportunityBizDocItems.numUnitHour,0),1))=0 then 1 ELSE (ISNULL(OpportunityBizDocItems.monTotAmount,0)/ISNULL(NULLIF(OpportunityBizDocItems.numUnitHour,0),1)) end) * 100) AS NUMERIC(18,2))'))
	END
	
	-- THIS ONE SHOULD BE AFTER ABOVE ONE
	IF CHARINDEX('OpportunityBizDocItems.numProfitPercent',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'OpportunityBizDocItems.numProfitPercent' ,CONCAT('CAST((((ISNULL(OpportunityBizDocItems.monTotAmount,0)/ISNULL(NULLIF(OpportunityBizDocItems.numUnitHour,0),1)) - (CASE ',ISNULL(@avgCost,1),' WHEN 3 THEN ISNULL(OpportunityItems.monVendorCost,0) * dbo.fn_UOMConversion(Item.numBaseUnit,Item.numItemCode,@numDomainID,Item.numPurchaseUnit) WHEN 2 THEN ISNULL(OpportunityItems.numCost,0) * dbo.fn_UOMConversion(Item.numBaseUnit,Item.numItemCode,@numDomainID,OpportunityItems.numUOMId) ELSE ISNULL(OpportunityBizDocItems.monAverageCost,ISNULL(OpportunityItems.monAvgCost,0)) END)) / (CASE WHEN (ISNULL(OpportunityBizDocItems.monTotAmount,0)/ISNULL(NULLIF(OpportunityBizDocItems.numUnitHour,0),1))=0 then 1 ELSE (ISNULL(OpportunityBizDocItems.monTotAmount,0)/ISNULL(NULLIF(OpportunityBizDocItems.numUnitHour,0),1)) end) * 100) AS NUMERIC(18,2))'))
	END

	IF ISNULL(@numCurrentPage,0) > 0 
		AND ISNULL(@tintReportType,0) <> 3 
		AND ISNULL(@tintReportType,0) <> 4 
		AND ISNULL(@tintReportType,0) <> 5 
		AND CHARINDEX('Select top',CAST(@textQuery AS VARCHAR(MAX))) <> 1
		AND (SELECT COUNT(*) FROM ReportSummaryGroupList WHERE numReportID=@numReportID) = 0
	BEGIN
		SET @textQuery = CONCAT(CAST(@textQuery AS VARCHAR(MAX)),' OFFSET ',(@numCurrentPage-1) * 100 ,' ROWS FETCH NEXT 100 ROWS ONLY OPTION (OPTIMIZE FOR (@numDomainID UNKNOWN));')
	END
	ELSE
	BEGIN
		SET @textQuery = CONCAT(CAST(@textQuery AS VARCHAR(MAX)),' OPTION (OPTIMIZE FOR (@numDomainID UNKNOWN));')
	END
	 
	 print CAST(@textQuery AS ntext)
	EXECUTE sp_executesql @textQuery,N'@numDomainID numeric(18, 0),@numUserCntID numeric(18,0),@ClientTimeZoneOffset int',@numDomainID=@numDomainID,@numUserCntID=@numUserCntID,@ClientTimeZoneOffset=@ClientTimeZoneOffset
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_SaveGridColumnWidth')
DROP PROCEDURE USP_SaveGridColumnWidth
GO
CREATE PROCEDURE [dbo].[USP_SaveGridColumnWidth]  
	@numDomainID AS NUMERIC(18,0)=0,  
	@numUserCntID AS NUMERIC(18,0)=0,
	@str AS TEXT
AS  
BEGIN
	declare @hDoc as int     
	EXEC sp_xml_preparedocument @hDoc OUTPUT, @str                                                                        

	CREATE TABLE #tempTable
	(
		numFormId NUMERIC(18,0)
		,numFieldId NUMERIC(9)
		,bitCustom BIT
		,intColumnWidth INT
	)

	INSERT INTO #tempTable 
	(
		numFormId
		,numFieldId
		,bitCustom
		,intColumnWidth
	)                                        
	SELECT 
		numFormId
		,numFieldId
		,bitCustom
		,CAST(intColumnWidth AS INT) 
	FROM 
		OPENXML (@hDoc,'/NewDataSet/GridColumnWidth',2)                                                  
	WITH 
	(
		numFormId numeric(9)
		,numFieldId numeric(9)
		,bitCustom bit
		,intColumnWidth FLOAT
	)                                           
     
	EXEC sp_xml_removedocument @hDoc  

	UPDATE 
		DFCD 
	SET 
		intColumnWidth=temp.intColumnWidth
	FROM 
		DycFormConfigurationDetails DFCD 
	JOIN 
		#tempTable temp
	ON 
		DFCD.numFormId=temp.numFormId 
		AND DFCD.numFieldId=temp.numFieldId 
		AND ISNULL(DFCD.bitCustom,0)=temp.bitCustom
	WHERE 
		DFCD.numDomainID=@numDomainID 
		AND DFCD.numUserCntID=@numUserCntID 
		AND DFCD.tintPagetype=1                                                         

	INSERT INTO DycFormConfigurationDetails
	(
		numDomainId
		,numUserCntID
		,numFormId
		,numFieldId
		,bitCustom
		,intColumnWidth
		,tintPagetype
		,intColumnNum
		,intRowNum
	)
	SELECT 
		@numDomainID
		,@numUserCntID
		,numFormId
		,numFieldId
		,bitCustom
		,intColumnWidth
		,1
		,1
		,1
	FROM
		#tempTable
	WHERE
		numFormId=154
		AND numFieldId NOT IN (SELECT numFieldId FROM DycFormConfigurationDetails WHERE numDomainID=@numDomainID AND numUserCntID=@numUserCntID AND numFormId=154)

	DROP TABLE #tempTable                                        

END
GO


/****** Object:  StoredProcedure [dbo].[usp_SetUsersWithDomains]    Script Date: 07/26/2008 16:21:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_setuserswithdomains')
DROP PROCEDURE usp_setuserswithdomains
GO
CREATE  PROCEDURE [dbo].[usp_SetUsersWithDomains]
	@numUserID NUMERIC(9),                                    
	@vcUserName VARCHAR(50),                                    
	@vcUserDesc VARCHAR(250),                              
	@numGroupId as numeric(9),                                           
	@numUserDetailID numeric ,                              
	@numUserCntID as numeric(9),                              
	@strTerritory as varchar(4000) ,                              
	@numDomainID as numeric(9),
	@strTeam as varchar(4000),
	@vcEmail as varchar(100),
	@vcPassword as varchar(100),          
	@Active as BIT,
	@numDefaultClass NUMERIC,
	@numDefaultWarehouse as numeric(18),
	@vcLinkedinId varchar(300)=null,
	@intAssociate int=0,
	@ProfilePic varchar(100)=null
	,@bitPayroll BIT = 0
	,@monHourlyRate DECIMAL(20,5) = 0
	,@tintPayrollType TINYINT = 0
	,@tintHourType TINYINT = 0
	,@monOverTimeRate DECIMAL(20,5) = 0
AS
BEGIN     
	IF @numUserID=0             
	BEGIN 
		DECLARE @APIPublicKey varchar(20)
		SELECT @APIPublicKey = dbo.GenerateBizAPIPublicKey() 
           
		INSERT INTO UserMaster
		(
			vcUserName
			,vcUserDesc
			,numGroupId
			,numUserDetailId
			,numModifiedBy
			,bintModifiedDate
			,vcEmailID
			,vcPassword
			,numDomainID
			,numCreatedBy
			,bintCreatedDate
			,bitActivateFlag
			,numDefaultClass
			,numDefaultWarehouse
			,vcBizAPIPublicKey
			,vcBizAPISecretKey
			,bitBizAPIAccessEnabled
			,vcLinkedinId
			,intAssociate
			,bitPayroll
			,monHourlyRate
			,tintPayrollType
			,tintHourType
			,monOverTimeRate
		)
		VALUES
		(
			@vcUserName
			,@vcUserDesc
			,@numGroupId
			,@numUserDetailID
			,@numUserCntID
			,GETUTCDATE()
			,@vcEmail
			,@vcPassword
			,@numDomainID
			,@numUserCntID
			,GETUTCDATE()
			,@Active
			,@numDefaultClass
			,@numDefaultWarehouse
			,@APIPublicKey
			,NEWID()
			,0
			,@vcLinkedinId
			,@intAssociate
			,@bitPayroll
			,@monHourlyRate
			,@tintPayrollType
			,@tintHourType
			,(CASE WHEN ISNULL(@monOverTimeRate,0) = 0 THEN @monHourlyRate ELSE @monOverTimeRate END)
		)            
		
		SET @numUserID=@@identity          
           
		INSERT INTO BizAPIThrottlePolicy 
		(
			[RateLimitKey],
			[bitIsIPAddress],
			[numPerSecond],
			[numPerMinute],
			[numPerHour],
			[numPerDay],
			[numPerWeek]
		)
		VALUES
		(
			@APIPublicKey,
			0,
			3,
			60,
			1200,
			28800,
			200000
		)
    
		EXECUTE [dbo].[Resource_Add]   @vcUserName  ,@vcUserDesc  ,@vcEmail  ,1  ,@numDomainID  ,@numUserDetailID  
	END
	ELSE            
	BEGIN            
		UPDATE 
			UserMaster 
		SET 
			vcUserName = @vcUserName,                                    
			vcUserDesc = @vcUserDesc,                              
			numGroupId =@numGroupId,                                    
			numUserDetailId = @numUserDetailID ,                              
			numModifiedBy= @numUserCntID ,                              
			bintModifiedDate= getutcdate(),                          
			vcEmailID=@vcEmail,            
			vcPassword=@vcPassword,          
			bitActivateFlag=@Active,
			numDefaultClass=@numDefaultClass,
			numDefaultWarehouse=@numDefaultWarehouse,
			vcLinkedinId=@vcLinkedinId,
			intAssociate=@intAssociate
			,bitPayroll=@bitPayroll
			,monHourlyRate=@monHourlyRate
			,tintPayrollType=@tintPayrollType
			,tintHourType=@tintHourType
			,monOverTimeRate=(CASE WHEN ISNULL(@monOverTimeRate,0) = 0 THEN @monHourlyRate ELSE @monOverTimeRate END)
		WHERE 
			numUserID = @numUserID          
          
  
		IF NOT EXISTS (SELECT * FROM RESOURCE WHERE numUserCntId = @numUserDetailID and numdomainId = @numDomainId)  
		BEGIN   
			EXECUTE [dbo].[Resource_Add]   @vcUserName  ,@vcUserDesc  ,@vcEmail  ,1  ,@numDomainID  ,@numUserDetailID     
		END      
	END
                       
	DECLARE @separator_position AS INTEGER
	DECLARE @strPosition AS VARCHAR(1000)                 
                                   
	DELETE FROM UserTerritory WHERE numUserCntID = @numUserDetailID and numDomainId=@numDomainID                              
          
	WHILE PATINDEX('%,%' , @strTerritory) <> 0                              
    BEGIN -- Begin for While Loop                              
		SELECT @separator_position = PATINDEX('%,%' , @strTerritory)                        
		SELECT @strPosition = LEFT(@strTerritory, @separator_position - 1)      
		SELECT @strTerritory = STUFF(@strTerritory, 1, @separator_position,'')                              
     
		INSERT INTO UserTerritory 
		(
			numUserCntID
			,numTerritoryID
			,numDomainID
		)                              
		VALUES
		(
			@numUserDetailID
			,@strPosition
			,@numDomainID
		)                        
	END                              
                                           
	DELETE FROM UserTeams WHERE numUserCntID = @numUserDetailID AND numDomainId=@numDomainID                              
                               
	WHILE PATINDEX('%,%' , @strTeam) <> 0                              
    BEGIN -- Begin for While Loop                              
		SELECT @separator_position =  PATINDEX('%,%' , @strTeam)                              
		SELECT @strPosition = left(@strTeam, @separator_position - 1)                  
		SELECT @strTeam = stuff(@strTeam, 1, @separator_position,'')                              
     
		INSERT INTO UserTeams 
		(
			numUserCntID
			,numTeam
			,numDomainID
		)                              
		VALUES 
		(
			@numUserDetailID
			,@strPosition
			,@numDomainID
		)                        
	END              
                
                                      
                              
	DELETE FROM 
		ForReportsByTeam 
	WHERE 
		numUserCntID = @numUserDetailID                              
		AND numDomainId=@numDomainID AND numTeam NOT IN (SELECT numTeam FROM UserTeams WHERE numUserCntID = @numUserCntID AND numDomainId=@numDomainID)                                                
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_TaxReports')
DROP PROCEDURE USP_TaxReports
GO
CREATE PROCEDURE [dbo].[USP_TaxReports]     
    @numDomainID numeric(18, 0),
    @dtFromDate DATETIME,
    @dtToDate DATETIME,
	@ClientTimeZoneOffset INT=0
AS                 
BEGIN                
	SELECT 
		T.numOppBizDocsId,
		T.numReturnHeaderID,
		T.numOppId,
		vcBizDocID,
		TI.vcTaxName,
		T.fltPercentage,
		T.tintTaxType,
		SUM(Case When T.tintOppType=1 then TaxAmount else 0 end) SalesTax,
		SUM(Case When T.tintOppType=2 then TaxAmount else 0 end) PurchaseTax,
		T.INVCreatedDate,
		T.Organization,
		T.vcShipState,
		ISNULL((SELECT SUM(monTotAmtBefDiscount) FROM OpportunityBizDocItems WHERE numOppBizDocID=T.numOppBizDocsId),0) AS SubTotal,
		T.GrandTotal
	FROM 
	(
		SELECT 
			OBD.numOppBizDocsId,
			0 AS numReturnHeaderID1,
		    OBD.vcBizDocID,
			OMTI.*,
			OM.tintOppType,
			ISNULL(dbo.fn_CalOppItemTotalTaxAmt(OM.numDomainId,OMTI.numTaxItemID,OM.numOppId,OBD.numOppBizDocsId),0) AS TaxAmount,
			[dbo].[FormatedDateFromDate](OBD.dtCreatedDate,CONVERT(VARCHAR(10), @numDomainId)) AS INVCreatedDate,
			CI.vcCompanyName AS Organization,
			ISNULL(dbo.fn_getOPPState(OM.numOppId,@numDomainId,2),'') AS vcShipState,			
			OBD.monDealAmount AS GrandTotal
			
		FROM OpportunityMaster OM 
		JOIN OpportunityBizDocs OBD 
		ON OM.numOppId=OBD.numOppId
		JOIN OpportunityMasterTaxItems OMTI 
		ON OMTI.numOppId=OBD.numOppId
		LEFT JOIN DivisionMaster DM
		ON OM.numDivisionId = DM.numDivisionID
		LEFT JOIN CompanyInfo CI
		ON DM.numCompanyID = CI.numCompanyId
		
		WHERE 
			OM.numDomainID=@numDomainID 
			AND OBD.bitAuthoritativeBizDocs=1 
			AND OMTI.fltPercentage>0
			AND OBD.dtFromDate BETWEEN  @dtFromDate AND @dtToDate
		UNION
		SELECT
			0 AS numOppBizDocsId,
			RH.numReturnHeaderID,
		    RH.vcRMA,
			OMTI.*,
			OM.tintOppType,
			RH.monTotalTax * -1 AS TaxAmount,
			[dbo].[FormatedDateFromDate](DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,RH.dtCreatedDate),@numDomainID) AS INVCreatedDate,
			CI.vcCompanyName AS Organization,
			ISNULL(dbo.fn_getOPPState(OM.numOppId,212,2),'') AS vcShipState,			
			RH.monBizDocAmount AS GrandTotal
		FROM
			ReturnHeader RH
		INNER JOIN OpportunityMaster OM ON RH.numOppId=OM.numOppId
		JOIN OpportunityMasterTaxItems OMTI 
		ON OMTI.numOppId=RH.numOppId
		LEFT JOIN DivisionMaster DM
		ON RH.numDivisionId = DM.numDivisionID
		LEFT JOIN CompanyInfo CI
		ON DM.numCompanyID = CI.numCompanyId
		WHERE
			RH.numDomainId=@numDomainID
			AND DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,RH.dtCreatedDate) BETWEEN  @dtFromDate AND @dtToDate
			AND RH.numReturnStatus=303
	) T 
	JOIN 
	(
		SELECT 
			numTaxItemId,
			vcTaxName 
		FROM 
			TaxItems 
		WHERE 
			numDomainID=@numDomainID 
		UNION 
		SELECT 
			0,
			'Sales Tax'
		UNION 
		SELECT 
			1,
			'CRV'
	) TI 
	ON T.numTaxItemId=TI.numTaxItemID 
	WHERE 
		TaxAmount <> 0
	GROUP BY 
		T.vcBizDocID,
		TI.vcTaxName,
		T.fltPercentage,
		T.tintTaxType,
		T.INVCreatedDate,
		T.Organization,
		T.vcShipState,
		T.GrandTotal,
		T.numOppBizDocsId,
		T.numReturnHeaderID,
		T.numOppId
END
GO
/****** Object:  StoredProcedure [dbo].[USP_UpdatedomainDetails]    Script Date: 05/07/2009 21:56:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                                      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_updatedomaindetails')
DROP PROCEDURE usp_updatedomaindetails
GO
CREATE PROCEDURE [dbo].[USP_UpdatedomainDetails]                                      
@numDomainID as numeric(9)=0,                                      
@vcDomainName as varchar(50)='',                                      
@vcDomainDesc as varchar(100)='',                                      
@bitExchangeIntegration as bit,                                      
@bitAccessExchange as bit,                                      
@vcExchUserName as varchar(100)='',                                      
@vcExchPassword as varchar(100)='',                                      
@vcExchPath as varchar(200)='',                                      
@vcExchDomain as varchar(100)='',                                    
@tintCustomPagingRows as tinyint,                                    
@vcDateFormat as varchar(30)='',                                    
@numDefCountry as numeric(9),                                    
@tintComposeWindow as tinyint,                                    
@sintStartDate as smallint,                                    
@sintNoofDaysInterval as smallint,                                    
@tintAssignToCriteria as tinyint,                                    
@bitIntmedPage as bit,                                    
@tintFiscalStartMonth as tinyint,                                      
@tintPayPeriod as tinyint,                        
@numCurrencyID as numeric(9) ,                
@charUnitSystem as char(1)='',              
@vcPortalLogo as varchar(100)='',            
@tintChrForComSearch as tinyint  ,            
@intPaymentGateWay as int, -- DO NOT UPDATE VALUE FROM THIS PARAMETER
@tintChrForItemSearch as tinyint,
@ShipCompany as numeric(9),
@bitMultiCurrency as bit,
@bitCreateInvoice AS tinyint = null,
@numDefaultSalesBizDocID AS NUMERIC(9)=NULL,
@numDefaultPurchaseBizDocID AS NUMERIC(9)=NULL,
@bitSaveCreditCardInfo AS BIT = NULL,
@intLastViewedRecord AS INT=10,
@tintCommissionType AS TINYINT,
@tintBaseTax AS TINYINT,
@bitEmbeddedCost AS BIT,
@numDefaultSalesOppBizDocId NUMERIC=NULL,
@tintSessionTimeOut TINYINT=1,
@tintDecimalPoints TINYINT,
@bitCustomizePortal BIT=0,
@tintBillToForPO TINYINT,
@tintShipToForPO TINYINT,
@tintOrganizationSearchCriteria TINYINT,
@bitDocumentRepositary BIT=0,
@tintComAppliesTo TINYINT=0,
@bitGtoBContact BIT=0,
@bitBtoGContact BIT=0,
@bitGtoBCalendar BIT=0,
@bitBtoGCalendar BIT=0,
@bitExpenseNonInventoryItem BIT=0,
@bitInlineEdit BIT=0,
@bitRemoveVendorPOValidation BIT=0,
@tintBaseTaxOnArea tinyint=0,
@bitAmountPastDue BIT=0,
@monAmountPastDue DECIMAL(20,5),
@bitSearchOrderCustomerHistory BIT=0,
--@bitRentalItem as bit=0,
--@numRentalItemClass as NUMERIC(9)=NULL,
--@numRentalHourlyUOM as NUMERIC(9)=NULL,
--@numRentalDailyUOM as NUMERIC(9)=NULL,
--@tintRentalPriceBasedOn as tinyint=NULL,
@tintCalendarTimeFormat as tinyint=NULL,
@tintDefaultClassType as tinyint=NULL,
@tintPriceBookDiscount as tinyint=0,
@bitAutoSerialNoAssign as bit=0,
@bitIsShowBalance AS BIT = 0,
@numIncomeAccID NUMERIC(18, 0) = 0,
@numCOGSAccID NUMERIC(18, 0) = 0,
@numAssetAccID NUMERIC(18, 0) = 0,
@IsEnableProjectTracking AS BIT = 0,
@IsEnableCampaignTracking AS BIT = 0,
@IsEnableResourceScheduling AS BIT = 0,
@ShippingServiceItem  AS NUMERIC(9)=0,
@numSOBizDocStatus AS NUMERIC(9)=0,
@bitAutolinkUnappliedPayment AS BIT=0,
@numDiscountServiceItemID AS NUMERIC(9)=0,
@vcShipToPhoneNumber as VARCHAR(50)='',
@vcGAUserEmailId as VARCHAR(50)='',
@vcGAUserPassword as VARCHAR(50)='',
@vcGAUserProfileId as VARCHAR(50)='',
@bitDiscountOnUnitPrice AS BIT=0,
@intShippingImageWidth INT = 0,
@intShippingImageHeight INT = 0,
@numTotalInsuredValue NUMERIC(10,2) = 0,
@numTotalCustomsValue NUMERIC(10,2) = 0,
@vcHideTabs VARCHAR(1000) = '',
@bitUseBizdocAmount BIT = 0,
@bitDefaultRateType BIT = 0,
@bitIncludeTaxAndShippingInCommission  BIT = 0,
@bitPurchaseTaxCredit BIT=0,
@bitLandedCost BIT=0,
@vcLanedCostDefault VARCHAR(10)='',
@bitMinUnitPriceRule BIT = 0,
@numAbovePercent AS NUMERIC(18,2),
@numBelowPercent AS NUMERIC(18,2),
@numBelowPriceField AS NUMERIC(18,0),
@vcUnitPriceApprover AS VARCHAR(100),
@numDefaultSalesPricing AS TINYINT,
@bitReOrderPoint AS BIT = 0,
@numReOrderPointOrderStatus AS NUMERIC(18,0) = 0,
@numPODropShipBizDoc AS NUMERIC(18,0) = 0,
@numPODropShipBizDocTemplate AS NUMERIC(18,0) = 0,
@IsEnableDeferredIncome AS BIT = 0,
@bitApprovalforTImeExpense AS BIT=0,
@numCost AS Numeric(9,0)=0,
@intTimeExpApprovalProcess int=0,
@bitApprovalforOpportunity AS BIT=0,
@intOpportunityApprovalProcess int=0,
@numDefaultSalesShippingDoc NUMERIC(18,0)=0,
@numDefaultPurchaseShippingDoc NUMERIC(18,0)=0,
@bitchkOverRideAssignto AS BIT=0,
@vcPrinterIPAddress VARCHAR(15) = '',
@vcPrinterPort VARCHAR(5) = '',
@numDefaultSiteID NUMERIC(18,0) = 0,
--@vcSalesOrderTabs VARCHAR(200)='',
--@vcSalesQuotesTabs VARCHAR(200)='',
--@vcItemPurchaseHistoryTabs VARCHAR(200)='',
--@vcItemsFrequentlyPurchasedTabs VARCHAR(200)='',
--@vcOpenCasesTabs VARCHAR(200)='',
--@vcOpenRMATabs VARCHAR(200)='',
--@bitSalesOrderTabs BIT=0,
--@bitSalesQuotesTabs BIT=0,
--@bitItemPurchaseHistoryTabs BIT=0,
--@bitItemsFrequentlyPurchasedTabs BIT=0,
--@bitOpenCasesTabs BIT=0,
--@bitOpenRMATabs BIT=0,
@bitRemoveGlobalLocation BIT = 0,
--@bitSupportTabs BIT=0,
--@vcSupportTabs VARCHAR(200)='',
@numAuthorizePercentage NUMERIC(18,0)=0,
@bitEDI BIT = 0,
@bit3PL BIT = 0,
@bitAllowDuplicateLineItems BIT = 0,
@tintCommitAllocation TINYINT = 1,
@tintInvoicing TINYINT = 1,
@tintMarkupDiscountOption TINYINT = 1,
@tintMarkupDiscountValue TINYINT = 1,
@bitCommissionBasedOn BIT = 0,
@tintCommissionBasedOn TINYINT = 1,
@bitDoNotShowDropshipPOWindow BIT = 0,
@tintReceivePaymentTo TINYINT = 2,
@numReceivePaymentBankAccount NUMERIC(18,0) = 0,
@numOverheadServiceItemID NUMERIC(18,0) = 0
as                                      
BEGIN
BEGIN TRY
	IF ISNULL(@bitMinUnitPriceRule,0) = 1 AND ((SELECT COUNT(*) FROM UnitPriceApprover WHERE numDomainID=@numDomainID) = 0 OR ISNULL(@intOpportunityApprovalProcess,0) = 0)
	BEGIN
		RAISERROR('INCOMPLETE_MIN_UNIT_PRICE_CONFIGURATION',16,1)
		RETURN
	END

	If ISNULL(@bitApprovalforTImeExpense,0) = 1 AND (SELECT
														COUNT(*) 
													FROM
														UserMaster
													LEFT JOIN
														ApprovalConfig
													ON
														ApprovalConfig.numUserId = UserMaster.numUserDetailId
													WHERE
														UserMaster.numDomainID=@numDomainID
														AND numModule=1
														AND ISNULL(numGroupID,0) > 0
														AND ISNULL(bitActivateFlag,0) = 1
														AND ISNULL(numLevel1Authority,0) = 0
														AND ISNULL(numLevel2Authority,0) = 0
														AND ISNULL(numLevel3Authority,0) = 0
														AND ISNULL(numLevel4Authority,0) = 0
														AND ISNULL(numLevel5Authority,0) = 0) > 0
	BEGIN
		RAISERROR('INCOMPLETE_TIMEEXPENSE_APPROVAL',16,1)
		RETURN
	END

	DECLARE @tintCommitAllocationOld AS TINYINT
	SELECT @tintCommitAllocationOld=ISNULL(tintCommitAllocation,1) FROM Domain WHERE numDomainID=@numDomainID

	IF @tintCommitAllocation <> @tintCommitAllocationOld AND (SELECT COUNT(*) FROM OpportunityMaster WHERE numDomainId=@numDomainID AND tintOppType=1 AND tintOppStatus=1 AND ISNULL(tintshipped,0)=0) > 0
	BEGIN
		RAISERROR('NOT_ALLOWED_CHANGE_COMMIT_ALLOCATION_OPEN_SALES_ORDER',16,1)
		RETURN
	END
                                  
	UPDATE 
		Domain                                       
	SET                                       
		vcDomainName=@vcDomainName,                                      
		vcDomainDesc=@vcDomainDesc,                                      
		bitExchangeIntegration=@bitExchangeIntegration,                                      
		bitAccessExchange=@bitAccessExchange,                                      
		vcExchUserName=@vcExchUserName,                                      
		vcExchPassword=@vcExchPassword,                                      
		vcExchPath=@vcExchPath,                                      
		vcExchDomain=@vcExchDomain,                                    
		tintCustomPagingRows =@tintCustomPagingRows,                                    
		vcDateFormat=@vcDateFormat,                                    
		numDefCountry =@numDefCountry,                                    
		tintComposeWindow=@tintComposeWindow,                                    
		sintStartDate =@sintStartDate,                                    
		sintNoofDaysInterval=@sintNoofDaysInterval,                                    
		tintAssignToCriteria=@tintAssignToCriteria,                                    
		bitIntmedPage=@bitIntmedPage,                                    
		tintFiscalStartMonth=@tintFiscalStartMonth,                                                      
		tintPayPeriod=@tintPayPeriod,
		vcCurrency=isnull((select varCurrSymbol from Currency Where numCurrencyID=@numCurrencyID  ),'')  ,                      
		numCurrencyID=@numCurrencyID,                
		charUnitSystem= @charUnitSystem,              
		vcPortalLogo=@vcPortalLogo ,            
		tintChrForComSearch=@tintChrForComSearch  ,      
		tintChrForItemSearch=@tintChrForItemSearch,
		numShipCompany= @ShipCompany,
		bitMultiCurrency=@bitMultiCurrency,
		bitCreateInvoice= @bitCreateInvoice,
		[numDefaultSalesBizDocId] =@numDefaultSalesBizDocId,
		bitSaveCreditCardInfo=@bitSaveCreditCardInfo,
		intLastViewedRecord=@intLastViewedRecord,
		[tintCommissionType]=@tintCommissionType,
		[tintBaseTax]=@tintBaseTax,
		[numDefaultPurchaseBizDocId]=@numDefaultPurchaseBizDocID,
		bitEmbeddedCost=@bitEmbeddedCost,
		numDefaultSalesOppBizDocId=@numDefaultSalesOppBizDocId,
		tintSessionTimeOut=@tintSessionTimeOut,
		tintDecimalPoints=@tintDecimalPoints,
		bitCustomizePortal=@bitCustomizePortal,
		tintBillToForPO = @tintBillToForPO,
		tintShipToForPO = @tintShipToForPO,
		tintOrganizationSearchCriteria=@tintOrganizationSearchCriteria,
		bitDocumentRepositary=@bitDocumentRepositary,
		bitGtoBContact=@bitGtoBContact,
		bitBtoGContact=@bitBtoGContact,
		bitGtoBCalendar=@bitGtoBCalendar,
		bitBtoGCalendar=@bitBtoGCalendar,
		bitExpenseNonInventoryItem=@bitExpenseNonInventoryItem,
		bitInlineEdit=@bitInlineEdit,
		bitRemoveVendorPOValidation=@bitRemoveVendorPOValidation,
		tintBaseTaxOnArea=@tintBaseTaxOnArea,
		bitAmountPastDue = @bitAmountPastDue,
		monAmountPastDue = @monAmountPastDue,
		bitSearchOrderCustomerHistory=@bitSearchOrderCustomerHistory,
		--bitRentalItem=@bitRentalItem,
		--numRentalItemClass=@numRentalItemClass,
		--numRentalHourlyUOM=@numRentalHourlyUOM,
		--numRentalDailyUOM=@numRentalDailyUOM,
		--tintRentalPriceBasedOn=@tintRentalPriceBasedOn,
		tintCalendarTimeFormat=@tintCalendarTimeFormat,
		tintDefaultClassType=@tintDefaultClassType,
		tintPriceBookDiscount=@tintPriceBookDiscount,
		bitAutoSerialNoAssign=@bitAutoSerialNoAssign,
		bitIsShowBalance = @bitIsShowBalance,
		numIncomeAccID = @numIncomeAccID,
		numCOGSAccID = @numCOGSAccID,
		numAssetAccID = @numAssetAccID,
		IsEnableProjectTracking = @IsEnableProjectTracking,
		IsEnableCampaignTracking = @IsEnableCampaignTracking,
		IsEnableResourceScheduling = @IsEnableResourceScheduling,
		numShippingServiceItemID = @ShippingServiceItem,
		numSOBizDocStatus=@numSOBizDocStatus,
		bitAutolinkUnappliedPayment=@bitAutolinkUnappliedPayment,
		numDiscountServiceItemID=@numDiscountServiceItemID,
		vcShipToPhoneNo = @vcShipToPhoneNumber,
		vcGAUserEMail = @vcGAUserEmailId,
		vcGAUserPassword = @vcGAUserPassword,
		vcGAUserProfileId = @vcGAUserProfileId,
		bitDiscountOnUnitPrice=@bitDiscountOnUnitPrice,
		intShippingImageWidth = @intShippingImageWidth ,
		intShippingImageHeight = @intShippingImageHeight,
		numTotalInsuredValue = @numTotalInsuredValue,
		numTotalCustomsValue = @numTotalCustomsValue,
		vcHideTabs = @vcHideTabs,
		bitUseBizdocAmount = @bitUseBizdocAmount,
		bitDefaultRateType = @bitDefaultRateType,
		bitIncludeTaxAndShippingInCommission = @bitIncludeTaxAndShippingInCommission,
		bitPurchaseTaxCredit=@bitPurchaseTaxCredit,
		bitLandedCost=@bitLandedCost,
		vcLanedCostDefault=@vcLanedCostDefault,
		bitMinUnitPriceRule = @bitMinUnitPriceRule,
		numDefaultSalesPricing = @numDefaultSalesPricing,
		numPODropShipBizDoc=@numPODropShipBizDoc,
		numPODropShipBizDocTemplate=@numPODropShipBizDocTemplate,
		IsEnableDeferredIncome=@IsEnableDeferredIncome,
		bitApprovalforTImeExpense=@bitApprovalforTImeExpense,
		numCost= @numCost,intTimeExpApprovalProcess=@intTimeExpApprovalProcess,
		bitApprovalforOpportunity=@bitApprovalforOpportunity,intOpportunityApprovalProcess=@intOpportunityApprovalProcess,
		numDefaultPurchaseShippingDoc=@numDefaultPurchaseShippingDoc,numDefaultSalesShippingDoc=@numDefaultSalesShippingDoc,
		bitchkOverRideAssignto=@bitchkOverRideAssignto,
		vcPrinterIPAddress=@vcPrinterIPAddress,
		vcPrinterPort=@vcPrinterPort,
		numDefaultSiteID=ISNULL(@numDefaultSiteID,0),
		--vcSalesOrderTabs=@vcSalesOrderTabs,
		--vcSalesQuotesTabs=@vcSalesQuotesTabs,
		--vcItemPurchaseHistoryTabs=@vcItemPurchaseHistoryTabs,
		--vcItemsFrequentlyPurchasedTabs=@vcItemsFrequentlyPurchasedTabs,
		--vcOpenCasesTabs=@vcOpenCasesTabs,
		--vcOpenRMATabs=@vcOpenRMATabs,
		--bitSalesOrderTabs=@bitSalesOrderTabs,
		--bitSalesQuotesTabs=@bitSalesQuotesTabs,
		--bitItemPurchaseHistoryTabs=@bitItemPurchaseHistoryTabs,
		--bitItemsFrequentlyPurchasedTabs=@bitItemsFrequentlyPurchasedTabs,
		--bitOpenCasesTabs=@bitOpenCasesTabs,
		--bitOpenRMATabs=@bitOpenRMATabs,
		bitRemoveGlobalLocation=@bitRemoveGlobalLocation,
		--bitSupportTabs=@bitSupportTabs,
		--vcSupportTabs=@vcSupportTabs,
		numAuthorizePercentage=@numAuthorizePercentage,
		bitEDI=ISNULL(@bitEDI,0),
		bit3PL=ISNULL(@bit3PL,0),
		bitAllowDuplicateLineItems=ISNULL(@bitAllowDuplicateLineItems,0),
		tintCommitAllocation=ISNULL(@tintCommitAllocation,1),
		tintInvoicing = ISNULL(@tintInvoicing,1),
		tintMarkupDiscountOption = ISNULL(@tintMarkupDiscountOption,1),
		tintMarkupDiscountValue = ISNULL(@tintMarkupDiscountValue,1)
		,bitCommissionBasedOn=ISNULL(@bitCommissionBasedOn,0)
		,tintCommissionBasedOn=ISNULL(@tintCommissionBasedOn,1)
		,bitDoNotShowDropshipPOWindow=ISNULL(@bitDoNotShowDropshipPOWindow,0)
		,tintReceivePaymentTo=ISNULL(@tintReceivePaymentTo,2)
		,numReceivePaymentBankAccount = ISNULL(@numReceivePaymentBankAccount,0)
		,numOverheadServiceItemID=@numOverheadServiceItemID
	WHERE 
		numDomainId=@numDomainID

	IF ISNULL(@bitRemoveGlobalLocation,0) = 1
	BEGIN
		DECLARE @TEMPGlobalWarehouse TABLE
		(
			ID INT IDENTITY(1,1)
			,numItemCode NUMERIC(18,0)
			,numWarehouseID NUMERIC(18,0)
			,numWarehouseItemID NUMERIC(28,0)
		)

		INSERT INTO @TEMPGlobalWarehouse
		(
			numItemCode
			,numWarehouseID
			,numWarehouseItemID
		)
		SELECT
			numItemID
			,numWareHouseID
			,numWareHouseItemID
		FROM
			WareHouseItems
		WHERE
			numDomainID=@numDomainID
			AND numWLocationID = -1
			AND ISNULL(numOnHand,0) = 0
			AND ISNULL(numOnOrder,0)=0
			AND ISNULL(numAllocation,0)=0
			AND ISNULL(numBackOrder,0)=0


		DECLARE @i AS INT = 1
		DECLARE @iCount AS INT
		DECLARE @numTempItemCode AS NUMERIC(18,0)
		DECLARE @numTempWareHouseID AS NUMERIC(18,0)
		DECLARE @numTempWareHouseItemID AS NUMERIC(18,0)
		SELECT @iCount=COUNT(*) FROM @TEMPGlobalWarehouse

		WHILE @i <= @iCount
		BEGIN
			SELECT @numTempItemCode=numItemCode,@numTempWareHouseID=numWarehouseID,@numTempWareHouseItemID=numWarehouseItemID FROM @TEMPGlobalWarehouse WHERE ID=@i

			BEGIN TRY
				EXEC USP_AddUpdateWareHouseForItems @numTempItemCode,  
													@numTempWareHouseID,
													@numTempWareHouseItemID,
													'',
													0,
													0,
													0,
													'',
													'',
													@numDomainID,
													'',
													'',
													'',
													0,
													3,
													0,
													0,
													NULL,
													0
			END TRY
			BEGIN CATCH

			END CATCH

			SET @i = @i + 1
		END
	END
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH

END
/****** Object:  StoredProcedure [dbo].[USP_UpdateItem]    Script Date: 07/26/2008 16:21:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_updateitem')
DROP PROCEDURE usp_updateitem
GO
CREATE PROCEDURE [dbo].[USP_UpdateItem]
@OppItemCode as numeric(9),
@ItemCode as numeric(9),
@Units as numeric(9)
as
declare @monPrice as DECIMAL(20,5)
declare @TotalmonPrice as DECIMAL(20,5)     
select @TotalmonPrice=convert(DECIMAL(20,5),dbo.GetPrice(@ItemCode,@Units)) 
select @monPrice=convert(DECIMAL(20,5),dbo.GetPrice(@ItemCode,1))

 update OpportunityItems set numItemCode=@ItemCode,
                             numUnitHour=@Units,
			     monPrice=isnull(@monPrice,0),
			     monTotAmount=isnull(@TotalmonPrice,0)
where numoppitemtCode=@OppItemCode

declare @OppId as numeric(9)
declare @TotalAmount as DECIMAL(20,5)
select @OppId=numOppId  from OpportunityItems where numoppitemtCode=@OppItemCode
select @TotalAmount=sum(monTotAmount)from OpportunityItems where numOppId=@OppId
	update OpportunityMaster set  monPamount=@TotalAmount
	where numOppId=@OppId
GO

GO
/****** Object:  StoredProcedure [dbo].[USP_UpdatingInventory]    Script Date: 06/01/2009 23:48:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj       
--exec usp_updatinginventory 0,181193,1,0,1       
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_updatinginventory')
DROP PROCEDURE usp_updatinginventory
GO
CREATE PROCEDURE [dbo].[USP_UpdatingInventory]        
	@byteMode AS TINYINT=0,  
	@numWareHouseItemID AS NUMERIC(18,0)=0,  
	@Units AS FLOAT,
	@numWOId AS NUMERIC(9)=0,
	@numUserCntID AS NUMERIC(9)=0,
	@numOppId AS NUMERIC(9)=0,
	@numAssembledItemID AS NUMERIC(18,0) = 0,
	@fltQtyRequiredForSingleBuild AS FLOAT = 0   
AS      
BEGIN TRY
BEGIN TRANSACTION
	DECLARE @Description AS VARCHAR(200)
	DECLARE @numItemCode NUMERIC(18)
	DECLARE @numDomain AS NUMERIC(18,0)
	DECLARE @ParentWOID AS NUMERIC(18,0)
	DECLARE @numDivisionID NUMERIC(18,0)
	DECLARE @numOverheadServiceItemID NUMERIC(18,0)

	DECLARE @tintCommitAllocation AS TINYINT
	DECLARE @bitAllocateInventoryOnPickList AS BIT

	SELECT @ParentWOID=numParentWOID FROM WorkOrder WHERE numWOId=@numWOId
	SELECT @numItemCode=numItemID,@numDomain = numDomainID from WareHouseItems where numWareHouseItemID = @numWareHouseItemID

	SELECT @numOverheadServiceItemID=ISNULL(numOverheadServiceItemID,0) FROM Domain WHERE numDomainId=@numDomain

	IF ISNULL(@numOppId,0) > 0
	BEGIN
		SELECT @tintCommitAllocation=ISNULL(tintCommitAllocation,1) FROM Domain WHERE numDomainId=@numDomain
		SELECT @bitAllocateInventoryOnPickList=ISNULL(bitAllocateInventoryOnPickList,0) FROM OpportunityMaster INNER JOIN DivisionMaster ON OpportunityMaster.numDivisionId=DivisionMaster.numDivisionID WHERE numOppId=@numOppId
	END
  
	IF @byteMode=0  -- Aseeembly Item
	BEGIN  
		DECLARE @CurrentAverageCost DECIMAL(20,5)
		DECLARE @TotalCurrentOnHand FLOAT
		DECLARE @newAverageCost DECIMAL(20,5)
	
		SELECT @CurrentAverageCost = (CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE monAverageCost END) FROM dbo.Item WHERE numItemCode =@numItemCode 
		SELECT @TotalCurrentOnHand = ISNULL((SUM(numOnHand) + SUM(numAllocation)),0) FROM dbo.WareHouseItems WHERE numItemID =@numItemCode 
		PRINT @CurrentAverageCost
		PRINT @TotalCurrentOnHand

		IF @numWOId > 0
		BEGIN
			DECLARE @monOverheadCost DECIMAL(20,5) 
			DECLARE @monLabourCost DECIMAL(20,5)

			-- Store hourly rate for all task assignee
			UPDATE 
				SPDT
			SET
				SPDT.monHourlyRate = ISNULL(UM.monHourlyRate,0)
			FROM
				StagePercentageDetailsTask SPDT
			INNER JOIN
				UserMaster UM
			ON
				SPDT.numAssignTo = UM.numUserDetailId
			WHERE
				SPDT.numDomainID = @numDomain
				AND SPDT.numWorkOrderId = @numWOID

			SELECT 
				@newAverageCost = SUM(numQtyItemsReq * ISNULL(I.monAverageCost,0))
			FROM 
				WorkOrderDetails WOD
			LEFT JOIN 
				dbo.Item I 
			ON 
				I.numItemCode = WOD.numChildItemID
			WHERE 
				WOD.numWOId = @numWOId
				AND I.charItemType = 'P'

			IF @numOverheadServiceItemID > 0 AND EXISTS (SELECT numWODetailId FROM WorkOrderDetails WHERE numWOId=@numWOId AND numChildItemID=@numOverheadServiceItemID)
			BEGIN
				SET @monOverheadCost = (SELECT SUM(numQtyItemsReq * ISNULL(Item.monListPrice,0)) FROM WorkOrderDetails WOD INNER JOIN Item ON WOD.numChildItemID=Item.numItemCode WHERE WOD.numWOId=@numWOId AND WOD.numChildItemID=@numOverheadServiceItemID)
			END

			SET @monLabourCost = dbo.GetWorkOrderLabourCost(@numDomain,@numWOId)			

			UPDATE WorkOrder SET monAverageCost=@newAverageCost,monLabourCost=ISNULL(@monLabourCost,0),monOverheadCost=ISNULL(@monOverheadCost,0) WHERE numWOId=@numWOId

			SET @newAverageCost = ((@CurrentAverageCost * @TotalCurrentOnHand)  + (@newAverageCost + ISNULL(@monOverheadCost,0) + ISNULL(@monLabourCost,0))) / (@TotalCurrentOnHand + @Units)
		END
		ELSE
		BEGIN
			SELECT 
				@newAverageCost = SUM((numQtyItemsReq * dbo.fn_UOMConversion(ID.numUOMID,I.numItemCode,I.numDomainId,I.numBaseUnit)) * I.monAverageCost )
			FROM 
				dbo.ItemDetails ID
			LEFT JOIN 
				dbo.Item I 
			ON 
				I.numItemCode = ID.numChildItemID
			WHERE 
				numItemKitID = @numItemCode
				AND I.charItemType = 'P'

			UPDATE AssembledItem SET monAverageCost=@newAverageCost WHERE ID=@numAssembledItemID

			SET @newAverageCost = ((@CurrentAverageCost * @TotalCurrentOnHand)  + (@newAverageCost * @Units)) / (@TotalCurrentOnHand + @Units)
		END		
	
		UPDATE item SET monAverageCost = (CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE @newAverageCost END) WHERE numItemCode = @numItemCode

		IF ISNULL(@numWOId,0) > 0
		BEGIN
			UPDATE 
				WareHouseItems 
			SET 
				numOnHand = ISNULL(numOnHand,0) + (CASE WHEN ISNULL(numBackOrder,0) >= @Units THEN 0 ELSE @Units - ISNULL(numBackOrder,0) END)
				,numAllocation = ISNULL(numAllocation,0) + (CASE WHEN ISNULL(numBackOrder,0) > @Units THEN @Units ELSE ISNULL(numBackOrder,0) END)
				,numBackOrder = (CASE WHEN ISNULL(numBackOrder,0) > @Units THEN ISNULL(numBackOrder,0) - @Units ELSE 0 END)  
				,numOnOrder = (CASE WHEN @tintCommitAllocation=2 AND @bitAllocateInventoryOnPickList=0 AND @numOppId > 0 THEN numOnOrder ELSE (CASE WHEN ISNULL(numOnOrder,0) < @Units THEN 0 ELSE ISNULL(numOnOrder,0) - @Units END) END) 
				,dtModified = GETDATE()  
			WHERE 
				numWareHouseItemID=@numWareHouseItemID  
		END
		ELSE
		BEGIN
			UPDATE 
				WareHouseItems 
			SET 
				numOnHand = ISNULL(numOnHand,0) + (CASE WHEN ISNULL(numBackOrder,0) >= @Units THEN  0 ELSE @Units - ISNULL(numBackOrder,0) END)
				,numAllocation = ISNULL(numAllocation,0) + (CASE WHEN ISNULL(numBackOrder,0) > @Units THEN @Units ELSE ISNULL(numBackOrder,0) END)
				,numBackOrder = (CASE WHEN ISNULL(numBackOrder,0) > @Units THEN ISNULL(numBackOrder,0) - @Units ELSE 0 END)
				,dtModified = GETDATE() 
			WHERE 
				numWareHouseItemID=@numWareHouseItemID  
		END
	END  
	ELSE IF @byteMode=1  --Aseembly Child item
	BEGIN  
		DECLARE @onHand as FLOAT                                    
		DECLARE @onOrder as FLOAT                                    
		DECLARE @onBackOrder as FLOAT                                   
		DECLARE @onAllocation as FLOAT    
		DECLARE @monAverageCost AS DECIMAL(20,5)


		SELECT @monAverageCost= (CASE WHEN ISNULL(bitVirtualInventory,0)=1 THEN 0 ELSE ISNULL(monAverageCost,0) END) FROM Item WHERE  numItemCode=@numItemCode

		IF @numWOId > 0
		BEGIN
			UPDATE
				WorkOrderDetails
			SET
				monAverageCost=@monAverageCost
			WHERE
				numWOId=@numWOId
				AND numChildItemID=@numItemCode
				AND numWareHouseItemId=@numWareHouseItemID
		END
		ELSE
		BEGIN
			-- KEEP HISTRIY OF AT WHICH PRICE CHILD ITEM IS USED IN ASSEMBLY BUILDING
			-- USEFUL IN CALCULATING AVERAGECOST WHEN DISASSEMBLING ITEM
			INSERT INTO [dbo].[AssembledItemChilds]
			(
				numAssembledItemID,
				numItemCode,
				numQuantity,
				numWarehouseItemID,
				monAverageCost,
				fltQtyRequiredForSingleBuild
			)
			VALUES
			(
				@numAssembledItemID,
				@numItemCode,
				@Units,
				@numWareHouseItemID,
				@monAverageCost,
				@fltQtyRequiredForSingleBuild
			)
		END

		select                                     
			@onHand=isnull(numOnHand,0),                                    
			@onAllocation=isnull(numAllocation,0),                                    
			@onOrder=isnull(numOnOrder,0),                                    
			@onBackOrder=isnull(numBackOrder,0)                                     
		from 
			WareHouseItems 
		where 
			numWareHouseItemID=@numWareHouseItemID  


		IF ISNULL(@numWOId,0) > 0
		BEGIN
			IF @tintCommitAllocation=2 AND @bitAllocateInventoryOnPickList=0 AND @numOppId > 0 -- Allocation When Fulfillment Order (Packing Slip) is added
			BEGIN
				IF @onHand >= @Units
				BEGIN
					--RELEASE QTY FROM ON HAND
					SET @onHand=@onHand-@Units
				END
				ELSE
				BEGIN
					RAISERROR('NOTSUFFICIENTQTY_ONHAND',16,1)
				END
			END
			ELSE
			BEGIN
				IF @onAllocation >= @Units
				BEGIN
					--RELEASE QTY FROM ALLOCATION
					SET @onAllocation=@onAllocation-@Units
				END
				ELSE
				BEGIN
					RAISERROR('NOTSUFFICIENTQTY_ALLOCATION',16,1)
				END
			END
		END
		ELSE
		BEGIN
			--DECREASE QTY FROM ONHAND
			SET @onHand=@onHand-@Units
		END

		 --UPDATE INVENTORY
		UPDATE 
			WareHouseItems 
		SET      
			numOnHand=@onHand,
			numOnOrder=@onOrder,                         
			numAllocation=@onAllocation,
			numBackOrder=@onBackOrder,
			dtModified = GETDATE()                                    
		WHERE 
			numWareHouseItemID=@numWareHouseItemID   
	END

	IF @numWOId>0
	BEGIN
    
		DECLARE @numReferenceID NUMERIC(18,0)
		DECLARE @tintRefType NUMERIC(18,0)

		IF ISNULL(@numOppId,0)>0 --FROM SALES ORDER 
		BEGIN
			IF @byteMode = 0 --Aseeembly Item
				SET @Description = 'SO-WO Work Order Completed (Qty: ' + CONVERT(varchar(10),@Units) + ')'
			ELSE IF @byteMode =1 --Aseembly Child item
				IF ISNULL(@ParentWOID,0) = 0
				BEGIN
					SET @Description = 'Items Used In SO-WO (Qty: ' + CONVERT(varchar(10),@Units) + ')'
				END
				ELSE
				BEGIN
					SET @Description = 'Items Used In SO-WO Work Order (Qty: ' + CONVERT(varchar(10),@Units) + ')'
				END

			SET @numReferenceID = @numOppId
			SET @tintRefType = 3
		END
		ELSE --FROM CREATE ASSEMBLY SCREEN FROM ITEM
		BEGIN
			IF @byteMode = 0 --Aseeembly Item
				SET @Description = 'Work Order Completed (Qty: ' + CONVERT(varchar(10),@Units) + ')'
			ELSE IF @byteMode =1 --Aseembly Child item
				SET @Description = 'Items Used In Work Order (Qty: ' + CONVERT(varchar(10),@Units) + ')'

			SET @numReferenceID = @numWOId
			SET @tintRefType=2
		END

		EXEC dbo.USP_ManageWareHouseItems_Tracking
					@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
					@numReferenceID = @numReferenceID, --  numeric(9, 0)
					@tintRefType = @tintRefType, --  tinyint
					@vcDescription = @Description, --  varchar(100)
					@numModifiedBy = @numUserCntID,
					@numDomainID = @numDomain
	END
	ELSE if @byteMode=0
	BEGIN
	DECLARE @desc AS VARCHAR(100)
	SET @desc= 'Create Assembly:' + CONVERT(varchar(10),@Units) + ' Units';

	  EXEC dbo.USP_ManageWareHouseItems_Tracking
					@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
					@numReferenceID = @numItemCode, --  numeric(9, 0)
					@tintRefType = 1, --  tinyint
					@vcDescription = @desc, --  varchar(100)
					@numModifiedBy = @numUserCntID,
					@numDomainID = @numDomain
	END
	ELSE if @byteMode=1
	BEGIN
	SET @Description = 'Item Used in Assembly (Qty: ' + CONVERT(varchar(10),@Units) + ')' 

	EXEC dbo.USP_ManageWareHouseItems_Tracking
					@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
					@numReferenceID = @numItemCode, --  numeric(9, 0)
					@tintRefType = 1, --  tinyint
					@vcDescription = @Description, --  varchar(100)
					@numModifiedBy = @numUserCntID,
					@numDomainID = @numDomain
	END 
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SET @numOppID = 0

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_WorkOrder_GetWorkOrderDetailByID')
DROP PROCEDURE USP_WorkOrder_GetWorkOrderDetailByID
GO
CREATE PROCEDURE [dbo].[USP_WorkOrder_GetWorkOrderDetailByID]  
	@numDomainID NUMERIC(18,0),
	@numWOID NUMERIC(18,0),
	@ClientTimeZoneOffset INT
AS
BEGIN
	SELECT 
		WO.numWOId,
		WO.monAverageCost,
		I.numItemCode,
		I.vcItemName,
		I.numAssetChartAcntId,
		WO.numQtyItemsReq,
		WO.vcWorkOrderName,
		WO.numBuildProcessId,
		WO.numWOStatus,
		dbo.GetTotalProgress(@numDomainID,WO.numWOId,1,1,'',0) numTotalProgress,
		CONCAT(dbo.fn_GetContactName(WO.numCreatedby),' ',dbo.FormatedDateTimeFromDate(DATEADD(MINUTE,-@ClientTimeZoneOffset,WO.bintCreatedDate),@numDomainID)) AS CreatedBy,
        CONCAT(dbo.fn_GetContactName(WO.numModifiedBy),' ',dbo.FormatedDateTimeFromDate(DATEADD(MINUTE,-@ClientTimeZoneOffset,WO.bintModifiedDate),@numDomainID)) AS ModifiedBy,
		CONCAT(dbo.fn_GetContactName(WO.numCompletedBy),' ',dbo.FormatedDateTimeFromDate(DATEADD(MINUTE,-@ClientTimeZoneOffset,WO.bintCompletedDate),@numDomainID)) AS CompletedBy
	FROM
		WorkOrder WO
	INNER JOIN
		Item I
	ON
		WO.numItemCode = I.numItemCode
	WHERE
		WO.numDomainID=@numDomainID
		AND WO.numWOId = @numWOID

	SELECT
		WOD.numQtyItemsReq AS RequiredQty,
		WOD.monAverageCost,
		I.numItemCode,
		I.vcItemName,
		I.numAssetChartAcntId
	FROM 
		WorkOrderDetails WOD
	INNER JOIN
		Item I
	ON
		WOD.numChildItemID = I.numItemCode
	WHERE
		WOD.numWOId=@numWOID
		AND I.charItemType = 'P'

END
GO
--select dbo.GetDealAmount(1133,getdate(),1610)
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='GetPaidAmount')
DROP FUNCTION GetPaidAmount
GO
CREATE FUNCTION [dbo].[GetPaidAmount]
(@numOppID numeric)    
returns DECIMAL(20,5)    
as    
begin   
DECLARE @TotalAmtPaid AS DECIMAL(20,5)
SET @TotalAmtPaid=(SELECT SUM(isnull(monAmountPaid ,0)) FROM OpportunityBizDocs WHERE numOppId=@numOppID)
return CAST(@TotalAmtPaid AS DECIMAL(20,5)) -- Set Accuracy of Two precision
END
GO
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_WorkSchedule_GetAll')
DROP PROCEDURE dbo.USP_WorkSchedule_GetAll
GO
CREATE PROCEDURE [dbo].[USP_WorkSchedule_GetAll]
(
	@numDomainID NUMERIC(18,0)
)
AS 
BEGIN
	SELECT
		UserMaster.numUserId
		,UserMaster.numUserDetailId
		,UserMaster.vcUserName
		,ISNULL(WorkSchedule.ID,0) AS numWorkScheduleID
		,WorkSchedule.numWorkHours
		,WorkSchedule.numWorkMinutes
		,WorkSchedule.numProductiveHours
		,WorkSchedule.numProductiveMinutes
		,WorkSchedule.tmStartOfDay
		,WorkSchedule.vcWorkDays
		,stuff((
			SELECT 
				CONCAT(', ',(CASE 
								WHEN WorkScheduleDaysOff.dtDayOffFrom = WorkScheduleDaysOff.dtDayOffTo 
								THEN dbo.FormatedDateFromDate(WorkScheduleDaysOff.dtDayOffFrom,@numDomainID) 
								ELSE CONCAT(dbo.FormatedDateFromDate(WorkScheduleDaysOff.dtDayOffFrom,@numDomainID),' - ',dbo.FormatedDateFromDate(WorkScheduleDaysOff.dtDayOffTo,@numDomainID))
							END))
			FROM 
				WorkScheduleDaysOff
			WHERE
				 numWorkScheduleID = ISNULL(WorkSchedule.ID,0)
			ORDER BY 
				dtDayOffFrom
			FOR XML PATH('')
		),1,1,'') AS vcDaysOff
	FROM
		UserMaster
	LEFT JOIN
		WorkSchedule
	ON
		UserMaster.numUserDetailId = WorkSchedule.numUserCntID
		AND WorkSchedule.numDomainID=@numDomainID
	WHERE
		UserMaster.numDomainID=@numDomainID
		AND ISNULL(UserMaster.bitActivateFlag,0) = 1
	ORDER BY
		vcUserName
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_TimeAndExpense_GetExpenseEntries')
DROP PROCEDURE USP_TimeAndExpense_GetExpenseEntries
GO
CREATE PROCEDURE [dbo].[USP_TimeAndExpense_GetExpenseEntries]
(
	@numDomainID NUMERIC(18,0)
	,@dtFromDate DATE
	,@dtToDate DATE
	,@vcEmployeeName VARCHAR(200)
	,@vcTeams VARCHAR(MAX)
	,@tintPayrollType TINYINT
	,@ClientTimeZoneOffset INT
	,@vcSortColumn VARCHAR(200)
	,@vcSortOrder VARCHAR(4)
)
AS
BEGIN
	DECLARE @TEMP TABLE
	(
		numUserCntID NUMERIC(18,0)
		,vcEmployee VARCHAR(200)
		,vcTeam VARCHAR(200)
		,monExpense DECIMAL(20,5)
		,vcPayrollType VARCHAR(50)
	)

	INSERT INTO @TEMP
	(
		numUserCntID
		,vcEmployee
		,vcTeam
		,monExpense
		,vcPayrollType
	)
	SELECT
		UserMaster.numUserDetailId
		,CONCAT(ISNULL(AdditionalContactsInformation.vcFirstName,'-'),' ',ISNULL(AdditionalContactsInformation.vcLastName,'-'))
		,dbo.GetListIemName(AdditionalContactsInformation.numTeam)
		,TEMPExpense.monExpense
		,(CASE ISNULL(tintPayrollType,0) WHEN 2 THEN 'Salary' WHEN 1 THEN 'Hourly' ELSE '' END) 
	FROM
		UserMaster
	INNER JOIN
		AdditionalContactsInformation
	ON
		UserMaster.numUserDetailId = AdditionalContactsInformation.numContactId
	OUTER APPLY
	(
		SELECT 
			ISNULL(SUM(monExpense),0) monExpense
		FROM
			dbo.fn_GetPayrollEmployeeExpense(@numDomainID,UserMaster.numUserDetailId,@dtFromDate,@dtToDate,@ClientTimeZoneOffset)
	) TEMPExpense
	WHERE
		UserMaster.numDomainID=@numDomainID
		AND ISNULL(UserMaster.bitActivateFlag,0) = 1
		AND (ISNULL(@vcEmployeeName,'') = '' OR AdditionalContactsInformation.vcFirstName LIKE CONCAT('%',@vcEmployeeName,'%') OR AdditionalContactsInformation.vcLastName LIKE CONCAT('%',@vcEmployeeName,'%'))
		AND (ISNULL(@vcTeams,'') = '' OR AdditionalContactsInformation.numTeam IN (SELECT Id FROM dbo.SplitIDs(@vcTeams,',')))
		AND 1 = (CASE WHEN @tintPayrollType=0 THEN 1 ELSE (CASE WHEN ISNULL(UserMaster.tintPayrollType,0)=@tintPayrollType THEN 1 ELSE 0 END) END)

	IF ISNULL(@vcSortColumn,'') = ''
	BEGIN
		SET @vcSortColumn = 'vcEmployee'
		SET @vcSortOrder = 'ASC'
	END

	IF ISNULL(@vcSortColumn,'') = ''
	BEGIN
		SET @vcSortOrder = 'ASC'
	END


	SELECT 
		* 
	FROM 
		@TEMP 
	ORDER BY
		CASE WHEN @vcSortColumn = 'vcEmployee' AND @vcSortOrder = 'ASC' then vcEmployee END ASC,
		CASE WHEN @vcSortColumn = 'vcEmployee' AND @vcSortOrder = 'DESC' then vcEmployee END DESC,
		CASE WHEN @vcSortColumn = 'vcTeam' AND @vcSortOrder = 'ASC' then vcTeam END ASC,
		CASE WHEN @vcSortColumn = 'vcTeam' AND @vcSortOrder = 'DESC' then vcTeam END DESC,
		CASE WHEN @vcSortColumn = 'vcPayrollType' AND @vcSortOrder = 'ASC' then vcPayrollType END ASC,
		CASE WHEN @vcSortColumn = 'vcPayrollType' AND @vcSortOrder = 'DESC' then vcPayrollType END DESC
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_TimeAndExpense_GetExpenseEntriesDetail')
DROP PROCEDURE USP_TimeAndExpense_GetExpenseEntriesDetail
GO
CREATE PROCEDURE [dbo].[USP_TimeAndExpense_GetExpenseEntriesDetail]
(
    @numDomainID NUMERIC(18,0)
	,@numUserCntID INT
	,@dtFromDate DATE
	,@dtToDate DATE
	,@ClientTimeZoneOffset INT
)
AS 
BEGIN
	SELECT 
		*
		,@numUserCntID AS numUserCntID
	FROM
		dbo.fn_GetPayrollEmployeeExpense(@numDomainID,@numUserCntID,@dtFromDate,@dtToDate,@ClientTimeZoneOffset)
	ORDER BY
		dtDate
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_TimeAndExpense_GetTimeSummary')
DROP PROCEDURE USP_TimeAndExpense_GetTimeSummary
GO
CREATE PROCEDURE [dbo].[USP_TimeAndExpense_GetTimeSummary]
(
	@numDomainID NUMERIC(18,0)
	,@dtFromDate DATE
	,@dtToDate DATE
	,@vcEmployeeName VARCHAR(200)
	,@vcTeams VARCHAR(MAX)
	,@tintPayrollType TINYINT
	,@ClientTimeZoneOffset INT
)
AS
BEGIN
	DECLARE @TEMP TABLE
	(
		numType TINYINT
		,numMinutes INT
	)

	INSERT INTO @TEMP
	(
		numType
		,numMinutes
	)
	SELECT
		TEMPTotalTime.numType
		,TEMPTotalTime.numMinutes
	FROM
		UserMaster
	INNER JOIN
		AdditionalContactsInformation
	ON
		UserMaster.numUserDetailId = AdditionalContactsInformation.numContactId
	OUTER APPLY
	(
		SELECT 
			*
		FROM
			dbo.fn_GetPayrollEmployeeTime(@numDomainID,UserMaster.numUserDetailId,@dtFromDate,@dtToDate,@ClientTimeZoneOffset)
	) TEMPTotalTime
	WHERE
		UserMaster.numDomainID=@numDomainID
		AND ISNULL(UserMaster.bitActivateFlag,0) = 1
		AND (ISNULL(@vcEmployeeName,'') = '' OR AdditionalContactsInformation.vcFirstName LIKE CONCAT('%',@vcEmployeeName,'%') OR AdditionalContactsInformation.vcLastName LIKE CONCAT('%',@vcEmployeeName,'%'))
		AND (ISNULL(@vcTeams,'') = '' OR AdditionalContactsInformation.numTeam IN (SELECT Id FROM dbo.SplitIDs(@vcTeams,',')))
		AND 1 = (CASE WHEN @tintPayrollType=0 THEN 1 ELSE (CASE WHEN ISNULL(UserMaster.tintPayrollType,0)=@tintPayrollType THEN 1 ELSE 0 END) END)


	DECLARE @numTotalTime NUMERIC(18,0)
	DECLARE @numTotalBillableTime NUMERIC(18,0)
	DECLARE @numTotalNonBillableTime NUMERIC(18,0)
	SET @numTotalTime = ISNULL((SELECT SUM(numMinutes) FROM @TEMP WHERE numType NOT IN (4)),0)
	SET @numTotalBillableTime = ISNULL((SELECT SUM(numMinutes) FROM @TEMP WHERE numType = 1),0)
	SET @numTotalNonBillableTime = ISNULL((SELECT SUM(numMinutes) FROM @TEMP WHERE  numType NOT IN (1,4)),0)

	SELECT 
		CONCAT(FORMAT(@numTotalTime/60,'00'),':',FORMAT(@numTotalTime%60,'00')) vcTotalTime
		,CONCAT(FORMAT(@numTotalBillableTime/60,'00'),':',FORMAT(@numTotalBillableTime%60,'00')) vcTotalBillableTime
		,CONCAT(FORMAT(@numTotalNonBillableTime/60,'00'),':',FORMAT(@numTotalNonBillableTime%60,'00')) vcTotalNonBillableTime
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_TimeAndExpense_GetExpenseSummary')
DROP PROCEDURE USP_TimeAndExpense_GetExpenseSummary
GO
CREATE PROCEDURE [dbo].[USP_TimeAndExpense_GetExpenseSummary]
(
	@numDomainID NUMERIC(18,0)
	,@dtFromDate DATE
	,@dtToDate DATE
	,@vcEmployeeName VARCHAR(200)
	,@vcTeams VARCHAR(MAX)
	,@tintPayrollType TINYINT
	,@ClientTimeZoneOffset INT
)
AS
BEGIN
	DECLARE @TEMP TABLE
	(
		numType TINYINT
		,monExpense DECIMAL(20,5)
	)

	INSERT INTO @TEMP
	(
		numType
		,monExpense
	)
	SELECT
		numType
		,monExpense
	FROM
		UserMaster
	INNER JOIN
		AdditionalContactsInformation
	ON
		UserMaster.numUserDetailId = AdditionalContactsInformation.numContactId
	OUTER APPLY
	(
		SELECT 
			numType
			,monExpense
		FROM
			dbo.fn_GetPayrollEmployeeExpense(@numDomainID,UserMaster.numUserDetailId,@dtFromDate,@dtToDate,@ClientTimeZoneOffset)
	) TEMPExpense
	WHERE
		UserMaster.numDomainID=@numDomainID
		AND ISNULL(UserMaster.bitActivateFlag,0) = 1
		AND (ISNULL(@vcEmployeeName,'') = '' OR AdditionalContactsInformation.vcFirstName LIKE CONCAT('%',@vcEmployeeName,'%') OR AdditionalContactsInformation.vcLastName LIKE CONCAT('%',@vcEmployeeName,'%'))
		AND (ISNULL(@vcTeams,'') = '' OR AdditionalContactsInformation.numTeam IN (SELECT Id FROM dbo.SplitIDs(@vcTeams,',')))
		AND 1 = (CASE WHEN @tintPayrollType=0 THEN 1 ELSE (CASE WHEN ISNULL(UserMaster.tintPayrollType,0)=@tintPayrollType THEN 1 ELSE 0 END) END)


	SELECT 
		ISNULL((SELECT CONVERT(varchar(5),DATEADD(MINUTE,ISNULL(SUM(monExpense),0),0),108) FROM @TEMP),0) monTotalExpense
		,ISNULL((SELECT CONVERT(varchar(5),DATEADD(MINUTE,ISNULL(SUM(monExpense),0),0),108) FROM @TEMP WHERE numType = 1),0) monTotalBillableExpense
		,ISNULL((SELECT CONVERT(varchar(5),DATEADD(MINUTE,ISNULL(SUM(monExpense),0),0),108) FROM @TEMP WHERE numType = 2),0) monTotalReimbursableExpense
		,ISNULL((SELECT CONVERT(varchar(5),DATEADD(MINUTE,ISNULL(SUM(monExpense),0),0),108) FROM @TEMP WHERE numType = 6),0) monTotalBillableReimbursableExpense
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_Item_GetAssemblyDetail')
DROP PROCEDURE USP_Item_GetAssemblyDetail
GO
CREATE PROCEDURE [dbo].[USP_Item_GetAssemblyDetail]
(
	@numDomainID NUMERIC(18,0)
	,@numItemCode NUMERIC(18,0)
	,@numWarehouseItemID NUMERIC(18,0)
	,@ClientTimeZoneOffset INT
)
AS
BEGIN
	DECLARE @numTempWarehouseID NUMERIC(18,0)
	SELECT @numTempWarehouseID=numWareHouseID FROM WareHouseItems WHERE numWareHouseItemID=@numWarehouseItemID

	SELECT
		dbo.FormatedDateFromDate(DATEADD(MINUTE,-1*@ClientTimeZoneOffset,GETUTCDATE()),@numDomainID) AS dtShipDate
		,dbo.fn_GetAssemblyPossibleWOQty(@numItemCode,@numWarehouseItemID) AS numMaxWOQty
		,CASE WHEN ISNULL(Item.bitCalAmtBasedonDepItems,0) = 1 THEN ISNULL((SELECT monMSRPPrice FROM dbo.fn_GetKitAssemblyCalculatedPrice(@numDomainID,0,@numItemCode,1,@numWarehouseItemID,ISNULL(tintKitAssemblyPriceBasedOn,1),0,0,'')),0) ELSE ISNULL(monListPrice,0) END AS monPrice 
	FROM
		Item
	WHERE
		numDomainID=@numDomainID
		AND numItemCode = @numItemCode


	;WITH CTE (sintOrder,numItemCode,vcItemName,numQuantity,monListPrice,inLevel) AS
	(
		SELECT 
			ISNULL(ItemDetails.sintOrder,0)
			,Item.numItemCode
			,CONCAT(Item.vcItemName,' (',ISNULL(Item.vcSKU,'-'),')')
			,CAST((1 * ISNULL(ItemDetails.numQtyItemsReq,0) * ISNULL(dbo.fn_UOMConversion(ISNULL(ItemDetails.numUOMID,0), Item.numItemCode, @numDomainID, ISNULL(Item.numBaseUnit, 0)),1)) AS FLOAT)
			,ISNULL(Item.monListPrice,0)
			,0
		FROM 
			ItemDetails
		INNER JOIN
			Item
		ON
			ItemDetails.numChildItemID = Item.numItemCode
		WHERE
			numItemKitID=@numItemCode
		UNION ALL
		SELECT 
			ISNULL(ItemDetails.sintOrder,0)
			,Item.numItemCode
			,CONCAT(Item.vcItemName,' (',ISNULL(Item.vcSKU,'-'),')')
			,CAST((ISNULL(C.numQuantity,0) * ISNULL(ItemDetails.numQtyItemsReq,0) * ISNULL(dbo.fn_UOMConversion(ISNULL(ItemDetails.numUOMID, 0), Item.numItemCode, @numDomainID, ISNULL(Item.numBaseUnit, 0)),1)) AS FLOAT)
			,ISNULL(Item.monListPrice,0)
			,C.inLevel + 1
		FROM 
			CTE C
		INNER JOIN
			ItemDetails
		ON
			C.numItemCode = ItemDetails.numItemKitID
		INNER JOIN
			Item
		ON
			ItemDetails.numChildItemID = Item.numItemCode
	)

	SELECT * FROM CTE ORDER BY sintOrder ASC,inLevel ASC

END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_UserMaster_UpdateDashboardTemplate')
DROP PROCEDURE USP_UserMaster_UpdateDashboardTemplate
GO
CREATE PROCEDURE [dbo].[USP_UserMaster_UpdateDashboardTemplate]
	@numDomainID NUMERIC(18,0)
	,@numUserCntID AS NUMERIC(18,0)
	,@numTemplateID AS NUMERIC(18,0)
AS
BEGIN 

	BEGIN
		UPDATE 
			UserMaster
		SET
			numDashboardTemplateID = @numTemplateID
		WHERE
			numDomainID = @numDomainID	AND  numUserDetailId = @numUserCntID
	END

	
END

GO


GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_Item_GetAssemblyMfgTimeCost')
DROP PROCEDURE USP_Item_GetAssemblyMfgTimeCost
GO
CREATE PROCEDURE [dbo].[USP_Item_GetAssemblyMfgTimeCost]
	@numDomainID NUMERIC(18,0)
	,@numItemCode NUMERIC(18,0)
	,@numBuildProcessID NUMERIC(18,0)
	,@dtProjectedStartDate DATETIME
	,@ClientTimeZoneOffset INT
AS
BEGIN 
	DECLARE @dtProjectedFinishDate AS DATETIME = @dtProjectedStartDate
	DECLARE @monLaborCost DECIMAL(20,5) = 0
	DECLARE @numOverheadServiceItemID NUMERIC(18,0)

	SELECT 
		@numOverheadServiceItemID=ISNULL(numOverheadServiceItemID,0) 
	FROM 
		Domain 
	WHERE 
		numDomainID=@numDomainID
	
	DECLARE @TempTasks TABLE
	(
		numTaskID NUMERIC(18,0)
		,numTaskTimeInMinutes NUMERIC(18,0)
		,numUserCntID NUMERIC(18,0)
		,intTaskType INT --1:Parallel, 2:Sequential
	)

	DECLARE @TaskAssignee TABLE
	(
		ID INT IDENTITY(1,1)
		,numWorkScheduleID NUMERIC(18,0)
		,numUserCntID NUMERIC(18,0)
		,numTaskTimeInMinutes NUMERIC(18,0)
		,numProductiveTimeInMinutes NUMERIC(18,0)
		,monHourlyRate DECIMAL(20,5)
		,tmStartOfDay TIME(7)
		,vcWorkDays VARCHAR(20)
		,dtFinishDate DATETIME
	)

	INSERT INTO @TempTasks
	(
		numTaskID
		,numTaskTimeInMinutes
		,intTaskType
		,numUserCntID
	)
	SELECT 
		SPDT.numTaskID
		,(ISNULL(SPDT.numHours,0) * 60) + ISNULL(SPDT.numMinutes,0)
		,ISNULL(SPDT.intTaskType,1)
		,SPDT.numAssignTo
	FROM 
		StagePercentageDetails SPD
	INNER JOIN
		StagePercentageDetailsTask SPDT
	ON
		SPD.numStageDetailsId=SPDT.numStageDetailsId
	
	WHERE 
		SPD.numDomainId=@numDomainID
		AND SPD.slp_id=@numBuildProcessID
		AND ISNULL(SPDT.numWorkOrderId,0) = 0

	INSERT INTO @TaskAssignee
	(
		numWorkScheduleID
		,numUserCntID
		,monHourlyRate
		,numProductiveTimeInMinutes
		,tmStartOfDay
		,vcWorkDays
	)
	SELECT
		WS.ID
		,WS.numUserCntID
		,ISNULL(UserMaster.monHourlyRate,0)
		,(ISNULL(WS.numProductiveHours,0) * 60) + ISNULL(WS.numProductiveMinutes,0)
		,tmStartOfDay
		,ISNULL(WS.vcWorkDays,'')
	FROM
		WorkSchedule WS
	INNER JOIN
		UserMaster
	ON
		WS.numUserCntID = UserMaster.numUserDetailId
	WHERE 
		WS.numUserCntID IN (SELECT numUserCntID FROM @TempTasks)

	UPDATE 
		TA
	SET
		TA.numTaskTimeInMinutes = TEMP.numTaskTimeInMinutes
	FROM
		@TaskAssignee TA
	CROSS APPLY
	(
		SELECT 
			MAX(numTaskTimeInMinutes) numTaskTimeInMinutes
		FROM
			@TempTasks TT
		WHERE
			TT.numUserCntID = TA.numUserCntID
			AND intTaskType = 1
	) TEMP
		
	UPDATE 
		TA
	SET
		TA.numTaskTimeInMinutes = ISNULL(TA.numTaskTimeInMinutes,0) + ISNULL(TEMP.numTaskTimeInMinutes,0)
	FROM
		@TaskAssignee TA
	CROSS APPLY
	(
		SELECT 
			SUm(numTaskTimeInMinutes) numTaskTimeInMinutes
		FROM
			@TempTasks TT
		WHERE
			TT.numUserCntID = TA.numUserCntID
			AND intTaskType = 2
	) TEMP


	DECLARE @i AS INT = 1
	DECLARE @iCount AS INT
	DECLARE @numWorkScheduleID NUMERIC(18,0)
	DECLARE @numTempUserCntID NUMERIC(18,0)
	DECLARE @dtStartDate DATETIME
	DECLARE @numTotalTaskInMinutes NUMERIC(18,0)
	DECLARE @numProductiveTimeInMinutes NUMERIC(18,0)
	DECLARE @tmStartOfDay TIME(7)
	DECLARE @numTimeLeftForDay NUMERIC(18,0)
	DECLARE @vcWorkDays VARCHAR(20)

	SELECT @iCount=COUNT(*) FROM @TaskAssignee

	WHILE @i <= @iCount
	BEGIN
		SELECT 
			@numWorkScheduleID = numWorkScheduleID
			,@numTotalTaskInMinutes = numTaskTimeInMinutes
			,@numProductiveTimeInMinutes=numProductiveTimeInMinutes
			,@tmStartOfDay = tmStartOfDay
			,@vcWorkDays=vcWorkDays
			,@dtStartDate = DATEADD(MINUTE,@ClientTimeZoneOffset,CONVERT(DATETIME,CONVERT(VARCHAR(8),@dtProjectedStartDate,112)) + tmStartOfDay)
		FROM 
			@TaskAssignee 
		WHERE 
			ID=@i

		IF @numProductiveTimeInMinutes > 0 AND @numTotalTaskInMinutes > 0
		BEGIN
			WHILE @numTotalTaskInMinutes > 0
			BEGIN
				-- IF ITS WORKING DAY AND ALSO NOT HOLIDAY THEN PROCEED OR INCRESE DATE TO NEXT DAY
				IF DATEPART(WEEKDAY,@dtStartDate) IN (SELECT Id FROM dbo.SplitIDs(@vcWorkDays,',')) AND NOT EXISTS (SELECT ID FROM WorkScheduleDaysOff WHERE numWorkScheduleID=@numWorkScheduleID AND CAST(DATEADD(MINUTE,@ClientTimeZoneOffset * -1,@dtStartDate) AS DATE) BETWEEN dtDayOffFrom AND dtDayOffTo)
				BEGIN
					-- IF START DATE IS LESS THEN TODAY DATE
					IF CAST(@dtStartDate AS DATE) >= CAST(GETUTCDATE() AS DATE)
					BEGIN	
						IF CAST(@dtStartDate AS DATE) = CAST(GETUTCDATE() AS DATE)
						BEGIN
							-- CHECK TIME LEFT FOR DAY BASED
							SET @numTimeLeftForDay = DATEDIFF(MINUTE,GETUTCDATE(),DATEADD(MINUTE,@numProductiveTimeInMinutes,DATEADD(MINUTE,@ClientTimeZoneOffset,CONVERT(DATETIME,CONVERT(VARCHAR(8),GETUTCDATE(),112)) + @tmStartOfDay)))

							IF @numTimeLeftForDay > 0
							BEGIN
								SET @numTimeLeftForDay = @numProductiveTimeInMinutes;
							END
						END
						ELSE
						BEGIN
							SET @numTimeLeftForDay = @numProductiveTimeInMinutes
						END

						IF @numTimeLeftForDay > 0
						BEGIN
							IF @numTimeLeftForDay > @numTotalTaskInMinutes
							BEGIN
								SET @dtStartDate = DATEADD(MINUTE,@numTotalTaskInMinutes,@dtStartDate)
							END
							ELSE
							BEGIN
								SET @dtStartDate = DATEADD(DAY,1,@dtStartDate)
							END

							SET @numTotalTaskInMinutes = @numTotalTaskInMinutes - @numTimeLeftForDay
						END
						ELSE
						BEGIN
							SET @dtStartDate = DATEADD(DAY,1,@dtStartDate)
						END
					END
					ELSE
					BEGIN
						SET @dtStartDate = DATEADD(DAY,1,@dtStartDate)
					END
				END
				ELSE
				BEGIN
					SET @dtStartDate = DATEADD(DAY,1,@dtStartDate)					
				END				
			END
		END

		UPDATE @TaskAssignee SET dtFinishDate=@dtStartDate WHERE ID = @i

		SET @i = @i + 1
	END

	
	IF (SELECT COUNT(*) FROM @TaskAssignee) > 0
	BEGIN
		SELECT @dtProjectedFinishDate=MAX(dtFinishDate) FROM @TaskAssignee
	END

	SET @monLaborCost = ISNULL((SELECT (numTaskTimeInMinutes * (monHourlyRate/60)) FROM @TaskAssignee),0)
	
	SELECT 
		DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,@dtProjectedFinishDate) AS dtProjectedFinishDate
		,ISNULL((SELECT SUM(ISNULL(ID.numQtyItemsReq,0) * ISNULL(IChild.monAverageCost,0)) FROM ItemDetails ID INNER JOIN Item IChild ON ID.numChildItemID=IChild.numItemCode WHERE ID.numItemKitID=@numItemCode AND ID.numChildItemID <> @numOverheadServiceItemID),0) monMaterialCost 
		,ISNULL((SELECT SUM(ISNULL(ID.numQtyItemsReq,0) * ISNULL(IChild.monListPrice,0)) FROM ItemDetails ID INNER JOIN Item IChild ON ID.numChildItemID=IChild.numItemCode WHERE ID.numItemKitID=@numItemCode AND ID.numChildItemID = @numOverheadServiceItemID),0) monMFGOverhead
		,@monLaborCost AS monLaborCost
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_WorkOrder_GetDetailPageFieldsData')
DROP PROCEDURE USP_WorkOrder_GetDetailPageFieldsData
GO
CREATE PROCEDURE [dbo].[USP_WorkOrder_GetDetailPageFieldsData]
	@numDomainID NUMERIC(18,0)
	,@numWOID NUMERIC(18,0)
	,@ClientTimeZoneOffset INT
AS
BEGIN 
	SELECT
		WorkOrder.numWOId
		,ISNULL(OpportunityMaster.numOppId,0) AS numOppId
		,ISNULL(OpportunityMaster.vcPOppName,'') AS vcPoppName
		,ISNULL(WorkOrder.numAssignedTo,0) numAssignedTo
		,dbo.fn_GetContactName(WorkOrder.numAssignedTo) numAssignedToName
		,ISNULL(Warehouses.vcWareHouse,'') vcWarehouse
		,ISNULL(WorkOrder.vcInstruction,'') vcInstruction
		,DATEADD(MINUTE,@ClientTimeZoneOffset * -1,WorkOrder.dtmStartDate) dtmStartDate
		,DATEADD(MINUTE,@ClientTimeZoneOffset * -1,WorkOrder.dtmEndDate) dtmEndDate
		,DATEADD(MINUTE,@ClientTimeZoneOffset * -1,(SELECT MIN(SPDTTL.dtActionTime) FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID IN (SELECT SPDT.numTaskID FROM StagePercentageDetailsTask SPDT WHERE SPDT.numWorkOrderId=WorkOrder.numWOId) AND tintAction=1)) dtActualStartDate
		,dbo.GetProjectedFinish(@numDomainID,@numWOID,@ClientTimeZoneOffset) AS vcProjectedFinish
	FROM
		WorkOrder
	INNER JOIN
		WareHouseItems
	ON
		WorkOrder.numWareHouseItemId = WareHouseItems.numWareHouseItemID
	INNER JOIN
		Warehouses
	ON
		WareHouseItems.numWareHouseID = Warehouses.numWareHouseID
	LEFT JOIN
		OpportunityMaster
	ON
		WorkOrder.numOppId = OpportunityMaster.numOppId
	WHERE
		WorkOrder.numDomainID = @numDomainID
		AND WorkOrder.numWOId = @numWOID
END
GO
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_WorkOrder_GetMilestones')
DROP PROCEDURE dbo.USP_WorkOrder_GetMilestones
GO
CREATE PROCEDURE [dbo].[USP_WorkOrder_GetMilestones]
(
	@numDomainID NUMERIC(18,0)
	,@numWOID NUMERIC(18,0)
)
AS 
BEGIN
	SELECT 
		vcMileStoneName
		,CONCAT(StagePercentageDetails.vcMileStoneName,' (',ISNULL(StagePercentageMaster.numStagePercentage,0),'% of total)') vcMileStone
		,dbo.GetTotalProgress(@numDomainID,@numWOID,1,2,vcMileStoneName,0) numTotalProgress
	FROM 
		WorkOrder
	INNER JOIN
		StagePercentageDetails
	ON
		WorkOrder.numBuildProcessId = StagePercentageDetails.slp_id
	LEFT JOIN
		StagePercentageMaster
	ON
		StagePercentageDetails.numParentStageID = StagePercentageMaster.numStagePercentageId
	WHERE
		WorkOrder.numDomainID = @numDomainID
		AND WorkOrder.numWOId = @numWOID
	GROUP BY
		vcMileStoneName
		,StagePercentageMaster.numStagePercentage
END
GO
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_WorkOrder_GetStages')
DROP PROCEDURE dbo.USP_WorkOrder_GetStages
GO
CREATE PROCEDURE [dbo].[USP_WorkOrder_GetStages]
(
	@numDomainID NUMERIC(18,0)
	,@numWOID NUMERIC(18,0)
	,@vcMilestoneName VARCHAR(500)
)
AS 
BEGIN
	SELECT 
		StagePercentageDetails.numStageDetailsId
		,StagePercentageDetails.vcStageName AS vcStageName
		,dbo.GetTotalProgress(@numDomainID,@numWOID,1,3,'',StagePercentageDetails.numStageDetailsId) numTotalProgress
	FROM
		StagePercentageDetails
	WHERE
		StagePercentageDetails.numDomainID = @numDomainID
		AND StagePercentageDetails.numWorkOrderId = @numWOID
		AND vcMileStoneName = @vcMilestoneName
END
GO
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_WorkOrder_GetTasks')
DROP PROCEDURE dbo.USP_WorkOrder_GetTasks
GO
CREATE PROCEDURE [dbo].[USP_WorkOrder_GetTasks]
(
	@numDomainID NUMERIC(18,0)
	,@numWOID NUMERIC(18,0)
	,@numStageDetailsId NUMERIC(18,0)
	,@ClientTimeZoneOffset INT
	,@tintMode TINYINT
)
AS 
BEGIN
	IF @tintMode = 1
	BEGIN
		DECLARE @numQtyToBuild FLOAT
		DECLARE @dtPlannedStartDate DATETIME
		SELECT 
			@numQtyToBuild=numQtyItemsReq
			,@dtPlannedStartDate=ISNULL(WorkOrder.dtmStartDate,WorkOrder.bintCreatedDate) 
		FROM 
			WorkOrder 
		WHERE
			WorkOrder.numDomainID=@numDomainID 
			AND WorkOrder.numWOId=@numWOID

		DECLARE @TempTaskAssignee TABLE
		(
			numAssignedTo NUMERIC(18,0)
			,dtLastTaskCompletionTime DATETIME
		)

		DECLARE @TempTasks TABLE
		(
			ID INT IDENTITY(1,1)
			,numTaskID NUMERIC(18,0)
			,numTaskTimeInMinutes NUMERIC(18,0)
			,numTaskAssignee NUMERIC(18,0)
			,intTaskType INT --1:Parallel, 2:Sequential
			,dtPlannedStartDate DATETIME
		)

		INSERT INTO @TempTasks
		(
			numTaskID
			,numTaskTimeInMinutes
			,intTaskType
			,numTaskAssignee
		)
		SELECT 
			SPDT.numTaskID
			,((ISNULL(SPDT.numHours,0) * 60) + ISNULL(SPDT.numMinutes,0)) * @numQtyToBuild
			,ISNULL(SPDT.intTaskType,1)
			,SPDT.numAssignTo
		FROM 
			StagePercentageDetailsTask SPDT
		INNER JOIN
			WorkOrder WO
		ON
			SPDT.numWorkOrderId = WO.numWOId
		WHERE 
			SPDT.numDomainId=@numDomainID
			AND SPDT.numWorkOrderId = @numWOID

		UPDATE @TempTasks SET dtPlannedStartDate=@dtPlannedStartDate

		INSERT INTO @TempTaskAssignee
		(
			numAssignedTo
		)
		SELECT DISTINCT
			numTaskAssignee
		FROM
			@TempTasks

		DECLARE @i INT = 1
		DECLARE @iCount INT 	
		SELECT @iCount = COUNT(*) FROM @TempTasks

		DECLARE @numTaskAssignee NUMERIC(18,0)
		DECLARE @intTaskType INT
		DECLARE @numWorkScheduleID NUMERIC(18,0)
		DECLARE @numTempUserCntID NUMERIC(18,0)
		DECLARE @dtStartDate DATETIME
		DECLARE @numTotalTaskInMinutes NUMERIC(18,0)
		DECLARE @numProductiveTimeInMinutes NUMERIC(18,0)
		DECLARE @tmStartOfDay TIME(7)
		DECLARE @numTimeLeftForDay NUMERIC(18,0)
		DECLARE @vcWorkDays VARCHAR(20)
		DECLARE @bitParallelStartSet BIT = 0

		WHILE @i <= @iCount
		BEGIN
			SELECT
				@numTaskAssignee=numTaskAssignee
				,@numTotalTaskInMinutes=numTaskTimeInMinutes
				,@intTaskType = intTaskType
			FROM
				@TempTasks 
			WHERE
				ID=@i

			-- GET TASK COMPLETION TIME WHICH WILL BE START DATETIME FOR NEXT TASK
			SELECT
				@numWorkScheduleID = WS.ID
				,@numProductiveTimeInMinutes=(ISNULL(WS.numProductiveHours,0) * 60) + ISNULL(WS.numProductiveMinutes,0)
				,@tmStartOfDay = tmStartOfDay
				,@vcWorkDays=vcWorkDays
				,@dtStartDate = DATEADD(MINUTE,@ClientTimeZoneOffset,CONVERT(DATETIME,CONVERT(VARCHAR(8),DATEADD(MINUTE,-@ClientTimeZoneOffset,@dtPlannedStartDate),112)) + tmStartOfDay)
			FROM
				WorkSchedule WS
			INNER JOIN
				UserMaster
			ON
				WS.numUserCntID = UserMaster.numUserDetailId
			WHERE 
				WS.numUserCntID = @numTaskAssignee

			IF @intTaskType=1 AND EXISTS (SELECT numAssignedTo FROM @TempTaskAssignee WHERE numAssignedTo = @numTaskAssignee AND dtLastTaskCompletionTime IS NOT NULL)
			BEGIN
				SELECT @dtStartDate = dtLastTaskCompletionTime FROM @TempTaskAssignee WHERE numAssignedTo = @numTaskAssignee
			END
			ELSE IF EXISTS (SELECT numAssignedTo FROM @TempTaskAssignee WHERE numAssignedTo = @numTaskAssignee AND dtLastTaskCompletionTime IS NOT NULL)
			BEGIN
				SELECT @dtStartDate = dtLastTaskCompletionTime FROM @TempTaskAssignee WHERE numAssignedTo = @numTaskAssignee
			END

			UPDATE @TempTasks SET dtPlannedStartDate=@dtStartDate WHERE ID=@i 

			IF @numProductiveTimeInMinutes > 0 AND @numTotalTaskInMinutes > 0
			BEGIN
				WHILE @numTotalTaskInMinutes > 0
				BEGIN
					-- IF ITS WORKING DAY AND ALSO NOT HOLIDAY THEN PROCEED OR INCRESE DATE TO NEXT DAY
					IF DATEPART(WEEKDAY,@dtStartDate) IN (SELECT Id FROM dbo.SplitIDs(@vcWorkDays,',')) AND NOT EXISTS (SELECT ID FROM WorkScheduleDaysOff WHERE numWorkScheduleID=@numWorkScheduleID AND CAST(DATEADD(MINUTE,@ClientTimeZoneOffset * -1,@dtStartDate) AS DATE) BETWEEN dtDayOffFrom AND dtDayOffTo)
					BEGIN
						IF CAST(@dtStartDate AS DATE) = CAST(GETUTCDATE() AS DATE)
						BEGIN
							-- CHECK TIME LEFT FOR DAY BASED
							SET @numTimeLeftForDay = DATEDIFF(MINUTE,@dtStartDate,DATEADD(MINUTE,@numProductiveTimeInMinutes,DATEADD(MINUTE,@ClientTimeZoneOffset,CONVERT(DATETIME,CONVERT(VARCHAR(8),DATEADD(MINUTE,-@ClientTimeZoneOffset,@dtStartDate),112)) + @tmStartOfDay)))
						END
						ELSE
						BEGIN
							SET @numTimeLeftForDay = @numProductiveTimeInMinutes
						END

						IF @numTimeLeftForDay > 0
						BEGIN
							IF @numTimeLeftForDay > @numTotalTaskInMinutes
							BEGIN
								SET @dtStartDate = DATEADD(MINUTE,@numTotalTaskInMinutes,@dtStartDate)
							END
							ELSE
							BEGIN
								SET @dtStartDate = DATEADD(MINUTE,@ClientTimeZoneOffset,CONVERT(DATETIME,CONVERT(VARCHAR(8),DATEADD(MINUTE,-@ClientTimeZoneOffset,DATEADD(DAY,1,@dtStartDate)),112)) + @tmStartOfDay)
							END

							SET @numTotalTaskInMinutes = @numTotalTaskInMinutes - @numTimeLeftForDay
						END
						ELSE
						BEGIN
							SET @dtStartDate = DATEADD(MINUTE,@ClientTimeZoneOffset,CONVERT(DATETIME,CONVERT(VARCHAR(8),DATEADD(MINUTE,-@ClientTimeZoneOffset,DATEADD(DAY,1,@dtStartDate)),112)) + @tmStartOfDay)
						END
					END
					ELSE
					BEGIN
						SET @dtStartDate = DATEADD(MINUTE,@ClientTimeZoneOffset,CONVERT(DATETIME,CONVERT(VARCHAR(8),DATEADD(MINUTE,-@ClientTimeZoneOffset,DATEADD(DAY,1,@dtStartDate)),112)) + @tmStartOfDay)					
					END				
				END
			END	

			UPDATE @TempTaskAssignee SET dtLastTaskCompletionTime=@dtStartDate WHERE numAssignedTo=@numTaskAssignee

			SET @i = @i + 1
		END

		SELECT	
			SPDT.numTaskId
			,SPDT.numReferenceTaskId
			,SPDT.vcTaskName
			,dbo.fn_GetContactName(SPDT.numAssignTo) vcAssignedTo
			,'Work Station 1' AS vcWorkStation
			,ISNULL((SELECT SUM(numProcessedQty) FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId),0) AS numProcessedQty
			,ISNULL(WO.numQtyItemsReq,0) - ISNULL((SELECT SUM(numProcessedQty) FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId),0) AS numRemainingQty
			,CASE ISNULL((SELECT TOP 1 tintAction FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId ORDER BY dtActionTime DESC),0) 
				WHEN 4 THEN '<img src="../images/comflag.png" />'
				WHEN 3 THEN CONCAT('<ul class="list-inline" style="margin-bottom:0px;"><li style="vertical-align:middle;padding-left:0px;padding-right:0px;"><button class="btn btn-xs btn-info" onclick="return ShowTaskTimeLogWindow(',SPDT.numTaskId,');"><i class="fa fa-clock-o"></i></button></li><li style="vertical-align:middle;padding-right:0px;"><button class="btn btn-xs btn-flat btn-success btn-task-start" onclick="return TaskStarted(this,',SPDT.numTaskId,',0);">Start</button></li></ul>')
				WHEN 2 THEN CONCAT('<ul class="list-inline" style="margin-bottom:0px;"><li style="vertical-align:middle;padding-left:0px;padding-right:0px;"><button class="btn btn-xs btn-info" onclick="return ShowTaskTimeLogWindow(',SPDT.numTaskId,');"><i class="fa fa-clock-o"></i></button></li><li style="vertical-align:middle;padding-right:0px;"><button class="btn btn-xs btn-flat bg-purple" onclick="return TaskStarted(this,',SPDT.numTaskId,',1);">Resume</button></li></ul>')
				WHEN 1 THEN CONCAT('<ul class="list-inline" style="margin-bottom:0px;"><li style="vertical-align:middle;padding-left:0px;padding-right:0px;"><button class="btn btn-xs btn-info" onclick="return ShowTaskTimeLogWindow(',SPDT.numTaskId,');"><i class="fa fa-clock-o"></i></button></li><li style="vertical-align:middle;padding-right:0px;"><button class="btn btn-xs btn-flat btn-warning" onclick="return ShowTaskPausedWindow(this,',SPDT.numTaskId,');">Pause</button></li><li style="vertical-align:middle;padding-right:0px;"><input type="text" onkeydown="return ProcessedUnitsChanged(this,',SPDT.numTaskId,',event);" class="form-control txtUnitsProcessed" style="width:50px;padding:1px 2px 1px 2px;height:23px;" /></li></ul>')
				ELSE CONCAT('<ul class="list-inline" style="margin-bottom:0px;"><li style="vertical-align:middle;padding-left:0px;padding-right:0px;"><button class="btn btn-xs btn-info" onclick="return ShowTaskTimeLogWindow(',SPDT.numTaskId,');"><i class="fa fa-clock-o"></i></button></li><li style="vertical-align:middle;padding-right:0px;"><button class="btn btn-xs btn-flat btn-success btn-task-start" onclick="return TaskStarted(this,',SPDT.numTaskId,',0);">Start</button></li></ul>')
			END  AS vcTaskControls
			,FORMAT(FLOOR((((ISNULL(SPDT.numHours,0) * 60) + ISNULL(SPDT.numMinutes,0)) * @numQtyToBuild) / 60),'00') + ':' + FORMAT((((ISNULL(SPDT.numHours,0) * 60) + ISNULL(SPDT.numMinutes,0)) * CAST(@numQtyToBuild AS DECIMAL)) % 60,'00')  vcEstimatedTaskTime
			,(CASE WHEN dbo.GetTimeSpendOnTask(SPDT.numDomainID,SPDT.numTaskId) = 'Invalid time sequence' THEN '00:00' ELSE dbo.GetTimeSpendOnTask(SPDT.numDomainID,SPDT.numTaskId) END) vcActualTaskTime
			,DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,TT.dtPlannedStartDate) dtPlannedStartDate
			,CASE WHEN EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId AND tintAction=1)
				THEN DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,(SELECT dtActionTime FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId AND tintAction=1))
				ELSE ''
			END AS dtStartDate
			,CASE WHEN EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId AND tintAction=4)
				THEN DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,(SELECT dtActionTime FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId AND tintAction=4))
				ELSE ''
			END AS dtFinishDate
		FROM 
			StagePercentageDetailsTask SPDT
		INNER JOIN
			WorkOrder WO
		ON
			SPDT.numWorkOrderId = WO.numWOId
		LEFT JOIN
			@TempTasks TT
		ON
			SPDT.numTaskId = TT.numTaskID
		WHERE
			SPDT.numStageDetailsId = @numStageDetailsId
			AND SPDT.numWorkOrderId = @numWOID
	END
	ELSE IF @tintMode = 2
	BEGIN
		SELECT	
			SPDT.numTaskId
			,SPDT.numReferenceTaskId
			,SPDT.numStageDetailsId
			,SPDT.vcTaskName
			,ISNULL(SPDT.numHours,0) numHours
			,ISNULL(SPDT.numMinutes,0) numMinutes
			,SPDT.numAssignTo
			,ADC.numTeam
			,CONCAT(FORMAT(ISNULL(SPDT.numHours,0),'00'),':',FORMAT(ISNULL(SPDT.numMinutes,0),'00')) vcEstimatedTaskTime
			,(CASE WHEN dbo.GetTimeSpendOnTask(SPDT.numDomainID,SPDT.numTaskId) = 'Invalid time sequence' THEN '00:00' ELSE dbo.GetTimeSpendOnTask(SPDT.numDomainID,SPDT.numTaskId) END) vcActualTaskTime
			,CASE WHEN EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId AND tintAction=1)
				THEN DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,(SELECT dtActionTime FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId AND tintAction=1))
				ELSE ''
			END AS dtStartDate
			,CASE WHEN EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId AND tintAction=4)
				THEN DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,(SELECT dtActionTime FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId AND tintAction=4))
				ELSE ''
			END AS dtFinishDate
			,CASE WHEN EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId)
				THEN 1
				ELSE 0
			END bitTaskStarted
		FROM 
			StagePercentageDetailsTask SPDT
		INNER JOIN
			WorkOrder WO
		ON
			SPDT.numWorkOrderId = WO.numWOId
		INNER JOIN
			AdditionalContactsInformation ADC
		ON
			SPDT.numAssignTo = ADC.numContactId
		LEFT JOIN
			@TempTasks TT
		ON
			SPDT.numTaskId = TT.numTaskID
		WHERE
			SPDT.numStageDetailsId = @numStageDetailsId
			AND SPDT.numWorkOrderId = @numWOID
	END
END
GO
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_StagePercentageDetailsTaskTimeLog_Save')
DROP PROCEDURE dbo.USP_StagePercentageDetailsTaskTimeLog_Save
GO
CREATE PROCEDURE [dbo].[USP_StagePercentageDetailsTaskTimeLog_Save]
(
	@ID NUMERIC(18,0)
	,@numDomainID NUMERIC(18,0)
	,@numUserCntID NUMERIC(18,0)
	,@numTaskID NUMERIC(18,0)
	,@tintAction TINYINT
	,@dtActionTime DATETIME
	,@numProcessedQty FLOAT
	,@numReasonForPause NUMERIC(18,0)
	,@vcNotes VARCHAR(1000)
	,@bitManualEntry BIT
)
AS 
BEGIN
	IF NOT EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numDomainID=@numDomainID AND numTaskID=@numTaskID AND tintAction=4)
	BEGIN
		IF EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numDomainID=@numDomainID AND numTaskID=@numTaskID AND tintAction=1) AND @tintAction=1 AND (ISNULL(@ID,0) = 0 OR (@ID > 0 AND (SELECT COUNT(*) FROM StagePercentageDetailsTaskTimeLog WHERE numDomainID=@numDomainID AND numTaskID=@numTaskID) > 1))
		BEGIN
			RAISERROR('TASK_IS_ALREADY_STARTED',16,1)
		END
		ELSE
		BEGIN
			IF EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE ID=@ID)
			BEGIN
				UPDATE
					StagePercentageDetailsTaskTimeLog
				SET
					tintAction = @tintAction
					,dtActionTime =  DATEADD(MINUTE, DATEDIFF(MINUTE,0,@dtActionTime),0)
					,numProcessedQty=@numProcessedQty
					,vcNotes = @vcNotes
					,bitManualEntry=@bitManualEntry
					,numModifiedBy = @numUserCntID
					,numReasonForPause = @numReasonForPause
					,dtModifiedDate = GETUTCDATE()
				WHERE
					ID = @ID			
			END
			ELSE
			BEGIN
				IF EXISTS (SELECT numTaskId FROM StagePercentageDetailsTask WHERE numDomainID=@numDomainID AND numTaskId=@numTaskID)
				BEGIN
					DECLARE @numTaskAssignee AS NUMERIC(18,0)
					SELECT @numTaskAssignee=numAssignTo FROM StagePercentageDetailsTask WHERE numDomainID=@numDomainID AND numTaskId=@numTaskID

					INSERT INTO StagePercentageDetailsTaskTimeLog
					(
						numDomainID
						,numUserCntID
						,numTaskID
						,tintAction
						,dtActionTime
						,numProcessedQty
						,numReasonForPause
						,vcNotes
						,bitManualEntry
					)
					VALUES
					(
						@numDomainID
						,@numTaskAssignee
						,@numTaskID
						,@tintAction
						,DATEADD(MINUTE, DATEDIFF(MINUTE,0,@dtActionTime),0)
						,@numProcessedQty
						,@numReasonForPause
						,@vcNotes
						,@bitManualEntry
					)
				END
				ELSE
				BEGIN
					RAISERROR('TASK_DOES_NOT_EXISTS',16,1)
				END
			END
		END
	END
	ELSE
	BEGIN
		RAISERROR('TASK_IS_MARKED_AS_FINISHED',16,1)
	END
END
GO
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_StagePercentageDetailsTaskTimeLog_GetByTaskID')
DROP PROCEDURE dbo.USP_StagePercentageDetailsTaskTimeLog_GetByTaskID
GO
CREATE PROCEDURE [dbo].[USP_StagePercentageDetailsTaskTimeLog_GetByTaskID]
(
	@numDomainID NUMERIC(18,0)
	,@numTaskID NUMERIC(18,0)
	,@ClientTimeZoneOffset INT
	,@vcTotalTimeSpendOnTask VARCHAR(50) OUTPUT
)
AS 
BEGIN
	SET @vcTotalTimeSpendOnTask = dbo.GetTimeSpendOnTask(@numDomainID,@numTaskID)

	SELECT
		SPDTTL.ID
		,SPDTTL.numTaskID
		,SPDTTL.tintAction
		,ISNULL(SPDTTL.numProcessedQty,0) numProcessedQty
		,ISNULL(SPDTTL.numReasonForPause,0) numReasonForPause
		,dbo.fn_GetContactName(SPDTTL.numUserCntID) vcEmployee
		,CASE SPDTTL.tintAction
			WHEN 1 THEN 'Started'
			WHEN 2 THEN 'Paused'
			WHEN 3 THEN 'Resumed'
			WHEN 4 THEN 'Finished'
			ELSE ''
		END vcAction
		,CONCAT(dbo.FormatedDateFromDate(DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,SPDTTL.dtActionTime),@numDomainID),' ',CONVERT(VARCHAR(5),DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,SPDTTL.dtActionTime), 108),' ',RIGHT(CONVERT(VARCHAR(30),DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,SPDTTL.dtActionTime),9),2)) vcActionTime
		,DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,SPDTTL.dtActionTime) dtActionTime
		,CASE WHEN tintAction = 2 THEN CAST(numProcessedQty AS VARCHAR) ELSE '' END vcProcessedQty
		,ISNULL(LD.vcData,'') vcReasonForPause
		,ISNULL(SPDTTL.vcNotes,'') vcNotes
		,(CASE WHEN EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog S WHERE S.numTaskID = @numTaskID AND tintAction=4) THEN 1 ELSE 0 END) bitTaskFinished
	FROM
		StagePercentageDetailsTaskTimeLog SPDTTL
	LEFT JOIN
		ListDetails LD
	ON
		LD.numListID = 52
		AND SPDTTL.numReasonForPause = LD.numListItemID
	WHERE
		SPDTTL.numDomainID=@numDomainID
		AND SPDTTL.numTaskID=@numTaskID
	ORDER BY
		dtActionTime
END
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_StagePercentageDetailsTaskTimeLog_Delete')
DROP PROCEDURE dbo.USP_StagePercentageDetailsTaskTimeLog_Delete
GO
CREATE PROCEDURE [dbo].[USP_StagePercentageDetailsTaskTimeLog_Delete]
(
	@numDomainID NUMERIC(18,0)
	,@ID NUMERIC(18,0)
	,@numTaskID NUMERIC(18,0)
)
AS 
BEGIN
	IF NOT EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numDomainID=@numDomainID AND numTaskID=@numTaskID AND tintAction=4)
	BEGIN
		IF @ID = -1
		BEGIN
			DELETE FROM StagePercentageDetailsTaskTimeLog WHERE numDomainID=@numDomainID AND numTaskID=@numTaskID
		END
		ELSE
		BEGIN
			DELETE FROM StagePercentageDetailsTaskTimeLog WHERE numDomainID=@numDomainID AND numTaskID=@numTaskID AND ID=@ID
		END
	END
	ELSE
	BEGIN
		RAISERROR('TASK_IS_MARKED_AS_FINISHED',16,1)
	END
END
GO
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetEmployeesWithCapacityLoad')
DROP PROCEDURE dbo.USP_GetEmployeesWithCapacityLoad
GO
CREATE PROCEDURE [dbo].[USP_GetEmployeesWithCapacityLoad]
(
	@numDomainID NUMERIC(18,0)
	,@numWorkOrderID NUMERIC(18,0)
	,@tintDateRange TINYINT -- 1:Today, 2:Week, 3:Month
	,@dtFromDate DATETIME
	,@ClientTimeZoneOffset INT
)
AS 
BEGIN
	 SELECT 
		A.numContactID numEmployeeID
		,CONCAT(A.vcFirstName,' ',A.vcLastName) as vcEmployeeName
		,[dbo].[GetCapacityLoad](@numDomainID,A.numContactID,0,@numWorkOrderID,0,@tintDateRange,@dtFromDate,@ClientTimeZoneOffset) numCapacityLoad
	FROM 
		UserMaster UM         
	INNER JOIN 
		AdditionalContactsInformation A        
	ON 
		UM.numUserDetailId=A.numContactID          
	WHERE 
		UM.numDomainID=@numDomainID 
		AND UM.numDomainID=A.numDomainID 
		AND UM.intAssociate=1  
END
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetTeamsWithCapacityLoad')
DROP PROCEDURE dbo.USP_GetTeamsWithCapacityLoad
GO
CREATE PROCEDURE [dbo].[USP_GetTeamsWithCapacityLoad]
(
	@numDomainID NUMERIC(18,0)
	,@numWorkOrderID NUMERIC(18,0)
	,@tintDateRange TINYINT -- 1:Today, 2:Week, 3:Month
	,@dtFromDate DATETIME
	,@ClientTimeZoneOffset INT
)
AS 
BEGIN
	 SELECT 
		LD.numListItemID numTeamID
		,ISNULL(vcData,'') as vcTeamName
		,[dbo].[GetCapacityLoad](@numDomainID,0,LD.numListItemID,@numWorkOrderID,0,@tintDateRange,@dtFromDate,@ClientTimeZoneOffset) numCapacityLoad
	FROM 
		ListDetails LD                 
	WHERE 
		LD.numDomainID=@numDomainID 
		AND LD.numListID=35
END
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_StagePercentageDetailsTask_ChangeAssignee')
DROP PROCEDURE dbo.USP_StagePercentageDetailsTask_ChangeAssignee
GO
CREATE PROCEDURE [dbo].[USP_StagePercentageDetailsTask_ChangeAssignee]
(
	@numDomainID NUMERIC(18,0)
	,@numTaskID NUMERIC(18,0)
	,@numAssignedTo NUMERIC(18,0)
)
AS 
BEGIN
	IF EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numDomainID=@numDomainID AND numTaskID = @numTaskID AND tintAction=4)
	BEGIN
		RAISERROR('TASK_IS_MARKED_AS_FINISHED',16,1)
	END
	ELSE IF ISNULL((SELECT TOP 1 tintAction FROM StagePercentageDetailsTaskTimeLog WHERE numDomainID=@numDomainID AND numTaskID = @numTaskID ORDER BY tintAction),0) IN (1,3)
	BEGIN
		RAISERROR('TASK_IS_ALREADY_STARTED',16,1)
	END
	ELSE
	BEGIN
		UPDATE StagePercentageDetailsTask SET numAssignTo=@numAssignedTo WHERE numDomainID=@numDomainID AND numTaskId=@numTaskID
	END	
END
GO
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_StagePercentageDetailsTask_ChangeTime')
DROP PROCEDURE dbo.USP_StagePercentageDetailsTask_ChangeTime
GO
CREATE PROCEDURE [dbo].[USP_StagePercentageDetailsTask_ChangeTime]
(
	@numDomainID NUMERIC(18,0)
	,@numTaskID NUMERIC(18,0)
	,@numHours NUMERIC(18,0)
	,@numMinutes NUMERIC(18,0)
	,@bitMasterUpdateAlso BIT
)
AS 
BEGIN
	IF EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numDomainID=@numDomainID AND numTaskID = @numTaskID AND tintAction=4)
	BEGIN
		RAISERROR('TASK_IS_MARKED_AS_FINISHED',16,1)
		RETURN
	END
	ELSE IF ISNULL((SELECT TOP 1 tintAction FROM StagePercentageDetailsTaskTimeLog WHERE numDomainID=@numDomainID AND numTaskID = @numTaskID ORDER BY tintAction),0) IN (1,3)
	BEGIN
		RAISERROR('TASK_IS_ALREADY_STARTED',16,1)
		RETURN
	END
	ELSE
	BEGIN
		DECLARE @numToTalTimeSpendOnTask NUMERIC(18,0)
		DECLARE @vcTimeSpend VARCHAR(20) = ''
		SET @vcTimeSpend = dbo.GetTimeSpendOnTask(@numDomainID,@numTaskID)

		IF @vcTimeSpend = 'Invalid time sequence'
		BEGIN
			RAISERROR('INVALID_TIME_ENTRY_SEQUENCE',16,1)
			RETURN
		END

		SELECT @numToTalTimeSpendOnTask = (CAST(SUBSTRING(@vcTimeSpend,0,CHARINDEX(':',@vcTimeSpend)) AS INT) * 60) + CAST(SUBSTRING(@vcTimeSpend,CHARINDEX(':',@vcTimeSpend) + 1,LEN(@vcTimeSpend)) AS INT)

		If @numToTalTimeSpendOnTask > ((ISNULL(@numHours,0) * 60) + ISNULL(@numMinutes,0))
		BEGIN
			RAISERROR('MORE_TIME_SPEND_ON_TASK',16,1)
			RETURN
		END

		UPDATE StagePercentageDetailsTask SET numHours=@numHours,numMinutes=@numMinutes WHERE numDomainID=@numDomainID AND numTaskId=@numTaskID

		IF @bitMasterUpdateAlso = 1
		BEGIN
			DECLARE @numReferenceTaskId NUMERIC(18,0)
			SELECT @numReferenceTaskId=ISNULL(numReferenceTaskId,0) FROM StagePercentageDetailsTask WHERE numDomainID=@numDomainID AND numTaskId=@numTaskID

			UPDATE StagePercentageDetailsTask SET numHours=@numHours,numMinutes=@numMinutes WHERE numDomainID=@numDomainID AND numTaskId=@numReferenceTaskId 
		END
	END	
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_WorkOrder_GetBOMDetail')
DROP PROCEDURE USP_WorkOrder_GetBOMDetail
GO
CREATE PROCEDURE [dbo].[USP_WorkOrder_GetBOMDetail]                             
	@numDomainID AS NUMERIC(18,0)                            
	,@numWOID NUMERIC(18,0)
AS                            
BEGIN
	DECLARE @tintCommitAllocation AS TINYINT
	SELECT @tintCommitAllocation=ISNULL(tintCommitAllocation,1) FROM Domain WHERE numDomainId=@numDomainID

	DECLARE @TEMP TABLE
	(
		numParentID NUMERIC(18,0)
		,ID NUMERIC(18,0)
		,numOppId NUMERIC(18,0)
		,numItemCode NUMERIC(18,0)
		,vcItemName VARCHAR(500)
		,numRequiredQty FLOAT
		,numWarehouseItemID NUMERIC(18,0)
		,vcWarehouse VARCHAR(200)
		,numAvailable FLOAT
		,numOnOrder FLOAT
		,numAllocation FLOAT
		,numBackOrder FLOAT
		,monCost DECIMAL(20,5)
		,bitWorkOrder BIT
		,bitReadyToBuild BIT
	)

	;WITH CTEWorkOrder (numParentWOID,numWOId,numOppId,numWODetailId,numItemCode,vcItemName,numQtyItemsReq,numWarehouseItemID,monCost,bitReadyToBuild) AS
	(
	
		SELECT
			CAST(0 AS NUMERIC),
			WOD.numWOId,
			WorkOrder.numOppId,
			WOD.numWODetailId,
			Item.numItemCode,
			CONCAT(Item.vcItemName,(CASE WHEN ISNULL(Item.vcSKU,'') <> '' THEN CONCAT(' (',Item.vcSKU,')') ELSE '' END)),
			WOD.numQtyItemsReq,
			WOD.numWarehouseItemID,
			ISNULL(WOD.monAverageCost,ISNULL(Item.monAverageCost,0)) * ISNULL(WOD.numQtyItemsReq,0),
			CAST((CASE WHEN WorkOrder.numWOStatus = 23184 THEN 1 ELSE 0 END) AS BIT)
		FROM 
			WorkOrder 
		INNER JOIN
			WorkOrderDetails WOD
		ON
			WorkOrder.numWOId = WOD.numWOId
		INNER JOIN 
			Item
		ON
			WOD.numChildItemID = Item.numItemCode
		WHERE
			WorkOrder.numDomainID = @numDomainID
			AND WorkOrder.numWOId = @numWOID
		UNION ALL
		SELECT
			CTEWorkOrder.numWODetailId
			,WorkOrder.numWOId
			,WorkOrder.numOppId
			,WorkOrderDetails.numWODetailId
			,Item.numItemCode,
			CONCAT(Item.vcItemName,(CASE WHEN ISNULL(Item.vcSKU,'') <> '' THEN CONCAT(' (',Item.vcSKU,')') ELSE '' END)),
			WorkOrderDetails.numQtyItemsReq,
			WorkOrderDetails.numWarehouseItemID,
			ISNULL(WorkOrderDetails.monAverageCost,ISNULL(Item.monAverageCost,0)) * ISNULL(WorkOrderDetails.numQtyItemsReq,0),
			CAST((CASE WHEN WorkOrder.numWOStatus = 23184 THEN 1 ELSE 0 END) AS BIT)
		FROM
			WorkOrder
		INNER JOIN
			CTEWorkOrder
		ON
			WorkOrder.numParentWOID = CTEWorkOrder.numWOID
			AND WorkOrder.numItemCode = CTEWorkOrder.numItemCode
		INNER JOIN
			WorkOrderDetails
		ON
			WorkOrder.numWOId = WorkOrderDetails.numWOId
		INNER JOIN 
			Item
		ON
			WorkOrderDetails.numChildItemID = Item.numItemCode
	)

	INSERT INTO @TEMP
	(
		numParentID
		,ID
		,numItemCode
		,vcItemName
		,numRequiredQty
		,numWarehouseItemID
		,vcWarehouse
		,numAvailable
		,numOnOrder
		,numAllocation
		,numBackOrder
		,monCost
		,bitWorkOrder
		,bitReadyToBuild
	)
	SELECT
		CTEWorkOrder.numParentWOID
		,CTEWorkOrder.numWODetailId
		,numItemCode
		,vcItemName
		,numQtyItemsReq
		,CTEWorkOrder.numWarehouseItemID
		,Warehouses.vcWareHouse
		,WareHouseItems.numOnHand
		,WareHouseItems.numOnOrder
		,WareHouseItems.numAllocation
		,WareHouseItems.numBackOrder
		,ISNULL(CTEWorkOrder.monCost,0)
		,(CASE WHEN (SELECT COUNT(*) FROM WorkOrder WOInner WHERE WOInner.numParentWOID=CTEWorkOrder.numWOId AND WOInner.numItemCode = CTEWorkOrder.numItemCode) > 0 THEN 1 ELSE 0 END)
		,CASE 
			WHEN ISNULL(WareHouseItems.numWareHouseItemID,0) > 0 AND ISNULL(CTEWorkOrder.bitReadyToBuild,0) = 0
			THEN
				CASE 
					WHEN (CASE WHEN ISNULL(CTEWorkOrder.numOppId,0) > 0 THEN 1 ELSE 0 END)=1 AND @tintCommitAllocation=2 
					THEN (CASE WHEN ISNULL(numQtyItemsReq,0) > WareHouseItems.numOnHand THEN 0 ELSE 1 END)
					ELSE (CASE WHEN ISNULL(numQtyItemsReq,0) > WareHouseItems.numAllocation THEN 0 ELSE 1 END) 
				END
			ELSE 1
		END
	FROM
		CTEWorkOrder
	LEFT JOIN
		WareHouseItems
	ON
		CTEWorkOrder.numWarehouseItemID = WareHouseItems.numWareHouseItemID
	LEFT JOIN
		Warehouses
	ON
		WareHouseItems.numWareHouseID = Warehouses.numWareHouseID

	SELECT * FROM @TEMP
END
GO
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_WorkSchedule_Save')
DROP PROCEDURE dbo.USP_WorkSchedule_Save
GO
CREATE PROCEDURE [dbo].[USP_WorkSchedule_Save]
(
	@numWorkScheduleID NUMERIC(18,0)
	,@numDomainID NUMERIC(18,0)
	,@numUserCntID NUMERIC(18,0)
	,@vcWorkDays VARCHAR(20)
	,@numWorkHours TINYINT
	,@numWorkMinutes TINYINT
	,@numProductiveHours TINYINT
	,@numProductiveMinutes TINYINT
	,@tmStartOfDay TIME
)
AS 
BEGIN
	IF EXISTS (SELECT ID FROM WorkSchedule WHERE numDomainID=@numDomainID AND ID=ISNULL(@numWorkScheduleID,0))
	BEGIN
		UPDATE
			WorkSchedule
		SET
			numWorkHours=@numWorkHours
			,numWorkMinutes=@numWorkMinutes
			,numProductiveHours=@numProductiveHours
			,numProductiveMinutes=@numProductiveMinutes
			,tmStartOfDay=@tmStartOfDay
			,vcWorkDays=@vcWorkDays
		WHERE	
			ID = @numWorkScheduleID
	END
	ELSE
	BEGIN
		INSERT INTO WorkSchedule
		(
			numDomainID
			,numUserCntID
			,numWorkHours
			,numWorkMinutes
			,numProductiveHours
			,numProductiveMinutes
			,tmStartOfDay
			,vcWorkDays
		)
		VALUES
		(
			@numDomainID
			,@numUserCntID
			,@numWorkHours
			,@numWorkMinutes
			,@numProductiveHours
			,@numProductiveMinutes
			,@tmStartOfDay
			,@vcWorkDays
		)
	END
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_WorkOrder_IsTaskPending')
DROP PROCEDURE USP_WorkOrder_IsTaskPending
GO
CREATE PROCEDURE [dbo].[USP_WorkOrder_IsTaskPending]                             
	@numDomainID AS NUMERIC(18,0)                            
	,@numWOID NUMERIC(18,0)
AS                            
BEGIN
	IF EXISTS (SELECT 
					SPDT.numTaskId 
				FROM 
					StagePercentageDetailsTask SPDT 
				WHERE
					SPDT.numDomainID=@numDomainID 
					AND SPDT.numWorkOrderId=@numWOID 
					AND ISNULL((SELECT COUNT(*) FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID = SPDT.numTaskId AND tintAction=4),0) = 0)
	BEGIN
		SELECT 1
	END
	ELSE
	BEGIN
		SELECT 0
	END
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_WorkOrder_IsTaskStarted')
DROP PROCEDURE USP_WorkOrder_IsTaskStarted
GO
CREATE PROCEDURE [dbo].[USP_WorkOrder_IsTaskStarted]                             
	@numDomainID AS NUMERIC(18,0)                            
	,@numWOID NUMERIC(18,0)
AS                            
BEGIN
	IF EXISTS (SELECT 
					SPDT.numTaskId 
				FROM 
					StagePercentageDetailsTask SPDT 
				WHERE
					SPDT.numDomainID=@numDomainID 
					AND SPDT.numWorkOrderId=@numWOID 
					AND ISNULL((SELECT COUNT(*) FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID = SPDT.numTaskId),0) > 0)
	BEGIN
		SELECT 1
	END
	ELSE
	BEGIN
		SELECT 0
	END
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_WorkOrder_IsCompleted')
DROP PROCEDURE USP_WorkOrder_IsCompleted
GO
CREATE PROCEDURE [dbo].[USP_WorkOrder_IsCompleted]                             
	@numDomainID AS NUMERIC(18,0)                            
	,@numWOID NUMERIC(18,0)
AS                            
BEGIN
	IF EXISTS (SELECT numWOId FROM WorkOrder WHERE numDomainID=@numDomainID AND numWOId=@numWOID AND numWOId=23184)
	BEGIN
		SELECT 1
	END
	ELSE
	BEGIN
		SELECT 0
	END
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_StagePercentageDetails_GetStagesByProcess')
DROP PROCEDURE USP_StagePercentageDetails_GetStagesByProcess
GO
CREATE PROCEDURE [dbo].[USP_StagePercentageDetails_GetStagesByProcess]                             
	@numDomainID AS NUMERIC(18,0)                            
	,@numProcessID NUMERIC(18,0)
	,@vcMileStoneName VARCHAR(300)
AS                            
BEGIN
	SELECT
		numStageDetailsId
		,vcStageName
	FROM
		StagePercentageDetails
	WHERE
		numDomainId=@numDomainID
		AND slp_id = @numProcessID
		AND vcMileStoneName = @vcMileStoneName
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_StagePercentageDetails_GetMilestonesByProcess')
DROP PROCEDURE USP_StagePercentageDetails_GetMilestonesByProcess
GO
CREATE PROCEDURE [dbo].[USP_StagePercentageDetails_GetMilestonesByProcess]                             
	@numDomainID AS NUMERIC(18,0)                            
	,@numProcessID NUMERIC(18,0)
AS                            
BEGIN
	SELECT
		vcMileStoneName
	FROM
		StagePercentageDetails
	WHERE
		numDomainId=@numDomainID
		AND slp_id = @numProcessID
	GROUP BY
		vcMileStoneName
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_StagePercentageDetailsTask_GetByStage')
DROP PROCEDURE USP_StagePercentageDetailsTask_GetByStage
GO
CREATE PROCEDURE [dbo].[USP_StagePercentageDetailsTask_GetByStage]                             
	@numDomainID AS NUMERIC(18,0)                            
	,@numStageDetailsId NUMERIC(18,0)
AS                            
BEGIN
	SELECT
		numTaskId
		,vcTaskName
	FROM
		StagePercentageDetailsTask
	WHERE
		numDomainId=@numDomainID
		AND numStageDetailsId=@numStageDetailsId
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetCapacityPlanningTasksReportData')
DROP PROCEDURE USP_GetCapacityPlanningTasksReportData
GO
CREATE PROCEDURE [dbo].[USP_GetCapacityPlanningTasksReportData]                             
	@numDomainID AS NUMERIC(18,0)                            
	,@tintProcessType TINYINT
	,@numProcessID NUMERIC(18,0)
	,@vcMilestone VARCHAR(300)
	,@numStageDetailsId NUMERIC(18,0)
	,@vcTaskIDs VARCHAR(200)
	,@vcGradeIDs VARCHAR(200)
	,@dtFromDate DATETIME
	,@dtToDate DATETIME
	,@tintView TINYINT --1:Day, 2:Week, 3:Month
	,@ClientTimeZoneOffset INT
AS                            
BEGIN
	SELECT 
		vcMileStoneName
	FROM
		StagePercentageDetails
	WHERE
		numDomainId = @numDomainID
		AND slp_id = @numProcessID
		AND (ISNULL(@vcMilestone,'') = '' OR vcMileStoneName = @vcMilestone)
	GROUP BY
		vcMileStoneName

	SELECT 
		numStageDetailsId
		,vcMileStoneName
		,vcStageName
	FROM
		StagePercentageDetails
	WHERE
		numDomainId = @numDomainID
		AND slp_id = @numProcessID
		AND (ISNULL(@vcMilestone,'') = '' OR vcMileStoneName = @vcMilestone)
		AND (ISNULL(@numStageDetailsId,0) = 0 OR numStageDetailsId=@numStageDetailsId)


	DECLARE @TEMP TABLE
	(
		numStageDetailsId NUMERIC(18,0)
		,numTaskID NUMERIC(18,0)
		,vcTaskName VARCHAR(300)
		,numTaskTimeInMinutes NUMERIC(18,0)
	)

	INSERT INTO 
		@TEMP
	SELECT
		SPDT.numStageDetailsId
		,SPDT.numTaskId
		,SPDT.vcTaskName
		,(ISNULL(SPDT.numHours,0) * 60) + ISNULL(SPDT.numMinutes,0)
	FROM
		StagePercentageDetails SPD
	INNER JOIN
		StagePercentageDetailsTask SPDT
	ON
		SPD.numStageDetailsId = SPDT.numStageDetailsId
	WHERE
		SPD.numDomainID=@numDomainID
		AND SPD.slp_id = @numProcessID
		AND (ISNULL(@vcMilestone,'') = '' OR SPD.vcMileStoneName = @vcMilestone)
		AND (ISNULL(@numStageDetailsId,0) = 0 OR SPD.numStageDetailsId=@numStageDetailsId)
		AND (ISNULL(@vcTaskIDs,'') = '' OR SPDT.numTaskId IN (SELECT ID FROM dbo.SplitIDs(@vcTaskIDs,',')))

	SELECT * FROM @TEMP

	DECLARE @TempTaskAssignee TABLE
	(
		numWorkOrderID NUMERIC(18,0)
		,numAssignedTo NUMERIC(18,0)			
		,dtLastTaskCompletionTime DATETIME
	)

	DECLARE @TempTimeLog TABLE
	(
		numWrokOrder NUMERIC(18,0)
		,numTaskID NUMERIC(18,0)
		,numReferenceTaskId NUMERIC(18,0)
		,numQtyItemsReq FLOAT
		,numAssignTo NUMERIC(18,0)
		,numProductiveMinutes NUMERIC(18,0)
		,dtDate DATETIME
		,numTotalMinutes NUMERIC(18,0)
	)

	DECLARE @TempTasks TABLE
	(
		ID INT IDENTITY(1,1)
		,numTaskID NUMERIC(18,0)
		,numReferenceTaskId NUMERIC(18,0)
		,numWorkOrderID NUMERIC(18,0)
		,numAssignTo NUMERIC(18,0)
		,numTaskTimeInMinutes NUMERIC(18,0)
		,intTaskType INT
		,dtStartDate DATETIME
		,dtFinishDate DATETIME
		,numQtyItemsReq FLOAT
		,numProcessedQty FLOAT
	)

	INSERT INTO @TempTasks
	(
		numTaskID
		,numReferenceTaskId
		,numWorkOrderID
		,numAssignTo
		,numTaskTimeInMinutes
		,intTaskType
		,dtStartDate
		,numQtyItemsReq
		,numProcessedQty
	)
	SELECT
		SPDT.numTaskId
		,SPDT.numReferenceTaskId
		,WorkOrder.numWOId
		,SPDT.numAssignTo
		,((ISNULL(SPDT.numHours,0) * 60) + ISNULL(SPDT.numMinutes,0)) * (ISNULL(numQtyItemsReq,0) - ISNULL((SELECT SUM(SPDTTL.numProcessedQty) FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskId = SPDT.numTaskId),0))
		,SPDT.intTaskType
		,(CASE	
			WHEN (SELECT COUNT(*) FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID = SPDT.numTaskId) > 0
			THEN (SELECT MAX(dtActionTime) FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID = SPDT.numTaskId)
			ELSE ISNULL(WorkOrder.dtmStartDate,WorkOrder.bintCreatedDate)
		END)
		,WorkOrder.numQtyItemsReq
		,ISNULL((SELECT SUM(numProcessedQty) FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID = SPDT.numTaskId),0)
	FROM
		StagePercentageDetailsTask SPDT
	INNER JOIN
		WorkOrder
	ON
		SPDT.numWorkOrderId = WorkOrder.numWOId
	WHERE
		SPDT.numDomainID=@numDomainID
		AND (SELECT COUNT(*) FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID = SPDT.numTaskId AND tintAction=4) = 0 -- Task in not finished
		--TODO:uncomment if we have performance hit on live 
		--AND (ISNULL(WorkOrder.dtmStartDate,WorkOrder.bintCreatedDate) BETWEEN @dtFromDate AND @dtToDate OR (SELECT COUNT(*) FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID = SPDT.numTaskId) > 0)
		AND (ISNULL(numQtyItemsReq,0) - ISNULL((SELECT SUM(SPDTTL.numProcessedQty) FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskId = SPDT.numTaskId),0)) > 0

	INSERT INTO @TempTaskAssignee
	(
		numWorkOrderID
		,numAssignedTo
	)
	SELECT
		numWorkOrderID
		,numAssignTo
	FROM
		@TempTasks
	GROUP BY
		numWorkOrderID
		,numAssignTo

	DECLARE @i INT = 1
	DECLARE @iCount INT 	
	SELECT @iCount = COUNT(*) FROM @TempTasks

	DECLARE @numTempTaskID NUMERIC(18,0)
	DECLARE @numTempParentTaskID NUMERIC(18,0)
	DECLARE @numTaskAssignee NUMERIC(18,0)
	DECLARE @intTaskType INT
	DECLARE @numTotalTaskInMinutes NUMERIC(18,0)
	DECLARE @numWorkOrderID NUMERIC(18,0)
	DECLARE @numQtyItemsReq FLOAT

	DECLARE @numWorkScheduleID NUMERIC(18,0)
	DECLARE @numProductiveTimeInMinutes NUMERIC(18,0)
	DECLARE @tmStartOfDay TIME(7)
	DECLARE @numTimeLeftForDay NUMERIC(18,0)
	DECLARE @vcWorkDays VARCHAR(20)
	DECLARE @dtStartDate DATETIME
		
	WHILE @i <= @iCount
	BEGIN
		SELECT
			@numTempTaskID=numTaskID
			,@numTempParentTaskID=numReferenceTaskId
			,@numTaskAssignee=numAssignTo
			,@numTotalTaskInMinutes=numTaskTimeInMinutes
			,@intTaskType = intTaskType
			,@dtStartDate=dtStartDate
			,@numWorkOrderID=numWorkOrderID
			,@numQtyItemsReq=numQtyItemsReq
		FROM
			@TempTasks 
		WHERE
			ID=@i

		-- GET TASK COMPLETION TIME WHICH WILL BE START DATETIME FOR NEXT TASK
		SELECT
			@numWorkScheduleID = WS.ID
			,@numProductiveTimeInMinutes=(ISNULL(WS.numProductiveHours,0) * 60) + ISNULL(WS.numProductiveMinutes,0)
			,@tmStartOfDay = tmStartOfDay
			,@vcWorkDays=vcWorkDays
			,@dtStartDate = DATEADD(MINUTE,@ClientTimeZoneOffset,CONVERT(DATETIME,CONVERT(VARCHAR(8),DATEADD(MINUTE,-@ClientTimeZoneOffset,@dtStartDate),112)) + tmStartOfDay)
		FROM
			WorkSchedule WS
		INNER JOIN
			UserMaster
		ON
			WS.numUserCntID = UserMaster.numUserDetailId
		WHERE 
			WS.numUserCntID = @numTaskAssignee

		IF @intTaskType=1 AND EXISTS (SELECT numAssignedTo FROM @TempTaskAssignee WHERE numWorkOrderID=@numWorkOrderID AND numAssignedTo = @numTaskAssignee AND dtLastTaskCompletionTime IS NOT NULL)
		BEGIN
			SELECT @dtStartDate = dtLastTaskCompletionTime FROM @TempTaskAssignee WHERE numWorkOrderID=@numWorkOrderID AND numAssignedTo = @numTaskAssignee
		END
		ELSE IF EXISTS (SELECT numAssignedTo FROM @TempTaskAssignee WHERE numWorkOrderID=@numWorkOrderID AND numAssignedTo = @numTaskAssignee AND dtLastTaskCompletionTime IS NOT NULL)
		BEGIN
			SELECT @dtStartDate = dtLastTaskCompletionTime FROM @TempTaskAssignee WHERE numWorkOrderID=@numWorkOrderID AND numAssignedTo = @numTaskAssignee
		END

		UPDATE @TempTasks SET dtStartDate=@dtStartDate WHERE ID=@i 

		IF @numProductiveTimeInMinutes > 0 AND @numTotalTaskInMinutes > 0
		BEGIN
			WHILE @numTotalTaskInMinutes > 0
			BEGIN
				-- IF ITS WORKING DAY AND ALSO NOT HOLIDAY THEN PROCEED OR INCRESE DATE TO NEXT DAY
				IF DATEPART(WEEKDAY,@dtStartDate) IN (SELECT Id FROM dbo.SplitIDs(@vcWorkDays,',')) AND NOT EXISTS (SELECT ID FROM WorkScheduleDaysOff WHERE numWorkScheduleID=@numWorkScheduleID AND CAST(DATEADD(MINUTE,@ClientTimeZoneOffset * -1,@dtStartDate) AS DATE) BETWEEN dtDayOffFrom AND dtDayOffTo)
				BEGIN
					-- CHECK TIME LEFT FOR DAY BASED
					SET @numTimeLeftForDay = DATEDIFF(MINUTE,@dtStartDate,DATEADD(MINUTE,@numProductiveTimeInMinutes,DATEADD(MINUTE,@ClientTimeZoneOffset,CONVERT(DATETIME,CONVERT(VARCHAR(8),DATEADD(MINUTE,-@ClientTimeZoneOffset,@dtStartDate),112)) + @tmStartOfDay)))

					IF @numTimeLeftForDay > 0
					BEGIN
						INSERT INTO @TempTimeLog
						(
							numWrokOrder
							,numTaskID
							,numReferenceTaskId
							,numQtyItemsReq
							,numAssignTo
							,numProductiveMinutes
							,dtDate
							,numTotalMinutes
						) 
						VALUES 
						(	
							@numWorkOrderID
							,@numTempTaskID
							,@numTempParentTaskID
							,@numQtyItemsReq
							,@numTaskAssignee
							,@numProductiveTimeInMinutes
							,@dtStartDate
							,(CASE WHEN @numTimeLeftForDay > @numTotalTaskInMinutes THEN @numTotalTaskInMinutes ELSE @numTimeLeftForDay END)
						)

						IF @numTimeLeftForDay > @numTotalTaskInMinutes
						BEGIN
							SET @dtStartDate = DATEADD(MINUTE,@numTotalTaskInMinutes,@dtStartDate)
						END
						ELSE
						BEGIN
							SET @dtStartDate = DATEADD(MINUTE,@ClientTimeZoneOffset,CONVERT(DATETIME,CONVERT(VARCHAR(8),DATEADD(MINUTE,-@ClientTimeZoneOffset,DATEADD(DAY,1,@dtStartDate)),112)) + @tmStartOfDay)
						END
							
						SET @numTotalTaskInMinutes = @numTotalTaskInMinutes - @numTimeLeftForDay
					END
					ELSE
					BEGIN
						SET @dtStartDate = DATEADD(MINUTE,@ClientTimeZoneOffset,CONVERT(DATETIME,CONVERT(VARCHAR(8),DATEADD(MINUTE,-@ClientTimeZoneOffset,DATEADD(DAY,1,@dtStartDate)),112)) + @tmStartOfDay)
					END
				END
				ELSE
				BEGIN
					SET @dtStartDate = DATEADD(MINUTE,@ClientTimeZoneOffset,CONVERT(DATETIME,CONVERT(VARCHAR(8),DATEADD(MINUTE,-@ClientTimeZoneOffset,DATEADD(DAY,1,@dtStartDate)),112)) + @tmStartOfDay)					
				END				
			END
		END	

		UPDATE @TempTasks SET dtFinishDate=@dtStartDate WHERE ID=@i 
		UPDATE @TempTaskAssignee SET dtLastTaskCompletionTime=@dtStartDate WHERE numWorkOrderID=@numWorkOrderID AND numAssignedTo=@numTaskAssignee

		SET @i = @i + 1
	END

	IF @tintView = 1
	BEGIN
		SELECT
			numReferenceTaskId AS numTaskID
			,TT.numTaskID AS numActualTaskID
			,WO.numWOId
			,ISNULL(WO.vcWorkOrderName,'-') vcWorkOrderName
			,I.numItemCode
			,ISNULL(I.vcItemName,'-') vcItemName
			,TT.numAssignTo
			,ISNULL(AdditionalContactsInformation.numTeam,0) numTeam
			,ISNULL((SELECT vcGradeId FROM tblStageGradeDetails WHERE tblStageGradeDetails.numTaskID=numReferenceTaskId AND tblStageGradeDetails.numAssigneId=TT.numAssignTo),'') vcGrade
			,ISNULL(TT.numProcessedQty,0) numProcessedQty
			,ISNULL(WO.numQtyItemsReq,0) - ISNULL(TT.numProcessedQty,0) numRemainingQty
			,CONCAT(FORMAT(ISNULL(TT.numTaskTimeInMinutes,0) / 60,'00'),':',FORMAT(ISNULL(TT.numTaskTimeInMinutes,0) % 60,'00')) vcTimeRequired
			,(CASE	
				WHEN (SELECT COUNT(*) FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID = TT.numTaskId) > 0
				THEN dbo.FormatedDateFromDate(DATEADD(MINUTE,-@ClientTimeZoneOffset,TT.dtStartDate),@numDomainID) + ' ' + CONVERT(VARCHAR(5),DATEADD(MINUTE,-@ClientTimeZoneOffset,TT.dtStartDate), 108) + ' ' + RIGHT(CONVERT(VARCHAR(30),DATEADD(MINUTE,-@ClientTimeZoneOffset,TT.dtStartDate),9),2) 
				ELSE ''
			END) dtStartDate
			,(CASE 
				WHEN TT.dtFinishDate IS NOT NULL
				THEN dbo.FormatedDateFromDate(DATEADD(MINUTE,-@ClientTimeZoneOffset,TT.dtFinishDate),@numDomainID) + ' ' + CONVERT(VARCHAR(5),DATEADD(MINUTE,-@ClientTimeZoneOffset,TT.dtFinishDate), 108) + ' ' + RIGHT(CONVERT(VARCHAR(30),DATEADD(MINUTE,-@ClientTimeZoneOffset,TT.dtFinishDate),9),2)
				ELSE ''
			END) dtFinishDate
			,(CASE 
				WHEN OI.numoppitemtCode > 0 
				THEN (CASE 
						WHEN OI.ItemReleaseDate IS NOT NULL
						THEN dbo.FormatedDateFromDate(DATEADD(MINUTE,-@ClientTimeZoneOffset,OI.ItemReleaseDate),@numDomainID) + ' ' + CONVERT(VARCHAR(5),DATEADD(MINUTE,-@ClientTimeZoneOffset,OI.ItemReleaseDate), 108) + ' ' + RIGHT(CONVERT(VARCHAR(30),DATEADD(MINUTE,-@ClientTimeZoneOffset,OI.ItemReleaseDate),9),2)
						ELSE ''
					END)
				ELSE (CASE 
						WHEN WO.dtmEndDate IS NOT NULL
						THEN dbo.FormatedDateFromDate(DATEADD(MINUTE,-@ClientTimeZoneOffset,WO.dtmEndDate),@numDomainID) + ' ' + CONVERT(VARCHAR(5),DATEADD(MINUTE,-@ClientTimeZoneOffset,WO.dtmEndDate), 108) + ' ' + RIGHT(CONVERT(VARCHAR(30),DATEADD(MINUTE,-@ClientTimeZoneOffset,WO.dtmEndDate),9),2)
						ELSE ''
					END) 
			END) dtReleaseDate
		FROM
			@TEMP T
		INNER JOIN
			@TempTasks TT
		ON
			T.numTaskID = TT.numReferenceTaskId
		INNER JOIN
			AdditionalContactsInformation
		ON
			TT.numAssignTo = AdditionalContactsInformation.numContactId
		INNER JOIN
			WorkOrder WO
		ON
			TT.numWorkOrderID = WO.numWOId
		LEFT JOIN
			OpportunityItems OI
		ON	
			WO.numOppItemID = OI.numoppitemtCode
		INNER JOIN
			Item I
		ON
			WO.numItemCode = I.numItemCode
		WHERE
			TT.numTaskID IN (SELECT TTL.numTaskID FROM @TempTimeLog TTL WHERE dtDate BETWEEN @dtFromDate AND @dtToDate)

		SELECT
			numUserDetailId
			,CONCAT(ISNULL(vcFirstName,'-'),' ',ISNULL(vcLastName,'-')) vcUserName
			,ISNULL(numTeam,0) numTeam
			,CONCAT('[',STUFF((SELECT 
						CONCAT(',{"numTaskID":',numTaskId,',','"vcGradeId":"',vcGradeId,'"}')
					FROM 
						tblStageGradeDetails 
					WHERE 
						numAssigneId=UserMaster.numUserDetailId
					FOR XML PATH('')),1,1,''),']') vcTaskGrades
			,ISNULL((SELECT 
						(CASE WHEN ISNULL(TTL.numProductiveMinutes,0) > 0 THEN (SUM(TTL.numTotalMinutes) * 100) / TTL.numProductiveMinutes ELSE 0 END) numCapacityLoad 
					FROM 
						@TempTimeLog TTL 
					WHERE 
						ISNULL(TTL.numProductiveMinutes,0) > 0
						AND TTL.numAssignTo = UserMaster.numUserDetailId
						AND TTL.dtDate BETWEEN @dtFromDate AND @dtToDate
					GROUP BY 
						TTL.numAssignTo
						,TTL.numProductiveMinutes),0) numCapacityLoad
		FROM
			UserMaster
		INNER JOIN
			AdditionalContactsInformation
		ON
			UserMaster.numUserDetailId = AdditionalContactsInformation.numContactId
		WHERE
			UserMaster.numDomainID=@numDomainID
			AND AdditionalContactsInformation.numDomainID = @numDomainID
			AND bitActivateFlag = 1

		SELECT
			numListItemID numTeam
			,ISNULL(vcData,'-') vcTeamName
			,ISNULL((SELECT 
						AVG(numCapacityLoad) 
					FROM 
					(
						SELECT 
							(CASE WHEN ISNULL(TTL.numProductiveMinutes,0) > 0 THEN (SUM(TTL.numTotalMinutes) * 100) / TTL.numProductiveMinutes ELSE 0 END) numCapacityLoad 
						FROM 
							@TempTimeLog TTL 
						INNER JOIN
							AdditionalContactsInformation ADC
						ON
							TTL.numAssignTo = ADC.numContactId
						WHERE 
							ADC.numTeam	= ListDetails.numListItemID
							AND ISNULL(TTL.numProductiveMinutes,0) > 0
							AND TTL.numAssignTo = ADC.numContactId
							AND TTL.dtDate BETWEEN @dtFromDate AND @dtToDate
						GROUP BY 
							TTL.numAssignTo
							,TTL.numProductiveMinutes
					) TEMP),0) numCapacityLoad
		FROM
			ListDetails
		WHERE
			numListID = 35
			AND numDomainID=@numDomainID

		SELECT
			T.numTaskID
			,ISNULL((SELECT AVG(numCapacityLoad) FROM (SELECT (CASE WHEN ISNULL(TTL.numProductiveMinutes,0) > 0 THEN (SUM(TTL.numTotalMinutes) * 100) / TTL.numProductiveMinutes ELSE 0 END) numCapacityLoad FROM @TempTimeLog TTL INNER JOIN tblStageGradeDetails ON TTL.numAssignTo = tblStageGradeDetails.numAssigneId WHERE tblStageGradeDetails.numTaskID = T.numTaskID AND tblStageGradeDetails.vcGradeId = 'A+' AND TTL.dtDate BETWEEN @dtFromDate AND @dtToDate AND ISNULL(TTL.numProductiveMinutes,0) > 0 GROUP BY TTL.numAssignTo,TTL.numProductiveMinutes) TEMP),0) AS [APlus]
			,ISNULL((SELECT AVG(numCapacityLoad) FROM (SELECT (CASE WHEN ISNULL(TTL.numProductiveMinutes,0) > 0 THEN (SUM(TTL.numTotalMinutes) * 100) / TTL.numProductiveMinutes ELSE 0 END) numCapacityLoad FROM @TempTimeLog TTL INNER JOIN tblStageGradeDetails ON TTL.numAssignTo = tblStageGradeDetails.numAssigneId WHERE tblStageGradeDetails.numTaskID = T.numTaskID AND tblStageGradeDetails.vcGradeId = 'A' AND TTL.dtDate BETWEEN @dtFromDate AND @dtToDate AND ISNULL(TTL.numProductiveMinutes,0) > 0 GROUP BY TTL.numAssignTo,TTL.numProductiveMinutes) TEMP),0) AS [A]
			,ISNULL((SELECT AVG(numCapacityLoad) FROM (SELECT (CASE WHEN ISNULL(TTL.numProductiveMinutes,0) > 0 THEN (SUM(TTL.numTotalMinutes) * 100) / TTL.numProductiveMinutes ELSE 0 END) numCapacityLoad FROM @TempTimeLog TTL INNER JOIN tblStageGradeDetails ON TTL.numAssignTo = tblStageGradeDetails.numAssigneId WHERE tblStageGradeDetails.numTaskID = T.numTaskID AND tblStageGradeDetails.vcGradeId = 'A-' AND TTL.dtDate BETWEEN @dtFromDate AND @dtToDate AND ISNULL(TTL.numProductiveMinutes,0) > 0 GROUP BY TTL.numAssignTo,TTL.numProductiveMinutes) TEMP),0) AS [AMinus]
			,ISNULL((SELECT AVG(numCapacityLoad) FROM (SELECT (CASE WHEN ISNULL(TTL.numProductiveMinutes,0) > 0 THEN (SUM(TTL.numTotalMinutes) * 100) / TTL.numProductiveMinutes ELSE 0 END) numCapacityLoad FROM @TempTimeLog TTL INNER JOIN tblStageGradeDetails ON TTL.numAssignTo = tblStageGradeDetails.numAssigneId WHERE tblStageGradeDetails.numTaskID = T.numTaskID AND tblStageGradeDetails.vcGradeId = 'B+' AND TTL.dtDate BETWEEN @dtFromDate AND @dtToDate AND ISNULL(TTL.numProductiveMinutes,0) > 0 GROUP BY TTL.numAssignTo,TTL.numProductiveMinutes) TEMP),0) AS [BPlus]
			,ISNULL((SELECT AVG(numCapacityLoad) FROM (SELECT (CASE WHEN ISNULL(TTL.numProductiveMinutes,0) > 0 THEN (SUM(TTL.numTotalMinutes) * 100) / TTL.numProductiveMinutes ELSE 0 END) numCapacityLoad FROM @TempTimeLog TTL INNER JOIN tblStageGradeDetails ON TTL.numAssignTo = tblStageGradeDetails.numAssigneId WHERE tblStageGradeDetails.numTaskID = T.numTaskID AND tblStageGradeDetails.vcGradeId = 'B' AND TTL.dtDate BETWEEN @dtFromDate AND @dtToDate AND ISNULL(TTL.numProductiveMinutes,0) > 0 GROUP BY TTL.numAssignTo,TTL.numProductiveMinutes) TEMP),0) AS [B]
			,ISNULL((SELECT AVG(numCapacityLoad) FROM (SELECT (CASE WHEN ISNULL(TTL.numProductiveMinutes,0) > 0 THEN (SUM(TTL.numTotalMinutes) * 100) / TTL.numProductiveMinutes ELSE 0 END) numCapacityLoad FROM @TempTimeLog TTL INNER JOIN tblStageGradeDetails ON TTL.numAssignTo = tblStageGradeDetails.numAssigneId WHERE tblStageGradeDetails.numTaskID = T.numTaskID AND tblStageGradeDetails.vcGradeId = 'B-' AND TTL.dtDate BETWEEN @dtFromDate AND @dtToDate AND ISNULL(TTL.numProductiveMinutes,0) > 0 GROUP BY TTL.numAssignTo,TTL.numProductiveMinutes) TEMP),0) AS [BMinus]
			,ISNULL((SELECT AVG(numCapacityLoad) FROM (SELECT (CASE WHEN ISNULL(TTL.numProductiveMinutes,0) > 0 THEN (SUM(TTL.numTotalMinutes) * 100) / TTL.numProductiveMinutes ELSE 0 END) numCapacityLoad FROM @TempTimeLog TTL INNER JOIN tblStageGradeDetails ON TTL.numAssignTo = tblStageGradeDetails.numAssigneId WHERE tblStageGradeDetails.numTaskID = T.numTaskID AND tblStageGradeDetails.vcGradeId = 'C+' AND TTL.dtDate BETWEEN @dtFromDate AND @dtToDate AND ISNULL(TTL.numProductiveMinutes,0) > 0 GROUP BY TTL.numAssignTo,TTL.numProductiveMinutes) TEMP),0) AS [CPlus]
			,ISNULL((SELECT AVG(numCapacityLoad) FROM (SELECT (CASE WHEN ISNULL(TTL.numProductiveMinutes,0) > 0 THEN (SUM(TTL.numTotalMinutes) * 100) / TTL.numProductiveMinutes ELSE 0 END) numCapacityLoad FROM @TempTimeLog TTL INNER JOIN tblStageGradeDetails ON TTL.numAssignTo = tblStageGradeDetails.numAssigneId WHERE tblStageGradeDetails.numTaskID = T.numTaskID AND tblStageGradeDetails.vcGradeId = 'C' AND TTL.dtDate BETWEEN @dtFromDate AND @dtToDate AND ISNULL(TTL.numProductiveMinutes,0) > 0 GROUP BY TTL.numAssignTo,TTL.numProductiveMinutes) TEMP),0) AS [C]
			,ISNULL((SELECT AVG(numCapacityLoad) FROM (SELECT (CASE WHEN ISNULL(TTL.numProductiveMinutes,0) > 0 THEN (SUM(TTL.numTotalMinutes) * 100) / TTL.numProductiveMinutes ELSE 0 END) numCapacityLoad FROM @TempTimeLog TTL INNER JOIN tblStageGradeDetails ON TTL.numAssignTo = tblStageGradeDetails.numAssigneId WHERE tblStageGradeDetails.numTaskID = T.numTaskID AND tblStageGradeDetails.vcGradeId = 'C-' AND TTL.dtDate BETWEEN @dtFromDate AND @dtToDate AND ISNULL(TTL.numProductiveMinutes,0) > 0 GROUP BY TTL.numAssignTo,TTL.numProductiveMinutes) TEMP),0) AS [CMinus]
		FROM
			@TEMP T
	END
	ELSE 
	IF @tintView = 2 OR @tintView = 3
	BEGIN
		DECLARE @TEMPResult TABLE
		(
			dtDate DATE
			,numTaskID NUMERIC(18,0)
			,numTotalTasks NUMERIC(18,0)
			,numMaxTasks NUMERIC(18,0)
			,numAssigneeCapacityLoad NUMERIC(18,0)
			,numGradeCapacity NUMERIC(18,0)
			,numGradeCapacityLoad NUMERIC(18,0)
		)

		WHILE @dtFromDate <= @dtToDate
		BEGIN
			INSERT INTO @TEMPResult
			(
				dtDate
				,numTaskID
				,numTotalTasks
				,numMaxTasks
				,numAssigneeCapacityLoad
				,numGradeCapacity
				,numGradeCapacityLoad
			)
			SELECT
				@dtFromDate
				,numTaskID
				,ISNULL((SELECT 
							SUM(TTL.numQtyItemsReq) 
						FROM 
							@TempTimeLog TTL 
						WHERE 
							TTL.numReferenceTaskId=T.numTaskID 
							AND CAST(dtDate AS DATE) = CAST(@dtFromDate AS DATE)
						GROUP BY
							TTL.numReferenceTaskId
							,TTL.numQtyItemsReq),0)
				,ISNULL((SELECT 
							SUM(numMaxTask) 
						FROM
						(
							SELECT 
								(CASE 
									WHEN ISNULL(TTL.numProductiveMinutes,0) > ISNULL(SUM(TTL.numTotalMinutes),0)
									THEN ((ISNULL(TTL.numProductiveMinutes,0) - ISNULL(SUM(TTL.numTotalMinutes),0)) / ISNULL(T.numTaskTimeInMinutes,0))
									ELSE ISNULL(TTL.numProductiveMinutes,0) / ISNULL(T.numTaskTimeInMinutes,0)
								END) numMaxTask 
							FROM 
								@TempTimeLog TTL
							WHERE 
								ISNULL(TTL.numProductiveMinutes,0) > 0
								AND CAST(dtDate AS DATE) = CAST(@dtFromDate AS DATE)
							GROUP BY 
								TTL.numAssignTo
								,TTL.numProductiveMinutes
							) TEMP),0)
				,ISNULL((SELECT 
							AVG(numCapacityLoad) 
						FROM 
						(
							SELECT 
								(CASE WHEN ISNULL(TTL.numProductiveMinutes,0) > 0 THEN (SUM(TTL.numTotalMinutes) * 100) / TTL.numProductiveMinutes ELSE 0 END) numCapacityLoad 
							FROM 
								@TempTimeLog TTL 
							WHERE 
								ISNULL(TTL.numProductiveMinutes,0) > 0
								AND CAST(dtDate AS DATE) = CAST(@dtFromDate AS DATE) 
							GROUP BY 
								TTL.numAssignTo
								,TTL.numProductiveMinutes
						) TEMP),0)
				,ISNULL((SELECT 
							SUM(numMaxTask) 
						FROM
						(
							SELECT
								(CASE 
									WHEN (ISNULL(WS.numProductiveHours,0) * 60 + ISNULL(WS.numProductiveMinutes,0)) > ISNULL(TTL.numTotalMinutes,0)
									THEN (((ISNULL(WS.numProductiveHours,0) * 60 + ISNULL(WS.numProductiveMinutes,0)) - ISNULL(TTL.numTotalMinutes,0)) / ISNULL(T.numTaskTimeInMinutes,0))
									ELSE (ISNULL(WS.numProductiveHours,0) * 60 + ISNULL(WS.numProductiveMinutes,0)) / ISNULL(T.numTaskTimeInMinutes,0)
								END) numMaxTask
							FROM
								tblStageGradeDetails TSGD 
							INNER JOIN
								WorkSchedule WS
							ON
								TSGD.numAssigneId = WS.numUserCntID
							OUTER APPLY
							(
								SELECT
									SUM(numTotalMinutes) numTotalMinutes
								FROM 
									@TempTimeLog TTL
								WHERE 
									CAST(dtDate AS DATE) = CAST(@dtFromDate AS DATE) 
									AND TTL.numAssignTo = TSGD.numAssigneId
							) TTL
							WHERE 
								TSGD.numTaskId=T.numTaskID 
								AND (ISNULL(WS.numProductiveHours,0) * 60 + ISNULL(WS.numProductiveMinutes,0)) > 0
								AND (ISNULL(@vcGradeIDs,'') = '' OR vcGradeId IN (SELECT OutParam FROM dbo.SplitString(@vcGradeIDs,',')))
							GROUP BY 
								TSGD.numAssigneId
								,WS.numProductiveHours
								,WS.numProductiveMinutes
								,TTL.numTotalMinutes) TEMP),0)
				,ISNULL((SELECT 
							AVG(numCapacityLoad) 
						FROM
						(
							SELECT
								(CASE WHEN (ISNULL(WS.numProductiveHours,0) * 60 + ISNULL(WS.numProductiveMinutes,0)) > 0 THEN (ISNULL(TTL.numTotalMinutes,0) * 100) / (ISNULL(WS.numProductiveHours,0) * 60 + ISNULL(WS.numProductiveMinutes,0)) ELSE 0 END) numCapacityLoad
							FROM
								tblStageGradeDetails TSGD 
							INNER JOIN
								WorkSchedule WS
							ON
								TSGD.numAssigneId = WS.numUserCntID
							OUTER APPLY
							(
								SELECT
									SUM(numTotalMinutes) numTotalMinutes
								FROM 
									@TempTimeLog TTL
								WHERE 
									CAST(dtDate AS DATE) = CAST(@dtFromDate AS DATE) 
									AND TTL.numAssignTo = TSGD.numAssigneId
							) TTL
							WHERE 
								TSGD.numTaskId = T.numTaskId
								AND (ISNULL(WS.numProductiveHours,0) * 60 + ISNULL(WS.numProductiveMinutes,0)) > 0
								AND (ISNULL(@vcGradeIDs,'') = '' OR vcGradeId IN (SELECT OutParam FROM dbo.SplitString(@vcGradeIDs,',')))
							GROUP BY 
								TSGD.numAssigneId
								,WS.numProductiveHours
								,WS.numProductiveMinutes
								,TTL.numTotalMinutes) TEMP),0)
			FROM
				@TEMP T
			
			SET @dtFromDate = DATEADD(DAY,1,@dtFromDate)
		END

		--SELECT * FROM @TempTimeLog ORDER BY numWrokOrder,numTaskID,dtDate
		SELECT * FROM @TEMPResult
	END
END
GO
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_tblStageGradeDetails_GetByProcess')
DROP PROCEDURE dbo.USP_tblStageGradeDetails_GetByProcess
GO
CREATE PROCEDURE [dbo].[USP_tblStageGradeDetails_GetByProcess]
(
	@numDomainID NUMERIC(18,0)
	,@numProcessID NUMERIC(18,0)
)
AS 
BEGIN
	SELECT
		tblStageGradeDetails.numTaskId
		,tblStageGradeDetails.numAssigneId
		,tblStageGradeDetails.vcGradeId
	FROM
		StagePercentageDetails
	INNER JOIN
		tblStageGradeDetails
	ON
		StagePercentageDetails.numStageDetailsId = tblStageGradeDetails.numStageDetailsId
	WHERE
		StagePercentageDetails.numDomainId = @numDomainID
		AND StagePercentageDetails.slp_id = @numProcessID
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetEmployee')
DROP PROCEDURE USP_GetEmployee
GO
CREATE PROCEDURE [dbo].[USP_GetEmployee]       
	@numDomainID NUMERIC(18,0)
as    
BEGIN   
	SELECT 
		A.numContactId AS numContactID
		,CONCAT(A.vcFirstName,' ',A.vcLastName) as vcUserName
		,ISNULL(A.numTeam,0) numTeam
	FROM 
		UserMaster UM
	INNER JOIN
		AdditionalContactsInformation AS A  
	ON
		UM.numUserDetailId = A.numContactId
	WHERE 
		UM.numDomainID = @numDomainID
		AND A.numDomainID=@numDomainId 
		AND ISNULL(UM.bitActivateFlag,0) = 1
	ORDER BY
		CONCAT(A.vcFirstName,' ',A.vcLastName)
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetCapacityPlanningResourceReportData')
DROP PROCEDURE USP_GetCapacityPlanningResourceReportData
GO
CREATE PROCEDURE [dbo].[USP_GetCapacityPlanningResourceReportData]                             
	@numDomainID AS NUMERIC(18,0)
	,@vcTeams VARCHAR(200)
	,@vcEmployees VARCHAR(200)
	,@tintProcessType TINYINT
	,@numProcessID NUMERIC(18,0)
	,@vcMilestone VARCHAR(300)
	,@numStageDetailsId NUMERIC(18,0)
	,@vcTaskIDs VARCHAR(200)
	,@dtFromDate DATETIME
	,@dtToDate DATETIME
	,@tintView TINYINT --1:Day, 2:Week, 3:Month
	,@ClientTimeZoneOffset INT
AS                            
BEGIN
	SELECT
		numListItemID
		,ISNULL(vcData,'-') vcData
	FROM
		ListDetails 
	WHERE
		numDomainID = @numDomainID
		AND numListID = 35
		AND (ISNULL(@vcTeams,'') = '' OR numListItemID IN (SELECT Id FROM dbo.SplitIDs(@vcTeams,','))) 

	SELECT 
		ACI.numContactId
		,CONCAT(ACI.vcFirstName,' ',ACI.vcLastName) vcContactName
		,ISNULL(ACI.numTeam,0) numTeam 
	FROM
		UserMaster UM
	INNER JOIN
		AdditionalContactsInformation ACI
	ON
		UM.numUserDetailId = ACI.numContactId
	WHERE
		UM.numDomainID = @numDomainID
		AND ACI.numDomainID = @numDomainID
		AND ISNULL(UM.bitActivateFlag,0) = 1
		AND (ISNULL(@vcTeams,'') = '' OR numTeam IN (SELECT Id FROM dbo.SplitIDs(@vcTeams,','))) 
		AND (ISNULL(@vcEmployees,'') = '' OR numContactId IN (SELECT Id FROM dbo.SplitIDs(@vcEmployees,',')))  

	DECLARE @EmployeeTime TABLE
	(	
		numUserCntID NUMERIC(18,0)
		,numRecordID NUMERIC(18,0)
		,tintRecordType TINYINT
		,vcSource VARCHAR(200)
		,dtDate DATE
		,vcDate VARCHAR(50)
		,numType TINYINT
		,vcType VARCHAR(500)
		,vcHours VARCHAR(20)
		,numMinutes INT
		,vcDetails VARCHAR(MAX)
	)

	-- Time & Expense
	INSERT INTO @EmployeeTime
	(
		numUserCntID
		,numRecordID
		,tintRecordType
		,vcSource
		,dtDate
		,vcDate
		,numType
		,vcType
		,vcHours
		,numMinutes
		,vcDetails
	)
	SELECT
		TimeAndExpense.numUserCntID
		,TimeAndExpense.numCategoryHDRID
		,1
		,'Time & Expense' AS vcSource
		,dtFromDate
		,dbo.FormatedDateFromDate(DATEADD(MINUTE,@ClientTimeZoneOffset * -1,dtFromDate),@numDomainID) AS vcDate
		,TimeAndExpense.numType
		,(CASE 
			WHEN TimeAndExpense.numType = 4 THEN 'Absence (Un-Paid)' 
			WHEN TimeAndExpense.numType = 3 THEN 'Absence (Paid)' 
			WHEN TimeAndExpense.numType = 2 Then 'Non-Billable Time' 
			WHEN TimeAndExpense.numType = 1 Then 'Billable' 
			ELSE '' 
		END) AS vcType
		,CONVERT(varchar(5),DATEADD(MINUTE,DATEDIFF(MINUTE,TimeAndExpense.dtFromDate,TimeAndExpense.dtToDate),0),108) vcHours
		,DATEDIFF(MINUTE,TimeAndExpense.dtFromDate,TimeAndExpense.dtToDate) numMinutes
		,ISNULL(TimeAndExpense.txtDesc,'') vcDetails
	FROM
		TimeAndExpense 
	LEFT JOIN
		OpportunityMaster
	ON
		TimeAndExpense.numOppID=OpportunityMaster.numOppID
	WHERE
		TimeAndExpense.numDomainID = @numDomainID
		AND ISNULL(numCategory,0) = 1
		AND numType NOT IN (4) -- DO NOT INCLUDE UNPAID LEAVE
		AND dtFromDate BETWEEN @dtFromDate AND @dtToDate
	ORDER BY
		dtFromDate

	-- Action Item
	INSERT INTO @EmployeeTime
	(
		numUserCntID
		,numRecordID
		,tintRecordType
		,vcSource
		,dtDate
		,vcDate
		,numType
		,vcType
		,vcHours
		,numMinutes
		,vcDetails
	)
	SELECT
		Communication.numAssign
		,Communication.numCommId
		,2
		,CONCAT('<a target="_blank" href="../admin/actionitemdetails.aspx?CommId=',Communication.numCommId,'&lngType=0">'
				,dbo.GetListIemName(bitTask),'</a>',(CASE WHEN DM.numDivisionID > 0 THEN CONCAT(' (<a target="_blank" href="',(CASE WHEN DM.tintCRMType = 2 THEN '../account/frmAccounts.aspx?DivID=' WHEN DM.tintCRMType = 1 THEN '../prospects/frmProspects.aspx?DivID=' ELSE '../Leads/frmLeads.aspx?DivID=' END),Dm.numDivisionID,'">',ISNULL(CI.vcCompanyName,'-'),'</a>)') ELSE '' END)
		) AS vcSource
		,dtStartTime
		,dbo.FormatedDateFromDate(DATEADD(MINUTE,@ClientTimeZoneOffset * -1,Communication.dtStartTime),@numDomainID) AS vcDate
		,0
		,dbo.GetListIemName(bitTask) AS vcType
		,CONVERT(varchar(5),DATEADD(MINUTE,DATEDIFF(MINUTE,Communication.dtStartTime,Communication.dtEndTime),0),108) vcHours
		,DATEDIFF(MINUTE,Communication.dtStartTime,Communication.dtEndTime) numMinutes
		,ISNULL(Communication.textDetails,'') vcDetails
	FROM
		Communication 
	LEFT JOIN
		DivisionMaster DM
	ON
		Communication.numDivisionId = DM.numDivisionID
	LEFT JOIN
		CompanyInfo CI
	ON
		DM.numCompanyID = CI.numCompanyId
	WHERE
		Communication.numDomainID = @numDomainID
		AND Communication.dtStartTime BETWEEN @dtFromDate AND @dtToDate
		AND ISNULL(bitClosedFlag,0) = 1
	ORDER BY
		dtStartTime

	-- Calendar
	INSERT INTO @EmployeeTime
	(
		numUserCntID
		,numRecordID
		,tintRecordType
		,vcSource
		,dtDate
		,vcDate
		,numType
		,vcType
		,vcHours
		,numMinutes
		,vcDetails
	)
	SELECT
		[Resource].numUserCntId
		,Activity.ActivityID
		,3
		,CONCAT('<a target="_blank" href="../admin/actionitemdetails.aspx?CommId=',Activity.ActivityID,'&lngType=1">','Action Item','</a>') AS vcSource
		,Activity.StartDateTimeUtc
		,dbo.FormatedDateFromDate(DATEADD(MINUTE,@ClientTimeZoneOffset * -1,Activity.StartDateTimeUtc),@numDomainID) AS vcDate
		,0
		,'Calendar' AS vcType
		,CONVERT(varchar(5),DATEADD(MINUTE,DATEDIFF(MINUTE,Activity.StartDateTimeUtc,DATEADD(SECOND,Activity.Duration,Activity.StartDateTimeUtc)),0),108) vcHours
		,DATEDIFF(MINUTE,Activity.StartDateTimeUtc,DATEADD(SECOND,Activity.Duration,Activity.StartDateTimeUtc)) numMinutes
		,ISNULL(Activity.ActivityDescription,'') vcDetails
	FROM
		[Resource]
	INNER JOIN
		ActivityResource 
	ON
		[Resource].ResourceID = ActivityResource.ResourceID
	INNER JOIN
		Activity 
	ON
		ActivityResource.ActivityID=Activity.ActivityID
	WHERE
		Activity.StartDateTimeUtc BETWEEN @dtFromDate AND @dtToDate
	ORDER BY
		Activity.StartDateTimeUtc


	DECLARE @TempTaskAssignee TABLE
	(
		numWorkOrderID NUMERIC(18,0)
		,numProjectID NUMERIC(18,0)
		,numAssignedTo NUMERIC(18,0)			
		,dtLastTaskCompletionTime DATETIME
	)

	DECLARE @TempTimeLog TABLE
	(
		numWrokOrder NUMERIC(18,0)
		,numProjectID NUMERIC(18,0)
		,numTaskID NUMERIC(18,0)
		,numReferenceTaskId NUMERIC(18,0)
		,numQtyItemsReq FLOAT
		,numAssignTo NUMERIC(18,0)
		,numProductiveMinutes NUMERIC(18,0)
		,dtDate DATETIME
		,numTotalMinutes NUMERIC(18,0)
	)

	DECLARE @TempTasks TABLE
	(
		ID INT IDENTITY(1,1)
		,numTaskID NUMERIC(18,0)
		,numReferenceTaskId NUMERIC(18,0)
		,numWorkOrderID NUMERIC(18,0)
		,numProjectID NUMERIC(18,0)
		,numAssignTo NUMERIC(18,0)
		,numTaskTimeInMinutes NUMERIC(18,0)
		,intTaskType INT
		,dtStartDate DATETIME
		,dtFinishDate DATETIME
		,numQtyItemsReq FLOAT
		,numProcessedQty FLOAT
	)

	INSERT INTO @TempTasks
	(
		numTaskID
		,numReferenceTaskId
		,numWorkOrderID
		,numProjectID
		,numAssignTo
		,numTaskTimeInMinutes
		,intTaskType
		,dtStartDate
		,numQtyItemsReq
		,numProcessedQty
	)
	SELECT
		SPDT.numTaskId
		,SPDT.numReferenceTaskId
		,WorkOrder.numWOId
		,ProjectsMaster.numProId
		,SPDT.numAssignTo
		,((ISNULL(SPDT.numHours,0) * 60) + ISNULL(SPDT.numMinutes,0)) * (ISNULL(numQtyItemsReq,0) - ISNULL((SELECT SUM(SPDTTL.numProcessedQty) FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskId = SPDT.numTaskId),0))
		,SPDT.intTaskType
		,(CASE	
			WHEN (SELECT COUNT(*) FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID = SPDT.numTaskId) > 0
			THEN (SELECT MAX(dtActionTime) FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID = SPDT.numTaskId)
			ELSE ISNULL(WorkOrder.dtmStartDate,WorkOrder.bintCreatedDate)
		END)
		,(CASE WHEN WorkOrder.numWOId IS NOT NULL THEN WorkOrder.numQtyItemsReq ELSE 1 END)
		,ISNULL((SELECT SUM(numProcessedQty) FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID = SPDT.numTaskId),0)
	FROM
		StagePercentageDetailsTask SPDT
	LEFT JOIN
		WorkOrder
	ON
		SPDT.numWorkOrderId = WorkOrder.numWOId
	LEFT JOIN
		ProjectsMaster
	ON
		SPDT.numProjectId = ProjectsMaster.numProId
	WHERE
		SPDT.numDomainID=@numDomainID
		AND (SELECT COUNT(*) FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID = SPDT.numTaskId AND tintAction=4) = 0 -- Task in not finished
		AND (ISNULL(numWorkOrderId,0) > 0 OR ISNULL(numProId,0) > 0)
		--TODO: Uncomment if there is performance issue on production
		--AND ((ISNULL(WorkOrder.dtmStartDate,WorkOrder.bintCreatedDate) BETWEEN @dtFromDate AND @dtToDate AND (ISNULL(numQtyItemsReq,0) - ISNULL((SELECT SUM(SPDTTL.numProcessedQty) FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskId = SPDT.numTaskId),0)) > 0) 
		--		OR (ISNULL(ProjectsMaster.dtmStartDate,ProjectsMaster.bintCreatedDate) BETWEEN @dtFromDate AND @dtToDate)
		--		OR (SELECT COUNT(*) FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID = SPDT.numTaskId) > 0)

	INSERT INTO @TempTaskAssignee
	(
		numWorkOrderID
		,numProjectID
		,numAssignedTo
	)
	SELECT
		numWorkOrderID
		,numProjectID
		,numAssignTo
	FROM
		@TempTasks
	GROUP BY
		numWorkOrderID
		,numProjectID
		,numAssignTo

	DECLARE @i INT = 1
	DECLARE @iCount INT 	
	SELECT @iCount = COUNT(*) FROM @TempTasks

	DECLARE @numTempTaskID NUMERIC(18,0)
	DECLARE @numTempParentTaskID NUMERIC(18,0)
	DECLARE @numTaskAssignee NUMERIC(18,0)
	DECLARE @intTaskType INT
	DECLARE @numTotalTaskInMinutes NUMERIC(18,0)
	DECLARE @numWorkOrderID NUMERIC(18,0)
	DECLARE @numProjectID NUMERIC(18,0)
	DECLARE @numQtyItemsReq FLOAT

	DECLARE @numWorkScheduleID NUMERIC(18,0)
	DECLARE @numProductiveTimeInMinutes NUMERIC(18,0)
	DECLARE @tmStartOfDay TIME(7)
	DECLARE @numTimeLeftForDay NUMERIC(18,0)
	DECLARE @vcWorkDays VARCHAR(20)
	DECLARE @dtStartDate DATETIME
		
	WHILE @i <= @iCount
	BEGIN
		SELECT
			@numTempTaskID=numTaskID
			,@numTempParentTaskID=numReferenceTaskId
			,@numTaskAssignee=numAssignTo
			,@numTotalTaskInMinutes=numTaskTimeInMinutes
			,@intTaskType = intTaskType
			,@dtStartDate=dtStartDate
			,@numWorkOrderID=numWorkOrderID
			,@numProjectID=numProjectID
			,@numQtyItemsReq=numQtyItemsReq
		FROM
			@TempTasks 
		WHERE
			ID=@i

		-- GET TASK COMPLETION TIME WHICH WILL BE START DATETIME FOR NEXT TASK
		SELECT
			@numWorkScheduleID = WS.ID
			,@numProductiveTimeInMinutes=(ISNULL(WS.numProductiveHours,0) * 60) + ISNULL(WS.numProductiveMinutes,0)
			,@tmStartOfDay = tmStartOfDay
			,@vcWorkDays=vcWorkDays
			,@dtStartDate = DATEADD(MINUTE,@ClientTimeZoneOffset,CONVERT(DATETIME,CONVERT(VARCHAR(8),DATEADD(MINUTE,-@ClientTimeZoneOffset,@dtStartDate),112)) + tmStartOfDay)
		FROM
			WorkSchedule WS
		INNER JOIN
			UserMaster
		ON
			WS.numUserCntID = UserMaster.numUserDetailId
		WHERE 
			WS.numUserCntID = @numTaskAssignee

		IF @intTaskType=1 AND EXISTS (SELECT numAssignedTo FROM @TempTaskAssignee WHERE ISNULL(numWorkOrderID,0)=ISNULL(@numWorkOrderID,0) AND ISNULL(numProjectID,0) = ISNULL(@numProjectID,0) AND numAssignedTo = @numTaskAssignee AND dtLastTaskCompletionTime IS NOT NULL)
		BEGIN
			SELECT @dtStartDate = dtLastTaskCompletionTime FROM @TempTaskAssignee WHERE ISNULL(numWorkOrderID,0)=ISNULL(@numWorkOrderID,0) AND ISNULL(numProjectID,0) = ISNULL(@numProjectID,0) AND numAssignedTo = @numTaskAssignee
		END
		ELSE IF EXISTS (SELECT numAssignedTo FROM @TempTaskAssignee WHERE ISNULL(numWorkOrderID,0)=ISNULL(@numWorkOrderID,0) AND ISNULL(numProjectID,0) = ISNULL(@numProjectID,0) AND numAssignedTo = @numTaskAssignee AND dtLastTaskCompletionTime IS NOT NULL)
		BEGIN
			SELECT @dtStartDate = dtLastTaskCompletionTime FROM @TempTaskAssignee WHERE ISNULL(numWorkOrderID,0)=ISNULL(@numWorkOrderID,0) AND ISNULL(numProjectID,0) = ISNULL(@numProjectID,0) AND numAssignedTo = @numTaskAssignee
		END

		UPDATE @TempTasks SET dtStartDate=@dtStartDate WHERE ID=@i 

		IF @numProductiveTimeInMinutes > 0 AND @numTotalTaskInMinutes > 0
		BEGIN
			WHILE @numTotalTaskInMinutes > 0
			BEGIN
				-- IF ITS WORKING DAY AND ALSO NOT HOLIDAY THEN PROCEED OR INCRESE DATE TO NEXT DAY
				IF DATEPART(WEEKDAY,@dtStartDate) IN (SELECT Id FROM dbo.SplitIDs(@vcWorkDays,',')) AND NOT EXISTS (SELECT ID FROM WorkScheduleDaysOff WHERE numWorkScheduleID=@numWorkScheduleID AND CAST(DATEADD(MINUTE,@ClientTimeZoneOffset * -1,@dtStartDate) AS DATE) BETWEEN dtDayOffFrom AND dtDayOffTo)
				BEGIN
					-- CHECK TIME LEFT FOR DAY BASED
					SET @numTimeLeftForDay = DATEDIFF(MINUTE,@dtStartDate,DATEADD(MINUTE,@numProductiveTimeInMinutes,DATEADD(MINUTE,@ClientTimeZoneOffset,CONVERT(DATETIME,CONVERT(VARCHAR(8),DATEADD(MINUTE,-@ClientTimeZoneOffset,@dtStartDate),112)) + @tmStartOfDay)))

					IF @numTimeLeftForDay > 0
					BEGIN
						INSERT INTO @TempTimeLog
						(
							numWrokOrder
							,numProjectID
							,numTaskID
							,numReferenceTaskId
							,numQtyItemsReq
							,numAssignTo
							,numProductiveMinutes
							,dtDate
							,numTotalMinutes
						) 
						VALUES 
						(	
							@numWorkOrderID
							,@numProjectID
							,@numTempTaskID
							,@numTempParentTaskID
							,@numQtyItemsReq
							,@numTaskAssignee
							,@numProductiveTimeInMinutes
							,@dtStartDate
							,(CASE WHEN @numTimeLeftForDay > @numTotalTaskInMinutes THEN @numTotalTaskInMinutes ELSE @numTimeLeftForDay END)
						)

						IF @numTimeLeftForDay > @numTotalTaskInMinutes
						BEGIN
							SET @dtStartDate = DATEADD(MINUTE,@numTotalTaskInMinutes,@dtStartDate)
						END
						ELSE
						BEGIN
							SET @dtStartDate = DATEADD(MINUTE,@ClientTimeZoneOffset,CONVERT(DATETIME,CONVERT(VARCHAR(8),DATEADD(MINUTE,-@ClientTimeZoneOffset,DATEADD(DAY,1,@dtStartDate)),112)) + @tmStartOfDay)
						END
							
						SET @numTotalTaskInMinutes = @numTotalTaskInMinutes - @numTimeLeftForDay
					END
					ELSE
					BEGIN
						SET @dtStartDate = DATEADD(MINUTE,@ClientTimeZoneOffset,CONVERT(DATETIME,CONVERT(VARCHAR(8),DATEADD(MINUTE,-@ClientTimeZoneOffset,DATEADD(DAY,1,@dtStartDate)),112)) + @tmStartOfDay)
					END
				END
				ELSE
				BEGIN
					SET @dtStartDate = DATEADD(MINUTE,@ClientTimeZoneOffset,CONVERT(DATETIME,CONVERT(VARCHAR(8),DATEADD(MINUTE,-@ClientTimeZoneOffset,DATEADD(DAY,1,@dtStartDate)),112)) + @tmStartOfDay)					
				END				
			END
		END	

		UPDATE @TempTasks SET dtFinishDate=@dtStartDate WHERE ID=@i 
		UPDATE @TempTaskAssignee SET dtLastTaskCompletionTime=@dtStartDate WHERE ISNULL(numWorkOrderID,0)=ISNULL(@numWorkOrderID,0) AND ISNULL(numProjectID,0) = ISNULL(@numProjectID,0) AND numAssignedTo=@numTaskAssignee

		SET @i = @i + 1
	END

	IF @tintView = 1
	BEGIN
		INSERT INTO @EmployeeTime
		(
			numUserCntID
			,numRecordID
			,tintRecordType
			,vcSource
			,dtDate
			,vcDate
			,numType
			,vcType
			,vcHours
			,numMinutes
			,vcDetails
		)
		SELECT
			TTL.numAssignTo
			,(CASE WHEN ISNULL(TTL.numWrokOrder,0) > 0 THEN TTL.numWrokOrder ELSE TTL.numProjectID END)
			,(CASE WHEN ISNULL(TTL.numWrokOrder,0) > 0 THEN 4 ELSE 5 END)
			,(CASE 
				WHEN ISNULL(TTL.numWrokOrder,0) > 0 
				THEN CONCAT('<a target="_blank" href="../Items/frmWorkOrder.aspx?WOID=',WO.numWOId,'">',ISNULL(WO.vcWorkOrderName,'-'),'</a>') 
				ELSE CONCAT('<a target="_blank" href="../projects/frmProjects.aspx?ProId=',PM.numProId,'">',ISNULL(PM.vcProjectName,'-'),'</a>')
			END) 
			,TTL.dtDate
			,dbo.FormatedDateFromDate(DATEADD(MINUTE,@ClientTimeZoneOffset * -1,TTL.dtDate),@numDomainID)
			,0
			,'Work Order'
			,CONCAT(FORMAT(ISNULL(TTL.numTotalMinutes,0) / 60,'00'),':',FORMAT(ISNULL(TTL.numTotalMinutes,0) % 60,'00'))
			,ISNULL(TTL.numTotalMinutes,0)
			,ISNULL(SPDT.vcTaskName,'') vcDetails
		FROM
			@TempTimeLog TTL
		INNER JOIN
			StagePercentageDetailsTask SPDT
		ON
			TTL.numTaskID = SPDT.numTaskId
		LEFT JOIN
			WorkOrder WO
		ON
			TTL.numWrokOrder = WO.numWOId
		LEFT JOIN
			ProjectsMaster PM
		ON
			TTL.numProjectID = PM.numProId
		WHERE
			dtDate BETWEEN @dtFromDate AND @dtToDate

		SELECT * FROM @EmployeeTime ORDER BY dtDate
	END
	ELSE IF @tintView = 2 OR @tintView = 3
	BEGIN
		DECLARE @TEMPResult TABLE
		(
			dtDate DATE
			,numUserCntID NUMERIC(18,0)
			,numTotalMinutes NUMERIC(18,0)
			,vcTotalHours VARCHAR(20)
			,numProductiveMinutes NUMERIC(18,0)
			,vcMaxHours VARCHAR(20)
			,numTotalBuildTasks NUMERIC(18,0)
			,numTotalProjectTasks NUMERIC(18,0)
			,numCapacityLoad NUMERIC(18,0)
		)

		WHILE @dtFromDate <= @dtToDate
		BEGIN
			INSERT INTO @TEMPResult
			(
				dtDate
				,numUserCntID
				,numTotalMinutes
				,numProductiveMinutes
				,vcMaxHours
				,numTotalBuildTasks
				,numTotalProjectTasks
			)	
			SELECT
				@dtFromDate
				,TTL.numAssignTo
				,ISNULL((SELECT SUM(numMinutes) FROM @EmployeeTime WHERE numUserCntID=TTL.numAssignTo AND CAST(dtDate AS DATE) = CAST(@dtFromDate AS DATE)),0) + ISNULL((SELECT SUM(numTotalMinutes) FROM @TempTimeLog TTLInner WHERE TTLInner.numAssignTo = TTL.numAssignTo AND CAST(TTLInner.dtdate AS DATE)  = CAST(@dtFromDate AS DATE)),0)
				,ISNULL(numProductiveMinutes,0)
				,CONCAT(FORMAT(ISNULL(numProductiveMinutes,0) / 60,'00'),':',FORMAT(ISNULL(numProductiveMinutes,0) % 60,'00'))
				,ISNULL((SELECT SUM(T.numQtyItemsReq) FROM (SELECT numQtyItemsReq FROM @TempTimeLog TTLInner WHERE TTLInner.numAssignTo = TTL.numAssignTo AND ISNULL(TTLInner.numWrokOrder,0) > 0 AND CAST(TTLInner.dtdate AS DATE)  = CAST(@dtFromDate AS DATE) GROUP BY TTLInner.numAssignTo,TTLInner.numTaskID,TTLInner.numQtyItemsReq) T),0)
				,ISNULL((SELECT SUM(T.numQtyItemsReq) FROM (SELECT numQtyItemsReq FROM @TempTimeLog TTLInner WHERE TTLInner.numAssignTo = TTL.numAssignTo AND ISNULL(TTLInner.numProjectID,0) > 0 AND CAST(TTLInner.dtdate AS DATE)  = CAST(@dtFromDate AS DATE) GROUP BY TTLInner.numAssignTo,TTLInner.numTaskID,TTLInner.numQtyItemsReq) T),0)				
			FROM
				@TempTimeLog TTL
			WHERE
				CAST(dtdate AS DATE)  = CAST(@dtFromDate AS DATE)
			GROUP BY
				numAssignTo
				,numProductiveMinutes
			
			SET @dtFromDate = DATEADD(DAY,1,@dtFromDate)
		END

		UPDATE
			@TEMPResult 
		SET 
			vcTotalHours = CONCAT(FORMAT(ISNULL(numTotalMinutes,0) / 60,'00'),':',FORMAT(ISNULL(numTotalMinutes,0) % 60,'00'))
			,numCapacityLoad = (CASE WHEN ISNULL(numProductiveMinutes,0) > 0 THEN (numTotalMinutes * 100) / numProductiveMinutes ELSE 0 END)

		SELECT * FROM @TEMPResult
	END
END
GO
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_WorkScheduleDaysOff_Save')
DROP PROCEDURE dbo.USP_WorkScheduleDaysOff_Save
GO
CREATE PROCEDURE [dbo].[USP_WorkScheduleDaysOff_Save]
(
	@numDomainID NUMERIC(18,0)
	,@numUserCntID NUMERIC(18,0)
	,@numWorkScheduleID NUMERIC(18,0)
	,@dtDayOffFrom Date
	,@dtDayOffTo Date
)
AS 
BEGIN
	IF NOT EXISTS (SELECT ID FROM WorkSchedule WHERE numDomainID=@numDomainID AND ID=ISNULL(@numWorkScheduleID,0))
	BEGIN
		INSERT INTO WorkSchedule
		(
			numDomainID
			,numUserCntID
			,numWorkHours
			,numWorkMinutes
			,numProductiveHours
			,numProductiveMinutes
			,tmStartOfDay
			,vcWorkDays
		)
		VALUES
		(
			@numDomainID
			,@numUserCntID
			,0
			,0
			,0
			,0
			,'00:00:00'
			,''
		)

		SET @numWorkScheduleID = SCOPE_IDENTITY()
	END
	
	INSERT INTO WorkScheduleDaysOff
	(
		numWorkScheduleID
		,dtDayOffFrom
		,dtDayOffTo
	)
	VALUES
	(
		@numWorkScheduleID
		,@dtDayOffFrom
		,@dtDayOffTo
	)
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetPerformanceEvaluationReportData')
DROP PROCEDURE USP_GetPerformanceEvaluationReportData
GO
CREATE PROCEDURE [dbo].[USP_GetPerformanceEvaluationReportData]                             
	@numDomainID AS NUMERIC(18,0)                            
	,@tintProcessType TINYINT
	,@numProcessID NUMERIC(18,0)
	,@vcMilestone VARCHAR(300)
	,@numStageDetailsId NUMERIC(18,0)
	,@vcTaskIDs VARCHAR(200)
	,@vcTeams VARCHAR(200)
	,@vcEmployees VARCHAR(200)
	,@dtFromDate DATETIME
	,@dtToDate DATETIME
	,@ClientTimeZoneOffset INT
AS                            
BEGIN
	DECLARE @TEMP TABLE
	(
		numStageDetailsId NUMERIC(18,0)
		,numTaskID NUMERIC(18,0)
		,vcTaskName VARCHAR(300)
		,numTaskTimeInMinutes NUMERIC(18,0)
	)

	INSERT INTO 
		@TEMP
	SELECT
		SPDT.numStageDetailsId
		,SPDT.numTaskId
		,SPDT.vcTaskName
		,(ISNULL(SPDT.numHours,0) * 60) + ISNULL(SPDT.numMinutes,0)
	FROM
		StagePercentageDetails SPD
	INNER JOIN
		StagePercentageDetailsTask SPDT
	ON
		SPD.numStageDetailsId = SPDT.numStageDetailsId
	WHERE
		SPD.numDomainID=@numDomainID
		AND SPD.slp_id = @numProcessID
		AND (ISNULL(@vcMilestone,'') = '' OR SPD.vcMileStoneName = @vcMilestone)
		AND (ISNULL(@numStageDetailsId,0) = 0 OR SPD.numStageDetailsId=@numStageDetailsId)
		AND (ISNULL(@vcTaskIDs,'') = '' OR SPDT.numTaskId IN (SELECT ID FROM dbo.SplitIDs(@vcTaskIDs,',')))

	DECLARE @TempTasks TABLE
	(
		numRecordID NUMERIC(18,0)
		,numTaskID NUMERIC(18,0)
		,numReferenceTaskId NUMERIC(18,0)
		,fltBuildQty FLOAT
		,numContactId NUMERIC(18,0)
		,numTeam NUMERIC(18,0)
		,numEstimatedTaskMinutes NUMERIC(18,0)
		,numActualTaskMinutes NUMERIC(18,0)
		,monEstimatedCost DECIMAL(20,5)
		,monActualCost DECIMAL(20,5)
	)

	INSERT INTO @TempTasks
	(
		numRecordID
		,numTaskID
		,numReferenceTaskId
		,fltBuildQty
		,numContactId
		,numTeam
		,numEstimatedTaskMinutes
		,numActualTaskMinutes
		,monEstimatedCost
		,monActualCost
	)
	SELECT
		(CASE WHEN @tintProcessType=3 THEN WO.numWOId ELSE PM.numProId END)
		,SPDT.numTaskID
		,SPDT.numReferenceTaskId
		,(CASE WHEN @tintProcessType=3 THEN ISNULL(WO.numQtyItemsReq,0) ELSE 1 END) numBuildQty
		,ACI.numContactId
		,ACI.numTeam
		,(((ISNULL(SPDT.numHours,0) * 60) + ISNULL(SPDT.numMinutes,0)) * (CASE WHEN @tintProcessType=3 THEN ISNULL(WO.numQtyItemsReq,0) ELSE 1 END)) numEstimatedTaskMinutes
		,ISNULL(TEMPTime.numTotalMinutes,0) numActualTaskMinutes
		,(((ISNULL(SPDT.numHours,0) * 60) + ISNULL(SPDT.numMinutes,0)) * (CASE WHEN @tintProcessType=3 THEN ISNULL(WO.numQtyItemsReq,0) ELSE 1 END)) * (ISNULL(SPDT.monHourlyRate,ISNULL(UM.monHourlyRate,0)) / 60) monEstimatedCost
		,ISNULL(TEMPTime.numTotalMinutes,0) * (ISNULL(SPDT.monHourlyRate,ISNULL(UM.monHourlyRate,0)) / 60) monActualCost
	FROM
		@TEMP T
	INNER JOIN
		StagePercentageDetailsTask SPDT
	ON
		T.numTaskID = SPDT.numReferenceTaskId
	INNER JOIN
		UserMaster UM
	ON
		SPDT.numAssignTo = UM.numUserDetailId
	INNER JOIN
		AdditionalContactsInformation ACI
	ON
		UM.numUserDetailId = ACI.numContactId
	LEFT JOIN
		WorkOrder WO
	ON
		SPDT.numWorkOrderId = WO.numWOId
	LEFT JOIN
		ProjectsMaster PM
	ON
		SPDT.numProjectId = PM.numProId
	CROSS APPLY
	(
		SELECT
			SUM(T.numTotalMinutes) numTotalMinutes
		FROM
		(
			SELECT 
				DATEDIFF(MINUTE,MIN(dtActionTime),MAX(dtActionTime)) AS numTotalMinutes
			FROM 
				StagePercentageDetailsTaskTimeLog SPDTTL
			WHERE 
				SPDTTL.numTaskID=SPDT.numTaskId
				AND dtActionTime BETWEEN @dtFromDate AND @dtToDate
			GROUP BY 
				CAST(dtActionTime AS DATE)
		) T
	) TEMPTime
	WHERE
		(ISNULL(@vcTeams,'') = '' OR ACI.numTeam IN (SELECT ID FROM dbo.SplitIDs(@vcTeams,',')))
		AND (ISNULL(@vcEmployees,'') = '' OR ACI.numContactId IN (SELECT ID FROM dbo.SplitIDs(@vcEmployees,',')))
		AND (SELECT COUNT(*) FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID=SPDT.numTaskId AND tintAction = 4) > 0

	DECLARE @TEMPResult TABLE
	(
		numTotalRecords NUMERIC(18,0)
		,numTaskID NUMERIC(18,0)
		,numBuildUnits FLOAT
		,numAssignedTo NUMERIC(18,0)
		,numTeam NUMERIC(18,0)
		,vcEmployee VARCHAR(200)
		,vcTeam VARCHAR(200)
		,numOnTimeOrEarly NUMERIC(18,0)
		,monBudgetImpact DECIMAL(20,5)
		,numAvailability NUMERIC(18,0)
		,numProductivity NUMERIC(18,0)
		,fltUnits FLOAT
		,monLabourPerUnit DECIMAL(20,5)
	)

	INSERT INTO @TEMPResult
	(
		numTotalRecords
		,numTaskID
		,numBuildUnits
		,numAssignedTo
		,numTeam
		,vcEmployee
		,vcTeam
		,numOnTimeOrEarly
		,monBudgetImpact
		,fltUnits
		,monLabourPerUnit
	)
	SELECT
		COUNT(DISTINCT TT.numRecordID)
		,TT.numReferenceTaskId
		,ISNULL((SELECT SUM(fltBuildQty) FROM @TempTasks TTInner WHERE TTInner.numReferenceTaskId = TT.numReferenceTaskId),0)
		,TT.numContactId
		,TT.numTeam
		,dbo.fn_GetContactName(TT.numContactId)
		,dbo.fn_GetListItemName(TT.numTeam)
		,((SELECT 
				COUNT(*) 
			FROM 
				@TempTasks TTInner 
			WHERE 
				TTInner.numReferenceTaskId = TT.numReferenceTaskId 
				AND TTInner.numContactId = TT.numContactId 
				AND TTInner.numTeam=TT.numTeam
				AND numActualTaskMinutes <= numEstimatedTaskMinutes) * 100 / (SELECT 
																					COUNT(*) 
																				FROM 
																					@TempTasks TTInner 
																				WHERE 
																					TTInner.numReferenceTaskId = TT.numReferenceTaskId 
																					AND TTInner.numContactId = TT.numContactId 
																					AND TTInner.numTeam=TT.numTeam))
		,SUM(ISNULL(monActualCost,0) - ISNULL(monEstimatedCost,0))
		,SUM(fltBuildQty)
		,AVG(ISNULL(monActualCost,0)/fltBuildQty)
	FROM
		@TempTasks TT
	GROUP BY
		TT.numReferenceTaskId
		,TT.numContactId
		,TT.numTeam

	SELECT 
		vcMileStoneName
	FROM
		StagePercentageDetails
	WHERE
		numDomainId = @numDomainID
		AND slp_id = @numProcessID
		AND (ISNULL(@vcMilestone,'') = '' OR vcMileStoneName = @vcMilestone)
	GROUP BY
		vcMileStoneName

	SELECT 
		numStageDetailsId
		,vcMileStoneName
		,vcStageName
	FROM
		StagePercentageDetails
	WHERE
		numDomainId = @numDomainID
		AND slp_id = @numProcessID
		AND (ISNULL(@vcMilestone,'') = '' OR vcMileStoneName = @vcMilestone)
		AND (ISNULL(@numStageDetailsId,0) = 0 OR numStageDetailsId=@numStageDetailsId)

	SELECT * FROM @TEMP

	SELECT * FROM @TEMPResult ORDER BY numTaskID,vcEmployee

	SELECT
		ACI.numContactId
		,CONCAT(ACI.vcFirstName,' ',ACI.vcLastName) vcContctName
		,ISNULL(TEMP.numAvailabilityPercent,0) numAvailabilityPercent
		,ISNULL(TEMP.numProductivityPercent,0) numProductivityPercent
	FROM
		UserMaster UM
	INNER JOIN
		AdditionalContactsInformation ACI
	ON
		UM.numUserDetailId = ACI.numContactId
	OUTER APPLY
	(
		SELECT
			numTotalAvailableMinutes
			,numAvailabilityPercent
			,numProductiveMinutes
			,numProductivityPercent
		FROM
			dbo.GetEmployeeAvailabilityPercent(@numDomainID,ACI.numContactId,@dtFromDate,@dtToDate,@ClientTimeZoneOffset) T
	) TEMP
	WHERE
		UM.numDomainID = @numDomainID
		AND ISNULL(UM.bitActivateFlag,0) = 1

	SELECT ISNULL((SELECT SUM(numTotalRecords) FROM @TEMPResult),0) numTotalWorkOrders
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetProductionPlanningReportData')
DROP PROCEDURE USP_GetProductionPlanningReportData
GO
CREATE PROCEDURE [dbo].[USP_GetProductionPlanningReportData]                             
	@numDomainID AS NUMERIC(18,0)                            
	,@tintProcessType TINYINT
	,@dtFromDate DATETIME
	,@dtToDate DATETIME
	,@numPageIndex INT
	,@numPageSize INT
	,@ClientTimeZoneOffset INT
AS                            
BEGIN
	DECLARE @TempData TABLE
	(
		[id] VARCHAR(50)
		,[text] VARCHAR(1000)
		,[start_date] DATETIME
		,[end_date] DATETIME
		,[duration] FLOAT
		,[progress] FLOAT
		,[sortorder] INT
		,[parent] VARCHAR(50)
		,[open] BIT
		,[color] VARCHAR(50)
		,[progressColor] VARCHAR(50)
		,numWOID NUMERIC(18,0)
		,vcMilestone VARCHAR(500)
		,numStageDetailsId NUMERIC(18,0)
		,numTaskID NUMERIC(18,0)
		,vcItem VARCHAR(300)
		,dtRequestedFinish VARCHAR(20)
		,dtProjectedFinish VARCHAR(20)
		,dtItemReleaseDate VARCHAR(20)
		,dtActualStartDate VARCHAR(20)
		,vcDurationHours VARCHAR(20)
	)

	DECLARE @TempLinks TABLE
	(
		[id] INT IDENTITY(1,1)
		,[source] VARCHAR(50)
		,[target] VARCHAR(50)
		,[type] INT
	)

	INSERT INTO @TempData
	(
		[id]
		,[text]
		,[start_date]
		,[duration]
		,[progress]
		,[sortorder]
		,[parent]
		,[open]
		,numWOID
		,vcItem
		,dtRequestedFinish
		,dtProjectedFinish
		,dtItemReleaseDate
		,dtActualStartDate
	)
	SELECT TOP 10
		CONCAT('WO',WorkOrder.numWOId)
		,CONCAT(ISNULL(WorkOrder.vcWorkOrderName,'-'),' (',dbo.GetTotalProgress(@numDomainID,WorkOrder.numWOId,1,1,'',0),') '
				,' <a href="javascript:void(0);" type="button" class="btn btn-secondary" data-toggle="tooltip" data-placement="right" data-html="true" title="<table><tr><td style=''white-space:nowrap;text-align:right;padding:5px''>Item (Qty):</td><td style=''white-space:nowrap;text-align:left;''>',CONCAT(Item.vcItemName,' (',ISNULL(WorkOrder.numQtyItemsReq,0),')'),'</td></tr><tr><td style=''white-space:nowrap;text-align:right;padding:5px''>Requested Finish:</td><td style=''white-space:nowrap;text-align:left;''>',dbo.FormatedDateFromDate(DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,WorkOrder.dtmEndDate),@numDomainID),'</td></tr><tr><td style=''white-space:nowrap;text-align:right;padding:5px''>Projected Finish:</td><td style=''white-space:nowrap;text-align:left;''>',dbo.FormatedDateFromDate(dbo.GetProjectedFinish(@numDomainID,WorkOrder.numWOID,@ClientTimeZoneOffset),@numDomainID),'</td></tr><tr><td style=''white-space:nowrap;text-align:right;padding:5px''>Item Release:</td><td style=''white-space:nowrap;text-align:left;''>',dbo.FormatedDateFromDate(OpportunityItems.ItemReleaseDate,@numDomainID),'</td></tr><tr><td style=''white-space:nowrap;text-align:right;padding:5px''>Actual Start:</td><td style=''white-space:nowrap;text-align:left;''>',(CASE 
											WHEN TEMP.dtActualStartDate IS NOT NULL 
											THEN dbo.FormatedDateFromDate(DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,TEMP.dtActualStartDate),@numDomainID) 
											ELSE NULL 
										END),'</td></tr></table>"><i class="fa fa-bars"></i></a>')
		--,CONCAT(ISNULL(WorkOrder.vcWorkOrderName,'-')
		--		,' <a href="javascript:void(0);" type="button" class="btn btn-secondary" data-toggle="tooltip" data-placement="bottom" data-html="true" title="Item Name: ',CONCAT(Item.vcItemName,' (',ISNULL(WorkOrder.numQtyItemsReq,0),')')
		--		,'<br/>Requested Finish: ',dbo.FormatedDateFromDate(DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,WorkOrder.dtmEndDate),@numDomainID)
		--		,'<br/>Projected Finish: ',dbo.FormatedDateFromDate(dbo.GetProjectedFinish(@numDomainID,WorkOrder.numWOID,@ClientTimeZoneOffset),@numDomainID)
		--		,'<br/>Item Release: ',dbo.FormatedDateFromDate(OpportunityItems.ItemReleaseDate,@numDomainID)
		--		,'<br/>Actual Start: ',(CASE 
		--									WHEN TEMP.dtActualStartDate IS NOT NULL 
		--									THEN dbo.FormatedDateFromDate(DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,TEMP.dtActualStartDate),@numDomainID) 
		--									ELSE NULL 
		--								END),'"><i class="fa fa-bars"></i></a>')
		,NULL
		,NULL
		,dbo.GetTotalProgress(@numDomainID,WorkOrder.numWOId,1,1,'',0) / 100.0
		,ROW_NUMBER() OVER(ORDER BY WorkOrder.dtmStartDate)
		,0
		,1
		,WorkOrder.numWOId
		,CONCAT(Item.vcItemName,' (',ISNULL(WorkOrder.numQtyItemsReq,0),')')
		,dbo.FormatedDateFromDate(DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,WorkOrder.dtmEndDate),@numDomainID)
		,dbo.FormatedDateFromDate(dbo.GetProjectedFinish(@numDomainID,WorkOrder.numWOID,@ClientTimeZoneOffset),@numDomainID)
		,dbo.FormatedDateFromDate(OpportunityItems.ItemReleaseDate,@numDomainID)
		,(CASE 
			WHEN TEMP.dtActualStartDate IS NOT NULL 
			THEN dbo.FormatedDateFromDate(DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,TEMP.dtActualStartDate),@numDomainID) 
			ELSE NULL 
		END)		
	FROM 
		WorkOrder
	INNER JOIN
		Item 
	ON
		WorkOrder.numItemCode = Item.numItemCode
	LEFT JOIN
		OpportunityItems
	ON
		WorkOrder.numOppItemID = OpportunityItems.numoppitemtCode
	OUTER APPLY
	(
		SELECT 
			MIN(SPDTTL.dtActionTime) dtActualStartDate
		FROM 
			StagePercentageDetailsTask SPDT
		INNER JOIN
			StagePercentageDetailsTaskTimeLog SPDTTL 
		ON
			SPDT.numTaskId = SPDTTL.numTaskID
		WHERE 
			SPDT.numWorkOrderId=WorkOrder.numWOId
	) TEMP
	WHERE
		WorkOrder.numDomainID = @numDomainID
		AND WorkOrder.numWOStatus <> 23184
		AND (CAST(ISNULL(WorkOrder.dtmStartDate,WorkOrder.bintCreatedDate) AS DATE) BETWEEN CAST(@dtFromDate AS DATE) AND CAST(@dtToDate AS DATE) OR EXISTS (SELECT * FROM StagePercentageDetailsTask SPDT WHERE SPDT.numWorkOrderId=WorkOrder.numWOId AND (SELECT COUNT(*) FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID=SPDT.numTaskId) > 0))
		AND EXISTS (SELECT numTaskId FROM StagePercentageDetailsTask SPDT WHERE SPDT.numWorkOrderId=WorkOrder.numWOId)
	ORDER BY
		WorkOrder.dtmStartDate

	INSERT INTO @TempData
	(
		[id]
		,[text]
		,[start_date]
		,[duration]
		,[progress]
		,[sortorder]
		,[parent]
		,[open]
		,numWOID
		,vcMilestone
	)
	SELECT
		CONCAT('MS',ROW_NUMBER() OVER(ORDER BY vcMileStoneName))
		,vcMileStoneName
		,NULL
		,NULL
		,dbo.GetTotalProgress(@numDomainID,TD.numWOID,1,2,vcMileStoneName,0) / 100.0
		,ROW_NUMBER() OVER(ORDER BY vcMileStoneName)
		,TD.[id]
		,1
		,numWOID
		,vcMileStoneName
	FROM
		StagePercentageDetails SPD
	INNER JOIN
		@TempData TD
	ON
		SPD.numWorkOrderId = TD.numWOID
	GROUP BY
		vcMileStoneName
		,TD.[id]
		,TD.numWOID

	INSERT INTO @TempData
	(
		[id]
		,[text]
		,[start_date]
		,[duration]
		,[progress]
		,[sortorder]
		,[parent]
		,[open]
		,numStageDetailsId
	)
	SELECT
		CONCAT('S',SPD.numStageDetailsId)
		,SPD.vcStageName
		,NULL
		,NULL
		,dbo.GetTotalProgress(@numDomainID,TD.numWOID,1,3,'',SPD.numStageDetailsId) / 100.0
		,ROW_NUMBER() OVER(ORDER BY SPD.numStageDetailsId)
		,TD.[id]
		,1
		,SPD.numStageDetailsId
	FROM
		StagePercentageDetails SPD
	INNER JOIN
		@TempData TD
	ON
		SPD.numWorkOrderId = TD.numWOID
		AND SPD.vcMileStoneName = TD.vcMilestone

	INSERT INTO @TempData
	(
		[id]
		,[text]
		,[start_date]
		,[end_date]
		,[duration]
		,[progress]
		,[sortorder]
		,[parent]
		,[open]
		,[color]
		,[progressColor]
		,numTaskID
		,vcDurationHours
	)
	SELECT
		CONCAT((CASE 
					WHEN EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID = SPDT.numTaskId AND tintAction=4) 
					THEN 'TC'
					WHEN (SELECT COUNT(*) FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID = SPDT.numTaskId) > 0
					THEN 'TS'
					ELSE 'T'
				END),SPDT.numTaskId)
		,SPDT.vcTaskName
		,(CASE 
			WHEN EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID = SPDT.numTaskId AND tintAction=1) AND EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID = SPDT.numTaskId AND tintAction=4)
			THEN (SELECT DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,MIN(dtActionTime)) FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID = SPDT.numTaskId AND tintAction=1)
			ELSE DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,SPDT.dtStartTime)
		END)
		,(CASE 
			WHEN EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID = SPDT.numTaskId AND tintAction=1) AND EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID = SPDT.numTaskId AND tintAction=4) 
			THEN (SELECT DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,MAX(dtActionTime)) FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID = SPDT.numTaskId AND tintAction=4)
			ELSE DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,SPDT.dtEndTime)
		END)
		,((ISNULL(numHours,0) * 60) + ISNULL(numMinutes,0)) * ISNULL(WorkOrder.numQtyItemsReq,0)
		,(CASE 
			WHEN EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID = SPDT.numTaskId AND tintAction=4) 
			THEN 100 / 100
			ELSE 0
		END)
		,ROW_NUMBER() OVER(ORDER BY SPDT.numTaskId)
		,TD.[id]
		,1
		,(CASE 
			WHEN EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID = SPDT.numTaskId AND tintAction=4) 
			THEN '#2ecc71'
			WHEN EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID = SPDT.numTaskId AND tintAction=1)
			THEN '#fd7622'
			ELSE '#3498db' 
		END)
		,(CASE 
			WHEN EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID = SPDT.numTaskId AND tintAction=4) 
			THEN '#02ce58'
			WHEN EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID = SPDT.numTaskId AND tintAction=1)
			THEN '#ff6200'
			ELSE '#0286de' 
		END)
		,SPDT.numTaskId
		,FORMAT(FLOOR((((ISNULL(SPDT.numHours,0) * 60) + ISNULL(SPDT.numMinutes,0)) * ISNULL(WorkOrder.numQtyItemsReq,0)) / 60),'00') + ':' + FORMAT((((ISNULL(SPDT.numHours,0) * 60) + ISNULL(SPDT.numMinutes,0)) * CAST(ISNULL(WorkOrder.numQtyItemsReq,0) AS DECIMAL)) % 60,'00')
	FROM
		StagePercentageDetailsTask SPDT
	INNER JOIN
		WorkOrder
	ON
		SPDT.numWorkOrderId = WorkOrder.numWOId
	INNER JOIN
		@TempData TD
	ON
		SPDT.numStageDetailsId = TD.numStageDetailsId


	DECLARE @TempTaskAssignee TABLE
	(
		numWorkOrderID NUMERIC(18,0)
		,numAssignedTo NUMERIC(18,0)		
		,numLastTaskID NUMERIC(18,0)	
		,dtLastTaskCompletionTime DATETIME
	)

	DECLARE @TempTimeLog TABLE
	(
		numWrokOrder NUMERIC(18,0)
		,numTaskID NUMERIC(18,0)
		,numQtyItemsReq FLOAT
		,numAssignTo NUMERIC(18,0)
		,numProductiveMinutes NUMERIC(18,0)
		,dtDate DATETIME
		,numTotalMinutes NUMERIC(18,0)
	)

	DECLARE @TempTasks TABLE
	(
		ID INT IDENTITY(1,1)
		,numTaskID NUMERIC(18,0)
		,numWorkOrderID NUMERIC(18,0)
		,numAssignTo NUMERIC(18,0)
		,numTaskTimeInMinutes NUMERIC(18,0)
		,intTaskType INT
		,dtStartDate DATETIME
		,dtFinishDate DATETIME
		,numQtyItemsReq FLOAT
		,numProcessedQty FLOAT
		,bitTaskStarted BIT
	)

	INSERT INTO @TempTasks
	(
		numTaskID
		,numWorkOrderID
		,numAssignTo
		,numTaskTimeInMinutes
		,intTaskType
		,dtStartDate
		,numQtyItemsReq
		,numProcessedQty
		,bitTaskStarted
	)
	SELECT
		SPDT.numTaskId
		,WorkOrder.numWOId
		,SPDT.numAssignTo
		,((ISNULL(SPDT.numHours,0) * 60) + ISNULL(SPDT.numMinutes,0)) * (ISNULL(numQtyItemsReq,0) - ISNULL((SELECT SUM(SPDTTL.numProcessedQty) FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskId = SPDT.numTaskId),0))
		,SPDT.intTaskType
		,(CASE	
			WHEN (SELECT COUNT(*) FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID = SPDT.numTaskId AND tintAction=1) > 0
			THEN (SELECT MIN(dtActionTime) FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID = SPDT.numTaskId AND tintAction=1)
			ELSE ISNULL(WorkOrder.dtmStartDate,WorkOrder.bintCreatedDate)
		END)
		,WorkOrder.numQtyItemsReq
		,ISNULL((SELECT SUM(numProcessedQty) FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID = SPDT.numTaskId),0)
		,(CASE	
			WHEN (SELECT COUNT(*) FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID = SPDT.numTaskId) > 0
			THEN 1
			ELSE 0
		END)
	FROM
		@TempData TD
	INNER JOIN
		StagePercentageDetailsTask SPDT
	ON
		TD.numTaskID = SPDT.numTaskId
	INNER JOIN
		WorkOrder
	ON
		SPDT.numWorkOrderId = WorkOrder.numWOId
	WHERE
		TD.numTaskID > 0 
		AND (SELECT COUNT(*) FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID = SPDT.numTaskId AND tintAction=4) = 0 -- Task in not finished
	
	INSERT INTO @TempTaskAssignee
	(
		numWorkOrderID
		,numAssignedTo
	)
	SELECT
		numWorkOrderID
		,numAssignTo
	FROM
		@TempTasks
	GROUP BY
		numWorkOrderID
		,numAssignTo

	DECLARE @i INT = 1
	DECLARE @iCount INT 	
	SELECT @iCount = COUNT(*) FROM @TempTasks

	DECLARE @numTempTaskID NUMERIC(18,0)
	DECLARE @numTaskAssignee NUMERIC(18,0)
	DECLARE @intTaskType INT
	DECLARE @numTotalTaskInMinutes NUMERIC(18,0)
	DECLARE @numWorkOrderID NUMERIC(18,0)
	DECLARE @numQtyItemsReq FLOAT

	DECLARE @numWorkScheduleID NUMERIC(18,0)
	DECLARE @numProductiveTimeInMinutes NUMERIC(18,0)
	DECLARE @tmStartOfDay TIME(7)
	DECLARE @numTimeLeftForDay NUMERIC(18,0)
	DECLARE @vcWorkDays VARCHAR(20)
	DECLARE @dtStartDate DATETIME
	DECLARE @bitTaskStarted BIT
		
	WHILE @i <= @iCount
	BEGIN
		SELECT
			@numTempTaskID=numTaskID
			,@numTaskAssignee=numAssignTo
			,@numTotalTaskInMinutes=numTaskTimeInMinutes
			,@intTaskType = intTaskType
			,@dtStartDate=dtStartDate
			,@numWorkOrderID=numWorkOrderID
			,@numQtyItemsReq=numQtyItemsReq
			,@bitTaskStarted = bitTaskStarted
		FROM
			@TempTasks 
		WHERE
			ID=@i

		-- GET TASK COMPLETION TIME WHICH WILL BE START DATETIME FOR NEXT TASK
		SELECT
			@numWorkScheduleID = WS.ID
			,@numProductiveTimeInMinutes=(ISNULL(WS.numProductiveHours,0) * 60) + ISNULL(WS.numProductiveMinutes,0)
			,@tmStartOfDay = tmStartOfDay
			,@vcWorkDays=vcWorkDays
			,@dtStartDate = (CASE 
								WHEN @bitTaskStarted = 1 
								THEN @dtStartDate
								ELSE DATEADD(MINUTE,@ClientTimeZoneOffset,CONVERT(DATETIME,CONVERT(VARCHAR(8),DATEADD(MINUTE,-@ClientTimeZoneOffset,@dtStartDate),112)) + tmStartOfDay)
							END)
		FROM
			WorkSchedule WS
		INNER JOIN
			UserMaster
		ON
			WS.numUserCntID = UserMaster.numUserDetailId
		WHERE 
			WS.numUserCntID = @numTaskAssignee

		IF EXISTS (SELECT numAssignedTo FROM @TempTaskAssignee WHERE numWorkOrderID=@numWorkOrderID AND numAssignedTo = @numTaskAssignee AND dtLastTaskCompletionTime IS NOT NULL)
		BEGIN
			INSERT INTO @TempLinks
			(
				[source]
				,[target]
				,[type]
			)
			VALUES
			(
				CONCAT((CASE WHEN @bitTaskStarted=1 THEN 'TS' ELSE 'T' END),(SELECT numLastTaskID FROM @TempTaskAssignee WHERE numWorkOrderID=@numWorkOrderID AND numAssignedTo = @numTaskAssignee))
				,CONCAT((CASE WHEN @bitTaskStarted=1 THEN 'TS' ELSE 'T' END),@numTempTaskID)
				,0
			)

			SELECT @dtStartDate = dtLastTaskCompletionTime FROM @TempTaskAssignee WHERE numWorkOrderID=@numWorkOrderID AND numAssignedTo = @numTaskAssignee
		END
		ELSE
		BEGIN
			INSERT INTO @TempLinks
			(
				[source]
				,[target]
				,[type]
			)
			VALUES
			(
				CONCAT('WO',@numWorkOrderID)
				,CONCAT((CASE WHEN @bitTaskStarted=1 THEN 'TS' ELSE 'T' END),@numTempTaskID)
				,1
			)
		END

		UPDATE @TempTasks SET dtStartDate=@dtStartDate WHERE ID=@i 

		IF @numProductiveTimeInMinutes > 0 AND @numTotalTaskInMinutes > 0
		BEGIN
			WHILE @numTotalTaskInMinutes > 0
			BEGIN
				-- IF ITS WORKING DAY AND ALSO NOT HOLIDAY THEN PROCEED OR INCRESE DATE TO NEXT DAY
				IF DATEPART(WEEKDAY,@dtStartDate) IN (SELECT Id FROM dbo.SplitIDs(@vcWorkDays,',')) AND NOT EXISTS (SELECT ID FROM WorkScheduleDaysOff WHERE numWorkScheduleID=@numWorkScheduleID AND CAST(DATEADD(MINUTE,@ClientTimeZoneOffset * -1,@dtStartDate) AS DATE) BETWEEN dtDayOffFrom AND dtDayOffTo)
				BEGIN
					-- CHECK TIME LEFT FOR DAY BASED
					SET @numTimeLeftForDay = DATEDIFF(MINUTE,@dtStartDate,DATEADD(MINUTE,@numProductiveTimeInMinutes,DATEADD(MINUTE,@ClientTimeZoneOffset,CONVERT(DATETIME,CONVERT(VARCHAR(8),DATEADD(MINUTE,-@ClientTimeZoneOffset,@dtStartDate),112)) + @tmStartOfDay)))

					IF @numTimeLeftForDay > 0
					BEGIN
						INSERT INTO @TempTimeLog
						(
							numWrokOrder
							,numTaskID
							,numQtyItemsReq
							,numAssignTo
							,numProductiveMinutes
							,dtDate
							,numTotalMinutes
						) 
						VALUES 
						(	
							@numWorkOrderID
							,@numTempTaskID							
							,@numQtyItemsReq
							,@numTaskAssignee
							,@numProductiveTimeInMinutes
							,@dtStartDate
							,(CASE WHEN @numTimeLeftForDay > @numTotalTaskInMinutes THEN @numTotalTaskInMinutes ELSE @numTimeLeftForDay END)
						)

						IF @numTimeLeftForDay > @numTotalTaskInMinutes
						BEGIN
							SET @dtStartDate = DATEADD(MINUTE,@numTotalTaskInMinutes,@dtStartDate)
						END
						ELSE
						BEGIN
							SET @dtStartDate = DATEADD(MINUTE,@ClientTimeZoneOffset,CONVERT(DATETIME,CONVERT(VARCHAR(8),DATEADD(MINUTE,-@ClientTimeZoneOffset,DATEADD(DAY,1,@dtStartDate)),112)) + @tmStartOfDay)
						END
							
						SET @numTotalTaskInMinutes = @numTotalTaskInMinutes - @numTimeLeftForDay
					END
					ELSE
					BEGIN
						SET @dtStartDate = DATEADD(MINUTE,@ClientTimeZoneOffset,CONVERT(DATETIME,CONVERT(VARCHAR(8),DATEADD(MINUTE,-@ClientTimeZoneOffset,DATEADD(DAY,1,@dtStartDate)),112)) + @tmStartOfDay)
					END
				END
				ELSE
				BEGIN
					SET @dtStartDate = DATEADD(MINUTE,@ClientTimeZoneOffset,CONVERT(DATETIME,CONVERT(VARCHAR(8),DATEADD(MINUTE,-@ClientTimeZoneOffset,DATEADD(DAY,1,@dtStartDate)),112)) + @tmStartOfDay)					
				END				
			END
		END	

		UPDATE @TempTasks SET dtFinishDate=@dtStartDate WHERE ID=@i 
		UPDATE @TempTaskAssignee SET numLastTaskID=@numTempTaskID, dtLastTaskCompletionTime=@dtStartDate WHERE numWorkOrderID=@numWorkOrderID AND numAssignedTo=@numTaskAssignee

		SET @i = @i + 1
	END

	UPDATE
		TD
	SET
		TD.[start_date] = (CASE WHEN TD.[start_date] IS NULL THEN DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,TT.dtStartDate) ELSE TD.[start_date] END)
		,TD.[end_date] = (CASE WHEN TD.[end_date] IS NULL THEN DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,TT.dtFinishDate) ELSE TD.[end_date] END)
		,TD.progress = (CASE WHEN ISNULL(TT.numQtyItemsReq,0) > 0 THEN (ISNULL(TT.numProcessedQty,0) * 100) / TT.numQtyItemsReq ELSE 0 END)
	FROM
		@TempData TD
	INNER JOIN
		@TempTasks TT
	ON
		TD.numTaskID = TT.numTaskID


	UPDATE
		@TempData
	SET
		[color] = (CASE 
						WHEN (dtItemReleaseDate IS NOT NULL AND dtProjectedFinish > dtItemReleaseDate) OR (dtRequestedFinish IS NOT NULL AND dtProjectedFinish > dtRequestedFinish)
						THEN '#f65a5b'
						ELSE '#2ecc71'
					END),
		[progressColor] = (CASE 
								WHEN (dtItemReleaseDate IS NOT NULL AND dtProjectedFinish > dtItemReleaseDate) OR (dtRequestedFinish IS NOT NULL AND dtProjectedFinish > dtRequestedFinish)
								THEN '#f91e1f'
								ELSE '#02ce58'
							END)
	WHERE
		numWOID > 0
		AND [parent] = '0'

	SELECT * FROM @TempData

	SELECT * FROM @TempLinks
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_StagePercentageDetailsTask_ChangeTimeProductionPlanning')
DROP PROCEDURE USP_StagePercentageDetailsTask_ChangeTimeProductionPlanning
GO
CREATE PROCEDURE [dbo].[USP_StagePercentageDetailsTask_ChangeTimeProductionPlanning]                             
	@numDomainID NUMERIC(18,0)                            
	,@numTaskID NUMERIC(18,0)
	,@dtStartTime DATETIME
	,@dtEndTime DATETIME
AS                            
BEGIN
	IF DATEDIFF(MINUTE,@dtStartTime,@dtEndTime) > 0
	BEGIN
		UPDATE
			StagePercentageDetailsTask
		SET
			dtStartTime=@dtStartTime
			,dtEndTime=@dtEndTime
			,numHours = FLOOR(DATEDIFF(MINUTE,@dtStartTime,@dtEndTime) / 60)
			,numMinutes = DATEDIFF(MINUTE,@dtStartTime,@dtEndTime) % 60
		WHERE
			numDomainID = @numDomainID
			AND numTaskId = @numTaskID
	END
	ELSE
	BEGIN
		RAISERROR('INVALID_START_AND_END_TIME',16,1)
	END
END
GO


GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_WorkScheduleDaysOff_Delete')
DROP PROCEDURE dbo.USP_WorkScheduleDaysOff_Delete
GO
CREATE PROCEDURE [dbo].[USP_WorkScheduleDaysOff_Delete]
(
	@numWorkScheduleID NUMERIC(18,0)
)
AS 
BEGIN
	DELETE FROM WorkScheduleDaysOff WHERE numWorkScheduleID=@numWorkScheduleID
END
GO
GO
IF EXISTS (SELECT * FROM sysobjects WHERE xtype = 'p' AND NAME = 'USP_CompanyInfo_SearchByName')
    DROP PROCEDURE USP_CompanyInfo_SearchByName
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Sandeep Patel
-- Create date: 11 DEC 2019
-- Description:	Search customer
-- =============================================
CREATE PROCEDURE USP_CompanyInfo_SearchByName
    @numDomainID AS NUMERIC(18,0),
    @str AS VARCHAR(1000),
    @numPageIndex AS INT,
    @numPageSize AS INT,
	@numTotalRecords INT OUTPUT
AS
BEGIN
	SELECT 
		@numTotalRecords = COUNT(*) 
	FROM 
		CompanyInfo
	INNER JOIN
		DivisionMaster 
	ON
		CompanyInfo.numCompanyId=DivisionMaster.numCompanyID
	WHERE
		CompanyInfo.numDomainID = @numDomainID
		AND DivisionMaster.numDomainID = @numDomainID
		AND vcCompanyName LIKE CONCAT('%',@str,'%')

	SELECT
		numDivisionID AS id
		,vcCompanyName AS [text]
	FROM
		CompanyInfo
	INNER JOIN
		DivisionMaster 
	ON
		CompanyInfo.numCompanyId=DivisionMaster.numCompanyID
	WHERE
		CompanyInfo.numDomainID = @numDomainID
		AND DivisionMaster.numDomainID = @numDomainID
		AND vcCompanyName LIKE CONCAT('%',@str,'%')
	ORDER BY
		vcCompanyName
	OFFSET 
		(@numPageIndex * @numPageSize) ROWS 
	FETCH NEXT @numPageSize ROWS ONLY
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_UserMaster_GetFieldValue')
DROP PROCEDURE USP_UserMaster_GetFieldValue
GO
CREATE PROCEDURE [dbo].[USP_UserMaster_GetFieldValue]
(
	@numDomainID NUMERIC(18,0)
	,@numUserCntID NUMERIC(18,0)
	,@vcFieldName VARCHAR(200)
)
AS
BEGIN
	IF @vcFieldName = 'tintPayrollType'
	BEGIN
		SELECT ISNULL(tintPayrollType,1) FROM UserMaster	WHERE numDomainID = @numDomainID AND numUserDetailId = @numUserCntID
	END
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_TimeAndExpense_GetTimeEntries')
DROP PROCEDURE USP_TimeAndExpense_GetTimeEntries
GO
CREATE PROCEDURE [dbo].[USP_TimeAndExpense_GetTimeEntries]
(
	@numDomainID NUMERIC(18,0)
	,@dtFromDate DATE
	,@dtToDate DATE
	,@vcEmployeeName VARCHAR(200)
	,@vcTeams VARCHAR(MAX)
	,@tintPayrollType TINYINT
	,@ClientTimeZoneOffset INT
	,@vcSortColumn VARCHAR(200)
	,@vcSortOrder VARCHAR(4)
)
AS
BEGIN
	DECLARE @TEMP TABLE
	(
		numUserCntID NUMERIC(18,0)
		,vcEmployee VARCHAR(200)
		,vcTeam VARCHAR(200)
		,numTotalHours VARCHAR(20)
		,vcPayrollType VARCHAR(50)
	)

	INSERT INTO @TEMP
	(
		numUserCntID
		,vcEmployee
		,vcTeam
		,numTotalHours
		,vcPayrollType
	)
	SELECT
		UserMaster.numUserDetailId
		,CONCAT(ISNULL(AdditionalContactsInformation.vcFirstName,'-'),' ',ISNULL(AdditionalContactsInformation.vcLastName,'-'))
		,dbo.GetListIemName(AdditionalContactsInformation.numTeam)
		,TEMPTime.vcHours
		,(CASE ISNULL(tintPayrollType,0) WHEN 2 THEN 'Salary' WHEN 1 THEN 'Hourly' ELSE '' END) 
	FROM
		UserMaster
	INNER JOIN
		AdditionalContactsInformation
	ON
		UserMaster.numUserDetailId = AdditionalContactsInformation.numContactId
	OUTER APPLY
	(
		SELECT 
			CONCAT(FORMAT(ISNULL(SUM(numMinutes),0) / 60,'00'),':',FORMAT(ISNULL(SUM(numMinutes),0) % 60,'00')) vcHours
		FROM
			dbo.fn_GetPayrollEmployeeTime(@numDomainID,UserMaster.numUserDetailId,@dtFromDate,@dtToDate,@ClientTimeZoneOffset)
		WHERE
			numType NOT IN (4)
	) TEMPTime
	WHERE
		UserMaster.numDomainID=@numDomainID
		AND ISNULL(UserMaster.bitActivateFlag,0) = 1
		AND (ISNULL(@vcEmployeeName,'') = '' OR AdditionalContactsInformation.vcFirstName LIKE CONCAT('%',@vcEmployeeName,'%') OR AdditionalContactsInformation.vcLastName LIKE CONCAT('%',@vcEmployeeName,'%'))
		AND (ISNULL(@vcTeams,'') = '' OR AdditionalContactsInformation.numTeam IN (SELECT Id FROM dbo.SplitIDs(@vcTeams,',')))
		AND 1 = (CASE WHEN @tintPayrollType=0 THEN 1 ELSE (CASE WHEN ISNULL(UserMaster.tintPayrollType,0)=@tintPayrollType THEN 1 ELSE 0 END) END)

	IF ISNULL(@vcSortColumn,'') = ''
	BEGIN
		SET @vcSortColumn = 'vcEmployee'
		SET @vcSortOrder = 'ASC'
	END

	IF ISNULL(@vcSortColumn,'') = ''
	BEGIN
		SET @vcSortOrder = 'ASC'
	END


	SELECT 
		* 
	FROM 
		@TEMP 
	ORDER BY
		CASE WHEN @vcSortColumn = 'vcEmployee' AND @vcSortOrder = 'ASC' then vcEmployee END ASC,
		CASE WHEN @vcSortColumn = 'vcEmployee' AND @vcSortOrder = 'DESC' then vcEmployee END DESC,
		CASE WHEN @vcSortColumn = 'vcTeam' AND @vcSortOrder = 'ASC' then vcTeam END ASC,
		CASE WHEN @vcSortColumn = 'vcTeam' AND @vcSortOrder = 'DESC' then vcTeam END DESC,
		CASE WHEN @vcSortColumn = 'numTotalHours' AND @vcSortOrder = 'ASC' then numTotalHours END ASC,
		CASE WHEN @vcSortColumn = 'numTotalHours' AND @vcSortOrder = 'DESC' then numTotalHours END DESC,
		CASE WHEN @vcSortColumn = 'vcPayrollType' AND @vcSortOrder = 'ASC' then vcPayrollType END ASC,
		CASE WHEN @vcSortColumn = 'vcPayrollType' AND @vcSortOrder = 'DESC' then vcPayrollType END DESC
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_TimeAndExpense_GetTimeEntriesDetail')
DROP PROCEDURE USP_TimeAndExpense_GetTimeEntriesDetail
GO
CREATE PROCEDURE [dbo].[USP_TimeAndExpense_GetTimeEntriesDetail]
(
    @numDomainID NUMERIC(18,0)
	,@numUserCntID INT
	,@dtFromDate DATE
	,@dtToDate DATE
	,@ClientTimeZoneOffset INT
)
AS 
BEGIN
	SELECT 
		*
		,@numUserCntID AS numUserCntID
	FROM
		dbo.fn_GetPayrollEmployeeTime(@numDomainID,@numUserCntID,@dtFromDate,@dtToDate,@ClientTimeZoneOffset)
	ORDER BY
		dtDate
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_CFWLocationAdmin')
DROP PROCEDURE dbo.USP_CFWLocationAdmin
GO


CREATE PROCEDURE [dbo].[USP_CFWLocationAdmin]  
  @numDomainId NUMERIC(18,0)
AS  
 
 
IF (SELECT ISNULL(bitEDI,0) FROM Domain WHERE numDomainId = @numDomainId) = 0

	SELECT Loc_id,Loc_name 
	FROM CFW_Loc_Master
	WHERE Loc_id <> 19 AND Loc_id <> 20 AND Loc_id <> 21 AND Loc_id NOT IN(12
,13
,14
,15
,16
,17
,18
,19
,20
,21)

ELSE

	SELECT Loc_id,Loc_name 
	FROM CFW_Loc_Master
	WHERE Loc_id <> 21 AND Loc_id NOT IN(12
,13
,14
,15
,16
,17
,18
,19
,20
,21)


GO


