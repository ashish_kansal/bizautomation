/******************************************************************
Project: Release 10.2 Date: 03.SEP.2018
Comments: STORE PROCEDURES
*******************************************************************/


/****** Object:  StoredProcedure [dbo].[USP_AdvancedSearch]    Script Date: 05/07/2009 22:04:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                              
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_advancedsearch')
DROP PROCEDURE usp_advancedsearch
GO
CREATE PROCEDURE [dbo].[USP_AdvancedSearch] 
@WhereCondition as varchar(4000)='',                            
@AreasofInt as varchar(100)='',                              
@tintUserRightType as tinyint,                              
@numDomainID as numeric(9)=0,                              
@numUserCntID as numeric(9)=0,                                                        
@CurrentPage int,                                                                    
@PageSize int,                                                                    
@TotRecs int output,                                                                                                             
@columnSortOrder as Varchar(10),                              
@ColumnSearch as varchar(100)='',                           
@ColumnName as varchar(20)='',                             
@GetAll as bit,                              
@SortCharacter as char(1),                        
@SortColumnName as varchar(20)='',        
@bitMassUpdate as bit,        
@LookTable as varchar(200)='' ,        
@strMassUpdate as varchar(2000)='',
@vcRegularSearchCriteria varchar(MAX)='',
@vcCustomSearchCriteria varchar(MAX)='',
@ClientTimeZoneOffset as int,
@vcDisplayColumns VARCHAR(MAX)
as                              
BEGIN
	DECLARE  @tintOrder  AS TINYINT
DECLARE  @vcFormFieldName  AS VARCHAR(50)
DECLARE  @vcListItemType  AS VARCHAR(3)
DECLARE  @vcListItemType1  AS VARCHAR(3)
DECLARE  @vcAssociatedControlType VARCHAR(20)
DECLARE  @numListID  AS NUMERIC(9)
DECLARE  @vcDbColumnName VARCHAR(50)
DECLARE  @vcLookBackTableName VARCHAR(50)
DECLARE  @WCondition VARCHAR(1000)
DECLARE @bitContactAddressJoinAdded AS BIT;
DECLARE @bitBillAddressJoinAdded AS BIT;
DECLARE @bitShipAddressJoinAdded AS BIT;
DECLARE @vcOrigDbColumnName as varchar(50)
DECLARE @bitAllowSorting AS CHAR(1)
DECLARE @bitAllowEdit AS CHAR(1)
DECLARE @bitCustomField AS BIT;
DECLARE @ListRelID AS NUMERIC(9)
DECLARE @intColumnWidth INT
DECLARE @bitAllowFiltering AS BIT;
DECLARE @vcfieldatatype CHAR(1)

Declare @zipCodeSearch as varchar(100);SET @zipCodeSearch=''


if CHARINDEX('$SZ$', @WhereCondition)>0
BEGIN
select @zipCodeSearch=SUBSTRING(@WhereCondition, CHARINDEX('$SZ$', @WhereCondition)+4,CHARINDEX('$EZ$', @WhereCondition) - CHARINDEX('$SZ$', @WhereCondition)-4) 

CREATE TABLE #tempAddress(
	[numAddressID] [numeric](18, 0) NOT NULL,
	[vcAddressName] [varchar](50) NULL,
	[vcStreet] [varchar](100) NULL,
	[vcCity] [varchar](50) NULL,
	[vcPostalCode] [varchar](15) NULL,
	[numState] [numeric](18, 0) NULL,
	[numCountry] [numeric](18, 0) NULL,
	[bitIsPrimary] [bit] NOT NULL CONSTRAINT [DF_AddressMaster_bitIsPrimary]  DEFAULT ((0)),
	[tintAddressOf] [tinyint] NOT NULL,
	[tintAddressType] [tinyint] NOT NULL CONSTRAINT [DF_AddressMaster_tintAddressType]  DEFAULT ((0)),
	[numRecordID] [numeric](18, 0) NOT NULL,
	[numDomainID] [numeric](18, 0) NOT NULL)

IF Len(@zipCodeSearch)>0
BEGIN
	Declare @Strtemp as nvarchar(500);
	SET @Strtemp='insert into #tempAddress select * from AddressDetails  where numDomainID=@numDomainID1  
					and vcPostalCode in (SELECT REPLICATE(''0'', 5 - LEN(ZipCode)) + ZipCode  FROM ' + @zipCodeSearch +')';
	
	EXEC sp_executesql @Strtemp,N'@numDomainID1 numeric(18)',@numDomainID1=@numDomainID

set @WhereCondition=replace(@WhereCondition,'$SZ$' + @zipCodeSearch + '$EZ$',
' (ADC.numContactID in (select  numRecordID from #tempAddress where tintAddressOf=1 AND tintAddressType=0)   
or (DM.numDivisionID in (select  numRecordID from #tempAddress where tintAddressOf=2 AND tintAddressType in(1,2))))')
END
END

SET @WCondition = ''
   
IF (@SortCharacter <> '0' AND @SortCharacter <> '')
  SET @ColumnSearch = @SortCharacter
CREATE TABLE #temptable (
  id           INT   IDENTITY   PRIMARY KEY,
  numcontactid VARCHAR(15))


---------------------Find Duplicate Code Start-----------------------------
DECLARE  @Sql           VARCHAR(8000),
         @numMaxFieldId NUMERIC(9),
         @Where         NVARCHAR(1000),
         @OrderBy       NVARCHAR(1000),
         @GroupBy       NVARCHAR(1000),
         @SelctFields   NVARCHAR(4000),
         @InneJoinOn    NVARCHAR(1000)
SET @InneJoinOn = ''
SET @OrderBy = ''
SET @Sql = ''
SET @Where = ''
SET @GroupBy = ''
SET @SelctFields = ''
    
IF (@WhereCondition = 'DUP' OR @WhereCondition = 'DUP-PRIMARY')
BEGIN
	SELECT @numMaxFieldId= MAX([numFieldID]) FROM View_DynamicColumns
	WHERE [numFormID] =24 AND [numDomainId] = @numDomainID
	--PRINT @numMaxFieldId
	WHILE @numMaxFieldId > 0
	BEGIN
		SELECT @vcFormFieldName=[vcFieldName],@vcDbColumnName=[vcDbColumnName],@vcLookBackTableName=[vcLookBackTableName] 
		FROM View_DynamicColumns 
		WHERE [numFieldID] = @numMaxFieldId and [numFormID] =24 AND [numDomainId] = @numDomainID
		--	PRINT @vcDbColumnName
			SET @SelctFields = @SelctFields +  @vcDbColumnName + ','
			IF(@vcLookBackTableName ='AdditionalContactsInformation')
			BEGIN
				SET @InneJoinOn = @InneJoinOn + 'ADC.'+ @vcDbColumnName +' = a1.' + @vcDbColumnName + ' and '
				SET @GroupBy = @GroupBy + 'ADC.'+  @vcDbColumnName + ','
			END
			ELSE IF (@vcLookBackTableName ='DivisionMaster')
			BEGIN
				SET @InneJoinOn = @InneJoinOn + 'DM.'+ @vcDbColumnName +' = a1.' + @vcDbColumnName + ' and '
				SET @GroupBy = @GroupBy + 'DM.'+  @vcDbColumnName + ','
			END
			ELSE
			BEGIN
				SET @InneJoinOn = @InneJoinOn + 'C.' + @vcDbColumnName +' = a1.' + @vcDbColumnName + ' and '
				SET @GroupBy = @GroupBy + 'C.' + @vcDbColumnName + ','
			END
			SET @Where = @Where + 'and LEN(' + @vcDbColumnName + ')>0 '

	
		SELECT @numMaxFieldId = MAX([numFieldID]) FROM View_DynamicColumns 
		WHERE [numFormID] =24 AND [numDomainId] = @numDomainID AND [numFieldID] < @numMaxFieldId
	END
			
			IF(LEN(@Where)>2)
			BEGIN
				SET @Where =RIGHT(@Where,LEN(@Where)-3)
				SET @OrderBy =  LEFT(@GroupBy,(Len(@GroupBy)- 1))
				SET @GroupBy =  LEFT(@GroupBy,(Len(@GroupBy)- 1))
				SET @InneJoinOn = LEFT(@InneJoinOn,LEN(@InneJoinOn)-3)
				SET @SelctFields = LEFT(@SelctFields,(Len(@SelctFields)- 1))
				SET @Sql = @Sql + ' INSERT INTO [#tempTable] ([numContactID])SELECT  ADC.numContactId FROM additionalcontactsinformation ADC JOIN DivisionMaster DM ON ADC.numDivisionId = DM.numDivisionID JOIN CompanyInfo C ON DM.numCompanyID = C.numCompanyId  
					JOIN (SELECT   '+ @SelctFields + '
				   FROM  additionalcontactsinformation ADC JOIN DivisionMaster DM ON ADC.numDivisionId = DM.numDivisionID JOIN CompanyInfo C ON DM.numCompanyID = C.numCompanyId  
				   WHERE  ' + @Where + ' AND C.[numDomainID] = '+convert(varchar(15),@numDomainID)+ ' AND ADC.[numDomainID] = '+convert(varchar(15),@numDomainID)+ ' AND DM.[numDomainID] = '+convert(varchar(15),@numDomainID)+ 
				   ' GROUP BY ' + @GroupBy + '
				   HAVING   COUNT(* ) > 1) a1
					ON  '+ @InneJoinOn + ' where ADC.[numDomainID] = '+convert(varchar(15),@numDomainID) + ' and DM.[numDomainID] = '+convert(varchar(15),@numDomainID) + ' ORDER BY '+ @OrderBy
			END	
			ELSE
			BEGIN
				SET @Where = ' 1=0 '
				SET @Sql = @Sql + ' INSERT INTO [#tempTable] ([numContactID])SELECT  ADC.numContactId FROM additionalcontactsinformation ADC  '
							+ ' Where ' + @Where
			END 	
		   PRINT @Sql
		   EXEC( @Sql)
		   SET @Sql = ''
		   SET @GroupBy = ''
		   SET @SelctFields = ''
		   SET @Where = ''
	
	IF (@WhereCondition = 'DUP-PRIMARY')
      BEGIN
        SET @WCondition = ' AND ADC.numContactId IN (SELECT DISTINCT numContactId FROM #tempTable) and ISNULL(ADC.bitPrimaryContact,0)=1'
      END
    ELSE
      BEGIN
        SET @WCondition = ' AND ADC.numContactId IN (SELECT DISTINCT numContactId FROM #tempTable)'
      END
    
	SET @Sql = ''
	SET @WhereCondition =''
	SET @Where= 'DUP' 
END
-----------------------------End Find Duplicate ------------------------

declare @strSql as varchar(8000)                                                              
 set  @strSql='Select ADC.numContactId                    
  FROM AdditionalContactsInformation ADC                                                   
  JOIN DivisionMaster DM ON ADC.numDivisionId = DM.numDivisionID                                                  
  JOIN CompanyInfo C ON DM.numCompanyID = C.numCompanyId'   

IF @SortColumnName='vcStreet' OR @SortColumnName='vcCity' OR @SortColumnName='vcPostalCode' OR @SortColumnName ='numCountry' OR @SortColumnName ='numState'
   OR @ColumnName='vcStreet' OR @ColumnName='vcCity' OR @ColumnName='vcPostalCode' OR @ColumnName ='numCountry' OR @ColumnName ='numState'	
BEGIN 
	set @strSql= @strSql +' left Join AddressDetails AD on AD.numRecordId= ADC.numContactID and AD.tintAddressOf=1 and AD.tintAddressType=0 AND AD.bitIsPrimary=1 and AD.numDomainID= ADC.numDomainID'	
END 
ELSE IF @SortColumnName='vcBillStreet' OR @SortColumnName='vcBillCity' OR @SortColumnName='vcBillPostCode' OR @SortColumnName ='numBillCountry' OR @SortColumnName ='numBillState'
     OR @ColumnName='vcBillStreet' OR @ColumnName='vcBillCity' OR @ColumnName='vcBillPostCode' OR @ColumnName ='numBillCountry' OR @ColumnName ='numBillState'
BEGIN 	
	set @strSql= @strSql +' left Join AddressDetails AD1 on AD1.numRecordId= DM.numDivisionID and AD1.tintAddressOf=2 and AD1.tintAddressType=1 AND AD1.bitIsPrimary=1 and AD1.numDomainID= ADC.numDomainID '
END	
ELSE IF @SortColumnName='vcShipStreet' OR @SortColumnName='vcShipCity' OR @SortColumnName='vcShipPostCode' OR @SortColumnName ='numShipCountry' OR @SortColumnName ='numShipState'
     OR @ColumnName='vcShipStreet' OR @ColumnName='vcShipCity' OR @ColumnName='vcShipPostCode' OR @ColumnName ='numShipCountry' OR @ColumnName ='numShipState'
BEGIN
	set @strSql= @strSql +' left Join AddressDetails AD2 on AD2.numRecordId= DM.numDivisionID and AD2.tintAddressOf=2 and AD2.tintAddressType=2 AND AD2.bitIsPrimary=1 and AD2.numDomainID= ADC.numDomainID '
END	
                         
  if (@ColumnName<>'' and  @ColumnSearch<>'')                        
  begin                          
 select @vcListItemType=vcListItemType,@numListID=numListID from View_DynamicDefaultColumns where vcDbColumnName=@ColumnName and numFormID=1 and numDomainId=@numDomainId                         
    if @vcListItemType='LI'                               
    begin                      
		IF @numListID=40--Country
		  BEGIN
			IF @ColumnName ='numCountry'
			BEGIN
					set @strSql= @strSql +' left Join ListDetails L1 on L1.numListItemID=AD.numCountry' 
			END
			ELSE IF @ColumnName ='numBillCountry'
			BEGIN
					set @strSql= @strSql +' left Join ListDetails L1 on L1.numListItemID=AD1.numCountry' 
			END
			ELSE IF @ColumnName ='numShipCountry'
			BEGIN
					set @strSql= @strSql +' left Join ListDetails L1 on L1.numListItemID=AD2.numCountry' 
			END
		  END                        
		  ELSE
		  BEGIN
				set @strSql= @strSql +' left Join ListDetails L1 on L1.numListItemID='+@ColumnName                              
		  END
		            
    end                              
    else if @vcListItemType='S'                               
    begin           
       IF @ColumnName ='numState'
		BEGIN
   			set @strSql= @strSql +' left join State S1 on S1.numStateID=AD.numState'                            
		END
		ELSE IF @ColumnName ='numBillState'
		BEGIN
   			set @strSql= @strSql +' left join State S1 on S1.numStateID=AD1.numState'                            
		END
	    ELSE IF @ColumnName ='numShipState'
	    BEGIN
   			set @strSql= @strSql +' left join State S1 on S1.numStateID=AD2.numState'                           
		END
    end                              
    else if @vcListItemType='T'                               
    begin                              
      set @strSql= @strSql +' left Join ListDetails L1 on L1.numListItemID='+@ColumnName                             
    end                           
   end       
           
                      
   if (@SortColumnName<>'')                        
  begin                          
 select @vcListItemType1=vcListItemType,@numListID=numListID from View_DynamicDefaultColumns where vcDbColumnName=@SortColumnName and numFormID=1 and numDomainId=@numDomainId                         
    if @vcListItemType1='LI'                   
    begin     
    IF @numListID=40--Country
		  BEGIN
			IF @SortColumnName ='numCountry'
			BEGIN
					set @strSql= @strSql +' left Join ListDetails L2 on L2.numListItemID=AD.numCountry' 
			END
			ELSE IF @SortColumnName ='numBillCountry'
			BEGIN
					set @strSql= @strSql +' left Join ListDetails L2 on L2.numListItemID=AD1.numCountry' 
			END
			ELSE IF @SortColumnName ='numShipCountry'
			BEGIN
					set @strSql= @strSql +' left Join ListDetails L2 on L2.numListItemID=AD2.numCountry' 
			END
		  END                        
		  ELSE
		  BEGIN
				set @strSql= @strSql +' left Join ListDetails L2 on L2.numListItemID='+@SortColumnName                              
		  END
    end                              
    else if @vcListItemType1='S'           
    begin                
        IF @SortColumnName ='numState'
		BEGIN
   			set @strSql= @strSql +' left join State S2 on S2.numStateID=AD.numState'                            
		END
		ELSE IF @SortColumnName ='numBillState'
		BEGIN
   			set @strSql= @strSql +' left join State S2 on S2.numStateID=AD1.numState'                            
		END
	    ELSE IF @SortColumnName ='numShipState'
	    BEGIN
   			set @strSql= @strSql +' left join State S2 on S2.numStateID=AD2.numState'                           
		END
    end                              
    else if @vcListItemType1='T'                  
    begin                              
      set @strSql= @strSql +' left Join ListDetails L2 on L2.numListItemID='+@SortColumnName                               
    end                           
  end                          
  set @strSql=@strSql+' where DM.numDomainID  = '+convert(varchar(15),@numDomainID)+   @WhereCondition                                  
                 
--if @tintUserRightType=1 set @strSql=@strSql + ' AND (ADC.numRecOwner = '+convert(varchar(15),@numUserCntID)+ ')'                                       
--else if @tintUserRightType=2 set @strSql=@strSql + ' AND (DM.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+convert(varchar(15),@numUserCntID)+' ) or DM.numTerID=0) '
                        
IF @vcRegularSearchCriteria<>'' set @strSql=@strSql+' and ' + @vcRegularSearchCriteria 
IF @vcCustomSearchCriteria<>'' set @strSql=@strSql+' and ' + @vcCustomSearchCriteria                          
                                            
if    @AreasofInt<>'' set  @strSql=@strSql+ ' and ADC.numContactID in (select numContactID from  AOIContactLink where numAOIID in('+ @AreasofInt+'))'                                 
if (@ColumnName<>'' and  @ColumnSearch<>'')                         
begin                          
  if @vcListItemType='LI'                               
    begin                                
      set @strSql= @strSql +' and L1.vcData like '''+@ColumnSearch +'%'''                             
    end                              
    else if @vcListItemType='S'                               
    begin                                  
      set @strSql= @strSql +' and S1.vcState like '''+@ColumnSearch+'%'''                          
    end                              
    else if @vcListItemType='T'                               
    begin                              
      set @strSql= @strSql +' and T1.vcTerName like '''+@ColumnSearch  +'%'''                            
    end   
    else  
       BEGIN     
		   IF @ColumnName='vcStreet' OR @ColumnName='vcBillStreet' OR @ColumnName='vcShipStreet'
				set @strSql= @strSql +' and vcStreet like '''+@ColumnSearch  +'%'''                            
		   ELSE IF @ColumnName='vcCity' OR @ColumnName='vcBillCity' OR @ColumnName='vcShipCity'
				set @strSql= @strSql +' and vcCity like '''+@ColumnSearch  +'%'''                            
		   ELSE IF @ColumnName='vcPostalCode' OR @ColumnName='vcBillPostCode' OR @ColumnName='vcShipPostCode'
				set @strSql= @strSql +' and vcPostalCode like '''+@ColumnSearch  +'%'''                            
		   else		                      
				set @strSql= @strSql +' and '+@ColumnName+' like '''+@ColumnSearch  +'%'''                            
        END   
                          
end                          
   if (@SortColumnName<>'')                        
  begin                           
    if @vcListItemType1='LI'                               
		begin                                
		  set @strSql= @strSql +' order by L2.vcData '+@columnSortOrder                              
		end                              
    else if @vcListItemType1='S'                               
		begin                                  
		  set @strSql= @strSql +' order by S2.vcState '+@columnSortOrder                              
		end                              
    else if @vcListItemType1='T'                               
		begin                              
		  set @strSql= @strSql +' order by T2.vcTerName '+@columnSortOrder                              
		end  
    else  
       BEGIN     
		   IF @SortColumnName='vcStreet' OR @SortColumnName='vcBillStreet' OR @SortColumnName='vcShipStreet'
				set @strSql= @strSql +' order by vcStreet ' +@columnSortOrder                            
		   ELSE IF @SortColumnName='vcCity' OR @SortColumnName='vcBillCity' OR @SortColumnName='vcShipCity'
				set @strSql= @strSql +' order by vcCity ' +@columnSortOrder                            
		   ELSE IF @SortColumnName='vcPostalCode' OR @SortColumnName='vcBillPostCode' OR @SortColumnName='vcShipPostCode'
				set @strSql= @strSql +' order by vcPostalCode ' +@columnSortOrder
		   else if @SortColumnName='numRecOwner'
				set @strSql= @strSql + ' order by dbo.fn_GetContactName(ADC.numRecOwner) ' +@columnSortOrder	                            
		   else		                      
				set @strSql= @strSql +' order by '+ @SortColumnName +' ' +@columnSortOrder                            
        END  
                                
  END
  ELSE 
	  set @strSql= @strSql +' order by  ADC.numContactID asc '
IF(@Where <>'DUP')
BEGIN
    PRINT @strSql

	INSERT INTO #temptable (numcontactid)
	EXEC( @strSql)
	
	SET @strSql = ''
END

SET @bitBillAddressJoinAdded=0;
SET @bitShipAddressJoinAdded=0;
SET @bitContactAddressJoinAdded=0;
set @tintOrder=0;
set @WhereCondition ='';
set @strSql='select ADC.numContactId,DM.numDivisionID,DM.numTerID,ADC.numRecOwner,DM.tintCRMType '                              
DECLARE @Prefix AS VARCHAR(5)
DECLARE @bitCustom BIT
DECLARE @numFieldGroupID AS INT
DECLARE @vcColumnName AS VARCHAR(200)
DECLARE @numFieldID AS NUMERIC(18,0)

	DECLARE @TEMPSelectedColumns TABLE
	(
		numFieldID NUMERIC(18,0)
		,bitCustomField BIT
		,tintOrder INT
		,intColumnWidth FLOAT
	)
	DECLARE @vcLocationID VARCHAR(100) = '0'
	SELECT @vcLocationID =isnull(vcLocationID,'0') from dynamicFormMaster where numFormID=1    

	-- IF USER HAS SELECTED ITS OWN DISPLAY COLUMNS THEN USE IT
	IF LEN(ISNULL(@vcDisplayColumns,'')) > 0
	BEGIN
		DECLARE @TempIDs TABLE
		(
			ID INT IDENTITY(1,1),
			vcFieldID VARCHAR(300)
		)

		INSERT INTO @TempIDs
		(
			vcFieldID
		)
		SELECT 
			OutParam 
		FROM 
			SplitString(@vcDisplayColumns,',')

		INSERT INTO @TEMPSelectedColumns
		(
			numFieldID,
			bitCustomField,
			tintOrder,
			intColumnWidth
		)
		SELECT
			numFormFieldID
			,bitCustomField
			,(SELECT (ID-1) FROM @TempIDs T3 WHERE T3.vcFieldID = TEMP.vcFieldID)
			,0
		FROM
		(
			SELECT  
				numFieldID as numFormFieldID,
				0 bitCustomField,
				CONCAT(numFieldID,'~0') vcFieldID
			FROM    
				View_DynamicDefaultColumns
			WHERE   
				numFormID = 1
				AND bitInResults = 1
				AND bitDeleted = 0 
				AND numDomainID = @numDomainID
			UNION 
			SELECT  
				c.Fld_id AS numFormFieldID
				,1 bitCustomField
				,CONCAT(Fld_id,'~1') vcFieldID
			FROM    
				CFW_Fld_Master C 
			LEFT JOIN 
				CFW_Validation V ON V.numFieldID = C.Fld_id
			JOIN 
				CFW_Loc_Master L on C.GRP_ID=L.Loc_Id
			WHERE   
				C.numDomainID = @numDomainID
				AND GRP_ID IN (SELECT ID FROM dbo.SplitIDs(@vcLocationID,','))
		) TEMP
		WHERE
			vcFieldID IN (SELECT vcFieldID FROM @TempIDs)
	END

	-- IF USER SELECTED DISPLAY COLUMNS IS NOT AVAILABLE THAN GET COLUMNS CONFIGURED FROM SEARCH RESULT GRID
	IF (SELECT COUNT(*) FROM @TEMPSelectedColumns) = 0
	BEGIN
		INSERT INTO @TEMPSelectedColumns
		(
			numFieldID,
			bitCustomField,
			tintOrder,
			intColumnWidth
		)
		SELECT
			numFormFieldID
			,bitCustomField
			,tintOrder
			,intColumnWidth
		FROM
		(
			SELECT  
				A.numFormFieldID,
				0 bitCustomField,
				A.tintOrder,
				A.intColumnWidth
			FROM    AdvSerViewConf A
					JOIN View_DynamicDefaultColumns D ON D.numFieldID = A.numFormFieldID
			WHERE   A.numFormID = 1
					AND D.bitInResults = 1
					AND D.bitDeleted = 0
					AND D.numDomainID = @numDomainID 
					AND D.numFormID = 1
					AND A.numDomainID = @numDomainID
					AND A.numUserCntID=@numUserCntID
					AND ISNULL(A.bitCustom,0) = 0
			UNION
			SELECT  
				A.numFormFieldID,
				1 bitCustomField,
				A.tintOrder,
				A.intColumnWidth
			FROM    AdvSerViewConf A
					JOIN CFW_Fld_Master C ON C.Fld_id = A.numFormFieldID
			WHERE   A.numFormID = 1
					AND C.numDomainID = @numDomainID 
					AND A.numDomainID = @numDomainID
					AND A.numUserCntID=@numUserCntID
					AND ISNULL(A.bitCustom,0) = 1
		) T1
		ORDER BY
			 tintOrder
	END

	-- IF USER HASN'T CONFIGURED COLUMNS FROM SEARCH RESULT GRID THEN RETURN DEFAULT COLUMNS
	IF (SELECT COUNT(*) FROM @TEMPSelectedColumns) = 0
	BEGIN
		INSERT INTO @TEMPSelectedColumns
		(
			numFieldID,
			bitCustomField,
			tintOrder,
			intColumnWidth
		)
		SELECT  
			numFieldID,
			0,
			1,
			0
		FROM    
			View_DynamicDefaultColumns
		WHERE   
			numFormID = 1
			AND numDomainID = @numDomainID
			AND numFieldID = 3
	END

	SELECT TOP 1
		@tintOrder=tintOrder,
		@numFieldID=numFieldID,
		@vcDbColumnName=vcDbColumnName,
		@vcFormFieldName=vcFieldName,
		@vcAssociatedControlType=vcAssociatedControlType,
		@vcListItemType=vcListItemType,
		@numListID=numListID,
		@vcLookBackTableName=vcLookBackTableName,
		@bitCustom=bitCustom,
		@vcOrigDbColumnName=vcOrigDbColumnName,
		@bitAllowSorting=bitAllowSorting,
		@bitAllowEdit= bitAllowEdit,
		@ListRelID=ListRelID,
        @intColumnWidth=intColumnWidth,
		@bitAllowFiltering=bitAllowFiltering,
		@vcfieldatatype=vcFieldDataType,
		@numFieldGroupID=numGroupID
	FROM
	(
		SELECT
			T1.tintOrder+1 tintOrder,
			D.numFieldID,
			vcDbColumnName,
			vcFieldName,
			vcAssociatedControlType,
			vcListItemType,
			numListID,
			vcLookBackTableName,
			0 AS bitCustom,
			vcOrigDbColumnName,
			bitAllowSorting,
			bitAllowEdit,			
			ListRelID,			
			T1.intColumnWidth as intColumnWidth,
			bitAllowFiltering,
			vcFieldDataType,			
			0 AS numGroupID
		FROM 
			View_DynamicDefaultColumns D
		INNER JOIN
			@TEMPSelectedColumns T1
		ON 
			D.numFieldID=T1.numFieldID                                                     
		WHERE 
			D.numDomainID = @numDomainID 
			AND D.numFormID = 1 
			AND ISNULL(T1.bitCustomField,0) = 0
			AND d.vcDbColumnName not in ('numBillShipCountry','numBillShipState')
		UNION
		SELECT
			T1.tintOrder+1 tintOrder,
			Fld_id AS numFieldID,
			CONCAT('Cust',Fld_id),
			Fld_label,
			c.Fld_type,
			'',
			c.numlistid,
			'',			
			1 AS bitCustom,
			'',
			null,
			null,
			null,
			null,
			null,
			'',
			Grp_id
		FROM    
			CFW_Fld_Master C 
		INNER JOIN
			@TEMPSelectedColumns T1
		ON 
			C.Fld_id=T1.numFieldID 
		WHERE   
			C.numDomainID = @numDomainID 
			AND ISNULL(T1.bitCustomField,0) = 1
	) T1
	ORDER BY 
		tintOrder asc                              

while @tintOrder>0                              
begin                              
     
	
    IF @vcLookBackTableName = 'AdditionalContactsInformation' 
        SET @Prefix = 'ADC.'
    IF @vcLookBackTableName = 'DivisionMaster' 
        SET @Prefix = 'DM.'
     
	IF @bitCustom = 0 
	BEGIN                           
		SET @vcColumnName= @vcDbColumnName+'~'+ convert(varchar(10),@numFieldId) +'~0'

		if @vcAssociatedControlType='SelectBox'                              
		begin        
		
		print @vcDbColumnName
					IF @vcDbColumnName='numDefaultShippingServiceID'
				BEGIN
					SET @strSql=@strSql+ ' ,ISNULL((SELECT vcShipmentService FROM ShippingService WHERE (numDomainID=DM.numDomainID OR ISNULL(numDomainID,0)=0) AND numShippingServiceID = DM.numDefaultShippingServiceID),'''')' + ' [' + @vcColumnName + ']' 
				END                      
                                              
			 else if @vcListItemType='LI'                               
			  begin    
				  IF @numListID=40--Country
				  BEGIN
	  				set @strSql=@strSql+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName+']'                              
					IF @vcDbColumnName ='numCountry'
					BEGIN
						IF @bitContactAddressJoinAdded=0 
					  BEGIN
							set @WhereCondition= @WhereCondition +' left Join AddressDetails AD on AD.numRecordId= ADC.numContactID and AD.tintAddressOf=1 and AD.tintAddressType=0 AND AD.bitIsPrimary=1 and AD.numDomainID= ADC.numDomainID'	
							SET @bitContactAddressJoinAdded=1;
					  END
					  set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID=AD.numCountry'	
					END
					ELSE IF @vcDbColumnName ='numBillCountry'
					BEGIN
						IF @bitBillAddressJoinAdded=0 
						BEGIN
							set @WhereCondition= @WhereCondition +' left Join AddressDetails AD1 on AD1.numRecordId= DM.numDivisionID and AD1.tintAddressOf=2 and AD1.tintAddressType=1 AND AD1.bitIsPrimary=1 and AD1.numDomainID= ADC.numDomainID '
					
							SET @bitBillAddressJoinAdded=1;
						END
						set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID=AD1.numCountry'
					END
					ELSE IF @vcDbColumnName ='numShipCountry'
					BEGIN
						IF @bitShipAddressJoinAdded=0 
						BEGIN
							set @WhereCondition= @WhereCondition +' left Join AddressDetails AD2 on AD2.numRecordId= DM.numDivisionID and AD2.tintAddressOf=2 and AD2.tintAddressType=2 AND AD2.bitIsPrimary=1 and AD2.numDomainID= ADC.numDomainID '
					
							SET @bitShipAddressJoinAdded=1;
						END
						set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID=AD2.numCountry'
					END
				  END                        
				  ELSE
				  BEGIN
						set @strSql=@strSql+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName +']'                              
						set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID='+@vcDbColumnName	
				  END
			  end                              
	                      
			  else if @vcListItemType='T'                               
			  begin                              
				  set @strSql=@strSql+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName +']'                              
			   set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID='+@vcDbColumnName                              
			  end  
			  else if   @vcListItemType='U'                                                     
				begin           
					set @strSql=@strSql+',dbo.fn_GetContactName('+ @Prefix + @vcDbColumnName+') ['+ @vcColumnName +']'
				end   
	
		end  
		ELSE IF @vcAssociatedControlType='ListBox'   
		BEGIN
			if @vcListItemType='S'                               
			  begin                         
				set @strSql=@strSql+',S'+ convert(varchar(3),@tintOrder)+'.vcState'+' ['+ @vcColumnName +']'
				IF @vcDbColumnName ='numState'
				BEGIN
					IF @bitContactAddressJoinAdded=0 
					   BEGIN
							set @WhereCondition= @WhereCondition +' left Join AddressDetails AD on AD.numRecordId= ADC.numContactID and AD.tintAddressOf=1 and AD.tintAddressType=0 AND AD.bitIsPrimary=1 and AD.numDomainID= ADC.numDomainID'	
							SET @bitContactAddressJoinAdded=1;
					  END	
					  set @WhereCondition= @WhereCondition +' left join State S'+ convert(varchar(3),@tintOrder)+ ' on S'+convert(varchar(3),@tintOrder)+ '.numStateID=AD.numState'
				END
				ELSE IF @vcDbColumnName ='numBillState'
				BEGIN
					IF @bitBillAddressJoinAdded=0 
					BEGIN
						set @WhereCondition= @WhereCondition +' left Join AddressDetails AD1 on AD1.numRecordId= DM.numDivisionID and AD1.tintAddressOf=2 and AD1.tintAddressType=1 AND AD1.bitIsPrimary=1 and AD1.numDomainID= ADC.numDomainID '
						SET @bitBillAddressJoinAdded=1;
					END
					set @WhereCondition= @WhereCondition +' left join State S'+ convert(varchar(3),@tintOrder)+ ' on S'+convert(varchar(3),@tintOrder)+ '.numStateID=AD1.numState'
				END
				ELSE IF @vcDbColumnName ='numShipState'
				BEGIN
					IF @bitShipAddressJoinAdded=0 
					BEGIN
						set @WhereCondition= @WhereCondition +' left Join AddressDetails AD2 on AD2.numRecordId= DM.numDivisionID and AD2.tintAddressOf=2 and AD2.tintAddressType=2 AND AD2.bitIsPrimary=1 and AD2.numDomainID= ADC.numDomainID '
						SET @bitShipAddressJoinAdded=1;
					END
					set @WhereCondition= @WhereCondition +' left join State S'+ convert(varchar(3),@tintOrder)+ ' on S'+convert(varchar(3),@tintOrder)+ '.numStateID=AD2.numState'
				END
			  end         
		END 
		ELSE IF @vcAssociatedControlType='TextBox'   
		BEGIN
			IF @vcDbColumnName='vcStreet' OR @vcDbColumnName='vcCity' OR @vcDbColumnName='vcPostalCode'
			begin                              
			   set @strSql=@strSql+',AD.'+ @vcDbColumnName+' ['+ @vcColumnName +']'                              
			   IF @bitContactAddressJoinAdded=0 
			   BEGIN
					set @WhereCondition= @WhereCondition +' left Join AddressDetails AD on AD.numRecordId= ADC.numContactID and AD.tintAddressOf=1 and AD.tintAddressType=0 AND AD.bitIsPrimary=1 and AD.numDomainID= ADC.numDomainID'	
					SET @bitContactAddressJoinAdded=1;
			   END
			end 
			ELSE IF @vcDbColumnName='vcBillStreet' OR @vcDbColumnName='vcBillCity' OR @vcDbColumnName='vcBillPostCode'
			begin                              
			   set @strSql=@strSql+',AD1.'+ REPLACE(REPLACE(@vcDbColumnName,'Bill',''),'PostCode','PostalCode') +' ['+ @vcColumnName +']'                              
			   IF @bitBillAddressJoinAdded=0 
			   BEGIN 
	   				set @WhereCondition= @WhereCondition +' left Join AddressDetails AD1 on AD1.numRecordId= DM.numDivisionID and AD1.tintAddressOf=2 and AD1.tintAddressType=1 AND AD1.bitIsPrimary=1 and AD1.numDomainID= ADC.numDomainID '
	   				SET @bitBillAddressJoinAdded=1;
			   END
			end 
			ELSE IF @vcDbColumnName='vcShipStreet' OR @vcDbColumnName='vcShipCity' OR @vcDbColumnName='vcShipPostCode'
			begin                              
			   set @strSql=@strSql+',AD2.'+ REPLACE(REPLACE(@vcDbColumnName,'Ship',''),'PostCode','PostalCode') +' ['+ @vcColumnName +']'                              
			   IF @bitShipAddressJoinAdded=0 
			   BEGIN
	   				set @WhereCondition= @WhereCondition +' left Join AddressDetails AD2 on AD2.numRecordId= DM.numDivisionID and AD2.tintAddressOf=2 and AD2.tintAddressType=2  AND AD2.bitIsPrimary=1 and AD2.numDomainID= ADC.numDomainID '
	   				SET @bitShipAddressJoinAdded=1;
			   END
			end 
			ELSE
			BEGIN
				set @strSql=@strSql+','+ case  when @vcDbColumnName='numAge' then 'year(getutcdate())-year(bintDOB)' when @vcDbColumnName='numDivisionID' then 'DM.numDivisionID' when @vcDbColumnName='bintCreatedDate' then 'dbo.[FormatedDateFromDate](DateAdd(minute, ' + CAST(-@ClientTimeZoneOffset AS VARCHAR) + ',DM.bintCreatedDate),DM.numDomainID)' else @vcDbColumnName end+' ['+ @vcColumnName +']'		
			END
		END
		ELSE IF @vcAssociatedControlType='Label' and @vcDbColumnName='vcLastSalesOrderDate'
		BEGIN
			SET @WhereCondition = @WhereCondition
											+ ' OUTER APPLY(' + CONCAT('SELECT MAX(bintCreatedDate) AS bintCreatedDate FROM OpportunityMaster WHERE numDomainID = ',@numDomainID,' AND numDivisionID=DM.numDivisionID AND tintOppType = 1 AND tintOppStatus = 1') + ' ) AS TempLastOrder '

			set @strSql=@strSql+','+ CONCAT('ISNULL(dbo.FormatedDateFromDate(DateAdd(minute, ',-@ClientTimeZoneOffset,',TempLastOrder.bintCreatedDate),',@numDomainID,'),'''')') +' ['+ @vcColumnName+']'
		END	                    
		ELSE 
			SET @strSql=@strSql+','+ case  when @vcDbColumnName='numAge' then 'year(getutcdate())-year(bintDOB)' when @vcDbColumnName='numDivisionID' then 'DM.numDivisionID' when @vcDbColumnName='bintCreatedDate' then 'dbo.[FormatedDateFromDate](DM.bintCreatedDate,DM.numDomainID)' else @vcDbColumnName end+' ['+ @vcColumnName +']'
	END
	ELSE IF @bitCustom = 1                
	BEGIN
		--SET @vcColumnName= CONCAT(@vcFormFieldName,'~','Cust',@numFieldId)
		SET @vcColumnName= CONCAT('Cust',@numFieldId,'~',@numFieldId,'~',1)
			
		IF @vcAssociatedControlType = 'TextBox' or @vcAssociatedControlType = 'TextArea'            
		BEGIN
			SET @strSql= @strSql+',CFW'+ convert(VARCHAR(3),@tintOrder)+'.Fld_Value  ['+ @vcColumnName +']'                   
				
			SET @WhereCondition= @WhereCondition 
								+' left Join ' + (CASE WHEN @numFieldGroupID = 1 THEN 'CFW_FLD_Values' WHEN @numFieldGroupID = 4 THEN 'CFW_FLD_Values_Cont' ELSE 'CFW_FLD_Values' END) + ' CFW'+ convert(varchar(3),@tintOrder)
								+ ' on CFW' + convert(varchar(3),@tintOrder) 
								+ '.Fld_Id='+convert(varchar(10),@numFieldId) 
								+ 'and CFW' + convert(varchar(3),@tintOrder) 
								+ '.RecId= (CASE WHEN ' + CAST(@numFieldGroupID AS VARCHAR) + ' = 1 THEN DM.numDivisionId WHEN ' + CAST(@numFieldGroupID AS VARCHAR) + ' = 4 THEN ADC.numContactID ELSE 0 END) '                                                         
		END   
		ELSE IF @vcAssociatedControlType = 'CheckBox'       
		BEGIN
			set @strSql= @strSql+',case when isnull(CFW' + CONVERT(VARCHAR(3), @tintOrder)+ '.Fld_Value,0)='''' then 0 else  isnull(CFW'
											+ CONVERT(VARCHAR(3), @tintOrder)+ '.Fld_Value,0) end ['+ @vcColumnName + ']'
 
			set @WhereCondition= @WhereCondition +' left Join ' + (CASE WHEN @numFieldGroupID = 1 THEN 'CFW_FLD_Values' WHEN @numFieldGroupID = 4 THEN 'CFW_FLD_Values_Cont' ELSE 'CFW_FLD_Values' END) + ' CFW'+ convert(varchar(3),@tintOrder)+ '             
			on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId= (CASE WHEN ' + CAST(@numFieldGroupID AS VARCHAR) + ' = 1 THEN DM.numDivisionId WHEN ' + CAST(@numFieldGroupID AS VARCHAR) + ' = 4 THEN ADC.numContactID ELSE 0 END) '                                                     
		END                
		ELSE IF @vcAssociatedControlType = 'DateField'            
		BEGIN
			set @strSql= @strSql+',dbo.FormatedDateFromDate(CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value,'+convert(varchar(10),@numDomainId)+')  ['+ @vcColumnName +']'                   
			set @WhereCondition= @WhereCondition +' left Join ' + (CASE WHEN @numFieldGroupID = 1 THEN 'CFW_FLD_Values' WHEN @numFieldGroupID = 4 THEN 'CFW_FLD_Values_Cont' ELSE 'CFW_FLD_Values' END) + ' CFW'+ convert(varchar(3),@tintOrder)+ '                 
			on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId= (CASE WHEN ' + CAST(@numFieldGroupID AS VARCHAR) + ' = 1 THEN DM.numDivisionId WHEN ' + CAST(@numFieldGroupID AS VARCHAR) + ' = 4 THEN ADC.numContactID ELSE 0 END) '                                                         
		END                
		ELSE IF @vcAssociatedControlType = 'SelectBox'             
		BEGIN
			set @vcDbColumnName = 'DCust'+Convert(varchar(10),@numFieldId)                
			set @strSql=@strSql+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName +']'                                                          
			set @WhereCondition= @WhereCondition +' left Join ' + (CASE WHEN @numFieldGroupID = 1 THEN 'CFW_FLD_Values' WHEN @numFieldGroupID = 4 THEN 'CFW_FLD_Values_Cont' ELSE 'CFW_FLD_Values' END) + ' CFW'+ convert(varchar(3),@tintOrder)+ '                 
				on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId= (CASE WHEN ' + CAST(@numFieldGroupID AS VARCHAR) + ' = 1 THEN DM.numDivisionId WHEN ' + CAST(@numFieldGroupID AS VARCHAR) + ' = 4 THEN ADC.numContactID ELSE 0 END) '                                                        
			set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID=CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value'                
		END         
		ELSE IF @vcAssociatedControlType = 'CheckBoxList'
		BEGIN
			PRINT 'Custom1'

			SET @strSql= @strSql+',(SELECT STUFF((SELECT CONCAT('', '', vcData) FROM ListDetails WHERE numListID= ' + CAST(@numListID AS varchar) + ' AND numlistitemid IN (SELECT ISNULL(Id,0) FROM dbo.SplitIDs(CFW'+ convert(VARCHAR(3),@tintOrder)+'.Fld_Value,'','')) FOR XML PATH('''')), 1, 1, ''''))  ['+ @vcColumnName +']'   

			SET @WhereCondition= @WhereCondition 
								+' left Join ' + (CASE WHEN @numFieldGroupID = 1 THEN 'CFW_FLD_Values' WHEN @numFieldGroupID = 4 THEN 'CFW_FLD_Values_Cont' ELSE 'CFW_FLD_Values' END) + ' CFW'+ convert(varchar(3),@tintOrder)
								+ ' on CFW' + convert(varchar(3),@tintOrder) 
								+ '.Fld_Id='+convert(varchar(10),@numFieldId) 
								+ 'and CFW' + convert(varchar(3),@tintOrder) 
								+ '.RecId= (CASE WHEN ' + CAST(@numFieldGroupID AS VARCHAR) + ' = 1 THEN DM.numDivisionId WHEN ' + CAST(@numFieldGroupID AS VARCHAR) + ' = 4 THEN ADC.numContactID ELSE 0 END) '										
		
		END        
	END          
    
	SELECT TOP 1
		@tintOrder=tintOrder,
		@numFieldID=numFieldID,
		@vcDbColumnName=vcDbColumnName,
		@vcFormFieldName=vcFieldName,
		@vcAssociatedControlType=vcAssociatedControlType,
		@vcListItemType=vcListItemType,
		@numListID=numListID,
		@vcLookBackTableName=vcLookBackTableName,
		@bitCustom=bitCustom,
		@vcOrigDbColumnName=vcOrigDbColumnName,
		@bitAllowSorting=bitAllowSorting,
		@bitAllowEdit= bitAllowEdit,
		@ListRelID=ListRelID,
        @intColumnWidth=intColumnWidth,
		@bitAllowFiltering=bitAllowFiltering,
		@vcfieldatatype=vcFieldDataType,
		@numFieldGroupID=numGroupID
	FROM
	(
		SELECT
			T1.tintOrder+1 tintOrder,
			D.numFieldID,
			vcDbColumnName,
			vcFieldName,
			vcAssociatedControlType,
			vcListItemType,
			numListID,
			vcLookBackTableName,
			0 AS bitCustom,
			vcOrigDbColumnName,
			bitAllowSorting,
			bitAllowEdit,			
			ListRelID,			
			T1.intColumnWidth as intColumnWidth,
			bitAllowFiltering,
			vcFieldDataType,			
			0 AS numGroupID
		FROM 
			View_DynamicDefaultColumns D
		INNER JOIN
			@TEMPSelectedColumns T1
		ON 
			D.numFieldID=T1.numFieldID                                                     
		WHERE 
			D.numDomainID = @numDomainID 
			AND D.numFormID = 1 
			AND ISNULL(T1.bitCustomField,0) = 0
			AND d.vcDbColumnName not in ('numBillShipCountry','numBillShipState')
			AND T1.tintOrder > @tintOrder-1
		UNION
		SELECT
			T1.tintOrder+1 tintOrder,
			Fld_id AS numFieldID,
			CONCAT('Cust',Fld_id),
			Fld_label,
			c.Fld_type,
			'',
			c.numlistid,
			'',			
			1 AS bitCustom,
			'',
			null,
			null,
			null,
			null,
			null,
			'',
			Grp_id
		FROM    
			CFW_Fld_Master C 
		INNER JOIN
			@TEMPSelectedColumns T1
		ON 
			C.Fld_id=T1.numFieldID 
		WHERE   
			C.numDomainID = @numDomainID 
			AND ISNULL(T1.bitCustomField,0) = 1
			AND T1.tintOrder > @tintOrder-1
	) T1
	ORDER BY 
		tintOrder asc                    
                                      
 if @@rowcount=0 set @tintOrder=0                              
end                              

                                                                  
  declare @firstRec as integer                                                                    
  declare @lastRec as integer                                                                    
 set @firstRec= (@CurrentPage-1) * @PageSize                      
 set @lastRec= (@CurrentPage*@PageSize+1)                                                                     
set @TotRecs=(select count(*) from #tempTable)                                                   
                                          
if @GetAll=0         
begin        
        
 if @bitMassUpdate=1            
 begin  

   Declare @strReplace as varchar(8000) = ''
    set @strReplace = case when @LookTable='DivisionMaster' then 'DM.numDivisionID' when @LookTable='AdditionalContactsInformation' then 'ADC.numContactId' when @LookTable='ContactAddress' then 'ADC.numContactId' when @LookTable='ShipAddress' then 'DM.numDivisionID' when @LookTable='BillAddress' then 'DM.numDivisionID' when @LookTable='CompanyInfo' then 'C.numCompanyId' end           
   +' FROM AdditionalContactsInformation ADC inner join DivisionMaster DM on ADC.numDivisionID = DM.numDivisionID                  
   JOIN CompanyInfo C ON DM.numCompanyID = C.numCompanyId                                
    '+@WhereCondition        
    set @strMassUpdate=replace(@strMassUpdate,'@$replace',@strReplace)      
    PRINT CONCAT('MassUPDATE:',@strMassUpdate)
  exec (@strMassUpdate)         
 end         
 
 
 IF (@Where = 'DUP')
 BEGIN
	set @strSql=@strSql+' ,Cast((select top 1 bintCreatedDate from OpportunityMaster where numDivisionId=DM.numDivisionID order by numOppId desc) AS DATE)  AS [Last Order Date~bintCreatedDate] FROM AdditionalContactsInformation ADC
	  JOIN DivisionMaster DM ON ADC.numDivisionId = DM.numDivisionID
	  JOIN CompanyInfo C ON DM.numCompanyID = C.numCompanyId    
	  JOIN OpportunityMaster OM on OM.numDivisionID= DM.numDivisionID                  
	  inner join #tempTable T on T.numContactId=ADC.numContactId ' 
	  + @WhereCondition +'
	  WHERE ID > '+convert(varchar(10),@firstRec)+ ' and ID <'+ convert(varchar(10),@lastRec) +  @WCondition + ' order by C.vcCompanyName, T.ID' -- @OrderBy
	  PRINT @strSql
	  exec(@strSql)                                                                 
	 drop table #tempTable
	 RETURN	
 END
ELSE
BEGIN
	SET @strSql = @strSql + ' ,Cast((select top 1 bintCreatedDate from OpportunityMaster where numDivisionId=DM.numDivisionID order by numOppId desc) AS DATE)  AS [Last Order Date~bintCreatedDate] FROM AdditionalContactsInformation ADC
		JOIN DivisionMaster DM ON ADC.numDivisionId = DM.numDivisionID   
	  JOIN CompanyInfo C ON DM.numCompanyID = C.numCompanyId                              
	   '+@WhereCondition+' inner join #tempTable T on T.numContactId=ADC.numContactId                                                               
	  WHERE ID > '+convert(varchar(10),@firstRec)+ ' and ID <'+ convert(varchar(10),@lastRec)
	  
	   
	SET @strSql=@strSql + ' order by C.vcCompanyName, T.ID' 
END
             
end                           
 else        
 begin                      
     set @strSql=@strSql+' ,Cast((select top 1 bintCreatedDate from OpportunityMaster where numDivisionId=DM.numDivisionID order by numOppId desc) AS DATE)  AS [Last Order Date~bintCreatedDate] FROM AdditionalContactsInformation ADC                                                   
  JOIN DivisionMaster DM ON ADC.numDivisionId = DM.numDivisionID                                                                   
  JOIN CompanyInfo C ON DM.numCompanyID = C.numCompanyId                              
  '+@WhereCondition+' join #tempTable T on T.numContactId=ADC.numContactId'        
  
	   SET @strSql=@strSql + ' order by C.vcCompanyName, T.ID'   
	end       
print @strSql                                                      
exec(@strSql) 
drop table #tempTable


	SELECT
		tintOrder,
		vcDbColumnName,
		vcFieldName,
		vcAssociatedControlType,
		vcListItemType,
		numListID,
		vcLookBackTableName,
		numFieldID,
		numFieldID AS numFormFieldID,
		intColumnWidth,
		bitCustom AS bitCustomField,
		vcOrigDbColumnName,
		bitAllowSorting,
		bitAllowEdit,
		ListRelID,
		bitAllowFiltering,
		vcFieldDataType
	FROM
	(
		SELECT
			T1.tintOrder+1 tintOrder,
			D.numFieldID,
			vcDbColumnName,
			vcFieldName,
			vcAssociatedControlType,
			vcListItemType,
			numListID,
			vcLookBackTableName,
			0 AS bitCustom,
			vcOrigDbColumnName,
			bitAllowSorting,
			bitAllowEdit,			
			ListRelID,			
			T1.intColumnWidth as intColumnWidth,
			bitAllowFiltering,
			vcFieldDataType,			
			0 AS numGroupID
		FROM 
			View_DynamicDefaultColumns D
		INNER JOIN
			@TEMPSelectedColumns T1
		ON 
			D.numFieldID=T1.numFieldID                                                     
		WHERE 
			D.numDomainID = @numDomainID 
			AND D.numFormID = 1 
			AND ISNULL(T1.bitCustomField,0) = 0
			AND d.vcDbColumnName not in ('numBillShipCountry','numBillShipState')
		UNION
		SELECT
			T1.tintOrder+1 tintOrder,
			Fld_id AS numFieldID,
			CONCAT('Cust',Fld_id),
			Fld_label,
			c.Fld_type,
			'',
			c.numlistid,
			'',			
			1 AS bitCustom,
			'',
			null,
			null,
			null,
			null,
			null,
			'',
			Grp_id
		FROM    
			CFW_Fld_Master C 
		INNER JOIN
			@TEMPSelectedColumns T1
		ON 
			C.Fld_id=T1.numFieldID 
		WHERE   
			C.numDomainID = @numDomainID 
			AND ISNULL(T1.bitCustomField,0) = 1
	) T1
	ORDER BY 
		tintOrder asc       

IF OBJECT_ID('tempdb..#tempAddress') IS NOT NULL
BEGIN
    DROP TABLE #tempAddress
END
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                           
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_AdvancedSearch_GetListDetails')
DROP PROCEDURE USP_AdvancedSearch_GetListDetails
GO
CREATE PROCEDURE [dbo].[USP_AdvancedSearch_GetListDetails]                      
	@numListID NUMERIC(18,0)                      
	,@vcItemType CHAR(3)                      
	,@numDomainID NUMERIC(18,0)                    
	,@vcSearchText VARCHAR(300)
	,@intOffset INT
	,@intPageSize INT
AS     
BEGIN
	IF @vcItemType = 'SYS'
	BEGIN
		SELECT
			numItemID as id
			,vcItemName as text
			,COUNT(*) OVER() AS numTotalRecords
		FROM
		(
			SELECT 0 AS numItemID,'Lead' AS vcItemName
			UNION ALL
			SELECT 1 AS numItemID,'Prospect' AS vcItemName
			UNION ALL
			SELECT 2 AS numItemID,'Account' AS vcItemName
		) TEMP
		WHERE
			(vcItemName LIKE '%' + @vcSearchText + '%' OR ISNULL(@vcSearchText,'') = '')
		ORDER BY
			vcItemName
		OFFSET 
			@intOffset ROWS FETCH NEXT @intPageSize ROWS ONLY;
	END
	
	IF @vcItemType='LI' AND @numListID=9 --Opportunity Source
	BEGIN
		DECLARE @TEMP TABLE
		(numItemID VARCHAR(50),vcItemName VARCHAR(100), constFlag BIT, bitDelete BIT,intSortOrder INT, numListItemID NUMERIC(9),vcData VARCHAR(100), vcListType VARCHAR(1000), vcOrderType VARCHAR(50), vcOppOrOrder VARCHAR(20), numListType NUMERIC(18,0), tintOppOrOrder TINYINT)

		INSERT INTO @TEMP
		SELECT '0~1' AS numItemID,'Internal Order' AS vcItemName, 1 AS  constFlag, 1 AS  bitDelete, 0 intSortOrder, -1 AS numListItemID,'Internal Order','Internal Order','','',0,0
		UNION
		SELECT CAST([numSiteID] AS VARCHAR(250)) +'~2' AS numItemID,[vcSiteName] AS vcItemName, 1 AS  constFlag, 1 AS  bitDelete, 0 intSortOrder, CAST([numSiteID] AS VARCHAR(18)) AS numListItemID,[vcSiteName],[Sites].[vcSiteName],'','',0,0
		FROM Sites WHERE [numDomainID] = @numDomainID
		UNION
		SELECT DISTINCT CAST(WebApiId  AS VARCHAR(250)) +'~3' AS numItemID,vcProviderName AS vcItemName,1 constFlag, 1 bitDelete, 0 intSortOrder, CAST(WebApiId  AS VARCHAR(18)) AS numListItemID,vcProviderName,[WebAPI].[vcProviderName],'','',0,0
		FROM dbo.WebAPI   
		UNION
		SELECT CAST(Ld.numListItemID  AS VARCHAR(250)) +'~1' AS numItemID, isnull(vcRenamedListName,vcData)  AS vcItemName, Ld.constFlag,bitDelete,isnull(intSortOrder,0) intSortOrder,CAST(Ld.numListItemID  AS VARCHAR(18)),isnull(vcRenamedListName,vcData) as vcData,
		(SELECT vcData FROM ListDetails WHERE numListId=9 and numListItemId = ld.numListType AND [ListDetails].[numDomainID] = @numDomainID),(CASE when ld.numListType=1 then 'Sales' when ld.numListType=2 then 'Purchase' else 'All' END)
		,(CASE WHEN ld.tintOppOrOrder=1 THEN 'Opportunity' WHEN ld.tintOppOrOrder=2 THEN 'Order' ELSE 'All' END),numListType,tintOppOrOrder
		FROM listdetails Ld  left join listorder LO 
		on Ld.numListItemID= LO.numListItemID and Lo.numDomainId = @numDomainID 
		WHERE Ld.numListID=9 and (constFlag=1 or Ld.numDomainID=@numDomainID)

		SELECT 
			numItemID as id
			,vcItemName as text
			,COUNT(*) OVER() AS numTotalRecords
		FROM 
			@TEMP 
		WHERE 
			(vcItemName LIKE '%' + @vcSearchText + '%' OR ISNULL(@vcSearchText,'') = '')
		ORDER BY
			vcItemName
		OFFSET 
			@intOffset ROWS FETCH NEXT @intPageSize ROWS ONLY;
	END                 
	ELSE IF @vcItemType = 'LI'    --Master List                    
	BEGIN
		SELECT 
			LD.vcData AS text
			,LD.numListItemID AS id
			,COUNT(*) OVER() AS numTotalRecords
		FROM 
			ListDetails LD 
		LEFT JOIN 
			listorder LO 
		ON 
			Ld.numListItemID= LO.numListItemID 
			AND Lo.numDomainId = @numDomainID  
		WHERE 
			LD.numListID = @numListID 
			AND (LD.numDomainID = @numDomainID OR LD.constFlag = 1)
			AND (LD.vcData LIKE '%' + @vcSearchText + '%' OR ISNULL(@vcSearchText,'') = '')
		ORDER BY 
			ISNULL(intSortOrder,LD.sintOrder)
		OFFSET 
			@intOffset ROWS FETCH NEXT @intPageSize ROWS ONLY;                        
	END      
	ELSE IF @vcItemType = 'L' AND ISNULL(@numListID,0) > 0    --Master List                    
	BEGIN
		SELECT 
			LD.vcData AS text
			,LD.numListItemID AS id
			,COUNT(*) OVER() AS numTotalRecords
		FROM 
			ListDetails LD 
		LEFT JOIN 
			listorder LO 
		ON 
			Ld.numListItemID= LO.numListItemID 
			AND Lo.numDomainId = @numDomainID  
		WHERE 
			LD.numListID = @numListID 
			AND (LD.numDomainID = @numDomainID OR LD.constFlag = 1)
			AND (LD.vcData LIKE '%' + @vcSearchText + '%' OR ISNULL(@vcSearchText,'') = '')
		ORDER BY 
			ISNULL(intSortOrder,LD.sintOrder)
		OFFSET 
			@intOffset ROWS FETCH NEXT @intPageSize ROWS ONLY;                       
	END                                          
	ELSE IF @vcItemType = 'T'    --Territories                    
	BEGIN
		SELECT 
			vcData AS text
			,numListItemID AS id
			,COUNT(*) OVER() AS numTotalRecords
		FROM 
			ListDetails 
		WHERE 
			numListID = 78 
			AND numDomainID = @numDomainID
			AND (vcData LIKE '%' + @vcSearchText + '%' OR ISNULL(@vcSearchText,'') = '')
		ORDER BY
			vcData
		OFFSET 
			@intOffset ROWS FETCH NEXT @intPageSize ROWS ONLY;                                  
	END                        
	ELSE IF @vcItemType = 'AG'   --Lead Groups                     
	BEGIN
		SELECT 
			vcGrpName AS text
			,numGrpId AS id
			,COUNT(*) OVER() AS numTotalRecords
		FROM 
			Groups                     
		WHERE
			(vcGrpName LIKE '%' + @vcSearchText + '%' OR ISNULL(@vcSearchText,'') = '')
		ORDER BY
			vcGrpName
		OFFSET 
			@intOffset ROWS FETCH NEXT @intPageSize ROWS ONLY;
	END                         
	ELSE IF @vcItemType = 'S'    --States                    
	BEGIN
		SELECT 
			vcState AS text
			,numStateID AS id
			,COUNT(*) OVER() AS numTotalRecords
		FROM 
			State 
		WHERE 
			numDomainID = @numDomainID
			AND (vcState LIKE '%' + @vcSearchText + '%' OR ISNULL(@vcSearchText,'') = '')
		ORDER BY
			vcState
		OFFSET 
			@intOffset ROWS FETCH NEXT @intPageSize ROWS ONLY;             
	END         
	ELSE IF @vcItemType = 'U'    --Users                    
	BEGIN
		SELECT
			numItemID id
			,vcItemName text
			,COUNT(*) OVER() AS numTotalRecords
		FROM
		(
			SELECT 
				A.numContactID AS numItemID
				,A.vcFirstName + ' '+ A.vcLastName AS vcItemName              
			FROM 
				UserMaster UM             
			JOIN 
				AdditionalContactsInformation A            
			ON 
				UM.numUserDetailId=A.numContactID              
			WHERE 
				UM.numDomainID=@numDomainID        
				AND ((vcFirstName LIKE '%' + @vcSearchText + '%' OR vcLastName LIKE '%' + @vcSearchText + '%' ) OR ISNULL(@vcSearchText,'') = '')
			UNION        
			SELECT 
				A.numContactID
				, vcCompanyName + ' - ' + A.vcFirstName + ' ' + A.vcLastName        
			FROM AdditionalContactsInformation A         
			join DivisionMaster D        
			on D.numDivisionID=A.numDivisionID        
			join ExtarnetAccounts E         
			on E.numDivisionID=D.numDivisionID        
			join ExtranetAccountsDtl DTL        
			on DTL.numExtranetID=E.numExtranetID        
			join CompanyInfo C        
			on C.numCompanyID=D.numCompanyID        
			where A.numDomainID=@numDomainID and bitPartnerAccess=1        
			and D.numDivisionID <>(select numDivisionID from domain where numDomainID=@numDomainID)
		) TEMP
		WHERE
			(vcItemName LIKE '%' + @vcSearchText + '%' OR ISNULL(@vcSearchText,'') = '')
		ORDER BY
			vcItemName
		OFFSET 
			@intOffset ROWS FETCH NEXT @intPageSize ROWS ONLY;                 
	END
	ELSE IF @vcItemType = 'C'    --Organization Campaign                    
	BEGIN
		SELECT  
			numCampaignID AS id
			,vcCampaignName + CASE bitIsOnline WHEN 1 THEN ' (Online)' ELSE ' (Offline)' END AS text
			,COUNT(*) OVER() AS numTotalRecords
		FROM 
			CampaignMaster
		WHERE 
			numDomainID = @numDomainID
			AND (vcCampaignName LIKE '%' + @vcSearchText + '%' OR ISNULL(@vcSearchText,'') = '')
		ORDER BY
			vcCampaignName
		OFFSET 
			@intOffset ROWS FETCH NEXT @intPageSize ROWS ONLY;
	END               
	ELSE IF @vcItemType = 'IG'
	BEGIN
		SELECT 
			vcItemGroup AS text
			,numItemGroupID AS id
			,COUNT(*) OVER() AS numTotalRecords
		FROM 
			dbo.ItemGroups 
		WHERE 
			numDomainID=@numDomainID   	
			AND (vcItemGroup LIKE '%' + @vcSearchText + '%' OR ISNULL(@vcSearchText,'') = '')
		ORDER BY
			vcItemGroup
		OFFSET 
			@intOffset ROWS FETCH NEXT @intPageSize ROWS ONLY;
	END
	ELSE IF @vcItemType = 'PP'
	BEGIN
		SELECT
			numItemID AS id
			,vcItemName AS text
			,COUNT(*) OVER() AS numTotalRecords
		FROM
		(
			SELECT 'Inventory Item' AS vcItemName, 'P' AS numItemID, 'P' As vcItemType, 0 As flagConst 
			UNION 
			SELECT 'Non-Inventory Item' AS vcItemName, 'N' AS numItemID, 'P' As vcItemType, 0 As flagConst 
			UNION 
			SELECT 'Service' AS vcItemName, 'S' AS numItemID, 'P' As vcItemType, 0 As flagConst
		) TEMP
		WHERE
			(vcItemName LIKE '%' + @vcSearchText + '%' OR ISNULL(@vcSearchText,'') = '')
		ORDER BY
			vcItemName
		OFFSET 
			@intOffset ROWS FETCH NEXT @intPageSize ROWS ONLY;
	END
	ELSE IF @vcItemType = 'V' 
	BEGIN
		SELECT 
			ISNULL(C.vcCompanyName,'') AS text
			,numVendorID AS id
			,COUNT(*) OVER() AS numTotalRecords 
		FROM 
			dbo.Vendor V 
		INNER JOIN 
			dbo.DivisionMaster DM 
		ON 
			DM.numDivisionID= V.numVendorID 
		INNER JOIN 
			dbo.CompanyInfo C 
		ON 
			C.numCompanyId = DM.numCompanyID 
		WHERE 
			V.numDomainID =@numDomainID
			AND (vcCompanyName LIKE '%' + @vcSearchText + '%' OR ISNULL(@vcSearchText,'') = '')
		ORDER BY
			C.vcCompanyName
		OFFSET 
			@intOffset ROWS FETCH NEXT @intPageSize ROWS ONLY;
	END
	ELSE IF @vcItemType = 'OC' 
	BEGIN
		SELECT DISTINCT 
			C.vcCurrencyDesc AS text
			,C.numCurrencyID AS id
			,COUNT(*) OVER() AS numTotalRecords 
		FROM 
			dbo.Currency C 
		WHERE 
			C.numDomainId=@numDomainID
			AND (vcCurrencyDesc LIKE '%' + @vcSearchText + '%' OR ISNULL(@vcSearchText,'') = '')
			AND C.numCurrencyID IN (SELECT ISNULL(numCurrencyID,0) FROM OpportunityMaster WHERE numDomainId=@numDomainID)
		ORDER BY
			vcCurrencyDesc
		OFFSET 
			@intOffset ROWS FETCH NEXT @intPageSize ROWS ONLY;
	END
	ELSE  IF @vcItemType = 'UOM'    --Organization Campaign                    
	BEGIN
		SELECT 
			u.numUOMId as id
			,u.vcUnitName as text
			,COUNT(*) OVER() AS numTotalRecords 
		FROM 
			UOM u 
		INNER JOIN 
			Domain d 
		ON 
			u.numDomainID=d.numDomainID
		WHERE 
			u.numDomainID=@numDomainID 
			AND d.numDomainID=@numDomainID 
			AND u.tintUnitType=(CASE WHEN ISNULL(d.charUnitSystem,'E')='E' THEN 1 WHEN d.charUnitSystem='M' THEN 2 END)
			AND (vcUnitName LIKE '%' + @vcSearchText + '%' OR ISNULL(@vcSearchText,'') = '')
		ORDER BY
			vcUnitName
		OFFSET 
			@intOffset ROWS FETCH NEXT @intPageSize ROWS ONLY;
    END   
	ELSE IF @vcItemType = 'O'    --Opp Type                   
	BEGIN
		SELECT
			numItemID AS id
			,vcItemName AS text
			,COUNT(*) OVER() AS numTotalRecords 
		FROM
		(
			SELECT  1 AS numItemID,'Sales' AS vcItemName
			UNion ALL
			SELECT  2 AS numItemID,'Purchase' AS vcItemName
		) TEMP
		WHERE
			(vcItemName LIKE '%' + @vcSearchText + '%' OR ISNULL(@vcSearchText,'') = '')
		ORDER BY
			vcItemName
		OFFSET 
			@intOffset ROWS FETCH NEXT @intPageSize ROWS ONLY;
	END 
	ELSE IF @vcItemType = 'OT'    --Opp Type                   
	BEGIN
		SELECT
			numItemID AS id
			,vcItemName AS text
			,COUNT(*) OVER() AS numTotalRecords 
		FROM
		(
			SELECT  1 AS numItemID,'Sales Opportunity' AS vcItemName
			UNION ALL
			SELECT  2 AS numItemID,'Purchase Opportunity' AS vcItemName
			UNION ALL
			SELECT  3 AS numItemID,'Sales Order' AS vcItemName
			UNION ALL
			SELECT  4 AS numItemID,'Purchase Order' AS vcItemName
		) TEMP
		WHERE
			(vcItemName LIKE '%' + @vcSearchText + '%' OR ISNULL(@vcSearchText,'') = '')
		ORDER BY
			vcItemName
		OFFSET 
			@intOffset ROWS FETCH NEXT @intPageSize ROWS ONLY;
	END
	ELSE  IF @vcItemType = 'CHK'    --Opp Type                   
	BEGIN
		SELECT
			numItemID AS id
			,vcItemName AS text
			,COUNT(*) OVER() AS numTotalRecords 
		FROM
		(
			SELECT  1 AS numItemID,'Yes' AS vcItemName
			UNION ALL
			SELECT  0 AS numItemID,'No' AS vcItemName
		) TEMP
		WHERE
			(vcItemName LIKE '%' + @vcSearchText + '%' OR ISNULL(@vcSearchText,'') = '')
		ORDER BY
			vcItemName
		OFFSET 
			@intOffset ROWS FETCH NEXT @intPageSize ROWS ONLY;
	END  
	ELSE IF @vcItemType='COA'
	BEGIN
		SELECT 
			C.[numAccountId] AS id
			,ISNULL(C.[vcAccountName],'') AS text
			,COUNT(*) OVER() AS numTotalRecords 
		FROM    [Chart_Of_Accounts] C
		INNER JOIN [AccountTypeDetail] ATD 
		ON C.[numParntAcntTypeId] = ATD.[numAccountTypeID]
		WHERE   C.[numDomainId] = @numDomainId
		AND (vcAccountName LIKE '%' + @vcSearchText + '%' OR ISNULL(@vcSearchText,'') = '')
		ORDER BY C.[vcAccountCode]
		OFFSET 
			@intOffset ROWS FETCH NEXT @intPageSize ROWS ONLY;
	END
	ELSE IF @vcItemType='IC'
	BEGIN
		SELECT
			numCategoryID AS id
			,ISNULL(vcCategoryName,'') AS text
			,COUNT(*) OVER() AS numTotalRecords 
		FROM
			Category
		WHERE
			numDomainID=@numDomainID
			AND (vcCategoryName LIKE '%' + @vcSearchText + '%' OR ISNULL(@vcSearchText,'') = '')
		ORDER BY
			vcCategoryName
		OFFSET 
			@intOffset ROWS FETCH NEXT @intPageSize ROWS ONLY;
	END
END       
GO
/****** Object:  StoredProcedure [dbo].[USP_AdvancedSearchCase]    Script Date: 07/26/2008 16:14:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_advancedsearchcase')
DROP PROCEDURE usp_advancedsearchcase
GO
CREATE PROCEDURE [dbo].[USP_AdvancedSearchCase]    
@WhereCondition as varchar(1000)='',                                                   
@numDomainID as numeric(9)=0,                                    
@numUserCntID as numeric(9)=0,                                                                       
@CurrentPage int,                                                                          
@PageSize int,                                                                          
@TotRecs int output,                                                                                                                   
@columnSortOrder as Varchar(10),      
@ColumnName as varchar(20)='',      
@SortCharacter as char(1),                              
@SortColumnName as varchar(20)='',      
@LookTable as varchar(10)='',    
@vcDisplayColumns VARCHAR(MAX) = ''    
as     
    
declare @tintOrder as tinyint                                    
declare @vcFormFieldName as varchar(50)                                    
declare @vcListItemType as varchar(3)                               
declare @vcListItemType1 as varchar(3)                                   
declare @vcAssociatedControlType varchar(20)                                    
declare @numListID AS numeric(9)                                    
declare @vcDbColumnName varchar(30)                                     
declare @ColumnSearch as varchar(10)                            
if (@SortCharacter<>'0' and @SortCharacter<>'') set @ColumnSearch=@SortCharacter     
Create table #tempTable ( ID INT IDENTITY PRIMARY KEY,                                                                           
      numCaseID varchar(15)                                                                          
 )    
    
declare @strSql as varchar(8000)                                                                    
 set  @strSql='select numCaseId from     
Cases
Join AdditionalContactsinformation ADC on ADC.numContactId = Cases.numContactId    
join DivisionMaster DM on Dm.numDivisionId = Cases.numDivisionId    
JOIN CompanyInfo C ON DM.numCompanyID = C.numCompanyId     
  
'    
    
                               
   if (@SortColumnName<>'')                          
  begin                                
 select @vcListItemType1=vcListItemType from View_DynamicDefaultColumns where vcDbColumnName=@SortColumnName and numFormID=1 and numDomainId=@numDomainId                              
  if @vcListItemType1='LI'                         
  begin                                      
    set @strSql= @strSql +' left Join ListDetails L2 on L2.numListItemID='+@SortColumnName                                    
  end                              
  end                                
  set @strSql=@strSql+' where Cases.numDomainID  = '+convert(varchar(15),@numDomainID)+   @WhereCondition                                        
       
    
if (@ColumnName<>'' and  @ColumnSearch<>'')                               
begin                                
  if @vcListItemType='LI'                                     
    begin                                      
      set @strSql= @strSql +' and L1.vcData like '''+@ColumnSearch +'%'''                                   
    end                                    
    else set @strSql= @strSql +' and '+  @ColumnName +' like '''+@ColumnSearch  +'%'''                                  
                                
end                                
   if (@SortColumnName<>'')                              
  begin                                 
    if @vcListItemType1='LI'                                     
    begin          
      set @strSql= @strSql +' order by L2.vcData '+@columnSortOrder            
    end                                    
    else  set @strSql= @strSql +' order by '+ case when @SortColumnName='textSubject' then 'convert(varchar(500),textSubject)' 
												   when @SortColumnName = 'textInternalComments' then 'convert(varchar(500),textInternalComments)' else @SortColumnName end  +' ' +@columnSortOrder                              
  end     
    
    
insert into #tempTable(numCaseID)                     
    
exec(@strSql)      
 print @strSql   
 print '===================================================='    
                          
set @strSql=''                                    
   DECLARE @Prefix AS VARCHAR(10)
DECLARE @Table AS VARCHAR(200)                                 
                                   
set @tintOrder=0                                    
set @WhereCondition =''                                 
set @strSql='select Cases.numCaseId '    

	DECLARE @TEMPSelectedColumns TABLE
	(
		numFieldID NUMERIC(18,0)
		,bitCustomField BIT
		,tintOrder INT
		,intColumnWidth FLOAT
	)
	DECLARE @vcLocationID VARCHAR(100) = '0'
	SELECT @vcLocationID =isnull(vcLocationID,'0') from dynamicFormMaster where numFormID=17  

	-- IF USER HAS SELECTED ITS OWN DISPLAY COLUMNS THEN USE IT
	IF LEN(ISNULL(@vcDisplayColumns,'')) > 0
	BEGIN
		DECLARE @TempIDs TABLE
		(
			ID INT IDENTITY(1,1),
			vcFieldID VARCHAR(300)
		)

		INSERT INTO @TempIDs
		(
			vcFieldID
		)
		SELECT 
			OutParam 
		FROM 
			SplitString(@vcDisplayColumns,',')

		INSERT INTO @TEMPSelectedColumns
		(
			numFieldID,
			bitCustomField,
			tintOrder,
			intColumnWidth
		)
		SELECT
			numFormFieldID
			,bitCustomField
			,(SELECT (ID-1) FROM @TempIDs T3 WHERE T3.vcFieldID = TEMP.vcFieldID)
			,0
		FROM
		(
			SELECT  
				numFieldID as numFormFieldID,
				0 bitCustomField,
				CONCAT(numFieldID,'~0') vcFieldID
			FROM    
				View_DynamicDefaultColumns
			WHERE   
				numFormID = 17
				AND bitInResults = 1
				AND bitDeleted = 0 
				AND numDomainID = @numDomainID
			UNION 
			SELECT  
				c.Fld_id AS numFormFieldID
				,1 bitCustomField
				,CONCAT(Fld_id,'~1') vcFieldID
			FROM    
				CFW_Fld_Master C 
			LEFT JOIN 
				CFW_Validation V ON V.numFieldID = C.Fld_id
			JOIN 
				CFW_Loc_Master L on C.GRP_ID=L.Loc_Id
			WHERE   
				C.numDomainID = @numDomainID
				AND GRP_ID IN (SELECT ID FROM dbo.SplitIDs(@vcLocationID,','))
		) TEMP
		WHERE
			vcFieldID IN (SELECT vcFieldID FROM @TempIDs)
	END

	-- IF USER SELECTED DISPLAY COLUMNS IS NOT AVAILABLE THAN GET COLUMNS CONFIGURED FROM SEARCH RESULT GRID
	IF (SELECT COUNT(*) FROM @TEMPSelectedColumns) = 0
	BEGIN
		INSERT INTO @TEMPSelectedColumns
		(
			numFieldID,
			bitCustomField,
			tintOrder,
			intColumnWidth
		)
		SELECT
			numFormFieldID
			,bitCustomField
			,tintOrder
			,intColumnWidth
		FROM
		(
			SELECT  
				A.numFormFieldID,
				0 bitCustomField,
				A.tintOrder,
				A.intColumnWidth
			FROM    AdvSerViewConf A
					JOIN View_DynamicDefaultColumns D ON D.numFieldID = A.numFormFieldID
			WHERE   A.numFormID = 17
					AND D.bitInResults = 1
					AND D.bitDeleted = 0
					AND D.numDomainID = @numDomainID 
					AND D.numFormID = 17
					AND A.numDomainID = @numDomainID
					AND A.numUserCntID=@numUserCntID
					AND ISNULL(A.bitCustom,0) = 0
			UNION
			SELECT  
				A.numFormFieldID,
				1 bitCustomField,
				A.tintOrder,
				A.intColumnWidth
			FROM    AdvSerViewConf A
					JOIN CFW_Fld_Master C ON C.Fld_id = A.numFormFieldID
			WHERE   A.numFormID = 17
					AND C.numDomainID = @numDomainID 
					AND A.numDomainID = @numDomainID
					AND A.numUserCntID=@numUserCntID
					AND ISNULL(A.bitCustom,0) = 1
		) T1
		ORDER BY
			 tintOrder
	END

	-- IF USER HASN'T CONFIGURED COLUMNS FROM SEARCH RESULT GRID THEN RETURN DEFAULT COLUMNS
	IF (SELECT COUNT(*) FROM @TEMPSelectedColumns) = 0
	BEGIN
		INSERT INTO @TEMPSelectedColumns
		(
			numFieldID,
			bitCustomField,
			tintOrder,
			intColumnWidth
		)
		SELECT  
			numFieldID,
			0,
			1,
			0
		FROM    
			View_DynamicDefaultColumns
		WHERE   
			numFormID = 17
			AND numDomainID = @numDomainID
			AND numFieldID = 127
	END                                

DECLARE @bitCustom BIT
DECLARE @numFieldGroupID AS INT
DECLARE @vcColumnName AS VARCHAR(200)
DECLARE @numFieldID AS NUMERIC(18,0)

	SELECT TOP 1
		@tintOrder=tintOrder,
		@numFieldID=numFormFieldID,
		@vcDbColumnName=vcDbColumnName,
		@vcFormFieldName=vcFieldName,
		@vcAssociatedControlType=vcAssociatedControlType,
		@vcListItemType=vcListItemType,
		@numListID=numListID,
		@Table=vcLookBackTableName,
		@bitCustom=bitCustom,
		@numFieldGroupID=numGroupID
	FROM
	(
		SELECT
			T1.tintOrder+1 tintOrder,
			T1.numFieldID AS numFormFieldID,
			vcDbColumnName,
			vcFieldName,
			vcAssociatedControlType,
			vcListItemType,
			numListID,
			vcLookBackTableName,
			0 AS bitCustom,
			0 AS numGroupID
		FROM 
			View_DynamicDefaultColumns D
		INNER JOIN
			@TEMPSelectedColumns T1                            
		ON 
			D.numFieldId=T1.numFieldID                         
		WHERE 
			D.numDomainID = @numDomainID 
			AND D.numFormID = 17
			AND ISNULL(T1.bitCustomField,0) = 0
		UNION
		SELECT
			T1.tintOrder+1 tintOrder,
			Fld_id AS numFormFieldID,
			CONCAT('Cust',Fld_id),
			Fld_label,
			c.Fld_type,
			'',
			c.numlistid,
			'',
			1 AS bitCustom,
			Grp_id
		FROM    
			CFW_Fld_Master C 
		INNER JOIN
			@TEMPSelectedColumns T1                            
		ON 
			C.Fld_id=T1.numFieldID	
		WHERE   
			C.numDomainID = @numDomainID
			AND ISNULL(T1.bitCustomField,0) = 1
	) T1
	ORDER BY 
		tintOrder asc  


while @tintOrder>0                                    
begin                                    
                               
	IF @Table = 'AdditionalContactsInformation' 
        SET @Prefix = 'ADC.'
	IF @Table = 'DivisionMaster' 
        SET @Prefix = 'DM.'
    IF @Table = 'CompanyInfo' 
        SET @Prefix = 'C.'
    IF @Table = 'Cases' 
        SET @Prefix = 'Cases.'

	IF @bitCustom = 0
    BEGIN           
		IF @vcAssociatedControlType='SelectBox'                                    
        BEGIN                                                                           
			IF @vcListItemType='LI'                                     
			BEGIN
				SET @strSql=@strSql+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcFormFieldName+'~'+ @vcDbColumnName+']'                                    
				SET @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID='+@vcDbColumnName                                    
			END                                    
			ELSE IF @vcListItemType='U'                                                     
			BEGIN           
				SET @strSql=@strSql+',dbo.fn_GetContactName(' + @Prefix + @vcDbColumnName+') ['+ @vcFormFieldName+'~'+ @vcDbColumnName+']'
			END                                    
		END                                    
		ELSE 
			SET @strSql = @strSql +',' + CASE 
											WHEN @vcDbColumnName='numAge' THEN 'year(getutcdate())-year(bintDOB)'     
											WHEN @vcDbColumnName='numDivisionID' THEN 'DM.numDivisionID'   
											WHEN @vcDbColumnName='vcProgress' THEN ' dbo.fn_OppTotalProgress(OppMas.numOppId)'  
											WHEN @vcDbColumnName='intTargetResolveDate' OR @vcDbColumnName='dtDateEntered' OR @vcDbColumnName='bintShippedDate' OR @vcDbColumnName='bintCreatedDate' OR @vcDbColumnName='bintModifiedDate' THEN 'dbo.FormatedDateFromDate('+ @Prefix + @vcDbColumnName +','+convert(varchar(10),@numDomainId)+')'    
											ELSE @vcDbColumnName 
										END + ' ['+ @vcFormFieldName+'~'+ @vcDbColumnName+']'                 
    END
	ELSE IF @bitCustom = 1
	BEGIN
		SET @vcColumnName= CONCAT(@vcFormFieldName,'~','Cust',@numFieldId)
			
		IF @vcAssociatedControlType = 'TextBox' or @vcAssociatedControlType = 'TextArea'            
		BEGIN
			SET @strSql= @strSql+',CFW'+ convert(VARCHAR(3),@tintOrder)+'.Fld_Value  ['+ @vcColumnName +']'                   
				
			SET @WhereCondition= @WhereCondition 
								+' left Join CFW_FLD_Values_Case CFW'+ convert(varchar(3),@tintOrder)
								+ ' on CFW' + convert(varchar(3),@tintOrder) 
								+ '.Fld_Id='+convert(varchar(10),@numFieldId) 
								+ 'and CFW' + convert(varchar(3),@tintOrder) 
								+ '.RecId=Cases.numCaseId '                                                         
		END   
		ELSE IF @vcAssociatedControlType = 'CheckBox'       
		BEGIN
			set @strSql= @strSql+',case when isnull(CFW' + CONVERT(VARCHAR(3), @tintOrder)+ '.Fld_Value,0)='''' then 0 else  isnull(CFW'
											+ CONVERT(VARCHAR(3), @tintOrder)+ '.Fld_Value,0) end ['+ @vcColumnName + ']'
 
			set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values_Case CFW'+ convert(varchar(3),@tintOrder)+ '             
			on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=Cases.numCaseId '                                                     
		END                
		ELSE IF @vcAssociatedControlType = 'DateField'            
		BEGIN
			set @strSql= @strSql+',dbo.FormatedDateFromDate(CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value,'+convert(varchar(10),@numDomainId)+')  ['+ @vcColumnName +']'                   
			set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values_Case CFW'+ convert(varchar(3),@tintOrder)+ '                 
			on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=Cases.numCaseId '
		END                
		ELSE IF @vcAssociatedControlType = 'SelectBox'             
		BEGIN
			set @vcDbColumnName = 'DCust'+Convert(varchar(10),@numFieldId)                
			set @strSql=@strSql+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName +']'                                                          
			set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values_Case CFW'+ convert(varchar(3),@tintOrder)+ '                 
				on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=Cases.numCaseId '
			set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID=CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value'                
		END         
		ELSE IF @vcAssociatedControlType = 'CheckBoxList'
		BEGIN
			SET @strSql= @strSql+',(SELECT STUFF((SELECT CONCAT('', '', vcData) FROM ListDetails WHERE numListID= ' + CAST(@numListID AS varchar) + ' AND numlistitemid IN (SELECT ISNULL(Id,0) FROM dbo.SplitIDs(CFW'+ convert(VARCHAR(3),@tintOrder)+'.Fld_Value,'','')) FOR XML PATH('''')), 1, 1, ''''))  ['+ @vcColumnName +']'   

			SET @WhereCondition= @WhereCondition 
								+' left Join CFW_FLD_Values_Case CFW'+ convert(varchar(3),@tintOrder)
								+ ' on CFW' + convert(varchar(3),@tintOrder) 
								+ '.Fld_Id='+convert(varchar(10),@numFieldId) 
								+ 'and CFW' + convert(varchar(3),@tintOrder) 
								+ '.RecId=Cases.numCaseId '
		END
	
	END 
        
	SELECT TOP 1
		@tintOrder=tintOrder,
		@numFieldID=numFormFieldID,
		@vcDbColumnName=vcDbColumnName,
		@vcFormFieldName=vcFieldName,
		@vcAssociatedControlType=vcAssociatedControlType,
		@vcListItemType=vcListItemType,
		@numListID=numListID,
		@Table=vcLookBackTableName,
		@bitCustom=bitCustom,
		@numFieldGroupID=numGroupID
	FROM
	(
		SELECT
			T1.tintOrder+1 tintOrder,
			T1.numFieldID AS numFormFieldID,
			vcDbColumnName,
			vcFieldName,
			vcAssociatedControlType,
			vcListItemType,
			numListID,
			vcLookBackTableName,
			0 AS bitCustom,
			0 AS numGroupID
		FROM 
			View_DynamicDefaultColumns D
		INNER JOIN
			@TEMPSelectedColumns T1                            
		ON 
			D.numFieldId=T1.numFieldID                         
		WHERE 
			D.numDomainID = @numDomainID 
			AND D.numFormID = 17
			AND ISNULL(T1.bitCustomField,0) = 0
			AND T1.tintOrder > @tintOrder-1
		UNION
		SELECT
			T1.tintOrder+1 tintOrder,
			Fld_id AS numFormFieldID,
			CONCAT('Cust',Fld_id),
			Fld_label,
			c.Fld_type,
			'',
			c.numlistid,
			'',
			1 AS bitCustom,
			Grp_id
		FROM    
			CFW_Fld_Master C 
		INNER JOIN
			@TEMPSelectedColumns T1                            
		ON 
			C.Fld_id=T1.numFieldID	
		WHERE   
			C.numDomainID = @numDomainID
			AND ISNULL(T1.bitCustomField,0) = 1
			AND T1.tintOrder > @tintOrder-1
	) T1
	ORDER BY 
		tintOrder asc      
		                              
 if @@rowcount=0 set @tintOrder=0                                    
end     
  declare @firstRec as integer                                                                          
  declare @lastRec as integer                                                                          
 set @firstRec= (@CurrentPage-1) * @PageSize                            
 set @lastRec= (@CurrentPage*@PageSize+1)                                                                           
set @TotRecs=(select count(*) from #tempTable)     
set @strSql=@strSql+'from Cases      
Join AdditionalContactsinformation ADC on ADC.numContactId = Cases.numContactId    
join DivisionMaster DM on Dm.numDivisionId = Cases.numDivisionId    
JOIN CompanyInfo C ON DM.numCompanyID = C.numCompanyId '+@WhereCondition+'    
    
 join #tempTable T on T.numCaseID=Cases.numCaseID     
 WHERE ID > '+convert(varchar(10),@firstRec)+ ' and ID <'+ convert(varchar(10),@lastRec)                        
 print @strSql                                                            
exec(@strSql)                                                                       
drop table #tempTable
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_AdvancedSearchGetModuleAndFields')
DROP PROCEDURE dbo.USP_AdvancedSearchGetModuleAndFields
GO
CREATE PROCEDURE [dbo].[USP_AdvancedSearchGetModuleAndFields]
(
	@numDomainID NUMERIC(18,0)
	,@numUserCntID NUMERIC(18,0)
)
AS
BEGIN
	DECLARE @numAuthGroupID NUMERIC(18,0)
	SELECT @numAuthGroupID=numGroupID FROM UserMaster WHERE numUserDetailId = @numUserCntID

	DECLARE @TEMP TABLE
	(
		numFormID NUMERIC(18,0)
		,vcFormName VARCHAR(300)
	)

	INSERT INTO @TEMP (numFormID,vcFormName) VALUES (1,'Organizations & Contacts')

	IF EXISTS (SELECT numGroupID FROM GroupAuthorization WHERE numModuleID=9 AND numPageID=7 AND numGroupID=@numAuthGroupID AND ISNULL(intViewAllowed,0) <> 0)
	BEGIN
		INSERT INTO @TEMP (numFormID,vcFormName) VALUES (15,'Opportunities & Orders (with items)')
	END

	IF EXISTS (SELECT numGroupID FROM GroupAuthorization WHERE numModuleID=9 AND numPageID=11 AND numGroupID=@numAuthGroupID AND ISNULL(intViewAllowed,0) <> 0)
	BEGIN
		INSERT INTO @TEMP (numFormID,vcFormName) VALUES (29,'Items')
	END

	IF EXISTS (SELECT numGroupID FROM GroupAuthorization WHERE numModuleID=9 AND numPageID=8 AND numGroupID=@numAuthGroupID AND ISNULL(intViewAllowed,0) <> 0)
	BEGIN
		INSERT INTO @TEMP (numFormID,vcFormName) VALUES (17,'Cases')
	END

	IF EXISTS (SELECT numGroupID FROM GroupAuthorization WHERE numModuleID=9 AND numPageID=8 AND numGroupID=@numAuthGroupID AND ISNULL(intViewAllowed,0) <> 0)
	BEGIN
		INSERT INTO @TEMP (numFormID,vcFormName) VALUES (18,'Projects')
	END

	IF EXISTS (SELECT numGroupID FROM GroupAuthorization WHERE numModuleID=9 AND numPageID=12 AND numGroupID=@numAuthGroupID AND ISNULL(intViewAllowed,0) <> 0)
	BEGIN
		INSERT INTO @TEMP (numFormID,vcFormName) VALUES (59,'Financial Transactions')
	END

	SELECT * FROM @TEMP
	
	SELECT
		*
	FROM
	(
		SELECT
			DFCD.numFormId
			,DFCD.numFieldId
			,DFFm.vcFieldName
			,DFFM.vcAssociatedControlType
			,0 AS bitCustomField
			,DFM.numListID
			,DFM.vcListItemType
			,DFM.vcLookBackTableName
			,DFM.vcDbColumnName
			,'R' AS vcFieldType
		FROM
			DycFormConfigurationDetails DFCD
		INNER JOIN 
			DycFormField_Mapping DFFM
		ON
			DFCD.numFieldId = DFFM.numFieldId
			AND DFCD.numFormId=DFFM.numFormID
		INNER JOIN
			DycFieldMaster DFM
		ON
			DFCD.numFieldId = DFM.numFieldId
		WHERE
			DFCD.numDomainId=@numDomainID
			AND numAuthGroupID=@numAuthGroupID
			AND DFCD.numFormId IN (SELECT numFormID FROm @TEMP)
		UNION
		SELECT
			DFCD.numFormId
			,DFCD.numFieldId
			,CFW.Fld_label AS vcFieldName
			,CFW.Fld_type AS vcAssociatedControlType
			,1 AS bitCustomField
			,CFW.numlistid
			,(CASE WHEN ISNULL(CFW.numlistid,0) > 0 THEN 'LI' ELSE '' END) vcListItemType
			,'' AS vcLookBackTableName
			,CONVERT(VARCHAR(15), DFCD.numFieldId) + '_C' vcDbColumnName
			,CASE CLM.Loc_id WHEN 3 THEN 'CA' WHEN 5 THEN 'I' WHEN 9 THEN 'IA' WHEN 11 THEN 'P' ELSE CLM.vcFieldType END vcFieldType
		FROM
			DycFormConfigurationDetails DFCD
		INNER JOIN 
			CFW_Fld_Master CFW
		ON
			DFCD.numFieldId = CFW.Fld_id
		INNER JOIN 
			CFW_Loc_Master CLM
		ON
			CFW.GRP_ID=CLM.Loc_Id
		WHERE
			DFCD.numDomainId=@numDomainID
			AND numAuthGroupID=@numAuthGroupID
			AND DFCD.bitCustom=1
			AND DFCD.numFormId IN (SELECT numFormID FROm @TEMP)
	) TEMP
	ORDER BY
		numFormId
		,vcFieldName
END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_AdvancedSearchItems')
DROP PROCEDURE USP_AdvancedSearchItems
GO
CREATE PROCEDURE USP_AdvancedSearchItems
@WhereCondition as varchar(4000)='',                            
@numDomainID as numeric(9)=0,
@numUserCntID NUMERIC(18,0),                         
@CurrentPage int,
@PageSize int,                                                                    
@TotRecs int output,                                                                                                             
@columnSortOrder as Varchar(10),                              
@ColumnSearch as varchar(100)='',                           
@ColumnName as varchar(50)='',                             
@GetAll as bit,                              
@SortCharacter as char(1),                        
@SortColumnName as varchar(50)='',        
@bitMassUpdate as bit,        
@LookTable as varchar(10)='' ,        
@strMassUpdate as varchar(2000)='',
@vcDisplayColumns VARCHAR(MAX)
AS 
BEGIN
 
 
   
IF (@SortCharacter <> '0' AND @SortCharacter <> '')
  SET @ColumnSearch = @SortCharacter
CREATE TABLE #temptable
  (
  id           INT   IDENTITY   PRIMARY KEY,
  numItemID VARCHAR(15))
  
------------Declaration---------------------  
DECLARE  @strSql           VARCHAR(8000),
         @numMaxFieldId NUMERIC(9),
         @from         NVARCHAR(1000),
         @Where         NVARCHAR(4000),
         @OrderBy       NVARCHAR(1000),
         @GroupBy       NVARCHAR(1000),
         @SelectFields   NVARCHAR(4000),
         @InneJoinOn    NVARCHAR(1000)
         
 DECLARE @tintOrder AS TINYINT,
		 @vcFormFieldName AS VARCHAR(50),
		 @vcListItemType AS VARCHAR(3),
		 @vcListItemType1 AS VARCHAR(3),
		 @vcAssociatedControlType VARCHAR(20),
		 @numListID AS NUMERIC(9),
		 @vcDbColumnName VARCHAR(30),
		 @vcLookBackTableName VARCHAR(50),
		 @WCondition VARCHAR(1000)
SET @InneJoinOn = ''
SET @OrderBy = ''
SET @Where = ''
SET @from = ' FROM 
				Item I 
			LEFT JOIN dbo.Vendor V ON I.numDomainID = V.numDomainID AND I.numItemCode = V.numItemCode AND I.numVendorID = V.numVendorID
			LEFT JOIN DivisionMaster DM ON V.numVendorID = DM.numDivisionID 
			LEFt JOIN AdditionalContactsinformation ADC on ADC.numDivisionID = DM.numDomainID AND ISNULL(ADC.bitPrimaryContact,0)=1
			LEFT JOIN CompanyInfo C ON DM.numCompanyID = C.numCompanyID
			OUTER APPLY
			(
				SELECT
					SUM(numOnHand) AS numOnHand,
					SUM(numBackOrder) AS numBackOrder,
					SUM(numOnOrder) AS numOnOrder,
					SUM(numAllocation) AS numAllocation,
					SUM(numReorder) AS numReorder
				FROM
					WareHouseItems
				WHERE
					numItemID = I.numItemCode
			) WI '
SET @GroupBy = ''
SET @SelectFields = ''  
SET @WCondition = ''


	
--	SELECT I.numItemCode,* FROM item I 
--	LEFT JOIN ItemGroups IG ON I.numDomainID = IG.numDomainID AND I.numItemGroup = IG.numItemGroupID
--	LEFT JOIN dbo.Vendor V ON I.numDomainID = V.numDomainID AND I.numItemCode = V.numItemCode --AND I.numVendorID = V.numVendorID
	
	
SET @Where = @Where + ' WHERE I.charItemType <>''A'' and I.numDomainID=' + convert(varchar(15),@numDomainID)
if (@ColumnSearch<>'') SET @Where = @Where + ' and I.vcItemName like ''' + @ColumnSearch + '%'''

CREATE TABLE #temp(ItemID INT PRIMARY KEY)
IF CHARINDEX('SplitString',@WhereCondition) > 0
	BEGIN
		DECLARE @strTemp AS NVARCHAR(MAX)
		SET @WhereCondition	= REPLACE(@WhereCondition,' AND I.numItemCode IN ','')
		SET @WhereCondition = RIGHT(@WhereCondition,LEN(@WhereCondition) - 1)
		SET @WhereCondition = LEFT(@WhereCondition,LEN(@WhereCondition) - 1)
		
		SET @strTemp = 'INSERT INTO #temp ' + @WhereCondition	
		PRINT @strTemp
		EXEC SP_EXECUTESQL @strTemp
		
		set @InneJoinOn= @InneJoinOn + ' JOIN #temp SP ON SP.ItemID = I.numItemCode '
	END
ELSE
	BEGIN
		SET @Where = @Where + @WhereCondition	
	END

	
	
	
set @tintOrder=0
set @WhereCondition =''
set @strSql='SELECT  I.numItemCode '

	DECLARE @TEMPSelectedColumns TABLE
	(
		numFieldID NUMERIC(18,0)
		,bitCustomField BIT
		,tintOrder INT
		,intColumnWidth FLOAT
	)
	DECLARE @vcLocationID VARCHAR(100) = '0'
	SELECT @vcLocationID =isnull(vcLocationID,'0') from dynamicFormMaster where numFormID=29   

	-- IF USER HAS SELECTED ITS OWN DISPLAY COLUMNS THEN USE IT
	IF LEN(ISNULL(@vcDisplayColumns,'')) > 0
	BEGIN
		DECLARE @TempIDs TABLE
		(
			ID INT IDENTITY(1,1),
			vcFieldID VARCHAR(300)
		)

		INSERT INTO @TempIDs
		(
			vcFieldID
		)
		SELECT 
			OutParam 
		FROM 
			SplitString(@vcDisplayColumns,',')

		INSERT INTO @TEMPSelectedColumns
		(
			numFieldID,
			bitCustomField,
			tintOrder,
			intColumnWidth
		)
		SELECT
			numFormFieldID
			,bitCustomField
			,(SELECT (ID-1) FROM @TempIDs T3 WHERE T3.vcFieldID = TEMP.vcFieldID)
			,0
		FROM
		(
			SELECT  
				numFieldID as numFormFieldID,
				0 bitCustomField,
				CONCAT(numFieldID,'~0') vcFieldID
			FROM    
				View_DynamicDefaultColumns
			WHERE   
				numFormID = 29
				AND bitInResults = 1
				AND bitDeleted = 0 
				AND numDomainID = @numDomainID
			UNION 
			SELECT  
				c.Fld_id AS numFormFieldID
				,1 bitCustomField
				,CONCAT(Fld_id,'~1') vcFieldID
			FROM    
				CFW_Fld_Master C 
			LEFT JOIN 
				CFW_Validation V ON V.numFieldID = C.Fld_id
			JOIN 
				CFW_Loc_Master L on C.GRP_ID=L.Loc_Id
			WHERE   
				C.numDomainID = @numDomainID
				AND GRP_ID IN (SELECT ID FROM dbo.SplitIDs(@vcLocationID,','))
		) TEMP
		WHERE
			vcFieldID IN (SELECT vcFieldID FROM @TempIDs)
	END

	-- IF USER SELECTED DISPLAY COLUMNS IS NOT AVAILABLE THAN GET COLUMNS CONFIGURED FROM SEARCH RESULT GRID
	IF (SELECT COUNT(*) FROM @TEMPSelectedColumns) = 0
	BEGIN
		INSERT INTO @TEMPSelectedColumns
		(
			numFieldID,
			bitCustomField,
			tintOrder,
			intColumnWidth
		)
		SELECT
			numFormFieldID
			,bitCustomField
			,tintOrder
			,intColumnWidth
		FROM
		(
			SELECT  
				A.numFormFieldID,
				0 bitCustomField,
				A.tintOrder,
				A.intColumnWidth
			FROM    AdvSerViewConf A
					JOIN View_DynamicDefaultColumns D ON D.numFieldID = A.numFormFieldID
			WHERE   A.numFormID = 29
					AND D.bitInResults = 1
					AND D.bitDeleted = 0
					AND D.numDomainID = @numDomainID 
					AND D.numFormID = 29
					AND A.numDomainID = @numDomainID
					AND A.numUserCntID=@numUserCntID
					AND ISNULL(A.bitCustom,0) = 0
			UNION
			SELECT  
				A.numFormFieldID,
				1 bitCustomField,
				A.tintOrder,
				A.intColumnWidth
			FROM    AdvSerViewConf A
					JOIN CFW_Fld_Master C ON C.Fld_id = A.numFormFieldID
			WHERE   A.numFormID = 29
					AND C.numDomainID = @numDomainID 
					AND A.numDomainID = @numDomainID
					AND A.numUserCntID=@numUserCntID
					AND ISNULL(A.bitCustom,0) = 1
		) T1
		ORDER BY
			 tintOrder
	END

	-- IF USER HASN'T CONFIGURED COLUMNS FROM SEARCH RESULT GRID THEN RETURN DEFAULT COLUMNS
	IF (SELECT COUNT(*) FROM @TEMPSelectedColumns) = 0
	BEGIN
		INSERT INTO @TEMPSelectedColumns
		(
			numFieldID,
			bitCustomField,
			tintOrder,
			intColumnWidth
		)
		SELECT  
			numFieldID,
			0,
			1,
			0
		FROM    
			View_DynamicDefaultColumns
		WHERE   
			numFormID = 29
			AND numDomainID = @numDomainID
			AND numFieldID = 189
	END

DECLARE @Table AS VARCHAR(200)
DECLARE @bitCustom BIT
DECLARE @numFieldGroupID AS INT
DECLARE @vcColumnName AS VARCHAR(200)
DECLARE @numFieldID AS NUMERIC(18,0)

	SELECT TOP 1
		@tintOrder=tintOrder,
		@numFieldID=numFormFieldID,
		@vcDbColumnName=vcDbColumnName,
		@vcFormFieldName=vcFieldName,
		@vcAssociatedControlType=vcAssociatedControlType,
		@vcListItemType=vcListItemType,
		@numListID=numListID,
		@vcLookBackTableName=vcLookBackTableName,
		@bitCustom=bitCustom,
		@numFieldGroupID=numGroupID
	FROM
	(
		SELECT
			T1.tintOrder+1 tintOrder,
			T1.numFieldID AS numFormFieldID,
			vcDbColumnName,
			vcFieldName,
			vcAssociatedControlType,
			vcListItemType,
			numListID,
			vcLookBackTableName,
			0 AS bitCustom,
			0 AS numGroupID
		FROM 
			View_DynamicDefaultColumns D
		INNER JOIN
			@TEMPSelectedColumns T1                         
		ON 
			D.numFieldId=T1.numFieldID                          
		WHERE 
			D.numDomainID = @numDomainID 
			AND D.numFormID = 29 
			AND ISNULL(T1.bitCustomField,0) = 0
		UNION
		SELECT
			T1.tintOrder+1 tintOrder,
			Fld_id AS numFormFieldID,
			CONCAT('Cust',Fld_id),
			Fld_label,
			c.Fld_type,
			'',
			c.numlistid,
			'',
			1 AS bitCustom,
			Grp_id
		FROM    
			CFW_Fld_Master C 
		INNER JOIN
			@TEMPSelectedColumns T1
		ON 
			C.Fld_id = T1.numFieldID
		WHERE   
			C.numDomainID = @numDomainID
			AND ISNULL(T1.bitCustomField,0) = 1
	) T1
	ORDER BY 
		tintOrder asc
while @tintOrder>0
begin

	IF @bitCustom = 0
	BEGIN
		if @vcAssociatedControlType='SelectBox'                              
        begin                              
		  if @vcListItemType='IG' --Item group
		  begin                              
			   set @SelectFields=@SelectFields+',IG.vcItemGroup ['+  @vcFormFieldName + '~' + @vcDbColumnName+']'                              
			   set @InneJoinOn= @InneJoinOn +' LEFT JOIN ItemGroups IG ON I.numDomainID = IG.numDomainID AND I.numItemGroup = IG.numItemGroupID '
			   SET @GroupBy = @GroupBy + 'IG.vcItemGroup,'
		  end
		  else if @vcListItemType='PP'--Item Type
		  begin
			   set @SelectFields=@SelectFields+', CASE I.charItemType WHEN ''p'' THEN ''Inventory Item'' WHEN ''n'' THEN  ''Non-Inventory Item'' WHEN ''s'' THEN ''Service'' ELSE '''' END   ['+  @vcFormFieldName + '~' + @vcDbColumnName+']'                              
			   SET @GroupBy = @GroupBy + 'I.charItemType,'
		  END
		  else if @vcListItemType='UOM'--Unit of Measure
		  BEGIN
		  		SET @SelectFields = @SelectFields + ' ,dbo.fn_GetUOMName(I.'+ @vcDbColumnName +') ['+  @vcFormFieldName + '~' + @vcDbColumnName+']'
				SET @GroupBy = @GroupBy + 'I.' + @vcDbColumnName + ','
		  END
		  else if @vcListItemType='LI'--Any List Item
		  BEGIN
			IF @vcDbColumnName='vcUnitofMeasure'
			BEGIN 		
				SET @SelectFields = @SelectFields + ' ,I.'+ @vcDbColumnName +' ['+  @vcFormFieldName + '~' + @vcDbColumnName+']'
				SET @GroupBy = @GroupBy + 'I.' + @vcDbColumnName + ','
			END 
			ELSE 
				BEGIN
					set @SelectFields=@SelectFields+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+  @vcFormFieldName + '~' + @vcDbColumnName+']'                              
					set @InneJoinOn= @InneJoinOn +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID='+@vcDbColumnName                              
					SET @GroupBy = @GroupBy + 'L' + convert(varchar(3),@tintOrder)+'.vcData,'
					IF @SortColumnName ='numItemClassification'
						SET @SortColumnName = 'L'+ convert(varchar(3),@tintOrder)+'.vcData'
					
				END
			END                              
			
		END
		ELSE if @vcAssociatedControlType='ListBox'
		BEGIN
	 		if @vcListItemType='V'--Vendor
			BEGIN
				set @SelectFields=@SelectFields+', dbo.fn_GetComapnyName(I.numVendorID)  ['+  @vcFormFieldName + '~' + @vcDbColumnName+']'
				SET @GroupBy = @GroupBy + 'I.' + @vcDbColumnName + ','
			END
		END
		ELSE
		BEGIN
			IF @vcLookBackTableName ='WareHouseItems'
			BEGIN
				IF @vcDbColumnName = 'vcBarCode'
				BEGIN
					SET @SelectFields = @SelectFields + ', STUFF((SELECT N'', '' + vcBarCode FROM WareHouseItems WHERE numDomainID = ' + convert(varchar(15),@numDomainID) + ' AND numItemID = I.numItemCode FOR XML PATH ('''')), 1, 2, '''') ['+  @vcFormFieldName + '~' + @vcDbColumnName+']'
				END
				ELSE IF @vcDbColumnName = 'vcWHSKU'
				BEGIN
					SET @SelectFields = @SelectFields + ', STUFF((SELECT N'', '' + vcWHSKU FROM WareHouseItems WHERE numDomainID = ' + convert(varchar(15),@numDomainID) + ' AND numItemID = I.numItemCode FOR XML PATH ('''')), 1, 2, '''') ['+  @vcFormFieldName + '~' + @vcDbColumnName+']'
				END
				ELSE
				BEGIN
		 			SET @SelectFields = @SelectFields + ' ,WI.'+ @vcDbColumnName +' ['+  @vcFormFieldName + '~' + @vcDbColumnName+']'
		 		END
			END
			else IF @vcDbColumnName='vcSerialNo'
			BEGIN
				SET @SelectFields = @SelectFields + ' ,0 ['+  @vcFormFieldName + '~' + @vcDbColumnName+']'
			END
			ELSE IF @vcDbColumnName='vcPartNo'
			BEGIN
				SET @SelectFields = @SelectFields + ' ,(SELECT top 1 ISNULL(vcPartNo,'''') FROM dbo.Vendor V1 WHERE V1.numVendorID =I.numVendorID) as  ['+  @vcFormFieldName + '~' + @vcDbColumnName+']'
			END
			ELSE 
			BEGIN
		  		SET @SelectFields = @SelectFields + ' ,I.'+ @vcDbColumnName +' ['+ @vcFormFieldName + '~' + @vcDbColumnName+']'
		  		SET @GroupBy = @GroupBy + 'I.' + @vcDbColumnName + ','
			END
		   
		END
	END
	ELSE IF @bitCustom = 1
	BEGIN
		SET @vcColumnName= CONCAT(@vcFormFieldName,'~','Cust',@numFieldId)
			
		IF @vcAssociatedControlType = 'TextBox' or @vcAssociatedControlType = 'TextArea'            
		BEGIN
			SET @SelectFields= @SelectFields+',CFW'+ convert(VARCHAR(3),@tintOrder)+'.Fld_Value  ['+ @vcColumnName +']'                   
				
			SET @InneJoinOn= @InneJoinOn 
								+' left Join CFW_FLD_Values_Item CFW'+ convert(varchar(3),@tintOrder)
								+ ' on CFW' + convert(varchar(3),@tintOrder) 
								+ '.Fld_Id='+convert(varchar(10),@numFieldId) 
								+ 'and CFW' + convert(varchar(3),@tintOrder) 
								+ '.RecId=I.numItemCode '                                                         
		END   
		ELSE IF @vcAssociatedControlType = 'CheckBox'       
		BEGIN
			set @SelectFields= @SelectFields+',case when isnull(CFW' + CONVERT(VARCHAR(3), @tintOrder)+ '.Fld_Value,0)='''' then 0 else  isnull(CFW'
											+ CONVERT(VARCHAR(3), @tintOrder)+ '.Fld_Value,0) end ['+ @vcColumnName + ']'
 
			set @InneJoinOn= @InneJoinOn +' left Join CFW_FLD_Values_Item CFW'+ convert(varchar(3),@tintOrder)+ '             
			on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=I.numItemCode '                                                     
		END                
		ELSE IF @vcAssociatedControlType = 'DateField'            
		BEGIN
			set @SelectFields= @SelectFields+',dbo.FormatedDateFromDate(CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value,'+convert(varchar(10),@numDomainId)+')  ['+ @vcColumnName +']'                   
			set @InneJoinOn= @InneJoinOn +' left Join CFW_FLD_Values_Item CFW'+ convert(varchar(3),@tintOrder)+ '                 
			on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=I.numItemCode '
		END                
		ELSE IF @vcAssociatedControlType = 'SelectBox'             
		BEGIN
			set @vcDbColumnName = 'DCust'+Convert(varchar(10),@numFieldId)                
			set @SelectFields=@SelectFields+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName +']'                                                          
			set @InneJoinOn= @InneJoinOn +' left Join CFW_FLD_Values_Item CFW'+ convert(varchar(3),@tintOrder)+ '                 
				on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=I.numItemCode '
			set @InneJoinOn= @InneJoinOn +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID=CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value'                
		END         
		ELSE IF @vcAssociatedControlType = 'CheckBoxList'
		BEGIN
			SET @SelectFields= @SelectFields+',(SELECT STUFF((SELECT CONCAT('', '', vcData) FROM ListDetails WHERE numListID= ' + CAST(@numListID AS varchar) + ' AND numlistitemid IN (SELECT ISNULL(Id,0) FROM dbo.SplitIDs(CFW'+ convert(VARCHAR(3),@tintOrder)+'.Fld_Value,'','')) FOR XML PATH('''')), 1, 1, ''''))  ['+ @vcColumnName +']'   

			SET @InneJoinOn= @InneJoinOn 
								+' left Join CFW_FLD_Values_Item CFW'+ convert(varchar(3),@tintOrder)
								+ ' on CFW' + convert(varchar(3),@tintOrder) 
								+ '.Fld_Id='+convert(varchar(10),@numFieldId) 
								+ 'and CFW' + convert(varchar(3),@tintOrder) 
								+ '.RecId=I.numItemCode '
		END

	END
               
                                      
	SELECT TOP 1
		@tintOrder=tintOrder,
		@numFieldID=numFormFieldID,
		@vcDbColumnName=vcDbColumnName,
		@vcFormFieldName=vcFieldName,
		@vcAssociatedControlType=vcAssociatedControlType,
		@vcListItemType=vcListItemType,
		@numListID=numListID,
		@vcLookBackTableName=vcLookBackTableName,
		@bitCustom=bitCustom,
		@numFieldGroupID=numGroupID
	FROM
	(
		SELECT
			T1.tintOrder+1 tintOrder,
			T1.numFieldID AS numFormFieldID,
			vcDbColumnName,
			vcFieldName,
			vcAssociatedControlType,
			vcListItemType,
			numListID,
			vcLookBackTableName,
			0 AS bitCustom,
			0 AS numGroupID
		FROM 
			View_DynamicDefaultColumns D
		INNER JOIN
			@TEMPSelectedColumns T1                         
		ON 
			D.numFieldId=T1.numFieldID                          
		WHERE 
			D.numDomainID = @numDomainID 
			AND D.numFormID = 29 
			AND ISNULL(T1.bitCustomField,0) = 0
			AND T1.tintOrder > @tintOrder-1
		UNION
		SELECT
			T1.tintOrder+1 tintOrder,
			Fld_id AS numFormFieldID,
			CONCAT('Cust',Fld_id),
			Fld_label,
			c.Fld_type,
			'',
			c.numlistid,
			'',
			1 AS bitCustom,
			Grp_id
		FROM    
			CFW_Fld_Master C 
		INNER JOIN
			@TEMPSelectedColumns T1
		ON 
			C.Fld_id = T1.numFieldID
		WHERE   
			C.numDomainID = @numDomainID
			AND ISNULL(T1.bitCustomField,0) = 1
			AND T1.tintOrder > @tintOrder-1
	) T1
	ORDER BY 
		tintOrder asc     
                            
 if @@rowcount=0 set @tintOrder=0                              
end                              

/*--------------Sorting logic-----------*/
	
	IF @SortColumnName=''
		SET @SortColumnName = 'vcItemName'
	ELSE IF @SortColumnName ='numItemGroup'
		SET @SortColumnName = 'IG.vcItemGroup'
	ELSE IF @SortColumnName ='charItemType'
		SET @SortColumnName = 'I.charItemType'
	ELSE IF @SortColumnName = 'vcPartNo'
		SET @SortColumnName = 'vcItemName'
		
	 
SET @OrderBy = ' ORDER BY ' + @SortColumnName + ' '+ @columnSortOrder	
	
/*--------------Sorting logic ends-----------*/
	
	

	
	SET @strSql =  @strSql /*+ @SelectFields */+ @from + @InneJoinOn + @Where + @OrderBy
	PRINT @strSql
	INSERT INTO #temptable (
		numItemID
	) EXEC(@strSql)

/*-----------------------------Mass Update-----------------------------*/
if @bitMassUpdate=1
BEGIN
	IF CHARINDEX('USP_ItemCategory_MassUpdate',@strMassUpdate) >0
	BEGIN
		declare @vcItemCodes VARCHAR(MAX)
		SET @vcItemCodes = ''
		select @vcItemCodes = @vcItemCodes + numItemID + ', ' from #tempTable

		SET @strMassUpdate = REPLACE(@strMassUpdate,'##ItemCodes##',SUBSTRING(@vcItemCodes, 0, LEN(@vcItemCodes)))

		PRINT @strMassUpdate
		exec (@strMassUpdate)
	END
	ELSE
	BEGIN
		PRINT @strMassUpdate
		exec (@strMassUpdate)
	END
END

	
/*-----------------------------Paging Logic-----------------------------*/
	declare @firstRec as integer
	declare @lastRec as integer
	set @firstRec = ( @CurrentPage - 1 ) * @PageSize
	set @lastRec = ( @CurrentPage * @PageSize + 1 )
	set @TotRecs=(select count(*) from #tempTable)
	
	SET @InneJoinOn =@InneJoinOn+ ' join #temptable T on T.numItemID = I.numItemCode '
	SET @strSql = 'SELECT distinct I.numItemCode  [numItemCode],T.ID [ID~ID],isnull(I.bitSerialized,0) as [bitSerialized],isnull(I.bitLotNo,0) as [bitLotNo] ' +  @SelectFields + @from + @InneJoinOn + @Where 
	IF @GetAll=0
	BEGIN
		SET @strSql = @strSql +' and T.ID > '+convert(varchar(10),@firstRec)+ ' and T.ID <'+ convert(varchar(10),@lastRec) 
	END
	
	-- enable or disble group by based on search result view
	IF CHARINDEX('dbo.WareHouseItems WI',@InneJoinOn)=0
		SET @strSql = @strSql + ' order by ID; '
	ELSE 
		SET @strSql = @strSql + ' GROUP BY I.numItemCode,T.ID,I.bitSerialized,I.bitLotNo,I.numVendorID,' + SUBSTRING(@GroupBy, 1, LEN(@GroupBy)-1);
	
	
	EXEC (@strSql)
	PRINT @strSql
	
	DROP TABLE #temptable;
-------------------------------------------------	


	SELECT
		vcDbColumnName,
		vcFormFieldName,
		tintOrder AS tintOrder,
		bitAllowSorting
	FROM
	(
		SELECT
			D.vcDbColumnName,
			D.vcFieldName AS vcFormFieldName,
			T1.tintOrder AS tintOrder,
			isnull(D.bitAllowSorting,0) AS bitAllowSorting
		FROM 
			View_DynamicDefaultColumns D                               
		INNER JOIN
			@TEMPSelectedColumns T1                         
		ON 
			D.numFieldId=T1.numFieldID                         
		WHERE 
			D.numDomainID = @numDomainID 
			AND D.numFormID = 29 
			AND ISNULL(T1.bitCustomField,0) = 0
			AND d.vcDbColumnName not in ('numBillShipCountry','numBillShipState')
		UNION
		SELECT
			CONCAT('Cust',Fld_id),
			Fld_label,
			T1.tintOrder AS tintOrder,
			1 AS bitAllowSorting
		FROM
			CFW_Fld_Master C 
		INNER JOIN
			@TEMPSelectedColumns T1
		ON 
			C.Fld_id = T1.numFieldID
		WHERE   
			C.numDomainID = @numDomainID
			AND ISNULL(T1.bitCustomField,0) = 1
	) T1      
	UNION 
	SELECT 'ID','numItemCode',0 tintOrder,0 
	order by T1.tintOrder ASC




DROP TABLE #temp
END
/****** Object:  StoredProcedure [dbo].[USP_AdvancedSearchOpp]    Script Date: 07/26/2008 16:14:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_advancedsearchopp')
DROP PROCEDURE usp_advancedsearchopp
GO
Create PROCEDURE [dbo].[USP_AdvancedSearchOpp]
@WhereCondition as varchar(4000)='',
@numDomainID as numeric(9)=0,
@numUserCntID as numeric(9)=0,
@CurrentPage int,
@PageSize int,                                                                  
@TotRecs int output,                                                                                                           
@columnSortOrder as Varchar(10),    
@ColumnName as varchar(50)='',    
@SortCharacter as char(1),                
@SortColumnName as varchar(50)='',    
@LookTable as varchar(10)='',
@strMassUpdate as varchar(2000)='',
@vcRegularSearchCriteria varchar(MAX)='',
@vcCustomSearchCriteria varchar(MAX)='',
@vcDisplayColumns VARCHAR(MAX) = ''
as   
  
	  
declare @tintOrder as tinyint                                  
declare @vcFormFieldName as varchar(50)                                  
declare @vcListItemType as varchar(3)                             
declare @vcListItemType1 as varchar(3)                                 
declare @vcAssociatedControlType varchar(20)                                  
declare @numListID AS numeric(9)                                  
declare @vcDbColumnName varchar(30)                                   
declare @ColumnSearch as varchar(10)
DECLARE @vcLookBackTableName VARCHAR(50)
DECLARE @bitIsSearchBizDoc BIT




IF CHARINDEX('OpportunityBizDocs', @WhereCondition) > 0
begin

	SET @bitIsSearchBizDoc = 1
	end
ELSE 
 BEGIN
	SET @bitIsSearchBizDoc = 0
	
 END
 --Added By Sachin Sadhu||Date:12thJune2014
  --Purpose:To add Functionaly :Search with Notes Field-OpportunityBizDocItems
  --Set Manually To fullfill "All" Selection in Search
 IF CHARINDEX('vcNotes', @WhereCondition) > 0
	BEGIN
			SET 	@bitIsSearchBizDoc = 1
	END
--End of script

if (@SortCharacter<>'0' and @SortCharacter<>'') set @ColumnSearch=@SortCharacter   
Create table #tempTable ( ID INT IDENTITY PRIMARY KEY,                                                                         
      numOppID varchar(15),
	  tintCRMType TINYINT,
	  numContactID NUMERIC(18,0),
	  numDivisionID NUMERIC(18,0),
      numOppBizDocID varchar(15)
 )  
  
declare @strSql as varchar(8000)
 set  @strSql='select OppMas.numOppId, DM.tintCRMType, OppMas.numDivisionID, ADC.numContactId '
 
IF @bitIsSearchBizDoc = 1 --Added to eliminate duplicate records 
	SET @strSql = @strSql + ' ,OpportunityBizDocs.numOppBizDocsId '
ELSE 
	SET @strSql = @strSql + ' ,0 numOppBizDocsId '

IF @SortColumnName ='CalAmount'
BEGIN
	ALTER TABLE #tempTable ADD CalAmount DECIMAL(20,5) ;
	SET @strSql = @strSql + ' ,[dbo].[getdealamount](OppMas.numOppId,Getutcdate(),0) as CalAmount'
END	

IF @SortColumnName ='monDealAmount'
BEGIN
	ALTER TABLE #tempTable ADD monDealAmount DECIMAL(20,5) ;

	SET @strSql = @strSql + ' ,ISNULL((SELECT SUM(ISNULL(BD.monDealAmount,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = OppMas.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0) as monDealAmount'
END	

IF @SortColumnName ='monAmountPaid'
BEGIN
	ALTER TABLE #tempTable ADD monAmountPaid DECIMAL(20,5) ;

	SET @strSql = @strSql + ' ,ISNULL((SELECT SUM(ISNULL(BD.monAmountPaid,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = OppMas.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0) AS monAmountPaid'
END	

SET @strSql = @strSql + ' from   
OpportunityMaster OppMas   
Join AdditionalContactsinformation ADC on OppMas.numContactId = ADC.numContactId  
join DivisionMaster DM on Dm.numDivisionId = OppMas.numDivisionId  
JOIN CompanyInfo C ON DM.numCompanyID = C.numCompanyId   
'  
IF @bitIsSearchBizDoc = 1 --Added to eliminate duplicate records 
BEGIN	
	IF CHARINDEX('vcNotes', @WhereCondition) > 0
		BEGIN
	  		SET @strSql = @strSql + ' left join OpportunityBizDocs on OppMas.numOppId = OpportunityBizDocs.numOppId left join OpportunityBizDocItems on OpportunityBizDocItems.numOppBizDocID=OpportunityBizDocs.numOppBizDocsId '
		END
	ELSE
		BEGIN
			SET @strSql = @strSql + ' left join OpportunityBizDocs on OppMas.numOppId = OpportunityBizDocs.numOppId '
		END


END
	
IF @SortColumnName ='numBillCountry' OR @SortColumnName ='numBillState'
     OR @ColumnName ='numBillCountry' OR @ColumnName ='numBillState'
BEGIN 	
	set @strSql= @strSql +' left Join AddressDetails AD1 on AD1.numRecordId= DM.numDivisionID and AD1.tintAddressOf=2 and AD1.tintAddressType=1 AND AD1.bitIsPrimary=1 and AD1.numDomainID= ADC.numDomainID '
END	
ELSE IF @SortColumnName ='numShipCountry' OR @SortColumnName ='numShipState'
     OR @ColumnName ='numShipCountry' OR @ColumnName ='numShipState'
BEGIN
	set @strSql= @strSql +' left Join AddressDetails AD2 on AD2.numRecordId= DM.numDivisionID and AD2.tintAddressOf=2 and AD2.tintAddressType=2 AND AD2.bitIsPrimary=1 and AD2.numDomainID= ADC.numDomainID '
END

   if (@SortColumnName<>'')                        
  begin                              
 select top 1 @vcListItemType1=vcListItemType from View_DynamicDefaultColumns where vcDbColumnName=@SortColumnName and numFormID=15 and numDomainId=@numDomainId
  if @vcListItemType1='LI'                       
  begin
			IF @SortColumnName ='numBillCountry'
			BEGIN
					set @strSql= @strSql +' left Join ListDetails L2 on L2.numListItemID=AD1.numCountry' 
			END
			ELSE IF @SortColumnName ='numShipCountry'
			BEGIN
					set @strSql= @strSql +' left Join ListDetails L2 on L2.numListItemID=AD2.numCountry' 
			END
		  ELSE
		  BEGIN                                    
				set @strSql= @strSql +' left Join ListDetails L2 on L2.numListItemID='+@SortColumnName                                  
		  END
  end  
 else if @vcListItemType1='S'                               
    begin           
		IF @SortColumnName ='numBillState'
		BEGIN
   			set @strSql= @strSql +' left join State S2 on S2.numStateID=AD1.numState'                            
		END
	    ELSE IF @SortColumnName ='numShipState'
	    BEGIN
   			set @strSql= @strSql +' left join State S2 on S2.numStateID=AD2.numState'                           
		END
    end                          
  end                              
  set @strSql=@strSql+' where OppMas.numDomainID  = '+convert(varchar(15),@numDomainID)+   @WhereCondition                                      
     
  
if (@ColumnName<>'' and  @ColumnSearch<>'')                             
begin                              
  if @vcListItemType='LI'                                   
    begin    
			IF @ColumnName ='numBillCountry'
			BEGIN
					set @strSql= @strSql +' left Join ListDetails L1 on L1.numListItemID=AD1.numCountry' 
			END
			ELSE IF @ColumnName ='numShipCountry'
			BEGIN
					set @strSql= @strSql +' left Join ListDetails L1 on L1.numListItemID=AD2.numCountry' 
			END
		  ELSE
		  BEGIN                                
				set @strSql= @strSql +' and L1.vcData like '''+@ColumnSearch +'%'''                                 
			END
		end                                  
 else if @vcListItemType='S'                               
    begin           
		IF @ColumnName ='numBillState'
		BEGIN
   			set @strSql= @strSql +' left join State S1 on S1.numStateID=AD1.numState'                            
		END
	    ELSE IF @ColumnName ='numShipState'
	    BEGIN
   			set @strSql= @strSql +' left join State S1 on S1.numStateID=AD2.numState'                           
		END
    end
    else set @strSql= @strSql +' and '+ 
				case when @ColumnName = 'numAssignedTo' then 'OppMas.'+@ColumnName 
				else @ColumnName end 
				+' like '''+@ColumnSearch  +'%'''                                
                              
end                              
   if (@SortColumnName<>'')                            
  begin     
                          
    if @vcListItemType1='LI'                                   
    begin        
      set @strSql= @strSql +' order by L2.vcData '+@columnSortOrder                                  
    end   
	else if @vcListItemType1='S'                               
		begin                                  
		  set @strSql= @strSql +' order by S2.vcState '+@columnSortOrder                              
		end                                
    ELSE
	BEGIN
		--SET @strSql  = REPLACE(@strSql,'distinct','')
		set @strSql= @strSql +' order by '+ @SortColumnName +' ' +@columnSortOrder
	END  
  end   
  
  
insert into #tempTable exec(@strSql)    
 print @strSql 
 print '===================================================='  
         
         --SELECT * FROM #tempTable               
set @strSql=''                                  
                                  
DECLARE @bitBillAddressJoinAdded AS BIT;
DECLARE @bitShipAddressJoinAdded AS BIT;
SET @bitBillAddressJoinAdded=0;
SET @bitShipAddressJoinAdded=0;
                      
set @tintOrder=0                                  
set @WhereCondition =''                               
set @strSql='select OppMas.numOppId, DM.tintCRMType, OppMas.numDivisionID, ADC.numContactId '
IF @bitIsSearchBizDoc = 1 --Added to eliminate duplicate records 
BEGIN
	SET @strSql = @strSql + ' ,OpportunityBizDocs.numOppBizDocsId '
END

	DECLARE @TEMPSelectedColumns TABLE
	(
		numFieldID NUMERIC(18,0)
		,bitCustomField BIT
		,tintOrder INT
		,intColumnWidth FLOAT
	)
	DECLARE @vcLocationID VARCHAR(100) = '2,6'

	-- IF USER HAS SELECTED ITS OWN DISPLAY COLUMNS THEN USE IT
	IF LEN(ISNULL(@vcDisplayColumns,'')) > 0
	BEGIN
		DECLARE @TempIDs TABLE
		(
			ID INT IDENTITY(1,1),
			vcFieldID VARCHAR(300)
		)

		INSERT INTO @TempIDs
		(
			vcFieldID
		)
		SELECT 
			OutParam 
		FROM 
			SplitString(@vcDisplayColumns,',')

		INSERT INTO @TEMPSelectedColumns
		(
			numFieldID,
			bitCustomField,
			tintOrder,
			intColumnWidth
		)
		SELECT
			numFormFieldID
			,bitCustomField
			,(SELECT (ID-1) FROM @TempIDs T3 WHERE T3.vcFieldID = TEMP.vcFieldID)
			,0
		FROM
		(
			SELECT  
				numFieldID as numFormFieldID,
				0 bitCustomField,
				CONCAT(numFieldID,'~0') vcFieldID
			FROM    
				View_DynamicDefaultColumns
			WHERE   
				numFormID = 15
				AND bitInResults = 1
				AND bitDeleted = 0 
				AND numDomainID = @numDomainID
			UNION 
			SELECT  
				c.Fld_id AS numFormFieldID
				,1 bitCustomField
				,CONCAT(Fld_id,'~1') vcFieldID
			FROM    
				CFW_Fld_Master C 
			LEFT JOIN 
				CFW_Validation V ON V.numFieldID = C.Fld_id
			JOIN 
				CFW_Loc_Master L on C.GRP_ID=L.Loc_Id
			WHERE   
				C.numDomainID = @numDomainID
				AND GRP_ID IN (SELECT ID FROM dbo.SplitIDs(@vcLocationID,','))
		) TEMP
		WHERE
			vcFieldID IN (SELECT vcFieldID FROM @TempIDs)
	END

	-- IF USER SELECTED DISPLAY COLUMNS IS NOT AVAILABLE THAN GET COLUMNS CONFIGURED FROM SEARCH RESULT GRID
	IF (SELECT COUNT(*) FROM @TEMPSelectedColumns) = 0
	BEGIN
		INSERT INTO @TEMPSelectedColumns
		(
			numFieldID,
			bitCustomField,
			tintOrder,
			intColumnWidth
		)
		SELECT
			numFormFieldID
			,bitCustomField
			,tintOrder
			,intColumnWidth
		FROM
		(
			SELECT  
				A.numFormFieldID,
				0 bitCustomField,
				A.tintOrder,
				A.intColumnWidth
			FROM    AdvSerViewConf A
					JOIN View_DynamicDefaultColumns D ON D.numFieldID = A.numFormFieldID
			WHERE   A.numFormID = 15
					AND D.bitInResults = 1
					AND D.bitDeleted = 0
					AND D.numDomainID = @numDomainID 
					AND D.numFormID = 15
					AND A.numDomainID = @numDomainID
					AND A.numUserCntID=@numUserCntID
					AND ISNULL(A.bitCustom,0) = 0
			UNION
			SELECT  
				A.numFormFieldID,
				1 bitCustomField,
				A.tintOrder,
				A.intColumnWidth
			FROM    AdvSerViewConf A
					JOIN CFW_Fld_Master C ON C.Fld_id = A.numFormFieldID
			WHERE   A.numFormID = 15
					AND C.numDomainID = @numDomainID 
					AND A.numDomainID = @numDomainID
					AND A.numUserCntID=@numUserCntID
					AND ISNULL(A.bitCustom,0) = 1
		) T1
		ORDER BY
			 tintOrder
	END

	-- IF USER HASN'T CONFIGURED COLUMNS FROM SEARCH RESULT GRID THEN RETURN DEFAULT COLUMNS
	IF (SELECT COUNT(*) FROM @TEMPSelectedColumns) = 0
	BEGIN
		INSERT INTO @TEMPSelectedColumns
		(
			numFieldID,
			bitCustomField,
			tintOrder,
			intColumnWidth
		)
		SELECT  
			numFieldID,
			0,
			1,
			0
		FROM    
			View_DynamicDefaultColumns
		WHERE   
			numFormID = 15
			AND numDomainID = @numDomainID
			AND numFieldID = 96
	END

DECLARE @bitCustom BIT
DECLARE @numFieldGroupID AS INT
DECLARE @vcColumnName AS VARCHAR(200)
DECLARE @numFieldID AS NUMERIC(18,0)

	SELECT TOP 1
		@tintOrder=tintOrder,
		@numFieldID=numFormFieldID,
		@vcDbColumnName=vcDbColumnName,
		@vcFormFieldName=vcFieldName,
		@vcAssociatedControlType=vcAssociatedControlType,
		@vcListItemType=vcListItemType,
		@numListID=numListID,
		@vcLookBackTableName=vcLookBackTableName,
		@bitCustom=bitCustom,
		@numFieldGroupID=numGroupID
	FROM
	(
		SELECT
			T1.tintOrder+1 tintOrder,
			D.numFieldID AS numFormFieldID,
			vcDbColumnName,
			vcFieldName,
			vcAssociatedControlType,
			vcListItemType,
			numListID,
			vcLookBackTableName,
			0 AS bitCustom,
			0 AS numGroupID
		FROM 
			View_DynamicDefaultColumns D                               
		INNER JOIN
			@TEMPSelectedColumns T1
		ON
			D.numFieldID = T1.numFieldID                       
		WHERE 
			D.numDomainID = @numDomainID 
			AND D.numFormID = 15 
			AND ISNULL(T1.bitCustomField,0) = 0
		UNION
		SELECT
			T1.tintOrder+1 tintOrder,
			Fld_id AS numFormFieldID,
			CONCAT('Cust',Fld_id),
			Fld_label,
			c.Fld_type,
			'',
			c.numlistid,
			'',
			1 AS bitCustom,
			Grp_id
		FROM    
			CFW_Fld_Master C
		INNER JOIN
			@TEMPSelectedColumns T1
		ON
			C.Fld_id = T1.numFieldID
		WHERE   
			C.numDomainID = @numDomainID
			AND ISNULL(T1.bitCustomField,0) = 1
	) T1
	ORDER BY 
		tintOrder asc                                                             

while @tintOrder>0                                  
begin                                  
	IF @bitCustom = 0
	BEGIN
	 SET @vcColumnName= @vcDbColumnName+'~'+ convert(varchar(10),@numFieldId) +'~0'
		if @vcAssociatedControlType='SelectBox'                                  
        begin                                  
                                                                              
			IF @vcDbColumnName = 'tintSource'
            BEGIN
              SET @strSql = @strSql
                                  + ',dbo.fn_GetOpportunitySourceValue(ISNULL(OppMas.tintSource,0),ISNULL(OppMas.tintSourceType,0),OppMas.numDomainID) '
                                  +' ['+ @vcColumnName+']'
				
            END                                            
			ELSE if @vcListItemType='LI'                                   
			begin   
				IF @numListID=40--Country
				BEGIN
	  				set @strSql=@strSql+',L'+ convert(varchar(3),@tintOrder)+'.vcData'++' ['+ @vcColumnName+']'                              
				IF @vcDbColumnName ='numBillCountry'
				BEGIN
					IF @bitBillAddressJoinAdded=0 
					BEGIN
						set @WhereCondition= @WhereCondition +' left Join AddressDetails AD1 on AD1.numRecordId= DM.numDivisionID and AD1.tintAddressOf=2 and AD1.tintAddressType=1 AND AD1.bitIsPrimary=1 and AD1.numDomainID= ADC.numDomainID '
					
						SET @bitBillAddressJoinAdded=1;
					END
					set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID=AD1.numCountry'
				END
				ELSE IF @vcDbColumnName ='numShipCountry'
				BEGIN
					IF @bitShipAddressJoinAdded=0 
					BEGIN
						set @WhereCondition= @WhereCondition +' left Join AddressDetails AD2 on AD2.numRecordId= DM.numDivisionID and AD2.tintAddressOf=2 and AD2.tintAddressType=2 AND AD2.bitIsPrimary=1 and AD2.numDomainID= ADC.numDomainID '
					
						SET @bitShipAddressJoinAdded=1;
					END
					set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID=AD2.numCountry'
				END
			  END                        
			  ELSE
			  BEGIN                               
				set @strSql=@strSql+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName+']'
				
				if @vcDbColumnName = 'numSalesOrPurType' AND @numListID=46
					set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID=OppMas.numSalesOrPurType and OppMas.tintOppType = 2 '                                 		
				else if @vcDbColumnName = 'numSalesOrPurType' AND @numListID=45
					set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID=OppMas.numSalesOrPurType and OppMas.tintOppType = 1 '                                 
				else
					set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID='+@vcDbColumnName                                  
			  END                                  
        END                      
		END 
		ELSE IF @vcAssociatedControlType='ListBox'   
		BEGIN
			if @vcListItemType='S'                               
			  begin                         
				set @strSql=@strSql+',S'+ convert(varchar(3),@tintOrder)+'.vcState'+' ['+ @vcColumnName+']'
				IF @vcDbColumnName ='numBillState'
				BEGIN
					IF @bitBillAddressJoinAdded=0 
					BEGIN
						set @WhereCondition= @WhereCondition +' left Join AddressDetails AD1 on AD1.numRecordId= DM.numDivisionID and AD1.tintAddressOf=2 and AD1.tintAddressType=1 AND AD1.bitIsPrimary=1 and AD1.numDomainID= ADC.numDomainID '
						SET @bitBillAddressJoinAdded=1;
					END
					set @WhereCondition= @WhereCondition +' left join State S'+ convert(varchar(3),@tintOrder)+ ' on S'+convert(varchar(3),@tintOrder)+ '.numStateID=AD1.numState'
				END
				ELSE IF @vcDbColumnName ='numShipState'
				BEGIN
					IF @bitShipAddressJoinAdded=0 
					BEGIN
						set @WhereCondition= @WhereCondition +' left Join AddressDetails AD2 on AD2.numRecordId= DM.numDivisionID and AD2.tintAddressOf=2 and AD2.tintAddressType=2 AND AD2.bitIsPrimary=1 and AD2.numDomainID= ADC.numDomainID '
						SET @bitShipAddressJoinAdded=1;
					END
					set @WhereCondition= @WhereCondition +' left join State S'+ convert(varchar(3),@tintOrder)+ ' on S'+convert(varchar(3),@tintOrder)+ '.numStateID=AD2.numState'
				END
			  end  
		END  
		else if @vcAssociatedControlType = 'CheckBox'           
		   begin            
               
			set @strSql= @strSql+',case when isnull('+ @vcDbColumnName +',0)=0 then ''No'' when isnull('+ @vcDbColumnName +',0)=1 then ''Yes'' end  ['+ @vcColumnName+']'              
 
		   end                                  
		 else 
				BEGIN
					IF @bitIsSearchBizDoc = 0
					BEGIN
						IF @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName != 'monDealAmount' AND @vcDbColumnName != 'monAmountPaid'
						  BEGIN
	  							SET @vcDbColumnName = '''''' --include OpportunityBizDocs tables column as blank string
						  END	
					END
	  
					set @strSql=@strSql+','+ 
						case  
							when @vcLookBackTableName = 'OpportunityMaster' AND @vcDbColumnName='monDealAmount' then 'OppMas.monDealAmount' 
							when @vcDbColumnName='numAge' then 'year(getutcdate())-year(bintDOB)'   
							when @vcDbColumnName='numDivisionID' then 'DM.numDivisionID' 
							when @vcDbColumnName='vcProgress' then  ' dbo.fn_OppTotalProgress(OppMas.numOppId)'
							when @vcDbColumnName='intPEstimatedCloseDate' or @vcDbColumnName='dtDateEntered' or @vcDbColumnName='bintShippedDate'  then  
									'dbo.FormatedDateFromDate('+@vcDbColumnName+','+convert(varchar(10),@numDomainId)+')'  
							WHEN @vcDbColumnName = 'CalAmount' THEN '[dbo].[getdealamount](OppMas.numOppId,Getutcdate(),0)'
							WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'monDealAmount' THEN 'ISNULL((SELECT SUM(ISNULL(BD.monDealAmount,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = OppMas.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)'
							WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'monAmountPaid' THEN 'ISNULL((SELECT SUM(ISNULL(BD.monAmountPaid,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = OppMas.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)'
							else @vcDbColumnName end+ ' ['+ @vcColumnName+']'
				END
    END
	ELSE IF @bitCustom = 1
	BEGIN
		--SET @vcColumnName= CONCAT(@vcFormFieldName,'~','Cust',@numFieldId)
			SET @vcColumnName= @vcDbColumnName +'~'+ convert(varchar(10),@numFieldId)+'~1'  
		IF @vcAssociatedControlType = 'TextBox' or @vcAssociatedControlType = 'TextArea'            
		BEGIN
			SET @strSql= @strSql+',CFW'+ convert(VARCHAR(3),@tintOrder)+'.Fld_Value  ['+ @vcColumnName +']'                   
				
			SET @WhereCondition= @WhereCondition 
								+' left Join CFW_Fld_Values_Opp CFW'+ convert(varchar(3),@tintOrder)
								+ ' on CFW' + convert(varchar(3),@tintOrder) 
								+ '.Fld_Id='+convert(varchar(10),@numFieldId) 
								+ 'and CFW' + convert(varchar(3),@tintOrder) 
								+ '.RecId=OppMas.numOppId '                                                         
		END   
		ELSE IF @vcAssociatedControlType = 'CheckBox'       
		BEGIN
			set @strSql= @strSql+',case when isnull(CFW' + CONVERT(VARCHAR(3), @tintOrder)+ '.Fld_Value,0)='''' then 0 else  isnull(CFW'
											+ CONVERT(VARCHAR(3), @tintOrder)+ '.Fld_Value,0) end ['+ @vcColumnName + ']'
 
			set @WhereCondition= @WhereCondition +' left Join CFW_Fld_Values_Opp CFW'+ convert(varchar(3),@tintOrder)+ '             
			on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=OppMas.numOppId '                                                     
		END                
		ELSE IF @vcAssociatedControlType = 'DateField'            
		BEGIN
			set @strSql= @strSql+',dbo.FormatedDateFromDate(CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value,'+convert(varchar(10),@numDomainId)+')  ['+ @vcColumnName +']'                   
			set @WhereCondition= @WhereCondition +' left Join CFW_Fld_Values_Opp CFW'+ convert(varchar(3),@tintOrder)+ '                 
			on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=OppMas.numOppId '
		END                
		ELSE IF @vcAssociatedControlType = 'SelectBox'             
		BEGIN
			set @vcDbColumnName = 'DCust'+Convert(varchar(10),@numFieldId)                
			set @strSql=@strSql+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName +']'                                                          
			set @WhereCondition= @WhereCondition +' left Join CFW_Fld_Values_Opp CFW'+ convert(varchar(3),@tintOrder)+ '                 
				on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=OppMas.numOppId '
			set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID=CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value'                
		END         
		ELSE IF @vcAssociatedControlType = 'CheckBoxList'
		BEGIN
			SET @strSql= @strSql+',(SELECT STUFF((SELECT CONCAT('', '', vcData) FROM ListDetails WHERE numListID= ' + CAST(@numListID AS varchar) + ' AND numlistitemid IN (SELECT ISNULL(Id,0) FROM dbo.SplitIDs(CFW'+ convert(VARCHAR(3),@tintOrder)+'.Fld_Value,'','')) FOR XML PATH('''')), 1, 1, ''''))  ['+ @vcColumnName +']'   

			SET @WhereCondition= @WhereCondition 
								+' left Join CFW_Fld_Values_Opp CFW'+ convert(varchar(3),@tintOrder)
								+ ' on CFW' + convert(varchar(3),@tintOrder) 
								+ '.Fld_Id='+convert(varchar(10),@numFieldId) 
								+ 'and CFW' + convert(varchar(3),@tintOrder) 
								+ '.RecId=OppMas.numOppId '
	END
	END            
                                          
	SELECT TOP 1
		@tintOrder=tintOrder,
		@numFieldID=numFormFieldID,
		@vcDbColumnName=vcDbColumnName,
		@vcFormFieldName=vcFieldName,
		@vcAssociatedControlType=vcAssociatedControlType,
		@vcListItemType=vcListItemType,
		@numListID=numListID,
		@vcLookBackTableName=vcLookBackTableName,
		@bitCustom=bitCustom,
		@numFieldGroupID=numGroupID
	FROM
	(
		SELECT
			T1.tintOrder+1 tintOrder,
			D.numFieldID AS numFormFieldID,
			vcDbColumnName,
			vcFieldName,
			vcAssociatedControlType,
			vcListItemType,
			numListID,
			vcLookBackTableName,
			0 AS bitCustom,
			0 AS numGroupID
		FROM 
			View_DynamicDefaultColumns D                               
		INNER JOIN
			@TEMPSelectedColumns T1
		ON
			D.numFieldID = T1.numFieldID                       
		WHERE 
			D.numDomainID = @numDomainID 
			AND D.numFormID = 15 
			AND ISNULL(T1.bitCustomField,0) = 0
			AND T1.tintOrder > @tintOrder-1
		UNION
		SELECT
			T1.tintOrder+1 tintOrder,
			Fld_id AS numFormFieldID,
			CONCAT('Cust',Fld_id),
			Fld_label,
			c.Fld_type,
			'',
			c.numlistid,
			'',
			1 AS bitCustom,
			Grp_id
		FROM    
			CFW_Fld_Master C
		INNER JOIN
			@TEMPSelectedColumns T1
		ON
			C.Fld_id = T1.numFieldID
		WHERE   
			C.numDomainID = @numDomainID
			AND ISNULL(T1.bitCustomField,0) = 1
			AND T1.tintOrder > @tintOrder-1
	) T1
	ORDER BY 
		tintOrder asc                                   

 if @@rowcount=0 set @tintOrder=0                                  
end   
	declare @firstRec as integer                                                                        
	declare @lastRec as integer                                                                        
	set @firstRec= (@CurrentPage-1) * @PageSize                          
	set @lastRec= (@CurrentPage*@PageSize+1)                                                                         
	set @TotRecs=(select count(*) from #tempTable)   


DECLARE @from VARCHAR(8000) 
set @from = ' from OpportunityMaster OppMas   
Join AdditionalContactsinformation ADC on OppMas.numContactId = ADC.numContactId  
join DivisionMaster DM on Dm.numDivisionId = OppMas.numDivisionId  
JOIN CompanyInfo C ON DM.numCompanyID = C.numCompanyId '

IF @bitIsSearchBizDoc = 1 --Added to eliminate duplicate records 
 --Added By Sachin Sadhu||Date:12thJune2014
  --Purpose:To add Functionaly :Search with Notes Field-OpportunityBizDocItems

	IF CHARINDEX('vcNotes', @WhereCondition) > 0
		BEGIN
	  		SET @from = @from + ' left join OpportunityBizDocs on OppMas.numOppId = OpportunityBizDocs.numOppId left join OpportunityBizDocItems on OpportunityBizDocItems.numOppBizDocID=OpportunityBizDocs.numOppBizDocsId '
		END
	ELSE
		BEGIN
			SET @from = @from + ' left join OpportunityBizDocs on OppMas.numOppId = OpportunityBizDocs.numOppId '
		END
	--End of Script

SET @strSql=@strSql+@from;

 
 
 if LEN(@strMassUpdate)>1
 begin     
	Declare @strReplace as varchar(2000)
    set @strReplace = case when @LookTable='OppMas' OR @LookTable='Opportunit' then 'OppMas.numOppID' ELSE '' end  + @from
   
    set @strMassUpdate=replace(@strMassUpdate,'@$replace',@strReplace)
    PRINT @strMassUpdate
   exec (@strMassUpdate)         
 end         




SET @strSql=@strSql+ @WhereCondition


	SET @strSql = @strSql +' join #tempTable T on T.numOppID=OppMas.numOppID 
 WHERE ID > '+convert(varchar(10),@firstRec)+ ' and ID <'+ convert(varchar(10),@lastRec)
 IF @bitIsSearchBizDoc = 1 
 BEGIN
 	SET @strSql = @strSql + '  and T.numOppBizDocID = OpportunityBizDocs.numOppBizDocsID '
 END
 
 SET @strSql = @strSql + ' order by ID'
print @strSql
exec(@strSql)                                                                     
drop table #tempTable

 

	SELECT 
 		tintOrder,
		numFormFieldID,
		vcDbColumnName,
		vcFieldName,
		vcAssociatedControlType,
		vcListItemType,
		numListID,
		vcLookBackTableName,
		numFieldID,
		intColumnWidth,
		bitCustom as bitCustomField,
		numGroupID,
		vcOrigDbColumnName,
		bitAllowSorting,
		bitAllowEdit,
		ListRelID,
		bitAllowFiltering,
		vcFieldDataType
	FROM
	(
		SELECT
			T1.tintOrder+1 tintOrder, 
			D.numFieldID numFormFieldID,
			vcDbColumnName,
			vcFieldName,
			vcAssociatedControlType,
			vcListItemType,
			numListID,
			vcLookBackTableName,
			D.numFieldID,
			ISNULL(T1.intColumnWidth,0) AS intColumnWidth,
			ISNULL(D.bitCustom,0) AS bitCustom,
			0 AS numGroupID
			,vcOrigDbColumnName
			,bitAllowSorting,
			bitAllowEdit,
			ListRelID,
			bitAllowFiltering,
			vcFieldDataType
		FROM 
			View_DynamicDefaultColumns D                               
		INNER JOIN
			@TEMPSelectedColumns T1
		ON
			D.numFieldID = T1.numFieldID
		WHERE 
			D.numDomainID = @numDomainID 
			AND D.numFormID = 15 
			AND ISNULL(T1.bitCustomField,0) = 0
			AND T1.tintOrder > @tintOrder-1
		UNION
		SELECT
			T1.tintOrder+1 tintOrder,
			Fld_id numFormFieldID,
			CONCAT('Cust',Fld_id),
			Fld_label,
			c.Fld_type,
			'',
			c.numlistid,
			'',
			Fld_id,
			ISNULL(T1.intColumnWidth,0) AS intColumnWidth,
			1 AS bitCustom,
			Grp_id
			,'',
			null,
			null,
			null,
			null,
			''
		FROM    
			CFW_Fld_Master C 
		INNER JOIN
			@TEMPSelectedColumns T1
		ON
			C.Fld_id = T1.numFieldID
		WHERE   
			C.numDomainID = @numDomainID
			AND ISNULL(T1.bitCustomField,0) = 1
			AND T1.tintOrder > @tintOrder-1
	) T1
	ORDER BY 
		tintOrder asc  

--Left Join OpportunityItems OppItems on OppItems.numOppId = OppMas.numOppId   
--left join Item on Item.numItemCode = OppItems.numItemCode  
--left join WareHouseItems on Item.numItemCode = WareHouseItems.numItemId  
--left Join WareHouses WareHouse on WareHouse.numWareHouseId =WareHouseItems.numWareHouseId  
--left Join WareHouseItmsDTL WareHouseItemDTL on WareHouseItemDTL.numWareHouseItemID = WareHouse.numWareHouseId
GO
/****** Object:  StoredProcedure [dbo].[USP_AdvancedSearchPro]    Script Date: 07/26/2008 16:14:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_advancedsearchpro')
DROP PROCEDURE usp_advancedsearchpro
GO
CREATE PROCEDURE [dbo].[USP_AdvancedSearchPro]
@WhereCondition as varchar(1000)='',                                                        
@numDomainID as numeric(9)=0,                                      
@numUserCntID as numeric(9)=0,                                     
@CurrentPage int,                                                                            
@PageSize int,                                                                            
@TotRecs int output,                                                                                                                     
@columnSortOrder as Varchar(10),        
@ColumnName as varchar(20)='',        
@SortCharacter as char(1),                                
@SortColumnName as varchar(20)='',        
@LookTable as varchar(10)='' ,
@vcDisplayColumns VARCHAR(MAX)
as
     
declare @tintOrder as tinyint                                      
declare @vcFormFieldName as varchar(50)                                      
declare @vcListItemType as varchar(3)                                 
declare @vcListItemType1 as varchar(3)                                     
declare @vcAssociatedControlType varchar(20)                                      
declare @numListID AS numeric(9)                                      
declare @vcDbColumnName varchar(30)                                       
declare @ColumnSearch as varchar(10)                              
if (@SortCharacter<>'0' and @SortCharacter<>'') set @ColumnSearch=@SortCharacter       
Create table #tempTable ( ID INT IDENTITY PRIMARY KEY,                                                                             
      numProID varchar(15)                                                                            
 )      
      
declare @strSql as varchar(8000)                                                                      
 set  @strSql='select numProID from       
ProjectsMaster ProMas
join DivisionMaster DM on Dm.numDivisionId = ProMas.numDivisionId    
JOIN CompanyInfo C ON DM.numCompanyID = C.numCompanyId
LEFT JOIN AdditionalContactsinformation ADC on ADC.numContactId = ProMas.numIntPrjMgr
'      
      
                                 
   if (@SortColumnName<>'')                            
  begin                                  
 select @vcListItemType1=vcListItemType from View_DynamicDefaultColumns where vcDbColumnName=@SortColumnName and numFormID=1  and numDomainId=@numDomainId                                
  if @vcListItemType1='L'                           
  begin                                        
    set @strSql= @strSql +' left Join ListDetails L2 on L2.numListItemID='+@SortColumnName                                      
  end                                
  end                                  
  set @strSql=@strSql+' where ProMas.numDomainID  = '+convert(varchar(15),@numDomainID)+   @WhereCondition                                          
         
      
if (@ColumnName<>'' and  @ColumnSearch<>'')                                 
begin                                  
  if @vcListItemType='L'                                       
    begin                                        
      set @strSql= @strSql +' and L1.vcData like '''+@ColumnSearch +'%'''                                     
    end                                      
    else set @strSql= @strSql +' and '+  @ColumnName +' like '''+@ColumnSearch  +'%'''                                    
                                  
end                                  
   if (@SortColumnName<>'')                                
  begin                                   
    if @vcListItemType1='L'                                       
    begin            
      set @strSql= @strSql +' order by L2.vcData '+@columnSortOrder              
    end                                      
    else  set @strSql= @strSql +' order by '+ case  when @SortColumnName = 'txtComments' then 'convert(varchar(500),txtComments)' 
		  else @SortColumnName end  +' ' +@columnSortOrder                                
  end       
      
      
insert into #tempTable(numProID)                       
      
exec(@strSql)        
 print @strSql     
 print '===================================================='      
                            
set @strSql=''                                      
                                      
                                     
set @tintOrder=0                                      
set @WhereCondition =''                                   
set @strSql='select ProMas.numProID '

	DECLARE @TEMPSelectedColumns TABLE
	(
		numFieldID NUMERIC(18,0)
		,bitCustomField BIT
		,tintOrder INT
		,intColumnWidth FLOAT
	)
	DECLARE @vcLocationID VARCHAR(100) = '0'
	SELECT @vcLocationID =isnull(vcLocationID,'0') from dynamicFormMaster where numFormID=18  

	-- IF USER HAS SELECTED ITS OWN DISPLAY COLUMNS THEN USE IT
	IF LEN(ISNULL(@vcDisplayColumns,'')) > 0
	BEGIN
		DECLARE @TempIDs TABLE
		(
			ID INT IDENTITY(1,1),
			vcFieldID VARCHAR(300)
		)

		INSERT INTO @TempIDs
		(
			vcFieldID
		)
		SELECT 
			OutParam 
		FROM 
			SplitString(@vcDisplayColumns,',')

		INSERT INTO @TEMPSelectedColumns
		(
			numFieldID,
			bitCustomField,
			tintOrder,
			intColumnWidth
		)
		SELECT
			numFormFieldID
			,bitCustomField
			,(SELECT (ID-1) FROM @TempIDs T3 WHERE T3.vcFieldID = TEMP.vcFieldID)
			,0
		FROM
		(
			SELECT  
				numFieldID as numFormFieldID,
				0 bitCustomField,
				CONCAT(numFieldID,'~0') vcFieldID
			FROM    
				View_DynamicDefaultColumns
			WHERE   
				numFormID = 18
				AND bitInResults = 1
				AND bitDeleted = 0 
				AND numDomainID = @numDomainID
			UNION 
			SELECT  
				c.Fld_id AS numFormFieldID
				,1 bitCustomField
				,CONCAT(Fld_id,'~1') vcFieldID
			FROM    
				CFW_Fld_Master C 
			LEFT JOIN 
				CFW_Validation V ON V.numFieldID = C.Fld_id
			JOIN 
				CFW_Loc_Master L on C.GRP_ID=L.Loc_Id
			WHERE   
				C.numDomainID = @numDomainID
				AND GRP_ID IN (SELECT ID FROM dbo.SplitIDs(@vcLocationID,','))
		) TEMP
		WHERE
			vcFieldID IN (SELECT vcFieldID FROM @TempIDs)
	END

	-- IF USER SELECTED DISPLAY COLUMNS IS NOT AVAILABLE THAN GET COLUMNS CONFIGURED FROM SEARCH RESULT GRID
	IF (SELECT COUNT(*) FROM @TEMPSelectedColumns) = 0
	BEGIN
		INSERT INTO @TEMPSelectedColumns
		(
			numFieldID,
			bitCustomField,
			tintOrder,
			intColumnWidth
		)
		SELECT
			numFormFieldID
			,bitCustomField
			,tintOrder
			,intColumnWidth
		FROM
		(
			SELECT  
				A.numFormFieldID,
				0 bitCustomField,
				A.tintOrder,
				A.intColumnWidth
			FROM    AdvSerViewConf A
					JOIN View_DynamicDefaultColumns D ON D.numFieldID = A.numFormFieldID
			WHERE   A.numFormID = 18
					AND D.bitInResults = 1
					AND D.bitDeleted = 0
					AND D.numDomainID = @numDomainID 
					AND D.numFormID = 18
					AND A.numDomainID = @numDomainID
					AND A.numUserCntID=@numUserCntID
					AND ISNULL(A.bitCustom,0) = 0
			UNION
			SELECT  
				A.numFormFieldID,
				1 bitCustomField,
				A.tintOrder,
				A.intColumnWidth
			FROM    AdvSerViewConf A
					JOIN CFW_Fld_Master C ON C.Fld_id = A.numFormFieldID
			WHERE   A.numFormID = 18
					AND C.numDomainID = @numDomainID 
					AND A.numDomainID = @numDomainID
					AND A.numUserCntID=@numUserCntID
					AND ISNULL(A.bitCustom,0) = 1
		) T1
		ORDER BY
			 tintOrder
	END

	-- IF USER HASN'T CONFIGURED COLUMNS FROM SEARCH RESULT GRID THEN RETURN DEFAULT COLUMNS
	IF (SELECT COUNT(*) FROM @TEMPSelectedColumns) = 0
	BEGIN
		INSERT INTO @TEMPSelectedColumns
		(
			numFieldID,
			bitCustomField,
			tintOrder,
			intColumnWidth
		)
		SELECT  
			numFieldID,
			0,
			1,
			0
		FROM    
			View_DynamicDefaultColumns
		WHERE   
			numFormID = 18
			AND numDomainID = @numDomainID
			AND numFieldID = 148
	END                                      


DECLARE @Table AS VARCHAR(200)
DECLARE @bitCustom BIT
DECLARE @numFieldGroupID AS INT
DECLARE @vcColumnName AS VARCHAR(200)
DECLARE @numFieldID AS NUMERIC(18,0)
DECLARE @Prefix AS VARCHAR(10)

	SELECT TOP 1
		@tintOrder=tintOrder,
		@numFieldID=numFormFieldID,
		@vcDbColumnName=vcDbColumnName,
		@vcFormFieldName=vcFieldName,
		@vcAssociatedControlType=vcAssociatedControlType,
		@vcListItemType=vcListItemType,
		@numListID=numListID,
		@Table=vcLookBackTableName,
		@bitCustom=bitCustom,
		@numFieldGroupID=numGroupID
	FROM
	(
		SELECT
			T1.tintOrder+1 tintOrder,
			D.numFieldID AS numFormFieldID,
			vcDbColumnName,
			vcFieldName,
			vcAssociatedControlType,
			vcListItemType,
			numListID,
			vcLookBackTableName,
			0 AS bitCustom,
			0 AS numGroupID
		FROM 
			View_DynamicDefaultColumns D                               
		INNER JOIN	
			@TEMPSelectedColumns T1
		ON
			D.numFieldID=T1.numFieldID
		WHERE 
			D.numDomainID = @numDomainID 
			AND D.numFormID = 18 
			AND ISNULL(T1.bitCustomField,0) = 0
		UNION
		SELECT
			T1.tintOrder+1 tintOrder,
			C.Fld_id AS numFormFieldID,
			CONCAT('Cust',Fld_id),
			Fld_label,
			c.Fld_type,
			'',
			c.numlistid,
			'',
			1 AS bitCustom,
			Grp_id
		FROM    
			CFW_Fld_Master C
		INNER JOIN	
			@TEMPSelectedColumns T1
		ON
			C.Fld_id=T1.numFieldID
		WHERE   
			C.numDomainID = @numDomainID
			AND ISNULL(T1.bitCustomField,0) = 1
	) T1
	ORDER BY 
		tintOrder asc    
                                    
while @tintOrder>0                                      
begin       

	IF @Table = 'AdditionalContactsInformation' 
        SET @Prefix = 'ADC.'
	IF @Table = 'DivisionMaster' 
        SET @Prefix = 'DM.'
    IF @Table = 'CompanyInfo' 
        SET @Prefix = 'C.'
    IF @Table = 'ProjectsMaster' 
        SET @Prefix = 'ProMas.'                               
                                 
   IF @bitCustom = 0
   BEGIN                                    
		if @vcAssociatedControlType='SelectBox'                                      
        begin                                      
                                                      
			if @vcListItemType='LI'                                       
			begin                                      
			set @strSql=@strSql+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcFormFieldName+'~'+ @vcDbColumnName+']'                                      
			set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID='+ (@Prefix + @vcDbColumnName)                                      
			end 
                                     
			if @vcListItemType='U'                                       
			begin                                      
				SET @strSql=@strSql+',dbo.fn_GetContactName(' + @Prefix + @vcDbColumnName+') ['+ @vcFormFieldName+'~'+ @vcDbColumnName+']'
			end 
                                      
			end                                      
		else set @strSql=@strSql+','+ case  when @vcDbColumnName='numAge' then 'year(getutcdate())-year(bintDOB)'       
			when @vcDbColumnName='numDivisionID' then 'DM.numDivisionID'     
			when @vcDbColumnName='vcProgress' then  ' dbo.fn_ProTotalProgress(ProMas.numProId)'    
			when @vcDbColumnName='intTargetResolveDate' or @vcDbColumnName='dtDateEntered' or @vcDbColumnName='bintShippedDate'  then      
			'dbo.FormatedDateFromDate('+ @Prefix + @vcDbColumnName +','+convert(varchar(10),@numDomainId)+')'      
			 else @Prefix + @vcDbColumnName end +' ['+ @vcFormFieldName+'~'+ @vcDbColumnName+']'                   
    END
	ELSE IF @bitCustom = 1
	BEGIN
		SET @vcColumnName= CONCAT(@vcFormFieldName,'~','Cust',@numFieldId)
			
		IF @vcAssociatedControlType = 'TextBox' or @vcAssociatedControlType = 'TextArea'            
		BEGIN
			SET @strSql= @strSql+',CFW'+ convert(VARCHAR(3),@tintOrder)+'.Fld_Value  ['+ @vcColumnName +']'                   
				
			SET @WhereCondition= @WhereCondition 
								+' left Join CFW_FLD_Values_Pro CFW'+ convert(varchar(3),@tintOrder)
								+ ' on CFW' + convert(varchar(3),@tintOrder) 
								+ '.Fld_Id='+convert(varchar(10),@numFieldId) 
								+ 'and CFW' + convert(varchar(3),@tintOrder) 
								+ '.RecId=ProMas.numProID '                                                         
		END   
		ELSE IF @vcAssociatedControlType = 'CheckBox'       
		BEGIN
			set @strSql= @strSql+',case when isnull(CFW' + CONVERT(VARCHAR(3), @tintOrder)+ '.Fld_Value,0)='''' then 0 else  isnull(CFW'
											+ CONVERT(VARCHAR(3), @tintOrder)+ '.Fld_Value,0) end ['+ @vcColumnName + ']'
 
			set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values_Pro CFW'+ convert(varchar(3),@tintOrder)+ '             
			on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=ProMas.numProID '                                                     
		END                
		ELSE IF @vcAssociatedControlType = 'DateField'            
		BEGIN
			set @strSql= @strSql+',dbo.FormatedDateFromDate(CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value,'+convert(varchar(10),@numDomainId)+')  ['+ @vcColumnName +']'                   
			set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values_Pro CFW'+ convert(varchar(3),@tintOrder)+ '                 
			on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=ProMas.numProID '
		END                
		ELSE IF @vcAssociatedControlType = 'SelectBox'             
		BEGIN
			set @vcDbColumnName = 'DCust'+Convert(varchar(10),@numFieldId)                
			set @strSql=@strSql+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName +']'                                                          
			set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values_Pro CFW'+ convert(varchar(3),@tintOrder)+ '                 
				on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=ProMas.numProID '
			set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID=CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value'                
		END         
		ELSE IF @vcAssociatedControlType = 'CheckBoxList'
		BEGIN
			SET @strSql= @strSql+',(SELECT STUFF((SELECT CONCAT('', '', vcData) FROM ListDetails WHERE numListID= ' + CAST(@numListID AS varchar) + ' AND numlistitemid IN (SELECT ISNULL(Id,0) FROM dbo.SplitIDs(CFW'+ convert(VARCHAR(3),@tintOrder)+'.Fld_Value,'','')) FOR XML PATH('''')), 1, 1, ''''))  ['+ @vcColumnName +']'   

			SET @WhereCondition= @WhereCondition 
								+' left Join CFW_FLD_Values_Pro CFW'+ convert(varchar(3),@tintOrder)
								+ ' on CFW' + convert(varchar(3),@tintOrder) 
								+ '.Fld_Id='+convert(varchar(10),@numFieldId) 
								+ 'and CFW' + convert(varchar(3),@tintOrder) 
								+ '.RecId=ProMas.numProID '
		END
	END    
          
                
             
                
                  
                       
                                              
 SELECT TOP 1
		@tintOrder=tintOrder,
		@numFieldID=numFormFieldID,
		@vcDbColumnName=vcDbColumnName,
		@vcFormFieldName=vcFieldName,
		@vcAssociatedControlType=vcAssociatedControlType,
		@vcListItemType=vcListItemType,
		@numListID=numListID,
		@Table=vcLookBackTableName,
		@bitCustom=bitCustom,
		@numFieldGroupID=numGroupID
	FROM
	(
		SELECT
			T1.tintOrder+1 tintOrder,
			D.numFieldID AS numFormFieldID,
			vcDbColumnName,
			vcFieldName,
			vcAssociatedControlType,
			vcListItemType,
			numListID,
			vcLookBackTableName,
			0 AS bitCustom,
			0 AS numGroupID
		FROM 
			View_DynamicDefaultColumns D                               
		INNER JOIN	
			@TEMPSelectedColumns T1
		ON
			D.numFieldID=T1.numFieldID
		WHERE 
			D.numDomainID = @numDomainID 
			AND D.numFormID = 18 
			AND ISNULL(T1.bitCustomField,0) = 0
			AND T1.tintOrder > @tintOrder-1
		UNION
		SELECT
			T1.tintOrder+1 tintOrder,
			C.Fld_id AS numFormFieldID,
			CONCAT('Cust',Fld_id),
			Fld_label,
			c.Fld_type,
			'',
			c.numlistid,
			'',
			1 AS bitCustom,
			Grp_id
		FROM    
			CFW_Fld_Master C
		INNER JOIN	
			@TEMPSelectedColumns T1
		ON
			C.Fld_id=T1.numFieldID
		WHERE   
			C.numDomainID = @numDomainID
			AND ISNULL(T1.bitCustomField,0) = 1
			AND T1.tintOrder > @tintOrder-1
	) T1
	ORDER BY 
		tintOrder asc                                              

 if @@rowcount=0 set @tintOrder=0                                      
end       
  declare @firstRec as integer                                                                            
  declare @lastRec as integer                                                                            
 set @firstRec= (@CurrentPage-1) * @PageSize                              
 set @lastRec= (@CurrentPage*@PageSize+1)                                                                             
set @TotRecs=(select count(*) from #tempTable)       
set @strSql=@strSql+'from ProjectsMaster ProMas join DivisionMaster DM on Dm.numDivisionId = ProMas.numDivisionId    
JOIN CompanyInfo C ON DM.numCompanyID = C.numCompanyId
LEFT JOIN AdditionalContactsinformation ADC on ADC.numContactId = ProMas.numIntPrjMgr     
 '+@WhereCondition+'      
      
 join #tempTable T on T.numProID=ProMas.numProID       
 WHERE ID > '+convert(varchar(10),@firstRec)+ ' and ID <'+ convert(varchar(10),@lastRec)                          
 print @strSql                                                              
exec(@strSql)                                                                         
drop table #tempTable
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_AdvancedSearchSurvey')
DROP PROCEDURE USP_AdvancedSearchSurvey
GO
CREATE  PROCEDURE [dbo].[USP_AdvancedSearchSurvey]  
@WhereCondition as varchar(1000)='',                   
@numDomainID as numeric(9)=0,            
@numUserCntID as numeric(9)=0,                                                 
@CurrentPage int,                                                                              
@PageSize int,                                                                              
@TotRecs int output,                                                                                                                       
@columnSortOrder as Varchar(10),          
@ColumnName as varchar(20)='',          
@SortCharacter as char(1),                                  
@SortColumnName as varchar(20)='',          
@LookTable as varchar(10)='',
@GetAll as bit,
@vcDisplayColumns VARCHAR(MAX)
as  
declare @tintOrder as tinyint                                        
declare @vcFormFieldName as varchar(50)                                        
declare @vcListItemType as varchar(3)                                   
declare @vcListItemType1 as varchar(3)                                       
declare @vcAssociatedControlType varchar(10)                                        
declare @numListID AS numeric(9)                                        
declare @vcDbColumnName varchar(30)                                         
declare @ColumnSearch as varchar(10)   
DECLARE @bitContactAddressJoinAdded AS BIT;
DECLARE @bitBillAddressJoinAdded AS BIT;
DECLARE @bitShipAddressJoinAdded AS BIT;

                             
if (@SortCharacter<>'0' and @SortCharacter<>'') set @ColumnSearch=@SortCharacter         
Create table #tempTable ( ID INT IDENTITY PRIMARY KEY,                                                                               
      numContactId varchar(15)                                                                              
 )   
                                                                       
declare @strSql as varchar(8000)                                                                  
 set  @strSql='Select ADC.numContactId                        
  FROM AdditionalContactsInformation ADC                                                       
  JOIN DivisionMaster DM ON ADC.numDivisionId = DM.numDivisionID                                                      
  JOIN CompanyInfo C ON DM.numCompanyID = C.numCompanyId                                  
   '                              
  if (@ColumnName<>'' and  @ColumnSearch<>'')                            
  begin                              
 select @vcListItemType=vcListItemType from View_DynamicDefaultColumns where vcDbColumnName=@ColumnName and numFormID=19 and numDomainId=@numDomainId                              
    if @vcListItemType='LI'                                   
    begin                                    
      set @strSql= @strSql +' left Join ListDetails L1 on L1.numListItemID='+@ColumnName                                  
    end                                  
    else if @vcListItemType='S'                                   
    begin                                      
      set @strSql= @strSql +' left join State S1 on S1.numStateID='+@ColumnName                                  
    end                                  
    else if @vcListItemType='T'                                   
    begin                                  
      set @strSql= @strSql +' left Join ListDetails L1 on L1.numListItemID='+@ColumnName                                 
    end                               
   end                              
   if (@SortColumnName<>'')                        
  begin                              
 select @vcListItemType1=vcListItemType from View_DynamicDefaultColumns where vcDbColumnName=@SortColumnName and numFormID=19  and numDomainId=@numDomainId                            
    if @vcListItemType1='LI'                       
    begin                                    
      set @strSql= @strSql +' left Join ListDetails L2 on L2.numListItemID='+@SortColumnName                           
    end                                  
    else if @vcListItemType1='S'               
    begin                                      
      set @strSql= @strSql +' left join State S2 on S2.numStateID='+@SortColumnName                                  
    end                                  
    else if @vcListItemType1='T'                      
    begin                                  
      set @strSql= @strSql +' left Join ListDetails L2 on L2.numListItemID='+@SortColumnName                                   
    end                               
  end                              
  set @strSql=@strSql+' where DM.numDomainID  = '+convert(varchar(15),@numDomainID)+    
' and ADC.numContactId in   
(select numRegisteredRespondentContactId from SurveyRespondentsMaster SRM   
join SurveyResponse  SR on SRM.numRespondantID=SR.numRespondantID  
where SRM.numDomainId = '+convert(varchar(15),@numDomainID)+  ' and bitRegisteredRespondant =1  '  
if @WhereCondition <> ''  
  set @strSql=@strSql+'and(  ' +@WhereCondition+')'  
set @strSql=@strSql+')'  
                                       
                        
                                
                                                
                             
   if (@SortColumnName<>'')                            
  begin                               
    if @vcListItemType1='LI'                                   
    begin                                    
      set @strSql= @strSql +' order by L2.vcData '+@columnSortOrder                                  
    end                                  
    else if @vcListItemType1='S'                                   
    begin                                      
      set @strSql= @strSql +' order by S2.vcState '+@columnSortOrder                                  
    end                                  
    else if @vcListItemType1='T'                                   
    begin                                  
      set @strSql= @strSql +' order by T2.vcTerName '+@columnSortOrder                                  
    end                              
           else  set @strSql= @strSql +' order by '+ @SortColumnName +' ' +@columnSortOrder                            
  end                            
                                                               
insert into #tempTable(numContactID)                   
exec(@strSql)    
print @strSql                                 
set @strSql=''                                  
                                  
SET @bitBillAddressJoinAdded=0;
SET @bitShipAddressJoinAdded=0;
SET @bitContactAddressJoinAdded=0;                            
set @tintOrder=0                                  
set @WhereCondition =''                               
set @strSql='select ADC.numContactId,DM.numDivisionID,DM.numTerID,ADC.numRecOwner,DM.tintCRMType '

	DECLARE @TEMPSelectedColumns TABLE
	(
		numFieldID NUMERIC(18,0)
		,bitCustomField BIT
		,tintOrder INT
		,intColumnWidth FLOAT
	)
	DECLARE @vcLocationID VARCHAR(100) = '0'
	SELECT @vcLocationID =isnull(vcLocationID,'0') from dynamicFormMaster where numFormID=19    

	-- IF USER HAS SELECTED ITS OWN DISPLAY COLUMNS THEN USE IT
	IF LEN(ISNULL(@vcDisplayColumns,'')) > 0
	BEGIN
		DECLARE @TempIDs TABLE
		(
			ID INT IDENTITY(1,1),
			vcFieldID VARCHAR(300)
		)

		INSERT INTO @TempIDs
		(
			vcFieldID
		)
		SELECT 
			OutParam 
		FROM 
			SplitString(@vcDisplayColumns,',')

		INSERT INTO @TEMPSelectedColumns
		(
			numFieldID,
			bitCustomField,
			tintOrder,
			intColumnWidth
		)
		SELECT
			numFormFieldID
			,bitCustomField
			,(SELECT (ID-1) FROM @TempIDs T3 WHERE T3.vcFieldID = TEMP.vcFieldID)
			,0
		FROM
		(
			SELECT  
				numFieldID as numFormFieldID,
				0 bitCustomField,
				CONCAT(numFieldID,'~0') vcFieldID
			FROM    
				View_DynamicDefaultColumns
			WHERE   
				numFormID = 19
				AND bitInResults = 1
				AND bitDeleted = 0 
				AND numDomainID = @numDomainID
			UNION 
			SELECT  
				c.Fld_id AS numFormFieldID
				,1 bitCustomField
				,CONCAT(Fld_id,'~1') vcFieldID
			FROM    
				CFW_Fld_Master C 
			LEFT JOIN 
				CFW_Validation V ON V.numFieldID = C.Fld_id
			JOIN 
				CFW_Loc_Master L on C.GRP_ID=L.Loc_Id
			WHERE   
				C.numDomainID = @numDomainID
				AND GRP_ID IN (SELECT ID FROM dbo.SplitIDs(@vcLocationID,','))
		) TEMP
		WHERE
			vcFieldID IN (SELECT vcFieldID FROM @TempIDs)
	END

	-- IF USER SELECTED DISPLAY COLUMNS IS NOT AVAILABLE THAN GET COLUMNS CONFIGURED FROM SEARCH RESULT GRID
	IF (SELECT COUNT(*) FROM @TEMPSelectedColumns) = 0
	BEGIN
		INSERT INTO @TEMPSelectedColumns
		(
			numFieldID,
			bitCustomField,
			tintOrder,
			intColumnWidth
		)
		SELECT
			numFormFieldID
			,bitCustomField
			,tintOrder
			,intColumnWidth
		FROM
		(
			SELECT  
				A.numFormFieldID,
				0 bitCustomField,
				A.tintOrder,
				A.intColumnWidth
			FROM    AdvSerViewConf A
					JOIN View_DynamicDefaultColumns D ON D.numFieldID = A.numFormFieldID
			WHERE   A.numFormID = 19
					AND D.bitInResults = 1
					AND D.bitDeleted = 0
					AND D.numDomainID = @numDomainID 
					AND D.numFormID = 19
					AND A.numDomainID = @numDomainID
					AND A.numUserCntID=@numUserCntID
					AND ISNULL(A.bitCustom,0) = 0
			UNION
			SELECT  
				A.numFormFieldID,
				1 bitCustomField,
				A.tintOrder,
				A.intColumnWidth
			FROM    AdvSerViewConf A
					JOIN CFW_Fld_Master C ON C.Fld_id = A.numFormFieldID
			WHERE   A.numFormID = 19
					AND C.numDomainID = @numDomainID 
					AND A.numDomainID = @numDomainID
					AND A.numUserCntID=@numUserCntID
					AND ISNULL(A.bitCustom,0) = 1
		) T1
		ORDER BY
			 tintOrder
	END

	-- IF USER HASN'T CONFIGURED COLUMNS FROM SEARCH RESULT GRID THEN RETURN DEFAULT COLUMNS
	IF (SELECT COUNT(*) FROM @TEMPSelectedColumns) = 0
	BEGIN
		INSERT INTO @TEMPSelectedColumns
		(
			numFieldID,
			bitCustomField,
			tintOrder,
			intColumnWidth
		)
		SELECT  
			numFieldID,
			0,
			1,
			0
		FROM    
			View_DynamicDefaultColumns
		WHERE   
			numFormID = 19
			AND bitInResults = 1
			AND bitDeleted = 0 
			AND numDomainID = @numDomainID
			AND numFieldID = 3
	END
                                
	SELECT TOP 1 
		@tintOrder=T1.tintOrder+1
		,@vcDbColumnName=vcDbColumnName
		,@vcFormFieldName=vcFieldName
		,@vcAssociatedControlType=vcAssociatedControlType
		,@vcListItemType=vcListItemType
		,@numListID=numListID                                   
	FROM 
		View_DynamicDefaultColumns D                                   
	INNER JOIN
		@TEMPSelectedColumns T1
	ON 
		D.numFieldID=T1.numFieldID                             
	WHERE 
		D.numFormID = 19 
	ORDER BY 
		T1.tintOrder ASC           
                       
	WHILE @tintOrder>0                                  
	BEGIN                                                           
		IF @vcAssociatedControlType='SelectBox'                                  
		BEGIN                                  
			  if @vcListItemType='LI'                                   
			  begin                  
				 IF @numListID=40--Country
				  BEGIN
	  				set @strSql=@strSql+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcFormFieldName+'~'+ @vcDbColumnName+']'                              
					IF @vcDbColumnName ='numCountry'
					BEGIN
						IF @bitContactAddressJoinAdded=0 
					  BEGIN
							set @WhereCondition= @WhereCondition +' left Join AddressDetails AD on AD.numRecordId= ADC.numContactID and AD.tintAddressOf=1 and AD.tintAddressType=0 AND AD.bitIsPrimary=1 and AD.numDomainID= ADC.numDomainID'	
							SET @bitContactAddressJoinAdded=1;
					  END
					  set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID=AD.numCountry'	
					END
					ELSE IF @vcDbColumnName ='numBillCountry'
					BEGIN
						IF @bitBillAddressJoinAdded=0 
						BEGIN
							set @WhereCondition= @WhereCondition +' left Join AddressDetails AD1 on AD1.numRecordId= DM.numDivisionID and AD1.tintAddressOf=2 and AD1.tintAddressType=1 AND AD1.bitIsPrimary=1 and AD1.numDomainID= ADC.numDomainID '
					
							SET @bitBillAddressJoinAdded=1;
						END
						set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID=AD1.numCountry'
					END
					ELSE IF @vcDbColumnName ='numShipCountry'
					BEGIN
						IF @bitShipAddressJoinAdded=0 
						BEGIN
							set @WhereCondition= @WhereCondition +' left Join AddressDetails AD2 on AD2.numRecordId= DM.numDivisionID and AD2.tintAddressOf=2 and AD2.tintAddressType=2 AND AD2.bitIsPrimary=1 and AD2.numDomainID= ADC.numDomainID '
					
							SET @bitShipAddressJoinAdded=1;
						END
						set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID=AD2.numCountry'
					END
				  END                        
				  ELSE
				  BEGIN                
					   set @strSql=@strSql+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcFormFieldName+'~'+ @vcDbColumnName+']'                                  
					   set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID='+@vcDbColumnName                                  
				  END			   
			  end                                  
			  else if @vcListItemType='S'                                   
			  begin     
				set @strSql=@strSql+',S'+ convert(varchar(3),@tintOrder)+'.vcState'+' ['+ @vcFormFieldName+'~'+ @vcDbColumnName+']'
				IF @vcDbColumnName ='numState'
				BEGIN
					IF @bitContactAddressJoinAdded=0 
					   BEGIN
							set @WhereCondition= @WhereCondition +' left Join AddressDetails AD on AD.numRecordId= ADC.numContactID and AD.tintAddressOf=1 and AD.tintAddressType=0 AND AD.bitIsPrimary=1 and AD.numDomainID= ADC.numDomainID'	
							SET @bitContactAddressJoinAdded=1;
					  END	
					  set @WhereCondition= @WhereCondition +' left join State S'+ convert(varchar(3),@tintOrder)+ ' on S'+convert(varchar(3),@tintOrder)+ '.numStateID=AD.numState'
				END
				ELSE IF @vcDbColumnName ='numBillState'
				BEGIN
					IF @bitBillAddressJoinAdded=0 
					BEGIN
						set @WhereCondition= @WhereCondition +' left Join AddressDetails AD1 on AD1.numRecordId= DM.numDivisionID and AD1.tintAddressOf=2 and AD1.tintAddressType=1 AND AD1.bitIsPrimary=1 and AD1.numDomainID= ADC.numDomainID '
						SET @bitBillAddressJoinAdded=1;
					END
					set @WhereCondition= @WhereCondition +' left join State S'+ convert(varchar(3),@tintOrder)+ ' on S'+convert(varchar(3),@tintOrder)+ '.numStateID=AD1.numState'
				END
				ELSE IF @vcDbColumnName ='numShipState'
				BEGIN
					IF @bitShipAddressJoinAdded=0 
					BEGIN
						set @WhereCondition= @WhereCondition +' left Join AddressDetails AD2 on AD2.numRecordId= DM.numDivisionID and AD2.tintAddressOf=2 and AD2.tintAddressType=2 AND AD2.bitIsPrimary=1 and AD2.numDomainID= ADC.numDomainID '
						SET @bitShipAddressJoinAdded=1;
					END
					set @WhereCondition= @WhereCondition +' left join State S'+ convert(varchar(3),@tintOrder)+ ' on S'+convert(varchar(3),@tintOrder)+ '.numStateID=AD2.numState'
				END
			  end                                  
			  else if @vcListItemType='T'                                   
			  begin                                  
				  set @strSql=@strSql+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcFormFieldName+'~'+ @vcDbColumnName+']'                                  
			   set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID='+@vcDbColumnName                                  
			  end                                  
		end
		ELSE IF @vcAssociatedControlType='TextBox'   
		BEGIN
 			IF @vcDbColumnName='vcStreet' OR @vcDbColumnName='vcCity' OR @vcDbColumnName='vcPostalCode'
			begin      
			PRINT 'hi'                        
			   set @strSql=@strSql+',AD.'+ @vcDbColumnName+' ['+ @vcFormFieldName+'~'+ @vcDbColumnName+']'                              
			   IF @bitContactAddressJoinAdded=0 
			   BEGIN
					set @WhereCondition= @WhereCondition +' left Join AddressDetails AD on AD.numRecordId= ADC.numContactID and AD.tintAddressOf=1 and AD.tintAddressType=0 AND AD.bitIsPrimary=1 and AD.numDomainID= ADC.numDomainID'	
					SET @bitContactAddressJoinAdded=1;
			   END
			   PRINT  @WhereCondition
			end 
			ELSE IF @vcDbColumnName='vcBillStreet' OR @vcDbColumnName='vcBillCity' OR @vcDbColumnName='vcBillPostCode'
			begin                              
			   set @strSql=@strSql+',AD1.'+ REPLACE(REPLACE(@vcDbColumnName,'Bill',''),'PostCode','PostalCode') +' ['+ @vcFormFieldName+'~'+ @vcDbColumnName+']'                              
			   IF @bitBillAddressJoinAdded=0 
			   BEGIN
	   				set @WhereCondition= @WhereCondition +' left Join AddressDetails AD1 on AD1.numRecordId= DM.numDivisionID and AD1.tintAddressOf=2 and AD1.tintAddressType=1 AND AD1.bitIsPrimary=1 and AD1.numDomainID= ADC.numDomainID '
	   				SET @bitBillAddressJoinAdded=1;
			   END
			end 
			ELSE IF @vcDbColumnName='vcShipStreet' OR @vcDbColumnName='vcShipCity' OR @vcDbColumnName='vcShipPostCode'
			begin                              
			   set @strSql=@strSql+',AD2.'+ REPLACE(REPLACE(@vcDbColumnName,'Ship',''),'PostCode','PostalCode') +' ['+ @vcFormFieldName+'~'+ @vcDbColumnName+']'                              
			   IF @bitShipAddressJoinAdded=0 
			   BEGIN
	   				set @WhereCondition= @WhereCondition +' left Join AddressDetails AD2 on AD2.numRecordId= DM.numDivisionID and AD2.tintAddressOf=2 and AD2.tintAddressType=2  AND AD2.bitIsPrimary=1 and AD2.numDomainID= ADC.numDomainID '
	   				SET @bitShipAddressJoinAdded=1;
			   END
			end 
			ELSE
			BEGIN
				set @strSql=@strSql+','+ case  when @vcDbColumnName='numAge' then 'year(getutcdate())-year(bintDOB)' when @vcDbColumnName='numDivisionID' then 'DM.numDivisionID' else @vcDbColumnName end+' ['+ @vcFormFieldName+'~'+ @vcDbColumnName+']'             
			END
		END
		ELSE 
		BEGIN
			SET @strSql=@strSql+','+ case  when @vcDbColumnName='numAge' then 'year(getutcdate())-year(bintDOB)' when @vcDbColumnName='numDivisionID' then 'DM.numDivisionID' else @vcDbColumnName end+' ['+ @vcFormFieldName+'~'+ @vcDbColumnName+']'             
		END
  
		SELECT TOP 1 
			@tintOrder=T1.tintOrder+1
			,@vcDbColumnName=vcDbColumnName
			,@vcFormFieldName=vcFieldName
			,@vcAssociatedControlType=vcAssociatedControlType
			,@vcListItemType=vcListItemType
			,@numListID=numListID                                   
		FROM 
			View_DynamicDefaultColumns D                                   
		INNER JOIN
			@TEMPSelectedColumns T1
		ON 
			D.numFieldID=T1.numFieldID                             
		WHERE 
			D.numFormID = 19
			AND T1.tintOrder > @tintOrder-1 
		ORDER BY 
			T1.tintOrder ASC
                                       
		IF @@rowcount=0 
			SET @tintOrder=0                                  
	END                                  
                                  
                                  
                                  
                                  
                             
                                                                        
  declare @firstRec as integer                                                                        
  declare @lastRec as integer                                                                        
 set @firstRec= (@CurrentPage-1) * @PageSize                          
 set @lastRec= (@CurrentPage*@PageSize+1)                                                                         
set @TotRecs=(select count(*) from #tempTable)                                                       
                                              
if @GetAll=0
begin           
            
set @strSql=@strSql+'FROM AdditionalContactsInformation ADC                                                       
  JOIN DivisionMaster DM ON ADC.numDivisionId = DM.numDivisionID                                                      
  JOIN CompanyInfo C ON DM.numCompanyID = C.numCompanyId                                  
   '+@WhereCondition+' join #tempTable T on T.numContactId=ADC.numContactId                                                                   
  WHERE ID > '+convert(varchar(10),@firstRec)+ ' and ID <'+ convert(varchar(10),@lastRec)                           
  
end
else
begin
  set @strSql=@strSql+'FROM AdditionalContactsInformation ADC                                                       
  JOIN DivisionMaster DM ON ADC.numDivisionId = DM.numDivisionID                                                      
  JOIN CompanyInfo C ON DM.numCompanyID = C.numCompanyId                                  
   '+@WhereCondition+' join #tempTable T on T.numContactId=ADC.numContactId'

end
 print @strSql                                                          
exec(@strSql)                                                                     
drop table #tempTable
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_EDIQueueLog_Insert')
DROP PROCEDURE dbo.USP_EDIQueueLog_Insert
GO
CREATE PROCEDURE [dbo].[USP_EDIQueueLog_Insert]
(
	@numEDIQueueID NUMERIC(18,0),
	@EDIType INT,
	@numDomainID NUMERIC(18,0),
    @numOppID NUMERIC(18,0),
	@vcLog NTEXT,
	@bitSuccess BIT,
	@vcExceptionMessage NTEXT
)
AS 
BEGIN
	INSERT INTO EDIQueueLog
	(
		numEDIQueueID
		,numDomainID
		,numOppID
		,vcLog
		,bitSuccess
		,vcExceptionMessage
		,dtDate
	)
	VALUES
	(
		@numEDIQueueID
		,@numDomainID
		,@numOppID
		,@vcLog
		,@bitSuccess
		,@vcExceptionMessage
		,GETUTCDATE()
	)

	IF ISNULL(@numEDIQueueID,0) > 0
	BEGIN
		UPDATE 
			EDIQueue
		SET
			bitExecuted=1
			,bitSuccess=@bitSuccess
			,dtExecutionDate=GETUTCDATE()
		WHERE
			ID=@numEDIQueueID
	END

	DECLARE @tintOppType AS TINYINT
	DECLARE @tintOppStatus AS TINYINT
	IF @EDIType = 940 
	BEGIN
		IF ISNULL(@bitSuccess,0) = 0
		BEGIN
			SELECT @tintOppStatus=tintOppStatus,@tintOppType=tintOppType from OpportunityMaster where numOppID=@numOppID
			UPDATE OpportunityMaster SET numStatus=15446,bintModifiedDate=GETUTCDATE(),tintEDIStatus=8 WHERE numOppId=@numOppID --15446: Shipment Request (940) Failed

			IF @tintOppType=1 AND @tintOppStatus=1
			BEGIN
				EXEC USP_SalesFulfillmentQueue_Save @numDomainID,0,@numOppID,15446
			END

		 	 IF EXISTS (SELECT 1 FROM dbo.OpportunityMaster WHERE numOppId = @numOppID AND tintOppType=1 AND isnull(tintOppStatus,0)=1 AND ISNULL(numStatus,0)!= 15446)
			 BEGIN
	   				EXEC USP_ManageOpportunityAutomationQueue
							@numOppQueueID = 0, -- numeric(18, 0)
							@numDomainID = @numDomainID, -- numeric(18, 0)
							@numOppId = @numOppID, -- numeric(18, 0)
							@numOppBizDocsId = 0, -- numeric(18, 0)
							@numOrderStatus = 15446, -- numeric(18, 0)
							@numUserCntID = 0, -- numeric(18, 0)
							@tintProcessStatus = 1, -- tinyint
							@tintMode = 1 -- TINYINT
			END 
		END
		ELSE
		BEGIN
			IF NOT EXISTS (SELECT ID FROM EDIHistory WHERE numOppID=@numOppID AND tintEDIStatus=3)
			BEGIN
				UPDATE OpportunityMaster SET tintEDIStatus=3,bintModifiedDate=GETUTCDATE() WHERE numOppId=@numOppID --3: 940 Sent
			
				INSERT INTO EDIHistory
				(
					numDomainID,numOppID,tintEDIStatus,dtDate
				)
				VALUES
				(
					@numDomainID,@numOppID,3,GETUTCDATE()
				)
			END
			
		END
	END
	ELSE IF @EDIType = 940997 -- 940 Acknowledge
	BEGIN
		IF NOT EXISTS (SELECT ID FROM EDIHistory WHERE numOppID=@numOppID AND tintEDIStatus=4)
		BEGIN
			UPDATE OpportunityMaster SET tintEDIStatus=4,bintModifiedDate=GETUTCDATE() WHERE numOppId=@numOppID --3: 940 Acknowledged
			
			INSERT INTO EDIHistory
			(
				numDomainID,numOppID,tintEDIStatus,dtDate
			)
			VALUES
			(
				@numDomainID,@numOppID,4,GETUTCDATE()
			)
		END
	END
	ELSE IF @EDIType = 8565 AND ISNULL(@bitSuccess,0) = 1 -- 856 Received from 3PL
	BEGIN
		IF NOT EXISTS (SELECT ID FROM EDIHistory WHERE numOppID=@numOppID AND tintEDIStatus=5)
		BEGIN
			UPDATE OpportunityMaster SET tintEDIStatus=5,bintModifiedDate=GETUTCDATE() WHERE numOppId=@numOppID --5: 856 Received
			
			INSERT INTO EDIHistory
			(
				numDomainID,numOppID,tintEDIStatus,dtDate
			)
			VALUES
			(
				@numDomainID,@numOppID,5,GETUTCDATE()
			)
		END
	END
	ELSE IF @EDIType = 856997 AND ISNULL(@bitSuccess,0) = 1 -- 856 Acknowledged
	BEGIN
		IF NOT EXISTS (SELECT ID FROM EDIHistory WHERE numOppID=@numOppID AND tintEDIStatus=6)
		BEGIN
			UPDATE OpportunityMaster SET tintEDIStatus=6,bintModifiedDate=GETUTCDATE() WHERE numOppId=@numOppID --6: 856 Acknowledged
			
			INSERT INTO EDIHistory
			(
				numDomainID,numOppID,tintEDIStatus,dtDate
			)
			VALUES
			(
				@numDomainID,@numOppID,6,GETUTCDATE()
			)
		END
	END
	ELSE IF @EDIType = 856810997 AND ISNULL(@bitSuccess,0) = 1 -- 856/810 Acknowledged
	BEGIN
		IF NOT EXISTS (SELECT ID FROM EDIHistory WHERE numOppID=@numOppID AND tintEDIStatus=10)
		BEGIN
			UPDATE OpportunityMaster SET tintEDIStatus=10,bintModifiedDate=GETUTCDATE() WHERE numOppId=@numOppID --10: 856/810 Acknowledged
			
			INSERT INTO EDIHistory
			(
				numDomainID,numOppID,tintEDIStatus,dtDate
			)
			VALUES
			(
				@numDomainID,@numOppID,10,GETUTCDATE()
			)
		END
	END
	ELSE IF @EDIType = 8566 -- 856 Sent to EDI
	BEGIN
		IF ISNULL(@bitSuccess,0) = 0
		BEGIN
			SELECT @tintOppStatus=tintOppStatus,@tintOppType=tintOppType from OpportunityMaster where numOppID=@numOppID
			UPDATE OpportunityMaster SET numStatus=15448,bintModifiedDate=GETUTCDATE(),tintEDIStatus=9 WHERE numOppId=@numOppID --15448: Send 856 & 810 Failed

			IF @tintOppType=1 AND @tintOppStatus=1
			BEGIN
				EXEC USP_SalesFulfillmentQueue_Save @numDomainID,0,@numOppID,15448
			END

		 	 IF EXISTS (SELECT 1 FROM dbo.OpportunityMaster WHERE numOppId = @numOppID AND tintOppType=1 AND isnull(tintOppStatus,0)=1 AND ISNULL(numStatus,0) != 15448)
			 BEGIN
	   				EXEC USP_ManageOpportunityAutomationQueue
							@numOppQueueID = 0, -- numeric(18, 0)
							@numDomainID = @numDomainID, -- numeric(18, 0)
							@numOppId = @numOppID, -- numeric(18, 0)
							@numOppBizDocsId = 0, -- numeric(18, 0)
							@numOrderStatus = 15448, -- numeric(18, 0)
							@numUserCntID = 0, -- numeric(18, 0)
							@tintProcessStatus = 1, -- tinyint
							@tintMode = 1 -- TINYINT
			END 
		END
		ELSE
		BEGIN
			IF NOT EXISTS (SELECT ID FROM EDIHistory WHERE numOppID=@numOppID AND tintEDIStatus=7)
			BEGIN
				UPDATE OpportunityMaster SET tintEDIStatus=7,bintModifiedDate=GETUTCDATE() WHERE numOppId=@numOppID --7: 856 Sent
			
				INSERT INTO EDIHistory
				(
					numDomainID,numOppID,tintEDIStatus,dtDate
				)
				VALUES
				(
					@numDomainID,@numOppID,7,GETUTCDATE()
				)
			END
		END
	END

	ELSE IF @EDIType = 85011 AND ISNULL(@bitSuccess,0) = 1   --- 850 SO Partially Created
	BEGIN
		BEGIN
		IF NOT EXISTS (SELECT ID FROM EDIHistory WHERE numOppID=@numOppID AND tintEDIStatus=11)
			UPDATE OpportunityMaster SET tintEDIStatus=11,bintModifiedDate=GETUTCDATE() WHERE numOppId=@numOppID --11: 850 SO Partially Created
			
			INSERT INTO EDIHistory
			(
				numDomainID,numOppID,tintEDIStatus,dtDate
			)
			VALUES
			(
				@numDomainID,@numOppID,11,GETUTCDATE()
			)
		END
	END

	ELSE IF @EDIType = 85012 AND ISNULL(@bitSuccess,0) = 1   --- 850 SO Created
	BEGIN
		BEGIN
		IF NOT EXISTS (SELECT ID FROM EDIHistory WHERE numOppID=@numOppID AND tintEDIStatus=12)
			UPDATE OpportunityMaster SET tintEDIStatus=12,bintModifiedDate=GETUTCDATE() WHERE numOppId=@numOppID --12: 850 SO Created
			
			INSERT INTO EDIHistory
			(
				numDomainID,numOppID,tintEDIStatus,dtDate
			)
			VALUES
			(
				@numDomainID,@numOppID,12,GETUTCDATE()
			)
		END
	END
	
END
GO
/****** Object:  StoredProcedure [dbo].[usp_GetCompanyInfoDtlPl]    Script Date: 07/26/2008 16:16:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--usp_GetCompanyInfoDtlPl 91099,72,-333
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcompanyinfodtlpl')
DROP PROCEDURE usp_getcompanyinfodtlpl
GO
CREATE PROCEDURE [dbo].[usp_GetCompanyInfoDtlPl]                                                                                                                   
@numDivisionID numeric(9) ,                      
@numDomainID numeric(9) ,        
@ClientTimeZoneOffset as int                              
    --                                                                           
AS                                                                            
BEGIN                                                                            
 SELECT CMP.numCompanyID,               
 CMP.vcCompanyName,                
 (select vcdata from listdetails where numListItemID = DM.numStatusID) as numCompanyStatusName,
 DM.numStatusID as numCompanyStatus,                                                                           
 (select vcdata from listdetails where numListItemID = CMP.numCompanyRating) as numCompanyRatingName, 
 CMP.numCompanyRating,             
 (select vcdata from listdetails where numListItemID = CMP.numCompanyType) as numCompanyTypeName,
 CMP.numCompanyType,              
 CMP.numCompanyType as numCmptype,               
 DM.bitPublicFlag,                 
 DM.numDivisionID,                                   
 DM. vcDivisionName,        
dbo.getCompanyAddress(dm.numdivisionId,1,dm.numDomainID) as Address,
dbo.getCompanyAddress(dm.numdivisionId,2,dm.numDomainID) as ShippingAddress,          
  isnull(AD2.vcStreet,'') as vcShipStreet,
  isnull(AD2.vcCity,'') as vcShipCity,
  isnull(dbo.fn_GetState(AD2.numState),'')  as vcShipState,
  ISNULL((SELECT numShippingZone FROM [State] WHERE (numDomainID=@numDomainID OR ConstFlag=1) AND numStateID=AD2.numState),0) AS numShippingZone,
 (select vcdata from listdetails where numListItemID = DM.numFollowUpStatus) as numFollowUpStatusName,
 DM.numFollowUpStatus,                
 CMP.vcWebLabel1,               
 CMP.vcWebLink1,               
 CMP.vcWebLabel2,               
 CMP.vcWebLink2,                                                                             
 CMP.vcWebLabel3,               
 CMP.vcWebLink3,               
 CMP.vcWeblabel4,               
 CMP.vcWebLink4,                                                                       
 DM.tintBillingTerms,              
 DM.numBillingDays,              
 DM.tintInterestType,              
 DM.fltInterest,                                                  
 isnull(AD2.vcPostalCode,'') as vcShipPostCode, 
 AD2.numCountry vcShipCountry,                          
 DM.bitPublicFlag,              
 (select vcdata from listdetails where numListItemID = DM.numTerID)  as numTerIDName,
 DM.numTerID,                                                                         
 (select vcdata from listdetails where numListItemID = CMP.numCompanyIndustry) as numCompanyIndustryName, 
  CMP.numCompanyIndustry,              
 (select vcdata from listdetails where numListItemID = CMP.numCompanyCredit) as  numCompanyCreditName,
 CMP.numCompanyCredit,                                                                             
 isnull(CMP.txtComments,'') as txtComments,               
 isnull(CMP.vcWebSite,'') vcWebSite,              
 (select vcdata from listdetails where numListItemID = CMP.numAnnualRevID) as  numAnnualRevIDName,
 CMP.numAnnualRevID,                
 (select vcdata from listdetails where numListItemID = CMP.numNoOfEmployeesID) as numNoOfEmployeesIDName,               
CMP.numNoOfEmployeesID,                                                                           
    (select vcdata from listdetails where numListItemID = CMP.vcProfile) as vcProfileName, 
 CMP.vcProfile,              
 DM.numCreatedBy,               
 dbo.fn_GetContactName(DM.numRecOwner) as RecOwner,               
 dbo.fn_GetContactName(DM.numCreatedBy)+' ' + convert(varchar(20),dateadd(minute,-@ClientTimeZoneOffset,DM.bintCreatedDate)) as vcCreatedBy,               
 DM.numRecOwner,                
 dbo.fn_GetContactName(DM.numModifiedBy)+' ' +convert(varchar(20),dateadd(minute,-@ClientTimeZoneOffset,DM.bintModifiedDate)) as vcModifiedBy,                    
 isnull(DM.vcComPhone,'') as vcComPhone, 
 DM.vcComFax,                   
 DM.numGrpID,  
 (select vcGrpName from Groups where numGrpId= DM.numGrpID) as numGrpIDName,              
 (select vcdata from listdetails where numListItemID = CMP.vcHow) as vcInfoSourceName,
 CMP.vcHow as vcInfoSource,                                                                            
 DM.tintCRMType,                                                                         
 (SELECT vcCampaignName FROM CampaignMaster WHERE numCampaignID= DM.numCampaignID) as  numCampaignIDName,
 DM.numCampaignID,              
 DM.numAssignedBy, isnull(DM.bitNoTax,0) bitNoTax,                     
 (select A.vcFirstName+' '+A.vcLastName                       
 from AdditionalContactsInformation A                  
 join DivisionMaster D                        
 on D.numDivisionID=A.numDivisionID
 where A.numContactID=DM.numAssignedTo) as numAssignedToName,DM.numAssignedTo,
 (select  count(*) from dbo.GenericDocuments where numRecID= @numDivisionID and vcDocumentSection='A' AND tintDocumentType=2) as DocumentCount ,
 (SELECT count(*)from CompanyAssociations where numDivisionID=@numDivisionID and bitDeleted=0 ) as AssociateCountFrom,                            
 (SELECT count(*)from CompanyAssociations where numAssociateFromDivisionID=@numDivisionID and bitDeleted=0 ) as AssociateCountTo,
 (select vcdata from listdetails where numListItemID = DM.numCompanyDiff) as numCompanyDiffName,               
 DM.numCompanyDiff,DM.vcCompanyDiff,isnull(DM.bitActiveInActive,1) as bitActiveInActive,
(SELECT SUBSTRING(
	(SELECT ','+  A.vcFirstName+' ' +A.vcLastName + CASE WHEN ISNULL(SR.numContactType,0)>0 THEN '(' + dbo.fn_GetListItemName(SR.numContactType) + ')' ELSE '' END
	FROM ShareRecord SR JOIN UserMaster UM ON UM.numUserDetailId=SR.numAssignedTo 
							 JOIN AdditionalContactsInformation A ON UM.numUserDetailId=A.numContactID 
		 WHERE SR.numDomainID=DM.numDomainID AND SR.numModuleID=1 AND SR.numRecordID=DM.numDivisionID
				AND UM.numDomainID=DM.numDomainID and UM.numDomainID=A.numDomainID FOR XML PATH('')),2,200000)) AS numShareWith,
 DM.bitOnCreditHold,
 DM.numDefaultPaymentMethod,
 DM.numAccountClassID,
 DM.tintPriceLevel,DM.vcPartnerCode as vcPartnerCode  ,
  ISNULL(DM.numPartenerSource,0) AS numPartenerSourceId,
 D3.vcPartnerCode+'-'+C3.vcCompanyName as numPartenerSource,
 ISNULL(DM.numPartenerContact,0) AS numPartenerContactId,
A1.vcFirstName+' '+A1.vcLastName AS numPartenerContact,
ISNULL(DM.bitEmailToCase,0) bitEmailToCase,
'' AS vcCreditCards,
----Fetch Last Followup(Template) and EmailSentDate(if any) Value
		  
	(CASE WHEN A2.numECampaignID > 0 THEN dbo.fn_FollowupDetailsInOrgList(A2.numContactID,1,@numDomainID) ELSE '' END) vcLastFollowup,

		----Fetch Last Followup(Template) Value

		----Fetch Next Followup(Template) and ExecutionDate Value
	(CASE WHEN A2.numECampaignID > 0 THEN dbo.fn_FollowupDetailsInOrgList(A2.numContactID,2,@numDomainID) ELSE '' END) vcNextFollowup,										 

		----Fetch Next Followup(Template) Value
(CASE WHEN (SELECt COUNT(*) FROM ExtarnetAccounts WHERE numDivisionID=DM.numDivisionID) >0 THEN 1 ELSE 0 END) AS bitEcommerceAccess,

ISNULL(DM.numDefaultShippingServiceID,0) numShippingService,
ISNULL((SELECT vcShipmentService FROM ShippingService WHERE (numDomainID=@numDomainID OR ISNULL(numDomainID,0)=0) AND numShippingServiceID = DM.numDefaultShippingServiceID),'') AS vcShippingService,
		CASE 
			WHEN vcSignatureType = 0 THEN 'Service Default'
			WHEN vcSignatureType = 1 THEN 'Adult Signature Required'
			WHEN vcSignatureType = 2 THEN 'Direct Signature'
			WHEN vcSignatureType = 3 THEN 'InDirect Signature'
			WHEN vcSignatureType = 4 THEN 'No Signature Required'
			ELSE ''
		END AS vcSignatureType,
		ISNULL(DM.intShippingCompany,0) intShippingCompany,
		CASE WHEN ISNULL(DM.intShippingCompany,0) = 0 THEN '' ELSE ISNULL((SELECT vcData FROM ListDetails WHERE numListID = 82 AND numListItemID=ISNULL(DM.intShippingCompany,0)),'') END AS vcShipVia,
        A2.vcFirstName AS vcFirstName,
		A2.vcLastName  AS vcLastName,
		A2.numPhone AS numPhone,
		A2.numPhoneExtension As numPhoneExtension,
		A2.vcEmail As vcEmail		     
		                       
 FROM  CompanyInfo CMP    
  join DivisionMaster DM    
  on DM.numCompanyID=CMP.numCompanyID                                                                                                                                        
   LEFT JOIN dbo.AddressDetails AD1 ON AD1.numDomainID=DM.numDomainID 
   AND AD1.numRecordID=DM.numDivisionID AND AD1.tintAddressOf=2 AND AD1.tintAddressType=1 AND AD1.bitIsPrimary=1
   LEFT JOIN dbo.AddressDetails AD2 ON AD2.numDomainID=DM.numDomainID 
   AND AD2.numRecordID= DM.numDivisionID AND AD2.tintAddressOf=2 AND AD2.tintAddressType=2 AND AD2.bitIsPrimary=1
    LEFT JOIN divisionMaster D3 ON DM.numPartenerSource = D3.numDivisionID
  LEFT JOIN CompanyInfo C3 ON C3.numCompanyID = D3.numCompanyID    
  LEFT JOIN AdditionalContactsInformation A1 ON DM.numPartenerContact = A1.numContactId 
  LEft join   AdditionalContactsInformation A2 on   DM.numDivisionID = A2.numDivisionId and A2.bitPrimaryContact = 1
  LEFT JOIN DivisionMasterShippingConfiguration ON DM.numDivisionID=DivisionMasterShippingConfiguration.numDivisionID
 where DM.numDivisionID=@numDivisionID   and DM.numDomainID=@numDomainID                                                                                          
END

GO




/****** Object:  StoredProcedure [dbo].[USP_GetDealsList1]    Script Date: 05/07/2009 22:12:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getdealslist1')
DROP PROCEDURE usp_getdealslist1
GO
CREATE PROCEDURE [dbo].[USP_GetDealsList1]
               @numUserCntID              NUMERIC(9)  = 0,
               @numDomainID               NUMERIC(9)  = 0,
               @tintSalesUserRightType    TINYINT  = 0,
               @tintPurchaseUserRightType TINYINT  = 0,
               @tintSortOrder             TINYINT  = 4,
               @CurrentPage               INT,
               @PageSize                  INT,
               @TotRecs                   INT  OUTPUT,
               @columnName                AS VARCHAR(50),
               @columnSortOrder           AS VARCHAR(10),
               @numCompanyID              AS NUMERIC(9)  = 0,
               @bitPartner                AS BIT  = 0,
               @ClientTimeZoneOffset      AS INT,
               @tintShipped               AS TINYINT,
               @intOrder                  AS TINYINT,
               @intType                   AS TINYINT,
               @tintFilterBy              AS TINYINT  = 0,
               @numOrderStatus			  AS NUMERIC,
			   @vcRegularSearchCriteria varchar(MAX)='',
			   @vcCustomSearchCriteria varchar(MAX)='',
			   @SearchText VARCHAR(300) = '',
			   @SortChar char(1)='0',
			   @tintDashboardReminderType TINYINT = 0
AS
	DECLARE  @PageId  AS TINYINT
	DECLARE  @numFormId  AS INT 
  
	SET @PageId = 0

	IF @inttype = 1
	BEGIN
		SET @PageId = 2
		SET @inttype = 3
		SET @numFormId=39
	END
	ELSE IF @inttype = 2
	BEGIN
		SET @PageId = 6
		SET @inttype = 4
		SET @numFormId=41
	END

	CREATE TABLE #tempForm 
	(
		tintOrder TINYINT,vcDbColumnName nvarchar(50),vcOrigDbColumnName nvarchar(50),vcFieldName nvarchar(50),vcAssociatedControlType nvarchar(50),vcListItemType varchar(3),
		numListID numeric(9),vcLookBackTableName varchar(50),bitCustomField BIT,numFieldId NUMERIC,bitAllowSorting BIT,bitAllowEdit BIT,bitIsRequired BIT,bitIsEmail BIT,
		bitIsAlphaNumeric BIT,bitIsNumeric BIT,bitIsLengthValidation BIT,intMaxLength int,intMinLength int,bitFieldMessage BIT,vcFieldMessage VARCHAR(500),
		ListRelID numeric(9),intColumnWidth int,bitAllowFiltering BIT,vcFieldDataType CHAR(1)
	)

	-- FIRST CHECK IF GRID COLUMNS ARE CONFIGURED
	DECLARE  @Nocolumns  AS TINYINT = 0

	SELECT 
		@Nocolumns=ISNULL(SUM(TotalRow),0) 
	FROM
	(            
		SELECT 
			COUNT(*) TotalRow 
		FROM 
			View_DynamicColumns 
		WHERE 
			numFormId=@numFormId 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND numRelCntType = @numFormId
		UNION 
		SELECT 
			COUNT(*) TotalRow 
		FROM 
			View_DynamicCustomColumns 
		WHERE 
			numFormId=@numFormId 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND numRelCntType = @numFormId
	) TotalRows

	-- IF GRID COLUMNS ARE NOT CONFIGURES THEN CHECK IF MASTER FORM CONFIGURATION IS CREATED BY ADMINISTRATOR
	DECLARE @IsMasterConfAvailable BIT = 0
	DECLARE @numUserGroup NUMERIC(18,0)

	IF @Nocolumns=0
	BEGIN
		SELECT @numUserGroup = numGroupID FROM UserMaster WHERE numUserDetailId = @numUserCntID 

		IF (SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = @numDomainID AND numFormID = @numFormId AND numRelCntType=@numFormId AND bitGridConfiguration = 1 AND numGroupID = @numUserGroup) > 0
		BEGIN
			SET @IsMasterConfAvailable = 1
		END
	END

	--IF MASTERCONFIGURATION IS AVAILABLE THEN LOAD IT OTHERWISE LOAD DEFAULT COLUMNS
	IF @IsMasterConfAvailable = 1
	BEGIN
		INSERT INTO DycFormConfigurationDetails 
		(
			numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth
		)
		SELECT 
			@numFormId,numFieldId,intColumnNum,intRowNum,@numDomainID,@numUserCntID,@numFormId,1,0,intColumnWidth
		FROM 
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId=@numFormId AND 
			View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicColumnsMasterConfig.numRelCntType=@numFormId AND 
			View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
			ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
		UNION
		SELECT 
			@numFormId,numFieldId,intColumnNum,intRowNum,@numDomainID,@numUserCntID,@numFormId,1,1,intColumnWidth
		FROM 
			View_DynamicCustomColumnsMasterConfig
		WHERE 
			View_DynamicCustomColumnsMasterConfig.numFormId=@numFormId AND 
			View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicCustomColumnsMasterConfig.numRelCntType=@numFormId AND 
			View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1


		INSERT INTO 
			#tempForm
		SELECT 
			(intRowNum + 1) as tintOrder, vcDbColumnName,	vcOrigDbColumnName,	ISNULL(vcCultureFieldName,vcFieldName), vcAssociatedControlType,
			vcListItemType, numListID, vcLookBackTableName,	bitCustom, numFieldId, bitAllowSorting,	bitInlineEdit, bitIsRequired,
			bitIsEmail,	bitIsAlphaNumeric, bitIsNumeric, bitIsLengthValidation,	intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,
			ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType 
		FROM 
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId=@numFormId AND 
			View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicColumnsMasterConfig.numRelCntType=@numFormId AND 
			View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
			ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
		UNION
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName, vcAssociatedControlType,'' as vcListItemType,numListID,'',
			bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,''
		FROM 
			View_DynamicCustomColumnsMasterConfig
		WHERE 
			View_DynamicCustomColumnsMasterConfig.numFormId=@numFormId AND 
			View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicCustomColumnsMasterConfig.numRelCntType=@numFormId AND 
			View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1
		ORDER BY 
			tintOrder ASC  
	END
	ELSE                                        
	BEGIN
		IF @Nocolumns=0
		BEGIN
			INSERT INTO DycFormConfigurationDetails 
			(
				numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth
			)
			SELECT 
				@numFormId,numFieldId,0,Row_number() over(order by tintRow desc),@numDomainID,@numUserCntID,@numFormId,1,0,intColumnWidth
			FROM 
				View_DynamicDefaultColumns
			WHERE 
				numFormId=@numFormId 
				AND bitDefault=1 
				AND ISNULL(bitSettingField,0)=1 
				AND numDomainID=@numDomainID
			ORDER BY 
				tintOrder asc   
		END

		INSERT INTO 
			#tempForm
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcOrigDbColumnName,ISNULL(vcCultureFieldName,vcFieldName),                  
			vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName,bitCustom,numFieldId,bitAllowSorting,bitInlineEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength,bitFieldMessage,
			vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType  
		FROM 
			View_DynamicColumns 
		WHERE 
			numFormId=@numFormId 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND ISNULL(bitSettingField,0)=1 
			AND ISNULL(bitCustom,0)=0  
			AND numRelCntType = @numFormId
		UNION
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName,vcAssociatedControlType,'' as vcListItemType,numListID,'',bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,'' 
		FROM 
			View_DynamicCustomColumns
		WHERE 
			numFormId=@numFormId 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND ISNULL(bitCustom,0)=1 
			AND numRelCntType = @numFormId
		ORDER BY 
			tintOrder asc  
	END

	DECLARE  @strColumns AS VARCHAR(MAX)
	SET @strColumns = ' ADC.numContactId, ADC.vcEmail, Div.numDivisionID,ISNULL(Div.numTerID,0) numTerID, opp.numRecOwner as numRecOwner, Div.tintCRMType, opp.numOppId, ISNULL(Opp.monDealAmount,0) monDealAmount 
	,ISNULL(opp.numShippingService,0) AS numShippingService'
	--Corr.bitIsEmailSent,

	DECLARE  @tintOrder  AS TINYINT;SET @tintOrder = 0
	DECLARE  @vcFieldName  AS VARCHAR(50)
	DECLARE  @vcListItemType  AS VARCHAR(3)
	DECLARE  @vcListItemType1  AS VARCHAR(1)
	DECLARE  @vcAssociatedControlType VARCHAR(30)
	DECLARE  @numListID  AS NUMERIC(9)
	DECLARE  @vcDbColumnName VARCHAR(40)
	DECLARE  @WhereCondition VARCHAR(2000);SET @WhereCondition = ''
	DECLARE  @vcLookBackTableName VARCHAR(2000)
	DECLARE  @bitCustom  AS BIT
	Declare @numFieldId as numeric  
	DECLARE @bitAllowSorting AS CHAR(1)   
	DECLARE @bitAllowEdit AS CHAR(1)                 
	DECLARE @vcColumnName AS VARCHAR(500)          
	
	Declare @ListRelID as numeric(9) 
	DECLARE @SearchQuery AS VARCHAR(MAX) = ''        

	SELECT TOP 1 
		@tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName,@vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,
		@numListID=numListID,@vcLookBackTableName=vcLookBackTableName ,@bitCustom=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,@bitAllowEdit=bitAllowEdit,
		@ListRelID=ListRelID
	FROM 
		#tempForm 
	ORDER BY 
		tintOrder ASC            
--	SET @strColumns = @strColumns + ' , (SELECT SUBSTRING((SELECT '', '' + cast(I.vcItemName as varchar)
--                      FROM OpportunityItems AS t
--					  LEFT JOIN OpportunityBizDocs as BC
--					  ON t.numOppId=BC.numOppId
--LEFT JOIN Item as I
--ON t.numItemCode=I.numItemCode
--WHERE t.numOppId=Opp.numOppId AND t.numoppItemtCode NOT IN (SELECT numOppItemId FROM OpportunityBizDocItems WHERE numOppBizDocID=BC.numOppBizDocsId)
--                     FOR XML PATH('''')), 2, 200000)  )[List_Item_Approval_UNIT] '
--SET @strColumns = @strColumns + ' , (SELECT 
-- SUBSTRING((SELECT '', '' + cast(I.vcItemName as varchar)
--FROM 
-- OpportunityItems AS t
--LEFT JOIN 
-- Item as I
--ON 
-- t.numItemCode=I.numItemCode
--WHERE 
-- t.numOppId=Opp.numOppId AND t.numoppItemtCode NOT IN (SELECT numOppItemId FROM OpportunityBizDocs JOIN OpportunityBizDocItems ON OpportunityBizDocs.numOppBizDocsId=OpportunityBizDocItems.numOppBizDocID  WHERE OpportunityBizDocs.numOppId=Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1)
--                     FOR XML PATH('''')), 2, 200000)  )[List_Item_Approval_UNIT] '
	WHILE @tintOrder > 0
    BEGIN
		IF @bitCustom = 0
		BEGIN
			DECLARE  @Prefix  AS VARCHAR(5)

			IF @vcLookBackTableName = 'AdditionalContactsInformation'
				SET @Prefix = 'ADC.'
			IF @vcLookBackTableName = 'DivisionMaster'
				SET @Prefix = 'Div.'
			IF @vcLookBackTableName = 'OpportunityMaster'
				SET @PreFix = 'Opp.'
			IF @vcLookBackTableName = 'OpportunityRecurring'
				SET @PreFix = 'OPR.'
			IF @vcLookBackTableName = 'CompanyInfo'
				SET @PreFix = 'cmp.'
			IF @vcLookBackTableName = 'ProjectProgress'
				SET @PreFix = 'PP'
			SET @vcColumnName= @vcDbColumnName+'~'+ convert(varchar(10),@numFieldId) +'~0'

			IF @vcAssociatedControlType = 'SelectBox'
			BEGIN
				-- KEEP THIS IF CONDITION SEPERATE FROM IF ELSE
				IF @vcDbColumnName = 'numStatus'
				BEGIN
					SET @strColumns=@strColumns+ ' ,(SELECT COUNT(OpportunityItems.numoppitemtCode) FROM OpportunityItems WHERE OpportunityItems.bitItemPriceApprovalRequired=1 AND OpportunityItems.numOppId=Opp.numOppId) ApprovalMarginCount'
				END


				IF @vcDbColumnName = 'numPartner'
				BEGIN
					SET @strColumns=@strColumns+ ' ,(Opp.numPartner) '+' ['+ @vcColumnName+']'    
					SET @strColumns=@strColumns+ ' ,(SELECT D.vcPartnerCode+''-''+C.vcCompanyName FROM DivisionMaster AS D LEFT JOIN CompanyInfo AS C ON D.numCompanyID=C.numCompanyID WHERE D.numDivisionID=Opp.numPartner) AS vcPartner' 
				END
				ELSE IF @vcDbColumnName = 'vcSignatureType'
				BEGIN
					SET @strColumns=@strColumns+ ' ,(SELECT CASE WHEN vcSignatureType = 0 THEN ''Service Default''
																	WHEN vcSignatureType = 1 THEN ''Adult Signature Required''
																	WHEN vcSignatureType = 2 THEN ''Direct Signature''
																	WHEN vcSignatureType = 3 THEN ''InDirect Signature''
																	WHEN vcSignatureType = 4 THEN ''No Signature Required''
																ELSE '''' END 
					FROM DivisionMaster AS D INNER JOIN DivisionMasterShippingConfiguration AS DMSC ON DMSC.numDivisionID=D.numDivisionID 
					WHERE D.numDivisionID=Div.numDivisionID) '  + ' [' + @vcColumnName + ']'
				END
				ELSE IF @vcDbColumnName = 'numDefaultShippingServiceID'
				BEGIN
					SET @strColumns=@strColumns+ ' ,ISNULL((SELECT vcShipmentService FROM ShippingService WHERE (numDomainID=D.numDomainID OR ISNULL(numDomainID,0)=0) AND numShippingServiceID = Div.numDefaultShippingServiceID),'''')' + ' [' + @vcColumnName + ']'
				END
				ELSE IF @vcDbColumnName = 'tintEDIStatus'
				BEGIN
					--SET @strColumns = @strColumns + ',(CASE Opp.tintEDIStatus WHEN 1 THEN ''850 Received'' WHEN 2 THEN ''850 Acknowledged'' WHEN 3 THEN ''940 Sent'' WHEN 4 THEN ''940 Acknowledged'' WHEN 5 THEN ''856 Received'' WHEN 6 THEN ''856 Acknowledged'' WHEN 7 THEN ''856 Sent'' ELSE '''' END)' + ' [' + @vcColumnName + ']'
					SET @strColumns = @strColumns + ',(CASE Opp.tintEDIStatus WHEN 2 THEN ''850 Acknowledged'' WHEN 3 THEN ''940 Sent'' WHEN 4 THEN ''940 Acknowledged'' WHEN 5 THEN ''856 Received'' WHEN 6 THEN ''856 Acknowledged'' WHEN 7 THEN ''856 Sent'' WHEN 11 THEN ''850 Partially Created'' WHEN 12 THEN ''850 SO Created'' ELSE '''' END)' + ' [' + @vcColumnName + ']'
				END
				ELSE IF @vcDbColumnName = 'numPartenerContact'
				BEGIN
					SET @strColumns=@strColumns+ ' ,(Opp.numPartenerContact) '+' ['+ @vcColumnName+']'    
					SET @strColumns=@strColumns+ ' ,(SELECT A.vcFirstName+'' ''+ A.vcLastName FROM AdditionalContactsInformation AS A WHERE A.numContactId=Opp.numPartenerContact) AS vcPartenerContact' 
				END
				ELSE IF @vcDbColumnName = 'tintInvoicing'
				BEGIN
					SET @strColumns=CONCAT(@strColumns,', CONCAT(ISNULL((SELECT SUBSTRING((SELECT ''$^$'' + CONCAT(numOppBizDocsId,''#^#'',vcBizDocID,''#^#'',ISNULL(bitisemailsent,0),''#^#'')
										FROM OpportunityBizDocs WHERE numOppId=Opp.numOppId AND numBizDocId = ISNULL((SELECT TOP 1 numAuthoritativeSales FROM AuthoritativeBizDocs WHERE numDomainId=' + CAST (@numDomainID AS VARCHAR) + '),287) FOR XML PATH('''')),4,200000)),'''')',
										CASE 
										WHEN CHARINDEX('tintInvoicing=3',@vcRegularSearchCriteria) > 0
											THEN ', (SELECT 
													CONCAT(''('',SUM(X.InvoicedQty),'' Out of '',SUM(X.OrderedQty),'')'')
												FROM 
												(
													SELECT
														OI.numoppitemtCode,
														ISNULL(OI.numUnitHour,0) AS OrderedQty,
														ISNULL(TempInvoice.InvoicedQty,0) AS InvoicedQty
													FROM
														OpportunityItems OI
													INNER JOIN
														Item I
													ON
														OI.numItemCode = I.numItemCode
													OUTER APPLY
													(
														SELECT
															SUM(OpportunityBizDocItems.numUnitHour) AS InvoicedQty
														FROM
															OpportunityBizDocs
														INNER JOIN
															OpportunityBizDocItems 
														ON
															OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
														WHERE
															OpportunityBizDocs.numOppId = Opp.numOppId
															AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
															AND ISNULL(OpportunityBizDocs.bitAuthoritativeBizDocs,0) = 1
													) AS TempInvoice
													WHERE
														OI.numOppID = Opp.numOppId
												) X)'

											WHEN CHARINDEX('tintInvoicing=4',@vcRegularSearchCriteria) > 0
											THEN ', (SELECT 
													CONCAT(''('',(X.InvoicedPercentage),'' % '','')'')
												FROM 
												(

													SELECT fltBreakupPercentage  AS InvoicedPercentage
													FROM dbo.OpportunityRecurring OPR 
														INNER JOIN dbo.OpportunityMaster OM ON OPR.numOppID = OM.numOppID
														LEFT JOIN dbo.OpportunityBizDocs OBD ON OBD.numOppBizDocsID = OPR.numOppBizDocID
													WHERE OPR.numOppId = Opp.numOppId 
														and tintRecurringType <> 4
												) X)'

											ELSE 
													','''''
											END,') [', @vcColumnName,']')


				END
				ELSE IF @vcDbColumnName = 'tintSource'
				BEGIN
					SET @strColumns = @strColumns + ',ISNULL(dbo.fn_GetOpportunitySourceValue(ISNULL(Opp.tintSource,0),ISNULL(Opp.tintSourceType,0),Opp.numDomainID),''Internal Order'')' + ' [' + @vcColumnName + ']'

					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'ISNULL(dbo.fn_GetOpportunitySourceValue(ISNULL(Opp.tintSource,0),ISNULL(Opp.tintSourceType,0),Opp.numDomainID),''Internal Order'') LIKE ''%' + @SearchText + '%'''
					END
				END
				ELSE IF @vcDbColumnName = 'numCampainID'
				BEGIN
					SET @strColumns = @strColumns + ', (SELECT vcCampaignName FROM CampaignMaster WHERE numCampaignID= Opp.numCampainID) ' + ' [' + @vcColumnName + ']'
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'ISNULL((SELECT vcCampaignName FROM CampaignMaster WHERE numCampaignID= Opp.numCampainID),'''') LIKE ''%' + @SearchText + '%'''
					END
				END
				ELSE IF @vcDbColumnName = 'numContactID'
				BEGIN
					SET @strColumns = @strColumns + ',dbo.fn_GetContactName(' + @Prefix + @vcDbColumnName + ') [' + @vcColumnName + ']'
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'dbo.fn_GetContactName(' + @Prefix + @vcDbColumnName + ') LIKE ''%' + @SearchText + '%'''
					END
				END
				ELSE IF @vcListItemType = 'LI'
				BEGIN
					SET @strColumns = @strColumns + ',L' + CONVERT(VARCHAR(3),@tintOrder) + '.vcData' + ' [' + @vcColumnName + ']'
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'L' + CONVERT(VARCHAR(3),@tintOrder) + '.vcData LIKE ''%' + @SearchText + '%'''
					END

					SET @WhereCondition = @WhereCondition + ' left Join ListDetails L' + CONVERT(VARCHAR(3),@tintOrder) + ' on L' + CONVERT(VARCHAR(3),@tintOrder) + '.numListItemID=' + @vcDbColumnName
				END
				ELSE IF @vcListItemType = 'S'
				BEGIN
					SET @strColumns = @strColumns + ',S' + CONVERT(VARCHAR(3),@tintOrder) + '.vcState' + ' [' + @vcColumnName + ']'
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'S' + CONVERT(VARCHAR(3),@tintOrder) + '.vcState LIKE ''%' + @SearchText + '%'''
					END

					SET @WhereCondition = @WhereCondition + ' left join State S' + CONVERT(VARCHAR(3),@tintOrder) + ' on S' + CONVERT(VARCHAR(3),@tintOrder) + '.numStateID=' + @vcDbColumnName
				END
				ELSE IF @vcListItemType = 'BP'
				BEGIN
					SET @strColumns = @strColumns + ',SPLM.Slp_Name' + ' [' + @vcColumnName + ']'
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'SPLM.Slp_Name LIKE ''%' + @SearchText + '%'''
					END

					SET @WhereCondition = @WhereCondition + ' LEFT JOIN Sales_process_List_Master SPLM on SPLM.Slp_Id = OPP.numBusinessProcessID '
				END
				ELSE IF @vcListItemType = 'PP'
				BEGIN
					SET @strColumns = @strColumns + ',ISNULL(PP.intTotalProgress,0)' + ' [' + @vcColumnName + ']'

					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'ISNULL(PP.intTotalProgress,0) LIKE ''%' + @SearchText + '%'''
					END
				END
				ELSE IF @vcListItemType = 'T'
				BEGIN
					SET @strColumns = @strColumns + ',L' + CONVERT(VARCHAR(3),@tintOrder) + '.vcData' + ' [' + @vcColumnName + ']'
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'L' + CONVERT(VARCHAR(3),@tintOrder) + '.vcData LIKE ''%' + @SearchText + '%'''
					END

					SET @WhereCondition = @WhereCondition + ' left Join ListDetails L' + CONVERT(VARCHAR(3),@tintOrder) + ' on L' + CONVERT(VARCHAR(3),@tintOrder) + '.numListItemID=' + @vcDbColumnName
				END
				ELSE IF @vcListItemType = 'U'
				BEGIN
					SET @strColumns = @strColumns + ',dbo.fn_GetContactName(' + @Prefix + @vcDbColumnName + ') [' + @vcColumnName + ']'
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'dbo.fn_GetContactName(' + @Prefix + @vcDbColumnName + ') LIKE ''%' + @SearchText + '%'''
					END
				END
				ELSE IF @vcListItemType = 'WI'
				BEGIN
					SET @strColumns = @strColumns + ', Case when isnull(OL.numSiteID,0)>0 then Sites.vcSiteName else WebAPI.vcProviderName end [' + @vcColumnName + ']'
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + '(Case when isnull(OL.numSiteID,0)>0 then Sites.vcSiteName else WebAPI.vcProviderName end) LIKE ''%' + @SearchText + '%'''
					END

					set @WhereCondition= @WhereCondition +' left JOIN OpportunityLinking OL on OL.numChildOppID=Opp.numOppId left join WebAPI on WebAPI.WebApiId=isnull(OL.numWebApiId,0)
													left join Sites on isnull(OL.numSiteID,0)=Sites.numSIteID and Sites.numDomainID=' + convert(varchar(15),@numDomainID)                                                       
				END 
            END
			ELSE IF @vcAssociatedControlType = 'DateField'
			BEGIN
				IF @Prefix ='OPR.'
				BEGIN
					SET @strColumns = @strColumns + ', (SELECT TOP 1 dbo.FormatedDateFromDate( DateAdd(minute,+'+convert(varchar(15),-@ClientTimeZoneOffset)+', dtRecurringDate)'+ ','+ CONVERT(VARCHAR(10),@numDomainId) +') FROM  dbo.OpportunityRecurring WHERE numOppID=OPP.numOppID AND ISNULL(numOppBizDocID,0)=0 and dtRecurringDate > dbo.GetUTCDateWithoutTime()) AS  ['+ @vcColumnName + ']'

					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + '(SELECT TOP 1 dbo.FormatedDateFromDate( DateAdd(minute,+'+convert(varchar(15),-@ClientTimeZoneOffset)+', dtRecurringDate)'+ ','+ CONVERT(VARCHAR(10),@numDomainId) +') FROM  dbo.OpportunityRecurring WHERE numOppID=OPP.numOppID AND ISNULL(numOppBizDocID,0)=0 and dtRecurringDate > dbo.GetUTCDateWithoutTime()) LIKE ''%' + @SearchText + '%'''
					END
				END
				ELSE IF @vcDbColumnName = 'dtReleaseDate'
				BEGIN
					SET @strColumns = @strColumns
                                + ',case when convert(varchar(11),'
                                + @Prefix
                                + @vcDbColumnName
                                + ')= convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',getdate())) then ''<b><font color=red>Today</font></b>'''
					SET @strColumns = @strColumns
									+ 'when convert(varchar(11),'
									+ @Prefix
									+ @vcDbColumnName
									+ ')= convert(varchar(11),dateadd(day,-1,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',getdate()))) then ''<b><font color=purple>Yesterday</font></b>'''
					SET @strColumns = @strColumns
									+ 'when convert(varchar(11),'
									+ @Prefix
									+ @vcDbColumnName
									+ ')= convert(varchar(11),dateadd(day,1,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',getdate()))) then''<b><font color=orange>Tommorow</font></b>'' '
					SET @strColumns = @strColumns
									+ 'else dbo.FormatedDateFromDate('
									+ @Prefix
									+ @vcDbColumnName
									+ ','
									+ CONVERT(VARCHAR(10),@numDomainId)
									+ ') end  ['
									+ @vcColumnName + ']' 	

					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + @Prefix + @vcDbColumnName + ' LIKE ''%' + @SearchText + '%'''
					END

				END
				ELSE
				BEGIN
					SET @strColumns = @strColumns
                                + ',case when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+','
                                + @Prefix
                                + @vcDbColumnName
                                + ') )= convert(varchar(11),getdate()) then ''<b><font color=red>Today</font></b>'''
					SET @strColumns = @strColumns
									+ 'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+','
									+ @Prefix
									+ @vcDbColumnName
									+ ') )= convert(varchar(11),dateadd(day,-1,getdate())) then ''<b><font color=purple>Yesterday</font></b>'''
					SET @strColumns = @strColumns
									+ 'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+','
									+ @Prefix
									+ @vcDbColumnName
									+ ') )= convert(varchar(11),dateadd(day,1,getdate())) then''<b><font color=orange>Tommorow</font></b>'' '
					SET @strColumns = @strColumns
									+ 'else dbo.FormatedDateFromDate('
									+ @Prefix
									+ @vcDbColumnName
									+ ','
									+ CONVERT(VARCHAR(10),@numDomainId)
									+ ') end  ['
									+ @vcColumnName + ']' 	

					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + @Prefix + @vcDbColumnName + ' LIKE ''%' + @SearchText + '%'''
					END
				 END	
            END
            ELSE
            IF @vcAssociatedControlType = 'TextBox'
            BEGIN
				
                SET @strColumns = @strColumns
                                  + ','
                                  + CASE 
                                      WHEN @vcDbColumnName = 'numAge' THEN 'year(getutcdate())-year(bintDOB)'
                                      WHEN @vcDbColumnName = 'monPAmount' THEN 'isnull((SELECT varCurrSymbol FROM [Currency] WHERE numCurrencyID = Opp.[numCurrencyID]),'''') + '' ''+ CONVERT(VARCHAR,CONVERT(DECIMAL(20,5),monPAmount),1)'
                                      WHEN @vcDbColumnName = 'numDivisionID' THEN 'Div.numDivisionID'
                                      WHEN @vcDbColumnName = 'tintOppStatus' THEN 'CASE tintOppStatus WHEN 0 then ''Open'' WHEN 1 THEN ''Closed-Won'' WHEN 2 THEN ''Closed-Lost'' END '
                                      WHEN @vcDbColumnName = 'tintOppType' THEN 'CASE tintOppType WHEN 1 then ''Sales'' WHEN 2 THEN ''Purchase'' Else ''NA'' END '
									  WHEN @vcDbColumnName = 'vcPOppName' THEN 'dbo.CheckChildRecord(Opp.numOppId,Opp.numDomainID)'
									  WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'vcBizDocsList' THEN 'ISNULL((SELECT SUBSTRING((SELECT ''$^$'' + CAST(numOppBizDocsId AS VARCHAR(18)) +''#^#''+ vcBizDocID
										FROM OpportunityBizDocs WHERE numOppId=Opp.numOppId AND 1 = (Case When bitAuthoritativeBizDocs=1 AND 1=' + (CASE WHEN @inttype=1 THEN '1' ELSE '0' END) + '  Then 0 ELSE 1 END) FOR XML PATH('''')),4,200000)),'''')'
                                      ELSE @Prefix + @vcDbColumnName
                                    END
                                  + ' ['
                                  + @vcColumnName + ']'
				IF(@vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'vcBizDocsList')
				BEGIN
					SET @strColumns=@strColumns+','+' (SELECT COUNT(I.vcItemName) FROM 
													 OpportunityItems AS t LEFT JOIN  Item as I ON 
													 t.numItemCode=I.numItemCode WHERE 
													 t.numOppId=Opp.numOppId AND t.numoppItemtCode NOT IN 
													 (SELECT numOppItemId FROM OpportunityBizDocs JOIN 
													 OpportunityBizDocItems ON OpportunityBizDocs.numOppBizDocsId=OpportunityBizDocItems.numOppBizDocID  
													 WHERE OpportunityBizDocs.numOppId=Opp.numOppId 
													 AND 1 = (Case When bitAuthoritativeBizDocs=1 Or numBizDocID=293 Then 0 ELSE 1 END) 
													 AND ISNULL(bitAuthoritativeBizDocs,0)=1)) AS [List_Item_Approval_UNIT] '
				END
				
				IF LEN(@SearchText) > 0
				BEGIN
					SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + (CASE 
                                      WHEN @vcDbColumnName = 'numAge' THEN 'year(getutcdate())-year(bintDOB)'
                                      WHEN @vcDbColumnName = 'monPAmount' THEN 'isnull((SELECT varCurrSymbol FROM [Currency] WHERE numCurrencyID = Opp.[numCurrencyID]),'''') + '' ''+ CONVERT(VARCHAR,CONVERT(DECIMAL(20,5),monPAmount),1)'
                                      WHEN @vcDbColumnName = 'numDivisionID' THEN 'Div.numDivisionID'
                                      WHEN @vcDbColumnName = 'tintOppStatus' THEN 'CASE tintOppStatus WHEN 0 then ''Open'' WHEN 1 THEN ''Closed-Won'' WHEN 2 THEN ''Closed-Lost'' END '
                                      WHEN @vcDbColumnName = 'tintOppType' THEN 'CASE tintOppType WHEN 1 then ''Sales'' WHEN 2 THEN ''Purchase'' Else ''NA'' END '
									  WHEN @vcDbColumnName = 'vcPOppName' THEN 'dbo.CheckChildRecord(Opp.numOppId,Opp.numDomainID)'
									  WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'vcBizDocsList' THEN 'ISNULL((SELECT SUBSTRING((SELECT ''$^$'' + CAST(numOppBizDocsId AS VARCHAR(18)) +''#^#''+ vcBizDocID
										FROM OpportunityBizDocs WHERE numOppId=Opp.numOppId AND 1 = (Case When bitAuthoritativeBizDocs=1 Or numBizDocID=293 Then 0 ELSE 1 END)  FOR XML PATH('''')),4,200000)),'''')'
                                      ELSE @Prefix + @vcDbColumnName
                                    END) + ' LIKE ''%' + @SearchText + '%'''
				END
            END
            ELSE
            IF @vcAssociatedControlType = 'TextArea'
            BEGIN
				SET @strColumns=@strColumns+','+  @Prefix + @vcDbColumnName +' ['+ @vcColumnName+']' 

				IF LEN(@SearchText) > 0
				BEGIN
					SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + @Prefix + @vcDbColumnName + ' LIKE ''%' + @SearchText + '%'''
				END
            END
            ELSE IF  @vcAssociatedControlType='Label'                                              
			BEGIN
				SET @strColumns=@strColumns + ',' + CASE                    
				WHEN @vcDbColumnName = 'CalAmount' THEN 'Opp.monDealAmount'--'[dbo].[getdealamount](Opp.numOppId,Getutcdate(),0)'
				WHEN @vcDbColumnName = 'vcInventoryStatus' THEN 'ISNULL(dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID),'''')'
				WHEN @vcDbColumnName = 'vcOrderedShipped' THEN 'dbo.GetOrderShipReceiveStatus(Opp.numOppId,1,Opp.tintshipped)'
				WHEN @vcDbColumnName = 'vcOrderedReceived' THEN 'dbo.GetOrderShipReceiveStatus(Opp.numOppId,2,Opp.tintshipped)'
				WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'monDealAmount' THEN 'ISNULL((SELECT SUM(ISNULL(monDealAmount,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)'
				WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'monAmountPaid' THEN 'ISNULL((SELECT SUM(ISNULL(monAmountPaid,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)'
				ELSE @Prefix + @vcDbColumnName END +' ['+ @vcColumnName+']'   
				
				IF @vcDbColumnName='vcOrderedShipped'
				BEGIN
				DECLARE @temp VARCHAR(MAX)
				SET @temp = '(SELECT 
				STUFF((SELECT '', '' + CAST((CAST(numOppBizDocsId AS varchar)+''~''+CAST(ISNULL(numShippingReportId,0) AS varchar) ) AS VARCHAR(MAX)) [text()]
				FROM OpportunityBizDocs
				LEFT JOIN ShippingReport
				ON ShippingReport.numOppBizDocId=OpportunityBizDocs.numOppBizDocsId
				WHERE OpportunityBizDocs.numOppId=Opp.numOppId  AND OpportunityBizDocs.bitShippingGenerated=1  FOR XML PATH(''''), TYPE)
						.value(''.'',''NVARCHAR(MAX)''),1,2,'' ''))'
					SET @strColumns = @strColumns + ' , (SELECT SUBSTRING('+ @temp +', 2, 200000))AS ShippingIcons '
					SET @strColumns = @strColumns + ' , CASE WHEN (SELECT COUNT(*) FROM ShippingBox INNER JOIN ShippingReport ON ShippingBox.numShippingReportId = ShippingReport.numShippingReportId WHERE ShippingReport.numOppID = Opp.numOppID AND LEN(vcTrackingNumber) > 0) > 0 THEN 1 ELSE 0 END [ISTrackingNumGenerated]'
				END

				IF LEN(@SearchText) > 0
				BEGIN
					SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + (CASE                    
					WHEN @vcDbColumnName = 'CalAmount' THEN 'Opp.monDealAmount'
					WHEN @vcDbColumnName = 'vcInventoryStatus' THEN 'ISNULL(dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID),'''')'
					WHEN @vcDbColumnName = 'vcOrderedShipped' THEN 'dbo.GetOrderShipReceiveStatus(Opp.numOppId,1,Opp.tintshipped)'
					WHEN @vcDbColumnName = 'vcOrderedReceived' THEN 'dbo.GetOrderShipReceiveStatus(Opp.numOppId,2,Opp.tintshipped)'
					WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'monDealAmount' THEN 'ISNULL((SELECT SUM(ISNULL(monDealAmount,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)'
					WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'monAmountPaid' THEN 'ISNULL((SELECT SUM(ISNULL(monAmountPaid,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)'
					else @Prefix + @vcDbColumnName END) + ' LIKE ''%' + @SearchText + '%'''
				END    
			END
            ELSE IF @vcAssociatedControlType='Popup' and @vcDbColumnName='numShareWith'                                                   
			BEGIN
				SET @strColumns = @strColumns + ',(SELECT SUBSTRING(
								(SELECT '','' + A.vcFirstName+'' ''+A.vcLastName + CASE WHEN ISNULL(SR.numContactType,0)>0 THEN ''('' + dbo.fn_GetListItemName(SR.numContactType) + '')'' ELSE '''' END
								FROM ShareRecord SR JOIN UserMaster UM ON UM.numUserDetailId=SR.numAssignedTo 
								JOIN AdditionalContactsInformation A ON UM.numUserDetailId=A.numContactID 
								WHERE SR.numDomainID=Opp.numDomainID AND SR.numModuleID=3 AND SR.numRecordID=Opp.numOppId
								AND UM.numDomainID=Opp.numDomainID and UM.numDomainID=A.numDomainID FOR XML PATH('''')),2,200000)) ['+ @vcColumnName+']'                
			END 
        END
		ELSE
        BEGIN
			SET @vcColumnName= @vcDbColumnName +'~'+ convert(varchar(10),@numFieldId)+'~1'

			IF @vcAssociatedControlType = 'TextBox' or @vcAssociatedControlType = 'TextArea'
            BEGIN
              SET @strColumns = @strColumns
                              + ',CFW'
                              + CONVERT(VARCHAR(3),@tintOrder)
                              + '.Fld_Value  ['
                              + @vcColumnName + ']'
              SET @WhereCondition = @WhereCondition
                                      + ' left Join CFW_FLD_Values_Opp CFW'
                                      + CONVERT(VARCHAR(3),@tintOrder)
                                      + ' on CFW'
                                      + CONVERT(VARCHAR(3),@tintOrder)
                                      + '.Fld_Id='
                                      + CONVERT(VARCHAR(10),@numFieldId)
                                      + 'and CFW'
                                      + CONVERT(VARCHAR(3),@tintOrder)
                                      + '.RecId=Opp.numOppid   '
			END
			ELSE
            IF @vcAssociatedControlType = 'CheckBox' 
            BEGIN
                SET @strColumns = @strColumns
                                + ',case when isnull(CFW'
                                + CONVERT(VARCHAR(3),@tintOrder)
                                + '.Fld_Value,0)=0 then 0 when isnull(CFW'
                                + CONVERT(VARCHAR(3),@tintOrder)
                                + '.Fld_Value,0)=1 then 1 end   ['
                                + @vcColumnName + ']'
                SET @WhereCondition = @WhereCondition
                                        + ' left Join CFW_FLD_Values_Opp CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + ' on CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.Fld_Id='
                                        + CONVERT(VARCHAR(10),@numFieldId)
                                        + 'and CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.RecId=Opp.numOppid   '
            END
            ELSE
            IF @vcAssociatedControlType = 'DateField'
            BEGIN
                  SET @strColumns = @strColumns
                                  + ',dbo.FormatedDateFromDate(CFW'
                                  + CONVERT(VARCHAR(3),@tintOrder)
                                  + '.Fld_Value,'
                                  + CONVERT(VARCHAR(10),@numDomainId)
                                  + ')  ['
                                  + @vcColumnName + ']'
                  SET @WhereCondition = @WhereCondition
                                          + ' left Join CFW_FLD_Values_Opp CFW'
                                          + CONVERT(VARCHAR(3),@tintOrder)
                                          + ' on CFW'
                                          + CONVERT(VARCHAR(3),@tintOrder)
                                          + '.Fld_Id='
                                          + CONVERT(VARCHAR(10),@numFieldId)
                                          + 'and CFW'
                                          + CONVERT(VARCHAR(3),@tintOrder)
                                          + '.RecId=Opp.numOppid    '
            END
            ELSE
            IF @vcAssociatedControlType = 'SelectBox'
            BEGIN
                SET @vcDbColumnName = 'DCust'
                                            + CONVERT(VARCHAR(10),@numFieldId)
                SET @strColumns = @strColumns
                                + ',L'
                                + CONVERT(VARCHAR(3),@tintOrder)
                                + '.vcData'
                                + ' ['
                                + @vcColumnName + ']'

                SET @WhereCondition = @WhereCondition
                                        + ' left Join CFW_FLD_Values_Opp CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + ' on CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.Fld_Id='
                                        + CONVERT(VARCHAR(10),@numFieldId)
                                        + 'and CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.RecId=Opp.numOppid '

                SET @WhereCondition = @WhereCondition
                                        + ' left Join ListDetails L'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + ' on L'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.numListItemID=CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.Fld_Value'
            END
			ELSE
			BEGIN
				SET @strColumns = @strColumns + CONCAT(',dbo.GetCustFldValueOpp(',@numFieldId,',opp.numOppID)') + ' [' + @vcColumnName + ']'
			END

			IF LEN(@SearchText) > 0
			BEGIN
				SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + CONCAT('dbo.GetCustFldValueOpp(',@numFieldId,',opp.numOppID) LIKE ''%',@SearchText,'%''')
			END
		END
      
     
		SELECT TOP 1 
			@tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName,@vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,
			@numListID=numListID,@vcLookBackTableName=vcLookBackTableName,@bitCustom=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,@bitAllowEdit=bitAllowEdit,
			@ListRelID=ListRelID
		FROM 
			#tempForm 
		WHERE 
			tintOrder > @tintOrder-1 
		ORDER BY 
			tintOrder asc            
 
		IF @@rowcount=0 SET @tintOrder=0 
	END 

	

	DECLARE @strShareRedordWith AS VARCHAR(300);
	SET @strShareRedordWith=' Opp.numOppId in (select SR.numRecordID FROM ShareRecord SR WHERE SR.numDomainID='+ convert(varchar(15),@numDomainID )+' AND SR.numModuleID=3 AND SR.numAssignedTo='+convert(varchar(15),@numUserCntId)+') '


	DECLARE @StrSql AS VARCHAR(MAX) = ''

	SET @StrSql = @StrSql + ' FROM OpportunityMaster Opp            
								 ##PLACEHOLDER##                                                   
                                 LEFT JOIN AdditionalContactsInformation ADC ON Opp.numContactId = ADC.numContactId                                                               
                                 INNER JOIN DivisionMaster Div ON Opp.numDivisionId = Div.numDivisionID 
								 INNER JOIN CompanyInfo cmp ON Div.numCompanyID = cmp.numCompanyId 
								 LEFT JOIN ProjectProgress PP On PP.numOppId = Opp.numOppId ' + @WhereCondition
								 --INNER JOIN Correspondence Corr ON opp.numOppId = Corr.numOpenRecordID 
	-------Change Row Color-------
	
	DECLARE @vcCSOrigDbCOlumnName AS VARCHAR(50) = ''
	DECLARE @vcCSLookBackTableName AS VARCHAR(50) = ''
	DECLARE @vcCSAssociatedControlType AS VARCHAR(50)

	CREATE TABLE #tempColorScheme
	(
		vcOrigDbCOlumnName VARCHAR(50),
		vcLookBackTableName VARCHAR(50),
		vcFieldValue VARCHAR(50),
		vcFieldValue1 VARCHAR(50),
		vcColorScheme VARCHAR(50),
		vcAssociatedControlType varchar(50)
	)

	INSERT INTO 
		#tempColorScheme 
	SELECT 
		DFM.vcOrigDbCOlumnName,
		DFM.vcLookBackTableName,
		DFCS.vcFieldValue,
		DFCS.vcFieldValue1,
		DFCS.vcColorScheme,
		DFFM.vcAssociatedControlType 
	FROM 
		DycFieldColorScheme DFCS 
	JOIN 
		DycFormField_Mapping DFFM 
	ON 
		DFCS.numFieldID=DFFM.numFieldID
	JOIN 
		DycFieldMaster DFM 
	ON 
		DFM.numModuleID=DFFM.numModuleID and DFM.numFieldID=DFFM.numFieldID
	WHERE
		DFCS.numDomainID=@numDomainID 
		AND DFFM.numFormID=@numFormId 
		AND DFCS.numFormID=@numFormId 
		AND isnull(DFFM.bitAllowGridColor,0)=1

	IF(SELECT COUNT(*) FROM #tempColorScheme)>0
	BEGIN
		SELECT TOP 1 @vcCSOrigDbCOlumnName=vcOrigDbCOlumnName,@vcCSLookBackTableName=vcLookBackTableName,@vcCSAssociatedControlType=vcAssociatedControlType from #tempColorScheme
	END                        

	IF len(@vcCSOrigDbCOlumnName)>0 and len(@vcCSLookBackTableName)>0
	BEGIN
		SET @strColumns=@strColumns + ',tCS.vcColorScheme'
	END

	IF len(@vcCSOrigDbCOlumnName)>0 and len(@vcCSLookBackTableName)>0
	BEGIN
		IF @vcCSLookBackTableName = 'AdditionalContactsInformation'                  
			SET @Prefix = 'ADC.'                  
		IF @vcCSLookBackTableName = 'DivisionMaster'                  
			SET @Prefix = 'Div.'
		if @vcCSLookBackTableName = 'CompanyInfo'                  
			SET @Prefix = 'CMP.'   
		if @vcCSLookBackTableName = 'OpportunityMaster'                  
			SET @Prefix = 'Opp.'   
 
		IF @vcCSAssociatedControlType='DateField'
		BEGIN
			SET @strSql=@strSql+' LEFT JOIN #tempColorScheme tCS ON CONVERT(VARCHAR(10),' + @Prefix + @vcCSOrigDbCOlumnName + ',111) >= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue as int),GETUTCDATE()),111)
				 and CONVERT(VARCHAR(10),' + @Prefix + @vcCSOrigDbCOlumnName + ',111) <= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue1 as int),GETUTCDATE()),111)'
		END
		ELSE IF @vcCSAssociatedControlType='SelectBox' OR @vcCSAssociatedControlType='ListBox' OR @vcCSAssociatedControlType='CheckBox'
		BEGIN
			SET @strSql=@strSql+' LEFT JOIN #tempColorScheme tCS on tCS.vcFieldValue=' + @Prefix + @vcCSOrigDbCOlumnName
		END
	END
                          
	IF @columnName like 'CFW.Cust%'             
	BEGIN            
		SET @strSql = @strSql + ' LEFT JOIN CFW_Fld_Values_Opp CFW on CFW.RecId=Opp.numOppid and CFW.fld_id= '+ REPLACE(@columnName,'CFW.Cust','') +' '                                                     
		SET @columnName='CFW.Fld_Value'            
	END 

	IF @bitPartner = 1
		SET @strSql = @strSql + ' LEFT JOIN OpportunityContact OppCont ON OppCont.numOppId=Opp.numOppId AND OppCont.bitPartner=1 AND OppCont.numContactId=' + CONVERT(VARCHAR(15),@numUserCntID)
                    
                
	IF @tintFilterBy = 1 --Partially Fulfilled Orders 
		SET @strSql = @strSql + ' LEFT JOIN AdditionalContactsInformation ADC1 ON ADC1.numContactId=Opp.numRecOwner                                                               
                             LEFT OUTER JOIN [OpportunityBizDocs] OB ON OB.[numOppId] = Opp.[numOppId] 
                             WHERE Opp.numDomainID=' + CONVERT(VARCHAR(15),@numDomainID) + ' and Opp.tintOppstatus=1 and (Opp.tintShipped=' + CONVERT(VARCHAR(15),@tintShipped) + ' OR ' + CONVERT(VARCHAR(15),@tintShipped) + '=2)'
	ELSE
		SET @strSql = @strSql + ' LEFT JOIN AdditionalContactsInformation ADC1 on ADC1.numContactId=Opp.numRecOwner                                                               
                             WHERE Opp.numDomainID=' + CONVERT(VARCHAR(15),@numDomainID) + ' and Opp.tintOppstatus=1 and (Opp.tintShipped='+ CONVERT(VARCHAR(15),@tintShipped)  + ' OR ' + CONVERT(VARCHAR(15),@tintShipped) + '=2)'
  
  
	IF @intOrder <> 0
    BEGIN
      IF @intOrder = 1
        SET @strSql = @strSql + ' and Opp.bitOrder = 0 '
      IF @intOrder = 2
        SET @strSql = @strSql + ' and Opp.bitOrder = 1'
    END
    
	IF @numCompanyID <> 0
		SET @strSql = @strSql + ' And Div.numCompanyID ='+ CONVERT(VARCHAR(15),@numCompanyID)

	IF @SortChar <> '0' 
		SET @strSql = @strSql + ' And Opp.vcPOppName like ''' + @SortChar + '%'''
  

	IF @inttype = 3
	BEGIN
		IF @tintSalesUserRightType = 0
			SET @strSql = @strSql + ' AND opp.tintOppType=0'
		ELSE IF @tintSalesUserRightType = 1
			SET @strSql = @strSql + ' AND (Opp.numRecOwner= ' + CONVERT(VARCHAR(15),@numUserCntID)
						+ ' or Opp.numAssignedTo= '+ CONVERT(VARCHAR(15),@numUserCntID)
						+ CASE WHEN @bitPartner = 1 THEN ' or ( OppCont.bitPartner=1 and OppCont.numContactId='+ CONVERT(VARCHAR(15),@numUserCntID) + ')' ELSE '' END
						+ ' or ' + @strShareRedordWith +')'
		ELSE IF @tintSalesUserRightType = 2
			SET @strSql = @strSql + ' AND (Div.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+ CONVERT(VARCHAR(15),@numUserCntID) + ' ) 
									 or Opp.numAssignedTo= '+ CONVERT(VARCHAR(15),@numUserCntID)
									+ ' or ' + @strShareRedordWith +')'
	END
	ELSE IF @inttype = 4
	BEGIN
		IF @tintPurchaseUserRightType = 0
			SET @strSql = @strSql + ' AND opp.tintOppType=0'
		ELSE IF @tintPurchaseUserRightType = 1
			SET @strSql = @strSql + ' AND (Opp.numRecOwner= ' + CONVERT(VARCHAR(15),@numUserCntID)
						+ ' or Opp.numAssignedTo= '+ CONVERT(VARCHAR(15),@numUserCntID)
						+ CASE WHEN @bitPartner = 1 THEN ' or ( OppCont.bitPartner=1 and OppCont.numContactId='+ CONVERT(VARCHAR(15),@numUserCntID) + ')' ELSE '' END
						+ ' or ' + @strShareRedordWith +')'
		ELSE IF @tintPurchaseUserRightType = 2
			SET @strSql = @strSql + ' AND (Div.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+ CONVERT(VARCHAR(15),@numUserCntID) + ' ) 
									 or Opp.numAssignedTo= '+ CONVERT(VARCHAR(15),@numUserCntID)
									+ ' or ' + @strShareRedordWith +')'
	END

	
					
	IF @tintSortOrder <> '0'
		SET @strSql = @strSql + '  AND Opp.tintOppType= '+ CONVERT(VARCHAR(1),@tintSortOrder)
    
    
	IF @tintFilterBy = 1 --Partially Fulfilled Orders
		SET @strSql = @strSql + ' AND OB.bitPartialFulfilment = 1 AND OB.bitAuthoritativeBizDocs = 1 '
	ELSE IF @tintFilterBy = 2 --Fulfilled Orders
		SET @strSql = @strSql + ' AND Opp.[numOppId] IN (SELECT OM.numOppId FROM [OpportunityMaster] OM
                      			INNER JOIN [OpportunityBizDocs] OB ON OB.[numOppId] = OM.[numOppId]
                      			INNER JOIN [OpportunityItems] OI ON OM.[numOppId] = OI.[numOppId]
                      			INNER JOIN [OpportunityBizDocItems] BI ON BI.[numOppBizDocID] = OB.[numOppBizDocsId]
                      			Where OM.numDomainId=' + CONVERT(VARCHAR(15),@numDomainId) + 'AND OB.[bitAuthoritativeBizDocs] = 1 GROUP BY OM.[numOppId]
                      			HAVING   COUNT(OI.[numUnitHour]) = COUNT(BI.[numUnitHour])) '
	ELSE IF @tintFilterBy = 3 --Orders without Authoritative-BizDocs
		SET @strSql = @strSql + ' AND Opp.[numOppId] not IN (SELECT DISTINCT OM.[numOppId]
                      		  FROM [OpportunityMaster] OM JOIN [OpportunityBizDocs] OB ON OM.[numOppId] = OB.[numOppId]
                      		  Where OM.numDomainId=' + CONVERT(VARCHAR(15),@numDomainId) + ' AND OM.tintOppstatus=1 AND (OM.tintShipped=' + CONVERT(VARCHAR(15),@tintShipped) + ' OR ' + CONVERT(VARCHAR(15),@tintShipped) + '=2)' + ' AND OM.tintOppType= '+ CONVERT(VARCHAR(1),@tintSortOrder) + ' AND isnull(OB.[bitAuthoritativeBizDocs],0)=1) '
	ELSE IF @tintFilterBy = 4 --Orders with items yet to be added to Invoices
		SET @strSql = @strSql + ' AND Opp.[numOppId] IN (SELECT distinct OPPM.numOppId FROM [OpportunityMaster] OPPM
                                  INNER JOIN [OpportunityItems] OPPI ON OPPM.[numOppId] = OPPI.[numOppId]
                                  Where OPPM.numDomainId=' + CONVERT(VARCHAR(15),@numDomainId) + ' 
								  and OPPI.numoppitemtCode not in (select numOppItemID from [OpportunityBizDocs] OB INNER JOIN [OpportunityBizDocItems] BI
                      			  ON BI.[numOppBizDocID] = OB.[numOppBizDocsId] where OB.[numOppId] = OPPM.[numOppId]))'      
	ELSE IF @tintFilterBy = 6 --Orders with deferred Income/Invoices
		SET @strSql = @strSql + ' AND Opp.[numOppId] IN (SELECT DISTINCT OM.[numOppId]
                      		 FROM [OpportunityMaster] OM LEFT OUTER JOIN [OpportunityBizDocs] OB ON OM.[numOppId] = OB.[numOppId]
                      		 Where OM.numDomainId=' + CONVERT(VARCHAR(15),@numDomainId) + ' AND OB.[bitAuthoritativeBizDocs] =1 and OB.tintDeferred=1)'

	IF @numOrderStatus <>0 
		SET @strSql = @strSql + ' AND Opp.numStatus = ' + CONVERT(VARCHAR(15),@numOrderStatus);

	IF CHARINDEX('OI.vcInventoryStatus',@vcRegularSearchCriteria) > 0
	BEGIN
		IF CHARINDEX('OI.vcInventoryStatus=1',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'OI.vcInventoryStatus=1',' CHARINDEX(''Not Applicable'', dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID)) > 0 ')
		IF CHARINDEX('OI.vcInventoryStatus=2',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'OI.vcInventoryStatus=2',' CHARINDEX(''Allocation Cleared'', dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID)) > 0 ')
		IF CHARINDEX('OI.vcInventoryStatus=3',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'OI.vcInventoryStatus=3',' CHARINDEX(''Back Order'', dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID)) > 0 ')
		IF CHARINDEX('OI.vcInventoryStatus=4',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'OI.vcInventoryStatus=4',' CHARINDEX(''Ready to Ship'', dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID)) > 0 ')
	END
	
	IF CHARINDEX('Opp.tintInvoicing',@vcRegularSearchCriteria) > 0
	BEGIN
		IF CHARINDEX('Opp.tintInvoicing=0',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.tintInvoicing=0',' CHARINDEX(''All'', dbo.CheckOrderInvoicingStatus(Opp.numOppID,Opp.numDomainID)) > 0 ')
		IF CHARINDEX('Opp.tintInvoicing=1',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.tintInvoicing=1',' (SELECT (CASE WHEN SUM(ISNULL(InvoicedQty,0)) = SUM(ISNULL(OrderedQty,0)) THEN 1 ELSE 0 END)
	FROM 
	(
		SELECT OI.numoppitemtCode, ISNULL(OI.numUnitHour,0) AS OrderedQty, ISNULL(TempInvoice.InvoicedQty,0) AS InvoicedQty
		FROM OpportunityItems OI
		INNER JOIN Item I ON OI.numItemCode = I.numItemCode
		OUTER APPLY
		(
			SELECT SUM(OpportunityBizDocItems.numUnitHour) AS InvoicedQty
			FROM OpportunityBizDocs
				INNER JOIN OpportunityBizDocItems ON OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
			WHERE OpportunityBizDocs.numOppId = Opp.numOppID
				AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
				AND numBizDocId IN (SELECT numAuthoritativeSales FROM AuthoritativeBizDocs WHERE numDomainId = Opp.numDomainID)
		) AS TempInvoice
		WHERE
		OI.numOppID = Opp.numOppID
	) X) > 0 ')
		IF CHARINDEX('Opp.tintInvoicing=2',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.tintInvoicing=2',' (SELECT COUNT(*) 
		FROM OpportunityBizDocs 
		WHERE numOppId = Opp.numOppID 
			AND numBizDocId IN (SELECT numAuthoritativeSales FROM AuthoritativeBizDocs WHERE numDomainId = Opp.numDomainID)) = 0 ')
		IF CHARINDEX('Opp.tintInvoicing=3',@vcRegularSearchCriteria) > 0 
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.tintInvoicing=3',' (SELECT (CASE WHEN SUM(ISNULL(InvoicedQty,0)) > 0 THEN SUM(ISNULL(OrderedQty,0)) - SUM(ISNULL(InvoicedQty,0)) ELSE 0 END)
	FROM 
	(
		SELECT OI.numoppitemtCode, ISNULL(OI.numUnitHour,0) AS OrderedQty, ISNULL(TempInvoice.InvoicedQty,0) AS InvoicedQty
		FROM OpportunityItems OI
		INNER JOIN Item I ON OI.numItemCode = I.numItemCode
		OUTER APPLY
		(
			SELECT SUM(OpportunityBizDocItems.numUnitHour) AS InvoicedQty
			FROM OpportunityBizDocs
				INNER JOIN OpportunityBizDocItems ON OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
			WHERE OpportunityBizDocs.numOppId = Opp.numOppID
				AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
				AND numBizDocId IN (SELECT numAuthoritativeSales FROM AuthoritativeBizDocs WHERE numDomainId = Opp.numDomainID)
		) AS TempInvoice
		WHERE
		OI.numOppID = Opp.numOppID
	) X) > 0 ')
		
		IF CHARINDEX('Opp.tintInvoicing=4',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.tintInvoicing=4',' (SELECT (CASE WHEN SUM(ISNULL(InvoicedQty,0)) > 0 THEN SUM(ISNULL(OrderedQty,0)) - SUM(ISNULL(InvoicedQty,0)) ELSE 0 END)
	FROM 
	(
		SELECT OI.numoppitemtCode, ISNULL(OI.numUnitHour,0) AS OrderedQty, ISNULL(TempInvoice.InvoicedQty,0) AS InvoicedQty
		FROM OpportunityItems OI
		INNER JOIN Item I ON OI.numItemCode = I.numItemCode
		OUTER APPLY
		(
			SELECT SUM(OpportunityBizDocItems.numUnitHour) AS InvoicedQty
			FROM OpportunityBizDocs
				INNER JOIN OpportunityBizDocItems ON OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
			WHERE OpportunityBizDocs.numOppId = Opp.numOppID
				AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
				AND numBizDocId IN (SELECT numAuthoritativeSales FROM AuthoritativeBizDocs WHERE numDomainId = Opp.numDomainID)
		) AS TempInvoice
		WHERE
		OI.numOppID = Opp.numOppID
	) X) > 0 ')

		IF CHARINDEX('Opp.tintInvoicing=5',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.tintInvoicing=5',' 
				Opp.[numOppId] IN (SELECT DISTINCT OM.[numOppId]
                      		 FROM [OpportunityMaster] OM INNER JOIN [OpportunityBizDocs] OB ON OM.[numOppId] = OB.[numOppId]
                      		 Where OM.numDomainId = Opp.numDomainID AND OB.[bitAuthoritativeBizDocs] = 1 and OB.tintDeferred=1) ')
	END
	
	IF CHARINDEX('Opp.tintEDIStatus',@vcRegularSearchCriteria) > 0
	BEGIN
		--IF CHARINDEX('Opp.tintEDIStatus=1',@vcRegularSearchCriteria) > 0
		--	SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.tintEDIStatus=1',' 1 = (SELECT CASE WHEN Opp.tintEDIStatus = 1 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.tintEDIStatus=2',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.tintEDIStatus=2',' 1 = (SELECT CASE WHEN Opp.tintEDIStatus = 2 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.tintEDIStatus=3',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.tintEDIStatus=3',' 1 = (SELECT CASE WHEN Opp.tintEDIStatus = 3 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.tintEDIStatus=4',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.tintEDIStatus=4',' 1 = (SELECT CASE WHEN Opp.tintEDIStatus = 4 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.tintEDIStatus=5',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.tintEDIStatus=5',' 1 = (SELECT CASE WHEN Opp.tintEDIStatus = 5 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.tintEDIStatus=6',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.tintEDIStatus=6',' 1 = (SELECT CASE WHEN Opp.tintEDIStatus = 6 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.tintEDIStatus=7',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.tintEDIStatus=7',' 1 = (SELECT CASE WHEN Opp.tintEDIStatus = 7 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.tintEDIStatus=7',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.tintEDIStatus=11',' 1 = (SELECT CASE WHEN Opp.tintEDIStatus = 11 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.tintEDIStatus=7',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.tintEDIStatus=12',' 1 = (SELECT CASE WHEN Opp.tintEDIStatus = 12 THEN 1 ELSE 0 END) ')
	END

	IF CHARINDEX('Opp.vcSignatureType',@vcRegularSearchCriteria) > 0
	BEGIN
		IF CHARINDEX('Opp.vcSignatureType=0',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.vcSignatureType=0',' 1 = (SELECT CASE WHEN (SELECT COUNT(*) FROM DivisionMaster AS D 
																			LEFT JOIN DivisionMasterShippingConfiguration AS DMSC ON DMSC.numDivisionID=D.numDivisionID 
																			WHERE D.numDivisionID=Div.numDivisionID AND vcSignatureType=0 ) > 0 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.vcSignatureType=1',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.vcSignatureType=1','  1 = (SELECT CASE WHEN (SELECT COUNT(*) FROM DivisionMaster AS D 
																			LEFT JOIN DivisionMasterShippingConfiguration AS DMSC ON DMSC.numDivisionID=D.numDivisionID 
																			WHERE D.numDivisionID=Div.numDivisionID AND vcSignatureType=1 ) > 0 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.vcSignatureType=2',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.vcSignatureType=2',' 1 = (SELECT CASE WHEN (SELECT COUNT(*) FROM DivisionMaster AS D 
																			LEFT JOIN DivisionMasterShippingConfiguration AS DMSC ON DMSC.numDivisionID=D.numDivisionID 
																			WHERE D.numDivisionID=Div.numDivisionID AND vcSignatureType=2 ) > 0 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.vcSignatureType=3',@vcRegularSearchCriteria) > 0								   
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.vcSignatureType=3',' 1 = (SELECT CASE WHEN (SELECT COUNT(*) FROM DivisionMaster AS D 
																			LEFT JOIN DivisionMasterShippingConfiguration AS DMSC ON DMSC.numDivisionID=D.numDivisionID 
																			WHERE D.numDivisionID=Div.numDivisionID AND vcSignatureType=3 ) > 0 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.vcSignatureType=4',@vcRegularSearchCriteria) > 0								   
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.vcSignatureType=4',' 1 = (SELECT CASE WHEN (SELECT COUNT(*) FROM DivisionMaster AS D 
																			LEFT JOIN DivisionMasterShippingConfiguration AS DMSC ON DMSC.numDivisionID=D.numDivisionID 
																			WHERE D.numDivisionID=Div.numDivisionID AND vcSignatureType=4 ) > 0 THEN 1 ELSE 0 END) ')
	END

	IF CHARINDEX('Opp.numDefaultShippingServiceID',@vcRegularSearchCriteria) > 0
	BEGIN
		IF CHARINDEX('Opp.numDefaultShippingServiceID=10',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.numDefaultShippingServiceID=10',' 1 = (SELECT CASE WHEN (SELECT COUNT(*) FROM DivisionMaster WHERE numDivisionID=Div.numDivisionID AND numDefaultShippingServiceID=10 ) > 0 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.numDefaultShippingServiceID=11',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.numDefaultShippingServiceID=11',' 1 = (SELECT CASE WHEN (SELECT COUNT(*) FROM DivisionMaster WHERE numDivisionID=Div.numDivisionID AND numDefaultShippingServiceID=11 ) > 0 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.numDefaultShippingServiceID=12',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.numDefaultShippingServiceID=12',' 1 = (SELECT CASE WHEN (SELECT COUNT(*) FROM DivisionMaster WHERE numDivisionID=Div.numDivisionID AND numDefaultShippingServiceID=12 ) > 0 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.numDefaultShippingServiceID=13',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.numDefaultShippingServiceID=13',' 1 = (SELECT CASE WHEN (SELECT COUNT(*) FROM DivisionMaster WHERE numDivisionID=Div.numDivisionID AND numDefaultShippingServiceID=13 ) > 0 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.numDefaultShippingServiceID=14',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.numDefaultShippingServiceID=14',' 1 = (SELECT CASE WHEN (SELECT COUNT(*) FROM DivisionMaster WHERE numDivisionID=Div.numDivisionID AND numDefaultShippingServiceID=14 ) > 0 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.numDefaultShippingServiceID=15',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.numDefaultShippingServiceID=15',' 1 = (SELECT CASE WHEN (SELECT COUNT(*) FROM DivisionMaster WHERE numDivisionID=Div.numDivisionID AND numDefaultShippingServiceID=15 ) > 0 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.numDefaultShippingServiceID=16',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.numDefaultShippingServiceID=16',' 1 = (SELECT CASE WHEN (SELECT COUNT(*) FROM DivisionMaster WHERE numDivisionID=Div.numDivisionID AND numDefaultShippingServiceID=16 ) > 0 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.numDefaultShippingServiceID=17',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.numDefaultShippingServiceID=17',' 1 = (SELECT CASE WHEN (SELECT COUNT(*) FROM DivisionMaster WHERE numDivisionID=Div.numDivisionID AND numDefaultShippingServiceID=17 ) > 0 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.numDefaultShippingServiceID=18',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.numDefaultShippingServiceID=18',' 1 = (SELECT CASE WHEN (SELECT COUNT(*) FROM DivisionMaster WHERE numDivisionID=Div.numDivisionID AND numDefaultShippingServiceID=18 ) > 0 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.numDefaultShippingServiceID=19',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.numDefaultShippingServiceID=19',' 1 = (SELECT CASE WHEN (SELECT COUNT(*) FROM DivisionMaster WHERE numDivisionID=Div.numDivisionID AND numDefaultShippingServiceID=19 ) > 0 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.numDefaultShippingServiceID=20',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.numDefaultShippingServiceID=20',' 1 = (SELECT CASE WHEN (SELECT COUNT(*) FROM DivisionMaster WHERE numDivisionID=Div.numDivisionID AND numDefaultShippingServiceID=20 ) > 0 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.numDefaultShippingServiceID=21',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.numDefaultShippingServiceID=21',' 1 = (SELECT CASE WHEN (SELECT COUNT(*) FROM DivisionMaster WHERE numDivisionID=Div.numDivisionID AND numDefaultShippingServiceID=21 ) > 0 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.numDefaultShippingServiceID=22',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.numDefaultShippingServiceID=22',' 1 = (SELECT CASE WHEN (SELECT COUNT(*) FROM DivisionMaster WHERE numDivisionID=Div.numDivisionID AND numDefaultShippingServiceID=22 ) > 0 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.numDefaultShippingServiceID=23',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.numDefaultShippingServiceID=23',' 1 = (SELECT CASE WHEN (SELECT COUNT(*) FROM DivisionMaster WHERE numDivisionID=Div.numDivisionID AND numDefaultShippingServiceID=23 ) > 0 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.numDefaultShippingServiceID=24',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.numDefaultShippingServiceID=24',' 1 = (SELECT CASE WHEN (SELECT COUNT(*) FROM DivisionMaster WHERE numDivisionID=Div.numDivisionID AND numDefaultShippingServiceID=24 ) > 0 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.numDefaultShippingServiceID=25',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.numDefaultShippingServiceID=25',' 1 = (SELECT CASE WHEN (SELECT COUNT(*) FROM DivisionMaster WHERE numDivisionID=Div.numDivisionID AND numDefaultShippingServiceID=25 ) > 0 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.numDefaultShippingServiceID=26',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.numDefaultShippingServiceID=26',' 1 = (SELECT CASE WHEN (SELECT COUNT(*) FROM DivisionMaster WHERE numDivisionID=Div.numDivisionID AND numDefaultShippingServiceID=26 ) > 0 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.numDefaultShippingServiceID=27',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.numDefaultShippingServiceID=27',' 1 = (SELECT CASE WHEN (SELECT COUNT(*) FROM DivisionMaster WHERE numDivisionID=Div.numDivisionID AND numDefaultShippingServiceID=27 ) > 0 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.numDefaultShippingServiceID=28',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.numDefaultShippingServiceID=28',' 1 = (SELECT CASE WHEN (SELECT COUNT(*) FROM DivisionMaster WHERE numDivisionID=Div.numDivisionID AND numDefaultShippingServiceID=28 ) > 0 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.numDefaultShippingServiceID=40',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.numDefaultShippingServiceID=40',' 1 = (SELECT CASE WHEN (SELECT COUNT(*) FROM DivisionMaster WHERE numDivisionID=Div.numDivisionID AND numDefaultShippingServiceID=40 ) > 0 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.numDefaultShippingServiceID=42',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.numDefaultShippingServiceID=42',' 1 = (SELECT CASE WHEN (SELECT COUNT(*) FROM DivisionMaster WHERE numDivisionID=Div.numDivisionID AND numDefaultShippingServiceID=42 ) > 0 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.numDefaultShippingServiceID=43',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.numDefaultShippingServiceID=43',' 1 = (SELECT CASE WHEN (SELECT COUNT(*) FROM DivisionMaster WHERE numDivisionID=Div.numDivisionID AND numDefaultShippingServiceID=43 ) > 0 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.numDefaultShippingServiceID=48',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.numDefaultShippingServiceID=48',' 1 = (SELECT CASE WHEN (SELECT COUNT(*) FROM DivisionMaster WHERE numDivisionID=Div.numDivisionID AND numDefaultShippingServiceID=48 ) > 0 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.numDefaultShippingServiceID=49',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.numDefaultShippingServiceID=49',' 1 = (SELECT CASE WHEN (SELECT COUNT(*) FROM DivisionMaster WHERE numDivisionID=Div.numDivisionID AND numDefaultShippingServiceID=49 ) > 0 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.numDefaultShippingServiceID=50',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.numDefaultShippingServiceID=50',' 1 = (SELECT CASE WHEN (SELECT COUNT(*) FROM DivisionMaster WHERE numDivisionID=Div.numDivisionID AND numDefaultShippingServiceID=50 ) > 0 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.numDefaultShippingServiceID=51',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.numDefaultShippingServiceID=51',' 1 = (SELECT CASE WHEN (SELECT COUNT(*) FROM DivisionMaster WHERE numDivisionID=Div.numDivisionID AND numDefaultShippingServiceID=51 ) > 0 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.numDefaultShippingServiceID=55',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.numDefaultShippingServiceID=55',' 1 = (SELECT CASE WHEN (SELECT COUNT(*) FROM DivisionMaster WHERE numDivisionID=Div.numDivisionID AND numDefaultShippingServiceID=55 ) > 0 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.numDefaultShippingServiceID=70',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.numDefaultShippingServiceID=70',' 1 = (SELECT CASE WHEN (SELECT COUNT(*) FROM DivisionMaster WHERE numDivisionID=Div.numDivisionID AND numDefaultShippingServiceID=70 ) > 0 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.numDefaultShippingServiceID=71',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.numDefaultShippingServiceID=71',' 1 = (SELECT CASE WHEN (SELECT COUNT(*) FROM DivisionMaster WHERE numDivisionID=Div.numDivisionID AND numDefaultShippingServiceID=71 ) > 0 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.numDefaultShippingServiceID=72',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.numDefaultShippingServiceID=72',' 1 = (SELECT CASE WHEN (SELECT COUNT(*) FROM DivisionMaster WHERE numDivisionID=Div.numDivisionID AND numDefaultShippingServiceID=72 ) > 0 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.numDefaultShippingServiceID=73',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.numDefaultShippingServiceID=73',' 1 = (SELECT CASE WHEN (SELECT COUNT(*) FROM DivisionMaster WHERE numDivisionID=Div.numDivisionID AND numDefaultShippingServiceID=73 ) > 0 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.numDefaultShippingServiceID=74',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.numDefaultShippingServiceID=74',' 1 = (SELECT CASE WHEN (SELECT COUNT(*) FROM DivisionMaster WHERE numDivisionID=Div.numDivisionID AND numDefaultShippingServiceID=74 ) > 0 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.numDefaultShippingServiceID=75',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.numDefaultShippingServiceID=75',' 1 = (SELECT CASE WHEN (SELECT COUNT(*) FROM DivisionMaster WHERE numDivisionID=Div.numDivisionID AND numDefaultShippingServiceID=75 ) > 0 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.numDefaultShippingServiceID=76',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.numDefaultShippingServiceID=76',' 1 = (SELECT CASE WHEN (SELECT COUNT(*) FROM DivisionMaster WHERE numDivisionID=Div.numDivisionID AND numDefaultShippingServiceID=76 ) > 0 THEN 1 ELSE 0 END) ')
	END

	IF CHARINDEX('OBD.monAmountPaid',@vcRegularSearchCriteria) > 0
	BEGIN
		IF CHARINDEX('OBD.monAmountPaid like ''%0%''',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'OBD.monAmountPaid like ''%0%''',' 1 = (SELECT ISNULL((SELECT SUM(ISNULL(monAmountPaid,0)) FROM  OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)) ')
		IF CHARINDEX('OBD.monAmountPaid like ''%1%''',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'OBD.monAmountPaid like ''%1%''','  1 = (SELECT (CASE WHEN (SELECT (CASE WHEN (SUM(ISNULL(monDealAmount,0)) = SUM(ISNULL(monAmountPaid,0)) AND SUM(ISNULL(monAmountPaid,0)) <> 0) THEN 1 ELSE 0 END) FROM OpportunityBizDocs BD WHERE BD.numOppId =  Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0) = 1) > 0 THEN 1 ELSE 0 END))  ')
		IF CHARINDEX('OBD.monAmountPaid like ''%2%''',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'OBD.monAmountPaid like ''%2%''',' 1 = (SELECT CASE WHEN (SELECT (CASE WHEN ((SUM(ISNULL(monDealAmount,0)) - SUM(ISNULL(monAmountPaid,0))) > 0 AND SUM(ISNULL(monAmountPaid,0)) <> 0) THEN 1 ELSE 0 END) FROM OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0) = 1) > 0 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('OBD.monAmountPaid like ''%3%''',@vcRegularSearchCriteria) > 0								   
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'OBD.monAmountPaid like ''%3%''',' 1 = (SELECT CASE WHEN (SELECT (CASE WHEN (SUM(ISNULL(monAmountPaid,0)) = 0) THEN 1 ELSE 0 END) FROM OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0) = 1) > 0 THEN 1 ELSE 0 END) ')
	END
	
	
	IF @vcRegularSearchCriteria<>'' 
	BEGIN
		SET @strSql=@strSql +' AND ' + @vcRegularSearchCriteria 
	END
	
	


	IF @vcCustomSearchCriteria<>'' 
	BEGIN
		SET @strSql = @strSql +' AND ' +  @vcCustomSearchCriteria
	END

	IF LEN(@SearchQuery) > 0
	BEGIN
		SET @strSql = @strSql + ' AND (' + @SearchQuery + ') '
	END

	IF ISNULL(@tintDashboardReminderType,0) = 19
	BEGIN
		SET @strSql = CONCAT(@strSql,' AND Opp.numOppID IN (','SELECT DISTINCT
																	OMInner.numOppID
																FROM 
																	StagePercentageDetails SPDInner
																INNER JOIN 
																	OpportunityMaster OMInner
																ON 
																	SPDInner.numOppID=OMInner.numOppId 
																WHERE 
																	SPDInner.numDomainId=',@numDOmainID,'
																	AND OMInner.numDomainId=',@numDOmainID,' 
																	AND OMInner.tintOppType = 1
																	AND ISNULL(OMInner.tintOppStatus,0) = 1
																	AND tinProgressPercentage <> 100',') ')

	END
	ELSE IF ISNULL(@tintDashboardReminderType,0) = 10
	BEGIN
		SET @strSql = CONCAT(@strSql,' AND Opp.numOppID IN (','SELECT DISTINCT
																	OMInner.numOppId
																FROM
																	OpportunityMaster OMInner
																INNER JOIN
																	OpportunityItems OIInner
																ON
																	OIInner.numOppId = OMInner.numOppID
																INNER JOIN
																	Item IInner
																ON
																	OIInner.numItemCode = IInner.numItemCode
																OUTER APPLY
																(
																	SELECT
																		SUM(OpportunityBizDocItems.numUnitHour) AS BilledQty
																	FROM
																		OpportunityBizDocs
																	INNER JOIN
																		OpportunityBizDocItems 
																	ON
																		OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																	WHERE
																		OpportunityBizDocs.numOppId = OMInner.numOppID
																		AND OpportunityBizDocItems.numOppItemID = OIInner.numoppitemtCode
																		AND OpportunityBizDocs.numBizDocId = (SELECT ISNULL(numAuthoritativePurchase,0) FROM AuthoritativeBizDocs WHERE numDomainId=',@numDomainID,')
																) AS TempBilled
																WHERE
																	OMInner.numDomainId = ',@numDomainID,'
																	AND ISNULL(OMInner.bitStockTransfer,0) = 0
																	AND OMInner.tintOppType = 2
																	AND OMInner.tintOppStatus  = 1
																	AND ISNULL(OIInner.numUnitHour,0) <> ISNULL(TempBilled.BilledQty,0)',') ')
	END
	ELSE IF ISNULL(@tintDashboardReminderType,0) = 11
	BEGIN
		SET @strSql = CONCAT(@strSql,' AND Opp.numOppID IN (','SELECT DISTINCT
																	OMInner.numOppId
																FROM
																	OpportunityMaster OMInner
																INNER JOIN
																	OpportunityItems OIInner
																ON
																	OIInner.numOppId = OMInner.numOppId
																INNER JOIN
																	Item IInner
																ON
																	OIInner.numItemCode = IInner.numItemCode
																OUTER APPLY
																(
																	SELECT
																		SUM(OpportunityBizDocItems.numUnitHour) AS PickPacked
																	FROM
																		OpportunityBizDocs
																	INNER JOIN
																		OpportunityBizDocItems 
																	ON
																		OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																	WHERE
																		OpportunityBizDocs.numOppId = OMInner.numOppId
																		AND ISNULL(OpportunityBizDocs.numBizDocId,0) = ',(SELECT ISNULL(numDefaultSalesShippingDoc,0) FROM Domain WHERE numDomainId=@numDomainID),'
																		AND OpportunityBizDocItems.numOppItemID = OIInner.numoppitemtCode
																) AS TempFulFilled
																WHERE
																	OMInner.numDomainId = ',@numDomainID,'
																	AND OMInner.tintOppType = 1
																	AND OMInner.tintOppStatus = 1
																	AND UPPER(IInner.charItemType) = ''P''
																	AND ISNULL(OMInner.tintshipped,0) = 0
																	AND ISNULL(OIInner.bitDropShip,0) = 0
																	AND ISNULL(OIInner.numUnitHour,0) <> ISNULL(TempFulFilled.PickPacked,0)',') ')
	END
	ELSE IF ISNULL(@tintDashboardReminderType,0) = 12
	BEGIN
		SET @strSql = CONCAT(@strSql,' AND Opp.numOppID IN (','SELECT DISTINCT
																	OMInner.numOppId
																FROM
																	OpportunityMaster OMInner
																INNER JOIN
																	OpportunityItems OIInner
																ON
																	OIInner.numOppId = OMInner.numOppId
																INNER JOIN
																	Item IInner
																ON
																	OIInner.numItemCode = IInner.numItemCode
																OUTER APPLY
																(
																	SELECT
																		SUM(OpportunityBizDocItems.numUnitHour) AS FulFilledQty
																	FROM
																		OpportunityBizDocs
																	INNER JOIN
																		OpportunityBizDocItems 
																	ON
																		OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																	WHERE
																		OpportunityBizDocs.numOppId = OMInner.numOppId
																		AND ISNULL(OpportunityBizDocs.numBizDocId,0) = 296
																		AND ISNULL(OpportunityBizDocs.bitFulFilled,0) = 1
																		AND OpportunityBizDocItems.numOppItemID = OIInner.numoppitemtCode
																) AS TempFulFilled
																WHERE
																	OMInner.numDomainId = ',@numDomainID,'
																	AND OMInner.tintOppType = 1
																	AND OMInner.tintOppStatus = 1
																	AND ISNULL(OMInner.tintshipped,0) = 0
																	AND UPPER(IInner.charItemType) = ''P''
																	AND ISNULL(OIInner.bitDropShip,0) = 0
																	AND ISNULL(OIInner.numUnitHour,0) <> ISNULL(TempFulFilled.FulFilledQty,0)',') ')
	END
	ELSE IF ISNULL(@tintDashboardReminderType,0) = 13
	BEGIN
		SET @strSql = CONCAT(@strSql,' AND Opp.numOppID IN (','SELECT DISTINCT
																	OMInner.numOppId
																FROM
																	OpportunityMaster OMInner
																INNER JOIN
																	OpportunityItems OIInner
																ON
																	OIInner.numOppId = OMInner.numOppID
																INNER JOIN
																	Item IInner
																ON
																	OIInner.numItemCode = IInner.numItemCode
																OUTER APPLY
																(
																	SELECT
																		SUM(OpportunityBizDocItems.numUnitHour) AS InvoicedQty
																	FROM
																		OpportunityBizDocs
																	INNER JOIN
																		OpportunityBizDocItems 
																	ON
																		OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																	WHERE
																		OpportunityBizDocs.numOppId = OMInner.numOppID
																		AND OpportunityBizDocItems.numOppItemID = OIInner.numoppitemtCode
																		AND OpportunityBizDocs.numBizDocId = ISNULL((SELECT TOP 1 numAuthoritativeSales FROM AuthoritativeBizDocs WHERE numDomainId=',@numDomainID,'),287)
																) AS TempInvoice
																WHERE
																	OMInner.numDomainId = ',@numDomainID,'
																	AND OMInner.tintOppType = 1
																	AND OMInner.tintOppStatus  = 1
																	AND ISNULL(OIInner.numUnitHour,0) <> ISNULL(TempInvoice.InvoicedQty,0)',') ')
	END
	ELSE IF ISNULL(@tintDashboardReminderType,0) = 14
	BEGIN
		DECLARE @TEMPOppID TABLE
		(
			numOppID NUMERIC(18,0)
		)

		INSERT INTO @TEMPOppID
		(
			numOppID
		)
		SELECT DISTINCT
			OM.numOppID
		FROM
			OpportunityMaster OM
		INNER JOIN
			OpportunityItems OI
		ON
			OM.numOppId = OI.numOppId
		WHERE   
			OM.numDomainId = @numDomainId
			AND OM.tintOppType = 1
			AND OM.tintOppStatus=1
			AND ISNULL(OI.bitDropShip, 0) = 1
			AND 1 = (CASE WHEN (SELECT COUNT(*) FROM OpportunityLinking OMInner INNER JOIN OpportunityItems OIInner ON OMInner.numChildOppID=OIInner.numOppId WHERE OMInner.numParentOppID = OM.numOppID AND OIInner.numItemCode=OI.numItemCode AND ISNULL(OIInner.numWarehouseItmsID,0)=ISNULL(OI.numWarehouseItmsID,0)) > 0 THEN 0 ELSE 1 END)


		If ISNULL((SELECT bitReOrderPoint FROM Domain WHERE numDomainId=@numDomainID),0) = 1
		BEGIN
			INSERT INTO @TEMPOppID
			(
				numOppID
			)
			SELECT DISTINCT
				OM.numOppID
			FROM
				OpportunityMaster OM
			INNER JOIN
				OpportunityItems OI
			ON
				OI.numOppId = OM.numOppId
			INNER JOIN 
				Item I 
			ON 
				I.numItemCode = OI.numItemCode
			INNER JOIN 
				WarehouseItems WI 
			ON 
				OI.numWarehouseItmsID = WI.numWareHouseItemID
			WHERE   
				OM.numDomainId = @numDomainId
				AND ISNULL((SELECT bitReOrderPoint FROM Domain WHERE numDomainId=@numDomainID),0) = 1
				AND (ISNULL(WI.numOnHand,0) + ISNULL(WI.numOnOrder,0) <= ISNULL(WI.numReOrder,0))
				AND OM.tintOppType = 1
				AND OM.tintOppStatus=1
				AND ISNULL(I.bitAssembly, 0) = 0
				AND ISNULL(I.bitKitParent, 0) = 0
				AND ISNULL(OI.bitDropship,0) = 0
				AND OM.numOppId NOT IN (SELECT numOppID FROM @TEMPOppID)

			IF (SELECT COUNT(*) FROM @TEMPOppID) > 0
			BEGIN
				DECLARE @tmpIds VARCHAR(MAX) = ''
				SELECT @tmpIds = CONCAT(@tmpIds,numOppID,', ') FROM @TEMPOppID
				SET @strSql = CONCAT(@strSql,' AND Opp.numOppID IN (',SUBSTRING(@tmpIds, 0, LEN(@tmpIds)),') ')
			END
			ELSE
			BEGIN
				SET @strSql = CONCAT(@strSql,' AND Opp.numOppID IN (0) ')
			END
		END
	END
	ELSE IF ISNULL(@tintDashboardReminderType,0) = 15
	BEGIN
		SET @strSql = CONCAT(@strSql,' AND Opp.numOppID IN (','SELECT
																	OMInner.numOppID
																FROM
																	OpportunityMaster OMInner
																WHERE
																	numDomainId=',@numDomainID,'
																	AND tintOppType=1
																	AND tintOppStatus=1
																	AND ISNULL(OMInner.tintshipped,0) = 0
																	AND dtReleaseDate IS NOT NULL
																	AND dtReleaseDate < DateAdd(minute,',@ClientTimeZoneOffset * -1,',GETUTCDATE())
																	AND (SELECT 
																			COUNT(*) 
																		FROM 
																		(
																			SELECT
																				OIInner.numoppitemtCode,
																				ISNULL(OIInner.numUnitHour,0) AS OrderedQty,
																				ISNULL(TempFulFilled.FulFilledQty,0) AS FulFilledQty
																			FROM
																				OpportunityItems OIInner
																			INNER JOIN
																				Item IInner
																			ON
																				OIInner.numItemCode = IInner.numItemCode
																			OUTER APPLY
																			(
																				SELECT
																					SUM(OpportunityBizDocItems.numUnitHour) AS FulFilledQty
																				FROM
																					OpportunityBizDocs
																				INNER JOIN
																					OpportunityBizDocItems 
																				ON
																					OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																				WHERE
																					OpportunityBizDocs.numOppId = OMInner.numOppId
																					AND ISNULL(OpportunityBizDocs.numBizDocId,0) = 296
																					AND ISNULL(OpportunityBizDocs.bitFulFilled,0) = 1
																					AND OpportunityBizDocItems.numOppItemID = OIInner.numoppitemtCode
																			) AS TempFulFilled
																			WHERE
																				OIInner.numOppID = OMInner.numOppID
																				AND UPPER(IInner.charItemType) = ''P''
																				AND ISNULL(OIInner.bitDropShip,0) = 0
																		) X
																		WHERE
																			X.OrderedQty <> X.FulFilledQty) > 0',') ')
	END



	DECLARE  @firstRec  AS INTEGER
	DECLARE  @lastRec  AS INTEGER
  
	SET @firstRec = (@CurrentPage - 1) * @PageSize
	SET @lastRec = (@CurrentPage * @PageSize + 1)



	-- EXECUTE FINAL QUERY
	DECLARE @strFinal AS NVARCHAR(MAX)
	SET @strFinal = CONCAT('SELECT ROW_NUMBER() OVER(ORDER BY ',@columnName,' ',@columnSortOrder,') ID, Opp.numOppID INTO #temp2', REPLACE(@strSql,'##PLACEHOLDER##',''),'; SELECT ID,',@strColumns,' INTO #tempTable',REPLACE(@strSql,'##PLACEHOLDER##',' JOIN #temp2 tblAllOrders ON Opp.numOppID = tblAllOrders.numOppID '),' AND tblAllOrders.ID > ',@firstRec,' and tblAllOrders.ID <',@lastRec,' ORDER BY ID; SELECT * FROM  #tempTable; DROP TABLE #tempTable; SELECT @TotalRecords = COUNT(*) FROM #temp2; DROP TABLE #temp2;')
	--SET @strFinal = CONCAT('SELECT ROW_NUMBER() OVER(ORDER BY ',@columnName,' ',@columnSortOrder,') ID, ',@strColumns,' INTO #tempTable',@strSql,'; SELECT * FROM #tempTable WHERE ID > ',@firstRec,' and ID <',@lastRec,' ORDER BY ID; SELECT @TotalRecords = COUNT(*) FROM #tempTable; DROP TABLE #tempTable;')

	PRINT CAST(@strFinal AS NTEXT)
	EXEC sp_executesql @strFinal, N'@TotalRecords INT OUT', @TotRecs OUT

	SELECT * FROM #tempForm

	DROP TABLE #tempForm

	DROP TABLE #tempColorScheme
/****** Object:  StoredProcedure [dbo].[usp_getDynamicFormFields]    Script Date: 07/26/2008 16:17:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----Created By: Debasish Tapan Nag                                                                          
----Purpose: Returns the available form fields from the database                                                                              
----Created Date: 07/09/2005                                
----exec usp_getDynamicFormFields 1,'',1,1,0                            
----Modified by anoop jayaraj                                                                         
----Modified by Gangadhar
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'usp_getdynamicformfields' ) 
    DROP PROCEDURE usp_getdynamicformfields
GO
CREATE PROCEDURE [dbo].[usp_getDynamicFormFields]
    @numDomainId NUMERIC(9),
    @cCustomFieldsAssociated VARCHAR(10),
    @numFormId INT,
	@numSubFormId NUMERIC(9),
    @numAuthGroupId NUMERIC(9),
	@numBizDocTemplateID numeric(9)
AS 
	DECLARE @vcLocationID VARCHAR(100);SET @vcLocationID='0'
	Select @vcLocationID =isnull(vcLocationID,'0') from dynamicFormMaster where numFormID=@numFormId

    CREATE TABLE #tempAvailableFields(numFormFieldId  NVARCHAR(20),vcFormFieldName NVARCHAR(100),
        vcFieldType CHAR(1),vcAssociatedControlType NVARCHAR(50),numListID NUMERIC(18),vcDbColumnName NVARCHAR(50),
        vcListItemType CHAR(3),intColumnNum INT,intRowNum INT,boolRequired tinyint, numAuthGroupID NUMERIC(18),
          vcFieldDataType CHAR(1),boolAOIField tinyint,numFormID NUMERIC(18),vcLookBackTableName NVARCHAR(50),vcFieldAndType NVARCHAR(50),numSubFormId NUMERIC(18))
   
   
   CREATE TABLE #tempAddedFields(numFormFieldId  NVARCHAR(20),vcFormFieldName NVARCHAR(100),
        vcFieldType CHAR(1),vcAssociatedControlType NVARCHAR(50),numListID NUMERIC(18),vcDbColumnName NVARCHAR(50),
        vcListItemType CHAR(3),intColumnNum INT,intRowNum INT,boolRequired tinyint, numAuthGroupID NUMERIC(18),
          vcFieldDataType CHAR(1),boolAOIField tinyint,numFormID NUMERIC(18),vcLookBackTableName NVARCHAR(50),vcFieldAndType NVARCHAR(50),numSubFormId NUMERIC(18))
 
   INSERT INTO #tempAddedFields (numFormFieldId,vcFormFieldName,vcFieldType,vcAssociatedControlType
        ,numListID,vcDbColumnName,vcListItemType,intColumnNum,intRowNum,boolRequired,numAuthGroupID,
          vcFieldDataType,boolAOIField,numFormID,vcLookBackTableName,vcFieldAndType,numSubFormId)
     SELECT  CONVERT(VARCHAR(15), numFieldId) + vcFieldType AS numFormFieldId,
                    ISNULL(vcCultureFieldName,vcFieldName) AS vcFormFieldName,
                    'R' as vcFieldType,
                    vcAssociatedControlType,
                    numListID,
                    vcDbColumnName,
                    vcListItemType,
                    tintColumn as intColumnNum,
                    tintRow as intRowNum,
                    bitIsRequired as boolRequired,
                    ISNULL(numAuthGroupID, 0) numAuthGroupID,
                    CASE WHEN vcAssociatedControlType = 'DateField' THEN 'D' ELSE vcFieldDataType END vcFieldDataType,
                    ISNULL(boolAOIField, 0) boolAOIField,
                    numFormID,
                    vcLookBackTableName,
                    CONVERT(VARCHAR(15), numFieldId) + '~' +  CONVERT(VARCHAR(15),ISNULL(bitCustom,0)) + '~' + CONVERT(VARCHAR(15),ISNULL(tintOrder,0)) AS vcFieldAndType
					,ISNULL(numSubFormId,0) AS numSubFormId
			FROM    View_DynamicColumns
            WHERE   numFormID = @numFormId 
					AND numAuthGroupID = @numAuthGroupId
                    AND numDomainID = @numDomainId
                    AND isnull(numRelCntType,0)=@numBizDocTemplateID
					AND ISNULL(numSubFormID,0)=ISNULL(@numSubFormId,0)
					AND bitDeleted=0 

            UNION

            SELECT  CONVERT(VARCHAR(15), numFieldId) + 'C' AS numFormFieldId,
                    vcFieldName AS vcFormFieldName,
                    vcFieldType,
					 vcAssociatedControlType,
                    ISNULL(numListID, 0) numListID,
                    vcFieldName vcDbColumnName,
                    CASE WHEN numListID > 0 THEN 'LI' ELSE '' END vcListItemType,
                    ISNULL(tintColumn,0) as intColumnNum,
                    isnull(tintRow,0) as intRowNum,
                    ISNULL(bitIsRequired,0) as boolRequired,
                    ISNULL(numAuthGroupID, 0) numAuthGroupID,
                    CASE WHEN numListID > 0 THEN 'N' WHEN vcAssociatedControlType = 'DateField' THEN 'D' ELSE 'V' END vcFieldDataType,
                    ISNULL(boolAOIField, 0),
                    numFormID,
                    '' AS vcLookBackTableName,
                    CONVERT(VARCHAR(15), numFieldId) + '~' + CONVERT(VARCHAR(15),ISNULL(bitCustom,0)) + '~' + CONVERT(VARCHAR(15),ISNULL(tintOrder,0)) AS vcFieldAndType
					,ISNULL(numSubFormId,0) AS numSubFormId
			FROM    View_DynamicCustomColumns                                            
            WHERE   numDomainID = @numDomainId
                    AND numFormID = @numFormId
                    AND numAuthGroupID = @numAuthGroupId
					AND ISNULL(numSubFormID,0)=ISNULL(@numSubFormId,0)
                    AND GRP_ID IN(	SELECT ID FROM dbo.SplitIDs(@vcLocationID,','))     
					 and isnull(numRelCntType,0)=@numBizDocTemplateID         


        INSERT INTO #tempAvailableFields (numFormFieldId,vcFormFieldName,vcFieldType,vcAssociatedControlType
        ,numListID,vcDbColumnName,vcListItemType,intColumnNum,intRowNum,boolRequired,numAuthGroupID,
          vcFieldDataType,boolAOIField,numFormID,vcLookBackTableName,vcFieldAndType,numSubFormId)                         
            SELECT  CONVERT(VARCHAR(15),numFieldId) + vcFieldType AS numFormFieldId,
                    ISNULL(vcCultureFieldName,vcFieldName) AS vcFormFieldName,
                    'R' as vcFieldType,
                    vcAssociatedControlType,
                    numListID,
                    vcDbColumnName,
                    vcListItemType,
                    0 intColumnNum,
                    0 intRowNum,
                    ISNULL(bitRequired,0) boolRequired,
                    0 numAuthGroupID,
                    CASE WHEN vcAssociatedControlType = 'DateField' THEN 'D' ELSE vcFieldDataType END vcFieldDataType,
                    0 boolAOIField,
                    numFormID,
                    vcLookBackTableName,
                    CONVERT(VARCHAR(15), numFieldId) + '~' + CONVERT(VARCHAR(15),ISNULL(bitCustom,0)) + '~' + CONVERT(VARCHAR(15),ISNULL(tintOrder,0)) AS vcFieldAndType
					,0 AS numSubFormId
			FROM    View_DynamicDefaultColumns
            WHERE   numFormID = @numFormId 
					AND bitDeleted=0
					AND numDomainID=@numDomainID
                    AND numFieldId NOT IN (
                    SELECT  numFieldId
                    FROM    View_DynamicColumns
                    WHERE   numFormID = @numFormId
							AND ISNULL(numSubFormID,0)=ISNULL(@numSubFormId,0)
                            AND numAuthGroupID = @numAuthGroupId
                            AND numDomainID = @numDomainId
                            and isnull(numRelCntType,0)=@numBizDocTemplateID)
           
			UNION
            
			SELECT  CONVERT(VARCHAR(15), Fld_id) + 'C' AS numFormFieldId,
                    Fld_label AS vcFormFieldName,
                    isnull(L.vcFieldType,'C') as vcFieldType,
                    fld_type as vcAssociatedControlType,
                    ISNULL(C.numListID, 0) numListID,
                    Fld_label vcDbColumnName,
                    CASE WHEN C.numListID > 0 THEN 'LI' ELSE '' END vcListItemType,
                    0 intColumnNum,
                    0 intRowNum,
                    ISNULL(V.bitIsRequired,0) as boolRequired,
                    0 numAuthGroupID,
                    CASE WHEN C.numListID > 0 THEN 'N' ELSE 'V' END vcFieldDataType,
                    0 boolAOIField,
                    @numFormId AS numFormID,
                    '' AS vcLookBackTableName,
                    CONVERT(VARCHAR(15), Fld_id) + '~1' + '~0' AS vcFieldAndType
					,0 AS numSubFormId
            FROM    CFW_Fld_Master C LEFT JOIN CFW_Validation V ON V.numFieldID = C.Fld_id
					JOIN CFW_Loc_Master L on C.GRP_ID=L.Loc_Id
            WHERE   C.numDomainID = @numDomainId
					AND GRP_ID IN(	SELECT ID FROM dbo.SplitIDs(@vcLocationID,','))
                    AND Fld_id NOT IN (
                    SELECT  numFieldId
                    FROM    View_DynamicCustomColumns
                    WHERE   numFormID = @numFormId
							AND GRP_ID IN(	SELECT ID FROM dbo.SplitIDs(@vcLocationID,','))
                            AND numAuthGroupID = @numAuthGroupId
                            AND numDomainID = @numDomainId and isnull(numRelCntType,0)=@numBizDocTemplateID)
        

		SELECT * FROM #tempAvailableFields WHERE numFormFieldId NOT IN (SELECT numFormFieldId FROM #tempAddedFields)
		AND #tempAvailableFields.vcLookBackTableName = (CASE @numAuthGroupId WHEN 40911 THEN 'CSGrid' ELSE REPLACE(#tempAvailableFields.vcLookBackTableName,'CSGrid','') END)
		 UNION            
        SELECT * FROM #tempAddedFields WHERE 1=1
		AND #tempAddedFields.vcLookBackTableName = (CASE @numAuthGroupId WHEN 40911 THEN 'CSGrid' ELSE REPLACE(#tempAddedFields.vcLookBackTableName,'CSGrid','') END)
        
        DROP TABLE #tempAvailableFields
        DROP TABLE #tempAddedFields

--	--added by chintan to select Custom field form multiple location
--	DECLARE @vcLocationID VARCHAR(100)
--	SET @vcLocationID = CASE WHEN @numFormId = 7 THEN '0'
--							 WHEN @numFormId = 8 THEN '0'
--							 WHEN @numFormId = 6 THEN '1'
--							 WHEN @numFormId = 17 THEN '0'
--							 WHEN @numFormId = 18 THEN '0'
--							 WHEN @numFormId = 29 THEN '9'
--							 WHEN @numFormId = 22 THEN '0'
--							 WHEN @numFormId = 30 THEN '0'
--							 ELSE '1,12,13,14'
--						END
--
--
--    CREATE TABLE #tempAvailableFields(numFormFieldId  NVARCHAR(20),vcFormFieldName NVARCHAR(50),
--        vcFieldType CHAR(1),vcAssociatedControlType NVARCHAR(50),numListID NUMERIC(18),vcDbColumnName NVARCHAR(50),
--        vcListItemType CHAR(1),intColumnNum INT,intRowNum INT,boolRequired tinyint, numAuthGroupID NUMERIC(18),
--          vcFieldDataType CHAR(1),boolAOIField tinyint,numFormID NUMERIC(18),vcLookBackTableName NVARCHAR(50))
--   
--   
--   CREATE TABLE #tempAddedFields(numFormFieldId  NVARCHAR(20),vcFormFieldName NVARCHAR(50),
--        vcFieldType CHAR(1),vcAssociatedControlType NVARCHAR(50),numListID NUMERIC(18),vcDbColumnName NVARCHAR(50),
--        vcListItemType CHAR(1),intColumnNum INT,intRowNum INT,boolRequired tinyint, numAuthGroupID NUMERIC(18),
--          vcFieldDataType CHAR(1),boolAOIField tinyint,numFormID NUMERIC(18),vcLookBackTableName NVARCHAR(50))
-- 
--    IF @numFormId <> 15
--        AND @numFormId <> 6 
--        BEGIN           
--        
--           
--        INSERT INTO #tempAddedFields (numFormFieldId,vcFormFieldName,vcFieldType,vcAssociatedControlType
--        ,numListID,vcDbColumnName,vcListItemType,intColumnNum,intRowNum,boolRequired,numAuthGroupID,
--          vcFieldDataType,boolAOIField,numFormID,vcLookBackTableName)
--        SELECT  CONVERT(VARCHAR(15), D.numFormFieldId) + D.vcFieldType AS numFormFieldId,
--                    ISNULL(DFV.vcNewFormFieldName,D.vcFormFieldName) AS vcFormFieldName,
--                    D.vcFieldType,
--                    D.vcAssociatedControlType,
--                    D.numListID,
--                    D.vcDbColumnName,
--                    vcListItemType,
--                    ISNULL(intColumnNum, 0) intColumnNum,
--                    ISNULL(intRowNum, 0) intRowNum,
--                    ISNULL(DFV.bitIsRequired,0) boolRequired,
--                    ISNULL(numAuthGroupID, 0) numAuthGroupID,
--                    CASE WHEN D.vcAssociatedControlType = 'DateField' THEN 'D'
--                         ELSE vcFieldDataType
--                    END vcFieldDataType,
--                    ISNULL(boolAOIField, 0) boolAOIField,
--                    D.numFormID,
--                    vcLookBackTableName
--            FROM    DynamicFormFieldMaster D
--                    JOIN DynamicFormConfigurationDetails DTL ON DTL.numFormFieldId = D.numFormFieldId
--                    LEFT JOIN DynamicFormField_Validation DFV ON DFV.numDomainID=DTL.numDomainID AND D.numFormFieldId=DFV.numFormFieldId AND D.numFormId=DFV.numFormId
--            WHERE   D.numFormID = @numFormId AND DTL.numFormID=D.numFormID
--                    AND numAuthGroupID = @numAuthGroupId
--                    AND DTL.numDomainID = @numDomainId
--                    AND DTL.vcFieldType = 'R' and isnull(DTL.numRelCntType,0)=@numBizDocTemplateID
--					AND D.bitDeleted=0
--                 UNION
--            SELECT  CONVERT(VARCHAR(15), Fld_id) + 'C' AS numFormFieldId,
--                    Fld_label AS vcFormFieldName,
--                    'C' AS vcFieldType,
--                    CASE WHEN fld_type = 'Text Box' THEN 'EditBox'
--                         WHEN fld_type = 'Drop Down List Box' THEN 'SelectBox'
--                         WHEN fld_type = 'Check box' THEN 'Check box'
--                         WHEN fld_type = 'Text Area' THEN 'EditBox'
--                         WHEN fld_type = 'Date Field' THEN 'Date Field'
--                    END vcAssociatedControlType,
--                    ISNULL(C.numListID, 0) numListID,
--                    Fld_label vcDbColumnName,
--                    CASE WHEN C.numListID > 0 THEN 'L'
--                         ELSE ''
--                    END vcListItemType,
--                    ISNULL(intColumnNum, 0) intColumnNum,
--                    ISNULL(intRowNum, 0) intRowNum,
--                    ISNULL(V.bitIsRequired,0) as boolRequired,
--                    ISNULL(numAuthGroupID, 0) numAuthGroupID,
--                    CASE WHEN C.numListID > 0 THEN 'N'
--                         ELSE 'V'
--                    END vcFieldDataType,
--                    ISNULL(boolAOIField, 0),
--                    DTL.numFormID,
--                    '' AS vcLookBackTableName
--            FROM    CFW_Fld_Master C
--                    JOIN DynamicFormConfigurationDetails DTL ON DTL.numFormFieldId = C.Fld_id
--                                                                AND DTL.vcFieldType = 'C'
--                    LEFT JOIN CFW_Validation V ON V.numFieldID = C.Fld_id                                            
--            WHERE   DTL.numDomainID = @numDomainId
--                    AND C.numDomainID = @numDomainId
--                    AND numFormID = @numFormId
--                    AND numAuthGroupID = @numAuthGroupId
--                    AND GRP_ID = CASE WHEN @numFormId = 7 THEN 5
--                                      WHEN @numFormId = 8 THEN 5
--                                      WHEN @numFormId = 17 THEN 3
--                                      WHEN @numFormId = 18 THEN 11
--                                      WHEN @numFormId = 29 THEN 5
--                                      WHEN @numFormId = 6 THEN 1
--                                      WHEN @numFormId = 22 THEN 5
--                                      WHEN @numFormId = 30 THEN 5
--                                      ELSE 4
--                                 END 
--					 and isnull(DTL.numRelCntType,0)=@numBizDocTemplateID         
--                      UNION
--            SELECT  CONVERT(VARCHAR(15), Fld_id) + 'C' AS numFormFieldId,
--                    Fld_label AS vcFormFieldName,
--                     CASE GRP_ID WHEN 9 /*item warehouse attributes*/ THEN 'I'ELSE 'D' END AS vcFieldType,
--                    CASE WHEN fld_type = 'Text Box' THEN 'EditBox'
--                         WHEN fld_type = 'Drop Down List Box' THEN 'SelectBox'
--                         WHEN fld_type = 'Check box' THEN 'Check box'
--                         WHEN fld_type = 'Text Area' THEN 'EditBox'
--                         WHEN fld_type = 'Date Field' THEN 'Date Field'
--                    END vcAssociatedControlType,
--                    ISNULL(C.numListID, 0) numListID,
--                    Fld_label vcDbColumnName,
--                    CASE WHEN C.numListID > 0 THEN 'L'
--                         ELSE ''
--                    END vcListItemType,
--                    ISNULL(intColumnNum, 0) intColumnNum,
--                    ISNULL(intRowNum, 0) intRowNum,
--                    ISNULL(V.bitIsRequired,0) as boolRequired,
--                    ISNULL(numAuthGroupID, 0) numAuthGroupID,
--                    CASE WHEN C.numListID > 0 THEN 'N'
--                         WHEN fld_type = 'Date Field' THEN 'D'
--                         ELSE 'V'
--                    END vcFieldDataType,
--                    ISNULL(boolAOIField, 0),
--                    DTL.numFormID,
--                    '' AS vcLookBackTableName
--            FROM    CFW_Fld_Master C
--                    JOIN DynamicFormConfigurationDetails DTL ON DTL.numFormFieldId = C.Fld_id
--                                                                AND DTL.vcFieldType = 'D'
--                    LEFT JOIN CFW_Validation V ON V.numFieldID = C.Fld_id                                            
--            WHERE   DTL.numDomainID = @numDomainId
--                    AND C.numDomainID = @numDomainId
--                    AND numFormID = @numFormId
--                    AND numAuthGroupID = @numAuthGroupId
--                    AND GRP_ID IN(	SELECT ID FROM dbo.SplitIDs(@vcLocationID,','))            
--                    and isnull(DTL.numRelCntType,0)=@numBizDocTemplateID           
--          
--        INSERT INTO #tempAvailableFields (numFormFieldId,vcFormFieldName,vcFieldType,vcAssociatedControlType
--        ,numListID,vcDbColumnName,vcListItemType,intColumnNum,intRowNum,boolRequired,numAuthGroupID,
--          vcFieldDataType,boolAOIField,numFormID,vcLookBackTableName)                         
--            SELECT  CONVERT(VARCHAR(15), D.numFormFieldId) + D.vcFieldType AS numFormFieldId,
--                    ISNULL(DFV.vcNewFormFieldName,D.vcFormFieldName) AS vcFormFieldName,
--                    D.vcFieldType,
--                    D.vcAssociatedControlType,
--                    D.numListID,
--                    D.vcDbColumnName,
--                    vcListItemType,
--                    0 intColumnNum,
--                    0 intRowNum,
--                    ISNULL(DFV.bitIsRequired,0) boolRequired,
--                    0 numAuthGroupID,
--                    vcFieldDataType,
--                    0 boolAOIField,
--                    D.numFormID,
--                    vcLookBackTableName
--            FROM    DynamicFormFieldMaster D
--					LEFT JOIN DynamicFormField_Validation DFV ON DFV.numDomainID=@numDomainId AND D.numFormFieldId=DFV.numFormFieldId AND D.numFormId=DFV.numFormId
--            WHERE   D.numFormID = @numFormId
--					AND D.bitDeleted=0
--                    AND ( D.numDomainID IS NULL
--                          OR D.numDomainID = @numDomainId
--                        )
--                    AND D.numFormFieldId NOT IN (
--                    SELECT  numFormFieldId
--                    FROM    DynamicFormConfigurationDetails
--                    WHERE   numFormID = @numFormId
--                            AND numAuthGroupID = @numAuthGroupId
--                            AND numDomainID = @numDomainId
--                            AND vcFieldType = 'R' and isnull(numRelCntType,0)=@numBizDocTemplateID)
--           UNION
--            SELECT  CONVERT(VARCHAR(15), Fld_id) + 'C' AS numFormFieldId,
--                    Fld_label AS vcFormFieldName,
--                    'C' AS vcFieldType,
--                    CASE WHEN fld_type = 'Text Box' THEN 'EditBox'
--                         WHEN fld_type = 'Drop Down List Box' THEN 'SelectBox'
--                         WHEN fld_type = 'Check box' THEN 'Check box'
--                         WHEN fld_type = 'Text Area' THEN 'EditBox'
--                         WHEN fld_type = 'Date Field' THEN 'Date Field'
--                    END vcAssociatedControlType,
--                    ISNULL(C.numListID, 0) numListID,
--                    Fld_label vcDbColumnName,
--                    CASE WHEN C.numListID > 0 THEN 'L'
--                         ELSE ''
--                    END vcListItemType,
--                    0 intColumnNum,
--                    0 intRowNum,
--                    ISNULL(V.bitIsRequired,0) as boolRequired,
--                    0 numAuthGroupID,
--                    CASE WHEN C.numListID > 0 THEN 'N'
--                         ELSE 'V'
--                    END vcFieldDataType,
--                    0 boolAOIField,
--                    @numFormId AS numFormID,
--                    '' AS vcLookBackTableName
--            FROM    CFW_Fld_Master C LEFT JOIN CFW_Validation V ON V.numFieldID = C.Fld_id
--            WHERE   C.numDomainID = @numDomainId
--                    AND GRP_ID = CASE WHEN @numFormId = 7 THEN 5
--                                      WHEN @numFormId = 8 THEN 5
--                                      WHEN @numFormId = 6 THEN 1
--                                      WHEN @numFormId = 17 THEN 3
--                                      WHEN @numFormId = 18 THEN 11
--                                      WHEN @numFormId = 29 THEN 5
--                                      WHEN @numFormId = 22 THEN 5
--                                      WHEN @numFormId = 30 THEN 5
--                                      ELSE 4
--                                 END
--                    AND Fld_id NOT IN (
--                    SELECT  numFormFieldId
--                    FROM    DynamicFormConfigurationDetails
--                    WHERE   numFormID = @numFormId
--                            AND vcFieldType = 'C'
--                            AND numAuthGroupID = @numAuthGroupId
--                            AND numDomainID = @numDomainId and isnull(numRelCntType,0)=@numBizDocTemplateID)
--           
--            UNION
--            SELECT  CONVERT(VARCHAR(15), Fld_id) + 'C' AS numFormFieldId,
--                    Fld_label AS vcFormFieldName,
--                    CASE GRP_ID WHEN 9 /*item warehouse attributes*/ THEN 'I'ELSE 'D' END AS vcFieldType,
--                    CASE WHEN fld_type = 'Text Box' THEN 'EditBox'
--                         WHEN fld_type = 'Drop Down List Box' THEN 'SelectBox'
--                         WHEN fld_type = 'Check box' THEN 'Check box'
--                         WHEN fld_type = 'Text Area' THEN 'EditBox'
--                         WHEN fld_type = 'Date Field' THEN 'Date Field'
--                    END vcAssociatedControlType,
--                    ISNULL(C.numListID, 0) numListID,
--                    Fld_label vcDbColumnName,
--                    CASE WHEN C.numListID > 0 THEN 'L'
--                         ELSE ''
--                    END vcListItemType,
--                    0 intColumnNum,
--                    0 intRowNum,
--                    ISNULL(V.bitIsRequired,0) as boolRequired,
--                    0 numAuthGroupID,
--                    CASE WHEN C.numListID > 0 THEN 'N'
--                         ELSE 'V'
--                    END vcFieldDataType,
--                    0 boolAOIField,
--                    @numFormId AS numFormID,
--                    '' AS vcLookBackTableName
--            FROM    CFW_Fld_Master C LEFT JOIN CFW_Validation V ON V.numFieldID = C.Fld_id
--            WHERE   C.numDomainID = @numDomainId
--					AND GRP_ID IN(	SELECT ID FROM dbo.SplitIDs(@vcLocationID,','))
--                    AND Fld_id NOT IN (
--                    SELECT  numFormFieldId
--                    FROM    DynamicFormConfigurationDetails
--                    WHERE   numFormID = @numFormId
--                            AND vcFieldType = 'D'
--                            AND numAuthGroupID = @numAuthGroupId
--                            AND numDomainID = @numDomainId and isnull(numRelCntType,0)=@numBizDocTemplateID)
--
--        
--        END        
--    IF @numFormId = 15 -- Advance search of Opportunity and Orders       
--        BEGIN    
--        
--           INSERT INTO #tempAddedFields (numFormFieldId,vcFormFieldName,vcFieldType,vcAssociatedControlType
--        ,numListID,vcDbColumnName,vcListItemType,intColumnNum,intRowNum,boolRequired,numAuthGroupID,
--          vcFieldDataType,boolAOIField,numFormID,vcLookBackTableName)
--			SELECT  CONVERT(VARCHAR(15), D.numFormFieldId) + D.vcFieldType AS numFormFieldId,
--                    ISNULL(DFV.vcNewFormFieldName,D.vcFormFieldName) AS vcFormFieldName,
--                    D.vcFieldType,
--                    D.vcAssociatedControlType,
--                    D.numListID,
--                    D.vcDbColumnName,
--                    vcListItemType,
--                    ISNULL(intColumnNum, 0) intColumnNum,
--                    ISNULL(intRowNum, 0) intRowNum,
--                    ISNULL(DFV.bitIsRequired,0) boolRequired,
--                    ISNULL(numAuthGroupID, 0) numAuthGroupID,
--                    CASE WHEN D.vcAssociatedControlType = 'DateField' THEN 'D'
--                         ELSE vcFieldDataType
--                    END vcFieldDataType,
--                    ISNULL(boolAOIField, 0) boolAOIField,
--                    D.numFormID,
--                    vcLookBackTableName
--            FROM    DynamicFormFieldMaster D
--                    JOIN DynamicFormConfigurationDetails DTL ON DTL.numFormFieldId = D.numFormFieldId
--                    LEFT JOIN DynamicFormField_Validation DFV ON DFV.numDomainID=DTL.numDomainID AND D.numFormFieldId=DFV.numFormFieldId AND D.numFormId=DFV.numFormId
--            WHERE   D.numFormID = @numFormId AND DTL.numFormID=D.numFormID
--                    AND numAuthGroupID = @numAuthGroupId
--                    AND DTL.numDomainID = @numDomainId
--                    AND DTL.vcFieldType = 'R'
--                    AND DTL.[numFormId] = @numFormId --Was adding duplicate values in BizForm Wizard-by chintan bugid#57
--					AND D.bitDeleted=0
--            UNION 
--            SELECT  CONVERT(VARCHAR(15), Fld_id) + 'C' AS numFormFieldId,
--                    Fld_label AS vcFormFieldName,
--                    'O' AS vcFieldType,
--                    CASE WHEN fld_type = 'Text Box' THEN 'EditBox'
--                         WHEN fld_type = 'Drop Down List Box' THEN 'SelectBox'
--                         WHEN fld_type = 'Check box' THEN 'Check box'
--                         WHEN fld_type = 'Text Area' THEN 'EditBox'
--                         WHEN fld_type = 'Date Field' THEN 'Date Field'
--                    END vcAssociatedControlType,
--                    ISNULL(C.numListID, 0) numListID,
--                    Fld_label vcDbColumnName,
--                    CASE WHEN C.numListID > 0 THEN 'L'
--                         ELSE ''
--                    END vcListItemType,
--                    ISNULL(intColumnNum, 0) intColumnNum,
--                    ISNULL(intRowNum, 0) intRowNum,
--                    ISNULL(V.bitIsRequired,0) as boolRequired,
--                    ISNULL(numAuthGroupID, 0) numAuthGroupID,
--                    CASE WHEN C.numListID > 0 THEN 'N'
--                         WHEN fld_type = 'Date Field' THEN 'D'
--                         ELSE 'V'
--                    END vcFieldDataType,
--                    ISNULL(boolAOIField, 0),
--                    DTL.numFormID,
--                    '' AS vcLookBackTableName
--            FROM    CFW_Fld_Master C
--                    JOIN DynamicFormConfigurationDetails DTL ON DTL.numFormFieldId = C.Fld_id
--                                                                AND DTL.vcFieldType = 'O'
--                    LEFT JOIN CFW_Validation V ON V.numFieldID = C.Fld_id                                            
--            WHERE   DTL.numDomainID = @numDomainId
--                    AND C.numDomainID = @numDomainId
--                    AND numFormID = @numFormId
--                    AND numAuthGroupID = @numAuthGroupId
--                    AND GRP_ID IN ( 2, 6 )
--            UNION
--            SELECT  CONVERT(VARCHAR(15), Fld_id) + 'C' AS numFormFieldId,
--                    Fld_label AS vcFormFieldName,
--                    CASE Grp_id
--                      WHEN 9 THEN 'I'
--                      WHEN 5 THEN 'P'
--                    END AS vcFieldType,
--                    CASE WHEN fld_type = 'Text Box' THEN 'EditBox'
--                         WHEN fld_type = 'Drop Down List Box' THEN 'SelectBox'
--                         WHEN fld_type = 'Check box' THEN 'Check box'
--                         WHEN fld_type = 'Text Area' THEN 'EditBox'
--                         WHEN fld_type = 'Date Field' THEN 'Date Field'
--                    END vcAssociatedControlType,
--                    ISNULL(C.numListID, 0) numListID,
--                    Fld_label vcDbColumnName,
--                    CASE WHEN C.numListID > 0 THEN 'L'
--                         ELSE ''
--                    END vcListItemType,
--                    ISNULL(intColumnNum, 0) intColumnNum,
--                    ISNULL(intRowNum, 0) intRowNum,
--                    ISNULL(V.bitIsRequired,0) as boolRequired,
--                    ISNULL(numAuthGroupID, 0) numAuthGroupID,
--                    CASE WHEN C.numListID > 0 THEN 'N'
--                         WHEN fld_type = 'Date Field' THEN 'D'
--                         ELSE 'V'
--                    END vcFieldDataType,
--                    ISNULL(boolAOIField, 0),
--                    DTL.numFormID,
--                    '' AS vcLookBackTableName
--            FROM    CFW_Fld_Master C
--                    JOIN DynamicFormConfigurationDetails DTL ON DTL.numFormFieldId = C.Fld_id
--                                                                AND DTL.vcFieldType = 'I'
--                    LEFT JOIN CFW_Validation V ON V.numFieldID = C.Fld_id                                            
--            WHERE   DTL.numDomainID = @numDomainId
--                    AND C.numDomainID = @numDomainId
--                    AND numFormID = @numFormId
--                    AND numAuthGroupID = @numAuthGroupId
--                    AND GRP_ID IN ( 5, 9 )
--       
--       
--            INSERT INTO #tempAvailableFields (numFormFieldId,vcFormFieldName,vcFieldType,vcAssociatedControlType
--        ,numListID,vcDbColumnName,vcListItemType,intColumnNum,intRowNum,boolRequired,numAuthGroupID,
--          vcFieldDataType,boolAOIField,numFormID,vcLookBackTableName)                         
--            SELECT  CONVERT(VARCHAR(15), D.numFormFieldId) + D.vcFieldType AS numFormFieldId,
--                    ISNULL(DFV.vcNewFormFieldName,D.vcFormFieldName) AS vcFormFieldName,
--                    D.vcFieldType,
--                    D.vcAssociatedControlType,
--                    D.numListID,
--                    D.vcDbColumnName,
--                    vcListItemType,
--                    0 intColumnNum,
--                    0 intRowNum,
--                    ISNULL(DFV.bitIsRequired,0) boolRequired,
--                    0 numAuthGroupID,
--                    vcFieldDataType,
--                    0 boolAOIField,
--                    D.numFormID,
--                    vcLookBackTableName
--            FROM    DynamicFormFieldMaster D
--                    LEFT JOIN DynamicFormField_Validation DFV ON DFV.numDomainID=@numDomainId AND D.numFormFieldId=DFV.numFormFieldId AND D.numFormId=DFV.numFormId
--            WHERE   D.numFormID = @numFormId
--					AND D.bitDeleted=0
--                    AND D.numFormFieldId NOT IN (
--                    SELECT  numFormFieldId
--                    FROM    DynamicFormConfigurationDetails
--                    WHERE   numFormID = @numFormId
--                            AND numAuthGroupID = @numAuthGroupId
--                            AND numDomainID = @numDomainId
--                            AND vcFieldType = 'R' )
--            UNION
---- GRP_ID = 2 -> Sales Opportunity Custom Field
---- GRP_ID = 6 -> Purchase Opportunity Custom Field
--            SELECT  CONVERT(VARCHAR(15), Fld_id) + 'C' AS numFormFieldId,
--                    Fld_label AS vcFormFieldName,
--                    'O' AS vcFieldType,
--                    CASE WHEN fld_type = 'Text Box' THEN 'EditBox'
--                         WHEN fld_type = 'Drop Down List Box' THEN 'SelectBox'
--                         WHEN fld_type = 'Check box' THEN 'Check box'
--                         WHEN fld_type = 'Text Area' THEN 'EditBox'
--                         WHEN fld_type = 'Date Field' THEN 'Date Field'
--                    END vcAssociatedControlType,
--                    ISNULL(C.numListID, 0) numListID,
--                    Fld_label vcDbColumnName,
--                    CASE WHEN C.numListID > 0 THEN 'L'
--                         ELSE ''
--                    END vcListItemType,
--                    0 intColumnNum,
--                    0 intRowNum,
--                    ISNULL(V.bitIsRequired,0) as boolRequired,
--                    0 numAuthGroupID,
--                    CASE WHEN C.numListID > 0 THEN 'N'
--                         WHEN fld_type = 'Date Field' THEN 'D'
--                         ELSE 'V'
--                    END vcFieldDataType,
--                    0 boolAOIField,
--                    @numFormId AS numFormID,
--                    '' AS vcLookBackTableName
--            FROM    CFW_Fld_Master C LEFT JOIN CFW_Validation V ON V.numFieldID = C.Fld_id
--            WHERE   C.numDomainID = @numDomainId
--                    AND GRP_ID IN ( 2, 6 )
--                    AND Fld_id NOT IN (
--                    SELECT  numFormFieldId
--                    FROM    DynamicFormConfigurationDetails
--                    WHERE   numFormID = @numFormId
--                            AND vcFieldType = 'O'
--                            AND numAuthGroupID = @numAuthGroupId
--                            AND numDomainID = @numDomainId )
--            UNION
---- GRP_ID = 9 -> Item Attributes  Custom Field    , vcFieldType = I (indicate item attributes)
---- GRP_ID = 5 -> Item Details Custom Field, vcFieldType = P (Product)
--            SELECT  CONVERT(VARCHAR(15), Fld_id) + 'C' AS numFormFieldId,
--                    Fld_label AS vcFormFieldName,
--                    CASE Grp_id
--                      WHEN 9 THEN 'I'
--                      WHEN 5 THEN 'P'
--                    END AS vcFieldType,
--                    CASE WHEN fld_type = 'Text Box' THEN 'EditBox'
--                         WHEN fld_type = 'Drop Down List Box' THEN 'SelectBox'
--                         WHEN fld_type = 'Check box' THEN 'Check box'
--                         WHEN fld_type = 'Text Area' THEN 'EditBox'
--                         WHEN fld_type = 'Date Field' THEN 'Date Field'
--                    END vcAssociatedControlType,
--                    ISNULL(C.numListID, 0) numListID,
--                    Fld_label vcDbColumnName,
--                    CASE WHEN C.numListID > 0 THEN 'L'
--                         ELSE ''
--                    END vcListItemType,
--                    0 intColumnNum,
--                    0 intRowNum,
--                    ISNULL(V.bitIsRequired,0) as boolRequired,
--                    0 numAuthGroupID,
--                    CASE WHEN C.numListID > 0 THEN 'N'
--                         WHEN fld_type = 'Date Field' THEN 'D'
--                         ELSE 'V'
--                    END vcFieldDataType,
--                    0 boolAOIField,
--                    @numFormId AS numFormID,
--                    '' AS vcLookBackTableName
--            FROM    CFW_Fld_Master C LEFT JOIN CFW_Validation V ON V.numFieldID = C.Fld_id
--            WHERE   C.numDomainID = @numDomainId
--                    AND GRP_ID IN ( 5, 9 )
--                    AND Fld_id NOT IN (
--                    SELECT  numFormFieldId
--                    FROM    DynamicFormConfigurationDetails
--                    WHERE   numFormID = @numFormId
--                            AND vcFieldType = 'I'
--                            AND numAuthGroupID = @numAuthGroupId
--                            AND numDomainID = @numDomainId )
--        END
--
--    IF @numFormId = 6 
--        BEGIN
--             INSERT INTO #tempAddedFields (numFormFieldId,vcFormFieldName,vcFieldType,vcAssociatedControlType
--        ,numListID,vcDbColumnName,vcListItemType,intColumnNum,intRowNum,boolRequired,numAuthGroupID,
--          vcFieldDataType,boolAOIField,numFormID,vcLookBackTableName)
--          SELECT  CONVERT(VARCHAR(15), D.numFormFieldId) + D.vcFieldType AS numFormFieldId,
--                    ISNULL(DFV.vcNewFormFieldName,D.vcFormFieldName) AS vcFormFieldName,
--                    D.vcFieldType,
--                    D.vcAssociatedControlType,
--                    D.numListID,
--                    D.vcDbColumnName,
--                    vcListItemType,
--                    ISNULL(intColumnNum, 0) intColumnNum,
--                    ISNULL(intRowNum, 0) intRowNum,
--                    ISNULL(DFV.bitIsRequired,0) boolRequired,
--                    ISNULL(numAuthGroupID, 0) numAuthGroupID,
--                    CASE WHEN D.vcAssociatedControlType = 'DateField' THEN 'D'
--                         ELSE vcFieldDataType
--                    END vcFieldDataType,
--                    ISNULL(boolAOIField, 0) boolAOIField,
--                    D.numFormID,
--                    vcLookBackTableName
--            FROM    DynamicFormFieldMaster D
--                    JOIN DynamicFormConfigurationDetails DTL ON DTL.numFormFieldId = D.numFormFieldId
--                    LEFT JOIN DynamicFormField_Validation DFV ON DFV.numDomainID=DTL.numDomainID AND D.numFormFieldId=DFV.numFormFieldId AND D.numFormId=DFV.numFormId
--            WHERE   D.numFormID = @numFormId AND DTL.numFormID=D.numFormID
--                    AND numAuthGroupID = @numAuthGroupId
--                    AND DTL.numDomainID = @numDomainId
--                    AND DTL.vcFieldType = 'R'
--					AND D.bitDeleted=0
--            UNION
--           -- select fields whose value resides into CFW_FLD_Values_Cont and are added 
--            SELECT  CONVERT(VARCHAR(15), Fld_id) + 'C' AS numFormFieldId,
--                    Fld_label AS vcFormFieldName,
--                    'C' AS vcFieldType,
--                    CASE WHEN fld_type = 'Text Box' THEN 'EditBox'
--                         WHEN fld_type = 'Drop Down List Box' THEN 'SelectBox'
--                         WHEN fld_type = 'Check box' THEN 'Check box'
--                         WHEN fld_type = 'Text Area' THEN 'EditBox'
--                         WHEN fld_type = 'Date Field' THEN 'Date Field'
--                    END vcAssociatedControlType,
--                    ISNULL(C.numListID, 0) numListID,
--                    Fld_label vcDbColumnName,
--                    CASE WHEN C.numListID > 0 THEN 'L'
--                         ELSE ''
--                    END vcListItemType,
--                    ISNULL(intColumnNum, 0) intColumnNum,
--                    ISNULL(intRowNum, 0) intRowNum,
--                    ISNULL(V.bitIsRequired,0) as boolRequired,
--                    ISNULL(numAuthGroupID, 0) numAuthGroupID,
--                    CASE WHEN C.numListID > 0 THEN 'N'
--                         ELSE 'V'
--                    END vcFieldDataType,
--                    ISNULL(boolAOIField, 0),
--                    DTL.numFormID,
--                    '' AS vcLookBackTableName
--            FROM    CFW_Fld_Master C
--                    JOIN DynamicFormConfigurationDetails DTL ON DTL.numFormFieldId = C.Fld_id
--                                                                AND DTL.vcFieldType = 'C'
--                    LEFT JOIN CFW_Validation V ON V.numFieldID = C.Fld_id                                            
--            WHERE   DTL.numDomainID = @numDomainId
--                    AND C.numDomainID = @numDomainId
--                    AND numFormID = @numFormId
--                    AND numAuthGroupID = @numAuthGroupId
--                    AND GRP_ID = 4
--                    AND Fld_type IN ( 'Text Box', 'Drop Down List Box',
--                                      'Text Area' )
--            UNION
--			SELECT  CONVERT(VARCHAR(15), Fld_id) + 'C' AS numFormFieldId,
--                    Fld_label AS vcFormFieldName,
--                    'D' AS vcFieldType,
--                    CASE WHEN fld_type = 'Text Box' THEN 'EditBox'
--                         WHEN fld_type = 'Drop Down List Box' THEN 'SelectBox'
--                         WHEN fld_type = 'Check box' THEN 'Check box'
--                         WHEN fld_type = 'Text Area' THEN 'EditBox'
--                         WHEN fld_type = 'Date Field' THEN 'Date Field'
--                    END vcAssociatedControlType,
--                    ISNULL(C.numListID, 0) numListID,
--                    Fld_label vcDbColumnName,
--                    CASE WHEN C.numListID > 0 THEN 'L'
--                         ELSE ''
--                    END vcListItemType,
--                    ISNULL(intColumnNum, 0) intColumnNum,
--                    ISNULL(intRowNum, 0) intRowNum,
--                    ISNULL(V.bitIsRequired,0) as boolRequired,
--                    ISNULL(numAuthGroupID, 0) numAuthGroupID,
--                    CASE WHEN C.numListID > 0 THEN 'N'
--                         WHEN fld_type = 'Date Field' THEN 'D'
--                         ELSE 'V'
--                    END vcFieldDataType,
--                    ISNULL(boolAOIField, 0),
--                    DTL.numFormID,
--                    '' AS vcLookBackTableName
--            FROM    CFW_Fld_Master C
--                    JOIN DynamicFormConfigurationDetails DTL ON DTL.numFormFieldId = C.Fld_id
--                                                                AND DTL.vcFieldType = 'D'
--                    LEFT JOIN CFW_Validation V ON V.numFieldID = C.Fld_id                                            
--            WHERE   DTL.numDomainID = @numDomainId
--                    AND C.numDomainID = @numDomainId
--                    AND numFormID = @numFormId
--                    AND numAuthGroupID = @numAuthGroupId
--                    AND GRP_ID IN ( 1, 12, 13, 14 )
--                    AND Fld_type IN ( 'Text Box', 'Drop Down List Box',
--                                      'Text Area' )
--                                      
--                                      
--                                      
--         INSERT INTO #tempAvailableFields (numFormFieldId,vcFormFieldName,vcFieldType,vcAssociatedControlType
--        ,numListID,vcDbColumnName,vcListItemType,intColumnNum,intRowNum,boolRequired,numAuthGroupID,
--          vcFieldDataType,boolAOIField,numFormID,vcLookBackTableName)                         
--            SELECT  CONVERT(VARCHAR(15), D.numFormFieldId) + D.vcFieldType AS numFormFieldId,
--                    ISNULL(DFV.vcNewFormFieldName,D.vcFormFieldName) AS vcFormFieldName,
--                    D.vcFieldType,
--                    D.vcAssociatedControlType,
--                    D.numListID,
--                    D.vcDbColumnName,
--                    vcListItemType,
--                    0 intColumnNum,
--                    0 intRowNum,
--                    ISNULL(DFV.bitIsRequired,0) boolRequired,
--                    0 numAuthGroupID,
--                    vcFieldDataType,
--                    0 boolAOIField,
--                    D.numFormID,
--                    vcLookBackTableName
--            FROM    DynamicFormFieldMaster D
--                    LEFT JOIN DynamicFormField_Validation DFV ON DFV.numDomainID=@numDomainId AND D.numFormFieldId=DFV.numFormFieldId AND D.numFormId=DFV.numFormId
--            WHERE   D.numFormID = @numFormId
--                    AND ( D.numDomainID IS NULL
--                          OR D.numDomainID = @numDomainId
--                        )
--					AND D.bitDeleted=0
--                    AND D.numFormFieldId NOT IN (
--                    SELECT  numFormFieldId
--                    FROM    DynamicFormConfigurationDetails
--                    WHERE   numFormID = @numFormId
--                            AND numAuthGroupID = @numAuthGroupId
--                            AND numDomainID = @numDomainId
--                            AND vcFieldType = 'R' )
--            UNION
--      -- select fields whose value resides into CFW_FLD_Values_Cont and are not added 
--            SELECT  CONVERT(VARCHAR(15), Fld_id) + 'C' AS numFormFieldId,
--                    Fld_label AS vcFormFieldName,
--                    'C' AS vcFieldType,
--                    CASE WHEN fld_type = 'Text Box' THEN 'EditBox'
--                         WHEN fld_type = 'Drop Down List Box' THEN 'SelectBox'
--                         WHEN fld_type = 'Check box' THEN 'Check box'
--                         WHEN fld_type = 'Text Area' THEN 'EditBox'
--                         WHEN fld_type = 'Date Field' THEN 'Date Field'
--                    END vcAssociatedControlType,
--                    ISNULL(C.numListID, 0) numListID,
--                    Fld_label vcDbColumnName,
--                    CASE WHEN C.numListID > 0 THEN 'L'
--                         ELSE ''
--                    END vcListItemType,
--                    0 intColumnNum,
--                    0 intRowNum,
--                    ISNULL(V.bitIsRequired,0) as boolRequired,
--                    0 numAuthGroupID,
--                    CASE WHEN C.numListID > 0 THEN 'N'
--                         ELSE 'V'
--                    END vcFieldDataType,
--                    0 boolAOIField,
--                    @numFormId AS numFormID,
--                    '' AS vcLookBackTableName
--            FROM    CFW_Fld_Master C LEFT JOIN CFW_Validation V ON V.numFieldID = C.Fld_id
--            WHERE   C.numDomainID = @numDomainId
--                    AND GRP_ID = 4
--                    AND Fld_type IN ( 'Text Box', 'Drop Down List Box',
--                                      'Text Area' )
--                    AND Fld_id NOT IN (
--                    SELECT  numFormFieldId
--                    FROM    DynamicFormConfigurationDetails
--                    WHERE   numFormID = @numFormId
--                            AND vcFieldType = 'C'
--                            AND numAuthGroupID = @numAuthGroupId
--                            AND numDomainID = @numDomainId )
--            UNION
-- -- select custom field where vcFieldType ='D' -> select fields whose value resides into CFW_FLD_Values (Division)
--            SELECT  CONVERT(VARCHAR(15), Fld_id) + 'C' AS numFormFieldId,
--                    Fld_label AS vcFormFieldName,
--                    'D' AS vcFieldType,
--                    CASE WHEN fld_type = 'Text Box' THEN 'EditBox'
--                         WHEN fld_type = 'Drop Down List Box' THEN 'SelectBox'
--                         WHEN fld_type = 'Check box' THEN 'Check box'
--                         WHEN fld_type = 'Text Area' THEN 'EditBox'
--                         WHEN fld_type = 'Date Field' THEN 'Date Field'
--                    END vcAssociatedControlType,
--                    ISNULL(C.numListID, 0) numListID,
--                    Fld_label vcDbColumnName,
--                    CASE WHEN C.numListID > 0 THEN 'L'
--                         ELSE ''
--                    END vcListItemType,
--                    0 intColumnNum,
--                    0 intRowNum,
--                    ISNULL(V.bitIsRequired,0) as boolRequired,
--                    0 numAuthGroupID,
--                    CASE WHEN C.numListID > 0 THEN 'N'
--                         ELSE 'V'
--                    END vcFieldDataType,
--                    0 boolAOIField,
--                    @numFormId AS numFormID,
--                    '' AS vcLookBackTableName
--            FROM    CFW_Fld_Master C LEFT JOIN CFW_Validation V ON V.numFieldID = C.Fld_id
--            WHERE   C.numDomainID = @numDomainId
--                    AND GRP_ID IN ( 1, 12, 13, 14 )
--                    AND Fld_type IN ( 'Text Box', 'Drop Down List Box',
--                                      'Text Area' )
--                    AND Fld_id NOT IN (
--                    SELECT  numFormFieldId
--                    FROM    DynamicFormConfigurationDetails
--                    WHERE   numFormID = @numFormId
--                            AND vcFieldType = 'D'
--                            AND numAuthGroupID = @numAuthGroupId
--                            AND numDomainID = @numDomainId )
--        END
--
--
--		SELECT * FROM #tempAvailableFields WHERE numFormFieldId NOT IN (SELECT numFormFieldId FROM #tempAddedFields)
--		 UNION            
--        SELECT * FROM #tempAddedFields
--        
--        DROP TABLE #tempAvailableFields
--        DROP TABLE #tempAddedFields
--        
----if @numFormId = 6 -- Simple search, Include custom fields for Location 
----BEGIN
---- select convert(varchar(15),D.numFormFieldId)+D.vcFieldType as numFormFieldId,vcFormFieldName,D.vcFieldType,                              
---- D.vcAssociatedControlType,D.numListID,D.vcDbColumnName,                              
---- vcListItemType,0 intColumnNum,0 intRowNum,isnull(boolRequired,0) boolRequired, 0 numAuthGroupID,                              
---- vcFieldDataType,isnull(boolAOIField,0) boolAOIField,D.numFormID,vcLookBackTableName from DynamicFormFieldMaster D                              
---- left join DynamicFormConfigurationDetails DTL                              
---- on DTL.numFormFieldId=D.numFormFieldId where  D.numFormID=@numFormId and (DTL.vcFieldType<>'D' or DTL.vcFieldType<>'C' or DTL.vcFieldType is null)                           
---- and (D.numDomainID is null or D.numDomainID=@numDomainId) and  D.numFormFieldId    not in (select numFormFieldId from DynamicFormConfigurationDetails where numFormID=@numFormId                              
---- and numAuthGroupID=@numAuthGroupId and numDomainID=@numDomainId and vcFieldType='R')                           
---- union                              
---- select convert(varchar(15),D.numFormFieldId)+D.vcFieldType as numFormFieldId,vcFormFieldName,D.vcFieldType,                              
---- D.vcAssociatedControlType,D.numListID,D.vcDbColumnName,                              
---- vcListItemType,isnull(intColumnNum,0) intColumnNum,isnull(intRowNum,0) intRowNum,isnull(boolRequired,0) boolRequired, isnull(numAuthGroupID,0) numAuthGroupID,                              
----  CASE WHEN D.vcAssociatedControlType = 'DateField'THEN 'D' ELSE vcFieldDataType END vcFieldDataType,isnull(boolAOIField,0) boolAOIField,D.numFormID,vcLookBackTableName from DynamicFormFieldMaster D                              
---- join DynamicFormConfigurationDetails DTL                              
---- on DTL.numFormFieldId=D.numFormFieldId where  D.numFormID=@numFormId                              
---- and numAuthGroupID=@numAuthGroupId and DTL.numDomainID=@numDomainId and DTL.vcFieldType in ('R')
---- union   
---- 
---- /*
----  GRP_ID =1,4,12,13,14  -> Leads/Prospects/Accounts,Contact Details,Prospect,Account,Leads
----  get custom fields which are mapped to current AuthenticationGroup
---- 'D' as vcFieldType -> select fields whose value resides into CFW_FLD_Values (Division) */
---- select convert(varchar(15),Fld_id)+'C' as  numFormFieldId,Fld_label as vcFormFieldName,'D' as vcFieldType,                              
---- case when fld_type='Text Box' then 'EditBox' when  fld_type='Drop Down List Box' then 'SelectBox' when   fld_type='Check box' then 'Check box'                               
---- when  fld_type='Text Area' then 'EditBox' when fld_type='Date Field' then 'Date Field' end vcAssociatedControlType,isnull(C.numListID,0) numListID,Fld_label vcDbColumnName,                              
---- case when C.numListID>0 then 'L' else '' end  vcListItemType,0 intColumnNum,0 intRowNum,0 boolRequired, 0 numAuthGroupID,                              
---- case when C.numListID>0 then 'N' WHEN fld_type = 'Date Field'THEN 'D' else 'V' end vcFieldDataType,0 boolAOIField,@numFormId as numFormID,'' as vcLookBackTableName                              
---- from CFW_Fld_Master C 
---- WHERE C.numDomainID=@numDomainId and GRP_ID in(1,12,13,14) 
----	--Fld_id not in (select numFormFieldId from  DynamicFormConfigurationDetails where numFormID=@numFormId and vcFieldType='D' and numAuthGroupID=@numAuthGroupId and  numDomainID=@numDomainId )
----                               
---- union                              
---- 
------'C' as vcFieldType -> select fields whose value resides into CFW_FLD_Values_Cont
---- select convert(varchar(15),Fld_id)+'C' as  numFormFieldId,Fld_label as vcFormFieldName,'C' as vcFieldType,                              
---- case when fld_type='Text Box' then 'EditBox' when  fld_type='Drop Down List Box' then 'SelectBox' when   fld_type='Check box' then 'Check box'                               
---- when  fld_type='Text Area' then 'EditBox' when fld_type='Date Field' then 'Date Field' end vcAssociatedControlType,isnull(C.numListID,0) numListID,Fld_label vcDbColumnName,                              
---- case when C.numListID>0 then 'L' else '' end  vcListItemType,0 intColumnNum,0 intRowNum,0 boolRequired, 0 numAuthGroupID,                              
---- case when C.numListID>0 then 'N' WHEN fld_type = 'Date Field'THEN 'D' else 'V' end vcFieldDataType,0 boolAOIField,@numFormId as numFormID,'' as vcLookBackTableName                              
---- from 
---- CFW_Fld_Master C 
---- where  C.numDomainID=@numDomainId and GRP_ID =4 
----	--Fld_id not in (select numFormFieldId from  DynamicFormConfigurationDetails where numFormID=@numFormId and vcFieldType='C' and numAuthGroupID=@numAuthGroupId and  numDomainID=@numDomainId )
----END
GO
/****** Object:  StoredProcedure [dbo].[usp_getDynamicSearchFormFieldsForAGroup]    Script Date: 07/26/2008 16:17:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Modified By:Anoop Jayaraj       
   --Created By: Debasish Tapan Nag                                            
--Purpose: Retrieves the list of available and selected fields for a Group          
--Created Date: 08/12/2005                                           
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getdynamicsearchformfieldsforagroup')
DROP PROCEDURE usp_getdynamicsearchformfieldsforagroup
GO
CREATE PROCEDURE [dbo].[usp_getDynamicSearchFormFieldsForAGroup]       
	@numDomainID NUMERIC(18,0),
	@numUserCntID NUMERIC(18,0),
	@FormId NUMERIC(18,0),
	@vcDisplayColumns VARCHAR(MAX)
AS        
BEGIN        
	DECLARE @vcLocationID VARCHAR(100) = '0'

	IF @FormId = 15
	BEGIN
		SELECT @vcLocationID ='2,6'
	END
	ELSE
	BEGIN
		SELECT @vcLocationID =isnull(vcLocationID,'0') from dynamicFormMaster where numFormID=@FormId    
	END
      
	SELECT  
		numFieldID as numFormFieldID,
		vcFieldName as vcFormFieldName,
		0 AS bitCustom,
		CONCAT(numFieldID,'~0') AS vcFieldID
	FROM    
		View_DynamicDefaultColumns
	WHERE   
		numFormID = @FormId
		AND bitInResults = 1
		AND bitDeleted = 0 
		AND numDomainID = @numDomainID
	UNION 
	SELECT  
		c.Fld_id AS numFormFieldID
		,c.Fld_label AS vcFormFieldName
		,1 AS bitCustom,
		CONCAT(c.Fld_id,'~1') AS vcFieldID
	FROM    
		CFW_Fld_Master C 
	LEFT JOIN 
		CFW_Validation V ON V.numFieldID = C.Fld_id
	JOIN 
		CFW_Loc_Master L on C.GRP_ID=L.Loc_Id
	WHERE   
		C.numDomainID = @numDomainID
		AND GRP_ID IN(	SELECT ID FROM dbo.SplitIDs(@vcLocationID,','))
	ORDER BY 
		vcFormFieldName ASC 
   
	DECLARE @TEMPSelectedColumns TABLE
	(
		numFormFieldID NUMERIC(18,0)
		,vcFormFieldName VARCHAR(300)
		,vcDbColumnName VARCHAR(300)
		,tintOrder INT
		,vcFieldID VARCHAR(300)
	)

	-- IF USER HAS SELECTED ITS OWN DISPLAY COLUMNS THEN USE IT
	IF LEN(ISNULL(@vcDisplayColumns,'')) > 0
	BEGIN
		INSERT INTO @TEMPSelectedColumns
		(
			numFormFieldID
			,vcFormFieldName
			,vcDbColumnName
			,tintOrder
			,vcFieldID
		)
		SELECT
			numFormFieldID
			,vcFormFieldName
			,vcDbColumnName
			,0
			,vcFieldID
		FROM
		(
			SELECT  
				numFieldID as numFormFieldID,
				vcFieldName as vcFormFieldName,
				0 AS bitCustom,
				CONCAT(numFieldID,'~0') AS vcFieldID,
				vcDbColumnName
			FROM    
				View_DynamicDefaultColumns
			WHERE   
				numFormID = @FormId
				AND bitInResults = 1
				AND bitDeleted = 0 
				AND numDomainID = @numDomainID
			UNION 
			SELECT  
				c.Fld_id AS numFormFieldID
				,c.Fld_label AS vcFormFieldName
				,1 AS bitCustom,
				CONCAT(c.Fld_id,'~1') AS vcFieldID,
				Fld_label AS vcDbColumnName
			FROM    
				CFW_Fld_Master C 
			LEFT JOIN 
				CFW_Validation V ON V.numFieldID = C.Fld_id
			JOIN 
				CFW_Loc_Master L on C.GRP_ID=L.Loc_Id
			WHERE   
				C.numDomainID = @numDomainID
				AND GRP_ID IN(	SELECT ID FROM dbo.SplitIDs(@vcLocationID,','))
		) TEMP
		WHERE
			vcFieldID IN (SELECT OutParam FROM SplitString(@vcDisplayColumns,','))

	END

	-- IF USER SELECTED DISPLAY COLUMNS IS NOT AVAILABLE THAN GET COLUMNS CONFIGURED FROM SEARCH RESULT GRID
	IF (SELECT COUNT(*) FROM @TEMPSelectedColumns) = 0
	BEGIN
		INSERT INTO @TEMPSelectedColumns
		(
			numFormFieldID
			,vcFormFieldName
			,vcDbColumnName
			,tintOrder
			,vcFieldID
		)
		SELECT
			numFormFieldID
			,vcFormFieldName
			,vcDbColumnName
			,tintOrder
			,vcFieldID
		FROM
		(
			SELECT  A.numFormFieldID,
					vcFieldName as vcFormFieldName,
					vcDbColumnName,
					A.tintOrder,
					CONCAT(numFieldID,'~0') AS vcFieldID
			FROM    AdvSerViewConf A
					JOIN View_DynamicDefaultColumns D ON D.numFieldID = A.numFormFieldID
			WHERE   A.numFormID = @FormId
					AND D.bitInResults = 1
					AND D.bitDeleted = 0
					AND D.numDomainID = @numDomainID 
					AND D.numFormID = @FormId
					AND A.numDomainID = @numDomainID
					AND A.numUserCntID=@numUserCntID
					AND ISNULL(A.bitCustom,0) = 0
			UNION
			SELECT  A.numFormFieldID,
					c.Fld_label as vcFormFieldName,
					c.Fld_label,
					A.tintOrder,
					CONCAT(c.Fld_id,'~1') AS vcFieldID
			FROM    AdvSerViewConf A
					JOIN CFW_Fld_Master C ON C.Fld_id = A.numFormFieldID
			WHERE   A.numFormID = @FormId
					AND C.numDomainID = @numDomainID 
					AND A.numDomainID = @numDomainID
					AND A.numUserCntID=@numUserCntID
					AND ISNULL(A.bitCustom,0) = 1
		) T1
		ORDER BY
			 tintOrder
	END

	-- IF USER HASN'T CONFIGURED COLUMNS FROM SEARCH RESULT GRID THEN RETURN DEFAULT COLUMNS
	IF (SELECT COUNT(*) FROM @TEMPSelectedColumns) = 0
	BEGIN
		INSERT INTO @TEMPSelectedColumns
		(
			numFormFieldID
			,vcFormFieldName
			,vcDbColumnName
			,tintOrder
			,vcFieldID
		)
		SELECT  
			numFieldID,
			vcFieldName,
			vcDbColumnName,
			0,
			CONCAT(numFieldID,'~0')
		FROM    
			View_DynamicDefaultColumns
		WHERE   
			numFormID = @FormId
			AND bitInResults = 1
			AND bitDeleted = 0 
			AND numDomainID = @numDomainID
			AND numFieldID = (CASE @FormId WHEN 1 THEN 3 WHEN 15 THEN 96 WHEN 19 THEN 3 WHEN 29 THEN 189 WHEN 17 THEN 127 WHEN 18 THEN 148 WHEN 59 THEN 423 END)
	END

	SELECT * FROM @TEMPSelectedColumns
END
GO

/****** Object:  StoredProcedure [dbo].[usp_GetSalesFulfillmentOrder]    Script Date: 03/06/2009 00:39:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetSalesFulfillmentOrder')
DROP PROCEDURE USP_GetSalesFulfillmentOrder
GO
CREATE PROCEDURE [dbo].[USP_GetSalesFulfillmentOrder]
(
    @numDomainId AS NUMERIC(9) = 0,
    @SortCol VARCHAR(50),
    @SortDirection VARCHAR(4),
    @CurrentPage INT,
    @PageSize INT,
    @TotRecs INT OUTPUT,
    @numUserCntID NUMERIC,
    @vcBizDocStatus VARCHAR(500),
    @vcBizDocType varchar(500),
    @numDivisionID as numeric(9)=0,
    @ClientTimeZoneOffset INT=0,
    @bitIncludeHistory bit=0,
    @vcOrderStatus VARCHAR(500),
    @vcOrderSource VARCHAR(1000),
	@numOppID AS NUMERIC(18,0) = 0,
	@vcShippingService AS VARCHAR(MAX) = '',
	@numShippingZone NUMERIC(18,0)=0,
	@vcRegularSearchCriteria VARCHAR(MAX) = '',
	@vcCustomSearchCriteria VARCHAr(MAX) = ''
)
AS 
BEGIN

	DECLARE @firstRec AS INTEGER                                                              
	DECLARE @lastRec AS INTEGER                                                     
	SET @firstRec = ( @CurrentPage - 1 ) * @PageSize                             
	SET @lastRec = ( @CurrentPage * @PageSize + 1 )                                                                    

	IF LEN(ISNULL(@SortCol,''))=0 OR CHARINDEX('OBD.',@SortCol) > 0 OR @SortCol='dtCreatedDate'
		SET @SortCol = 'Opp.bintCreatedDate'
		
	IF LEN(ISNULL(@SortDirection,''))=0	
		SET @SortDirection = 'DESC'

	CREATE TABLE #tempForm 
	(
		ID INT IDENTITY(1,1), tintOrder TINYINT,vcDbColumnName nvarchar(50),vcOrigDbColumnName nvarchar(50),vcFieldName nvarchar(50),vcAssociatedControlType nvarchar(50),vcListItemType varchar(3),
		numListID numeric(9),vcLookBackTableName varchar(50),bitCustomField BIT,numFieldId NUMERIC,bitAllowSorting BIT,bitAllowEdit BIT,bitIsRequired BIT,bitIsEmail BIT,
		bitIsAlphaNumeric BIT,bitIsNumeric BIT,bitIsLengthValidation BIT,intMaxLength int,intMinLength int,bitFieldMessage BIT,vcFieldMessage VARCHAR(500),
		ListRelID numeric(9),intColumnWidth int,bitAllowFiltering BIT,vcFieldDataType CHAR(1)
	)

	-- FIRST CHECK IF GRID COLUMNS ARE CONFIGURED
	DECLARE  @Nocolumns  AS TINYINT = 0
	DECLARE @numFormId INT = 23

	SELECT 
		@Nocolumns=ISNULL(SUM(TotalRow),0) 
	FROM
	(            
		SELECT 
			COUNT(*) TotalRow 
		FROM 
			View_DynamicColumns 
		WHERE 
			numFormId=@numFormId 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1
		UNION 
		SELECT 
			COUNT(*) TotalRow 
		FROM 
			View_DynamicCustomColumns 
		WHERE 
			numFormId=@numFormId 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
	) TotalRows

	-- IF GRID COLUMNS ARE NOT CONFIGURES THEN CHECK IF MASTER FORM CONFIGURATION IS CREATED BY ADMINISTRATOR
	DECLARE @IsMasterConfAvailable BIT = 0
	DECLARE @numUserGroup NUMERIC(18,0)

	IF @Nocolumns=0
	BEGIN
		SELECT @numUserGroup = numGroupID FROM UserMaster WHERE numUserDetailId = @numUserCntID 

		IF (SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = @numDomainID AND numFormID = @numFormId AND bitGridConfiguration = 1 AND numGroupID = @numUserGroup) > 0
		BEGIN
			SET @IsMasterConfAvailable = 1
		END
	END

	--IF MASTERCONFIGURATION IS AVAILABLE THEN LOAD IT OTHERWISE LOAD DEFAULT COLUMNS
	IF @IsMasterConfAvailable = 1
	BEGIN
		INSERT INTO DycFormConfigurationDetails 
		(
			numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth
		)
		SELECT 
			@numFormId,numFieldId,intColumnNum,intRowNum,@numDomainID,@numUserCntID,0,1,0,intColumnWidth
		FROM 
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId=@numFormId AND 
			View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
			ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
		UNION
		SELECT 
			@numFormId,numFieldId,intColumnNum,intRowNum,@numDomainID,@numUserCntID,0,1,1,intColumnWidth
		FROM 
			View_DynamicCustomColumnsMasterConfig
		WHERE 
			View_DynamicCustomColumnsMasterConfig.numFormId=@numFormId AND 
			View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1


		INSERT INTO 
			#tempForm
		SELECT 
			(intRowNum + 1) as tintOrder, vcDbColumnName,	vcOrigDbColumnName,	ISNULL(vcCultureFieldName,vcFieldName), vcAssociatedControlType,
			vcListItemType, numListID, vcLookBackTableName,	bitCustom, numFieldId, bitAllowSorting,	bitInlineEdit, bitIsRequired,
			bitIsEmail,	bitIsAlphaNumeric, bitIsNumeric, bitIsLengthValidation,	intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,
			ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType 
		FROM 
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId=@numFormId AND 
			View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
			ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
		UNION
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName, vcAssociatedControlType,'' as vcListItemType,numListID,'',
			bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,''
		FROM 
			View_DynamicCustomColumnsMasterConfig
		WHERE 
			View_DynamicCustomColumnsMasterConfig.numFormId=@numFormId AND 
			View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1
		ORDER BY 
			tintOrder ASC  
	END
	ELSE                                        
	BEGIN
		IF @Nocolumns=0
		BEGIN
			INSERT INTO DycFormConfigurationDetails 
			(
				numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth
			)
			SELECT 
				@numFormId,numFieldId,0,Row_number() over(order by tintRow desc),@numDomainID,@numUserCntID,0,1,0,intColumnWidth
			FROM 
				View_DynamicDefaultColumns
			WHERE 
				numFormId=@numFormId 
				AND bitDefault=1 
				AND ISNULL(bitSettingField,0)=1 
				AND numDomainID=@numDomainID
			ORDER BY 
				tintOrder asc   
		END

		INSERT INTO 
			#tempForm
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcOrigDbColumnName,ISNULL(vcCultureFieldName,vcFieldName),                  
			vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName,bitCustom,numFieldId,bitAllowSorting,bitInlineEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength,bitFieldMessage,
			vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType  
		FROM 
			View_DynamicColumns 
		WHERE 
			numFormId=@numFormId 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND ISNULL(bitSettingField,0)=1 
			AND ISNULL(bitCustom,0)=0  
		UNION
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName,vcAssociatedControlType,'' as vcListItemType,numListID,'',bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,'' 
		FROM 
			View_DynamicCustomColumns
		WHERE 
			numFormId=@numFormId 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND ISNULL(bitCustom,0)=1
		ORDER BY 
			tintOrder asc  
	END

	DECLARE @strColumns VARCHAR(MAX) = ''
    DECLARE @FROM NVARCHAR(MAX) = ''
    DECLARE @WHERE NVARCHAR(MAX) = ''

	SET @FROM =' FROM 
						OpportunityMaster Opp
					INNER JOIN 
						DivisionMaster DM 
					ON 
						Opp.numDivisionId = DM.numDivisionID
					INNER JOIN 
						CompanyInfo cmp 
					ON 
						cmp.numCompanyID=DM.numCompanyID
					LEFT JOIN
						AdditionalContactsInformation ADC
					ON
						Opp.numContactID=ADC.numContactID
					LEFT JOIN
						DivisionMasterShippingConfiguration DMSC
					ON
						DM.numDivisionID=DMSC.numDivisionID'
					
	SET @WHERE = CONCAT(' WHERE 
							(Opp.numOppID = ',@numOppID,' OR ',@numOppID,' = 0) 
							AND Opp.tintopptype = 1 
							AND Opp.tintOppStatus=1 
							AND (CHARINDEX(''Ready to Ship'', dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID)) > 0 OR CHARINDEX(''Not Applicable'', dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID)) > 0)
							AND Opp.numDomainID = ',@numDomainId)

	IF @SortCol like 'CFW.Cust%'             
	BEGIN            
		SET @FROM = @FROM + ' LEFT JOIN CFW_Fld_Values_Opp CFW on CFW.RecId=Opp.numOppid and CFW.fld_id= '+ REPLACE(@SortCol,'CFW.Cust','') +' '                                                     
		SET @SortCol='CFW.Fld_Value'            
	END 
      
    DECLARE @strSql NVARCHAR(MAX)
    DECLARE @SELECT NVARCHAR(MAX) = 'WITH bizdocs AS 
									(SELECT
										Opp.numOppID
										,Opp.numRecOwner
										,Opp.vcpOppName
										,DM.tintCRMType
										,ISNULL(DM.numTerID,0) numTerID
										,DM.numDivisionID
										,ISNULL(Opp.intUsedShippingCompany,ISNULL(DM.intShippingCompany,90)) AS numShipVia
										,Opp.monDealAmount
										,(CASE WHEN (SELECT COUNT(*) FROM SalesFulfillmentQueue WHERE numDomainID = Opp.numDomainId AND numOppId=Opp.numOppId AND ISNULL(bitExecuted,0)=0) > 0 THEN 1 ELSE 0 END) as bitPendingExecution
										,ISNULL(Opp.monDealAmount,0) AS monDealAmount1'

	IF CHARINDEX('OBD.monAmountPaid',@vcRegularSearchCriteria) > 0
	BEGIN
		IF CHARINDEX('OBD.monAmountPaid like ''%0%''',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'OBD.monAmountPaid like ''%0%''',' 1 = (SELECT ISNULL((SELECT SUM(ISNULL(monAmountPaid,0)) FROM  OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)) ')
		IF CHARINDEX('OBD.monAmountPaid like ''%1%''',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'OBD.monAmountPaid like ''%1%''','  1 = (SELECT (CASE WHEN (SELECT (CASE WHEN (SUM(ISNULL(monDealAmount,0)) = SUM(ISNULL(monAmountPaid,0)) AND SUM(ISNULL(monAmountPaid,0)) <> 0) THEN 1 ELSE 0 END) FROM OpportunityBizDocs BD WHERE BD.numOppId =  Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0) = 1) > 0 THEN 1 ELSE 0 END))  ')
		IF CHARINDEX('OBD.monAmountPaid like ''%2%''',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'OBD.monAmountPaid like ''%2%''',' 1 = (SELECT CASE WHEN (SELECT (CASE WHEN ((SUM(ISNULL(monDealAmount,0)) - SUM(ISNULL(monAmountPaid,0))) > 0 AND SUM(ISNULL(monAmountPaid,0)) <> 0) THEN 1 ELSE 0 END) FROM OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0) = 1) > 0 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('OBD.monAmountPaid like ''%3%''',@vcRegularSearchCriteria) > 0								   
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'OBD.monAmountPaid like ''%3%''',' 1 = (SELECT CASE WHEN (SELECT (CASE WHEN (SUM(ISNULL(monAmountPaid,0)) = 0) THEN 1 ELSE 0 END) FROM OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0) = 1) > 0 THEN 1 ELSE 0 END) ')
	END
	
	IF CHARINDEX('OI.vcInventoryStatus',@vcRegularSearchCriteria) > 0
	BEGIN
		IF CHARINDEX('OI.vcInventoryStatus=1',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'OI.vcInventoryStatus=1',' CHARINDEX(''Not Applicable'', dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID)) > 0 ')
		IF CHARINDEX('OI.vcInventoryStatus=2',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'OI.vcInventoryStatus=2',' CHARINDEX(''Allocation Cleared'', dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID)) > 0 ')
		IF CHARINDEX('OI.vcInventoryStatus=3',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'OI.vcInventoryStatus=3',' CHARINDEX(''Back Order'', dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID)) > 0 ')
		IF CHARINDEX('OI.vcInventoryStatus=4',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'OI.vcInventoryStatus=4',' CHARINDEX(''Ready to Ship'', dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID)) > 0 ')
	END

	IF CHARINDEX('dbo.fn_getOPPAddressState(Opp.numOppId,Opp.numDomainID,2)',@vcRegularSearchCriteria) > 0
	BEGIN
		SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'dbo.fn_getOPPAddressState(Opp.numOppId,Opp.numDomainID,2)','(CASE 
																																		WHEN Opp.tintShipToType IS NULL OR Opp.tintShipToType = 1
																																		THEN
																																			(SELECT  
																																				ISNULL(dbo.fn_GetState(AD.numState),'''')
																																			FROM 
																																				AddressDetails AD 
																																			WHERE 
																																				AD.numDomainID=Opp.numDomainID AND AD.numRecordID=Opp.numDivisionID 
																																				AND AD.tintAddressOf = 2 AND AD.tintAddressType = 2 AND AD.bitIsPrimary=1)
																																		WHEN Opp.tintShipToType = 0
																																		THEN
																																			(SELECT 
																																				ISNULL(dbo.fn_GetState(AD.numState),'''')
																																			FROM 
																																				CompanyInfo [Com1] 
																																			JOIN 
																																				DivisionMaster div1 
																																			ON 
																																				com1.numCompanyID = div1.numCompanyID
																																			JOIN Domain D1 ON D1.numDivisionID = div1.numDivisionID
																																			JOIN dbo.AddressDetails AD ON AD.numDomainID = div1.numDomainID AND AD.numRecordID = div1.numDivisionID AND tintAddressOf = 2 AND tintAddressType = 2 AND bitIsPrimary=1
																																			WHERE  D1.numDomainID = Opp.numDomainID)
																																		WHEN Opp.tintShipToType = 2 OR Opp.tintShipToType = 3
																																		THEN
																																			(SELECT
																																				ISNULL(dbo.fn_GetState(numShipState),'''')
																																			FROM 
																																				OpportunityAddress 
																																			WHERE 
																																				numOppID = Opp.numOppId)
																																		ELSE ''''
																																	END)')
	END
	

	IF @vcRegularSearchCriteria<>'' 
	BEGIN
		SET @WHERE = @WHERE + ' AND ' + @vcRegularSearchCriteria 
	END

	IF @vcCustomSearchCriteria<>'' 
	BEGIN
		SET @WHERE = @WHERE +' AND ' +  @vcCustomSearchCriteria
	END

	DECLARE @tintOrder  AS TINYINT;SET @tintOrder = 0
	DECLARE @vcFieldName  AS VARCHAR(50)
	DECLARE @vcListItemType  AS VARCHAR(3)
	DECLARE @vcListItemType1  AS VARCHAR(1)
	DECLARE @vcAssociatedControlType VARCHAR(30)
	DECLARE @numListID  AS NUMERIC(9)
	DECLARE @vcDbColumnName VARCHAR(40)
	DECLARE @vcLookBackTableName VARCHAR(2000)
	DECLARE @bitCustom  AS BIT
	Declare @numFieldId as numeric  
	DECLARE @bitAllowSorting AS CHAR(1)   
	DECLARE @bitAllowEdit AS CHAR(1)                 
	DECLARE @vcColumnName AS VARCHAR(500)
	DECLARE @ListRelID AS NUMERIC(9) 

	DECLARE @i AS INT = 1
	DECLARE @iCount AS INT 
	SELECT @iCount = COUNT(*) FROM #tempForm

	WHILE @i <= @iCount
	BEGIN
		SELECT 
			@tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName,@vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,
			@numListID=numListID,@vcLookBackTableName=vcLookBackTableName ,@bitCustom=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,@bitAllowEdit=bitAllowEdit,
			@ListRelID=ListRelID
		FROM 
			#tempForm 
		WHERE
			ID=@i

		IF @bitCustom = 0
		BEGIN
			DECLARE  @Prefix  AS VARCHAR(5)

			IF @vcLookBackTableName = 'AdditionalContactsInformation'
				SET @Prefix = 'ADC.'
			IF @vcLookBackTableName = 'DivisionMaster'
				SET @Prefix = 'DM.'
			IF @vcLookBackTableName = 'OpportunityMaster'
				SET @PreFix = 'Opp.'
			IF @vcLookBackTableName = 'CompanyInfo'
				SET @PreFix = 'cmp.'
			IF @vcLookBackTableName = 'DivisionMasterShippingConfiguration'
				SET @PreFix = 'DMSC.'

			SET @vcColumnName= @vcDbColumnName+'~'+ convert(varchar(10),@numFieldId) +'~0'

			IF @vcAssociatedControlType = 'SelectBox'
			BEGIN
				IF @vcDbColumnName = 'numPartner'
				BEGIN
					SET @strColumns=@strColumns+ ' ,(SELECT D.vcPartnerCode+''-''+C.vcCompanyName FROM DivisionMaster AS D LEFT JOIN CompanyInfo AS C ON D.numCompanyID=C.numCompanyID WHERE D.numDivisionID=Opp.numPartner) '+' ['+ @vcColumnName+']'    
					SET @strColumns=@strColumns+ ' ,(SELECT D.vcPartnerCode+''-''+C.vcCompanyName FROM DivisionMaster AS D LEFT JOIN CompanyInfo AS C ON D.numCompanyID=C.numCompanyID WHERE D.numDivisionID=Opp.numPartner) AS vcPartner' 
				END
				ELSE IF @vcDbColumnName = 'tintSource'
				BEGIN
					SET @strColumns = @strColumns + ',ISNULL(dbo.fn_GetOpportunitySourceValue(ISNULL(Opp.tintSource,0),ISNULL(Opp.tintSourceType,0),Opp.numDomainID),''Internal Order'')' + ' [' + @vcColumnName + ']'
				END
				ELSE IF @vcDbColumnName = 'numContactID'
				BEGIN
					SET @strColumns = @strColumns + ',dbo.fn_GetContactName(' + @Prefix + @vcDbColumnName + ') [' + @vcColumnName + ']'
				END
				ELSE IF @vcListItemType = 'LI'
				BEGIN
					SET @strColumns = @strColumns + ',L' + CONVERT(VARCHAR(3),@tintOrder) + '.vcData' + ' [' + @vcColumnName + ']'
					SET @FROM = @FROM + ' LEFT JOIN ListDetails L' + CONVERT(VARCHAR(3),@tintOrder) + ' on L' + CONVERT(VARCHAR(3),@tintOrder) + '.numListItemID=' + @vcDbColumnName
				END
				ELSE IF @vcListItemType = 'U'
				BEGIN
					SET @strColumns = @strColumns + ',dbo.fn_GetContactName(' + @Prefix + @vcDbColumnName + ') [' + @vcColumnName + ']'
				END
				ELSE IF @vcListItemType = 'SGT'
				BEGIN
					SET @strColumns = @strColumns + ',(CASE ' + @Prefix + @vcDbColumnName + ' 
														WHEN 0 THEN ''Service Default''
														WHEN 1 THEN ''Adult Signature Required''
														WHEN 2 THEN ''Direct Signature''
														WHEN 3 THEN ''InDirect Signature''
														WHEN 4 THEN ''No Signature Required''
														ELSE ''''
														END) [' + @vcColumnName + ']'
				END
				ELSE IF @vcListItemType = 'PSS'
				BEGIN
					SET @strColumns = @strColumns + ',ISNULL((SELECT vcShipmentService FROM ShippingService WHERE (numDomainID=Opp.numDomainID OR ISNULL(numDomainID,0)=0) AND numShippingServiceID = ' + @Prefix + @vcDbColumnName + '),'''') [' + @vcColumnName + ']'
				END
            END
			ELSE IF @vcAssociatedControlType = 'DateField'
			BEGIN
				IF @vcDbColumnName = 'dtReleaseDate'
				BEGIN
					SET @strColumns = @strColumns
                                + ',case when convert(varchar(11),'
                                + @Prefix
                                + @vcDbColumnName
                                + ')= convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',getdate())) then ''<b><font color=red>Today</font></b>'''
					SET @strColumns = @strColumns
									+ 'when convert(varchar(11),'
									+ @Prefix
									+ @vcDbColumnName
									+ ')= convert(varchar(11),dateadd(day,-1,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',getdate()))) then ''<b><font color=purple>Yesterday</font></b>'''
					SET @strColumns = @strColumns
									+ 'when convert(varchar(11),'
									+ @Prefix
									+ @vcDbColumnName
									+ ')= convert(varchar(11),dateadd(day,1,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',getdate()))) then''<b><font color=orange>Tommorow</font></b>'' '
					SET @strColumns = @strColumns
									+ 'else dbo.FormatedDateFromDate('
									+ @Prefix
									+ @vcDbColumnName
									+ ','
									+ CONVERT(VARCHAR(10),@numDomainId)
									+ ') end  ['
									+ @vcColumnName + ']'
				END
				ELSE
				BEGIN
					SET @strColumns = @strColumns
                                + ',case when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+','
                                + @Prefix
                                + @vcDbColumnName
                                + ') )= convert(varchar(11),getdate()) then ''<b><font color=red>Today</font></b>'''
					SET @strColumns = @strColumns
									+ 'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+','
									+ @Prefix
									+ @vcDbColumnName
									+ ') )= convert(varchar(11),dateadd(day,-1,getdate())) then ''<b><font color=purple>Yesterday</font></b>'''
					SET @strColumns = @strColumns
									+ 'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+','
									+ @Prefix
									+ @vcDbColumnName
									+ ') )= convert(varchar(11),dateadd(day,1,getdate())) then''<b><font color=orange>Tommorow</font></b>'' '
					SET @strColumns = @strColumns
									+ 'else dbo.FormatedDateFromDate('
									+ @Prefix
									+ @vcDbColumnName
									+ ','
									+ CONVERT(VARCHAR(10),@numDomainId)
									+ ') end  ['
									+ @vcColumnName + ']'
				 END	
            END
            ELSE IF @vcAssociatedControlType = 'TextBox' OR @vcAssociatedControlType = 'TextArea'
            BEGIN
				IF @vcDbColumnName = 'vcCompanyName'
				BEGIN
					SET @strColumns = @strColumns + ',' + @Prefix + @vcDbColumnName + ' [' + @vcColumnName + ']'
					SET @strColumns = @strColumns + ',dbo.fn_GetListItemName(Opp.[numstatus]) vcOrderStatus'
				END
				ELSE IF @vcDbColumnName = 'numShipState'
				BEGIN
					SET @strColumns = @strColumns + ',dbo.fn_getOPPAddressState(Opp.numOppId,Opp.numDomainID,2)' + ' [' + @vcColumnName + ']'
				END
				ELSE
				BEGIN
					SET @strColumns = @strColumns + ',' + @Prefix + @vcDbColumnName + ' [' + @vcColumnName + ']'
				END
            END
            ELSE IF  @vcAssociatedControlType='Label'                                              
			BEGIN
				SET @strColumns=@strColumns + ',' + CASE   
				WHEN @vcDbColumnName = 'vcFulfillmentStatus' THEN 'ISNULL(STUFF((SELECT CONCAT(''<br />'',vcMessage) FROM SalesFulfillmentLog WHERE numDomainID = Opp.numDomainId AND numOppID=Opp.numOppId AND ISNULL(bitSuccess,0) = 1 FOR XML PATH('''')) ,1,12,''''),'''')'                 
				WHEN @vcDbColumnName = 'vcPricedBoxedTracked' THEN 'STUFF((SELECT CONCAT('', '',OpportunityBizDocs.numOppID,''~'',numOppBizDocsId,''~'',ISNULL(numShippingReportId,0),''~'',(CASE WHEN (SELECT COUNT(*) FROM ShippingBox WHERE numShippingReportId=ShippingReport.numShippingReportId AND LEN(ISNULL(vcShippingLabelImage,'''')) > 0) > 0 THEN 1 ELSE 0 END)) vcText FROM OpportunityBizDocs LEFT JOIN ShippingReport ON ShippingReport.numOppBizDocId=OpportunityBizDocs.numOppBizDocsId WHERE OpportunityBizDocs.numBizDocId IN (SELECT ISNULL(numDefaultSalesShippingDoc,0) FROM Domain WHERE numDomainID=Opp.numDomainID) AND OpportunityBizDocs.numOppId=Opp.numOppID AND OpportunityBizDocs.bitShippingGenerated=1 FOR XML PATH(''''), TYPE).value(''.'',''NVARCHAR(MAX)''),1,2,'' '')'
				WHEN @vcDbColumnName = 'CalAmount' THEN 'Opp.monDealAmount'--'[dbo].[getdealamount](Opp.numOppId,Getutcdate(),0)'
				WHEN @vcDbColumnName = 'vcInventoryStatus' THEN 'ISNULL(dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID),'''')'
				WHEN @vcDbColumnName = 'vcOrderedShipped' THEN 'dbo.GetOrderShipReceiveStatus(Opp.numOppId,1,Opp.tintshipped)'
				WHEN @vcDbColumnName = 'vcOrderedReceived' THEN 'dbo.GetOrderShipReceiveStatus(Opp.numOppId,2,Opp.tintshipped)'
				WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'monDealAmount' THEN 'ISNULL((SELECT SUM(ISNULL(monDealAmount,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)'
				WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'monAmountPaid' THEN 'ISNULL((SELECT SUM(ISNULL(monAmountPaid,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)'
				ELSE @Prefix + @vcDbColumnName END +' ['+ @vcColumnName+']'        
			END
        END
		ELSE
        BEGIN
			SET @vcColumnName= @vcDbColumnName +'~'+ convert(varchar(10),@numFieldId)+'~1'

			IF @vcAssociatedControlType = 'TextBox' or @vcAssociatedControlType = 'TextArea'
            BEGIN
              SET @strColumns = @strColumns
                              + ',CFW'
                              + CONVERT(VARCHAR(3),@tintOrder)
                              + '.Fld_Value  ['
                              + @vcColumnName + ']'
              SET @FROM = @FROM
                                      + ' left Join CFW_FLD_Values_Opp CFW'
                                      + CONVERT(VARCHAR(3),@tintOrder)
                                      + ' on CFW'
                                      + CONVERT(VARCHAR(3),@tintOrder)
                                      + '.Fld_Id='
                                      + CONVERT(VARCHAR(10),@numFieldId)
                                      + 'and CFW'
                                      + CONVERT(VARCHAR(3),@tintOrder)
                                      + '.RecId=Opp.numOppid   '
			END
			ELSE
            IF @vcAssociatedControlType = 'CheckBox' 
            BEGIN
                SET @strColumns = @strColumns
                                + ',case when isnull(CFW'
                                + CONVERT(VARCHAR(3),@tintOrder)
                                + '.Fld_Value,0)=0 then 0 when isnull(CFW'
                                + CONVERT(VARCHAR(3),@tintOrder)
                                + '.Fld_Value,0)=1 then 1 end   ['
                                + @vcColumnName + ']'
                SET @FROM = @FROM
                                        + ' left Join CFW_FLD_Values_Opp CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + ' on CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.Fld_Id='
                                        + CONVERT(VARCHAR(10),@numFieldId)
                                        + 'and CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.RecId=Opp.numOppid   '
            END
            ELSE
            IF @vcAssociatedControlType = 'DateField'
            BEGIN
                  SET @strColumns = @strColumns
                                  + ',dbo.FormatedDateFromDate(CFW'
                                  + CONVERT(VARCHAR(3),@tintOrder)
                                  + '.Fld_Value,'
                                  + CONVERT(VARCHAR(10),@numDomainId)
                                  + ')  ['
                                  + @vcColumnName + ']'
                  SET @FROM = @FROM
                                          + ' left Join CFW_FLD_Values_Opp CFW'
                                          + CONVERT(VARCHAR(3),@tintOrder)
                                          + ' on CFW'
                                          + CONVERT(VARCHAR(3),@tintOrder)
                                          + '.Fld_Id='
                                          + CONVERT(VARCHAR(10),@numFieldId)
                                          + 'and CFW'
                                          + CONVERT(VARCHAR(3),@tintOrder)
                                          + '.RecId=Opp.numOppid    '
            END
            ELSE
            IF @vcAssociatedControlType = 'SelectBox'
            BEGIN
                SET @vcDbColumnName = 'DCust'
                                            + CONVERT(VARCHAR(10),@numFieldId)
                SET @strColumns = @strColumns
                                + ',L'
                                + CONVERT(VARCHAR(3),@tintOrder)
                                + '.vcData'
                                + ' ['
                                + @vcColumnName + ']'

                SET @FROM = @FROM
                                        + ' left Join CFW_FLD_Values_Opp CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + ' on CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.Fld_Id='
                                        + CONVERT(VARCHAR(10),@numFieldId)
                                        + 'and CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.RecId=Opp.numOppid '

                SET @FROM = @FROM
                                        + ' left Join ListDetails L'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + ' on L'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.numListItemID=CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.Fld_Value'
            END
			ELSE
			BEGIN
				SET @strColumns = @strColumns + CONCAT(',dbo.GetCustFldValueOpp(',@numFieldId,',opp.numOppID)') + ' [' + @vcColumnName + ']'
			END
		END

		SET @i = @i + 1
	END

		
		DECLARE @numShippingServiceItemID AS NUMERIC
		SELECT @numShippingServiceItemID=ISNULL(numShippingServiceItemID,0) FROM domain WHERE numDomainID=@numDomainID
		
		IF  LEN(ISNULL(@vcOrderStatus,''))>0
		BEGIN
			SET @WHERE = @WHERE + ' and isnull(Opp.numStatus,0) in (SELECT Items FROM dbo.Split(''' +@vcOrderStatus + ''','',''))' 			
		END

		IF LEN(ISNULL(@vcShippingService,''))>0
		BEGIN
			SET @WHERE = @WHERE + ' and isnull(Opp.numShippingService,0) in (SELECT Items FROM dbo.Split(''' +@vcShippingService + ''','',''))'
		END

		IF ISNULL(@numShippingZone,0) > 0
		BEGIN
			SET @WHERE = @WHERE + ' AND 1 = (CASE 
												WHEN ISNULL((SELECT TOP 1 numState FROM fn_getOPPAddressDetails(Opp.numOppId,Opp.numDomainID,2)),0) > 0 
												THEN
													CASE 
														WHEN (SELECT COUNT(*) FROM dbo.State WHERE numDomainID=' + CAST(@numDomainId AS VARCHAR) + ' AND numStateID =  (SELECT TOP 1 numState FROM fn_getOPPAddressDetails(Opp.numOppId,Opp.numDomainID,2)) AND numShippingZone=' + CAST(@numShippingZone as VARCHAR) + ' ) > 0 
														THEN 1
														ELSE 0
													END
												ELSE 0 
											END)'
		END
		
		IF LEN(ISNULL(@vcOrderSource,''))>0
		BEGIN
				SET @WHERE = @WHERE + ' and (' + @vcOrderSource +')' 			
		END
		
		SET @WHERE = @WHERE + ' AND (Opp.numDivisionID ='+ CONVERT(VARCHAR(15), @numDivisionID) + ' OR '+ CONVERT(VARCHAR(15), @numDivisionID) + ' = 0) '
		
        SET @strSql = CONCAT(@SELECT,@strColumns,@FROM,@WHERE,' ORDER BY ',@SortCol,' ',@SortDirection,' OFFSET ',((@CurrentPage - 1) * @PageSize),' ROWS FETCH NEXT ',@PageSize, ' ROWS ONLY) SELECT * INTO #temp FROM [bizdocs];')
        
		SET @strSql = @strSql + CONCAT('SELECT 
											OM.*
											,CAST((Case WHEN ISNULL(OI.numoppitemtCode,0)>0 then 1 else 0 end) as bit) as IsShippingServiceItemID
										FROM #temp OM 
											LEFT JOIN (SELECT numOppId,numoppitemtCode,ISNULL(vcItemDesc, '''') vcItemDesc,monTotAmount,
											Row_number() OVER(PARTITION BY numOppId ORDER BY numoppitemtCode) AS row 
											FROM OpportunityItems OI WHERE OI.numOppId in (SELECT numOppId FROM #temp) AND numItemCode=',@numShippingServiceItemID,') 
											OI ON OI.row=1 AND OM.numOppId = OI.numOppId; 
											
											SELECT 
											opp.numOppId
											,Opp.vcitemname AS vcItemName
											,ISNULL((SELECT TOP 1 dbo.FormatedDateFromDate(dtReleaseDate,',@numDomainId,') FROM OpportunityItemsReleaseDates WHERE numDomainID=',@numDomainId,' AND numOppID=Opp.numOppId AND numOppItemID=Opp.numoppitemtCode),'''')  AS vcItemReleaseDate
											,ISNULL((SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numDomainId=',@numDomainId,' AND ItemImages.numItemCode=i.numItemCode AND bitDefault=1 AND bitIsImage=1),'''') AS vcImage
											,CASE 
												WHEN charitemType = ''P'' THEN ''Product''
												WHEN charitemType = ''S'' THEN ''Service''
											END AS charitemType
											,CASE WHEN charitemType = ''P'' THEN ''Inventory''
												WHEN charitemType = ''N'' THEN ''Non-Inventory''
												WHEN charitemType = ''S'' THEN ''Service''
											END AS vcItemType
											,CASE WHEN ISNULL(numItemGroup,0) > 0 THEN WI.vcWHSKU ELSE i.vcSKU END vcSKU
											,dbo.USP_GetAttributes(opp.numWarehouseItmsID,bitSerialized) AS vcAttributes
											,ISNULL(W.vcWareHouse, '''') AS Warehouse,WL.vcLocation,opp.numUnitHour AS numUnitHourOrig
											,Opp.bitDropShip AS DropShip
											,SUBSTRING((SELECT  
															'' ,'' + vcSerialNo
																				+ CASE WHEN ISNULL(I.bitLotNo, 0) = 1
																					   THEN ''(''
																							+ CONVERT(VARCHAR(15), oppI.numQty)
																							+ '')''
																					   ELSE ''''
																				  END
														FROM    OppWarehouseSerializedItem oppI
																JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID = whi.numWareHouseItmsDTLID
														WHERE   oppI.numOppID = Opp.numOppId
																AND oppI.numOppItemID = Opp.numoppitemtCode
														ORDER BY vcSerialNo
														FOR
														XML PATH('''')
														), 3, 200000) AS SerialLotNo
											,ISNULL(bitSerialized,0) AS bitSerialized
											,ISNULL(bitLotNo,0) AS bitLotNo
											,Opp.numWarehouseItmsID
											,opp.numoppitemtCode
										FROM 
											OpportunityItems opp 
										JOIN 
											#temp OM 
										ON 
											opp.numoppid=OM.numoppid 
										LEFT JOIN 
											Item i 
										ON 
											opp.numItemCode = i.numItemCode
										INNER JOIN 
											dbo.WareHouseItems WI 
										ON 
											opp.numWarehouseItmsID = WI.numWareHouseItemID
											AND WI.numDomainID = ',@numDomainId,'
										INNER JOIN 
											dbo.Warehouses W 
										ON 
											WI.numDomainID = W.numDomainID
											AND WI.numWareHouseID = W.numWareHouseID
										LEFT JOIN 
											dbo.WarehouseLocation WL 
											ON WL.numWLocationID= WI.numWLocationID;
			
										DROP TABLE #temp;')

        PRINT CAST(@strSql AS NTEXT);
        EXEC ( @strSql ) ;
       
      
        
        /*Get total count */
        SET @strSql =  ' (SELECT @TotRecs =COUNT(*) ' + @FROM + @WHERE + ')';
        
        PRINT @strSql ;
        EXEC sp_executesql 
        @query = @strSql, 
        @params = N'@TotRecs INT OUTPUT', 
        @TotRecs = @TotRecs OUTPUT 
        
		SELECT * FROM #tempForm
		DROP TABLE #tempForm
    END
  
SET ANSI_NULLS ON
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetWorkFlowFormFieldMaster')
DROP PROCEDURE USP_GetWorkFlowFormFieldMaster
GO
CREATE PROCEDURE [dbo].[USP_GetWorkFlowFormFieldMaster]     
    @numDomainID numeric(18, 0),
	@numFormID numeric(18, 0)
as                 
BEGIN
CREATE TABLE #tempField(numFieldID NUMERIC,
vcFieldName NVARCHAR(50),vcDbColumnName NVARCHAR(50),vcOrigDbColumnName NVARCHAR(50),vcFieldDataType CHAR(1),
vcAssociatedControlType NVARCHAR(50),vcListItemType VARCHAR(3),numListID NUMERIC,bitCustom BIT,vcLookBackTableName NVARCHAR(50),bitAllowFiltering BIT,bitAllowEdit BIT,vcGroup varchar(100),intWFCompare int)

--Regular Fields
INSERT INTO #tempField
	SELECT numFieldID,vcFieldName,
		   vcDbColumnName,vcOrigDbColumnName,vcFieldDataType,vcAssociatedControlType,
		   vcListItemType,numListID,CAST(0 AS BIT) AS bitCustom,vcLookBackTableName,bitAllowFiltering,bitAllowEdit,ISNULL(vcGroup,'Custom Fields')as vcGroup,ISNULL(intWFCompare,0) as intWFCompare
		FROM View_DynamicDefaultColumns
		where numFormId=@numFormId and numDomainID=@numDomainID 
		AND ISNULL(bitWorkFlowField,0)=1 AND ISNULL(bitCustom,0)=0 AND 1 = (CASE 
																				WHEN @numFormId = 68
																				THEN (CASE WHEN vcGroup='Contact Fields' THEN 0 ELSE 1 END)
																				WHEN @numFormId = 70
																				THEN (CASE WHEN (vcGroup='BizDoc Fields' AND vcDbColumnName <> 'monAmountPaid') OR vcGroup='Contact Fields' OR vcGroup='Milestone/Stage Fields' THEN 0 ELSE 1 END)
																				WHEN @numFormId = 94
																				THEN (CASE WHEN vcGroup='Project Fields' THEN 0 ELSE 1 END)
																				ELSE 1
																			END)


DECLARE @vcLocationID VARCHAR(100);SET @vcLocationID='0'
Select @vcLocationID =isnull(vcLocationID,'0') from dynamicFormMaster where numFormID=@numFormId

IF @numFormID = 49
BEGIN
	IF LEN(ISNULL(@vcLocationID,'')) > 0
	BEGIN
		Select @vcLocationID = @vcLocationID + ',' + isnull(vcLocationID,'0') from dynamicFormMaster where numFormID=70
	END
	ELSE
	BEGIN
		Select @vcLocationID =isnull(vcLocationID,'0') from dynamicFormMaster where numFormID=70
	END
END
ELSE IF @numFormID = 94
BEGIN
	-- DO NOT SHOW CUSTOM FIELDS
	SET @vcLocationID = ''
END

IF @numFormID IN (69,70,49,72,73,124)
BEGIN
	IF LEN(ISNULL(@vcLocationID ,'')) > 0
	BEGIN
		SET @vcLocationID = CONCAT(@vcLocationID,',1,12,13,14')
	END
	ELSE
	BEGIN
		SET @vcLocationID = '1,12,13,14'
	END
END

--Custom Fields			
INSERT INTO #tempField
SELECT ISNULL(CFM.Fld_Id,0) as numFieldID,CFM.Fld_label as vcFieldName,
	   'Cust'+Convert(varchar(10),Fld_Id) as vcDbColumnName,'Cust'+Convert(varchar(10),Fld_Id) AS vcOrigDbColumnName,CASE CFM.Fld_type WHEN 'SelectBox' THEN 'V' WHEN 'DateField' THEN 'D' ELSE 'V' END AS vcFieldDataType,
	   CFM.Fld_type AS vcAssociatedControlType,CASE WHEN CFM.Fld_type='SelectBox' THEN 'LI' ELSE '' END AS vcListItemType,isnull(CFM.numListID,0) as numListID,
	   CAST(1 AS BIT) AS bitCustom,L.vcCustomLookBackTableName AS vcLookBackTableName,0 AS bitAllowFiltering,1 AS bitAllowEdit,(CASE WHEN Grp_id IN (1,12,13,14) THEN 'Organization Custom Fields' WHEN Grp_id=4 THEN 'Contact Custom Fields' WHEN Grp_id IN (2,6) THEN 'Opportunity & Order Custom Fields' WHEN Grp_id = 3 THEN 'Case Custom Fields' WHEN Grp_id=11 THEN 'Project Custom Fields' ELSE 'Custom Fields' END)  as vcGroup,0 as intWFCompare
FROM CFW_Fld_Master CFM LEFT JOIN CFW_Validation V ON V.numFieldID = CFM.Fld_id
					JOIN CFW_Loc_Master L on CFM.GRP_ID=L.Loc_Id
            WHERE   CFM.numDomainID = @numDomainId
					AND GRP_ID IN(	SELECT ID FROM dbo.SplitIDs(@vcLocationID,','))
 

SELECT *,/*vcOrigDbColumnName + '~' +*/ CAST(numFieldID AS VARCHAR(18)) + '_' + CAST(CASE WHEN bitCustom=1 THEN 'True' ELSE 'False' END AS VARCHAR(10)) AS ID  FROM #tempField
ORDER BY vcGroup,vcFieldName,bitCustom

DROP TABLE #tempField
END
GO
/****** Object:  StoredProcedure [dbo].[USP_ManageAdvSearchViews]    Script Date: 07/26/2008 16:19:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_manageadvsearchviews')
DROP PROCEDURE usp_manageadvsearchviews
GO
CREATE PROCEDURE [dbo].[USP_ManageAdvSearchViews]  
@strAdvSerView AS TEXT='',  
@numDomainID NUMERIC(18,0),  
@FormId NUMERIC(18,0),
@numUserCntID NUMERIC(18,0)
AS
BEGIN
	DELETE FROM AdvSerViewConf WHERE numDomainID=@numDomainID and numUserCntID=@numUserCntID and numFormId = @FormId
  
	DECLARE @hDoc1 INT                                  
	EXEC sp_xml_preparedocument @hDoc1 OUTPUT, @strAdvSerView                                  
                                  
	INSERT INTO AdvSerViewConf                                  
	(
		numFormFieldID
		,tintOrder
		,numDomainID
		,numUserCntID
		,numformId
		,bitCustom
	)                                                                 
	SELECT 
		X.numFormFieldID
		,X.tintOrder
		,@numDomainID
		,@numUserCntID
		,@FormId
		,X.bitCustom 
	FROM 
	(
		SELECT 
			* 
		FROM 
			OPENXML (@hDoc1,'/NewDataSet/Table',2)                                  
		WITH 
		(
			numFormFieldID numeric(9),  
			tintOrder tinyint,
			bitCustom BIT
		)
	)X                                 
                                                                
	EXEC sp_xml_removedocument @hDoc1
END
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ManageSavedSearch' ) 
    DROP PROCEDURE USP_ManageSavedSearch
GO
CREATE PROCEDURE USP_ManageSavedSearch
    @numSearchID NUMERIC  output,
    @vcSearchName VARCHAR(50),
    @vcSearchQuery VARCHAR(8000),
    @intSlidingDays INT= 0,
    @vcTimeExpression VARCHAR(1000),
    @numFormID NUMERIC(18, 0),
    @numUserCntID NUMERIC(18, 0),
    @numDomainID NUMERIC(18, 0),
    @tintMode TINYINT,
    @vcSearchQueryDisplay VARCHAR(8000),
	@vcSearchConditionJson VARCHAR(MAX),
	@vcSharedWithUsers VARCHAR(MAX),
	@vcDisplayColumns VARCHAR(MAX)
AS 
BEGIN

    IF @tintMode = 1 
    BEGIN
        SELECT  
			numSearchID,
            vcSearchName,
            vcSearchQuery,
            intSlidingDays,
            vcTimeExpression,
            S.numFormID,
            numUserCntID,
            numDomainID,
            vcSearchQueryDisplay
			,CASE DFM.numFormId WHEN 1 THEN 'Companies & Contacts Search' ELSE DFM.vcFormName END AS vcFormName
			,vcSearchConditionJson
			,vcDisplayColumns
			,STUFF((SELECT 
						CONCAT(',',numUserCntID)
					FROM 
						SavedSearch SS 
					WHERE 
						SS.numDomainID=@numDomainID 
						AND numSharedFromSearchID=S.numSearchID
					FOR XML PATH('')),1,1,'') vcSharedWith
        FROM
			dbo.SavedSearch S
        INNER JOIN
			dbo.DynamicFormMaster DFM 
		ON 
			DFM.numFormId = S.numFormId
        WHERE 
			(numSearchID = @numSearchID OR @numSearchID = 0)
			AND numDomainID = @numDomainID
			AND numUserCntID = @numUserCntID
			AND (S.numFormID=@numFormID OR @numFormID = 0)
    END
    ELSE IF @tintMode = 2 
    BEGIN
        IF EXISTS(select * from SavedSearch where numSearchID = @numSearchID)
        BEGIN 
            UPDATE 
				dbo.SavedSearch 
			SET 
				vcSearchName = @vcSearchName
				,vcSearchQuery = @vcSearchQuery
				,intSlidingDays = @intSlidingDays
				,vcTimeExpression = @vcTimeExpression
				,numFormID = @numFormID
				,numUserCntID = @numUserCntID
				,numDomainID = @numDomainID
				,vcSearchQueryDisplay = @vcSearchQueryDisplay
				,vcSearchConditionJson = @vcSearchConditionJson
				,vcDisplayColumns=@vcDisplayColumns
            WHERE 
				numSearchID = @numSearchID

			DELETE FROM SavedSearch WHERE numDomainID=@numDomainID AND numSharedFromSearchID=@numSearchID
			
			IF LEN(ISNULL(@vcSharedWithUsers,0)) > 0
			BEGIN
				INSERT  INTO dbo.SavedSearch 
				(
					vcSearchName
					,vcSearchQuery
					,intSlidingDays
					,vcTimeExpression
					,numFormID
					,numUserCntID
					,numDomainID
					,vcSearchQueryDisplay
					,vcSearchConditionJson
					,numSharedFromSearchID
					,vcDisplayColumns
				)
				SELECT
					@vcSearchName,
					@vcSearchQuery,
					@intSlidingDays,
					@vcTimeExpression,
					@numFormID,
					UserMaster.numUserDetailId,
					@numDomainID,
					@vcSearchQueryDisplay,
					@vcSearchConditionJson,
					@numSearchID,
					@vcDisplayColumns
				FROM 
					SplitIDs(@vcSharedWithUsers,',') AS TEMPTable
				INNER JOIN
					UserMaster
				ON
					TEMPTable.Id = UserMaster.numUserDetailId
				WHERE
					UserMaster.numDomainID= @numDomainID
			END
        END   
        ELSE 
        BEGIN
            INSERT  INTO dbo.SavedSearch 
			(
				vcSearchName
				,vcSearchQuery
				,intSlidingDays
				,vcTimeExpression
				,numFormID
				,numUserCntID
				,numDomainID
				,vcSearchQueryDisplay
				,vcSearchConditionJson
				,vcDisplayColumns
			)
			VALUES  
			(
                @vcSearchName,
                @vcSearchQuery,
                @intSlidingDays,
                @vcTimeExpression,
                @numFormID,
                @numUserCntID,
                @numDomainID,
                @vcSearchQueryDisplay,
				@vcSearchConditionJson,
				@vcDisplayColumns
            ) 

			SELECT @numSearchID =  SCOPE_IDENTITY()
                        
			
			IF LEN(ISNULL(@vcSharedWithUsers,0)) > 0
			BEGIN
				INSERT  INTO dbo.SavedSearch 
				(
					vcSearchName
					,vcSearchQuery
					,intSlidingDays
					,vcTimeExpression
					,numFormID
					,numUserCntID
					,numDomainID
					,vcSearchQueryDisplay
					,vcSearchConditionJson
					,numSharedFromSearchID
					,vcDisplayColumns
				)
				SELECT
					@vcSearchName,
					@vcSearchQuery,
					@intSlidingDays,
					@vcTimeExpression,
					@numFormID,
					UserMaster.numUserDetailId,
					@numDomainID,
					@vcSearchQueryDisplay,
					@vcSearchConditionJson,
					@numSearchID,
					@vcDisplayColumns
				FROM 
					SplitIDs(@vcSharedWithUsers,',') AS TEMPTable
				INNER JOIN
					UserMaster
				ON
					TEMPTable.Id = UserMaster.numUserDetailId
				WHERE
					UserMaster.numDomainID= @numDomainID
			END
        END
    END
    ELSE 
    IF @tintMode = 3 
    BEGIN
        DELETE FROM dbo.SavedSearch WHERE numUserCntID = @numUserCntID AND numDomainID = @numDomainID AND numSearchID = @numSearchID
    END
END

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_oppdetailsdtlpl')
DROP PROCEDURE usp_oppdetailsdtlpl
GO
CREATE PROCEDURE [dbo].[USP_OPPDetailsDTLPL]
(
               @OpportunityId        AS NUMERIC(9)  = NULL,
               @numDomainID          AS NUMERIC(9),
               @ClientTimeZoneOffset AS INT)
AS
  SELECT Opp.numoppid,numCampainID,
		 CAMP.vcCampaignName AS [numCampainIDName],
         dbo.Fn_getcontactname(Opp.numContactId) numContactIdName,
		 Opp.numContactId,
         Opp.txtComments,
         Opp.numDivisionID,
         tintOppType,
         Dateadd(MINUTE,-@ClientTimeZoneOffset,bintAccountClosingDate) AS bintAccountClosingDate,
         dbo.fn_GetOpportunitySourceValue(ISNULL(Opp.tintSource,0),ISNULL(Opp.tintSourceType,0),Opp.numDomainID) AS tintSourceName,
          tintSource,
         Opp.VcPoppName,
         intpEstimatedCloseDate,
         [dbo].[getdealamount](@OpportunityId,Getutcdate(),0) AS CalAmount,
         --monPAmount,
         CONVERT(DECIMAL(10,2),Isnull(monPAmount,0)) AS monPAmount,
		lngPConclAnalysis,
         (SELECT vcdata
          FROM   listdetails
          WHERE  numlistitemid = lngPConclAnalysis) AS lngPConclAnalysisName,
         (SELECT vcdata
          FROM   listdetails
          WHERE  numlistitemid = numSalesOrPurType) AS numSalesOrPurTypeName,
		  numSalesOrPurType,
		 Opp.vcOppRefOrderNo,
		 Opp.vcMarketplaceOrderID,
         C2.vcCompanyName,
         D2.tintCRMType,
         Opp.tintActive,
         dbo.Fn_getcontactname(Opp.numCreatedby)
           + ' '
           + CONVERT(VARCHAR(20),Dateadd(MINUTE,-@ClientTimeZoneOffset,Opp.bintCreatedDate)) AS CreatedBy,
         dbo.Fn_getcontactname(Opp.numModifiedBy)
           + ' '
           + CONVERT(VARCHAR(20),Dateadd(MINUTE,-@ClientTimeZoneOffset,Opp.bintModifiedDate)) AS ModifiedBy,
         dbo.Fn_getcontactname(Opp.numRecOwner) AS RecordOwner,
         Opp.numRecOwner,
         Isnull(tintshipped,0) AS tintshipped,
         Isnull(tintOppStatus,0) AS tintOppStatus,
         [dbo].[GetOppStatus](Opp.numOppId) AS vcDealStatus,
         dbo.Opportunitylinkeditems(@OpportunityId) +
         (SELECT COUNT(*) FROM  dbo.Communication Comm JOIN Correspondence Corr ON Comm.numCommId=Corr.numCommID
			WHERE Comm.numDomainId=Opp.numDomainId AND Corr.numDomainID=Comm.numDomainID AND Corr.tintCorrType IN (1,2,3,4) AND Corr.numOpenRecordID=Opp.numOppId) AS NoOfProjects,
         (SELECT A.vcFirstName
                   + ' '
                   + A.vcLastName
          FROM   AdditionalContactsInformation A
          WHERE  A.numcontactid = opp.numAssignedTo) AS numAssignedToName,
		  opp.numAssignedTo,
         Opp.numAssignedBy,
         (SELECT COUNT(* )
          FROM   GenericDocuments
          WHERE  numRecID = @OpportunityId
                 AND vcDocumentSection = 'O') AS DocumentCount,
         CASE 
           WHEN tintOppStatus = 1 THEN 100
           ELSE isnull((SELECT SUM(tintPercentage)
                 FROM   OpportunityStageDetails
                 WHERE  numOppId = @OpportunityId
                        AND bitStageCompleted = 1),0)
         END AS TProgress,
         (SELECT TOP 1 Isnull(varRecurringTemplateName,'')
          FROM   RecurringTemplate
          WHERE  numRecurringId = OPR.numRecurringId) AS numRecurringIdName,
		 OPR.numRecurringId,
         Link.numOppID AS numParentOppID,
         Link.vcPoppName AS ParentOppName,
         Link.vcSource AS Source,
         ISNULL(C.varCurrSymbol,'') varCurrSymbol,
          ISNULL(Opp.fltExchangeRate,0) AS [fltExchangeRate],
           ISNULL(C.chrCurrency,'') AS [chrCurrency],
         ISNULL(C.numCurrencyID,0) AS [numCurrencyID],
         (SELECT COUNT(*) FROM [OpportunityLinking] OL WHERE OL.[numParentOppID] = Opp.numOppId) AS ChildCount,
         (SELECT COUNT(*) FROM [RecurringTransactionReport] RTR WHERE RTR.[numRecTranSeedId] = Opp.numOppId)  AS RecurringChildCount,
         (SELECT COUNT(*) FROM [ShippingReport] WHERE numOppBizDocId IN (SELECT numOppBizDocsId FROM   OpportunityBizDocs BD WHERE  BD.numOppId = @OpportunityId))AS ShippingReportCount,
         ISNULL(Opp.numStatus ,0) numStatus,
         (SELECT vcdata
          FROM   listdetails
          WHERE  numlistitemid = Opp.numStatus) AS numStatusName,
          (SELECT SUBSTRING(
	(SELECT ','+  A.vcFirstName+' ' +A.vcLastName + CASE WHEN ISNULL(SR.numContactType,0)>0 THEN '(' + dbo.fn_GetListItemName(SR.numContactType) + ')' ELSE '' END
	FROM ShareRecord SR JOIN UserMaster UM ON UM.numUserDetailId=SR.numAssignedTo 
							 JOIN AdditionalContactsInformation A ON UM.numUserDetailId=A.numContactID 
		 WHERE SR.numDomainID=Opp.numDomainID AND SR.numModuleID=3 AND SR.numRecordID=Opp.numOppId
				AND UM.numDomainID=Opp.numDomainID and UM.numDomainID=A.numDomainID FOR XML PATH('')),2,200000)) AS numShareWith,
		ISNULL((SELECT SUM(ISNULL(monDealAmount,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = @OpportunityId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0) AS monDealAmount,
		ISNULL((SELECT SUM(ISNULL(monAmountPaid,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = @OpportunityId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0) AS monAmountPaid,
		ISNULL(Opp.tintSourceType,0) AS tintSourceType,
		ISNULL(Opp.tintTaxOperator,0) AS tintTaxOperator,  
             isnull(Opp.intBillingDays,0) AS numBillingDays,isnull(Opp.bitInterestType,0) AS bitInterestType,
             isnull(opp.fltInterest,0) AS fltInterest,  isnull(Opp.fltDiscount,0) AS fltDiscount,
             isnull(Opp.bitDiscountType,0) AS bitDiscountType,ISNULL(Opp.bitBillingTerms,0) AS bitBillingTerms,
             ISNULL(OPP.numDiscountAcntType,0) AS numDiscountAcntType,
              dbo.fn_getOPPAddress(Opp.numOppId,@numDomainID,1) AS BillingAddress,
            dbo.fn_getOPPAddress(Opp.numOppId,@numDomainID,2) AS ShippingAddress,

            -- ISNULL((SELECT SUBSTRING((SELECT '$^$' + ISNULL( (SELECT vcShipFieldValue FROM dbo.ShippingFieldValues WHERE numDomainID=Opp.numDomainID AND intShipFieldID IN ( SELECT intShipFieldID FROM dbo.ShippingFields WHERE numListItemID=OBD.numShipVia  AND vcFieldName='Tracking URL')),'') +'#^#'+ RTRIM(LTRIM(vcTrackingNo) )  FROM dbo.OpportunityBizDocs OBD WHERE OBD.numOppId=Opp.numOppID FOR XML PATH('')),4,200000)),'') AS  vcTrackingNo,
			 (SELECT  
				SUBSTRING(
							(SELECT '$^$' + 
							(SELECT vcShipFieldValue FROM dbo.ShippingFieldValues 
								WHERE numDomainID=Opp.numDomainID AND intShipFieldID IN ( SELECT intShipFieldID FROM dbo.ShippingFields WHERE numListItemID=OBD.numShipVia  AND vcFieldName='Tracking URL')
							) +'#^#'+ RTRIM(LTRIM(vcTrackingNo))						
							FROM dbo.OpportunityBizDocs OBD WHERE OBD.numOppId= Opp.numOppID and ISNULL(OBD.numShipVia,0) <> 0
				FOR XML PATH('')),4,200000)
						
			 ) AS  vcTrackingNo,

            ISNULL(numPercentageComplete,0) numPercentageComplete,
            dbo.GetListIemName(ISNULL(numPercentageComplete,0)) AS PercentageCompleteName,
            ISNULL(numBusinessProcessID,0) AS [numBusinessProcessID],
            CASE WHEN opp.tintOppType=1 THEN dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID) ELSE '' END AS vcInventoryStatus
            ,ISNULL(bitUseShippersAccountNo,0) [bitUseShippersAccountNo], ISNULL(D2.vcShippersAccountNo,'') [vcShippersAccountNo] ,
			(SELECT SUM((ISNULL(monTotAmtBefDiscount, ISNULL(monTotAmount,0))- ISNULL(monTotAmount,0))) FROM OpportunityBizDocItems WHERE numOppBizDocID IN (SELECT numOppBizDocsId FROM OpportunityBizDocs WHERE numOppId = @OpportunityId AND bitAuthoritativeBizDocs = 1)) AS numTotalDiscount,
			ISNULL(Opp.vcRecurrenceType,'') AS vcRecurrenceType,
			ISNULL(Opp.bitRecurred,0) AS bitRecurred,
			(
				CASE 
				WHEN opp.tintOppType=1 AND opp.tintOppStatus = 1 
				THEN  
					dbo.GetOrderShipReceiveStatus(opp.numOppId,1,opp.tintShipped)
				ELSE 
						'' 
				END
			) AS vcOrderedShipped,
			(
				CASE 
				WHEN opp.tintOppType = 2 AND opp.tintOppStatus = 1 
				THEN  
					dbo.GetOrderShipReceiveStatus(opp.numOppId,2,opp.tintShipped)
				ELSE 
						'' 
				END
			) AS vcOrderedReceived,
			ISNULL(C2.numCompanyType,0) AS numCompanyType,
		ISNULL(C2.vcProfile,0) AS vcProfile,
		ISNULL(Opp.numAccountClass,0) AS numAccountClass,D3.vcPartnerCode+'-'+C3.vcCompanyName as numPartner,
		ISNULL(Opp.numPartner,0) AS numPartnerId,
		ISNULL(Opp.bitIsInitalSalesOrder,0) AS bitIsInitalSalesOrder,
		ISNULL(Opp.dtReleaseDate,'') AS dtReleaseDate,
		ISNULL(Opp.numPartenerContact,0) AS numPartenerContactId,
		A.vcGivenName AS numPartenerContact,
		Opp.numReleaseStatus,
		(CASE Opp.numReleaseStatus WHEN 1 THEN 'Open' WHEN 2 THEN 'Purchased' ELSE '' END) numReleaseStatusName,
		ISNULL(Opp.intUsedShippingCompany,0) intUsedShippingCompany,
		CASE WHEN ISNULL(Opp.intUsedShippingCompany,0) = 0 THEN '' ELSE ISNULL((SELECT vcData FROM ListDetails WHERE numListID = 82 AND numListItemID=ISNULL(Opp.intUsedShippingCompany,0)),'') END AS vcShippingCompany,
		ISNULL(Opp.numShipmentMethod,0) numShipmentMethod
		,CASE WHEN ISNULL(Opp.numShipmentMethod,0) = 0 THEN '' ELSE ISNULL((SELECT vcData FROM ListDetails WHERE numListID = 338 AND numListItemID=ISNULL(Opp.numShipmentMethod,0)),'') END AS vcShipmentMethod
		,ISNULL(Opp.vcCustomerPO#,'') vcCustomerPO#,
		CONCAT(CASE tintEDIStatus 
			--WHEN 1 THEN '850 Received'
			WHEN 2 THEN '850 Acknowledged'
			WHEN 3 THEN '940 Sent'
			WHEN 4 THEN '940 Acknowledged'
			WHEN 5 THEN '856 Received'
			WHEN 6 THEN '856 Acknowledged'
			WHEN 7 THEN '856 Sent'
			WHEN 8 THEN 'Send 940 Failed'
			WHEN 9 THEN 'Send 856 & 810 Failed'
			WHEN 11 THEN '850 Partially Created'
			WHEN 12 THEN '850 SO Created'
			ELSE '' 
		END,'  ','<a onclick="return Open3PLEDIHistory(',Opp.numOppID,')"><img src="../images/GLReport.png" border="0"></a>') tintEDIStatusName,
		ISNULL(Opp.numShippingService,0) numShippingService,
		ISNULL((SELECT vcShipmentService FROM ShippingService WHERE (numDomainID=@numDomainID OR ISNULL(numDomainID,0)=0) AND numShippingServiceID = Opp.numShippingService),'') AS vcShippingService,
		CASE 
			WHEN vcSignatureType = 0 THEN 'Service Default'
			WHEN vcSignatureType = 1 THEN 'Adult Signature Required'
			WHEN vcSignatureType = 2 THEN 'Direct Signature'
			WHEN vcSignatureType = 3 THEN 'InDirect Signature'
			WHEN vcSignatureType = 4 THEN 'No Signature Required'
			ELSE ''
		END AS vcSignatureType
  FROM   OpportunityMaster Opp
         JOIN divisionMaster D2 ON Opp.numDivisionID = D2.numDivisionID
         JOIN CompanyInfo C2 ON C2.numCompanyID = D2.numCompanyID
		 LEFT JOIN DivisionMasterShippingConfiguration ON D2.numDivisionID=DivisionMasterShippingConfiguration.numDivisionID
		 LEFT JOIN divisionMaster D3 ON Opp.numPartner = D3.numDivisionID
		 LEFT JOIN AdditionalContactsInformation A ON Opp.numPartenerContact = A.numContactId
         LEFT JOIN CompanyInfo C3 ON C3.numCompanyID = D3.numCompanyID
         LEFT JOIN (SELECT TOP 1 vcPoppName,
                                 numOppID,
                                 numChildOppID,
                                 vcSource,numParentOppID,OpportunityMaster.numDivisionID AS numParentDivisionID 
                    FROM   OpportunityLinking
                          left JOIN OpportunityMaster
                             ON numOppID = numParentOppID
                    WHERE  numChildOppID = @OpportunityId) Link
           ON Link.numChildOppID = Opp.numOppID
           LEFT OUTER JOIN [Currency] C
					ON C.numCurrencyID = Opp.numCurrencyID
			LEFT OUTER JOIN [OpportunityRecurring] OPR
					ON OPR.numOppId = Opp.numOppId
					 LEFT JOIN AddressDetails AD --Billing add
				ON AD.numDomainID=D2.numDomainID AND AD.numRecordID=D2.numDivisionID AND AD.tintAddressOf = 2 AND AD.tintAddressType = 1 AND AD.bitIsPrimary=1
			 LEFT JOIN AddressDetails AD2--Shipping address
				ON AD2.numDomainID=D2.numDomainID AND AD2.numRecordID=D2.numDivisionID AND AD2.tintAddressOf = 2 AND AD2.tintAddressType = 2 AND AD2.bitIsPrimary=1	
		LEFT JOIN dbo.CampaignMaster CAMP ON CAMP.numCampaignID = Opp.numCampainID 
  WHERE  Opp.numOppId = @OpportunityId
         AND Opp.numDomainID = @numDomainID
/****** Object:  StoredProcedure [dbo].[USP_OppManage]    Script Date: 07/26/2008 16:20:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OppManage')
DROP PROCEDURE usp_oppmanage
GO
CREATE PROCEDURE [dbo].[USP_OppManage] 
(                                                                          
  @numOppID as numeric=0 OUTPUT,                                                                          
  @numContactId numeric(9)=null,                                                                          
  @numDivisionId numeric(9)=null,                                                                          
  @tintSource numeric(9)=null,                                                                          
  @vcPOppName Varchar(100)='' OUTPUT,                                                                          
  @Comments varchar(1000)='',                                                                          
  @bitPublicFlag bit=0,                                                                          
  @numUserCntID numeric(9)=0,                                                                                 
  @monPAmount DECIMAL(20,5) =0,                                                 
  @numAssignedTo as numeric(9)=0,                                                                                                        
  @numDomainId numeric(9)=0,                                                                                                                                                                                   
  @strItems as text=null,                                                                          
  @strMilestone as text=null,                                                                          
  @dtEstimatedCloseDate datetime,                                                                          
  @CampaignID as numeric(9)=null,                                                                          
  @lngPConclAnalysis as numeric(9)=null,                                                                         
  @tintOppType as tinyint,                                                                                                                                             
  @tintActive as tinyint,                                                              
  @numSalesOrPurType as numeric(9),              
  @numRecurringId as numeric(9),
  @numCurrencyID as numeric(9)=0,
  @DealStatus as tinyint =0,-- 0=Open,1=Won,2=Lost
  @numStatus AS NUMERIC(18,0),
  @vcOppRefOrderNo VARCHAR(100),
  @numBillAddressId numeric(9)=0,
  @numShipAddressId numeric(9)=0,
  @bitStockTransfer BIT=0,
  @WebApiId INT = 0,
  @tintSourceType TINYINT=0,
  @bitDiscountType as bit,
  @fltDiscount  as float,
  @bitBillingTerms as bit,
  @intBillingDays as integer,
  @bitInterestType as bit,
  @fltInterest as float,
  @tintTaxOperator AS TINYINT,
  @numDiscountAcntType AS NUMERIC(9),
  @vcWebApiOrderNo VARCHAR(100)=NULL,
  @vcCouponCode		VARCHAR(100) = '',
  @vcMarketplaceOrderID VARCHAR(100)=NULL,
  @vcMarketplaceOrderDate  datetime=NULL,
  @vcMarketplaceOrderReportId VARCHAR(100)=NULL,
  @numPercentageComplete NUMERIC(9),
  @bitUseShippersAccountNo BIT = 0,
  @bitUseMarkupShippingRate BIT = 0,
  @numMarkupShippingRate NUMERIC(19,2) = 0,
  @intUsedShippingCompany INT = 0,
  @numShipmentMethod NUMERIC(18,0)=0,
  @dtReleaseDate DATE = NULL,
  @numPartner NUMERIC(18,0)=0,
  @tintClickBtn INT=0,
  @numPartenerContactId NUMERIC(18,0)=0,
  @numAccountClass NUMERIC(18,0) = 0,
  @numWillCallWarehouseID NUMERIC(18,0) = 0,
  @numVendorAddressID NUMERIC(18,0) = 0,
  @numShipFromWarehouse NUMERIC(18,0) = 0,
  @numShippingService NUMERIC(18,0) = 0,
  @ClientTimeZoneOffset INT = 0,
  @vcCustomerPO VARCHAR(100)=NULL
  -- USE PARAMETER WITH OPTIONAL VALUE BECAUSE PROCEDURE IS CALLED FROM OTHER PROCEDURE WHICH WILL NOT PASS NEW PARAMETER VALUE
)                                                                          
                                                                          
as  
BEGIN TRY
BEGIN TRANSACTION
	DECLARE @fltExchangeRate float                                 
	DECLARE @hDocItem int                                                                                                                            
	DECLARE @TotalAmount as DECIMAL(20,5)                                                                          
	DECLARE @tintOppStatus as tinyint
	DECLARE @bitNewOrder AS BIT = (CASE WHEN ISNULL(@numOppID,0) = 0 THEN 1 ELSE 0 END)
	
	IF ISNULL(@numContactId,0) = 0
	BEGIN
		RAISERROR('CONCAT_REQUIRED',16,1)
		RETURN
	END  
	
	IF ISNULL(@tintOppType,0) = 0
	BEGIN
		RAISERROR('OPP_TYPE_CAN_NOT_BE_0',16,1)
		RETURN
	END             

	IF ISNULL(@numOppID,0) > 0 AND EXISTS (SELECT numOppId FROM OpportunityMaster WHERE numDomainId=@numDomainId AND numOppId=@numOppID AND ISNULL(tintshipped,0) = 1) 
	BEGIN	 
		RAISERROR('OPP/ORDER_IS_CLOSED',16,1)
		RETURN
	END

	DECLARE @dtItemRelease AS DATE
	SET @dtItemRelease = DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE())


	IF CONVERT(VARCHAR(10),@strItems) <> ''
	BEGIN
		EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems

		--VALIDATE PROMOTION OF COUPON CODE
		DECLARE @TEMP TABLE
		(
			ID INT IDENTITY(1,1),
			numOppItemID NUMERIC(18,0),
			numPromotionID NUMERIC(18,0)
		)

		INSERT INTO @TEMP
		(
			numOppItemID,
			numPromotionID
		)
		SELECT
			numoppitemtCode,
			numPromotionID
		FROM
			OPENXML (@hDocItem,'/NewDataSet/Item',2)
		WITH
		(
			numoppitemtCode NUMERIC(18,0),
			numPromotionID NUMERIC(18,0)
		)

		IF (SELECT
				COUNT(*)
			FROM
				PromotionOffer PO
			WHERE
				numDomainId=@numDomainId
				AND bitRequireCouponCode = 1
				AND ISNULL(bitEnabled,0) = 1
				AND ISNULL(tintUsageLimit,0) > 0
				AND (intCouponCodeUsed 
					- (CASE WHEN ISNULL((SELECT COUNT(*) FROM OpportunityItems WHERE numOppID=ISNULL(@numOppID,0) AND numPromotionID=PO.numProId),0) > 0 THEN 1 ELSE 0 END) 
					+ (CASE WHEN ISNULL((SELECT COUNT(*) FROM @TEMP WHERE numPromotionID=PO.numProId),0) > 0 THEN 1 ELSE 0 END)) > tintUsageLimit) > 0
		BEGIN
			RAISERROR('USAGE_OF_COUPON_EXCEED',16,1)
			RETURN
		END
		ELSE
		BEGIN
			UPDATE
				PO
			SET 
				intCouponCodeUsed = ISNULL(intCouponCodeUsed,0) 
									- (CASE WHEN ISNULL((SELECT COUNT(*) FROM OpportunityItems WHERE numOppID=ISNULL(@numOppID,0) AND numPromotionID=PO.numProId),0) > 0 THEN 1 ELSE 0 END) 
									+ (CASE WHEN ISNULL(T1.intUsed,0) > 0 THEN 1 ELSE 0 END) 
			FROM
				PromotionOffer PO
			INNER JOIN
				(
					SELECT
						numPromotionID,
						COUNT(*) AS intUsed
					FROM
						@TEMP
					GROUP BY
						numPromotionID
				) AS T1
			ON
				PO.numProId = T1.numPromotionID
			WHERE
				numDomainId=@numDomainId
				AND bitRequireCouponCode = 1
				AND ISNULL(bitEnabled,0) = 1
		END
	END

	DECLARE @numCompany AS NUMERIC(18)
	SELECT @numCompany = numCompanyID FROM DivisionMaster WHERE numDivisionID = @numDivisionId

	DECLARE @tintCommitAllocation AS TINYINT
	DECLARE @bitAllocateInventoryOnPickList AS BIT

	SELECT @tintCommitAllocation=ISNULL(tintCommitAllocation,1) FROM Domain WHERE numDomainId=@numDomainId
	SELECT @bitAllocateInventoryOnPickList=ISNULL(bitAllocateInventoryOnPickList,0) FROM DivisionMaster WHERE numDivisionID=@numDivisionId

	--IF NEW OPPERTUNITY
	IF @numOppID = 0                                                                          
	BEGIN
		DECLARE @bitIsInitalSalesOrder BIT
		IF(@DealStatus=1 AND @tintOppType=1)
		BEGIN
			SET @bitIsInitalSalesOrder=1
		END

		DECLARE @intOppTcode AS NUMERIC(9)  
  
		SELECT @intOppTcode = MAX(numOppId) FROM OpportunityMaster                

		SET @intOppTcode = ISNULL(@intOppTcode,0) + 1                                                
		
		SET @vcPOppName = @vcPOppName + '-'+ CONVERT(varchar(4),YEAR(GETUTCDATE()))  +'-A'+ CONVERT(varchar(10),@intOppTcode)                                  
  
		IF ISNULL(@numCurrencyID,0) = 0 
		BEGIN
			SET @fltExchangeRate=1
			SELECT @numCurrencyID= ISNULL(numCurrencyID,0) FROM dbo.Domain WHERE numDomainId=@numDomainId	
		END
		ELSE
		BEGIN 
			SET @fltExchangeRate = dbo.GetExchangeRate(@numDomainID,@numCurrencyID)
        END  
		                                                        
		IF ISNULL(@numAccountClass,0) = 0
		BEGIN                                                  
		DECLARE @tintDefaultClassType AS INT = 0
		SELECT @tintDefaultClassType = ISNULL(tintDefaultClassType,0) FROM Domain WHERE numDomainId = @numDomainID 

		IF @tintDefaultClassType = 1 --USER
		BEGIN
			SELECT 
				@numAccountClass=ISNULL(numDefaultClass,0) 
			FROM 
				dbo.UserMaster UM 
			JOIN 
				dbo.Domain D 
			ON 
				UM.numDomainID=D.numDomainId
			WHERE 
				D.numDomainId=@numDomainId 
				AND UM.numUserDetailId=@numUserCntID
		END
		ELSE IF @tintDefaultClassType = 2 --COMPANY
		BEGIN
			SELECT 
				@numAccountClass=ISNULL(numAccountClassID,0) 
			FROM 
				dbo.DivisionMaster DM 
			WHERE 
				DM.numDomainId=@numDomainId 
				AND DM.numDivisionID=@numDivisionID
		END
		ELSE
		BEGIN
			SET @numAccountClass = 0
		END
		END

        IF(@DealStatus=1 AND @tintOppType=1 AND @tintClickBtn=1)
		BEGIN
			SET @DealStatus=0
		END         
		--PRINT ('Deal Staus ' +@DealStatus)                                                      
		INSERT INTO OpportunityMaster
		(                                                                             
			numContactId,numDivisionId,txtComments,numCampainID,bitPublicFlag,tintSource,tintSourceType,vcPOppName,                                                                          
			intPEstimatedCloseDate,monPAmount,numCreatedBy,bintCreatedDate,numModifiedBy,bintModifiedDate,numDomainId,                                                                                                             
			numRecOwner,lngPConclAnalysis,tintOppType,numSalesOrPurType,numCurrencyID,fltExchangeRate,numAssignedTo,
			numAssignedBy,[tintOppStatus],numStatus,vcOppRefOrderNo,bitStockTransfer,bitBillingTerms,intBillingDays,
			bitInterestType,fltInterest,vcCouponCode,bitDiscountType,fltDiscount,bitPPVariance,vcMarketplaceOrderID,
			vcMarketplaceOrderReportId,numPercentageComplete,numAccountClass,tintTaxOperator,bitUseShippersAccountNo,
			bitUseMarkupShippingRate,numMarkupShippingRate,intUsedShippingCompany,numShipmentMethod,numShippingService,numPartner,numPartenerContact,dtReleaseDate,bitIsInitalSalesOrder,numVendorAddressID,numShipFromWarehouse
			,vcCustomerPO#
		)                                                                          
		Values                                                                          
		(                                                                          
			@numContactId,@numDivisionId,ISNULL(@Comments,''),@CampaignID,@bitPublicFlag,@tintSource,@tintSourceType,@vcPOppName,                                                                          
			@dtEstimatedCloseDate,@monPAmount,@numUserCntID,(CASE @tintSourceType WHEN 3 THEN @vcMarketplaceOrderDate ELSE GETUTCDATE() END),                                                                          
			@numUserCntID,GETUTCDATE(),@numDomainId,@numUserCntID,@lngPConclAnalysis,@tintOppType,@numSalesOrPurType,@numCurrencyID, 
			@fltExchangeRate,(CASE WHEN @numAssignedTo > 0 THEN @numAssignedTo ELSE NULL END),(CASE WHEN @numAssignedTo>0 THEN @numUserCntID ELSE NULL END),
			@DealStatus,@numStatus,@vcOppRefOrderNo,@bitStockTransfer,@bitBillingTerms,@intBillingDays,@bitInterestType,@fltInterest,
			@vcCouponCode,@bitDiscountType,@fltDiscount,1,@vcMarketplaceOrderID,@vcMarketplaceOrderReportId,@numPercentageComplete,
			@numAccountClass,@tintTaxOperator,@bitUseShippersAccountNo,@bitUseMarkupShippingRate,@numMarkupShippingRate,@intUsedShippingCompany,@numShipmentMethod,@numShippingService,@numPartner,@numPartenerContactId,@dtReleaseDate,@bitIsInitalSalesOrder,@numVendorAddressID,@numShipFromWarehouse
			,@vcCustomerPO
		)                                                                                                                      
		
		SET @numOppID=(SELECT SCOPE_IDENTITY())                
                            
		--UPDATE OPPNAME AS PER NAME TEMPLATE
		EXEC dbo.USP_UpdateNameTemplateValue @tintOppType,@numDomainID,@numOppID

		SELECT @vcPOppName=ISNULL(vcPOppName,'') FROM OpportunityMaster WHERE numOppId=@numOppID

		--MAP CUSTOM FIELD	
		DECLARE @tintPageID AS TINYINT;
		SET @tintPageID = CASE WHEN @tintOppType=1 THEN 2 ELSE 6 END 
  	
		EXEC dbo.USP_AddParentChildCustomFieldMap
				@numDomainID = @numDomainID, --  numeric(18, 0)
				@numRecordID = @numOppID, --  numeric(18, 0)
				@numParentRecId = @numDivisionId, --  numeric(18, 0)
				@tintPageID = @tintPageID --  tinyint
 	
		IF ISNULL(@numStatus,0) > 0 AND @tintOppType=1 AND isnull(@DealStatus,0)=1
		BEGIN
	   		EXEC USP_ManageOpportunityAutomationQueue
					@numOppQueueID = 0, -- numeric(18, 0)
					@numDomainID = @numDomainID, -- numeric(18, 0)
					@numOppId = @numOppID, -- numeric(18, 0)
					@numOppBizDocsId = 0, -- numeric(18, 0)
					@numOrderStatus = @numStatus, -- numeric(18, 0)
					@numUserCntID = @numUserCntID, -- numeric(18, 0)
					@tintProcessStatus = 1, -- tinyint
					@tintMode = 1 -- TINYINT
		END
  
		-- INSERTING ITEMS                                                                                                                    
		IF CONVERT(VARCHAR(10),@strItems) <> '' AND @numOppID > 0
		BEGIN
			EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems       
	
			INSERT INTO OpportunityItems
			(
				numOppId,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,bitDropShip,bitDiscountType,fltDiscount,
				monTotAmtBefDiscount,vcItemName,vcModelID,vcManufacturer,vcPathForTImage,monVendorCost,numUOMId,bitWorkOrder,numVendorWareHouse,
				numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,numClassID,numToWarehouseItemID,vcAttributes,vcAttrValues,monAvgCost,bitItemPriceApprovalRequired,
				numPromotionID,bitPromotionTriggered,vcPromotionDetail,numSortOrder,numCost,vcNotes,vcInstruction,bintCompliationDate,numWOAssignedTo,numShipToAddressID,ItemReleaseDate
			)
			SELECT 
				@numOppID,X.numItemCode,dbo.fn_UOMConversion(X.numUOM,numItemCode,@numDomainId,null) * x.numUnitHour,x.monPrice,x.monTotAmount,X.vcItemDesc,
				(CASE X.numWarehouseItmsID 
				WHEN -1 
				THEN 
					CASE 
						WHEN ((SELECT COUNT(*) FROM Item WHERE numDomainID=@numDomainId AND vcSKU= X.vcSKU) > 0)
							THEN
								(SELECT TOP 1 
									numWareHouseItemID 
								FROM 
									WareHouseItems 
								WHERE 
									[numItemID] = X.numItemCode 
									AND numWareHouseID IN (SELECT TOP 1 ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail WHERE numDomainID =@numDomainId AND WebApiId = @WebApiId))
						ELSE
							(SELECT 
								[numWareHouseItemID] 
							FROM 
								[WareHouseItems] 
							WHERE 
								[numItemID] = X.numItemCode 
								AND ISNULL(WareHouseItems.vcWHSKU,'') = ISNULL(X.vcSKU,'')
								AND numWareHouseID IN (SELECT TOP 1 ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail WHERE numDomainID =@numDomainId AND WebApiId = @WebApiId)) 
					END
				WHEN 0 
				THEN 
					CASE 
					WHEN ((SELECT COUNT(*) FROM Item WHERE numDomainID=@numDomainId AND vcSKU=X.vcSKU) > 0)
						THEN
							(SELECT TOP 1 
								numWareHouseItemID 
							FROM 
								WareHouseItems 
							WHERE 
								[numItemID] = X.numItemCode 
								AND numWareHouseID IN (SELECT TOP 1 ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail WHERE numDomainID =@numDomainId AND WebApiId = @WebApiId))
					ELSE
						(SELECT 
							[numWareHouseItemID] 
						FROM 
							[WareHouseItems] 
						WHERE 
							[numItemID] = X.numItemCode 
							AND ISNULL(WareHouseItems.vcWHSKU,'') = ISNULL(X.vcSKU,'')
							AND numWareHouseID IN (SELECT TOP 1 ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail WHERE numDomainID =@numDomainId AND WebApiId = @WebApiId)) 
					END
				ELSE  
					X.numWarehouseItmsID 
				END) AS numWarehouseItmsID,
				X.ItemType,X.DropShip,X.bitDiscountType,X.fltDiscount,X.monTotAmtBefDiscount,X.vcItemName,(SELECT vcModelID FROM item WHERE numItemCode = X.numItemCode),
				(SELECT vcManufacturer FROM item WHERE numItemCode = X.numItemCode),(SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = X.numItemCode AND BitDefault = 1 AND numdomainId = @numdomainId),
				(SELECT isnull(VN.monCost,0) FROM Vendor VN INNER JOIN Item IT ON IT.numItemCode=VN.numItemCode AND VN.numVendorID=IT.numVendorID WHERE  VN.numItemCode=X.numItemCode),
				X.numUOM,X.bitWorkOrder,X.numVendorWareHouse,X.numShipmentMethod,X.numSOVendorId,X.numProjectID,X.numProjectStageID,
				0,
				X.numToWarehouseItemID,X.Attributes,X.AttributeIDs,(SELECT (CASE WHEN ISNULL(I.bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(I.monAverageCost,0) END) FROM Item I WHERE I.numDomainId=@numDomainId AND I.numItemCode=X.numItemCode),
				X.bitItemPriceApprovalRequired,X.numPromotionID,X.bitPromotionTriggered,X.vcPromotionDetail,X.numSortOrder,ISNULL(X.numCost,0),ISNULL(X.vcVendorNotes,'')
				,X.vcInstruction,X.bintCompliationDate,X.numWOAssignedTo,X.numShipToAddressID,ISNULL(ItemReleaseDate,@dtItemRelease)
			FROM
				(
					SELECT 
						*
					FROM 
						OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=1]',2)                                                                          
					WITH                       
					(                                                                          
						numoppitemtCode NUMERIC(9),numItemCode NUMERIC(9), numUnitHour FLOAT, monPrice DECIMAL(30,16), monTotAmount DECIMAL(20,5),vcItemDesc VARCHAR(1000),                                    
						numWarehouseItmsID NUMERIC(9), ItemType VARCHAR(30), DropShip BIT, bitDiscountType BIT,	fltDiscount DECIMAL(30,16), monTotAmtBefDiscount DECIMAL(20,5),
						vcItemName VARCHAR(300),numUOM NUMERIC(9),bitWorkOrder BIT,numVendorWareHouse NUMERIC(9),numShipmentMethod NUMERIC(9),numSOVendorId NUMERIC(9),
						numProjectID numeric(9),numProjectStageID numeric(9),numToWarehouseItemID NUMERIC(9),Attributes varchar(500),AttributeIDs VARCHAR(500),vcSKU VARCHAR(100),bitItemPriceApprovalRequired BIT,
						numPromotionID NUMERIC(18,0),bitPromotionTriggered BIT,vcPromotionDetail VARCHAR(2000),numSortOrder NUMERIC(18,0),numCost DECIMAL(20,5),vcVendorNotes VARCHAR(1000),
						vcInstruction VARCHAR(1000),bintCompliationDate DATETIME,numWOAssignedTo NUMERIC(18,0),numShipToAddressID  NUMERIC(18,0),ItemReleaseDate DATETIME
					)
				)X    
			ORDER BY 
				X.numoppitemtCode
    
			-- UPDATE EXISTING CUSTOMER PART NO
			UPDATE
				CPN
			SET
				CPN.CustomerPartNo = ISNULL(TEMPCPN.CustomerPartNo,'')
			FROM
				CustomerPartNumber CPN
			INNER JOIN
			(
				SELECT 
					numItemCode
					,MIN(CustomerPartNo) AS CustomerPartNo
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=1]',2)                                                                          
				WITH                       
				(                                                                          
					CustomerPartNo VARCHAR(300), numItemCode NUMERIC(18,0)
				)
				GROUP BY
					numItemCode
			) AS TEMPCPN
			ON
				CPN.numCompanyId = @numCompany
				AND CPN.numItemCode = TEMPCPN.numItemCode
			WHERE
				CPN.numDomainID=@numDomainID
				AND CPN.numCompanyId = @numCompany
				AND LEN(ISNULL(TEMPCPN.CustomerPartNo,'')) > 0
				AND ISNULL(TEMPCPN.CustomerPartNo,'') <> '0'
			
			-- INSERT NEW CUSTOMER PART NO
			INSERT INTO CustomerPartNumber
			(
				numCompanyId, numDomainID, CustomerPartNo, numItemCode 
			)
			SELECT 
				@numCompany, @numDomainID, TEMPCPN.CustomerPartNo,TEMPCPN.numItemCode
			FROM
			(
				SELECT 
					numItemCode
					,MIN(CustomerPartNo) AS CustomerPartNo
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=1]',2)                                                                          
				WITH                       
				(                                                                          
					CustomerPartNo VARCHAR(300), numItemCode NUMERIC(18,0)
				)
				GROUP BY
					numItemCode
			) TEMPCPN 
			LEFT JOIN
				CustomerPartNumber CPN
			ON
				CPN.numCompanyId = @numCompany
				AND CPN.numItemCode = TEMPCPN.numItemCode
			WHERE
				CPN.CustomerPartNoID IS NULL
				AND LEN(ISNULL(TEMPCPN.CustomerPartNo,'')) > 0
				AND ISNULL(TEMPCPN.CustomerPartNo,'') <> '0'

			UPDATE 
				OpportunityItems                       
			SET 
				numItemCode=X.numItemCode,numOppId=@numOppID,numUnitHour=dbo.fn_UOMConversion(X.numUOM,X.numItemCode,@numDomainId,null) * X.numUnitHour,                      
				monPrice=x.monPrice,monTotAmount=x.monTotAmount,vcItemDesc=X.vcItemDesc,numWarehouseItmsID=X.numWarehouseItmsID,bitDropShip=X.DropShip,
				bitDiscountType=X.bitDiscountType,fltDiscount=X.fltDiscount,monTotAmtBefDiscount=X.monTotAmtBefDiscount,vcItemName=X.vcItemName,numUOMId=numUOM,
				bitWorkOrder=X.bitWorkOrder,numVendorWareHouse=X.numVendorWareHouse,numShipmentMethod=X.numShipmentMethod,numSOVendorId=X.numSOVendorId,
				numProjectId=X.numProjectID,numProjectStageId=X.numProjectStageID,numToWarehouseItemID = X.numToWarehouseItemID,vcAttributes=X.Attributes,
				numClassID=0,vcModelID=X.vcModelID,
				bitItemPriceApprovalRequired = X.bitItemPriceApprovalRequired,vcAttrValues=X.AttributeIDs,
				numPromotionID=X.numPromotionID,bitPromotionTriggered=X.bitPromotionTriggered,vcPromotionDetail=X.vcPromotionDetail,numSortOrder=X.numSortOrder,numCost=X.numCost
				,vcNotes=X.vcVendorNotes,vcInstruction=X.vcInstruction,bintCompliationDate=X.bintCompliationDate,numWOAssignedTo=X.numWOAssignedTo
			FROM 
			(
				SELECT 
					numoppitemtCode as numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,vcModelID,numWarehouseItmsID,DropShip,bitDiscountType,fltDiscount,
					monTotAmtBefDiscount,vcItemName,numUOM,bitWorkOrder,numVendorWareHouse,numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,numToWarehouseItemID,
					Attributes,bitItemPriceApprovalRequired,AttributeIDs,numPromotionID,bitPromotionTriggered,vcPromotionDetail,numSortOrder,ISNULL(numCost,0) numCost
					,ISNULL(vcVendorNotes,'') vcVendorNotes,vcInstruction,bintCompliationDate,numWOAssignedTo
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=2]',2)
				WITH  
					(                      
						numoppitemtCode NUMERIC(9),numItemCode NUMERIC(9),numUnitHour FLOAT,monPrice DECIMAL(30,16),monTotAmount DECIMAL(20,5),vcItemDesc VARCHAR(1000), vcModelID VARCHAR(300),
						numWarehouseItmsID NUMERIC(9),DropShip BIT,bitDiscountType BIT,fltDiscount DECIMAL(30,16),monTotAmtBefDiscount DECIMAL(20,5),vcItemName VARCHAR(300),
						numUOM NUMERIC(9),bitWorkOrder BIT,numVendorWareHouse NUMERIC(9),numShipmentMethod NUMERIC(9),numSOVendorId NUMERIC(9),numProjectID NUMERIC(9),
						numProjectStageID numeric(9),numToWarehouseItemID NUMERIC(9),Attributes varchar(500),bitItemPriceApprovalRequired BIT,AttributeIDs VARCHAR(500),
						numPromotionID NUMERIC(18,0),bitPromotionTriggered BIT,vcPromotionDetail VARCHAR(2000),numSortOrder NUMERIC(18,0),numCost DECIMAL(20,5),vcVendorNotes VARCHAR(1000)
						,vcInstruction VARCHAR(1000),bintCompliationDate DATETIME,numWOAssignedTo NUMERIC(18,0)
					)
			)X 
			WHERE 
				numoppitemtCode=X.numOppItemID AND numOppID=@numOppID
				
			--INSERT KIT ITEMS                 
			INSERT INTO OpportunityKitItems
			(
				numOppId,
				numOppItemID,
				numChildItemID,
				numWareHouseItemId,
				numQtyItemsReq,
				numQtyItemsReq_Orig,
				numUOMId,
				numQtyShipped,
				monAvgCost
			)
			SELECT
				@numOppId,
				OI.numoppitemtCode,
				ID.numChildItemID,
				ID.numWareHouseItemId,
				(ID.numQtyItemsReq * ISNULL(dbo.fn_UOMConversion(ID.numUOMId,ID.numChildItemID,@numDomainID,I.numBaseUnit),1)) * OI.numUnitHour,
				ID.numQtyItemsReq,
				ID.numUOMId,
				0,
				ISNULL(I.monAverageCost,0)
			FROM 
				OpportunityItems OI 
			JOIN 
				ItemDetails ID 
			ON 
				OI.numItemCode=ID.numItemKitID 
			JOIN
				Item I
			ON
				ID.numChildItemID = I.numItemCode
			WHERE 
				OI.numOppId=@numOppId 

			IF (SELECT COUNT(*) FROM OPENXML (@hDocItem,'/NewDataSet/Item[bitHasKitAsChild=''true'']',2)) > 0
			BEGIN
				--INSERT KIT ITEM HAS ANOTHER KIT AS CHILD THEN SAVE CONFIGURATION OF KIT
				DECLARE @TempKitConfiguration TABLE
				(
					numItemCode NUMERIC(18,0),
					numWarehouseItemID NUMERIC(18,0),
					KitChildItems VARCHAR(MAX)
				)

				INSERT INTO 
					@TempKitConfiguration
				SELECT
					numItemCode,
					numWarehouseItmsID,
					KitChildItems
				FROM
					OPENXML (@hDocItem,'/NewDataSet/Item[bitHasKitAsChild=''true'']',2)
				WITH                       
				(
					numItemCode NUMERIC(9),numWarehouseItmsID NUMERIC(9), KitChildItems VARCHAR(MAX)
				) 

				INSERT INTO OpportunityKitChildItems
				(
					numOppID,
					numOppItemID,
					numOppChildItemID,
					numItemID,
					numWareHouseItemId,
					numQtyItemsReq,
					numQtyItemsReq_Orig,
					numUOMId,
					numQtyShipped,
					monAvgCost
				)
				SELECT 
					@numOppID,
					OKI.numOppItemID,
					OKI.numOppChildItemID,
					ItemDetails.numChildItemID,
					ItemDetails.numWareHouseItemId,
					(ItemDetails.numQtyItemsReq * ISNULL(dbo.fn_UOMConversion(ItemDetails.numUOMId,ItemDetails.numChildItemID,@numDomainID,Item.numBaseUnit),1)) * OKI.numQtyItemsReq,
					ItemDetails.numQtyItemsReq,
					ItemDetails.numUOMId,
					0,
					ISNULL(Item.monAverageCost,0)
				FROM
					(
						SELECT 
							t1.numItemCode,
							t1.numWarehouseItemID,
							SUBSTRING(
										SUBSTRING(items,0,CHARINDEX('-',items)),
										0,
										CHARINDEX('#',SUBSTRING(items,0,CHARINDEX('-',items)))
									  ) AS numKitItemID,
							SUBSTRING(
										SUBSTRING(items,0,CHARINDEX('-',items)),
										CHARINDEX('#',SUBSTRING(items,0,CHARINDEX('-',items))) + 1, 
										LEN(SUBSTRING(items,0,CHARINDEX('-',items))) - CHARINDEX('#',SUBSTRING(items,0,CHARINDEX('-',items)))
									  ) AS numKitWarehouseItemID,
							SUBSTRING(
										SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)), 
										0, 
										CHARINDEX('#',SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)))
									  ) As numKitChildItemID,
							SUBSTRING(
										SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)), 
										CHARINDEX('#',SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items))) + 1, 
										LEN(SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items))) - CHARINDEX('#',SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)))
									  ) As numKitChildItemWarehouseItemID
						FROM 
							@TempKitConfiguration t1
						CROSS APPLY
						(
							SELECT
								*
							FROM 
								dbo.Split((SELECT KitChildItems FROM @TempKitConfiguration WHERE numItemCode=t1.numItemCode AND numWarehouseItemID=t1.numWarehouseItemID),',') 
						) as t2
					) TempChildItems
				INNER JOIN
					OpportunityKitItems OKI
				ON
					OKI.numOppId=@numOppId
					AND OKI.numChildItemID=TempChildItems.numKitItemID
					AND OKI.numWarehouseItemId=TempChildItems.numKitWarehouseItemID
				INNER JOIN
					OpportunityItems OI
				ON
					OI.numItemCode = TempChildItems.numItemCode
					AND OI.numWarehouseItmsID = TempChildItems.numWarehouseItemID
					AND OI.numoppitemtcode = OKI.numOppItemID
				INNER JOIN
					ItemDetails
				ON
					TempChildItems.numKitItemID = ItemDetails.numItemKitID
					AND TempChildItems.numKitChildItemID = ItemDetails.numChildItemID
					AND (TempChildItems.numKitChildItemWarehouseItemID = ItemDetails.numWareHouseItemId  OR ISNULL(TempChildItems.numKitChildItemWarehouseItemID,0) = 0)
				INNER JOIN
					Item 
				ON
					ItemDetails.numChildItemID = Item.numItemCode
			END

			-- UPDATE UOM OF OPPORTUNITY ITEMS HAVE NOT UOM SET WHILE EXPORTED FROM MARKETPLACE (MEANS TINTSOURCETYPE = 3)
			IF ISNULL(@tintSourceType,0) = 3
			BEGIN
				UPDATE 
					OI 
				SET 
					numUOMID = ISNULL(I.numSaleUnit,0), 
					numUnitHour = dbo.fn_UOMConversion(ISNULL(I.numSaleUnit,0),OI.numItemCode,@numDomainId,null) * OI.numUnitHour
				FROM 
					OpportunityItems OI
				JOIN 
					Item I 
				ON 
					OI.numItemCode = I.numItemCode
				WHERE 
					numOppId = @numOppID
					AND I.numDomainID = @numDomainId
			END	
                                     
			INSERT INTO OppWarehouseSerializedItem
			(
				numOppID,
				numWarehouseItmsDTLID,
				numOppItemID,
				numWarehouseItmsID
			)                      
			SELECT 
				@numOppID,
				X.numWarehouseItmsDTLID,
				X.numoppitemtCode,
				X.numWItmsID 
			FROM
			(                                                                          
				SELECT
					* 
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/SerialNo',2)                                                                          
				WITH                        
				(                                               
				numWarehouseItmsDTLID numeric(9),                      
				numoppitemtCode numeric(9),                      
				numWItmsID numeric(9)                                                     
				)
			)X                                    
                                   
			UPDATE 
				OppWarehouseSerializedItem                       
			SET 
				numOppItemID=X.numoppitemtCode                                
			FROM                       
			(
				SELECT 
					numWarehouseItmsDTLID AS DTLID,
					O.numoppitemtCode 
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/SerialNo',2)                                                                         
				WITH
				(
					numWarehouseItmsDTLID NUMERIC(9),numoppitemtCode NUMERIC(9),numWItmsID NUMERIC(9)
				)                                
				JOIN 
					OpportunityItems O 
				ON 
					O.numWarehouseItmsID=numWItmsID and O.numOppID=@numOppID
			)X                 
			WHERE 
				numOppID=@numOppID 
				AND numWarehouseItmsDTLID=X.DTLID      
    
			IF NOT (@tintCommitAllocation=2 AND @bitAllocateInventoryOnPickList=1)
			BEGIN
				EXEC USP_ManageSOWorkOrder @numDomainId,@numOppID,@strItems,@numUserCntID
			END
                     
			EXEC sp_xml_removedocument @hDocItem                                     
		END          
	END
	ELSE                                                                                                                          
	BEGIN

		DECLARE @tempAssignedTo AS NUMERIC(9) = NULL                                                    
		SELECT 
			@tintOppStatus = tintOppStatus,
			@tempAssignedTo=ISNULL(numAssignedTo,0) 
		FROM   
			OpportunityMaster 
		WHERE  
			numOppID = @numOppID

		IF @tempAssignedTo <> @numAssignedTo
		BEGIN
			IF (SELECT COUNT(*) FROM BizDocComission WHERE ISNULL(numOppID,0) = @numOppID AND ISNULL(bitCommisionPaid,0) = 1) > 0
			BEGIN
				RAISERROR('COMMISSION_PAID_AGAINST_BIZDOC',16,1)
			END
		END
 
		--REVERTING BACK THE WAREHOUSE ITEMS                  
		IF @tintOppStatus = 1 AND @tintCommitAllocation=1
		BEGIN               
			EXEC USP_RevertDetailsOpp @numOppID,0,@numUserCntID                  
		END                  

		IF LEN(ISNULL(@vcOppRefOrderNo,''))>0
		BEGIN
			UPDATE dbo.OpportunityBizDocs SET vcRefOrderNo=@vcOppRefOrderNo WHERE numOppId=@numOppID
		END
		
		IF EXISTS (SELECT 1 FROM dbo.OpportunityMaster WHERE numOppId = @numOppID AND tintOppType=1 AND isnull(tintOppStatus,0)=1 AND ISNULL(numStatus,0)!= @numStatus)
		BEGIN
			EXEC USP_ManageOpportunityAutomationQueue
					@numOppQueueID = 0, -- numeric(18, 0)
					@numDomainID = @numDomainID, -- numeric(18, 0)
					@numOppId = @numOppID, -- numeric(18, 0)
					@numOppBizDocsId = 0, -- numeric(18, 0)
					@numOrderStatus = @numStatus, -- numeric(18, 0)
					@numUserCntID = @numUserCntID, -- numeric(18, 0)
					@tintProcessStatus = 1, -- tinyint
					@tintMode = 1 -- TINYINT
		END 
						
		UPDATE   
			OpportunityMaster
		SET      
			vcPOppName = @vcPOppName,txtComments = ISNULL(@Comments,''),bitPublicFlag = @bitPublicFlag,numCampainID = @CampaignID,tintSource = @tintSource,tintSourceType=@tintSourceType,
			intPEstimatedCloseDate = @dtEstimatedCloseDate,numModifiedBy = @numUserCntID,bintModifiedDate = GETUTCDATE(),lngPConclAnalysis = @lngPConclAnalysis,monPAmount = @monPAmount,
			tintActive = @tintActive,numSalesOrPurType = @numSalesOrPurType,numStatus = @numStatus,vcOppRefOrderNo=@vcOppRefOrderNo,
			bintOppToOrder=(CASE WHEN @tintOppStatus=0 AND @DealStatus=1 THEN GETUTCDATE() ELSE bintOppToOrder END),bitDiscountType=@bitDiscountType,fltDiscount=@fltDiscount,
			bitBillingTerms=@bitBillingTerms,intBillingDays=@intBillingDays,bitInterestType=@bitInterestType,fltInterest=@fltInterest,tintTaxOperator=@tintTaxOperator,
			numDiscountAcntType=@numDiscountAcntType,vcCouponCode = @vcCouponCode,numPercentageComplete =@numPercentageComplete,bitUseShippersAccountNo = @bitUseShippersAccountNo,
			intUsedShippingCompany = @intUsedShippingCompany, numContactID=@numContactId,numShipmentMethod=@numShipmentMethod,numShippingService=@numShippingService,numPartner=@numPartner,numPartenerContact=@numPartenerContactId
		WHERE
			numOppId = @numOppID   
	   
		-- UPDATING IF ORGANIZATION IS ASSIGNED TO SOMEONE                                                      
		IF ( @tempAssignedTo <> @numAssignedTo AND @numAssignedTo <> '0' ) 
		BEGIN
			UPDATE
				OpportunityMaster 
			SET	
				numAssignedTo = @numAssignedTo, 
				numAssignedBy = @numUserCntID 
			WHERE 
				numOppId = @numOppID                                                    
		END                                                     
		ELSE IF(@numAssignedTo = 0) 
		BEGIN
			UPDATE 
				OpportunityMaster 
			SET 
				numAssignedTo = 0, 
				numAssignedBy = 0 
			WHERE
				numOppId = @numOppID
		END  
			                                                                    
		-- UPDATING OPP ITEMS
		IF CONVERT(VARCHAR(10),@strItems) <> '' AND @numOppID > 0                                                                         
		BEGIN
			EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems                                      
			   
			IF EXISTS 
			(
				SELECT 
					OI.numoppitemtCode
				FROM 
					OpportunityItems OI
				WHERE 
					numOppID=@numOppID 
					AND OI.numoppitemtCode NOT IN (SELECT numoppitemtCode FROM OPENXML (@hDocItem,'/NewDataSet/Item',2) WITH (numoppitemtCode NUMERIC(9)))
					AND ISNULL((SELECT COUNT(*) FROM OppWarehouseSerializedItem WHERE numOppId=@numOppID AND numOppItemID=OI.numoppitemtCode),0) > 0
			)
			BEGIN
				RAISERROR('SERIAL/LOT#_ASSIGNED_TO_ITEM_DELETED',16,1)
			END

			--DELETE CHILD ITEMS OF KIT WITHIN KIT
			DELETE FROM 
				OpportunityKitChildItems 
			WHERE 
				numOppID=@numOppID AND 
				numOppItemID NOT IN (SELECT ISNULL(numoppitemtCode,0) FROM OPENXML (@hDocItem,'/NewDataSet/Item',2) WITH (numoppitemtCode NUMERIC(9)))   

			--DELETE ITEMS
			DELETE FROM 
				OpportunityKitItems 
			WHERE 
				numOppID=@numOppID AND 
				numOppItemID NOT IN (SELECT ISNULL(numoppitemtCode,0) FROM OPENXML (@hDocItem,'/NewDataSet/Item',2) WITH (numoppitemtCode NUMERIC(9)))                      
			                            
			DELETE FROM 
				OpportunityItems 
			WHERE 
				numOppID=@numOppID 
				AND numoppitemtCode NOT IN (SELECT numoppitemtCode FROM OPENXML (@hDocItem,'/NewDataSet/Item',2) WITH (numoppitemtCode NUMERIC(9)))   
	          
			-- DELETE CORRESPONSING TIME AND EXPENSE ENTRIES OF DELETED ITEMS
			DELETE FROM
				TimeAndExpense
			WHERE
				numDomainID=@numDomainId
				AND numOppId=@numOppID
				AND numOppItemID NOT IN (SELECT numoppitemtCode FROM OPENXML (@hDocItem,'/NewDataSet/Item',2) WITH (numoppitemtCode NUMERIC(9))) 

			--INSERT NEW ADDED ITEMS                        
			INSERT INTO OpportunityItems
			(
				numOppId,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount,
				[vcItemName],[vcModelID],[vcManufacturer],vcPathForTImage,monVendorCost,numUOMId,bitWorkOrder,numVendorWareHouse,numToWarehouseItemID,numShipmentMethod,
				numSOVendorId,numProjectID,numProjectStageID,vcAttributes,numClassID,monAvgCost,bitItemPriceApprovalRequired,vcAttrValues,numPromotionID,bitPromotionTriggered
				,vcPromotionDetail,numSortOrder,numCost,vcNotes,vcInstruction,bintCompliationDate,numWOAssignedTo,ItemReleaseDate
			)
			SELECT 
				@numOppID,X.numItemCode,dbo.fn_UOMConversion(X.numUOM,numItemCode,@numDomainId,null) * x.numUnitHour,x.monPrice,x.monTotAmount,X.vcItemDesc,X.numWarehouseItmsID,
				X.ItemType,X.DropShip,X.bitDiscountType,X.fltDiscount,X.monTotAmtBefDiscount,X.vcItemName,(SELECT vcModelID FROM item WHERE numItemCode = X.numItemCode),
				(SELECT vcManufacturer FROM item WHERE numItemCode = X.numItemCode),
				(SELECT TOP 1 vcPathForTImage FROM dbo.ItemImages WHERE numItemCode = X.numItemCode AND BitDefault = 1 AND numDomainId = @numDomainId),
				(select isnull(VN.monCost,0) from Vendor VN INNER JOIN Item IT ON IT.numItemCode=VN.numItemCode and VN.numVendorID=IT.numVendorID where VN.numItemCode=X.numItemCode),
				X.numUOM,X.bitWorkOrder,X.numVendorWareHouse,X.numToWarehouseItemID,X.numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,Attributes,
				0,
				(SELECT (CASE WHEN ISNULL(I.bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(I.monAverageCost,0) END) FROM Item I WHERE I.numDomainId=@numDomainId AND I.numItemCode=X.numItemCode), X.bitItemPriceApprovalRequired, X.AttributeIDs,
				X.numPromotionID,X.bitPromotionTriggered,X.vcPromotionDetail,X.numSortOrder,ISNULL(X.numCost,0),ISNULL(vcVendorNotes,'')
				,X.vcInstruction,X.bintCompliationDate,X.numWOAssignedTo,@dtItemRelease
			FROM
			(
				SELECT 
					*
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=1]',2)                                                                          
				WITH  
				(                      
					numoppitemtCode numeric(9),numItemCode numeric(9),numUnitHour FLOAT,monPrice DECIMAL(30,16),monTotAmount DECIMAL(20,5),vcItemDesc varchar(1000),                                    
					numWarehouseItmsID numeric(9),numToWarehouseItemID numeric(9),Op_Flag tinyint,ItemType varchar(30),DropShip bit,bitDiscountType bit,
					fltDiscount DECIMAL(30,16),monTotAmtBefDiscount DECIMAL(20,5),vcItemName VARCHAR(300),numUOM NUMERIC(9),bitWorkOrder BIT,numVendorWareHouse NUMERIC(9),
					numShipmentMethod NUMERIC(9),numSOVendorId numeric(9),numProjectID numeric(9),numProjectStageID numeric(9),Attributes varchar(500), 
					bitItemPriceApprovalRequired BIT,AttributeIDs VARCHAR(500),numPromotionID NUMERIC(18,0),bitPromotionTriggered BIT,vcPromotionDetail VARCHAR(2000)
					,numSortOrder NUMERIC(18,0),numCost DECIMAL(20,5),vcVendorNotes VARCHAR(1000),vcInstruction VARCHAR(1000),bintCompliationDate DATETIME,numWOAssignedTo NUMERIC(18,0)
				)
			)X 
			
			-- UPDATE EXISTING CUSTOMER PART NO
			UPDATE
				CPN
			SET
				CPN.CustomerPartNo = ISNULL(TEMPCPN.CustomerPartNo,'')
			FROM
				CustomerPartNumber CPN
			INNER JOIN
			(
				SELECT 
					numItemCode
					,MIN(CustomerPartNo) AS CustomerPartNo
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item',2)                                                                          
				WITH                       
				(                                                                          
					CustomerPartNo VARCHAR(300), numItemCode NUMERIC(18,0)
				)
				GROUP BY
					numItemCode
			) AS TEMPCPN
			ON
				CPN.numCompanyId = @numCompany
				AND CPN.numItemCode = TEMPCPN.numItemCode
			WHERE
				CPN.numDomainID=@numDomainID
				AND CPN.numCompanyId = @numCompany
				AND LEN(ISNULL(TEMPCPN.CustomerPartNo,'')) > 0
				AND ISNULL(TEMPCPN.CustomerPartNo,'') <> '0'
			
			-- INSERT NEW CUSTOMER PART NO
			INSERT INTO CustomerPartNumber
			(
				numCompanyId, numDomainID, CustomerPartNo, numItemCode 
			)
			SELECT 
				@numCompany, @numDomainID, TEMPCPN.CustomerPartNo,TEMPCPN.numItemCode
			FROM
			(
				SELECT 
					numItemCode
					,MIN(CustomerPartNo) AS CustomerPartNo
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item',2)                                                                          
				WITH                       
				(                                                                          
					CustomerPartNo VARCHAR(300), numItemCode NUMERIC(18,0)
				)
				GROUP BY
					numItemCode
			) TEMPCPN 
			LEFT JOIN
				CustomerPartNumber CPN
			ON
				CPN.numCompanyId = @numCompany
				AND CPN.numItemCode = TEMPCPN.numItemCode
			WHERE
				CPN.CustomerPartNoID IS NULL
				AND LEN(ISNULL(TEMPCPN.CustomerPartNo,'')) > 0
				AND ISNULL(TEMPCPN.CustomerPartNo,'') <> '0'
			
			-- UPDATE ITEMS
			UPDATE 
				OpportunityItems                       
			SET 
				numItemCode=X.numItemCode,numOppId=@numOppID,numUnitHour=dbo.fn_UOMConversion(X.numUOM,X.numItemCode,@numDomainId,null) * x.numUnitHour,                      
				monPrice=x.monPrice,monTotAmount=x.monTotAmount,vcItemDesc=X.vcItemDesc,vcModelID=X.vcModelID,numWarehouseItmsID=X.numWarehouseItmsID,numToWarehouseItemID = X.numToWarehouseItemID,             
				bitDropShip=X.DropShip,bitDiscountType=X.bitDiscountType,fltDiscount=X.fltDiscount,monTotAmtBefDiscount=X.monTotAmtBefDiscount,vcItemName=X.vcItemName,
				numUOMId=numUOM,numVendorWareHouse=X.numVendorWareHouse,numShipmentMethod=X.numShipmentMethod,numSOVendorId=X.numSOVendorId,numProjectId=X.numProjectID,
				numProjectStageId=X.numProjectStageID,vcAttributes=X.Attributes,bitItemPriceApprovalRequired=X.bitItemPriceApprovalRequired,bitWorkOrder=X.bitWorkOrder,vcAttrValues=AttributeIDs,
				numPromotionID=X.numPromotionID,bitPromotionTriggered=X.bitPromotionTriggered,vcPromotionDetail=X.vcPromotionDetail,numSortOrder=X.numSortOrder,numCost=X.numCost,vcNotes=vcVendorNotes
			FROM 
			(
				SELECT 
					numoppitemtCode as numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,vcModelID,numWarehouseItmsID,numToWarehouseItemID,DropShip,bitDiscountType,
					fltDiscount,monTotAmtBefDiscount,vcItemName,numUOM,numVendorWareHouse,numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,Attributes,
					bitItemPriceApprovalRequired,bitWorkOrder,AttributeIDs,numPromotionID,bitPromotionTriggered,vcPromotionDetail,numSortOrder,ISNULL(numCost,0) AS numCost,ISNULL(vcVendorNotes,'') vcVendorNotes
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=2]',2)                                                                
				WITH  
				(                      
					numoppitemtCode NUMERIC(9),numItemCode NUMERIC(9),numUnitHour FLOAT,monPrice DECIMAL(30,16),monTotAmount DECIMAL(20,5),vcItemDesc VARCHAR(1000),vcModelID VARCHAR(300), numWarehouseItmsID NUMERIC(9), 
					numToWarehouseItemID NUMERIC(18,0),DropShip BIT,bitDiscountType BIT,fltDiscount DECIMAL(30,16),monTotAmtBefDiscount DECIMAL(20,5),vcItemName VARCHAR(300),numUOM NUMERIC(9),
					numVendorWareHouse NUMERIC(9),numShipmentMethod NUMERIC(9),numSOVendorId NUMERIC(9),numProjectID NUMERIC(9),numProjectStageID NUMERIC(9),Attributes VARCHAR(500),
					bitItemPriceApprovalRequired BIT, bitWorkOrder BIT,AttributeIDs VARCHAR(500),numPromotionID NUMERIC(18,0),bitPromotionTriggered BIT,vcPromotionDetail VARCHAR(2000),numSortOrder NUMERIC(18,0),numCost DECIMAL(20,5),vcVendorNotes VARCHAR(1000)                           
				)
			)X 
			WHERE 
				numoppitemtCode=X.numOppItemID AND numOppId=@numOppID

			--INSERT NEW ADDED KIT ITEMS                 
			INSERT INTO OpportunityKitItems                                                                          
			(
				numOppId,
				numOppItemID,
				numChildItemID,
				numWareHouseItemId,
				numQtyItemsReq,
				numQtyItemsReq_Orig,
				numUOMId,
				numQtyShipped,
				monAvgCost
			)
			SELECT
				@numOppId,
				OI.numoppitemtCode,
				ID.numChildItemID,
				ID.numWareHouseItemId,
				ID.numQtyItemsReq * OI.numUnitHour,
				ID.numQtyItemsReq,
				ID.numUOMId,
				0,
				ISNULL(I.monAverageCost,0)
			FROM 
				OpportunityItems OI 
			JOIN 
				ItemDetails ID 
			ON 
				OI.numItemCode=ID.numItemKitID
			INNER JOIN
				Item I
			ON
				ID.numChildItemID = I.numItemCode
			WHERE 
				OI.numOppId=@numOppId 
				AND OI.numoppitemtCode NOT IN (SELECT ISNULL(numOppItemID,0) FROM OpportunityKitItems OKI WHERE OKI.numOppId=@numOppID)  
				
			IF (SELECT COUNT(*) FROM OPENXML (@hDocItem,'/NewDataSet/Item[bitHasKitAsChild=''true'']',2)) > 0
			BEGIN
				--INSERT KIT ITEM HAS ANOTHER KIT AS CHILD THEN SAVE CONFIGURATION OF KIT
				DELETE FROM @TempKitConfiguration

				INSERT INTO 
					@TempKitConfiguration
				SELECT
					numItemCode,
					numWarehouseItmsID,
					KitChildItems
				FROM
					OPENXML (@hDocItem,'/NewDataSet/Item[bitHasKitAsChild=''true'']',2)
				WITH                       
				(
					numItemCode NUMERIC(9),numWarehouseItmsID NUMERIC(9), KitChildItems VARCHAR(MAX)
				) 

				INSERT INTO OpportunityKitChildItems
				(
					numOppID,
					numOppItemID,
					numOppChildItemID,
					numItemID,
					numWareHouseItemId,
					numQtyItemsReq,
					numQtyItemsReq_Orig,
					numUOMId,
					numQtyShipped,
					monAvgCost
				)
				SELECT 
					@numOppID,
					OKI.numOppItemID,
					OKI.numOppChildItemID,
					ItemDetails.numChildItemID,
					ItemDetails.numWareHouseItemId,
					ItemDetails.numQtyItemsReq * OKI.numQtyItemsReq,
					ItemDetails.numQtyItemsReq,
					ItemDetails.numUOMId,
					0,
					ISNULL(Item.monAverageCost,0)
				FROM
					(
						SELECT 
							t1.numItemCode,
							t1.numWarehouseItemID,
							SUBSTRING(
										SUBSTRING(items,0,CHARINDEX('-',items)),
										0,
										CHARINDEX('#',SUBSTRING(items,0,CHARINDEX('-',items)))
									  ) AS numKitItemID,
							SUBSTRING(
										SUBSTRING(items,0,CHARINDEX('-',items)),
										CHARINDEX('#',SUBSTRING(items,0,CHARINDEX('-',items))) + 1, 
										LEN(SUBSTRING(items,0,CHARINDEX('-',items))) - CHARINDEX('#',SUBSTRING(items,0,CHARINDEX('-',items)))
									  ) AS numKitWarehouseItemID,
							SUBSTRING(
										SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)), 
										0, 
										CHARINDEX('#',SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)))
									  ) As numKitChildItemID,
							SUBSTRING(
										SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)), 
										CHARINDEX('#',SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items))) + 1, 
										LEN(SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items))) - CHARINDEX('#',SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)))
									  ) As numKitChildItemWarehouseItemID
						FROM 
							@TempKitConfiguration t1
						CROSS APPLY
						(
							SELECT
								*
							FROM 
								dbo.Split((SELECT KitChildItems FROM @TempKitConfiguration WHERE numItemCode=t1.numItemCode AND numWarehouseItemID=t1.numWarehouseItemID),',') 
						) as t2
					) TempChildItems
				INNER JOIN
					OpportunityKitItems OKI
				ON
					OKI.numOppId=@numOppId
					AND OKI.numChildItemID=TempChildItems.numKitItemID
					AND OKI.numWarehouseItemId=TempChildItems.numKitWarehouseItemID
				INNER JOIN
					OpportunityItems OI
				ON
					OI.numItemCode = TempChildItems.numItemCode
					AND OI.numWarehouseItmsID = TempChildItems.numWarehouseItemID
					AND OI.numoppitemtcode = OKI.numOppItemID
				INNER JOIN
					ItemDetails
				ON
					TempChildItems.numKitItemID = ItemDetails.numItemKitID
					AND TempChildItems.numKitChildItemID = ItemDetails.numChildItemID
					AND (TempChildItems.numKitChildItemWarehouseItemID = ItemDetails.numWareHouseItemId  OR ISNULL(TempChildItems.numKitChildItemWarehouseItemID,0) = 0)
				INNER JOIN
					Item 
				ON
					ItemDetails.numChildItemID = Item.numItemCode
			END                                  
			
			--UPDATE KIT ITEMS                 
			UPDATE 
				OKI 
			SET 
				numQtyItemsReq = numQtyItemsReq_Orig * OI.numUnitHour
			FROM 
				OpportunityKitItems OKI 
			JOIN 
				OpportunityItems OI 
			ON 
				OKI.numOppItemID=OI.numoppitemtCode 
				AND OKI.numOppId=OI.numOppId  
			WHERE 
				OI.numOppId=@numOppId 

			-- UPDATE CHILD ITEMS OF KIT WITHIN KIT
			UPDATE
				OKCI
			SET
				OKCI.numQtyItemsReq = (OKI.numQtyItemsReq * OKCI.numQtyItemsReq_Orig)
			FROM
				OpportunityKitChildItems OKCI
			INNER JOIN
				OpportunityKitItems OKI
			ON
				OKCI.numOppID = @numOppId
				AND OKCI.numOppItemID = OKI.numOppItemID
				AND OKCI.numOppChildItemID = OKI.numOppChildItemID                     
			                                    
			IF NOT (@tintCommitAllocation=2 AND @bitAllocateInventoryOnPickList=1)
			BEGIN
				IF @tintCommitAllocation = 2
				BEGIN
					DELETE FROM WorkOrderDetails WHERE numWOId IN (SELECT numWOId FROM WorkOrder WHERE numOppId=@numOppID AND numWOStatus <> 23184)
					DELETE FROM WorkOrder WHERE numOppId=@numOppID AND numWOStatus <> 23184
				END

				EXEC USP_ManageSOWorkOrder @numDomainId,@numOppID,@strItems,@numUserCntID
			END
			   		                                                                      
			EXEC sp_xml_removedocument @hDocItem                                               
		END     

		IF @tintOppType=1 AND @tintOppStatus=1 --Sales Order
		BEGIN
			IF EXISTS (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId=@numOppID AND ISNULL(numWarehouseItmsID,0) > 0 AND ISNULL(bitDropShip,0) = 0 AND (ISNULL(numUnitHour,0) - ISNULL(numQtyShipped,0)) < 0)
			BEGIN
				RAISERROR('EDITED_ITEMS_QTY_IS_LESS_THEN_SHIPPED_QTY',16,1)
				RETURN
			END

			IF EXISTS (SELECT numoppitemtCode FROM OpportunityItems INNER JOIN WorkOrder ON OpportunityItems.numOppID=WorkOrder.numOppID AND OpportunityItems.numoppitemtCode=WorkOrder.numOppItemID AND ISNULL(WorkOrder.numParentWOID,0)=0 AND WorkOrder.numWOStatus=23184 AND ISNULL(OpportunityItems.numUnitHour,0) > ISNULL(WorkOrder.numQtyItemsReq,0) WHERE OpportunityItems.numOppID=@numOppID)
			BEGIN
				RAISERROR('EDITED_ITEMS_QTY_CAN_NOT_BE_GREATER_THEN_COMPLETED_WORK_ORDER',16,1)
				RETURN
			END
		END
		ELSE IF @tintOppType=2 AND @tintOppStatus=1 --Purchase Order
		BEGIN
			IF EXISTS (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId=@numOppID AND ISNULL(numWarehouseItmsID,0) > 0 AND ISNULL(bitDropShip,0) = 0 AND (ISNULL(numUnitHour,0) - ISNULL(numUnitHourReceived,0)) < 0)
			BEGIN
				RAISERROR('EDITED_ITEMS_QTY_IS_LESS_THEN_RECEIVED_QTY',16,1)
				RETURN
			END
		END

		--FOLLOWING DELETE STATEMENT IS COMMENTED BECUASE NOW WE ARE NOT ALLOWING USER TO EIDT SERIAL/LOT ITEM UNTILL USER REMOVED ASSIGNED SERIAL/LOT NUMBERS
	    --Delete previously added serial lot number added for opportunity
		--DELETE FROM OppWarehouseSerializedItem WHERE numOppID = @numOppID                                                         
	END

	IF @tintOppStatus = 1
	BEGIN
		IF (SELECT COUNT(*) FROM OpportunityItems WHERE numOppId=@numOppID AND ISNULL(numWarehouseItmsID,0) > 0 AND ISNULL(bitDropShip,0)=0) <>
			(SELECT COUNT(*) FROM OpportunityItems INNER JOIN WareHouseItems ON OpportunityItems.numWarehouseItmsID=WareHouseItems.numWareHouseItemID AND OpportunityItems.numItemCode=WareHouseItems.numItemID WHERE numOppId=@numOppID AND OpportunityItems.numWarehouseItmsID > 0 AND ISNULL(bitDropShip,0)=0)
		BEGIN
			RAISERROR('INVALID_ITEM_WAREHOUSES',16,1)
			RETURN
		END
	END

	IF(SELECT 
			COUNT(*) 
		FROM 
			OpportunityBizDocItems OBDI 
		INNER JOIN 
			OpportunityBizDocs OBD 
		ON 
			OBDI.numOppBizDocID=OBD.numOppBizDocsID 
		WHERE 
			OBD.numOppId=@numOppID AND OBDI.numOppItemID NOT IN (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId=@numOppID)) > 0
	BEGIN
		RAISERROR('ITEM_USED_IN_BIZDOC',16,1)
		RETURN
	END

	IF @tintOppType=1 AND @tintCommitAllocation=2 AND @bitAllocateInventoryOnPickList=1
	BEGIN
		IF(SELECT 
				COUNT(*)
			FROM
			(
				SELECT
					numOppItemID
					,SUM(numUnitHour) PackingSlipUnits
				FROM 
					OpportunityBizDocItems OBDI 
				INNER JOIN 
					OpportunityBizDocs OBD 
				ON 
					OBDI.numOppBizDocID=OBD.numOppBizDocsID 
				WHERE 
					OBD.numOppId=@numOppID
					AND OBD.numBizDocId=29397 --Picking Slip
				GROUP BY
					numOppItemID
			) AS TEMP
			INNER JOIN
				OpportunityItems OI
			ON
				TEMP.numOppItemID = OI.numoppitemtCode
			WHERE
				OI.numUnitHour < PackingSlipUnits
			) > 0
		BEGIN
			RAISERROR('ORDERED_QUANTITY_CAN_NOT_BE_LESS_THEN_PACKING_SLIP_QTY',16,1)
			RETURN
		END
	END
	
	-- Set item release date to today
	If @tintOppType = 1
	BEGIN
		INSERT INTO OpportunityItemsReleaseDates
		(
			numDomainID
			,numOppID
			,numOppItemID
			,dtReleaseDate
		)
		SELECT
			@numDomainId
			,OI.numOppId
			,OI.numoppitemtCode
			,ISNULL(OI.ItemReleaseDate,@dtItemRelease)
		FROM
			OpportunityItems OI
		LEFT JOIN
			OpportunityItemsReleaseDates OIRD
		ON
			OI.numOppId=OIRD.numOppID
			AND OI.numoppitemtCode = OIRD.numOppItemID
		WHERE
			OI.numOppId=@numOppID
			AND OIRD.numID IS NULL
	END



	SET @TotalAmount=0                                                           
	SELECT @TotalAmount = SUM(monTotAmount) FROM OpportunityItems WHERE numOppId=@numOppID                                                                          
  
	UPDATE OpportunityMaster SET monPamount=@TotalAmount WHERE numOppId=@numOppID

/*Added by chintan - While creating SO from PO it wasn't puting Item Qty to Allocation. this segment was being called from USP_OppSaveMilestone which No longer in use after changes anoop did. */     
--Updating the warehouse items              
            
			
			DECLARE @bitViolatePrice BIT
			DECLARE @intPendingApprovePending INT

			SET @bitIsInitalSalesOrder=(SELECT bitIsInitalSalesOrder FROM OpportunityMaster WHERE numOppId=@numOppID AND numDomainId=@numDomainID)
			--SET @bitViolatePrice=(SELECT bitMarginPriceViolated FROM Domain WHERE numDomainId=@numDomainID)
			IF EXISTS (SELECT numApprovalRuleID FROM ApprovalProcessItemsClassification WHERE numDomainId=@numDomainID AND ISNULL(bitMarginPriceViolated,0) = 1)
			BEGIN
				SET @bitViolatePrice = 1
			END
			
			IF(@bitViolatePrice=1 AND @bitIsInitalSalesOrder=1 AND @tintClickBtn = 1)
			BEGIN
				UPDATE OpportunityMaster SET tintOppStatus=@DealStatus WHERE numOppId=@numOppID
			END
			ELSE
			BEGIN
				UPDATE OpportunityMaster SET tintOppStatus = @DealStatus WHERE numOppId=@numOppID
				DECLARE @tintShipped AS TINYINT               
				SELECT @tintOppStatus=tintOppStatus,@tintOppType=tintOppType,@tintShipped=tintShipped from OpportunityMaster where numOppID=@numOppID               
				
				IF @tintOppStatus=1 AND @tintCommitAllocation=1             
				BEGIN         
					EXEC USP_UpdatingInventoryonCloseDeal @numOppID,@numUserCntID
				END  
				declare @tintCRMType as numeric(9)        
				declare @AccountClosingDate as datetime        

				select @numDivisionID=numDivisionID,@AccountClosingDate=bintAccountClosingDate from OpportunityMaster where numOppID=@numOppID         
				select @tintCRMType=tintCRMType from divisionmaster where numDivisionID =@numDivisionID 

				--When deal is won                                         
				IF @DealStatus = 1 
				BEGIN
					IF @AccountClosingDate IS NULL                   
					BEGIN
						UPDATE OpportunityMaster SET bintAccountClosingDate=GETUTCDATE() WHERE numOppID=@numOppID              
					END

					-- Promote Lead to Account when Sales/Purchase Order is created against it
					IF @tintCRMType=0 --Lead & Order
					BEGIN
						UPDATE 
							divisionmaster 
						SET 
							tintCRMType=2
							,bintLeadProm=getutcdate()
							,bintLeadPromBy=@numUserCntID
							,bintProsProm=getutcdate()
							,bintProsPromBy=@numUserCntID        
						WHERE 
							numDivisionID=@numDivisionID        
					END
					--Promote Prospect to Account
					ELSE IF @tintCRMType=1 
					BEGIN
						UPDATE 
							divisionmaster 
						SET 
							tintCRMType=2
							,bintProsProm=getutcdate()
							,bintProsPromBy=@numUserCntID        
						WHERE 
							numDivisionID=@numDivisionID        
					END
				END         
				--When Deal is Lost
				ELSE IF @DealStatus = 2
				BEGIN
					SELECT @AccountClosingDate=bintAccountClosingDate FROM OpportunityMaster WHERE numOppID=@numOppID         
					
					IF @AccountClosingDate IS NULL                   
					BEGIN
						UPDATE OpportunityMaster SET bintAccountClosingDate=GETUTCDATE() WHERE numOppID=@numOppID
					END
				END 
			END

			/*Added by chintan -  Biz usually automatically promotes the record from Lead/Prospect to Account When a Sales Opp/Order is made against it*/  
			-- Promote Lead to Prospect when Sales/Purchase Opp is created against it
			if @tintCRMType=0 AND @DealStatus = 0 --Lead & Open Opp
			begin        
				update divisionmaster set tintCRMType=1,bintLeadProm=getutcdate(),bintLeadPromBy=@numUserCntID,bintProsProm=getutcdate(),bintProsPromBy=@numUserCntID        
				where numDivisionID=@numDivisionID        
			end        

--Add/Update Address Details
DECLARE @vcStreet  varchar(100),@vcCity  varchar(50),@vcPostalCode  varchar(15),@vcCompanyName  varchar(100),@vcAddressName VARCHAR(50), @vcAltContact VARCHAR(200)      
DECLARE @numState  numeric(9),@numCountry  numeric(9), @numCompanyId  numeric(9), @numContact NUMERIC(18,0)
DECLARE @bitIsPrimary BIT, @bitAltContact BIT;
DECLARE @tintAddressOf TINYINT

select  @vcCompanyName=vcCompanyName,@numCompanyId=div.numCompanyID from CompanyInfo Com                            
join divisionMaster Div                            
on div.numCompanyID=com.numCompanyID                            
where div.numdivisionID=@numDivisionId

--Bill Address
IF @numBillAddressId>0
BEGIN
	SELECT 
		@vcStreet=ISNULL(vcStreet,'')
		,@vcCity=ISNULL(vcCity,'')
		,@vcPostalCode=ISNULL(vcPostalCode,'')
		,@numState=ISNULL(numState,0)
		,@numCountry=ISNULL(numCountry,0)
		,@bitIsPrimary=bitIsPrimary
		,@vcAddressName=vcAddressName
		,@tintAddressOf=ISNULL(tintAddressOf,0)
		,@numContact=numContact
		,@bitAltContact=bitAltContact
		,@vcAltContact=vcAltContact
    FROM 
		dbo.AddressDetails 
	WHERE 
		numDomainID = @numDomainID 
		AND numAddressID = @numBillAddressId

	IF @tintAddressOf = 2 AND @tintOppType = 2
	BEGIN
		SELECT
			@vcCompanyName = ISNULL(vcCompanyName,'')
			,@numCompanyId = ISNULL(CompanyInfo.numCompanyId,0)
		FROM
			AddressDetails
		INNER JOIN
			DivisionMaster 
		ON
			AddressDetails.numRecordID = DivisionMaster.numDivisionID
		INNER JOIN
			CompanyInfo
		ON
			DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
		WHERE 
			AddressDetails.numDomainID = @numDomainID 
			AND numAddressID = @numBillAddressId
	END

  	EXEC dbo.USP_UpdateOppAddress
	@numOppID = @numOppID, --  numeric(9, 0)
	@byteMode = 0, --  tinyint
	@vcStreet = @vcStreet, --  varchar(100)
	@vcCity = @vcCity, --  varchar(50)
	@vcPostalCode = @vcPostalCode, --  varchar(15)
	@numState = @numState, --  numeric(9, 0)
	@numCountry = @numCountry, --  numeric(9, 0)
	@vcCompanyName = @vcCompanyName, --  varchar(100)
	@numCompanyId = 0, --  numeric(9, 0)
	@vcAddressName = @vcAddressName,
	@bitCalledFromProcedure = 1,
	@numContact = @numContact,
	@bitAltContact = @bitAltContact,
	@vcAltContact = @vcAltContact
END
  
  
--Ship Address
IF @intUsedShippingCompany = 92 -- WILL CALL (PICK UP FROM Warehouse)
BEGIN
	IF EXISTS (SELECT numWareHouseID FROM Warehouses WHERE Warehouses.numDomainID = @numDomainID AND numWareHouseID = @numWillCallWarehouseID AND ISNULL(numAddressID,0) > 0)
	BEGIN
		SELECT  
			@vcStreet=ISNULL(vcStreet,'')
			,@vcCity=ISNULL(vcCity,'')
			,@vcPostalCode=ISNULL(AddressDetails.vcPostalCode,'')
			,@numState=ISNULL(numState,0)
			,@numCountry=ISNULL(numCountry,0)
			,@vcAddressName=vcAddressName
		FROM 
			dbo.Warehouses 
		INNER JOIN
			AddressDetails
		ON
			Warehouses.numAddressID = AddressDetails.numAddressID
		WHERE 
			Warehouses.numDomainID = @numDomainID 
			AND numWareHouseID = @numWillCallWarehouseID
	END
	ELSE
	BEGIN
	SELECT  
		@vcStreet=ISNULL(vcWStreet,'')
		,@vcCity=ISNULL(vcWCity,'')
		,@vcPostalCode=ISNULL(vcWPinCode,'')
		,@numState=ISNULL(numWState,0)
		,@numCountry=ISNULL(numWCountry,0)
		,@vcAddressName=''
	FROM 
		dbo.Warehouses 
	WHERE 
		numDomainID = @numDomainID 
		AND numWareHouseID = @numWillCallWarehouseID
	END

	UPDATE OpportunityMaster SET tintShipToType=2 WHERE numOppId=@numOppID
 
  	EXEC dbo.USP_UpdateOppAddress
	@numOppID = @numOppID, --  numeric(9, 0)
	@byteMode = 1, --  tinyint
	@vcStreet = @vcStreet, --  varchar(100)
	@vcCity = @vcCity, --  varchar(50)
	@vcPostalCode = @vcPostalCode, --  varchar(15)
	@numState = @numState, --  numeric(9, 0)
	@numCountry = @numCountry, --  numeric(9, 0)
	@vcCompanyName = '', --  varchar(100)
	@numCompanyId =0, --  numeric(9, 0)
	@vcAddressName = @vcAddressName,
	@bitCalledFromProcedure = 1,
	@numContact = 0,
	@bitAltContact = 0,
	@vcAltContact = ''
END
ELSE IF @numShipAddressId>0
BEGIN
	SELECT
		@vcStreet=ISNULL(vcStreet,'')
		,@vcCity=ISNULL(vcCity,'')
		,@vcPostalCode=ISNULL(vcPostalCode,'')
		,@numState=ISNULL(numState,0)
		,@numCountry=ISNULL(numCountry,0)
		,@bitIsPrimary=bitIsPrimary
		,@vcAddressName=vcAddressName
		,@tintAddressOf=ISNULL(tintAddressOf,0)
		,@numContact=numContact
		,@bitAltContact=bitAltContact
		,@vcAltContact=vcAltContact
    FROM  
		dbo.AddressDetails 
	WHERE 
		numDomainID = @numDomainID 
		AND numAddressID = @numShipAddressId
 

	IF @tintAddressOf = 2 AND @tintOppType = 2
	BEGIN
		SELECT
			@vcCompanyName = ISNULL(vcCompanyName,'')
			,@numCompanyId = ISNULL(CompanyInfo.numCompanyId,0)
		FROM
			AddressDetails
		INNER JOIN
			DivisionMaster 
		ON
			AddressDetails.numRecordID = DivisionMaster.numDivisionID
		INNER JOIN
			CompanyInfo
		ON
			DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
		WHERE 
			AddressDetails.numDomainID = @numDomainID 
			AND numAddressID = @numShipAddressId
	END


  	EXEC dbo.USP_UpdateOppAddress
	@numOppID = @numOppID, --  numeric(9, 0)
	@byteMode = 1, --  tinyint
	@vcStreet = @vcStreet, --  varchar(100)
	@vcCity = @vcCity, --  varchar(50)
	@vcPostalCode = @vcPostalCode, --  varchar(15)
	@numState = @numState, --  numeric(9, 0)
	@numCountry = @numCountry, --  numeric(9, 0)
	@vcCompanyName = @vcCompanyName, --  varchar(100)
	@numCompanyId =@numCompanyId, --  numeric(9, 0)
	@vcAddressName = @vcAddressName,
	@bitCalledFromProcedure = 1,
	@numContact = @numContact,
	@bitAltContact = @bitAltContact,
	@vcAltContact = @vcAltContact
END

-- IMPORTANT:KEEP THIS BELOW ADDRESS UPDATE LOGIC
-- GET TAX APPLICABLE TO DIVISION WHEN ORDER IS CREATED AFTER THAT NOT NEED TO CHANGE IT
IF @bitNewOrder = 1
BEGIN
	--INSERT TAX FOR DIVISION   
	IF @tintOppType=1 OR (@tintOppType=2 and (select isnull(bitPurchaseTaxCredit,0) from Domain where numDomainId=@numDomainId)=1)
	BEGIN
		INSERT INTO dbo.OpportunityMasterTaxItems 
		(
			numOppId,
			numTaxItemID,
			fltPercentage,
			tintTaxType,
			numTaxID
		) 
		SELECT 
			@numOppID,
			TI.numTaxItemID,
			TEMPTax.decTaxValue,
			TEMPTax.tintTaxType,
			0
		FROM 
			TaxItems TI 
		JOIN 
			DivisionTaxTypes DTT 
		ON 
			TI.numTaxItemID = DTT.numTaxItemID
		CROSS APPLY
		(
			SELECT
				decTaxValue,
				tintTaxType
			FROM
				dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,TI.numTaxItemID,@numOppId,0,NULL)
		) AS TEMPTax
		WHERE 
			DTT.numDivisionID=@numDivisionId 
			AND DTT.bitApplicable=1
		UNION 
		SELECT 
			@numOppID,
			0,
			TEMPTax.decTaxValue,
			TEMPTax.tintTaxType,
			0
		FROM 
			dbo.DivisionMaster 
		CROSS APPLY
		(
			SELECT
				decTaxValue,
				tintTaxType
			FROM
				dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,0,@numOppId,1,NULL)
		) AS TEMPTax
		WHERE 
			bitNoTax=0 
			AND numDivisionID=@numDivisionID	
		UNION 
		SELECT
			@numOppId
			,1
			,decTaxValue
			,tintTaxType
			,numTaxID
		FROM
			dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,1,@numOppId,1,NULL)		
	END	
END

--INSERT TAX FOR DIVISION   
IF @tintOppType=1 OR (@tintOppType=2 and (select isnull(bitPurchaseTaxCredit,0) from Domain where numDomainId=@numDomainId)=1)
BEGIN
--Delete Tax for Opportunity Items if item deleted 
DELETE FROM OpportunityItemsTaxItems WHERE numOppId=@numOppId AND numOppItemID NOT IN (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId=@numOppID)

--Insert Tax for Opportunity Items
INSERT INTO dbo.OpportunityItemsTaxItems 
(
	numOppId,
	numOppItemID,
	numTaxItemID,
	numTaxID
) 
SELECT 
	@numOppId,
	OI.numoppitemtCode,
	TI.numTaxItemID,
	0
FROM 
	dbo.OpportunityItems OI 
JOIN 
	dbo.ItemTax IT 
ON 
	OI.numItemCode=IT.numItemCode 
JOIN
	TaxItems TI 
ON 
	TI.numTaxItemID = IT.numTaxItemID 
WHERE 
	OI.numOppId=@numOppID 
	AND IT.bitApplicable=1 
	AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)
UNION
SELECT 
	@numOppId,
	OI.numoppitemtCode,
	0,
	0
FROM 
	dbo.OpportunityItems OI 
JOIN 
	dbo.Item I 
ON 
	OI.numItemCode=I.numItemCode
WHERE 
	OI.numOppId=@numOppID 
	AND I.bitTaxable=1 
	AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)
UNION
SELECT
	@numOppId,
	OI.numoppitemtCode,
	1,
	TD.numTaxID
FROM
	dbo.OpportunityItems OI 
INNER JOIN
	ItemTax IT
ON
	IT.numItemCode = OI.numItemCode
INNER JOIN
	TaxDetails TD
ON
	TD.numTaxID = IT.numTaxID
	AND TD.numDomainId = @numDomainId
WHERE
	OI.numOppId = @numOppID
	AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)

END
  
 UPDATE OpportunityMaster SET monDealAmount=dbo.GetDealAmount(@numOppID,getdate(),0) WHERE numOppId=@numOppID

	IF @tintOppType=1 AND @tintOppStatus=1 
	BEGIN
		EXEC USP_SalesFulfillmentQueue_Save @numDomainID,@numUserCntID,@numOppID,@numStatus
	END
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SET @numOppID = 0

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON            
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityBizDocs_CheckIfItemsPendingToAddByBizDocID')
DROP PROCEDURE USP_OpportunityBizDocs_CheckIfItemsPendingToAddByBizDocID
GO
CREATE PROCEDURE [dbo].[USP_OpportunityBizDocs_CheckIfItemsPendingToAddByBizDocID]              
	@numOppID NUMERIC(18,0)
	,@numBizDocID NUMERIC(18,0)
AS             
BEGIN
	IF (SELECT 
			COUNT(*) 
		FROM 
		(
			SELECT
				OI.numoppitemtCode,
				ISNULL(OI.numUnitHour,0) AS OrderedQty,
				ISNULL(TempInvoice.AddedQty,0) AS AddedQty
			FROM
				OpportunityItems OI
			INNER JOIN
				Item I
			ON
				OI.numItemCode = I.numItemCode
			OUTER APPLY
			(
				SELECT
					SUM(OpportunityBizDocItems.numUnitHour) AS AddedQty
				FROM
					OpportunityBizDocs
				INNER JOIN
					OpportunityBizDocItems 
				ON
					OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
				WHERE
					OpportunityBizDocs.numOppId = @numOppID
					AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
					AND OpportunityBizDocs.numBizDocId = @numBizDocID
			) AS TempInvoice
			WHERE
				OI.numOppID = @numOppID
		) X
		WHERE
			X.OrderedQty <> X.AddedQty) > 0
	BEGIN
		SELECT 1
	END
	ELSE
	BEGIN
		SELECT 0
	END
END

	
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityBizDocs_GetUnPaidInvoices')
DROP PROCEDURE USP_OpportunityBizDocs_GetUnPaidInvoices
GO
CREATE PROCEDURE [dbo].[USP_OpportunityBizDocs_GetUnPaidInvoices]
(                        
 @numDomainID AS NUMERIC(18,0),
 @numOppId AS NUMERIC(18,0)
)                        
AS 
BEGIN
	SELECT
		numOppBizDocsId,
		ISNULL(OpportunityBizDocs.monDealAmount,0) AS monDealAmount,
		ISNULL(monAmountPaid,0) AS monAmountPaid,
		ISNULL(OpportunityBizDocs.monDealAmount,0) -ISNULL(OpportunityBizDocs.monAmountPaid,0) AS monAmountToPay,
		ISNULL(OpportunityMaster.numCurrencyID,0) AS numCurrencyID,
		OpportunityMaster.numDivisionId
	FROM
		OpportunityBizDocs
	INNER JOIN
		OpportunityMaster
	ON
		OpportunityBizDocs.numOppId = OpportunityMaster.numOppId
	WHERE
		OpportunityMaster.numDomainId=@numDomainID
		AND OpportunityBizDocs.numOppId=@numOppId
		AND numBizDocId = 287
		AND bitAuthoritativeBizDocs=1
		AND ISNULL(OpportunityBizDocs.monDealAmount,0) -ISNULL(OpportunityBizDocs.monAmountPaid,0) > 0
END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetCompanyDetailsFrom850')
DROP PROCEDURE dbo.USP_GetCompanyDetailsFrom850
GO

CREATE PROCEDURE [dbo].[USP_GetCompanyDetailsFrom850]
	@numDivisionId NUMERIC(18,0)
AS 
BEGIN

	DECLARE @CountCmp INT
	DECLARE @numCompanyID NUMERIC(18,0)

	SET @CountCmp = (SELECT COUNT(numCompanyID) FROM DivisionMaster DM WHERE DM.numDivisionID = @numDivisionId)


	PRINT @CountCmp
	IF @CountCmp = 1

	SET @numCompanyID = (SELECT numCompanyId FROM DivisionMaster DM WHERE DM.numDivisionID = @numDivisionId)

	IF(@numCompanyID) > 0

	BEGIN

		SELECT ACI.numContactID
			,DM.numDivisionID
			,ISNULL(DM.tintInbound850PickItem,0) AS tintInbound850PickItem
			,CI.numCompanyId
			,CI.vcCompanyName

		FROM AdditionalContactsInformation ACI 
		INNER JOIN DivisionMaster DM
		ON ACI.numDivisionId = DM.numDivisionID
		INNER JOIN CompanyInfo CI
		ON DM.numCompanyID = CI.numCompanyId

		WHERE CI.numCompanyId = @numCompanyID AND ISNULL(ACI.bitPrimaryContact,0)=1

	END
	

END

GO


