/******************************************************************
Project: Release 12.4 Date: 20.AUG.2019
Comments: STORE PROCEDURES
*******************************************************************/

/****** Object:  StoredProcedure [dbo].[USP_AdvancedSearch]    Script Date: 05/07/2009 22:04:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                              
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_advancedsearch')
DROP PROCEDURE usp_advancedsearch
GO
CREATE PROCEDURE [dbo].[USP_AdvancedSearch] 
@WhereCondition as varchar(4000)='',                            
@AreasofInt as varchar(100)='',                              
@tintUserRightType as tinyint,                              
@numDomainID as numeric(9)=0,                              
@numUserCntID as numeric(9)=0,                                                        
@CurrentPage int,                                                                    
@PageSize int,                                                                    
@TotRecs int output,                                                                                                             
@columnSortOrder as Varchar(10),                              
@ColumnSearch as varchar(100)='',                           
@ColumnName as varchar(20)='',                             
@GetAll as bit,                              
@SortCharacter as char(1),                        
@SortColumnName as varchar(100)='',        
@bitMassUpdate as bit,        
@LookTable as varchar(200)='' ,        
@strMassUpdate as varchar(MAX)='',
@vcRegularSearchCriteria varchar(MAX)='',
@vcCustomSearchCriteria varchar(MAX)='',
@ClientTimeZoneOffset as int,
@vcDisplayColumns VARCHAR(MAX)
as                              
BEGIN
	DECLARE @tintOrder  AS TINYINT
	DECLARE @vcFormFieldName  AS VARCHAR(50)
	DECLARE @vcListItemType  AS VARCHAR(3)
	DECLARE @vcListItemType1  AS VARCHAR(3)
	DECLARE @vcAssociatedControlType VARCHAR(20)
	DECLARE @numListID  AS NUMERIC(9)
	DECLARE @vcDbColumnName VARCHAR(50)
	DECLARE @vcLookBackTableName VARCHAR(50)
	DECLARE @WCondition VARCHAR(1000)
	DECLARE @bitContactAddressJoinAdded AS BIT;
	DECLARE @bitBillAddressJoinAdded AS BIT;
	DECLARE @bitShipAddressJoinAdded AS BIT;
	DECLARE @vcOrigDbColumnName as varchar(50)
	DECLARE @bitAllowSorting AS CHAR(1)
	DECLARE @bitAllowEdit AS CHAR(1)
	DECLARE @bitCustomField AS BIT;
	DECLARE @ListRelID AS NUMERIC(9)
	DECLARE @intColumnWidth INT
	DECLARE @bitAllowFiltering AS BIT;
	DECLARE @vcfieldatatype CHAR(1)
	DECLARE @zipCodeSearch AS VARCHAR(100) = ''


	IF CHARINDEX('$SZ$', @WhereCondition)>0
	BEGIN
		SELECT @zipCodeSearch=SUBSTRING(@WhereCondition, CHARINDEX('$SZ$', @WhereCondition)+4,CHARINDEX('$EZ$', @WhereCondition) - CHARINDEX('$SZ$', @WhereCondition)-4) 

		CREATE TABLE #tempAddress
		(
			[numAddressID] [numeric](18, 0) NOT NULL,
			[vcAddressName] [varchar](50) NULL,
			[vcStreet] [varchar](100) NULL,
			[vcCity] [varchar](50) NULL,
			[vcPostalCode] [varchar](15) NULL,
			[numState] [numeric](18, 0) NULL,
			[numCountry] [numeric](18, 0) NULL,
			[bitIsPrimary] [bit] NOT NULL CONSTRAINT [DF_AddressMaster_bitIsPrimary]  DEFAULT ((0)),
			[tintAddressOf] [tinyint] NOT NULL,
			[tintAddressType] [tinyint] NOT NULL CONSTRAINT [DF_AddressMaster_tintAddressType]  DEFAULT ((0)),
			[numRecordID] [numeric](18, 0) NOT NULL,
			[numDomainID] [numeric](18, 0) NOT NULL
		)

		IF Len(@zipCodeSearch)>0
		BEGIN
			Declare @Strtemp as nvarchar(500);
			SET @Strtemp='insert into #tempAddress select * from AddressDetails  where numDomainID=@numDomainID1  
							and vcPostalCode in (SELECT REPLICATE(''0'', 5 - LEN(ZipCode)) + ZipCode  FROM ' + @zipCodeSearch +')';
	
			EXEC sp_executesql @Strtemp,N'@numDomainID1 numeric(18)',@numDomainID1=@numDomainID

			set @WhereCondition=replace(@WhereCondition,'$SZ$' + @zipCodeSearch + '$EZ$',
			' (ADC.numContactID in (select  numRecordID from #tempAddress where tintAddressOf=1 AND tintAddressType=0)   
			or (DM.numDivisionID in (select  numRecordID from #tempAddress where tintAddressOf=2 AND tintAddressType in(1,2))))')
		END
	END

	SET @WCondition = ''
   
	IF (@SortCharacter <> '0' AND @SortCharacter <> '')
	  SET @ColumnSearch = @SortCharacter

	CREATE TABLE #temptable 
	(
		id INT IDENTITY PRIMARY KEY,
		numcontactid VARCHAR(15)
	)


---------------------Find Duplicate Code Start-----------------------------
	DECLARE  @Sql           VARCHAR(8000),
			 @numMaxFieldId NUMERIC(9),
			 @Where         NVARCHAR(1000),
			 @OrderBy       NVARCHAR(1000),
			 @GroupBy       NVARCHAR(1000),
			 @SelctFields   NVARCHAR(4000),
			 @InneJoinOn    NVARCHAR(1000)
	SET @InneJoinOn = ''
	SET @OrderBy = ''
	SET @Sql = ''
	SET @Where = ''
	SET @GroupBy = ''
	SET @SelctFields = ''
    
	IF (@WhereCondition = 'DUP' OR @WhereCondition = 'DUP-PRIMARY')
	BEGIN
		SELECT @numMaxFieldId= MAX([numFieldID]) FROM View_DynamicColumns
		WHERE [numFormID] =24 AND [numDomainId] = @numDomainID
		--PRINT @numMaxFieldId
		WHILE @numMaxFieldId > 0
		BEGIN
			SELECT @vcFormFieldName=[vcFieldName],@vcDbColumnName=[vcDbColumnName],@vcLookBackTableName=[vcLookBackTableName] 
			FROM View_DynamicColumns 
			WHERE [numFieldID] = @numMaxFieldId and [numFormID] =24 AND [numDomainId] = @numDomainID
			--	PRINT @vcDbColumnName
				SET @SelctFields = @SelctFields +  @vcDbColumnName + ','
				IF(@vcLookBackTableName ='AdditionalContactsInformation')
				BEGIN
					SET @InneJoinOn = @InneJoinOn + 'ADC.'+ @vcDbColumnName +' = a1.' + @vcDbColumnName + ' and '
					SET @GroupBy = @GroupBy + 'ADC.'+  @vcDbColumnName + ','
				END
				ELSE IF (@vcLookBackTableName ='DivisionMaster')
				BEGIN
					SET @InneJoinOn = @InneJoinOn + 'DM.'+ @vcDbColumnName +' = a1.' + @vcDbColumnName + ' and '
					SET @GroupBy = @GroupBy + 'DM.'+  @vcDbColumnName + ','
				END
				ELSE
				BEGIN
					SET @InneJoinOn = @InneJoinOn + 'C.' + @vcDbColumnName +' = a1.' + @vcDbColumnName + ' and '
					SET @GroupBy = @GroupBy + 'C.' + @vcDbColumnName + ','
				END
				SET @Where = @Where + 'and LEN(' + @vcDbColumnName + ')>0 '

	
			SELECT @numMaxFieldId = MAX([numFieldID]) FROM View_DynamicColumns 
			WHERE [numFormID] =24 AND [numDomainId] = @numDomainID AND [numFieldID] < @numMaxFieldId
		END
			
				IF(LEN(@Where)>2)
				BEGIN
					SET @Where =RIGHT(@Where,LEN(@Where)-3)
					SET @OrderBy =  LEFT(@GroupBy,(Len(@GroupBy)- 1))
					SET @GroupBy =  LEFT(@GroupBy,(Len(@GroupBy)- 1))
					SET @InneJoinOn = LEFT(@InneJoinOn,LEN(@InneJoinOn)-3)
					SET @SelctFields = LEFT(@SelctFields,(Len(@SelctFields)- 1))
					SET @Sql = @Sql + ' INSERT INTO [#tempTable] ([numContactID])SELECT  ADC.numContactId FROM additionalcontactsinformation ADC JOIN DivisionMaster DM ON ADC.numDivisionId = DM.numDivisionID JOIN CompanyInfo C ON DM.numCompanyID = C.numCompanyId  
						JOIN (SELECT   '+ @SelctFields + '
					   FROM  additionalcontactsinformation ADC JOIN DivisionMaster DM ON ADC.numDivisionId = DM.numDivisionID JOIN CompanyInfo C ON DM.numCompanyID = C.numCompanyId  
					   WHERE  ' + @Where + ' AND C.[numDomainID] = '+convert(varchar(15),@numDomainID)+ ' AND ADC.[numDomainID] = '+convert(varchar(15),@numDomainID)+ ' AND DM.[numDomainID] = '+convert(varchar(15),@numDomainID)+ 
					   ' GROUP BY ' + @GroupBy + '
					   HAVING   COUNT(* ) > 1) a1
						ON  '+ @InneJoinOn + ' where ADC.[numDomainID] = '+convert(varchar(15),@numDomainID) + ' and DM.[numDomainID] = '+convert(varchar(15),@numDomainID) + ' ORDER BY '+ @OrderBy
				END	
				ELSE
				BEGIN
					SET @Where = ' 1=0 '
					SET @Sql = @Sql + ' INSERT INTO [#tempTable] ([numContactID])SELECT  ADC.numContactId FROM additionalcontactsinformation ADC  '
								+ ' Where ' + @Where
				END 	
			   PRINT @Sql
			   EXEC( @Sql)
			   SET @Sql = ''
			   SET @GroupBy = ''
			   SET @SelctFields = ''
			   SET @Where = ''
	
		IF (@WhereCondition = 'DUP-PRIMARY')
		  BEGIN
			SET @WCondition = ' AND ADC.numContactId IN (SELECT DISTINCT numContactId FROM #tempTable) and ISNULL(ADC.bitPrimaryContact,0)=1'
		  END
		ELSE
		  BEGIN
			SET @WCondition = ' AND ADC.numContactId IN (SELECT DISTINCT numContactId FROM #tempTable)'
		  END
    
		SET @Sql = ''
		SET @WhereCondition =''
		SET @Where= 'DUP' 
	END
-----------------------------End Find Duplicate ------------------------

	declare @strSql as varchar(8000)                                                              
	 set  @strSql='Select ADC.numContactId                    
	  FROM AdditionalContactsInformation ADC                                                   
	  JOIN DivisionMaster DM ON ADC.numDivisionId = DM.numDivisionID                                                  
	  JOIN CompanyInfo C ON DM.numCompanyID = C.numCompanyId'   

	IF @SortColumnName='vcStreet' OR @SortColumnName='vcCity' OR @SortColumnName='vcPostalCode' OR @SortColumnName ='numCountry' OR @SortColumnName ='numState'
	   OR @ColumnName='vcStreet' OR @ColumnName='vcCity' OR @ColumnName='vcPostalCode' OR @ColumnName ='numCountry' OR @ColumnName ='numState'	
	BEGIN 
		set @strSql= @strSql +' left Join AddressDetails AD on AD.numRecordId= ADC.numContactID and AD.tintAddressOf=1 and AD.tintAddressType=0 AND AD.bitIsPrimary=1 and AD.numDomainID= ADC.numDomainID'	
	END 
	ELSE IF @SortColumnName='vcBillStreet' OR @SortColumnName='vcBillCity' OR @SortColumnName='vcBillPostCode' OR @SortColumnName ='numBillCountry' OR @SortColumnName ='numBillState'
		 OR @ColumnName='vcBillStreet' OR @ColumnName='vcBillCity' OR @ColumnName='vcBillPostCode' OR @ColumnName ='numBillCountry' OR @ColumnName ='numBillState'
	BEGIN 	
		set @strSql= @strSql +' left Join AddressDetails AD1 on AD1.numRecordId= DM.numDivisionID and AD1.tintAddressOf=2 and AD1.tintAddressType=1 AND AD1.bitIsPrimary=1 and AD1.numDomainID= ADC.numDomainID '
	END	
	ELSE IF @SortColumnName='vcShipStreet' OR @SortColumnName='vcShipCity' OR @SortColumnName='vcShipPostCode' OR @SortColumnName ='numShipCountry' OR @SortColumnName ='numShipState'
		 OR @ColumnName='vcShipStreet' OR @ColumnName='vcShipCity' OR @ColumnName='vcShipPostCode' OR @ColumnName ='numShipCountry' OR @ColumnName ='numShipState'
	BEGIN
		set @strSql= @strSql +' left Join AddressDetails AD2 on AD2.numRecordId= DM.numDivisionID and AD2.tintAddressOf=2 and AD2.tintAddressType=2 AND AD2.bitIsPrimary=1 and AD2.numDomainID= ADC.numDomainID '
	END	
                         
	if (@ColumnName<>'' and  @ColumnSearch<>'')                        
	begin                          
		SELECT @vcListItemType=vcListItemType,@numListID=numListID from View_DynamicDefaultColumns where vcDbColumnName=@ColumnName and numFormID=1 and numDomainId=@numDomainId                         
    
		if @vcListItemType='LI'                               
		begin                      
			IF @numListID=40--Country
			BEGIN
				IF @ColumnName ='numCountry'
				BEGIN
						set @strSql= @strSql +' left Join ListDetails L1 on L1.numListItemID=AD.numCountry' 
				END
				ELSE IF @ColumnName ='numBillCountry'
				BEGIN
						set @strSql= @strSql +' left Join ListDetails L1 on L1.numListItemID=AD1.numCountry' 
				END
				ELSE IF @ColumnName ='numShipCountry'
				BEGIN
						set @strSql= @strSql +' left Join ListDetails L1 on L1.numListItemID=AD2.numCountry' 
				END
			END                        
			ELSE
			BEGIN
				set @strSql= @strSql +' left Join ListDetails L1 on L1.numListItemID='+@ColumnName                              
			END   
		end                              
		else if @vcListItemType='S'                               
		begin           
		   IF @ColumnName ='numState'
			BEGIN
   				set @strSql= @strSql +' left join State S1 on S1.numStateID=AD.numState'                            
			END
			ELSE IF @ColumnName ='numBillState'
			BEGIN
   				set @strSql= @strSql +' left join State S1 on S1.numStateID=AD1.numState'                            
			END
			ELSE IF @ColumnName ='numShipState'
			BEGIN
   				set @strSql= @strSql +' left join State S1 on S1.numStateID=AD2.numState'                           
			END
		end                              
		else if @vcListItemType='T'                               
		begin                              
		  set @strSql= @strSql +' left Join ListDetails L1 on L1.numListItemID='+@ColumnName                             
		end                           
	end       
                      
	if (@SortColumnName<>'')                        
	begin                          
		select @vcListItemType1=vcListItemType,@numListID=numListID from View_DynamicDefaultColumns where vcDbColumnName=@SortColumnName and numFormID=1 and numDomainId=@numDomainId                         
    
		if @vcListItemType1='LI'                   
		begin     
			IF @numListID=40--Country
			BEGIN
				IF @SortColumnName ='numCountry'
				BEGIN
						set @strSql= @strSql +' left Join ListDetails L2 on L2.numListItemID=AD.numCountry' 
				END
				ELSE IF @SortColumnName ='numBillCountry'
				BEGIN
						set @strSql= @strSql +' left Join ListDetails L2 on L2.numListItemID=AD1.numCountry' 
				END
				ELSE IF @SortColumnName ='numShipCountry'
				BEGIN
						set @strSql= @strSql +' left Join ListDetails L2 on L2.numListItemID=AD2.numCountry' 
				END
			  END                        
			ELSE
			BEGIN
				set @strSql= @strSql +' left Join ListDetails L2 on L2.numListItemID='+@SortColumnName                              
			END
		END                              
		else if @vcListItemType1='S'           
		begin                
			IF @SortColumnName ='numState'
			BEGIN
   				set @strSql= @strSql +' left join State S2 on S2.numStateID=AD.numState'                            
			END
			ELSE IF @SortColumnName ='numBillState'
			BEGIN
   				set @strSql= @strSql +' left join State S2 on S2.numStateID=AD1.numState'                            
			END
			ELSE IF @SortColumnName ='numShipState'
			BEGIN
   				set @strSql= @strSql +' left join State S2 on S2.numStateID=AD2.numState'                           
			END
		end                              
		else if @vcListItemType1='T'                  
		begin                              
			set @strSql= @strSql +' left Join ListDetails L2 on L2.numListItemID='+@SortColumnName                               
		end                           
	end                          
  
	set @strSql=@strSql+' where DM.numDomainID  = '+convert(varchar(15),@numDomainID)+   @WhereCondition                                  
                 
--if @tintUserRightType=1 set @strSql=@strSql + ' AND (ADC.numRecOwner = '+convert(varchar(15),@numUserCntID)+ ')'                                       
--else if @tintUserRightType=2 set @strSql=@strSql + ' AND (DM.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+convert(varchar(15),@numUserCntID)+' ) or DM.numTerID=0) '
                        
IF @vcRegularSearchCriteria<>'' set @strSql=@strSql+' and ' + @vcRegularSearchCriteria 
IF @vcCustomSearchCriteria<>'' set @strSql=@strSql+' and ' + @vcCustomSearchCriteria                          
                                            
if    @AreasofInt<>'' set  @strSql=@strSql+ ' and ADC.numContactID in (select numContactID from  AOIContactLink where numAOIID in('+ @AreasofInt+'))'
                             
if (@ColumnName<>'' and  @ColumnSearch<>'')                         
begin                          
  if @vcListItemType='LI'                               
    begin                                
      set @strSql= @strSql +' and L1.vcData like '''+@ColumnSearch +'%'''                             
    end                              
    else if @vcListItemType='S'                               
    begin                                  
      set @strSql= @strSql +' and S1.vcState like '''+@ColumnSearch+'%'''                          
    end                              
    else if @vcListItemType='T'                               
    begin                              
      set @strSql= @strSql +' and T1.vcTerName like '''+@ColumnSearch  +'%'''                            
    end   
    else  
       BEGIN     
		   IF @ColumnName='vcStreet' OR @ColumnName='vcBillStreet' OR @ColumnName='vcShipStreet'
				set @strSql= @strSql +' and vcStreet like '''+@ColumnSearch  +'%'''                            
		   ELSE IF @ColumnName='vcCity' OR @ColumnName='vcBillCity' OR @ColumnName='vcShipCity'
				set @strSql= @strSql +' and vcCity like '''+@ColumnSearch  +'%'''                            
		   ELSE IF @ColumnName='vcPostalCode' OR @ColumnName='vcBillPostCode' OR @ColumnName='vcShipPostCode'
				set @strSql= @strSql +' and vcPostalCode like '''+@ColumnSearch  +'%'''                            
		   else		                      
				set @strSql= @strSql +' and '+@ColumnName+' like '''+@ColumnSearch  +'%'''                            
        END   
                          
end                          
   
	if (@SortColumnName<>'')                        
	begin                           
		if @vcListItemType1='LI'                               
		begin                                
			set @strSql= @strSql +' order by L2.vcData '+@columnSortOrder                              
		end                              
		else if @vcListItemType1='S'                               
		begin                                  
			set @strSql= @strSql +' order by S2.vcState '+@columnSortOrder                              
		end                              
		else if @vcListItemType1='T'                               
		begin                              
			set @strSql= @strSql +' order by T2.vcTerName '+@columnSortOrder                              
		end  
		else  
       BEGIN     
		   IF @SortColumnName='vcStreet' OR @SortColumnName='vcBillStreet' OR @SortColumnName='vcShipStreet'
				set @strSql= @strSql +' order by vcStreet ' +@columnSortOrder                            
		   ELSE IF @SortColumnName='vcCity' OR @SortColumnName='vcBillCity' OR @SortColumnName='vcShipCity'
				set @strSql= @strSql +' order by vcCity ' +@columnSortOrder                            
		   ELSE IF @SortColumnName='vcPostalCode' OR @SortColumnName='vcBillPostCode' OR @SortColumnName='vcShipPostCode'
				set @strSql= @strSql +' order by vcPostalCode ' +@columnSortOrder
		   else if @SortColumnName='numRecOwner'
				set @strSql= @strSql + ' order by dbo.fn_GetContactName(ADC.numRecOwner) ' +@columnSortOrder
			ELSE IF @SortColumnName='Opp.vcLastSalesOrderDate'
				SET @strSql= @strSql + ' order by (' + CONCAT('SELECT MAX(bintCreatedDate) AS bintCreatedDate FROM OpportunityMaster WHERE numDomainID = ',@numDomainID,' AND numDivisionID=DM.numDivisionID AND tintOppType = 1 AND tintOppStatus = 1') + ') ' +@columnSortOrder	                            
		   else		                      
				set @strSql= @strSql +' order by '+ @SortColumnName +' ' +@columnSortOrder                            
        END                        
	END
	ELSE 
		set @strSql= @strSql +' order by  ADC.numContactID asc '
IF(@Where <>'DUP')
BEGIN
    PRINT @strSql

	INSERT INTO #temptable (numcontactid)
	EXEC( @strSql)
	
	SET @strSql = ''
END

SET @bitBillAddressJoinAdded=0;
SET @bitShipAddressJoinAdded=0;
SET @bitContactAddressJoinAdded=0;
set @tintOrder=0;
set @WhereCondition ='';
set @strSql='select ADC.numContactId,DM.numDivisionID,DM.numTerID,ADC.numRecOwner,DM.tintCRMType '                              
DECLARE @Prefix AS VARCHAR(5)
DECLARE @bitCustom BIT
DECLARE @numFieldGroupID AS INT
DECLARE @vcColumnName AS VARCHAR(200)
DECLARE @numFieldID AS NUMERIC(18,0)

	DECLARE @TEMPSelectedColumns TABLE
	(
		numFieldID NUMERIC(18,0)
		,bitCustomField BIT
		,tintOrder INT
		,intColumnWidth FLOAT
	)
	DECLARE @vcLocationID VARCHAR(100) = '0'
	SELECT @vcLocationID =isnull(vcLocationID,'0') from dynamicFormMaster where numFormID=1    

	-- IF USER HAS SELECTED ITS OWN DISPLAY COLUMNS THEN USE IT
	IF LEN(ISNULL(@vcDisplayColumns,'')) > 0
	BEGIN
		DECLARE @TempIDs TABLE
		(
			ID INT IDENTITY(1,1),
			vcFieldID VARCHAR(300)
		)

		INSERT INTO @TempIDs
		(
			vcFieldID
		)
		SELECT 
			OutParam 
		FROM 
			SplitString(@vcDisplayColumns,',')

		INSERT INTO @TEMPSelectedColumns
		(
			numFieldID,
			bitCustomField,
			tintOrder,
			intColumnWidth
		)
		SELECT
			numFormFieldID
			,bitCustomField
			,(SELECT (ID-1) FROM @TempIDs T3 WHERE T3.vcFieldID = TEMP.vcFieldID)
			,0
		FROM
		(
			SELECT  
				numFieldID as numFormFieldID,
				0 bitCustomField,
				CONCAT(numFieldID,'~0') vcFieldID
			FROM    
				View_DynamicDefaultColumns
			WHERE   
				numFormID = 1
				AND bitInResults = 1
				AND bitDeleted = 0 
				AND numDomainID = @numDomainID
			UNION 
			SELECT  
				c.Fld_id AS numFormFieldID
				,1 bitCustomField
				,CONCAT(Fld_id,'~1') vcFieldID
			FROM    
				CFW_Fld_Master C 
			LEFT JOIN 
				CFW_Validation V ON V.numFieldID = C.Fld_id
			JOIN 
				CFW_Loc_Master L on C.GRP_ID=L.Loc_Id
			WHERE   
				C.numDomainID = @numDomainID
				AND GRP_ID IN (SELECT ID FROM dbo.SplitIDs(@vcLocationID,','))
		) TEMP
		WHERE
			vcFieldID IN (SELECT vcFieldID FROM @TempIDs)
	END

	-- IF USER SELECTED DISPLAY COLUMNS IS NOT AVAILABLE THAN GET COLUMNS CONFIGURED FROM SEARCH RESULT GRID
	IF (SELECT COUNT(*) FROM @TEMPSelectedColumns) = 0
	BEGIN
		INSERT INTO @TEMPSelectedColumns
		(
			numFieldID,
			bitCustomField,
			tintOrder,
			intColumnWidth
		)
		SELECT
			numFormFieldID
			,bitCustomField
			,tintOrder
			,intColumnWidth
		FROM
		(
			SELECT  
				A.numFormFieldID,
				0 bitCustomField,
				A.tintOrder,
				A.intColumnWidth
			FROM    AdvSerViewConf A
					JOIN View_DynamicDefaultColumns D ON D.numFieldID = A.numFormFieldID
			WHERE   A.numFormID = 1
					AND D.bitInResults = 1
					AND D.bitDeleted = 0
					AND D.numDomainID = @numDomainID 
					AND D.numFormID = 1
					AND A.numDomainID = @numDomainID
					AND A.numUserCntID=@numUserCntID
					AND ISNULL(A.bitCustom,0) = 0
			UNION
			SELECT  
				A.numFormFieldID,
				1 bitCustomField,
				A.tintOrder,
				A.intColumnWidth
			FROM    AdvSerViewConf A
					JOIN CFW_Fld_Master C ON C.Fld_id = A.numFormFieldID
			WHERE   A.numFormID = 1
					AND C.numDomainID = @numDomainID 
					AND A.numDomainID = @numDomainID
					AND A.numUserCntID=@numUserCntID
					AND ISNULL(A.bitCustom,0) = 1
		) T1
		ORDER BY
			 tintOrder
	END

	-- IF USER HASN'T CONFIGURED COLUMNS FROM SEARCH RESULT GRID THEN RETURN DEFAULT COLUMNS
	IF (SELECT COUNT(*) FROM @TEMPSelectedColumns) = 0
	BEGIN
		INSERT INTO @TEMPSelectedColumns
		(
			numFieldID,
			bitCustomField,
			tintOrder,
			intColumnWidth
		)
		SELECT  
			numFieldID,
			0,
			1,
			0
		FROM    
			View_DynamicDefaultColumns
		WHERE   
			numFormID = 1
			AND numDomainID = @numDomainID
			AND numFieldID = 3
	END

	SELECT TOP 1
		@tintOrder=tintOrder,
		@numFieldID=numFieldID,
		@vcDbColumnName=vcDbColumnName,
		@vcFormFieldName=vcFieldName,
		@vcAssociatedControlType=vcAssociatedControlType,
		@vcListItemType=vcListItemType,
		@numListID=numListID,
		@vcLookBackTableName=vcLookBackTableName,
		@bitCustom=bitCustom,
		@vcOrigDbColumnName=vcOrigDbColumnName,
		@bitAllowSorting=bitAllowSorting,
		@bitAllowEdit= bitAllowEdit,
		@ListRelID=ListRelID,
        @intColumnWidth=intColumnWidth,
		@bitAllowFiltering=bitAllowFiltering,
		@vcfieldatatype=vcFieldDataType,
		@numFieldGroupID=numGroupID
	FROM
	(
		SELECT
			T1.tintOrder+1 tintOrder,
			D.numFieldID,
			vcDbColumnName,
			vcFieldName,
			vcAssociatedControlType,
			vcListItemType,
			numListID,
			vcLookBackTableName,
			0 AS bitCustom,
			vcOrigDbColumnName,
			bitAllowSorting,
			bitAllowEdit,			
			ListRelID,			
			T1.intColumnWidth as intColumnWidth,
			bitAllowFiltering,
			vcFieldDataType,			
			0 AS numGroupID
		FROM 
			View_DynamicDefaultColumns D
		INNER JOIN
			@TEMPSelectedColumns T1
		ON 
			D.numFieldID=T1.numFieldID                                                     
		WHERE 
			D.numDomainID = @numDomainID 
			AND D.numFormID = 1 
			AND ISNULL(T1.bitCustomField,0) = 0
			AND d.vcDbColumnName not in ('numBillShipCountry','numBillShipState')
		UNION
		SELECT
			T1.tintOrder+1 tintOrder,
			Fld_id AS numFieldID,
			CONCAT('Cust',Fld_id),
			Fld_label,
			c.Fld_type,
			'',
			c.numlistid,
			'',			
			1 AS bitCustom,
			'',
			null,
			null,
			null,
			null,
			null,
			'',
			Grp_id
		FROM    
			CFW_Fld_Master C 
		INNER JOIN
			@TEMPSelectedColumns T1
		ON 
			C.Fld_id=T1.numFieldID 
		WHERE   
			C.numDomainID = @numDomainID 
			AND ISNULL(T1.bitCustomField,0) = 1
	) T1
	ORDER BY 
		tintOrder asc                              

while @tintOrder>0                              
begin                              
     
	
    IF @vcLookBackTableName = 'AdditionalContactsInformation' 
        SET @Prefix = 'ADC.'
    IF @vcLookBackTableName = 'DivisionMaster' 
        SET @Prefix = 'DM.'
     
	IF @bitCustom = 0 
	BEGIN                           
		SET @vcColumnName= @vcDbColumnName+'~'+ convert(varchar(10),@numFieldId) +'~0'

		if @vcAssociatedControlType='SelectBox'                              
		begin        
		
		print @vcDbColumnName
					IF @vcDbColumnName='numDefaultShippingServiceID'
				BEGIN
					SET @strSql=@strSql+ ' ,ISNULL((SELECT vcShipmentService FROM ShippingService WHERE (numDomainID=DM.numDomainID OR ISNULL(numDomainID,0)=0) AND numShippingServiceID = DM.numDefaultShippingServiceID),'''')' + ' [' + @vcColumnName + ']' 
				END                      
                                              
			 else if @vcListItemType='LI'                               
			  begin    
				  IF @numListID=40--Country
				  BEGIN
	  				set @strSql=@strSql+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName+']'                              
					IF @vcDbColumnName ='numCountry'
					BEGIN
						IF @bitContactAddressJoinAdded=0 
					  BEGIN
							set @WhereCondition= @WhereCondition +' left Join AddressDetails AD on AD.numRecordId= ADC.numContactID and AD.tintAddressOf=1 and AD.tintAddressType=0 AND AD.bitIsPrimary=1 and AD.numDomainID= ADC.numDomainID'	
							SET @bitContactAddressJoinAdded=1;
					  END
					  set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID=AD.numCountry'	
					END
					ELSE IF @vcDbColumnName ='numBillCountry'
					BEGIN
						IF @bitBillAddressJoinAdded=0 
						BEGIN
							set @WhereCondition= @WhereCondition +' left Join AddressDetails AD1 on AD1.numRecordId= DM.numDivisionID and AD1.tintAddressOf=2 and AD1.tintAddressType=1 AND AD1.bitIsPrimary=1 and AD1.numDomainID= ADC.numDomainID '
					
							SET @bitBillAddressJoinAdded=1;
						END
						set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID=AD1.numCountry'
					END
					ELSE IF @vcDbColumnName ='numShipCountry'
					BEGIN
						IF @bitShipAddressJoinAdded=0 
						BEGIN
							set @WhereCondition= @WhereCondition +' left Join AddressDetails AD2 on AD2.numRecordId= DM.numDivisionID and AD2.tintAddressOf=2 and AD2.tintAddressType=2 AND AD2.bitIsPrimary=1 and AD2.numDomainID= ADC.numDomainID '
					
							SET @bitShipAddressJoinAdded=1;
						END
						set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID=AD2.numCountry'
					END
				  END                        
				  ELSE
				  BEGIN
						set @strSql=@strSql+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName +']'                              
						set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID='+@vcDbColumnName	
				  END
			  end                              
	                      
			  else if @vcListItemType='T'                               
			  begin                              
				  set @strSql=@strSql+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName +']'                              
			   set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID='+@vcDbColumnName                              
			  end  
			  else if   @vcListItemType='U'                                                     
				begin           
					set @strSql=@strSql+',dbo.fn_GetContactName('+ @Prefix + @vcDbColumnName+') ['+ @vcColumnName +']'
				end   
	
		end  
		ELSE IF @vcAssociatedControlType='ListBox'   
		BEGIN
			if @vcListItemType='S'                               
			  begin                         
				set @strSql=@strSql+',S'+ convert(varchar(3),@tintOrder)+'.vcState'+' ['+ @vcColumnName +']'
				IF @vcDbColumnName ='numState'
				BEGIN
					IF @bitContactAddressJoinAdded=0 
					   BEGIN
							set @WhereCondition= @WhereCondition +' left Join AddressDetails AD on AD.numRecordId= ADC.numContactID and AD.tintAddressOf=1 and AD.tintAddressType=0 AND AD.bitIsPrimary=1 and AD.numDomainID= ADC.numDomainID'	
							SET @bitContactAddressJoinAdded=1;
					  END	
					  set @WhereCondition= @WhereCondition +' left join State S'+ convert(varchar(3),@tintOrder)+ ' on S'+convert(varchar(3),@tintOrder)+ '.numStateID=AD.numState'
				END
				ELSE IF @vcDbColumnName ='numBillState'
				BEGIN
					IF @bitBillAddressJoinAdded=0 
					BEGIN
						set @WhereCondition= @WhereCondition +' left Join AddressDetails AD1 on AD1.numRecordId= DM.numDivisionID and AD1.tintAddressOf=2 and AD1.tintAddressType=1 AND AD1.bitIsPrimary=1 and AD1.numDomainID= ADC.numDomainID '
						SET @bitBillAddressJoinAdded=1;
					END
					set @WhereCondition= @WhereCondition +' left join State S'+ convert(varchar(3),@tintOrder)+ ' on S'+convert(varchar(3),@tintOrder)+ '.numStateID=AD1.numState'
				END
				ELSE IF @vcDbColumnName ='numShipState'
				BEGIN
					IF @bitShipAddressJoinAdded=0 
					BEGIN
						set @WhereCondition= @WhereCondition +' left Join AddressDetails AD2 on AD2.numRecordId= DM.numDivisionID and AD2.tintAddressOf=2 and AD2.tintAddressType=2 AND AD2.bitIsPrimary=1 and AD2.numDomainID= ADC.numDomainID '
						SET @bitShipAddressJoinAdded=1;
					END
					set @WhereCondition= @WhereCondition +' left join State S'+ convert(varchar(3),@tintOrder)+ ' on S'+convert(varchar(3),@tintOrder)+ '.numStateID=AD2.numState'
				END
			  end         
		END 
		ELSE IF @vcAssociatedControlType='TextBox'   
		BEGIN
			IF @vcDbColumnName='vcStreet' OR @vcDbColumnName='vcCity' OR @vcDbColumnName='vcPostalCode'
			begin                              
			   set @strSql=@strSql+',AD.'+ @vcDbColumnName+' ['+ @vcColumnName +']'                              
			   IF @bitContactAddressJoinAdded=0 
			   BEGIN
					set @WhereCondition= @WhereCondition +' left Join AddressDetails AD on AD.numRecordId= ADC.numContactID and AD.tintAddressOf=1 and AD.tintAddressType=0 AND AD.bitIsPrimary=1 and AD.numDomainID= ADC.numDomainID'	
					SET @bitContactAddressJoinAdded=1;
			   END
			end 
			ELSE IF @vcDbColumnName='vcBillStreet' OR @vcDbColumnName='vcBillCity' OR @vcDbColumnName='vcBillPostCode'
			begin                              
			   set @strSql=@strSql+',AD1.'+ REPLACE(REPLACE(@vcDbColumnName,'Bill',''),'PostCode','PostalCode') +' ['+ @vcColumnName +']'                              
			   IF @bitBillAddressJoinAdded=0 
			   BEGIN 
	   				set @WhereCondition= @WhereCondition +' left Join AddressDetails AD1 on AD1.numRecordId= DM.numDivisionID and AD1.tintAddressOf=2 and AD1.tintAddressType=1 AND AD1.bitIsPrimary=1 and AD1.numDomainID= ADC.numDomainID '
	   				SET @bitBillAddressJoinAdded=1;
			   END
			end 
			ELSE IF @vcDbColumnName='vcShipStreet' OR @vcDbColumnName='vcShipCity' OR @vcDbColumnName='vcShipPostCode'
			begin                              
			   set @strSql=@strSql+',AD2.'+ REPLACE(REPLACE(@vcDbColumnName,'Ship',''),'PostCode','PostalCode') +' ['+ @vcColumnName +']'                              
			   IF @bitShipAddressJoinAdded=0 
			   BEGIN
	   				set @WhereCondition= @WhereCondition +' left Join AddressDetails AD2 on AD2.numRecordId= DM.numDivisionID and AD2.tintAddressOf=2 and AD2.tintAddressType=2  AND AD2.bitIsPrimary=1 and AD2.numDomainID= ADC.numDomainID '
	   				SET @bitShipAddressJoinAdded=1;
			   END
			end 
			ELSE
			BEGIN
				set @strSql=@strSql+','+ case  when @vcDbColumnName='numAge' then 'year(getutcdate())-year(bintDOB)' when @vcDbColumnName='numDivisionID' then 'DM.numDivisionID' when @vcDbColumnName='bintCreatedDate' then 'dbo.[FormatedDateFromDate](DateAdd(minute, ' + CAST(-@ClientTimeZoneOffset AS VARCHAR) + ',DM.bintCreatedDate),DM.numDomainID)' else @vcDbColumnName end+' ['+ @vcColumnName +']'		
			END
		END
		ELSE IF @vcAssociatedControlType='DateField' and @vcDbColumnName='vcLastSalesOrderDate'
		BEGIN
			SET @WhereCondition = @WhereCondition
											+ ' OUTER APPLY(' + CONCAT('SELECT MAX(bintCreatedDate) AS bintCreatedDate FROM OpportunityMaster WHERE numDomainID = ',@numDomainID,' AND numDivisionID=DM.numDivisionID AND tintOppType = 1 AND tintOppStatus = 1') + ' ) AS TempLastOrder '

			set @strSql=@strSql+','+ CONCAT('ISNULL(dbo.FormatedDateFromDate(DateAdd(minute, ',-@ClientTimeZoneOffset,',TempLastOrder.bintCreatedDate),',@numDomainID,'),'''')') +' ['+ @vcColumnName+']'
		END	                    
		ELSE 
			SET @strSql=@strSql+','+ case  when @vcDbColumnName='numAge' then 'year(getutcdate())-year(bintDOB)' when @vcDbColumnName='numDivisionID' then 'DM.numDivisionID' when @vcDbColumnName='bintCreatedDate' then 'dbo.[FormatedDateFromDate](DM.bintCreatedDate,DM.numDomainID)' else @vcDbColumnName end+' ['+ @vcColumnName +']'
	END
	ELSE IF @bitCustom = 1                
	BEGIN
		--SET @vcColumnName= CONCAT(@vcFormFieldName,'~','Cust',@numFieldId)
		SET @vcColumnName= CONCAT('Cust',@numFieldId,'~',@numFieldId,'~',1)
			
		IF @vcAssociatedControlType = 'TextBox' or @vcAssociatedControlType = 'TextArea'            
		BEGIN
			SET @strSql= @strSql+',CFW'+ convert(VARCHAR(3),@tintOrder)+'.Fld_Value  ['+ @vcColumnName +']'                   
				
			SET @WhereCondition= @WhereCondition 
								+' left Join ' + (CASE WHEN @numFieldGroupID = 1 THEN 'CFW_FLD_Values' WHEN @numFieldGroupID = 4 THEN 'CFW_FLD_Values_Cont' ELSE 'CFW_FLD_Values' END) + ' CFW'+ convert(varchar(3),@tintOrder)
								+ ' on CFW' + convert(varchar(3),@tintOrder) 
								+ '.Fld_Id='+convert(varchar(10),@numFieldId) 
								+ 'and CFW' + convert(varchar(3),@tintOrder) 
								+ '.RecId= (CASE WHEN ' + CAST(@numFieldGroupID AS VARCHAR) + ' = 1 THEN DM.numDivisionId WHEN ' + CAST(@numFieldGroupID AS VARCHAR) + ' = 4 THEN ADC.numContactID ELSE 0 END) '                                                         
		END   
		ELSE IF @vcAssociatedControlType = 'CheckBox'       
		BEGIN
			set @strSql= @strSql+',case when isnull(CFW' + CONVERT(VARCHAR(3), @tintOrder)+ '.Fld_Value,0)='''' then 0 else  isnull(CFW'
											+ CONVERT(VARCHAR(3), @tintOrder)+ '.Fld_Value,0) end ['+ @vcColumnName + ']'
 
			set @WhereCondition= @WhereCondition +' left Join ' + (CASE WHEN @numFieldGroupID = 1 THEN 'CFW_FLD_Values' WHEN @numFieldGroupID = 4 THEN 'CFW_FLD_Values_Cont' ELSE 'CFW_FLD_Values' END) + ' CFW'+ convert(varchar(3),@tintOrder)+ '             
			on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId= (CASE WHEN ' + CAST(@numFieldGroupID AS VARCHAR) + ' = 1 THEN DM.numDivisionId WHEN ' + CAST(@numFieldGroupID AS VARCHAR) + ' = 4 THEN ADC.numContactID ELSE 0 END) '                                                     
		END                
		ELSE IF @vcAssociatedControlType = 'DateField'            
		BEGIN
			set @strSql= @strSql+',dbo.FormatedDateFromDate(CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value,'+convert(varchar(10),@numDomainId)+')  ['+ @vcColumnName +']'                   
			set @WhereCondition= @WhereCondition +' left Join ' + (CASE WHEN @numFieldGroupID = 1 THEN 'CFW_FLD_Values' WHEN @numFieldGroupID = 4 THEN 'CFW_FLD_Values_Cont' ELSE 'CFW_FLD_Values' END) + ' CFW'+ convert(varchar(3),@tintOrder)+ '                 
			on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId= (CASE WHEN ' + CAST(@numFieldGroupID AS VARCHAR) + ' = 1 THEN DM.numDivisionId WHEN ' + CAST(@numFieldGroupID AS VARCHAR) + ' = 4 THEN ADC.numContactID ELSE 0 END) '                                                         
		END                
		ELSE IF @vcAssociatedControlType = 'SelectBox'             
		BEGIN
			set @vcDbColumnName = 'DCust'+Convert(varchar(10),@numFieldId)                
			set @strSql=@strSql+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName +']'                                                          
			set @WhereCondition= @WhereCondition +' left Join ' + (CASE WHEN @numFieldGroupID = 1 THEN 'CFW_FLD_Values' WHEN @numFieldGroupID = 4 THEN 'CFW_FLD_Values_Cont' ELSE 'CFW_FLD_Values' END) + ' CFW'+ convert(varchar(3),@tintOrder)+ '                 
				on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId= (CASE WHEN ' + CAST(@numFieldGroupID AS VARCHAR) + ' = 1 THEN DM.numDivisionId WHEN ' + CAST(@numFieldGroupID AS VARCHAR) + ' = 4 THEN ADC.numContactID ELSE 0 END) '                                                        
			set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID=CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value'                
		END         
		ELSE IF @vcAssociatedControlType = 'CheckBoxList'
		BEGIN
			PRINT 'Custom1'

			SET @strSql= @strSql+',(SELECT STUFF((SELECT CONCAT('', '', vcData) FROM ListDetails WHERE numListID= ' + CAST(@numListID AS varchar) + ' AND numlistitemid IN (SELECT ISNULL(Id,0) FROM dbo.SplitIDs(CFW'+ convert(VARCHAR(3),@tintOrder)+'.Fld_Value,'','')) FOR XML PATH('''')), 1, 1, ''''))  ['+ @vcColumnName +']'   

			SET @WhereCondition= @WhereCondition 
								+' left Join ' + (CASE WHEN @numFieldGroupID = 1 THEN 'CFW_FLD_Values' WHEN @numFieldGroupID = 4 THEN 'CFW_FLD_Values_Cont' ELSE 'CFW_FLD_Values' END) + ' CFW'+ convert(varchar(3),@tintOrder)
								+ ' on CFW' + convert(varchar(3),@tintOrder) 
								+ '.Fld_Id='+convert(varchar(10),@numFieldId) 
								+ 'and CFW' + convert(varchar(3),@tintOrder) 
								+ '.RecId= (CASE WHEN ' + CAST(@numFieldGroupID AS VARCHAR) + ' = 1 THEN DM.numDivisionId WHEN ' + CAST(@numFieldGroupID AS VARCHAR) + ' = 4 THEN ADC.numContactID ELSE 0 END) '										
		
		END        
	END          
    
	SELECT TOP 1
		@tintOrder=tintOrder,
		@numFieldID=numFieldID,
		@vcDbColumnName=vcDbColumnName,
		@vcFormFieldName=vcFieldName,
		@vcAssociatedControlType=vcAssociatedControlType,
		@vcListItemType=vcListItemType,
		@numListID=numListID,
		@vcLookBackTableName=vcLookBackTableName,
		@bitCustom=bitCustom,
		@vcOrigDbColumnName=vcOrigDbColumnName,
		@bitAllowSorting=bitAllowSorting,
		@bitAllowEdit= bitAllowEdit,
		@ListRelID=ListRelID,
        @intColumnWidth=intColumnWidth,
		@bitAllowFiltering=bitAllowFiltering,
		@vcfieldatatype=vcFieldDataType,
		@numFieldGroupID=numGroupID
	FROM
	(
		SELECT
			T1.tintOrder+1 tintOrder,
			D.numFieldID,
			vcDbColumnName,
			vcFieldName,
			vcAssociatedControlType,
			vcListItemType,
			numListID,
			vcLookBackTableName,
			0 AS bitCustom,
			vcOrigDbColumnName,
			bitAllowSorting,
			bitAllowEdit,			
			ListRelID,			
			T1.intColumnWidth as intColumnWidth,
			bitAllowFiltering,
			vcFieldDataType,			
			0 AS numGroupID
		FROM 
			View_DynamicDefaultColumns D
		INNER JOIN
			@TEMPSelectedColumns T1
		ON 
			D.numFieldID=T1.numFieldID                                                     
		WHERE 
			D.numDomainID = @numDomainID 
			AND D.numFormID = 1 
			AND ISNULL(T1.bitCustomField,0) = 0
			AND d.vcDbColumnName not in ('numBillShipCountry','numBillShipState')
			AND T1.tintOrder > @tintOrder-1
		UNION
		SELECT
			T1.tintOrder+1 tintOrder,
			Fld_id AS numFieldID,
			CONCAT('Cust',Fld_id),
			Fld_label,
			c.Fld_type,
			'',
			c.numlistid,
			'',			
			1 AS bitCustom,
			'',
			null,
			null,
			null,
			null,
			null,
			'',
			Grp_id
		FROM    
			CFW_Fld_Master C 
		INNER JOIN
			@TEMPSelectedColumns T1
		ON 
			C.Fld_id=T1.numFieldID 
		WHERE   
			C.numDomainID = @numDomainID 
			AND ISNULL(T1.bitCustomField,0) = 1
			AND T1.tintOrder > @tintOrder-1
	) T1
	ORDER BY 
		tintOrder asc                    
                                      
 if @@rowcount=0 set @tintOrder=0                              
end                              

                                                                  
  declare @firstRec as integer                                                                    
  declare @lastRec as integer                                                                    
 set @firstRec= (@CurrentPage-1) * @PageSize                      
 set @lastRec= (@CurrentPage*@PageSize+1)                                                                     
set @TotRecs=(select count(*) from #tempTable)                                                   
                                          
if @GetAll=0         
begin        
        
 if @bitMassUpdate=1            
 begin  

   Declare @strReplace as varchar(8000) = ''
    set @strReplace = case when @LookTable='DivisionMaster' then 'DM.numDivisionID' when @LookTable='AdditionalContactsInformation' then 'ADC.numContactId' when @LookTable='ContactAddress' then 'ADC.numContactId' when @LookTable='ShipAddress' then 'DM.numDivisionID' when @LookTable='BillAddress' then 'DM.numDivisionID' when @LookTable='CompanyInfo' then 'C.numCompanyId' end           
   +' FROM AdditionalContactsInformation ADC inner join DivisionMaster DM on ADC.numDivisionID = DM.numDivisionID                  
   JOIN CompanyInfo C ON DM.numCompanyID = C.numCompanyId                                
    '+@WhereCondition        
    set @strMassUpdate=replace(@strMassUpdate,'@$replace',@strReplace)      
    PRINT CONCAT('MassUPDATE:',@strMassUpdate)
  exec (@strMassUpdate)         
 end         
 
 
 IF (@Where = 'DUP')
 BEGIN
	set @strSql=@strSql+' FROM AdditionalContactsInformation ADC
	  JOIN DivisionMaster DM ON ADC.numDivisionId = DM.numDivisionID
	  JOIN CompanyInfo C ON DM.numCompanyID = C.numCompanyId    
	  JOIN OpportunityMaster OM on OM.numDivisionID= DM.numDivisionID                  
	  inner join #tempTable T on T.numContactId=ADC.numContactId ' 
	  + @WhereCondition +'
	  WHERE ID > '+convert(varchar(10),@firstRec)+ ' and ID <'+ convert(varchar(10),@lastRec) +  @WCondition + ' order by T.ID, C.vcCompanyName' -- @OrderBy
	  PRINT @strSql
	  exec(@strSql)                                                                 
	 drop table #tempTable
	 RETURN	
 END
ELSE
BEGIN
	SET @strSql = @strSql + ' FROM AdditionalContactsInformation ADC
		JOIN DivisionMaster DM ON ADC.numDivisionId = DM.numDivisionID   
	  JOIN CompanyInfo C ON DM.numCompanyID = C.numCompanyId                              
	   '+@WhereCondition+' inner join #tempTable T on T.numContactId=ADC.numContactId                                                               
	  WHERE ID > '+convert(varchar(10),@firstRec)+ ' and ID <'+ convert(varchar(10),@lastRec)
	  
	   
	SET @strSql=@strSql + ' order by T.ID, C.vcCompanyName ' 
END
             
end                           
 else        
 begin                      
     set @strSql=@strSql+' FROM AdditionalContactsInformation ADC                                                   
  JOIN DivisionMaster DM ON ADC.numDivisionId = DM.numDivisionID                                                                   
  JOIN CompanyInfo C ON DM.numCompanyID = C.numCompanyId                              
  '+@WhereCondition+' join #tempTable T on T.numContactId=ADC.numContactId'        
  
	   SET @strSql=@strSql + ' order by T.ID, C.vcCompanyName'   
	end       
print @strSql                                                      
exec(@strSql) 
drop table #tempTable


	SELECT
		tintOrder,
		vcDbColumnName,
		vcFieldName,
		vcAssociatedControlType,
		vcListItemType,
		numListID,
		vcLookBackTableName,
		numFieldID,
		numFieldID AS numFormFieldID,
		intColumnWidth,
		bitCustom AS bitCustomField,
		vcOrigDbColumnName,
		bitAllowSorting,
		bitAllowEdit,
		ListRelID,
		bitAllowFiltering,
		vcFieldDataType
	FROM
	(
		SELECT
			T1.tintOrder+1 tintOrder,
			D.numFieldID,
			vcDbColumnName,
			vcFieldName,
			vcAssociatedControlType,
			vcListItemType,
			numListID,
			vcLookBackTableName,
			0 AS bitCustom,
			vcOrigDbColumnName,
			bitAllowSorting,
			bitAllowEdit,			
			ListRelID,			
			T1.intColumnWidth as intColumnWidth,
			bitAllowFiltering,
			vcFieldDataType,			
			0 AS numGroupID
		FROM 
			View_DynamicDefaultColumns D
		INNER JOIN
			@TEMPSelectedColumns T1
		ON 
			D.numFieldID=T1.numFieldID                                                     
		WHERE 
			D.numDomainID = @numDomainID 
			AND D.numFormID = 1 
			AND ISNULL(T1.bitCustomField,0) = 0
			AND d.vcDbColumnName not in ('numBillShipCountry','numBillShipState')
		UNION
		SELECT
			T1.tintOrder+1 tintOrder,
			Fld_id AS numFieldID,
			CONCAT('Cust',Fld_id),
			Fld_label,
			c.Fld_type,
			'',
			c.numlistid,
			'',			
			1 AS bitCustom,
			'',
			null,
			null,
			null,
			null,
			null,
			'',
			Grp_id
		FROM    
			CFW_Fld_Master C 
		INNER JOIN
			@TEMPSelectedColumns T1
		ON 
			C.Fld_id=T1.numFieldID 
		WHERE   
			C.numDomainID = @numDomainID 
			AND ISNULL(T1.bitCustomField,0) = 1
	) T1
	ORDER BY 
		tintOrder asc       

IF OBJECT_ID('tempdb..#tempAddress') IS NOT NULL
BEGIN
    DROP TABLE #tempAddress
END
END
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_createbizdocs')
DROP PROCEDURE usp_createbizdocs
GO
CREATE PROCEDURE [dbo].[USP_CreateBizDocs]                        
(                                              
 @numOppId as numeric(9)=null,                        
 @numBizDocId as numeric(9)=null,                        
 @numUserCntID as numeric(9)=null,                        
 @numOppBizDocsId as numeric(9)=NULL OUTPUT,
 @vcComments as varchar(1000),
 @bitPartialFulfillment as bit,
 @strBizDocItems as text='' ,
 @numShipVia as numeric(9),
 @vcTrackingURL as varchar(1000),
 @dtFromDate AS DATETIME,
 @numBizDocStatus as numeric(9) = 0,
 @bitRecurringBizDoc AS BIT = NULL,
 @numSequenceId AS VARCHAR(50) = '',
 @tintDeferred as tinyint=0,
 @monCreditAmount as DECIMAL(20,5)= 0 OUTPUT,
 @ClientTimeZoneOffset as int=0,
 @monDealAmount as DECIMAL(20,5)= 0 OUTPUT,
 @bitRentalBizDoc as bit=0,
 @numBizDocTempID as numeric(9)=0,
 @vcTrackingNo AS VARCHAR(500),
 @vcRefOrderNo AS VARCHAR(100),
 @OMP_SalesTaxPercent AS FLOAT = 0, --(joseph) Added to Get Online Marketplace Sales Tax Percentage
 @numFromOppBizDocsId AS NUMERIC(9)=0,
 @bitTakeSequenceId AS BIT=0,
 @bitAllItems AS BIT=0,
 @bitNotValidateBizDocFulfillment AS BIT=0,
 @fltExchangeRateBizDoc AS FLOAT=0,
 @bitRecur BIT = 0,
 @dtStartDate DATE = NULL,
 @dtEndDate DATE = NULL,
 @numFrequency SMALLINT = 0,
 @vcFrequency VARCHAR(20) = '',
 @numRecConfigID NUMERIC(18,0) = 0,
 @bitDisable bit = 0,
 @bitInvoiceForDeferred BIT = 0,
 @numSourceBizDocId Numeric(18,0)=0,
 @bitShippingBizDoc BIT=0,
 @vcVendorInvoiceName  VARCHAR(100) = NULL,
 @numARAccountID NUMERIC(18,0) = 0
)                        
AS 
BEGIN TRY
	IF ISNULL(@numSequenceId,'') = ''
	BEGIN
		SET @bitTakeSequenceId = 1
	END

	DECLARE @hDocItem as INTEGER
	DECLARE @numDivisionID AS NUMERIC(9)
	DECLARE @numDomainID NUMERIC(9)
	DECLARE @tintOppType AS TINYINT	
	DECLARE @numFulfillmentOrderBizDocId NUMERIC(9);
	DECLARE @bitShippingGenerated BIT=0;

	SELECT 
		@numDomainID=numDomainID,
		@numDivisionID=numDivisionID,
		@tintOppType=tintOppType 
	FROM 
		OpportunityMaster 
	WHERE 
		numOppID=@numOppId

	DECLARE @tintCommitAllocation AS TINYINT
	DECLARE @bitAllocateInventoryOnPickList AS BIT

	SELECT @tintCommitAllocation=ISNULL(tintCommitAllocation,1) FROM Domain WHERE numDomainId=@numDomainID
	SELECT @bitAllocateInventoryOnPickList=ISNULL(bitAllocateInventoryOnPickList,0) FROM DivisionMaster WHERE numDivisionID=@numDivisionID

	IF ISNULL(@numFromOppBizDocsId,0) > 0
	BEGIN
		IF (SELECT 
				COUNT(*) 
			FROM 
			(
				SELECT
					OBDI.numOppItemID
					,OI.numUnitHour AS OrderedQty
					,OBDI.numUnitHour AS FromBizDocQty
					,OtherSameBizDocType.BizDocQty
				FROM
					OpportunityBizDocs OBD
				INNER JOIN
					OpportunityBizDocItems OBDI
				ON
					OBD.numOppBizDocsId = OBDI.numOppBizDocID
				INNER JOIN
					OpportunityItems OI
				ON
					OI.numoppitemtCode = OBDI.numOppItemID
				OUTER APPLY
				(
					SELECT
						SUM(OpportunityBizDocItems.numUnitHour) BizDocQty
					FROM
						OpportunityBizDocs
					INNER JOIN
						OpportunityBizDocItems
					ON
						OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
					WHERE
						OpportunityBizDocs.numOppId = OBD.numOppId
						AND OpportunityBizDocItems.numOppItemID=OBDI.numOppItemID
						AND OpportunityBizDocs.numBizDocId = @numBizDocId
				) AS OtherSameBizDocType
				WHERE
					OBD.numOppBizDocsId = @numFromOppBizDocsId
			) X
			WHERE
				ISNULL(X.FromBizDocQty,0) > (ISNULL(X.OrderedQty,0) - ISNULL(X.BizDocQty,0))) > 0
		BEGIN
			-- IF SOMEONE HAS ALREADY CREATED OTHE SAME BIZDOC TYPE AND ENOUGHT QTY IS NOT LEFT FOR ALL ITEMS OF BIZDOC THAN BIZDOC CAN NOT BE CREATED AGAINST VENDOR INVOICE
			RAISERROR('BIZDOC_ALREADY_GENERATED_AGAINST_SOURCE',16,1)
		END
	END

	SET @numFulfillmentOrderBizDocId=296 
	    
	IF ISNULL(@fltExchangeRateBizDoc,0)=0
		SELECT @fltExchangeRateBizDoc=fltExchangeRate FROM OpportunityMaster where numOppID=@numOppId
	
	IF(@bitShippingBizDoc=1)
	BEGIN
		SET @bitShippingGenerated=1
	END
	ELSE
	BEGIN
	IF((SELECT COUNT(numBizDocId) FROM OpportunityBizDocs WHERE bitShippingGenerated=1 AND numOppId=@numOppId)=0)
	BEGIN
		IF((SELECT TOP 1 CAST(numDefaultSalesShippingDoc AS int) FROM Domain WHERE numDomainId=@numDomainID)=@numBizDocId)
		BEGIN
			SET @bitShippingGenerated=1
		END
	END
	END

	IF @numBizDocTempID=0
	BEGIN
		SELECT 
			@numBizDocTempID=numBizDocTempID 
		FROM 
			BizDocTemplate 
		WHERE 
			numBizDocId=@numBizDocId 
			AND numDomainID=@numDomainID and numOpptype=@tintOppType 
			AND tintTemplateType=0 
			AND isnull(bitDefault,0)=1 
			AND isnull(bitEnabled,0)=1
	END

	IF @numOppBizDocsId > 0 AND @numBizDocId = 29397 AND @tintCommitAllocation=2 AND @bitAllocateInventoryOnPickList=1
	BEGIN
		-- Revert Allocation
		EXEC USP_RevertAllocationPickList @numDomainID,@numOppID,@numOppBizDocsId,@numUserCntID
	END

	IF @numOppBizDocsId = 0 AND @numBizDocId = 296 AND @tintCommitAllocation=2 AND @bitAllocateInventoryOnPickList=1
	BEGIN
		-- Revert Allocation
		IF ISNULL(@numFromOppBizDocsId,0) = 0 AND ISNULL(@numSourceBizDocId,0) = 0
		BEGIN
			RAISERROR('FULFILLMENT_BIZDOC_REQUIRED_PICKLIST_FOR_ALLOCATE_ON_PICKLIST_SETTING',16,1)
		END
		ELSE IF ISNULL(@numFromOppBizDocsId,0) > 0 AND ISNULL(@numSourceBizDocId,0) = 0
		BEGIN
			SET @numSourceBizDocId = @numFromOppBizDocsId
		END
	END

	IF @numOppBizDocsId=0
	BEGIN
		IF (
			(@bitPartialFulfillment = 1 AND NOT EXISTS (SELECT * FROM OpportunityBizDocs WHERE numOppID = @numOppId AND numBizDocId = @numBizDocId AND bitPartialFulfilment = 0))
			 OR (NOT EXISTS (SELECT * FROM OpportunityBizDocs WHERE numOppID = @numOppId AND numBizDocId = @numBizDocId AND bitPartialFulfilment = 0)
				 AND NOT EXISTS (SELECT * FROM OpportunityBizDocs WHERE numOppID = @numOppId AND numBizDocId = @numBizDocId AND bitPartialFulfilment = 1))
			 OR (@bitRecurringBizDoc = 1) -- Added new flag to add unit qty in case of recurring bizdoc
			 OR (@numFromOppBizDocsId > 0)
			)
		BEGIN
			DECLARE @tintShipped AS TINYINT	
			DECLARE @dtShipped AS DATETIME
			DECLARE @bitAuthBizdoc AS BIT
        
			SELECT 
				@tintShipped=ISNULL(tintShipped,0),
				@numDomainID=numDomainID 
			FROM 
				OpportunityMaster 
			WHERE numOppId=@numOppId                        
		
			IF @tintOppType = 1 AND @bitNotValidateBizDocFulfillment=0
				EXEC USP_ValidateBizDocFulfillment @numDomainID,@numOppId,@numFromOppBizDocsId,@numBizDocId
		

			IF @tintShipped =1 
				SET @dtShipped = GETUTCDATE();
			ELSE
				SET @dtShipped = null;
			IF @tintOppType = 1 AND (SELECT numAuthoritativeSales FROM [AuthoritativeBizDocs] WHERE numDomainID = @numDomainID) = @numBizDocId
				SET @bitAuthBizdoc = 1
			ELSE if @tintOppType = 2 AND (SELECT numAuthoritativePurchase FROM [AuthoritativeBizDocs] WHERE numDomainID = @numDomainID) = @numBizDocId
				SET @bitAuthBizdoc = 1
			ELSE 
				SET @bitAuthBizdoc = 0
		
			Declare @dtCreatedDate as datetime;SET @dtCreatedDate=Getutcdate()
			IF @tintDeferred=1
			BEGIn
				SET @dtCreatedDate=DateAdd(minute, @ClientTimeZoneOffset,@dtFromDate)
			END
	
			IF @bitTakeSequenceId=1
			BEGIN
				--Sequence #
				CREATE TABLE #tempSequence (numSequenceId bigint )
				INSERT INTO #tempSequence EXEC [dbo].[USP_GetScalerValue] @numDomainID ,33,@numBizDocId,0
				SELECT @numSequenceId =ISNULL(numSequenceId,0) FROM #tempSequence
				DROP TABLE #tempSequence
			
				--BizDoc Template ID
				CREATE TABLE #tempBizDocTempID (numBizDocTempID NUMERIC(9) )
				INSERT INTO #tempBizDocTempID EXEC [dbo].[USP_GetBizDocTemplateList] @numDomainID,@numBizDocId,1,1
				SELECT @numBizDocTempID =ISNULL(numBizDocTempID,0) FROM #tempBizDocTempID
				DROP TABLE #tempBizDocTempID
			END	
			
			--To insert Tracking Id of previous record into the new record
			IF @vcTrackingNo IS NULL OR @vcTrackingNo = ''
			BEGIN
				SELECT TOP 1 @vcTrackingNo = vcTrackingNo FROM OpportunityBizDocs WHERE numOppId = @numOppId ORDER BY numOppBizDocsId DESC 
			END
			INSERT INTO OpportunityBizDocs
			(
				numOppId
				,numBizDocId
				,numCreatedBy
				,dtCreatedDate
				,numModifiedBy
				,dtModifiedDate
				,vcComments
				,bitPartialFulfilment
				,numShipVia
				,vcTrackingURL
				,dtFromDate
				,numBizDocStatus
				,[dtShippedDate]
				,[bitAuthoritativeBizDocs]
				,tintDeferred
				,bitRentalBizDoc
				,numBizDocTempID
				,vcTrackingNo
				,vcRefOrderNo
				,numSequenceId
				,numBizDocStatusOLD
				,fltExchangeRateBizDoc
				,bitAutoCreated
				,[numMasterBizdocSequenceID]
				,bitShippingGenerated
				,numSourceBizDocId
				,vcVendorInvoice
				,numARAccountID
			)
			VALUES     
			(
				@numOppId
				,@numBizDocId
				,@numUserCntID
				,@dtCreatedDate
				,@numUserCntID
				,GETUTCDATE()
				,@vcComments
				,@bitPartialFulfillment
				,@numShipVia
				,@vcTrackingURL
				,@dtFromDate
				,@numBizDocStatus
				,@dtShipped
				,ISNULL(@bitAuthBizdoc,0)
				,@tintDeferred
				,@bitRentalBizDoc
				,@numBizDocTempID
				,@vcTrackingNo
				,@vcRefOrderNo
				,@numSequenceId
				,0
				,@fltExchangeRateBizDoc
				,0
				,@numSequenceId
				,@bitShippingGenerated
				,@numSourceBizDocId
				,@vcVendorInvoiceName
				,@numARAccountID
			)
			
        
			SET @numOppBizDocsId = SCOPE_IDENTITY()
		
			--Added By:Sachin Sadhu||Date:27thAug2014
			--Purpose :MAke entry in WFA queue 
			IF EXISTS (SELECT 1 FROM dbo.OpportunityMaster WHERE numOppId = @numOppID AND tintOppType=1 AND isnull(tintOppStatus,0)=1 )
			BEGIN
				EXEC 
				USP_ManageOpportunityAutomationQueue			
				@numOppQueueID = 0, -- numeric(18, 0)							
				@numDomainID = @numDomainID, -- numeric(18, 0)							
				@numOppId = @numOppID, -- numeric(18, 0)							
				@numOppBizDocsId = @numOppBizDocsId, -- numeric(18, 0)							
				@numOrderStatus = 0, -- numeric(18, 0)					
				@numUserCntID = @numUserCntID, -- numeric(18, 0)					
				@tintProcessStatus = 1, -- tinyint						
				@tintMode = 1 -- TINYINT
			END 
		   --end of script

			--Deferred BizDocs : Create Recurring entry only for create Account Journal
			if @tintDeferred=1
			BEGIN
				exec USP_RecurringAddOpportunity @numOppId,@numOppBizDocsId,0,4,@dtCreatedDate,0,@numDomainID,0,0,100,''
			END

			-- Update name template set for bizdoc
			--DECLARE @tintType TINYINT
			--SELECT @tintType = CASE @tintOppType WHEN 1 THEN 3 WHEN 2 THEN 4 END;
			EXEC dbo.USP_UpdateBizDocNameTemplate @tintOppType,@numDomainID,@numOppBizDocsId

			IF @numFromOppBizDocsId>0
			BEGIN
				INSERT INTO OpportunityBizDocItems
  				(
					numOppBizDocID
					,numOppItemID
					,numItemCode
					,numUnitHour
					,monPrice
					,monTotAmount
					,vcItemDesc
					,numWarehouseItmsID
					,vcType
					,vcAttributes
					,bitDropShip
					,bitDiscountType
					,fltDiscount
					,monTotAmtBefDiscount
					,vcNotes
					,bitEmbeddedCost
					,monEmbeddedCost
					,dtRentalStartDate
					,dtRentalReturnDate
					,tintTrackingStatus
				)
				SELECT 
					@numOppBizDocsId
					,OBDI.numOppItemID
					,OBDI.numItemCode
					,OBDI.numUnitHour
					,OBDI.monPrice
					,OBDI.monTotAmount
					,OBDI.vcItemDesc
					,OBDI.numWarehouseItmsID
					,OBDI.vcType
					,OBDI.vcAttributes
					,OBDI.bitDropShip
					,OBDI.bitDiscountType
					,OBDI.fltDiscount
					,OBDI.monTotAmtBefDiscount
					,OBDI.vcNotes
					,OBDI.bitEmbeddedCost
					,OBDI.monEmbeddedCost
					,OBDI.dtRentalStartDate
					,OBDI.dtRentalReturnDate
					,OBDI.tintTrackingStatus
				FROM 
					OpportunityBizDocs OBD 
				JOIN OpportunityBizDocItems OBDI ON OBD.numOppBizDocsId=OBDI.numOppBizDocID 
				JOIN OpportunityItems OI ON OBDI.numOppItemID=OI.numoppitemtCode
				JOIN Item I ON OBDI.numItemCode=I.numItemCode
				WHERE 
					OBD.numOppId=@numOppId 
					AND OBD.numOppBizDocsId=@numFromOppBizDocsId
			END
			ELSE IF (@bitPartialFulfillment=1 and DATALENGTH(@strBizDocItems)>2)
			BEGIN
				IF DATALENGTH(@strBizDocItems)>2
				BEGIN
					EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strBizDocItems 

						IF @bitRentalBizDoc=1
						BEGIN
							update OI set OI.monPrice=OBZ.monPrice,OI.monTotAmount=OBZ.monTotAmount,OI.monTotAmtBefDiscount=OBZ.monTotAmount
							from OpportunityItems OI
							Join (SELECT * FROM OPENXML (@hDocItem,'/NewDataSet/BizDocItems',2)                                                                          
								WITH ( OppItemID numeric(9),Quantity FLOAT,monPrice DECIMAL(20,5),monTotAmount DECIMAL(20,5))) OBZ
							on OBZ.OppItemID=OI.numoppitemtCode
							where OBZ.OppItemID=OI.numoppitemtCode
						END
	
						insert into                       
					   OpportunityBizDocItems                                                                          
					   (numOppBizDocID,numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,vcAttributes,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount,vcNotes,/*vcTrackingNo,vcShippingMethod,monShipCost,dtDeliveryDate,*/dtRentalStartDate,dtRentalReturnDate)
				   
					   select @numOppBizDocsId,numoppitemtCode,numItemCode,Quantity,CASE WHEN @tintOppType=2 THEN OBZ.monPrice ELSE OI.monPrice END,CASE WHEN @tintOppType=2 THEN OBZ.monPrice * Quantity ELSE (monTotAmount/numUnitHour)*Quantity END,vcItemDesc,numWarehouseItmsID,vcType,vcAttributes,bitDropShip,bitDiscountType,
					   case when bitDiscountType=0 then fltDiscount when bitDiscountType=1 then (fltDiscount/numUnitHour)*Quantity else fltDiscount end ,CASE WHEN @tintOppType=2 THEN OBZ.monPrice * Quantity ELSE (monTotAmtBefDiscount/numUnitHour)*Quantity END,Notes,/*TrackingNo,vcShippingMethod,monShipCost,dtDeliveryDate,*/dtRentalStartDate,dtRentalReturnDate  from OpportunityItems OI
					   Join (SELECT * FROM OPENXML (@hDocItem,'/NewDataSet/BizDocItems',2)                                                                          
					   WITH                       
					   (OppItemID numeric(9),                               
						Quantity FLOAT,
						Notes varchar(500),
						monPrice DECIMAL(20,5),
						dtRentalStartDate datetime,dtRentalReturnDate datetime
						)) OBZ
					   on OBZ.OppItemID=OI.numoppitemtCode
					   where  numOppId=@numOppId AND ISNULL(OI.numUnitHour,0)>0 AND ISNULL(OI.numUnitHour,0)>0
				   
					EXEC sp_xml_removedocument @hDocItem 
				END
			END
			ELSE
			BEGIN
				IF @numBizDocId <> 304 AND @bitAuthBizdoc = 1 AND @bitInvoiceForDeferred = 1
				BEGIN
					DECLARE @numTop1DeferredBizDocID AS NUMERIC(18,0)

					SELECT TOP 1
						@numTop1DeferredBizDocID = 	OpportunityBizDocs.numOppBizDocsId
					FROM 
						OpportunityBizDocs 
					WHERE 
						numOppId=@numOppId 
						AND numOppBizDocsId NOT IN (SELECT ISNULL(numDeferredBizDocID,0) FROM OpportunityBizDocs WHERE numOppId=@numOppId)
						AND OpportunityBizDocs.numBizDocId = 304 -- 304 IS DEFERRED INVOICE BIZDOC
					ORDER BY
						numOppBizDocsId

					INSERT INTO OpportunityBizDocItems
  					(
						numOppBizDocID,
						numOppItemID,
						numItemCode,
						numUnitHour,
						monPrice,
						monTotAmount,
						vcItemDesc,
						numWarehouseItmsID,
						vcType,
						vcAttributes,
						bitDropShip,
						bitDiscountType,
						fltDiscount,
						monTotAmtBefDiscount,
						vcNotes
					)
					SELECT 
						@numOppBizDocsId,
						numoppitemtCode,
						OI.numItemCode,
						(CASE @bitRecurringBizDoc WHEN 1 THEN 1 ELSE ISNULL(TempBizDoc.numUnitHour,0) END) AS numUnitHour,
						OI.monPrice,
						(CASE @bitRecurringBizDoc 
							WHEN 1 THEN monPrice * 1 
							ELSE (OI.monTotAmount/OI.numUnitHour) * ISNULL(TempBizDoc.numUnitHour,0)
						END) AS monTotAmount,
						vcItemDesc,
						numWarehouseItmsID,
						vcType,vcAttributes,
						bitDropShip,
						bitDiscountType,
						(CASE 
							WHEN bitDiscountType=0 THEN fltDiscount 
							WHEN bitDiscountType=1 THEN ((fltDiscount/OI.numUnitHour) * ISNULL(TempBizDoc.numUnitHour,0))
							ELSE fltDiscount 
						END),
						(monTotAmtBefDiscount/OI.numUnitHour) * ISNULL(TempBizDoc.numUnitHour,0),
						OI.vcNotes
					FROM 
						OpportunityItems OI
					JOIN 
						[dbo].[Item] AS I 
					ON 
						I.[numItemCode] = OI.[numItemCode]
					INNER JOIN
						(
							SELECT 
								OpportunityBizDocItems.numOppItemID,
								OpportunityBizDocItems.numUnitHour
							FROM 
								OpportunityBizDocs 
							JOIN 
								OpportunityBizDocItems 
							ON 
								OpportunityBizDocs.numOppBizDocsId  = OpportunityBizDocItems.numOppBizDocId
							WHERE 
								numOppId=@numOppId 
								AND OpportunityBizDocs.numOppBizDocsId = @numTop1DeferredBizDocID
						) TempBizDoc
					ON
						TempBizDoc.numOppItemID = OI.numoppitemtCode
					WHERE  
						numOppId=@numOppId 
						AND ISNULL(OI.numUnitHour,0) > 0
						AND (@bitRecurringBizDoc = 1 OR ISNULL(TempBizDoc.numUnitHour,0) > 0)


					UPDATE OpportunityBizDocs SET numDeferredBizDocID = @numTop1DeferredBizDocID WHERE numOppBizDocsId = @numOppBizDocsId
				END
				ELSE
				BEGIN
		   			INSERT INTO OpportunityBizDocItems                                                                          
  					(
						numOppBizDocID,
						numOppItemID,
						numItemCode,
						numUnitHour,
						monPrice,
						monTotAmount,
						vcItemDesc,
						numWarehouseItmsID,
						vcType,
						vcAttributes,
						bitDropShip,
						bitDiscountType,
						fltDiscount,
						monTotAmtBefDiscount,
						vcNotes
					)
					SELECT 
						@numOppBizDocsId,
						numoppitemtCode,
						OI.numItemCode,
						(CASE @bitRecurringBizDoc 
							WHEN 1 
							THEN 1 
							ELSE (OI.numUnitHour - ISNULL(TempBizDoc.numUnitHour,0)) 
						END) AS numUnitHour,
						OI.monPrice,
						(CASE @bitRecurringBizDoc 
							WHEN 1 THEN monPrice * 1 
							ELSE (OI.monTotAmount/OI.numUnitHour) * (OI.numUnitHour - ISNULL(TempBizDoc.numUnitHour,0))
						END) AS monTotAmount,
						vcItemDesc,
						numWarehouseItmsID,
						vcType,vcAttributes,
						bitDropShip,
						bitDiscountType,
						(CASE 
							WHEN bitDiscountType=0 THEN fltDiscount 
							WHEN bitDiscountType=1 THEN ((fltDiscount/OI.numUnitHour) * (OI.numUnitHour - ISNULL(TempBizDoc.numUnitHour,0)))
							ELSE fltDiscount 
						END),
						(monTotAmtBefDiscount/OI.numUnitHour) * (OI.numUnitHour - ISNULL(TempBizDoc.numUnitHour,0)),
						OI.vcNotes
					FROM 
						OpportunityItems OI
					JOIN 
						[dbo].[Item] AS I 
					ON 
						I.[numItemCode] = OI.[numItemCode]
					OUTER APPLY
						(
							SELECT 
								SUM(OBDI.numUnitHour) AS numUnitHour
							FROM 
								OpportunityBizDocs OBD 
							JOIN 
								dbo.OpportunityBizDocItems OBDI 
							ON 
								OBDI.numOppBizDocId  = OBD.numOppBizDocsId
								AND OBDI.numOppItemID = OI.numoppitemtCode
							WHERE 
								numOppId=@numOppId 
								AND 1 = (CASE 
											WHEN @bitAuthBizdoc = 1 
											THEN (CASE WHEN (numBizDocId = 304 OR (numBizDocId = @numBizDocId AND ISNULL(numDeferredBizDocID,0) = 0)) THEN 1 ELSE 0 END)
											WHEN @numBizDocId = 304 --Deferred Income
											THEN (CASE WHEN (numBizDocId = @numBizDocId OR ISNULL(OBD.bitAuthoritativeBizDocs,0) = 1) THEN 1 ELSE 0 END)
											ELSE (CASE WHEN numBizDocId = @numBizDocId THEN 1 ELSE 0 END) 
										END)
						) TempBizDoc
					WHERE  
						numOppId=@numOppId 
						AND ISNULL(OI.numUnitHour,0) > 0
						AND (@bitRecurringBizDoc = 1 OR (ISNULL(OI.numUnitHour,0) - ISNULL(TempBizDoc.numUnitHour,0)) > 0)
				END
			END

			SET @numOppBizDocsID = @numOppBizDocsId
			--select @numOppBizDocsId
		END
		ELSE
		BEGIN
  			SET @numOppBizDocsId=0
			SET @monDealAmount = 0
			SET @monCreditAmount = 0
			RAISERROR ('NOT_ALLOWED',16,1);
		END
	END
	ELSE
	BEGIN
		UPDATE 
			OpportunityBizDocs
		SET    
			numModifiedBy = @numUserCntID,
			dtModifiedDate = GETUTCDATE(),
			vcComments = @vcComments,
			numShipVia = @numShipVia,
			vcTrackingURL = @vcTrackingURL,
			dtFromDate = @dtFromDate,
			numBizDocStatus = @numBizDocStatus,
			numSequenceId=@numSequenceId,
			numBizDocTempID=@numBizDocTempID,
			vcTrackingNo=@vcTrackingNo,
			vcRefOrderNo=@vcRefOrderNo,
			fltExchangeRateBizDoc=@fltExchangeRateBizDoc,
			bitShippingGenerated=@bitShippingGenerated,
			vcVendorInvoice=@vcVendorInvoiceName,
			numARAccountID=@numARAccountID
		WHERE  
			numOppBizDocsId = @numOppBizDocsId

		--Added By:Sachin Sadhu||Date:27thAug2014
		--Purpose :MAke entry in WFA queue 
		IF EXISTS (SELECT 1 FROM dbo.OpportunityMaster WHERE numOppId = @numOppID AND tintOppType=1 AND isnull(tintOppStatus,0)=1 )
		BEGIN
			EXEC 
			USP_ManageOpportunityAutomationQueue			
			@numOppQueueID = 0, -- numeric(18, 0)							
			@numDomainID = @numDomainID, -- numeric(18, 0)							
			@numOppId = @numOppID, -- numeric(18, 0)							
			@numOppBizDocsId = @numOppBizDocsId, -- numeric(18, 0)							
			@numOrderStatus = 0, -- numeric(18, 0)					
			@numUserCntID = @numUserCntID, -- numeric(18, 0)					
			@tintProcessStatus = 1, -- tinyint						
			@tintMode = 1 -- TINYINT
		END 
		--end of code

   		IF DATALENGTH(@strBizDocItems)>2
		BEGIN
			EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strBizDocItems 

            DELETE FROM 
				OpportunityBizDocItems  
			WHERE 
				numOppItemID NOT IN 
				(
					SELECT 
						OppItemID 
					FROM OPENXML 
						(@hDocItem,'/NewDataSet/BizDocItems',2)                                                                          
					WITH                       
					   (OppItemID numeric(9))
				)
				AND numOppBizDocID=@numOppBizDocsId  
                  
                     
			IF @bitRentalBizDoc=1
			BEGIN
				update OI set OI.monPrice=OBZ.monPrice,OI.monTotAmount=OBZ.monTotAmount,OI.monTotAmtBefDiscount=OBZ.monTotAmount
				from OpportunityItems OI
				Join (SELECT * FROM OPENXML (@hDocItem,'/NewDataSet/BizDocItems',2)                                                                          
					WITH ( OppItemID numeric(9),Quantity FLOAT,monPrice DECIMAL(20,5),monTotAmount DECIMAL(20,5))) OBZ
				on OBZ.OppItemID=OI.numoppitemtCode
				where OBZ.OppItemID=OI.numoppitemtCode
			END

			update OBI set OBI.numUnitHour= Quantity,OBI.monPrice=CASE WHEN @tintOppType=2 THEN OBZ.monPrice ELSE OI.monPrice END,OBI.monTotAmount=CASE WHEN @tintOppType=2 THEN OBZ.monPrice * Quantity ELSE (OI.monTotAmount/OI.numUnitHour)*Quantity END,OBI.vcNotes= Notes,
			OBI.fltDiscount=case when OI.bitDiscountType=0 then OI.fltDiscount when OI.bitDiscountType=1 then (OI.fltDiscount/OI.numUnitHour)*Quantity else OI.fltDiscount end ,
			OBI.monTotAmtBefDiscount=(OI.monTotAmtBefDiscount/OI.numUnitHour)*Quantity ,                                                                    
			/*OBI.vcTrackingNo=TrackingNo,OBI.vcShippingMethod=OBZ.vcShippingMethod,OBI.monShipCost=OBZ.monShipCost,OBI.dtDeliveryDate=OBZ.dtDeliveryDate*/
			OBI.dtRentalStartDate=OBZ.dtRentalStartDate,OBI.dtRentalReturnDate=OBZ.dtRentalReturnDate
			from OpportunityBizDocItems OBI
			join OpportunityItems OI
			on OBI.numOppItemID=OI.numoppitemtCode
			Join (SELECT * FROM OPENXML (@hDocItem,'/NewDataSet/BizDocItems',2)                                                                          
			WITH                       
			( OppItemID numeric(9),                                     
			Quantity FLOAT,
			Notes varchar(500),
			monPrice DECIMAL(20,5),
			dtRentalStartDate datetime,dtRentalReturnDate datetime
			)) OBZ
			on OBZ.OppItemID=OI.numoppitemtCode
			where  OI.numOppId=@numOppId and OBI.numOppBizDocID=@numOppBizDocsId AND ISNULL(OI.numUnitHour,0)>0


            insert into                       
			OpportunityBizDocItems                                                                          
			(numOppBizDocID,numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,vcAttributes,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount,vcNotes/*,vcTrackingNo,vcShippingMethod,monShipCost,dtDeliveryDate*/)                      
			select @numOppBizDocsId,numoppitemtCode,numItemCode,Quantity,CASE WHEN @tintOppType=2 THEN OBZ.monPrice ELSE OI.monPrice END,CASE WHEN @tintOppType=2 THEN OBZ.monPrice * Quantity ELSE (monTotAmount/numUnitHour)*Quantity END,vcItemDesc,numWarehouseItmsID,vcType,vcAttributes,bitDropShip,bitDiscountType,
			case when bitDiscountType=0 then fltDiscount when bitDiscountType=1 then (fltDiscount/numUnitHour)*Quantity else fltDiscount end ,(monTotAmtBefDiscount/numUnitHour)*Quantity,Notes/*,TrackingNo,vcShippingMethod,monShipCost,dtDeliveryDate*/ from OpportunityItems OI
			Join (SELECT * FROM OPENXML (@hDocItem,'/NewDataSet/BizDocItems',2)                                                                          
			WITH                       
			( OppItemID numeric(9),                                     
			Quantity FLOAT,
			Notes varchar(500),
			monPrice DECIMAL(20,5)
			--TrackingNo varchar(500),
			--vcShippingMethod varchar(100),
			--monShipCost DECIMAL(20,5),
			--dtDeliveryDate DATETIME
			)) OBZ
			on OBZ.OppItemID=OI.numoppitemtCode and OI.numoppitemtCode not in (select numOppItemID from OpportunityBizDocItems where numOppBizDocID =@numOppBizDocsId)
			where  numOppId=@numOppId AND ISNULL(OI.numUnitHour,0)>0
                 
			EXEC sp_xml_removedocument @hDocItem 
		END
	END

	IF EXISTS (SELECT
					OBDI.numOppItemID
				FROM
					OpportunityBizDocs OBD
				INNER JOIN
					OpportunityBizDocItems OBDI
				ON
					OBD.numOppBizDocsId = OBDI.numOppBizDocID
				LEFT JOIN
					OpportunityItems OI
				ON
					OBDI.numOppItemID = OI.numoppitemtCode
				WHERE
					OBD.numOppId=@numOppID
					AND OBD.numBizDocId IN (287,296)
				GROUP BY
					OBDI.numOppItemID
					,OI.numUnitHour
				HAVING 
					SUM(ISNULL(OBDI.numUnitHour,0)) > ISNULL(OI.numUnitHour,0))
	BEGIN
		RAISERROR('BIZDOC_QTY_MORE_THEN_ORDERED_QTY',16,1)
	END

	IF @numOppBizDocsId>0
	BEGIN
		SELECT @monDealAmount=isnull(dbo.[GetDealAmount](@numOppId,GETUTCDATE(),@numOppBizDocsId),0)

		IF @numBizDocId = 297 OR @numBizDocId = 299
		BEGIN
			IF NOT EXISTS(SELECT * FROM dbo.NameTemplate WHERE numDomainID=@numDomainID AND numRecordID=@numBizDocId AND tintModuleID=2)
			BEGIN
				INSERT INTO NameTemplate
				(numDomainId,tintModuleID,numRecordID,vcNameTemplate,numSequenceId,numMinLength)
				SELECT 
					@numDomainId,2,@numBizDocId,UPPER(SUBSTRING(dbo.GetListIemName(@numBizDocId),0,4)) + '-',1,4

				SET @numSequenceId = 1
			END
			ELSE
			BEGIN
				SELECT @numSequenceId=numSequenceId FROM NameTemplate WHERE numDomainID=@numDomainID AND numRecordID=@numBizDocId AND tintModuleID=2
				UPDATE NameTemplate SET numSequenceId=ISNULL(numSequenceId,0) + 1 WHERE numDomainID=@numDomainID AND numRecordID=@numBizDocId AND tintModuleID=2
			END
		END

		UPDATE 
			OpportunityBizDocs 
		SET 
			vcBizDocID=vcBizDocName + ISNULL(@numSequenceId,''),
			numSequenceId = CASE WHEN (numBizDocId = 297 OR numBizDocId = 299) THEN @numSequenceId ELSE numSequenceId END,
			monDealAmount=@monDealAmount
		WHERE  
			numOppBizDocsId = @numOppBizDocsId
	END

	-- 1 if recurrence is enabled for authorative bizdoc
	IF ISNULL(@bitRecur,0) = 1
	BEGIN
		-- First Check if receurrece is already created for sales order because user should not 
		-- be able to create recurrence on both order and invoice
		IF (SELECT COUNT(*) FROM RecurrenceConfiguration WHERE numOppID = @numOppId AND numType=1) > 0 
		BEGIN
			RAISERROR ('RECURRENCE_ALREADY_CREATED_FOR_ORDER',16,1);
		END
		--User can not create recurrence on bizdoc where all items are added to bizdoc
		ELSE IF (SELECT dbo.CheckAllItemQuantityAddedToBizDoc(@numOppId,@numOppBizDocsID)) = 1
		BEGIN
			RAISERROR ('RECURRENCE_NOT_ALLOWED_ALL_ITEM_QUANTITY_ADDED_TO_INVOICE',16,1);
		END
		ELSE
		BEGIN

			EXEC USP_RecurrenceConfiguration_Insert
				@numDomainID = @numDomainId,
				@numUserCntID = @numUserCntId,
				@numOppID = @numOppID,
				@numOppBizDocID = @numOppBizDocsId,
				@dtStartDate = @dtStartDate,
				@dtEndDate =NULL,
				@numType = 2,
				@vcType = 'Invoice',
				@vcFrequency = @vcFrequency,
				@numFrequency = @numFrequency

			UPDATE OpportunityMaster SET vcRecurrenceType = 'Invoice' WHERE numOppId = @numOppID
		END
	END
	ELSE IF ISNULL(@numRecConfigID,0) > 0 AND ISNULL(@bitDisable,0)= 1
	BEGIN
		UPDATE
			RecurrenceConfiguration
		SET
			bitDisabled = 1,
			numDisabledBy = @numUserCntId,
			dtDisabledDate = GETDATE()
		WHERE	
			numRecConfigID = @numRecConfigID
	END 
 
	IF @numOppBizDocsId > 0 AND @numBizDocId = 29397 AND @tintCommitAllocation=2 AND @bitAllocateInventoryOnPickList=1
	BEGIN
		EXEC USP_PickListManageSOWorkOrder @numDomainID,@numUserCntID,@numOppID,@numOppBizDocsId
		-- Allocate Inventory
		EXEC USP_ManageInventoryPickList @numDomainID,@numOppID,@numOppBizDocsId,@numUserCntID
	END
END TRY
BEGIN CATCH
    DECLARE @ErrorMessage NVARCHAR(4000);
    DECLARE @ErrorSeverity INT;
    DECLARE @ErrorState INT;

    SELECT 
        @ErrorMessage = ERROR_MESSAGE(),
        @ErrorSeverity = ERROR_SEVERITY(),
        @ErrorState = ERROR_STATE();

    -- Use RAISERROR inside the CATCH block to return error
    -- information about the original error that caused
    -- execution to jump to the CATCH block.
    RAISERROR (@ErrorMessage, -- Message text.
               @ErrorSeverity, -- Severity.
               @ErrorState -- State.
               );
END CATCH;
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DemandForecast_Execute')
DROP PROCEDURE dbo.USP_DemandForecast_Execute
GO
CREATE PROCEDURE [dbo].[USP_DemandForecast_Execute]
	@numDFID NUMERIC (18,0)
	,@numUserCntID NUMERIC(18,0)
	,@ClientTimeZoneOffset INT
	,@CurrentPage INT
    ,@PageSize INT
	,@columnName VARCHAR(MAX)                                                          
	,@columnSortOrder VARCHAR(10)
	,@numDomainID NUMERIC(18,0)
	,@vcRegularSearchCriteria VARCHAR(MAX)=''
	,@bitShowAllItems BIT
	,@bitShowHistoricSales BIT
	,@numHistoricalAnalysisPattern BIT
	,@bitBasedOnLastYear BIT
	,@bitIncludeOpportunity BIT
	,@numOpportunityPercentComplete NUMERIC(18,0)
	,@tintDemandPlanBasedOn TINYINT
AS 
BEGIN
	DECLARE @dtLastExecution DATETIME

	IF (SELECT COUNT(*) FROM DemandForecast WHERE numDFID = @numDFID) > 0
	BEGIN
		DECLARE @tintUnitsRecommendationForAutoPOBackOrder TINYINT
		DECLARE @bitIncludeRequisitions BIT

		SELECT 
			@tintUnitsRecommendationForAutoPOBackOrder=ISNULL(tintUnitsRecommendationForAutoPOBackOrder,1)
			,@bitIncludeRequisitions=ISNULL(bitIncludeRequisitions,0)
		FROM 
			Domain 
		WHERE 
			numDomainID = @numDomainID

		DECLARE @bitWarehouseFilter BIT = 0
		DECLARE @bitItemClassificationFilter BIT = 0
		DECLARE @bitItemGroupFilter BIT = 0

		IF (SELECT COUNT(*) FROM DemandForecastWarehouse WHERE numDFID = @numDFID) > 0
		BEGIN
			SET @bitWarehouseFilter = 1
		END

		IF (SELECT COUNT(*) FROM DemandForecastItemClassification WHERE numDFID = @numDFID) > 0
		BEGIN
			SET @bitItemClassificationFilter = 1
		END

		IF (SELECT COUNT(*) FROM DemandForecastItemGroup WHERE numDFID = @numDFID) > 0
		BEGIN
			SET @bitItemGroupFilter = 1
		END				

		DECLARE @TEMP TABLE
		(
			numItemCode NUMERIC(18,0)
			,numWarehouseItemID NUMERIC(18,0)
			,numUnitHour FLOAT
			,dtReleaseDate DATE
		)

		INSERT INTO @TEMP
		(
			numItemCode
			,numWarehouseItemID
			,numUnitHour
			,dtReleaseDate
		)
		SELECT
			Item.numItemCode
			,OpportunityItems.numWarehouseItmsID
			,OpportunityItems.numUnitHour - ISNULL(OpportunityItems.numQtyShipped,0)
			,ISNULL(OpportunityItems.ItemReleaseDate,OpportunityMaster.dtReleaseDate)
		FROM
			OpportunityMaster
		INNER JOIN
			OpportunityItems
		ON
			OpportunityMaster.numOppId = OpportunityItems.numOppId
		INNER JOIN
			Item
		ON
			OpportunityItems.numItemCode = Item.numItemCode
		INNER JOIN
			WareHouseItems
		ON
			OpportunityItems.numWarehouseItmsID = WareHouseItems.numWareHouseItemID
		WHERE
			OpportunityMaster.numDomainId=@numDomainID
			AND OpportunityMaster.tintOppType=1
			AND OpportunityMaster.tintOppStatus=1
			AND ISNULL(OpportunityMaster.tintshipped,0)=0
			AND (ISNULL(OpportunityItems.numUnitHour,0)  - ISNULL(OpportunityItems.numQtyShipped,0)) > 0
			AND ISNULL(OpportunityItems.bitDropShip,0) = 0
			AND ISNULL(Item.IsArchieve,0) = 0
			AND ISNULL(Item.bitAssembly,0) = 0
			AND ISNULL(Item.bitKitParent,0) = 0
			AND (OpportunityItems.ItemReleaseDate IS NOT NULL OR OpportunityMaster.dtReleaseDate IS NOT NULL)
			AND
			(
				@bitWarehouseFilter = 0 OR
				WareHouseItems.numWareHouseID IN (SELECT numWareHouseID FROM DemandForecastWarehouse WHERE numDFID =@numDFID)
			)
			AND
			(
				@bitItemClassificationFilter = 0 OR
				Item.numItemClassification IN (SELECT numItemClassificationID FROM DemandForecastItemClassification WHERE numDFID = @numDFID)
			)
			AND
			(
				@bitItemGroupFilter = 0 OR
				Item.numItemGroup IN (SELECT numItemGroupID FROM DemandForecastItemGroup WHERE numDFID = @numDFID)
			)

		INSERT INTO @TEMP
		(
			numItemCode
			,numWarehouseItemID
			,numUnitHour
			,dtReleaseDate
		)
		SELECT
			Item.numItemCode
			,OKI.numWareHouseItemId
			,OKI.numQtyItemsReq - ISNULL(OKI.numQtyShipped,0)
			,ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate)
		FROM
			OpportunityMaster OM
		INNER JOIN
			OpportunityItems OI
		ON
			OM.numOppId = OI.numOppId
		INNER JOIN
			OpportunityKitItems OKI
		ON
			OI.numoppitemtCode=OKI.numOppItemID
		INNER JOIN
			Item
		ON
			OKI.numChildItemID = Item.numItemCode
		INNER JOIN
			WareHouseItems WI
		ON
			OKI.numWareHouseItemId = WI.numWareHouseItemID
		WHERE
			OM.numDomainId=@numDomainID
			AND OM.tintOppType=1
			AND OM.tintOppStatus=1
			AND ISNULL(OM.tintshipped,0)=0
			AND ISNULL(OI.bitDropShip,0) = 0
			AND ISNULL(OI.bitWorkOrder,0) = 0
			AND ISNULL(Item.IsArchieve,0) = 0
			AND ISNULL(Item.bitAssembly,0) = 0
			AND ISNULL(Item.bitKitParent,0) = 0
			AND (ISNULL(OKI.numQtyItemsReq,0) - ISNULL(OKI.numQtyShipped,0)) > 0
			AND (OM.dtReleaseDate IS NOT NULL OR OI.ItemReleaseDate IS NOT NULL)
			AND
			(
				@bitWarehouseFilter = 0 OR
				WI.numWareHouseID IN (SELECT numWareHouseID FROM DemandForecastWarehouse WHERE numDFID =@numDFID)
			)
			AND
			(
				@bitItemClassificationFilter = 0 OR
				Item.numItemClassification IN (SELECT numItemClassificationID FROM DemandForecastItemClassification WHERE numDFID = @numDFID)
			)
			AND
			(
				@bitItemGroupFilter = 0 OR
				Item.numItemGroup IN (SELECT numItemGroupID FROM DemandForecastItemGroup WHERE numDFID = @numDFID)
			)

		INSERT INTO @TEMP
		(
			numItemCode
			,numWarehouseItemID
			,numUnitHour
			,dtReleaseDate
		)
		SELECT
			Item.numItemCode
			,OKCI.numWareHouseItemId
			,OKCI.numQtyItemsReq - ISNULL(OKCI.numQtyShipped,0)
			,ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate)
		FROM
			OpportunityMaster OM
		INNER JOIN
			OpportunityItems OI
		ON
			OM.numOppId = OI.numOppId
		INNER JOIN
			OpportunityKitChildItems OKCI
		ON
			OI.numoppitemtCode = OKCI.numOppItemID
		INNER JOIN
			Item
		ON
			OKCI.numItemID = Item.numItemCode
		INNER JOIN
			WareHouseItems WI
		ON
			OKCI.numWareHouseItemId = WI.numWareHouseItemID
		WHERE
			OM.numDomainId=@numDomainID
			AND OM.tintOppType=1
			AND OM.tintOppStatus=1
			AND ISNULL(OM.tintshipped,0)=0
			AND ISNULL(OI.bitDropShip,0) = 0
			AND ISNULL(OI.bitWorkOrder,0) = 0
			AND ISNULL(Item.IsArchieve,0) = 0
			AND ISNULL(Item.bitAssembly,0) = 0
			AND ISNULL(Item.bitKitParent,0) = 0
			AND (ISNULL(OKCI.numQtyItemsReq,0) - ISNULL(OKCI.numQtyShipped,0)) > 0
			AND (OM.dtReleaseDate IS NOT NULL OR OI.ItemReleaseDate IS NOT NULL)
			AND
			(
				@bitWarehouseFilter = 0 OR
				WI.numWareHouseID IN (SELECT numWareHouseID FROM DemandForecastWarehouse WHERE numDFID =@numDFID)
			)
			AND
			(
				@bitItemClassificationFilter = 0 OR
				Item.numItemClassification IN (SELECT numItemClassificationID FROM DemandForecastItemClassification WHERE numDFID = @numDFID)
			)
			AND
			(
				@bitItemGroupFilter = 0 OR
				Item.numItemGroup IN (SELECT numItemGroupID FROM DemandForecastItemGroup WHERE numDFID = @numDFID)
			)

		INSERT INTO @TEMP
		(
			numItemCode
			,numWarehouseItemID
			,numUnitHour
			,dtReleaseDate
		)
		SELECT
			WOD.numChildItemID
			,WOD.numWareHouseItemId
			,WOD.numQtyItemsReq
			,(CASE WHEN OM.numOppId IS NOT NULL THEN ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate) ELSE WO.bintCompliationDate END)
		FROM
			WorkOrderDetails WOD
		INNER JOIN
			Item
		ON
			WOD.numChildItemID = Item.numItemCode
		INNER JOIN
			WareHouseItems WI
		ON
			WOD.numWareHouseItemId = WI.numWareHouseItemID
		INNER JOIN
			WorkOrder WO
		ON
			WOD.numWOId=WO.numWOId
		LEFT JOIN
			OpportunityItems OI
		ON
			WO.numOppItemID = OI.numoppitemtCode
		LEFT JOIN
			OpportunityMaster OM
		ON
			OI.numOppId=OM.numOppId
		WHERE
			WO.numDomainID=@numDomainID
			AND WO.numWOStatus <> 23184 -- NOT COMPLETED
			AND ISNULL(Item.IsArchieve,0) = 0
			AND ISNULL(Item.bitAssembly,0) = 0
			AND ISNULL(Item.bitKitParent,0) = 0
			AND ISNULL(WOD.numQtyItemsReq,0) > 0
			AND (OM.dtReleaseDate IS NOT NULL OR OI.ItemReleaseDate IS NOT NULL OR WO.bintCompliationDate IS NOT NULL)
			AND 1 = (CASE WHEN OM.numOppId IS NOT NULL THEN (CASE WHEN OM.tintOppStatus=1 THEN 1 ELSE 0 END) ELSE 1 END)
			AND
			(
				@bitWarehouseFilter = 0 OR
				WI.numWareHouseID IN (SELECT numWareHouseID FROM DemandForecastWarehouse WHERE numDFID =@numDFID)
			)
			AND
			(
				@bitItemClassificationFilter = 0 OR
				Item.numItemClassification IN (SELECT numItemClassificationID FROM DemandForecastItemClassification WHERE numDFID = @numDFID)
			)
			AND
			(
				@bitItemGroupFilter = 0 OR
				Item.numItemGroup IN (SELECT numItemGroupID FROM DemandForecastItemGroup WHERE numDFID = @numDFID)
			)

		CREATE TABLE #TEMPItems
		(
			numItemCode NUMERIC(18,0)
			,numWarehouseItemID NUMERIC(18,0)
			,numUnitHour FLOAT
			,dtReleaseDate VARCHAR(MAX)
			,dtReleaseDateHidden VARCHAR(MAX)
		)

		INSERT INTO #TEMPItems
		(
			numItemCode
			,numWarehouseItemID
			,numUnitHour
			,dtReleaseDate
			,dtReleaseDateHidden
		)
		SELECT
			numItemCode
			,numWarehouseItemID
			,SUM(numUnitHour)
			,STUFF((SELECT 
						CONCAT(', <a href="#" onclick="return OpenDFReleaseDateRecords(',numItemCode,',',numWarehouseItemID,',''',CONVERT(VARCHAR(10), dtReleaseDate, 101),''');">',CONVERT(VARCHAR(10), dtReleaseDate, 101),'</a> (',SUM(numUnitHour),')')
					FROM 
						@TEMP
					WHERE 
						numItemCode=T1.numItemCode
						AND numWarehouseItemID=T1.numWarehouseItemID
					GROUP BY
						numItemCode
						,numWarehouseItemID
						,dtReleaseDate
					FOR XML PATH(''),TYPE).value('(./text())[1]','VARCHAR(MAX)')
				  ,1,2,'')
			,STUFF((SELECT 
						CONCAT(', ',CONVERT(VARCHAR(10), dtReleaseDate, 101),' (',SUM(numUnitHour),')')
					FROM 
						@TEMP
					WHERE 
						numItemCode=T1.numItemCode
						AND numWarehouseItemID=T1.numWarehouseItemID
					GROUP BY
						numItemCode
						,numWarehouseItemID
						,dtReleaseDate
					FOR XML PATH(''),TYPE).value('(./text())[1]','VARCHAR(MAX)')
				  ,1,2,'')
		FROM
			@TEMP T1
		GROUP BY
			numItemCode
			,numWarehouseItemID

		---------------------------- Dynamic Query -------------------------------
		
		CREATE TABLE #tempForm 
		(
			tintOrder TINYINT,vcDbColumnName nvarchar(50),vcOrigDbColumnName nvarchar(50),vcFieldName nvarchar(50),vcAssociatedControlType nvarchar(50),vcListItemType varchar(3),
			numListID numeric(9),vcLookBackTableName varchar(50),bitCustomField BIT,numFieldId NUMERIC,bitAllowSorting BIT,bitAllowEdit BIT,bitIsRequired BIT,bitIsEmail BIT,
			bitIsAlphaNumeric BIT,bitIsNumeric BIT,bitIsLengthValidation BIT,intMaxLength int,intMinLength int,bitFieldMessage BIT,vcFieldMessage VARCHAR(500),
			ListRelID numeric(9),intColumnWidth int,bitAllowFiltering BIT,vcFieldDataType CHAR(1)
		)

		-- FIRST CHECK IF GRID COLUMNS ARE CONFIGURED
		DECLARE  @Nocolumns  AS TINYINT = 0

		SELECT
			@Nocolumns=ISNULL(SUM(TotalRow),0) 
		FROM
		(            
			SELECT 
				COUNT(*) TotalRow 
			FROM 
				View_DynamicColumns 
			WHERE 
				numFormId=139 
				AND numUserCntID=@numUserCntID 
				AND numDomainID=@numDomainID 
				AND tintPageType=1 
				AND numRelCntType = 0
			UNION 
			SELECT 
				COUNT(*) TotalRow 
			FROM 
				View_DynamicCustomColumns 
			WHERE 
				numFormId=139 
				AND numUserCntID=@numUserCntID 
				AND numDomainID=@numDomainID 
				AND tintPageType=1 
				AND numRelCntType = 0
		) TotalRows

		IF @Nocolumns=0
		BEGIN
			INSERT INTO DycFormConfigurationDetails 
			(
				numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth
			)
			SELECT 
				139,numFieldId,0,Row_number() over(order by tintRow desc),@numDomainID,@numUserCntID,0,1,0,intColumnWidth
			FROM 
				View_DynamicDefaultColumns
			WHERE 
				numFormId=139 
				AND bitDefault=1 
				AND ISNULL(bitSettingField,0)=1 
				AND numDomainID=@numDomainID
			ORDER BY 
				tintOrder asc   
		END

		INSERT INTO 
			#tempForm
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcOrigDbColumnName,ISNULL(vcCultureFieldName,vcFieldName),                  
			vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName,bitCustom,numFieldId,bitAllowSorting,bitInlineEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength,bitFieldMessage,
			vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType  
		FROM 
			View_DynamicColumns 
		WHERE 
			numFormId=139 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND ISNULL(bitSettingField,0)=1 
			AND ISNULL(bitCustom,0)=0  
			AND numRelCntType = 0
		UNION
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName,vcAssociatedControlType,'' as vcListItemType,numListID,'',bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,'' 
		FROM 
			View_DynamicCustomColumns
		WHERE 
			numFormId=139 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND ISNULL(bitCustom,0)=1 
			AND numRelCntType = 0
		ORDER BY 
			tintOrder asc
					
		DECLARE  @strColumns AS VARCHAR(MAX) = ''
		SET @strColumns = CONCAT(' COUNT(*) OVER() TotalRowsCount, Item.numItemCode, Item.vcItemName, Item.vcSKU
		,ISNULL((SELECT vcUnitName FROM UOM WHERE numUOMId = Item.numBaseUnit),''-'') AS vcUOM
		,Item.charItemType AS vcItemType
		,(SELECT TOP 1 numContactId FROM AdditionalContactsInformation WHERE numDivisionId = Item.numVendorID ORDER BY ISNULL(bitPrimaryContact,0) DESC) AS numContactID
		,ISNULL(Item.numPurchaseUnit,0) AS numUOM
		,dbo.fn_UOMConversion(ISNULL(numBaseUnit, 0), Item.numItemCode, Item.numDomainId, ISNULL(Item.numPurchaseUnit, 0)) fltUOMConversionfactor
		,dbo.fn_GetAttributes(WI.numWareHouseItemId,1) AS vcAttributes
		,WI.numWareHouseItemID
		,W.vcWarehouse
		,ISNULL(V.numVendorID,0) numVendorID
		,ISNULL(V.intMinQty,0) intMinQty
		,ISNULL(monCost,0) * dbo.fn_UOMConversion(Item.numBaseUnit,Item.numItemCode,Item.numDomainID,Item.numPurchaseUnit) AS monVendorCost
		,T1.numUnitHour AS numQtyToPurchase, T1.dtReleaseDateHidden, T1.dtReleaseDate,(CASE
			WHEN ',@tintUnitsRecommendationForAutoPOBackOrder,'= 3
			THEN 
				(ISNULL(numBackOrder,0) - ISNULL(numOnOrder,0) + (CASE WHEN ',@bitIncludeRequisitions,' = 1  
												THEN ISNULL((SELECT 
															SUM(numUnitHour) 
														FROM 
															OpportunityItems 
														INNER JOIN 
															OpportunityMaster 
														ON 
															OpportunityItems.numOppID=OpportunityMaster.numOppId 
														WHERE 
															OpportunityMaster.numDomainId=',@numDomainID,'
															AND OpportunityMaster.tintOppType=2 
															AND OpportunityMaster.tintOppStatus=0 
															AND OpportunityItems.numItemCode=Item.numItemCode 
															AND OpportunityItems.numWarehouseItmsID=WI.numWareHouseItemID),0) 
												ELSE 0 
												END)) +
				(CASE WHEN ISNULL(Item.fltReorderQty,0) >= ISNULL(V.intMinQty,0) THEN ISNULL(Item.fltReorderQty,0) ELSE ISNULL(V.intMinQty,0) END) 
			WHEN ',@tintUnitsRecommendationForAutoPOBackOrder,'=2
			THEN 
				(CASE
					WHEN ISNULL(Item.fltReorderQty,0) >= ISNULL(V.intMinQty,0) AND ISNULL(Item.fltReorderQty,0) >= ISNULL(numBackOrder,0) THEN ISNULL(Item.fltReorderQty,0)
					WHEN ISNULL(V.intMinQty,0) >= ISNULL(Item.fltReorderQty,0) AND ISNULL(V.intMinQty,0) >= ISNULL(numBackOrder,0) THEN ISNULL(V.intMinQty,0)
					WHEN ISNULL(numBackOrder,0) >= ISNULL(Item.fltReorderQty,0) AND ISNULL(numBackOrder,0) >= ISNULL(V.intMinQty,0) THEN ISNULL(numBackOrder,0)
					ELSE ISNULL(Item.fltReorderQty,0)
				END) - (ISNULL(numOnOrder,0) + (CASE WHEN ',@bitIncludeRequisitions,' = 1  THEN ISNULL((SELECT 
															SUM(numUnitHour) 
														FROM 
															OpportunityItems 
														INNER JOIN 
															OpportunityMaster 
														ON 
															OpportunityItems.numOppID=OpportunityMaster.numOppId 
														WHERE 
															OpportunityMaster.numDomainId=',@numDomainID,'
															AND OpportunityMaster.tintOppType=2 
															AND OpportunityMaster.tintOppStatus=0 
															AND OpportunityItems.numItemCode=Item.numItemCode 
															AND OpportunityItems.numWarehouseItmsID=WI.numWareHouseItemID),0) ELSE 0 END))
			ELSE
				(CASE WHEN ISNULL(Item.fltReorderQty,0) > ISNULL(V.intMinQty,0) THEN ISNULL(Item.fltReorderQty,0) ELSE ISNULL(V.intMinQty,0) END) 
				+ ISNULL(numBackOrder,0) - (ISNULL(numOnOrder,0) + (CASE WHEN ',@bitIncludeRequisitions,' = 1  THEN ISNULL((SELECT 
																														SUM(numUnitHour) 
																													FROM 
																														OpportunityItems 
																													INNER JOIN 
																														OpportunityMaster 
																													ON 
																														OpportunityItems.numOppID=OpportunityMaster.numOppId 
																													WHERE 
																														OpportunityMaster.numDomainId=',@numDomainID,'
																														AND OpportunityMaster.tintOppType=2 
																														AND OpportunityMaster.tintOppStatus=0 
																														AND OpportunityItems.numItemCode=Item.numItemCode 
																														AND OpportunityItems.numWarehouseItmsID=WI.numWareHouseItemID),0) ELSE 0 END)) 
		END) AS numQtyBasedOnPurchasePlan ')

		--Custom field 
		DECLARE @tintOrder AS TINYINT = 0                                                 
		DECLARE @vcFieldName AS VARCHAR(50)                                                  
		DECLARE @vcListItemType AS VARCHAR(3)                                             
		DECLARE @vcAssociatedControlType VARCHAR(20)     
		declare @numListID AS numeric(9)
		DECLARE @vcDbColumnName VARCHAR(50)                      
		DECLARE @WhereCondition VARCHAR(MAX) = ''                   
		DECLARE @vcLookBackTableName VARCHAR(2000)                
		DECLARE @bitCustom AS BIT
		DECLARE @bitAllowEdit AS CHAR(1)                   
		Declare @numFieldId as numeric  
		DECLARE @bitAllowSorting AS CHAR(1)    
		DECLARE @vcColumnName AS VARCHAR(500)             
		Declare @ListRelID as numeric(9) 
		DECLARE @SearchQuery AS VARCHAR(MAX) = '' 
	   
		SELECT TOP 1 
			@tintOrder=tintOrder+1
			,@vcDbColumnName=vcDbColumnName
			,@vcFieldName=vcFieldName
			,@vcAssociatedControlType=vcAssociatedControlType
			,@vcListItemType=vcListItemType
			,@numListID=numListID
			,@vcLookBackTableName=vcLookBackTableName
			,@bitCustom=bitCustomField
			,@numFieldId=numFieldId
			,@bitAllowSorting=bitAllowSorting
			,@bitAllowEdit=bitAllowEdit
			,@ListRelID=ListRelID
		FROM  
			#tempForm 
		ORDER BY 
			tintOrder ASC   

		WHILE @tintOrder>0                                                  
		BEGIN
	
			IF @bitCustom = 0  
			BEGIN
				DECLARE @Prefix AS VARCHAR(10)

				IF @vcLookBackTableName = 'AdditionalContactsInformation'
					SET @Prefix = 'ADC.'
				ELSE IF @vcLookBackTableName = 'DivisionMaster'
					SET @Prefix = 'Div.'
				ELSE IF @vcLookBackTableName = 'OpportunityMaster'
					SET @PreFix = 'Opp.'			
				ELSE IF @vcLookBackTableName = 'CompanyInfo'
					SET @PreFix = 'cmp.'
				ELSE IF @vcLookBackTableName = 'DemandForecastGrid'
					SET @PreFix = 'DF.'
				ELSE IF @vcLookBackTableName = 'Warehouses'
					SET @PreFix = 'W.'
				ELSE IF @vcLookBackTableName = 'WareHouseItems'
					SET @PreFix = 'WI.'
				ELSE 
					SET @PreFix = CONCAT(@vcLookBackTableName,'.')
			
				SET @vcColumnName= @vcDbColumnName+'~'+ convert(varchar(10),@numFieldId) +'~0'

				IF @vcAssociatedControlType = 'TextBox'
				BEGIN
					IF @vcDbColumnName = 'numQtyToPurchase'
					BEGIN
						SET @strColumns=@strColumns+',' + ' (CASE WHEN ISNULL(WI.numBackOrder,0) > ISNULL(WI.numOnOrder,0) THEN  CEILING(ISNULL(WI.numBackOrder,0) - ISNULL(WI.numOnOrder,0))  ELSE 0 END) '  + ' [' + @vcColumnName + ']'
					END
					ELSE IF @vcDbColumnName = 'moncost'
					BEGIN
						SET @strColumns=@strColumns+',' + ' ISNULL((SELECT vcUnitName FROM UOM WHERE numUOMId = Item.numBaseUnit),''-'') '  + ' [' + @vcColumnName + ']'
					END
					ELSE
					BEGIN
						SET @strColumns=@strColumns+','+  @Prefix + @vcDbColumnName +' ['+ @vcColumnName+']' 
					END
				END
				ELSE IF  @vcAssociatedControlType='Label'                                              
				BEGIN
					IF @vcDbColumnName = 'vc7Days'
					BEGIN
						SET @strColumns=@strColumns+ CONCAT(',dbo.fn_DemandForecastDaysDisplay(',@numDomainID,',',@ClientTimeZoneOffset,',Item.numItemCode,ISNULL(TempLeadDays.numLeadDays,0),WI.numWarehouseItemID,7,',@bitShowHistoricSales,',',@numHistoricalAnalysisPattern,',',@bitBasedOnLastYear,',',@bitIncludeOpportunity,',',@numOpportunityPercentComplete,',',@tintDemandPlanBasedOn,') [',@vcColumnName,']')
					END
					ELSE IF @vcDbColumnName = 'vc15Days'
					BEGIN
						SET @strColumns=@strColumns+ CONCAT(',dbo.fn_DemandForecastDaysDisplay(',@numDomainID,',',@ClientTimeZoneOffset,',Item.numItemCode,ISNULL(TempLeadDays.numLeadDays,0),WI.numWarehouseItemID,15,',@bitShowHistoricSales,',',@numHistoricalAnalysisPattern,',',@bitBasedOnLastYear,',',@bitIncludeOpportunity,',',@numOpportunityPercentComplete,',',@tintDemandPlanBasedOn,') [',@vcColumnName,']')
					END
					ELSE IF @vcDbColumnName = 'vc30Days'
					BEGIN
						SET @strColumns=@strColumns+ CONCAT(',dbo.fn_DemandForecastDaysDisplay(',@numDomainID,',',@ClientTimeZoneOffset,',Item.numItemCode,ISNULL(TempLeadDays.numLeadDays,0),WI.numWarehouseItemID,30,',@bitShowHistoricSales,',',@numHistoricalAnalysisPattern,',',@bitBasedOnLastYear,',',@bitIncludeOpportunity,',',@numOpportunityPercentComplete,',',@tintDemandPlanBasedOn,') [',@vcColumnName,']')
					END
					ELSE IF @vcDbColumnName = 'vc60Days'
					BEGIN
						SET @strColumns=@strColumns+ CONCAT(',dbo.fn_DemandForecastDaysDisplay(',@numDomainID,',',@ClientTimeZoneOffset,',Item.numItemCode,ISNULL(TempLeadDays.numLeadDays,0),WI.numWarehouseItemID,60,',@bitShowHistoricSales,',',@numHistoricalAnalysisPattern,',',@bitBasedOnLastYear,',',@bitIncludeOpportunity,',',@numOpportunityPercentComplete,',',@tintDemandPlanBasedOn,') [',@vcColumnName,']')
					END
					ELSE IF @vcDbColumnName = 'vc90Days'
					BEGIN
						SET @strColumns=@strColumns+ CONCAT(',dbo.fn_DemandForecastDaysDisplay(',@numDomainID,',',@ClientTimeZoneOffset,',Item.numItemCode,ISNULL(TempLeadDays.numLeadDays,0),WI.numWarehouseItemID,90,',@bitShowHistoricSales,',',@numHistoricalAnalysisPattern,',',@bitBasedOnLastYear,',',@bitIncludeOpportunity,',',@numOpportunityPercentComplete,',',@tintDemandPlanBasedOn,') [',@vcColumnName,']')
					END
					ELSE IF @vcDbColumnName = 'vc180Days'
					BEGIN
						SET @strColumns=@strColumns+ CONCAT(',dbo.fn_DemandForecastDaysDisplay(',@numDomainID,',',@ClientTimeZoneOffset,',Item.numItemCode,ISNULL(TempLeadDays.numLeadDays,0),WI.numWarehouseItemID,180,',@bitShowHistoricSales,',',@numHistoricalAnalysisPattern,',',@bitBasedOnLastYear,',',@bitIncludeOpportunity,',',@numOpportunityPercentComplete,',',@tintDemandPlanBasedOn,') [',@vcColumnName,']')
					END
					ELSE IF (@vcDbColumnName = 'vcBuyUOM')
					BEGIN
						SET @strColumns=@strColumns+',' + ' ISNULL((SELECT vcUnitName FROM UOM WHERE numUOMId = ISNULL(Item.numPurchaseUnit,0)),'''')' + ' [' + @vcColumnName + ']'
					END
					ELSE IF @vcDbColumnName = 'numOnOrderReq'
					BEGIN
						SET @strColumns=@strColumns+',' + ' (ISNULL(WI.numOnOrder,0) + ISNULL(WI.numReorder,0)) '  + ' [' + @vcColumnName + ']'
					END
					ELSE IF @vcDbColumnName = 'numAvailable'
					BEGIN
						SET @strColumns=@strColumns+',' + ' ISNULL(WI.numOnHand,0) '  + ' [' + @vcColumnName + ']'
					END
					ELSE
					BEGIN
						SET @strColumns=@strColumns + ',' + @Prefix + @vcDbColumnName  +' ['+ @vcColumnName+']' 
					END   
				END
				ELSE IF  @vcAssociatedControlType='SelectBox' or @vcAssociatedControlType='ListBox'  
				BEGIN
					IF @vcDbColumnName = 'numVendorID'
					BEGIN
						SET @strColumns=@strColumns+',' + ' V.numVendorID '  + ' [' + @vcColumnName + ']'
					END
					ELSE IF @vcDbColumnName = 'ShipmentMethod'
					BEGIN
						SET @strColumns=@strColumns+',' + ' '''' '  + ' [' + @vcColumnName + ']'
					END
					ELSE
					BEGIN
						SET @strColumns=@strColumns + ',' + @Prefix + @vcDbColumnName  +' ['+ @vcColumnName+']' 
					END
				END
				ELSE IF @vcAssociatedControlType='DateField'   
				BEGIN
					IF @vcDbColumnName = 'dtExpectedDelivery'
					BEGIN
						SET @strColumns=@strColumns+',' + ' GETDATE() '  + ' [' + @vcColumnName + ']'
					END
					ELSE IF @vcDbColumnName = 'ItemRequiredDate'
					BEGIN
						SET @strColumns=@strColumns+',T1.dtReleaseDate [' + @vcColumnName + ']'
					END
					ELSE
					BEGIN
						SET @strColumns=@strColumns + ',' + @Prefix + @vcDbColumnName  +' ['+ @vcColumnName+']' 
					END					
				END
			END

			SELECT TOP 1 
				@tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName,@vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,
				@numListID=numListID,@vcLookBackTableName=vcLookBackTableName,@bitCustom=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,
				@bitAllowEdit=bitAllowEdit,@ListRelID=ListRelID
			FROM
				#tempForm 
			WHERE 
				tintOrder > @tintOrder-1 
			ORDER BY 
				tintOrder asc            
 
			IF @@rowcount=0 SET @tintOrder=0 

		 END
	  
		DECLARE @StrSql AS VARCHAR(MAX) = ''
		SET @StrSql = CONCAT('SELECT ',@strColumns,' 
							FROM
								#TEMPItems T1
							INNER JOIN
								Item
							ON
								T1.numItemCode = Item.numItemCode
							INNER JOIN
								WarehouseItems WI
							ON
								Item.numItemCode=WI.numItemID
								AND T1.numWarehouseItemID=WI.numWarehouseItemID
							INNER JOIN
								Warehouses W
							ON
								WI.numWarehouseID = W.numWareHouseID
							LEFT JOIN
								Vendor V
							ON
								Item.numVendorID = V.numVendorID
								AND Item.numItemCode = V.numItemCode
							OUTER APPLY
							(
								SELECT TOP 1
									numListValue AS numLeadDays
								FROM
									VendorShipmentMethod VSM
								WHERE
									VSM.numVendorID = V.numVendorID
								ORDER BY
									ISNULL(bitPrimary,0) DESC, bitPreferredMethod DESC, numListItemID ASC
							) TempLeadDays
							WHERE
								Item.numDomainID=',@numDomainID,'
								AND 1 = (CASE 
											WHEN ',@bitShowAllItems,' = 1 
											THEN 1 
											ELSE 
												(CASE 
													WHEN ', ISNULL(@tintDemandPlanBasedOn,1),' = 2
													THEN (CASE 
															WHEN (CASE
																	WHEN ',@tintUnitsRecommendationForAutoPOBackOrder,'= 3
																	THEN 
																		(ISNULL(numBackOrder,0) - ISNULL(numOnOrder,0) + (CASE WHEN ',@bitIncludeRequisitions,' = 1  
																										THEN ISNULL((SELECT 
																													SUM(numUnitHour) 
																												FROM 
																													OpportunityItems 
																												INNER JOIN 
																													OpportunityMaster 
																												ON 
																													OpportunityItems.numOppID=OpportunityMaster.numOppId 
																												WHERE 
																													OpportunityMaster.numDomainId=',@numDomainID,'
																													AND OpportunityMaster.tintOppType=2 
																													AND OpportunityMaster.tintOppStatus=0 
																													AND OpportunityItems.numItemCode=Item.numItemCode 
																													AND OpportunityItems.numWarehouseItmsID=WI.numWareHouseItemID),0) 
																										ELSE 0 
																										END)) +
																		(CASE WHEN ISNULL(Item.fltReorderQty,0) >= ISNULL(V.intMinQty,0) THEN ISNULL(Item.fltReorderQty,0) ELSE ISNULL(V.intMinQty,0) END) 
																	WHEN ',@tintUnitsRecommendationForAutoPOBackOrder,'=2
																	THEN 
																		(CASE
																			WHEN ISNULL(Item.fltReorderQty,0) >= ISNULL(V.intMinQty,0) AND ISNULL(Item.fltReorderQty,0) >= ISNULL(numBackOrder,0) THEN ISNULL(Item.fltReorderQty,0)
																			WHEN ISNULL(V.intMinQty,0) >= ISNULL(Item.fltReorderQty,0) AND ISNULL(V.intMinQty,0) >= ISNULL(numBackOrder,0) THEN ISNULL(V.intMinQty,0)
																			WHEN ISNULL(numBackOrder,0) >= ISNULL(Item.fltReorderQty,0) AND ISNULL(numBackOrder,0) >= ISNULL(V.intMinQty,0) THEN ISNULL(numBackOrder,0)
																			ELSE ISNULL(Item.fltReorderQty,0)
																		END) - (ISNULL(numOnOrder,0) + (CASE WHEN ',@bitIncludeRequisitions,' = 1  THEN ISNULL((SELECT 
																													SUM(numUnitHour) 
																												FROM 
																													OpportunityItems 
																												INNER JOIN 
																													OpportunityMaster 
																												ON 
																													OpportunityItems.numOppID=OpportunityMaster.numOppId 
																												WHERE 
																													OpportunityMaster.numDomainId=',@numDomainID,'
																													AND OpportunityMaster.tintOppType=2 
																													AND OpportunityMaster.tintOppStatus=0 
																													AND OpportunityItems.numItemCode=Item.numItemCode 
																													AND OpportunityItems.numWarehouseItmsID=WI.numWareHouseItemID),0) ELSE 0 END))
																	ELSE
																		(CASE WHEN ISNULL(Item.fltReorderQty,0) > ISNULL(V.intMinQty,0) THEN ISNULL(Item.fltReorderQty,0) ELSE ISNULL(V.intMinQty,0) END) 
																		+ ISNULL(numBackOrder,0) - (ISNULL(numOnOrder,0) + (CASE WHEN ',@bitIncludeRequisitions,' = 1  THEN ISNULL((SELECT 
																																												SUM(numUnitHour) 
																																											FROM 
																																												OpportunityItems 
																																											INNER JOIN 
																																												OpportunityMaster 
																																											ON 
																																												OpportunityItems.numOppID=OpportunityMaster.numOppId 
																																											WHERE 
																																												OpportunityMaster.numDomainId=',@numDomainID,'
																																												AND OpportunityMaster.tintOppType=2 
																																												AND OpportunityMaster.tintOppStatus=0 
																																												AND OpportunityItems.numItemCode=Item.numItemCode 
																																												AND OpportunityItems.numWarehouseItmsID=WI.numWareHouseItemID),0) ELSE 0 END)) 
																END) > 0
															THEN 1
															ELSE 0
														END)
													ELSE
														(CASE 
															WHEN 
																T1.numUnitHour > 0 
															THEN 1 
															ELSE 0 
														END)
												END)
										END)
							')

		IF CHARINDEX('I.',@vcRegularSearchCriteria) > 0
		BEGIN			   
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'I.','Item.')
		END
		IF CHARINDEX('WH.',@vcRegularSearchCriteria) > 0
		BEGIN			   
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'WH.','W.')
		END
	
		IF @vcRegularSearchCriteria<>'' 
		BEGIN
			SET @strSql=@strSql +' AND ' + @vcRegularSearchCriteria 
		END
	  
		DECLARE  @firstRec  AS INTEGER
		DECLARE  @lastRec  AS INTEGER
  
		SET @firstRec = (@CurrentPage - 1) * @PageSize
		SET @lastRec = (@CurrentPage * @PageSize + 1)

		-- EXECUTE FINAL QUERY
		DECLARE @strFinal AS NVARCHAR(MAX)
		SET @strFinal = CONCAT(@StrSql,' ORDER BY ',CASE WHEN CHARINDEX(@columnName,@strColumns) > 0 THEN @columnName ELSE 'Item.numItemCode' END,' OFFSET ',(@CurrentPage-1) * @PageSize,' ROWS FETCH NEXT ',@PageSize,' ROWS ONLY')

		PRINT CAST(@strFinal AS NTEXT)
		EXEC sp_executesql @strFinal

		SELECT * FROM #tempForm

		DROP TABLE #tempForm

		DROP TABLE #TEMPItems


		UPDATE DemandForecast SET dtExecutionDate = dateadd(MINUTE,-@ClientTimeZoneOffset,GETDATE()) WHERE numDFID = @numDFID

		SELECT CONVERT(VARCHAR(20),dateadd(MINUTE,-@ClientTimeZoneOffset,@dtLastExecution)) AS dtLastExecution
	END
END
/****** Object:  StoredProcedure [dbo].[USP_GetColumnConfiguration1]    Script Date: 07/26/2008 16:16:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'usp_getcolumnconfiguration1' )
    DROP PROCEDURE usp_getcolumnconfiguration1
GO
CREATE PROCEDURE [dbo].[USP_GetColumnConfiguration1]
    @numDomainID NUMERIC(9) ,
    @numUserCntId NUMERIC(9) ,
    @FormId TINYINT ,
    @numtype NUMERIC(9) ,
    @numViewID NUMERIC(9) = 0
AS
    DECLARE @PageId AS TINYINT         
    IF @FormId = 10 OR @FormId = 44
        SET @PageId = 4          
    ELSE IF @FormId = 11 OR @FormId = 34 OR @FormId = 35 OR @FormId = 36 OR @FormId = 96 OR @FormId = 97
            SET @PageId = 1          
    ELSE IF @FormId = 12
                SET @PageId = 3          
    ELSE IF @FormId = 13
                    SET @PageId = 11       
    ELSE IF (@FormId = 14 AND (@numtype = 1 OR @numtype = 3)) OR @FormId = 33 OR @FormId = 38 OR @FormId = 39 OR @FormId = 23
                        SET @PageId = 2          
    ELSE IF @FormId = 14 AND (@numtype = 2 OR @numtype = 4) OR @FormId = 40 OR @FormId = 41
                            SET @PageId = 6  
    ELSE IF @FormId = 21 OR @FormId = 26 OR @FormId = 32 OR @FormId = 126 OR @FormId = 129
                                SET @PageId = 5   
    ELSE IF @FormId = 22 OR @FormId = 30
                                    SET @PageId = 5    
    ELSE IF @FormId = 76
                                        SET @PageId = 10 
    ELSE IF @FormId = 84 OR @FormId = 85
                                            SET @PageId = 5

              
    IF @PageId = 1 OR @PageId = 4
    BEGIN
        SELECT  CONVERT(VARCHAR(9), numFieldID) + '~0' AS numFieldID ,
                ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                0 AS Custom
        FROM    View_DynamicDefaultColumns
        WHERE   numFormID = @FormId
                AND ISNULL(bitSettingField, 0) = 1
                AND ISNULL(bitDeleted, 0) = 0
                AND numDomainID = @numDomainID
        UNION
        SELECT  CONVERT(VARCHAR(9), fld_id) + '~1' AS numFieldID ,
                fld_label AS vcFieldName ,
                1 AS Custom
        FROM    CFW_Fld_Master
                LEFT JOIN CFW_Fld_Dtl ON Fld_id = numFieldId
                                            AND numRelation = CASE
                                                            WHEN @numtype IN (
                                                            1, 2, 3 )
                                                            THEN numRelation
                                                            ELSE @numtype
                                                            END
                LEFT JOIN CFw_Grp_Master ON subgrp = CFw_Grp_Master.Grp_id
        WHERE   CFW_Fld_Master.grp_id = @PageId
                AND CFW_Fld_Master.numDomainID = @numDomainID
                AND Fld_type <> 'Link'
        ORDER BY Custom ,
                vcFieldName                                       
    END  
    ELSE IF @PageId = 10
    BEGIN
        SELECT  CONVERT(VARCHAR(9), numFieldID) + '~0' AS numFieldID ,
                ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                0 AS Custom
        FROM    View_DynamicDefaultColumns
        WHERE   numFormID = @FormId
                AND ISNULL(bitSettingField, 0) = 1
                AND ISNULL(bitDeleted, 0) = 0
                AND numDomainID = @numDomainID
        UNION
        SELECT  CONVERT(VARCHAR(9), fld_id) + '~1' AS numFieldID ,
                fld_label AS vcFieldName ,
                1 AS Custom
        FROM    CFW_Fld_Master
                LEFT JOIN CFW_Fld_Dtl ON Fld_id = numFieldId
                                            AND numRelation = CASE
                                                        WHEN @numtype IN (
                                                        1, 2, 3 )
                                                        THEN numRelation
                                                        ELSE @numtype
                                                        END
                LEFT JOIN CFw_Grp_Master ON subgrp = CFw_Grp_Master.Grp_id
        WHERE   CFW_Fld_Master.grp_id = 5
                AND CFW_Fld_Master.numDomainID = @numDomainID
                AND Fld_Type = 'SelectBox'
        ORDER BY Custom ,
                vcFieldName       
    END 
	ELSE IF @FormId=43
	BEGIN
		PRINT 'ABC'
		SELECT  CONVERT(VARCHAR(9), numFieldID) + '~0' AS numFieldID ,
                    ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                    0 AS Custom ,
                    vcDbColumnName
            FROM    View_DynamicDefaultColumns
            WHERE   numFormID = @FormId
                    AND ISNULL(bitSettingField, 0) = 1
                    AND ISNULL(bitDeleted, 0) = 0
                    AND numDomainID = @numDomainID
            UNION
            SELECT  CONVERT(VARCHAR(9), fld_id) + '~1' AS numFieldID ,
                    fld_label AS vcFieldName ,
                    1 AS Custom ,
                    CONVERT(VARCHAR(10), fld_id) AS vcDbColumnName
            FROM    CFW_Fld_Master
                    LEFT JOIN CFw_Grp_Master ON subgrp = CFw_Grp_Master.Grp_id
            WHERE   CFW_Fld_Master.grp_id = 1
                    AND CFW_Fld_Master.numDomainID = @numDomainID
                    AND Fld_type <> 'Link'
            ORDER BY Custom ,
                    vcFieldName          
	END
	ELSE IF @FormId = 125 AND @PageId=0
	BEGIN
			SELECT  CONVERT(VARCHAR(9), numFieldID) + '~0' AS numFieldID ,
                    ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                    0 AS Custom
            FROM    View_DynamicDefaultColumns
            WHERE   numFormID = @FormId
                    AND ISNULL(bitSettingField, 0) = 1
                    AND ISNULL(bitDeleted, 0) = 0
                    AND numDomainID = @numDomainID
	END
    ELSE IF @PageId = 5 AND (@FormId = 84 OR @FormId = 85)
    BEGIN
        SELECT  CONVERT(VARCHAR(9), numFieldID) + '~0' AS numFieldID ,
                ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                0 AS Custom ,
                vcDbColumnName
        FROM    View_DynamicDefaultColumns
        WHERE   numFormID = @FormId
                AND ISNULL(bitSettingField, 0) = 1
                AND ISNULL(bitDeleted, 0) = 0
                AND numDomainID = @numDomainID
        UNION
        SELECT  CONVERT(VARCHAR(9), fld_id) + '~1' AS numFieldID ,
                fld_label AS vcFieldName ,
                1 AS Custom ,
                CONVERT(VARCHAR(10), fld_id) AS vcDbColumnName
        FROM    CFW_Fld_Master
                LEFT JOIN CFw_Grp_Master ON subgrp = CFw_Grp_Master.Grp_id
        WHERE   CFW_Fld_Master.grp_id = @PageId
                AND CFW_Fld_Master.numDomainID = @numDomainID
                AND Fld_type = 'TextBox'
        ORDER BY Custom ,
                vcFieldName             
    END 
	ELSE IF @FormId=141
	BEGIN
		SELECT  
			CONVERT(VARCHAR(9), numFieldID) + '~0' AS numFieldID ,
            ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
            0 AS Custom,
            vcDbColumnName,
			bitRequired
        FROM
			View_DynamicDefaultColumns
        WHERE 
			numFormID = @FormId
            AND ISNULL(bitSettingField, 0) = 1
            AND ISNULL(bitDeleted, 0) = 0
            AND numDomainID = @numDomainID
        UNION
        SELECT 
			CONVERT(VARCHAR(9), fld_id) + '~1' AS numFieldID ,
            fld_label AS vcFieldName ,
            1 AS Custom ,
            CONVERT(VARCHAR(10), fld_id) AS vcDbColumnName,
			0 AS bitRequired
        FROM 
			CFW_Fld_Master
        LEFT JOIN 
			CFw_Grp_Master 
		ON 
			subgrp = CFw_Grp_Master.Grp_id
        WHERE 
			CFW_Fld_Master.grp_id IN (2,5)
            AND CFW_Fld_Master.numDomainID = @numDomainID
            AND Fld_type <> 'Link'
        ORDER BY 
			Custom,
            vcFieldName
	END
	ELSE IF @FormId=142
	BEGIN
		SELECT  
			CONVERT(VARCHAR(9), numFieldID) + '~0' AS numFieldID,
            ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName,
            0 AS Custom,
            vcDbColumnName,
			bitRequired
        FROM
			View_DynamicDefaultColumns
        WHERE 
			numFormID = @FormId
            AND ISNULL(bitSettingField, 0) = 1
            AND ISNULL(bitDeleted, 0) = 0
            AND numDomainID = @numDomainID
		ORDER BY
			vcFieldName
	END
    ELSE
    BEGIN
        SELECT  CONVERT(VARCHAR(9), numFieldID) + '~0' AS numFieldID ,
                ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                0 AS Custom ,
                vcDbColumnName
        FROM    View_DynamicDefaultColumns
        WHERE   numFormID = @FormId
                AND ISNULL(bitSettingField, 0) = 1
                AND ISNULL(bitDeleted, 0) = 0
                AND numDomainID = @numDomainID
        UNION
        SELECT  CONVERT(VARCHAR(9), fld_id) + '~1' AS numFieldID ,
                fld_label AS vcFieldName ,
                1 AS Custom ,
                CONVERT(VARCHAR(10), fld_id) AS vcDbColumnName
        FROM    CFW_Fld_Master
                LEFT JOIN CFw_Grp_Master ON subgrp = CFw_Grp_Master.Grp_id
        WHERE   1 = (CASE WHEN @FormId=21 THEN (CASE WHEN  CFW_Fld_Master.grp_id IN (5,9) THEN 1 ELSE 0 END) ELSE (CASE WHEN  CFW_Fld_Master.grp_id = @PageId THEN 1 ELSE 0 END) END)
                AND CFW_Fld_Master.numDomainID = @numDomainID
                AND Fld_type <> 'Link'
        ORDER BY Custom ,
                vcFieldName             
    END
	                          
                   
    IF @PageId = 1 OR @PageId = 4
    BEGIN
		IF (@FormId = 96 OR @FormId = 97)
                SELECT  CONVERT(VARCHAR(9), numFieldID) + '~0' numFieldID ,
                        ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                        vcDbColumnName ,
                        0 AS Custom ,
                        tintRow AS tintOrder
                FROM    View_DynamicColumns
                WHERE   numFormId = @FormId
                        AND numDomainID = @numDomainID
                        AND ISNULL(bitCustom, 0) = 0
                        AND tintPageType = 1
                        AND ISNULL(bitSettingField, 0) = 1
                        AND numRelCntType = @numtype
                        AND ISNULL(bitDeleted, 0) = 0
                UNION
                SELECT  CONVERT(VARCHAR(9), numFieldID) + '~1' AS numFieldID ,
                        vcFieldName ,
                        vcFieldName AS vcDbColumnName ,
                        1 AS Custom ,
                        tintRow AS tintOrder
                FROM    View_DynamicCustomColumns
                WHERE   numFormID = @FormId
                        AND numDomainID = @numDomainID
                        AND grp_id = @PageId
                        AND numDomainID = @numDomainID
                        AND vcAssociatedControlType <> 'Link'
                        AND ISNULL(bitCustom, 0) = 1
                        AND tintPageType = 1
                        AND numRelCntType = @numtype
                ORDER BY tintOrder                
            ELSE
                SELECT  CONVERT(VARCHAR(9), numFieldID) + '~0' numFieldID ,
                        ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                        vcDbColumnName ,
                        0 AS Custom ,
                        tintRow AS tintOrder
                FROM    View_DynamicColumns
                WHERE   numFormId = @FormId
                        AND numUserCntID = @numUserCntID
                        AND numDomainID = @numDomainID
                        AND ISNULL(bitCustom, 0) = 0
                        AND tintPageType = 1
                        AND ISNULL(bitSettingField, 0) = 1
                        AND numRelCntType = @numtype
                        AND ISNULL(bitDeleted, 0) = 0
                UNION
                SELECT  CONVERT(VARCHAR(9), numFieldID) + '~1' AS numFieldID ,
                        vcFieldName ,
                        '' AS vcDbColumnName ,
                        1 AS Custom ,
                        tintRow AS tintOrder
                FROM    View_DynamicCustomColumns
                WHERE   numFormID = @FormId
                        AND numUserCntID = @numUserCntID
                        AND numDomainID = @numDomainID
                        AND grp_id = @PageId
                        AND numDomainID = @numDomainID
                        AND vcAssociatedControlType <> 'Link'
                        AND ISNULL(bitCustom, 0) = 1
                        AND tintPageType = 1
                        AND numRelCntType = @numtype
                ORDER BY tintOrder          
    END     
    ELSE IF @PageId = 10
    BEGIN
        SELECT  CONVERT(VARCHAR(9), numFieldID) + '~0' numFieldID ,
                ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                vcDbColumnName ,
                0 AS Custom ,
                tintRow AS tintOrder ,
                numListID ,
                numFieldID AS [numFieldID1]
        FROM    View_DynamicColumns
        WHERE   numFormId = @FormId
                AND numUserCntID = @numUserCntID
                AND numDomainID = @numDomainID
                AND ISNULL(bitCustom, 0) = 0
                AND tintPageType = 1
                AND ISNULL(bitSettingField, 0) = 1
                AND numRelCntType = @numtype
                AND ISNULL(bitDeleted, 0) = 0
        UNION
        SELECT  CONVERT(VARCHAR(9), numFieldID) + '~1' AS numFieldID ,
                vcFieldName ,
                '' AS vcDbColumnName ,
                1 AS Custom ,
                tintRow AS tintOrder ,
                numListID ,
                numFieldID AS [numFieldID1]
        FROM    View_DynamicCustomColumns
        WHERE   numFormID = @FormId
                AND numUserCntID = @numUserCntID
                AND numDomainID = @numDomainID   
                AND numDomainID = @numDomainID
                AND vcAssociatedControlType = 'SelectBox'
                AND ISNULL(bitCustom, 0) = 1
                AND tintPageType = 1
                AND numRelCntType = @numtype
        ORDER BY tintOrder    
    END	   
    ELSE IF @PageId = 5 AND (@FormId = 84 OR @FormId = 85)
    BEGIN
        IF EXISTS (SELECT 
						'col1'
                    FROM 
						DycFormConfigurationDetails
                    WHERE 
						numDomainID = @numDomainID
                        AND numFormId = @FormID
                        AND numFieldID IN (507, 508, 509, 510, 511, 512)
					)
        BEGIN
            SELECT  CONVERT(VARCHAR(9), numFieldID) + '~0' numFieldID ,
                    ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                    vcDbColumnName ,
                    0 AS Custom ,
                    tintRow AS tintOrder ,
                    vcAssociatedControlType ,
                    numlistid ,
                    vcLookBackTableName ,
                    numFieldID AS FieldId ,
                    bitAllowEdit ,
                    0 AS AZordering
            FROM    View_DynamicColumns
            WHERE   numFormId = @FormID
                    AND numDomainID = @numDomainID
                    AND ISNULL(bitCustom, 0) = 0   
                    AND ISNULL(bitSettingField, 0) = 1
                    AND ISNULL(bitDeleted, 0) = 0
            UNION
            SELECT  CONVERT(VARCHAR(9), numFieldID) + '~1' AS numFieldID ,
                    vcFieldName ,
                    '' AS vcDbColumnName ,
                    1 AS Custom ,
                    tintRow AS tintOrder ,
                    vcAssociatedControlType ,
                    numlistid ,
                    '' AS vcLookBackTableName ,
                    numFieldID AS FieldId ,
                    0 AS bitAllowEdit ,
                    0 AS AZordering
            FROM    View_DynamicCustomColumns
            WHERE   numFormID = @FormID
                    AND numDomainID = @numDomainID
                    AND grp_id = @PageID
                    AND vcAssociatedControlType = 'TextBox'
                    AND ISNULL(bitCustom, 0) = 1
            ORDER BY tintOrder 
        END
        ELSE
        BEGIN
            SELECT  CONVERT(VARCHAR(9), numFieldID) + '~0' numFieldID ,
                    ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                    vcDbColumnName ,
                    0 AS Custom ,
                    tintRow AS tintOrder ,
                    vcAssociatedControlType ,
                    numlistid ,
                    vcLookBackTableName ,
                    numFieldID AS FieldId ,
                    bitAllowEdit ,
                    0 AS AZordering
            FROM    View_DynamicColumns
            WHERE   numFormId = @FormID
                    AND numDomainID = @numDomainID
                    AND ISNULL(bitCustom, 0) = 0   
                    AND ISNULL(bitSettingField, 0) = 1
                    AND ISNULL(bitDeleted, 0) = 0
            UNION
            SELECT  CONVERT(VARCHAR(9), numFieldID) + '~1' AS numFieldID ,
                    vcFieldName ,
                    '' AS vcDbColumnName ,
                    1 AS Custom ,
                    tintRow AS tintOrder ,
                    vcAssociatedControlType ,
                    numlistid ,
                    '' AS vcLookBackTableName ,
                    numFieldID AS FieldId ,
                    0 AS bitAllowEdit ,
                    0 AS AZordering
            FROM    View_DynamicCustomColumns
            WHERE   numFormID = @FormID
                    AND numDomainID = @numDomainID
                    AND grp_id = @PageID
                    AND vcAssociatedControlType = 'TextBox'
                    AND ISNULL(bitCustom, 0) = 1
            UNION
            SELECT  '507~0' AS numFieldID ,
                    'Title:A-Z' AS vcFieldName ,
                    'Title:A-Z' AS vcDbColumnName ,
                    1 AS Custom ,
                    0 AS tintOrder ,
                    ' ' AS vcAssociatedControlType ,
                    1 AS numlistid ,
                    '' AS vcLookBackTableName ,
                    507 AS FieldId ,
                    0 AS bitAllowEdit ,
                    1 AS IsFixed
            UNION
            SELECT  '508~0' AS numFieldID ,
                    'Title:Z-A' AS vcFieldName ,
                    'Title:Z-A' AS vcDbColumnName ,
                    1 AS Custom ,
                    0 AS tintOrder ,
                    ' ' AS vcAssociatedControlType ,
                    1 AS numlistid ,
                    '' AS vcLookBackTableName ,
                    508 AS FieldId ,
                    0 AS bitAllowEdit ,
                    1 AS IsFixed
            UNION
            SELECT  '509~0' AS numFieldID ,
                    'Price:Low to High' AS vcFieldName ,
                    'Price:Low to High' AS vcDbColumnName ,
                    1 AS Custom ,
                    0 AS tintOrder ,
                    ' ' AS vcAssociatedControlType ,
                    1 AS numlistid ,
                    '' AS vcLookBackTableName ,
                    509 AS FieldId ,
                    0 AS bitAllowEdit ,
                    1 AS IsFixed
            UNION
            SELECT  '510~0' AS numFieldID ,
                    'Price:High to Low' AS vcFieldName ,
                    'Price:High to Low' AS vcDbColumnName ,
                    1 AS Custom ,
                    0 AS tintOrder ,
                    ' ' AS vcAssociatedControlType ,
                    1 AS numlistid ,
                    '' AS vcLookBackTableName ,
                    510 AS FieldId ,
                    0 AS bitAllowEdit ,
                    1 AS IsFixed
            UNION
            SELECT  '511~0' AS numFieldID ,
                    'New Arrival' AS vcFieldName ,
                    'New Arrival' AS vcDbColumnName ,
                    1 AS Custom ,
                    0 AS tintOrder ,
                    ' ' AS vcAssociatedControlType ,
                    1 AS numlistid ,
                    '' AS vcLookBackTableName ,
                    511 AS FieldId ,
                    0 AS bitAllowEdit ,
                    1 AS IsFixed
            UNION
            SELECT  '512~0' AS numFieldID ,
                    'Oldest' AS vcFieldName ,
                    'Oldest' AS vcDbColumnName ,
                    1 AS Custom ,
                    0 AS tintOrder ,
                    ' ' AS vcAssociatedControlType ,
                    1 AS numlistid ,
                    '' AS vcLookBackTableName ,
                    512 AS FieldId ,
                    0 AS bitAllowEdit ,
                    1 AS IsFixed
            ORDER BY tintOrder 
        END
    END
	ELSE IF @FormId=43
	BEGIN
            SELECT  CONVERT(VARCHAR(9), numFieldID) + '~0' numFieldID ,
                    ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                    vcDbColumnName ,
                    0 AS Custom ,
                    tintRow AS tintOrder ,
                    vcAssociatedControlType ,
                    numlistid ,
                    vcLookBackTableName ,
                    numFieldID AS FieldId ,
                    bitAllowEdit
            FROM    View_DynamicColumns
            WHERE   numFormId = @FormId
                    AND numUserCntID = @numUserCntID
                    AND numDomainID = @numDomainID
                    AND ISNULL(bitCustom, 0) = 0
                    AND tintPageType = 1
                    AND ISNULL(bitSettingField, 0) = 1
                    AND ISNULL(numRelCntType, 0) = @numtype
                    AND ISNULL(bitDeleted, 0) = 0
                    AND ISNULL(numViewID, 0) = @numViewID
            UNION
            SELECT  CONVERT(VARCHAR(9), numFieldID) + '~1' AS numFieldID ,
                    vcFieldName ,
                    '' AS vcDbColumnName ,
                    1 AS Custom ,
                    tintRow AS tintOrder ,
                    vcAssociatedControlType ,
                    numlistid ,
                    '' AS vcLookBackTableName ,
                    numFieldID AS FieldId ,
                    0 AS bitAllowEdit
            FROM    View_DynamicCustomColumns
            WHERE   numFormID = @FormId
                    AND numUserCntID = @numUserCntID
                    AND numDomainID = @numDomainID
                    AND grp_id = 1
                    AND numDomainID = @numDomainID
                    AND vcAssociatedControlType <> 'Link'
                    AND ISNULL(bitCustom, 0) = 1
                    AND tintPageType = 1
                    AND ISNULL(numRelCntType, 0) = @numtype
                    AND ISNULL(numViewID, 0) = @numViewID
            ORDER BY tintOrder       
	END
	ELSE IF @FormId=141
	BEGIN
		SELECT  
			CONVERT(VARCHAR(9), numFieldID) + '~0' numFieldID ,
            (CASE WHEN @numViewID=6 AND numFieldID=248 THEN 'BizDoc' ELSE ISNULL(vcCultureFieldName, vcFieldName) END) AS vcFieldName ,
            vcDbColumnName,
            0 AS Custom,
            tintRow AS tintOrder,
            vcAssociatedControlType,
            numlistid,
            vcLookBackTableName,
            numFieldID AS FieldId,
            bitAllowEdit
        FROM 
			View_DynamicColumns
        WHERE 
			numFormId = @FormId
            AND numUserCntID = @numUserCntID
            AND numDomainID = @numDomainID
            AND ISNULL(bitCustom, 0) = 0
            AND tintPageType = 1
            AND ISNULL(bitSettingField, 0) = 1
            AND ISNULL(numRelCntType, 0) = @numtype
            AND ISNULL(bitDeleted, 0) = 0
            AND ISNULL(numViewID, 0) = @numViewID
        UNION
        SELECT 
			CONVERT(VARCHAR(9), numFieldID) + '~1' AS numFieldID ,
            vcFieldName ,
            '' AS vcDbColumnName ,
            1 AS Custom ,
            tintRow AS tintOrder ,
            vcAssociatedControlType ,
            numlistid ,
            '' AS vcLookBackTableName ,
            numFieldID AS FieldId ,
            0 AS bitAllowEdit
        FROM 
			View_DynamicCustomColumns
        WHERE 
			numFormID = @FormId
            AND numUserCntID = @numUserCntID
            AND numDomainID = @numDomainID
            AND grp_id IN (2,5)
            AND numDomainID = @numDomainID
            AND vcAssociatedControlType <> 'Link'
            AND ISNULL(bitCustom, 0) = 1
            AND tintPageType = 1
            AND ISNULL(numRelCntType, 0) = @numtype
            AND ISNULL(numViewID, 0) = @numViewID
        ORDER BY 
			tintOrder 
	END
	ELSE IF @FormId=142
	BEGIN
		SELECT  
			CONVERT(VARCHAR(9), numFieldID) + '~0' numFieldID ,
            ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
            vcDbColumnName ,
            0 AS Custom ,
            tintRow AS tintOrder ,
            vcAssociatedControlType ,
            numlistid ,
            vcLookBackTableName ,
            numFieldID AS FieldId ,
            bitAllowEdit
        FROM 
			View_DynamicColumns
        WHERE 
			numFormId = @FormId
            AND numUserCntID = @numUserCntID
            AND numDomainID = @numDomainID
            AND ISNULL(bitCustom, 0) = 0
            AND tintPageType = 1
            AND ISNULL(bitSettingField, 0) = 1
            AND ISNULL(numRelCntType, 0) = @numtype
            AND ISNULL(bitDeleted, 0) = 0
            AND ISNULL(numViewID, 0) = @numViewID
	END
    ELSE
    BEGIN
		SELECT  CONVERT(VARCHAR(9), numFieldID) + '~0' numFieldID ,
				ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
				vcDbColumnName ,
				0 AS Custom ,
				tintRow AS tintOrder ,
				vcAssociatedControlType ,
				numlistid ,
				vcLookBackTableName ,
				numFieldID AS FieldId ,
				bitAllowEdit
		FROM    View_DynamicColumns
		WHERE   numFormId = @FormId
				AND numUserCntID = @numUserCntID
				AND numDomainID = @numDomainID
				AND ISNULL(bitCustom, 0) = 0
				AND tintPageType = 1
				AND ISNULL(bitSettingField, 0) = 1
				AND ISNULL(numRelCntType, 0) = @numtype
				AND ISNULL(bitDeleted, 0) = 0
				AND ISNULL(numViewID, 0) = @numViewID
		UNION
		SELECT  CONVERT(VARCHAR(9), numFieldID) + '~1' AS numFieldID ,
				vcFieldName ,
				'' AS vcDbColumnName ,
				1 AS Custom ,
				tintRow AS tintOrder ,
				vcAssociatedControlType ,
				numlistid ,
				'' AS vcLookBackTableName ,
				numFieldID AS FieldId ,
				0 AS bitAllowEdit
		FROM    View_DynamicCustomColumns
		WHERE   numFormID = @FormId
				AND numUserCntID = @numUserCntID
				AND numDomainID = @numDomainID
				AND 1 = (CASE WHEN @FormId=21 THEN (CASE WHEN  grp_id IN (5,9) THEN 1 ELSE 0 END) ELSE (CASE WHEN  grp_id = @PageId THEN 1 ELSE 0 END) END)
				AND numDomainID = @numDomainID
				AND vcAssociatedControlType <> 'Link'
				AND ISNULL(bitCustom, 0) = 1
				AND tintPageType = 1
				AND ISNULL(numRelCntType, 0) = @numtype
				AND ISNULL(numViewID, 0) = @numViewID
		ORDER BY tintOrder          
    END
GO
/****** Object:  StoredProcedure [dbo].[usp_getDynamicFormFields]    Script Date: 07/26/2008 16:17:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----Created By: Debasish Tapan Nag                                                                          
----Purpose: Returns the available form fields from the database                                                                              
----Created Date: 07/09/2005                                
----exec usp_getDynamicFormFields 1,'',1,1,0                            
----Modified by anoop jayaraj                                                                         
----Modified by Gangadhar
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'usp_getdynamicformfields' ) 
    DROP PROCEDURE usp_getdynamicformfields
GO
CREATE PROCEDURE [dbo].[usp_getDynamicFormFields]
    @numDomainId NUMERIC(9),
    @cCustomFieldsAssociated VARCHAR(10),
    @numFormId INT,
	@numSubFormId NUMERIC(9),
    @numAuthGroupId NUMERIC(9),
	@numBizDocTemplateID numeric(9)
AS 
	DECLARE @vcLocationID VARCHAR(100);SET @vcLocationID='0'
	Select @vcLocationID =isnull(vcLocationID,'0') from dynamicFormMaster where numFormID=@numFormId

    CREATE TABLE #tempAvailableFields(numFormFieldId  NVARCHAR(20),vcFormFieldName NVARCHAR(100),
        vcFieldType CHAR(1),vcAssociatedControlType NVARCHAR(50),numListID NUMERIC(18),vcDbColumnName NVARCHAR(50),
        vcListItemType CHAR(3),intColumnNum INT,intRowNum INT,boolRequired tinyint, numAuthGroupID NUMERIC(18),
          vcFieldDataType CHAR(1),boolAOIField tinyint,numFormID NUMERIC(18),vcLookBackTableName NVARCHAR(50),vcFieldAndType NVARCHAR(50),numSubFormId NUMERIC(18))
   
   
   CREATE TABLE #tempAddedFields(numFormFieldId  NVARCHAR(20),vcFormFieldName NVARCHAR(100),
        vcFieldType CHAR(1),vcAssociatedControlType NVARCHAR(50),numListID NUMERIC(18),vcDbColumnName NVARCHAR(50),
        vcListItemType CHAR(3),intColumnNum INT,intRowNum INT,boolRequired tinyint, numAuthGroupID NUMERIC(18),
          vcFieldDataType CHAR(1),boolAOIField tinyint,numFormID NUMERIC(18),vcLookBackTableName NVARCHAR(50),vcFieldAndType NVARCHAR(50),numSubFormId NUMERIC(18))
 
   INSERT INTO #tempAddedFields (numFormFieldId,vcFormFieldName,vcFieldType,vcAssociatedControlType
        ,numListID,vcDbColumnName,vcListItemType,intColumnNum,intRowNum,boolRequired,numAuthGroupID,
          vcFieldDataType,boolAOIField,numFormID,vcLookBackTableName,vcFieldAndType,numSubFormId)
     SELECT  CONVERT(VARCHAR(15), numFieldId) + vcFieldType AS numFormFieldId,
                    ISNULL(vcCultureFieldName,vcFieldName) AS vcFormFieldName,
                    'R' as vcFieldType,
                    vcAssociatedControlType,
                    numListID,
                    vcDbColumnName,
                    vcListItemType,
                    tintColumn as intColumnNum,
                    tintRow as intRowNum,
                    bitIsRequired as boolRequired,
                    ISNULL(numAuthGroupID, 0) numAuthGroupID,
                    CASE WHEN vcAssociatedControlType = 'DateField' THEN 'D' ELSE vcFieldDataType END vcFieldDataType,
                    ISNULL(boolAOIField, 0) boolAOIField,
                    numFormID,
                    vcLookBackTableName,
                    CONVERT(VARCHAR(15), numFieldId) + '~' +  CONVERT(VARCHAR(15),ISNULL(bitCustom,0)) + '~' + CONVERT(VARCHAR(15),ISNULL(tintOrder,0)) AS vcFieldAndType
					,ISNULL(numSubFormId,0) AS numSubFormId
			FROM    View_DynamicColumns
            WHERE   numFormID = @numFormId 
					AND numAuthGroupID = @numAuthGroupId
                    AND numDomainID = @numDomainId
                    AND isnull(numRelCntType,0)=@numBizDocTemplateID
					AND ISNULL(numSubFormID,0)=ISNULL(@numSubFormId,0)
					AND bitDeleted=0 

            UNION

            SELECT  CONVERT(VARCHAR(15), numFieldId) + 'C' AS numFormFieldId,
                    vcFieldName AS vcFormFieldName,
                    vcFieldType,
					 vcAssociatedControlType,
                    ISNULL(numListID, 0) numListID,
                    vcFieldName vcDbColumnName,
                    CASE WHEN numListID > 0 THEN 'LI' ELSE '' END vcListItemType,
                    ISNULL(tintColumn,0) as intColumnNum,
                    isnull(tintRow,0) as intRowNum,
                    ISNULL(bitIsRequired,0) as boolRequired,
                    ISNULL(numAuthGroupID, 0) numAuthGroupID,
                    CASE WHEN numListID > 0 THEN 'N' WHEN vcAssociatedControlType = 'DateField' THEN 'D' ELSE 'V' END vcFieldDataType,
                    ISNULL(boolAOIField, 0),
                    numFormID,
                    '' AS vcLookBackTableName,
                    CONVERT(VARCHAR(15), numFieldId) + '~' + CONVERT(VARCHAR(15),ISNULL(bitCustom,0)) + '~' + CONVERT(VARCHAR(15),ISNULL(tintOrder,0)) AS vcFieldAndType
					,ISNULL(numSubFormId,0) AS numSubFormId
			FROM    View_DynamicCustomColumns                                            
            WHERE   numDomainID = @numDomainId
                    AND numFormID = @numFormId
                    AND numAuthGroupID = @numAuthGroupId
					AND ISNULL(numSubFormID,0)=ISNULL(@numSubFormId,0)
                    AND GRP_ID IN(	SELECT ID FROM dbo.SplitIDs(@vcLocationID,','))     
					 and isnull(numRelCntType,0)=@numBizDocTemplateID         


        INSERT INTO #tempAvailableFields (numFormFieldId,vcFormFieldName,vcFieldType,vcAssociatedControlType
        ,numListID,vcDbColumnName,vcListItemType,intColumnNum,intRowNum,boolRequired,numAuthGroupID,
          vcFieldDataType,boolAOIField,numFormID,vcLookBackTableName,vcFieldAndType,numSubFormId)                         
            SELECT  CONVERT(VARCHAR(15),numFieldId) + vcFieldType AS numFormFieldId,
                    ISNULL(vcCultureFieldName,vcFieldName) AS vcFormFieldName,
                    'R' as vcFieldType,
                    vcAssociatedControlType,
                    numListID,
                    vcDbColumnName,
                    vcListItemType,
                    0 intColumnNum,
                    0 intRowNum,
                    ISNULL(bitRequired,0) boolRequired,
                    0 numAuthGroupID,
                    CASE WHEN vcAssociatedControlType = 'DateField' THEN 'D' ELSE vcFieldDataType END vcFieldDataType,
                    0 boolAOIField,
                    numFormID,
                    vcLookBackTableName,
                    CONVERT(VARCHAR(15), numFieldId) + '~' + CONVERT(VARCHAR(15),ISNULL(bitCustom,0)) + '~' + CONVERT(VARCHAR(15),ISNULL(tintOrder,0)) AS vcFieldAndType
					,0 AS numSubFormId
			FROM    View_DynamicDefaultColumns
            WHERE   numFormID = @numFormId 
					AND bitDeleted=0
					AND numDomainID=@numDomainID
                    AND numFieldId NOT IN (
                    SELECT  numFieldId
                    FROM    View_DynamicColumns
                    WHERE   numFormID = @numFormId
							AND ISNULL(numSubFormID,0)=ISNULL(@numSubFormId,0)
                            AND numAuthGroupID = @numAuthGroupId
                            AND numDomainID = @numDomainId
                            and isnull(numRelCntType,0)=@numBizDocTemplateID)
           
			UNION
            
			SELECT  CONVERT(VARCHAR(15), Fld_id) + 'C' AS numFormFieldId,
                    Fld_label AS vcFormFieldName,
                    isnull(L.vcFieldType,'C') as vcFieldType,
                    fld_type as vcAssociatedControlType,
                    ISNULL(C.numListID, 0) numListID,
                    Fld_label vcDbColumnName,
                    CASE WHEN C.numListID > 0 THEN 'LI' ELSE '' END vcListItemType,
                    0 intColumnNum,
                    0 intRowNum,
                    ISNULL(V.bitIsRequired,0) as boolRequired,
                    0 numAuthGroupID,
                    CASE WHEN C.numListID > 0 THEN 'N' ELSE 'V' END vcFieldDataType,
                    0 boolAOIField,
                    @numFormId AS numFormID,
                    '' AS vcLookBackTableName,
                    CONVERT(VARCHAR(15), Fld_id) + '~1' + '~0' AS vcFieldAndType
					,0 AS numSubFormId
            FROM    CFW_Fld_Master C LEFT JOIN CFW_Validation V ON V.numFieldID = C.Fld_id
					JOIN CFW_Loc_Master L on C.GRP_ID=L.Loc_Id
            WHERE   C.numDomainID = @numDomainId
					AND GRP_ID IN(	SELECT ID FROM dbo.SplitIDs(@vcLocationID,','))
                    AND Fld_id NOT IN (
                    SELECT  numFieldId
                    FROM    View_DynamicCustomColumns
                    WHERE   numFormID = @numFormId
							AND ISNULL(numSubFormID,0)=ISNULL(@numSubFormId,0)
							AND GRP_ID IN(	SELECT ID FROM dbo.SplitIDs(@vcLocationID,','))
                            AND numAuthGroupID = @numAuthGroupId
                            AND numDomainID = @numDomainId and isnull(numRelCntType,0)=@numBizDocTemplateID)
        

		SELECT * FROM #tempAvailableFields WHERE numFormFieldId NOT IN (SELECT numFormFieldId FROM #tempAddedFields)
		AND #tempAvailableFields.vcLookBackTableName = (CASE @numAuthGroupId WHEN 40911 THEN 'CSGrid' ELSE REPLACE(#tempAvailableFields.vcLookBackTableName,'CSGrid','') END)
		 UNION            
        SELECT * FROM #tempAddedFields WHERE 1=1
		AND #tempAddedFields.vcLookBackTableName = (CASE @numAuthGroupId WHEN 40911 THEN 'CSGrid' ELSE REPLACE(#tempAddedFields.vcLookBackTableName,'CSGrid','') END)
        
        DROP TABLE #tempAvailableFields
        DROP TABLE #tempAddedFields
GO
--Created by anoop jayaraj

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetPOItemsForFulfillment')
DROP PROCEDURE USP_GetPOItemsForFulfillment
GO
CREATE PROCEDURE [dbo].[USP_GetPOItemsForFulfillment]
	@numDomainID as numeric(9),
	@numUserCntID numeric(9)=0,                                                                                                               
	@SortChar char(1)='0',                                                            
	@CurrentPage int,                                                              
	@PageSize int,                                                              
	@TotRecs int output,                                                              
	@columnName as Varchar(50),                                                              
	@columnSortOrder as Varchar(10),
	@numRecordID NUMERIC(18,0),
	@FilterBy TINYINT,
	@Filter VARCHAR(300),
	@ClientTimeZoneOffset INT,
	@numVendorInvoiceBizDocID NUMERIC(18,0)
AS
BEGIN
	DECLARE @firstRec AS INTEGER                                                              
	DECLARE @lastRec AS INTEGER                                                     
	SET @firstRec = (@CurrentPage - 1) * @PageSize                             
	SET @lastRec = (@CurrentPage * @PageSize + 1 ) 

	IF LEN(ISNULL(@columnName,''))=0
		SET @columnName = 'Opp.numOppID'
		
	IF LEN(ISNULL(@columnSortOrder,''))=0	
		SET @columnSortOrder = 'DESC'

	CREATE TABLE #tempForm 
	(
		ID INT IDENTITY(1,1), tintOrder TINYINT,vcDbColumnName nvarchar(50),vcOrigDbColumnName nvarchar(50),vcFieldName nvarchar(50),vcAssociatedControlType nvarchar(50),vcListItemType varchar(3),
		numListID numeric(9),vcLookBackTableName varchar(50),bitCustomField BIT,numFieldId NUMERIC,bitAllowSorting BIT,bitAllowEdit BIT,bitIsRequired BIT,bitIsEmail BIT,
		bitIsAlphaNumeric BIT,bitIsNumeric BIT,bitIsLengthValidation BIT,intMaxLength int,intMinLength int,bitFieldMessage BIT,vcFieldMessage VARCHAR(500),
		ListRelID numeric(9),intColumnWidth int,bitAllowFiltering BIT,vcFieldDataType CHAR(1)
	)

	-- FIRST CHECK IF GRID COLUMNS ARE CONFIGURED
	DECLARE  @Nocolumns  AS TINYINT = 0
	DECLARE @numFormId INT = 135

	SELECT 
		@Nocolumns=ISNULL(SUM(TotalRow),0) 
	FROM
	(            
		SELECT 
			COUNT(*) TotalRow 
		FROM 
			View_DynamicColumns 
		WHERE 
			numFormId=@numFormId 
			AND numViewId = 0
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1
		UNION 
		SELECT 
			COUNT(*) TotalRow 
		FROM 
			View_DynamicCustomColumns 
		WHERE 
			numFormId=@numFormId 
			AND numViewId = 0
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
	) TotalRows

	-- IF GRID COLUMNS ARE NOT CONFIGURES THEN CHECK IF MASTER FORM CONFIGURATION IS CREATED BY ADMINISTRATOR
	DECLARE @IsMasterConfAvailable BIT = 0
	DECLARE @numUserGroup NUMERIC(18,0)

	IF @Nocolumns=0
	BEGIN
		SELECT @numUserGroup = numGroupID FROM UserMaster WHERE numUserDetailId = @numUserCntID 

		IF (SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = @numDomainID AND numFormID = @numFormId AND bitGridConfiguration = 1 AND numGroupID = @numUserGroup) > 0
		BEGIN
			SET @IsMasterConfAvailable = 1
		END
	END

	--IF MASTERCONFIGURATION IS AVAILABLE THEN LOAD IT OTHERWISE LOAD DEFAULT COLUMNS
	IF @IsMasterConfAvailable = 1
	BEGIN
		INSERT INTO DycFormConfigurationDetails 
		(
			numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth
		)
		SELECT 
			@numFormId,numFieldId,intColumnNum,intRowNum,@numDomainID,@numUserCntID,0,1,0,intColumnWidth
		FROM 
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId=@numFormId AND 
			View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
			ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
		UNION
		SELECT 
			@numFormId,numFieldId,intColumnNum,intRowNum,@numDomainID,@numUserCntID,0,1,1,intColumnWidth
		FROM 
			View_DynamicCustomColumnsMasterConfig
		WHERE 
			View_DynamicCustomColumnsMasterConfig.numFormId=@numFormId AND 
			View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1


		INSERT INTO 
			#tempForm
		SELECT 
			(intRowNum + 1) as tintOrder, vcDbColumnName,	vcOrigDbColumnName,	ISNULL(vcCultureFieldName,vcFieldName), vcAssociatedControlType,
			vcListItemType, numListID, vcLookBackTableName,	bitCustom, numFieldId, bitAllowSorting,	bitInlineEdit, bitIsRequired,
			bitIsEmail,	bitIsAlphaNumeric, bitIsNumeric, bitIsLengthValidation,	intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,
			ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType 
		FROM 
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId=@numFormId AND 
			View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
			ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
		UNION
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName, vcAssociatedControlType,'' as vcListItemType,numListID,'',
			bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,''
		FROM 
			View_DynamicCustomColumnsMasterConfig
		WHERE 
			View_DynamicCustomColumnsMasterConfig.numFormId=@numFormId AND 
			View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1
		ORDER BY 
			tintOrder ASC  
	END
	ELSE                                        
	BEGIN
		IF @Nocolumns=0
		BEGIN
			INSERT INTO DycFormConfigurationDetails 
			(
				numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth
			)
			SELECT 
				@numFormId,numFieldId,0,Row_number() over(order by tintRow desc),@numDomainID,@numUserCntID,0,1,0,intColumnWidth
			FROM 
				View_DynamicDefaultColumns
			WHERE 
				numFormId=@numFormId 
				AND bitDefault=1 
				AND ISNULL(bitSettingField,0)=1 
				AND numDomainID=@numDomainID
			ORDER BY 
				tintOrder asc   
		END

		INSERT INTO 
			#tempForm
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcOrigDbColumnName,ISNULL(vcCultureFieldName,vcFieldName),                  
			vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName,bitCustom,numFieldId,bitAllowSorting,bitInlineEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength,bitFieldMessage,
			vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType  
		FROM 
			View_DynamicColumns 
		WHERE 
			numFormId=@numFormId 
			AND numViewId = 0
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND ISNULL(bitSettingField,0)=1 
			AND ISNULL(bitCustom,0)=0  
		UNION
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName,vcAssociatedControlType,'' as vcListItemType,numListID,'',bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,'' 
		FROM 
			View_DynamicCustomColumns
		WHERE 
			numFormId=@numFormId 
			AND numViewId = 0
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND ISNULL(bitCustom,0)=1
		ORDER BY 
			tintOrder asc  
	END

	DECLARE @strColumns VARCHAR(MAX) = ''
    DECLARE @FROM NVARCHAR(MAX) = ''
    DECLARE @WHERE NVARCHAR(MAX) = ''

	SET @FROM =' FROM 
					OpportunityMaster Opp
				INNER JOIN DivisionMaster DM ON Opp.numDivisionId = DM.numDivisionID
				INNER JOIN CompanyInfo cmp ON cmp.numCompanyID=DM.numCompanyID
				INNER JOIN OpportunityItems OI On OI.numOppId=Opp.numOppId
				INNER JOIN Item I on I.numItemCode=OI.numItemCode
				LEFT JOIN WareHouseItems WI on (CASE WHEN ISNULL(Opp.bitStockTransfer,0) = 1 THEN numToWarehouseItemID ELSE OI.numWarehouseItmsID END)=WI.numWareHouseItemID
				LEFT JOIN Warehouses W on W.numWarehouseID=WI.numWarehouseID
				LEFT JOIN WarehouseLocation WL on WL.numWLocationID =WI.numWLocationID'

	SET @WHERE = CONCAT(' WHERE 
							tintOppType=2 
							AND tintOppstatus=1
							AND tintShipped=0 
							AND ',(CASE WHEN ISNULL(@numVendorInvoiceBizDocID,0) > 0 THEN 'OBDI.numUnitHour - ISNULL(OBDI.numVendorInvoiceUnitReceived,0) > 0' ELSE 'OI.numUnitHour - ISNULL(OI.numUnitHourReceived,0) > 0' END),'
							AND I.[charItemType] IN (''P'',''N'',''S'') 
							AND (OI.bitDropShip=0 or OI.bitDropShip IS NULL) 
							AND Opp.numDomainID=',@numDomainID)
	
	IF ISNULL(@numRecordID,0) > 0
	BEGIN
		IF @FilterBy=1 --Item
			set	@WHERE=@WHERE + CONCAT(' AND I.numItemCode = ',@numRecordID)
		ELSE IF @FilterBy=2 --Warehouse
			set	@WHERE=@WHERE + CONCAT(' AND WI.numWarehouseID = ',@numRecordID)
		ELSE IF @FilterBy=3 --Purchase Order
			set	@WHERE=@WHERE + CONCAT(' AND Opp.numOppID = ',@numRecordID)
		ELSE IF @FilterBy=4 --Vendor
			set	@WHERE=@WHERE + CONCAT(' AND DM.numDivisionID = ',@numRecordID)	
		ELSE IF @FilterBy=5 --BizDoc
			set	@WHERE=@WHERE + CONCAT(' AND Opp.numOppID in (SELECT numOppId FROM OpportunityBizDocs WHERE numOppBizDocsId = ',@numRecordID,')')
		ELSE IF @FilterBy=6 --SKU
			set	@WHERE=@WHERE + CONCAT(' AND I.numItemCode = ',@numRecordID)
	END
	ELSE IF LEN(ISNULL(@Filter,'')) > 0
	BEGIN
		IF @FilterBy=1 --Item
			set	@WHERE=@WHERE + ' and OI.vcItemName like ''%' + @Filter + '%'''
		ELSE IF @FilterBy=2 --Warehouse
			set	@WHERE=@WHERE + ' and W.vcWareHouse like ''%' + @Filter + '%'''
		ELSE IF @FilterBy=3 --Purchase Order
			set	@WHERE=@WHERE + ' and Opp.vcPOppName like ''%' + @Filter + '%'''
		ELSE IF @FilterBy=4 --Vendor
			set	@WHERE=@WHERE + ' and cmp.vcCompanyName LIKE ''%' + @Filter + '%'''		
		ELSE IF @FilterBy=5 --BizDoc
			set	@WHERE=@WHERE + ' and Opp.numOppID in (SELECT numOppId FROM OpportunityBizDocs WHERE vcBizDocID like ''%' + @Filter + '%'')'
		ELSE IF @FilterBy=6 --SKU
			set	@WHERE=@WHERE + ' and  I.vcSKU LIKE ''%' + @Filter + '%'' OR WI.vcWHSKU LIKE ''%' + @Filter + '%'')'
	END

	IF ISNULL(@numVendorInvoiceBizDocID,0) > 0
	BEGIN
		SET @FROM = @FROM + ' INNER JOIN OpportunityBizDocs OBD ON OBD.numOppID=Opp.numOppID INNER JOIN OpportunityBizDocItems OBDI ON OBD.numOppBizDocsId = OBDI.numOppBizDocID AND OBDI.numOppItemID=OI.numoppitemtCode'
		SET	@WHERE=@WHERE + CONCAT(' AND OBD.numOppBizDocsId = ',@numVendorInvoiceBizDocID)
	END


	DECLARE @strSql NVARCHAR(MAX)
    DECLARE @SELECT NVARCHAR(MAX) = CONCAT('WITH POItems AS 
									(SELECT
										Row_number() OVER(ORDER BY ',@columnName,' ',@columnSortOrder,') AS row 
										,Opp.numDomainID
										,Opp.numOppID
										,ISNULL(Opp.bitStockTransfer,0) bitStockTransfer
										,OI.numoppitemtCode
										,CASE WHEN ISNULL(Opp.bitStockTransfer,0) = 1 THEN numToWarehouseItemID ELSE OI.numWarehouseItmsID END numWarehouseItmsID
										,WI.numWarehouseID
										,WI.numWLocationID
										,Opp.numRecOwner
										,DM.tintCRMType
										,ISNULL(DM.numTerID,0) numTerID
										,DM.numDivisionID
										,',(CASE WHEN ISNULL(@numVendorInvoiceBizDocID,0) > 0 THEN 'ISNULL(OBDI.numUnitHour,0)' ELSE 'ISNULL(OI.numUnitHour,0)' END), ' numUnitHour
										,',(CASE WHEN ISNULL(@numVendorInvoiceBizDocID,0) > 0 THEN 'ISNULL(OBDI.numVendorInvoiceUnitReceived,0)' ELSE 'ISNULL(OI.numUnitHourReceived,0)' END), ' numUnitHourReceived
										,ISNULL(Opp.fltExchangeRate,0) AS fltExchangeRate
										,ISNULL(Opp.numCurrencyID,0) AS numCurrencyID
										,ISNULL(I.numIncomeChartAcntId,0) AS itemIncomeAccount
										,ISNULL(I.numAssetChartAcntId,0) AS itemInventoryAsset
										,ISNULL(I.numCOGsChartAcntId,0) AS itemCoGs
										,ISNULL(OI.bitDropShip,0) AS DropShip
										,I.charItemType
										,OI.vcType ItemType
										,ISNULL(OI.numProjectID, 0) numProjectID
										,ISNULL(OI.numClassID, 0) numClassID
										,OI.numItemCode
										,OI.monPrice
										,CONCAT(I.vcItemName,CASE WHEN OI.vcItemName <> I.vcItemName THEN CONCAT('' ('',''Changed in order to: '',OI.vcItemName,'')'') ELSE '''' END) AS vcItemName
										,vcPOppName
										,ISNULL(Opp.bitPPVariance,0) AS bitPPVariance
										,ISNULL(I.bitLotNo,0) as bitLotNo
										,ISNULL(I.bitSerialized,0) AS bitSerialized
										,cmp.vcCompanyName')

	DECLARE @tintOrder  AS TINYINT;SET @tintOrder = 0
	DECLARE @vcFieldName  AS VARCHAR(50)
	DECLARE @vcListItemType  AS VARCHAR(3)
	DECLARE @vcListItemType1  AS VARCHAR(1)
	DECLARE @vcAssociatedControlType VARCHAR(30)
	DECLARE @numListID  AS NUMERIC(9)
	DECLARE @vcDbColumnName VARCHAR(40)
	DECLARE @vcLookBackTableName VARCHAR(2000)
	DECLARE @bitCustom  AS BIT
	Declare @numFieldId as numeric  
	DECLARE @bitAllowSorting AS CHAR(1)   
	DECLARE @bitAllowEdit AS CHAR(1)                 
	DECLARE @vcColumnName AS VARCHAR(500)
	DECLARE @ListRelID AS NUMERIC(9) 

	DECLARE @i AS INT = 1
	DECLARE @iCount AS INT 
	SELECT @iCount = COUNT(*) FROM #tempForm

	WHILE @i <= @iCount
	BEGIN
		SELECT 
			@tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName,@vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,
			@numListID=numListID,@vcLookBackTableName=vcLookBackTableName ,@bitCustom=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,@bitAllowEdit=bitAllowEdit,
			@ListRelID=ListRelID
		FROM 
			#tempForm 
		WHERE
			ID=@i

		IF @bitCustom = 0
		BEGIN
			DECLARE  @Prefix  AS VARCHAR(5)

			IF @vcLookBackTableName = 'AdditionalContactsInformation'
				SET @Prefix = 'ADC.'
			IF @vcLookBackTableName = 'DivisionMaster'
				SET @Prefix = 'DM.'
			IF @vcLookBackTableName = 'OpportunityMaster'
				SET @PreFix = 'Opp.'
			IF @vcLookBackTableName = 'OpportunityItems'
				SET @PreFix = 'OI.'
			IF @vcLookBackTableName = 'Item'
				SET @PreFix = 'I.'
			IF @vcLookBackTableName = 'WareHouseItems'
				SET @PreFix = 'WI.'
			IF @vcLookBackTableName = 'Warehouses'
				SET @PreFix = 'W.'
			IF @vcLookBackTableName = 'WarehouseLocation'
				SET @PreFix = 'WL.'
			IF @vcLookBackTableName = 'CompanyInfo'
				SET @PreFix = 'cmp.'

			SET @vcColumnName= @vcDbColumnName+'~'+ convert(varchar(10),@numFieldId) +'~0'

			IF @vcAssociatedControlType = 'SelectBox'
			BEGIN
				IF @vcDbColumnName = 'numPartner'
				BEGIN
					SET @strColumns=@strColumns+ ' ,(SELECT D.vcPartnerCode+''-''+C.vcCompanyName FROM DivisionMaster AS D LEFT JOIN CompanyInfo AS C ON D.numCompanyID=C.numCompanyID WHERE D.numDivisionID=Opp.numPartner) '+' ['+ @vcColumnName+']'    
					SET @strColumns=@strColumns+ ' ,(SELECT D.vcPartnerCode+''-''+C.vcCompanyName FROM DivisionMaster AS D LEFT JOIN CompanyInfo AS C ON D.numCompanyID=C.numCompanyID WHERE D.numDivisionID=Opp.numPartner) AS vcPartner' 
				END
				ELSE IF @vcDbColumnName = 'tintSource'
				BEGIN
					SET @strColumns = @strColumns + ',ISNULL(dbo.fn_GetOpportunitySourceValue(ISNULL(Opp.tintSource,0),ISNULL(Opp.tintSourceType,0),Opp.numDomainID),''Internal Order'')' + ' [' + @vcColumnName + ']'
				END
				ELSE IF @vcDbColumnName = 'numContactID'
				BEGIN
					SET @strColumns = @strColumns + ',dbo.fn_GetContactName(' + @Prefix + @vcDbColumnName + ') [' + @vcColumnName + ']'
				END
				ELSE IF @vcDbColumnName = 'vcLocation'
				BEGIN
					SET @strColumns = @strColumns + ',CONCAT(W.vcWarehouse,CASE WHEN LEN(ISNULL(WL.vcLocation,'''')) > 0 THEN CONCAT('' ('',WL.vcLocation,'')'') ELSE '''' END) [' + @vcColumnName + ']'
				END
				ELSE IF @vcListItemType = 'LI'
				BEGIN
					SET @strColumns = @strColumns + ',L' + CONVERT(VARCHAR(3),@tintOrder) + '.vcData' + ' [' + @vcColumnName + ']'
					SET @FROM = @FROM + ' LEFT JOIN ListDetails L' + CONVERT(VARCHAR(3),@tintOrder) + ' on L' + CONVERT(VARCHAR(3),@tintOrder) + '.numListItemID=' + @vcDbColumnName
				END
				ELSE IF @vcListItemType = 'U'
				BEGIN
					SET @strColumns = @strColumns + ',dbo.fn_GetContactName(' + @Prefix + @vcDbColumnName + ') [' + @vcColumnName + ']'
				END
				ELSE IF @vcListItemType = 'SGT'
				BEGIN
					SET @strColumns = @strColumns + ',(CASE ' + @Prefix + @vcDbColumnName + ' 
														WHEN 0 THEN ''Service Default''
														WHEN 1 THEN ''Adult Signature Required''
														WHEN 2 THEN ''Direct Signature''
														WHEN 3 THEN ''InDirect Signature''
														WHEN 4 THEN ''No Signature Required''
														ELSE ''''
														END) [' + @vcColumnName + ']'
				END
				ELSE IF @vcListItemType = 'PSS'
				BEGIN
					SET @strColumns = @strColumns + ',ISNULL((SELECT vcShipmentService FROM ShippingService WHERE (numDomainID=Opp.numDomainID OR ISNULL(numDomainID,0)=0) AND numShippingServiceID = ' + @Prefix + @vcDbColumnName + '),'''') [' + @vcColumnName + ']'
				END
            END
			ELSE IF @vcAssociatedControlType = 'DateField'
			BEGIN
				IF @vcDbColumnName = 'dtReleaseDate'
				BEGIN
					SET @strColumns = @strColumns
                                + ',case when convert(varchar(11),'
                                + @Prefix
                                + @vcDbColumnName
                                + ')= convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',getdate())) then ''<b><font color=red>Today</font></b>'''
					SET @strColumns = @strColumns
									+ 'when convert(varchar(11),'
									+ @Prefix
									+ @vcDbColumnName
									+ ')= convert(varchar(11),dateadd(day,-1,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',getdate()))) then ''<b><font color=purple>Yesterday</font></b>'''
					SET @strColumns = @strColumns
									+ 'when convert(varchar(11),'
									+ @Prefix
									+ @vcDbColumnName
									+ ')= convert(varchar(11),dateadd(day,1,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',getdate()))) then''<b><font color=orange>Tommorow</font></b>'' '
					SET @strColumns = @strColumns
									+ 'else dbo.FormatedDateFromDate('
									+ @Prefix
									+ @vcDbColumnName
									+ ','
									+ CONVERT(VARCHAR(10),@numDomainId)
									+ ') end  ['
									+ @vcColumnName + ']'
				END
				ELSE
				BEGIN
					SET @strColumns = @strColumns
                                + ',case when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+','
                                + @Prefix
                                + @vcDbColumnName
                                + ') )= convert(varchar(11),getdate()) then ''<b><font color=red>Today</font></b>'''
					SET @strColumns = @strColumns
									+ 'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+','
									+ @Prefix
									+ @vcDbColumnName
									+ ') )= convert(varchar(11),dateadd(day,-1,getdate())) then ''<b><font color=purple>Yesterday</font></b>'''
					SET @strColumns = @strColumns
									+ 'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+','
									+ @Prefix
									+ @vcDbColumnName
									+ ') )= convert(varchar(11),dateadd(day,1,getdate())) then''<b><font color=orange>Tommorow</font></b>'' '
					SET @strColumns = @strColumns
									+ 'else dbo.FormatedDateFromDate('
									+ @Prefix
									+ @vcDbColumnName
									+ ','
									+ CONVERT(VARCHAR(10),@numDomainId)
									+ ') end  ['
									+ @vcColumnName + ']'
				 END	
            END
            ELSE IF @vcAssociatedControlType = 'TextBox' OR @vcAssociatedControlType = 'TextArea'
            BEGIN
				IF @vcDbColumnName = 'numShipState'
				BEGIN
					SET @strColumns = @strColumns + ',dbo.fn_getOPPAddressState(Opp.numOppId,Opp.numDomainID,2)' + ' [' + @vcColumnName + ']'
				END
				ELSE IF @vcDbColumnName = 'numQtyToShipReceive'
				BEGIN
					IF ISNULL(@numVendorInvoiceBizDocID,0) > 0
					BEGIN
						SET @strColumns = @strColumns + ',ISNULL(OBDI.numUnitHour,0) - ISNULL(OBDI.numVendorInvoiceUnitReceived,0)' + ' [' + @vcColumnName + ']'
					END
					ELSE
					BEGIN
						SET @strColumns = @strColumns + ',ISNULL(OI.numUnitHour,0) - ISNULL(OI.numUnitHourReceived,0)' + ' [' + @vcColumnName + ']'
					END
				END
				ELSE IF @vcDbColumnName = 'vcPathForTImage'
		   		BEGIN
					SET @strColumns = @strColumns + ',ISNULL(II.vcPathForTImage,'''') AS ' + ' ['+ @vcColumnName+']'                  
					SET @FROM = @FROM + ' LEFT JOIN ItemImages II ON II.numItemCode = I.numItemCode AND II.bitDefault = 1'
				END
				ELSE IF @vcDbColumnName = 'SerialLotNo'
				BEGIN
					SET @strColumns = @strColumns + ',SUBSTRING((SELECT '','' + vcSerialNo + CASE WHEN isnull(I.bitLotNo,0)=1 THEN '' ('' + CONVERT(VARCHAR(15),whi.numQty) + '')'' ELSE '''' end 
							FROM OppWarehouseSerializedItem oppI JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID=whi.numWareHouseItmsDTLID where oppI.numOppID=OI.numOppId and oppI.numOppItemID=OI.numoppitemtCode 
							ORDER BY vcSerialNo FOR XML PATH('''')),2,200000)  AS ' + ' ['+ @vcColumnName+']' 
				END
				ELSE IF @vcDbColumnName = 'numUnitHour'
				BEGIN
					IF ISNULL(@numVendorInvoiceBizDocID,0) > 0
					BEGIN
						SET @strColumns = @strColumns + ',ISNULL(OBDI.numUnitHour,0) [' + @vcColumnName + ']'
					END
					ELSE
					BEGIN
						SET @strColumns = @strColumns + ',ISNULL(' + @Prefix + @vcDbColumnName + ',0) [' + @vcColumnName + ']'
					END
				END
				ELSE IF @vcDbColumnName = 'numUnitHourReceived'
				BEGIN
					IF ISNULL(@numVendorInvoiceBizDocID,0) > 0
					BEGIN
						SET @strColumns = @strColumns + ',ISNULL(OBDI.numVendorInvoiceUnitReceived,0) [' + @vcColumnName + ']'
					END
					ELSE
					BEGIN
						SET @strColumns = @strColumns + ',ISNULL(' + @Prefix + @vcDbColumnName + ',0) [' + @vcColumnName + ']'
					END
				END
				ELSE IF @vcDbColumnName = 'vcSKU'
				BEGIN
					SET @strColumns = @strColumns + ',ISNULL(OI.vcSKU,ISNULL(I.vcSKU,'''')) [' + @vcColumnName + ']'
				END
				ELSE IF @vcDbColumnName = 'vcItemName'
				BEGIN
					SET @strColumns = @strColumns + ',CONCAT(I.vcItemName,CASE WHEN OI.vcItemName <> I.vcItemName THEN CONCAT('' ('',''Changed in order to: '',OI.vcItemName,'')'') ELSE '''' END)  [' + @vcColumnName + ']'
				END
				ELSE
				BEGIN
					SET @strColumns = @strColumns + ',' + @Prefix + @vcDbColumnName + ' [' + @vcColumnName + ']'
				END
            END
            ELSE IF  @vcAssociatedControlType='Label'                                              
			BEGIN
				IF @vcDbColumnName = 'numRemainingQty'
				BEGIN
					IF ISNULL(@numVendorInvoiceBizDocID,0) > 0
					BEGIN
						SET @strColumns = @strColumns + ',ISNULL(OBDI.numUnitHour,0) - ISNULL(OBDI.numVendorInvoiceUnitReceived,0)' + ' [' + @vcColumnName + ']'
					END
					ELSE
					BEGIN
						SET @strColumns = @strColumns + ',ISNULL(OI.numUnitHour,0) - ISNULL(OI.numUnitHourReceived,0)' + ' [' + @vcColumnName + ']'
					END
				END
				ELSE
				BEGIN
					SET @strColumns=@strColumns + ',' + @Prefix + @vcDbColumnName +' ['+ @vcColumnName+']'        
				END
			END
        END
		ELSE
        BEGIN
			SET @vcColumnName= @vcDbColumnName +'~'+ convert(varchar(10),@numFieldId)+'~1'

			IF @vcAssociatedControlType = 'TextBox' or @vcAssociatedControlType = 'TextArea'
            BEGIN
              SET @strColumns = @strColumns
                              + ',CFW'
                              + CONVERT(VARCHAR(3),@tintOrder)
                              + '.Fld_Value  ['
                              + @vcColumnName + ']'
              SET @FROM = @FROM
                                      + ' left Join CFW_FLD_Values_Opp CFW'
                                      + CONVERT(VARCHAR(3),@tintOrder)
                                      + ' on CFW'
                                      + CONVERT(VARCHAR(3),@tintOrder)
                                      + '.Fld_Id='
                                      + CONVERT(VARCHAR(10),@numFieldId)
                                      + 'and CFW'
                                      + CONVERT(VARCHAR(3),@tintOrder)
                                      + '.RecId=Opp.numOppid   '
			END
			ELSE
            IF @vcAssociatedControlType = 'CheckBox' 
            BEGIN
                SET @strColumns = @strColumns
                                + ',case when isnull(CFW'
                                + CONVERT(VARCHAR(3),@tintOrder)
                                + '.Fld_Value,0)=0 then 0 when isnull(CFW'
                                + CONVERT(VARCHAR(3),@tintOrder)
                                + '.Fld_Value,0)=1 then 1 end   ['
                                + @vcColumnName + ']'
                SET @FROM = @FROM
                                        + ' left Join CFW_FLD_Values_Opp CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + ' on CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.Fld_Id='
                                        + CONVERT(VARCHAR(10),@numFieldId)
                                        + 'and CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.RecId=Opp.numOppid   '
            END
            ELSE
            IF @vcAssociatedControlType = 'DateField'
            BEGIN
                  SET @strColumns = @strColumns
                                  + ',dbo.FormatedDateFromDate(CFW'
                                  + CONVERT(VARCHAR(3),@tintOrder)
                                  + '.Fld_Value,'
                                  + CONVERT(VARCHAR(10),@numDomainId)
                                  + ')  ['
                                  + @vcColumnName + ']'
                  SET @FROM = @FROM
                                          + ' left Join CFW_FLD_Values_Opp CFW'
                                          + CONVERT(VARCHAR(3),@tintOrder)
                                          + ' on CFW'
                                          + CONVERT(VARCHAR(3),@tintOrder)
                                          + '.Fld_Id='
                                          + CONVERT(VARCHAR(10),@numFieldId)
                                          + 'and CFW'
                                          + CONVERT(VARCHAR(3),@tintOrder)
                                          + '.RecId=Opp.numOppid    '
            END
            ELSE
            IF @vcAssociatedControlType = 'SelectBox'
            BEGIN
                SET @vcDbColumnName = 'DCust'
                                            + CONVERT(VARCHAR(10),@numFieldId)
                SET @strColumns = @strColumns
                                + ',L'
                                + CONVERT(VARCHAR(3),@tintOrder)
                                + '.vcData'
                                + ' ['
                                + @vcColumnName + ']'

                SET @FROM = @FROM
                                        + ' left Join CFW_FLD_Values_Opp CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + ' on CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.Fld_Id='
                                        + CONVERT(VARCHAR(10),@numFieldId)
                                        + 'and CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.RecId=Opp.numOppid '

                SET @FROM = @FROM
                                        + ' left Join ListDetails L'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + ' on L'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.numListItemID=CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.Fld_Value'
            END
			ELSE
			BEGIN
				SET @strColumns = @strColumns + CONCAT(',dbo.GetCustFldValueOpp(',@numFieldId,',opp.numOppID)') + ' [' + @vcColumnName + ']'
			END
		END

		SET @i = @i + 1
	END

	SET @strSql = @SELECT + @strColumns + @FROM + @WHERE + ') SELECT * INTO #tempTable FROM [POItems] WHERE row > ' + CONVERT(VARCHAR(10), @firstRec) + ' and row <' + CONVERT(VARCHAR(10), @lastRec)

	PRINT CAST(@strSql AS NTEXT)
	SET @strSql = @strSql + '; (SELECT @TotRecs =COUNT(*) ' + @FROM + @WHERE + ')' + '; SELECT * FROM #tempTable ORDER BY [row]; DROP TABLE #tempTable;'

	EXEC sp_executesql 
        @query = @strSql, 
        @params = N'@TotRecs INT OUTPUT', 
        @TotRecs = @TotRecs OUTPUT 

	SELECT * FROM #tempForm
	DROP TABLE #tempForm
END
GO



/****** Object:  StoredProcedure [dbo].[usp_GetUpdatableFieldList]    Script Date: 07/26/2008 16:18:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By: Debasish Tapan Nag                          
--Purpose: Returns the list of fields from the database that can be mass updated            
--Created Date: 18/03/2006            
                              
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getupdatablefieldlist')
DROP PROCEDURE usp_getupdatablefieldlist
GO
CREATE PROCEDURE [dbo].[usp_GetUpdatableFieldList]            
 @numDomainID numeric (9),
 @numFormID NUMERIC = 1
AS   

IF @numFormID = 1 
BEGIN
	SELECT vcFieldName as vcFormFieldName, vcOrigDbColumnName+'~'+vcLookBackTableName+'~'+ vcListItemType+'~'+convert(varchar(10),numListID)+'~'+vcFieldDataType+'~'+vcAssociatedControlType+'~'+convert(varchar(10),numFieldId) +'~'+ vcDbColumnName as Value        
		 FROM View_DynamicDefaultColumns            
		 WHERE numDomainID=@numDomainID and bitAllowEdit = 1            
		 AND bitDeleted = 0 and numFormID=@numFormID
	 UNION  
		 select Fld_label,'Custom~'+case when grp_id=1 then 'DivisionMaster~LI~' when grp_id=4 then 'Contacts~LI~' end + convert(varchar(10),isnull(numlistid,0))+'~'+Case Fld_type WHEN 'SelectBox' THEN 'N~' ELSE 'V~' END + Fld_type +'~'+convert(varchar(10),fld_id) +'~' as value  
		 from  CFW_Fld_Master  
		 where (Grp_id=1 or Grp_id=4) and fld_type<>'link' and numDomainID=@numDomainID
	 UNION  
		select 'Organization Record Owner','Ownership~DivisionMaster~~~~SelectBox~~~' as value  
	 UNION  
		select 'Contact Record Owner','Ownership~Contacts~~~~SelectBox~~~' as value	
	 UNION  
		select 'Drip Campaign','DripCampaign~Contacts~~~~SelectBox~~~' as value	
END         
ELSE IF @numFormID = 15
BEGIN
	SELECT vcFieldName as vcFormFieldName, vcOrigDbColumnName+'~'+vcLookBackTableName+'~'+ vcListItemType+'~'+convert(varchar(10),numListID)+'~'+vcFieldDataType+'~'+vcAssociatedControlType+'~'+convert(varchar(10),numFieldId) +'~'+ vcDbColumnName as Value        
		 FROM View_DynamicDefaultColumns            
		 WHERE numDomainID=@numDomainID and bitAllowEdit = 1            
		 AND bitDeleted = 0 and numFormID=@numFormID and vcOrigDbColumnName!='numContactType'
END 
ELSE IF @numFormID = 29 -- adv search (item search)
BEGIN
	SELECT vcFieldName as vcFormFieldName, vcOrigDbColumnName+'~'+vcLookBackTableName+'~'+ vcListItemType+'~'+convert(varchar(10),numListID)+'~'+vcFieldDataType+'~'+vcAssociatedControlType+'~'+convert(varchar(10),numFieldId) +'~'+ vcDbColumnName as Value        
		 FROM View_DynamicDefaultColumns            
		 WHERE numDomainID=@numDomainID and bitAllowEdit = 1            
		 AND bitDeleted = 0 and numFormID=@numFormID AND vcLookBackTableName <> 'WareHouseItmsDTL'
		 AND vcDbColumnName <> 'vcPartNo' AND vcDbColumnName <> 'monAverageCost'
		 AND vcOrigDbColumnName <> 'numAllocation' AND vcOrigDbColumnName <> 'numBackOrder'
		 AND vcOrigDbColumnName <> 'numOnHand' AND vcOrigDbColumnName <> 'numOnOrder'
UNION 
		 SELECT vcFieldName as vcFormFieldName, vcOrigDbColumnName+'~'+vcLookBackTableName+'~'+ ISNULL(vcListItemType,'') +'~'+convert(varchar(10),numListID)+'~'+vcFieldDataType+'~'+vcAssociatedControlType+'~'+convert(varchar(10),numFieldId) +'~'+ vcDbColumnName as Value        
		 FROM View_DynamicDefaultColumns            
		 WHERE numDomainID=@numDomainID and numFormID=29 AND vcLookBackTableName = 'WareHouseItems'
		  AND vcOrigDbColumnName <> 'numAllocation' AND vcOrigDbColumnName <> 'numBackOrder'
		 AND vcOrigDbColumnName <> 'numOnHand' AND vcOrigDbColumnName <> 'numOnOrder'
Union 
		 SELECT 'Tax : ' + isnull(vcTaxName,'') as vcFormFieldName, 'Tax~Tax~~0~Y~CheckBox~'+convert(varchar(10),numTaxItemId)+'~' as Value        
		 FROM TaxItems            
		 WHERE numDomainID=@numDomainID 
END

 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_InLineEditDataSave')
DROP PROCEDURE USP_InLineEditDataSave
GO
CREATE PROCEDURE [dbo].[USP_InLineEditDataSave] 
( 
 @numDomainID NUMERIC(9),
 @numUserCntID NUMERIC(9),
 @numFormFieldId NUMERIC(9),
 @bitCustom AS BIT = 0,
 @InlineEditValue VARCHAR(8000),
 @numContactID NUMERIC(9),
 @numDivisionID NUMERIC(9),
 @numWOId NUMERIC(9),
 @numProId NUMERIC(9),
 @numOppId NUMERIC(9),
 @numRecId NUMERIC(9),
 @vcPageName NVARCHAR(100),
 @numCaseId NUMERIC(9),
 @numWODetailId NUMERIC(9),
 @numCommID NUMERIC(9)
 )
AS 

declare @strSql as nvarchar(4000)
DECLARE @vcDbColumnName AS VARCHAR(100),@vcOrigDbColumnName AS VARCHAR(100),@vcLookBackTableName AS varchar(100)
DECLARE @vcListItemType as VARCHAR(3),@numListID AS numeric(9),@vcAssociatedControlType varchar(20)
declare @tempAssignedTo as numeric(9);set @tempAssignedTo=null           
DECLARE @Value1 AS VARCHAR(18),@Value2 AS VARCHAR(18)

IF  @bitCustom =0
BEGIN
	SELECT @vcDbColumnName=vcDbColumnName,@vcLookBackTableName=vcLookBackTableName,
		   @vcListItemType=vcListItemType,@numListID=numListID,@vcAssociatedControlType=vcAssociatedControlType,
		   @vcOrigDbColumnName=vcOrigDbColumnName
	 FROM DycFieldMaster WHERE numFieldId=@numFormFieldId
	 
	IF @vcAssociatedControlType = 'DateField' AND  @InlineEditValue=''
	BEGIN
		SET @InlineEditValue = null
	END

	DECLARE @intExecuteDiv INT=0
	if @vcLookBackTableName = 'DivisionMaster' 
	 BEGIN 
		
	   IF @vcDbColumnName='numFollowUpStatus' --Add to FollowUpHistory
	    Begin
			IF(@numCommID>0 AND @numDivisionID=0)
			BEGIN
				SET @numDivisionID=(SELECT TOP 1 numDivisionId FROM Communication WHERE numCommId=@numCommID)
			END
			declare @numFollow as varchar(10),@binAdded as varchar(20),@PreFollow as varchar(10),@RowCount as varchar(2)                            
			                      
			set @PreFollow=(select count(*)  numFollowUpStatus from FollowUpHistory where numDivisionID= @numDivisionID and numDomainId= @numDomainID)                          
			set @RowCount=@@rowcount  
			                        
			select   @numFollow=numFollowUpStatus, @binAdded=bintModifiedDate from divisionmaster where numDivisionID=@numDivisionID and numDomainId= @numDomainID                    
			
			if @numFollow <>'0' and @numFollow <> @InlineEditValue                          
			begin                          
				if @PreFollow<>0                          
					begin                          
			                   
						if @numFollow <> (select top 1  numFollowUpStatus from FollowUpHistory where numDivisionID= @numDivisionID and numDomainId= @numDomainID order by numFollowUpStatusID desc)                          
							begin                          
			                      	insert into FollowUpHistory(numFollowUpstatus,numDivisionID,bintAddedDate,numDomainId)                          
									values(@numFollow,@numDivisionID,@binAdded,@numDomainID)                          
							end                          
					end                          
				else                          
					begin                          
						insert into FollowUpHistory(numFollowUpstatus,numDivisionID,bintAddedDate,numDomainId)                          
						 values(@numFollow,@numDivisionID,@binAdded,@numDomainID)                          
					end                          
			   end 
			   
			
			UPDATE  DivisionMaster SET  dtLastFollowUp=getutcdate() where numDivisionID=@numDivisionID and numDomainID=@numDomainID
		END
		 IF @vcDbColumnName='numAssignedTo' ---Updating if organization is assigned to someone
			BEGIN
				select @tempAssignedTo=isnull(numAssignedTo,0) from DivisionMaster where numDivisionID = @numDivisionID and numDomainId= @numDomainID           
			
				if (@tempAssignedTo<>@InlineEditValue and  @InlineEditValue<>'0')          
					begin            
						update DivisionMaster set numAssignedTo=@InlineEditValue ,numAssignedBy=@numUserCntID where numDivisionID = @numDivisionID  and numDomainId= @numDomainID  
						UPDATE CompanyInfo SET bintModifiedDate=GETUTCDATE() WHERE numCompanyId IN (SELECT numCompanyId 
																									FROM 
																										DivisionMaster
																									WHERE 
																										numDivisionID = @numDivisionID  and numDomainId= @numDomainID)         
					end					
				else if  (@InlineEditValue ='0')          
					begin          
						update DivisionMaster set numAssignedTo=0 ,numAssignedBy=0 where numDivisionID = @numDivisionID  and numDomainId= @numDomainID
						UPDATE CompanyInfo SET bintModifiedDate=GETUTCDATE() WHERE numCompanyId IN (SELECT numCompanyId 
																									FROM 
																										DivisionMaster
																									WHERE 
																										numDivisionID = @numDivisionID  and numDomainId= @numDomainID)          
					end    
			END
	    UPDATE CompanyInfo SET numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() WHERE numCompanyId=(SELECT TOP 1 numCompanyId FROM DivisionMaster where numDivisionID=@numDivisionID and numDomainID=@numDomainID) and numDomainID=@numDomainID
		
		IF @vcDbColumnName='vcPartnerCode'
		BEGIN
			IF(@InlineEditValue<>'')
			BEGIN
				IF EXISTS(SELECT vcPartnerCode FROM DivisionMaster where numDivisionID<>@numDivisionID and numDomainID=@numDomainID AND vcPartnerCode=@InlineEditValue)
				BEGIN
					SET @intExecuteDiv=1
					SET @InlineEditValue='-#12'
				END
			END
		END
		IF @intExecuteDiv=0
		BEGIN
			SET @strSql='update DivisionMaster set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numDivisionID=@numDivisionID and numDomainID=@numDomainID'
			
		END
	 END 
	 ELSE if @vcLookBackTableName = 'AdditionalContactsInformation' 
	 BEGIN 
			IF @vcDbColumnName='bitPrimaryContact' AND @InlineEditValue='True'
			BEGIN
				DECLARE @bitPrimaryContactCheck AS BIT             
				DECLARE @numID AS NUMERIC(9) 
				
				SELECT TOP 1 @numID = numContactID,@bitPrimaryContactCheck = ISNULL(bitPrimaryContact,0)
					FROM AdditionalContactsInformation WHERE numDivisionID = @numDivisionID 
                   
					IF @@rowcount = 0 SET @numID = 0             
				
					WHILE @numID > 0            
					BEGIN            
						IF @bitPrimaryContactCheck = 1 
							UPDATE  AdditionalContactsInformation SET bitPrimaryContact = 0 WHERE numContactID = @numID  
                              
						SELECT TOP 1 @numID = numContactID,@bitPrimaryContactCheck = ISNULL(bitPrimaryContact,0)
						FROM AdditionalContactsInformation WHERE numDivisionID = @numDivisionID AND numContactID > @numID    
                                
						IF @@rowcount = 0 SET @numID = 0             
					END 
				END
				ELSE IF @vcDbColumnName='bitPrimaryContact' AND @InlineEditValue='False'
				BEGIN
					IF (SELECT COUNT(*) FROM AdditionalContactsInformation WHERE numDivisionID = @numDivisionID AND numContactId!=@numContactId AND ISNULL(bitPrimaryContact,0)=1) = 0
						SET @InlineEditValue='True'
				END 
				-- Added by Priya(to check no Campaign got engaged wothout contacts email
				ELSE IF @vcDbColumnName = 'numECampaignID' 
				BEGIN
					
					IF EXISTS (select 1 from AdditionalContactsInformation WHERE numContactId = @numContactID AND (vcEmail IS NULL OR vcEmail = ''))
					BEGIN
						RAISERROR('EMAIL_ADDRESS_REQUIRED',16,1)
								RETURN
					END

				END
				
		SET @strSql='update AdditionalContactsInformation set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numDivisionID=@numDivisionID and numDomainID=@numDomainID and numContactId=@numContactId'
	 END 
	 ELSE if @vcLookBackTableName = 'CompanyInfo' 
	 BEGIN 
		SET @strSql='update CompanyInfo set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numCompanyId=(select numCompanyId from divisionmaster where numDivisionID=@numDivisionID and numDomainID=@numDomainID) and numDomainID=@numDomainID'
	 END 
	 ELSE if @vcLookBackTableName = 'WorkOrder' 
	 BEGIN 
		DECLARE @numItemCode AS NUMERIC(9)
	 
		IF @numWODetailId>0
		BEGIN
			SET @strSql='update WorkOrderDetails set ' + @vcDbColumnName + '=@InlineEditValue where numWOId=@numWOId and numWODetailId=@numWODetailId'
			
			IF @vcDbColumnName='numQtyItemsReq'
			BEGIN 
				SELECT @numItemCode=numChildItemID FROM WorkOrderDetails WHERE numWOId=@numWOId and numWODetailId=@numWODetailId
				
				EXEC USP_UpdateWorkOrderDetailQuantity @numItemCode,@InlineEditValue,@numWOId
				--UPDATE WorkOrderDetails SET numQtyItemsReq=CONVERT(NUMERIC(9),numQtyItemsReq_Orig * CONVERT(NUMERIC(9,0),@InlineEditValue)) WHERE numWOId=@numWOId
			END 
		END 	
		ELSE
		BEGIN
			SET @strSql='update WorkOrder set ' + @vcDbColumnName + '=@InlineEditValue where numWOId=@numWOId and numDomainID=@numDomainID'
			
			IF @vcDbColumnName='numQtyItemsReq'
			BEGIN 
				SELECT @numItemCode=numItemCode FROM WorkOrder WHERE numWOId=@numWOId and numDomainID=@numDomainID
			
				EXEC USP_UpdateWorkOrderDetailQuantity @numItemCode,@InlineEditValue,@numWOId
				--UPDATE WorkOrderDetails SET numQtyItemsReq=CONVERT(NUMERIC(9),numQtyItemsReq_Orig * CONVERT(NUMERIC(9,0),@InlineEditValue)) WHERE numWOId=@numWOId
			END 
		END	
	 END
	 ELSE if @vcLookBackTableName = 'ProjectsMaster' 
	 BEGIN 
		IF @vcDbColumnName='numAssignedTo' ---Updating if Project is assigned to someone 
			BEGIN
				select @tempAssignedTo=isnull(numAssignedTo,0) from ProjectsMaster where numProId=@numProId and numDomainId= @numDomainID           
			
				if (@tempAssignedTo<>@InlineEditValue and  @InlineEditValue<>'0')          
					begin            
						update ProjectsMaster set numAssignedTo=@InlineEditValue ,numAssignedBy=@numUserCntID,bintModifiedDate=GETUTCDATE() where numProId = @numProId  and numDomainId= @numDomainID           
					end           
				else if  (@InlineEditValue ='0')          
					begin          
						update ProjectsMaster set numAssignedTo=0 ,numAssignedBy=0,bintModifiedDate=GETUTCDATE() where numProId = @numProId  and numDomainId= @numDomainID        
					end    
			END
		else IF @vcDbColumnName='numProjectStatus'  ---Updating All Statge to 100% if Completed
			BEGIN
				IF @InlineEditValue='27492'
				BEGIN
					UPDATE PM SET dtCompletionDate=GETUTCDATE(),monTotalExpense=PIE.monExpense,
					monTotalIncome=PIE.monIncome,monTotalGrossProfit=PIE.monBalance,numProjectStatus=27492
					FROM ProjectsMaster PM,dbo.GetProjectIncomeExpense(@numDomainId,@numProId) PIE
					WHERE PM.numProId=@numProId
					
					EXEC USP_ManageProjectCommission @numDomainId,@numProId
						
					Update StagePercentageDetails set tinProgressPercentage=100,bitclose=1 where numProjectID=@numProId

					EXEC dbo.USP_GetProjectTotalProgress
							@numDomainID = @numDomainID, --  numeric(9, 0)
							@numProId = @numProId, --  numeric(9, 0)
							@tintMode = 1 --  tinyint
				END			
				ELSE
				BEGIN
					DELETE from BizDocComission WHERE numDomainID = @numDomainID AND numProId=@numProId AND numComissionID 
						NOT IN (SELECT BC.numComissionID FROM dbo.BizDocComission BC JOIN dbo.PayrollTracking PT 
							ON BC.numComissionID = PT.numComissionID AND BC.numDomainId = PT.numDomainId WHERE BC.numDomainID = @numDomainID AND ISNULL(BC.numProId,0) = @numProId)
				END
			END
			

		SET @strSql='update ProjectsMaster set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numProId=@numProId and numDomainID=@numDomainID'
	 END 
	 ELSE if @vcLookBackTableName = 'OpportunityMaster' 
	 BEGIN 
		IF @vcDbColumnName='numAssignedTo' ---Updating if organization is assigned to someone  
			BEGIN
				select @tempAssignedTo=isnull(numAssignedTo,0) from OpportunityMaster where numOppId=@numOppId and numDomainId= @numDomainID           
			
				if (@tempAssignedTo<>@InlineEditValue and  @InlineEditValue<>'0')          
					begin            
						update OpportunityMaster set numAssignedTo=@InlineEditValue ,numAssignedBy=@numUserCntID,bintModifiedDate=GETUTCDATE() where numOppId = @numOppId  and numDomainId= @numDomainID           
					end           
				else if  (@InlineEditValue ='0')          
					begin          
						update OpportunityMaster set numAssignedTo=0 ,numAssignedBy=0,bintModifiedDate=GETUTCDATE() where numOppId = @numOppId  and numDomainId= @numDomainID        
					end    
			END
		ELSE IF @vcDbColumnName='numPartner' OR @vcDbColumnName='numPartenerSource'
		 BEGIN
			update OpportunityMaster set numPartner=@InlineEditValue ,bintModifiedDate=GETUTCDATE() where numOppId = @numOppId  and numDomainId= @numDomainID           
		 END
		
		ELSE IF @vcDbColumnName='tintActive'
		 BEGIN
		 	SET @InlineEditValue= CASE @InlineEditValue WHEN 'True' THEN 1 ELSE 0 END 
		 END
		 ELSE IF @vcDbColumnName='tintSource'
		 BEGIN
			IF CHARINDEX('~', @InlineEditValue)>0
			BEGIN
				SET @Value1=SUBSTRING(@InlineEditValue, 1,CHARINDEX('~', @InlineEditValue)-1) 
				SET @Value2=SUBSTRING(@InlineEditValue, CHARINDEX('~', @InlineEditValue)+1,LEN(@InlineEditValue)) 
			END	
			ELSE
			BEGIN
				SET @Value1=@InlineEditValue
				SET @Value2=0
			END
			
		 	update OpportunityMaster set tintSourceType=@Value2 where numOppId = @numOppId  and numDomainId= @numDomainID        
		 	
		 	SET @InlineEditValue= @Value1
		 END
		ELSE IF @vcDbColumnName='tintOppStatus' --Update Inventory  0=Open,1=Won,2=Lost
		 BEGIN			
			DECLARE @bitViolatePrice BIT
			DECLARE @intPendingApprovePending INT
		
			IF EXISTS (SELECT numApprovalRuleID FROM ApprovalProcessItemsClassification WHERE numDomainId=@numDomainID AND ISNULL(bitMarginPriceViolated,0) = 1)
			BEGIN
				SET @bitViolatePrice = 1
			END
			SET @intPendingApprovePending=(SELECT COUNT(*) FROM OpportunityItems WHERE numOppId=@numOppId AND ISNULL(bitItemPriceApprovalRequired,0)=1)

			IF(@intPendingApprovePending > 0 AND @bitViolatePrice=1 AND @InlineEditValue='1')
			BEGIN
				SET @intExecuteDiv=1
				SET @InlineEditValue='-#123'
			END

			IF ISNULL(@intExecuteDiv,0)=0
			BEGIN
				DECLARE @tintOppStatus AS TINYINT
				SELECT @tintOppStatus=tintOppStatus FROM OpportunityMaster WHERE numOppID=@numOppID              

				IF @tintOppStatus=0 AND @InlineEditValue='1' --Open to Won
				BEGIN
					SELECT @numDivisionID=numDivisionID FROM OpportunityMaster WHERE numOppID=@numOppID   
					
					DECLARE @tintCRMType AS TINYINT      
					SELECT @tintCRMType=tintCRMType FROM divisionmaster WHERE numDivisionID =@numDivisionID 

					-- Promote Lead to Account when Sales/Purchase Order is created against it
					IF @tintCRMType=0 --Lead & Order
					BEGIN        
						UPDATE 
							divisionmaster 
						SET 
							tintCRMType=2
							,bintLeadProm=GETUTCDATE()
							,bintLeadPromBy=@numUserCntID
							,bintProsProm=GETUTCDATE()
							,bintProsPromBy=@numUserCntID        
						WHERE 
							numDivisionID=@numDivisionID        
					END
					--Promote Prospect to Account
					ELSE IF @tintCRMType=1 
					BEGIN        
						UPDATE 
							divisionmaster 
						SET 
							tintCRMType=2
							,bintProsProm=GETUTCDATE()
							,bintProsPromBy=@numUserCntID        
						WHERE 
							numDivisionID=@numDivisionID        
					END    

		 			EXEC USP_UpdatingInventoryonCloseDeal @numOppID,@numUserCntID

					UPDATE OpportunityMaster SET bintOppToOrder=GETUTCDATE() WHERE numOppId=@numOppId and numDomainID=@numDomainID
				END
				ELSE IF (@InlineEditValue='2') -- Win to Lost
				BEGIN
					UPDATE OpportunityMaster SET dtDealLost=GETUTCDATE() WHERE numOppId=@numOppId and numDomainID=@numDomainID
				END
				
				IF (@tintOppStatus=1 and @InlineEditValue='2') or (@tintOppStatus=1 and @InlineEditValue='0') --Won to Lost or Won to Open
						EXEC USP_RevertDetailsOpp @numOppID,0,@numUserCntID 

				UPDATE OpportunityMaster SET tintOppStatus = @InlineEditValue WHERE numOppId=@numOppID
				
			END
		 END
		 ELSE IF @vcDbColumnName='vcOppRefOrderNo' --Update vcRefOrderNo OpportunityBizDocs
		 BEGIN
			 IF LEN(ISNULL(@InlineEditValue,''))>0
			 BEGIN
	   			UPDATE dbo.OpportunityBizDocs SET vcRefOrderNo=@InlineEditValue WHERE numOppId=@numOppID
			 END
		 END
		 ELSE IF @vcDbColumnName='numStatus' 
		 BEGIN
			DECLARE @tintOppType AS TINYINT
			select @tintOppStatus=tintOppStatus,@tintOppType=tintOppType from OpportunityMaster where numOppID=@numOppID

			IF @tintOppType=1 AND @tintOppStatus=1
			BEGIN
				DECLARE @numOppStatus AS NUMERIC(18,0) = CAST(@InlineEditValue AS NUMERIC(18,0))
				EXEC USP_SalesFulfillmentQueue_Save @numDomainID,@numUserCntID,@numOppID,@numOppStatus
			END

		 	 IF EXISTS (SELECT 1 FROM dbo.OpportunityMaster WHERE numOppId = @numOppID AND tintOppType=1 AND isnull(tintOppStatus,0)=1 AND ISNULL(numStatus,0)!= @InlineEditValue)
			 BEGIN
	   				EXEC USP_ManageOpportunityAutomationQueue
							@numOppQueueID = 0, -- numeric(18, 0)
							@numDomainID = @numDomainID, -- numeric(18, 0)
							@numOppId = @numOppID, -- numeric(18, 0)
							@numOppBizDocsId = 0, -- numeric(18, 0)
							@numOrderStatus = @InlineEditValue, -- numeric(18, 0)
							@numUserCntID = @numUserCntID, -- numeric(18, 0)
							@tintProcessStatus = 1, -- tinyint
							@tintMode = 1 -- TINYINT
			END 
		 END	
		 ELSE IF @vcDbColumnName = 'dtReleaseDate'
		 BEGIN
			IF EXISTS (SELECT 1 FROM OpportunityMaster WHERE numDomainId=@numDomainID AND numOppId=@numOppId AND tintOppType=1 AND tintOppStatus=0)
			BEGIN
				IF (SELECT dbo.GetListIemName(ISNULL(numPercentageComplete,0)) FROM OpportunityMaster WHERE numDomainId=@numDomainID AND numOppId=@numOppId) = '100'
				BEGIN
					SET @strSql='update OpportunityMaster set ' + @vcDbColumnName + '=DATEADD(hh, DATEDIFF(hh, GETDATE(), GETUTCDATE()), @InlineEditValue),numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numOppId=@numOppId and numDomainID=@numDomainID'
				END
				ELSE
				BEGIN
					RAISERROR('PERCENT_COMPLETE_MUST_BE_100',16,1)
					RETURN
				END
			END
			ELSE
			BEGIN
				SET @strSql='update OpportunityMaster set ' + @vcDbColumnName + '=DATEADD(hh, DATEDIFF(hh, GETDATE(), GETUTCDATE()), @InlineEditValue),numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numOppId=@numOppId and numDomainID=@numDomainID'
			END
		END
		ELSE IF @vcDbColumnName = 'numPercentageComplete'
		BEGIN
			IF EXISTS (SELECT 1 FROM OpportunityMaster WHERE numDomainId=@numDomainID AND numOppId=@numOppId AND tintOppType=1 AND tintOppStatus=0 AND dbo.GetListIemName(ISNULL(numPercentageComplete,0))='100')
			BEGIN
				IF EXISTS(SELECT dtReleaseDate FROM OpportunityMaster WHERE numDomainId=@numDomainID AND numOppId=@numOppId AND dtReleaseDate IS NULL)
				BEGIN
					SET @strSql='update OpportunityMaster set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numOppId=@numOppId and numDomainID=@numDomainID'
				END
				ELSE
				BEGIN
					RAISERROR('REMOVE_RELEASE_DATE',16,1)
					RETURN
				END
			END
			ELSE
			BEGIN
				SET @strSql='update OpportunityMaster set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numOppId=@numOppId and numDomainID=@numDomainID'
			END
		END

		IF(@vcDbColumnName!='tintOppStatus')
		BEGIN
			SET @strSql='update OpportunityMaster set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numOppId=@numOppId and numDomainID=@numDomainID'
		END
	 END
	 ELSE if @vcLookBackTableName = 'Cases' 
	 BEGIN 
	 		IF @vcDbColumnName='numAssignedTo' ---Updating if Case is assigned to someone  
			BEGIN
				select @tempAssignedTo=isnull(numAssignedTo,0) from Cases where numCaseId=@numCaseId and numDomainId= @numDomainID           
			
				if (@tempAssignedTo<>@InlineEditValue and  @InlineEditValue<>'0')          
					begin            
						update Cases set numAssignedTo=@InlineEditValue ,numAssignedBy=@numUserCntID,bintModifiedDate=GETUTCDATE() where numCaseId = @numCaseId  and numDomainId= @numDomainID           
					end           
				else if  (@InlineEditValue ='0')          
					begin          
						update Cases set numAssignedTo=0 ,numAssignedBy=0,bintModifiedDate=GETUTCDATE() where numCaseId = @numCaseId  and numDomainId= @numDomainID        
					end    
			END
		UPDATE Cases SET bintModifiedDate=GETUTCDATE() WHERE numCaseId = @numCaseId  and numDomainId= @numDomainID    
		SET @strSql='update Cases set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numCaseId=@numCaseId and numDomainID=@numDomainID'
	 END 
	 ELSE if @vcLookBackTableName = 'Tickler' 
	 BEGIN 	
		SET @strSql='update Communication set ' + @vcOrigDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,dtModifiedDate=getutcdate() where numCommId=@numCommId and numDomainID=@numDomainID'
	 END
	 ELSE IF @vcLookBackTableName = 'ExtarnetAccounts'
	 BEGIN
		IF @vcDbColumnName='bitEcommerceAccess'
		BEGIN
			IF LOWER(@InlineEditValue) = 'yes' OR LOWER(@InlineEditValue) = 'true'
			BEGIN
				IF NOT EXISTS (SELECT * FROM ExtarnetAccounts where numDivisionID=@numDivisionID) 
				BEGIN
					DECLARE @numGroupID NUMERIC
					DECLARE @numCompanyID NUMERIC(18,0)
					SELECT @numCompanyID=numCompanyID FROM DivisionMaster WHERE numDivisionID=@numDivisionID

					SELECT TOP 1 @numGroupID=numGroupID FROM dbo.AuthenticationGroupMaster WHERE numDomainID=@numDomainID AND tintGroupType=2
				
					IF @numGroupID IS NULL SET @numGroupID = 0 
					INSERT INTO ExtarnetAccounts (numCompanyID,numDivisionID,numGroupID,numDomainID)                                                
					VALUES(@numCompanyID,@numDivisionID,@numGroupID,@numDomainID)
				END
			END
			ELSE
			BEGIN
				DELETE FROM ExtranetAccountsDtl WHERE numExtranetID IN (SELECT ISNULL(numExtranetID,0) FROM ExtarnetAccounts where numDivisionID=@numDivisionID)
				DELETE FROM ExtarnetAccounts where numDivisionID=@numDivisionID
			END

			SELECT (CASE WHEN LOWER(@InlineEditValue) = 'yes' OR LOWER(@InlineEditValue) = 'true' THEN 'Yes' ELSE 'No' END) AS vcData

			RETURN
		END
	 END
	ELSE IF @vcLookBackTableName = 'ExtranetAccountsDtl'
	BEGIN
		IF @vcDbColumnName='vcPassword'
		BEGIN
			IF LEN(@InlineEditValue) = 0
			BEGIN
				DELETE FROM ExtranetAccountsDtl WHERE numContactID=@numContactID
			END
			ELSE
			BEGIN
				IF EXISTS (SELECT numExtranetID FROM ExtarnetAccounts WHERE numDivisionID = @numDivisionID)
				BEGIN
					IF EXISTS (SELECT numExtranetDtlID FROM ExtranetAccountsDtl WHERE numContactID=@numContactID)
					BEGIN
						UPDATE
							ExtranetAccountsDtl
						SET 
							vcPassword=@InlineEditValue
							,tintAccessAllowed=1
						WHERE
							numContactID=@numContactID
					END
					ELSE
					BEGIN
						DECLARE @numExtranetID NUMERIC(18,0)
						SELECT @numExtranetID=numExtranetID FROM ExtarnetAccounts WHERE numDivisionID = @numDivisionID

						INSERT INTO ExtranetAccountsDtl (numExtranetID,numContactID,vcPassword,tintAccessAllowed,numDomainID)
						VALUES (@numExtranetID,@numContactID,@InlineEditValue,1,@numDomainID)
					END
				END
				ELSE
				BEGIN
					RAISERROR('Organization e-commerce access is not enabled',16,1)
				END
			END

			SELECT @InlineEditValue AS vcData

			RETURN
		END
	END
	ELSE IF @vcLookBackTableName = 'CustomerPartNumber'
	BEGIN
		IF @vcDbColumnName='CustomerPartNo'
		BEGIN
			SELECT @numCompanyID=numCompanyID FROM DivisionMaster WHERE numDivisionID=@numDivisionID
			
			IF EXISTS(SELECT CustomerPartNoID FROM CustomerPartNumber WHERE numItemCode = @numRecId AND numCompanyID = @numCompanyID)
			BEGIN
				UPDATE CustomerPartNumber SET CustomerPartNo = @InlineEditValue WHERE numItemCode = @numRecId AND numCompanyId = @numCompanyID AND numDomainID = @numDomainId 
			END
			ELSE
			BEGIN
				INSERT INTO CustomerPartNumber
				(numItemCode, numCompanyId, numDomainID, CustomerPartNo)
				VALUES
				(@numRecId, @numCompanyID, @numDomainID, @InlineEditValue)
			END

			SELECT @InlineEditValue AS vcData

			RETURN
		END
		
	END
	ELSE IF @vcLookBackTableName = 'CheckHeader'
	BEGIN
		IF EXISTS (SELECT 1 FROM CheckHeader WHERE numDomainID=@numDomainID AND numCheckNo=@InlineEditValue AND numCheckHeaderID <> @numRecId)
		BEGIN
			RAISERROR('DUPLICATE_CHECK_NUMBER',16,1)
			RETURN
		END

		SET @strSql='UPDATE CheckHeader SET ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,dtModifiedDate=getutcdate() WHERE numCheckHeaderID=@numRecId and numDomainID=@numDomainID'
	END
	--ELSE IF @vcLookBackTableName = 'Item'
	--BEGIN
	--	IF @vcDbColumnName = 'monListPrice'
	--	BEGIN
	--		IF EXISTS (SELECT numItemCode FROM Item WHERE numDomainID=@numDomainID AND numItemCode=@numRecId AND 1 = (CASE WHEN ISNULL(numItemGroup,0) > 0 THEN (CASE WHEN ISNULL(bitMatrix,0)=1 THEN 1 ELSE 0 END) ELSE 1 END))
	--		BEGIN
	--			UPDATE WareHouseItems SET monWListPrice=@InlineEditValue WHERE numDomainID=@numDomainID AND numItemID=@numRecId
	--			UPDATE Item SET monListPrice=@InlineEditValue WHERE numDomainID=@numDomainID AND numItemCode=@numRecId

	--			SELECT @InlineEditValue AS vcData
	--		END
	--		ELSE
	--		BEGIN
	--			RAISERROR('You are not allowed to edit list price for warehouse level matrix item.',16,1)
	--		END

	--		RETURN
	--	END

	--	SET @strSql='UPDATE Item set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numItemCode=@numRecId AND numDomainID=@numDomainID'
	--END

	EXECUTE sp_executeSQL @strSql, N'@numDomainID NUMERIC(9),@numUserCntID NUMERIC(9),@numDivisionID NUMERIC(9),@numContactID NUMERIC(9),@InlineEditValue VARCHAR(8000),@numWOId NUMERIC(9),@numProId NUMERIC(9),@numOppId NUMERIC(9),@numCaseId NUMERIC(9),@numWODetailId NUMERIC(9),@numCommID Numeric(9),@numRecId NUMERIC(18,0)',
		     @numDomainID,@numUserCntID,@numDivisionID,@numContactID,@InlineEditValue,@numWOId,@numProId,@numOppId,@numCaseId,@numWODetailId,@numCommID,@numRecId;
		     
	IF @vcAssociatedControlType='SelectBox'                                                    
       BEGIN       
			IF @vcDbColumnName='tintSource' AND @vcLookBackTableName = 'OpportunityMaster'
			BEGIN
				SELECT dbo.fn_GetOpportunitySourceValue(@Value1,@Value2,@numDomainID) AS vcData                                             
			END
			ELSE IF @vcDbColumnName='numContactID'
			BEGIN
				SELECT dbo.fn_GetContactName(@InlineEditValue)
			END
			ELSE IF @vcDbColumnName='numPartner' OR @vcDbColumnName='numPartenerSource'
			BEGIN
				SET @intExecuteDiv=1
				SELECT D.vcPartnerCode+'-'+C.vcCompanyName  AS vcData FROM DivisionMaster AS D 
				LEFT JOIN CompanyInfo AS C
				ON D.numCompanyID=C.numCompanyID WHERE D.numDivisionID=@InlineEditValue
			END
			
			ELSE IF @vcDbColumnName='tintOppStatus' AND @intExecuteDiv=1
			BEGIN
				SELECT @InlineEditValue AS vcData
			END
			ELSE IF @vcDbColumnName='numReleaseStatus'
			BEGIN
				If @InlineEditValue = '1'
				BEGIN
					SELECT 'Open'
				END
				ELSE IF @InlineEditValue = '2'
				BEGIN
					SELECT 'Purchased'
				END
				ELSE
				BEGIN
					SELECT ''
				END
			END
			ELSE IF @vcDbColumnName='numPartenerContact'
			BEGIN
				SET @InlineEditValue=(SELECT TOP 1 A.vcGivenName FROM AdditionalContactsInformation AS A 
										LEFT JOIN DivisionMaster AS D 
										ON D.numDivisionID=A.numDivisionId
										WHERE D.numDomainID=@numDomainID AND A.numCOntactId=@InlineEditValue )
				SELECT @InlineEditValue AS vcData
			END
			ELSE IF @vcDbColumnName='numPartenerSource'
			BEGIN
				
				SET @InlineEditValue=(SELECT D.vcPartnerCode+'-'+C.vcCompanyName  AS vcData FROM DivisionMaster AS D 
				LEFT JOIN CompanyInfo AS C
				ON D.numCompanyID=C.numCompanyID WHERE D.numDivisionID=@InlineEditValue)
				PRINT 'MU'
				SELECT @InlineEditValue AS vcData
			END
			ELSE IF @vcDbColumnName='numShippingService'
			BEGIN
				SELECT ISNULL((SELECT vcShipmentService FROM ShippingService WHERE (numDomainID=@numDomainID OR ISNULL(numDomainID,0)=0) AND numShippingServiceID = @InlineEditValue),'') AS vcData
			END
			else
			BEGIN
				SELECT dbo.fn_getSelectBoxValue(@vcListItemType,@InlineEditValue,@vcDbColumnName) AS vcData                                             
			END
		end 
		ELSE
		BEGIN
			 IF @vcDbColumnName='tintActive'
		  	 BEGIN
			 	SET @InlineEditValue= CASE @InlineEditValue WHEN 1 THEN 'True' ELSE 'False' END 
			 END

			IF @vcAssociatedControlType = 'Check box' or @vcAssociatedControlType = 'Checkbox'
			BEGIN
	 			SET @InlineEditValue= Case @InlineEditValue WHEN 'True' THEN 'Yes' ELSE 'No' END
			END
			
		    SELECT @InlineEditValue AS vcData
		END  
		
	IF @vcLookBackTableName = 'AdditionalContactsInformation' AND @vcDbColumnName = 'numECampaignID'
	BEGIN
		--Manage ECampiagn When Campaign is changed from inline edit
		DECLARE @Date AS DATETIME 
		SELECT @Date = dbo.[GetUTCDateWithoutTime]()
		DECLARE @numECampaignID AS NUMERIC(18,0)
		SET @numECampaignID = @InlineEditValue

		IF @numECampaignID = -1 
		SET @numECampaignID=0
	 
 		EXEC [USP_ManageConEmailCampaign]
		@numConEmailCampID = 0, --  numeric(9, 0)
		@numContactID = @numContactID, --  numeric(9, 0)
		@numECampaignID = @numECampaignID, --  numeric(9, 0)
		@intStartDate = @Date, --  datetime
		@bitEngaged = 1, --  bit
		@numUserCntID = @numUserCntID --  numeric(9, 0)
	END  
END
ELSE
BEGIN	 

	SET @InlineEditValue=Cast(@InlineEditValue as varchar(1000))
	SELECT @vcAssociatedControlType=Fld_type
	 FROM CFW_Fld_Master WHERE Fld_Id=@numFormFieldId 

	IF @vcAssociatedControlType = 'CheckBox'
	BEGIN
	    SET @InlineEditValue=(case when @InlineEditValue='False' then 0 ELSE 1 END)
	END

	IF @vcPageName='organization'
	BEGIN 
		IF (SELECT COUNT(*) FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)>0
			BEGIN
				UPDATE CFW_FLD_Values SET Fld_Value=@InlineEditValue WHERE FldDTLID=(SELECT CFW.FldDTLID FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)
			END	
		ELSE
			BEGIN
				insert into CFW_FLD_Values (Fld_ID,Fld_Value,RecId) VALUES (@numFormFieldId,@InlineEditValue,@numRecId)         
			END

			--Added By :Sachin Sadhu ||Date:13thOct2014
			--Purpose  :To manage CustomFields in Queue
			EXEC USP_CFW_FLD_Values_CT
					@numDomainID =@numDomainID,
					@numUserCntID =@numUserCntID,
					@numRecordID =@numRecId ,
					@numFormFieldId  =@numFormFieldId
  
	  END
	ELSE IF @vcPageName='contact'
	BEGIN 
		IF (SELECT COUNT(*) FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Cont CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)>0
			BEGIN
				UPDATE CFW_FLD_Values_Cont SET Fld_Value=@InlineEditValue WHERE FldDTLID=(SELECT CFW.FldDTLID FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Cont CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)
			END	
		ELSE
			BEGIN
				insert into CFW_FLD_Values_Cont (Fld_ID,Fld_Value,RecId) VALUES (@numFormFieldId,@InlineEditValue,@numRecId)         
			END
			
			--Added By :Sachin Sadhu ||Date:13thOct2014
			--Purpose  :To manage CustomFields in Queue
			EXEC USP_CFW_FLD_Values_Cont_CT
					@numDomainID =@numDomainID,
					@numUserCntID =@numUserCntID,
					@numRecordID =@numRecId ,
					@numFormFieldId  =@numFormFieldId

	END
	ELSE IF @vcPageName='project'
	BEGIN 
		IF (SELECT COUNT(*) FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Pro CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)>0
			BEGIN
				UPDATE CFW_FLD_Values_Pro SET Fld_Value=@InlineEditValue WHERE FldDTLID=(SELECT CFW.FldDTLID FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Pro CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)
			END	
		ELSE
			BEGIN
				insert into CFW_FLD_Values_Pro (Fld_ID,Fld_Value,RecId) VALUES (@numFormFieldId,@InlineEditValue,@numRecId)         
			END

			
			--Added By :Sachin Sadhu ||Date:14thOct2014
			--Purpose  :To manage CustomFields in Queue
			EXEC USP_CFW_FLD_Values_Pro_CT
					@numDomainID =@numDomainID,
					@numUserCntID =@numUserCntID,
					@numRecordID =@numRecId ,
					@numFormFieldId  =@numFormFieldId
	END
	ELSE IF @vcPageName='opp'
	BEGIN 
		DECLARE @dtDate AS DATETIME = GETUTCDATE()

		IF (SELECT COUNT(*) FROM CFW_Fld_Master CFM JOIN CFW_Fld_Values_Opp CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)>0
		BEGIN
			UPDATE 
				CFW_Fld_Values_Opp 
			SET 
				Fld_Value=@InlineEditValue ,
				numModifiedBy = @numUserCntID,
				bintModifiedDate = @dtDate
			WHERE 
				FldDTLID= (SELECT CFW.FldDTLID FROM CFW_Fld_Master CFM JOIN CFW_Fld_Values_Opp CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)
		END	
		ELSE
		BEGIN
			INSERT INTO CFW_Fld_Values_Opp (Fld_ID,Fld_Value,RecId,numModifiedBy,bintModifiedDate) VALUES (@numFormFieldId,@InlineEditValue,@numRecId,@numUserCntID,@dtDate)         
		END

		UPDATE OpportunityMaster SET numModifiedBy=@numUserCntID,bintModifiedDate=@dtDate WHERE numOppID = @numRecId 

		--Added By :Sachin Sadhu ||Date:14thOct2014
		--Purpose  :To manage CustomFields in Queue
		EXEC USP_CFW_Fld_Values_Opp_CT
				@numDomainID =@numDomainID,
				@numUserCntID =@numUserCntID,
				@numRecordID =@numRecId ,
				@numFormFieldId  =@numFormFieldId

	END
	ELSE IF @vcPageName='oppitems'
	BEGIN 
		IF (SELECT COUNT(*) FROM CFW_Fld_Master CFM JOIN CFW_Fld_Values_OppItems CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)>0
		BEGIN
			UPDATE 
				CFW_Fld_Values_OppItems 
			SET 
				Fld_Value=@InlineEditValue
			WHERE 
				FldDTLID= (SELECT CFW.FldDTLID FROM CFW_Fld_Master CFM JOIN CFW_Fld_Values_OppItems CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)
		END	
		ELSE
		BEGIN
			INSERT INTO CFW_Fld_Values_OppItems(Fld_ID,Fld_Value,RecId) VALUES (@numFormFieldId,@InlineEditValue,@numRecId)         
		END
	END
	ELSE IF @vcPageName='case'
	BEGIN 
		IF (SELECT COUNT(*) FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Case CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)>0
			BEGIN
				UPDATE CFW_FLD_Values_Case SET Fld_Value=@InlineEditValue WHERE FldDTLID=(SELECT CFW.FldDTLID FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Case CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)
			END	
		ELSE
			BEGIN
				insert into CFW_FLD_Values_Case (Fld_ID,Fld_Value,RecId) VALUES (@numFormFieldId,@InlineEditValue,@numRecId)         
			END

			
			--Added By :Sachin Sadhu ||Date:14thOct2014
			--Purpose  :To manage CustomFields in Queue
			EXEC USP_CFW_FLD_Values_Case_CT
					@numDomainID =@numDomainID,
					@numUserCntID =@numUserCntID,
					@numRecordID =@numRecId ,
					@numFormFieldId  =@numFormFieldId
	END
	ELSE IF @vcPageName='item'
	BEGIN
		IF (SELECT COUNT(*) FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Item CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)>0
		BEGIN
			UPDATE CFW_FLD_Values_Item SET Fld_Value=@InlineEditValue WHERE FldDTLID=(SELECT CFW.FldDTLID FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Item CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)
		END	
		ELSE
		BEGIN
			INSERT INTO CFW_FLD_Values_Item (Fld_ID,Fld_Value,RecId) VALUES (@numFormFieldId,@InlineEditValue,@numRecId)         
		END
	END


	IF @vcAssociatedControlType = 'SelectBox'            
	BEGIN            
		    
		 SELECT vcData FROM ListDetails WHERE numListItemID=@InlineEditValue            
	END
	ELSE IF @vcAssociatedControlType = 'CheckBoxList'
	BEGIN
		DECLARE @str AS VARCHAR(MAX)
		SET @str = 'SELECT STUFF((SELECT CONCAT('','', vcData) FROM ListDetails WHERE numlistitemid IN (' + (CASE WHEN LEN(@InlineEditValue) > 0 THEN @InlineEditValue ELSE '0' END) + ') FOR XML PATH('''')), 1, 1, '''')'
		EXEC(@str)
	END
	ELSE IF @vcAssociatedControlType = 'CheckBox'
	BEGIN
	 	select CASE @InlineEditValue WHEN 1 THEN 'Yes' ELSE 'No' END AS vcData
	END
	ELSE
	BEGIN
		SELECT @InlineEditValue AS vcData
	END 
END

GO
/****** Object:  StoredProcedure [dbo].[USP_ManageItemsAndKits]    Script Date: 02/15/2010 21:44:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Anoop Jayaraj                                                          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_manageitemsandkits')
DROP PROCEDURE usp_manageitemsandkits
GO
CREATE PROCEDURE [dbo].[USP_ManageItemsAndKits]                                                                
@numItemCode as numeric(9) output,                                                                
@vcItemName as varchar(300),                                                                
@txtItemDesc as varchar(2000),                                                                
@charItemType as char(1),                                                                
@monListPrice as DECIMAL(20,5),                                                                
@numItemClassification as numeric(9),                                                                
@bitTaxable as bit,                                                                
@vcSKU as varchar(50),                                                                                                                                      
@bitKitParent as bit,                                                                                   
--@dtDateEntered as datetime,                                                                
@numDomainID as numeric(9),                                                                
@numUserCntID as numeric(9),                                                                                                  
@numVendorID as numeric(9),                                                            
@bitSerialized as bit,                                               
@strFieldList as text,                                            
@vcModelID as varchar(200)='' ,                                            
@numItemGroup as numeric(9)=0,                                      
@numCOGSChartAcntId as numeric(9)=0,                                      
@numAssetChartAcntId as numeric(9)=0,                                      
@numIncomeChartAcntId as numeric(9)=0,                            
@monAverageCost as DECIMAL(20,5),                          
@monLabourCost as DECIMAL(20,5),                  
@fltWeight as float,                  
@fltHeight as float,                  
@fltLength as float,                  
@fltWidth as float,                  
@bitFreeshipping as bit,                
@bitAllowBackOrder as bit,              
@UnitofMeasure as varchar(30)='',              
@strChildItems as text,            
@bitShowDeptItem as bit,              
@bitShowDeptItemDesc as bit,        
@bitCalAmtBasedonDepItems as bit,    
@bitAssembly as BIT,
@intWebApiId INT=null,
@vcApiItemId AS VARCHAR(50)=null,
@numBarCodeId as VARCHAR(25)=null,
@vcManufacturer as varchar(250)=NULL,
@numBaseUnit AS NUMERIC(18)=0,
@numPurchaseUnit AS NUMERIC(18)=0,
@numSaleUnit AS NUMERIC(18)=0,
@bitLotNo as BIT,
@IsArchieve AS BIT,
@numItemClass as numeric(9)=0,
@tintStandardProductIDType AS TINYINT=0,
@vcExportToAPI VARCHAR(50),
@numShipClass NUMERIC,
@ProcedureCallFlag SMALLINT =0,
@vcCategories AS VARCHAR(2000) = '',
@bitAllowDropShip AS BIT=0,
@bitArchiveItem AS BIT = 0,
@bitAsset AS BIT = 0,
@bitRental AS BIT = 0,
@numCategoryProfileID AS NUMERIC(18,0) = 0,
@bitVirtualInventory BIT,
@bitContainer BIT,
@numContainer NUMERIC(18,0)=0,
@numNoItemIntoContainer NUMERIC(18,0),
@bitMatrix BIT = 0 ,
@vcItemAttributes VARCHAR(2000) = '',
@numManufacturer NUMERIC(18,0),
@vcASIN as varchar(50),
@tintKitAssemblyPriceBasedOn TINYINT = 1,
@fltReorderQty FLOAT = 0,
@bitSOWorkOrder BIT = 0,
@bitKitSingleSelect BIT = 0,
@bitExpenseItem BIT = 0,
@numExpenseChartAcntId NUMERIC(18,0) = 0
AS
BEGIN     
	SET @vcManufacturer = CASE WHEN ISNULL(@numManufacturer,0) > 0 THEN ISNULL((SELECT vcCompanyName FROM DivisionMaster INNER JOIN CompanyInfo ON DivisionMaster.numCompanyID=CompanyInfo.numCompanyId WHERE numDivisionID=@numManufacturer),'')  ELSE ISNULL(@vcManufacturer,'') END

	DECLARE @bitAttributesChanged AS BIT  = 1
	DECLARE @bitOppOrderExists AS BIT = 0

	IF ISNULL(@numItemCode,0) > 0
	BEGIN
		IF (SELECT COUNT(OI.numOppId) FROM dbo.OpportunityItems OI WHERE OI.numItemCode =@numItemCode) > 0
		BEGIN
			SET @bitOppOrderExists = 1
		END
		ELSE IF (SELECT COUNT(OI.numOppId) FROM dbo.OpportunityKitItems OI WHERE OI.numChildItemID =@numItemCode) > 0
		BEGIN
			SET @bitOppOrderExists = 1
		END
		ELSE IF (SELECT COUNT(OI.numOppId) FROM dbo.OpportunityKitChildItems OI WHERE OI.numItemID =@numItemCode) > 0
		BEGIN
			SET @bitOppOrderExists = 1
		END
	END
	ELSE 
	BEGIN
		IF (ISNULL(@vcItemName,'') = '')
		BEGIN
			RAISERROR('ITEM_NAME_NOT_SELECTED',16,1)
			RETURN
		END
        ELSE IF (ISNULL(@charItemType,'') = '0')
		BEGIN
			RAISERROR('ITEM_TYPE_NOT_SELECTED',16,1)
			RETURN
		END
        ELSE
		BEGIN 
		    If (@charItemType = 'P' OR (@charItemType = 'S' AND ISNULL(@bitExpenseItem,0) = 1))
			BEGIN
				IF ((SELECT COUNT(*) FROM Chart_Of_Accounts WHERE numDomainId=@numDomainID AND numAccountId=@numAssetChartAcntId) = 0)
				BEGIN
					RAISERROR('INVALID_ASSET_ACCOUNT',16,1)
					RETURN
				END
			END

			-- This is common check for CharItemType P and Other
			IF ((SELECT COUNT(*) FROM Chart_Of_Accounts WHERE numDomainId=@numDomainID AND numAccountId=@numCOGSChartAcntId) = 0)
			BEGIN
				RAISERROR('INVALID_COGS_ACCOUNT',16,1)
				RETURN
			END

			IF ((SELECT COUNT(*) FROM Chart_Of_Accounts WHERE numDomainId=@numDomainID AND numAccountId=@numIncomeChartAcntId) = 0)
			BEGIN
				RAISERROR('INVALID_INCOME_ACCOUNT',16,1)
				RETURN
			END

			IF ISNULL(@bitExpenseItem,0) = 1
			BEGIN
				IF ((SELECT COUNT(*) FROM Chart_Of_Accounts WHERE numDomainId=@numDomainID AND numAccountId=@numExpenseChartAcntId) = 0)
				BEGIN
					RAISERROR('INVALID_EXPENSE_ACCOUNT',16,1)
					RETURN
				END
			END
		END
	END

	--INSERT CUSTOM FIELDS BASE ON WAREHOUSEITEMS
	DECLARE @hDoc AS INT     
	                                                                        	
	DECLARE @TempTable TABLE 
	(
		ID INT IDENTITY(1,1),
		Fld_ID NUMERIC(18,0),
		Fld_Value NUMERIC(18,0)
	)   

	IF ISNULL(@bitMatrix,0) = 1 AND ISNULL(@numItemGroup,0) = 0
	BEGIN
		RAISERROR('ITEM_GROUP_NOT_SELECTED',16,1)
		RETURN
	END
	ELSE IF ISNULL(@bitMatrix,0) = 1 AND LEN(ISNULL(@vcItemAttributes,'')) = 0 
	BEGIN
		RAISERROR('ITEM_ATTRIBUTES_NOT_SELECTED',16,1)
		RETURN
	END
	ELSE IF ISNULL(@bitMatrix,0) = 1 
	BEGIN
		DECLARE @bitDuplicate AS BIT = 0
	
		IF DATALENGTH(ISNULL(@vcItemAttributes,''))>2
		BEGIN
			SET @vcItemAttributes = CONCAT('<?xml version="1.0" encoding="iso-8859-1"?>',@vcItemAttributes)

			EXEC sp_xml_preparedocument @hDoc OUTPUT, @vcItemAttributes
	                                      
			INSERT INTO @TempTable 
			(
				Fld_ID,
				Fld_Value
			)    
			SELECT
				*          
			FROM 
				OPENXML (@hDoc,'/NewDataSet/CusFlds',2)                                                  
			WITH 
				(Fld_ID NUMERIC(18,0),Fld_Value NUMERIC(18,0)) 
			ORDER BY 
				Fld_ID       

			DECLARE @strAttribute VARCHAR(500) = ''
			DECLARE @i AS INT = 1
			DECLARE @COUNT AS INT 
			DECLARE @numFldID AS NUMERIC(18,0)
			DECLARE @numFldValue AS NUMERIC(18,0)
			SELECT @COUNT = COUNT(*) FROM @TempTable

			WHILE @i <= @COUNT
			BEGIN
				SELECT @numFldID=Fld_ID,@numFldValue=Fld_Value FROM @TempTable WHERE ID=@i

				IF ISNUMERIC(@numFldValue)=1
				BEGIN
					SELECT @strAttribute=@strAttribute+Fld_label FROM CFW_Fld_Master WHERE Fld_id=@numFldID AND Grp_id = 9
                
					IF LEN(@strAttribute) > 0
					BEGIN
						SELECT @strAttribute=@strAttribute+':'+ vcData +',' FROM ListDetails WHERE numListItemID=@numFldValue
					END
				END

				SET @i = @i + 1
			END

			SET @strAttribute = SUBSTRING(@strAttribute,0,LEN(@strAttribute))

			IF
				(SELECT
					COUNT(*)
				FROM
					Item
				INNER JOIN
					ItemAttributes
				ON
					Item.numItemCode = ItemAttributes.numItemCode
					AND ItemAttributes.numDomainID = @numDomainID                     
				WHERE
					Item.numItemCode <> @numItemCode
					AND Item.vcItemName = @vcItemName
					AND Item.numItemGroup = @numItemGroup
					AND Item.numDomainID = @numDomainID
					AND dbo.fn_GetItemAttributes(@numDomainID,Item.numItemCode) = @strAttribute
				) > 0
			BEGIN
				RAISERROR('ITEM_WITH_ATTRIBUTES_ALREADY_EXISTS',16,1)
				RETURN
			END

			If ISNULL(@numItemCode,0) > 0 AND dbo.fn_GetItemAttributes(@numDomainID,@numItemCode) = @strAttribute
			BEGIN
				SET @bitAttributesChanged = 0
			END

			EXEC sp_xml_removedocument @hDoc 
		END	
	END
	ELSE IF ISNULL(@bitMatrix,0) = 0
	BEGIN
		-- REMOVE ATTRIBUTES CONFIGURATION OF ITEM AND WAREHOUSE
		DELETE FROM ItemAttributes WHERE numDomainID=@numDomainID AND numItemCode=@numItemCode
	END

	IF ISNULL(@numItemGroup,0) > 0 AND ISNULL(@bitMatrix,0) = 1
	BEGIN
		IF (SELECT COUNT(*) FROM ItemGroupsDTL WHERE numItemGroupID = @numItemGroup AND tintType=2) = 0
		BEGIN
			RAISERROR('ITEM_GROUP_NOT_CONFIGURED',16,1)
			RETURN
		END
		ELSE IF EXISTS (SELECT 
							CFM.Fld_id
						FROM 
							CFW_Fld_Master CFM
						LEFT JOIN
							ListDetails LD
						ON
							CFM.numlistid = LD.numListID
						WHERE
							CFM.numDomainID = @numDomainID
							AND CFM.Fld_id IN (SELECT numOppAccAttrID FROM ItemGroupsDTL WHERE numItemGroupID=@numItemGroup AND tintType = 2)
						GROUP BY
							CFM.Fld_id HAVING COUNT(LD.numListItemID) = 0)
		BEGIN
			RAISERROR('ITEM_GROUP_NOT_CONFIGURED',16,1)
			RETURN
		END
	END

BEGIN TRY
BEGIN TRANSACTION

	

	IF @numCOGSChartAcntId=0 SET @numCOGSChartAcntId=NULL
	IF @numAssetChartAcntId=0 SET @numAssetChartAcntId=NULL  
	IF @numIncomeChartAcntId=0 SET @numIncomeChartAcntId=NULL     
	IF @numExpenseChartAcntId=0 SET @numExpenseChartAcntId=NULL

	DECLARE @ParentSKU VARCHAR(50) = @vcSKU                        
	DECLARE @ItemID AS NUMERIC(9)
	DECLARE @cnt AS INT

	IF ISNULL(@charItemType,'') = '' OR ISNULL(@charItemType,'') = '0'  
		SET @charItemType = 'N'

	IF @intWebApiId = 2 AND LEN(ISNULL(@vcApiItemId, '')) > 0 AND ISNULL(@numItemGroup, 0) > 0 
    BEGIN 
        -- check wether this id already Mapped in ITemAPI 
        SELECT 
			@cnt = COUNT([numItemID])
        FROM 
			[ItemAPI]
        WHERE 
			[WebApiId] = @intWebApiId
            AND [numDomainId] = @numDomainID
            AND [vcAPIItemID] = @vcApiItemId

        IF @cnt > 0 
        BEGIN
            SELECT 
				@ItemID = [numItemID]
            FROM 
				[ItemAPI]
            WHERE 
				[WebApiId] = @intWebApiId
                AND [numDomainId] = @numDomainID
                AND [vcAPIItemID] = @vcApiItemId
        
			--Update Existing Product coming from API
            SET @numItemCode = @ItemID
        END
    END
	ELSE IF @intWebApiId > 1 AND LEN(ISNULL(@vcSKU, '')) > 0 
    BEGIN
        SET @ParentSKU = @vcSKU
		 
        SELECT 
			@cnt = COUNT([numItemCode])
        FROM 
			[Item]
        WHERE 
			[numDomainId] = @numDomainID
            AND (vcSKU = @vcSKU OR @vcSKU IN (SELECT vcWHSKU FROM dbo.WareHouseItems WHERE numItemID = Item.[numItemCode] AND vcWHSKU = @vcSKU))
            
		IF @cnt > 0 
        BEGIN
            SELECT 
				@ItemID = [numItemCode],
                @ParentSKU = vcSKU
            FROM 
				[Item]
            WHERE 
				[numDomainId] = @numDomainID
                AND (vcSKU = @vcSKU OR @vcSKU IN (SELECT vcWHSKU FROM dbo.WareHouseItems WHERE numItemID = Item.[numItemCode] AND vcWHSKU = @vcSKU))

            SET @numItemCode = @ItemID
        END
		ELSE 
		BEGIN
			-- check wether this id already Mapped in ITemAPI 
			SELECT 
				@cnt = COUNT([numItemID])
			FROM 
				[ItemAPI]
			WHERE 
				[WebApiId] = @intWebApiId
				AND [numDomainId] = @numDomainID
				AND [vcAPIItemID] = @vcApiItemId
                    
			IF @cnt > 0 
			BEGIN
				SELECT 
					@ItemID = [numItemID]
				FROM 
					[ItemAPI]
				WHERE 
					[WebApiId] = @intWebApiId
					AND [numDomainId] = @numDomainID
					AND [vcAPIItemID] = @vcApiItemId
        
				--Update Existing Product coming from API
				SET @numItemCode = @ItemID
			END
		END
    END
                                                         
	IF @numItemCode=0 OR @numItemCode IS NULL
	BEGIN
		INSERT INTO Item 
		(                                             
			vcItemName,txtItemDesc,charItemType,monListPrice,numItemClassification,bitTaxable,vcSKU,bitKitParent,
			numVendorID,numDomainID,numCreatedBy,bintCreatedDate,bintModifiedDate,numModifiedBy,bitSerialized,
			vcModelID,numItemGroup,numCOGsChartAcntId,numAssetChartAcntId,numIncomeChartAcntId,monAverageCost,                          
			monCampaignLabourCost,fltWeight,fltHeight,fltWidth,fltLength,bitFreeShipping,bitAllowBackOrder,vcUnitofMeasure,
			bitShowDeptItem,bitShowDeptItemDesc,bitCalAmtBasedonDepItems,bitAssembly,numBarCodeId,vcManufacturer,numBaseUnit,
			numPurchaseUnit,numSaleUnit,bitLotNo,IsArchieve,numItemClass,tintStandardProductIDType,vcExportToAPI,numShipClass,
			bitAllowDropShip,bitArchiveItem,bitAsset,bitRental,bitVirtualInventory,bitContainer,numContainer,numNoItemIntoContainer
			,bitMatrix,numManufacturer,vcASIN,tintKitAssemblyPriceBasedOn,fltReorderQty,bitSOWorkOrder,bitKitSingleSelect,bitExpenseItem,numExpenseChartAcntId
		)                                                              
		VALUES                                                                
		(                                                                
			@vcItemName,@txtItemDesc,@charItemType,@monListPrice,@numItemClassification,@bitTaxable,@ParentSKU,@bitKitParent,
			@numVendorID,@numDomainID,@numUserCntID,getutcdate(),getutcdate(),@numUserCntID,@bitSerialized,@vcModelID,@numItemGroup,                                      
			@numCOGsChartAcntId,@numAssetChartAcntId,@numIncomeChartAcntId,@monAverageCost,@monLabourCost,@fltWeight,@fltHeight,@fltWidth,                  
			@fltLength,@bitFreeshipping,@bitAllowBackOrder,@UnitofMeasure,@bitShowDeptItem,@bitShowDeptItemDesc,@bitCalAmtBasedonDepItems,    
			@bitAssembly,@numBarCodeId,@vcManufacturer,@numBaseUnit,@numPurchaseUnit,@numSaleUnit,@bitLotNo,@IsArchieve,@numItemClass,
			@tintStandardProductIDType,@vcExportToAPI,@numShipClass,@bitAllowDropShip,@bitArchiveItem ,@bitAsset,@bitRental,
			@bitVirtualInventory,@bitContainer,@numContainer,@numNoItemIntoContainer
			,@bitMatrix,@numManufacturer,@vcASIN,@tintKitAssemblyPriceBasedOn,@fltReorderQty,@bitSOWorkOrder,@bitKitSingleSelect,@bitExpenseItem,@numExpenseChartAcntId
		)                                             
 
		SET @numItemCode = SCOPE_IDENTITY()
  
		IF  @intWebApiId > 1 
		BEGIN
			 -- insert new product
			 --insert this id into linking table
			 INSERT INTO [ItemAPI] (
	 			[WebApiId],
	 			[numDomainId],
	 			[numItemID],
	 			[vcAPIItemID],
	 			[numCreatedby],
	 			[dtCreated],
	 			[numModifiedby],
	 			[dtModified],vcSKU
			 ) VALUES     
			 (
				@intWebApiId,
				@numDomainID,
				@numItemCode,
				@vcApiItemId,
	 			@numUserCntID,
	 			GETUTCDATE(),
	 			@numUserCntID,
	 			GETUTCDATE(),@vcSKU
	 		) 
		END
 
		IF @charItemType = 'P'
		BEGIN
			
			DECLARE @TEMPWarehouse TABLE
			(
				ID INT IDENTITY(1,1)
				,numWarehouseID NUMERIC(18,0)
			)
		
			INSERT INTO @TEMPWarehouse (numWarehouseID) SELECT numWareHouseID FROM Warehouses WHERE numDomainID=@numDomainID

			DECLARE @j AS INT = 1
			DECLARE @jCount AS INT
			DECLARE @numTempWarehouseID NUMERIC(18,0)
			SELECT @jCount = COUNT(*) FROM @TEMPWarehouse

			WHILE @j <= @jCount
			BEGIN
				SELECT @numTempWarehouseID=numWarehouseID FROM @TEMPWarehouse WHERE ID=@j

				EXEC USP_WarehouseItems_Save @numDomainID,
											@numUserCntID,
											0,
											@numTempWarehouseID,
											0,
											@numItemCode,
											'',
											@monListPrice,
											0,
											0,
											'',
											'',
											'',
											'',
											0
 
				SET @j = @j + 1
			END
		END 
	END         
	ELSE IF @numItemCode>0 AND @intWebApiId > 0 
	BEGIN 
		IF @ProcedureCallFlag = 1 
		BEGIN
			DECLARE @ExportToAPIList AS VARCHAR(30)
			SELECT @ExportToAPIList = vcExportToAPI FROM dbo.Item WHERE numItemCode = @numItemCode

			IF @ExportToAPIList != ''
			BEGIN
				IF NOT EXISTS(select * from (SELECT * FROM dbo.Split(@ExportToAPIList ,','))as A where items= @vcExportToAPI ) 
				BEGIN
					SET @vcExportToAPI = @ExportToAPIList + ',' + @vcExportToAPI
				End
				ELSE
				BEGIN
					SET @vcExportToAPI = @ExportToAPIList 
				End
			END

			--update ExportToAPI String value
			UPDATE dbo.Item SET vcExportToAPI=@vcExportToAPI where numItemCode=@numItemCode  AND numDomainID=@numDomainID
					
			SELECT 
				@cnt = COUNT([numItemID]) 
			FROM 
				[ItemAPI] 
			WHERE  
				[WebApiId] = @intWebApiId 
				AND [numDomainId] = @numDomainID
				AND [vcAPIItemID] = @vcApiItemId
				AND numItemID = @numItemCode

			IF @cnt > 0
			BEGIN
				--update lastUpdated Date 
				UPDATE [ItemAPI] SET [dtModified] =GETUTCDATE() WHERE [WebApiId] =@intWebApiId AND [numDomainId] = @numDomainID
			END
			ELSE
			--Insert ItemAPI mapping
			BEGIN
				INSERT INTO [ItemAPI] 
				(
					[WebApiId],
					[numDomainId],
					[numItemID],
					[vcAPIItemID],
					[numCreatedby],
					[dtCreated],
					[numModifiedby],
					[dtModified],vcSKU
				) VALUES 
				(
					@intWebApiId,
					@numDomainID,
					@numItemCode,
					@vcApiItemId,
					@numUserCntID,
					GETUTCDATE(),
					@numUserCntID,
					GETUTCDATE(),@vcSKU
				)	
			END
		END   
	END                                            
	ELSE IF @numItemCode > 0 AND @intWebApiId <= 0                                                           
	BEGIN
		DECLARE @OldGroupID NUMERIC 
		SELECT @OldGroupID = ISNULL(numItemGroup,0) FROM [Item] WHERE numitemcode = @numItemCode AND [numDomainId] = @numDomainID
	
		DECLARE @monOldAverageCost AS DECIMAL(18,4);SET @monOldAverageCost=0
		SELECT @monOldAverageCost=(CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(monAverageCost,0) END) FROM Item WHERE numItemCode =@numItemCode AND [numDomainId] = @numDomainID
	   
		UPDATE 
			Item 
		SET 
			vcItemName=@vcItemName,txtItemDesc=@txtItemDesc,charItemType=@charItemType,monListPrice= @monListPrice,numItemClassification=@numItemClassification,                                             
			bitTaxable=@bitTaxable,vcSKU = (CASE WHEN ISNULL(@ParentSKU,'') = '' THEN @vcSKU ELSE @ParentSKU END),bitKitParent=@bitKitParent,                                             
			numVendorID=@numVendorID,numDomainID=@numDomainID,bintModifiedDate=getutcdate(),numModifiedBy=@numUserCntID,bitSerialized=@bitSerialized,                                                         
			vcModelID=@vcModelID,numItemGroup=@numItemGroup,numCOGsChartAcntId=@numCOGsChartAcntId,numAssetChartAcntId=@numAssetChartAcntId,                                      
			numIncomeChartAcntId=@numIncomeChartAcntId,monCampaignLabourCost=@monLabourCost,fltWeight=@fltWeight,fltHeight=@fltHeight,fltWidth=@fltWidth,                  
			fltLength=@fltLength,bitFreeShipping=@bitFreeshipping,bitAllowBackOrder=@bitAllowBackOrder,vcUnitofMeasure=@UnitofMeasure,bitShowDeptItem=@bitShowDeptItem,            
			bitShowDeptItemDesc=@bitShowDeptItemDesc,bitCalAmtBasedonDepItems=@bitCalAmtBasedonDepItems,bitAssembly=@bitAssembly,numBarCodeId=@numBarCodeId,
			vcManufacturer=@vcManufacturer,numBaseUnit=@numBaseUnit,numPurchaseUnit=@numPurchaseUnit,numSaleUnit=@numSaleUnit,bitLotNo=@bitLotNo,
			IsArchieve = @IsArchieve,numItemClass=@numItemClass,tintStandardProductIDType=@tintStandardProductIDType,vcExportToAPI=@vcExportToAPI,
			numShipClass=@numShipClass,bitAllowDropShip=@bitAllowDropShip, bitArchiveItem = @bitArchiveItem,bitAsset=@bitAsset,bitRental=@bitRental,
			bitVirtualInventory = @bitVirtualInventory,numContainer=@numContainer,numNoItemIntoContainer=@numNoItemIntoContainer,bitMatrix=@bitMatrix,numManufacturer=@numManufacturer,
			vcASIN = @vcASIN,tintKitAssemblyPriceBasedOn=@tintKitAssemblyPriceBasedOn,fltReorderQty=@fltReorderQty,bitSOWorkOrder=@bitSOWorkOrder,bitKitSingleSelect=@bitKitSingleSelect
			,bitExpenseItem=@bitExpenseItem,numExpenseChartAcntId=@numExpenseChartAcntId
		WHERE 
			numItemCode=@numItemCode 
			AND numDomainID=@numDomainID

		IF NOT EXISTS(SELECT numItemCode FROM dbo.OpportunityItems WHERE numItemCode = @numItemCode)
		BEGIN
			IF (((SELECT ISNULL(SUM(numOnHand),0) FROM WareHouseItems WHERE numItemID = @numItemCode AND numDomainId=@numDomainID) = 0) AND ((SELECT ISNULL(bitVirtualInventory,0) FROM Item  WHERE numItemCode = @numItemCode AND numDomainId=@numDomainID) = 0))
			BEGIN
				UPDATE Item SET monAverageCost=@monAverageCost  where numItemCode=@numItemCode  AND numDomainID=@numDomainID	
			END
		END
	
		--If Average Cost changed then add in Tracking
		IF ISNULL(@monAverageCost,0) != ISNULL(@monOldAverageCost,0)
		BEGIN
			DECLARE @numWareHouseItemID NUMERIC(9);SET @numWareHouseItemID=0
			DECLARE @numTotal int;SET @numTotal=0
			SET @i=0
			DECLARE @vcDescription AS VARCHAR(100);SET @vcDescription=''
		
			SELECT @numTotal=COUNT(*) FROM WareHouseItems where numItemID=@numItemCode AND numDomainId=@numDomainId
	     
			WHILE @i < @numTotal
			BEGIN
				SELECT @numWareHouseItemID = min(numWareHouseItemID) FROM WareHouseItems 
					WHERE numItemID=@numItemCode AND numDomainId=@numDomainId and numWareHouseItemID > @numWareHouseItemID 
				
				IF @numWareHouseItemID>0
				BEGIN
					SET @vcDescription='Average Cost Changed Manually OLD : ' + CAST(ISNULL(@monOldAverageCost,0) AS VARCHAR(20))
				
					DECLARE @numDomain AS NUMERIC(18,0)
					SET @numDomain = @numDomainID
					EXEC dbo.USP_ManageWareHouseItems_Tracking
						@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)		
						@numReferenceID = @numItemCode, --  numeric(9, 0)
						@tintRefType = 1, --  tinyint
						@vcDescription = @vcDescription, --  varchar(100)
						@tintMode = 0, --  tinyint
						@numModifiedBy = @numUserCntID, --  numeric(9, 0)
						@numDomainID = @numDomain
				END
		    
				SET @i = @i + 1
			END
		END
 
		DECLARE @bitCartFreeShipping as  bit 
		SELECT @bitCartFreeShipping = bitFreeShipping FROM dbo.CartItems where numItemCode = @numItemCode  AND numDomainID = @numDomainID 
 
		IF @bitCartFreeShipping <> @bitFreeshipping
		BEGIN
			UPDATE dbo.CartItems SET bitFreeShipping = @bitFreeshipping WHERE numItemCode = @numItemCode  AND numDomainID=@numDomainID
		END 
 
		IF @intWebApiId > 1 
		BEGIN
			SELECT 
				@cnt = COUNT([numItemID])
			FROM 
				[ItemAPI]
			WHERE 
				[WebApiId] = @intWebApiId
				AND [numDomainId] = @numDomainID
				AND [vcAPIItemID] = @vcApiItemId

			IF @cnt > 0
			BEGIN
				--update lastUpdated Date 
				UPDATE [ItemAPI] SET [dtModified] =GETUTCDATE() WHERE [WebApiId] =@intWebApiId AND [numDomainId] = @numDomainID
			END
			ELSE --Insert ItemAPI mapping
			BEGIN
				INSERT INTO [ItemAPI] 
				(
	 				[WebApiId],
	 				[numDomainId],
	 				[numItemID],
	 				[vcAPIItemID],
	 				[numCreatedby],
	 				[dtCreated],
	 				[numModifiedby],
	 				[dtModified],vcSKU
				 )
				 VALUES 
				 (
					@intWebApiId,
					@numDomainID,
					@numItemCode,
					@vcApiItemId,
	 				@numUserCntID,
	 				GETUTCDATE(),
	 				@numUserCntID,
	 				GETUTCDATE(),@vcSKU
	 			) 
			END
		END   
		                                                                            
		-- kishan It is necessary to add item into itemTax table . 
		IF	@bitTaxable = 1
		BEGIN
			IF NOT EXISTS(SELECT * FROM dbo.ItemTax WHERE numItemCode = @numItemCode AND numTaxItemID = 0)
			BEGIN
			    INSERT INTO dbo.ItemTax 
				(
					numItemCode,
					numTaxItemID,
					bitApplicable
				)
				VALUES 
				( 
					@numItemCode,
					0,
					1 
				) 						
			END
		END	 
      
		IF @charItemType='S' or @charItemType='N'                                                                       
		BEGIN
			DELETE FROM WareHouseItmsDTL where numWareHouseItemID in (select numWareHouseItemID from WareHouseItems where numItemID=@numItemCode AND [WareHouseItems].[numDomainID] = @numDomainID)
			DELETE FROM WareHouseItems where numItemID=@numItemCode AND [WareHouseItems].[numDomainID] = @numDomainID
		END                                      
   
		IF @bitKitParent=1 OR @bitAssembly = 1              
		BEGIN              
			IF DATALENGTH(ISNULL(@strChildItems,'')) > 0
			BEGIN
				SET @strChildItems = CONCAT('<?xml version="1.0" encoding="iso-8859-1"?>',@strChildItems)
			END

			DECLARE @hDoc1 AS INT                                            
			EXEC sp_xml_preparedocument @hDoc1 OUTPUT, @strChildItems                                                                        
                                             
			UPDATE 
				ItemDetails 
			SET
				numQtyItemsReq=X.QtyItemsReq
				,numUOMId=X.numUOMId
				,vcItemDesc=X.vcItemDesc
				,sintOrder=X.sintOrder                                                                                          
			From 
			(
				SELECT 
					QtyItemsReq,ItemKitID,ChildItemID,numUOMId,vcItemDesc,sintOrder, numItemDetailID As ItemDetailID                             
				FROM 
					OPENXML(@hDoc1,'/NewDataSet/Table1',2)                                                                         
				WITH
				(
					ItemKitID NUMERIC(9),                                                          
					ChildItemID NUMERIC(9),                                                                        
					QtyItemsReq DECIMAL(30, 16),
					numUOMId NUMERIC(9),
					vcItemDesc VARCHAR(1000),
					sintOrder int,
					numItemDetailID NUMERIC(18,0)
				)
			)X                                                                         
			WHERE 
				numItemKitID=X.ItemKitID 
				AND numChildItemID=ChildItemID 
				AND numItemDetailID = X.ItemDetailID

			EXEC sp_xml_removedocument @hDoc1
		 END   
		 ELSE
		 BEGIN
 			DELETE FROM ItemDetails WHERE numItemKitID=@numItemCode
		 END                                                
	END

	IF ISNULL(@numItemGroup,0) = 0 OR ISNULL(@bitMatrix,0) = 1
	BEGIN
		UPDATE WareHouseItems SET monWListPrice = @monListPrice, vcWHSKU=@vcSKU, vcBarCode=@numBarCodeId WHERE numDomainID = @numDomainID AND numItemID = @numItemCode
	END

	-- SAVE MATRIX ITEM ATTRIBUTES
	IF @numItemCode > 0 AND ISNULL(@bitAttributesChanged,0)=1 AND ISNULL(@bitMatrix,0) = 1 AND LEN(ISNULL(@vcItemAttributes,'')) > 0 AND ISNULL(@bitOppOrderExists,0) <> 1 AND (SELECT COUNT(*) FROM @TempTable) > 0
	BEGIN
		-- DELETE PREVIOUS ITEM ATTRIBUTES
		DELETE FROM ItemAttributes WHERE numDomainID=@numDomainID AND numItemCode=@numItemCode

		-- INSERT NEW ITEM ATTRIBUTES
		INSERT INTO ItemAttributes
		(
			numDomainID
			,numItemCode
			,FLD_ID
			,FLD_Value
		)
		SELECT
			@numDomainID
			,@numItemCode
			,Fld_ID
			,Fld_Value
		FROM
			@TempTable

		IF (SELECT COUNT(*) FROM WarehouseItems WHERE numDomainID=@numDomainID AND numItemID=@numItemCode) > 0
		BEGIN

			-- DELETE PREVIOUS WAREHOUSE ATTRIBUTES
			DELETE FROM CFW_Fld_Values_Serialized_Items WHERE RecId IN (SELECT ISNULL(numWarehouseItemID,0) FROM WarehouseItems WHERE numDomainID=@numDomainID AND numItemID=@numItemCode)

			-- INSERT NEW WAREHOUSE ATTRIBUTES
			INSERT INTO CFW_Fld_Values_Serialized_Items
			(
				Fld_ID,
				Fld_Value,
				RecId,
				bitSerialized
			)                                                  
			SELECT 
				Fld_ID,
				ISNULL(CAST(Fld_Value AS VARCHAR),'') AS Fld_Value,
				TEMPWarehouse.numWarehouseItemID,
				0 
			FROM 
				@TempTable  
			OUTER APPLY
			(
				SELECT
					numWarehouseItemID
				FROM 
					WarehouseItems 
				WHERE 
					numDomainID=@numDomainID 
					AND numItemID=@numItemCode
			) AS TEMPWarehouse
		END
	END	  
COMMIT

	IF @numItemCode > 0
	BEGIN
		DELETE 
			IC
		FROM 
			dbo.ItemCategory IC
		INNER JOIN 
			Category 
		ON 
			IC.numCategoryID = Category.numCategoryID
		WHERE 
			numItemID = @numItemCode		
			AND numCategoryProfileID = @numCategoryProfileID

		IF @vcCategories <> ''	
		BEGIN
			INSERT INTO ItemCategory( numItemID , numCategoryID )  SELECT  @numItemCode AS numItemID,ID from dbo.SplitIDs(@vcCategories,',')	
		END
	END
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH        
END
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_MassPurchaseFulfillment_GetConfiguration')
DROP PROCEDURE dbo.USP_MassPurchaseFulfillment_GetConfiguration
GO

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_MassPurchaseFulfillment_GetConfiguration]
(
	@numDomainID NUMERIC(18,0)
	,@numUserCntID NUMERIC(18,0)
)
AS
BEGIN
	IF EXISTS (SELECT ID FROM MassPurchaseFulfillmentConfiguration WHERE numDomainID=@numDomainID AND numUserCntID=@numUserCntID)
	BEGIN
		SELECT 
			ISNULL(bitGroupByOrderForReceive,0) bitGroupByOrderForReceive
			,ISNULL(bitGroupByOrderForPutAway,0) bitGroupByOrderForPutAway
			,ISNULL(bitGroupByOrderForBill,0) bitGroupByOrderForBill
			,ISNULL(bitGroupByOrderForClose,0) bitGroupByOrderForClose
			,ISNULL(tintScanValue,1) tintScanValue
		FROM	
			MassPurchaseFulfillmentConfiguration
		WHERE
			numDomainID=@numDomainID
			AND numUserCntID=@numUserCntID
	END
	ELSE 
	BEGIN
		SELECT 
			0 bitGroupByOrderForReceive
			,0 bitGroupByOrderForPutAway
			,0 bitGroupByOrderForBill
			,0 bitGroupByOrderForClose
			,1 tintScanValue
	END
END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_MassPurchaseFulfillment_GetOrderItems')
DROP PROCEDURE dbo.USP_MassPurchaseFulfillment_GetOrderItems
GO

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_MassPurchaseFulfillment_GetOrderItems]
(
	@numDomainID NUMERIC(18,0)
	,@numUserCntID NUMERIC(18,0)
	,@numOppID NUMERIC(18,0)
	,@tintMode TINYINT
	,@vcSelectedRecords VARCHAR(MAX)
)
AS 
BEGIN
	IF ISNULL(@vcSelectedRecords,'') <> ''
	BEGIN
		DECLARE @TempItems TABLE
		(	
			numOppItemID NUMERIC(18,0),
			numUnitHour FLOAT
		)

		INSERT INTO @TempItems
		(
			numOppItemID,
			numUnitHour
		)
		SELECT
			CAST(SUBSTRING(OutParam,0,CHARINDEX('-',OutParam)) AS NUMERIC(18,0))
			,CAST(SUBSTRING(OutParam,CHARINDEX('-',OutParam) + 1,LEN(OutParam)) AS FLOAT)
		FROM
			dbo.SplitString(@vcSelectedRecords,',')

		IF @tintMode = 1 -- For bill orders
		BEGIN
			SELECT
				numoppitemtCode
				,ISNULL(OpportunityItems.numUnitHour,0) - ISNULL((SELECT 
																		SUM(OpportunityBizDocItems.numUnitHour)
																	FROM
																		OpportunityBizDocs
																	INNER JOIN
																		OpportunityBizDocItems
																	ON
																		OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																	WHERE
																		OpportunityBizDocs.numOppId = @numOppID
																		AND OpportunityBizDocs.numBizDocId=644
																		AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode),0) numUnitHour
				,monPrice
			FROM
				@TempItems T1
			INNER JOIN
				OpportunityItems
			ON
				T1.numOppItemID=OpportunityItems.numoppitemtCode
			INNER JOIN
				OpportunityMaster
			ON
				OpportunityItems.numOppId = OpportunityMaster.numOppId
			WHERE
				OpportunityMaster.numDomainId=@numDomainID
				AND OpportunityMaster.numOppId=@numOppID
				AND OpportunityMaster.tintOppType = 2
				AND OpportunityMaster.tintOppStatus = 1
				AND ISNULL(OpportunityMaster.tintshipped,0) = 0
				AND ISNULL(OpportunityItems.numUnitHour,0) - ISNULL((SELECT 
																		SUM(OpportunityBizDocItems.numUnitHour)
																	FROM
																		OpportunityBizDocs
																	INNER JOIN
																		OpportunityBizDocItems
																	ON
																		OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																	WHERE
																		OpportunityBizDocs.numOppId = @numOppID
																		AND OpportunityBizDocs.numBizDocId=644
																		AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode),0) > 0
		END
	END
	ELSE
	BEGIN
		IF @tintMode = 1 -- For bill orders
		BEGIN
			SELECT
				numoppitemtCode
				,ISNULL(OpportunityItems.numUnitHour,0) - ISNULL((SELECT 
																		SUM(OpportunityBizDocItems.numUnitHour)
																	FROM
																		OpportunityBizDocs
																	INNER JOIN
																		OpportunityBizDocItems
																	ON
																		OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																	WHERE
																		OpportunityBizDocs.numOppId = @numOppID
																		AND OpportunityBizDocs.numBizDocId=644
																		AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode),0) numUnitHour
				,monPrice
			FROM
				OpportunityMaster
			INNER JOIN
				OpportunityItems
			ON
				OpportunityMaster.numOppId=OpportunityItems.numOppId
			WHERE
				OpportunityMaster.numDomainId=@numDomainID
				AND OpportunityMaster.numOppId=@numOppID
				AND OpportunityMaster.tintOppType = 2
				AND OpportunityMaster.tintOppStatus = 1
				AND ISNULL(OpportunityMaster.tintshipped,0) = 0
				AND ISNULL(OpportunityItems.numUnitHour,0) - ISNULL((SELECT 
																		SUM(OpportunityBizDocItems.numUnitHour)
																	FROM
																		OpportunityBizDocs
																	INNER JOIN
																		OpportunityBizDocItems
																	ON
																		OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																	WHERE
																		OpportunityBizDocs.numOppId = @numOppID
																		AND OpportunityBizDocs.numBizDocId=644
																		AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode),0) > 0
		END
	END

END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_MassPurchaseFulfillment_GetPOItemsPendingToReceive')
DROP PROCEDURE USP_MassPurchaseFulfillment_GetPOItemsPendingToReceive
GO
CREATE PROCEDURE [dbo].[USP_MassPurchaseFulfillment_GetPOItemsPendingToReceive]
	@numDomainID as numeric(18,0),
	@numOppID numeric(18,0)=0
AS
BEGIN
	SELECT
		OpportunityMaster.numOppID AS OppID
		,ISNULL(OpportunityMaster.bitStockTransfer,0) AS IsStockTransfer
		,OpportunityMaster.numDivisionID AS DivisionID
		,ISNULL(OpportunityMaster.vcPoppName,'''') AS OppName
		,ISNULL(OpportunityMaster.bitPPVariance,0) AS IsPPVariance
		,ISNULL(OpportunityMaster.numCurrencyID,0) AS CurrencyID
		,ISNULL(OpportunityMaster.fltExchangeRate,0) AS ExchangeRate
		,ISNULL(OpportunityItems.numoppitemtCode,0) AS OppItemID 
        ,ISNULL(Item.vcItemName,0) AS ItemName
        ,ISNULL(OpportunityItems.numWarehouseItmsID,0) AS WarehouseItemID
        ,ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyReceived,0) AS RemainingQty
        ,ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyReceived,0) AS QtyToReceive
        ,'' AS SerialLot
        ,ISNULL(Item.bitSerialized,0) AS IsSerial
        ,ISNULL(Item.bitLotNo,0) AS IsLot
        ,ISNULL(Item.charItemType,'') AS CharItemType
        ,ISNULL(OpportunityItems.vcType,'') AS ItemType
        ,ISNULL(OpportunityItems.monPrice,0) AS Price
        ,ISNULL(OpportunityItems.bitDropShip,0) AS DropShip
        ,ISNULL(OpportunityItems.numProjectID,0) AS ProjectID
        ,ISNULL(OpportunityItems.numClassID,0) AS ClassID
        ,ISNULL(Item.numAssetChartAcntId,0) AS AssetChartAcntId
        ,ISNULL(Item.numCOGsChartAcntId,0) AS COGsChartAcntId
        ,ISNULL(Item.numIncomeChartAcntId,0) AS IncomeChartAcntId
		,1 AS IsSuccess
		,'' AS ErrorMessage
	FROM
		OpportunityMaster
	INNER JOIN
		OpportunityItems
	ON
		OpportunityMaster.numOppId = OpportunityItems.numoppitemtCode
	INNER JOIN
		Item
	ON
		OpportunityItems.numItemCode = Item.numItemCode
	WHERE
		OpportunityMaster.numDomainID=@numDomainID
		AND OpportunityMaster.numOppId=@numOppID
		AND OpportunityMaster.tintOppType = 2
		AND OpportunityMaster.tintOppStatus = 1
		AND ISNULL(OpportunityMaster.tintshipped,0) = 0
		AND ISNULL(OpportunityItems.bitDropShip,0)=0 
		AND ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numUnitHourReceived,0) - ISNULL(OpportunityItems.numQtyReceived,0) > 0
END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_MassPurchaseFulfillment_GetRecords')
DROP PROCEDURE dbo.USP_MassPurchaseFulfillment_GetRecords
GO

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_MassPurchaseFulfillment_GetRecords]
(
	@numDomainID NUMERIC(18,0)
	,@numUserCntID NUMERIC(18,0)
	,@ClientTimeZoneOffset INT
	,@numViewID NUMERIC(18,0)
	,@numWarehouseID NUMERIC(18,0)
	,@bitGroupByOrder BIT
	,@bitIncludeSearch BIT
	,@vcOrderStauts VARCHAR(MAX)
	,@vcCustomSearchValue VARCHAR(MAX)
	,@numPageIndex INT
	,@vcSortColumn VARCHAR(100) OUTPUT
	,@vcSortOrder VARCHAR(4) OUTPUT
	,@numTotalRecords NUMERIC(18,0) OUTPUT
)
AS 
BEGIN
	IF @numViewID = 4
	BEGIN
		SET @bitGroupByOrder = 1
	END

	DECLARE @TempFieldsLeft TABLE
	(
		numPKID INT IDENTITY(1,1)
		,ID VARCHAR(50)
		,numFieldID NUMERIC(18,0)
		,vcFieldName VARCHAR(100)
		,vcOrigDbColumnName VARCHAR(100)
		,vcListItemType VARCHAR(10)
		,numListID NUMERIC(18,0)
		,bitAllowSorting BIT
		,bitAllowFiltering BIT
		,vcAssociatedControlType VARCHAR(20)
		,vcLookBackTableName VARCHAR(100)
		,bitCustomField BIT
		,intRowNum INT
		,intColumnWidth FLOAT
		,bitIsRequired BIT
		,bitIsEmail BIT
		,bitIsAlphaNumeric BIT
		,bitIsNumeric BIT
		,bitIsLengthValidation BIT
		,intMaxLength INT
		,intMinLength INT
		,bitFieldMessage BIT
		,vcFieldMessage VARCHAR(MAX)
	)

	IF @bitGroupByOrder = 1
	BEGIN
		IF @vcSortColumn NOT IN ('CompanyInfo.vcCompanyName','OpportunityMaster.bintCreatedDate','OpportunityMaster.vcPoppName','OpportunityMaster.numStatus','OpportunityMaster.numAssignedTo','OpportunityMaster.numRecOwner')
		BEGIN
			SET @vcSortColumn = 'OpportunityMaster.bintCreatedDate'
			SET @vcSortOrder = 'DESC'
		END
	END
	ELSE 
	BEGIN
		IF @vcSortColumn NOT IN ('CompanyInfo.vcCompanyName'
								,'OpportunityMaster.bintCreatedDate'
								,'OpportunityMaster.vcPoppName'
								,'OpportunityMaster.numStatus'
								,'OpportunityMaster.numAssignedTo'
								,'OpportunityMaster.numRecOwner'
								,'WarehouseItems.numOnHand'
								,'WarehouseItems.numOnOrder'
								,'WarehouseItems.numAllocation'
								,'WarehouseItems.numBackOrder'
								,'Item.numItemCode'
								,'Item.vcItemName'
								,'OpportunityMaster.numUnitHour'
								,'OpportunityMaster.numUnitHourReceived')
		BEGIN
			SET @vcSortColumn = 'OpportunityMaster.bintCreatedDate'
			SET @vcSortOrder = 'DESC'
		END
	END

	INSERT INTO @TempFieldsLeft
	(
		ID
		,numFieldID
		,vcFieldName
		,vcOrigDbColumnName
		,vcListItemType
		,numListID
		,bitAllowSorting
		,bitAllowFiltering
		,vcAssociatedControlType
		,vcLookBackTableName
		,bitCustomField
		,intRowNum
		,intColumnWidth
		,bitIsRequired
		,bitIsEmail
		,bitIsAlphaNumeric
		,bitIsNumeric
		,bitIsLengthValidation
		,intMaxLength
		,intMinLength
		,bitFieldMessage
		,vcFieldMessage
	)
	SELECT
		CONCAT(135,'~',DFM.numFieldID,'~',0)
		,DFM.numFieldID
		,(CASE WHEN @numViewID=6 AND DFM.numFieldID=248 THEN 'BizDoc' ELSE DFFM.vcFieldName END)
		,DFM.vcOrigDbColumnName
		,ISNULL(DFM.vcListItemType,'')
		,ISNULL(DFM.numListID,0)
		,DFFM.bitAllowSorting
		,DFFM.bitAllowFiltering
		,DFM.vcAssociatedControlType
		,DFM.vcLookBackTableName
		,0
		,DFCD.intRowNum
		,ISNULL(DFCD.intColumnWidth,0)
		,0
		,0
		,0
		,0
		,0
		,0
		,0
		,0
		,''
	FROM
		DycFormConfigurationDetails DFCD
	INNER JOIN
		DycFormField_Mapping DFFM
	ON
		DFCD.numFormId = DFFM.numFormID
		AND DFCD.numFieldId = DFFM.numFieldID
	INNER JOIN
		DycFieldMaster DFM
	ON
		DFFM.numFieldID = DFM.numFieldId
	WHERE 
		DFCD.numDomainId=@numDomainID 
		AND DFCD.numUserCntID=@numUserCntID 
		AND DFCD.numFormId=135 
		AND DFCD.numViewId=@numViewID
		AND ISNULL(DFFM.bitDeleted,0) = 0
		AND ISNULL(DFFM.bitSettingField,0) = 1
		AND ISNULL(DFCD.bitCustom,0) = 0
	UNION
	SELECT
		CONCAT(135,'~',CFM.Fld_id,'~',0)
		,CFM.Fld_id
		,CFM.Fld_label
		,CONCAT('CUST',CFM.Fld_id)
		,''
		,ISNULL(CFM.numListID,0)
		,0
		,0
		,CFM.Fld_type
		,''
		,1
		,DFCD.intRowNum
		,ISNULL(DFCD.intColumnWidth,0)
		,ISNULL(bitIsRequired,0)
		,ISNULL(bitIsEmail,0)
		,ISNULL(bitIsAlphaNumeric,0)
		,ISNULL(bitIsNumeric,0)
		,ISNULL(bitIsLengthValidation,0)
		,ISNULL(intMaxLength,0)
		,ISNULL(intMinLength,0)
		,ISNULL(bitFieldMessage,0)
		,ISNULL(vcFieldMessage,'')
	FROM
		DycFormConfigurationDetails DFCD
	INNER JOIN
		CFW_Fld_Master CFM
	ON
		DFCD.numFieldId = CFM.Fld_id
	LEFT JOIN
		CFW_Validation CV
	ON
		CFM.Fld_id = CV.numFieldID
	WHERE
		DFCD.numDomainId=@numDomainID 
		AND DFCD.numUserCntID=@numUserCntID 
		AND DFCD.numFormId=135 
		AND DFCD.numViewId=@numViewID
		AND ISNULL(DFCD.bitCustom,0) = 1

	IF (SELECT COUNT(*) FROM @TempFieldsLeft) = 0
	BEGIN
		INSERT INTO @TempFieldsLeft
		(
			ID
			,numFieldID
			,vcFieldName
			,vcOrigDbColumnName
			,vcListItemType
			,numListID
			,bitAllowSorting
			,bitAllowFiltering
			,vcAssociatedControlType
			,vcLookBackTableName
			,bitCustomField
			,intRowNum
			,intColumnWidth
		)
		SELECT
			CONCAT(135,'~',DFM.numFieldID,'~',0)
			,DFM.numFieldID
			,DFFM.vcFieldName
			,DFM.vcOrigDbColumnName
			,ISNULL(DFM.vcListItemType,'')
			,ISNULL(DFM.numListID,0)
			,DFFM.bitAllowSorting
			,DFFM.bitAllowFiltering
			,DFM.vcAssociatedControlType
			,DFM.vcLookBackTableName
			,0
			,DFFM.[order]
			,100
		FROM
			DycFormField_Mapping DFFM
		INNER JOIN
			DycFieldMaster DFM
		ON
			DFFM.numFieldID = DFM.numFieldId
		WHERE 
			DFFM.numFormId=135 
			AND ISNULL(DFFM.bitDeleted,0) = 0
			AND ISNULL(DFFM.bitSettingField,0) = 1
			AND ISNULL(DFFM.bitDefault,0) = 1
	END

	DECLARE @j INT = 0
	DECLARE @jCount INT
	DECLARE @numFieldId NUMERIC(18,0)
	DECLARE @vcFieldName AS VARCHAR(50)                                                                                                
	DECLARE @vcAssociatedControlType VARCHAR(10)  
	DECLARE @vcDbColumnName VARCHAR(100)                                                
	--DECLARE @numListID AS NUMERIC(9)       
	DECLARE @bitCustomField BIT       
	SET @jCount = (SELECT COUNT(*) FROM @TempFieldsLeft)
	DECLARE @vcCustomFields VARCHAR(MAX) = ''
	DECLARE @vcCustomWhere VARCHAR(MAX) = ''

	WHILE @j <= @jCount
	BEGIN
		SELECT
			@numFieldId=numFieldID,
			@vcFieldName = vcFieldName,
			@vcAssociatedControlType = vcAssociatedControlType,
			@vcDbColumnName = CONCAT('Cust',numFieldID),
			@bitCustomField=ISNULL(bitCustomField,0)
		FROM 
			@TempFieldsLeft
		WHERE 
			numPKID = @j     
		
		
			
		IF @bitCustomField = 1
		BEGIN         
			IF @vcAssociatedControlType = 'TextBox' OR @vcAssociatedControlType = 'TextArea' 
			BEGIN
				SET @vcCustomFields = CONCAT(@vcCustomFields,',CFW',@numFieldId,'.Fld_Value  [',@vcDbColumnName,']')
             
				SET @vcCustomWhere = CONCAT(@vcCustomWhere,' left Join CFW_FLD_Values_Opp CFW',@numFieldId
								,' ON CFW' , @numFieldId , '.Fld_Id='
								,@numFieldId
								, ' and CFW' , @numFieldId
								, '.RecId=T1.numOppID')                                                         
			END   
			ELSE IF @vcAssociatedControlType = 'CheckBox' 
			BEGIN
				SET @vcCustomFields =CONCAT( @vcCustomFields
					, ',case when isnull(CFW'
					, @numFieldId
					, '.Fld_Value,0)=0 then 0 when isnull(CFW'
					, @numFieldId
					, '.Fld_Value,0)=1 then 1 end   ['
					,  @vcDbColumnName
					, ']')               
 
				SET @vcCustomWhere = CONCAT(@vcCustomWhere
					, ' left Join CFW_FLD_Values_Opp CFW'
					, @numFieldId
					, ' ON CFW',@numFieldId,'.Fld_Id='
					, @numFieldId
					, ' and CFW'
					, @numFieldId
					, '.RecId=T1.numOppID   ')                                                    
			END                
			ELSE IF @vcAssociatedControlType = 'DateField' 
			BEGIN
				SET @vcCustomFields = CONCAT(@vcCustomFields
					, ',dbo.FormatedDateFromDate(CFW'
					, @numFieldId
					, '.Fld_Value,'
					, @numDomainId
					, ')  [',@vcDbColumnName ,']' )  
					                  
				SET @vcCustomWhere = CONCAT(@vcCustomWhere
					, ' left Join CFW_FLD_Values_Opp CFW'
					, @numFieldId
					, ' on CFW', @numFieldId, '.Fld_Id='
					, @numFieldId
					, ' and CFW'
					, @numFieldId
					, '.RecId=T1.numOppID   ' )                                                        
			END                
			ELSE IF @vcAssociatedControlType = 'SelectBox' 
			BEGIN
				SET @vcCustomFields = CONCAT(@vcCustomFields,',L',@numFieldId,'.vcData',' [',@vcDbColumnName,']')
					                                                    
				SET @vcCustomWhere = CONCAT(@vcCustomWhere
					, ' left Join CFW_FLD_Values_Opp CFW'
					, @numFieldId
					, ' on CFW',@numFieldId ,'.Fld_Id='
					, @numFieldId
					, ' and CFW'
					, @numFieldId
					, '.RecId=T1.numOppID    ')     
					                                                    
				SET @vcCustomWhere = CONCAT(@vcCustomWhere
					, ' left Join ListDetails L'
					, @numFieldId
					, ' on L'
					, @numFieldId
					, '.numListItemID=CFW'
					, @numFieldId
					, '.Fld_Value' )              
			END
			ELSE
			BEGIN
				SET @vcCustomFields = CONCAT(@vcCustomFields,CONCAT(',dbo.GetCustFldValueOpp(',@numFieldId,',T1.numOppID)'),' [',@vcDbColumnName,']')
			END 
		END

		SET @j = @j + 1
	END
	
	CREATE TABLE #TEMPMSRecords
	(
		numOppID NUMERIC(18,0)
		,numOppItemID NUMERIC(18,0)
	)

	DECLARE @vcSQL NVARCHAR(MAX)
	DECLARE @vcSQLFinal NVARCHAR(MAX)
	
	SET @vcSQL = CONCAT('INSERT INTO #TEMPMSRecords
						(
							numOppID
							,numOppItemID
						)
						SELECT
							OpportunityMaster.numOppId
							,',(CASE WHEN @bitGroupByOrder = 1 THEN '0' ELSE 'OpportunityItems.numoppitemtCode' END),'
						FROM
							OpportunityMaster
						INNER JOIN
							DivisionMaster
						ON
							OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
						INNER JOIN
							CompanyInfo
						ON
							DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
						INNER JOIN 
							OpportunityItems
						ON
							OpportunityMaster.numOppId = OpportunityItems.numOppId
						INNER JOIN
							Item
						ON
							OpportunityItems.numItemCode = Item.numItemCode
						LEFT JOIN
							WareHouseItems
						ON
							OpportunityItems.numWarehouseItmsID = WareHouseItems.numWareHouseItemID
						WHERE
							OpportunityMaster.numDomainId=@numDomainID
							AND OpportunityMaster.tintOppType = 2
							AND OpportunityMaster.tintOppStatus = 1
							AND ISNULL(OpportunityMaster.tintshipped,0) = 0
							AND 1 = (CASE WHEN @numViewID IN (3,4) THEN 1 ELSE (CASE WHEN ISNULL(OpportunityItems.bitDropShip,0)=0 THEN 1 ELSE 0 END) END)
							AND 1 = (CASE 
										WHEN @numViewID=1 
										THEN (CASE WHEN ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyReceived,0) > 0 THEN 1 ELSE 0 END)
										WHEN @numViewID=2
										THEN (CASE WHEN ISNULL(OpportunityItems.numQtyReceived,0) > 0 AND ISNULL(OpportunityItems.numQtyReceived,0) - ISNULL(OpportunityItems.numUnitHourReceived,0) > 0 THEN 1 ELSE 0 END)
										WHEN @numViewID=3
										THEN (CASE 
												WHEN ISNULL(OpportunityItems.numUnitHour,0)  - ISNULL((SELECT 
																											SUM(OpportunityBizDocItems.numUnitHour)
																										FROM
																											OpportunityBizDocs
																										INNER JOIN
																											OpportunityBizDocItems
																										ON
																											OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																										WHERE
																											OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
																											AND OpportunityBizDocs.numBizDocId=644
																											AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode),0) > 0 
												THEN 1
												ELSE 0 
											END)
										ELSE 1
									END)
							AND 1 = (CASE WHEN @numViewID IN (1,2) AND ISNULL(@numWarehouseID,0) > 0 AND ISNULL(OpportunityItems.numWarehouseItmsID,0) > 0 THEN (CASE WHEN @numWarehouseID=WareHouseItems.numWarehouseID THEN 1 ELSE 0 END) ELSE 1 END)
							AND 1 = (CASE WHEN LEN(ISNULL(@vcOrderStauts,'''')) > 0 THEN (CASE WHEN OpportunityMaster.numStatus ',(CASE WHEN ISNULL(@bitIncludeSearch,0) = 0 THEN 'NOT IN' ELSE 'IN' END),' (SELECT Id FROM dbo.SplitIDs(@vcOrderStauts,'','')) THEN 1 ELSE 0 END) ELSE 1 END)'
							,@vcCustomSearchValue
							,(CASE WHEN @bitGroupByOrder = 1 THEN ' GROUP BY OpportunityMaster.numOppId' ELSE '' END));

	EXEC sp_executesql @vcSQL, N'@numDomainID NUMERIC(18,0), @vcOrderStauts VARCHAR(MAX), @numViewID NUMERIC(18,0), @numWarehouseID NUMERIC(18,0)', @numDomainID, @vcOrderStauts, @numViewID, @numWarehouseID;

	SET @numTotalRecords = (SELECT COUNT(*) FROM #TEMPMSRecords)
	
	CREATE TABLE #TEMPResult
	(
		numOppID NUMERIC(18,0)
		,numDivisionID NUMERIC(18,0)
		,numTerID NUMERIC(18,0)
		,numItemCode NUMERIC(18,0)
		,numOppItemID NUMERIC(18,0)
		,numWarehouseItemID NUMERIC(18,0)
		,vcCompanyName VARCHAR(300)
		,vcPoppName VARCHAR(300)
		,numStatus VARCHAR(300)
		,bintCreatedDate VARCHAR(50)
		,numAssignedTo VARCHAR(100)
		,numRecOwner VARCHAR(100)
		,vcItemName VARCHAR(300)
		,numOnHand FLOAT
		,numOnOrder FLOAT
		,numAllocation FLOAT
		,numBackOrder FLOAT
		,numBarCodeId VARCHAR(50)
		,vcLocation VARCHAR(100)
		,vcModelID VARCHAR(200)
		,vcItemDesc VARCHAR(2000)
		,numUnitHour FLOAT
		,numUOMId VARCHAR(50)
		,vcAttributes VARCHAR(300)
		,vcPathForTImage VARCHAR(300)
		,numUnitHourReceived FLOAT
		,numQtyReceived FLOAT
		,vcNotes VARCHAR(MAX)
		,SerialLotNo VARCHAR(MAX)
		,vcSKU VARCHAR(50)
		,numRemainingQty FLOAT
		,numQtyToShipReceive FLOAT
	)
	
	IF @bitGroupByOrder = 1
	BEGIN
		SET @vcSQLFinal = CONCAT('INSERT INTO #TEMPResult
								(
									numOppID
									,numDivisionID
									,numTerID
									,vcCompanyName
									,vcPoppName
									,bintCreatedDate
									,numStatus
									,numAssignedTo
									,numRecOwner
								)
								SELECT
									OpportunityMaster.numOppID
									,DivisionMaster.numDivisionID
									,ISNULL(DivisionMaster.numTerID,0) numTerID
									,ISNULL(CompanyInfo.vcCompanyName,'''')
									,ISNULL(OpportunityMaster.vcPoppName,'''')
									,dbo.FormatedDateFromDate(DATEADD(MINUTE,@ClientTimeZoneOffset * -1,OpportunityMaster.bintCreatedDate),@numDomainID)
									,dbo.GetListIemName(OpportunityMaster.numStatus)
									,dbo.fn_GetContactName(OpportunityMaster.numAssignedTo)
									,dbo.fn_GetContactName(OpportunityMaster.numRecOwner)
								FROM
									#TEMPMSRecords TEMPOrder
								INNER JOIN
									OpportunityMaster
								ON
									TEMPOrder.numOppID = OpportunityMaster.numOppId
								INNER JOIN
									DivisionMaster
								ON
									OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
								INNER JOIN
									CompanyInfo
								ON
									DivisionMaster.numCompanyID = CompanyInfo.numCompanyId ORDER BY ',
								(CASE @vcSortOrder
									WHEN 'CompanyInfo.vcCompanyName' THEN 'CompanyInfo.vcCompanyName '
									WHEN 'OpportunityMaster.vcPoppName' THEN 'OpportunityMaster.vcPoppName'
									WHEN 'OpportunityMaster.bintCreatedDate' THEN 'OpportunityMaster.bintCreatedDate'
									WHEN 'OpportunityMaster.numStatus' THEN 'dbo.GetListIemName(OpportunityMaster.numStatus)'
									WHEN 'OpportunityMaster.numAssignedTo' THEN 'dbo.fn_GetContactName(OpportunityMaster.numAssignedTo)'
									WHEN 'OpportunityMaster.numRecOwner' THEN 'dbo.fn_GetContactName(OpportunityMaster.numRecOwner)'
									ELSE 'OpportunityMaster.bintCreatedDate'
								END),' ',(CASE WHEN ISNULL(@vcSortOrder,'') <> '' THEN @vcSortOrder ELSE 'DESC' END),' OFFSET (@numPageIndex-1) * 200 ROWS FETCH NEXT 200 ROWS ONLY')


		EXEC sp_executesql @vcSQLFinal, N'@numDomainID NUMERIC(18,0), @ClientTimeZoneOffset INT, @numPageIndex INT', @numDomainID, @ClientTimeZoneOffset, @numPageIndex;
	END
	ELSE
	BEGIN
		SET @vcSQLFinal = CONCAT('INSERT INTO #TEMPResult
								(
									numOppID
									,numDivisionID
									,numTerID
									,vcCompanyName
									,vcPoppName
									,bintCreatedDate
									,numStatus
									,numAssignedTo
									,numRecOwner
									,numOppItemID
									,numWarehouseItemID
									,numItemCode
									,vcItemName
									,numOnHand
									,numOnOrder
									,numAllocation
									,numBackOrder
									,numBarCodeId
									,vcLocation
									,vcModelID
									,vcItemDesc
									,numUnitHour
									,numUOMId
									,vcAttributes
									,vcPathForTImage
									,numUnitHourReceived
									,numQtyReceived
									,vcNotes
									,SerialLotNo
									,vcSKU
									,numRemainingQty
									,numQtyToShipReceive
								)
								SELECT
									OpportunityMaster.numOppID
									,DivisionMaster.numDivisionID
									,ISNULL(DivisionMaster.numTerID,0) numTerID
									,ISNULL(CompanyInfo.vcCompanyName,'''')
									,ISNULL(OpportunityMaster.vcPoppName,'''')
									,dbo.FormatedDateFromDate(DATEADD(MINUTE,@ClientTimeZoneOffset * -1,OpportunityMaster.bintCreatedDate),@numDomainID)
									,dbo.GetListIemName(OpportunityMaster.numStatus)
									,dbo.fn_GetContactName(OpportunityMaster.numAssignedTo)
									,dbo.fn_GetContactName(OpportunityMaster.numRecOwner)
									,OpportunityItems.numoppitemtCode
									,ISNULL(OpportunityItems.numWarehouseItmsID,0)
									,OpportunityItems.numItemCode
									,ISNULL(Item.vcItemName,'''')
									,ISNULL(numOnHand,0) + ISNULL(numAllocation,0)
									,ISNULL(numOnOrder,0)
									,ISNULL(numAllocation,0)
									,ISNULL(numBackOrder,0)
									,ISNULL(Item.numBarCodeId,'''')
									,ISNULL(WarehouseLocation.vcLocation,'''')
									,ISNULL(Item.vcModelID,'''')
									,ISNULL(OpportunityItems.vcItemDesc,'''')
									,ISNULL(OpportunityItems.numUnitHour,0)
									,dbo.fn_GetUOMName(OpportunityItems.numUOMId)
									,ISNULL(OpportunityItems.vcAttributes,'''')
									,ISNULL(Item.vcPathForTImage,'''')
									,ISNULL(OpportunityItems.numUnitHourReceived,0)
									,ISNULL(OpportunityItems.numQtyReceived,0)
									,ISNULL(OpportunityItems.vcNotes,'''')
									,SUBSTRING((SELECT '','' + vcSerialNo + CASE WHEN isnull(Item.bitLotNo,0)=1 THEN '' ('' + CONVERT(VARCHAR(15),whi.numQty) + '')'' ELSE '''' end 
									FROM OppWarehouseSerializedItem oppI JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID=whi.numWareHouseItmsDTLID where oppI.numOppID=OpportunityItems.numOppId and oppI.numOppItemID=OpportunityItems.numoppitemtCode 
									ORDER BY vcSerialNo FOR XML PATH('''')),2,200000)
									,ISNULL(Item.vcSKU,'''')
									,(CASE 
										WHEN @numViewID=1 THEN ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyReceived,0) 
										WHEN @numViewID=2 THEN ISNULL(OpportunityItems.numQtyReceived,0) - ISNULL(OpportunityItems.numUnitHourReceived,0) 
										WHEN @numViewID=3 THEN ISNULL(OpportunityItems.numUnitHour,0)  - ISNULL((SELECT 
																											SUM(OpportunityBizDocItems.numUnitHour)
																										FROM
																											OpportunityBizDocs
																										INNER JOIN
																											OpportunityBizDocItems
																										ON
																											OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																										WHERE
																											OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
																											AND OpportunityBizDocs.numBizDocId=644
																											AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode),0)
										ELSE ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numUnitHourReceived,0) 
									END)
									,(CASE 
										WHEN @numViewID=1 THEN ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyReceived,0) 
										WHEN @numViewID=2 THEN ISNULL(OpportunityItems.numQtyReceived,0) - ISNULL(OpportunityItems.numUnitHourReceived,0) 
										ELSE ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numUnitHourReceived,0) 
									END)
								FROM
									#TEMPMSRecords TEMPOrder
								INNER JOIN
									OpportunityMaster
								ON
									TEMPOrder.numOppID = OpportunityMaster.numOppId
								INNER JOIN
									DivisionMaster
								ON
									OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
								INNER JOIN
									CompanyInfo
								ON
									DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
								INNER JOIN 
									OpportunityItems
								ON
									OpportunityMaster.numOppId = OpportunityItems.numOppId
									AND TEMPOrder.numOppItemID = OpportunityItems.numoppitemtCode
								INNER JOIN
									Item
								ON
									OpportunityItems.numItemCode = Item.numItemCode
								LEFT JOIN
									WareHouseItems
								ON
									OpportunityItems.numWarehouseItmsID = WareHouseItems.numWareHouseItemID
								LEFT JOIN
									WarehouseLocation
								ON
									WareHouseItems.numWLocationID = WarehouseLocation.numWLocationID ORDER BY ',
								(CASE @vcSortOrder
									WHEN 'CompanyInfo.vcCompanyName' THEN 'CompanyInfo.vcCompanyName '
									WHEN 'OpportunityMaster.vcPoppName' THEN 'OpportunityMaster.vcPoppName'
									WHEN 'OpportunityMaster.bintCreatedDate' THEN 'OpportunityMaster.bintCreatedDate'
									WHEN 'OpportunityMaster.numStatus' THEN 'dbo.GetListIemName(OpportunityMaster.numStatus)'
									WHEN 'OpportunityMaster.numAssignedTo' THEN 'dbo.fn_GetContactName(OpportunityMaster.numAssignedTo)'
									WHEN 'OpportunityMaster.numRecOwner' THEN 'dbo.fn_GetContactName(OpportunityMaster.numRecOwner)'
									WHEN 'numOnHand' THEN 'ISNULL(WareHouseItems.numOnHand,0) + ISNULL(WareHouseItems.numAllocation,0)'
									WHEN 'numOnOrder' THEN 'ISNULL(WareHouseItems.numOnOrder,0)'
									WHEN 'numAllocation' THEN 'ISNULL(WareHouseItems.numAllocation,0)'
									WHEN 'numBackOrder' THEN 'ISNULL(WareHouseItems.numBackOrder,0)'
									WHEN 'numItemCode' THEN 'Item.numItemCode'
									WHEN 'vcItemName' THEN 'Item.vcItemName'
									WHEN 'numUnitHour' THEN 'OpportunityItems.numUnitHour'
									WHEN 'numUnitHourReceived' THEN 'OpportunityItems.numUnitHourReceived'
									ELSE 'OpportunityMaster.bintCreatedDate'
								END),' ',(CASE WHEN ISNULL(@vcSortOrder,'') <> '' THEN @vcSortOrder ELSE 'DESC' END),' OFFSET (@numPageIndex-1) * 200 ROWS FETCH NEXT 200 ROWS ONLY')

		EXEC sp_executesql @vcSQLFinal, N'@numDomainID NUMERIC(18,0), @numViewID NUMERIC(18,0), @ClientTimeZoneOffset INT, @numPageIndex INT', @numDomainID, @numViewID, @ClientTimeZoneOffset, @numPageIndex;
	END
	
	SET @vcSQLFinal = CONCAT('SELECT T1.*',@vcCustomFields,' FROM #TEMPResult T1',@vcCustomWhere)
	EXEC sp_executesql @vcSQLFinal

	SELECT * FROM @TempFieldsLeft ORDER BY intRowNum

	IF OBJECT_ID('tempdb..#TEMPMSRecords') IS NOT NULL DROP TABLE #TEMPMSRecords
	IF OBJECT_ID('tempdb..#TEMPResult') IS NOT NULL DROP TABLE #TEMPResult
END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_MassPurchaseFulfillment_GetRecordsForPutAway')
DROP PROCEDURE dbo.USP_MassPurchaseFulfillment_GetRecordsForPutAway
GO

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_MassPurchaseFulfillment_GetRecordsForPutAway]
(
	@numDomainID NUMERIC(18,0)
	,@numUserCntID NUMERIC(18,0)
	,@tintMode TINYINT
	,@vcSelectedRecords VARCHAR(MAX)
)
AS
BEGIN
	DECLARE @TempFieldsRight TABLE
	(
		ID VARCHAR(50)
		,numFieldID NUMERIC(18,0)
		,vcFieldName VARCHAR(100)
		,vcOrigDbColumnName VARCHAR(100)
		,bitAllowSorting BIT
		,bitAllowFiltering BIT
		,vcAssociatedControlType VARCHAR(20)
		,bitCustomField BIT
		,intRowNum INT
		,intColumnWidth FLOAT
	)

	INSERT INTO @TempFieldsRight
	(
		ID
		,numFieldID
		,vcFieldName
		,vcOrigDbColumnName
		,bitAllowSorting
		,bitAllowFiltering
		,vcAssociatedControlType
		,bitCustomField
		,intRowNum
		,intColumnWidth
	)
	SELECT
		CONCAT(143,'~',DFM.numFieldID,'~',0)
		,DFM.numFieldID
		,DFFM.vcFieldName
		,DFM.vcOrigDbColumnName
		,DFFM.bitAllowSorting
		,DFFM.bitAllowFiltering
		,DFM.vcAssociatedControlType
		,0
		,DFCD.intRowNum
		,ISNULL(DFCD.intColumnWidth,0)
	FROM
		DycFormConfigurationDetails DFCD
	INNER JOIN
		DycFormField_Mapping DFFM
	ON
		DFCD.numFormId = DFFM.numFormID
		AND DFCD.numFieldId = DFFM.numFieldID
	INNER JOIN
		DycFieldMaster DFM
	ON
		DFFM.numFieldID = DFM.numFieldId
	WHERE 
		DFCD.numDomainId=@numDomainID 
		AND DFCD.numUserCntID=@numUserCntID 
		AND DFCD.numFormId=143 
		AND ISNULL(DFFM.bitDeleted,0) = 0
		AND ISNULL(DFFM.bitSettingField,0) = 1
		AND ISNULL(DFCD.bitCustom,0) = 0

	IF (SELECT COUNT(*) FROM @TempFieldsRight) = 0
	BEGIN
		INSERT INTO @TempFieldsRight
		(
			ID
			,numFieldID
			,vcFieldName
			,vcOrigDbColumnName
			,bitAllowSorting
			,bitAllowFiltering
			,vcAssociatedControlType
			,bitCustomField
			,intRowNum
			,intColumnWidth
		)
		SELECT
			CONCAT(143,'~',DFM.numFieldID,'~',0)
			,DFM.numFieldID
			,DFFM.vcFieldName
			,DFM.vcOrigDbColumnName
			,DFFM.bitAllowSorting
			,DFFM.bitAllowFiltering
			,DFM.vcAssociatedControlType
			,0
			,DFFM.[order]
			,100
		FROM
			DycFormField_Mapping DFFM
		INNER JOIN
			DycFieldMaster DFM
		ON
			DFFM.numFieldID = DFM.numFieldId
		WHERE 
			DFFM.numFormId=143 
			AND ISNULL(DFFM.bitDeleted,0) = 0
			AND ISNULL(DFFM.bitSettingField,0) = 1
			AND (ISNULL(DFFM.bitDefault,0) = 1 OR ISNULL(DFFM.bitRequired,0) = 1)
	END
	ELSE
	BEGIN
		INSERT INTO @TempFieldsRight
		(
			ID
			,numFieldID
			,vcFieldName
			,vcOrigDbColumnName
			,bitAllowSorting
			,bitAllowFiltering
			,vcAssociatedControlType
			,bitCustomField
			,intRowNum
			,intColumnWidth
		)
		SELECT
			CONCAT(143,'~',DFM.numFieldID,'~',0)
			,DFM.numFieldID
			,DFFM.vcFieldName
			,DFM.vcOrigDbColumnName
			,DFFM.bitAllowSorting
			,DFFM.bitAllowFiltering
			,DFM.vcAssociatedControlType
			,0
			,DFFM.[order]
			,100
		FROM
			DycFormField_Mapping DFFM
		INNER JOIN
			DycFieldMaster DFM
		ON
			DFFM.numFieldID = DFM.numFieldId
		WHERE 
			DFFM.numFormId=143 
			AND ISNULL(DFFM.bitDeleted,0) = 0
			AND ISNULL(DFFM.bitSettingField,0) = 1
			AND ISNULL(DFFM.bitRequired,0) = 1
			AND DFM.numFieldId NOT IN (SELECT T1.numFieldID FROM @TempFieldsRight T1)
	END


	DECLARE @TEMP TABLE
	(
		ID INT IDENTITY(1,1)
		,numOppID NUMERIC(18,0)
		,numOppItemID NUMERIC(18,0)
	)

	INSERT INTO @TEMP
	(
		numOppID
		,numOppItemID
	)
	SELECT
		CAST(SUBSTRING(OutParam,0,CHARINDEX('-',OutParam)) AS NUMERIC(18,0)) numOppID
		,CAST(SUBSTRING(OutParam,CHARINDEX('-',OutParam) + 1,LEN(OutParam)) AS NUMERIC(18,0)) numOppItemID
	FROM
		dbo.SplitString(@vcSelectedRecords,',')

	DECLARE @TEMPItems TABLE
	(	
		ID NUMERIC(18,0)
		,numSortOrder INT
		,numOppID NUMERIC(18,0)
		,bitStockTransfer BIT
		,numDivisionID NUMERIC(18,0)
		,numOppItemID NUMERIC(18,0)
		,numItemCode NUMERIC(18,0)
		,vcLocation NUMERIC(18,0)
		,vcPoppName VARCHAR(300)
		,numBarCodeId VARCHAR(300)
		,vcItemName VARCHAR(300)
		,vcItemDesc VARCHAR(MAX)
		,vcSKU VARCHAR(300)
		,vcPathForTImage VARCHAR(MAX)
		,vcAttributes VARCHAR(MAX)
		,numRemainingQty FLOAT
		,numQtyToShipReceive FLOAT
		,bitSerialized BIT
		,bitLotNo BIT
		,bitPPVariance BIT
		,numCurrencyID NUMERIC(18,0)
		,fltExchangeRate FLOAT
		,charItemType CHAR(3)
		,vcItemType VARCHAR(50)
		,monPrice DECIMAL(20,5)
		,bitDropShip BIT
		,numProjectID NUMERIC(18,0)
		,numClassID NUMERIC(18,0)
		,numAssetChartAcntId NUMERIC(18,0)
		,numCOGsChartAcntId NUMERIC(18,0)
		,numIncomeChartAcntId NUMERIC(18,0)
		,vcWarehouses VARCHAR(MAX)
	)

	INSERT INTO @TEMPItems
	(	
		ID
		,numSortOrder
		,numOppID
		,bitStockTransfer
		,numDivisionID
		,numOppItemID 
		,numItemCode
		,vcLocation
		,vcPoppName
		,numBarCodeId
		,vcItemName
		,vcItemDesc
		,vcSKU
		,vcPathForTImage
		,vcAttributes 
		,numRemainingQty
		,numQtyToShipReceive
		,bitSerialized
		,bitLotNo
		,bitPPVariance
		,numCurrencyID
		,fltExchangeRate
		,charItemType
		,vcItemType
		,monPrice
		,bitDropShip
		,numProjectID
		,numClassID
		,numAssetChartAcntId
		,numCOGsChartAcntId
		,numIncomeChartAcntId
		,vcWarehouses
	)
	SELECT 
		T1.ID
		,ISNULL(OpportunityItems.numSortOrder,0)
		,OpportunityMaster.numOppID
		,ISNULL(OpportunityMaster.bitStockTransfer,0)
		,OpportunityMaster.numDivisionId
		,OpportunityItems.numoppitemtCode
		,OpportunityItems.numItemCode
		,OpportunityItems.numWarehouseItmsID
		,OpportunityMaster.vcPoppName
		,Item.numBarCodeId
		,Item.vcItemName
		,Item.txtItemDesc AS vcItemDesc
		,Item.vcSKU
		,ISNULL(ItemImages.vcPathForTImage,'') vcPathForTImage
		,ISNULL(OpportunityItems.vcAttributes,'') AS vcAttributes
		,(CASE 
			WHEN @tintMode=1 THEN ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyReceived,0) 
			WHEN @tintMode=3 THEN ISNULL(OpportunityItems.numQtyReceived,0) - ISNULL(OpportunityItems.numUnitHourReceived,0)
			ELSE 0
		END) numRemainingQty
		,0 numQtyToShipReceive
		,ISNULL(bitSerialized,0)
		,ISNULL(bitLotNo,0)
		,ISNULL(OpportunityMaster.bitPPVariance,0)
		,ISNULL(OpportunityMaster.numCurrencyID,0)
		,ISNULL(OpportunityMaster.fltExchangeRate,0)
		,Item.charItemType
		,OpportunityItems.vcType
		,ISNULL(OpportunityItems.monPrice,0)
		,ISNULL(OpportunityItems.bitDropShip,0)
		,ISNULL(OpportunityItems.numProjectID,0)
		,ISNULL(OpportunityItems.numClassID,0)
		,ISNULL(Item.numAssetChartAcntId,0)
		,ISNULL(Item.numCOGsChartAcntId,0)
		,ISNULL(Item.numIncomeChartAcntId,0)
		,STUFF((SELECT 
				N'#,#' + CONCAT(WareHouseItems.numWareHouseItemID,'-',CONCAT(Warehouses.vcWareHouse,(CASE WHEN LEN(ISNULL(WarehouseLocation.vcLocation,'')) > 0 THEN CONCAT(' (',WarehouseLocation.vcLocation,')') ELSE '' END)))
			FROM 
				WareHouseItems 
			INNER JOIN
				Warehouses
			ON
				WareHouseItems.numWareHouseID = Warehouses.numWareHouseID
			LEFT JOIN
				WarehouseLocation
			ON
				WareHouseItems.numWLocationID = WarehouseLocation.numWLocationID
			WHERE 
				WareHouseItems.numDomainID=@numDomainID 
				AND numItemID=Item.numItemCode 
			ORDER BY
				Warehouses.vcWareHouse
				,WarehouseLocation.vcLocation
			FOR XML PATH(''),TYPE)
				.value('text()[1]','nvarchar(max)'),1,3,N'')
	FROM
		@TEMP T1
	INNER JOIN
		OpportunityMaster
	ON
		OpportunityMaster.numOppID= T1.numOppID
	INNER JOIN
		OpportunityItems
	ON
		OpportunityMaster.numOppID = OpportunityItems.numOppID
		AND (OpportunityItems.numOppItemtcode = T1.numOppItemID OR ISNULL(T1.numOppItemID,0) = 0)
	INNER JOIN
		Item 
	ON
		OpportunityItems.numItemCode = Item.numItemCode
	LEFT JOIN
		ItemImages
	ON
		ItemImages.numDomainId=@numDomainID
		AND ItemImages.numItemCode = Item.numItemCode
		AND ItemImages.bitDefault=1 
		AND ItemImages.bitIsImage=1
	WHERE
		OpportunityMaster.numDomainID=@numDomainID
		AND (CASE 
			WHEN @tintMode=1 THEN ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyReceived,0) 
			WHEN @tintMode=3 THEN ISNULL(OpportunityItems.numQtyReceived,0) - ISNULL(OpportunityItems.numUnitHourReceived,0)
			ELSE 0
		END) > 0
	ORDER BY
		T1.ID, OpportunityItems.numSortOrder

	SELECT * FROM @TEMPItems ORDER BY ID	
	SELECT * FROM @TempFieldsRight ORDER BY intRowNum
END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_MassPurchaseFulfillment_ReceiveOnlyItems')
DROP PROCEDURE dbo.USP_MassPurchaseFulfillment_ReceiveOnlyItems
GO

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_MassPurchaseFulfillment_ReceiveOnlyItems]
(
	@numDomainID NUMERIC(18,0)
	,@numUserCntID NUMERIC(18,0)
	,@vcSelectedRecords VARCHAR(MAX)
)
AS 
BEGIN
BEGIN TRY
BEGIN TRANSACTION
	DECLARE @hDocItem INT

	DECLARE @Temp TABLE
	(
		ID INT IDENTITY(1,1)
		,OppID NUMERIC(18,0)
		,OppItemID NUMERIC(18,0)
		,numQtyToReceive FLOAT
		,IsSuccess BIT
		,ErrorMessage VARCHAR(300)
	)

	EXEC sp_xml_preparedocument @hDocItem OUTPUT, @vcSelectedRecords

	INSERT INTO @Temp
	(
		OppID
		,OppItemID
		,numQtyToReceive
		,IsSuccess
	)
	SELECT
		OppID
		,OppItemID
		,QtyToReceive
		,IsSuccess
	FROM
		OPENXML (@hDocItem,'/NewDataSet/Table1',2)
	WITH
	(
		OppID NUMERIC(18,0),
		OppItemID NUMERIC(18,0),
		QtyToReceive FLOAT,
		IsSuccess BIT
	)

	EXEC sp_xml_removedocument @hDocItem 

	DECLARE @i INT = 1
	DECLARE @iCount INT
	DECLARE @numOppID NUMERIC(18,0)
	DECLARE @numOppItemID NUMERIC(18,0)
	DECLARE @numQtyToReceive FLOAT
	SELECT @iCount = COUNT(*) FROM @Temp

	WHILE @i <= @iCount
	BEGIN
		SELECT 
			@numOppID = OppID
			,@numOppItemID = ISNULL(OppItemID,0)
			,@numQtyToReceive = ISNULL(numQtyToReceive,0)
		FROM 
			@Temp 
		WHERE 
			ID = @i


		IF @numOppItemID > 0
		BEGIN
			IF EXISTS (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId=@numOppID AND numoppitemtCode=@numOppItemID AND ISNULL(numUnitHour,0) >= (ISNULL(numQtyReceived,0) + @numQtyToReceive))
			BEGIN
				UPDATE
					OpportunityItems
				SET
					numQtyReceived = ISNULL(numQtyReceived,0) + @numQtyToReceive
				WHERE
					numOppId=@numOppID 
					AND numoppitemtCode=@numOppItemID

				UPDATE 
					@Temp
				SET
					IsSuccess = 1
				WHERE
					ID=@i
			END
			ELSE
			BEGIN
				UPDATE 
					@Temp
				SET
					IsSuccess = 0
					,ErrorMessage = 'Received quantity can not be greater than ordered quantity'
				WHERE
					ID=@i
			END
		END
		ELSE
		BEGIN
			UPDATE
				OpportunityItems
			SET
				numQtyReceived = numUnitHour
			WHERE
				numOppId=@numOppID 
				AND ISNULL(OpportunityItems.bitDropShip,0)=0
				AND ISNULL(numUnitHour,0) <> ISNULL(numQtyReceived,0)

			UPDATE 
				@Temp
			SET
				IsSuccess = 1
			WHERE
				ID=@i
		END
		

		SET @i = @i + 1
	END

	UPDATE
		OM
	SET
		bintModifiedDate=GETUTCDATE()
		,numModifiedBy = @numUserCntID
	FROM
		OpportunityMaster OM
	INNER JOIN
		@Temp T1
	ON
		OM.numOppId = T1.OppID
		AND T1.IsSuccess = 1
	WHERE
		OM.numDomainId = @numDomainID

	SELECT * FROM @Temp
COMMIT
END TRY
BEGIN CATCH
DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);

END CATCH
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_MassPurchaseFulfillmentConfiguration_GetByUser')
DROP PROCEDURE dbo.USP_MassPurchaseFulfillmentConfiguration_GetByUser
GO
CREATE PROCEDURE [dbo].[USP_MassPurchaseFulfillmentConfiguration_GetByUser]
(
	@numDomainID NUMERIC(18,0),
    @numUserCntID NUMERIC(18,0)
)
AS 
BEGIN
	SELECT * FROM MassPurchaseFulfillmentConfiguration WHERE numDomainID=@numDomainID AND numUserCntID=@numUserCntID
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_MassPurchaseFulfillmentConfiguration_Save')
DROP PROCEDURE dbo.USP_MassPurchaseFulfillmentConfiguration_Save
GO
CREATE PROCEDURE [dbo].[USP_MassPurchaseFulfillmentConfiguration_Save]
(
	@numDomainID NUMERIC(18,0)
    ,@numUserCntID NUMERIC(18,0)
	,@bitGroupByOrderForReceive BIT
	,@bitGroupByOrderForPutAway BIT
	,@bitGroupByOrderForBill BIT
	,@bitGroupByOrderForClose BIT
	,@tintScanValue TINYINT
)
AS 
BEGIN
	IF EXISTS (SELECT ID FROM MassPurchaseFulfillmentConfiguration WHERE numDomainID=@numDomainID AND numUserCntID=@numUserCntID)
	BEGIN
		UPDATE
			MassPurchaseFulfillmentConfiguration
		SET
			bitGroupByOrderForReceive=@bitGroupByOrderForReceive
			,bitGroupByOrderForPutAway=@bitGroupByOrderForPutAway
			,bitGroupByOrderForBill=@bitGroupByOrderForBill
			,bitGroupByOrderForClose=@bitGroupByOrderForClose
			,tintScanValue=@tintScanValue
		WHERE
			numDomainID=@numDomainID 
			AND numUserCntID=@numUserCntID
	END
	ELSE
	BEGIN
		INSERT INTO MassPurchaseFulfillmentConfiguration
		(
			numDomainID 
			,numUserCntID
			,bitGroupByOrderForReceive
			,bitGroupByOrderForPutAway
			,bitGroupByOrderForBill
			,bitGroupByOrderForClose
			,tintScanValue
		)
		VALUES
		(
			@numDomainID 
			,@numUserCntID
			,@bitGroupByOrderForReceive
			,@bitGroupByOrderForPutAway
			,@bitGroupByOrderForBill
			,@bitGroupByOrderForClose
			,@tintScanValue
		)
	END
END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_MassSalesFulfillment_GetConfiguration')
DROP PROCEDURE dbo.USP_MassSalesFulfillment_GetConfiguration
GO

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_MassSalesFulfillment_GetConfiguration]
(
	@numDomainID NUMERIC(18,0)
	,@numUserCntID NUMERIC(18,0)
)
AS
BEGIN
	DECLARE @numDefaultSalesShippingDoc NUMERIC(18,0)

	SELECT @numDefaultSalesShippingDoc=ISNULL(numDefaultSalesShippingDoc,0) FROM Domain WHERE numDomainId=@numDomainID

	IF EXISTS (SELECT ID FROM MassSalesFulfillmentConfiguration WHERE numDomainID=@numDomainID AND numUserCntID=@numUserCntID)
	BEGIN
		SELECT 
			ISNULL(bitGroupByOrderForPick,0) bitGroupByOrderForPick
			,ISNULL(bitGroupByOrderForShip,0) bitGroupByOrderForShip
			,ISNULL(bitGroupByOrderForInvoice,0) bitGroupByOrderForInvoice
			,ISNULL(bitGroupByOrderForPay,0) bitGroupByOrderForPay
			,ISNULL(bitGroupByOrderForClose,0) bitGroupByOrderForClose
			,ISNULL(tintInvoicingType,1) tintInvoicingType
			,ISNULL(tintScanValue,1) tintScanValue
			,ISNULL(tintPackingMode,1) tintPackingMode
			,@numDefaultSalesShippingDoc numDefaultSalesShippingDoc
			,ISNULL(tintPendingCloseFilter,1) tintPendingCloseFilter
			,ISNULL(bitGeneratePickListByOrder,0) bitGeneratePickListByOrder
		FROM	
			MassSalesFulfillmentConfiguration
		WHERE
			numDomainID=@numDomainID
			AND numUserCntID=@numUserCntID
	END
	ELSE 
	BEGIN
		SELECT 
			0 bitGroupByOrderForPick
			,0 bitGroupByOrderForShip
			,0 bitGroupByOrderForInvoice
			,0 bitGroupByOrderForPay
			,0 bitGroupByOrderForClose
			,2 tintInvoicingType
			,1 tintScanValue
			,1 tintPackingMode
			,@numDefaultSalesShippingDoc numDefaultSalesShippingDoc
			,1 tintPendingCloseFilter
			,0 bitGeneratePickListByOrder
	END
END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_MassSalesFulfillment_GetRecords')
DROP PROCEDURE dbo.USP_MassSalesFulfillment_GetRecords
GO

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_MassSalesFulfillment_GetRecords]
(
	@numDomainID NUMERIC(18,0)
	,@numUserCntID NUMERIC(18,0)
	,@ClientTimeZoneOffset INT
	,@numViewID NUMERIC(18,0)
	,@numWarehouseID NUMERIC(18,0)
	,@bitGroupByOrder BIT
	,@numShippingZone NUMERIC(18,0)
	,@bitIncludeSearch BIT
	,@vcOrderStauts VARCHAR(MAX)
	,@tintFilterBy TINYINT
	,@vcFilterValue VARCHAR(MAX)
	,@vcCustomSearchValue VARCHAR(MAX)
	,@numBatchID NUMERIC(18,0)
	,@tintPackingViewMode TINYINT
	,@tintPrintBizDocViewMode TINYINT
	,@tintPendingCloseFilter TINYINT
	,@numPageIndex INT
	,@vcSortColumn VARCHAR(100) OUTPUT
	,@vcSortOrder VARCHAR(4) OUTPUT
	,@numTotalRecords NUMERIC(18,0) OUTPUT
)
AS 
BEGIN
	IF @numViewID = 4 OR @numViewID = 5 OR @numViewID = 6 OR (@numViewID=2 AND @tintPackingViewMode=4)
	BEGIN
		SET @bitGroupByOrder = 1
	END

	SET @vcCustomSearchValue = REPLACE(@vcCustomSearchValue,'OpportunityMaster.dtItemReceivedDate','(SELECT TOP 1 OMInner.dtItemReceivedDate FROM OpportunityMaster OMInner INNER JOIN OpportunityItems OIInner ON OMInner.numOppID=OIInner.numOppID WHERE OMInner.tintOppType=2 AND OMInner.tintOppStatus=1 AND OIInner.numItemCode=OpportunityItems.numItemCode ORDER BY OMInner.dtItemReceivedDate DESC)')

	DECLARE @TempFieldsLeft TABLE
	(
		numPKID INT IDENTITY(1,1)
		,ID VARCHAR(50)
		,numFieldID NUMERIC(18,0)
		,vcFieldName VARCHAR(100)
		,vcOrigDbColumnName VARCHAR(100)
		,vcListItemType VARCHAR(10)
		,numListID NUMERIC(18,0)
		,bitAllowSorting BIT
		,bitAllowFiltering BIT
		,vcAssociatedControlType VARCHAR(20)
		,vcLookBackTableName VARCHAR(100)
		,bitCustomField BIT
		,intRowNum INT
		,intColumnWidth FLOAT
		,bitIsRequired BIT
		,bitIsEmail BIT
		,bitIsAlphaNumeric BIT
		,bitIsNumeric BIT
		,bitIsLengthValidation BIT
		,intMaxLength INT
		,intMinLength INT
		,bitFieldMessage BIT
		,vcFieldMessage VARCHAR(MAX)
		,Grp_id INT
	)

	CREATE TABLE #TEMPResult
	(
		numOppID NUMERIC(18,0)
		,numDivisionID NUMERIC(18,0)
		,numTerID NUMERIC(18,0)
		,numOppItemID NUMERIC(18,0)
		,numOppBizDocID NUMERIC(18,0)
		,bintCreatedDate VARCHAR(50)
		,numItemCode NUMERIC(18,0)
		,vcCompanyName VARCHAR(300)
		,vcPoppName VARCHAR(300)
		,numAssignedTo VARCHAR(100)
		,numRecOwner VARCHAR(100)
		,vcBizDocID VARCHAR(300)
		,numStatus VARCHAR(300)
		,txtComments VARCHAR(MAX)
		,numBarCodeId VARCHAR(50)
		,vcLocation VARCHAR(100)
		,vcItemName VARCHAR(300)
		,vcItemDesc VARCHAR(2000)
		,numQtyShipped FLOAT
		,numUnitHour FLOAT
		,monAmountPaid DECIMAL(20,5)
		,monAmountToPay DECIMAL(20,5)
		,numItemClassification VARCHAR(100)
		,vcSKU VARCHAR(50)
		,charItemType VARCHAR(50)
		,vcAttributes VARCHAR(300)
		,vcNotes VARCHAR(MAX)
		,vcItemReleaseDate DATE
		,dtItemReceivedDate VARCHAR(50)
		,intUsedShippingCompany VARCHAR(300)
		,intShippingCompany VARCHAR(300)
		,numPreferredShipVia NUMERIC(18,0)
		,numPreferredShipService NUMERIC(18,0)
		,vcInvoiced FLOAT
		,vcInclusionDetails VARCHAR(MAX)
		,bitPaidInFull BIT
		,numRemainingQty FLOAT
		,numAge VARCHAR(50)
		,dtAnticipatedDelivery  DATE
		,vcShipStatus VARCHAR(MAX)
		,dtExpectedDate VARCHAR(20)
		,dtExpectedDateOrig DATE
		,numQtyPicked FLOAT
		,numQtyPacked FLOAT
		,vcPaymentStatus VARCHAR(MAX)
		,numShipRate VARCHAR(40)
	)

	IF @bitGroupByOrder = 1
	BEGIN
		IF @vcSortColumn NOT IN ('CompanyInfo.vcCompanyName','OpportunityBizDocs.monAmountPaid','OpportunityMaster.bintCreatedDate','OpportunityMaster.bitPaidInFull','OpportunityMaster.numAge','OpportunityMaster.numStatus','OpportunityMaster.vcPoppName','OpportunityMaster.dtItemReceivedDate','OpportunityMaster.numAssignedTo','OpportunityMaster.numRecOwner')
		BEGIN
			SET @vcSortColumn = 'OpportunityMaster.bintCreatedDate'
			SET @vcSortOrder = 'DESC'
		END
	END
	ELSE 
	BEGIN
		IF @vcSortColumn NOT IN ('CompanyInfo.vcCompanyName'
								,'Item.charItemType'
								,'Item.numBarCodeId'
								,'Item.numItemClassification'
								,'Item.vcSKU'
								,'OpportunityBizDocs.monAmountPaid'
								,'OpportunityMaster.dtExpectedDate'
								,'OpportunityItems.numQtyPacked'
								,'OpportunityItems.numQtyPicked'
								,'OpportunityItems.numQtyShipped'
								,'OpportunityItems.numRemainingQty'
								,'OpportunityItems.numUnitHour'
								,'OpportunityItems.vcInvoiced'
								,'OpportunityItems.vcItemName'
								,'OpportunityItems.vcItemReleaseDate'
								,'OpportunityMaster.bintCreatedDate'
								,'OpportunityMaster.bitPaidInFull'
								,'OpportunityMaster.numAge'
								,'OpportunityMaster.numStatus'
								,'OpportunityMaster.vcPoppName'
								,'WareHouseItems.vcLocation'
								,'OpportunityMaster.dtItemReceivedDate')
		BEGIN
			SET @vcSortColumn = 'OpportunityMaster.bintCreatedDate'
			SET @vcSortOrder = 'DESC'
		END
	END

	DECLARE @tintPackingMode TINYINT
	DECLARE @tintInvoicingType TINYINT
	DECLARE @tintCommitAllocation TINYINT
	DECLARE @numShippingServiceItemID NUMERIC(18,0)
	DECLARE @numDefaultSalesShippingDoc NUMERIC(18,0)
	SELECT 
		@tintCommitAllocation=ISNULL(tintCommitAllocation,1) 
		,@numShippingServiceItemID=ISNULL(numShippingServiceItemID,0)
		,@numDefaultSalesShippingDoc=ISNULL(numDefaultSalesShippingDoc,0)
	FROM 
		Domain 
	WHERE 
		numDomainId=@numDomainID 

	IF @numViewID = 2 AND @tintPackingViewMode=3 AND @numDefaultSalesShippingDoc = 0
	BEGIN
		RAISERROR('DEFAULT_SHIPPING_BIZDOC_NOT_CONFIGURED',16,1)
		RETURN
	END

	SELECT
		@tintPackingMode=tintPackingMode
		,@tintInvoicingType=tintInvoicingType
	FROM
		MassSalesFulfillmentConfiguration
	WHERE
		numDomainID = @numDomainID 
		AND numUserCntID = @numUserCntID

	PRINT CONCAT('1-',CONVERT( VARCHAR(24), GETDATE(), 121))

	INSERT INTO @TempFieldsLeft
	(
		ID
		,numFieldID
		,vcFieldName
		,vcOrigDbColumnName
		,vcListItemType
		,numListID
		,bitAllowSorting
		,bitAllowFiltering
		,vcAssociatedControlType
		,vcLookBackTableName
		,bitCustomField
		,intRowNum
		,intColumnWidth
		,bitIsRequired
		,bitIsEmail
		,bitIsAlphaNumeric
		,bitIsNumeric
		,bitIsLengthValidation
		,intMaxLength
		,intMinLength
		,bitFieldMessage
		,vcFieldMessage
		,Grp_id
	)
	SELECT
		CONCAT(141,'~',DFM.numFieldID,'~',0)
		,DFM.numFieldID
		,(CASE WHEN @numViewID=6 AND DFM.numFieldID=248 THEN 'BizDoc' ELSE DFFM.vcFieldName END)
		,DFM.vcOrigDbColumnName
		,ISNULL(DFM.vcListItemType,'')
		,ISNULL(DFM.numListID,0)
		,DFFM.bitAllowSorting
		,DFFM.bitAllowFiltering
		,DFM.vcAssociatedControlType
		,DFM.vcLookBackTableName
		,0
		,DFCD.intRowNum
		,ISNULL(DFCD.intColumnWidth,0)
		,0
		,0
		,0
		,0
		,0
		,0
		,0
		,0
		,''
		,0
	FROM
		DycFormConfigurationDetails DFCD
	INNER JOIN
		DycFormField_Mapping DFFM
	ON
		DFCD.numFormId = DFFM.numFormID
		AND DFCD.numFieldId = DFFM.numFieldID
	INNER JOIN
		DycFieldMaster DFM
	ON
		DFFM.numFieldID = DFM.numFieldId
	WHERE 
		DFCD.numDomainId=@numDomainID 
		AND DFCD.numUserCntID=@numUserCntID 
		AND DFCD.numFormId=141 
		AND DFCD.numViewId=@numViewID
		AND ISNULL(DFFM.bitDeleted,0) = 0
		AND ISNULL(DFFM.bitSettingField,0) = 1
		AND ISNULL(DFCD.bitCustom,0) = 0
	UNION
	SELECT
		CONCAT(141,'~',CFM.Fld_id,'~',0)
		,CFM.Fld_id
		,CFM.Fld_label
		,CONCAT('CUST',CFM.Fld_id)
		,''
		,ISNULL(CFM.numListID,0)
		,0
		,0
		,CFM.Fld_type
		,''
		,1
		,DFCD.intRowNum
		,ISNULL(DFCD.intColumnWidth,0)
		,ISNULL(bitIsRequired,0)
		,ISNULL(bitIsEmail,0)
		,ISNULL(bitIsAlphaNumeric,0)
		,ISNULL(bitIsNumeric,0)
		,ISNULL(bitIsLengthValidation,0)
		,ISNULL(intMaxLength,0)
		,ISNULL(intMinLength,0)
		,ISNULL(bitFieldMessage,0)
		,ISNULL(vcFieldMessage,'')
		,CFM.Grp_id
	FROM
		DycFormConfigurationDetails DFCD
	INNER JOIN
		CFW_Fld_Master CFM
	ON
		DFCD.numFieldId = CFM.Fld_id
	LEFT JOIN
		CFW_Validation CV
	ON
		CFM.Fld_id = CV.numFieldID
	WHERE
		DFCD.numDomainId=@numDomainID 
		AND DFCD.numUserCntID=@numUserCntID 
		AND DFCD.numFormId=141 
		AND DFCD.numViewId=@numViewID
		AND ISNULL(DFCD.bitCustom,0) = 1

	PRINT CONCAT('2-',CONVERT( VARCHAR(24), GETDATE(), 121))

	IF (SELECT COUNT(*) FROM @TempFieldsLeft) = 0
	BEGIN
		INSERT INTO @TempFieldsLeft
		(
			ID
			,numFieldID
			,vcFieldName
			,vcOrigDbColumnName
			,vcListItemType
			,numListID
			,bitAllowSorting
			,bitAllowFiltering
			,vcAssociatedControlType
			,vcLookBackTableName
			,bitCustomField
			,intRowNum
			,intColumnWidth
		)
		SELECT
			CONCAT(141,'~',DFM.numFieldID,'~',0)
			,DFM.numFieldID
			,DFFM.vcFieldName
			,DFM.vcOrigDbColumnName
			,ISNULL(DFM.vcListItemType,'')
			,ISNULL(DFM.numListID,0)
			,DFFM.bitAllowSorting
			,DFFM.bitAllowFiltering
			,DFM.vcAssociatedControlType
			,DFM.vcLookBackTableName
			,0
			,DFFM.[order]
			,100
		FROM
			DycFormField_Mapping DFFM
		INNER JOIN
			DycFieldMaster DFM
		ON
			DFFM.numFieldID = DFM.numFieldId
		WHERE 
			DFFM.numFormId=141 
			AND ISNULL(DFFM.bitDeleted,0) = 0
			AND ISNULL(DFFM.bitSettingField,0) = 1
			AND ISNULL(DFFM.bitDefault,0) = 1
	END

	DECLARE @j INT = 0
	DECLARE @jCount INT
	DECLARE @numFieldId NUMERIC(18,0)
	DECLARE @vcFieldName AS VARCHAR(50)                                                                                                
	DECLARE @vcAssociatedControlType VARCHAR(10)  
	DECLARE @vcDbColumnName VARCHAR(100)
	DECLARE @Grp_id INT                                          
	--DECLARE @numListID AS NUMERIC(9)       
	DECLARE @bitCustomField BIT       
	SET @jCount = (SELECT COUNT(*) FROM @TempFieldsLeft)
	DECLARE @vcCustomFields VARCHAR(MAX) = ''
	DECLARE @vcCustomWhere VARCHAR(MAX) = ''

	WHILE @j <= @jCount
	BEGIN
		SELECT
			@numFieldId=numFieldID,
			@vcFieldName = vcFieldName,
			@vcAssociatedControlType = vcAssociatedControlType,
			@vcDbColumnName = CONCAT('Cust',numFieldID),
			@bitCustomField=ISNULL(bitCustomField,0),
			@Grp_id = ISNULL(Grp_id,0)
		FROM 
			@TempFieldsLeft
		WHERE 
			numPKID = @j     
		
		
			
		IF @bitCustomField = 1
		BEGIN         
			IF @Grp_id = 5
			BEGIN
				SET @vcCustomFields = CONCAT(@vcCustomFields,CONCAT(',dbo.GetCustFldValueOppItems(',@numFieldId,',T1.numOppItemID,T1.numItemCode)'),' [',@vcDbColumnName,']')
			END
			ELSE
			BEGIN
				IF @vcAssociatedControlType = 'TextBox' OR @vcAssociatedControlType = 'TextArea' 
				BEGIN
					SET @vcCustomFields = CONCAT(@vcCustomFields,',CFW',@numFieldId,'.Fld_Value  [',@vcDbColumnName,']')
             
					SET @vcCustomWhere = CONCAT(@vcCustomWhere,' left Join CFW_FLD_Values_Opp CFW',@numFieldId
									,' ON CFW' , @numFieldId , '.Fld_Id='
									,@numFieldId
									, ' and CFW' , @numFieldId
									, '.RecId=T1.numOppID')                                                         
				END   
				ELSE IF @vcAssociatedControlType = 'CheckBox' 
				BEGIN
					SET @vcCustomFields =CONCAT( @vcCustomFields
						, ',case when isnull(CFW'
						, @numFieldId
						, '.Fld_Value,0)=0 then 0 when isnull(CFW'
						, @numFieldId
						, '.Fld_Value,0)=1 then 1 end   ['
						,  @vcDbColumnName
						, ']')               
 
					SET @vcCustomWhere = CONCAT(@vcCustomWhere
						, ' left Join CFW_FLD_Values_Opp CFW'
						, @numFieldId
						, ' ON CFW',@numFieldId,'.Fld_Id='
						, @numFieldId
						, ' and CFW'
						, @numFieldId
						, '.RecId=T1.numOppID   ')                                                    
				END                
				ELSE IF @vcAssociatedControlType = 'DateField' 
				BEGIN
					SET @vcCustomFields = CONCAT(@vcCustomFields
						, ',dbo.FormatedDateFromDate(CFW'
						, @numFieldId
						, '.Fld_Value,'
						, @numDomainId
						, ')  [',@vcDbColumnName ,']' )  
					                  
					SET @vcCustomWhere = CONCAT(@vcCustomWhere
						, ' left Join CFW_FLD_Values_Opp CFW'
						, @numFieldId
						, ' on CFW', @numFieldId, '.Fld_Id='
						, @numFieldId
						, ' and CFW'
						, @numFieldId
						, '.RecId=T1.numOppID   ' )                                                        
				END                
				ELSE IF @vcAssociatedControlType = 'SelectBox' 
				BEGIN
					SET @vcCustomFields = CONCAT(@vcCustomFields,',L',@numFieldId,'.vcData',' [',@vcDbColumnName,']')
					                                                    
					SET @vcCustomWhere = CONCAT(@vcCustomWhere
						, ' left Join CFW_FLD_Values_Opp CFW'
						, @numFieldId
						, ' on CFW',@numFieldId ,'.Fld_Id='
						, @numFieldId
						, ' and CFW'
						, @numFieldId
						, '.RecId=T1.numOppID    ')     
					                                                    
					SET @vcCustomWhere = CONCAT(@vcCustomWhere
						, ' left Join ListDetails L'
						, @numFieldId
						, ' on L'
						, @numFieldId
						, '.numListItemID=CFW'
						, @numFieldId
						, '.Fld_Value' )              
				END
				ELSE
				BEGIN
					SET @vcCustomFields = CONCAT(@vcCustomFields,CONCAT(',dbo.GetCustFldValueOpp(',@numFieldId,',T1.numOppID)'),' [',@vcDbColumnName,']')
				END 
			END
			
		END

		SET @j = @j + 1
	END
	
	PRINT CONCAT('3-',CONVERT( VARCHAR(24), GETDATE(), 121))

	CREATE TABLE #TEMPMSRecords
	(
		numOppID NUMERIC(18,0)
		,numOppItemID NUMERIC(18,0)
		,numOppBizDocID NUMERIC(18,0)
	)

	DECLARE @vcSQL NVARCHAR(MAX)
	DECLARE @vcSQLFinal NVARCHAR(MAX)
	
	SET @vcSQL = CONCAT('INSERT INTO #TEMPMSRecords
						(
							numOppID
							,numOppItemID
							,numOppBizDocID
						)
						SELECT
							OpportunityMaster.numOppId
							,',(CASE WHEN @bitGroupByOrder = 1 THEN '0' ELSE 'OpportunityItems.numoppitemtCode' END),'
							,',(CASE 
									WHEN @numViewID = 4 OR @numViewID = 6 OR (@numViewID=2 AND @tintPackingViewMode=4)
									THEN 'OpportunityBizDocs.numOppBizDocsId'  
									ELSE '0' 
								END),'
						FROM
							OpportunityMaster
						INNER JOIN
							DivisionMaster
						ON
							OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
						INNER JOIN
							CompanyInfo
						ON
							DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
						INNER JOIN 
							OpportunityItems
						ON
							OpportunityMaster.numOppId = OpportunityItems.numOppId
						INNER JOIN
							Item
						ON
							OpportunityItems.numItemCode = Item.numItemCode
						LEFT JOIN
							WareHouseItems
						ON
							OpportunityItems.numWarehouseItmsID = WareHouseItems.numWareHouseItemID
						',
						(CASE 
							WHEN ISNULL(@numBatchID,0) > 0 
							THEN 
								' INNER JOIN 
										MassSalesFulfillmentBatchOrders
								ON
									OpportunityMaster.numOppId = MassSalesFulfillmentBatchOrders.numOppID
									AND (OpportunityItems.numOppItemtCode = MassSalesFulfillmentBatchOrders.numOppItemID OR ISNULL(MassSalesFulfillmentBatchOrders.numOppItemID,0) = 0)'
							ELSE 
								'' 
						END)	
						,
						(CASE 
							WHEN @numViewID = 4 
							THEN
								'INNER JOIN 
									OpportunityBizDocs
								ON
									OpportunityMaster.numOppId = OpportunityBizDocs.numOppId
									AND OpportunityBizDocs.numBizDocID=287'
							WHEN @numViewID = 6
							THEN
								CONCAT('INNER JOIN 
											OpportunityBizDocs
										ON
											OpportunityMaster.numOppId = OpportunityBizDocs.numOppId
											AND OpportunityBizDocs.numBizDocID=',(CASE @tintPrintBizDocViewMode WHEN 2 THEN 29397 WHEN 3 THEN 296 WHEN 4 THEN 287 ELSE 55206 END))
							WHEN @numViewID = 2 AND @tintPackingViewMode = 4
							THEN 
								'INNER JOIN 
									OpportunityBizDocs
								ON
									OpportunityMaster.numOppId = OpportunityBizDocs.numOppId
									AND OpportunityBizDocs.numBizDocID=@numDefaultSalesShippingDoc'
							ELSE ''

						END)
						,'
						WHERE
							OpportunityMaster.numDomainId=@numDomainID
							AND OpportunityMaster.tintOppType = 1
							AND OpportunityMaster.tintOppStatus = 1
							AND ISNULL(OpportunityMaster.tintshipped,0) = 0
							AND 1 = (CASE WHEN LEN(ISNULL(@vcOrderStauts,'''')) > 0 THEN (CASE WHEN OpportunityMaster.numStatus ',(CASE WHEN ISNULL(@bitIncludeSearch,0) = 0 THEN 'NOT IN' ELSE 'IN' END),' (SELECT Id FROM dbo.SplitIDs(@vcOrderStauts,'','')) THEN 1 ELSE 0 END) ELSE 1 END)
							AND 1 = (CASE 
										WHEN LEN(ISNULL(@vcFilterValue,'''')) > 0
										THEN
											CASE 
												WHEN @tintFilterBy=1 --Item Classification
												THEN (CASE WHEN Item.numItemClassification ',(CASE WHEN ISNULL(@bitIncludeSearch,0) = 0 THEN 'NOT IN' ELSE 'IN' END),' (SELECT Id FROM dbo.SplitIDs(@vcFilterValue,'','')) THEN 1 ELSE 0 END)
												ELSE 1
											END
										ELSE 1
									END)
							AND 1 = (CASE
										WHEN @numViewID = 1 THEN (CASE WHEN ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyPicked,0) > 0 THEN 1 ELSE 0 END)
										WHEN @numViewID = 2 THEN (CASE 
																		WHEN @tintPackingViewMode = 1 
																		THEN (CASE 
																				WHEN ISNULL((SELECT COUNT(*) FROM OpportunityItems OIInner WHERE OIInner.numOppID=OpportunityMaster.numOppId AND OIInner.numItemCode=ISNULL(@numShippingServiceItemID,0)),0) = 0 
																				THEN 1 
																				ELSE 0 
																			END)
																		WHEN @tintPackingViewMode = 2
																		THEN (CASE 
																				WHEN ',(CASE WHEN ISNULL(@tintPackingMode,1) = 2 THEN 'ISNULL(OpportunityItems.numQtyPicked,0)' ELSE 'ISNULL(OpportunityItems.numUnitHour,0)' END),' - ISNULL(OpportunityItems.numQtyShipped,0) > 0
																				THEN 1 
																				ELSE 0 
																			END)
																		WHEN @tintPackingViewMode = 3
																		THEN (CASE 
																				WHEN ',(CASE WHEN ISNULL(@tintPackingMode,1) = 2 THEN 'ISNULL(OpportunityItems.numQtyPicked,0)' ELSE 'ISNULL(OpportunityItems.numUnitHour,0)' END),' - ISNULL((SELECT 
																																																													SUM(OpportunityBizDocItems.numUnitHour)
																																																												FROM
																																																													OpportunityBizDocs
																																																												INNER JOIN
																																																													OpportunityBizDocItems
																																																												ON
																																																													OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																																																												WHERE
																																																													OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
																																																													AND OpportunityBizDocs.numBizDocId=@numDefaultSalesShippingDoc
																																																													AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode),0) > 0
																				THEN 1 
																				ELSE 0 
																			END)',
																			(CASE 
																				WHEN @numViewID = 2 AND @tintPackingViewMode = 4
																				THEN 'WHEN @tintPackingViewMode = 4
																						THEN (CASE 
																								WHEN ISNULL((SELECT COUNT(*) FROM ShippingBox WHERE ISNULL(vcShippingLabelImage,'''') <> '''' AND ISNULL(vcTrackingNumber,'''') <> '''' AND numShippingReportId IN (SELECT numShippingReportId FROM ShippingReport WHERE ShippingReport.numOppID=OpportunityMaster.numOppId AND ShippingReport.numOppBizDocId=OpportunityBizDocs.numOppBizDocsId)),0) = 0 
																								THEN 1 
																								ELSE 0 
																							END)'
																				ELSE ''
																			END)
																			,'
																		ELSE 0
																	END)
										WHEN @numViewID = 3
										THEN (CASE 
												WHEN (CASE 
														WHEN ISNULL(@tintInvoicingType,0)=1 
														THEN ISNULL(OpportunityItems.numQtyShipped,0) 
														ELSE ISNULL(OpportunityItems.numUnitHour,0) 
													END) - ISNULL((SELECT 
																		SUM(OpportunityBizDocItems.numUnitHour)
																	FROM
																		OpportunityBizDocs
																	INNER JOIN
																		OpportunityBizDocItems
																	ON
																		OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																	WHERE
																		OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
																		AND OpportunityBizDocs.numBizDocId=287
																		AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode),0) > 0 
												THEN 1
												ELSE 0 
											END)
										WHEN @numViewID = 4 THEN 1
										WHEN @numViewID = 5 THEN 1
										WHEN @numViewID = 6 THEN 1
									END)
							AND 1 = (CASE
										WHEN @numViewID = 1 THEN (CASE WHEN ISNULL(WareHouseItems.numWareHouseItemID,0) > 0 AND ISNULL(OpportunityItems.bitDropship,0) = 0 THEN 1 ELSE 0 END)
										WHEN @numViewID = 2 AND @tintPackingViewMode <> 2 THEN (CASE WHEN ISNULL(WareHouseItems.numWareHouseItemID,0) > 0 AND ISNULL(OpportunityItems.bitDropship,0) = 0 THEN 1 ELSE 0 END)
										WHEN @numViewID = 2 AND @tintPackingViewMode = 2 THEN 1
										WHEN @numViewID = 3 THEN 1
										WHEN @numViewID = 4 THEN 1
										WHEN @numViewID = 5 THEN 1
										WHEN @numViewID = 6 THEN 1
										ELSE 0
									END)',
							(CASE 
								WHEN @numViewID = 5 AND ISNULL(@tintPendingCloseFilter,0) = 2
								THEN ' AND (SELECT 
												COUNT(*) 
											FROM 
											(
												SELECT
													OI.numoppitemtCode,
													ISNULL(OI.numUnitHour,0) AS OrderedQty,
													ISNULL(TempFulFilled.FulFilledQty,0) AS FulFilledQty
												FROM
													OpportunityItems OI
												INNER JOIN
													Item I
												ON
													OI.numItemCode = I.numItemCode
												OUTER APPLY
												(
													SELECT
														SUM(OpportunityBizDocItems.numUnitHour) AS FulFilledQty
													FROM
														OpportunityBizDocs
													INNER JOIN
														OpportunityBizDocItems 
													ON
														OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
													WHERE
														OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
														AND ISNULL(OpportunityBizDocs.numBizDocId,0) = 296
														AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
												) AS TempFulFilled
												WHERE
													OI.numOppID = OpportunityMaster.numOppID
													AND ISNULL(OI.numUnitHour,0) > 0
													AND ISNULL(OI.bitDropShip,0) = 0
											) X
											WHERE
												X.OrderedQty <> X.FulFilledQty) = 0
										AND (SELECT 
												COUNT(*) 
											FROM 
											(
												SELECT
													OI.numoppitemtCode,
													ISNULL(OI.numUnitHour,0) AS OrderedQty,
													ISNULL(TempInvoice.InvoicedQty,0) AS InvoicedQty
												FROM
													OpportunityItems OI
												INNER JOIN
													Item I
												ON
													OI.numItemCode = I.numItemCode
												OUTER APPLY
												(
													SELECT
														SUM(OpportunityBizDocItems.numUnitHour) AS InvoicedQty
													FROM
														OpportunityBizDocs
													INNER JOIN
														OpportunityBizDocItems 
													ON
														OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
													WHERE
														OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
														AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
														AND ISNULL(OpportunityBizDocs.bitAuthoritativeBizDocs,0) = 1
												) AS TempInvoice
												WHERE
													OI.numOppID = OpportunityMaster.numOppID
													AND ISNULL(OI.numUnitHour,0) > 0
											) X
											WHERE
												X.OrderedQty > X.InvoicedQty) = 0'
								ELSE '' 
							END)
							,(CASE WHEN @numViewID = 4 THEN 'AND 1 = (CASE WHEN ISNULL(OpportunityBizDocs.monDealAmount,0) - ISNULL(OpportunityBizDocs.monAmountPaid,0) > 0 THEN 1 ELSE 0 END)' ELSE '' END)
							,' AND 1 = (CASE WHEN @numViewID = 2 AND @tintPackingViewMode = 3 THEN (CASE WHEN ISNULL(Item.bitContainer,0)=1 THEN 0 ELSE 1 END) ELSE 1 END)
								AND 1 = (CASE WHEN ISNULL(@numWarehouseID,0) > 0 AND ISNULL(OpportunityItems.numWarehouseItmsID,0) > 0 THEN (CASE WHEN @numWarehouseID=WareHouseItems.numWarehouseID THEN 1 ELSE 0 END) ELSE 1 END)'
								,(CASE WHEN ISNULL(@numShippingZone,0) > 0 THEN ' AND 1 = (CASE 
																				WHEN ISNULL((SELECT TOP 1 numState FROM fn_getOPPAddressDetails(OpportunityMaster.numOppId,OpportunityMaster.numDomainID,2)),0) > 0 
																				THEN
																					CASE 
																						WHEN (SELECT COUNT(*) FROM dbo.State WHERE numDomainID=@numDomainId AND numStateID =  (SELECT TOP 1 numState FROM fn_getOPPAddressDetails(OpportunityMaster.numOppId,OpportunityMaster.numDomainID,2)) AND numShippingZone=@numShippingZone) > 0 
																						THEN 1
																						ELSE 0
																					END
																				ELSE 0 
																			END)' ELSE '' END),
								@vcCustomSearchValue,(CASE WHEN @bitGroupByOrder = 1 THEN CONCAT('GROUP BY OpportunityMaster.numOppId',(CASE WHEN @numViewID = 4 OR (@numViewID=2 AND @tintPackingViewMode=4) THEN ',OpportunityBizDocs.numOppBizDocsId,OpportunityBizDocs.monAmountPaid' WHEN @numViewID = 6 THEN ',OpportunityBizDocs.numOppBizDocsId'  ELSE '' END)) ELSE '' END));

	EXEC sp_executesql @vcSQL, N'@numDomainID NUMERIC(18,0), @vcOrderStauts VARCHAR(MAX), @vcFilterValue VARCHAR(MAX), @tintFilterBy TINYINT, @numViewID NUMERIC(18,0), @tintPackingViewMode TINYINT, @numShippingServiceItemID NUMERIC(18,0), @numWarehouseID NUMERIC(18,0), @numShippingZone NUMERIC(18,0), @numDefaultSalesShippingDoc NUMERIC(18,0), @tintInvoicingType TINYINT', @numDomainID, @vcOrderStauts, @vcFilterValue, @tintFilterBy, @numViewID, @tintPackingViewMode, @numShippingServiceItemID, @numWarehouseID, @numShippingZone, @numDefaultSalesShippingDoc, @tintInvoicingType;

	PRINT CONCAT('4-',CONVERT( VARCHAR(24), GETDATE(), 121))

	SET @numTotalRecords = (SELECT COUNT(*) FROM #TEMPMSRecords)
	
	PRINT CONCAT('5-',CONVERT( VARCHAR(24), GETDATE(), 121))

	IF @bitGroupByOrder = 1
	BEGIN
		
		SET @vcSQLFinal = CONCAT('

		INSERT INTO #TEMPResult
		(
			numOppID
			,numDivisionID
			,numTerID
			,numOppItemID
			,numOppBizDocID
			,bintCreatedDate
			,vcCompanyName
			,vcPoppName
			,numAssignedTo
			,numRecOwner
			,vcBizDocID
			,numStatus
			,txtComments
			,monAmountPaid
			,monAmountToPay
			,intUsedShippingCompany
			,intShippingCompany
			,numPreferredShipVia
			,numPreferredShipService
			,bitPaidInFull
			,numAge
			,dtAnticipatedDelivery
			,vcShipStatus
			,dtExpectedDate
			,dtExpectedDateOrig
			,vcPaymentStatus
			,numShipRate
		)
		SELECT
			OpportunityMaster.numOppId
			,OpportunityMaster.numDivisionID
			,ISNULL(DivisionMaster.numTerID,0) numTerID
			,0
			,TEMPOrder.numOppBizDocID
			,dbo.FormatedDateFromDate(DATEADD(MINUTE,@ClientTimeZoneOffset * -1,OpportunityMaster.bintCreatedDate),@numDomainID)
			,ISNULL(CompanyInfo.vcCompanyName,'''')
			,ISNULL(OpportunityMaster.vcPoppName,'''')
			,dbo.fn_GetContactName(OpportunityMaster.numAssignedTo)
			,dbo.fn_GetContactName(OpportunityMaster.numRecOwner)
			,(CASE WHEN @numViewID = 4 OR @numViewID = 6 THEN ISNULL(OpportunityBizDocs.vcBizDocID,''-'') ELSE '''' END)
			,dbo.GetListIemName(OpportunityMaster.numStatus)
			,ISNULL(OpportunityMaster.txtComments,'''')
			,ISNULL(TempPaid.monAmountPaid,0)
			,ISNULL(OpportunityBizDocs.monDealAmount,0) - ISNULL(OpportunityBizDocs.monAmountPaid,0)
			,CONCAT(ISNULL(LDOpp.vcData,''-''),'', '',ISNULL(SSOpp.vcShipmentService,''-'')) 
			,CONCAT(ISNULL(LDDiv.vcData,''-''),'', '',ISNULL(SSDiv.vcShipmentService,''-''))
			,ISNULL(DivisionMaster.intShippingCompany,0)
			,ISNULL(DivisionMaster.numDefaultShippingServiceID,0)
			,(CASE WHEN ISNULL(TempPaid.monAmountPaid,0) >= ISNULL(OpportunityMaster.monDealAmount,0) THEN 1 ELSE 0 END)
			,CONCAT(''<span style="color:'',(CASE
						WHEN (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24) = 0 AND (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) < 4  
						THEN ''#00ff00''
						WHEN (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24) = 0 AND ((DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) >= 4 AND (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) < 8)
						THEN ''#00b050''
						WHEN (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24) = 0 AND ((DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) >= 8 AND (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) < 12)
						THEN ''#3399ff''
						WHEN (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24) = 0 AND ((DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) >= 12 AND (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) < 24)
						THEN ''#ff9900''
						WHEN (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24) BETWEEN 1 AND 2  
						THEN ''#9b3596''
						ELSE ''#ff0000''
					END),''">'',CONCAT(DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24,''d '',DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24,''h''),''</span>'')
			,'''' dtAnticipatedDelivery
			,'''' vcShipStatus
			,dbo.FormatedDateFromDate(OpportunityMaster.dtExpectedDate,@numDomainID)
			,OpportunityMaster.dtExpectedDate
			,'''' vcPaymentStatus
			,ISNULL(CAST((SELECT SUM(monTotAmount) FROM OpportunityItems OIInner WHERE OIInner.numOppID=OpportunityMaster.numOppId AND OIInner.numItemCode=@numShippingServiceItemID) AS VARCHAR),'''')
		FROM
			#TEMPMSRecords TEMPOrder
		INNER JOIN
			OpportunityMaster
		ON
			TEMPOrder.numOppID = OpportunityMaster.numOppId
		INNER JOIN
			DivisionMaster
		ON
			OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
		INNER JOIN
			CompanyInfo
		ON
			DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
		LEFT JOIN
			OpportunityBizDocs
		ON
			OpportunityMaster.numOppId = OpportunityBizDocs.numOppId
			AND OpportunityBizDocs.numOppBizDocsId = ISNULL(TEMPOrder.numOppBizDocID,0)
		LEFT JOIN
			ListDetails  LDOpp
		ON
			LDOpp.numListID = 82
			AND (LDOpp.numDomainID = @numDomainID OR LDOpp.constFlag=1)
			AND ISNULL(OpportunityMaster.intUsedShippingCompany,0) = LDOpp.numListItemID
		LEFT JOIN
			ShippingService AS SSOpp
		ON
			(SSOpp.numDomainID=@numDomainID OR ISNULL(SSOpp.numDomainID,0)=0) 
			AND OpportunityMaster.numShippingService = SSOpp.numShippingServiceID
		LEFT JOIN
			ListDetails  LDDiv
		ON
			LDDiv.numListID = 82
			AND (LDDiv.numDomainID = @numDomainID OR LDDiv.constFlag=1)
			AND ISNULL(DivisionMaster.intShippingCompany,0) = LDDiv.numListItemID
		LEFT JOIN
			ShippingService AS SSDiv
		ON
			(SSDiv.numDomainID=@numDomainID OR ISNULL(SSDiv.numDomainID,0)=0) 
			AND ISNULL(DivisionMaster.numDefaultShippingServiceID,0) = SSDiv.numShippingServiceID
		OUTER APPLY
		(
			SELECT 
				SUM(OpportunityBizDocs.monAmountPaid) monAmountPaid
			FROM 
				OpportunityBizDocs 
			WHERE 
				OpportunityBizDocs.numOppId=OpportunityMaster.numOppId 
				AND OpportunityBizDocs.numBizDocId=287
		) TempPaid
		ORDER BY ',
		(CASE @vcSortOrder
			WHEN 'CompanyInfo.vcCompanyName' THEN 'CompanyInfo.vcCompanyName '
			WHEN 'OpportunityMaster.numStatus' THEN 'dbo.GetListIemName(OpportunityMaster.numStatus)'
			WHEN 'OpportunityMaster.vcPoppName' THEN 'OpportunityMaster.vcPoppName'
			WHEN 'OpportunityMaster.bintCreatedDate' THEN 'OpportunityMaster.bintCreatedDate'
			WHEN 'OpportunityMaster.numAssignedTo' THEN 'dbo.fn_GetContactName(OpportunityMaster.numAssignedTo)'
			WHEN 'OpportunityMaster.numRecOwner' THEN 'dbo.fn_GetContactName(OpportunityMaster.numRecOwner)'
			WHEN 'OpportunityMaster.numAge' THEN 'OpportunityMaster.bintCreatedDate'
			WHEN 'OpportunityBizDocs.monAmountPaid' THEN 'TempPaid.monAmountPaid'
			WHEN 'OpportunityMaster.bitPaidInFull' THEN '(CASE WHEN ISNULL(TempPaid.monAmountPaid,0) >= ISNULL(OpportunityMaster.monDealAmount,0) THEN 1 ELSE 0 END)'
			ELSE 'OpportunityMaster.bintCreatedDate'
		END),' ',(CASE WHEN ISNULL(@vcSortOrder,'') <> '' THEN @vcSortOrder ELSE 'DESC' END),' OFFSET (@numPageIndex-1) * 200 ROWS FETCH NEXT 200 ROWS ONLY')

		EXEC sp_executesql @vcSQLFinal, N'@numDomainID NUMERIC(18,0), @vcOrderStauts VARCHAR(MAX), @vcFilterValue VARCHAR(MAX), @tintFilterBy TINYINT, @numViewID NUMERIC(18,0), @tintPackingViewMode TINYINT, @numShippingServiceItemID NUMERIC(18,0), @numWarehouseID NUMERIC(18,0), @numShippingZone NUMERIC(18,0), @numDefaultSalesShippingDoc NUMERIC(18,0), @tintInvoicingType TINYINT, @ClientTimeZoneOffset INT, @tintPackingMode TINYINT, @numPageIndex INT', @numDomainID, @vcOrderStauts, @vcFilterValue, @tintFilterBy, @numViewID, @tintPackingViewMode, @numShippingServiceItemID, @numWarehouseID, @numShippingZone, @numDefaultSalesShippingDoc, @tintInvoicingType, @ClientTimeZoneOffset, @tintPackingMode, @numPageIndex;
	END
	ELSE
	BEGIN
		SET @vcSQLFinal = CONCAT('

		INSERT INTO #TEMPResult
		(
			numOppID
			,numDivisionID
			,numTerID
			,numOppItemID
			,numOppBizDocID
			,bintCreatedDate
			,numItemCode
			,vcCompanyName
			,vcPoppName
			,numAssignedTo
			,numRecOwner
			,vcBizDocID
			,numStatus
			,txtComments
			,numBarCodeId
			,vcLocation
			,vcItemName
			,vcItemDesc
			,numQtyShipped
			,numUnitHour
			,monAmountPaid
			,numItemClassification
			,vcSKU
			,charItemType
			,vcAttributes
			,vcNotes
			,vcItemReleaseDate
			,dtItemReceivedDate
			,intUsedShippingCompany
			,intShippingCompany
			,numPreferredShipVia
			,numPreferredShipService
			,vcInvoiced
			,vcInclusionDetails
			,bitPaidInFull
			,numAge
			,dtAnticipatedDelivery
			,vcShipStatus
			,numRemainingQty
			,dtExpectedDate
			,dtExpectedDateOrig
			,numQtyPicked
			,numQtyPacked
			,vcPaymentStatus
			,numShipRate
		)
		SELECT
			OpportunityMaster.numOppId
			,OpportunityMaster.numDivisionID
			,ISNULL(DivisionMaster.numTerID,0) numTerID
			,OpportunityItems.numoppitemtCode
			,0
			,dbo.FormatedDateFromDate(DATEADD(MINUTE,@ClientTimeZoneOffset * -1,OpportunityMaster.bintCreatedDate),@numDomainID)
			,Item.numItemCode
			,ISNULL(CompanyInfo.vcCompanyName,'''')
			,ISNULL(OpportunityMaster.vcPoppName,'''')
			,dbo.fn_GetContactName(OpportunityMaster.numAssignedTo)
			,dbo.fn_GetContactName(OpportunityMaster.numRecOwner)
			,''''
			,dbo.GetListIemName(OpportunityMaster.numStatus)
			,ISNULL(OpportunityMaster.txtComments,'''')
			,ISNULL(Item.numBarCodeId,'''')
			,ISNULL(WarehouseLocation.vcLocation,'''')
			,ISNULL(Item.vcItemName,'''')
			,ISNULL(OpportunityItems.vcItemDesc,'''')
			,ISNULL(OpportunityItems.numQtyShipped,0)
			,ISNULL(OpportunityItems.numUnitHour,0)
			,ISNULL(TempPaid.monAmountPaid,0)
			,dbo.GetListIemName(Item.numItemClassification)
			,ISNULL(Item.vcSKU,'''')
			,(CASE 
				WHEN Item.charItemType=''P''  THEN ''Inventory Item''
				WHEN Item.charItemType=''N'' THEN ''Non Inventory Item'' 
				WHEN Item.charItemType=''S'' THEN ''Service'' 
				WHEN Item.charItemType=''A'' THEN ''Accessory'' 
			END)
			,ISNULL(OpportunityItems.vcAttributes,'''')
			,ISNULL(OpportunityItems.vcNotes,'''')
			,dbo.FormatedDateFromDate(OpportunityItems.ItemReleaseDate,@numDomainID)
			,dbo.FormatedDateFromDate((SELECT TOP 1 OMInner.dtItemReceivedDate FROM OpportunityMaster OMInner INNER JOIN OpportunityItems OIInner ON OMInner.numOppID=OIInner.numOppID WHERE OMInner.tintOppType=2 AND OMInner.tintOppStatus=1 AND OIInner.numItemCode=OpportunityItems.numItemCode ORDER BY OMInner.dtItemReceivedDate DESC),@numDomainID)
			,CONCAT(ISNULL(LDOpp.vcData,''-''),'', '',ISNULL(SSOpp.vcShipmentService,''-'')) 
			,CONCAT(ISNULL(LDDiv.vcData,''-''),'', '',ISNULL(SSDiv.vcShipmentService,''-''))
			,ISNULL(DivisionMaster.intShippingCompany,0)
			,ISNULL(DivisionMaster.numDefaultShippingServiceID,0)
			,ISNULL((SELECT SUM(numUnitHour) FROM OpportunityBizDocs OBD INNER JOIN OpportunityBizDocItems OBDI ON OBD.numOppBizDocsId=OBDI.numOppBizDocID  WHERE OBD.numOppId=OpportunityMaster.numOppId AND OBD.numBizDocId=287 AND OBDI.numOppItemID=OpportunityItems.numoppitemtCode),0)
			,ISNULL(OpportunityItems.vcInclusionDetail,'''')
			,(CASE WHEN ISNULL(TempPaid.monAmountPaid,0) >= ISNULL(OpportunityMaster.monDealAmount,0) THEN 1 ELSE 0 END)
			,CONCAT(''<span style="color:'',(CASE
						WHEN (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24) = 0 AND (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) < 4  
						THEN ''#00ff00''
						WHEN (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24) = 0 AND ((DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) >= 4 AND (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) < 8)
						THEN ''#00b050''
						WHEN (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24) = 0 AND ((DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) >= 8 AND (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) < 12)
						THEN ''#3399ff''
						WHEN (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24) = 0 AND ((DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) >= 12 AND (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) < 24)
						THEN ''#ff9900''
						WHEN (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24) BETWEEN 1 AND 2  
						THEN ''#9b3596''
						ELSE ''#ff0000''
					END),''">'',CONCAT(DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24,''d '',DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24,''h''),''</span>'')
			,'''' dtAnticipatedDelivery
			,'''' vcShipStatus
			,(CASE 
				WHEN @numViewID = 1
				THEN ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyPicked,0)
				WHEN @numViewID = 2 AND @tintPackingViewMode = 2
				THEN (CASE WHEN ISNULL(@tintPackingMode,1) = 2 THEN ISNULL(OpportunityItems.numQtyPicked,0) - ISNULL(OpportunityItems.numQtyShipped,0) ELSE ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyShipped,0) END)
				WHEN @numViewID = 2 AND @tintPackingViewMode = 3
				THEN (CASE WHEN ISNULL(@tintPackingMode,1) = 2 THEN ISNULL(OpportunityItems.numQtyPicked,0) ELSE ISNULL(OpportunityItems.numUnitHour,0) END) - ISNULL((SELECT 
																																											SUM(OpportunityBizDocItems.numUnitHour)
																																										FROM
																																											OpportunityBizDocs
																																										INNER JOIN
																																											OpportunityBizDocItems
																																										ON
																																											OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																																										WHERE
																																											OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
																																											AND OpportunityBizDocs.numBizDocId=@numDefaultSalesShippingDoc
					 																																					AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode),0)
				WHEN @numViewID = 3
				THEN (CASE 
						WHEN ISNULL(@tintInvoicingType,0)=1 
						THEN ISNULL(OpportunityItems.numQtyShipped,0) 
						ELSE ISNULL(OpportunityItems.numUnitHour,0) 
					END) - ISNULL((SELECT 
										SUM(OpportunityBizDocItems.numUnitHour)
									FROM
										OpportunityBizDocs
									INNER JOIN
										OpportunityBizDocItems
									ON
										OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
									WHERE
										OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
										AND OpportunityBizDocs.numBizDocId=287
										AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode),0)
				ELSE 0
			END) numRemainingQty
			,dbo.FormatedDateFromDate(OpportunityMaster.dtExpectedDate,@numDomainID)
			,OpportunityMaster.dtExpectedDate
			,ISNULL(OpportunityItems.numQtyPicked,0)
			,ISNULL(TempInvoiced.numInvoicedQty,0)
			,'''' vcPaymentStatus
			,ISNULL(CAST((SELECT SUM(monTotAmount) FROM OpportunityItems OIInner WHERE OIInner.numOppID=OpportunityMaster.numOppId AND OIInner.numItemCode=@numShippingServiceItemID) AS VARCHAR),'''')
		FROM
			#TEMPMSRecords TEMPOrder
		INNER JOIN
			OpportunityMaster
		ON
			TEMPOrder.numOppID = OpportunityMaster.numOppId
		INNER JOIN
			DivisionMaster
		ON
			OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
		INNER JOIN
			CompanyInfo
		ON
			DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
		INNER JOIN 
			OpportunityItems
		ON
			OpportunityMaster.numOppId = OpportunityItems.numOppId
			AND TEMPOrder.numOppItemID = OpportunityItems.numoppitemtCode
		INNER JOIN
			Item
		ON
			OpportunityItems.numItemCode = Item.numItemCode
		LEFT JOIN
			ListDetails  LDOpp
		ON
			LDOpp.numListID = 82
			AND (LDOpp.numDomainID = @numDomainID OR LDOpp.constFlag=1)
			AND ISNULL(OpportunityMaster.intUsedShippingCompany,0) = LDOpp.numListItemID
		LEFT JOIN
			ShippingService AS SSOpp
		ON
			(SSOpp.numDomainID=@numDomainID OR ISNULL(SSOpp.numDomainID,0)=0) 
			AND OpportunityMaster.numShippingService = SSOpp.numShippingServiceID
		LEFT JOIN
			ListDetails  LDDiv
		ON
			LDDiv.numListID = 82
			AND (LDDiv.numDomainID = @numDomainID OR LDDiv.constFlag=1)
			AND ISNULL(DivisionMaster.intShippingCompany,0) = LDDiv.numListItemID
		LEFT JOIN
			ShippingService AS SSDiv
		ON
			(SSDiv.numDomainID=@numDomainID OR ISNULL(SSDiv.numDomainID,0)=0) 
			AND ISNULL(DivisionMaster.numDefaultShippingServiceID,0) = SSDiv.numShippingServiceID
		LEFT JOIN
			WareHouseItems
		ON
			OpportunityItems.numWarehouseItmsID = WareHouseItems.numWareHouseItemID
		LEFT JOIN
			WarehouseLocation
		ON
			WareHouseItems.numWLocationID = WarehouseLocation.numWLocationID
		OUTER APPLY
		(
			SELECT 
				SUM(OpportunityBizDocItems.numUnitHour) numInvoicedQty
			FROM 
				OpportunityBizDocs
			INNER JOIN
				OpportunityBizDocItems
			ON
				OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
			WHERE 
				OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
				AND OpportunityBizDocs.numBizDocId=@numDefaultSalesShippingDoc
				AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode
		) TempInvoiced
		OUTER APPLY
		(
			SELECT 
				SUM(OpportunityBizDocs.monAmountPaid) monAmountPaid
			FROM 
				OpportunityBizDocs 
			WHERE 
				OpportunityBizDocs.numOppId=OpportunityMaster.numOppId 
				AND OpportunityBizDocs.numBizDocId=287
		) TempPaid
		ORDER BY ',
		(CASE @vcSortColumn
			WHEN 'CompanyInfo.vcCompanyName' THEN 'CompanyInfo.vcCompanyName' 
			WHEN 'Item.charItemType' THEN '(CASE 
												WHEN Item.charItemType=''P''  THEN ''Inventory Item''
												WHEN Item.charItemType=''N'' THEN ''Non Inventory Item'' 
												WHEN Item.charItemType=''S'' THEN ''Service''
												WHEN Item.charItemType=''A'' THEN ''Accessory''
											END)'
			WHEN 'Item.numBarCodeId' THEN 'Item.numBarCodeId'
			WHEN 'Item.numItemClassification' THEN 'dbo.GetListIemName(Item.numItemClassification)'
			WHEN 'Item.vcSKU' THEN 'Item.vcSKU'
			WHEN 'OpportunityItems.vcItemName' THEN 'Item.vcItemName'
			WHEN 'OpportunityMaster.numStatus' THEN 'dbo.GetListIemName(OpportunityMaster.numStatus)'
			WHEN 'OpportunityMaster.vcPoppName' THEN 'OpportunityMaster.vcPoppName'
			WHEN 'OpportunityMaster.numAssignedTo' THEN 'dbo.fn_GetContactName(OpportunityMaster.numAssignedTo)'
			WHEN 'OpportunityMaster.numRecOwner' THEN 'dbo.fn_GetContactName(OpportunityMaster.numRecOwner)'
			WHEN 'WareHouseItems.vcLocation' THEN 'WarehouseLocation.vcLocation'
			WHEN 'OpportunityBizDocs.monAmountPaid' THEN 'TempPaid.monAmountPaid'
			WHEN 'OpportunityItems.numQtyPacked' THEN 'ISNULL((SELECT SUM(numUnitHour) FROM OpportunityBizDocs OBD INNER JOIN OpportunityBizDocItems OBDI ON OBD.numOppBizDocsId=OBDI.numOppBizDocID  WHERE OBD.numOppId=OpportunityMaster.numOppId AND OBD.numBizDocId=29397 AND OBDI.numOppItemID=OpportunityItems.numoppitemtCode),0)'
			WHEN 'OpportunityItems.numQtyPicked' THEN 'OpportunityItems.numQtyPicked'
			WHEN 'OpportunityItems.numQtyShipped' THEN 'OpportunityItems.numQtyShipped'
			WHEN 'OpportunityItems.numRemainingQty' THEN '(CASE 
																WHEN @numViewID = 1
																THEN ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyPicked,0)
																WHEN @numViewID = 2
																THEN ISNULL(OpportunityItems.numUnitHour,0) - ISNULL((SELECT SUM(numUnitHour) FROM OpportunityBizDocs OBD INNER JOIN OpportunityBizDocItems OBDI ON OBD.numOppBizDocsId=OBDI.numOppBizDocID  WHERE OBD.numOppId=OpportunityMaster.numOppId AND OBD.numBizDocId=29397 AND OBDI.numOppItemID=OpportunityItems.numoppitemtCode),0)
																ELSE 0
															END)'
			WHEN 'OpportunityItems.numUnitHour' THEN 'OpportunityItems.numUnitHour'
			WHEN 'OpportunityItems.vcInvoiced' THEN 'ISNULL((SELECT SUM(numUnitHour) FROM OpportunityBizDocs OBD INNER JOIN OpportunityBizDocItems OBDI ON OBD.numOppBizDocsId=OBDI.numOppBizDocID  WHERE OBD.numOppId=OpportunityMaster.numOppId AND OBD.numBizDocId=287 AND OBDI.numOppItemID=OpportunityItems.numoppitemtCode),0)'
			WHEN 'OpportunityMaster.dtExpectedDate' THEN 'OpportunityMaster.dtExpectedDate'
			WHEN 'OpportunityItems.vcItemReleaseDate' THEN 'OpportunityItems.ItemReleaseDate'
			WHEN 'OpportunityMaster.bintCreatedDate' THEN 'OpportunityMaster.bintCreatedDate'
			WHEN 'OpportunityMaster.numAge' THEN 'OpportunityMaster.bintCreatedDate'
			WHEN 'OpportunityMaster.bitPaidInFull' THEN '(CASE WHEN ISNULL(TempPaid.monAmountPaid,0) >= ISNULL(OpportunityMaster.monDealAmount,0) THEN 1 ELSE 0 END)'
			WHEN 'OpportunityMaster.dtItemReceivedDate' THEN '(SELECT TOP 1 OMInner.dtItemReceivedDate FROM OpportunityMaster OMInner INNER JOIN OpportunityItems OIInner ON OMInner.numOppID=OIInner.numOppID WHERE OMInner.tintOppType=2 AND OMInner.tintOppStatus=1 AND OIInner.numItemCode=OpportunityItems.numItemCode ORDER BY OMInner.dtItemReceivedDate)'
			ELSE 'OpportunityMaster.bintCreatedDate'
		END),' ',(CASE WHEN ISNULL(@vcSortOrder,'') <> '' THEN @vcSortOrder ELSE 'ASC' END),' OFFSET (@numPageIndex-1) * 200 ROWS FETCH NEXT 200 ROWS ONLY')

		EXEC sp_executesql @vcSQLFinal, N'@numDomainID NUMERIC(18,0), @vcOrderStauts VARCHAR(MAX), @vcFilterValue VARCHAR(MAX), @tintFilterBy TINYINT, @numViewID NUMERIC(18,0), @tintPackingViewMode TINYINT, @numShippingServiceItemID NUMERIC(18,0), @numWarehouseID NUMERIC(18,0), @numShippingZone NUMERIC(18,0), @numDefaultSalesShippingDoc NUMERIC(18,0), @tintInvoicingType TINYINT, @ClientTimeZoneOffset INT, @tintPackingMode TINYINT, @numPageIndex INT', @numDomainID, @vcOrderStauts, @vcFilterValue, @tintFilterBy, @numViewID, @tintPackingViewMode, @numShippingServiceItemID, @numWarehouseID, @numShippingZone, @numDefaultSalesShippingDoc, @tintInvoicingType, @ClientTimeZoneOffset, @tintPackingMode, @numPageIndex;
	END

	PRINT CONCAT('6-',CONVERT( VARCHAR(24), GETDATE(), 121))

	SET @vcSQLFinal = CONCAT('SELECT T1.*',@vcCustomFields,' FROM #TEMPResult T1',@vcCustomWhere)

	PRINT CAST(@vcSQLFinal AS NTEXT)

	EXEC sp_executesql @vcSQLFinal

	PRINT CONCAT('7-',CONVERT( VARCHAR(24), GETDATE(), 121))

	SELECT * FROM @TempFieldsLeft ORDER BY intRowNum

	PRINT CONCAT('8-',CONVERT( VARCHAR(24), GETDATE(), 121))

	IF OBJECT_ID('tempdb..#TEMPMSRecords') IS NOT NULL DROP TABLE #TEMPMSRecords
	IF OBJECT_ID('tempdb..#TEMPResult') IS NOT NULL DROP TABLE #TEMPResult
END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_MassSalesFulfillment_GetShippingBizDocForPacking')
DROP PROCEDURE dbo.USP_MassSalesFulfillment_GetShippingBizDocForPacking
GO

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_MassSalesFulfillment_GetShippingBizDocForPacking]
(
	@numDomainID NUMERIC(18,0)
	,@numUserCntID NUMERIC(18,0)
	,@numOppID NUMERIC(18,0)
	,@numOppBizDocID NUMERIC(18,0)
)
AS
BEGIN
	IF EXISTS (SELECT numShippingReportId FROM ShippingReport WHERE numOppID=@numOppID AND numOppBizDocId=@numOppBizDocID)
	BEGIN
		DECLARE @numShippingReportID NUMERIC(18,0)
		DECLARE @vcPOppName VARCHAR(300)

		SELECT TOP 1  
			@numShippingReportID = numShippingReportId
			,@vcPOppName=OpportunityMaster.vcPOppName
		FROM 
			ShippingReport 
		INNER JOIN
			OpportunityMaster
		ON
			ShippingReport.numOppID = OpportunityMaster.numOppId
		WHERE 
			ShippingReport.numOppID=@numOppID 
			AND ShippingReport.numOppBizDocId=@numOppBizDocID


		SELECT 
			@numShippingReportID AS numShippingReportID
			,@vcPOppName vcPOppName
			,numBoxID
			,numShippingReportId
			,vcBoxName
			,fltTotalWeight
			,fltHeight
			,fltWidth
			,fltLength 
		FROM 
			ShippingBox 
		WHERE 
			numShippingReportId = @numShippingReportID


		SELECT 
			ShippingReportItems.numBoxID
			,ShippingReportItems.numOppBizDocItemID
			,Item.numItemCode
			,Item.vcItemName
			,ShippingReportItems.intBoxQty 
		FROM 
			ShippingReportItems 
		INNER JOIN 
			Item 
		ON 
			ShippingReportItems.numItemCode=Item.numItemCode 
		WHERE 
			numShippingReportId = @numShippingReportID
	END
	ELSE
	BEGIN
		SELECT 
			OM.numOppID
			,ISNULL(OM.intUsedShippingCompany,ISNULL(DM.intShippingCompany,0)) numShipVia
			,ISNULL(OM.numShippingService,ISNULL(DM.numDefaultShippingServiceID,0)) numShipService
			,OBD.numOppBizDocsId
			,OBDI.numOppBizDocItemID
			,OM.vcPOppName
			,I.numContainer
			,ISNULL(IContainer.vcItemName,'') vcContainer
			,(CASE WHEN ISNULL(I.numContainer,0) > 0 THEN I.numNoItemIntoContainer ELSE OI.numUnitHour END) AS numNoItemIntoContainer
			,ISNULL(IContainer.fltWeight,0) fltContainerWeight
			,ISNULL(IContainer.fltHeight,0) fltContainerHeight
			,ISNULL(IContainer.fltWidth,0) fltContainerWidth
			,ISNULL(IContainer.fltLength,0) fltContainerLength
			,OI.numoppitemtCode
			,I.numItemCode
			,I.vcItemName
			,ISNULL(I.fltWeight,0) fltItemWeight
			,ISNULL(I.fltHeight,0) fltItemHeight
			,ISNULL(I.fltWidth,0) fltItemWidth
			,ISNULL(I.fltLength,0) fltItemLength
			,ISNULL(OBDI.numUnitHour,0) numTotalQty
		FROM 
			OpportunityBizDocs OBD
		INNER JOIN
			OpportunityBizDocItems OBDI
		ON
			OBD.numOppBizDocsId = OBDI.numOppBizDocID
		INNER JOIN
			OpportunityMaster OM
		ON
			OBD.numOppId = OM.numOppId
		INNER JOIN
			DivisionMaster DM
		ON
			OM.numDivisionId = DM.numDivisionID
		INNER JOIN
			OpportunityItems OI
		ON
			OM.numOppId = OI.numOppId
			AND OBDI.numOppItemID = OI.numoppitemtCode
		INNER JOIN 
			Item AS I
		ON 
			OI.numItemCode = I.numItemCode
		LEFT JOIN
			Item AS IContainer
		ON
			I.numContainer = IContainer.numItemCode
		WHERE
			OM.numOppId=@numOppID
			AND OBD.numOppBizDocsId = @numOppBizDocID
			AND ISNULL(I.bitContainer,0) = 0
	END	
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_MassSalesFulfillmentConfiguration_Save')
DROP PROCEDURE dbo.USP_MassSalesFulfillmentConfiguration_Save
GO
CREATE PROCEDURE [dbo].[USP_MassSalesFulfillmentConfiguration_Save]
(
	@numDomainID NUMERIC(18,0)
    ,@numUserCntID NUMERIC(18,0)
	,@bitGroupByOrderForPick BIT
	,@bitGroupByOrderForShip BIT
	,@bitGroupByOrderForInvoice BIT
	,@bitGroupByOrderForPay BIT
	,@bitGroupByOrderForClose BIT
	,@tintInvoicingType TINYINT
	,@tintScanValue TINYINT
	,@tintPackingMode TINYINT
	,@tintPendingCloseFilter TINYINT
	,@bitGeneratePickListByOrder BIT
)
AS 
BEGIN
	IF EXISTS (SELECT ID FROM MassSalesFulfillmentConfiguration WHERE numDomainID=@numDomainID AND numUserCntID=@numUserCntID)
	BEGIN
		UPDATE
			MassSalesFulfillmentConfiguration
		SET
			bitGroupByOrderForPick=@bitGroupByOrderForPick
			,bitGroupByOrderForShip=@bitGroupByOrderForShip
			,bitGroupByOrderForInvoice=@bitGroupByOrderForInvoice
			,bitGroupByOrderForPay=@bitGroupByOrderForPay
			,bitGroupByOrderForClose=@bitGroupByOrderForClose
			,tintInvoicingType=@tintInvoicingType
			,tintScanValue=@tintScanValue
			,tintPackingMode=@tintPackingMode
			,tintPendingCloseFilter=@tintPendingCloseFilter
			,bitGeneratePickListByOrder=@bitGeneratePickListByOrder
		WHERE
			numDomainID=@numDomainID 
			AND numUserCntID=@numUserCntID
	END
	ELSE
	BEGIN
		INSERT INTO MassSalesFulfillmentConfiguration
		(
			numDomainID 
			,numUserCntID
			,bitGroupByOrderForPick
			,bitGroupByOrderForShip
			,bitGroupByOrderForInvoice
			,bitGroupByOrderForPay
			,bitGroupByOrderForClose
			,tintInvoicingType
			,tintScanValue
			,tintPackingMode
			,tintPendingCloseFilter
			,bitGeneratePickListByOrder
		)
		VALUES
		(
			@numDomainID 
			,@numUserCntID
			,@bitGroupByOrderForPick
			,@bitGroupByOrderForShip
			,@bitGroupByOrderForInvoice
			,@bitGroupByOrderForPay
			,@bitGroupByOrderForClose
			,@tintInvoicingType
			,@tintScanValue
			,@tintPackingMode
			,@tintPendingCloseFilter
			,@bitGeneratePickListByOrder
		)
	END
END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OPPGetINItems')
DROP PROCEDURE USP_OPPGetINItems
GO
-- =============================================  
-- Modified by: <Author,,Sachin Sadhu>  
-- Create date: <Create Date,,24thDec2013>  
-- Description: <Description,,add 3 fields:vcBizDocImagePath,vcBizDocFooter,vcPurBizDocFooter>  
-- =============================================  
CREATE PROCEDURE [dbo].[USP_OPPGetINItems]
(
	@byteMode AS TINYINT  = NULL,
	@numOppId AS NUMERIC(18,0)  = NULL,
	@numOppBizDocsId AS NUMERIC(18,0)  = NULL,
	@numDomainID AS NUMERIC(18,0)  = 0,
	@numUserCntID AS NUMERIC(9)  = 0,
	@ClientTimeZoneOffset INT=0
)
AS
BEGIN
	-- TRANSFERRED PARAMETER VALUES TO TEMPORARY VARIABLES TO AVAOID PARAMETER SNIFFING
	DECLARE @byteModeTemp AS TINYINT
	DECLARE @numOppIdTemp AS NUMERIC(18,0)
	DECLARE @numOppBizDocsIdTemp AS NUMERIC(18,0)
	DECLARE @numDomainIDTemp AS NUMERIC(18,0)
	DECLARE @numUserCntIDTemp AS NUMERIC(18,0)
	DECLARE @ClientTimeZoneOffsetTemp INT

	SET @byteModeTemp= @byteMode
	SET @numOppIdTemp = @numOppId
	SET @numOppBizDocsIdTemp=@numOppBizDocsId
	SET @numDomainIDTemp = @numDomainID
	SET @numUserCntIDTemp = @numUserCntID
	SET @ClientTimeZoneOffsetTemp = @ClientTimeZoneOffset


	IF @byteModeTemp = 1
    BEGIN
      DECLARE  @BizDcocName  AS VARCHAR(100)
      DECLARE  @OppName  AS VARCHAR(100)
      DECLARE  @Contactid  AS VARCHAR(100)
      DECLARE  @shipAmount  AS DECIMAL(20,5)
      DECLARE  @tintOppType  AS TINYINT
      DECLARE  @numlistitemid  AS VARCHAR(15)
      DECLARE  @RecOwner  AS VARCHAR(15)
      DECLARE  @MonAmount  AS VARCHAR(15)
      DECLARE  @OppBizDocID  AS VARCHAR(100)
      DECLARE  @bizdocOwner  AS VARCHAR(15)
      DECLARE  @tintBillType  AS TINYINT
      DECLARE  @tintShipType  AS TINYINT
      DECLARE  @numCustomerDivID AS NUMERIC 
      
      SELECT @RecOwner = numRecOwner,@MonAmount = CONVERT(VARCHAR(20),monPAmount),
             @Contactid = numContactId,@OppName = vcPOppName,
             @tintOppType = tintOppType,@tintBillType = tintBillToType,@tintShipType = tintShipToType
      FROM   OpportunityMaster WHERE  numOppId = @numOppIdTemp
      
      -- When Creating PO from SO and Bill type is Customer selected 
      SELECT @numCustomerDivID = ISNULL(numDivisionID,0) FROM dbo.OpportunityMaster WHERE 
			numOppID IN (SELECT  ISNULL(numParentOppID,0) FROM dbo.OpportunityLinking WHERE numChildOppID = @numOppIdTemp)
			
      --select @BizDcocName=vcBizDocID from OpportunityBizDocs where numOppBizDocsId=@numOppBizDocsIdTemp
      SELECT @BizDcocName = vcdata,
             @shipAmount = monShipCost,
             @numlistitemid = numlistitemid,
             @OppBizDocID = vcBizDocID,
             @bizdocOwner = OpportunityBizDocs.numCreatedBy
      FROM   listdetails JOIN OpportunityBizDocs ON numBizDocId = numlistitemid
      WHERE  numOppBizDocsId = @numOppBizDocsIdTemp
      
      SELECT @bizdocOwner AS BizDocOwner,
             @OppBizDocID AS BizDocID,
             @MonAmount AS Amount,
             isnull(@RecOwner,1) AS Owner,
             @BizDcocName AS BizDcocName,
             @numlistitemid AS BizDoc,
             @OppName AS OppName,
             VcCompanyName AS CompName,
             @shipAmount AS ShipAmount,
             dbo.fn_getOPPAddress(@numOppIdTemp,@numDomainIDTemp,1) AS BillAdd,
             dbo.fn_getOPPAddress(@numOppIdTemp,@numDomainIDTemp,2) AS ShipAdd,
             AD.VcStreet,
             AD.VcCity,
             isnull(dbo.fn_GetState(AD.numState),'') AS vcBilState,
             AD.vcPostalCode,
             isnull(dbo.fn_GetListItemName(AD.numCountry),
                    '') AS vcBillCountry,
             con.vcFirstName AS ConName,
             ISNULL(con.numPhone,'') numPhone,
             con.vcFax,
             isnull(con.vcEmail,'') AS vcEmail,
             div.numDivisionID,
             numContactid,
             isnull(vcFirstFldValue,'') vcFirstFldValue,
             isnull(vcSecndFldValue,'') vcSecndFldValue,
             isnull(bitTest,0) AS bitTest,
			dbo.fn_getOPPAddress(@numOppIdTemp,@numDomainIDTemp,1) AS BillToAddressName,
			dbo.fn_getOPPAddress(@numOppIdTemp,@numDomainIDTemp,1) AS ShipToAddressName,
             CASE 
               WHEN @tintBillType IS NULL THEN Com.vcCompanyName
             	-- When Sales order and bill to is set to customer 
               WHEN @tintBillType = 1 AND @tintOppType = 1 THEN Com.vcCompanyName
             -- When Create PO from SO and Bill to is set to Customer 
			   WHEN @tintBillType = 1 AND @tintOppType = 2 THEN dbo.fn_GetComapnyName(@numCustomerDivID)
               WHEN @tintBillType = 0 THEN (SELECT TOP 1 Com1.vcCompanyName
                                            FROM   companyinfo [Com1]
                                                   JOIN divisionmaster div1
                                                     ON com1.numCompanyID = div1.numCompanyID
                                                   JOIN Domain D1
                                                     ON D1.numDivisionID = div1.numDivisionID
                                                    JOIN dbo.AddressDetails AD1
														ON AD1.numDomainID = div1.numDomainID AND AD1.numRecordID = div1.numDivisionID AND tintAddressOf = 2 AND tintAddressType = 1 AND bitIsPrimary=1
                                            WHERE  D1.numDomainID = @numDomainIDTemp)
               WHEN @tintBillType = 2 THEN (SELECT vcBillCompanyName
                                            FROM   OpportunityAddress
                                            WHERE  numOppID = @numOppIdTemp)
				WHEN @tintBillType = 3 THEN  Com.vcCompanyName
              END AS BillToCompanyName,
             CASE 
               WHEN @tintShipType IS NULL THEN Com.vcCompanyName
               WHEN @tintShipType = 1 AND @tintOppType = 1 THEN Com.vcCompanyName
				-- When Create PO from SO and Ship to is set to Customer 
   			   WHEN @tintShipType = 1 AND @tintOppType = 2 THEN dbo.fn_GetComapnyName(@numCustomerDivID)
               WHEN @tintShipType = 0 THEN (SELECT TOP 1 Com1.vcCompanyName
                                            FROM   companyinfo [Com1]
                                                   JOIN divisionmaster div1
                                                     ON com1.numCompanyID = div1.numCompanyID
                                                   JOIN Domain D1
                                                     ON D1.numDivisionID = div1.numDivisionID
                                                    JOIN dbo.AddressDetails AD1
													 ON AD1.numDomainID = div1.numDomainID AND AD1.numRecordID = div1.numDivisionID AND tintAddressOf = 2 AND tintAddressType = 2 AND bitIsPrimary=1
                                            WHERE  D1.numDomainID = @numDomainIDTemp)
               WHEN @tintShipType = 2 THEN (SELECT vcShipCompanyName
                                            FROM   OpportunityAddress
                                            WHERE  numOppID = @numOppIdTemp)
				WHEN @tintShipType = 3 THEN (SELECT vcShipCompanyName
														FROM   OpportunityAddress
														WHERE  numOppID = @numOppIdTemp)
             END AS ShipToCompanyName
      FROM   companyinfo [Com] JOIN divisionmaster div ON com.numCompanyID = div.numCompanyID
             JOIN AdditionalContactsInformation con ON con.numdivisionId = div.numdivisionId
             JOIN Domain D ON D.numDomainID = div.numDomainID
             LEFT JOIN PaymentGatewayDTLID PGDTL ON PGDTL.intPaymentGateWay = D.intPaymentGateWay AND D.numDomainID = PGDTL.numDomainID
             LEFT JOIN AddressDetails AD --Billing add
				ON AD.numDomainID=DIV.numDomainID AND AD.numRecordID=div.numDivisionID AND AD.tintAddressOf = 2 AND AD.tintAddressType = 1 AND AD.bitIsPrimary=1
			 LEFT JOIN AddressDetails AD2--Shipping address
				ON AD2.numDomainID=DIV.numDomainID AND AD2.numRecordID=div.numDivisionID AND AD2.tintAddressOf = 2 AND AD2.tintAddressType = 2 AND AD2.bitIsPrimary=1	
      WHERE  numContactid = @Contactid AND Div.numDomainID = @numDomainIDTemp
    END
  IF @byteModeTemp = 2
    BEGIN
      SELECT opp.vcBizDocID,
             Mst.fltDiscount AS decDiscount,
             isnull(opp.monAmountPaid,0) AS monAmountPaid,
             isnull(opp.vcComments,'') AS vcComments,
             CONVERT(VARCHAR(20),DATEADD(minute,-@ClientTimeZoneOffsetTemp,opp.dtCreatedDate)) dtCreatedDate,
			 dbo.fn_GetContactName(Opp.numCreatedby)+ ' ' + CONVERT(VARCHAR(20),dateadd(MINUTE,-@ClientTimeZoneOffsetTemp,Opp.dtCreatedDate)) AS CreatedBy,
             dbo.fn_GetContactName(opp.numCreatedby) AS numCreatedby,
             dbo.fn_GetContactName(opp.numModifiedBy) AS numModifiedBy,
             CONVERT(VARCHAR(20),DATEADD(minute,-@ClientTimeZoneOffsetTemp,opp.dtModifiedDate)) dtModifiedDate,
			 dbo.fn_GetContactName(Opp.numModifiedBy) + ' ' + CONVERT(VARCHAR(20),dateadd(MINUTE,-@ClientTimeZoneOffsetTemp,Opp.dtModifiedDate)) AS ModifiedBy,
             CASE 
               WHEN isnull(opp.numViewedBy,0) = 0 THEN ''
               ELSE dbo.fn_GetContactName(opp.numViewedBy)
             END AS numViewedBy,
             CASE 
               WHEN opp.numViewedBy = 0 THEN NULL
               ELSE opp.dtViewedDate
             END AS dtViewedDate,
             (CASE WHEN isnull(Mst.intBillingDays,0) > 0 THEN CAST(1 AS BIT) ELSE isnull(Mst.bitBillingTerms,0) END) AS tintBillingTerms,
             isnull(Mst.intBillingDays,0) AS numBillingDays,
			 ISNULL(BTR.numNetDueInDays,0) AS numBillingDaysName,
			 BTR.vcTerms AS vcBillingTermsName,
             isnull(Mst.bitInterestType,0) AS tintInterestType,
             isnull(Mst.fltInterest,0) AS fltInterest,
             tintOPPType,
			 tintOppStatus,
             Opp.numBizDocId,
             dbo.fn_GetContactName(numApprovedBy) AS ApprovedBy,
             dtApprovedDate,
             Mst.bintAccountClosingDate,
             tintShipToType,
             tintBillToType,
             tintshipped,
             dtShippedDate bintShippedDate,
             isnull(opp.monShipCost,0) AS monShipCost,
             (SELECT COUNT(* )
              FROM   DocumentWorkflow
              WHERE  numDocID = @numOppBizDocsIdTemp
                     AND numContactID = @numUserCntIDTemp
                     AND cDocType = 'B'
                     AND tintApprove = 0) AS AppReq,
             (SELECT COUNT(* )
              FROM   DocumentWorkflow
              WHERE  numDocID = @numOppBizDocsIdTemp
                     AND cDocType = 'B'
                     AND tintApprove = 1) AS Approved,
             (SELECT COUNT(* )
              FROM   DocumentWorkflow
              WHERE  numDocID = @numOppBizDocsIdTemp
                     AND cDocType = 'B'
                     AND tintApprove = 2) AS Declined,
             (SELECT COUNT(* )
              FROM   DocumentWorkflow
              WHERE  numDocID = @numOppBizDocsIdTemp
                     AND cDocType = 'B'
                     AND tintApprove = 0) AS Pending,
             ISNULL(BT.vcBizDocFooter,D.vcBizDocFooter) AS BizdocFooter,
             ISNULL(BT.vcBizDocImagePath,D.vcBizDocImagePath) As vcBizDocImagePath, 
             Mst.numDivisionID,
             ISNULL(BT.vcPurBizDocFooter,D.vcPurBizDocFooter) AS PurBizdocFooter, 
             isnull(Mst.fltDiscount,0) AS fltDiscount,
             isnull(Mst.bitDiscountType,0) AS bitDiscountType,
             isnull(Opp.numShipVia,ISNULL(Mst.intUsedShippingCompany,0)) AS numShipVia,
--             isnull(vcTrackingURL,'') AS vcTrackingURL,
			ISNULL( (SELECT vcShipFieldValue FROM dbo.ShippingFieldValues WHERE numDomainID=MSt.numDomainID AND intShipFieldID IN ( SELECT intShipFieldID FROM dbo.ShippingFields WHERE numListItemID=Opp.numShipVia  AND vcFieldName='Tracking URL')),'')		AS vcTrackingURL,
             isnull(Opp.numBizDocStatus,0) AS numBizDocStatus,
             CASE 
               WHEN Opp.numBizDocStatus IS NULL THEN '-'
               ELSE dbo.fn_GetListItemName(Opp.numBizDocStatus)
             END AS BizDocStatus,
             CASE 
               WHEN ISNULL(Opp.numShipVia,0) = 0 THEN (CASE WHEN Mst.intUsedShippingCompany IS NULL THEN '-' WHEN Mst.intUsedShippingCompany = -1 THEN 'Will-call' ELSE dbo.fn_GetListItemName(Mst.intUsedShippingCompany) END)
			   WHEN Opp.numShipVia = -1 THEN 'Will-call'
               ELSE dbo.fn_GetListItemName(Opp.numShipVia)
             END AS ShipVia,
			 ISNULL((SELECT vcShipmentService FROM ShippingService WHERE (numDomainID=@numDomainIDTemp OR ISNULL(numDomainID,0)=0) AND numShippingServiceID = Mst.numShippingService),'') AS vcShippingService,
             ISNULL(C.varCurrSymbol,'') varCurrSymbol,
             (SELECT COUNT(*) FROM [ShippingReport] WHERE numOppBizDocId = opp.numOppBizDocsId )AS ShippingReportCount,
			 isnull(bitPartialFulfilment,0) bitPartialFulfilment,
			 ISNULL(Opp.vcBizDocName,'') vcBizDocName,
			 dbo.[fn_GetContactName](opp.[numModifiedBy])  + ', '
				+ CONVERT(VARCHAR(20),dateadd(MINUTE,-@ClientTimeZoneOffsetTemp,Opp.dtModifiedDate)) AS ModifiedBy,
			 dbo.[fn_GetContactName](Mst.[numRecOwner]) + ', ' +  dbo.[fn_GetContactName](Mst.[numAssignedTo]) AS OrderRecOwner,
			 dbo.[fn_GetContactName](DM.[numRecOwner]) + ', ' +  dbo.[fn_GetContactName](DM.[numAssignedTo]) AS AccountRecOwner,
			 Opp.dtFromDate,
			 Mst.tintTaxOperator,
			 dbo.GetAccountedEmbeddedCost(opp.numOppBizDocsID,1) AS monTotalEmbeddedCost,
			 dbo.GetAccountedEmbeddedCost(opp.numOppBizDocsID,2) AS monTotalAccountedCost,
			 ISNULL(BT.bitEnabled,0) bitEnabled,
			 ISNULL(BT.txtBizDocTemplate,'') txtBizDocTemplate, 
			 ISNULL(BT.txtCSS,'') txtCSS,
			 ISNULL(BT.numOrientation,0) numOrientation,
			 ISNULL(BT.bitKeepFooterBottom,0) bitKeepFooterBottom,
			 dbo.fn_GetContactName(Mst.numAssignedTo) AS AssigneeName,
			 ISNULL((SELECT vcEmail FROM dbo.AdditionalContactsInformation WHERE numContactID = Mst.numAssignedTo),'') AS AssigneeEmail,
			 ISNULL((SELECT numPhone FROM dbo.AdditionalContactsInformation WHERE numContactID = Mst.numAssignedTo),'') AS AssigneePhone,
			 dbo.fn_GetComapnyName(Mst.numDivisionId) OrganizationName,
			 dbo.fn_GetContactName(Mst.numContactID)  OrgContactName,

--CASE WHEN Mst.tintOppType = 1 THEN 
--dbo.getCompanyAddress((select ISNULL(DM.numDivisionID,0)  as numDivisionID from AdditionalContactsInformation AC INNER JOIN DivisionMaster DM 
--on AC.numDivisionID = DM.numDivisionID where numContactID = Mst.numCreatedBy),1,Mst.numDomainId)
--               ELSE dbo.getCompanyAddress(Mst.numDivisionId,1,Mst.numDomainId)
--             END AS CompanyBillingAddress1,

CASE WHEN Mst.tintOppType = 1 THEN 
dbo.getCompanyAddress((select ISNULL(AC.numDivisionID,0)  as numDivisionID from AdditionalContactsInformation AC
INNER JOIN Domain D ON D.numDomainID = AC.numDomainID where AC.numContactID = D.numAdminID AND D.numDomainID = Mst.numDomainId),1,Mst.numDomainId)
               ELSE dbo.getCompanyAddress(Mst.numDivisionId,1,Mst.numDomainId)
             END AS CompanyBillingAddress,

CASE WHEN Mst.tintOppType = 1 THEN 
dbo.getCompanyAddress((select ISNULL(AC.numDivisionID,0)  as numDivisionID from AdditionalContactsInformation AC
INNER JOIN Domain D ON D.numDomainID = AC.numDomainID where AC.numContactID = D.numAdminID AND D.numDomainID = Mst.numDomainId),1,Mst.numDomainId)
               ELSE dbo.getCompanyAddress(Mst.numDivisionId,2,Mst.numDomainId)
             END AS CompanyShippingAddress,
--
--			 dbo.getCompanyAddress(Mst.numDivisionId,1,Mst.numDomainId) CompanyBillingAddress,
--			 dbo.getCompanyAddress(Mst.numDivisionId,2,Mst.numDomainId) CompanyShippingAddress,
			 case when ACI.numPhone<>'' then ACI.numPhone +case when ACI.numPhoneExtension<>'' then ' - ' + ACI.numPhoneExtension else '' end  else '' END OrgContactPhone,
			 DM.vcComPhone as OrganizationPhone,
             ISNULL(ACI.vcEmail,'') AS OrgContactEmail,
 			 dbo.[fn_GetContactName](Mst.[numRecOwner]) AS OnlyOrderRecOwner,
 			 ISNULL(Opp.bitAuthoritativeBizDocs,0) bitAuthoritativeBizDocs,
			 ISNULL(Opp.tintDeferred,0) tintDeferred,isnull(Opp.monCreditAmount,0) AS monCreditAmount,
			 isnull(Opp.bitRentalBizDoc,0) as bitRentalBizDoc,isnull(BT.numBizDocTempID,0) as numBizDocTempID,isnull(BT.vcTemplateName,'') as vcTemplateName,
		     [dbo].[GetDealAmount](opp.numOppId ,getutcdate(),opp.numOppBizDocsId ) as monPAmount,
			 --(select top 1 SD.vcSignatureFile from SignatureDetail SD join OpportunityBizDocsDetails OBD on SD.numSignID=OBD.numSignID where SD.numDomainID = @numDomainIDTemp and OBD.numDomainID = @numDomainIDTemp and OBD.numSignID IS not null and OBD.numBizDocsId=opp.numOppBizDocsId order by OBD.numBizDocsPaymentDetId desc) as vcSignatureFile,
			 isnull(CMP.txtComments,'') as vcOrganizationComments,
			 isnull(Opp.vcTrackingNo,'') AS  vcTrackingNo
			 ,'' AS  vcShippingMethod
			 ,Opp.dtDeliveryDate,
			 CASE WHEN Opp.vcRefOrderNo IS NULL OR LEN(Opp.vcRefOrderNo)=0 THEN ISNULL(Mst.vcOppRefOrderNo,'') ELSE 
			  isnull(Opp.vcRefOrderNo,'') END AS vcRefOrderNo,ISNULL(Mst.numDiscountAcntType,0) AS numDiscountAcntType,
			 ISNULL(opp.numSequenceId,'') AS numSequenceId,
			 CASE WHEN ISNULL(Opp.numBizDocId,0)=296 THEN ISNULL((SELECT SUBSTRING((SELECT '$^$' + dbo.fn_GetListItemName(numBizDocStatus) +'#^#'+ dbo.fn_GetContactName(numUserCntID) +'#^#'+ dbo.FormatedDateTimeFromDate(DateAdd(minute, -@ClientTimeZoneOffsetTemp , dtCreatedDate),@numDomainIDTemp) + '#^#'+ isnull(DFCS.vcColorScheme,'NoForeColor')
					FROM OppFulfillmentBizDocsStatusHistory OFBS left join DycFieldColorScheme DFCS on OFBS.numBizDocStatus=DFCS.vcFieldValue AND DFCS.numDomainID=@numDomainIDTemp
					WHERE numOppId=opp.numOppId and numOppBizDocsId=opp.numOppBizDocsId FOR XML PATH('')),4,200000)),'') ELSE '' END AS vcBizDocsStatusList,ISNULL(Mst.numCurrencyID,0) AS numCurrencyID,
			ISNULL(Opp.fltExchangeRateBizDoc,Mst.fltExchangeRate) AS fltExchangeRateBizDoc,
			ISNULL(TBLEmployer.vcCompanyName,'') AS EmployerOrganizationName,ISNULL(TBLEmployer.vcComPhone,'') as EmployerOrganizationPhone,ISNULL(opp.bitAutoCreated,0) AS bitAutoCreated,
			ISNULL(RecurrenceConfiguration.numRecConfigID,0) AS numRecConfigID,
			(CASE WHEN RecurrenceConfiguration.numRecConfigID IS NULL THEN 0 ELSE 1 END) AS bitRecur,
			(CASE WHEN RecurrenceTransaction.numRecTranID IS NULL THEN 0 ELSE 1 END) AS bitRecurred,
			ISNULL(Mst.bitRecurred,0) AS bitOrderRecurred,
			dbo.FormatedDateFromDate(RecurrenceConfiguration.dtStartDate,@numDomainIDTemp) AS dtStartDate,
			RecurrenceConfiguration.vcFrequency AS vcFrequency,
			ISNULL(RecurrenceConfiguration.bitDisabled,0) AS bitDisabled,
			ISNULL(opp.bitFulfilled,0) AS bitFulfilled,
			ISNULL(opp.numDeferredBizDocID,0) AS numDeferredBizDocID,
			dbo.GetTotalQtyByUOM(opp.numOppBizDocsId) AS vcTotalQtybyUOM,
			ISNULL(Mst.numShippingService,0) AS numShippingService,
			dbo.FormatedDateFromDate(Mst.dtReleaseDate,@numDomainIDTemp) AS vcReleaseDate,
			(CASE WHEN ISNULL(Mst.numPartner,0) > 0 THEN ISNULL((SELECT CONCAT(DivisionMaster.vcPartnerCode,'-',CompanyInfo.vcCompanyName) FROM DivisionMaster INNER JOIN CompanyInfo ON DivisionMaster.numCompanyID=CompanyInfo.numCompanyId WHERE DivisionMaster.numDivisionID=Mst.numPartner),'') ELSE '' END) AS vcPartner,
			dbo.FormatedDateFromDate(mst.dtReleaseDate,@numDomainIDTemp) AS dtReleaseDate,
			(select TOP 1 vcBizDocID from OpportunityBizDocs where numOppId=opp.numOppId and numBizDocId=296) as vcFulFillment,
			(select TOP 1 vcOppRefOrderNo from OpportunityMaster WHERE numOppId=opp.numOppId) AS vcPOName,
			ISNULL(vcCustomerPO#,'') AS vcCustomerPO#,
			ISNULL(Mst.txtComments,'') vcSOComments,
			ISNULL(DM.vcShippersAccountNo,'') AS vcShippersAccountNo,
			CONVERT(VARCHAR(20),DATEADD(minute,-@ClientTimeZoneOffsetTemp,Mst.bintCreatedDate)) OrderCreatedDate,
			(CASE WHEN ISNULL(opp.numSourceBizDocId,0) > 0 THEN ISNULL((SELECT vcVendorInvoice FROM OpportunityBizDocs WHERE OpportunityBizDocs.numOppBizDocsId=opp.numSourceBizDocId),'') ELSE ISNULL(vcVendorInvoice,'') END) vcVendorInvoice,
			(CASE 
				WHEN ISNULL(opp.numSourceBizDocId,0) > 0
				THEN ISNULL((SELECT vcBizDocID FROM OpportunityBizDocs WHERE OpportunityBizDocs.numOppBizDocsId=Opp.numSourceBizDocId),'')
				ELSE ''
			END) vcPackingSlip
			,ISNULL(numARAccountID,0) numARAccountID
		FROM   OpportunityBizDocs opp JOIN OpportunityMaster Mst ON Mst.numOppId = opp.numOppId
             JOIN Domain D ON D.numDomainID = Mst.numDomainID
             LEFT OUTER JOIN [Currency] C ON C.numCurrencyID = Mst.numCurrencyID
             LEFT JOIN [DivisionMaster] DM ON DM.numDivisionID = Mst.numDivisionID   
			 LEFT JOIN CompanyInfo CMP ON DM.numCompanyId = CMP.numCompanyId and CMP.numDomainID = @numDomainIDTemp  
             LEFT JOIN dbo.AdditionalContactsInformation ACI ON ACI.numContactID = Mst.numContactID
             LEFT JOIN [BizDocTemplate] BT ON BT.numDomainID = @numDomainIDTemp AND BT.numBizDocTempID=opp.numBizDocTempID
             LEFT JOIN BillingTerms BTR ON BTR.numTermsID = ISNULL(Mst.intBillingDays,0)
             JOIN divisionmaster div1 ON Mst.numDivisionID = div1.numDivisionID
			 JOIN companyinfo [Com1] ON com1.numCompanyID = div1.numCompanyID
			 LEFT JOIN RecurrenceConfiguration ON Opp.numOppBizDocsId = RecurrenceConfiguration.numOppBizDocID
			 LEFT JOIN RecurrenceTransaction ON Opp.numOppBizDocsId = RecurrenceTransaction.numRecurrOppBizDocID
			 OUTER APPLY
			 (
				SELECT TOP 1 
					Com1.vcCompanyName, div1.vcComPhone
                FROM   companyinfo [Com1]
                        JOIN divisionmaster div1
                            ON com1.numCompanyID = div1.numCompanyID
                        JOIN Domain D1
                            ON D1.numDivisionID = div1.numDivisionID
                        JOIN dbo.AddressDetails AD1
							ON AD1.numDomainID = div1.numDomainID AND AD1.numRecordID = div1.numDivisionID AND tintAddressOf = 2 AND tintAddressType = 1 AND bitIsPrimary=1
                WHERE  D1.numDomainID = @numDomainIDTemp
			 ) AS TBLEmployer
      WHERE  opp.numOppBizDocsId = @numOppBizDocsIdTemp
             AND Mst.numDomainID = @numDomainIDTemp
    END
END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityMaster_GetOrderDetailForBizDoc')
DROP PROCEDURE dbo.USP_OpportunityMaster_GetOrderDetailForBizDoc
GO

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_OpportunityMaster_GetOrderDetailForBizDoc]
(
	@numDomainID NUMERIC(18,0)
	,@numOppID NUMERIC(18,0)
)
AS
BEGIN
	SELECT
		OpportunityMaster.numOppId
		,OpportunityMaster.tintOppType
		,OpportunityMaster.numContactID
		,DivisionMaster.numCurrencyID AS numBaseCurrencyID
		,ISNULL(OpportunityMaster.numCurrencyID,0) AS numCurrencyID
		,DivisionMaster.numDivisionID
		,CompanyInfo.numCompanyType
		,CompanyInfo.vcProfile
		,DivisionMaster.numDivisionID
		,CompanyInfo.vcCompanyName
		,ISNULL(OpportunityMaster.fltExchangeRate,1) AS fltExchangeRate
		,ISNULL(OpportunityMaster.intUsedShippingCompany,DivisionMaster.intShippingCompany) AS intUsedShippingCompany
	FROM
		OpportunityMaster
	INNER JOIN
		DivisionMaster
	ON
		OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
	INNER JOIN
		CompanyInfo
	ON
		DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
	WHERE
		OpportunityMaster.numDomainId = @numDomainID
		AND numOppId=@numOppID
END
GO

GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_UpdateInventoryAdjustments' ) 
    DROP PROCEDURE USP_UpdateInventoryAdjustments
GO
CREATE PROCEDURE USP_UpdateInventoryAdjustments
    @numDomainId NUMERIC(18,0),
    @numUserCntID NUMERIC(18,0),
	@numItemCode NUMERIC(18,0),
    @strItems VARCHAR(8000),
    @dtAdjustmentDate AS DATETIME,
	@fltReorderQty FLOAT,
	@fltReorderPoint FLOAT
AS 
BEGIN
	-- TRANSACTION IS USED IN CODE
	UPDATE Item SET fltReorderQty=@fltReorderQty WHERE numItemCode=@numItemCode
	UPDATE WareHouseItems SET numReorder=@fltReorderPoint WHERE numItemID=@numItemCode


	IF LEN(ISNULL(@strItems,'')) > 0
	BEGIN
		DECLARE @hDoc INT    
		EXEC sp_xml_preparedocument @hDoc OUTPUT, @strItems    
  
		SELECT 
			* 
		INTO 
			#temp 
		FROM 
			OPENXML (@hDoc,'/NewDataSet/Item',2)    
		WITH 
		( 
			numWareHouseItemID NUMERIC(18,0)
			,numItemCode NUMERIC(18,0)
			,intAdjust FLOAT
			,monAverageCost DECIMAL(20,5)
		) 
		
		SELECT 
			*
			,ROW_NUMBER() OVER(ORDER BY numItemCode) AS RowNo 
		INTO 
			#tempSerialLotNO 
		FROM 
			OPENXML (@hDoc,'/NewDataSet/SerialLotNo',2)    
		WITH 
			(
				numItemCode NUMERIC(18,0)
				,numWareHouseID NUMERIC(18,0)
				,numWareHouseItemID NUMERIC(18,0)
				,vcSerialNo VARCHAR(3000)
				,byteMode TINYINT
			) 
		
		EXEC sp_xml_removedocument @hDoc  

		DECLARE @numOnHand FLOAT
		DECLARE @numAllocation FLOAT
		DECLARE @numBackOrder FLOAT
		DECLARE @numWareHouseItemID AS NUMERIC(18,0)
		DECLARE @numTempItemCode NUMERIC(18,0)
			
		SELECT 
			@numWareHouseItemID = MIN(numWareHouseItemID) 
		FROM 
			#temp
			
		DECLARE @bitLotNo AS BIT = 0  
		DECLARE @bitSerialized AS BIT = 0  
		DECLARE @monItemAvgCost DECIMAL(20,5)
		DECLARE @monUnitCost DECIMAL(20,5)
		DECLARE @monAvgCost AS DECIMAL(20,5) 
		DECLARE @TotalOnHand AS FLOAT;
		DECLARE @numNewQtyReceived AS FLOAT

		WHILE @numWareHouseItemID>0    
		BEGIN		  
			SET @monAvgCost = 0
			SET @TotalOnHand = 0
			SET @numNewQtyReceived = 0

			SELECT TOP 1
				@bitLotNo=ISNULL(Item.bitLotNo,0)
				,@bitSerialized=ISNULL(Item.bitSerialized,0)
				,@monItemAvgCost = (CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(Item.monAverageCost, 0) END)
				,@monUnitCost = (CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(#temp.monAverageCost, 0) END)
				,@numNewQtyReceived = ISNULL(#temp.intAdjust,0)
			FROM 
				Item 
			INNER JOIN 
				#temp 
			ON 
				item.numItemCode=#temp.numItemCode 
			INNER JOIN
				WareHouseItems
			ON
				#temp.numWareHouseItemID = WareHouseItems.numWareHouseItemID
			WHERE 
				#temp.numWareHouseItemID=@numWareHouseItemID 
				AND Item.numDomainID=@numDomainID
				AND WareHouseItems.numDomainID=@numDomainID

			IF @bitLotNo=0 AND @bitSerialized=0
			BEGIN		
				IF (SELECT 
						ISNULL(numOnHand,0) + ISNULL(numAllocation,0) + #temp.intAdjust 
					FROM 
						WareHouseItems WI 
					INNER JOIN 
						#temp 
					ON 
						WI.numWareHouseItemID = #temp.numWareHouseItemID 
					WHERE 
						WI.numWareHouseItemID = @numWareHouseItemID AND WI.numDomainID=@numDomainId) >= 0
				BEGIN
					SELECT @numTempItemCode=numItemID FROM dbo.WareHouseItems WHERE numWareHouseItemID = @numWareHouseItemID AND numDomainID=@numDomainId
								
					--Updating the Average Cost
					SELECT @TotalOnHand=(SUM(ISNULL(numOnHand,0)) + SUM(ISNULL(numAllocation,0))) FROM dbo.WareHouseItems WHERE numItemID=@numTempItemCode
					IF @TotalOnHand + @numNewQtyReceived > 0
					BEGIN
						SET @monAvgCost = ((@TotalOnHand * @monItemAvgCost) + (@numNewQtyReceived * @monUnitCost)) / ( @TotalOnHand + @numNewQtyReceived )
					END
					ELSE
					BEGIN
						SET @monAvgCost = @monItemAvgCost
					END

					UPDATE  
						item
					SET 
						monAverageCost = (CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE @monAvgCost END)
						,bintModifiedDate=GETUTCDATE()
						,numModifiedBy=@numUserCntID
					WHERE 
						numItemCode = @numTempItemCode

					IF (SELECT intAdjust FROM #temp WHERE numWareHouseItemID = @numWareHouseItemID) >= 0
					BEGIN
						UPDATE 
							WI 
						SET 
							numOnHand = ISNULL(numOnHand,0) + (CASE WHEN ISNULL(numBackOrder,0) >= #temp.intAdjust THEN  0 ELSE (#temp.intAdjust - ISNULL(numBackOrder,0)) END)
							,numAllocation = ISNULL(numAllocation,0) + (CASE WHEN ISNULL(numBackOrder,0) >= #temp.intAdjust THEN  #temp.intAdjust ELSE ISNULL(numBackOrder,0) END) 
							,numBackOrder = (CASE WHEN ISNULL(numBackOrder,0) >= #temp.intAdjust THEN (ISNULL(numBackOrder,0) - #temp.intAdjust) ELSE 0 END)
							,dtModified=GETDATE() 
						FROM 
							dbo.WareHouseItems WI 
						INNER JOIN 
							#temp 
						ON 
							WI.numWareHouseItemID = #temp.numWareHouseItemID
						WHERE 
							WI.numWareHouseItemID = @numWareHouseItemID 
							AND WI.numDomainID=@numDomainId
					END
					ELSE
					BEGIN
						UPDATE 
							WI 
						SET 
							numOnHand = (CASE WHEN ISNULL(numOnHand,0) + #temp.intAdjust >= 0 THEN ISNULL(numOnHand,0) + #temp.intAdjust ELSE 0 END)
							,numAllocation = (CASE WHEN ISNULL(numOnHand,0) + #temp.intAdjust < 0 THEN (CASE WHEN ISNULL(numAllocation,0) + (ISNULL(numOnHand,0) + #temp.intAdjust) >= 0 THEN ISNULL(numAllocation,0) + (ISNULL(numOnHand,0) + #temp.intAdjust) ELSE 0 END) ELSE ISNULL(numAllocation,0) END)
							,numBackOrder = (CASE WHEN ISNULL(numOnHand,0) + #temp.intAdjust < 0 THEN ISNULL(numBackOrder,0) - (CASE WHEN ISNULL(numAllocation,0) + (ISNULL(numOnHand,0) + #temp.intAdjust) >= 0 THEN ISNULL(numOnHand,0) + #temp.intAdjust ELSE ISNULL(numAllocation,0) + (ISNULL(numOnHand,0) + #temp.intAdjust) END) ELSE ISNULL(numBackOrder,0) END)
							,dtModified=GETDATE() 
						FROM 
							dbo.WareHouseItems WI 
						INNER JOIN 
							#temp 
						ON 
							WI.numWareHouseItemID = #temp.numWareHouseItemID
						WHERE 
							WI.numWareHouseItemID = @numWareHouseItemID 
							AND WI.numDomainID=@numDomainId
					END	  
								
					EXEC dbo.USP_ManageWareHouseItems_Tracking
					@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
					@numReferenceID = @numTempItemCode, --  numeric(9, 0)
					@tintRefType = 1, --  tinyint
					@vcDescription = 'Inventory Adjustment', --  varchar(100)
					@numModifiedBy = @numUserCntID,
					@dtRecordDate = @dtAdjustmentDate,
					@numDomainId = @numDomainID
				END
				ELSE
				BEGIN
					RAISERROR('INVALID_INVENTORY_ADJUSTMENT_NUMBER',16,1)
				END
			END	  
				    
			SELECT TOP 1 
				@numWareHouseItemID = numWareHouseItemID 
			FROM 
				#temp 
			WHERE 
				numWareHouseItemID >@numWareHouseItemID 
			ORDER BY 
				numWareHouseItemID  
				    
			IF @@rowcount=0 
				SET @numWareHouseItemID =0    
		END    
    

		DROP TABLE #temp
    
    
		-------------Serial/Lot #s----------------
		DECLARE @minRowNo NUMERIC(18),@maxRowNo NUMERIC(18)
		SELECT @minRowNo = MIN(RowNo),@maxRowNo = Max(RowNo) FROM #tempSerialLotNO
		    
		DECLARE @numWareHouseID NUMERIC(9),@vcSerialNo VARCHAR(3000),@numWareHouseItmsDTLID AS NUMERIC(9),
				@vcComments VARCHAR(1000),@OldQty FLOAT,@numQty FLOAT,@byteMode TINYINT 		    
		DECLARE @posComma int, @strKeyVal varchar(20)
		    
		WHILE @minRowNo <= @maxRowNo
		BEGIN
			SELECT @numTempItemCode=numItemCode,@numWareHouseID=numWareHouseID,@numWareHouseItemID=numWareHouseItemID,
				@vcSerialNo=vcSerialNo,@byteMode=byteMode FROM #tempSerialLotNO WHERE RowNo=@minRowNo
		
			SET @posComma=0
		
			SET @vcSerialNo=RTRIM(@vcSerialNo)
			IF RIGHT(@vcSerialNo, 1)!=',' SET @vcSerialNo=@vcSerialNo+','

			SET @posComma=PatIndex('%,%', @vcSerialNo)
			WHILE @posComma>1
				BEGIN
					SET @strKeyVal=ltrim(rtrim(substring(@vcSerialNo, 1, @posComma-1)))
	
					DECLARE @posBStart INT,@posBEnd int, @strQty varchar(20),@strName varchar(20)

					SET @posBStart=PatIndex('%(%', @strKeyVal)
					SET @posBEnd=PatIndex('%)%', @strKeyVal)
					IF( @posBStart>1 AND @posBEnd>1)
					BEGIN
						SET @strName=ltrim(rtrim(substring(@strKeyVal, 1, @posBStart-1)))
						SET	@strQty=ltrim(rtrim(substring(@strKeyVal, @posBStart+1,len(@strKeyVal)-@posBStart-1)))
					END		
					ELSE
					BEGIN
						SET @strName=@strKeyVal
						SET	@strQty=1
					END
	  
				SET @numWareHouseItmsDTLID=0
				SET @OldQty=0
				SET @vcComments=''
			
				select top 1 @numWareHouseItmsDTLID=ISNULL(numWareHouseItmsDTLID,0),@OldQty=ISNULL(numQty,0),@vcComments=ISNULL(vcComments,'') 
						from WareHouseItmsDTL where numWareHouseItemID=@numWareHouseItemID and
						vcSerialNo=@strName AND ISNULL(numQty,0) > 0
			
				IF @byteMode=0 --Add
					SET @numQty=@OldQty + @strQty
				ELSE --Deduct
					SET @numQty=@OldQty - @strQty
			
				EXEC dbo.USP_AddUpdateWareHouseForItems
						@numItemCode = @numTempItemCode, --  numeric(9, 0)
						@numWareHouseID = @numWareHouseID, --  numeric(9, 0)
						@numWareHouseItemID =@numWareHouseItemID,
						@numDomainID = @numDomainId, --  numeric(9, 0)
						@vcSerialNo = @strName, --  varchar(100)
						@vcComments = @vcComments, -- varchar(1000)
						@numQty = @numQty, --  numeric(18, 0)
						@byteMode = 5, --  tinyint
						@numWareHouseItmsDTLID = @numWareHouseItmsDTLID, --  numeric(18, 0)
						@numUserCntID = @numUserCntID, --  numeric(9, 0)
						@dtAdjustmentDate = @dtAdjustmentDate
	  
				SET @vcSerialNo=substring(@vcSerialNo, @posComma +1, len(@vcSerialNo)-@posComma)
				SET @posComma=PatIndex('%,%',@vcSerialNo)
			END

			SET @minRowNo=@minRowNo+1
		END
	
		DROP TABLE #tempSerialLotNO

		

		UPDATE Item SET bintModifiedDate=GETUTCDATE(),numModifiedBy=@numUserCntID WHERE numItemCode=@numItemCode
	END   
END


--Created by Anoop Jayaraj

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_UpdatePOItemsForFulfillment')
DROP PROCEDURE USP_UpdatePOItemsForFulfillment
GO
CREATE PROCEDURE  USP_UpdatePOItemsForFulfillment
    @numQtyReceived FLOAT,
    @numOppItemID NUMERIC(9),
    @vcError VARCHAR(200) = '' OUTPUT,
    @numUserCntID AS NUMERIC(9),
    @dtItemReceivedDate AS DATETIME,
	@numSelectedWarehouseItemID AS NUMERIC(18,0) = 0,
	@numVendorInvoiceBizDocID NUMERIC(18,0) = 0,
	@tintMode TINYINT = 0
AS 
BEGIN
--Transaction is invoked from ADO.net (Purchase Fulfillment) Do not add transaction code here.
-- BEGIN TRY
-- BEGIN TRANSACTION
	IF ISNULL(@numVendorInvoiceBizDocID,0) > 0
	BEGIN
		-- IF RECEIVED AGAINST VENDOR INVOICE MAKE SURE ENOUGH QTY IS PENDING TO RECIEVED AGAINST VEDOR INVOICE BECAUSE USER CAN RECEIVE QTY WITHOUT SELECTING VENDOR INVOCIE ALSO
		IF (SELECT
				COUNT(*)
			FROM
				OpportunityBizDocItems OBDI
			INNER JOIN
				OpportunityItems OI
			ON
				OBDI.numOppItemID=OI.numoppitemtCode
			WHERE
				numOppBizDocID=@numVendorInvoiceBizDocID
				AND OBDI.numOppItemID = @numOppItemID
				AND @numQtyReceived > (ISNULL(OI.numUnitHour,0) - ISNULL(OI.numUnitHourReceived,0))
			) > 0
		BEGIN
			RAISERROR('INVALID_VENDOR_INVOICE_RECEIVE_QTY',16,1)
			RETURN
		END
	END


	DECLARE @numDomain AS NUMERIC(18,0)
	          
	DECLARE @numWarehouseItemID AS NUMERIC
	DECLARE @numWLocationID AS NUMERIC(18,0)     
	DECLARE @numOldQtyReceived AS FLOAT       
	DECLARE @numNewQtyReceived AS FLOAT
	              
	DECLARE @bitStockTransfer BIT
	DECLARE @numToWarehouseItemID NUMERIC
	DECLARE @numOppId NUMERIC
	DECLARE @numUnits FLOAT
	DECLARE @monPrice AS DECIMAL(20,5) 
	DECLARE @numItemCode NUMERIC 
		
	SELECT  
		@numWarehouseItemID = ISNULL([numWarehouseItmsID], 0),
		@numWLocationID = ISNULL(WI.numWLocationID,0),
		@numOldQtyReceived = ISNULL([numUnitHourReceived], 0),
		@bitStockTransfer = ISNULL(OM.bitStockTransfer,0),
		@numToWarehouseItemID= ISNULL(numToWarehouseItemID,0),
		@numOppId=OM.numOppId,
		@numUnits=OI.numUnitHour,
		@monPrice=isnull(monPrice,0) * (CASE WHEN ISNULL(OM.fltExchangeRate,0)=0 THEN 1 ELSE OM.fltExchangeRate END),
		@numItemCode=OI.numItemCode,
		@numDomain = OM.[numDomainId]
	FROM 
		[OpportunityItems] OI 
	INNER JOIN 
		dbo.OpportunityMaster OM
	ON 
		OI.numOppId = OM.numOppId
	LEFT JOIN
		WareHouseItems WI
	ON
		OI.numWarehouseItmsID = WI.numWareHouseItemID
	WHERE 
		[numoppitemtCode] = @numOppItemID


	IF @bitStockTransfer = 1 --added by chintan
	BEGIN
	--ship item from FROM warehouse
		DECLARE @p3 VARCHAR(MAX) = ''
		
		EXEC USP_UpdateQtyShipped @numQtyReceived,@numOppItemID,@numUserCntID,@vcError=@p3 OUTPUT

		IF LEN(@p3)>0 
		BEGIN
			RAISERROR ( @p3,16, 1 )
			RETURN ;
		END
		
		-- Receive item from To Warehouse
		SET @numWarehouseItemID=@numToWarehouseItemID
    
	END  
    

	DECLARE @numTotalQuantityReceived FLOAT
	SET @numTotalQuantityReceived = @numQtyReceived + @numOldQtyReceived ;
	SET @numNewQtyReceived = @numQtyReceived;
	DECLARE @description AS VARCHAR(100)

	SET @description=CONCAT('PO Qty Received (Qty:',@numTotalQuantityReceived,')')
	
	IF @numTotalQuantityReceived > @numUnits
	BEGIN
		RAISERROR('RECEIVED_QTY_MUST_BE_LESS_THEN_OR_EQUAL_TO_ORDERED_QTY',16,1)
	END

	IF @numNewQtyReceived <= 0 
		RETURN 
  
	
	IF ISNULL(@bitStockTransfer,0) = 0
	BEGIN
		DECLARE @TotalOnHand AS FLOAT;SET @TotalOnHand=0  
		SELECT @TotalOnHand=(SUM(ISNULL(numOnHand,0)) + SUM(ISNULL(numAllocation,0))) FROM dbo.WareHouseItems WHERE numItemID=@numItemCode 
						
		--Updating the Average Cost
		DECLARE @monAvgCost AS DECIMAL(20,5) 
		SELECT  @monAvgCost = (CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(monAverageCost, 0) END) FROM Item WHERE   numitemcode = @numItemCode  
    
  		SET @monAvgCost = ((@TotalOnHand * @monAvgCost) + (@numNewQtyReceived * @monPrice)) / ( @TotalOnHand + @numNewQtyReceived )
                            
		UPDATE  
			item
		SET 
			monAverageCost = (CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE @monAvgCost END)
		WHERE 
			numItemCode = @numItemCode
    END	

	IF ISNULL(@numSelectedWarehouseItemID,0) > 0 AND ISNULL(@numSelectedWarehouseItemID,0) <> @numWareHouseItemID		
	BEGIN
		DECLARE @vcFromLocation AS VARCHAR(300)
		DECLARE @vcToLocation AS VARCHAR(300)
		SELECT @vcFromLocation=ISNULL(WL.vcLocation,'') FROM WareHouseItems WI LEFT JOIN WarehouseLocation WL ON WI.numWLocationID=WL.numWLocationID WHERE WI.numWareHouseItemID=@numWareHouseItemID
		SELECT @vcToLocation=ISNULL(WL.vcLocation,'') FROM WareHouseItems WI LEFT JOIN WarehouseLocation WL ON WI.numWLocationID=WL.numWLocationID WHERE WI.numWareHouseItemID=@numSelectedWarehouseItemID

		UPDATE
			WareHouseItems
		SET 
			numOnOrder = ISNULL(numOnOrder,0) - @numNewQtyReceived,
			dtModified = GETDATE()
		WHERE
			numWareHouseItemID=@numWareHouseItemID
		
		SET @description = CONCAT('PO Qty Received To Internal Location ',@vcToLocation,' (Qty:',@numNewQtyReceived,')')

		EXEC dbo.USP_ManageWareHouseItems_Tracking
			@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
			@numReferenceID = @numOppId, --  numeric(9, 0)
			@tintRefType = 4, --  tinyint
			@vcDescription = @description,
			@numModifiedBy = @numUserCntID,
			@dtRecordDate =  @dtItemReceivedDate,
			@numDomainID = @numDomain

		-- INCREASE THE OnHand Of Destination Location
		UPDATE
			WareHouseItems
		SET
			numBackOrder = (CASE WHEN numBackOrder >= @numNewQtyReceived THEN ISNULL(numBackOrder,0) - @numNewQtyReceived ELSE 0 END),         
			numAllocation = (CASE WHEN numBackOrder >= @numNewQtyReceived THEN ISNULL(numAllocation,0) + @numNewQtyReceived ELSE ISNULL(numAllocation,0) + ISNULL(numBackOrder,0) END),
			numOnHand = (CASE WHEN numBackOrder >= @numNewQtyReceived THEN numOnHand ELSE ISNULL(numOnHand,0) + @numNewQtyReceived - ISNULL(numBackOrder,0) END),
			dtModified = GETDATE()
		WHERE
			numWareHouseItemID=@numSelectedWarehouseItemID

		SET @description = CONCAT('PO Qty Received From Internal Location ',@vcFromLocation,' (Qty:',@numNewQtyReceived,')')

		EXEC dbo.USP_ManageWareHouseItems_Tracking
			@numWareHouseItemID = @numSelectedWarehouseItemID, --  numeric(9, 0)
			@numReferenceID = @numOppId, --  numeric(9, 0)
			@tintRefType = 4, --  tinyint
			@vcDescription = @description,
			@numModifiedBy = @numUserCntID,
			@dtRecordDate =  @dtItemReceivedDate,
			@numDomainID = @numDomain

		INSERT INTO OpportunityItemsReceievedLocation
		(
			numDomainID,
			numOppID,
			numOppItemID,
			numWarehouseItemID,
			numUnitReceieved
		)
		VALUES
		(
			@numDomain,
			@numOppId,
			@numOppItemID,
			@numSelectedWarehouseItemID,
			@numNewQtyReceived
		)
	END
	ELSE 
	BEGIN
		DECLARE @onHand AS FLOAT
		DECLARE @onAllocation AS FLOAT    
		DECLARE @onOrder AS FLOAT            
		DECLARE @onBackOrder AS FLOAT

		SELECT  
			@onHand = ISNULL(numOnHand, 0),
			@onAllocation = ISNULL(numAllocation, 0),
			@onOrder = ISNULL(numOnOrder, 0),
			@onBackOrder = ISNULL(numBackOrder, 0)
		FROM 
			WareHouseItems
		WHERE 
			numWareHouseItemID = @numWareHouseItemID

		IF @onOrder >= @numNewQtyReceived 
		BEGIN
			PRINT '1 case'

			SET @onOrder = @onOrder - @numNewQtyReceived             
			
			IF @onBackOrder >= @numNewQtyReceived 
			BEGIN            
				SET @onBackOrder = @onBackOrder - @numNewQtyReceived             
				SET @onAllocation = @onAllocation + @numNewQtyReceived             
			END            
			ELSE 
			BEGIN            
				SET @onAllocation = @onAllocation + @onBackOrder            
				SET @numNewQtyReceived = @numNewQtyReceived - @onBackOrder            
				SET @onBackOrder = 0            
				SET @onHand = @onHand + @numNewQtyReceived             
			END         
		END            
		ELSE IF @onOrder < @numNewQtyReceived 
		BEGIN
			PRINT '2 case'        
			SET @onHand = @onHand + @onOrder
			SET @onOrder = @numNewQtyReceived - @onOrder
		END   

		SELECT 
			@onHand onHand,
			@onAllocation onAllocation,
			@onBackOrder onBackOrder,
			@onOrder onOrder
                
		UPDATE 
			WareHouseItems
		SET     
			numOnHand = @onHand,
			numAllocation = @onAllocation,
			numBackOrder = @onBackOrder,
			numOnOrder = @onOrder,
			dtModified = GETDATE() 
		WHERE 
			numWareHouseItemID = @numWareHouseItemID
    
		EXEC dbo.USP_ManageWareHouseItems_Tracking
			@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
			@numReferenceID = @numOppId, --  numeric(9, 0)
			@tintRefType = 4, --  tinyint
			@vcDescription = @description, --  varchar(100)
			@numModifiedBy = @numUserCntID,
			@dtRecordDate =  @dtItemReceivedDate,
			@numDomainID = @numDomain
	END
 
	UPDATE  
		[OpportunityItems]
	SET 
		numUnitHourReceived = @numTotalQuantityReceived
		,numQtyReceived = (CASE WHEN ISNULL(@tintMode,0)=1 THEN ISNULL(numQtyReceived,0) + ISNULL(@numQtyReceived,0) ELSE ISNULL(@numQtyReceived,0) END)
	WHERE
		[numoppitemtCode] = @numOppItemID


	IF ISNULL(@numVendorInvoiceBizDocID,0) > 0
	BEGIN
		UPDATE  
			OpportunityBizDocItems
		SET 
			numVendorInvoiceUnitReceived = ISNULL(numVendorInvoiceUnitReceived,0) +  ISNULL(@numQtyReceived,0)
		WHERE
			numOppBizDocID=@numVendorInvoiceBizDocID
			AND numOppItemID = @numOppItemID
	END

	UPDATE dbo.OpportunityMaster SET dtItemReceivedDate=@dtItemReceivedDate WHERE numOppId=@numOppId
END	
