/******************************************************************
Project: Release 7.1 Date: 14.MAR.2017
Comments: STORE PROCEDURES
*******************************************************************/

/****** Object:  UserDefinedFunction [dbo].[fn_GetAttributes]    Script Date: 07/26/2008 18:12:28 ******/

GO

GO
--- Created By Anoop Jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_getattributes')
DROP FUNCTION fn_getattributes
GO
CREATE FUNCTION [dbo].[fn_GetAttributes] (@numRecID numeric,@bitSerialized  bit)
returns varchar(100)
as
begin
declare @strAttribute varchar(100)
declare @numCusFldID numeric(9)
declare @numCusFldValue varchar(100)
set @numCusFldID=0
set @strAttribute=''
select top 1 @numCusFldID=fld_id,@numCusFldValue=fld_value from CFW_Fld_Values_Serialized_Items where RecId=@numRecID and fld_value!='0' and fld_value!='' order by fld_id
while @numCusFldID>0
begin
	if isnumeric(@numCusFldValue)=1
	begin
		--set @strAttribute=@strAttribute+@numCusFldValue
		select @strAttribute=@strAttribute+Fld_label from  CFW_Fld_Master where Fld_id=@numCusFldID AND Grp_id = 9
                
        IF LEN(@strAttribute) > 0
        BEGIN
			select @strAttribute=@strAttribute+':'+ vcData +',' from ListDetails where numListItemID=@numCusFldValue
        END
		
	end
	else
	begin
		select @strAttribute=@strAttribute+Fld_label from  CFW_Fld_Master where Fld_id=@numCusFldID AND Grp_id = 9
		IF LEN(@strAttribute) > 0
        BEGIN
			select @strAttribute=@strAttribute+':-,' 
        END
	end
	select top 1 @numCusFldID=fld_id,@numCusFldValue=fld_value from CFW_Fld_Values_Serialized_Items where RecId=@numRecID and fld_value!='0' and fld_value!='' and fld_id>@numCusFldID order by fld_id
	if @@rowcount=0 set @numCusFldID=0
	
end

If LEN(@strAttribute) > 0
BEGIN
	SET @strAttribute = LEFT(@strAttribute, LEN(@strAttribute) - 1)
END

return @strAttribute
end
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_GetAttributesIds')
DROP FUNCTION fn_GetAttributesIds
GO
CREATE FUNCTION [dbo].[fn_GetAttributesIds] (@numRecID numeric)
returns varchar(100)
as
begin
declare @strAttribute varchar(100)
declare @numCusFldID numeric(9)
declare @numCusFldValue varchar(100)
set @numCusFldID=0
set @strAttribute=''
select top 1 @numCusFldID=fld_id,@numCusFldValue=fld_value from CFW_Fld_Values_Serialized_Items where RecId=@numRecID and fld_value!='0' and fld_value!='' order by fld_id
while @numCusFldID>0
begin
	IF ISNUMERIC(@numCusFldValue) = 1
	BEGIN
		--set @strAttribute=@strAttribute+@numCusFldValue
		select @strAttribute=CONCAT(@strAttribute,Fld_id) from  CFW_Fld_Master where Fld_id=@numCusFldID AND Grp_id = 9
                
        IF LEN(@strAttribute) > 0
        BEGIN
			select @strAttribute=CONCAT(@strAttribute,':',numListItemID,',') from ListDetails where numListItemID=@numCusFldValue
        END
	END
	
	SELECT TOP 1 @numCusFldID=fld_id,@numCusFldValue=fld_value from CFW_Fld_Values_Serialized_Items where RecId=@numRecID and fld_value!='0' and fld_value!='' and fld_id>@numCusFldID order by fld_id
	if @@rowcount=0 set @numCusFldID=0
	
end

If LEN(@strAttribute) > 0
BEGIN
	SET @strAttribute = LEFT(@strAttribute, LEN(@strAttribute) - 1)
END

return @strAttribute
end
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_GetItemAttributes')
DROP FUNCTION fn_GetItemAttributes
GO
CREATE FUNCTION [dbo].[fn_GetItemAttributes] 
(
	@numDomainID NUMERIC(18,0),
	@numItemCode NUMERIC(18,0)
)
RETURNS VARCHAR(500)
AS
BEGIN
	DECLARE @strAttribute varchar(500) = ''
	DECLARE @numCusFldID NUMERIC(18,0) = 0
	DECLARE @numCusFldValue NUMERIC(18,0) = 0

	SELECT TOP 1
		@numCusFldID=fld_id
		,@numCusFldValue=fld_value 
	FROM 
		ItemAttributes 
	WHERE 
		numDomainID = @numDomainID
		AND numItemCode=@numItemCode 
		AND fld_value != 0
	ORDER BY 
		fld_id

	while @numCusFldID>0
	begin
		if isnumeric(@numCusFldValue)=1
		begin
			select @strAttribute=@strAttribute+Fld_label from  CFW_Fld_Master where Fld_id=@numCusFldID AND Grp_id = 9
                
			IF LEN(@strAttribute) > 0
			BEGIN
				select @strAttribute=@strAttribute+':'+ vcData +',' from ListDetails where numListItemID=@numCusFldValue
			END
		
		end
		else
		begin
			select @strAttribute=@strAttribute+Fld_label from  CFW_Fld_Master where Fld_id=@numCusFldID AND Grp_id = 9
			IF LEN(@strAttribute) > 0
			BEGIN
				select @strAttribute=@strAttribute+':-,' 
			END
		end


		SELECT TOP 1
			@numCusFldID=fld_id
			,@numCusFldValue=fld_value 
		FROM 
			ItemAttributes 
		WHERE 
			numDomainID = @numDomainID
			AND numItemCode=@numItemCode 
			AND fld_value != 0
			AND fld_id > @numCusFldID
		ORDER BY 
			fld_id

		IF @@rowcount=0 SET @numCusFldID=0
	END

	If LEN(@strAttribute) > 0
	BEGIN
		SET @strAttribute = LEFT(@strAttribute, LEN(@strAttribute) - 1)
	END

	RETURN @strAttribute
END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='TF'AND NAME ='fn_GetItemPriceLevel')
DROP FUNCTION fn_GetItemPriceLevel
GO
CREATE FUNCTION [dbo].[fn_GetItemPriceLevel]
(
    @numItemCode NUMERIC,
	@numWareHouseItemID NUMERIC(18,0)
)
RETURNS @PriceBookRules table
(
	intFromQty int,intToQty int,vcRuleType VARCHAR(50),vcDiscountType VARCHAR(50),
	decDiscount DECIMAL(18,2),monPriceLevelPrice MONEY,vcPriceLevelType VARCHAR(50)
)                               
AS 
BEGIN


	DECLARE @monListPrice AS MONEY = 0
	DECLARE @monWListPrice AS MONEY = 0
	DECLARE @monVendorCost AS MONEY = 0
	DECLARE @numDomainID AS NUMERIC(18,0)
	DECLARE @numItemClassification NUMERIC(18,0)

	SELECT @numDomainID=numDomainID, @numItemClassification=numItemClassification, @monListPrice=monListPrice FROM Item WHERE numitemCode=@numItemCode

	IF ((@numWareHouseItemID>0) AND EXISTS(SELECT * FROM item WHERE numItemCode=@numItemCode and  charItemType='P'))      
	BEGIN      
		SELECT 
			@monWListPrice=isnull(monWListPrice,0) 
		FROM 
			WareHouseItems 
		WHERE 
			numWareHouseItemID=@numWareHouseItemID      
		
		IF ISNULL(@monWListPrice,0) > 0 
			SET @monListPrice = @monWListPrice
	END      



	SELECT @monVendorCost = dbo.[fn_GetVendorCost](@numItemCode)


    INSERT INTO @PriceBookRules
	SELECT 
		[intFromQty]
		,[intToQty]
		,CASE tintRuleType 
			WHEN 1 THEN 'Deduct from List price'
			WHEN 2 THEN 'Add to Primary Vendor Cost'
			WHEN 3 THEN 'Named Price' 
		END vcRuleType
		,CASE 
			WHEN (tintRuleType=1 OR tintRuleType=2) AND tintDiscountType=1 THEN 'Percentage'
			WHEN (tintRuleType=1 OR tintRuleType=2) AND tintDiscountType=2 THEN 'Flat'
			ELSE '' 
		END vcDiscountType
		,CASE WHEN tintRuleType=3 THEN 0 ELSE decDiscount END AS decDiscount
		,CASE WHEN tintRuleType=1 AND tintDiscountType=1 --Deduct from List price & Percentage
					THEN @monListPrice - (@monListPrice * ( decDiscount /100))
                 WHEN tintRuleType=1 AND tintDiscountType=2 --Deduct from List price & Flat discount
					THEN @monListPrice - decDiscount
                 WHEN tintRuleType=2 AND tintDiscountType=1 --Add to Primary Vendor Cost & Percentage
					THEN @monVendorCost + (@monVendorCost * ( decDiscount /100))
                 WHEN tintRuleType=2 AND tintDiscountType=2 --Add to Primary Vendor Cost & Flat discount
					THEN @monVendorCost + decDiscount
                 WHEN tintRuleType=3 --Named Price
					THEN decDiscount
                END AS monPriceLevelPrice,'Item Price Level'											
        FROM    [PricingTable]
        WHERE   ISNULL(numItemCode,0)=@numItemCode
        ORDER BY [numPricingID] 


	declare @numRelationship as numeric(9);SET @numRelationship=0                
	declare @numProfile as numeric(9);SET @numProfile=0  
	declare @numDivisionID as numeric(9);SET @numDivisionID=0                
                

INSERT INTO @PriceBookRules
	SELECT PT.[intFromQty],PT.[intToQty],CASE PT.tintRuleType WHEN 1 THEN 'Deduct from List price'
												WHEN 2 THEN 'Add to Primary Vendor Cost'
												WHEN 3 THEN 'Named Price' END vcRuleType,
	CASE WHEN (PT.tintRuleType=1 OR PT.tintRuleType=2) AND PT.tintDiscountType=1 THEN 'Percentage'
		 WHEN (PT.tintRuleType=1 OR PT.tintRuleType=2) AND PT.tintDiscountType=2 THEN 'Flat'
		ELSE '' END vcDiscountType,CASE WHEN PT.tintRuleType=3 THEN 0 ELSE PT.decDiscount END AS decDiscount,
		CASE WHEN PT.tintRuleType=1 AND PT.tintDiscountType=1 --Deduct from List price & Percentage
					THEN @monListPrice - (@monListPrice * ( PT.decDiscount /100))
                 WHEN PT.tintRuleType=1 AND PT.tintDiscountType=2 --Deduct from List price & Flat discount
					THEN @monListPrice - PT.decDiscount
                 WHEN PT.tintRuleType=2 AND PT.tintDiscountType=1 --Add to Primary Vendor Cost & Percentage
					THEN @monVendorCost + (@monVendorCost * ( PT.decDiscount /100))
                 WHEN PT.tintRuleType=2 AND PT.tintDiscountType=2 --Add to Primary Vendor Cost & Flat discount
					THEN @monVendorCost + PT.decDiscount
                 WHEN PT.tintRuleType=3 --Named Price
					THEN PT.decDiscount
                END AS monPriceLevelPrice,'Price Rule'	
FROM   PriceBookRules P 
       LEFT JOIN PriceBookRuleDTL PDTL ON P.numPricRuleID = PDTL.numRuleID
         LEFT JOIN PriceBookRuleItems PBI ON P.numPricRuleID = PBI.numRuleID
        LEFT JOIN [PriceBookPriorities] PP ON PP.[Step2Value] = P.[tintStep2] AND PP.[Step3Value] = P.[tintStep3]
        JOIN [PricingTable] PT ON P.numPricRuleID = PT.numPriceRuleID
WHERE   P.numDomainID = @numDomainID AND P.tintRuleFor=1 AND P.tintPricingMethod = 1
AND 
(
((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 1 AND PDTL.numValue = @numDivisionID)) -- Priority 1
OR ((tintStep2 = 2 AND PBI.numValue = @numItemClassification) AND (tintStep3= 1 AND PDTL.numValue = @numDivisionID)) -- Priority 2
OR ((tintStep2 = 3) AND (tintStep3 = 1 AND PDTL.numValue = @numDivisionID)) -- Priority 3

OR ((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 4
OR ((tintStep2 = 2 AND PBI.numValue = @numItemClassification) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 5
OR ((tintStep2 = 3) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 6

OR ((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 3)) -- Priority 7
OR ((tintStep2 = 2 AND PBI.numValue = @numItemClassification) AND (tintStep3 = 3)) -- Priority 8
OR ((tintStep2 = 3) and (tintStep3 = 3)) -- Priority 9
)
ORDER BY PP.Priority ASC

Return
   END

GO


GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_GetItemPriceLevelDetail')
DROP FUNCTION fn_GetItemPriceLevelDetail
GO
CREATE FUNCTION [dbo].[fn_GetItemPriceLevelDetail] 
(
	@numDomainID NUMERIC(18,0)
	,@numItemCode NUMERIC(18,0)
	,@numWarehouseID NUMERIC(18,0)

)
RETURNS VARCHAR(MAX)
AS
BEGIN
	DECLARE @vcValue AS VARCHAR(MAX) = ''
   
	DECLARE @numWareHouseItemID AS BIGINT
	SELECT TOP 1 @numWareHouseItemID = ISNULL(numWareHouseItemID,0) FROM dbo.WareHouseItems WHERE numWareHouseID = @numWareHouseID AND numItemID = @numItemCode AND numWLocationID <> -1

	IF @numWareHouseItemID > 0
	BEGIN
		SELECT 
			@vcValue = CONCAT('<b>Warehouse Price</b><br/>',ISNULL(W.vcWareHouse,''),' - ',ISNULL(WI.monWListPrice,0))
		FROM    
			Item I
		LEFT JOIN 
			dbo.WareHouseItems WI 
		ON 
			WI.numItemID = I.numItemCode AND WI.numDomainID = I.numDomainID
		LEFT JOIN 
			dbo.Warehouses W 
		ON 
			W.numWareHouseID = WI.numWareHouseID
		WHERE 
			ISNULL(I.numItemCode,0) = @numItemCode 
			AND (W.numWareHouseID = ISNULL(@numWareHouseID,0) OR ISNULL(@numWareHouseID,0) = 0) 
			AND numWLocationID <> -1
	END

	DECLARE @monListPrice AS MONEY = 0
	DECLARE @monVendorCost AS MONEY = 0

	

	IF @numWareHouseItemID > 0    
	BEGIN      
		SELECT @monListPrice=ISNULL(monWListPrice,0) FROM WareHouseItems WHERE numWareHouseItemID=@numWareHouseItemID     
	END      
	ELSE      
	BEGIN      
		SELECT @monListPrice=monListPrice FROM Item WHERE numItemCode=@numItemCode      
	END 

	SELECT @monVendorCost = dbo.[fn_GetVendorCost](@numItemCode)


	-- PRICE TABLE
	IF EXISTS (SELECT * FROM PricingTable WHERE numItemCode = @numItemCode)
	BEGIN
		DECLARE @listPriceTable VARCHAR(MAX)

		SELECT
			@listPriceTable = COALESCE(@listPriceTable+', ' ,'') + 
			CONCAT(CAST(CASE WHEN tintRuleType=1 AND tintDiscountType=1 --Deduct from List price & Percentage
				THEN @monListPrice - (@monListPrice * ( decDiscount /100))
			 WHEN tintRuleType=1 AND tintDiscountType=2 --Deduct from List price & Flat discount
				THEN @monListPrice - decDiscount
			 WHEN tintRuleType=2 AND tintDiscountType=1 --Add to Primary Vendor Cost & Percentage
				THEN @monVendorCost + (@monVendorCost * ( decDiscount /100))
			 WHEN tintRuleType=2 AND tintDiscountType=2 --Add to Primary Vendor Cost & Flat discount
				THEN @monVendorCost + decDiscount
			 WHEN tintRuleType=3 --Named Price
				THEN decDiscount
			END AS MONEY), ' (',intFromQty,'-',intToQty,')')
        FROM
			PricingTable PT
		WHERE 
			ISNULL(PT.numItemCode,0)=@numItemCode
		ORDER BY 
			[numPricingID]


		IF LEN(@listPriceTable) > 0
		BEGIN
			SET @vcValue = CONCAT(@vcValue,'<br/><b>Price Table</b><br/>',@listPriceTable)
		END
	END

	IF EXISTS 
	(
		SELECT 
			*
		FROM    
			Item I
		JOIN PriceBookRules P ON I.numDomainID = P.numDomainID
		LEFT JOIN PriceBookRuleDTL PDTL ON P.numPricRuleID = PDTL.numRuleID
		LEFT JOIN PriceBookRuleItems PBI ON P.numPricRuleID = PBI.numRuleID
		LEFT JOIN [PriceBookPriorities] PP ON PP.[Step2Value] = P.[tintStep2] AND PP.[Step3Value] = P.[tintStep3]
		JOIN [PricingTable] PT ON P.numPricRuleID = PT.numPriceRuleID    
		WHERE   
			I.numItemCode = @numItemCode 
			AND P.tintRuleFor=1 
			AND P.tintPricingMethod = 1
			AND 
			(
				((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 3)) -- Priority 7
				OR ((tintStep2 = 2 AND PBI.numValue = I.numItemClassification) AND (tintStep3 = 3)) -- Priority 8
				OR ((tintStep2 = 3) and (tintStep3 = 3)) -- Priority 9
			)
	)
	BEGIN
		DECLARE @listPriceRule VARCHAR(MAX)

		SELECT  
			@listPriceRule = COALESCE(@listPriceRule+', ' ,'') + 
			CONCAT(CAST(CASE WHEN PT.tintRuleType=1 AND PT.tintDiscountType=1 --Deduct from List price & Percentage
						THEN @monListPrice - (@monListPrice * ( PT.decDiscount /100))
					 WHEN PT.tintRuleType=1 AND PT.tintDiscountType=2 --Deduct from List price & Flat discount
						THEN @monListPrice - PT.decDiscount
					 WHEN PT.tintRuleType=2 AND PT.tintDiscountType=1 --Add to Primary Vendor Cost & Percentage
						THEN @monVendorCost + (@monVendorCost * ( PT.decDiscount /100))
					 WHEN PT.tintRuleType=2 AND PT.tintDiscountType=2 --Add to Primary Vendor Cost & Flat discount
						THEN @monVendorCost + PT.decDiscount END AS MONEY),' (',PT.intFromQty,'-',PT.intToQty,')')
		FROM    
			Item I
			JOIN PriceBookRules P ON I.numDomainID = P.numDomainID
			LEFT JOIN PriceBookRuleDTL PDTL ON P.numPricRuleID = PDTL.numRuleID
			LEFT JOIN PriceBookRuleItems PBI ON P.numPricRuleID = PBI.numRuleID
			LEFT JOIN [PriceBookPriorities] PP ON PP.[Step2Value] = P.[tintStep2] AND PP.[Step3Value] = P.[tintStep3]
			LEFT JOIN [PricingTable] PT ON P.numPricRuleID = PT.numPriceRuleID
		WHERE   
			I.numItemCode = @numItemCode 
			AND P.tintRuleFor=1 
			AND P.tintPricingMethod = 1
			AND 
			(
				((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 3)) -- Priority 7
				OR ((tintStep2 = 2 AND PBI.numValue = I.numItemClassification) AND (tintStep3 = 3)) -- Priority 8
				OR ((tintStep2 = 3) and (tintStep3 = 3)) -- Priority 9
			)
		ORDER BY 
			PP.Priority ASC

		IF LEN(@listPriceRule) > 0
		BEGIN
			SET @vcValue = CONCAT(@vcValue,'<br/><b>Price Rule</b><br/>',@listPriceRule)
		END
	END

   RETURN @vcValue
END
GO

/****** Object:  UserDefinedFunction [dbo].[GetAccountTypeCode]    Script Date: 09/25/2009 16:11:52 ******/

GO

GO
-- select dbo.GetAccountTypeCode(72,124,1) 
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='GetAccountTypeCode')
DROP FUNCTION GetAccountTypeCode
GO
CREATE FUNCTION	[dbo].[GetAccountTypeCode]
(@numDomainID NUMERIC(9),@numParentID NUMERIC(9),@tintMode TINYINT=0)
returns varchar(50)
AS 
BEGIN
	DECLARE @numAccountTypeID NUMERIC(9)
	DECLARE @vcMaxAccountTypeCode VARCHAR(50)
	DECLARE @vcMaxCharOfAccountCode VARCHAR(50)
	DECLARE @vcNewAccountCode VARCHAR(50)
	DECLARE @vcParentAccountCode VARCHAR(50)

IF @tintMode = 0 OR @tintMode = 1 --0: Get Account code for Account Type, 1:Get new Account code for Chart of Account on change of parent account type 
	BEGIN
		SELECT @numAccountTypeID =[numAccountTypeID],@vcParentAccountCode=[vcAccountCode] FROM [AccountTypeDetail] WHERE [numDomainID]=@numDomainID AND numAccountTypeID =@numParentID
		
		IF (SELECT COUNT(*) FROM  [AccountTypeDetail] WHERE numDomainID=@numDomainID AND  [numParentID]=@numAccountTypeID) > 0 OR (SELECT COUNT(*) FROM [Chart_Of_Accounts] WHERE numDomainID=@numDomainID AND numParntAcntTypeID=@numParentID) > 0
		BEGIN
			SELECT  TOP 1 @vcMaxAccountTypeCode = @vcParentAccountCode + REPLACE( STR(isnull(CAST(SUBSTRING([vcAccountCode],LEN([vcAccountCode])-1,LEN([vcAccountCode])) AS INT),0) + 1 ,2) , ' ', '0')
			FROM [AccountTypeDetail] WHERE numDomainID=@numDomainID AND [numParentID]=@numAccountTypeID ORDER BY [numAccountTypeID] DESC

			SELECT  TOP 1 @vcMaxCharOfAccountCode = @vcParentAccountCode + REPLACE( STR(isnull(CAST(SUBSTRING([vcAccountCode],LEN([vcAccountCode])-1,LEN([vcAccountCode])) AS INT),0) + 1,2) , ' ', '0')
				FROM [Chart_Of_Accounts] WHERE numDomainID=@numDomainID AND numParntAcntTypeID=@numParentID AND isnull(bitIsSubAccount,0) = 0 ORDER BY [vcAccountCode] DESC

			SET @vcNewAccountCode = (CASE WHEN ISNULL(@vcMaxCharOfAccountCode,0) > ISNULL(@vcMaxAccountTypeCode,'0') THEN @vcMaxCharOfAccountCode ELSE  @vcMaxAccountTypeCode END)
		END
		ELSE
		BEGIN
			SELECT  @vcNewAccountCode = @vcParentAccountCode + '01'
		END
END 
IF @tintMode = 2 --select code from AccounttypeDetail table
BEGIN
		SELECT @vcNewAccountCode=[vcAccountCode] FROM [AccountTypeDetail]  WHERE numDomainID=@numDomainID AND [numAccountTypeID]=@numParentID
END 

IF @tintMode = 3 --Get new Account code for Chart of Account on change of IsSubAccount flag from 0 to 1 or 1 to 0
BEGIN
		SELECT @vcParentAccountCode=[vcAccountCode] FROM [Chart_Of_Accounts]
		WHERE [numDomainID]=@numDomainID AND numAccountId=@numParentID
				
			--RETURN  @vcParentAccountCode
		IF (SELECT COUNT(*) FROM  [Chart_Of_Accounts] WHERE numDomainID=@numDomainID AND numParentAccId=@numParentID) >0
			BEGIN
				
				SELECT  TOP 1 @vcNewAccountCode = @vcParentAccountCode + REPLACE( STR(isnull(CAST(SUBSTRING([vcAccountCode],LEN([vcAccountCode])-1,LEN([vcAccountCode])) AS INT),0) + 1 ,2) , ' ', '0')
				FROM [Chart_Of_Accounts] WHERE numDomainID=@numDomainID AND numParentAccId=@numParentID ORDER BY [vcAccountCode] DESC
			END
		ELSE
			BEGIN
				SELECT  @vcNewAccountCode = @vcParentAccountCode + '01' --REPLACE( STR(1 ,2) , ' ', '0')
					--			FROM [AccountTypeDetail] WHERE numAccountTypeID = @numParentID
		END
END 	

RETURN ISNULL(@vcNewAccountCode,'')
END
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='GetCommissionContactItemList')
DROP FUNCTION GetCommissionContactItemList
GO
CREATE FUNCTION dbo.GetCommissionContactItemList
    (
      @numRuleID NUMERIC,
      @tintMode TINYINT 
    )
RETURNS VARCHAR(2000)
AS BEGIN
    DECLARE @ItemList VARCHAR(2000)

	IF @tintMode = 0
        BEGIN
			DECLARE @tintAssignTo AS TINYINT

			SELECT @tintAssignTo=tintAssignTo FROM CommissionRules WHERE numComRuleID=@numRuleID

			IF @tintAssignTo = 3
			BEGIN
				SELECT 
					@ItemList = COALESCE(@ItemList + ', ', '') + CONCAT(D.vcPartnerCode,'-',C.vcCompanyName,' (Partner)')
				FROM 
					CommissionRuleContacts CC 
				INNER JOIN
					DivisionMaster D
				ON
					CC.numValue = D.numDivisionID
				INNER JOIN
					CompanyInfo C
				ON
					D.numCompanyID =C.numCompanyId
				WHERE 
					CC.numComRuleID=@numRuleID
			END
			ELSE
			BEGIN
				SELECT TOP 10
						@ItemList = COALESCE(@ItemList + ', ', '') + CASE CC.bitCommContact WHEN 1 THEN (SELECT vcCompanyName FROM DivisionMaster D join CompanyInfo C on C.numCompanyID=D.numCompanyID  where D.numDivisionID=CC.numValue) + '(Commission Contact)' ELSE dbo.fn_GetContactName(numValue) END
				 FROM    [CommissionRuleContacts] CC
						--INNER JOIN AdditionalContactsInformation A ON  CC.numValue=A.numContactID
				WHERE   CC.[numComRuleID] = @numRuleID
			END
        END
        
    IF @tintMode = 1 
        BEGIN

            SELECT TOP 10
                    @ItemList = COALESCE(@ItemList + ', ', '') + vcItemName
            FROM    [CommissionRuleItems] CI
                    INNER JOIN Item I ON I.[numItemCode] = CI.[numValue]
            WHERE   CI.[numComRuleID] = @numRuleID
        END
      
      IF @tintMode = 2
        BEGIN

            SELECT TOP 10
                    @ItemList = COALESCE(@ItemList + ', ', '') + vcData
            FROM    [CommissionRuleItems] CI
                    INNER JOIN [ListDetails] LI ON LI.[numListItemID] = CI.[numValue]
            WHERE   CI.[numComRuleID] = @numRuleID
        END
       IF @tintMode = 3
        BEGIN

            SELECT TOP 10
                    @ItemList = COALESCE(@ItemList + ', ', '') + C.[vcCompanyName]
            FROM    [CommissionRuleOrganization] PD
                    INNER JOIN [DivisionMaster] DM ON DM.[numDivisionID] = PD.[numValue]
                    LEFT OUTER JOIN [CompanyInfo] C ON C.[numCompanyId] = DM.[numCompanyID]
                    LEFT OUTER JOIN [CommissionRules] PB ON PB.[numComRuleID] = PD.[numComRuleID]
            WHERE   PB.tintComOrgType = 1
                    AND PB.[numComRuleID] = @numRuleID
        END
        
        IF @tintMode = 4
        BEGIN

             SELECT TOP 10
                    @ItemList = COALESCE(@ItemList + ', ', '') + (LI.vcData +'/' + LI1.vcData)
            FROM    [CommissionRuleOrganization] PD
                    INNER JOIN [ListDetails] LI ON LI.[numListItemID] = PD.[numValue]
                    Inner JOIN [ListDetails] LI1 ON LI1.[numListItemID] = PD.[numProfile]
                    LEFT OUTER JOIN [CommissionRules] PB ON PB.[numComRuleID] = PD.[numComRuleID]
            WHERE   PB.tintComOrgType = 2
                    AND PB.[numComRuleID] = @numRuleID
        END
        
    RETURN ISNULL(@ItemList, '')
   END 

--- Created By Anoop jayaraj     
--- Modified By Gangadhar 03/05/2008                                                        

--Select all records  with 
--Rule 1. ANY relationship with a Sales Order. 
--Rule 2. Relationships that are Customers, that have been entered as Accounts from the UI or Record Import, or ..... 
GO
SET ANSI_NULLS on
GO
SET QUOTED_IDENTIFIER on
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_accountlist1')
DROP PROCEDURE usp_accountlist1
GO
CREATE PROCEDURE [dbo].[USP_AccountList1]                                                              
		@CRMType numeric,                                                              
		@numUserCntID numeric,                                                              
		@tintUserRightType tinyint,                                                              
		@tintSortOrder numeric=4,                                                                                  
		@SortChar char(1)='0',  
		@numDomainID as numeric(9)=0,                                                       
		@CurrentPage int,                                                            
		@PageSize int,        
		 @TotRecs int output,                                                           
		@columnName as Varchar(MAX),                                                            
		@columnSortOrder as Varchar(10)    ,                    
		@numProfile as numeric   ,                  
		@bitPartner as BIT,       
		@ClientTimeZoneOffset as int,
		@bitActiveInActive as bit,
		@vcRegularSearchCriteria varchar(MAX)='',
		@vcCustomSearchCriteria varchar(MAX)=''   ,
		@SearchText VARCHAR(100) = ''
AS                                                            
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
           
	DECLARE @tintPerformanceFilter AS TINYINT

	IF PATINDEX('%cmp.vcPerformance=1%', @vcRegularSearchCriteria)>0 --Last 3 Months
	BEGIN
		SET @tintPerformanceFilter = 1
	END
	ELSE IF PATINDEX('%cmp.vcPerformance=2%', @vcRegularSearchCriteria)>0 --Last 6 Months
	BEGIN
		SET @tintPerformanceFilter = 2
	END
	ELSE IF PATINDEX('%cmp.vcPerformance=3%', @vcRegularSearchCriteria)>0 --Last 1 Year
	BEGIN
		SET @tintPerformanceFilter = 3
	END
	ELSE
	BEGIN
		SET @tintPerformanceFilter = 0
	END

	IF CHARINDEX('AD.numBillCountry',@vcRegularSearchCriteria) > 0
	BEGIN
		SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'AD.numBillCountry','AD1.numCountry')
	END
	ELSE IF CHARINDEX('AD.numShipCountry',@vcRegularSearchCriteria) > 0
	BEGIN
		SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'AD.numShipCountry','AD2.numCountry')
	END
	ELSE IF CHARINDEX('AD.vcBillCity',@vcRegularSearchCriteria) > 0
	BEGIN
		SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'AD.vcBillCity','AD5.vcCity')
	END
	ELSE IF CHARINDEX('AD.vcShipCity',@vcRegularSearchCriteria) > 0
	BEGIN
		SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'AD.vcShipCity','AD6.vcCity')
	END

	

	CREATE TABLE #tempForm 
	(
		tintOrder TINYINT,vcDbColumnName nvarchar(50),vcOrigDbColumnName nvarchar(50),vcFieldName nvarchar(50),vcAssociatedControlType nvarchar(50),vcListItemType varchar(3),
		numListID numeric(9),vcLookBackTableName varchar(50),bitCustomField BIT,numFieldId NUMERIC,bitAllowSorting BIT,bitAllowEdit BIT,bitIsRequired BIT,bitIsEmail BIT,
		bitIsAlphaNumeric BIT,bitIsNumeric BIT,bitIsLengthValidation BIT,intMaxLength int,intMinLength int,bitFieldMessage BIT,vcFieldMessage VARCHAR(500),ListRelID numeric(9),
		intColumnWidth INT,bitAllowFiltering BIT,vcFieldDataType CHAR(1)
	)   

	DECLARE @Nocolumns AS TINYINT = 0                

	SELECT 
		@Nocolumns =ISNULL(SUM(TotalRow),0)  
	FROM
	(            
		SELECT 
			COUNT(*) TotalRow 
		FROM 
			View_DynamicColumns 
		WHERE 
			numFormId=36 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND numRelCntType=2 
		UNION 
		SELECT 
			COUNT(*) TotalRow 
		FROM 
			View_DynamicCustomColumns 
		WHERE 
			numFormId=36 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND numRelCntType=2
	) TotalRows

	DECLARE @IsMasterConfAvailable BIT = 0
	DECLARE @numUserGroup NUMERIC(18,0)

	IF @Nocolumns=0
	BEGIN
		SELECT @numUserGroup = numGroupID FROM UserMaster WHERE numUserDetailId = @numUserCntID 

		IF (SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = @numDomainID AND numFormID = 36 AND numRelCntType=2 AND bitGridConfiguration = 1 AND numGroupID = @numUserGroup) > 0
		BEGIN
			SET @IsMasterConfAvailable = 1
		END
	END


	--If MasterConfiguration is available then load it otherwise load default columns
	IF @IsMasterConfAvailable = 1
	BEGIN
		INSERT INTO #tempForm
		SELECT 
			(intRowNum + 1) as tintOrder, vcDbColumnName,	vcOrigDbColumnName,	ISNULL(vcCultureFieldName,vcFieldName), vcAssociatedControlType,
			vcListItemType, numListID, vcLookBackTableName,	bitCustom, numFieldId, bitAllowSorting,	bitInlineEdit, bitIsRequired,
			bitIsEmail,	bitIsAlphaNumeric, bitIsNumeric, bitIsLengthValidation,	intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,
			ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType 
		FROM 
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId=36 AND 
			View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicColumnsMasterConfig.numRelCntType=2 AND 
			View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
			ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
		UNION
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName, vcAssociatedControlType,'' as vcListItemType,numListID,'',
			bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,''
		FROM 
			View_DynamicCustomColumnsMasterConfig
		WHERE 
			View_DynamicCustomColumnsMasterConfig.numFormId=36 AND 
			View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicCustomColumnsMasterConfig.numRelCntType=2 AND 
			View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1
		ORDER BY 
			tintOrder ASC  
	END
	ELSE                                        
	BEGIN
		if @Nocolumns=0
		BEGIN
			INSERT INTO DycFormConfigurationDetails 
			(
				numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth
			)
			SELECT 
				36,numFieldId,0,Row_number() over(order by tintRow desc),@numDomainID,@numUserCntID,2,1,0,intColumnWidth
			FROM 
				View_DynamicDefaultColumns
			WHERE 
				numFormId=36 
				AND bitDefault=1 
				AND ISNULL(bitSettingField,0)=1 
				AND numDomainID=@numDomainID
			ORDER BY 
				tintOrder ASC   
		END
          
		INSERT INTO 
			#tempForm
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcOrigDbColumnName,ISNULL(vcCultureFieldName,vcFieldName),vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName,
			bitCustom,numFieldId,bitAllowSorting,bitInlineEdit,bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength,
			bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID ,intColumnWidth,bitAllowFiltering,vcFieldDataType
		FROM 
			View_DynamicColumns 
		WHERE 
			numFormId=36 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND numRelCntType=2
			AND ISNULL(bitSettingField,0)=1 
			AND ISNULL(bitCustom,0)=0
		UNION
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName,vcAssociatedControlType,'' as vcListItemType,numListID,'',bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,''
		FROM 
			View_DynamicCustomColumns
		WHERE 
			numFormId=36 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND numRelCntType=2
			AND ISNULL(bitCustom,0)=1
		ORDER BY 
			tintOrder asc  
	END


	DECLARE  @strColumns AS VARCHAR(MAX) = ''
	set @strColumns=' isnull(ADC.numContactId,0)numContactId,isnull(DM.numDivisionID,0)numDivisionID,isnull(DM.numTerID,0)numTerID,isnull(ADC.numRecOwner,0)numRecOwner,isnull(DM.tintCRMType,0) as tintCRMType,ADC.vcGivenName'   

	--Custom field 
	DECLARE @tintOrder AS TINYINT = 0                                                 
	DECLARE @vcFieldName AS VARCHAR(50)                                                  
	DECLARE @vcListItemType AS VARCHAR(3)                                             
	DECLARE @vcAssociatedControlType VARCHAR(20)     
	declare @numListID AS numeric(9)
	DECLARE @vcDbColumnName VARCHAR(20)                      
	DECLARE @WhereCondition VARCHAR(MAX) = ''                   
	DECLARE @vcLookBackTableName VARCHAR(2000)                
	DECLARE @bitCustom AS BIT
	DECLARE @bitAllowEdit AS CHAR(1)                   
	Declare @numFieldId as numeric  
	DECLARE @bitAllowSorting AS CHAR(1)    
	DECLARE @vcColumnName AS VARCHAR(500)             
	Declare @ListRelID as numeric(9) 
	DECLARE @SearchQuery AS VARCHAR(MAX) = '' 
	   
    SELECT TOP 1 
		@tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName,@vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,
		@numListID=numListID,@vcLookBackTableName=vcLookBackTableName,@bitCustom=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,
		@bitAllowEdit=bitAllowEdit,@ListRelID=ListRelID
	FROM  
		#tempForm 
	ORDER BY 
		tintOrder ASC            
                             
	WHILE @tintOrder>0                                                  
	BEGIN
		IF @bitCustom = 0                
		BEGIN
			DECLARE @Prefix AS VARCHAR(5)
			
			IF @vcLookBackTableName = 'AdditionalContactsInformation' 
				SET @Prefix = 'ADC.'
			IF @vcLookBackTableName = 'DivisionMaster' 
				SET @Prefix = 'DM.'
	
			SET @vcColumnName= @vcDbColumnName+'~'+ convert(varchar(10),@numFieldId) +'~0'

	
			IF @vcAssociatedControlType='SelectBox' or @vcAssociatedControlType='ListBox'                  
			BEGIN
				IF @vcDbColumnName = 'numPartenerSource'
				BEGIN
					SET @strColumns=@strColumns+ ' ,(DM.numPartenerSource) '+' ['+ @vcColumnName+']'    
					SET @strColumns=@strColumns+ ' ,(SELECT D.vcPartnerCode+''-''+C.vcCompanyName FROM DivisionMaster AS D LEFT JOIN CompanyInfo AS C ON D.numCompanyID=C.numCompanyID WHERE D.numDivisionID=DM.numPartenerSource) AS vcPartenerSource' 
				END
				ELSE IF @vcListItemType='LI'                                                         
				BEGIN
					IF @numListID=40 
					BEGIN
						SET @strColumns = @strColumns + ',L' + CONVERT(VARCHAR(3), @tintOrder) + '.vcData' + ' [' + @vcColumnName + ']'  
						                                                 
						IF LEN(@SearchText) > 0
						BEGIN 
							SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'L' + CONVERT(VARCHAR(3),@tintOrder) + '.vcData LIKE ''%' + @SearchText + '%'' '     
						END                              
						IF @vcDbColumnName='numShipCountry'
						BEGIN
							SET @WhereCondition = @WhereCondition
												+ ' left Join AddressDetails AD2 on AD2.numRecordId= DM.numDivisionID and AD2.tintAddressOf=2 and AD2.tintAddressType=2 AND AD2.bitIsPrimary=1 and AD2.numDomainID= DM.numDomainID '
												+ ' left Join ListDetails L' + CONVERT(VARCHAR(3), @tintOrder) + ' on L' + CONVERT(VARCHAR(3), @tintOrder) + '.numListItemID=AD2.numCountry '
						END
						ELSE
						BEGIN
							SET @WhereCondition = @WhereCondition
											+ ' left Join AddressDetails AD1 on AD1.numRecordId= DM.numDivisionID and AD1.tintAddressOf=2 and AD1.tintAddressType=1 AND AD1.bitIsPrimary=1 and AD1.numDomainID= DM.numDomainID '
											+ ' left Join ListDetails L' + CONVERT(VARCHAR(3), @tintOrder) + ' on L' + CONVERT(VARCHAR(3), @tintOrder) + '.numListItemID=AD1.numCountry '
						END
					 END
					ELSE
					BEGIN
						SET @strColumns=@strColumns+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName+']'   
						
						IF LEN(@SearchText) > 0
						BEGIN 
							SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'L' + CONVERT(VARCHAR(3),@tintOrder) + '.vcData LIKE ''%' + @SearchText + '%'' '          
						END  
						                                                        
						SET @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID='+@vcDbColumnName                                                        
					 END
				END                                                        
				ELSE IF @vcListItemType='S'                                                         
				BEGIN 
					
					                                                           
					IF LEN(@SearchText) > 0
					BEGIN 
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'S' + CONVERT(VARCHAR(3), @tintOrder) + '.vcState LIKE ''%' + @SearchText + '%'' '     
					END
					      						
					IF @vcDbColumnName='numShipState'
					BEGIN
						SET @strColumns = @strColumns + ',ShipState.vcState' + ' [' + @vcColumnName + ']' 
						SET @WhereCondition = @WhereCondition
							+ ' left Join AddressDetails AD3 on AD3.numRecordId= DM.numDivisionID and AD3.tintAddressOf=2 and AD3.tintAddressType=2 AND AD3.bitIsPrimary=1 and AD3.numDomainID= DM.numDomainID '
							+ ' left Join State ShipState on ShipState.numStateID=AD3.numState '
					END
					ELSE IF @vcDbColumnName='numBillState'
					BEGIN
						SET @strColumns = @strColumns + ',BillState.vcState' + ' [' + @vcColumnName + ']' 
						SET @WhereCondition = @WhereCondition
							+ ' left Join AddressDetails AD4 on AD4.numRecordId= DM.numDivisionID and AD4.tintAddressOf=2 and AD4.tintAddressType=1 AND AD4.bitIsPrimary=1 and AD4.numDomainID= DM.numDomainID '
							+ ' left Join State BillState on BillState.numStateID=AD4.numState '
					END
					ELSE  
					BEGIN
					SET @strColumns = @strColumns + ',S' + CONVERT(VARCHAR(3), @tintOrder) + '.vcState' + ' [' + @vcColumnName + ']' 
						SET @WhereCondition = @WhereCondition + ' left join State S' + CONVERT(VARCHAR(3), @tintOrder) + ' on S' + CONVERT(VARCHAR(3), @tintOrder) + '.numStateID=' + @vcDbColumnName        
					END		                                           
				END                                                        
				ELSE IF @vcListItemType='T'                                                         
				BEGIN
					SET @strColumns=@strColumns+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName+']'   

					IF LEN(@SearchText) > 0
					BEGIN 
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'L' + CONVERT(VARCHAR(3), @tintOrder) + '.vcState LIKE ''%' + @SearchText + '%'' '     
					END                                                           
				  
					SET @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID='+@vcDbColumnName                                                        
				END  
				ELSE IF @vcListItemType='C'
				BEGIN
					SET @strColumns=@strColumns+',C'+ convert(varchar(3),@tintOrder)+'.vcCampaignName'+' ['+ @vcColumnName+']' 
					 
					IF LEN(@SearchText) > 0
					BEGIN 
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'C' + CONVERT(VARCHAR(3),@tintOrder) + '.vcCampaignName LIKE ''%' + @SearchText + '%'' '     
					END  
					                                                  
					SET @WhereCondition= @WhereCondition +' left Join CampaignMaster C'+ convert(varchar(3),@tintOrder)+ ' on C'+convert(varchar(3),@tintOrder)+ '.numCampaignId=DM.'+@vcDbColumnName                                                    
				END                       
				ELSE IF @vcListItemType='U'                                                     
				BEGIN
					SET @strColumns=@strColumns+',dbo.fn_GetContactName('+@Prefix+@vcDbColumnName+') ['+ @vcColumnName+']'

					IF LEN(@SearchText) > 0
					BEGIN 
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'dbo.fn_GetContactName(' + @Prefix + @vcDbColumnName + ') LIKE ''%' + @SearchText + '%'' '    
					END                                                          
				END  
				ELSE IF @vcListItemType='SYS'                                                         
				BEGIN
					SET @strColumns=@strColumns+',case tintCRMType when 1 then ''Prospect'' when 2 then ''Account'' else ''Lead'' end as ['+ @vcColumnName+']'                                                        

					IF LEN(@SearchText) > 0
					BEGIN 
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'case tintCRMType when 1 then ''Prospect'' when 2 then ''Account'' else ''Lead'' end LIKE ''%' + @SearchText + '%'' '    
					END         
				END                     
			END                                                        
			ELSE IF @vcAssociatedControlType='DateField'                                                    
			BEGIN
				 SET @strColumns=@strColumns+',case when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',' +@Prefix+@vcDbColumnName+') )= convert(varchar(11),getdate()) then ''<b><font color=red>Today</font></b>'''        
				 SET @strColumns=@strColumns+'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',' +@Prefix+@vcDbColumnName+') )= convert(varchar(11),dateadd(day,-1,getdate())) then ''<b><font color=purple>YesterDay</font></b>'''        
				 SET @strColumns=@strColumns+'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',' +@Prefix+@vcDbColumnName+') )= convert(varchar(11),dateadd(day,1,getdate())) then''<b><font color=orange>Tommorow</font></b>'' '        
				 SET @strColumns=@strColumns+'else dbo.FormatedDateFromDate(DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',' +@Prefix+@vcDbColumnName+'),'+convert(varchar(10),@numDomainId)+') end  ['+ @vcColumnName+']'                  
					
				IF LEN(@SearchText) > 0
				BEGIN
					SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + @Prefix + @vcDbColumnName + ' LIKE ''%' + @SearchText + '%'' '
				END
			END      
			ELSE IF @vcAssociatedControlType='TextBox' AND @vcDbColumnName='vcBillCity' 
			BEGIN
				SET @strColumns = @strColumns + ',AD5.vcCity' + ' [' + @vcColumnName + ']'   

				IF LEN(@SearchText) > 0
				BEGIN
					SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + ' AD5.vcCity LIKE ''%' + @SearchText + '%'' '
				END
				
				SET @WhereCondition = @WhereCondition
							+ ' left Join AddressDetails AD5 on AD5.numRecordId= DM.numDivisionID and AD5.tintAddressOf=2 and AD5.tintAddressType=1 AND AD5.bitIsPrimary=1 and AD5.numDomainID= DM.numDomainID '

			END
			ELSE IF @vcAssociatedControlType='TextBox' AND  @vcDbColumnName='vcShipCity'
			BEGIN
				SET @strColumns=@strColumns + ',AD6.vcCity' + ' [' + @vcColumnName + ']' 
				
				IF LEN(@SearchText) > 0
				BEGIN
					SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + ' AD6.vcCity LIKE ''%' + @SearchText + '%'' '
				END  
				
				SET @WhereCondition = @WhereCondition + ' left Join AddressDetails AD6 on AD6.numRecordId= DM.numDivisionID and AD6.tintAddressOf=2 and AD6.tintAddressType=2 AND AD6.bitIsPrimary=1 and AD6.numDomainID= DM.numDomainID '
			END
			ELSE IF @vcAssociatedControlType='Label' and @vcDbColumnName='vcLastSalesOrderDate'
			BEGIN
				SET @WhereCondition = @WhereCondition
												+ ' OUTER APPLY(' + CONCAT('SELECT MAX(bintCreatedDate) AS bintCreatedDate FROM OpportunityMaster WHERE numDomainID = ',@numDomainID,' AND numDivisionID=DM.numDivisionID AND tintOppType = 1 AND tintOppStatus = 1') + ' ) AS TempLastOrder '

				set @strColumns=@strColumns+','+ CONCAT('ISNULL(dbo.FormatedDateFromDate(TempLastOrder.bintCreatedDate,',@numDomainID,'),'''')') +' ['+ @vcColumnName+']'
			END	
			ELSE IF @vcAssociatedControlType='Label' and @vcDbColumnName='vcPerformance'
			BEGIN
				SET @WhereCondition = @WhereCondition
												+ CONCAT(' OUTER APPLY(SELECT
																	SUM(monDealAmount) AS monDealAmount
																	,AVG(ProfitPer) AS fltProfitPer
																FROM
																(
																	SELECT
																		OM.numDivisionId
																		,monDealAmount
																		,AVG(((ISNULL(OI.monPrice,0) - (CASE D.numCost WHEN 3 THEN ISNULL(V.monCost,0) * dbo.fn_UOMConversion(I.numBaseUnit,I.numItemCode,I.numDomainID,I.numPurchaseUnit) WHEN 2 THEN ISNULL(OI.numCost,0) ELSE ISNULL(I.monAverageCost,0) END)) / ISNULL(NULLIF(OI.monPrice,0),1)) * 100) ProfitPer
																	FROM
																		OpportunityMaster OM
																	INNER JOIN
																		Domain D
																	ON
																		OM.numDomainID = D.numDomainID
																	INNER JOIN
																		OpportunityItems OI
																	ON
																		OM.numOppId = OI.numOppId
																	INNER JOIN
																		Item I
																	ON
																		OI.numItemCode = I.numItemCode
																	LEFT JOIN
																		Vendor V
																	ON
																		V.numVendorID = I.numVendorID
																		AND V.numItemCode = I.numItemCode
																	WHERE
																		OM.numDomainId = ',@numDomainID,'
																		AND OM.numDivisionId = DM.numDivisionID
																		AND ISNULL(I.bitContainer,0) = 0
																		AND tintOppType = 1
																		AND tintOppStatus=1
																		AND 1 = (CASE ', @tintPerformanceFilter,' 
																				WHEN 1 THEN (CASE WHEN OM.bintCreatedDate BETWEEN DATEADD(MONTH,-3,GETUTCDATE()) AND GETUTCDATE() THEN 1 ELSE 0 END)
																				WHEN 2 THEN (CASE WHEN OM.bintCreatedDate BETWEEN DATEADD(MONTH,-6,GETUTCDATE()) AND GETUTCDATE() THEN 1 ELSE 0 END)
																				WHEN 3 THEN (CASE WHEN OM.bintCreatedDate BETWEEN DATEADD(YEAR,-1,GETUTCDATE()) AND GETUTCDATE() THEN 1 ELSE 0 END)
																				ELSE 1
																				END)
																	GROUP BY
																		OM.numDivisionId
																		,OM.numOppId
																		,monDealAmount
																) T1
																GROUP BY
																	T1.numDivisionId ) AS TempPerformance ')

				set @strColumns=@strColumns +', (CASE WHEN ISNULL(TempPerformance.monDealAmount,0) > 0 THEN CONCAT(FORMAT(TempPerformance.monDealAmount,''##,####.00''),'' ('',CAST(TempPerformance.fltProfitPer AS DECIMAL(18,2)),''%)'') ELSE '''' END) ['+ @vcColumnName+']'
			END	
			ELSE IF @vcAssociatedControlType='TextBox'  OR @vcAssociatedControlType='Label'                                                   
			BEGIN
				SET @strColumns=@strColumns+','
								+ case  
									when @vcDbColumnName='numAge' then 'year(getutcdate())-year(bintDOB)' 
									when @vcDbColumnName='numDivisionID' then 'DM.numDivisionID' 
									else @vcDbColumnName 
								end +' ['+ @vcColumnName+']'                
				
				IF LEN(@SearchText) > 0
				BEGIN
					SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 
					case  when @vcDbColumnName='numAge' then 'year(getutcdate())-year(bintDOB)' when      
				  @vcDbColumnName='numDivisionID' then 'DM.numDivisionID' else @vcDbColumnName end
					+' LIKE ''%' + @SearchText + '%'' '
				END  
			END   
		 	ELSE IF @vcAssociatedControlType='Popup' and @vcDbColumnName='numShareWith'                                                   
			BEGIN
				set @strColumns=@strColumns+',(SELECT SUBSTRING(
				(SELECT '','' + A.vcFirstName+'' ''+A.vcLastName + CASE WHEN ISNULL(SR.numContactType,0)>0 THEN ''('' + dbo.fn_GetListItemName(SR.numContactType) + '')'' ELSE '''' END
				FROM ShareRecord SR JOIN UserMaster UM ON UM.numUserDetailId=SR.numAssignedTo 
										 JOIN AdditionalContactsInformation A ON UM.numUserDetailId=A.numContactID 
					 WHERE SR.numDomainID=DM.numDomainID AND SR.numModuleID=1 AND SR.numRecordID=DM.numDivisionID
							AND UM.numDomainID=DM.numDomainID and UM.numDomainID=A.numDomainID FOR XML PATH('''')),2,200000)) ['+ @vcColumnName+']'                
			END  
			ELSE                                                 
			BEGIN
				 set @strColumns=@strColumns+','+ @vcDbColumnName +' ['+ @vcColumnName+']'
			END             
		END                
		ELSE IF @bitCustom = 1                
		BEGIN
			SET @vcColumnName= @vcDbColumnName +'~'+ convert(varchar(10),@numFieldId)+'~1'  
                
			SELECT 
				@vcFieldName=FLd_label,
				@vcAssociatedControlType=fld_type,
				@vcDbColumnName='Cust'+Convert(varchar(10),Fld_Id)                               
			FROM 
				CFW_Fld_Master
			WHERE 
				CFW_Fld_Master.Fld_Id = @numFieldId
			
			IF @vcAssociatedControlType = 'TextBox' or @vcAssociatedControlType = 'TextArea'            
			BEGIN
				SET @strColumns= @strColumns+',CFW'+ convert(VARCHAR(3),@tintOrder)+'.Fld_Value  ['+ @vcColumnName +']'                   
				
				SET @WhereCondition= @WhereCondition 
									+' left Join CFW_FLD_Values CFW'+ convert(varchar(3),@tintOrder)
									+ ' on CFW' + convert(varchar(3),@tintOrder) 
									+ '.Fld_Id='+convert(varchar(10),@numFieldId) 
									+ 'and CFW' + convert(varchar(3),@tintOrder) 
									+ '.RecId=DM.numDivisionId   '                                                         
			END   
			ELSE IF @vcAssociatedControlType = 'CheckBox'       
			BEGIN
				set @strColumns= @strColumns+',case when isnull(CFW' + CONVERT(VARCHAR(3), @tintOrder)+ '.Fld_Value,0)='''' then 0 else  isnull(CFW'
												+ CONVERT(VARCHAR(3), @tintOrder)+ '.Fld_Value,0) end ['+ @vcColumnName + ']'
 
				set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values CFW'+ convert(varchar(3),@tintOrder)+ '             
				on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=DM.numDivisionId   '                                                     
		   END                
			ELSE IF @vcAssociatedControlType = 'DateField'            
			BEGIN
				set @strColumns= @strColumns+',dbo.FormatedDateFromDate(CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value,'+convert(varchar(10),@numDomainId)+')  ['+ @vcColumnName +']'                   
				set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values CFW'+ convert(varchar(3),@tintOrder)+ '                 
				on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=DM.numDivisionId   '                                                         
			END                
			ELSE IF @vcAssociatedControlType = 'SelectBox'             
			BEGIN
				set @vcDbColumnName = 'DCust'+Convert(varchar(10),@numFieldId)                
				set @strColumns=@strColumns+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName +']'                                                          
				set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values CFW'+ convert(varchar(3),@tintOrder)+ '                 
					on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=DM.numDivisionId    '                                                         
				set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID=CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value'                
			END
			ELSE IF @vcAssociatedControlType = 'CheckBoxList' 
			BEGIN
				SET @strColumns= @strColumns+',(SELECT STUFF((SELECT CONCAT('', '', vcData) FROM ListDetails WHERE numListID= ' + CAST(@numListID AS varchar) + ' AND numlistitemid IN (SELECT ISNULL(Id,0) FROM dbo.SplitIDs(CFW'+ convert(VARCHAR(3),@tintOrder)+'.Fld_Value,'','')) FOR XML PATH('''')), 1, 1, ''''))  ['+ @vcColumnName +']'   

			SET @WhereCondition= @WhereCondition 
								+' left Join CFW_FLD_Values CFW'+ convert(varchar(3),@tintOrder)
								+ ' on CFW' + convert(varchar(3),@tintOrder) 
								+ '.Fld_Id='+convert(varchar(10),@numFieldId) 
								+ 'and CFW' + convert(varchar(3),@tintOrder) 
								+ '.RecId=DM.numDivisionId '
			END              
		END          
                
		SELECT TOP 1 
			@tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName,@vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,
			@numListID=numListID,@vcLookBackTableName=vcLookBackTableName,@bitCustom=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,
			@bitAllowEdit=bitAllowEdit,@ListRelID=ListRelID
		FROM
			#tempForm 
		WHERE 
			tintOrder > @tintOrder-1 
		ORDER BY 
			tintOrder asc            
 
		IF @@rowcount=0 SET @tintOrder=0 
	END     

	  
	DECLARE @strShareRedordWith AS VARCHAR(300);
	SET @strShareRedordWith=' DM.numDivisionID in (select SR.numRecordID FROM ShareRecord SR WHERE SR.numDomainID='+ convert(varchar(15),@numDomainID )+' AND SR.numModuleID=1 AND SR.numAssignedTo='+convert(varchar(15),@numUserCntId)+') '
   
	SET @strColumns=@strColumns+' ,ISNULL(VIE.Total,0) as TotalEmail '
	SET @strColumns=@strColumns+' ,ISNULL(VOA.OpenActionItemCount,0) as TotalActionItem '
   
	DECLARE @StrSql AS VARCHAR(MAX) = ''
	SET @StrSql=' FROM  CompanyInfo CMP                                                            
					join DivisionMaster DM on DM.numCompanyID=CMP.numCompanyID
					join AdditionalContactsInformation ADC on ADC.numDivisionID=DM.numDivisionID
					left join VIEW_OPEN_ACTION_ITEMS VOA WITH (NOEXPAND) ON VOA.numDomainID = '+ CAST(@numDomainID as varchar )+' AND  VOA.numDivisionId = DM.numDivisionID 
					left join View_InboxEmail VIE  WITH (NOEXPAND) ON VIE.numDomainID = '+ CAST(@numDomainID as varchar )+' AND  VIE.numContactId = ADC.numContactId ' + ISNULL(@WhereCondition,'')

	IF @tintSortOrder= 9 set @strSql=@strSql+' join Favorites F on F.numContactid=DM.numDivisionID ' 
	
	 -------Change Row Color-------
	Declare @vcCSOrigDbCOlumnName as varchar(50),@vcCSLookBackTableName as varchar(50),@vcCSAssociatedControlType as varchar(50)
	SET @vcCSOrigDbCOlumnName=''
	SET @vcCSLookBackTableName=''

	Create table #tempColorScheme(vcOrigDbCOlumnName varchar(50),vcLookBackTableName varchar(50),vcFieldValue varchar(50),vcFieldValue1 varchar(50),vcColorScheme varchar(50),vcAssociatedControlType varchar(50))

	insert into #tempColorScheme select DFM.vcOrigDbCOlumnName,DFM.vcLookBackTableName,DFCS.vcFieldValue,DFCS.vcFieldValue1,DFCS.vcColorScheme,DFFM.vcAssociatedControlType 
	from DycFieldColorScheme DFCS join DycFormField_Mapping DFFM on DFCS.numFieldID=DFFM.numFieldID
	join DycFieldMaster DFM on DFM.numModuleID=DFFM.numModuleID and DFM.numFieldID=DFFM.numFieldID
	where DFCS.numDomainID=@numDomainID and DFFM.numFormID=36 AND DFCS.numFormID=36 and isnull(DFFM.bitAllowGridColor,0)=1

	IF(SELECT COUNT(*) FROM #tempColorScheme)>0
	BEGIN
	   select top 1 @vcCSOrigDbCOlumnName=vcOrigDbCOlumnName,@vcCSLookBackTableName=vcLookBackTableName,@vcCSAssociatedControlType=vcAssociatedControlType from #tempColorScheme
	END   
	----------------------------                   
 
	 IF len(@vcCSOrigDbCOlumnName)>0 and len(@vcCSLookBackTableName)>0
	BEGIN
	set @strColumns=@strColumns+',tCS.vcColorScheme'
	END

	IF len(@vcCSOrigDbCOlumnName)>0 and len(@vcCSLookBackTableName)>0
	BEGIN
		 if @vcCSLookBackTableName = 'AdditionalContactsInformation'                  
			set @Prefix = 'ADC.'                  
		 if @vcCSLookBackTableName = 'DivisionMaster'                  
			set @Prefix = 'DM.'
		 if @vcCSLookBackTableName = 'CompanyInfo'                  
			set @Prefix = 'CMP.'   
 
		IF @vcCSAssociatedControlType='DateField'
		BEGIN
				set @strSql=@strSql+' left join #tempColorScheme tCS on CONVERT(VARCHAR(10),' + @Prefix + @vcCSOrigDbCOlumnName + ',111) >= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue as int),GETUTCDATE()),111)
				 and CONVERT(VARCHAR(10),' + @Prefix + @vcCSOrigDbCOlumnName + ',111) <= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue1 as int),GETUTCDATE()),111)'
		END
		ELSE IF @vcCSAssociatedControlType='SelectBox' OR @vcCSAssociatedControlType='ListBox' OR @vcCSAssociatedControlType='CheckBox'
		BEGIN
			set @strSql=@strSql+' left join #tempColorScheme tCS on tCS.vcFieldValue=' + @Prefix + @vcCSOrigDbCOlumnName
		END
	END

	IF @columnName LIKE 'CFW.Cust%' 
	BEGIN                
		SET @strSql = @strSql + ' left Join CFW_FLD_Values CFW on CFW.RecId=DM.numDivisionID and CFW.fld_id= ' + REPLACE(@columnName, 'CFW.Cust', '') + ' '                                                         
		SET @columnName = 'CFW.Fld_Value'                
	END                                         
	ELSE IF @columnName LIKE 'DCust%' 
    BEGIN                
        SET @strSql = @strSql + ' left Join CFW_FLD_Values CFW on CFW.RecId=DM.numDivisionID  and CFW.fld_id= ' + REPLACE(@columnName, 'DCust', '')                                                                                                   
        SET @strSql = @strSql + ' left Join ListDetails LstCF on LstCF.numListItemID=CFW.Fld_Value  '                
        SET @columnName = 'LstCF.vcData'                  
    END
	ELSE IF @columnName = 'Opp.vcLastSalesOrderDate'   
	BEGIN
		SET @columnName = 'TempLastOrder.bintCreatedDate'
	END
	
	IF @columnName = 'CMP.vcPerformance'   
	BEGIN
		SET @columnName = 'TempPerformance.monDealAmount'
	END

	SET @strSql = @strSql + ' WHERE  ISNULL(ADC.bitPrimaryContact,0)=1  
							AND CMP.numDomainID=DM.numDomainID   
							and DM.numDomainID=ADC.numDomainID   
							AND CMP.numCompanyType=46                                                        
							AND DM.tintCRMType= '+ convert(varchar(2),@CRMType)+'                                            
							AND DM.numDomainID= ' + convert(varchar(15),@numDomainID)

	SET @strSql=@strSql + ' AND DM.bitActiveInActive=' + CONVERT(VARCHAR(15), @bitActiveInActive)
	
	IF LEN(@SearchQuery) > 0
	BEGIN
		SET @StrSql = @StrSql + ' AND (' + @SearchQuery + ') '
	END	

	 IF @bitPartner = 1 
	BEGIN
        SET @strSql = @strSql + 'and (DM.numAssignedTo=' + CONVERT(VARCHAR(15), @numUserCntID) + ' or DM.numCreatedBy=' + CONVERT(VARCHAR(15), @numUserCntID) + ') '                                                                 
    END
	                                                         
    IF @SortChar <> '0' 
	BEGIN
        SET @strSql = @strSql + ' And CMP.vcCompanyName like ''' + @SortChar + '%'''                                                               
	END

    IF @tintUserRightType = 1 
	BEGIN
        SET @strSql = @strSql + CONCAT(' AND (DM.numRecOwner = ',@numUserCntID,' or DM.numAssignedTo=',@numUserCntID,' or ',@strShareRedordWith,')')                                                                     
	END
    ELSE IF @tintUserRightType = 2 
	BEGIN
        SET @strSql = @strSql + CONCAT(' AND (DM.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= ',@numUserCntID,' ) or DM.numAssignedTo=',@numUserCntID,' or ',@strShareRedordWith,')')
    END
	               
    IF @numProfile <> 0 
	BEGIN
		SET @strSQl = @strSql + ' and cmp.vcProfile = ' + CONVERT(VARCHAR(15), @numProfile)                                                    
    END
	                                                      
    IF @tintSortOrder = 1 
        SET @strSql = @strSql + ' AND DM.numStatusID=2 '                                                              
    ELSE IF @tintSortOrder = 2 
        SET @strSql = @strSql + ' AND DM.numStatusID=3 '                          
    ELSE IF @tintSortOrder = 3 
        SET @strSql = @strSql + ' AND (DM.numRecOwner = ' + CONVERT(VARCHAR(15), @numUserCntID) + ' or DM.numAssignedTo=' + CONVERT(VARCHAR(15), @numUserCntID) + ' or ' + @strShareRedordWith +')'                                                                                  
    ELSE IF @tintSortOrder = 5 
        SET @strSql = @strSql + ' order by numCompanyRating desc '                                                                     
    ELSE IF @tintSortOrder = 6 
        SET @strSql = @strSql + ' AND DM.bintCreatedDate > ''' + CONVERT(VARCHAR(20), DATEADD(day, -7,GETUTCDATE())) + ''''                                                                    
    ELSE IF @tintSortOrder = 7 
        SET @strSql = @strSql + ' and DM.numCreatedby=' + CONVERT(VARCHAR(15), @numUserCntID)                     
    ELSE IF @tintSortOrder = 8 
        SET @strSql = @strSql + ' and DM.numModifiedby=' + CONVERT(VARCHAR(15), @numUserCntID)                  
                                                   
        
	IF @vcRegularSearchCriteria<>'' 
	BEGIN
		IF PATINDEX('%cmp.vcPerformance%', @vcRegularSearchCriteria)>0
		BEGIN
			-- WE ARE MANAGING CONDITION IN OUTER APPLY
			set @strSql=@strSql
		END
		ELSE
			set @strSql=@strSql+' and ' + @vcRegularSearchCriteria 
	END


	IF @vcCustomSearchCriteria <> '' 
	BEGIN
		SET @strSql=@strSql +' AND ' + @vcCustomSearchCriteria 
    END  

	DECLARE @firstRec AS INTEGER                                                            
	DECLARE @lastRec AS INTEGER                                                            
	SET @firstRec = ( @CurrentPage - 1 ) * @PageSize                                                            
	SET @lastRec = ( @CurrentPage * @PageSize + 1 )                  

	-- EXECUTE FINAL QUERY
	DECLARE @strFinal AS NVARCHAR(MAX)
	SET @strFinal = CONCAT('SELECT ROW_NUMBER() OVER(ORDER BY ',@columnName,' ',@columnSortOrder,') ID, ',@strColumns,' INTO #tempTable',@StrSql,'; SELECT @TotalRecords = COUNT(*) FROM #tempTable; SELECT * FROM #tempTable WHERE ID > ',@firstRec,' and ID <',@lastRec,'; DROP TABLE #tempTable;')
	
	PRINT @strFinal
	exec sp_executesql @strFinal, N'@TotalRecords INT OUT', @TotRecs OUT

	SELECT * FROM #tempForm
	DROP TABLE #tempForm
	DROP TABLE #tempColorScheme
END
/****** Object:  StoredProcedure [dbo].[USP_AddUpdateWareHouseForItems]    Script Date: 07/26/2008 16:14:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_AddUpdateWareHouseForItems')
DROP PROCEDURE USP_AddUpdateWareHouseForItems
GO
CREATE PROCEDURE [dbo].[USP_AddUpdateWareHouseForItems]  
@numItemCode as numeric(9)=0,  
@numWareHouseID as numeric(9)=0,
@numWareHouseItemID as numeric(9)=0 OUTPUT,
@vcLocation as varchar(250)='',
@monWListPrice as money =0,
@numOnHand as FLOAT=0,
@numReorder as FLOAT=0,
@vcWHSKU as varchar(100)='',
@vcBarCode as varchar(50)='',
@numDomainID AS NUMERIC(9),
@strFieldList as TEXT='',
@vcSerialNo as varchar(100)='',
@vcComments as varchar(1000)='',
@numQty as numeric(18)=0,
@byteMode as tinyint=0,
@numWareHouseItmsDTLID as numeric(18)=0,
@numUserCntID AS NUMERIC(9)=0,
@dtAdjustmentDate AS DATETIME=NULL,
@numWLocationID NUMERIC(9)=0
AS  
BEGIN
	DECLARE @numDomain AS NUMERIC(18,0)
	DECLARE @numGlobalWarehouseItemID NUMERIC(18,0)
	SET @numDomain = @numDomainID

	DECLARE @bitLotNo AS BIT = 0	
	DECLARE @bitSerialized AS BIT  = 0
	DECLARE @bitMatrix AS BIT = 0
	DECLARE @numItemGroup AS NUMERIC(18,0) =0
	DECLARE @vcSKU AS VARCHAR(200) = ''
	DECLARE @vcUPC AS VARCHAR(200) = ''
	DECLARE @monListPrice MONEY


	SELECT 
		@bitLotNo=ISNULL(Item.bitLotNo,0),
		@bitSerialized=ISNULL(Item.bitSerialized,0),
		@bitMatrix = ISNULL(bitMatrix,0),
		@numItemGroup = ISNULL(@numItemGroup,0),
		@vcSKU = ISNULL(vcSKU,0),
		@vcUPC = ISNULL(numBarCodeId,0),
		@monListPrice = ISNULL(monListPrice,0)
	FROM 
		Item 
	WHERE 
		numItemCode=@numItemCode 
		AND numDomainID=@numDomainID

	DECLARE @vcDescription AS VARCHAR(100)

	IF @byteMode=0 or @byteMode=1 or @byteMode=2 or @byteMode=5
	BEGIN
		--Insert/Update WareHouseItems
		IF @byteMode=0 or @byteMode=1
		BEGIN
			IF @numWareHouseItemID>0
			BEGIN
				UPDATE 
					WareHouseItems
				SET 
					numWareHouseID=@numWareHouseID,				
					numReorder=@numReorder,
					monWListPrice=@monWListPrice,
					vcLocation=@vcLocation,
					numWLocationID = @numWLocationID,
					vcWHSKU=@vcWHSKU,
					vcBarCode=@vcBarCode,
					dtModified=GETDATE()   
				WHERE 
					numItemID=@numItemCode 
					AND numDomainID=@numDomainID 
					AND numWareHouseItemID=@numWareHouseItemID
		
				IF NOT EXISTS(SELECT numItemCode FROM dbo.OpportunityItems WHERE numItemCode = @numItemCode)
				BEGIN
					IF ((SELECT ISNULL(numOnHand,0) FROM WareHouseItems where numItemID = @numItemCode AND numDomainId = @numDomainID and numWareHouseItemID = @numWareHouseItemID) = 0)
					BEGIN
						UPDATE 
							WareHouseItems 
						SET 
							numOnHand = @numOnHand 
						WHERE 
							numItemID = @numItemCode 
							AND numDomainID = @numDomainID 
							AND numWareHouseItemID = @numWareHouseItemID		
					END
				END
		
				SET @vcDescription='UPDATE WareHouse'
			END
			ELSE
			BEGIN
				INSERT INTO WareHouseItems 
				(
					numItemID, 
					numWareHouseID,
					numOnHand,
					numReorder,
					monWListPrice,
					vcLocation,
					vcWHSKU,
					vcBarCode,
					numDomainID,
					dtModified,
					numWLocationID
				)  
				VALUES
				(
					@numItemCode,
					@numWareHouseID,
					(Case When @bitLotNo=1 or @bitSerialized=1 then 0 else @numOnHand end),
					@numReorder,
					@monWListPrice,
					@vcLocation,
					@vcWHSKU,
					@vcBarCode,
					@numDomainID,
					GETDATE(),
					@numWLocationID
				)  

				SET @numWareHouseItemID = @@identity


				IF (SELECT COUNT(*) FROM WareHouseItems WHERE numWareHouseID=@numWareHouseID AND numItemID=@numItemCode AND numWLocationID=-1) = 0
				BEGIN
					INSERT INTO WareHouseItems 
					(
						numItemID, 
						numWareHouseID,
						numWLocationID,
						numOnHand,
						numReorder,
						monWListPrice,
						vcLocation,
						vcWHSKU,
						vcBarCode,
						numDomainID,
						dtModified
					)  
					VALUES
					(
						@numItemCode,
						@numWareHouseID,
						-1,
						0,
						0,
						@monWListPrice,
						'',
						@vcWHSKU,
						@vcBarCode,
						@numDomainID,
						GETDATE()
					)  

					SELECT @numGlobalWarehouseItemID = SCOPE_IDENTITY()
				END



				SET @vcDescription='INSERT WareHouse'
			END
		END

		--Insert/Update WareHouseItmsDTL

		DECLARE @OldQty FLOAT = 0

		IF @byteMode=0 or @byteMode=2 or @byteMode=5
		BEGIN
			IF @bitLotNo=1 or @bitSerialized=1
			BEGIN
				IF @bitSerialized=1
					SET @numQty=1
	
				IF @byteMode=0
				BEGIN
					SELECT TOP 1 
						@numWareHouseItmsDTLID=numWareHouseItmsDTLID,
						@OldQty=numQty 
					FROM 
						WareHouseItmsDTL 
					WHERE 
						numWareHouseItemID=@numWareHouseItemID 
						AND vcSerialNo=@vcSerialNo and numQty > 0
				END
				ELSE IF @numWareHouseItmsDTLID>0
				BEGIN
					SELECT TOP 1 
						@OldQty=numQty 
					FROM 
						WareHouseItmsDTL 
					WHERE 
						numWareHouseItmsDTLID=@numWareHouseItmsDTLID
				END

				IF @numWareHouseItmsDTLID>0
				BEGIN
					UPDATE 
						WareHouseItmsDTL 
					SET 
						vcComments = @vcComments,
						numQty=@numQty,
						vcSerialNo=@vcSerialNo
					WHERE 
						numWareHouseItmsDTLID=@numWareHouseItmsDTLID 
						AND numWareHouseItemID=@numWareHouseItemID
                
					SET @vcDescription=CASE WHEN @byteMode=5 THEN 'Inventory Adjustment (Update Lot/Serial#) : ' ELSE 'UPDATE Lot/Serial# : ' END + @vcSerialNo 
				END
				ELSE
				BEGIN
				   INSERT INTO WareHouseItmsDTL
				   (numWareHouseItemID,vcSerialNo,vcComments,numQty,bitAddedFromPO)  
				   VALUES 
				   (@numWareHouseItemID,@vcSerialNo,@vcComments,@numQty,0)
				
				   SET @numWareHouseItmsDTLID=@@identity

				   SET @vcDescription=CASE WHEN @byteMode=5 THEN 'Inventory Adjustment (Insert Lot/Serial#) : ' ELSE 'INSERT Lot/Serial# : ' END + @vcSerialNo 
				END

 				UPDATE 
					WareHouseItems 
				SET 
					numOnHand=numOnHand + (@numQty - @OldQty),
					dtModified=GETDATE() 
				WHERE 
					numWareHouseItemID = @numWareHouseItemID 
					AND [numDomainID] = @numDomainID
					AND (numOnHand + (@numQty - @OldQty))>=0
			END 
		END

		DECLARE @bitOppOrderExists AS BIT = 0

		IF (SELECT COUNT(OI.numOppId) FROM dbo.OpportunityItems OI WHERE OI.numItemCode =@numItemCode) > 0
		BEGIN
			SET @bitOppOrderExists = 1
		END
		ELSE IF (SELECT COUNT(OI.numOppId) FROM dbo.OpportunityKitItems OI WHERE OI.numChildItemID =@numItemCode) > 0
		BEGIN
			SET @bitOppOrderExists = 1
		END
		ELSE IF (SELECT COUNT(OI.numOppId) FROM dbo.OpportunityKitChildItems OI WHERE OI.numItemID =@numItemCode) > 0
		BEGIN
			SET @bitOppOrderExists = 1
		END

		IF @bitMatrix = 1 AND ISNULL(@bitOppOrderExists,0) <> 1 -- ITEM LEVEL ATTRIBUTES
		BEGIN
			IF ISNULL(@numWareHouseItemID,0) > 0
			BEGIN
				DELETE FROM CFW_Fld_Values_Serialized_Items WHERE RecId = @numWareHouseItemID
			
				INSERT INTO CFW_Fld_Values_Serialized_Items
				(
					Fld_ID,
					Fld_Value,
					RecId,
					bitSerialized
				)                                                  
				SELECT 
					Fld_ID,
					ISNULL(CAST(Fld_Value AS VARCHAR),'') AS Fld_Value,
					@numWareHouseItemID,
					0 
				FROM 
					ItemAttributes
				WHERE
					numDomainID = @numDomainID
					AND numItemCode = @numItemCode
			END

			IF ISNULL(@numGlobalWarehouseItemID,0) > 0
			BEGIN
				DELETE FROM CFW_Fld_Values_Serialized_Items WHERE RecId = @numGlobalWarehouseItemID
				
				INSERT INTO CFW_Fld_Values_Serialized_Items
				(
					Fld_ID,
					Fld_Value,
					RecId,
					bitSerialized
				)                                                  
				SELECT 
					Fld_ID,
					ISNULL(CAST(Fld_Value AS VARCHAR),'') AS Fld_Value,
					@numGlobalWarehouseItemID,
					0 
				FROM 
					ItemAttributes
				WHERE
					numDomainID = @numDomainID
					AND numItemCode = @numItemCode
			END   
		END
		ELSE IF DATALENGTH(@strFieldList) > 2 AND ISNULL(@bitOppOrderExists,0) <> 1
		BEGIN
			--Insert Custom Fields base on WareHouseItems/WareHouseItmsDTL 
			declare @hDoc as int     
			EXEC sp_xml_preparedocument @hDoc OUTPUT, @strFieldList                                                                        

				declare  @rows as integer                                        
	                                         
			SELECT @rows=count(*) FROM OPENXML (@hDoc,'/NewDataSet/CusFlds',2)                                                  
							WITH (Fld_ID numeric(9),Fld_Value varchar(100))                                        
	                                                                           

			if @rows>0                                        
			begin  
				Create table #tempTable (ID INT IDENTITY PRIMARY KEY,Fld_ID numeric(9),Fld_Value varchar(100))   
	                                      
				insert into #tempTable (Fld_ID,Fld_Value)                                        
				SELECT * FROM OPENXML (@hDoc,'/NewDataSet/CusFlds',2)                                                  
				WITH (Fld_ID numeric(9),Fld_Value varchar(100))                                           
	                                         
				delete CFW_Fld_Values_Serialized_Items from CFW_Fld_Values_Serialized_Items C inner join #tempTable T on C.Fld_ID=T.Fld_ID where C.RecId=@numWareHouseItemID                              
	      
				INSERT INTO CFW_Fld_Values_Serialized_Items
				(
					Fld_ID,Fld_Value,RecId,bitSerialized
				) 

				select Fld_ID,isnull(Fld_Value,'') as Fld_Value,@numWareHouseItemID,0 from #tempTable 

				drop table #tempTable                                        
			 end  

			 EXEC sp_xml_removedocument @hDoc 
		END	  
 
		EXEC dbo.USP_ManageWareHouseItems_Tracking
		@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
		@numReferenceID = @numItemCode, --  numeric(9, 0)
		@tintRefType = 1, --  tinyint
		@vcDescription = @vcDescription, --  varchar(100)
		@numModifiedBy = @numUserCntID,
		@ClientTimeZoneOffset = 0,
		@dtRecordDate = @dtAdjustmentDate,
		@numDomainID = @numDomain 

		IF ISNULL(@numGlobalWarehouseItemID,0) > 0
		BEGIN        
			EXEC dbo.USP_ManageWareHouseItems_Tracking
			@numWareHouseItemID = @numGlobalWarehouseItemID,
			@numReferenceID = @numItemCode,
			@tintRefType = 1,
			@vcDescription = @vcDescription,
			@numModifiedBy = @numUserCntID,
			@ClientTimeZoneOffset = 0,
			@dtRecordDate = @dtAdjustmentDate,
			@numDomainID = @numDomainID 
		END

		-- KEEP IT LAST IN SECTION
		IF ISNULL(@numItemGroup,0) > 0 OR ISNULL(@bitMatrix,0) = 1 -- IF IT's MATRIX ITEM THEN 
		BEGIN
			UPDATE WareHouseItems SET monWListPrice = (CASE WHEN @monListPrice > 0 THEN @monListPrice ELSE monWListPrice END), vcWHSKU=@vcSKU, vcBarCode=@vcUPC WHERE numDomainID = @numDomainID AND numItemID = @numItemCode
		END
	END
	ELSE IF @byteMode=3
	BEGIN
		IF EXISTS (SELECT * FROM [OpportunityMaster] OM INNER JOIN [OpportunityItems] OI ON OM.[numOppId] = OI.[numOppId] WHERE OM.[numDomainId]=@numDomainID AND [numWarehouseItmsID]=@numWareHouseItemID)
		BEGIN
			RAISERROR('OpportunityItems_Depend',16,1);
			RETURN
		END

		IF EXISTS (SELECT * FROM OpportunityItemsReceievedLocation WHERE [numDomainId]=@numDomainID AND numWarehouseItemID=@numWareHouseItemID)
		BEGIN
			RAISERROR('OpportunityItems_Depend',16,1);
			RETURN
		END

		IF (SELECT COUNT(*) FROM OpportunityKitItems WHERE numWareHouseItemId=@numWareHouseItemID) > 0
 		BEGIN
	  		RAISERROR('OpportunityKitItems_Depend',16,1);
			RETURN
		END
	
		IF (SELECT COUNT(*) FROM ItemDetails WHERE numWareHouseItemId=@numWareHouseItemID) > 0
 		BEGIN
	  		RAISERROR ('KitItems_Depend',16,1);
			RETURN
		END	
	    
		IF @bitLotNo=1 OR @bitSerialized=1
		BEGIN
			DELETE from CFW_Fld_Values_Serialized_Items where RecId in (select numWareHouseItmsDTLID from WareHouseItmsDTL where numWareHouseItemID=@numWareHouseItemID)
					and bitSerialized=(Case when @bitLotNo=1 or @bitSerialized=1 then 1 else 0 end) 
		END
		ELSE
		BEGIN
			DELETE from CFW_Fld_Values_Serialized_Items where RecId=@numWareHouseItemID
					and bitSerialized=(Case when @bitLotNo=1 or @bitSerialized=1 then 1 else 0 end) 
		END

		DELETE from WareHouseItmsDTL where numWareHouseItemID=@numWareHouseItemID 
		DELETE FROM WareHouseItems_Tracking WHERE numWareHouseItemID=@numWareHouseItemID and numDomainID=@numDomainID
		DELETE from WareHouseItems where numWareHouseItemID=@numWareHouseItemID and numDomainID=@numDomainID
	END
	ELSE IF @byteMode=4
	BEGIN
		IF @bitLotNo=1 or @bitSerialized=1
		BEGIN
			DELETE from CFW_Fld_Values_Serialized_Items where RecId=@numWareHouseItmsDTLID
			and bitSerialized=(Case when @bitLotNo=1 or @bitSerialized=1 then 1 else 0 end) 
		END

		update WHI SET numOnHand=WHI.numOnHand - WHID.numQty,dtModified=GETDATE()
		from WareHouseItems WHI join WareHouseItmsDTL WHID on WHI.numWareHouseItemID=WHID.numWareHouseItemID 
		where WHID.numWareHouseItmsDTLID=@numWareHouseItmsDTLID 
		AND WHI.numDomainID = @numDomainID
		AND (WHI.numOnHand - WHID.numQty)>=0

		SELECT @numWareHouseItemID=WHI.numWareHouseItemID,@numItemCode=numItemID 
		from WareHouseItems WHI join WareHouseItmsDTL WHID on WHI.numWareHouseItemID=WHID.numWareHouseItemID 
		where WHID.numWareHouseItmsDTLID=@numWareHouseItmsDTLID 
		AND WHI.numDomainID = @numDomainID


		EXEC dbo.USP_ManageWareHouseItems_Tracking
			@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
			@numReferenceID = @numItemCode, --  numeric(9, 0)
			@tintRefType = 1, --  tinyint
			@vcDescription = 'DELETE Lot/Serial#', --  varchar(100)
			@numModifiedBy = @numUserCntID,
			@ClientTimeZoneOffset = 0,
			@dtRecordDate = NULL,
			@numDomainID = @numDomain

		DELETE from WareHouseItmsDTL where numWareHouseItmsDTLID=@numWareHouseItmsDTLID
	END
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_CalculateCommissionPaidBizDoc')
DROP PROCEDURE USP_CalculateCommissionPaidBizDoc
GO
CREATE PROCEDURE [dbo].[USP_CalculateCommissionPaidBizDoc]
	-- Add the parameters for the stored procedure here
	 @numComPayPeriodID AS NUMERIC(18,0),
	 @numDomainID AS NUMERIC(18,0),
	 @ClientTimeZoneOffset INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
SET NOCOUNT ON;

BEGIN TRY
BEGIN TRANSACTION
	DECLARE @dtPayStart DATE 
	DECLARE @dtPayEnd DATE

	SELECT @dtPayStart=dtStart,@dtPayEnd=dtEnd FROM CommissionPayPeriod WHERE numComPayPeriodID = @numComPayPeriodID

	--GET COMMISSION RULE OF DOMAIN
	DECLARE @TempCommissionRule TABLE
	(
		ID INT IDENTITY(1,1),
		numComRuleID NUMERIC(18,0),
		tintComBasedOn TINYINT,
		tintComType TINYINT,
		tintComAppliesTo TINYINT,
		tintComOrgType TINYINT,
		tintAssignTo TINYINT
	)

	INSERT INTO
		@TempCommissionRule
	SELECT
		CR.numComRuleID,
		CR.tintComBasedOn,
		CR.tintComType,
		CR.tintComAppliesTo,
		CR.tintComOrgType,
		CR.tintAssignTo
	FROM
		CommissionRules CR
	LEFT JOIN
		PriceBookPriorities PBP
	ON	
		CR.tintComAppliesTo = PBP.Step2Value 
		AND CR.tintComOrgType = PBP.Step3Value
	WHERE
		CR.numDomainID = @numDomainID
		AND ISNULL(PBP.[Priority],0) > 0
	ORDER BY
		ISNULL(PBP.[Priority],0) ASC

	--GET COMMISSION GLOBAL SETTINGS
	DECLARE @numDiscountServiceItemID NUMERIC(18,0)
	DECLARE @numShippingServiceItemID NUMERIC(18,0)
	DECLARE @tintCommissionType TINYINT
	DECLARE @bitIncludeTaxAndShippingInCommission BIT

	SELECT 
		@numDiscountServiceItemID=numDiscountServiceItemID, 
		@numShippingServiceItemID = numShippingServiceItemID, 
		@tintCommissionType=tintCommissionType, 
		@bitIncludeTaxAndShippingInCommission=bitIncludeTaxAndShippingInCommission 
	FROM 
		Domain 
	WHERE 
		numDomainID=@numDomainID

	--TEMPORARY TABLES TO HOLD DATA
	DECLARE @TempBizCommission TABLE
	(
		numUserCntID NUMERIC(18,0),
		numOppID NUMERIC(18,0),
		numOppBizDocID NUMERIC(18,0),
		numOppBizDocItemID NUMERIC(18,0),
		monCommission MONEY,
		numComRuleID NUMERIC(18,0),
		tintComType TINYINT,
		tintComBasedOn TINYINT,
		decCommission FLOAT,
		tintAssignTo TINYINT
	)

	DECLARE @TABLEPAID TABLE
	(
		ID INT IDENTITY(1,1),
		numDivisionID NUMERIC(18,0),
		numRelationship NUMERIC(18,0),
		numProfile NUMERIC(18,0),
		numAssignedTo NUMERIC(18,0),
		numRecordOwner NUMERIC(18,0),
		numItemCode NUMERIC(18,0),
		numItemClassification NUMERIC(18,0),
		numOppID NUMERIC(18,0),
		numOppBizDocID NUMERIC(18,0),
		numOppBizDocItemID NUMERIC(18,0),
		numUnitHour FLOAT,
		monTotAmount MONEY,
		monVendorCost MONEY,
		monAvgCost MONEY,
		numPartner NUMERIC(18,0)
	)

	-- GET FULLY PAID INVOICES WITHIN PAY PERIOD
	INSERT INTO 
		@TABLEPAID
	SELECT 
		OppM.numDivisionID,
		ISNULL(CompanyInfo.numCompanyType,0),
		ISNULL(CompanyInfo.vcProfile,0),
		OppM.numAssignedTo,
		OppM.numRecOwner,
		OppBizItems.numItemCode,
		Item.numItemClassification,
		OppM.numOppID,
		OppBizItems.numOppBizDocID,
		OppBizItems.numOppBizDocItemID,
		ISNULL(OppBizItems.numUnitHour,0),
		ISNULL(OppBizItems.monTotAmount,0),
		(ISNULL(OppMItems.monVendorCost,0) * ISNULL(OppBizItems.numUnitHour,0)),
		(ISNULL(OppMItems.monAvgCost,0) * ISNULL(OppBizItems.numUnitHour,0)),
		OppM.numPartner
	FROM 
		OpportunityBizDocs OppBiz
	INNER JOIN
		OpportunityMaster OppM
	ON
		OppBiz.numOppID = OppM.numOppID
	INNER JOIN
		DivisionMaster 
	ON
		OppM.numDivisionID = DivisionMaster.numDivisionID
	INNER JOIN
		CompanyInfo
	ON
		DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
	INNER JOIN
		OpportunityBizDocItems OppBizItems
	ON
		OppBiz.numOppBizDocsId = OppBizItems.numOppBizDocID
	INNER JOIN
		OpportunityItems OppMItems 
	ON
		OppBizItems.numOppItemID = OppMItems.numoppitemtCode
	INNER JOIN
		Item
	ON
		OppBizItems.numItemCode = Item.numItemCode
	LEFT JOIN
		BizDocComission BDC
	ON
		OppM.numOppId = BDC.numOppID 
		AND OppBiz.numOppBizDocsId = BDC.numOppBizDocId
		AND OppBizItems.numOppBizDocItemID = BDC.numOppBizDocItemID
	CROSS APPLY
	(
		SELECT 
			MAX(DM.dtDepositDate) dtDepositDate
		FROM 
			DepositMaster DM 
		JOIN 
			dbo.DepositeDetails DD
		ON 
			DM.numDepositId=DD.numDepositID 
		WHERE 
			DM.tintDepositePage=2 
			AND DM.numDomainId=@numDomainId
			AND DD.numOppID=OppBiz.numOppID 
			AND DD.numOppBizDocsID =OppBiz.numOppBizDocsID
	) TempDepositMaster
	WHERE
		OppM.numDomainId = @numDomainID 
		AND OppM.tintOppType = 1
		AND OppM.tintOppStatus = 1
		AND OppBiz.bitAuthoritativeBizDocs = 1
		AND ISNULL(OppBiz.monAmountPaid,0) >= OppBiz.monDealAmount
		AND (TempDepositMaster.dtDepositDate IS NOT NULL AND (CAST(DATEADD(MINUTE,-@ClientTimeZoneOffset,TempDepositMaster.dtDepositDate) AS DATE) BETWEEN @dtPayStart AND @dtPayEnd))
		AND OppBizItems.numItemCode NOT IN ( @numDiscountServiceItemID )  /*DO NOT INCLUDE DISCOUNT ITEM*/
		AND 1 = (
					CASE 
						WHEN OppBizItems.numItemCode = @numShippingServiceItemID 
						THEN 
							CASE 
								WHEN ISNULL(@bitIncludeTaxAndShippingInCommission,0) = 1
								THEN	
									1
								ELSE	
									0
							END
						ELSE 
							1 
					END
				)/*CONDITION TO INCLUDE/EXCLUDE INCLUDE SHIPPING ITEM */
		AND BDC.numOppBizDocItemID IS NULL --NO ENTRY AVAILABLE IN BizDocComission TABLE FOR BIZDOC ITEM


		-- LOOP ALL COMMISSION RULES
		DECLARE @i INT = 1
		DECLARE @COUNT INT = 0

		SELECT @COUNT=COUNT(*) FROM @TempCommissionRule

		DECLARE @TEMPITEM TABLE
		(
			ID INT,
			UniqueID INT,
			numUserCntID NUMERIC(18,0),
			numOppID NUMERIC(18,0),
			numOppBizDocID NUMERIC(18,0),
			numOppBizDocItemID NUMERIC(18,0),
			numItemCode NUMERIC(18,0),
			numUnitHour FLOAT,
			monTotAmount MONEY,
			monVendorCost MONEY,
			monAvgCost MONEY
		)

		WHILE @i <= @COUNT
		BEGIN
			DECLARE @numComRuleID NUMERIC(18,0)
			DECLARE @tintComBasedOn TINYINT
			DECLARE @tintComAppliesTo TINYINT
			DECLARE @tintComOrgType TINYINT
			DECLARE @tintComType TINYINT
			DECLARE @tintAssignTo TINYINT
			DECLARE @decCommission MONEY
			DECLARE @numTotalAmount AS MONEY
			DECLARE @numTotalUnit AS FLOAT
			DECLARE @RowCount AS INT

			--CLEAR PREVIOUS VALUES FROM TEMP TABLE
			DELETE FROM @TEMPITEM

			--FETCH COMMISSION RULE
			SELECT @numComRuleID=numComRuleID,@tintComBasedOn=tintComBasedOn,@tintComType=tintComType,@tintComAppliesTo=tintComAppliesTo,@tintComOrgType=tintComOrgType,@tintAssignTo=tintAssignTo FROM @TempCommissionRule WHERE ID=@i

			--FIND ALL BIZ DOC ITEMS WHERE COMMISSION IS NOT PAID AND CURRENT RULE ITEMS, ORGANIZATION AND ASSINGED TO FILTER CAN BE APPLIED
			INSERT INTO
				@TEMPITEM
			SELECT
				ROW_NUMBER() OVER (ORDER BY ID),
				ID,
				(CASE @tintAssignTo WHEN 1 THEN numAssignedTo WHEN 2 THEN numRecordOwner ELSE numPartner END),
				numOppID,
				numOppBizDocID,
				numOppBizDocItemID,
				numItemCode,
				numUnitHour,
				monTotAmount,
				monVendorCost,
				monAvgCost
			FROM
				@TABLEPAID
			WHERE
				1 = (CASE @tintComAppliesTo
						--SPECIFIC ITEMS
						WHEN 1 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleItems WHERE numComRuleID = @numComRuleID AND ISNULL(numValue,0) = numItemCode) > 0 THEN 1 ELSE 0 END
						-- ITEM WITH SPECIFIC CLASSIFICATIONS
						WHEN 2 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleItems WHERE numComRuleID = @numComRuleID AND ISNULL(numValue,0) = numItemClassification) > 0 THEN 1 ELSE 0 END
						-- ALL ITEMS
						ELSE 1
					END)
				AND 1 = (CASE @tintComOrgType
							-- SPECIFIC ORGANIZATION
							WHEN 1 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleOrganization WHERE numComRuleID = @numComRuleID AND ISNULL(numValue,0) = numDivisionID) > 0 THEN 1 ELSE 0 END
							-- ORGANIZATION WITH SPECIFIC RELATIONSHIP AND PROFILE
							WHEN 2 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleOrganization WHERE numComRuleID = @numComRuleID AND ISNULL(numValue,0) = numRelationship AND ISNULL(numProfile,0) = numProfile) > 0 THEN 1 ELSE 0 END
							-- ALL ORGANIZATIONS
							ELSE 1
						END)
				AND 1 = (CASE @tintAssignTo 
							-- ORDER ASSIGNED TO
							WHEN 1 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleContacts WHERE numComRuleID = @numComRuleID AND numValue=numAssignedTo) > 0 THEN 1 ELSE 0 END
							-- ORDER OWNER
							WHEN 2 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleContacts WHERE numComRuleID = @numComRuleID AND numValue=numRecordOwner) > 0 THEN 1 ELSE 0 END
							-- Order Partner
							WHEN 3 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleContacts WHERE numComRuleID = @numComRuleID AND numValue=numPartner) > 0 THEN 1 ELSE 0 END
							-- NO OPTION SELECTED IN RULE
							ELSE 0
						END)

			-- IF BIZDOC ITEMS ARE FOUND AFTER FILTER
			IF (SELECT COUNT(*) FROM @TEMPITEM) > 0
			BEGIN
				--CHECK COMMISSION BASED ON OPTION OF RULE
				--BASED ON AMOUNT SOLD
				IF @tintComBasedOn = 1
				BEGIN
					SELECT @numTotalAmount = SUM(monTotAmount) FROM @TEMPITEM

					IF (SELECT COUNT(*) FROM CommissionRuleDtl WHERE numComRuleID=@numComRuleID AND (@numTotalAmount BETWEEN intFrom AND intTo)) > 0
					BEGIN
						SELECT TOP 1 @decCommission=ISNULL(decCommission,0) FROM CommissionRuleDtl WHERE numComRuleID=@numComRuleID AND (@numTotalAmount BETWEEN intFrom AND intTo)
					END
				END
				--BASED ON UNITS SOLD
				ELSE IF @tintComBasedOn = 2
				BEGIN
					SELECT @numTotalUnit = SUM(numUnitHour) FROM @TEMPITEM

					IF (SELECT COUNT(*) FROM CommissionRuleDtl WHERE numComRuleID=@numComRuleID AND (@numTotalUnit BETWEEN intFrom AND intTo)) > 0
					BEGIN
						SELECT TOP 1 @decCommission=ISNULL(decCommission,0) FROM CommissionRuleDtl WHERE numComRuleID=@numComRuleID AND (@numTotalUnit BETWEEN intFrom AND intTo)
					END
				END

				--IF COMMISSION AMOUNT FOUND AFTER CHECKING COMMISSION FROM AND TO RANGE IN ABOVE STEP
				IF ISNULL(@decCommission,0) > 0
				BEGIN
					SELECT @RowCount=COUNT(*) FROM @TEMPITEM

					--CALCULATE COMMISSION AND STORE IN TEMPARORY TABLE
					INSERT INTO @TempBizCommission 
					(
						numUserCntID,
						numOppID,
						numOppBizDocID,
						numOppBizDocItemID,
						monCommission,
						numComRuleID,
						tintComType,
						tintComBasedOn,
						decCommission,
						tintAssignTo
					)
					SELECT
						numUserCntID,
						numOppID,
						numOppBizDocID,
						numOppBizDocItemID,
						CASE @tintComType 
							WHEN 1 --PERCENT
								THEN 
									CASE @tintCommissionType
										--TOTAL AMOUNT PAID
										WHEN 1 THEN monTotAmount * (@decCommission / 100)
										--ITEM GROSS PROFIT (VENDOR COST)
										WHEN 2 THEN (ISNULL(monTotAmount,0) - ISNULL(monVendorCost,0)) * (@decCommission / 100)
										--ITEM GROSS PROFIT (AVERAGE COST)
										WHEN 3 THEN(ISNULL(monTotAmount,0) - ISNULL(monAvgCost,0)) * (@decCommission / 100)
									END
							ELSE  --FLAT: COMMISSION AMOUNT IS EQUALLY DISTRIBUTED AMONG FOUND ENTERIES
								(@decCommission / @RowCount)
						END,
						@numComRuleID,
						@tintComType,
						@tintComBasedOn,
						@decCommission,
						@tintAssignTo
					FROM 
						@TEMPITEM AS T1
					WHERE
						1 = (CASE @tintCommissionType 
								--TOTAL PAID INVOICE
								WHEN 1 THEN CASE WHEN (ISNULL(monTotAmount,0) > 0) THEN 1 ELSE 0 END 
								-- ITEM GROSS PROFIT (VENDOR COST)
								WHEN 2 THEN CASE WHEN ((ISNULL(monTotAmount,0) - ISNULL(monVendorCost,0)) > 0) THEN 1 ELSE 0 END 
								-- ITEM GROSS PROFIT (AVERAGE COST)
								WHEN 3 THEN CASE WHEN ((ISNULL(monTotAmount,0) - ISNULL(monAvgCost,0)) > 0) THEN 1 ELSE 0 END
								ELSE
									0 
							END) 
						AND (
								SELECT 
									COUNT(*) 
								FROM 
									@TempBizCommission 
								WHERE 
									numUserCntID=T1.numUserCntID 
									AND numOppID=T1.numOppID 
									AND numOppBizDocID = T1.numOppBizDocID 
									AND numOppBizDocItemID = T1.numOppBizDocItemID 
							) = 0
				END
			END

			SET @i = @i + 1

			SET @numComRuleID = 0
			SET @tintComBasedOn = 0
			SET @tintComAppliesTo = 0
			SET @tintComOrgType = 0
			SET @tintComType = 0
			SET @tintAssignTo = 0
			SET @decCommission = 0
			SET @numTotalAmount = 0
			SET @numTotalUnit = 0
			SET @RowCount = 0
		END

		--INSERT CALCULATED ENTERIES IN BIZDOCCOMMISSION TABLE
		INSERT INTO BizDocComission
		(	
			numDomainID,
			numUserCntID,
			numOppBizDocId,
			numComissionAmount,
			numOppBizDocItemID,
			numComRuleID,
			tintComType,
			tintComBasedOn,
			decCommission,
			numOppID,
			bitFullPaidBiz,
			numComPayPeriodID,
			tintAssignTo
		)
		SELECT
			@numDomainID,
			numUserCntID,
			numOppBizDocID,
			monCommission,
			numOppBizDocItemID,
			numComRuleID,
			tintComType,
			tintComBasedOn,
			decCommission,
			numOppID,
			1,
			@numComPayPeriodID,
			@tintAssignTo
		FROM
			@TempBizCommission
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
END
GO

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_CalculateCommissionUnPaidBizDoc')
DROP PROCEDURE USP_CalculateCommissionUnPaidBizDoc
GO
CREATE PROCEDURE [dbo].[USP_CalculateCommissionUnPaidBizDoc]
	-- Add the parameters for the stored procedure here
	 @numComPayPeriodID AS NUMERIC(18,0),
	 @numDomainID AS NUMERIC(18,0),
	 @ClientTimeZoneOffset INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
SET NOCOUNT ON;

BEGIN TRY
BEGIN TRANSACTION
	DECLARE @dtPayStart DATE 
	DECLARE @dtPayEnd DATE

	SELECT @dtPayStart=dtStart,@dtPayEnd=dtEnd FROM CommissionPayPeriod WHERE numComPayPeriodID = @numComPayPeriodID

	--GET COMMISSION RULE OF DOMAIN
	DECLARE @TempCommissionRule TABLE
	(
		ID INT IDENTITY(1,1),
		numComRuleID NUMERIC(18,0),
		tintComBasedOn TINYINT,
		tintComType TINYINT,
		tintComAppliesTo TINYINT,
		tintComOrgType TINYINT,
		tintAssignTo TINYINT
	)

	INSERT INTO
		@TempCommissionRule
	SELECT
		CR.numComRuleID,
		CR.tintComBasedOn,
		CR.tintComType,
		CR.tintComAppliesTo,
		CR.tintComOrgType,
		CR.tintAssignTo
	FROM
		CommissionRules CR
	LEFT JOIN
		PriceBookPriorities PBP
	ON	
		CR.tintComAppliesTo = PBP.Step2Value 
		AND CR.tintComOrgType = PBP.Step3Value
	WHERE
		CR.numDomainID = @numDomainID
		AND ISNULL(PBP.[Priority],0) > 0
	ORDER BY
		ISNULL(PBP.[Priority],0) ASC

	--GET COMMISSION GLOBAL SETTINGS
	DECLARE @numDiscountServiceItemID NUMERIC(18,0)
	DECLARE @numShippingServiceItemID NUMERIC(18,0)
	DECLARE @tintCommissionType TINYINT
	DECLARE @bitIncludeTaxAndShippingInCommission BIT

	SELECT 
		@numDiscountServiceItemID=numDiscountServiceItemID, 
		@numShippingServiceItemID = numShippingServiceItemID, 
		@tintCommissionType=tintCommissionType, 
		@bitIncludeTaxAndShippingInCommission=bitIncludeTaxAndShippingInCommission 
	FROM 
		Domain 
	WHERE 
		numDomainID=@numDomainID

	--TEMPORARY TABLES TO HOLD DATA
	DECLARE @TempBizCommission TABLE
	(
		numUserCntID NUMERIC(18,0),
		numOppID NUMERIC(18,0),
		numOppBizDocID NUMERIC(18,0),
		numOppBizDocItemID NUMERIC(18,0),
		monCommission MONEY,
		numComRuleID NUMERIC(18,0),
		tintComType TINYINT,
		tintComBasedOn TINYINT,
		decCommission FLOAT,
		tintAssignTo TINYINT
	)

	DECLARE @TABLEPAID TABLE
	(
		ID INT IDENTITY(1,1),
		numDivisionID NUMERIC(18,0),
		numRelationship NUMERIC(18,0),
		numProfile NUMERIC(18,0),
		numAssignedTo NUMERIC(18,0),
		numRecordOwner NUMERIC(18,0),
		numItemCode NUMERIC(18,0),
		numItemClassification NUMERIC(18,0),
		numOppID NUMERIC(18,0),
		numOppBizDocID NUMERIC(18,0),
		numOppBizDocItemID NUMERIC(18,0),
		numUnitHour FLOAT,
		monTotAmount MONEY,
		monVendorCost MONEY,
		monAvgCost MONEY,
		numPartner NUMERIC(18,0)
	)

	-- GET FULLY PAID INVOICES WITHIN PAY PERIOD
	INSERT INTO 
		@TABLEPAID
	SELECT 
		OppM.numDivisionID,
		ISNULL(CompanyInfo.numCompanyType,0),
		ISNULL(CompanyInfo.vcProfile,0),
		OppM.numAssignedTo,
		OppM.numRecOwner,
		OppBizItems.numItemCode,
		Item.numItemClassification,
		OppM.numOppID,
		OppBizItems.numOppBizDocID,
		OppBizItems.numOppBizDocItemID,
		ISNULL(OppBizItems.numUnitHour,0),
		ISNULL(OppBizItems.monTotAmount,0),
		(ISNULL(OppMItems.monVendorCost,0) * ISNULL(OppBizItems.numUnitHour,0)),
		(ISNULL(OppMItems.monAvgCost,0) * ISNULL(OppBizItems.numUnitHour,0)),
		OppM.numPartner
	FROM 
		OpportunityBizDocs OppBiz
	INNER JOIN
		OpportunityMaster OppM
	ON
		OppBiz.numOppID = OppM.numOppID
	INNER JOIN
		DivisionMaster 
	ON
		OppM.numDivisionID = DivisionMaster.numDivisionID
	INNER JOIN
		CompanyInfo
	ON
		DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
	INNER JOIN
		OpportunityBizDocItems OppBizItems
	ON
		OppBiz.numOppBizDocsId = OppBizItems.numOppBizDocID
	INNER JOIN
		OpportunityItems OppMItems 
	ON
		OppBizItems.numOppItemID = OppMItems.numoppitemtCode
	INNER JOIN
		Item
	ON
		OppBizItems.numItemCode = Item.numItemCode
	LEFT JOIN
		BizDocComission BDC
	ON
		OppM.numOppId = BDC.numOppID 
		AND OppBiz.numOppBizDocsId = BDC.numOppBizDocId
		AND OppBizItems.numOppBizDocItemID = BDC.numOppBizDocItemID
	WHERE
		OppM.numDomainId = @numDomainID 
		AND OppM.tintOppType = 1
		AND OppM.tintOppStatus = 1
		AND OppBiz.bitAuthoritativeBizDocs = 1
		AND ISNULL(OppBiz.monAmountPaid,0) < OppBiz.monDealAmount
		AND (OppBiz.dtFromDate IS NOT NULL AND (CAST(DATEADD(MINUTE,-@ClientTimeZoneOffset,OppBiz.dtFromDate) AS DATE) BETWEEN @dtPayStart AND @dtPayEnd))
		AND OppBizItems.numItemCode NOT IN ( @numDiscountServiceItemID )  /*DO NOT INCLUDE DISCOUNT ITEM*/
		AND 1 = (
					CASE 
						WHEN OppBizItems.numItemCode = @numShippingServiceItemID 
						THEN 
							CASE 
								WHEN ISNULL(@bitIncludeTaxAndShippingInCommission,0) = 1
								THEN	
									1
								ELSE	
									0
							END
						ELSE 
							1 
					END
				)/*CONDITION TO INCLUDE/EXCLUDE INCLUDE SHIPPING ITEM */
		AND BDC.numOppBizDocItemID IS NULL --NO ENTRY AVAILABLE IN BizDocComission TABLE FOR BIZDOC ITEM


		-- LOOP ALL COMMISSION RULES
		DECLARE @i INT = 1
		DECLARE @COUNT INT = 0

		SELECT @COUNT=COUNT(*) FROM @TempCommissionRule

		DECLARE @TEMPITEM TABLE
		(
			ID INT,
			UniqueID INT,
			numUserCntID NUMERIC(18,0),
			numOppID NUMERIC(18,0),
			numOppBizDocID NUMERIC(18,0),
			numOppBizDocItemID NUMERIC(18,0),
			numItemCode NUMERIC(18,0),
			numUnitHour FLOAT,
			monTotAmount MONEY,
			monVendorCost MONEY,
			monAvgCost MONEY
		)

		WHILE @i <= @COUNT
		BEGIN
			DECLARE @numComRuleID NUMERIC(18,0)
			DECLARE @tintComBasedOn TINYINT
			DECLARE @tintComAppliesTo TINYINT
			DECLARE @tintComOrgType TINYINT
			DECLARE @tintComType TINYINT
			DECLARE @tintAssignTo TINYINT
			DECLARE @decCommission MONEY
			DECLARE @numTotalAmount AS MONEY
			DECLARE @numTotalUnit AS FLOAT
			DECLARE @RowCount AS INT

			--CLEAR PREVIOUS VALUES FROM TEMP TABLE
			DELETE FROM @TEMPITEM

			--FETCH COMMISSION RULE
			SELECT @numComRuleID=numComRuleID,@tintComBasedOn=tintComBasedOn,@tintComType=tintComType,@tintComAppliesTo=tintComAppliesTo,@tintComOrgType=tintComOrgType,@tintAssignTo=tintAssignTo FROM @TempCommissionRule WHERE ID=@i

			--FIND ALL BIZ DOC ITEMS WHERE COMMISSION IS NOT PAID AND CURRENT RULE ITEMS, ORGANIZATION AND ASSINGED TO FILTER CAN BE APPLIED
			INSERT INTO
				@TEMPITEM
			SELECT
				ROW_NUMBER() OVER (ORDER BY ID),
				ID,
				(CASE @tintAssignTo WHEN 1 THEN numAssignedTo WHEN 2 THEN numRecordOwner ELSE numPartner END),
				numOppID,
				numOppBizDocID,
				numOppBizDocItemID,
				numItemCode,
				numUnitHour,
				monTotAmount,
				monVendorCost,
				monAvgCost
			FROM
				@TABLEPAID
			WHERE
				1 = (CASE @tintComAppliesTo
						--SPECIFIC ITEMS
						WHEN 1 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleItems WHERE numComRuleID = @numComRuleID AND ISNULL(numValue,0) = numItemCode) > 0 THEN 1 ELSE 0 END
						-- ITEM WITH SPECIFIC CLASSIFICATIONS
						WHEN 2 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleItems WHERE numComRuleID = @numComRuleID AND ISNULL(numValue,0) = numItemClassification) > 0 THEN 1 ELSE 0 END
						-- ALL ITEMS
						ELSE 1
					END)
				AND 1 = (CASE @tintComOrgType
							-- SPECIFIC ORGANIZATION
							WHEN 1 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleOrganization WHERE numComRuleID = @numComRuleID AND ISNULL(numValue,0) = numDivisionID) > 0 THEN 1 ELSE 0 END
							-- ORGANIZATION WITH SPECIFIC RELATIONSHIP AND PROFILE
							WHEN 2 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleOrganization WHERE numComRuleID = @numComRuleID AND ISNULL(numValue,0) = numRelationship AND ISNULL(numProfile,0) = numProfile) > 0 THEN 1 ELSE 0 END
							-- ALL ORGANIZATIONS
							ELSE 1
						END)
				AND 1 = (CASE @tintAssignTo 
							-- ORDER ASSIGNED TO
							WHEN 1 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleContacts WHERE numComRuleID = @numComRuleID AND numValue=numAssignedTo) > 0 THEN 1 ELSE 0 END
							-- ORDER OWNER
							WHEN 2 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleContacts WHERE numComRuleID = @numComRuleID AND numValue=numRecordOwner) > 0 THEN 1 ELSE 0 END
							-- ORDER OWNER
							WHEN 3 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleContacts WHERE numComRuleID = @numComRuleID AND numValue=numPartner) > 0 THEN 1 ELSE 0 END
							-- NO OPTION SELECTED IN RULE
							ELSE 0
						END)

			-- IF BIZDOC ITEMS ARE FOUND AFTER FILTER
			IF (SELECT COUNT(*) FROM @TEMPITEM) > 0
			BEGIN
				--CHECK COMMISSION BASED ON OPTION OF RULE
				--BASED ON AMOUNT SOLD
				IF @tintComBasedOn = 1
				BEGIN
					SELECT @numTotalAmount = SUM(monTotAmount) FROM @TEMPITEM

					IF (SELECT COUNT(*) FROM CommissionRuleDtl WHERE numComRuleID=@numComRuleID AND (@numTotalAmount BETWEEN intFrom AND intTo)) > 0
					BEGIN
						SELECT TOP 1 @decCommission=ISNULL(decCommission,0) FROM CommissionRuleDtl WHERE numComRuleID=@numComRuleID AND (@numTotalAmount BETWEEN intFrom AND intTo)
					END
				END
				--BASED ON UNITS SOLD
				ELSE IF @tintComBasedOn = 2
				BEGIN
					SELECT @numTotalUnit = SUM(numUnitHour) FROM @TEMPITEM

					IF (SELECT COUNT(*) FROM CommissionRuleDtl WHERE numComRuleID=@numComRuleID AND (@numTotalUnit BETWEEN intFrom AND intTo)) > 0
					BEGIN
						SELECT TOP 1 @decCommission=ISNULL(decCommission,0) FROM CommissionRuleDtl WHERE numComRuleID=@numComRuleID AND (@numTotalUnit BETWEEN intFrom AND intTo)
					END
				END

				--IF COMMISSION AMOUNT FOUND AFTER CHECKING COMMISSION FROM AND TO RANGE IN ABOVE STEP
				IF ISNULL(@decCommission,0) > 0
				BEGIN
					SELECT @RowCount=COUNT(*) FROM @TEMPITEM

					--CALCULATE COMMISSION AND STORE IN TEMPARORY TABLE
					INSERT INTO @TempBizCommission 
					(
						numUserCntID,
						numOppID,
						numOppBizDocID,
						numOppBizDocItemID,
						monCommission,
						numComRuleID,
						tintComType,
						tintComBasedOn,
						decCommission,
						tintAssignTo
					)
					SELECT
						numUserCntID,
						numOppID,
						numOppBizDocID,
						numOppBizDocItemID,
						CASE @tintComType 
							WHEN 1 --PERCENT
								THEN 
									CASE @tintCommissionType
										--TOTAL AMOUNT PAID
										WHEN 1 THEN monTotAmount * (@decCommission / 100)
										--ITEM GROSS PROFIT (VENDOR COST)
										WHEN 2 THEN (ISNULL(monTotAmount,0) - ISNULL(monVendorCost,0)) * (@decCommission / 100)
										--ITEM GROSS PROFIT (AVERAGE COST)
										WHEN 3 THEN(ISNULL(monTotAmount,0) - ISNULL(monAvgCost,0)) * (@decCommission / 100)
									END
							ELSE  --FLAT: COMMISSION AMOUNT IS EQUALLY DISTRIBUTED AMONG FOUND ENTERIES
								(@decCommission / @RowCount)
						END,
						@numComRuleID,
						@tintComType,
						@tintComBasedOn,
						@decCommission,
						@tintAssignTo
					FROM 
						@TEMPITEM AS T1
					WHERE
						1 = (CASE @tintCommissionType 
								--TOTAL PAID INVOICE
								WHEN 1 THEN CASE WHEN (ISNULL(monTotAmount,0) > 0) THEN 1 ELSE 0 END 
								-- ITEM GROSS PROFIT (VENDOR COST)
								WHEN 2 THEN CASE WHEN ((ISNULL(monTotAmount,0) - ISNULL(monVendorCost,0)) > 0) THEN 1 ELSE 0 END 
								-- ITEM GROSS PROFIT (AVERAGE COST)
								WHEN 3 THEN CASE WHEN ((ISNULL(monTotAmount,0) - ISNULL(monAvgCost,0)) > 0) THEN 1 ELSE 0 END
								ELSE
									0 
							END) 
						AND (
								SELECT 
									COUNT(*) 
								FROM 
									@TempBizCommission 
								WHERE 
									numUserCntID=T1.numUserCntID 
									AND numOppID=T1.numOppID 
									AND numOppBizDocID = T1.numOppBizDocID 
									AND numOppBizDocItemID = T1.numOppBizDocItemID 
							) = 0
				END
			END

			SET @i = @i + 1

			SET @numComRuleID = 0
			SET @tintComBasedOn = 0
			SET @tintComAppliesTo = 0
			SET @tintComOrgType = 0
			SET @tintComType = 0
			SET @tintAssignTo = 0
			SET @decCommission = 0
			SET @numTotalAmount = 0
			SET @numTotalUnit = 0
			SET @RowCount = 0
		END

		--INSERT CALCULATED ENTERIES IN BIZDOCCOMMISSION TABLE
		INSERT INTO BizDocComission
		(	
			numDomainID,
			numUserCntID,
			numOppBizDocId,
			numComissionAmount,
			numOppBizDocItemID,
			numComRuleID,
			tintComType,
			tintComBasedOn,
			decCommission,
			numOppID,
			bitFullPaidBiz,
			numComPayPeriodID,
			tintAssignTo
		)
		SELECT
			@numDomainID,
			numUserCntID,
			numOppBizDocID,
			monCommission,
			numOppBizDocItemID,
			numComRuleID,
			tintComType,
			tintComBasedOn,
			decCommission,
			numOppID,
			0,
			@numComPayPeriodID,
			tintAssignTo
		FROM
			@TempBizCommission
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
END

GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                                      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_CreateExtranetAccountforContact')
DROP PROCEDURE USP_CreateExtranetAccountforContact
GO
CREATE PROCEDURE [dbo].[USP_CreateExtranetAccountforContact]  
(
	@numDomainID as numeric(9),
	@vcPassword as varchar(100),
	@numContactID as numeric(9)
) AS
BEGIN
	
	declare @numDivisionID as numeric(9)
	declare @numExtranetID as numeric(9)

	select @numDivisionID=numDivisionID from AdditionalContactsInformation where numContactID=@numContactID

	select @numExtranetID=numExtranetID from ExtarnetAccounts where numDivisionID=@numDivisionID
	IF NOT EXISTS(SELECT * FROM dbo.ExtranetAccountsDtl WHERE numDomainID=@numDomainID AND numContactID=@numContactID)
	BEGIN
		insert into ExtranetAccountsDtl (numExtranetID,numContactID,vcPassword,tintAccessAllowed,numDomainID)
		values (@numExtranetID,@numContactID,@vcPassword,1,@numDomainID)	
	END
	--PRINT ISNULL(@numExtranetID,0)
	select ISNULL(@numExtranetID,0)
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_EcommerceRelationshipProfile_Delete')
DROP PROCEDURE dbo.USP_EcommerceRelationshipProfile_Delete
GO
CREATE PROCEDURE [dbo].[USP_EcommerceRelationshipProfile_Delete]
(
	@numSiteID NUMERIC(18,0)
	,@vcIds VARCHAR(1000)
)
AS 
BEGIN
	DELETE FROM EcommerceRelationshipProfile WHERE numSiteID=@numSiteID AND ID IN (SELECT Id FROM dbo.SplitIDs(@vcIds,','))
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_EcommerceRelationshipProfile_GetBySiteID')
DROP PROCEDURE dbo.USP_EcommerceRelationshipProfile_GetBySiteID
GO
CREATE PROCEDURE [dbo].[USP_EcommerceRelationshipProfile_GetBySiteID]
(
	@numSiteID NUMERIC(18,0)
)
AS 
BEGIN
	SELECT
		ID
		,dbo.GetListIemName(numRelationship) vcRelationship
		,dbo.GetListIemName(numProfile) vcProfile
		,STUFF((SELECT 
					', ' + vcData
				FROM 
					ECommerceItemClassification EIC 
				INNER JOIN 
					ListDetails 
				ON 
					EIC.numItemClassification = ListDetails.numListItemID 
				WHERE 
					EIC.numECommRelatiionshipProfileID=ERP.ID
				FOR XML PATH(''),TYPE 
				).value('.','VARCHAR(MAX)') 
			  ,1,2,'') vcItemClassifications
	FROM
		EcommerceRelationshipProfile ERP
	WHERE
		numSiteID=@numSiteID
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_EcommerceRelationshipProfile_Save')
DROP PROCEDURE dbo.USP_EcommerceRelationshipProfile_Save
GO
CREATE PROCEDURE [dbo].[USP_EcommerceRelationshipProfile_Save]
(
	@numSiteID NUMERIC(18,0)
	,@numRelationship NUMERIC(18,0)
	,@numProfile NUMERIC(18,0)
	,@vcItemClassificationIds VARCHAR(500)
)
AS 
BEGIN
	IF ISNULL(@numSiteID,0) = 0 OR NOT EXISTS (SELECT * FROM Sites WHERE numSiteID = @numSiteID)
	BEGIN
		RAISERROR('SITE_DOES_NOT_EXISTS',16,1)
		RETURN
	END
	ELSE IF ISNULL(@numRelationship,0) = 0
	BEGIN
		RAISERROR('RELATIONSHIP_REQUIRED',16,1)
		RETURN
	END
	ELSE IF ISNULL(@numProfile,0) = 0
	BEGIN
		RAISERROR('PROFILE_REQUIRED',16,1)
		RETURN
	END

	DECLARE @TEMP TABLE
	(
		numItemClassification NUMERIC(18,0)
	)

	INSERT INTO @TEMP SELECT Id FROM dbo.SplitIDs(@vcItemClassificationIds,',')

	IF (SELECT COUNT(*) FROM @TEMP) = 0
	BEGIN
		RAISERROR('ITEMCLASSIFICATION_REQUIRED',16,1)
		RETURN
	END

	IF EXISTS (SELECT 
				* 
				FROM 
					EcommerceRelationshipProfile 
				INNER JOIN
					ECommerceItemClassification
				ON
					EcommerceRelationshipProfile.ID=ECommerceItemClassification.[numECommRelatiionshipProfileID]
				WHERE 
					numSiteID=@numSiteID 
					AND numRelationship=@numRelationship 
					AND numProfile=@numProfile 
					AND numItemClassification IN (SELECT numItemClassification FROM @TEMP)
				)
	BEGIN
		RAISERROR('DUPLICATE_SELECTION',16,1)
		RETURN
	END

	INSERT INTO EcommerceRelationshipProfile
	(
		[numSiteID]
		,[numRelationship]
		,[numProfile]
	)
	VALUES
	(
		@numSiteID
		,@numRelationship
		,@numProfile
	)

	DECLARE @ID INT = SCOPE_IDENTITY()

	INSERT INTO ECommerceItemClassification
	(
		[numECommRelatiionshipProfileID]
		,[numItemClassification]
	)
	SELECT
		@ID
		,numItemClassification
	FROM
		@TEMP
END
/****** Object:  StoredProcedure [dbo].[USP_EcommerceSettings]    Script Date: 03/25/2009 16:46:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj              
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_ecommercesettings')
DROP PROCEDURE usp_ecommercesettings
GO
CREATE PROCEDURE [dbo].[USP_EcommerceSettings]              
@numDomainID as numeric(9),              
@vcPaymentGateWay as int ,                
@vcPGWUserId as varchar(100)='' ,                
@vcPGWPassword as varchar(100),              
@bitShowInStock as bit,              
@bitShowQOnHand as bit,
@bitCheckCreditStatus as BIT,
@numDefaultWareHouseID as numeric(9),
@numRelationshipId as numeric(9),
@numProfileId as numeric(9),
@bitHidePriceBeforeLogin	BIT,
@bitAuthOnlyCreditCard BIT=0,
@bitSendMail BIT = 1,
@vcGoogleMerchantID VARCHAR(1000)  = '',
@vcGoogleMerchantKey VARCHAR(1000) = '',
@IsSandbox BIT ,
@numAuthrizedOrderStatus NUMERIC,
@numSiteID numeric(18, 0),
@vcPaypalUserName VARCHAR(50)  = '',
@vcPaypalPassword VARCHAR(50)  = '',
@vcPaypalSignature VARCHAR(500)  = '',
@IsPaypalSandbox BIT,
@bitSkipStep2 BIT = 0,
@bitDisplayCategory BIT = 0,
@bitShowPriceUsingPriceLevel BIT = 0,
@bitEnableSecSorting BIT = 0,
@bitSortPriceMode   BIT=0,
@numPageSize    INT=15,
@numPageVariant  INT=6,
@bitAutoSelectWarehouse BIT =0,
@bitPreSellUp BIT = 0, 
@bitPostSellUp BIT = 0, 
@numDefaultClass NUMERIC(18,0) = 0,
@vcPreSellUp VARCHAR(200)=NULL,
@vcPostSellUp VARCHAR(200)=NULL,
@vcRedirectThankYouUrl VARCHAR(300)=NULL,
@tintPreLoginProceLevel TINYINT = 0
as              
BEGIN	        
if not exists(select 'col1' from eCommerceDTL where numDomainID=@numDomainID AND [numSiteId] = @numSiteID)
BEGIN           
INSERT INTO eCommerceDTL
           (numDomainId,
            vcPaymentGateWay,
            vcPGWManagerUId,
            vcPGWManagerPassword,
            bitShowInStock,
            bitShowQOnHand,
            numDefaultWareHouseID,
            bitCheckCreditStatus,
            [numRelationshipId],
            [numProfileId],
            [bitHidePriceBeforeLogin],
            bitAuthOnlyCreditCard,
            bitSendEmail,
            vcGoogleMerchantID,
            vcGoogleMerchantKey,
            IsSandbox ,
            numAuthrizedOrderStatus,
			numSiteId,
			vcPaypalUserName,
			vcPaypalPassword,
			vcPaypalSignature,
			IsPaypalSandbox,
			bitSkipStep2,
			bitDisplayCategory,
			bitShowPriceUsingPriceLevel,
			bitEnableSecSorting,
			bitSortPriceMode,
			numPageSize,
			numPageVariant,
			bitAutoSelectWarehouse,
			[bitPreSellUp],
			[bitPostSellUp],
			numDefaultClass,
			vcPreSellUp,
			vcPostSellUp,
			vcRedirectThankYouUrl,
			tintPreLoginProceLevel
			)

VALUES     (@numDomainID,
            @vcPaymentGateWay,
            @vcPGWUserId,
            @vcPGWPassword,
            @bitShowInStock,
            @bitShowQOnHand,
            @numDefaultWareHouseID,
            @bitCheckCreditStatus,
            @numRelationshipId,
            @numProfileId,
            @bitHidePriceBeforeLogin,
            @bitAuthOnlyCreditCard,
            @bitSendMail,
            @vcGoogleMerchantID,
            @vcGoogleMerchantKey ,
            @IsSandbox,
            @numAuthrizedOrderStatus,
			@numSiteID,
			@vcPaypalUserName,
			@vcPaypalPassword,
			@vcPaypalSignature,
			@IsPaypalSandbox,
			@bitSkipStep2,
			@bitDisplayCategory,
			@bitShowPriceUsingPriceLevel,
			@bitEnableSecSorting,
			@bitSortPriceMode,
			@numPageSize,
			@numPageVariant,
			@bitAutoSelectWarehouse,
			@bitPreSellUp,
			@bitPostSellUp,
			@numDefaultClass,
			@vcPreSellUp,
			@vcPostSellUp,
			@vcRedirectThankYouUrl,
			@tintPreLoginProceLevel
            )

	IF ISNULL((SELECT numDefaultSiteID FROM DOmain WHERE numDomainId=@numDomainID ),0) = 0
	BEGIN
		UPDATE Domain SET numDefaultSiteID=SCOPE_IDENTITY() WHERE numDomainId=@numDomainID
	END
END
 else            
 update eCommerceDTL                                   
   set              
 vcPaymentGateWay=@vcPaymentGateWay,                
 vcPGWManagerUId=@vcPGWUserId ,                
 vcPGWManagerPassword= @vcPGWPassword,              
 bitShowInStock=@bitShowInStock ,               
 bitShowQOnHand=@bitShowQOnHand,        
 numDefaultWareHouseID =@numDefaultWareHouseID,
 bitCheckCreditStatus=@bitCheckCreditStatus ,
 numRelationshipId = @numRelationshipId,
 numProfileId = @numProfileId,
 [bitHidePriceBeforeLogin]=@bitHidePriceBeforeLogin,
 bitAuthOnlyCreditCard=@bitAuthOnlyCreditCard,
 bitSendEmail = @bitSendMail,
 vcGoogleMerchantID = @vcGoogleMerchantID ,
 vcGoogleMerchantKey =  @vcGoogleMerchantKey ,
 IsSandbox = @IsSandbox,
 numAuthrizedOrderStatus = @numAuthrizedOrderStatus,
	numSiteId = @numSiteID,
vcPaypalUserName = @vcPaypalUserName ,
vcPaypalPassword = @vcPaypalPassword,
vcPaypalSignature = @vcPaypalSignature,
IsPaypalSandbox = @IsPaypalSandbox,
bitSkipStep2 = @bitSkipStep2,
bitDisplayCategory = @bitDisplayCategory,
bitShowPriceUsingPriceLevel = @bitShowPriceUsingPriceLevel,
bitEnableSecSorting = @bitEnableSecSorting,
bitSortPriceMode=@bitSortPriceMode,
numPageSize=@numPageSize,
numPageVariant=@numPageVariant,
bitAutoSelectWarehouse=@bitAutoSelectWarehouse,
[bitPreSellUp] = @bitPreSellUp,
[bitPostSellUp] = @bitPostSellUp,
numDefaultClass=@numDefaultClass,
vcPreSellUp=@vcPreSellUp,
vcPostSellUp=@vcPostSellUp,
vcRedirectThankYouUrl=@vcRedirectThankYouUrl,
tintPreLoginProceLevel=@tintPreLoginProceLevel
 where numDomainId=@numDomainID AND [numSiteId] = @numSiteID
END
Go
/****** Object:  StoredProcedure [dbo].[USP_ExtranetLogin]    Script Date: 07/26/2008 16:15:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec USP_ExtranetLogin @Email = 'john@jinco.com', @Password = '12345' ,@numDomainID=72
--Create By Anoop jayaraj                                          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_extranetlogin')
DROP PROCEDURE usp_extranetlogin
GO
CREATE PROCEDURE [dbo].[USP_ExtranetLogin]                                              
@Email as varchar(100),
@Password as varchar(100),
@numDomainID as numeric(9)                                             
as                                              
declare @numContactId as numeric(9)                                              
declare @numDivisionId as numeric(9)                                              
declare @numcompanyID as numeric(9)                                              
declare @numGroupID as numeric(9)                                              
declare @tintCRMType as numeric(9)                                                           
declare @vcCompanyName as varchar(100)                                          
declare @AccOwner as numeric(9)                                        
declare @AccOwnerName as varchar(100)                                       
declare @numExtranetDtlID as varchar(15)                                             
declare @numNoOfTimes as varchar(15)                        
declare @numPartnerGroup as numeric(9)                        
declare @bitPartnerAccess as bit                        
declare @numRelationShip as numeric(9)                        
declare @numProfileID as numeric(9)
declare @numContactType as numeric(9)                      
--declare @vcHomePage as varchar(1000)                     
declare @UserEmail as varchar(100)                   
declare @ContactName as varchar(100)         
declare @vcFirstName AS VARCHAR(100)
declare @vcLastName AS VARCHAR(100)                  
declare @count as tinyint       

                
select @count=count(*)  from AdditionalContactsInformation A                                  
join ExtranetAccountsDtl E                                  
on A.numContactID=E.numContactID                                  
where (E.vcUserName=@Email OR vcemail=@Email) and vcPassword=@Password and tintAccessAllowed=1 and A.numDomainID= @numDomainID
print  @count                                 
if @count=1             ---checking whether an user exists with the username and password.Multiple results will be rejected                                 
begin                                         
 select @numContactId=A.numContactId,@ContactName=vcFirstName+' '+vcLastName,@numDivisionId=numDivisionId,@numContactType=numContactType,@UserEmail=isnull(vcEmail,''),@vcFirstName=ISNULL(vcFirstName,''),@vcLastName=ISNULL(vcLastName,'') from AdditionalContactsInformation A                                  
 join ExtranetAccountsDtl E                                  
 on A.numContactID=E.numContactID                                  
 where (E.vcUserName=@Email OR vcemail=@Email) and vcPassword=@Password AND E.numDomainID=@numDomainID AND E.tintAccessAllowed=1
                                  
select @numcompanyID=numCompanyID,@tintCRMType=tintCRMType,@AccOwner=numrecowner from divisionmaster where numDivisionID=@numDivisionId                                              
                                              
select @numDomainID=numDomainID,@vcCompanyName=vcCompanyName ,@numRelationShip=numCompanyType,@numProfileID=[vcProfile] from companyInfo where numCompanyID=@numcompanyID                                              
select @AccOwnerName=isnull(vcFirstName,'')+' '+isnull(vcLastName,'') from AdditionalContactsInformation where numContactID=@AccOwner                       
                      
--select @vcHomePage=vcHomePage from HomePage where numDomainID=@numDomainID and numRelationship=@numRelationShip and numContactType=@numContactType                                         
select @numNoOfTimes=numNoOfTimes,@bitPartnerAccess=bitPartnerAccess,@numPartnerGroup=numPartnerGroupID,@numExtranetDtlID=numExtranetDtlID,@numGroupID=numGroupID from ExtarnetAccounts hdr                                              
join ExtranetAccountsDtl Dtl                                              
on hdr.numExtranetID=Dtl.numExtranetID where numDivisionId=@numDivisionId and tintAccessAllowed=1 and numContactID=@numContactId and vcPassword=@Password            
if @bitPartnerAccess=0 set @numPartnerGroup=0                             


              
select 1 ,@numContactId,@numDivisionId,@numcompanyID,@numGroupID,@tintCRMType,@numDomainID,@vcCompanyName,@AccOwner,@AccOwnerName,isnull(@numPartnerGroup,0),isnull(@numRelationShip,0) ,/* isnull(@vcHomePage,'')*/ '',              
@UserEmail ,@ContactName ContactName,isnull(@bitPartnerAccess,0),               
tintCustomPagingRows,               
vcDateFormat,               
numDefCountry,               
tintAssignToCriteria,               
bitIntmedPage,            
isnull(vcPortalLogo,'') vcPortalLogo,               
tintFiscalStartMonth,            
 dateadd(day,-sintStartDate,getutcdate()) as StartDate,                    
 dateadd(day,sintNoofDaysInterval,dateadd(day,-sintStartDate,getutcdate())) as EndDate,  
case when bitPSMTPServer= 1 then vcPSMTPServer else '' end as vcSMTPServer ,     
case when bitPSMTPServer= 1 then isnull(numPSMTPPort,0) else 0 end as numSMTPPort 
,isnull(bitPSMTPAuth,0) bitSMTPAuth  
,isnull([vcPSmtpPassword],'')vcSmtpPassword  
,isnull(bitPSMTPSSL,0) bitSMTPSSL   ,isnull(bitPSMTPServer,0) bitSMTPServer  ,isnull(vcPSMTPUserName,'') vcSMTPUserName ,
isnull(numCurrencyID,0) numCurrencyID, isnull(vcCurrency ,'') vcCurrency,vcDomainName,
isnull(@numProfileID,0) numProfileID,
ISNULL(bitCustomizePortal,0) bitCustomizePortal,
ISNULL(vcPortalName,'') vcPortalName,
@vcFirstName as vcFirstName,
@vcLastName as vcLastName
from Domain              
where numDomainID=@numDomainID                                     
                   
                                    
set @numNoOfTimes=convert(numeric,isnull(@numNoOfTimes,0))+1                                    
update ExtranetAccountsDtl set bintLastLoggedIn=getutcdate(),numNoOfTimes=@numNoOfTimes  where numExtranetDtlID=@numExtranetDtlID                                   
                                   
end                                           
select @count
GO
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetChartAcntDetailsForBalanceSheet_New')
DROP PROCEDURE USP_GetChartAcntDetailsForBalanceSheet_New
GO
CREATE PROCEDURE [dbo].[USP_GetChartAcntDetailsForBalanceSheet_New]                                 
@numDomainId AS NUMERIC(9),                                                  
@dtFromDate AS DATETIME,                                                
@dtToDate AS DATETIME,
@ClientTimeZoneOffset INT, 
@numAccountClass AS NUMERIC(9)=0,
@DateFilter AS VARCHAR(20),
@ReportColumn AS VARCHAR(20)                                                        
AS                                                                
BEGIN                                                                
	DECLARE @PLAccountStruc VARCHAR(500)
	DECLARE @FinancialYearStartDate AS DATETIME
	SELECT @FinancialYearStartDate = dbo.GetFiscalStartDate(YEAR(GETDATE()),@numDomainId)

	DECLARE @TEMPQuarter AS TABLE
	(
		ID INT,
		StartDate DATETIME,
		EndDate DATETIME
	)

	INSERT INTO @TEMPQuarter VALUES (1,@FinancialYearStartDate,DATEADD(ms,-3,DATEADD(QUARTER,1,@FinancialYearStartDate)))
	INSERT INTO @TEMPQuarter VALUES (2,DATEADD(QUARTER,1,@FinancialYearStartDate),DATEADD(ms,-3,DATEADD(QUARTER,2,@FinancialYearStartDate)))
	INSERT INTO @TEMPQuarter VALUES (3,DATEADD(QUARTER,2,@FinancialYearStartDate),DATEADD(ms,-3,DATEADD(QUARTER,3,@FinancialYearStartDate)))
	INSERT INTO @TEMPQuarter VALUES (4,DATEADD(QUARTER,3,@FinancialYearStartDate),DATEADD(ms,-3,DATEADD(QUARTER,4,@FinancialYearStartDate)))

	IF @DateFilter = 'CurYear'
	BEGIN
		SELECT @dtFromDate = @FinancialYearStartDate,
			@dtToDate = DATEADD(ms,-3,DATEADD(YEAR,1,@FinancialYearStartDate));
	END
	ELSE IF @DateFilter = 'PreYear'
	BEGIN
		SELECT @dtFromDate = DATEADD(YEAR,-1,@FinancialYearStartDate),
			@dtToDate = DATEADD(ms,-3,DATEADD(YEAR,1,DATEADD(YEAR,-1,@FinancialYearStartDate)));
	END
	ELSE IF @DateFilter = 'CurPreYear'
	BEGIN
		SELECT @dtFromDate = DATEADD(YEAR,-1,@FinancialYearStartDate),
			@dtToDate = DATEADD(ms,-3,DATEADD(YEAR,1,@FinancialYearStartDate));
	END
	ELSE IF @DateFilter = 'CuQur'
	BEGIN
		SELECT @dtFromDate = StartDate, @dtToDate=EndDate FROM @TEMPQuarter WHERE GetDate() BETWEEN StartDate AND EndDate
	END
	ELSE IF @DateFilter = 'CurPreQur'
	BEGIN
		SELECT @dtFromDate = DATEADD(QUARTER,-1,StartDate), @dtToDate=EndDate FROM @TEMPQuarter WHERE GetDate() BETWEEN StartDate AND EndDate
	END
	ELSE IF @DateFilter = 'PreQur'
	BEGIN
		SELECT @dtFromDate = DATEADD(QUARTER,-1,StartDate), @dtToDate=DATEADD(QUARTER,-1,EndDate) FROM @TEMPQuarter WHERE GetDate() BETWEEN StartDate AND EndDate
	END
	ELSE IF @DateFilter = 'LastMonth'
	BEGIN
		select @dtFromDate = DATEADD(mm,-1,DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0)),
				@dtToDate = DATEADD(ms,-3,DATEADD(mm,0,DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0)));  
	END
	ELSE IF @DateFilter = 'ThisMonth'
	BEGIN
		select @dtFromDate = DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0),
				@dtToDate = DATEADD(ms,- 3,DATEADD(mm,0,DATEADD(mm,DATEDIFF(mm,0,GETDATE())+1,0)));  
	END
	ELSE IF @DateFilter = 'CurPreMonth'
	BEGIN
		select @dtFromDate = DATEADD(mm,-1,DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0)),
				@dtToDate = DATEADD(ms,- 3,DATEADD(mm,0,DATEADD(mm,DATEDIFF(mm,0,GETDATE())+1,0))); 
	END

	CREATE TABLE #VIEW_JOURNALBS
	(
		numDomainID NUMERIC(18,0),
		numAccountID NUMERIC(18,0),
		numAccountClass NUMERIC(18,0),
		COAvcAccountCode VARCHAR(50),
		datEntry_Date DATETIME,
		Debit MONEY,
		Credit MONEY
	)

	INSERT INTO #VIEW_JOURNALBS SELECT numDomainID,numAccountID,numAccountClass,COAvcAccountCode,datEntry_Date,Debit,Credit FROM VIEW_JOURNALBS_MASTER WHERE numDomainID =@numDomainId AND (numAccountClass=@numAccountClass OR @numAccountClass=0) ORDER BY numAccountId; 

	DECLARE @view_journal TABLE
	(
		numAccountId NUMERIC(18,0),
		vcAccountCode VARCHAR(50),
		datEntry_Date SMALLDATETIME,
		Debit MONEY,
		Credit MONEY
	)

	INSERT INTO @view_journal SELECT numAccountId,AccountTypeCode,datEntry_Date,Debit,Credit FROM VIEW_Journal_Master WHERE numDomainId = @numDomainID AND (numClassIDDetail=@numAccountClass OR @numAccountClass=0);
	
	DECLARE @PLCHARTID NUMERIC(8)
	SELECT @PLCHARTID = numAccountId FROM dbo.Chart_Of_Accounts WHERE numDomainId=@numDomainID AND bitProfitLoss=1

	DECLARE @PLOPENING AS MONEY
	SELECT @PLOPENING = ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM @view_journal VJ WHERE numAccountId=@PLCHARTID

	;WITH DirectReport (ParentId, vcCompundParentKey, numAccountTypeID, numAccountID, vcAccountType,vcAccountCode, LEVEL,Struc)
	AS
	(
		SELECT 
			CAST('' AS VARCHAR) AS ParentId, 
			CAST(CONCAT([ATD].[numAccountTypeID],'#',0) AS VARCHAR),
			[ATD].[numAccountTypeID],
			0,
			[ATD].[vcAccountType],
			[ATD].[vcAccountCode],
			1 AS LEVEL, 
			CAST(CAST([ATD].[vcAccountCode] AS VARCHAR) AS VARCHAR(300))  AS Struc
		FROM 
			[dbo].[AccountTypeDetail] AS ATD
		WHERE 
			[ATD].[numDomainID]=@numDomainId 
			AND [ATD].[vcAccountCode] IN ('0101','0102','0105')
		UNION ALL
		SELECT 
			D.vcCompundParentKey AS ParentId,
			CAST(CONCAT([ATD].[numAccountTypeID],'#',0) AS VARCHAR),
			[ATD].[numAccountTypeID],
			0,
			[ATD].[vcAccountType],
			[ATD].[vcAccountCode],
			LEVEL + 1, 
			CAST(d.Struc + '#' + cast([ATD].[vcAccountCode] AS VARCHAR) AS VARCHAR(300))  AS Struc
		FROM 
			[dbo].[AccountTypeDetail] AS ATD 
		JOIN 
			[DirectReport] D 
		ON 
			D.[numAccountTypeID] = [ATD].[numParentID]
		WHERE 
			[ATD].[numDomainID]=@numDomainId 
	),
	DirectReport1 (ParentId, vcCompundParentKey, numAccountTypeID, vcAccountType,vcAccountCode, LEVEL,numAccountId,Struc, bitProfitLoss)
	AS
	(
		SELECT 
			ParentId, 
			vcCompundParentKey,
			numAccountTypeID, 
			vcAccountType,
			vcAccountCode, 
			LEVEL,
			CAST(numAccountID AS NUMERIC(18)),
			Struc,
			CAST(0 AS BIT)
		FROM 
			DirectReport 
		UNION ALL
		SELECT 
			CAST(CONCAT(COA.numParntAcntTypeId,'#',0) AS VARCHAR) AS ParentId,
			CAST(CONCAT(COA.numParntAcntTypeId,'#',COA.numAccountId) AS VARCHAR),
			CAST(NULL AS NUMERIC(18)), 
			[vcAccountName],
			COA.[vcAccountCode],
			LEVEL + 1,
			[COA].[numAccountId], 
			CAST(d.Struc + '#' + CAST(COA.[numAccountId] AS VARCHAR) AS VARCHAR(300)) AS Struc,
			COA.bitProfitLoss
		FROM 
			[dbo].[Chart_of_Accounts] AS COA 
		JOIN 
			[DirectReport1] D 
		ON 
			D.[numAccountTypeID] = COA.[numParntAcntTypeId]
		WHERE 
			[COA].[numDomainID]=@numDomainId 
			AND ISNULL(COA.bitActive,0) = 1
			AND ISNULL(COA.bitIsSubAccount,0) = 0
		UNION ALL
		SELECT 
			CAST(CONCAT(COA.numParntAcntTypeId,'#',COA.numParentAccId) AS VARCHAR) AS ParentId,
			CAST(CONCAT(COA.numParntAcntTypeId,'#',COA.numAccountId) AS VARCHAR),
			CAST(NULL AS NUMERIC(18)),
			[vcAccountName],
			COA.[vcAccountCode],
			LEVEL + 1,
			[COA].[numAccountId], 
			CAST(d.Struc + '#' + CAST(COA.[numAccountId] AS VARCHAR) AS VARCHAR(300)) AS Struc,
			COA.bitProfitLoss
		FROM 
			[dbo].[Chart_of_Accounts] AS COA 
		JOIN 
			[DirectReport1] D 
		ON 
			D.numAccountId = COA.numParentAccId
		WHERE 
			[COA].[numDomainID]=@numDomainId 
			AND ISNULL(COA.bitActive,0) = 1
			AND ISNULL(COA.bitIsSubAccount,0) = 1
	)

  
	SELECT 
		ParentId,
		vcCompundParentKey,
		numAccountTypeID, 
		vcAccountType, 
		LEVEL, 
		vcAccountCode,
		numAccountId,
		Struc,
		bitProfitLoss
	INTO 
		#tempDirectReport
	FROM 
		DirectReport1

	SELECT 
		COA.ParentId, 
		COA.vcCompundParentKey,
		COA.numAccountTypeID, 
		COA.vcAccountType, 
		COA.LEVEL, 
		COA.vcAccountCode, 
		COA.numAccountId, 
		COA.Struc,
		(CASE 
			WHEN COA.vcAccountCode LIKE '0105%' AND COA.numAccountId <> @PLCHARTID THEN (ISNULL(Debit,0) * (-1) - ISNULL(Credit,0) * (-1)) 
			ELSE ISNULL(Debit,0) - ISNULL(Credit,0) 
		END) AS Amount,
		V.datEntry_Date
	INTO 
		#tempViewData
	FROM 
		#tempDirectReport COA 
	LEFT JOIN 
		#VIEW_JOURNALBS V 
	ON  
		V.COAvcAccountCode like COA.vcAccountCode + '%' 
		AND datEntry_Date <= @dtToDate
	WHERE 
		COA.[numAccountId] IS NOT NULL
	
	DECLARE @columns VARCHAR(8000);--SET @columns = '';
	DECLARE @SUMColumns VARCHAR(8000);--SET @SUMColumns = '';
	DECLARE @PivotColumns VARCHAR(8000);--SET @PivotColumns = '';
	DECLARE @Select VARCHAR(8000)
	DECLARE @Where VARCHAR(8000)
	SET @Select = 'SELECT ParentId, vcCompundParentKey, numAccountTypeID, vcAccountType, LEVEL, vcAccountCode, numAccountId,Struc'


	DECLARE @sql VARCHAR(MAX) = ''

	IF @ReportColumn = 'Year'
	BEGIN
		CREATE TABLE #tempYearMonth
		(
			intYear INT,
			intMonth INT,
			MonthYear VARCHAR(50),
			StartDate DATETIME,
			EndDate DATETIME,
			ProfitLossAmount MONEY
		)

		;WITH CTE AS 
		(
			SELECT
				YEAR(@dtFromDate) AS 'yr',
				MONTH(@dtFromDate) AS 'mm',
				DATENAME(mm, @dtFromDate) AS 'mon',
				(DATENAME(mm, @dtFromDate) + '_' + CAST(YEAR(@dtFromDate) AS VARCHAR)) AS MonthYear,
				CAST(DATEFROMPARTS(YEAR(@dtFromDate),MONTH(@dtFromDate),1) AS DATETIME) dtStartDate,
				DATEADD(MS,-2,CAST(DATEADD(MONTH,1,DATEFROMPARTS(YEAR(@dtFromDate),MONTH(@dtFromDate),1)) AS DATETIME)) dtEndDate,
				@dtFromDate 'new_date'
			UNION ALL
			SELECT
				YEAR(DATEADD(d,1,new_date)) AS 'yr',
				MONTH(DATEADD(d,1,new_date)) AS 'mm',
				DATENAME(mm, DATEADD(d,1,new_date)) AS 'mon',
				(DATENAME(mm, DATEADD(d,1,new_date)) + '_' + CAST(YEAR(DATEADD(d,1,new_date)) AS VARCHAR)) AS MonthYear,
				CAST(DATEFROMPARTS(YEAR(DATEADD(d,1,new_date)),MONTH(DATEADD(d,1,new_date)),1) AS DATETIME) dtStartDate,
				DATEADD(MS,-2,CAST(DATEADD(MONTH,1,DATEFROMPARTS(YEAR(DATEADD(d,1,new_date)),MONTH(DATEADD(d,1,new_date)),1)) AS DATETIME)) dtEndDate,
				DATEADD(d,1,new_date) 'new_date'
			FROM CTE
			WHERE DATEADD(d,1,new_date) < @dtToDate
		)
	
		INSERT INTO 
			#tempYearMonth
		SELECT 
			yr,mm,MonthYear,dtStartDate,dtEndDate,0
		FROM 
			CTE
		GROUP BY 
			yr, mm, MonthYear, dtStartDate, dtEndDate
		ORDER BY 
			yr, mm
		OPTION (MAXRECURSION 5000)

		INSERT INTO #tempYearMonth VALUES (5000,5000,'Total',@dtFromDate,@dtToDate,0)

		UPDATE 
			#tempYearMonth
		SET
			EndDate = (CASE WHEN EndDate < @dtToDate THEN EndDate ELSE @dtToDate END)

		UPDATE 
			#tempYearMonth
		SET
			ProfitLossAmount = (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM @view_journal VJ WHERE vcAccountCode LIKE '0103%' AND datEntry_Date between StartDate and EndDate)
			+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM @view_journal VJ WHERE vcAccountCode LIKE '0104%' AND datEntry_Date between StartDate and EndDate)
			+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM @view_journal VJ WHERE vcAccountCode LIKE '0106%' AND datEntry_Date between StartDate and EndDate)
			+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM @view_journal VJ WHERE (vcAccountCode LIKE '0103%' OR vcAccountCode LIKE '0104%' OR vcAccountCode LIKE '0106%') AND datEntry_Date <=  DATEADD(Minute,-1,StartDate))
			+ @PLOPENING

		SELECT @columns = STUFF((SELECT ', ISNULL(' + MonthYear  + ',0) AS [' + REPLACE(MonthYear,'_',' ') + ']' FROM #tempYearMonth ORDER BY intYear,intMonth FOR XML PATH('')), 1, 2, ''); 
		SELECT @PivotColumns = STUFF((SELECT ', ' + MonthYear FROM #tempYearMonth ORDER BY intYear,intMonth FOR XML PATH('')), 1, 2, ''); 

		SELECT TOP 1 @PLAccountStruc= '#' + Struc + '#' FROM #tempDirectReport WHERe bitProfitLoss = 1

		SET @sql = 'SELECT
						ParentId,
						vcCompundParentKey,
						numAccountId,
						numAccountTypeID,
						vcAccountType,
						LEVEL,
						vcAccountCode,
						Struc,
						[Type],' + @columns + '
					FROM
					(
						SELECT 
							#tempDirectReport.ParentId,
							#tempDirectReport.vcCompundParentKey,
							#tempDirectReport.numAccountId,
							#tempDirectReport.numAccountTypeID,
							#tempDirectReport.vcAccountType,
							#tempDirectReport.LEVEL,
							#tempDirectReport.vcAccountCode,
							(''#'' + #tempDirectReport.Struc + ''#'') AS Struc,
							(CASE WHEN ISNULL(#tempDirectReport.numAccountId,0) > 0 THEN 1 ELSE 2 END) [Type],
							(CASE 
								WHEN bitProfitLoss = 1 
								THEN Period.ProfitLossAmount
								ELSE
									(CASE WHEN CHARINDEX((''#'' + #tempDirectReport.Struc + ''#''),''' + CAST(@PLAccountStruc AS VARCHAR) + ''') > 0 THEN Period.ProfitLossAmount ELSE 0 END) +
									ISNULL((SELECT sum(ISNULL(Debit,0)) FROM #VIEW_JOURNALBS VJ WHERE VJ.COAvcAccountCode like #tempDirectReport.vcAccountCode + ''%'' AND datEntry_Date <= Period.EndDate AND ISNULL(VJ.numAccountId,0) <> ' + CAST(@PLCHARTID AS VARCHAR(20))  + '),0) * (CASE WHEN #tempDirectReport.vcAccountCode LIKE ''0102%'' OR (#tempDirectReport.vcAccountCode LIKE ''0105%'' AND ISNULL(numAccountId,0) <> ' + CAST(@PLCHARTID AS VARCHAR) + ') THEN -1 ELSE 1 END) -
									ISNULL((SELECT sum(ISNULL(Credit,0)) FROM #VIEW_JOURNALBS VJ WHERE VJ.COAvcAccountCode like #tempDirectReport.vcAccountCode + ''%'' AND datEntry_Date <= Period.EndDate AND ISNULL(VJ.numAccountId,0) <> ' + CAST(@PLCHARTID AS VARCHAR(20))  + '),0) * (CASE WHEN #tempDirectReport.vcAccountCode LIKE ''0102%'' OR (#tempDirectReport.vcAccountCode LIKE ''0105%'' AND ISNULL(numAccountId,0) <> ' + CAST(@PLCHARTID AS VARCHAR) + ') THEN -1 ELSE 1 END)
							END) AS Amount,
							Period.MonthYear
						FROM 
							#tempDirectReport
						OUTER APPLY
						(
							SELECT
								MonthYear,
								StartDate,
								EndDate,
								ProfitLossAmount
							FROM
								#tempYearMonth
						) AS Period
					) AS SourceTable
					pivot
					(
						SUM(AMOUNT)
						FOR [MonthYear] IN ( ' + @pivotcolumns + ' )
					) AS P ORDER BY Struc, [Type] desc'

		EXECUTE (@sql)

		DROP TABLE #tempYearMonth
	END
	Else IF @ReportColumn = 'Quarter'
	BEGIN
		CREATE TABLE #TempYearMonthQuarter
		(
			intYear INT,
			intQuarter INT,
			MonthYear VARCHAR(50),
			StartDate DATETIME,
			EndDate DATETIME,
			ProfitLossAmount MONEY
		)

		;WITH CTE AS 
		(
			SELECT
				dbo.GetFiscalyear(@dtFromDate,@numDomainId) AS 'yr',
				dbo.GetFiscalQuarter(@dtFromDate,@numDomainId) AS 'qq',
				('Q' + cast(dbo.GetFiscalQuarter(@dtFromDate,@numDomainId) AS VARCHAR) + '_' + cast(dbo.GetFiscalyear(@dtFromDate,@numDomainId) AS VARCHAR)) AS MonthYear,
				CASE dbo.GetFiscalQuarter(@dtFromDate,@numDomainId) 
				WHEN 1 THEN dateadd(month,0,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId))
				WHEN 2 THEN dateadd(month,3,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId)) 
				WHEN 3 THEN dateadd(month,6,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId))
				WHEN 4 THEN dateadd(month,9,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId)) 
				END AS dtStartDate,
				CASE dbo.GetFiscalQuarter(@dtFromDate,@numDomainId) 
				WHEN 1 THEN dateadd(MS,-3,dateadd(month,3,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId)))
				WHEN 2 THEN dateadd(MS,-3,dateadd(month,6,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId)))
				WHEN 3 THEN dateadd(MS,-3,dateadd(month,9,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId)))
				WHEN 4 THEN dateadd(MS,-3,dateadd(month,12,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId)))
				END AS dtEndDate,
				@dtFromDate 'new_date'
			UNION ALL
			SELECT
				dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId) AS 'yr',
				dbo.GetFiscalQuarter(DATEADD(d,1,new_date),@numDomainId) AS 'qq',
				('Q' + cast(dbo.GetFiscalQuarter(DATEADD(d,1,new_date),@numDomainId) AS VARCHAR) + '_' + cast(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId) AS VARCHAR)) AS MonthYear,
				CASE dbo.GetFiscalQuarter(DATEADD(d,1,new_date),@numDomainId) 
				WHEN 1 THEN dateadd(month,0,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId))
				WHEN 2 THEN dateadd(month,3,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId)) 
				WHEN 3 THEN dateadd(month,6,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId))
				WHEN 4 THEN dateadd(month,9,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId)) 
				END AS dtStartDate,
				CASE dbo.GetFiscalQuarter(DATEADD(d,1,new_date),@numDomainId) 
				WHEN 1 THEN dateadd(MS,-3,dateadd(month,3,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId)))
				WHEN 2 THEN dateadd(MS,-3,dateadd(month,6,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId)))
				WHEN 3 THEN dateadd(MS,-3,dateadd(month,9,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId)))
				WHEN 4 THEN dateadd(MS,-3,dateadd(month,12,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId)))
				END AS dtEndDate,
				DATEADD(d,1,new_date) 'new_date'
			FROM CTE
			WHERE DATEADD(d,1,new_date) < @dtToDate
		)
	
		INSERT INTO 
			#TempYearMonthQuarter 		
		SELECT 
			yr,qq,MonthYear,dtStartDate,dtEndDate,0
		FROM 
			CTE
		GROUP BY 
			yr, qq, MonthYear, dtStartDate, dtEndDate
		ORDER BY 
			yr, qq
		OPTION
			(MAXRECURSION 5000)

		INSERT INTO #TempYearMonthQuarter VALUES (5000,5000,'Total',@dtFromDate,@dtToDate,0)

		UPDATE 
			#TempYearMonthQuarter
		SET
			EndDate = (CASE WHEN EndDate < @dtToDate THEN EndDate ELSE @dtToDate END)

		UPDATE 
			#TempYearMonthQuarter
		SET
			ProfitLossAmount = (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM @view_journal VJ WHERE vcAccountCode LIKE '0103%' AND datEntry_Date between StartDate and EndDate)
			+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM @view_journal VJ WHERE vcAccountCode LIKE '0104%' AND datEntry_Date between StartDate and EndDate)
			+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM @view_journal VJ WHERE vcAccountCode LIKE '0106%' AND datEntry_Date between StartDate and EndDate)
			+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM @view_journal VJ WHERE (vcAccountCode LIKE '0103%' OR vcAccountCode LIKE '0104%' OR vcAccountCode LIKE '0106%') AND datEntry_Date <=  DATEADD(Minute,-1,StartDate))
			+ @PLOPENING

		SELECT @columns = STUFF((SELECT ', ISNULL(' + MonthYear  + ',0) AS [' + REPLACE(MonthYear,'_',' ') + ']' FROM #TempYearMonthQuarter ORDER BY intYear,intQuarter FOR XML PATH('')), 1, 2, ''); 
		SELECT @PivotColumns = STUFF((SELECT ', ' + MonthYear FROM #TempYearMonthQuarter ORDER BY intYear,intQuarter FOR XML PATH('')), 1, 2, ''); 

		SELECT TOP 1 @PLAccountStruc= '#' + Struc + '#' FROM #tempDirectReport WHERe bitProfitLoss = 1

		SET @sql = 'SELECT
						ParentId,
						vcCompundParentKey,
						numAccountId,
						numAccountTypeID,
						vcAccountType,
						LEVEL,
						vcAccountCode,
						Struc,
						[Type],' + @columns + '
					FROM
					(
						SELECT 
							#tempDirectReport.ParentId,
							#tempDirectReport.vcCompundParentKey,
							#tempDirectReport.numAccountId,
							#tempDirectReport.numAccountTypeID,
							#tempDirectReport.vcAccountType,
							#tempDirectReport.LEVEL,
							#tempDirectReport.vcAccountCode,
							(''#'' + #tempDirectReport.Struc + ''#'') AS Struc,
							(CASE WHEN ISNULL(#tempDirectReport.numAccountId,0) > 0 THEN 1 ELSE 2 END) [Type],
							(CASE 
								WHEN bitProfitLoss = 1 
								THEN Period.ProfitLossAmount
								ELSE
									(CASE WHEN CHARINDEX((''#'' + #tempDirectReport.Struc + ''#''),''' + CAST(@PLAccountStruc AS VARCHAR) + ''') > 0 THEN Period.ProfitLossAmount ELSE 0 END) +
									ISNULL((SELECT sum(ISNULL(Debit,0)) FROM #VIEW_JOURNALBS VJ WHERE VJ.COAvcAccountCode like #tempDirectReport.vcAccountCode + ''%'' AND datEntry_Date <= Period.EndDate AND ISNULL(VJ.numAccountId,0) <> ' + CAST(@PLCHARTID AS VARCHAR(20))  + '),0) * (CASE WHEN #tempDirectReport.vcAccountCode LIKE ''0102%'' OR (#tempDirectReport.vcAccountCode LIKE ''0105%'' AND ISNULL(numAccountId,0) <> ' + CAST(@PLCHARTID AS VARCHAR) + ') THEN -1 ELSE 1 END) -
									ISNULL((SELECT sum(ISNULL(Credit,0)) FROM #VIEW_JOURNALBS VJ WHERE VJ.COAvcAccountCode like #tempDirectReport.vcAccountCode + ''%'' AND datEntry_Date <= Period.EndDate AND ISNULL(VJ.numAccountId,0) <> ' + CAST(@PLCHARTID AS VARCHAR(20)) + '),0) * (CASE WHEN #tempDirectReport.vcAccountCode LIKE ''0102%'' OR (#tempDirectReport.vcAccountCode LIKE ''0105%'' AND ISNULL(numAccountId,0) <> ' + CAST(@PLCHARTID AS VARCHAR) + ') THEN -1 ELSE 1 END)
							END) AS Amount,							
							Period.MonthYear
						FROM 
							#tempDirectReport
						OUTER APPLY
						(
							SELECT
								MonthYear,
								StartDate,
								EndDate,
								ProfitLossAmount
							FROM
								#TempYearMonthQuarter
						) AS Period
					) AS SourceTable
					pivot
					(
						SUM(AMOUNT)
						FOR [MonthYear] IN ( ' + @pivotcolumns + ' )
					) AS P ORDER BY Struc, [Type] desc'

		PRINT @sql
		EXECUTE (@sql)

		DROP TABLE #TempYearMonthQuarter
	END
	ELSE
	BEGIN
		DECLARE @ProifitLossAmount AS MONEY = 0

		SET @ProifitLossAmount = (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM @view_journal VJ WHERE vcAccountCode  LIKE '0103%' AND datEntry_Date between @dtFromDate and @dtToDate)
								+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM @view_journal VJ WHERE vcAccountCode  LIKE '0104%' AND datEntry_Date between @dtFromDate and @dtToDate)
								+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM @view_journal VJ WHERE vcAccountCode  LIKE '0106%' AND datEntry_Date between @dtFromDate and @dtToDate)
								+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM @view_journal VJ WHERE (vcAccountCode LIKE '0103%' OR vcAccountCode LIKE '0104%' OR vcAccountCode LIKE '0106%') AND datEntry_Date <=  DATEADD(MILLISECOND,-3,@dtFromDate))
								+ @PLOPENING		

		SELECT TOP 1 @PLAccountStruc= '#' + Struc + '#' FROM #tempDirectReport WHERe bitProfitLoss = 1
		
		SELECT 
			#tempDirectReport.ParentId,
			#tempDirectReport.vcCompundParentKey,
			#tempDirectReport.numAccountId,
			#tempDirectReport.numAccountTypeID,
			#tempDirectReport.vcAccountType,
			#tempDirectReport.LEVEL,
			#tempDirectReport.vcAccountCode,
			('#' + #tempDirectReport.Struc + '#') AS Struc,
			(CASE WHEN ISNULL(#tempDirectReport.numAccountId,0) > 0 THEN 1 ELSE 2 END) [Type],
			(CASE 
				WHEN bitProfitLoss = 1 
				THEN @ProifitLossAmount
				ELSE
					(CASE WHEN CHARINDEX(('#' + #tempDirectReport.Struc + '#'),@PLAccountStruc) > 0 THEN @ProifitLossAmount ELSE 0 END) +
					ISNULL((SELECT ISNULL(sum(Debit),0) FROM #VIEW_JOURNALBS VJ WHERE VJ.COAvcAccountCode like #tempDirectReport.vcAccountCode + '%' AND datEntry_Date <= @dtToDate AND ISNULL(VJ.numAccountId,0) <> @PLCHARTID),0) * (CASE WHEN #tempDirectReport.vcAccountCode LIKE '0102%' OR (#tempDirectReport.vcAccountCode LIKE '0105%' AND ISNULL(numAccountId,0) <> @PLCHARTID) THEN -1 ELSE 1 END) -
					ISNULL((SELECT ISNULL(sum(Credit),0) FROM #VIEW_JOURNALBS VJ WHERE VJ.COAvcAccountCode like #tempDirectReport.vcAccountCode + '%' AND datEntry_Date <= @dtToDate AND ISNULL(VJ.numAccountId,0) <> @PLCHARTID),0) * (CASE WHEN #tempDirectReport.vcAccountCode LIKE '0102%' OR (#tempDirectReport.vcAccountCode LIKE '0105%' AND ISNULL(numAccountId,0) <> @PLCHARTID) THEN -1 ELSE 1 END) 
			END) AS Amount
		FROM 
			#tempDirectReport
		ORDER BY 
			Struc, [Type] desc
	END

	DROP TABLE #tempViewData 
	DROP TABLE #tempDirectReport
	DROP TABLE #VIEW_JOURNALBS
END

/****** Object:  StoredProcedure [dbo].[USP_GetColumnConfiguration1]    Script Date: 07/26/2008 16:16:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'usp_getcolumnconfiguration1' )
    DROP PROCEDURE usp_getcolumnconfiguration1
GO
CREATE PROCEDURE [dbo].[USP_GetColumnConfiguration1]
    @numDomainID NUMERIC(9) ,
    @numUserCntId NUMERIC(9) ,
    @FormId TINYINT ,
    @numtype NUMERIC(9) ,
    @numViewID NUMERIC(9) = 0
AS
    DECLARE @PageId AS TINYINT         
    IF @FormId = 10
        OR @FormId = 44
        SET @PageId = 4          
    ELSE
        IF @FormId = 11
            OR @FormId = 34
            OR @FormId = 35
            OR @FormId = 36
            OR @FormId = 96
            OR @FormId = 97
            SET @PageId = 1          
        ELSE
            IF @FormId = 12
                SET @PageId = 3          
            ELSE
                IF @FormId = 13
                    SET @PageId = 11       
                ELSE
                    IF ( @FormId = 14
                         AND ( @numtype = 1
                               OR @numtype = 3
                             )
                       )
                        OR @FormId = 33
                        OR @FormId = 38
                        OR @FormId = 39
                        SET @PageId = 2          
                    ELSE
                        IF @FormId = 14
                            AND ( @numtype = 2
                                  OR @numtype = 4
                                )
                            OR @FormId = 40
                            OR @FormId = 41
                            SET @PageId = 6  
                        ELSE
                            IF @FormId = 21
                                OR @FormId = 26
                                OR @FormId = 32
								OR @FormId = 126
                                SET @PageId = 5   
                            ELSE
                                IF @FormId = 22
                                    OR @FormId = 30
                                    SET @PageId = 5    
                                ELSE
                                    IF @FormId = 76
                                        SET @PageId = 10 
                                    ELSE
                                        IF @FormId = 84
                                            OR @FormId = 85
                                            SET @PageId = 5

              
    IF @PageId = 1
        OR @PageId = 4
        BEGIN          
            SELECT  CONVERT(VARCHAR(9), numFieldID) + '~0' AS numFieldID ,
                    ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                    0 AS Custom
            FROM    View_DynamicDefaultColumns
            WHERE   numFormID = @FormId
                    AND ISNULL(bitSettingField, 0) = 1
                    AND ISNULL(bitDeleted, 0) = 0
                    AND numDomainID = @numDomainID
            UNION
            SELECT  CONVERT(VARCHAR(9), fld_id) + '~1' AS numFieldID ,
                    fld_label AS vcFieldName ,
                    1 AS Custom
            FROM    CFW_Fld_Master
                    LEFT JOIN CFW_Fld_Dtl ON Fld_id = numFieldId
                                             AND numRelation = CASE
                                                              WHEN @numtype IN (
                                                              1, 2, 3 )
                                                              THEN numRelation
                                                              ELSE @numtype
                                                              END
                    LEFT JOIN CFw_Grp_Master ON subgrp = CFw_Grp_Master.Grp_id
            WHERE   CFW_Fld_Master.grp_id = @PageId
                    AND CFW_Fld_Master.numDomainID = @numDomainID
                    AND Fld_type <> 'Link'
            ORDER BY Custom ,
                    vcFieldName                                       
        END  
    ELSE IF @PageId = 10
    BEGIN 
	         
                SELECT  CONVERT(VARCHAR(9), numFieldID) + '~0' AS numFieldID ,
                        ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                        0 AS Custom
                FROM    View_DynamicDefaultColumns
                WHERE   numFormID = @FormId
                        AND ISNULL(bitSettingField, 0) = 1
                        AND ISNULL(bitDeleted, 0) = 0
                        AND numDomainID = @numDomainID
                UNION
                SELECT  CONVERT(VARCHAR(9), fld_id) + '~1' AS numFieldID ,
                        fld_label AS vcFieldName ,
                        1 AS Custom
                FROM    CFW_Fld_Master
                        LEFT JOIN CFW_Fld_Dtl ON Fld_id = numFieldId
                                                 AND numRelation = CASE
                                                              WHEN @numtype IN (
                                                              1, 2, 3 )
                                                              THEN numRelation
                                                              ELSE @numtype
                                                              END
                        LEFT JOIN CFw_Grp_Master ON subgrp = CFw_Grp_Master.Grp_id
                WHERE   CFW_Fld_Master.grp_id = 5
                        AND CFW_Fld_Master.numDomainID = @numDomainID
                        AND Fld_Type = 'SelectBox'
                ORDER BY Custom ,
                        vcFieldName       
	                                     
            END 
	ELSE IF @FormId=43
			BEGIN
				PRINT 'ABC'
				SELECT  CONVERT(VARCHAR(9), numFieldID) + '~0' AS numFieldID ,
                            ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                            0 AS Custom ,
                            vcDbColumnName
                    FROM    View_DynamicDefaultColumns
                    WHERE   numFormID = @FormId
                            AND ISNULL(bitSettingField, 0) = 1
                            AND ISNULL(bitDeleted, 0) = 0
                            AND numDomainID = @numDomainID
                    UNION
                    SELECT  CONVERT(VARCHAR(9), fld_id) + '~1' AS numFieldID ,
                            fld_label AS vcFieldName ,
                            1 AS Custom ,
                            CONVERT(VARCHAR(10), fld_id) AS vcDbColumnName
                    FROM    CFW_Fld_Master
                            LEFT JOIN CFw_Grp_Master ON subgrp = CFw_Grp_Master.Grp_id
                    WHERE   CFW_Fld_Master.grp_id = 1
                            AND CFW_Fld_Master.numDomainID = @numDomainID
                            AND Fld_type <> 'Link'
                    ORDER BY Custom ,
                            vcFieldName          
			END
	ELSE IF @FormId = 125 AND @PageId=0
	BEGIN
			SELECT  CONVERT(VARCHAR(9), numFieldID) + '~0' AS numFieldID ,
                    ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                    0 AS Custom
            FROM    View_DynamicDefaultColumns
            WHERE   numFormID = @FormId
                    AND ISNULL(bitSettingField, 0) = 1
                    AND ISNULL(bitDeleted, 0) = 0
                    AND numDomainID = @numDomainID
	END

        ELSE
            IF @PageId = 5
                AND ( @FormId = 84
                      OR @FormId = 85
                    )
                BEGIN          
                    SELECT  CONVERT(VARCHAR(9), numFieldID) + '~0' AS numFieldID ,
                            ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                            0 AS Custom ,
                            vcDbColumnName
                    FROM    View_DynamicDefaultColumns
                    WHERE   numFormID = @FormId
                            AND ISNULL(bitSettingField, 0) = 1
                            AND ISNULL(bitDeleted, 0) = 0
                            AND numDomainID = @numDomainID
                    UNION
                    SELECT  CONVERT(VARCHAR(9), fld_id) + '~1' AS numFieldID ,
                            fld_label AS vcFieldName ,
                            1 AS Custom ,
                            CONVERT(VARCHAR(10), fld_id) AS vcDbColumnName
                    FROM    CFW_Fld_Master
                            LEFT JOIN CFw_Grp_Master ON subgrp = CFw_Grp_Master.Grp_id
                    WHERE   CFW_Fld_Master.grp_id = @PageId
                            AND CFW_Fld_Master.numDomainID = @numDomainID
                            AND Fld_type = 'TextBox'
                    ORDER BY Custom ,
                            vcFieldName             
                END  
             
            ELSE
                BEGIN          
      
                    SELECT  CONVERT(VARCHAR(9), numFieldID) + '~0' AS numFieldID ,
                            ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                            0 AS Custom ,
                            vcDbColumnName
                    FROM    View_DynamicDefaultColumns
                    WHERE   numFormID = @FormId
                            AND ISNULL(bitSettingField, 0) = 1
                            AND ISNULL(bitDeleted, 0) = 0
                            AND numDomainID = @numDomainID
                    UNION
                    SELECT  CONVERT(VARCHAR(9), fld_id) + '~1' AS numFieldID ,
                            fld_label AS vcFieldName ,
                            1 AS Custom ,
                            CONVERT(VARCHAR(10), fld_id) AS vcDbColumnName
                    FROM    CFW_Fld_Master
                            LEFT JOIN CFw_Grp_Master ON subgrp = CFw_Grp_Master.Grp_id
                    WHERE   1 = (CASE WHEN @FormId=21 THEN (CASE WHEN  CFW_Fld_Master.grp_id IN (5,9) THEN 1 ELSE 0 END) ELSE (CASE WHEN  CFW_Fld_Master.grp_id = @PageId THEN 1 ELSE 0 END) END)
                            AND CFW_Fld_Master.numDomainID = @numDomainID
                            AND Fld_type <> 'Link'
                    ORDER BY Custom ,
                            vcFieldName             
                END                      
                      
                   
    IF @PageId = 1
        OR @PageId = 4
        BEGIN
 
            IF ( @FormId = 96
                 OR @FormId = 97
               )
                SELECT  CONVERT(VARCHAR(9), numFieldID) + '~0' numFieldID ,
                        ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                        vcDbColumnName ,
                        0 AS Custom ,
                        tintRow AS tintOrder
                FROM    View_DynamicColumns
                WHERE   numFormId = @FormId
                        AND numDomainID = @numDomainID
                        AND ISNULL(bitCustom, 0) = 0
                        AND tintPageType = 1
                        AND ISNULL(bitSettingField, 0) = 1
                        AND numRelCntType = @numtype
                        AND ISNULL(bitDeleted, 0) = 0
                UNION
                SELECT  CONVERT(VARCHAR(9), numFieldID) + '~1' AS numFieldID ,
                        vcFieldName ,
                        vcFieldName AS vcDbColumnName ,
                        1 AS Custom ,
                        tintRow AS tintOrder
                FROM    View_DynamicCustomColumns
                WHERE   numFormID = @FormId
                        AND numDomainID = @numDomainID
                        AND grp_id = @PageId
                        AND numDomainID = @numDomainID
                        AND vcAssociatedControlType <> 'Link'
                        AND ISNULL(bitCustom, 0) = 1
                        AND tintPageType = 1
                        AND numRelCntType = @numtype
                ORDER BY tintOrder                
            ELSE
                SELECT  CONVERT(VARCHAR(9), numFieldID) + '~0' numFieldID ,
                        ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                        vcDbColumnName ,
                        0 AS Custom ,
                        tintRow AS tintOrder
                FROM    View_DynamicColumns
                WHERE   numFormId = @FormId
                        AND numUserCntID = @numUserCntID
                        AND numDomainID = @numDomainID
                        AND ISNULL(bitCustom, 0) = 0
                        AND tintPageType = 1
                        AND ISNULL(bitSettingField, 0) = 1
                        AND numRelCntType = @numtype
                        AND ISNULL(bitDeleted, 0) = 0
                UNION
                SELECT  CONVERT(VARCHAR(9), numFieldID) + '~1' AS numFieldID ,
                        vcFieldName ,
                        '' AS vcDbColumnName ,
                        1 AS Custom ,
                        tintRow AS tintOrder
                FROM    View_DynamicCustomColumns
                WHERE   numFormID = @FormId
                        AND numUserCntID = @numUserCntID
                        AND numDomainID = @numDomainID
                        AND grp_id = @PageId
                        AND numDomainID = @numDomainID
                        AND vcAssociatedControlType <> 'Link'
                        AND ISNULL(bitCustom, 0) = 1
                        AND tintPageType = 1
                        AND numRelCntType = @numtype
                ORDER BY tintOrder          
        END     
    ELSE
        IF @PageId = 10
            BEGIN
	
                SELECT  CONVERT(VARCHAR(9), numFieldID) + '~0' numFieldID ,
                        ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                        vcDbColumnName ,
                        0 AS Custom ,
                        tintRow AS tintOrder ,
                        numListID ,
                        numFieldID AS [numFieldID1]
                FROM    View_DynamicColumns
                WHERE   numFormId = @FormId
                        AND numUserCntID = @numUserCntID
                        AND numDomainID = @numDomainID
                        AND ISNULL(bitCustom, 0) = 0
                        AND tintPageType = 1
                        AND ISNULL(bitSettingField, 0) = 1
                        AND numRelCntType = @numtype
                        AND ISNULL(bitDeleted, 0) = 0
                UNION
                SELECT  CONVERT(VARCHAR(9), numFieldID) + '~1' AS numFieldID ,
                        vcFieldName ,
                        '' AS vcDbColumnName ,
                        1 AS Custom ,
                        tintRow AS tintOrder ,
                        numListID ,
                        numFieldID AS [numFieldID1]
                FROM    View_DynamicCustomColumns
                WHERE   numFormID = @FormId
                        AND numUserCntID = @numUserCntID
                        AND numDomainID = @numDomainID   
   --and grp_id=@PageId    
                        AND numDomainID = @numDomainID
                        AND vcAssociatedControlType = 'SelectBox'
                        AND ISNULL(bitCustom, 0) = 1
                        AND tintPageType = 1
                        AND numRelCntType = @numtype
                ORDER BY tintOrder    
--   
--    SELECT DISTINCT numFieldID as numFieldID,'Archived Items' AS vcFieldName,bitCustom as Custom 
--	   FROM DycFormConfigurationDetails
--	   WHERE 1=1
--	   AND numFormID = @FormId
--	   AND ISNULL(numFieldID,0)= -1
--	   AND numDomainID = @numDomainID	
            END	   
--ELSE IF @PageId = 12
--	BEGIN
--		SELECT  CONVERT(VARCHAR(9),numFieldID) + '~0' numFieldID,
--				ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName,
--				vcDbColumnName ,
--				0 AS Custom,
--				tintRow AS tintOrder,
--				vcAssociatedControlType,
--				numlistid,
--				vcLookBackTableName,
--				numFieldID AS FieldId,
--				bitAllowEdit                    
--		FROM View_DynamicColumns
--		WHERE numFormId = @FormId 
--		AND numUserCntID = @numUserCntID 
--		AND  numDomainID = @numDomainID  
--	    AND ISNULL(bitCustom,0) = 0 
--		AND tintPageType = 1 
--		AND ISNULL(bitSettingField,0) = 1 
--		AND ISNULL(numRelCntType,0) = @numtype 
--		AND ISNULL(bitDeleted,0) = 0
--	    AND  ISNULL(numViewID,0) = @numViewID   
--	   
--	    UNION
--	   
--	    SELECT  CONVERT(VARCHAR(9),numFieldID)+'~1' AS numFieldID ,
--				vcFieldName ,'' AS vcDbColumnName,
--				1 AS Custom,
--				tintRow AS tintOrder,
--				vcAssociatedControlType,
--				numlistid,'' AS vcLookBackTableName,
--				numFieldID AS FieldId,
--				0 AS bitAllowEdit           
--	    FROM View_DynamicCustomColumns          
--	    WHERE numFormID = @FormId 
--		AND numUserCntID = @numUserCntID 
--		--AND numDomainID=@numDomainID
--		AND grp_id = @PageId  
--		AND numDomainID = @numDomainID  
--		AND vcAssociatedControlType <> 'Link' 
--		AND ISNULL(bitCustom,0) = 1 
--		--AND tintPageType = 1  
--		---AND ISNULL(numRelCntType,0)= @numtype     
--		--AND  ISNULL(numViewID,0) = @numViewID
--	    ORDER BY tintOrder     
--
--	END 
        ELSE
            IF @PageId = 5
                AND ( @FormId = 84
                      OR @FormId = 85
                    )
                BEGIN

                    IF EXISTS ( SELECT  'col1'
                                FROM    DycFormConfigurationDetails
                                WHERE   numDomainID = @numDomainID
                                        AND numFormId = @FormID
                                        AND numFieldID IN ( 507, 508, 509, 510,
                                                            511, 512 ) )
                        BEGIN
                            SELECT  CONVERT(VARCHAR(9), numFieldID) + '~0' numFieldID ,
                                    ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                                    vcDbColumnName ,
                                    0 AS Custom ,
                                    tintRow AS tintOrder ,
                                    vcAssociatedControlType ,
                                    numlistid ,
                                    vcLookBackTableName ,
                                    numFieldID AS FieldId ,
                                    bitAllowEdit ,
                                    0 AS AZordering
                            FROM    View_DynamicColumns
                            WHERE   numFormId = @FormID
                                    AND numDomainID = @numDomainID
                                    AND ISNULL(bitCustom, 0) = 0   
		 --AND tintPageType = 1   
                                    AND ISNULL(bitSettingField, 0) = 1
                                    AND ISNULL(bitDeleted, 0) = 0
                            UNION
                            SELECT  CONVERT(VARCHAR(9), numFieldID) + '~1' AS numFieldID ,
                                    vcFieldName ,
                                    '' AS vcDbColumnName ,
                                    1 AS Custom ,
                                    tintRow AS tintOrder ,
                                    vcAssociatedControlType ,
                                    numlistid ,
                                    '' AS vcLookBackTableName ,
                                    numFieldID AS FieldId ,
                                    0 AS bitAllowEdit ,
                                    0 AS AZordering
                            FROM    View_DynamicCustomColumns
                            WHERE   numFormID = @FormID
                                    AND numDomainID = @numDomainID
                                    AND grp_id = @PageID
                                    AND vcAssociatedControlType = 'TextBox'
                                    AND ISNULL(bitCustom, 0) = 1
                            ORDER BY tintOrder 
                        END
                    ELSE
                        BEGIN
                            SELECT  CONVERT(VARCHAR(9), numFieldID) + '~0' numFieldID ,
                                    ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                                    vcDbColumnName ,
                                    0 AS Custom ,
                                    tintRow AS tintOrder ,
                                    vcAssociatedControlType ,
                                    numlistid ,
                                    vcLookBackTableName ,
                                    numFieldID AS FieldId ,
                                    bitAllowEdit ,
                                    0 AS AZordering
                            FROM    View_DynamicColumns
                            WHERE   numFormId = @FormID
                                    AND numDomainID = @numDomainID
                                    AND ISNULL(bitCustom, 0) = 0   
		 --AND tintPageType = 1   
                                    AND ISNULL(bitSettingField, 0) = 1
                                    AND ISNULL(bitDeleted, 0) = 0
                            UNION
                            SELECT  CONVERT(VARCHAR(9), numFieldID) + '~1' AS numFieldID ,
                                    vcFieldName ,
                                    '' AS vcDbColumnName ,
                                    1 AS Custom ,
                                    tintRow AS tintOrder ,
                                    vcAssociatedControlType ,
                                    numlistid ,
                                    '' AS vcLookBackTableName ,
                                    numFieldID AS FieldId ,
                                    0 AS bitAllowEdit ,
                                    0 AS AZordering
                            FROM    View_DynamicCustomColumns
                            WHERE   numFormID = @FormID
                                    AND numDomainID = @numDomainID
                                    AND grp_id = @PageID
                                    AND vcAssociatedControlType = 'TextBox'
                                    AND ISNULL(bitCustom, 0) = 1
                            UNION
                            SELECT  '507~0' AS numFieldID ,
                                    'Title:A-Z' AS vcFieldName ,
                                    'Title:A-Z' AS vcDbColumnName ,
                                    1 AS Custom ,
                                    0 AS tintOrder ,
                                    ' ' AS vcAssociatedControlType ,
                                    1 AS numlistid ,
                                    '' AS vcLookBackTableName ,
                                    507 AS FieldId ,
                                    0 AS bitAllowEdit ,
                                    1 AS IsFixed
                            UNION
                            SELECT  '508~0' AS numFieldID ,
                                    'Title:Z-A' AS vcFieldName ,
                                    'Title:Z-A' AS vcDbColumnName ,
                                    1 AS Custom ,
                                    0 AS tintOrder ,
                                    ' ' AS vcAssociatedControlType ,
                                    1 AS numlistid ,
                                    '' AS vcLookBackTableName ,
                                    508 AS FieldId ,
                                    0 AS bitAllowEdit ,
                                    1 AS IsFixed
                            UNION
                            SELECT  '509~0' AS numFieldID ,
                                    'Price:Low to High' AS vcFieldName ,
                                    'Price:Low to High' AS vcDbColumnName ,
                                    1 AS Custom ,
                                    0 AS tintOrder ,
                                    ' ' AS vcAssociatedControlType ,
                                    1 AS numlistid ,
                                    '' AS vcLookBackTableName ,
                                    509 AS FieldId ,
                                    0 AS bitAllowEdit ,
                                    1 AS IsFixed
                            UNION
                            SELECT  '510~0' AS numFieldID ,
                                    'Price:High to Low' AS vcFieldName ,
                                    'Price:High to Low' AS vcDbColumnName ,
                                    1 AS Custom ,
                                    0 AS tintOrder ,
                                    ' ' AS vcAssociatedControlType ,
                                    1 AS numlistid ,
                                    '' AS vcLookBackTableName ,
                                    510 AS FieldId ,
                                    0 AS bitAllowEdit ,
                                    1 AS IsFixed
                            UNION
                            SELECT  '511~0' AS numFieldID ,
                                    'New Arrival' AS vcFieldName ,
                                    'New Arrival' AS vcDbColumnName ,
                                    1 AS Custom ,
                                    0 AS tintOrder ,
                                    ' ' AS vcAssociatedControlType ,
                                    1 AS numlistid ,
                                    '' AS vcLookBackTableName ,
                                    511 AS FieldId ,
                                    0 AS bitAllowEdit ,
                                    1 AS IsFixed
                            UNION
                            SELECT  '512~0' AS numFieldID ,
                                    'Oldest' AS vcFieldName ,
                                    'Oldest' AS vcDbColumnName ,
                                    1 AS Custom ,
                                    0 AS tintOrder ,
                                    ' ' AS vcAssociatedControlType ,
                                    1 AS numlistid ,
                                    '' AS vcLookBackTableName ,
                                    512 AS FieldId ,
                                    0 AS bitAllowEdit ,
                                    1 AS IsFixed
                            ORDER BY tintOrder 

                        END



    

                END
			IF @FormId=43
			BEGIN
				
                    SELECT  CONVERT(VARCHAR(9), numFieldID) + '~0' numFieldID ,
                            ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                            vcDbColumnName ,
                            0 AS Custom ,
                            tintRow AS tintOrder ,
                            vcAssociatedControlType ,
                            numlistid ,
                            vcLookBackTableName ,
                            numFieldID AS FieldId ,
                            bitAllowEdit
                    FROM    View_DynamicColumns
                    WHERE   numFormId = @FormId
                            AND numUserCntID = @numUserCntID
                            AND numDomainID = @numDomainID
                            AND ISNULL(bitCustom, 0) = 0
                            AND tintPageType = 1
                            AND ISNULL(bitSettingField, 0) = 1
                            AND ISNULL(numRelCntType, 0) = @numtype
                            AND ISNULL(bitDeleted, 0) = 0
                            AND ISNULL(numViewID, 0) = @numViewID
                    UNION
                    SELECT  CONVERT(VARCHAR(9), numFieldID) + '~1' AS numFieldID ,
                            vcFieldName ,
                            '' AS vcDbColumnName ,
                            1 AS Custom ,
                            tintRow AS tintOrder ,
                            vcAssociatedControlType ,
                            numlistid ,
                            '' AS vcLookBackTableName ,
                            numFieldID AS FieldId ,
                            0 AS bitAllowEdit
                    FROM    View_DynamicCustomColumns
                    WHERE   numFormID = @FormId
                            AND numUserCntID = @numUserCntID
                            AND numDomainID = @numDomainID
                            AND grp_id = 1
                            AND numDomainID = @numDomainID
                            AND vcAssociatedControlType <> 'Link'
                            AND ISNULL(bitCustom, 0) = 1
                            AND tintPageType = 1
                            AND ISNULL(numRelCntType, 0) = @numtype
                            AND ISNULL(numViewID, 0) = @numViewID
                    ORDER BY tintOrder       
			END
            ELSE
                BEGIN  
					
                    SELECT  CONVERT(VARCHAR(9), numFieldID) + '~0' numFieldID ,
                            ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                            vcDbColumnName ,
                            0 AS Custom ,
                            tintRow AS tintOrder ,
                            vcAssociatedControlType ,
                            numlistid ,
                            vcLookBackTableName ,
                            numFieldID AS FieldId ,
                            bitAllowEdit
                    FROM    View_DynamicColumns
                    WHERE   numFormId = @FormId
                            AND numUserCntID = @numUserCntID
                            AND numDomainID = @numDomainID
                            AND ISNULL(bitCustom, 0) = 0
                            AND tintPageType = 1
                            AND ISNULL(bitSettingField, 0) = 1
                            AND ISNULL(numRelCntType, 0) = @numtype
                            AND ISNULL(bitDeleted, 0) = 0
                            AND ISNULL(numViewID, 0) = @numViewID
                    UNION
                    SELECT  CONVERT(VARCHAR(9), numFieldID) + '~1' AS numFieldID ,
                            vcFieldName ,
                            '' AS vcDbColumnName ,
                            1 AS Custom ,
                            tintRow AS tintOrder ,
                            vcAssociatedControlType ,
                            numlistid ,
                            '' AS vcLookBackTableName ,
                            numFieldID AS FieldId ,
                            0 AS bitAllowEdit
                    FROM    View_DynamicCustomColumns
                    WHERE   numFormID = @FormId
                            AND numUserCntID = @numUserCntID
                            AND numDomainID = @numDomainID
                            AND 1 = (CASE WHEN @FormId=21 THEN (CASE WHEN  grp_id IN (5,9) THEN 1 ELSE 0 END) ELSE (CASE WHEN  grp_id = @PageId THEN 1 ELSE 0 END) END)
                            AND numDomainID = @numDomainID
                            AND vcAssociatedControlType <> 'Link'
                            AND ISNULL(bitCustom, 0) = 1
                            AND tintPageType = 1
                            AND ISNULL(numRelCntType, 0) = @numtype
                            AND ISNULL(numViewID, 0) = @numViewID
                    ORDER BY tintOrder          
                END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_GetCommissionRuleContacts')
DROP PROCEDURE usp_GetCommissionRuleContacts
GO
CREATE PROCEDURE [dbo].[usp_GetCommissionRuleContacts]
@numComRuleID AS NUMERIC
AS
BEGIN
	DECLARE @tintAssignTo AS TINYINT

	SELECT @tintAssignTo=tintAssignTo FROM CommissionRules WHERE numComRuleID=@numComRuleID

	IF @tintAssignTo = 3
	BEGIN
		SELECT 
			CONCAT(CC.numValue,'~',CC.bitCommContact,'~1') AS numContactID,
			CONCAT(D.vcPartnerCode,'-',C.vcCompanyName) as vcUserName
		FROM 
			CommissionRuleContacts CC 
		INNER JOIN
			DivisionMaster D
		ON
			CC.numValue = D.numDivisionID
		INNER JOIN
			CompanyInfo C
		ON
			D.numCompanyID =C.numCompanyId
		WHERE 
			CC.numComRuleID=@numComRuleID 
	END
	ELSE
	BEGIN
		SELECT 
			CONCAT(CC.numValue,'~',CC.bitCommContact,'~0') AS numContactID,
			CONCAT(dbo.fn_GetContactName(numValue),CASE CC.bitCommContact WHEN 1 THEN '(' + ISNULL((SELECT CompanyInfo.vcCompanyName FROM AdditionalContactsInformation ACI INNER JOIN DivisionMaster DM ON ACI.numDivisionId=DM.numDivisionID INNER JOIN CompanyInfo ON DM.numCompanyID = CompanyInfo.numCompanyId WHERE ACI.numContactId=CC.numValue),'') + ')' ELSE '' END) as vcUserName
		FROM 
			CommissionRuleContacts CC 
		WHERE 
			CC.numComRuleID=@numComRuleID 
	END
END	
GO
/****** Object:  StoredProcedure [dbo].[USP_GetDealsList1]    Script Date: 05/07/2009 22:12:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getdealslist1')
DROP PROCEDURE usp_getdealslist1
GO
CREATE PROCEDURE [dbo].[USP_GetDealsList1]
               @numUserCntID              NUMERIC(9)  = 0,
               @numDomainID               NUMERIC(9)  = 0,
               @tintSalesUserRightType    TINYINT  = 0,
               @tintPurchaseUserRightType TINYINT  = 0,
               @tintSortOrder             TINYINT  = 4,
               @CurrentPage               INT,
               @PageSize                  INT,
               @TotRecs                   INT  OUTPUT,
               @columnName                AS VARCHAR(50),
               @columnSortOrder           AS VARCHAR(10),
               @numCompanyID              AS NUMERIC(9)  = 0,
               @bitPartner                AS BIT  = 0,
               @ClientTimeZoneOffset      AS INT,
               @tintShipped               AS TINYINT,
               @intOrder                  AS TINYINT,
               @intType                   AS TINYINT,
               @tintFilterBy              AS TINYINT  = 0,
               @numOrderStatus			  AS NUMERIC,
			   @vcRegularSearchCriteria varchar(MAX)='',
			   @vcCustomSearchCriteria varchar(MAX)='',
			   @SearchText VARCHAR(300) = '',
			   @SortChar char(1)='0'  
AS
	DECLARE  @PageId  AS TINYINT
	DECLARE  @numFormId  AS INT 
  
	SET @PageId = 0

	IF @inttype = 1
	BEGIN
		SET @PageId = 2
		SET @inttype = 3
		SET @numFormId=39
	END
	ELSE IF @inttype = 2
	BEGIN
		SET @PageId = 6
		SET @inttype = 4
		SET @numFormId=41
	END

	CREATE TABLE #tempForm 
	(
		tintOrder TINYINT,vcDbColumnName nvarchar(50),vcOrigDbColumnName nvarchar(50),vcFieldName nvarchar(50),vcAssociatedControlType nvarchar(50),vcListItemType varchar(3),
		numListID numeric(9),vcLookBackTableName varchar(50),bitCustomField BIT,numFieldId NUMERIC,bitAllowSorting BIT,bitAllowEdit BIT,bitIsRequired BIT,bitIsEmail BIT,
		bitIsAlphaNumeric BIT,bitIsNumeric BIT,bitIsLengthValidation BIT,intMaxLength int,intMinLength int,bitFieldMessage BIT,vcFieldMessage VARCHAR(500),
		ListRelID numeric(9),intColumnWidth int,bitAllowFiltering BIT,vcFieldDataType CHAR(1)
	)

	-- FIRST CHECK IF GRID COLUMNS ARE CONFIGURED
	DECLARE  @Nocolumns  AS TINYINT = 0

	SELECT 
		@Nocolumns=ISNULL(SUM(TotalRow),0) 
	FROM
	(            
		SELECT 
			COUNT(*) TotalRow 
		FROM 
			View_DynamicColumns 
		WHERE 
			numFormId=@numFormId 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND numRelCntType = @numFormId
		UNION 
		SELECT 
			COUNT(*) TotalRow 
		FROM 
			View_DynamicCustomColumns 
		WHERE 
			numFormId=@numFormId 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND numRelCntType = @numFormId
	) TotalRows

	-- IF GRID COLUMNS ARE NOT CONFIGURES THEN CHECK IF MASTER FORM CONFIGURATION IS CREATED BY ADMINISTRATOR
	DECLARE @IsMasterConfAvailable BIT = 0
	DECLARE @numUserGroup NUMERIC(18,0)

	IF @Nocolumns=0
	BEGIN
		SELECT @numUserGroup = numGroupID FROM UserMaster WHERE numUserDetailId = @numUserCntID 

		IF (SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = @numDomainID AND numFormID = @numFormId AND numRelCntType=@numFormId AND bitGridConfiguration = 1 AND numGroupID = @numUserGroup) > 0
		BEGIN
			SET @IsMasterConfAvailable = 1
		END
	END

	--IF MASTERCONFIGURATION IS AVAILABLE THEN LOAD IT OTHERWISE LOAD DEFAULT COLUMNS
	IF @IsMasterConfAvailable = 1
	BEGIN
		INSERT INTO 
			#tempForm
		SELECT 
			(intRowNum + 1) as tintOrder, vcDbColumnName,	vcOrigDbColumnName,	ISNULL(vcCultureFieldName,vcFieldName), vcAssociatedControlType,
			vcListItemType, numListID, vcLookBackTableName,	bitCustom, numFieldId, bitAllowSorting,	bitInlineEdit, bitIsRequired,
			bitIsEmail,	bitIsAlphaNumeric, bitIsNumeric, bitIsLengthValidation,	intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,
			ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType 
		FROM 
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId=@numFormId AND 
			View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicColumnsMasterConfig.numRelCntType=@numFormId AND 
			View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
			ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
		UNION
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName, vcAssociatedControlType,'' as vcListItemType,numListID,'',
			bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,''
		FROM 
			View_DynamicCustomColumnsMasterConfig
		WHERE 
			View_DynamicCustomColumnsMasterConfig.numFormId=@numFormId AND 
			View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicCustomColumnsMasterConfig.numRelCntType=@numFormId AND 
			View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1
		ORDER BY 
			tintOrder ASC  
	END
	ELSE                                        
	BEGIN
		IF @Nocolumns=0
		BEGIN
			INSERT INTO DycFormConfigurationDetails 
			(
				numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth
			)
			SELECT 
				@numFormId,numFieldId,0,Row_number() over(order by tintRow desc),@numDomainID,@numUserCntID,@numFormId,1,0,intColumnWidth
			FROM 
				View_DynamicDefaultColumns
			WHERE 
				numFormId=@numFormId 
				AND bitDefault=1 
				AND ISNULL(bitSettingField,0)=1 
				AND numDomainID=@numDomainID
			ORDER BY 
				tintOrder asc   
		END

		INSERT INTO 
			#tempForm
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcOrigDbColumnName,ISNULL(vcCultureFieldName,vcFieldName),                  
			vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName,bitCustom,numFieldId,bitAllowSorting,bitInlineEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength,bitFieldMessage,
			vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType  
		FROM 
			View_DynamicColumns 
		WHERE 
			numFormId=@numFormId 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND ISNULL(bitSettingField,0)=1 
			AND ISNULL(bitCustom,0)=0  
			AND numRelCntType = @numFormId
		UNION
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName,vcAssociatedControlType,'' as vcListItemType,numListID,'',bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,'' 
		FROM 
			View_DynamicCustomColumns
		WHERE 
			numFormId=@numFormId 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND ISNULL(bitCustom,0)=1 
			AND numRelCntType = @numFormId
		ORDER BY 
			tintOrder asc  
	END

	DECLARE  @strColumns AS VARCHAR(MAX)
	SET @strColumns = ' ADC.numContactId, Div.numDivisionID,ISNULL(Div.numTerID,0) numTerID, opp.numRecOwner as numRecOwner, Div.tintCRMType, opp.numOppId, opp.monDealAmount, ISNULL(opp.intUsedShippingCompany,0) AS intUsedShippingCompany'


	DECLARE  @tintOrder  AS TINYINT;SET @tintOrder = 0
	DECLARE  @vcFieldName  AS VARCHAR(50)
	DECLARE  @vcListItemType  AS VARCHAR(3)
	DECLARE  @vcListItemType1  AS VARCHAR(1)
	DECLARE  @vcAssociatedControlType VARCHAR(30)
	DECLARE  @numListID  AS NUMERIC(9)
	DECLARE  @vcDbColumnName VARCHAR(40)
	DECLARE  @WhereCondition VARCHAR(2000);SET @WhereCondition = ''
	DECLARE  @vcLookBackTableName VARCHAR(2000)
	DECLARE  @bitCustom  AS BIT
	Declare @numFieldId as numeric  
	DECLARE @bitAllowSorting AS CHAR(1)   
	DECLARE @bitAllowEdit AS CHAR(1)                 
	DECLARE @vcColumnName AS VARCHAR(500)          
	
	Declare @ListRelID as numeric(9) 
	DECLARE @SearchQuery AS VARCHAR(MAX) = ''        

	SELECT TOP 1 
		@tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName,@vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,
		@numListID=numListID,@vcLookBackTableName=vcLookBackTableName ,@bitCustom=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,@bitAllowEdit=bitAllowEdit,
		@ListRelID=ListRelID
	FROM 
		#tempForm 
	ORDER BY 
		tintOrder ASC            
--	SET @strColumns = @strColumns + ' , (SELECT SUBSTRING((SELECT '', '' + cast(I.vcItemName as varchar)
--                      FROM OpportunityItems AS t
--					  LEFT JOIN OpportunityBizDocs as BC
--					  ON t.numOppId=BC.numOppId
--LEFT JOIN Item as I
--ON t.numItemCode=I.numItemCode
--WHERE t.numOppId=Opp.numOppId AND t.numoppItemtCode NOT IN (SELECT numOppItemId FROM OpportunityBizDocItems WHERE numOppBizDocID=BC.numOppBizDocsId)
--                     FOR XML PATH('''')), 2, 200000)  )[List_Item_Approval_UNIT] '
--SET @strColumns = @strColumns + ' , (SELECT 
-- SUBSTRING((SELECT '', '' + cast(I.vcItemName as varchar)
--FROM 
-- OpportunityItems AS t
--LEFT JOIN 
-- Item as I
--ON 
-- t.numItemCode=I.numItemCode
--WHERE 
-- t.numOppId=Opp.numOppId AND t.numoppItemtCode NOT IN (SELECT numOppItemId FROM OpportunityBizDocs JOIN OpportunityBizDocItems ON OpportunityBizDocs.numOppBizDocsId=OpportunityBizDocItems.numOppBizDocID  WHERE OpportunityBizDocs.numOppId=Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1)
--                     FOR XML PATH('''')), 2, 200000)  )[List_Item_Approval_UNIT] '
	WHILE @tintOrder > 0
    BEGIN
		IF @bitCustom = 0
		BEGIN
			DECLARE  @Prefix  AS VARCHAR(5)

			IF @vcLookBackTableName = 'AdditionalContactsInformation'
				SET @Prefix = 'ADC.'
			IF @vcLookBackTableName = 'DivisionMaster'
				SET @Prefix = 'Div.'
			IF @vcLookBackTableName = 'OpportunityMaster'
				SET @PreFix = 'Opp.'
			IF @vcLookBackTableName = 'OpportunityRecurring'
				SET @PreFix = 'OPR.'
			IF @vcLookBackTableName = 'CompanyInfo'
				SET @PreFix = 'cmp.'
			IF @vcLookBackTableName = 'ProjectProgress'
				SET @PreFix = 'PP'
			SET @vcColumnName= @vcDbColumnName+'~'+ convert(varchar(10),@numFieldId) +'~0'

			IF @vcAssociatedControlType = 'SelectBox'
			BEGIN
				IF @vcDbColumnName = 'numPartner'
				BEGIN
					SET @strColumns=@strColumns+ ' ,(Opp.numPartner) '+' ['+ @vcColumnName+']'    
					SET @strColumns=@strColumns+ ' ,(SELECT D.vcPartnerCode+''-''+C.vcCompanyName FROM DivisionMaster AS D LEFT JOIN CompanyInfo AS C ON D.numCompanyID=C.numCompanyID WHERE D.numDivisionID=Opp.numPartner) AS vcPartner' 
				END
				IF @vcDbColumnName = 'numPartenerContact'
				BEGIN
					SET @strColumns=@strColumns+ ' ,(Opp.numPartenerContact) '+' ['+ @vcColumnName+']'    
					SET @strColumns=@strColumns+ ' ,(SELECT A.vcFirstName+'' ''+ A.vcLastName FROM AdditionalContactsInformation AS A WHERE A.numContactId=Opp.numPartenerContact) AS vcPartenerContact' 
				END
				IF @vcDbColumnName = 'numStatus'
				BEGIN
					SET @strColumns=@strColumns+ ' ,(SELECT COUNT(OpportunityItems.numoppitemtCode) FROM OpportunityItems 
WHERE OpportunityItems.bitItemPriceApprovalRequired=1 AND OpportunityItems.numOppId=Opp.numOppId) ApprovalMarginCount'
				END
				IF @vcDbColumnName = 'tintSource'
				BEGIN
					SET @strColumns = @strColumns + ',ISNULL(dbo.fn_GetOpportunitySourceValue(ISNULL(Opp.tintSource,0),ISNULL(Opp.tintSourceType,0),Opp.numDomainID),''Internal Order'')' + ' [' + @vcColumnName + ']'

					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'ISNULL(dbo.fn_GetOpportunitySourceValue(ISNULL(Opp.tintSource,0),ISNULL(Opp.tintSourceType,0),Opp.numDomainID),''Internal Order'') LIKE ''%' + @SearchText + '%'''
					END
				END
				ELSE IF @vcDbColumnName = 'numCampainID'
				BEGIN
					SET @strColumns = @strColumns + ', (SELECT vcCampaignName FROM CampaignMaster WHERE numCampaignID= Opp.numCampainID) ' + ' [' + @vcColumnName + ']'
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'ISNULL((SELECT vcCampaignName FROM CampaignMaster WHERE numCampaignID= Opp.numCampainID),'''') LIKE ''%' + @SearchText + '%'''
					END
				END
				ELSE IF @vcListItemType = 'LI'
				BEGIN
					SET @strColumns = @strColumns + ',L' + CONVERT(VARCHAR(3),@tintOrder) + '.vcData' + ' [' + @vcColumnName + ']'
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'L' + CONVERT(VARCHAR(3),@tintOrder) + '.vcData LIKE ''%' + @SearchText + '%'''
					END

					SET @WhereCondition = @WhereCondition + ' left Join ListDetails L' + CONVERT(VARCHAR(3),@tintOrder) + ' on L' + CONVERT(VARCHAR(3),@tintOrder) + '.numListItemID=' + @vcDbColumnName
				END
				ELSE IF @vcListItemType = 'S'
				BEGIN
					SET @strColumns = @strColumns + ',S' + CONVERT(VARCHAR(3),@tintOrder) + '.vcState' + ' [' + @vcColumnName + ']'
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'S' + CONVERT(VARCHAR(3),@tintOrder) + '.vcState LIKE ''%' + @SearchText + '%'''
					END

					SET @WhereCondition = @WhereCondition + ' left join State S' + CONVERT(VARCHAR(3),@tintOrder) + ' on S' + CONVERT(VARCHAR(3),@tintOrder) + '.numStateID=' + @vcDbColumnName
				END
				ELSE IF @vcListItemType = 'BP'
				BEGIN
					SET @strColumns = @strColumns + ',SPLM.Slp_Name' + ' [' + @vcColumnName + ']'
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'SPLM.Slp_Name LIKE ''%' + @SearchText + '%'''
					END

					SET @WhereCondition = @WhereCondition + ' LEFT JOIN Sales_process_List_Master SPLM on SPLM.Slp_Id = OPP.numBusinessProcessID '
				END
				ELSE IF @vcListItemType = 'PP'
				BEGIN
					SET @strColumns = @strColumns + ',ISNULL(PP.intTotalProgress,0)' + ' [' + @vcColumnName + ']'

					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'ISNULL(PP.intTotalProgress,0) LIKE ''%' + @SearchText + '%'''
					END
				END
				ELSE IF @vcListItemType = 'T'
				BEGIN
					SET @strColumns = @strColumns + ',L' + CONVERT(VARCHAR(3),@tintOrder) + '.vcData' + ' [' + @vcColumnName + ']'
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'L' + CONVERT(VARCHAR(3),@tintOrder) + '.vcData LIKE ''%' + @SearchText + '%'''
					END

					SET @WhereCondition = @WhereCondition + ' left Join ListDetails L' + CONVERT(VARCHAR(3),@tintOrder) + ' on L' + CONVERT(VARCHAR(3),@tintOrder) + '.numListItemID=' + @vcDbColumnName
				END
				ELSE IF @vcListItemType = 'U'
				BEGIN
					SET @strColumns = @strColumns + ',dbo.fn_GetContactName(' + @Prefix + @vcDbColumnName + ') [' + @vcColumnName + ']'
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'dbo.fn_GetContactName(' + @Prefix + @vcDbColumnName + ') LIKE ''%' + @SearchText + '%'''
					END
				END
				ELSE IF @vcListItemType = 'WI'
				BEGIN
					SET @strColumns = @strColumns + ', Case when isnull(OL.numSiteID,0)>0 then Sites.vcSiteName else WebAPI.vcProviderName end [' + @vcColumnName + ']'
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + '(Case when isnull(OL.numSiteID,0)>0 then Sites.vcSiteName else WebAPI.vcProviderName end) LIKE ''%' + @SearchText + '%'''
					END

					set @WhereCondition= @WhereCondition +' left JOIN OpportunityLinking OL on OL.numChildOppID=Opp.numOppId left join WebAPI on WebAPI.WebApiId=isnull(OL.numWebApiId,0)
													left join Sites on isnull(OL.numSiteID,0)=Sites.numSIteID and Sites.numDomainID=' + convert(varchar(15),@numDomainID)                                                       
				END 
            END
			ELSE IF @vcAssociatedControlType = 'DateField'
			BEGIN
				IF @Prefix ='OPR.'
				BEGIN
					SET @strColumns = @strColumns + ', (SELECT TOP 1 dbo.FormatedDateFromDate( DateAdd(minute,+'+convert(varchar(15),-@ClientTimeZoneOffset)+', dtRecurringDate)'+ ','+ CONVERT(VARCHAR(10),@numDomainId) +') FROM  dbo.OpportunityRecurring WHERE numOppID=OPP.numOppID AND ISNULL(numOppBizDocID,0)=0 and dtRecurringDate > dbo.GetUTCDateWithoutTime()) AS  ['+ @vcColumnName + ']'

					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + '(SELECT TOP 1 dbo.FormatedDateFromDate( DateAdd(minute,+'+convert(varchar(15),-@ClientTimeZoneOffset)+', dtRecurringDate)'+ ','+ CONVERT(VARCHAR(10),@numDomainId) +') FROM  dbo.OpportunityRecurring WHERE numOppID=OPP.numOppID AND ISNULL(numOppBizDocID,0)=0 and dtRecurringDate > dbo.GetUTCDateWithoutTime()) LIKE ''%' + @SearchText + '%'''
					END
				END
				ELSE IF @vcDbColumnName = 'dtReleaseDate'
				BEGIN
					SET @strColumns = @strColumns
                                + ',case when convert(varchar(11),'
                                + @Prefix
                                + @vcDbColumnName
                                + ')= convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',getdate())) then ''<b><font color=red>Today</font></b>'''
					SET @strColumns = @strColumns
									+ 'when convert(varchar(11),'
									+ @Prefix
									+ @vcDbColumnName
									+ ')= convert(varchar(11),dateadd(day,-1,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',getdate()))) then ''<b><font color=purple>Yesterday</font></b>'''
					SET @strColumns = @strColumns
									+ 'when convert(varchar(11),'
									+ @Prefix
									+ @vcDbColumnName
									+ ')= convert(varchar(11),dateadd(day,1,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',getdate()))) then''<b><font color=orange>Tommorow</font></b>'' '
					SET @strColumns = @strColumns
									+ 'else dbo.FormatedDateFromDate('
									+ @Prefix
									+ @vcDbColumnName
									+ ','
									+ CONVERT(VARCHAR(10),@numDomainId)
									+ ') end  ['
									+ @vcColumnName + ']' 	

					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + @Prefix + @vcDbColumnName + ' LIKE ''%' + @SearchText + '%'''
					END

				END
				ELSE
				BEGIN
					SET @strColumns = @strColumns
                                + ',case when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+','
                                + @Prefix
                                + @vcDbColumnName
                                + ') )= convert(varchar(11),getdate()) then ''<b><font color=red>Today</font></b>'''
					SET @strColumns = @strColumns
									+ 'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+','
									+ @Prefix
									+ @vcDbColumnName
									+ ') )= convert(varchar(11),dateadd(day,-1,getdate())) then ''<b><font color=purple>Yesterday</font></b>'''
					SET @strColumns = @strColumns
									+ 'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+','
									+ @Prefix
									+ @vcDbColumnName
									+ ') )= convert(varchar(11),dateadd(day,1,getdate())) then''<b><font color=orange>Tommorow</font></b>'' '
					SET @strColumns = @strColumns
									+ 'else dbo.FormatedDateFromDate('
									+ @Prefix
									+ @vcDbColumnName
									+ ','
									+ CONVERT(VARCHAR(10),@numDomainId)
									+ ') end  ['
									+ @vcColumnName + ']' 	

					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + @Prefix + @vcDbColumnName + ' LIKE ''%' + @SearchText + '%'''
					END
				 END	
            END
            ELSE
            IF @vcAssociatedControlType = 'TextBox'
            BEGIN
				
                SET @strColumns = @strColumns
                                  + ','
                                  + CASE 
                                      WHEN @vcDbColumnName = 'numAge' THEN 'year(getutcdate())-year(bintDOB)'
                                      WHEN @vcDbColumnName = 'monPAmount' THEN 'isnull((SELECT varCurrSymbol FROM [Currency] WHERE numCurrencyID = Opp.[numCurrencyID]),'''') + '' ''+ CONVERT(VARCHAR,CONVERT(MONEY,monPAmount),1)'
                                      WHEN @vcDbColumnName = 'numDivisionID' THEN 'Div.numDivisionID'
                                      WHEN @vcDbColumnName = 'tintOppStatus' THEN 'CASE tintOppStatus WHEN 0 then ''Open'' WHEN 1 THEN ''Closed-Won'' WHEN 2 THEN ''Closed-Lost'' END '
                                      WHEN @vcDbColumnName = 'tintOppType' THEN 'CASE tintOppType WHEN 1 then ''Sales'' WHEN 2 THEN ''Purchase'' Else ''NA'' END '
                                      WHEN @vcDbColumnName = 'vcPOppName' THEN 'Opp.vcPOppName'
									  WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'vcBizDocsList' THEN 'ISNULL((SELECT SUBSTRING((SELECT ''$^$'' + CAST(numOppBizDocsId AS VARCHAR(18)) +''#^#''+ vcBizDocID
										FROM OpportunityBizDocs WHERE numOppId=Opp.numOppId FOR XML PATH('''')),4,200000)),'''')'
                                      ELSE @Prefix + @vcDbColumnName
                                    END
                                  + ' ['
                                  + @vcColumnName + ']'
				IF(@vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'vcBizDocsList')
				BEGIN
					SET @strColumns=@strColumns+','+' (SELECT COUNT(I.vcItemName) FROM 
													 OpportunityItems AS t LEFT JOIN  Item as I ON 
													 t.numItemCode=I.numItemCode WHERE 
													 t.numOppId=Opp.numOppId AND t.numoppItemtCode NOT IN 
													 (SELECT numOppItemId FROM OpportunityBizDocs JOIN 
													 OpportunityBizDocItems ON OpportunityBizDocs.numOppBizDocsId=OpportunityBizDocItems.numOppBizDocID  
													 WHERE OpportunityBizDocs.numOppId=Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1)) AS [List_Item_Approval_UNIT] '
				END
				
				IF LEN(@SearchText) > 0
				BEGIN
					SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + (CASE 
                                      WHEN @vcDbColumnName = 'numAge' THEN 'year(getutcdate())-year(bintDOB)'
                                      WHEN @vcDbColumnName = 'monPAmount' THEN 'isnull((SELECT varCurrSymbol FROM [Currency] WHERE numCurrencyID = Opp.[numCurrencyID]),'''') + '' ''+ CONVERT(VARCHAR,CONVERT(MONEY,monPAmount),1)'
                                      WHEN @vcDbColumnName = 'numDivisionID' THEN 'Div.numDivisionID'
                                      WHEN @vcDbColumnName = 'tintOppStatus' THEN 'CASE tintOppStatus WHEN 0 then ''Open'' WHEN 1 THEN ''Closed-Won'' WHEN 2 THEN ''Closed-Lost'' END '
                                      WHEN @vcDbColumnName = 'tintOppType' THEN 'CASE tintOppType WHEN 1 then ''Sales'' WHEN 2 THEN ''Purchase'' Else ''NA'' END '
                                      WHEN @vcDbColumnName = 'vcPOppName' THEN 'Opp.vcPOppName'
									  WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'vcBizDocsList' THEN 'ISNULL((SELECT SUBSTRING((SELECT ''$^$'' + CAST(numOppBizDocsId AS VARCHAR(18)) +''#^#''+ vcBizDocID
										FROM OpportunityBizDocs WHERE numOppId=Opp.numOppId FOR XML PATH('''')),4,200000)),'''')'
                                      ELSE @Prefix + @vcDbColumnName
                                    END) + ' LIKE ''%' + @SearchText + '%'''
				END
            END
            ELSE
            IF @vcAssociatedControlType = 'TextArea'
            BEGIN
				SET @strColumns=@strColumns+','+  @Prefix + @vcDbColumnName +' ['+ @vcColumnName+']' 

				IF LEN(@SearchText) > 0
				BEGIN
					SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + @Prefix + @vcDbColumnName + ' LIKE ''%' + @SearchText + '%'''
				END
            END
            ELSE IF  @vcAssociatedControlType='Label'                                              
			BEGIN
				SET @strColumns=@strColumns + ',' + CASE                    
				WHEN @vcDbColumnName = 'CalAmount' THEN 'Opp.monDealAmount'--'[dbo].[getdealamount](Opp.numOppId,Getutcdate(),0)'
				WHEN @vcDbColumnName = 'vcInventoryStatus' THEN 'ISNULL(dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID),'''')'
				WHEN @vcDbColumnName = 'vcOrderedShipped' THEN 'dbo.GetOrderShipReceiveStatus(Opp.numOppId,1,Opp.tintshipped)'
				WHEN @vcDbColumnName = 'vcOrderedReceived' THEN 'dbo.GetOrderShipReceiveStatus(Opp.numOppId,2,Opp.tintshipped)'
				WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'monDealAmount' THEN 'ISNULL((SELECT SUM(ISNULL(monDealAmount,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)'
				WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'monAmountPaid' THEN 'ISNULL((SELECT SUM(ISNULL(monAmountPaid,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)'
				else @Prefix + @vcDbColumnName END +' ['+ @vcColumnName+']'        
				
				IF @vcDbColumnName='vcOrderedShipped'
				BEGIN
				DECLARE @temp VARCHAR(MAX)
				SET @temp = '(SELECT 
				STUFF((SELECT '', '' + CAST((CAST(numOppBizDocsId AS varchar)+''~''+CAST(ISNULL(numShippingReportId,0) AS varchar) ) AS VARCHAR(MAX)) [text()]
				FROM OpportunityBizDocs
				LEFT JOIN ShippingReport
				ON ShippingReport.numOppBizDocId=OpportunityBizDocs.numOppBizDocsId
				WHERE OpportunityBizDocs.numOppId=Opp.numOppId  AND OpportunityBizDocs.bitShippingGenerated=1  FOR XML PATH(''''), TYPE)
						.value(''.'',''NVARCHAR(MAX)''),1,2,'' ''))'
					SET @strColumns = @strColumns + ' , (SELECT SUBSTRING('+ @temp +', 2, 200000))AS ShippingIcons '
					SET @strColumns = @strColumns + ' , CASE WHEN (SELECT COUNT(*) FROM ShippingBox INNER JOIN ShippingReport ON ShippingBox.numShippingReportId = ShippingReport.numShippingReportId WHERE ShippingReport.numOppID = Opp.numOppID AND LEN(vcTrackingNumber) > 0) > 0 THEN 1 ELSE 0 END [ISTrackingNumGenerated]'
				END

				IF LEN(@SearchText) > 0
				BEGIN
					SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + (CASE                    
					WHEN @vcDbColumnName = 'CalAmount' THEN 'Opp.monDealAmount'
					WHEN @vcDbColumnName = 'vcInventoryStatus' THEN 'ISNULL(dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID),'''')'
					WHEN @vcDbColumnName = 'vcOrderedShipped' THEN 'dbo.GetOrderShipReceiveStatus(Opp.numOppId,1,Opp.tintshipped)'
					WHEN @vcDbColumnName = 'vcOrderedReceived' THEN 'dbo.GetOrderShipReceiveStatus(Opp.numOppId,2,Opp.tintshipped)'
					WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'monDealAmount' THEN 'ISNULL((SELECT SUM(ISNULL(monDealAmount,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)'
					WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'monAmountPaid' THEN 'ISNULL((SELECT SUM(ISNULL(monAmountPaid,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)'
					else @Prefix + @vcDbColumnName END) + ' LIKE ''%' + @SearchText + '%'''
				END    
			END
            ELSE IF @vcAssociatedControlType='Popup' and @vcDbColumnName='numShareWith'                                                   
			BEGIN
				SET @strColumns = @strColumns + ',(SELECT SUBSTRING(
								(SELECT '','' + A.vcFirstName+'' ''+A.vcLastName + CASE WHEN ISNULL(SR.numContactType,0)>0 THEN ''('' + dbo.fn_GetListItemName(SR.numContactType) + '')'' ELSE '''' END
								FROM ShareRecord SR JOIN UserMaster UM ON UM.numUserDetailId=SR.numAssignedTo 
								JOIN AdditionalContactsInformation A ON UM.numUserDetailId=A.numContactID 
								WHERE SR.numDomainID=Opp.numDomainID AND SR.numModuleID=3 AND SR.numRecordID=Opp.numOppId
								AND UM.numDomainID=Opp.numDomainID and UM.numDomainID=A.numDomainID FOR XML PATH('''')),2,200000)) ['+ @vcColumnName+']'                
			END 
        END
		ELSE
        BEGIN
			SET @vcColumnName= @vcDbColumnName +'~'+ convert(varchar(10),@numFieldId)+'~1'

			IF @vcAssociatedControlType = 'TextBox' or @vcAssociatedControlType = 'TextArea'
            BEGIN
              SET @strColumns = @strColumns
                              + ',CFW'
                              + CONVERT(VARCHAR(3),@tintOrder)
                              + '.Fld_Value  ['
                              + @vcColumnName + ']'
              SET @WhereCondition = @WhereCondition
                                      + ' left Join CFW_FLD_Values_Opp CFW'
                                      + CONVERT(VARCHAR(3),@tintOrder)
                                      + ' on CFW'
                                      + CONVERT(VARCHAR(3),@tintOrder)
                                      + '.Fld_Id='
                                      + CONVERT(VARCHAR(10),@numFieldId)
                                      + 'and CFW'
                                      + CONVERT(VARCHAR(3),@tintOrder)
                                      + '.RecId=Opp.numOppid   '
			END
			ELSE
            IF @vcAssociatedControlType = 'CheckBox' 
            BEGIN
                SET @strColumns = @strColumns
                                + ',case when isnull(CFW'
                                + CONVERT(VARCHAR(3),@tintOrder)
                                + '.Fld_Value,0)=0 then 0 when isnull(CFW'
                                + CONVERT(VARCHAR(3),@tintOrder)
                                + '.Fld_Value,0)=1 then 1 end   ['
                                + @vcColumnName + ']'
                SET @WhereCondition = @WhereCondition
                                        + ' left Join CFW_FLD_Values_Opp CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + ' on CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.Fld_Id='
                                        + CONVERT(VARCHAR(10),@numFieldId)
                                        + 'and CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.RecId=Opp.numOppid   '
            END
            ELSE
            IF @vcAssociatedControlType = 'DateField'
            BEGIN
                  SET @strColumns = @strColumns
                                  + ',dbo.FormatedDateFromDate(CFW'
                                  + CONVERT(VARCHAR(3),@tintOrder)
                                  + '.Fld_Value,'
                                  + CONVERT(VARCHAR(10),@numDomainId)
                                  + ')  ['
                                  + @vcColumnName + ']'
                  SET @WhereCondition = @WhereCondition
                                          + ' left Join CFW_FLD_Values_Opp CFW'
                                          + CONVERT(VARCHAR(3),@tintOrder)
                                          + ' on CFW'
                                          + CONVERT(VARCHAR(3),@tintOrder)
                                          + '.Fld_Id='
                                          + CONVERT(VARCHAR(10),@numFieldId)
                                          + 'and CFW'
                                          + CONVERT(VARCHAR(3),@tintOrder)
                                          + '.RecId=Opp.numOppid    '
            END
            ELSE
            IF @vcAssociatedControlType = 'SelectBox'
            BEGIN
                SET @vcDbColumnName = 'DCust'
                                            + CONVERT(VARCHAR(10),@numFieldId)
                SET @strColumns = @strColumns
                                + ',L'
                                + CONVERT(VARCHAR(3),@tintOrder)
                                + '.vcData'
                                + ' ['
                                + @vcColumnName + ']'

                SET @WhereCondition = @WhereCondition
                                        + ' left Join CFW_FLD_Values_Opp CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + ' on CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.Fld_Id='
                                        + CONVERT(VARCHAR(10),@numFieldId)
                                        + 'and CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.RecId=Opp.numOppid '

                SET @WhereCondition = @WhereCondition
                                        + ' left Join ListDetails L'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + ' on L'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.numListItemID=CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.Fld_Value'
            END
			ELSE
			BEGIN
				SET @strColumns = @strColumns + CONCAT(',dbo.GetCustFldValueOpp(',@numFieldId,',opp.numOppID)') + ' [' + @vcColumnName + ']'
			END

			IF LEN(@SearchText) > 0
			BEGIN
				SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + CONCAT('dbo.GetCustFldValueOpp(',@numFieldId,',opp.numOppID) LIKE ''%',@SearchText,'%''')
			END
		END
      
     
		SELECT TOP 1 
			@tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName,@vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,
			@numListID=numListID,@vcLookBackTableName=vcLookBackTableName,@bitCustom=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,@bitAllowEdit=bitAllowEdit,
			@ListRelID=ListRelID
		FROM 
			#tempForm 
		WHERE 
			tintOrder > @tintOrder-1 
		ORDER BY 
			tintOrder asc            
 
		IF @@rowcount=0 SET @tintOrder=0 
	END 

	

	DECLARE @strShareRedordWith AS VARCHAR(300);
	SET @strShareRedordWith=' Opp.numOppId in (select SR.numRecordID FROM ShareRecord SR WHERE SR.numDomainID='+ convert(varchar(15),@numDomainID )+' AND SR.numModuleID=3 AND SR.numAssignedTo='+convert(varchar(15),@numUserCntId)+') '


	DECLARE @StrSql AS VARCHAR(MAX) = ''

	SET @StrSql = @StrSql + ' FROM OpportunityMaster Opp            
								 ##PLACEHOLDER##                                                   
                                 INNER JOIN AdditionalContactsInformation ADC ON Opp.numContactId = ADC.numContactId                                                               
                                 INNER JOIN DivisionMaster Div ON Opp.numDivisionId = Div.numDivisionID AND ADC.numDivisionId = Div.numDivisionID
								 INNER JOIN CompanyInfo cmp ON Div.numCompanyID = cmp.numCompanyId 
								 LEFT JOIN ProjectProgress PP On PP.numOppId = Opp.numOppId ' + @WhereCondition

	-------Change Row Color-------
	
	DECLARE @vcCSOrigDbCOlumnName AS VARCHAR(50) = ''
	DECLARE @vcCSLookBackTableName AS VARCHAR(50) = ''
	DECLARE @vcCSAssociatedControlType AS VARCHAR(50)

	CREATE TABLE #tempColorScheme
	(
		vcOrigDbCOlumnName VARCHAR(50),
		vcLookBackTableName VARCHAR(50),
		vcFieldValue VARCHAR(50),
		vcFieldValue1 VARCHAR(50),
		vcColorScheme VARCHAR(50),
		vcAssociatedControlType varchar(50)
	)

	INSERT INTO 
		#tempColorScheme 
	SELECT 
		DFM.vcOrigDbCOlumnName,
		DFM.vcLookBackTableName,
		DFCS.vcFieldValue,
		DFCS.vcFieldValue1,
		DFCS.vcColorScheme,
		DFFM.vcAssociatedControlType 
	FROM 
		DycFieldColorScheme DFCS 
	JOIN 
		DycFormField_Mapping DFFM 
	ON 
		DFCS.numFieldID=DFFM.numFieldID
	JOIN 
		DycFieldMaster DFM 
	ON 
		DFM.numModuleID=DFFM.numModuleID and DFM.numFieldID=DFFM.numFieldID
	WHERE
		DFCS.numDomainID=@numDomainID 
		AND DFFM.numFormID=@numFormId 
		AND DFCS.numFormID=@numFormId 
		AND isnull(DFFM.bitAllowGridColor,0)=1

	IF(SELECT COUNT(*) FROM #tempColorScheme)>0
	BEGIN
		SELECT TOP 1 @vcCSOrigDbCOlumnName=vcOrigDbCOlumnName,@vcCSLookBackTableName=vcLookBackTableName,@vcCSAssociatedControlType=vcAssociatedControlType from #tempColorScheme
	END                        

	IF len(@vcCSOrigDbCOlumnName)>0 and len(@vcCSLookBackTableName)>0
	BEGIN
		SET @strColumns=@strColumns + ',tCS.vcColorScheme'
	END

	IF len(@vcCSOrigDbCOlumnName)>0 and len(@vcCSLookBackTableName)>0
	BEGIN
		IF @vcCSLookBackTableName = 'AdditionalContactsInformation'                  
			SET @Prefix = 'ADC.'                  
		IF @vcCSLookBackTableName = 'DivisionMaster'                  
			SET @Prefix = 'Div.'
		if @vcCSLookBackTableName = 'CompanyInfo'                  
			SET @Prefix = 'CMP.'   
		if @vcCSLookBackTableName = 'OpportunityMaster'                  
			SET @Prefix = 'Opp.'   
 
		IF @vcCSAssociatedControlType='DateField'
		BEGIN
			SET @strSql=@strSql+' LEFT JOIN #tempColorScheme tCS ON CONVERT(VARCHAR(10),' + @Prefix + @vcCSOrigDbCOlumnName + ',111) >= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue as int),GETUTCDATE()),111)
				 and CONVERT(VARCHAR(10),' + @Prefix + @vcCSOrigDbCOlumnName + ',111) <= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue1 as int),GETUTCDATE()),111)'
		END
		ELSE IF @vcCSAssociatedControlType='SelectBox' OR @vcCSAssociatedControlType='ListBox' OR @vcCSAssociatedControlType='CheckBox'
		BEGIN
			SET @strSql=@strSql+' LEFT JOIN #tempColorScheme tCS on tCS.vcFieldValue=' + @Prefix + @vcCSOrigDbCOlumnName
		END
	END
                          
	IF @columnName like 'CFW.Cust%'             
	BEGIN            
		SET @strSql = @strSql + ' LEFT JOIN CFW_Fld_Values_Opp CFW on CFW.RecId=Opp.numOppid and CFW.fld_id= '+ REPLACE(@columnName,'CFW.Cust','') +' '                                                     
		SET @columnName='CFW.Fld_Value'            
	END 

	IF @bitPartner = 1
		SET @strSql = @strSql + ' LEFT JOIN OpportunityContact OppCont ON OppCont.numOppId=Opp.numOppId AND OppCont.bitPartner=1 AND OppCont.numContactId=' + CONVERT(VARCHAR(15),@numUserCntID)
                    
                
	IF @tintFilterBy = 1 --Partially Fulfilled Orders 
		SET @strSql = @strSql + ' LEFT JOIN AdditionalContactsInformation ADC1 ON ADC1.numContactId=Opp.numRecOwner                                                               
                             LEFT OUTER JOIN [OpportunityBizDocs] OB ON OB.[numOppId] = Opp.[numOppId] 
                             WHERE Opp.numDomainID=' + CONVERT(VARCHAR(15),@numDomainID) + ' and Opp.tintOppstatus=1 and (Opp.tintShipped=' + CONVERT(VARCHAR(15),@tintShipped) + ' OR ' + CONVERT(VARCHAR(15),@tintShipped) + '=2)'
	ELSE
		SET @strSql = @strSql + ' LEFT JOIN AdditionalContactsInformation ADC1 on ADC1.numContactId=Opp.numRecOwner                                                               
                             WHERE Opp.numDomainID=' + CONVERT(VARCHAR(15),@numDomainID) + ' and Opp.tintOppstatus=1 and (Opp.tintShipped='+ CONVERT(VARCHAR(15),@tintShipped)  + ' OR ' + CONVERT(VARCHAR(15),@tintShipped) + '=2)'
  
  
	IF @intOrder <> 0
    BEGIN
      IF @intOrder = 1
        SET @strSql = @strSql + ' and Opp.bitOrder = 0 '
      IF @intOrder = 2
        SET @strSql = @strSql + ' and Opp.bitOrder = 1'
    END
    
	IF @numCompanyID <> 0
		SET @strSql = @strSql + ' And Div.numCompanyID ='+ CONVERT(VARCHAR(15),@numCompanyID)

	IF @SortChar <> '0' 
		SET @strSql = @strSql + ' And Opp.vcPOppName like ''' + @SortChar + '%'''
  

	IF @inttype = 3
	BEGIN
		IF @tintSalesUserRightType = 0
			SET @strSql = @strSql + ' AND opp.tintOppType=0'
		ELSE IF @tintSalesUserRightType = 1
			SET @strSql = @strSql + ' AND (Opp.numRecOwner= ' + CONVERT(VARCHAR(15),@numUserCntID)
						+ ' or Opp.numAssignedTo= '+ CONVERT(VARCHAR(15),@numUserCntID)
						+ CASE WHEN @bitPartner = 1 THEN ' or ( OppCont.bitPartner=1 and OppCont.numContactId='+ CONVERT(VARCHAR(15),@numUserCntID) + ')' ELSE '' END
						+ ' or ' + @strShareRedordWith +')'
		ELSE IF @tintSalesUserRightType = 2
			SET @strSql = @strSql + ' AND (Div.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+ CONVERT(VARCHAR(15),@numUserCntID) + ' ) 
									 or Opp.numAssignedTo= '+ CONVERT(VARCHAR(15),@numUserCntID)
									+ ' or ' + @strShareRedordWith +')'
	END
	ELSE IF @inttype = 4
	BEGIN
		IF @tintPurchaseUserRightType = 0
			SET @strSql = @strSql + ' AND opp.tintOppType=0'
		ELSE IF @tintPurchaseUserRightType = 1
			SET @strSql = @strSql + ' AND (Opp.numRecOwner= ' + CONVERT(VARCHAR(15),@numUserCntID)
						+ ' or Opp.numAssignedTo= '+ CONVERT(VARCHAR(15),@numUserCntID)
						+ CASE WHEN @bitPartner = 1 THEN ' or ( OppCont.bitPartner=1 and OppCont.numContactId='+ CONVERT(VARCHAR(15),@numUserCntID) + ')' ELSE '' END
						+ ' or ' + @strShareRedordWith +')'
		ELSE IF @tintPurchaseUserRightType = 2
			SET @strSql = @strSql + ' AND (Div.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+ CONVERT(VARCHAR(15),@numUserCntID) + ' ) 
									 or Opp.numAssignedTo= '+ CONVERT(VARCHAR(15),@numUserCntID)
									+ ' or ' + @strShareRedordWith +')'
	END

	
					
	IF @tintSortOrder <> '0'
		SET @strSql = @strSql + '  AND Opp.tintOppType= '+ CONVERT(VARCHAR(1),@tintSortOrder)
    
    
	IF @tintFilterBy = 1 --Partially Fulfilled Orders
		SET @strSql = @strSql + ' AND OB.bitPartialFulfilment = 1 AND OB.bitAuthoritativeBizDocs = 1 '
	ELSE IF @tintFilterBy = 2 --Fulfilled Orders
		SET @strSql = @strSql + ' AND Opp.[numOppId] IN (SELECT OM.numOppId FROM [OpportunityMaster] OM
                      			INNER JOIN [OpportunityBizDocs] OB ON OB.[numOppId] = OM.[numOppId]
                      			INNER JOIN [OpportunityItems] OI ON OM.[numOppId] = OI.[numOppId]
                      			INNER JOIN [OpportunityBizDocItems] BI ON BI.[numOppBizDocID] = OB.[numOppBizDocsId]
                      			Where OM.numDomainId=' + CONVERT(VARCHAR(15),@numDomainId) + 'AND OB.[bitAuthoritativeBizDocs] = 1 GROUP BY OM.[numOppId]
                      			HAVING   COUNT(OI.[numUnitHour]) = COUNT(BI.[numUnitHour])) '
	ELSE IF @tintFilterBy = 3 --Orders without Authoritative-BizDocs
		SET @strSql = @strSql + ' AND Opp.[numOppId] not IN (SELECT DISTINCT OM.[numOppId]
                      		  FROM [OpportunityMaster] OM JOIN [OpportunityBizDocs] OB ON OM.[numOppId] = OB.[numOppId]
                      		  Where OM.numDomainId=' + CONVERT(VARCHAR(15),@numDomainId) + ' AND OM.tintOppstatus=1 AND (OM.tintShipped=' + CONVERT(VARCHAR(15),@tintShipped) + ' OR ' + CONVERT(VARCHAR(15),@tintShipped) + '=2)' + ' AND OM.tintOppType= '+ CONVERT(VARCHAR(1),@tintSortOrder) + ' AND isnull(OB.[bitAuthoritativeBizDocs],0)=1) '
	ELSE IF @tintFilterBy = 4 --Orders with items yet to be added to Invoices
		SET @strSql = @strSql + ' AND Opp.[numOppId] IN (SELECT distinct OPPM.numOppId FROM [OpportunityMaster] OPPM
                                  INNER JOIN [OpportunityItems] OPPI ON OPPM.[numOppId] = OPPI.[numOppId]
                                  Where OPPM.numDomainId=' + CONVERT(VARCHAR(15),@numDomainId) + ' 
								  and OPPI.numoppitemtCode not in (select numOppItemID from [OpportunityBizDocs] OB INNER JOIN [OpportunityBizDocItems] BI
                      			  ON BI.[numOppBizDocID] = OB.[numOppBizDocsId] where OB.[numOppId] = OPPM.[numOppId]))'      
	ELSE IF @tintFilterBy = 6 --Orders with deferred Income/Invoices
		SET @strSql = @strSql + ' AND Opp.[numOppId] IN (SELECT DISTINCT OM.[numOppId]
                      		 FROM [OpportunityMaster] OM LEFT OUTER JOIN [OpportunityBizDocs] OB ON OM.[numOppId] = OB.[numOppId]
                      		 Where OM.numDomainId=' + CONVERT(VARCHAR(15),@numDomainId) + ' AND OB.[bitAuthoritativeBizDocs] =1 and OB.tintDeferred=1)'

	IF @numOrderStatus <>0 
		SET @strSql = @strSql + ' AND Opp.numStatus = ' + CONVERT(VARCHAR(15),@numOrderStatus);

	IF CHARINDEX('vcInventoryStatus',@vcRegularSearchCriteria) > 0
	BEGIN
		IF CHARINDEX('vcInventoryStatus=1',@vcRegularSearchCriteria) > 0
			SET @strSql = @strSql + ' AND ' + ' CHARINDEX(''Not Applicable'', dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID)) > 0 '
		IF CHARINDEX('vcInventoryStatus=2',@vcRegularSearchCriteria) > 0
			SET @strSql = @strSql + ' AND ' + ' CHARINDEX(''Allocation Cleared'', dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID)) > 0 '
		IF CHARINDEX('vcInventoryStatus=3',@vcRegularSearchCriteria) > 0
			SET @strSql = @strSql + ' AND ' + ' CHARINDEX(''Back Order'', dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID)) > 0 '
		IF CHARINDEX('vcInventoryStatus=4',@vcRegularSearchCriteria) > 0
			SET @strSql = @strSql + ' AND ' + ' CHARINDEX(''Ready to Ship'', dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID)) > 0 '
	END
	ELSE IF @vcRegularSearchCriteria<>'' 
	BEGIN
		SET @strSql=@strSql +' AND ' + @vcRegularSearchCriteria 
	END
	
	IF @vcCustomSearchCriteria<>'' 
	BEGIN
		SET @strSql = @strSql +' AND ' +  @vcCustomSearchCriteria
	END

	IF LEN(@SearchQuery) > 0
	BEGIN
		SET @strSql = @strSql + ' AND (' + @SearchQuery + ') '
	END

	

	DECLARE  @firstRec  AS INTEGER
	DECLARE  @lastRec  AS INTEGER
  
	SET @firstRec = (@CurrentPage - 1) * @PageSize
	SET @lastRec = (@CurrentPage * @PageSize + 1)



	-- EXECUTE FINAL QUERY
	DECLARE @strFinal AS NVARCHAR(MAX)
	SET @strFinal = CONCAT('SELECT ROW_NUMBER() OVER(ORDER BY ',@columnName,' ',@columnSortOrder,') ID, Opp.numOppID INTO #temp2', REPLACE(@strSql,'##PLACEHOLDER##',''),'; SELECT ID,',@strColumns,' INTO #tempTable',REPLACE(@strSql,'##PLACEHOLDER##',' JOIN #temp2 tblAllOrders ON Opp.numOppID = tblAllOrders.numOppID '),' AND tblAllOrders.ID > ',@firstRec,' and tblAllOrders.ID <',@lastRec,' ORDER BY ID; SELECT * FROM  #tempTable; DROP TABLE #tempTable; SELECT @TotalRecords = COUNT(*) FROM #temp2; DROP TABLE #temp2;')
	--SET @strFinal = CONCAT('SELECT ROW_NUMBER() OVER(ORDER BY ',@columnName,' ',@columnSortOrder,') ID, ',@strColumns,' INTO #tempTable',@strSql,'; SELECT * FROM #tempTable WHERE ID > ',@firstRec,' and ID <',@lastRec,' ORDER BY ID; SELECT @TotalRecords = COUNT(*) FROM #tempTable; DROP TABLE #tempTable;')

	PRINT @strFinal
	EXEC sp_executesql @strFinal, N'@TotalRecords INT OUT', @TotRecs OUT

	SELECT * FROM #tempForm

	DROP TABLE #tempForm

	DROP TABLE #tempColorScheme
--created by anoop jayaraj                                          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getdomaindetails')
DROP PROCEDURE usp_getdomaindetails
GO
CREATE PROCEDURE [dbo].[USP_GetDomainDetails]                                          
@numDomainID as numeric(9)=0                                          
as           

DECLARE @listIds VARCHAR(MAX)
SELECT @listIds = COALESCE(@listIds+',' ,'') + CAST(numUserID AS VARCHAR(100)) FROM UnitPriceApprover WHERE numDomainID = @numDomainID
   
DECLARE @canChangeDiferredIncomeSelection AS BIT = 1

IF EXISTS(SELECT 
			OB.numOppBizDocsId 
		FROM 
			OpportunityBizDocs OB 
		INNER JOIN 
			OpportunityMaster OM 
		ON 
			OB.numOppId=OM.numOppId 
		WHERE 
			numBizDocId=304 
			AND OM.numDomainId=@numDomainID)
BEGIN
	SET @canChangeDiferredIncomeSelection = 0
END   
   
                               
select                                           
vcDomainName,                                           
isnull(vcDomainDesc,'') vcDomainDesc,                                           
isnull(bitExchangeIntegration,0) bitExchangeIntegration,                                           
isnull(bitAccessExchange,0) bitAccessExchange,                                           
isnull(vcExchUserName,'') vcExchUserName,                                           
isnull(vcExchPassword,'') vcExchPassword,                                           
isnull(vcExchPath,'') vcExchPath,                                           
isnull(vcExchDomain,'') vcExchDomain,                                         
tintCustomPagingRows,                                         
vcDateFormat,                                         
numDefCountry,                                         
tintComposeWindow,                                         
sintStartDate,                                         
sintNoofDaysInterval,                                         
tintAssignToCriteria,                                         
bitIntmedPage,
ISNULL(D.numCost,0) as numCost,                                         
tintFiscalStartMonth,                                    
--isnull(bitDeferredIncome,0) as bitDeferredIncome,                                  
isnull(tintPayPeriod,0) as tintPayPeriod,                                
isnull(numCurrencyID,0) as numCurrencyID,                            
isnull(vcCurrency,'') as vcCurrency,                      
isnull(charUnitSystem,'') as charUnitSystem ,                    
isnull(vcPortalLogo,'') as vcPortalLogo,                  
isnull(tintChrForComSearch,2) as tintChrForComSearch,                
isnull(tintChrForItemSearch,'') as tintChrForItemSearch,            
isnull(intPaymentGateWay,0) as intPaymentGateWay  ,        
 case when bitPSMTPServer= 1 then vcPSMTPServer else '' end as vcPSMTPServer  ,              
 case when bitPSMTPServer= 1 then  isnull(numPSMTPPort,0)  else 0 end as numPSMTPPort          
,isnull(bitPSMTPAuth,0) bitPSMTPAuth        
,isnull([vcPSmtpPassword],'')vcPSmtpPassword        
,isnull(bitPSMTPSSL,0) bitPSMTPSSL     
,isnull(bitPSMTPServer,0) bitPSMTPServer  ,isnull(vcPSMTPUserName,'') vcPSMTPUserName,      
--isnull(numDefaultCategory,0) numDefaultCategory,
isnull(numShipCompany,0)   numShipCompany,
isnull(bitAutoPopulateAddress,0) bitAutoPopulateAddress,
isnull(tintPoulateAddressTo,0) tintPoulateAddressTo,
isnull(bitMultiCompany,0) bitMultiCompany,
isnull(bitMultiCurrency,0) bitMultiCurrency,
ISNULL(bitCreateInvoice,0) bitCreateInvoice,
ISNULL([numDefaultSalesBizDocId],0) numDefaultSalesBizDocId,
ISNULL([bitSaveCreditCardInfo],0) bitSaveCreditCardInfo,
ISNULL([intLastViewedRecord],10) intLastViewedRecord,
ISNULL([tintCommissionType],1) tintCommissionType,
ISNULL([tintBaseTax],2) tintBaseTax,
ISNULL([numDefaultPurchaseBizDocId],0) numDefaultPurchaseBizDocId,
ISNULL(bitEmbeddedCost,0) bitEmbeddedCost,
ISNULL(numDefaultSalesOppBizDocId,0) numDefaultSalesOppBizDocId,
ISNULL(tintSessionTimeOut,1) tintSessionTimeOut,
ISNULL(tintDecimalPoints,2) tintDecimalPoints,
ISNULL(bitCustomizePortal,0) bitCustomizePortal,
ISNULL(tintBillToForPO,0) tintBillToForPO,
ISNULL(tintShipToForPO,0) tintShipToForPO,
ISNULL(tintOrganizationSearchCriteria,1) tintOrganizationSearchCriteria,
ISNULL(tintLogin,0) tintLogin,
lower(ISNULL(vcPortalName,'')) vcPortalName,
ISNULL(bitDocumentRepositary,0) bitDocumentRepositary,
D.numDomainID,
ISNULL(vcPSMTPDisplayName,'') vcPSMTPDisplayName,
ISNULL(bitGtoBContact,0) bitGtoBContact,
ISNULL(bitBtoGContact,0) bitBtoGContact,
ISNULL(bitGtoBCalendar,0) bitGtoBCalendar,
ISNULL(bitBtoGCalendar,0) bitBtoGCalendar,
ISNULL(bitExpenseNonInventoryItem,0) bitExpenseNonInventoryItem,
ISNULL(bitInlineEdit,0) bitInlineEdit,
ISNULL(bitRemoveVendorPOValidation,0) bitRemoveVendorPOValidation,
isnull(D.tintBaseTaxOnArea,0) tintBaseTaxOnArea,
ISNULL(D.bitAmountPastDue,0) bitAmountPastDue,
CONVERT(DECIMAL(18,2),ISNULL(D.monAmountPastDue,0)) monAmountPastDue,
ISNULL(bitSearchOrderCustomerHistory,0) bitSearchOrderCustomerHistory,
isnull(D.bitRentalItem,0) as bitRentalItem,
isnull(D.numRentalItemClass,0) as numRentalItemClass,
isnull(D.numRentalHourlyUOM,0) as numRentalHourlyUOM,
isnull(D.numRentalDailyUOM,0) as numRentalDailyUOM,
isnull(D.tintRentalPriceBasedOn,0) as tintRentalPriceBasedOn,
isnull(D.tintCalendarTimeFormat,24) as tintCalendarTimeFormat,
isnull(D.tintDefaultClassType,0) as tintDefaultClassType,
ISNULL(D.tintPriceBookDiscount,0) tintPriceBookDiscount,
ISNULL(D.bitAutoSerialNoAssign,0) as bitAutoSerialNoAssign,
ISNULL(D.bitIsShowBalance,0) AS [bitIsShowBalance],
ISNULL(numIncomeAccID,0) AS [numIncomeAccID],
ISNULL(numCOGSAccID,0) AS [numCOGSAccID],
ISNULL(numAssetAccID,0) AS [numAssetAccID],
ISNULL(IsEnableProjectTracking,0) AS [IsEnableProjectTracking],
ISNULL(IsEnableCampaignTracking,0) AS [IsEnableCampaignTracking],
ISNULL(IsEnableResourceScheduling,0) AS [IsEnableResourceScheduling],
ISNULL(numShippingServiceItemID,0) AS [numShippingServiceItemID],
ISNULL(D.numSOBizDocStatus,0) AS numSOBizDocStatus,
ISNULL(D.bitAutolinkUnappliedPayment,0) AS bitAutolinkUnappliedPayment,
ISNULL(D.numDiscountServiceItemID,0) AS numDiscountServiceItemID,
ISNULL(D.vcShipToPhoneNo,0) AS vcShipToPhoneNo,
ISNULL(D.vcGAUserEMail,'') AS vcGAUserEMail,
ISNULL(D.vcGAUserPassword,'') AS vcGAUserPassword,
ISNULL(D.vcGAUserProfileId,0) AS vcGAUserProfileId,
ISNULL(D.bitDiscountOnUnitPrice,0) AS bitDiscountOnUnitPrice,
ISNULL(D.intShippingImageWidth,0) AS intShippingImageWidth ,
ISNULL(D.intShippingImageHeight,0) AS intShippingImageHeight,
ISNULL(D.numTotalInsuredValue,0) AS numTotalInsuredValue,
ISNULL(D.numTotalCustomsValue,0) AS numTotalCustomsValue,
ISNULL(D.vcHideTabs,'') AS vcHideTabs,
ISNULL(D.bitUseBizdocAmount,0) AS bitUseBizdocAmount,
ISNULL(D.bitDefaultRateType,0) AS bitDefaultRateType,
ISNULL(D.bitIncludeTaxAndShippingInCommission,0) AS bitIncludeTaxAndShippingInCommission,
/*,ISNULL(D.bitAllowPPVariance,0) AS bitAllowPPVariance*/
ISNULL(D.bitPurchaseTaxCredit,0) AS bitPurchaseTaxCredit,
ISNULL(D.bitLandedCost,0) AS bitLandedCost,
ISNULL(D.vcLanedCostDefault,'') AS vcLanedCostDefault,
ISNULL(D.bitMinUnitPriceRule,0) AS bitMinUnitPriceRule,
ISNULL(D.numAbovePercent,0) AS numAbovePercent,
ISNULL(D.numBelowPercent,0) AS numBelowPercent,
ISNULL(D.numBelowPriceField,0) AS numBelowPriceField,
@listIds AS vcUnitPriceApprover,
ISNULL(D.numDefaultSalesPricing,2) AS numDefaultSalesPricing,
ISNULL(D.bitReOrderPoint,0) AS bitReOrderPoint,
ISNULL(D.numReOrderPointOrderStatus,0) AS numReOrderPointOrderStatus,
ISNULL(D.numPODropShipBizDoc,0) AS numPODropShipBizDoc,
ISNULL(D.numPODropShipBizDocTemplate,0) AS numPODropShipBizDocTemplate,
ISNULL(D.IsEnableDeferredIncome,0) AS IsEnableDeferredIncome,
ISNULL(D.bitApprovalforTImeExpense,0) AS bitApprovalforTImeExpense,
ISNULL(D.intTimeExpApprovalProcess,0) AS intTimeExpApprovalProcess,
ISNULL(D.bitApprovalforOpportunity,0) AS bitApprovalforOpportunity,
ISNULL(D.intOpportunityApprovalProcess,0) AS intOpportunityApprovalProcess,
@canChangeDiferredIncomeSelection AS canChangeDiferredIncomeSelection,
ISNULL(CAST(D.numDefaultSalesShippingDoc AS int),0) AS numDefaultSalesShippingDoc,
ISNULL(CAST(D.numDefaultPurchaseShippingDoc AS int),0) AS numDefaultPurchaseShippingDoc,
ISNULL(D.bitchkOverRideAssignto,0) AS bitchkOverRideAssignto,
ISNULL(D.vcPrinterIPAddress,'') vcPrinterIPAddress,
ISNULL(D.vcPrinterPort,'') vcPrinterPort,
ISNULL(bitCostApproval,0) bitCostApproval
,ISNULL(bitListPriceApproval,0) bitListPriceApproval
,ISNULL(bitMarginPriceViolated,0) bitMarginPriceViolated
,ISNULL(numDefaultSiteID,0) AS numDefaultSiteID
from Domain D  
left join eCommerceDTL eComm  
on eComm.numDomainID=D.numDomainID  
where (D.numDomainID=@numDomainID OR @numDomainID=-1)


/****** Object:  StoredProcedure [dbo].[usp_GetDuplicateCompany]    Script Date: 07/26/2008 16:17:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getdueamount')
DROP PROCEDURE usp_getdueamount
GO
CREATE PROCEDURE [dbo].[USP_GetDueAmount]                      
@numDivisionID numeric(9)=0                      
as                      
declare @Amt as MONEY;set @Amt=0                        
declare @Paid as MONEY;set @Paid=0      
                 
declare @AmountDueSO as money;set @AmountDueSO=0                    
declare @AmountPastDueSO as money;set @AmountPastDueSO=0 

declare @AmountDuePO as money;set @AmountDuePO=0                    
declare @AmountPastDuePO as money;set @AmountPastDuePO=0 
                   
declare @CompanyCredit as money;set @CompanyCredit=0   
          
declare @PCreditMemo as money;set @PCreditMemo=0            
declare @SCreditMemo as money;set @SCreditMemo=0            

DECLARE @UTCtime AS datetime	       
SET	@UTCtime = dbo.[GetUTCDateWithoutTime]()

--Sales Order Total Amount Due                
select @Amt=isnull(sum(OppBD.[monDealAmount]),0) from OpportunityMaster  Opp
join  OpportunityBizDocs OppBD
on    OppBD.numOppID=Opp.numOppID               
where bitAuthoritativeBizDocs=1 and numDivisionID=@numDivisionID and tintOppStatus=1   and tintOpptype=1              
               
select @Paid=isnull(sum(monAmountPaid),0) from OpportunityMaster  Opp
join  OpportunityBizDocs OppBD
on    OppBD.numOppID=Opp.numOppID               
where bitAuthoritativeBizDocs=1 and numDivisionID=@numDivisionID and tintOppStatus=1   and tintOpptype=1               
                
set @AmountDueSO=@Amt-@Paid                
         
--Sales Order Total Amount Past Due                
select @Amt=isnull(sum(OppBD.[monDealAmount]),0) from OpportunityMaster   Opp
join  OpportunityBizDocs OppBD
on    OppBD.numOppID=Opp.numOppID               
where  bitAuthoritativeBizDocs=1 and numDivisionID=@numDivisionID and tintOppStatus=1   and tintOpptype=1              
AND DATEADD(day,CASE WHEN Opp.bitBillingTerms = 1 
					 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(Opp.intBillingDays,0)),0))  
					 ELSE 0 
				END, OppBD.[dtFromDate]) < @UTCtime
--AND DATEADD(day,CASE WHEN Opp.bitBillingTerms = 1 
--					 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(Opp.intBillingDays,0)),0))  
--					 ELSE 0 
--				END, OppBD.[dtFromDate]) < @UTCtime				

select @Paid=isnull(SUM(OppBD.monAmountPaid),0) from OpportunityMaster  Opp
join  OpportunityBizDocs OppBD on    OppBD.numOppID=Opp.numOppID               
where  OppBD.bitAuthoritativeBizDocs=1 and Opp.numDivisionID=@numDivisionID and tintOppStatus=1  and tintOpptype=1               
AND DATEADD(day,CASE WHEN Opp.bitBillingTerms = 1 
					 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(Opp.intBillingDays,0)),0)) 
					 ELSE 0 
				END, OppBD.[dtFromDate]) <@UTCtime
--AND DATEADD(day,CASE WHEN Opp.bitBillingTerms = 1 
--					 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(Opp.intBillingDays,0)),0)) 
--					 ELSE 0 
--				END, OppBD.[dtFromDate]) <@UTCtime				

set @AmountPastDueSO=@Amt-@Paid                    
                    
   
--Purchase Order Total Amount Due                
select @Amt=isnull(sum(OppBD.[monDealAmount]),0) from OpportunityMaster  Opp
join  OpportunityBizDocs OppBD
on    OppBD.numOppID=Opp.numOppID               
where bitAuthoritativeBizDocs=1 and numDivisionID=@numDivisionID and tintOppStatus=1   and tintOpptype=2              
               
select @Paid=isnull(sum(monAmountPaid),0) from OpportunityMaster  Opp
join  OpportunityBizDocs OppBD
on    OppBD.numOppID=Opp.numOppID               
where bitAuthoritativeBizDocs=1 and numDivisionID=@numDivisionID and tintOppStatus=1   and tintOpptype=2              
                
set @AmountDuePO=@Amt-@Paid                
         
--Purchase Order Total Amount Past Due                
select @Amt=isnull(sum(OppBD.[monDealAmount]),0) from OpportunityMaster   Opp
join  OpportunityBizDocs OppBD
on    OppBD.numOppID=Opp.numOppID               
where  bitAuthoritativeBizDocs=1 and numDivisionID=@numDivisionID and tintOppStatus=1   and tintOpptype=2              
AND DATEADD(day,CASE WHEN Opp.bitBillingTerms = 1 
					 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(Opp.intBillingDays,0)),0))  
					 ELSE 0 
				END, OppBD.[dtFromDate]) <@UTCtime
--AND DATEADD(day,CASE WHEN Opp.bitBillingTerms = 1 
--					 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(Opp.intBillingDays,0)),0))  
--					 ELSE 0 
--				END, OppBD.[dtFromDate]) <@UTCtime
				

select @Paid=isnull(SUM(OppBD.monAmountPaid),0) from OpportunityMaster  Opp
join  OpportunityBizDocs OppBD on    OppBD.numOppID=Opp.numOppID               
where  OppBD.bitAuthoritativeBizDocs=1 and Opp.numDivisionID=@numDivisionID and tintOppStatus=1  and tintOpptype=2               
AND DATEADD(day,CASE WHEN Opp.bitBillingTerms = 1 
					 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(Opp.intBillingDays,0)),0)) 
					 ELSE 0 
				END, OppBD.[dtFromDate]) <@UTCtime
--AND DATEADD(day,CASE WHEN Opp.bitBillingTerms = 1 
--					 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(Opp.intBillingDays,0)),0)) 
--					 ELSE 0 
--				END, OppBD.[dtFromDate]) <@UTCtime

set @AmountPastDuePO=@Amt-@Paid  


--Company Cresit Limit                    
select @CompanyCredit=convert(money,case when isnumeric(dbo.fn_GetListItemName(numCompanyCredit))=1 then dbo.fn_GetListItemName(numCompanyCredit) else '0' end)
	 from companyinfo C                    
join divisionmaster d                    
on d.numCompanyID=C.numCompanyID                    
where d.numdivisionid=@numDivisionID                    
                    
 
--Purchase Credit Memo
select @PCreditMemo=ISNULL(BPH.monPaymentAmount,0) - ISNULL (BPH.monAppliedAmount,0)
	 from BillPaymentHeader BPH          
where BPH.numdivisionid=@numDivisionID AND ISNULL(numReturnHeaderID,0)>0                  

--Sales Credit Memo
select @SCreditMemo=ISNULL(DM.monDepositAmount,0) - ISNULL (DM.monAppliedAmount,0)
	 from dbo.DepositMaster DM          
where DM.numdivisionid=@numDivisionID AND tintDepositePage=3 AND ISNULL(numReturnHeaderID,0)>0  

DECLARE @vcShippersAccountNo AS VARCHAR(100)
DECLARE @numDefaultShippingServiceID AS NUMERIC(18,0)
DECLARE @intShippingCompany AS NUMERIC(18,0)
DECLARE @numPartnerSource NUMERIC(18,0)
SELECT @vcShippersAccountNo = ISNULL(vcShippersAccountNo,''), @numDefaultShippingServiceID=ISNULL(numDefaultShippingServiceID,0), @intShippingCompany=ISNULL(intShippingCompany,0), @numPartnerSource=ISNULL(numPartenerSource,0) FROM dbo.DivisionMaster WHERE numDivisionID = @numDivisionID

select isnull(@AmountDueSO,0) as AmountDueSO,isnull(@AmountPastDueSO,0) as AmountPastDueSO, 
isnull(@AmountDuePO,0) as AmountDuePO,isnull(@AmountPastDuePO,0) as AmountPastDuePO,                      
ISNULL(@CompanyCredit,0) - (isnull(@AmountDueSO,0) + isnull(@AmountDuePO,0)) as RemainingCredit,
isnull(@PCreditMemo,0) as PCreditMemo,
isnull(@SCreditMemo,0) as SCreditMemo,
ISNULL(@CompanyCredit,0) AS CreditLimit,
@vcShippersAccountNo AS vcShippersAccountNo,
@numDefaultShippingServiceID AS numDefaultShippingServiceID,
@intShippingCompany AS intShippingCompany,
@numPartnerSource AS numPartnerSource

GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetECommerceDetails')
DROP PROCEDURE USP_GetECommerceDetails
GO
CREATE PROCEDURE [dbo].[USP_GetECommerceDetails]      
@numDomainID as numeric(9)=0,
@numSiteID AS INTEGER = 0  
as
BEGIN
	IF ISNULL(@numSiteID,0) = 0
	BEGIN
		SELECT @numSiteID=numDefaultSiteID FROM Domain WHERE numDomainId=@numDomainID
	END                  
select                               
isnull(vcPaymentGateWay,'0') as vcPaymentGateWay,                
isnull(vcPGWManagerUId,'')as vcPGWManagerUId ,                
isnull(vcPGWManagerPassword,'') as vcPGWManagerPassword,              
isnull(bitShowInStock,0) as bitShowInStock,              
isnull(bitShowQOnHand,0) as bitShowQOnHand, 
isnull([numDefaultWareHouseID],0) numDefaultWareHouseID,
isnull(bitCheckCreditStatus,0) as bitCheckCreditStatus,
isnull([numRelationshipId],0) numRelationshipId,
isnull([numProfileId],0) numProfileId,
ISNULL([bitHidePriceBeforeLogin],0) AS bitHidePriceBeforeLogin,
ISNULL([numBizDocForCreditCard],0) AS numBizDocForCreditCard,
ISNULL([numBizDocForCreditTerm],0) AS numBizDocForCreditTerm,
ISNULL(bitAuthOnlyCreditCard,0) AS bitAuthOnlyCreditCard,
ISNULL(numAuthrizedOrderStatus,0) AS numAuthrizedOrderStatus,
ISNULL(bitSendEmail,1) AS bitSendEmail,
ISNULL(vcGoogleMerchantID , '') AS vcGoogleMerchantID,
ISNULL(vcGoogleMerchantKey,'') AS vcGoogleMerchantKey,
ISNULL(IsSandbox,0) AS IsSandbox,
ISNULL(numSiteID,0) AS numSiteID,
ISNULL(vcPaypalUserName,'') AS vcPaypalUserName,
ISNULL(vcPaypalPassword,'') AS vcPaypalPassword,
ISNULL(vcPaypalSignature,'') AS vcPaypalSignature,
ISNULL(IsPaypalSandbox,0) AS IsPaypalSandbox,
ISNULL(bitSkipStep2,0) AS [bitSkipStep2],
ISNULL(bitDisplayCategory,0) AS [bitDisplayCategory],
ISNULL(bitShowPriceUsingPriceLevel,0) AS [bitShowPriceUsingPriceLevel],
ISNULL(bitEnableSecSorting,0) AS [bitEnableSecSorting],
ISNULL(bitSortPriceMode,0) AS [bitSortPriceMode],
ISNULL(numPageSize,0) AS [numPageSize],
ISNULL(numPageVariant,0) AS [numPageVariant],
ISNULL(bitAutoSelectWarehouse,0) AS bitAutoSelectWarehouse,
ISNULL([bitPreSellUp],0) AS [bitPreSellUp],
ISNULL([bitPostSellUp],0) AS [bitPostSellUp],
ISNULL([dcPostSellDiscount],0) AS [dcPostSellDiscount],
ISNULL(numDefaultClass,0) numDefaultClass,
vcPreSellUp
,vcPostSellUp
,ISNULL(vcRedirectThankYouUrl,'') AS vcRedirectThankYouUrl
,ISNULL(tintPreLoginProceLevel,0) tintPreLoginProceLevel
FROM eCommerceDTL 
WHERE numDomainID=@numDomainID
AND ([numSiteId] = @numSiteID OR ISNULL(@numSiteID,0) = 0)
END
/****** Object:  StoredProcedure [dbo].[USP_GetOppWareHouseItemAttributes]    Script Date: 07/26/2008 16:18:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetOppWareHouseItemAttributesIds')
DROP PROCEDURE USP_GetOppWareHouseItemAttributesIds
GO
CREATE PROCEDURE [dbo].[USP_GetOppWareHouseItemAttributesIds]
@numRecID as numeric(9)=0
AS
BEGIN
	SELECT dbo.fn_GetAttributesIds(@numRecID)
END
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetOrderItems' ) 
    DROP PROCEDURE USP_GetOrderItems
GO
CREATE PROCEDURE USP_GetOrderItems ( @tintMode TINYINT = 0,
                                     @numOppItemID NUMERIC(9) = 0,
                                     @numOppID NUMERIC(9) = 0,
                                     @numDomainId NUMERIC(9),
                                     @ClientTimeZoneOffset INT,
									 @vcOppItemIDs VARCHAR(2000)='' )
AS 
BEGIN
	
    IF @tintMode = 1 --Add Return grid data & For Recurring Get Original Units to split
        BEGIN
            SELECT  OI.numoppitemtCode,
                    OI.numItemCode,
                    OI.vcItemName,
                    OI.vcItemDesc,
					OI.vcNotes as txtNotes,ISNULL(OI.numCost,0) AS numCost,
                    OI.numUnitHour numOriginalUnitHour,
					CAST(( dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), OI.numItemCode,
                                    OM.numDomainId, ISNULL(OI.numUOMId, 0))
						* OI.numUnitHour ) AS NUMERIC(18, 2)) AS numUnitHour,
					OI.monPrice
            FROM    dbo.OpportunityMaster OM
                    INNER JOIN dbo.OpportunityItems OI ON OM.numOppId = OI.numOppId
                    INNER JOIN dbo.Item I ON I.numItemCode = OI.numItemCode
            WHERE   OM.numDomainId = @numDomainId
                    AND OM.numOppId = @numOppID
                    
        END
 
    IF @tintMode = 2 -- Get single item based on PK, or all items
		-- Used by Sales/Purchase Template Load items in New Sales Order
        BEGIN 
            SELECT  OI.numoppitemtCode,
                    OI.numOppId,
                    OI.numItemCode,
					OI.bitItemPriceApprovalRequired,
                    OI.numUnitHour numOriginalUnitHour,
                    OI.monPrice,
                    OI.monTotAmount,
                    OI.numSourceID,
					ISNULL(OI.numCost,0) AS numCost,
                    OI.vcItemDesc,
					OI.vcNotes as txtNotes,
                    OI.numWarehouseItmsID,
                    OI.vcType,
                    OI.vcAttributes,
                    OI.bitDropShip,
                    ISNULL(OI.numUnitHourReceived,0) AS numUnitHourReceived,
                    OI.bitDiscountType,
                    OI.fltDiscount,
                    OI.monTotAmtBefDiscount,
                    ISNULL(OI.numQtyShipped,0) AS numQtyShipped,
                    OI.vcItemName,
                    OI.vcModelID,
                    OI.vcPathForTImage,
                    OI.vcManufacturer,
                    OI.monVendorCost,
                    ISNULL(OI.numUOMId, 0) AS numUOMId,
                    ISNULL(OI.numUOMId, 0) numUOM, -- used from add edit order
                    ISNULL(U.vcUnitName, '') vcUOMName,
                    dbo.fn_UOMConversion(OI.numUOMId, OI.numItemCode,
                                         OM.numDomainId, NULL) AS UOMConversionFactor,
                    OI.bitWorkOrder,
                    isnull(OI.numVendorWareHouse,0) as numVendorWareHouse,
                    OI.numShipmentMethod,
                    OI.numSOVendorId,
                    OI.numProjectID,
                    OI.numClassID,
                    OI.numProjectStageID,
                    OI.numToWarehouseItemID,
                    WItems.numWarehouseID,
                    W.vcWareHouse AS Warehouse,
					CAST(( dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), OI.numItemCode,
                                    OM.numDomainId, ISNULL(OI.numUOMId, 0))
						* OI.numUnitHour ) AS NUMERIC(18, 2)) AS numUnitHour,
					isnull(OI.numClassID,0) as numClassID,
					isnull(OI.numProjectID,0) as numProjectID,
					dbo.fn_GetUOMName(I.numBaseUnit) AS vcBaseUOMName,
					OI.bitDropShip AS bitAllowDropShip,
					I.numVendorID,
					ISNULL(I.bitFreeShipping,0) AS [bitFreeShipping],
					ISNULL(I.txtItemDesc,'') txtItemDesc,
					ISNULL(I.vcModelID,'') vcModelID,
					ISNULL(I.vcManufacturer,'') vcManufacturer,
					ISNULL(I.numBarCodeId,'') numBarCodeId,
					ISNULL(I.bitSerialized,0) bitSerialized,
					ISNULL(I.bitKitParent,0) bitKitParent,
					ISNULL(I.bitAssembly,0) bitAssembly,
					ISNULL(I.IsArchieve,0) IsArchieve,
					ISNULL(I.bitLotNo,0) bitLotNo,
					ISNULL(WItems.monWListPrice,0) monWListPrice,
					ISNULL(I.bitTaxable, 0) bitTaxable,
					ISNULL(I.numItemClassification,0) AS numItemClassification,
                    I.charItemType,
                    I.bitKitParent,
					I.bitAssembly,
					I.bitSerialized,
					ISNULL(I.fltWeight,0) AS [fltWeight],
					ISNULL(I.fltLength,0) AS [fltLength],
					ISNULL(I.fltWidth,0) AS [fltWidth],
					ISNULL(I.fltHeight,0) AS [fltHeight],
					ISNULL(WItems.vcWHSKU,ISNULL(I.vcSKU,'')) AS [vcSKU],
					(CASE WHEN ISNULL(I.bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(I.monAverageCost,0) END)  AS monAverageCost,
					CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numOnHand),0) AS VARCHAR(10)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numOnHand,
					CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numOnOrder),0) AS VARCHAR(10)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numOnOrder,
					CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numAllocation),0) AS VARCHAR(10)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numAllocation,
					CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numBackOrder),0) AS VARCHAR(10)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numBackOrder,
					ISNULL(OI.vcAttrValues,'') AS vcAttrValues,
					ISNULL(OI.numSortOrder,0) numSortOrder
            FROM    dbo.OpportunityItems OI
                    INNER JOIN dbo.OpportunityMaster OM ON OM.numOppId = OI.numOppId
                    INNER JOIN dbo.Item I ON I.numItemCode = OI.numItemCode
                    LEFT JOIN UOM U ON U.numUOMId = OI.numUOMId
                    LEFT JOIN WareHouseItems WItems ON WItems.numWareHouseItemID = OI.numWarehouseItmsID
                    LEFT JOIN Warehouses W ON W.numWarehouseID = WItems.numWarehouseID
            WHERE   OI.numOppId = @numOppID
                    AND ( OI.numoppitemtCode = @numOppItemID
                          OR @numOppItemID = 0
                        )
                    AND OM.numDomainId = @numDomainId
                    
        END
 
    IF @tintMode = 3 -- Get item for SO to PO and vice versa creation
        BEGIN
            DECLARE @tintOppType AS TINYINT
			SELECT @tintOppType=tintOppType FROM OpportunityMaster WHERE numOppId=@numOppID

			IF @tintOppType = 1
			BEGIN
				DECLARE @TempItems TABLE
				(
					ID INT IDENTITY(1,1),numItemCode NUMERIC(18,0),vcItemName VARCHAR(300),txtNotes Varchar(100),charItemType char,vcModelID VARCHAR(100),vcItemDesc TEXT,vcType VARCHAR(20),vcPathForTImage VARCHAR(200),
					numWarehouseItmsID NUMERIC(18,0),vcAttributes VARCHAR(500),bitDropShip BIT,numUnitHour INT,numUOM NUMERIC(18,0),vcUOMName VARCHAR(100),UOMConversionFactor DECIMAL(18,5),
					monPrice MONEY,numVendorID NUMERIC(18,0),numCost decimal(30, 16),vcPartNo VARCHAR(300),monVendorCost MONEY,numSOVendorId NUMERIC(18,0),numClassID NUMERIC(18,0),numProjectID NUMERIC(18,0),tintLevel TINYINT, vcAttrValues VARCHAR(500)
				)

				INSERT INTO 
					@TempItems
				SELECT
					OI.numItemCode,OI.vcItemName,OI.vcNotes as txtNotes,I.charItemType,OI.vcModelID,OI.vcItemDesc,OI.vcType,ISNULL(OI.vcPathForTImage, ''),ISNULL(OI.numWarehouseItmsID,0),ISNULL(OI.vcAttributes, ''),
					ISNULL(OI.bitDropShip, 0),OI.numUnitHour,ISNULL(OI.numUOMId, 0),ISNULL(U.vcUnitName, ''),dbo.fn_UOMConversion(OI.numUOMId, OI.numItemCode,OM.numDomainId, NULL) AS UOMConversionFactor,
					OI.monPrice,ISNULL(I.numVendorID, 0),ISNULL(OI.numCost,0),ISNULL(V.vcPartNo, ''),ISNULL(V.monCost, 0),ISNULL(OI.numSOVendorId, 0),0,0,0,ISNULL(vcAttrValues,'')
				FROM    
					dbo.OpportunityItems OI
					INNER JOIN dbo.OpportunityMaster OM ON OM.numOppId = OI.numOppId
					INNER JOIN dbo.Item I ON I.numItemCode = OI.numItemCode
					LEFT JOIN UOM U ON U.numUOMId = OI.numUOMId
					LEFT JOIN Vendor V ON V.numVendorID = I.numVendorID	AND I.numItemCode = V.numItemCode
				WHERE   
					OI.numOppId = @numOppID
					AND OM.numDomainId = @numDomainId
					AND ISNULL(I.bitKitParent,0) = 0
					AND ISNULL(I.bitAssembly,0) = 0


				--INSERT KIT CHILD ITEMS WHICH ARE NOT KIT AND NOT YET ADDED
				INSERT INTO 
					@TempItems
				SELECT
					OKI.numChildItemID,'' as txtNotes,I.vcItemName,I.charItemType,I.vcModelID,I.txtItemDesc,(CASE WHEN I.charitemType = 'P' THEN 'Product' WHEN I.charitemType = 'S' THEN 'Service' END),
					ISNULL((SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = I.numItemCode AND bitDefault=1),''),ISNULL(OKI.numWareHouseItemId,0),
					ISNULL(dbo.USP_GetAttributes(OKI.numWarehouseItemID, bitSerialized),''),0,OKI.numQtyItemsReq,ISNULL(OKI.numUOMId, 0),ISNULL(U.vcUnitName, ''),
					dbo.fn_UOMConversion(OKI.numUOMId, OKI.numChildItemID,OM.numDomainId, NULL),
					(CASE WHEN I.charitemType = 'P' THEN WI.monWListPrice ELSE I.monListPrice END),
					ISNULL(I.numVendorID, 0),ISNULL(t1.numCost,0),ISNULL(V.vcPartNo, ''),ISNULL(V.monCost, 0),0,0,0,1,''
				FROM    
					dbo.OpportunityKitItems OKI
				LEFT JOIN WarehouseItems WI ON OKI.numWareHouseItemId = WI.numWarehouseItemID
				INNER JOIN dbo.OpportunityMaster OM ON OM.numOppId = OKI.numOppId
				INNER JOIN dbo.Item I ON I.numItemCode = OKI.numChildItemID
				LEFT JOIN UOM U ON U.numUOMId = OKI.numUOMId
				LEFT JOIN Vendor V ON V.numVendorID = I.numVendorID	AND I.numItemCode = V.numItemCode
				LEFT JOIN @TempItems t1 ON OKI.numChildItemID = t1.numItemCode AND ISNULL(OKI.numWareHouseItemId,0) = ISNULL(t1.numWarehouseItmsID,0)
				WHERE   
					OKI.numOppId = @numOppID
					AND OM.numDomainId = @numDomainId
					AND ISNULL(I.bitKitParent,0) = 0
					AND ISNULL(I.bitAssembly,0) = 0
					AND t1.ID IS NULL


				-- FOR KIT CHILD ITEMS WHICH ARE ALREADY ADDED AT PARENT LEVEL - UPDATE QTY ONLY
				UPDATE 
					t2
				SET 
					t2.numUnitHour = ISNULL(t2.numUnitHour,0) + ISNULL(OKI.numQtyItemsReq,0),
					t2.monPrice = (CASE 
									WHEN ISNULL(t2.monPrice,0) < (CASE WHEN I.charitemType = 'P' THEN ISNULL(WI.monWListPrice,0) ELSE ISNULL(I.monListPrice,0) END)  
									THEN t2.monPrice
									ELSE (CASE WHEN I.charitemType = 'P' THEN ISNULL(WI.monWListPrice,0) ELSE ISNULL(I.monListPrice,0) END)
									END)
				FROM
					@TempItems t2
				INNER JOIN
					OpportunityKitItems OKI 
				ON
					OKI.numChildItemID = t2.numItemCode AND ISNULL(OKI.numWareHouseItemId,0) = ISNULL(t2.numWarehouseItmsID,0)
				LEFT JOIN WarehouseItems WI ON OKI.numWareHouseItemId = WI.numWarehouseItemID
				INNER JOIN dbo.OpportunityMaster OM ON OM.numOppId = OKI.numOppId
				INNER JOIN dbo.Item I ON I.numItemCode = OKI.numChildItemID
				LEFT JOIN UOM U ON U.numUOMId = OKI.numUOMId
				LEFT JOIN Vendor V ON V.numVendorID = I.numVendorID	AND I.numItemCode = V.numItemCode
				WHERE   
					OKI.numOppId = @numOppID
					AND OM.numDomainId = @numDomainId
					AND ISNULL(I.bitKitParent,0) = 0
					AND ISNULL(I.bitAssembly,0) = 0
					AND t2.tintLevel <> 1

				--INSERT SUB KIT CHILD ITEMS WHICH ARE NOT KIT AND NOT YET ADDED
				INSERT INTO 
					@TempItems
				SELECT
					OKCI.numItemID,'' as txtNotes,I.vcItemName,I.charItemType,I.vcModelID,I.txtItemDesc,(CASE WHEN I.charitemType = 'P' THEN 'Product' WHEN I.charitemType = 'S' THEN 'Service' END),
					ISNULL((SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = I.numItemCode AND bitDefault=1),''),ISNULL(OKCI.numWareHouseItemId,0),
					ISNULL(dbo.USP_GetAttributes(OKCI.numWarehouseItemID, bitSerialized),''),0,OKCI.numQtyItemsReq,ISNULL(OKCI.numUOMId, 0),ISNULL(U.vcUnitName, ''),
					dbo.fn_UOMConversion(OKCI.numUOMId, OKCI.numItemID,OM.numDomainId, NULL),
					(CASE WHEN I.charitemType = 'P' THEN WI.monWListPrice ELSE I.monListPrice END),
					ISNULL(I.numVendorID, 0),ISNULL(t1.numCost,0),ISNULL(V.vcPartNo, ''),ISNULL(V.monCost, 0),0,0,0,2,''
				FROM    
					dbo.OpportunityKitChildItems OKCI
				LEFT JOIN WarehouseItems WI ON OKCI.numWareHouseItemId = WI.numWarehouseItemID
				INNER JOIN dbo.OpportunityMaster OM ON OM.numOppId = OKCI.numOppId
				INNER JOIN dbo.Item I ON I.numItemCode = OKCI.numItemID
				LEFT JOIN UOM U ON U.numUOMId = OKCI.numUOMId
				LEFT JOIN Vendor V ON V.numVendorID = I.numVendorID	AND I.numItemCode = V.numItemCode
				LEFT JOIN @TempItems t1 ON OKCI.numItemID = t1.numItemCode AND ISNULL(OKCI.numWareHouseItemId,0) = ISNULL(t1.numWarehouseItmsID,0)
				WHERE   
					OKCI.numOppId = @numOppID
					AND OM.numDomainId = @numDomainId
					AND ISNULL(I.bitKitParent,0) = 0
					AND ISNULL(I.bitAssembly,0) = 0
					AND t1.ID IS NULL

				-- FOR SUB KIT CHILD ITEMS WHICH ARE ALREADY ADDED AT PARENT LEVEL - UPDATE QTY ONLY
				UPDATE 
					t2
				SET 
					t2.numUnitHour = ISNULL(t2.numUnitHour,0) + ISNULL(OKCI.numQtyItemsReq,0),
					t2.monPrice = (CASE 
									WHEN ISNULL(t2.monPrice,0) < (CASE WHEN I.charitemType = 'P' THEN ISNULL(WI.monWListPrice,0) ELSE ISNULL(I.monListPrice,0) END)  
									THEN t2.monPrice
									ELSE (CASE WHEN I.charitemType = 'P' THEN ISNULL(WI.monWListPrice,0) ELSE ISNULL(I.monListPrice,0) END)
									END)
				FROM
					@TempItems t2
				INNER JOIN
					OpportunityKitChildItems OKCI 
				ON
					OKCI.numItemID = t2.numItemCode AND ISNULL(OKCI.numWareHouseItemId,0) = ISNULL(t2.numWarehouseItmsID,0)
				LEFT JOIN WarehouseItems WI ON OKCI.numWareHouseItemId = WI.numWarehouseItemID
				INNER JOIN dbo.OpportunityMaster OM ON OM.numOppId = OKCI.numOppId
				INNER JOIN dbo.Item I ON I.numItemCode = OKCI.numItemID
				LEFT JOIN UOM U ON U.numUOMId = OKCI.numUOMId
				LEFT JOIN Vendor V ON V.numVendorID = I.numVendorID	AND I.numItemCode = V.numItemCode
				WHERE   
					OKCI.numOppId = @numOppID
					AND OM.numDomainId = @numDomainId
					AND ISNULL(I.bitKitParent,0) = 0
					AND ISNULL(I.bitAssembly,0) = 0
					AND t2.tintLevel <> 2

				SELECT * FROM @TempItems
			END
			ELSE
			BEGIN
				SELECT  OI.numoppitemtCode,
                    OI.numOppId,
					OI.vcNotes as txtNotes,
					OI.bitItemPriceApprovalRequired,
                    OI.vcItemDesc,
                    ISNULL(OI.vcPathForTImage, '') vcPathForTImage,
                    ISNULL(OI.numWarehouseItmsID, 0) numWarehouseItmsID,
                    OI.vcItemName,
                    OI.vcModelID,
					ISNULL(OI.numCost,0) numCost,
                    ISNULL(OI.vcAttributes, '') vcAttributes,
                    OI.vcType,
                    ISNULL(OI.bitDropShip, 0) bitDropShip,
                    OI.numItemCode,
                    OI.numUnitHour numOriginalUnitHour,
                    ISNULL(OI.numUOMId, 0) AS numUOM,
                    ISNULL(U.vcUnitName, '') vcUOMName,
                    dbo.fn_UOMConversion(OI.numUOMId, OI.numItemCode,
                                         OM.numDomainId, NULL) AS UOMConversionFactor,
                    ISNULL(I.numVendorID, 0) numVendorID,
                    ISNULL(V.monCost, 0) monVendorCost,
                    ISNULL(V.vcPartNo, '') vcPartNo,
                    OI.monPrice,
                    ISNULL(OI.numSOVendorId, 0) numSOVendorId,
                    ISNULL(I.charItemType, 'N') charItemType,
					CAST(( dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), OI.numItemCode,
                                    OM.numDomainId, ISNULL(OI.numUOMId, 0))
						* OI.numUnitHour ) AS NUMERIC(18, 2)) AS numUnitHour,
					isnull(OI.numClassID,0) as numClassID,isnull(OI.numProjectID,0) as numProjectID,ISNULL(I.fltWeight,0) AS [fltWeight],ISNULL(I.bitFreeShipping,0) AS [bitFreeShipping],
					ISNULL(I.fltLength,0) AS [fltLength],
					ISNULL(I.fltWidth,0) AS [fltWidth],
					ISNULL(I.fltHeight,0) AS [fltHeight],
					ISNULL((SELECT vcWHSKU FROM dbo.WareHouseItems WHERE numWarehouseItemID = OI.numWarehouseItmsID ),ISNULL(I.vcSKU,'')) AS [vcSKU],
					ISNULL(vcAttrValues,'') AS vcAttrValues
				FROM    dbo.OpportunityItems OI
						INNER JOIN dbo.OpportunityMaster OM ON OM.numOppId = OI.numOppId
						INNER JOIN dbo.Item I ON I.numItemCode = OI.numItemCode
						LEFT JOIN UOM U ON U.numUOMId = OI.numUOMId
						LEFT JOIN Vendor V ON V.numVendorID = I.numVendorID
												AND I.numItemCode = V.numItemCode
				WHERE   OI.numOppId = @numOppID
						AND OM.numDomainId = @numDomainId
			END

            
					--AND 1 = (CASE 
					--			WHEN @tintOppType = 1 
					--			THEN (CASE WHEN ISNULL(I.bitKitParent,0) = 0 THEN 1 ELSE 0 END)
					--			ELSE 1
					--		END)
        END
        IF @tintMode = 4 -- Add Edit Order items
        BEGIN 
            SELECT  OI.numoppitemtCode,
                    OI.numOppId,
					OI.vcNotes as txtNotes,
                    OI.numItemCode,
					OI.bitItemPriceApprovalRequired,
					OI.numUnitHour AS numOrigUnitHour,
                    OI.numUnitHour,
                    OI.monPrice,
                    OI.monTotAmount,
                    OI.numSourceID,
                    OI.vcItemDesc,
					OI.vcNotes,
					ISNULL(OI.numCost,0) numCost,
                    OI.numWarehouseItmsID,
                    OI.vcType ItemType,
                    OI.vcAttributes Attributes,
                    OI.bitDropShip DropShip,
                    OI.numUnitHourReceived,
                    OI.bitDiscountType,
                    OI.fltDiscount,
                    OI.monTotAmtBefDiscount,
					ISNULL(OI.monTotAmtBefDiscount,0) - ISNULL(OI.monTotAmount,0) AS TotalDiscountAmount,
                    OI.numQtyShipped,
                    OI.vcItemName,
                    OI.vcModelID,
                    OI.vcPathForTImage,
                    OI.vcManufacturer,
                    OI.monVendorCost,
                    ISNULL(OI.numUOMId, 0) AS numUOM,
                    ISNULL(U.vcUnitName, '') vcUOMName,
                    ISNULL(dbo.fn_UOMConversion(OI.numUOMId, OI.numItemCode,
                                         OM.numDomainId, NULL),1) AS UOMConversionFactor,
                    OI.bitWorkOrder,
                    isnull(OI.numVendorWareHouse,0) as numVendorWareHouse,
                    OI.numShipmentMethod,
                    OI.numSOVendorId,
                    OI.numProjectStageID,
                    OI.numToWarehouseItemID,
                   UPPER(I.charItemType) charItemType,
                    WItems.numWarehouseID,
                    W.vcWareHouse AS Warehouse,
                    0 as Op_Flag,
					CAST(( dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), OI.numItemCode,
                                    OM.numDomainId, ISNULL(OI.numUOMId, 0))
						* OI.numUnitHour ) AS NUMERIC(18, 2)) AS numUnitHour,
					isnull(OI.numClassID,0) as numClassID,isnull(OI.numProjectID,0) as numProjectID,
					CASE WHEN (SELECT COUNT(*) FROM OpportunityBizDocs OB JOIN dbo.OpportunityBizDocItems OBI ON ob.numOppBizDocsId=obi.numOppBizDocID
					AND (ob.bitAuthoritativeBizDocs=1 OR ob.numBizDocId=304) AND obi.numOppItemID=oi.numoppitemtCode AND ob.numOppId=OM.numOppId)>0 THEN 1 ELSE 0 END  AS bitIsAuthBizDoc,
					ISNULL(OI.numUnitHourReceived,0) AS numUnitHourReceived,ISNULL(numQtyShipped,0) AS numQtyShipped,dbo.fn_GetUOMName(I.numBaseUnit) AS vcBaseUOMName,ISNULL(I.fltWeight,0) AS [fltWeight],ISNULL(I.bitFreeShipping,0) AS [bitFreeShipping],
					ISNULL(I.fltLength,0) AS [fltLength],
					ISNULL(I.fltWidth,0) AS [fltWidth],
					ISNULL(I.fltHeight,0) AS [fltHeight],
					ISNULL(WItems.vcWHSKU,ISNULL(I.vcSKU,'')) AS [vcSKU],
					(CASE WHEN ISNULL(I.bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(I.monAverageCost,0) END) AS monAverageCost,
					ISNULL(I.bitFreeShipping,0) AS [bitFreeShipping],
					ISNULL(I.txtItemDesc,'') txtItemDesc,
					ISNULL(I.numBarCodeId,'') numBarCodeId,
					ISNULL(I.bitSerialized,0) bitSerialized,
					ISNULL(I.bitKitParent,0) bitKitParent,
					ISNULL(I.bitAssembly,0) bitAssembly,
					ISNULL(I.IsArchieve,0) IsArchieve,
					ISNULL(I.bitLotNo,0) bitLotNo,
					ISNULL(WItems.monWListPrice,0) monWListPrice,
					ISNULL(I.bitTaxable, 0) bitTaxable,
					ISNULL(I.numItemClassification,0) AS numItemClassification,
					CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numOnHand),0) AS VARCHAR(10)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numOnHand,
					CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numOnOrder),0) AS VARCHAR(10)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numOnOrder,
					CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numAllocation),0) AS VARCHAR(10)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numAllocation,
					CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numBackOrder),0) AS VARCHAR(10)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numBackOrder,
					case when ISNULL(I.bitAssembly,0)=1 then dbo.fn_GetAssemblyPossibleWOQty(I.numItemCode) else 0 end AS numMaxWorkOrderQty,
					ISNULL((SELECT COUNT(*) FROM OppWarehouseSerializedItem WHERE numOppId=@numOppID AND numOppItemID=OI.numoppitemtCode),0) AS numSerialNoAssigned,
					(CASE WHEN (SELECT COUNT(*) FROM OpportunityBizDocs OB JOIN dbo.OpportunityBizDocItems OBI ON OB.numOppBizDocsId=OBI.numOppBizDocID WHERE OB.numOppId = OI.numOppId AND OBI.numOppItemID=OI.numoppitemtCode AND OB.numBizDocId=296) > 0 THEN 1 ELSE 0 END) AS bitAddedFulFillmentBizDoc,
					ISNULL(OI.vcAttrValues,'') AS AttributeIDs,
					ISNULL(I.numContainer,0) AS numContainer
					,ISNULL(I.numNoItemIntoContainer,0) AS numContainerQty
					,ISNULL(numPromotionID,0) AS numPromotionID,
					ISNULL(bitPromotionTriggered,0) AS bitPromotionTriggered,
					ISNULL(vcPromotionDetail,'') AS vcPromotionDetail,
					CASE WHEN ISNULL(OI.numSortOrder,0)=0 THEN row_number() OVER (ORDER BY OI.numOppId) ELSE ISNULL(OI.numSortOrder,0) END AS numSortOrder
--					0 AS [Tax0],0 AS bitTaxable0
            FROM    dbo.OpportunityItems OI
                    INNER JOIN dbo.OpportunityMaster OM ON OM.numOppId = OI.numOppId
                    INNER JOIN dbo.Item I ON I.numItemCode = OI.numItemCode
                    LEFT JOIN UOM U ON U.numUOMId = OI.numUOMId
                    LEFT JOIN WareHouseItems WItems ON WItems.numWareHouseItemID = OI.numWarehouseItmsID
                    LEFT JOIN Warehouses W ON W.numWarehouseID = WItems.numWarehouseID
            WHERE   OI.numOppId = @numOppID
                    AND OM.numDomainId = @numDomainId
			ORDER BY OI.numSortOrder
                    
			SELECT  
				vcSerialNo,
				vcComments AS Comments,
				numOppItemID AS numoppitemtCode,
				W.numWarehouseItmsDTLID,
				numWarehouseItmsID AS numWItmsID,
				dbo.Fn_getattributes(W.numWarehouseItmsDTLID, 1) Attributes
			FROM    
				WareHouseItmsDTL W
			JOIN 
				OppWarehouseSerializedItem O 
			ON 
				O.numWarehouseItmsDTLID = W.numWareHouseItmsDTLID
			WHERE   
				numOppID = @numOppID         
        END
        
        
		IF @tintMode = 5 -- Get WebApi Order Items to enter WebApi Order line Item Details 
		BEGIN 
			SELECT OI.numOppItemtCode,OI.numOppId,OI.numItemCode,OI.vcNotes as txtNotes,ISNULL(OI.numCost,0) numCost,
			ISNULL((SELECT vcWHSKU FROM dbo.WareHouseItems WHERE WareHouseItems.numWarehouseItemID = OI.numWarehouseItmsID),ISNULL(IT.vcSKU,'')) AS [vcSKU],
			OI.vcItemDesc,OMAPI.vcAPIOppId
			FROM OpportunityItems OI 
			INNER JOIN dbo.OpportunityMaster OM ON OI.numOppId = OM.numOppId 
			INNER JOIN dbo.OpportunityMasterAPI OMAPI ON OM.numOppId = OMAPI.numOppId
			INNER JOIN dbo.Item IT ON OI.numItemCode = IT.numItemCode
			WHERE OI.numOppId = @numOppID and OM.numDomainID = @numDomainId 
        END

	IF @tintMode = 6 -- Add Edit Order items
        BEGIN 
            SELECT  OI.numoppitemtCode,
                    OI.numOppId,
					OI.vcNotes as txtNotes,
                    OI.numItemCode,
					OI.bitItemPriceApprovalRequired,
					OI.numUnitHour AS numOrigUnitHour,
                    (OI.numUnitHour-ISNULL((SELECT SUM(numUnitHour) FROM ReturnItems where 
					numReturnHeaderID IN (SELECT numReturnHeaderID FROM ReturnHeader 
					WHERE numOppId=@numOppID) AND numOppItemID=OI.numoppitemtCode),0))
					AS numUnitHour,
                    OI.monPrice,
                    OI.monTotAmount,
                    OI.numSourceID,
                    OI.vcItemDesc,
					OI.numCost,
                    OI.numWarehouseItmsID,
                    OI.vcType ItemType,
                    OI.vcAttributes Attributes,
                    OI.bitDropShip DropShip,
                    OI.numUnitHourReceived,
                    OI.bitDiscountType,
                    OI.fltDiscount,
                    OI.monTotAmtBefDiscount,
                    OI.numQtyShipped,
                    OI.vcItemName,
                    OI.vcModelID,
                    OI.vcPathForTImage,
                    OI.vcManufacturer,
                    OI.monVendorCost,
                    ISNULL(OI.numUOMId, 0) AS numUOM,
                    ISNULL(U.vcUnitName, '') vcUOMName,
                    ISNULL(dbo.fn_UOMConversion(OI.numUOMId, OI.numItemCode,
                                         OM.numDomainId, NULL),1) AS UOMConversionFactor,
                    OI.bitWorkOrder,
                    isnull(OI.numVendorWareHouse,0) as numVendorWareHouse,
                    OI.numShipmentMethod,
                    OI.numSOVendorId,
                    OI.numProjectID,
                    OI.numClassID,
                    OI.numProjectStageID,
                    OI.numToWarehouseItemID,
                   UPPER(I.charItemType) charItemType,
                    WItems.numWarehouseID,
                    W.vcWareHouse AS Warehouse,
                    0 as Op_Flag,
					CAST(( dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), OI.numItemCode,
                                    OM.numDomainId, ISNULL(OI.numUOMId, 0))
						* OI.numUnitHour ) AS NUMERIC(18, 2)) AS numUnitHour,
					isnull(OI.numClassID,0) as numClassID,isnull(OI.numProjectID,0) as numProjectID,
					CASE WHEN (SELECT COUNT(*) FROM OpportunityBizDocs OB JOIN dbo.OpportunityBizDocItems OBI ON ob.numOppBizDocsId=obi.numOppBizDocID
					AND (ob.bitAuthoritativeBizDocs=1 OR ob.numBizDocId=304) AND obi.numOppItemID=oi.numoppitemtCode AND ob.numOppId=OM.numOppId)>0 THEN 1 ELSE 0 END  AS bitIsAuthBizDoc,
					ISNULL(OI.numUnitHourReceived,0) AS numUnitHourReceived,ISNULL(numQtyShipped,0) AS numQtyShipped,dbo.fn_GetUOMName(I.numBaseUnit) AS vcBaseUOMName,ISNULL(I.fltWeight,0) AS [fltWeight],ISNULL(I.bitFreeShipping,0) AS [bitFreeShipping],
					ISNULL(I.fltLength,0) AS [fltLength],
					ISNULL(I.fltWidth,0) AS [fltWidth],
					ISNULL(I.fltHeight,0) AS [fltHeight],
					ISNULL(WItems.vcWHSKU,ISNULL(I.vcSKU,'')) AS [vcSKU],
					(CASE WHEN ISNULL(I.bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(I.monAverageCost,0) END) AS monAverageCost,
					ISNULL(I.bitFreeShipping,0) AS [bitFreeShipping],
					ISNULL(I.txtItemDesc,'') txtItemDesc,
					ISNULL(I.vcModelID,'') vcModelID,
					ISNULL(I.vcManufacturer,'') vcManufacturer,
					ISNULL(I.numBarCodeId,'') numBarCodeId,
					ISNULL(I.bitSerialized,0) bitSerialized,
					ISNULL(I.bitKitParent,0) bitKitParent,
					ISNULL(I.bitAssembly,0) bitAssembly,
					ISNULL(I.IsArchieve,0) IsArchieve,
					ISNULL(I.bitLotNo,0) bitLotNo,
					ISNULL(WItems.monWListPrice,0) monWListPrice,
					ISNULL(I.bitTaxable, 0) bitTaxable,
					CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numOnHand),0) AS VARCHAR(10)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numOnHand,
					CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numOnOrder),0) AS VARCHAR(10)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numOnOrder,
					CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numAllocation),0) AS VARCHAR(10)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numAllocation,
					CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numBackOrder),0) AS VARCHAR(10)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numBackOrder,
					case when ISNULL(I.bitAssembly,0)=1 then dbo.fn_GetAssemblyPossibleWOQty(I.numItemCode) else 0 end AS numMaxWorkOrderQty,
					ISNULL((SELECT COUNT(*) FROM OppWarehouseSerializedItem WHERE numOppId=@numOppID AND numOppItemID=OI.numoppitemtCode),0) AS numSerialNoAssigned,
					(CASE WHEN (SELECT COUNT(*) FROM OpportunityBizDocs OB JOIN dbo.OpportunityBizDocItems OBI ON OB.numOppBizDocsId=OBI.numOppBizDocID WHERE OB.numOppId = OI.numOppId AND OBI.numOppItemID=OI.numoppitemtCode AND OB.numBizDocId=296) > 0 THEN 1 ELSE 0 END) AS bitAddedFulFillmentBizDoc,
					ISNULL(OI.vcAttrValues,'') AS AttributeIDs
--					0 AS [Tax0],0 AS bitTaxable0
            FROM    dbo.OpportunityItems OI
                    INNER JOIN dbo.OpportunityMaster OM ON OM.numOppId = OI.numOppId
                    INNER JOIN dbo.Item I ON I.numItemCode = OI.numItemCode
                    LEFT JOIN UOM U ON U.numUOMId = OI.numUOMId
                    LEFT JOIN WareHouseItems WItems ON WItems.numWareHouseItemID = OI.numWarehouseItmsID
                    LEFT JOIN Warehouses W ON W.numWarehouseID = WItems.numWarehouseID
            WHERE   OI.numOppId = @numOppID
                    AND OM.numDomainId = @numDomainId
					AND (OI.numUnitHour-ISNULL((SELECT SUM(numUnitHour) FROM ReturnItems where 
					numReturnHeaderID IN (SELECT numReturnHeaderID FROM ReturnHeader 
					WHERE numOppId=@numOppID) AND numOppItemID=OI.numoppitemtCode),0))>0
                    
		SELECT  vcSerialNo,
				vcComments AS Comments,
				numOppItemID AS numoppitemtCode,
				W.numWarehouseItmsDTLID,
				numWarehouseItmsID AS numWItmsID,
				dbo.Fn_getattributes(W.numWarehouseItmsDTLID, 1) Attributes
		FROM    WareHouseItmsDTL W
				JOIN OppWarehouseSerializedItem O ON O.numWarehouseItmsDTLID = W.numWareHouseItmsDTLID
		WHERE   numOppID = @numOppID  
					              
                   
END
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Created By Anoop Jayaraj                                                              
--- Modified By Gangadhar 03/05/2008    
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'usp_getprospectslist1' ) 
    DROP PROCEDURE usp_getprospectslist1
GO
CREATE PROCEDURE [dbo].[USP_GetProspectsList1]
    @CRMType NUMERIC,
    @numUserCntID NUMERIC,
    @tintUserRightType TINYINT,
    @tintSortOrder NUMERIC = 4,
    @numDomainID AS NUMERIC(9) = 0,
    @SortChar CHAR(1) = '0',
    @CurrentPage INT,
    @PageSize INT,
    @columnName AS VARCHAR(50),
    @columnSortOrder AS VARCHAR(10),
    @numProfile AS NUMERIC,
    @bitPartner AS BIT,
    @ClientTimeZoneOffset AS INT,
	@bitActiveInActive as bit,
	@vcRegularSearchCriteria varchar(MAX)='',
	@vcCustomSearchCriteria varchar(MAX)='' ,
	@SearchText VARCHAR(100) = '',
	 @TotRecs int output 
AS 
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	DECLARE @tintPerformanceFilter AS TINYINT

	IF PATINDEX('%cmp.vcPerformance=1%', @vcRegularSearchCriteria)>0 --Last 3 Months
	BEGIN
		SET @tintPerformanceFilter = 1
	END
	ELSE IF PATINDEX('%cmp.vcPerformance=2%', @vcRegularSearchCriteria)>0 --Last 6 Months
	BEGIN
		SET @tintPerformanceFilter = 2
	END
	ELSE IF PATINDEX('%cmp.vcPerformance=3%', @vcRegularSearchCriteria)>0 --Last 1 Year
	BEGIN
		SET @tintPerformanceFilter = 3
	END
	ELSE
	BEGIN
		SET @tintPerformanceFilter = 0
	END

	IF CHARINDEX('AD.numBillCountry',@vcRegularSearchCriteria) > 0
	BEGIN
		SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'AD.numBillCountry','AD1.numCountry')
	END
	ELSE IF CHARINDEX('AD.numShipCountry',@vcRegularSearchCriteria) > 0
	BEGIN
		SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'AD.numShipCountry','AD2.numCountry')
	END

	CREATE TABLE #tempForm 
	(
		tintOrder TINYINT,vcDbColumnName nvarchar(50),vcOrigDbColumnName nvarchar(50),vcFieldName nvarchar(50),vcAssociatedControlType nvarchar(50),vcListItemType varchar(3),
		numListID numeric(9),vcLookBackTableName varchar(50),bitCustomField BIT,numFieldId NUMERIC,bitAllowSorting BIT,bitAllowEdit BIT,bitIsRequired BIT,bitIsEmail BIT,
		bitIsAlphaNumeric BIT,bitIsNumeric BIT,bitIsLengthValidation BIT,intMaxLength int,intMinLength int,bitFieldMessage BIT,vcFieldMessage VARCHAR(500),ListRelID numeric(9),
		intColumnWidth INT,bitAllowFiltering BIT,vcFieldDataType CHAR(1)
	)
    
	DECLARE @Nocolumns AS TINYINT = 0
    
	SELECT 
		@Nocolumns=ISNULL(SUM(TotalRow),0)  
	FROM
	(            
		SELECT 
			COUNT(*) TotalRow 
		FROM 
			View_DynamicColumns 
		WHERE 
			numFormId=35 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND numRelCntType=3 
		UNION 
		SELECT 
			COUNT(*) TotalRow 
		FROM 
			View_DynamicCustomColumns 
		WHERE 
			numFormId=35 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND numRelCntType=3
	) TotalRows


	--Check if Master Form Configuration is created by administrator if NoColumns=0
	DECLARE @IsMasterConfAvailable BIT = 0
	DECLARE @numUserGroup NUMERIC(18,0)

	IF @Nocolumns=0
	BEGIN
		SELECT @numUserGroup = numGroupID FROM UserMaster WHERE numUserDetailId = @numUserCntID 

		IF (SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = @numDomainID AND numFormID = 35 AND numRelCntType=3 AND bitGridConfiguration = 1 AND numGroupID = @numUserGroup) > 0
		BEGIN
			SET @IsMasterConfAvailable = 1
		END
	END

	--If MasterConfiguration is available then load it otherwise load default columns
	IF @IsMasterConfAvailable = 1
	BEGIN
		INSERT INTO #tempForm
		SELECT 
			(intRowNum + 1) as tintOrder, vcDbColumnName,	vcOrigDbColumnName,	ISNULL(vcCultureFieldName,vcFieldName), vcAssociatedControlType,
			vcListItemType, numListID, vcLookBackTableName,	bitCustom, numFieldId, bitAllowSorting,	bitInlineEdit, bitIsRequired,
			bitIsEmail,	bitIsAlphaNumeric, bitIsNumeric, bitIsLengthValidation,	intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,
			ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType 
		FROM 
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId=35 AND 
			View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicColumnsMasterConfig.numRelCntType=3 AND 
			View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
			ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
		UNION
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName, vcAssociatedControlType,'' as vcListItemType,numListID,'',
			bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,''
		FROM 
			View_DynamicCustomColumnsMasterConfig
		WHERE 
			View_DynamicCustomColumnsMasterConfig.numFormId=35 AND 
			View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicCustomColumnsMasterConfig.numRelCntType=3 AND 
			View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1
		ORDER BY 
			tintOrder ASC  
	END
	ELSE                                        
	BEGIN
		IF @Nocolumns=0
		BEGIN
			INSERT INTO DycFormConfigurationDetails (numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth)
			select 35,numFieldId,0,Row_number() over(order by tintRow desc),@numDomainID,@numUserCntID,3,1,0,intColumnWidth
			 FROM View_DynamicDefaultColumns
			 where numFormId=35 and bitDefault=1 AND ISNULL(bitSettingField,0)=1 and numDomainID=@numDomainID
			order by tintOrder asc   
		END    
       
		INSERT INTO 
			#tempForm
        SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcOrigDbColumnName,ISNULL(vcCultureFieldName,vcFieldName),vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName,
			bitCustom,numFieldId,bitAllowSorting,bitInlineEdit,bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength,
			bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID ,intColumnWidth,bitAllowFiltering,vcFieldDataType
		FROM 
			View_DynamicColumns 
        WHERE 
			numFormId = 35
			AND numUserCntID = @numUserCntID
			AND numDomainID = @numDomainID
			AND tintPageType = 1
			AND numRelCntType = 3
			AND ISNULL(bitSettingField, 0) = 1
			AND ISNULL(bitCustom, 0) = 0
        UNION
        SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName,vcAssociatedControlType,'' as vcListItemType,numListID,'',
			bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength,
			bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,''
		FROM 
			View_DynamicCustomColumns
        WHERE 
			numFormId = 35
            AND numUserCntID = @numUserCntID
            AND numDomainID = @numDomainID
            AND tintPageType = 1
            AND numRelCntType = 3
            AND ISNULL(bitCustom, 0) = 1
        ORDER BY 
			tintOrder ASC  
	END  

		

	DECLARE  @strColumns AS VARCHAR(MAX)
	SET @strColumns=''

	 SET @strColumns =' ISNULL(ADC.numContactId,0) numContactId, ISNULL(DM.numDivisionID,0) numDivisionID, ISNULL(DM.numTerID,0)numTerID, ISNULL(ADC.numRecOwner,0) numRecOwner, ISNULL(DM.tintCRMType,0) AS tintCRMType, CMP.vcCompanyName '              

	DECLARE @tintOrder AS TINYINT                                                      
    DECLARE @vcFieldName AS VARCHAR(50)                                                      
    DECLARE @vcListItemType AS VARCHAR(3)                                                 
    DECLARE @vcAssociatedControlType VARCHAR(20)                                                      
    DECLARE @numListID AS NUMERIC(9)                                                      
    DECLARE @vcDbColumnName VARCHAR(20)                          
    DECLARE @WhereCondition VARCHAR(MAX)                           
    DECLARE @vcLookBackTableName VARCHAR(2000)                    
    DECLARE @bitCustom AS BIT
    DECLARE @bitAllowEdit AS CHAR(1)                 
    DECLARE @numFieldId AS NUMERIC  
    DECLARE @bitAllowSorting AS CHAR(1)              
    DECLARE @vcColumnName AS VARCHAR(500)  
    SET @tintOrder = 0                                                      
    SET @WhereCondition = ''
	Declare @ListRelID as numeric(9) 
	DECLARE @SearchQuery AS VARCHAR(MAX) = '' 

	SELECT TOP 1
            @tintOrder = tintOrder + 1,
            @vcDbColumnName = vcDbColumnName,
            @vcFieldName = vcFieldName,
            @vcAssociatedControlType = vcAssociatedControlType,
            @vcListItemType = vcListItemType,
            @numListID = numListID,
            @vcLookBackTableName = vcLookBackTableName,
            @bitCustom = bitCustomField,
            @numFieldId = numFieldId,
            @bitAllowSorting = bitAllowSorting,
            @bitAllowEdit = bitAllowEdit,@ListRelID=ListRelID
    FROM    #tempForm
    ORDER BY tintOrder ASC            
 
                                               
    WHILE @tintOrder > 0  
    BEGIN
            IF @bitCustom = 0 
                BEGIN            
                    DECLARE @Prefix AS VARCHAR(5)                        
                    IF @vcLookBackTableName = 'AdditionalContactsInformation' 
                        SET @Prefix = 'ADC.'                        
                    IF @vcLookBackTableName = 'DivisionMaster' 
                        SET @Prefix = 'DM.'  

					SET @vcColumnName= @vcDbColumnName+'~'+ convert(varchar(10),@numFieldId) +'~0'

					IF @vcAssociatedControlType='Label' and @vcDbColumnName='vcPerformance'
					BEGIN
						SET @WhereCondition = @WhereCondition
														+ CONCAT(' OUTER APPLY(SELECT
																			SUM(monDealAmount) AS monDealAmount
																			,AVG(ProfitPer) AS fltProfitPer
																		FROM
																		(
																			SELECT
																				OM.numDivisionId
																				,monDealAmount
																				,AVG(((ISNULL(OI.monPrice,0) - (CASE D.numCost WHEN 3 THEN ISNULL(V.monCost,0) * dbo.fn_UOMConversion(I.numBaseUnit,I.numItemCode,I.numDomainID,I.numPurchaseUnit) WHEN 2 THEN ISNULL(OI.numCost,0) ELSE ISNULL(I.monAverageCost,0) END)) / ISNULL(NULLIF(OI.monPrice,0),1)) * 100) ProfitPer
																			FROM
																				OpportunityMaster OM
																			INNER JOIN
																				Domain D
																			ON
																				OM.numDomainID = D.numDomainID
																			INNER JOIN
																				OpportunityItems OI
																			ON
																				OM.numOppId = OI.numOppId
																			INNER JOIN
																				Item I
																			ON
																				OI.numItemCode = I.numItemCode
																			LEFT JOIN
																				Vendor V
																			ON
																				V.numVendorID = I.numVendorID
																				AND V.numItemCode = I.numItemCode
																			WHERE
																				OM.numDomainId = ',@numDomainID,'
																				AND OM.numDivisionId = DM.numDivisionID
																				AND ISNULL(I.bitContainer,0) = 0
																				AND tintOppType = 1
																				AND tintOppStatus=1
																				AND 1 = (CASE ', @tintPerformanceFilter,' 
																						WHEN 1 THEN (CASE WHEN OM.bintCreatedDate BETWEEN DATEADD(MONTH,-3,GETUTCDATE()) AND GETUTCDATE() THEN 1 ELSE 0 END)
																						WHEN 2 THEN (CASE WHEN OM.bintCreatedDate BETWEEN DATEADD(MONTH,-6,GETUTCDATE()) AND GETUTCDATE() THEN 1 ELSE 0 END)
																						WHEN 3 THEN (CASE WHEN OM.bintCreatedDate BETWEEN DATEADD(YEAR,-1,GETUTCDATE()) AND GETUTCDATE() THEN 1 ELSE 0 END)
																						ELSE 1
																						END)
																			GROUP BY
																				OM.numDivisionId
																				,OM.numOppId
																				,monDealAmount
																		) T1
																		GROUP BY
																			T1.numDivisionId ) AS TempPerformance ')

						set @strColumns=@strColumns +', (CASE WHEN ISNULL(TempPerformance.monDealAmount,0) > 0 THEN CONCAT(FORMAT(TempPerformance.monDealAmount,''##,####.00''),'' ('',CAST(TempPerformance.fltProfitPer AS DECIMAL(18,2)),''%)'') ELSE '''' END) ['+ @vcColumnName+']'
					END	
                    ELSE IF @vcAssociatedControlType = 'SelectBox' or @vcAssociatedControlType='ListBox'     
                        BEGIN                                                          
                            IF @vcDbColumnName = 'numPartenerSource'
							BEGIN
								SET @strColumns=@strColumns+ ' ,(DM.numPartenerSource) '+' ['+ @vcColumnName+']'    
								SET @strColumns=@strColumns+ ' ,(SELECT D.vcPartnerCode+''-''+C.vcCompanyName FROM DivisionMaster AS D LEFT JOIN CompanyInfo AS C ON D.numCompanyID=C.numCompanyID WHERE D.numDivisionID=DM.numPartenerSource) AS vcPartenerSource' 
							END
							ELSE IF @vcListItemType = 'LI' 
                                BEGIN    
                                    IF @numListID = 40 
                                        BEGIN
                                            SET @strColumns = @strColumns + ',L' + CONVERT(VARCHAR(3), @tintOrder) + '.vcData' + ' [' + @vcColumnName + ']'                                                   
                                            
											IF LEN(@SearchText) > 0
											BEGIN 
											SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'L' + CONVERT(VARCHAR(3),@tintOrder) + '.vcData LIKE ''%' + @SearchText + '%'' '     
											END   

											IF @vcDbColumnName='numShipCountry'
											BEGIN
												SET @WhereCondition = @WhereCondition
													+ ' left Join AddressDetails AD2 on AD2.numRecordId= DM.numDivisionID and AD2.tintAddressOf=2 and AD2.tintAddressType=2 AND AD2.bitIsPrimary=1 and AD2.numDomainID= DM.numDomainID '
													+ ' left Join ListDetails L' + CONVERT(VARCHAR(3), @tintOrder) + ' on L' + CONVERT(VARCHAR(3), @tintOrder) + '.numListItemID=AD2.numCountry '
											END
											ELSE
											BEGIN
												SET @WhereCondition = @WhereCondition
													+ ' left Join AddressDetails AD1 on AD1.numRecordId= DM.numDivisionID and AD1.tintAddressOf=2 and AD1.tintAddressType=1 AND AD1.bitIsPrimary=1 and AD1.numDomainID= DM.numDomainID '
													+ ' left Join ListDetails L' + CONVERT(VARCHAR(3), @tintOrder) + ' on L' + CONVERT(VARCHAR(3), @tintOrder) + '.numListItemID=AD1.numCountry '
											END
                                        END
                                    ELSE 
                                        BEGIN                                                      
                                            SET @strColumns = @strColumns + ',L'
                                                + CONVERT(VARCHAR(3), @tintOrder)
                                                + '.vcData' + ' ['
                                                + @vcColumnName + ']'           
											
											IF LEN(@SearchText) > 0
											BEGIN 
											SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'L' + CONVERT(VARCHAR(3),@tintOrder) + '.vcData LIKE ''%' + @SearchText + '%'' '     
											END   

											                                              
                                            SET @WhereCondition = @WhereCondition
                                                + ' left Join ListDetails L'
                                                + CONVERT(VARCHAR(3), @tintOrder)
                                                + ' on L'
                                                + CONVERT(VARCHAR(3), @tintOrder)
                                                + '.numListItemID='
                                                + @vcDbColumnName                                                          
                                        END
                                END                                                          
                            ELSE 
                                IF @vcListItemType = 'S' 
                                    BEGIN     
											
											                                                           
											
											IF LEN(@SearchText) > 0
											BEGIN 
											SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'S' + CONVERT(VARCHAR(3),@tintOrder) + '.vcState LIKE ''%' + @SearchText + '%'' '     
											END   

											IF @vcDbColumnName='numShipState'
											BEGIN
												SET @strColumns = @strColumns + ',ShipState.vcState' + ' [' + @vcColumnName + ']' 
												SET @WhereCondition = @WhereCondition
													+ ' left Join AddressDetails AD3 on AD3.numRecordId= DM.numDivisionID and AD3.tintAddressOf=2 and AD3.tintAddressType=2 AND AD3.bitIsPrimary=1 and AD3.numDomainID= DM.numDomainID '
													+ ' left Join State ShipState on ShipState.numStateID=AD3.numState '
											END
											ELSE IF @vcDbColumnName='numBillState'
											BEGIN
												SET @strColumns = @strColumns + ',BillState.vcState' + ' [' + @vcColumnName + ']' 
												SET @WhereCondition = @WhereCondition
													+ ' left Join AddressDetails AD4 on AD4.numRecordId= DM.numDivisionID and AD4.tintAddressOf=2 and AD4.tintAddressType=1 AND AD4.bitIsPrimary=1 and AD4.numDomainID= DM.numDomainID '
													+ ' left Join State BillState on BillState.numStateID=AD4.numState '
											END
											ELSE  
											BEGIN
												SET @strColumns = @strColumns + ',S' + CONVERT(VARCHAR(3), @tintOrder) + '.vcState' + ' [' + @vcColumnName + ']' 
												SET @WhereCondition = @WhereCondition + ' left join State S' + CONVERT(VARCHAR(3), @tintOrder) + ' on S' + CONVERT(VARCHAR(3), @tintOrder) + '.numStateID=' + @vcDbColumnName        
											END
                                                 
                                                                                          
                                    END                                                          
                                ELSE 
                                    IF @vcListItemType = 'T' 
                                        BEGIN                                                          
                                            SET @strColumns = @strColumns + ',L'
                                                + CONVERT(VARCHAR(3), @tintOrder)
                                                + '.vcData' + ' ['
                                                + @vcColumnName + ']'     
												
											IF LEN(@SearchText) > 0
											BEGIN 
											SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'L' + CONVERT(VARCHAR(3),@tintOrder) + '.vcData LIKE ''%' + @SearchText + '%'' '     
											END  
											                                                     
                                            SET @WhereCondition = @WhereCondition
                                                + ' left Join ListDetails L'
                                                + CONVERT(VARCHAR(3), @tintOrder)
                                                + ' on L'
                                                + CONVERT(VARCHAR(3), @tintOrder)
                                                + '.numListItemID='
                                                + @vcDbColumnName                                                          
                                        END
                                    ELSE 
                                        IF @vcListItemType = 'C' 
                                            BEGIN                                                    
                                                SET @strColumns = @strColumns + ',C'
                                                    + CONVERT(VARCHAR(3), @tintOrder)
                                                    + '.vcCampaignName' + ' ['
                                                    + @vcColumnName + ']'    
												
												IF LEN(@SearchText) > 0
												BEGIN 
												SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'C' + CONVERT(VARCHAR(3),@tintOrder) + '.vcCampaignName LIKE ''%' + @SearchText + '%'' '     
												END  
												                                                
                                                SET @WhereCondition = @WhereCondition
                                                    + ' left Join CampaignMaster C'
                                                    + CONVERT(VARCHAR(3), @tintOrder)
                                                    + ' on C'
                                                    + CONVERT(VARCHAR(3), @tintOrder)
                                                    + '.numCampaignId=DM.'
                                                    + @vcDbColumnName                                                    
                                            END                       
                                        ELSE 
                                            IF @vcListItemType = 'U' 
                                                BEGIN                         
                            
                              
                                                    SET @strColumns = @strColumns
                                                        + ',dbo.fn_GetContactName('
                                                        + @Prefix
                                                        + @vcDbColumnName
                                                        + ') ['
                                                        + @vcColumnName + ']'  
														
													IF LEN(@SearchText) > 0
													  BEGIN 
														SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'dbo.fn_GetContactName(' + @Prefix + @vcDbColumnName + ') LIKE ''%' + @SearchText + '%'' '    
													  END       
													                                                        
                                                END                        
                        END       
      
                    ELSE 
                        IF @vcAssociatedControlType = 'DateField' 
                            BEGIN              
                                SET @strColumns = @strColumns
                                    + ',case when convert(varchar(11),DateAdd(minute, '
                                    + CONVERT(VARCHAR(15), -@ClientTimeZoneOffset)
                                    + ',' + @Prefix + @vcDbColumnName
                                    + ') )= convert(varchar(11),getdate()) then ''<b><font color=red>Today</font></b>'''        
                                SET @strColumns = @strColumns
                                    + 'when convert(varchar(11),DateAdd(minute, '
                                    + CONVERT(VARCHAR(15), -@ClientTimeZoneOffset)
                                    + ',' + @Prefix + @vcDbColumnName
                                    + ') )= convert(varchar(11),dateadd(day,-1,getdate())) then ''<b><font color=purple>YesterDay</font></b>'''        
                                SET @strColumns = @strColumns
                                    + 'when convert(varchar(11),DateAdd(minute, '
                                    + CONVERT(VARCHAR(15), -@ClientTimeZoneOffset)
                                    + ',' + @Prefix + @vcDbColumnName
                                    + ') )= convert(varchar(11),dateadd(day,1,getdate())) then''<b><font color=orange>Tommorow</font></b>'' '        
                                SET @strColumns = @strColumns
                                    + 'else dbo.FormatedDateFromDate(DateAdd(minute, '
                                    + CONVERT(VARCHAR(15), -@ClientTimeZoneOffset)
                                    + ',' + @Prefix + @vcDbColumnName + '),'
                                    + CONVERT(VARCHAR(10), @numDomainId)
                                    + ') end  [' + @vcColumnName + ']'    
									
								
								IF LEN(@SearchText) > 0
								BEGIN
									SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + @Prefix + @vcDbColumnName + ' LIKE ''%' + @SearchText + '%'' '
								END
								
								              
                            END      
            
                        ELSE 
                            IF @vcAssociatedControlType = 'TextBox'
                                OR @vcAssociatedControlType = 'Label' 
                                BEGIN      
                                    SET @strColumns = @strColumns + ','
                                        + CASE WHEN @vcDbColumnName = 'numAge'
                                               THEN 'year(getutcdate())-year(bintDOB)'
                                               WHEN @vcDbColumnName = 'numDivisionID'
                                               THEN 'DM.numDivisionID'
                                               ELSE @vcDbColumnName
                                          END + ' [' + @vcColumnName + ']'    
										  
										  
									IF LEN(@SearchText) > 0
									BEGIN
										SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 
										CASE WHEN @vcDbColumnName = 'numAge'
                                               THEN 'year(getutcdate())-year(bintDOB)'
                                               WHEN @vcDbColumnName = 'numDivisionID'
                                               THEN 'DM.numDivisionID'
                                               ELSE @vcDbColumnName
                                          END
										+' LIKE ''%' + @SearchText + '%'' '
									END  		  
										              
         
                                END 
                          else if @vcAssociatedControlType='Popup' and @vcDbColumnName='numShareWith'                                                   
		begin      
			 set @strColumns=@strColumns+',(SELECT SUBSTRING(
	(SELECT '','' + A.vcFirstName+'' ''+A.vcLastName + CASE WHEN ISNULL(SR.numContactType,0)>0 THEN ''('' + dbo.fn_GetListItemName(SR.numContactType) + '')'' ELSE '''' END
	FROM ShareRecord SR JOIN UserMaster UM ON UM.numUserDetailId=SR.numAssignedTo 
							 JOIN AdditionalContactsInformation A ON UM.numUserDetailId=A.numContactID 
		 WHERE SR.numDomainID=DM.numDomainID AND SR.numModuleID=1 AND SR.numRecordID=DM.numDivisionID
				AND UM.numDomainID=DM.numDomainID and UM.numDomainID=A.numDomainID FOR XML PATH('''')),2,200000)) ['+ @vcColumnName+']'                
		 end         
                            ELSE 
                                BEGIN    
                                    SET @strColumns = @strColumns + ', '
                                        + @vcDbColumnName + ' ['
                                        + @vcColumnName + ']'              
       
                                END                                    
                   
                END                  
            ELSE 
                IF @bitCustom = 1 
                    BEGIN                  
      
						SET @vcColumnName= @vcDbColumnName +'~'+ convert(varchar(10),@numFieldId)+'~1'
 
                        SELECT  @vcFieldName = FLd_label,
                                @vcAssociatedControlType = fld_type,
                                @vcDbColumnName = 'Cust'
                                + CONVERT(VARCHAR(10), Fld_Id)
                        FROM    CFW_Fld_Master
                        WHERE   CFW_Fld_Master.Fld_Id = @numFieldId                                 
                        IF @vcAssociatedControlType = 'TextBox' or @vcAssociatedControlType = 'TextArea'   
                            BEGIN                              
                                SET @strColumns = @strColumns + ',CFW'
                                    + CONVERT(VARCHAR(3), @tintOrder)
                                    + '.Fld_Value  [' + @vcColumnName + ']'                     
                                SET @WhereCondition = @WhereCondition
                                    + ' left Join CFW_FLD_Values CFW'
                                    + CONVERT(VARCHAR(3), @tintOrder)
                                    + '                   
    on CFW' + CONVERT(VARCHAR(3), @tintOrder) + '.Fld_Id='
                                    + CONVERT(VARCHAR(10), @numFieldId)
                                    + 'and CFW'
                                    + CONVERT(VARCHAR(3), @tintOrder)
                                    + '.RecId=DM.numDivisionId   '                                                           
                            END    
                        ELSE 
                            IF @vcAssociatedControlType = 'CheckBox'  
                                BEGIN              
set @strColumns= @strColumns+',case when isnull(CFW' + CONVERT(VARCHAR(3), @tintOrder)+ '.Fld_Value,0)='''' then 0 else  isnull(CFW'
                                        + CONVERT(VARCHAR(3), @tintOrder)+ '.Fld_Value,0) end ['+ @vcColumnName + ']'        
 
                                    SET @WhereCondition = @WhereCondition
                                        + ' left Join CFW_FLD_Values CFW'
                                        + CONVERT(VARCHAR(3), @tintOrder)
                                        + '             
    on CFW' + CONVERT(VARCHAR(3), @tintOrder) + '.Fld_Id='
                                        + CONVERT(VARCHAR(10), @numFieldId)
                                        + 'and CFW'
                                        + CONVERT(VARCHAR(3), @tintOrder)
                                        + '.RecId=DM.numDivisionId   '                                                     
                                END       
                            ELSE 
                                IF @vcAssociatedControlType = 'DateField'  
                                    BEGIN                  
                     
                                        SET @strColumns = @strColumns
                                            + ',dbo.FormatedDateFromDate(CFW'
                                            + CONVERT(VARCHAR(3), @tintOrder)
                                            + '.Fld_Value,'
                                            + CONVERT(VARCHAR(10), @numDomainId)
                                            + ')  [' + @vcColumnName + ']'                     
                                        SET @WhereCondition = @WhereCondition
                                            + ' left Join CFW_FLD_Values CFW'
                                            + CONVERT(VARCHAR(3), @tintOrder)
                                            + '                   
    on CFW' + CONVERT(VARCHAR(3), @tintOrder) + '.Fld_Id='
                                            + CONVERT(VARCHAR(10), @numFieldId)
                                            + 'and CFW'
                                            + CONVERT(VARCHAR(3), @tintOrder)
                                            + '.RecId=DM.numDivisionId   '                                                           
                                    END                  
                                ELSE 
                                    IF @vcAssociatedControlType = 'SelectBox'
                                        BEGIN                   
                                            SET @vcDbColumnName = 'DCust'
                                                + CONVERT(VARCHAR(10), @numFieldId)                  
                                            SET @strColumns = @strColumns + ',L'
                                                + CONVERT(VARCHAR(3), @tintOrder)
                                                + '.vcData' + ' ['
                                                + @vcColumnName + ']'                                                            
                                            SET @WhereCondition = @WhereCondition
                                                + ' left Join CFW_FLD_Values CFW'
                                                + CONVERT(VARCHAR(3), @tintOrder)
                                                + '                   
     on CFW' + CONVERT(VARCHAR(3), @tintOrder) + '.Fld_Id='
                                                + CONVERT(VARCHAR(10), @numFieldId)
                                                + 'and CFW'
                                                + CONVERT(VARCHAR(3), @tintOrder)
                                                + '.RecId=DM.numDivisionId    '                                                           
                                            SET @WhereCondition = @WhereCondition
                                                + ' left Join ListDetails L'
                                                + CONVERT(VARCHAR(3), @tintOrder)
                                                + ' on L'
                                                + CONVERT(VARCHAR(3), @tintOrder)
                                                + '.numListItemID=CFW'
                                                + CONVERT(VARCHAR(3), @tintOrder)
                                                + '.Fld_Value'                  
                                        END      
								ELSE IF @vcAssociatedControlType = 'CheckBoxList' 
								BEGIN
									SET @strColumns= @strColumns+',(SELECT STUFF((SELECT CONCAT('', '', vcData) FROM ListDetails WHERE numListID= ' + CAST(@numListID AS varchar) + ' AND numlistitemid IN (SELECT ISNULL(Id,0) FROM dbo.SplitIDs(CFW'+ convert(VARCHAR(3),@tintOrder)+'.Fld_Value,'','')) FOR XML PATH('''')), 1, 1, ''''))  ['+ @vcColumnName +']'   

								SET @WhereCondition= @WhereCondition 
													+' left Join CFW_FLD_Values CFW'+ convert(varchar(3),@tintOrder)
													+ ' on CFW' + convert(varchar(3),@tintOrder) 
													+ '.Fld_Id='+convert(varchar(10),@numFieldId) 
													+ 'and CFW' + convert(varchar(3),@tintOrder) 
													+ '.RecId=DM.numDivisionId '
								END                       
                    END    
                    

		  select top 1 
		   @tintOrder = tintOrder + 1,
            @vcDbColumnName = vcDbColumnName,
            @vcFieldName = vcFieldName,
            @vcAssociatedControlType = vcAssociatedControlType,
            @vcListItemType = vcListItemType,
            @numListID = numListID,
            @vcLookBackTableName = vcLookBackTableName,
            @bitCustom = bitCustomField,
            @numFieldId = numFieldId,
            @bitAllowSorting = bitAllowSorting,
            @bitAllowEdit = bitAllowEdit,@ListRelID=ListRelID
		  from #tempForm WHERE tintOrder > @tintOrder-1 order by tintOrder asc
            IF @@rowcount = 0 
                SET @tintOrder = 0 
    END  

	DECLARE @strShareRedordWith AS VARCHAR(300);
	SET @strShareRedordWith=' DM.numDivisionID in (select SR.numRecordID FROM ShareRecord SR WHERE SR.numDomainID='+ convert(varchar(15),@numDomainID )+' AND SR.numModuleID=1 AND SR.numAssignedTo='+convert(varchar(15),@numUserCntId)+') '

	SET @strColumns=@strColumns+' ,ISNULL(VIE.Total,0) as TotalEmail '
	SET @strColumns=@strColumns+' ,ISNULL(VOA.OpenActionItemCount,0) as TotalActionItem '
   

		DECLARE @StrSql AS VARCHAR(MAX) = ''
		SET @StrSql='	FROM  CompanyInfo CMP                                       
						join DivisionMaster DM on DM.numCompanyID=CMP.numCompanyID
						join AdditionalContactsInformation ADC on ADC.numDivisionID=DM.numDivisionID 
						left join VIEW_OPEN_ACTION_ITEMS VOA WITH (NOEXPAND) ON VOA.numDomainID =  '+ convert(varchar(15),@numDomainID )+' AND  VOA.numDivisionId = DM.numDivisionID 
						left join View_InboxEmail VIE WITH (NOEXPAND) ON VIE.numDomainID = '+ convert(varchar(15),@numDomainID )+' AND  VIE.numContactId = ADC.numContactId '
		IF @tintSortOrder = 9
		BEGIN
			SET @StrSql=@StrSql+ ' join Favorites F on F.numContactid=DM.numDivisionID '
		END 

		SET @StrSql=@StrSql + ISNULL(@WhereCondition,'')

		 -------Change Row Color-------
		Declare @vcCSOrigDbCOlumnName as varchar(50),@vcCSLookBackTableName as varchar(50),@vcCSAssociatedControlType as varchar(50)
		SET @vcCSOrigDbCOlumnName=''
		SET @vcCSLookBackTableName=''

		Create table #tempColorScheme(vcOrigDbCOlumnName varchar(50),vcLookBackTableName varchar(50),vcFieldValue varchar(50),vcFieldValue1 varchar(50),vcColorScheme varchar(50),vcAssociatedControlType varchar(50))

		insert into #tempColorScheme select DFM.vcOrigDbCOlumnName,DFM.vcLookBackTableName,DFCS.vcFieldValue,DFCS.vcFieldValue1,DFCS.vcColorScheme,DFFM.vcAssociatedControlType 
		from DycFieldColorScheme DFCS join DycFormField_Mapping DFFM on DFCS.numFieldID=DFFM.numFieldID
		join DycFieldMaster DFM on DFM.numModuleID=DFFM.numModuleID and DFM.numFieldID=DFFM.numFieldID
		where DFCS.numDomainID=@numDomainID and DFFM.numFormID=35 AND DFCS.numFormID=35 and isnull(DFFM.bitAllowGridColor,0)=1

		IF(SELECT COUNT(*) FROM #tempColorScheme)>0
		BEGIN
		   select top 1 @vcCSOrigDbCOlumnName=vcOrigDbCOlumnName,@vcCSLookBackTableName=vcLookBackTableName,@vcCSAssociatedControlType=vcAssociatedControlType from #tempColorScheme
		END   
		----------------------------                        
 
		 IF len(@vcCSOrigDbCOlumnName)>0 and len(@vcCSLookBackTableName)>0
		BEGIN
		set @strColumns=@strColumns+' ,tCS.vcColorScheme '
		END

		IF len(@vcCSOrigDbCOlumnName)>0 and len(@vcCSLookBackTableName)>0
		BEGIN
			 if @vcCSLookBackTableName = 'AdditionalContactsInformation'                  
				set @Prefix = 'ADC.'                  
			 if @vcCSLookBackTableName = 'DivisionMaster'                  
				set @Prefix = 'DM.'
			 if @vcCSLookBackTableName = 'CompanyInfo'                  
				set @Prefix = 'CMP.'   
 
			IF @vcCSAssociatedControlType='DateField'
			BEGIN
					set @strSql=@strSql+' left join #tempColorScheme tCS on CONVERT(VARCHAR(10),' + @Prefix + @vcCSOrigDbCOlumnName + ',111) >= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue as int),GETUTCDATE()),111)
					 and CONVERT(VARCHAR(10),' + @Prefix + @vcCSOrigDbCOlumnName + ',111) <= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue1 as int),GETUTCDATE()),111)'
			END
			ELSE IF @vcCSAssociatedControlType='SelectBox' OR @vcCSAssociatedControlType='ListBox' OR @vcCSAssociatedControlType='CheckBox'
			BEGIN
				set @strSql=@strSql+' left join #tempColorScheme tCS on tCS.vcFieldValue=' + @Prefix + @vcCSOrigDbCOlumnName
			END
		END

		IF @columnName LIKE 'CFW.Cust%' 
        BEGIN                   
            SET @strSql = @strSql + ' left Join CFW_FLD_Values CFW on CFW.RecId=DM.numDivisionID and CFW.fld_id= ' + REPLACE(@columnName, 'CFW.Cust', '') + ' '                                                           
            SET @columnName = 'CFW.Fld_Value'                  
        END                                           
		ELSE IF @columnName LIKE 'DCust%' 
        BEGIN                                                      
            SET @strSql = @strSql + ' left Join CFW_FLD_Values CFW on CFW.RecId=DM.numDivisionID  and CFW.fld_id= ' + REPLACE(@columnName, 'DCust', '')                                                                                                     
            SET @strSql = @strSql + ' left Join ListDetails LstCF on LstCF.numListItemID=CFW.Fld_Value  '                  
            SET @columnName = 'LstCF.vcData' 
        END        

		IF @columnName = 'CMP.vcPerformance'   
		BEGIN
			SET @columnName = 'TempPerformance.monDealAmount'
		END

		SET @StrSql = @StrSql + ' WHERE (numCompanyType = 0 OR numCompanyType=46) 
									AND ISNULL(ADC.bitPrimaryContact,0) = 1 
									AND CMP.numDomainID=DM.numDomainID   
									AND DM.numDomainID=ADC.numDomainID  
									AND DM.tintCRMType= ' + CONVERT(VARCHAR(2), @CRMType) + ' AND DM.numDomainID = ' + CONVERT(VARCHAR(20),@numDomainID)


		IF @tintUserRightType = 1 
		BEGIN
			SET @strSql = @strSql + CONCAT(' AND (DM.numRecOwner = ',@numUserCntID,' or DM.numAssignedTo=',@numUserCntID,' or ',@strShareRedordWith,')')                                                                     
		END
		ELSE IF @tintUserRightType = 2 
		BEGIN
			SET @strSql = @strSql + CONCAT(' AND (DM.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= ',@numUserCntID,' ) or DM.numAssignedTo=',@numUserCntID,' or ',@strShareRedordWith,')')
		END

		IF LEN(@SearchQuery) > 0
		BEGIN
			SET @StrSql = @StrSql + ' AND (' + @SearchQuery + ') '
		END	

		IF @SortChar <> '0' 
		BEGIN
			SET @strSql = @strSql + ' And CMP.vcCompanyName like ''' + @SortChar + '%'' '                                                               
		END

		IF LEN(@SearchQuery) > 0
		BEGIN
			IF @tintSortOrder = 1 
				SET @strSql = @strSql + ' AND DM.numStatusID=2 '                                                              
			ELSE IF @tintSortOrder = 2 
				SET @strSql = @strSql + ' AND DM.numStatusID=3 '                          
			ELSE IF @tintSortOrder = 3 
                SET @strSql = @strSql + ' AND (DM.numRecOwner = '
                    + CONVERT(VARCHAR(15), @numUserCntID)
                    + ' or DM.numAssignedTo='
                    + CONVERT(VARCHAR(15), @numUserCntID) + ' or ' + @strShareRedordWith +')'                                                                                                                                                     
            ELSE IF @tintSortOrder = 6 
                    SET @strSql = @strSql + ' AND DM.bintCreatedDate > '''
                            + CONVERT(VARCHAR(20), DATEADD(day, -7,
                                                           GETUTCDATE()))
                            + ''''                                                                    
            ELSE IF @tintSortOrder = 7 
                     SET @strSql = @strSql + ' AND DM.numCreatedby=' + CONVERT(VARCHAR(15), @numUserCntID)                                                                              
            ELSE IF @tintSortOrder = 8 
                     SET @strSql = @strSql + ' AND DM.numModifiedby=' + CONVERT(VARCHAR(15), @numUserCntID) 
			 IF @numProfile <> 0 
					SET @strSQl = @strSql + ' AND cmp.vcProfile = '
            + CONVERT(VARCHAR(15), @numProfile) 
		END
		ELSE
		BEGIN
			IF @tintSortOrder = 1 
				SET @strSql = @strSql + ' AND DM.numStatusID=2 '                                                              
			ELSE IF @tintSortOrder = 2 
				SET @strSql = @strSql + ' AND DM.numStatusID=3 '                          
			ELSE IF @tintSortOrder = 3 
                SET @strSql = @strSql + ' AND (DM.numRecOwner = '
                    + CONVERT(VARCHAR(15), @numUserCntID)
                    + ' or DM.numAssignedTo='
                    + CONVERT(VARCHAR(15), @numUserCntID) + ' or ' + @strShareRedordWith +')'                                                                                                                                                   
            ELSE IF @tintSortOrder = 6 
                    SET @strSql = @strSql + ' AND DM.bintCreatedDate > '''
                            + CONVERT(VARCHAR(20), DATEADD(day, -7,
                                                           GETUTCDATE()))
                            + ''''                                                                    
            ELSE IF @tintSortOrder = 7 
                     SET @strSql = @strSql + ' AND DM.numCreatedby=' + CONVERT(VARCHAR(15), @numUserCntID)                                                                              
            ELSE IF @tintSortOrder = 8 
                     SET @strSql = @strSql + ' AND DM.numModifiedby=' + CONVERT(VARCHAR(15), @numUserCntID) 
			 IF @numProfile <> 0 
        SET @strSQl = @strSql + ' AND cmp.vcProfile = '
            + CONVERT(VARCHAR(15), @numProfile) 
		END

		IF @vcRegularSearchCriteria<>'' 
		BEGIN
			IF PATINDEX('%cmp.vcPerformance%', @vcRegularSearchCriteria)>0
			BEGIN
				-- WE ARE MANAGING CONDITION IN OUTER APPLY
				set @strSql=@strSql
			END
			ELSE
				set @strSql=@strSql+' and ' + @vcRegularSearchCriteria 
		END


		IF @vcCustomSearchCriteria <> '' 
			SET @strSql=@strSql + ' AND ' + @vcCustomSearchCriteria
                                            


		
		DECLARE @firstRec AS INTEGER                                                              
		DECLARE @lastRec AS INTEGER                                                              
		SET @firstRec = ( @CurrentPage - 1 ) * @PageSize                                                              
		SET @lastRec = ( @CurrentPage * @PageSize + 1 )     
		
		DECLARE @strTotal NVARCHAR(MAX) = 'SELECT @TotalRecords = COUNT(*) ' + @StrSql
		exec sp_executesql @strTotal, N'@TotalRecords INT OUT', @TotRecs OUT                                                                   
       
		-- EXECUTE FINAL QUERY
		DECLARE @strFinal AS NVARCHAR(MAX)
		SET @strFinal = CONCAT('SELECT ROW_NUMBER() OVER(ORDER BY ',@columnName,' ',@columnSortOrder,') ID, ',@strColumns,' INTO #tempTable',@StrSql,'; SELECT @TotalRecords = COUNT(*) FROM #tempTable; SELECT * FROM #tempTable WHERE ID > ',@firstRec,' and ID <',@lastRec,' ORDER BY ID; DROP TABLE #tempTable;')
	
		PRINT @strFinal
		exec sp_executesql @strFinal, N'@TotalRecords INT OUT', @TotRecs OUT

		SELECT * FROM #tempForm

		DROP TABLE #tempForm

		DROP TABLE #tempColorScheme 
		 
END	
/****** Object:  StoredProcedure [dbo].[usp_InsertNewChartAccountDetails]    Script Date: 09/25/2009 16:15:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Siva                                
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'usp_insertnewchartaccountdetails' ) 
    DROP PROCEDURE usp_insertnewchartaccountdetails
GO
CREATE PROCEDURE [dbo].[usp_InsertNewChartAccountDetails]
    @numAcntTypeId NUMERIC(9) = 0,
    @numParntAcntTypeID NUMERIC(9) = 0,
    @vcAccountName VARCHAR(100),
    @vcAccountDescription VARCHAR(100),
    @monOriginalOpeningBal MONEY,
    @monOpeningBal MONEY,
    @dtOpeningDate DATETIME,
    @bitActive BIT,
    @numAccountId NUMERIC(9) = 0 OUTPUT,
    @bitFixed BIT,
    @numDomainId NUMERIC(9) = 0,
    @numListItemID NUMERIC(9) = 0,
    @numUserCntId NUMERIC(9) = 0,
    @bitProfitLoss BIT,
    @bitDepreciation BIT,
    @dtDepreciationCostDate DATETIME,
    @monDepreciationCost MONEY,
    @vcNumber VARCHAR(50),
    @bitIsSubAccount BIT,
    @numParentAccId NUMERIC(18, 0),
    @IsBankAccount			BIT	=0,
	@vcStartingCheckNumber	VARCHAR(50)=NULL ,
	@vcBankRountingNumber	VARCHAR(50)=NULL ,
	@bitIsConnected				BIT =0,
	@numBankDetailID			NUMERIC(18,0)=NULL

AS 
BEGIN 
	BEGIN TRY
	BEGIN TRANSACTION

    
        DECLARE @numAccountId1 AS NUMERIC(9)                              
        DECLARE @strSQl AS VARCHAR(1000)                             
        DECLARE @strSQLBalance AS VARCHAR(1000)                           
        DECLARE @numJournalId AS NUMERIC(9) 
        DECLARE @numDepositId AS NUMERIC(9)     
        DECLARE @numDepositJournalId AS NUMERIC(9)  
        DECLARE @numCashCreditJournalId AS NUMERIC(9)  
        DECLARE @numCashCreditId AS NUMERIC(9)   
        DECLARE @numAccountIdOpeningEquity AS NUMERIC(9)
        DECLARE @vcAccountCode VARCHAR(50)
        DECLARE @intLevel AS INT
        DECLARE @intFlag INT
        DECLARE @vcAccountNameWithNumber AS VARCHAR(100)
        
    
        SET @strSQl = ''                          
        SET @strSQLBalance = ''   
        
        SET @intFlag = 1 
        IF ISNULL(@vcNumber,'') = '-1' --@vcNumber will have -1 value only when its being called from [USP_ChartAcntDefaultValues], as well cursor below creates time out.
		BEGIN
			SET @intFlag = 0 
			SET @vcNumber= ''
			PRINT @intFlag
		END
		  
        IF ISNULL(@vcNumber, '') = '' 
            SET @vcAccountNameWithNumber = ''	
        ELSE 
            SET @vcAccountNameWithNumber = @vcNumber + ' '   
	
		IF @numAcntTypeId = 0 
		BEGIN
			DECLARE @AccountTypeCode VARCHAR(4)
			(SELECT @AccountTypeCode = SUBSTRING(vcAccountCode ,1,4) FROM dbo.AccountTypeDetail WHERE numAccountTypeID = @numParntAcntTypeID)
			SELECT @numAcntTypeId = numAccountTypeID FROM dbo.AccountTypeDetail WHERE numDomainID=@numDomainID AND vcAccountCode =  @AccountTypeCode
		END
	                             
		--Check for Current Fiancial Year
        IF (SELECT COUNT([numFinYearId]) FROM [FinancialYear] WHERE [numDomainId] = @numDomainId AND [bitCurrentYear] = 1) = 0 
        BEGIN
            RAISERROR ( 'NoCurrentFinancialYearSet', 16, 1 ) ;
            RETURN ;
        END
	
		  
        IF @numAccountId = 0 
        BEGIN 
			IF ISNULL(@vcNumber, '') <> '' 
			BEGIN
				IF EXISTS(SELECT * FROM Chart_Of_Accounts WHERE vcNumber = @vcNumber AND numDomainID =  @numDomainId)	
				BEGIN
					RAISERROR ( 'DuplicateNumber', 16, 1 ) ;
					RETURN ;
				END
			END 
				 
            IF @numParntAcntTypeID = 0 
			BEGIN
                SET @numParntAcntTypeID = (SELECT TOP 1 numAccountId FROM dbo.Chart_Of_Accounts WHERE Chart_Of_Accounts.numParntAcntTypeID IS NULL AND Chart_Of_Accounts.numDomainId = @numDomainId)
			END

            SET @numAccountIdOpeningEquity = (SELECT TOP 1 numAccountId FROM dbo.Chart_Of_Accounts WHERE bitProfitLoss = 1 AND Chart_Of_Accounts.numDomainId = @numDomainId) 
            
         
            IF (ISNULL(@numAccountIdOpeningEquity, 0) = 0 AND @bitProfitLoss = 0)
            BEGIN
                RAISERROR ( 'NoProfitLossACC', 16, 1 ) ;
                RETURN 
            END
			
            SELECT  @vcAccountCode = dbo.GetAccountTypeCode(@numDomainId, @numParntAcntTypeID, 1)
                
            IF (@bitIsSubAccount=1)
			BEGIN
				SELECT  @vcAccountCode = dbo.GetAccountTypeCode(@numDomainId, @numParentAccId, 3)
				SELECT @numParntAcntTypeID = numParntAcntTypeId FROM Chart_Of_Accounts WHERE numDomainId=@numDomainId AND numAccountId = @numParentAccId
			END
                
            SELECT  @intLevel = CASE WHEN @numParentAccId = 0 AND @bitIsSubAccount = 0 THEN 0 ELSE ISNULL(MAX(intLevel),0) + 1 END  FROM dbo.Chart_Of_Accounts WHERE numAccountId = @numParentAccId 
            PRINT @intLevel
									   
--			PRINT @vcAccountCode
            INSERT  INTO dbo.Chart_Of_Accounts
            (
                numAcntTypeId,
                numParntAcntTypeID,
                vcAccountCode,
                vcAccountName,
                vcAccountDescription,
                numOriginalOpeningBal,
                numOpeningBal,
                dtOpeningDate,
                bitActive,
                bitFixed,
                numDomainId,
                numListItemID,
                bitProfitLoss,
                [bitDepreciation],
                [dtDepreciationCostDate],
                [monDepreciationCost],
                vcNumber,
                bitIsSubAccount,
                numParentAccId,
                intLevel,
                IsBankAccount,
                vcStartingCheckNumber,
                vcBankRountingNumber,
                IsConnected,
                numBankDetailID
            )
            VALUES  
			(
                @numAcntTypeId,
                @numParntAcntTypeID,
                @vcAccountCode,
                @vcAccountNameWithNumber + @vcAccountName,
                @vcAccountDescription,
                @monOriginalOpeningBal,
                @monOpeningBal,
                @dtOpeningDate,
                @bitActive,
                @bitFixed,
                @numDomainId,
                @numListItemID,
                @bitProfitLoss,
                @bitDepreciation,
                @dtDepreciationCostDate,
                @monDepreciationCost,
                @vcNumber,
                @bitIsSubAccount,
                @numParentAccId,
                @intLevel,
                @IsBankAccount,
                @vcStartingCheckNumber,
                @vcBankRountingNumber,
                @bitIsConnected,
                @numBankDetailID
            )
                
				
			SET @numAccountId1 = @@IDENTITY                                                  
            SET @numAccountId = @numAccountId1 --used at bottom
            SELECT @numAccountId1                            
				
			
			
			--Added By Chintan
            EXEC USP_ManageChartAccountOpening @numDomainId, @numAccountId1, @monOriginalOpeningBal, @dtOpeningDate                                    
        END
        ELSE IF @numAccountId <> 0 
        BEGIN
            IF EXISTS(SELECT * FROM Chart_Of_Accounts WHERE numAccountId = @numAccountId AND numDomainId = @numDomainId AND (numAcntTypeId != @numAcntTypeId OR numParntAcntTypeId != @numParntAcntTypeID))
			BEGIN
				IF NOT ((SELECT COUNT(*) FROM AccountTypeDetail WHERE numAccountTypeID=@numParntAcntTypeID AND vcAccountcode LIKE '0106%' AND numDomainId = @numDomainId )=1
					AND (SELECT COUNT(*) FROM AccountTypeDetail WHERE numAccountTypeID=(SELECT numParntAcntTypeId FROM Chart_Of_Accounts WHERE numAccountId = @numAccountId AND numDomainId = @numDomainId) AND vcAccountcode LIKE '0104%' AND numDomainId = @numDomainId)=1)
				BEGIN
					IF EXISTS (SELECT GJD.numJournalId FROM dbo.General_Journal_Details GJD WHERE numDomainId = @numDomainId AND numChartAcntId=@numAccountId)
					BEGIN
						RAISERROR ( 'JournalEntryExists', 16, 1 ) ;
						RETURN ;
					END
				END
			END
					
			IF ISNULL(@vcNumber, '') <> '' 
			BEGIN
				IF EXISTS(SELECT * FROM Chart_Of_Accounts WHERE vcNumber = @vcNumber AND numDomainID =  @numDomainId AND numAccountId <> @numAccountId)	
				BEGIN
					RAISERROR ( 'DuplicateNumber', 16, 1 ) ;
					RETURN ;
				END
			END 

            IF @numParntAcntTypeID = 0 
			BEGIN
                SET @numParntAcntTypeID = (SELECT Chart_Of_Accounts.numAccountId FROM dbo.Chart_Of_Accounts WHERE Chart_Of_Accounts.numParntAcntTypeID IS NULL AND Chart_Of_Accounts.numDomainId = @numDomainId) 
                            
                DECLARE @OldOriginalOpeningBalance AS MONEY 
                DECLARE @OldOpeningBalance AS MONEY 
                DECLARE @NewOpeningBalance AS MONEY
                DECLARE @OldParentAcntTypeID AS NUMERIC
				DECLARE @OldIsSubAccount AS NUMERIC
					
                SELECT  @OldOriginalOpeningBalance = numOriginalOpeningBal,
                        @OldOpeningBalance = numOpeningBal,
                        @OldParentAcntTypeID = numParntAcntTypeId,
                        @vcAccountCode = vcAccountCode,
                        @OldIsSubAccount = ISNULL(bitIsSubAccount,0)
                FROM    dbo.Chart_Of_Accounts
                WHERE   numAccountId = @numAccountID
                        AND numDomainId = @numDomainID
			
                IF ( @OldParentAcntTypeID != @numParntAcntTypeID ) --bug fix 1439
                BEGIN
                    SELECT  @vcAccountCode = dbo.GetAccountTypeCode(@numDomainId, @numParntAcntTypeID, 1)
                    SET @numParentAccId = 0
                    SET @bitIsSubAccount = 0
                END
                        
				IF (@bitIsSubAccount=1)
				BEGIN
					SELECT  @vcAccountCode = dbo.GetAccountTypeCode(@numDomainId, @numParentAccId, 3)
					SELECT @numParntAcntTypeID = numParntAcntTypeId FROM Chart_Of_Accounts WHERE numDomainId=@numDomainId AND numAccountId = @numParentAccId
				END
			
                UPDATE  
					Chart_Of_Accounts
                SET     
					numAcntTypeId = @numAcntTypeId,
                    numParntAcntTypeId = @numParntAcntTypeID,
                    vcAccountName = @vcAccountNameWithNumber
                    + @vcAccountName,
                    vcAccountDescription = @vcAccountDescription,
                    bitActive = @bitActive,
                    bitProfitLoss = @bitProfitLoss,
                    [numOriginalOpeningBal] = @monOriginalOpeningBal,
                    [dtOpeningDate] = @dtOpeningDate,
                    [bitDepreciation] = @bitDepreciation,
                    [dtDepreciationCostDate] = @dtDepreciationCostDate,
                    [monDepreciationCost] = @monDepreciationCost,
                    vcAccountCode = @vcAccountCode,
                    bitIsSubAccount = @bitIsSubAccount,
                    vcNumber = @vcNumber,
                    numParentAccId = @numParentAccId,
					IsBankAccount = @IsBankAccount,
					vcStartingCheckNumber = @vcStartingCheckNumber,
					vcBankRountingNumber = @vcBankRountingNumber,
					IsConnected = @bitIsConnected,
					numBankDetailID = @numBankDetailID
                WHERE   
					(numAccountId = @numAccountId) 
					AND ( numDomainId = @numDomainId )
			
			--UPDATE dbo.Chart_Of_Accounts SET intLevel = 0 WHERE numAccountId = @numAccountId
                UPDATE  
					dbo.Chart_Of_Accounts
                SET 
					intLevel = CASE 
								WHEN numParentAccId = 0 AND bitIsSubAccount = 0 THEN 0
								ELSE ( SELECT ISNULL(intLevel,0) + 1 FROM dbo.Chart_Of_Accounts WHERE numAccountId = @numParentAccId )
								END
                WHERE (bitIsSubAccount = 1 OR bitIsSubAccount = 0) AND numAccountId = @numAccountId

                EXEC USP_ManageChartAccountOpening @numDomainId,@numAccountId, @monOriginalOpeningBal, @dtOpeningDate                                                                      
            END 
		END
    
    
		--Maintain Sorting by Account code when adding new account with sub account  or  updating Account numeber
		PRINT @intFlag
		if (@intFlag =1) -- @vcNumber will have -1 value only when its being called from [USP_ChartAcntDefaultValues], as well cursor below creates time out.
		BEGIN
			UPDATE dbo.Chart_Of_Accounts SET vcAccountCode='' WHERE numParntAcntTypeId = @numParntAcntTypeID  AND numDomainId=@numDomainID


			DECLARE @TEMP TABLE
			(
				ID INT IDENTITY(1,1)
				,numAccountId NUMERIC(18,0)
				,bitIsSubAccount BIT
				,numParentAccId NUMERIC(18,0)
			)

			;WITH CTE(tintLevel, numAccountId, vcAccountName, bitIsSubAccount, numParentAccId) AS
			(
				SELECT 
					0
					,numAccountId
					,vcAccountName
					,bitIsSubAccount
					,numParentAccId
				FROM 
					dbo.Chart_Of_Accounts 
				WHERE 
					numParntAcntTypeId = @numParntAcntTypeID 
					AND ISNULL(numParentAccId,0) = 0
				UNION ALL
				SELECT 
					c.tintLevel + 1
					,COA.numAccountId
					,COA.vcAccountName
					,COA.bitIsSubAccount
					,COA.numParentAccId
				FROM
					dbo.Chart_Of_Accounts  COA
				INNER JOIN
					CTE c
				ON
					COA.numParentAccId=C.numAccountId
				WHERE 
					numParntAcntTypeId = @numParntAcntTypeID 
			)

			INSERT INTO @TEMP
			(
				numAccountId
				,bitIsSubAccount
				,numParentAccId
			)
			SELECT
				numAccountId
				,bitIsSubAccount
				,numParentAccId
			FROM 
				CTE 
			ORDER BY 
				tintLevel,vcAccountName



			Declare @numTempAccountID NUMERIC
			Declare @bitTempIsSubAccount bit
			Declare @numTempParentAccId NUMERIC
			DECLARE @vcTempAccountCode VARCHAR(100)


			DECLARE @i AS INT  = 1
			DECLARE @iCount AS INT

			SELECT @iCount=COUNT(*) FROM @TEMP

			WHILE @i <= @iCount
			BEGIN
				SELECT @numTempAccountID=numAccountId,@bitTempIsSubAccount=bitIsSubAccount,@numTempParentAccId=numParentAccId  FROM @TEMP WHERE ID=@i
				

				IF @bitTempIsSubAccount =1 
					SELECT @vcTempAccountCode = dbo.GetAccountTypeCode(@numDomainId, @numTempParentAccId, 3)
				ELSE
					SELECT @vcTempAccountCode = dbo.GetAccountTypeCode(@numDomainId, @numParntAcntTypeID, 1)


				UPDATE dbo.Chart_Of_Accounts SET vcAccountCode=@vcTempAccountCode WHERE numAccountId=@numTempAccountID

				SET @i = @i + 1
			END
		END

		--Maintain only one profit and loss account through out system
		IF @bitProfitLoss = 1 
        BEGIN
            UPDATE  dbo.Chart_Of_Accounts
            SET     bitProfitLoss = 0
            WHERE   numAccountId <> @numAccountId
                    AND numDomainId = @numDomainID
        END
	COMMIT
	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000)
		DECLARE @ErrorNumber INT
		DECLARE @ErrorSeverity INT
		DECLARE @ErrorState INT
		DECLARE @ErrorLine INT
		DECLARE @ErrorProcedure NVARCHAR(200)

		IF @@TRANCOUNT > 0
			ROLLBACK TRANSACTION;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorNumber = ERROR_NUMBER(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE(),
			@ErrorLine = ERROR_LINE(),
			@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

		RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
	END CATCH
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_Item_GetFromAttributeIds')
DROP PROCEDURE USP_Item_GetFromAttributeIds
GO
CREATE PROCEDURE [dbo].[USP_Item_GetFromAttributeIds]
(
	@numDomainID NUMERIC(18,0)
	,@numItemCode AS NUMERIC(18,0)=0    
	,@vcAttributeIDs VARCHAR(500)    
)
AS
BEGIN  
	DECLARE @vcItemName VARCHAR(300) = ''
	DECLARE @numItemGroup NUMERIC(18,0)
	SELECT @vcItemName=ISNULL(vcItemName,''),@numItemGroup=ISNULL(numItemGroup,0) FROM Item WHERE numItemCode=@numItemCode AND numDomainID=@numDomainID


	SELECT TOP 1
		numItemCode
		,(SELECT TOP 1 numWarehouseItemID FROM WareHouseItems WHERE numDomainID=@numDomainID AND numItemID=Item.numItemCode AND numWLocationID <> -1) numWarehouseItemID
	FROM
		Item
	WHERE
		numDomainID=@numDomainID
		AND vcItemName = @vcItemName
		AND bitMatrix=1
		AND numItemGroup=@numItemGroup
		AND Stuff((SELECT N',' + CONCAT(FLD_ID,':',FLD_Value) FROM ItemAttributes WHERE numDomainID=@numDomainID AND numItemCode=Item.numItemCode FOR XML PATH(''),TYPE).value('text()[1]','nvarchar(max)'),1,1,N'') = @vcAttributeIDs
END
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_Item_SearchForSalesOrder' )
    DROP PROCEDURE USP_Item_SearchForSalesOrder
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Sandeep Patel
-- Create date: 3 March 2014
-- Description:	Search items for sales order
-- =============================================
CREATE PROCEDURE USP_Item_SearchForSalesOrder
    @tintOppType AS TINYINT ,
    @numDomainID AS NUMERIC(9) ,
    @numDivisionID AS NUMERIC(9) = 0 ,
    @str AS VARCHAR(1000) ,
    @numUserCntID AS NUMERIC(9) ,
    @tintSearchOrderCustomerHistory AS TINYINT = 0 ,
    @numPageIndex AS INT ,
    @numPageSize AS INT ,
    @WarehouseID AS NUMERIC(18, 0) = NULL ,
    @TotalCount AS INT OUTPUT
AS
    BEGIN
  SET NOCOUNT ON;

DECLARE @strNewQuery NVARCHAR(MAX)
DECLARE @strSQL AS NVARCHAR(MAX)
SELECT  @str = REPLACE(@str, '''', '''''')

DECLARE @TableRowCount TABLE ( Value INT );
	
SELECT  *
INTO    #Temp1
FROM    ( SELECT    numFieldId ,
                    vcDbColumnName ,
                    ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                    tintRow AS tintOrder ,
                    0 AS Custom
          FROM      View_DynamicColumns
          WHERE     numFormId = 22
                    AND numUserCntID = @numUserCntID
                    AND numDomainID = @numDomainID
                    AND tintPageType = 1
                    AND bitCustom = 0
                    AND ISNULL(bitSettingField, 0) = 1
                    AND numRelCntType = 0
          UNION
          SELECT    numFieldId ,
                    vcFieldName ,
                    vcFieldName ,
                    tintRow AS tintOrder ,
                    1 AS Custom
          FROM      View_DynamicCustomColumns
          WHERE     Grp_id = 5
                    AND numFormId = 22
                    AND numUserCntID = @numUserCntID
                    AND numDomainID = @numDomainID
                    AND tintPageType = 1
                    AND bitCustom = 1
                    AND numRelCntType = 0
        ) X 
  
IF NOT EXISTS ( SELECT  *
                FROM    #Temp1 )
    BEGIN
        INSERT  INTO #Temp1
                SELECT  numFieldId ,
                        vcDbColumnName ,
                        ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                        tintorder ,
                        0
                FROM    View_DynamicDefaultColumns
                WHERE   numFormId = 22
                        AND bitDefault = 1
                        AND ISNULL(bitSettingField, 0) = 1
                        AND numDomainID = @numDomainID 
    END

CREATE TABLE #tempItemCode ( numItemCode NUMERIC(9) )
DECLARE @bitRemoveVendorPOValidation AS BIT
SELECT  
	@bitRemoveVendorPOValidation = ISNULL(bitRemoveVendorPOValidation,0)
FROM    
	domain
WHERE  
	numDomainID = @numDomainID

IF @tintOppType > 0
    BEGIN 
        IF CHARINDEX(',', @str) > 0
        BEGIN
			DECLARE @itemSearchText VARCHAR(100)
            DECLARE @vcAttribureSearch VARCHAR(MAX)
			DECLARE @vcAttribureSearchCondition VARCHAR(MAX)

			SET @itemSearchText = LTRIM(RTRIM(SUBSTRING(@str,0,CHARINDEX(',',@str))))

			--GENERATES ATTRIBUTE SEARCH CONDITION
			SET @vcAttribureSearch = SUBSTRING(@str,CHARINDEX(',',@str) + 1,LEN(@str))
	
			DECLARE @attributeSearchText VARCHAR(MAX)
			DECLARE @TempTable TABLE (vcValue VARCHAR(MAX))

			WHILE LEN(@vcAttribureSearch) > 0
			BEGIN
				SET @attributeSearchText = LEFT(@vcAttribureSearch, 
										ISNULL(NULLIF(CHARINDEX(',', @vcAttribureSearch) - 1, -1),
										LEN(@vcAttribureSearch)))
				SET @vcAttribureSearch = SUBSTRING(@vcAttribureSearch,
												ISNULL(NULLIF(CHARINDEX(',', @vcAttribureSearch), 0),
												LEN(@vcAttribureSearch)) + 1, LEN(@vcAttribureSearch))

				INSERT INTO @TempTable (vcValue) VALUES ( @attributeSearchText )
			END
		
			--REMOVES WHITE SPACES
			UPDATE
				@TempTable
			SET
				vcValue = LTRIM(RTRIM(vcValue))
	
			--CONVERTING TABLE TO COMMA SEPERATED STRING AFTER REMOVEING WHITE SPACES
			SELECT @vcAttribureSearch = COALESCE(@vcAttribureSearch,'') +  CONVERT(VARCHAR(MAX),vcValue) + ',' FROM @TempTable
	
			--REMOVE LAST COMMA FROM FINAL SEARCH STRING
			SET @vcAttribureSearch = LTRIM(RTRIM(@vcAttribureSearch))
			IF DATALENGTH(@vcAttribureSearch) > 0
				SET @vcAttribureSearch = LEFT(@vcAttribureSearch, LEN(@vcAttribureSearch) - 1)
	
			--START: GENERATES ATTRIBUTE SEARCH CONDITION
			SELECT 	@vcAttribureSearchCondition = '(TEMPMATRIX.Attributes LIKE ''%' + REPLACE(@vcAttribureSearch, ',', '%'' AND TEMPMATRIX.Attributes LIKE ''%') + '%'')' 

			--LOGIC FOR GENERATING FINAL SQL STRING
			IF @tintOppType = 1 OR ( @bitRemoveVendorPOValidation = 1 AND @tintOppType = 2)
                BEGIN      

				SET @strSQL = 'SELECT
									I.numItemCode,
									I.numVendorID,
									MIN(WHT.numWareHouseItemID) as numWareHouseItemID,
									dbo.fn_GetAttributes(WHT.numWareHouseItemId,I.bitSerialized) AS [Attributes]
								FROM
									dbo.Item I WITH (NOLOCK)
								LEFT JOIN
									dbo.WareHouseItems WHT
								ON
									I.numItemCode = WHT.numItemID
								WHERE 
									((ISNULL(I.numItemGroup,0) > 0 AND I.vcItemName LIKE ''%' + @itemSearchText + '%'') OR (ISNULL(I.numItemGroup,0) = 0 AND I.vcItemName LIKE ''%' + @str + '%''))
									AND ISNULL(I.Isarchieve, 0) <> 1
									AND I.charItemType NOT IN ( ''A'' )
									AND I.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + '
									AND (WHT.numWareHouseID = ' + CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20)) + ' OR ' + CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20)) + ' = 0)'

				--- added Asset validation  by sojan
                IF @bitRemoveVendorPOValidation = 1
                    AND @tintOppType = 2
                    SET @strSQL = @strSQL
                        + ' and ISNULL(I.bitKitParent,0) = 0 AND ISNULL(I.bitAssembly,0) = 0 '

                IF @tintSearchOrderCustomerHistory = 1 AND @numDivisionID > 0
                BEGIN
                    SET @strSQL = @strSQL
                        + ' and I.numItemCode in (select distinct oppI.numItemCode from 
						OpportunityMaster oppM join OpportunityItems oppI on oppM.numOppId=oppI.numOppId where oppM.numDomainID='
                        + CONVERT(VARCHAR(20), @numDomainID)
                        + ' and oppM.numDivisionId='
                        + CONVERT(VARCHAR(20), @numDivisionId)
                        + ')'
                END

				SET @strSQL = @strSQL + ' GROUP BY
										I.numItemCode,
										I.numVendorID,
										dbo.fn_GetAttributes(numWareHouseItemId,I.bitSerialized)'

				SET @strSQL = 
				'
				SELECT    
					SRNO=ROW_NUMBER() OVER( ORDER BY  I.vcItemName),
					I.numItemCode,
					(SELECT TOP 1 vcPathForImage FROM ItemImages WHERE numItemCode = I.numItemCode) AS vcPathForImage ,
					(SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = I.numItemCode) AS vcPathForTImage ,
					ISNULL(vcItemName, '''') vcItemName ,
					CASE WHEN I.[charItemType] = ''P''
					THEN CONVERT(VARCHAR(100), ISNULL((SELECT TOP 1 ROUND(monWListPrice,2) FROM [WareHouseItems] WHERE [numItemID] = I.numItemCode), 0))
					ELSE CONVERT(VARCHAR(100), ROUND(monListPrice, 2))
					END AS monListPrice,
					ISNULL(vcSKU, '''') vcSKU ,
					ISNULL(numBarCodeId, 0) numBarCodeId ,
					ISNULL(vcModelID, '''') vcModelID ,
					ISNULL(txtItemDesc, '''') AS txtItemDesc ,
					ISNULL(C.vcCompanyName, '''') AS vcCompanyName,
					TEMPMATRIX.numWareHouseItemID,
					TEMPMATRIX.Attributes AS [vcAttributes],
					(CASE 
						WHEN ISNULL(I.bitKitParent,0) = 1 
						THEN 
							(
								CASE
									WHEN (SELECT COUNT(*) FROM ItemDetails INNER JOIN Item ON ItemDetails.numChildItemID = Item.numItemCode WHERE numItemKitID=I.numItemCode AND ISNULL(Item.bitKitParent,0) = 1) > 0
									THEN 1
									ELSE 0
								END
							)
						ELSE 0 
					END) bitHasKitAsChild
				FROM  
					(' + @strSQL +') AS TEMPMATRIX
				INNER JOIN    
					Item I
				ON
					TEMPMATRIX.numItemCode = I.numItemCode
				LEFT JOIN 
					DivisionMaster D 
				ON 
					I.numVendorID = D.numDivisionID
				LEFT JOIN 
					CompanyInfo C 
				ON 
					C.numCompanyID = D.numCompanyID
				WHERE  
				  ((ISNULL(I.numItemGroup,0) = 0 AND I.vcItemName LIKE ''%' + @str + '%'') OR ' + @vcAttribureSearchCondition + ')'

				

                INSERT  INTO @TableRowCount
                        EXEC
                            ( 'SELECT COUNT(*) FROM ( ' + @strSQL
                                + ') as t2'
                                    );

                SELECT  @TotalCount = Value
                FROM    @TableRowCount;

                SET @strNewQuery = 'select * from (' + @strSQL
                    + ') as t where SRNO> '
                    + CAST(( ( @numPageIndex - 1 ) * @numPageSize ) AS VARCHAR(10))
                    + ' and SRNO < '
                    + CAST(( ( @numPageIndex * @numPageSize ) + 1 ) AS VARCHAR(10))
                    + ' order by  vcItemName'

				--PRINT @strNewQuery
                EXEC(@strNewQuery)
				END
			ELSE
				BEGIN

				SET @strSQL = 'SELECT
									I.numItemCode,
									I.numVendorID,
									MIN(WHT.numWareHouseItemID) as numWareHouseItemID,
									dbo.fn_GetAttributes(WHT.numWareHouseItemId,I.bitSerialized) AS [Attributes]
								FROM
									dbo.Item I WITH (NOLOCK)
								INNER JOIN
									dbo.Vendor V
								ON
									V.numItemCode = I.numItemCode
								LEFT JOIN
									dbo.WareHouseItems WHT
								ON
									I.numItemCode = WHT.numItemID
								WHERE 
									((ISNULL(I.numItemGroup,0) > 0 AND I.vcItemName LIKE ''%' + @itemSearchText + '%'') OR (ISNULL(I.numItemGroup,0) = 0 AND I.vcItemName LIKE ''%' + @str + '%''))
									AND ISNULL(I.Isarchieve, 0) <> 1
									AND I.charItemType NOT IN ( ''A'' )
									AND I.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + '
									AND (WHT.numWareHouseID = ' + CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20)) + ' OR ' + CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20)) + ' = 0)'

				--- added Asset validation  by sojan
                IF @tintSearchOrderCustomerHistory = 1 AND @numDivisionID > 0
                BEGIN
                    SET @strSQL = @strSQL
                        + ' and I.numItemCode in (select distinct oppI.numItemCode from 
						OpportunityMaster oppM join OpportunityItems oppI on oppM.numOppId=oppI.numOppId where oppM.numDomainID='
                        + CONVERT(VARCHAR(20), @numDomainID)
                        + ' and oppM.numDivisionId='
                        + CONVERT(VARCHAR(20), @numDivisionId)
                        + ')'
                END

				SET @strSQL = @strSQL + ' GROUP BY
										I.numItemCode,
										I.numVendorID,
										dbo.fn_GetAttributes(numWareHouseItemId,I.bitSerialized)'

				SET @strSQL = 
				'
				SELECT    
					SRNO=ROW_NUMBER() OVER( ORDER BY  I.vcItemName),
					I.numItemCode,
					(SELECT TOP 1 vcPathForImage FROM ItemImages WHERE numItemCode = I.numItemCode) AS vcPathForImage ,
					(SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = I.numItemCode) AS vcPathForTImage ,
					ISNULL(vcItemName, '''') vcItemName ,
					CASE WHEN I.[charItemType] = ''P''
					THEN CONVERT(VARCHAR(100), ISNULL((SELECT TOP 1 ROUND(monWListPrice,2) FROM [WareHouseItems] WHERE [numItemID] = I.numItemCode), 0))
					ELSE CONVERT(VARCHAR(100), ROUND(monListPrice, 2))
					END AS monListPrice,
					ISNULL(vcSKU, '''') vcSKU ,
					ISNULL(numBarCodeId, 0) numBarCodeId ,
					ISNULL(vcModelID, '''') vcModelID ,
					ISNULL(txtItemDesc, '''') AS txtItemDesc ,
					ISNULL(C.vcCompanyName, '''') AS vcCompanyName,
					TEMPMATRIX.numWareHouseItemID,
					TEMPMATRIX.Attributes AS [vcAttributes],
					(CASE 
						WHEN ISNULL(I.bitKitParent,0) = 1 
						THEN 
							(
								CASE
									WHEN (SELECT COUNT(*) FROM ItemDetails INNER JOIN Item ON ItemDetails.numChildItemID = Item.numItemCode WHERE numItemKitID=I.numItemCode AND ISNULL(Item.bitKitParent,0) = 1) > 0
									THEN 1
									ELSE 0
								END
							)
						ELSE 0 
					END) bitHasKitAsChild
				FROM  
					(' + @strSQL +') AS TEMPMATRIX
				INNER JOIN    
					Item I
				ON
					TEMPMATRIX.numItemCode = I.numItemCode
				LEFT JOIN 
					DivisionMaster D 
				ON 
					I.numVendorID = D.numDivisionID
				LEFT JOIN 
					CompanyInfo C 
				ON 
					C.numCompanyID = D.numCompanyID
				WHERE  
				 ((ISNULL(I.numItemGroup,0) = 0 AND I.vcItemName LIKE ''%' + @str + '%'') OR ' + @vcAttribureSearchCondition + ')'

                INSERT  INTO @TableRowCount
                        EXEC
                            ( 'SELECT COUNT(*) FROM ( ' + @strSQL
                                + ') as t2'
                                    );

                SELECT  @TotalCount = Value
                FROM    @TableRowCount;

                SET @strNewQuery = 'select * from (' + @strSQL
                    + ') as t where SRNO> '
                    + CAST(( ( @numPageIndex - 1 ) * @numPageSize ) AS VARCHAR(10))
                    + ' and SRNO < '
                    + CAST(( ( @numPageIndex * @numPageSize ) + 1 ) AS VARCHAR(10))
                    + ' order by  vcItemName'

				--PRINT @strNewQuery
                EXEC(@strNewQuery)
			END	
        END
        ELSE
        BEGIN
				DECLARE @vcWareHouseSearch VARCHAR(1000)
				DECLARE @vcVendorSearch VARCHAR(1000)
               

				SET @vcWareHouseSearch = ''
				SET @vcVendorSearch = ''

                DECLARE @tintOrder AS INT
                DECLARE @Fld_id AS NVARCHAR(20)
                DECLARE @Fld_Name AS VARCHAR(20)
                SET @strSQL = ''
                SELECT TOP 1
                        @tintOrder = tintOrder + 1 ,
                        @Fld_id = numFieldId ,
                        @Fld_Name = vcFieldName
                FROM    #Temp1
                WHERE   Custom = 1
                ORDER BY tintOrder
                WHILE @tintOrder > 0
                    BEGIN
                        SET @strSQL = @strSQL + ', dbo.GetCustFldValueItem('
                            + @Fld_id + ', I.numItemCode) as [' + @Fld_Name
                            + ']'

                        SELECT TOP 1
                                @tintOrder = tintOrder + 1 ,
                                @Fld_id = numFieldId ,
                                @Fld_Name = vcFieldName
                        FROM    #Temp1
                        WHERE   Custom = 1
                                AND tintOrder >= @tintOrder
                        ORDER BY tintOrder
                        IF @@rowcount = 0
                            SET @tintOrder = 0
                    END


				--Temp table for Item Search Configuration

                SELECT  *
                INTO    #tempSearch
                FROM    ( SELECT    numFieldId ,
                                    vcDbColumnName ,
                                    ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                                    tintRow AS tintOrder ,
                                    0 AS Custom
                          FROM      View_DynamicColumns
                          WHERE     numFormId = 22
                                    AND numUserCntID = @numUserCntID
                                    AND numDomainID = @numDomainID
                                    AND tintPageType = 1
                                    AND bitCustom = 0
                                    AND ISNULL(bitSettingField, 0) = 1
                                    AND numRelCntType = 1
                          UNION
                          SELECT    numFieldId ,
                                    vcFieldName ,
                                    vcFieldName ,
                                    tintRow AS tintOrder ,
                                    1 AS Custom
                          FROM      View_DynamicCustomColumns
                          WHERE     Grp_id = 5
                                    AND numFormId = 22
                                    AND numUserCntID = @numUserCntID
                                    AND numDomainID = @numDomainID
                                    AND tintPageType = 1
                                    AND bitCustom = 1
                                    AND numRelCntType = 1
                        ) X 
  
                IF NOT EXISTS ( SELECT  *
                                FROM    #tempSearch )
                    BEGIN
                        INSERT  INTO #tempSearch
                                SELECT  numFieldId ,
                                        vcDbColumnName ,
                                        ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                                        tintorder ,
                                        0
                                FROM    View_DynamicDefaultColumns
                                WHERE   numFormId = 22
                                        AND bitDefault = 1
                                        AND ISNULL(bitSettingField, 0) = 1
                                        AND numDomainID = @numDomainID 
                    END

				--Regular Search
                DECLARE @strSearch AS VARCHAR(8000) ,
                    @CustomSearch AS VARCHAR(4000) ,
                    @numFieldId AS NUMERIC(9)
                SET @strSearch = ''
                SELECT TOP 1
                        @tintOrder = tintOrder + 1 ,
                        @Fld_Name = vcDbColumnName ,
                        @numFieldId = numFieldId
                FROM    #tempSearch
                WHERE   Custom = 0
                ORDER BY tintOrder
                WHILE @tintOrder > -1
                    BEGIN
                        IF @Fld_Name = 'vcPartNo'
						BEGIN
                            INSERT  INTO #tempItemCode
                                    SELECT DISTINCT
                                            Item.numItemCode
                                    FROM    Vendor
                                            JOIN Item ON Item.numItemCode = Vendor.numItemCode
                                    WHERE   Item.numDomainID = @numDomainID
                                            AND Vendor.numDomainID = @numDomainID
                                            AND Vendor.vcPartNo IS NOT NULL
                                            AND LEN(Vendor.vcPartNo) > 0
                                            AND vcPartNo LIKE '%' + @str + '%'
							
							SET @vcVendorSearch = 'dbo.Vendor.vcPartNo LIKE ''%' + @str + '%'''
							
						END
                        ELSE IF @Fld_Name = 'vcBarCode'
						BEGIN
                            INSERT  INTO #tempItemCode
                                    SELECT DISTINCT
                                            Item.numItemCode
                                    FROM    Item
                                            JOIN WareHouseItems WI ON WI.numDomainID = @numDomainID
                                                            AND Item.numItemCode = WI.numItemID
                                            JOIN Warehouses W ON W.numDomainID = @numDomainID
                                                            AND W.numWareHouseID = WI.numWareHouseID
                                    WHERE   Item.numDomainID = @numDomainID
                                            AND ISNULL(Item.numItemGroup,
                                                        0) > 0
                                            AND WI.vcBarCode IS NOT NULL
                                            AND LEN(WI.vcBarCode) > 0
                                            AND WI.vcBarCode LIKE '%' + @str + '%'

							SET @vcWareHouseSearch = 'dbo.WareHouseItems.vcBarCode LIKE ''%' + @str + '%'''
						END
                        ELSE IF @Fld_Name = 'vcWHSKU'
						BEGIN
                            INSERT  INTO #tempItemCode
                                    SELECT DISTINCT
                                            Item.numItemCode
                                    FROM    Item
                                            JOIN WareHouseItems WI ON WI.numDomainID = @numDomainID
                                                        AND Item.numItemCode = WI.numItemID
                                            JOIN Warehouses W ON W.numDomainID = @numDomainID
                                                        AND W.numWareHouseID = WI.numWareHouseID
                                    WHERE   Item.numDomainID = @numDomainID
                                            AND ISNULL(Item.numItemGroup,
                                                        0) > 0
                                            AND WI.vcWHSKU IS NOT NULL
                                            AND LEN(WI.vcWHSKU) > 0
                                            AND WI.vcWHSKU LIKE '%'
                                            + @str + '%'

							IF LEN(@vcWareHouseSearch) > 0
								SET @vcWareHouseSearch = @vcWareHouseSearch + ' OR dbo.WareHouseItems.vcWHSKU LIKE ''%' + @str + '%'''
							ELSE
								SET @vcWareHouseSearch = 'dbo.WareHouseItems.vcWHSKU LIKE ''%' + @str + '%'''
						END
                        ELSE
                            SET @strSearch = @strSearch
                                + (CASE @Fld_Name WHEN 'numItemCode' THEN ' I.[' + @Fld_Name ELSE ' [' + @Fld_Name END)  + '] LIKE ''%' + @str
                                + '%'''

                        SELECT TOP 1
                                @tintOrder = tintOrder + 1 ,
                                @Fld_Name = vcDbColumnName ,
                                @numFieldId = numFieldId
                        FROM    #tempSearch
                        WHERE   tintOrder >= @tintOrder
                                AND Custom = 0
                        ORDER BY tintOrder
                        IF @@rowcount = 0
                            BEGIN
                                SET @tintOrder = -1
                            END
                        ELSE
                            IF @Fld_Name != 'vcPartNo'
                                AND @Fld_Name != 'vcBarCode'
                                AND @Fld_Name != 'vcWHSKU'
                                BEGIN
                                    SET @strSearch = @strSearch + ' or '
                                END
                    END

                IF ( SELECT COUNT(*)
                     FROM   #tempItemCode
                   ) > 0
                    SET @strSearch = @strSearch
                        + ' or  I.numItemCode in (select numItemCode from #tempItemCode)' 

				--Custom Search
                SET @CustomSearch = ''
                SELECT TOP 1
                        @tintOrder = tintOrder + 1 ,
                        @Fld_Name = vcDbColumnName ,
                        @numFieldId = numFieldId
                FROM    #tempSearch
                WHERE   Custom = 1
                ORDER BY tintOrder
                WHILE @tintOrder > -1
                    BEGIN
    
                        SET @CustomSearch = @CustomSearch
                            + CAST(@numFieldId AS VARCHAR(10)) 

                        SELECT TOP 1
                                @tintOrder = tintOrder + 1 ,
                                @Fld_Name = vcDbColumnName ,
                                @numFieldId = numFieldId
                        FROM    #tempSearch
                        WHERE   tintOrder >= @tintOrder
                                AND Custom = 1
                        ORDER BY tintOrder
                        IF @@rowcount = 0
                            BEGIN
                                SET @tintOrder = -1
                            END
                        ELSE
                            BEGIN
                                SET @CustomSearch = @CustomSearch + ' , '
                            END
                    END
	
                IF LEN(@CustomSearch) > 0
                    BEGIN
                        SET @CustomSearch = ' I.numItemCode in (SELECT RecId FROM dbo.CFW_FLD_Values_Item WHERE Fld_ID IN ('
                            + @CustomSearch + ') and dbo.GetCustFldValueItem(Fld_ID,RecId) like ''%'
                            + @str + '%'')'

                        IF LEN(@strSearch) > 0
                            SET @strSearch = @strSearch + ' OR '
                                + @CustomSearch
                        ELSE
                            SET @strSearch = @CustomSearch  
                    END


                IF @tintOppType = 1
                    OR ( @bitRemoveVendorPOValidation = 1
                         AND @tintOppType = 2
                       )
                    BEGIN      
                        SET @strSQL = 'select I.numItemCode,
									  (SELECT TOP 1 vcPathForImage from ItemImages WHERE numItemCode = I.numItemCode) as vcPathForImage,
									  (SELECT TOP 1 vcPathForTImage from ItemImages WHERE numItemCode = I.numItemCode) as vcPathForTImage,
									  isnull(vcItemName,'''') vcItemName,
									  CASE 
									  WHEN I.[charItemType]=''P''
									  THEN convert(varchar(100),ISNULL((SELECT TOP 1 ROUND(monWListPrice,2) FROM [WareHouseItems] WHERE [numItemID]=I.numItemCode),0)) 
									  ELSE 
										CONVERT(VARCHAR(100), ROUND(monListPrice, 2)) 
									  END AS monListPrice,
									  isnull(vcSKU,'''') vcSKU,
									  isnull(numBarCodeId,0) numBarCodeId,
									  isnull(vcModelID,'''') vcModelID,
									  isnull(txtItemDesc,'''') as txtItemDesc,
									  isnull(C.vcCompanyName,'''') as vcCompanyName,
									  WHT.numWareHouseItemID,
									  dbo.fn_GetAttributes(WHT.numWareHouseItemId,I.bitSerialized) AS [vcAttributes],
									  (CASE 
											WHEN ISNULL(I.bitKitParent,0) = 1 
											THEN 
												(
													CASE
														WHEN (SELECT COUNT(*) FROM ItemDetails INNER JOIN Item ON ItemDetails.numChildItemID = Item.numItemCode WHERE numItemKitID=I.numItemCode AND ISNULL(Item.bitKitParent,0) = 1) > 0
														THEN 1
														ELSE 0
													END
												)
											ELSE 0 
										END) bitHasKitAsChild,ISNULL(I.bitMatrix,0) AS bitMatrix,ISNULL(I.numItemGroup,0) AS numItemGroup '
									  + @strSQL
									  + ' INTO #TEMPItems FROM Item  I WITH (NOLOCK)
									  Left join DivisionMaster D              
									  on I.numVendorID=D.numDivisionID  
									  left join CompanyInfo C  
									  on C.numCompanyID=D.numCompanyID 
									  LEFT JOIN
											dbo.WareHouseItems WHT
									  ON
											WHT.numItemID = I.numItemCode AND
											WHT.numWareHouseItemID = (' +
																			CASE LEN(@vcWareHouseSearch)
																			WHEN 0 
																			THEN 
																				'SELECT 
																					TOP 1 numWareHouseItemID 
																				FROM 
																					dbo.WareHouseItems 
																				WHERE 
																					dbo.WareHouseItems.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + ' 
																					AND dbo.WareHouseItems.numItemID = I.numItemCode 
																					AND (dbo.WareHouseItems.numWareHouseID = ' +  CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20))  + ' OR ' + CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20)) + ' = 0)'
																			ELSE
																				'CASE 
																					WHEN (SELECT 
																								COUNT(*) 
																							FROM 
																								dbo.WareHouseItems 
																							WHERE 
																								dbo.WareHouseItems.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + ' 
																								AND dbo.WareHouseItems.numItemID = I.numItemCode 
																								AND (dbo.WareHouseItems.numWareHouseID = ' +  CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20))  + ' OR ' + CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20)) + ' = 0)' +
																								' AND (' + @vcWareHouseSearch + ')) > 0 
																					THEN
																						(SELECT 
																							TOP 1 numWareHouseItemID 
																						FROM 
																							dbo.WareHouseItems 
																						WHERE 
																							dbo.WareHouseItems.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + ' 
																							AND dbo.WareHouseItems.numItemID = I.numItemCode 
																							AND (dbo.WareHouseItems.numWareHouseID = ' +  CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20))  + ' OR ' + CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20)) + ' = 0) AND (' + @vcWareHouseSearch + '))
																					ELSE
																						(SELECT 
																							TOP 1 numWareHouseItemID 
																						FROM 
																							dbo.WareHouseItems 
																						WHERE 
																							dbo.WareHouseItems.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + ' 
																							AND dbo.WareHouseItems.numItemID = I.numItemCode 
																							AND (dbo.WareHouseItems.numWareHouseID = ' +  CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20))  + ' OR ' + CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20)) + ' = 0))
																					END' 
																			END
																		+
																	') 
										where ((I.charItemType <> ''P'') OR ((SELECT COUNT(*) FROM dbo.WareHouseItems WHERE numItemID = I.numItemCode AND numDomainID = '
									  + CONVERT(VARCHAR(20), @numDomainID)
									  + ' AND numWareHouseID =  ' +  CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20))  + ' OR ' + CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20)) + ' = 0) > 0)) AND ISNULL(I.Isarchieve,0) <> 1 And I.charItemType NOT IN(''A'') AND  I.numDomainID='
									  + CONVERT(VARCHAR(20), @numDomainID) + ' and ('
									  + @strSearch + ') '   
						     
						--- added Asset validation  by sojan
                        IF @bitRemoveVendorPOValidation = 1
                            AND @tintOppType = 2
                            SET @strSQL = @strSQL
                                + ' and ISNULL(I.bitKitParent,0) = 0 AND ISNULL(I.bitAssembly,0) = 0 '

                        IF @tintSearchOrderCustomerHistory = 1
                            AND @numDivisionID > 0
                            BEGIN
                                SET @strSQL = @strSQL
                                    + ' and I.numItemCode in (select distinct oppI.numItemCode from 
									OpportunityMaster oppM join OpportunityItems oppI on oppM.numOppId=oppI.numOppId where oppM.numDomainID='
                                    + CONVERT(VARCHAR(20), @numDomainID)
                                    + ' and oppM.numDivisionId='
                                    + CONVERT(VARCHAR(20), @numDivisionId)
                                    + ')'
                            END


						 SET @strSQL = @strSQL + 'DELETE FROM 
														#TEMPItems
													WHERE 
														numItemCode IN (
																			SELECT 
																				F.numItemCode
																			FROM 
																				#TEMPItems AS F
																			WHERE 
																				ISNULL(F.bitMatrix,0) = 1 AND
																				EXISTS (
																							SELECT 
																								t1.vcItemName, t1.bitMatrix, t1.numItemGroup, Count(t1.numItemCode)
																							FROM 
																								#TEMPItems t1
																							WHERE 
																								t1.vcItemName = F.vcItemName
																								AND t1.bitMatrix = 1
																								AND t1.numItemGroup = F.numItemGroup
																							GROUP BY 
																								t1.vcItemName, t1.bitMatrix, t1.numItemGroup
																							HAVING 
																								Count(t1.numItemCode) > 1
																						)
																		)
														AND numItemCode NOT IN (
																								SELECT 
																									Min(numItemCode)
																								FROM 
																									#TEMPItems AS F
																								WHERE
																									ISNULL(F.bitMatrix,0) = 1 AND
																									Exists (
																												SELECT 
																													t2.vcItemName, t2.bitMatrix, t2.numItemGroup, Count(t2.numItemCode)
																												FROM 
																													#TEMPItems t2
																												WHERE 
																													t2.vcItemName = F.vcItemName
																												   AND t2.bitMatrix = 1
																												   AND t2.numItemGroup = F.numItemGroup
																												GROUP BY 
																													t2.vcItemName, t2.bitMatrix, t2.numItemGroup
																												HAVING 
																													Count(t2.numItemCode) > 1
																											)
																								GROUP BY 
																									vcItemName, bitMatrix, numItemGroup
																							);'


						SET @strSQL = @strSQL + ' SELECT SRNO=ROW_NUMBER() OVER( ORDER BY vcItemName),(CASE WHEN vcSKU=''' + @str + ''' THEN 1 ELSE 0 END) bitSKUMatch,* INTO #TEMPItemsFinal FROM #TEMPItems;'


                        SET @strSQL = @strSQL + 'SELECT @TotalCount = COUNT(*) FROM #TEMPItemsFinal; select * from #TEMPItemsFinal where SRNO> '
                            + CAST(( ( @numPageIndex - 1 ) * @numPageSize ) AS VARCHAR(10))
                            + ' and SRNO < '
                            + CAST(( ( @numPageIndex * @numPageSize ) + 1 ) AS VARCHAR(10))
                            + ' order by  vcItemName; DROP TABLE #TEMPItemsFinal; DROP TABLE #TEMPItems;'
                        

						--PRINT @strSQL
						EXEC sp_executeSQL @strSQL, N'@TotalCount INT OUTPUT', @TotalCount OUTPUT
                    END      
                ELSE
                    IF @tintOppType = 2
                        BEGIN      
                            SET @strSQL = 'SELECT I.numItemCode,
										  (SELECT TOP 1 vcPathForImage from ItemImages WHERE numItemCode = I.numItemCode) as vcPathForImage,
										  (SELECT TOP 1 vcPathForTImage from ItemImages WHERE numItemCode = I.numItemCode) as vcPathForTImage,
										  ISNULL(vcItemName,'''') vcItemName,
										  convert(varchar(200),round(monListPrice,2)) as monListPrice,
										  isnull(vcSKU,'''') vcSKU,
										  isnull(numBarCodeId,0) numBarCodeId,
										  isnull(vcModelID,'''') vcModelID,
										  isnull(txtItemDesc,'''') as txtItemDesc,
										  isnull(C.vcCompanyName,'''') as vcCompanyName,
										  WHT.numWareHouseItemID,
										  dbo.fn_GetAttributes(WHT.numWareHouseItemId,I.bitSerialized) AS [vcAttributes],
										  (CASE 
												WHEN ISNULL(I.bitKitParent,0) = 1 
												THEN 
													(
														CASE
															WHEN (SELECT COUNT(*) FROM ItemDetails INNER JOIN Item ON ItemDetails.numChildItemID = Item.numItemCode WHERE numItemKitID=I.numItemCode AND ISNULL(Item.bitKitParent,0) = 1) > 0
															THEN 1
															ELSE 0
														END
													)
												ELSE 0 
											END) bitHasKitAsChild'
										  + @strSQL
										  + '  from item I  WITH (NOLOCK)
										  INNER JOIN
											dbo.Vendor V
										  ON
											V.numItemCode = I.numItemCode
										  Left join 
											DivisionMaster D              
										  ON 
											V.numVendorID=D.numDivisionID  
										  LEFT join 
											CompanyInfo C  
										  ON 
											C.numCompanyID=D.numCompanyID    
										  LEFT JOIN
											dbo.WareHouseItems WHT
										  ON
											WHT.numItemID = I.numItemCode AND
											WHT.numWareHouseItemID = (' +
																			CASE LEN(@vcWareHouseSearch)
																			WHEN 0 
																			THEN 
																				'SELECT 
																					TOP 1 numWareHouseItemID 
																				FROM 
																					dbo.WareHouseItems 
																				WHERE 
																					dbo.WareHouseItems.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + ' 
																					AND dbo.WareHouseItems.numItemID = I.numItemCode 
																					AND (dbo.WareHouseItems.numWareHouseID = ' +  CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20))  + ' OR ' + CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20)) + ' = 0)'
																			ELSE
																				'CASE 
																					WHEN (SELECT 
																								COUNT(*) 
																							FROM 
																								dbo.WareHouseItems 
																							WHERE 
																								dbo.WareHouseItems.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + ' 
																								AND dbo.WareHouseItems.numItemID = I.numItemCode 
																								AND (dbo.WareHouseItems.numWareHouseID = ' +  CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20))  + ' OR ' + CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20)) + ' = 0)' +
																								' AND (' + @vcWareHouseSearch + ')) > 0 
																					THEN
																						(SELECT 
																							TOP 1 numWareHouseItemID 
																						FROM 
																							dbo.WareHouseItems 
																						WHERE 
																							dbo.WareHouseItems.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + ' 
																							AND dbo.WareHouseItems.numItemID = I.numItemCode 
																							AND (dbo.WareHouseItems.numWareHouseID = ' +  CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20))  + ' OR ' + CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20)) + ' = 0) AND (' + @vcWareHouseSearch + '))
																					ELSE
																						(SELECT 
																							TOP 1 numWareHouseItemID 
																						FROM 
																							dbo.WareHouseItems 
																						WHERE 
																							dbo.WareHouseItems.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + ' 
																							AND dbo.WareHouseItems.numItemID = I.numItemCode 
																							AND (dbo.WareHouseItems.numWareHouseID = ' +  CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20))  + ' OR ' + CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20)) + ' = 0))
																					END' 
																			END
																		+
																	')
										       
										  WHERE (
													(I.charItemType <> ''P'') OR 
													((SELECT COUNT(*) FROM dbo.WareHouseItems WHERE numItemID = I.numItemCode AND numDomainID = '
										 + CONVERT(VARCHAR(20), @numDomainID)
										 + ' AND numWareHouseID =  ' +  CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20))  + ' OR ' + CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20)) + ' = 0) > 0)) AND ISNULL(I.Isarchieve,0) <> 1 and ISNULL(I.bitKitParent,0) = 0  AND ISNULL(I.bitAssembly,0) = 0 And I.charItemType NOT IN(''A'') AND V.numVendorID='
										 + CONVERT(VARCHAR(20), @numDivisionID)
										 + ' and ('+ CASE LEN(@vcVendorSearch)
																			WHEN 0 THEN ''
																			ELSE REPLACE(@vcVendorSearch,'dbo.Vendor','V') + ' OR '
																			END + @strSearch + ') ' 

							--- added Asset validation  by sojan
                            IF @tintSearchOrderCustomerHistory = 1
                                AND @numDivisionID > 0
                                BEGIN
                                    SET @strSQL = @strSQL
                                        + ' and I.numItemCode in (select distinct oppI.numItemCode from 
									OpportunityMaster oppM join OpportunityItems oppI on oppM.numOppId=oppI.numOppId where oppM.numDomainID='
                                        + CONVERT(VARCHAR(20), @numDomainID)
                                        + ' and oppM.numDivisionId='
                                        + CONVERT(VARCHAR(20), @numDivisionId)
                                        + ')'
                                END

							

							 SET @strSQL = @strSQL + 'DELETE FROM 
														#TEMPItems
													WHERE 
														numItemCode IN (
																			SELECT 
																				F.numItemCode
																			FROM 
																				#TEMPItems AS F
																			WHERE
																				ISNULL(F.bitMatrix,0) = 1 AND
																				EXISTS (
																							SELECT 
																								t1.vcItemName, t1.bitMatrix, t1.numItemGroup, Count(t1.numItemCode)
																							FROM 
																								#TEMPItems t1
																							WHERE 
																								t1.vcItemName = F.vcItemName
																								AND t1.bitMatrix = 1
																								AND t1.numItemGroup = F.numItemGroup
																							GROUP BY 
																								t1.vcItemName, t1.bitMatrix, t1.numItemGroup
																							HAVING 
																								Count(t1.numItemCode) > 1
																						)
																		)
														AND numItemCode NOT IN (
																								SELECT 
																									Min(numItemCode)
																								FROM 
																									#TEMPItems AS F
																								WHERE
																									ISNULL(F.bitMatrix,0) = 1 AND
																									Exists (
																												SELECT 
																													t2.vcItemName, t2.bitMatrix, t2.numItemGroup, Count(t2.numItemCode)
																												FROM 
																													#TEMPItems t2
																												WHERE 
																													t2.vcItemName = F.vcItemName
																												   AND t2.bitMatrix = 1
																												   AND t2.numItemGroup = F.numItemGroup
																												GROUP BY 
																													t2.vcItemName, t2.bitMatrix, t2.numItemGroup
																												HAVING 
																													Count(t2.numItemCode) > 1
																											)
																								GROUP BY 
																									vcItemName, bitMatrix, numItemGroup
																							);'


							SET @strSQL = @strSQL + ' SELECT SRNO=ROW_NUMBER() OVER( ORDER BY vcItemName),(CASE WHEN vcSKU=''' + @str + ''' THEN 1 ELSE 0 END) bitSKUMatch,* INTO #TEMPItemsFinal FROM #TEMPItems;'


							SET @strSQL = @strSQL + 'SELECT @TotalCount = COUNT(*) FROM #TEMPItemsFinal; select * from #TEMPItemsFinal where SRNO> '
                            + CAST(( ( @numPageIndex - 1 ) * @numPageSize ) AS VARCHAR(10))
                            + ' and SRNO < '
                            + CAST(( ( @numPageIndex * @numPageSize ) + 1 ) AS VARCHAR(10))
                            + ' order by  vcItemName; DROP TABLE #TEMPItemsFinal; DROP TABLE #TEMPItems;'
                        
							EXEC sp_executeSQL @strSQL, N'@TotalCount INT OUTPUT', @TotalCount OUTPUT
                        END
                    ELSE
                        SELECT  0  
            END
    END
ELSE
    SELECT  0  

SELECT  *
FROM    #Temp1
WHERE   vcDbColumnName NOT IN ( 'vcPartNo', 'vcBarCode', 'vcWHSKU' )ORDER BY tintOrder 
   


IF OBJECT_ID('dbo.Scores', 'U') IS NOT NULL
  DROP TABLE dbo.Scores

IF OBJECT_ID('tempdb..#Temp1') IS NOT NULL
BEGIN
    DROP TABLE #Temp1
END

IF OBJECT_ID('tempdb..#tempSearch') IS NOT NULL
BEGIN
    DROP TABLE #tempSearch
END

IF OBJECT_ID('tempdb..#tempItemCode') IS NOT NULL
BEGIN
    DROP TABLE #tempItemCode
END


    END
--Created By Anoop Jayaraj          
--exec USP_ItemDetailsForEcomm @numItemCode=859064,@numWareHouseID=1074,@numDomainID=172,@numSiteId=90
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_itemdetailsforecomm')
DROP PROCEDURE USP_ItemDetailsForEcomm
GO
CREATE PROCEDURE [dbo].[USP_ItemDetailsForEcomm]
@numItemCode as numeric(9),          
@numWareHouseID as numeric(9),    
@numDomainID as numeric(9),
@numSiteId as numeric(9),
@vcCookieId as VARCHAR(MAX)=null,
@numUserCntID AS NUMERIC(9)=0,
@numDivisionID AS NUMERIC(18,0) = 0                                        
as     
/*Removed warehouse parameter, and taking Default warehouse by default from DB*/
    
declare @bitShowInStock as bit        
declare @bitShowQuantity as BIT
declare @bitAutoSelectWarehouse as bit        
--declare @tintColumns as tinyint        
DECLARE @numDefaultWareHouseID as numeric(9)
DECLARE @numDefaultRelationship AS NUMERIC(18,0)
DECLARE @numDefaultProfile AS NUMERIC(18,0)
DECLARE @tintPreLoginPriceLevel AS TINYINT

set @bitShowInStock=0        
set @bitShowQuantity=0        
--set @tintColumns=1        
        
SELECT 
	@bitShowInStock=ISNULL(bitShowInStock,0)
	,@bitShowQuantity=ISNULL(bitShowQOnHand,0)
	,@bitAutoSelectWarehouse=ISNULL(bitAutoSelectWarehouse,0)
	,@numDefaultRelationship=numRelationshipId
	,@numDefaultProfile=numProfileId
	,@tintPreLoginPriceLevel=(CASE WHEN @numDivisionID > 0 THEN 0 ELSE ISNULL(tintPreLoginProceLevel,0) END)
FROM 
	eCommerceDTL 
WHERE 
	numDomainID=@numDomainID

IF ISNULL(@numDivisionID,0) > 0
BEGIN
	SELECT @numDefaultRelationship=ISNULL(tintCRMType,0),@numDefaultProfile=ISNULL(vcProfile,0) FROM DivisionMaster INNER JOIN CompanyInfo ON DivisionMaster.numCompanyID=CompanyInfo.numCompanyId WHERE numDivisionID=@numDivisionID AND DivisionMaster.numDomainID=@numDomainID
END


IF @numWareHouseID=0
BEGIN
	SELECT @numDefaultWareHouseID=ISNULL(numDefaultWareHouseID,0) FROM [eCommerceDTL] WHERE [numDomainID]=@numDomainID	
	SET @numWareHouseID =@numDefaultWareHouseID;
END

/*If qty is not found in default warehouse then choose next warehouse with sufficient qty */
IF @bitAutoSelectWarehouse = 1 AND ( SELECT COUNT(*) FROM dbo.WareHouseItems WHERE  numItemID = @numItemCode AND numWarehouseID= @numWareHouseID ) = 0 
BEGIN
	SELECT TOP 1 @numDefaultWareHouseID = numWareHouseID FROM dbo.WareHouseItems WHERE  numItemID = @numItemCode AND numOnHand>0 /*TODO: Pass actuall qty from cart*/ ORDER BY numOnHand DESC 
	IF (ISNULL(@numDefaultWareHouseID,0)>0)
		SET @numWareHouseID =@numDefaultWareHouseID;
END

      DECLARE @UOMConversionFactor AS DECIMAL(18,5)
      
      Select @UOMConversionFactor= ISNULL(dbo.fn_UOMConversion(ISNULL(I.numSaleUnit,0),I.numItemCode,I.numDomainId,ISNULL(I.numBaseUnit,0)),0)
      FROM Item I where numItemCode=@numItemCode   
	   
	  DECLARE @PromotionOffers AS DECIMAL(18,2)=0     
	  IF(@vcCookieId!=null OR @vcCookieId!='')   
	  BEGIN                    
SET @PromotionOffers=(SELECT TOP 1 CASE WHEN P.tintDiscountType=1  
				THEN (I.monListPrice*fltDiscountValue)/100
				WHEN P.tintDiscountType=2
				THEN fltDiscountValue
				WHEN P.tintDiscountType=3
				THEN fltDiscountValue*I.monListPrice
				ELSE 0 END
FROM CartItems AS C
LEFT JOIN PromotionOffer AS P ON P.numProId=C.PromotionID
LEFT JOIN Item I ON I.numItemCode=C.numItemCode
WHERE I.numItemCode=@numItemCode AND C.vcCookieId=@vcCookieId AND C.numUserCntId=@numUserCntID AND C.fltDiscount=0 AND 
(I.numItemCode IN (SELECT PIS.numValue FROM PromotionOfferItems AS PIS WHERE PIS.numProId=P.numProId AND PIS.tintType=1 AND PIS.tintRecordType=6) OR
I.numItemClassification IN (SELECT PIS.numValue FROM PromotionOfferItems AS PIS WHERE PIS.numProId=P.numProId AND PIS.tintType=2 AND PIS.tintRecordType=6))
AND P.numDomainId=@numDomainID 
		AND ISNULL(bitEnabled,0)=1 
		AND ISNULL(bitAppliesToSite,0)=1 
		AND 1 = (CASE WHEN bitRequireCouponCode = 1 THEN CASE WHEN ISNULL(intCouponCodeUsed,0) < ISNULL(tintUsageLimit,0) OR ISNULL(tintUsageLimit,0) = 0  THEN 1 ELSE 0 END ELSE 1 END)
		AND 1 = (CASE WHEN bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN CAST(GETDATE() AS DATE)>=dtValidFrom AND CAST(GETDATE() AS DATE)<=dtValidTo THEN 1 ELSE 0 END) END))
		END
		PRINT @PromotionOffers
 
declare @strSql as varchar(MAX)
set @strSql=' With tblItem AS (                  
select I.numItemCode, vcItemName, txtItemDesc, charItemType, ISNULL(I.bitMatrix,0) bitMatrix,' + 
(CASE WHEN @tintPreLoginPriceLevel > 0 THEN 'ISNULL(PricingOption.monPrice,ISNULL(CASE WHEN I.[charItemType]=''P'' 
			THEN '+ convert(varchar(15),@UOMConversionFactor) + ' * W.[monWListPrice]  
			ELSE '+ convert(varchar(15),@UOMConversionFactor) + ' * monListPrice 
	   END,0))' ELSE 'ISNULL(CASE WHEN I.[charItemType]=''P'' 
			THEN '+ convert(varchar(15),@UOMConversionFactor) + ' * W.[monWListPrice]  
			ELSE '+ convert(varchar(15),@UOMConversionFactor) + ' * monListPrice 
	   END,0)' END)
+'
AS monListPrice,
numItemClassification, bitTaxable, vcSKU, bitKitParent,              
I.numModifiedBy, (SELECT numItemImageId,vcPathForImage,vcPathForTImage,bitDefault,case when bitdefault=1  then -1 else isnull(intDisplayOrder,0) end as intDisplayOrder FROM ItemImages  WHERE numItemCode=I.numItemCode and bitIsImage = 1 order by case when bitdefault=1 then -1 else isnull(intDisplayOrder,0) end  asc FOR XML AUTO,ROOT(''Images''))  vcImages,
(SELECT vcPathForImage FROM ItemImages  WHERE numItemCode=I.numItemCode and bitIsImage = 0 order by intDisplayOrder  asc FOR XML AUTO,ROOT(''Documents''))  vcDocuments ,
 isnull(bitSerialized,0) as bitSerialized, vcModelID,                 
numItemGroup,                   
(isnull(sum(numOnHand),0) / '+ convert(varchar(15),@UOMConversionFactor) + ') as numOnHand,                    
sum(numOnOrder) as numOnOrder,                    
sum(numReorder)  as numReorder,                    
sum(numAllocation)  as numAllocation,                    
sum(numBackOrder)  as numBackOrder,              
isnull(fltWeight,0) as fltWeight,              
isnull(fltHeight,0) as fltHeight,              
isnull(fltWidth,0) as fltWidth,              
isnull(fltLength,0) as fltLength,              
case when charItemType=''P''then isnull(bitFreeShipping,0) else 1 end as bitFreeShipping,
Case When (case when charItemType=''P''then isnull(bitFreeShipping,0) else 1 end )=1 then ''Free shipping'' else ''Calculated at checkout'' end as Shipping,              
isnull(bitAllowBackOrder,0) as bitAllowBackOrder,bitShowInStock,bitShowQOnHand,isnull(numWareHouseItemID,0)  as numWareHouseItemID,    
(case when charItemType<>''S'' and charItemType<>''N'' then         
 (Case when '+ convert(varchar(15),@bitShowInStock) + '=1  then ( Case when numWareHouseItemID is null then ''<font color=red>Out Of Stock</font>'' when bitAllowBackOrder=1 then ''In Stock'' else         
 (Case when isnull(sum(numOnHand),0)>0 then ''In Stock''         
 else ''<font color=red>Out Of Stock</font>'' end ) end)    else '''' end) else '''' end) as InStock,
ISNULL(numSaleUnit,0) AS numUOM,
ISNULL(vcUnitName,'''') AS vcUOMName,
 '+ convert(varchar(15),@UOMConversionFactor) + ' AS UOMConversionFactor,
I.numCreatedBy,
I.[vcManufacturer],
(SELECT  COUNT(*) FROM  Review where vcReferenceId = CONVERT(VARCHAR(50) ,I.numItemCode) and numDomainId = I.numDomainId and numSiteId = '+ CONVERT(VARCHAR(20) , @numSiteId)  +' AND bitHide = 0 ) as ReviewCount ,
(SELECT  COUNT(*) FROM  Ratings where vcReferenceId = CONVERT(VARCHAR(50) ,I.numItemCode) and numDomainId = I.numDomainId and numSiteId = '+ CONVERT(VARCHAR(20) , @numSiteId)  +'  ) as RatingCount ,
(select top 1 vcMetaKeywords  from MetaTags where numReferenceID = I.numItemCode and tintMetaTagFor =2) as vcMetaKeywords,
(select top 1 vcMetaDescription  from MetaTags where numReferenceID = I.numItemCode and tintMetaTagFor =2) as vcMetaDescription ,
(SELECT vcCategoryName  FROM  dbo.Category WHERE numCategoryID =(SELECT TOP 1 numCategoryID FROM dbo.ItemCategory WHERE numItemID = I.numItemCode)) as vcCategoryName,('+(SELECT CAST(ISNULL(@PromotionOffers,0) AS varchar(20)))+') as PromotionOffers

from Item I ' + 
(CASE WHEN @tintPreLoginPriceLevel > 0 THEN ' OUTER APPLY(SELECT decDiscount AS monPrice FROM PricingTable WHERE tintRuleType=3 AND numItemCode=I.numItemCode ORDER BY numPricingID OFFSET ' + CAST(@tintPreLoginPriceLevel-1 AS VARCHAR) + ' ROWS FETCH NEXT 1 ROWS ONLY) As PricingOption ' ELSE '' END)
+ '                 
left join  WareHouseItems W                
on W.numItemID=I.numItemCode and numWareHouseID= '+ convert(varchar(15),@numWareHouseID) + '
left join  eCommerceDTL E          
on E.numDomainID=I.numDomainID     
LEFT JOIN UOM u ON u.numUOMId=I.numSaleUnit    
where I.numItemCode='+ convert(varchar(15),@numItemCode) +
CASE WHEN (SELECT COUNT(*) FROM [dbo].[EcommerceRelationshipProfile] WHERE numSiteID=@numSiteID AND numRelationship = @numDefaultRelationship AND numProfile = @numDefaultProfile) > 0
THEN
	CONCAT(' AND I.numItemClassification NOT IN (SELECT numItemClassification FROM EcommerceRelationshipProfile ERP INNER JOIN ECommerceItemClassification EIC ON EIC.numECommRelatiionshipProfileID=ERP.ID WHERE ERP.numSiteID=',@numSiteID,' AND ERP.numRelationship=',@numDefaultRelationship,' AND ERP.numProfile=', @numDefaultProfile,')')
ELSE
	''
END
+ '           
group by I.numItemCode, vcItemName, txtItemDesc, charItemType, monListPrice,numItemClassification, bitTaxable, vcSKU, bitKitParent,           
I.numDomainID,I.numModifiedBy,  bitSerialized, vcModelID,numItemGroup, fltWeight,fltHeight,fltWidth,          
fltLength,bitFreeShipping,bitAllowBackOrder,bitShowInStock,bitShowQOnHand ,numWareHouseItemID,vcUnitName,I.numCreatedBy,W.[monWListPrice],I.[vcManufacturer],
numSaleUnit, I.bitMatrix ' + (CASE WHEN @tintPreLoginPriceLevel > 0 THEN ',PricingOption.monPrice' ELSE '' END) + ')'

set @strSql=@strSql+' select numItemCode,vcItemName,txtItemDesc,charItemType, bitMatrix, monListPrice,numItemClassification, bitTaxable, vcSKU, bitKitParent,                  
tblItem.numModifiedBy, vcImages,vcDocuments, bitSerialized, vcModelID, numItemGroup,numOnHand,numOnOrder,numReorder,numAllocation, 
numBackOrder, fltWeight, fltHeight, fltWidth, fltLength, bitFreeShipping,bitAllowBackOrder,bitShowInStock,
bitShowQOnHand,numWareHouseItemID,InStock,numUOM,vcUOMName,UOMConversionFactor,tblItem.numCreatedBy,vcManufacturer,ReviewCount,RatingCount,Shipping,isNull(vcMetaKeywords,'''') as vcMetaKeywords ,isNull(vcMetaDescription,'''') as vcMetaDescription,isNull(vcCategoryName,'''') as vcCategoryName,PromotionOffers'     

set @strSql=@strSql+ ' from tblItem'
PRINT @strSql
exec (@strSql)


declare @tintOrder as tinyint                                                  
declare @vcFormFieldName as varchar(50)                                                  
declare @vcListItemType as varchar(1)                                             
declare @vcAssociatedControlType varchar(10)                                                  
declare @numListID AS numeric(9)                                                  
declare @WhereCondition varchar(2000)                       
Declare @numFormFieldId as numeric  
DECLARE @vcFieldType CHAR(1)
                  
set @tintOrder=0                                                  
set @WhereCondition =''                 
                   
              
  CREATE TABLE #tempAvailableFields(numFormFieldId  numeric(9),vcFormFieldName NVARCHAR(50),
        vcFieldType CHAR(1),vcAssociatedControlType NVARCHAR(50),numListID NUMERIC(18),
        vcListItemType CHAR(1),intRowNum INT,vcItemValue VARCHAR(3000))
   
  INSERT INTO #tempAvailableFields (numFormFieldId,vcFormFieldName,vcFieldType,vcAssociatedControlType
        ,numListID,vcListItemType,intRowNum)                         
            SELECT  Fld_id,REPLACE(Fld_label,' ','') AS vcFormFieldName,
                   CASE GRP_ID WHEN 4 then 'C' WHEN 1 THEN 'D' ELSE 'C' END AS vcFieldType,
                    fld_type as vcAssociatedControlType,
                    ISNULL(C.numListID, 0) numListID,
                    CASE WHEN C.numListID > 0 THEN 'L'
                         ELSE ''
                    END vcListItemType,ROW_NUMBER() OVER (ORDER BY Fld_id asc) AS intRowNum
            FROM    CFW_Fld_Master C LEFT JOIN CFW_Validation V ON V.numFieldID = C.Fld_id
            WHERE   C.numDomainID = @numDomainId
                    AND GRP_ID IN (5)

     select top 1 @tintOrder=intRowNum,@numFormFieldId=numFormFieldId,@vcFormFieldName=vcFormFieldName,
				  @vcFieldType=vcFieldType,@vcAssociatedControlType=vcAssociatedControlType,@numListID=numListID,
				  @vcListItemType=vcListItemType
	from #tempAvailableFields order by intRowNum ASC
   
while @tintOrder>0                                                  
begin                                                  
    if @vcAssociatedControlType = 'TextBox' or @vcAssociatedControlType = 'TextArea'
    begin  
		UPDATE #tempAvailableFields SET vcItemValue=(SELECT Fld_Value FROM CFW_FLD_Values_Item WHERE Fld_Id=@numFormFieldId AND RecId=@numItemCode) WHERE numFormFieldId=@numFormFieldId              
    end   
    else if @vcAssociatedControlType = 'CheckBox'
	begin      
		UPDATE #tempAvailableFields SET vcItemValue=(SELECT case when isnull(Fld_Value,0)=0 then 'No' when isnull(Fld_Value,0)=1 then 'Yes' END FROM CFW_FLD_Values_Item WHERE Fld_Id=@numFormFieldId AND RecId=@numItemCode) WHERE numFormFieldId=@numFormFieldId              
   end                
   else if @vcAssociatedControlType = 'DateField'           
   begin   
		UPDATE #tempAvailableFields SET vcItemValue=(SELECT dbo.FormatedDateFromDate(Fld_Value,@numDomainId) FROM CFW_FLD_Values_Item WHERE Fld_Id=@numFormFieldId AND RecId=@numItemCode) WHERE numFormFieldId=@numFormFieldId              
   end                
    else if @vcAssociatedControlType = 'SelectBox'           
   begin 
		UPDATE #tempAvailableFields SET vcItemValue=(SELECT L.vcData FROM CFW_FLD_Values_Item CFW left Join ListDetails L
			on L.numListItemID=CFW.Fld_Value                
			WHERE CFW.Fld_Id=@numFormFieldId AND CFW.RecId=@numItemCode) WHERE numFormFieldId=@numFormFieldId              
end          
               
 
    select top 1 @tintOrder=intRowNum,@numFormFieldId=numFormFieldId,@vcFormFieldName=vcFormFieldName,
				  @vcFieldType=vcFieldType,@vcAssociatedControlType=vcAssociatedControlType,@numListID=numListID,
				  @vcListItemType=vcListItemType
	from #tempAvailableFields WHERE intRowNum > @tintOrder order by intRowNum ASC
 
   if @@rowcount=0 set @tintOrder=0                                                  
end   


  
SELECT * FROM #tempAvailableFields

DROP TABLE #tempAvailableFields

--exec USP_ItemDetailsForEcomm @numItemCode=197611,@numWareHouseID=58,@numDomainID=1,@numSiteId=18
--exec USP_ItemDetailsForEcomm @numItemCode=735364,@numWareHouseID=1039,@numDomainID=156,@numSiteId=104
GO
/****** Object:  StoredProcedure [dbo].[USP_ItemList1]    Script Date: 05/07/2009 18:14:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Created By Anoop Jayaraj                                        
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'usp_itemlist1' ) 
    DROP PROCEDURE usp_itemlist1
GO
CREATE PROCEDURE [dbo].[USP_ItemList1]
    @ItemClassification AS NUMERIC(9) = 0,
    @IsKit AS TINYINT,
    @SortChar CHAR(1) = '0',
    @CurrentPage INT,
    @PageSize INT,
    @TotRecs INT OUTPUT, 
    @columnName AS VARCHAR(50),
    @columnSortOrder AS VARCHAR(10),
    @numDomainID AS NUMERIC(9) = 0,
    @ItemType AS CHAR(1),
    @bitSerialized AS BIT,
    @numItemGroup AS NUMERIC(9),
    @bitAssembly AS BIT,
    @numUserCntID AS NUMERIC(9),
    @Where	AS VARCHAR(MAX),
    @IsArchive AS BIT = 0,
    @byteMode AS TINYINT,
	@bitAsset as BIT=0,
	@bitRental as BIT=0,
	@bitContainer AS BIT=0,
	@SearchText VARCHAR(300),
	@vcRegularSearchCriteria varchar(MAX)='',
	@vcCustomSearchCriteria varchar(MAX)=''
AS 
BEGIN	
	DECLARE @numWarehouseID NUMERIC(18,0)
	SELECT TOP 1 @numWarehouseID = numWareHouseID FROM Warehouses WHERE numDomainID = @numDomainID


	CREATE TABLE #tempForm 
	(
		tintOrder TINYINT,vcDbColumnName nvarchar(50),vcOrigDbColumnName nvarchar(50),vcFieldName nvarchar(50),vcAssociatedControlType nvarchar(50),vcListItemType varchar(3),
		numListID numeric(9),vcLookBackTableName varchar(50),bitCustomField BIT,numFieldId NUMERIC,bitAllowSorting BIT,bitAllowEdit BIT,bitIsRequired BIT,bitIsEmail BIT,
		bitIsAlphaNumeric BIT,bitIsNumeric BIT,bitIsLengthValidation BIT,intMaxLength int,intMinLength int,bitFieldMessage BIT,vcFieldMessage VARCHAR(500),
		ListRelID numeric(9),intColumnWidth int,bitAllowFiltering BIT,vcFieldDataType CHAR(1),Grp_Id TINYINT
	)

	-- FIRST CHECK IF GRID COLUMNS ARE CONFIGURED
	DECLARE  @Nocolumns  AS TINYINT = 0

	SELECT 
		@Nocolumns=ISNULL(SUM(TotalRow),0) 
	FROM
	(            
		SELECT 
			COUNT(*) TotalRow 
		FROM 
			View_DynamicColumns 
		WHERE 
			numFormId=21 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND numRelCntType = 0
		UNION 
		SELECT 
			COUNT(*) TotalRow 
		FROM 
			View_DynamicCustomColumns 
		WHERE 
			numFormId=21 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND numRelCntType = 0
	) TotalRows

	-- IF GRID COLUMNS ARE NOT CONFIGURES THEN CHECK IF MASTER FORM CONFIGURATION IS CREATED BY ADMINISTRATOR
	DECLARE @IsMasterConfAvailable BIT = 0
	DECLARE @numUserGroup NUMERIC(18,0)

	IF @Nocolumns=0
	BEGIN
		SELECT @numUserGroup = numGroupID FROM UserMaster WHERE numUserDetailId = @numUserCntID 

		IF (SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = @numDomainID AND numFormID = 21 AND numRelCntType=0 AND bitGridConfiguration = 1 AND numGroupID = @numUserGroup) > 0
		BEGIN
			SET @IsMasterConfAvailable = 1
		END
	END

	--IF MASTERCONFIGURATION IS AVAILABLE THEN LOAD IT OTHERWISE LOAD DEFAULT COLUMNS
	IF @IsMasterConfAvailable = 1
	BEGIN
		INSERT INTO 
			#tempForm
		SELECT 
			(intRowNum + 1) as tintOrder, vcDbColumnName,	vcOrigDbColumnName,	ISNULL(vcCultureFieldName,vcFieldName), vcAssociatedControlType,
			vcListItemType, numListID, vcLookBackTableName,	bitCustom, numFieldId, bitAllowSorting,	bitInlineEdit, bitIsRequired,
			bitIsEmail,	bitIsAlphaNumeric, bitIsNumeric, bitIsLengthValidation,	intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,
			ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType,0
		FROM 
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId=21 AND 
			View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicColumnsMasterConfig.numRelCntType=0 AND 
			View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
			ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
		UNION
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName, vcAssociatedControlType,'' as vcListItemType,numListID,'',
			bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,'',Grp_Id
		FROM 
			View_DynamicCustomColumnsMasterConfig
		WHERE 
			View_DynamicCustomColumnsMasterConfig.numFormId=21 AND 
			View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicCustomColumnsMasterConfig.numRelCntType=0 AND 
			View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1
		ORDER BY 
			tintOrder ASC  
	END
	ELSE                                        
	BEGIN
		IF @Nocolumns=0
		BEGIN
			INSERT INTO DycFormConfigurationDetails 
			(
				numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth
			)
			SELECT 
				21,numFieldId,0,Row_number() over(order by tintRow desc),@numDomainID,@numUserCntID,0,1,0,intColumnWidth
			FROM 
				View_DynamicDefaultColumns
			WHERE 
				numFormId=21 
				AND bitDefault=1 
				AND ISNULL(bitSettingField,0)=1 
				AND numDomainID=@numDomainID
			ORDER BY 
				tintOrder asc   
		END

		INSERT INTO 
			#tempForm
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcOrigDbColumnName,ISNULL(vcCultureFieldName,vcFieldName),                  
			vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName,bitCustom,numFieldId,bitAllowSorting,bitInlineEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength,bitFieldMessage,
			vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType,0
		FROM 
			View_DynamicColumns 
		WHERE 
			numFormId=21 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND ISNULL(bitSettingField,0)=1 
			AND ISNULL(bitCustom,0)=0  
			AND numRelCntType = 0
		UNION
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName,vcAssociatedControlType,'' as vcListItemType,numListID,'',bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,'',Grp_Id
		FROM 
			View_DynamicCustomColumns
		WHERE 
			numFormId=21 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND ISNULL(bitCustom,0)=1 
			AND numRelCntType = 0
		ORDER BY 
			tintOrder asc  
	END   

	IF @byteMode=0
	BEGIN
		DECLARE @strColumns AS VARCHAR(MAX)
		SET @strColumns = ' I.numItemCode,ISNULL(DivisionMaster.numDivisionID,0) numDivisionID,ISNULL(DivisionMaster.tintCRMType,0) tintCRMType, ' + CAST(ISNULL(@numWarehouseID,0) AS VARCHAR) + ' AS numWarehouseID, I.numDomainID,0 AS numRecOwner,0 AS numTerID, I.charItemType, I.numItemCode AS [numItemCode~211~0]'

		DECLARE @tintOrder AS TINYINT                                                  
		DECLARE @vcFieldName AS VARCHAR(50)                                                  
		DECLARE @vcListItemType AS VARCHAR(3)                                             
		DECLARE @vcListItemType1 AS VARCHAR(1)                                                 
		DECLARE @vcAssociatedControlType VARCHAR(10)                                                  
		DECLARE @numListID AS NUMERIC(9)                                                  
		DECLARE @vcDbColumnName VARCHAR(50)                      
		DECLARE @WhereCondition VARCHAR(2000)                       
		DECLARE @vcLookBackTableName VARCHAR(2000)                
		DECLARE @bitCustom AS BIT                  
		DECLARE @numFieldId AS NUMERIC  
		DECLARE @bitAllowSorting AS CHAR(1)   
		DECLARE @bitAllowEdit AS CHAR(1)                   
        DECLARE @vcColumnName AS VARCHAR(200)
		DECLARE @Grp_ID AS TINYINT
		SET @tintOrder = 0                                                  
		SET @WhereCondition = ''                 
                   
		DECLARE @ListRelID AS NUMERIC(9) 
		DECLARE @SearchQuery AS VARCHAR(MAX) = ''

		IF LEN(@SearchText) > 0
		BEGIN
			SET @SearchQuery = ' I.numItemCode LIKE ''%' + @SearchText + '%'' OR I.vcItemName LIKE ''%' + @SearchText + '%'' OR I.vcModelID LIKE ''%' + @SearchText + '%'' OR I.vcSKU LIKE ''%' + @SearchText + '%'' OR cmp.vcCompanyName LIKE ''%' + @SearchText + '%'' OR I.vcManufacturer LIKE ''%' + @SearchText + '%'''
		END


		DECLARE @j INT = 0
  
		SELECT TOP 1
				@tintOrder = tintOrder + 1,
				@vcDbColumnName = vcDbColumnName,
				@vcFieldName = vcFieldName,
				@vcAssociatedControlType = vcAssociatedControlType,
				@vcListItemType = vcListItemType,
				@numListID = numListID,
				@vcLookBackTableName = vcLookBackTableName,
				@bitCustom = bitCustomField,
				@numFieldId = numFieldId,
				@bitAllowSorting = bitAllowSorting,
				@bitAllowEdit = bitAllowEdit,
				@ListRelID = ListRelID
		FROM    #tempForm --WHERE bitCustomField=1
		ORDER BY tintOrder ASC            

		WHILE @tintOrder > 0                                                  
		BEGIN
			IF @bitCustom = 0
			BEGIN
				DECLARE  @Prefix  AS VARCHAR(5)

				IF @vcLookBackTableName = 'Item'
					SET @Prefix = 'I.'
				ELSE IF @vcLookBackTableName = 'CompanyInfo'
					SET @Prefix = 'cmp.'
				ELSE IF @vcLookBackTableName = 'Vendor'
					SET @Prefix = 'V.'

				SET @vcColumnName= @vcDbColumnName+'~'+ convert(varchar(10),@numFieldId) +'~0'

				IF @vcAssociatedControlType='SelectBox' 
				BEGIN
					IF @vcListItemType='LI' 
					BEGIN
						SET @strColumns = @strColumns+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName+']'
						SET @WhereCondition= @WhereCondition +' LEFT JOIN ListDetails L'+ convert(varchar(3),@tintOrder)+ ' ON L'+convert(varchar(3),@tintOrder)+ '.numListItemID='+@vcDbColumnName                                                      
					END
					ELSE IF @vcListItemType = 'UOM'
					BEGIN
						SET @strColumns = @strColumns + ',ISNULL((SELECT vcUnitName FROM UOM WHERE numUOMId = I.' + @vcDbColumnName + '),'''')' + ' ['+ @vcColumnName+']'
					END
					ELSE IF @vcListItemType = 'IG'
					BEGIN
						SET @strColumns = @strColumns+ ',ISNULL((SELECT vcItemGroup FROM ItemGroups WHERE numItemGroupID = I.numItemGroup),'''')' + ' ['+ @vcColumnName+']'  
					END
				END
				ELSE IF @vcDbColumnName = 'vcPathForTImage'
		   		BEGIN
					SET @strColumns = @strColumns + ',ISNULL(II.vcPathForTImage,'''') AS ' + ' ['+ @vcColumnName+']'                  
					SET @WhereCondition = @WhereCondition + ' LEFT JOIN ItemImages II ON II.numItemCode = I.numItemCode AND bitDefault = 1'
				END
				ELSE IF @vcLookBackTableName = 'Item' AND @vcDbColumnName = 'monListPrice'
				BEGIN
					SET @strColumns = @strColumns + ',CASE WHEN I.charItemType=''P'' THEN ISNULL((SELECT TOP 1 monWListPrice FROM WarehouseItems WHERE numDomainID=I.numDomainID AND numItemID=I.numItemCode AND monWListPrice > 0),0.00) ELSE ISNULL(I.monListPrice,0.00) END AS ' + ' ['+ @vcColumnName+']'
				END
				ELSE IF @vcLookBackTableName = 'Item' AND @vcDbColumnName = 'ItemType'
				BEGIN
					SET @strColumns = @strColumns + ',' + '(CASE 
																WHEN I.charItemType=''P'' 
																THEN 
																	CASE 
																		WHEN ISNULL(I.bitAssembly,0) = 1 THEN ''Assembly''
																		WHEN ISNULL(I.bitKitParent,0) = 1 THEN ''Kit''
																		WHEN ISNULL(I.bitAsset,0) = 1 AND ISNULL(I.bitRental,0)=0 THEN ''Asset''
																		WHEN ISNULL(I.bitAsset,0) = 1 AND ISNULL(I.bitRental,0)=1 THEN ''Rental Asset''
																		WHEN ISNULL(I.bitSerialized,0) = 1 AND ISNULL(I.bitAsset,0) = 0  THEN ''Serialized''
																		WHEN ISNULL(I.bitSerialized,0) = 1 AND ISNULL(I.bitAsset,0) = 1 AND ISNULL(I.bitRental,0)=0 THEN ''Serialized Asset''
																		WHEN ISNULL(I.bitSerialized,0) = 1 AND ISNULL(I.bitAsset,0) = 1 AND ISNULL(I.bitRental,0)=1 THEN ''Serialized Rental Asset''
																		WHEN ISNULL(I.bitLotNo,0)=1 THEN ''Lot #''
																		ELSE ''Inventory Item'' 
																	END
																WHEN I.charItemType=''N'' THEN ''Non Inventory Item'' 
																WHEN I.charItemType=''S'' THEN ''Service'' 
																WHEN I.charItemType=''A'' THEN ''Accessory'' 
															END)' + ' AS ' + ' [' + @vcColumnName + ']'  
				END
				ELSE IF @vcLookBackTableName = 'Item' AND @vcDbColumnName = 'monStockValue'
				BEGIN
					SET @strColumns = @strColumns+',WarehouseItems.monStockValue'+' ['+ @vcColumnName+']'                                                      
				END
				ELSE IF @vcLookBackTableName = 'Warehouses' AND @vcDbColumnName = 'vcWareHouse'
				BEGIN
					SET @strColumns = @strColumns + ', (SELECT STUFF((SELECT CONCAT('', '', vcWareHouse) FROM WareHouseItems WI JOIN Warehouses W ON WI.numWareHouseID=W.numWareHouseID WHERE WI.numItemID=I.numItemCode FOR XML PATH('''')), 1, 1, ''''))' + ' AS ' + ' [' + @vcColumnName + ']'
				END
				ELSE IF @vcLookBackTableName = 'WorkOrder' AND @vcDbColumnName = 'WorkOrder'
				BEGIN
					SET @strColumns = @strColumns + ',(CASE WHEN (SELECT SUM(WO.numQtyItemsReq) from WorkOrder WO where WO.numItemCode=I.numItemCode and WO.numWOStatus=0) > 0 THEN 1 ELSE 0 END)' + ' AS ' + ' [' + @vcColumnName + ']'
				END
				ELSE IF @vcDbColumnName = 'vcPriceLevelDetail' 
				BEGIN
					SELECT @strColumns = @strColumns + CONCAT(', dbo.fn_GetItemPriceLevelDetail( ',@numDomainID,',I.numItemCode,',@numWarehouseID,')') +' ['+ @vcColumnName+']' 
				END
				ELSE
				BEGIN
					If @vcDbColumnName <> 'numItemCode' AND @vcDbColumnName <> 'vcPriceLevelDetail' --WE AE ALREADY ADDING COLUMN IN SELECT CLUASE DIRECTLY
					BEGIN
						SET @strColumns = @strColumns + ',' + (CASE @vcLookBackTableName WHEN 'Item' THEN 'I' WHEN 'CompanyInfo' THEN 'cmp' WHEN 'Vendor' THEN 'V' ELSE @vcLookBackTableName END) + '.' + @vcDbColumnName + ' AS ' + ' [' + @vcColumnName + ']' 
					END
				END
			END                              
			ELSE IF @bitCustom = 1 
			BEGIN
				
				SET @vcColumnName= @vcDbColumnName +'~'+ convert(varchar(10),@numFieldId)+'~1'

				SELECT 
					@vcFieldName = FLd_label,
					@vcAssociatedControlType = fld_type,
					@vcDbColumnName = 'Cust' + CONVERT(VARCHAR(10), Fld_Id),
					@Grp_ID = Grp_id
				FROM 
					CFW_Fld_Master
				WHERE 
					CFW_Fld_Master.Fld_Id = @numFieldId                 
        
				IF @Grp_ID = 9
				BEGIN
					SET @strColumns = @strColumns + ',L' + CONVERT(VARCHAR(3), @tintOrder) + '.vcData' + ' [' + @vcColumnName + ']'     
             
					SET @WhereCondition = @WhereCondition + ' left Join ItemAttributes CFW' + CONVERT(VARCHAR(3), @tintOrder)
									+ ' ON CFW' + CONVERT(VARCHAR(3), @tintOrder) + '.numDOmainID=' + CAST(@numDomainID AS VARCHAR) + ' AND CFW' + CONVERT(VARCHAR(3), @tintOrder) + '.Fld_Id='
									+ CONVERT(VARCHAR(10), @numFieldId)
									+ 'and CFW' + CONVERT(VARCHAR(3), @tintOrder)
									+ '.numItemCode=I.numItemCode   '  
					SET @WhereCondition = @WhereCondition
						+ ' left Join ListDetails L'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ ' on L'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ '.numListItemID=CFW'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ '.Fld_Value'      
				END
				ELSE IF @vcAssociatedControlType = 'TextBox' OR @vcAssociatedControlType = 'TextArea' 
				BEGIN
					SET @strColumns = @strColumns + ',CFW'  + CONVERT(VARCHAR(3), @tintOrder)  + '.Fld_Value  [' + @vcColumnName + ']'     
             
					SET @WhereCondition = @WhereCondition + ' left Join CFW_FLD_Values_Item CFW' + CONVERT(VARCHAR(3), @tintOrder)
									+ ' ON CFW' + CONVERT(VARCHAR(3), @tintOrder) + '.Fld_Id='
									+ CONVERT(VARCHAR(10), @numFieldId)
									+ 'and CFW' + CONVERT(VARCHAR(3), @tintOrder)
									+ '.RecId=I.numItemCode   '                                                         
				END   
				ELSE IF @vcAssociatedControlType = 'CheckBox' 
				BEGIN
					SET @strColumns = @strColumns
						+ ',case when isnull(CFW'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ '.Fld_Value,0)=0 then 0 when isnull(CFW'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ '.Fld_Value,0)=1 then 1 end   ['
						+  @vcColumnName
						+ ']'               
 
					SET @WhereCondition = @WhereCondition
						+ ' left Join CFW_FLD_Values_Item CFW'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ ' ON CFW' + CONVERT(VARCHAR(3), @tintOrder) + '.Fld_Id='
						+ CONVERT(VARCHAR(10), @numFieldId)
						+ ' and CFW'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ '.RecId=I.numItemCode   '                                                     
				END                
				ELSE IF @vcAssociatedControlType = 'DateField' 
				BEGIN
					SET @strColumns = @strColumns
						+ ',dbo.FormatedDateFromDate(CFW'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ '.Fld_Value,'
						+ CONVERT(VARCHAR(10), @numDomainId)
						+ ')  [' +@vcColumnName + ']'    
					                  
					SET @WhereCondition = @WhereCondition
						+ ' left Join CFW_FLD_Values_Item CFW'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ ' on CFW' + CONVERT(VARCHAR(3), @tintOrder) + '.Fld_Id='
						+ CONVERT(VARCHAR(10), @numFieldId)
						+ ' and CFW'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ '.RecId=I.numItemCode   '                                                         
				END                
				ELSE IF @vcAssociatedControlType = 'SelectBox' 
				BEGIN
					SET @vcDbColumnName = 'Cust' + CONVERT(VARCHAR(10), @numFieldId)

					SET @strColumns = @strColumns + ',L'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ '.vcData' + ' [' + @vcColumnName + ']'          
					                                                    
					SET @WhereCondition = @WhereCondition
						+ ' left Join CFW_FLD_Values_Item CFW'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ ' on CFW' + CONVERT(VARCHAR(3), @tintOrder) + '.Fld_Id='
						+ CONVERT(VARCHAR(10), @numFieldId)
						+ ' and CFW'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ '.RecId=I.numItemCode    '     
					                                                    
					SET @WhereCondition = @WhereCondition
						+ ' left Join ListDetails L'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ ' on L'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ '.numListItemID=CFW'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ '.Fld_Value'                
				END
				ELSE
				BEGIN
					SET @strColumns = @strColumns + CONCAT(',dbo.GetCustFldValueItem(',@numFieldId,',I.numItemCode)') + ' [' + @vcColumnName + ']'
				END                
			END        
  
			

			SELECT TOP 1
					@tintOrder = tintOrder + 1,
					@vcDbColumnName = vcDbColumnName,
					@vcFieldName = vcFieldName,
					@vcAssociatedControlType = vcAssociatedControlType,
					@vcListItemType = vcListItemType,
					@numListID = numListID,
					@vcLookBackTableName = vcLookBackTableName,
					@bitCustom = bitCustomField,
					@numFieldId = numFieldId,
					@bitAllowSorting = bitAllowSorting,
					@bitAllowEdit = bitAllowEdit,
					@ListRelID = ListRelID
			FROM    #tempForm
			WHERE   tintOrder > @tintOrder - 1 --AND bitCustomField=1
			ORDER BY tintOrder ASC            
 
			IF @@rowcount = 0 
				SET @tintOrder = 0 
		END  

		DECLARE @strSQL AS VARCHAR(MAX)
		SET @strSQL = ' FROM 
							Item I
						LEFT JOIN
							Vendor V
						ON
							I.numItemCode = V.numItemCode
							AND I.numVendorID = V.numVendorID
						LEFT JOIN
							DivisionMaster
						ON
							V.numVendorID = DivisionMaster.numDivisionID
						LEFT JOIN
							CompanyInfo cmp
						ON
							DivisionMaster.numCompanyId = cmp.numCompanyId
						OUTER APPLY
						(
							SELECT
								SUM(numOnHand) AS numOnHand,
								SUM(numBackOrder) AS numBackOrder,
								SUM(numOnOrder) AS numOnOrder,
								SUM(numAllocation) AS numAllocation,
								SUM(numReorder) AS numReorder,
								SUM(numOnHand) * (CASE WHEN ISNULL(I.bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(I.monAverageCost,0) END) AS monStockValue
							FROM
								WareHouseItems
							WHERE
								numItemID = I.numItemCode
						) WarehouseItems ' + @WhereCondition

		IF @columnName = 'OnHand' 
			SET @columnName = 'numOnHand'
		ELSE IF @columnName = 'Backorder' 
			SET @columnName = 'numBackOrder'
		ELSE IF @columnName = 'OnOrder' 
			SET @columnName = 'numOnOrder'
		ELSE IF @columnName = 'OnAllocation' 
			SET @columnName = 'numAllocation'
		ELSE IF @columnName = 'Reorder' 
			SET @columnName = 'numReorder'
		ELSE IF @columnName = 'monStockValue' 
			SET @columnName = 'sum(numOnHand) * (CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(monAverageCost,0) END)'
		ELSE IF @columnName = 'ItemType' 
			SET @columnName = 'charItemType'
		ELSE IF @columnName = 'numItemCode' 
			SET @columnName = 'I.numItemCode'

		DECLARE @column AS VARCHAR(50)              
		SET @column = @columnName
	         
		IF @columnName LIKE '%Cust%' 
		BEGIN
			DECLARE @CustomFieldType AS VARCHAR(10)
			DECLARE @fldId AS VARCHAR(10)
			
			IF CHARINDEX('CFW.Cust',@columnName) > 0
			BEGIN
				SET @fldId = REPLACE(@columnName, 'CFW.Cust', '')
			END
			ELSE
			BEGIN
				SET @fldId = REPLACE(@columnName, 'Cust', '')
			END
			SELECT TOP 1 @CustomFieldType = ISNULL(vcAssociatedControlType,'') FROM View_DynamicCustomColumns WHERE numFieldID = @fldId
            
			IF ISNULL(@CustomFieldType,'') = 'SelectBox' OR ISNULL(@CustomFieldType,'') = 'ListBox'
			BEGIN
				SET @strSQL = @strSQL + ' left Join CFW_FLD_Values_Item CFW on CFW.RecId=i.numItemCode and CFW.fld_id= ' + @fldId + ' ' 
				SET @strSQL = @strSQL + ' left Join ListDetails LstCF on LstCF.numListItemID = CFW.Fld_Value  '            
				SET @columnName = ' LstCF.vcData ' 	
			END
			ELSE
			BEGIN
				SET @strSQL =  @strSQL + ' left Join CFW_FLD_Values_Item CFW on CFW.RecId=i.numItemCode and CFW.fld_id= ' + @fldId + ' '                                                         
				SET @columnName = 'CFW.Fld_Value'                	
			END
		END                                              
                  
		DECLARE @strWhere AS VARCHAR(8000)
		SET @strWhere = CONCAT(' WHERE I.numDomainID = ',@numDomainID)
    
		IF @ItemType = 'P'
		BEGIN
			SET @strWhere = @strWhere + ' AND I.charItemType= ''P''' 
		END
		ELSE IF @ItemType = 'S'
		BEGIN
			SET @strWhere = @strWhere + ' AND I.charItemType= ''S'''
		END
		ELSE IF @ItemType = 'N'    
		BEGIN
			SET @strWhere = @strWhere + ' AND I.charItemType= ''N'''
		END
	    
		IF @bitContainer= '1'
		BEGIN
			SET @strWhere = @strWhere + ' AND I.bitContainer= ' + CONVERT(VARCHAR(2), @bitContainer) + ''  
		END

		IF @bitAssembly = '1' 
		BEGIN
			SET @strWhere = @strWhere + ' AND I.bitAssembly= ' + CONVERT(VARCHAR(2), @bitAssembly) + ''  
		END 
		ELSE IF @ItemType = 'P'
		BEGIN
			SET @strWhere = @strWhere + ' AND ISNULL(I.bitAssembly,0)=0 '
		END

		IF @IsKit = '1' 
		BEGIN
			SET @strWhere = @strWhere + ' AND ISNULL(I.bitAssembly,0)=0 AND I.bitKitParent= ' + CONVERT(VARCHAR(2), @IsKit) + ''  
		END           
		ELSE IF @ItemType = 'P'
		BEGIN
			SET @strWhere = @strWhere + ' AND ISNULL(I.bitKitParent,0)=0 '
		END

		IF @bitSerialized = 1 
		BEGIN
			SET @strWhere = @strWhere + ' AND (I.bitSerialized=1 OR I.bitLotNo=1)'
		END
		--ELSE IF @ItemType = 'P'
		--BEGIN
		--	SET @strWhere = @strWhere + ' AND ISNULL(Item.bitSerialized,0)=0 AND ISNULL(Item.bitLotNo,0)=0 '
		--END

		IF @bitAsset = 1 
		BEGIN
			SET @strWhere = @strWhere + ' AND ISNULL(I.bitAsset,0) = 1'
		END
		ELSE IF @ItemType = 'P'
		BEGIN
			SET @strWhere = @strWhere + ' AND ISNULL(I.bitAsset,0) = 0 '
		END

		IF @bitRental = 1
		BEGIN
			SET @strWhere = @strWhere + ' AND ISNULL(I.bitRental,0) = 1 AND ISNULL(I.bitAsset,0) = 1 '
		END
		ELSE IF @ItemType = 'P'
		BEGIN
			SET @strWhere = @strWhere + ' AND ISNULL(I.bitRental,0) = 0 '
		END

		IF @SortChar <> '0' 
		BEGIN
			SET @strWhere = @strWhere + ' AND I.vcItemName like ''' + @SortChar + '%'''                                       
		END

		IF @ItemClassification <> '0' 
		BEGIN
			SET @strWhere = @strWhere + ' AND I.numItemClassification=' + CONVERT(VARCHAR(15), @ItemClassification)    
		END

		IF @IsArchive = 0 
		BEGIN
			SET @strWhere = @strWhere + ' AND ISNULL(I.IsArchieve,0) = 0'       
		END
        
		IF @numItemGroup > 0 
		BEGIN
			SET @strWhere = @strWhere + ' AND I.numItemGroup = ' + CONVERT(VARCHAR(20), @numItemGroup)  
        END  
		
		IF @numItemGroup = -1 
		BEGIN
			SET @strWhere = @strWhere + ' AND I.numItemGroup in (select numItemGroupID from ItemGroups where numDomainID=' + CONVERT(VARCHAR(20), @numDomainID) + ')'                                      
        END

		IF @vcRegularSearchCriteria<>'' 
		BEGIN
			SET @strWhere = @strWhere +' AND ' + @vcRegularSearchCriteria 
		END
	
		IF @vcCustomSearchCriteria<>'' 
		BEGIN
			SET @strWhere = @strWhere + ' AND ' + @vcCustomSearchCriteria
		END
		
		IF LEN(@SearchQuery) > 0
		BEGIN
			SET @strWhere = @strWhere + ' AND (' + @SearchQuery + ') '
		END

		SET @strWhere = @strWhere + ISNULL(@Where,'')
  
		DECLARE @firstRec AS INTEGER                                        
		DECLARE @lastRec AS INTEGER                                        
		SET @firstRec = ( @CurrentPage - 1 ) * @PageSize                                        
		SET @lastRec = ( @CurrentPage * @PageSize + 1 )

		-- EXECUTE FINAL QUERY
		DECLARE @strFinal AS NVARCHAR(MAX)
		
		IF @CurrentPage = -1 AND @PageSize = -1
		BEGIN
			SET @strFinal = CONCAT('SELECT I.numItemCode, I.vcItemName, I.vcModelID INTO #tempTable ',@strSql + @strWhere,'; SELECT * FROM #tempTable; SELECT @TotalRecords = COUNT(*) FROM #tempTable; DROP TABLE #tempTable;')
		END
		ELSE
		BEGIN
			SET @strFinal = CONCAT('SELECT ROW_NUMBER() OVER(ORDER BY ',@columnName,' ',@columnSortOrder,') RowID,I.numItemCode INTO #temp2 ',@strSql, @strWhere,'; SELECT RowID, ',@strColumns,' INTO #tempTable ',@strSql, ' JOIN #temp2 tblAllItems ON I.numItemCode = tblAllItems.numItemCode ',@strWhere,' AND tblAllItems.RowID > ',@firstRec,' and tblAllItems.RowID <',@lastRec,'; SELECT * FROM  #tempTable; DROP TABLE #tempTable; SELECT @TotalRecords = COUNT(*) FROM #temp2; DROP TABLE #temp2;')
		END

		
		PRINT @strFinal
		exec sp_executesql @strFinal, N'@TotalRecords INT OUT', @TotRecs OUT

	END

	--UPDATE  
	--	#tempForm
 --   SET 
	--	vcDbColumnName = (CASE WHEN bitCustomField = 1 THEN vcFieldName + '~' + vcDbColumnName ELSE vcDbColumnName END)
                                  
	SELECT * FROM #tempForm
    DROP TABLE #tempForm
END
--SELECT * FROM Category
--- Alterd By Chintan Prajapati
-- EXEC dbo.USP_ItemsForECommerce 9,60,1,1,15,0,''
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ItemsForECommerce' ) 
    DROP PROCEDURE USP_ItemsForECommerce
GO
CREATE PROCEDURE [dbo].[USP_ItemsForECommerce]
    @numSiteID AS NUMERIC(9) = 0,
    @numCategoryID AS NUMERIC(18) = 0,
    @numWareHouseID AS NUMERIC(9) = 0,
    @SortBy NVARCHAR(MAX) = 0,
    @CurrentPage INT,
    @PageSize INT,
    @TotRecs INT = 0 OUTPUT,
    @SearchText AS NVARCHAR(MAX) = '',--Comma seperated item ids
    @FilterQuery AS NVARCHAR(MAX) = '',----Comma seperated Attributes Ids
    @FilterRegularCondition AS NVARCHAR(MAX),
    @FilterCustomFields AS NVARCHAR(MAX),
    @FilterCustomWhere AS NVARCHAR(MAX),
	@numDivisionID AS NUMERIC(18,0) = 0
   
AS 
    BEGIN
		
		DECLARE @numDomainID AS NUMERIC(9)
		DECLARE @numDefaultRelationship AS NUMERIC(18,0)
		DECLARE @numDefaultProfile AS NUMERIC(18,0)
		DECLARE @tintPreLoginPriceLevel AS TINYINT

		SELECT  @numDomainID = numDomainID
        FROM    [Sites]
        WHERE   numSiteID = @numSiteID
        
        DECLARE @tintDisplayCategory AS TINYINT
		
		SELECT 
			@tintDisplayCategory = ISNULL(bitDisplayCategory,0)
			,@numDefaultRelationship=numRelationshipId
			,@numDefaultProfile=numProfileId
			,@tintPreLoginPriceLevel=(CASE WHEN @numDivisionID > 0 THEN 0 ELSE ISNULL(tintPreLoginProceLevel,0) END)
		FROM 
			dbo.eCommerceDTL 
		WHERE 
			numSiteID = @numSiteId

		IF ISNULL(@numDivisionID,0) > 0
		BEGIN
			SELECT @numDefaultRelationship=ISNULL(tintCRMType,0),@numDefaultProfile=ISNULL(vcProfile,0) FROM DivisionMaster INNER JOIN CompanyInfo ON DivisionMaster.numCompanyID=CompanyInfo.numCompanyId WHERE numDivisionID=@numDivisionID AND DivisionMaster.numDomainID=@numDomainID
		END
		
		
		CREATE TABLE #tmpItemCat(numCategoryID NUMERIC(18,0))
		CREATE TABLE #tmpItemCode(numItemCode NUMERIC(18,0))
		IF LEN(@FilterCustomFields) > 0 AND LEN(@FilterCustomWhere) > 0
		BEGIN			
			DECLARE @FilterCustomFieldIds AS NVARCHAR(MAX)
			SET @FilterCustomFieldIds = Replace(@FilterCustomFields,'[','')
			SET @FilterCustomFieldIds = Replace(@FilterCustomFieldIds,']','')
			
			DECLARE @strCustomSql AS NVARCHAR(MAX)
			SET @strCustomSql = 'INSERT INTO #tmpItemCode(numItemCode)
								 SELECT DISTINCT T.RecId FROM 
															(
																SELECT * FROM 
																	(
																		SELECT  t2.RecId, t1.fld_id, t2.Fld_Value FROM CFW_Fld_Master t1 
																		JOIN CFW_FLD_Values_Item AS t2 ON t1.Fld_ID = t2.Fld_ID 
																		WHERE t1.Grp_id = 5 
																		AND t1.numDomainID = ' + CONVERT(VARCHAR(10),@numDomainID) + '
																		AND t1.fld_type <> ''Link'' 
																		AND t1.fld_id IN (' + @FilterCustomFieldIds + ')
																	) p  PIVOT (MAX([Fld_Value]) FOR fld_id IN (' + @FilterCustomFields + ')) AS pvt 
															) T  WHERE 1=1 AND ' + @FilterCustomWhere
								 
			PRINT @strCustomSql
			EXEC SP_EXECUTESQL @strCustomSql								 					 
		END
		
        DECLARE @strSQL NVARCHAR(MAX)
        DECLARE @firstRec AS INTEGER
        DECLARE @lastRec AS INTEGER
        DECLARE @Where NVARCHAR(MAX)
        DECLARE @SortString NVARCHAR(MAX)
        DECLARE @searchPositionColumn NVARCHAR(MAX)
        DECLARE @row AS INTEGER
        DECLARE @data AS VARCHAR(100)
        DECLARE @dynamicFilterQuery NVARCHAR(MAX)
        DECLARE @checkFldType VARCHAR(100)
        DECLARE @count NUMERIC(9,0)
        DECLARE @whereNumItemCode NVARCHAR(MAX)
        DECLARE @filterByNumItemCode VARCHAR(100)
        
		SET @SortString = ''
        set @searchPositionColumn = ''
        SET @strSQL = ''
		SET @whereNumItemCode = ''
		SET @dynamicFilterQuery = ''
		 
        SET @firstRec = ( @CurrentPage - 1 ) * @PageSize
        SET @lastRec = ( @CurrentPage * @PageSize + 1 )
                
        DECLARE @bitAutoSelectWarehouse AS BIT
		SELECT @bitAutoSelectWarehouse = ISNULL([ECD].[bitAutoSelectWarehouse],0) FROM [dbo].[eCommerceDTL] AS ECD WHERE [ECD].[numSiteId] = @numSiteID
		AND [ECD].[numDomainId] = @numDomainID

		DECLARE @vcWarehouseIDs AS VARCHAR(1000)
        IF (@numWareHouseID = 0 OR @bitAutoSelectWarehouse = 1)
            BEGIN				
				IF @bitAutoSelectWarehouse = 1
					BEGIN
						SELECT @vcWarehouseIDs = ISNULL( STUFF((SELECT DISTINCT ',' + CAST(numWarehouseID AS VARCHAR(10)) FROM [dbo].[Warehouses] WHERE [Warehouses].[numDomainID] = @numDomainID FOR XML PATH('')),1, 1, '') , '') 
					END
				
				ELSE
					BEGIN
						SELECT  @numWareHouseID = ISNULL(numDefaultWareHouseID,0)
						FROM    [eCommerceDTL]
						WHERE   [numDomainID] = @numDomainID	

						SET @vcWarehouseIDs = @numWareHouseID
					END
            END
		ELSE
		BEGIN
			SET @vcWarehouseIDs = @numWareHouseID
		END

		PRINT @vcWarehouseIDs

		--PRIAMRY SORTING USING CART DROPDOWN
		DECLARE @Join AS NVARCHAR(MAX)
		DECLARE @SortFields AS NVARCHAR(MAX)
		DECLARE @vcSort AS VARCHAR(MAX)

		SET @Join = ''
		SET @SortFields = ''
		SET @vcSort = ''

		IF @SortBy = ''
			BEGIN
				SET @SortString = ' vcItemName Asc '
			END
		ELSE IF CHARINDEX('~0',@SortBy) > 0 OR CHARINDEX('~1',@SortBy) > 0
			BEGIN
				DECLARE @fldID AS NUMERIC(18,0)
				DECLARE @RowNumber AS VARCHAR(MAX)
				
				SELECT * INTO #fldValues FROM dbo.SplitIDsWithPosition(@SortBy,'~')
				SELECT @RowNumber = RowNumber, @fldID = id FROM #fldValues WHERE RowNumber = 1
				SELECT @vcSort = CASE WHEN Id = 0 THEN 'ASC' ELSE 'DESC' END FROM #fldValues WHERE RowNumber = 2
				
				SET @Join = @Join + ' LEFT JOIN CFW_FLD_Values_Item CFVI' + @RowNumber + ' ON I.numItemCode = CFVI' + @RowNumber + '.RecID AND CFVI' + @RowNumber + '.fld_Id = ' + CONVERT(VARCHAR(10),@fldID)
				---SET @SortString = 'CFVI' + @RowNumber + '.fld_Value' 
				SET @SortString = '[fld_Value' + @RowNumber + '] ' --+ @vcSort
				SET @SortFields = ', CFVI' + @RowNumber + '.fld_Value [fld_Value' + @RowNumber + ']'
			END
		ELSE
			BEGIN
				--SET @SortString = @SortBy	
				
				IF CHARINDEX('monListPrice',@SortBy) > 0
				BEGIN
					DECLARE @bitSortPriceMode AS BIT
					SELECT @bitSortPriceMode = ISNULL(bitSortPriceMode,0) FROM dbo.eCommerceDTL WHERE numSiteId = @numSiteID
					PRINT @bitSortPriceMode
					IF @bitSortPriceMode = 1
					BEGIN
						SET @SortString = ' '
					END
					ELSE
					BEGIN
						SET @SortString = @SortBy	
					END
				END
				ELSE
				BEGIN
					SET @SortString = @SortBy	
				END
				
			END	

		--SECONDARY SORTING USING DEFAULT SETTINGS FROM ECOMMERCE
		SELECT ROW_NUMBER() OVER (ORDER BY Custom,vcFieldName) AS [RowID],* INTO #tempSort FROM (
		SELECT  CONVERT(VARCHAR(9),numFieldID)+'~0' AS numFieldID,
				ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName,
				0 AS Custom,
				vcDbColumnName 
		FROM View_DynamicColumns
		WHERE numFormID = 84 
		AND ISNULL(bitSettingField,0) = 1 
		AND ISNULL(bitDeleted,0)=0 
		AND ISNULL(bitCustom,0) = 0
		AND numDomainID = @numDomainID
					       
		UNION     

		SELECT  CONVERT(VARCHAR(9),numFieldID)+'~1' AS numFieldID ,
				vcFieldName ,
				1 AS Custom,
				'' AS vcDbColumnName
		FROM View_DynamicCustomColumns          
		WHERE numFormID = 84
		AND numDomainID = @numDomainID
		AND grp_id = 5 
		AND vcAssociatedControlType = 'TextBox' 
		AND ISNULL(bitCustom,0) = 1 
		AND tintPageType=1  
		) TABLE1 ORDER BY Custom,vcFieldName
		
		--SELECT * FROM #tempSort
		DECLARE @DefaultSort AS NVARCHAR(MAX)
		DECLARE @DefaultSortFields AS NVARCHAR(MAX)
		DECLARE @DefaultSortJoin AS NVARCHAR(MAX)
		DECLARE @intCnt AS INT
		DECLARE @intTotalCnt AS INT	
		DECLARE @strColumnName AS VARCHAR(100)
		DECLARE @bitCustom AS BIT
		DECLARE @FieldID AS VARCHAR(100)				

		SET @DefaultSort = ''
		SET @DefaultSortFields = ''
		SET @DefaultSortJoin = ''		
		SET @intCnt = 0
		SET @intTotalCnt = ( SELECT COUNT(*) FROM #tempSort )
		SET @strColumnName = ''
		
		IF (SELECT ISNULL(bitEnableSecSorting,0) FROM EcommerceDtl WHERE numSiteID = @numSiteID ) = 1
		BEGIN
			CREATE TABLE #fldDefaultValues(RowNumber INT,Id VARCHAR(1000))

			WHILE(@intCnt < @intTotalCnt)
			BEGIN
				SET @intCnt = @intCnt + 1
				SELECT @FieldID = numFieldID, @strColumnName = vcDbColumnName, @bitCustom = Custom FROM #tempSort WHERE RowID = @intCnt
				
				--PRINT @FieldID
				--PRINT @strColumnName
				--PRINT @bitCustom

				IF @bitCustom = 0
					BEGIN
						SET @DefaultSort =  @strColumnName + ',' + @DefaultSort 
					END

				ELSE
					BEGIN
						DECLARE @fldID1 AS NUMERIC(18,0)
						DECLARE @RowNumber1 AS VARCHAR(MAX)
						DECLARE @vcSort1 AS VARCHAR(10)
						DECLARE @vcSortBy AS VARCHAR(1000)
						SET @vcSortBy = CONVERT(VARCHAR(100),@FieldID) + '~' + '0'
						--PRINT @vcSortBy

						INSERT INTO #fldDefaultValues(RowNumber,Id) SELECT RowNumber, id FROM dbo.SplitIDsWithPosition(@vcSortBy,'~')
						SELECT @RowNumber1 = RowNumber + 100 + @intCnt, @fldID1 = id FROM #fldDefaultValues WHERE RowNumber = 1
						SELECT @vcSort1 = 'ASC' 
						
						SET @DefaultSortJoin = @DefaultSortJoin + ' LEFT JOIN CFW_FLD_Values_Item CFVI' + @RowNumber1 + ' ON I.numItemCode = CFVI' + @RowNumber1 + '.RecID AND CFVI' + @RowNumber1 + '.fld_Id = ' + CONVERT(VARCHAR(10),@fldID1)
						SET @DefaultSort = '[fld_Value' + @RowNumber1 + '] ' + ',' + @DefaultSort 
						SET @DefaultSortFields = @DefaultSortFields + ', CFVI' + @RowNumber1 + '.fld_Value [fld_Value' + @RowNumber1 + ']'
					END

			END
			IF LEN(@DefaultSort) > 0
			BEGIN
				SET  @DefaultSort = LEFT(@DefaultSort,LEN(@DefaultSort) - 1)
				SET @SortString = @SortString + ',' 
			END
			--PRINT @SortString
			--PRINT @DefaultSort
			--PRINT @DefaultSortFields
		END
--        IF @SortBy = 1 
--            SET @SortString = ' vcItemName Asc '
--        ELSE IF @SortBy = 2 
--            SET @SortString = ' vcItemName Desc '
--        ELSE IF @SortBy = 3 
--            SET @SortString = ' monListPrice Asc '
--        ELSE IF @SortBy = 4 
--            SET @SortString = ' monListPrice Desc '
--        ELSE IF @SortBy = 5 
--            SET @SortString = ' bintCreatedDate Desc '
--        ELSE IF @SortBy = 6 
--            SET @SortString = ' bintCreatedDate ASC '	
--        ELSE IF @SortBy = 7 --sort by relevance, invokes only when search
--            BEGIN
--                 IF LEN(@SearchText) > 0 
--		            BEGIN
--						SET @SortString = ' SearchOrder ASC ';
--					    set @searchPositionColumn = ' ,SearchItems.RowNumber AS SearchOrder ' 						
--					END
--			END
--        ELSE 
--            SET @SortString = ' vcItemName Asc '
		
                                		
        IF LEN(@SearchText) > 0
			BEGIN
				PRINT 1
				IF CHARINDEX('SearchOrder',@SortString) > 0
				BEGIN
					SET @searchPositionColumn = ' ,SearchItems.RowNumber AS SearchOrder ' 
				END

				SET @Where = ' INNER JOIN dbo.SplitIDsWithPosition('''+ @SearchText +''','','') AS SearchItems 
                        ON SearchItems.Id = I.numItemCode  and SC.numSiteID = ' + CONVERT(VARCHAR(15), @numSiteID) + ' WHERE ISNULL(IsArchieve,0) = 0'
				END

		ELSE IF @SearchText = ''

			SET @Where = ' WHERE 1=2 '
			+ ' and SC.numSiteID = ' + CONVERT(VARCHAR(15), @numSiteID)
			+ ' AND ISNULL(IsArchieve,0) = 0'
		ELSE
			BEGIN
				PRINT 2
				SET @Where = ' WHERE 1=1 '
							+ ' and SC.numSiteID = ' + CONVERT(VARCHAR(15), @numSiteID)
							+ ' AND ISNULL(IsArchieve,0) = 0'


				PRINT @tintDisplayCategory							
				PRINT @Where

				IF @tintDisplayCategory = 1
				BEGIN
					
					;WITH CTE AS(
					SELECT C.numCategoryID,C.tintLevel,C.numDepCategory,CASE WHEN tintLevel=1 THEN 1 ELSE 0 END AS bitChild 
						FROM Category C WHERE (numCategoryID = @numCategoryID OR ISNULL(@numCategoryID,0) = 0) AND numDomainID = @numDomainID
					UNION ALL
					SELECT  C.numCategoryID,C.tintLevel,C.numDepCategory,CTE.bitChild FROM Category C JOIN CTE ON 
					C.numDepCategory=CTE.numCategoryID AND CTE.bitChild=1 AND numDomainID = @numDomainID)
					
					INSERT INTO #tmpItemCat(numCategoryID)												 
					SELECT DISTINCT C.numCategoryID FROM CTE C JOIN dbo.ItemCategory IC ON C.numCategoryID = IC.numCategoryID 
														WHERE numItemID IN ( 
																						
													 						SELECT numItemID FROM WareHouseItems WI
																			INNER JOIN dbo.Item I ON I.numItemCode = WI.numItemID AND I.numDomainID = @numDomainID
																			GROUP BY numItemID 
																			HAVING SUM(WI.numOnHand) > 0 
																			UNION ALL
													 						SELECT numItemID FROM WareHouseItems WI
																			INNER JOIN dbo.Item I ON I.numItemCode = WI.numItemID AND I.numDomainID = @numDomainID
																			GROUP BY numItemID 
																			HAVING SUM(WI.numAllocation) > 0
																		 )		
					SELECT numItemID INTO #tmpOnHandItems FROM 
					(													 					
						SELECT numItemID FROM WareHouseItems WI 
						INNER JOIN dbo.Item I ON I.numItemCode = WI.numItemID AND I.numDomainID = @numDomainID
						GROUP BY numItemID 
						HAVING SUM(WI.numOnHand) > 0
						UNION ALL
						SELECT numItemID FROM WareHouseItems WI 
						INNER JOIN dbo.Item I ON I.numItemCode = WI.numItemID AND I.numDomainID = @numDomainID
						GROUP BY numItemID 
						HAVING SUM(WI.numAllocation) > 0				
					) TABLE1
				END

			END

		       SET @strSQL = @strSQL + ' WITH Items AS 
	                                     ( 
	                                     SELECT   
                                               I.numItemCode,
											   I.bitMatrix,
											   I.numItemGroup,
                                               ROW_NUMBER() OVER ( Partition by I.numItemCode Order by I.numItemCode) RowID,
                                               vcItemName,
                                               I.bintCreatedDate,
                                               vcManufacturer,
                                               C.vcCategoryName,
                                               ISNULL(txtItemDesc,'''') txtItemDesc,' + 
											   (CASE WHEN @tintPreLoginPriceLevel > 0 THEN 'ISNULL(PricingOption.monPrice,ISNULL(CASE WHEN I.[charItemType] = ''P'' THEN ( UOM * ISNULL(W1.[monWListPrice],0)) ELSE (UOM * monListPrice) END, 0))' ELSE 'ISNULL(CASE WHEN I.[charItemType] = ''P'' THEN ( UOM * ISNULL(W1.[monWListPrice],0)) ELSE (UOM * monListPrice) END, 0)' END)
											   +' AS monListPrice,
                                               vcSKU,
                                               fltHeight,
                                               fltWidth,
                                               fltLength,
                                               IM.vcPathForImage,
                                               IM.vcPathForTImage,
                                               vcModelID,
                                               fltWeight,
                                               bitFreeShipping,
                                               UOM AS UOMConversionFactor,
											   UOMPurchase AS UOMPurchaseConversionFactor,
												( CASE WHEN charItemType <> ''S'' AND charItemType <> ''N''
													   THEN ( CASE WHEN bitShowInStock = 1
																   THEN ( CASE WHEN ( ISNULL(W.numOnHand,0)<=0) THEN CASE WHEN ' + CONVERT(VARCHAR(10),@numDomainID) + ' = 172 THEN ''<font color=red>ON HOLD</font>'' ELSE ''<font color=red>Out Of Stock</font>'' END
																			   WHEN bitAllowBackOrder = 1 THEN CASE WHEN ' + CONVERT(VARCHAR(10),@numDomainID) + ' = 172 THEN ''<font color=green>AVAILABLE</font>'' ELSE ''<font color=green>In Stock</font>'' END 
																			   ELSE CASE WHEN ' + CONVERT(VARCHAR(10),@numDomainID) + ' = 172 THEN ''<font color=green>AVAILABLE</font>'' ELSE ''<font color=green>In Stock</font>'' END  
																		  END )
																   ELSE ''''
															  END )
													   ELSE ''''
												  END ) AS InStock, C.vcDescription as CategoryDesc, ISNULL(W1.numWareHouseItemID,0) [numWareHouseItemID] ' 
								               +	@searchPositionColumn + @SortFields + @DefaultSortFields + ',
											   (SELECT COUNT(numProId) 
													FROM 
														PromotionOffer PO
													WHERE 
														numDomainId='+CAST(@numDomainID AS VARCHAR(20))+' 
														AND ISNULL(bitEnabled,0)=1
														AND ISNULL(bitAppliesToSite,0)=1 
														AND ISNULL(bitRequireCouponCode,0)=0
														AND 1 = (CASE WHEN bitRequireCouponCode = 1 THEN CASE WHEN ISNULL(intCouponCodeUsed,0) < ISNULL(tintUsageLimit,0) OR ISNULL(tintUsageLimit,0) = 0  THEN 1 ELSE 0 END ELSE 1 END)
														AND 1 = (CASE WHEN bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN CAST(GETDATE() AS DATE)>=dtValidFrom AND CAST(GETDATE() AS DATE)<=dtValidTo THEN 1 ELSE 0 END) END)
														AND (1 = (CASE 
																	WHEN tintOfferBasedOn = 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=1 AND numValue IN (SELECT numOppAccAttrID FROM ItemGroupsDTL WHERE numItemGroupID IN (SELECT numItemGroupID FROM ItemGroups WHERE numProfileItem=I.numItemCode AND numDomainID='+CAST(@numDomainID AS VARCHAR(20))+' ) UNION SELECT I.numItemCode)) > 0 THEN 1 ELSE 0 END)
																	WHEN tintOfferBasedOn = 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=2 AND numValue=I.numItemClassification) > 0 THEN 1 ELSE 0 END)
																	ELSE 0
																END)
															OR
															1 = (CASE 
																	WHEN tintDiscoutBaseOn = 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=6 AND tintType=1 AND numValue IN (SELECT numOppAccAttrID FROM ItemGroupsDTL WHERE numItemGroupID IN (SELECT numItemGroupID FROM ItemGroups WHERE numProfileItem=I.numItemCode AND numDomainID='+CAST(@numDomainID AS VARCHAR(20))+' ) UNION SELECT I.numItemCode)) > 0 THEN 1 ELSE 0 END)
																	WHEN tintDiscoutBaseOn = 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=6 AND tintType=2 AND numValue=I.numItemClassification) > 0 THEN 1 ELSE 0 END)
																	WHEN tintDiscoutBaseOn = 3 AND tintOfferBasedOn = 1 THEN (CASE WHEN (SELECT COUNT(SimilarItems.numItemCode) FROM SimilarItems WHERE numParentItemCode IN (SELECT numValue FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=1 AND SimilarItems.numItemCode=I.numItemCode))>0 THEN 1 ELSE 0 END)
																	WHEN tintDiscoutBaseOn = 3 AND tintOfferBasedOn = 2 THEN (CASE WHEN (SELECT COUNT(SimilarItems.numItemCode) FROM SimilarItems WHERE SimilarItems.numItemCode IN(I.numItemCode) AND numParentItemCode IN (SELECT numItemCode FROM Item WHERE numItemClassification IN (SELECT numValue FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=2)))>0 THEN 1 ELSE 0 END)
																	ELSE 0
																END)
															)
													)  IsOnSale
								
                                         FROM      
                                                      Item AS I
                                         INNER JOIN   ItemCategory IC   ON   I.numItemCode = IC.numItemID
                                         LEFT JOIN    ItemImages IM     ON   I.numItemCode = IM.numItemCode AND IM.bitdefault=1 AND IM.bitIsImage = 1
                                         INNER JOIN   Category C        ON   IC.numCategoryID = C.numCategoryID
                                         INNER JOIN   SiteCategories SC ON   IC.numCategoryID = SC.numCategoryID ' + 
										 (CASE WHEN @tintPreLoginPriceLevel > 0 THEN ' OUTER APPLY(SELECT decDiscount AS monPrice FROM PricingTable WHERE tintRuleType=3 AND numItemCode=I.numItemCode ORDER BY numPricingID OFFSET ' + CAST(@tintPreLoginPriceLevel-1 AS VARCHAR) + ' ROWS FETCH NEXT 1 ROWS ONLY) As PricingOption ' ELSE '' END)
										 + ' INNER JOIN   eCommerceDTL E    ON   E.numDomainID = I.numDomainID AND E.numSiteId = SC.numSiteID ' + @Join + ' ' + @DefaultSortJoin + ' 
                                         OUTER APPLY (SELECT SUM(numOnHand) numOnHand FROM WareHouseItems W 
													  WHERE  W.numItemID = I.numItemCode
                                                      AND numWareHouseID IN (SELECT [OutParam] FROM dbo.[SplitString](''' + @vcWarehouseIDs + ''','',''))
                                                      AND W.numDomainID = ' + CONVERT(VARCHAR(10),@numDomainID) + ') AS W  
										 OUTER APPLY (SELECT TOP 1 numWareHouseItemID,monWListPrice FROM WareHouseItems W 
													  WHERE  W.numItemID = I.numItemCode 
													  AND W.numDomainID = ' + CONVERT(VARCHAR(10),@numDomainID) + '
													  AND numWareHouseID IN (SELECT [OutParam] FROM dbo.[SplitString](''' + @vcWarehouseIDs + ''','',''))) AS W1
										 OUTER APPLY (SELECT SUM(numAllocation) numAllocation FROM WareHouseItems W2 
													  WHERE  W2.numItemID = I.numItemCode
                                                      AND numWareHouseID IN (SELECT [OutParam] FROM dbo.[SplitString](''' + @vcWarehouseIDs + ''','',''))
                                                      AND W2.numDomainID = ' + CONVERT(VARCHAR(10),@numDomainID) + ') AS W2  			  
										 CROSS APPLY (SELECT dbo.fn_UOMConversion(ISNULL(I.numSaleUnit, 0),I.numItemCode, I.numDomainId,ISNULL(I.numBaseUnit, 0)) AS UOM) AS UOM
										 CROSS APPLY (SELECT dbo.fn_UOMConversion(ISNULL(I.numPurchaseUnit, 0),I.numItemCode, I.numDomainId,ISNULL(I.numBaseUnit, 0)) AS UOMPurchase) AS UOMPurchase '

			IF LEN(@FilterCustomFields) > 0 AND LEN(@FilterCustomWhere) > 0
			BEGIN
				SET @strSQL = @strSQL + ' INNER JOIN #tmpItemCode TP ON TP.numItemCode = I.numItemCode ' 
			END
			
			IF (SELECT COUNT(*) FROM #tmpItemCat) > 0
			BEGIN
				SET @strSQL = @strSQL + ' INNER JOIN #tmpItemCat TC ON TC.numCategoryID = IC.numCategoryID '
				SET @strSQL = @strSQL + ' LEFT JOIN #tmpOnHandItems TOIC ON TOIC.numItemID = I.numItemCode '
			END
			ELSE IF @numCategoryID>0
			BEGIN
				SET @Where = REPLACE(@Where,'WHERE',' WHERE IC.numCategoryID = ' + CONVERT(VARCHAR(10),@numCategoryID) + ' AND ')
			END 
			
			IF @tintDisplayCategory = 1
			BEGIN
				SET @Where = @Where + ' AND 1 = (CASE WHEN (I.charItemType=''P'' AND ISNULL(I.bitKitParent,0) = 0) THEN CASE WHEN (ISNULL(W.numOnHand, 0) > 0 OR ISNULL(W2.numAllocation, 0) > 0) THEN 1 ELSE 0 END ELSE 1 END)'
			END
			
			IF (SELECT COUNT(*) FROM [dbo].[EcommerceRelationshipProfile] WHERE numSiteID=@numSiteID AND numRelationship = @numDefaultRelationship AND numProfile = @numDefaultProfile) > 0
			BEGIN
				SET @Where = @Where + CONCAT(' AND I.numItemClassification NOT IN (SELECT numItemClassification FROM EcommerceRelationshipProfile ERP INNER JOIN ECommerceItemClassification EIC ON EIC.numECommRelatiionshipProfileID=ERP.ID WHERE ERP.numSiteID=',@numSiteID,' AND ERP.numRelationship=',@numDefaultRelationship,' AND ERP.numProfile=', @numDefaultProfile,')')
			END

			--PRINT @Where
			SET @strSQL = @strSQL + @Where
			
			IF LEN(@FilterRegularCondition) > 0
			BEGIN
				SET @strSQL = @strSQL + @FilterRegularCondition
			END                                         
			
---PRINT @SortString + ',' + @DefaultSort + ' ' + @vcSort 
	
            IF LEN(@whereNumItemCode) > 0
                                        BEGIN
								              SET @strSQL = @strSQL + @filterByNumItemCode	
								    	END
            SET @strSQL = @strSQL +     '   ) SELECT * INTO #TEMPItems FROM Items
                                        
									DELETE FROM 
										#TEMPItems
									WHERE 
										numItemCode IN (
															SELECT 
																F.numItemCode
															FROM 
																#TEMPItems AS F
															WHERE 
																ISNULL(F.bitMatrix,0) = 1 
																AND EXISTS (
																			SELECT 
																				t1.vcItemName, t1.bitMatrix, t1.numItemGroup, Count(t1.numItemCode)
																			FROM 
																				#TEMPItems t1
																			WHERE 
																				t1.vcItemName = F.vcItemName
																				AND t1.bitMatrix = 1
																				AND t1.numItemGroup = F.numItemGroup
																			GROUP BY 
																				t1.vcItemName, t1.bitMatrix, t1.numItemGroup
																			HAVING 
																				Count(t1.numItemCode) > 1
																		)
														)
										AND numItemCode NOT IN (
																				SELECT 
																					Min(numItemCode)
																				FROM 
																					#TEMPItems AS F
																				WHERE
																					ISNULL(F.bitMatrix,0) = 1 
																					AND Exists (
																								SELECT 
																									t2.vcItemName, t2.bitMatrix, t2.numItemGroup, Count(t2.numItemCode)
																								FROM 
																									#TEMPItems t2
																								WHERE 
																									t2.vcItemName = F.vcItemName
																								   AND t2.bitMatrix = 1
																								   AND t2.numItemGroup = F.numItemGroup
																								GROUP BY 
																									t2.vcItemName, t2.bitMatrix, t2.numItemGroup
																								HAVING 
																									Count(t2.numItemCode) > 1
																							)
																				GROUP BY 
																					vcItemName, bitMatrix, numItemGroup
																			);  WITH ItemSorted AS (SELECT  ROW_NUMBER() OVER ( ORDER BY  ' + @SortString + @DefaultSort + ' ' + @vcSort + ' ) AS Rownumber,* FROM  #TEMPItems WHERE RowID = 1)'



            DECLARE @fldList AS NVARCHAR(MAX)
			SELECT @fldList=COALESCE(@fldList + ',','') + QUOTENAME(REPLACE(Fld_label,' ','') + '_C') FROM dbo.CFW_Fld_Master WHERE numDomainID=@numDomainID AND Grp_id=5
            
            SET @strSQL = @strSQL +' SELECT  Rownumber,numItemCode,bitMatrix,numItemGroup,txtItemDesc,vcManufacturer,monListPrice,vcItemName,vcCategoryName,vcPathForImage,vcpathForTImage,vcSKU,fltHeight,fltWidth,fltLength,vcModelID,fltWeight,bitFreeShipping,UOMConversionFactor,UOMPurchaseConversionFactor,InStock,CategoryDesc,numWareHouseItemID,IsOnSale
                                     INTO #tempItem FROM ItemSorted ' 

			
            
            SET @strSQL = @strSQL + 'SELECT * INTO #tmpPagedItems FROM #tempItem WHERE Rownumber > ' + CONVERT(VARCHAR(10), @firstRec) + '
                                      AND   Rownumber < ' + CONVERT(VARCHAR(10), @lastRec) + '
                                      order by Rownumber '
                                                                  
            IF LEN(ISNULL(@fldList,''))>0
            BEGIN
				PRINT 'flds1'
				SET @strSQL = @strSQL +'SELECT  Rownumber,numItemCode,txtItemDesc,vcManufacturer,monListPrice,vcItemName,vcCategoryName,vcPathForImage,vcpathForTImage,vcSKU,fltHeight,fltWidth,fltLength,vcModelID,fltWeight,bitFreeShipping,UOMConversionFactor,UOMPurchaseConversionFactor,InStock,CategoryDesc,numWareHouseItemID,IsOnSale,' + @fldList + '
                                        FROM #tmpPagedItems I left join (
			SELECT  t2.RecId, REPLACE(t1.Fld_label,'' '','''') + ''_C'' as Fld_label, 
				CASE t1.fld_type WHEN ''SelectBox'' THEN isnull(dbo.fn_GetListItemName(t2.Fld_Value),'''')
								 WHEN ''CheckBox'' THEN isnull(dbo.fn_getCustomDataValue(t2.Fld_Value,9),'''')
								 WHEN ''DateField'' THEN dbo.FormatedDateFromDate(t2.Fld_Value,' + CAST(@numDomainId AS VARCHAR(18)) +')
								 ELSE Replace(ISNULL(t2.Fld_Value,''0''),''$'',''$$'') end AS Fld_Value FROM CFW_Fld_Master t1 
			JOIN CFW_FLD_Values_Item AS t2 ON t1.Fld_ID = t2.Fld_ID where t1.Grp_id = 5 and t1.numDomainID=' + CAST(@numDomainId AS VARCHAR(18)) +' AND t1.fld_type <> ''Link''
			AND t2.RecId IN (SELECT numItemCode FROM #tmpPagedItems )
			) p PIVOT (MAX([Fld_Value]) FOR Fld_label IN (' + @fldList + ')) AS pvt on I.numItemCode=pvt.RecId order by Rownumber;' 	
			END 
			ELSE
			BEGIN
				PRINT 'flds2'
				SET @strSQL = @strSQL +' SELECT  Rownumber,numItemCode,txtItemDesc,vcManufacturer,monListPrice,vcItemName,vcCategoryName,vcPathForImage,vcpathForTImage,vcSKU,fltHeight,fltWidth,fltLength,vcModelID,fltWeight,bitFreeShipping,UOMConversionFactor,UOMPurchaseConversionFactor,InStock,CategoryDesc,numWareHouseItemID,IsOnSale
                                         FROM  #tmpPagedItems order by Rownumber;'
			END               
                                        
            
--            PRINT(@strSQL)        
--            EXEC ( @strSQL) ; 
			                           
            SET @strSQL = @strSQL + ' SELECT @TotRecs = COUNT(*) FROM #tempItem '                            

			 PRINT  LEN(@strSQL)
			 DECLARE @tmpSQL AS VARCHAR(MAX)
			 SET @tmpSQL = @strSQL
			 PRINT  @tmpSQL
			 
             EXEC sp_executeSQL @strSQL, N'@TotRecs INT OUTPUT', @TotRecs OUTPUT
             
             
        DECLARE @tintOrder AS TINYINT                                                  
		DECLARE @vcFormFieldName AS VARCHAR(50)                                                  
		DECLARE @vcListItemType AS VARCHAR(1)                                             
		DECLARE @vcAssociatedControlType VARCHAR(10)                                                  
		DECLARE @numListID AS NUMERIC(9)                                                  
		DECLARE @WhereCondition VARCHAR(MAX)                       
		DECLARE @numFormFieldId AS NUMERIC  
		DECLARE @vcFieldType CHAR(1)
		                  
		SET @tintOrder=0                                                  
		SET @WhereCondition =''                 
                   
              
		CREATE TABLE #tempAvailableFields(numFormFieldId  NUMERIC(9),vcFormFieldName NVARCHAR(50),
		vcFieldType CHAR(1),vcAssociatedControlType NVARCHAR(50),numListID NUMERIC(18),
		vcListItemType CHAR(1),intRowNum INT,vcItemValue VARCHAR(3000))
		   
		INSERT INTO #tempAvailableFields (numFormFieldId,vcFormFieldName,vcFieldType,vcAssociatedControlType
				,numListID,vcListItemType,intRowNum)                         
		SELECT  Fld_id,REPLACE(Fld_label,' ','') AS vcFormFieldName,
			   CASE GRP_ID WHEN 4 then 'C' WHEN 1 THEN 'D' ELSE 'C' END AS vcFieldType,
				fld_type as vcAssociatedControlType,
				ISNULL(C.numListID, 0) numListID,
				CASE WHEN C.numListID > 0 THEN 'L'
					 ELSE ''
				END vcListItemType,ROW_NUMBER() OVER (ORDER BY Fld_id ASC) AS intRowNum
		FROM    CFW_Fld_Master C LEFT JOIN CFW_Validation V ON V.numFieldID = C.Fld_id
		WHERE   C.numDomainID = @numDomainId
				AND GRP_ID IN (5)

		  
		SELECT * FROM #tempAvailableFields
		
		/*
		DROP TABLE #tempAvailableFields
		DROP TABLE #tmpItemCode
		DROP TABLE #tmpItemCat
		*/	

		IF OBJECT_ID('tempdb..#tempAvailableFields') IS NOT NULL
		DROP TABLE #tempAvailableFields
		
		IF OBJECT_ID('tempdb..#tmpItemCode') IS NOT NULL
		DROP TABLE #tmpItemCode
		
		IF OBJECT_ID('tempdb..#tmpItemCat') IS NOT NULL
		DROP TABLE #tmpItemCat
		
		IF OBJECT_ID('tempdb..#fldValues') IS NOT NULL
		DROP TABLE #fldValues
		
		IF OBJECT_ID('tempdb..#tempSort') IS NOT NULL
		DROP TABLE #tempSort
		
		IF OBJECT_ID('tempdb..#fldDefaultValues') IS NOT NULL
		DROP TABLE #fldDefaultValues
		
		IF OBJECT_ID('tempdb..#tmpOnHandItems') IS NOT NULL
		DROP TABLE #tmpOnHandItems

		IF OBJECT_ID('tempdb..#tmpPagedItems') IS NOT NULL
		DROP TABLE #tmpPagedItems


		IF OBJECT_ID('tempdb..#TEMPItems') IS NOT NULL
		DROP TABLE #TEMPItems
    END


/****** Object:  StoredProcedure [dbo].[USP_LoadAttributesEcommerce]    Script Date: 07/26/2008 16:19:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec USP_LoadAttributes 14,66,'834,837'      
--created by anoop jayaraj      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_loadattributesecommerce')
DROP PROCEDURE usp_loadattributesecommerce
GO
CREATE PROCEDURE [dbo].[USP_LoadAttributesEcommerce]      
@numItemCode as numeric(9)=0,      
@numListID as numeric(9)=0,      
@strAtrr as varchar(1000)='',    
@numWareHouseID as numeric(9)=0      
AS
BEGIN        
	DECLARE @ItemGroup AS NUMERIC(9)      
	DECLARE @strSQL AS VARCHAR(MAX) 
	DECLARE @chrItemType AS VARCHAR(5)
	DECLARE @numDomainID AS NUMERIC(18,0)
	DECLARE @bitMatrix AS BIT
	DECLARE @vcItemName VARCHAR(500)
	
	SELECT 
		@ItemGroup=numItemgroup
		,@chrItemType=charItemType
		,@numDomainID=numDomainID 
		,@bitMatrix=ISNULL(bitMatrix,0)
		,@vcItemName=vcItemName
	FROM
		Item 
	WHERE 
		numItemCode=@numItemCode      
      
      
	IF @strAtrr!=''      
	BEGIN
		DECLARE @Cnt INT = 1
		DECLARE @SplitOn CHAR(1)      
	 
		SET @SplitOn = ','    
		
		IF @bitMatrix = 1
		BEGIN
			SET @strSQL = 'SELECT Item.numItemCode FROM ItemAttributes INNER JOIN Item ON ItemAttributes.numItemCode = Item.numItemCode  WHERE'
		END
		ELSE
		BEGIN  
			SET @strSQL = 'SELECT recid FROM CFW_Fld_Values_Serialized_Items WHERE'  
		END
		    
		While(CHARINDEX(@SplitOn,@strAtrr) > 0)      
		BEGIN      
			IF @Cnt=1 
				SET @strSQL = @strSQL+' fld_value=''' + (ltrim(rtrim(Substring(@strAtrr,1,Charindex(@SplitOn,@strAtrr)-1))))+''''      
			ELSE 
				SET @strSQL=@strSQL+' OR fld_value=''' + (ltrim(rtrim(Substring(@strAtrr,1,Charindex(@SplitOn,@strAtrr)-1))))+''''      

			SET @strAtrr = SUBSTRING(@strAtrr,CHARINDEX(@SplitOn,@strAtrr)+1,len(@strAtrr))      
			
			SET @Cnt = @Cnt + 1      
		End 
		
		IF @bitMatrix = 1
		BEGIN
			IF @Cnt=1 
				SET @strSQL= CONCAT(@strSQL,' fld_value=''',@strAtrr,''' AND ISNULL(bitMatrix,0) = 1 AND ISNULL(Item.numItemGroup,0) = ',@ItemGroup,' AND Item.vcItemName=''',@vcItemName,''' GROUP BY Item.numItemCode HAVING count(*) >',@Cnt-1)      
			ELSE
				SET @strSQL= CONCAT(@strSQL,' OR fld_value=''',@strAtrr,''' AND ISNULL(bitMatrix,0) = 1 AND ISNULL(Item.numItemGroup,0) = ',@ItemGroup,' AND Item.vcItemName=''',@vcItemName,''' GROUP BY Item.numItemCode HAVING count(*) > ', @Cnt-1)
		END
		ELSE
		BEGIN
			IF @Cnt=1 
				SET @strSQL=@strSQL+' fld_value=''' + @strAtrr + ''' GROUP BY recid HAVING count(*) > '+convert(varchar(10), @Cnt-1)      
			ELSE
				SET @strSQL=@strSQL+' OR fld_value=''' + @strAtrr + ''' GROUP BY recid HAVING count(*) > '+convert(varchar(10), @Cnt-1)   
		END   
	END      
    
      
	IF @numItemCode>0      
	BEGIN      
		IF @strAtrr = ''      
		BEGIN
			If @chrItemType <> 'P'
			BEGIN
				SELECT numListItemID,vcData from listdetails where numListID=@numListID AND numDomainID=@numDomainID
			END
			ELSE IF @bitMatrix = 1
			BEGIN
				SELECT 
					numListItemID,vcData 
				FROM 
					ListDetails 
				WHERE 
					numlistitemid IN (
										SELECT 
											DISTINCT(fld_value) 
										FROM 
											Item I
										JOIN
											WareHouseItems W      
										ON
											I.numItemCode=W.numItemID
										JOIN 
											ItemAttributes
										ON 
											I.numItemCode=ItemAttributes.numItemCode      
										JOIN 
											CFW_Fld_Master M 
										ON 
											ItemAttributes.Fld_ID=M.Fld_ID                             
										WHERE 
											I.numDomainID = @numDomainID
											AND W.numDomainID = @numDomainID
											AND M.numListID=@numListID 
											AND vcItemName=@vcItemName
											AND bitMatrix = 1
											AND numItemGroup = @ItemGroup
											AND (W.numWareHouseID=@numWareHouseID OR ISNULL(@numWareHouseID,0)=0)
									)      
			END
			ELSE
			BEGIN
				SELECT 
					numListItemID,vcData 
				FROM 
					ListDetails 
				WHERE 
					numlistitemid IN (
										SELECT 
											DISTINCT(fld_value) 
										FROM 
											WareHouseItems W      
										JOIN 
											CFW_Fld_Values_Serialized_Items CSI 
										ON 
											W.numWareHouseItemID=CSI.RecId      
										JOIN 
											CFW_Fld_Master M 
										ON 
											CSI.Fld_ID=M.Fld_ID                        
										JOIN 
											ItemGroupsDTL 
										ON 
											CSI.Fld_ID=ItemGroupsDTL.numOppAccAttrID      
										WHERE 
											M.numListID=@numListID 
											AND tintType=2 
											AND numItemID=@numItemCode 
											AND W.numWareHouseID=@numWareHouseID
									)      
			END
		END
		ELSE      
		BEGIN      
			If @chrItemType <> 'P'
			BEGIN
				select numListItemID,vcData from listdetails where numListID=@numListID AND numDomainID=@numDomainID
			END
			ELSE IF @bitMatrix = 1
			BEGIN
				PRINT 123
				set @strSQL = CONCAT('SELECT 
											numListItemID,vcData 
										FROM 
											ListDetails 
										WHERE 
											numlistitemid IN (
																SELECT 
																	DISTINCT(fld_value) 
																FROM 
																	Item I
																JOIN
																	WareHouseItems W      
																ON
																	I.numItemCode=W.numItemID
																JOIN 
																	ItemAttributes
																ON 
																	I.numItemCode=ItemAttributes.numItemCode      
																JOIN 
																	CFW_Fld_Master M 
																ON 
																	ItemAttributes.Fld_ID=M.Fld_ID                             
																WHERE 
																	I.numDomainID=',@numDomainID,'
																	AND W.numDomainID =',@numDomainID,'
																	AND M.numListID=',@numListID,'
																	AND vcItemName=''',@vcItemName,'''
																	AND bitMatrix = 1
																	AND numItemGroup =',@ItemGroup,'
																	AND (W.numWareHouseID=',@numWareHouseID ,' OR ' + CAST(ISNULL(@numWareHouseID,0) AS VARCHAR) + ' = 0)
																	AND I.numItemCode IN (',@strSQL,'))')

				Print @strSQL       
				exec (@strSQL) 
			END
			ELSE
			BEGIN
				set @strSQL ='select numListItemID,vcData from listdetails where numlistitemid in(select distinct(fld_value) from WareHouseItems W      
				join CFW_Fld_Values_Serialized_Items CSI on W.numWareHouseItemID=CSI.RecId      
				join CFW_Fld_Master M on CSI.Fld_ID=M.Fld_ID      
				join ItemGroupsDTL on CSI.Fld_ID=ItemGroupsDTL.numOppAccAttrID     
				where  M.numListID='+convert(varchar(20),@numListID)+' and tintType=2 and CSI.bitSerialized=0 and numItemID='+convert(varchar(20),@numItemCode)+' and W.numWareHouseID='+convert(varchar(20),@numWareHouseID)+' and fld_value!=''0'' and fld_value!=''''     
 
				and W.numWareHouseItemID in ('+@strSQL+'))' 
  
				Print @strSQL       
				exec (@strSQL)      
			END
		END
	END     
	ELSE
	BEGIN
		SELECT 0
	END
END
GO
/****** Object:  StoredProcedure [dbo].[USP_LoadWarehouseAttributesEcomm]    Script Date: 07/26/2008 16:19:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec USP_LoadWarehouseAttributes 14,''        
-- exec USP_LoadWarehouseAttributesEcomm @numItemCode='173033',@strAtrr='7270,9708,',@numWareHouseID='63'
--created by anoop jayaraj        
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_loadwarehouseattributesecomm')
DROP PROCEDURE usp_loadwarehouseattributesecomm
GO
CREATE PROCEDURE [dbo].[USP_LoadWarehouseAttributesEcomm]
@numItemCode as varchar(20)='',        
@strAtrr as varchar(1000)='',    
@numWareHouseID as varchar(20)        
AS  
BEGIN
 
	DECLARE @numDomainID AS NUMERIC(18,0)   
       
	DECLARE @strSQL AS VARCHAR(MAX)    
	DECLARE @strAtrrTemp AS VARCHAR(MAX) 
	DECLARE @CharType AS CHAR(1)
	DECLARE @bitMatrix AS BIT
	DECLARE @numItemGroup NUMERIC(18,0)
	DECLARE @vcItemName VARCHAR(500)

    
	SELECT 
		@numDomainID=numDomainID
		,@CharType=charItemType
		,@bitMatrix = ISNULL(bitMatrix,0)
		,@numItemGroup = ISNULL(numItemGroup,0)
		,@vcItemName = vcItemName
	FROM 
		Item 
	WHERE 
		numItemCode=@numItemCode  
  
	SET @strAtrrTemp= @strAtrr       

	IF @strAtrr!=''        
	BEGIN        
		DECLARE @Cnt INT
		DECLARE @vcAttribute AS VARCHAR(100)
	 
		SET @Cnt = 1        
		DECLARE @SplitOn CHAR(1)=','        
	
		IF @bitMatrix = 1
		BEGIN
			SET @strSQL = 'SELECT Item.numItemCode FROM ItemAttributes INNER JOIN Item ON ItemAttributes.numItemCode = Item.numItemCode  WHERE'
		END
		ELSE
		BEGIN  
			SET @strSQL = 'SELECT recid FROM CFW_Fld_Values_Serialized_Items WHERE'  
		END   
	 
		WHILE (Charindex(@SplitOn,@strAtrr)>0)        
		BEGIN
			IF @bitMatrix = 1
			BEGIN
				SET @strSQL=@strSQL + (CASE WHEN @Cnt=1 THEN '' ELSE ' OR ' END) + ' fld_value=CAST(''' + (ltrim(rtrim(Substring(@strAtrr,1,Charindex(@SplitOn,@strAtrr)-1))))+''' AS NUMERIC)'
			END
			ELSE
			BEGIN
				SET @strSQL=@strSQL + (CASE WHEN @Cnt=1 THEN '' ELSE ' OR ' END) + ' fld_value=''' + (ltrim(rtrim(Substring(@strAtrr,1,Charindex(@SplitOn,@strAtrr)-1))))+''''         
			END
			
			SET @strAtrr = SUBSTRING(@strAtrr,CHARINDEX(@SplitOn,@strAtrr)+1,LEN(@strAtrr))
			       
			SET @Cnt = @Cnt + 1        
		End

		IF @bitMatrix = 1
		BEGIN
			SET @strSQL= CONCAT(@strSQL,(CASE WHEN @Cnt=1 THEN '' ELSE ' OR ' END),' fld_value=CAST(''',@strAtrr,''' AS NUMERIC) AND ISNULL(bitMatrix,0) = 1 AND ISNULL(Item.numItemGroup,0) = ',@numItemGroup,' AND Item.vcItemName=''',@vcItemName,'''', ' GROUP BY Item.numItemCode HAVING count(*) > ',@Cnt-1)
		END
		ELSE
		BEGIN
			SET @strSQL=@strSQL + (CASE WHEN @Cnt=1 THEN '' ELSE ' OR ' END) +' fld_value=''' + @strAtrr + ''' GROUP BY recid HAVING count(*) > '+convert(varchar(10), @Cnt-1)  
		END  		    		     
	END 


	IF @strAtrrTemp <> ''
	BEGIN      
		IF @bitMatrix = 1
		BEGIN	
			set @strSQL = 'DECLARE @bitShowInStock AS BIT = 0
						   DECLARE @bitShowQuantity AS BIT = 0   
						   DECLARE @bitAllowBackOrder AS BIT
							
							SELECT 
								@bitShowInStock=isnull(bitShowInStock,0)
								,@bitShowQuantity=isnull(bitShowQOnHand,0) 
							FROM 
								eCommerceDTL 
							WHERE 
								numDomainID='+ CAST(@numDomainID AS VARCHAR) +'
						   
							SELECT @bitAllowBackOrder=bitAllowBackOrder FROM Item Where numItemCode IN (' + @strSQL + ')
						   
							SELECT TOP 1
								numItemID
								,WareHouseItems.numWareHouseItemId
								,ISNULL(sum(numOnHand),0) as numOnHand
								,('+CASE 
									WHEN @CharType<>'S' 
									THEN '(Case 
											when @bitShowInStock=1 then (Case 
																			when @bitAllowBackOrder=1 then ''In Stock'' 
																			else (Case 
																					when isnull(sum(numOnHand),0) > 0 then ''In Stock'' + (Case when @bitShowQuantity=1 then ''(''+convert(varchar(20),isnull(sum(numOnHand),0))+'')'' else '''' end)     
																					else ''<font color=red>Out Of Stock</font>'' 
																					end 
																				) 
																			end
																		)    
											else ''''
											end
										)' 
									ELSE '''' END + ') as InStock
								,monWListPrice  
							FROM 
								WareHouseItems        
							JOIN 
								Warehouses 
							ON 
								Warehouses.numWareHouseID=WareHouseItems.numWareHouseID        
							WHERE 
								numItemID IN ('+ @strSQL +' )
								AND WareHouseItems.numWareHouseID='+@numWareHouseID+' GROUP BY numItemID,numWareHouseItemId,numOnHand,monWListPrice'
		END
		ELSE
		BEGIN
			set @strSQL = '
						   declare @bitShowInStock as bit    
						   declare @bitShowQuantity as bit   
						   declare @bitAllowBackOrder as bit    
							set @bitShowInStock=0    
							set @bitShowQuantity=0  
						   select @bitShowInStock=isnull(bitShowInStock,0),@bitShowQuantity=isnull(bitShowQOnHand,0) from eCommerceDTL where numDomainID='+ CAST(@numDomainID AS VARCHAR) +'
						   select @bitAllowBackOrder=bitAllowBackOrder from item where numItemCode='+@numItemCode+'
						   select numItemID, WareHouseItems.numWareHouseItemId,isnull(sum(numOnHand),0) as numOnHand,('+case when @CharType<>'S' then     
						 '(Case when @bitShowInStock=1 then ( Case when @bitAllowBackOrder=1 then ''In Stock'' else     
						 (Case when isnull(sum(numOnHand),0)>0 then ''In Stock''+(Case when @bitShowQuantity=1 then ''(''+convert(varchar(20),isnull(sum(numOnHand),0))+'')'' else '''' end)     
						 else ''<font color=red>Out Of Stock</font>'' end ) end)    else '''' end)' else '''' end +') as InStock,monWListPrice  from WareHouseItems        
						   join Warehouses on Warehouses.numWareHouseID=WareHouseItems.numWareHouseID        
						   where numItemID='+@numItemCode+' and WareHouseItems.numWareHouseID='+@numWareHouseID+' and numWareHouseItemId in ('+@strSQL+') group by numItemID,numWareHouseItemId,numOnHand,monWListPrice'
		END
	END
	ELSE IF @strAtrrTemp=''        
	BEGIN
		IF @bitMatrix = 1
		BEGIN	
			set @strSQL='
						declare @bitShowInStock as bit    
						declare @bitShowQuantity as bit   
						declare @bitAllowBackOrder as bit    
						set @bitShowInStock=0    
						set @bitShowQuantity=0 
						select @bitShowInStock=isnull(bitShowInStock,0),@bitShowQuantity=isnull(bitShowQOnHand,0) from eCommerceDTL where numDomainID='+ CAST(@numDomainID AS VARCHAR) +'
						select @bitAllowBackOrder=bitAllowBackOrder from item where numItemCode IN (' + @strSQL + ')

						select TOP 1 numItemID, WareHouseItems.numWareHouseItemId,isnull(sum(numOnHand),0) as numOnHand,('+case when @CharType<>'S' then     
						'(Case when @bitShowInStock=1 then ( Case when @bitAllowBackOrder=1 then ''In Stock'' else     
						(Case when isnull(sum(numOnHand),0)>0 then ''In Stock''+(Case when @bitShowQuantity=1 then ''(''+convert(varchar(20),isnull(sum(numOnHand),0))+'')'' else '''' end)     
						else ''<font color=red>Out Of Stock</font>'' end ) end)    else '''' end)' else '''' end +') as InStock,monWListPrice from WareHouseItems        
						join Warehouses on Warehouses.numWareHouseID=WareHouseItems.numWareHouseID        
						where numItemID IN ('+ @strSQL +' )  and WareHouseItems.numWareHouseID='+@numWareHouseID+' group by numItemID,numWareHouseItemId,numOnHand,monWListPrice'
		END
		ELSE 
		BEGIN
			set @strSQL='
						declare @bitShowInStock as bit    
						declare @bitShowQuantity as bit   
						declare @bitAllowBackOrder as bit    
						set @bitShowInStock=0    
						set @bitShowQuantity=0 
						select @bitShowInStock=isnull(bitShowInStock,0),@bitShowQuantity=isnull(bitShowQOnHand,0) from eCommerceDTL where numDomainID='+ CAST(@numDomainID AS VARCHAR) +'
						select @bitAllowBackOrder=bitAllowBackOrder from item where numItemCode='+@numItemCode+'

						select numItemID, WareHouseItems.numWareHouseItemId,isnull(sum(numOnHand),0) as numOnHand,('+case when @CharType<>'S' then     
						'(Case when @bitShowInStock=1 then ( Case when @bitAllowBackOrder=1 then ''In Stock'' else     
						(Case when isnull(sum(numOnHand),0)>0 then ''In Stock''+(Case when @bitShowQuantity=1 then ''(''+convert(varchar(20),isnull(sum(numOnHand),0))+'')'' else '''' end)     
						else ''<font color=red>Out Of Stock</font>'' end ) end)    else '''' end)' else '''' end +') as InStock,monWListPrice from WareHouseItems        
						join Warehouses on Warehouses.numWareHouseID=WareHouseItems.numWareHouseID        
						where numItemID='+@numItemCode +'  and WareHouseItems.numWareHouseID='+@numWareHouseID+' group by numItemID,numWareHouseItemId,numOnHand,monWListPrice'

		END
		   
	END 
	print @strSQL
 exec(@strSQL)              
       
        
	

END
GO

GO
/****** Object:  StoredProcedure [dbo].[usp_ManageAddlContInfo]    Script Date: 04/02/2009 00:17:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Modified By Anoop jayaraj                  
 GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_manageaddlcontinfo')
DROP PROCEDURE usp_manageaddlcontinfo
GO
CREATE PROCEDURE [dbo].[usp_ManageAddlContInfo]                                    
 @numcontactId numeric=0,                                    
 @numContactType numeric=0,                                    
 @vcDepartment numeric(9)=0,                                    
 @vcCategory numeric(9)=0,                                    
 @vcGivenName varchar(100)='',                                    
 @vcFirstName varchar(50)='',                                    
 @vcLastName varchar(50)='',                                    
 @numDivisionId numeric,                                    
 @numPhone varchar(15)='',                                    
 @numPhoneExtension varchar(7)='',                                    
 @numCell varchar(15)='',                                    
 @NumHomePhone varchar(15)='',                                    
 @vcFax varchar(15)='',                                    
 @vcEmail varchar(50)='',                                    
 @VcAsstFirstName varchar(50)='',                                    
 @vcAsstLastName varchar(50)='',                                    
 @numAsstPhone varchar(15)='',                                    
 @numAsstExtn varchar(6)='',                                    
 @vcAsstEmail varchar(50)='',                                                      
 @charSex char(1)='',                                    
 @bintDOB datetime,                                    
 @vcPosition numeric(9)=0,                                                     
 @txtNotes text='',                                                     
 @numUserCntID numeric,                                                                                              
 @numDomainID numeric=1,                                                   
 @vcPStreet varchar(100)='',                                    
 @vcPCity varchar(50)='',                                    
 @vcPPostalCode varchar(15)='',                                    
 @vcPState numeric(9)=0,                                    
 @vcPCountry numeric(9)=0,                  
 @numManagerID numeric=0,        
 @numTeam as numeric(9)=0,        
 @numEmpStatus as numeric(9)=0,
 @vcTitle AS VARCHAR(100)='',
 @bitPrimaryContact AS BIT=0,
 @bitOptOut AS BIT=0,
 @vcLinkedinId VARCHAR(30)=NULL,
 @vcLinkedinUrl VARCHAR(300)=NULL,
 @numECampaignID NUMERIC(18)=0
AS   


 SELECT @vcGivenName = @vcLastName + ', ' + @vcFirstName  + '.eml'                                 
   if @bintDOB ='Jan  1 1753 12:00AM' set  @bintDOB=null                      
IF @numContactId=0
     BEGIN   

IF @bitPrimaryContact = 1 
    BEGIN            
        DECLARE @numID AS NUMERIC(9)            
        DECLARE @bitPrimaryContactCheck AS BIT             
             
        SELECT TOP 1
                @numID = numContactID,
                @bitPrimaryContactCheck = ISNULL(bitPrimaryContact,0)
        FROM    AdditionalContactsInformation
        WHERE   numDivisionID = @numDivisionID 
                   
        IF @@rowcount = 0 
            SET @numID = 0             
        WHILE @numID > 0            
            BEGIN            
                IF @bitPrimaryContactCheck = 1 
                    UPDATE  AdditionalContactsInformation
                    SET     bitPrimaryContact = 0
                    WHERE   numContactID = @numID  
                              
                SELECT TOP 1
                        @numID = numContactID,
                        @bitPrimaryContactCheck = ISNULL(bitPrimaryContact,0)
                FROM    AdditionalContactsInformation
                WHERE   numDivisionID = @numDivisionID
                        AND numContactID > @numID    
                                
                IF @@rowcount = 0 
                    SET @numID = 0             
            END            
    END  
    ELSE IF @bitPrimaryContact = 0
    BEGIN
		IF (SELECT COUNT(*) FROM AdditionalContactsInformation WHERE numDivisionID = @numDivisionID AND numContactId!=@numContactId AND ISNULL(bitPrimaryContact,0)=1)=0
			SET @bitPrimaryContact = 1
    END                 
    
    
declare @bitAutoPopulateAddress bit 
declare @tintPoulateAddressTo tinyint 

select @bitAutoPopulateAddress= isnull(bitAutoPopulateAddress,'0'),@tintPoulateAddressTo=isnull(tintPoulateAddressTo,'0') from Domain where numDomainId = @numDomainId ;
if (@bitAutoPopulateAddress = 1)
	if(@tintPoulateAddressTo = 1) --if primary address is not specified then getting billing address    
	begin
		 
		  select                   
			@vcPStreet = AD1.vcStreet,
			@vcPCity= AD1.vcCity,
			@vcPState= AD1.numState,
			@vcPPostalCode= AD1.vcPostalCode,
			@vcPCountry= AD1.numCountry              
		  from divisionMaster  DM              
		   LEFT JOIN dbo.AddressDetails AD1 ON AD1.numDomainID=DM.numDomainID 
			AND AD1.numRecordID=DM.numDivisionID AND AD1.tintAddressOf=2 AND AD1.tintAddressType=1 AND AD1.bitIsPrimary=1
		  where numDivisionID=@numDivisionId    
	end  
	else if (@tintPoulateAddressTo = 2)-- Primary Address is Shipping Address
	begin
		  select                   
			@vcPStreet= AD2.vcStreet,
			@vcPCity=AD2.vcCity,
			@vcPState=AD2.numState,
			@vcPPostalCode=AD2.vcPostalCode,
			@vcPCountry=AD2.numCountry                   
		  from divisionMaster DM
		  LEFT JOIN dbo.AddressDetails AD2 ON AD2.numDomainID=DM.numDomainID 
		  AND AD2.numRecordID= DM.numDivisionID AND AD2.tintAddressOf=2 AND AD2.tintAddressType=2 AND AD2.bitIsPrimary=1
		  where numDivisionID=@numDivisionId    
	
	end                       
                       
 if (@numEmpStatus is null or @numEmpStatus=0) set @numEmpStatus=658        
 
    INSERT into AdditionalContactsInformation                                    
    (                  
   numContactType,                  
   vcDepartment,                  
   vcCategory,                  
   vcGivenName,                  
   vcFirstName,                  
   vcLastName,                   
   numDivisionId ,                  
   numPhone ,                  
   numPhoneExtension,                  
   numCell ,                  
   numHomePhone ,                          
   vcFax ,                  
   vcEmail ,                  
   VcAsstFirstName ,                  
   vcAsstLastName ,                  
   numAsstPhone ,                  
   numAsstExtn ,                  
   vcAsstEmail  ,                  
   charSex ,                  
   bintDOB ,                  
   vcPosition ,                  
   txtNotes ,                  
   numCreatedBy ,                                    
   bintCreatedDate,             
   numModifiedBy,            
   bintModifiedDate,                   
   numDomainID,                  
   numManagerID,                   
   numRecOwner,                  
   numTeam,                  
   numEmpStatus,
   vcTitle,bitPrimaryContact,bitOptOut,
   vcLinkedinId,vcLinkedinUrl,numECampaignID           
   )                                    
    VALUES                                    
      (                  
   @numContactType,                  
   @vcDepartment,                  
   @vcCategory,                  
   @vcGivenName ,                  
   @vcFirstName ,                  
   @vcLastName,                   
   @numDivisionId ,                  
   @numPhone ,                                    
   @numPhoneExtension,                  
   @numCell ,                  
   @NumHomePhone ,                  
   @vcFax ,                  
   @vcEmail ,                  
   @VcAsstFirstName,                         
   @vcAsstLastName,                  
   @numAsstPhone,                  
   @numAsstExtn,                  
   @vcAsstEmail,                  
   @charSex,                  
   @bintDOB ,                  
   @vcPosition,                                   
   @txtNotes,                  
   @numUserCntID,                  
   getutcdate(),             
   @numUserCntID,                  
   getutcdate(),                   
   @numDomainID,                   
   @numManagerID,                   
   @numUserCntID,                   
   @numTeam,                  
   @numEmpStatus,
   @vcTitle,@bitPrimaryContact,@bitOptOut,
   @vcLinkedinId,@vcLinkedinUrl,@numECampaignID                
   )                                    
                     
 set @numcontactId= SCOPE_IDENTITY()
 
 SELECT @numcontactId             

     INSERT INTO dbo.AddressDetails (
	 	vcAddressName,
	 	vcStreet,
	 	vcCity,
	 	vcPostalCode,
	 	numState,
	 	numCountry,
	 	bitIsPrimary,
	 	tintAddressOf,
	 	tintAddressType,
	 	numRecordID,
	 	numDomainID
	 ) VALUES ('Primary', @vcPStreet,@vcPCity,@vcPPostalCode,@vcPState,@vcPCountry,1,1,0,@numcontactId,@numDomainID)
     
	 IF ISNULL(@numECampaignID,0) > 0 
	 BEGIN
		  --Manage ECampiagn When Campaign is changed from inline edit
		DECLARE @Date AS DATETIME 
		SELECT @Date = dbo.[GetUTCDateWithoutTime]()

 		EXEC [USP_ManageConEmailCampaign]
		@numConEmailCampID = 0, --  numeric(9, 0)
		@numContactID = @numcontactId, --  numeric(9, 0)
		@numECampaignID = @numECampaignID, --  numeric(9, 0)
		@intStartDate = @Date, --  datetime
		@bitEngaged = 1, --  bit
		@numUserCntID = @numUserCntID --  numeric(9, 0)     
	END                                         
                                                 
  END                                    
 ELSE if @numContactId>0                                                        
    BEGIN                                    
    UPDATE AdditionalContactsInformation SET                                    
      numContactType=@numContactType,                                    
      vcGivenName=@vcGivenName,                                    
      vcFirstName=@vcFirstName ,                                    
      vcLastName =@vcLastName ,                   
      numDivisionId =@numDivisionId ,                                    
      numPhone=@numPhone ,                                    
      numPhoneExtension=@numPhoneExtension,                                    
      numCell =@numCell ,                                    
      NumHomePhone =@NumHomePhone ,                                    
      vcFax=@vcFax ,                                    
      vcEmail=@vcEmail ,                                    
      VcAsstFirstName=@VcAsstFirstName,                                    
      vcAsstLastName=@vcAsstLastName,                                    
      numAsstPhone=@numAsstPhone,                                    
      numAsstExtn=@numAsstExtn,                                    
      vcAsstEmail=@vcAsstEmail,                                                      
      charSex=@charSex,                            
      bintDOB=@bintDOB ,                                    
      vcPosition=@vcPosition,                                                    
      txtNotes=@txtNotes,                                                     
      numModifiedBy=@numUserCntID,                                    
      bintModifiedDate=getutcdate(),                                    
      numManagerID=@numManagerID,
      bitPrimaryContact=@bitPrimaryContact                                    
     WHERE numContactId=@numContactId   
                    
     Update dbo.AddressDetails set                   
       vcStreet=@vcPStreet,                                    
       vcCity =@vcPCity,                                    
       vcPostalCode=@vcPPostalCode,                                     
       numState=@vcPState,                                    
       numCountry=@vcPCountry
	  where numRecordID=@numContactId AND bitIsPrimary=1 AND tintAddressOf=1 AND tintAddressType=0
 
     SELECT @numcontactId   
                                                
  END    
  ------Check the Email id Exist into the Email Master Table

DECLARE @numCount AS NUMERIC(18,0)
SET @numCount = 0
IF @vcEmail <> ''
BEGIN 
SELECT @numCount = COUNT(*) FROM dbo.EmailMaster WHERE vcEmailID =@vcEmail and numDomainId=@numDomainID
IF @numCount = 0 
BEGIN
	INSERT INTO EmailMaster(vcEmailId,vcName,numDomainId,numContactId)
	VALUES(@vcEmail,@vcFirstName,@numDomainID,@numcontactId)
END
else
begin
	update EmailMaster set numContactId=@numcontactId where vcEmailID =@vcEmail and numDomainId=@numDomainID
end
END 
--IF @vcAsstEmail <> ''
--BEGIN
--SELECT @numCount = COUNT(*) FROM dbo.EmailMaster WHERE vcEmailID =@vcAsstEmail
--IF @numCount = 0 
--BEGIN
--	INSERT INTO EmailMaster(vcEmailId,vcName,numDomainId,numContactId)
--	VALUES(@vcAsstEmail,@vcFirstName,@numDomainID,@numcontactId)
--END
--END
---------------------------------------------------------

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageCommissionRule')
DROP PROCEDURE USP_ManageCommissionRule
GO
CREATE PROCEDURE [dbo].[USP_ManageCommissionRule]
    @numComRuleID NUMERIC,
    @vcCommissionName VARCHAR(100),
    @tintComAppliesTo TINYINT,
    @tintComBasedOn TINYINT,
    @tinComDuration TINYINT,
    @tintComType TINYINT,
    @numDomainID NUMERIC,
    @strItems TEXT,
    @tintAssignTo TINYINT,
    @tintComOrgType TINYINT
AS 
BEGIN TRY
BEGIN TRANSACTION
    IF @numComRuleID = 0 
    BEGIN
		INSERT  INTO CommissionRules
				(
					[vcCommissionName],
					[numDomainID]
				)
		VALUES  (
					@vcCommissionName,
					@numDomainID
				)
		SET @numComRuleID=@@identity
		Select @numComRuleID;
    END
    ELSE 
    BEGIN		
		UPDATE  
				CommissionRules
            SET     
				[vcCommissionName] = @vcCommissionName,
                [tintComBasedOn] = @tintComBasedOn,
                [tinComDuration] = @tinComDuration,
                [tintComType] = @tintComType,
				[tintComAppliesTo] = @tintComAppliesTo,
				[tintComOrgType] = @tintComOrgType,
				[tintAssignTo] = @tintAssignTo
            WHERE   
				[numComRuleID] = @numComRuleID 
				AND [numDomainID] = @numDomainID

		IF @tintComAppliesTo = 3
		BEGIN
			DELETE FROM CommissionRuleItems WHERE numComRuleID=@numComRuleID
		END
				
		DELETE FROM [CommissionRuleDtl] WHERE [numComRuleID] = @numComRuleID
		DELETE FROM [CommissionRuleContacts] WHERE [numComRuleID] = @numComRuleID

		DECLARE @hDocItem INT
		EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems
        
		IF CONVERT(VARCHAR(10), @strItems) <> '' 
        BEGIN
            INSERT  INTO [CommissionRuleDtl]
                    (
                        numComRuleID,
                        [intFrom],
                        [intTo],
                        [decCommission]
                    )
                    SELECT  @numComRuleID,
                            X.[intFrom],
                            X.[intTo],
                            X.[decCommission]
                    FROM    ( SELECT    *
                                FROM      OPENXML (@hDocItem, '/NewDataSet/CommissionTable', 2)
                                        WITH ( intFrom DECIMAL(18,2), intTo DECIMAL(18,2), decCommission DECIMAL(18,2) )
                            ) X  
            
			INSERT  INTO [CommissionRuleContacts]
                    (
                        numComRuleID,
                        numValue,bitCommContact
                    )
                    SELECT  @numComRuleID,
                            X.[numValue],X.[bitCommContact]
                    FROM    ( SELECT    *
                                FROM      OPENXML (@hDocItem, '/NewDataSet/ContactTable', 2)
                                        WITH ( numValue NUMERIC,bitCommContact bit)
                            ) X
			
            EXEC sp_xml_removedocument @hDocItem
        END
        

		DECLARE @bitItemDuplicate AS BIT = 0
		DECLARE @bitOrgDuplicate AS BIT = 0
		DECLARE @bitContactDuplicate AS BIT = 0

		/*Duplicate Rule values checking */
		-- CHECK IF OTHER COMMISSION RULE IS EXISTS IN ACCOUNT WIRH SAME OPTION IN SETP 2,3,4 and 5
		IF (SELECT COUNT(*) FROM CommissionRules WHERE numDomainID=@numDomainID AND tintComAppliesTo=@tintComAppliesTo AND tintComOrgType=@tintComOrgType AND tintAssignTo = @tintAssignTo AND numComRuleID <> @numComRuleID) > 0
		BEGIN
			-- ITEM
			IF @tintComAppliesTo = 1 OR @tintComAppliesTo = 2
			BEGIN
				-- ITEM ID OR ITEM CLASSIFICATION
				IF (SELECT COUNT(*) FROM CommissionRuleItems WHERE numValue IN (SELECT ISNULL(numValue,0) FROM CommissionRuleItems WHERE numComRuleID=@numComRuleID) AND numComRuleID IN (SELECT ISNULL(numComRuleID,0) FROM CommissionRules WHERE numDomainID=@numDomainID AND numComRuleID <> @numComRuleID)) > 0
				BEGIN
					SET @bitItemDuplicate = 1
				END
			END
			ELSE IF @tintComAppliesTo = 3
			BEGIN
				-- ALL ITEMS
				SET @bitItemDuplicate = 1
			END

			--ORGANIZATION
			IF @tintComOrgType = 1
			BEGIN
				-- ORGANIZATION ID
				IF (SELECT COUNT(*) FROM CommissionRuleOrganization WHERE numValue IN (SELECT ISNULL(numValue,0) FROM CommissionRuleOrganization WHERE numComRuleID=@numComRuleID) AND numComRuleID IN (SELECT ISNULL(numComRuleID,0) FROM CommissionRules WHERE numDomainID=@numDomainID AND numComRuleID <> @numComRuleID)) > 0
				BEGIN
					SET @bitOrgDuplicate = 1
				END
			END
			ELSE IF @tintComOrgType = 2
			BEGIN
				-- ORGANIZATION RELATIONSHIP OR PROFILE
				IF (SELECT COUNT(*) FROM CommissionRuleOrganization WHERE numValue IN (SELECT ISNULL(numValue,0) FROM CommissionRuleOrganization WHERE numComRuleID=@numComRuleID) AND numProfile IN (SELECT ISNULL(numProfile,0) FROM CommissionRuleOrganization WHERE numComRuleID=@numComRuleID) AND numComRuleID IN (SELECT ISNULL(numComRuleID,0) FROM CommissionRules WHERE numDomainID=@numDomainID AND numComRuleID <> @numComRuleID)) > 0
				BEGIN
					SET @bitOrgDuplicate = 1
				END
			END
			ELSE IF  @tintComOrgType = 3
			BEGIN
				-- ALL ORGANIZATIONS
				SET @bitOrgDuplicate = 1
			END

			--CONTACT
			IF (
				SELECT 
					COUNT(*) 
				FROM 
					CommissionRuleContacts 
				WHERE 
					numValue IN (SELECT ISNULL(numValue,0) FROM CommissionRuleContacts WHERE numComRuleID=@numComRuleID) 
					AND numComRuleID IN (SELECT ISNULL(numComRuleID,0) FROM CommissionRules WHERE numDomainID=@numDomainID AND numComRuleID <> @numComRuleID)) > 0
			BEGIN
				SET @bitContactDuplicate = 1
			END
		END

		IF (ISNULL(@bitItemDuplicate,0) = 1 AND ISNULL(@bitOrgDuplicate,0) = 1 AND ISNULL(@bitContactDuplicate,0) = 1)
		BEGIN
			RAISERROR ( 'DUPLICATE',16, 1 )
			RETURN ;
		END

		SELECT  @numComRuleID
    END
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
/****** Object:  StoredProcedure [dbo].[USP_ManageDivisions]    Script Date: 07/26/2008 16:19:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Modified by anoop jayaraj                                  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_managedivisions')
DROP PROCEDURE usp_managedivisions
GO
CREATE PROCEDURE [dbo].[USP_ManageDivisions]                                                        
 @numDivisionID  numeric=0,                                                         
 @numCompanyID  numeric=0,                                                         
 @vcDivisionName  varchar (100)='',                                                        
 @vcBillStreet  varchar (50)='',                                                        
 @vcBillCity   varchar (50)='',                                                        
 @vcBillState  NUMERIC=0,                                                        
 @vcBillPostCode  varchar (15)='',                                                        
 @vcBillCountry  numeric=0,                                                        
 @bitSameAddr  bit=0,                                                        
 @vcShipStreet  varchar (50)='',                                                        
 @vcShipCity   varchar (50)='',                                                        
 @vcShipState  varchar (50)='',                                                        
 @vcShipPostCode  varchar (15)='',                                                        
 @vcShipCountry  varchar (50)='',                                                        
 @numGrpId   numeric=0,                                                                                          
 @numTerID   numeric=0,                                                        
 @bitPublicFlag  bit=0,                                                        
 @tintCRMType  tinyint=0,                                                        
 @numUserCntID  numeric=0,                                                                                                                                                              
 @numDomainID  numeric=0,                                                        
 @bitLeadBoxFlg  bit=0,  --Added this because when called form leadbox this value will be 1 else 0                                                        
 @numStatusID  numeric=0,                                                      
 @numCampaignID numeric=0,                                          
 @numFollowUpStatus numeric(9)=0,                                                                                  
 @tintBillingTerms as tinyint,                                                        
 @numBillingDays as numeric(18,0),                                                       
 @tintInterestType as tinyint,                                                        
 @fltInterest as float,                                        
 @vcComFax as varchar(50)=0,                          
 @vcComPhone as varchar(50)=0,                
 @numAssignedTo as numeric(9)=0,    
 @bitNoTax as bit,
 @UpdateDefaultTax as bit=1,
@numCompanyDiff as numeric(9)=0,
@vcCompanyDiff as varchar(100)='',
@numCurrencyID	AS NUMERIC(9,0),
@numAccountClass AS NUMERIC(18,0) = 0,
@numBillAddressID AS NUMERIC(18,0) = 0,
@numShipAddressID AS NUMERIC(18,0) = 0,
@vcPartenerSource AS VARCHAR(300)=0,              
@vcPartenerContact AS VARCHAR(300)=0,
@numPartner AS NUMERIC(18,0) = 0
AS   

DECLARE @numPartenerSource NUMERIC(18,0)      
DECLARE @numPartenerContact NUMERIC(18,0)     


IF ISNULL(@numPartner,0) = 0
BEGIN
	--Added By Prasanta Pradhan
	--Get Partenr Source and Code by Email and Partenr Code
	SET @numPartenerSource=(SELECT TOP 1 numDivisionID FROM DivisionMaster WHERE numDomainId=@numDomainID AND vcPartnerCode=@vcPartenerSource)
END
ELSE 
BEGIN
	SET @numPartenerSource = @numPartner
END


SET @numPartenerContact=(SELECT Top 1 numContactId FROM AdditionalContactsInformation WHERE numDomainId=@numDomainID AND numDivisionId=@numPartenerSource AND vcEmail=@vcPartenerContact)
                                 
 -- Added By Manish Anjara As On : 21st Jam,2013
 -- Check whether GroupID Or tintCRMType is not passed then we need to add record as prospect (means numGroupID = 0, tintCRMType = 1)
 IF ISNULL(@numGrpId,0) = 0 AND ISNULL(@tintCRMType,0) = 0
 BEGIN
 	SET @numGrpId = 0
 	SET @tintCRMType = 1
 END
                                                                                                               
 IF @numDivisionId is null OR @numDivisionId=0                                                 
 BEGIN    
 if @UpdateDefaultTax=1 set @bitNoTax=0
                                                                                  
    INSERT INTO DivisionMaster                                    
     (                      
      numCompanyID,                      
      vcDivisionName,                      
      numGrpId,                      
      numFollowUpStatus,               
      bitPublicFlag,                      
      numCreatedBy,                      
      bintCreatedDate,                                    
      numModifiedBy,                      
      bintModifiedDate,                      
      tintCRMType,                      
      numDomainID,                                    
      bitLeadBoxFlg,                      
      numTerID,                     
      numStatusID,                                    
      numRecOwner,                              
      tintBillingTerms,                                    
      numBillingDays,                      
      tintInterestType,                      
      fltInterest,                      
      numCampaignID,            
      vcComPhone,            
      vcComFax,    
   bitNoTax,numCompanyDiff,vcCompanyDiff,bitActiveInActive,numCurrencyID,numAccountClassID,numPartenerSource,numPartenerContact        
 )                                    
     VALUES                      
    (                      
    @numCompanyID,                       
    @vcDivisionName,                       
                      
    @numGrpId,                       
    @numFollowUpStatus,                               
    @bitPublicFlag,                       
    @numUserCntID,                       
    getutcdate(),                       
    @numUserCntID,                       
    getutcdate(),                       
    @tintCRMType,                       
    @numDomainID,                       
    @bitLeadBoxFlg,                       
    @numTerID,                       
    @numStatusID,                                                        
    @numUserCntID,                                
    0,                      
    @numBillingDays,                      
    0,                      
    0,                      
    @numCampaignID,            
    @vcComPhone,            
    @vcComFax,    
    @bitNoTax,@numCompanyDiff,@vcCompanyDiff,1,@numCurrencyID,@numAccountClass,@numPartenerSource,@numPartenerContact                         
  )                                  
	SELECT @numDivisionID = SCOPE_IDENTITY()

	IF @numBillAddressID > 0 OR @numShipAddressID > 0
	BEGIN
		IF @numBillAddressID > 0
		BEGIN
			UPDATE AddressDetails SET numRecordID=@numDivisionID WHERE numAddressID=@numBillAddressID
		END

		IF @numShipAddressID > 0
		BEGIN
			UPDATE AddressDetails SET numRecordID=@numDivisionID WHERE numAddressID=@numShipAddressID
		END
	END
	ELSE
	BEGIN
	  --added By Sachin Sadhu||Issued Founded on:1stFeb14||By:JJ-AudioJenex
		IF EXISTS(SELECT 'col1' FROM dbo.AddressDetails WHERE  bitIsPrimary=1 AND numDomainID=@numDomainID AND numRecordID=@numDivisionID )
        BEGIN
			INSERT INTO dbo.AddressDetails 
			(
				vcAddressName,
				vcStreet,
				vcCity,
				vcPostalCode,
				numState,
				numCountry,
				bitIsPrimary,
				tintAddressOf,
				tintAddressType,
				numRecordID,
				numDomainID
			) 
			SELECT 'Address1',@vcBillStreet,@vcBillCity,@vcBillPostCode,@vcBillState,@vcBillCountry,0,2,1,@numDivisionID,@numDomainID
			union
			SELECT 'Address1',@vcShipStreet,@vcShipCity,@vcShipPostCode,@vcShipState,@vcShipCountry,0,2,2,@numDivisionID,@numDomainID
        END
		ELSE
		BEGIN
			INSERT INTO dbo.AddressDetails 
			(
				vcAddressName,
				vcStreet,
				vcCity,
				vcPostalCode,
				numState,
				numCountry,
				bitIsPrimary,
				tintAddressOf,
				tintAddressType,
				numRecordID,
				numDomainID
			) 
			SELECT 'Address1',@vcBillStreet,@vcBillCity,@vcBillPostCode,@vcBillState,@vcBillCountry,1,2,1,@numDivisionID,@numDomainID
			union
			SELECT 'Address1',@vcShipStreet,@vcShipCity,@vcShipPostCode,@vcShipState,@vcShipCountry,1,2,2,@numDivisionID,@numDomainID
		END
	END
       --End of Sachin Script                   
     



  if @UpdateDefaultTax=1 
  begin
   insert into DivisionTaxTypes
   select @numDivisionID,numTaxItemID,1 from TaxItems
   where numDomainID=@numDomainID
   union
   select @numDivisionID,0,1 
  end                                                


	                             
	DECLARE @numGroupID NUMERIC
	SELECT TOP 1 @numGroupID=numGroupID FROM dbo.AuthenticationGroupMaster WHERE numDomainID=@numDomainID AND tintGroupType=2
	IF @numGroupID IS NULL SET @numGroupID = 0 
	insert into ExtarnetAccounts (numCompanyID,numDivisionID,numGroupID,numDomainID)                                                
      values(@numCompanyID,@numDivisionID,@numGroupID,@numDomainID)                                                                      
                                                                          
     SELECT @numDivisionID                                                  
                                                   
 END                                                        
 ELSE if @numDivisionId>0                                                      
 BEGIN                                                                    
   --Inserting into Follow up History                                       
    declare @numFollow as varchar(10)                                        
    declare @binAdded as varchar(20)                                        
    declare @PreFollow as varchar(10)                                        
    declare @RowCount as varchar(2)                                        
    set @PreFollow=(select count(*)  numFollowUpStatus from FollowUpHistory where numDivisionID= @numDivisionID)                                        
    set @RowCount=@@rowcount                                        
    select   @numFollow=numFollowUpStatus,   @binAdded=bintModifiedDate from divisionmaster where numDivisionID=@numDivisionID                                         
    if @numFollow <>'0' and @numFollow <> @numFollowUpStatus                                        
     begin                                        
   select top 1  numFollowUpStatus from FollowUpHistory where numDivisionID= @numDivisionID order by numFollowUpStatusID desc                                        
                                         
  if @PreFollow<>0                                        
  begin                                        
                                         
   if @numFollow <> (select top 1  numFollowUpStatus from FollowUpHistory where numDivisionID= @numDivisionID order by numFollowUpStatusID desc)                                        
      begin                                        
                                          
            insert into FollowUpHistory(numFollowUpstatus,numDivisionID,bintAddedDate)                                        
                   values(@numFollow,@numDivisionID,@binAdded)                                        
          end                                        
  end                              
  else                                        
  begin                                        
                                         
  insert into FollowUpHistory(numFollowUpstatus,numDivisionID,bintAddedDate)                                        
                   values(@numFollow,@numDivisionID,@binAdded)                                   
  end                                        
                                     
  end                                        
    -----                                                      
                                                        
   UPDATE DivisionMaster                       
  SET                        
   numCompanyID = @numCompanyID ,                     
   vcDivisionName = @vcDivisionName,                                                        
     numGrpId = @numGrpId,                                                                       
     numFollowUpStatus =@numFollowUpStatus,                                                        
     numTerID = @numTerID,                                                   
     bitPublicFlag =@bitPublicFlag,                                                        
     numModifiedBy = @numUserCntID,                                                  
     bintModifiedDate = getutcdate(),                                                     
     tintCRMType = @tintCRMType,                                                      
     numStatusID = @numStatusID,                                                 
     tintBillingTerms = @tintBillingTerms,                                                      
     numBillingDays = @numBillingDays,                                                     
     tintInterestType = @tintInterestType,                                                       
     fltInterest =@fltInterest,                          
     vcComPhone=@vcComPhone,                          
     vcComFax= @vcComFax,                      
     numCampaignID=@numCampaignID,    
     bitNoTax=@bitNoTax,numCompanyDiff=@numCompanyDiff,vcCompanyDiff=@vcCompanyDiff,numCurrencyID = @numCurrencyID,numPartenerSource=@numPartenerSource,numPartenerContact=@numPartenerContact                                                         
   WHERE numDivisionID = @numDivisionID       

    Update dbo.AddressDetails set
       vcStreet=@vcBillStreet,                                    
       vcCity =@vcBillCity,                                    
       vcPostalCode=@vcBillPostCode,                                     
       numState=@vcBillState,                                    
       numCountry=@vcBillCountry
	  WHERE numDomainID=@numDomainID and numRecordID=@numDivisionID AND bitIsPrimary=1 AND tintAddressOf=2 AND tintAddressType=1         
	   Update dbo.AddressDetails set
       vcStreet=@vcShipStreet,                                    
       vcCity =@vcShipCity,                                    
       vcPostalCode=@vcShipPostCode,                                     
       numState=@vcShipState,                                    
       numCountry=@vcShipCountry
	  WHERE numDomainID=@numDomainID and numRecordID=@numDivisionID AND bitIsPrimary=1 AND tintAddressOf=2 AND tintAddressType=2         
	  
             
  end 

    ---Updating if organization is assigned to someone                
  declare @tempAssignedTo as numeric(9)              
  set @tempAssignedTo=null               
  select @tempAssignedTo=isnull(numAssignedTo,0) from DivisionMaster where numDivisionID = @numDivisionID                
print @tempAssignedTo              
  if (@tempAssignedTo<>@numAssignedTo and  @numAssignedTo<>'0')              
  begin                
    update DivisionMaster set numAssignedTo=@numAssignedTo ,numAssignedBy=@numUserCntID where numDivisionID = @numDivisionID                
  end               
  else if  (@numAssignedTo =0)              
  begin              
   update DivisionMaster set numAssignedTo=0 ,numAssignedBy=0 where numDivisionID = @numDivisionID              
                  
                
                                                                      
   SELECT @numDivisionID                                                   
 END
GO
/****** Object:  StoredProcedure [dbo].[USP_ManageItemsAndKits]    Script Date: 02/15/2010 21:44:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Anoop Jayaraj                                                          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_manageitemsandkits')
DROP PROCEDURE usp_manageitemsandkits
GO
CREATE PROCEDURE [dbo].[USP_ManageItemsAndKits]                                                                
@numItemCode as numeric(9) output,                                                                
@vcItemName as varchar(300),                                                                
@txtItemDesc as varchar(1000),                                                                
@charItemType as char(1),                                                                
@monListPrice as money,                                                                
@numItemClassification as numeric(9),                                                                
@bitTaxable as bit,                                                                
@vcSKU as varchar(50),                                                                                                                                      
@bitKitParent as bit,                                                                                   
--@dtDateEntered as datetime,                                                                
@numDomainID as numeric(9),                                                                
@numUserCntID as numeric(9),                                                                                                  
@numVendorID as numeric(9),                                                            
@bitSerialized as bit,                                               
@strFieldList as text,                                            
@vcModelID as varchar(200)='' ,                                            
@numItemGroup as numeric(9)=0,                                      
@numCOGSChartAcntId as numeric(9)=0,                                      
@numAssetChartAcntId as numeric(9)=0,                                      
@numIncomeChartAcntId as numeric(9)=0,                            
@monAverageCost as money,                          
@monLabourCost as money,                  
@fltWeight as float,                  
@fltHeight as float,                  
@fltLength as float,                  
@fltWidth as float,                  
@bitFreeshipping as bit,                
@bitAllowBackOrder as bit,              
@UnitofMeasure as varchar(30)='',              
@strChildItems as text,            
@bitShowDeptItem as bit,              
@bitShowDeptItemDesc as bit,        
@bitCalAmtBasedonDepItems as bit,    
@bitAssembly as BIT,
@intWebApiId INT=null,
@vcApiItemId AS VARCHAR(50)=null,
@numBarCodeId as VARCHAR(25)=null,
@vcManufacturer as varchar(250)=NULL,
@numBaseUnit AS NUMERIC(18)=0,
@numPurchaseUnit AS NUMERIC(18)=0,
@numSaleUnit AS NUMERIC(18)=0,
@bitLotNo as BIT,
@IsArchieve AS BIT,
@numItemClass as numeric(9)=0,
@tintStandardProductIDType AS TINYINT=0,
@vcExportToAPI VARCHAR(50),
@numShipClass NUMERIC,
@ProcedureCallFlag SMALLINT =0,
@vcCategories AS VARCHAR(2000) = '',
@bitAllowDropShip AS BIT=0,
@bitArchiveItem AS BIT = 0,
@bitAsset AS BIT = 0,
@bitRental AS BIT = 0,
@numCategoryProfileID AS NUMERIC(18,0) = 0,
@bitVirtualInventory BIT,
@bitContainer BIT,
@numContainer NUMERIC(18,0)=0,
@numNoItemIntoContainer NUMERIC(18,0),
@bitMatrix BIT = 0 ,
@vcItemAttributes VARCHAR(2000) = ''
AS
BEGIN     
BEGIN TRY
BEGIN TRANSACTION
	DECLARE @bitOppOrderExists AS BIT = 0

	IF ISNULL(@numItemCode,0) > 0
	BEGIN
		IF (SELECT COUNT(OI.numOppId) FROM dbo.OpportunityItems OI WHERE OI.numItemCode =@numItemCode) > 0
		BEGIN
			SET @bitOppOrderExists = 1
		END
		ELSE IF (SELECT COUNT(OI.numOppId) FROM dbo.OpportunityKitItems OI WHERE OI.numChildItemID =@numItemCode) > 0
		BEGIN
			SET @bitOppOrderExists = 1
		END
		ELSE IF (SELECT COUNT(OI.numOppId) FROM dbo.OpportunityKitChildItems OI WHERE OI.numItemID =@numItemCode) > 0
		BEGIN
			SET @bitOppOrderExists = 1
		END
	END

	--INSERT CUSTOM FIELDS BASE ON WAREHOUSEITEMS
	DECLARE @hDoc AS INT     
	                                                                        	
	DECLARE @TempTable TABLE 
	(
		ID INT IDENTITY(1,1),
		Fld_ID NUMERIC(18,0),
		Fld_Value NUMERIC(18,0)
	)   

	IF ISNULL(@bitMatrix,0) = 1 AND ISNULL(@numItemGroup,0) = 0
	BEGIN
		RAISERROR('ITEM_GROUP_NOT_SELECTED',16,1)
		RETURN
	END
	ELSE IF ISNULL(@bitMatrix,0) = 1 AND LEN(ISNULL(@vcItemAttributes,'')) = 0 
	BEGIN
		RAISERROR('ITEM_ATTRIBUTES_NOT_SELECTED',16,1)
		RETURN
	END
	ELSE IF ISNULL(@bitMatrix,0) = 1 
	BEGIN
		DECLARE @bitDuplicate AS BIT = 0
	
		IF DATALENGTH(ISNULL(@vcItemAttributes,''))>2
		BEGIN
			EXEC sp_xml_preparedocument @hDoc OUTPUT, @vcItemAttributes
	                                      
			INSERT INTO @TempTable 
			(
				Fld_ID,
				Fld_Value
			)    
			SELECT
				*          
			FROM 
				OPENXML (@hDoc,'/NewDataSet/CusFlds',2)                                                  
			WITH 
				(Fld_ID NUMERIC(18,0),Fld_Value NUMERIC(18,0)) 
			ORDER BY 
				Fld_ID       

			DECLARE @strAttribute VARCHAR(500) = ''
			DECLARE @i AS INT = 1
			DECLARE @COUNT AS INT 
			DECLARE @numFldID AS NUMERIC(18,0)
			DECLARE @numFldValue AS NUMERIC(18,0)
			SELECT @COUNT = COUNT(*) FROM @TempTable

			WHILE @i <= @COUNT
			BEGIN
				SELECT @numFldID=Fld_ID,@numFldValue=Fld_Value FROM @TempTable WHERE ID=@i

				IF ISNUMERIC(@numFldValue)=1
				BEGIN
					SELECT @strAttribute=@strAttribute+Fld_label FROM CFW_Fld_Master WHERE Fld_id=@numFldID AND Grp_id = 9
                
					IF LEN(@strAttribute) > 0
					BEGIN
						SELECT @strAttribute=@strAttribute+':'+ vcData +',' FROM ListDetails WHERE numListItemID=@numFldValue
					END
				END

				SET @i = @i + 1
			END

			SET @strAttribute = SUBSTRING(@strAttribute,0,LEN(@strAttribute))

			IF
				(SELECT
					COUNT(*)
				FROM
					Item
				INNER JOIN
					ItemAttributes
				ON
					Item.numItemCode = ItemAttributes.numItemCode
					AND ItemAttributes.numDomainID = @numDomainID                     
				WHERE
					Item.numItemCode <> @numItemCode
					AND Item.vcItemName = @vcItemName
					AND Item.numItemGroup = @numItemGroup
					AND Item.numDomainID = @numDomainID
					AND dbo.fn_GetItemAttributes(@numDomainID,Item.numItemCode) = @strAttribute
				) > 0
			BEGIN
				RAISERROR('ITEM_WITH_ATTRIBUTES_ALREADY_EXISTS',16,1)
				RETURN
			END

			EXEC sp_xml_removedocument @hDoc 
		END	
	END
	ELSE IF ISNULL(@bitMatrix,0) = 0
	BEGIN
		-- REMOVE ATTRIBUTES CONFIGURATION OF ITEM AND WAREHOUSE
		DELETE FROM ItemAttributes WHERE numDomainID=@numDomainID AND numItemCode=@numItemCode
	END 
	
	IF ISNULL(@numItemGroup,0) > 0
	BEGIN
		IF (SELECT COUNT(*) FROM ItemGroupsDTL WHERE numItemGroupID = @numItemGroup AND tintType=2) = 0
		BEGIN
			RAISERROR('ITEM_GROUP_NOT_CONFIGURED',16,1)
			RETURN
		END
		ELSE IF EXISTS (SELECT 
							CFM.Fld_id
						FROM 
							CFW_Fld_Master CFM
						LEFT JOIN
							ListDetails LD
						ON
							CFM.numlistid = LD.numListID
						WHERE
							CFM.numDomainID = @numDomainID
							AND CFM.Fld_id IN (SELECT numOppAccAttrID FROM ItemGroupsDTL WHERE numItemGroupID=@numItemGroup AND tintType = 2)
						GROUP BY
							CFM.Fld_id HAVING COUNT(LD.numListItemID) = 0)
		BEGIN
			RAISERROR('ITEM_GROUP_NOT_CONFIGURED',16,1)
			RETURN
		END
	END

	IF @numCOGSChartAcntId=0 SET @numCOGSChartAcntId=NULL
	IF @numAssetChartAcntId=0 SET @numAssetChartAcntId=NULL  
	IF @numIncomeChartAcntId=0 SET @numIncomeChartAcntId=NULL     

	DECLARE @ParentSKU VARCHAR(50) = @vcSKU                        
	DECLARE @ItemID AS NUMERIC(9)
	DECLARE @cnt AS INT

	IF ISNULL(@charItemType,'') = '' OR ISNULL(@charItemType,'') = '0'  
		SET @charItemType = 'N'

	IF @intWebApiId = 2 AND LEN(ISNULL(@vcApiItemId, '')) > 0 AND ISNULL(@numItemGroup, 0) > 0 
    BEGIN 
        -- check wether this id already Mapped in ITemAPI 
        SELECT 
			@cnt = COUNT([numItemID])
        FROM 
			[ItemAPI]
        WHERE 
			[WebApiId] = @intWebApiId
            AND [numDomainId] = @numDomainID
            AND [vcAPIItemID] = @vcApiItemId

        IF @cnt > 0 
        BEGIN
            SELECT 
				@ItemID = [numItemID]
            FROM 
				[ItemAPI]
            WHERE 
				[WebApiId] = @intWebApiId
                AND [numDomainId] = @numDomainID
                AND [vcAPIItemID] = @vcApiItemId
        
			--Update Existing Product coming from API
            SET @numItemCode = @ItemID
        END
    END
	ELSE IF @intWebApiId > 1 AND LEN(ISNULL(@vcSKU, '')) > 0 
    BEGIN
        SET @ParentSKU = @vcSKU
		 
        SELECT 
			@cnt = COUNT([numItemCode])
        FROM 
			[Item]
        WHERE 
			[numDomainId] = @numDomainID
            AND (vcSKU = @vcSKU OR @vcSKU IN (SELECT vcWHSKU FROM dbo.WareHouseItems WHERE numItemID = Item.[numItemCode] AND vcWHSKU = @vcSKU))
            
		IF @cnt > 0 
        BEGIN
            SELECT 
				@ItemID = [numItemCode],
                @ParentSKU = vcSKU
            FROM 
				[Item]
            WHERE 
				[numDomainId] = @numDomainID
                AND (vcSKU = @vcSKU OR @vcSKU IN (SELECT vcWHSKU FROM dbo.WareHouseItems WHERE numItemID = Item.[numItemCode] AND vcWHSKU = @vcSKU))

            SET @numItemCode = @ItemID
        END
		ELSE 
		BEGIN
			-- check wether this id already Mapped in ITemAPI 
			SELECT 
				@cnt = COUNT([numItemID])
			FROM 
				[ItemAPI]
			WHERE 
				[WebApiId] = @intWebApiId
				AND [numDomainId] = @numDomainID
				AND [vcAPIItemID] = @vcApiItemId
                    
			IF @cnt > 0 
			BEGIN
				SELECT 
					@ItemID = [numItemID]
				FROM 
					[ItemAPI]
				WHERE 
					[WebApiId] = @intWebApiId
					AND [numDomainId] = @numDomainID
					AND [vcAPIItemID] = @vcApiItemId
        
				--Update Existing Product coming from API
				SET @numItemCode = @ItemID
			END
		END
    END
                                                         
	IF @numItemCode=0 OR @numItemCode IS NULL
	BEGIN
		INSERT INTO Item 
		(                                             
			vcItemName,txtItemDesc,charItemType,monListPrice,numItemClassification,bitTaxable,vcSKU,bitKitParent,
			numVendorID,numDomainID,numCreatedBy,bintCreatedDate,bintModifiedDate,numModifiedBy,bitSerialized,
			vcModelID,numItemGroup,numCOGsChartAcntId,numAssetChartAcntId,numIncomeChartAcntId,monAverageCost,                          
			monCampaignLabourCost,fltWeight,fltHeight,fltWidth,fltLength,bitFreeShipping,bitAllowBackOrder,vcUnitofMeasure,
			bitShowDeptItem,bitShowDeptItemDesc,bitCalAmtBasedonDepItems,bitAssembly,numBarCodeId,vcManufacturer,numBaseUnit,
			numPurchaseUnit,numSaleUnit,bitLotNo,IsArchieve,numItemClass,tintStandardProductIDType,vcExportToAPI,numShipClass,
			bitAllowDropShip,bitArchiveItem,bitAsset,bitRental,bitVirtualInventory,bitContainer,numContainer,numNoItemIntoContainer,bitMatrix
		)                                                              
		VALUES                                                                
		(                                                                
			@vcItemName,@txtItemDesc,@charItemType,@monListPrice,@numItemClassification,@bitTaxable,@ParentSKU,@bitKitParent,
			@numVendorID,@numDomainID,@numUserCntID,getutcdate(),getutcdate(),@numUserCntID,@bitSerialized,@vcModelID,@numItemGroup,                                      
			@numCOGsChartAcntId,@numAssetChartAcntId,@numIncomeChartAcntId,@monAverageCost,@monLabourCost,@fltWeight,@fltHeight,@fltWidth,                  
			@fltLength,@bitFreeshipping,@bitAllowBackOrder,@UnitofMeasure,@bitShowDeptItem,@bitShowDeptItemDesc,@bitCalAmtBasedonDepItems,    
			@bitAssembly,@numBarCodeId,@vcManufacturer,@numBaseUnit,@numPurchaseUnit,@numSaleUnit,@bitLotNo,@IsArchieve,@numItemClass,
			@tintStandardProductIDType,@vcExportToAPI,@numShipClass,@bitAllowDropShip,@bitArchiveItem ,@bitAsset,@bitRental,
			@bitVirtualInventory,@bitContainer,@numContainer,@numNoItemIntoContainer,@bitMatrix
		)                                             
 
		SET @numItemCode = SCOPE_IDENTITY()
  
		IF  @intWebApiId > 1 
		BEGIN
			 -- insert new product
			 --insert this id into linking table
			 INSERT INTO [ItemAPI] (
	 			[WebApiId],
	 			[numDomainId],
	 			[numItemID],
	 			[vcAPIItemID],
	 			[numCreatedby],
	 			[dtCreated],
	 			[numModifiedby],
	 			[dtModified],vcSKU
			 ) VALUES     
			 (
				@intWebApiId,
				@numDomainID,
				@numItemCode,
				@vcApiItemId,
	 			@numUserCntID,
	 			GETUTCDATE(),
	 			@numUserCntID,
	 			GETUTCDATE(),@vcSKU
	 		) 
		END
 
 
	END         
	ELSE IF @numItemCode>0 AND @intWebApiId > 0 
	BEGIN 
		IF @ProcedureCallFlag = 1 
		BEGIN
			DECLARE @ExportToAPIList AS VARCHAR(30)
			SELECT @ExportToAPIList = vcExportToAPI FROM dbo.Item WHERE numItemCode = @numItemCode

			IF @ExportToAPIList != ''
			BEGIN
				IF NOT EXISTS(select * from (SELECT * FROM dbo.Split(@ExportToAPIList ,','))as A where items= @vcExportToAPI ) 
				BEGIN
					SET @vcExportToAPI = @ExportToAPIList + ',' + @vcExportToAPI
				End
				ELSE
				BEGIN
					SET @vcExportToAPI = @ExportToAPIList 
				End
			END

			--update ExportToAPI String value
			UPDATE dbo.Item SET vcExportToAPI=@vcExportToAPI where numItemCode=@numItemCode  AND numDomainID=@numDomainID
					
			SELECT 
				@cnt = COUNT([numItemID]) 
			FROM 
				[ItemAPI] 
			WHERE  
				[WebApiId] = @intWebApiId 
				AND [numDomainId] = @numDomainID
				AND [vcAPIItemID] = @vcApiItemId
				AND numItemID = @numItemCode

			IF @cnt > 0
			BEGIN
				--update lastUpdated Date 
				UPDATE [ItemAPI] SET [dtModified] =GETUTCDATE() WHERE [WebApiId] =@intWebApiId AND [numDomainId] = @numDomainID
			END
			ELSE
			--Insert ItemAPI mapping
			BEGIN
				INSERT INTO [ItemAPI] 
				(
					[WebApiId],
					[numDomainId],
					[numItemID],
					[vcAPIItemID],
					[numCreatedby],
					[dtCreated],
					[numModifiedby],
					[dtModified],vcSKU
				) VALUES 
				(
					@intWebApiId,
					@numDomainID,
					@numItemCode,
					@vcApiItemId,
					@numUserCntID,
					GETUTCDATE(),
					@numUserCntID,
					GETUTCDATE(),@vcSKU
				)	
			END
		END   
	END                                            
	ELSE IF @numItemCode > 0 AND @intWebApiId <= 0                                                           
	BEGIN
		DECLARE @OldGroupID NUMERIC 
		SELECT @OldGroupID = ISNULL(numItemGroup,0) FROM [Item] WHERE numitemcode = @numItemCode AND [numDomainId] = @numDomainID
	
		DECLARE @monOldAverageCost AS DECIMAL(18,4);SET @monOldAverageCost=0
		SELECT @monOldAverageCost=(CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(monAverageCost,0) END) FROM Item WHERE numItemCode =@numItemCode AND [numDomainId] = @numDomainID
	   
		UPDATE 
			Item 
		SET 
			vcItemName=@vcItemName,txtItemDesc=@txtItemDesc,charItemType=@charItemType,monListPrice= @monListPrice,numItemClassification=@numItemClassification,                                             
			bitTaxable=@bitTaxable,vcSKU = (CASE WHEN ISNULL(@ParentSKU,'') = '' THEN @vcSKU ELSE @ParentSKU END),bitKitParent=@bitKitParent,                                             
			numVendorID=@numVendorID,numDomainID=@numDomainID,bintModifiedDate=getutcdate(),numModifiedBy=@numUserCntID,bitSerialized=@bitSerialized,                                                         
			vcModelID=@vcModelID,numItemGroup=@numItemGroup,numCOGsChartAcntId=@numCOGsChartAcntId,numAssetChartAcntId=@numAssetChartAcntId,                                      
			numIncomeChartAcntId=@numIncomeChartAcntId,monCampaignLabourCost=@monLabourCost,fltWeight=@fltWeight,fltHeight=@fltHeight,fltWidth=@fltWidth,                  
			fltLength=@fltLength,bitFreeShipping=@bitFreeshipping,bitAllowBackOrder=@bitAllowBackOrder,vcUnitofMeasure=@UnitofMeasure,bitShowDeptItem=@bitShowDeptItem,            
			bitShowDeptItemDesc=@bitShowDeptItemDesc,bitCalAmtBasedonDepItems=@bitCalAmtBasedonDepItems,bitAssembly=@bitAssembly,numBarCodeId=@numBarCodeId,
			vcManufacturer=@vcManufacturer,numBaseUnit=@numBaseUnit,numPurchaseUnit=@numPurchaseUnit,numSaleUnit=@numSaleUnit,bitLotNo=@bitLotNo,
			IsArchieve = @IsArchieve,numItemClass=@numItemClass,tintStandardProductIDType=@tintStandardProductIDType,vcExportToAPI=@vcExportToAPI,
			numShipClass=@numShipClass,bitAllowDropShip=@bitAllowDropShip, bitArchiveItem = @bitArchiveItem,bitAsset=@bitAsset,bitRental=@bitRental,
			bitVirtualInventory = @bitVirtualInventory,numContainer=@numContainer,numNoItemIntoContainer=@numNoItemIntoContainer,bitMatrix=@bitMatrix
		WHERE 
			numItemCode=@numItemCode 
			AND numDomainID=@numDomainID

		IF NOT EXISTS(SELECT numItemCode FROM dbo.OpportunityItems WHERE numItemCode = @numItemCode)
		BEGIN
			IF (((SELECT ISNULL(SUM(numOnHand),0) FROM WareHouseItems WHERE numItemID = @numItemCode AND numDomainId=@numDomainID) = 0) AND ((SELECT (CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(monAverageCost,0) END) FROM Item  WHERE numItemCode = @numItemCode AND numDomainId=@numDomainID ) = 0))
			BEGIN
				UPDATE Item SET monAverageCost=@monAverageCost  where numItemCode=@numItemCode  AND numDomainID=@numDomainID	
			END
		END
	
		--If Average Cost changed then add in Tracking
		IF ISNULL(@monAverageCost,0) != ISNULL(@monOldAverageCost,0)
		BEGIN
			DECLARE @numWareHouseItemID NUMERIC(9);SET @numWareHouseItemID=0
			DECLARE @numTotal int;SET @numTotal=0
			SET @i=0
			DECLARE @vcDescription AS VARCHAR(100);SET @vcDescription=''
		
			SELECT @numTotal=COUNT(*) FROM WareHouseItems where numItemID=@numItemCode AND numDomainId=@numDomainId
	     
			WHILE @i < @numTotal
			BEGIN
				SELECT @numWareHouseItemID = min(numWareHouseItemID) FROM WareHouseItems 
					WHERE numItemID=@numItemCode AND numDomainId=@numDomainId and numWareHouseItemID > @numWareHouseItemID 
				
				IF @numWareHouseItemID>0
				BEGIN
					SET @vcDescription='Average Cost Changed Manually OLD : ' + CAST(ISNULL(@monOldAverageCost,0) AS VARCHAR(20))
				
					DECLARE @numDomain AS NUMERIC(18,0)
					SET @numDomain = @numDomainID
					EXEC dbo.USP_ManageWareHouseItems_Tracking
						@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)		
						@numReferenceID = @numItemCode, --  numeric(9, 0)
						@tintRefType = 1, --  tinyint
						@vcDescription = @vcDescription, --  varchar(100)
						@tintMode = 0, --  tinyint
						@numModifiedBy = @numUserCntID, --  numeric(9, 0)
						@numDomainID = @numDomain
				END
		    
				SET @i = @i + 1
			END
		END
 
		DECLARE @bitCartFreeShipping as  bit 
		SELECT @bitCartFreeShipping = bitFreeShipping FROM dbo.CartItems where numItemCode = @numItemCode  AND numDomainID = @numDomainID 
 
		IF @bitCartFreeShipping <> @bitFreeshipping
		BEGIN
			UPDATE dbo.CartItems SET bitFreeShipping = @bitFreeshipping WHERE numItemCode = @numItemCode  AND numDomainID=@numDomainID
		END 
 
		IF @intWebApiId > 1 
		BEGIN
			SELECT 
				@cnt = COUNT([numItemID])
			FROM 
				[ItemAPI]
			WHERE 
				[WebApiId] = @intWebApiId
				AND [numDomainId] = @numDomainID
				AND [vcAPIItemID] = @vcApiItemId

			IF @cnt > 0
			BEGIN
				--update lastUpdated Date 
				UPDATE [ItemAPI] SET [dtModified] =GETUTCDATE() WHERE [WebApiId] =@intWebApiId AND [numDomainId] = @numDomainID
			END
			ELSE --Insert ItemAPI mapping
			BEGIN
				INSERT INTO [ItemAPI] 
				(
	 				[WebApiId],
	 				[numDomainId],
	 				[numItemID],
	 				[vcAPIItemID],
	 				[numCreatedby],
	 				[dtCreated],
	 				[numModifiedby],
	 				[dtModified],vcSKU
				 )
				 VALUES 
				 (
					@intWebApiId,
					@numDomainID,
					@numItemCode,
					@vcApiItemId,
	 				@numUserCntID,
	 				GETUTCDATE(),
	 				@numUserCntID,
	 				GETUTCDATE(),@vcSKU
	 			) 
			END
		END   
		                                                                            
		-- kishan It is necessary to add item into itemTax table . 
		IF	@bitTaxable = 1
		BEGIN
			IF NOT EXISTS(SELECT * FROM dbo.ItemTax WHERE numItemCode = @numItemCode AND numTaxItemID = 0)
			BEGIN
			    INSERT INTO dbo.ItemTax 
				(
					numItemCode,
					numTaxItemID,
					bitApplicable
				)
				VALUES 
				( 
					@numItemCode,
					0,
					1 
				) 						
			END
		END	 
      
		IF @charItemType='S' or @charItemType='N'                                                                       
		BEGIN
			DELETE FROM WareHouseItmsDTL where numWareHouseItemID in (select numWareHouseItemID from WareHouseItems where numItemID=@numItemCode AND [WareHouseItems].[numDomainID] = @numDomainID)
			DELETE FROM WareHouseItems where numItemID=@numItemCode AND [WareHouseItems].[numDomainID] = @numDomainID
		END                                      
   
		IF @bitKitParent=1 OR @bitAssembly = 1              
		BEGIN              
			DECLARE @hDoc1 AS INT                                            
			EXEC sp_xml_preparedocument @hDoc1 OUTPUT, @strChildItems                                                                        
                                             
			UPDATE 
				ItemDetails 
			SET
				numQtyItemsReq=X.QtyItemsReq
				,numWareHouseItemId=X.numWarehouseItmsID
				,numUOMId=X.numUOMId
				,vcItemDesc=X.vcItemDesc
				,sintOrder=X.sintOrder                                                                                          
			From 
			(
				SELECT 
					QtyItemsReq,ItemKitID,ChildItemID,numWarehouseItmsID,numUOMId,vcItemDesc,sintOrder, numItemDetailID As ItemDetailID                             
				FROM 
					OPENXML(@hDoc1,'/NewDataSet/Table1',2)                                                                         
				WITH
				(
					ItemKitID NUMERIC(9),                                                          
					ChildItemID NUMERIC(9),                                                                        
					QtyItemsReq DECIMAL(30, 16),
					numWarehouseItmsID NUMERIC(9),
					numUOMId NUMERIC(9),
					vcItemDesc VARCHAR(1000),
					sintOrder int,
					numItemDetailID NUMERIC(18,0)
				)
			)X                                                                         
			WHERE 
				numItemKitID=X.ItemKitID 
				AND numChildItemID=ChildItemID 
				AND numItemDetailID = X.ItemDetailID

			EXEC sp_xml_removedocument @hDoc1
		 END   
		 ELSE
		 BEGIN
 			DELETE FROM ItemDetails WHERE numItemKitID=@numItemCode
		 END                                                
	END

	IF ISNULL(@numItemGroup,0) = 0 OR ISNULL(@bitMatrix,0) = 1
	BEGIN
		UPDATE WareHouseItems SET monWListPrice = @monListPrice, vcWHSKU=@vcSKU, vcBarCode=@numBarCodeId WHERE numDomainID = @numDomainID AND numItemID = @numItemCode
	END

	IF @numItemCode > 0
	BEGIN
		DELETE 
			IC
		FROM 
			dbo.ItemCategory IC
		INNER JOIN 
			Category 
		ON 
			IC.numCategoryID = Category.numCategoryID
		WHERE 
			numItemID = @numItemCode		
			AND numCategoryProfileID = @numCategoryProfileID

		IF @vcCategories <> ''	
		BEGIN
			INSERT INTO ItemCategory( numItemID , numCategoryID )  SELECT  @numItemCode AS numItemID,ID from dbo.SplitIDs(@vcCategories,',')	
		END
	END

	


	-- SAVE MATRIX ITEM ATTRIBUTES
	IF @numItemCode > 0 AND ISNULL(@bitMatrix,0) = 1 AND LEN(ISNULL(@vcItemAttributes,'')) > 0 AND ISNULL(@bitOppOrderExists,0) <> 1 AND (SELECT COUNT(*) FROM @TempTable) > 0
	BEGIN
		-- DELETE PREVIOUS ITEM ATTRIBUTES
		DELETE FROM ItemAttributes WHERE numDomainID=@numDomainID AND numItemCode=@numItemCode

		-- INSERT NEW ITEM ATTRIBUTES
		INSERT INTO ItemAttributes
		(
			numDomainID
			,numItemCode
			,FLD_ID
			,FLD_Value
		)
		SELECT
			@numDomainID
			,@numItemCode
			,Fld_ID
			,Fld_Value
		FROM
			@TempTable

		IF (SELECT COUNT(*) FROM WarehouseItems WHERE numDomainID=@numDomainID AND numItemID=@numItemCode) > 0
		BEGIN

			-- DELETE PREVIOUS WAREHOUSE ATTRIBUTES
			DELETE FROM CFW_Fld_Values_Serialized_Items WHERE RecId IN (SELECT ISNULL(numWarehouseItemID,0) FROM WarehouseItems WHERE numDomainID=@numDomainID AND numItemID=@numItemCode)

			-- INSERT NEW WAREHOUSE ATTRIBUTES
			INSERT INTO CFW_Fld_Values_Serialized_Items
			(
				Fld_ID,
				Fld_Value,
				RecId,
				bitSerialized
			)                                                  
			SELECT 
				Fld_ID,
				ISNULL(CAST(Fld_Value AS VARCHAR),'') AS Fld_Value,
				TEMPWarehouse.numWarehouseItemID,
				0 
			FROM 
				@TempTable  
			OUTER APPLY
			(
				SELECT
					numWarehouseItemID
				FROM 
					WarehouseItems 
				WHERE 
					numDomainID=@numDomainID 
					AND numItemID=@numItemCode
			) AS TEMPWarehouse
		END
	END	  
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH        
END

GO
/****** Object:  StoredProcedure [dbo].[USP_PayrollLiabilityList]    Script Date: 02/28/2009 13:55:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Siva
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_payrollliabilitylist')
DROP PROCEDURE usp_payrollliabilitylist
GO
CREATE PROCEDURE [dbo].[USP_PayrollLiabilityList]
(
    @numDomainId          AS NUMERIC(9)  = 0,
    @numDepartmentId      AS NUMERIC(9)  = 0,
    @dtStartDate          AS DATETIME,
    @dtEndDate            AS DATETIME,
    @ClientTimeZoneOffset INT,
	@numOrgUserCntID AS NUMERIC(18,0),
	@tintUserRightType INT = 3,
	@tintUserRightForUpdate INT = 3
)
AS
BEGIN
	
	-- TEMPORARY TABLE TO HOLD DATA
	CREATE TABLE #tempHrs 
	(
		ID INT IDENTITY(1,1),
		numUserId NUMERIC,
		vcUserName VARCHAR(100),
		vcdepartment VARCHAR(100),
		bitLinkVisible BIT,
		bitpayroll BIT,
		numUserCntID NUMERIC,
		vcEmployeeId VARCHAR(50),
		monHourlyRate MONEY,
		decRegularHrs decimal(10,2),
		decPaidLeaveHrs decimal(10,2),
		monExpense MONEY,
		monReimburse MONEY,
		monCommPaidInvoice MONEY,
		monCommUNPaidInvoice MONEY,
		monTotalAmountDue MONEY,
		monAmountPaid MONEY, 
		bitCommissionContact BIT,
		numDivisionID NUMERIC(18,0),
		bitPartner BIT,
		tintCRMType TINYINT
	)

	-- GET EMPLOYEE OF DOMAIN
	INSERT INTO #tempHrs
	(
		numUserId,
		vcUserName,
		vcdepartment,
		bitLinkVisible,
		bitpayroll,
		numUserCntID,
		vcEmployeeId,
		monHourlyRate,
		bitCommissionContact
	)
	SELECT 
		UM.numUserId,
		Isnull(ADC.vcfirstname + ' ' + adc.vclastname,'-') vcUserName,
		dbo.fn_GetListItemName(adc.vcdepartment),
		(CASE @tintUserRightForUpdate
			WHEN 0 THEN 0
			WHEN 1 THEN CASE WHEN UM.numUserDetailId = @numOrgUserCntID THEN 1 ELSE 0 END
			WHEN 2 THEN CASE WHEN UM.numUserDetailId IN (SELECT numUserCntID FROM UserTerritory WHERE numUserCntID <> @numOrgUserCntID AND numTerritoryID IN (SELECT numTerritoryID FROM UserTerritory WHERE numUserCntID = @numOrgUserCntID)) THEN 1 ELSE 0 END
			ELSE 1
		END),
		Isnull(um.bitpayroll,0) AS bitpayroll,
		adc.numcontactid AS numUserCntID,
		Isnull(um.vcEmployeeId,'') AS vcEmployeeId,
		ISNULL(UM.monHourlyRate,0),
		0
	FROM  
		dbo.UserMaster UM 
	JOIN 
		dbo.AdditionalContactsInformation adc 
	ON 
		adc.numDomainID=@numDomainId 
		AND adc.numcontactid = um.numuserdetailid
	WHERE 
		UM.numDomainId=@numDomainId AND (adc.vcdepartment=@numDepartmentId OR @numDepartmentId=0)
		AND 1 = (CASE @tintUserRightType
					WHEN 1 THEN CASE WHEN UM.numUserDetailId = @numOrgUserCntID THEN 1 ELSE 0 END
					WHEN 2 THEN CASE WHEN UM.numUserDetailId IN (SELECT numUserCntID FROM UserTerritory WHERE numUserCntID <> @numOrgUserCntID AND numTerritoryID IN (SELECT numTerritoryID FROM UserTerritory WHERE numUserCntID = @numOrgUserCntID)) THEN 1 ELSE 0 END
					ELSE 1
				END)

	-- GET COMMISSION CONTACTS OF DOMAIN
	INSERT INTO #tempHrs 
	(
		numUserId,
		vcUserName,
		numUserCntID,
		vcEmployeeId,
		vcdepartment,
		bitLinkVisible,
		bitpayroll,
		monHourlyRate,
		bitCommissionContact
	)
	SELECT  
		0,
		ISNULL(A.vcFirstName +' '+ A.vcLastName + '(' + C.vcCompanyName +')','-'),
		A.numContactId,
		'',
		'-',
		(CASE WHEN @tintUserRightForUpdate = 0 OR @tintUserRightForUpdate = 1 OR @tintUserRightForUpdate = 2 THEN 0 ELSE 1 END),
		0,
		0,
		1
	FROM 
		CommissionContacts CC 
	JOIN DivisionMaster D ON CC.numDivisionID=D.numDivisionID
	JOIN AdditionalContactsInformation A ON D.numDivisionID=A.numDivisionID
	JOIN CompanyInfo C ON D.numCompanyID = C.numCompanyId
	WHERE 
		CC.numDomainID=@numDomainID
		AND 1 = (CASE WHEN @tintUserRightType = 1 OR @tintUserRightType = 2 THEN 0 ELSE 1 END)


	-- GET PARTNERS OF DOMAIN
	INSERT INTO #tempHrs 
	(
		numUserId,
		vcUserName,
		numUserCntID,
		vcEmployeeId,
		vcdepartment,
		bitLinkVisible,
		bitpayroll,
		monHourlyRate,
		numDivisionID,
		bitPartner,
		bitCommissionContact,
		tintCRMType
	)
	SELECT  
		0,
		ISNULL(C.vcCompanyName,'-'),
		0,
		'',
		'-',
		(CASE WHEN @tintUserRightForUpdate = 0 OR @tintUserRightForUpdate = 1 OR @tintUserRightForUpdate = 2 THEN 0 ELSE 1 END),
		0,
		0,
		numDivisionID,
		1,
		1,
		tintCRMType
	FROM  
		DivisionMaster AS D 
	LEFT JOIN 
		CompanyInfo AS C
	ON 
		D.numCompanyID=C.numCompanyID
	WHERE 
		D.numDomainID=@numDomainID 
		AND D.vcPartnerCode <> ''
		AND 1 = (CASE WHEN @tintUserRightType = 1 OR @tintUserRightType = 2 THEN 0 ELSE 1 END)
	ORDER BY
		vcCompanyName

	-- FETCH ALLREADY PAID COMMISSION DATA AND STORE IN TEMPORARY TABLE
	SELECT * INTO #tempPayrollTracking FROM PayrollTracking WHERE numDomainId=@numDomainId

	Declare @decTotalHrsWorked as decimal(10,2)
	DECLARE @decTotalPaidLeaveHrs AS DECIMAL(10,2)
	DECLARE @monExpense MONEY,@monReimburse MONEY,@monCommPaidInvoice MONEY,@monCommUNPaidInvoice MONEY 

	DECLARE @i INT = 1
	DECLARE @COUNT INT = 0
	DECLARE @numUserCntID NUMERIC(18,0)
	DECLARE @numDivisionID NUMERIC(18,0)
	DECLARE @bitCommissionContact BIT
    
	SELECT @COUNT=COUNT(*) FROM #tempHrs

	-- LOOP OVER EACH EMPLOYEE
	WHILE @i <= @COUNT
	BEGIN
		SELECT @decTotalHrsWorked=0,@decTotalPaidLeaveHrs=0,@monExpense=0,@monReimburse=0,@monCommPaidInvoice=0,@monCommUNPaidInvoice=0, @numUserCntID=0,@bitCommissionContact=0
		
		SELECT
			@numUserCntID = ISNULL(numUserCntID,0),
			@bitCommissionContact = ISNULL(bitCommissionContact,0),
			@numDivisionID=ISNULL(numDivisionID,0)
		FROM
			#tempHrs
		WHERE
			ID = @i
		
		-- EMPLOYEE (ONLY COMMISSION AMOUNT IS NEEDED FOR COMMISSION CONTACTS)
		IF @bitCommissionContact = 0
		BEGIN
			-- CALCULATE REGULAR HRS
			SELECT 
				@decTotalHrsWorked=sum(x.Hrs) 
			FROM 
				(
					SELECT  
						ISNULL(SUM(CAST(DATEDIFF(MINUTE,dtfromdate,dttodate) AS FLOAT)/CAST(60 AS FLOAT)),0) AS Hrs            
					FROM 
						TimeAndExpense 
					WHERE 
						(numType=1 OR numType=2) 
						AND numCategory=1                 
						AND (
								(DATEADD(MINUTE,-@ClientTimeZoneOffset,dtFromDate) BETWEEN @dtStartDate AND @dtEndDate) 
								OR 
								(DATEADD(MINUTE,-@ClientTimeZoneOffset,dtToDate) BETWEEN @dtStartDate AND @dtEndDate)
							) 
						AND numUserCntID=@numUserCntID  
						AND numDomainID=@numDomainId 
						AND numCategoryHDRID NOT IN (SELECT numCategoryHDRID FROM #tempPayrollTracking WHERE ISNULL(numCategoryHDRID,0)>0)
				) x

			-- CALCULATE PAID LEAVE HRS
			SELECT 
				@decTotalPaidLeaveHrs=ISNULL(
												SUM( 
														CASE 
														WHEN dtfromdate = dttodate 
														THEN 
															24 - (CASE WHEN bitfromfullday = 0 THEN 12 ELSE 0 END)
														ELSE 
															(Datediff(DAY,dtfromdate,dttodate) + 1) * 24 - (CASE WHEN bitfromfullday = 0 THEN 12 ELSE 0 END) - (CASE WHEN bittofullday = 0 THEN 12 ELSE 0 END)
														 END
													)
											 ,0)
            FROM   
				TimeAndExpense
            WHERE  
				numtype = 3 
				AND numcategory = 3 
				AND numusercntid = @numUserCntID 
				AND numdomainid = @numDomainId 
                AND (
						(DATEADD(MINUTE,-@ClientTimeZoneOffset,dtFromDate) BETWEEN @dtStartDate AND @dtEndDate)
						Or 
						(DATEADD(MINUTE,-@ClientTimeZoneOffset,dtToDate) BETWEEN @dtStartDate AND @dtEndDate)
					)
				AND numCategoryHDRID NOT IN (SELECT numCategoryHDRID FROM #tempPayrollTracking WHERE ISNULL(numCategoryHDRID,0)>0)

			-- CALCULATE EXPENSES
			SELECT 
				@monExpense=ISNULL((SUM(CAST(monAmount AS FLOAT))),0)    
			FROM 
				TimeAndExpense 
			WHERE 
				numCategory=2 
				AND numType in (1,2) 
				AND numUserCntID=@numUserCntID 
				AND numDomainID=@numDomainId 
				AND ((dtFromDate BETWEEN @dtStartDate And @dtEndDate) Or (dtToDate BETWEEN @dtStartDate And @dtEndDate)) 
				AND numCategoryHDRID NOT IN (SELECT numCategoryHDRID FROM #tempPayrollTracking WHERE ISNULL(numCategoryHDRID,0)>0)

			-- CALCULATE REIMBURSABLE EXPENSES
			SELECT 
				@monReimburse=Isnull((SUM(CAST(monamount AS FLOAT))),0)
            FROM   
				TimeAndExpense 
            WHERE  
				bitreimburse = 1 
				AND numcategory = 2 
                AND numusercntid = @numUserCntID 
				AND numdomainid = @numDomainId
                AND dtfromdate BETWEEN @dtStartDate AND @dtEndDate
                AND numCategoryHDRID NOT IN (SELECT numCategoryHDRID FROM #tempPayrollTracking WHERE ISNULL(numCategoryHDRID,0)>0)

		END

		-- CALCULATE COMMISSION PAID INVOICE
		SELECT 
			@monCommPaidInvoice=ISNULL(SUM(Amount),0) 
		FROM 
			(
				SELECT 
					oppBiz.numOppID,
					oppBiz.numOppBizDocsID,
					ISNULL(SUM(BC.numComissionAmount - ISNULL(PT.monCommissionAmt,0)),0) AS Amount
				FROM 
					OpportunityMaster Opp 
				INNER JOIN OpportunityBizDocs oppBiz on oppBiz.numOppId=opp.numOppId 
				INNER JOIN BizDocComission BC on BC.numOppBizDocId=oppBiz.numOppBizDocsId 
				LEFT JOIN #tempPayrollTracking PT ON PT.numComissionID=BC.numComissionID
				CROSS APPLY
				(
					SELECT 
						MAX(DM.dtDepositDate) dtDepositDate
					FROM 
						DepositMaster DM 
					JOIN dbo.DepositeDetails DD	ON DM.numDepositId=DD.numDepositID 
					WHERE 
						DM.tintDepositePage=2 
						AND DM.numDomainId=@numDomainId
						AND DD.numOppID=oppBiz.numOppID 
						AND DD.numOppBizDocsID =oppBiz.numOppBizDocsID
				) TEMPDeposit
				WHERE 
					Opp.numDomainId=@numDomainId 
					AND ((BC.numUserCntId=@numUserCntID AND BC.tintAssignTo <> 3) OR (BC.numUserCntID=@numDivisionID AND BC.tintAssignTo=3))
					AND BC.bitCommisionPaid=0 
					AND oppBiz.bitAuthoritativeBizDocs = 1 
					AND isnull(oppBiz.monAmountPaid,0) >=oppBiz.monDealAmount 
					AND CAST(DATEADD(MINUTE,-@ClientTimeZoneOffset,TEMPDeposit.dtDepositDate) AS DATE) BETWEEN CAST(@dtStartDate AS DATE) AND CAST(@dtEndDate AS DATE)
				GROUP BY 
					oppBiz.numOppID,
					oppBiz.numOppBizDocsID
			) A

		-- CALCULATE COMMISSION UNPAID INVOICE 
		SELECT 
			@monCommUNPaidInvoice=isnull(sum(BC.numComissionAmount - ISNULL(PT.monCommissionAmt,0)),0) 
		FROM 
			OpportunityMaster Opp 
		INNER JOIN OpportunityBizDocs oppBiz on oppBiz.numOppId=opp.numOppId 
        INNER JOIN BizDocComission BC on BC.numOppBizDocId=oppBiz.numOppBizDocsId
        LEFT JOIN #tempPayrollTracking PT ON PT.numComissionID=BC.numComissionID 
		WHERE 
			Opp.numDomainId=@numDomainId 
			AND ((BC.numUserCntId=@numUserCntID AND BC.tintAssignTo <> 3) OR (BC.numUserCntID=@numDivisionID AND BC.tintAssignTo=3))
			AND BC.bitCommisionPaid=0 
			AND oppBiz.bitAuthoritativeBizDocs = 1 
			AND ISNULL(oppBiz.monAmountPaid,0) <oppBiz.monDealAmount
			AND ((DATEADD(MINUTE,-@ClientTimeZoneOffset,oppBiz.dtFromDate) BETWEEN @dtStartDate AND @dtEndDate))	

		UPDATE 
			#tempHrs 
		SET    
			decRegularHrs=@decTotalHrsWorked,
			decPaidLeaveHrs=@decTotalPaidLeaveHrs,
			monExpense=@monExpense,
			monReimburse=@monReimburse,
			monCommPaidInvoice=@monCommPaidInvoice,
			monCommUNPaidInvoice=@monCommUNPaidInvoice,
			monTotalAmountDue= @decTotalHrsWorked * monHourlyRate + @decTotalPaidLeaveHrs * monHourlyRate - @monExpense + @monReimburse + @monCommPaidInvoice + @monCommUNPaidInvoice
        WHERE 
			(numUserCntID=@numUserCntID AND ISNULL(bitPartner,0) = 0) 
			OR (numDivisionID=@numDivisionID AND ISNULL(bitPartner,0) = 1)

		SET @i = @i + 1
	END

	-- CALCULATE PAID AMOUNT
	UPDATE 
		temp 
	SET 
		monAmountPaid=ISNULL(PD.monTotalAmt,0) 
	FROM 
		#tempHrs temp  
	JOIN dbo.PayrollDetail PD ON PD.numUserCntID=temp.numUserCntID
	JOIN dbo.PayrollHeader PH ON PH.numPayrollHeaderID=PD.numPayrollHeaderID
	WHERE 
		ISNULL(PD.numCheckStatus,0)=2  
		AND ((dtFromDate BETWEEN @dtStartDate AND @dtEndDate) OR (dtToDate BETWEEN @dtStartDate And @dtEndDate))
 
	SELECT * FROM #tempHrs
	DROP TABLE #tempPayrollTracking
	DROP TABLE #tempHrs
END
/****** Object:  StoredProcedure [dbo].[USP_SaveJournalDetails]    Script Date: 07/26/2008 16:21:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Create by Siva                                                                                        
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_SaveJournalDetails' ) 
    DROP PROCEDURE USP_SaveJournalDetails
GO
CREATE PROCEDURE [dbo].[USP_SaveJournalDetails]
    @numJournalId AS NUMERIC(9) = 0 ,
    @strRow AS TEXT = '' ,
    @Mode AS TINYINT = 0 ,
    @numDomainId AS NUMERIC(9) = 0                       
--@RecurringMode as tinyint=0,    
AS 
BEGIN           
                
BEGIN TRY 
		-- INCLUDE ENCODING OTHER WISE IT WILL THROW ERROR WHEN SPECIAL CHARACTERS ARE AVAILABLE IN DESCRIPT FIELDS
		IF CHARINDEX('<?xml',@strRow) = 0
		BEGIN
			SET @strRow = CONCAT('<?xml version="1.0" encoding="ISO-8859-1"?>',@strRow)
		END

        BEGIN TRAN  
        
        ---Always set default Currency ID as base currency ID of domain, and Rate =1
        DECLARE @numBaseCurrencyID NUMERIC
        SELECT @numBaseCurrencyID = ISNULL(numCurrencyID,0) FROM dbo.Domain WHERE numDomainId=@numDomainId
        
        IF CONVERT(VARCHAR(100), @strRow) <> '' 
            BEGIN                                                                                                            
                DECLARE @hDoc3 INT                                                                                                                                                                
                EXEC sp_xml_preparedocument @hDoc3 OUTPUT, @strRow                                                                                                             
--                PRINT '---Insert xml into temp table ---'
                    SELECT numTransactionId ,numDebitAmt ,numCreditAmt ,numChartAcntId ,varDescription ,numCustomerId ,/*numDomainId ,*/bitMainDeposit ,bitMainCheck ,bitMainCashCredit ,numoppitemtCode ,chBizDocItems ,vcReference ,numPaymentMethod ,bitReconcile , 
                    ISNULL(NULLIF(numCurrencyID,0),@numBaseCurrencyID) AS numCurrencyID  ,ISNULL(NULLIF(fltExchangeRate,0.0000),1) AS fltExchangeRate ,numTaxItemID ,numBizDocsPaymentDetailsId ,numcontactid ,numItemID ,numProjectID ,numClassID ,numCommissionID ,numReconcileID ,bitCleared ,tintReferenceType ,numReferenceID ,numCampaignID 
                     INTO #temp FROM      OPENXML (@hdoc3,'/ArrayOfJournalEntryNew/JournalEntryNew',2)
												WITH (
												numTransactionId numeric(10, 0),numDebitAmt FLOAT,numCreditAmt FLOAT,numChartAcntId numeric(18, 0),varDescription varchar(1000),numCustomerId numeric(18, 0),/*numDomainId numeric(18, 0),*/bitMainDeposit bit,bitMainCheck bit,bitMainCashCredit bit,numoppitemtCode numeric(18, 0),chBizDocItems nchar(10),vcReference varchar(500),numPaymentMethod numeric(18, 0),bitReconcile bit,numCurrencyID numeric(18, 0),fltExchangeRate float,numTaxItemID numeric(18, 0),numBizDocsPaymentDetailsId numeric(18, 0),numcontactid numeric(9, 0),numItemID numeric(18, 0),numProjectID numeric(18, 0),numClassID numeric(18, 0),numCommissionID numeric(9, 0),numReconcileID numeric(18, 0),bitCleared bit,tintReferenceType tinyint,numReferenceID numeric(18, 0),numCampaignID NUMERIC(18,0)
													)

                 EXEC sp_xml_removedocument @hDoc3 
                                    

/*-----------------------Validation of balancing entry*/

IF EXISTS(SELECT * FROM #temp)
BEGIN
	DECLARE @SumTotal MONEY 
	SELECT @SumTotal = CAST(SUM(numDebitAmt) AS MONEY)-CAST(SUM(numCreditAmt) AS MONEY) FROM #temp

--	PRINT cast( @SumTotal AS DECIMAL(10,4))
	IF cast( @SumTotal AS DECIMAL(10,4)) <> 0.0000
	BEGIN
		 INSERT INTO [dbo].[GenealEntryAudit]
           ([numDomainID]
           ,[numJournalID]
           ,[numTransactionID]
           ,[dtCreatedDate]
           ,[varDescription])
		 SELECT @numDomainID,
				@numJournalId,
				0,
				GETUTCDATE(),
				'IM_BALANCE'

		 RAISERROR ( 'IM_BALANCE', 16, 1 ) ;
		 RETURN;
	END

	
END



DECLARE @numEntryDateSortOrder AS NUMERIC
DECLARE @datEntry_Date AS DATETIME
--Combine Date from datentryDate with time part of current time
SELECT @datEntry_Date=( CAST( CONVERT(VARCHAR,DATEPART(year, datEntry_Date)) + '-' + RIGHT('00000'+ CONVERT(VARCHAR,DATEPART(month, datEntry_Date)),2)+ 
+ '-' + CONVERT(VARCHAR,DATEPART(day, datEntry_Date))
+ ' ' + CONVERT(VARCHAR,DATEPART(hour, GETDATE()))
+ ':' + CONVERT(VARCHAR,DATEPART(minute, GETDATE()))
+ ':' + CONVERT(VARCHAR,DATEPART(second, GETDATE())) AS DATETIME)  
) 
 from dbo.General_Journal_Header WHERE numJournal_Id=@numJournalId AND numDomainId=@numDomainId

SET @numEntryDateSortOrder = convert(numeric,
RIGHT('00000'+ CONVERT(VARCHAR,DATEPART(year, @datEntry_Date)),4)+
RIGHT('00000'+ CONVERT(VARCHAR,DATEPART(month, @datEntry_Date)),2)+
RIGHT('00000'+ CONVERT(VARCHAR,DATEPART(day, @datEntry_Date)),2)+
RIGHT('00000'+ CONVERT(VARCHAR,DATEPART(hour, @datEntry_Date)),2)+
RIGHT('00000'+ CONVERT(VARCHAR,DATEPART(minute, @datEntry_Date)),2)+
RIGHT('00000'+ CONVERT(VARCHAR,DATEPART(second, @datEntry_Date)),2)+
RIGHT('00000'+ CONVERT(VARCHAR,DATEPART(millisecond, @datEntry_Date)),3))

/*-----------------------Validation of balancing entry*/

			

                IF @Mode = 1  /*Edit Journal entries*/
                    BEGIN             
						PRINT '---delete removed transactions ---'
                        DELETE  FROM General_Journal_Details
                        WHERE   numJournalId = @numJournalId
                                AND numTransactionId NOT IN (SELECT numTransactionId FROM #temp as X )
			                                                           
						--Update transactions
						PRINT '---updated existing transactions ---'
                        UPDATE  dbo.General_Journal_Details 
                        SET    		[numDebitAmt] = X.numDebitAmt,
									[numCreditAmt] = X.numCreditAmt,
									[numChartAcntId] = X.numChartAcntId,
									[varDescription] = X.varDescription,
									[numCustomerId] = NULLIF(X.numCustomerId,0),
									[bitMainDeposit] = X.bitMainDeposit,
									[bitMainCheck] = X.bitMainCheck,
									[bitMainCashCredit] = X.bitMainCashCredit,
									[numoppitemtCode] = NULLIF(X.numoppitemtCode,0),
									[chBizDocItems] = X.chBizDocItems,
									[vcReference] = X.vcReference,
									[numPaymentMethod] = X.numPaymentMethod,
									[numCurrencyID] = NULLIF(X.numCurrencyID,0),
									[fltExchangeRate] = X.fltExchangeRate,
									[numTaxItemID] = NULLIF(X.numTaxItemID,0),
									[numBizDocsPaymentDetailsId] = NULLIF(X.numBizDocsPaymentDetailsId,0),
									[numcontactid] = NULLIF(X.numcontactid,0),
									[numItemID] = NULLIF(X.numItemID,0),
									[numProjectID] =NULLIF( X.numProjectID,0),
									[numClassID] = NULLIF(X.numClassID,0),
									[numCommissionID] = NULLIF(X.numCommissionID,0),
									numCampaignID=NULLIF(X.numCampaignID,0),
									numEntryDateSortOrder1=@numEntryDateSortOrder
--									[tintReferenceType] = X.tintReferenceType,
--									[numReferenceID] = X.numReferenceID
                          FROM    #temp as X 
						  WHERE   X.numTransactionId = General_Journal_Details.numTransactionId
			               
		               
					    INSERT INTO [dbo].[GenealEntryAudit]
									([numDomainID]
									,[numJournalID]
									,[numTransactionID]
									,[dtCreatedDate]
									,[varDescription])
						SELECT GJD.numDomainID, GJD.[numJournalId], GJD.[numTransactionId], GETUTCDATE(),[GJD].[varDescription]
						FROM General_Journal_Details AS GJD                                             
						JOIN #temp AS X ON  X.numTransactionId = GJD.numTransactionId
						
                    END 
                IF @Mode = 0 OR @Mode = 1 /*-- Insert journal only*/
                    BEGIN    
						PRINT '---insert existing transactions ---'                                                                                               
                        INSERT  INTO [dbo].[General_Journal_Details]
                                ( [numJournalId] ,
                                  [numDebitAmt] ,
                                  [numCreditAmt] ,
                                  [numChartAcntId] ,
                                  [varDescription] ,
                                  [numCustomerId] ,
                                  [numDomainId] ,
                                  [bitMainDeposit] ,
                                  [bitMainCheck] ,
                                  [bitMainCashCredit] ,
                                  [numoppitemtCode] ,
                                  [chBizDocItems] ,
                                  [vcReference] ,
                                  [numPaymentMethod] ,
                                  [bitReconcile] ,
                                  [numCurrencyID] ,
                                  [fltExchangeRate] ,
                                  [numTaxItemID] ,
                                  [numBizDocsPaymentDetailsId] ,
                                  [numcontactid] ,
                                  [numItemID] ,
                                  [numProjectID] ,
                                  [numClassID] ,
                                  [numCommissionID] ,
                                  [numReconcileID] ,
                                  [bitCleared],
                                  [tintReferenceType],
								  [numReferenceID],[numCampaignID],numEntryDateSortOrder1
	                                
                                )
                                SELECT  @numJournalId ,
                                        [numDebitAmt] ,
                                        [numCreditAmt] ,
                                        CASE WHEN [numChartAcntId]=0 THEN NULL ELSE numChartAcntId END  ,
                                        [varDescription] ,
                                        NULLIF(numCustomerId, 0) ,
                                        @numDomainId ,
                                        [bitMainDeposit] ,
                                        [bitMainCheck] ,
                                        [bitMainCashCredit] ,
                                        NULLIF([numoppitemtCode], 0) ,
                                        [chBizDocItems] ,
                                        [vcReference] ,
                                        [numPaymentMethod] ,
                                        [bitReconcile] ,
                                        NULLIF([numCurrencyID], 0) ,
                                        [fltExchangeRate] ,
                                        NULLIF([numTaxItemID], 0) ,
                                        NULLIF([numBizDocsPaymentDetailsId], 0) ,
                                        NULLIF([numcontactid], 0) ,
                                        NULLIF([numItemID], 0) ,
                                        NULLIF([numProjectID], 0) ,
                                        NULLIF([numClassID], 0) ,
                                        NULLIF([numCommissionID], 0) ,
                                        NULLIF([numReconcileID], 0) ,
                                        [bitCleared],
                                        tintReferenceType,
										NULLIF(numReferenceID,0),NULLIF(numCampaignID,0),@numEntryDateSortOrder
                                        
                                FROM #temp as X    
                                WHERE X.numTransactionId =0

						--Set Default Class If enable User Level Class Accountng 
						DECLARE @numAccountClass AS NUMERIC(18)=0                             
                        SELECT @numAccountClass=ISNULL(numAccountClass,0) FROM General_Journal_Header WHERE numDomainId=@numDomainId AND numJournal_Id=@numJournalId
                        IF @numAccountClass>0
                        BEGIN
							UPDATE General_Journal_Details SET numClassID=@numAccountClass WHERE numJournalId=@numJournalId AND numDomainId=@numDomainId AND ISNULL(numClassID,0) = 0
						END

						INSERT INTO [dbo].[GenealEntryAudit]
									([numDomainID]
									,[numJournalID]
									,[numTransactionID]
									,[dtCreatedDate]
									,[varDescription])
						SELECT @numDomainID, @numJournalId, X.numTransactionId, GETUTCDATE(),X.varDescription
						FROM #temp AS X WHERE X.numTransactionId = 0

                    END 
                    
                    
                    UPDATE dbo.General_Journal_Header 
						SET numAmount=(SELECT SUM(numDebitAmt) FROM dbo.General_Journal_Details WHERE numJournalID=@numJournalID)
						WHERE numJournal_Id=@numJournalId AND numDomainId=@numDomainId
						
					SELECT @SumTotal = CAST(SUM(numDebitAmt) AS MONEY)-CAST(SUM(numCreditAmt) AS MONEY) FROM General_Journal_Details WHERE numJournalId=@numJournalId AND numDomainId=@numDomainId
					IF cast( @SumTotal AS DECIMAL(10,4)) > 0.0001
					BEGIN
						 INSERT INTO [dbo].[GenealEntryAudit]
						   ([numDomainID]
						   ,[numJournalID]
						   ,[numTransactionID]
						   ,[dtCreatedDate]
						   ,[varDescription])
						 SELECT @numDomainID,
								@numJournalId,
								0,
								GETUTCDATE(),
								'IM_BALANCE'

						 RAISERROR ( 'IM_BALANCE', 16, 1 ) ;
						 RETURN;
					END

                    drop table #temp
                    
                    -- Update leads,prospects to Accounts
					DECLARE @numDivisionID AS NUMERIC(18)
					DECLARE @numUserCntID AS NUMERIC(18)
					
					SELECT TOP 1 @numDivisionID = ISNULL(numCustomerId,0), @numUserCntID = ISNULL(numCreatedBy,0)
					FROM dbo.General_Journal_Details GJD
					JOIN dbo.General_Journal_Header GJH ON GJD.numJournalId = GJH.numJournal_Id AND GJD.numDomainId = GJH.numDomainId
					WHERE numJournalId = @numJournalId 
					AND GJH.numDomainId = @numDomainID
					
					IF @numDivisionID > 0 AND @numUserCntID > 0
					BEGIN
						EXEC dbo.USP_UpdateCRMTypeToAccounts @numDivisionID ,@numDomainID ,@numUserCntID
					END
            END
            
SELECT GJD.numTransactionId,GJD.numEntryDateSortOrder1,Row_number() OVER(ORDER BY GJD.numTransactionId) AS NewSortId  INTO #Temp1 
FROM General_Journal_Details GJD WHERE GJD.numDomainId = @numDomainID and  GJD.numEntryDateSortOrder1  LIKE SUBSTRING(CAST(@numEntryDateSortOrder AS VARCHAR(25)),1,12) + '%'

UPDATE  GJD SET GJD.numEntryDateSortOrder1 = (Temp1.numEntryDateSortOrder1 + Temp1.NewSortId) 
FROM General_Journal_Details GJD INNER JOIN #Temp1 AS Temp1 ON GJD.numTransactionId = Temp1.numTransactionId

IF object_id('TEMPDB.DBO.#Temp1') IS NOT NULL DROP TABLE #Temp1

        
        COMMIT TRAN 
    END TRY 
    BEGIN CATCH		
        DECLARE @strMsg VARCHAR(200)
        SET @strMsg = ERROR_MESSAGE()
        IF ( @@TRANCOUNT > 0 ) 
            BEGIN
				INSERT INTO [dbo].[GenealEntryAudit]
									([numDomainID]
									,[numJournalID]
									,[numTransactionID]
									,[dtCreatedDate]
									,[varDescription])
						SELECT @numDomainID, @numJournalId,0, GETUTCDATE(),@strMsg

                RAISERROR ( @strMsg, 16, 1 ) ;
                ROLLBACK TRAN
                RETURN 1
            END
    END CATCH	

            
--End                                              
---For Recurring Transaction                                              
                                              
--if @RecurringMode=1                                              
--Begin    
--  print 'SIVA--Recurring'    
--  print  '@numDomainId=============='+Convert(varchar(10),@numDomainId)                                     
--  Declare @numMaxJournalId as numeric(9)                                              
--  Select @numMaxJournalId=max(numJournal_Id) From General_Journal_Header Where numDomainId=@numDomainId                                              
--  insert into General_Journal_Details(numJournalId,numDebitAmt,numCreditAmt,numChartAcntId,varDescription,numCustomerId,numDomainId,bitMainCheck,bitMainCashCredit)                                              
--  Select @numMaxJournalId,numDebitAmt,numCreditAmt,numChartAcntId,varDescription,NULLIF(numCustomerId,0),numDomainId,bitMainCheck,bitMainCashCredit from General_Journal_Details Where numJournalId=@numJournalId  And numDomainId=@numDomainId                         
--   
--                   
--/*Commented by chintan Opening balance Will be updated by Trigger*/
----Exec USP_UpdateChartAcntOpnBalanceForJournalEntry @JournalId=@numMaxJournalId,@numDomainId=@numDomainId                                              
--End           
    END
GO
/****** Object:  StoredProcedure [dbo].[USP_UpdatedomainDetails]    Script Date: 05/07/2009 21:56:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                                      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_updatedomaindetails')
DROP PROCEDURE usp_updatedomaindetails
GO
CREATE PROCEDURE [dbo].[USP_UpdatedomainDetails]                                      
@numDomainID as numeric(9)=0,                                      
@vcDomainName as varchar(50)='',                                      
@vcDomainDesc as varchar(100)='',                                      
@bitExchangeIntegration as bit,                                      
@bitAccessExchange as bit,                                      
@vcExchUserName as varchar(100)='',                                      
@vcExchPassword as varchar(100)='',                                      
@vcExchPath as varchar(200)='',                                      
@vcExchDomain as varchar(100)='',                                    
@tintCustomPagingRows as tinyint,                                    
@vcDateFormat as varchar(30)='',                                    
@numDefCountry as numeric(9),                                    
@tintComposeWindow as tinyint,                                    
@sintStartDate as smallint,                                    
@sintNoofDaysInterval as smallint,                                    
@tintAssignToCriteria as tinyint,                                    
@bitIntmedPage as bit,                                    
@tintFiscalStartMonth as tinyint,                                      
@tintPayPeriod as tinyint,                        
@numCurrencyID as numeric(9) ,                
@charUnitSystem as char(1)='',              
@vcPortalLogo as varchar(100)='',            
@tintChrForComSearch as tinyint  ,            
@intPaymentGateWay as int,    
@tintChrForItemSearch as tinyint,
@ShipCompany as numeric(9),
@bitAutoPopulateAddress as bit=null,
@tintPoulateAddressTo as tinyint=null,
@bitMultiCurrency as bit,
@bitCreateInvoice AS tinyint = null,
@numDefaultSalesBizDocID AS NUMERIC(9)=NULL,
@numDefaultPurchaseBizDocID AS NUMERIC(9)=NULL,
@bitSaveCreditCardInfo AS BIT = NULL,
@intLastViewedRecord AS INT=10,
@tintCommissionType AS TINYINT,
@tintBaseTax AS TINYINT,
@bitEmbeddedCost AS BIT,
@numDefaultSalesOppBizDocId NUMERIC=NULL,
@tintSessionTimeOut TINYINT=1,
@tintDecimalPoints TINYINT,
@bitCustomizePortal BIT=0,
@tintBillToForPO TINYINT,
@tintShipToForPO TINYINT,
@tintOrganizationSearchCriteria TINYINT,
@bitDocumentRepositary BIT=0,
@tintComAppliesTo TINYINT=0,
@bitGtoBContact BIT=0,
@bitBtoGContact BIT=0,
@bitGtoBCalendar BIT=0,
@bitBtoGCalendar BIT=0,
@bitExpenseNonInventoryItem BIT=0,
@bitInlineEdit BIT=0,
@bitRemoveVendorPOValidation BIT=0,
@tintBaseTaxOnArea tinyint=0,
@bitAmountPastDue BIT=0,
@monAmountPastDue money,
@bitSearchOrderCustomerHistory BIT=0,
@bitRentalItem as bit=0,
@numRentalItemClass as NUMERIC(9)=NULL,
@numRentalHourlyUOM as NUMERIC(9)=NULL,
@numRentalDailyUOM as NUMERIC(9)=NULL,
@tintRentalPriceBasedOn as tinyint=NULL,
@tintCalendarTimeFormat as tinyint=NULL,
@tintDefaultClassType as tinyint=NULL,
@tintPriceBookDiscount as tinyint=0,
@bitAutoSerialNoAssign as bit=0,
@bitIsShowBalance AS BIT = 0,
@numIncomeAccID NUMERIC(18, 0) = 0,
@numCOGSAccID NUMERIC(18, 0) = 0,
@numAssetAccID NUMERIC(18, 0) = 0,
@IsEnableProjectTracking AS BIT = 0,
@IsEnableCampaignTracking AS BIT = 0,
@IsEnableResourceScheduling AS BIT = 0,
@ShippingServiceItem  AS NUMERIC(9)=0,
@numSOBizDocStatus AS NUMERIC(9)=0,
@bitAutolinkUnappliedPayment AS BIT=0,
@numDiscountServiceItemID AS NUMERIC(9)=0,
@vcShipToPhoneNumber as VARCHAR(50)='',
@vcGAUserEmailId as VARCHAR(50)='',
@vcGAUserPassword as VARCHAR(50)='',
@vcGAUserProfileId as VARCHAR(50)='',
@bitDiscountOnUnitPrice AS BIT=0,
@intShippingImageWidth INT = 0,
@intShippingImageHeight INT = 0,
@numTotalInsuredValue NUMERIC(10,2) = 0,
@numTotalCustomsValue NUMERIC(10,2) = 0,
@vcHideTabs VARCHAR(1000) = '',
@bitUseBizdocAmount BIT = 0,
@bitDefaultRateType BIT = 0,
@bitIncludeTaxAndShippingInCommission  BIT = 0,
@bitPurchaseTaxCredit BIT=0,
@bitLandedCost BIT=0,
@vcLanedCostDefault VARCHAR(10)='',
@bitMinUnitPriceRule BIT = 0,
@numAbovePercent AS NUMERIC(18,2),
@numBelowPercent AS NUMERIC(18,2),
@numBelowPriceField AS NUMERIC(18,0),
@vcUnitPriceApprover AS VARCHAR(100),
@numDefaultSalesPricing AS TINYINT,
@bitReOrderPoint AS BIT = 0,
@numReOrderPointOrderStatus AS NUMERIC(18,0) = 0,
@numPODropShipBizDoc AS NUMERIC(18,0) = 0,
@numPODropShipBizDocTemplate AS NUMERIC(18,0) = 0,
@IsEnableDeferredIncome AS BIT = 0,
@bitApprovalforTImeExpense AS BIT=0,
@numCost AS Numeric(9,0)=0,
@intTimeExpApprovalProcess int=0,
@bitApprovalforOpportunity AS BIT=0,
@intOpportunityApprovalProcess int=0,
@numDefaultSalesShippingDoc NUMERIC(18,0)=0,
@numDefaultPurchaseShippingDoc NUMERIC(18,0)=0,
@bitchkOverRideAssignto AS BIT=0,
@vcPrinterIPAddress VARCHAR(15) = '',
@vcPrinterPort VARCHAR(5) = '',
@numDefaultSiteID NUMERIC(18,0) = 0
as                                      
BEGIN
BEGIN TRY
BEGIN TRANSACTION

	IF ISNULL(@bitMinUnitPriceRule,0) = 1 AND ((SELECT COUNT(*) FROM UnitPriceApprover WHERE numDomainID=@numDomainID) = 0 OR ISNULL(@bitApprovalforOpportunity,0)=0 OR ISNULL(@intOpportunityApprovalProcess,0) = 0)
	BEGIN
		RAISERROR('INCOMPLETE_MIN_UNIT_PRICE_CONFIGURATION',16,1)
		RETURN
	END


	If ISNULL(@bitApprovalforTImeExpense,0) = 1 AND (SELECT
														COUNT(*) 
													FROM
														UserMaster
													LEFT JOIN
														ApprovalConfig
													ON
														ApprovalConfig.numUserId = UserMaster.numUserDetailId
													WHERE
														UserMaster.numDomainID=@numDomainID
														AND ISNULL(bitActivateFlag,0) = 1
														AND ISNULL(numLevel1Authority,0) = 0
														AND ISNULL(numLevel2Authority,0) = 0
														AND ISNULL(numLevel3Authority,0) = 0
														AND ISNULL(numLevel4Authority,0) = 0
														AND ISNULL(numLevel5Authority,0) = 0) > 0
	BEGIN
		RAISERROR('INCOMPLETE_TIMEEXPENSE_APPROVAL',16,1)
		RETURN
	END

                                  
 update Domain                                       
   set                                       
   vcDomainName=@vcDomainName,                                      
   vcDomainDesc=@vcDomainDesc,                                      
   bitExchangeIntegration=@bitExchangeIntegration,                                      
   bitAccessExchange=@bitAccessExchange,                                      
   vcExchUserName=@vcExchUserName,                                      
   vcExchPassword=@vcExchPassword,                                      
   vcExchPath=@vcExchPath,                                      
   vcExchDomain=@vcExchDomain,                                    
   tintCustomPagingRows =@tintCustomPagingRows,                                    
   vcDateFormat=@vcDateFormat,                                    
   numDefCountry =@numDefCountry,                                    
   tintComposeWindow=@tintComposeWindow,                                    
   sintStartDate =@sintStartDate,                                    
   sintNoofDaysInterval=@sintNoofDaysInterval,                                    
   tintAssignToCriteria=@tintAssignToCriteria,                                    
   bitIntmedPage=@bitIntmedPage,                                    
   tintFiscalStartMonth=@tintFiscalStartMonth,                            
--   bitDeferredIncome=@bitDeferredIncome,                          
   tintPayPeriod=@tintPayPeriod,
   vcCurrency=isnull((select varCurrSymbol from Currency Where numCurrencyID=@numCurrencyID  ),'')  ,                      
  numCurrencyID=@numCurrencyID,                
  charUnitSystem= @charUnitSystem,              
  vcPortalLogo=@vcPortalLogo ,            
  tintChrForComSearch=@tintChrForComSearch  ,      
  intPaymentGateWay=@intPaymentGateWay,
 tintChrForItemSearch=@tintChrForItemSearch,
 numShipCompany= @ShipCompany,
 bitAutoPopulateAddress = @bitAutoPopulateAddress,
 tintPoulateAddressTo =@tintPoulateAddressTo,
 --bitMultiCompany=@bitMultiCompany ,
 bitMultiCurrency=@bitMultiCurrency,
 bitCreateInvoice= @bitCreateInvoice,
 [numDefaultSalesBizDocId] =@numDefaultSalesBizDocId,
 bitSaveCreditCardInfo=@bitSaveCreditCardInfo,
 intLastViewedRecord=@intLastViewedRecord,
 [tintCommissionType]=@tintCommissionType,
 [tintBaseTax]=@tintBaseTax,
 [numDefaultPurchaseBizDocId]=@numDefaultPurchaseBizDocID,
 bitEmbeddedCost=@bitEmbeddedCost,
 numDefaultSalesOppBizDocId=@numDefaultSalesOppBizDocId,
 tintSessionTimeOut=@tintSessionTimeOut,
 tintDecimalPoints=@tintDecimalPoints,
 bitCustomizePortal=@bitCustomizePortal,
 tintBillToForPO = @tintBillToForPO,
 tintShipToForPO = @tintShipToForPO,
 tintOrganizationSearchCriteria=@tintOrganizationSearchCriteria,
-- vcPortalName=@vcPortalName,
 bitDocumentRepositary=@bitDocumentRepositary,
 --tintComAppliesTo=@tintComAppliesTo,THIS FIELD IS REMOVED FROM GLOBAL SETTINGS
 bitGtoBContact=@bitGtoBContact,
 bitBtoGContact=@bitBtoGContact,
 bitGtoBCalendar=@bitGtoBCalendar,
 bitBtoGCalendar=@bitBtoGCalendar,
 bitExpenseNonInventoryItem=@bitExpenseNonInventoryItem,
 bitInlineEdit=@bitInlineEdit,
 bitRemoveVendorPOValidation=@bitRemoveVendorPOValidation,
tintBaseTaxOnArea=@tintBaseTaxOnArea,
bitAmountPastDue = @bitAmountPastDue,
monAmountPastDue = @monAmountPastDue,
bitSearchOrderCustomerHistory=@bitSearchOrderCustomerHistory,
bitRentalItem=@bitRentalItem,
numRentalItemClass=@numRentalItemClass,
numRentalHourlyUOM=@numRentalHourlyUOM,
numRentalDailyUOM=@numRentalDailyUOM,
tintRentalPriceBasedOn=@tintRentalPriceBasedOn,
tintCalendarTimeFormat=@tintCalendarTimeFormat,
tintDefaultClassType=@tintDefaultClassType,
tintPriceBookDiscount=@tintPriceBookDiscount,
bitAutoSerialNoAssign=@bitAutoSerialNoAssign,
bitIsShowBalance = @bitIsShowBalance,
numIncomeAccID = @numIncomeAccID,
numCOGSAccID = @numCOGSAccID,
numAssetAccID = @numAssetAccID,
IsEnableProjectTracking = @IsEnableProjectTracking,
IsEnableCampaignTracking = @IsEnableCampaignTracking,
IsEnableResourceScheduling = @IsEnableResourceScheduling,
numShippingServiceItemID = @ShippingServiceItem,
numSOBizDocStatus=@numSOBizDocStatus,
bitAutolinkUnappliedPayment=@bitAutolinkUnappliedPayment,
numDiscountServiceItemID=@numDiscountServiceItemID,
vcShipToPhoneNo = @vcShipToPhoneNumber,
vcGAUserEMail = @vcGAUserEmailId,
vcGAUserPassword = @vcGAUserPassword,
vcGAUserProfileId = @vcGAUserProfileId,
bitDiscountOnUnitPrice=@bitDiscountOnUnitPrice,
intShippingImageWidth = @intShippingImageWidth ,
intShippingImageHeight = @intShippingImageHeight,
numTotalInsuredValue = @numTotalInsuredValue,
numTotalCustomsValue = @numTotalCustomsValue,
vcHideTabs = @vcHideTabs,
bitUseBizdocAmount = @bitUseBizdocAmount,
bitDefaultRateType = @bitDefaultRateType,
bitIncludeTaxAndShippingInCommission = @bitIncludeTaxAndShippingInCommission,
/*,bitAllowPPVariance=@bitAllowPPVariance*/
bitPurchaseTaxCredit=@bitPurchaseTaxCredit,
bitLandedCost=@bitLandedCost,
vcLanedCostDefault=@vcLanedCostDefault,
bitMinUnitPriceRule = @bitMinUnitPriceRule,
numDefaultSalesPricing = @numDefaultSalesPricing,
bitReOrderPoint=@bitReOrderPoint,
numReOrderPointOrderStatus=@numReOrderPointOrderStatus,
numPODropShipBizDoc=@numPODropShipBizDoc,
numPODropShipBizDocTemplate=@numPODropShipBizDocTemplate,
IsEnableDeferredIncome=@IsEnableDeferredIncome,
bitApprovalforTImeExpense=@bitApprovalforTImeExpense,
numCost= @numCost,intTimeExpApprovalProcess=@intTimeExpApprovalProcess,
bitApprovalforOpportunity=@bitApprovalforOpportunity,intOpportunityApprovalProcess=@intOpportunityApprovalProcess,
 numDefaultPurchaseShippingDoc=@numDefaultPurchaseShippingDoc,numDefaultSalesShippingDoc=@numDefaultSalesShippingDoc,
 bitchkOverRideAssignto=@bitchkOverRideAssignto,
 vcPrinterIPAddress=@vcPrinterIPAddress,
 vcPrinterPort=@vcPrinterPort,
 numDefaultSiteID=ISNULL(@numDefaultSiteID,0)
 where numDomainId=@numDomainID

COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH

END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetItemPriceBasedOnMethod')
DROP PROCEDURE USP_GetItemPriceBasedOnMethod
GO
CREATE PROCEDURE [dbo].[USP_GetItemPriceBasedOnMethod]
	@numDomainID NUMERIC(18,0),
	@numDivisionID NUMERIC(18,0),
	@tintOppType AS TINYINT,
	@numItemCode NUMERIC(18,0),
	@tintPriceMethod TINYINT,
	@monListPrice AS MONEY,
	@monVendorCost AS MONEY,
	@numQty INT,
	@fltUOMConversionFactor AS FLOAT
AS
BEGIN
	DECLARE @tintPriceLevel AS TINYINT
	DECLARE @tintRuleType AS TINYINT
	DECLARE @tintDisountType AS TINYINT
	DECLARE @decDiscount AS FLOAT
	DECLARE @monPrice AS MONEY = 0
	DECLARE @monFinalPrice AS MONEY = 0
	DECLARE @numRelationship NUMERIC(18,0)
	DECLARE @numProfile NUMERIC(18,0)
	DECLARE @numItemClassification AS NUMERIC(18,0)
	DECLARE @tintPriceBookDiscount AS TINYINT

	SELECT @numItemClassification=numItemClassification FROM Item WHERE numItemCode=@numItemCode

	SELECT @tintPriceBookDiscount=tintPriceBookDiscount FROM Domain WHERE numDomainId=@numDomainID

	-- GET ORGANIZATION DETAL
	SELECT 
		@numRelationship=numCompanyType,
		@numProfile=vcProfile,
		@tintPriceLevel = ISNULL(D.tintPriceLevel,0)
	FROM 
		DivisionMaster D                  
	JOIN 
		CompanyInfo C 
	ON 
		C.numCompanyId=D.numCompanyID                  
	WHERE 
		numDivisionID =@numDivisionID       

	IF @tintPriceMethod= 1 -- PRICE LEVEL
	BEGIN
		IF (SELECT COUNT(*) FROM PricingTable WHERE numItemCode=@numItemCode AND ISNULL(numPriceRuleID,0) = 0) > 0
		BEGIN
			SELECT
				@tintDisountType = 1,
				@decDiscount = 0,
				@tintRuleType = tintRuleType,
				@monPrice = (CASE tintRuleType
										WHEN 1 -- Deduct From List Price
										THEN
											CASE tintDiscountType 
												WHEN 1 -- PERCENT
												THEN (CASE WHEN @monListPrice > 0 THEN @monListPrice - (@monListPrice * (ISNULL(decDiscount,0)/100)) ELSE 0 END)
												WHEN 2 -- FLAT AMOUNT
												THEN (CASE WHEN @monListPrice > 0 THEN (CASE WHEN (@monListPrice * @numQty * @fltUOMConversionFactor) - ISNULL(decDiscount,0) > 0 THEN ((@monListPrice * @numQty * @fltUOMConversionFactor) - ISNULL(decDiscount,0)) / (@numQty * @fltUOMConversionFactor) ELSE 0 END) ELSE 0 END)
												WHEN 3 -- NAMED PRICE
												THEN ISNULL(decDiscount,0)
											END
										WHEN 2 -- Add to primary vendor cost
										THEN
											CASE tintDiscountType 
												WHEN 1  -- PERCENT
												THEN @monVendorCost + (@monVendorCost * (ISNULL(decDiscount,0)/100)) * @fltUOMConversionFactor
												WHEN 2 -- FLAT AMOUNT
												THEN (CASE WHEN (@monVendorCost * @numQty * @fltUOMConversionFactor) + ISNULL(decDiscount,0) > 0 THEN ((@monVendorCost * @numQty * @fltUOMConversionFactor) + ISNULL(decDiscount,0)) / (@numQty * @fltUOMConversionFactor)  ELSE 0 END)
												WHEN 3 -- NAMED PRICE
												THEN ISNULL(decDiscount,0)
											END
										WHEN 3 -- Named price
										THEN
											CASE 
											WHEN ISNULL(@tintPriceLevel,0) > 0
											THEN
												(SELECT 
													 ISNULL(decDiscount,0)
												FROM
												(
													SELECT 
														ROW_NUMBER() OVER(PARTITION BY PricingTable.numItemCode ORDER BY numPricingID) Id,
														decDiscount
													FROM 
														PricingTable 
													WHERE 
														PricingTable.numItemCode = @numItemCode 
														AND tintRuleType = 3 
												) TEMP
												WHERE
													Id = @tintPriceLevel)
											ELSE
												ISNULL(decDiscount,0)
											END
										END
									) 
			FROM
				PricingTable
			WHERE
				numItemCode=@numItemCode AND (@numQty * @fltUOMConversionFactor) >= intFromQty AND (@numQty * @fltUOMConversionFactor) <= intToQty
		END

		SELECT 
			ISNULL(@monPrice,0) AS monPrice,
			ISNULL(@tintRuleType,1) AS tintRuleType,
			1 AS tintDisountType,
			0 AS decDiscount
	END
	ELSE IF (SELECT COUNT(*) FROM PriceBookRules WHERE numDomainID=@numDomainID AND tintRuleFor=@tintOppType) > 0 -- PRICE RULE
	BEGIN
		DECLARE @numPriceRuleID NUMERIC(18,0)
		DECLARE @tintPriceRuleType AS TINYINT
		DECLARE @tintPricingMethod TINYINT
		DECLARE @tintPriceBookDiscountType TINYINT
		DECLARE @decPriceBookDiscount FLOAT
		DECLARE @intQntyItems INT
		DECLARE @decMaxDedPerAmt FLOAT

		SELECT 
			TOP 1
			@numPriceRuleID = numPricRuleID,
			@tintPricingMethod = tintPricingMethod,
			@tintPriceRuleType = p.tintRuleType,
			@tintPriceBookDiscountType = P.[tintDiscountType],
			@decPriceBookDiscount = P.[decDiscount],
			@intQntyItems = P.[intQntyItems],
			@decMaxDedPerAmt = P.[decMaxDedPerAmt]
		FROM   
			PriceBookRules P 
		LEFT JOIN 
			PriceBookRuleDTL PDTL 
		ON 
			P.numPricRuleID = PDTL.numRuleID
		LEFT JOIN 
			PriceBookRuleItems PBI 
		ON 
			P.numPricRuleID = PBI.numRuleID
		LEFT JOIN 
			[PriceBookPriorities] PP 
		ON 
			PP.[Step2Value] = P.[tintStep2] 
			AND PP.[Step3Value] = P.[tintStep3]
		WHERE  
			P.numDomainID=@numDomainID 
			AND tintRuleFor=@tintOppType
			AND (
					((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 1 AND PDTL.numValue = @numDivisionID)) -- Priority 1
					OR ((tintStep2 = 2 AND PBI.numValue = @numItemClassification) AND (tintStep3= 1 AND PDTL.numValue = @numDivisionID)) -- Priority 2
					OR ((tintStep2 = 3) AND (tintStep3 = 1 AND PDTL.numValue = @numDivisionID)) -- Priority 3

					OR ((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 4
					OR ((tintStep2 = 2 AND PBI.numValue = @numItemClassification) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 5
					OR ((tintStep2 = 3) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 6

					OR ((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 3)) -- Priority 7
					OR ((tintStep2 = 2 AND PBI.numValue = @numItemClassification) AND (tintStep3 = 3)) -- Priority 8
					OR ((tintStep2 = 3) and (tintStep3 = 3)) -- Priority 9
				)
		ORDER BY 
			PP.Priority ASC


		IF ISNULL(@numPriceRuleID,0) > 0
		BEGIN
			IF @tintPricingMethod = 1 -- BASED ON PRICE TABLE
			BEGIN
				SELECT
					@tintDisountType = tintDiscountType,
					@decDiscount = ISNULL(decDiscount,0),
					@tintRuleType = tintRuleType,
					@monPrice = (CASE tintRuleType WHEN 1 THEN @monListPrice WHEN 2 THEN @monVendorCost END) ,
					@monFinalPrice = (CASE tintRuleType
											WHEN 1 -- Deduct From List Price
											THEN
												CASE tintDiscountType 
													WHEN 1 -- PERCENT
													THEN (CASE WHEN @monListPrice > 0 THEN @monListPrice - (@monListPrice * (ISNULL(decDiscount,0)/100)) ELSE 0 END)
													WHEN 2 -- FLAT AMOUNT
													THEN (CASE WHEN @monListPrice > 0 THEN (CASE WHEN (@monListPrice * @numQty * @fltUOMConversionFactor) - ISNULL(decDiscount,0) > 0 THEN ((@monListPrice * @numQty * @fltUOMConversionFactor) - ISNULL(decDiscount,0)) / (@numQty * @fltUOMConversionFactor) ELSE 0 END) ELSE 0 END)
													WHEN 3 -- NAMED PRICE
													THEN ISNULL(decDiscount,0)
												END
											WHEN 2 -- Add to primary vendor cost
											THEN
												CASE tintDiscountType 
													WHEN 1  -- PERCENT
													THEN @monVendorCost + (@monVendorCost * (ISNULL(decDiscount,0)/100))
													WHEN 2 -- FLAT AMOUNT
													THEN (CASE WHEN (@monVendorCost * @numQty * @fltUOMConversionFactor) + ISNULL(decDiscount,0) > 0 THEN ((@monVendorCost * @numQty * @fltUOMConversionFactor) + ISNULL(decDiscount,0)) / (@numQty * @fltUOMConversionFactor)  ELSE 0 END)
													WHEN 3 -- NAMED PRICE
													THEN ISNULL(decDiscount,0)
												END
											WHEN 3 -- Named price
											THEN
												CASE 
												WHEN ISNULL(@tintPriceLevel,0) > 0
												THEN
													(SELECT 
														 ISNULL(decDiscount,0)
													FROM
													(
														SELECT 
															ROW_NUMBER() OVER(PARTITION BY PricingTable.numItemCode ORDER BY numPricingID) Id,
															decDiscount
														FROM 
															PricingTable 
														WHERE 
															PricingTable.numItemCode = @numItemCode 
															AND tintRuleType = 3 
													) TEMP
													WHERE
														Id = @tintPriceLevel)
												ELSE
													ISNULL(decDiscount,0)
												END
											END
										) 
				FROM
					PricingTable
				WHERE
					numPriceRuleID=@numPriceRuleID AND @numQty * @fltUOMConversionFactor >= intFromQty AND @numQty * @fltUOMConversionFactor <= intToQty
			END
			ELSE IF @tintPricingMethod = 2 -- BASED ON RULE
			BEGIN
				SET @monPrice = (CASE WHEN @tintPriceRuleType = 2 THEN @monVendorCost ELSE @monListPrice END)

				IF (@tintPriceBookDiscountType = 1 ) -- Percentage 
				BEGIN
					SET @tintDisountType = @tintPriceBookDiscountType
					SET @decDiscount = @decDiscount * (@numQty/@intQntyItems);
					SET @decDiscount = @decDiscount * (CASE WHEN @tintPriceRuleType = 2 THEN @monVendorCost ELSE @monListPrice END) /100;
					SET @decMaxDedPerAmt = @decMaxDedPerAmt * (CASE WHEN @tintPriceRuleType = 2 THEN @monVendorCost ELSE @monListPrice END) /100;
					
					IF (@decDiscount > @decMaxDedPerAmt)
						SET @decDiscount = @decMaxDedPerAmt;
				
					IF ( @tintPriceRuleType = 1 )
							SET @monFinalPrice = (@monListPrice - @decDiscount);
					IF ( @tintPriceRuleType = 2 )
							SET @monFinalPrice = (@monVendorCost + @decDiscount);
				END
				IF ( @tintPriceBookDiscountType = 2 ) -- Flat discount 
				BEGIN
					SET @decDiscount = @decDiscount * ((@numQty * @fltUOMConversionFactor)/@intQntyItems);

					IF (@decDiscount > @decMaxDedPerAmt)
						SET @decDiscount = @decMaxDedPerAmt;
					
					IF ( @tintPriceRuleType = 1 )
						SET @monFinalPrice =  ((@monListPrice * @numQty * @fltUOMConversionFactor) - @decDiscount) / (@numQty * @fltUOMConversionFactor);
						
					IF ( @tintPriceRuleType = 2 )
						SET @monFinalPrice = ((@monVendorCost * @numQty * @fltUOMConversionFactor) + @decDiscount) / (@numQty * @fltUOMConversionFactor);
				END
			END
		END
		

		SELECT 
			(CASE WHEN @tintPriceBookDiscount = 1 THEN ISNULL(@monPrice,0) ELSE ISNULL(@monFinalPrice,0) END) AS monPrice,
			ISNULL(@tintRuleType,1) AS tintRuleType,
			(CASE WHEN @tintPriceBookDiscount = 1 THEN ISNULL(@tintDisountType,1) ELSE 1 END) AS tintDisountType,
			(CASE WHEN @tintPriceBookDiscount = 1 THEN ISNULL(@decDiscount,0) ELSE 0 END) AS decDiscount
	END
END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetOrderSelectedItemDetail')
DROP PROCEDURE USP_GetOrderSelectedItemDetail
GO
CREATE PROCEDURE [dbo].[USP_GetOrderSelectedItemDetail]
	@numDomainID NUMERIC(18,0),
	@numDivisionID NUMERIC(18,0),
	@tintOppType AS TINYINT,
	@numItemCode NUMERIC(18,0),
	@numUOMID NUMERIC(18,0),
	@numWarehouseItemID AS NUMERIC(18,0),
	@numQty INT,
	@monPrice FLOAT,
	@numOppID NUMERIC(18,0),
	@numOppItemID NUMERIC(18,0),
	@vcSelectedKitChildItems VARCHAR(MAX),
	@numShippingCountry AS NUMERIC(18,0),
	@numWarehouseID AS NUMERIC(18,0),
	@vcCoupon AS VARCHAR(200)
AS
BEGIN
	DECLARE @vcItemName AS VARCHAR(500)
	DECLARE @bitDropship AS BIT
	DECLARE @bitMatrix AS BIT
	DECLARE @chrItemType AS CHAR(1)
	DECLARE @numItemGroup AS NUMERIC(18,0)
	DECLARE @numBaseUnit AS NUMERIC(18,0)
	DECLARE @numSaleUnit AS NUMERIC(18,0)
	DECLARE @fltUOMConversionFactor AS FLOAT
	DECLARE @numPurchaseUnit AS NUMERIC(18,0)
	DECLARE @bitAssemblyOrKit BIT
	DECLARE @bitAssembly AS BIT
	DECLARE @bitCalAmtBasedonDepItems AS BIT
	DECLARE @ItemDesc AS VARCHAR(1000)
	DECLARE @numItemClassification AS NUMERIC(18,0)

	DECLARE @CalPrice AS MONEY = 0.0000
	DECLARE @monListPrice AS MONEY = 0.0000
	DECLARE @monVendorCost AS MONEY = 0.0000
	DECLARE @tintPricingBasedOn AS TINYINT = 0.0000

	DECLARE @tintRuleType AS TINYINT
	DECLARE @monPriceLevelPrice AS MONEY = 0.0000
	DECLARE @monPriceFinalPrice AS MONEY = 0.0000
	DECLARE @monPriceRulePrice AS MONEY = 0.0000
	DECLARE @monPriceRuleFinalPrice AS MONEY = 0.0000
	DECLARE @monLastPrice AS MONEY = 0.0000
	DECLARE @dtLastOrderDate VARCHAR(200) = ''

	DECLARE @tintDisountType TINYINT
	DECLARE @decDiscount AS FLOAT

	DECLARE @numVendorID AS NUMERIC(18,0)
	DECLARE @numRelationship NUMERIC(18,0)
	DECLARE @numProfile NUMERIC(18,0)
	DECLARE @tintPriceLevel AS TINYINT
	DECLARE @tintDefaultSalesPricing AS TINYINT
	DECLARE @tintPriceBookDiscount AS TINYINT

	SELECT @tintDefaultSalesPricing = numDefaultSalesPricing,@tintPriceBookDiscount=tintPriceBookDiscount FROM Domain WHERE numDomainId=@numDomainID

	IF ISNULL(@numWarehouseID,0) > 0
	BEGIN
		SELECT TOP 1 @numWarehouseItemID=numWareHouseItemID FROM WareHouseItems WHERE numItemID=@numItemCode AND numWareHouseID=@numWarehouseID ORDER BY numWareHouseItemID ASC
	END

	-- GET ORGANIZATION DETAL
	SELECT 
		@numRelationship=numCompanyType,
		@numProfile=vcProfile,
		@tintPriceLevel = ISNULL(D.tintPriceLevel,0)
	FROM 
		DivisionMaster D                  
	JOIN 
		CompanyInfo C 
	ON 
		C.numCompanyId=D.numCompanyID                  
	WHERE 
		numDivisionID =@numDivisionID       

	-- GET ITEM DETAILS
	SELECT
		@vcItemName = vcItemName,
		@bitDropship = bitAllowDropShip,
		@ItemDesc = Item.txtItemDesc,
		@bitMatrix = ISNULL(Item.bitMatrix,0),
		@chrItemType = charItemType,
		@numItemGroup = numItemGroup,
		@bitAssembly = ISNULL(bitAssembly,0),
		@numBaseUnit = ISNULL(numBaseUnit,0),
		@numSaleUnit = (CASE WHEN ISNULL(@numUOMID,0) > 0 THEN @numUOMID ELSE ISNULL(numSaleUnit,0) END),
		@numPurchaseUnit = (CASE WHEN ISNULL(@numUOMID,0) > 0 THEN @numUOMID ELSE ISNULL(numPurchaseUnit,0) END),
		@numItemClassification = numItemClassification,
		@bitAssemblyOrKit = (CASE WHEN ISNULL(bitKitParent,0)=1 OR ISNULL(bitAssembly,0)=1 THEN 1 ELSE 0 END),
		@monListPrice = (CASE WHEN Item.charItemType='P' THEN ISNULL(WareHouseItems.monWListPrice,0) ELSE ISNULL(Item.monListPrice,0) END),
		@monVendorCost = ISNULL((SELECT monCost FROM Vendor WHERE numVendorID=Item.numVendorID AND Vendor.numItemCode=Item.numItemCode),0),
		@numVendorID = ISNULL(Item.numVendorID,0),
		@numWarehouseItemID = (CASE WHEN ISNULL(@numWarehouseItemID,0) > 0 THEN @numWarehouseItemID ELSE WareHouseItems.numWareHouseItemID END)
	FROM
		Item
	OUTER APPLY
	(
		SELECT TOP 1 numWareHouseItemID,monWListPrice FROM WareHouseItems WHERE numItemID=@numItemCode AND (numWareHouseItemID = @numWarehouseItemID OR ISNULL(@numWarehouseItemID,0) = 0)
	) AS WareHouseItems
	WHERE
		Item.numItemCode = @numItemCode


	IF @chrItemType = 'P' AND ISNULL(@numWarehouseItemID,0) = 0
	BEGIN
		RAISERROR('WAREHOUSE_DOES_NOT_EXISTS',16,1)
	END

	SET @fltUOMConversionFactor = dbo.fn_UOMConversion((CASE WHEN  @tintOppType = 1 THEN @numSaleUnit ELSE @numPurchaseUnit END),@numItemCode,@numDomainId,@numBaseUnit)

	IF @tintOppType = 1
	BEGIN
		/*Use Calculated Price When item is Assembly/Kit,Calculate price based on sum of dependent items.*/
		IF @bitCalAmtBasedonDepItems = 1 
		BEGIN
			CREATE TABLE #TEMPSelectedKitChilds
			(
				ChildKitItemID NUMERIC(18,0),
				ChildKitWarehouseItemID NUMERIC(18,0),
				ChildKitChildItemID NUMERIC(18,0),
				ChildKitChildWarehouseItemID NUMERIC(18,0)
			)

			IF ISNULL(@numOppItemID,0) > 0
			BEGIN
				INSERT INTO 
					#TEMPSelectedKitChilds
				SELECT 
					OKI.numChildItemID,
					OKI.numWareHouseItemId,
					OKCI.numItemID,
					OKCI.numWareHouseItemId
				FROM
					OpportunityKitItems OKI
				INNER JOIN
					OpportunityKitChildItems OKCI
				ON
					OKI.numOppChildItemID = OKCI.numOppChildItemID
				WHERE
					OKI.numOppId = @numOppID
					AND OKI.numOppItemID = @numOppItemID
			END
			ELSE IF ISNULL(@vcSelectedKitChildItems,'') <> ''
			BEGIN
				INSERT INTO 
					#TEMPSelectedKitChilds
				SELECT 
					SUBSTRING
					(
						SUBSTRING(OutParam,0,CHARINDEX('-',OutParam)),
						0,
						CHARINDEX('#', SUBSTRING(OutParam,0,CHARINDEX('-',OutParam)))
					) AS numChildKitID,
					SUBSTRING
					(
						SUBSTRING(OutParam,0,CHARINDEX('-',OutParam)),
						CHARINDEX('#', SUBSTRING(OutParam,0,CHARINDEX('-',OutParam))) + 1,
						LEN(SUBSTRING(OutParam,0,CHARINDEX('-',OutParam)))
					) AS numChildKitWarehouseItemID,
					SUBSTRING
					(
						SUBSTRING(OutParam,CHARINDEX('-',OutParam) + 1, LEN(OutParam)),
						0,
						CHARINDEX('#', SUBSTRING(OutParam,CHARINDEX('-',OutParam) + 1, LEN(OutParam)))
					) AS numChildKitID,
					SUBSTRING
					(
						SUBSTRING(OutParam,CHARINDEX('-',OutParam) + 1, LEN(OutParam)),
						CHARINDEX('#', SUBSTRING(OutParam,CHARINDEX('-',OutParam) + 1, LEN(OutParam))) + 1,
						LEN(SUBSTRING(OutParam,CHARINDEX('-',OutParam) + 1, LEN(OutParam)))
					) AS numChildKitWarehouseItemID
		
				FROM 
					dbo.SplitString(@vcSelectedKitChildItems,',')
			END

			;WITH CTE (numItemCode, vcItemName, bitKitParent, numWarehouseItemID,numQtyItemsReq,numUOMId) AS
			(
				SELECT
					ID.numChildItemID,
					I.vcItemName,
					ISNULL(I.bitKitParent,0),
					ISNULL(ID.numWarehouseItemId,0),
					(ISNULL(ID.numQtyItemsReq,0) * ISNULL(dbo.fn_UOMConversion(ISNULL(ID.numUOMID, 0), I.numItemCode, @numDomainID, ISNULL(I.numBaseUnit, 0)),1)),
					ISNULL(numUOMId,0)
				FROM 
					[ItemDetails] ID
				INNER JOIN 
					[Item] I 
				ON 
					ID.[numChildItemID] = I.[numItemCode]
				WHERE   
					[numItemKitID] = @numItemCode
				UNION ALL
				SELECT
					ID.numChildItemID,
					I.vcItemName,
					ISNULL(I.bitKitParent,0),
					ISNULL(ID.numWarehouseItemId,0),
					(ISNULL(ID.numQtyItemsReq,0) * ISNULL(dbo.fn_UOMConversion(ISNULL(ID.numUOMID, 0), I.numItemCode, @numDomainID, ISNULL(I.numBaseUnit, 0)),1)),
					ISNULL(ID.numUOMId,0)
				FROM 
					CTE As Temp1
				INNER JOIN
					[ItemDetails] ID
				ON
					ID.numItemKitID = Temp1.numItemCode
				INNER JOIN 
					[Item] I 
				ON 
					ID.[numChildItemID] = I.[numItemCode]
				INNER JOIN
					#TEMPSelectedKitChilds
				ON
					ISNULL(Temp1.bitKitParent,0) = 1
					AND ID.numChildItemID = #TEMPSelectedKitChilds.ChildKitChildItemID
					AND ID.numWareHouseItemId = #TEMPSelectedKitChilds.ChildKitChildWarehouseItemID
			)

			SELECT  
					@CalPrice = ISNULL(  SUM(ISNULL(CASE WHEN I.[charItemType]='P' 
					THEN WI.[monWListPrice] 
					ELSE monListPrice END,0) * ID.[numQtyItemsReq] )  ,0)
			FROM    CTE ID
					INNER JOIN [Item] I ON ID.numItemCode = I.[numItemCode]
					left join  WareHouseItems WI
					on WI.numItemID=I.numItemCode and WI.[numWareHouseItemID]=ID.numWarehouseItemID
			WHERE
				ISNULL(ID.bitKitParent,0) = 0

			DROP TABLE #TEMPSelectedKitChilds
		END

		IF @tintDefaultSalesPricing = 1 AND (SELECT COUNT(*) FROM PricingTable WHERE numItemCode=@numItemCode AND ISNULL(numPriceRuleID,0) = 0) > 0 -- PRICE LEVEL
		BEGIN
			SELECT
				@tintDisountType = tintDiscountType,
				@decDiscount = ISNULL(decDiscount,0),
				@tintRuleType = tintRuleType,
				@monPriceLevelPrice = (CASE tintRuleType WHEN 1 THEN @monListPrice WHEN 2 THEN @monVendorCost ELSE ISNULL(decDiscount,0) END),
				@monPriceFinalPrice = (CASE tintRuleType
										WHEN 1 -- Deduct From List Price
										THEN
											CASE tintDiscountType 
												WHEN 1 -- PERCENT
												THEN (CASE WHEN @monListPrice > 0 THEN @monListPrice - (@monListPrice * (ISNULL(decDiscount,0)/100)) ELSE 0 END)
												WHEN 2 -- FLAT AMOUNT
												THEN (CASE WHEN @monListPrice > 0 THEN (CASE WHEN (@monListPrice * @numQty * @fltUOMConversionFactor) - ISNULL(decDiscount,0) > 0 THEN ((@monListPrice * @numQty * @fltUOMConversionFactor) - ISNULL(decDiscount,0)) / (@numQty * @fltUOMConversionFactor) ELSE 0 END) ELSE 0 END)
												WHEN 3 -- NAMED PRICE
												THEN ISNULL(decDiscount,0)
											END
										WHEN 2 -- Add to primary vendor cost
										THEN
											CASE tintDiscountType 
												WHEN 1  -- PERCENT
												THEN @monVendorCost + (@monVendorCost * (ISNULL(decDiscount,0)/100))  * @fltUOMConversionFactor
												WHEN 2 -- FLAT AMOUNT
												THEN (CASE WHEN (@monVendorCost * @numQty * @fltUOMConversionFactor) + ISNULL(decDiscount,0) > 0 THEN ((@monVendorCost * @numQty * @fltUOMConversionFactor) + ISNULL(decDiscount,0)) / (@numQty * @fltUOMConversionFactor)  ELSE 0 END)
												WHEN 3 -- NAMED PRICE
												THEN ISNULL(decDiscount,0)
											END
										WHEN 3 -- Named price
										THEN
											ISNULL(decDiscount,0)
										END
									) 
			FROM
			(
				SELECT 
					ROW_NUMBER() OVER(PARTITION BY PricingTable.numItemCode ORDER BY numPricingID) Id,
					*
				FROM
					PricingTable
				WHERE 
					PricingTable.numItemCode = @numItemCode
					
			) TEMP
			WHERE
				numItemCode=@numItemCode AND ((tintRuleType = 3 AND Id=@tintPriceLevel) OR @numQty * @fltUOMConversionFactor >= intFromQty AND @numQty * @fltUOMConversionFactor <= intToQty)
		END
		ELSE IF @tintDefaultSalesPricing = 2 AND (SELECT COUNT(*) FROM PriceBookRules WHERE numDomainID=@numDomainID AND tintRuleFor=@tintOppType) > 0 -- PRICE RULE
		BEGIN
			DECLARE @numPriceRuleID NUMERIC(18,0)
			DECLARE @tintPriceRuleType AS TINYINT
			DECLARE @tintPricingMethod TINYINT
			DECLARE @tintPriceBookDiscountType TINYINT
			DECLARE @decPriceBookDiscount FLOAT
			DECLARE @intQntyItems INT
			DECLARE @decMaxDedPerAmt FLOAT

			SELECT 
				TOP 1
				@numPriceRuleID = numPricRuleID,
				@tintPricingMethod = tintPricingMethod,
				@tintPriceRuleType = p.tintRuleType,
				@tintPriceBookDiscountType = P.[tintDiscountType],
				@decPriceBookDiscount = P.[decDiscount],
				@intQntyItems = P.[intQntyItems],
				@decMaxDedPerAmt = P.[decMaxDedPerAmt]
			FROM   
				PriceBookRules P 
			LEFT JOIN 
				PriceBookRuleDTL PDTL 
			ON 
				P.numPricRuleID = PDTL.numRuleID
			LEFT JOIN 
				PriceBookRuleItems PBI 
			ON 
				P.numPricRuleID = PBI.numRuleID
			LEFT JOIN 
				[PriceBookPriorities] PP 
			ON 
				PP.[Step2Value] = P.[tintStep2] 
				AND PP.[Step3Value] = P.[tintStep3]
			WHERE  
				P.numDomainID=@numDomainID 
				AND tintRuleFor=@tintOppType
				AND (
						((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 1 AND PDTL.numValue = @numDivisionID)) -- Priority 1
						OR ((tintStep2 = 2 AND PBI.numValue = @numItemClassification) AND (tintStep3= 1 AND PDTL.numValue = @numDivisionID)) -- Priority 2
						OR ((tintStep2 = 3) AND (tintStep3 = 1 AND PDTL.numValue = @numDivisionID)) -- Priority 3

						OR ((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 4
						OR ((tintStep2 = 2 AND PBI.numValue = @numItemClassification) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 5
						OR ((tintStep2 = 3) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 6

						OR ((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 3)) -- Priority 7
						OR ((tintStep2 = 2 AND PBI.numValue = @numItemClassification) AND (tintStep3 = 3)) -- Priority 8
						OR ((tintStep2 = 3) and (tintStep3 = 3)) -- Priority 9
					)
			ORDER BY 
				PP.Priority ASC


			IF ISNULL(@numPriceRuleID,0) > 0
			BEGIN
				IF @tintPricingMethod = 1 -- BASED ON PRICE TABLE
				BEGIN
					SELECT
						@tintDisountType = tintDiscountType,
						@decDiscount = ISNULL(decDiscount,0),
						@tintRuleType = tintRuleType,
						@monPriceRulePrice = (CASE tintRuleType WHEN 1 THEN @monListPrice WHEN 2 THEN @monVendorCost END) ,
						@monPriceRuleFinalPrice = (CASE tintRuleType
												WHEN 1 -- Deduct From List Price
												THEN
													CASE tintDiscountType 
														WHEN 1 -- PERCENT
														THEN (CASE WHEN @monListPrice > 0 THEN @monListPrice - (@monListPrice * (ISNULL(decDiscount,0)/100)) ELSE 0 END)
														WHEN 2 -- FLAT AMOUNT
														THEN (CASE WHEN @monListPrice > 0 THEN (CASE WHEN (@monListPrice * @numQty * @fltUOMConversionFactor) - ISNULL(decDiscount,0) > 0 THEN ((@monListPrice * @numQty * @fltUOMConversionFactor) - ISNULL(decDiscount,0)) / (@numQty * @fltUOMConversionFactor) ELSE 0 END) ELSE 0 END)
														WHEN 3 -- NAMED PRICE
														THEN ISNULL(decDiscount,0)
													END
												WHEN 2 -- Add to primary vendor cost
												THEN
													CASE tintDiscountType 
														WHEN 1  -- PERCENT
														THEN @monVendorCost + (@monVendorCost * (ISNULL(decDiscount,0)/100))
														WHEN 2 -- FLAT AMOUNT
														THEN (CASE WHEN (@monVendorCost * @numQty * @fltUOMConversionFactor) + ISNULL(decDiscount,0) > 0 THEN ((@monVendorCost * @numQty * @fltUOMConversionFactor) + ISNULL(decDiscount,0)) / (@numQty * @fltUOMConversionFactor)  ELSE 0 END)
														WHEN 3 -- NAMED PRICE
														THEN ISNULL(decDiscount,0)
													END
												WHEN 3 -- Named price
												THEN
													CASE 
													WHEN ISNULL(@tintPriceLevel,0) > 0
													THEN
														(SELECT 
															 ISNULL(decDiscount,0)
														FROM
														(
															SELECT 
																ROW_NUMBER() OVER(PARTITION BY PricingTable.numItemCode ORDER BY numPricingID) Id,
																decDiscount
															FROM 
																PricingTable 
															WHERE 
																PricingTable.numItemCode = @numItemCode 
																AND tintRuleType = 3 
														) TEMP
														WHERE
															Id = @tintPriceLevel)
													ELSE
														ISNULL(decDiscount,0)
													END
												END
											) 
					FROM
						PricingTable
					WHERE
						numPriceRuleID=@numPriceRuleID AND @numQty * @fltUOMConversionFactor >= intFromQty AND @numQty * @fltUOMConversionFactor <= intToQty
				END
				ELSE IF @tintPricingMethod = 2 -- BASED ON RULE
				BEGIN
					SET @tintRuleType = @tintPriceRuleType
					SET @monPriceRulePrice = (CASE @tintPriceRuleType WHEN 1 THEN @monListPrice WHEN 2 THEN @monVendorCost END) 

					IF (@tintPriceBookDiscountType = 1 ) -- Percentage 
					BEGIN
						SET @tintDisountType = @tintPriceBookDiscountType
						SET @decDiscount = @decDiscount * (@numQty/@intQntyItems);
						SET @decDiscount = @decDiscount * (CASE WHEN @tintPriceRuleType = 2 THEN @monVendorCost ELSE @monListPrice END) /100;
						SET @decMaxDedPerAmt = @decMaxDedPerAmt * (CASE WHEN @tintPriceRuleType = 2 THEN @monVendorCost ELSE @monListPrice END) /100;
					
						IF (@decDiscount > @decMaxDedPerAmt)
							SET @decDiscount = @decMaxDedPerAmt;
				
						IF ( @tintPriceRuleType = 1 )
								SET @monPriceRuleFinalPrice = (@monListPrice - @decDiscount);
						IF ( @tintPriceRuleType = 2 )
								SET @monPriceRuleFinalPrice = (@monVendorCost + @decDiscount);
					END
					IF ( @tintPriceBookDiscountType = 2 ) -- Flat discount 
					BEGIN
						SET @decDiscount = @decDiscount * ((@numQty * @fltUOMConversionFactor)/@intQntyItems);

						IF (@decDiscount > @decMaxDedPerAmt)
							SET @decDiscount = @decMaxDedPerAmt;
					
						IF ( @tintPriceRuleType = 1 )
							SET @monPriceRuleFinalPrice =  ((@monListPrice * @numQty * @fltUOMConversionFactor) - @decDiscount)/(@numQty * @fltUOMConversionFactor);
						IF ( @tintPriceRuleType = 2 )
							SET @monPriceRuleFinalPrice = ((@monVendorCost * @numQty * @fltUOMConversionFactor) + @decDiscount)/(@numQty * @fltUOMConversionFactor);
					END
				END
			END
		
		END
	
		-- GET Last Price
		SELECT 
			TOP 1 
			@monLastPrice = monPrice,
			@dtLastOrderDate = dbo.FormatedDateFromDate(OpportunityMaster.bintCreatedDate,@numDomainID)
		FROM 
			OpportunityItems 
		JOIN 
			OpportunityMaster 
		ON 
			OpportunityItems.numOppId = OpportunityMaster.numOppId 
		WHERE 
			OpportunityMaster.numDomainId= @numDomainID
			AND tintOppType = @tintOppType
			AND tintOppStatus = 1
			AND numItemCode=@numItemCode
		ORDER BY
			OpportunityMaster.numOppId DESC

		IF @tintDefaultSalesPricing = 1 AND ISNULL(@monPriceFinalPrice,0) > 0
		BEGIN
			-- WE ARE NOT DISPLAYING DISCOUNT SEPERATELY IN PRICE LEVEL
			SET @tintPricingBasedOn = 1
			SET @monPrice = @monPriceFinalPrice
			SET @tintDisountType = 1
			SET @decDiscount = 0
		END
		ELSE IF @tintDefaultSalesPricing = 2 AND ISNULL(@monPriceRuleFinalPrice,0) > 0
		BEGIN
			SET @tintPricingBasedOn = 2
		
			IF @tintPriceBookDiscountType = 1 -- Display Disocunt Price As Unit Price
			BEGIN
				SET @monPrice = @monPriceRuleFinalPrice
				SET @tintDisountType = 1
				SET @decDiscount = 0
			END
			ELSE -- Display Unit Price And Disocunt Price Seperately
			BEGIN
				SET @monPrice = @monPriceRulePrice
			END
		
		END
		ELSE IF @tintDefaultSalesPricing = 3 AND ISNULL(@monLastPrice,0) > 0
		BEGIN
			SET @tintPricingBasedOn = 3
			SET @monPrice = @monLastPrice
		END
		ELSE IF @bitCalAmtBasedonDepItems = 1 AND @CalPrice > 0 AND @tintOppType = 1
		BEGIN
			SET @tintPricingBasedOn = 4
			SET @monPrice = @CalPrice
			SET @tintRuleType = 0
			SET @tintDisountType = 1
			SET @decDiscount = 0
		END
		ELSE
		BEGIN
			SET @tintPricingBasedOn = 0
			SET @tintDisountType = 1
			SET @decDiscount = 0
			SET @tintRuleType = 0

			IF @tintOppType = 1
				SET @monPrice = @monListPrice
			ELSE
				SET @monPrice = @monVendorCost
		END
	END
	ELSE IF @tintOppType = 2
	BEGIN
		SELECT 
			TOP 1
			@numPriceRuleID = numPricRuleID,
			@tintPricingMethod = tintPricingMethod,
			@tintPriceRuleType = p.tintRuleType,
			@tintPriceBookDiscountType = P.[tintDiscountType],
			@decPriceBookDiscount = P.[decDiscount],
			@intQntyItems = P.[intQntyItems],
			@decMaxDedPerAmt = P.[decMaxDedPerAmt]
		FROM   
			PriceBookRules P 
		LEFT JOIN 
			PriceBookRuleDTL PDTL 
		ON 
			P.numPricRuleID = PDTL.numRuleID
		LEFT JOIN 
			PriceBookRuleItems PBI 
		ON 
			P.numPricRuleID = PBI.numRuleID
		LEFT JOIN 
			[PriceBookPriorities] PP 
		ON 
			PP.[Step2Value] = P.[tintStep2] 
			AND PP.[Step3Value] = P.[tintStep3]
		WHERE  
			P.numDomainID=@numDomainID 
			AND tintRuleFor=@tintOppType
			AND (
					((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 1 AND PDTL.numValue = @numDivisionID)) -- Priority 1
					OR ((tintStep2 = 2 AND PBI.numValue = @numItemClassification) AND (tintStep3= 1 AND PDTL.numValue = @numDivisionID)) -- Priority 2
					OR ((tintStep2 = 3) AND (tintStep3 = 1 AND PDTL.numValue = @numDivisionID)) -- Priority 3

					OR ((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 4
					OR ((tintStep2 = 2 AND PBI.numValue = @numItemClassification) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 5
					OR ((tintStep2 = 3) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 6

					OR ((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 3)) -- Priority 7
					OR ((tintStep2 = 2 AND PBI.numValue = @numItemClassification) AND (tintStep3 = 3)) -- Priority 8
					OR ((tintStep2 = 3) and (tintStep3 = 3)) -- Priority 9
				)
		ORDER BY 
			PP.Priority ASC


		IF ISNULL(@numPriceRuleID,0) > 0
		BEGIN
			IF @tintPricingMethod = 1 -- BASED ON PRICE TABLE
			BEGIN
				SELECT
					@tintDisountType = tintDiscountType,
					@decDiscount = ISNULL(decDiscount,0),
					@tintRuleType = tintRuleType,
					@monPriceRulePrice = (CASE tintRuleType WHEN 1 THEN @monListPrice WHEN 2 THEN @monVendorCost END) ,
					@monPriceRuleFinalPrice = (CASE tintRuleType
											WHEN 1 -- Deduct From List Price
											THEN
												CASE tintDiscountType 
													WHEN 1 -- PERCENT
													THEN (CASE WHEN @monListPrice > 0 THEN @monListPrice - (@monListPrice * (ISNULL(decDiscount,0)/100)) ELSE 0 END)
													WHEN 2 -- FLAT AMOUNT
													THEN (CASE WHEN @monListPrice > 0 THEN (CASE WHEN (@monListPrice * @numQty * @fltUOMConversionFactor) - ISNULL(decDiscount,0) > 0 THEN ((@monListPrice * @numQty * @fltUOMConversionFactor) - ISNULL(decDiscount,0)) / (@numQty * @fltUOMConversionFactor) ELSE 0 END) ELSE 0 END)
													WHEN 3 -- NAMED PRICE
													THEN ISNULL(decDiscount,0)
												END
											WHEN 2 -- Add to primary vendor cost
											THEN
												CASE tintDiscountType 
													WHEN 1  -- PERCENT
													THEN @monVendorCost + (@monVendorCost * (ISNULL(decDiscount,0)/100))
													WHEN 2 -- FLAT AMOUNT
													THEN (CASE WHEN (@monVendorCost * @numQty * @fltUOMConversionFactor) + ISNULL(decDiscount,0) > 0 THEN ((@monVendorCost * @numQty * @fltUOMConversionFactor) + ISNULL(decDiscount,0)) / (@numQty * @fltUOMConversionFactor)  ELSE 0 END)
													WHEN 3 -- NAMED PRICE
													THEN ISNULL(decDiscount,0)
												END
											WHEN 3 -- Named price
											THEN
												CASE 
												WHEN ISNULL(@tintPriceLevel,0) > 0
												THEN
													(SELECT 
															ISNULL(decDiscount,0)
													FROM
													(
														SELECT 
															ROW_NUMBER() OVER(PARTITION BY PricingTable.numItemCode ORDER BY numPricingID) Id,
															decDiscount
														FROM 
															PricingTable 
														WHERE 
															PricingTable.numItemCode = @numItemCode 
															AND tintRuleType = 3 
													) TEMP
													WHERE
														Id = @tintPriceLevel)
												ELSE
													ISNULL(decDiscount,0)
												END
											END
										) 
				FROM
					PricingTable
				WHERE
					numPriceRuleID=@numPriceRuleID AND @numQty * @fltUOMConversionFactor >= intFromQty AND @numQty * @fltUOMConversionFactor <= intToQty
			END
			ELSE IF @tintPricingMethod = 2 -- BASED ON RULE
			BEGIN
				SET @tintRuleType = @tintPriceRuleType
				SET @monPriceRulePrice = (CASE @tintPriceRuleType WHEN 1 THEN @monListPrice WHEN 2 THEN @monVendorCost END) 

				IF (@tintPriceBookDiscountType = 1 ) -- Percentage 
				BEGIN
					SET @tintDisountType = @tintPriceBookDiscountType
					SET @decDiscount = @decDiscount * (@numQty/@intQntyItems);
					SET @decDiscount = @decDiscount * (CASE WHEN @tintPriceRuleType = 2 THEN @monVendorCost ELSE @monListPrice END) /100;
					SET @decMaxDedPerAmt = @decMaxDedPerAmt * (CASE WHEN @tintPriceRuleType = 2 THEN @monVendorCost ELSE @monListPrice END) /100;
					
					IF (@decDiscount > @decMaxDedPerAmt)
						SET @decDiscount = @decMaxDedPerAmt;
				
					IF ( @tintPriceRuleType = 1 )
							SET @monPriceRuleFinalPrice = (@monListPrice - @decDiscount);
					IF ( @tintPriceRuleType = 2 )
							SET @monPriceRuleFinalPrice = (@monVendorCost + @decDiscount);
				END
				IF ( @tintPriceBookDiscountType = 2 ) -- Flat discount 
				BEGIN
					SET @decDiscount = @decDiscount * ((@numQty * @fltUOMConversionFactor)/@intQntyItems);

					IF (@decDiscount > @decMaxDedPerAmt)
						SET @decDiscount = @decMaxDedPerAmt;
					
					IF ( @tintPriceRuleType = 1 )
						SET @monPriceRuleFinalPrice =  ((@monListPrice * @numQty * @fltUOMConversionFactor) - @decDiscount)/(@numQty * @fltUOMConversionFactor);
					IF ( @tintPriceRuleType = 2 )
						SET @monPriceRuleFinalPrice = ((@monVendorCost * @numQty * @fltUOMConversionFactor) + @decDiscount)/(@numQty * @fltUOMConversionFactor);
				END
			END
		END

		If @monPriceRuleFinalPrice > 0
		BEGIN
			SET @monPrice = @monPriceRuleFinalPrice
		END
		ELSE
		BEGIN
			SET @monPrice = @monVendorCost
		END


		SET @monPriceLevelPrice = 0
		SET @monPriceFinalPrice = 0
		SET @monPriceRulePrice = 0
		SET @monPriceRuleFinalPrice = 0
		SET @monLastPrice = 0
		SET @dtLastOrderDate = NULL
		SET @tintDisountType = 1
		SET @decDiscount =0 
	END


	

	SELECT
		@vcItemName AS vcItemName,
		@bitDropship AS bitDropship,
		@chrItemType AS ItemType,
		@bitMatrix AS bitMatrix,
		@numItemClassification AS ItemClassification,
		@numItemGroup AS ItemGroup,
		@ItemDesc AS ItemDescription,
		@bitAssembly AS bitAssembly,
		case when ISNULL(@bitAssembly,0)=1 then dbo.fn_GetAssemblyPossibleWOQty(@numItemCode) else 0 end AS MaxWorkOrderQty,
		@tintPricingBasedOn AS tintPriceBaseOn,
		@monPrice AS monPrice,
		@fltUOMConversionFactor AS UOMConversionFactor,
		(CASE WHEN @tintOppType=1 THEN @monListPrice ELSE @monVendorCost END) AS monListPrice,
		@monVendorCost AS VendorCost,
		@numVendorID AS numVendorID,
		@tintRuleType AS tintRuleType,
		@monPriceLevelPrice AS monPriceLevel,
		@monPriceFinalPrice AS monPriceLevelFinalPrice,
		@monPriceRulePrice AS monPriceRule,
		@monPriceRuleFinalPrice AS monPriceRuleFinalPrice,
		@monLastPrice AS monLastPrice,
		@dtLastOrderDate AS LastOrderDate,
		@tintDisountType AS DiscountType,
		@decDiscount AS Discount,
		@numBaseUnit AS numBaseUnit,
		@numSaleUnit AS numSaleUnit,
		@numPurchaseUnit AS numPurchaseUnit,
		dbo.fn_GetUOMName(@numBaseUnit) AS vcBaseUOMName,
		dbo.fn_GetUOMName(@numSaleUnit) AS vcSaleUOMName,
		@numPurchaseUnit AS numPurchaseUnit,
		@numWarehouseItemID AS numWarehouseItemID,
		dbo.fn_GetItemTransitCount(@numItemcode,@numDomainID) AS OrdersInTransist,
		(SELECT COUNT(*) FROM SimilarItems SI INNER JOIN Item I ON I.numItemCode = SI.numItemCode WHERE SI.numDomainId = @numDomainID AND SI.numParentItemCode = @numItemCode) AS RelatedItemsCount


	-- CHECK IF PROMOTION CAN BE TRIGGERED FROM THIS ITEM
	DECLARE @numPromotionID AS NUMERIC(18,0)

	SELECT TOP 1
		@numPromotionID = numProId 
	FROM 
		PromotionOffer PO
	WHERE 
		numDomainId=@numDomainID 
		AND ISNULL(bitEnabled,0)=1
		AND ISNULL(bitApplyToInternalOrders,0)=1 
		AND 1 = (CASE WHEN bitRequireCouponCode = 1 THEN CASE WHEN ISNULL(intCouponCodeUsed,0) < ISNULL(tintUsageLimit,0) OR ISNULL(tintUsageLimit,0) = 0  THEN 1 ELSE 0 END ELSE 1 END)
		AND 1 = (CASE WHEN bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN CAST(GETDATE() AS DATE)>=dtValidFrom AND CAST(GETDATE() AS DATE)<=dtValidTo THEN 1 ELSE 0 END) END)
		AND 1 = (CASE 
					WHEN tintOfferBasedOn = 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=1 AND numValue=@numItemCode) > 0 THEN 1 ELSE 0 END)
					WHEN tintOfferBasedOn = 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=2 AND numValue=@numItemClassification) > 0 THEN 1 ELSE 0 END)
					ELSE 0
				END)
	ORDER BY
		CASE 
			WHEN ISNULL(PO.bitRequireCouponCode,0)=1 AND PO.txtCouponCode=@vcCoupon THEN 1
			WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 1 THEN 2
			WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 1 THEN 3
			WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 2 THEN 4
			WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 2 THEN 5
			WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 3 THEN 6
			WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 3 THEN 7
		END


	IF ISNULL(@numPromotionID,0) > 0
	BEGIN
		-- Get Top 1 Promotion Based on priority

		SELECT 
			numProId
			,vcProName
			,bitNeverExpires
			,bitRequireCouponCode
			,txtCouponCode
			,tintUsageLimit
			,intCouponCodeUsed
			,bitFreeShiping
			,monFreeShippingOrderAmount
			,numFreeShippingCountry
			,bitFixShipping1
			,monFixShipping1OrderAmount
			,monFixShipping1Charge
			,bitFixShipping2
			,monFixShipping2OrderAmount
			,monFixShipping2Charge
			,tintOfferTriggerValueType
			,fltOfferTriggerValue
			,tintOfferBasedOn
			,fltDiscountValue
			,tintDiscountType
			,tintDiscoutBaseOn
			,dbo.FormatedDateFromDate(dtValidTo,@numDomainID) AS dtExpire
			,(CASE tintOfferBasedOn
				WHEN 1 THEN (SELECT Stuff((SELECT CONCAT(',',numValue) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=1 FOR XML PATH('')), 1, 1, ''))
				WHEN 2 THEN (SELECT Stuff((SELECT CONCAT(',',numValue) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=2 FOR XML PATH('')), 1, 1, ''))
			END) As vcPromotionItems
			,(CASE tintDiscoutBaseOn
				WHEN 1 THEN (SELECT Stuff((SELECT CONCAT(',',numValue) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=6 AND tintType=1 FOR XML PATH('')), 1, 1, ''))
				WHEN 2 THEN (SELECT Stuff((SELECT CONCAT(',',numValue) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=6 AND tintType=2 FOR XML PATH('')), 1, 1, ''))
				WHEN 3 THEN (SELECT Stuff((SELECT CONCAT(',',SimilarItems.numItemCode) FROM SimilarItems WHERE 1 = (CASE 
																														WHEN tintOfferBasedOn=1 THEN CASE WHEN numParentItemCode IN (SELECT numValue FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=1) THEN 1 ELSE 0 END
																														WHEN tintOfferBasedOn=2 THEN CASE WHEN numParentItemCode IN (SELECT numItemCode FROM Item WHERE numDomainID=@numDomainID AND numItemClassification IN (SELECT numValue FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=2)) THEN 1 ELSE 0 END
																														ELSE 0
																													END) FOR XML PATH('')), 1, 1, ''))
			END) As vcItems
			,CONCAT('Buy '
					,CASE WHEN tintOfferTriggerValueType=1 THEN CAST(fltOfferTriggerValue AS VARCHAR) ELSE CONCAT('$',fltOfferTriggerValue) END
					,CASE tintOfferBasedOn
							WHEN 1 THEN CONCAT(' of "',(SELECT Stuff((SELECT CONCAT(', ',Item.vcItemName) FROM PromotionOfferItems INNER JOIN Item ON PromotionOfferItems.numValue = Item.numItemCode WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=1 FOR XML PATH('')), 1, 2, '')), '"')
							WHEN 2 THEN CONCAT(' of items belonging to classification(s) "',(SELECT Stuff((SELECT CONCAT(', ',ListDetails.vcData) FROM PromotionOfferItems INNER JOIN ListDetails ON PromotionOfferItems.numValue = ListDetails.numListItemID WHERE numListID=36 AND numProId=PO.numProId AND tintRecordType=5 AND tintType=2 FOR XML PATH('')), 1, 2, '')), '"')
						END
					,' & get '
					, CASE tintDiscountType WHEN 1 THEN CONCAT(fltDiscountValue,'% off on') WHEN 2 THEN CONCAT('$',fltDiscountValue, ' off on') WHEN 3 THEN CONCAT(fltDiscountValue,' of') END
					, CASE 
						WHEN tintDiscoutBaseOn = 1 THEN 
							CONCAT
							(
								CONCAT(' "',(SELECT Stuff((SELECT CONCAT(',',Item.vcItemName) FROM PromotionOfferItems INNER JOIN Item ON PromotionOfferItems.numValue = Item.numItemCode WHERE numProId=PO.numProId AND tintRecordType=6 AND tintType=1 FOR XML PATH('')), 1, 1, '')), '"')
								,(CASE WHEN PO.tintDiscountType = 3 THEN ' free' ELSE '' END)
								,'.'
							)
						WHEN tintDiscoutBaseOn = 2 THEN 
							CONCAT
							(
								CONCAT(' items belonging to classification(s) "',(SELECT Stuff((SELECT CONCAT(',',ListDetails.vcData) FROM PromotionOfferItems INNER JOIN ListDetails ON PromotionOfferItems.numValue = ListDetails.numListItemID WHERE numListID=36 AND numProId=PO.numProId AND tintRecordType=6 AND tintType=2 FOR XML PATH('')), 1, 1, '')), '"')
								,(CASE WHEN PO.tintDiscountType = 3 THEN ' free' ELSE '' END)
								,'.'
							)
						WHEN tintDiscoutBaseOn = 3 THEN ' related items.'
						ELSE '' 
					END
				) AS vcPromotionDescription
				,CONCAT
				(
					(CASE WHEN ISNULL(bitFixShipping1,0)=1 THEN CONCAT('Spend $',monFixShipping1OrderAmount,' and get $',monFixShipping1Charge,' shipping. ') ELSE '' END)
					,(CASE WHEN ISNULL(bitFixShipping2,0)=1  THEN CONCAT('Spend $',monFixShipping2OrderAmount,' and get $',monFixShipping2Charge,' shipping. ') ELSE '' END)
					,(CASE WHEN (bitFreeShiping=1 AND numFreeShippingCountry=@numShippingCountry) THEN CONCAT('Spend $',monFreeShippingOrderAmount,' and shipping is FREE! ') ELSE '' END) 
				) AS vcShippingDescription,
				CASE 
					WHEN ISNULL(PO.bitRequireCouponCode,0)=1 AND PO.txtCouponCode=@vcCoupon THEN 1
					WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 1 THEN 2
					WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 1 THEN 3
					WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 2 THEN 4
					WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 2 THEN 5
					WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 3 THEN 6
					WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 3 THEN 7
				END AS tintPriority
		FROM
			PromotionOffer PO
		WHERE
			numDomainId=@numDomainID 
			AND numProId = @numPromotionID
	END
END 
GO

/****** Object:  StoredProcedure [dbo].[usp_VerifyPartnerCode]    Script Date: 07/26/2008 16:15:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetPartnerSource')
DROP PROCEDURE USP_GetPartnerSource
GO
CREATE PROCEDURE [dbo].[USP_GetPartnerSource]                                                                                    
 @numDomainID  numeric=0                                           
AS                                                                       
BEGIN   
	SELECT 
		D.numDivisionID
		,D.vcPartnerCode + '-' + C.vcCompanyName AS vcPartner
	FROM 
		DivisionMaster AS D 
	LEFT JOIN 
		CompanyInfo AS C
	ON 
		D.numCompanyID=C.numCompanyID
	WHERE 
		D.numDomainID=@numDomainID 
		AND D.vcPartnerCode <> ''
	ORDER BY
		vcCompanyName
END
