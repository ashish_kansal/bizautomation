/******************************************************************
Project: Release 5.0 Date: 23.Nov.2015
Comments: ALTER SCRIPTS
*******************************************************************/

ALTER TABLE DivisionMaster ADD
numAccountClassID NUMERIC(18,0)

-------------------------------------------------------------------------

ALTER TABLE Domain 
DROP COLUMN IsEnableClassTracking

-------------------------------------------------------------------------

ALTER TABLE Domain
DROP COLUMN IsEnableUserLevelClassTracking

====================================================================

GO

/****** Object:  Table [dbo].[ItemUOMConversion]    Script Date: 23-Nov-15 3:00:10 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ItemUOMConversion](
	[numItemUOMConvID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numDomainID] [numeric](18, 0) NOT NULL,
	[numItemCode] [numeric](18, 0) NOT NULL,
	[numSourceUOM] [numeric](18, 0) NOT NULL,
	[numTargetUOM] [numeric](18, 0) NOT NULL,
	[numTargetUnit] [float] NOT NULL,
	[numCreatedBy] [numeric](18, 0) NOT NULL,
	[dtCreated] [datetime] NOT NULL,
	[numModifiedBy] [numeric](18, 0) NULL,
	[dtModified] [datetime] NULL,
 CONSTRAINT [PK_ItemUOMConversion] PRIMARY KEY CLUSTERED 
(
	[numItemUOMConvID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[ItemUOMConversion]  WITH CHECK ADD  CONSTRAINT [FK_ItemUOMConversion_Domain] FOREIGN KEY([numDomainID])
REFERENCES [dbo].[Domain] ([numDomainId])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[ItemUOMConversion] CHECK CONSTRAINT [FK_ItemUOMConversion_Domain]
GO

ALTER TABLE [dbo].[ItemUOMConversion]  WITH CHECK ADD  CONSTRAINT [FK_ItemUOMConversion_Item] FOREIGN KEY([numItemCode])
REFERENCES [dbo].[Item] ([numItemCode])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[ItemUOMConversion] CHECK CONSTRAINT [FK_ItemUOMConversion_Item]
GO

ALTER TABLE [dbo].[ItemUOMConversion]  WITH CHECK ADD  CONSTRAINT [FK_ItemUOMConversion_UOM] FOREIGN KEY([numSourceUOM])
REFERENCES [dbo].[UOM] ([numUOMId])
GO

ALTER TABLE [dbo].[ItemUOMConversion] CHECK CONSTRAINT [FK_ItemUOMConversion_UOM]
GO

ALTER TABLE [dbo].[ItemUOMConversion]  WITH CHECK ADD  CONSTRAINT [FK_ItemUOMConversion_UOM1] FOREIGN KEY([numTargetUOM])
REFERENCES [dbo].[UOM] ([numUOMId])
GO

ALTER TABLE [dbo].[ItemUOMConversion] CHECK CONSTRAINT [FK_ItemUOMConversion_UOM1]
GO

=====================================================================================

ALTER TABLE Domain ADD
bitEnableItemLevelUOM BIT

=====================================================

ALTER TABLE OpportunityItems ALTER
COLUMN monPrice DECIMAL(30,16)

ALTER TABLE OpportunityItems ALTER
COLUMN fltDiscount DECIMAL(30,16)


ALTER TABLE OpportunityBizDocItems ALTER
COLUMN monPrice DECIMAL(30,16)

ALTER TABLE OpportunityBizDocItems ALTER
COLUMN fltDiscount DECIMAL(30,16)
=====================================================


DECLARE @numFieldID AS NUMERIC(18,0)

INSERT INTO DycFieldMaster 
(
	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcLookBackTableName,vcFieldDataType,
	vcFieldType,vcAssociatedControlType,vcListItemType,numListID,[order],bitInResults,bitDeleted,bitAllowEdit,
	bitDefault,bitSettingField,bitAllowFiltering
)
SELECT
	numModuleID,vcFieldName,'monPriceUOM','monPriceUOM',vcLookBackTableName,vcFieldDataType,
	vcFieldType,vcAssociatedControlType,vcListItemType,numListID,[order],bitInResults,bitDeleted,bitAllowEdit,
	bitDefault,bitSettingField,bitAllowFiltering
FROM
	DycFieldMaster
WHERE
	numFieldId = 259

SELECT @numFieldID = SCOPE_IDENTITY()

INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,[order],bitInResults,
	bitdeleted,bitDefault,bitSettingField,bitAllowFiltering
)
SELECT
	3,@numFieldID,26,1,1,'Unit Price','TextBox',8,0,0,1,1,1

	
UPDATE DycFormConfigurationDetails SET numFieldId=@numFieldID WHERE numFormId = 26 AND numFieldId=259 

DELETE FROM DycFormField_Mapping WHERE numFormID=26 AND numFieldID=259

=====================================================================================

ALTER TABLE TaxDetails ADD
tintTaxType TINYINT NULL

UPDATE TaxDetails SET tintTaxType = 1


ALTER TABLE OpportunityMasterTaxItems ADD 
tintTaxType TINYINT NULL

UPDATE OpportunityMasterTaxItems SET tintTaxType = 1


ALTER TABLE OpportunityBizDocTaxItems ADD 
tintTaxType TINYINT NULL

UPDATE OpportunityBizDocTaxItems SET tintTaxType = 1

========================================================================================


IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_calitemtaxamt')
DROP FUNCTION fn_calitemtaxamt

==========================================================================================
