/******************************************************************
Project: Release 8.9 Date: 27.JANUARY.2018
Comments: STORE PROCEDURES
*******************************************************************/

GO
Alter TRIGGER [dbo].[AfterInsertUpdateOpportunityMaster]
ON [dbo].[OpportunityMaster]
AFTER INSERT,UPDATE
AS
BEGIN
	DECLARE @action as char(1) = 'I';

	IF EXISTS(SELECT * FROM DELETED)
    BEGIN
        SET @action = CASE WHEN EXISTS(SELECT * FROM INSERTED) THEN 'U' ELSE 'D' END
    END
    ELSE IF NOT EXISTS(SELECT * FROM INSERTED) 
	BEGIN
		SET @action = ''
    END


	DECLARE @numOppId NUMERIC(9)
	DECLARE @numDomainID NUMERIC(18,0)
	DECLARE @numCreatedBy NUMERIC(18,0)
	DECLARE @numModifiedBy NUMERIC(18,0)
	DECLARE @numDivisionID NUMERIC(18,0)
	DECLARE @numStatus NUMERIC(18,0)
  
	SELECT
		@numDomainID = numDomainID
		,@numDivisionID = numDivisionId
		,@numOppId = numOppId 
		,@numCreatedBy = numCreatedBy
		,@numModifiedBy = numModifiedBy
		,@numStatus = numStatus
	FROM 
		inserted

	IF @action = 'I'
	BEGIN
		INSERT INTO ElasticSearchModifiedRecords (vcModule,numRecordID, vcAction, numDomainID) SELECT 'Opp/Order',numOppId,'Insert',numDomainID FROM INSERTED
		EXEC USP_EDIQueue_Insert @numDomainID,@numCreatedBy,@numOppId,@numStatus
	END
	ELSE IF @action = 'U'
	BEGIN
		INSERT INTO ElasticSearchModifiedRecords (vcModule,numRecordID, vcAction, numDomainID) SELECT 'Opp/Order',numOppId,'Update',numDomainID FROM INSERTED

		IF UPDATE(numAssignedTo) OR UPDATE(numRecOwner) OR UPDATE(tintOppType) OR UPDATE(tintOppStatus)
		BEGIN
			INSERT INTO ElasticSearchModifiedRecords (vcModule,numRecordID, vcAction, numDomainID) SELECT 'BizDoc',numOppBizDocsId,'Update',INSERTED.numDomainID FROM INSERTED INNER JOIN OpportunityBizDocs ON INSERTED.numOppId = OpportunityBizDocs.numOppId
		END

		IF UPDATE(numStatus)
		BEGIN
			EXEC USP_EDIQueue_Insert @numDomainID,@numModifiedBy,@numOppId,@numStatus
		END
	END
  
	UPDATE 
		[OpportunityMaster]
	SET 
		monDealAmount = dbo.[GetDealAmount](@numOppId,GETUTCDATE(),0)
	WHERE 
		[numOppId] = @numOppId

	IF (SELECT COUNT(*) FROM OrganizationRatingRule WHERE numDomainID = @numDomainID) > 0
	BEGIN
		DECLARE @tintPerformance AS NUMERIC(18,0)
		SET @tintPerformance = (SELECT DISTINCT tintPerformance FROM OrganizationRatingRule WHERE numDomainID = @numDomainID)

		EXEC USP_CompanyInfo_UpdateRating @numDomainID,@numDivisionID,@tintPerformance
	END
END  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='CheckIfPriceRuleApplicableToItem')
DROP FUNCTION CheckIfPriceRuleApplicableToItem
GO
CREATE FUNCTION [dbo].[CheckIfPriceRuleApplicableToItem](@numRuleID NUMERIC(18,0), @numItemID NUMERIC(18,0), @numDivisionID NUMERIC(18,0))  
RETURNS BIT  
AS  
BEGIN  

DECLARE @isRuleApplicable BIT = 0;

DECLARE @isStep2RuleApplicable BIT = 0;
DECLARE @isStep3RuleApplicable BIT = 0;

DECLARE @tempStep2 INT;
DECLARE @tempStep3 INT;

SELECT @tempStep2=tintStep2, @tempStep3=tintStep3  FROM PriceBookRules WHERE numPricRuleID = @numRuleID

/* Check if rule is applied to given item */
IF @tempStep2 = 1 --Apply to items individually
BEGIN
	IF (SELECT COUNT(*) FROM PriceBookRuleItems WHERE numRuleID = @numRuleID AND tintType=1 AND numValue = @numItemID) > 0
		SET @isStep2RuleApplicable = 1
END
ELSE IF @tempStep2 = 2 --Apply to items with the selected item classifications
BEGIN
	IF (SELECT COUNT(*) FROM PriceBookRuleItems WHERE numRuleID = @numRuleID AND tintType=2 AND numValue = (SELECT numItemClassification FROM Item WHERE numItemCode = @numItemID)) > 0
		SET @isStep2RuleApplicable = 1
END
ELSE IF @tempStep2 = 3 -- All Items
BEGIN
	SET @isStep2RuleApplicable = 1
END

/* If step 2 rule is not applicable then there is no need to go for check in step 3 */
IF @isStep2RuleApplicable = 1
BEGIN
	IF @tempStep3 = 1 --Apply to customers individually
	BEGIN
		IF (SELECT COUNT(*) FROM PriceBookRuleDTL WHERE numRuleID = @numRuleID AND numValue = @numDivisionID) > 0
			SET @isStep3RuleApplicable = 1
	END
	ELSE IF @tempStep3 = 2 --Apply to customers with the following Relationships & Profiles
	BEGIN
		IF (
			SELECT 
				COUNT(*) 
			FROM 
				PriceBookRuleDTL 
			WHERE 
				numRuleID = @numRuleID AND 
				numValue = (
								SELECT 
									ISNULL(numCompanyType,0) 
								FROM 
									CompanyInfo 
								WHERE 
									numCompanyId = (SELECT numCompanyID FROM DivisionMaster WHERE numDivisionID = @numDivisionID)
							) AND 
				numProfile = (
								SELECT 
									ISNULL(vcProfile,0) 
								FROM 
									CompanyInfo 
								WHERE 
									numCompanyId = (SELECT numCompanyID FROM DivisionMaster WHERE numDivisionID = @numDivisionID))
			) > 0
			SET @isStep3RuleApplicable = 1
	END
	ELSE IF @tempStep3 = 3 --All Customers
	BEGIN
		SET @isStep3RuleApplicable = 1
	END
END

IF @isStep2RuleApplicable = 1 AND @isStep3RuleApplicable = 1
BEGIN
	SET @isRuleApplicable = 1
END  

return @isRuleApplicable
END
GO
GO
IF EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[CompanyInfo_IUD]'))
	DROP TRIGGER [dbo].[CompanyInfo_IUD]
GO

CREATE TRIGGER dbo.CompanyInfo_IUD
ON dbo.CompanyInfo
AFTER INSERT, UPDATE, DELETE
AS 
BEGIN
    SET NOCOUNT ON;

    DECLARE @action as char(1) = 'I';

	IF EXISTS(SELECT * FROM DELETED)
    BEGIN
        SET @action = CASE WHEN EXISTS(SELECT * FROM INSERTED) THEN 'U' ELSE 'D' END
    END
    ELSE IF NOT EXISTS(SELECT * FROM INSERTED) 
	BEGIN
		RETURN; -- Nothing updated or inserted.
    END

	IF @action = 'I'
	BEGIN
		INSERT INTO ElasticSearchModifiedRecords (vcModule,numRecordID, vcAction, numDomainID) SELECT 'Organization',INSERTED.numCompanyID,'Insert',INSERTED.numDomainID FROM INSERTED 
	END
	ELSE IF @action = 'U'
	BEGIN
		INSERT INTO ElasticSearchModifiedRecords (vcModule,numRecordID, vcAction, numDomainID) SELECT 'Organization',INSERTED.numCompanyID,'Update',INSERTED.numDomainID FROM INSERTED

		IF UPDATE(vcCompanyName)
		BEGIN
			INSERT INTO ElasticSearchModifiedRecords (vcModule,numRecordID, vcAction, numDomainID) SELECT 'Contact',numContactId,'Update',INSERTED.numDomainID FROM INSERTED INNER JOIN DivisionMaster ON INSERTED.numCompanyId = DivisionMaster.numCompanyID INNER JOIN AdditionalContactsInformation ON DivisionMaster.numDivisionID=AdditionalContactsInformation.numDivisionId
			INSERT INTO ElasticSearchModifiedRecords (vcModule,numRecordID, vcAction, numDomainID) SELECT 'Opp/Order',numOppId,'Update',INSERTED.numDomainID FROM INSERTED INNER JOIN DivisionMaster ON INSERTED.numCompanyId = DivisionMaster.numCompanyID INNER JOIN OpportunityMaster ON DivisionMaster.numDivisionID=OpportunityMaster.numDivisionId
			INSERT INTO ElasticSearchModifiedRecords (vcModule,numRecordID, vcAction, numDomainID) SELECT 'ActionItem',numCommId,'Update',INSERTED.numDomainID FROM INSERTED INNER JOIN DivisionMaster ON INSERTED.numCompanyId = DivisionMaster.numCompanyID INNER JOIN Communication ON DivisionMaster.numDivisionID=Communication.numDivisionId
			INSERT INTO ElasticSearchModifiedRecords (vcModule,numRecordID, vcAction, numDomainID) SELECT 'BizDoc',numOppBizDocsId,'Update',INSERTED.numDomainID FROM INSERTED INNER JOIN DivisionMaster ON INSERTED.numCompanyId = DivisionMaster.numCompanyID INNER JOIN OpportunityMaster ON DivisionMaster.numDivisionID=OpportunityMaster.numDivisionId INNER JOIN OpportunityBizDocs ON OpportunityMaster.numOppId = OpportunityBizDocs.numOppId
			INSERT INTO ElasticSearchModifiedRecords (vcModule,numRecordID, vcAction, numDomainID) SELECT 'Case',numCaseId,'Update',INSERTED.numDomainID FROM INSERTED INNER JOIN DivisionMaster ON INSERTED.numCompanyId = DivisionMaster.numCompanyID INNER JOIN Cases ON DivisionMaster.numDivisionID=Cases.numDivisionId
			INSERT INTO ElasticSearchModifiedRecords (vcModule,numRecordID, vcAction, numDomainID) SELECT 'Project',numProId,'Update',INSERTED.numDomainID FROM INSERTED INNER JOIN DivisionMaster ON INSERTED.numCompanyId = DivisionMaster.numCompanyID INNER JOIN ProjectsMaster ON DivisionMaster.numDivisionID=ProjectsMaster.numDivisionId
			INSERT INTO ElasticSearchModifiedRecords (vcModule,numRecordID, vcAction, numDomainID) SELECT 'Return',numReturnHeaderID,'Update',INSERTED.numDomainID FROM INSERTED INNER JOIN DivisionMaster ON INSERTED.numCompanyId = DivisionMaster.numCompanyID INNER JOIN ReturnHeader ON DivisionMaster.numDivisionID=ReturnHeader.numDivisionId
		END
	END
	ELSE IF @action = 'D'
	BEGIN
		INSERT INTO ElasticSearchModifiedRecords (vcModule,numRecordID, vcAction, numDomainID) SELECT 'Organization',numDivisionID,'Delete',DELETED.numDomainID FROM DELETED INNER JOIN DivisionMaster ON DELETED.numCompanyId = DivisionMaster.numCompanyID
	END
END
GO
IF EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[DivisionMaster_IUD]'))
	DROP TRIGGER [dbo].[DivisionMaster_IUD]
GO

CREATE TRIGGER dbo.DivisionMaster_IUD
ON dbo.DivisionMaster
AFTER INSERT, UPDATE, DELETE
AS 
BEGIN
    SET NOCOUNT ON;

    DECLARE @action as char(1) = 'I';

	IF EXISTS(SELECT * FROM DELETED)
    BEGIN
        SET @action = CASE WHEN EXISTS(SELECT * FROM INSERTED) THEN 'U' ELSE 'D' END
    END
    ELSE IF NOT EXISTS(SELECT * FROM INSERTED) 
	BEGIN
		RETURN; -- Nothing updated or inserted.
    END

	IF @action = 'I'
	BEGIN
		INSERT INTO ElasticSearchModifiedRecords (vcModule,numRecordID, vcAction, numDomainID) SELECT 'Organization',numCompanyID,'Insert',INSERTED.numDomainID FROM INSERTED
	END
	ELSE IF @action = 'U'
	BEGIN
		INSERT INTO ElasticSearchModifiedRecords (vcModule,numRecordID, vcAction, numDomainID) SELECT 'Organization',numCompanyID,'Update',INSERTED.numDomainID FROM INSERTED

		IF UPDATE (numTerID) 
		BEGIN
			INSERT INTO ElasticSearchModifiedRecords (vcModule,numRecordID, vcAction, numDomainID) SELECT 'Contact',numContactId,'Update',INSERTED.numDomainID FROM INSERTED INNER JOIN AdditionalContactsInformation ON INSERTED.numDivisionID=AdditionalContactsInformation.numDivisionId
			INSERT INTO ElasticSearchModifiedRecords (vcModule,numRecordID, vcAction, numDomainID) SELECT 'Opp/Order',numOppId,'Update',INSERTED.numDomainID FROM INSERTED INNER JOIN OpportunityMaster ON INSERTED.numDivisionID=OpportunityMaster.numDivisionId
			INSERT INTO ElasticSearchModifiedRecords (vcModule,numRecordID, vcAction, numDomainID) SELECT 'ActionItem',numCommId,'Update',INSERTED.numDomainID FROM INSERTED INNER JOIN Communication ON INSERTED.numDivisionID=Communication.numDivisionId
			INSERT INTO ElasticSearchModifiedRecords (vcModule,numRecordID, vcAction, numDomainID) SELECT 'BizDoc',numOppBizDocsId,'Update',INSERTED.numDomainID FROM INSERTED INNER JOIN OpportunityMaster ON INSERTED.numDivisionID=OpportunityMaster.numDivisionId INNER JOIN OpportunityBizDocs ON OpportunityMaster.numOppId = OpportunityBizDocs.numOppId
			INSERT INTO ElasticSearchModifiedRecords (vcModule,numRecordID, vcAction, numDomainID) SELECT 'Case',numCaseId,'Update',INSERTED.numDomainID FROM INSERTED INNER JOIN Cases ON INSERTED.numDivisionID=Cases.numDivisionId
			INSERT INTO ElasticSearchModifiedRecords (vcModule,numRecordID, vcAction, numDomainID) SELECT 'Project',numProId,'Update',INSERTED.numDomainID FROM INSERTED INNER JOIN ProjectsMaster ON INSERTED.numDivisionID=ProjectsMaster.numDivisionId
		END
	END
	ELSE IF @action = 'D'
	BEGIN
		INSERT INTO ElasticSearchModifiedRecords (vcModule,numRecordID, vcAction, numDomainID) SELECT 'Organization',numDivisionID,'Delete',DELETED.numDomainID FROM DELETED
	END
END
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='GetCommissionContactItemList')
DROP FUNCTION GetCommissionContactItemList
GO
CREATE FUNCTION dbo.GetCommissionContactItemList
    (
      @numRuleID NUMERIC,
      @tintMode TINYINT 
    )
RETURNS VARCHAR(MAX)
AS BEGIN
    DECLARE @ItemList VARCHAR(MAX)

	IF @tintMode = 0
        BEGIN
			DECLARE @tintAssignTo AS TINYINT

			SELECT @tintAssignTo=tintAssignTo FROM CommissionRules WHERE numComRuleID=@numRuleID

			IF @tintAssignTo = 3
			BEGIN
				SELECT 
					@ItemList = COALESCE(@ItemList + ', ', '') + CONCAT(D.vcPartnerCode,'-',C.vcCompanyName,' (Partner)')
				FROM 
					CommissionRuleContacts CC 
				INNER JOIN
					DivisionMaster D
				ON
					CC.numValue = D.numDivisionID
				INNER JOIN
					CompanyInfo C
				ON
					D.numCompanyID =C.numCompanyId
				WHERE 
					CC.numComRuleID=@numRuleID
			END
			ELSE
			BEGIN
				SELECT 
						@ItemList = COALESCE(@ItemList + ', ', '') + CASE CC.bitCommContact WHEN 1 THEN (SELECT vcCompanyName FROM DivisionMaster D join CompanyInfo C on C.numCompanyID=D.numCompanyID  where D.numDivisionID=CC.numValue) + '(Commission Contact)' ELSE dbo.fn_GetContactName(numValue) END
				 FROM    [CommissionRuleContacts] CC
						--INNER JOIN AdditionalContactsInformation A ON  CC.numValue=A.numContactID
				WHERE   CC.[numComRuleID] = @numRuleID
			END
        END
        
    IF @tintMode = 1 
        BEGIN

            SELECT TOP 10
                    @ItemList = COALESCE(@ItemList + ', ', '') + vcItemName
            FROM    [CommissionRuleItems] CI
                    INNER JOIN Item I ON I.[numItemCode] = CI.[numValue]
            WHERE   CI.[numComRuleID] = @numRuleID
        END
      
      IF @tintMode = 2
        BEGIN

            SELECT TOP 10
                    @ItemList = COALESCE(@ItemList + ', ', '') + vcData
            FROM    [CommissionRuleItems] CI
                    INNER JOIN [ListDetails] LI ON LI.[numListItemID] = CI.[numValue]
            WHERE   CI.[numComRuleID] = @numRuleID
        END
       IF @tintMode = 3
        BEGIN

            SELECT TOP 10
                    @ItemList = COALESCE(@ItemList + ', ', '') + C.[vcCompanyName]
            FROM    [CommissionRuleOrganization] PD
                    INNER JOIN [DivisionMaster] DM ON DM.[numDivisionID] = PD.[numValue]
                    LEFT OUTER JOIN [CompanyInfo] C ON C.[numCompanyId] = DM.[numCompanyID]
                    LEFT OUTER JOIN [CommissionRules] PB ON PB.[numComRuleID] = PD.[numComRuleID]
            WHERE   PB.tintComOrgType = 1
                    AND PB.[numComRuleID] = @numRuleID
        END
        
        IF @tintMode = 4
        BEGIN

             SELECT TOP 10
                    @ItemList = COALESCE(@ItemList + ', ', '') + (LI.vcData +'/' + LI1.vcData)
            FROM    [CommissionRuleOrganization] PD
                    INNER JOIN [ListDetails] LI ON LI.[numListItemID] = PD.[numValue]
                    Inner JOIN [ListDetails] LI1 ON LI1.[numListItemID] = PD.[numProfile]
                    LEFT OUTER JOIN [CommissionRules] PB ON PB.[numComRuleID] = PD.[numComRuleID]
            WHERE   PB.tintComOrgType = 2
                    AND PB.[numComRuleID] = @numRuleID
        END
        
    RETURN ISNULL(@ItemList, '')
   END 
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='GetUnitPriceAfterPriceLevelApplication')
DROP FUNCTION GetUnitPriceAfterPriceLevelApplication
GO
CREATE FUNCTION [dbo].[GetUnitPriceAfterPriceLevelApplication]
(
	@numDomainID numeric(18,0), 
	@numItemCode NUMERIC(18,0), 
	@numQuantity FLOAT, 
	@numWarehouseItemID NUMERIC(18,0), 
	@isPriceRule BIT, 
	@numPriceRuleID NUMERIC(18,0),
	@numDivisionID NUMERIC(18,0)
)  
RETURNS FLOAT  
AS  
BEGIN  

DECLARE @finalUnitPrice FLOAT = 0
DECLARE @tintRuleType INT
DECLARE @tintDiscountType INT
DECLARE @decDiscount FLOAT
DECLARE @ItemPrice FLOAT

SET @tintRuleType = 0
SET @tintDiscountType = 0
SET @decDiscount  = 0
SET @ItemPrice = 0

IF @isPriceRule = 1
	SELECT TOP 1
		@tintRuleType = tintRuleType,
		@tintDiscountType = tintDiscountType,
		@decDiscount = decDiscount
	FROM 
		PricingTable 
	WHERE 
		numPriceRuleID = @numPriceRuleID AND
		(@numQuantity BETWEEN intFromQty AND intToQty)
ELSE
	SELECT TOP 1
		@tintRuleType = tintRuleType,
		@tintDiscountType = tintDiscountType,
		@decDiscount = decDiscount
	FROM 
		PricingTable 
	WHERE 
		numItemCode = @numItemCode AND
		((@numQuantity BETWEEN intFromQty AND intToQty) OR tintRuleType = 3)

IF @tintRuleType > 0 AND @tintDiscountType > 0
BEGIN
	IF @tintRuleType = 1 -- Deduct from List price
	BEGIN
		IF (SELECT ISNULL(charItemType,'') FROM Item WHERE numDomainID = @numDomainID AND numItemCode = @numItemCode) = 'P'
			If (SELECT COUNT(*) FROM Item WHERE numDomainID = @numDomainID AND numItemCode = @numItemCode AND (bitKitParent = 1 OR bitAssembly = 1)) > 0 -- Kit OR Assembly
				SELECT @ItemPrice = dbo.GetUnitPriceForKitOrAssembly(@numDomainID,@numItemCode,@numWarehouseItemID,@numQuantity)
			ELSE
				SELECT @ItemPrice = ISNULL(monWListPrice,0) FROM WareHouseItems WHERE numDomainID = @numDomainID AND numItemID = @numItemCode AND numWareHouseItemID = @numWarehouseItemID
		ELSE
			SELECT @ItemPrice = ISNULL(monListPrice,0) FROM Item WHERE numDomainID = @numDomainID AND numItemCode = @numItemCode

		IF @tintDiscountType = 1 -- Percentage
		BEGIN
			SELECT @finalUnitPrice = @ItemPrice - (@ItemPrice * (@decDiscount/100))
		END
		ELSE IF @tintDiscountType = 2 -- Flat Amount
		BEGIN
				SELECT @finalUnitPrice = @ItemPrice - @decDiscount
		END
	END
	ELSE IF @tintRuleType = 2 -- Add to Primary Vendor Cost
	BEGIN
		If (SELECT COUNT(*) FROM Item WHERE numDomainID = @numDomainID AND numItemCode = @numItemCode AND (bitKitParent = 1 OR bitAssembly = 1)) > 0 -- Kit OR Assembly
			SELECT @ItemPrice = dbo.GetUnitPriceForKitOrAssembly(@numDomainID,@numItemCode,@numWarehouseItemID,@numQuantity)
		ELSE
			SELECT @ItemPrice = ISNULL(monCost,0) FROM Vendor WHERE numItemCode = @numItemCode AND numVendorID = (SELECT numVendorID FROM Item WHERE numItemCode = @numItemCode)

		IF @tintDiscountType = 1 -- Percentage
		BEGIN
			SELECT @finalUnitPrice = @ItemPrice + (@ItemPrice * (@decDiscount/100))
		END
		ELSE IF @tintDiscountType = 2 -- Flat Amount
		BEGIN
			SELECT @finalUnitPrice = @ItemPrice + @decDiscount
		END
	END
	ELSE IF @tintRuleType = 3 -- Named Price
	BEGIN
		IF ISNULL(@numDivisionID,0) = 0
		BEGIN
			SELECT @finalUnitPrice = @decDiscount
		END
		ELSE
		BEGIN
			DECLARE @monPriceLevelPrice AS FLOAT = 0
			DECLARE @tintPriceLevel INT = 0
			SELECT @tintPriceLevel=ISNULL(tintPriceLevel,0) FROM DivisionMaster WHERE numDivisionID=@numDivisionID

			IF ISNULL(@tintPriceLevel,0) > 0
			BEGIN
				SET @decDiscount = 0

				SELECT 
					@monPriceLevelPrice = ISNULL(decDiscount,0)
				FROM
				(
					SELECT 
						ROW_NUMBER() OVER(PARTITION BY PricingTable.numItemCode ORDER BY numPricingID) Id,
						decDiscount
					FROM 
						PricingTable 
					INNER JOIN
						Item
					ON
						Item.numItemCode = @numItemCode 
						AND Item.numDomainID = @numDomainID
					WHERE 
						tintRuleType = 3 
				) TEMP
				WHERE
					Id BETWEEN @tintPriceLevel AND @tintPriceLevel

				IF ISNULL(@monPriceLevelPrice,0) > 0
				BEGIN
					SELECT @finalUnitPrice = @monPriceLevelPrice
				END
				ELSE
				BEGIN
					SELECT @finalUnitPrice = @decDiscount
				END
			END
			ELSE
			BEGIN
				SELECT @finalUnitPrice = @decDiscount
			END
		END
	END
END

return @finalUnitPrice
END
GO
/****** Object:  StoredProcedure [dbo].[uso_cfwfldDetails]    Script Date: 07/26/2008 16:14:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='uso_cfwflddetails')
DROP PROCEDURE uso_cfwflddetails
GO
CREATE PROCEDURE [dbo].[uso_cfwfldDetails]  
@fldid as numeric(9)=null  
as  
SELECT  fld_type,
        fld_label,
        fld_tab_ord,
        grp_id,
        subgrp,
        vcURL,
		DTL.bitIsRequired,
		DTL.bitIsNumeric,
		DTL.bitIsAlphaNumeric,
		DTL.bitIsEmail,
		DTL.bitIsLengthValidation,
		DTL.intMaxLength,
		DTL.intMinLength,
		DTL.bitFieldMessage,
		DTL.vcFieldMessage
		,ISNULL(M.vcToolTip,'') AS vcToolTip
		,ISNULL(bitAutocomplete,0) bitAutocomplete
FROM    CFW_Fld_Master M
        LEFT JOIN dbo.CFW_Validation DTL ON M.Fld_id = DTL.numFieldId
WHERE   fld_id = @fldid
GO
/****** Object:  StoredProcedure [dbo].[USP_AdvancedSearch]    Script Date: 05/07/2009 22:04:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                              
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_advancedsearch')
DROP PROCEDURE usp_advancedsearch
GO
CREATE PROCEDURE [dbo].[USP_AdvancedSearch] 
@WhereCondition as varchar(4000)='',                              
@ViewID as tinyint,                              
@AreasofInt as varchar(100)='',                              
@tintUserRightType as tinyint,                              
@numDomainID as numeric(9)=0,                              
@numUserCntID as numeric(9)=0,                              
@numGroupID as numeric(9)=0,                              
@CurrentPage int,                                                                    
@PageSize int,                                                                    
@TotRecs int output,                                                                                                             
@columnSortOrder as Varchar(10),                              
@ColumnSearch as varchar(100)='',                           
@ColumnName as varchar(20)='',                             
@GetAll as bit,                              
@SortCharacter as char(1),                        
@SortColumnName as varchar(20)='',        
@bitMassUpdate as bit,        
@LookTable as varchar(200)='' ,        
@strMassUpdate as varchar(2000)='',
@vcRegularSearchCriteria varchar(MAX)='',
@vcCustomSearchCriteria varchar(MAX)='',
@ClientTimeZoneOffset as int
as                              
BEGIN
	DECLARE  @tintOrder  AS TINYINT
DECLARE  @vcFormFieldName  AS VARCHAR(50)
DECLARE  @vcListItemType  AS VARCHAR(3)
DECLARE  @vcListItemType1  AS VARCHAR(3)
DECLARE  @vcAssociatedControlType VARCHAR(20)
DECLARE  @numListID  AS NUMERIC(9)
DECLARE  @vcDbColumnName VARCHAR(20)
DECLARE  @vcLookBackTableName VARCHAR(50)
DECLARE  @WCondition VARCHAR(1000)
DECLARE @bitContactAddressJoinAdded AS BIT;
DECLARE @bitBillAddressJoinAdded AS BIT;
DECLARE @bitShipAddressJoinAdded AS BIT;
DECLARE @vcOrigDbColumnName as varchar(50)
DECLARE @bitAllowSorting AS CHAR(1)
DECLARE @bitAllowEdit AS CHAR(1)
DECLARE @bitCustomField AS BIT;
DECLARE @ListRelID AS NUMERIC(9)
DECLARE @intColumnWidth INT
DECLARE @bitAllowFiltering AS BIT;
DECLARE @vcfieldatatype CHAR(1)

Declare @zipCodeSearch as varchar(100);SET @zipCodeSearch=''


if CHARINDEX('$SZ$', @WhereCondition)>0
BEGIN
select @zipCodeSearch=SUBSTRING(@WhereCondition, CHARINDEX('$SZ$', @WhereCondition)+4,CHARINDEX('$EZ$', @WhereCondition) - CHARINDEX('$SZ$', @WhereCondition)-4) 

CREATE TABLE #tempAddress(
	[numAddressID] [numeric](18, 0) NOT NULL,
	[vcAddressName] [varchar](50) NULL,
	[vcStreet] [varchar](100) NULL,
	[vcCity] [varchar](50) NULL,
	[vcPostalCode] [varchar](15) NULL,
	[numState] [numeric](18, 0) NULL,
	[numCountry] [numeric](18, 0) NULL,
	[bitIsPrimary] [bit] NOT NULL CONSTRAINT [DF_AddressMaster_bitIsPrimary]  DEFAULT ((0)),
	[tintAddressOf] [tinyint] NOT NULL,
	[tintAddressType] [tinyint] NOT NULL CONSTRAINT [DF_AddressMaster_tintAddressType]  DEFAULT ((0)),
	[numRecordID] [numeric](18, 0) NOT NULL,
	[numDomainID] [numeric](18, 0) NOT NULL)

IF Len(@zipCodeSearch)>0
BEGIN
	Declare @Strtemp as nvarchar(500);
	SET @Strtemp='insert into #tempAddress select * from AddressDetails  where numDomainID=@numDomainID1  
					and vcPostalCode in (SELECT REPLICATE(''0'', 5 - LEN(ZipCode)) + ZipCode  FROM ' + @zipCodeSearch +')';
	
	EXEC sp_executesql @Strtemp,N'@numDomainID1 numeric(18)',@numDomainID1=@numDomainID

set @WhereCondition=replace(@WhereCondition,'$SZ$' + @zipCodeSearch + '$EZ$',
' (ADC.numContactID in (select  numRecordID from #tempAddress where tintAddressOf=1 AND tintAddressType=0)   
or (DM.numDivisionID in (select  numRecordID from #tempAddress where tintAddressOf=2 AND tintAddressType in(1,2))))')
END
END

SET @WCondition = ''
   
IF (@SortCharacter <> '0' AND @SortCharacter <> '')
  SET @ColumnSearch = @SortCharacter
CREATE TABLE #temptable (
  id           INT   IDENTITY   PRIMARY KEY,
  numcontactid VARCHAR(15))


---------------------Find Duplicate Code Start-----------------------------
DECLARE  @Sql           VARCHAR(8000),
         @numMaxFieldId NUMERIC(9),
         @Where         NVARCHAR(1000),
         @OrderBy       NVARCHAR(1000),
         @GroupBy       NVARCHAR(1000),
         @SelctFields   NVARCHAR(4000),
         @InneJoinOn    NVARCHAR(1000)
SET @InneJoinOn = ''
SET @OrderBy = ''
SET @Sql = ''
SET @Where = ''
SET @GroupBy = ''
SET @SelctFields = ''
    
IF (@WhereCondition = 'DUP' OR @WhereCondition = 'DUP-PRIMARY')
BEGIN
	SELECT @numMaxFieldId= MAX([numFieldID]) FROM View_DynamicColumns
	WHERE [numFormID] =24 AND [numDomainId] = @numDomainID
	--PRINT @numMaxFieldId
	WHILE @numMaxFieldId > 0
	BEGIN
		SELECT @vcFormFieldName=[vcFieldName],@vcDbColumnName=[vcDbColumnName],@vcLookBackTableName=[vcLookBackTableName] 
		FROM View_DynamicColumns 
		WHERE [numFieldID] = @numMaxFieldId and [numFormID] =24 AND [numDomainId] = @numDomainID
		--	PRINT @vcDbColumnName
			SET @SelctFields = @SelctFields +  @vcDbColumnName + ','
			IF(@vcLookBackTableName ='AdditionalContactsInformation')
			BEGIN
				SET @InneJoinOn = @InneJoinOn + 'ADC.'+ @vcDbColumnName +' = a1.' + @vcDbColumnName + ' and '
				SET @GroupBy = @GroupBy + 'ADC.'+  @vcDbColumnName + ','
			END
			ELSE IF (@vcLookBackTableName ='DivisionMaster')
			BEGIN
				SET @InneJoinOn = @InneJoinOn + 'DM.'+ @vcDbColumnName +' = a1.' + @vcDbColumnName + ' and '
				SET @GroupBy = @GroupBy + 'DM.'+  @vcDbColumnName + ','
			END
			ELSE
			BEGIN
				SET @InneJoinOn = @InneJoinOn + 'C.' + @vcDbColumnName +' = a1.' + @vcDbColumnName + ' and '
				SET @GroupBy = @GroupBy + 'C.' + @vcDbColumnName + ','
			END
			SET @Where = @Where + 'and LEN(' + @vcDbColumnName + ')>0 '

	
		SELECT @numMaxFieldId = MAX([numFieldID]) FROM View_DynamicColumns 
		WHERE [numFormID] =24 AND [numDomainId] = @numDomainID AND [numFieldID] < @numMaxFieldId
	END
			
			IF(LEN(@Where)>2)
			BEGIN
				SET @Where =RIGHT(@Where,LEN(@Where)-3)
				SET @OrderBy =  LEFT(@GroupBy,(Len(@GroupBy)- 1))
				SET @GroupBy =  LEFT(@GroupBy,(Len(@GroupBy)- 1))
				SET @InneJoinOn = LEFT(@InneJoinOn,LEN(@InneJoinOn)-3)
				SET @SelctFields = LEFT(@SelctFields,(Len(@SelctFields)- 1))
				SET @Sql = @Sql + ' INSERT INTO [#tempTable] ([numContactID])SELECT  ADC.numContactId FROM additionalcontactsinformation ADC JOIN DivisionMaster DM ON ADC.numDivisionId = DM.numDivisionID JOIN CompanyInfo C ON DM.numCompanyID = C.numCompanyId  
					JOIN (SELECT   '+ @SelctFields + '
				   FROM  additionalcontactsinformation ADC JOIN DivisionMaster DM ON ADC.numDivisionId = DM.numDivisionID JOIN CompanyInfo C ON DM.numCompanyID = C.numCompanyId  
				   WHERE  ' + @Where + ' AND C.[numDomainID] = '+convert(varchar(15),@numDomainID)+ ' AND ADC.[numDomainID] = '+convert(varchar(15),@numDomainID)+ ' AND DM.[numDomainID] = '+convert(varchar(15),@numDomainID)+ 
				   ' GROUP BY ' + @GroupBy + '
				   HAVING   COUNT(* ) > 1) a1
					ON  '+ @InneJoinOn + ' where ADC.[numDomainID] = '+convert(varchar(15),@numDomainID) + ' and DM.[numDomainID] = '+convert(varchar(15),@numDomainID) + ' ORDER BY '+ @OrderBy
			END	
			ELSE
			BEGIN
				SET @Where = ' 1=0 '
				SET @Sql = @Sql + ' INSERT INTO [#tempTable] ([numContactID])SELECT  ADC.numContactId FROM additionalcontactsinformation ADC  '
							+ ' Where ' + @Where
			END 	
		   PRINT @Sql
		   EXEC( @Sql)
		   SET @Sql = ''
		   SET @GroupBy = ''
		   SET @SelctFields = ''
		   SET @Where = ''
	
	IF (@WhereCondition = 'DUP-PRIMARY')
      BEGIN
        SET @WCondition = ' AND ADC.numContactId IN (SELECT DISTINCT numContactId FROM #tempTable) and ISNULL(ADC.bitPrimaryContact,0)=1'
      END
    ELSE
      BEGIN
        SET @WCondition = ' AND ADC.numContactId IN (SELECT DISTINCT numContactId FROM #tempTable)'
      END
    
	SET @Sql = ''
	SET @WhereCondition =''
	SET @Where= 'DUP' 
END
-----------------------------End Find Duplicate ------------------------

declare @strSql as varchar(8000)                                                              
 set  @strSql='Select ADC.numContactId                    
  FROM AdditionalContactsInformation ADC                                                   
  JOIN DivisionMaster DM ON ADC.numDivisionId = DM.numDivisionID                                                  
  JOIN CompanyInfo C ON DM.numCompanyID = C.numCompanyId'   

IF @SortColumnName='vcStreet' OR @SortColumnName='vcCity' OR @SortColumnName='vcPostalCode' OR @SortColumnName ='numCountry' OR @SortColumnName ='numState'
   OR @ColumnName='vcStreet' OR @ColumnName='vcCity' OR @ColumnName='vcPostalCode' OR @ColumnName ='numCountry' OR @ColumnName ='numState'	
BEGIN 
	set @strSql= @strSql +' left Join AddressDetails AD on AD.numRecordId= ADC.numContactID and AD.tintAddressOf=1 and AD.tintAddressType=0 AND AD.bitIsPrimary=1 and AD.numDomainID= ADC.numDomainID'	
END 
ELSE IF @SortColumnName='vcBillStreet' OR @SortColumnName='vcBillCity' OR @SortColumnName='vcBillPostCode' OR @SortColumnName ='numBillCountry' OR @SortColumnName ='numBillState'
     OR @ColumnName='vcBillStreet' OR @ColumnName='vcBillCity' OR @ColumnName='vcBillPostCode' OR @ColumnName ='numBillCountry' OR @ColumnName ='numBillState'
BEGIN 	
	set @strSql= @strSql +' left Join AddressDetails AD1 on AD1.numRecordId= DM.numDivisionID and AD1.tintAddressOf=2 and AD1.tintAddressType=1 AND AD1.bitIsPrimary=1 and AD1.numDomainID= ADC.numDomainID '
END	
ELSE IF @SortColumnName='vcShipStreet' OR @SortColumnName='vcShipCity' OR @SortColumnName='vcShipPostCode' OR @SortColumnName ='numShipCountry' OR @SortColumnName ='numShipState'
     OR @ColumnName='vcShipStreet' OR @ColumnName='vcShipCity' OR @ColumnName='vcShipPostCode' OR @ColumnName ='numShipCountry' OR @ColumnName ='numShipState'
BEGIN
	set @strSql= @strSql +' left Join AddressDetails AD2 on AD2.numRecordId= DM.numDivisionID and AD2.tintAddressOf=2 and AD2.tintAddressType=2 AND AD2.bitIsPrimary=1 and AD2.numDomainID= ADC.numDomainID '
END	
                         
  if (@ColumnName<>'' and  @ColumnSearch<>'')                        
  begin                          
 select @vcListItemType=vcListItemType,@numListID=numListID from View_DynamicDefaultColumns where vcDbColumnName=@ColumnName and numFormID=1 and numDomainId=@numDomainId                         
    if @vcListItemType='LI'                               
    begin                      
		IF @numListID=40--Country
		  BEGIN
			IF @ColumnName ='numCountry'
			BEGIN
					set @strSql= @strSql +' left Join ListDetails L1 on L1.numListItemID=AD.numCountry' 
			END
			ELSE IF @ColumnName ='numBillCountry'
			BEGIN
					set @strSql= @strSql +' left Join ListDetails L1 on L1.numListItemID=AD1.numCountry' 
			END
			ELSE IF @ColumnName ='numShipCountry'
			BEGIN
					set @strSql= @strSql +' left Join ListDetails L1 on L1.numListItemID=AD2.numCountry' 
			END
		  END                        
		  ELSE
		  BEGIN
				set @strSql= @strSql +' left Join ListDetails L1 on L1.numListItemID='+@ColumnName                              
		  END
		            
    end                              
    else if @vcListItemType='S'                               
    begin           
       IF @ColumnName ='numState'
		BEGIN
   			set @strSql= @strSql +' left join State S1 on S1.numStateID=AD.numState'                            
		END
		ELSE IF @ColumnName ='numBillState'
		BEGIN
   			set @strSql= @strSql +' left join State S1 on S1.numStateID=AD1.numState'                            
		END
	    ELSE IF @ColumnName ='numShipState'
	    BEGIN
   			set @strSql= @strSql +' left join State S1 on S1.numStateID=AD2.numState'                           
		END
    end                              
    else if @vcListItemType='T'                               
    begin                              
      set @strSql= @strSql +' left Join ListDetails L1 on L1.numListItemID='+@ColumnName                             
    end                           
   end       
           
                      
   if (@SortColumnName<>'')                        
  begin                          
 select @vcListItemType1=vcListItemType,@numListID=numListID from View_DynamicDefaultColumns where vcDbColumnName=@SortColumnName and numFormID=1 and numDomainId=@numDomainId                         
    if @vcListItemType1='LI'                   
    begin     
    IF @numListID=40--Country
		  BEGIN
			IF @SortColumnName ='numCountry'
			BEGIN
					set @strSql= @strSql +' left Join ListDetails L2 on L2.numListItemID=AD.numCountry' 
			END
			ELSE IF @SortColumnName ='numBillCountry'
			BEGIN
					set @strSql= @strSql +' left Join ListDetails L2 on L2.numListItemID=AD1.numCountry' 
			END
			ELSE IF @SortColumnName ='numShipCountry'
			BEGIN
					set @strSql= @strSql +' left Join ListDetails L2 on L2.numListItemID=AD2.numCountry' 
			END
		  END                        
		  ELSE
		  BEGIN
				set @strSql= @strSql +' left Join ListDetails L2 on L2.numListItemID='+@SortColumnName                              
		  END
    end                              
    else if @vcListItemType1='S'           
    begin                
        IF @SortColumnName ='numState'
		BEGIN
   			set @strSql= @strSql +' left join State S2 on S2.numStateID=AD.numState'                            
		END
		ELSE IF @SortColumnName ='numBillState'
		BEGIN
   			set @strSql= @strSql +' left join State S2 on S2.numStateID=AD1.numState'                            
		END
	    ELSE IF @SortColumnName ='numShipState'
	    BEGIN
   			set @strSql= @strSql +' left join State S2 on S2.numStateID=AD2.numState'                           
		END
    end                              
    else if @vcListItemType1='T'                  
    begin                              
      set @strSql= @strSql +' left Join ListDetails L2 on L2.numListItemID='+@SortColumnName                               
    end                           
  end                          
  set @strSql=@strSql+' where DM.numDomainID  = '+convert(varchar(15),@numDomainID)+   @WhereCondition                                  
                 
--if @tintUserRightType=1 set @strSql=@strSql + ' AND (ADC.numRecOwner = '+convert(varchar(15),@numUserCntID)+ ')'                                       
--else if @tintUserRightType=2 set @strSql=@strSql + ' AND (DM.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+convert(varchar(15),@numUserCntID)+' ) or DM.numTerID=0) '
                        
--IF @vcRegularSearchCriteria<>'' set @strSql=@strSql+' and ' + @vcRegularSearchCriteria 
--IF @vcCustomSearchCriteria<>'' set @strSql=@strSql+' and ' + @vcCustomSearchCriteria                          
                                            
if    @AreasofInt<>'' set  @strSql=@strSql+ ' and ADC.numContactID in (select numContactID from  AOIContactLink where numAOIID in('+ @AreasofInt+'))'                                 
if (@ColumnName<>'' and  @ColumnSearch<>'')                         
begin                          
  if @vcListItemType='LI'                               
    begin                                
      set @strSql= @strSql +' and L1.vcData like '''+@ColumnSearch +'%'''                             
    end                              
    else if @vcListItemType='S'                               
    begin                                  
      set @strSql= @strSql +' and S1.vcState like '''+@ColumnSearch+'%'''                          
    end                              
    else if @vcListItemType='T'                               
    begin                              
      set @strSql= @strSql +' and T1.vcTerName like '''+@ColumnSearch  +'%'''                            
    end   
    else  
       BEGIN     
		   IF @ColumnName='vcStreet' OR @ColumnName='vcBillStreet' OR @ColumnName='vcShipStreet'
				set @strSql= @strSql +' and vcStreet like '''+@ColumnSearch  +'%'''                            
		   ELSE IF @ColumnName='vcCity' OR @ColumnName='vcBillCity' OR @ColumnName='vcShipCity'
				set @strSql= @strSql +' and vcCity like '''+@ColumnSearch  +'%'''                            
		   ELSE IF @ColumnName='vcPostalCode' OR @ColumnName='vcBillPostCode' OR @ColumnName='vcShipPostCode'
				set @strSql= @strSql +' and vcPostalCode like '''+@ColumnSearch  +'%'''                            
		   else		                      
				set @strSql= @strSql +' and '+@ColumnName+' like '''+@ColumnSearch  +'%'''                            
        END   
                          
end                          
   if (@SortColumnName<>'')                        
  begin                           
    if @vcListItemType1='LI'                               
		begin                                
		  set @strSql= @strSql +' order by L2.vcData '+@columnSortOrder                              
		end                              
    else if @vcListItemType1='S'                               
		begin                                  
		  set @strSql= @strSql +' order by S2.vcState '+@columnSortOrder                              
		end                              
    else if @vcListItemType1='T'                               
		begin                              
		  set @strSql= @strSql +' order by T2.vcTerName '+@columnSortOrder                              
		end  
    else  
       BEGIN     
		   IF @SortColumnName='vcStreet' OR @SortColumnName='vcBillStreet' OR @SortColumnName='vcShipStreet'
				set @strSql= @strSql +' order by vcStreet ' +@columnSortOrder                            
		   ELSE IF @SortColumnName='vcCity' OR @SortColumnName='vcBillCity' OR @SortColumnName='vcShipCity'
				set @strSql= @strSql +' order by vcCity ' +@columnSortOrder                            
		   ELSE IF @SortColumnName='vcPostalCode' OR @SortColumnName='vcBillPostCode' OR @SortColumnName='vcShipPostCode'
				set @strSql= @strSql +' order by vcPostalCode ' +@columnSortOrder
		   else if @SortColumnName='numRecOwner'
				set @strSql= @strSql + ' order by dbo.fn_GetContactName(ADC.numRecOwner) ' +@columnSortOrder	                            
		   else		                      
				set @strSql= @strSql +' order by '+ @SortColumnName +' ' +@columnSortOrder                            
        END  
                                
  END
  ELSE 
	  set @strSql= @strSql +' order by  ADC.numContactID asc '
IF(@Where <>'DUP')
BEGIN
    PRINT @strSql

	INSERT INTO #temptable (numcontactid)
	EXEC( @strSql)
	
	SET @strSql = ''
END

SET @bitBillAddressJoinAdded=0;
SET @bitShipAddressJoinAdded=0;
SET @bitContactAddressJoinAdded=0;
set @tintOrder=0;
set @WhereCondition ='';
set @strSql='select ADC.numContactId,DM.numDivisionID,DM.numTerID,ADC.numRecOwner,DM.tintCRMType '                              
DECLARE @Prefix AS VARCHAR(5)
DECLARE @bitCustom BIT
DECLARE @numFieldGroupID AS INT
DECLARE @vcColumnName AS VARCHAR(200)
DECLARE @numFieldID AS NUMERIC(18,0)

	SELECT TOP 1
		@tintOrder=tintOrder,
		@numFieldID=numFormFieldID,
		@vcDbColumnName=vcDbColumnName,
		@vcFormFieldName=vcFieldName,
		@vcAssociatedControlType=vcAssociatedControlType,
		@vcListItemType=vcListItemType,
		@numListID=numListID,
		@vcLookBackTableName=vcLookBackTableName,
		@bitCustom=bitCustom,
		@vcOrigDbColumnName=vcOrigDbColumnName,
		@bitAllowSorting=bitAllowSorting,
		@bitAllowEdit= bitAllowEdit,
		@ListRelID=ListRelID,
        @intColumnWidth=intColumnWidth,
		@bitAllowFiltering=bitAllowFiltering,
		@vcfieldatatype=vcFieldDataType,
		@numFieldGroupID=numGroupID
	FROM
	(
		SELECT
			A.tintOrder+1 tintOrder,
			numFormFieldID,
			vcDbColumnName,
			vcFieldName,
			vcAssociatedControlType,
			vcListItemType,
			numListID,
			vcLookBackTableName,
			0 AS bitCustom,
			vcOrigDbColumnName,
			bitAllowSorting,
			bitAllowEdit,			
			ListRelID,			
			A.intColumnWidth as intColumnWidth,
			bitAllowFiltering,
			vcFieldDataType,			
			0 AS numGroupID
		FROM 
			AdvSerViewConf A                              
		JOIN 
			View_DynamicDefaultColumns D                               
		ON 
			D.numFieldId=A.numFormFieldId  
			AND d.numformId = 1                           
		WHERE 
			D.numDomainID = @numDomainID 
			AND D.numFormID = 1 
			AND A.numFormID = 1 
			AND tintViewID=@ViewID 
			AND numGroupID=@numGroupID 
			AND A.numDomainID=@numDomainID 
			AND ISNULL(A.bitCustom,0) = 0
			AND d.vcDbColumnName not in ('numBillShipCountry','numBillShipState')
		UNION
		SELECT
			A.tintOrder+1 tintOrder,
			numFormFieldID,
			CONCAT('Cust',numFormFieldID),
			Fld_label,
			c.Fld_type,
			'',
			c.numlistid,
			'',			
			1 AS bitCustom,
			'',
			null,
			null,
			null,
			null,
			null,
			'',
			Grp_id
		FROM    
			AdvSerViewConf A
		JOIN 
			CFW_Fld_Master C ON C.Fld_id = A.numFormFieldID
		WHERE   
			A.numFormID = 1
			AND A.tintViewID = @ViewID
			AND C.numDomainID = @numDomainID AND A.numFormID = 1
			AND A.numGroupID = @numGroupID
			AND A.numDomainID = @numDomainID
			AND ISNULL(A.bitCustom,0) = 1
	) T1
	ORDER BY 
		tintOrder asc                              

while @tintOrder>0                              
begin                              
     
	
    IF @vcLookBackTableName = 'AdditionalContactsInformation' 
        SET @Prefix = 'ADC.'
    IF @vcLookBackTableName = 'DivisionMaster' 
        SET @Prefix = 'DM.'
     
	IF @bitCustom = 0 
	BEGIN                           
		SET @vcColumnName= @vcDbColumnName+'~'+ convert(varchar(10),@numFieldId) +'~0'

		if @vcAssociatedControlType='SelectBox'                              
		begin                              
                                              
			  if @vcListItemType='LI'                               
			  begin    
				  IF @numListID=40--Country
				  BEGIN
	  				set @strSql=@strSql+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName+']'                              
					IF @vcDbColumnName ='numCountry'
					BEGIN
						IF @bitContactAddressJoinAdded=0 
					  BEGIN
							set @WhereCondition= @WhereCondition +' left Join AddressDetails AD on AD.numRecordId= ADC.numContactID and AD.tintAddressOf=1 and AD.tintAddressType=0 AND AD.bitIsPrimary=1 and AD.numDomainID= ADC.numDomainID'	
							SET @bitContactAddressJoinAdded=1;
					  END
					  set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID=AD.numCountry'	
					END
					ELSE IF @vcDbColumnName ='numBillCountry'
					BEGIN
						IF @bitBillAddressJoinAdded=0 
						BEGIN
							set @WhereCondition= @WhereCondition +' left Join AddressDetails AD1 on AD1.numRecordId= DM.numDivisionID and AD1.tintAddressOf=2 and AD1.tintAddressType=1 AND AD1.bitIsPrimary=1 and AD1.numDomainID= ADC.numDomainID '
					
							SET @bitBillAddressJoinAdded=1;
						END
						set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID=AD1.numCountry'
					END
					ELSE IF @vcDbColumnName ='numShipCountry'
					BEGIN
						IF @bitShipAddressJoinAdded=0 
						BEGIN
							set @WhereCondition= @WhereCondition +' left Join AddressDetails AD2 on AD2.numRecordId= DM.numDivisionID and AD2.tintAddressOf=2 and AD2.tintAddressType=2 AND AD2.bitIsPrimary=1 and AD2.numDomainID= ADC.numDomainID '
					
							SET @bitShipAddressJoinAdded=1;
						END
						set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID=AD2.numCountry'
					END
				  END                        
				  ELSE
				  BEGIN
						set @strSql=@strSql+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName +']'                              
						set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID='+@vcDbColumnName	
				  END
			  end                              
	                      
			  else if @vcListItemType='T'                               
			  begin                              
				  set @strSql=@strSql+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName +']'                              
			   set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID='+@vcDbColumnName                              
			  end  
			  else if   @vcListItemType='U'                                                     
				begin           
					set @strSql=@strSql+',dbo.fn_GetContactName('+ @Prefix + @vcDbColumnName+') ['+ @vcColumnName +']'
				end   
	
		end  
		ELSE IF @vcAssociatedControlType='ListBox'   
		BEGIN
			if @vcListItemType='S'                               
			  begin                         
				set @strSql=@strSql+',S'+ convert(varchar(3),@tintOrder)+'.vcState'+' ['+ @vcColumnName +']'
				IF @vcDbColumnName ='numState'
				BEGIN
					IF @bitContactAddressJoinAdded=0 
					   BEGIN
							set @WhereCondition= @WhereCondition +' left Join AddressDetails AD on AD.numRecordId= ADC.numContactID and AD.tintAddressOf=1 and AD.tintAddressType=0 AND AD.bitIsPrimary=1 and AD.numDomainID= ADC.numDomainID'	
							SET @bitContactAddressJoinAdded=1;
					  END	
					  set @WhereCondition= @WhereCondition +' left join State S'+ convert(varchar(3),@tintOrder)+ ' on S'+convert(varchar(3),@tintOrder)+ '.numStateID=AD.numState'
				END
				ELSE IF @vcDbColumnName ='numBillState'
				BEGIN
					IF @bitBillAddressJoinAdded=0 
					BEGIN
						set @WhereCondition= @WhereCondition +' left Join AddressDetails AD1 on AD1.numRecordId= DM.numDivisionID and AD1.tintAddressOf=2 and AD1.tintAddressType=1 AND AD1.bitIsPrimary=1 and AD1.numDomainID= ADC.numDomainID '
						SET @bitBillAddressJoinAdded=1;
					END
					set @WhereCondition= @WhereCondition +' left join State S'+ convert(varchar(3),@tintOrder)+ ' on S'+convert(varchar(3),@tintOrder)+ '.numStateID=AD1.numState'
				END
				ELSE IF @vcDbColumnName ='numShipState'
				BEGIN
					IF @bitShipAddressJoinAdded=0 
					BEGIN
						set @WhereCondition= @WhereCondition +' left Join AddressDetails AD2 on AD2.numRecordId= DM.numDivisionID and AD2.tintAddressOf=2 and AD2.tintAddressType=2 AND AD2.bitIsPrimary=1 and AD2.numDomainID= ADC.numDomainID '
						SET @bitShipAddressJoinAdded=1;
					END
					set @WhereCondition= @WhereCondition +' left join State S'+ convert(varchar(3),@tintOrder)+ ' on S'+convert(varchar(3),@tintOrder)+ '.numStateID=AD2.numState'
				END
			  end         
		END 
		ELSE IF @vcAssociatedControlType='TextBox'   
		BEGIN
			IF @vcDbColumnName='vcStreet' OR @vcDbColumnName='vcCity' OR @vcDbColumnName='vcPostalCode'
			begin                              
			   set @strSql=@strSql+',AD.'+ @vcDbColumnName+' ['+ @vcColumnName +']'                              
			   IF @bitContactAddressJoinAdded=0 
			   BEGIN
					set @WhereCondition= @WhereCondition +' left Join AddressDetails AD on AD.numRecordId= ADC.numContactID and AD.tintAddressOf=1 and AD.tintAddressType=0 AND AD.bitIsPrimary=1 and AD.numDomainID= ADC.numDomainID'	
					SET @bitContactAddressJoinAdded=1;
			   END
			end 
			ELSE IF @vcDbColumnName='vcBillStreet' OR @vcDbColumnName='vcBillCity' OR @vcDbColumnName='vcBillPostCode'
			begin                              
			   set @strSql=@strSql+',AD1.'+ REPLACE(REPLACE(@vcDbColumnName,'Bill',''),'PostCode','PostalCode') +' ['+ @vcColumnName +']'                              
			   IF @bitBillAddressJoinAdded=0 
			   BEGIN 
	   				set @WhereCondition= @WhereCondition +' left Join AddressDetails AD1 on AD1.numRecordId= DM.numDivisionID and AD1.tintAddressOf=2 and AD1.tintAddressType=1 AND AD1.bitIsPrimary=1 and AD1.numDomainID= ADC.numDomainID '
	   				SET @bitBillAddressJoinAdded=1;
			   END
			end 
			ELSE IF @vcDbColumnName='vcShipStreet' OR @vcDbColumnName='vcShipCity' OR @vcDbColumnName='vcShipPostCode'
			begin                              
			   set @strSql=@strSql+',AD2.'+ REPLACE(REPLACE(@vcDbColumnName,'Ship',''),'PostCode','PostalCode') +' ['+ @vcColumnName +']'                              
			   IF @bitShipAddressJoinAdded=0 
			   BEGIN
	   				set @WhereCondition= @WhereCondition +' left Join AddressDetails AD2 on AD2.numRecordId= DM.numDivisionID and AD2.tintAddressOf=2 and AD2.tintAddressType=2  AND AD2.bitIsPrimary=1 and AD2.numDomainID= ADC.numDomainID '
	   				SET @bitShipAddressJoinAdded=1;
			   END
			end 
			ELSE
			BEGIN
				set @strSql=@strSql+','+ case  when @vcDbColumnName='numAge' then 'year(getutcdate())-year(bintDOB)' when @vcDbColumnName='numDivisionID' then 'DM.numDivisionID' when @vcDbColumnName='bintCreatedDate' then 'dbo.[FormatedDateFromDate](DateAdd(minute, ' + CAST(-@ClientTimeZoneOffset AS VARCHAR) + ',DM.bintCreatedDate),DM.numDomainID)' else @vcDbColumnName end+' ['+ @vcColumnName +']'		
			END
		END
		ELSE IF @vcAssociatedControlType='Label' and @vcDbColumnName='vcLastSalesOrderDate'
		BEGIN
			SET @WhereCondition = @WhereCondition
											+ ' OUTER APPLY(' + CONCAT('SELECT MAX(bintCreatedDate) AS bintCreatedDate FROM OpportunityMaster WHERE numDomainID = ',@numDomainID,' AND numDivisionID=DM.numDivisionID AND tintOppType = 1 AND tintOppStatus = 1') + ' ) AS TempLastOrder '

			set @strSql=@strSql+','+ CONCAT('ISNULL(dbo.FormatedDateFromDate(DateAdd(minute, ',-@ClientTimeZoneOffset,',TempLastOrder.bintCreatedDate),',@numDomainID,'),'''')') +' ['+ @vcColumnName+']'
		END	                    
		ELSE 
			SET @strSql=@strSql+','+ case  when @vcDbColumnName='numAge' then 'year(getutcdate())-year(bintDOB)' when @vcDbColumnName='numDivisionID' then 'DM.numDivisionID' when @vcDbColumnName='bintCreatedDate' then 'dbo.[FormatedDateFromDate](DM.bintCreatedDate,DM.numDomainID)' else @vcDbColumnName end+' ['+ @vcColumnName +']'
	END
	ELSE IF @bitCustom = 1                
	BEGIN
		--SET @vcColumnName= CONCAT(@vcFormFieldName,'~','Cust',@numFieldId)
		SET @vcColumnName= CONCAT('Cust',@numFieldId,'~',@numFieldId,'~',1)
			
		IF @vcAssociatedControlType = 'TextBox' or @vcAssociatedControlType = 'TextArea'            
		BEGIN
			SET @strSql= @strSql+',CFW'+ convert(VARCHAR(3),@tintOrder)+'.Fld_Value  ['+ @vcColumnName +']'                   
				
			SET @WhereCondition= @WhereCondition 
								+' left Join ' + (CASE WHEN @numFieldGroupID = 1 THEN 'CFW_FLD_Values' WHEN @numFieldGroupID = 4 THEN 'CFW_FLD_Values_Cont' ELSE 'CFW_FLD_Values' END) + ' CFW'+ convert(varchar(3),@tintOrder)
								+ ' on CFW' + convert(varchar(3),@tintOrder) 
								+ '.Fld_Id='+convert(varchar(10),@numFieldId) 
								+ 'and CFW' + convert(varchar(3),@tintOrder) 
								+ '.RecId= (CASE WHEN ' + CAST(@numFieldGroupID AS VARCHAR) + ' = 1 THEN DM.numDivisionId WHEN ' + CAST(@numFieldGroupID AS VARCHAR) + ' = 4 THEN ADC.numContactID ELSE 0 END) '                                                         
		END   
		ELSE IF @vcAssociatedControlType = 'CheckBox'       
		BEGIN
			set @strSql= @strSql+',case when isnull(CFW' + CONVERT(VARCHAR(3), @tintOrder)+ '.Fld_Value,0)='''' then 0 else  isnull(CFW'
											+ CONVERT(VARCHAR(3), @tintOrder)+ '.Fld_Value,0) end ['+ @vcColumnName + ']'
 
			set @WhereCondition= @WhereCondition +' left Join ' + (CASE WHEN @numFieldGroupID = 1 THEN 'CFW_FLD_Values' WHEN @numFieldGroupID = 4 THEN 'CFW_FLD_Values_Cont' ELSE 'CFW_FLD_Values' END) + ' CFW'+ convert(varchar(3),@tintOrder)+ '             
			on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId= (CASE WHEN ' + CAST(@numFieldGroupID AS VARCHAR) + ' = 1 THEN DM.numDivisionId WHEN ' + CAST(@numFieldGroupID AS VARCHAR) + ' = 4 THEN ADC.numContactID ELSE 0 END) '                                                     
		END                
		ELSE IF @vcAssociatedControlType = 'DateField'            
		BEGIN
			set @strSql= @strSql+',dbo.FormatedDateFromDate(CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value,'+convert(varchar(10),@numDomainId)+')  ['+ @vcColumnName +']'                   
			set @WhereCondition= @WhereCondition +' left Join ' + (CASE WHEN @numFieldGroupID = 1 THEN 'CFW_FLD_Values' WHEN @numFieldGroupID = 4 THEN 'CFW_FLD_Values_Cont' ELSE 'CFW_FLD_Values' END) + ' CFW'+ convert(varchar(3),@tintOrder)+ '                 
			on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId= (CASE WHEN ' + CAST(@numFieldGroupID AS VARCHAR) + ' = 1 THEN DM.numDivisionId WHEN ' + CAST(@numFieldGroupID AS VARCHAR) + ' = 4 THEN ADC.numContactID ELSE 0 END) '                                                         
		END                
		ELSE IF @vcAssociatedControlType = 'SelectBox'             
		BEGIN
			set @vcDbColumnName = 'DCust'+Convert(varchar(10),@numFieldId)                
			set @strSql=@strSql+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName +']'                                                          
			set @WhereCondition= @WhereCondition +' left Join ' + (CASE WHEN @numFieldGroupID = 1 THEN 'CFW_FLD_Values' WHEN @numFieldGroupID = 4 THEN 'CFW_FLD_Values_Cont' ELSE 'CFW_FLD_Values' END) + ' CFW'+ convert(varchar(3),@tintOrder)+ '                 
				on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId= (CASE WHEN ' + CAST(@numFieldGroupID AS VARCHAR) + ' = 1 THEN DM.numDivisionId WHEN ' + CAST(@numFieldGroupID AS VARCHAR) + ' = 4 THEN ADC.numContactID ELSE 0 END) '                                                        
			set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID=CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value'                
		END         
		ELSE IF @vcAssociatedControlType = 'CheckBoxList'
		BEGIN
			PRINT 'Custom1'

			SET @strSql= @strSql+',(SELECT STUFF((SELECT CONCAT('', '', vcData) FROM ListDetails WHERE numListID= ' + CAST(@numListID AS varchar) + ' AND numlistitemid IN (SELECT ISNULL(Id,0) FROM dbo.SplitIDs(CFW'+ convert(VARCHAR(3),@tintOrder)+'.Fld_Value,'','')) FOR XML PATH('''')), 1, 1, ''''))  ['+ @vcColumnName +']'   

			SET @WhereCondition= @WhereCondition 
								+' left Join ' + (CASE WHEN @numFieldGroupID = 1 THEN 'CFW_FLD_Values' WHEN @numFieldGroupID = 4 THEN 'CFW_FLD_Values_Cont' ELSE 'CFW_FLD_Values' END) + ' CFW'+ convert(varchar(3),@tintOrder)
								+ ' on CFW' + convert(varchar(3),@tintOrder) 
								+ '.Fld_Id='+convert(varchar(10),@numFieldId) 
								+ 'and CFW' + convert(varchar(3),@tintOrder) 
								+ '.RecId= (CASE WHEN ' + CAST(@numFieldGroupID AS VARCHAR) + ' = 1 THEN DM.numDivisionId WHEN ' + CAST(@numFieldGroupID AS VARCHAR) + ' = 4 THEN ADC.numContactID ELSE 0 END) '										
		
		END        
	END          
                   
	SELECT TOP 1
		@tintOrder=tintOrder,
		@numFieldID=numFormFieldID,
		@vcDbColumnName=vcDbColumnName,
		@vcFormFieldName=vcFieldName,
		@vcAssociatedControlType=vcAssociatedControlType,
		@vcListItemType=vcListItemType,
		@numListID=numListID,
		@vcLookBackTableName=vcLookBackTableName,
		@bitCustom=bitCustom,
	    @vcOrigDbColumnName=vcOrigDbColumnName,
		@bitAllowSorting=bitAllowSorting,
		@bitAllowEdit= bitAllowEdit,
		@ListRelID=ListRelID,
		@intColumnWidth=intColumnWidth,
		@bitAllowFiltering=bitAllowFiltering,
		@vcfieldatatype=vcFieldDataType,
		@numFieldGroupID=numGroupID
	FROM
	(
		SELECT
			A.tintOrder+1 tintOrder, 
			numFormFieldID,
			vcDbColumnName,
			vcFieldName,
			vcAssociatedControlType,
			vcListItemType,
			numListID,
			vcLookBackTableName,
			0 AS bitCustom,
            vcOrigDbColumnName,
			bitAllowSorting,
			bitAllowEdit,
			ListRelID,
			A.intColumnWidth as intColumnWidth,
			bitAllowFiltering,
			vcFieldDataType,
			0 AS numGroupID
		FROM 
			AdvSerViewConf A                              
		JOIN 
			View_DynamicDefaultColumns D                               
		ON 
			D.numFieldId=A.numFormFieldId  
			AND d.numformId = 1                           
		WHERE 
			D.numDomainID = @numDomainID 
			AND D.numFormID = 1 
			AND A.numFormID = 1 
			AND tintViewID=@ViewID 
			AND numGroupID=@numGroupID 
			AND A.numDomainID=@numDomainID 
			AND ISNULL(A.bitCustom,0) = 0
			AND d.vcDbColumnName not in ('numBillShipCountry','numBillShipState')
			AND A.tintOrder > @tintOrder-1
		UNION
		SELECT
			A.tintOrder+1 tintOrder,
			numFormFieldID,
			CONCAT('Cust',numFormFieldID),
			Fld_label,
			c.Fld_type,
			'',
			c.numlistid,
			'',
			1 AS bitCustom,
			'',
			null,
			null,
			null,
			null,
			null,
			'',			
			Grp_id
		FROM    
			AdvSerViewConf A
		JOIN 
			CFW_Fld_Master C ON C.Fld_id = A.numFormFieldID
		WHERE   
			A.numFormID = 1
			AND A.tintViewID = @ViewID
			AND C.numDomainID = @numDomainID AND A.numFormID = 1
			AND A.numGroupID = @numGroupID
			AND A.numDomainID = @numDomainID
			AND ISNULL(A.bitCustom,0) = 1
			AND A.tintOrder > @tintOrder-1
	) T1
	ORDER BY 
		tintOrder asc                
                                   
                                      
 if @@rowcount=0 set @tintOrder=0                              
end                              

                                                                  
  declare @firstRec as integer                                                                    
  declare @lastRec as integer                                                                    
 set @firstRec= (@CurrentPage-1) * @PageSize                      
 set @lastRec= (@CurrentPage*@PageSize+1)                                                                     
set @TotRecs=(select count(*) from #tempTable)                                                   
                                          
if @GetAll=0         
begin        
        
 if @bitMassUpdate=1            
 begin  

   Declare @strReplace as varchar(8000) = ''
    set @strReplace = case when @LookTable='DivisionMaster' then 'DM.numDivisionID' when @LookTable='AdditionalContactsInformation' then 'ADC.numContactId' when @LookTable='ContactAddress' then 'ADC.numContactId' when @LookTable='ShipAddress' then 'DM.numDivisionID' when @LookTable='BillAddress' then 'DM.numDivisionID' when @LookTable='CompanyInfo' then 'C.numCompanyId' end           
   +' FROM AdditionalContactsInformation ADC inner join DivisionMaster DM on ADC.numDivisionID = DM.numDivisionID                  
   JOIN CompanyInfo C ON DM.numCompanyID = C.numCompanyId                                
    '+@WhereCondition        
    set @strMassUpdate=replace(@strMassUpdate,'@$replace',@strReplace)      
    PRINT CONCAT('MassUPDATE:',@strMassUpdate)
  exec (@strMassUpdate)         
 end         
 
 
 IF (@Where = 'DUP')
 BEGIN
	set @strSql=@strSql+' ,Cast((select top 1 bintCreatedDate from OpportunityMaster where numDivisionId=DM.numDivisionID order by numOppId desc) AS DATE)  AS [Last Order Date~bintCreatedDate] FROM AdditionalContactsInformation ADC
	  JOIN DivisionMaster DM ON ADC.numDivisionId = DM.numDivisionID
	  JOIN CompanyInfo C ON DM.numCompanyID = C.numCompanyId    
	  JOIN OpportunityMaster OM on OM.numDivisionID= DM.numDivisionID                  
	  inner join #tempTable T on T.numContactId=ADC.numContactId ' 
	  + @WhereCondition +'
	  WHERE ID > '+convert(varchar(10),@firstRec)+ ' and ID <'+ convert(varchar(10),@lastRec) +  @WCondition + ' order by C.vcCompanyName, T.ID' -- @OrderBy
	  PRINT @strSql
	  exec(@strSql)                                                                 
	 drop table #tempTable
	 RETURN	
 END
ELSE
BEGIN
	SET @strSql = @strSql + ' ,Cast((select top 1 bintCreatedDate from OpportunityMaster where numDivisionId=DM.numDivisionID order by numOppId desc) AS DATE)  AS [Last Order Date~bintCreatedDate] FROM AdditionalContactsInformation ADC
		JOIN DivisionMaster DM ON ADC.numDivisionId = DM.numDivisionID   
	  JOIN CompanyInfo C ON DM.numCompanyID = C.numCompanyId                              
	   '+@WhereCondition+' inner join #tempTable T on T.numContactId=ADC.numContactId                                                               
	  WHERE ID > '+convert(varchar(10),@firstRec)+ ' and ID <'+ convert(varchar(10),@lastRec)
	  
	   
	SET @strSql=@strSql + ' order by C.vcCompanyName, T.ID' 
END
             
end                           
 else        
 begin                      
     set @strSql=@strSql+' ,Cast((select top 1 bintCreatedDate from OpportunityMaster where numDivisionId=DM.numDivisionID order by numOppId desc) AS DATE)  AS [Last Order Date~bintCreatedDate] FROM AdditionalContactsInformation ADC                                                   
  JOIN DivisionMaster DM ON ADC.numDivisionId = DM.numDivisionID                                                                   
  JOIN CompanyInfo C ON DM.numCompanyID = C.numCompanyId                              
  '+@WhereCondition+' join #tempTable T on T.numContactId=ADC.numContactId'        
  
	   SET @strSql=@strSql + ' order by C.vcCompanyName, T.ID'   
	end       
print @strSql                                                      
exec(@strSql) 
drop table #tempTable


	SELECT
		tintOrder,
		vcDbColumnName,
		vcFieldName,
		vcAssociatedControlType,
		vcListItemType,
		numListID,
		vcLookBackTableName,
		numFieldID,
		numFormFieldID,
		intColumnWidth,
		bitCustom AS bitCustomField,
		vcOrigDbColumnName,
		bitAllowSorting,
		bitAllowEdit,
		ListRelID,
		bitAllowFiltering,
		vcFieldDataType
	FROM
	(
		SELECT
			A.tintOrder+1 tintOrder,
			vcDbColumnName,
			vcFieldName,
			vcAssociatedControlType,
			vcListItemType,
			numListID,
			vcLookBackTableName,
			numFieldID,
			numFormFieldID,
			ISNULL(A.intColumnWidth,0) AS intColumnWidth,
			ISNULL(D.bitCustom,0) AS bitCustom,
			vcOrigDbColumnName,
			bitAllowSorting,
			bitAllowEdit,
			ListRelID,
			bitAllowFiltering,
			vcFieldDataType
		FROM 
			AdvSerViewConf A                              
		JOIN 
			View_DynamicDefaultColumns D                               
		ON 
			D.numFieldId=A.numFormFieldId  
			AND d.numformId = 1                           
		WHERE 
			D.numDomainID = @numDomainID 
			AND D.numFormID = 1 
			AND A.numFormID = 1 
			AND tintViewID=@ViewID 
			AND numGroupID=@numGroupID 
			AND A.numDomainID=@numDomainID 
			AND ISNULL(A.bitCustom,0) = 0
			AND d.vcDbColumnName not in ('numBillShipCountry','numBillShipState')
		UNION
		SELECT
			A.tintOrder+1 tintOrder,
			CONCAT('Cust',numFormFieldID),
			Fld_label,
			Fld_type,
			'',
			numListID,
			'',
			numFormFieldID,
			numFormFieldID,
			ISNULL(A.intColumnWidth,0) AS intColumnWidth,
			1 AS bitCustom,
			'',
			null,
			null,
			null,
			null,
			''
		FROM    
			AdvSerViewConf A
		JOIN 
			CFW_Fld_Master C ON C.Fld_id = A.numFormFieldID
		WHERE   
			A.numFormID = 1
			AND A.tintViewID = @ViewID
			AND C.numDomainID = @numDomainID AND A.numFormID = 1
			AND A.numGroupID = @numGroupID
			AND A.numDomainID = @numDomainID
			AND ISNULL(A.bitCustom,0) = 1
	) T1
	ORDER BY 
		tintOrder asc       

IF OBJECT_ID('tempdb..#tempAddress') IS NOT NULL
BEGIN
    DROP TABLE #tempAddress
END
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_CalculateCommissionPaidBizDoc')
DROP PROCEDURE USP_CalculateCommissionPaidBizDoc
GO
CREATE PROCEDURE [dbo].[USP_CalculateCommissionPaidBizDoc]
	-- Add the parameters for the stored procedure here
	 @numComPayPeriodID AS NUMERIC(18,0),
	 @numDomainID AS NUMERIC(18,0),
	 @ClientTimeZoneOffset INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
SET NOCOUNT ON;

BEGIN TRY
BEGIN TRANSACTION
	DECLARE @dtPayStart DATETIME
	DECLARE @dtPayEnd DATETIME

	SELECT @dtPayStart=dtStart,@dtPayEnd=dtEnd FROM CommissionPayPeriod WHERE numComPayPeriodID = @numComPayPeriodID

	SET @dtPayStart = DATEADD (MS , 000 , @dtPayStart)
	SET @dtPayEnd = DATEADD (MS , 997 , @dtPayEnd)

	-- DELETE UNPAID COMMISSION ENTRIES BETWEEN PAY PERIOD DATE RANGE
	DELETE FROM 
		BizDocComission 
	WHERE 
		numDomainId=@numDomainID 
		AND ISNULL(bitCommisionPaid,0)=0 
		AND numOppBizDocItemID IN (SELECT 
										ISNULL(numOppBizDocItemID,0)
									FROM 
										OpportunityBizDocs OppBiz
									INNER JOIN
										OpportunityMaster OppM
									ON
										OppBiz.numOppID = OppM.numOppID
									INNER JOIN
										OpportunityBizDocItems OppBizItems
									ON
										OppBiz.numOppBizDocsId = OppBizItems.numOppBizDocID
									OUTER APPLY
									(
										SELECT 
											MAX(DM.dtDepositDate) dtDepositDate
										FROM 
											DepositMaster DM 
										JOIN 
											dbo.DepositeDetails DD
										ON 
											DM.numDepositId=DD.numDepositID 
										WHERE 
											DM.tintDepositePage=2 
											AND DM.numDomainId=@numDomainId
											AND DD.numOppID=OppBiz.numOppID 
											AND DD.numOppBizDocsID =OppBiz.numOppBizDocsID
									) TempDepositMaster
									WHERE
										OppM.numDomainId = @numDomainID 
										AND OppM.tintOppType = 1
										AND OppM.tintOppStatus = 1
										AND OppBiz.bitAuthoritativeBizDocs = 1
										AND ISNULL(OppBiz.monAmountPaid,0) >= OppBiz.monDealAmount
										AND (TempDepositMaster.dtDepositDate IS NOT NULL AND (CAST(DATEADD(MINUTE,-@ClientTimeZoneOffset,TempDepositMaster.dtDepositDate) AS DATE) BETWEEN @dtPayStart AND @dtPayEnd)))

	--GET COMMISSION RULE OF DOMAIN
	DECLARE @TempCommissionRule TABLE
	(
		ID INT IDENTITY(1,1),
		numComRuleID NUMERIC(18,0),
		tintComBasedOn TINYINT,
		tintComType TINYINT,
		tintComAppliesTo TINYINT,
		tintComOrgType TINYINT,
		tintAssignTo TINYINT
	)

	INSERT INTO
		@TempCommissionRule
	SELECT
		CR.numComRuleID,
		CR.tintComBasedOn,
		CR.tintComType,
		CR.tintComAppliesTo,
		CR.tintComOrgType,
		CR.tintAssignTo
	FROM
		CommissionRules CR
	LEFT JOIN
		PriceBookPriorities PBP
	ON	
		CR.tintComAppliesTo = PBP.Step2Value 
		AND CR.tintComOrgType = PBP.Step3Value
	WHERE
		CR.numDomainID = @numDomainID
		AND ISNULL(PBP.[Priority],0) > 0
	ORDER BY
		ISNULL(PBP.[Priority],0) ASC

	--GET COMMISSION GLOBAL SETTINGS
	DECLARE @numDiscountServiceItemID NUMERIC(18,0)
	DECLARE @numShippingServiceItemID NUMERIC(18,0)
	DECLARE @tintCommissionType TINYINT
	DECLARE @bitIncludeTaxAndShippingInCommission BIT

	SELECT 
		@numDiscountServiceItemID=numDiscountServiceItemID, 
		@numShippingServiceItemID = numShippingServiceItemID, 
		@tintCommissionType=tintCommissionType, 
		@bitIncludeTaxAndShippingInCommission=bitIncludeTaxAndShippingInCommission 
	FROM 
		Domain 
	WHERE 
		numDomainID=@numDomainID

	--TEMPORARY TABLES TO HOLD DATA
	DECLARE @TempBizCommission TABLE
	(
		numUserCntID NUMERIC(18,0),
		numOppID NUMERIC(18,0),
		numOppBizDocID NUMERIC(18,0),
		numOppBizDocItemID NUMERIC(18,0),
		monCommission MONEY,
		numComRuleID NUMERIC(18,0),
		tintComType TINYINT,
		tintComBasedOn TINYINT,
		decCommission FLOAT,
		tintAssignTo TINYINT
	)

	DECLARE @TABLEPAID TABLE
	(
		ID INT IDENTITY(1,1),
		numDivisionID NUMERIC(18,0),
		numRelationship NUMERIC(18,0),
		numProfile NUMERIC(18,0),
		numAssignedTo NUMERIC(18,0),
		numRecordOwner NUMERIC(18,0),
		numItemCode NUMERIC(18,0),
		numItemClassification NUMERIC(18,0),
		numOppID NUMERIC(18,0),
		numOppBizDocID NUMERIC(18,0),
		numOppBizDocItemID NUMERIC(18,0),
		numUnitHour FLOAT,
		monTotAmount MONEY,
		monVendorCost MONEY,
		monAvgCost MONEY,
		numPartner NUMERIC(18,0)
	)

	-- GET FULLY PAID INVOICES WITHIN PAY PERIOD
	INSERT INTO 
		@TABLEPAID
	SELECT 
		OppM.numDivisionID,
		ISNULL(CompanyInfo.numCompanyType,0),
		ISNULL(CompanyInfo.vcProfile,0),
		OppM.numAssignedTo,
		OppM.numRecOwner,
		OppBizItems.numItemCode,
		Item.numItemClassification,
		OppM.numOppID,
		OppBizItems.numOppBizDocID,
		OppBizItems.numOppBizDocItemID,
		ISNULL(OppBizItems.numUnitHour,0),
		ISNULL(OppBizItems.monTotAmount,0),
		(ISNULL(OppMItems.monVendorCost,0) * ISNULL(OppBizItems.numUnitHour,0)),
		(ISNULL(OppMItems.monAvgCost,0) * ISNULL(OppBizItems.numUnitHour,0)),
		OppM.numPartner
	FROM 
		OpportunityBizDocs OppBiz
	INNER JOIN
		OpportunityMaster OppM
	ON
		OppBiz.numOppID = OppM.numOppID
	INNER JOIN
		DivisionMaster 
	ON
		OppM.numDivisionID = DivisionMaster.numDivisionID
	INNER JOIN
		CompanyInfo
	ON
		DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
	INNER JOIN
		OpportunityBizDocItems OppBizItems
	ON
		OppBiz.numOppBizDocsId = OppBizItems.numOppBizDocID
	INNER JOIN
		OpportunityItems OppMItems 
	ON
		OppBizItems.numOppItemID = OppMItems.numoppitemtCode
	INNER JOIN
		Item
	ON
		OppBizItems.numItemCode = Item.numItemCode
	LEFT JOIN
		BizDocComission BDC
	ON
		OppM.numOppId = BDC.numOppID 
		AND OppBiz.numOppBizDocsId = BDC.numOppBizDocId
		AND OppBizItems.numOppBizDocItemID = BDC.numOppBizDocItemID
	OUTER APPLY
	(
		SELECT 
			MAX(DM.dtDepositDate) dtDepositDate
		FROM 
			DepositMaster DM 
		JOIN 
			dbo.DepositeDetails DD
		ON 
			DM.numDepositId=DD.numDepositID 
		WHERE 
			DM.tintDepositePage=2 
			AND DM.numDomainId=@numDomainId
			AND DD.numOppID=OppBiz.numOppID 
			AND DD.numOppBizDocsID =OppBiz.numOppBizDocsID
	) TempDepositMaster
	WHERE
		OppM.numDomainId = @numDomainID 
		AND OppM.tintOppType = 1
		AND OppM.tintOppStatus = 1
		AND OppBiz.bitAuthoritativeBizDocs = 1
		AND ISNULL(OppBiz.monAmountPaid,0) >= OppBiz.monDealAmount
		AND (TempDepositMaster.dtDepositDate IS NOT NULL AND (CAST(DATEADD(MINUTE,-@ClientTimeZoneOffset,TempDepositMaster.dtDepositDate) AS DATE) BETWEEN @dtPayStart AND @dtPayEnd))
		AND OppBizItems.numItemCode NOT IN ( @numDiscountServiceItemID )  /*DO NOT INCLUDE DISCOUNT ITEM*/
		AND 1 = (
					CASE 
						WHEN OppBizItems.numItemCode = @numShippingServiceItemID 
						THEN 
							CASE 
								WHEN ISNULL(@bitIncludeTaxAndShippingInCommission,0) = 1
								THEN	
									1
								ELSE	
									0
							END
						ELSE 
							1 
					END
				)/*CONDITION TO INCLUDE/EXCLUDE INCLUDE SHIPPING ITEM */
		AND BDC.numOppBizDocItemID IS NULL --NO ENTRY AVAILABLE IN BizDocComission TABLE FOR BIZDOC ITEM


		-- LOOP ALL COMMISSION RULES
		DECLARE @i INT = 1
		DECLARE @COUNT INT = 0

		SELECT @COUNT=COUNT(*) FROM @TempCommissionRule

		DECLARE @TEMPITEM TABLE
		(
			ID INT,
			UniqueID INT,
			numUserCntID NUMERIC(18,0),
			numOppID NUMERIC(18,0),
			numOppBizDocID NUMERIC(18,0),
			numOppBizDocItemID NUMERIC(18,0),
			numItemCode NUMERIC(18,0),
			numUnitHour FLOAT,
			monTotAmount MONEY,
			monVendorCost MONEY,
			monAvgCost MONEY
		)

		WHILE @i <= @COUNT
		BEGIN
			DECLARE @numComRuleID NUMERIC(18,0)
			DECLARE @tintComBasedOn TINYINT
			DECLARE @tintComAppliesTo TINYINT
			DECLARE @tintComOrgType TINYINT
			DECLARE @tintComType TINYINT
			DECLARE @tintAssignTo TINYINT
			DECLARE @numTotalAmount AS MONEY
			DECLARE @numTotalUnit AS FLOAT

			--CLEAR PREVIOUS VALUES FROM TEMP TABLE
			DELETE FROM @TEMPITEM

			--FETCH COMMISSION RULE
			SELECT @numComRuleID=numComRuleID,@tintComBasedOn=tintComBasedOn,@tintComType=tintComType,@tintComAppliesTo=tintComAppliesTo,@tintComOrgType=tintComOrgType,@tintAssignTo=tintAssignTo FROM @TempCommissionRule WHERE ID=@i

			--FIND ALL BIZ DOC ITEMS WHERE COMMISSION IS NOT PAID AND CURRENT RULE ITEMS, ORGANIZATION AND ASSINGED TO FILTER CAN BE APPLIED
			INSERT INTO
				@TEMPITEM
			SELECT
				ROW_NUMBER() OVER (ORDER BY ID),
				ID,
				(CASE @tintAssignTo WHEN 1 THEN numAssignedTo WHEN 2 THEN numRecordOwner ELSE numPartner END),
				numOppID,
				numOppBizDocID,
				numOppBizDocItemID,
				numItemCode,
				numUnitHour,
				monTotAmount,
				monVendorCost,
				monAvgCost
			FROM
				@TABLEPAID
			WHERE
				1 = (CASE @tintComAppliesTo
						--SPECIFIC ITEMS
						WHEN 1 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleItems WHERE numComRuleID = @numComRuleID AND ISNULL(numValue,0) = numItemCode) > 0 THEN 1 ELSE 0 END
						-- ITEM WITH SPECIFIC CLASSIFICATIONS
						WHEN 2 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleItems WHERE numComRuleID = @numComRuleID AND ISNULL(numValue,0) = numItemClassification) > 0 THEN 1 ELSE 0 END
						-- ALL ITEMS
						ELSE 1
					END)
				AND 1 = (CASE @tintComOrgType
							-- SPECIFIC ORGANIZATION
							WHEN 1 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleOrganization WHERE numComRuleID = @numComRuleID AND ISNULL(numValue,0) = numDivisionID) > 0 THEN 1 ELSE 0 END
							-- ORGANIZATION WITH SPECIFIC RELATIONSHIP AND PROFILE
							WHEN 2 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleOrganization WHERE numComRuleID = @numComRuleID AND ISNULL(numValue,0) = numRelationship AND ISNULL(numProfile,0) = numProfile) > 0 THEN 1 ELSE 0 END
							-- ALL ORGANIZATIONS
							ELSE 1
						END)
				AND 1 = (CASE @tintAssignTo 
							-- ORDER ASSIGNED TO
							WHEN 1 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleContacts WHERE numComRuleID = @numComRuleID AND numValue=numAssignedTo) > 0 THEN 1 ELSE 0 END
							-- ORDER OWNER
							WHEN 2 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleContacts WHERE numComRuleID = @numComRuleID AND numValue=numRecordOwner) > 0 THEN 1 ELSE 0 END
							-- Order Partner
							WHEN 3 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleContacts WHERE numComRuleID = @numComRuleID AND numValue=numPartner) > 0 THEN 1 ELSE 0 END
							-- NO OPTION SELECTED IN RULE
							ELSE 0
						END)


			--CALCULATE COMMISSION AND STORE IN TEMPARORY TABLE
					INSERT INTO @TempBizCommission 
					(
						numUserCntID,
						numOppID,
						numOppBizDocID,
						numOppBizDocItemID,
						monCommission,
						numComRuleID,
						tintComType,
						tintComBasedOn,
						decCommission,
						tintAssignTo
					)
					SELECT
						numUserCntID,
						numOppID,
						numOppBizDocID,
						numOppBizDocItemID,
						CASE @tintComType 
							WHEN 1 --PERCENT
								THEN 
									CASE @tintCommissionType
										--TOTAL AMOUNT PAID
										WHEN 1 THEN monTotAmount * (T2.decCommission / CAST(100 AS FLOAT))
										--ITEM GROSS PROFIT (VENDOR COST)
										WHEN 2 THEN (ISNULL(monTotAmount,0) - ISNULL(monVendorCost,0)) * (T2.decCommission / CAST(100 AS FLOAT))
										--ITEM GROSS PROFIT (AVERAGE COST)
										WHEN 3 THEN(ISNULL(monTotAmount,0) - ISNULL(monAvgCost,0)) * (T2.decCommission / CAST(100 AS FLOAT))
									END
							ELSE  --FLAT
								T2.decCommission
						END,
						@numComRuleID,
						@tintComType,
						@tintComBasedOn,
						T2.decCommission,
						@tintAssignTo
					FROM 
						@TEMPITEM AS T1
					CROSS APPLY
					(
						SELECT TOP 1 
							ISNULL(decCommission,0) AS decCommission
						FROM 
							CommissionRuleDtl 
						WHERE 
							numComRuleID=@numComRuleID 
							AND ISNULL(decCommission,0) > 0
							AND 1 = (CASE 
									WHEN @tintComBasedOn = 1 --BASED ON AMOUNT SOLD 
									THEN (CASE WHEN (T1.monTotAmount BETWEEN intFrom AND intTo) THEN 1 ELSE 0 END)
									WHEN @tintComBasedOn = 2 --BASED ON UNITS SOLD
									THEN (CASE WHEN (T1.numUnitHour BETWEEN intFrom AND intTo) THEN 1 ELSE 0 END)
									ELSE 0
									END)
					) AS T2
					WHERE
						1 = (CASE @tintCommissionType 
								--TOTAL PAID INVOICE
								WHEN 1 THEN CASE WHEN (ISNULL(monTotAmount,0) > 0) THEN 1 ELSE 0 END 
								-- ITEM GROSS PROFIT (VENDOR COST)
								WHEN 2 THEN CASE WHEN ((ISNULL(monTotAmount,0) - ISNULL(monVendorCost,0)) > 0) THEN 1 ELSE 0 END 
								-- ITEM GROSS PROFIT (AVERAGE COST)
								WHEN 3 THEN CASE WHEN ((ISNULL(monTotAmount,0) - ISNULL(monAvgCost,0)) > 0) THEN 1 ELSE 0 END
								ELSE
									0 
							END) 
						AND (
								SELECT 
									COUNT(*) 
								FROM 
									@TempBizCommission 
								WHERE 
									numUserCntID=T1.numUserCntID 
									AND numOppID=T1.numOppID 
									AND numOppBizDocID = T1.numOppBizDocID 
									AND numOppBizDocItemID = T1.numOppBizDocItemID 
									AND tintAssignTo = @tintAssignTo
							) = 0

			SET @i = @i + 1

			SET @numComRuleID = 0
			SET @tintComBasedOn = 0
			SET @tintComAppliesTo = 0
			SET @tintComOrgType = 0
			SET @tintComType = 0
			SET @tintAssignTo = 0
			SET @numTotalAmount = 0
			SET @numTotalUnit = 0
		END

		--INSERT CALCULATED ENTERIES IN BIZDOCCOMMISSION TABLE
		INSERT INTO BizDocComission
		(	
			numDomainID,
			numUserCntID,
			numOppBizDocId,
			numComissionAmount,
			numOppBizDocItemID,
			numComRuleID,
			tintComType,
			tintComBasedOn,
			decCommission,
			numOppID,
			bitFullPaidBiz,
			numComPayPeriodID,
			tintAssignTo
		)
		SELECT
			@numDomainID,
			numUserCntID,
			numOppBizDocID,
			monCommission,
			numOppBizDocItemID,
			numComRuleID,
			tintComType,
			tintComBasedOn,
			decCommission,
			numOppID,
			1,
			@numComPayPeriodID,
			tintAssignTo
		FROM
			@TempBizCommission
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
END
GO

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_CalculateCommissionUnPaidBizDoc')
DROP PROCEDURE USP_CalculateCommissionUnPaidBizDoc
GO
CREATE PROCEDURE [dbo].[USP_CalculateCommissionUnPaidBizDoc]
	-- Add the parameters for the stored procedure here
	 @numComPayPeriodID AS NUMERIC(18,0),
	 @numDomainID AS NUMERIC(18,0),
	 @ClientTimeZoneOffset INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
SET NOCOUNT ON;

BEGIN TRY
BEGIN TRANSACTION
	DECLARE @dtPayStart DATETIME 
	DECLARE @dtPayEnd DATETIME

	SELECT @dtPayStart=dtStart,@dtPayEnd=dtEnd FROM CommissionPayPeriod WHERE numComPayPeriodID = @numComPayPeriodID

	SET @dtPayStart = DATEADD (MS , 000 , @dtPayStart)
	SET @dtPayEnd = DATEADD (MS , 997 , @dtPayEnd)

	-- DELETE UNPAID COMMISSION ENTRIES BETWEEN PAY PERIOD DATE RANGE
	DELETE FROM 
		BizDocComission 
	WHERE 
		numDomainId=@numDomainID 
		AND ISNULL(bitCommisionPaid,0)=0 
		AND numOppBizDocItemID IN (SELECT 
										ISNULL(numOppBizDocItemID,0)
									FROM 
										OpportunityBizDocs OppBiz
									INNER JOIN
										OpportunityMaster OppM
									ON
										OppBiz.numOppID = OppM.numOppID
									INNER JOIN
										OpportunityBizDocItems OppBizItems
									ON
										OppBiz.numOppBizDocsId = OppBizItems.numOppBizDocID
									WHERE
										OppM.numDomainId = @numDomainID 
										AND OppM.tintOppType = 1
										AND OppM.tintOppStatus = 1
										AND OppBiz.bitAuthoritativeBizDocs = 1
										AND ISNULL(OppBiz.monAmountPaid,0) < OppBiz.monDealAmount
										AND (OppBiz.dtFromDate IS NOT NULL AND (CAST(DATEADD(MINUTE,-@ClientTimeZoneOffset,OppBiz.dtFromDate) AS DATE) BETWEEN @dtPayStart AND @dtPayEnd)))
 
	--GET COMMISSION RULE OF DOMAIN
	DECLARE @TempCommissionRule TABLE
	(
		ID INT IDENTITY(1,1),
		numComRuleID NUMERIC(18,0),
		tintComBasedOn TINYINT,
		tintComType TINYINT,
		tintComAppliesTo TINYINT,
		tintComOrgType TINYINT,
		tintAssignTo TINYINT
	)

	INSERT INTO
		@TempCommissionRule
	SELECT
		CR.numComRuleID,
		CR.tintComBasedOn,
		CR.tintComType,
		CR.tintComAppliesTo,
		CR.tintComOrgType,
		CR.tintAssignTo
	FROM
		CommissionRules CR
	LEFT JOIN
		PriceBookPriorities PBP
	ON	
		CR.tintComAppliesTo = PBP.Step2Value 
		AND CR.tintComOrgType = PBP.Step3Value
	WHERE
		CR.numDomainID = @numDomainID
		AND ISNULL(PBP.[Priority],0) > 0
	ORDER BY
		ISNULL(PBP.[Priority],0) ASC

	--GET COMMISSION GLOBAL SETTINGS
	DECLARE @numDiscountServiceItemID NUMERIC(18,0)
	DECLARE @numShippingServiceItemID NUMERIC(18,0)
	DECLARE @tintCommissionType TINYINT
	DECLARE @bitIncludeTaxAndShippingInCommission BIT

	SELECT 
		@numDiscountServiceItemID=numDiscountServiceItemID, 
		@numShippingServiceItemID = numShippingServiceItemID, 
		@tintCommissionType=tintCommissionType, 
		@bitIncludeTaxAndShippingInCommission=bitIncludeTaxAndShippingInCommission 
	FROM 
		Domain 
	WHERE 
		numDomainID=@numDomainID

	--TEMPORARY TABLES TO HOLD DATA
	DECLARE @TempBizCommission TABLE
	(
		numUserCntID NUMERIC(18,0),
		numOppID NUMERIC(18,0),
		numOppBizDocID NUMERIC(18,0),
		numOppBizDocItemID NUMERIC(18,0),
		monCommission MONEY,
		numComRuleID NUMERIC(18,0),
		tintComType TINYINT,
		tintComBasedOn TINYINT,
		decCommission FLOAT,
		tintAssignTo TINYINT
	)

	DECLARE @TABLEPAID TABLE
	(
		ID INT IDENTITY(1,1),
		numDivisionID NUMERIC(18,0),
		numRelationship NUMERIC(18,0),
		numProfile NUMERIC(18,0),
		numAssignedTo NUMERIC(18,0),
		numRecordOwner NUMERIC(18,0),
		numItemCode NUMERIC(18,0),
		numItemClassification NUMERIC(18,0),
		numOppID NUMERIC(18,0),
		numOppBizDocID NUMERIC(18,0),
		numOppBizDocItemID NUMERIC(18,0),
		numUnitHour FLOAT,
		monTotAmount MONEY,
		monVendorCost MONEY,
		monAvgCost MONEY,
		numPartner NUMERIC(18,0)
	)

	-- GET FULLY PAID INVOICES WITHIN PAY PERIOD
	INSERT INTO 
		@TABLEPAID
	SELECT 
		OppM.numDivisionID,
		ISNULL(CompanyInfo.numCompanyType,0),
		ISNULL(CompanyInfo.vcProfile,0),
		OppM.numAssignedTo,
		OppM.numRecOwner,
		OppBizItems.numItemCode,
		Item.numItemClassification,
		OppM.numOppID,
		OppBizItems.numOppBizDocID,
		OppBizItems.numOppBizDocItemID,
		ISNULL(OppBizItems.numUnitHour,0),
		ISNULL(OppBizItems.monTotAmount,0),
		(ISNULL(OppMItems.monVendorCost,0) * ISNULL(OppBizItems.numUnitHour,0)),
		(ISNULL(OppMItems.monAvgCost,0) * ISNULL(OppBizItems.numUnitHour,0)),
		OppM.numPartner
	FROM 
		OpportunityBizDocs OppBiz
	INNER JOIN
		OpportunityMaster OppM
	ON
		OppBiz.numOppID = OppM.numOppID
	INNER JOIN
		DivisionMaster 
	ON
		OppM.numDivisionID = DivisionMaster.numDivisionID
	INNER JOIN
		CompanyInfo
	ON
		DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
	INNER JOIN
		OpportunityBizDocItems OppBizItems
	ON
		OppBiz.numOppBizDocsId = OppBizItems.numOppBizDocID
	INNER JOIN
		OpportunityItems OppMItems 
	ON
		OppBizItems.numOppItemID = OppMItems.numoppitemtCode
	INNER JOIN
		Item
	ON
		OppBizItems.numItemCode = Item.numItemCode
	LEFT JOIN
		BizDocComission BDC
	ON
		OppM.numOppId = BDC.numOppID 
		AND OppBiz.numOppBizDocsId = BDC.numOppBizDocId
		AND OppBizItems.numOppBizDocItemID = BDC.numOppBizDocItemID
	WHERE
		OppM.numDomainId = @numDomainID 
		AND OppM.tintOppType = 1
		AND OppM.tintOppStatus = 1
		AND OppBiz.bitAuthoritativeBizDocs = 1
		AND ISNULL(OppBiz.monAmountPaid,0) < OppBiz.monDealAmount
		AND (OppBiz.dtFromDate IS NOT NULL AND (CAST(DATEADD(MINUTE,-@ClientTimeZoneOffset,OppBiz.dtFromDate) AS DATE) BETWEEN @dtPayStart AND @dtPayEnd))
		AND OppBizItems.numItemCode NOT IN ( @numDiscountServiceItemID )  /*DO NOT INCLUDE DISCOUNT ITEM*/
		AND 1 = (
					CASE 
						WHEN OppBizItems.numItemCode = @numShippingServiceItemID 
						THEN 
							CASE 
								WHEN ISNULL(@bitIncludeTaxAndShippingInCommission,0) = 1
								THEN	
									1
								ELSE	
									0
							END
						ELSE 
							1 
					END
				)/*CONDITION TO INCLUDE/EXCLUDE INCLUDE SHIPPING ITEM */
		AND BDC.numOppBizDocItemID IS NULL --NO ENTRY AVAILABLE IN BizDocComission TABLE FOR BIZDOC ITEM


		-- LOOP ALL COMMISSION RULES
		DECLARE @i INT = 1
		DECLARE @COUNT INT = 0

		SELECT @COUNT=COUNT(*) FROM @TempCommissionRule

		DECLARE @TEMPITEM TABLE
		(
			ID INT,
			UniqueID INT,
			numUserCntID NUMERIC(18,0),
			numOppID NUMERIC(18,0),
			numOppBizDocID NUMERIC(18,0),
			numOppBizDocItemID NUMERIC(18,0),
			numItemCode NUMERIC(18,0),
			numUnitHour FLOAT,
			monTotAmount MONEY,
			monVendorCost MONEY,
			monAvgCost MONEY
		)

		WHILE @i <= @COUNT
		BEGIN
			DECLARE @numComRuleID NUMERIC(18,0)
			DECLARE @tintComBasedOn TINYINT
			DECLARE @tintComAppliesTo TINYINT
			DECLARE @tintComOrgType TINYINT
			DECLARE @tintComType TINYINT
			DECLARE @tintAssignTo TINYINT
			DECLARE @numTotalAmount AS MONEY
			DECLARE @numTotalUnit AS FLOAT

			--CLEAR PREVIOUS VALUES FROM TEMP TABLE
			DELETE FROM @TEMPITEM

			--FETCH COMMISSION RULE
			SELECT @numComRuleID=numComRuleID,@tintComBasedOn=tintComBasedOn,@tintComType=tintComType,@tintComAppliesTo=tintComAppliesTo,@tintComOrgType=tintComOrgType,@tintAssignTo=tintAssignTo FROM @TempCommissionRule WHERE ID=@i

			--FIND ALL BIZ DOC ITEMS WHERE COMMISSION IS NOT PAID AND CURRENT RULE ITEMS, ORGANIZATION AND ASSINGED TO FILTER CAN BE APPLIED
			INSERT INTO
				@TEMPITEM
			SELECT
				ROW_NUMBER() OVER (ORDER BY ID),
				ID,
				(CASE @tintAssignTo WHEN 1 THEN numAssignedTo WHEN 2 THEN numRecordOwner ELSE numPartner END),
				numOppID,
				numOppBizDocID,
				numOppBizDocItemID,
				numItemCode,
				numUnitHour,
				monTotAmount,
				monVendorCost,
				monAvgCost
			FROM
				@TABLEPAID
			WHERE
				1 = (CASE @tintComAppliesTo
						--SPECIFIC ITEMS
						WHEN 1 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleItems WHERE numComRuleID = @numComRuleID AND ISNULL(numValue,0) = numItemCode) > 0 THEN 1 ELSE 0 END
						-- ITEM WITH SPECIFIC CLASSIFICATIONS
						WHEN 2 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleItems WHERE numComRuleID = @numComRuleID AND ISNULL(numValue,0) = numItemClassification) > 0 THEN 1 ELSE 0 END
						-- ALL ITEMS
						ELSE 1
					END)
				AND 1 = (CASE @tintComOrgType
							-- SPECIFIC ORGANIZATION
							WHEN 1 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleOrganization WHERE numComRuleID = @numComRuleID AND ISNULL(numValue,0) = numDivisionID) > 0 THEN 1 ELSE 0 END
							-- ORGANIZATION WITH SPECIFIC RELATIONSHIP AND PROFILE
							WHEN 2 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleOrganization WHERE numComRuleID = @numComRuleID AND ISNULL(numValue,0) = numRelationship AND ISNULL(numProfile,0) = numProfile) > 0 THEN 1 ELSE 0 END
							-- ALL ORGANIZATIONS
							ELSE 1
						END)
				AND 1 = (CASE @tintAssignTo 
							-- ORDER ASSIGNED TO
							WHEN 1 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleContacts WHERE numComRuleID = @numComRuleID AND numValue=numAssignedTo) > 0 THEN 1 ELSE 0 END
							-- ORDER OWNER
							WHEN 2 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleContacts WHERE numComRuleID = @numComRuleID AND numValue=numRecordOwner) > 0 THEN 1 ELSE 0 END
							-- ORDER OWNER
							WHEN 3 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleContacts WHERE numComRuleID = @numComRuleID AND numValue=numPartner) > 0 THEN 1 ELSE 0 END
							-- NO OPTION SELECTED IN RULE
							ELSE 0
						END)

			--CALCULATE COMMISSION AND STORE IN TEMPARORY TABLE
			INSERT INTO @TempBizCommission 
			(
				numUserCntID,
				numOppID,
				numOppBizDocID,
				numOppBizDocItemID,
				monCommission,
				numComRuleID,
				tintComType,
				tintComBasedOn,
				decCommission,
				tintAssignTo
			)
			SELECT
				numUserCntID,
				numOppID,
				numOppBizDocID,
				numOppBizDocItemID,
				CASE @tintComType 
					WHEN 1 --PERCENT
						THEN 
							CASE @tintCommissionType
								--TOTAL AMOUNT PAID
								WHEN 1 THEN monTotAmount * (T2.decCommission / CAST(100 AS FLOAT))
								--ITEM GROSS PROFIT (VENDOR COST)
								WHEN 2 THEN (ISNULL(monTotAmount,0) - ISNULL(monVendorCost,0)) * (T2.decCommission / CAST(100 AS FLOAT))
								--ITEM GROSS PROFIT (AVERAGE COST)
								WHEN 3 THEN(ISNULL(monTotAmount,0) - ISNULL(monAvgCost,0)) * (T2.decCommission / CAST(100 AS FLOAT))
							END
					ELSE  --FLAT
						T2.decCommission
				END,
				@numComRuleID,
				@tintComType,
				@tintComBasedOn,
				T2.decCommission,
				@tintAssignTo
			FROM 
				@TEMPITEM AS T1
			CROSS APPLY
			(
				SELECT TOP 1 
					ISNULL(decCommission,0) AS decCommission
				FROM 
					CommissionRuleDtl 
				WHERE 
					numComRuleID=@numComRuleID 
					AND ISNULL(decCommission,0) > 0
					AND 1 = (CASE 
							WHEN @tintComBasedOn = 1 --BASED ON AMOUNT SOLD 
							THEN (CASE WHEN (T1.monTotAmount BETWEEN intFrom AND intTo) THEN 1 ELSE 0 END)
							WHEN @tintComBasedOn = 2 --BASED ON UNITS SOLD
							THEN (CASE WHEN (T1.numUnitHour BETWEEN intFrom AND intTo) THEN 1 ELSE 0 END)
							ELSE 0
							END)
			) AS T2
			WHERE
				1 = (CASE @tintCommissionType 
						--TOTAL PAID INVOICE
						WHEN 1 THEN CASE WHEN (ISNULL(monTotAmount,0) > 0) THEN 1 ELSE 0 END 
						-- ITEM GROSS PROFIT (VENDOR COST)
						WHEN 2 THEN CASE WHEN ((ISNULL(monTotAmount,0) - ISNULL(monVendorCost,0)) > 0) THEN 1 ELSE 0 END 
						-- ITEM GROSS PROFIT (AVERAGE COST)
						WHEN 3 THEN CASE WHEN ((ISNULL(monTotAmount,0) - ISNULL(monAvgCost,0)) > 0) THEN 1 ELSE 0 END
						ELSE
							0 
					END) 
				AND (
						SELECT 
							COUNT(*) 
						FROM 
							@TempBizCommission 
						WHERE 
							numUserCntID=T1.numUserCntID 
							AND numOppID=T1.numOppID 
							AND numOppBizDocID = T1.numOppBizDocID 
							AND numOppBizDocItemID = T1.numOppBizDocItemID
							AND tintAssignTo = @tintAssignTo
					) = 0

			SET @i = @i + 1

			SET @numComRuleID = 0
			SET @tintComBasedOn = 0
			SET @tintComAppliesTo = 0
			SET @tintComOrgType = 0
			SET @tintComType = 0
			SET @tintAssignTo = 0
			SET @numTotalAmount = 0
			SET @numTotalUnit = 0
		END

		--INSERT CALCULATED ENTERIES IN BIZDOCCOMMISSION TABLE
		INSERT INTO BizDocComission
		(	
			numDomainID,
			numUserCntID,
			numOppBizDocId,
			numComissionAmount,
			numOppBizDocItemID,
			numComRuleID,
			tintComType,
			tintComBasedOn,
			decCommission,
			numOppID,
			bitFullPaidBiz,
			numComPayPeriodID,
			tintAssignTo
		)
		SELECT
			@numDomainID,
			numUserCntID,
			numOppBizDocID,
			monCommission,
			numOppBizDocItemID,
			numComRuleID,
			tintComType,
			tintComBasedOn,
			decCommission,
			numOppID,
			0,
			@numComPayPeriodID,
			tintAssignTo
		FROM
			@TempBizCommission
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
END

GO

/****** Object:  StoredProcedure [dbo].[USP_cflManage]    Script Date: 07/26/2008 16:15:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Anoop Jayaraj          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_cflmanage')
DROP PROCEDURE usp_cflmanage
GO
CREATE PROCEDURE [dbo].[USP_cflManage]          
@locid as tinyint=0,          
@fldtype as varchar (30)='',          
@fldlbl as varchar(50)='',          
@FieldOdr as tinyint=0,          
@userId as numeric(9)=0,          
@TabId as numeric(9)=0,          
@fldId as numeric(9) OUTPUT,      
@numDomainID as numeric(9)=0,  
@vcURL as varchar(1000)='',
@vcToolTip AS VARCHAR(1000)='',         
@ListId as numeric(9) OUTPUT,
@bitAutocomplete AS BIT
AS
BEGIN          
       
if @fldId =0          
begin          
  --declare @ListId as numeric(9)          
            
            
  if @fldtype='SelectBox' OR @fldtype='CheckBoxList'          
  begin          
  INSERT INTO ListMaster           
   (          
   vcListName,           
   numCreatedBy,           
   bintCreatedDate,           
   bitDeleted,           
   bitFixed ,    
   numModifiedBy,    
   bintModifiedDate,    
   numDomainID,    
   bitFlag,numModuleId         
   )           
   values          
   (          
   @fldlbl,          
   1,          
   getutcdate(),          
   0,          
   0,    
   1,    
   getutcdate(),     
   @numDomainID,    
   0,8        
   )          
            
  SET @ListId = SCOPE_IDENTITY()
         
  end          
            
  Insert into cfw_fld_master          
   (          
   Fld_type,          
   Fld_label,          
   Fld_tab_ord,          
   Grp_id,          
   subgrp,          
   numlistid,      
   numDomainID,  
	   vcURL,vcToolTip,bitAutocomplete       
   )           
   values          
   (          
   @fldtype,          
   @fldlbl,          
   @FieldOdr,          
   @locid,          
   @TabId,          
   @ListId,      
   @numDomainID,  
   @vcURL,@vcToolTip, @bitAutoComplete          
   )       
   
  SET @fldId = SCOPE_IDENTITY() --(SELECT MAX(fld_id) FROM dbo.CFW_Fld_Master WHERE numDomainID = @numDomainID)
   
end          
          
else           
begin          
 if @fldtype='SelectBox' OR @fldtype='CheckBoxList'          
  begin         
        
    declare @ddlName as varchar(100)        
  select @ddlName=Fld_label from cfw_fld_master where Fld_id=@fldId  and numDomainID =@numDomainID     
         
  update  ListMaster set  vcListName=@fldlbl where vcListName=@ddlName  and    numDomainID =@numDomainID     
        
  end        
 update cfw_fld_master set           
          
  Fld_label=@fldlbl,          
  Grp_id=@locid,          
  subgrp=@TabId,  
  vcURL= @vcURL,vcToolTip=@vcToolTip, bitAutoComplete = @bitAutoComplete        
 where Fld_id=@fldId and   numDomainID =@numDomainID         
            
          
END
END
GO
/****** Object:  StoredProcedure [dbo].[USP_cfwGetFields]    Script Date: 07/26/2008 16:15:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_cfwgetfields')
DROP PROCEDURE usp_cfwgetfields
GO
CREATE PROCEDURE [dbo].[USP_cfwGetFields]                              
@PageId as tinyint,                              
@numRelation as numeric(9),                              
@numRecordId as numeric(9),                  
@numDomainID as numeric(9)                              
as            

                  
if @PageId= 1 
begin                              
select CFW_Fld_Master.fld_id,fld_type,fld_label,numlistid,numOrder,Rec.FldDTLID,isnull(Rec.Fld_Value,0) as Value,subgrp as TabId,Grp_Name as tabname,vcURL,CFW_Fld_Master.vcToolTip from CFW_Fld_Master join CFW_Fld_Dtl                              
on Fld_id=numFieldId                              
left join CFw_Grp_Master                               
on subgrp=CFw_Grp_Master.Grp_id 
left join dbo.GetCustomFieldDTLIDAndValues(@PageId,@numRecordId) Rec
on Rec.Fld_ID=CFW_Fld_Master.fld_id                             
where CFW_Fld_Master.grp_id=@PageId and numRelation=@numRelation and CFW_Fld_Master.numDomainID=@numDomainID                          
         
union        
select 0 as fld_id,'Frame' as fld_type,'' as fld_label,0 as numlistid,0,0,'' as Value,Grp_id as TabId,Grp_Name as tabname,vcURLF,Null as vcToolTip from CFw_Grp_Master        
where tintType=1 and loc_Id = @PageId   and numDomainID=@numDomainID           
order by subgrp,numOrder                             
end                              

---select Shared Custom fields and tab specific customfields -- where Pageid=1 stands for shared custom fields among accounts,leads,prospects
IF  @PageId= 12 or  @PageId= 13 or  @PageId= 14
BEGIN
	select CFW_Fld_Master.fld_id,fld_type,fld_label,numlistid,numOrder,Rec.FldDTLID,isnull(Rec.Fld_Value,0) as Value,subgrp as TabId,Grp_Name as tabname,vcURL,CFW_Fld_Master.vcToolTip from CFW_Fld_Master join CFW_Fld_Dtl                              
	on Fld_id=numFieldId                              
	left join CFw_Grp_Master                               
	on subgrp=CFw_Grp_Master.Grp_id 
	left join dbo.GetCustomFieldDTLIDAndValues(@PageId,@numRecordId) Rec
	on Rec.Fld_ID=CFW_Fld_Master.fld_id                             
	where CFW_Fld_Master.grp_id=@PageId and numRelation=@numRelation and CFW_Fld_Master.numDomainID=@numDomainID                          
	
	UNION 
	
	select CFW_Fld_Master.fld_id,fld_type,fld_label,numlistid,numOrder,Rec.FldDTLID,isnull(Rec.Fld_Value,0) as Value,subgrp as TabId,Grp_Name as tabname,vcURL,CFW_Fld_Master.vcToolTip from CFW_Fld_Master join CFW_Fld_Dtl                              
	on Fld_id=numFieldId                              
	left join CFw_Grp_Master                               
	on subgrp=CFw_Grp_Master.Grp_id 
	left join dbo.GetCustomFieldDTLIDAndValues(1,@numRecordId) Rec
	on Rec.Fld_ID=CFW_Fld_Master.fld_id                             
	where CFW_Fld_Master.grp_id=1 and numRelation=@numRelation and CFW_Fld_Master.numDomainID=@numDomainID
	         
	union        
	select 0 as fld_id,'Frame' as fld_type,'' as fld_label,0 as numlistid,0,0,'' as Value,Grp_id as TabId,Grp_Name as tabname,vcURLF,NUll as vcToolTip from CFw_Grp_Master        
	where tintType=1 and loc_Id = @PageId   and numDomainID=@numDomainID           
	order by subgrp,numOrder                             
	
END


                              
if @PageId= 4                              
begin       
---------------------Very Important----------------------    
-------Who wrote this part    
---------------------------------------------------------    
    
    
--declare @ContactId as numeric(9)                              
---select @ContactId=numContactId from cases where numCaseid=@numRecordId                              
--select @numRelation=numCompanyType from AdditionalContactsInformation AddC                              
---join DivisionMaster Div on AddC.numDivisionId=Div.numDivisionId                              
---join CompanyInfo Com on  Com.numCompanyId=Div.numCompanyId                              
---where AddC.numContactID=@ContactId                              
                              
select CFW_Fld_Master.fld_id,fld_type,fld_label,numlistid,numOrder,
Rec.FldDTLID,isnull(Rec.Fld_Value,0) as Value,
subgrp as TabId,Grp_Name as tabname,vcURL,CFW_Fld_Master.vcToolTip from CFW_Fld_Master join CFW_Fld_Dtl                              
on Fld_id=numFieldId                              
left join CFw_Grp_Master                               
on subgrp=CFw_Grp_Master.Grp_id  
left join dbo.GetCustomFieldDTLIDAndValues(@PageId,@numRecordId) Rec
on Rec.Fld_ID=CFW_Fld_Master.fld_id                             
where CFW_Fld_Master.grp_id=@PageId and numRelation=@numRelation and CFW_Fld_Master.numDomainID=@numDomainID                             
--select @numRelation=numContactType from AdditionalContactsInformation AddC                              
--join DivisionMaster Div on AddC.numDivisionId=Div.numDivisionId                              
--join CompanyInfo Com on  Com.numCompanyId=Div.numCompanyId                              
--where AddC.numContactID=@numRecordId                              
                              
union        
select 0 as fld_id,'Frame' as fld_type,'' as fld_label,0 as numlistid,0,0,'' as Value,
Grp_id as TabId,Grp_Name as tabname,vcURLF,NUll as vcToolTip from CFw_Grp_Master        
where tintType=1 and loc_Id = @PageId   and numDomainID=@numDomainID                              
   order by subgrp,numOrder                           
end                              
                              
if @PageId= 2 or  @PageId= 3 or @PageId= 5 or @PageId= 6 or @PageId= 7 or @PageId= 8     or @PageId= 11   or @PageId = 17                
begin                              
select CFW_Fld_Master.fld_id,fld_type,fld_label,numlistid,ISNULL(Rec.FldDTLID,0) FldDTLID,
CASE WHEN @PageId = 5 THEN (CASE WHEN ISNULL(Rec.Fld_Value, '') = '' AND fld_type = 'Checkbox' THEN '0' ELSE ISNULL(Rec.Fld_Value, 0) END) ELSE ISNULL(Rec.Fld_Value,0) END  AS Value,
subgrp as TabId,Grp_Name as tabname,vcURL,CFW_Fld_Master.vcToolTip  from CFW_Fld_Master                               
left join CFw_Grp_Master                               
on subgrp=CFw_Grp_Master.Grp_id  
left join dbo.GetCustomFieldDTLIDAndValues(@PageId,@numRecordId) Rec
on Rec.Fld_ID=CFW_Fld_Master.fld_id                             
where CFW_Fld_Master.grp_id=@PageId and CFW_Fld_Master.numDomainID=@numDomainID         
union        
select 0 as fld_id,'Frame' as fld_type,'' as fld_label,0 as numlistid,0 as FldDTLID,'0' as Value,Grp_id as TabId,Grp_Name as tabname,vcURLF,NULL as vcToolTip from CFw_Grp_Master        
where tintType=1 and loc_Id = @PageId  and numDomainID=@numDomainID      
end

select GRp_Id as TabID,Grp_Name AS TabName from CFw_Grp_Master where 
(Loc_ID=@PageId OR 1=(CASE WHEN @PageId IN (12,13,14) THEN CASE WHEN Loc_Id=1 THEN 1 ELSE 0 END ELSE 0 END))
 and numDomainID=@numDomainID
/*tintType stands for static tabs */ AND tintType<>2
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_cfwgetfieldsoppitems')
DROP PROCEDURE dbo.usp_cfwgetfieldsoppitems
GO
CREATE PROCEDURE [dbo].[usp_cfwgetfieldsoppitems]
 @numDomainID as numeric(18,0),                       
	@numOppItemId as numeric(18,0),                  
    @numItemCode AS NUMERIC(18,0)                        
AS
BEGIN
	IF EXISTS (SELECT * FROM CFW_Fld_Values_OppItems WHERE RecId=@numOppItemId)
	BEGIN
		SELECT 
			CFW_Fld_Master.fld_id
			,fld_type,fld_label
			,numlistid
			,ISNULL(CFW_Fld_Values_OppItems.FldDTLID,0) FldDTLID
			,(CASE WHEN (ISNULL(CFW_Fld_Values_OppItems.Fld_Value, '') = '' OR UPPER(ISNULL(CFW_Fld_Values_OppItems.Fld_Value, '')) = 'NO' OR UPPER(ISNULL(CFW_Fld_Values_OppItems.Fld_Value, '')) = 'FALSE') AND fld_type = 'Checkbox' THEN '0' ELSE ISNULL(CFW_Fld_Values_OppItems.Fld_Value, 0) END) AS Value
			,subgrp as TabId
			,Grp_Name as tabname
			,vcURL
			,CFW_Fld_Master.vcToolTip  
		FROM 
			CFW_Fld_Master                               
		LEFT JOIN 
			CFw_Grp_Master                               
		ON 
			subgrp=CFw_Grp_Master.Grp_id  
        LEFT JOIN
			CFW_Fld_Values_OppItems
		ON
			CFW_Fld_Master.Fld_id = CFW_Fld_Values_OppItems.Fld_ID
			AND CFW_Fld_Values_OppItems.RecId=@numOppItemId
		WHERE 
			CFW_Fld_Master.grp_id=5 
			and CFW_Fld_Master.numDomainID=@numDomainID 
	END
	ELSE
	BEGIN
		SELECT 
			CFW_Fld_Master.fld_id
			,fld_type,fld_label
			,numlistid
			,ISNULL(CFW_Fld_Values_Item.FldDTLID,0) FldDTLID
			,(CASE WHEN (ISNULL(CFW_Fld_Values_Item.Fld_Value, '') = '' OR UPPER(ISNULL(CFW_Fld_Values_Item.Fld_Value, '')) = 'NO' OR UPPER(ISNULL(CFW_Fld_Values_Item.Fld_Value, '')) = 'FALSE') AND fld_type = 'Checkbox' THEN '0' ELSE ISNULL(CFW_Fld_Values_Item.Fld_Value, 0) END) AS Value
			,subgrp as TabId
			,Grp_Name as tabname
			,vcURL
			,CFW_Fld_Master.vcToolTip  
		FROM 
			CFW_Fld_Master                               
		LEFT JOIN 
			CFw_Grp_Master                               
		ON 
			subgrp=CFw_Grp_Master.Grp_id  
        LEFT JOIN
			CFW_Fld_Values_Item
		ON
			CFW_Fld_Master.Fld_id = CFW_Fld_Values_Item.Fld_ID
			AND CFW_Fld_Values_Item.RecId=@numItemCode
		WHERE 
			CFW_Fld_Master.grp_id=5 
			and CFW_Fld_Master.numDomainID=@numDomainID 
	END
END
/****** Object:  StoredProcedure [dbo].[USP_cfwSaveCusfld]    Script Date: 07/26/2008 16:15:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_cfwsavecusfld')
DROP PROCEDURE usp_cfwsavecusfld
GO
CREATE PROCEDURE [dbo].[USP_cfwSaveCusfld]          
          
@RecordId as numeric(9)=null,          
@strDetails as text='',          
@PageId as tinyint=null          
AS 
BEGIN        
	IF CHARINDEX('<?xml',@strDetails) = 0
	BEGIN
		SET @strDetails = CONCAT('<?xml version="1.0" encoding="ISO-8859-1"?>',@strDetails)
	END

 
declare @hDoc int          
          
        
          
EXEC sp_xml_preparedocument @hDoc OUTPUT, @strDetails          
          
if @PageId=1          
begin          

Delete from CFW_FLD_Values where FldDTLID not in (SELECT FldDTLID FROM OPENXML (@hDoc,'/NewDataSet/Table[FldDTLID>0]',2)          
WITH (fld_id numeric(9),FldDTLID numeric(9),Value varchar(1000))) 
and  RecId= @RecordId      


insert into CFW_FLD_Values (Fld_ID,Fld_Value,RecId)          
select X.fld_id,X.Value,@RecordId  from(SELECT *FROM OPENXML (@hDoc,'/NewDataSet/Table[FldDTLID=0]',2)          
WITH (fld_id numeric(9),FldDTLID numeric(9),Value varchar(1000)))X 


update CFW_FLD_Values set Fld_Value=X.Value  from      
(SELECT * FROM OPENXML (@hDoc,'/NewDataSet/Table[FldDTLID>0]',2)          
WITH (fld_id numeric(9),FldDTLID numeric(9),Value varchar(1000)))X
where CFW_FLD_Values.FldDTLID=X.FldDTLID


--Author :Sachin Sadhu ||Date:13thOctober2014
--Purpose:To make Entry in WorkFLow queue(Identify rules based on Custom fields)

DECLARE @CFFields as table (  
  RowID INT IDENTITY(1, 1),
  fld_id numeric(9)
  )
INSERT INTO @CFFields(fld_id) 
SELECT Fld_ID from CFW_FLD_Values where RecId=@RecordId
--select X.fld_id  from(SELECT *FROM OPENXML (@hDoc,'/NewDataSet/Table[FldDTLID>0]',2)          
--WITH (fld_id numeric(9),FldDTLID numeric(9),Value varchar(1000)))X 

DECLARE @intCnt AS INT
DECLARE @intCntFields AS INT
Declare @numFieldID As numeric(9,0)
Declare @numDomainID as numeric(18,0)
Declare @numUserID as numeric(18,0)
SET @intCnt = 0
SET @intCntFields = (SELECT COUNT(*) FROM @CFFields)
SELECT @numDomainID=numDomainID,@numUserID=numCreatedBy from DivisionMaster where numDivisionID=@RecordId

IF @intCntFields > 0
	BEGIN
		WHILE(@intCnt < @intCntFields)
			BEGIN
			--print 'sahc'
				SET @intCnt = @intCnt + 1
				SELECT @numFieldID = fld_id FROM @CFFields WHERE RowID = @intCnt
				 --Added By :Sachin Sadhu ||Date:13thOct2014
				 --Purpose  :To manage Custom Fields Queue
							EXEC USP_CFW_FLD_Values_CT
									@numDomainID =@numDomainID,
									@numUserCntID =0,
									@numRecordID =@RecordId ,
									@numFormFieldId  =@numFieldID
			END 

	END
End       
-- action item details, hard coded flag to control delete behaviour of custom field bug id 635
if @PageId=128 
begin          

insert into CFW_FLD_Values (Fld_ID,Fld_Value,RecId)          
select X.fld_id,X.Value,@RecordId  from(SELECT *FROM OPENXML (@hDoc,'/NewDataSet/Table[FldDTLID=0]',2)          
WITH (fld_id numeric(9),FldDTLID numeric(9),Value varchar(1000)))X 


update CFW_FLD_Values set Fld_Value=X.Value  from      
(SELECT * FROM OPENXML (@hDoc,'/NewDataSet/Table[FldDTLID>0]',2)          
WITH (fld_id numeric(9),FldDTLID numeric(9),Value varchar(1000)))X
where CFW_FLD_Values.FldDTLID=X.FldDTLID

End        
 
          
if @PageId=4          
begin  

Delete from CFW_FLD_Values_Cont where FldDTLID not in (SELECT FldDTLID FROM OPENXML (@hDoc,'/NewDataSet/Table[FldDTLID>0]',2)          
WITH (fld_id numeric(9),FldDTLID numeric(9),Value varchar(1000))) 
and  RecId= @RecordId      


insert into CFW_FLD_Values_Cont (Fld_ID,Fld_Value,RecId)          
select X.fld_id,X.Value,@RecordId  from(SELECT *FROM OPENXML (@hDoc,'/NewDataSet/Table[FldDTLID=0]',2)          
WITH (fld_id numeric(9),FldDTLID numeric(9),Value varchar(1000)))X 


update CFW_FLD_Values_Cont set Fld_Value=X.Value  from      
(SELECT * FROM OPENXML (@hDoc,'/NewDataSet/Table[FldDTLID>0]',2)          
WITH (fld_id numeric(9),FldDTLID numeric(9),Value varchar(1000)))X
where CFW_FLD_Values_Cont.FldDTLID=X.FldDTLID

--        
--Delete from CFW_FLD_Values_Cont where RecId=@RecordId          
--insert into CFW_FLD_Values_Cont (Fld_ID,Fld_Value,RecId)          
--          
--select X.fld_id,X.Value,@RecordId  from(SELECT *FROM OPENXML (@hDoc,'/NewDataSet/Table[fld_id>0]',2)          
-- WITH (fld_id numeric(9),Value varchar(1000)))X          
    
	
--Author :Sachin Sadhu ||Date:13thOctober2014
--Purpose:To make Entry in WorkFLow queue(Identify rules based on Custom fields)

DECLARE @CFFieldsCon as table (  
  RowID INT IDENTITY(1, 1),
  fld_id numeric(9)
  )
INSERT INTO @CFFieldsCon(fld_id) 
SELECT Fld_ID from CFW_FLD_Values_Cont where RecId=@RecordId
--select X.fld_id  from(SELECT *FROM OPENXML (@hDoc,'/NewDataSet/Table[FldDTLID>0]',2)          
--WITH (fld_id numeric(9),FldDTLID numeric(9),Value varchar(1000)))X 

DECLARE @intCntCon AS INT
DECLARE @intCntFieldsCon AS INT
Declare @numFieldIDCon As numeric(9,0)
Declare @numDomainIDCon as numeric(18,0)
Declare @numUserIDCon as numeric(18,0)
SET @intCntCon = 0
SET @intCntFieldsCon = (SELECT COUNT(*) FROM @CFFieldsCon)
SELECT @numDomainIDCon=numDomainID,@numUserIDCon=numCreatedBy from AdditionalContactsInformation where numContactId=@RecordId

IF @intCntFieldsCon > 0
	BEGIN
		WHILE(@intCntCon < @intCntFieldsCon)
			BEGIN
			
				SET @intCntCon = @intCntCon + 1
				SELECT @numFieldIDCon = fld_id FROM @CFFieldsCon WHERE RowID = @intCntCon
				 --Added By :Sachin Sadhu ||Date:14thOct2014
				 --Purpose  :To manage Custom Fields Queue
							EXEC USP_CFW_FLD_Values_Cont_CT
									@numDomainID =@numDomainIDCon,
									@numUserCntID =@numUserIDCon,
									@numRecordID =@RecordId ,
									@numFormFieldId  =@numFieldIDCon
			END 

	END  
	   
end          
          
if @PageId=3          
BEGIN 
	DECLARE @TEMP TABLE
	(
		fld_id numeric(9),
		FldDTLID numeric(9),
		Value varchar(1000)
	)

	INSERT INTO @TEMP 
	(
		fld_id,
		FldDTLID,
		Value
	)
	SELECT
		fld_id,
		FldDTLID,
		Value
	FROM OPENXML 
		(@hDoc,'/NewDataSet/Table',2)          
	WITH 
		(fld_id NUMERIC(18,0),FldDTLID NUMERIC(18,0),Value VARCHAR(1000)) 

  
	DELETE FROM 
		CFW_FLD_Values_Case 
	WHERE 
		FldDTLID NOT IN (SELECT FldDTLID FROM @TEMP WHERE FldDTLID > 0) 
		AND RecId= @RecordId      
		AND Fld_ID NOT IN (SELECT ISNULL(numChildFieldID,0) FROM ParentChildCustomFieldMap WHERE tintChildModule=3)  


	INSERT INTO CFW_FLD_Values_Case 
	(
		Fld_ID,
		Fld_Value,
		RecId
	)          
	SELECT 
		fld_id,
		Value,
		@RecordId  
	FROM
		@TEMP
	WHERE
		FldDTLID=0
		AND fld_id NOT IN (SELECT ISNULL(numChildFieldID,0) FROM ParentChildCustomFieldMap WHERE tintChildModule=3)


	UPDATE 
		CFW_FLD_Values_Case 
	SET 
		Fld_Value = X.Value  
	FROM
	   @TEMP X
	WHERE
	   X.FldDTLID > 0
	   AND CFW_FLD_Values_Case.FldDTLID=X.FldDTLID
	   AND  CFW_FLD_Values_Case.Fld_ID NOT IN (SELECT ISNULL(numChildFieldID,0) FROM ParentChildCustomFieldMap WHERE tintChildModule=3)           
          
--Delete from CFW_FLD_Values_Case where RecId=@RecordId          
--insert into CFW_FLD_Values_Case (Fld_ID,Fld_Value,RecId)          
--          
--select X.fld_id,X.Value,@RecordId  from(SELECT *FROM OPENXML (@hDoc,'/NewDataSet/Table[fld_id>0]',2)          
-- WITH (fld_id numeric(9),Value varchar(1000)))X          
     
	 --Author :Sachin Sadhu ||Date:13thOctober2014
--Purpose:To make Entry in WorkFLow queue(Identify rules based on Custom fields)

DECLARE @CFFieldsCase as table (  
  RowID INT IDENTITY(1, 1),
  fld_id numeric(9)
  )
INSERT INTO @CFFieldsCase(fld_id) 
SELECT Fld_ID from CFW_FLD_Values_Case where RecId=@RecordId
--select X.fld_id  from(SELECT *FROM OPENXML (@hDoc,'/NewDataSet/Table[FldDTLID>0]',2)          
--WITH (fld_id numeric(9),FldDTLID numeric(9),Value varchar(1000)))X 

DECLARE @intCntCase AS INT
DECLARE @intCntFieldsCase AS INT
Declare @numFieldIDCase As numeric(9,0)
Declare @numDomainIDCase as numeric(18,0)
Declare @numUserIDCase as numeric(18,0)
SET @intCntCase = 0
SET @intCntFieldsCase = (SELECT COUNT(*) FROM @CFFieldsCase)
SELECT @numDomainIDCase=numDomainID,@numUserIDCase=numCreatedBy from Cases where numCaseId=@RecordId

IF @intCntFieldsCase > 0
	BEGIN
		WHILE(@intCntCase < @intCntFieldsCase)
			BEGIN
			--print 'sahc'
				SET @intCntCase = @intCntCase + 1
				SELECT @numFieldIDCase = fld_id FROM @CFFieldsCase WHERE RowID = @intCntCase
				 --Added By :Sachin Sadhu ||Date:13thOct2014
				 --Purpose  :To manage Custom Fields Queue
							EXEC USP_CFW_FLD_Values_Case_CT
									@numDomainID =@numDomainIDCase,
									@numUserCntID =@numUserIDCase,
									@numRecordID =@RecordId ,
									@numFormFieldId  =@numFieldIDCase
			END 

	END
	      
end          
        
if @PageId=5        
begin          


Delete from CFW_FLD_Values_Item where FldDTLID not in (SELECT FldDTLID FROM OPENXML (@hDoc,'/NewDataSet/Table[FldDTLID>0]',2)          
WITH (fld_id numeric(9),FldDTLID numeric(9),Value varchar(1000))) 
and  RecId= @RecordId      


insert into CFW_FLD_Values_Item (Fld_ID,Fld_Value,RecId)          
select X.fld_id,X.Value,@RecordId  from(SELECT *FROM OPENXML (@hDoc,'/NewDataSet/Table[FldDTLID=0]',2)          
WITH (fld_id numeric(9),FldDTLID numeric(9),Value varchar(1000)))X 


update CFW_FLD_Values_Item set Fld_Value=X.Value  from      
(SELECT * FROM OPENXML (@hDoc,'/NewDataSet/Table[FldDTLID>0]',2)          
WITH (fld_id numeric(9),FldDTLID numeric(9),Value varchar(1000)))X
where CFW_FLD_Values_Item.FldDTLID=X.FldDTLID  


--Delete from CFW_FLD_Values_Item where RecId=@RecordId          
--insert into CFW_FLD_Values_Item (Fld_ID,Fld_Value,RecId)          
--          
--select X.fld_id,X.Value,@RecordId  from(SELECT *FROM OPENXML (@hDoc,'/NewDataSet/Table[fld_id>0]',2)          
-- WITH (fld_id numeric(9),Value varchar(1000)))X          
          
end         
          
if @PageId=6 or @PageId=2      
begin       

Delete from CFW_Fld_Values_Opp where FldDTLID not in (SELECT FldDTLID FROM OPENXML (@hDoc,'/NewDataSet/Table[FldDTLID>0]',2)          
WITH (fld_id numeric(9),FldDTLID numeric(9),Value varchar(1000))) 
and  RecId= @RecordId      


insert into CFW_Fld_Values_Opp (Fld_ID,Fld_Value,RecId)          
select X.fld_id,X.Value,@RecordId  from(SELECT *FROM OPENXML (@hDoc,'/NewDataSet/Table[FldDTLID=0]',2)          
WITH (fld_id numeric(9),FldDTLID numeric(9),Value varchar(1000)))X 


update CFW_Fld_Values_Opp set Fld_Value=X.Value  from      
(SELECT * FROM OPENXML (@hDoc,'/NewDataSet/Table[FldDTLID>0]',2)          
WITH (fld_id numeric(9),FldDTLID numeric(9),Value varchar(1000)))X
where CFW_Fld_Values_Opp.FldDTLID=X.FldDTLID 
   
--Delete from CFW_Fld_Values_Opp where RecId=@RecordId          
--insert into CFW_Fld_Values_Opp (Fld_ID,Fld_Value,RecId)          
--          
--select X.fld_id,X.Value,@RecordId  from(SELECT *FROM OPENXML (@hDoc,'/NewDataSet/Table[fld_id>0]',2)          
-- WITH (fld_id numeric(9),Value varchar(1000)))X     
  
--Author :Sachin Sadhu ||Date:13thOctober2014
--Purpose:To make Entry in WorkFLow queue(Identify rules based on Custom fields)

DECLARE @CFFieldsOpp as table (  
  RowID INT IDENTITY(1, 1),
  fld_id numeric(9)
  )
INSERT INTO @CFFieldsOpp(fld_id) 
SELECT Fld_ID from CFW_Fld_Values_Opp where RecId=@RecordId
--select X.fld_id  from(SELECT *FROM OPENXML (@hDoc,'/NewDataSet/Table[FldDTLID>0]',2)          
--WITH (fld_id numeric(9),FldDTLID numeric(9),Value varchar(1000)))X 

DECLARE @intCntOpp AS INT
DECLARE @intCntFieldsOpp AS INT
Declare @numFieldIDOpp As numeric(9,0)
Declare @numDomainIDOpp as numeric(18,0)
Declare @numUserIDOpp as numeric(18,0)
SET @intCntOpp = 0
SET @intCntFieldsOpp = (SELECT COUNT(*) FROM @CFFieldsOpp)
SELECT @numDomainIDOpp=numDomainID,@numUserIDOpp=numCreatedBy from OpportunityMaster where numOppId=@RecordId

IF @intCntFieldsOpp > 0
	BEGIN
		WHILE(@intCntOpp < @intCntFieldsOpp)
			BEGIN
			--print 'sahc'
				SET @intCntOpp = @intCntOpp + 1
				SELECT @numFieldIDOpp = fld_id FROM @CFFieldsOpp WHERE RowID = @intCntOpp
				 --Added By :Sachin Sadhu ||Date:14thOct2014
				 --Purpose  :To manage Custom Fields Queue
							EXEC USP_CFW_Fld_Values_Opp_CT
									@numDomainID =@numDomainIDOpp,
									@numUserCntID =@numUserIDOpp,
									@numRecordID =@RecordId ,
									@numFormFieldId  =@numFieldIDOpp
			END 

	END

   
          
end      
    
if @PageId=7 or    @PageId=8    
begin

Delete from CFW_Fld_Values_Product where FldDTLID not in (SELECT FldDTLID FROM OPENXML (@hDoc,'/NewDataSet/Table[FldDTLID>0]',2)          
WITH (fld_id numeric(9),FldDTLID numeric(9),Value varchar(1000))) 
and  RecId= @RecordId      


insert into CFW_Fld_Values_Product (Fld_ID,Fld_Value,RecId)          
select X.fld_id,X.Value,@RecordId  from(SELECT *FROM OPENXML (@hDoc,'/NewDataSet/Table[FldDTLID=0]',2)          
WITH (fld_id numeric(9),FldDTLID numeric(9),Value varchar(1000)))X 


update CFW_Fld_Values_Product set Fld_Value=X.Value  from      
(SELECT * FROM OPENXML (@hDoc,'/NewDataSet/Table[FldDTLID>0]',2)          
WITH (fld_id numeric(9),FldDTLID numeric(9),Value varchar(1000)))X
where CFW_Fld_Values_Product.FldDTLID=X.FldDTLID
          
--Delete from CFW_Fld_Values_Product where RecId=@RecordId          
--insert into CFW_Fld_Values_Product (Fld_ID,Fld_Value,RecId)          
--          
--select X.fld_id,X.Value,@RecordId  from(SELECT *FROM OPENXML (@hDoc,'/NewDataSet/Table[fld_id>0]',2)          
-- WITH (fld_id numeric(9),Value varchar(1000)))X          
          
end     
          
if @PageId=11  
begin   

Delete from CFW_Fld_Values_Pro where FldDTLID not in (SELECT FldDTLID FROM OPENXML (@hDoc,'/NewDataSet/Table[FldDTLID>0]',2)          
WITH (fld_id numeric(9),FldDTLID numeric(9),Value varchar(1000))) 
and  RecId= @RecordId      


insert into CFW_Fld_Values_Pro (Fld_ID,Fld_Value,RecId)          
select X.fld_id,X.Value,@RecordId  from(SELECT *FROM OPENXML (@hDoc,'/NewDataSet/Table[FldDTLID=0]',2)          
WITH (fld_id numeric(9),FldDTLID numeric(9),Value varchar(1000)))X 


update CFW_Fld_Values_Pro set Fld_Value=X.Value  from      
(SELECT * FROM OPENXML (@hDoc,'/NewDataSet/Table[FldDTLID>0]',2)          
WITH (fld_id numeric(9),FldDTLID numeric(9),Value varchar(1000)))X
where CFW_Fld_Values_Pro.FldDTLID=X.FldDTLID
       
--Delete from CFW_Fld_Values_Pro where RecId=@RecordId          
--insert into CFW_Fld_Values_Pro (Fld_ID,Fld_Value,RecId)          
--          
--select X.fld_id,X.Value,@RecordId  from(SELECT *FROM OPENXML (@hDoc,'/NewDataSet/Table[fld_id>0]',2)          
-- WITH (fld_id numeric(9),Value varchar(1000)))X    


--Author :Sachin Sadhu ||Date:13thOctober2014
--Purpose:To make Entry in WorkFLow queue(Identify rules based on Custom fields)

DECLARE @CFFieldsPro as table (  
  RowID INT IDENTITY(1, 1),
  fld_id numeric(9)
  )
INSERT INTO @CFFieldsPro(fld_id) 
SELECT Fld_ID from CFW_FLD_Values_Pro where RecId=@RecordId
--select X.fld_id  from(SELECT *FROM OPENXML (@hDoc,'/NewDataSet/Table[FldDTLID>0]',2)          
--WITH (fld_id numeric(9),FldDTLID numeric(9),Value varchar(1000)))X 

DECLARE @intCntPro AS INT
DECLARE @intCntFieldsPro AS INT
Declare @numFieldIDPro As numeric(9,0)
Declare @numDomainIDPro as numeric(18,0)
Declare @numUserIDPro as numeric(18,0)
SET @intCntPro = 0
SET @intCntFieldsPro = (SELECT COUNT(*) FROM @CFFieldsPro)
SELECT @numDomainIDPro=numDomainID,@numUserIDPro=numCreatedBy from ProjectsMaster where numProId=@RecordId

IF @intCntFieldsPro > 0
	BEGIN
		WHILE(@intCntPro < @intCntFieldsPro)
			BEGIN
			--print 'sahc'
				SET @intCntPro = @intCntPro + 1
				SELECT @numFieldIDPro = fld_id FROM @CFFieldsPro WHERE RowID = @intCntPro
				 --Added By :Sachin Sadhu ||Date:14thOct2014
				 --Purpose  :To manage Custom Fields Queue
							EXEC USP_CFW_FLD_Values_Pro_CT
									@numDomainID =@numDomainIDPro,
									@numUserCntID =@numUserIDPro,
									@numRecordID =@RecordId ,
									@numFormFieldId  =@numFieldIDPro
			END 

	END      
          
end

--Added by Neelam Kapila || 12/01/2017 - Added condition to save custom items to CFW_Fld_Values_OppItems table
IF @PageId = 17        
BEGIN      
	DELETE FROM CFW_Fld_Values_OppItems 
	WHERE FldDTLID NOT IN (SELECT FldDTLID FROM OPENXML (@hDoc,'/NewDataSet/Table[FldDTLID>0]',2)          
	WITH (fld_id NUMERIC(9), FldDTLID NUMERIC(9), Value VARCHAR(1000))) 
	AND RecId = @RecordId    

	INSERT INTO CFW_Fld_Values_OppItems (Fld_ID,Fld_Value,RecId)          
	SELECT X.fld_id, X.Value, @RecordId  
	FROM (SELECT * FROM OPENXML (@hDoc,'/NewDataSet/Table[FldDTLID=0]',2)          
	WITH (fld_id NUMERIC(9), FldDTLID NUMERIC(9), Value VARCHAR(1000)))X 

	UPDATE CFW_Fld_Values_OppItems 
	SET Fld_Value=X.Value  
	FROM (SELECT * FROM OPENXML (@hDoc,'/NewDataSet/Table[FldDTLID>0]',2)          
	WITH (fld_id NUMERIC(9), FldDTLID NUMERIC(9), Value VARCHAR(1000)))X
	WHERE CFW_Fld_Values_OppItems.FldDTLID = X.FldDTLID  
END 
 
       
EXEC sp_xml_removedocument @hDoc
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_CheckFinancialyear')
DROP PROCEDURE USP_CheckFinancialyear
GO
CREATE PROCEDURE [dbo].[USP_CheckFinancialyear]    
	@numDomainID as numeric(9),
	@dtDate as DATETIME
as                            
BEGIN
SELECT COUNT(*) FROM Financialyear WHERE numDomainID=@numDomainID AND bitCloseStatus=0 AND bitCurrentYear=1 AND (CAST(dtPeriodFrom AS DATE) <= CAST(@dtDate AS DATE) AND CAST(dtPeriodTO AS DATE) >= CAST(@dtDate AS DATE))
END	
GO
 
/****** Object:  StoredProcedure [dbo].[USP_OPPCheckCanBeShipped]    Script Date: 07/26/2008 16:20:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON            
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_CheckOrderedAndInvoicedOrBilledQty')
DROP PROCEDURE USP_CheckOrderedAndInvoicedOrBilledQty
GO
CREATE PROCEDURE [dbo].[USP_CheckOrderedAndInvoicedOrBilledQty]              
@numOppID AS NUMERIC(18,0)
AS             
BEGIN
	DECLARE @TMEP TABLE
	(
		ID INT,
		vcDescription VARCHAR(100),
		bitTrue BIT
	)

	INSERT INTO 
		@TMEP
	VALUES
		(1,'Fulfillment bizdoc is added but not yet fulfilled',0),
		(2,'Invoice/Bill and Ordered Qty is not same',0),
		(3,'Ordered & Fulfilled Qty is not same',0),
		(4,'Invoice is not generated against diferred income bizDocs.',0),
		(5,'Qty is not available to add in deferred bizdoc.',0)

	DECLARE @tintOppType AS TINYINT
	DECLARE @tintOppStatus AS TINYINT

	SELECT @tintOppType=tintOppType,@tintOppStatus=tintOppStatus FROM OpportunityMaster WHERe numOppID=@numOppID


	-- CHECK IF FULFILLMENT BIZDOC IS ADDED TO SALES ORDER BUT IT IS NOT FULFILLED FROM SALES FULFILLMENT SCREEN
	IF @tintOppType = 1 AND @tintOppStatus = 1  --SALES ORDER
	BEGIN
		IF (SELECT COUNT(*) FROM OpportunityBizDocs WHERE numOppId=@numOppID AND numBizDocId=296 AND ISNULL(bitFulFilled,0) = 0) > 0
		BEGIN
			UPDATE @TMEP SET bitTrue = 1 WHERE ID = 1
		END

		-- CHECK IF FULFILLMENT BIZDOC QTY OF ITEMS IS NOT SAME AS ORDERED QTY
		IF (SELECT 
				COUNT(*) 
			FROM 
			(
				SELECT
					OI.numoppitemtCode,
					ISNULL(OI.numUnitHour,0) AS OrderedQty,
					ISNULL(TempFulFilled.FulFilledQty,0) AS FulFilledQty
				FROM
					OpportunityItems OI
				INNER JOIN
					Item I
				ON
					OI.numItemCode = I.numItemCode
				OUTER APPLY
				(
					SELECT
						SUM(OpportunityBizDocItems.numUnitHour) AS FulFilledQty
					FROM
						OpportunityBizDocs
					INNER JOIN
						OpportunityBizDocItems 
					ON
						OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
					WHERE
						OpportunityBizDocs.numOppId = @numOppID
						AND ISNULL(OpportunityBizDocs.numBizDocId,0) = 296
						AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
				) AS TempFulFilled
				WHERE
					OI.numOppID = @numOppID
					AND UPPER(I.charItemType) = 'P'
					AND ISNULL(OI.bitDropShip,0) = 0
			) X
			WHERE
				X.OrderedQty <> X.FulFilledQty) > 0
		BEGIN
			UPDATE @TMEP SET bitTrue = 1 WHERE ID = 3
		END

		-- CHECK WHETHER INVOICES ARE GENERATED AGAINST DIFERRED INCOME BIZDOC
		IF (SELECT 
				COUNT(*)
			FROM  
			(
				SELECT 
					OBDI.numOppItemID,
					(ISNULL(SUM(OBDI.numUnitHour),0) - ISNULL(SUM(TEMPInvoiceAgainstDeferred.numUnitHour),0)) AS intQtyRemaining
				FROM
					OpportunityBizDocs OBD
				INNER JOIN
					OpportunityBizDocItems OBDI
				ON
					OBD.numOppBizDocsId = OBDI.numOppBizDocID
				CROSS APPLY
				(
					SELECT 
						SUM(OpportunityBizDocItems.numUnitHour) AS numUnitHour
					FROM
						OpportunityBizDocs
					INNER JOIN
						OpportunityBizDocItems
					ON
						OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
					WHERE
						numOppId=@numOppID
						AND ISNULL(numDeferredBizDocID,0) > 0
						AND OpportunityBizDocItems.numOppItemID = OBDI.numOppItemID

				) AS TEMPInvoiceAgainstDeferred
				WHERE
					numOppId=@numOppID 
					AND numBizDocId=304
				GROUP BY
					OBDI.numOppItemID
			) AS TEMP
			WHERE
				intQtyRemaining > 0) > 0
		BEGIN
			UPDATE @TMEP SET bitTrue = 1 WHERE ID = 4
		END

		-- CHECK WHETHER ITEM QTY LEFT TO ADD TO DIFERRED INCOME BIZDOC
		IF (SELECT 
				COUNT(*) 
			FROM 
			(
				SELECT
					OI.numoppitemtCode,
					ISNULL(OI.numUnitHour,0) AS OrderedQty,
					ISNULL(TempQtyLeftForDifferedIncome.intQty,0) AS intQty
				FROM
					OpportunityItems OI
				INNER JOIN
					Item I
				ON
					OI.numItemCode = I.numItemCode
				OUTER APPLY
				(
					SELECT
						SUM(OpportunityBizDocItems.numUnitHour) AS intQty
					FROM
						OpportunityBizDocs
					INNER JOIN
						OpportunityBizDocItems 
					ON
						OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
					WHERE
						OpportunityBizDocs.numOppId = @numOppID
						AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
						AND (ISNULL(OpportunityBizDocs.bitAuthoritativeBizDocs,0) = 1 OR OpportunityBizDocs.numBizDocID = 304)
				) AS TempQtyLeftForDifferedIncome
				WHERE
					OI.numOppID = @numOppID
			) X
			WHERE
				X.OrderedQty <> X.intQty) = 0
		BEGIN
			UPDATE @TMEP SET bitTrue = 1 WHERE ID = 5
		END
	END

	-- IF INVOICED/BILLED QTY OF ITEMS IS NOT SAME AS ORDERED QTY THEN ORDER CAN NOT BE SHIPPED/RECEIVED
	IF (SELECT 
			COUNT(*) 
		FROM 
		(
			SELECT
				OI.numoppitemtCode,
				ISNULL(OI.numUnitHour,0) AS OrderedQty,
				ISNULL(TempInvoice.InvoicedQty,0) AS InvoicedQty
			FROM
				OpportunityItems OI
			INNER JOIN
				Item I
			ON
				OI.numItemCode = I.numItemCode
			OUTER APPLY
			(
				SELECT
					SUM(OpportunityBizDocItems.numUnitHour) AS InvoicedQty
				FROM
					OpportunityBizDocs
				INNER JOIN
					OpportunityBizDocItems 
				ON
					OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
				WHERE
					OpportunityBizDocs.numOppId = @numOppID
					AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
					AND ISNULL(OpportunityBizDocs.bitAuthoritativeBizDocs,0) = 1
			) AS TempInvoice
			WHERE
				OI.numOppID = @numOppID
		) X
		WHERE
			X.OrderedQty <> X.InvoicedQty) > 0
	BEGIN
		UPDATE @TMEP SET bitTrue = 1 WHERE ID = 2
	END

	SELECT * FROM @TMEP

	--GET FULFILLMENT BIZDOCS WHICH ARE NOT FULFILLED YET
	SELECT numOppBizDocsId FROM OpportunityBizDocs WHERE OpportunityBizDocs.numOppId = @numOppID AND ISNULL(OpportunityBizDocs.numBizDocId,0) = 296 AND ISNULL(OpportunityBizDocs.bitFulFilled,0) = 0
END

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_CommissionPayPeriod_Save')
DROP PROCEDURE USP_CommissionPayPeriod_Save
GO
Create PROCEDURE [dbo].[USP_CommissionPayPeriod_Save]
	-- Add the parameters for the stored procedure here
	 @numComPayPeriodID AS NUMERIC(18,0) OUTPUT,
	 @numDomainID AS NUMERIC(18,0),
	 @dtStart DATE,
	 @dtEnd DATE,
	 @tintPayPeriod INT,
	 @numUserID NUMERIC(18,0),
	 @ClientTimeZoneOffset INT     
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
SET NOCOUNT ON;

BEGIN TRY
BEGIN TRANSACTION
	--NEW PAY PERIOD COMMISSION CALCULATION
	IF @numComPayPeriodID = 0
	BEGIN
		INSERT INTO CommissionPayPeriod
		(
			numDomainID,
			dtStart,
			dtEnd,
			tintPayPeriod,
			numCreatedBy,
			dtCreated
		)
		VALUES
		(
			@numDomainID,
			@dtStart,
			@dtEnd,
			@tintPayPeriod,
			@numUserID,
			GETDATE()
		)

		SET @numComPayPeriodID = SCOPE_IDENTITY()
	END
	-- RECALCULATE PAY PERIOD COMMISSION
	ELSE
	BEGIN
		UPDATE 
			CommissionPayPeriod
		SET 
			numModifiedBy = @numUserID,
			dtModified = GETDATE()
		WHERE
			numComPayPeriodID = @numComPayPeriodID
	END

	--CALCULATE COMMISSION ENTERIES FOR PAID BIZDOC
	EXEC USP_CalculateCommissionPaidBizDoc @numComPayPeriodID=@numComPayPeriodID,@numDomainID=@numDomainID,@ClientTimeZoneOffset=@ClientTimeZoneOffset
	--CALCULATE COMMISSION ENTERIES FOR UNPAID OR PARTIALLY BIZDOC
	EXEC USP_CalculateCommissionUnPaidBizDoc @numComPayPeriodID=@numComPayPeriodID,@numDomainID=@numDomainID,@ClientTimeZoneOffset=@ClientTimeZoneOffset

	SELECT @numComPayPeriodID
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
END
GO

/****** Object:  StoredProcedure [dbo].[USP_DeleteJournalDetails]    Script Date: 07/26/2008 16:15:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Siva                          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_deletejournaldetails')
DROP PROCEDURE usp_deletejournaldetails
GO
CREATE PROCEDURE [dbo].[USP_DeleteJournalDetails]    
@numDomainID as numeric(9)=0,                        
@numJournalID as numeric(9)=0,                          
@numBillPaymentID as numeric(9)=0,                    
@numDepositID as numeric(9)=0,          
@numCheckHeaderID as numeric(9)=0,
@numBillID as numeric(9)=0,
@numCategoryHDRID AS NUMERIC(9)=0,
@numReturnID AS NUMERIC(9)=0,
@numUserCntID AS NUMERIC(9)=0                        
As                          
BEGIN

	           
BEGIN TRY
   BEGIN TRANSACTION 

--   DECLARE @vcCompanyName varchar(200)
--   DECLARE @vcAmount varchar(200)
--   DECLARE @vcDesc varchar(200)
--   DECLARE @vcHDesc varchar(200)

--   SELECT TOP 1 
--		@vcCompanyName=dbo.fn_GetComapnyName(DM.numDivisionID),
--		@vcAmount=DM.monDepositAmount
--		FROM DepositMaster as DM  WHERE numDepositId=@numDepositID

--	SET @vcDesc=(select TOP 1 varDescription from General_Journal_Details where numJournalId=@numJournalID)
--	SET @vcHDesc=(SELECT TOP 1
--CASE 
--WHEN isnull(dbo.General_Journal_Header.numCheckHeaderID, 0) <> 0 THEN + 'Checks' 
--WHEN isnull(dbo.General_Journal_Header.numDepositId, 0) <> 0 AND DM.tintDepositePage = 1 THEN 'Deposit' 
--WHEN isnull(dbo.General_Journal_Header.numDepositId, 0) <> 0 AND DM.tintDepositePage = 2 THEN 'Receved Pmt' 
--WHEN isnull(dbo.General_Journal_Header.numOppId, 0) <> 0 AND isnull(dbo.General_Journal_Header.numOppBizDocsId, 0) <> 0 AND isnull(dbo.General_Journal_Header.numBizDocsPaymentDetId, 0) = 0 AND 
--dbo.OpportunityMaster.tintOppType = 1 THEN 'BizDocs Invoice' 
--WHEN isnull(dbo.General_Journal_Header.numOppId, 0) <> 0 AND isnull(dbo.General_Journal_Header.numOppBizDocsId, 0) <> 0 AND isnull(dbo.General_Journal_Header.numBizDocsPaymentDetId, 0) = 0 AND 
--dbo.OpportunityMaster.tintOppType = 2 THEN 'BizDocs Purchase' 
--WHEN isnull(dbo.General_Journal_Header.numCategoryHDRID, 0) <> 0 THEN 'Time And Expenses' 
--WHEN ISNULL(General_Journal_Header.numBillId,0) <>0 AND isnull(BH.bitLandedCost,0)=1 then 'Landed Cost' 
--WHEN isnull(dbo.General_Journal_Header.numBillID, 0) <> 0 THEN 'Bill'
--WHEN isnull(dbo.General_Journal_Header.numBillPaymentID, 0) <> 0 THEN 'Pay Bill'
--WHEN isnull(dbo.General_Journal_Header.numReturnID, 0) <> 0 THEN 
--																CASE 
--																	WHEN RH.tintReturnType=1 AND RH.tintReceiveType=1 THEN 'Sales Return Refund'
--																	WHEN RH.tintReturnType=1 AND RH.tintReceiveType=2 THEN 'Sales Return Credit Memo'
--																	WHEN RH.tintReturnType=2 AND RH.tintReceiveType=2 THEN 'Purchase Return Credit Memo'
--																	WHEN RH.tintReturnType=3 AND RH.tintReceiveType=2 THEN 'Credit Memo'
--																	WHEN RH.tintReturnType=4 AND RH.tintReceiveType=1 THEN 'Refund Against Credit Memo'
--																END 
--WHEN isnull(dbo.General_Journal_Header.numOppId, 0) <> 0 AND isnull(dbo.General_Journal_Header.numOppBizDocsId, 0) = 0 THEN 'PO Fulfillment'
--WHEN dbo.General_Journal_Header.numJournal_Id <> 0 THEN 'Journal' 
--END AS TransactionType
--FROM dbo.General_Journal_Header INNER JOIN
--dbo.General_Journal_Details ON dbo.General_Journal_Header.numJournal_Id = dbo.General_Journal_Details.numJournalId INNER JOIN
--dbo.Chart_Of_Accounts ON dbo.General_Journal_Details.numChartAcntId = dbo.Chart_Of_Accounts.numAccountId LEFT OUTER JOIN
--dbo.TimeAndExpense ON dbo.General_Journal_Header.numCategoryHDRID = dbo.TimeAndExpense.numCategoryHDRID 
--LEFT OUTER JOIN dbo.DivisionMaster ON dbo.General_Journal_Details.numCustomerId = dbo.DivisionMaster.numDivisionID and 
--dbo.General_Journal_Details.numDomainID = dbo.DivisionMaster.numDomainID
--LEFT OUTER JOIN dbo.CompanyInfo ON dbo.DivisionMaster.numCompanyID = dbo.CompanyInfo.numCompanyId 
--LEFT OUTER JOIN dbo.OpportunityMaster ON dbo.General_Journal_Header.numOppId = dbo.OpportunityMaster.numOppId LEFT OUTER JOIN
--dbo.OpportunityBizDocs ON dbo.General_Journal_Header.numOppBizDocsId = dbo.OpportunityBizDocs.numOppBizDocsId LEFT OUTER JOIN
--dbo.VIEW_JOURNALCHEQ ON (dbo.General_Journal_Header.numCheckHeaderID = dbo.VIEW_JOURNALCHEQ.numCheckHeaderID 
--or (dbo.General_Journal_Header.numBillPaymentID=dbo.VIEW_JOURNALCHEQ.numReferenceID and dbo.VIEW_JOURNALCHEQ.tintReferenceType=8))LEFT OUTER JOIN
--dbo.VIEW_BIZPAYMENT ON dbo.General_Journal_Header.numBizDocsPaymentDetId = dbo.VIEW_BIZPAYMENT.numBizDocsPaymentDetId LEFT OUTER JOIN
--dbo.DepositMaster DM ON DM.numDepositId = dbo.General_Journal_Header.numDepositId  LEFT OUTER JOIN
--dbo.ReturnHeader RH ON RH.numReturnHeaderID = dbo.General_Journal_Header.numReturnID LEFT OUTER JOIN
--dbo.CheckHeader CH ON CH.numReferenceID=RH.numReturnHeaderID AND CH.tintReferenceType=10
--LEFT OUTER JOIN BillHeader BH  ON ISNULL(General_Journal_Header.numBillId,0) = BH.numBillId
--WHERE General_Journal_Header.numJournal_Id=@numJournalID)

--   INSERT INTO RecordHistory VALUES(@numDomainID,@numUserCntID,ISNULL(GETDATE(),GETUTCDATE()),@numDepositID,7,'Removed Ledger Payment',NULL,NULL,0,@vcCompanyName,@vcAmount,@vcDesc,@vcHDesc)
 
IF @numDepositID>0
BEGIN
	--Throw error messae if Deposite to Default Undeposited Funds Account
	IF EXISTS(SELECT numDepositID FROM dbo.DepositMaster WHERE numDepositID=@numDepositID And numDomainId=@numDomainId 
			AND tintDepositeToType=2 AND tintDepositePage IN(2,3) AND bitDepositedToAcnt=1)
	BEGIN
		raiserror('Undeposited_Account',16,1); 
		RETURN;
	END
	
	--Throw error messae if Deposite use in Refund
	IF EXISTS(SELECT numDepositID FROM dbo.DepositMaster WHERE numDepositID=@numDepositID And monRefundAmount>0)
	BEGIN
		raiserror('Refund_Payment',16,1); 
		RETURN;
	END
	
	DECLARE @datEntry_Date DATETIME
	SELECT @datEntry_Date = ISNULL([DM].[dtDepositDate],[DM].[dtCreationDate]) FROM [dbo].[DepositMaster] AS DM WHERE DM.[numDepositId] = @numDepositID AND [DM].[numDomainId] = @numDomainID
	--Validation of closed financial year
	PRINT @datEntry_Date
	EXEC dbo.USP_ValidateFinancialYearClosingDate @numDomainID,@datEntry_Date

	--Update SO bizdocs amount Paid
	UPDATE OBD SET monAmountPaid= OBD.monAmountPaid - DD.monAmountPaid 
		FROM DepositMaster DM JOIN DepositeDetails DD ON DM.numDepositID=DD.numDepositID 
							  JOIN OpportunityBizDocs OBD ON DD.numOppBizDocsID=OBD.numOppBizDocsID
	WHERE DM.numDomainId=@numDomainId AND DM.numDepositID=@numDepositID AND DM.tintDepositePage IN(2,3)
	
	
	--Reset Integration with accounting
	UPDATE dbo.DepositMaster SET bitDepositedToAcnt =0 WHERE numDepositId IN (SELECT numChildDepositID FROM dbo.DepositeDetails WHERE numDepositID=@numDepositID AND numChildDepositID>0)
	
	--Delete Transaction History
	DELETE FROM TransactionHistory WHERE numTransHistoryID IN (SELECT numTransHistoryID FROM DepositMaster WHERE numDepositID=@numDepositID And numDomainId=@numDomainId)
	
	DELETE FROM DepositeDetails WHERE numDepositID=@numDepositID   
	
	IF EXISTS (SELECT 1 FROM DepositMaster WHERE numDepositID=@numDepositID And numDomainId=@numDomainId AND ISNULL(numReturnHeaderID,0)>0)
	BEGIN
		UPDATE DepositMaster SET monAppliedAmount=0 WHERE numDepositID=@numDepositID And numDomainId=@numDomainId 
	END 
	ELSE
	BEGIN  
		IF @numJournalId=0
		BEGIN
			SELECT @numJournalId=numJournal_Id FROM dbo.General_Journal_Header WHERE numDepositID=@numDepositID And numDomainId=@numDomainId
		END
	 
		Delete From General_Journal_Details Where numJournalId=@numJournalId And numDomainId=@numDomainId                          
		Delete From General_Journal_Header Where numJournal_Id=@numJournalId And numDomainId=@numDomainId  
	
		DELETE FROM DepositMaster WHERE numDepositID=@numDepositID And numDomainId=@numDomainId     
	END
END    

ELSE IF @numBillID>0
BEGIN
	--Throw error message if amount paid
	IF EXISTS (SELECT numBillID FROM BillHeader WHERE numBillID=@numBillID AND ISNULL(monAmtPaid,0)>0)
	BEGIN
		raiserror('BILL_PAID',16,1); --Bill is paid, can not delete bill
		RETURN;
	END

	IF @numJournalId=0
	BEGIN
		 SELECT @numJournalId=numJournal_Id FROM dbo.General_Journal_Header WHERE numBillID=@numBillID And numDomainId=@numDomainId
	END
	
	Delete From General_Journal_Details Where numJournalId=@numJournalId And numDomainId=@numDomainId                          
	Delete From General_Journal_Header Where numJournal_Id=@numJournalId And numDomainId=@numDomainId  

	DELETE FROM ProjectsOpportunities WHERE numBillId=@numBillID AND numDomainId=@numDomainID

	DELETE FROM BillDetails WHERE numBillID=@numBillID     
	DELETE FROM BillHeader WHERE numBillID=@numBillID And numDomainId=@numDomainId     
END 

ELSE IF @numBillPaymentID>0
BEGIN
	--Throw error messae if Bill Payment use in Refund
	IF EXISTS(SELECT numBillPaymentID FROM dbo.BillPaymentHeader WHERE numBillPaymentID=@numBillPaymentID And monRefundAmount>0)
	BEGIN
		raiserror('Refund_Payment',16,1); 
		RETURN;
	END	
		
	UPDATE dbo.BillHeader SET bitIsPaid =0 WHERE numBillID IN (SELECT numBillID FROM dbo.BillPaymentDetails WHERE numBillPaymentID=@numBillPaymentID AND numBillID>0)


	--Update Add Bill amount Paid
	UPDATE BH SET monAmtPaid= monAmtPaid - BPD.monAmount FROM BillPaymentDetails BPD JOIN BillHeader BH ON BPD.numBillID=BH.numBillID
	WHERE numBillPaymentID=@numBillPaymentID AND BPD.tintBillType=2

	--Update PO bizdocs amount Paid
	UPDATE OBD SET monAmountPaid= OBD.monAmountPaid - BPD.monAmount FROM BillPaymentDetails BPD JOIN OpportunityBizDocs OBD ON BPD.numOppBizDocsId=OBD.numOppBizDocsId
	WHERE numBillPaymentID=@numBillPaymentID AND BPD.tintBillType=1
	
	--Delete if check entry
	DELETE FROM CheckHeader WHERE tintReferenceType=8 AND numReferenceID=@numBillPaymentID And numDomainId=@numDomainId     
	
	IF @numJournalId=0
	BEGIN
		SELECT @numJournalId=numJournal_Id FROM dbo.General_Journal_Header WHERE numBillPaymentID=@numBillPaymentID And numDomainId=@numDomainId
	END
     
    Delete From General_Journal_Details Where numJournalId=@numJournalId And numDomainId=@numDomainId                          
	Delete From General_Journal_Header Where numJournal_Id=@numJournalId And numDomainId=@numDomainId 
	 
	DELETE FROM BillPaymentDetails WHERE numBillPaymentID=@numBillPaymentID     
	DELETE FROM BillPaymentHeader WHERE numBillPaymentID=@numBillPaymentID And numDomainId=@numDomainId AND ISNULL(numReturnHeaderID,0)=0
	UPDATE BillPaymentHeader SET monAppliedAmount=0 WHERE numBillPaymentID=@numBillPaymentID And numDomainId=@numDomainId AND ISNULL(numReturnHeaderID,0)!=0
	
END 

ELSE IF @numCheckHeaderID>0
BEGIN
	IF @numJournalId=0
	BEGIN
		SELECT @numJournalId=numJournal_Id FROM dbo.General_Journal_Header WHERE numCheckHeaderID=@numCheckHeaderID And numDomainId=@numDomainId
	END

	Delete From General_Journal_Details Where numJournalId=@numJournalId And numDomainId=@numDomainId                          
	Delete From General_Journal_Header Where numJournal_Id=@numJournalId And numDomainId=@numDomainId 

	DELETE FROM CheckDetails WHERE numCheckHeaderID=@numCheckHeaderID
	DELETE FROM CheckHeader WHERE numCheckHeaderID=@numCheckHeaderID And numDomainId=@numDomainId     
END 

ELSE IF @numCategoryHDRID>0
BEGIN
	EXEC dbo.USP_DeleteTimExpLeave
	@numCategoryHDRID = @numCategoryHDRID, --  numeric(9, 0)
	@numDomainId = @numDomainId --  numeric(9, 0)
END

ELSE IF @numReturnID>0
BEGIN
	EXEC dbo.USP_DeleteSalesReturnDetails
	@numReturnHeaderID = @numReturnID, --  numeric(9, 0)
	@numReturnStatus = 14879, --  numeric(18, 0) Received
	@numDomainId = @numDomainId, --  numeric(9, 0)
	@numUserCntID = @numUserCntID --  numeric(9, 0)
END


IF EXISTS(SELECT numItemID FROM dbo.General_Journal_Details WHERE numJournalId=@numJournalId And numDomainId=@numDomainId AND (chBizDocItems='IA1' OR chBizDocItems='OE1') AND (ISNULL(numCreditAmt,0) <> 0 OR ISNULL(numDebitAmt,0) <> 0))
BEGIN
 		raiserror('InventoryAdjustment',16,1); 
		RETURN;
END

IF EXISTS(SELECT numJournalId FROM dbo.General_Journal_Details WHERE numJournalId=@numJournalId And numDomainId=@numDomainId AND (chBizDocItems='OE')  )
BEGIN
 		raiserror('OpeningBalance',16,1); 
		RETURN;
END



Delete From General_Journal_Details Where numJournalId IN (SELECT numJournal_Id FROM General_Journal_Header WHERE numJournal_Id=@numJournalId And numDomainId=@numDomainId 
				AND isnull(numBillID,0)=0 AND isnull(numBillPaymentID,0)=0 AND isnull(numPayrollDetailID,0)=0
				AND isnull(numCheckHeaderID,0)=0 AND isnull(numReturnID,0)=0 
				AND isnull(numDepositId,0)=0)
				                         
Delete From General_Journal_Header Where numJournal_Id=@numJournalId And numDomainId=@numDomainId 
			    AND isnull(numBillID,0)=0 AND isnull(numBillPaymentID,0)=0 AND isnull(numPayrollDetailID,0)=0
				AND isnull(numCheckHeaderID,0)=0 AND isnull(numReturnID,0)=0 
				AND isnull(numDepositId,0)=0                      

 COMMIT
 
END TRY
BEGIN CATCH
  -- Whoops, there was an error
  IF @@TRANCOUNT > 0
     ROLLBACK

  -- Raise an error with the details of the exception
  DECLARE @ErrMsg nvarchar(4000), @ErrSeverity int
  SELECT @ErrMsg = ERROR_MESSAGE(),
         @ErrSeverity = ERROR_SEVERITY()

  RAISERROR(@ErrMsg, @ErrSeverity, 1)
END CATCH
 
 
 

	
--DECLARE @numBizDocsPaymentDetId NUMERIC 
--DECLARE @bitIntegratedToAcnt BIT
--DECLARE @tintPaymentType TINYINT
--SELECT @numBizDocsPaymentDetId=ISNULL(numBizDocsPaymentDetId,0) FROM dbo.General_Journal_Header WHERE numJournal_Id = @numJournalId AND numDomainId=@numDomainId
--IF @numBizDocsPaymentDetId>0
--BEGIN
--	SELECT @bitIntegratedToAcnt=bitIntegratedToAcnt,@tintPaymentType=tintPaymentType 
--	From OpportunityBizDocsDetails Where numBizDocsPaymentDetId=@numBizDocsPaymentDetId
--	AND numBillPaymentJournalID <>@numJournalId
--
--	-- @tintPaymentType - 2 means Bill against vendor, 4 Bill Against liability
--	IF (@tintPaymentType =2 OR @tintPaymentType =4) AND @bitIntegratedToAcnt=1
--	BEGIN
--		raiserror('BILL_PAID',16,1); --Bill is paid, can not delete bill
--		RETURN;
--	END
--	
--	
--END
--
--
--
-- -- if bitOpeningBalance=0 then it is not Opening Balance         
-- Declare @bitOpeningBalance as bit        
-- Select @bitOpeningBalance=isnull(bitOpeningBalance,0) From General_Journal_Header Where numJournal_Id=@numJournalId And numDomainId=@numDomainId          
--  Print @bitOpeningBalance    
-- if @bitOpeningBalance=0
-- Begin                       
--   Delete From General_Journal_Details Where numJournalId=@numJournalId And numDomainId=@numDomainId                          
--   Delete From General_Journal_Header Where numJournal_Id=@numJournalId And numDomainId=@numDomainId                       
--                        
--   --To Delete the CheckDetails
--   Delete From CheckDetails  Where numCheckId=@numCheckId And numDomainId=@numDomainId                      
--
--   -- To Delete the CashCreditCardDetails
--   Delete From CashCreditCardDetails Where numCashCreditId=@numCashCreditCardId And numDomainId=@numDomainId             
--            
--   -- To Delete DepositDetails    
--   UPDATE dbo.DepositMaster SET bitDepositedToAcnt = 0 WHERE numDepositId IN (SELECT numChildDepositID FROM dbo.DepositeDetails WHERE numDepositID=@numDepositId)      
--   Delete From dbo.DepositeDetails Where numDepositId=@numDepositId 
--   DELETE FROM dbo.DepositMaster WHERE numDepositId =@numDepositId And numDomainId=@numDomainId       
--   --Update Vendor Payement reference Journal ID if exist
--   UPDATE  dbo.OpportunityBizDocsDetails SET numBillPaymentJournalID=NULL WHERE numBillPaymentJournalID= @numJournalId AND numBizDocsPaymentDetId=@numBizDocsPaymentDetId
--   
-- End          
--Else
-- Begin
--  Update General_Journal_Header Set numAmount=0 Where numJournal_Id=@numJournalId And numDomainId=@numDomainId          
--  Update General_Journal_Details Set numDebitAmt=0,numCreditAmt=0 Where numJournalId=@numJournalId  And numDomainId=@numDomainId       
--  Update dbo.DepositMaster Set monDepositAmount=0 Where numDepositId=@numDepositId And numDomainId=@numDomainId       
--  Update CashCreditCardDetails Set monAmount=0 Where numCashCreditId=@numCashCreditCardId And numDomainId=@numDomainId        
-- End        
End
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DomainSFTPDetail_Generate')
DROP PROCEDURE dbo.USP_DomainSFTPDetail_Generate
GO
CREATE PROCEDURE [dbo].[USP_DomainSFTPDetail_Generate]
	@numDomainID NUMERIC(18,0)
AS 
BEGIN
	IF NOT EXISTS (SELECT ID FROM DomainSFTPDetail WHERE numDomainID=@numDomainID)
	BEGIN
		BEGIN TRY
		BEGIN TRANSACTION
			DECLARE @TP1Username VARCHAr(20)
			DECLARE @TP1Password VARCHAr(20)
			DECLARE @TP2Username VARCHAr(20)
			DECLARE @TP2Password VARCHAr(20)

			;WITH CTETP1Username(n) AS (SELECT 1 UNION ALL SELECT n+1 FROM CTETP1Username WHERE n<68)
			SELECT 
				@TP1Username = LEFT((SELECT 
										'' + SUBSTRING('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz+-!@#$',n,1)
									FROM 
										CTETP1Username 
									ORDER BY 
										NEWID() FOR XML PATH('')),10)

			;WITH CTETP1Password(n) AS (SELECT 1 UNION ALL SELECT n+1 FROM CTETP1Password WHERE n<68)
			SELECT 
				@TP1Password = LEFT((SELECT 
										'' + SUBSTRING('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz+-!@#$',n,1)
									FROM 
										CTETP1Password 
									ORDER BY 
										NEWID() FOR XML PATH('')),10)

			;WITH CTETP2Username(n) AS (SELECT 1 UNION ALL SELECT n+1 FROM CTETP2Username WHERE n<68)
			SELECT 
				@TP2Username = LEFT((SELECT 
										'' + SUBSTRING('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz+-!@#$',n,1)
									FROM 
										CTETP2Username 
									ORDER BY 
										NEWID() FOR XML PATH('')),10)

			;WITH CTETP2Password(n) AS (SELECT 1 UNION ALL SELECT n+1 FROM CTETP2Password WHERE n<68)
			SELECT 
				@TP2Password = LEFT((SELECT 
										'' + SUBSTRING('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz+-!@#$',n,1)
									FROM 
										CTETP2Password 
									ORDER BY 
										NEWID() FOR XML PATH('')),10)

			INSERT INTO DomainSFTPDetail (numDomainID,vcUsername,vcPassword,tintType) VALUES (@numDomainID,@TP1Username,@TP1Password,1)
			INSERT INTO DomainSFTPDetail (numDomainID,vcUsername,vcPassword,tintType) VALUES (@numDomainID,@TP2Username,@TP2Password,2)

		COMMIT
		END TRY
		BEGIN CATCH
			DECLARE @ErrorMessage NVARCHAR(4000)
			DECLARE @ErrorNumber INT
			DECLARE @ErrorSeverity INT
			DECLARE @ErrorState INT
			DECLARE @ErrorLine INT
			DECLARE @ErrorProcedure NVARCHAR(200)

			IF @@TRANCOUNT > 0
				ROLLBACK TRANSACTION;

			SELECT 
				@ErrorMessage = ERROR_MESSAGE(),
				@ErrorNumber = ERROR_NUMBER(),
				@ErrorSeverity = ERROR_SEVERITY(),
				@ErrorState = ERROR_STATE(),
				@ErrorLine = ERROR_LINE(),
				@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

			RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);

		END CATCH
	END
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DomainSFTPDetail_Validate')
DROP PROCEDURE dbo.USP_DomainSFTPDetail_Validate
GO
CREATE PROCEDURE [dbo].[USP_DomainSFTPDetail_Validate]
	@vcUsername VARCHAR(20)
	,@vcPassword VARCHAR(20)
AS 
BEGIN
	SELECT
		numDomainID
		,tintType
	FROM
		DomainSFTPDetail
	WHERE
		vcUsername=@vcUsername
		AND vcPassword=@vcPassword
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_EDIQueue_GetPendingRecords')
DROP PROCEDURE dbo.USP_EDIQueue_GetPendingRecords
GO
CREATE PROCEDURE [dbo].[USP_EDIQueue_GetPendingRecords]

AS 
BEGIN
	SELECT TOP 50
		ID
		,EDIQueue.numDomainID
		,EDIQueue.numOppID
		,EDIType
	FROM 
		EDIQueue
	INNER JOIN
		OpportunityMaster
	ON
		EDIQueue.numOppID = OpportunityMaster.numOppId
	WHERE
		ISNULL(bitExecuted,0) = 0
		AND 1 = (CASE 
					WHEN EDIType=940 
					THEN (CASE WHEN ISNULL(tintOppType,0)=1 AND ISNULL(tintOppStatus,0)=1 THEN 1 ELSE 0 END) 
					ELSE 1 
				END)
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_EDIQueue_Insert')
DROP PROCEDURE dbo.USP_EDIQueue_Insert
GO
CREATE PROCEDURE [dbo].[USP_EDIQueue_Insert]
(
	@numDomainID NUMERIC(18,0),
	@numUserCntID NUMERIC(18,0),
    @numOppID NUMERIC(18,0),
	@numOrderStatus NUMERIC(18,0)
)
AS 
BEGIN
	IF ISNULL((SELECT bitEDI FROM Domain WHERE numDomainID=@numDomainID),0) = 1
	BEGIN
		IF ISNULL(@numOrderStatus,0) = 15445 -- 15445: SEND SHIPMENT REQUEST (940) 
			AND NOT EXISTS (SELECT ID FROM EDIQueue WHERE numDomainID=@numDomainID AND numOppID=@numOppID AND EDIType=940 AND ((ISNULL(bitSuccess,0)=1 AND ISNULL(bitExecuted,0)= 1) OR ISNULL(bitExecuted,0) = 0))
		BEGIN
			INSERT INTO EDIQueue
			(
				numDomainID
				,numOppID
				,bitExecuted
				,bitSuccess
				,dtCreatedDate
				,numCreatedBy
				,EDIType
			)
			VALUES
			(
				@numDomainID
				,@numOppID
				,0
				,0
				,GETUTCDATE()
				,@numUserCntID
				,940
			)
		END
		ELSE IF ISNULL(@numOrderStatus,0) = 15447  -- 15445: SEND 856
			AND NOT EXISTS (SELECT ID FROM EDIQueue WHERE numDomainID=@numDomainID AND numOppID=@numOppID AND EDIType=856 AND ((ISNULL(bitSuccess,0)=1 AND ISNULL(bitExecuted,0)= 1) OR ISNULL(bitExecuted,0) = 0))
		BEGIN
			INSERT INTO EDIQueue
			(
				numDomainID
				,numOppID
				,bitExecuted
				,bitSuccess
				,dtCreatedDate
				,numCreatedBy
				,EDIType
			)
			VALUES
			(
				@numDomainID
				,@numOppID
				,0
				,0
				,GETUTCDATE()
				,@numUserCntID
				,856
			)
		END
	END
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_EDIQueueLog_Insert')
DROP PROCEDURE dbo.USP_EDIQueueLog_Insert
GO
CREATE PROCEDURE [dbo].[USP_EDIQueueLog_Insert]
(
	@numEDIQueueID NUMERIC(18,0),
	@EDIType INT,
	@numDomainID NUMERIC(18,0),
    @numOppID NUMERIC(18,0),
	@vcLog NTEXT,
	@bitSuccess BIT,
	@vcExceptionMessage NTEXT
)
AS 
BEGIN
	INSERT INTO EDIQueueLog
	(
		numEDIQueueID
		,numDomainID
		,numOppID
		,vcLog
		,bitSuccess
		,vcExceptionMessage
		,dtDate
	)
	VALUES
	(
		@numEDIQueueID
		,@numDomainID
		,@numOppID
		,@vcLog
		,@bitSuccess
		,@vcExceptionMessage
		,GETUTCDATE()
	)

	IF ISNULL(@numEDIQueueID,0) > 0
	BEGIN
		UPDATE 
			EDIQueue
		SET
			bitExecuted=1
			,bitSuccess=@bitSuccess
			,dtExecutionDate=GETUTCDATE()
		WHERE
			ID=@numEDIQueueID
	END

	DECLARE @tintOppType AS TINYINT
	DECLARE @tintOppStatus AS TINYINT
	IF @EDIType = 940 
	BEGIN
		IF ISNULL(@bitSuccess,0) = 0
		BEGIN
			SELECT @tintOppStatus=tintOppStatus,@tintOppType=tintOppType from OpportunityMaster where numOppID=@numOppID
			UPDATE OpportunityMaster SET numStatus=15446,bintModifiedDate=GETUTCDATE() WHERE numOppId=@numOppID --15446: Shipment Request (940) Failed

			IF @tintOppType=1 AND @tintOppStatus=1
			BEGIN
				EXEC USP_SalesFulfillmentQueue_Save @numDomainID,0,@numOppID,15446
			END

		 	 IF EXISTS (SELECT 1 FROM dbo.OpportunityMaster WHERE numOppId = @numOppID AND tintOppType=1 AND isnull(tintOppStatus,0)=1 AND ISNULL(numStatus,0)!= 15446)
			 BEGIN
	   				EXEC USP_ManageOpportunityAutomationQueue
							@numOppQueueID = 0, -- numeric(18, 0)
							@numDomainID = @numDomainID, -- numeric(18, 0)
							@numOppId = @numOppID, -- numeric(18, 0)
							@numOppBizDocsId = 0, -- numeric(18, 0)
							@numOrderStatus = 15446, -- numeric(18, 0)
							@numUserCntID = 0, -- numeric(18, 0)
							@tintProcessStatus = 1, -- tinyint
							@tintMode = 1 -- TINYINT
			END 
		END
		ELSE
		BEGIN
			IF NOT EXISTS (SELECT ID FROM EDIHistory WHERE numOppID=@numOppID AND tintEDIStatus=3)
			BEGIN
				UPDATE OpportunityMaster SET tintEDIStatus=3,bintModifiedDate=GETUTCDATE() WHERE numOppId=@numOppID --3: 940 Sent
			
				INSERT INTO EDIHistory
				(
					numDomainID,numOppID,tintEDIStatus,dtDate
				)
				VALUES
				(
					@numDomainID,@numOppID,3,GETUTCDATE()
				)
			END
			
		END
	END
	ELSE IF @EDIType = 940997
	BEGIN
		IF NOT EXISTS (SELECT ID FROM EDIHistory WHERE numOppID=@numOppID AND tintEDIStatus=4)
		BEGIN
			UPDATE OpportunityMaster SET tintEDIStatus=4,bintModifiedDate=GETUTCDATE() WHERE numOppId=@numOppID --3: 940 Acknowledged
			
			INSERT INTO EDIHistory
			(
				numDomainID,numOppID,tintEDIStatus,dtDate
			)
			VALUES
			(
				@numDomainID,@numOppID,4,GETUTCDATE()
			)
		END
	END
	ELSE IF @EDIType = 856 
	BEGIN
		IF ISNULL(@bitSuccess,0) = 0
		BEGIN
			SELECT @tintOppStatus=tintOppStatus,@tintOppType=tintOppType from OpportunityMaster where numOppID=@numOppID
			UPDATE OpportunityMaster SET numStatus=15448,bintModifiedDate=GETUTCDATE() WHERE numOppId=@numOppID --15448: Send 856 Failed

			IF @tintOppType=1 AND @tintOppStatus=1
			BEGIN
				EXEC USP_SalesFulfillmentQueue_Save @numDomainID,0,@numOppID,15448
			END

		 	 IF EXISTS (SELECT 1 FROM dbo.OpportunityMaster WHERE numOppId = @numOppID AND tintOppType=1 AND isnull(tintOppStatus,0)=1 AND ISNULL(numStatus,0) != 15448)
			 BEGIN
	   				EXEC USP_ManageOpportunityAutomationQueue
							@numOppQueueID = 0, -- numeric(18, 0)
							@numDomainID = @numDomainID, -- numeric(18, 0)
							@numOppId = @numOppID, -- numeric(18, 0)
							@numOppBizDocsId = 0, -- numeric(18, 0)
							@numOrderStatus = 15448, -- numeric(18, 0)
							@numUserCntID = 0, -- numeric(18, 0)
							@tintProcessStatus = 1, -- tinyint
							@tintMode = 1 -- TINYINT
			END 
		END
		ELSE
		BEGIN
			IF NOT EXISTS (SELECT ID FROM EDIHistory WHERE numOppID=@numOppID AND tintEDIStatus=5)
			BEGIN
				UPDATE OpportunityMaster SET tintEDIStatus=5,bintModifiedDate=GETUTCDATE() WHERE numOppId=@numOppID --3: 856 Sent
			
				INSERT INTO EDIHistory
				(
					numDomainID,numOppID,tintEDIStatus,dtDate
				)
				VALUES
				(
					@numDomainID,@numOppID,5,GETUTCDATE()
				)
			END
		END
	END

END
GO
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ElasticSearch_GetEmails')
DROP PROCEDURE dbo.USP_ElasticSearch_GetEmails
GO
CREATE PROCEDURE [dbo].[USP_ElasticSearch_GetEmails]
	@numDomainID NUMERIC(18,0)
	,@numCurrentPage INT
	,@numPageSize INT
	,@vcEmailHistoryIds VARCHAR(MAX)
AS 
BEGIN
	--DECLARE @TEMP TABLE
	--(
	--	numEmailHstrID NUMERIC(18,0)
	--	,vcSubject VARCHAR(MAX)
	--	,vcBodyText VARCHAR(MAX)
	--	,vcFromEmail VARCHAR(150)
	--	,vcToEmail VARCHAR(MAX)
	--	,numFromEmailID NUMERIC(18,0)
	--	,numToEmailID NUMERIC(18,0)
	--	,numCCEmailID NUMERIC(18,0)
	--	,numBCCEmailID NUMERIC(18,0)
	--	,dtReceivedOn DATETIME
	--)

	--INSERT INTO 
	--	@TEMP
	--SELECT
	--	numEmailHstrID
	--	,vcSubject
	--	,vcBodyText
	--	,vcFromEmail
	--	,CONCAT(vcToEmail,CASE WHEN LEN(vcCCEmail) > 0 THEN CONCAT(', CC:',vcCCEmail) ELSE '' END,CASE WHEN LEN(vcBCCEmail) > 0 THEN CONCAT(', BCC:',vcBCCEmail) ELSE '' END)
	--	,numFromEmailID
	--	,numToEmailID
	--	,numCCEmailID
	--	,numBCCEmailID
	--	,dtReceivedOn
	--FROM
	--(
	--	SELECT
	--		numEmailHstrID
	--		,vcSubject
	--		,vcBodyText
	--		,(CASE WHEN CHARINDEX('$^$',LTRIM(RTRIM(EmailHistory.vcFrom)), (CHARINDEX('$^$',LTRIM(RTRIM(EmailHistory.vcFrom)))+1)) > 0 THEN SUBSTRING(EmailHistory.vcFrom,CHARINDEX('$^$',LTRIM(RTRIM(EmailHistory.vcFrom)), (CHARINDEX('$^$',LTRIM(RTRIM(EmailHistory.vcFrom)))+1)) + 3,LEN(EmailHistory.vcFrom) - CHARINDEX('$^$',LTRIM(RTRIM(EmailHistory.vcFrom)), (CHARINDEX('$^$',LTRIM(RTRIM(EmailHistory.vcFrom)))+1)) + 3) ELSE '' END) AS vcFromEmail
	--		,(CASE WHEN CHARINDEX('$^$',LTRIM(RTRIM(TEMPTO.vcTo)), (CHARINDEX('$^$',LTRIM(RTRIM(TEMPTO.vcTo)))+1)) > 0 THEN SUBSTRING(TEMPTO.vcTo,CHARINDEX('$^$',LTRIM(RTRIM(TEMPTO.vcTo)), (CHARINDEX('$^$',LTRIM(RTRIM(TEMPTO.vcTo)))+1)) + 3,LEN(TEMPTO.vcTo) - CHARINDEX('$^$',LTRIM(RTRIM(TEMPTO.vcTo)), (CHARINDEX('$^$',LTRIM(RTRIM(TEMPTO.vcTo)))+1)) + 3) ELSE '' END) AS vcToEmail
	--		,(CASE WHEN CHARINDEX('$^$',LTRIM(RTRIM(TEMPCC.vcCC)), (CHARINDEX('$^$',LTRIM(RTRIM(TEMPCC.vcCC)))+1)) > 0 THEN SUBSTRING(TEMPCC.vcCC,CHARINDEX('$^$',LTRIM(RTRIM(TEMPCC.vcCC)), (CHARINDEX('$^$',LTRIM(RTRIM(TEMPCC.vcCC)))+1)) + 3,LEN(TEMPCC.vcCC)-CHARINDEX('$^$',LTRIM(RTRIM(TEMPCC.vcCC)), (CHARINDEX('$^$',LTRIM(RTRIM(TEMPCC.vcCC)))+1)) + 3) ELSE '' END) AS vcCCEmail
	--		,(CASE WHEN CHARINDEX('$^$',LTRIM(RTRIM(TEMPBCC.vcBCC)), (CHARINDEX('$^$',LTRIM(RTRIM(TEMPBCC.vcBCC)))+1)) > 0 THEN SUBSTRING(TEMPBCC.vcBCC,CHARINDEX('$^$',LTRIM(RTRIM(TEMPBCC.vcBCC)), (CHARINDEX('$^$',LTRIM(RTRIM(TEMPBCC.vcBCC)))+1)) + 3,LEN(TEMPBCC.vcBCC) - CHARINDEX('$^$',LTRIM(RTRIM(TEMPBCC.vcBCC)), (CHARINDEX('$^$',LTRIM(RTRIM(TEMPBCC.vcBCC)))+1)) + 3) ELSE '' END) AS vcBCCEmail
	--		,numEmailId AS numFromEmailID
	--		,(CASE WHEN LEN(ISNULL(TEMPTO.vcTo,'')) > 0 AND CHARINDEX('$^$',TEMPTO.vcTo,0) > 0 THEN SUBSTRING(TEMPTO.vcTo,0,CHARINDEX('$^$',TEMPTO.vcTo,0)) ELSE 0 END) numToEmailID
	--		,(CASE WHEN LEN(ISNULL(TEMPCC.vcCC,'')) > 0 AND CHARINDEX('$^$',TEMPCC.vcCC,0) > 0 THEN SUBSTRING(TEMPCC.vcCC,0,CHARINDEX('$^$',TEMPCC.vcCC,0)) ELSE 0 END) numCCEmailID 
	--		,(CASE WHEN LEN(ISNULL(TEMPBCC.vcBCC,'')) > 0 AND CHARINDEX('$^$',TEMPBCC.vcBCC,0) > 0 THEN SUBSTRING(TEMPBCC.vcBCC,0,CHARINDEX('$^$',TEMPBCC.vcBCC,0)) ELSE 0 END) numBCCEmailID 
	--		,dtReceivedOn 
	--	FROM
	--		EmailHistory
	--	OUTER APPLY
	--	(
	--		SELECT item AS vcTo FROM dbo.DelimitedSplit8K(EmailHistory.vcTo,'#^#')
	--	) AS TEMPTO
	--	OUTER APPLY
	--	(
	--	SELECT item AS vcCC FROM dbo.DelimitedSplit8K(EmailHistory.vcCC,'#^#')
	--	) AS TEMPCC
	--	OUTER APPLY
	--	(
	--	SELECT item AS vcBCC FROM dbo.DelimitedSplit8K(EmailHistory.vcBCC,'#^#')
	--	) AS TEMPBCC
	--	WHERE
	--		numDomainID=@numDomainID
	--		AND (@vcEmailHistoryIds IS NULL OR numEmailHstrID IN (SELECT Id FROM dbo.SplitIDs(ISNULL(@vcEmailHistoryIds,'0'),','))) 
	--) T1

	IF ISNULL(@numCurrentPage,0) > 0 AND ISNULL(@numPageSize,0) > 0
	BEGIN
		SELECT
			numEmailHstrID AS id
			,'email' AS module
			,'' AS url
			,vcSubject AS [text]
			,CONCAT('<b style="color:#efe129">'
					,'Email:</b> '
					,dbo.FormatedDateFromDate(dtReceivedOn,@numDomainID)
					,', From:'
					,ISNULL((CASE WHEN CHARINDEX('$^$',LTRIM(RTRIM(EmailHistory.vcFrom)), (CHARINDEX('$^$',LTRIM(RTRIM(EmailHistory.vcFrom)))+1)) > 0 THEN SUBSTRING(EmailHistory.vcFrom,CHARINDEX('$^$',LTRIM(RTRIM(EmailHistory.vcFrom)), (CHARINDEX('$^$',LTRIM(RTRIM(EmailHistory.vcFrom)))+1)) + 3,LEN(EmailHistory.vcFrom) - CHARINDEX('$^$',LTRIM(RTRIM(EmailHistory.vcFrom)), (CHARINDEX('$^$',LTRIM(RTRIM(EmailHistory.vcFrom)))+1)) + 3) ELSE '' END),'')
					,', To:'
					,ISNULL(TEMPTO.vcTo,'')
					,(CASE WHEN LEN(ISNULL(TEMPCC.vcCC,'')) > 0 THEN CONCAT(', CC:',ISNULL(TEMPCC.vcCC,'')) ELSE '' END)
					,(CASE WHEN LEN(ISNULL(TEMPBCC.vcBCC,'')) > 0 THEN CONCAT(', BCC:',ISNULL(TEMPBCC.vcBCC,'')) ELSE '' END)
					,', '
					,ISNULL(vcSubject,'')
			) AS displaytext
			,ISNULL(vcSubject,'') AS Search_vcSubject
			,ISNULL(vcBodyText,'') AS Search_vcBodyText
			,dtReceivedOn AS SearchDate_dtReceivedOn
			,(CASE WHEN CHARINDEX('$^$',LTRIM(RTRIM(EmailHistory.vcFrom)), (CHARINDEX('$^$',LTRIM(RTRIM(EmailHistory.vcFrom)))+1)) > 0 THEN SUBSTRING(EmailHistory.vcFrom,CHARINDEX('$^$',LTRIM(RTRIM(EmailHistory.vcFrom)), (CHARINDEX('$^$',LTRIM(RTRIM(EmailHistory.vcFrom)))+1)) + 3,LEN(EmailHistory.vcFrom) - CHARINDEX('$^$',LTRIM(RTRIM(EmailHistory.vcFrom)), (CHARINDEX('$^$',LTRIM(RTRIM(EmailHistory.vcFrom)))+1)) + 3) ELSE '' END) AS Search_vcFromEmail
			,ISNULL(TEMPTO.vcTo,'') AS Search_vcToEmail
			,ISNULL(TEMPCC.vcCC,'') AS Search_vcCCEmail
			,ISNULL(TEMPBCC.vcBCC,'') AS Search_vcBCCEmail
			,dtReceivedOn
			,numUserCntId
		FROM
			EmailHistory
		OUTER APPLY
		(
			Select STUFF((SELECT ', ' + (CASE WHEN CHARINDEX('$^$',LTRIM(RTRIM(item)), (CHARINDEX('$^$',LTRIM(RTRIM(item)))+1)) > 0 THEN SUBSTRING(item,CHARINDEX('$^$',LTRIM(RTRIM(item)), (CHARINDEX('$^$',LTRIM(RTRIM(item)))+1)) + 3,LEN(item) - CHARINDEX('$^$',LTRIM(RTRIM(item)), (CHARINDEX('$^$',LTRIM(RTRIM(item)))+1)) + 3) ELSE '' END) FROM dbo.DelimitedSplit8K(EmailHistory.vcTo,'#^#') FOR XML PATH(''), TYPE).value('.[1]', 'nvarchar(max)'), 1, 2, '') AS vcTo
		) AS TEMPTO
		OUTER APPLY
		(
			Select STUFF((SELECT ', ' + (CASE WHEN CHARINDEX('$^$',LTRIM(RTRIM(item)), (CHARINDEX('$^$',LTRIM(RTRIM(item)))+1)) > 0 THEN SUBSTRING(item,CHARINDEX('$^$',LTRIM(RTRIM(item)), (CHARINDEX('$^$',LTRIM(RTRIM(item)))+1)) + 3,LEN(item) - CHARINDEX('$^$',LTRIM(RTRIM(item)), (CHARINDEX('$^$',LTRIM(RTRIM(item)))+1)) + 3) ELSE '' END) FROM dbo.DelimitedSplit8K(EmailHistory.vcCC,'#^#') FOR XML PATH(''), TYPE).value('.[1]', 'nvarchar(max)'), 1, 2, '') AS vcCC
		) AS TEMPCC
		OUTER APPLY
		(
			Select STUFF((SELECT ', ' + (CASE WHEN CHARINDEX('$^$',LTRIM(RTRIM(item)), (CHARINDEX('$^$',LTRIM(RTRIM(item)))+1)) > 0 THEN SUBSTRING(item,CHARINDEX('$^$',LTRIM(RTRIM(item)), (CHARINDEX('$^$',LTRIM(RTRIM(item)))+1)) + 3,LEN(item) - CHARINDEX('$^$',LTRIM(RTRIM(item)), (CHARINDEX('$^$',LTRIM(RTRIM(item)))+1)) + 3) ELSE '' END) FROM dbo.DelimitedSplit8K(EmailHistory.vcBCC,'#^#') FOR XML PATH(''), TYPE).value('.[1]', 'nvarchar(max)'), 1, 2, '') AS vcBCC
		) AS TEMPBCC
		WHERE
			numDomainID=@numDomainID
			AND (@vcEmailHistoryIds IS NULL OR numEmailHstrID IN (SELECT Id FROM dbo.SplitIDs(ISNULL(@vcEmailHistoryIds,'0'),',')))
		ORDER BY
			dtReceivedOn DESC
		OFFSET (@numCurrentPage - 1) * @numPageSize ROWS FETCH NEXT @numPageSize ROWS ONLY; 
	END
	ELSE
	BEGIN
		SELECT
			numEmailHstrID AS id
			,'email' AS module
			,'' AS url
			,vcSubject AS [text]
			,CONCAT('<b style="color:#efe129">'
					,'Email:</b> '
					,dbo.FormatedDateFromDate(dtReceivedOn,@numDomainID)
					,', From:'
					,ISNULL((CASE WHEN CHARINDEX('$^$',LTRIM(RTRIM(EmailHistory.vcFrom)), (CHARINDEX('$^$',LTRIM(RTRIM(EmailHistory.vcFrom)))+1)) > 0 THEN SUBSTRING(EmailHistory.vcFrom,CHARINDEX('$^$',LTRIM(RTRIM(EmailHistory.vcFrom)), (CHARINDEX('$^$',LTRIM(RTRIM(EmailHistory.vcFrom)))+1)) + 3,LEN(EmailHistory.vcFrom) - CHARINDEX('$^$',LTRIM(RTRIM(EmailHistory.vcFrom)), (CHARINDEX('$^$',LTRIM(RTRIM(EmailHistory.vcFrom)))+1)) + 3) ELSE '' END),'')
					,', To:'
					,ISNULL(TEMPTO.vcTo,'')
					,(CASE WHEN LEN(ISNULL(TEMPCC.vcCC,'')) > 0 THEN CONCAT(', CC:',ISNULL(TEMPCC.vcCC,'')) ELSE '' END)
					,(CASE WHEN LEN(ISNULL(TEMPBCC.vcBCC,'')) > 0 THEN CONCAT(', BCC:',ISNULL(TEMPBCC.vcBCC,'')) ELSE '' END)
					,', '
					,ISNULL(vcSubject,'')
			) AS displaytext
			,ISNULL(vcSubject,'') AS Search_vcSubject
			,ISNULL(vcBodyText,'') AS Search_vcBodyText
			,dtReceivedOn AS SearchDate_dtReceivedOn
			,(CASE WHEN CHARINDEX('$^$',LTRIM(RTRIM(EmailHistory.vcFrom)), (CHARINDEX('$^$',LTRIM(RTRIM(EmailHistory.vcFrom)))+1)) > 0 THEN SUBSTRING(EmailHistory.vcFrom,CHARINDEX('$^$',LTRIM(RTRIM(EmailHistory.vcFrom)), (CHARINDEX('$^$',LTRIM(RTRIM(EmailHistory.vcFrom)))+1)) + 3,LEN(EmailHistory.vcFrom) - CHARINDEX('$^$',LTRIM(RTRIM(EmailHistory.vcFrom)), (CHARINDEX('$^$',LTRIM(RTRIM(EmailHistory.vcFrom)))+1)) + 3) ELSE '' END) AS Search_vcFromEmail
			,ISNULL(TEMPTO.vcTo,'') AS Search_vcToEmail
			,ISNULL(TEMPCC.vcCC,'') AS Search_vcCCEmail
			,ISNULL(TEMPBCC.vcBCC,'') AS Search_vcBCCEmail
			,dtReceivedOn
			,numUserCntId
		FROM
			EmailHistory
		OUTER APPLY
		(
			Select STUFF((SELECT ', ' + (CASE WHEN CHARINDEX('$^$',LTRIM(RTRIM(item)), (CHARINDEX('$^$',LTRIM(RTRIM(item)))+1)) > 0 THEN SUBSTRING(item,CHARINDEX('$^$',LTRIM(RTRIM(item)), (CHARINDEX('$^$',LTRIM(RTRIM(item)))+1)) + 3,LEN(item) - CHARINDEX('$^$',LTRIM(RTRIM(item)), (CHARINDEX('$^$',LTRIM(RTRIM(item)))+1)) + 3) ELSE '' END) FROM dbo.DelimitedSplit8K(EmailHistory.vcTo,'#^#') FOR XML PATH(''), TYPE).value('.[1]', 'nvarchar(max)'), 1, 2, '') AS vcTo
		) AS TEMPTO
		OUTER APPLY
		(
			Select STUFF((SELECT ', ' + (CASE WHEN CHARINDEX('$^$',LTRIM(RTRIM(item)), (CHARINDEX('$^$',LTRIM(RTRIM(item)))+1)) > 0 THEN SUBSTRING(item,CHARINDEX('$^$',LTRIM(RTRIM(item)), (CHARINDEX('$^$',LTRIM(RTRIM(item)))+1)) + 3,LEN(item) - CHARINDEX('$^$',LTRIM(RTRIM(item)), (CHARINDEX('$^$',LTRIM(RTRIM(item)))+1)) + 3) ELSE '' END) FROM dbo.DelimitedSplit8K(EmailHistory.vcCC,'#^#') FOR XML PATH(''), TYPE).value('.[1]', 'nvarchar(max)'), 1, 2, '') AS vcCC
		) AS TEMPCC
		OUTER APPLY
		(
			Select STUFF((SELECT ', ' + (CASE WHEN CHARINDEX('$^$',LTRIM(RTRIM(item)), (CHARINDEX('$^$',LTRIM(RTRIM(item)))+1)) > 0 THEN SUBSTRING(item,CHARINDEX('$^$',LTRIM(RTRIM(item)), (CHARINDEX('$^$',LTRIM(RTRIM(item)))+1)) + 3,LEN(item) - CHARINDEX('$^$',LTRIM(RTRIM(item)), (CHARINDEX('$^$',LTRIM(RTRIM(item)))+1)) + 3) ELSE '' END) FROM dbo.DelimitedSplit8K(EmailHistory.vcBCC,'#^#') FOR XML PATH(''), TYPE).value('.[1]', 'nvarchar(max)'), 1, 2, '') AS vcBCC
		) AS TEMPBCC
		WHERE
			numDomainID=@numDomainID
			AND (@vcEmailHistoryIds IS NULL OR numEmailHstrID IN (SELECT Id FROM dbo.SplitIDs(ISNULL(@vcEmailHistoryIds,'0'),',')))
		ORDER BY
			dtReceivedOn DESC
	END
END
/****** Object:  StoredProcedure [dbo].[USP_GetAlertIDs]    Script Date: 07/26/2008 16:16:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Pratik Vasani


GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getalertids')
DROP PROCEDURE usp_getalertids
GO
CREATE PROCEDURE [dbo].[USP_GetAlertIDs]  
@numAlertID as numeric(9)=0
as 

select * from AlertDTL where numAlertID = @numAlertID and numAlertDTLid In (50,51)

GO
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetChartAcntDetailsForBalanceSheet_New')
DROP PROCEDURE USP_GetChartAcntDetailsForBalanceSheet_New
GO
CREATE PROCEDURE [dbo].[USP_GetChartAcntDetailsForBalanceSheet_New]                                 
@numDomainId AS NUMERIC(9),                                                  
@dtFromDate AS DATETIME,                                                
@dtToDate AS DATETIME,
@ClientTimeZoneOffset INT, 
@numAccountClass AS NUMERIC(9)=0,
@DateFilter AS VARCHAR(20),
@ReportColumn AS VARCHAR(20)                                                        
AS                                                                
BEGIN            
	SET @dtFromDate = DATEADD (MS , 000 , @dtFromDate)
	SET @dtToDate = DATEADD (MS , 997 , @dtToDate)
                                                    
	DECLARE @PLAccountStruc VARCHAR(500)
	DECLARE @FinancialYearStartDate AS DATETIME
	SELECT @FinancialYearStartDate = dbo.GetFiscalStartDate(YEAR(GETDATE()),@numDomainId)

	DECLARE @TEMPQuarter AS TABLE
	(
		ID INT,
		StartDate DATETIME,
		EndDate DATETIME
	)

	INSERT INTO @TEMPQuarter VALUES (1,@FinancialYearStartDate,DATEADD(ms,-3,DATEADD(QUARTER,1,@FinancialYearStartDate)))
	INSERT INTO @TEMPQuarter VALUES (2,DATEADD(QUARTER,1,@FinancialYearStartDate),DATEADD(ms,-3,DATEADD(QUARTER,2,@FinancialYearStartDate)))
	INSERT INTO @TEMPQuarter VALUES (3,DATEADD(QUARTER,2,@FinancialYearStartDate),DATEADD(ms,-3,DATEADD(QUARTER,3,@FinancialYearStartDate)))
	INSERT INTO @TEMPQuarter VALUES (4,DATEADD(QUARTER,3,@FinancialYearStartDate),DATEADD(ms,-3,DATEADD(QUARTER,4,@FinancialYearStartDate)))

	IF @DateFilter = 'CurYear'
	BEGIN
		SELECT @dtFromDate = @FinancialYearStartDate,
			@dtToDate = DATEADD(ms,-3,DATEADD(YEAR,1,@FinancialYearStartDate));
	END
	ELSE IF @DateFilter = 'PreYear'
	BEGIN
		SELECT @dtFromDate = DATEADD(YEAR,-1,@FinancialYearStartDate),
			@dtToDate = DATEADD(ms,-3,DATEADD(YEAR,1,DATEADD(YEAR,-1,@FinancialYearStartDate)));
	END
	ELSE IF @DateFilter = 'CurPreYear'
	BEGIN
		SELECT @dtFromDate = DATEADD(YEAR,-1,@FinancialYearStartDate),
			@dtToDate = DATEADD(ms,-3,DATEADD(YEAR,1,@FinancialYearStartDate));
	END
	ELSE IF @DateFilter = 'CuQur'
	BEGIN
		SELECT @dtFromDate = StartDate, @dtToDate=EndDate FROM @TEMPQuarter WHERE GetDate() BETWEEN StartDate AND EndDate
	END
	ELSE IF @DateFilter = 'CurPreQur'
	BEGIN
		SELECT @dtFromDate = DATEADD(QUARTER,-1,StartDate), @dtToDate=EndDate FROM @TEMPQuarter WHERE GetDate() BETWEEN StartDate AND EndDate
	END
	ELSE IF @DateFilter = 'PreQur'
	BEGIN
		SELECT @dtFromDate = DATEADD(QUARTER,-1,StartDate), @dtToDate=DATEADD(QUARTER,-1,EndDate) FROM @TEMPQuarter WHERE GetDate() BETWEEN StartDate AND EndDate
	END
	ELSE IF @DateFilter = 'LastMonth'
	BEGIN
		select @dtFromDate = DATEADD(mm,-1,DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0)),
				@dtToDate = DATEADD(ms,-3,DATEADD(mm,0,DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0)));  
	END
	ELSE IF @DateFilter = 'ThisMonth'
	BEGIN
		select @dtFromDate = DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0),
				@dtToDate = DATEADD(ms,- 3,DATEADD(mm,0,DATEADD(mm,DATEDIFF(mm,0,GETDATE())+1,0)));  
	END
	ELSE IF @DateFilter = 'CurPreMonth'
	BEGIN
		select @dtFromDate = DATEADD(mm,-1,DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0)),
				@dtToDate = DATEADD(ms,- 3,DATEADD(mm,0,DATEADD(mm,DATEDIFF(mm,0,GETDATE())+1,0))); 
	END

	CREATE TABLE #VIEW_JOURNALBS
	(
		numDomainID NUMERIC(18,0),
		numAccountID NUMERIC(18,0),
		numAccountClass NUMERIC(18,0),
		COAvcAccountCode VARCHAR(50),
		datEntry_Date DATETIME,
		Debit MONEY,
		Credit MONEY
	)

	INSERT INTO #VIEW_JOURNALBS SELECT numDomainID,numAccountID,numAccountClass,COAvcAccountCode,datEntry_Date,Debit,Credit FROM VIEW_JOURNALBS_MASTER WHERE numDomainID =@numDomainId AND (numAccountClass=@numAccountClass OR @numAccountClass=0) ORDER BY numAccountId; 

	DECLARE @view_journal TABLE
	(
		numAccountId NUMERIC(18,0),
		vcAccountCode VARCHAR(50),
		datEntry_Date SMALLDATETIME,
		Debit MONEY,
		Credit MONEY
	)

	INSERT INTO @view_journal SELECT numAccountId,vcAccountCode,datEntry_Date,Debit,Credit FROM view_journal WHERE numDomainId = @numDomainID AND (numAccountClass=@numAccountClass OR @numAccountClass=0);
	
	DECLARE @PLCHARTID NUMERIC(8)
	SELECT @PLCHARTID = numAccountId FROM dbo.Chart_Of_Accounts WHERE numDomainId=@numDomainID AND bitProfitLoss=1

	DECLARE @PLOPENING AS MONEY
	SELECT @PLOPENING = ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM @view_journal VJ WHERE numAccountId=@PLCHARTID

	;WITH DirectReport (ParentId, vcCompundParentKey, numAccountTypeID, numAccountID, vcAccountType,vcAccountCode, LEVEL,Struc)
	AS
	(
		SELECT 
			CAST('' AS VARCHAR) AS ParentId, 
			CAST(CONCAT([ATD].[numAccountTypeID],'#',0) AS VARCHAR),
			[ATD].[numAccountTypeID],
			0,
			[ATD].[vcAccountType],
			[ATD].[vcAccountCode],
			1 AS LEVEL, 
			CAST(CAST([ATD].[vcAccountCode] AS VARCHAR) AS VARCHAR(300))  AS Struc
		FROM 
			[dbo].[AccountTypeDetail] AS ATD
		WHERE 
			[ATD].[numDomainID]=@numDomainId 
			AND [ATD].[vcAccountCode] IN ('0101','0102','0105')
		UNION ALL
		SELECT 
			D.vcCompundParentKey AS ParentId,
			CAST(CONCAT([ATD].[numAccountTypeID],'#',0) AS VARCHAR),
			[ATD].[numAccountTypeID],
			0,
			[ATD].[vcAccountType],
			[ATD].[vcAccountCode],
			LEVEL + 1, 
			CAST(d.Struc + '#' + cast([ATD].[vcAccountCode] AS VARCHAR) AS VARCHAR(300))  AS Struc
		FROM 
			[dbo].[AccountTypeDetail] AS ATD 
		JOIN 
			[DirectReport] D 
		ON 
			D.[numAccountTypeID] = [ATD].[numParentID]
		WHERE 
			[ATD].[numDomainID]=@numDomainId 
	),
	DirectReport1 (ParentId, vcCompundParentKey, numAccountTypeID, vcAccountType,vcAccountCode, LEVEL,numAccountId,Struc, bitProfitLoss)
	AS
	(
		SELECT 
			ParentId, 
			vcCompundParentKey,
			numAccountTypeID, 
			vcAccountType,
			vcAccountCode, 
			LEVEL,
			CAST(numAccountID AS NUMERIC(18)),
			Struc,
			CAST(0 AS BIT)
		FROM 
			DirectReport 
		UNION ALL
		SELECT 
			CAST(CONCAT(COA.numParntAcntTypeId,'#',0) AS VARCHAR) AS ParentId,
			CAST(CONCAT(COA.numParntAcntTypeId,'#',COA.numAccountId) AS VARCHAR),
			CAST(NULL AS NUMERIC(18)), 
			[vcAccountName],
			COA.[vcAccountCode],
			LEVEL + 1,
			[COA].[numAccountId], 
			CAST(d.Struc + '#' + CAST(COA.[numAccountId] AS VARCHAR) AS VARCHAR(300)) AS Struc,
			COA.bitProfitLoss
		FROM 
			[dbo].[Chart_of_Accounts] AS COA 
		JOIN 
			[DirectReport1] D 
		ON 
			D.[numAccountTypeID] = COA.[numParntAcntTypeId]
		WHERE 
			[COA].[numDomainID]=@numDomainId 
			AND ISNULL(COA.bitActive,0) = 1
			AND ISNULL(COA.bitIsSubAccount,0) = 0
		UNION ALL
		SELECT 
			CAST(CONCAT(COA.numParntAcntTypeId,'#',COA.numParentAccId) AS VARCHAR) AS ParentId,
			CAST(CONCAT(COA.numParntAcntTypeId,'#',COA.numAccountId) AS VARCHAR),
			CAST(NULL AS NUMERIC(18)),
			[vcAccountName],
			COA.[vcAccountCode],
			LEVEL + 1,
			[COA].[numAccountId], 
			CAST(d.Struc + '#' + CAST(COA.[numAccountId] AS VARCHAR) AS VARCHAR(300)) AS Struc,
			COA.bitProfitLoss
		FROM 
			[dbo].[Chart_of_Accounts] AS COA 
		JOIN 
			[DirectReport1] D 
		ON 
			D.numAccountId = COA.numParentAccId
		WHERE 
			[COA].[numDomainID]=@numDomainId 
			AND ISNULL(COA.bitActive,0) = 1
			AND ISNULL(COA.bitIsSubAccount,0) = 1
	)

  
	SELECT 
		ParentId,
		vcCompundParentKey,
		numAccountTypeID, 
		vcAccountType, 
		LEVEL, 
		vcAccountCode,
		numAccountId,
		Struc,
		bitProfitLoss
	INTO 
		#tempDirectReport
	FROM 
		DirectReport1

	SELECT 
		COA.ParentId, 
		COA.vcCompundParentKey,
		COA.numAccountTypeID, 
		COA.vcAccountType, 
		COA.LEVEL, 
		COA.vcAccountCode, 
		COA.numAccountId, 
		COA.Struc,
		(CASE 
			WHEN COA.vcAccountCode LIKE '0105%' AND COA.numAccountId <> @PLCHARTID THEN (ISNULL(Debit,0) * (-1) - ISNULL(Credit,0) * (-1)) 
			ELSE ISNULL(Debit,0) - ISNULL(Credit,0) 
		END) AS Amount,
		V.datEntry_Date
	INTO 
		#tempViewData
	FROM 
		#tempDirectReport COA 
	LEFT JOIN 
		#VIEW_JOURNALBS V 
	ON  
		V.COAvcAccountCode like COA.vcAccountCode + '%' 
		AND datEntry_Date <= @dtToDate
	WHERE 
		COA.[numAccountId] IS NOT NULL
	
	DECLARE @columns VARCHAR(8000);--SET @columns = '';
	DECLARE @SUMColumns VARCHAR(8000);--SET @SUMColumns = '';
	DECLARE @PivotColumns VARCHAR(8000);--SET @PivotColumns = '';
	DECLARE @Select VARCHAR(8000)
	DECLARE @Where VARCHAR(8000)
	SET @Select = 'SELECT ParentId, vcCompundParentKey, numAccountTypeID, vcAccountType, LEVEL, vcAccountCode, numAccountId,Struc'


	DECLARE @sql VARCHAR(MAX) = ''

	IF @ReportColumn = 'Year'
	BEGIN
		CREATE TABLE #tempYearMonth
		(
			intYear INT,
			intMonth INT,
			MonthYear VARCHAR(50),
			StartDate DATETIME,
			EndDate DATETIME,
			ProfitLossAmount MONEY
		)

		;WITH CTE AS 
		(
			SELECT
				YEAR(@dtFromDate) AS 'yr',
				MONTH(@dtFromDate) AS 'mm',
				DATENAME(mm, @dtFromDate) AS 'mon',
				(DATENAME(mm, @dtFromDate) + '_' + CAST(YEAR(@dtFromDate) AS VARCHAR)) AS MonthYear,
				CAST(DATEFROMPARTS(YEAR(@dtFromDate),MONTH(@dtFromDate),1) AS DATETIME) dtStartDate,
				DATEADD(MS,-2,CAST(DATEADD(MONTH,1,DATEFROMPARTS(YEAR(@dtFromDate),MONTH(@dtFromDate),1)) AS DATETIME)) dtEndDate,
				@dtFromDate 'new_date'
			UNION ALL
			SELECT
				YEAR(DATEADD(d,1,new_date)) AS 'yr',
				MONTH(DATEADD(d,1,new_date)) AS 'mm',
				DATENAME(mm, DATEADD(d,1,new_date)) AS 'mon',
				(DATENAME(mm, DATEADD(d,1,new_date)) + '_' + CAST(YEAR(DATEADD(d,1,new_date)) AS VARCHAR)) AS MonthYear,
				CAST(DATEFROMPARTS(YEAR(DATEADD(d,1,new_date)),MONTH(DATEADD(d,1,new_date)),1) AS DATETIME) dtStartDate,
				DATEADD(MS,-2,CAST(DATEADD(MONTH,1,DATEFROMPARTS(YEAR(DATEADD(d,1,new_date)),MONTH(DATEADD(d,1,new_date)),1)) AS DATETIME)) dtEndDate,
				DATEADD(d,1,new_date) 'new_date'
			FROM CTE
			WHERE DATEADD(d,1,new_date) < @dtToDate
		)
	
		INSERT INTO 
			#tempYearMonth
		SELECT 
			yr,mm,MonthYear,dtStartDate,dtEndDate,0
		FROM 
			CTE
		GROUP BY 
			yr, mm, MonthYear, dtStartDate, dtEndDate
		ORDER BY 
			yr, mm
		OPTION (MAXRECURSION 5000)

		INSERT INTO #tempYearMonth VALUES (5000,5000,'Total',@dtFromDate,@dtToDate,0)

		UPDATE 
			#tempYearMonth
		SET
			EndDate = (CASE WHEN EndDate < @dtToDate THEN EndDate ELSE @dtToDate END)

		UPDATE 
			#tempYearMonth
		SET
			ProfitLossAmount = (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM @view_journal VJ WHERE vcAccountCode LIKE '0103%' AND datEntry_Date between StartDate and EndDate)
			+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM @view_journal VJ WHERE vcAccountCode LIKE '0104%' AND datEntry_Date between StartDate and EndDate)
			+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM @view_journal VJ WHERE vcAccountCode LIKE '0106%' AND datEntry_Date between StartDate and EndDate)
			+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM @view_journal VJ WHERE (vcAccountCode LIKE '0103%' OR vcAccountCode LIKE '0104%' OR vcAccountCode LIKE '0106%') AND datEntry_Date <=  DATEADD(Minute,-1,StartDate))
			+ @PLOPENING

		SELECT @columns = STUFF((SELECT ', ISNULL(' + MonthYear  + ',0) AS [' + REPLACE(MonthYear,'_',' ') + ']' FROM #tempYearMonth ORDER BY intYear,intMonth FOR XML PATH('')), 1, 2, ''); 
		SELECT @PivotColumns = STUFF((SELECT ', ' + MonthYear FROM #tempYearMonth ORDER BY intYear,intMonth FOR XML PATH('')), 1, 2, ''); 

		SELECT TOP 1 @PLAccountStruc= '#' + Struc + '#' FROM #tempDirectReport WHERe bitProfitLoss = 1

		SET @sql = 'SELECT
						ParentId,
						vcCompundParentKey,
						numAccountId,
						numAccountTypeID,
						vcAccountType,
						LEVEL,
						vcAccountCode,
						Struc,
						[Type],' + @columns + '
					FROM
					(
						SELECT 
							#tempDirectReport.ParentId,
							#tempDirectReport.vcCompundParentKey,
							#tempDirectReport.numAccountId,
							#tempDirectReport.numAccountTypeID,
							#tempDirectReport.vcAccountType,
							#tempDirectReport.LEVEL,
							#tempDirectReport.vcAccountCode,
							(''#'' + #tempDirectReport.Struc + ''#'') AS Struc,
							(CASE WHEN ISNULL(#tempDirectReport.numAccountId,0) > 0 THEN 1 ELSE 2 END) [Type],
							(CASE 
								WHEN bitProfitLoss = 1 
								THEN Period.ProfitLossAmount
								ELSE
									(CASE WHEN CHARINDEX((''#'' + #tempDirectReport.Struc + ''#''),''' + CAST(@PLAccountStruc AS VARCHAR) + ''') > 0 THEN Period.ProfitLossAmount ELSE 0 END) +
									ISNULL((SELECT sum(ISNULL(Debit,0)) FROM #VIEW_JOURNALBS VJ WHERE VJ.COAvcAccountCode like #tempDirectReport.vcAccountCode + ''%'' AND datEntry_Date <= Period.EndDate AND ISNULL(VJ.numAccountId,0) <> ' + CAST(@PLCHARTID AS VARCHAR(20))  + '),0) * (CASE WHEN #tempDirectReport.vcAccountCode LIKE ''0102%'' OR (#tempDirectReport.vcAccountCode LIKE ''0105%'' AND ISNULL(numAccountId,0) <> ' + CAST(@PLCHARTID AS VARCHAR) + ') THEN -1 ELSE 1 END) -
									ISNULL((SELECT sum(ISNULL(Credit,0)) FROM #VIEW_JOURNALBS VJ WHERE VJ.COAvcAccountCode like #tempDirectReport.vcAccountCode + ''%'' AND datEntry_Date <= Period.EndDate AND ISNULL(VJ.numAccountId,0) <> ' + CAST(@PLCHARTID AS VARCHAR(20))  + '),0) * (CASE WHEN #tempDirectReport.vcAccountCode LIKE ''0102%'' OR (#tempDirectReport.vcAccountCode LIKE ''0105%'' AND ISNULL(numAccountId,0) <> ' + CAST(@PLCHARTID AS VARCHAR) + ') THEN -1 ELSE 1 END)
							END) AS Amount,
							Period.MonthYear
						FROM 
							#tempDirectReport
						OUTER APPLY
						(
							SELECT
								MonthYear,
								StartDate,
								EndDate,
								ProfitLossAmount
							FROM
								#tempYearMonth
						) AS Period
					) AS SourceTable
					pivot
					(
						SUM(AMOUNT)
						FOR [MonthYear] IN ( ' + @pivotcolumns + ' )
					) AS P ORDER BY Struc, [Type] desc'

		EXECUTE (@sql)

		DROP TABLE #tempYearMonth
	END
	Else IF @ReportColumn = 'Quarter'
	BEGIN
		CREATE TABLE #TempYearMonthQuarter
		(
			intYear INT,
			intQuarter INT,
			MonthYear VARCHAR(50),
			StartDate DATETIME,
			EndDate DATETIME,
			ProfitLossAmount MONEY
		)

		;WITH CTE AS 
		(
			SELECT
				dbo.GetFiscalyear(@dtFromDate,@numDomainId) AS 'yr',
				dbo.GetFiscalQuarter(@dtFromDate,@numDomainId) AS 'qq',
				('Q' + cast(dbo.GetFiscalQuarter(@dtFromDate,@numDomainId) AS VARCHAR) + '_' + cast(dbo.GetFiscalyear(@dtFromDate,@numDomainId) AS VARCHAR)) AS MonthYear,
				CASE dbo.GetFiscalQuarter(@dtFromDate,@numDomainId) 
				WHEN 1 THEN dateadd(month,0,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId))
				WHEN 2 THEN dateadd(month,3,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId)) 
				WHEN 3 THEN dateadd(month,6,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId))
				WHEN 4 THEN dateadd(month,9,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId)) 
				END AS dtStartDate,
				CASE dbo.GetFiscalQuarter(@dtFromDate,@numDomainId) 
				WHEN 1 THEN dateadd(MS,-3,dateadd(month,3,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId)))
				WHEN 2 THEN dateadd(MS,-3,dateadd(month,6,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId)))
				WHEN 3 THEN dateadd(MS,-3,dateadd(month,9,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId)))
				WHEN 4 THEN dateadd(MS,-3,dateadd(month,12,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId)))
				END AS dtEndDate,
				@dtFromDate 'new_date'
			UNION ALL
			SELECT
				dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId) AS 'yr',
				dbo.GetFiscalQuarter(DATEADD(d,1,new_date),@numDomainId) AS 'qq',
				('Q' + cast(dbo.GetFiscalQuarter(DATEADD(d,1,new_date),@numDomainId) AS VARCHAR) + '_' + cast(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId) AS VARCHAR)) AS MonthYear,
				CASE dbo.GetFiscalQuarter(DATEADD(d,1,new_date),@numDomainId) 
				WHEN 1 THEN dateadd(month,0,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId))
				WHEN 2 THEN dateadd(month,3,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId)) 
				WHEN 3 THEN dateadd(month,6,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId))
				WHEN 4 THEN dateadd(month,9,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId)) 
				END AS dtStartDate,
				CASE dbo.GetFiscalQuarter(DATEADD(d,1,new_date),@numDomainId) 
				WHEN 1 THEN dateadd(MS,-3,dateadd(month,3,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId)))
				WHEN 2 THEN dateadd(MS,-3,dateadd(month,6,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId)))
				WHEN 3 THEN dateadd(MS,-3,dateadd(month,9,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId)))
				WHEN 4 THEN dateadd(MS,-3,dateadd(month,12,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId)))
				END AS dtEndDate,
				DATEADD(d,1,new_date) 'new_date'
			FROM CTE
			WHERE DATEADD(d,1,new_date) < @dtToDate
		)
	
		INSERT INTO 
			#TempYearMonthQuarter 		
		SELECT 
			yr,qq,MonthYear,dtStartDate,dtEndDate,0
		FROM 
			CTE
		GROUP BY 
			yr, qq, MonthYear, dtStartDate, dtEndDate
		ORDER BY 
			yr, qq
		OPTION
			(MAXRECURSION 5000)

		INSERT INTO #TempYearMonthQuarter VALUES (5000,5000,'Total',@dtFromDate,@dtToDate,0)

		UPDATE 
			#TempYearMonthQuarter
		SET
			EndDate = (CASE WHEN EndDate < @dtToDate THEN EndDate ELSE @dtToDate END)

		UPDATE 
			#TempYearMonthQuarter
		SET
			ProfitLossAmount = (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM @view_journal VJ WHERE vcAccountCode LIKE '0103%' AND datEntry_Date between StartDate and EndDate)
			+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM @view_journal VJ WHERE vcAccountCode LIKE '0104%' AND datEntry_Date between StartDate and EndDate)
			+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM @view_journal VJ WHERE vcAccountCode LIKE '0106%' AND datEntry_Date between StartDate and EndDate)
			+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM @view_journal VJ WHERE (vcAccountCode LIKE '0103%' OR vcAccountCode LIKE '0104%' OR vcAccountCode LIKE '0106%') AND datEntry_Date <=  DATEADD(Minute,-1,StartDate))
			+ @PLOPENING

		SELECT @columns = STUFF((SELECT ', ISNULL(' + MonthYear  + ',0) AS [' + REPLACE(MonthYear,'_',' ') + ']' FROM #TempYearMonthQuarter ORDER BY intYear,intQuarter FOR XML PATH('')), 1, 2, ''); 
		SELECT @PivotColumns = STUFF((SELECT ', ' + MonthYear FROM #TempYearMonthQuarter ORDER BY intYear,intQuarter FOR XML PATH('')), 1, 2, ''); 

		SELECT TOP 1 @PLAccountStruc= '#' + Struc + '#' FROM #tempDirectReport WHERe bitProfitLoss = 1

		SET @sql = 'SELECT
						ParentId,
						vcCompundParentKey,
						numAccountId,
						numAccountTypeID,
						vcAccountType,
						LEVEL,
						vcAccountCode,
						Struc,
						[Type],' + @columns + '
					FROM
					(
						SELECT 
							#tempDirectReport.ParentId,
							#tempDirectReport.vcCompundParentKey,
							#tempDirectReport.numAccountId,
							#tempDirectReport.numAccountTypeID,
							#tempDirectReport.vcAccountType,
							#tempDirectReport.LEVEL,
							#tempDirectReport.vcAccountCode,
							(''#'' + #tempDirectReport.Struc + ''#'') AS Struc,
							(CASE WHEN ISNULL(#tempDirectReport.numAccountId,0) > 0 THEN 1 ELSE 2 END) [Type],
							(CASE 
								WHEN bitProfitLoss = 1 
								THEN Period.ProfitLossAmount
								ELSE
									(CASE WHEN CHARINDEX((''#'' + #tempDirectReport.Struc + ''#''),''' + CAST(@PLAccountStruc AS VARCHAR) + ''') > 0 THEN Period.ProfitLossAmount ELSE 0 END) +
									ISNULL((SELECT sum(ISNULL(Debit,0)) FROM #VIEW_JOURNALBS VJ WHERE VJ.COAvcAccountCode like #tempDirectReport.vcAccountCode + ''%'' AND datEntry_Date <= Period.EndDate AND ISNULL(VJ.numAccountId,0) <> ' + CAST(@PLCHARTID AS VARCHAR(20))  + '),0) * (CASE WHEN #tempDirectReport.vcAccountCode LIKE ''0102%'' OR (#tempDirectReport.vcAccountCode LIKE ''0105%'' AND ISNULL(numAccountId,0) <> ' + CAST(@PLCHARTID AS VARCHAR) + ') THEN -1 ELSE 1 END) -
									ISNULL((SELECT sum(ISNULL(Credit,0)) FROM #VIEW_JOURNALBS VJ WHERE VJ.COAvcAccountCode like #tempDirectReport.vcAccountCode + ''%'' AND datEntry_Date <= Period.EndDate AND ISNULL(VJ.numAccountId,0) <> ' + CAST(@PLCHARTID AS VARCHAR(20)) + '),0) * (CASE WHEN #tempDirectReport.vcAccountCode LIKE ''0102%'' OR (#tempDirectReport.vcAccountCode LIKE ''0105%'' AND ISNULL(numAccountId,0) <> ' + CAST(@PLCHARTID AS VARCHAR) + ') THEN -1 ELSE 1 END)
							END) AS Amount,							
							Period.MonthYear
						FROM 
							#tempDirectReport
						OUTER APPLY
						(
							SELECT
								MonthYear,
								StartDate,
								EndDate,
								ProfitLossAmount
							FROM
								#TempYearMonthQuarter
						) AS Period
					) AS SourceTable
					pivot
					(
						SUM(AMOUNT)
						FOR [MonthYear] IN ( ' + @pivotcolumns + ' )
					) AS P ORDER BY Struc, [Type] desc'

		PRINT @sql
		EXECUTE (@sql)

		DROP TABLE #TempYearMonthQuarter
	END
	ELSE
	BEGIN
		DECLARE @ProifitLossAmount AS MONEY = 0

		SET @ProifitLossAmount = (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM @view_journal VJ WHERE vcAccountCode  LIKE '0103%' AND datEntry_Date between @dtFromDate and @dtToDate)
								+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM @view_journal VJ WHERE vcAccountCode  LIKE '0104%' AND datEntry_Date between @dtFromDate and @dtToDate)
								+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM @view_journal VJ WHERE vcAccountCode  LIKE '0106%' AND datEntry_Date between @dtFromDate and @dtToDate)
								+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM @view_journal VJ WHERE (vcAccountCode LIKE '0103%' OR vcAccountCode LIKE '0104%' OR vcAccountCode LIKE '0106%') AND datEntry_Date <=  DATEADD(MILLISECOND,-3,@dtFromDate))
								+ @PLOPENING		

		SELECT TOP 1 @PLAccountStruc= '#' + Struc + '#' FROM #tempDirectReport WHERe bitProfitLoss = 1
		
		SELECT 
			#tempDirectReport.ParentId,
			#tempDirectReport.vcCompundParentKey,
			#tempDirectReport.numAccountId,
			#tempDirectReport.numAccountTypeID,
			#tempDirectReport.vcAccountType,
			#tempDirectReport.LEVEL,
			#tempDirectReport.vcAccountCode,
			('#' + #tempDirectReport.Struc + '#') AS Struc,
			(CASE WHEN ISNULL(#tempDirectReport.numAccountId,0) > 0 THEN 1 ELSE 2 END) [Type],
			(CASE 
				WHEN bitProfitLoss = 1 
				THEN @ProifitLossAmount
				ELSE
					(CASE WHEN CHARINDEX(('#' + #tempDirectReport.Struc + '#'),@PLAccountStruc) > 0 THEN @ProifitLossAmount ELSE 0 END) +
					ISNULL((SELECT ISNULL(sum(Debit),0) FROM #VIEW_JOURNALBS VJ WHERE VJ.COAvcAccountCode like #tempDirectReport.vcAccountCode + '%' AND datEntry_Date <= @dtToDate AND ISNULL(VJ.numAccountId,0) <> @PLCHARTID),0) * (CASE WHEN #tempDirectReport.vcAccountCode LIKE '0102%' OR (#tempDirectReport.vcAccountCode LIKE '0105%' AND ISNULL(numAccountId,0) <> @PLCHARTID) THEN -1 ELSE 1 END) -
					ISNULL((SELECT ISNULL(sum(Credit),0) FROM #VIEW_JOURNALBS VJ WHERE VJ.COAvcAccountCode like #tempDirectReport.vcAccountCode + '%' AND datEntry_Date <= @dtToDate AND ISNULL(VJ.numAccountId,0) <> @PLCHARTID),0) * (CASE WHEN #tempDirectReport.vcAccountCode LIKE '0102%' OR (#tempDirectReport.vcAccountCode LIKE '0105%' AND ISNULL(numAccountId,0) <> @PLCHARTID) THEN -1 ELSE 1 END) 
			END) AS Amount
		FROM 
			#tempDirectReport
		ORDER BY 
			Struc, [Type] desc
	END

	DROP TABLE #tempViewData 
	DROP TABLE #tempDirectReport
	DROP TABLE #VIEW_JOURNALBS
END

/****** Object:  StoredProcedure [dbo].[usp_GetCompanyInfo]    Script Date: 07/26/2008 16:16:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--modified by anoop jayaraj                                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcompanyinfo')
DROP PROCEDURE usp_getcompanyinfo
GO
CREATE PROCEDURE [dbo].[usp_GetCompanyInfo]                                                                                                   
 @numDivisionID numeric,        
 @numDomainID as numeric(9)   ,        
@ClientTimeZoneOffset  int           
    --                                                         
AS                                                          
BEGIN                                                          
 SELECT CMP.numCompanyID, CMP.vcCompanyName,DM.numStatusID as numCompanyStatus,                                                          
  CMP.numCompanyRating, CMP.numCompanyType, DM.bitPublicFlag, DM.numDivisionID,                                                           
  DM. vcDivisionName, 
  ISNULL(AD1.numAddressID,0) as numBillAddressID,
  isnull(AD1.vcStreet,'') as vcBillStreet, 
  isnull(AD1.vcCity,'') as vcBillCity, 
  isnull(dbo.fn_GetState(AD1.numState),'') as vcBilState,
  isnull(AD1.vcPostalCode,'') as vcBillPostCode,
  isnull(dbo.fn_GetListName(AD1.numCountry,0),'')  as vcBillCountry,
  ISNULL(AD2.numAddressID,0) as numShipAddressID,
  isnull(AD2.vcStreet,'') as vcShipStreet,
  isnull(AD2.vcCity,'') as vcShipCity,
  isnull(dbo.fn_GetState(AD2.numState),'')  as vcShipState,
  isnull(AD2.vcPostalCode,'') as vcShipPostCode, 
  AD2.numCountry vcShipCountry,
  numFollowUpStatus,
  CMP.vcWebLabel1, CMP.vcWebLink1, CMP.vcWebLabel2, CMP.vcWebLink2,                                                           
  CMP.vcWebLabel3, CMP.vcWebLink3, CMP.vcWeblabel4, CMP.vcWebLink4,  
   tintBillingTerms,numBillingDays,
   --ISNULL(dbo.fn_GetListItemName(isnull(numBillingDays,0)),0) AS numBillingDaysName,
   (SELECT numNetDueInDays FROM dbo.BillingTerms WHERE numTermsID = numBillingDays AND numDomainID = DM.numDomainID) AS numBillingDaysName,
   tintInterestType,fltInterest,                                                          
   DM.bitPublicFlag, ISNULL(DM.numTerID,0) numTerID,                                                           
  CMP.numCompanyIndustry, CMP.numCompanyCredit,                                                           
  isnull(CMP.txtComments,'') as txtComments, isnull(CMP.vcWebSite,'') vcWebSite, CMP.numAnnualRevID, CMP.numNoOfEmployeesID, dbo.fn_GetListItemName(CMP.numNoOfEmployeesID) as NoofEmp,                                                         
  CMP.vcProfile, DM.numCreatedBy, ISNULL(DM.numRecOwner,0) AS numRecOwner, dbo.fn_GetContactName(DM.numRecOwner) as RecOwner,       
dbo.fn_GetContactName(DM.numCreatedBy)+' ' +convert(varchar(20),dateadd(minute,-@ClientTimeZoneOffset,DM.bintCreatedDate)) as vcCreatedBy,                                                    
 dbo.fn_GetContactName(DM.numModifiedBy)+' ' +convert(varchar(20),dateadd(minute,-@ClientTimeZoneOffset,DM.bintModifiedDate)) as vcModifiedBy,vcComPhone,vcComFax,                                                 
  DM.numGrpID, CMP.vcHow,                                                          
  DM.tintCRMType,isnull(DM.bitNoTax,0) bitNoTax ,                                                      
   numCampaignID,numAssignedBy,numAssignedTo,            
(select  count(*) from dbo.GenericDocuments where numRecID= @numDivisionID and  vcDocumentSection='A' AND tintDocumentType=2) as DocumentCount ,          
(SELECT count(*)from CompanyAssociations where numDivisionID=@numDivisionID and bitDeleted=0 ) as AssociateCountFrom,          
(SELECT count(*)from CompanyAssociations where numAssociateFromDivisionID=@numDivisionID and bitDeleted=0 ) as AssociateCountTo,
 ISNULL(DM.numCurrencyID,0) AS numCurrencyID,ISNULL(DM.numDefaultPaymentMethod,0) AS numDefaultPaymentMethod,ISNULL(DM.numDefaultCreditCard,0) AS numDefaultCreditCard,ISNULL(DM.bitOnCreditHold,0) AS bitOnCreditHold,
 ISNULL(vcShippersAccountNo,'') AS [vcShippersAccountNo],
 ISNULL(intShippingCompany,0) AS [intShippingCompany],ISNULL(DM.bitEmailToCase,0) AS bitEmailToCase,
 ISNULL(numDefaultExpenseAccountID,0) AS [numDefaultExpenseAccountID],
 ISNULL(numDefaultShippingServiceID,0) AS [numDefaultShippingServiceID],
 ISNULL(numAccountClassID,0) As numAccountClassID,
 ISNULL(tintPriceLevel,0) AS tintPriceLevel,DM.vcPartnerCode as vcPartnerCode,
 bitSaveCreditCardInfo
 FROM  CompanyInfo CMP                                                          
 join DivisionMaster DM    
	on DM.numCompanyID=CMP.numCompanyID
 LEFT JOIN dbo.AddressDetails AD1 ON AD1.numDomainID=DM.numDomainID 
   AND AD1.numRecordID=DM.numDivisionID AND AD1.tintAddressOf=2 AND AD1.tintAddressType=1 AND AD1.bitIsPrimary=1
   LEFT JOIN dbo.AddressDetails AD2 ON AD1.numDomainID=DM.numDomainID 
   AND AD2.numRecordID= DM.numDivisionID AND AD2.tintAddressOf=2 AND AD2.tintAddressType=2 AND AD2.bitIsPrimary=1
   left join Domain on Domain.numDomainId=DM.numDomainID
where                                            
   DM.numDivisionID=@numDivisionID   and DM.numDomainID=@numDomainID                                                                        
END
GO
/****** Object:  StoredProcedure [dbo].[Usp_getCorrespondance]    Script Date: 07/26/2008 16:17:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcorrespondance')
DROP PROCEDURE usp_getcorrespondance
GO
CREATE PROCEDURE [dbo].[Usp_getCorrespondance]                          
@FromDate as datetime,                          
@toDate as datetime,                          
@numContactId as numeric,                          
@vcMessageFrom as varchar(200)='' ,                        
@tintSortOrder as numeric,                        
@SeachKeyword as varchar(100),                        
@CurrentPage as numeric,                        
@PageSize as numeric,                        
@TotRecs as numeric output,                        
@columnName as varchar(100),                        
@columnSortOrder as varchar(50),                        
@filter as numeric,                        
@numdivisionId as numeric =0,              
@numDomainID as numeric(9)=0,              
@numCaseID as numeric(9)=0,              
@ClientTimeZoneOffset INT,
@numOpenRecordID NUMERIC=0,
@tintMode TINYINT=0,
@bitOpenCommu BIT=0
as 

set @vcMessageFrom=replace(@vcMessageFrom,'''','|')
                         
if @filter>0 set @tintSortOrder=  @filter
               
if ((@filter <=2 and @tintSortOrder<=2) or (@tintSortOrder>2 and @filter>2))                 
begin  
                                                                      
 declare @strSql as varchar(MAX)       
                                  
  declare @firstRec as integer                                                        
  declare @lastRec as integer                                                        
  set @firstRec= (@CurrentPage-1) * @PageSize                                                        
  set @lastRec= (@CurrentPage*@PageSize+1)                 
  set @strSql=''                
 if (@filter <=2 and @tintSortOrder<=2 and @bitOpenCommu=0)                 
 begin     
 declare @strCondition as varchar(MAX);
 declare @vcContactEmail as varchar(MAX);
              
  if @numdivisionId =0 and  @vcMessageFrom <> '' 
 BEGIN 
	If @filter = 1 --ReceivedMessages
		set  @strCondition=' and (vcFrom like ''%'+ REPLACE(@vcMessageFrom,'''','''''')+'%'')  ' 
	ELSE IF @filter=2 --SentMessages
		set  @strCondition=' and (vcTo like ''%'+ REPLACE(@vcMessageFrom,'''','''''')+'%'')  ' 
	ELSE
		set  @strCondition=' and (vcFrom like ''%'+ REPLACE(@vcMessageFrom,'''','''''')+'%'' or vcTo like ''%'+ REPLACE(@vcMessageFrom,'''','''''')+'%'')  ' 
 END
  else if @numdivisionId <>0 
 BEGIN

 /* This approach does not work as numEmailId stores who sent email, id of sender
  set @strCondition=' and HDR.numEmailID in (
  SELECT numEmailID FROM dbo.EmailMaster WHERE numDomainID= ' + CONVERT(VARCHAR(15),@numDomainID) +' AND numContactID 
  IN (SELECT numcontactid FROM dbo.AdditionalContactsInformation WHERE numDivisionID = ' + convert(varchar(15),@numDivisionID) + ' AND numDomainID = ' + CONVERT(VARCHAR(15),@numDomainID) +' ))';
*/
-- bug fix id 1591,1628 
  DECLARE @TnumContactID NUMERIC(9);set @strCondition=''

  select TOP 1 @TnumContactID=numContactID,@vcContactEmail=replace(vcEmail,'''','''''') from AdditionalContactsInformation where numdivisionid=@numdivisionId and len(vcEmail)>2
  ORDER BY numContactID ASC
  
  WHILE @TnumContactID>0
  BEGIN
   if len(@strCondition)>0
    SET @strCondition=@strCondition + ' or '

	If @filter = 1 --ReceivedMessages
		set  @strCondition = @strCondition + ' (vcFrom like ''%'+ REPLACE(@vcContactEmail,'''','''''') +'%'')  ' 
	ELSE IF @filter=2 --SentMessages
		set  @strCondition= @strCondition + ' (vcTo like ''%'+ REPLACE(@vcContactEmail,'''','''''') +'%'')  ' 
	ELSE
		set  @strCondition= @strCondition + ' (vcFrom like ''%'+ REPLACE(@vcContactEmail,'''','''''')+'%'' or vcTo like ''%'+ REPLACE(@vcContactEmail,'''','''''')+'%'')  '

   select TOP 1 @TnumContactID=numContactID,@vcContactEmail=vcEmail from AdditionalContactsInformation where numdivisionid=@numdivisionId and len(vcEmail)>2 and numContactID>@TnumContactID
   ORDER BY numContactID ASC

   IF @@rowcount = 0 SET @TnumContactID = 0 
  END
 
  if len(@strCondition)>0
   SET @strCondition= 'and (' + @strCondition + ')'
  else
   SET @strCondition=' and (1=0)' 
  --set @strCondition=' and DTL.numEmailId  in (select numEmailId from EmailMaster where vcEmailID in (select vcEmail  from AdditionalContactsInformation where numdivisionid='''+ convert(varchar(15),@numdivisionId)+''' ))    '          
  END
  else if @numContactId>0 
 BEGIN
  select @vcContactEmail=isnull(replace(vcEmail,'''',''''''),'') from AdditionalContactsInformation where numContactID=@numContactId and len(vcEmail)>2

  if len(@vcContactEmail)>0
	If @filter = 1 --ReceivedMessages
		set  @strCondition=' and (vcFrom like ''%'+ REPLACE(@vcContactEmail,'''','''''') +'%'')  ' 
	ELSE IF @filter=2 --SentMessages
		set  @strCondition=' and (vcTo like ''%'+ REPLACE(@vcContactEmail,'''','''''')+'%'')  ' 
	ELSE
		set  @strCondition=' and (vcFrom like ''%'+ REPLACE(@vcContactEmail,'''','''''')+'%'' or vcTo like ''%'+ REPLACE(@vcContactEmail,'''','''''')+'%'')' 
  else
   SET @strCondition=' and (1=0)'
 END
 else 
  SET @strCondition=''
    
  set @strSql= '                        
  SELECT distinct(HDR.numEmailHstrID),convert(varchar(15),HDR.numEmailHstrID)+''~1~0''+convert(varchar(15),HDR.numUserCntId) as DelData,HDR.bintCreatedOn,
  dbo.FormatedDateTimeFromDate(DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+', bintCreatedOn),'+convert(varchar(15),@numDomainID)+') as [date],                
  vcSubject as [Subject],                          
  ''Email'' as [type] ,                          
  '''' as phone,'''' as assignedto ,                          
  ''0'' as caseid,                             
  null as vcCasenumber,                                 
  ''0''as caseTimeid,                                
  ''0''as caseExpid ,                          
  hdr.tinttype ,HDR.bintCreatedOn as dtCreatedDate,vcFrom + '','' + vcTo as [From] ,0 as bitClosedflag,ISNULL(HDR.bitHasAttachments,0) bitHasAttachments,
 CASE WHEN LEN(Cast(vcBodyText AS VARCHAR(1000)))>150 THEN SUBSTRING(vcBodyText,0,150) + ''...'' ELSE
   Cast(vcBodyText AS VARCHAR(1000)) END AS vcBody                     
  from EmailHistory HDR                                 
  LEFT JOIN [Correspondence] CR ON CR.numEmailHistoryID = HDR.numEmailHstrID '                    
   set  @strSql=@strSql +' where ISNULL(HDR.bitInvisible,0) = 0 AND HDR.numDomainID='+convert(varchar(15),@numDomainID)+' and  (bintCreatedOn between '''+ convert(varchar(30),@FromDate)+''' and '''+ convert(varchar(30),@ToDate)+''') '
   + ' AND (CR.numOpenRecordID = '+ convert(varchar(15),@numOpenRecordID) +' OR '+ convert(varchar(15),@numOpenRecordID) +'=0)'
  
 set  @strSql=@strSql+ @strCondition
--  if @numdivisionId =0 and  @vcMessageFrom <> '' set  @strSql=@strSql+ 'and (vcFrom like ''%'+ @vcMessageFrom+'%'' or vcTo like ''%'+ @vcMessageFrom+'%'')  '                      
--  else if @numdivisionId <>0 
-- BEGIN
--  set @strSql=@strSql+ 'and DTL.numEmailId  in (select numEmailId from EmailMaster where vcEmailID in (select vcEmail  from AdditionalContactsInformation where numdivisionid='''+ convert(varchar(15),@numdivisionId)+''' ))    '          
--  END
--  else if @numContactId>0 
-- BEGIN
--  declare @vcContactEmail as varchar(100);
--  select @vcContactEmail=vcEmail from AdditionalContactsInformation where numContactID=@numContactId
--
--  set  @strSql=@strSql+ 'and (vcFrom like ''%'+ @vcContactEmail+'%'' or vcTo like ''%'+ @vcContactEmail+'%'')    ' 
-- END

  IF @tintMode=1 SET @strSql=@strSql+ ' and  (CR.tintCorrType IN(1,3) OR '+ convert(varchar(15),@numOpenRecordID) +'=0)'
  IF @tintMode=2 SET @strSql=@strSql+ ' and  (CR.tintCorrType IN(2,4) OR '+ convert(varchar(15),@numOpenRecordID) +'=0)'
  IF @tintMode=5 SET @strSql=@strSql+ ' and  (CR.tintCorrType IN(5) OR '+ convert(varchar(15),@numOpenRecordID) +'=0)'
  IF @tintMode=6 SET @strSql=@strSql+ ' and  (CR.tintCorrType IN(6) OR '+ convert(varchar(15),@numOpenRecordID) +'=0)'
  
  if @filter =0 set  @strSql=@strSql+ ' and (hdr.tinttype=1 or hdr.tinttype=2 or  hdr.tinttype=5)'                  
  else if @filter =1 OR @filter =2 set  @strSql=@strSql+ ' and (hdr.tinttype=1 OR hdr.tinttype=2)'                                             
  if @filter > 2 set  @strSql=@strSql+ ' and hdr.tinttype=0'                 
                        
  if @tintSortOrder =0 and @SeachKeyword <> ''                 
  begin                
   set  @strSql=@strSql+ ' and (vcFrom like ''%'+ @SeachKeyword+'%'' or vcTo like ''%'+ @SeachKeyword+'%'' or vcsubject like ''%'+@SeachKeyword+'%'' or vcBody like ''%'+@SeachKeyword+'%'')'               
  end                
  if @tintSortOrder =1 and @SeachKeyword <> ''                 
  begin                
     set  @strSql=@strSql+ ' and (vcFrom like ''%'+ @SeachKeyword+'%'' or vcsubject like ''%'+@SeachKeyword+'%'' or  vcBody like ''%'+@SeachKeyword+'%'')'                 
  end                
  if @tintSortOrder =2 and @SeachKeyword <> ''                 
  begin                
     set  @strSql=@strSql+ ' and (vcTo like ''%'+ @SeachKeyword+'%'' or vcsubject like ''%'+@SeachKeyword+'%'' or  vcBody like ''%'+@SeachKeyword+'%'')'                 
  end                            
 end                
 if (@filter =0 and @tintSortOrder=0 and @bitOpenCommu=0) set  @strSql=@strSql+ ' union '                
                
 if ((@filter=0 or @filter>2) and (@tintSortOrder=0 or @tintSortOrder>2))                
 begin                
  set  @strSql=@strSql+ '                     
  SELECT c.numCommId as numEmailHstrID,convert(varchar(15),C.numCommId)+''~2~''+convert(varchar(15),C.numCreatedBy) as DelData,CASE WHEN c.bitTask = 973 THEN dtCreatedDate WHEN c.bitTask = 974 THEN dtCreatedDate WHEN c.bitTask = 971 THEN dtStartTime ELSE dtEndTime END AS bintCreatedOn,
  case when c.bitTask=973 then dbo.FormatedDateTimeFromDate(DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+', dtCreatedDate),'+convert(varchar(15),@numDomainID)+') 
  when c.bitTask=974 then dbo.FormatedDateTimeFromDate(DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+', dtEndTime),'+convert(varchar(15),@numDomainID)+') 
  when c.bitTask=971 THEN dbo.FormatedDateFromDate(DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+', dtEndTime),'+convert(varchar(15),@numDomainID)+') + '' '' + RIGHT(CONVERT(VARCHAR,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+', [dtStartTime]),100),7)  +'' - '' + RIGHT(CONVERT(VARCHAR,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+', [dtEndTime]),100),7)
  else  dbo.FormatedDateFromDate(DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+', dtEndTime),'+convert(varchar(15),@numDomainID)+') + '' '' + RIGHT(CONVERT(VARCHAR,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+', [dtStartTime]),100),7)  +'' - '' + RIGHT(CONVERT(VARCHAR,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+', [dtEndTime]),100),7)  END AS [date],
  convert(varchar(8000),textdetails) as [Subject],                                                                         
 dbo.fn_GetListItemName(c.bitTask)  AS [Type],                                                                 
  A1.vcFirstName +'' ''+ A1.vcLastName +'' ''+                                      
  case when A1.numPhone<>'''' then + A1.numPhone +case when A1.numPhoneExtension<>'''' then '' - '' + A1.numPhoneExtension else '''' end  else '''' end as [Phone],                        
  A2.vcFirstName+ '' ''+A2.vcLastName  as assignedto,                         
  c.caseid,                           
  (select top 1 vcCaseNumber from cases where cases.numcaseid= c.caseid )as vcCasenumber,                               
  isnull(caseTimeid,0)as caseTimeid,                              
  isnull(caseExpid,0)as caseExpid  ,                        
  1 as tinttype ,dtCreatedDate,'''' as [FROM]     ,isnull(C.bitClosedflag,0) as bitClosedflag,0 as bitHasAttachments,
'''' as vcBody
  from  Communication C                  
  JOIN AdditionalContactsInformation  A1                                           
  ON c.numContactId = A1.numContactId
  LEFT JOIN CommunicationLinkedOrganization clo ON C.numCommID=clo.numCommID                                                                             
  left join AdditionalContactsInformation A2 on A2.numContactID=c.numAssign
  left join UserMaster UM on A2.numContactid=UM.numUserDetailId and isnull(UM.bitActivateFlag,0)=1                                                                           
  left join listdetails on numActivity=numListID                           
  LEFT JOIN [Correspondence] CR ON CR.numCommID = C.numCommID
  where  C.numDomainID=' +convert(varchar(15),@numDomainID)                       
  + 'AND (numOpenRecordID = '+ convert(varchar(15),@numOpenRecordID) +' OR '+ convert(varchar(15),@numOpenRecordID) +'=0)'
  if (@numdivisionId =0 and @numContactId<>0) set  @strSql=@strSql+' and C.numContactId='+ convert(varchar(15),@numContactId)                        
  if @numdivisionId <> 0 set  @strSql=@strSql+' and  (C.numDivisionId='+ convert(varchar(15),@numdivisionId) + ' OR clo.numDivisionID='+ convert(varchar(15),@numdivisionId) + ') '             
  if @numCaseID>0  set  @strSql=@strSql+' and  C.CaseId='+ convert(varchar(15),@numCaseID)                       
  if @filter =0  
  BEGIN
	IF @tintMode <> 9
	BEGIN
		PRINT @tintMode
		SET @strSql=@strSql+ ' and dtStartTime  >= '''+ convert(varchar(30),@FromDate)+''' and dtStartTime  <= '''+ convert(varchar(30),@ToDate)  +''''
	END
  END
  else
    SET @strSql=@strSql+ ' and c.bitTask=' + CONVERT(varchar(12),@filter) + ' and ((dtStartTime  >= '''+ convert(varchar(30),@FromDate)+''' and dtStartTime  <= '''+ convert(varchar(30),@ToDate)  +''') OR (dtEndTime  >= '''+ convert(varchar(30),@FromDate)+''' and dtEndTime  <= '''+ convert(varchar(30),@ToDate)  +'''))'
  
  IF @tintMode=1 SET @strSql=@strSql+ ' and  (CR.tintCorrType IN(1,3) OR '+ convert(varchar(15),@numOpenRecordID) +'=0)'
  IF @tintMode=2 SET @strSql=@strSql+ ' and  (CR.tintCorrType IN(2,4) OR '+ convert(varchar(15),@numOpenRecordID) +'=0)'
  IF @tintMode=5 SET @strSql=@strSql+ ' and  (CR.tintCorrType IN(5) OR '+ convert(varchar(15),@numOpenRecordID) +'=0)'
  IF @tintMode=6 SET @strSql=@strSql+ ' and  (CR.tintCorrType IN(6) OR '+ convert(varchar(15),@numOpenRecordID) +'=0)'
                          
  IF @tintSortOrder =0 and @SeachKeyword <>'' 
	SET @strSql=@strSql+ ' and textdetails like ''%'+@SeachKeyword+'%'''
  ELSE
	IF ISNULL(@filter,0) = 0 
	BEGIN
		SET @strSql=@strSql+ ' and textdetails like ''%'+@SeachKeyword+'%'''
	END
	ELSE
	BEGIN
		SET @strSql=@strSql+ ' and c.bitTask=' + CONVERT(varchar(12),@filter) + ' and textdetails like ''%'+@SeachKeyword+'%'''                     
	END
  IF @bitOpenCommu=1  SET @strSql=@strSql+ ' and isnull(C.bitClosedflag,0)=0'
 end                
 set @strSql='With tblCorr as (SELECT ROW_NUMBER() OVER (ORDER BY '+@columnName+' '+ @columnSortOrder+') AS RowNumber,X.* from('+@strSql+')X)'                

 set @strSql=@strSql +'                       
  select  *  from tblCorr
  Where RowNumber > '+convert(varchar(10),@firstRec)+' and RowNumber < '+convert(varchar(10),@lastRec)+'                     
  union select 0,count(*),null,null,null,'','',null,null,null,null,null,null,null,null,null,null,0,0,null from tblCorr order by RowNumber'                 
                
-- set @strSql=@strSql +'                       
--  select  *,CASE WHEN [Type]=''Email'' THEN dbo.GetEmaillAdd(numEmailHstrID,4)+'', '' +dbo.GetEmaillAdd(numEmailHstrID,1) ELSE '''' END as [From] from tblCorr
--  Where RowNumber > '+convert(varchar(10),@firstRec)+' and RowNumber < '+convert(varchar(10),@lastRec)+'                     
--  union select 0,count(*),null,null,null,'','',null,null,null,null,null,null,null,null,null,null from tblCorr order by RowNumber'                 
                
 print @strSql                
 exec (@strSql)                
end                
else                
select 0,0
/****** Object:  StoredProcedure [dbo].[usp_getCustomFormFields]    Script Date: 07/26/2008 16:17:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'usp_getCustomFormFields' ) 
    DROP PROCEDURE usp_getCustomFormFields
GO
CREATE PROCEDURE [dbo].[usp_getCustomFormFields]
    @numDomainId NUMERIC(9),
    @vcLocID VARCHAR(50)--Comma seperated location id from CFW_Loc_Master
AS 
    CREATE TABLE #tempAvailableFields(numFormFieldId  NVARCHAR(20),vcFormFieldName NVARCHAR(50),
        vcFieldType VARCHAR(15),vcAssociatedControlType NVARCHAR(50),numListID NUMERIC(18),vcDbColumnName NVARCHAR(50),
        vcListItemType CHAR(1),intColumnNum INT,intRowNum INT,boolRequired tinyint,
          vcFieldDataType CHAR(1),boolAOIField tinyint,vcLookBackTableName NVARCHAR(50))
  
   
        INSERT INTO #tempAvailableFields (numFormFieldId,vcFormFieldName,vcFieldType,vcAssociatedControlType
        ,numListID,vcDbColumnName,vcListItemType,intColumnNum,intRowNum,boolRequired,
          vcFieldDataType,boolAOIField,vcLookBackTableName)                         
          
            SELECT  CONVERT(VARCHAR(15), Fld_id) + 'C' AS numFormFieldId,
                    Fld_label AS vcFormFieldName,
                   CASE GRP_ID WHEN 4 then 'C' 
								WHEN 1 THEN 'D' 
								WHEN 9 THEN CONVERT(VARCHAR(15), Fld_id)
								ELSE 'C' 
					END AS vcFieldType,
                    fld_type as vcAssociatedControlType,
                    ISNULL(C.numListID, 0) numListID,
                    Fld_label vcDbColumnName,
                    CASE WHEN C.numListID > 0 THEN 'L'
                         ELSE ''
                    END vcListItemType,
                    0 intColumnNum,
                    0 intRowNum,
                    ISNULL(V.bitIsRequired,0) as boolRequired,
                    CASE WHEN C.numListID > 0 THEN 'N'
                         ELSE 'V'
                    END vcFieldDataType,
                    0 boolAOIField,
                    '' AS vcLookBackTableName
            FROM    CFW_Fld_Master C LEFT JOIN CFW_Validation V ON V.numFieldID = C.Fld_id
            WHERE   C.numDomainID = @numDomainId
                    AND GRP_ID IN (SELECT id from dbo.SplitIDs(@vcLocID,','))

        
  		SELECT * FROM #tempAvailableFields 
        
        DROP TABLE #tempAvailableFields
        
GO
/****** Object:  StoredProcedure [dbo].[USP_GetDealsList1]    Script Date: 05/07/2009 22:12:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getdealslist1')
DROP PROCEDURE usp_getdealslist1
GO
CREATE PROCEDURE [dbo].[USP_GetDealsList1]
               @numUserCntID              NUMERIC(9)  = 0,
               @numDomainID               NUMERIC(9)  = 0,
               @tintSalesUserRightType    TINYINT  = 0,
               @tintPurchaseUserRightType TINYINT  = 0,
               @tintSortOrder             TINYINT  = 4,
               @CurrentPage               INT,
               @PageSize                  INT,
               @TotRecs                   INT  OUTPUT,
               @columnName                AS VARCHAR(50),
               @columnSortOrder           AS VARCHAR(10),
               @numCompanyID              AS NUMERIC(9)  = 0,
               @bitPartner                AS BIT  = 0,
               @ClientTimeZoneOffset      AS INT,
               @tintShipped               AS TINYINT,
               @intOrder                  AS TINYINT,
               @intType                   AS TINYINT,
               @tintFilterBy              AS TINYINT  = 0,
               @numOrderStatus			  AS NUMERIC,
			   @vcRegularSearchCriteria varchar(MAX)='',
			   @vcCustomSearchCriteria varchar(MAX)='',
			   @SearchText VARCHAR(300) = '',
			   @SortChar char(1)='0',
			   @tintDashboardReminderType TINYINT = 0
AS
	DECLARE  @PageId  AS TINYINT
	DECLARE  @numFormId  AS INT 
  
	SET @PageId = 0

	IF @inttype = 1
	BEGIN
		SET @PageId = 2
		SET @inttype = 3
		SET @numFormId=39
	END
	ELSE IF @inttype = 2
	BEGIN
		SET @PageId = 6
		SET @inttype = 4
		SET @numFormId=41
	END

	CREATE TABLE #tempForm 
	(
		tintOrder TINYINT,vcDbColumnName nvarchar(50),vcOrigDbColumnName nvarchar(50),vcFieldName nvarchar(50),vcAssociatedControlType nvarchar(50),vcListItemType varchar(3),
		numListID numeric(9),vcLookBackTableName varchar(50),bitCustomField BIT,numFieldId NUMERIC,bitAllowSorting BIT,bitAllowEdit BIT,bitIsRequired BIT,bitIsEmail BIT,
		bitIsAlphaNumeric BIT,bitIsNumeric BIT,bitIsLengthValidation BIT,intMaxLength int,intMinLength int,bitFieldMessage BIT,vcFieldMessage VARCHAR(500),
		ListRelID numeric(9),intColumnWidth int,bitAllowFiltering BIT,vcFieldDataType CHAR(1)
	)

	-- FIRST CHECK IF GRID COLUMNS ARE CONFIGURED
	DECLARE  @Nocolumns  AS TINYINT = 0

	SELECT 
		@Nocolumns=ISNULL(SUM(TotalRow),0) 
	FROM
	(            
		SELECT 
			COUNT(*) TotalRow 
		FROM 
			View_DynamicColumns 
		WHERE 
			numFormId=@numFormId 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND numRelCntType = @numFormId
		UNION 
		SELECT 
			COUNT(*) TotalRow 
		FROM 
			View_DynamicCustomColumns 
		WHERE 
			numFormId=@numFormId 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND numRelCntType = @numFormId
	) TotalRows

	-- IF GRID COLUMNS ARE NOT CONFIGURES THEN CHECK IF MASTER FORM CONFIGURATION IS CREATED BY ADMINISTRATOR
	DECLARE @IsMasterConfAvailable BIT = 0
	DECLARE @numUserGroup NUMERIC(18,0)

	IF @Nocolumns=0
	BEGIN
		SELECT @numUserGroup = numGroupID FROM UserMaster WHERE numUserDetailId = @numUserCntID 

		IF (SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = @numDomainID AND numFormID = @numFormId AND numRelCntType=@numFormId AND bitGridConfiguration = 1 AND numGroupID = @numUserGroup) > 0
		BEGIN
			SET @IsMasterConfAvailable = 1
		END
	END

	--IF MASTERCONFIGURATION IS AVAILABLE THEN LOAD IT OTHERWISE LOAD DEFAULT COLUMNS
	IF @IsMasterConfAvailable = 1
	BEGIN
		INSERT INTO DycFormConfigurationDetails 
		(
			numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth
		)
		SELECT 
			@numFormId,numFieldId,intColumnNum,intRowNum,@numDomainID,@numUserCntID,@numFormId,1,0,intColumnWidth
		FROM 
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId=@numFormId AND 
			View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicColumnsMasterConfig.numRelCntType=@numFormId AND 
			View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
			ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
		UNION
		SELECT 
			@numFormId,numFieldId,intColumnNum,intRowNum,@numDomainID,@numUserCntID,@numFormId,1,1,intColumnWidth
		FROM 
			View_DynamicCustomColumnsMasterConfig
		WHERE 
			View_DynamicCustomColumnsMasterConfig.numFormId=@numFormId AND 
			View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicCustomColumnsMasterConfig.numRelCntType=@numFormId AND 
			View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1


		INSERT INTO 
			#tempForm
		SELECT 
			(intRowNum + 1) as tintOrder, vcDbColumnName,	vcOrigDbColumnName,	ISNULL(vcCultureFieldName,vcFieldName), vcAssociatedControlType,
			vcListItemType, numListID, vcLookBackTableName,	bitCustom, numFieldId, bitAllowSorting,	bitInlineEdit, bitIsRequired,
			bitIsEmail,	bitIsAlphaNumeric, bitIsNumeric, bitIsLengthValidation,	intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,
			ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType 
		FROM 
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId=@numFormId AND 
			View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicColumnsMasterConfig.numRelCntType=@numFormId AND 
			View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
			ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
		UNION
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName, vcAssociatedControlType,'' as vcListItemType,numListID,'',
			bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,''
		FROM 
			View_DynamicCustomColumnsMasterConfig
		WHERE 
			View_DynamicCustomColumnsMasterConfig.numFormId=@numFormId AND 
			View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicCustomColumnsMasterConfig.numRelCntType=@numFormId AND 
			View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1
		ORDER BY 
			tintOrder ASC  
	END
	ELSE                                        
	BEGIN
		IF @Nocolumns=0
		BEGIN
			INSERT INTO DycFormConfigurationDetails 
			(
				numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth
			)
			SELECT 
				@numFormId,numFieldId,0,Row_number() over(order by tintRow desc),@numDomainID,@numUserCntID,@numFormId,1,0,intColumnWidth
			FROM 
				View_DynamicDefaultColumns
			WHERE 
				numFormId=@numFormId 
				AND bitDefault=1 
				AND ISNULL(bitSettingField,0)=1 
				AND numDomainID=@numDomainID
			ORDER BY 
				tintOrder asc   
		END

		INSERT INTO 
			#tempForm
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcOrigDbColumnName,ISNULL(vcCultureFieldName,vcFieldName),                  
			vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName,bitCustom,numFieldId,bitAllowSorting,bitInlineEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength,bitFieldMessage,
			vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType  
		FROM 
			View_DynamicColumns 
		WHERE 
			numFormId=@numFormId 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND ISNULL(bitSettingField,0)=1 
			AND ISNULL(bitCustom,0)=0  
			AND numRelCntType = @numFormId
		UNION
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName,vcAssociatedControlType,'' as vcListItemType,numListID,'',bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,'' 
		FROM 
			View_DynamicCustomColumns
		WHERE 
			numFormId=@numFormId 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND ISNULL(bitCustom,0)=1 
			AND numRelCntType = @numFormId
		ORDER BY 
			tintOrder asc  
	END

	DECLARE  @strColumns AS VARCHAR(MAX)
	SET @strColumns = ' ADC.numContactId, ADC.vcEmail, Div.numDivisionID,ISNULL(Div.numTerID,0) numTerID, opp.numRecOwner as numRecOwner, Div.tintCRMType, opp.numOppId, opp.monDealAmount, ISNULL(opp.intUsedShippingCompany,0) AS intUsedShippingCompany'
	--Corr.bitIsEmailSent,

	DECLARE  @tintOrder  AS TINYINT;SET @tintOrder = 0
	DECLARE  @vcFieldName  AS VARCHAR(50)
	DECLARE  @vcListItemType  AS VARCHAR(3)
	DECLARE  @vcListItemType1  AS VARCHAR(1)
	DECLARE  @vcAssociatedControlType VARCHAR(30)
	DECLARE  @numListID  AS NUMERIC(9)
	DECLARE  @vcDbColumnName VARCHAR(40)
	DECLARE  @WhereCondition VARCHAR(2000);SET @WhereCondition = ''
	DECLARE  @vcLookBackTableName VARCHAR(2000)
	DECLARE  @bitCustom  AS BIT
	Declare @numFieldId as numeric  
	DECLARE @bitAllowSorting AS CHAR(1)   
	DECLARE @bitAllowEdit AS CHAR(1)                 
	DECLARE @vcColumnName AS VARCHAR(500)          
	
	Declare @ListRelID as numeric(9) 
	DECLARE @SearchQuery AS VARCHAR(MAX) = ''        

	SELECT TOP 1 
		@tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName,@vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,
		@numListID=numListID,@vcLookBackTableName=vcLookBackTableName ,@bitCustom=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,@bitAllowEdit=bitAllowEdit,
		@ListRelID=ListRelID
	FROM 
		#tempForm 
	ORDER BY 
		tintOrder ASC            
--	SET @strColumns = @strColumns + ' , (SELECT SUBSTRING((SELECT '', '' + cast(I.vcItemName as varchar)
--                      FROM OpportunityItems AS t
--					  LEFT JOIN OpportunityBizDocs as BC
--					  ON t.numOppId=BC.numOppId
--LEFT JOIN Item as I
--ON t.numItemCode=I.numItemCode
--WHERE t.numOppId=Opp.numOppId AND t.numoppItemtCode NOT IN (SELECT numOppItemId FROM OpportunityBizDocItems WHERE numOppBizDocID=BC.numOppBizDocsId)
--                     FOR XML PATH('''')), 2, 200000)  )[List_Item_Approval_UNIT] '
--SET @strColumns = @strColumns + ' , (SELECT 
-- SUBSTRING((SELECT '', '' + cast(I.vcItemName as varchar)
--FROM 
-- OpportunityItems AS t
--LEFT JOIN 
-- Item as I
--ON 
-- t.numItemCode=I.numItemCode
--WHERE 
-- t.numOppId=Opp.numOppId AND t.numoppItemtCode NOT IN (SELECT numOppItemId FROM OpportunityBizDocs JOIN OpportunityBizDocItems ON OpportunityBizDocs.numOppBizDocsId=OpportunityBizDocItems.numOppBizDocID  WHERE OpportunityBizDocs.numOppId=Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1)
--                     FOR XML PATH('''')), 2, 200000)  )[List_Item_Approval_UNIT] '
	WHILE @tintOrder > 0
    BEGIN
		IF @bitCustom = 0
		BEGIN
			DECLARE  @Prefix  AS VARCHAR(5)

			IF @vcLookBackTableName = 'AdditionalContactsInformation'
				SET @Prefix = 'ADC.'
			IF @vcLookBackTableName = 'DivisionMaster'
				SET @Prefix = 'Div.'
			IF @vcLookBackTableName = 'OpportunityMaster'
				SET @PreFix = 'Opp.'
			IF @vcLookBackTableName = 'OpportunityRecurring'
				SET @PreFix = 'OPR.'
			IF @vcLookBackTableName = 'CompanyInfo'
				SET @PreFix = 'cmp.'
			IF @vcLookBackTableName = 'ProjectProgress'
				SET @PreFix = 'PP'
			SET @vcColumnName= @vcDbColumnName+'~'+ convert(varchar(10),@numFieldId) +'~0'

			IF @vcAssociatedControlType = 'SelectBox'
			BEGIN
				IF @vcDbColumnName = 'numPartner'
				BEGIN
					SET @strColumns=@strColumns+ ' ,(Opp.numPartner) '+' ['+ @vcColumnName+']'    
					SET @strColumns=@strColumns+ ' ,(SELECT D.vcPartnerCode+''-''+C.vcCompanyName FROM DivisionMaster AS D LEFT JOIN CompanyInfo AS C ON D.numCompanyID=C.numCompanyID WHERE D.numDivisionID=Opp.numPartner) AS vcPartner' 
				END

				IF @vcDbColumnName = 'tintEDIStatus'
				BEGIN
					SET @strColumns = @strColumns + ',(CASE Opp.tintEDIStatus WHEN 1 THEN ''850 Received'' WHEN 2 THEN ''850 Acknowledged'' WHEN 3 THEN ''940 Sent'' WHEN 4 THEN ''940 Acknowledged'' WHEN 5 THEN ''856 Sent'' WHEN 6 THEN ''945 Acknowledged'' ELSE '''' END)' + ' [' + @vcColumnName + ']'
				END
				
				IF @vcDbColumnName = 'numPartenerContact'
				BEGIN
					SET @strColumns=@strColumns+ ' ,(Opp.numPartenerContact) '+' ['+ @vcColumnName+']'    
					SET @strColumns=@strColumns+ ' ,(SELECT A.vcFirstName+'' ''+ A.vcLastName FROM AdditionalContactsInformation AS A WHERE A.numContactId=Opp.numPartenerContact) AS vcPartenerContact' 
				END
				ELSE IF @vcDbColumnName = 'tintInvoicing'
				BEGIN
					SET @strColumns=CONCAT(@strColumns,', CONCAT(ISNULL((SELECT SUBSTRING((SELECT ''$^$'' + CONCAT(numOppBizDocsId,''#^#'',vcBizDocID,''#^#'',ISNULL(bitisemailsent,0),''#^#'')
										FROM OpportunityBizDocs WHERE numOppId=Opp.numOppId AND numBizDocId = ISNULL((SELECT TOP 1 numAuthoritativeSales FROM AuthoritativeBizDocs WHERE numDomainId=' + CAST (@numDomainID AS VARCHAR) + '),287) FOR XML PATH('''')),4,200000)),'''')',
										CASE 
										WHEN CHARINDEX('tintInvoicing=3',@vcRegularSearchCriteria) > 0
											THEN ', (SELECT 
													CONCAT(''('',SUM(X.InvoicedQty),'' Out of '',SUM(X.OrderedQty),'')'')
												FROM 
												(
													SELECT
														OI.numoppitemtCode,
														ISNULL(OI.numUnitHour,0) AS OrderedQty,
														ISNULL(TempInvoice.InvoicedQty,0) AS InvoicedQty
													FROM
														OpportunityItems OI
													INNER JOIN
														Item I
													ON
														OI.numItemCode = I.numItemCode
													OUTER APPLY
													(
														SELECT
															SUM(OpportunityBizDocItems.numUnitHour) AS InvoicedQty
														FROM
															OpportunityBizDocs
														INNER JOIN
															OpportunityBizDocItems 
														ON
															OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
														WHERE
															OpportunityBizDocs.numOppId = Opp.numOppId
															AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
															AND ISNULL(OpportunityBizDocs.bitAuthoritativeBizDocs,0) = 1
													) AS TempInvoice
													WHERE
														OI.numOppID = Opp.numOppId
												) X)'

											WHEN CHARINDEX('tintInvoicing=4',@vcRegularSearchCriteria) > 0
											THEN ', (SELECT 
													CONCAT(''('',(X.InvoicedPercentage),'' % '','')'')
												FROM 
												(

													SELECT fltBreakupPercentage  AS InvoicedPercentage
													FROM dbo.OpportunityRecurring OPR 
														INNER JOIN dbo.OpportunityMaster OM ON OPR.numOppID = OM.numOppID
														LEFT JOIN dbo.OpportunityBizDocs OBD ON OBD.numOppBizDocsID = OPR.numOppBizDocID
													WHERE OPR.numOppId = Opp.numOppId 
														and tintRecurringType <> 4
												) X)'

											ELSE 
													','''''
											END,') [', @vcColumnName,']')


				END
				IF @vcDbColumnName = 'numStatus'
				BEGIN
					SET @strColumns=@strColumns+ ' ,(SELECT COUNT(OpportunityItems.numoppitemtCode) FROM OpportunityItems WHERE OpportunityItems.bitItemPriceApprovalRequired=1 AND OpportunityItems.numOppId=Opp.numOppId) ApprovalMarginCount'
				END
				
				IF @vcDbColumnName = 'tintSource'
				BEGIN
					SET @strColumns = @strColumns + ',ISNULL(dbo.fn_GetOpportunitySourceValue(ISNULL(Opp.tintSource,0),ISNULL(Opp.tintSourceType,0),Opp.numDomainID),''Internal Order'')' + ' [' + @vcColumnName + ']'

					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'ISNULL(dbo.fn_GetOpportunitySourceValue(ISNULL(Opp.tintSource,0),ISNULL(Opp.tintSourceType,0),Opp.numDomainID),''Internal Order'') LIKE ''%' + @SearchText + '%'''
					END
				END

				ELSE IF @vcDbColumnName = 'numCampainID'
				BEGIN
					SET @strColumns = @strColumns + ', (SELECT vcCampaignName FROM CampaignMaster WHERE numCampaignID= Opp.numCampainID) ' + ' [' + @vcColumnName + ']'
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'ISNULL((SELECT vcCampaignName FROM CampaignMaster WHERE numCampaignID= Opp.numCampainID),'''') LIKE ''%' + @SearchText + '%'''
					END
				END

				ELSE IF @vcDbColumnName = 'numContactID'
				BEGIN
					SET @strColumns = @strColumns + ',dbo.fn_GetContactName(' + @Prefix + @vcDbColumnName + ') [' + @vcColumnName + ']'
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'dbo.fn_GetContactName(' + @Prefix + @vcDbColumnName + ') LIKE ''%' + @SearchText + '%'''
					END
				END
				ELSE IF @vcListItemType = 'LI'
				BEGIN
					SET @strColumns = @strColumns + ',L' + CONVERT(VARCHAR(3),@tintOrder) + '.vcData' + ' [' + @vcColumnName + ']'
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'L' + CONVERT(VARCHAR(3),@tintOrder) + '.vcData LIKE ''%' + @SearchText + '%'''
					END

					SET @WhereCondition = @WhereCondition + ' left Join ListDetails L' + CONVERT(VARCHAR(3),@tintOrder) + ' on L' + CONVERT(VARCHAR(3),@tintOrder) + '.numListItemID=' + @vcDbColumnName
				END
				ELSE IF @vcListItemType = 'S'
				BEGIN
					SET @strColumns = @strColumns + ',S' + CONVERT(VARCHAR(3),@tintOrder) + '.vcState' + ' [' + @vcColumnName + ']'
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'S' + CONVERT(VARCHAR(3),@tintOrder) + '.vcState LIKE ''%' + @SearchText + '%'''
					END

					SET @WhereCondition = @WhereCondition + ' left join State S' + CONVERT(VARCHAR(3),@tintOrder) + ' on S' + CONVERT(VARCHAR(3),@tintOrder) + '.numStateID=' + @vcDbColumnName
				END
				ELSE IF @vcListItemType = 'BP'
				BEGIN
					SET @strColumns = @strColumns + ',SPLM.Slp_Name' + ' [' + @vcColumnName + ']'
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'SPLM.Slp_Name LIKE ''%' + @SearchText + '%'''
					END

					SET @WhereCondition = @WhereCondition + ' LEFT JOIN Sales_process_List_Master SPLM on SPLM.Slp_Id = OPP.numBusinessProcessID '
				END
				ELSE IF @vcListItemType = 'PP'
				BEGIN
					SET @strColumns = @strColumns + ',ISNULL(PP.intTotalProgress,0)' + ' [' + @vcColumnName + ']'

					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'ISNULL(PP.intTotalProgress,0) LIKE ''%' + @SearchText + '%'''
					END
				END
				ELSE IF @vcListItemType = 'T'
				BEGIN
					SET @strColumns = @strColumns + ',L' + CONVERT(VARCHAR(3),@tintOrder) + '.vcData' + ' [' + @vcColumnName + ']'
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'L' + CONVERT(VARCHAR(3),@tintOrder) + '.vcData LIKE ''%' + @SearchText + '%'''
					END

					SET @WhereCondition = @WhereCondition + ' left Join ListDetails L' + CONVERT(VARCHAR(3),@tintOrder) + ' on L' + CONVERT(VARCHAR(3),@tintOrder) + '.numListItemID=' + @vcDbColumnName
				END
				ELSE IF @vcListItemType = 'U'
				BEGIN
					SET @strColumns = @strColumns + ',dbo.fn_GetContactName(' + @Prefix + @vcDbColumnName + ') [' + @vcColumnName + ']'
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'dbo.fn_GetContactName(' + @Prefix + @vcDbColumnName + ') LIKE ''%' + @SearchText + '%'''
					END
				END
				ELSE IF @vcListItemType = 'WI'
				BEGIN
					SET @strColumns = @strColumns + ', Case when isnull(OL.numSiteID,0)>0 then Sites.vcSiteName else WebAPI.vcProviderName end [' + @vcColumnName + ']'
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + '(Case when isnull(OL.numSiteID,0)>0 then Sites.vcSiteName else WebAPI.vcProviderName end) LIKE ''%' + @SearchText + '%'''
					END

					set @WhereCondition= @WhereCondition +' left JOIN OpportunityLinking OL on OL.numChildOppID=Opp.numOppId left join WebAPI on WebAPI.WebApiId=isnull(OL.numWebApiId,0)
													left join Sites on isnull(OL.numSiteID,0)=Sites.numSIteID and Sites.numDomainID=' + convert(varchar(15),@numDomainID)                                                       
				END 
            END
			ELSE IF @vcAssociatedControlType = 'DateField'
			BEGIN
				IF @Prefix ='OPR.'
				BEGIN
					SET @strColumns = @strColumns + ', (SELECT TOP 1 dbo.FormatedDateFromDate( DateAdd(minute,+'+convert(varchar(15),-@ClientTimeZoneOffset)+', dtRecurringDate)'+ ','+ CONVERT(VARCHAR(10),@numDomainId) +') FROM  dbo.OpportunityRecurring WHERE numOppID=OPP.numOppID AND ISNULL(numOppBizDocID,0)=0 and dtRecurringDate > dbo.GetUTCDateWithoutTime()) AS  ['+ @vcColumnName + ']'

					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + '(SELECT TOP 1 dbo.FormatedDateFromDate( DateAdd(minute,+'+convert(varchar(15),-@ClientTimeZoneOffset)+', dtRecurringDate)'+ ','+ CONVERT(VARCHAR(10),@numDomainId) +') FROM  dbo.OpportunityRecurring WHERE numOppID=OPP.numOppID AND ISNULL(numOppBizDocID,0)=0 and dtRecurringDate > dbo.GetUTCDateWithoutTime()) LIKE ''%' + @SearchText + '%'''
					END
				END
				ELSE IF @vcDbColumnName = 'dtReleaseDate'
				BEGIN
					SET @strColumns = @strColumns
                                + ',case when convert(varchar(11),'
                                + @Prefix
                                + @vcDbColumnName
                                + ')= convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',getdate())) then ''<b><font color=red>Today</font></b>'''
					SET @strColumns = @strColumns
									+ 'when convert(varchar(11),'
									+ @Prefix
									+ @vcDbColumnName
									+ ')= convert(varchar(11),dateadd(day,-1,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',getdate()))) then ''<b><font color=purple>Yesterday</font></b>'''
					SET @strColumns = @strColumns
									+ 'when convert(varchar(11),'
									+ @Prefix
									+ @vcDbColumnName
									+ ')= convert(varchar(11),dateadd(day,1,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',getdate()))) then''<b><font color=orange>Tommorow</font></b>'' '
					SET @strColumns = @strColumns
									+ 'else dbo.FormatedDateFromDate('
									+ @Prefix
									+ @vcDbColumnName
									+ ','
									+ CONVERT(VARCHAR(10),@numDomainId)
									+ ') end  ['
									+ @vcColumnName + ']' 	

					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + @Prefix + @vcDbColumnName + ' LIKE ''%' + @SearchText + '%'''
					END

				END
				ELSE
				BEGIN
					SET @strColumns = @strColumns
                                + ',case when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+','
                                + @Prefix
                                + @vcDbColumnName
                                + ') )= convert(varchar(11),getdate()) then ''<b><font color=red>Today</font></b>'''
					SET @strColumns = @strColumns
									+ 'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+','
									+ @Prefix
									+ @vcDbColumnName
									+ ') )= convert(varchar(11),dateadd(day,-1,getdate())) then ''<b><font color=purple>Yesterday</font></b>'''
					SET @strColumns = @strColumns
									+ 'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+','
									+ @Prefix
									+ @vcDbColumnName
									+ ') )= convert(varchar(11),dateadd(day,1,getdate())) then''<b><font color=orange>Tommorow</font></b>'' '
					SET @strColumns = @strColumns
									+ 'else dbo.FormatedDateFromDate('
									+ @Prefix
									+ @vcDbColumnName
									+ ','
									+ CONVERT(VARCHAR(10),@numDomainId)
									+ ') end  ['
									+ @vcColumnName + ']' 	

					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + @Prefix + @vcDbColumnName + ' LIKE ''%' + @SearchText + '%'''
					END
				 END	
            END
            ELSE
            IF @vcAssociatedControlType = 'TextBox'
            BEGIN
				
                SET @strColumns = @strColumns
                                  + ','
                                  + CASE 
                                      WHEN @vcDbColumnName = 'numAge' THEN 'year(getutcdate())-year(bintDOB)'
                                      WHEN @vcDbColumnName = 'monPAmount' THEN 'isnull((SELECT varCurrSymbol FROM [Currency] WHERE numCurrencyID = Opp.[numCurrencyID]),'''') + '' ''+ CONVERT(VARCHAR,CONVERT(MONEY,monPAmount),1)'
                                      WHEN @vcDbColumnName = 'numDivisionID' THEN 'Div.numDivisionID'
                                      WHEN @vcDbColumnName = 'tintOppStatus' THEN 'CASE tintOppStatus WHEN 0 then ''Open'' WHEN 1 THEN ''Closed-Won'' WHEN 2 THEN ''Closed-Lost'' END '
                                      WHEN @vcDbColumnName = 'tintOppType' THEN 'CASE tintOppType WHEN 1 then ''Sales'' WHEN 2 THEN ''Purchase'' Else ''NA'' END '
									  WHEN @vcDbColumnName = 'vcPOppName' THEN 'dbo.CheckChildRecord(Opp.numOppId,Opp.numDomainID)'
									  WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'vcBizDocsList' THEN 'ISNULL((SELECT SUBSTRING((SELECT ''$^$'' + CAST(numOppBizDocsId AS VARCHAR(18)) +''#^#''+ vcBizDocID
										FROM OpportunityBizDocs WHERE numOppId=Opp.numOppId AND 1 = (Case When bitAuthoritativeBizDocs=1 Or numBizDocID=293 Then 0 ELSE 1 END) AND numBizDocId NOT IN (296,ISNULL((SELECT TOP 1 numAuthoritativeSales FROM AuthoritativeBizDocs WHERE numDomainId=' + CAST (@numDomainID AS VARCHAR) + '),287)) FOR XML PATH('''')),4,200000)),'''')'
                                      ELSE @Prefix + @vcDbColumnName
                                    END
                                  + ' ['
                                  + @vcColumnName + ']'
				IF(@vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'vcBizDocsList')
				BEGIN
					SET @strColumns=@strColumns+','+' (SELECT COUNT(I.vcItemName) FROM 
													 OpportunityItems AS t LEFT JOIN  Item as I ON 
													 t.numItemCode=I.numItemCode WHERE 
													 t.numOppId=Opp.numOppId AND t.numoppItemtCode NOT IN 
													 (SELECT numOppItemId FROM OpportunityBizDocs JOIN 
													 OpportunityBizDocItems ON OpportunityBizDocs.numOppBizDocsId=OpportunityBizDocItems.numOppBizDocID  
													 WHERE OpportunityBizDocs.numOppId=Opp.numOppId 
													 AND 1 = (Case When bitAuthoritativeBizDocs=1 Or numBizDocID=293 Then 0 ELSE 1 END) 
													 AND ISNULL(bitAuthoritativeBizDocs,0)=1)) AS [List_Item_Approval_UNIT] '
				END
				
				IF LEN(@SearchText) > 0
				BEGIN
					SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + (CASE 
                                      WHEN @vcDbColumnName = 'numAge' THEN 'year(getutcdate())-year(bintDOB)'
                                      WHEN @vcDbColumnName = 'monPAmount' THEN 'isnull((SELECT varCurrSymbol FROM [Currency] WHERE numCurrencyID = Opp.[numCurrencyID]),'''') + '' ''+ CONVERT(VARCHAR,CONVERT(MONEY,monPAmount),1)'
                                      WHEN @vcDbColumnName = 'numDivisionID' THEN 'Div.numDivisionID'
                                      WHEN @vcDbColumnName = 'tintOppStatus' THEN 'CASE tintOppStatus WHEN 0 then ''Open'' WHEN 1 THEN ''Closed-Won'' WHEN 2 THEN ''Closed-Lost'' END '
                                      WHEN @vcDbColumnName = 'tintOppType' THEN 'CASE tintOppType WHEN 1 then ''Sales'' WHEN 2 THEN ''Purchase'' Else ''NA'' END '
									  WHEN @vcDbColumnName = 'vcPOppName' THEN 'dbo.CheckChildRecord(Opp.numOppId,Opp.numDomainID)'
									  WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'vcBizDocsList' THEN 'ISNULL((SELECT SUBSTRING((SELECT ''$^$'' + CAST(numOppBizDocsId AS VARCHAR(18)) +''#^#''+ vcBizDocID
										FROM OpportunityBizDocs WHERE numOppId=Opp.numOppId AND 1 = (Case When bitAuthoritativeBizDocs=1 Or numBizDocID=293 Then 0 ELSE 1 END)  FOR XML PATH('''')),4,200000)),'''')'
                                      ELSE @Prefix + @vcDbColumnName
                                    END) + ' LIKE ''%' + @SearchText + '%'''
				END
            END
            ELSE
            IF @vcAssociatedControlType = 'TextArea'
            BEGIN
				SET @strColumns=@strColumns+','+  @Prefix + @vcDbColumnName +' ['+ @vcColumnName+']' 

				IF LEN(@SearchText) > 0
				BEGIN
					SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + @Prefix + @vcDbColumnName + ' LIKE ''%' + @SearchText + '%'''
				END
            END
            ELSE IF  @vcAssociatedControlType='Label'                                              
			BEGIN
				SET @strColumns=@strColumns + ',' + CASE                    
				WHEN @vcDbColumnName = 'CalAmount' THEN 'Opp.monDealAmount'--'[dbo].[getdealamount](Opp.numOppId,Getutcdate(),0)'
				WHEN @vcDbColumnName = 'vcInventoryStatus' THEN 'ISNULL(dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID),'''')'
				WHEN @vcDbColumnName = 'vcOrderedShipped' THEN 'dbo.GetOrderShipReceiveStatus(Opp.numOppId,1,Opp.tintshipped)'
				WHEN @vcDbColumnName = 'vcOrderedReceived' THEN 'dbo.GetOrderShipReceiveStatus(Opp.numOppId,2,Opp.tintshipped)'
				WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'monDealAmount' THEN 'ISNULL((SELECT SUM(ISNULL(monDealAmount,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)'
				WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'monAmountPaid' THEN 'ISNULL((SELECT SUM(ISNULL(monAmountPaid,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)'
				else @Prefix + @vcDbColumnName END +' ['+ @vcColumnName+']'        
				
				IF @vcDbColumnName='vcOrderedShipped'
				BEGIN
				DECLARE @temp VARCHAR(MAX)
				SET @temp = '(SELECT 
				STUFF((SELECT '', '' + CAST((CAST(numOppBizDocsId AS varchar)+''~''+CAST(ISNULL(numShippingReportId,0) AS varchar) ) AS VARCHAR(MAX)) [text()]
				FROM OpportunityBizDocs
				LEFT JOIN ShippingReport
				ON ShippingReport.numOppBizDocId=OpportunityBizDocs.numOppBizDocsId
				WHERE OpportunityBizDocs.numOppId=Opp.numOppId  AND OpportunityBizDocs.bitShippingGenerated=1  FOR XML PATH(''''), TYPE)
						.value(''.'',''NVARCHAR(MAX)''),1,2,'' ''))'
					SET @strColumns = @strColumns + ' , (SELECT SUBSTRING('+ @temp +', 2, 200000))AS ShippingIcons '
					SET @strColumns = @strColumns + ' , CASE WHEN (SELECT COUNT(*) FROM ShippingBox INNER JOIN ShippingReport ON ShippingBox.numShippingReportId = ShippingReport.numShippingReportId WHERE ShippingReport.numOppID = Opp.numOppID AND LEN(vcTrackingNumber) > 0) > 0 THEN 1 ELSE 0 END [ISTrackingNumGenerated]'
				END

				IF LEN(@SearchText) > 0
				BEGIN
					SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + (CASE                    
					WHEN @vcDbColumnName = 'CalAmount' THEN 'Opp.monDealAmount'
					WHEN @vcDbColumnName = 'vcInventoryStatus' THEN 'ISNULL(dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID),'''')'
					WHEN @vcDbColumnName = 'vcOrderedShipped' THEN 'dbo.GetOrderShipReceiveStatus(Opp.numOppId,1,Opp.tintshipped)'
					WHEN @vcDbColumnName = 'vcOrderedReceived' THEN 'dbo.GetOrderShipReceiveStatus(Opp.numOppId,2,Opp.tintshipped)'
					WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'monDealAmount' THEN 'ISNULL((SELECT SUM(ISNULL(monDealAmount,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)'
					WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'monAmountPaid' THEN 'ISNULL((SELECT SUM(ISNULL(monAmountPaid,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)'
					else @Prefix + @vcDbColumnName END) + ' LIKE ''%' + @SearchText + '%'''
				END    
			END
            ELSE IF @vcAssociatedControlType='Popup' and @vcDbColumnName='numShareWith'                                                   
			BEGIN
				SET @strColumns = @strColumns + ',(SELECT SUBSTRING(
								(SELECT '','' + A.vcFirstName+'' ''+A.vcLastName + CASE WHEN ISNULL(SR.numContactType,0)>0 THEN ''('' + dbo.fn_GetListItemName(SR.numContactType) + '')'' ELSE '''' END
								FROM ShareRecord SR JOIN UserMaster UM ON UM.numUserDetailId=SR.numAssignedTo 
								JOIN AdditionalContactsInformation A ON UM.numUserDetailId=A.numContactID 
								WHERE SR.numDomainID=Opp.numDomainID AND SR.numModuleID=3 AND SR.numRecordID=Opp.numOppId
								AND UM.numDomainID=Opp.numDomainID and UM.numDomainID=A.numDomainID FOR XML PATH('''')),2,200000)) ['+ @vcColumnName+']'                
			END 
        END
		ELSE
        BEGIN
			SET @vcColumnName= @vcDbColumnName +'~'+ convert(varchar(10),@numFieldId)+'~1'

			IF @vcAssociatedControlType = 'TextBox' or @vcAssociatedControlType = 'TextArea'
            BEGIN
              SET @strColumns = @strColumns
                              + ',CFW'
                              + CONVERT(VARCHAR(3),@tintOrder)
                              + '.Fld_Value  ['
                              + @vcColumnName + ']'
              SET @WhereCondition = @WhereCondition
                                      + ' left Join CFW_FLD_Values_Opp CFW'
                                      + CONVERT(VARCHAR(3),@tintOrder)
                                      + ' on CFW'
                                      + CONVERT(VARCHAR(3),@tintOrder)
                                      + '.Fld_Id='
                                      + CONVERT(VARCHAR(10),@numFieldId)
                                      + 'and CFW'
                                      + CONVERT(VARCHAR(3),@tintOrder)
                                      + '.RecId=Opp.numOppid   '
			END
			ELSE
            IF @vcAssociatedControlType = 'CheckBox' 
            BEGIN
                SET @strColumns = @strColumns
                                + ',case when isnull(CFW'
                                + CONVERT(VARCHAR(3),@tintOrder)
                                + '.Fld_Value,0)=0 then 0 when isnull(CFW'
                                + CONVERT(VARCHAR(3),@tintOrder)
                                + '.Fld_Value,0)=1 then 1 end   ['
                                + @vcColumnName + ']'
                SET @WhereCondition = @WhereCondition
                                        + ' left Join CFW_FLD_Values_Opp CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + ' on CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.Fld_Id='
                                        + CONVERT(VARCHAR(10),@numFieldId)
                                        + 'and CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.RecId=Opp.numOppid   '
            END
            ELSE
            IF @vcAssociatedControlType = 'DateField'
            BEGIN
                  SET @strColumns = @strColumns
                                  + ',dbo.FormatedDateFromDate(CFW'
                                  + CONVERT(VARCHAR(3),@tintOrder)
                                  + '.Fld_Value,'
                                  + CONVERT(VARCHAR(10),@numDomainId)
                                  + ')  ['
                                  + @vcColumnName + ']'
                  SET @WhereCondition = @WhereCondition
                                          + ' left Join CFW_FLD_Values_Opp CFW'
                                          + CONVERT(VARCHAR(3),@tintOrder)
                                          + ' on CFW'
                                          + CONVERT(VARCHAR(3),@tintOrder)
                                          + '.Fld_Id='
                                          + CONVERT(VARCHAR(10),@numFieldId)
                                          + 'and CFW'
                                          + CONVERT(VARCHAR(3),@tintOrder)
                                          + '.RecId=Opp.numOppid    '
            END
            ELSE
            IF @vcAssociatedControlType = 'SelectBox'
            BEGIN
                SET @vcDbColumnName = 'DCust'
                                            + CONVERT(VARCHAR(10),@numFieldId)
                SET @strColumns = @strColumns
                                + ',L'
                                + CONVERT(VARCHAR(3),@tintOrder)
                                + '.vcData'
                                + ' ['
                                + @vcColumnName + ']'

                SET @WhereCondition = @WhereCondition
                                        + ' left Join CFW_FLD_Values_Opp CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + ' on CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.Fld_Id='
                                        + CONVERT(VARCHAR(10),@numFieldId)
                                        + 'and CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.RecId=Opp.numOppid '

                SET @WhereCondition = @WhereCondition
                                        + ' left Join ListDetails L'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + ' on L'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.numListItemID=CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.Fld_Value'
            END
			ELSE
			BEGIN
				SET @strColumns = @strColumns + CONCAT(',dbo.GetCustFldValueOpp(',@numFieldId,',opp.numOppID)') + ' [' + @vcColumnName + ']'
			END

			IF LEN(@SearchText) > 0
			BEGIN
				SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + CONCAT('dbo.GetCustFldValueOpp(',@numFieldId,',opp.numOppID) LIKE ''%',@SearchText,'%''')
			END
		END
      
     
		SELECT TOP 1 
			@tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName,@vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,
			@numListID=numListID,@vcLookBackTableName=vcLookBackTableName,@bitCustom=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,@bitAllowEdit=bitAllowEdit,
			@ListRelID=ListRelID
		FROM 
			#tempForm 
		WHERE 
			tintOrder > @tintOrder-1 
		ORDER BY 
			tintOrder asc            
 
		IF @@rowcount=0 SET @tintOrder=0 
	END 

	

	DECLARE @strShareRedordWith AS VARCHAR(300);
	SET @strShareRedordWith=' Opp.numOppId in (select SR.numRecordID FROM ShareRecord SR WHERE SR.numDomainID='+ convert(varchar(15),@numDomainID )+' AND SR.numModuleID=3 AND SR.numAssignedTo='+convert(varchar(15),@numUserCntId)+') '


	DECLARE @StrSql AS VARCHAR(MAX) = ''

	SET @StrSql = @StrSql + ' FROM OpportunityMaster Opp            
								 ##PLACEHOLDER##                                                   
                                 LEFT JOIN AdditionalContactsInformation ADC ON Opp.numContactId = ADC.numContactId                                                               
                                 INNER JOIN DivisionMaster Div ON Opp.numDivisionId = Div.numDivisionID 
								 INNER JOIN CompanyInfo cmp ON Div.numCompanyID = cmp.numCompanyId 
								 LEFT JOIN ProjectProgress PP On PP.numOppId = Opp.numOppId ' + @WhereCondition
								 --INNER JOIN Correspondence Corr ON opp.numOppId = Corr.numOpenRecordID 
	-------Change Row Color-------
	
	DECLARE @vcCSOrigDbCOlumnName AS VARCHAR(50) = ''
	DECLARE @vcCSLookBackTableName AS VARCHAR(50) = ''
	DECLARE @vcCSAssociatedControlType AS VARCHAR(50)

	CREATE TABLE #tempColorScheme
	(
		vcOrigDbCOlumnName VARCHAR(50),
		vcLookBackTableName VARCHAR(50),
		vcFieldValue VARCHAR(50),
		vcFieldValue1 VARCHAR(50),
		vcColorScheme VARCHAR(50),
		vcAssociatedControlType varchar(50)
	)

	INSERT INTO 
		#tempColorScheme 
	SELECT 
		DFM.vcOrigDbCOlumnName,
		DFM.vcLookBackTableName,
		DFCS.vcFieldValue,
		DFCS.vcFieldValue1,
		DFCS.vcColorScheme,
		DFFM.vcAssociatedControlType 
	FROM 
		DycFieldColorScheme DFCS 
	JOIN 
		DycFormField_Mapping DFFM 
	ON 
		DFCS.numFieldID=DFFM.numFieldID
	JOIN 
		DycFieldMaster DFM 
	ON 
		DFM.numModuleID=DFFM.numModuleID and DFM.numFieldID=DFFM.numFieldID
	WHERE
		DFCS.numDomainID=@numDomainID 
		AND DFFM.numFormID=@numFormId 
		AND DFCS.numFormID=@numFormId 
		AND isnull(DFFM.bitAllowGridColor,0)=1

	IF(SELECT COUNT(*) FROM #tempColorScheme)>0
	BEGIN
		SELECT TOP 1 @vcCSOrigDbCOlumnName=vcOrigDbCOlumnName,@vcCSLookBackTableName=vcLookBackTableName,@vcCSAssociatedControlType=vcAssociatedControlType from #tempColorScheme
	END                        

	IF len(@vcCSOrigDbCOlumnName)>0 and len(@vcCSLookBackTableName)>0
	BEGIN
		SET @strColumns=@strColumns + ',tCS.vcColorScheme'
	END

	IF len(@vcCSOrigDbCOlumnName)>0 and len(@vcCSLookBackTableName)>0
	BEGIN
		IF @vcCSLookBackTableName = 'AdditionalContactsInformation'                  
			SET @Prefix = 'ADC.'                  
		IF @vcCSLookBackTableName = 'DivisionMaster'                  
			SET @Prefix = 'Div.'
		if @vcCSLookBackTableName = 'CompanyInfo'                  
			SET @Prefix = 'CMP.'   
		if @vcCSLookBackTableName = 'OpportunityMaster'                  
			SET @Prefix = 'Opp.'   
 
		IF @vcCSAssociatedControlType='DateField'
		BEGIN
			SET @strSql=@strSql+' LEFT JOIN #tempColorScheme tCS ON CONVERT(VARCHAR(10),' + @Prefix + @vcCSOrigDbCOlumnName + ',111) >= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue as int),GETUTCDATE()),111)
				 and CONVERT(VARCHAR(10),' + @Prefix + @vcCSOrigDbCOlumnName + ',111) <= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue1 as int),GETUTCDATE()),111)'
		END
		ELSE IF @vcCSAssociatedControlType='SelectBox' OR @vcCSAssociatedControlType='ListBox' OR @vcCSAssociatedControlType='CheckBox'
		BEGIN
			SET @strSql=@strSql+' LEFT JOIN #tempColorScheme tCS on tCS.vcFieldValue=' + @Prefix + @vcCSOrigDbCOlumnName
		END
	END
                          
	IF @columnName like 'CFW.Cust%'             
	BEGIN            
		SET @strSql = @strSql + ' LEFT JOIN CFW_Fld_Values_Opp CFW on CFW.RecId=Opp.numOppid and CFW.fld_id= '+ REPLACE(@columnName,'CFW.Cust','') +' '                                                     
		SET @columnName='CFW.Fld_Value'            
	END 

	IF @bitPartner = 1
		SET @strSql = @strSql + ' LEFT JOIN OpportunityContact OppCont ON OppCont.numOppId=Opp.numOppId AND OppCont.bitPartner=1 AND OppCont.numContactId=' + CONVERT(VARCHAR(15),@numUserCntID)
                    
                
	IF @tintFilterBy = 1 --Partially Fulfilled Orders 
		SET @strSql = @strSql + ' LEFT JOIN AdditionalContactsInformation ADC1 ON ADC1.numContactId=Opp.numRecOwner                                                               
                             LEFT OUTER JOIN [OpportunityBizDocs] OB ON OB.[numOppId] = Opp.[numOppId] 
                             WHERE Opp.numDomainID=' + CONVERT(VARCHAR(15),@numDomainID) + ' and Opp.tintOppstatus=1 and (Opp.tintShipped=' + CONVERT(VARCHAR(15),@tintShipped) + ' OR ' + CONVERT(VARCHAR(15),@tintShipped) + '=2)'
	ELSE
		SET @strSql = @strSql + ' LEFT JOIN AdditionalContactsInformation ADC1 on ADC1.numContactId=Opp.numRecOwner                                                               
                             WHERE Opp.numDomainID=' + CONVERT(VARCHAR(15),@numDomainID) + ' and Opp.tintOppstatus=1 and (Opp.tintShipped='+ CONVERT(VARCHAR(15),@tintShipped)  + ' OR ' + CONVERT(VARCHAR(15),@tintShipped) + '=2)'
  
  
	IF @intOrder <> 0
    BEGIN
      IF @intOrder = 1
        SET @strSql = @strSql + ' and Opp.bitOrder = 0 '
      IF @intOrder = 2
        SET @strSql = @strSql + ' and Opp.bitOrder = 1'
    END
    
	IF @numCompanyID <> 0
		SET @strSql = @strSql + ' And Div.numCompanyID ='+ CONVERT(VARCHAR(15),@numCompanyID)

	IF @SortChar <> '0' 
		SET @strSql = @strSql + ' And Opp.vcPOppName like ''' + @SortChar + '%'''
  

	IF @inttype = 3
	BEGIN
		IF @tintSalesUserRightType = 0
			SET @strSql = @strSql + ' AND opp.tintOppType=0'
		ELSE IF @tintSalesUserRightType = 1
			SET @strSql = @strSql + ' AND (Opp.numRecOwner= ' + CONVERT(VARCHAR(15),@numUserCntID)
						+ ' or Opp.numAssignedTo= '+ CONVERT(VARCHAR(15),@numUserCntID)
						+ CASE WHEN @bitPartner = 1 THEN ' or ( OppCont.bitPartner=1 and OppCont.numContactId='+ CONVERT(VARCHAR(15),@numUserCntID) + ')' ELSE '' END
						+ ' or ' + @strShareRedordWith +')'
		ELSE IF @tintSalesUserRightType = 2
			SET @strSql = @strSql + ' AND (Div.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+ CONVERT(VARCHAR(15),@numUserCntID) + ' ) 
									 or Opp.numAssignedTo= '+ CONVERT(VARCHAR(15),@numUserCntID)
									+ ' or ' + @strShareRedordWith +')'
	END
	ELSE IF @inttype = 4
	BEGIN
		IF @tintPurchaseUserRightType = 0
			SET @strSql = @strSql + ' AND opp.tintOppType=0'
		ELSE IF @tintPurchaseUserRightType = 1
			SET @strSql = @strSql + ' AND (Opp.numRecOwner= ' + CONVERT(VARCHAR(15),@numUserCntID)
						+ ' or Opp.numAssignedTo= '+ CONVERT(VARCHAR(15),@numUserCntID)
						+ CASE WHEN @bitPartner = 1 THEN ' or ( OppCont.bitPartner=1 and OppCont.numContactId='+ CONVERT(VARCHAR(15),@numUserCntID) + ')' ELSE '' END
						+ ' or ' + @strShareRedordWith +')'
		ELSE IF @tintPurchaseUserRightType = 2
			SET @strSql = @strSql + ' AND (Div.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+ CONVERT(VARCHAR(15),@numUserCntID) + ' ) 
									 or Opp.numAssignedTo= '+ CONVERT(VARCHAR(15),@numUserCntID)
									+ ' or ' + @strShareRedordWith +')'
	END

	
					
	IF @tintSortOrder <> '0'
		SET @strSql = @strSql + '  AND Opp.tintOppType= '+ CONVERT(VARCHAR(1),@tintSortOrder)
    
    
	IF @tintFilterBy = 1 --Partially Fulfilled Orders
		SET @strSql = @strSql + ' AND OB.bitPartialFulfilment = 1 AND OB.bitAuthoritativeBizDocs = 1 '
	ELSE IF @tintFilterBy = 2 --Fulfilled Orders
		SET @strSql = @strSql + ' AND Opp.[numOppId] IN (SELECT OM.numOppId FROM [OpportunityMaster] OM
                      			INNER JOIN [OpportunityBizDocs] OB ON OB.[numOppId] = OM.[numOppId]
                      			INNER JOIN [OpportunityItems] OI ON OM.[numOppId] = OI.[numOppId]
                      			INNER JOIN [OpportunityBizDocItems] BI ON BI.[numOppBizDocID] = OB.[numOppBizDocsId]
                      			Where OM.numDomainId=' + CONVERT(VARCHAR(15),@numDomainId) + 'AND OB.[bitAuthoritativeBizDocs] = 1 GROUP BY OM.[numOppId]
                      			HAVING   COUNT(OI.[numUnitHour]) = COUNT(BI.[numUnitHour])) '
	ELSE IF @tintFilterBy = 3 --Orders without Authoritative-BizDocs
		SET @strSql = @strSql + ' AND Opp.[numOppId] not IN (SELECT DISTINCT OM.[numOppId]
                      		  FROM [OpportunityMaster] OM JOIN [OpportunityBizDocs] OB ON OM.[numOppId] = OB.[numOppId]
                      		  Where OM.numDomainId=' + CONVERT(VARCHAR(15),@numDomainId) + ' AND OM.tintOppstatus=1 AND (OM.tintShipped=' + CONVERT(VARCHAR(15),@tintShipped) + ' OR ' + CONVERT(VARCHAR(15),@tintShipped) + '=2)' + ' AND OM.tintOppType= '+ CONVERT(VARCHAR(1),@tintSortOrder) + ' AND isnull(OB.[bitAuthoritativeBizDocs],0)=1) '
	ELSE IF @tintFilterBy = 4 --Orders with items yet to be added to Invoices
		SET @strSql = @strSql + ' AND Opp.[numOppId] IN (SELECT distinct OPPM.numOppId FROM [OpportunityMaster] OPPM
                                  INNER JOIN [OpportunityItems] OPPI ON OPPM.[numOppId] = OPPI.[numOppId]
                                  Where OPPM.numDomainId=' + CONVERT(VARCHAR(15),@numDomainId) + ' 
								  and OPPI.numoppitemtCode not in (select numOppItemID from [OpportunityBizDocs] OB INNER JOIN [OpportunityBizDocItems] BI
                      			  ON BI.[numOppBizDocID] = OB.[numOppBizDocsId] where OB.[numOppId] = OPPM.[numOppId]))'      
	ELSE IF @tintFilterBy = 6 --Orders with deferred Income/Invoices
		SET @strSql = @strSql + ' AND Opp.[numOppId] IN (SELECT DISTINCT OM.[numOppId]
                      		 FROM [OpportunityMaster] OM LEFT OUTER JOIN [OpportunityBizDocs] OB ON OM.[numOppId] = OB.[numOppId]
                      		 Where OM.numDomainId=' + CONVERT(VARCHAR(15),@numDomainId) + ' AND OB.[bitAuthoritativeBizDocs] =1 and OB.tintDeferred=1)'

	IF @numOrderStatus <>0 
		SET @strSql = @strSql + ' AND Opp.numStatus = ' + CONVERT(VARCHAR(15),@numOrderStatus);

	IF CHARINDEX('Opp.vcInventoryStatus',@vcRegularSearchCriteria) > 0
	BEGIN
		IF CHARINDEX('Opp.vcInventoryStatus=1',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.vcInventoryStatus=1',' CHARINDEX(''Not Applicable'', dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID)) > 0 ')
		IF CHARINDEX('Opp.vcInventoryStatus=2',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.vcInventoryStatus=2',' CHARINDEX(''Allocation Cleared'', dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID)) > 0 ')
		IF CHARINDEX('Opp.vcInventoryStatus=3',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.vcInventoryStatus=3',' CHARINDEX(''Back Order'', dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID)) > 0 ')
		IF CHARINDEX('Opp.vcInventoryStatus=4',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.vcInventoryStatus=4',' CHARINDEX(''Ready to Ship'', dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID)) > 0 ')
	END
	
	IF CHARINDEX('Opp.tintInvoicing',@vcRegularSearchCriteria) > 0
	BEGIN
		IF CHARINDEX('Opp.tintInvoicing=0',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.tintInvoicing=0',' CHARINDEX(''All'', dbo.CheckOrderInvoicingStatus(Opp.numOppID,Opp.numDomainID)) > 0 ')
		IF CHARINDEX('Opp.tintInvoicing=1',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.tintInvoicing=1',' (SELECT (CASE WHEN SUM(ISNULL(InvoicedQty,0)) = SUM(ISNULL(OrderedQty,0)) THEN 1 ELSE 0 END)
	FROM 
	(
		SELECT OI.numoppitemtCode, ISNULL(OI.numUnitHour,0) AS OrderedQty, ISNULL(TempInvoice.InvoicedQty,0) AS InvoicedQty
		FROM OpportunityItems OI
		INNER JOIN Item I ON OI.numItemCode = I.numItemCode
		OUTER APPLY
		(
			SELECT SUM(OpportunityBizDocItems.numUnitHour) AS InvoicedQty
			FROM OpportunityBizDocs
				INNER JOIN OpportunityBizDocItems ON OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
			WHERE OpportunityBizDocs.numOppId = Opp.numOppID
				AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
				AND numBizDocId IN (SELECT numAuthoritativeSales FROM AuthoritativeBizDocs WHERE numDomainId = Opp.numDomainID)
		) AS TempInvoice
		WHERE
		OI.numOppID = Opp.numOppID
	) X) > 0 ')
		IF CHARINDEX('Opp.tintInvoicing=2',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.tintInvoicing=2',' (SELECT COUNT(*) 
		FROM OpportunityBizDocs 
		WHERE numOppId = Opp.numOppID 
			AND numBizDocId IN (SELECT numAuthoritativeSales FROM AuthoritativeBizDocs WHERE numDomainId = Opp.numDomainID)) = 0 ')
		IF CHARINDEX('Opp.tintInvoicing=3',@vcRegularSearchCriteria) > 0 
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.tintInvoicing=3',' (SELECT (CASE WHEN SUM(ISNULL(InvoicedQty,0)) > 0 THEN SUM(ISNULL(OrderedQty,0)) - SUM(ISNULL(InvoicedQty,0)) ELSE 0 END)
	FROM 
	(
		SELECT OI.numoppitemtCode, ISNULL(OI.numUnitHour,0) AS OrderedQty, ISNULL(TempInvoice.InvoicedQty,0) AS InvoicedQty
		FROM OpportunityItems OI
		INNER JOIN Item I ON OI.numItemCode = I.numItemCode
		OUTER APPLY
		(
			SELECT SUM(OpportunityBizDocItems.numUnitHour) AS InvoicedQty
			FROM OpportunityBizDocs
				INNER JOIN OpportunityBizDocItems ON OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
			WHERE OpportunityBizDocs.numOppId = Opp.numOppID
				AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
				AND numBizDocId IN (SELECT numAuthoritativeSales FROM AuthoritativeBizDocs WHERE numDomainId = Opp.numDomainID)
		) AS TempInvoice
		WHERE
		OI.numOppID = Opp.numOppID
	) X) > 0 ')
		
		IF CHARINDEX('Opp.tintInvoicing=4',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.tintInvoicing=4',' (SELECT (CASE WHEN SUM(ISNULL(InvoicedQty,0)) > 0 THEN SUM(ISNULL(OrderedQty,0)) - SUM(ISNULL(InvoicedQty,0)) ELSE 0 END)
	FROM 
	(
		SELECT OI.numoppitemtCode, ISNULL(OI.numUnitHour,0) AS OrderedQty, ISNULL(TempInvoice.InvoicedQty,0) AS InvoicedQty
		FROM OpportunityItems OI
		INNER JOIN Item I ON OI.numItemCode = I.numItemCode
		OUTER APPLY
		(
			SELECT SUM(OpportunityBizDocItems.numUnitHour) AS InvoicedQty
			FROM OpportunityBizDocs
				INNER JOIN OpportunityBizDocItems ON OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
			WHERE OpportunityBizDocs.numOppId = Opp.numOppID
				AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
				AND numBizDocId IN (SELECT numAuthoritativeSales FROM AuthoritativeBizDocs WHERE numDomainId = Opp.numDomainID)
		) AS TempInvoice
		WHERE
		OI.numOppID = Opp.numOppID
	) X) > 0 ')

		IF CHARINDEX('Opp.tintInvoicing=5',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.tintInvoicing=5',' 
				Opp.[numOppId] IN (SELECT DISTINCT OM.[numOppId]
                      		 FROM [OpportunityMaster] OM INNER JOIN [OpportunityBizDocs] OB ON OM.[numOppId] = OB.[numOppId]
                      		 Where OM.numDomainId = Opp.numDomainID AND OB.[bitAuthoritativeBizDocs] = 1 and OB.tintDeferred=1) ')
	END
	
	
	IF @vcRegularSearchCriteria<>'' 
	BEGIN
		SET @strSql=@strSql +' AND ' + @vcRegularSearchCriteria 
	END
	
	


	IF @vcCustomSearchCriteria<>'' 
	BEGIN
		SET @strSql = @strSql +' AND ' +  @vcCustomSearchCriteria
	END

	IF LEN(@SearchQuery) > 0
	BEGIN
		SET @strSql = @strSql + ' AND (' + @SearchQuery + ') '
	END

	IF ISNULL(@tintDashboardReminderType,0) = 19
	BEGIN
		SET @strSql = CONCAT(@strSql,' AND Opp.numOppID IN (','SELECT DISTINCT
																	OMInner.numOppID
																FROM 
																	StagePercentageDetails SPDInner
																INNER JOIN 
																	OpportunityMaster OMInner
																ON 
																	SPDInner.numOppID=OMInner.numOppId 
																WHERE 
																	SPDInner.numDomainId=',@numDOmainID,'
																	AND OMInner.numDomainId=',@numDOmainID,' 
																	AND OMInner.tintOppType = 1
																	AND ISNULL(OMInner.tintOppStatus,0) = 1
																	AND tinProgressPercentage <> 100',') ')

	END
	ELSE IF ISNULL(@tintDashboardReminderType,0) = 10
	BEGIN
		SET @strSql = CONCAT(@strSql,' AND Opp.numOppID IN (','SELECT DISTINCT
																	OMInner.numOppId
																FROM
																	OpportunityMaster OMInner
																INNER JOIN
																	OpportunityItems OIInner
																ON
																	OIInner.numOppId = OMInner.numOppID
																INNER JOIN
																	Item IInner
																ON
																	OIInner.numItemCode = IInner.numItemCode
																OUTER APPLY
																(
																	SELECT
																		SUM(OpportunityBizDocItems.numUnitHour) AS BilledQty
																	FROM
																		OpportunityBizDocs
																	INNER JOIN
																		OpportunityBizDocItems 
																	ON
																		OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																	WHERE
																		OpportunityBizDocs.numOppId = OMInner.numOppID
																		AND OpportunityBizDocItems.numOppItemID = OIInner.numoppitemtCode
																		AND OpportunityBizDocs.numBizDocId = (SELECT ISNULL(numAuthoritativePurchase,0) FROM AuthoritativeBizDocs WHERE numDomainId=',@numDomainID,')
																) AS TempBilled
																WHERE
																	OMInner.numDomainId = ',@numDomainID,'
																	AND ISNULL(OMInner.bitStockTransfer,0) = 0
																	AND OMInner.tintOppType = 2
																	AND OMInner.tintOppStatus  = 1
																	AND ISNULL(OIInner.numUnitHour,0) <> ISNULL(TempBilled.BilledQty,0)',') ')
	END
	ELSE IF ISNULL(@tintDashboardReminderType,0) = 11
	BEGIN
		SET @strSql = CONCAT(@strSql,' AND Opp.numOppID IN (','SELECT DISTINCT
																	OMInner.numOppId
																FROM
																	OpportunityMaster OMInner
																INNER JOIN
																	OpportunityItems OIInner
																ON
																	OIInner.numOppId = OMInner.numOppId
																INNER JOIN
																	Item IInner
																ON
																	OIInner.numItemCode = IInner.numItemCode
																OUTER APPLY
																(
																	SELECT
																		SUM(OpportunityBizDocItems.numUnitHour) AS PickPacked
																	FROM
																		OpportunityBizDocs
																	INNER JOIN
																		OpportunityBizDocItems 
																	ON
																		OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																	WHERE
																		OpportunityBizDocs.numOppId = OMInner.numOppId
																		AND ISNULL(OpportunityBizDocs.numBizDocId,0) = ',(SELECT ISNULL(numDefaultSalesShippingDoc,0) FROM Domain WHERE numDomainId=@numDomainID),'
																		AND OpportunityBizDocItems.numOppItemID = OIInner.numoppitemtCode
																) AS TempFulFilled
																WHERE
																	OMInner.numDomainId = ',@numDomainID,'
																	AND OMInner.tintOppType = 1
																	AND OMInner.tintOppStatus = 1
																	AND UPPER(IInner.charItemType) = ''P''
																	AND ISNULL(OMInner.tintshipped,0) = 0
																	AND ISNULL(OIInner.bitDropShip,0) = 0
																	AND ISNULL(OIInner.numUnitHour,0) <> ISNULL(TempFulFilled.PickPacked,0)',') ')
	END
	ELSE IF ISNULL(@tintDashboardReminderType,0) = 12
	BEGIN
		SET @strSql = CONCAT(@strSql,' AND Opp.numOppID IN (','SELECT DISTINCT
																	OMInner.numOppId
																FROM
																	OpportunityMaster OMInner
																INNER JOIN
																	OpportunityItems OIInner
																ON
																	OIInner.numOppId = OMInner.numOppId
																INNER JOIN
																	Item IInner
																ON
																	OIInner.numItemCode = IInner.numItemCode
																OUTER APPLY
																(
																	SELECT
																		SUM(OpportunityBizDocItems.numUnitHour) AS FulFilledQty
																	FROM
																		OpportunityBizDocs
																	INNER JOIN
																		OpportunityBizDocItems 
																	ON
																		OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																	WHERE
																		OpportunityBizDocs.numOppId = OMInner.numOppId
																		AND ISNULL(OpportunityBizDocs.numBizDocId,0) = 296
																		AND ISNULL(OpportunityBizDocs.bitFulFilled,0) = 1
																		AND OpportunityBizDocItems.numOppItemID = OIInner.numoppitemtCode
																) AS TempFulFilled
																WHERE
																	OMInner.numDomainId = ',@numDomainID,'
																	AND OMInner.tintOppType = 1
																	AND OMInner.tintOppStatus = 1
																	AND ISNULL(OMInner.tintshipped,0) = 0
																	AND UPPER(IInner.charItemType) = ''P''
																	AND ISNULL(OIInner.bitDropShip,0) = 0
																	AND ISNULL(OIInner.numUnitHour,0) <> ISNULL(TempFulFilled.FulFilledQty,0)',') ')
	END
	ELSE IF ISNULL(@tintDashboardReminderType,0) = 13
	BEGIN
		SET @strSql = CONCAT(@strSql,' AND Opp.numOppID IN (','SELECT DISTINCT
																	OMInner.numOppId
																FROM
																	OpportunityMaster OMInner
																INNER JOIN
																	OpportunityItems OIInner
																ON
																	OIInner.numOppId = OMInner.numOppID
																INNER JOIN
																	Item IInner
																ON
																	OIInner.numItemCode = IInner.numItemCode
																OUTER APPLY
																(
																	SELECT
																		SUM(OpportunityBizDocItems.numUnitHour) AS InvoicedQty
																	FROM
																		OpportunityBizDocs
																	INNER JOIN
																		OpportunityBizDocItems 
																	ON
																		OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																	WHERE
																		OpportunityBizDocs.numOppId = OMInner.numOppID
																		AND OpportunityBizDocItems.numOppItemID = OIInner.numoppitemtCode
																		AND OpportunityBizDocs.numBizDocId = ISNULL((SELECT TOP 1 numAuthoritativeSales FROM AuthoritativeBizDocs WHERE numDomainId=',@numDomainID,'),287)
																) AS TempInvoice
																WHERE
																	OMInner.numDomainId = ',@numDomainID,'
																	AND OMInner.tintOppType = 1
																	AND OMInner.tintOppStatus  = 1
																	AND ISNULL(OIInner.numUnitHour,0) <> ISNULL(TempInvoice.InvoicedQty,0)',') ')
	END
	ELSE IF ISNULL(@tintDashboardReminderType,0) = 14
	BEGIN
		DECLARE @TEMPOppID TABLE
		(
			numOppID NUMERIC(18,0)
		)

		INSERT INTO @TEMPOppID
		(
			numOppID
		)
		SELECT DISTINCT
			OM.numOppID
		FROM
			OpportunityMaster OM
		INNER JOIN
			OpportunityItems OI
		ON
			OM.numOppId = OI.numOppId
		WHERE   
			OM.numDomainId = @numDomainId
			AND OM.tintOppType = 1
			AND OM.tintOppStatus=1
			AND ISNULL(OI.bitDropShip, 0) = 1
			AND 1 = (CASE WHEN (SELECT COUNT(*) FROM OpportunityLinking OMInner INNER JOIN OpportunityItems OIInner ON OMInner.numChildOppID=OIInner.numOppId WHERE OMInner.numParentOppID = OM.numOppID AND OIInner.numItemCode=OI.numItemCode AND ISNULL(OIInner.numWarehouseItmsID,0)=ISNULL(OI.numWarehouseItmsID,0)) > 0 THEN 0 ELSE 1 END)


		If ISNULL((SELECT bitReOrderPoint FROM Domain WHERE numDomainId=@numDomainID),0) = 1
		BEGIN
			INSERT INTO @TEMPOppID
			(
				numOppID
			)
			SELECT DISTINCT
				OM.numOppID
			FROM
				OpportunityMaster OM
			INNER JOIN
				OpportunityItems OI
			ON
				OI.numOppId = OM.numOppId
			INNER JOIN 
				Item I 
			ON 
				I.numItemCode = OI.numItemCode
			INNER JOIN 
				WarehouseItems WI 
			ON 
				OI.numWarehouseItmsID = WI.numWareHouseItemID
			WHERE   
				OM.numDomainId = @numDomainId
				AND ISNULL((SELECT bitReOrderPoint FROM Domain WHERE numDomainId=@numDomainID),0) = 1
				AND (ISNULL(WI.numOnHand,0) + ISNULL(WI.numOnOrder,0) <= ISNULL(WI.numReOrder,0))
				AND OM.tintOppType = 1
				AND OM.tintOppStatus=1
				AND ISNULL(I.bitAssembly, 0) = 0
				AND ISNULL(I.bitKitParent, 0) = 0
				AND ISNULL(OI.bitDropship,0) = 0
				AND OM.numOppId NOT IN (SELECT numOppID FROM @TEMPOppID)

			IF (SELECT COUNT(*) FROM @TEMPOppID) > 0
			BEGIN
				DECLARE @tmpIds VARCHAR(MAX) = ''
				SELECT @tmpIds = CONCAT(@tmpIds,numOppID,', ') FROM @TEMPOppID
				SET @strSql = CONCAT(@strSql,' AND Opp.numOppID IN (',SUBSTRING(@tmpIds, 0, LEN(@tmpIds)),') ')
			END
			ELSE
			BEGIN
				SET @strSql = CONCAT(@strSql,' AND Opp.numOppID IN (0) ')
			END
		END
	END
	ELSE IF ISNULL(@tintDashboardReminderType,0) = 15
	BEGIN
		SET @strSql = CONCAT(@strSql,' AND Opp.numOppID IN (','SELECT
																	OMInner.numOppID
																FROM
																	OpportunityMaster OMInner
																WHERE
																	numDomainId=',@numDomainID,'
																	AND tintOppType=1
																	AND tintOppStatus=1
																	AND ISNULL(OMInner.tintshipped,0) = 0
																	AND dtReleaseDate IS NOT NULL
																	AND dtReleaseDate < DateAdd(minute,',@ClientTimeZoneOffset * -1,',GETUTCDATE())
																	AND (SELECT 
																			COUNT(*) 
																		FROM 
																		(
																			SELECT
																				OIInner.numoppitemtCode,
																				ISNULL(OIInner.numUnitHour,0) AS OrderedQty,
																				ISNULL(TempFulFilled.FulFilledQty,0) AS FulFilledQty
																			FROM
																				OpportunityItems OIInner
																			INNER JOIN
																				Item IInner
																			ON
																				OIInner.numItemCode = IInner.numItemCode
																			OUTER APPLY
																			(
																				SELECT
																					SUM(OpportunityBizDocItems.numUnitHour) AS FulFilledQty
																				FROM
																					OpportunityBizDocs
																				INNER JOIN
																					OpportunityBizDocItems 
																				ON
																					OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																				WHERE
																					OpportunityBizDocs.numOppId = OMInner.numOppId
																					AND ISNULL(OpportunityBizDocs.numBizDocId,0) = 296
																					AND ISNULL(OpportunityBizDocs.bitFulFilled,0) = 1
																					AND OpportunityBizDocItems.numOppItemID = OIInner.numoppitemtCode
																			) AS TempFulFilled
																			WHERE
																				OIInner.numOppID = OMInner.numOppID
																				AND UPPER(IInner.charItemType) = ''P''
																				AND ISNULL(OIInner.bitDropShip,0) = 0
																		) X
																		WHERE
																			X.OrderedQty <> X.FulFilledQty) > 0',') ')
	END



	DECLARE  @firstRec  AS INTEGER
	DECLARE  @lastRec  AS INTEGER
  
	SET @firstRec = (@CurrentPage - 1) * @PageSize
	SET @lastRec = (@CurrentPage * @PageSize + 1)



	-- EXECUTE FINAL QUERY
	DECLARE @strFinal AS NVARCHAR(MAX)
	SET @strFinal = CONCAT('SELECT ROW_NUMBER() OVER(ORDER BY ',@columnName,' ',@columnSortOrder,') ID, Opp.numOppID INTO #temp2', REPLACE(@strSql,'##PLACEHOLDER##',''),'; SELECT ID,',@strColumns,' INTO #tempTable',REPLACE(@strSql,'##PLACEHOLDER##',' JOIN #temp2 tblAllOrders ON Opp.numOppID = tblAllOrders.numOppID '),' AND tblAllOrders.ID > ',@firstRec,' and tblAllOrders.ID <',@lastRec,' ORDER BY ID; SELECT * FROM  #tempTable; DROP TABLE #tempTable; SELECT @TotalRecords = COUNT(*) FROM #temp2; DROP TABLE #temp2;')
	--SET @strFinal = CONCAT('SELECT ROW_NUMBER() OVER(ORDER BY ',@columnName,' ',@columnSortOrder,') ID, ',@strColumns,' INTO #tempTable',@strSql,'; SELECT * FROM #tempTable WHERE ID > ',@firstRec,' and ID <',@lastRec,' ORDER BY ID; SELECT @TotalRecords = COUNT(*) FROM #tempTable; DROP TABLE #tempTable;')

	PRINT CAST(@strFinal AS NTEXT)
	EXEC sp_executesql @strFinal, N'@TotalRecords INT OUT', @TotRecs OUT

	SELECT * FROM #tempForm

	DROP TABLE #tempForm

	DROP TABLE #tempColorScheme
--created by anoop jayaraj                                          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getdomaindetails')
DROP PROCEDURE usp_getdomaindetails
GO
CREATE PROCEDURE [dbo].[USP_GetDomainDetails]                                          
@numDomainID as numeric(9)=0                                          
as           

DECLARE @listIds VARCHAR(MAX)
SELECT @listIds = COALESCE(@listIds+',' ,'') + CAST(numUserID AS VARCHAR(100)) FROM UnitPriceApprover WHERE numDomainID = @numDomainID
   
DECLARE @canChangeDiferredIncomeSelection AS BIT = 1

IF EXISTS(SELECT 
			OB.numOppBizDocsId 
		FROM 
			OpportunityBizDocs OB 
		INNER JOIN 
			OpportunityMaster OM 
		ON 
			OB.numOppId=OM.numOppId 
		WHERE 
			numBizDocId=304 
			AND OM.numDomainId=@numDomainID)
BEGIN
	SET @canChangeDiferredIncomeSelection = 0
END   
   
                               
select                                           
vcDomainName,                                           
isnull(vcDomainDesc,'') vcDomainDesc,                                           
isnull(bitExchangeIntegration,0) bitExchangeIntegration,                                           
isnull(bitAccessExchange,0) bitAccessExchange,                                           
isnull(vcExchUserName,'') vcExchUserName,                                           
isnull(vcExchPassword,'') vcExchPassword,                                           
isnull(vcExchPath,'') vcExchPath,                                           
isnull(vcExchDomain,'') vcExchDomain,                                         
tintCustomPagingRows,                                         
vcDateFormat,                                         
numDefCountry,                                         
tintComposeWindow,                                         
sintStartDate,                                         
sintNoofDaysInterval,                                         
tintAssignToCriteria,                                         
bitIntmedPage,
ISNULL(D.numCost,0) as numCost,                                         
tintFiscalStartMonth,                                    
--isnull(bitDeferredIncome,0) as bitDeferredIncome,                                  
isnull(tintPayPeriod,0) as tintPayPeriod,                                
isnull(numCurrencyID,0) as numCurrencyID,                            
isnull(vcCurrency,'') as vcCurrency,                      
isnull(charUnitSystem,'') as charUnitSystem ,                    
isnull(vcPortalLogo,'') as vcPortalLogo,                  
isnull(tintChrForComSearch,2) as tintChrForComSearch,                
isnull(tintChrForItemSearch,'') as tintChrForItemSearch,            
isnull(intPaymentGateWay,0) as intPaymentGateWay  ,        
 case when bitPSMTPServer= 1 then vcPSMTPServer else '' end as vcPSMTPServer  ,              
 case when bitPSMTPServer= 1 then  isnull(numPSMTPPort,0)  else 0 end as numPSMTPPort          
,isnull(bitPSMTPAuth,0) bitPSMTPAuth        
,isnull([vcPSmtpPassword],'')vcPSmtpPassword        
,isnull(bitPSMTPSSL,0) bitPSMTPSSL     
,isnull(bitPSMTPServer,0) bitPSMTPServer  ,isnull(vcPSMTPUserName,'') vcPSMTPUserName,      
--isnull(numDefaultCategory,0) numDefaultCategory,
isnull(numShipCompany,0)   numShipCompany,
isnull(bitAutoPopulateAddress,0) bitAutoPopulateAddress,
isnull(tintPoulateAddressTo,0) tintPoulateAddressTo,
isnull(bitMultiCompany,0) bitMultiCompany,
isnull(bitMultiCurrency,0) bitMultiCurrency,
ISNULL(bitCreateInvoice,0) bitCreateInvoice,
ISNULL([numDefaultSalesBizDocId],0) numDefaultSalesBizDocId,
ISNULL([bitSaveCreditCardInfo],0) bitSaveCreditCardInfo,
ISNULL([intLastViewedRecord],10) intLastViewedRecord,
ISNULL([tintCommissionType],1) tintCommissionType,
ISNULL([tintBaseTax],2) tintBaseTax,
ISNULL([numDefaultPurchaseBizDocId],0) numDefaultPurchaseBizDocId,
ISNULL(bitEmbeddedCost,0) bitEmbeddedCost,
ISNULL(numDefaultSalesOppBizDocId,0) numDefaultSalesOppBizDocId,
ISNULL(tintSessionTimeOut,1) tintSessionTimeOut,
ISNULL(tintDecimalPoints,2) tintDecimalPoints,
ISNULL(bitCustomizePortal,0) bitCustomizePortal,
ISNULL(tintBillToForPO,0) tintBillToForPO,
ISNULL(tintShipToForPO,0) tintShipToForPO,
ISNULL(tintOrganizationSearchCriteria,1) tintOrganizationSearchCriteria,
ISNULL(tintLogin,0) tintLogin,
lower(ISNULL(vcPortalName,'')) vcPortalName,
ISNULL(bitDocumentRepositary,0) bitDocumentRepositary,
D.numDomainID,
ISNULL(vcPSMTPDisplayName,'') vcPSMTPDisplayName,
ISNULL(bitGtoBContact,0) bitGtoBContact,
ISNULL(bitBtoGContact,0) bitBtoGContact,
ISNULL(bitGtoBCalendar,0) bitGtoBCalendar,
ISNULL(bitBtoGCalendar,0) bitBtoGCalendar,
ISNULL(bitExpenseNonInventoryItem,0) bitExpenseNonInventoryItem,
ISNULL(bitInlineEdit,0) bitInlineEdit,
ISNULL(bitRemoveVendorPOValidation,0) bitRemoveVendorPOValidation,
isnull(D.tintBaseTaxOnArea,0) tintBaseTaxOnArea,
ISNULL(D.bitAmountPastDue,0) bitAmountPastDue,
CONVERT(DECIMAL(18,2),ISNULL(D.monAmountPastDue,0)) monAmountPastDue,
ISNULL(bitSearchOrderCustomerHistory,0) bitSearchOrderCustomerHistory,
isnull(D.bitRentalItem,0) as bitRentalItem,
isnull(D.numRentalItemClass,0) as numRentalItemClass,
isnull(D.numRentalHourlyUOM,0) as numRentalHourlyUOM,
isnull(D.numRentalDailyUOM,0) as numRentalDailyUOM,
isnull(D.tintRentalPriceBasedOn,0) as tintRentalPriceBasedOn,
isnull(D.tintCalendarTimeFormat,24) as tintCalendarTimeFormat,
isnull(D.tintDefaultClassType,0) as tintDefaultClassType,
ISNULL(D.tintPriceBookDiscount,0) tintPriceBookDiscount,
ISNULL(D.bitAutoSerialNoAssign,0) as bitAutoSerialNoAssign,
ISNULL(D.bitIsShowBalance,0) AS [bitIsShowBalance],
ISNULL(numIncomeAccID,0) AS [numIncomeAccID],
ISNULL(numCOGSAccID,0) AS [numCOGSAccID],
ISNULL(numAssetAccID,0) AS [numAssetAccID],
ISNULL(IsEnableProjectTracking,0) AS [IsEnableProjectTracking],
ISNULL(IsEnableCampaignTracking,0) AS [IsEnableCampaignTracking],
ISNULL(IsEnableResourceScheduling,0) AS [IsEnableResourceScheduling],
ISNULL(numShippingServiceItemID,0) AS [numShippingServiceItemID],
ISNULL(D.numSOBizDocStatus,0) AS numSOBizDocStatus,
ISNULL(D.bitAutolinkUnappliedPayment,0) AS bitAutolinkUnappliedPayment,
ISNULL(D.numDiscountServiceItemID,0) AS numDiscountServiceItemID,
ISNULL(D.vcShipToPhoneNo,0) AS vcShipToPhoneNo,
ISNULL(D.vcGAUserEMail,'') AS vcGAUserEMail,
ISNULL(D.vcGAUserPassword,'') AS vcGAUserPassword,
ISNULL(D.vcGAUserProfileId,0) AS vcGAUserProfileId,
ISNULL(D.bitDiscountOnUnitPrice,0) AS bitDiscountOnUnitPrice,
ISNULL(D.intShippingImageWidth,0) AS intShippingImageWidth ,
ISNULL(D.intShippingImageHeight,0) AS intShippingImageHeight,
ISNULL(D.numTotalInsuredValue,0) AS numTotalInsuredValue,
ISNULL(D.numTotalCustomsValue,0) AS numTotalCustomsValue,
ISNULL(D.vcHideTabs,'') AS vcHideTabs,
ISNULL(D.bitUseBizdocAmount,0) AS bitUseBizdocAmount,
ISNULL(D.bitDefaultRateType,0) AS bitDefaultRateType,
ISNULL(D.bitIncludeTaxAndShippingInCommission,0) AS bitIncludeTaxAndShippingInCommission,
/*,ISNULL(D.bitAllowPPVariance,0) AS bitAllowPPVariance*/
ISNULL(D.bitPurchaseTaxCredit,0) AS bitPurchaseTaxCredit,
ISNULL(D.bitLandedCost,0) AS bitLandedCost,
ISNULL(D.vcLanedCostDefault,'') AS vcLanedCostDefault,
ISNULL(D.bitMinUnitPriceRule,0) AS bitMinUnitPriceRule,
ISNULL(D.numAbovePercent,0) AS numAbovePercent,
ISNULL(D.numBelowPercent,0) AS numBelowPercent,
ISNULL(D.numBelowPriceField,0) AS numBelowPriceField,
@listIds AS vcUnitPriceApprover,
ISNULL(D.numDefaultSalesPricing,2) AS numDefaultSalesPricing,
ISNULL(D.bitReOrderPoint,0) AS bitReOrderPoint,
ISNULL(D.numReOrderPointOrderStatus,0) AS numReOrderPointOrderStatus,
ISNULL(D.numPODropShipBizDoc,0) AS numPODropShipBizDoc,
ISNULL(D.numPODropShipBizDocTemplate,0) AS numPODropShipBizDocTemplate,
ISNULL(D.IsEnableDeferredIncome,0) AS IsEnableDeferredIncome,
ISNULL(D.bitApprovalforTImeExpense,0) AS bitApprovalforTImeExpense,
ISNULL(D.intTimeExpApprovalProcess,0) AS intTimeExpApprovalProcess,
ISNULL(D.bitApprovalforOpportunity,0) AS bitApprovalforOpportunity,
ISNULL(D.intOpportunityApprovalProcess,0) AS intOpportunityApprovalProcess,
@canChangeDiferredIncomeSelection AS canChangeDiferredIncomeSelection,
ISNULL(CAST(D.numDefaultSalesShippingDoc AS int),0) AS numDefaultSalesShippingDoc,
ISNULL(CAST(D.numDefaultPurchaseShippingDoc AS int),0) AS numDefaultPurchaseShippingDoc,
ISNULL(D.bitchkOverRideAssignto,0) AS bitchkOverRideAssignto,
ISNULL(D.vcPrinterIPAddress,'') vcPrinterIPAddress,
ISNULL(D.vcPrinterPort,'') vcPrinterPort,
ISNULL(bitCostApproval,0) bitCostApproval
,ISNULL(bitListPriceApproval,0) bitListPriceApproval
,ISNULL(bitMarginPriceViolated,0) bitMarginPriceViolated
,ISNULL(vcSalesOrderTabs,'Sales Orders') AS vcSalesOrderTabs
,ISNULL(vcSalesQuotesTabs,'Sales Quotes') AS vcSalesQuotesTabs
,ISNULL(vcItemPurchaseHistoryTabs,'Item Purchase History') AS vcItemPurchaseHistoryTabs
,ISNULL(vcItemsFrequentlyPurchasedTabs,'Items Frequently Purchased') AS vcItemsFrequentlyPurchasedTabs
,ISNULL(vcOpenCasesTabs,'Open Cases') AS vcOpenCasesTabs
,ISNULL(vcOpenRMATabs,'Open RMAs') AS vcOpenRMATabs
,ISNULL(bitSalesOrderTabs,0) AS bitSalesOrderTabs
,ISNULL(bitSalesQuotesTabs,0) AS bitSalesQuotesTabs
,ISNULL(bitItemPurchaseHistoryTabs,0) AS bitItemPurchaseHistoryTabs
,ISNULL(bitItemsFrequentlyPurchasedTabs,0) AS bitItemsFrequentlyPurchasedTabs
,ISNULL(bitOpenCasesTabs,0) AS bitOpenCasesTabs
,ISNULL(bitOpenRMATabs,0) AS bitOpenRMATabs
,ISNULL(bitSupportTabs,0) AS bitSupportTabs
,ISNULL(vcSupportTabs,'Support') AS vcSupportTabs

,ISNULL(numDefaultSiteID,0) AS numDefaultSiteID
,ISNULL(tintOppStautsForAutoPOBackOrder,0) AS tintOppStautsForAutoPOBackOrder
,ISNULL(tintUnitsRecommendationForAutoPOBackOrder,1) AS tintUnitsRecommendationForAutoPOBackOrder
,ISNULL(bitRemoveGlobalLocation,0) AS bitRemoveGlobalLocation
,ISNULL(numAuthorizePercentage,0) AS numAuthorizePercentage
,ISNULL(bitEDI,0) AS bitEDI
from Domain D  
left join eCommerceDTL eComm  
on eComm.numDomainID=D.numDomainID  
where (D.numDomainID=@numDomainID OR @numDomainID=-1)


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetItemAttributesEcommerce')
DROP PROCEDURE dbo.[USP_GetItemAttributesEcommerce]
GO
CREATE PROCEDURE [dbo].[USP_GetItemAttributesEcommerce]
(
	@numDomainID NUMERIC(9),
	@numWareHouseID AS NUMERIC(9) = 0,
    @FilterQuery AS VARCHAR(2000) = '',
    @numCategoryID AS NUMERIC(18) = 0
)
AS 
BEGIN
	SELECT
		Fld_id 
		,Fld_type 
		,Fld_label 
		,numlistid
		,bitAutoComplete
	FROM
		CFW_Fld_Master
	WHERE
		numDomainID=@numDomainID
		AND Grp_id=9 
	ORDER BY 
		bitAutoComplete
END


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetListDetailAttributesUsingDomainID')
DROP PROCEDURE dbo.[USP_GetListDetailAttributesUsingDomainID]
GO
CREATE PROCEDURE [dbo].[USP_GetListDetailAttributesUsingDomainID]            
 @numDomainID as numeric(9)=0       
AS
BEGIN            
	SELECT 
		* 
	FROM 
		ListDetails LD
	JOIN 
		CFW_Fld_Master CFM 
	ON 
		CFM.numlistid=LD.numListID  
	WHERE 
		Grp_id=9 
		AND LD.numDomainID = @numDomainID
END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetMirrorBizDocDetails' ) 
    DROP PROCEDURE USP_GetMirrorBizDocDetails
GO

-- EXEC USP_GetMirrorBizDocDetails 53,8,1,1,0
CREATE PROCEDURE [dbo].[USP_GetMirrorBizDocDetails]
    (
      @numReferenceID NUMERIC(9) = 0,
      @numReferenceType TINYINT,
      @numDomainId NUMERIC(9),
      @numUserCntID NUMERIC(9),
      @ClientTimeZoneOffset INT
    )
AS 
    BEGIN
--        DECLARE @numBizDocID AS NUMERIC
        DECLARE @numBizDocType AS NUMERIC
        
        IF @numReferenceType = 1
			SELECT  @numBizDocType = numListItemID FROM dbo.ListDetails WHERE numListID=27 AND vcData='Sales Order' AND constFlag=1
        ELSE IF @numReferenceType = 2
			SELECT  @numBizDocType = numListItemID FROM dbo.ListDetails WHERE numListID=27 AND vcData='Purchase Order' AND constFlag=1
        ELSE IF @numReferenceType = 3
			SELECT  @numBizDocType = numListItemID FROM dbo.ListDetails WHERE numListID=27 AND vcData='Sales Opportunity' AND constFlag=1
        ELSE IF @numReferenceType = 4
			SELECT  @numBizDocType = numListItemID FROM dbo.ListDetails WHERE numListID=27 AND vcData='Purchase Opportunity' AND constFlag=1
        ELSE IF @numReferenceType = 5 
			SELECT @numBizDocType =  numListItemID FROM dbo.ListDetails WHERE vcData='RMA' AND constFlag=1
		ELSE IF @numReferenceType = 6 
			SELECT @numBizDocType =  numListItemID FROM dbo.ListDetails WHERE vcData='RMA' AND constFlag=1 
		ELSE IF @numReferenceType = 7 
			SELECT @numBizDocType =  numListItemID FROM dbo.ListDetails WHERE vcData='Sales Credit Memo' AND constFlag=1 
		ELSE IF @numReferenceType = 8
			SELECT @numBizDocType =  numListItemID FROM dbo.ListDetails WHERE vcData='Purchase Credit Memo' AND constFlag=1 
		ELSE IF @numReferenceType = 9
			SELECT @numBizDocType =  numListItemID FROM dbo.ListDetails WHERE vcData='Refund Receipt' AND constFlag=1 
		ELSE IF @numReferenceType = 10
			SELECT @numBizDocType =  numListItemID FROM dbo.ListDetails WHERE vcData='Credit Memo' AND constFlag=1 
		ELSE IF @numReferenceType = 11
			SELECT  @numBizDocType = numListItemID FROM dbo.ListDetails WHERE vcData='Invoice' AND constFlag=1
	 
        IF @numReferenceType = 1 OR @numReferenceType = 2 OR @numReferenceType = 3 OR @numReferenceType = 4 
		BEGIN
		      
			---------------------------------------------------------------------------------------
      
				  SELECT Mst.vcPOppName AS vcBizDocID,
						 Mst.vcPOppName AS OppName,Mst.vcPOppName AS BizDcocName,@numBizDocType AS BizDoc,ISNULL(Mst.numRecOwner,0) AS Owner,
						 CMP.VcCompanyName AS CompName,Mst.numContactID,Mst.numCreatedBy AS BizDocOwner,
						 Mst.fltDiscount AS decDiscount,
						 (SELECT SUM(ISNULL(monAmountPaid,0)) FROM OpportunityBizDocs WHERE numOppId=MSt.numOppId) AS monAmountPaid,
						 isnull(Mst.txtComments,'') AS vcComments,
						 CONVERT(VARCHAR(20),DATEADD(minute,-@ClientTimeZoneOffset,Mst.bintCreatedDate)) dtCreatedDate,
						 dbo.fn_GetContactName(Mst.numCreatedby) AS numCreatedby,
						 dbo.fn_GetContactName(Mst.numModifiedBy) AS numModifiedBy,
						 Mst.bintModifiedDate AS dtModifiedDate,
						 '' AS numViewedBy,
						 NULL AS dtViewedDate,
						 isnull(Mst.bitBillingTerms,0) AS tintBillingTerms,
						 isnull(Mst.intBillingDays,0) AS numBillingDays,
						 ISNULL(BTR.numNetDueInDays,0) AS numBillingDaysName,
						 BTR.vcTerms AS vcBillingTermsName,
						 isnull(Mst.bitInterestType,0) AS tintInterestType,
						 isnull(Mst.fltInterest,0) AS fltInterest,
						 tintOPPType,
						 @numBizDocType AS numBizDocId,
						 Mst.bintAccountClosingDate,
						 tintShipToType,
						 tintBillToType,
						 tintshipped,
						 NULL bintShippedDate,
						 ISNULL(Mst.monShipCost ,0) AS monShipCost,
						 ISNULL(BT.vcBizDocFooter,D.vcBizDocFooter) AS BizdocFooter,
						 ISNULL(BT.vcBizDocImagePath,D.vcBizDocImagePath) As vcBizDocImagePath, 
						 Mst.numDivisionID,
						  ISNULL(BT.vcPurBizDocFooter,D.vcPurBizDocFooter) AS PurBizdocFooter,
						 isnull(Mst.fltDiscount,0) AS fltDiscount,
						 isnull(Mst.bitDiscountType,0) AS bitDiscountType,
						 0 AS numShipVia,
						 '' AS vcTrackingURL,
						 0 AS numBizDocStatus,
						 '' AS BizDocStatus,
						 '' AS ShipVia,
						 ISNULL(C.varCurrSymbol,'') varCurrSymbol,
						 0 AS ShippingReportCount,
						 0 bitPartialFulfilment,
						 vcPOppName as vcBizDocName,
						 dbo.[fn_GetContactName](Mst.[numModifiedBy])  + ', '
							+ CONVERT(VARCHAR(20),dateadd(MINUTE,-@ClientTimeZoneOffset,Mst.bintModifiedDate)) AS ModifiedBy,
						 dbo.[fn_GetContactName](Mst.[numRecOwner]) + ', ' +  dbo.[fn_GetContactName](Mst.[numAssignedTo]) AS OrderRecOwner,
						 dbo.[fn_GetContactName](DM.[numRecOwner]) + ', ' +  dbo.[fn_GetContactName](DM.[numAssignedTo]) AS AccountRecOwner,
						 Mst.bintCreatedDate AS dtFromDate,
						 Mst.tintTaxOperator,
						 0 AS monTotalEmbeddedCost,
						 0 AS monTotalAccountedCost,
						 ISNULL(BT.bitEnabled,0) bitEnabled,
						 ISNULL(BT.txtBizDocTemplate,'') txtBizDocTemplate, 
						 ISNULL(BT.txtCSS,'') txtCSS,
						 dbo.fn_GetContactName(Mst.numAssignedTo) AS AssigneeName,
						 ISNULL((SELECT vcEmail FROM dbo.AdditionalContactsInformation WHERE numContactID = Mst.numAssignedTo),'') AS AssigneeEmail,
						 ISNULL((SELECT numPhone FROM dbo.AdditionalContactsInformation WHERE numContactID = Mst.numAssignedTo),'') AS AssigneePhone,
						 dbo.fn_GetComapnyName(Mst.numDivisionId) OrganizationName,
						 dbo.fn_GetContactName(Mst.numContactID)  OrgContactName,
						 case when ACI.numPhone<>'' then ACI.numPhone +case when ACI.numPhoneExtension<>'' then ' - ' + ACI.numPhoneExtension else '' end  else '' END OrgContactPhone,
						 DM.vcComPhone as OrganizationPhone,
						 ISNULL(ACI.vcEmail,'') AS OrgContactEmail,
 						 dbo.[fn_GetContactName](Mst.[numRecOwner]) AS OnlyOrderRecOwner,
 						 0 bitAuthoritativeBizDocs,
						0 tintDeferred,0 AS monCreditAmount,
						 0 as bitRentalBizDoc,isnull(BT.numBizDocTempID,0) as numBizDocTempID,isnull(BT.vcTemplateName,'') as vcTemplateName,
						 [dbo].[GetDealAmount](Mst.numOppId ,getutcdate(),0 ) as monPAmount,
						 --(select top 1 SD.vcSignatureFile from SignatureDetail SD join OpportunityBizDocsDetails OBD on SD.numSignID=OBD.numSignID where SD.numDomainID = @numDomainID and OBD.numDomainID = @numDomainID and OBD.numSignID IS not null and OBD.numBizDocsId=opp.numOppBizDocsId order by OBD.numBizDocsPaymentDetId desc) as vcSignatureFile,
						 isnull(CMP.txtComments,'') as vcOrganizationComments,
						 ISNULL((SELECT SUBSTRING((SELECT '$^$' + ISNULL( (SELECT vcShipFieldValue FROM dbo.ShippingFieldValues WHERE numDomainID=MSt.numDomainID AND intShipFieldID IN ( SELECT intShipFieldID FROM dbo.ShippingFields WHERE numListItemID=OBD.numShipVia  AND vcFieldName='Tracking URL')),'') +'#^#'+ RTRIM(LTRIM(vcTrackingNo) )  FROM dbo.OpportunityBizDocs OBD WHERE OBD.numOppId=Mst.numOppID FOR XML PATH('')),4,200000)),'') AS  vcTrackingNo,'' AS  vcShippingMethod,
						 NULL AS dtDeliveryDate,ISNULL(Mst.vcOppRefOrderNo,'') AS vcOppRefOrderNo,ISNULL(Mst.numDiscountAcntType,0) AS numDiscountAcntType ,
						 isnull(numOppBizDocTempID,0) as numOppBizDocTempID,
						 com1.vcCompanyName AS EmployerOrganizationName,div1.vcComPhone as EmployerOrganizationPhone,dbo.FormatedDateFromDate(Mst.dtReleaseDate,@numDomainID) AS vcReleaseDate
						 ,(CASE WHEN ISNULL(Mst.numPartner,0) > 0 THEN ISNULL((SELECT CONCAT(DivisionMaster.vcPartnerCode,'-',CompanyInfo.vcCompanyName) FROM DivisionMaster INNER JOIN CompanyInfo ON DivisionMaster.numCompanyID=CompanyInfo.numCompanyId WHERE DivisionMaster.numDivisionID=Mst.numPartner),'') ELSE '' END) AS vcPartner,
						 ISNULL(vcCustomerPO#,'') vcCustomerPO#,
						 ISNULL(Mst.txtComments,'') vcSOComments
				  FROM    OpportunityMaster Mst JOIN Domain D ON D.numDomainID = Mst.numDomainID
						  LEFT OUTER JOIN [Currency] C ON C.numCurrencyID = Mst.numCurrencyID
						  LEFT JOIN [DivisionMaster] DM ON DM.numDivisionID = Mst.numDivisionID   
						  LEFT JOIN CompanyInfo CMP ON DM.numCompanyId = CMP.numCompanyId and CMP.numDomainID = @numDomainID  
						  LEFT JOIN dbo.AdditionalContactsInformation ACI ON ACI.numContactID = Mst.numContactID
						  LEFT JOIN [BizDocTemplate] BT ON BT.numDomainID = @numDomainID AND BT.numBizDocID = @numBizDocType 
							 AND ((BT.numBizDocTempID=ISNULL(Mst.numOppBizDocTempID, 0)) OR (ISNULL(BT.bitDefault,0)=1 AND ISNULL(BT.bitEnabled,0)=1))  
						  LEFT JOIN BillingTerms BTR ON BTR.numTermsID = ISNULL(Mst.intBillingDays,0)
						  JOIN divisionmaster div1 ON D.numDivisionID = div1.numDivisionID
						  JOIN companyinfo [Com1] ON com1.numCompanyID = div1.numCompanyID
				  WHERE  Mst.numOppID = @numReferenceID AND Mst.numDomainID = @numDomainID
			             
						 EXEC USP_OPPGetOppAddressDetails @numReferenceID,@numDomainId
             
            END
    
    
        ELSE IF @numReferenceType = 5 OR @numReferenceType = 6 OR @numReferenceType = 7 OR @numReferenceType = 8 
					OR @numReferenceType = 9 OR @numReferenceType = 10
         BEGIN
         
								DECLARE @numBizDocTempID AS NUMERIC(9)
								DECLARE @numDivisionID AS NUMERIC(9)
         
                                SELECT @numDivisionID=numDivisionID,@numBizDocTempID=(CASE WHEN @numReferenceType = 5 OR @numReferenceType = 6 OR @numReferenceType = 9 OR @numReferenceType = 10 THEN numRMATempID ELSE numBizDocTempID END)				
                                FROM    dbo.ReturnHeader RH
                                WHERE   RH.numReturnHeaderID = @numReferenceID
                                
    			---------------------
               
                                SELECT  RH.vcBizDocName AS [vcBizDocID],
                                  RH.vcRMA AS OppName,RH.vcBizDocName AS BizDcocName,@numBizDocType AS BizDoc,ISNULL(RH.numCreatedBy,0) AS Owner,
								   CMP.VcCompanyName AS CompName,RH.numContactID,RH.numCreatedBy AS BizDocOwner,
                                        RH.monTotalDiscount AS decDiscount,
                                        ISNULL(RDA.monAmount + RDA.monTotalTax - RDA.monTotalDiscount, 0) AS monAmountPaid,
                                        ISNULL(RH.vcComments, '') AS vcComments,
                                        CONVERT(VARCHAR(20), DATEADD(minute, -@ClientTimeZoneOffset, RH.dtCreatedDate)) dtCreatedDate,
                                        dbo.fn_GetContactName(RH.numCreatedby) AS numCreatedby,
                                        dbo.fn_GetContactName(RH.numModifiedBy) AS numModifiedBy,
                                        RH.dtModifiedDate,
                                        '' AS numViewedBy,
                                        '' AS dtViewedDate,
                                        0 AS tintBillingTerms,
                                        0 AS numBillingDays,
                                        0 AS [numBillingDaysName],
                                        '' AS vcBillingTermsName,
                                        0 AS tintInterestType,
                                        0 AS fltInterest,
                                        tintReturnType AS tintOPPType,
                                        @numBizDocType AS numBizDocId,
                                        dbo.fn_GetContactName(RH.numCreatedby) AS ApprovedBy,
                                        dtCreatedDate AS dtApprovedDate,
                                        0 AS bintAccountClosingDate,
                                        0 AS tintShipToType,
                                        0 AS tintBillToType,
                                        0 AS tintshipped,
                                        0 AS bintShippedDate,
                                        0 AS monShipCost,
                                        ISNULL(BT.vcBizDocFooter,D.vcBizDocFooter) AS BizdocFooter,
                                        ISNULL(BT.vcBizDocImagePath,D.vcBizDocImagePath) As vcBizDocImagePath,
                                        RH.numDivisionID,
                                         ISNULL(BT.vcPurBizDocFooter,D.vcPurBizDocFooter) AS PurBizdocFooter,
                                        ISNULL(RH.monTotalDiscount, 0) AS fltDiscount,
                                        0 AS bitDiscountType,
                                        0 AS numShipVia,
                                        '' AS vcTrackingURL,
                                        0 AS numBizDocStatus,
                                        '-' AS BizDocStatus,
                                        '-' AS ShipVia,
                                        ISNULL(C.varCurrSymbol, '') varCurrSymbol,
                                        0 AS ShippingReportCount,
                                        0 bitPartialFulfilment,
                                        vcBizDocName,
                                        dbo.[fn_GetContactName](RH.[numModifiedBy])
                                        + ', '
                                        + CONVERT(VARCHAR(20), DATEADD(MINUTE, -@ClientTimeZoneOffset, RH.dtModifiedDate)) AS ModifiedBy,
                                        dbo.[fn_GetContactName](RH.[numCreatedBy]) AS OrderRecOwner,
                                        dbo.[fn_GetContactName](DM.[numRecOwner])
                                        + ', '
                                        + dbo.[fn_GetContactName](DM.[numAssignedTo]) AS AccountRecOwner,
                                        RH.dtcreatedDate AS dtFromDate,
                                        0 AS tintTaxOperator,
                                        1 AS monTotalEmbeddedCost,
                                        2 AS monTotalAccountedCost,
                                        ISNULL(BT.bitEnabled, 0) bitEnabled,
                                        ISNULL(BT.txtBizDocTemplate, '') txtBizDocTemplate,
                                        ISNULL(BT.txtCSS, '') txtCSS,
                                        '' AS AssigneeName,
                                        '' AS AssigneeEmail,
                                        '' AS AssigneePhone,
                                        dbo.fn_GetComapnyName(RH.numDivisionId) OrganizationName,
                                        dbo.fn_GetContactName(RH.numContactID) OrgContactName,
                                        dbo.getCompanyAddress(RH.numDivisionId, 1,RH.numDomainId) CompanyBillingAddress,
                                        CASE WHEN ACI.numPhone <> ''
                                             THEN ACI.numPhone
                                                  + CASE WHEN ACI.numPhoneExtension <> ''
                                                         THEN ' - ' + ACI.numPhoneExtension
                                                         ELSE ''
                                                    END
                                             ELSE ''
                                        END OrgContactPhone,
                                        DM.vcComPhone AS OrganizationPhone,
                                        ISNULL(ACI.vcEmail, '') AS OrgContactEmail,
                                        dbo.[fn_GetContactName](RH.[numCreatedBy]) AS OnlyOrderRecOwner,
                                        1 bitAuthoritativeBizDocs,
                                        0 tintDeferred,
                                        0 AS monCreditAmount,
                                        0 AS bitRentalBizDoc,
                                        ISNULL(BT.numBizDocTempID, 0) AS numBizDocTempID,
                                        ISNULL(BT.vcTemplateName, '') AS vcTemplateName,
                                        0 AS monPAmount,
                                        ISNULL(CMP.txtComments, '') AS vcOrganizationComments,
                                        '' AS vcTrackingNo,
                                        '' AS vcShippingMethod,
                                        '' AS dtDeliveryDate,
                                        '' AS vcOppRefOrderNo,
                                        0 AS numDiscountAcntType ,
										0 AS numOppBizDocTempID
                                        ,com1.vcCompanyName AS EmployerOrganizationName,div1.vcComPhone as EmployerOrganizationPhone
										,'' AS vcReleaseDate
										,'' AS vcPartner
										,'' vcCustomerPO#
										,'' vcSOComments
                                FROM    dbo.ReturnHeader RH
                                        JOIN Domain D ON D.numDomainID = RH.numDomainID
                                        JOIN [DivisionMaster] DM ON DM.numDivisionID = RH.numDivisionID
                                        LEFT OUTER JOIN [Currency] C ON C.numCurrencyID = DM.numCurrencyID
                                        LEFT JOIN CompanyInfo CMP ON DM.numCompanyId = CMP.numCompanyId
                                                                     AND CMP.numDomainID = @numDomainID
                                        LEFT JOIN dbo.AdditionalContactsInformation ACI ON ACI.numContactID = RH.numContactID
                                        LEFT JOIN [BizDocTemplate] BT ON BT.numDomainID = @numDomainID
                                                                         AND BT.numBizDocID = @numBizDocType	
                                                                         AND ((BT.numBizDocTempID=ISNULL(@numBizDocTempID, 0)) OR (ISNULL(BT.bitDefault,0)=1 AND ISNULL(BT.bitEnabled,0)=1))  
                                         JOIN divisionmaster div1 ON D.numDivisionID = div1.numDivisionID
										 JOIN companyinfo [Com1] ON com1.numCompanyID = div1.numCompanyID,
                                                                         dbo.GetReturnDealAmount(@numReferenceID,@numReferenceType) RDA
                                WHERE   RH.numReturnHeaderID = @numReferenceID
                                
                                	--************Customer/Vendor Billing Address************
                                		SELECT  '<pre>' + isnull(AD.vcStreet,'') + '</pre>' + isnull(AD.vcCity,'') + ' ,' + isnull(dbo.fn_GetState(AD.numState),'') + ' ' + isnull(AD.vcPostalCode,'') + ' <br>' + isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcFullAddress,
											isnull(AD.vcStreet,'') AS vcStreet,
											isnull(AD.vcCity,'') AS vcCity,
											isnull(dbo.fn_GetState(AD.numState),'') AS vcState,
											isnull(AD.vcPostalCode,'') AS vcPostalCode,
											isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcCountry,
											ISNULL(AD.vcAddressName,'') AS vcAddressName,
            								CMP.vcCompanyName
											,(CASE 
													WHEN ISNULL(bitAltContact,0) = 1 
													THEN ISNULL(vcAltContact,'') 
													ELSE 
														(CASE 
															WHEN ISNULL(numContact,0) > 0 
															THEN 
																ISNULL((SELECT TOP 1 CONCAT(vcFirstName,' ',vcLastName) FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND numContactId=numContact),'')
															ELSE 
																ISNULL((SELECT TOP 1 CONCAT(vcFirstName,' ',vcLastName) FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND numDivisionId=AD.numRecordID AND ISNULL(bitPrimaryContact,0)=1),'')
															END
														)  
											END) AS vcContact
										 FROM AddressDetails AD 
										 JOIN [DivisionMaster] DM ON DM.numDivisionID = AD.numRecordID   
										 JOIN CompanyInfo CMP ON DM.numCompanyId = CMP.numCompanyId and CMP.numDomainID = AD.numDomainID  
										 --LEFT JOIN dbo.AdditionalContactsInformation ACI ON ACI.numContactID = @numContactID AND ACI.numDivisionId=DM.numDivisionID
										 WHERE AD.numDomainID=@numDomainID AND AD.numRecordID=@numDivisionID 
										 AND AD.tintAddressOf = 2 AND AD.tintAddressType = 1 AND AD.bitIsPrimary=1

                                        
                                	--************Customer/Vendor Shipping Address************
                                	SELECT  '<pre>' + isnull(AD.vcStreet,'') + '</pre>' + isnull(AD.vcCity,'') + ' ,' + isnull(dbo.fn_GetState(AD.numState),'') + ' ' + isnull(AD.vcPostalCode,'') + ' <br>' + isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcFullAddress,
											isnull(AD.vcStreet,'') AS vcStreet,
											isnull(AD.vcCity,'') AS vcCity,
											isnull(dbo.fn_GetState(AD.numState),'') AS vcState,
											isnull(AD.vcPostalCode,'') AS vcPostalCode,
											isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcCountry,
											ISNULL(AD.vcAddressName,'') AS vcAddressName,
            								CMP.vcCompanyName,
											(CASE 
													WHEN ISNULL(bitAltContact,0) = 1 
													THEN ISNULL(vcAltContact,'') 
													ELSE 
														(CASE 
															WHEN ISNULL(numContact,0) > 0 
															THEN 
																ISNULL((SELECT TOP 1 CONCAT(vcFirstName,' ',vcLastName) FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND numContactId=numContact),'')
															ELSE 
																ISNULL((SELECT TOP 1 CONCAT(vcFirstName,' ',vcLastName) FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND numDivisionId=AD.numRecordID AND ISNULL(bitPrimaryContact,0)=1),'')
															END
														)  
											END) AS vcContact
										 FROM AddressDetails AD 
										 JOIN [DivisionMaster] DM ON DM.numDivisionID = AD.numRecordID   
										 JOIN CompanyInfo CMP ON DM.numCompanyId = CMP.numCompanyId and CMP.numDomainID = AD.numDomainID  
										 --LEFT JOIN dbo.AdditionalContactsInformation ACI ON ACI.numContactID = @numContactID AND ACI.numDivisionId=DM.numDivisionID
										 WHERE AD.numDomainID=@numDomainID AND AD.numRecordID=@numDivisionID 
										 AND AD.tintAddressOf = 2 AND AD.tintAddressType = 2 AND AD.bitIsPrimary=1

                                --************Employer Shipping Address************ 
									SELECT  '<pre>' + isnull(AD.vcStreet,'') + '</pre>' + isnull(AD.vcCity,'') + ' ,' + isnull(dbo.fn_GetState(AD.numState),'') + ' ' + isnull(AD.vcPostalCode,'') + ' <br>' + isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcFullAddress,
											isnull(AD.vcStreet,'') AS vcStreet,
											isnull(AD.vcCity,'') AS vcCity,
											isnull(dbo.fn_GetState(AD.numState),'') AS vcState,
											isnull(AD.vcPostalCode,'') AS vcPostalCode,
											isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcCountry,
											ISNULL(AD.vcAddressName,'') AS vcAddressName,vcCompanyName
									 FROM Domain D1 JOIN divisionmaster div1 ON D1.numDivisionID = div1.numDivisionID
										  JOIN companyinfo [Com1] ON com1.numCompanyID = div1.numCompanyID
										  JOIN dbo.AddressDetails AD ON AD.numDomainID = div1.numDomainID AND AD.numRecordID = div1.numDivisionID 
									  WHERE  D1.numDomainID = @numDomainID
										AND AD.tintAddressOf = 2 AND AD.tintAddressType = 1 AND AD.bitIsPrimary=1


								--************Employer Shipping Address************ 
								SELECT  '<pre>' + isnull(AD.vcStreet,'') + '</pre>' + isnull(AD.vcCity,'') + ' ,' + isnull(dbo.fn_GetState(AD.numState),'') + ' ' + isnull(AD.vcPostalCode,'') + ' <br>' + isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcFullAddress,
										isnull(AD.vcStreet,'') AS vcStreet,
										isnull(AD.vcCity,'') AS vcCity,
										isnull(dbo.fn_GetState(AD.numState),'') AS vcState,
										isnull(AD.vcPostalCode,'') AS vcPostalCode,
										isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcCountry,
										ISNULL(AD.vcAddressName,'') AS vcAddressName,vcCompanyName
								 FROM Domain D1 JOIN divisionmaster div1 ON D1.numDivisionID = div1.numDivisionID
									  JOIN companyinfo [Com1] ON com1.numCompanyID = div1.numCompanyID
									  JOIN dbo.AddressDetails AD ON AD.numDomainID = div1.numDomainID AND AD.numRecordID = div1.numDivisionID 
								 WHERE AD.numDomainID=@numDomainID 
								 AND AD.tintAddressOf = 2 AND AD.tintAddressType = 2 AND AD.bitIsPrimary=1
           END
		
        ELSE IF @numReferenceType = 11
		BEGIN
		      
			---------------------------------------------------------------------------------------
      
				  SELECT Mst.vcPOppName AS vcBizDocID,
						 Mst.vcPOppName AS OppName,OBD.vcBizDocID AS BizDcocName,@numBizDocType AS BizDoc,ISNULL(Mst.numRecOwner,0) AS Owner,
						 CMP.VcCompanyName AS CompName,Mst.numContactID,Mst.numCreatedBy AS BizDocOwner,
						 Mst.fltDiscount AS decDiscount,
						 (SELECT SUM(ISNULL(monAmountPaid,0)) FROM OpportunityBizDocs WHERE numOppId=MSt.numOppId) AS monAmountPaid,
						 isnull(Mst.txtComments,'') AS vcComments,
						 CONVERT(VARCHAR(20),DATEADD(minute,-@ClientTimeZoneOffset,Mst.bintCreatedDate)) dtCreatedDate,
						 dbo.fn_GetContactName(Mst.numCreatedby) AS numCreatedby,
						 dbo.fn_GetContactName(Mst.numModifiedBy) AS numModifiedBy,
						 Mst.bintModifiedDate AS dtModifiedDate,
						 '' AS numViewedBy,
						 NULL AS dtViewedDate,
						 isnull(Mst.bitBillingTerms,0) AS tintBillingTerms,
						 isnull(Mst.intBillingDays,0) AS numBillingDays,
						 ISNULL(BTR.numNetDueInDays,0) AS numBillingDaysName,
						 BTR.vcTerms AS vcBillingTermsName,
						 isnull(Mst.bitInterestType,0) AS tintInterestType,
						 isnull(Mst.fltInterest,0) AS fltInterest,
						 tintOPPType,
						 @numBizDocType AS numBizDocId,
						 Mst.bintAccountClosingDate,
						 tintShipToType,
						 tintBillToType,
						 tintshipped,
						 NULL bintShippedDate,
						 ISNULL(Mst.monShipCost ,0) AS monShipCost,
						 ISNULL(BT.vcBizDocFooter,D.vcBizDocFooter) AS BizdocFooter,
						 ISNULL(BT.vcBizDocImagePath,D.vcBizDocImagePath) As vcBizDocImagePath, 
						 Mst.numDivisionID,
						  ISNULL(BT.vcPurBizDocFooter,D.vcPurBizDocFooter) AS PurBizdocFooter,
						 isnull(Mst.fltDiscount,0) AS fltDiscount,
						 isnull(Mst.bitDiscountType,0) AS bitDiscountType,
						 0 AS numShipVia,
						 '' AS vcTrackingURL,
						 0 AS numBizDocStatus,
						 '' AS BizDocStatus,
						 '' AS ShipVia,
						 ISNULL(C.varCurrSymbol,'') varCurrSymbol,
						 0 AS ShippingReportCount,
						 0 bitPartialFulfilment,
						 vcPOppName as vcBizDocName,
						 dbo.[fn_GetContactName](Mst.[numModifiedBy])  + ', '
							+ CONVERT(VARCHAR(20),dateadd(MINUTE,-@ClientTimeZoneOffset,Mst.bintModifiedDate)) AS ModifiedBy,
						 dbo.[fn_GetContactName](Mst.[numRecOwner]) + ', ' +  dbo.[fn_GetContactName](Mst.[numAssignedTo]) AS OrderRecOwner,
						 dbo.[fn_GetContactName](DM.[numRecOwner]) + ', ' +  dbo.[fn_GetContactName](DM.[numAssignedTo]) AS AccountRecOwner,
						 Mst.bintCreatedDate AS dtFromDate,
						 Mst.tintTaxOperator,
						 0 AS monTotalEmbeddedCost,
						 0 AS monTotalAccountedCost,
						 ISNULL(BT.bitEnabled,0) bitEnabled,
						 ISNULL(BT.txtBizDocTemplate,'') txtBizDocTemplate, 
						 ISNULL(BT.txtCSS,'') txtCSS,
						 dbo.fn_GetContactName(Mst.numAssignedTo) AS AssigneeName,
						 ISNULL((SELECT vcEmail FROM dbo.AdditionalContactsInformation WHERE numContactID = Mst.numAssignedTo),'') AS AssigneeEmail,
						 ISNULL((SELECT numPhone FROM dbo.AdditionalContactsInformation WHERE numContactID = Mst.numAssignedTo),'') AS AssigneePhone,
						 dbo.fn_GetComapnyName(Mst.numDivisionId) OrganizationName,
						 dbo.fn_GetContactName(Mst.numContactID)  OrgContactName,
						 case when ACI.numPhone<>'' then ACI.numPhone +case when ACI.numPhoneExtension<>'' then ' - ' + ACI.numPhoneExtension else '' end  else '' END OrgContactPhone,
						 DM.vcComPhone as OrganizationPhone,
						 ISNULL(ACI.vcEmail,'') AS OrgContactEmail,
 						 dbo.[fn_GetContactName](Mst.[numRecOwner]) AS OnlyOrderRecOwner,
 						 0 bitAuthoritativeBizDocs,
						0 tintDeferred,0 AS monCreditAmount,
						 0 as bitRentalBizDoc,isnull(BT.numBizDocTempID,0) as numBizDocTempID,isnull(BT.vcTemplateName,'') as vcTemplateName,
						 [dbo].[GetDealAmount](Mst.numOppId ,getutcdate(),0 ) as monPAmount,
						 --(select top 1 SD.vcSignatureFile from SignatureDetail SD join OpportunityBizDocsDetails OBD on SD.numSignID=OBD.numSignID where SD.numDomainID = @numDomainID and OBD.numDomainID = @numDomainID and OBD.numSignID IS not null and OBD.numBizDocsId=opp.numOppBizDocsId order by OBD.numBizDocsPaymentDetId desc) as vcSignatureFile,
						 isnull(CMP.txtComments,'') as vcOrganizationComments,
						 ISNULL((SELECT SUBSTRING((SELECT '$^$' + ISNULL( (SELECT vcShipFieldValue FROM dbo.ShippingFieldValues WHERE numDomainID=MSt.numDomainID AND intShipFieldID IN ( SELECT intShipFieldID FROM dbo.ShippingFields WHERE numListItemID=OBD.numShipVia  AND vcFieldName='Tracking URL')),'') +'#^#'+ RTRIM(LTRIM(vcTrackingNo) )  FROM dbo.OpportunityBizDocs OBD WHERE OBD.numOppId=Mst.numOppID FOR XML PATH('')),4,200000)),'') AS  vcTrackingNo,'' AS  vcShippingMethod,
						 NULL AS dtDeliveryDate,ISNULL(Mst.vcOppRefOrderNo,'') AS vcOppRefOrderNo,ISNULL(Mst.numDiscountAcntType,0) AS numDiscountAcntType ,
						 isnull(numOppBizDocTempID,0) as numOppBizDocTempID,
						 com1.vcCompanyName AS EmployerOrganizationName,div1.vcComPhone as EmployerOrganizationPhone
						 ,dbo.FormatedDateFromDate(Mst.dtReleaseDate,@numDomainID) AS vcReleaseDate
						 ,(CASE WHEN ISNULL(Mst.numPartner,0) > 0 THEN ISNULL((SELECT CONCAT(DivisionMaster.vcPartnerCode,'-',CompanyInfo.vcCompanyName) FROM DivisionMaster INNER JOIN CompanyInfo ON DivisionMaster.numCompanyID=CompanyInfo.numCompanyId WHERE DivisionMaster.numDivisionID=Mst.numPartner),'') ELSE '' END) AS vcPartner,
						 ISNULL(Mst.vcCustomerPO#,'') vcCustomerPO#,
						 ISNULL(Mst.txtComments,'') vcSOComments
				  FROM    OpportunityMaster Mst JOIN Domain D ON D.numDomainID = Mst.numDomainID
					LEFT JOIN OpportunityBizDocs AS OBD ON Mst.numOppid=OBD.numOppId
						  LEFT OUTER JOIN [Currency] C ON C.numCurrencyID = Mst.numCurrencyID
						  LEFT JOIN [DivisionMaster] DM ON DM.numDivisionID = Mst.numDivisionID   
						  LEFT JOIN CompanyInfo CMP ON DM.numCompanyId = CMP.numCompanyId and CMP.numDomainID = @numDomainID  
						  LEFT JOIN dbo.AdditionalContactsInformation ACI ON ACI.numContactID = Mst.numContactID
						  LEFT JOIN [BizDocTemplate] BT ON BT.numDomainID = @numDomainID AND BT.numBizDocID = @numBizDocType 
							 AND ((BT.numBizDocTempID=ISNULL(Mst.numOppBizDocTempID, 0)) OR (ISNULL(BT.bitDefault,0)=1 AND ISNULL(BT.bitEnabled,0)=1))  
						  LEFT JOIN BillingTerms BTR ON BTR.numTermsID = ISNULL(Mst.intBillingDays,0)
						  JOIN divisionmaster div1 ON D.numDivisionID = div1.numDivisionID
						  JOIN companyinfo [Com1] ON com1.numCompanyID = div1.numCompanyID
				  WHERE  OBD.numOppBizDocsId = @numReferenceID AND  Mst.numDomainID = @numDomainID
				  SET @numReferenceType=1
			      SET @numReferenceID=(SELECT TOP 1 numOppId FROM OpportunityBizDocs WHERE numOppBizDocsId = @numReferenceID)
					EXEC USP_OPPGetOppAddressDetails @numReferenceID,@numDomainId	
             
            END
    END

GO
--exec USP_GetMirrorBizDocItems @numReferenceID=37056,@numReferenceType=1,@numDomainID=1

GO
/****** Object:  StoredProcedure [dbo].[USP_GetOpportunityList1]    Script Date: 03/06/2009 00:37:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- created by anoop jayaraj                                                               
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getopportunitylist1')
DROP PROCEDURE usp_getopportunitylist1
GO
CREATE PROCEDURE [dbo].[USP_GetOpportunityList1]                                                                    
	@numUserCntID numeric(9)=0,
	@numDomainID numeric(9)=0,
	@tintUserRightType tinyint=0,
	@tintSortOrder tinyint=4,
	@dtLastDate datetime,
	@OppType as tinyint,
	@CurrentPage int,
	@PageSize int,
	@TotRecs int output,
	@columnName as Varchar(50),
	@columnSortOrder as Varchar(10),
	@numDivisionID as numeric(9)=0,
	@bitPartner as bit=0,
	@ClientTimeZoneOffset as int,
	@inttype as tinyint,
	@vcRegularSearchCriteria varchar(1000)='',
	@vcCustomSearchCriteria varchar(1000)='',
	@SearchText VARCHAR(300) = '',
	@SortChar char(1)='0',
	@tintDashboardReminderType TINYINT = 0                                                              
AS                
BEGIN
         
	DECLARE @PageId as TINYINT = 0
	DECLARE @numFormId  AS INT
           
	IF @inttype = 1 
	BEGIN           
		SET @PageId =2  
		SET @numFormId=38          
	END
	ELSE IF @inttype = 2            
	BEGIN
		SET @PageId =6 
		SET @numFormId=40           
	END

	CREATE TABLE #tempForm 
	(
		tintOrder TINYINT,vcDbColumnName nvarchar(50),vcOrigDbColumnName nvarchar(50),vcFieldName nvarchar(50),vcAssociatedControlType nvarchar(50),vcListItemType varchar(3),
		numListID numeric(9),vcLookBackTableName varchar(50),bitCustomField BIT,numFieldId NUMERIC,bitAllowSorting BIT,bitAllowEdit BIT,bitIsRequired BIT,bitIsEmail BIT,
		bitIsAlphaNumeric BIT,bitIsNumeric BIT,bitIsLengthValidation BIT,intMaxLength int,intMinLength int,bitFieldMessage BIT,vcFieldMessage VARCHAR(500),ListRelID numeric(9),
		intColumnWidth int,bitAllowFiltering BIT,vcFieldDataType CHAR(1)
	)

	-- FIRST CHECK IF GRID COLUMNS ARE CONFIGURED
	DECLARE @Nocolumns AS TINYINT = 0  

	SELECT 
		@Nocolumns = ISNULL(SUM(TotalRow),0)  
	FROM
	(            
		SELECT 
			COUNT(*) TotalRow 
		FROM 
			View_DynamicColumns 
		WHERE 
			numFormId=@numFormId 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND numRelCntType = @numFormId
		UNION 
		SELECT 
			COUNT(*) TotalRow 
		FROM 
			View_DynamicCustomColumns 
		WHERE 
			numFormId=@numFormId 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND numRelCntType = @numFormId
	) TotalRows
	
	DECLARE @IsMasterConfAvailable BIT = 0
	DECLARE @numUserGroup NUMERIC(18,0)

	-- IF NoColumns=0 CHECK IF MASTER FORM CONFIGURATION IS CREATED BY ADMINISTRATOR
	IF @Nocolumns=0
	BEGIN
		SELECT @numUserGroup = numGroupID FROM UserMaster WHERE numUserDetailId = @numUserCntID 

		IF (SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = @numDomainID AND numFormID = @numFormId AND numRelCntType=@numFormId AND bitGridConfiguration = 1 AND numGroupID = @numUserGroup) > 0
		BEGIN
			SET @IsMasterConfAvailable = 1
		END
	END

	--IF MASTERCONFIGURATION IS AVAILABLE THEN LOAD IT OTHERWISE LOAD DEFAULT COLUMNS
	IF @IsMasterConfAvailable = 1
	BEGIN
		INSERT INTO DycFormConfigurationDetails 
		(
			numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth
		)
		SELECT 
			@numFormId,numFieldId,intColumnNum,intRowNum,@numDomainID,@numUserCntID,@numFormId,1,0,intColumnWidth
		FROM 
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId=@numFormId AND 
			View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicColumnsMasterConfig.numRelCntType=@numFormId AND 
			View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
			ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
		UNION
		SELECT 
			@numFormId,numFieldId,intColumnNum,intRowNum,@numDomainID,@numUserCntID,@numFormId,1,1,intColumnWidth
		FROM 
			View_DynamicCustomColumnsMasterConfig
		WHERE 
			View_DynamicCustomColumnsMasterConfig.numFormId=@numFormId AND 
			View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicCustomColumnsMasterConfig.numRelCntType=@numFormId AND 
			View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1

		INSERT INTO 
			#tempForm
		SELECT 
			(intRowNum + 1) as tintOrder, vcDbColumnName,	vcOrigDbColumnName,	ISNULL(vcCultureFieldName,vcFieldName), vcAssociatedControlType,
			vcListItemType, numListID, vcLookBackTableName,	bitCustom, numFieldId, bitAllowSorting,	bitInlineEdit, bitIsRequired,
			bitIsEmail,	bitIsAlphaNumeric, bitIsNumeric, bitIsLengthValidation,	intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,
			ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType 
		FROM 
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId=@numFormId AND 
			View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicColumnsMasterConfig.numRelCntType=@numFormId AND 
			View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
			ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
		UNION
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName, vcAssociatedControlType,'' as vcListItemType,numListID,'',
			bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,0,''
		FROM 
			View_DynamicCustomColumnsMasterConfig
		WHERE 
			View_DynamicCustomColumnsMasterConfig.numFormId=@numFormId AND 
			View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicCustomColumnsMasterConfig.numRelCntType=@numFormId AND 
			View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1
		ORDER BY 
			tintOrder ASC  
	END
	ELSE                                        
	BEGIN 
		IF @Nocolumns=0
		BEGIN
			INSERT INTO DycFormConfigurationDetails 
			(
				numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth
			)
			SELECT 
				@numFormId,numFieldId,0,Row_number() over(order by tintRow desc),@numDomainID,@numUserCntID,@numFormId,1,0,intColumnWidth
			FROM 
				View_DynamicDefaultColumns
			WHERE 
				numFormId=@numFormId 
				AND bitDefault=1 
				AND ISNULL(bitSettingField,0)=1 
				AND numDomainID=@numDomainID
			ORDER BY 
				tintOrder ASC  
		END
  

		INSERT INTO 
			#tempForm
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcOrigDbColumnName,ISNULL(vcCultureFieldName,vcFieldName),vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName,
			bitCustom,numFieldId,bitAllowSorting,bitInlineEdit,bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength,bitFieldMessage,
			vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType  
		FROM 
			View_DynamicColumns 
		WHERE 
			numFormId=@numFormId 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND ISNULL(bitSettingField,0)=1 
			AND ISNULL(bitCustom,0)=0  
			AND numRelCntType = @numFormId
		UNION
		SELECT 
			tintRow+1 AS tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName,vcAssociatedControlType,'' as vcListItemType,numListID,'',bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,0,'' 
		FROM 
			View_DynamicCustomColumns
		WHERE 
			numFormId=@numFormId 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND ISNULL(bitCustom,0)=1 
			AND numRelCntType = @numFormId
		ORDER BY 
			tintOrder ASC             
	END
	
	DECLARE  @strColumns AS VARCHAR(MAX)
	SET @strColumns = ' ADC.numContactId, DM.numDivisionID, ISNULL(DM.numTerID,0) as numTerID, Opp.numRecOwner, DM.tintCRMType, Opp.numOppID, ISNULL(Opp.numBusinessProcessID,0) AS numBusinessProcessID'
        
	
	DECLARE @tintOrder as tinyint  = 0                                                  
	DECLARE @vcFieldName as varchar(50)                                                      
	DECLARE @vcListItemType as varchar(3)                                                 
	DECLARE @vcListItemType1 as varchar(1)                                                     
	DECLARE @vcAssociatedControlType varchar(10)                                                      
	DECLARE @numListID AS numeric(9)                                                      
	DECLARE @vcDbColumnName varchar(30)                          
	DECLARE @WhereCondition varchar(2000) = ''                          
	DECLARE @vcLookBackTableName varchar(2000)                    
	DECLARE @bitCustom as bit              
	DECLARE @numFieldId as numeric  
	DECLARE @bitAllowSorting AS CHAR(1)   
	DECLARE @bitAllowEdit AS CHAR(1)                 
	DECLARE @vcColumnName AS VARCHAR(500)                  
	Declare @ListRelID AS NUMERIC(9) 
	DECLARE @SearchQuery AS VARCHAR(MAX) = ''   

	SELECT TOP 1
		@tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName, @vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,
		@numListID=numListID,@vcLookBackTableName=vcLookBackTableName,@bitCustom=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,@bitAllowEdit=bitAllowEdit,
		@ListRelID=ListRelID
	FROM 
		#tempForm 
	ORDER BY 
		tintOrder asc            
                                                   
	WHILE @tintOrder>0                                                      
	BEGIN 
		IF @bitCustom = 0            
		BEGIN 
			DECLARE @Prefix as varchar(5)                                               
			
			IF @vcLookBackTableName = 'AdditionalContactsInformation'                    
				SET @Prefix = 'ADC.'                    
			IF @vcLookBackTableName = 'DivisionMaster'                    
				SET @Prefix = 'DM.'                    
			IF @vcLookBackTableName = 'OpportunityMaster'                    
				SET @PreFix ='Opp.'      
			IF @vcLookBackTableName = 'OpportunityRecurring'
				SET @PreFix = 'OPR.'
			IF @vcLookBackTableName = 'CompanyInfo'
				SET @PreFix = 'cmp.'
			IF @vcLookBackTableName = 'ProjectProgress'
				SET @PreFix = 'PP'

			SET @vcColumnName= @vcDbColumnName+'~'+ convert(varchar(10),@numFieldId) +'~0'
      
			IF @vcAssociatedControlType='SelectBox'                                                      
			BEGIN  
				IF @vcDbColumnName = 'numStatus'
				BEGIN
					SET @strColumns=@strColumns+ ' ,(SELECT COUNT(OpportunityItems.numoppitemtCode) FROM OpportunityItems 
					WHERE OpportunityItems.bitItemPriceApprovalRequired=1 AND OpportunityItems.numOppId=Opp.numOppId) ApprovalMarginCount,ISNULL(Opp.intPromotionApprovalStatus,0) AS intPromotionApprovalStatus '
				END
				
				IF @vcDbColumnName='numPartner'
				BEGIN
					SET @strColumns=@strColumns+ ' ,(Opp.numPartner) '+' ['+ @vcColumnName+']'    
					SET @strColumns=@strColumns+ ' ,(SELECT D.vcPartnerCode+''-''+C.vcCompanyName FROM DivisionMaster AS D LEFT JOIN CompanyInfo AS C ON D.numCompanyID=C.numCompanyID WHERE D.numDivisionID=Opp.numPartner) AS vcPartner' 
				END
				ELSE IF @vcDbColumnName = 'numPartnerContact'
				BEGIN
					SET @strColumns=@strColumns+ ' ,(Opp.numPartenerContact) '+' ['+ @vcColumnName+']'    
					SET @strColumns=@strColumns+ ' ,(SELECT A.vcFirstName+'' ''+ A.vcLastName FROM AdditionalContactsInformation AS A WHERE A.numContactId=Opp.numPartenerContact) AS vcPartnerContact' 
				END
				ELSE IF @vcDbColumnName = 'tintSource'
				BEGIN
					SET @strColumns = @strColumns + ',dbo.fn_GetOpportunitySourceValue(ISNULL(Opp.tintSource,0),ISNULL(Opp.tintSourceType,0),Opp.numDomainID) ' + ' [' + @vcColumnName + ']'

					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'dbo.fn_GetOpportunitySourceValue(ISNULL(Opp.tintSource,0),ISNULL(Opp.tintSourceType,0),Opp.numDomainID) LIKE ''%' + @SearchText + '%'''
					END
				END
				ELSE IF @vcDbColumnName = 'numContactId'
				BEGIN
					SET @strColumns = @strColumns + ',dbo.fn_GetContactName('+@Prefix+@vcDbColumnName+') ['+ @vcColumnName+']'     
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'dbo.fn_GetContactName('+@Prefix+@vcDbColumnName+') LIKE ''%' + @SearchText + '%'''
					END             
				END
				ELSE if @vcListItemType='LI'                                                       
				BEGIN
					SET @strColumns = @strColumns+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName+']'                                                      
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'L' + CONVERT(VARCHAR(3),@tintOrder) + '.vcData LIKE ''%' + @SearchText + '%'''
					END

					SET @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID='+@vcDbColumnName                                                      
				END                                                      
				ELSE IF @vcListItemType='S'                                                       
				BEGIN
					SET @strColumns = @strColumns+',S'+ convert(varchar(3),@tintOrder)+'.vcState'+' ['+ @vcColumnName+']'   
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'S'+ convert(varchar(3),@tintOrder)+'.vcState LIKE ''%' + @SearchText + '%'''
					END
					                                                   
					SET @WhereCondition= @WhereCondition +' left join State S'+ convert(varchar(3),@tintOrder)+ ' on S'+convert(varchar(3),@tintOrder)+ '.numStateID='+@vcDbColumnName                                                      
				END     
				ELSE IF @vcListItemType = 'BP'
				BEGIN
					SET @strColumns = @strColumns + ',SPLM.Slp_Name' + ' ['	+ @vcColumnName + ']'

					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'SPLM.Slp_Name LIKE ''%' + @SearchText + '%'''
					END

					SET @WhereCondition = @WhereCondition + ' LEFT JOIN Sales_process_List_Master SPLM on SPLM.Slp_Id = OPP.numBusinessProcessID '
				END
				ELSE IF @vcListItemType='PP'
				BEGIN
					SET @strColumns = @strColumns+', CASE WHEN PP.numOppID >0 then ISNULL(PP.intTotalProgress,0) ELSE dbo.GetListIemName(OPP.numPercentageComplete) END '+' ['+ @vcColumnName+']'

					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + '(CASE WHEN PP.numOppID >0 then CONCAT(ISNULL(PP.intTotalProgress,0),''%'') ELSE dbo.GetListIemName(OPP.numPercentageComplete) END) LIKE ''%' + @SearchText + '%'''
					END
				END  
				ELSE IF @vcListItemType='T'                                                       
				BEGIN
					SET @strColumns = @strColumns+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName+']' 
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'L'+ convert(varchar(3),@tintOrder)+'.vcData LIKE ''%' + @SearchText + '%'''
					END
					                                                     
					SET @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID='+@vcDbColumnName                                                      
				END                     
				ELSE IF @vcListItemType='U'                                                   
				BEGIN
					SET @strColumns = @strColumns + ',dbo.fn_GetContactName('+@Prefix+@vcDbColumnName+') ['+ @vcColumnName+']'     
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'dbo.fn_GetContactName('+@Prefix+@vcDbColumnName+') LIKE ''%' + @SearchText + '%'''
					END              
				END  
				ELSE IF @vcListItemType = 'WI'
				BEGIN
					SET @strColumns = @strColumns + ', Case when isnull(OL.numSiteID,0)>0 then Sites.vcSiteName else WebAPI.vcProviderName end [' + @vcColumnName + ']'
				  
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + '(Case when isnull(OL.numSiteID,0)>0 then Sites.vcSiteName else WebAPI.vcProviderName end) LIKE ''%' + @SearchText + '%'''
					END 

					SET @WhereCondition= @WhereCondition +' left join WebAPI on WebAPI.WebApiId=isnull(OL.numWebApiId,0)
															left join Sites on isnull(OL.numSiteID,0)=Sites.numSIteID and Sites.numDomainID=' + convert(varchar(15),@numDomainID)                                                    
			   END                  
			END               
			ELSE IF @vcAssociatedControlType='DateField'                                                      
			BEGIN  
				SET @strColumns=@strColumns+',case when convert(varchar(11),'+@Prefix+@vcDbColumnName+')= convert(varchar(11),getdate()) then ''<b><font color=red>Today</font></b>'''      
				SET @strColumns=@strColumns+'when convert(varchar(11),'+@Prefix+@vcDbColumnName+')= convert(varchar(11),dateadd(day,-1,getdate())) then ''<b><font color=purple>YesterDay</font></b>'''      
				SET @strColumns=@strColumns+'when convert(varchar(11),'+@Prefix+@vcDbColumnName+')= convert(varchar(11),dateadd(day,1,getdate())) then''<b><font color=orange>Tommorow</font></b>'' '      
				SET @strColumns=@strColumns+'else dbo.FormatedDateFromDate('+@Prefix+@vcDbColumnName+','+convert(varchar(10),@numDomainId)+') end  ['+ @vcColumnName+']'               

				IF LEN(@SearchText) > 0
				BEGIN
					SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'dbo.FormatedDateFromDate('+@Prefix+@vcDbColumnName+','+ CONVERT(VARCHAR(10),@numDomainId)+') LIKE ''%' + @SearchText + '%'''
				END
			END              
			ELSE IF @vcAssociatedControlType='TextBox'                                                      
			BEGIN  
				SET @strColumns = @strColumns + ','+ CASE 
														WHEN @vcDbColumnName='numAge' then 'year(getutcdate())-year(bintDOB)'                   
														WHEN @vcDbColumnName='monPAmount' then 'isnull((SELECT varCurrSymbol FROM [Currency] WHERE numCurrencyID = Opp.[numCurrencyID]),'''') + '' ''+ CONVERT(VARCHAR,CONVERT(MONEY,monPAmount),1)'
														WHEN @vcDbColumnName='numDivisionID' then 'DM.numDivisionID'
														WHEN @vcDbColumnName = 'tintOppStatus' THEN 'CASE tintOppStatus WHEN 0 then ''Open'' WHEN 1 THEN ''Closed-Won'' WHEN 2 THEN ''Closed-Lost'' END '
														WHEN @vcDbColumnName = 'tintOppType' THEN 'CASE tintOppType WHEN 1 then ''Sales'' WHEN 2 THEN ''Purchase'' Else ''NA'' END ' 
														ELSE @Prefix + @vcDbColumnName 
													END +' ['+ @vcColumnName+']'  
				
				IF LEN(@SearchText) > 0
				BEGIN
					SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + CASE 
														WHEN @vcDbColumnName='numAge' then 'year(getutcdate())-year(bintDOB)'                   
														WHEN @vcDbColumnName='monPAmount' then 'isnull((SELECT varCurrSymbol FROM [Currency] WHERE numCurrencyID = Opp.[numCurrencyID]),'''') + '' ''+ CONVERT(VARCHAR,CONVERT(MONEY,monPAmount),1)'
														WHEN @vcDbColumnName='numDivisionID' then 'DM.numDivisionID'
														WHEN @vcDbColumnName = 'tintOppStatus' THEN 'CASE tintOppStatus WHEN 0 then ''Open'' WHEN 1 THEN ''Closed-Won'' WHEN 2 THEN ''Closed-Lost'' END '
														WHEN @vcDbColumnName = 'tintOppType' THEN 'CASE tintOppType WHEN 1 then ''Sales'' WHEN 2 THEN ''Purchase'' Else ''NA'' END ' 
														ELSE @Prefix + @vcDbColumnName 
													END + ' LIKE ''%' + @SearchText + '%'''
				END              
			END  
			ELSE IF @vcAssociatedControlType='TextArea'                                              
			BEGIN  
				SET @strColumns=@strColumns+','+  @Prefix + @vcDbColumnName +' ['+ @vcColumnName+']' 

				IF LEN(@SearchText) > 0
				BEGIN
					SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + @Prefix + @vcDbColumnName + ' LIKE ''%' + @SearchText + '%'''
				END     
			END
			ELSE IF @vcAssociatedControlType='Label'                                              
			BEGIN  
				SET @strColumns = @strColumns + ',' + CASE 
												WHEN @vcDbColumnName = 'CalAmount' THEN '[dbo].[getdealamount](Opp.numOppId,Getutcdate(),0)'
												WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'monDealAmount' THEN 'ISNULL((SELECT SUM(ISNULL(monDealAmount,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)'
												WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'monAmountPaid' THEN 'ISNULL((SELECT SUM(ISNULL(monAmountPaid,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)'
												WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'vcBizDocsList' THEN 'ISNULL((SELECT SUBSTRING((SELECT ''$^$'' + CAST(numOppBizDocsId AS VARCHAR(18)) +''#^#''+ vcBizDocID
												FROM OpportunityBizDocs WHERE numOppId=Opp.numOppId FOR XML PATH('''')),4,200000)),'''')'
												else @Prefix + @vcDbColumnName 
											END +' ['+ @vcColumnName+']'     
											
				IF LEN(@SearchText) > 0
				BEGIN
					SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + CASE 
												WHEN @vcDbColumnName = 'CalAmount' THEN '[dbo].[getdealamount](Opp.numOppId,Getutcdate(),0)'
												WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'monDealAmount' THEN 'ISNULL((SELECT SUM(ISNULL(monDealAmount,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)'
												WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'monAmountPaid' THEN 'ISNULL((SELECT SUM(ISNULL(monAmountPaid,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)'
												WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'vcBizDocsList' THEN 'ISNULL((SELECT SUBSTRING((SELECT ''$^$'' + CAST(numOppBizDocsId AS VARCHAR(18)) +''#^#''+ vcBizDocID
												FROM OpportunityBizDocs WHERE numOppId=Opp.numOppId FOR XML PATH('''')),4,200000)),'''')'
												else @Prefix + @vcDbColumnName
											END + ' LIKE ''%' + @SearchText + '%'''
				END           
			END
  			ELSE IF @vcAssociatedControlType='Popup' and @vcDbColumnName='numShareWith'                                                   
			BEGIN  
				SET @strColumns = @strColumns+',(SELECT SUBSTRING(
				(SELECT '','' + A.vcFirstName+'' ''+A.vcLastName + CASE WHEN ISNULL(SR.numContactType,0)>0 THEN ''('' + dbo.fn_GetListItemName(SR.numContactType) + '')'' ELSE '''' END
				FROM ShareRecord SR JOIN UserMaster UM ON UM.numUserDetailId=SR.numAssignedTo 
											JOIN AdditionalContactsInformation A ON UM.numUserDetailId=A.numContactID 
						WHERE SR.numDomainID=Opp.numDomainID AND SR.numModuleID=3 AND SR.numRecordID=Opp.numOppId
							AND UM.numDomainID=Opp.numDomainID and UM.numDomainID=A.numDomainID FOR XML PATH('''')),2,200000)) ['+ @vcColumnName+']'    
				
				IF LEN(@SearchText) > 0
				BEGIN
					SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + '(SELECT SUBSTRING(
						(SELECT '','' + A.vcFirstName+'' ''+A.vcLastName + CASE WHEN ISNULL(SR.numContactType,0)>0 THEN ''('' + dbo.fn_GetListItemName(SR.numContactType) + '')'' ELSE '''' END
						FROM ShareRecord SR JOIN UserMaster UM ON UM.numUserDetailId=SR.numAssignedTo 
													JOIN AdditionalContactsInformation A ON UM.numUserDetailId=A.numContactID 
								WHERE SR.numDomainID=Opp.numDomainID AND SR.numModuleID=3 AND SR.numRecordID=Opp.numOppId
									AND UM.numDomainID=Opp.numDomainID and UM.numDomainID=A.numDomainID FOR XML PATH('''')),2,200000)) LIKE ''%' + @SearchText + '%'''
				END            
			END  
		END               
		Else                          
		BEGIN 
			SET @vcColumnName= @vcDbColumnName +'~'+ convert(varchar(10),@numFieldId)+'~1'
               
			IF @vcAssociatedControlType = 'TextBox' or @vcAssociatedControlType = 'TextArea'          
			BEGIN              
				SET @strColumns= @strColumns+',CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value  ['+ @vcColumnName + ']'               
				SET @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values_Opp CFW'+ convert(varchar(3),@tintOrder)+ '             
				on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=Opp.numOppid   '                                                     
			END            
			ELSE IF @vcAssociatedControlType = 'CheckBox'         
			BEGIN          
				SET @strColumns = @strColumns + ',case when isnull(CFW'+ CONVERT(VARCHAR(3),@tintOrder)+'.Fld_Value,0)=0 then ''No'' when isnull(CFW'+ CONVERT(VARCHAR(3),@tintOrder)+'.Fld_Value,0)=1 then ''Yes'' end   ['+ @vcColumnName + ']'             
				SET @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values_Opp CFW'+ convert(varchar(3),@tintOrder)+ '           
					on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=Opp.numOppid   '                                                   
			END          
			ELSE IF @vcAssociatedControlType = 'DateField'       
			BEGIN            
				set @strColumns= @strColumns+',dbo.FormatedDateFromDate(CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value,'+convert(varchar(10),@numDomainId)+')  ['+ @vcColumnName + ']'               
				set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values_Opp CFW'+ convert(varchar(3),@tintOrder)+ '             
				on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=Opp.numOppid    '                                                     
			END            
			ELSE IF @vcAssociatedControlType = 'SelectBox'         
			BEGIN            
				SET @vcDbColumnName = 'DCust'+Convert(varchar(10),@numFieldId)    
				        
				SET @strColumns = @strColumns+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName + ']'                                                      
				SET @WhereCondition = @WhereCondition +' left Join CFW_FLD_Values_Opp CFW'+ convert(varchar(3),@tintOrder)+ '             
									on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=Opp.numOppid     '                                                     
				SET @WhereCondition = @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID=CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value' 
			END
			ELSE
			BEGIN
				SET @strColumns = @strColumns + CONCAT(',dbo.GetCustFldValueOpp(',@numFieldId,',opp.numOppID)') + ' [' + @vcColumnName + ']'
			END
		   
			IF LEN(@SearchText) > 0
			BEGIN
				SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + CONCAT('dbo.GetCustFldValueOpp(',@numFieldId,',opp.numOppID) LIKE ''%',@SearchText,'%''')
			END        
		END 
  
		SELECT TOP 1 
			@tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName,@vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,
			@numListID=numListID,@vcLookBackTableName=vcLookBackTableName,@bitCustom=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,@bitAllowEdit=bitAllowEdit,
			@ListRelID=ListRelID
		FROM 
			#tempForm 
		WHERE 
			tintOrder > @tintOrder-1 
		ORDER BY 
			tintOrder asc            
 
		IF @@rowcount=0 
			SET @tintOrder=0 
	END                            
		
	 
	DECLARE @strShareRedordWith AS VARCHAR(300);
	SET @strShareRedordWith=' Opp.numOppId in (select SR.numRecordID FROM ShareRecord SR WHERE SR.numDomainID=' + CONVERT(VARCHAR(15),@numDomainID) + ' AND SR.numModuleID=3 AND SR.numAssignedTo=' + CONVERT(VARCHAR(15),@numUserCntId) + ') '
                                             
	DECLARE @strSql AS VARCHAR(MAX) = ''                                       
                                                                                 
	SET @strSql = ' FROM 
						OpportunityMaster Opp                                                                     
					INNER JOIN 
						AdditionalContactsInformation ADC                                                                     
					ON
						Opp.numContactId = ADC.numContactId                                  
					INNER JOIN 
						DivisionMaster DM                                                                     
					ON 
						Opp.numDivisionId = DM.numDivisionID AND ADC.numDivisionId = DM.numDivisionID                                                                     
					INNER JOIN 
						CompanyInfo cmp                                                                     
					ON 
						DM.numCompanyID = cmp.numCompanyId                                                                         
					LEFT JOIN 
						AdditionalContactsInformation ADC1 
					ON 
						ADC1.numContactId=Opp.numRecOwner 
					LEFT JOIN 
						ProjectProgress PP 
					ON 
						PP.numOppID = Opp.numOppID
					LEFT JOIN 
						OpportunityLinking OL 
					on 
						OL.numChildOppID=Opp.numOppId ' + @WhereCondition
                                     

	-------Change Row Color-------
	DECLARE @vcCSOrigDbCOlumnName AS VARCHAR(50) = ''
	DECLARE @vcCSLookBackTableName AS VARCHAR(50) = ''
	DECLARE @vcCSAssociatedControlType AS VARCHAR(50) = ''

	CREATE TABLE #tempColorScheme
	(
		vcOrigDbCOlumnName VARCHAR(50),
		vcLookBackTableName VARCHAR(50),
		vcFieldValue VARCHAR(50),
		vcFieldValue1 VARCHAR(50),
		vcColorScheme VARCHAR(50),
		vcAssociatedControlType VARCHAR(50)
	)

	INSERT INTO 
		#tempColorScheme 
	SELECT 
		DFM.vcOrigDbCOlumnName,
		DFM.vcLookBackTableName,
		DFCS.vcFieldValue,
		DFCS.vcFieldValue1,
		DFCS.vcColorScheme,
		DFFM.vcAssociatedControlType 
	FROM 
		DycFieldColorScheme DFCS 
	JOIN 
		DycFormField_Mapping DFFM 
	ON 
		DFCS.numFieldID=DFFM.numFieldID
	JOIN 
		DycFieldMaster DFM 
	ON 
		DFM.numModuleID=DFFM.numModuleID 
		AND DFM.numFieldID=DFFM.numFieldID
	WHERE 
		DFCS.numDomainID=@numDomainID 
		AND DFFM.numFormID=@numFormId 
		AND DFCS.numFormID=@numFormId 
		AND ISNULL(DFFM.bitAllowGridColor,0)=1

	IF(SELECT COUNT(*) FROM #tempColorScheme)>0
	BEGIN
		SELECT TOP 1 @vcCSOrigDbCOlumnName=vcOrigDbCOlumnName,@vcCSLookBackTableName=vcLookBackTableName,@vcCSAssociatedControlType=vcAssociatedControlType FROM #tempColorScheme
	END   

	IF len(@vcCSOrigDbCOlumnName)>0 and len(@vcCSLookBackTableName)>0
	BEGIN
		SET @strColumns = @strColumns + ',tCS.vcColorScheme'
	END

	IF len(@vcCSOrigDbCOlumnName)>0 and len(@vcCSLookBackTableName)>0
	BEGIN
		IF @vcCSLookBackTableName = 'AdditionalContactsInformation'                  
			SET @Prefix = 'ADC.'                  
		IF @vcCSLookBackTableName = 'DivisionMaster'                  
			SET @Prefix = 'DM.'
		IF @vcCSLookBackTableName = 'CompanyInfo'                  
			SET @Prefix = 'CMP.' 
		IF @vcCSLookBackTableName = 'OpportunityMaster'                  
			SET @Prefix = 'Opp.'
		
		IF @vcCSAssociatedControlType='DateField'
		BEGIN
			SET @strSql=@strSql+' LEFT JOIN #tempColorScheme tCS on CONVERT(VARCHAR(10),' + @Prefix + @vcCSOrigDbCOlumnName + ',111) >= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue as int),GETUTCDATE()),111)
					and CONVERT(VARCHAR(10),' + @Prefix + @vcCSOrigDbCOlumnName + ',111) <= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue1 as int),GETUTCDATE()),111)'
		END
		ELSE IF @vcCSAssociatedControlType='SelectBox' OR @vcCSAssociatedControlType='ListBox' OR @vcCSAssociatedControlType='CheckBox'
		BEGIN
			SET @strSql=@strSql+' LEFT JOIN #tempColorScheme tCS on tCS.vcFieldValue=' + @Prefix + @vcCSOrigDbCOlumnName
		END
	END      

            
	IF @columnName like 'CFW.Cust%'             
	BEGIN
		SET @strSql = @strSql +' LEFT JOIN CFW_Fld_Values_Opp CFW on CFW.RecId=Opp.numOppid  and CFW.fld_id= ' + REPLACE(@columnName,'CFW.Cust','') +' '                                                     
		SET @columnName='CFW.Fld_Value'   
	END
	
	ELSE IF @columnName like 'DCust%'            
	BEGIN            
		SET @strSql = @strSql + ' left Join CFW_Fld_Values_Opp CFW on CFW.RecId=Opp.numOppid   and CFW.fld_id= '+replace(@columnName,'DCust','')                                                                                               
		SET @strSql = @strSql + ' left Join ListDetails LstCF on LstCF.numListItemID=CFW.Fld_Value  '            
		set @columnName='LstCF.vcData'                   
	END
                                        
	IF @bitPartner=1 
	BEGIN
		SET @strSql=@strSql+' LEFT JOIN OpportunityContact OppCont ON OppCont.numOppId=Opp.numOppId'                                                                               
	END

	SET @strSql = @strSql +' WHERE Opp.tintOppstatus=0 AND DM.numDomainID=' + CONVERT(VARCHAR(15),@numDomainID) + ' AND Opp.tintOppType= ' + convert(varchar(1),@OppType)                                                              

	IF @SortChar <> '0' 
		SET @strSql = @strSql + ' And Opp.vcPOppName like ''' + @SortChar + '%'''
                                
	IF @numDivisionID <> 0 
	BEGIN
		SET @strSql = @strSql + ' AND DM.numDivisionID =' + CONVERT(VARCHAR(15),@numDivisionID)                              
    END
	                                                                
	IF @tintUserRightType=1 
	BEGIN
		SET @strSql = @strSql + ' AND (Opp.numRecOwner = '
							+ CONVERT(VARCHAR(15),@numUserCntID) 
							+ ' or Opp.numAssignedTo= '
							+ convert(varchar(15),@numUserCntID)  
							+' or  Opp.numOppId in (select distinct(numOppId) from OpportunityStageDetails where numAssignTo ='
							+ convert(varchar(15),@numUserCntID)+')'
							+ case when @bitPartner=1 then ' or ( OppCont.bitPartner=1 and OppCont.numContactId='+convert(varchar(15),@numUserCntID)+ ')' else '' end 
							+ ' or ' + @strShareRedordWith +')'
    END                                        
	ELSE IF @tintUserRightType=2 
	BEGIN
		SET @strSql = @strSql + ' AND (DM.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '
							+ convert(varchar(15),@numUserCntID)+' ) or DM.numTerID=0 or Opp.numAssignedTo= '
							+ convert(varchar(15),@numUserCntID)  +' or  Opp.numOppId in (select distinct(numOppId) from OpportunityStageDetails where numAssignTo ='
							+ convert(varchar(15),@numUserCntID)+')' + ' or ' + @strShareRedordWith +')'                                                                        
	END           
	
	                                                  
	IF @tintSortOrder=1 
	BEGIN 
		SET @strSql = @strSql + ' AND (Opp.numRecOwner = ' 
							+ CONVERT(VARCHAR(15),@numUserCntID) 
							+ ' or Opp.numAssignedTo= '+ convert(varchar(15),@numUserCntID) 
							+ ' or  Opp.numOppId in (select distinct(numOppId) from OpportunityStageDetails where numAssignTo ='
							+ convert(varchar(15),@numUserCntID)+')'
							+ case when @bitPartner=1 then ' or ( OppCont.bitPartner=1 and OppCont.numContactId='+convert(varchar(15),@numUserCntID)+ ')' else '' end 
							+ ' or ' + @strShareRedordWith +')'                        
    END
	ELSE IF @tintSortOrder=2  
	BEGIN
		SET @strSql = @strSql + 'AND (Opp.numRecOwner in (select A.numContactID from AdditionalContactsInformation A where A.numManagerID='
							+ convert(varchar(15),@numUserCntID) 
							+') or Opp.numAssignedTo in (select A.numContactID from AdditionalContactsInformation A where A.numManagerID='
							+ convert(varchar(15),@numUserCntID)  
							+') or  Opp.numOppId in (select distinct(numOppId) from OpportunityStageDetails where numAssignTo in (select A.numContactID from AdditionalContactsInformation A where A.numManagerID='
							+ convert(varchar(15),@numUserCntID)+'))'
							+ case when @bitPartner=1 then ' or ( OppCont.bitPartner=1 and OppCont.numContactId in (select A.numContactID from AdditionalContactsInformation A where A.numManagerID='+ convert(varchar(15),@numUserCntID)+ '))' else '' end 
							+ ' or ' + @strShareRedordWith +')'                                                                    
	END
	ELSE IF @tintSortOrder=4  
	BEGIN
		SET @strSql=@strSql + ' AND Opp.bintCreatedDate> '''+convert(varchar(20),dateadd(day,-8,@dtLastDate))+''''                                                                    
	END
                                                 

	IF @vcRegularSearchCriteria <> '' 
	BEGIN
		SET @strSql = @strSql + ' AND ' + @vcRegularSearchCriteria 
	END

	IF @vcCustomSearchCriteria <> '' 
	BEGIN
		SET @strSql = @strSql + ' AND Opp.numOppid IN (SELECT DISTINCT CFW.RecId FROM CFW_Fld_Values_Opp CFW where ' + @vcCustomSearchCriteria + ')'
	END

	IF LEN(@SearchQuery) > 0
	BEGIN
		SET @strSql = @strSql + ' AND (' + @SearchQuery + ') '
	END

	IF ISNULL(@tintDashboardReminderType,0) = 4
	BEGIN
		SET @strSql = CONCAT(@strSql,' AND Opp.numOppID IN (','SELECT DISTINCT
																	OMInner.numOppID
																FROM 
																	StagePercentageDetails SPDInner
																INNER JOIN 
																	OpportunityMaster OMInner
																ON 
																	SPDInner.numOppID=OMInner.numOppId 
																WHERE 
																	SPDInner.numDomainId=',@numDOmainID,'
																	AND OMInner.numDomainId=',@numDOmainID,' 
																	AND OMInner.tintOppType = 1
																	AND ISNULL(OMInner.tintOppStatus,0) = 0
																	AND tinProgressPercentage <> 100',') ')

	END
	ELSE IF ISNULL(@tintDashboardReminderType,0) = 18
	BEGIN
		SET @strSql = CONCAT(@strSql,' AND Opp.numOppID IN (','SELECT DISTINCT
																	OMInner.numOppID
																FROM 
																	StagePercentageDetails SPDInner
																INNER JOIN 
																	OpportunityMaster OMInner
																ON 
																	SPDInner.numOppID=OMInner.numOppId 
																WHERE 
																	SPDInner.numDomainId=',@numDOmainID,'
																	AND OMInner.numDomainId=',@numDOmainID,' 
																	AND OMInner.tintOppType = 2
																	AND ISNULL(OMInner.tintOppStatus,0) = 0
																	AND tinProgressPercentage <> 100',') ')

	END

	DECLARE @firstRec AS INTEGER                                                              
	DECLARE @lastRec AS INTEGER    
	                                                 
	SET @firstRec= (@CurrentPage-1) * @PageSize                             
	SET @lastRec= (@CurrentPage*@PageSize+1)                                                                    


    -- GETS ROWS COUNT
	DECLARE @strTotal NVARCHAR(MAX) = 'SELECT @TotalRecords = COUNT(*) ' + @strSql
	exec sp_executesql @strTotal, N'@TotalRecords INT OUT', @TotRecs OUT
               
                              
	-- EXECUTE FINAL QUERY
	DECLARE @strFinal AS VARCHAR(MAX)
	SET @strFinal = CONCAT('SELECT ROW_NUMBER() OVER(ORDER BY ',@columnName,' ',@columnSortOrder,') ID, ',@strColumns,' INTO #tempTable',@strSql,'; SELECT * FROM #tempTable WHERE ID > ',@firstRec,' and ID <',@lastRec,'; DROP TABLE #tempTable;')
	PRINT @strFinal
	EXEC (@strFinal) 

	SELECT * FROM #tempForm

	DROP TABLE #tempForm
	DROP TABLE #tempColorScheme
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetPriceLevelItemPrice')
DROP PROCEDURE USP_GetPriceLevelItemPrice
GO
CREATE PROCEDURE [dbo].[USP_GetPriceLevelItemPrice]                                        
@numItemCode as numeric(9),
@numDomainID as numeric(9)=0,            
--@numPricingID as numeric(9)=0,
@numWareHouseItemID as numeric(9),
@numDivisionID as numeric(9)=0                  
as                 
BEGIN
	DECLARE @monListPrice AS MONEY;	SET @monListPrice=0
	DECLARE @monVendorCost AS MONEY;	SET @monVendorCost=0

	IF((@numWareHouseItemID>0) and exists(select * from item where numItemCode=@numItemCode and charItemType='P'))      
	BEGIN      
		SELECT 
			@monListPrice=ISNULL(monWListPrice,0) 
		FROM 
			WareHouseItems 
		WHERE 
			numWareHouseItemID=@numWareHouseItemID      
		
		IF @monListPrice=0 
		BEGIN
			SELECT @monListPrice=monListPrice FROM Item WHERE numItemCode=@numItemCode       
		END
	END      
	ELSE      
	BEGIN      
		 select @monListPrice=monListPrice from Item where numItemCode=@numItemCode      
	END 

	SELECT @monVendorCost = dbo.[fn_GetVendorCost](@numItemCode)

	SELECT
		[numPricingID],
		[numPriceRuleID],
		[intFromQty],
		[intToQty],
		[tintRuleType],
		[tintDiscountType],
		ISNULL(NULLIF(pnt.vcPriceLevelName, ''), CONCAT('Price Level ',rowId)) AS [vcName],
		decDiscount
	FROM
	(
		SELECT  
			ROW_NUMBER() OVER(ORDER BY numPricingID) as rowId,
			[numPricingID],
			[numPriceRuleID],
			[intFromQty],
			[intToQty],
			[tintRuleType],
			[tintDiscountType],
			CASE 
				WHEN tintRuleType=1 AND tintDiscountType=1 --Deduct from List price & Percentage
					THEN @monListPrice - (@monListPrice * ( decDiscount /100))
				WHEN tintRuleType=1 AND tintDiscountType=2 --Deduct from List price & Flat discount
					THEN @monListPrice - decDiscount
				WHEN tintRuleType=2 AND tintDiscountType=1 --Add to Primary Vendor Cost & Percentage
					THEN @monVendorCost + (@monVendorCost * ( decDiscount /100))
				WHEN tintRuleType=2 AND tintDiscountType=2 --Add to Primary Vendor Cost & Flat discount
					THEN @monVendorCost + decDiscount
				WHEN tintRuleType=3 --Named Price
					THEN decDiscount
				WHEN tintDiscountType=3 --Named Price
					THEN decDiscount
			END AS decDiscount
		FROM
			[PricingTable]
		WHERE
			ISNULL(numItemCode,0)=@numItemCode
	) T1
	LEFT JOIN 
		PricingNamesTable pnt ON pnt.tintPriceLevel = T1.rowId AND pnt.numDomainID=@numDomainID
	WHERE 
		rowId IS NOT NULL 
		OR (pnt.vcPriceLevelName IS NOT NULL AND pnt.vcPriceLevelName <> '')
	ORDER BY
		T1.rowId
			
  
	DECLARE @numRelationship AS NUMERIC(9)
	DECLARE @numProfile AS NUMERIC(9)

	SELECT 
		@numRelationship=numCompanyType,
		@numProfile=vcProfile 
	FROM 
		DivisionMaster D                  
	JOIN 
		CompanyInfo C 
	ON 
		C.numCompanyId=D.numCompanyID
	WHERE 
		numDivisionID =@numDivisionID       

  
	SELECT 
		PT.numPricingID,
		PT.numPriceRuleID,
		ISNULL(PT.intFromQty,0) AS intFromQty,
		ISNULL(PT.intToQty,0) AS intToQty,
		PT.tintRuleType,
		PT.tintDiscountType,
		ISNULL(PT.vcName,'') vcName,
		CASE 
			WHEN PT.tintRuleType=1 AND PT.tintDiscountType=1 --Deduct from List price & Percentage
				THEN @monListPrice - (@monListPrice * ( PT.decDiscount /100))
            WHEN PT.tintRuleType=1 AND PT.tintDiscountType=2 --Deduct from List price & Flat discount
				THEN @monListPrice - PT.decDiscount
            WHEN PT.tintRuleType=2 AND PT.tintDiscountType=1 --Add to Primary Vendor Cost & Percentage
				THEN @monVendorCost + (@monVendorCost * ( PT.decDiscount /100))
            WHEN PT.tintRuleType=2 AND PT.tintDiscountType=2 --Add to Primary Vendor Cost & Flat discount
				THEN @monVendorCost + PT.decDiscount 
		END AS decDiscount
	FROM 
		Item I
    JOIN 
		PriceBookRules P 
	ON 
		I.numDomainID = P.numDomainID
    LEFT JOIN 
		PriceBookRuleDTL PDTL 
	ON 
		P.numPricRuleID = PDTL.numRuleID
    LEFT JOIN 
		PriceBookRuleItems PBI 
	ON 
		P.numPricRuleID = PBI.numRuleID
    LEFT JOIN 
		[PriceBookPriorities] PP 
	ON 
		PP.[Step2Value] = P.[tintStep2] 
		AND PP.[Step3Value] = P.[tintStep3]
    JOIN 
		[PricingTable] PT 
	ON 
		P.numPricRuleID = PT.numPriceRuleID
	WHERE
		I.numItemCode = @numItemCode 
		AND P.tintRuleFor=1 
		AND P.tintPricingMethod = 1
		AND 
		(
		((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 1 AND PDTL.numValue = @numDivisionID)) -- Priority 1
		OR ((tintStep2 = 2 AND PBI.numValue = I.numItemClassification) AND (tintStep3= 1 AND PDTL.numValue = @numDivisionID)) -- Priority 2
		OR ((tintStep2 = 3) AND (tintStep3 = 1 AND PDTL.numValue = @numDivisionID)) -- Priority 3

		OR ((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 4
		OR ((tintStep2 = 2 AND PBI.numValue = I.numItemClassification) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 5
		OR ((tintStep2 = 3) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 6

		OR ((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 3)) -- Priority 7
		OR ((tintStep2 = 2 AND PBI.numValue = I.numItemClassification) AND (tintStep3 = 3)) -- Priority 8
		OR ((tintStep2 = 3) and (tintStep3 = 3)) -- Priority 9
		)
	ORDER BY PP.Priority ASC
END
      
--USP_GetPriceTable 360
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetPriceTable')
DROP PROCEDURE USP_GetPriceTable
GO
CREATE PROCEDURE USP_GetPriceTable
   @numPriceRuleID NUMERIC,
    @numItemCode AS NUMERIC 
	,@numDomainID AS NUMERIC
AS 
BEGIN

	WITH PricingCte AS 
	(
		SELECT  pt.[numPricingID],
		pt.[numPriceRuleID],
		pt.[intFromQty],
		pt.[intToQty],
		pt.[tintRuleType],
		pt.[tintDiscountType],
		CAST(CONVERT(DECIMAL(18, 4), pt.[decDiscount]) AS VARCHAR) decDiscount,
		ROW_NUMBER() OVER(ORDER BY [numPricingID]) as rowId
		FROM    [PricingTable] pt
		WHERE   ISNULL([numPriceRuleID],0) = @numPriceRuleID AND ISNULL(numItemCode,0)=@numItemCode
	)
	SELECT  [numPricingID]
	,[numPriceRuleID]
	,[intFromQty]
	,[intToQty]
	,[tintRuleType]
	,[tintDiscountType]
	,decDiscount
	,ISNULL(NULLIF(pnt.vcPriceLevelName, ''), CONCAT('Price Level ',rowId)) AS [vcName]
	FROM PricingCte
	LEFT JOIN PricingNamesTable pnt ON pnt.tintPriceLevel = PricingCte.rowId AND pnt.numDomainID=@numDomainID
	WHERE rowId IS NOT NULL OR (pnt.vcPriceLevelName IS NOT NULL AND pnt.vcPriceLevelName <> '')
	ORDER BY [numPriceRuleID] 

END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON

IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetProductFilters' ) 
                    DROP PROCEDURE USP_GetProductFilters
Go

CREATE PROCEDURE [dbo].[USP_GetProductFilters]
    @numDomainId NUMERIC ,
    @numWareHouseID AS NUMERIC(9) = 0,
    @FilterQuery AS VARCHAR(2000) = '',
    @numCategoryID AS NUMERIC(18) = 0
AS 
    BEGIN  
   
        DECLARE @sqlQuery AS NVARCHAR(MAX) 
        DECLARE @sqlwhere AS NVARCHAR(MAX) 
        DECLARE @sqlFilter AS NVARCHAR(MAX)
        DECLARE @dynamicFilterQuery NVARCHAR(max) 
        DECLARE @where NVARCHAR(max) 
        DECLARE @data AS VARCHAR(100)
        DECLARE @row AS INTEGER        
        DECLARE @checkbox VARCHAR(2000)
        DECLARE @checkboxGroupBy VARCHAR(2000)
        DECLARE @dropdown VARCHAR(2000)
        DECLARE @dropdownGroupBy VARCHAR(2000)
        DECLARE @textbox VARCHAR(2000)
        DECLARE @textboxGroupBy VARCHAR(2000)
        DECLARE @filterNumItemIds VARCHAR(2000)
        DECLARE @priceStart VARCHAR(3000)
        DECLARE @priceInnerQuery VARCHAR(3000)
        DECLARE @priceEnd VARCHAR(3000)
        DECLARE @manufacturer VARCHAR(2000)
        DECLARE @manufacturerGroupBy VARCHAR(2000)
        DECLARE @attributes VARCHAR(2000)
        DECLARE @filterByNumItemCode VARCHAR(3000)
        DECLARE @count NUMERIC(9,0)
       
        BEGIN
            
            SET @where = ''
            SET @checkbox = ''
          
            
            IF @numWareHouseID = 0 
            BEGIN
                SELECT  @numWareHouseID = ISNULL(numDefaultWareHouseID,0)
                FROM    [eCommerceDTL]
                WHERE   [numDomainID] = @numDomainID	
            END

            IF LEN(@FilterQuery) > 0
				BEGIN 
				   
				    SELECT row_number() over(order by OutParam) as row, OutParam into #temp FROM dbo.SplitString(@FilterQuery,';')
				    
				    select top 1 @row = row ,@data =data from #temp 
				   
		            SET @dynamicFilterQuery = ''
		            SET @count = 0
		            WHILE @row>0
	                  BEGIN
	                     
	                     IF SUBSTRING(@data,1,1) = 'f'
	                        BEGIN	
	                            IF  CHARINDEX('-2',@data) >  0   
	                                BEGIN
	                                  SET @dynamicFilterQuery  =  ' SELECT   I.numItemCode 
                                                                    FROM 
                                                                         ( 
                                                                           ( SELECT              
                                                                                  numItemCode ,
                                                                                  CEILING
                                                                                  (
                                                                                     ISNULL
                                                                                     (
                                                                                       CASE WHEN I.[charItemType]=''P'' 
                                                                                            THEN 
                                                                                                 CASE WHEN I.bitSerialized = 1 
                                                                                                      THEN 
                                                                                                          (dbo.fn_UOMConversion(ISNULL(I.numSaleUnit,0),I.numItemCode,I.numDomainId,ISNULL(I.numBaseUnit,0)) * monListPrice) 
                                                                                                      ELSE 
                                                                                                          (SELECT TOP 1 (dbo.fn_UOMConversion(ISNULL(I.numSaleUnit,0),I.numItemCode,I.numDomainId,ISNULL(I.numBaseUnit,0)) * ISNULL([monWListPrice],0)) FROM [WareHouseItems] 
                                                                                                           WHERE [numItemID]= I.numItemCode 
                                                                                                           and  [numWareHouseID]=' + CONVERT(VARCHAR(10), @numWareHouseID)
                                                                                                      + ' ) 
                                                                                                  END
								                                                             ELSE 
								                                                                  (dbo.fn_UOMConversion(ISNULL(I.numSaleUnit,0),I.numItemCode,I.numDomainId,ISNULL(I.numBaseUnit,0)) * monListPrice) 
								                                                             END,0) 
								                                                    )  
                                                                             AS Price
                                                                             FROM 
                                                                                    dbo.ItemCategory IC 
                                                                             JOIN   Item I 
                                                                             ON     I.numItemCode = IC.numItemID
                                                                             WHERE
                                                                                       IC.numCategoryId = '+CONVERT(VARCHAR(10) , @numCategoryID)+'
                                                                                    AND I.numDomainId = '+CONVERT(VARCHAR(10) , @numDomainId)+'
                                                                          ))
                                                                    AS I 
                                                                    WHERE       I.Price 
                                                                    BETWEEN     SUBSTRING('''+@data+''',CHARINDEX(''='','''+@data+''')+1,(CHARINDEX(''~'','''+@data+''')) - (CHARINDEX(''='','''+@data+''')+1) )
                                                                    AND         SUBSTRING('''+@data+''',CHARINDEX(''~'','''+@data+''') + 1,(LEN('''+@data+'''))- (CHARINDEX(''~'','''+@data+''')))
    
    '
    PRINT(@dynamicFilterQuery)
	                                                                
	                                END 
                               ELSE IF CHARINDEX('-1',@data) > 0
                                    BEGIN
                              	     SET @dynamicFilterQuery  =   'SELECT      I.numItemCode 
                               	                                   FROM 
                               	                                   dbo.Item    I 
                               	                                   INNER JOIN  dbo.ItemCategory  IC
                                                                   ON          I.numItemCode = IC.numItemID 
                                     
                                                                   WHERE       IC.numCategoryID =' + CONVERT(VARCHAR(20), @numCategoryID)+ ' 
                                                                   AND         I.numDomainID    =' + CONVERT(VARCHAR(20), @numDomainID)+ '
                              	                                   AND         I.vcManufacturer = SUBSTRING('''+@data+''',CHARINDEX(''='','''+@data+''')+1,(LEN('''+@data+'''))-(CHARINDEX(''='','''+@data+''')))'
						            END
					        	ELSE   
						           BEGIN
						             SET @dynamicFilterQuery = ' SELECT        DISTINCT     
                                                                               I.numItemCode 
                                                                 FROM          dbo.Item I 
                                                                 INNER JOIN    dbo.ItemCategory IC 
                                                                 ON            IC.numItemID = i.numItemCode
                                                                 INNER JOIN    dbo.ItemGroupsDTL IGD 
                                                                 ON            I.numItemGroup = IGD.numItemGroupID
                                                                 INNER JOIN    dbo.CFW_Fld_Master CFM 
                                                                 ON                IGD.numOppAccAttrID = CFM.Fld_id 
                                                                               AND CFM.numDomainID= '+CONVERT(VARCHAR(10),@numDomainId) +'
                                                                 LEFT JOIN     dbo.ListDetails LD 
                                                                 ON                LD.numListID = CFM.numlistid 
                                                                               AND CFM.numDomainID = LD.numDomainID
                                                                 INNER JOIN    dbo.WareHouseItems WHI 
                                                                 ON            WHI.numItemID = I.numItemCode 
                                                                 INNER JOIN    dbo.CFW_Fld_Values_Serialized_Items CFVSI 
                                                                 ON            CFVSI.RecId = WHI.numWareHouseItemID
                   
                                                                 WHERE 
                                                                     IC.numCategoryID = '+CONVERT(VARCHAR(10),@numCategoryID)+' 
                                                                 AND I.numDomainID = '+CONVERT(VARCHAR(10),@numDomainId)+' 
                                                                 AND LD.numListItemID = CFVSI.Fld_Value 
                                                                 AND CFM.Fld_id  =  CFVSI.Fld_ID
                                                                 AND CFM.Fld_id = CONVERT(NUMERIC(9,0),  SUBSTRING('''+@data+''',CHARINDEX(''>'','''+@data+''')+1,CHARINDEX(''='','''+@data+''')-(CHARINDEX(''>'','''+@data+''')+1) )) 
                                                                 AND LD.numListItemID = CONVERT( VARCHAR(1000), SUBSTRING('''+@data+''',CHARINDEX(''='','''+@data+''')+1,LEN('''+@data+''')))  
                     '
                                                                       
						           END
	                         END
		                
		                    
		                 DELETE from #temp where row=@row
                   		 SELECT top 1 @row=row ,@data = data from #temp
                   		
                         IF @@rowCount=0
                           BEGIN
                                 SET @row=0
                                 IF @count = 0
                                   BEGIN 
                                      SET @where = @where + @dynamicFilterQuery 
                                      SET @count = @count + 1   
                                                                   
                                   END
                                ELSE
                                  BEGIN
                                  	   SET @where =  @dynamicFilterQuery + '  And I.numItemCode IN( ' + @where + ' ) '  
								  	   SET @count = @count + 1
								  END
                           END
                         ELSE 
                           BEGIN
                                IF @count = 0
                                   BEGIN 
                                      SET @where = @where + @dynamicFilterQuery 
                                      SET @count = @count + 1   
                                                                 
                                   END
                                ELSE
                                  BEGIN
                                   	   SET @where =  @dynamicFilterQuery + '  And I.numItemCode IN( ' + @where + ' ) '  
								  	   SET @count = @count + 1
								  END
                           END
                      END
                     
                      Drop table #temp
                      SET @sqlQuery  = 'SELECT numItemCode into #TempItemCode FROM (' + @where +') AS a'
                          
	           END
	           
	           ELSE
	           BEGIN
	               SET  @sqlQuery = '' 
               END
               
            SET @filterByNumItemCode = ' AND I.numItemCode IN ( SELECT * FROM #TempItemCode ) '
             
                      
            SET @sqlQuery = @sqlQuery +  ' SELECT      DISTINCT  
                                                       CFM.Fld_id AS FilterID,
                                                       REPLACE(REPLACE(CFM.Fld_label,'';'','' '') ,''~'','' '')   AS FilterName,
                                                       LD.vcdata AS FilterValue,
                                                       LD.numListItemID AS FilterValueID,
                                                       ''f~'' +CFM.Fld_label + ''~''+ CAST(CFM.Fld_id AS VARCHAR(10)) +''^'' +  LD.vcdata+''~''+ CAST(LD.numListItemID AS VARCHAR(10)) + '';'' AS FilterQuery ,
                                                       dbo.fn_GetItemCountBasedOnAttribute(CFM.Fld_id,LD.numListItemID,'+CONVERT(VARCHAR(10),@numCategoryID)+','+CONVERT(VARCHAR(10),@numDomainId)+')  ItemCount, LD.numListID
                                           FROM        dbo.Item I 
                                           INNER JOIN  dbo.ItemCategory IC 
                                           ON          IC.numItemID = I.numItemCode
                                           INNER JOIN  dbo.ItemGroupsDTL IGD 
                                           ON          I.numItemGroup = IGD.numItemGroupID
                                           INNER JOIN  dbo.CFW_Fld_Master CFM 
                                           ON              IGD.numOppAccAttrID = CFM.Fld_id 
                                                       AND CFM.numDomainID= @numDomainID
                                           LEFT JOIN   dbo.ListDetails LD 
                                           ON              LD.numListID = CFM.numlistid 
                                                       AND CFM.numDomainID = LD.numDomainID
                                           INNER JOIN  dbo.WareHouseItems WHI 
                                           ON          WHI.numItemID = I.numItemCode 
                                           INNER JOIN  dbo.CFW_Fld_Values_Serialized_Items CFVSI 
                                           ON          CFVSI.RecId = WHI.numWareHouseItemID
                   
                                           WHERE 
                                           
                                                          IC.numCategoryID = @numCategoryID 
                                                       AND i.numDomainID = @numDomainID
                                                       AND CONVERT(VARCHAR(1000) ,LD.numListItemID)  = CFVSI.Fld_Value 
                                                       AND  CFM.Fld_id  =  CFVSI.Fld_ID 
                                           
                                           GROUP BY    LD.vcdata  ,
                                                       CFM.Fld_id ,
                                                       CFM.Fld_label,
                                                       LD.numListItemID,
													   LD.numListID
                                         '    

        

	SET @manufacturer =  'UNION
	       
		 SELECT       DISTINCT 
		             -1 AS FilterID,
		             ''Manufacturer'' ,
		              vcManufacturer  FilterValue,
		              0 AS FilterValueID, 
		              ''f~Manufacturer~-1^''+vcManufacturer+''~-1''+'';'' AS FilterQuery,
		              Count(*) ItemCount, -1 AS numListID 
		 FROM 
		              Item I INNER JOIN dbo.ItemCategory IC ON I.numItemCode = IC.numItemID 
		 where 
		              numDomainID=@numDomainId
		              AND IC.numCategoryID = @numCategoryID 
		              AND LEN(ISNULL(vcManufacturer,''''))>1 ' 
	
	SET @manufacturerGroupBy =	'GROUP BY     I.vcManufacturer '
   
     SET @priceStart = '    UNION 
        
                    SELECT  
                      -2 AS FilterID,
                      ''Price'' AS FilterName,
                      CAST(MIN(FLOOR(a.Price)) AS VARCHAR(10))  AS FilterValueId,
                      CEILING(MAX(a.Price))  AS FilterVAlueID , 
                      ''f~Min~''+ CAST(MIN(FLOOR(a.Price)) AS VARCHAR(10))+''^Max~''+CAST(MAX(CEILING(a.Price)) AS VARCHAR(10))+'';'' AS FilterQuery ,
                      COUNT(price) AS ItemCount, -1 AS numListID 
                      FROM  ('
                               

       SET @priceInnerQuery ='SELECT              
                                     CEILING(
                                               ISNULL
                                               (
                                                   CASE WHEN I.[charItemType]=''P'' 
                                                   THEN 
                                                       CASE WHEN I.bitSerialized = 1 
                                                            THEN 
                                                                (dbo.fn_UOMConversion(ISNULL(I.numSaleUnit,0),I.numItemCode,I.numDomainId,ISNULL(I.numBaseUnit,0)) * monListPrice) 
                                                            ELSE 
                                                                (SELECT TOP 1 (dbo.fn_UOMConversion(ISNULL(I.numSaleUnit,0),I.numItemCode,I.numDomainId,ISNULL(I.numBaseUnit,0)) * ISNULL([monWListPrice],0)) FROM [WareHouseItems] 
                                                                 WHERE [numItemID]= I.numItemCode 
                                                                 and  [numWareHouseID]=' + CONVERT(VARCHAR(10), @numWareHouseID)
                                                         + ' ) 
                                                         END
								                   ELSE 
								                        (dbo.fn_UOMConversion(ISNULL(I.numSaleUnit,0),I.numItemCode,I.numDomainId,ISNULL(I.numBaseUnit,0)) * monListPrice) 
								                   END,0) 
								                  
                                            )  
                              AS Price
                              FROM 
                                     dbo.ItemCategory IC JOIN Item I ON I.numItemCode = IC.numItemID
                              WHERE
                                         IC.numCategoryId = @numCategoryId
                                     AND I.numDomainId = @numDomainId 
                              '
                       
       SET @priceEnd =' )  AS a
                '
    
     
        IF LEN(@where) > 0 
           BEGIN 
           
              SET   @sqlQuery = @sqlQuery +@priceStart + @priceInnerQuery + @priceEnd +  @manufacturer + @filterByNumItemCode + @manufacturerGroupBy                   
           END
        ELSE
          BEGIN
              SET   @sqlQuery = @sqlQuery +@priceStart + @priceInnerQuery + @priceEnd +  @manufacturer + @manufacturerGroupBy              
          END   
        
         END
		 print @sqlQuery
         EXEC sp_executeSql @sqlQuery,
            N'@numDomainId numeric(9),@numCategoryID numeric(9)', @numDomainId,
            @numCategoryID
    END

--exec USP_GetProductFilters @numDomainId=1,@numCategoryID=1735,@FilterQuery='f>-2=335~700'
--exec USP_GetProductFilters @numDomainId=154,@numCategoryID=2297,@FilterQuery=NULL
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetTrialBalance')
DROP PROCEDURE USP_GetTrialBalance
GO
CREATE PROCEDURE [dbo].[USP_GetTrialBalance]                                 
@numDomainId AS NUMERIC(9),                                                  
@dtFromDate AS DATETIME,                                                
@dtToDate AS DATETIME,
@ClientTimeZoneOffset INT, 
@numAccountClass AS NUMERIC(9)=0,
@DateFilter AS VARCHAR(20),
@ReportColumn AS VARCHAR(20)                                                        
AS                                                                
BEGIN        
	DECLARE @PLCHARTID NUMERIC(18,0) = 0

	SELECT @PLCHARTID=COA.numAccountId FROM Chart_of_Accounts COA WHERE numDomainID=@numDomainId AND bitProfitLoss=1

	DECLARE @FinancialYearStartDate AS DATETIME
	SELECT @FinancialYearStartDate = dbo.GetFiscalStartDate(YEAR(GETDATE()),@numDomainId)

	DECLARE @TEMPQuarter AS TABLE
	(
		ID INT,
		StartDate DATETIME,
		EndDate DATETIME
	)

	INSERT INTO @TEMPQuarter VALUES (1,@FinancialYearStartDate,DATEADD(ms,-3,DATEADD(QUARTER,1,@FinancialYearStartDate)))
	INSERT INTO @TEMPQuarter VALUES (2,DATEADD(QUARTER,1,@FinancialYearStartDate),DATEADD(ms,-3,DATEADD(QUARTER,2,@FinancialYearStartDate)))
	INSERT INTO @TEMPQuarter VALUES (3,DATEADD(QUARTER,2,@FinancialYearStartDate),DATEADD(ms,-3,DATEADD(QUARTER,3,@FinancialYearStartDate)))
	INSERT INTO @TEMPQuarter VALUES (4,DATEADD(QUARTER,3,@FinancialYearStartDate),DATEADD(ms,-3,DATEADD(QUARTER,4,@FinancialYearStartDate)))

	IF @DateFilter = 'CurYear'
	BEGIN
		SELECT @dtFromDate = @FinancialYearStartDate,
			@dtToDate = DATEADD(ms,-3,DATEADD(YEAR,1,@FinancialYearStartDate));
	END
	ELSE IF @DateFilter = 'PreYear'
	BEGIN
		SELECT @dtFromDate = DATEADD(YEAR,-1,@FinancialYearStartDate),
			@dtToDate = DATEADD(ms,-3,DATEADD(YEAR,1,DATEADD(YEAR,-1,@FinancialYearStartDate)));
	END
	ELSE IF @DateFilter = 'CurPreYear'
	BEGIN
		SELECT @dtFromDate = DATEADD(YEAR,-1,@FinancialYearStartDate),
			@dtToDate = DATEADD(ms,-3,DATEADD(YEAR,1,@FinancialYearStartDate));
	END
	ELSE IF @DateFilter = 'CuQur'
	BEGIN
		SELECT @dtFromDate = StartDate, @dtToDate=EndDate FROM @TEMPQuarter WHERE GetDate() BETWEEN StartDate AND EndDate
	END
	ELSE IF @DateFilter = 'CurPreQur'
	BEGIN
		SELECT @dtFromDate = DATEADD(QUARTER,-1,StartDate), @dtToDate=EndDate FROM @TEMPQuarter WHERE GetDate() BETWEEN StartDate AND EndDate
	END
	ELSE IF @DateFilter = 'PreQur'
	BEGIN
		SELECT @dtFromDate = DATEADD(QUARTER,-1,StartDate), @dtToDate=DATEADD(QUARTER,-1,EndDate) FROM @TEMPQuarter WHERE GetDate() BETWEEN StartDate AND EndDate
	END
	ELSE IF @DateFilter = 'LastMonth'
	BEGIN
		select @dtFromDate = DATEADD(mm,-1,DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0)),
				@dtToDate = DATEADD(ms,-3,DATEADD(mm,0,DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0)));  
	END
	ELSE IF @DateFilter = 'ThisMonth'
	BEGIN
		select @dtFromDate = DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0),
				@dtToDate = DATEADD(ms,- 3,DATEADD(mm,0,DATEADD(mm,DATEDIFF(mm,0,GETDATE())+1,0)));  
	END
	ELSE IF @DateFilter = 'CurPreMonth'
	BEGIN
		select @dtFromDate = DATEADD(mm,-1,DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0)),
				@dtToDate = DATEADD(ms,- 3,DATEADD(mm,0,DATEADD(mm,DATEDIFF(mm,0,GETDATE())+1,0))); 
	END

	CREATE TABLE #view_journal
	(
		numAccountId NUMERIC(18,0),
		vcAccountCode VARCHAR(50),
		COAvcAccountCode VARCHAR(100),
		datEntry_Date SMALLDATETIME,
		Debit MONEY,
		Credit MONEY
	)

	INSERT INTO #view_journal SELECT numAccountId,vcAccountCode,COAvcAccountCode,datEntry_Date,Debit,Credit FROM view_journal WHERE numDomainId = @numDomainID AND (numAccountClass=@numAccountClass OR @numAccountClass=0);

	DECLARE @dtFinYearFromJournal DATETIME ;
	DECLARE @dtFinYearFrom DATETIME ;

	SELECT @dtFinYearFromJournal = MIN(datEntry_Date) FROM #view_journal
	SET @dtFinYearFrom = (SELECT dtPeriodFrom FROM FINANCIALYEAR WHERE dtPeriodFrom <= @dtFromDate AND dtPeriodTo >= @dtFromDate AND numDomainId = @numDomainId)

	

	;WITH DirectReport (ParentId, vcCompundParentKey, numAccountTypeID, numAccountID, vcAccountType,vcAccountCode, LEVEL,Struc)
	AS
	(
		SELECT 
			CAST('' AS VARCHAR) AS ParentId, 
			CAST(CONCAT([ATD].[numAccountTypeID],'#',0) AS VARCHAR),
			[ATD].[numAccountTypeID], 
			0,
			[ATD].[vcAccountType],
			[ATD].[vcAccountCode],
			1 AS LEVEL, 
			CAST(CAST([ATD].[vcAccountCode] AS VARCHAR) AS VARCHAR(300))  AS Struc
		FROM 
			[dbo].[AccountTypeDetail] AS ATD
		WHERE 
			[ATD].[numDomainID]=@numDomainId 
			AND vcAccountCode IN ('0101','0102','0103','0104','0105','0106')
		UNION ALL
		SELECT 
			D.vcCompundParentKey AS ParentId,
			CAST(CONCAT([ATD].[numAccountTypeID],'#',0) AS VARCHAR),
			[ATD].[numAccountTypeID], 
			0,
			[ATD].[vcAccountType],
			[ATD].[vcAccountCode],
			LEVEL + 1, 
			CAST(d.Struc + '#' + cast([ATD].[vcAccountCode] AS VARCHAR) AS VARCHAR(300))  AS Struc
		FROM 
			[dbo].[AccountTypeDetail] AS ATD 
		JOIN 
			[DirectReport] D 
		ON 
			D.[numAccountTypeID] = [ATD].[numParentID]
		WHERE 
			[ATD].[numDomainID]=@numDomainId 
	),
	DirectReport1 (ParentId, vcCompundParentKey, numAccountTypeID, vcAccountType, vcAccountCode, LEVEL,numAccountId,Struc, bitProfitLoss)
	AS
	(
		SELECT 
			ParentId, 
			vcCompundParentKey,
			numAccountTypeID,
			vcAccountType,
			vcAccountCode, 
			LEVEL,
			CAST(numAccountID AS NUMERIC(18)),
			Struc,
			CAST(0 AS BIT)
		FROM 
			DirectReport 
		UNION ALL
		SELECT 
			CAST(CONCAT(COA.numParntAcntTypeId,'#',0) AS VARCHAR) AS ParentId,
			CAST(CONCAT(COA.numParntAcntTypeId,'#',COA.numAccountId) AS VARCHAR),
			CAST(NULL AS NUMERIC(18)),
			[vcAccountName],
			COA.[vcAccountCode],
			LEVEL + 1,
			[COA].[numAccountId], 
			CAST(d.Struc + '#' + CAST(COA.[numAccountId] AS VARCHAR) AS VARCHAR(300)) Struc,
			COA.bitProfitLoss
		FROM 
			[dbo].[Chart_of_Accounts] AS COA 
		JOIN 
			[DirectReport1] D 
		ON 
			D.[numAccountTypeID] = COA.[numParntAcntTypeId]
		WHERE 
			[COA].[numDomainID]=@numDomainId 
			AND ISNULL(COA.bitActive,0) = 1
			AND ISNULL(COA.bitIsSubAccount,0) = 0
		UNION ALL
		SELECT 
			CAST(CONCAT(COA.numParntAcntTypeId,'#',COA.numParentAccId) AS VARCHAR) AS ParentId,
			CAST(CONCAT(COA.numParntAcntTypeId,'#',COA.numAccountId) AS VARCHAR),
			CAST(NULL AS NUMERIC(18)),
			[vcAccountName],
			COA.[vcAccountCode],
			LEVEL + 1,
			[COA].[numAccountId], 
			CAST(d.Struc + '#' + CAST(COA.[numAccountId] AS VARCHAR) AS VARCHAR(300)) AS Struc,
			COA.bitProfitLoss
		FROM 
			[dbo].[Chart_of_Accounts] AS COA 
		JOIN 
			[DirectReport1] D 
		ON 
			D.numAccountId = COA.numParentAccId
		WHERE 
			[COA].[numDomainID]=@numDomainId 
			AND ISNULL(COA.bitActive,0) = 1
			AND ISNULL(COA.bitIsSubAccount,0) = 1
	)
  
	SELECT 
		ParentId, 
		vcCompundParentKey,
		numAccountTypeID, 
		vcAccountType, 
		LEVEL, 
		vcAccountCode,
		numAccountId,
		Struc,
		bitProfitLoss
	INTO 
		#tempDirectReport
	FROM 
		DirectReport1

	DECLARE @columns VARCHAR(8000) = ''
	DECLARE @PivotColumns VARCHAR(8000) = ''
	DECLARE @sql VARCHAR(MAX) = ''

	IF @ReportColumn = 'Year'
	BEGIN
		CREATE TABLE #TempYearMonth
		(
			intYear INT,
			intMonth INT,
			MonthYear VARCHAR(50),
			StartDate DATETIME,
			EndDate DATETIME,
			ProfitLossAmount MONEY
		)
		
		;WITH CTE AS 
		(
			SELECT
				YEAR(@dtFromDate) AS 'yr',
				MONTH(@dtFromDate) AS 'mm',
				DATENAME(mm, @dtFromDate) AS 'mon',
				(DATENAME(mm, @dtFromDate) + '_' + CAST(YEAR(@dtFromDate) AS VARCHAR)) AS MonthYear,
				CAST(DATEFROMPARTS(YEAR(@dtFromDate),MONTH(@dtFromDate),1) AS DATETIME) dtStartDate,
				DATEADD(MS,-2,CAST(DATEADD(MONTH,1,DATEFROMPARTS(YEAR(@dtFromDate),MONTH(@dtFromDate),1)) AS DATETIME)) dtEndDate,
				@dtFromDate 'new_date'
			UNION ALL
			SELECT
				YEAR(DATEADD(d,1,new_date)) AS 'yr',
				MONTH(DATEADD(d,1,new_date)) AS 'mm',
				DATENAME(mm, DATEADD(d,1,new_date)) AS 'mon',
				(DATENAME(mm, DATEADD(d,1,new_date)) + '_' + CAST(YEAR(DATEADD(d,1,new_date)) AS VARCHAR)) AS MonthYear,
				CAST(DATEFROMPARTS(YEAR(DATEADD(d,1,new_date)),MONTH(DATEADD(d,1,new_date)),1) AS DATETIME) dtStartDate,
				DATEADD(MS,-2,CAST(DATEADD(MONTH,1,DATEFROMPARTS(YEAR(DATEADD(d,1,new_date)),MONTH(DATEADD(d,1,new_date)),1)) AS DATETIME)) dtEndDate,
				DATEADD(d,1,new_date) 'new_date'
			FROM CTE
			WHERE DATEADD(d,1,new_date) < @dtToDate
		)
	
		INSERT INTO 
			#tempYearMonth
		SELECT 
			yr,mm,MonthYear,dtStartDate,dtEndDate,0
		FROM 
			CTE
		GROUP BY 
			yr, mm, MonthYear, dtStartDate, dtEndDate
		ORDER BY 
			yr, mm
		OPTION (MAXRECURSION 5000)

		INSERT INTO #tempYearMonth VALUES (5000,5000,'Total',@dtFromDate,@dtToDate,0)

		UPDATE 
			#tempYearMonth
		SET
			EndDate = (CASE WHEN EndDate < @dtToDate THEN EndDate ELSE @dtToDate END)

		UPDATE 
			#tempYearMonth
		SET
			ProfitLossAmount = (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE numAccountId=@PLCHARTID AND vcAccountCode LIKE '0103%' AND datEntry_Date between StartDate and EndDate)
							+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE vcAccountCode LIKE '0104%' AND datEntry_Date between StartDate and EndDate)
							+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE vcAccountCode LIKE '0106%' AND datEntry_Date between StartDate and EndDate)
							+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE vcAccountCode LIKE '0103%' AND datEntry_Date between StartDate and EndDate)
							+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE (vcAccountCode LIKE '0103%' OR vcAccountCode LIKE '0104%' OR vcAccountCode  LIKE '0106%') AND datEntry_Date <=  DATEADD(MS,-3,StartDate))
							+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE numAccountId=@PLCHARTID AND datEntry_Date <=  DATEADD(MS,-3,StartDate))

		SELECT @columns = STUFF((SELECT ', ISNULL(' + MonthYear  + ',0) AS [' + REPLACE(MonthYear,'_',' ') + ']' FROM #tempYearMonth ORDER BY intYear,intMonth FOR XML PATH('')), 1, 2, ''); 
		SELECT @PivotColumns = STUFF((SELECT ', ' + MonthYear FROM #tempYearMonth ORDER BY intYear,intMonth FOR XML PATH('')), 1, 2, ''); 

		SELECT 
			COA.ParentId, 
			COA.vcCompundParentKey,
			COA.numAccountTypeID, 
			COA.vcAccountType, 
			COA.LEVEL, 
			COA.vcAccountCode, 
			COA.numAccountId, 
			COA.Struc,
			Period.MonthYear,
			Period.ProfitLossAmount,
			(CASE 
				WHEN bitProfitLoss = 1 
				THEN -Period.ProfitLossAmount
				ELSE
					(CASE 
						WHEN COA.[vcAccountCode] LIKE '0103%' OR COA.[vcAccountCode] LIKE '0104%' OR COA.[vcAccountCode] LIKE '0106%' THEN 0 
						ELSE ISNULL(t2.OPENING,0) 
					END) + (ISNULL(Debit,0) - ISNULL(Credit,0)) 
			END) AS Amount
		INTO 
			#tempViewDataYear
		FROM 
			#tempDirectReport COA 
		OUTER APPLY
		(
			SELECT
				MonthYear,
				StartDate,
				EndDate,
				ProfitLossAmount
			FROM
				#tempYearMonth
		) AS Period
		OUTER APPLY
		(
			SELECT 
				SUM(Debit) AS Debit,
				SUM(Credit) AS Credit
			FROM
				#view_journal V 
			WHERE  
				V.numAccountID = COA.numAccountID 
				AND datEntry_Date BETWEEN Period.StartDate AND Period.EndDate
		) AS t1
		OUTER APPLY 
		(
			SELECT   
				SUM(Debit - Credit) AS OPENING
			FROM     
				#view_journal VJ
			WHERE    
				VJ.numAccountID = COA.numAccountID 
				AND datEntry_Date BETWEEN (CASE WHEN COA.[vcAccountCode] LIKE '0103%' OR COA.[vcAccountCode] LIKE '0104%' OR COA.[vcAccountCode] LIKE '0106%' THEN @dtFinYearFrom ELSE @dtFinYearFromJournal END )
                                                              AND  DATEADD(MS,-3,Period.StartDate)
		)  AS t2

		SET @sql = 'SELECT
						ParentId,
						vcCompundParentKey,
						numAccountId,
						numAccountTypeID,
						vcAccountType,
						LEVEL,
						vcAccountCode,
						Struc,
						[Type],' + @columns + '
					FROM
					(
						SELECT 
							COA.ParentId, 
							COA.vcCompundParentKey,
							COA.numAccountId,
							COA.numAccountTypeID, 
							COA.vcAccountType, 
							COA.LEVEL, 
							COA.vcAccountCode, 
							(''#'' + COA.Struc + ''#'') AS Struc,
							(CASE WHEN ISNULL(COA.numAccountId,0) > 0 THEN 1 ELSE 2 END) [Type],
							ISNULL(SUM(Amount),0) AS Amount,
							V.MonthYear
						FROM 
							#tempDirectReport COA 
						LEFT OUTER JOIN 
							#tempViewDataYear V 
						ON  
							V.Struc like COA.Struc + ''%'' 
						GROUP BY 
							COA.ParentId, COA.vcCompundParentKey, COA.numAccountTypeID, COA.vcAccountType, COA.LEVEL, COA.vcAccountCode, COA.numAccountId,COA.Struc,COA.bitProfitLoss,V.MonthYear,V.ProfitLossAmount
						UNION
						SELECT 
							'''', 
							'''',
							NULL,
							NULL,
							''Total'',
							-1, 
							NULL, 
							''-1'',
							2,
							(SELECT 
								ISNULL(SUM(Debit) - SUM(Credit),0)
							FROM 
								VIEW_Journal_Master
							WHERE 
								numDomainID = ' + CAST(@numDomainId AS VARCHAR) + '
								AND (AccountCode LIKE ''0101%'' 
								OR AccountCode LIKE ''0102%''
								OR AccountCode LIKE ''0103%''
								OR AccountCode LIKE ''0104%''
								OR AccountCode LIKE ''0105%''
								OR AccountCode LIKE ''0106%'')
								AND datEntry_Date BETWEEN #TempYearMonth.StartDate AND #TempYearMonth.EndDate),
							MonthYear
						FROM
							#TempYearMonth
					) AS SourceTable
					pivot
					(
						SUM(AMOUNT)
						FOR [MonthYear] IN ( ' + @pivotcolumns + ' )
					) AS P 
					ORDER BY Struc, [Type] desc'

		PRINT @sql
		EXECUTE (@sql)

		DROP TABLE #TempYearMonth
		DROP TABLE #tempViewDataYear
	END
	ELSE IF @ReportColumn = 'Quarter'
	BEGIN
		CREATE TABLE #TempYearMonthQuarter
		(
			intYear INT,
			intQuarter INT,
			MonthYear VARCHAR(50),
			StartDate DATETIME,
			EndDate DATETIME,
			ProfitLossAmount MONEY
		)

		;WITH CTE AS 
		(
			SELECT
				dbo.GetFiscalyear(@dtFromDate,@numDomainId) AS 'yr',
				dbo.GetFiscalQuarter(@dtFromDate,@numDomainId) AS 'qq',
				('Q' + cast(dbo.GetFiscalQuarter(@dtFromDate,@numDomainId) AS VARCHAR) + '_' + cast(dbo.GetFiscalyear(@dtFromDate,@numDomainId) AS VARCHAR)) AS MonthYear,
				CASE dbo.GetFiscalQuarter(@dtFromDate,@numDomainId) 
				WHEN 1 THEN dateadd(month,0,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId))
				WHEN 2 THEN dateadd(month,3,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId)) 
				WHEN 3 THEN dateadd(month,6,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId))
				WHEN 4 THEN dateadd(month,9,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId)) 
				END AS dtStartDate,
				CASE dbo.GetFiscalQuarter(@dtFromDate,@numDomainId) 
				WHEN 1 THEN dateadd(MS,-3,dateadd(month,3,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId)))
				WHEN 2 THEN dateadd(MS,-3,dateadd(month,6,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId)))
				WHEN 3 THEN dateadd(MS,-3,dateadd(month,9,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId)))
				WHEN 4 THEN dateadd(MS,-3,dateadd(month,12,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId)))
				END AS dtEndDate,
				@dtFromDate 'new_date'
			UNION ALL
			SELECT
				dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId) AS 'yr',
				dbo.GetFiscalQuarter(DATEADD(d,1,new_date),@numDomainId) AS 'qq',
				('Q' + cast(dbo.GetFiscalQuarter(DATEADD(d,1,new_date),@numDomainId) AS VARCHAR) + '_' + cast(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId) AS VARCHAR)) AS MonthYear,
				CASE dbo.GetFiscalQuarter(DATEADD(d,1,new_date),@numDomainId) 
				WHEN 1 THEN dateadd(month,0,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId))
				WHEN 2 THEN dateadd(month,3,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId)) 
				WHEN 3 THEN dateadd(month,6,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId))
				WHEN 4 THEN dateadd(month,9,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId)) 
				END AS dtStartDate,
				CASE dbo.GetFiscalQuarter(DATEADD(d,1,new_date),@numDomainId) 
				WHEN 1 THEN dateadd(MS,-3,dateadd(month,3,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId)))
				WHEN 2 THEN dateadd(MS,-3,dateadd(month,6,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId)))
				WHEN 3 THEN dateadd(MS,-3,dateadd(month,9,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId)))
				WHEN 4 THEN dateadd(MS,-3,dateadd(month,12,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId)))
				END AS dtEndDate,
				DATEADD(d,1,new_date) 'new_date'
			FROM CTE
			WHERE DATEADD(d,1,new_date) < @dtToDate
		)
	
		INSERT INTO 
			#TempYearMonthQuarter 		
		SELECT 
			yr,qq,MonthYear,dtStartDate,dtEndDate,0
		FROM 
			CTE
		GROUP BY 
			yr, qq, MonthYear, dtStartDate, dtEndDate
		ORDER BY 
			yr, qq
		OPTION
			(MAXRECURSION 5000)

		INSERT INTO #TempYearMonthQuarter VALUES (5000,5000,'Total',@dtFromDate,@dtToDate,0)

		UPDATE 
			#TempYearMonthQuarter
		SET
			EndDate = (CASE WHEN EndDate < @dtToDate THEN EndDate ELSE @dtToDate END)

		UPDATE 
			#TempYearMonthQuarter
		SET
			ProfitLossAmount = (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE numAccountId=@PLCHARTID AND vcAccountCode LIKE '0103%' AND datEntry_Date between StartDate and EndDate)
							+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE vcAccountCode LIKE '0104%' AND datEntry_Date between StartDate and EndDate)
							+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE vcAccountCode LIKE '0106%' AND datEntry_Date between StartDate and EndDate)
							+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE vcAccountCode LIKE '0103%' AND datEntry_Date between StartDate and EndDate)
							+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE (vcAccountCode LIKE '0103%' OR vcAccountCode LIKE '0104%' OR vcAccountCode  LIKE '0106%') AND datEntry_Date <=  DATEADD(MS,-3,StartDate))
							+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE numAccountId=@PLCHARTID AND datEntry_Date <=  DATEADD(MS,-3,StartDate))

		SELECT @columns = STUFF((SELECT ', ISNULL(' + MonthYear  + ',0) AS [' + REPLACE(MonthYear,'_',' ') + ']' FROM #TempYearMonthQuarter ORDER BY intYear,intQuarter FOR XML PATH('')), 1, 2, ''); 
		SELECT @PivotColumns = STUFF((SELECT ', ' + MonthYear FROM #TempYearMonthQuarter ORDER BY intYear,intQuarter FOR XML PATH('')), 1, 2, ''); 

		SELECT 
			COA.ParentId, 
			COA.vcCompundParentKey,
			COA.numAccountTypeID, 
			COA.vcAccountType, 
			COA.LEVEL, 
			COA.vcAccountCode, 
			COA.numAccountId, 
			COA.Struc,
			Period.MonthYear,
			Period.ProfitLossAmount,
			(CASE 
				WHEN bitProfitLoss = 1 
				THEN -Period.ProfitLossAmount
				ELSE
					(CASE 
						WHEN COA.[vcAccountCode] LIKE '0103%' OR COA.[vcAccountCode] LIKE '0104%' OR COA.[vcAccountCode] LIKE '0106%' THEN 0 
						ELSE ISNULL(t2.OPENING,0) 
					END) + (ISNULL(Debit,0) - ISNULL(Credit,0))
			END) AS Amount
		INTO 
			#tempViewDataQuarter
		FROM 
			#tempDirectReport COA 
		OUTER APPLY
		(
			SELECT
				MonthYear,
				StartDate,
				EndDate,
				ProfitLossAmount
			FROM
				#TempYearMonthQuarter
		) AS Period
		OUTER APPLY
		(
			SELECT 
				SUM(Debit) AS Debit,
				SUM(Credit) AS Credit
			FROM
				#view_journal V 
			WHERE  
				V.numAccountID = COA.numAccountID 
				AND datEntry_Date BETWEEN Period.StartDate AND Period.EndDate
		) AS t1
		OUTER APPLY 
		(
			SELECT   
				SUM(Debit - Credit) AS OPENING
			FROM     
				#view_journal VJ
			WHERE    
				VJ.numAccountID = COA.numAccountID 
				AND datEntry_Date BETWEEN (CASE WHEN COA.[vcAccountCode] LIKE '0103%' OR COA.[vcAccountCode] LIKE '0104%' OR COA.[vcAccountCode] LIKE '0106%' THEN @dtFinYearFrom ELSE @dtFinYearFromJournal END )
                                                              AND  DATEADD(MS,-3,Period.StartDate)
		)  AS t2
		
		SET @sql = 'SELECT
						ParentId,
						vcCompundParentKey,
						numAccountId,
						numAccountTypeID,
						vcAccountType,
						LEVEL,
						vcAccountCode,
						Struc,
						[Type],' + @columns + '
					FROM
					(
						SELECT 
							COA.ParentId, 
							COA.vcCompundParentKey,
							COA.numAccountId,
							COA.numAccountTypeID, 
							COA.vcAccountType, 
							COA.LEVEL, 
							COA.vcAccountCode, 
							(''#'' + COA.Struc + ''#'') AS Struc,
							(CASE WHEN ISNULL(COA.numAccountId,0) > 0 THEN 1 ELSE 2 END) [Type],
							ISNULL(SUM(Amount),0)  AS Amount,
							V.MonthYear
						FROM 
							#tempDirectReport COA 
						LEFT OUTER JOIN 
							#tempViewDataQuarter V 
						ON  
							V.Struc like COA.Struc + ''%'' 
						GROUP BY 
							COA.ParentId, COA.vcCompundParentKey, COA.numAccountTypeID, COA.vcAccountType, COA.LEVEL, COA.vcAccountCode, COA.numAccountId,COA.Struc,COA.bitProfitLoss,V.MonthYear,V.ProfitLossAmount
						UNION
						SELECT 
							'''',
							'''', 
							NULL,
							NULL,
							''Total'',
							-1, 
							NULL, 
							''-1'',
							2,
							(SELECT 
								ISNULL(SUM(Debit) - SUM(Credit),0)
							FROM 
								VIEW_Journal_Master
							WHERE 
								numDomainID = ' + CAST(@numDomainId AS VARCHAR) + '
								AND (AccountCode LIKE ''0101%'' 
								OR AccountCode LIKE ''0102%''
								OR AccountCode LIKE ''0103%''
								OR AccountCode LIKE ''0104%''
								OR AccountCode LIKE ''0105%''
								OR AccountCode LIKE ''0106%'')
								AND datEntry_Date BETWEEN #TempYearMonthQuarter.StartDate AND #TempYearMonthQuarter.EndDate),
							MonthYear
						FROM
							#TempYearMonthQuarter
					) AS SourceTable
					pivot
					(
						SUM(AMOUNT)
						FOR [MonthYear] IN ( ' + @pivotcolumns + ' )
					) AS P 
					ORDER BY Struc, [Type] desc'

		PRINT @sql
		EXECUTE (@sql)

		DROP TABLE #TempYearMonthQuarter
		DROP TABLE #tempViewDataQuarter
	END
	ELSE
	BEGIN
		DECLARE @monProfitLossAmount AS MONEY = 0
		-- GETTING P&L VALUE
		SELECT @monProfitLossAmount = (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE numAccountId=@PLCHARTID AND vcAccountCode LIKE '0103%' AND datEntry_Date between @dtFromDate and @dtToDate)
			+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE vcAccountCode LIKE '0104%' AND datEntry_Date BETWEEN @dtFromDate AND @dtToDate)
			+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE vcAccountCode LIKE '0106%' AND datEntry_Date BETWEEN @dtFromDate AND @dtToDate)
			+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE vcAccountCode LIKE '0103%' AND datEntry_Date BETWEEN @dtFromDate AND @dtToDate)
			+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE (vcAccountCode LIKE '0103%' OR vcAccountCode LIKE '0104%' OR vcAccountCode  LIKE '0106%') AND datEntry_Date <=  DATEADD(MS,-3,@dtFromDate))
			+ (SELECT ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE numAccountId=@PLCHARTID AND datEntry_Date <=  DATEADD(MS,-3,@dtFromDate))

		SELECT 
			COA.ParentId, 
			COA.numAccountTypeID, 
			COA.vcAccountType, 
			COA.LEVEL, 
			COA.vcAccountCode, 
			COA.numAccountId, 
			COA.Struc,
			(CASE 
				WHEN bitProfitLoss = 1 
				THEN -@monProfitLossAmount
				ELSE
					(CASE 
						WHEN COA.[vcAccountCode] LIKE '0103%' OR COA.[vcAccountCode] LIKE '0104%' OR COA.[vcAccountCode] LIKE '0106%' THEN 0 
						ELSE 
							ISNULL(t2.OPENING,0) 
					END) + (ISNULL(Debit,0) - ISNULL(Credit,0))
			END) AS Amount
		INTO 
			#tempViewData
		FROM 
			#tempDirectReport COA 
		OUTER APPLY
		(
			SELECT 
				SUM(Debit) AS Debit,
				SUM(Credit) AS Credit
			FROM
				#view_journal V 
			WHERE  
				V.numAccountID = COA.numAccountID 
				AND datEntry_Date BETWEEN @dtFromDate AND @dtToDate
		) AS t1
		OUTER APPLY 
		(
			SELECT   
				SUM(Debit - Credit) AS OPENING
			FROM     
				#view_journal VJ
			WHERE    
				VJ.numAccountID = COA.numAccountID 
				AND datEntry_Date BETWEEN (CASE WHEN COA.[vcAccountCode] LIKE '0103%' OR COA.[vcAccountCode] LIKE '0104%' OR COA.[vcAccountCode] LIKE '0106%' THEN @dtFinYearFrom ELSE @dtFinYearFromJournal END )
                                                              AND  DATEADD(MS,-3,@dtFromDate)
		)  AS t2

		SELECT 
			COA.ParentId, 
			COA.vcCompundParentKey,
			COA.numAccountId,
			COA.numAccountTypeID, 
			COA.vcAccountType, 
			COA.LEVEL, 
			COA.vcAccountCode, 
			('#' + COA.Struc + '#') AS Struc,
			(CASE WHEN ISNULL(COA.numAccountId,0) > 0 THEN 1 ELSE 2 END) [Type],
			ISNULL(SUM(Amount),0) AS Amount
		FROM 
			#tempDirectReport COA 
		LEFT OUTER JOIN 
			#tempViewData V 
		ON  
			V.Struc like COA.Struc + '%' 
		GROUP BY 
			COA.ParentId, COA.vcCompundParentKey, COA.numAccountTypeID, COA.vcAccountType, COA.LEVEL, COA.vcAccountCode, COA.numAccountId,COA.Struc,COA.bitProfitLoss
		UNION
		SELECT 
			'', 
			'',
			NULL,
			-1, 
			'Total', 
			0, 
			NULL, 
			'-1' AS Struc,
			2,
			(SELECT 
				SUM(Debit) - SUM(Credit)
			FROM 
				VIEW_Journal_Master
			WHERE 
				numDomainID = @numDomainId
				AND (AccountCode LIKE '0101%' 
				OR AccountCode LIKE '0102%'
				OR AccountCode LIKE '0103%'
				OR AccountCode LIKE '0104%'
				OR AccountCode LIKE '0105%'
				OR AccountCode LIKE '0106%')
				AND datEntry_Date BETWEEN @dtFromDate AND @dtToDate)
		ORDER BY
			Struc, [Type] desc

		DROP TABLE #tempViewData 
	END

	DROP TABLE #view_journal
	DROP TABLE #tempDirectReport
END

GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_InboxItems' ) 
    DROP PROCEDURE USP_InboxItems
GO
Create PROCEDURE [dbo].[USP_InboxItems]                                                                                              
@PageSize [int] ,                            
@CurrentPage [int],                            
@srch as varchar(100) = '',                            
--@vcSubjectFromTo as varchar(100) ='',
--@srchAttachmenttype as varchar(100) ='',                                   
@ToEmail as varchar (100)  ,        
@columnName as varchar(50) ,        
@columnSortOrder as varchar(4)  ,  
@numDomainId as numeric(9),  
@numUserCntId as numeric(9)  ,    
@numNodeId as numeric(9),  
--@chrSource as char(1),
@ClientTimeZoneOffset AS INT=0,
@tintUserRightType AS INT,
@EmailStatus AS NUMERIC(9)=0,
@srchFrom as varchar(100) = '', 
@srchTo as varchar(100) = '', 
@srchSubject as varchar(100) = '', 
@srchHasWords as varchar(100) = '', 
@srchHasAttachment as bit = 0, 
@srchIsAdvancedsrch as bit = 0,
@srchInNode as numeric(9)=0,
@FromDate AS DATE = NULL,
@ToDate AS DATE = NULL,
@dtSelectedDate AS DATE =NULL,
@bitExcludeEmailFromNonBizContact BIT = 0
as                                                      

--GET ENTERIES FROM PERTICULAR DOMAIN FROM VIEW_Email_Alert_Config
SELECT numDivisionID,numContactId,TotalBalanceDue,OpenSalesOppCount, OpenCaseCount, OpenProjectCount, UnreadEmailCount,OpenActionItemCount INTO #TEMP FROM VIEW_Email_Alert_Config WHERE numDomainId=@numDomainId

/************* START - GET ALERT CONFIGURATION **************/
DECLARE @bitOpenActionItem AS BIT = 1
DECLARE @bitOpencases AS BIT = 1
DECLARE @bitOpenProject AS BIT = 1
DECLARE @bitOpenSalesOpp AS BIT = 1
DECLARE @bitBalancedue AS BIT = 1
DECLARE @bitUnreadEmail AS BIT = 1
DECLARE @bitCampaign AS BIT = 1

SELECT  
	@bitOpenActionItem = ISNULL([bitOpenActionItem],1),
    @bitOpencases = ISNULL([bitOpenCases],1),
    @bitOpenProject = ISNULL([bitOpenProject],1),
    @bitOpenSalesOpp = ISNULL([bitOpenSalesOpp],1),
    @bitBalancedue = ISNULL([bitBalancedue],1),
    @bitUnreadEmail = ISNULL([bitUnreadEmail],1),
	@bitCampaign = ISNULL([bitCampaign],1)
FROM    
	AlertConfig
WHERE   
	AlertConfig.numDomainId = @numDomainId AND 
	AlertConfig.numContactId = @numUserCntId

/************* END - GET ALERT CONFIGURATION **************/
                                                      
 ---DECLARE @CRMType NUMERIC 
DECLARE @tintOrder AS TINYINT                                                      
DECLARE @vcFieldName AS VARCHAR(50)                                                      
DECLARE @vcListItemType AS VARCHAR(1)                                                 
DECLARE @vcAssociatedControlType VARCHAR(20)                                                      
DECLARE @numListID AS NUMERIC(9)                                                      
DECLARE @vcDbColumnName VARCHAR(20)                          
DECLARE @WhereCondition VARCHAR(2000)                           
DECLARE @vcLookBackTableName VARCHAR(2000)                    
DECLARE @bitCustom AS BIT
DECLARE @bitAllowEdit AS CHAR(1)                 
DECLARE @numFieldId AS NUMERIC  
DECLARE @bitAllowSorting AS CHAR(1)              
DECLARE @vcColumnName AS VARCHAR(500)  

DECLARE @strSql AS VARCHAR(5000)
DECLARE @column AS VARCHAR(50)                             
DECLARE @join AS VARCHAR(400) 
DECLARE @Nocolumns AS TINYINT               
--DECLARE @lastRec AS INTEGER 
--DECLARE @firstRec AS INTEGER                   
SET @join = ''           
SET @strSql = ''
--DECLARE @ClientTimeZoneOffset AS INT 
--DECLARE @numDomainId AS NUMERIC(18, 0)
SET @tintOrder = 0  
SET @WhereCondition = ''

--SET @firstRec = ( @CurrentPage - 1 ) * @PageSize                                                              
--SET @lastRec = ( @CurrentPage * @PageSize + 1 )   
SET @column = @columnName                
--DECLARE @join AS VARCHAR(400)                    
SET @join = ''             
IF @columnName LIKE 'Cust%' 
    BEGIN                  
        SET @join = ' left Join CFW_FLD_Values_Email CFW on CFW.RecId=DM.numEmailHstrID and CFW.fld_id= '
            + REPLACE(@columnName, 'Cust', '') + ' '                                                           
        SET @column = 'CFW.Fld_Value'
    END                                           
ELSE 
    IF @columnName LIKE 'DCust%' 
        BEGIN                  
            SET @join = ' left Join CFW_FLD_Values_Email CFW on CFW.RecId=DM.numEmailHstrID  and CFW.fld_id= '
                + REPLACE(@columnName, 'DCust', '')                                                                                                     
            SET @join = @join
                + ' left Join ListDetails LstCF on LstCF.numListItemID=CFW.Fld_Value  '                  
            SET @column = 'LstCF.vcData'                    
                  
        END            
    ELSE 
        BEGIN            
            DECLARE @lookbckTable AS VARCHAR(50)                
            SET @lookbckTable = ''                                                         
            SELECT  @lookbckTable = vcLookBackTableName
            FROM    View_DynamicDefaultColumns
            WHERE   numFormId = 44
                    AND vcDbColumnName = @columnName  and numDomainID=@numDOmainID               
            IF @lookbckTable <> '' 
                BEGIN                
                    IF @lookbckTable = 'EmailHistory' 
                        SET @column = 'ADC.' + @column                
                END                                                              
        END              
--DECLARE @strSql AS VARCHAR(5000)   
--SET @numNodeID = 0  
--SET @srchBody = ''
--SET @vcSubjectFromTo =''
--PRINT 'numnodeid' + CONVERT(VARCHAR, @numNodeId)                                 
SET @strSql = '
with FilterRows as (SELECT ' + (CASE WHEN LEN(ISNULL(@srch,'')) > 0 THEN 'TOP(100) ' ELSE ' ' END)               
SET @strSql = @strSql + 'ROW_NUMBER() OVER(ORDER BY EH.bintCreatedOn DESC ) AS RowNumber,COUNT(*) OVER(PARTITION BY NULL) AS TotalRowCount,EH.numEmailHstrID,isnull(EM.numContactId,0) as numContactId'
SET @strSql = @strSql + ' FROM [EmailHistory] EH JOIN EmailMaster EM on EM.numEMailId=(CASE numNodeID WHEN 4 THEN SUBSTRING(EH.vcTo, 1, CHARINDEX(''$^$'', EH.vcTo, 1) - 1) ELSE EH.numEmailId END)'
SET @strSql = @strSql + ' 
WHERE EH.numDomainID ='+ CONVERT(VARCHAR,@numDomainId) +' And [numUserCntId] = '+ CONVERT(VARCHAR,@numUserCntId) 

IF LEN(ISNULL(@srch,'')) = 0
BEGIN
	SET @strSql = @strSql + ' AND CAST(DATEADD(MINUTE,'+ CAST(@ClientTimeZoneOffset AS VARCHAR) +' * -1,EH.dtReceivedOn) AS DATE) = '''+ CONVERT(VARCHAR,@dtSelectedDate) + '''' + ' ' 
END

SET @strSql = @strSql + ' AND chrSource IN(''B'',''I'') '

IF ISNULL(@bitExcludeEmailFromNonBizContact,0) = 1
BEGIN
	SET @strSql = @strSql + ' AND ISNULL(EM.numContactId,0) > 0 '
END
 
--Simple Search for All Node
IF @srchIsAdvancedsrch=0 
BEGIN
if len(@srch)>0
	BEGIN
		SET @strSql = @strSql + '
			AND (EH.vcSubject LIKE ''%'' + '''+ @srch +''' + ''%'' or EH.vcBodyText LIKE ''%'' + '''+ @srch + ''' + ''%''
			or EH.vcFrom LIKE ''%'' + '''+ @srch + ''' + ''%'' or EH.vcTo LIKE ''%'' + '''+ @srch + ''' + ''%'')'
	END
ELSE
	BEGIN --Particular Node
		SET @strSql = @strSql + ' AND [numNodeID] = '+ CONVERT(VARCHAR,@numNodeID) +' '
	END
END
--Advanced Search for All Node or selected Node
Else IF @srchIsAdvancedsrch=1
BEGIN
declare @strCondition as varchar(2000);set @strCondition=''

if len(@srchSubject)>0
	SET @strCondition='EH.vcSubject LIKE ''%'' + '''+ @srchSubject +''' + ''%'''

if len(@srchFrom)>0
	begin
		if len(@strCondition)>0
			SET @strCondition=@strCondition + ' and '

		SET @strCondition = @strCondition + ' (EH.vcFrom LIKE ''%' + REPLACE(@srchFrom,',','%'' OR EH.vcFrom LIKE ''%') + '%'')'
	end

if len(@srchTo)>0
	begin
		if len(@strCondition)>0
			SET @strCondition=@strCondition + ' and '

		SET @strCondition = @strCondition + ' (EH.vcTo LIKE ''%' + REPLACE(@srchTo,',','%'' OR EH.vcTo LIKE ''%') + '%'')' 
	end

if len(@srchHasWords)>0
	begin
		if len(@strCondition)>0
			SET @strCondition=@strCondition + ' and '

		SET @strCondition = @strCondition + ' EH.vcBodyText LIKE ''%'' + '''+ @srchHasWords + ''' + ''%'''
	end

IF @srchHasAttachment=1
BEGIN
	if len(@strCondition)>0
			SET @strCondition=@strCondition + ' and '
	
	SET @strCondition = @strCondition + ' EH.bitHasAttachments =  '''+ CONVERT(VARCHAR,@srchHasAttachment) + ''''
END

IF (LEN(@FromDate) > 0 AND  ISDATE(CAST(@FromDate AS VARCHAR(100))) = 1)
BEGIN
	IF LEN(@strCondition)>0
		SET @strCondition=@strCondition + ' AND '

	SET @strCondition = @strCondition + ' EH.dtReceivedOn >= '''+ CONVERT(VARCHAR,@FromDate) + ''''
END

IF (LEN(@ToDate) > 0 AND ISDATE(CAST(@ToDate AS VARCHAR(100))) = 1)
BEGIN
	IF LEN(@strCondition)>0
		SET @strCondition=@strCondition + ' AND '

	SET @strCondition = @strCondition + ' EH.dtReceivedOn <= ''' + CONVERT(VARCHAR,@ToDate) + ''''
END

if len(@strCondition)>0
	BEGIN
		SET @strSql = @strSql +' and (' + @strCondition + ')'
		
		if @numNodeId>-1
			SET @strSql = @strSql + ' AND [numNodeID] = '+ CONVERT(VARCHAR,@numNodeId) +' '
	END
ELSE
	BEGIN --Particular Node
		SET @strSql = @strSql + ' AND [numNodeID] = '+ CONVERT(VARCHAR,@numNodeID) +' '
	END



--SET @strSql = @strSql + '
--AND (EH.vcSubject LIKE ''%'' + '''+ @srchSubject +''' + ''%'' and EH.vcFrom LIKE ''%'' + '''+ @srchFrom + ''' + ''%'' 
--and EH.vcTo LIKE ''%'' + '''+ @srchTo + ''' + ''%'' and EH.vcBodyText LIKE ''%'' + '''+ @srchHasWords + ''' + ''%'''
--
--IF @srchHasAttachment=1
--BEGIN
--SET @strSql = @strSql + ' and EH.bitHasAttachments =  '''+ CONVERT(VARCHAR,@srchHasAttachment) + ''''
--END

END


SET @strSql = @strSql + ' )'
--,FinalResult As
--( SELECT  TOP ('+CONVERT(VARCHAR,@PageSize)+ ') numEmailHstrID'
--SET @strSql = @strSql + ' 
--From FilterRows'
--SET @strSql = @strSql + '
--WHERE RowNumber > (('+ CONVERT(VARCHAR,@CurrentPage) + ' - 1) * '+ CONVERT(VARCHAR,@PageSize) +' ))'
--PRINT @strSql
--EXEC(@strSql)

select @Nocolumns=isnull(sum(TotalRow),0)  from(            
Select count(*) TotalRow from View_DynamicColumns where numFormId=44 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 AND numRelCntType=0 
Union 
Select count(*) TotalRow from View_DynamicCustomColumns where numFormId=44 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 AND numRelCntType=0) TotalRows

  CREATE TABLE #tempForm (tintOrder TINYINT,vcDbColumnName nvarchar(50),vcFieldName nvarchar(50),vcAssociatedControlType nvarchar(50),
  vcListItemType char(3),numListID numeric(9),vcLookBackTableName varchar(50),bitCustomField BIT,
  numFieldId NUMERIC,bitAllowSorting BIT,bitAllowEdit BIT,bitIsRequired BIT,bitIsEmail BIT,bitIsAlphaNumeric BIT,bitIsNumeric BIT,
  bitIsLengthValidation BIT,intMaxLength int,intMinLength int,bitFieldMessage BIT,vcFieldMessage VARCHAR(500),ListRelID numeric(9))


---Insert number of rows into temp table.
PRINT 'number OF columns :' + CONVERT(VARCHAR, @Nocolumns)
IF  @Nocolumns > 0 
    BEGIN    

INSERT INTO #tempForm
select tintRow+1 as tintOrder,vcDbColumnName,ISNULL(vcCultureFieldName,vcFieldName),                  
 vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName                                                 
,bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID 
 FROM View_DynamicColumns 
 where numFormId=44 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 AND numRelCntType=0
  AND ISNULL(bitSettingField,0)=1 AND ISNULL(bitCustom,0)=0
                  
    UNION

     select tintRow+1 as tintOrder,vcDbColumnName,vcFieldName,                  
 vcAssociatedControlType,'' as vcListItemType,numListID,''                                                 
,bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID
 from View_DynamicCustomColumns
 where numFormId=44 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 AND numRelCntType=0
 AND ISNULL(bitCustom,0)=1
 
 ORDER BY tintOrder ASC  
   
 END 
ELSE 
    BEGIN

 INSERT INTO #tempForm
select tintOrder,vcDbColumnName,ISNULL(vcCultureFieldName,vcFieldName),                  
 vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName                                                 
,bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID 
 FROM View_DynamicDefaultColumns
 where numFormId=44 and bitDefault=1 AND ISNULL(bitSettingField,0)=1 AND numDomainID=@numDomainID
order by tintOrder asc   

    END
      SET @strSql =@strSql + ' Select TotalRowCount, EH.numEmailHstrID ,EH.numListItemId,bitIsRead,CASE WHEN LEN(Cast(EH.vcBodyText AS VARCHAR(1000)))>150 THEN SUBSTRING(EH.vcBodyText,0,150) + ''...'' ELSE
		 EH.vcBodyText END as vcBodyText,EH.numEmailHstrID As [KeyId~numEmailHstrID~0~0~0~0~HiddenField],FR.numContactId AS [ContactId~numContactID~0~0~0~0~HiddenField] ,EH.bitIsRead As [IsRead~bitIsRead~0~0~0~0~Image],isnull(EH.IsReplied,0) As [~IsReplied~0~0~0~0~Image] '
 
Declare @ListRelID as numeric(9) 

   select top 1 @tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName,                
  @vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,@numListID=numListID,@vcLookBackTableName=vcLookBackTableName                                               
,@bitCustom=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,@bitAllowEdit=bitAllowEdit,@ListRelID=ListRelID
  from  #tempForm order by tintOrder asc            

WHILE @tintOrder > 0                                                      
    BEGIN                                                      
        IF @bitCustom = 0  
            BEGIN            
                DECLARE @Prefix AS VARCHAR(5)                        
                IF @vcLookBackTableName = 'EmailHistory' 
                    SET @Prefix = 'EH.'                        
                SET @vcColumnName = @vcFieldName + '~' + @vcDbColumnName
                    + '~' + @bitAllowSorting + '~'
                    + CONVERT(VARCHAR(10), @numFieldId) + '~'
                    + @bitAllowEdit + '~' + CONVERT(VARCHAR(1), @bitCustom)
                    + '~' + @vcAssociatedControlType
                PRINT @vcColumnName
                
                IF @vcAssociatedControlType = 'SelectBox' 
                    BEGIN      
                        PRINT @vcListItemType                                                    
                        IF @vcListItemType = 'L' 
                            BEGIN    
                            ---PRINT 'columnName' + @vcColumnName
                                SET @strSql = @strSql + ',L'
                                    + CONVERT(VARCHAR(3), @tintOrder)
                                    + '.vcData' + ' [' + @vcColumnName + ']'                                                         
                                SET @WhereCondition = @WhereCondition
                                    + ' left Join ListDetails L'
                                    + CONVERT(VARCHAR(3), @tintOrder)
                                    + ' on L' + CONVERT(VARCHAR(3), @tintOrder)
                                    + '.numListItemID=EH.numListItemID'                                                           
                            END 
                           
                    END 
                  ELSE IF (@vcAssociatedControlType = 'TextBox'
                        OR @vcAssociatedControlType = 'TextArea'
                        OR @vcAssociatedControlType = 'Image')  AND @vcDbColumnName<>'AlertPanel' AND @vcDbColumnName<>'RecentCorrespondance'
                        BEGIN
							IF @numNodeId=4 and @vcDbColumnName='vcFrom'  
							BEGIN
								  SET @vcColumnName = 'To' + '~' + @vcDbColumnName
                    + '~' + @bitAllowSorting + '~'
                    + CONVERT(VARCHAR(10), @numFieldId) + '~'
                    + @bitAllowEdit + '~' + CONVERT(VARCHAR(1), @bitCustom)
                    + '~' + @vcAssociatedControlType

								 SET @strSql = @strSql + ',' + @Prefix  + 'vcTo' + ' [' + @vcColumnName + ']'   
							END 
							ELSE
							BEGIN
								SET @strSql = @strSql + ',' + @Prefix  + @vcDbColumnName + ' [' + @vcColumnName + ']'   
							END                        
                             PRINT @Prefix   
                        END 
				Else IF  @vcDbColumnName='AlertPanel'   
							BEGIN 
								SET @strSql = @strSql +  ', dbo.GetAlertDetail(EH.numEmailHstrID, ACI.numDivisionID, ACI.numECampaignID, FR.numContactId,' 
													  + CAST(@bitOpenActionItem AS VARCHAR(10)) + ',' 
													  + CAST(@bitOpencases AS VARCHAR(10)) + ',' 
													  + CAST(@bitOpenProject AS VARCHAR(10)) + ',' 
													  + CAST(@bitOpenSalesOpp AS VARCHAR(10)) + ',' 
													  + CAST(@bitBalancedue AS VARCHAR(10)) + ',' 
													  + CAST(@bitUnreadEmail AS VARCHAR(10)) + ',' 
													  + CAST(@bitCampaign AS VARCHAR(10)) + ',' +
													  'V1.TotalBalanceDue,V1.OpenSalesOppCount,V1.OpenCaseCount,V1.OpenProjectCount,V1.UnreadEmailCount,V2.OpenActionItemCount,V3.CampaignDTLCount) AS  [' + '' + @vcColumnName + '' + ']' 
							 END 
                 ELSE IF  @vcDbColumnName='RecentCorrespondance'
							BEGIN 
					            --SET @strSql = @strSql + ','+ '(SELECT COUNT(*) FROM dbo.Communication WHERE numContactId = FR.numContactId)' + ' AS  [' + '' + @vcColumnName + '' + ']'                                                       
					            SET @strSql = @strSql + ',0 AS  [' + '' + @vcColumnName + '' + ']'
                             END 
--				else if @vcAssociatedControlType='DateField'                                                  
--					begin            
--							set @strSql=@strSql+',case when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',' +@Prefix+@vcDbColumnName+') )= convert(varchar(11),getdate()) then ''<b><font color=red>Today</font></b>'''      
--							set @strSql=@strSql+'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',' +@Prefix+@vcDbColumnName+') )= convert(varchar(11),dateadd(day,-1,getdate())) then ''<b><font color=purple>YesterDay</font></b>'''      
--							set @strSql=@strSql+'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',' +@Prefix+@vcDbColumnName+') )= convert(varchar(11),dateadd(day,1,getdate())) then''<b><font color=orange>Tommorow</font></b>'' ' 
--							set @strSql=@strSql+'else dbo.FormatedDateFromDate(DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',' +@Prefix+@vcDbColumnName+'),'+convert(varchar(10),@numDomainId)+') end  ['+ @vcColumnName+']'
--				    end    
				else if @vcAssociatedControlType='DateField' AND @vcDbColumnName <> 'dtReceivedOn'                                     
					begin           
							set @strSql=@strSql+',case when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',' +@Prefix+@vcDbColumnName+') )= convert(varchar(11),getdate()) then ''<b><font color=red>Today</font></b>'''      
							set @strSql=@strSql+'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',' +@Prefix+@vcDbColumnName+') )= convert(varchar(11),dateadd(day,-1,getdate())) then ''<b><font color=purple>YesterDay</font></b>'''      
							set @strSql=@strSql+'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',' +@Prefix+@vcDbColumnName+') )= convert(varchar(11),dateadd(day,1,getdate())) then''<b><font color=orange>Tommorow</font></b>'' ' 
							set @strSql=@strSql+'else dbo.FormatedDateFromDate(DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',' +@Prefix+@vcDbColumnName+'),'+convert(varchar(10),@numDomainId)+') end  ['+ @vcColumnName+']'
				    end    
				else if @vcAssociatedControlType='DateField' AND @vcDbColumnName = 'dtReceivedOn'                                                 
					begin    
							set @strSql=@strSql+',case when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',' +@Prefix+@vcDbColumnName+') )= convert(varchar(11),getdate()) then ''<b><font color=red>Today</font></b>'''      
							set @strSql=@strSql+'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',' +@Prefix+@vcDbColumnName+') )= convert(varchar(11),dateadd(day,-1,getdate())) then ''<b><font color=purple>YesterDay</font></b>'''      
							set @strSql=@strSql+'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',' +@Prefix+@vcDbColumnName+') )= convert(varchar(11),dateadd(day,1,getdate())) then''<b><font color=orange>Tommorow</font></b>'' ' 
							set @strSql=@strSql+'else CONVERT(VARCHAR(20),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',' +@Prefix+@vcDbColumnName+'),100) end  ['+ @vcColumnName+']'
				    end   
			END 
else if @bitCustom = 1                
begin                
            
               SET @vcColumnName= @vcFieldName+'~'+ @vcDbColumnName+'~0~'+ convert(varchar(10),@numFieldId)+'~1~'+ convert(varchar(1),@bitCustom) +'~'+ @vcAssociatedControlType
       
   select @vcFieldName=FLd_label,@vcAssociatedControlType=fld_type,@vcDbColumnName='Cust'+Convert(varchar(10),Fld_Id)                               
    from CFW_Fld_Master                                                
   where    CFW_Fld_Master.Fld_Id = @numFieldId                 
                
                 
    if @vcAssociatedControlType = 'TextBox' or @vcAssociatedControlType = 'TextArea'               
   begin                
                   
    set @strSql= @strSql+',CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value  ['+ @vcColumnName +']'                   
    set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values_Cont CFW'+ convert(varchar(3),@tintOrder)+ '                 
    on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=FR.numContactId   '                                                         
   end      
    else if @vcAssociatedControlType = 'CheckBox'                 
   begin              
                 
    set @strSql= @strSql+',case when isnull(CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value,0)=0 then ''No'' when isnull(CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value,0)=1 then ''Yes'' end   ['+ @vcColumnName +']'              
   
    set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values_Cont CFW'+ convert(varchar(3),@tintOrder)+ '               
    on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=FR.numContactId   '                                                       
   end              
  else if @vcAssociatedControlType = 'DateField'              
   begin                
                   
    set @strSql= @strSql+',dbo.FormatedDateFromDate(CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value,'+convert(varchar(10),@numDomainId)+')  ['+ @vcColumnName +']'                   
    set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values_Cont CFW'+ convert(varchar(3),@tintOrder)+ '                 
    on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=FR.numContactId   '                                                         
   end                
   else if @vcAssociatedControlType = 'SelectBox'                    
   begin                
    set @vcDbColumnName = 'DCust'+Convert(varchar(10),@numFieldId)                
    set @strSql=@strSql+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName +']'                                                          
    set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values_Cont CFW'+ convert(varchar(3),@tintOrder)+ '                 
     on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=FR.numContactId   '                                                         
    set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID=CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value'                
   end                 
end     
                   --    SET @vcColumnName= @vcFieldName+'~'+ @vcDbColumnName+'~0~'+ convert(varchar(10),@numFieldId)+'~1~'+ convert(varchar(1),@bitCustom) +'~'+ @vcAssociatedControlType   
  select top 1 @tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName,                
  @vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,@numListID=numListID,@vcLookBackTableName=vcLookBackTableName                                               
,@bitCustom=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,@bitAllowEdit=bitAllowEdit,@ListRelID=ListRelID
  from #tempForm WHERE tintOrder > @tintOrder-1 order by tintOrder asc            
 
           IF @@rowcount = 0 SET @tintOrder = 0 
		   PRINT @tintOrder 
    END  
                     
SELECT  * FROM    #tempForm

-------Change Row Color-------
Declare @vcCSOrigDbCOlumnName as varchar(50),@vcCSLookBackTableName as varchar(50),@vcCSAssociatedControlType as varchar(50)
SET @vcCSOrigDbCOlumnName=''
SET @vcCSLookBackTableName=''

Create table #tempColorScheme(vcOrigDbCOlumnName varchar(50),vcLookBackTableName varchar(50),vcFieldValue varchar(50),vcFieldValue1 varchar(50),vcColorScheme varchar(50),vcAssociatedControlType varchar(50))

insert into #tempColorScheme select DFM.vcOrigDbCOlumnName,DFM.vcLookBackTableName,DFCS.vcFieldValue,DFCS.vcFieldValue1,DFCS.vcColorScheme,DFFM.vcAssociatedControlType 
from DycFieldColorScheme DFCS join DycFormField_Mapping DFFM on DFCS.numFieldID=DFFM.numFieldID AND DFCS.numFormID=DFFM.numFormID
join DycFieldMaster DFM on DFM.numModuleID=DFFM.numModuleID and DFM.numFieldID=DFFM.numFieldID
where DFCS.numDomainID=@numDomainID and DFFM.numFormID=10 AND DFCS.numFormID=10 and isnull(DFFM.bitAllowGridColor,0)=1

IF(SELECT COUNT(*) FROM #tempColorScheme)>0
BEGIN
   select top 1 @vcCSOrigDbCOlumnName=vcOrigDbCOlumnName,@vcCSLookBackTableName=vcLookBackTableName,@vcCSAssociatedControlType=vcAssociatedControlType from #tempColorScheme
END   
---------------------------- 

IF len(@vcCSOrigDbCOlumnName)>0 and len(@vcCSLookBackTableName)>0
BEGIN
set @strSql=@strSql+',tCS.vcColorScheme'
END  

DECLARE @fltTotalSize FLOAT
SELECT @fltTotalSize=isnull(SUM(ISNULL(SpaceOccupied,0)),0) FROM dbo.View_InboxCount where numDomainID=@numDomainID and numUserCntID=@numUserCntID;

SET @strSql = @strSql + ', '+ CONVERT(VARCHAR(18),@fltTotalSize) +' AS TotalSize From EmailHistory EH Inner Join FilterRows FR on  FR.numEmailHstrID = EH.numEmailHstrID '
SET @strSql=@strSql + ' LEFT JOIN
	AdditionalContactsInformation ACI
ON 
	ACI.numContactId = FR.numContactId
CROSS APPLY
(
	SELECT
		SUM(TotalBalanceDue) TotalBalanceDue,
		SUM(OpenSalesOppCount) OpenSalesOppCount,
		SUM(OpenCaseCount) OpenCaseCount,
		SUM(OpenProjectCount) OpenProjectCount,
		SUM(UnreadEmailCount) UnreadEmailCount
	FROM
		#TEMP
	WHERE
		#TEMP.numDivisionId = ACI.numDivisionId
) AS V1
CROSS APPLY
(
	SELECT
		SUM(OpenActionItemCount) OpenActionItemCount
	FROM
		#TEMP
	WHERE
		#TEMP.numDivisionId = ACI.numDivisionId AND
		#TEMP.numContactId = FR.numContactId
) AS V2
CROSS APPLY
(
	SELECT 
		COUNT(*) AS CampaignDTLCount
	FROM 
		ConECampaignDTL 
	WHERE 
		numConECampID = (SELECT TOP 1 ISNULL(numConEmailCampID,0)  FROM ConECampaign WHERE numECampaignID = ACI.numECampaignID AND numContactID = ACI.numContactId ORDER BY numConEmailCampID DESC) AND 
		ISNULL(bitSend,0) = 0
) AS V3 '
SET @strSql = @strSql + @WhereCondition

IF len(@vcCSOrigDbCOlumnName)>0 and len(@vcCSLookBackTableName)>0
BEGIN
	 if @vcCSLookBackTableName = 'AdditionalContactsInformation'                  
		set @Prefix = 'ADC.'                  
     if @vcCSLookBackTableName = 'DivisionMaster'                  
		set @Prefix = 'DM.'
	 if @vcCSLookBackTableName = 'CompanyInfo'                  
		set @Prefix = 'CI.' 
	 if @vcCSLookBackTableName = 'AddressDetails'                  
		set @Prefix = 'AD.'   

set @strSql=@strSql+' left JOIN AdditionalContactsInformation ADC ON ADC.numcontactId=FR.numContactID                                                                             
 left JOIN DivisionMaster DM ON ADC.numDivisionId = DM.numDivisionID                                                                            
 left JOIN CompanyInfo CI ON DM.numCompanyID = CI.numCompanyId '
   
	IF @vcCSAssociatedControlType='DateField'
	BEGIN
			set @strSql=@strSql+' left join #tempColorScheme tCS on CONVERT(VARCHAR(10),' + @Prefix + @vcCSOrigDbCOlumnName + ',111) >= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue as int),GETUTCDATE()),111)
			 and CONVERT(VARCHAR(10),' + @Prefix + @vcCSOrigDbCOlumnName + ',111) <= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue1 as int),GETUTCDATE()),111)'
	END
	ELSE IF @vcCSAssociatedControlType='SelectBox' OR @vcCSAssociatedControlType='ListBox' OR @vcCSAssociatedControlType='CheckBox'
	BEGIN
		set @strSql=@strSql+' left join #tempColorScheme tCS on tCS.vcFieldValue=' + @Prefix + @vcCSOrigDbCOlumnName
	END
END

--SET @strSql = @strSql + ' Where RowNumber >'  + CONVERT(VARCHAR(10), @firstRec) + ' and RowNumber <'
--        + CONVERT(VARCHAR(10), @lastRec) 
IF @EmailStatus <> 0 
BEGIN
	SET @strSql = @strSql + ' AND EH.numListItemId = ' + CONVERT(VARCHAR(10), @EmailStatus)
END       
SET @strSql = @strSql + ' order by RowNumber'

PRINT @strSql 
EXEC(@strSql)   
DROP TABLE #tempForm          
DROP TABLE #TEMP
 
 /*
 declare @firstRec as integer                                                      
 declare @lastRec as integer                                                      
 set @firstRec= (@CurrentPage-1) * @PageSize                                                      
 set @lastRec= (@CurrentPage*@PageSize+1)   ;    
 
DECLARE @totalRows  INT
DECLARE @fltTotalSize FLOAT
SELECT @totalRows = RecordCount FROM dbo.View_InboxCount where numDomainID=@numDomainID and numUserCntID=@numUserCntID and numNodeId = @numNodeID ;
SELECT @fltTotalSize=SUM(ISNULL(SpaceOccupied,0)) FROM dbo.View_InboxCount where numDomainID=@numDomainID and numUserCntID=@numUserCntID;



-- Step 1: Get Only rows specific to user and domain 

WITH FilterRows
AS 
(
   SELECT 
   TOP (@CurrentPage * @PageSize)
        ROW_NUMBER() OVER(ORDER BY EH.bintCreatedOn DESC ) AS totrows,
        EH.numEmailHstrID
   FROM [EmailHistory] EH
		--LEFT JOIN EmailHstrAttchDtls A ON EH.numEmailHstrID = A.numEmailHstrID
   WHERE 
	   [numDomainID] = @numDomainID
	AND [numUserCntId] = @numUserCntId
	AND numNodeID = @numNodeId
	AND chrSource IN('B','I')
    AND (EH.vcBodyText LIKE '%' + @srchBody + '%' OR @srchBody = '')
	AND (EH.vcSubject LIKE '%' + @vcSubjectFromTo + '%' OR @vcSubjectFromTo ='')
    --AND (A.[vcAttachmentType] LIKE '%'+ @srchAttachmenttype + '%' OR @srchAttachmenttype='')
   
)
 --select seq1 + totrows1 -1 as TotRows2,* FROM [FilterRows]
,FinalResult
AS 
(
	SELECT  TOP (@PageSize) numEmailHstrID --,seq + totrows -1 as TotRows
	FROM FilterRows
	WHERE totrows > ((@CurrentPage - 1) * @PageSize)
)



-- select * FROM [UserEmail]
select  
		@totalRows TotRows,
		@fltTotalSize AS TotalSize,
		B.[numEmailHstrID],
        B.[numDomainID],
--        B.bintCreatedOn,
--        B.dtReceivedOn,
        B.numEmailHstrID,
        ISNULL(B.numListItemId,-1) AS numListItemId,
        ISNULL(vcSubject, '') AS vcSubject,
        CONVERT(VARCHAR(MAX), vcBody) AS vcBody,
        ISNULL(vcItemId, '') AS ItemId,
        ISNULL(vcChangeKey, '') AS ChangeKey,
        ISNULL(bitIsRead, 'False') AS IsRead,
        ISNULL(vcSize, 0) AS vcSize,
        ISNULL(bitHasAttachments, 'False') AS HasAttachments,
        ISNULL(B.tintType, 1) AS type,
        ISNULL(B.chrSource, 'B') AS chrSource,
        ISNULL(vcCategory, 'white') AS vcCategory,
        CASE B.[numNodeId] WHEN 4 THEN REPLACE(REPLACE(ISNULL(B.[vcTo],''),'<','') , '>','') ELSE B.vcFrom END FromName,
--        dbo.GetEmaillName(B.numEmailHstrID, 4) AS FromName,
--        dbo.GetEmaillName(B.numEmailHstrID, 1) AS ToName,
		CASE B.[numNodeId] WHEN 4 THEN 
			dbo.FormatedDateTimeFromDate(DATEADD(minute, -@ClientTimeZoneOffset,B.bintCreatedOn),B.numDomainId) 
		ELSE 
			dbo.FormatedDateTimeFromDate(B.dtReceivedOn,B.numDomainId)
		END dtReceivedOn,
        ISNULL(CONVERT(VARCHAR(5),B.[numNoofTimes]),'') NoOfTimeOpened
FROM [FinalResult] A INNER JOIN emailHistory B ON B.numEmailHstrID = A.numEmailHstrID;



--WITH UserEmail
--AS
--(
--			   SELECT
--						ROW_NUMBER() OVER ( ORDER BY X.bintCreatedOn DESC ) AS RowNumber,
--                        X.[numEmailHstrID]
--               FROM		[EmailHistory] X
--               WHERE    [numDomainID] = @numDomainID
--						AND [numUserCntId] = @numUserCntId
--						AND numNodeID = @numNodeId
--						AND X.chrSource IN('B','I')
--						
--)
--	SELECT 
--		COUNT(*) 
--   FROM UserEmail UM INNER JOIN [EmailHistory] EH  ON UM.numEmailHstrID =EH.numEmailHstrID
--		LEFT JOIN EmailHstrAttchDtls A ON EH.numEmailHstrID = A.numEmailHstrID
--   WHERE 
--		 (EH.vcBodyText LIKE '%' + @srchBody + '%' OR @srchBody = '')
--	AND (EH.vcSubject LIKE '%' + @vcSubjectFromTo + '%' OR @vcSubjectFromTo ='')
--    AND (A.[vcAttachmentType] LIKE '%'+ @srchAttachmenttype + '%' OR @srchAttachmenttype='')



--SELECT COUNT(*)
--FROM      View_Inbox I INNER JOIN [EmailHistory] EH ON EH.numEmailHstrID = I.numEmailHstrID
--			LEFT JOIN EmailHstrAttchDtls A ON EH.numEmailHstrID = A.numEmailHstrID
-- WHERE     I.numDomainId = @numDomainId
--                                AND I.numUserCntId = @numUserCntId
--                                AND I.numNodeId = @numNodeId
--                                AND I.chrSource IN('B','I')
--                                AND (EH.vcBodyText LIKE '%' + @srchBody + '%' OR @srchBody = '')
--								AND (I.vcSubjectFromTo LIKE '%' + @vcSubjectFromTo + '%' OR @vcSubjectFromTo ='')
--                                AND (A.[vcAttachmentType] LIKE '%'+ @srchAttachmenttype + '%' OR @srchAttachmenttype='')

RETURN ;
*/                                           
/* SELECT Y.[ID],
        Y.[numEmailHstrID],
        emailHistory.[numDomainID],
        emailHistory.bintCreatedOn,
        emailHistory.dtReceivedOn,
        emailHistory.numEmailHstrID,
        ISNULL(vcSubject, '') AS vcSubject,
        CONVERT(VARCHAR(MAX), vcBody) AS vcBody,
        ISNULL(vcItemId, '') AS ItemId,
        ISNULL(vcChangeKey, '') AS ChangeKey,
        ISNULL(bitIsRead, 'False') AS IsRead,
        ISNULL(vcSize, 0) AS vcSize,
        ISNULL(bitHasAttachments, 'False') AS HasAttachments,
        ISNULL(emailHistory.tintType, 1) AS type,
        ISNULL(emailHistory.chrSource, 'B') AS chrSource,
        ISNULL(vcCategory, 'white') AS vcCategory,
        dbo.GetEmaillName(Y.numEmailHstrID, 4) AS FromName,
        dbo.GetEmaillName(Y.numEmailHstrID, 1) AS ToName,
        dbo.FormatedDateFromDate(DATEADD(minute, -@ClientTimeZoneOffset,
                                         emailHistory.bintCreatedOn),
                                 emailHistory.numDomainId) AS bintCreatedOn,
        dbo.FormatedDateFromDate(emailHistory.dtReceivedOn,
                                 emailHistory.numDomainId) AS dtReceivedOn,
        ISNULL(CONVERT(VARCHAR(5),EmailHistory.[numNoofTimes]),'') NoOfTimeOpened
 FROM   ( SELECT   
				ROW_NUMBER() OVER ( ORDER BY X.bintCreatedOn DESC ) AS ID,
				X.[numEmailHstrID]
          FROM      ( SELECT    DISTINCT
                                EH.numEmailHstrID,
                                EH.[bintCreatedOn]
                      FROM      --View_Inbox I INNER JOIN 
                      [EmailHistory] EH --ON EH.numEmailHstrID = I.numEmailHstrID
								LEFT JOIN EmailHstrAttchDtls A ON EH.numEmailHstrID = A.numEmailHstrID
                      WHERE     EH.numDomainId = @numDomainId
                                AND EH.numUserCntId = @numUserCntId
                                AND EH.numNodeId = @numNodeId
                                AND EH.chrSource IN('B','I')
                                AND (EH.vcBodyText LIKE '%' + @srchBody + '%' OR @srchBody = '')
								AND (EH.vcSubject LIKE '%' + @vcSubjectFromTo + '%' OR @vcSubjectFromTo ='')
                                AND (A.[vcAttachmentType] LIKE '%'+ @srchAttachmenttype + '%' OR @srchAttachmenttype='')
                    ) X
          
        ) Y
        INNER JOIN emailHistory ON emailHistory.numEmailHstrID = Y.numEmailHstrID
--        LEFT JOIN EmailHstrAttchDtls ON emailHistory.numEmailHstrID = EmailHstrAttchDtls.numEmailHstrID
        AND Y.ID > @firstRec AND Y.ID < @lastRec
--UNION
--   SELECT   0 AS ID,
--			COUNT(*),
--            NULL,
--            NULL,
--            NULL,
--            NULL,
--            NULL,
--            NULL,
--            NULL,
--            NULL,
--            NULL,
--            NULL,
--            NULL,
----            NULL,
----            NULL,
----            NULL,
--            1,
--            NULL,
--            NULL,
--            NULL,
--            NULL,
--            NULL,
--            NULL,
--            ''
--FROM      View_Inbox I INNER JOIN [EmailHistory] EH ON EH.numEmailHstrID = I.numEmailHstrID
--			LEFT JOIN EmailHstrAttchDtls A ON EH.numEmailHstrID = A.numEmailHstrID
-- WHERE     I.numDomainId = @numDomainId
--                                AND I.numUserCntId = @numUserCntId
--                                AND I.numNodeId = @numNodeId
--                                AND I.chrSource IN('B','I')
--                                AND (EH.vcBodyText LIKE '%' + @srchBody + '%' OR @srchBody = '')
--								AND (I.vcSubjectFromTo LIKE '%' + @vcSubjectFromTo + '%' OR @vcSubjectFromTo ='')
--                                AND (A.[vcAttachmentType] LIKE '%'+ @srchAttachmenttype + '%' OR @srchAttachmenttype='')
--   ORDER BY ID

RETURN 
*/
/*
declare @strSql as varchar(8000)                    
   if @columnName = 'FromName'               
 begin        
 set @columnName =  'dbo.GetEmaillName(emailHistory.numEmailHstrID,4)'        
 end        
 if @columnName = 'FromEmail'        
 begin        
 set @columnName = 'dbo.GetEmaillAdd(emailHistory.numEmailHstrID,4)'        
 end        
  if @columnName = 'ToName'               
 begin        
 set @columnName =  'dbo.GetEmaillName(emailHistory.numEmailHstrID,1)'        
 end        
 if @columnName = 'ToEmail'        
 begin        
 set @columnName = 'dbo.GetEmaillAdd(emailHistory.numEmailHstrID,1)'        
 end 
                  
set @strSql='With tblSubscriber AS (SELECT  distinct(emailHistory.numEmailHstrID) ,            
ROW_NUMBER() OVER (ORDER BY '+@columnName+' '+@columnSortOrder+') AS  RowNumber             
from emailHistory                                
--left join EmailHstrAttchDtls on emailHistory.numEmailHstrID=EmailHstrAttchDtls.numEmailHstrID                     
--join EmailHStrToBCCAndCC on emailHistory.numEmailHstrID=EmailHStrToBCCAndCC.numEmailHstrID                     
--join EmailMaster on        EmailMaster.numEmailId=EmailHStrToBCCAndCC.numEmailId                     
where numDomainId='+convert(varchar(15),@numDomainId)+' and numUserCntId='+convert(varchar(15),@numuserCntId)+  
' and numNodeId='+convert(varchar(15),@numNodeId)  
    
 if @chrSource<> '0'   set @strSql=@strSql +' and (chrSource  = ''B'' or chrSource like ''%'+@chrSource+'%'')'   
           
 if @srchBody <> '' set @strSql=@strSql +' and (vcSubject  like ''%'+@srchBody+'%'' or  
  vcBodyText like ''%'+@srchBody+'%''            
  or emailHistory.numEmailHstrID in (select numEmailHstrId from EmailHStrToBCCAndCC where             
 (numemailid in (select numEmailid from emailmaster where vcemailid like ''%'+@srchBody+'%'')) or   
  vcName like ''%'+@srchBody+'%'') )'                
--if @ToEmail <> '' set @strSql=@strSql +'               
--      and (EmailMaster.vcEmailId like '''+@ToEmail+''' and EmailHStrToBCCAndCC.tintType<>4 )  '            
if @srchAttachmenttype <>'' set @strSql=@strSql +'              
  and  emailHistory.numEmailHstrID in  (select numEmailHstrId from EmailHstrAttchDtls                
  where EmailHstrAttchDtls.vcAttachmentType like ''%'+@srchAttachmenttype+'%'')'            */
            
            
/*Please Do not use GetEmaillAdd function as it recursively fetched email address for single mail which create too bad performance*/                 

/*set @strSql=@strSql +')                     
 select RowNumber,            
--dbo.GetEmaillAdd(emailHistory.numEmailHstrID,4) as FromEmail,                  
dbo.GetEmaillName(emailHistory.numEmailHstrID,4) as FromName,                             
--isnull(vcFromEmail,'''') as FromEmail,                                      
--dbo.GetEmaillAdd(emailHistory.numEmailHstrID,1) as ToEmail,                                 
  
--dbo.GetEmaillAdd(emailHistory.numEmailHstrID,2) as CCEmail,                                
--dbo.GetEmaillAdd(emailHistory.numEmailHstrID,3) as BCCEmail,                                     
emailHistory.numEmailHstrID,                            
isnull(vcSubject,'''') as vcSubject,                                      
convert(varchar(max),vcBody) as vcBody,                              
dbo.FormatedDateFromDate( DATEADD(minute,'+ CONVERT(VARCHAR(15),-@ClientTimeZoneOffset)+',bintCreatedOn),numDomainId) as bintCreatedOn,                              
isnull(vcItemId,'''') as ItemId,                              
isnull(vcChangeKey,'''') as ChangeKey,                              
isnull(bitIsRead,''False'') as IsRead,                  
isnull(vcSize,0) as vcSize,                              
isnull(bitHasAttachments,''False'') as HasAttachments,                             
  isnull(EmailHstrAttchDtls.vcFileName,'''') as AttachmentPath,                            
isnull(EmailHstrAttchDtls.vcAttachmentType,'''') as AttachmentType,                      
isnull(EmailHstrAttchDtls.vcAttachmentItemId,'''') as AttachmentItemId,                             
dbo.FormatedDateFromDate(dtReceivedOn,numDomainId) as dtReceivedOn,    
dbo.GetEmaillName(emailHistory.numEmailHstrID,1) as ToName,                          
isnull(emailHistory.tintType,1) as type,                              
isnull(chrSource,''B'') as chrSource,                          
isnull(vcCategory,''white'') as vcCategory                   
 from tblSubscriber T                  
 join emailHistory                   
 on emailHistory.numEmailHstrID=T.numEmailHstrID             
left join EmailHstrAttchDtls on emailHistory.numEmailHstrID=EmailHstrAttchDtls.numEmailHstrID             
where numDomainId='+convert(varchar(15),@numDomainId)+' and numUserCntId='+convert(varchar(15),@numuserCntId)+  
' and numNodeId='+convert(varchar(15),@numNodeId)+' and                
  RowNumber > '+convert(varchar(10),@firstRec)+' and RowNumber < '+convert(varchar(10),@lastRec)+'              
union                 
 select 0 as RowNumber,null,count(*),null,null,            
null,null,null,null,null,null,null,null,null,NULL,           
null,1,null,null from tblSubscriber  order by RowNumber'            
--print    @strSql             
exec (@strSql)
GO
*/               

--SELECT * FROM Category
--- Alterd By Chintan Prajapati
-- EXEC dbo.USP_ItemsForECommerce 9,60,1,1,15,0,''
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ItemsForECommerce' ) 
    DROP PROCEDURE USP_ItemsForECommerce
GO
CREATE PROCEDURE [dbo].[USP_ItemsForECommerce]
    @numSiteID AS NUMERIC(9) = 0,
    @numCategoryID AS NUMERIC(18) = 0,
    @numWareHouseID AS NUMERIC(9) = 0,
    @SortBy NVARCHAR(MAX) = 0,
    @CurrentPage INT,
    @PageSize INT,
    @TotRecs INT = 0 OUTPUT,
    @SearchText AS NVARCHAR(MAX) = '',--Comma seperated item ids
    @FilterQuery AS NVARCHAR(MAX) = '',----Comma seperated Attributes Ids
    @FilterRegularCondition AS NVARCHAR(MAX),
    @FilterCustomFields AS NVARCHAR(MAX),
    @FilterCustomWhere AS NVARCHAR(MAX),
	@numDivisionID AS NUMERIC(18,0) = 0,
	@numManufacturerID AS NUMERIC(18,0)=0,
	@FilterItemAttributes AS NVARCHAR(MAX)
   
AS 
    BEGIN

		
		
		DECLARE @numDomainID AS NUMERIC(9)
		DECLARE @numDefaultRelationship AS NUMERIC(18,0)
		DECLARE @numDefaultProfile AS NUMERIC(18,0)
		DECLARE @tintPreLoginPriceLevel AS TINYINT
		DECLARE @tintPriceLevel AS TINYINT

		SELECT  @numDomainID = numDomainID
        FROM    [Sites]
        WHERE   numSiteID = @numSiteID
        
        DECLARE @tintDisplayCategory AS TINYINT
		
		IF ISNULL(@numDivisionID,0) > 0
		BEGIN
			SELECT @numDefaultRelationship=ISNULL(tintCRMType,0),@numDefaultProfile=ISNULL(vcProfile,0),@tintPriceLevel=ISNULL(tintPriceLevel,0) FROM DivisionMaster INNER JOIN CompanyInfo ON DivisionMaster.numCompanyID=CompanyInfo.numCompanyId WHERE numDivisionID=@numDivisionID AND DivisionMaster.numDomainID=@numDomainID
		END

		SELECT 
			@tintDisplayCategory = ISNULL(bitDisplayCategory,0)
			,@numDefaultRelationship=numRelationshipId
			,@numDefaultProfile=numProfileId
			,@tintPreLoginPriceLevel=(CASE WHEN @numDivisionID > 0 THEN (CASE WHEN ISNULL(bitShowPriceUsingPriceLevel,0)=1 THEN ISNULL(@tintPriceLevel,0) ELSE 0 END) ELSE ISNULL(tintPreLoginProceLevel,0) END)
		FROM 
			dbo.eCommerceDTL 
		WHERE 
			numSiteID = @numSiteId
		
		CREATE TABLE #tmpItemCat(numCategoryID NUMERIC(18,0))
		CREATE TABLE #tmpItemCode(numItemCode NUMERIC(18,0))
		IF LEN(@FilterCustomFields) > 0 AND LEN(@FilterCustomWhere) > 0
		BEGIN			
			DECLARE @FilterCustomFieldIds AS NVARCHAR(MAX)
			SET @FilterCustomFieldIds = Replace(@FilterCustomFields,'[','')
			SET @FilterCustomFieldIds = Replace(@FilterCustomFieldIds,']','')
			
			DECLARE @strCustomSql AS NVARCHAR(MAX)
			SET @strCustomSql = 'INSERT INTO #tmpItemCode(numItemCode)
								 SELECT DISTINCT T.RecId FROM 
															(
																SELECT * FROM 
																	(
																		SELECT  t2.RecId, t1.fld_id, t2.Fld_Value FROM CFW_Fld_Master t1 
																		JOIN CFW_FLD_Values_Item AS t2 ON t1.Fld_ID = t2.Fld_ID 
																		WHERE t1.Grp_id  In (5,9)
																		AND t1.numDomainID = ' + CONVERT(VARCHAR(10),@numDomainID) + '
																		AND t1.fld_type <> ''Link'' 
																		AND t1.fld_id IN (' + @FilterCustomFieldIds + ')
																	) p  PIVOT (MAX([Fld_Value]) FOR fld_id IN (' + @FilterCustomFields + ')) AS pvt 
															) T  WHERE 1=1 AND ' + @FilterCustomWhere
								 
			PRINT @strCustomSql
			EXEC SP_EXECUTESQL @strCustomSql								 					 
		END
		
        DECLARE @strSQL NVARCHAR(MAX)
        DECLARE @firstRec AS INTEGER
        DECLARE @lastRec AS INTEGER
        DECLARE @Where NVARCHAR(MAX)
        DECLARE @SortString NVARCHAR(MAX)
        DECLARE @searchPositionColumn NVARCHAR(MAX)
        DECLARE @row AS INTEGER
        DECLARE @data AS VARCHAR(100)
        DECLARE @dynamicFilterQuery NVARCHAR(MAX)
        DECLARE @checkFldType VARCHAR(100)
        DECLARE @count NUMERIC(9,0)
        DECLARE @whereNumItemCode NVARCHAR(MAX)
        DECLARE @filterByNumItemCode VARCHAR(100)
        
		SET @SortString = ''
        set @searchPositionColumn = ''
        SET @strSQL = ''
		SET @whereNumItemCode = ''
		SET @dynamicFilterQuery = ''
		 
        SET @firstRec = ( @CurrentPage - 1 ) * @PageSize
        SET @lastRec = ( @CurrentPage * @PageSize + 1 )
                
        DECLARE @bitAutoSelectWarehouse AS BIT
		SELECT @bitAutoSelectWarehouse = ISNULL([ECD].[bitAutoSelectWarehouse],0) FROM [dbo].[eCommerceDTL] AS ECD WHERE [ECD].[numSiteId] = @numSiteID
		AND [ECD].[numDomainId] = @numDomainID

		DECLARE @vcWarehouseIDs AS VARCHAR(1000)
        IF (@numWareHouseID = 0 OR @bitAutoSelectWarehouse = 1)
            BEGIN				
				IF @bitAutoSelectWarehouse = 1
					BEGIN
						SELECT @vcWarehouseIDs = ISNULL( STUFF((SELECT DISTINCT ',' + CAST(numWarehouseID AS VARCHAR(10)) FROM [dbo].[Warehouses] WHERE [Warehouses].[numDomainID] = @numDomainID FOR XML PATH('')),1, 1, '') , '') 
					END
				
				ELSE
					BEGIN
						SELECT  @numWareHouseID = ISNULL(numDefaultWareHouseID,0)
						FROM    [eCommerceDTL]
						WHERE   [numDomainID] = @numDomainID	

						SET @vcWarehouseIDs = @numWareHouseID
					END
            END
		ELSE
		BEGIN
			SET @vcWarehouseIDs = @numWareHouseID
		END

		PRINT @vcWarehouseIDs

		--PRIAMRY SORTING USING CART DROPDOWN
		DECLARE @Join AS NVARCHAR(MAX)
		DECLARE @SortFields AS NVARCHAR(MAX)
		DECLARE @vcSort AS VARCHAR(MAX)

		SET @Join = ''
		SET @SortFields = ''
		SET @vcSort = ''

		IF @SortBy = ''
			BEGIN
				SET @SortString = ' vcItemName Asc '
			END
		ELSE IF CHARINDEX('~0',@SortBy) > 0 OR CHARINDEX('~1',@SortBy) > 0
			BEGIN
				DECLARE @fldID AS NUMERIC(18,0)
				DECLARE @RowNumber AS VARCHAR(MAX)
				
				SELECT * INTO #fldValues FROM dbo.SplitIDsWithPosition(@SortBy,'~')
				SELECT @RowNumber = RowNumber, @fldID = id FROM #fldValues WHERE RowNumber = 1
				SELECT @vcSort = CASE WHEN Id = 0 THEN 'ASC' ELSE 'DESC' END FROM #fldValues WHERE RowNumber = 2
				
				SET @Join = @Join + ' LEFT JOIN CFW_FLD_Values_Item CFVI' + @RowNumber + ' ON I.numItemCode = CFVI' + @RowNumber + '.RecID AND CFVI' + @RowNumber + '.fld_Id = ' + CONVERT(VARCHAR(10),@fldID)
				---SET @SortString = 'CFVI' + @RowNumber + '.fld_Value' 
				SET @SortString = '[fld_Value' + @RowNumber + '] ' --+ @vcSort
				SET @SortFields = ', CFVI' + @RowNumber + '.fld_Value [fld_Value' + @RowNumber + ']'
			END
		ELSE
			BEGIN
				--SET @SortString = @SortBy	
				
				IF CHARINDEX('monListPrice',@SortBy) > 0
				BEGIN
					DECLARE @bitSortPriceMode AS BIT
					SELECT @bitSortPriceMode = ISNULL(bitSortPriceMode,0) FROM dbo.eCommerceDTL WHERE numSiteId = @numSiteID
					PRINT @bitSortPriceMode
					IF @bitSortPriceMode = 1
					BEGIN
						SET @SortString = ' '
					END
					ELSE
					BEGIN
						SET @SortString = @SortBy	
					END
				END
				ELSE
				BEGIN
					SET @SortString = @SortBy	
				END
				
			END	

		--SECONDARY SORTING USING DEFAULT SETTINGS FROM ECOMMERCE
		SELECT ROW_NUMBER() OVER (ORDER BY Custom,vcFieldName) AS [RowID],* INTO #tempSort FROM (
		SELECT  CONVERT(VARCHAR(9),numFieldID)+'~0' AS numFieldID,
				ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName,
				0 AS Custom,
				vcDbColumnName 
		FROM View_DynamicColumns
		WHERE numFormID = 84 
		AND ISNULL(bitSettingField,0) = 1 
		AND ISNULL(bitDeleted,0)=0 
		AND ISNULL(bitCustom,0) = 0
		AND numDomainID = @numDomainID
					       
		UNION     

		SELECT  CONVERT(VARCHAR(9),numFieldID)+'~1' AS numFieldID ,
				vcFieldName ,
				1 AS Custom,
				'' AS vcDbColumnName
		FROM View_DynamicCustomColumns          
		WHERE numFormID = 84
		AND numDomainID = @numDomainID
		AND grp_id IN (5,9)
		AND vcAssociatedControlType = 'TextBox' 
		AND ISNULL(bitCustom,0) = 1 
		AND tintPageType=1  
		) TABLE1 ORDER BY Custom,vcFieldName
		
		--SELECT * FROM #tempSort
		DECLARE @DefaultSort AS NVARCHAR(MAX)
		DECLARE @DefaultSortFields AS NVARCHAR(MAX)
		DECLARE @DefaultSortJoin AS NVARCHAR(MAX)
		DECLARE @intCnt AS INT
		DECLARE @intTotalCnt AS INT	
		DECLARE @strColumnName AS VARCHAR(100)
		DECLARE @bitCustom AS BIT
		DECLARE @FieldID AS VARCHAR(100)				

		SET @DefaultSort = ''
		SET @DefaultSortFields = ''
		SET @DefaultSortJoin = ''		
		SET @intCnt = 0
		SET @intTotalCnt = ( SELECT COUNT(*) FROM #tempSort )
		SET @strColumnName = ''
		
		IF (SELECT ISNULL(bitEnableSecSorting,0) FROM EcommerceDtl WHERE numSiteID = @numSiteID ) = 1
		BEGIN
			CREATE TABLE #fldDefaultValues(RowNumber INT,Id VARCHAR(1000))

			WHILE(@intCnt < @intTotalCnt)
			BEGIN
				SET @intCnt = @intCnt + 1
				SELECT @FieldID = numFieldID, @strColumnName = vcDbColumnName, @bitCustom = Custom FROM #tempSort WHERE RowID = @intCnt
				
				--PRINT @FieldID
				--PRINT @strColumnName
				--PRINT @bitCustom

				IF @bitCustom = 0
					BEGIN
						SET @DefaultSort =  @strColumnName + ',' + @DefaultSort 
					END

				ELSE
					BEGIN
						DECLARE @fldID1 AS NUMERIC(18,0)
						DECLARE @RowNumber1 AS VARCHAR(MAX)
						DECLARE @vcSort1 AS VARCHAR(10)
						DECLARE @vcSortBy AS VARCHAR(1000)
						SET @vcSortBy = CONVERT(VARCHAR(100),@FieldID) + '~' + '0'
						--PRINT @vcSortBy

						INSERT INTO #fldDefaultValues(RowNumber,Id) SELECT RowNumber, id FROM dbo.SplitIDsWithPosition(@vcSortBy,'~')
						SELECT @RowNumber1 = RowNumber + 100 + @intCnt, @fldID1 = id FROM #fldDefaultValues WHERE RowNumber = 1
						SELECT @vcSort1 = 'ASC' 
						
						SET @DefaultSortJoin = @DefaultSortJoin + ' LEFT JOIN CFW_FLD_Values_Item CFVI' + @RowNumber1 + ' ON I.numItemCode = CFVI' + @RowNumber1 + '.RecID AND CFVI' + @RowNumber1 + '.fld_Id = ' + CONVERT(VARCHAR(10),@fldID1)
						SET @DefaultSort = '[fld_Value' + @RowNumber1 + '] ' + ',' + @DefaultSort 
						SET @DefaultSortFields = @DefaultSortFields + ', CFVI' + @RowNumber1 + '.fld_Value [fld_Value' + @RowNumber1 + ']'
					END

			END
			IF LEN(@DefaultSort) > 0
			BEGIN
				SET  @DefaultSort = LEFT(@DefaultSort,LEN(@DefaultSort) - 1)
				SET @SortString = @SortString + ',' 
			END
			--PRINT @SortString
			--PRINT @DefaultSort
			--PRINT @DefaultSortFields
		END
--        IF @SortBy = 1 
--            SET @SortString = ' vcItemName Asc '
--        ELSE IF @SortBy = 2 
--            SET @SortString = ' vcItemName Desc '
--        ELSE IF @SortBy = 3 
--            SET @SortString = ' monListPrice Asc '
--        ELSE IF @SortBy = 4 
--            SET @SortString = ' monListPrice Desc '
--        ELSE IF @SortBy = 5 
--            SET @SortString = ' bintCreatedDate Desc '
--        ELSE IF @SortBy = 6 
--            SET @SortString = ' bintCreatedDate ASC '	
--        ELSE IF @SortBy = 7 --sort by relevance, invokes only when search
--            BEGIN
--                 IF LEN(@SearchText) > 0 
--		            BEGIN
--						SET @SortString = ' SearchOrder ASC ';
--					    set @searchPositionColumn = ' ,SearchItems.RowNumber AS SearchOrder ' 						
--					END
--			END
--        ELSE 
--            SET @SortString = ' vcItemName Asc '
		
                                		
        IF LEN(@SearchText) > 0
			BEGIN
				PRINT 1
				IF CHARINDEX('SearchOrder',@SortString) > 0
				BEGIN
					SET @searchPositionColumn = ' ,SearchItems.RowNumber AS SearchOrder ' 
				END

				SET @Where = ' INNER JOIN dbo.SplitIDsWithPosition('''+ @SearchText +''','','') AS SearchItems 
                        ON SearchItems.Id = I.numItemCode  and SC.numSiteID = ' + CONVERT(VARCHAR(15), @numSiteID) + ' WHERE ISNULL(IsArchieve,0) = 0'
				END

		ELSE IF @SearchText = ''

			SET @Where = ' WHERE 1=2 '
			+ ' and SC.numSiteID = ' + CONVERT(VARCHAR(15), @numSiteID)
			+ ' AND ISNULL(IsArchieve,0) = 0'
		ELSE
			BEGIN
				PRINT 2
				SET @Where = ' WHERE 1=1 '
							+ ' and SC.numSiteID = ' + CONVERT(VARCHAR(15), @numSiteID)
							+ ' AND ISNULL(IsArchieve,0) = 0'


				PRINT @tintDisplayCategory							
				PRINT @Where

				IF @tintDisplayCategory = 1
				BEGIN
					
					;WITH CTE AS(
					SELECT C.numCategoryID,C.tintLevel,C.numDepCategory,CASE WHEN tintLevel=1 THEN 1 ELSE 0 END AS bitChild 
						FROM Category C WHERE (numCategoryID = @numCategoryID OR ISNULL(@numCategoryID,0) = 0) AND numDomainID = @numDomainID
					UNION ALL
					SELECT  C.numCategoryID,C.tintLevel,C.numDepCategory,CTE.bitChild FROM Category C JOIN CTE ON 
					C.numDepCategory=CTE.numCategoryID AND CTE.bitChild=1 AND numDomainID = @numDomainID)
					
					INSERT INTO #tmpItemCat(numCategoryID)												 
					SELECT DISTINCT C.numCategoryID FROM CTE C JOIN dbo.ItemCategory IC ON C.numCategoryID = IC.numCategoryID 
														WHERE numItemID IN ( 
																						
													 						SELECT numItemID FROM WareHouseItems WI
																			INNER JOIN dbo.Item I ON I.numItemCode = WI.numItemID AND I.numDomainID = @numDomainID
																			GROUP BY numItemID 
																			HAVING SUM(WI.numOnHand) > 0 
																			UNION ALL
													 						SELECT numItemID FROM WareHouseItems WI
																			INNER JOIN dbo.Item I ON I.numItemCode = WI.numItemID AND I.numDomainID = @numDomainID
																			GROUP BY numItemID 
																			HAVING SUM(WI.numAllocation) > 0
																		 )		
					SELECT numItemID INTO #tmpOnHandItems FROM 
					(													 					
						SELECT numItemID FROM WareHouseItems WI 
						INNER JOIN dbo.Item I ON I.numItemCode = WI.numItemID AND I.numDomainID = @numDomainID
						GROUP BY numItemID 
						HAVING SUM(WI.numOnHand) > 0
						UNION ALL
						SELECT numItemID FROM WareHouseItems WI 
						INNER JOIN dbo.Item I ON I.numItemCode = WI.numItemID AND I.numDomainID = @numDomainID
						GROUP BY numItemID 
						HAVING SUM(WI.numAllocation) > 0				
					) TABLE1
				END

			END

		       SET @strSQL = @strSQL + ' WITH Items AS 
	                                     ( 
	                                     SELECT   
                                               I.numItemCode,
											   I.bitMatrix,
											   I.numItemGroup,
                                               ROW_NUMBER() OVER ( Partition by I.numItemCode Order by I.numItemCode) RowID,
                                               vcItemName,
                                               I.bintCreatedDate,
                                               vcManufacturer,
                                               C.vcCategoryName,
                                               ISNULL(txtItemDesc,'''') txtItemDesc,' + 
											   (CASE WHEN @tintPreLoginPriceLevel > 0 THEN 'ISNULL(PricingOption.monPrice,ISNULL(CASE WHEN I.[charItemType] = ''P'' THEN ( UOM * ISNULL(W1.[monWListPrice],0)) ELSE (UOM * monListPrice) END, 0))' ELSE 'ISNULL(CASE WHEN I.[charItemType] = ''P'' THEN ( UOM * ISNULL(W1.[monWListPrice],0)) ELSE (UOM * monListPrice) END, 0)' END)
											   +' AS monListPrice,
											   ISNULL(CASE WHEN I.[charItemType] = ''P'' THEN ( UOM * ISNULL(W1.[monWListPrice],0)) ELSE (UOM * monListPrice) END, 0) monMSRP,
                                               vcSKU,
                                               fltHeight,
                                               fltWidth,
                                               fltLength,
                                               IM.vcPathForImage,
                                               IM.vcPathForTImage,
                                               vcModelID,
                                               fltWeight,
                                               bitFreeShipping,
                                               UOM AS UOMConversionFactor,
											   UOMPurchase AS UOMPurchaseConversionFactor,
											   ( CASE WHEN charItemType <> ''S'' AND charItemType <> ''N''
													   THEN (CASE 
																WHEN bitAllowBackOrder = 1 THEN 1
																WHEN (ISNULL(W.numOnHand,0)<=0) THEN 0
																ELSE 1
															END)
													   ELSE 1
												  END ) AS bitInStock,
												( CASE WHEN charItemType <> ''S'' AND charItemType <> ''N''
													   THEN ( CASE WHEN bitShowInStock = 1
																   THEN (CASE 
																			WHEN bitAllowBackOrder = 1 AND I.numDomainID <> 172 AND ISNULL(W.numOnHand,0)<=0 AND ISNULL(I.numVendorID,0) > 0 AND dbo.GetVendorPreferredMethodLeadTime(I.numVendorID) > 0 THEN CONCAT(''<font class="vendorLeadTime" style="color:green">Usually ships in '',dbo.GetVendorPreferredMethodLeadTime(I.numVendorID),'' days</font>'')
																			WHEN bitAllowBackOrder = 1 AND ISNULL(W.numOnHand,0)<=0 THEN (CASE WHEN I.numDomainID = 172 THEN ''<font style="color:green">AVAILABLE</font>'' ELSE ''<font style="color:green">In Stock</font>'' END)
																			WHEN (ISNULL(W.numOnHand,0)<=0) THEN (CASE WHEN I.numDomainID = 172 THEN ''<font style="color:red">ON HOLD</font>'' ELSE ''<font style="color:red">Out Of Stock</font>'' END)
																			ELSE (CASE WHEN I.numDomainID = 172 THEN ''<font style="color:green">AVAILABLE</font>'' ELSE ''<font style="color:green">In Stock</font>'' END)  
																		END)
																   ELSE ''''
															  END )
													   ELSE ''''
												  END ) AS InStock, C.vcDescription as CategoryDesc, ISNULL(W1.numWareHouseItemID,0) [numWareHouseItemID] ' 
								               +	@searchPositionColumn + @SortFields + @DefaultSortFields + ',
											   (SELECT COUNT(numProId) 
													FROM 
														PromotionOffer PO
													WHERE 
														numDomainId='+CAST(@numDomainID AS VARCHAR(20))+' 
														AND ISNULL(bitEnabled,0)=1
														AND ISNULL(bitAppliesToSite,0)=1 
														AND ISNULL(bitRequireCouponCode,0)=0
														AND 1 = (CASE WHEN bitRequireCouponCode = 1 THEN CASE WHEN ISNULL(intCouponCodeUsed,0) < ISNULL(tintUsageLimit,0) OR ISNULL(tintUsageLimit,0) = 0  THEN 1 ELSE 0 END ELSE 1 END)
														AND 1 = (CASE WHEN bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN CAST(GETDATE() AS DATE)>=dtValidFrom AND CAST(GETDATE() AS DATE)<=dtValidTo THEN 1 ELSE 0 END) END)
														AND (1 = (CASE 
																	WHEN tintOfferBasedOn = 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=1 AND numValue IN (SELECT numOppAccAttrID FROM ItemGroupsDTL WHERE numItemGroupID IN (SELECT numItemGroupID FROM ItemGroups WHERE numProfileItem=I.numItemCode AND numDomainID='+CAST(@numDomainID AS VARCHAR(20))+' ) UNION SELECT I.numItemCode)) > 0 THEN 1 ELSE 0 END)
																	WHEN tintOfferBasedOn = 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=2 AND numValue=I.numItemClassification) > 0 THEN 1 ELSE 0 END)
																	ELSE 0
																END)
															OR
															1 = (CASE 
																	WHEN tintDiscoutBaseOn = 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=6 AND tintType=1 AND numValue IN (SELECT numOppAccAttrID FROM ItemGroupsDTL WHERE numItemGroupID IN (SELECT numItemGroupID FROM ItemGroups WHERE numProfileItem=I.numItemCode AND numDomainID='+CAST(@numDomainID AS VARCHAR(20))+' ) UNION SELECT I.numItemCode)) > 0 THEN 1 ELSE 0 END)
																	WHEN tintDiscoutBaseOn = 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=6 AND tintType=2 AND numValue=I.numItemClassification) > 0 THEN 1 ELSE 0 END)
																	WHEN tintDiscoutBaseOn = 3 AND tintOfferBasedOn = 1 THEN (CASE WHEN (SELECT COUNT(SimilarItems.numItemCode) FROM SimilarItems WHERE numParentItemCode IN (SELECT numValue FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=1 AND SimilarItems.numItemCode=I.numItemCode))>0 THEN 1 ELSE 0 END)
																	WHEN tintDiscoutBaseOn = 3 AND tintOfferBasedOn = 2 THEN (CASE WHEN (SELECT COUNT(SimilarItems.numItemCode) FROM SimilarItems WHERE SimilarItems.numItemCode IN(I.numItemCode) AND numParentItemCode IN (SELECT numItemCode FROM Item WHERE numItemClassification IN (SELECT numValue FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=2)))>0 THEN 1 ELSE 0 END)
																	ELSE 0
																END)
															)
													)  IsOnSale
								
                                         FROM      
                                                      Item AS I
                                         INNER JOIN   ItemCategory IC   ON   I.numItemCode = IC.numItemID
                                         LEFT JOIN    ItemImages IM     ON   I.numItemCode = IM.numItemCode AND IM.bitdefault=1 AND IM.bitIsImage = 1
                                         INNER JOIN   Category C        ON   IC.numCategoryID = C.numCategoryID
                                         INNER JOIN   SiteCategories SC ON   IC.numCategoryID = SC.numCategoryID ' + 
										 (CASE WHEN @tintPreLoginPriceLevel > 0 THEN ' OUTER APPLY(SELECT decDiscount AS monPrice FROM PricingTable WHERE tintRuleType=3 AND numItemCode=I.numItemCode ORDER BY numPricingID OFFSET ' + CAST(@tintPreLoginPriceLevel-1 AS VARCHAR) + ' ROWS FETCH NEXT 1 ROWS ONLY) As PricingOption ' ELSE '' END)
										 + ' INNER JOIN   eCommerceDTL E    ON   E.numDomainID = I.numDomainID AND E.numSiteId = SC.numSiteID ' + @Join + ' ' + @DefaultSortJoin + ' 
                                         OUTER APPLY (SELECT SUM(numOnHand) numOnHand FROM WareHouseItems W 
													  WHERE  W.numItemID = I.numItemCode
                                                      AND numWareHouseID IN (SELECT [OutParam] FROM dbo.[SplitString](''' + @vcWarehouseIDs + ''','',''))
                                                      AND W.numDomainID = ' + CONVERT(VARCHAR(10),@numDomainID) + ') AS W  
										 OUTER APPLY (SELECT TOP 1 numWareHouseItemID,monWListPrice FROM WareHouseItems W 
													  WHERE  W.numItemID = I.numItemCode 
													  AND W.numDomainID = ' + CONVERT(VARCHAR(10),@numDomainID) + '
													  AND numWareHouseID IN (SELECT [OutParam] FROM dbo.[SplitString](''' + @vcWarehouseIDs + ''','',''))) AS W1
										 OUTER APPLY (SELECT SUM(numAllocation) numAllocation FROM WareHouseItems W2 
													  WHERE  W2.numItemID = I.numItemCode
                                                      AND numWareHouseID IN (SELECT [OutParam] FROM dbo.[SplitString](''' + @vcWarehouseIDs + ''','',''))
                                                      AND W2.numDomainID = ' + CONVERT(VARCHAR(10),@numDomainID) + ') AS W2  			  
										 CROSS APPLY (SELECT dbo.fn_UOMConversion(ISNULL(I.numSaleUnit, 0),I.numItemCode, I.numDomainId,ISNULL(I.numBaseUnit, 0)) AS UOM) AS UOM
										 CROSS APPLY (SELECT dbo.fn_UOMConversion(ISNULL(I.numPurchaseUnit, 0),I.numItemCode, I.numDomainId,ISNULL(I.numBaseUnit, 0)) AS UOMPurchase) AS UOMPurchase '

			IF LEN(@FilterCustomFields) > 0 AND LEN(@FilterCustomWhere) > 0
			BEGIN
				SET @strSQL = @strSQL + ' INNER JOIN #tmpItemCode TP ON TP.numItemCode = I.numItemCode ' 
			END
			
			IF (SELECT COUNT(*) FROM #tmpItemCat) > 0
			BEGIN
				SET @strSQL = @strSQL + ' INNER JOIN #tmpItemCat TC ON TC.numCategoryID = IC.numCategoryID '
				SET @strSQL = @strSQL + ' LEFT JOIN #tmpOnHandItems TOIC ON TOIC.numItemID = I.numItemCode '
			END
			ELSE IF @numManufacturerID>0
			BEGIN
				SET @Where = REPLACE(@Where,'WHERE',' WHERE I.numManufacturer = ' + CONVERT(VARCHAR(10),@numManufacturerID) + ' AND ')
			END 
			ELSE IF @numCategoryID>0
			BEGIN
				SET @Where = REPLACE(@Where,'WHERE',' WHERE IC.numCategoryID = ' + CONVERT(VARCHAR(10),@numCategoryID) + ' AND ')
			END 
			
			IF @tintDisplayCategory = 1
			BEGIN
				SET @Where = @Where + ' AND 1 = (CASE WHEN (I.charItemType=''P'' AND ISNULL(I.bitKitParent,0) = 0) THEN CASE WHEN (ISNULL(W.numOnHand, 0) > 0 OR ISNULL(W2.numAllocation, 0) > 0) THEN 1 ELSE 0 END ELSE 1 END)'
			END
			
			IF (SELECT COUNT(*) FROM [dbo].[EcommerceRelationshipProfile] WHERE numSiteID=@numSiteID AND numRelationship = @numDefaultRelationship AND numProfile = @numDefaultProfile) > 0
			BEGIN
				SET @Where = @Where + CONCAT(' AND I.numItemClassification NOT IN (SELECT numItemClassification FROM EcommerceRelationshipProfile ERP INNER JOIN ECommerceItemClassification EIC ON EIC.numECommRelatiionshipProfileID=ERP.ID WHERE ERP.numSiteID=',@numSiteID,' AND ERP.numRelationship=',@numDefaultRelationship,' AND ERP.numProfile=', @numDefaultProfile,')')
			END

			IF LEN(ISNULL(@FilterItemAttributes,'')) > 0
			BEGIN
				SET @FilterItemAttributes = REPLACE(@FilterItemAttributes,') OR (',')) AND I.numItemCode IN (SELECT numItemCode FROM ItemAttributes WHERE numDomainID = ' + CAST(@numDomainId AS VARCHAR(18)) +' AND (')
				SET @Where = @Where + 'AND I.numItemCode IN (SELECT numItemCode FROM ItemAttributes WHERE numDomainID = ' + CAST(@numDomainId AS VARCHAR(18)) +' AND '+@FilterItemAttributes+')'
			END
			--PRINT @Where
			SET @strSQL = @strSQL + @Where
			PRINT @strSQL
			IF LEN(@FilterRegularCondition) > 0
			BEGIN
				SET @strSQL = @strSQL + @FilterRegularCondition
			END                                         
			
---PRINT @SortString + ',' + @DefaultSort + ' ' + @vcSort 
	
            IF LEN(@whereNumItemCode) > 0
                                        BEGIN
								              SET @strSQL = @strSQL + @filterByNumItemCode	
								    	END
            SET @strSQL = @strSQL +     '   ) SELECT * INTO #TEMPItems FROM Items
                                        
									DELETE FROM 
										#TEMPItems
									WHERE 
										numItemCode IN (
															SELECT 
																F.numItemCode
															FROM 
																#TEMPItems AS F
															WHERE 
																ISNULL(F.bitMatrix,0) = 1 
																AND EXISTS (
																			SELECT 
																				t1.vcItemName, t1.bitMatrix, t1.numItemGroup, Count(t1.numItemCode)
																			FROM 
																				#TEMPItems t1
																			WHERE 
																				t1.vcItemName = F.vcItemName
																				AND t1.bitMatrix = 1
																				AND t1.numItemGroup = F.numItemGroup
																			GROUP BY 
																				t1.vcItemName, t1.bitMatrix, t1.numItemGroup
																			HAVING 
																				Count(t1.numItemCode) > 1
																		)
														)
										AND numItemCode NOT IN (
																				SELECT 
																					Min(numItemCode)
																				FROM 
																					#TEMPItems AS F
																				WHERE
																					ISNULL(F.bitMatrix,0) = 1 
																					AND Exists (
																								SELECT 
																									t2.vcItemName, t2.bitMatrix, t2.numItemGroup, Count(t2.numItemCode)
																								FROM 
																									#TEMPItems t2
																								WHERE 
																									t2.vcItemName = F.vcItemName
																								   AND t2.bitMatrix = 1
																								   AND t2.numItemGroup = F.numItemGroup
																								GROUP BY 
																									t2.vcItemName, t2.bitMatrix, t2.numItemGroup
																								HAVING 
																									Count(t2.numItemCode) > 1
																							)
																				GROUP BY 
																					vcItemName, bitMatrix, numItemGroup
																			);  WITH ItemSorted AS (SELECT  ROW_NUMBER() OVER ( ORDER BY  ' + @SortString + @DefaultSort + ' ' + @vcSort + ' ) AS Rownumber,* FROM  #TEMPItems WHERE RowID = 1)'



            DECLARE @fldList AS NVARCHAR(MAX)
			DECLARE @fldList1 AS NVARCHAR(MAX)
			SELECT @fldList=COALESCE(@fldList + ',','') + QUOTENAME(REPLACE(Fld_label,' ','') + '_C') FROM dbo.CFW_Fld_Master WHERE numDomainID=@numDomainID AND Grp_id=5
			SELECT @fldList1=COALESCE(@fldList1 + ',','') + QUOTENAME(REPLACE(Fld_label,' ','') + '_' + CAST(Fld_Id AS VARCHAR)) FROM dbo.CFW_Fld_Master WHERE numDomainID=@numDomainID AND Grp_id=9
            
            SET @strSQL = @strSQL +' SELECT  Rownumber,numItemCode,bitMatrix,numItemGroup,txtItemDesc,vcManufacturer,monListPrice,monMSRP,vcItemName,vcCategoryName,vcPathForImage,
				vcpathForTImage,vcSKU,fltHeight,fltWidth,fltLength,vcModelID,fltWeight,bitFreeShipping,UOMConversionFactor,UOMPurchaseConversionFactor,bitInStock,InStock,CategoryDesc,
				numWareHouseItemID,IsOnSale
                                     INTO #tempItem FROM ItemSorted ' 
            
            SET @strSQL = @strSQL + 'SELECT * INTO #tmpPagedItems FROM #tempItem WHERE Rownumber > ' + CONVERT(VARCHAR(10), @firstRec) + '
                                      AND   Rownumber < ' + CONVERT(VARCHAR(10), @lastRec) + '
                                      order by Rownumber '
                                                                  
            IF LEN(ISNULL(@fldList,'')) > 0 OR LEN(ISNULL(@fldList1,'')) > 0
            BEGIN
				
				SET @strSQL = @strSQL +'SELECT  Rownumber,numItemCode,txtItemDesc,vcManufacturer,monListPrice,monMSRP,vcItemName,vcCategoryName,vcPathForImage,vcpathForTImage,vcSKU,fltHeight,fltWidth,fltLength,vcModelID,fltWeight,bitFreeShipping,UOMConversionFactor,UOMPurchaseConversionFactor,bitInStock,InStock,CategoryDesc,numWareHouseItemID,IsOnSale,' + @fldList + (CASE WHEN LEN(ISNULL(@fldList1,'')) > 0 THEN CONCAT(',',@fldList1) ELSE '' END) +'
                                        FROM #tmpPagedItems I'

				IF LEN(ISNULL(@fldList,'')) > 0
				BEGIN
					SET @strSQL = @strSQL +' left join (
														SELECT  t2.RecId, REPLACE(t1.Fld_label,'' '','''') + ''_C'' as Fld_label, 
															CASE t1.fld_type WHEN ''SelectBox'' THEN isnull(dbo.fn_GetListItemName(t2.Fld_Value),'''')
																				WHEN ''CheckBox'' THEN isnull(dbo.fn_getCustomDataValue(t2.Fld_Value,9),'''')
																				WHEN ''DateField'' THEN dbo.FormatedDateFromDate(t2.Fld_Value,' + CAST(@numDomainId AS VARCHAR(18)) +')
																				ELSE Replace(ISNULL(t2.Fld_Value,''0''),''$'',''$$'') end AS Fld_Value FROM CFW_Fld_Master t1 
														JOIN CFW_FLD_Values_Item AS t2 ON t1.Fld_ID = t2.Fld_ID where t1.Grp_id = 5 and t1.numDomainID=' + CAST(@numDomainId AS VARCHAR(18)) +' AND t1.fld_type <> ''Link''
														AND t2.RecId IN (SELECT numItemCode FROM #tmpPagedItems )
														) p PIVOT (MAX([Fld_Value]) FOR Fld_label IN (' + @fldList + ')) AS pvt on I.numItemCode=pvt.RecId' 
				END

				IF LEN(ISNULL(@fldList1,'')) > 0
				BEGIN
					
					SET @strSQL = @strSQL +' left join (
														SELECT  t2.numItemCode AS RecId, REPLACE(t1.Fld_label,'' '','''') + ''_'' + CAST(t1.Fld_Id AS VARCHAR) as Fld_label, 
															CASE t1.fld_type WHEN ''SelectBox'' THEN isnull(dbo.fn_GetListItemName(t2.Fld_Value),'''')
																				WHEN ''CheckBox'' THEN isnull(dbo.fn_getCustomDataValue(t2.Fld_Value,9),'''')
																				WHEN ''DateField'' THEN dbo.FormatedDateFromDate(t2.Fld_Value,' + CAST(@numDomainId AS VARCHAR(18)) +')
																				ELSE Replace(ISNULL(t2.Fld_Value,''0''),''$'',''$$'') end AS Fld_Value FROM CFW_Fld_Master t1 
														JOIN ItemAttributes AS t2 ON t1.Fld_ID = t2.Fld_ID where t1.Grp_id = 9 and t1.numDomainID=' + CAST(@numDomainId AS VARCHAR(18)) +' AND t1.fld_type <> ''Link''
														AND t2.numItemCode IN (SELECT numItemCode FROM #tmpPagedItems )
														) p PIVOT (MAX([Fld_Value]) FOR Fld_label IN (' + @fldList1 + ')) AS pvt1 on I.numItemCode=pvt1.RecId'
				END
				ELSE
				BEGIN
					SET @strSQL = REPLACE(@strSQL, ',##FLDLIST1##', '');
				END

				SET  @strSQL = @strSQL +' order by Rownumber';
			END 
			ELSE
			BEGIN
				PRINT 'flds2'
				SET @strSQL = @strSQL +' SELECT  Rownumber,numItemCode,txtItemDesc,vcManufacturer,monListPrice,monMSRP,vcItemName,vcCategoryName,vcPathForImage,vcpathForTImage,vcSKU,fltHeight,fltWidth,fltLength,vcModelID,fltWeight,bitFreeShipping,UOMConversionFactor,UOMPurchaseConversionFactor,InStock,CategoryDesc,numWareHouseItemID,IsOnSale
                                         FROM  #tmpPagedItems order by Rownumber;'
			END               
                                        
            --print('@strSQL=')
            --PRINT(@strSQL)        
--            EXEC ( @strSQL) ; 
			                           
            SET @strSQL = @strSQL + ' SELECT @TotRecs = COUNT(*) FROM #tempItem '                            

			 PRINT  LEN(@strSQL)
			 DECLARE @tmpSQL AS NVARCHAR(MAX)
			 SET @tmpSQL = @strSQL
			 --PRINT  @tmpSQL
			 PRINT CAST(@tmpSQL AS NTEXT)
             EXEC sp_executeSQL @strSQL, N'@TotRecs INT OUTPUT', @TotRecs OUTPUT
             
             
        DECLARE @tintOrder AS TINYINT                                                  
		DECLARE @vcFormFieldName AS VARCHAR(50)                                                  
		DECLARE @vcListItemType AS VARCHAR(1)                                             
		DECLARE @vcAssociatedControlType VARCHAR(10)                                                  
		DECLARE @numListID AS NUMERIC(9)                                                  
		DECLARE @WhereCondition VARCHAR(MAX)                       
		DECLARE @numFormFieldId AS NUMERIC  
		--DECLARE @vcFieldType CHAR(1)
		DECLARE @vcFieldType VARCHAR(15)
		                  
		SET @tintOrder=0                                                  
		SET @WhereCondition =''                 
                   
              
		CREATE TABLE #tempAvailableFields(numFormFieldId  NUMERIC(9),vcFormFieldName NVARCHAR(50),
		--vcFieldType CHAR(1),
		vcFieldType VARCHAR(15),vcAssociatedControlType NVARCHAR(50),numListID NUMERIC(18),
		vcListItemType CHAR(1),intRowNum INT,vcItemValue VARCHAR(3000))
		   
		INSERT INTO #tempAvailableFields (numFormFieldId,vcFormFieldName,vcFieldType,vcAssociatedControlType
				,numListID,vcListItemType,intRowNum)                         
		SELECT  Fld_id,REPLACE(Fld_label,' ','') AS vcFormFieldName,
			   CASE GRP_ID WHEN 4 then 'C' 
					WHEN 1 THEN 'D'
					--WHEN 9 THEN 'A' 
					WHEN 9 THEN CONVERT(VARCHAR(15), Fld_id)
					ELSE 'C' END AS vcFieldType,
				fld_type as vcAssociatedControlType,
				ISNULL(C.numListID, 0) numListID,
				CASE WHEN C.numListID > 0 THEN 'L'
					 ELSE ''
				END vcListItemType,ROW_NUMBER() OVER (ORDER BY Fld_id ASC) AS intRowNum
		FROM    CFW_Fld_Master C LEFT JOIN CFW_Validation V ON V.numFieldID = C.Fld_id
		WHERE   C.numDomainID = @numDomainId
				AND GRP_ID IN (5,9)

		  
		SELECT * FROM #tempAvailableFields
		
		/*
		DROP TABLE #tempAvailableFields
		DROP TABLE #tmpItemCode
		DROP TABLE #tmpItemCat
		*/	

		IF OBJECT_ID('tempdb..#tempAvailableFields') IS NOT NULL
		DROP TABLE #tempAvailableFields
		
		IF OBJECT_ID('tempdb..#tmpItemCode') IS NOT NULL
		DROP TABLE #tmpItemCode
		
		IF OBJECT_ID('tempdb..#tmpItemCat') IS NOT NULL
		DROP TABLE #tmpItemCat
		
		IF OBJECT_ID('tempdb..#fldValues') IS NOT NULL
		DROP TABLE #fldValues
		
		IF OBJECT_ID('tempdb..#tempSort') IS NOT NULL
		DROP TABLE #tempSort
		
		IF OBJECT_ID('tempdb..#fldDefaultValues') IS NOT NULL
		DROP TABLE #fldDefaultValues
		
		IF OBJECT_ID('tempdb..#tmpOnHandItems') IS NOT NULL
		DROP TABLE #tmpOnHandItems

		IF OBJECT_ID('tempdb..#tmpPagedItems') IS NOT NULL
		DROP TABLE #tmpPagedItems


		IF OBJECT_ID('tempdb..#TEMPItems') IS NOT NULL
		DROP TABLE #TEMPItems
    END


GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ItemUnitPriceApproval_Check' ) 
    DROP PROCEDURE USP_ItemUnitPriceApproval_Check
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Sandeep Patel
-- Create date: 1 July 2014
-- Description:	Checks wheather unit price approval is required or not for item
-- =============================================
CREATE PROCEDURE USP_ItemUnitPriceApproval_Check
	@numDomainID numeric(18,0),
	@numDivisionID numeric(18,0),
	@numItemCode numeric(18,0),
	@numQuantity FLOAT,
	@numUnitPrice float,
	@numTotalAmount float,
	@numCost FLOAT,
	@numWarehouseItemID numeric(18,0)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	DECLARE @numAbovePercent FLOAT
	DECLARE @numAboveField FLOAT
	DECLARE @numBelowPercent FLOAT
	DECLARE @numBelowField FLOAT
	DECLARE @numDefaultCost TINYINT
	DECLARE @bitCostApproval BIT
	DECLARE @bitListPriceApproval BIT

	SELECT
		@numAbovePercent = ISNULL(numAbovePercent,0)
		,@numBelowPercent = ISNULL(numBelowPercent,0)
		,@numBelowField = ISNULL(numBelowPriceField,0)
		,@numAboveField=ISNULL(numCost,1)
		,@bitCostApproval=ISNULL(bitCostApproval,0)
		,@bitListPriceApproval=ISNULL(bitListPriceApproval,0)
	FROM
		Domain
	WHERE
		numDomainId=@numDomainID


	DECLARE @IsApprovalRequired BIT
	DECLARE @ItemAbovePrice FLOAT
	DECLARE @ItemBelowPrice FLOAT

	SET @IsApprovalRequired = 0
	SET @ItemAbovePrice = 0
	SET @ItemBelowPrice = 0

	IF @numQuantity > 0
		SET @numUnitPrice = (@numTotalAmount / @numQuantity)
	ELSE
		SET @numUnitPrice = 0


	IF @numAboveField > 0 AND ISNULL(@bitCostApproval,0) = 1
	BEGIN
		IF @numAboveField = 3 -- Primaty Vendor Cost
			IF (SELECT COUNT(*) FROM Item WHERE numDomainID = @numDomainID AND numItemCode = @numItemCode  AND (bitKitParent = 1 OR bitAssembly = 1)) > 0 -- Kit Or Assembly
				SELECT @ItemAbovePrice = dbo.GetUnitPriceForKitOrAssembly(@numDomainID,@numItemCode,@numWarehouseItemID,@numQuantity)
			ELSE
				SELECT @ItemAbovePrice = ISNULL(monCost,0) FROM Vendor WHERE numItemCode = @numItemCode AND numVendorID = (SELECT numVendorID FROM Item WHERE numItemCode = @numItemCode)
		ELSE IF @numAboveField = 2 -- Product & Sevices Cost
			SELECT @ItemAbovePrice = ISNULL(@numCost,0)
		ELSE IF @numAboveField = 1 -- Average Cost
			SELECT @ItemAbovePrice = (CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(monAverageCost,0) END) FROM Item WHERE numDomainID = @numDomainID AND numItemCode = @numItemCode

		IF @ItemAbovePrice > 0
		BEGIN
			IF @numUnitPrice < (@ItemAbovePrice + (@ItemAbovePrice * (@numAbovePercent/100)))
				SET @IsApprovalRequired = 1
		END
		ELSE IF @ItemAbovePrice = 0
		BEGIN
			SET @IsApprovalRequired = 1
		END
	END

	IF @IsApprovalRequired = 0 AND @numBelowField > 0  AND ISNULL(@bitListPriceApproval,0) = 1
	BEGIN
		IF @numBelowField = 1 -- List Price
		BEGIN
			IF (SELECT ISNULL(charItemType,'') AS charItemType FROM Item WHERE numDomainID = @numDomainID AND numItemCode = @numItemCode) = 'P' -- Inventory Item
				If (SELECT COUNT(*) FROM Item WHERE numDomainID = @numDomainID AND numItemCode = @numItemCode AND (bitKitParent = 1 OR bitAssembly = 1)) > 0 -- Kit OR Assembly
					SELECT @ItemBelowPrice = dbo.GetUnitPriceForKitOrAssembly(@numDomainID,@numItemCode,@numWarehouseItemID,@numQuantity)
				ELSE
					SELECT @ItemBelowPrice = ISNULL(monWListPrice,0) FROM WareHouseItems WHERE numDomainID = @numDomainID AND numItemID = @numItemCode AND numWareHouseItemID = @numWarehouseItemID
			ELSE
				SELECT @ItemBelowPrice = ISNULL(monListPrice,0) FROM Item WHERE numDomainID = @numDomainID AND numItemCode = @numItemCode
		END
		ELSE IF @numBelowField = 2 -- Price Rule
		BEGIN
			/* Check if valid price book rules exists for sales in domain */
			IF (SELECT COUNT(*) FROM PriceBookRules WHERE numDomainId = @numDomainID AND tintRuleFor = 1 AND tintStep2 > 0 AND tintStep3 > 0) > 0
			BEGIN
				DECLARE @i INT = 0
				DECLARE @Count int = 0
				DECLARE @tempPriority INT
				DECLARE @tempNumPriceRuleID NUMERIC(18,0)
				DECLARE @numPriceRuleIDApplied NUMERIC(18,0) = 0
				DECLARE @TempTable TABLE (numID INT, numRuleID numeric(18,0), numPriority INT)

				INSERT INTO 
					@TempTable
				SELECT 
					ROW_NUMBER() OVER (ORDER BY PriceBookPriorities.Priority) AS id,
					numPricRuleID,
					PriceBookPriorities.Priority 
				FROM 
					PriceBookRules
				INNER JOIN
					PriceBookPriorities
				ON
					PriceBookRules.tintStep2 = PriceBookPriorities.Step2Value AND
					PriceBookRules.tintStep3 = PriceBookPriorities.Step3Value
				WHERE 
					PriceBookRules.numDomainId = @numDomainID AND 
					PriceBookRules.tintRuleFor = 1 AND 
					PriceBookRules.tintStep2 > 0 AND PriceBookRules.tintStep3 > 0
				ORDER BY
					PriceBookPriorities.Priority

				SELECT @Count = COUNT(*) FROM @TempTable

				/* Loop all price rule with priority */
				WHILE (@i < @count)
				BEGIN
					SELECT @tempNumPriceRuleID = numRuleID, @tempPriority = numPriority FROM @TempTable WHERE numID = (@i + 1)
					
					/* IF proprity is 9 then price rule is applied to all items and all customers. 
					So price rule must be applied to item or not.*/
					IF @tempPriority = 9
					BEGIN
						SET @numPriceRuleIDApplied = @tempNumPriceRuleID
						BREAK
					END
					/* Check if current item exists in rule. if eixts then exit loop with rule id else continie loop. if item not exist in any rule then nothing to check */
					ELSE
					BEGIN
						DECLARE @isRuleApplicable BIT = 0
						EXEC @isRuleApplicable = dbo.CheckIfPriceRuleApplicableToItem @numRuleID = @tempNumPriceRuleID, @numItemID = @numItemCode, @numDivisionID = @numDivisionID 

						IF @isRuleApplicable = 1
						BEGIN
							SET @numPriceRuleIDApplied = @tempNumPriceRuleID
							BREAK
						END
					END

					SET @i = @i + 1
				END

				/* If @numPriceRuleIDApplied > 0 Get final unit price limit after applying below rule */
				PRINT @numPriceRuleIDApplied
				IF @numPriceRuleIDApplied > 0
				BEGIN
					EXEC @ItemBelowPrice = GetUnitPriceAfterPriceRuleApplication @numRuleID = @numPriceRuleIDApplied, @numDomainID = @numDomainID, @numItemCode = @numItemCode, @numQuantity = @numQuantity, @numWarehouseItemID = @numWarehouseItemID, @numDivisionID = @numDivisionID
				END
			END
		END
		ELSE IF @numBelowField = 3 -- Price Level
		BEGIN
			EXEC @ItemBelowPrice = GetUnitPriceAfterPriceLevelApplication @numDomainID = @numDomainID, @numItemCode = @numItemCode, @numQuantity = @numQuantity, @numWarehouseItemID = @numWarehouseItemID, @isPriceRule = 0, @numPriceRuleID = 0, @numDivisionID = @numDivisionID
		END
		
		IF @numUnitPrice < (@ItemBelowPrice - (@ItemBelowPrice * (@numBelowPercent/100)))
				SET @IsApprovalRequired = 1
	END

	SELECT @IsApprovalRequired
END
GO

GO
/****** Object:  StoredProcedure [dbo].[usp_ManageAddlContInfo]    Script Date: 04/02/2009 00:17:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Modified By Anoop jayaraj                  
 GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_manageaddlcontinfo')
DROP PROCEDURE usp_manageaddlcontinfo
GO
CREATE PROCEDURE [dbo].[usp_ManageAddlContInfo]                                    
 @numcontactId numeric=0,                                    
 @numContactType numeric=0,                                    
 @vcDepartment numeric(9)=0,                                    
 @vcCategory numeric(9)=0,                                    
 @vcGivenName varchar(100)='',                                    
 @vcFirstName varchar(50)='',                                    
 @vcLastName varchar(50)='',                                    
 @numDivisionId numeric,                                    
 @numPhone varchar(15)='',                                    
 @numPhoneExtension varchar(7)='',                                    
 @numCell varchar(15)='',                                    
 @NumHomePhone varchar(15)='',                                    
 @vcFax varchar(15)='',                                    
 @vcEmail varchar(50)='',                                    
 @VcAsstFirstName varchar(50)='',                                    
 @vcAsstLastName varchar(50)='',                                    
 @numAsstPhone varchar(15)='',                                    
 @numAsstExtn varchar(6)='',                                    
 @vcAsstEmail varchar(50)='',                                                      
 @charSex char(1)='',                                    
 @bintDOB datetime,                                    
 @vcPosition numeric(9)=0,                                                     
 @txtNotes text='',                                                     
 @numUserCntID numeric,                                                                                              
 @numDomainID numeric=1,                                                   
 @vcPStreet varchar(100)='',                                    
 @vcPCity varchar(50)='',                                    
 @vcPPostalCode varchar(15)='',                                    
 @vcPState numeric(9)=0,                                    
 @vcPCountry numeric(9)=0,                  
 @numManagerID numeric=0,        
 @numTeam as numeric(9)=0,        
 @numEmpStatus as numeric(9)=0,
 @vcTitle AS VARCHAR(100)='',
 @bitPrimaryContact AS BIT=0,
 @bitOptOut AS BIT=0,
 @vcLinkedinId VARCHAR(30)=NULL,
 @vcLinkedinUrl VARCHAR(300)=NULL,
 @numECampaignID NUMERIC(18)=0,
 @vcPassword VARCHAR(50) = '',
 @vcTaxID VARCHAR(100) = ''
AS   
	If LEN(ISNULL(@vcFirstName,'')) = 0
	BEGIN
		SET @vcFirstName = '-'
	END

	If LEN(ISNULL(@vcLastName,'')) = 0
	BEGIN
		SET @vcLastName = '-'
	END

 SELECT @vcGivenName = @vcLastName + ', ' + @vcFirstName  + '.eml'                                 
   if @bintDOB ='Jan  1 1753 12:00AM' set  @bintDOB=null                      
IF @numContactId=0
     BEGIN   

IF @bitPrimaryContact = 1 
    BEGIN            
        DECLARE @numID AS NUMERIC(9)            
        DECLARE @bitPrimaryContactCheck AS BIT             
             
        SELECT TOP 1
                @numID = numContactID,
                @bitPrimaryContactCheck = ISNULL(bitPrimaryContact,0)
        FROM    AdditionalContactsInformation
        WHERE   numDivisionID = @numDivisionID 
                   
        IF @@rowcount = 0 
            SET @numID = 0             
        WHILE @numID > 0            
            BEGIN            
                IF @bitPrimaryContactCheck = 1 
                    UPDATE  AdditionalContactsInformation
                    SET     bitPrimaryContact = 0
                    WHERE   numContactID = @numID  
                              
                SELECT TOP 1
                        @numID = numContactID,
                        @bitPrimaryContactCheck = ISNULL(bitPrimaryContact,0)
                FROM    AdditionalContactsInformation
                WHERE   numDivisionID = @numDivisionID
                        AND numContactID > @numID    
                                
                IF @@rowcount = 0 
                    SET @numID = 0             
            END            
    END  
    ELSE IF @bitPrimaryContact = 0
    BEGIN
		IF (SELECT COUNT(*) FROM AdditionalContactsInformation WHERE numDivisionID = @numDivisionID AND numContactId!=@numContactId AND ISNULL(bitPrimaryContact,0)=1)=0
			SET @bitPrimaryContact = 1
    END                 
    
    
declare @bitAutoPopulateAddress bit 
declare @tintPoulateAddressTo tinyint 

select @bitAutoPopulateAddress= isnull(bitAutoPopulateAddress,'0'),@tintPoulateAddressTo=isnull(tintPoulateAddressTo,'0') from Domain where numDomainId = @numDomainId ;
if (@bitAutoPopulateAddress = 1)
	if(@tintPoulateAddressTo = 1) --if primary address is not specified then getting billing address    
	begin
		 
		  select                   
			@vcPStreet = AD1.vcStreet,
			@vcPCity= AD1.vcCity,
			@vcPState= AD1.numState,
			@vcPPostalCode= AD1.vcPostalCode,
			@vcPCountry= AD1.numCountry              
		  from divisionMaster  DM              
		   LEFT JOIN dbo.AddressDetails AD1 ON AD1.numDomainID=DM.numDomainID 
			AND AD1.numRecordID=DM.numDivisionID AND AD1.tintAddressOf=2 AND AD1.tintAddressType=1 AND AD1.bitIsPrimary=1
		  where numDivisionID=@numDivisionId    
	end  
	else if (@tintPoulateAddressTo = 2)-- Primary Address is Shipping Address
	begin
		  select                   
			@vcPStreet= AD2.vcStreet,
			@vcPCity=AD2.vcCity,
			@vcPState=AD2.numState,
			@vcPPostalCode=AD2.vcPostalCode,
			@vcPCountry=AD2.numCountry                   
		  from divisionMaster DM
		  LEFT JOIN dbo.AddressDetails AD2 ON AD2.numDomainID=DM.numDomainID 
		  AND AD2.numRecordID= DM.numDivisionID AND AD2.tintAddressOf=2 AND AD2.tintAddressType=2 AND AD2.bitIsPrimary=1
		  where numDivisionID=@numDivisionId    
	
	end                       
                       
 if (@numEmpStatus is null or @numEmpStatus=0) set @numEmpStatus=658        
 
    INSERT into AdditionalContactsInformation                                    
    (                  
   numContactType,                  
   vcDepartment,                  
   vcCategory,                  
   vcGivenName,                  
   vcFirstName,                  
   vcLastName,                   
   numDivisionId ,                  
   numPhone ,                  
   numPhoneExtension,                  
   numCell ,                  
   numHomePhone ,                          
   vcFax ,                  
   vcEmail ,                  
   VcAsstFirstName ,                  
   vcAsstLastName ,                  
   numAsstPhone ,                  
   numAsstExtn ,                  
   vcAsstEmail  ,                  
   charSex ,                  
   bintDOB ,                  
   vcPosition ,                  
   txtNotes ,                  
   numCreatedBy ,                                    
   bintCreatedDate,             
   numModifiedBy,            
   bintModifiedDate,                   
   numDomainID,                  
   numManagerID,                   
   numRecOwner,                  
   numTeam,                  
   numEmpStatus,
   vcTitle,bitPrimaryContact,bitOptOut,
   vcLinkedinId,vcLinkedinUrl,numECampaignID,vcTaxID           
   )                                    
    VALUES                                    
      (                  
   @numContactType,                  
   @vcDepartment,                  
   @vcCategory,                  
   @vcGivenName ,                  
   @vcFirstName ,                  
   @vcLastName,                   
   @numDivisionId ,                  
   @numPhone ,                                    
   @numPhoneExtension,                  
   @numCell ,                  
   @NumHomePhone ,                  
   @vcFax ,                  
   @vcEmail ,                  
   @VcAsstFirstName,                         
   @vcAsstLastName,                  
   @numAsstPhone,                  
   @numAsstExtn,                  
   @vcAsstEmail,                  
   @charSex,                  
   @bintDOB ,                  
   @vcPosition,                                   
   @txtNotes,                  
   @numUserCntID,                  
   getutcdate(),             
   @numUserCntID,                  
   getutcdate(),                   
   @numDomainID,                   
   @numManagerID,                   
   @numUserCntID,                   
   @numTeam,                  
   @numEmpStatus,
   @vcTitle,@bitPrimaryContact,@bitOptOut,
   @vcLinkedinId,@vcLinkedinUrl,@numECampaignID,@vcTaxID           
   )                                    
                     
 set @numcontactId= SCOPE_IDENTITY()
 
 SELECT @numcontactId             

     INSERT INTO dbo.AddressDetails (
	 	vcAddressName,
	 	vcStreet,
	 	vcCity,
	 	vcPostalCode,
	 	numState,
	 	numCountry,
	 	bitIsPrimary,
	 	tintAddressOf,
	 	tintAddressType,
	 	numRecordID,
	 	numDomainID
	 ) VALUES ('Primary', @vcPStreet,@vcPCity,@vcPPostalCode,@vcPState,@vcPCountry,1,1,0,@numcontactId,@numDomainID)
     
	 IF ISNULL(@numECampaignID,0) > 0 
	 BEGIN
		  --Manage ECampiagn When Campaign is changed from inline edit
		DECLARE @Date AS DATETIME 
		SELECT @Date = dbo.[GetUTCDateWithoutTime]()

 		EXEC [USP_ManageConEmailCampaign]
		@numConEmailCampID = 0, --  numeric(9, 0)
		@numContactID = @numcontactId, --  numeric(9, 0)
		@numECampaignID = @numECampaignID, --  numeric(9, 0)
		@intStartDate = @Date, --  datetime
		@bitEngaged = 1, --  bit
		@numUserCntID = @numUserCntID --  numeric(9, 0)     
	END                                         
                                                 
  END                                    
 ELSE if @numContactId>0                                                        
    BEGIN                                    
    UPDATE AdditionalContactsInformation SET                                    
      numContactType=@numContactType,                                    
      vcGivenName=@vcGivenName,                                    
      vcFirstName=@vcFirstName ,                                    
      vcLastName =@vcLastName ,                   
      numDivisionId =@numDivisionId ,                                    
      numPhone=@numPhone ,                                    
      numPhoneExtension=@numPhoneExtension,                                    
      numCell =@numCell ,                                    
      NumHomePhone =@NumHomePhone ,                                    
      vcFax=@vcFax ,                                    
      vcEmail=@vcEmail ,                                    
      VcAsstFirstName=@VcAsstFirstName,                                    
      vcAsstLastName=@vcAsstLastName,                                    
      numAsstPhone=@numAsstPhone,                                    
      numAsstExtn=@numAsstExtn,                                    
      vcAsstEmail=@vcAsstEmail,                                                      
      charSex=@charSex,                            
      bintDOB=@bintDOB ,                                    
      vcPosition=@vcPosition,                                                    
      txtNotes=@txtNotes,                                                     
      numModifiedBy=@numUserCntID,                                    
      bintModifiedDate=getutcdate(),                                    
      numManagerID=@numManagerID,
      bitPrimaryContact=@bitPrimaryContact                              
     WHERE numContactId=@numContactId   
                    
     Update dbo.AddressDetails set                   
       vcStreet=@vcPStreet,                                    
       vcCity =@vcPCity,                                    
       vcPostalCode=@vcPPostalCode,                                     
       numState=@vcPState,                                    
       numCountry=@vcPCountry
	  where numRecordID=@numContactId AND bitIsPrimary=1 AND tintAddressOf=1 AND tintAddressType=0
 
     SELECT @numcontactId   
                                                
  END    
  ------Check the Email id Exist into the Email Master Table

	IF @vcPassword IS NOT NULL AND  LEN(@vcPassword) > 0
	BEGIN
		IF EXISTS (SELECT numExtranetID FROM ExtarnetAccounts WHERE numDivisionID = @numDivisionID)
		BEGIN
			IF EXISTS (SELECT numExtranetDtlID FROM ExtranetAccountsDtl WHERE numContactID=@numContactID)
			BEGIN
				UPDATE
					ExtranetAccountsDtl
				SET 
					vcPassword=@vcPassword
					,tintAccessAllowed=1
				WHERE
					numContactID=@numContactID
			END
			ELSE
			BEGIN
				DECLARE @numExtranetID NUMERIC(18,0)
				SELECT @numExtranetID=numExtranetID FROM ExtarnetAccounts WHERE numDivisionID = @numDivisionID

				INSERT INTO ExtranetAccountsDtl (numExtranetID,numContactID,vcPassword,tintAccessAllowed,numDomainID)
				VALUES (@numExtranetID,@numContactID,@vcPassword,1,@numDomainID)
			END
		END
		ELSE
		BEGIN
			RAISERROR('Organization e-commerce access is not enabled',16,1)
		END
	END



DECLARE @numCount AS NUMERIC(18,0)
SET @numCount = 0
IF @vcEmail <> ''
BEGIN 
SELECT @numCount = COUNT(*) FROM dbo.EmailMaster WHERE vcEmailID =@vcEmail and numDomainId=@numDomainID
IF @numCount = 0 
BEGIN
	INSERT INTO EmailMaster(vcEmailId,vcName,numDomainId,numContactId)
	VALUES(@vcEmail,@vcFirstName,@numDomainID,@numcontactId)
END
else
begin
	update EmailMaster set numContactId=@numcontactId where vcEmailID =@vcEmail and numDomainId=@numDomainID
end
END 
--IF @vcAsstEmail <> ''
--BEGIN
--SELECT @numCount = COUNT(*) FROM dbo.EmailMaster WHERE vcEmailID =@vcAsstEmail
--IF @numCount = 0 
--BEGIN
--	INSERT INTO EmailMaster(vcEmailId,vcName,numDomainId,numContactId)
--	VALUES(@vcAsstEmail,@vcFirstName,@numDomainID,@numcontactId)
--END
--END
---------------------------------------------------------

GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ManageBillHeader' ) 
    DROP PROCEDURE USP_ManageBillHeader
GO

CREATE PROCEDURE USP_ManageBillHeader
    @numBillID [numeric](18, 0) OUTPUT,
    @numDivisionID [numeric](18, 0),
    @numAccountID [numeric](18, 0),
    @dtBillDate [datetime],
    @numTermsID [numeric](18, 0),
    @numDueAmount [numeric](18, 2),
    @dtDueDate [datetime],
    @vcMemo [varchar](1000),
    @vcReference [varchar](500),
    @numBillTotalAmount [numeric](18, 2),
    @bitIsPaid [bit],
    @numDomainID [numeric](18, 0),
    @strItems VARCHAR(MAX),
    @numUserCntID NUMERIC(18, 0),
	@numOppId NUMERIC(18,0)=0,
	@bitLandedCost BIT=0,
	@numCurrencyID NUMERIC(18,0) = 0
AS 
    SET NOCOUNT ON  
    BEGIN TRY 
        BEGIN TRAN  
        --Validation of closed financial year
		EXEC dbo.USP_ValidateFinancialYearClosingDate @numDomainID,@dtBillDate

		DECLARE @fltExchangeRate AS FLOAT

		IF ISNULL(@numCurrencyID,0) = 0
		BEGIN
			SET @fltExchangeRate=1
			SELECT @numCurrencyID=numCurrencyID FROM Domain WHERE numDomainId = @numDomainID
		END
		ELSE
		BEGIN 
			SET @fltExchangeRate = dbo.GetExchangeRate(@numDomainID,@numCurrencyID)
        END  
        
        IF EXISTS ( SELECT  [numBillID]
                    FROM    [dbo].[BillHeader]
                    WHERE   [numBillID] = @numBillID ) 
            BEGIN
                UPDATE  [dbo].[BillHeader]
                SET     numDivisionID = @numDivisionID,
                        numAccountID = @numAccountID,
                        dtBillDate = @dtBillDate,
                        numTermsID = @numTermsID,
                        monAmountDue = @numDueAmount,
                        dtDueDate = @dtDueDate,
                        vcMemo = @vcMemo,
                        vcReference = @vcReference,
                        --monAmtPaid = @numBillTotalAmount,
                        bitIsPaid = (CASE WHEN ISNULL(monAmtPaid,0)=@numDueAmount THEN 1
									ELSE 0 END),
                        numDomainID = @numDomainID,
                        dtModifiedDate = GETUTCDATE(),
                        numModifiedBy = @numUserCntID,
						numCurrencyID=@numCurrencyID,
						fltExchangeRate = @fltExchangeRate
                WHERE   [numBillID] = @numBillID
                        AND numDomainID = @numDomainID
            END
        ELSE 
            BEGIN

				-- GET ACCOUNT CLASS IF ENABLED
				DECLARE @numAccountClass AS NUMERIC(18) = 0

				DECLARE @tintDefaultClassType AS INT = 0
				SELECT @tintDefaultClassType = ISNULL(tintDefaultClassType,0) FROM Domain WHERE numDomainId = @numDomainID 

				IF @tintDefaultClassType = 1 --USER
				BEGIN
					SELECT 
						@numAccountClass=ISNULL(numDefaultClass,0) 
					FROM 
						dbo.UserMaster UM 
					JOIN 
						dbo.Domain D 
					ON 
						UM.numDomainID=D.numDomainId
					WHERE 
						D.numDomainId=@numDomainId 
						AND UM.numUserDetailId=@numUserCntID
				END
				ELSE IF @tintDefaultClassType = 2 --COMPANY
				BEGIN
					SELECT 
						@numAccountClass=ISNULL(numAccountClassID,0) 
					FROM 
						dbo.DivisionMaster DM 
					WHERE 
						DM.numDomainId=@numDomainId 
						AND DM.numDivisionID=@numDivisionID
				END
				ELSE
				BEGIN
					SET @numAccountClass = 0
				END
            
                INSERT  INTO [dbo].[BillHeader]
                        (
                          [numDivisionID],
                          [numAccountID],
                          [dtBillDate],
                          [numTermsID],
                          [monAmountDue],
                          [dtDueDate],
                          [vcMemo],
                          [vcReference],
                          --[monAmtPaid],
                          [bitIsPaid],
                          [numDomainID],
                          [numCreatedBy],
                          [dtCreatedDate],numAccountClass,[numOppId],[bitLandedCost],[numCurrencyID],[fltExchangeRate]
                        )
                VALUES  (
                          @numDivisionID,
                          @numAccountID,
                          @dtBillDate,
                          @numTermsID,
                          @numDueAmount,
                          @dtDueDate,
                          @vcMemo,
                          @vcReference,
                          --@numBillTotalAmount,
                          @bitIsPaid,
                          @numDomainID,
                          @numUserCntID,
                          GETUTCDATE(),@numAccountClass,@numOppId,@bitLandedCost,@numCurrencyID,@fltExchangeRate
                        )
                SET @numBillID = SCOPE_IDENTITY()
            END
				
		PRINT @numBillID		
--        DELETE  FROM dbo.BillDetails
--        WHERE   numBillID = @numBillID 
				
        DECLARE @hDocItem INT
        IF CONVERT(VARCHAR(MAX), @strItems) <> '' 
            BEGIN
                EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems
                
                 DELETE FROM BillDetails WHERE numBillID = @numBillID 
					AND numBillDetailID NOT IN (SELECT X.numBillDetailID 
			 FROM OPENXML(@hDocItem,'/NewDataSet/Table1 [numBillDetailID>0]',2)                                                                                                              
			With(numBillDetailID numeric(9))X)
			                                                           
			--Update transactions
			Update BillDetails  Set numExpenseAccountID=X.numExpenseAccountID,monAmount=X.monAmount,                                                                                              
			vcDescription=X.vcDescription,numProjectID=X.numProjectID
			,numClassID=X.numClassID,numCampaignID=X.numCampaignID
			From (SELECT * FROM OPENXML(@hDocItem,'/NewDataSet/Table1 [numBillDetailID>0]',2)                                                                                                              
			With(                                                                                       
			numBillDetailID NUMERIC(18,0),[numExpenseAccountID] NUMERIC(18, 0), [monAmount] MONEY, [vcDescription] VARCHAR(1000), [numProjectID] NUMERIC(18, 0), [numClassID] NUMERIC(18, 0), [numCampaignID] NUMERIC(18, 0)                                                                                                 
			))X                                                                                                  
			Where BillDetails.numBillDetailID=X.numBillDetailID AND numBillID = @numBillID                                                                                             
			               
			
                INSERT  INTO [dbo].[BillDetails]
                        (
                          [numBillID],
                          [numExpenseAccountID],
                          [monAmount],
                          [vcDescription],
                          [numProjectID],
                          [numClassID],
                          [numCampaignID]
                        )
                        SELECT  @numBillID,
                                X.[numExpenseAccountID],
                                X.[monAmount],
                                X.[vcDescription],
                                X.[numProjectID],
                                X.[numClassID],
                                X.[numCampaignID]
                        FROM    ( SELECT    *
                                  FROM      OPENXML (@hDocItem, '/NewDataSet/Table1 [numBillDetailID=0]', 2)
                                            WITH (numBillDetailID NUMERIC(18,0), [numExpenseAccountID] NUMERIC(18, 0), [monAmount] MONEY, [vcDescription] VARCHAR(1000), [numProjectID] NUMERIC(18, 0), [numClassID] NUMERIC(18, 0), [numCampaignID] NUMERIC(18, 0) )
                                ) X
                EXEC sp_xml_removedocument @hDocItem
            END

			IF EXISTS (SELECT numBillDetailID FROM BillDetails WHERE numBillID=@numBillID AND numExpenseAccountID NOT IN (SELECT numAccountId FROM Chart_Of_Accounts WHERE numDomainId=@numDomainID))
			BEGIN
				RAISERROR('INVALID_EXPENSE_ACCOUNT',16,1)
			END


		-- Update leads,prospects to Accounts
		EXEC dbo.USP_UpdateCRMTypeToAccounts @numDivisionID ,@numDomainID ,@numUserCntID
		
        COMMIT TRAN 
    END TRY 
    BEGIN CATCH		
        DECLARE @strMsg VARCHAR(200)
        SET @strMsg = ERROR_MESSAGE()
        IF ( @@TRANCOUNT > 0 ) 
            BEGIN
                RAISERROR ( @strMsg, 16, 1 ) ;
                ROLLBACK TRAN
                RETURN 1
            END
    END CATCH	
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageCommissionRule')
DROP PROCEDURE USP_ManageCommissionRule
GO
CREATE PROCEDURE [dbo].[USP_ManageCommissionRule]
    @numComRuleID NUMERIC,
    @vcCommissionName VARCHAR(100),
    @tintComAppliesTo TINYINT,
    @tintComBasedOn TINYINT,
    @tinComDuration TINYINT,
    @tintComType TINYINT,
    @numDomainID NUMERIC,
    @strItems TEXT,
    @tintAssignTo TINYINT,
    @tintComOrgType TINYINT
AS 
BEGIN TRY
BEGIN TRANSACTION
    IF @numComRuleID = 0 
    BEGIN
		INSERT  INTO CommissionRules
				(
					[vcCommissionName],
					[numDomainID]
				)
		VALUES  (
					@vcCommissionName,
					@numDomainID
				)
		SET @numComRuleID=@@identity
		Select @numComRuleID;
    END
    ELSE 
    BEGIN		
		UPDATE  
				CommissionRules
            SET     
				[vcCommissionName] = @vcCommissionName,
                [tintComBasedOn] = @tintComBasedOn,
                [tinComDuration] = @tinComDuration,
                [tintComType] = @tintComType,
				[tintComAppliesTo] = @tintComAppliesTo,
				[tintComOrgType] = @tintComOrgType,
				[tintAssignTo] = @tintAssignTo
            WHERE   
				[numComRuleID] = @numComRuleID 
				AND [numDomainID] = @numDomainID

		IF @tintComAppliesTo = 3
		BEGIN
			DELETE FROM CommissionRuleItems WHERE numComRuleID=@numComRuleID
		END
				
		DELETE FROM [CommissionRuleDtl] WHERE [numComRuleID] = @numComRuleID
		DELETE FROM [CommissionRuleContacts] WHERE [numComRuleID] = @numComRuleID

		DECLARE @hDocItem INT
		EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems
        
		IF CONVERT(VARCHAR(10), @strItems) <> '' 
        BEGIN
            INSERT  INTO [CommissionRuleDtl]
                    (
                        numComRuleID,
                        [intFrom],
                        [intTo],
                        [decCommission]
                    )
                    SELECT  @numComRuleID,
                            X.[intFrom],
                            X.[intTo],
                            X.[decCommission]
                    FROM    ( SELECT    *
                                FROM      OPENXML (@hDocItem, '/NewDataSet/CommissionTable', 2)
                                        WITH ( intFrom DECIMAL(18,2), intTo DECIMAL(18,2), decCommission DECIMAL(18,2) )
                            ) X  
            
			INSERT  INTO [CommissionRuleContacts]
                    (
                        numComRuleID,
                        numValue,bitCommContact
                    )
                    SELECT  @numComRuleID,
                            X.[numValue],X.[bitCommContact]
                    FROM    ( SELECT    *
                                FROM      OPENXML (@hDocItem, '/NewDataSet/ContactTable', 2)
                                        WITH ( numValue NUMERIC,bitCommContact bit)
                            ) X
			
            EXEC sp_xml_removedocument @hDocItem
        END
        

		DECLARE @bitItemDuplicate AS BIT = 0
		DECLARE @bitOrgDuplicate AS BIT = 0
		DECLARE @bitContactDuplicate AS BIT = 0

		/*Duplicate Rule values checking */
		-- CHECK IF OTHER COMMISSION RULE IS EXISTS IN ACCOUNT WIRH SAME OPTION IN SETP 2,3,4 and 5
		IF (SELECT COUNT(*) FROM CommissionRules WHERE numDomainID=@numDomainID AND tintComAppliesTo=@tintComAppliesTo AND tintComOrgType=@tintComOrgType AND tintAssignTo = @tintAssignTo AND numComRuleID <> @numComRuleID) > 0
		BEGIN
			-- ITEM
			IF @tintComAppliesTo = 1
			BEGIN
				-- ITEM ID
				IF (SELECT 
						COUNT(*) 
					FROM 
						CommissionRuleItems 
					WHERE 
						tintType=1
						AND numValue IN (SELECT ISNULL(numValue,0) FROM CommissionRuleItems WHERE numComRuleID=@numComRuleID) 
						AND numComRuleID IN (SELECT ISNULL(numComRuleID,0) FROM CommissionRules WHERE numDomainID=@numDomainID AND tintComAppliesTo=@tintComAppliesTo AND tintComOrgType=@tintComOrgType AND tintAssignTo = @tintAssignTo AND numComRuleID <> @numComRuleID)) > 0
				BEGIN
					SET @bitItemDuplicate = 1
				END
			END
			ELSE IF @tintComAppliesTo = 2
			BEGIN
				-- ITEM CLASSIFICATION
				IF (SELECT 
						COUNT(*) 
					FROM 
						CommissionRuleItems 
					WHERE 
						tintType=2
						AND numValue IN (SELECT ISNULL(numValue,0) FROM CommissionRuleItems WHERE numComRuleID=@numComRuleID) 
						AND numComRuleID IN (SELECT ISNULL(numComRuleID,0) FROM CommissionRules WHERE numDomainID=@numDomainID AND tintComAppliesTo=@tintComAppliesTo AND tintComOrgType=@tintComOrgType AND tintAssignTo = @tintAssignTo AND numComRuleID <> @numComRuleID)) > 0
				BEGIN
					SET @bitItemDuplicate = 1
				END
			END
			ELSE IF @tintComAppliesTo = 3
			BEGIN
				-- ALL ITEMS
				SET @bitItemDuplicate = 1
			END

			--ORGANIZATION
			IF @tintComOrgType = 1
			BEGIN
				-- ORGANIZATION ID
				IF (SELECT 
						COUNT(*) 
					FROM 
						CommissionRuleOrganization 
					WHERE 
						numValue IN (SELECT ISNULL(numValue,0) FROM CommissionRuleOrganization WHERE numComRuleID=@numComRuleID) 
						AND numComRuleID IN (SELECT ISNULL(numComRuleID,0) FROM CommissionRules WHERE numDomainID=@numDomainID AND tintComAppliesTo=@tintComAppliesTo AND tintComOrgType=@tintComOrgType AND tintAssignTo = @tintAssignTo AND numComRuleID <> @numComRuleID)) > 0
				BEGIN
					SET @bitOrgDuplicate = 1
				END
			END
			ELSE IF @tintComOrgType = 2
			BEGIN
				-- ORGANIZATION RELATIONSHIP OR PROFILE
				IF (SELECT 
						COUNT(*) 
					FROM 
						CommissionRuleOrganization 
					WHERE 
						numValue IN (SELECT ISNULL(numValue,0) FROM CommissionRuleOrganization WHERE numComRuleID=@numComRuleID) 
						AND numProfile IN (SELECT ISNULL(numProfile,0) FROM CommissionRuleOrganization WHERE numComRuleID=@numComRuleID) 
						AND numComRuleID IN (SELECT ISNULL(numComRuleID,0) FROM CommissionRules WHERE numDomainID=@numDomainID AND tintComAppliesTo=@tintComAppliesTo AND tintComOrgType=@tintComOrgType AND tintAssignTo = @tintAssignTo AND numComRuleID <> @numComRuleID)) > 0
				BEGIN
					SET @bitOrgDuplicate = 1
				END
			END
			ELSE IF  @tintComOrgType = 3
			BEGIN
				-- ALL ORGANIZATIONS
				SET @bitOrgDuplicate = 1
			END

			--CONTACT
			IF (
				SELECT 
					COUNT(*) 
				FROM 
					CommissionRuleContacts 
				WHERE 
					numValue IN (SELECT ISNULL(numValue,0) FROM CommissionRuleContacts WHERE numComRuleID=@numComRuleID) 
					AND numComRuleID IN (SELECT 
											ISNULL(numComRuleID,0) 
										FROM 
											CommissionRules 
										WHERE 
											numDomainID=@numDomainID 
											AND tintComAppliesTo = @tintComAppliesTo
											AND 1 = (CASE tintComAppliesTo 
														WHEN 1 THEN (CASE WHEN (SELECT COUNT(*) FROM CommissionRuleItems CRIInner WHERE tintType=1 AND numValue IN (SELECT ISNULL(numValue,0) FROM CommissionRuleItems WHERE numComRuleID=@numComRuleID) AND CRIInner.numComRuleID = CommissionRuleContacts.numComRuleID) > 0 THEN 1 ELSE 0 END)
														WHEN 2 THEN (CASE WHEN (SELECT COUNT(*) FROM CommissionRuleItems CRIInner WHERE tintType=2 AND numValue IN (SELECT ISNULL(numValue,0) FROM CommissionRuleItems WHERE numComRuleID=@numComRuleID) AND CRIInner.numComRuleID = CommissionRuleContacts.numComRuleID) > 0 THEN 1 ELSE 0 END)
														WHEN 3 THEN 1 
													END)
											AND tintComOrgType=@tintComOrgType
											AND 1 = (CASE tintComOrgType 
														WHEN 1 THEN (CASE WHEN (SELECT COUNT(*) FROM CommissionRuleOrganization CROInner WHERE numValue IN (SELECT ISNULL(numValue,0) FROM CommissionRuleOrganization WHERE numComRuleID=@numComRuleID) AND CROInner.numComRuleID = CommissionRuleContacts.numComRuleID) > 0 THEN 1 ELSE 0 END)
														WHEN 2 THEN (CASE WHEN (SELECT COUNT(*) FROM CommissionRuleOrganization CROInner WHERE numValue IN (SELECT ISNULL(numValue,0) FROM CommissionRuleOrganization WHERE numComRuleID=@numComRuleID) AND numProfile IN (SELECT ISNULL(numProfile,0) FROM CommissionRuleOrganization WHERE numComRuleID=@numComRuleID) AND CROInner.numComRuleID = CommissionRuleContacts.numComRuleID) > 0 THEN 1 ELSE 0 END)
														WHEN 3 THEN 1 
													END)
											AND tintAssignTo = @tintAssignTo 
											AND numComRuleID <> @numComRuleID
										)
					) > 0
			BEGIN
				SET @bitContactDuplicate = 1
			END
		END

		IF (ISNULL(@bitItemDuplicate,0) = 1 AND ISNULL(@bitOrgDuplicate,0) = 1 AND ISNULL(@bitContactDuplicate,0) = 1)
		BEGIN
			RAISERROR ( 'DUPLICATE',16, 1 )
			RETURN ;
		END

		SELECT  @numComRuleID
    END
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageCorrespondence')
DROP PROCEDURE USP_ManageCorrespondence
GO
CREATE PROCEDURE [dbo].[USP_ManageCorrespondence]
    @numCorrespondenceID NUMERIC = 0 OUTPUT,
    @numCommID NUMERIC = 0,
    @numEmailHistoryID NUMERIC = 0,
    @tintCorrType TINYINT,
    @numDomainID NUMERIC,
    @numOpenRecordID NUMERIC,
	@monMRItemAmount money=0
AS 
    SET NOCOUNT ON

    IF @numCommID = 0 
        SET @numCommID = NULL
    IF @numEmailHistoryID = 0 
        SET @numEmailHistoryID = NULL
    IF @numCorrespondenceID = 0 
        BEGIN
	
            INSERT  INTO Correspondence
                    (
                      [numCommID],
                      [numEmailHistoryID],
                      [tintCorrType],
                      [numDomainID],
					  [numOpenRecordID],
					  [monMRItemAmount]
	              )
            VALUES  (
                      @numCommID,
                      @numEmailHistoryID,
                      @tintCorrType,
                      @numDomainID,
					  @numOpenRecordID,
					  @monMRItemAmount
	              )

            SELECT  @numCorrespondenceID = SCOPE_IDENTITY()
        END
    ELSE 
        BEGIN
            UPDATE  Correspondence
            SET     [numCommID] = @numCommID,
                    [numEmailHistoryID] = @numEmailHistoryID,
                    [tintCorrType] = @tintCorrType,
                    [numOpenRecordID] = @numOpenRecordID,
					[monMRItemAmount] = @monMRItemAmount
            WHERE   [numCorrespondenceID] = @numCorrespondenceID

        END		UPDATE OpportunityBizDocs SET [bitIsEmailSent] = 1 WHERE numOppId = @numOpenRecordID
GO
/****** Object:  StoredProcedure [dbo].[USP_ManageItemsAndKits]    Script Date: 02/15/2010 21:44:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Anoop Jayaraj                                                          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_manageitemsandkits')
DROP PROCEDURE usp_manageitemsandkits
GO
CREATE PROCEDURE [dbo].[USP_ManageItemsAndKits]                                                                
@numItemCode as numeric(9) output,                                                                
@vcItemName as varchar(300),                                                                
@txtItemDesc as varchar(1000),                                                                
@charItemType as char(1),                                                                
@monListPrice as money,                                                                
@numItemClassification as numeric(9),                                                                
@bitTaxable as bit,                                                                
@vcSKU as varchar(50),                                                                                                                                      
@bitKitParent as bit,                                                                                   
--@dtDateEntered as datetime,                                                                
@numDomainID as numeric(9),                                                                
@numUserCntID as numeric(9),                                                                                                  
@numVendorID as numeric(9),                                                            
@bitSerialized as bit,                                               
@strFieldList as text,                                            
@vcModelID as varchar(200)='' ,                                            
@numItemGroup as numeric(9)=0,                                      
@numCOGSChartAcntId as numeric(9)=0,                                      
@numAssetChartAcntId as numeric(9)=0,                                      
@numIncomeChartAcntId as numeric(9)=0,                            
@monAverageCost as money,                          
@monLabourCost as money,                  
@fltWeight as float,                  
@fltHeight as float,                  
@fltLength as float,                  
@fltWidth as float,                  
@bitFreeshipping as bit,                
@bitAllowBackOrder as bit,              
@UnitofMeasure as varchar(30)='',              
@strChildItems as text,            
@bitShowDeptItem as bit,              
@bitShowDeptItemDesc as bit,        
@bitCalAmtBasedonDepItems as bit,    
@bitAssembly as BIT,
@intWebApiId INT=null,
@vcApiItemId AS VARCHAR(50)=null,
@numBarCodeId as VARCHAR(25)=null,
@vcManufacturer as varchar(250)=NULL,
@numBaseUnit AS NUMERIC(18)=0,
@numPurchaseUnit AS NUMERIC(18)=0,
@numSaleUnit AS NUMERIC(18)=0,
@bitLotNo as BIT,
@IsArchieve AS BIT,
@numItemClass as numeric(9)=0,
@tintStandardProductIDType AS TINYINT=0,
@vcExportToAPI VARCHAR(50),
@numShipClass NUMERIC,
@ProcedureCallFlag SMALLINT =0,
@vcCategories AS VARCHAR(2000) = '',
@bitAllowDropShip AS BIT=0,
@bitArchiveItem AS BIT = 0,
@bitAsset AS BIT = 0,
@bitRental AS BIT = 0,
@numCategoryProfileID AS NUMERIC(18,0) = 0,
@bitVirtualInventory BIT,
@bitContainer BIT,
@numContainer NUMERIC(18,0)=0,
@numNoItemIntoContainer NUMERIC(18,0),
@bitMatrix BIT = 0 ,
@vcItemAttributes VARCHAR(2000) = '',
@numManufacturer NUMERIC(18,0)
AS
BEGIN     
BEGIN TRY
BEGIN TRANSACTION

	SET @vcManufacturer = CASE WHEN ISNULL(@numManufacturer,0) > 0 THEN ISNULL((SELECT vcCompanyName FROM DivisionMaster INNER JOIN CompanyInfo ON DivisionMaster.numCompanyID=CompanyInfo.numCompanyId WHERE numDivisionID=@numManufacturer),'')  ELSE ISNULL(@vcManufacturer,'') END

	DECLARE @bitAttributesChanged AS BIT  = 1
	DECLARE @bitOppOrderExists AS BIT = 0

	IF ISNULL(@numItemCode,0) > 0
	BEGIN
		IF (SELECT COUNT(OI.numOppId) FROM dbo.OpportunityItems OI WHERE OI.numItemCode =@numItemCode) > 0
		BEGIN
			SET @bitOppOrderExists = 1
		END
		ELSE IF (SELECT COUNT(OI.numOppId) FROM dbo.OpportunityKitItems OI WHERE OI.numChildItemID =@numItemCode) > 0
		BEGIN
			SET @bitOppOrderExists = 1
		END
		ELSE IF (SELECT COUNT(OI.numOppId) FROM dbo.OpportunityKitChildItems OI WHERE OI.numItemID =@numItemCode) > 0
		BEGIN
			SET @bitOppOrderExists = 1
		END
	END
	ELSE 
		BEGIN
		   IF (ISNULL(@vcItemName,'') = '')
			   BEGIN
				   RAISERROR('ITEM_NAME_NOT_SELECTED',16,1)
				   RETURN
			   END
           ELSE IF (ISNULL(@charItemType,'') = '0')
			   BEGIN
				   RAISERROR('ITEM_TYPE_NOT_SELECTED',16,1)
				   RETURN
			   END
           ELSE
		   BEGIN 
		      If (@charItemType = 'P')
			  BEGIN
				IF ((SELECT COUNT(*) FROM Chart_Of_Accounts WHERE numDomainId=@numDomainID AND numAccountId=@numAssetChartAcntId) = 0)
				BEGIN
					RAISERROR('INVALID_ASSET_ACCOUNT',16,1)
					RETURN
				END
				
			  END

				-- This is common check for CharItemType P and Other
				IF ((SELECT COUNT(*) FROM Chart_Of_Accounts WHERE numDomainId=@numDomainID AND numAccountId=@numCOGSChartAcntId) = 0)
				BEGIN
					RAISERROR('INVALID_COGS_ACCOUNT',16,1)
					RETURN
				END

				IF ((SELECT COUNT(*) FROM Chart_Of_Accounts WHERE numDomainId=@numDomainID AND numAccountId=@numIncomeChartAcntId) = 0)
				BEGIN
					RAISERROR('INVALID_INCOME_ACCOUNT',16,1)
					RETURN
				END
		   END
		END
	--INSERT CUSTOM FIELDS BASE ON WAREHOUSEITEMS
	DECLARE @hDoc AS INT     
	                                                                        	
	DECLARE @TempTable TABLE 
	(
		ID INT IDENTITY(1,1),
		Fld_ID NUMERIC(18,0),
		Fld_Value NUMERIC(18,0)
	)   

	IF ISNULL(@bitMatrix,0) = 1 AND ISNULL(@numItemGroup,0) = 0
	BEGIN
		RAISERROR('ITEM_GROUP_NOT_SELECTED',16,1)
		RETURN
	END
	ELSE IF ISNULL(@bitMatrix,0) = 1 AND LEN(ISNULL(@vcItemAttributes,'')) = 0 
	BEGIN
		RAISERROR('ITEM_ATTRIBUTES_NOT_SELECTED',16,1)
		RETURN
	END
	ELSE IF ISNULL(@bitMatrix,0) = 1 
	BEGIN
		DECLARE @bitDuplicate AS BIT = 0
	
		IF DATALENGTH(ISNULL(@vcItemAttributes,''))>2
		BEGIN
			EXEC sp_xml_preparedocument @hDoc OUTPUT, @vcItemAttributes
	                                      
			INSERT INTO @TempTable 
			(
				Fld_ID,
				Fld_Value
			)    
			SELECT
				*          
			FROM 
				OPENXML (@hDoc,'/NewDataSet/CusFlds',2)                                                  
			WITH 
				(Fld_ID NUMERIC(18,0),Fld_Value NUMERIC(18,0)) 
			ORDER BY 
				Fld_ID       

			DECLARE @strAttribute VARCHAR(500) = ''
			DECLARE @i AS INT = 1
			DECLARE @COUNT AS INT 
			DECLARE @numFldID AS NUMERIC(18,0)
			DECLARE @numFldValue AS NUMERIC(18,0)
			SELECT @COUNT = COUNT(*) FROM @TempTable

			WHILE @i <= @COUNT
			BEGIN
				SELECT @numFldID=Fld_ID,@numFldValue=Fld_Value FROM @TempTable WHERE ID=@i

				IF ISNUMERIC(@numFldValue)=1
				BEGIN
					SELECT @strAttribute=@strAttribute+Fld_label FROM CFW_Fld_Master WHERE Fld_id=@numFldID AND Grp_id = 9
                
					IF LEN(@strAttribute) > 0
					BEGIN
						SELECT @strAttribute=@strAttribute+':'+ vcData +',' FROM ListDetails WHERE numListItemID=@numFldValue
					END
				END

				SET @i = @i + 1
			END

			SET @strAttribute = SUBSTRING(@strAttribute,0,LEN(@strAttribute))

			IF
				(SELECT
					COUNT(*)
				FROM
					Item
				INNER JOIN
					ItemAttributes
				ON
					Item.numItemCode = ItemAttributes.numItemCode
					AND ItemAttributes.numDomainID = @numDomainID                     
				WHERE
					Item.numItemCode <> @numItemCode
					AND Item.vcItemName = @vcItemName
					AND Item.numItemGroup = @numItemGroup
					AND Item.numDomainID = @numDomainID
					AND dbo.fn_GetItemAttributes(@numDomainID,Item.numItemCode) = @strAttribute
				) > 0
			BEGIN
				RAISERROR('ITEM_WITH_ATTRIBUTES_ALREADY_EXISTS',16,1)
				RETURN
			END

			If ISNULL(@numItemCode,0) > 0 AND dbo.fn_GetItemAttributes(@numDomainID,@numItemCode) = @strAttribute
			BEGIN
				SET @bitAttributesChanged = 0
			END

			EXEC sp_xml_removedocument @hDoc 
		END	
	END
	ELSE IF ISNULL(@bitMatrix,0) = 0
	BEGIN
		-- REMOVE ATTRIBUTES CONFIGURATION OF ITEM AND WAREHOUSE
		DELETE FROM ItemAttributes WHERE numDomainID=@numDomainID AND numItemCode=@numItemCode
	END 
	
	IF ISNULL(@numItemGroup,0) > 0
	BEGIN
		IF (SELECT COUNT(*) FROM ItemGroupsDTL WHERE numItemGroupID = @numItemGroup AND tintType=2) = 0
		BEGIN
			RAISERROR('ITEM_GROUP_NOT_CONFIGURED',16,1)
			RETURN
		END
		ELSE IF EXISTS (SELECT 
							CFM.Fld_id
						FROM 
							CFW_Fld_Master CFM
						LEFT JOIN
							ListDetails LD
						ON
							CFM.numlistid = LD.numListID
						WHERE
							CFM.numDomainID = @numDomainID
							AND CFM.Fld_id IN (SELECT numOppAccAttrID FROM ItemGroupsDTL WHERE numItemGroupID=@numItemGroup AND tintType = 2)
						GROUP BY
							CFM.Fld_id HAVING COUNT(LD.numListItemID) = 0)
		BEGIN
			RAISERROR('ITEM_GROUP_NOT_CONFIGURED',16,1)
			RETURN
		END
	END

	IF @numCOGSChartAcntId=0 SET @numCOGSChartAcntId=NULL
	IF @numAssetChartAcntId=0 SET @numAssetChartAcntId=NULL  
	IF @numIncomeChartAcntId=0 SET @numIncomeChartAcntId=NULL     

	DECLARE @ParentSKU VARCHAR(50) = @vcSKU                        
	DECLARE @ItemID AS NUMERIC(9)
	DECLARE @cnt AS INT

	IF ISNULL(@charItemType,'') = '' OR ISNULL(@charItemType,'') = '0'  
		SET @charItemType = 'N'

	IF @intWebApiId = 2 AND LEN(ISNULL(@vcApiItemId, '')) > 0 AND ISNULL(@numItemGroup, 0) > 0 
    BEGIN 
        -- check wether this id already Mapped in ITemAPI 
        SELECT 
			@cnt = COUNT([numItemID])
        FROM 
			[ItemAPI]
        WHERE 
			[WebApiId] = @intWebApiId
            AND [numDomainId] = @numDomainID
            AND [vcAPIItemID] = @vcApiItemId

        IF @cnt > 0 
        BEGIN
            SELECT 
				@ItemID = [numItemID]
            FROM 
				[ItemAPI]
            WHERE 
				[WebApiId] = @intWebApiId
                AND [numDomainId] = @numDomainID
                AND [vcAPIItemID] = @vcApiItemId
        
			--Update Existing Product coming from API
            SET @numItemCode = @ItemID
        END
    END
	ELSE IF @intWebApiId > 1 AND LEN(ISNULL(@vcSKU, '')) > 0 
    BEGIN
        SET @ParentSKU = @vcSKU
		 
        SELECT 
			@cnt = COUNT([numItemCode])
        FROM 
			[Item]
        WHERE 
			[numDomainId] = @numDomainID
            AND (vcSKU = @vcSKU OR @vcSKU IN (SELECT vcWHSKU FROM dbo.WareHouseItems WHERE numItemID = Item.[numItemCode] AND vcWHSKU = @vcSKU))
            
		IF @cnt > 0 
        BEGIN
            SELECT 
				@ItemID = [numItemCode],
                @ParentSKU = vcSKU
            FROM 
				[Item]
            WHERE 
				[numDomainId] = @numDomainID
                AND (vcSKU = @vcSKU OR @vcSKU IN (SELECT vcWHSKU FROM dbo.WareHouseItems WHERE numItemID = Item.[numItemCode] AND vcWHSKU = @vcSKU))

            SET @numItemCode = @ItemID
        END
		ELSE 
		BEGIN
			-- check wether this id already Mapped in ITemAPI 
			SELECT 
				@cnt = COUNT([numItemID])
			FROM 
				[ItemAPI]
			WHERE 
				[WebApiId] = @intWebApiId
				AND [numDomainId] = @numDomainID
				AND [vcAPIItemID] = @vcApiItemId
                    
			IF @cnt > 0 
			BEGIN
				SELECT 
					@ItemID = [numItemID]
				FROM 
					[ItemAPI]
				WHERE 
					[WebApiId] = @intWebApiId
					AND [numDomainId] = @numDomainID
					AND [vcAPIItemID] = @vcApiItemId
        
				--Update Existing Product coming from API
				SET @numItemCode = @ItemID
			END
		END
    END
                                                         
	IF @numItemCode=0 OR @numItemCode IS NULL
	BEGIN
		INSERT INTO Item 
		(                                             
			vcItemName,txtItemDesc,charItemType,monListPrice,numItemClassification,bitTaxable,vcSKU,bitKitParent,
			numVendorID,numDomainID,numCreatedBy,bintCreatedDate,bintModifiedDate,numModifiedBy,bitSerialized,
			vcModelID,numItemGroup,numCOGsChartAcntId,numAssetChartAcntId,numIncomeChartAcntId,monAverageCost,                          
			monCampaignLabourCost,fltWeight,fltHeight,fltWidth,fltLength,bitFreeShipping,bitAllowBackOrder,vcUnitofMeasure,
			bitShowDeptItem,bitShowDeptItemDesc,bitCalAmtBasedonDepItems,bitAssembly,numBarCodeId,vcManufacturer,numBaseUnit,
			numPurchaseUnit,numSaleUnit,bitLotNo,IsArchieve,numItemClass,tintStandardProductIDType,vcExportToAPI,numShipClass,
			bitAllowDropShip,bitArchiveItem,bitAsset,bitRental,bitVirtualInventory,bitContainer,numContainer,numNoItemIntoContainer,bitMatrix,numManufacturer
		)                                                              
		VALUES                                                                
		(                                                                
			@vcItemName,@txtItemDesc,@charItemType,@monListPrice,@numItemClassification,@bitTaxable,@ParentSKU,@bitKitParent,
			@numVendorID,@numDomainID,@numUserCntID,getutcdate(),getutcdate(),@numUserCntID,@bitSerialized,@vcModelID,@numItemGroup,                                      
			@numCOGsChartAcntId,@numAssetChartAcntId,@numIncomeChartAcntId,@monAverageCost,@monLabourCost,@fltWeight,@fltHeight,@fltWidth,                  
			@fltLength,@bitFreeshipping,@bitAllowBackOrder,@UnitofMeasure,@bitShowDeptItem,@bitShowDeptItemDesc,@bitCalAmtBasedonDepItems,    
			@bitAssembly,@numBarCodeId,@vcManufacturer,@numBaseUnit,@numPurchaseUnit,@numSaleUnit,@bitLotNo,@IsArchieve,@numItemClass,
			@tintStandardProductIDType,@vcExportToAPI,@numShipClass,@bitAllowDropShip,@bitArchiveItem ,@bitAsset,@bitRental,
			@bitVirtualInventory,@bitContainer,@numContainer,@numNoItemIntoContainer,@bitMatrix,@numManufacturer
		)                                             
 
		SET @numItemCode = SCOPE_IDENTITY()
  
		IF  @intWebApiId > 1 
		BEGIN
			 -- insert new product
			 --insert this id into linking table
			 INSERT INTO [ItemAPI] (
	 			[WebApiId],
	 			[numDomainId],
	 			[numItemID],
	 			[vcAPIItemID],
	 			[numCreatedby],
	 			[dtCreated],
	 			[numModifiedby],
	 			[dtModified],vcSKU
			 ) VALUES     
			 (
				@intWebApiId,
				@numDomainID,
				@numItemCode,
				@vcApiItemId,
	 			@numUserCntID,
	 			GETUTCDATE(),
	 			@numUserCntID,
	 			GETUTCDATE(),@vcSKU
	 		) 
		END
 
		IF @charItemType = 'P' AND 1 = (CASE WHEN ISNULL(@numItemGroup,0) > 0 THEN (CASE WHEN ISNULL(@bitMatrix,0)=1 THEN 1 ELSE 0 END) ELSE 1 END)
		BEGIN
			
			DECLARE @TEMPWarehouse TABLE
			(
				ID INT IDENTITY(1,1)
				,numWarehouseID NUMERIC(18,0)
			)
		
			INSERT INTO @TEMPWarehouse (numWarehouseID) SELECT numWareHouseID FROM Warehouses WHERE numDomainID=@numDomainID

			DECLARE @j AS INT = 1
			DECLARE @jCount AS INT
			DECLARE @numTempWarehouseID NUMERIC(18,0)
			SELECT @jCount = COUNT(*) FROM @TEMPWarehouse

			WHILE @j <= @jCount
			BEGIN
				SELECT @numTempWarehouseID=numWarehouseID FROM @TEMPWarehouse WHERE ID=@j

				EXEC USP_WarehouseItems_Save @numDomainID,
											@numUserCntID,
											0,
											@numTempWarehouseID,
											0,
											@numItemCode,
											'',
											@monListPrice,
											0,
											0,
											'',
											'',
											'',
											'',
											0
 
				SET @j = @j + 1
			END
		END 
	END         
	ELSE IF @numItemCode>0 AND @intWebApiId > 0 
	BEGIN 
		IF @ProcedureCallFlag = 1 
		BEGIN
			DECLARE @ExportToAPIList AS VARCHAR(30)
			SELECT @ExportToAPIList = vcExportToAPI FROM dbo.Item WHERE numItemCode = @numItemCode

			IF @ExportToAPIList != ''
			BEGIN
				IF NOT EXISTS(select * from (SELECT * FROM dbo.Split(@ExportToAPIList ,','))as A where items= @vcExportToAPI ) 
				BEGIN
					SET @vcExportToAPI = @ExportToAPIList + ',' + @vcExportToAPI
				End
				ELSE
				BEGIN
					SET @vcExportToAPI = @ExportToAPIList 
				End
			END

			--update ExportToAPI String value
			UPDATE dbo.Item SET vcExportToAPI=@vcExportToAPI where numItemCode=@numItemCode  AND numDomainID=@numDomainID
					
			SELECT 
				@cnt = COUNT([numItemID]) 
			FROM 
				[ItemAPI] 
			WHERE  
				[WebApiId] = @intWebApiId 
				AND [numDomainId] = @numDomainID
				AND [vcAPIItemID] = @vcApiItemId
				AND numItemID = @numItemCode

			IF @cnt > 0
			BEGIN
				--update lastUpdated Date 
				UPDATE [ItemAPI] SET [dtModified] =GETUTCDATE() WHERE [WebApiId] =@intWebApiId AND [numDomainId] = @numDomainID
			END
			ELSE
			--Insert ItemAPI mapping
			BEGIN
				INSERT INTO [ItemAPI] 
				(
					[WebApiId],
					[numDomainId],
					[numItemID],
					[vcAPIItemID],
					[numCreatedby],
					[dtCreated],
					[numModifiedby],
					[dtModified],vcSKU
				) VALUES 
				(
					@intWebApiId,
					@numDomainID,
					@numItemCode,
					@vcApiItemId,
					@numUserCntID,
					GETUTCDATE(),
					@numUserCntID,
					GETUTCDATE(),@vcSKU
				)	
			END
		END   
	END                                            
	ELSE IF @numItemCode > 0 AND @intWebApiId <= 0                                                           
	BEGIN
		DECLARE @OldGroupID NUMERIC 
		SELECT @OldGroupID = ISNULL(numItemGroup,0) FROM [Item] WHERE numitemcode = @numItemCode AND [numDomainId] = @numDomainID
	
		DECLARE @monOldAverageCost AS DECIMAL(18,4);SET @monOldAverageCost=0
		SELECT @monOldAverageCost=(CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(monAverageCost,0) END) FROM Item WHERE numItemCode =@numItemCode AND [numDomainId] = @numDomainID
	   
		UPDATE 
			Item 
		SET 
			vcItemName=@vcItemName,txtItemDesc=@txtItemDesc,charItemType=@charItemType,monListPrice= @monListPrice,numItemClassification=@numItemClassification,                                             
			bitTaxable=@bitTaxable,vcSKU = (CASE WHEN ISNULL(@ParentSKU,'') = '' THEN @vcSKU ELSE @ParentSKU END),bitKitParent=@bitKitParent,                                             
			numVendorID=@numVendorID,numDomainID=@numDomainID,bintModifiedDate=getutcdate(),numModifiedBy=@numUserCntID,bitSerialized=@bitSerialized,                                                         
			vcModelID=@vcModelID,numItemGroup=@numItemGroup,numCOGsChartAcntId=@numCOGsChartAcntId,numAssetChartAcntId=@numAssetChartAcntId,                                      
			numIncomeChartAcntId=@numIncomeChartAcntId,monCampaignLabourCost=@monLabourCost,fltWeight=@fltWeight,fltHeight=@fltHeight,fltWidth=@fltWidth,                  
			fltLength=@fltLength,bitFreeShipping=@bitFreeshipping,bitAllowBackOrder=@bitAllowBackOrder,vcUnitofMeasure=@UnitofMeasure,bitShowDeptItem=@bitShowDeptItem,            
			bitShowDeptItemDesc=@bitShowDeptItemDesc,bitCalAmtBasedonDepItems=@bitCalAmtBasedonDepItems,bitAssembly=@bitAssembly,numBarCodeId=@numBarCodeId,
			vcManufacturer=@vcManufacturer,numBaseUnit=@numBaseUnit,numPurchaseUnit=@numPurchaseUnit,numSaleUnit=@numSaleUnit,bitLotNo=@bitLotNo,
			IsArchieve = @IsArchieve,numItemClass=@numItemClass,tintStandardProductIDType=@tintStandardProductIDType,vcExportToAPI=@vcExportToAPI,
			numShipClass=@numShipClass,bitAllowDropShip=@bitAllowDropShip, bitArchiveItem = @bitArchiveItem,bitAsset=@bitAsset,bitRental=@bitRental,
			bitVirtualInventory = @bitVirtualInventory,numContainer=@numContainer,numNoItemIntoContainer=@numNoItemIntoContainer,bitMatrix=@bitMatrix,numManufacturer=@numManufacturer
		WHERE 
			numItemCode=@numItemCode 
			AND numDomainID=@numDomainID

		IF NOT EXISTS(SELECT numItemCode FROM dbo.OpportunityItems WHERE numItemCode = @numItemCode)
		BEGIN
			IF (((SELECT ISNULL(SUM(numOnHand),0) FROM WareHouseItems WHERE numItemID = @numItemCode AND numDomainId=@numDomainID) = 0) AND ((SELECT ISNULL(bitVirtualInventory,0) FROM Item  WHERE numItemCode = @numItemCode AND numDomainId=@numDomainID) = 0))
			BEGIN
				UPDATE Item SET monAverageCost=@monAverageCost  where numItemCode=@numItemCode  AND numDomainID=@numDomainID	
			END
		END
	
		--If Average Cost changed then add in Tracking
		IF ISNULL(@monAverageCost,0) != ISNULL(@monOldAverageCost,0)
		BEGIN
			DECLARE @numWareHouseItemID NUMERIC(9);SET @numWareHouseItemID=0
			DECLARE @numTotal int;SET @numTotal=0
			SET @i=0
			DECLARE @vcDescription AS VARCHAR(100);SET @vcDescription=''
		
			SELECT @numTotal=COUNT(*) FROM WareHouseItems where numItemID=@numItemCode AND numDomainId=@numDomainId
	     
			WHILE @i < @numTotal
			BEGIN
				SELECT @numWareHouseItemID = min(numWareHouseItemID) FROM WareHouseItems 
					WHERE numItemID=@numItemCode AND numDomainId=@numDomainId and numWareHouseItemID > @numWareHouseItemID 
				
				IF @numWareHouseItemID>0
				BEGIN
					SET @vcDescription='Average Cost Changed Manually OLD : ' + CAST(ISNULL(@monOldAverageCost,0) AS VARCHAR(20))
				
					DECLARE @numDomain AS NUMERIC(18,0)
					SET @numDomain = @numDomainID
					EXEC dbo.USP_ManageWareHouseItems_Tracking
						@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)		
						@numReferenceID = @numItemCode, --  numeric(9, 0)
						@tintRefType = 1, --  tinyint
						@vcDescription = @vcDescription, --  varchar(100)
						@tintMode = 0, --  tinyint
						@numModifiedBy = @numUserCntID, --  numeric(9, 0)
						@numDomainID = @numDomain
				END
		    
				SET @i = @i + 1
			END
		END
 
		DECLARE @bitCartFreeShipping as  bit 
		SELECT @bitCartFreeShipping = bitFreeShipping FROM dbo.CartItems where numItemCode = @numItemCode  AND numDomainID = @numDomainID 
 
		IF @bitCartFreeShipping <> @bitFreeshipping
		BEGIN
			UPDATE dbo.CartItems SET bitFreeShipping = @bitFreeshipping WHERE numItemCode = @numItemCode  AND numDomainID=@numDomainID
		END 
 
		IF @intWebApiId > 1 
		BEGIN
			SELECT 
				@cnt = COUNT([numItemID])
			FROM 
				[ItemAPI]
			WHERE 
				[WebApiId] = @intWebApiId
				AND [numDomainId] = @numDomainID
				AND [vcAPIItemID] = @vcApiItemId

			IF @cnt > 0
			BEGIN
				--update lastUpdated Date 
				UPDATE [ItemAPI] SET [dtModified] =GETUTCDATE() WHERE [WebApiId] =@intWebApiId AND [numDomainId] = @numDomainID
			END
			ELSE --Insert ItemAPI mapping
			BEGIN
				INSERT INTO [ItemAPI] 
				(
	 				[WebApiId],
	 				[numDomainId],
	 				[numItemID],
	 				[vcAPIItemID],
	 				[numCreatedby],
	 				[dtCreated],
	 				[numModifiedby],
	 				[dtModified],vcSKU
				 )
				 VALUES 
				 (
					@intWebApiId,
					@numDomainID,
					@numItemCode,
					@vcApiItemId,
	 				@numUserCntID,
	 				GETUTCDATE(),
	 				@numUserCntID,
	 				GETUTCDATE(),@vcSKU
	 			) 
			END
		END   
		                                                                            
		-- kishan It is necessary to add item into itemTax table . 
		IF	@bitTaxable = 1
		BEGIN
			IF NOT EXISTS(SELECT * FROM dbo.ItemTax WHERE numItemCode = @numItemCode AND numTaxItemID = 0)
			BEGIN
			    INSERT INTO dbo.ItemTax 
				(
					numItemCode,
					numTaxItemID,
					bitApplicable
				)
				VALUES 
				( 
					@numItemCode,
					0,
					1 
				) 						
			END
		END	 
      
		IF @charItemType='S' or @charItemType='N'                                                                       
		BEGIN
			DELETE FROM WareHouseItmsDTL where numWareHouseItemID in (select numWareHouseItemID from WareHouseItems where numItemID=@numItemCode AND [WareHouseItems].[numDomainID] = @numDomainID)
			DELETE FROM WareHouseItems where numItemID=@numItemCode AND [WareHouseItems].[numDomainID] = @numDomainID
		END                                      
   
		IF @bitKitParent=1 OR @bitAssembly = 1              
		BEGIN              
			DECLARE @hDoc1 AS INT                                            
			EXEC sp_xml_preparedocument @hDoc1 OUTPUT, @strChildItems                                                                        
                                             
			UPDATE 
				ItemDetails 
			SET
				numQtyItemsReq=X.QtyItemsReq
				,numWareHouseItemId=X.numWarehouseItmsID
				,numUOMId=X.numUOMId
				,vcItemDesc=X.vcItemDesc
				,sintOrder=X.sintOrder                                                                                          
			From 
			(
				SELECT 
					QtyItemsReq,ItemKitID,ChildItemID,numWarehouseItmsID,numUOMId,vcItemDesc,sintOrder, numItemDetailID As ItemDetailID                             
				FROM 
					OPENXML(@hDoc1,'/NewDataSet/Table1',2)                                                                         
				WITH
				(
					ItemKitID NUMERIC(9),                                                          
					ChildItemID NUMERIC(9),                                                                        
					QtyItemsReq DECIMAL(30, 16),
					numWarehouseItmsID NUMERIC(9),
					numUOMId NUMERIC(9),
					vcItemDesc VARCHAR(1000),
					sintOrder int,
					numItemDetailID NUMERIC(18,0)
				)
			)X                                                                         
			WHERE 
				numItemKitID=X.ItemKitID 
				AND numChildItemID=ChildItemID 
				AND numItemDetailID = X.ItemDetailID

			EXEC sp_xml_removedocument @hDoc1
		 END   
		 ELSE
		 BEGIN
 			DELETE FROM ItemDetails WHERE numItemKitID=@numItemCode
		 END                                                
	END

	IF ISNULL(@numItemGroup,0) = 0 OR ISNULL(@bitMatrix,0) = 1
	BEGIN
		UPDATE WareHouseItems SET monWListPrice = @monListPrice, vcWHSKU=@vcSKU, vcBarCode=@numBarCodeId WHERE numDomainID = @numDomainID AND numItemID = @numItemCode
	END

	IF @numItemCode > 0
	BEGIN
		DELETE 
			IC
		FROM 
			dbo.ItemCategory IC
		INNER JOIN 
			Category 
		ON 
			IC.numCategoryID = Category.numCategoryID
		WHERE 
			numItemID = @numItemCode		
			AND numCategoryProfileID = @numCategoryProfileID

		IF @vcCategories <> ''	
		BEGIN
			INSERT INTO ItemCategory( numItemID , numCategoryID )  SELECT  @numItemCode AS numItemID,ID from dbo.SplitIDs(@vcCategories,',')	
		END
	END

	


	-- SAVE MATRIX ITEM ATTRIBUTES
	IF @numItemCode > 0 AND ISNULL(@bitAttributesChanged,0)=1 AND ISNULL(@bitMatrix,0) = 1 AND LEN(ISNULL(@vcItemAttributes,'')) > 0 AND ISNULL(@bitOppOrderExists,0) <> 1 AND (SELECT COUNT(*) FROM @TempTable) > 0
	BEGIN
		-- DELETE PREVIOUS ITEM ATTRIBUTES
		DELETE FROM ItemAttributes WHERE numDomainID=@numDomainID AND numItemCode=@numItemCode

		-- INSERT NEW ITEM ATTRIBUTES
		INSERT INTO ItemAttributes
		(
			numDomainID
			,numItemCode
			,FLD_ID
			,FLD_Value
		)
		SELECT
			@numDomainID
			,@numItemCode
			,Fld_ID
			,Fld_Value
		FROM
			@TempTable

		IF (SELECT COUNT(*) FROM WarehouseItems WHERE numDomainID=@numDomainID AND numItemID=@numItemCode) > 0
		BEGIN

			-- DELETE PREVIOUS WAREHOUSE ATTRIBUTES
			DELETE FROM CFW_Fld_Values_Serialized_Items WHERE RecId IN (SELECT ISNULL(numWarehouseItemID,0) FROM WarehouseItems WHERE numDomainID=@numDomainID AND numItemID=@numItemCode)

			-- INSERT NEW WAREHOUSE ATTRIBUTES
			INSERT INTO CFW_Fld_Values_Serialized_Items
			(
				Fld_ID,
				Fld_Value,
				RecId,
				bitSerialized
			)                                                  
			SELECT 
				Fld_ID,
				ISNULL(CAST(Fld_Value AS VARCHAR),'') AS Fld_Value,
				TEMPWarehouse.numWarehouseItemID,
				0 
			FROM 
				@TempTable  
			OUTER APPLY
			(
				SELECT
					numWarehouseItemID
				FROM 
					WarehouseItems 
				WHERE 
					numDomainID=@numDomainID 
					AND numItemID=@numItemCode
			) AS TEMPWarehouse
		END
	END	  
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH        
END
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ManageNameTemplate' ) 
    DROP PROCEDURE USP_ManageNameTemplate
GO
CREATE PROCEDURE USP_ManageNameTemplate
    @numDomainID AS NUMERIC,
    @strItems TEXT,
    @tintMode AS TINYINT,
    @tintModuleID AS TINYINT 
AS 
BEGIN
    IF @tintMode = 0 
        BEGIN
			IF @tintModuleID=1 --SO/PO
			BEGIN
				/*insert if doesn't exist*/
            IF NOT EXISTS ( SELECT  * FROM    dbo.NameTemplate WHERE   numDomainID = @numDomainID AND tintModuleID=@tintModuleID) 
                BEGIN
                    INSERT  INTO dbo.NameTemplate ( numDomainID,vcModuleName,vcNameTemplate,tintModuleID,numSequenceId,numMinLength,numRecordID)
                            SELECT  @numDomainID,vcModuleName,vcNameTemplate,tintModuleID,numSequenceId,numMinLength,numRecordID
                            FROM    dbo.NameTemplate WHERE   numDomainID = 0 AND tintModuleID=@tintModuleID 	
                END
                
				SELECT numDomainID,vcModuleName,vcNameTemplate,tintModuleID,ISNULL(numSequenceId,1) AS numSequenceId,ISNULL(numMinLength,4) AS numMinLength,numRecordID 
						FROM dbo.NameTemplate WHERE numDomainID = @numDomainID AND tintModuleID=@tintModuleID 
			END
			ELSE IF @tintModuleID=2 --BizDocs
			BEGIN
			        SELECT NT.numDomainID,LD.numListItemID AS numRecordID,vcData AS vcModuleName,ISNULL(NT.vcNameTemplate,CASE WHEN LD.numListItemID=290 THEN 'PROI' ELSE UPPER(SUBSTRING(dbo.GetListIemName(LD.numListItemID),0,4)) END + '-') AS vcNameTemplate,ISNULL(NT.numSequenceId,1) AS numSequenceId,ISNULL(NT.numMinLength,4) AS numMinLength 
					FROM dbo.ListDetails LD LEFT JOIN NameTemplate NT ON LD.numListItemID=NT.numRecordID AND NT.numDomainID=@numDomainID AND NT.tintModuleID=@tintModuleID 
					WHERE LD.numListID=27 and (LD.constFlag=1 or LD.numDomainID=@numDomainID)
			END
        END

    IF @tintMode = 1 
        BEGIN
            DELETE  FROM dbo.NameTemplate WHERE numDomainID=@numDomainID AND tintModuleID=@tintModuleID 

            DECLARE @hDocItem INT
            IF CONVERT(VARCHAR(10), @strItems) <> '' 
                BEGIN
                    EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems
            
                    INSERT  INTO dbo.NameTemplate ( numDomainID,vcModuleName,vcNameTemplate,tintModuleID,numSequenceId,numMinLength,numRecordID,dtCreatedDate)
                    
                            SELECT  @numDomainID,
                                    X.vcModuleName,
                                    X.vcNameTemplate,
                                    @tintModuleID,X.numSequenceId,
                                    X.numMinLength,X.numRecordID,GETUTCDATE()
                            FROM    ( SELECT    *
                                      FROM      OPENXML (@hDocItem, '/NewDataSet/Table', 2)
                                                WITH ( vcModuleName VARCHAR(50),vcNameTemplate VARCHAR(200),numSequenceId NUMERIC(9),numMinLength NUMERIC(9),numRecordID NUMERIC(9))
                                    ) X
                    EXEC sp_xml_removedocument @hDocItem
                END
            SELECT  ''
        END	
END



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ManageShareRecord' ) 
    DROP PROCEDURE USP_ManageShareRecord
GO
CREATE PROCEDURE [dbo].[USP_ManageShareRecord]
     @numDomainID Numeric(9),        
	 @numRecordID Numeric(9),      
	 @numModuleID tinyint ,    
	 @strXml   as text ='',
	 @tintMode AS TINYINT=0 
AS 
	IF @tintMode=0 --Select
	BEGIN
		SELECT CAST(A.numContactID AS VARCHAR(18)) +'~0' AS numContactID ,A.vcFirstName+' '+A.vcLastName as vcUserName          
			from UserMaster UM join AdditionalContactsInformation A  on UM.numUserDetailId=A.numContactID          
			where UM.numDomainID=@numDomainID  and UM.intAssociate=1 and UM.numDomainID=A.numDomainID AND A.numContactID NOT IN (SELECT numAssignedTo FROM ShareRecord WHERE numDomainID=@numDomainID AND numModuleID=@numModuleID AND numRecordID=@numRecordID)   
			order by vcUserName
		SELECT CAST(SR.numAssignedTo AS VARCHAR(18)) +'~'+ CAST(ISNULL(SR.numContactType,0) AS VARCHAR(18)) AS numContactID,
				A.vcFirstName+' '+A.vcLastName + CASE WHEN ISNULL(SR.numContactType,0)>0 THEN '(' + dbo.fn_GetListItemName(SR.numContactType) + ')' ELSE '' END as vcUserName
		 FROM ShareRecord SR JOIN UserMaster UM ON UM.numUserDetailId=SR.numAssignedTo 
							 JOIN AdditionalContactsInformation A ON UM.numUserDetailId=A.numContactID 
		 WHERE SR.numDomainID=@numDomainID AND SR.numModuleID=@numModuleID AND SR.numRecordID=@numRecordID
				AND UM.numDomainID=@numDomainID and UM.numDomainID=A.numDomainID
		order by vcUserName
	END
	ELSE IF @tintMode=1 --Insert
	BEGIN
			delete from ShareRecord where numDomainID=@numDomainID and numRecordID= @numRecordID  AND numModuleID=@numModuleID    
			    
			 DECLARE @hDoc1 int                                      
			 EXEC sp_xml_preparedocument @hDoc1 OUTPUT, @strXml                                      

			 insert into ShareRecord(numDomainID,numRecordID,numAssignedTo,numContactType,numModuleID)    
					select @numDomainID,@numRecordID,X.numAssignedTo,X.numContactType,@numModuleID
						from (SELECT *FROM OPENXML (@hDoc1,'/NewDataSet/Table',2)                                      
							WITH (numAssignedTo numeric(9), numContactType numeric(9)))X                                     
			                                      
			 EXEC sp_xml_removedocument @hDoc1
	END

    
/****** Object:  StoredProcedure [dbo].[USP_ManageTabsInCuSFields]    Script Date: 07/26/2008 16:20:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_managetabsincusfields')
DROP PROCEDURE usp_managetabsincusfields
GO
CREATE PROCEDURE [dbo].[USP_ManageTabsInCuSFields]      
@byteMode as tinyint=0,      
@LocID as numeric(9)=0,      
@TabName as varchar(50)='',      
@TabID as numeric(9)=0  ,    
--@vcURL as varchar(1000)='',  
@numDomainID as numeric(9),
@vcURL as varchar(100)=''    
as      
      
      
if @byteMode=1      
begin      
insert into  CFw_Grp_Master(Grp_Name,Loc_Id,numDomainID,tintType)       
values(@TabName,@LocID,@numDomainID,0)      
      
end      
      
if @byteMode=2      
begin      
update  CFw_Grp_Master set Grp_Name=@TabName       
where Grp_id=@TabID      
      
end      
      
if @byteMode=3      
begin      
--Check if any custom fields associated with subtab 
IF (SELECT COUNT(*) FROM cfw_fld_master WHERE [subgrp]= @TabID)>0
BEGIN
	  RAISERROR ('CHILD_RECORD_FOUND',16,1);
	RETURN 
END
--delete permission
--[tintType]=1=subtab 
DELETE FROM [GroupTabDetails] WHERE [numTabId]=@TabID AND [tintType]=1
--delete subtabs
delete from  CFw_Grp_Master        
where Grp_id=@TabID  
    
      
end     
   
if @byteMode=4      
begin      
insert into  CFw_Grp_Master(Grp_Name,Loc_Id,numDomainID,tintType,vcURLF)       
values(@TabName,@LocID,@numDomainID,1,@vcURL)      
      
END
--Added by chintan, this mode will add existing subtabs with tintType=2 which can not be deleted  
if @byteMode=5 
begin      
insert into  CFw_Grp_Master(Grp_Name,Loc_Id,numDomainID,tintType,vcURLF)       
values(@TabName,@LocID,@numDomainID,2,@vcURL)      
      
END
--Added by chintan, this mode will add Default subtabs and give permission to all Roles by default
-- Only required Parameter is DomainID
IF @byteMode = 6 
    BEGIN      
        IF ( SELECT COUNT(*) FROM   domain WHERE  numDomainID = @numDomainID ) > 0 
        BEGIN
			DECLARE @i NUMERIC(9)	

			--ContactDetails
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=4,@TabName='Contact Details',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=4,@TabName='Areas of Interest',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=4,@TabName='Opportunities',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=4,@TabName='Survey History',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=4,@TabName='Correspondence',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			--SO
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=2,@TabName='Deal Details',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=2,@TabName='Milestones & Stages',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=2,@TabName='Associated Contacts',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=2,@TabName='Product/Service',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=2,@TabName='BizDocs',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=2,@TabName='Correspondence',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			--Added By Manish || To add Packaging/Shipping in Orders
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=2,@TabName='Shipping & Packaging',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			
			--PO
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=6,@TabName='Deal Details',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=6,@TabName='Milestones & Stages',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=6,@TabName='Associated Contacts',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=6,@TabName='Product/Service',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=6,@TabName='BizDocs',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=6,@TabName='Correspondence',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			--Added By Manish || To add Packaging/Shipping in Orders
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=6,@TabName='Shipping & Packaging',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			
			-- Case details
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=3,@TabName='Case Details',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=3,@TabName='Associated Contacts',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=3,@TabName='Correspondence',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			-- Project details
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=11,@TabName='Project Details',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=11,@TabName='Milestones And Stages',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=11,@TabName='Associated Contacts',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=11,@TabName='Project Income & Expense',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=11,@TabName='Correspondence',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			-- Leads locid has been changed from 1 to 14
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=14,@TabName='Lead Detail',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=14,@TabName='Areas of Interest',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=14,@TabName='Web Analysis',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=14,@TabName='Correspondence',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=14,@TabName='Assets',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=14,@TabName='Associations',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=14,@TabName='Survey History',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			-- Prospects
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=12,@TabName='Prospect Details',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=12,@TabName='Contacts',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=12,@TabName='Transactions',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=12,@TabName='Projects',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=12,@TabName='Accounting',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=12,@TabName='Web Analysis',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=12,@TabName='Correspondence',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=12,@TabName='Assets',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=12,@TabName='Associations',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			-- Accounts
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=13,@TabName='Account Details',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=13,@TabName='Contacts',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=13,@TabName='Transactions',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=13,@TabName='Projects',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=13,@TabName='Cases',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=13,@TabName='Accounting',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=13,@TabName='Web Analysis',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=13,@TabName='Correspondence',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=13,@TabName='Assets',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=13,@TabName='Contracts',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=13,@TabName='Associations',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			
			--Added By Sachin ||Issue raised by Customer:EasternBikes
			--Reason:To Make default entry for any new subscription
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=13,@TabName='Items',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=15,@TabName='Bought & Sold',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=15,@TabName='Vended',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			--end of code		
			--Added By Sachin||Issue raised by Customer:Coloroda State Uni
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=2,@TabName='Shipping & Packaging',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
            exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=6,@TabName='Shipping & Packaging',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			--end of Code

			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=17,@TabName='Action Items & Meetings',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=17,@TabName='Opportunities & Projects',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=17,@TabName='Cases',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=17,@TabName='Approval Requests',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			/*********Get Auth groups */
			---- Give permission by default
				DECLARE  @minGroupID NUMERIC(9)
				DECLARE  @maxGroupID NUMERIC(9)
				SELECT @minGroupID=MIN(numGroupID) FROM AuthenticationGroupMaster WHERE [numDomainID]=@numDomainID
				SELECT @maxGroupID=MAX(numGroupID) FROM AuthenticationGroupMaster WHERE [numDomainID]=@numDomainID
				WHILE  @minGroupID <= @maxGroupID
				  BEGIN
					PRINT 'GroupID=' + CONVERT(VARCHAR(20),@minGroupID)
							Set @i=0
							DECLARE  @minTabID NUMERIC(9)
							DECLARE  @maxTabID NUMERIC(9)
							SELECT @minTabID=MIN([Grp_id]) FROM [CFw_Grp_Master] WHERE [numDomainID]=@numDomainID 
							SELECT @maxTabID=max([Grp_id]) FROM [CFw_Grp_Master] WHERE [numDomainID]=@numDomainID 
							WHILE @minTabID<=@maxTabID
							BEGIN
								PRINT 'TabID=' + CONVERT(VARCHAR(20),@minTabID)
								---------------
								insert into GroupTabDetails
								(numGroupId,numTabId,bitallowed,numOrder,numRelationShip,tintType)
								VALUES (@minGroupID,@minTabID,1,@i+1,0,1)

								---------------
								SELECT @minTabID=MIN([Grp_id]) FROM [CFw_Grp_Master] WHERE [numDomainID]=@numDomainID AND [Grp_id]>@minTabID
							END
					
					SELECT @minGroupID = min(numGroupID)
					FROM AuthenticationGroupMaster WHERE [numDomainID]= @numDomainID AND [numGroupID] > @minGroupID
				  END		


        END
      
    END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageWorkFlowMaster')
DROP PROCEDURE USP_ManageWorkFlowMaster
GO
Create PROCEDURE [dbo].[USP_ManageWorkFlowMaster]                                        
	@numWFID numeric(18, 0) OUTPUT,
    @vcWFName varchar(200),
    @vcWFDescription varchar(500),   
    @numDomainID numeric(18, 0),
    @numUserCntID numeric(18, 0),
    @bitActive bit,
    @numFormID NUMERIC(18,0),
    @tintWFTriggerOn NUMERIC(18),
    @vcWFAction varchar(Max),
    @vcDateField VARCHAR(MAX),
    @intDays INT,
    @intActionOn INT,
	@bitCustom bit,
	@numFieldID numeric(18),
    @strText TEXT = ''
as                 
DECLARE @ErrorMessage NVARCHAR(4000);SET @ErrorMessage=''
DECLARE @ErrorSeverity INT;
DECLARE @ErrorState INT;
		
BEGIN TRANSACTION;

BEGIN TRY
	DECLARE @hDocItems INT                                                                                                                                                                
	EXEC sp_xml_preparedocument @hDocItems OUTPUT, @strText


	DECLARE @TEMP TABLE
	(
		tintActionType TINYINT,
		numBizDocTypeID NUMERIC(18,0)
	)
	INSERT INTO 
		@TEMP
	SELECT 
		tintActionType,
		numBizDocTypeID 
	FROM 
		OPENXML (@hDocItems, '/NewDataSet/WorkFlowActionList',2) 
	WITH 
		(
			tintActionType TINYINT, 
			numBizDocTypeID numeric(18,0)
		)

	IF (SELECT COUNT(*) FROM @TEMP WHERE tintActionType = 6 AND (numBizDocTypeID=287 OR numBizDocTypeID=29397)) > 0
		BEGIN
			--CHECK IF CREATE INVOICE OR CREATE PACKING SLIP RULE EXISTS IN MASS SALES FULLFILLMENT
			IF (
				SELECT 
					COUNT(*) 
				FROM 
					OpportunityAutomationRules 
				WHERE 
					numDomainID = @numDomainID AND 
					(numRuleID = 1 OR numRuleID = 2)
				) > 0
			BEGIN
				RAISERROR ('MASS SALES FULLFILLMENT RULE EXISTS',16,1);
				RETURN -1
			END
		END


	DECLARE @bitDiplicate BIT = 0

	-- FIRST CHECK IF WORKFLOW WITH SAME MODULE AND TRIGGER TYPE EXISTS
	IF EXISTS (SELECT numWFID FROM WorkFlowMaster WHERE numDomainID=@numDomainID AND numWFID <> @numWFID AND numFormID=@numFormID AND tintWFTriggerOn=@tintWFTriggerOn AND (SELECT COUNT(*) FROM WorkFlowConditionList WHERE WorkFlowConditionList.numWFID = WorkFlowMaster.numWFID) = (SELECT COUNT(*) FROM OPENXML (@hDocItems, '/NewDataSet/WorkFlowConditionList',2) WITH (numFieldID NUMERIC, bitCustom BIT, vcFilterValue VARCHAR(500), vcFilterOperator VARCHAR(10), vcFilterData VARCHAR(MAX))))
	BEGIN
		-- NOT CHECK IF CONDITIONS ARE ALSO SAME
		DECLARE @TEMPWF TABLE
		(
			ID INT IDENTITY(1,1)
			,numWFID NUMERIC(18,0)
		)
		INSERT INTO 
			@TEMPWF 
		SELECT 
			numWFID 
		FROM 
			WorkFlowMaster 
		WHERE 
			numDomainID=@numDomainID
			AND numWFID <> @numWFID
			AND numFormID=@numFormID 
			AND tintWFTriggerOn=@tintWFTriggerOn

		DECLARE @i AS INT = 1
		DECLARE @iCount AS INT
		DECLARE @numTempWFID NUMERIC(18,0)

		SELECT @iCount=COUNT(*) FROM @TEMPWF

		WHILE @i <= @iCount
		BEGIN
			SELECT @numTempWFID=numWFID FROM @TEMPWF WHERE ID=@i

			IF NOT EXISTS (
				SELECT 
					numFieldID,bitCustom,vcFilterValue,vcFilterOperator,vcFilterData 
				FROM 
				(
					SELECT 
						numFieldID
						,bitCustom
						,vcFilterValue
						,vcFilterOperator
						,Split.a.value('.', 'VARCHAR(100)') AS vcFilterData  
					FROM  
					(
						SELECT 
							numFieldID
							,bitCustom
							,vcFilterValue
							,vcFilterOperator
							,CAST ('<M>' + REPLACE(vcFilterData, ',', '</M><M>') + '</M>' AS XML) AS vcFilterData
						FROM 
							OPENXML (@hDocItems, '/NewDataSet/WorkFlowConditionList',2) 
						WITH 
							(numFieldID NUMERIC, bitCustom BIT, vcFilterValue VARCHAR(500), vcFilterOperator VARCHAR(10), vcFilterData VARCHAR(MAX))
					) AS A CROSS APPLY vcFilterData.nodes ('/M') AS Split(a)
				) T1
				EXCEPT
				SELECT 
					numFieldID,bitCustom,vcFilterValue,vcFilterOperator,vcFilterData 
				FROM 
				(
					SELECT 
						numFieldID
						,bitCustom
						,vcFilterValue
						,vcFilterOperator
						,Split.a.value('.', 'VARCHAR(100)') AS vcFilterData  
					FROM  
					(
						SELECT 
							numFieldID
							,bitCustom
							,vcFilterValue
							,vcFilterOperator
							,CAST ('<M>' + REPLACE(vcFilterData, ',', '</M><M>') + '</M>' AS XML) AS vcFilterData  
						FROM
							WorkFlowConditionList
						WHERE
							numWFID=@numTempWFID
					) AS A CROSS APPLY vcFilterData.nodes ('/M') AS Split(a)
				) T2
			)
			SELECT @bitDiplicate = 1 ELSE SELECT @bitDiplicate = 0


			IF NOT EXISTS (
				SELECT 
					numFieldID,bitCustom,vcFilterValue,vcFilterOperator,vcFilterData 
				FROM 
				(
					SELECT 
						numFieldID
						,bitCustom
						,vcFilterValue
						,vcFilterOperator
						,Split.a.value('.', 'VARCHAR(100)') AS vcFilterData  
					FROM  
					(
						SELECT 
							numFieldID
							,bitCustom
							,vcFilterValue
							,vcFilterOperator
							,CAST ('<M>' + REPLACE(vcFilterData, ',', '</M><M>') + '</M>' AS XML) AS vcFilterData  
						FROM
							WorkFlowConditionList
						WHERE
							numWFID=@numTempWFID
					) AS A CROSS APPLY vcFilterData.nodes ('/M') AS Split(a)
				) T2
				EXCEPT
				SELECT 
					numFieldID,bitCustom,vcFilterValue,vcFilterOperator,vcFilterData 
				FROM 
				(
					SELECT 
						numFieldID
						,bitCustom
						,vcFilterValue
						,vcFilterOperator
						,Split.a.value('.', 'VARCHAR(100)') AS vcFilterData  
					FROM  
					(
						SELECT 
							numFieldID
							,bitCustom
							,vcFilterValue
							,vcFilterOperator
							,CAST ('<M>' + REPLACE(vcFilterData, ',', '</M><M>') + '</M>' AS XML) AS vcFilterData
						FROM 
							OPENXML (@hDocItems, '/NewDataSet/WorkFlowConditionList',2) 
						WITH 
							(numFieldID NUMERIC, bitCustom BIT, vcFilterValue VARCHAR(500), vcFilterOperator VARCHAR(10), vcFilterData VARCHAR(MAX))
					) AS A CROSS APPLY vcFilterData.nodes ('/M') AS Split(a)
				) T1	
			)

			SELECT @bitDiplicate = 1 ELSE SELECT @bitDiplicate = 0

			IF @bitDiplicate = 1
			BEGIN
				BREAK
			END

			SET @i = @i + 1
		END
	END


	IF @bitDiplicate = 1
	BEGIN
		RAISERROR ('DUPLICATE_WORKFLOW',16,1);
		RETURN -1
	END

	IF @numWFID=0
	BEGIN
		INSERT INTO [dbo].[WorkFlowMaster] ([numDomainID], [vcWFName], [vcWFDescription], [numCreatedBy], [numModifiedBy], [dtCreatedDate], [dtModifiedDate], [bitActive], [numFormID], [tintWFTriggerOn],[vcWFAction],[vcDateField],[intDays],[intActionOn],[bitCustom],[numFieldID])
		SELECT @numDomainID, @vcWFName, @vcWFDescription, @numUserCntID, @numUserCntID, GETUTCDATE(), GETUTCDATE(), @bitActive, @numFormID, @tintWFTriggerOn,@vcWFAction,@vcDateField,@intDays,@intActionOn,@bitCustom,@numFieldID

		SET @numWFID=@@IDENTITY
		--start of my
		IF DATALENGTH(@strText)>2
		BEGIN                                                                                                         
			--Delete records into WorkFlowTriggerFieldList and Insert
			DELETE FROM WorkFlowTriggerFieldList WHERE  [numWFID] = @numWFID

			INSERT INTO dbo.WorkFlowTriggerFieldList (numWFID,numFieldID,bitCustom) 
				SELECT @numWFID,numFieldID,bitCustom
				  FROM OPENXML (@hDocItems, '/NewDataSet/WorkFlowTriggerFieldList',2) WITH ( numFieldID NUMERIC, bitCustom BIT )
	
	
			--Delete records into WorkFlowConditionList and Insert
			DELETE FROM WorkFlowConditionList WHERE  [numWFID] = @numWFID

			INSERT INTO dbo.WorkFlowConditionList (numWFID,numFieldID,bitCustom,vcFilterValue,vcFilterOperator,vcFilterANDOR,vcFilterData,intCompare) 
				SELECT @numWFID,numFieldID,bitCustom,vcFilterValue,vcFilterOperator,vcFilterANDOR,vcFilterData,intCompare
				  FROM OPENXML (@hDocItems, '/NewDataSet/WorkFlowConditionList',2) WITH ( numFieldID NUMERIC, bitCustom BIT, vcFilterValue VARCHAR(500), vcFilterOperator VARCHAR(10),vcFilterANDOR VARCHAR(10),vcFilterData VARCHAR(MAX),intCompare int)


			--Delete records into WorkFlowActionUpdateFields and Insert
			DELETE FROM WorkFlowActionUpdateFields WHERE numWFActionID IN(SELECT numWFActionID FROM WorkFlowActionList WHERE numWFID=@numWFID)

			--Delete records into WorkFlowActionList and Insert
			DELETE FROM WorkFlowActionList WHERE  [numWFID] = @numWFID

			INSERT INTO dbo.WorkFlowActionList (numWFID,tintActionType,numTemplateID,vcEmailToType,tintTicklerActionAssignedTo,vcAlertMessage,tintActionOrder,tintIsAttachment,vcApproval,tintSendMail,numBizDocTypeID,numBizDocTemplateID,vcSMSText,vcEmailSendTo,vcMailBody,vcMailSubject) 
				SELECT @numWFID,tintActionType,numTemplateID,vcEmailToType,tintTicklerActionAssignedTo,vcAlertMessage,tintActionOrder,tintIsAttachment,vcApproval ,tintSendMail,numBizDocTypeID,numBizDocTemplateID,vcSMSText,vcEmailSendTo,vcMailBody ,vcMailSubject
				  FROM OPENXML (@hDocItems, '/NewDataSet/WorkFlowActionList',2) WITH ( tintActionType TINYINT, numTemplateID NUMERIC,vcEmailToType VARCHAR(50),tintTicklerActionAssignedTo TINYINT,vcAlertMessage NTEXT,tintActionOrder TINYINT,tintIsAttachment TINYINT,vcApproval VARCHAR(100),tintSendMail TINYINT,numBizDocTypeID numeric(18,0),numBizDocTemplateID numeric(18,0),vcSMSText NTEXT,vcEmailSendTo varchar(500),vcMailBody ntext,vcMailSubject varchar(500))

			INSERT INTO dbo.WorkFlowActionUpdateFields (numWFActionID,tintModuleType,numFieldID,bitCustom,vcValue,vcData) 
				SELECT WFA.numWFActionID,tintModuleType,numFieldID,bitCustom,vcValue,vcData
				  FROM OPENXML (@hDocItems, '/NewDataSet/WorkFlowActionUpdateFields',2) WITH ( numWFActionID NUMERIC, tintModuleType NUMERIC,numFieldID NUMERIC,bitCustom BIT,vcValue VARCHAR(500),vcData VARCHAR(500) ) WFAUF
				  JOIN WorkFlowActionList WFA ON WFA.tintActionOrder=WFAUF.numWFActionID WHERE WFA.numWFID=@numWFID

			EXEC sp_xml_removedocument @hDocItems
		END	
		--end of my
	END
	ELSE
	BEGIN
	
		UPDATE [dbo].[WorkFlowMaster]
		SET    [vcWFName] = @vcWFName, [vcWFDescription] = @vcWFDescription, [numModifiedBy] = @numUserCntID, [dtModifiedDate] = GETUTCDATE(), [bitActive] = @bitActive, [numFormID] = @numFormID, [tintWFTriggerOn] = @tintWFTriggerOn,[vcWFAction]=@vcWFAction,[vcDateField]=@vcDateField,[intDays]=@intDays,[intActionOn]=@intActionOn,[bitCustom]=@bitCustom,[numFieldID]=@numFieldID
		WHERE  [numDomainID] = @numDomainID AND [numWFID] = @numWFID
	
		IF DATALENGTH(@strText)>2
		BEGIN
			DECLARE @hDocItem INT                                                                                                                                                                
	
			EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strText                                                                                                             
        
			--Delete records into WorkFlowTriggerFieldList and Insert
			DELETE FROM WorkFlowTriggerFieldList WHERE  [numWFID] = @numWFID

			INSERT INTO dbo.WorkFlowTriggerFieldList (numWFID,numFieldID,bitCustom) 
				SELECT @numWFID,numFieldID,bitCustom
				  FROM OPENXML (@hDocItem, '/NewDataSet/WorkFlowTriggerFieldList',2) WITH ( numFieldID NUMERIC, bitCustom BIT )
	
	
			--Delete records into WorkFlowConditionList and Insert
			DELETE FROM WorkFlowConditionList WHERE  [numWFID] = @numWFID

			INSERT INTO dbo.WorkFlowConditionList (numWFID,numFieldID,bitCustom,vcFilterValue,vcFilterOperator,vcFilterANDOR,vcFilterData,intCompare) 
				SELECT @numWFID,numFieldID,bitCustom,vcFilterValue,vcFilterOperator,vcFilterANDOR,vcFilterData,intCompare
				  FROM OPENXML (@hDocItem, '/NewDataSet/WorkFlowConditionList',2) WITH ( numFieldID NUMERIC, bitCustom BIT, vcFilterValue VARCHAR(500), vcFilterOperator VARCHAR(10),vcFilterANDOR VARCHAR(10),vcFilterData varchar(MAX),intCompare int)


			--Delete records into WorkFlowActionUpdateFields and Insert
			DELETE FROM WorkFlowActionUpdateFields WHERE numWFActionID IN(SELECT numWFActionID FROM WorkFlowActionList WHERE numWFID=@numWFID)

			--Delete records into WorkFlowActionList and Insert
			DELETE FROM WorkFlowActionList WHERE  [numWFID] = @numWFID

			INSERT INTO dbo.WorkFlowActionList (numWFID,tintActionType,numTemplateID,vcEmailToType,tintTicklerActionAssignedTo,vcAlertMessage,tintActionOrder,tintIsAttachment,vcApproval ,tintSendMail,numBizDocTypeID ,numBizDocTemplateID,vcSMSText,vcEmailSendTo,vcMailBody,vcMailSubject ) 
				SELECT @numWFID,tintActionType,numTemplateID,vcEmailToType,tintTicklerActionAssignedTo,vcAlertMessage,tintActionOrder,tintIsAttachment,vcApproval ,tintSendMail,numBizDocTypeID ,numBizDocTemplateID,vcSMSText ,vcEmailSendTo,vcMailBody,vcMailSubject
				  FROM OPENXML (@hDocItem, '/NewDataSet/WorkFlowActionList',2) WITH ( tintActionType TINYINT, numTemplateID NUMERIC,vcEmailToType VARCHAR(50),tintTicklerActionAssignedTo TINYINT,vcAlertMessage NTEXT,tintActionOrder TINYINT,tintIsAttachment TINYINT,vcApproval VARCHAR(100),tintSendMail TINYINT,numBizDocTypeID numeric(18,0),numBizDocTemplateID numeric(18,0),vcSMSText NTEXT ,vcEmailSendTo varchar(500),vcMailBody ntext,vcMailSubject varchar(500))

			INSERT INTO dbo.WorkFlowActionUpdateFields (numWFActionID,tintModuleType,numFieldID,bitCustom,vcValue,vcData) 
				SELECT WFA.numWFActionID,tintModuleType,numFieldID,bitCustom,vcValue,vcData
				  FROM OPENXML (@hDocItem, '/NewDataSet/WorkFlowActionUpdateFields',2) WITH ( numWFActionID NUMERIC, tintModuleType NUMERIC,numFieldID NUMERIC,bitCustom BIT,vcValue VARCHAR(500),vcData VARCHAR(500)) WFAUF
				  JOIN WorkFlowActionList WFA ON WFA.tintActionOrder=WFAUF.numWFActionID WHERE WFA.numWFID=@numWFID

			EXEC sp_xml_removedocument @hDocItem
		END	
	END

	IF EXISTS (SELECT * FROM SalesFulfillmentConfiguration WHERE numDomainID=@numDomainID AND bitActive=1)
	BEGIN
		DECLARE @numRule2OrderStatus NUMERIC(18,0)
		DECLARE @numRule3OrderStatus NUMERIC(18,0)
		DECLARE @numRule4OrderStatus NUMERIC(18,0)

		SELECT
			@numRule2OrderStatus= (CASE WHEN bitRule2IsActive = 1 THEN numRule2OrderStatus ELSE 0 END)
			,@numRule3OrderStatus= (CASE WHEN bitRule3IsActive = 1 THEN numRule3OrderStatus ELSE 0 END)
			,@numRule4OrderStatus= (CASE WHEN bitRule3IsActive = 1 THEN numRule4OrderStatus ELSE 0 END)
		FROM
			SalesFulfillmentConfiguration
		WHERE 
			numDomainID=@numDomainID 
			AND bitActive=1

		IF EXISTS (SELECT * FROM WorkFlowConditionList WHERE numFieldID=101 AND vcFilterValue IN (@numRule2OrderStatus,@numRule3OrderStatus,@numRule4OrderStatus))
		BEGIN
			RAISERROR ('MASS SALES FULLFILLMENT RULE EXISTS',16,1);
			RETURN -1
		END
	END

	COMMIT
END TRY
BEGIN CATCH

	SELECT @ErrorMessage = ERROR_MESSAGE(), @ErrorSeverity = ERROR_SEVERITY(), @ErrorState = ERROR_STATE();

	IF @@TRANCOUNT > 0
	ROLLBACK TRANSACTION;

	-- Use RAISERROR inside the CATCH block to return error
	-- information about the original error that caused
	-- execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorState);
END CATCH;
/****** Object:  StoredProcedure [dbo].[USP_OppBizDocs]    Script Date: 03/25/2009 15:23:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_oppbizdocs')
DROP PROCEDURE usp_oppbizdocs
GO
CREATE PROCEDURE [dbo].[USP_OppBizDocs]
(                        
 @byteMode as tinyint=null,                        
 @numOppId as numeric(9)=null,                        
 @numOppBizDocsId as numeric(9)=null,
 @ClientTimeZoneOffset AS INT=0,
 @numDomainID AS NUMERIC(18,0	) = NULL,
 @numUserCntID AS NUMERIC(18,0) = NULL
)                        
as      

Declare @ParentBizDoc AS NUMERIC(9)=0
DECLARE @lngJournalId AS NUMERIC(18,0)

SET @ParentBizDoc=(SELECT TOP 1 numOppBizDocsId FROM OpportunityBizDocs WHERE numSourceBizDocId= @numOppBizDocsId)     
IF @ParentBizDoc>0
BEGIN	
	IF @byteMode = 1
	BEGIN
		SET @lngJournalId=(Select TOP 1 GJH.numJournal_Id as numJournalId from General_Journal_Header GJH Where GJH.numOppId=@numOppId And GJH.numOppBizDocsId=@ParentBizDoc)
		IF(@lngJournalId>0)
		BEGIN
			exec USP_DeleteJournalDetails @numDomainID=@numDomainID,@numJournalID=@lngJournalId,@numBillPaymentID=0,@numDepositID=0,@numCheckHeaderID=0,@numBillID=0,@numCategoryHDRID=0,@numReturnID=0,@numUserCntID=0
		END
		exec DeleteComissionDetails @numDomainId=@numDomainID,@numOppBizDocID=@ParentBizDoc       
	END
	exec USP_OppBizDocs @byteMode=@byteMode,@numOppId=@numOppId,@numOppBizDocsId=@ParentBizDoc,@ClientTimeZoneOffset=@ClientTimeZoneOffset,@numDomainID=@numDomainID,@numUserCntID=@numUserCntID
END      
SELECT 
	ISNULL(CompanyInfo.numCompanyType,0) AS numCompanyType,
	ISNULL(CompanyInfo.vcProfile,0) AS vcProfile,
	ISNULL(OpportunityMaster.numAccountClass,0) AS numAccountClass
FROM 
	OpportunityMaster 
JOIN 
	DivisionMaster 
ON 
	DivisionMaster.numDivisionId = OpportunityMaster.numDivisionId
JOIN 
	CompanyInfo
ON 
	CompanyInfo.numCompanyId = DivisionMaster.numCompanyID
WHERE 
	numOppId = @numOppId
		                                  
if @byteMode= 1                        
begin 
BEGIN TRY
BEGIN TRANSACTION
DECLARE @numBizDocID AS INT
DECLARE @bitFulfilled AS BIT
Declare @tintOppType as tinyint
DECLARE @tintShipped AS BIT
DECLARE @bitAuthoritativeBizDoc AS BIT

SELECT @tintOppType=tintOppType,@tintShipped=ISNULL(tintshipped,0) FROM OpportunityMaster WHERE  numOppId=@numOppId  and numDomainID= @numDomainID 
SELECT @numBizDocID = numBizDocId, @bitFulfilled=bitFulfilled, @bitAuthoritativeBizDoc=ISNULL(bitAuthoritativeBizDocs,0) FROM OpportunityBizDocs WHERE numOppBizDocsId = @numOppBizDocsId

IF ISNULL(@tintShipped,0) = 1 AND (@numBizDocID=296 OR ISNULL(@bitAuthoritativeBizDoc,0)=1)
BEGIN
	RAISERROR('NOT_ALLOWED_TO_DELETE_AUTHORITATIVE_BIZDOC_AFTER_ORDER_IS_CLOSED',16,1)
	RETURN
END

--IF it's as sales order and BizDoc is of type of fulfillment and already fulfilled revert items to allocations
IF @tintOppType = 1 AND @numBizDocID = 296 AND @bitFulfilled = 1
BEGIN
	EXEC USP_OpportunityBizDocs_RevertFulFillment @numDomainID,@numUserCntID,@numOppId,@numOppBizDocsId
END


DELETE FROM [ProjectsOpportunities] WHERE numOppBizDocID=@numOppBizDocsId

DELETE FROM [ShippingReportItems] WHERE [numShippingReportId] IN  (SELECT numShippingReportId from [ShippingReport] WHERE [numOppBizDocId] = @numOppBizDocsId)
DELETE FROM [ShippingBox] WHERE [numShippingReportId] IN  (SELECT numShippingReportId from [ShippingReport] WHERE [numOppBizDocId] = @numOppBizDocsId)
DELETE FROM [ShippingReport] WHERE [numOppBizDocId] = @numOppBizDocsId

 --Delete All entries for current bizdoc
EXEC [USP_DeleteEmbeddedCost] @numOppBizDocsId, 0, @numDomainID 
--DELETE FROM [OpportunityBizDocsPaymentDetails] WHERE [numBizDocsPaymentDetId] IN (SELECT  [numBizDocsPaymentDetId] FROM [OpportunityBizDocsDetails] WHERE [numBizDocsPaymentDetId] IN (SELECT [numBizDocsPaymentDetId] FROM [EmbeddedCost] WHERE [numOppBizDocID]=@numOppBizDocsId))
--DELETE FROM [EmbeddedCostItems] WHERE [numEmbeddedCostID] IN (SELECT [numEmbeddedCostID] FROM [EmbeddedCost] WHERE [numOppBizDocID] = @numOppBizDocsId)
--DELETE FROM [EmbeddedCost] WHERE [numOppBizDocID] = @numOppBizDocsId
--DELETE FROM [OpportunityBizDocsDetails] WHERE [numBizDocsPaymentDetId] IN (SELECT [numBizDocsPaymentDetId] FROM [EmbeddedCost] WHERE [numOppBizDocID]=@numOppBizDocsId)


delete  from OpportunityBizDocItems where   numOppBizDocID=  @numOppBizDocsId              

--Delete Deferred BizDoc Recurring Entries
delete from OpportunityRecurring where numOppBizDocID=@numOppBizDocsId and tintRecurringType=4

UPDATE dbo.OpportunityRecurring SET numOppBizDocID=NULL WHERE numOppBizDocID=@numOppBizDocsId

DELETE FROM [RecurringTransactionReport] WHERE [numRecTranBizDocID] = @numOppBizDocsId

--Credit Balance
Declare @monCreditAmount as money;Set @monCreditAmount=0
Declare @numDivisionID as numeric(9);Set @numDivisionID=0

Select @monCreditAmount=isnull(oppBD.monCreditAmount,0),@numDivisionID=Om.numDivisionID,
@numOppId=OM.numOppId 
from OpportunityBizDocs oppBD join OpportunityMaster Om on OM.numOppId=oppBD.numOppId WHERE  numOppBizDocsId = @numOppBizDocsId

delete from [CreditBalanceHistory] where numOppBizDocsId=@numOppBizDocsId

 
IF @tintOppType=1 --Sales
	update DivisionMaster set monSCreditBalance=isnull(monSCreditBalance,0) + isnull(@monCreditAmount,0) where numDivisionID=@numDivisionID and numDomainID=@numDomainID
else IF @tintOppType=2 --Purchase
	update DivisionMaster set monPCreditBalance=isnull(monPCreditBalance,0) + isnull(@monCreditAmount,0) where numDivisionID=@numDivisionID and numDomainID=@numDomainID


delete from DocumentWorkflow where numDocID=@numOppBizDocsId and cDocType='B'      

delete from BizDocAction where numBizActionId in(select numBizActionId from BizActionDetails where numOppBizDocsId =@numOppBizDocsId and btDocType=1) 
delete from BizActionDetails where numOppBizDocsId =@numOppBizDocsId and btDocType=1


  DELETE FROM dbo.General_Journal_Details WHERE numJournalId IN (SELECT numJournal_Id FROM dbo.General_Journal_Header WHERE numOppId=@numOppId AND numOppBizDocsId=@numOppBizDocsId AND numDomainId=@numDomainID)
  DELETE FROM dbo.General_Journal_Header WHERE numOppId=@numOppId AND numOppBizDocsId=@numOppBizDocsId AND numDomainId= @numDomainID


delete from OpportunityBizDocs where numOppBizDocsId=@numOppBizDocsId                        

COMMIT TRANSACTION
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH             
end                        
                        
if @byteMode= 2                        
begin  

DECLARE @BaseCurrencySymbol nVARCHAR(3);SET @BaseCurrencySymbol='$'
SELECT @BaseCurrencySymbol = ISNULL(varCurrSymbol,'$') FROM dbo.Currency 
														   WHERE numCurrencyID IN ( SELECT numCurrencyID FROM dbo.Domain 
																					WHERE numDomainId=@numDomainId)
--PRINT @BaseCurrencySymbol

																					                      
select *, isnull(X.monPAmount,0)-X.monAmountPaid as BalDue from (select  opp.numOppBizDocsId,                        
 opp.numOppId,                        
 opp.numBizDocId,                        
 ISNULL((SELECT TOP 1 numOrientation FROM BizDocTemplate WHERE numBizDocTempID = Opp.numBizDocTempID),0) AS numOrientation, 
 ISNULL((SELECT TOP 1 vcTemplateName FROM BizDocTemplate WHERE numBizDocTempID = Opp.numBizDocTempID),0) AS vcTemplateName, 
 ISNULL((SELECT TOP 1 bitKeepFooterBottom FROM BizDocTemplate WHERE numBizDocTempID = Opp.numBizDocTempID),0) AS bitKeepFooterBottom,                  
 oppmst.vcPOppName,                        
 mst.vcData as BizDoc,                        
 opp.numCreatedBy,                        
DATEADD(minute,-@ClientTimeZoneOffset,opp.dtCreatedDate) CreatedDate,
 opp.vcRefOrderNo,                       
 vcBizDocID,
 CASE WHEN LEN(ISNULL(vcBizDocName,''))=0 THEN vcBizDocID ELSE vcBizDocName END vcBizDocName,      
  [dbo].[GetDealAmount](@numOppId ,getutcdate(),opp.numOppBizDocsId ) as monPAmount,
  CASE 
	WHEN tintOppType=1  
	THEN (CASE 
			WHEN (ISNULL(numAuthoritativeSales,0)=numBizDocId OR numBizDocId=296) 
			THEN 'Authoritative' + CASE WHEN ISNULL(Opp.tintDeferred,0)=1 THEN ' (Deferred)' ELSE '' END 
			WHEN opp.numBizDocId =  304 THEN 'Deferred-Authoritative'
			ELSE 'Non-Authoritative' END ) 
  when tintOppType=2  then (case when isnull(numAuthoritativePurchase,0)=numBizDocId then 'Authoritative' + Case when isnull(Opp.tintDeferred,0)=1 then ' (Deferred)' else '' end else 'Non-Authoritative' end )  end as BizDocType,
  isnull(monAmountPaid,0) monAmountPaid, 
  ISNULL(C.varCurrSymbol,'') varCurrSymbol ,
  ISNULL(Opp.[bitAuthoritativeBizDocs],0) bitAuthoritativeBizDocs,
  ISNULL(oppmst.[tintshipped] ,0) tintshipped,isnull(opp.numShipVia,0) as numShipVia,oppmst.numDivisionId,isnull(Opp.tintDeferred,0) as tintDeferred,
  ISNULL(Opp.numBizDocTempID,0) AS numBizDocTempID,con.numContactid,isnull(con.vcEmail,'') AS vcEmail,ISNULL(Opp.numBizDocStatus,0) AS numBizDocStatus,
--  case when isnumeric(ISNULL(dbo.fn_GetListItemName(isnull(oppmst.intBillingDays,0)),0))=1 
--	   then ISNULL(dbo.fn_GetListItemName(isnull(oppmst.intBillingDays,0)),0) 
--	   else 0 
--  end AS numBillingDaysName,
  CASE WHEN ISNUMERIC((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(oppmst.intBillingDays,0))) = 1
 	   THEN (SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(oppmst.intBillingDays,0))
	   ELSE 0
  END AS numBillingDaysName,
  Opp.dtFromDate,
  opp.dtFromDate AS BillingDate,
  ISNULL(C.fltExchangeRate,1) fltExchangeRateCurrent,
  ISNULL(oppmst.fltExchangeRate,1) fltExchangeRateOfOrder,
  @BaseCurrencySymbol BaseCurrencySymbol,0 AS numReturnHeaderID,0 AS tintReturnType,ISNULL(Opp.fltExchangeRateBizDoc,ISNULL(oppmst.fltExchangeRate,1)) AS fltExchangeRateBizDoc,
  CASE WHEN (SELECT COUNT(*) FROM dbo.ShippingReport WHERE numOppBizDocId = opp.numOppBizDocsId AND numDomainID = @numDomainID) > 0 THEN 1 ELSE 0 END AS [IsShippingReportGenerated],
  --CASE WHEN (SELECT ISNULL(vcTrackingNo,'') FROM dbo.OpportunityBizDocs WHERE numOppBizDocsId = opp.numOppBizDocsId) <> '' THEN 1 ELSE 0 END AS [ISTrackingNumGenerated]
  CASE WHEN (SELECT COUNT(*) FROM dbo.ShippingBox 
			 WHERE numShippingReportId IN (SELECT numShippingReportId FROM dbo.ShippingReport WHERE numOppBizDocId = opp.numOppBizDocsId AND numDomainID = @numDomainID)
			 AND LEN(ISNULL(vcTrackingNumber,'')) > 0) > 0 THEN 1 ELSE 0 END AS [ISTrackingNumGenerated],
  ISNULL((SELECT MAX(numShippingReportId) from ShippingReport SR 
		  WHERE SR.numOppBizDocId = opp.numoppbizdocsid 
		  AND SR.numDomainID = oppmst.numDomainID),0) AS numShippingReportId,
  ISNULL((SELECT MAX(SB.numShippingReportId) FROM [ShippingBox] SB 
		  JOIN dbo.ShippingReport SR ON SB.numShippingReportId = SR.numShippingReportId 
		  where SR.numOppBizDocId = opp.numoppbizdocsid AND SR.numDomainID = oppmst.numDomainID 
		  AND LEN(ISNULL(SB.vcTrackingNumber,''))>0),0) AS numShippingLabelReportId			 
 ,ISNULL(STUFF(( SELECT  ',' +  CASE WHEN LEN(ISNULL(W.[vcWareHouse],'')  + ':' + ISNULL([WL].[vcLocation],'')) > 0 THEN ISNULL(W.[vcWareHouse],'')  + ':' + ISNULL([WL].[vcLocation],'') ELSE '' END
                FROM    [dbo].[WarehouseLocation] AS WL
				JOIN [dbo].[Warehouses] W ON [WL].[numWarehouseID] = [W].[numWareHouseID]
                WHERE  [WL].[numWLocationID] IN (
                        SELECT  WHI.[numWLocationID]
                        FROM    [dbo].[WareHouseItems] AS WHI
                                JOIN [dbo].[OpportunityItems] AS OI ON [WHI].[numWareHouseItemID] = [OI].[numWarehouseItmsID]
                        WHERE   [OI].[numOppId] = oppmst.[numOppId])
              FOR
                XML PATH('')
              ), 1, 1, ''),'') AS [vcLocation],
		(CASE WHEN ISNULL(RecurrenceConfiguration.numRecConfigID,0) = 0 THEN 0 ELSE 1 END) AS bitRecur,
		ISNULL(Opp.bitRecurred,0) AS bitRecurred,
		ISNULL((SELECT COUNT(*) FROM OpportunityBizDocs WHERE numOppID=@numOppID AND ISNULL(numDeferredBizDocID,0) = ISNULL(opp.numOppBizDocsId,0)),0) AS intInvoiceForDiferredBizDoc,
		ISNULL(Opp.bitShippingGenerated,0) AS bitShippingGenerated,
		ISNULL(oppmst.intUsedShippingCompany,0) as intUsedShippingCompany,
		numSourceBizDocId,
		ISNULL((SELECT TOP 1 vcBizDocID FROM OpportunityBizDocs WHERE numOppBizDOcsId=opp.numSourceBizDocId),'NA') AS vcSourceBizDocID,
		ISNULL(oppmst.numShipmentMethod,0) numShipmentMethod
 FROM                         
 OpportunityBizDocs  opp                        
 left join ListDetails mst                        
 on mst.numListItemID=opp.numBizDocId                        
 join OpportunityMaster oppmst                        
 on oppmst.numOppId= opp.numOppId                                
 left join  AuthoritativeBizDocs  AB
 on AB.numDomainId=  oppmst.numDomainID             
 LEFT OUTER JOIN [Currency] C
  ON C.numCurrencyID = oppmst.numCurrencyID
  LEFT JOIN AdditionalContactsInformation con ON con.numContactid = oppmst.numContactId
  LEFT JOIN
	RecurrenceConfiguration
  ON
	RecurrenceConfiguration.numOppBizDocID = opp.numOppBizDocsId
where opp.numOppId=@numOppId

UNION ALL

select  0 AS numOppBizDocsId,                        
 RH.numOppId,                        
 0 AS numBizDocId,                        
 1 AS numOrientation,  
 '' AS vcTemplateName,
 0 AS bitKeepFooterBottom,                   
 '' AS vcPOppName,                        
 'RMA' as BizDoc,                        
 RH.numCreatedBy,                        
DATEADD(minute,-@ClientTimeZoneOffset,RH.dtCreatedDate) CreatedDate,
 '' vcRefOrderNo,                       
 RH.vcRMA AS vcBizDocID,
 RH.vcRMA AS vcBizDocName,      
  ISNULL(monAmount,0) as monPAmount,
  'RMA' as BizDocType,
  0 AS monAmountPaid, 
  '' varCurrSymbol ,
  0 bitAuthoritativeBizDocs,
  0 tintshipped,0 as numShipVia,RH.numDivisionId,0 as tintDeferred,
  0 AS numBizDocTempID,con.numContactid,isnull(con.vcEmail,'') AS vcEmail,0 AS numBizDocStatus,
  0 AS numBillingDaysName,RH.dtCreatedDate AS dtFromDate,
  RH.dtCreatedDate AS BillingDate,
  1 fltExchangeRateCurrent,
  1 fltExchangeRateOfOrder,
  @BaseCurrencySymbol BaseCurrencySymbol,RH.numReturnHeaderID,RH.tintReturnType,1 AS fltExchangeRateBizDoc
  ,0 [IsShippingReportGenerated],0 [ISTrackingNumGenerated],
  0 AS numShippingReportId,
  0 AS numShippingLabelReportId			 
  ,ISNULL(STUFF(( SELECT  ',' +  CASE WHEN LEN(ISNULL(W.[vcWareHouse],'')  + ':' + ISNULL([WL].[vcLocation],'')) > 0 THEN ISNULL(W.[vcWareHouse],'')  + ':' + ISNULL([WL].[vcLocation],'') ELSE '' END
                FROM    [dbo].[WarehouseLocation] AS WL
				JOIN [dbo].[Warehouses] W ON [WL].[numWarehouseID] = [W].[numWareHouseID]
                WHERE  [WL].[numWLocationID] IN (
                        SELECT  WHI.[numWLocationID]
                        FROM    [dbo].[WareHouseItems] AS WHI
                                JOIN [dbo].[OpportunityItems] AS OI ON [WHI].[numWareHouseItemID] = [OI].[numWarehouseItmsID]
                        WHERE   [OI].[numOppId] = RH.[numOppId])
              FOR
                XML PATH('')
              ), 1, 1, ''),'') AS [vcLocation],
			  0 AS bitRecur,
			  0 AS bitRecurred,
		0 AS intInvoiceForDiferredBizDoc ,
		0 AS bitShippingGenerated,
		0 as intUsedShippingCompany,
		0 AS numSourceBizDocId,
		'NA' AS vcSourceBizDocID,
		0 AS numShipmentMethod
from                         
 ReturnHeader RH                        
 LEFT JOIN AdditionalContactsInformation con ON con.numContactid = RH.numContactId
  
where RH.numOppId=@numOppId
)X         
                        
END


GO

/****** Object:  StoredProcedure [dbo].[USP_OPPItemDetails]    Script Date: 09/01/2009 18:59:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- exec USP_OPPItemDetails @numItemCode=31455,@OpportunityId=12387,@numDomainId=110
--created by anooop jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_oppitemdetails')
DROP PROCEDURE usp_oppitemdetails
GO
CREATE PROCEDURE [dbo].[USP_OPPItemDetails]
    (
      @numUserCntID NUMERIC(9) = 0,
      @OpportunityId NUMERIC(9) = 0,
      @numDomainId NUMERIC(9),
      @ClientTimeZoneOffset  INT
    )
AS 
BEGIN
    IF @OpportunityId >0
    BEGIN
        DECLARE @DivisionID AS NUMERIC(9)
        DECLARE @TaxPercentage AS FLOAT
        DECLARE @tintOppType AS TINYINT
        DECLARE @fld_Name AS VARCHAR(500)
        DECLARE @strSQL AS VARCHAR(MAX)
        DECLARE @fld_ID AS NUMERIC(9)
        DECLARE @numBillCountry AS NUMERIC(9)
        DECLARE @numBillState AS NUMERIC(9)

                
        SELECT  @DivisionID = numDivisionID,
                @tintOppType = tintOpptype
        FROM    OpportunityMaster
        WHERE   numOppId = @OpportunityId
               

		CREATE TABLE #tempForm 
		(
			tintOrder TINYINT,vcDbColumnName nvarchar(50),vcOrigDbColumnName nvarchar(50),vcFieldName nvarchar(50),vcAssociatedControlType nvarchar(50),
			vcListItemType varchar(3),numListID numeric(9),vcLookBackTableName varchar(50),bitCustomField BIT,
			numFieldId NUMERIC,bitAllowSorting BIT,bitAllowEdit BIT,bitIsRequired BIT,bitIsEmail BIT,bitIsAlphaNumeric BIT,bitIsNumeric BIT,
			bitIsLengthValidation BIT,intMaxLength int,intMinLength int,bitFieldMessage BIT,vcFieldMessage VARCHAR(500),ListRelID numeric(9),intColumnWidth int,intVisible int
			,vcFieldDataType CHAR(1)
		)

		IF
			(
				SELECT 
					COUNT(*)
				FROM 
					View_DynamicColumns DC 
				WHERE 
					DC.numFormId=(CASE WHEN @tintOppType = 2 THEN 129 ELSE 26 END) and DC.numDomainID=@numDomainID AND numUserCntID = @numUserCntID AND
					ISNULL(DC.bitSettingField,0)=1 AND ISNULL(DC.bitDeleted,0)=0 AND ISNULL(DC.bitCustom,0)=0) > 0
			OR
			(
				SELECT 
					COUNT(*)
				FROM 
					View_DynamicCustomColumns
				WHERE 
					numFormId=(CASE WHEN @tintOppType = 2 THEN 129 ELSE 26 END) AND numUserCntID=@numUserCntID AND numDomainID=@numDomainID AND tintPageType=1 AND numRelCntType=0 AND ISNULL(bitCustom,0)=1
			) > 0
		BEGIN	

		INSERT INTO #tempForm
		select isnull(DC.tintRow,0)+1 as tintOrder,DDF.vcDbColumnName,DDF.vcOrigDbColumnName,ISNULL(DDF.vcCultureFieldName,DDF.vcFieldName),                  
		 DDF.vcAssociatedControlType,DDF.vcListItemType,DDF.numListID,DDF.vcLookBackTableName                                                 
		,DDF.bitCustom,DDF.numFieldId,DDF.bitAllowSorting,DDF.bitInlineEdit,
		DDF.bitIsRequired,DDF.bitIsEmail,DDF.bitIsAlphaNumeric,DDF.bitIsNumeric,DDF.bitIsLengthValidation,
		DDF.intMaxLength,DDF.intMinLength,DDF.bitFieldMessage,DDF.vcFieldMessage vcFieldMessage,DDF.ListRelID,DDF.intColumnWidth,Case when DC.numFieldID is null then 0 else 1 end,DDF.vcFieldDataType
		 FROM View_DynamicDefaultColumns DDF left join View_DynamicColumns DC on DC.numFormId=(CASE WHEN @tintOppType = 2 THEN 129 ELSE 26 END) and DDF.numFieldId=DC.numFieldId and DC.numUserCntID=@numUserCntID and DC.numDomainID=@numDomainID and numRelCntType=0 AND tintPageType=1 
		 where DDF.numFormId=(CASE WHEN @tintOppType = 2 THEN 129 ELSE 26 END) and DDF.numDomainID=@numDomainID AND ISNULL(DDF.bitSettingField,0)=1 AND ISNULL(DDF.bitDeleted,0)=0 AND ISNULL(DDF.bitCustom,0)=0
       
		 UNION
    
			select tintRow+1 as tintOrder,vcDbColumnName,vcFieldName,vcFieldName,                  
		 vcAssociatedControlType,'' as vcListItemType,numListID,''                                                 
		,bitCustom,numFieldId,bitAllowSorting,0 as bitAllowEdit,
		bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
		intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,''
		 from View_DynamicCustomColumns
		 where numFormId=(CASE WHEN @tintOppType = 2 THEN 129 ELSE 26 END) and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 AND numRelCntType=0
		 AND ISNULL(bitCustom,0)=1

		END
		ELSE
		BEGIN
			--Check if Master Form Configuration is created by administrator if NoColumns=0
			DECLARE @IsMasterConfAvailable BIT = 0
			DECLARE @numUserGroup NUMERIC(18,0)

			SELECT @numUserGroup = numGroupID FROM UserMaster WHERE numUserDetailId = @numUserCntID 

			IF (SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = @numDomainID AND numFormID = (CASE WHEN @tintOppType = 2 THEN 129 ELSE 26 END) AND bitGridConfiguration = 1 AND numGroupID = @numUserGroup) > 0
			BEGIN
				SET @IsMasterConfAvailable = 1
			END

			--If MasterConfiguration is available then load it otherwise load default columns
			IF @IsMasterConfAvailable = 1
			BEGIN
				INSERT INTO #tempForm
				SELECT 
					(intRowNum + 1) as tintOrder, vcDbColumnName, vcOrigDbColumnName,ISNULL(vcCultureFieldName,vcFieldName),                  
					vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName,bitCustom,numFieldId,bitAllowSorting,bitInlineEdit,
					bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength,
					bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth,1,vcFieldDataType
				FROM 
					View_DynamicColumnsMasterConfig
				WHERE
					View_DynamicColumnsMasterConfig.numFormId=(CASE WHEN @tintOppType = 2 THEN 129 ELSE 26 END) AND 
					View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
					View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
					View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
					ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
					ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
				UNION
				SELECT 
					tintRow+1 as tintOrder,vcDbColumnName,vcFieldName,vcFieldName,                  
					 vcAssociatedControlType,'' as vcListItemType,numListID,''                                                 
					,bitCustom,numFieldId,bitAllowSorting,0 as bitAllowEdit,
					bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
					intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,''
				FROM 
					View_DynamicCustomColumnsMasterConfig
				WHERE 
					View_DynamicCustomColumnsMasterConfig.numFormId=(CASE WHEN @tintOppType = 2 THEN 129 ELSE 26 END) AND 
					View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
					View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
					View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
					ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1
				ORDER BY 
					tintOrder ASC  
			END
		END


SET @strSQL = ''

DECLARE @avgCost int
SET @avgCost = ISNULL((select TOP 1 numCost from Domain where numDOmainId=@numDomainID),0)



--------------Make Dynamic Query--------------
SET @strSQL = @strSQL + '
Select 
Opp.numoppitemtCode,Opp.vcNotes,WItems.numBackOrder,CAST(ISNULL(Opp.numCost,0) AS MONEY) AS numCost,
ISNULL(Opp.bitItemPriceApprovalRequired,0) AS bitItemPriceApprovalRequired,
ISNULL(Opp.numSOVendorID,0) AS numVendorID,'+
CONCAT('ISNULL(Opp.monTotAmount,0) - (ISNULL(Opp.numUnitHour,0) * (CASE ',@avgCost,' WHEN 3 THEN ISNULL(V.monCost,0) / dbo.fn_UOMConversion(numPurchaseUnit,I.numItemCode,',@numDomainID,',numBaseUnit) WHEN 2 THEN (CASE WHEN ISNULL(Opp.numSOVendorID,0) > 0 THEN Opp.numCost ELSE ISNULL(V.monCost,0) / dbo.fn_UOMConversion(numPurchaseUnit,I.numItemCode,',@numDomainID,',numBaseUnit) END) ELSE monAverageCost END)) AS vcMargin,')
+'
OM.tintOppType,
Opp.numItemCode,
I.charItemType,
ISNULL(I.bitAsset,0) as bitAsset,
ISNULL(I.bitRental,0) as bitRental,
ISNULL(I.bitSerialized,0) bitSerialized,
ISNULL(I.bitLotNo,0) bitLotNo,
Opp.numOppId,
Opp.bitWorkOrder,
Opp.fltDiscount,
ISNULL(Opp.bitDiscountType,0) bitDiscountType,
dbo.fn_UOMConversion(Opp.numUOMId, Opp.numItemCode,' + CAST(@numDomainId AS VARCHAR) +', NULL) AS UOMConversionFactor,
ISNULL(Opp.bitWorkOrder,0) bitWorkOrder,
ISNULL(Opp.numUnitHourReceived,0) numUnitHourReceived,
ISNULL((SELECT COUNT(*) FROM OppWarehouseSerializedItem WHERE numOppId=Opp.numOppID AND numOppItemID=Opp.numoppitemtCode),0) AS numSerialNoAssigned,
CASE WHEN (SELECT COUNT(*) FROM OpportunityBizDocs OB JOIN dbo.OpportunityBizDocItems OBI ON ob.numOppBizDocsId=obi.numOppBizDocID
					AND (ob.bitAuthoritativeBizDocs=1 OR ob.numBizDocId=304) AND obi.numOppItemID=Opp.numoppitemtCode AND ob.numOppId=Opp.numOppId)>0 THEN 1 ELSE 0 END  AS bitIsAuthBizDoc,
(CASE WHEN (SELECT COUNT(*) FROM OpportunityBizDocs OB JOIN dbo.OpportunityBizDocItems OBI ON OB.numOppBizDocsId=OBI.numOppBizDocID WHERE OB.numOppId = Opp.numOppId AND OBI.numOppItemID=Opp.numoppitemtCode AND OB.numBizDocId=296) > 0 THEN 1 ELSE 0 END) AS bitAddedFulFillmentBizDoc,
ISNULL(numPromotionID,0) AS numPromotionID,
ISNULL(( SELECT numReturnID FROM   Returns WHERE  numOppItemCode = Opp.numoppitemtCode AND numOppId = Opp.numOppID), 0) AS numReturnID,
dbo.fn_GetReturnItemQty(Opp.numoppitemtCode) AS ReturrnedQty,
[dbo].fn_GetReturnStatus(Opp.numoppitemtCode) AS ReturnStatus,
ISNULL(opp.numUOMId, 0) AS numUOM,
(SELECT vcUnitName FROM dbo.UOM WHERE numUOMId = opp.numUOMId AND ISNULL(bitEnabled,0) = 1)  AS  [vcBaseUOMName],
Opp.numUnitHour AS numOriginalUnitHour, 
isnull(M.vcPOppName,''Internal'') as Source,
numSourceID,
  CASE WHEN ISNULL(bitKitParent,0) = 1
                               AND ISNULL(bitAssembly,0) = 1 THEN 0
                          WHEN ISNULL(bitKitParent,0) = 1 THEN 1
                          ELSE 0
                     END as bitKitParent,
(CASE WHEN OM.tintOppType = 1 AND OM.tintOppStatus = 1 THEN 
(CASE WHEN (I.charItemType = ''p'' AND ISNULL(bitDropShip, 0) = 0 AND ISNULL(bitAsset,0)=0)THEN dbo.CheckOrderItemInventoryStatus(Opp.numItemCode,Opp.numUnitHour,Opp.numoppitemtCode,Opp.numWarehouseItmsID,(CASE WHEN ISNULL(bitKitParent,0) = 1
                               AND ISNULL(bitAssembly,0) = 1 THEN 0
                          WHEN ISNULL(bitKitParent,0) = 1 THEN 1
                          ELSE 0
                     END)) ELSE 0 END) ELSE 0 END) AS bitBackOrder,
(SELECT vcData FROM dbo.ListDetails WHERE 1=1 AND numListItemID = numShipClass ) AS [numShipClass],ISNULL(Opp.numQtyShipped,0) numQtyShipped,ISNULL(Opp.numUnitHourReceived,0) AS numUnitHourReceived,ISNULL(OM.fltExchangeRate,0) AS fltExchangeRate,ISNULL(OM.numCurrencyID,0) AS numCurrencyID,ISNULL(OM.numDivisionID,0) AS numDivisionID,
(CASE 
	WHEN OM.tintOppType = 1 AND OM.tintOppStatus = 1 AND UPPER(I.charItemType) = ''P'' AND bitDropShip = 0
	THEN (CASE WHEN OM.tintShipped = 1 THEN CAST(ISNULL(Opp.numUnitHour,0) AS VARCHAR) ELSE CAST(ISNULL(Opp.numQtyShipped,0) AS VARCHAR) END)
	WHEN OM.tintOppType = 2 AND OM.tintOppStatus = 1 AND UPPER(I.charItemType) = ''P'' AND bitDropShip = 0
	THEN (CASE WHEN OM.tintShipped = 1 THEN CAST(ISNULL(Opp.numUnitHour,0) AS VARCHAR) ELSE CAST(ISNULL(Opp.numUnitHourReceived,0) AS VARCHAR) END)
	ELSE '''' END) AS vcShippedReceived,
isnull(i.numIncomeChartAcntId,0) as itemIncomeAccount,isnull(i.numAssetChartAcntId,0) as itemInventoryAsset,isnull(i.numCOGsChartAcntId,0) as itemCoGs,
ISNULL(Opp.numRentalIN,0) as numRentalIN,
ISNULL(Opp.numRentalLost,0) as numRentalLost,
ISNULL(Opp.numRentalOut,0) as numRentalOut,
(SELECT vcData FROM dbo.ListDetails WHERE 1=1 AND numListItemID = numShipClass ) AS [numShipClass],ISNULL(bitFreeShipping,0) AS [IsFreeShipping],Opp.bitDiscountType,isnull(Opp.monLandedCost,0) as monLandedCost, ISNULL(Opp.vcPromotionDetail,'''') AS vcPromotionDetail,
(CASE WHEN I.charItemType = ''p'' AND ISNULL(bitDropShip, 0) = 0 THEN dbo.GetOppItemReleaseDates(om.numDomainId,Opp.numOppId,Opp.numoppitemtCode,1) ELSE '''' END) AS vcItemReleaseDate,'''' AS numPurchasedQty, '

--	IF @fld_Name = 'vcPathForTImage' 
		SET @strSQL = @strSQL + 'Opp.vcPathForTImage,'
--	ELSE IF @fld_Name = 'vcItemName' 
		SET @strSQL = @strSQL + 'Opp.vcItemName,'
--	ELSE IF @fld_Name = 'vcModelID,' 
		SET @strSQL = @strSQL + 'Opp.vcModelID,'
--	ELSE IF @fld_Name = 'vcWareHouse' 
		SET @strSQL = @strSQL + 'vcWarehouse,WItems.numWarehouseID,'
		SET @strSQL = @strSQL + 'WItems.numOnHand,isnull(WItems.numAllocation,0) as numAllocation,'
		SET @strSQL = @strSQL + 'WL.vcLocation,'
--	ELSE IF @fld_Name = 'ItemType' 
		SET @strSQL = @strSQL + 'Opp.vcType ItemType,Opp.vcType,'
--	ELSE IF @fld_Name = 'vcItemDesc' 
		SET @strSQL = @strSQL + 'ISNULL(Opp.vcItemDesc, '''') vcItemDesc,'
--	ELSE IF @fld_Name = 'Attributes' 
		SET @strSQL = @strSQL + 'Opp.vcAttributes,' --dbo.fn_GetAttributes(Opp.numWarehouseItmsID, I.bitSerialized) AS Attributes,
--	ELSE IF @fld_Name = 'bitDropShip' 
		SET @strSQL = @strSQL + 'ISNULL(bitDropShip, 0) as DropShip,CASE ISNULL(bitDropShip, 0) WHEN 1 THEN ''Yes'' ELSE '''' END AS bitDropShip,'
--	ELSE IF @fld_Name = 'numUnitHour' 
		SET @strSQL = @strSQL + 'CAST(( dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), Opp.numItemCode,om.numDomainId, ISNULL(opp.numUOMId, 0))* Opp.numUnitHour ) AS NUMERIC(18, 2)) AS numUnitHour,'
--	ELSE IF @fld_Name = 'monPrice' 
		SET @strSQL = @strSQL + 'Opp.monPrice,'
--	ELSE IF @fld_Name = 'monPriceUOM' 
		SET @strSQL = @strSQL + 'CAST(( dbo.fn_UOMConversion(ISNULL(opp.numUOMId, 0), Opp.numItemCode,om.numDomainId, ISNULL(I.numBaseUnit, 0)) * Opp.monPrice) AS NUMERIC(18, 4)) AS monPriceUOM,'
--	ELSE IF @fld_Name = 'monTotAmount' 
		SET @strSQL = @strSQL + 'Opp.monTotAmount,Opp.monTotAmtBefDiscount,'
--	ELSE IF @fld_Name = 'vcSKU' 
		SET @strSQL = @strSQL + 'ISNULL(WItems.vcWHSKU,ISNULL(I.vcSKU,'''')) [vcSKU],'
--	ELSE IF @fld_Name = 'vcManufacturer' 
		SET @strSQL = @strSQL + 'Opp.vcManufacturer,'
--	ELSE IF @fld_Name = 'vcItemClassification' 
		SET @strSQL = @strSQL + ' dbo.[fn_GetListItemName](I.[numItemClassification]) numItemClassification,'
--	ELSE IF @fld_Name = 'numItemGroup' 
		SET @strSQL = @strSQL + '( SELECT    [vcItemGroup] FROM      [ItemGroups] WHERE     [numItemGroupID] = I.numItemGroup ) numItemGroup,'
--	ELSE IF @fld_Name = 'vcUOMName' 
		SET @strSQL = @strSQL + 'ISNULL(u.vcUnitName, '''') numUOMId,'
--	ELSE IF @fld_Name = 'fltWeight' 
		SET @strSQL = @strSQL + 'I.fltWeight,'
--	ELSE IF @fld_Name = 'fltHeight' 
		SET @strSQL = @strSQL + 'I.fltHeight,'
--	ELSE IF @fld_Name = 'fltWidth' 
		SET @strSQL = @strSQL + 'I.fltWidth,'
--	ELSE IF @fld_Name = 'fltLength' 
		SET @strSQL = @strSQL + 'I.fltLength,'
--	ELSE IF @fld_Name = 'numBarCodeId' 
		SET @strSQL = @strSQL + 'I.numBarCodeId,'
		SET @strSQL = @strSQL + 'I.IsArchieve,ISNULL(I.numContainer,0) AS numContainer,ISNULL(I.numNoItemIntoContainer,0) AS numContainerQty,'
--	ELSE IF @fld_Name = 'vcWorkOrderStatus' 
		SET @strSQL = @strSQL + 'CASE WHEN ISNULL(Opp.bitWorkOrder, 0) = 1
				 THEN ( SELECT  dbo.GetListIemName(numWOStatus)
								+ CASE WHEN ISNULL(numWOStatus, 0) = 23184
									   THEN '' - ''
											+ CAST(ISNULL(dbo.fn_GetContactName(numAssignedTo),
														  '''') + '',''
											+ CONVERT(VARCHAR(20), DATEADD(minute, '+ CONVERT(VARCHAR(15),-@ClientTimeZoneOffset)  +', bintCompletedDate)) AS VARCHAR(100))
									   ELSE ''''
								  END
						FROM    WorkOrder
						WHERE   ISNULL(numOppId, 0) = Opp.numOppId
								AND numItemCode = Opp.numItemCode AND ISNULL(numParentWOID,0)=0
					  )
				 ELSE ''''
			END AS vcWorkOrderStatus,'
--	ELSE IF @fld_Name = 'DiscAmt' 
		SET @strSQL = @strSQL + 'CASE WHEN Opp.bitDiscountType = 0
				 THEN REPLACE(STR(ISNULL(Opp.monTotAmtBefDiscount, Opp.monTotAmount)
					  - Opp.monTotAmount,20,4), '' '', '''') + '' (''
					  + CAST(FORMAT(Opp.fltDiscount,''0.00##'') AS VARCHAR(50)) + ''%)''
				 ELSE REPLACE(STR(ISNULL(Opp.monTotAmtBefDiscount, Opp.monTotAmount)
					  - Opp.monTotAmount,20,4), '' '', '''')
			END AS DiscAmt,'	
--	ELSE IF @fld_Name = 'numProjectID' 
		SET @strSQL = @strSQL + ' ISNULL(Opp.numProjectID, 0) numProjectID,
		 CASE WHEN ( SELECT TOP 1
								ISNULL(OBI.numOppBizDocItemID, 0)
						FROM    dbo.OpportunityBizDocs OBD
								INNER JOIN dbo.OpportunityBizDocItems OBI ON OBD.numOppBizDocsID = OBI.numOppBizDocID
						WHERE   OBI.numOppItemID = Opp.numoppitemtCode
								AND ISNULL(OBD.bitAuthoritativeBizDocs, 0) = 1
								and isnull(OBD.numOppID,0)=OM.numOppID
					  ) > 0 THEN 1
				 ELSE 0
			END AS bitItemAddedToAuthBizDoc,'	
--	ELSE IF @fld_Name = 'numClassID' 
		SET @strSQL = @strSQL + 'ISNULL(Opp.numClassID, 0) numClassID,'	
--	ELSE IF @fld_Name = 'vcProjectStageName' 
		SET @strSQL = @strSQL + ' ISNULL(Opp.numProjectStageID, 0) numProjectStageID,
				CASE WHEN OPP.numProjectStageID > 0
				 THEN dbo.fn_GetProjectStageName(OPP.numProjectId,
												 OPP.numProjectStageID)
				 WHEN OPP.numProjectStageID = -1 THEN ''T&E''
				 WHEN OPP.numProjectId > 0
				 THEN CASE WHEN OM.tintOppType = 1 THEN ''S.O.''
						   ELSE ''P.O.''
					  END
				 ELSE ''-''
			END AS vcProjectStageName,'	
	   
	   IF EXISTS (SELECT * FROM #tempForm WHERE vcOrigDbColumnName='vcInclusionDetails')
	   BEGIN
			SET @strSQL = @strSQL + 'dbo.GetOrderAssemblyKitInclusionDetails(OM.numOppID,Opp.numoppitemtCode,Opp.numUnitHour) AS vcInclusionDetails,'
	   END     
	
	-- (CASE WHEN @tintOppType=1 THEN 26 ELSE 129 END) --

--------------Add custom fields to query----------------
SELECT TOP 1 @fld_Name= vcFieldName,@fld_ID = numFieldId FROM #tempForm ORDER BY numFieldId
WHILE @fld_ID > 0
BEGIN ------------------

	 PRINT @fld_Name
	
	       SET @strSQL = @strSQL + 'dbo.GetCustFldValueOppItems('
                                    + CONVERT(VARCHAR(15), @fld_ID)
                                    + ',Opp.numoppitemtCode,Opp.numItemCode) as ''' + @fld_Name + ''','
       

SELECT TOP 1 @fld_Name= vcFieldName,@fld_ID = numFieldId FROM #tempForm
WHERE numFieldId > @fld_ID ORDER BY numFieldId
 IF @@ROWCOUNT=0
 SET @fld_ID = 0
END ------------------


                                
--Purchase Opportunity ?
IF @tintOppType = 2 
    BEGIN
        SET @strSQL =  @strSQL + 'ISNULL((SELECT sum(numQty) FROM OppWarehouseSerializedItem WHERE numOppID=OM.numOppID AND numOppItemID=Opp.numoppitemtCode),0) AS SerialLotNo,
		  SUBSTRING(( SELECT  '', '' + vcSerialNo
                        + CASE WHEN ISNULL(I.bitLotNo, 0) = 1
                                THEN '' (''
                                    + CONVERT(VARCHAR(15), oppI.numQty)
                                    + '')''
                                ELSE ''''
                            END
                FROM    OppWarehouseSerializedItem oppI
                        JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID = whi.numWareHouseItmsDTLID
                WHERE   oppI.numOppID = Opp.numOppId
                        AND oppI.numOppItemID = Opp.numoppitemtCode
                ORDER BY vcSerialNo
                FOR
                XML PATH('''')
                ), 3, 200000) AS vcSerialLotNo,
		  CAST(Opp.numUnitHour AS FLOAT) numQty,
		  (SELECT vcUnitName FROM dbo.UOM WHERE numUOMId = opp.numUOMId AND ISNULL(bitEnabled,0) = 1)  AS  [vcBaseUOMName],
		  I.bitSerialized,
		  ISNULL(I.bitLotNo, 0) AS bitLotNo,
		  0 AS bitBarcodePrint,
		  Opp.numWarehouseItmsID as numWarehouseItmsID,'
	END
ELSE
	BEGIN	
        SET @strSQL =  @strSQL + ' ISNULL((SELECT sum(numQty) FROM OppWarehouseSerializedItem WHERE numOppID=OM.numOppID AND numOppItemID=Opp.numoppitemtCode),0) AS SerialLotNo,
		   SUBSTRING(( SELECT  '', '' + vcSerialNo
                        + CASE WHEN ISNULL(I.bitLotNo, 0) = 1
                                THEN '' (''
                                    + CONVERT(VARCHAR(15), oppI.numQty)
                                    + '')''
                                ELSE ''''
                            END
                FROM    OppWarehouseSerializedItem oppI
                        JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID = whi.numWareHouseItmsDTLID
                WHERE   oppI.numOppID = Opp.numOppId
                        AND oppI.numOppItemID = Opp.numoppitemtCode
                ORDER BY vcSerialNo
                FOR
                XML PATH('''')
                ), 3, 200000) AS vcSerialLotNo,
		   CAST(Opp.numUnitHour AS FLOAT) numQty,
		  I.bitSerialized,
		  ISNULL(I.bitLotNo, 0) AS bitLotNo,
		  Opp.numWarehouseItmsID as numWarehouseItmsID,'
	END


        
 SET @strSQL = left(@strSQL,len(@strSQL)-1) + '   
        FROM    OpportunityItems Opp
        LEFT JOIN OpportunityMaster OM ON Opp.numOppID = oM.numOppID
        JOIN item I ON Opp.numItemCode = i.numItemcode
        LEFT JOIN WareHouseItems WItems ON WItems.numWareHouseItemID = Opp.numWarehouseItmsID
        LEFT JOIN Warehouses W ON W.numWarehouseID = WItems.numWarehouseID
        LEFT JOIN WarehouseLocation WL ON WL.numWLocationID = WItems.numWLocationID
        LEFT JOIN OpportunityMaster M ON Opp.numSourceID = M.numOppID
        LEFT JOIN UOM u ON u.numUOMId = opp.numUOMId
		LEFT JOIN Vendor V ON I.numVendorID = V.numVendorID  AND V.numItemCode = I.numItemCode
WHERE   om.numDomainId = '+ CONVERT(VARCHAR(10),@numDomainId) + '
        AND Opp.numOppId = '+ CONVERT(VARCHAR(15),@OpportunityId) +'
ORDER BY opp.numSortOrder,numoppitemtCode ASC '
  
PRINT @strSQL;
EXEC (@strSQL);
SELECT  vcSerialNo,
       vcComments AS Comments,
        numOppItemID AS numoppitemtCode,
        W.numWarehouseItmsDTLID,
        numWarehouseItmsID AS numWItmsID,
        dbo.Fn_getattributes(W.numWarehouseItmsDTLID,
                             1) Attributes
FROM    WareHouseItmsDTL W
        JOIN OppWarehouseSerializedItem O ON O.numWarehouseItmsDTLID = W.numWareHouseItmsDTLID
WHERE   numOppID = @OpportunityId


select * from #tempForm order by tintOrder

drop table #tempForm

            END
            
    END
/****** Object:  StoredProcedure [dbo].[USP_OppManage]    Script Date: 07/26/2008 16:20:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OppManage')
DROP PROCEDURE usp_oppmanage
GO
CREATE PROCEDURE [dbo].[USP_OppManage] 
(                                                                          
  @numOppID as numeric=0 OUTPUT,                                                                          
  @numContactId numeric(9)=null,                                                                          
  @numDivisionId numeric(9)=null,                                                                          
  @tintSource numeric(9)=null,                                                                          
  @vcPOppName Varchar(100)='' OUTPUT,                                                                          
  @Comments varchar(1000)='',                                                                          
  @bitPublicFlag bit=0,                                                                          
  @numUserCntID numeric(9)=0,                                                                                 
  @monPAmount money =0,                                                 
  @numAssignedTo as numeric(9)=0,                                                                                                        
  @numDomainId numeric(9)=0,                                                                                                                                                                                   
  @strItems as text=null,                                                                          
  @strMilestone as text=null,                                                                          
  @dtEstimatedCloseDate datetime,                                                                          
  @CampaignID as numeric(9)=null,                                                                          
  @lngPConclAnalysis as numeric(9)=null,                                                                         
  @tintOppType as tinyint,                                                                                                                                             
  @tintActive as tinyint,                                                              
  @numSalesOrPurType as numeric(9),              
  @numRecurringId as numeric(9),
  @numCurrencyID as numeric(9)=0,
  @DealStatus as tinyint =0,-- 0=Open,1=Won,2=Lost
  @numStatus AS NUMERIC(18,0),
  @vcOppRefOrderNo VARCHAR(100),
  @numBillAddressId numeric(9)=0,
  @numShipAddressId numeric(9)=0,
  @bitStockTransfer BIT=0,
  @WebApiId INT = 0,
  @tintSourceType TINYINT=0,
  @bitDiscountType as bit,
  @fltDiscount  as float,
  @bitBillingTerms as bit,
  @intBillingDays as integer,
  @bitInterestType as bit,
  @fltInterest as float,
  @tintTaxOperator AS TINYINT,
  @numDiscountAcntType AS NUMERIC(9),
  @vcWebApiOrderNo VARCHAR(100)=NULL,
  @vcCouponCode		VARCHAR(100) = '',
  @vcMarketplaceOrderID VARCHAR(100)=NULL,
  @vcMarketplaceOrderDate  datetime=NULL,
  @vcMarketplaceOrderReportId VARCHAR(100)=NULL,
  @numPercentageComplete NUMERIC(9),
  @bitUseShippersAccountNo BIT = 0,
  @bitUseMarkupShippingRate BIT = 0,
  @numMarkupShippingRate NUMERIC(19,2) = 0,
  @intUsedShippingCompany INT = 0,
  @numShipmentMethod NUMERIC(18,0)=0,
  @dtReleaseDate DATE = NULL,
  @numPartner NUMERIC(18,0)=0,
  @tintClickBtn INT=0,
  @numPartenerContactId NUMERIC(18,0)=0,
  @numAccountClass NUMERIC(18,0) = 0,
  @numWillCallWarehouseID NUMERIC(18,0) = 0,
  @numVendorAddressID NUMERIC(18,0) = 0,
  @numShipFromWarehouse NUMERIC(18,0) = 0
)                                                                          
                                                                          
as  
BEGIN TRY
BEGIN TRANSACTION
	DECLARE @fltExchangeRate float                                 
	DECLARE @hDocItem int                                                                                                                            
	DECLARE @TotalAmount as money                                                                          
	DECLARE @tintOppStatus as tinyint
	DECLARE @bitNewOrder AS BIT = (CASE WHEN ISNULL(@numOppID,0) = 0 THEN 1 ELSE 0 END)
	
	IF ISNULL(@numContactId,0) = 0
	BEGIN
		RAISERROR('CONCAT_REQUIRED',16,1)
		RETURN
	END  
	
	IF ISNULL(@tintOppType,0) = 0
	BEGIN
		RAISERROR('OPP_TYPE_CAN_NOT_BE_0',16,1)
		RETURN
	END             

	IF CONVERT(VARCHAR(10),@strItems) <> ''
	BEGIN
		EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems

		--VALIDATE PROMOTION OF COUPON CODE
		DECLARE @TEMP TABLE
		(
			ID INT IDENTITY(1,1),
			numOppItemID NUMERIC(18,0),
			numPromotionID NUMERIC(18,0)
		)

		INSERT INTO @TEMP
		(
			numOppItemID,
			numPromotionID
		)
		SELECT
			numoppitemtCode,
			numPromotionID
		FROM
			OPENXML (@hDocItem,'/NewDataSet/Item',2)
		WITH
		(
			numoppitemtCode NUMERIC(18,0),
			numPromotionID NUMERIC(18,0)
		)

		IF (SELECT
				COUNT(*)
			FROM
				PromotionOffer PO
			WHERE
				numDomainId=@numDomainId
				AND bitRequireCouponCode = 1
				AND ISNULL(bitEnabled,0) = 1
				AND ISNULL(tintUsageLimit,0) > 0
				AND (intCouponCodeUsed 
					- (CASE WHEN ISNULL((SELECT COUNT(*) FROM OpportunityItems WHERE numOppID=ISNULL(@numOppID,0) AND numPromotionID=PO.numProId),0) > 0 THEN 1 ELSE 0 END) 
					+ (CASE WHEN ISNULL((SELECT COUNT(*) FROM @TEMP WHERE numPromotionID=PO.numProId),0) > 0 THEN 1 ELSE 0 END)) > tintUsageLimit) > 0
		BEGIN
			RAISERROR('USAGE_OF_COUPON_EXCEED',16,1)
			RETURN
		END
		ELSE
		BEGIN
			UPDATE
				PO
			SET 
				intCouponCodeUsed = ISNULL(intCouponCodeUsed,0) 
									- (CASE WHEN ISNULL((SELECT COUNT(*) FROM OpportunityItems WHERE numOppID=ISNULL(@numOppID,0) AND numPromotionID=PO.numProId),0) > 0 THEN 1 ELSE 0 END) 
									+ (CASE WHEN ISNULL(T1.intUsed,0) > 0 THEN 1 ELSE 0 END) 
			FROM
				PromotionOffer PO
			INNER JOIN
				(
					SELECT
						numPromotionID,
						COUNT(*) AS intUsed
					FROM
						@TEMP
					GROUP BY
						numPromotionID
				) AS T1
			ON
				PO.numProId = T1.numPromotionID
			WHERE
				numDomainId=@numDomainId
				AND bitRequireCouponCode = 1
				AND ISNULL(bitEnabled,0) = 1
		END
	END

	--IF NEW OPPERTUNITY
	IF @numOppID = 0                                                                          
	BEGIN
		DECLARE @bitIsInitalSalesOrder BIT
		IF(@DealStatus=1 AND @tintOppType=1)
		BEGIN
			SET @bitIsInitalSalesOrder=1
		END

		DECLARE @intOppTcode AS NUMERIC(9)  
  
		SELECT @intOppTcode = MAX(numOppId) FROM OpportunityMaster                

		SET @intOppTcode = ISNULL(@intOppTcode,0) + 1                                                
		
		SET @vcPOppName = @vcPOppName + '-'+ CONVERT(varchar(4),YEAR(GETUTCDATE()))  +'-A'+ CONVERT(varchar(10),@intOppTcode)                                  
  
		IF ISNULL(@numCurrencyID,0) = 0 
		BEGIN
			SET @fltExchangeRate=1
			SELECT @numCurrencyID= ISNULL(numCurrencyID,0) FROM dbo.Domain WHERE numDomainId=@numDomainId	
		END
		ELSE
		BEGIN 
			SET @fltExchangeRate = dbo.GetExchangeRate(@numDomainID,@numCurrencyID)
        END  
		                                                        
		IF ISNULL(@numAccountClass,0) = 0
		BEGIN                                                  
		DECLARE @tintDefaultClassType AS INT = 0
		SELECT @tintDefaultClassType = ISNULL(tintDefaultClassType,0) FROM Domain WHERE numDomainId = @numDomainID 

		IF @tintDefaultClassType = 1 --USER
		BEGIN
			SELECT 
				@numAccountClass=ISNULL(numDefaultClass,0) 
			FROM 
				dbo.UserMaster UM 
			JOIN 
				dbo.Domain D 
			ON 
				UM.numDomainID=D.numDomainId
			WHERE 
				D.numDomainId=@numDomainId 
				AND UM.numUserDetailId=@numUserCntID
		END
		ELSE IF @tintDefaultClassType = 2 --COMPANY
		BEGIN
			SELECT 
				@numAccountClass=ISNULL(numAccountClassID,0) 
			FROM 
				dbo.DivisionMaster DM 
			WHERE 
				DM.numDomainId=@numDomainId 
				AND DM.numDivisionID=@numDivisionID
		END
		ELSE
		BEGIN
			SET @numAccountClass = 0
		END
		END

        IF(@DealStatus=1 AND @tintOppType=1 AND @tintClickBtn=1)
		BEGIN
			SET @DealStatus=0
		END         
		--PRINT ('Deal Staus ' +@DealStatus)                                                      
		INSERT INTO OpportunityMaster
		(                                                                             
			numContactId,numDivisionId,txtComments,numCampainID,bitPublicFlag,tintSource,tintSourceType,vcPOppName,                                                                          
			intPEstimatedCloseDate,monPAmount,numCreatedBy,bintCreatedDate,numModifiedBy,bintModifiedDate,numDomainId,                                                                                                             
			numRecOwner,lngPConclAnalysis,tintOppType,numSalesOrPurType,numCurrencyID,fltExchangeRate,numAssignedTo,
			numAssignedBy,[tintOppStatus],numStatus,vcOppRefOrderNo,bitStockTransfer,bitBillingTerms,intBillingDays,
			bitInterestType,fltInterest,vcCouponCode,bitDiscountType,fltDiscount,bitPPVariance,vcMarketplaceOrderID,
			vcMarketplaceOrderReportId,numPercentageComplete,numAccountClass,tintTaxOperator,bitUseShippersAccountNo,
			bitUseMarkupShippingRate,numMarkupShippingRate,intUsedShippingCompany,numShipmentMethod,numPartner,numPartenerContact,dtReleaseDate,bitIsInitalSalesOrder,numVendorAddressID,numShipFromWarehouse
		)                                                                          
		Values                                                                          
		(                                                                          
			@numContactId,@numDivisionId,ISNULL(@Comments,''),@CampaignID,@bitPublicFlag,@tintSource,@tintSourceType,@vcPOppName,                                                                          
			@dtEstimatedCloseDate,@monPAmount,@numUserCntID,(CASE @tintSourceType WHEN 3 THEN @vcMarketplaceOrderDate ELSE GETUTCDATE() END),                                                                          
			@numUserCntID,GETUTCDATE(),@numDomainId,@numUserCntID,@lngPConclAnalysis,@tintOppType,@numSalesOrPurType,@numCurrencyID, 
			@fltExchangeRate,(CASE WHEN @numAssignedTo > 0 THEN @numAssignedTo ELSE NULL END),(CASE WHEN @numAssignedTo>0 THEN @numUserCntID ELSE NULL END),
			@DealStatus,@numStatus,@vcOppRefOrderNo,@bitStockTransfer,@bitBillingTerms,@intBillingDays,@bitInterestType,@fltInterest,
			@vcCouponCode,@bitDiscountType,@fltDiscount,1,@vcMarketplaceOrderID,@vcMarketplaceOrderReportId,@numPercentageComplete,
			@numAccountClass,@tintTaxOperator,@bitUseShippersAccountNo,@bitUseMarkupShippingRate,@numMarkupShippingRate,@intUsedShippingCompany,@numShipmentMethod,@numPartner,@numPartenerContactId,@dtReleaseDate,@bitIsInitalSalesOrder,@numVendorAddressID,@numShipFromWarehouse
		)                                                                                                                      
		
		SET @numOppID=(SELECT SCOPE_IDENTITY())                
                            
		
		--UPDATE OPPNAME AS PER NAME TEMPLATE
		EXEC dbo.USP_UpdateNameTemplateValue @tintOppType,@numDomainID,@numOppID

		--MAP CUSTOM FIELD	
		DECLARE @tintPageID AS TINYINT;
		SET @tintPageID = CASE WHEN @tintOppType=1 THEN 2 ELSE 6 END 
  	
		EXEC dbo.USP_AddParentChildCustomFieldMap
				@numDomainID = @numDomainID, --  numeric(18, 0)
				@numRecordID = @numOppID, --  numeric(18, 0)
				@numParentRecId = @numDivisionId, --  numeric(18, 0)
				@tintPageID = @tintPageID --  tinyint
 	
		IF ISNULL(@numStatus,0) > 0 AND @tintOppType=1 AND isnull(@DealStatus,0)=1
		BEGIN
	   		EXEC USP_ManageOpportunityAutomationQueue
					@numOppQueueID = 0, -- numeric(18, 0)
					@numDomainID = @numDomainID, -- numeric(18, 0)
					@numOppId = @numOppID, -- numeric(18, 0)
					@numOppBizDocsId = 0, -- numeric(18, 0)
					@numOrderStatus = @numStatus, -- numeric(18, 0)
					@numUserCntID = @numUserCntID, -- numeric(18, 0)
					@tintProcessStatus = 1, -- tinyint
					@tintMode = 1 -- TINYINT
		END
  
		-- INSERTING ITEMS                                                                                                                    
		IF CONVERT(VARCHAR(10),@strItems) <> '' AND @numOppID > 0
		BEGIN
			EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems       
	
			INSERT INTO OpportunityItems
			(
				numOppId,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,bitDropShip,bitDiscountType,fltDiscount,
				monTotAmtBefDiscount,vcItemName,vcModelID,vcManufacturer,vcPathForTImage,monVendorCost,numUOMId,bitWorkOrder,numVendorWareHouse,
				numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,numClassID,numToWarehouseItemID,vcAttributes,vcAttrValues,monAvgCost,bitItemPriceApprovalRequired,
				numPromotionID,bitPromotionTriggered,vcPromotionDetail,numSortOrder,numCost,vcNotes
			)
			SELECT 
				@numOppID,X.numItemCode,dbo.fn_UOMConversion(X.numUOM,numItemCode,@numDomainId,null) * x.numUnitHour,x.monPrice,x.monTotAmount,X.vcItemDesc,
				(CASE X.numWarehouseItmsID 
				WHEN -1 
				THEN 
					CASE 
						WHEN ((SELECT COUNT(*) FROM Item WHERE numDomainID=@numDomainId AND vcSKU= X.vcSKU) > 0)
							THEN
								(SELECT TOP 1 
									numWareHouseItemID 
								FROM 
									WareHouseItems 
								WHERE 
									[numItemID] = X.numItemCode 
									AND numWareHouseID IN (SELECT TOP 1 ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail WHERE numDomainID =@numDomainId AND WebApiId = @WebApiId))
						ELSE
							(SELECT 
								[numWareHouseItemID] 
							FROM 
								[WareHouseItems] 
							WHERE 
								[numItemID] = X.numItemCode 
								AND ISNULL(WareHouseItems.vcWHSKU,'') = ISNULL(X.vcSKU,'')
								AND numWareHouseID IN (SELECT TOP 1 ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail WHERE numDomainID =@numDomainId AND WebApiId = @WebApiId)) 
					END
				WHEN 0 
				THEN 
					CASE 
					WHEN ((SELECT COUNT(*) FROM Item WHERE numDomainID=@numDomainId AND vcSKU=X.vcSKU) > 0)
						THEN
							(SELECT TOP 1 
								numWareHouseItemID 
							FROM 
								WareHouseItems 
							WHERE 
								[numItemID] = X.numItemCode 
								AND numWareHouseID IN (SELECT TOP 1 ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail WHERE numDomainID =@numDomainId AND WebApiId = @WebApiId))
					ELSE
						(SELECT 
							[numWareHouseItemID] 
						FROM 
							[WareHouseItems] 
						WHERE 
							[numItemID] = X.numItemCode 
							AND ISNULL(WareHouseItems.vcWHSKU,'') = ISNULL(X.vcSKU,'')
							AND numWareHouseID IN (SELECT TOP 1 ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail WHERE numDomainID =@numDomainId AND WebApiId = @WebApiId)) 
					END
				ELSE  
					X.numWarehouseItmsID 
				END) AS numWarehouseItmsID,
				X.ItemType,X.DropShip,X.bitDiscountType,X.fltDiscount,X.monTotAmtBefDiscount,X.vcItemName,(SELECT vcModelID FROM item WHERE numItemCode = X.numItemCode),
				(SELECT vcManufacturer FROM item WHERE numItemCode = X.numItemCode),(SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = X.numItemCode AND BitDefault = 1 AND numdomainId = @numdomainId),
				(SELECT isnull(VN.monCost,0) FROM Vendor VN INNER JOIN Item IT ON IT.numItemCode=VN.numItemCode AND VN.numVendorID=IT.numVendorID WHERE  VN.numItemCode=X.numItemCode),
				X.numUOM,X.bitWorkOrder,X.numVendorWareHouse,X.numShipmentMethod,X.numSOVendorId,X.numProjectID,X.numProjectStageID,
				0,
				X.numToWarehouseItemID,X.Attributes,X.AttributeIDs,(SELECT (CASE WHEN ISNULL(I.bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(I.monAverageCost,0) END) FROM Item I WHERE I.numDomainId=@numDomainId AND I.numItemCode=X.numItemCode),
				X.bitItemPriceApprovalRequired,X.numPromotionID,X.bitPromotionTriggered,X.vcPromotionDetail,X.numSortOrder,ISNULL(X.numCost,0),ISNULL(X.vcVendorNotes,'')
			FROM
				(
					SELECT 
						*
					FROM 
						OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=1]',2)                                                                          
					WITH                       
					(                                                                          
						numoppitemtCode NUMERIC(9),numItemCode NUMERIC(9), numUnitHour FLOAT, monPrice DECIMAL(30,16), monTotAmount MONEY,vcItemDesc VARCHAR(1000),                                    
						numWarehouseItmsID NUMERIC(9), ItemType VARCHAR(30), DropShip BIT, bitDiscountType BIT,	fltDiscount DECIMAL(30,16), monTotAmtBefDiscount MONEY,
						vcItemName VARCHAR(300),numUOM NUMERIC(9),bitWorkOrder BIT,numVendorWareHouse NUMERIC(9),numShipmentMethod NUMERIC(9),numSOVendorId NUMERIC(9),
						numProjectID numeric(9),numProjectStageID numeric(9),numToWarehouseItemID NUMERIC(9),Attributes varchar(500),AttributeIDs VARCHAR(500),vcSKU VARCHAR(100),bitItemPriceApprovalRequired BIT,
						numPromotionID NUMERIC(18,0),bitPromotionTriggered BIT,vcPromotionDetail VARCHAR(2000),numSortOrder NUMERIC(18,0),numCost MONEY,vcVendorNotes VARCHAR(1000)
					)
				)X    
			ORDER BY 
				X.numoppitemtCode
    
			UPDATE 
				OpportunityItems                       
			SET 
				numItemCode=X.numItemCode,numOppId=@numOppID,numUnitHour=dbo.fn_UOMConversion(X.numUOM,X.numItemCode,@numDomainId,null) * X.numUnitHour,                      
				monPrice=x.monPrice,monTotAmount=x.monTotAmount,vcItemDesc=X.vcItemDesc,numWarehouseItmsID=X.numWarehouseItmsID,bitDropShip=X.DropShip,
				bitDiscountType=X.bitDiscountType,fltDiscount=X.fltDiscount,monTotAmtBefDiscount=X.monTotAmtBefDiscount,vcItemName=X.vcItemName,numUOMId=numUOM,
				bitWorkOrder=X.bitWorkOrder,numVendorWareHouse=X.numVendorWareHouse,numShipmentMethod=X.numShipmentMethod,numSOVendorId=X.numSOVendorId,
				numProjectId=X.numProjectID,numProjectStageId=X.numProjectStageID,numToWarehouseItemID = X.numToWarehouseItemID,vcAttributes=X.Attributes,
				numClassID=0,vcModelID=X.vcModelID,
				bitItemPriceApprovalRequired = X.bitItemPriceApprovalRequired,vcAttrValues=X.AttributeIDs,
				numPromotionID=X.numPromotionID,bitPromotionTriggered=X.bitPromotionTriggered,vcPromotionDetail=X.vcPromotionDetail,numSortOrder=X.numSortOrder,numCost=X.numCost,vcNotes=X.vcVendorNotes
			FROM 
			(
				SELECT 
					numoppitemtCode as numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,vcModelID,numWarehouseItmsID,DropShip,bitDiscountType,fltDiscount,
					monTotAmtBefDiscount,vcItemName,numUOM,bitWorkOrder,numVendorWareHouse,numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,numToWarehouseItemID,
					Attributes,bitItemPriceApprovalRequired,AttributeIDs,numPromotionID,bitPromotionTriggered,vcPromotionDetail,numSortOrder,ISNULL(numCost,0) numCost,ISNULL(vcVendorNotes,'') vcVendorNotes
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=2]',2)
				WITH  
					(                      
						numoppitemtCode NUMERIC(9),numItemCode NUMERIC(9),numUnitHour FLOAT,monPrice DECIMAL(30,16),monTotAmount MONEY,vcItemDesc VARCHAR(1000), vcModelID VARCHAR(300),
						numWarehouseItmsID NUMERIC(9),DropShip BIT,bitDiscountType BIT,fltDiscount DECIMAL(30,16),monTotAmtBefDiscount MONEY,vcItemName VARCHAR(300),
						numUOM NUMERIC(9),bitWorkOrder BIT,numVendorWareHouse NUMERIC(9),numShipmentMethod NUMERIC(9),numSOVendorId NUMERIC(9),numProjectID NUMERIC(9),
						numProjectStageID numeric(9),numToWarehouseItemID NUMERIC(9),Attributes varchar(500),bitItemPriceApprovalRequired BIT,AttributeIDs VARCHAR(500),
						numPromotionID NUMERIC(18,0),bitPromotionTriggered BIT,vcPromotionDetail VARCHAR(2000),numSortOrder NUMERIC(18,0),numCost MONEY,vcVendorNotes VARCHAR(1000)
					)
			)X 
			WHERE 
				numoppitemtCode=X.numOppItemID AND numOppID=@numOppID
				
			--INSERT KIT ITEMS                 
			INSERT INTO OpportunityKitItems
			(
				numOppId,
				numOppItemID,
				numChildItemID,
				numWareHouseItemId,
				numQtyItemsReq,
				numQtyItemsReq_Orig,
				numUOMId,
				numQtyShipped,
				monAvgCost
			)
			SELECT
				@numOppId,
				OI.numoppitemtCode,
				ID.numChildItemID,
				ID.numWareHouseItemId,
				(ID.numQtyItemsReq * ISNULL(dbo.fn_UOMConversion(ID.numUOMId,ID.numChildItemID,@numDomainID,I.numBaseUnit),1)) * OI.numUnitHour,
				ID.numQtyItemsReq,
				ID.numUOMId,
				0,
				ISNULL(I.monAverageCost,0)
			FROM 
				OpportunityItems OI 
			JOIN 
				ItemDetails ID 
			ON 
				OI.numItemCode=ID.numItemKitID 
			JOIN
				Item I
			ON
				ID.numChildItemID = I.numItemCode
			WHERE 
				OI.numOppId=@numOppId 

			IF (SELECT COUNT(*) FROM OPENXML (@hDocItem,'/NewDataSet/Item[bitHasKitAsChild=''true'']',2)) > 0
			BEGIN
				--INSERT KIT ITEM HAS ANOTHER KIT AS CHILD THEN SAVE CONFIGURATION OF KIT
				DECLARE @TempKitConfiguration TABLE
				(
					numItemCode NUMERIC(18,0),
					numWarehouseItemID NUMERIC(18,0),
					KitChildItems VARCHAR(MAX)
				)

				INSERT INTO 
					@TempKitConfiguration
				SELECT
					numItemCode,
					numWarehouseItmsID,
					KitChildItems
				FROM
					OPENXML (@hDocItem,'/NewDataSet/Item[bitHasKitAsChild=''true'']',2)
				WITH                       
				(
					numItemCode NUMERIC(9),numWarehouseItmsID NUMERIC(9), KitChildItems VARCHAR(MAX)
				) 

				INSERT INTO OpportunityKitChildItems
				(
					numOppID,
					numOppItemID,
					numOppChildItemID,
					numItemID,
					numWareHouseItemId,
					numQtyItemsReq,
					numQtyItemsReq_Orig,
					numUOMId,
					numQtyShipped,
					monAvgCost
				)
				SELECT 
					@numOppID,
					OKI.numOppItemID,
					OKI.numOppChildItemID,
					ItemDetails.numChildItemID,
					ItemDetails.numWareHouseItemId,
					(ItemDetails.numQtyItemsReq * ISNULL(dbo.fn_UOMConversion(ItemDetails.numUOMId,ItemDetails.numChildItemID,@numDomainID,Item.numBaseUnit),1)) * OKI.numQtyItemsReq,
					ItemDetails.numQtyItemsReq,
					ItemDetails.numUOMId,
					0,
					ISNULL(Item.monAverageCost,0)
				FROM
					(
						SELECT 
							t1.numItemCode,
							t1.numWarehouseItemID,
							SUBSTRING(
										SUBSTRING(items,0,CHARINDEX('-',items)),
										0,
										CHARINDEX('#',SUBSTRING(items,0,CHARINDEX('-',items)))
									  ) AS numKitItemID,
							SUBSTRING(
										SUBSTRING(items,0,CHARINDEX('-',items)),
										CHARINDEX('#',SUBSTRING(items,0,CHARINDEX('-',items))) + 1, 
										LEN(SUBSTRING(items,0,CHARINDEX('-',items))) - CHARINDEX('#',SUBSTRING(items,0,CHARINDEX('-',items)))
									  ) AS numKitWarehouseItemID,
							SUBSTRING(
										SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)), 
										0, 
										CHARINDEX('#',SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)))
									  ) As numKitChildItemID,
							SUBSTRING(
										SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)), 
										CHARINDEX('#',SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items))) + 1, 
										LEN(SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items))) - CHARINDEX('#',SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)))
									  ) As numKitChildItemWarehouseItemID
						FROM 
							@TempKitConfiguration t1
						CROSS APPLY
						(
							SELECT
								*
							FROM 
								dbo.Split((SELECT KitChildItems FROM @TempKitConfiguration WHERE numItemCode=t1.numItemCode AND numWarehouseItemID=t1.numWarehouseItemID),',') 
						) as t2
					) TempChildItems
				INNER JOIN
					OpportunityKitItems OKI
				ON
					OKI.numOppId=@numOppId
					AND OKI.numChildItemID=TempChildItems.numKitItemID
					AND OKI.numWarehouseItemId=TempChildItems.numKitWarehouseItemID
				INNER JOIN
					OpportunityItems OI
				ON
					OI.numItemCode = TempChildItems.numItemCode
					AND OI.numWarehouseItmsID = TempChildItems.numWarehouseItemID
					AND OI.numoppitemtcode = OKI.numOppItemID
				INNER JOIN
					ItemDetails
				ON
					TempChildItems.numKitItemID = ItemDetails.numItemKitID
					AND TempChildItems.numKitChildItemID = ItemDetails.numChildItemID
					AND (TempChildItems.numKitChildItemWarehouseItemID = ItemDetails.numWareHouseItemId  OR ISNULL(TempChildItems.numKitChildItemWarehouseItemID,0) = 0)
				INNER JOIN
					Item 
				ON
					ItemDetails.numChildItemID = Item.numItemCode
			END

			-- UPDATE UOM OF OPPORTUNITY ITEMS HAVE NOT UOM SET WHILE EXPORTED FROM MARKETPLACE (MEANS TINTSOURCETYPE = 3)
			IF ISNULL(@tintSourceType,0) = 3
			BEGIN
				UPDATE 
					OI 
				SET 
					numUOMID = ISNULL(I.numSaleUnit,0), 
					numUnitHour = dbo.fn_UOMConversion(ISNULL(I.numSaleUnit,0),OI.numItemCode,@numDomainId,null) * OI.numUnitHour
				FROM 
					OpportunityItems OI
				JOIN 
					Item I 
				ON 
					OI.numItemCode = I.numItemCode
				WHERE 
					numOppId = @numOppID
					AND I.numDomainID = @numDomainId
			END	
                                     
			INSERT INTO OppWarehouseSerializedItem
			(
				numOppID,
				numWarehouseItmsDTLID,
				numOppItemID,
				numWarehouseItmsID
			)                      
			SELECT 
				@numOppID,
				X.numWarehouseItmsDTLID,
				X.numoppitemtCode,
				X.numWItmsID 
			FROM
			(                                                                          
				SELECT
					* 
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/SerialNo',2)                                                                          
				WITH                        
				(                                               
				numWarehouseItmsDTLID numeric(9),                      
				numoppitemtCode numeric(9),                      
				numWItmsID numeric(9)                                                     
				)
			)X                                    
                                   
			UPDATE 
				OppWarehouseSerializedItem                       
			SET 
				numOppItemID=X.numoppitemtCode                                
			FROM                       
			(
				SELECT 
					numWarehouseItmsDTLID AS DTLID,
					O.numoppitemtCode 
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/SerialNo',2)                                                                         
				WITH
				(
					numWarehouseItmsDTLID NUMERIC(9),numoppitemtCode NUMERIC(9),numWItmsID NUMERIC(9)
				)                                
				JOIN 
					OpportunityItems O 
				ON 
					O.numWarehouseItmsID=numWItmsID and O.numOppID=@numOppID
			)X                 
			WHERE 
				numOppID=@numOppID 
				AND numWarehouseItmsDTLID=X.DTLID      
    
			EXEC USP_ManageSOWorkOrder @numDomainId,@numOppID,@strItems,@numUserCntID
                     
			EXEC sp_xml_removedocument @hDocItem                                     
		END          
	END
	ELSE                                                                                                                          
	BEGIN
		DECLARE @tempAssignedTo AS NUMERIC(9) = NULL                                                    
		SELECT 
			@tintOppStatus = tintOppStatus,
			@tempAssignedTo=ISNULL(numAssignedTo,0) 
		FROM   
			OpportunityMaster 
		WHERE  
			numOppID = @numOppID

		IF @tempAssignedTo <> @numAssignedTo
		BEGIN
			IF (SELECT COUNT(*) FROM BizDocComission WHERE ISNULL(numOppID,0) = @numOppID AND ISNULL(bitCommisionPaid,0) = 1) > 0
			BEGIN
				RAISERROR('COMMISSION_PAID_AGAINST_BIZDOC',16,1)
			END
		END
 
		--REVERTING BACK THE WAREHOUSE ITEMS                  
		IF @tintOppStatus = 1 
		BEGIN               
			EXEC USP_RevertDetailsOpp @numOppID,0,@numUserCntID                  
		END                  

		IF LEN(ISNULL(@vcOppRefOrderNo,''))>0
		BEGIN
			UPDATE dbo.OpportunityBizDocs SET vcRefOrderNo=@vcOppRefOrderNo WHERE numOppId=@numOppID
		END
		
		IF EXISTS (SELECT 1 FROM dbo.OpportunityMaster WHERE numOppId = @numOppID AND tintOppType=1 AND isnull(tintOppStatus,0)=1 AND ISNULL(numStatus,0)!= @numStatus)
		BEGIN
			EXEC USP_ManageOpportunityAutomationQueue
					@numOppQueueID = 0, -- numeric(18, 0)
					@numDomainID = @numDomainID, -- numeric(18, 0)
					@numOppId = @numOppID, -- numeric(18, 0)
					@numOppBizDocsId = 0, -- numeric(18, 0)
					@numOrderStatus = @numStatus, -- numeric(18, 0)
					@numUserCntID = @numUserCntID, -- numeric(18, 0)
					@tintProcessStatus = 1, -- tinyint
					@tintMode = 1 -- TINYINT
		END 
						
		UPDATE   
			OpportunityMaster
		SET      
			vcPOppName = @vcPOppName,txtComments = ISNULL(@Comments,''),bitPublicFlag = @bitPublicFlag,numCampainID = @CampaignID,tintSource = @tintSource,tintSourceType=@tintSourceType,
			intPEstimatedCloseDate = @dtEstimatedCloseDate,numModifiedBy = @numUserCntID,bintModifiedDate = GETUTCDATE(),lngPConclAnalysis = @lngPConclAnalysis,monPAmount = @monPAmount,
			tintActive = @tintActive,numSalesOrPurType = @numSalesOrPurType,numStatus = @numStatus,vcOppRefOrderNo=@vcOppRefOrderNo,
			bintOppToOrder=(CASE WHEN @tintOppStatus=0 AND @DealStatus=1 THEN GETUTCDATE() ELSE bintOppToOrder END),bitDiscountType=@bitDiscountType,fltDiscount=@fltDiscount,
			bitBillingTerms=@bitBillingTerms,intBillingDays=@intBillingDays,bitInterestType=@bitInterestType,fltInterest=@fltInterest,tintTaxOperator=@tintTaxOperator,
			numDiscountAcntType=@numDiscountAcntType,vcCouponCode = @vcCouponCode,numPercentageComplete =@numPercentageComplete,bitUseShippersAccountNo = @bitUseShippersAccountNo,
			bitUseMarkupShippingRate = @bitUseMarkupShippingRate,numMarkupShippingRate = @numMarkupShippingRate,intUsedShippingCompany = @intUsedShippingCompany, numContactID=@numContactId,numShipmentMethod=@numShipmentMethod,numPartner=@numPartner,numPartenerContact=@numPartenerContactId
		WHERE
			numOppId = @numOppID   
	   
		-- UPDATING IF ORGANIZATION IS ASSIGNED TO SOMEONE                                                      
		IF ( @tempAssignedTo <> @numAssignedTo AND @numAssignedTo <> '0' ) 
		BEGIN
			UPDATE
				OpportunityMaster 
			SET	
				numAssignedTo = @numAssignedTo, 
				numAssignedBy = @numUserCntID 
			WHERE 
				numOppId = @numOppID                                                    
		END                                                     
		ELSE IF(@numAssignedTo = 0) 
		BEGIN
			UPDATE 
				OpportunityMaster 
			SET 
				numAssignedTo = 0, 
				numAssignedBy = 0 
			WHERE
				numOppId = @numOppID
		END  
			                                                                    
		-- UPDATING OPP ITEMS
		IF CONVERT(VARCHAR(10),@strItems) <> '' AND @numOppID > 0                                                                         
		BEGIN
			EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems                                      
			   
			IF EXISTS 
			(
				SELECT 
					OI.numoppitemtCode
				FROM 
					OpportunityItems OI
				WHERE 
					numOppID=@numOppID 
					AND OI.numoppitemtCode NOT IN (SELECT numoppitemtCode FROM OPENXML (@hDocItem,'/NewDataSet/Item',2) WITH (numoppitemtCode NUMERIC(9)))
					AND ISNULL((SELECT COUNT(*) FROM OppWarehouseSerializedItem WHERE numOppId=@numOppID AND numOppItemID=OI.numoppitemtCode),0) > 0
			)
			BEGIN
				RAISERROR('SERIAL/LOT#_ASSIGNED_TO_ITEM_DELETED',16,1)
			END

			--DELETE CHILD ITEMS OF KIT WITHIN KIT
			DELETE FROM 
				OpportunityKitChildItems 
			WHERE 
				numOppID=@numOppID AND 
				numOppItemID NOT IN (SELECT ISNULL(numoppitemtCode,0) FROM OPENXML (@hDocItem,'/NewDataSet/Item',2) WITH (numoppitemtCode NUMERIC(9)))   

			--DELETE ITEMS
			DELETE FROM 
				OpportunityKitItems 
			WHERE 
				numOppID=@numOppID AND 
				numOppItemID NOT IN (SELECT ISNULL(numoppitemtCode,0) FROM OPENXML (@hDocItem,'/NewDataSet/Item',2) WITH (numoppitemtCode NUMERIC(9)))                      
			                            
			DELETE FROM 
				OpportunityItems 
			WHERE 
				numOppID=@numOppID 
				AND numoppitemtCode NOT IN (SELECT numoppitemtCode FROM OPENXML (@hDocItem,'/NewDataSet/Item',2) WITH (numoppitemtCode NUMERIC(9)))   
	          
			--INSERT NEW ADDED ITEMS                        
			INSERT INTO OpportunityItems
			(
				numOppId,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount,
				[vcItemName],[vcModelID],[vcManufacturer],vcPathForTImage,monVendorCost,numUOMId,bitWorkOrder,numVendorWareHouse,numToWarehouseItemID,numShipmentMethod,
				numSOVendorId,numProjectID,numProjectStageID,vcAttributes,numClassID,monAvgCost,bitItemPriceApprovalRequired,vcAttrValues,numPromotionID,bitPromotionTriggered,vcPromotionDetail,numSortOrder,numCost,vcNotes
			)
			SELECT 
				@numOppID,X.numItemCode,dbo.fn_UOMConversion(X.numUOM,numItemCode,@numDomainId,null) * x.numUnitHour,x.monPrice,x.monTotAmount,X.vcItemDesc,X.numWarehouseItmsID,
				X.ItemType,X.DropShip,X.bitDiscountType,X.fltDiscount,X.monTotAmtBefDiscount,X.vcItemName,(SELECT vcModelID FROM item WHERE numItemCode = X.numItemCode),
				(SELECT vcManufacturer FROM item WHERE numItemCode = X.numItemCode),
				(SELECT TOP 1 vcPathForTImage FROM dbo.ItemImages WHERE numItemCode = X.numItemCode AND BitDefault = 1 AND numDomainId = @numDomainId),
				(select isnull(VN.monCost,0) from Vendor VN INNER JOIN Item IT ON IT.numItemCode=VN.numItemCode and VN.numVendorID=IT.numVendorID where VN.numItemCode=X.numItemCode),
				X.numUOM,X.bitWorkOrder,X.numVendorWareHouse,X.numToWarehouseItemID,X.numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,Attributes,
				0,
				(SELECT (CASE WHEN ISNULL(I.bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(I.monAverageCost,0) END) FROM Item I WHERE I.numDomainId=@numDomainId AND I.numItemCode=X.numItemCode), X.bitItemPriceApprovalRequired, X.AttributeIDs,
				X.numPromotionID,X.bitPromotionTriggered,X.vcPromotionDetail,X.numSortOrder,ISNULL(X.numCost,0),ISNULL(vcVendorNotes,'')
			FROM
			(
				SELECT 
					*
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=1]',2)                                                                          
				WITH  
				(                      
					numoppitemtCode numeric(9),numItemCode numeric(9),numUnitHour FLOAT,monPrice DECIMAL(30,16),monTotAmount money,vcItemDesc varchar(1000),                                    
					numWarehouseItmsID numeric(9),numToWarehouseItemID numeric(9),Op_Flag tinyint,ItemType varchar(30),DropShip bit,bitDiscountType bit,
					fltDiscount DECIMAL(30,16),monTotAmtBefDiscount MONEY,vcItemName VARCHAR(300),numUOM NUMERIC(9),bitWorkOrder BIT,numVendorWareHouse NUMERIC(9),
					numShipmentMethod NUMERIC(9),numSOVendorId numeric(9),numProjectID numeric(9),numProjectStageID numeric(9),Attributes varchar(500), 
					bitItemPriceApprovalRequired BIT,AttributeIDs VARCHAR(500),numPromotionID NUMERIC(18,0),bitPromotionTriggered BIT,vcPromotionDetail VARCHAR(2000)
					,numSortOrder NUMERIC(18,0),numCost MONEY,vcVendorNotes VARCHAR(1000)
				)
			)X 
			
			-- UPDATE ITEMS
			UPDATE 
				OpportunityItems                       
			SET 
				numItemCode=X.numItemCode,numOppId=@numOppID,numUnitHour=dbo.fn_UOMConversion(X.numUOM,X.numItemCode,@numDomainId,null) * x.numUnitHour,                      
				monPrice=x.monPrice,monTotAmount=x.monTotAmount,vcItemDesc=X.vcItemDesc,vcModelID=X.vcModelID,numWarehouseItmsID=X.numWarehouseItmsID,numToWarehouseItemID = X.numToWarehouseItemID,             
				bitDropShip=X.DropShip,bitDiscountType=X.bitDiscountType,fltDiscount=X.fltDiscount,monTotAmtBefDiscount=X.monTotAmtBefDiscount,vcItemName=X.vcItemName,
				numUOMId=numUOM,numVendorWareHouse=X.numVendorWareHouse,numShipmentMethod=X.numShipmentMethod,numSOVendorId=X.numSOVendorId,numProjectId=X.numProjectID,
				numProjectStageId=X.numProjectStageID,vcAttributes=X.Attributes,bitItemPriceApprovalRequired=X.bitItemPriceApprovalRequired,bitWorkOrder=X.bitWorkOrder,vcAttrValues=AttributeIDs,
				numPromotionID=X.numPromotionID,bitPromotionTriggered=X.bitPromotionTriggered,vcPromotionDetail=X.vcPromotionDetail,numSortOrder=X.numSortOrder,numCost=X.numCost,vcNotes=vcVendorNotes
			FROM 
			(
				SELECT 
					numoppitemtCode as numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,vcModelID,numWarehouseItmsID,numToWarehouseItemID,DropShip,bitDiscountType,
					fltDiscount,monTotAmtBefDiscount,vcItemName,numUOM,numVendorWareHouse,numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,Attributes,
					bitItemPriceApprovalRequired,bitWorkOrder,AttributeIDs,numPromotionID,bitPromotionTriggered,vcPromotionDetail,numSortOrder,ISNULL(numCost,0) AS numCost,ISNULL(vcVendorNotes,'') vcVendorNotes
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=2]',2)                                                                
				WITH  
				(                      
					numoppitemtCode NUMERIC(9),numItemCode NUMERIC(9),numUnitHour FLOAT,monPrice DECIMAL(30,16),monTotAmount MONEY,vcItemDesc VARCHAR(1000),vcModelID VARCHAR(300), numWarehouseItmsID NUMERIC(9), 
					numToWarehouseItemID NUMERIC(18,0),DropShip BIT,bitDiscountType BIT,fltDiscount DECIMAL(30,16),monTotAmtBefDiscount MONEY,vcItemName VARCHAR(300),numUOM NUMERIC(9),
					numVendorWareHouse NUMERIC(9),numShipmentMethod NUMERIC(9),numSOVendorId NUMERIC(9),numProjectID NUMERIC(9),numProjectStageID NUMERIC(9),Attributes VARCHAR(500),
					bitItemPriceApprovalRequired BIT, bitWorkOrder BIT,AttributeIDs VARCHAR(500),numPromotionID NUMERIC(18,0),bitPromotionTriggered BIT,vcPromotionDetail VARCHAR(2000),numSortOrder NUMERIC(18,0),numCost MONEY,vcVendorNotes VARCHAR(1000)                           
				)
			)X 
			WHERE 
				numoppitemtCode=X.numOppItemID AND numOppId=@numOppID

			--INSERT NEW ADDED KIT ITEMS                 
			INSERT INTO OpportunityKitItems                                                                          
			(
				numOppId,
				numOppItemID,
				numChildItemID,
				numWareHouseItemId,
				numQtyItemsReq,
				numQtyItemsReq_Orig,
				numUOMId,
				numQtyShipped,
				monAvgCost
			)
			SELECT
				@numOppId,
				OI.numoppitemtCode,
				ID.numChildItemID,
				ID.numWareHouseItemId,
				ID.numQtyItemsReq * OI.numUnitHour,
				ID.numQtyItemsReq,
				ID.numUOMId,
				0,
				ISNULL(I.monAverageCost,0)
			FROM 
				OpportunityItems OI 
			JOIN 
				ItemDetails ID 
			ON 
				OI.numItemCode=ID.numItemKitID
			INNER JOIN
				Item I
			ON
				ID.numChildItemID = I.numItemCode
			WHERE 
				OI.numOppId=@numOppId 
				AND OI.numoppitemtCode NOT IN (SELECT ISNULL(numOppItemID,0) FROM OpportunityKitItems OKI WHERE OKI.numOppId=@numOppID)  
				
			IF (SELECT COUNT(*) FROM OPENXML (@hDocItem,'/NewDataSet/Item[bitHasKitAsChild=''true'']',2)) > 0
			BEGIN
				--INSERT KIT ITEM HAS ANOTHER KIT AS CHILD THEN SAVE CONFIGURATION OF KIT
				DELETE FROM @TempKitConfiguration

				INSERT INTO 
					@TempKitConfiguration
				SELECT
					numItemCode,
					numWarehouseItmsID,
					KitChildItems
				FROM
					OPENXML (@hDocItem,'/NewDataSet/Item[bitHasKitAsChild=''true'']',2)
				WITH                       
				(
					numItemCode NUMERIC(9),numWarehouseItmsID NUMERIC(9), KitChildItems VARCHAR(MAX)
				) 

				INSERT INTO OpportunityKitChildItems
				(
					numOppID,
					numOppItemID,
					numOppChildItemID,
					numItemID,
					numWareHouseItemId,
					numQtyItemsReq,
					numQtyItemsReq_Orig,
					numUOMId,
					numQtyShipped,
					monAvgCost
				)
				SELECT 
					@numOppID,
					OKI.numOppItemID,
					OKI.numOppChildItemID,
					ItemDetails.numChildItemID,
					ItemDetails.numWareHouseItemId,
					ItemDetails.numQtyItemsReq * OKI.numQtyItemsReq,
					ItemDetails.numQtyItemsReq,
					ItemDetails.numUOMId,
					0,
					ISNULL(Item.monAverageCost,0)
				FROM
					(
						SELECT 
							t1.numItemCode,
							t1.numWarehouseItemID,
							SUBSTRING(
										SUBSTRING(items,0,CHARINDEX('-',items)),
										0,
										CHARINDEX('#',SUBSTRING(items,0,CHARINDEX('-',items)))
									  ) AS numKitItemID,
							SUBSTRING(
										SUBSTRING(items,0,CHARINDEX('-',items)),
										CHARINDEX('#',SUBSTRING(items,0,CHARINDEX('-',items))) + 1, 
										LEN(SUBSTRING(items,0,CHARINDEX('-',items))) - CHARINDEX('#',SUBSTRING(items,0,CHARINDEX('-',items)))
									  ) AS numKitWarehouseItemID,
							SUBSTRING(
										SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)), 
										0, 
										CHARINDEX('#',SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)))
									  ) As numKitChildItemID,
							SUBSTRING(
										SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)), 
										CHARINDEX('#',SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items))) + 1, 
										LEN(SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items))) - CHARINDEX('#',SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)))
									  ) As numKitChildItemWarehouseItemID
						FROM 
							@TempKitConfiguration t1
						CROSS APPLY
						(
							SELECT
								*
							FROM 
								dbo.Split((SELECT KitChildItems FROM @TempKitConfiguration WHERE numItemCode=t1.numItemCode AND numWarehouseItemID=t1.numWarehouseItemID),',') 
						) as t2
					) TempChildItems
				INNER JOIN
					OpportunityKitItems OKI
				ON
					OKI.numOppId=@numOppId
					AND OKI.numChildItemID=TempChildItems.numKitItemID
					AND OKI.numWarehouseItemId=TempChildItems.numKitWarehouseItemID
				INNER JOIN
					OpportunityItems OI
				ON
					OI.numItemCode = TempChildItems.numItemCode
					AND OI.numWarehouseItmsID = TempChildItems.numWarehouseItemID
					AND OI.numoppitemtcode = OKI.numOppItemID
				INNER JOIN
					ItemDetails
				ON
					TempChildItems.numKitItemID = ItemDetails.numItemKitID
					AND TempChildItems.numKitChildItemID = ItemDetails.numChildItemID
					AND (TempChildItems.numKitChildItemWarehouseItemID = ItemDetails.numWareHouseItemId  OR ISNULL(TempChildItems.numKitChildItemWarehouseItemID,0) = 0)
				INNER JOIN
					Item 
				ON
					ItemDetails.numChildItemID = Item.numItemCode
			END                                  
			
			--UPDATE KIT ITEMS                 
			UPDATE 
				OKI 
			SET 
				numQtyItemsReq = numQtyItemsReq_Orig * OI.numUnitHour
			FROM 
				OpportunityKitItems OKI 
			JOIN 
				OpportunityItems OI 
			ON 
				OKI.numOppItemID=OI.numoppitemtCode 
				AND OKI.numOppId=OI.numOppId  
			WHERE 
				OI.numOppId=@numOppId 

			-- UPDATE CHILD ITEMS OF KIT WITHIN KIT
			UPDATE
				OKCI
			SET
				OKCI.numQtyItemsReq = (OKI.numQtyItemsReq * OKCI.numQtyItemsReq_Orig)
			FROM
				OpportunityKitChildItems OKCI
			INNER JOIN
				OpportunityKitItems OKI
			ON
				OKCI.numOppID = @numOppId
				AND OKCI.numOppItemID = OKI.numOppItemID
				AND OKCI.numOppChildItemID = OKI.numOppChildItemID                     
			                                    
			EXEC USP_ManageSOWorkOrder @numDomainId,@numOppID,@strItems,@numUserCntID
			   		                                                                      
			EXEC sp_xml_removedocument @hDocItem                                               
		END     

		--FOLLOWING DELETE STATEMENT IS COMMENTED BECUASE NOW WE ARE NOT ALLOWING USER TO EIDT SERIAL/LOT ITEM UNTILL USER REMOVED ASSIGNED SERIAL/LOT NUMBERS
	    --Delete previously added serial lot number added for opportunity
		--DELETE FROM OppWarehouseSerializedItem WHERE numOppID = @numOppID                                                         
	END


	SET @TotalAmount=0                                                           
	SELECT @TotalAmount = SUM(monTotAmount) FROM OpportunityItems WHERE numOppId=@numOppID                                                                          
  
	UPDATE OpportunityMaster SET monPamount=@TotalAmount WHERE numOppId=@numOppID

/*Added by chintan - While creating SO from PO it wasn't puting Item Qty to Allocation. this segment was being called from USP_OppSaveMilestone which No longer in use after changes anoop did. */     
--Updating the warehouse items              
            
			declare @bitApprovalforOpportunity bit
			declare @intOpportunityApprovalProcess int
			SELECT @bitApprovalforOpportunity=bitApprovalforOpportunity,@intOpportunityApprovalProcess=intOpportunityApprovalProcess 
					FROM Domain WHERE numDomainId=@numDomainID
			DECLARE @bitViolatePrice BIT
			DECLARE @intPendingApprovePending INT

			SET @bitIsInitalSalesOrder=(SELECT bitIsInitalSalesOrder FROM OpportunityMaster WHERE numOppId=@numOppID AND numDomainId=@numDomainID)
			SET @bitViolatePrice=(SELECT bitMarginPriceViolated FROM Domain WHERE numDomainId=@numDomainID)
			
			IF(@bitViolatePrice=1 AND @bitIsInitalSalesOrder=1 AND @tintClickBtn = 1)
			BEGIN
				UPDATE OpportunityMaster SET tintOppStatus=@DealStatus WHERE numOppId=@numOppID
			END
			ELSE
			BEGIN
				IF(@bitApprovalforOpportunity=1)
				BEGIN
					IF(@DealStatus='1' OR @DealStatus='2')
					BEGIN
						IF(@intOpportunityApprovalProcess=1)
						BEGIN
							UPDATE OpportunityMaster SET intPromotionApprovalStatus=6,intChangePromotionStatus=@DealStatus
							WHERE numOppId=@numOppId
						END
						IF(@intOpportunityApprovalProcess=2)
						BEGIN
							UPDATE OpportunityMaster SET intPromotionApprovalStatus=1,intChangePromotionStatus=@DealStatus
							WHERE numOppId=@numOppId
						END
					END
				END

				UPDATE OpportunityMaster SET tintOppStatus = @DealStatus WHERE numOppId=@numOppID
				declare @tintShipped as tinyint               
				select @tintOppStatus=tintOppStatus,@tintOppType=tintOppType,@tintShipped=tintShipped from OpportunityMaster where numOppID=@numOppID              
				if @tintOppStatus=1               
					begin         
					PRINT 'inside USP_UpdatingInventoryonCloseDeal'
						exec USP_UpdatingInventoryonCloseDeal @numOppID,@numUserCntID
					end  
				declare @tintCRMType as numeric(9)        
				declare @AccountClosingDate as datetime        

				select @numDivisionID=numDivisionID,@AccountClosingDate=bintAccountClosingDate from  OpportunityMaster where numOppID=@numOppID         
				select @tintCRMType=tintCRMType from divisionmaster where numDivisionID =@numDivisionID 

				--When deal is won                                         
				IF @DealStatus = 1 
				BEGIN
					IF @AccountClosingDate IS NULL                   
					BEGIN
						UPDATE OpportunityMaster SET bintAccountClosingDate=GETUTCDATE() WHERE numOppID=@numOppID              
					END

					-- Promote Lead to Account when Sales/Purchase Order is created against it
					IF @tintCRMType=0 --Lead & Order
					BEGIN
						UPDATE 
							divisionmaster 
						SET 
							tintCRMType=2
							,bintLeadProm=getutcdate()
							,bintLeadPromBy=@numUserCntID
							,bintProsProm=getutcdate()
							,bintProsPromBy=@numUserCntID        
						WHERE 
							numDivisionID=@numDivisionID        
					END
					--Promote Prospect to Account
					ELSE IF @tintCRMType=1 
					BEGIN
						UPDATE 
							divisionmaster 
						SET 
							tintCRMType=2
							,bintProsProm=getutcdate()
							,bintProsPromBy=@numUserCntID        
						WHERE 
							numDivisionID=@numDivisionID        
					END
				END         
				--When Deal is Lost
				ELSE IF @DealStatus = 2
				BEGIN
					SELECT @AccountClosingDate=bintAccountClosingDate FROM OpportunityMaster WHERE numOppID=@numOppID         
					
					IF @AccountClosingDate IS NULL                   
					BEGIN
						UPDATE OpportunityMaster SET bintAccountClosingDate=GETUTCDATE() WHERE numOppID=@numOppID
					END
				END 
			END

			/*Added by chintan -  Biz usually automatically promotes the record from Lead/Prospect to Account When a Sales Opp/Order is made against it*/  
			-- Promote Lead to Prospect when Sales/Purchase Opp is created against it
			if @tintCRMType=0 AND @DealStatus = 0 --Lead & Open Opp
			begin        
				update divisionmaster set tintCRMType=1,bintLeadProm=getutcdate(),bintLeadPromBy=@numUserCntID,bintProsProm=getutcdate(),bintProsPromBy=@numUserCntID        
				where numDivisionID=@numDivisionID        
			end        

--Add/Update Address Details
DECLARE @vcStreet  varchar(100),@vcCity  varchar(50),@vcPostalCode  varchar(15),@vcCompanyName  varchar(100),@vcAddressName VARCHAR(50), @vcAltContact VARCHAR(200)      
DECLARE @numState  numeric(9),@numCountry  numeric(9), @numCompanyId  numeric(9), @numContact NUMERIC(18,0)
DECLARE @bitIsPrimary BIT, @bitAltContact BIT;
DECLARE @tintAddressOf TINYINT

select  @vcCompanyName=vcCompanyName,@numCompanyId=div.numCompanyID from CompanyInfo Com                            
join divisionMaster Div                            
on div.numCompanyID=com.numCompanyID                            
where div.numdivisionID=@numDivisionId

--Bill Address
IF @numBillAddressId>0
BEGIN
	SELECT 
		@vcStreet=ISNULL(vcStreet,'')
		,@vcCity=ISNULL(vcCity,'')
		,@vcPostalCode=ISNULL(vcPostalCode,'')
		,@numState=ISNULL(numState,0)
		,@numCountry=ISNULL(numCountry,0)
		,@bitIsPrimary=bitIsPrimary
		,@vcAddressName=vcAddressName
		,@tintAddressOf=ISNULL(tintAddressOf,0)
		,@numContact=numContact
		,@bitAltContact=bitAltContact
		,@vcAltContact=vcAltContact
    FROM 
		dbo.AddressDetails 
	WHERE 
		numDomainID = @numDomainID 
		AND numAddressID = @numBillAddressId

	IF @tintAddressOf = 2 AND @tintOppType = 2
	BEGIN
		SELECT
			@vcCompanyName = ISNULL(vcCompanyName,'')
			,@numCompanyId = ISNULL(CompanyInfo.numCompanyId,0)
		FROM
			AddressDetails
		INNER JOIN
			DivisionMaster 
		ON
			AddressDetails.numRecordID = DivisionMaster.numDivisionID
		INNER JOIN
			CompanyInfo
		ON
			DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
		WHERE 
			AddressDetails.numDomainID = @numDomainID 
			AND numAddressID = @numBillAddressId
	END

  	EXEC dbo.USP_UpdateOppAddress
	@numOppID = @numOppID, --  numeric(9, 0)
	@byteMode = 0, --  tinyint
	@vcStreet = @vcStreet, --  varchar(100)
	@vcCity = @vcCity, --  varchar(50)
	@vcPostalCode = @vcPostalCode, --  varchar(15)
	@numState = @numState, --  numeric(9, 0)
	@numCountry = @numCountry, --  numeric(9, 0)
	@vcCompanyName = @vcCompanyName, --  varchar(100)
	@numCompanyId = 0, --  numeric(9, 0)
	@vcAddressName = @vcAddressName,
	@bitCalledFromProcedure = 1,
	@numContact = @numContact,
	@bitAltContact = @bitAltContact,
	@vcAltContact = @vcAltContact
END
  
  
--Ship Address
IF @numShipmentMethod = 92 -- WILL CALL (PICK UP FROM Warehouse)
BEGIN
	SELECT  
		@vcStreet=ISNULL(vcWStreet,'')
		,@vcCity=ISNULL(vcWCity,'')
		,@vcPostalCode=ISNULL(vcWPinCode,'')
		,@numState=ISNULL(numWState,0)
		,@numCountry=ISNULL(numWCountry,0)
		,@vcAddressName=''
	FROM 
		dbo.Warehouses 
	WHERE 
		numDomainID = @numDomainID 
		AND numWareHouseID = @numWillCallWarehouseID

	UPDATE OpportunityMaster SET tintShipToType=2 WHERE numOppId=@numOppID
 
  	EXEC dbo.USP_UpdateOppAddress
	@numOppID = @numOppID, --  numeric(9, 0)
	@byteMode = 1, --  tinyint
	@vcStreet = @vcStreet, --  varchar(100)
	@vcCity = @vcCity, --  varchar(50)
	@vcPostalCode = @vcPostalCode, --  varchar(15)
	@numState = @numState, --  numeric(9, 0)
	@numCountry = @numCountry, --  numeric(9, 0)
	@vcCompanyName = '', --  varchar(100)
	@numCompanyId =0, --  numeric(9, 0)
	@vcAddressName = @vcAddressName,
	@bitCalledFromProcedure = 1,
	@numContact = 0,
	@bitAltContact = 0,
	@vcAltContact = ''
END
ELSE IF @numShipAddressId>0
BEGIN
	SELECT
		@vcStreet=ISNULL(vcStreet,'')
		,@vcCity=ISNULL(vcCity,'')
		,@vcPostalCode=ISNULL(vcPostalCode,'')
		,@numState=ISNULL(numState,0)
		,@numCountry=ISNULL(numCountry,0)
		,@bitIsPrimary=bitIsPrimary
		,@vcAddressName=vcAddressName
		,@tintAddressOf=ISNULL(tintAddressOf,0)
		,@numContact=numContact
		,@bitAltContact=bitAltContact
		,@vcAltContact=vcAltContact
    FROM  
		dbo.AddressDetails 
	WHERE 
		numDomainID = @numDomainID 
		AND numAddressID = @numShipAddressId
 

	IF @tintAddressOf = 2 AND @tintOppType = 2
	BEGIN
		SELECT
			@vcCompanyName = ISNULL(vcCompanyName,'')
			,@numCompanyId = ISNULL(CompanyInfo.numCompanyId,0)
		FROM
			AddressDetails
		INNER JOIN
			DivisionMaster 
		ON
			AddressDetails.numRecordID = DivisionMaster.numDivisionID
		INNER JOIN
			CompanyInfo
		ON
			DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
		WHERE 
			AddressDetails.numDomainID = @numDomainID 
			AND numAddressID = @numShipAddressId
	END


  	EXEC dbo.USP_UpdateOppAddress
	@numOppID = @numOppID, --  numeric(9, 0)
	@byteMode = 1, --  tinyint
	@vcStreet = @vcStreet, --  varchar(100)
	@vcCity = @vcCity, --  varchar(50)
	@vcPostalCode = @vcPostalCode, --  varchar(15)
	@numState = @numState, --  numeric(9, 0)
	@numCountry = @numCountry, --  numeric(9, 0)
	@vcCompanyName = @vcCompanyName, --  varchar(100)
	@numCompanyId =@numCompanyId, --  numeric(9, 0)
	@vcAddressName = @vcAddressName,
	@bitCalledFromProcedure = 1,
	@numContact = @numContact,
	@bitAltContact = @bitAltContact,
	@vcAltContact = @vcAltContact
END

-- IMPORTANT:KEEP THIS BELOW ADDRESS UPDATE LOGIC
-- GET TAX APPLICABLE TO DIVISION WHEN ORDER IS CREATED AFTER THAT NOT NEED TO CHANGE IT
IF @bitNewOrder = 1
BEGIN
	--INSERT TAX FOR DIVISION   
	IF @tintOppType=1 OR (@tintOppType=2 and (select isnull(bitPurchaseTaxCredit,0) from Domain where numDomainId=@numDomainId)=1)
	BEGIN
		INSERT INTO dbo.OpportunityMasterTaxItems 
		(
			numOppId,
			numTaxItemID,
			fltPercentage,
			tintTaxType,
			numTaxID
		) 
		SELECT 
			@numOppID,
			TI.numTaxItemID,
			TEMPTax.decTaxValue,
			TEMPTax.tintTaxType,
			0
		FROM 
			TaxItems TI 
		JOIN 
			DivisionTaxTypes DTT 
		ON 
			TI.numTaxItemID = DTT.numTaxItemID
		CROSS APPLY
		(
			SELECT
				decTaxValue,
				tintTaxType
			FROM
				dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,TI.numTaxItemID,@numOppId,0,NULL)
		) AS TEMPTax
		WHERE 
			DTT.numDivisionID=@numDivisionId 
			AND DTT.bitApplicable=1
		UNION 
		SELECT 
			@numOppID,
			0,
			TEMPTax.decTaxValue,
			TEMPTax.tintTaxType,
			0
		FROM 
			dbo.DivisionMaster 
		CROSS APPLY
		(
			SELECT
				decTaxValue,
				tintTaxType
			FROM
				dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,0,@numOppId,1,NULL)
		) AS TEMPTax
		WHERE 
			bitNoTax=0 
			AND numDivisionID=@numDivisionID	
		UNION 
		SELECT
			@numOppId
			,1
			,decTaxValue
			,tintTaxType
			,numTaxID
		FROM
			dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,1,@numOppId,1,NULL)		
	END	
END

--INSERT TAX FOR DIVISION   
IF @tintOppType=1 OR (@tintOppType=2 and (select isnull(bitPurchaseTaxCredit,0) from Domain where numDomainId=@numDomainId)=1)
BEGIN
--Delete Tax for Opportunity Items if item deleted 
DELETE FROM OpportunityItemsTaxItems WHERE numOppId=@numOppId AND numOppItemID NOT IN (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId=@numOppID)

--Insert Tax for Opportunity Items
INSERT INTO dbo.OpportunityItemsTaxItems 
(
	numOppId,
	numOppItemID,
	numTaxItemID,
	numTaxID
) 
SELECT 
	@numOppId,
	OI.numoppitemtCode,
	TI.numTaxItemID,
	0
FROM 
	dbo.OpportunityItems OI 
JOIN 
	dbo.ItemTax IT 
ON 
	OI.numItemCode=IT.numItemCode 
JOIN
	TaxItems TI 
ON 
	TI.numTaxItemID = IT.numTaxItemID 
WHERE 
	OI.numOppId=@numOppID 
	AND IT.bitApplicable=1 
	AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)
UNION
SELECT 
	@numOppId,
	OI.numoppitemtCode,
	0,
	0
FROM 
	dbo.OpportunityItems OI 
JOIN 
	dbo.Item I 
ON 
	OI.numItemCode=I.numItemCode
WHERE 
	OI.numOppId=@numOppID 
	AND I.bitTaxable=1 
	AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)
UNION
SELECT
	@numOppId,
	OI.numoppitemtCode,
	1,
	TD.numTaxID
FROM
	dbo.OpportunityItems OI 
INNER JOIN
	ItemTax IT
ON
	IT.numItemCode = OI.numItemCode
INNER JOIN
	TaxDetails TD
ON
	TD.numTaxID = IT.numTaxID
	AND TD.numDomainId = @numDomainId
WHERE
	OI.numOppId = @numOppID
	AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)

END
  
 UPDATE OpportunityMaster SET monDealAmount=dbo.GetDealAmount(@numOppID,getdate(),0) WHERE numOppId=@numOppID

	IF @tintOppType=1 AND @tintOppStatus=1 
	BEGIN
		EXEC USP_SalesFulfillmentQueue_Save @numDomainID,@numUserCntID,@numOppID,@numStatus
	END
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SET @numOppID = 0

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_Opportunity_BulkSales')
DROP PROCEDURE USP_Opportunity_BulkSales
GO
CREATE PROCEDURE [dbo].[USP_Opportunity_BulkSales]  
  
AS  
BEGIN
	DECLARE @TEMP TABLE
	(
		ID INT,
		numMSOQID NUMERIC(18,0),
		numDivisionID NUMERIC(18,0),
		numContactID NUMERIC(18,0)
	)

	DECLARE @TEMPMasterOpp TABLE
	(
		ID INT IDENTITY(1,1),
		numOppID NUMERIC(18,0)
	)

	DECLARE @TEMPOppItems TABLE
	(
		ID INT
		,numoppitemtCode NUMERIC(18,0)
		,numItemCode NUMERIC(18,0)
		,numUnitHour FLOAT
		,monPrice DECIMAL(30,16)
		,monTotAmount MONEY
		,vcItemDesc VARCHAR(1000)
		,numWarehouseItmsID NUMERIC(18,0)
		,ItemType VARCHAR(30)
		,DropShip BIT
		,bitDiscountType BIT
		,fltDiscount DECIMAL(30,16)
		,monTotAmtBefDiscount MONEY
		,vcItemName VARCHAR(300)
		,numUOM NUMERIC(18,0)
		,bitWorkOrder BIT
		,numVendorWareHouse NUMERIC(18,0)
		,numShipmentMethod NUMERIC(18,0)
		,numSOVendorId NUMERIC(18,0)
		,numProjectID numeric(18,0)
		,numProjectStageID numeric(18,0)
		,numToWarehouseItemID NUMERIC(18,0)
		,Attributes varchar(500)
		,AttributeIDs VARCHAR(500)
		,vcSKU VARCHAR(100)
		,bitItemPriceApprovalRequired BIT
		,numPromotionID NUMERIC(18,0)
		,bitPromotionTriggered BIT
		,vcPromotionDetail VARCHAR(2000)
		,numSortOrder NUMERIC(18,0)
	)


	-- FIRST GET UNIQUE OPPID THAT NEEDS TO BE RE CREATED WHICH ARE NOT EXECUTED
	INSERT INTO 
		@TEMPMasterOpp
	SELECT
		DISTINCT numOppId
	FROM
		dbo.MassSalesOrderQueue
	WHERE
		ISNULL(bitExecuted,0) = 0
	
	DECLARE @i AS INT = 1
	DECLARE @iCount AS INT
	SELECT @iCount = COUNT(*) FROM @TEMPMasterOpp

	DECLARE @tempOppID AS INT
	DECLARE @numNewOppID AS NUMERIC(18,0)
	DECLARE @numDomainID AS NUMERIC(18,0)
	DECLARE @numUserCntID AS NUMERIC(18,0)
	DECLARE @tintSource AS NUMERIC(9)
	DECLARE @txtComments AS VARCHAR(1000)
	DECLARE @bitPublicFlag AS BIT
	DECLARE @monPAmount AS MONEY
	DECLARE @numAssignedTo AS NUMERIC(18,0)                                                                                                                                                                              
	DECLARE @strItems AS VARCHAR(MAX)
	DECLARE @dtEstimatedCloseDate DATETIME                                                            
	DECLARE @lngPConclAnalysis AS NUMERIC(18,0)
	DECLARE @numCurrencyID AS NUMERIC(18,0)
	DECLARE @numOrderStatus NUMERIC(18,0)

	DECLARE @bitBillingTerms AS BIT
	DECLARE @intBillingDays as integer
	DECLARE @bitInterestType as bit
	DECLARE @fltInterest as float
	DECLARE @vcCouponCode VARCHAR(100)

	DECLARE @dtReleaseDate DATE

	DECLARE @TEMPFimal TABLE
	(
		numDomainID NUMERIC(18,0)
		,numUserCntID NUMERIC(18,0)
		,numDivisionID NUMERIC(18,0)
		,numOppID NUMERIC(18,0)
	)

	

	WHILE @i <= @iCount
	BEGIN
		SELECT @tempOppID=numOppID FROM @TEMPMasterOpp WHERE ID = @i

		IF EXISTS (SELECT * FROM OpportunityMaster WHERE numOppId=@tempOppID)
		BEGIN

			SELECT 
				@numDomainID=numDomainId
				,@numUserCntID=numCreatedBy
				,@tintSource=tintSource
				,@txtComments=txtComments
				,@bitPublicFlag=bitPublicFlag
				,@dtEstimatedCloseDate=GETUTCDATE()
				,@lngPConclAnalysis=lngPConclAnalysis
				,@numCurrencyID=numCurrencyID
				,@vcCouponCode=vcCouponCode
				,@numAssignedTo=numAssignedTo
				,@numOrderStatus=numStatus
			FROM
				OpportunityMaster
			WHERE
				numOppId=@tempOppID

			-- DELETE ENTRIES OF PREVIOUS LOOP
			DELETE FROM @TEMPOppItems

			INSERT INTO 
				@TEMPOppItems
			SELECT
				ROW_NUMBER() OVER(ORDER BY numoppitemtCode)
				,numoppitemtCode
				,numItemCode
				,numUnitHour
				,monPrice
				,monTotAmount
				,vcItemDesc
				,numWarehouseItmsID
				,vcType
				,bitDropShip
				,bitDiscountType
				,fltDiscount
				,monTotAmtBefDiscount
				,vcItemName
				,numUOMId
				,bitWorkOrder
				,numVendorWareHouse
				,numShipmentMethod
				,numSOVendorId
				,0 AS numProjectID
				,0 AS numProjectStageID
				,numToWarehouseItemID
				,vcAttributes AS Attributes
				,vcAttrValues AS AttributeIDs
				,vcSKU
				,bitItemPriceApprovalRequired
				,numPromotionID
				,bitPromotionTriggered
				,vcPromotionDetail
				,numSortOrder
			FROM 
				OpportunityItems
			WHERE
				numOppId = @tempOppID

			DECLARE @k AS INT = 1
			DECLARE @numTempOppItemID NUMERIC(18,0)
			DECLARE @kCount AS INT
			SELECT @kCount=COUNT(*) FROM @TEMPOppItems
	
			--TODO CREATE ITEMS
			SET @strItems = '<?xml version="1.0" encoding="iso-8859-1" ?><NewDataSet>'

			WHILE @k <= @kCount
			BEGIN
				SELECT 
					@numTempOppItemID = numoppitemtCode
					,@strItems = CONCAT(@strItems
										,'<Item><Op_Flag>1</Op_Flag>'
										,'<numoppitemtCode>',ID,'</numoppitemtCode>'
										,'<numItemCode>',numItemCode,'</numItemCode>'
										,'<numUnitHour>',numUnitHour,'</numUnitHour>'
										,'<monPrice>',monPrice,'</monPrice>'
										,'<monTotAmount>',monTotAmount,'</monTotAmount>'
										,'<vcItemDesc>',vcItemDesc,'</vcItemDesc>'
										,'<numWarehouseItmsID>',ISNULL(numWarehouseItmsID,0),'</numWarehouseItmsID>'
										,'<ItemType>',ItemType,'</ItemType>'
										,'<DropShip>',ISNULL(DropShip,0),'</DropShip>'
										,'<bitDiscountType>',ISNULL(bitDiscountType,0),'</bitDiscountType>'
										,'<fltDiscount>',ISNULL(fltDiscount,0),'</fltDiscount>'
										,'<monTotAmtBefDiscount>',monTotAmtBefDiscount,'</monTotAmtBefDiscount>'
										,'<vcItemName>',vcItemName,'</vcItemName>'
										,'<numUOM>',ISNULL(numUOM,0),'</numUOM>'
										,'<bitWorkOrder>',ISNULL(bitWorkOrder,0),'</bitWorkOrder>'
										,'<numVendorWareHouse>',ISNULL(numVendorWareHouse,0),'</numVendorWareHouse>'
										,'<numShipmentMethod>',ISNULL(numShipmentMethod,0),'</numShipmentMethod>'
										,'<numSOVendorId>',ISNULL(numSOVendorId,0),'</numSOVendorId>'
										,'<numProjectID>',ISNULL(numProjectID,0),'</numProjectID>'
										,'<numProjectStageID>',ISNULL(numProjectStageID,0),'</numProjectStageID>'
										,'<numToWarehouseItemID>',ISNULL(numToWarehouseItemID,0),'</numToWarehouseItemID>'
										,'<Attributes>',Attributes,'</Attributes>'
										,'<AttributeIDs>',AttributeIDs,'</AttributeIDs>'
										,'<vcSKU>',vcSKU,'</vcSKU>'
										,'<bitItemPriceApprovalRequired>',ISNULL(bitItemPriceApprovalRequired,0),'</bitItemPriceApprovalRequired>'
										,'<numPromotionID>',ISNULL(numPromotionID,0),'</numPromotionID>'
										,'<bitPromotionTriggered>',ISNULL(bitPromotionTriggered,0),'</bitPromotionTriggered>'
										,'<vcPromotionDetail>',vcPromotionDetail,'</vcPromotionDetail>'
										,'<numSortOrder>',ISNULL(numSortOrder,0),'</numSortOrder>')
									
				FROM 
					@TEMPOppItems 
				WHERE 
					ID=@k

			
				IF (SELECT COUNT(*) FROM OpportunityKitChildItems WHERE numOppID = @tempOppID AND numOppItemID=@numTempOppItemID) > 0
				BEGIN
					SET @strItems = CONCAT(@strItems,'<bitHasKitAsChild>true</bitHasKitAsChild>')
					SET @strItems = CONCAT(@strItems,'<KitChildItems>',
											STUFF((SELECT 
											',' + CONCAT(OKI.numChildItemID,'#',OKI.numWareHouseItemId,'-',OKCI.numItemID,'#',OKCI.numWareHouseItemId)
											FROM
												OpportunityKitChildItems OKCI
											INNER JOIN
												OpportunityKitItems OKI
											ON
												OKCI.numOppChildItemID = OKI.numOppChildItemID
											WHERE
												OKCI.numOppID=@tempOppID
												AND OKCI.numOppItemID=@numTempOppItemID
																		 FOR XML PATH('')), 
																		1, 1, ''),'</KitChildItems>')
				END
				ELSE
				BEGIN
					SET @strItems = CONCAT(@strItems,'<bitHasKitAsChild>false</bitHasKitAsChild>')
					SET @strItems = CONCAT(@strItems,'<KitChildItems />')
				END
			

				SET @strItems = CONCAT(@strItems,'</Item>')

				SET @k = @k + 1
			END

			SET @strItems = CONCAT(@strItems,'</NewDataSet>')
		
			-- DELETE ENTRIES OF PREVIOUS LOOP
			DELETE FROM @TEMP

			-- GET ALL ORGANIZATION FOR WHICH ORDER NEEDS TO BE CREATED
			INSERT INTO @TEMP
			(
				ID,
				numMSOQID,
				numDivisionID,
				numContactID
			)
			SELECT 
				ROW_NUMBER() OVER(ORDER BY numMSOQID),
				numMSOQID,
				numDivisionId,
				numContactId
			FROM
				MassSalesOrderQueue
			WHERE
				numOppId = @tempOppID
				AND ISNULL(bitExecuted,0) = 0

			DECLARE @j AS INT = 1
			DECLARE @jCount AS INT
			DECLARE @numTempMSOQID NUMERIC(18,0)
			DECLARE @numTempDivisionID NUMERIC(18,0)
			DECLARE @numTempContactID NUMERIC(18,0)
			SELECT @jCount = COUNT(*) FROM @TEMP

			WHILE @j <= @jCount
			BEGIN
			
				SELECT 
					@numTempMSOQID=numMSOQID
					,@numTempDivisionID=numDivisionID
					,@numTempContactID=numContactID
				FROM 
					@TEMP 
				WHERE 
					ID = @j

				SELECT 
					@bitBillingTerms=(CASE WHEN ISNULL(tintBillingTerms,0) = 1 THEN 1 ELSE 0 END) 
					,@intBillingDays = ISNULL(numBillingDays,0)
					,@bitInterestType= (CASE WHEN ISNULL(tintInterestType,0) = 1 THEN 1 ELSE 0 END)
					,@fltInterest=ISNULL(fltInterest ,0)
					,@dtReleaseDate=CAST(GETUTCDATE() AS DATE)
					,@numAssignedTo=ISNULL(numAssignedTo,0)
				FROM 
					DivisionMaster
				WHERE 
					numDomainID=@numDomainID AND numDivisionID=@numTempDivisionID

				BEGIN TRY
					SET @numNewOppID = 0

					EXEC USP_OppManage 
					@numNewOppID OUTPUT
					,@numTempContactID
					,@numTempDivisionID
					,@tintSource
					,''
					,@txtComments
					,@bitPublicFlag
					,@numUserCntID
					,0
					,@numAssignedTo
					,@numDomainID
					,@strItems
					,''
					,@dtEstimatedCloseDate
					,0
					,@lngPConclAnalysis
					,1
					,0
					,0
					,0
					,@numCurrencyID
					,1
					,@numOrderStatus
					,NULL
					,0
					,0
					,0
					,0
					,1
					,0
					,0
					,@bitBillingTerms
					,@intBillingDays
					,@bitInterestType
					,@fltInterest
					,0
					,0
					,NULL
					,@vcCouponCode
					,NULL
					,NULL
					,NULL
					,NULL
					,0
					,0
					,0
					,0
					,0
					,@dtReleaseDate
					,0
					,0

					-- UPDATE STATUS TO EXECUTED
					UPDATE 
						MassSalesOrderQueue
					SET
						bitExecuted = 1
					WHERE
						numMSOQID = @numTempMSOQID


					-- MAKE ENTRY INTO MASS SALES ORDER LOG
					INSERT INTO MassSalesOrderLog
					(
						numMSOQID
						,bitSuccess
						,numCreatedOrderID
					)
					VALUES
					(
						@numTempMSOQID
						,1
						,@numNewOppID
					)


					INSERT INTO CFW_Fld_Values_Opp
					(
						Fld_ID
						,Fld_Value
						,RecId
						,bintModifiedDate
						,numModifiedBy
					)
					SELECT
						Fld_ID
						,Fld_Value
						,@numNewOppID
						,GETUTCDATE()
						,numModifiedBy
					FROM
						CFW_Fld_Values_Opp
					WHERE
						RecId=@tempOppID

					INSERT INTO @TEMPFimal (numDomainID,numUserCntID,numDivisionID,numOppID) VALUES (@numDomainID,@numUserCntID,@numTempDivisionID,@numNewOppID)
				END TRY
				BEGIN CATCH
					SELECT ERROR_MESSAGE(),ERROR_LINE()
						
					-- UPDATE STATUS TO EXECUTED
					UPDATE 
						MassSalesOrderQueue
					SET
						bitExecuted = 1
					WHERE
						numMSOQID = @numTempMSOQID


					-- MAKE ENTRY INTO MASS SALES ORDER LOG
					INSERT INTO MassSalesOrderLog
					(
						numMSOQID
						,bitSuccess
						,vcError
					)
					VALUES
					(
						@numTempMSOQID
						,0
						,ERROR_MESSAGE()
					)
				END CATCH

				SET @j = @j + 1
			END
		END

		SET @i = @i + 1
	END

	SELECT t1.*,SFC.numRule1BizDoc,ISNULL(bitRule1IsActive,0) bitRule1IsActive,ISNULL(tintRule1Type,0) tintRule1Type FROM @TEMPFimal t1 INNER JOIN SalesFulfillmentConfiguration SFC ON t1.numDomainID = SFC.numDomainID WHERE ISNULL(SFC.bitActive,0)=1
END
/****** Object:  StoredProcedure [dbo].[USP_GetPageElementAttributes]    Script Date: 08/08/2009 16:15:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--EXEC [USP_GetPageElementAttributes] @numSiteID = 3,@numElementID =4
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityBizDocs_CT')
DROP PROCEDURE USP_OpportunityBizDocs_CT
GO
-- =============================================
-- Author:		<Author,,Sachin Sadhu>
-- Create date: <Create Date,, 18thMay2014>
-- Description:	<Description,,Get only Order type :Sales /or Purchase>
-- =============================================
Create PROCEDURE [dbo].[USP_OpportunityBizDocs_CT]
	-- Add the parameters for the stored procedure here
    @numDomainID numeric(18, 0)=0,
    @numUserCntID numeric(18, 0)=0,
    @numRecordID numeric(18, 0)=0 
AS
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
	SET NOCOUNT ON;
 -- Insert statements for procedure here
	declare @tblFields as table 
	(
		FieldName varchar(200),
		FieldValue varchar(200)
	)

	Declare @Type char(1)
	Declare @ChangeVersion bigint
	Declare @CreationVersion bigint
	Declare @last_synchronization_version bigInt=null

	SELECT 			
		@ChangeVersion=Ct.Sys_Change_version,@CreationVersion=Ct.Sys_Change_Creation_version
	FROM 
		CHANGETABLE(CHANGES dbo.OpportunityBizDocs, 0) AS CT
	WHERE 
		numOppBizDocsId=@numRecordID

	IF (@ChangeVersion=@CreationVersion)--Insert
	BEGIN
		SELECT 
			@Type = CT.SYS_CHANGE_OPERATION
		FROM 
			CHANGETABLE(CHANGES dbo.OpportunityBizDocs, @last_synchronization_version) AS CT
		WHERE  
			CT.numOppBizDocsId=@numRecordID
	END
	ELSE--UPDATE
	BEGIN
		SET @last_synchronization_version=@ChangeVersion-1     
	
		SELECT 
			@Type = CT.SYS_CHANGE_OPERATION
		FROM 
			CHANGETABLE(CHANGES dbo.OpportunityBizDocs, @last_synchronization_version) AS CT
		WHERE  
			CT.numOppBizDocsId=@numRecordID

		--Fields Update & for Type :Insert or Update:
			Declare @UFFields as table 
			(
			    vcBizDocID varchar(70),
				monAmountPaid varchar(70),
				numBizDocStatus varchar(70),
				numBizDocId varchar(70),
				monDealAmount varchar(70),
				numModifiedBy varchar(70),
				numCreatedBy varchar(70),
				bitRecurred varchar(70)
			)

		INSERT INTO @UFFields
		(
			vcBizDocID
			,monAmountPaid
			,numBizDocStatus
			,numBizDocId
			,monDealAmount
			,numModifiedBy
			,numCreatedBy
			,bitRecurred
		)
		SELECT
			CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityBizDocs'), 'vcBizDocID', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS vcBizDocID ,
			CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityBizDocs'), 'monAmountPaid', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS monAmountPaid, 
			CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityBizDocs'), 'numBizDocStatus', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numBizDocStatus, 
			CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityBizDocs'), 'numBizDocId', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numBizDocId, 
			CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityBizDocs'), 'monDealAmount', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS monDealAmount	,
			CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityBizDocs'), 'numModifiedBy', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS  numModifiedBy ,
			CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityBizDocs'), 'numCreatedBy', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numCreatedBy ,
			CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityBizDocs'), 'bitRecurred', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS bitRecurred 
		FROM 
			CHANGETABLE(CHANGES dbo.OpportunityBizDocs, @last_synchronization_version) AS CT
		WHERE 
			ct.numOppBizDocsId=@numRecordID


		INSERT INTO @tblFields
		(
			FieldName
			,FieldValue
		)
		SELECT
			FieldName
			,FieldValue
		FROM
		(
			SELECT  vcBizDocID,monAmountPaid,numBizDocStatus,numBizDocId,monDealAmount,bitRecurred
			FROM @UFFields	
		) AS UP
		UNPIVOT
		(
			FieldValue FOR FieldName IN (vcBizDocID,monAmountPaid,numBizDocStatus,numBizDocId,monDealAmount,bitRecurred)
		) AS upv
		WHERE 
			FieldValue<>0
	END

 select @Type
DECLARE @tintWFTriggerOn AS TINYINT
SET @tintWFTriggerOn=CASE @Type WHEN 'I' THEN 1 WHEN 'U' THEN 2 WHEN 'D' THEN 5 END

--For Fields Update Exection Point
DECLARE @Columns_Updated VARCHAR(1000)
SELECT @Columns_Updated = COALESCE(@Columns_Updated + ',', '') + FieldName FROM @tblFields 
SET @Columns_Updated=ISNULL(@Columns_Updated,'')

SELECT @Columns_Updated

--Adding data As per Opertaion
	EXEC dbo.USP_ManageWorkFlowQueue
	@numWFQueueID = 0, --  numeric(18, 0)
	@numDomainID = @numDomainID, --  numeric(18, 0)
	@numUserCntID = @numUserCntID, --  numeric(18, 0)
	@numRecordID = @numRecordID, --  numeric(18, 0)
	@numFormID = 71, --  numeric(18, 0)
	@tintProcessStatus = 1, --  tinyint
	@tintWFTriggerOn = @tintWFTriggerOn, --  tinyint
	@tintMode = 1, --  tinyint
	@vcColumnsUpdated = @Columns_Updated

	
/***************************************************Commented SP for Checking RowVersion with Change Tracking:Date:23Apr||Sachin********************************************************/



--DECLARE @synchronization_version BIGINT 



--SET @synchronization_version = CHANGE_TRACKING_CURRENT_VERSION();











--SELECT 



--    CT.numOppID, CT.SYS_CHANGE_OPERATION, 



--    CT.SYS_CHANGE_COLUMNS, CT.SYS_CHANGE_CONTEXT, 



--    CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'numOppID', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numOppID, 



--    CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'vcPOppName', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS vcPOppName 



--FROM 



--    CHANGETABLE(CHANGES dbo.OpportunityMaster, @synchronization_version) AS CT











--SET @synchronization_version = CHANGE_TRACKING_CURRENT_VERSION();



/***************************************************End of Comment||Sachin********************************************************/



END


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityMaster_GetDataForEDI856')
DROP PROCEDURE dbo.USP_OpportunityMaster_GetDataForEDI856
GO
CREATE PROCEDURE [dbo].[USP_OpportunityMaster_GetDataForEDI856]
	@numDomainID NUMERIC(18,0)
	,@numOppID NUMERIC(18,0)
AS 
BEGIN
	DECLARE @TableShippingReport TABLE
	(
		numShippingReportId NUMERIC(18,0)
	)

	DECLARE @TableShippingBox TABLE
	(
		numBoxID NUMERIC(18,0)
	)

	INSERT INTO @TableShippingReport
	(
		numShippingReportId
	)
	SELECT
		numShippingReportId
	FROM
		ShippingReport
	WHERE
		ShippingReport.numDomainID=@numDomainID
		AND ShippingReport.numOppID=@numOppID
		AND ISNULL(ShippingReport.bitSendToTP2,0) = 0
		AND 1 = (CASE WHEN (SELECT COUNT(*) FROM ShippingBox WHERE numShippingReportId=ShippingReport.numShippingReportId AND vcShippingLabelImage IS NOT NULL AND vcTrackingNumber IS NOT NULL) > 0 THEN 1 ELSE 0 END)

	INSERT INTO @TableShippingBox
	(
		numBoxID
	)
	SELECT 
		numBoxID
	FROM 
		ShippingBox 
	WHERE 
		numShippingReportId IN (SELECT numShippingReportId FROM @TableShippingReport)

	SELECT
		numShippingReportId
		,CONVERT(CHAR(8), ShippingReport.dtCreateDate, 112) vcShipDate
		,ISNULL(ShippingReport.vcToName,'') vcToName
		,ISNULL(ShippingReport.vcToAddressLine1,'') vcToAddressLine1
		,ISNULL(ShippingReport.vcToAddressLine2,'') vcToAddressLine2
		,ISNULL(ShippingReport.vcToCity,'') vcToCity
		,ISNULL(dbo.fn_GetState(ShippingReport.vcToState),'') vcToState
		,ISNULL(ShippingReport.vcToZip,'') vcToZip
		,ISNULL(dbo.fn_GetListName(ShippingReport.vcToCountry,0),'') vcToCountry
		,ISNULL(ShippingReport.vcFromName,'') vcFromName
		,ISNULL(ShippingReport.vcFromAddressLine1,'') vcFromAddressLine1
		,ISNULL(ShippingReport.vcFromAddressLine2,'') vcFromAddressLine2
		,ISNULL(ShippingReport.vcFromCity,'') vcFromCity
		,ISNULL(dbo.fn_GetState(ShippingReport.vcFromState),'') vcFromState
		,ISNULL(ShippingReport.vcFromZip,'') vcFromZip
		,ISNULL(dbo.fn_GetListName(ShippingReport.vcFromCountry,0),'') vcFromCountry
		,(CASE numShippingCompany WHEN 88 THEN 'UPS' WHEN 90 THEN 'USPS' WHEN 91 THEN 'FedEx' ELSE '' END) vcShipVia
		,CAST(ROUND(((SELECT SUM(ISNULL(fltTotalWeight,0) * ISNULL(intBoxQty,0)) FROM dbo.ShippingReportItems WHERE numShippingReportId = ShippingReport.numShippingReportId)),2) AS DECIMAL(9, 2)) AS fltTotalRegularWeight
		,CAST(ROUND((SELECT SUM(fltDimensionalWeight) FROM dbo.ShippingBox WHERE numShippingReportId = ShippingReport.numShippingReportId), 2) AS DECIMAL(9, 2)) AS fltTotalDimensionalWeight
		,ISNULL(vcCustomerPO#,'') vcCustomerPO#
	FROM
		ShippingReport
	INNER JOIN
		OpportunityMaster
	ON
		ShippingReport.numOppID = OpportunityMaster.numOppId
	WHERE
		numShippingReportId IN (SELECT numShippingReportId FROM @TableShippingReport)


	SELECT 
		numBoxID
		,numShippingReportId
		,fltHeight
		,fltLength
		,fltWidth
		,fltTotalWeight
		,BoxItems.Qty
	FROM 
		ShippingBox 
	OUTER APPLY
	(
		SELECT 
			SUM(ISNULL(ShippingReportItems.intBoxQty,0) * dbo.fn_UOMConversion(Item.numBaseUnit,OpportunityItems.numItemCode,@numDomainID,OpportunityItems.numUOMId)) AS Qty
		FROM 
			ShippingReportItems
		INNER JOIN
			OpportunityBizDocItems 
		ON
			ShippingReportItems.numOppBizDocItemID = OpportunityBizDocItems.numOppBizDocItemID
		INNER JOIN
			OpportunityItems
		ON
			OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtCode
		INNER JOIN
			Item
		ON
			OpportunityItems.numItemCode = Item.numItemCode
		WHERE 
			numBoxID=ShippingBox.numBoxID
	) BoxItems
	WHERE 
		numBoxID IN (SELECT numBoxID FROM @TableShippingBox)
		
	SELECT 
		numBoxID
		,CASE 
			WHEN ISNULL(Item.numItemGroup,0) > 0 AND ISNULL(Item.bitMatrix,0) = 0
			THEN 
				(CASE WHEN LEN(ISNULL(WareHouseItems.vcWHSKU,'')) > 0 THEN WareHouseItems.vcWHSKU ELSE ISNULL(Item.vcSKU,'') END)
			ELSE
				(CASE WHEN LEN(ISNULL(OpportunityItems.vcSKU,'')) > 0 THEN OpportunityItems.vcSKU ELSE ISNULL(Item.vcSKU,'') END)
		END vcSKU
		,CASE 
			WHEN ISNULL(Item.numItemGroup,0) > 0 AND ISNULL(Item.bitMatrix,0) = 0
			THEN 
				(CASE WHEN LEN(ISNULL(WareHouseItems.vcBarCode,'')) > 0 THEN WareHouseItems.vcBarCode ELSE ISNULL(Item.numBarCodeId,'') END)
			ELSE
				ISNULL(Item.numBarCodeId,'')
		END vcUPC
		,CAST((dbo.fn_UOMConversion(ISNULL(Item.numBaseUnit,0),OpportunityItems.numItemCode,@numDomainID, ISNULL(OpportunityItems.numUOMId, 0))* OpportunityItems.numUnitHour) AS NUMERIC(18, 2)) AS numOrderedUnits
		,ISNULL(UOM.vcUnitName,'Units') vcUOM
		,(CASE 
			WHEN charItemType='P' THEN 'Inventory Item'
			WHEN charItemType='S' THEN 'Service' 
			WHEN charItemType='A' THEN 'Accessory' 
			WHEN charItemType='N' THEN 'Non-Inventory Item' 
		END) AS vcItemType
		,CAST((dbo.fn_UOMConversion(ISNULL(Item.numBaseUnit,0),OpportunityItems.numItemCode,@numDomainID, ISNULL(OpportunityItems.numUOMId, 0))* ISNULL(ShippingReportItems.intBoxQty,0)) AS NUMERIC(18, 2)) AS numShippedUnits
	FROM 
		ShippingReportItems
	INNER JOIN
		OpportunityBizDocItems 
	ON
		ShippingReportItems.numOppBizDocItemID = OpportunityBizDocItems.numOppBizDocItemID
	INNER JOIN
		OpportunityItems
	ON
		OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtCode
	INNER JOIN
		Item
	ON
		OpportunityItems.numItemCode = Item.numItemCode
	LEFT JOIN
		WareHouseItems
	ON
		OpportunityItems.numWarehouseItmsID = WareHouseItems.numWareHouseItemID
	LEFT JOIN 
		UOM 
	ON
		OpportunityItems.numUOMId = UOM.numUOMId
	WHERE 
		numBoxID IN (SELECT numBoxID FROM @TableShippingBox)

END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityMaster_GetDataForEDI940')
DROP PROCEDURE dbo.USP_OpportunityMaster_GetDataForEDI940
GO
CREATE PROCEDURE [dbo].[USP_OpportunityMaster_GetDataForEDI940]
	@numDomainID NUMERIC(18,0)
	,@numOppID NUMERIC(18,0)
AS 
BEGIN
	SELECT
		OpportunityMaster.numOppID
		,OpportunityMaster.vcPOppName
		,CompanyInfo.vcCompanyName
		,Domain.vcDomainName
		,CONCAT(OpportunityMaster.numDomainID,'-',OpportunityMaster.numDivisionID) AS vcDomainDivisionID
		,ISNULL(OpportunityMaster.txtComments,'') vcComments
		,ISNULL(AdditionalContactsInformation.vcFirstName,'') vcFirstName
		,ISNULL(AdditionalContactsInformation.vcLastName,'') vcLastName
		,ISNULL(AdditionalContactsInformation.numCell,'') vcPhone
		,ISNULL((SELECT TOP 1 vcStreet FROM fn_getOPPAddressDetails(OpportunityMaster.numOppId,OpportunityMaster.numDomainID,1)),'') AS [vcBillStreet]
		,ISNULL((SELECT TOP 1 vcCity FROM fn_getOPPAddressDetails(OpportunityMaster.numOppId,OpportunityMaster.numDomainID,1)),'') AS [vcBillCity]
		,ISNULL((SELECT TOP 1 vcState FROM fn_getOPPAddressDetails(OpportunityMaster.numOppId,OpportunityMaster.numDomainID,1)),'') AS [vcBillState]
		,ISNULL((SELECT TOP 1 vcCountry FROM fn_getOPPAddressDetails(OpportunityMaster.numOppId,OpportunityMaster.numDomainID,1)),'') AS [vcBillCountry]
		,ISNULL((SELECT TOP 1 vcPostalCode FROM fn_getOPPAddressDetails(OpportunityMaster.numOppId,OpportunityMaster.numDomainID,1)),'') AS [vcBillPostalCode]
		,ISNULL((SELECT TOP 1 vcStreet FROM fn_getOPPAddressDetails(OpportunityMaster.numOppId,OpportunityMaster.numDomainID,2)),'') AS [vcShipStreet]
		,ISNULL((SELECT TOP 1 vcCity FROM fn_getOPPAddressDetails(OpportunityMaster.numOppId,OpportunityMaster.numDomainID,2)),'') AS [vcShipCity]
		,ISNULL((SELECT TOP 1 vcState FROM fn_getOPPAddressDetails(OpportunityMaster.numOppId,OpportunityMaster.numDomainID,2)),'') AS [vcShipState]
		,ISNULL((SELECT TOP 1 vcCountry FROM fn_getOPPAddressDetails(OpportunityMaster.numOppId,OpportunityMaster.numDomainID,2)),'') AS [vcShipCountry]
		,ISNULL((SELECT TOP 1 vcPostalCode FROM fn_getOPPAddressDetails(OpportunityMaster.numOppId,OpportunityMaster.numDomainID,2)),'') AS [vcShipPostalCode]
	FROM
		OpportunityMaster
	INNER JOIN
		Domain
	ON
		OpportunityMaster.numDomainID = Domain.numDomainId
	INNER JOIN
		DivisionMaster
	ON
		OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
	INNER JOIN
		CompanyInfo
	ON
		DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
	LEFT JOIN
		AdditionalContactsInformation
	ON
		OpportunityMaster.numContactId = AdditionalContactsInformation.numContactId
	WHERE
		OpportunityMaster.numDomainId=@numDomainID
		AND OpportunityMaster.numOppId = @numOppID


	SELECT
		Item.vcItemName
		,CASE 
			WHEN ISNULL(Item.numItemGroup,0) > 0 AND ISNULL(Item.bitMatrix,0) = 0
			THEN 
				(CASE WHEN LEN(ISNULL(WareHouseItems.vcWHSKU,'')) > 0 THEN WareHouseItems.vcWHSKU ELSE ISNULL(Item.vcSKU,'') END)
			ELSE
				(CASE WHEN LEN(ISNULL(OpportunityItems.vcSKU,'')) > 0 THEN OpportunityItems.vcSKU ELSE ISNULL(Item.vcSKU,'') END)
		END vcSKU
		,CAST((dbo.fn_UOMConversion(ISNULL(Item.numBaseUnit, 0),OpportunityItems.numItemCode,@numDomainID, ISNULL(OpportunityItems.numUOMId, 0))* OpportunityItems.numUnitHour) AS NUMERIC(18, 2)) AS numUnits
		,ISNULL(UOM.vcUnitName,'Units') vcUOM
		,(CASE 
			WHEN charItemType='P' THEN 'Inventory Item'
			WHEN charItemType='S' THEN 'Service' 
			WHEN charItemType='A' THEN 'Accessory' 
			WHEN charItemType='N' THEN 'Non-Inventory Item' 
		END) AS vcItemType
		,ISNULL(OpportunityItems.vcNotes,'') vcNotes
	FROM
		OpportunityItems
	INNER JOIN
		Item
	ON
		OpportunityItems.numItemCode = Item.numItemCode
	LEFT JOIN
		WareHouseItems
	ON
		OpportunityItems.numWarehouseItmsID = WareHouseItems.numWareHouseItemID
	LEFT JOIN 
		UOM 
	ON
		OpportunityItems.numUOMId = UOM.numUOMId
	WHERE
		numOppId = @numOppID
		AND ISNULL(OpportunityItems.bitDropShip,0) = 0
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityMaster_GetDomainID')
DROP PROCEDURE dbo.USP_OpportunityMaster_GetDomainID
GO
CREATE PROCEDURE [dbo].[USP_OpportunityMaster_GetDomainID]
	@numOppID NUMERIC(18,0)
AS 
BEGIN
	SELECT
		numDomainID
	FROM
		OpportunityMaster
	WHERE
		numOppId=@numOppID
END
GO

GO
/****** Object:  StoredProcedure [dbo].[USP_PayrollLiabilityList]    Script Date: 02/28/2009 13:55:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Siva
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_payrollliabilitylist')
DROP PROCEDURE usp_payrollliabilitylist
GO
CREATE PROCEDURE [dbo].[USP_PayrollLiabilityList]
(
    @numDomainId          AS NUMERIC(9)  = 0,
    @numDepartmentId      AS NUMERIC(9)  = 0,
    @dtStartDate          AS DATETIME,
    @dtEndDate            AS DATETIME,
    @ClientTimeZoneOffset INT,
	@numOrgUserCntID AS NUMERIC(18,0),
	@tintUserRightType INT = 3,
	@tintUserRightForUpdate INT = 3
)
AS
BEGIN
	SET @dtStartDate = DATEADD (MS , 000 , @dtStartDate)
	SET @dtEndDate = DATEADD (MS , 997 , @dtEndDate)  

	DECLARE @TempCommissionPaidCreditMemoOrRefund TABLE
	(
		numUserCntID NUMERIC(18,0),
		numReturnHeaderID NUMERIC(18,0),
		tintReturnType TINYINT,
		numReturnItemID NUMERIC(18,0),
		monCommission MONEY,
		numComRuleID NUMERIC(18,0),
		tintComType TINYINT,
		tintComBasedOn TINYINT,
		decCommission FLOAT,
		tintAssignTo TINYINT
	)    

	INSERT INTO 
		@TempCommissionPaidCreditMemoOrRefund 
	SELECT 
		numUserCntID,
		numReturnHeaderID,
		tintReturnType,
		numReturnItemID,
		monCommission,
		numComRuleID,
		tintComType,
		tintComBasedOn,
		decCommission,
		tintAssignTo
	FROM 
		dbo.GetCommissionPaidCreditMemoOrRefund(@numDomainId,@ClientTimeZoneOffset,@dtStartDate,@dtEndDate)

	DECLARE @TempCommissionUnPaidCreditMemoOrRefund TABLE
	(
		numUserCntID NUMERIC(18,0),
		numReturnHeaderID NUMERIC(18,0),
		tintReturnType TINYINT,
		numReturnItemID NUMERIC(18,0),
		monCommission MONEY,
		numComRuleID NUMERIC(18,0),
		tintComType TINYINT,
		tintComBasedOn TINYINT,
		decCommission FLOAT,
		tintAssignTo TINYINT
	)    

	INSERT INTO 
		@TempCommissionUnPaidCreditMemoOrRefund 
	SELECT 
		numUserCntID,
		numReturnHeaderID,
		tintReturnType,
		numReturnItemID,
		monCommission,
		numComRuleID,
		tintComType,
		tintComBasedOn,
		decCommission,
		tintAssignTo
	FROM 
		dbo.GetCommissionUnPaidCreditMemoOrRefund(@numDomainId,@ClientTimeZoneOffset,@dtStartDate,@dtEndDate)

	
	-- TEMPORARY TABLE TO HOLD DATA
	CREATE TABLE #tempHrs 
	(
		ID INT IDENTITY(1,1),
		numUserId NUMERIC,
		vcteritorries VARCHAR(1000),
		vcTaxID VARCHAR(100),
		vcUserName VARCHAR(100),
		vcdepartment VARCHAR(100),
		bitLinkVisible BIT,
		bitpayroll BIT,
		numUserCntID NUMERIC,
		vcEmployeeId VARCHAR(50),
		monHourlyRate MONEY,
		decRegularHrs decimal(10,2),
		decPaidLeaveHrs decimal(10,2),
		monExpense MONEY,
		monReimburse MONEY,
		monCommPaidInvoice MONEY,
		monCommPaidInvoiceDeposited MONEY,
		monCommUNPaidInvoice MONEY,
		monCommPaidCreditMemoOrRefund MONEY,
		monCommUnPaidCreditMemoOrRefund MONEY,
		monTotalAmountDue MONEY,
		monAmountPaid MONEY, 
		bitCommissionContact BIT,
		numDivisionID NUMERIC(18,0),
		bitPartner BIT,
		tintCRMType TINYINT
	)

	-- GET EMPLOYEE OF DOMAIN
	INSERT INTO #tempHrs
	(
		numUserId,
		vcteritorries,
		vcTaxID,
		vcUserName,
		vcdepartment,
		bitLinkVisible,
		bitpayroll,
		numUserCntID,
		vcEmployeeId,
		monHourlyRate,
		bitCommissionContact
	)
	SELECT 
		UM.numUserId,
		CONCAT(',',STUFF((SELECT CONCAT(',',numTerritoryID) FROM UserTerritory WHERE numUserCntID=UM.numUserDetailId AND numDomainID=@numDomainId FOR XML PATH('')),1,1,''),','),
		ISNULL(adc.vcTaxID,''),
		Isnull(ADC.vcfirstname + ' ' + adc.vclastname,'-') vcUserName,
		dbo.fn_GetListItemName(adc.vcdepartment),
		(CASE @tintUserRightForUpdate
			WHEN 0 THEN 0
			WHEN 1 THEN CASE WHEN UM.numUserDetailId = @numOrgUserCntID THEN 1 ELSE 0 END
			WHEN 2 THEN CASE WHEN UM.numUserDetailId IN (SELECT numUserCntID FROM UserTerritory WHERE numUserCntID <> @numOrgUserCntID AND numTerritoryID IN (SELECT numTerritoryID FROM UserTerritory WHERE numUserCntID = @numOrgUserCntID)) THEN 1 ELSE 0 END
			ELSE 1
		END),
		Isnull(um.bitpayroll,0) AS bitpayroll,
		adc.numcontactid AS numUserCntID,
		Isnull(um.vcEmployeeId,'') AS vcEmployeeId,
		ISNULL(UM.monHourlyRate,0),
		0
	FROM  
		dbo.UserMaster UM 
	JOIN 
		dbo.AdditionalContactsInformation adc 
	ON 
		adc.numDomainID=@numDomainId 
		AND adc.numcontactid = um.numuserdetailid
	WHERE 
		UM.numDomainId=@numDomainId AND (adc.vcdepartment=@numDepartmentId OR @numDepartmentId=0)
		AND 1 = (CASE @tintUserRightType
					WHEN 1 THEN CASE WHEN UM.numUserDetailId = @numOrgUserCntID THEN 1 ELSE 0 END
					WHEN 2 THEN CASE WHEN UM.numUserDetailId IN (SELECT numUserCntID FROM UserTerritory WHERE numUserCntID <> @numOrgUserCntID AND numTerritoryID IN (SELECT numTerritoryID FROM UserTerritory WHERE numUserCntID = @numOrgUserCntID)) THEN 1 ELSE 0 END
					ELSE 1
				END)

	-- GET COMMISSION CONTACTS OF DOMAIN
	INSERT INTO #tempHrs 
	(
		numUserId,
		vcTaxID,
		vcUserName,
		numUserCntID,
		vcEmployeeId,
		vcdepartment,
		bitLinkVisible,
		bitpayroll,
		monHourlyRate,
		bitCommissionContact
	)
	SELECT  
		0,
		ISNULL(A.vcTaxID,''),
		ISNULL(A.vcFirstName +' '+ A.vcLastName + '(' + C.vcCompanyName +')','-'),
		A.numContactId,
		'',
		'-',
		(CASE WHEN @tintUserRightForUpdate = 0 OR @tintUserRightForUpdate = 1 OR @tintUserRightForUpdate = 2 THEN 0 ELSE 1 END),
		0,
		0,
		1
	FROM 
		CommissionContacts CC 
	JOIN DivisionMaster D ON CC.numDivisionID=D.numDivisionID
	JOIN AdditionalContactsInformation A ON D.numDivisionID=A.numDivisionID
	JOIN CompanyInfo C ON D.numCompanyID = C.numCompanyId
	WHERE 
		CC.numDomainID=@numDomainID
		AND 1 = (CASE WHEN @tintUserRightType = 1 OR @tintUserRightType = 2 THEN 0 ELSE 1 END)


	-- GET PARTNERS OF DOMAIN
	INSERT INTO #tempHrs 
	(
		numUserId,
		vcTaxID,
		vcUserName,
		numUserCntID,
		vcEmployeeId,
		vcdepartment,
		bitLinkVisible,
		bitpayroll,
		monHourlyRate,
		numDivisionID,
		bitPartner,
		bitCommissionContact,
		tintCRMType
	)
	SELECT  
		0,
		ISNULL(A.vcTaxID,''),
		ISNULL(C.vcCompanyName,'-'),
		0,
		'',
		'-',
		(CASE WHEN @tintUserRightForUpdate = 0 OR @tintUserRightForUpdate = 1 OR @tintUserRightForUpdate = 2 THEN 0 ELSE 1 END),
		0,
		0,
		D.numDivisionID,
		1,
		1,
		tintCRMType
	FROM  
		DivisionMaster AS D 
	LEFT JOIN 
		CompanyInfo AS C
	ON 
		D.numCompanyID=C.numCompanyID
	LEFT JOIN
		AdditionalContactsInformation A
	ON
		A.numDivisionID = D.numDivisionID
		AND ISNULL(A.bitPrimaryContact,1) = 1
	WHERE 
		D.numDomainID=@numDomainID 
		AND D.vcPartnerCode <> ''
		AND 1 = (CASE WHEN @tintUserRightType = 1 OR @tintUserRightType = 2 THEN 0 ELSE 1 END)
	ORDER BY
		vcCompanyName

	-- FETCH ALLREADY PAID COMMISSION DATA AND STORE IN TEMPORARY TABLE
	SELECT * INTO #tempPayrollTracking FROM PayrollTracking WHERE numDomainId=@numDomainId

	Declare @decTotalHrsWorked as decimal(10,2)
	DECLARE @decTotalPaidLeaveHrs AS DECIMAL(10,2)
	DECLARE @monExpense MONEY,@monReimburse MONEY,@monCommPaidInvoice MONEY,@monCommPaidInvoiceDepositedToBank MONEY,@monCommUNPaidInvoice MONEY,@monCommPaidCreditMemoOrRefund MONEY,@monCommUnPaidCreditMemoOrRefund MONEY

	DECLARE @i INT = 1
	DECLARE @COUNT INT = 0
	DECLARE @numUserCntID NUMERIC(18,0)
	DECLARE @numDivisionID NUMERIC(18,0)
	DECLARE @bitCommissionContact BIT
    
	SELECT @COUNT=COUNT(*) FROM #tempHrs

	-- LOOP OVER EACH EMPLOYEE
	WHILE @i <= @COUNT
	BEGIN
		SELECT @decTotalHrsWorked=0,@decTotalPaidLeaveHrs=0,@monExpense=0,@monReimburse=0,@monCommPaidInvoice=0,@monCommPaidInvoiceDepositedToBank=0,@monCommUNPaidInvoice=0, @numUserCntID=0,@bitCommissionContact=0,@monCommPaidCreditMemoOrRefund=0,@monCommUnPaidCreditMemoOrRefund=0
		
		SELECT
			@numUserCntID = ISNULL(numUserCntID,0),
			@bitCommissionContact = ISNULL(bitCommissionContact,0),
			@numDivisionID=ISNULL(numDivisionID,0)
		FROM
			#tempHrs
		WHERE
			ID = @i
		
		-- EMPLOYEE (ONLY COMMISSION AMOUNT IS NEEDED FOR COMMISSION CONTACTS)
		IF @bitCommissionContact = 0
		BEGIN
			-- CALCULATE REGULAR HRS
			SELECT 
				@decTotalHrsWorked=sum(x.Hrs) 
			FROM 
				(
					SELECT  
						ISNULL(SUM(CAST(DATEDIFF(MINUTE,dtfromdate,dttodate) AS FLOAT)/CAST(60 AS FLOAT)),0) AS Hrs            
					FROM 
						TimeAndExpense 
					WHERE 
						(numType=1 OR numType=2) 
						AND numCategory=1                 
						AND (
								(DATEADD(MINUTE,-@ClientTimeZoneOffset,dtFromDate) BETWEEN @dtStartDate AND @dtEndDate) 
								OR 
								(DATEADD(MINUTE,-@ClientTimeZoneOffset,dtToDate) BETWEEN @dtStartDate AND @dtEndDate)
							) 
						AND numUserCntID=@numUserCntID  
						AND numDomainID=@numDomainId 
						AND numCategoryHDRID NOT IN (SELECT numCategoryHDRID FROM #tempPayrollTracking WHERE ISNULL(numCategoryHDRID,0)>0)
				) x

			-- CALCULATE PAID LEAVE HRS
			SELECT 
				@decTotalPaidLeaveHrs=ISNULL(
												SUM( 
														CASE 
														WHEN dtfromdate = dttodate 
														THEN 
															24 - (CASE WHEN bitfromfullday = 0 THEN 12 ELSE 0 END)
														ELSE 
															(Datediff(DAY,dtfromdate,dttodate) + 1) * 24 - (CASE WHEN bitfromfullday = 0 THEN 12 ELSE 0 END) - (CASE WHEN bittofullday = 0 THEN 12 ELSE 0 END)
														 END
													)
											 ,0)
            FROM   
				TimeAndExpense
            WHERE  
				numtype = 3 
				AND numcategory = 3 
				AND numusercntid = @numUserCntID 
				AND numdomainid = @numDomainId 
                AND (
						(DATEADD(MINUTE,-@ClientTimeZoneOffset,dtFromDate) BETWEEN @dtStartDate AND @dtEndDate)
						Or 
						(DATEADD(MINUTE,-@ClientTimeZoneOffset,dtToDate) BETWEEN @dtStartDate AND @dtEndDate)
					)
				AND numCategoryHDRID NOT IN (SELECT numCategoryHDRID FROM #tempPayrollTracking WHERE ISNULL(numCategoryHDRID,0)>0)

			-- CALCULATE EXPENSES
			SELECT 
				@monExpense=ISNULL((SUM(CAST(monAmount AS FLOAT))),0)    
			FROM 
				TimeAndExpense 
			WHERE 
				numCategory=2 
				AND numType in (1,2) 
				AND numUserCntID=@numUserCntID 
				AND numDomainID=@numDomainId 
				AND ((dtFromDate BETWEEN @dtStartDate And @dtEndDate) Or (dtToDate BETWEEN @dtStartDate And @dtEndDate)) 
				AND numCategoryHDRID NOT IN (SELECT numCategoryHDRID FROM #tempPayrollTracking WHERE ISNULL(numCategoryHDRID,0)>0)

			-- CALCULATE REIMBURSABLE EXPENSES
			SELECT 
				@monReimburse=Isnull((SUM(CAST(monamount AS FLOAT))),0)
            FROM   
				TimeAndExpense 
            WHERE  
				bitreimburse = 1 
				AND numcategory = 2 
                AND numusercntid = @numUserCntID 
				AND numdomainid = @numDomainId
                AND dtfromdate BETWEEN @dtStartDate AND @dtEndDate
                AND numCategoryHDRID NOT IN (SELECT numCategoryHDRID FROM #tempPayrollTracking WHERE ISNULL(numCategoryHDRID,0)>0)

		END

		-- CALCULATE COMMISSION PAID INVOICE
		SELECT 
			@monCommPaidInvoice=ISNULL(SUM(Amount),0) 
		FROM 
			(
				SELECT 
					oppBiz.numOppID,
					oppBiz.numOppBizDocsID,
					ISNULL(SUM(BC.numComissionAmount - ISNULL(PT.monCommissionAmt,0)),0) AS Amount
				FROM 
					OpportunityMaster Opp 
				INNER JOIN OpportunityBizDocs oppBiz on oppBiz.numOppId=opp.numOppId 
				INNER JOIN BizDocComission BC on BC.numOppBizDocId=oppBiz.numOppBizDocsId 
				LEFT JOIN #tempPayrollTracking PT ON PT.numComissionID=BC.numComissionID
				CROSS APPLY
				(
					SELECT 
						MAX(DM.dtDepositDate) dtDepositDate
					FROM 
						DepositMaster DM 
					JOIN dbo.DepositeDetails DD	ON DM.numDepositId=DD.numDepositID 
					WHERE 
						DM.tintDepositePage = 2
						AND DM.numDomainId=@numDomainId
						AND DD.numOppID=oppBiz.numOppID 
						AND DD.numOppBizDocsID =oppBiz.numOppBizDocsID
				) TEMPDeposit
				WHERE 
					Opp.numDomainId=@numDomainId 
					AND ((BC.numUserCntId=@numUserCntID AND ISNULL(BC.tintAssignTo,0) <> 3) OR (BC.numUserCntID=@numDivisionID AND BC.tintAssignTo=3))
					AND BC.bitCommisionPaid=0 
					AND oppBiz.bitAuthoritativeBizDocs = 1 
					AND isnull(oppBiz.monAmountPaid,0) >=oppBiz.monDealAmount 
					AND CAST(DATEADD(MINUTE,-@ClientTimeZoneOffset,TEMPDeposit.dtDepositDate) AS DATE) BETWEEN CAST(@dtStartDate AS DATE) AND CAST(@dtEndDate AS DATE)
				GROUP BY 
					oppBiz.numOppID,
					oppBiz.numOppBizDocsID
			) A

		-- CALCULATE COMMISSION PAID INVOICE (DEPOSITED TO BANK)
		SELECT 
			@monCommPaidInvoiceDepositedToBank=ISNULL(SUM(Amount),0) 
		FROM 
			(
				SELECT 
					oppBiz.numOppID,
					oppBiz.numOppBizDocsID,
					ISNULL(SUM(BC.numComissionAmount - ISNULL(PT.monCommissionAmt,0)),0) AS Amount
				FROM 
					OpportunityMaster Opp 
				INNER JOIN OpportunityBizDocs oppBiz on oppBiz.numOppId=opp.numOppId 
				INNER JOIN BizDocComission BC on BC.numOppBizDocId=oppBiz.numOppBizDocsId 
				LEFT JOIN #tempPayrollTracking PT ON PT.numComissionID=BC.numComissionID
				CROSS APPLY
				(
					SELECT 
						MAX(DM.dtDepositDate) dtDepositDate
					FROM 
						DepositMaster DM 
					JOIN dbo.DepositeDetails DD	ON DM.numDepositId=DD.numDepositID 
					WHERE 
						DM.tintDepositePage = 2
						AND DM.numDomainId=@numDomainId
						AND DD.numOppID=oppBiz.numOppID 
						AND DD.numOppBizDocsID =oppBiz.numOppBizDocsID
						AND DM.tintDepositePage=2 
						AND 1 = (CASE WHEN DM.tintDepositeToType=2 THEN (CASE WHEN ISNULL(DM.bitDepositedToAcnt,0)=1 THEN 1 ELSE 0 END) ELSE 1 END)
				) TEMPDeposit
				WHERE 
					Opp.numDomainId=@numDomainId 
					AND ((BC.numUserCntId=@numUserCntID AND ISNULL(BC.tintAssignTo,0) <> 3) OR (BC.numUserCntID=@numDivisionID AND BC.tintAssignTo=3))
					AND BC.bitCommisionPaid=0 
					AND oppBiz.bitAuthoritativeBizDocs = 1 
					AND isnull(oppBiz.monAmountPaid,0) >=oppBiz.monDealAmount 
					AND CAST(DATEADD(MINUTE,-@ClientTimeZoneOffset,TEMPDeposit.dtDepositDate) AS DATE) BETWEEN CAST(@dtStartDate AS DATE) AND CAST(@dtEndDate AS DATE)
				GROUP BY 
					oppBiz.numOppID,
					oppBiz.numOppBizDocsID
			) A
		

		-- CALCULATE COMMISSION UNPAID INVOICE 
		SELECT 
			@monCommUNPaidInvoice=isnull(sum(BC.numComissionAmount - ISNULL(PT.monCommissionAmt,0)),0) 
		FROM 
			OpportunityMaster Opp 
		INNER JOIN OpportunityBizDocs oppBiz on oppBiz.numOppId=opp.numOppId 
        INNER JOIN BizDocComission BC on BC.numOppBizDocId=oppBiz.numOppBizDocsId
        LEFT JOIN #tempPayrollTracking PT ON PT.numComissionID=BC.numComissionID 
		WHERE 
			Opp.numDomainId=@numDomainId 
			AND ((BC.numUserCntId=@numUserCntID AND ISNULL(BC.tintAssignTo,0) <> 3) OR (BC.numUserCntID=@numDivisionID AND BC.tintAssignTo=3))
			AND BC.bitCommisionPaid=0 
			AND oppBiz.bitAuthoritativeBizDocs = 1 
			AND ISNULL(oppBiz.monAmountPaid,0) <oppBiz.monDealAmount
			AND ((DATEADD(MINUTE,-@ClientTimeZoneOffset,oppBiz.dtFromDate) BETWEEN @dtStartDate AND @dtEndDate))	

		-- CALCULATE COMMISSION FOR PAID CREDIT MEMO/REFUND
		SET @monCommPaidCreditMemoOrRefund = ISNULL((SELECT SUM(monCommission) FROM @TempCommissionPaidCreditMemoOrRefund WHERE ((numUserCntId=@numUserCntID AND ISNULL(tintAssignTo,0) <> 3) OR (numUserCntID=@numDivisionID AND tintAssignTo=3))),0)		
		-- CALCULATE COMMISSION FOR UN-PAID CREDIT MEMO/REFUND
		SET @monCommUnPaidCreditMemoOrRefund = ISNULL((SELECT SUM(monCommission) FROM @TempCommissionUnPaidCreditMemoOrRefund WHERE ((numUserCntId=@numUserCntID AND ISNULL(tintAssignTo,0) <> 3) OR (numUserCntID=@numDivisionID AND tintAssignTo=3))),0)
	
		UPDATE 
			#tempHrs 
		SET    
			decRegularHrs=@decTotalHrsWorked,
			decPaidLeaveHrs=@decTotalPaidLeaveHrs,
			monExpense=@monExpense,
			monReimburse=@monReimburse,
			monCommPaidInvoice=@monCommPaidInvoice,
			monCommPaidInvoiceDeposited=@monCommPaidInvoiceDepositedToBank,
			monCommUNPaidInvoice=@monCommUNPaidInvoice,
			monCommPaidCreditMemoOrRefund=@monCommPaidCreditMemoOrRefund,
			monCommUnPaidCreditMemoOrRefund=@monCommUnPaidCreditMemoOrRefund,
			monTotalAmountDue= @decTotalHrsWorked * monHourlyRate + @monReimburse + @monCommPaidInvoice + @monCommUNPaidInvoice - ISNULL(@monCommPaidCreditMemoOrRefund,0)
        WHERE 
			(numUserCntID=@numUserCntID AND ISNULL(bitPartner,0) = 0) 
			OR (numDivisionID=@numDivisionID AND ISNULL(bitPartner,0) = 1)

		SET @i = @i + 1
	END

	-- CALCULATE PAID AMOUNT
	UPDATE 
		temp 
	SET 
		monAmountPaid=ISNULL(PD.monTotalAmt,0) 
	FROM 
		#tempHrs temp  
	JOIN dbo.PayrollDetail PD ON PD.numUserCntID=temp.numUserCntID
	JOIN dbo.PayrollHeader PH ON PH.numPayrollHeaderID=PD.numPayrollHeaderID
	WHERE 
		ISNULL(PD.numCheckStatus,0)=2  
		AND ((dtFromDate BETWEEN @dtStartDate AND @dtEndDate) OR (dtToDate BETWEEN @dtStartDate And @dtEndDate))
 
	SELECT * FROM #tempHrs
	DROP TABLE #tempPayrollTracking
	DROP TABLE #tempHrs
END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_SalesFulfillmentQueue_Get')
DROP PROCEDURE USP_SalesFulfillmentQueue_Get
GO
CREATE PROCEDURE [dbo].[USP_SalesFulfillmentQueue_Get]  
	
AS
BEGIN
	DELETE FROM SalesFulfillmentQueue WHERE numOppID NOT IN (SELECT numOppID FROM OpportunityMaster)


	-- FIRST DELETE ENTRIES FROM QUEUE WHICH ARE NOT VALID BECAUSE MASS FULFILLMENT RULE MAY BE CHANGED
	DELETE FROM 
		SalesFulfillmentQueue
	WHERE
		ISNULL(bitExecuted,0) = 0
		AND numSFQID NOT IN (
								SELECT 
									SFQ.numSFQID 
								FROM 
									SalesFulfillmentQueue SFQ
								INNER JOIN
									SalesFulfillmentConfiguration SFC
								ON
									SFQ.numDomainID = SFC.numDomainID
								WHERE
									(SFC.bitRule1IsActive = 1 AND SFC.tintRule1Type = 2 AND SFQ.numOrderStatus=SFC.numRule1OrderStatus)
									OR (SFC.bitRule2IsActive = 1 AND SFQ.numOrderStatus=SFC.numRule2OrderStatus)
									OR (SFC.bitRule3IsActive = 1 AND SFQ.numOrderStatus=SFC.numRule3OrderStatus)
									OR (SFC.bitRule4IsActive = 1 AND SFQ.numOrderStatus=SFC.numRule4OrderStatus)
							)
	
	-- GET TOP 25 entries
	SELECT TOP 25 
		SFQ.numSFQID,
		SFQ.numDomainID,
		SFQ.numUserCntID,
		SFQ.numOppID,
		OM.numDivisionId,
		OM.numContactID,
		D.numCurrencyID,
		D.bitMinUnitPriceRule,
		OM.tintOppStatus,
		OM.tintshipped,
		D.numDefaultSalesShippingDoc,
		D.IsEnableDeferredIncome,
		D.bitAutolinkUnappliedPayment,
		ISNULL(OM.intUsedShippingCompany,ISNULL(D.numShipCompany,91)) numShipCompany,
		CASE 
			WHEN (SFC.bitRule1IsActive = 1 AND SFC.tintRule1Type = 2 AND SFQ.numOrderStatus=SFC.numRule1OrderStatus) THEN 1
			WHEN (SFC.bitRule2IsActive = 1 AND SFQ.numOrderStatus=SFC.numRule2OrderStatus) THEN 2
			WHEN (SFC.bitRule3IsActive = 1 AND SFQ.numOrderStatus=SFC.numRule3OrderStatus) THEN 3
			WHEN (SFC.bitRule4IsActive = 1 AND SFQ.numOrderStatus=SFC.numRule4OrderStatus) THEN 4
			ELSE 0
		END	AS tintRule
	FROM 
		SalesFulfillmentQueue SFQ
	INNER JOIN
		SalesFulfillmentConfiguration SFC
	ON
		SFQ.numDomainID = SFC.numDomainID
	INNER JOIN
		OpportunityMaster OM
	ON
		SFQ.numOppID = OM.numOppId
	INNER JOIN
		Domain D
	ON
		SFQ.numDomainID = D.numDomainID
	WHERE
		ISNULL(SFQ.bitExecuted,0) = 0
		AND ((SFC.bitRule1IsActive = 1 AND SFC.tintRule1Type = 2 AND SFQ.numOrderStatus=SFC.numRule1OrderStatus)
		OR (SFC.bitRule2IsActive = 1 AND SFQ.numOrderStatus=SFC.numRule2OrderStatus)
		OR (SFC.bitRule3IsActive = 1 AND SFQ.numOrderStatus=SFC.numRule3OrderStatus)
		OR (SFC.bitRule4IsActive = 1 AND SFQ.numOrderStatus=SFC.numRule4OrderStatus))
	
END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_SalesFulfillmentWorkflow')
DROP PROCEDURE dbo.USP_SalesFulfillmentWorkflow
GO
CREATE PROCEDURE [dbo].[USP_SalesFulfillmentWorkflow]
    (
	  @numDomainID NUMERIC(9),
      @numOppID NUMERIC(9),
      @numOppBizDocsId NUMERIC(9),
      @numUserCntID NUMERIC(9),
      @numBizDocStatus NUMERIC(9),
	  @numOppAuthBizDocsId as numeric(9)=0 OUTPUT,
	  @bitNotValidateBizDocFulfillment AS BIT
    )
AS 
    BEGIN
    DECLARE @ErrorMessage NVARCHAR(4000);SET @ErrorMessage=''
    DECLARE @ErrorSeverity INT;
    DECLARE @ErrorState INT;
    
BEGIN TRY		
		--DECLARE @numBizDocStatus AS NUMERIC(9);SET @numBizDocStatus=0
		DECLARE @numBizDocId AS NUMERIC(9);SET @numBizDocId=0
		DECLARE @numRuleID AS NUMERIC(9);SET @numRuleID=0
		DECLARE @numPackingSlipBizDocId NUMERIC(9),@numAuthInvoice NUMERIC(9)
		DECLARE @tintOppType NUMERIC(9);SET @tintOppType=0
	   
		SELECT /*@numBizDocStatus=ISNULL(OBD.numBizDocStatus,0),*/@tintOppType=tintOppType,@numBizDocId=OBD.numBizDocId FROM dbo.OpportunityMaster OM JOIN dbo.OpportunityBizDocs OBD ON OM.numOppID=OBD.numOppID
				WHERE OM.numDomainId=@numDomainId AND OM.numOppID=@numOppID AND OBD.numOppBizDocsId=@numOppBizDocsId 
				/*AND ISNULL(OBD.numBizDocStatus,0)!=0 AND ISNULL(OBD.numBizDocStatus,0)!=ISNULL(OBD.numBizDocStatusOLD,0)*/
		
		IF @tintOppType=1
		BEGIN
		IF @numBizDocStatus>0 AND @numBizDocId>0
		BEGIN
			IF @numBizDocId=296
			BEGIN
				SELECT @numRuleID=ISNULL(numRuleID,0) FROM dbo.OpportunityAutomationRules WHERE numDomainID=@numDomainID AND numBizDocStatus1=@numBizDocStatus AND numRuleID IN (1,2,3,4,5,6)
				
				IF @numRuleID=1 --create an invoice for all items within the fulfillment order
				BEGIN
					Select @numAuthInvoice=isnull(numAuthoritativeSales,0) From dbo.AuthoritativeBizDocs Where AuthoritativeBizDocs.numDomainId=@numDomainId 
					
					EXEC dbo.USP_CreateBizDocs
							@numOppId = @numOppID, --  numeric(9, 0)
							@numBizDocId = @numAuthInvoice, --  numeric(9, 0)
							@numUserCntID = @numUserCntID, --  numeric(9, 0)
							@numOppBizDocsId = @numOppAuthBizDocsId output,
							@vcComments = '', --  varchar(1000)
							@bitPartialFulfillment = 1, --  bit
							@strBizDocItems = '', --  text
							--@monShipCost = 0, --  money
							@numShipVia = 0, --  numeric(9, 0)
							@vcTrackingURL = '', --  varchar(1000)
							@dtFromDate = '2013-1-2 10:55:29.610', --  datetime
							@numBizDocStatus = 0, --  numeric(9, 0)
							@bitRecurringBizDoc = 0, --  bit
							@numSequenceId = '', --  varchar(50)
							@tintDeferred = 0, --  tinyint
							@monCreditAmount =0,
							@ClientTimeZoneOffset = 0, --  int
							@monDealAmount =0,
							@bitRentalBizDoc = 0, --  bit
							@numBizDocTempID = 0, --  numeric(9, 0)
							@vcTrackingNo = '', --  varchar(500)
							--@vcShippingMethod = '', --  varchar(100)
							@vcRefOrderNo = '', --  varchar(100)
							--@dtDeliveryDate = '2013-1-2 10:55:29.611', --  datetime
							@OMP_SalesTaxPercent = 0, --  float
							@numFromOppBizDocsId = @numOppBizDocsId, --  numeric(9, 0)
							@bitTakeSequenceId = 1,  --bit
							@bitNotValidateBizDocFulfillment=@bitNotValidateBizDocFulfillment
					
				END
				ELSE IF @numRuleID=2 --create a packing slip for its inventory items
				BEGIN
					SELECT @numPackingSlipBizDocId=numListItemID FROM dbo.ListDetails WHERE ListDetails.vcData='Packing Slip' AND ListDetails.constFlag=1 AND ListDetails.numListID=27
				
					EXEC dbo.USP_CreateBizDocs
								@numOppId = @numOppID, --  numeric(9, 0)
								@numBizDocId = @numPackingSlipBizDocId, --  numeric(9, 0)
								@numUserCntID = @numUserCntID, --  numeric(9, 0)
								@numOppBizDocsId =0,
								@vcComments = '', --  varchar(1000)
								@bitPartialFulfillment = 1, --  bit
								@strBizDocItems = '', --  text
								--@monShipCost = 0, --  money
								@numShipVia = 0, --  numeric(9, 0)
								@vcTrackingURL = '', --  varchar(1000)
								@dtFromDate = '2013-1-2 10:56:4.477', --  datetime
								@numBizDocStatus = 0, --  numeric(9, 0)
								@bitRecurringBizDoc = 0, --  bit
								@numSequenceId = '', --  varchar(50)
								@tintDeferred = 0, --  tinyint
								@monCreditAmount =0,
								@ClientTimeZoneOffset = 0, --  int
								@monDealAmount =0,
								@bitRentalBizDoc = 0, --  bit
								@numBizDocTempID = 0, --  numeric(9, 0)
								@vcTrackingNo = '', --  varchar(500)
								--@vcShippingMethod = '', --  varchar(100)
								@vcRefOrderNo = '', --  varchar(100)
								--@dtDeliveryDate = '2013-1-2 10:56:4.477', --  datetime
								@OMP_SalesTaxPercent = 0, --  float
								@numFromOppBizDocsId = @numOppBizDocsId, --  numeric(9, 0)
								@bitTakeSequenceId = 1,  --bit
								@bitNotValidateBizDocFulfillment=@bitNotValidateBizDocFulfillment
				END
				ELSE IF @numRuleID=3 --release its inventory items from allocation
				BEGIN
					BEGIN TRY		
						EXEC dbo.USP_SalesFulfillmentUpdateInventory
							@numDomainID = @numDomainID, --  numeric(9, 0)
							@numOppID = @numOppID, --  numeric(9, 0)
							@numOppBizDocsId = @numOppBizDocsId, --  numeric(9, 0)
							@numUserCntID = @numUserCntID,  --  numeric(9, 0) 
							@tintMode = 1 --  tinyint
					END TRY
					BEGIN CATCH
					SELECT @ErrorMessage = ERROR_MESSAGE(),
								       @ErrorSeverity = ERROR_SEVERITY(),
									   @ErrorState = ERROR_STATE();
									   
						IF @ErrorMessage='NotSufficientQty_Allocation'
						BEGIN
							SELECT @numBizDocStatus=ISNULL(numBizDocStatus1,0) FROM dbo.OpportunityAutomationRules 
								WHERE numDomainID=@numDomainID AND ISNULL(numRuleID,0)=6
							
							EXEC dbo.USP_UpdateSingleFieldValue
								@tintMode = 27, --  tinyint
								@numUpdateRecordID = @numOppBizDocsId, --  numeric(18, 0)
								@numUpdateValueID = @numBizDocStatus, --  numeric(18, 0)
								@vcText = '', --  text
								@numDomainID = @numDomainID --  numeric(18, 0)
								
								INSERT INTO dbo.OppFulfillmentBizDocsStatusHistory (
										numDomainId,numOppId,numOppBizDocsId,numBizDocStatus,numUserCntID,dtCreatedDate
										) VALUES ( 
										@numDomainID,@numOppID,@numOppBizDocsId,@numBizDocStatus,@numUserCntID,GETUTCDATE() ) 
						END	

						RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorState);
						
					END CATCH;		
				END
				ELSE IF @numRuleID=4 --return its inventory items back to allocation
				BEGIN
					Select @numAuthInvoice=isnull(numAuthoritativeSales,0) From dbo.AuthoritativeBizDocs Where AuthoritativeBizDocs.numDomainId=@numDomainId 
				
						IF EXISTS (SELECT numOppBizDocsId FROM OpportunityMaster OM JOIN OpportunityBizDocs OBD ON OM.numOppID=OBD.numOppID
							WHERE OM.numDomainId=@numDomainId AND OM.numOppID=@numOppID  AND OBD.numBizDocId IN (@numAuthInvoice))
						BEGIN
								RAISERROR ('AlreadyInvoice_DoNotReturn',16,1);
								RETURN -1
						END		
						ELSE
						BEGIN
								EXEC dbo.USP_SalesFulfillmentUpdateInventory
							@numDomainID = @numDomainID, --  numeric(9, 0)
							@numOppID = @numOppID, --  numeric(9, 0)
							@numOppBizDocsId = @numOppBizDocsId, --  numeric(9, 0)
							@numUserCntID = @numUserCntID,  --  numeric(9, 0) 
							@tintMode = 2 --  tinyint
						END
				END
				ELSE IF @numRuleID=5 --Remove the Fulfillment Order from the Sales Fulfillment queue
				BEGIN
					PRINT 'Rule-5'
				
				END
			END
		END
		
		IF @numBizDocStatus>0
		BEGIN
				INSERT INTO dbo.OppFulfillmentBizDocsStatusHistory (
					numDomainId,
					numOppId,
					numOppBizDocsId,
					numBizDocStatus,
					numUserCntID,
					dtCreatedDate
				) VALUES ( 
					/* numDomainId - numeric(18, 0) */ @numDomainID,
					/* numOppId - numeric(18, 0) */ @numOppID,
					/* numOppBizDocsId - numeric(18, 0) */ @numOppBizDocsId,
					/* numBizDocStatus - numeric(18, 0) */ @numBizDocStatus,
					/* numUserCntID - numeric(18, 0) */ @numUserCntID,
					/* dtCreatedDate - datetime */ GETUTCDATE() )  
		 END			
	
	END
			 END TRY
BEGIN CATCH

    SELECT 
        @ErrorMessage = ERROR_MESSAGE(),
        @ErrorSeverity = ERROR_SEVERITY(),
        @ErrorState = ERROR_STATE();

    -- Use RAISERROR inside the CATCH block to return error
    -- information about the original error that caused
    -- execution to jump to the CATCH block.
    RAISERROR (@ErrorMessage, -- Message text.
               @ErrorSeverity, -- Severity.
               @ErrorState -- State.
               );
END CATCH;
		
    END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- exec USP_OPPItemDetails @numItemCode=31455,@OpportunityId=12387,@numDomainId=110
--created by anooop jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ShippingReport_Save')
DROP PROCEDURE USP_ShippingReport_Save
GO
CREATE PROCEDURE [dbo].[USP_ShippingReport_Save]
	@numDomainId NUMERIC(18,0)
	,@numUserCntID NUMERIC(18,0)
	,@numOppID NUMERIC(18,0)
    ,@numOppBizDocId NUMERIC(18,0)
AS
BEGIN
BEGIN TRY
BEGIN TRANSACTION
	
	DECLARE @numShippingReportID NUMERIC(18,0)
	DECLARE @IsCOD BIT,@IsDryIce BIT,@IsHoldSaturday BIT,@IsHomeDelivery BIT,@IsInsideDelevery BIT,@IsInsidePickup BIT,@IsReturnShipment BIT,@IsSaturdayDelivery BIT,@IsSaturdayPickup BIT,@IsAdditionalHandling BIT,@IsLargePackage BIT
	DECLARE @numCODAmount NUMERIC(18,2),@vcCODType VARCHAR(50),@numTotalInsuredValue NUMERIC(18,2),@vcDeliveryConfirmation VARCHAR(1000),@vcDescription VARCHAR(MAX),@numTotalCustomsValue NUMERIC(18,2)
	DECLARE @tintSignatureType TINYINT
	DECLARE @numOrderShippingCompany NUMERIC(18,0)
	DECLARE @numOrderShippingService NUMERIC(18,0)
	DECLARE @numDivisionShippingCompany NUMERIC(18,0)
    DECLARE @numDivisionShippingService NUMERIC(18,0)

	SELECT
		@IsCOD = IsCOD
		,@IsHomeDelivery = IsHomeDelivery
		,@IsInsideDelevery = IsInsideDelevery
		,@IsInsidePickup = IsInsidePickup
		,@IsSaturdayDelivery = IsSaturdayDelivery
		,@IsSaturdayPickup = IsSaturdayPickup
		,@IsAdditionalHandling = IsAdditionalHandling
		,@IsLargePackage=IsLargePackage
		,@vcCODType = vcCODType
		,@vcDeliveryConfirmation = vcDeliveryConfirmation
		,@vcDescription = vcDescription
		,@tintSignatureType = CAST(ISNULL(vcSignatureType,'0') AS TINYINT)
		,@numOrderShippingCompany = ISNULL(numShipmentMethod,0)
		,@numOrderShippingService = ISNULL(intUsedShippingCompany,0)
		,@numDivisionShippingCompany = ISNULL(intShippingCompany,0)
		,@numDivisionShippingService = ISNULL(numDefaultShippingServiceID,0)
	FROM
		OpportunityMaster
	INNER JOIN
		DivisionMaster
	ON
		OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
	INNER JOIN
		DivisionMasterShippingConfiguration
	ON
		DivisionMaster.numDivisionID = DivisionMasterShippingConfiguration.numDivisionID
	WHERE
		OpportunityMaster.numDomainId = @numDomainId
		AND DivisionMaster.numDomainID=@numDomainId
		AND DivisionMasterShippingConfiguration.numDomainID=@numDomainId
		AND OpportunityMaster.numOppId = @numOppID



	DECLARE @vcFromName VARCHAR(1000)
    DECLARE @vcFromCompany VARCHAR(1000)
    DECLARE @vcFromPhone VARCHAR(100)
    DECLARE @vcFromAddressLine1 VARCHAR(50)
    DECLARE @vcFromAddressLine2 VARCHAR(50) = ''
    DECLARE @vcFromCity VARCHAR(50)
	DECLARE @vcFromState VARCHAR(50)
    DECLARE @vcFromZip VARCHAR(50)
    DECLARE @vcFromCountry VARCHAR(50)
	DECLARE @bitFromResidential BIT = 0

	DECLARE @vcToName VARCHAR(1000)
    DECLARE @vcToCompany VARCHAR(1000)
    DECLARE @vcToPhone VARCHAR(100)
    DECLARE @vcToAddressLine1 VARCHAR(50)
    DECLARE @vcToAddressLine2 VARCHAR(50) = ''
	DECLARE @vcToCity VARCHAR(50)
	DECLARE @vcToState VARCHAR(50)
    DECLARE @vcToZip VARCHAR(50)
    DECLARE @vcToCountry VARCHAR(50)
    DECLARE @bitToResidential BIT = 0

	-- GET FROM ADDRESS
	SELECT
		@vcFromName=vcName
		,@vcFromCompany=vcCompanyName
		,@vcFromPhone=vcPhone
		,@vcFromAddressLine1=vcStreet
		,@vcFromCity=vcCity
		,@vcFromState=CAST(vcState AS VARCHAR(18))
		,@vcFromZip=vcZipCode
		,@vcFromCountry=CAST(vcCountry AS VARCHAR(18))
	FROM
		dbo.fn_GetShippingReportAddress(1,@numDomainId,@numOppID)

	-- GET TO ADDRESS
	SELECT
		@vcToName=vcName
		,@vcToCompany=vcCompanyName
		,@vcToPhone=vcPhone
		,@vcToAddressLine1=vcStreet
		,@vcToCity=vcCity
		,@vcToState=CAST(vcState AS VARCHAR(18))
		,@vcToZip=vcZipCode
		,@vcToCountry=CAST(vcCountry AS VARCHAR(18))
	FROM
		dbo.fn_GetShippingReportAddress(2,@numDomainId,@numOppID)

	DECLARE @numShippingCompany NUMERIC(18,0)
	DECLARE @numShippingService NUMERIC(18,0)
	

	-- FIRST CHECK IF ORDER LEVEL SHIPPING FIELDS HAVE VALUES
	IF ISNULL(@numOrderShippingCompany,0) > 0
	BEGIN
		If @numOrderShippingCompany >= 10 And @numOrderShippingCompany <= 28
		BEGIN
             SET @numOrderShippingCompany = 91
		END
        ELSE IF (@numOrderShippingCompany = 40 Or
                @numOrderShippingCompany = 42 Or
                @numOrderShippingCompany = 43 Or
                @numOrderShippingCompany = 48 Or
                @numOrderShippingCompany = 49 Or
                @numOrderShippingCompany = 50 Or
                @numOrderShippingCompany = 51 Or
                @numOrderShippingCompany = 55) 
		BEGIN
            SET @numOrderShippingCompany = 88
		END
        Else If @numOrderShippingCompany >= 70 And @numOrderShippingCompany <= 76
		BEGIN
            SET @numOrderShippingCompany = 90
		END


		IF ISNULL(@numOrderShippingService,0) = 0
		BEGIN
			SET @numOrderShippingService = CASE @numOrderShippingCompany 
									WHEN 91 THEN 15 --FEDEX
									WHEN 88 THEN 43 --UPS
									WHEN 90 THEN 70 --USPS
								 END
		END

		SET @numShippingCompany = @numOrderShippingCompany
		SET @numShippingService = @numOrderShippingService
	END
	ELSE IF ISNULL(@numDivisionShippingCompany,0) > 0 -- IF DIVISION SHIPPIGN SETTING AVAILABLE
	BEGIN
		If @numDivisionShippingCompany >= 10 And @numDivisionShippingCompany <= 28
		BEGIN
             SET @numDivisionShippingCompany = 91
		END
        ELSE IF (@numDivisionShippingCompany = 40 Or
                @numDivisionShippingCompany = 42 Or
                @numDivisionShippingCompany = 43 Or
                @numDivisionShippingCompany = 48 Or
                @numDivisionShippingCompany = 49 Or
                @numDivisionShippingCompany = 50 Or
                @numDivisionShippingCompany = 51 Or
                @numDivisionShippingCompany = 55) 
		BEGIN
            SET @numDivisionShippingCompany = 88
		END
        Else If @numDivisionShippingCompany >= 70 And @numDivisionShippingCompany <= 76
		BEGIN
            SET @numDivisionShippingCompany = 90
		END


		IF ISNULL(@numDivisionShippingService,0) = 0
		BEGIN
			SET @numDivisionShippingService = CASE @numDivisionShippingCompany 
									WHEN 91 THEN 15 --FEDEX
									WHEN 88 THEN 43 --UPS
									WHEN 90 THEN 70 --USPS
								 END
		END

		SET @numShippingCompany = @numDivisionShippingCompany
		SET @numShippingService = @numDivisionShippingService
	END
	ELSE -- USE DOMAIN DEFAULT
	BEGIN
		SELECT @numShippingCompany=ISNULL(numShipCompany,0) FROM Domain WHERE numDomainId=@numDomainId
		
		IF @numShippingCompany <> 91 OR @numShippingCompany <> 88 OR @numShippingCompany <> 90
		BEGIN
			SET @numShippingCompany = 91
		END

		SET @numShippingService = CASE @numShippingCompany 
									WHEN 91 THEN 15 --FEDEX
									WHEN 88 THEN 43 --UPS
									WHEN 90 THEN 70 --USPS
								 END
	END



	DECLARE @bitUseBizdocAmount BIT

	SELECT @bitUseBizdocAmount=bitUseBizdocAmount,@numTotalCustomsValue=ISNULL(numTotalInsuredValue,0),@numTotalInsuredValue=ISNULL(numTotalCustomsValue,0) FROM Domain WHERE numDomainId=@numDomainId

	IF @bitUseBizdocAmount = 1
	BEGIN
		SELECT @numTotalCustomsValue=ISNULL(monDealAmount,0),@numTotalInsuredValue=ISNULL(monDealAmount,0),@numCODAmount=ISNULL(monDealAmount,0) FROM OpportunityBizDocs WHERE numOppBizDocsId=@numOppBizDocId 
	END
	ELSE
	BEGIN
		SELECT @numCODAmount=ISNULL(monDealAmount,0) FROM OpportunityBizDocs WHERE numOppBizDocsId=@numOppBizDocId 
	END

	-- MAKE ENTRY IN ShippingReport TABLE
	INSERT INTO ShippingReport
	(
		numDomainId,numCreatedBy,dtCreateDate,numOppID,numOppBizDocId,numShippingCompany,vcValue2
		,vcFromName,vcFromCompany,vcFromPhone,vcFromAddressLine1,vcFromAddressLine2,vcFromCity,vcFromState,vcFromZip,vcFromCountry,bitFromResidential
		,vcToName,vcToCompany,vcToPhone,vcToAddressLine1,vcToAddressLine2,vcToCity,vcToState,vcToZip,vcToCountry,bitToResidential
		,IsCOD,IsDryIce,IsHoldSaturday,IsHomeDelivery,IsInsideDelevery,IsInsidePickup,IsReturnShipment,IsSaturdayDelivery,IsSaturdayPickup
		,numCODAmount,vcCODType,numTotalInsuredValue,IsAdditionalHandling,IsLargePackage,vcDeliveryConfirmation,vcDescription,numTotalCustomsValue,tintSignatureType
	)
	VALUES
	(
		@numDomainId,@numUserCntID,GETUTCDATE(),@numOppID,@numOppBizDocId,@numShippingCompany,CAST(@numShippingService AS VARCHAR)
		,@vcFromName,@vcFromCompany,@vcFromPhone,@vcFromAddressLine1,@vcFromAddressLine2,@vcFromCity,@vcFromState,@vcFromZip,@vcFromCountry,@bitFromResidential
		,@vcToName,@vcToCompany,@vcToPhone,@vcToAddressLine1,@vcToAddressLine2,@vcToCity,@vcToState,@vcToZip,@vcToCountry,@bitToResidential
		,@IsCOD,@IsDryIce,@IsHoldSaturday,@IsHomeDelivery,@IsInsideDelevery,@IsInsidePickup,@IsReturnShipment,@IsSaturdayDelivery,@IsSaturdayPickup
		,@numCODAmount,@vcCODType,@numTotalInsuredValue,@IsAdditionalHandling,@IsLargePackage,@vcDeliveryConfirmation,@vcDescription,@numTotalCustomsValue,@tintSignatureType
	)

	SET @numShippingReportID = SCOPE_IDENTITY()

	-- GET CONTAINER BY WAREHOUSE AND NOOFITEMSCANBEADDED
	DECLARE @TEMPContainer TABLE
	(
		ID INT IDENTITY(1,1)
		,numContainer NUMERIC(18,0)
		,numWareHouseID NUMERIC(18,0)
		,numContainerQty NUMERIC(18,0)
		,numNoItemIntoContainer NUMERIC(18,0)
		,numTotalContainer NUMERIC(18,0)
		,numUsedContainer NUMERIC(18,0)
		,bitItemAdded BIT
		,fltWeight FLOAT
		,fltHeight FLOAT
		,fltWidth FLOAT
		,fltLength FLOAT
	)

	INSERT INTO 
		@TEMPContainer
	SELECT
		T1.numContainer
		,T1.numWareHouseID
		,T1.numNoItemIntoContainer
		,T1.numNoItemIntoContainer
		,T1.numTotalContainer
		,0 AS numTotalContainer
		,0 AS bitItemAdded
		,T2.fltWeight
		,T2.fltHeight
		,T2.fltWidth
		,T2.fltLength
	FROM
	(
		SELECT 
			I.numContainer
			,WI.numWareHouseID
			,I.numNoItemIntoContainer
			,CEILING(SUM(OBI.numUnitHour)/CAST(I.numNoItemIntoContainer AS FLOAT)) AS numTotalContainer
			,0 AS numUsedContainer
		FROM 
			OpportunityBizDocItems OBI
		INNER JOIN 
			Item AS I
		ON 
			OBI.numItemCode = I.numItemCode
		INNER JOIN
			WareHouseItems WI
		ON
			OBI.numWarehouseItmsID = WI.numWareHouseItemID
		WHERE
			numOppBizDocID=@numOppBizDocID
			AND I.charItemType ='P'
			AND ISNULL(numContainer,0) > 0
		GROUP BY
			I.numContainer
			,WI.numWareHouseID
			,I.numNoItemIntoContainer
	) AS T1
	INNER JOIN
	(
		SELECT
			I.numItemCode
			,WI.numWareHouseID
			,ISNULL(I.fltWeight,0) fltWeight
			,ISNULL(I.fltHeight,0) fltHeight
			,ISNULL(I.fltWidth,0) fltWidth
			,ISNULL(I.fltLength,0) fltLength
		FROM 
			OpportunityBizDocItems OBI
		INNER JOIN 
			Item AS I
		ON 
			OBI.numItemCode = I.numItemCode
		INNER JOIN
			WareHouseItems WI
		ON
			OBI.numWarehouseItmsID = WI.numWareHouseItemID
		WHERE
			numOppBizDocID=@numOppBizDocID
			AND I.charItemType ='P'
			AND ISNULL(I.bitContainer,0) = 1
		GROUP BY
			I.numItemCode
			,WI.numWareHouseID
			,ISNULL(I.fltWeight,0)
			,ISNULL(I.fltHeight,0)
			,ISNULL(I.fltWidth,0)
			,ISNULL(I.fltLength,0)
	) AS T2
	ON
		T1.numContainer = T2.numItemCode
		AND T1.numWareHouseID = T2.numWareHouseID

	IF (SELECT COUNT(*) FROM @TEMPContainer) = 0
	BEGIN
		RAISERROR('Container(s) are not available in bizdoc.',16,1)
	END
	ELSE
	BEGIN
		DECLARE @TEMPItems TABLE
		(
			ID INT IDENTITY(1,1)
			,numOppBizDocItemID NUMERIC(18,0)
			,numItemCode NUMERIC(18,0)
			,numWarehouseID NUMERIC(18,0)
			,numUnitHour FLOAT
			,numContainer NUMERIC(18,0)
			,numNoItemIntoContainer NUMERIC(18,0)
			,fltItemWidth FLOAT
			,fltItemHeight FLOAT
			,fltItemLength FLOAT
			,fltItemWeight FLOAT
		)

		INSERT INTO 
			@TEMPItems
		SELECT
			OBDI.numOppBizDocItemID
			,I.numItemCode
			,ISNULL(WI.numWareHouseID,0)
			,ISNULL(OBDI.numUnitHour,0)
			,ISNULL(I.numContainer,0)
			,ISNULL(I.numNoItemIntoContainer,0)
			,ISNULL(fltWidth,0)
			,ISNULL(fltHeight,0)
			,ISNULL(fltLength,0)
			,ISNULL(fltWeight,0)
		FROM
			OpportunityBizDocs OBD
		INNER JOIN
			OpportunityBizDocItems OBDI
		ON
			OBD.numOppBizDocsId=OBDI.numOppBizDocID
		INNER JOIN
			WareHouseItems WI
		ON
			OBDI.numWarehouseItmsID=WI.numWareHouseItemID
		INNER JOIN
			Item I
		ON
			OBDI.numItemCode=I.numItemCode
		WHERE
			numOppId=@numOppID
			AND numOppBizDocsId=@numOppBizDocId
			AND ISNULL(I.numContainer,0) > 0

		DECLARE @i AS INT = 1
		DEClARE @j AS INT = 1
		DECLARE @iCount as INT
		SELECT @iCount = COUNT(*) FROM @TEMPItems 

		DECLARE @numBoxID NUMERIC(18,0)
		DECLARE @numTempQty AS FLOAT
		DECLARE @numTempOppBizDocItemID AS NUMERIC(18,0)
		DECLARE @numTempContainerQty AS NUMERIC(18,0)
		DECLARE @numtempContainer AS NUMERIC(18,0)
		DECLARE @numTempItemCode AS NUMERIC(18,0)
		DECLARE @numTempWarehouseID AS NUMERIC(18,0)
		DECLARE @fltTempItemWidth AS FLOAT
		DECLARE @fltTempItemHeight AS FLOAT
		DECLARE @fltTempItemLength AS FLOAT
		DECLARE @fltTempItemWeight AS FLOAT

		IF ISNULL(@iCount,0) > 0
		BEGIN
			WHILE @i <= @iCount
			BEGIN
				SELECT 
					@numTempOppBizDocItemID=numOppBizDocItemID
					,@numtempContainer=numContainer
					,@numTempItemCode=numItemCode
					,@numTempWarehouseID=numWarehouseID
					,@numTempQty=numUnitHour
					,@numTempContainerQty=numNoItemIntoContainer
					,@fltTempItemWidth=fltItemWidth
					,@fltTempItemHeight=fltItemHeight
					,@fltTempItemLength=fltItemLength
					,@fltTempItemWeight=fltItemWeight
				FROM 
					@TEMPItems 
				WHERE 
					ID=@i

				IF EXISTS (SELECT * FROM @TEMPContainer WHERE numContainer=@numtempContainer AND numWareHouseID=@numTempWarehouseID AND numContainerQty=@numTempContainerQty AND numUsedContainer < numTotalContainer)
				BEGIN
					DECLARE @numID AS INT
					DECLARE @numTempTotalContainer NUMERIC(18,0)
					DECLARE @numTempMainContainerQty NUMERIC(18,0)
					DECLARE @bitTempItemAdded BIT
					DECLARE @fltTempWeight FLOAT
					DECLARE @fltTempHeight FLOAT
					DECLARE @fltTempWidth FLOAT
					DECLARE @fltTempLength FLOAT

					SELECT
						@numID=ID
						,@numTempTotalContainer=numTotalContainer
						,@numTempMainContainerQty=numNoItemIntoContainer
						,@bitTempItemAdded=bitItemAdded
						,@fltTempWeight=fltWeight
						,@fltTempHeight=fltHeight
						,@fltTempWidth=fltWidth
						,@fltTempLength=fltLength
					FROM
						@TEMPContainer 
					WHERE 
						numContainer=@numtempContainer 
						AND numWareHouseID=@numTempWarehouseID 
						AND numContainerQty=@numTempContainerQty

					WHILE @numTempQty > 0
					BEGIN
						 If @numTempTotalContainer > 0
						 BEGIN
							IF @numTempQty <= @numTempMainContainerQty
							BEGIN
								IF ISNULL(@bitTempItemAdded,0) = 0 OR @numTempMainContainerQty=@numTempContainerQty
								BEGIN
									-- CREATE NEW SHIPING BOX
									INSERT INTO ShippingBox
									(
										vcBoxName
										,numShippingReportId
										,fltTotalWeight
										,fltHeight
										,fltWidth
										,fltLength
										,dtCreateDate
										,numCreatedBy
										,numPackageTypeID
										,numServiceTypeID
										,fltDimensionalWeight
										,numShipCompany
									)
									VALUES
									(
										CONCAT('Box',@j)
										,@numShippingReportID
										,(CASE WHEN ISNULL(@fltTempWeight,0) = 0 THEN CEILING(CAST((CASE WHEN ISNULL(@fltTempHeight,0)=0 THEN 1 ELSE @fltTempHeight END) * (CASE WHEN ISNULL(@fltTempWidth,0)=0 THEN 1 ELSE @fltTempWidth END) * (CASE WHEN ISNULL(@fltTempLength,0)=0 THEN 1 ELSE @fltTempLength END) AS FLOAT)/166) ELSE @fltTempWeight END)
										,@fltTempHeight
										,@fltTempWidth
										,@fltTempLength
										,GETUTCDATE()
										,@numUserCntID
										,31
										,@numShippingService
										,(CASE WHEN ISNULL(@fltTempWeight,0) = 0 THEN CEILING(CAST((CASE WHEN ISNULL(@fltTempHeight,0)=0 THEN 1 ELSE @fltTempHeight END) * (CASE WHEN ISNULL(@fltTempWidth,0)=0 THEN 1 ELSE @fltTempWidth END) * (CASE WHEN ISNULL(@fltTempLength,0)=0 THEN 1 ELSE @fltTempLength END) AS FLOAT)/166) ELSE @fltTempWeight END)
										,@numShippingCompany
										
									)

									SELECT @numBoxID = SCOPE_IDENTITY()

									UPDATE @TEMPContainer SET bitItemAdded = 1 WHERE ID=@numID
									
									SET @j = @j + 1
								END
								ELSE
								BEGIN
									SELECT @numBoxID=numBoxID FROM ShippingBox WHERE numShippingReportId=@numShippingReportID AND vcBoxName=CONCAT('Box',@j)
								END

								SET @fltTempItemWeight = (CASE WHEN ISNULL(@fltTempItemWeight,0) > 0 THEN @fltTempItemWeight ELSE CEILING(CAST(@fltTempItemHeight * @fltTempItemWidth * @fltTempItemLength AS FLOAT)/166) END)

								UPDATE ShippingBox SET fltTotalWeight = ISNULL(fltTotalWeight,0) + (@fltTempItemWeight * @numTempQty) WHERE numBoxID=@numBoxID

								-- MAKE ENTRY IN ShippingReportItems Table
								INSERT INTO ShippingReportItems
								(
									numShippingReportId
									,numItemCode
									,tintServiceType
									,monShippingRate
									,fltTotalWeight
									,intNoOfBox
									,fltHeight
									,fltWidth
									,fltLength
									,dtCreateDate
									,numCreatedBy
									,numBoxID
									,numOppBizDocItemID
									,intBoxQty
								)
								VALUES
								(
									@numShippingReportID
									,@numTempItemCode
									,@numShippingService
									,0
									,@fltTempItemWeight
									,1
									,@fltTempHeight
									,@fltTempItemWidth
									,@fltTempItemLength
									,GETUTCDATE()
									,@numUserCntID
									,@numBoxID
									,@numTempOppBizDocItemID
									,@numTempQty
								)

								UPDATE @TEMPContainer SET numNoItemIntoContainer=numNoItemIntoContainer - @numTempQty WHERE ID=@numID
								SET @numTempMainContainerQty = @numTempMainContainerQty - @numTempQty

								IF @numTempMainContainerQty = 0
								BEGIN
									-- IF NO QTY LEFT TO ADD IN CONTAINER RESET CONAINER QTY AND MAKE FLAG bitItemAdded=0 SO IF numUsedContainer < numTotalContainer THEN NEW SHIPPING BOX WILL BE CREATED
									UPDATE @TEMPContainer SET numNoItemIntoContainer=numContainerQty,bitItemAdded=0,numUsedContainer = numUsedContainer + 1 WHERE ID=@numID
								END

								SET @numTempQty = 0
							END
							ELSE 
							BEGIN
								-- CREATE NEW SHIPING BOX
								INSERT INTO ShippingBox
								(
									vcBoxName
									,numShippingReportId
									,fltTotalWeight
									,fltHeight
									,fltWidth
									,fltLength
									,dtCreateDate
									,numCreatedBy
									,numPackageTypeID
									,numServiceTypeID
									,fltDimensionalWeight
									,numShipCompany
								)
								VALUES
								(
									CONCAT('Box',@j)
									,@numShippingReportID
									,(CASE WHEN ISNULL(@fltTempWeight,0) = 0 THEN CEILING(CAST((CASE WHEN ISNULL(@fltTempHeight,0)=0 THEN 1 ELSE @fltTempHeight END) * (CASE WHEN ISNULL(@fltTempWidth,0)=0 THEN 1 ELSE @fltTempWidth END) * (CASE WHEN ISNULL(@fltTempLength,0)=0 THEN 1 ELSE @fltTempLength END) AS FLOAT)/166) ELSE @fltTempWeight END)
									,@fltTempHeight
									,@fltTempWidth
									,@fltTempLength
									,GETUTCDATE()
									,@numUserCntID
									,31
									,@numShippingService
									,(CASE WHEN ISNULL(@fltTempWeight,0) = 0 THEN CEILING(CAST((CASE WHEN ISNULL(@fltTempHeight,0)=0 THEN 1 ELSE @fltTempHeight END) * (CASE WHEN ISNULL(@fltTempWidth,0)=0 THEN 1 ELSE @fltTempWidth END) * (CASE WHEN ISNULL(@fltTempLength,0)=0 THEN 1 ELSE @fltTempLength END) AS FLOAT)/166) ELSE @fltTempWeight END)
									,@numShippingCompany
										
								)

								SELECT @numBoxID = SCOPE_IDENTITY()

								UPDATE @TEMPContainer SET numNoItemIntoContainer=numContainerQty,bitItemAdded=0,numUsedContainer = numUsedContainer + 1 WHERE ID=@numID
									
								SET @j = @j + 1

								--SELECT @numBoxID=numBoxID FROM ShippingBox WHERE numShippingReportId=@numShippingReportID AND vcBoxName=CONCAT('Box',@j)

								SET @fltTempItemWeight = (CASE WHEN ISNULL(@fltTempItemWeight,0) > CEILING(CAST(@fltTempItemHeight * @fltTempItemWidth * @fltTempItemLength AS FLOAT)/139) THEN @fltTempItemWeight ELSE CEILING(CAST(@fltTempItemHeight * @fltTempItemWidth * @fltTempItemLength AS FLOAT)/139) END)

								UPDATE ShippingBox SET fltTotalWeight = ISNULL(fltTotalWeight,0) + (@fltTempItemWeight * @numTempMainContainerQty) WHERE numBoxID=@numBoxID

								-- MAKE ENTRY IN ShippingReportItems Table
								INSERT INTO ShippingReportItems
								(
									numShippingReportId
									,numItemCode
									,tintServiceType
									,monShippingRate
									,fltTotalWeight
									,intNoOfBox
									,fltHeight
									,fltWidth
									,fltLength
									,dtCreateDate
									,numCreatedBy
									,numBoxID
									,numOppBizDocItemID
									,intBoxQty
								)
								VALUES
								(
									@numShippingReportID
									,@numTempItemCode
									,@numShippingService
									,0
									,@fltTempItemWeight
									,1
									,@fltTempHeight
									,@fltTempItemWidth
									,@fltTempItemLength
									,GETUTCDATE()
									,@numUserCntID
									,@numBoxID
									,@numTempOppBizDocItemID
									,@numTempMainContainerQty
								)

								UPDATE @TEMPContainer SET numNoItemIntoContainer=@numTempContainerQty,numTotalContainer=numTotalContainer-1 WHERE ID=@numID 

								SET @numTempQty = @numTempQty - @numTempMainContainerQty
							END
						 END
						 ELSE
						 BEGIN
							RAISERROR('Sufficient container(s) are not available.',16,1)
							BREAK
						 END
					END
				END
				ELSE
				BEGIN
					RAISERROR('Sufficient container(s) require for item and warehouse are not available.',16,1)
					BREAK
				END


				SET @i = @i + 1
			END
		END
		ELSE
		BEGIN
			RAISERROR('Inventory item(s) are not available in bizdoc.',16,1)
		END
	END

	SELECT @numShippingReportID
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);

END CATCH
END
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_UpdateBizDocNameTemplate')
DROP PROCEDURE USP_UpdateBizDocNameTemplate
GO
--SELECT * FROM dbo.ReturnHeader
--EXEC USP_UpdateBizDocNameTemplate  6,1,18
CREATE PROCEDURE USP_UpdateBizDocNameTemplate
@tintType TINYINT = 0,
@numDomainID NUMERIC,
@RecordID NUMERIC-- pass Orderid ,BizDocID,ProjectID, or ReturnHeaderID
AS 
BEGIN

	DECLARE @Tempate VARCHAR(200)
	DECLARE @OrgName AS VARCHAR(100)
	DECLARE @numBizDocId AS NUMERIC,@OrderID AS NUMERIC 
	DECLARE @numSequenceId NUMERIC(9),@numMinLength NUMERIC(9)
	
	IF ISNULL(@tintType,0) > 2
	BEGIN
		BEGIN TRY
		BEGIN TRANSACTION
			SET @numSequenceId=0
			
			IF @tintType =3 --Sales Credit Memo
				SET @numBizDocId = (SELECT numListItemID FROM dbo.ListDetails WHERE numListID=27 AND constFlag=1 AND (vcData='Sales Credit Memo'))
			ELSE IF @tintType =4 --Purchase Credit Memo
				SET @numBizDocId = (SELECT numListItemID FROM dbo.ListDetails WHERE numListID=27 AND constFlag=1 AND (vcData='Purchase Credit Memo'))
			ELSE IF @tintType =5 --Refund Receipt
				SET @numBizDocId = (SELECT numListItemID FROM dbo.ListDetails WHERE numListID=27 AND constFlag=1 AND (vcData='Refund Receipt'))
			ELSE IF @tintType =6 --Credit Memo
				SET @numBizDocId = (SELECT numListItemID FROM dbo.ListDetails WHERE numListID=27 AND constFlag=1 AND (vcData='Credit Memo'))
			ELSE IF @tintType =7 --Sales RMA (Mirror BizDoc)
				SET @numBizDocId = (SELECT numListItemID FROM dbo.ListDetails WHERE numListID=27 AND constFlag=1 AND (vcData='RMA'))
				
			IF ISNULL(@numBizDocId,0) =0 
			BEGIN
				RAISERROR('BizDoc Type Not found!!',16,1);
				RETURN ;
			END
				
			IF NOT EXISTS(SELECT * FROM dbo.NameTemplate WHERE numDomainID=@numDomainID AND numRecordID=@numBizDocId AND tintModuleID=2)
			BEGIN
				INSERT INTO NameTemplate(numDomainId,tintModuleID,numRecordID,vcNameTemplate,numSequenceId,numMinLength)
					SELECT @numDomainId,2,@numBizDocId,CASE WHEN @numBizDocId=290 THEN 'PROI' WHEN @tintType=3 THEN 'SCM' WHEN @tintType=4 THEN 'PCM' WHEN @tintType=5 THEN 'RFND' WHEN @tintType=6 THEN 'CM' WHEN @tintType=7 THEN 'RMA' ELSE UPPER(SUBSTRING(dbo.GetListIemName(@numBizDocId),0,4)) END + '-',1,4
			END
				

			SELECT 
				@Tempate = ISNULL(vcNameTemplate,'')
				,@numSequenceId=ISNULL(numSequenceId,1)
				,@numMinLength=ISNULL(numMinLength,4) 
			FROM 
				dbo.NameTemplate 
			WHERE 
				numDomainID=@numDomainID 
				AND numRecordID=@numBizDocId 
				AND tintModuleID=2			
			
			--Replace Other Values
			SET @Tempate = REPLACE(@Tempate,'$YEAR$',DATENAME(year ,GETUTCDATE()) )
			SET @Tempate = REPLACE(@Tempate,'$MONTH$',DATENAME(month ,GETUTCDATE()) )
			SET @Tempate = REPLACE(@Tempate,'$DAY$',DATENAME(day ,GETUTCDATE()) )
			SET @Tempate = REPLACE(@Tempate,'$QUARTER$',DATENAME(quarter ,GETUTCDATE()) )
				
			IF CHARINDEX(lower(@Tempate),'$organizationname$')>=0
			BEGIN
				SELECT @OrgName= ISNULL(C.vcCompanyName,'') FROM dbo.ReturnHeader RH INNER JOIN dbo.DivisionMaster DM ON RH.numDivisionId=DM.numDivisionID
				INNER JOIN dbo.CompanyInfo C ON C.numCompanyId = DM.numCompanyID
				WHERE RH.numReturnHeaderID=@RecordID;
				
				SET @Tempate = REPLACE(@Tempate,'$OrganizationName$',@OrgName)
			END
			
			SET @Tempate = @Tempate + ISNULL(REPLICATE('0', @numMinLength - LEN(@numSequenceId)),'') + CAST(@numSequenceId AS VARCHAR(18))
			
			PRINT @Tempate
			
			IF EXISTS (SELECT numReturnHeaderID FROM ReturnHeader WHERE numDomainId=@numDomainID AND numReturnHeaderID <> @RecordID AND vcRMA = @Tempate)
			BEGIN
				RAISERROR('Duplicate return name!!',16,1);
				RETURN ;
			END
			
			IF @tintType =3 OR @tintType =4  
				UPDATE dbo.ReturnHeader SET vcBizDocName = @Tempate WHERE numReturnHeaderID = @RecordID
			ELSE IF @tintType =5 OR @tintType =6	
				UPDATE dbo.ReturnHeader SET vcBizDocName = @Tempate,vcRMA=(CASE WHEN tintReturnType=1 THEN vcRMA ELSE @Tempate END) WHERE numReturnHeaderID = @RecordID
			ELSE IF  @tintType =7 --RMA
				UPDATE dbo.ReturnHeader SET vcRMA= @Tempate WHERE numReturnHeaderID = @RecordID
			
			UPDATE NameTemplate SET numSequenceId=numSequenceId + 1  WHERE numDomainID=@numDomainID 
				AND numRecordID=@numBizDocId AND tintModuleID=2
				
		COMMIT
		END TRY
		BEGIN CATCH
			DECLARE @ErrorMessage NVARCHAR(4000)
			DECLARE @ErrorNumber INT
			DECLARE @ErrorSeverity INT
			DECLARE @ErrorState INT
			DECLARE @ErrorLine INT
			DECLARE @ErrorProcedure NVARCHAR(200)

			IF @@TRANCOUNT > 0
				ROLLBACK TRANSACTION;

			SELECT 
				@ErrorMessage = ERROR_MESSAGE(),
				@ErrorNumber = ERROR_NUMBER(),
				@ErrorSeverity = ERROR_SEVERITY(),
				@ErrorState = ERROR_STATE(),
				@ErrorLine = ERROR_LINE(),
				@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

			RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
		END CATCH
	END
	ELSE
	BEGIN
	
		SELECT @numBizDocId = ISNULL(numBizDocId,0),@OrderID = ISNULL(numOppId,0) FROM dbo.OpportunityBizDocs WHERE numOppBizDocsId =@RecordID
		SET @numSequenceId=0
		
		IF NOT EXISTS(SELECT * FROM dbo.NameTemplate WHERE numDomainID=@numDomainID AND numRecordID=@numBizDocId AND tintModuleID=2)
		BEGIN
			INSERT INTO NameTemplate(numDomainId,tintModuleID,numRecordID,vcNameTemplate,numSequenceId,numMinLength)
				SELECT @numDomainId,2,@numBizDocId,CASE WHEN @numBizDocId=290 THEN 'PROI' ELSE UPPER(SUBSTRING(dbo.GetListIemName(@numBizDocId),0,4)) END + '-',1,4
		END
		
		--Get Custom Template If enabled
		SELECT @Tempate = ISNULL(vcNameTemplate,'')/*,@numSequenceId=ISNULL(numSequenceId,1),@numMinLength=ISNULL(numMinLength,4)*/ FROM dbo.NameTemplate WHERE numDomainID=@numDomainID 
		AND numRecordID=@numBizDocId AND tintModuleID=2
		
		--Replace Other Values
		SET @Tempate = REPLACE(@Tempate,'$YEAR$',DATENAME(year ,GETUTCDATE()) )
		SET @Tempate = REPLACE(@Tempate,'$MONTH$',DATENAME(month ,GETUTCDATE()) )
		SET @Tempate = REPLACE(@Tempate,'$DAY$',DATENAME(day ,GETUTCDATE()) )
		SET @Tempate = REPLACE(@Tempate,'$QUARTER$',DATENAME(quarter ,GETUTCDATE()) )
			
			IF CHARINDEX(lower(@Tempate),'$organizationname$')>=0
			BEGIN
				SELECT @OrgName= ISNULL(C.vcCompanyName,'') FROM dbo.OpportunityMaster OM INNER JOIN dbo.DivisionMaster DM ON OM.numDivisionId=DM.numDivisionID
				INNER JOIN dbo.CompanyInfo C ON C.numCompanyId = DM.numCompanyID
				WHERE numOppId=@OrderID;
				
				SET @Tempate = REPLACE(@Tempate,'$OrganizationName$',@OrgName)
			END
			
			SET @Tempate = REPLACE(@Tempate,'$OrderID$', @OrderID)
			SET @Tempate = REPLACE(@Tempate,'$BizDocID$',@RecordID )
			
			SET @Tempate = @Tempate --+ ISNULL(REPLICATE('0', @numMinLength - LEN(@numSequenceId)),'') + CAST(@numSequenceId AS VARCHAR(18))
			
			PRINT @Tempate
			
			SELECT @numSequenceId=ISNULL(MAX(CAST(numSequenceId AS BIGINT)),1) FROM dbo.OpportunityMaster OM JOIN dbo.OpportunityBizDocs OBD ON OM.numOppId=OBD.numOppId
			 WHERE OM.numDomainId=@numDomainID AND OBD.numBizDocId=@numBizDocId
			
			UPDATE dbo.OpportunityBizDocs SET vcBizDocName = @Tempate/*,numSequenceId=ISNULL(@numSequenceId,0) + 1*/  WHERE numOppBizDocsId=@RecordID
			
			UPDATE NameTemplate SET numSequenceId=ISNULL(@numSequenceId,0) + 1  WHERE numDomainID=@numDomainID 
				AND numRecordID=@numBizDocId AND tintModuleID=2
	END
END
/****** Object:  StoredProcedure [dbo].[USP_UpdatedomainDetails]    Script Date: 05/07/2009 21:56:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                                      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_updatedomaindetails')
DROP PROCEDURE usp_updatedomaindetails
GO
CREATE PROCEDURE [dbo].[USP_UpdatedomainDetails]                                      
@numDomainID as numeric(9)=0,                                      
@vcDomainName as varchar(50)='',                                      
@vcDomainDesc as varchar(100)='',                                      
@bitExchangeIntegration as bit,                                      
@bitAccessExchange as bit,                                      
@vcExchUserName as varchar(100)='',                                      
@vcExchPassword as varchar(100)='',                                      
@vcExchPath as varchar(200)='',                                      
@vcExchDomain as varchar(100)='',                                    
@tintCustomPagingRows as tinyint,                                    
@vcDateFormat as varchar(30)='',                                    
@numDefCountry as numeric(9),                                    
@tintComposeWindow as tinyint,                                    
@sintStartDate as smallint,                                    
@sintNoofDaysInterval as smallint,                                    
@tintAssignToCriteria as tinyint,                                    
@bitIntmedPage as bit,                                    
@tintFiscalStartMonth as tinyint,                                      
@tintPayPeriod as tinyint,                        
@numCurrencyID as numeric(9) ,                
@charUnitSystem as char(1)='',              
@vcPortalLogo as varchar(100)='',            
@tintChrForComSearch as tinyint  ,            
@intPaymentGateWay as int, -- DO NOT UPDATE VALUE FROM THIS PARAMETER
@tintChrForItemSearch as tinyint,
@ShipCompany as numeric(9),
@bitMultiCurrency as bit,
@bitCreateInvoice AS tinyint = null,
@numDefaultSalesBizDocID AS NUMERIC(9)=NULL,
@numDefaultPurchaseBizDocID AS NUMERIC(9)=NULL,
@bitSaveCreditCardInfo AS BIT = NULL,
@intLastViewedRecord AS INT=10,
@tintCommissionType AS TINYINT,
@tintBaseTax AS TINYINT,
@bitEmbeddedCost AS BIT,
@numDefaultSalesOppBizDocId NUMERIC=NULL,
@tintSessionTimeOut TINYINT=1,
@tintDecimalPoints TINYINT,
@bitCustomizePortal BIT=0,
@tintBillToForPO TINYINT,
@tintShipToForPO TINYINT,
@tintOrganizationSearchCriteria TINYINT,
@bitDocumentRepositary BIT=0,
@tintComAppliesTo TINYINT=0,
@bitGtoBContact BIT=0,
@bitBtoGContact BIT=0,
@bitGtoBCalendar BIT=0,
@bitBtoGCalendar BIT=0,
@bitExpenseNonInventoryItem BIT=0,
@bitInlineEdit BIT=0,
@bitRemoveVendorPOValidation BIT=0,
@tintBaseTaxOnArea tinyint=0,
@bitAmountPastDue BIT=0,
@monAmountPastDue money,
@bitSearchOrderCustomerHistory BIT=0,
@bitRentalItem as bit=0,
@numRentalItemClass as NUMERIC(9)=NULL,
@numRentalHourlyUOM as NUMERIC(9)=NULL,
@numRentalDailyUOM as NUMERIC(9)=NULL,
@tintRentalPriceBasedOn as tinyint=NULL,
@tintCalendarTimeFormat as tinyint=NULL,
@tintDefaultClassType as tinyint=NULL,
@tintPriceBookDiscount as tinyint=0,
@bitAutoSerialNoAssign as bit=0,
@bitIsShowBalance AS BIT = 0,
@numIncomeAccID NUMERIC(18, 0) = 0,
@numCOGSAccID NUMERIC(18, 0) = 0,
@numAssetAccID NUMERIC(18, 0) = 0,
@IsEnableProjectTracking AS BIT = 0,
@IsEnableCampaignTracking AS BIT = 0,
@IsEnableResourceScheduling AS BIT = 0,
@ShippingServiceItem  AS NUMERIC(9)=0,
@numSOBizDocStatus AS NUMERIC(9)=0,
@bitAutolinkUnappliedPayment AS BIT=0,
@numDiscountServiceItemID AS NUMERIC(9)=0,
@vcShipToPhoneNumber as VARCHAR(50)='',
@vcGAUserEmailId as VARCHAR(50)='',
@vcGAUserPassword as VARCHAR(50)='',
@vcGAUserProfileId as VARCHAR(50)='',
@bitDiscountOnUnitPrice AS BIT=0,
@intShippingImageWidth INT = 0,
@intShippingImageHeight INT = 0,
@numTotalInsuredValue NUMERIC(10,2) = 0,
@numTotalCustomsValue NUMERIC(10,2) = 0,
@vcHideTabs VARCHAR(1000) = '',
@bitUseBizdocAmount BIT = 0,
@bitDefaultRateType BIT = 0,
@bitIncludeTaxAndShippingInCommission  BIT = 0,
@bitPurchaseTaxCredit BIT=0,
@bitLandedCost BIT=0,
@vcLanedCostDefault VARCHAR(10)='',
@bitMinUnitPriceRule BIT = 0,
@numAbovePercent AS NUMERIC(18,2),
@numBelowPercent AS NUMERIC(18,2),
@numBelowPriceField AS NUMERIC(18,0),
@vcUnitPriceApprover AS VARCHAR(100),
@numDefaultSalesPricing AS TINYINT,
@bitReOrderPoint AS BIT = 0,
@numReOrderPointOrderStatus AS NUMERIC(18,0) = 0,
@numPODropShipBizDoc AS NUMERIC(18,0) = 0,
@numPODropShipBizDocTemplate AS NUMERIC(18,0) = 0,
@IsEnableDeferredIncome AS BIT = 0,
@bitApprovalforTImeExpense AS BIT=0,
@numCost AS Numeric(9,0)=0,
@intTimeExpApprovalProcess int=0,
@bitApprovalforOpportunity AS BIT=0,
@intOpportunityApprovalProcess int=0,
@numDefaultSalesShippingDoc NUMERIC(18,0)=0,
@numDefaultPurchaseShippingDoc NUMERIC(18,0)=0,
@bitchkOverRideAssignto AS BIT=0,
@vcPrinterIPAddress VARCHAR(15) = '',
@vcPrinterPort VARCHAR(5) = '',
@numDefaultSiteID NUMERIC(18,0) = 0,
@vcSalesOrderTabs VARCHAR(200)='',
@vcSalesQuotesTabs VARCHAR(200)='',
@vcItemPurchaseHistoryTabs VARCHAR(200)='',
@vcItemsFrequentlyPurchasedTabs VARCHAR(200)='',
@vcOpenCasesTabs VARCHAR(200)='',
@vcOpenRMATabs VARCHAR(200)='',
@bitSalesOrderTabs BIT=0,
@bitSalesQuotesTabs BIT=0,
@bitItemPurchaseHistoryTabs BIT=0,
@bitItemsFrequentlyPurchasedTabs BIT=0,
@bitOpenCasesTabs BIT=0,
@bitOpenRMATabs BIT=0,
@bitRemoveGlobalLocation BIT = 0,
@bitSupportTabs BIT=0,
@vcSupportTabs VARCHAR(200)='',
@numAuthorizePercentage NUMERIC(18,0)=0,
@bitEDI BIT = 0
as                                      
BEGIN
BEGIN TRY
BEGIN TRANSACTION

	IF ISNULL(@bitMinUnitPriceRule,0) = 1 AND ((SELECT COUNT(*) FROM UnitPriceApprover WHERE numDomainID=@numDomainID) = 0 OR ISNULL(@bitApprovalforOpportunity,0)=0 OR ISNULL(@intOpportunityApprovalProcess,0) = 0)
	BEGIN
		RAISERROR('INCOMPLETE_MIN_UNIT_PRICE_CONFIGURATION',16,1)
		RETURN
	END


	If ISNULL(@bitApprovalforTImeExpense,0) = 1 AND (SELECT
														COUNT(*) 
													FROM
														UserMaster
													LEFT JOIN
														ApprovalConfig
													ON
														ApprovalConfig.numUserId = UserMaster.numUserDetailId
													WHERE
														UserMaster.numDomainID=@numDomainID
														AND numModule=1
														AND ISNULL(numGroupID,0) > 0
														AND ISNULL(bitActivateFlag,0) = 1
														AND ISNULL(numLevel1Authority,0) = 0
														AND ISNULL(numLevel2Authority,0) = 0
														AND ISNULL(numLevel3Authority,0) = 0
														AND ISNULL(numLevel4Authority,0) = 0
														AND ISNULL(numLevel5Authority,0) = 0) > 0
	BEGIN
		RAISERROR('INCOMPLETE_TIMEEXPENSE_APPROVAL',16,1)
		RETURN
	END

                                  
 update Domain                                       
   set                                       
   vcDomainName=@vcDomainName,                                      
   vcDomainDesc=@vcDomainDesc,                                      
   bitExchangeIntegration=@bitExchangeIntegration,                                      
   bitAccessExchange=@bitAccessExchange,                                      
   vcExchUserName=@vcExchUserName,                                      
   vcExchPassword=@vcExchPassword,                                      
   vcExchPath=@vcExchPath,                                      
   vcExchDomain=@vcExchDomain,                                    
   tintCustomPagingRows =@tintCustomPagingRows,                                    
   vcDateFormat=@vcDateFormat,                                    
   numDefCountry =@numDefCountry,                                    
   tintComposeWindow=@tintComposeWindow,                                    
   sintStartDate =@sintStartDate,                                    
   sintNoofDaysInterval=@sintNoofDaysInterval,                                    
   tintAssignToCriteria=@tintAssignToCriteria,                                    
   bitIntmedPage=@bitIntmedPage,                                    
   tintFiscalStartMonth=@tintFiscalStartMonth,                            
--   bitDeferredIncome=@bitDeferredIncome,                          
   tintPayPeriod=@tintPayPeriod,
   vcCurrency=isnull((select varCurrSymbol from Currency Where numCurrencyID=@numCurrencyID  ),'')  ,                      
  numCurrencyID=@numCurrencyID,                
  charUnitSystem= @charUnitSystem,              
  vcPortalLogo=@vcPortalLogo ,            
  tintChrForComSearch=@tintChrForComSearch  ,      
 tintChrForItemSearch=@tintChrForItemSearch,
 numShipCompany= @ShipCompany,
 bitMultiCurrency=@bitMultiCurrency,
 bitCreateInvoice= @bitCreateInvoice,
 [numDefaultSalesBizDocId] =@numDefaultSalesBizDocId,
 bitSaveCreditCardInfo=@bitSaveCreditCardInfo,
 intLastViewedRecord=@intLastViewedRecord,
 [tintCommissionType]=@tintCommissionType,
 [tintBaseTax]=@tintBaseTax,
 [numDefaultPurchaseBizDocId]=@numDefaultPurchaseBizDocID,
 bitEmbeddedCost=@bitEmbeddedCost,
 numDefaultSalesOppBizDocId=@numDefaultSalesOppBizDocId,
 tintSessionTimeOut=@tintSessionTimeOut,
 tintDecimalPoints=@tintDecimalPoints,
 bitCustomizePortal=@bitCustomizePortal,
 tintBillToForPO = @tintBillToForPO,
 tintShipToForPO = @tintShipToForPO,
 tintOrganizationSearchCriteria=@tintOrganizationSearchCriteria,
-- vcPortalName=@vcPortalName,
 bitDocumentRepositary=@bitDocumentRepositary,
 --tintComAppliesTo=@tintComAppliesTo,THIS FIELD IS REMOVED FROM GLOBAL SETTINGS
 bitGtoBContact=@bitGtoBContact,
 bitBtoGContact=@bitBtoGContact,
 bitGtoBCalendar=@bitGtoBCalendar,
 bitBtoGCalendar=@bitBtoGCalendar,
 bitExpenseNonInventoryItem=@bitExpenseNonInventoryItem,
 bitInlineEdit=@bitInlineEdit,
 bitRemoveVendorPOValidation=@bitRemoveVendorPOValidation,
tintBaseTaxOnArea=@tintBaseTaxOnArea,
bitAmountPastDue = @bitAmountPastDue,
monAmountPastDue = @monAmountPastDue,
bitSearchOrderCustomerHistory=@bitSearchOrderCustomerHistory,
bitRentalItem=@bitRentalItem,
numRentalItemClass=@numRentalItemClass,
numRentalHourlyUOM=@numRentalHourlyUOM,
numRentalDailyUOM=@numRentalDailyUOM,
tintRentalPriceBasedOn=@tintRentalPriceBasedOn,
tintCalendarTimeFormat=@tintCalendarTimeFormat,
tintDefaultClassType=@tintDefaultClassType,
tintPriceBookDiscount=@tintPriceBookDiscount,
bitAutoSerialNoAssign=@bitAutoSerialNoAssign,
bitIsShowBalance = @bitIsShowBalance,
numIncomeAccID = @numIncomeAccID,
numCOGSAccID = @numCOGSAccID,
numAssetAccID = @numAssetAccID,
IsEnableProjectTracking = @IsEnableProjectTracking,
IsEnableCampaignTracking = @IsEnableCampaignTracking,
IsEnableResourceScheduling = @IsEnableResourceScheduling,
numShippingServiceItemID = @ShippingServiceItem,
numSOBizDocStatus=@numSOBizDocStatus,
bitAutolinkUnappliedPayment=@bitAutolinkUnappliedPayment,
numDiscountServiceItemID=@numDiscountServiceItemID,
vcShipToPhoneNo = @vcShipToPhoneNumber,
vcGAUserEMail = @vcGAUserEmailId,
vcGAUserPassword = @vcGAUserPassword,
vcGAUserProfileId = @vcGAUserProfileId,
bitDiscountOnUnitPrice=@bitDiscountOnUnitPrice,
intShippingImageWidth = @intShippingImageWidth ,
intShippingImageHeight = @intShippingImageHeight,
numTotalInsuredValue = @numTotalInsuredValue,
numTotalCustomsValue = @numTotalCustomsValue,
vcHideTabs = @vcHideTabs,
bitUseBizdocAmount = @bitUseBizdocAmount,
bitDefaultRateType = @bitDefaultRateType,
bitIncludeTaxAndShippingInCommission = @bitIncludeTaxAndShippingInCommission,
/*,bitAllowPPVariance=@bitAllowPPVariance*/
bitPurchaseTaxCredit=@bitPurchaseTaxCredit,
bitLandedCost=@bitLandedCost,
vcLanedCostDefault=@vcLanedCostDefault,
bitMinUnitPriceRule = @bitMinUnitPriceRule,
numDefaultSalesPricing = @numDefaultSalesPricing,
numPODropShipBizDoc=@numPODropShipBizDoc,
numPODropShipBizDocTemplate=@numPODropShipBizDocTemplate,
IsEnableDeferredIncome=@IsEnableDeferredIncome,
bitApprovalforTImeExpense=@bitApprovalforTImeExpense,
numCost= @numCost,intTimeExpApprovalProcess=@intTimeExpApprovalProcess,
bitApprovalforOpportunity=@bitApprovalforOpportunity,intOpportunityApprovalProcess=@intOpportunityApprovalProcess,
 numDefaultPurchaseShippingDoc=@numDefaultPurchaseShippingDoc,numDefaultSalesShippingDoc=@numDefaultSalesShippingDoc,
 bitchkOverRideAssignto=@bitchkOverRideAssignto,
 vcPrinterIPAddress=@vcPrinterIPAddress,
 vcPrinterPort=@vcPrinterPort,
 numDefaultSiteID=ISNULL(@numDefaultSiteID,0),
  vcSalesOrderTabs=@vcSalesOrderTabs,
 vcSalesQuotesTabs=@vcSalesQuotesTabs,
 vcItemPurchaseHistoryTabs=@vcItemPurchaseHistoryTabs,
 vcItemsFrequentlyPurchasedTabs=@vcItemsFrequentlyPurchasedTabs,
 vcOpenCasesTabs=@vcOpenCasesTabs,
 vcOpenRMATabs=@vcOpenRMATabs,
 bitSalesOrderTabs=@bitSalesOrderTabs,
 bitSalesQuotesTabs=@bitSalesQuotesTabs,
 bitItemPurchaseHistoryTabs=@bitItemPurchaseHistoryTabs,
 bitItemsFrequentlyPurchasedTabs=@bitItemsFrequentlyPurchasedTabs,
 bitOpenCasesTabs=@bitOpenCasesTabs,
 bitOpenRMATabs=@bitOpenRMATabs,
 bitRemoveGlobalLocation=@bitRemoveGlobalLocation,
  bitSupportTabs=@bitSupportTabs,
 vcSupportTabs=@vcSupportTabs,
 numAuthorizePercentage=@numAuthorizePercentage,
 bitEDI=ISNULL(@bitEDI,0)
 where numDomainId=@numDomainID
COMMIT

 IF ISNULL(@bitRemoveGlobalLocation,0) = 1
 BEGIN
	DECLARE @TEMPGlobalWarehouse TABLE
	(
		ID INT IDENTITY(1,1)
		,numItemCode NUMERIC(18,0)
		,numWarehouseID NUMERIC(18,0)
		,numWarehouseItemID NUMERIC(28,0)
	)

	INSERT INTO @TEMPGlobalWarehouse
	(
		numItemCode
		,numWarehouseID
		,numWarehouseItemID
	)
	SELECT
		numItemID
		,numWareHouseID
		,numWareHouseItemID
	FROM
		WareHouseItems
	WHERE
		numDomainID=@numDomainID
		AND numWLocationID = -1
		AND ISNULL(numOnHand,0) = 0
		AND ISNULL(numOnOrder,0)=0
		AND ISNULL(numAllocation,0)=0
		AND ISNULL(numBackOrder,0)=0


	DECLARE @i AS INT = 1
	DECLARE @iCount AS INT
	DECLARE @numTempItemCode AS NUMERIC(18,0)
	DECLARE @numTempWareHouseID AS NUMERIC(18,0)
	DECLARE @numTempWareHouseItemID AS NUMERIC(18,0)
	SELECT @iCount=COUNT(*) FROM @TEMPGlobalWarehouse

	WHILE @i <= @iCount
	BEGIN
		SELECT @numTempItemCode=numItemCode,@numTempWareHouseID=numWarehouseID,@numTempWareHouseItemID=numWarehouseItemID FROM @TEMPGlobalWarehouse WHERE ID=@i

		BEGIN TRY
			EXEC USP_AddUpdateWareHouseForItems @numTempItemCode,  
												@numTempWareHouseID,
												@numTempWareHouseItemID,
												'',
												0,
												0,
												0,
												'',
												'',
												@numDomainID,
												'',
												'',
												'',
												0,
												3,
												0,
												0,
												NULL,
												0
		END TRY
		BEGIN CATCH

		END CATCH

		SET @i = @i + 1
	END
 END

END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH

END
/****** Object:  StoredProcedure [dbo].[USP_UserList]    Script Date: 07/26/2008 16:21:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj    
--Modified By Sachin Sadhu
--Worked on AND-OR condition       
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_userlist')
DROP PROCEDURE usp_userlist
GO
CREATE PROCEDURE [dbo].[USP_UserList]                  
 @UserName as varchar(100)='',                  
 @tintGroupType as tinyint,                  
 @SortChar char(1)='0',                  
 @CurrentPage int,                  
 @PageSize int,                  
 @TotRecs int output,                  
 @columnName as Varchar(50),                  
 @columnSortOrder as Varchar(10),            
 @numDomainID as numeric(9),          
 @Status as char(1)                  
                  
as                  
                     
Create table #tempTable ( ID INT IDENTITY PRIMARY KEY,                   
      numUserID VARCHAR(15),                  
      vcEmailID varchar(100),                  
      [Name] VARCHAR(100),                  
      vcGroupName varchar(100),                  
 vcDomainName varchar(50),          
 Active varchar(10),
 numDefaultWarehouse numeric(18,0)              
 )                  
declare @strSql as varchar(8000)                  
set @strSql='SELECT numUserID, isnull(vcEmailID,''-'') as vcEmailID,                  
ISNULL(ADC.vcfirstname,'''') +'' ''+ ISNULL(ADC.vclastname,'''') as Name,                  
isnull(vcGroupName,''-'')as vcGroupName, vcDomainName, case when bitActivateFlag=1 then ''Active'' else ''Suspended'' end as bitActivateFlag, numDefaultWarehouse                          
   FROM UserMaster                    
   join Domain                      
   on UserMaster.numDomainID =  Domain.numDomainID                     
    left join AdditionalContactsInformation ADC                    
   on ADC.numContactid=UserMaster.numUserDetailId                   
   left join AuthenticationGroupMaster GM                   
   on Gm.numGroupID= UserMaster.numGroupID              
    where  UserMaster.numDomainID='+convert(varchar(15),@numDomainID)+ ' and bitActivateFlag=' +@Status+' and numContactId in (select numLevel1Authority from ApprovalConfig where numLevel1Authority <> 0 )'               
                  
if @SortChar<>'0' set @strSql=@strSql + ' And (vcUserName like '''+@SortChar+'%'' OR vcEmailID LIKE '''+@SortChar+'%'')'

if @UserName<>''  set @strSql=@strSql + ' And (vcUserName like '''+@UserName+'%'' OR vcEmailID LIKE '''+@UserName+'%'')'

if @columnName<>'' set @strSql=@strSql + ' ORDER BY ' + @columnName +' '+ @columnSortOrder                  
   print @strSql               
insert into #tempTable(                  
 numUserID,                  
      vcEmailID,                  
      [Name],                  
      vcGroupName,                  
 vcDomainName,Active,numDefaultWarehouse)                  
exec (@strSql)                   
                  
  declare @firstRec as integer                   
  declare @lastRec as integer                  
 set @firstRec= (@CurrentPage-1) * @PageSize                  
     set @lastRec= (@CurrentPage*@PageSize+1)                  
select * from #tempTable where ID > @firstRec and ID < @lastRec                  
set @TotRecs=(select count(*) from #tempTable)                  
drop table #tempTable            
          
          
select  vcCompanyName,ISNULL(intNoofUsersSubscribed,0) + ISNULL(intNoofPartialSubscribed,0)/2 + ISNULL(intNoofMinimalSubscribed,0)/10 AS intNoofUsersSubscribed,dtSubStartDate,dtSubEndDate,(select count(*) from UserMaster  join Domain                      
   on UserMaster.numDomainID =  Domain.numDomainID                     
    join AdditionalContactsInformation ADC                    
   on ADC.numContactid=UserMaster.numUserDetailId  where Domain.numDomainID=@numDomainID and bitActivateFlag=1) as ActiveSub          
from Subscribers S           
Join Domain D          
on D.numDomainID=S.numTargetDomainID          
join DivisionMaster Div          
on Div.numDivisionID=D.numDivisionID          
join CompanyInfo C          
on C.numCompanyID=Div.numCompanyID          
where D.numDomainID=@numDomainID
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_WorkFlowMaster_History')
DROP PROCEDURE USP_WorkFlowMaster_History
GO
  
 
-- =============================================      
-- Author:  <Author,,Sachin Sadhu>      
-- Create date: <Create Date,,3rdApril2014>      
-- Description: <Description,,To maintain Work rule execution history>      
-- =============================================      
Create PROCEDURE [dbo].[USP_WorkFlowMaster_History]       
 -- Add the parameters for the stored procedure here      
@numDomainID numeric(18,0),      
@numFormID INT  ,    
@CurrentPage int,                                                              
@PageSize int,                                                              
@TotRecs int output,           
@SortChar char(1)='0' ,                                                             
@columnName as Varchar(50),                                                              
@columnSortOrder as Varchar(50)  ,      
@SearchStr  as Varchar(50)         
AS      
BEGIN      
 -- SET NOCOUNT ON added to prevent extra result sets from      
 -- interfering with SELECT statements.      
 SET NOCOUNT ON;      
      
    -- Insert statements for procedure here      
    Create table #tempTable ( ID INT IDENTITY PRIMARY KEY,                                                               
        numWFID NUMERIC(18,0) ,    
        numFormID NUMERIC(18,0),    
        vcWFName VARCHAR(50),      
        vcPOppName VARCHAR(500),      
        vcWFDescription VARCHAR(max),      
        numRecordID NUMERIC(18,0),      
        tintProcessStatus int,      
        bitSuccess int,      
        vcDescription VARCHAR(max),      
        vcFormName VARCHAR(50),      
        dtExecutionDate DATETIME,    
        numOppId NUMERIC(18,0),      
        STATUS VARCHAR(50),    
        TriggeredOn VARCHAR(50) ,  
        vcUserName VARCHAR(200)   ,
		dtCreatedDate     DATETIME                                                   
 )           
declare @strSql as varchar(8000)                                                        
          
set  @strSql='  SELECT       
      
   m.numWFID,      
   m.numFormID,    
   m.vcWFName,      
   ISNULL(o.vcPOppName,'''') as vcPOppName,      
   m.vcWFDescription,      
   q.numRecordID,      
   q.tintProcessStatus,      
   e.bitSuccess,      
  ISNULL( e.vcDescription,''Waiting'') as  vcDescription,      
   DFM.vcFormName,      
   e.dtExecutionDate,    
   o.numOppId,     
   CASE WHEN q.tintProcessStatus=1 THEN ''Pending Execution'' WHEN q.tintProcessStatus= 2 THEN ''In Progress''  WHEN q.tintProcessStatus=3 THEN ''Success'' WHEN q.tintProcessStatus=4 then ''Failed'' WHEN q.tintProcessStatus=6 then ''Condition Not Matched'' else ''No WF Found'' END AS STATUS,      
   CASE WHEN (m.tintWFTriggerOn=1 AND m.intDays=0)THEN ''Create'' WHEN m.tintWFTriggerOn=2 THEN ''Edit'' WHEN m.tintWFTriggerOn=3 THEN ''Create or Edit'' WHEN m.tintWFTriggerOn=4 THEN ''Fields Update'' WHEN m.tintWFTriggerOn=5 THEN ''Delete'' WHEN m.intDays>0 THEN     
''Date Field'' ELSE ''NA'' end AS TriggeredOn,  
 dbo.fn_GetContactName(ISNULL(m.numCreatedBy,0)) AS vcUserName   ,
 q.dtCreatedDate   
  FROM dbo.WorkFlowMaster as m       
  INNER JOIN dbo.WorkFlowQueue AS q ON m.numWFID=q.numWFID       
  LEFT JOIN dbo.WorkFlowQueueExecution AS e ON e.numWFQueueID=q.numWFQueueID      
  LEFT JOIN dbo.OpportunityMaster o ON q.numRecordID =o.numOppId  AND o.numDomainID=m.numDomainID
  JOIN DynamicFormMaster DFM ON m.numFormID=DFM.numFormID AND DFM.tintFlag=3 AND q.tintProcessStatus NOT IN (6,5)  WHERE m.numDomainID='+ convert(varchar(15),@numDomainID)       
          
if @SortChar<>'0' set @strSql=@strSql + ' And m.vcWFName like '''+@SortChar+'%'''           
      
if @numFormID <> 0 set @strSql=@strSql + ' And m.numFormID = '+ convert(varchar(15),@numFormID) +''         
      
if @SearchStr<>'' set @strSql=@strSql + ' And (m.vcWFName like ''%'+@SearchStr+'%'' or       
m.vcWFDescription like ''%'+@SearchStr+'%'') '       
          
set  @strSql=@strSql +'ORDER BY ' + @columnName +' '+ @columnSortOrder          
     
insert into #tempTable( numWFID ,    
            numFormID ,    
            vcWFName ,    
            vcPOppName ,    
            vcWFDescription ,    
            numRecordID ,    
            tintProcessStatus ,    
            bitSuccess ,    
            vcDescription ,    
            vcFormName ,    
            dtExecutionDate ,    
            numOppId ,    
            STATUS ,    
            TriggeredOn,vcUserName,dtCreatedDate) exec(@strSql)          
          
 declare @firstRec as integer                                                              
 declare @lastRec as integer                                                   
 set @firstRec= (@CurrentPage-1) * @PageSize                                                              
 set @lastRec= (@CurrentPage*@PageSize+1)                                                               
      
 SELECT  @TotRecs = COUNT(*)  FROM #tempTable       
       
  SELECT       
   ID AS RowNo,*    
   From #tempTable T        
   WHERE ID > @firstRec and ID < @lastRec  order by ID      
       
       
--   SELECT       
--   ROW_NUMBER() OVER(ORDER BY m.numWFID ASC) AS RowNo,    
--   m.numWFID,      
--   m.vcWFName,      
--   o.vcPOppName,      
--   m.vcWFDescription,      
--   q.numRecordID,      
--   q.tintProcessStatus,      
--   e.bitSuccess,      
--   e.vcDescription ,      
--   DFM.vcFormName,      
--   e.dtExecutionDate,    
--   o.numOppId,      
--   CASE WHEN q.tintProcessStatus=1 THEN 'Pending Execution' WHEN q.tintProcessStatus= 2 THEN 'In Progress' WHEN q.tintProcessStatus=3 THEN 'Success' WHEN q.tintProcessStatus=4 then 'Failed' else 'No WF Found' END AS STATUS,      
--   CASE WHEN (m.tintWFTriggerOn=1 AND m.intDays=0)THEN 'Create' WHEN m.tintWFTriggerOn=2 THEN 'Edit' WHEN m.tintWFTriggerOn=3 THEN 'Create or Edit' WHEN m.tintWFTriggerOn=4 THEN 'Fields Update' WHEN m.tintWFTriggerOn=5 THEN 'Delete' WHEN m.intDays>0 TH
  
--E--N     
--'Date Field' ELSE 'NA' end AS TriggeredOn      
--  FROM dbo.WorkFlowMaster as m       
--  INNER JOIN dbo.WorkFlowQueue AS q ON m.numWFID=q.numWFID       
--  LEFT JOIN dbo.WorkFlowQueueExecution AS e ON e.numWFQueueID=q.numWFQueueID      
--  INNER JOIN dbo.OpportunityMaster o ON q.numRecordID =o.numOppId      
--  JOIN DynamicFormMaster DFM ON m.numFormID=DFM.numFormID AND DFM.tintFlag=3  join #tempTable T on T.numWFID=m.numWFID      
--    
--   WHERE ID > @firstRec and ID < @lastRec AND m.numDomainID=@numDomainID order by ID      
         
   DROP TABLE #tempTable      
        
        
        
        
        
        
        
        
        
        
     
    
      
      
END 
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetOrderSelectedItemDetail')
DROP PROCEDURE USP_GetOrderSelectedItemDetail
GO
CREATE PROCEDURE [dbo].[USP_GetOrderSelectedItemDetail]
	@numDomainID NUMERIC(18,0),
	@numDivisionID NUMERIC(18,0),
	@tintOppType AS TINYINT,
	@numItemCode NUMERIC(18,0),
	@numUOMID NUMERIC(18,0),
	@numWarehouseItemID AS NUMERIC(18,0),
	@numQty INT,
	@monPrice FLOAT,
	@numOppID NUMERIC(18,0),
	@numOppItemID NUMERIC(18,0),
	@vcSelectedKitChildItems VARCHAR(MAX),
	@numShippingCountry AS NUMERIC(18,0),
	@numWarehouseID AS NUMERIC(18,0),
	@vcCoupon AS VARCHAR(200)
AS
BEGIN
	DECLARE @vcItemName AS VARCHAR(500)
	DECLARE @bitDropship AS BIT
	DECLARE @bitMatrix AS BIT
	DECLARE @chrItemType AS CHAR(1)
	DECLARE @numItemGroup AS NUMERIC(18,0)
	DECLARE @numBaseUnit AS NUMERIC(18,0)
	DECLARE @numSaleUnit AS NUMERIC(18,0)
	DECLARE @fltUOMConversionFactor AS FLOAT
	DECLARE @numPurchaseUnit AS NUMERIC(18,0)
	DECLARE @bitAssemblyOrKit BIT
	DECLARE @bitAssembly AS BIT
	DECLARE @bitCalAmtBasedonDepItems AS BIT
	DECLARE @ItemDesc AS VARCHAR(1000)
	DECLARE @numItemClassification AS NUMERIC(18,0)

	DECLARE @monListPrice AS MONEY = 0.0000
	DECLARE @monVendorCost AS MONEY = 0.0000
	DECLARE @tintPricingBasedOn AS TINYINT = 0.0000

	DECLARE @tintRuleType AS TINYINT
	DECLARE @monPriceLevelPrice AS MONEY = 0.0000
	DECLARE @monPriceFinalPrice AS MONEY = 0.0000
	DECLARE @monPriceRulePrice AS MONEY = 0.0000
	DECLARE @monPriceRuleFinalPrice AS MONEY = 0.0000
	DECLARE @monLastPrice AS MONEY = 0.0000
	DECLARE @dtLastOrderDate VARCHAR(200) = ''

	DECLARE @tintDisountType TINYINT
	DECLARE @decDiscount AS FLOAT

	DECLARE @numVendorID AS NUMERIC(18,0)
	DECLARE @vcMinOrderQty AS VARCHAR(5) = '-'
	DECLARE @vcVendorNotes AS VARCHAR(300) = ''
	DECLARE @numRelationship NUMERIC(18,0)
	DECLARE @numProfile NUMERIC(18,0)
	DECLARE @tintPriceLevel AS TINYINT
	DECLARE @tintDefaultSalesPricing AS TINYINT
	DECLARE @tintPriceBookDiscount AS TINYINT

	IF ISNULL(@numDivisionID,0) > 0
	BEGIN
		SELECT @vcMinOrderQty=ISNULL(CAST(intMinQty AS VARCHAR),'-'),@vcVendorNotes=ISNULL(vcNotes,'') FROM Vendor WHERE numVendorID=@numDivisionID AND numItemCode=@numItemCode
	END

	SELECT @tintDefaultSalesPricing = numDefaultSalesPricing,@tintPriceBookDiscount=tintPriceBookDiscount FROM Domain WHERE numDomainId=@numDomainID

	IF ISNULL(@numWarehouseID,0) > 0
	BEGIN
		SELECT TOP 1 @numWarehouseItemID=numWareHouseItemID FROM WareHouseItems WHERE numItemID=@numItemCode AND numWareHouseID=@numWarehouseID ORDER BY numWareHouseItemID ASC
	END

	-- GET ORGANIZATION DETAL
	SELECT 
		@numRelationship=numCompanyType,
		@numProfile=vcProfile,
		@tintPriceLevel = ISNULL(D.tintPriceLevel,0)
	FROM 
		DivisionMaster D                  
	JOIN 
		CompanyInfo C 
	ON 
		C.numCompanyId=D.numCompanyID                  
	WHERE 
		numDivisionID =@numDivisionID       

	-- GET ITEM DETAILS
	SELECT
		@vcItemName = vcItemName,
		@bitDropship = bitAllowDropShip,
		@ItemDesc = Item.txtItemDesc,
		@bitMatrix = ISNULL(Item.bitMatrix,0),
		@chrItemType = charItemType,
		@numItemGroup = numItemGroup,
		@bitAssembly = ISNULL(bitAssembly,0),
		@numBaseUnit = ISNULL(numBaseUnit,0),
		@numSaleUnit = (CASE WHEN ISNULL(@numUOMID,0) > 0 THEN @numUOMID ELSE ISNULL(numSaleUnit,0) END),
		@numPurchaseUnit = (CASE WHEN ISNULL(@numUOMID,0) > 0 THEN @numUOMID ELSE ISNULL(numPurchaseUnit,0) END),
		@numItemClassification = numItemClassification,
		@bitAssemblyOrKit = (CASE WHEN ISNULL(bitKitParent,0)=1 OR ISNULL(bitAssembly,0)=1 THEN 1 ELSE 0 END),
		@monListPrice = (CASE WHEN Item.charItemType='P' THEN ISNULL(WareHouseItems.monWListPrice,0) ELSE ISNULL(Item.monListPrice,0) END),
		@monVendorCost = ISNULL((SELECT monCost FROM Vendor WHERE numVendorID=Item.numVendorID AND Vendor.numItemCode=Item.numItemCode),0),
		@numVendorID = ISNULL(Item.numVendorID,0),
		@numWarehouseItemID = (CASE WHEN ISNULL(@numWarehouseItemID,0) > 0 THEN @numWarehouseItemID ELSE WareHouseItems.numWareHouseItemID END),
		@bitCalAmtBasedonDepItems = ISNULL(bitCalAmtBasedonDepItems,0)
	FROM
		Item
	OUTER APPLY
	(
		SELECT TOP 1 numWareHouseItemID,monWListPrice FROM WareHouseItems WHERE numItemID=@numItemCode AND (numWareHouseItemID = @numWarehouseItemID OR ISNULL(@numWarehouseItemID,0) = 0)
	) AS WareHouseItems
	WHERE
		Item.numItemCode = @numItemCode

	IF ISNULL(@numDivisionID,0) > 0 AND EXISTS (SELECT * FROM Vendor WHERE numItemCode=@numItemCode AND numVendorID=@numDivisionID)
	BEGIN
		SELECT @monVendorCost=ISNULL(monCost,0) FROM Vendor WHERE numVendorID=@numDivisionID AND numItemCode=@numItemCode
	END


	IF @chrItemType = 'P' AND ISNULL(@numWarehouseItemID,0) = 0
	BEGIN
		RAISERROR('WAREHOUSE_DOES_NOT_EXISTS',16,1)
	END

	SET @fltUOMConversionFactor = dbo.fn_UOMConversion((CASE WHEN  @tintOppType = 1 THEN @numSaleUnit ELSE @numPurchaseUnit END),@numItemCode,@numDomainId,@numBaseUnit)

	IF @tintOppType = 1
	BEGIN
		/*Use Calculated Price When item is Assembly/Kit,Calculate price based on sum of dependent items.*/
		IF @bitCalAmtBasedonDepItems = 1 
		BEGIN
			PRINT 'IN DEPENDED ITEM'

			CREATE TABLE #TEMPSelectedKitChilds
			(
				ChildKitItemID NUMERIC(18,0),
				ChildKitWarehouseItemID NUMERIC(18,0),
				ChildKitChildItemID NUMERIC(18,0),
				ChildKitChildWarehouseItemID NUMERIC(18,0)
			)

			IF ISNULL(@numOppItemID,0) > 0
			BEGIN
				INSERT INTO 
					#TEMPSelectedKitChilds
				SELECT 
					OKI.numChildItemID,
					OKI.numWareHouseItemId,
					OKCI.numItemID,
					OKCI.numWareHouseItemId
				FROM
					OpportunityKitItems OKI
				INNER JOIN
					OpportunityKitChildItems OKCI
				ON
					OKI.numOppChildItemID = OKCI.numOppChildItemID
				WHERE
					OKI.numOppId = @numOppID
					AND OKI.numOppItemID = @numOppItemID
			END
			ELSE IF ISNULL(@vcSelectedKitChildItems,'') <> ''
			BEGIN
				INSERT INTO 
					#TEMPSelectedKitChilds
				SELECT 
					SUBSTRING
					(
						SUBSTRING(OutParam,0,CHARINDEX('-',OutParam)),
						0,
						CHARINDEX('#', SUBSTRING(OutParam,0,CHARINDEX('-',OutParam)))
					) AS numChildKitID,
					SUBSTRING
					(
						SUBSTRING(OutParam,0,CHARINDEX('-',OutParam)),
						CHARINDEX('#', SUBSTRING(OutParam,0,CHARINDEX('-',OutParam))) + 1,
						LEN(SUBSTRING(OutParam,0,CHARINDEX('-',OutParam)))
					) AS numChildKitWarehouseItemID,
					SUBSTRING
					(
						SUBSTRING(OutParam,CHARINDEX('-',OutParam) + 1, LEN(OutParam)),
						0,
						CHARINDEX('#', SUBSTRING(OutParam,CHARINDEX('-',OutParam) + 1, LEN(OutParam)))
					) AS numChildKitID,
					SUBSTRING
					(
						SUBSTRING(OutParam,CHARINDEX('-',OutParam) + 1, LEN(OutParam)),
						CHARINDEX('#', SUBSTRING(OutParam,CHARINDEX('-',OutParam) + 1, LEN(OutParam))) + 1,
						LEN(SUBSTRING(OutParam,CHARINDEX('-',OutParam) + 1, LEN(OutParam)))
					) AS numChildKitWarehouseItemID
		
				FROM 
					dbo.SplitString(@vcSelectedKitChildItems,',')
			END

			;WITH CTE (numItemCode, vcItemName, bitKitParent, numWarehouseItemID,numQtyItemsReq,numUOMId) AS
			(
				SELECT
					ID.numChildItemID,
					I.vcItemName,
					ISNULL(I.bitKitParent,0),
					ISNULL(ID.numWarehouseItemId,0),
					CAST((ISNULL(ID.numQtyItemsReq,0) * ISNULL(dbo.fn_UOMConversion(ISNULL(ID.numUOMID, 0), I.numItemCode, @numDomainID, ISNULL(I.numBaseUnit, 0)),1)) AS FLOAT),
					ISNULL(numUOMId,0)
				FROM 
					[ItemDetails] ID
				INNER JOIN 
					[Item] I 
				ON 
					ID.[numChildItemID] = I.[numItemCode]
				WHERE   
					[numItemKitID] = @numItemCode
				UNION ALL
				SELECT
					ID.numChildItemID,
					I.vcItemName,
					ISNULL(I.bitKitParent,0),
					ISNULL(ID.numWarehouseItemId,0),
					CAST((ISNULL(Temp1.numQtyItemsReq,0) * ISNULL(ID.numQtyItemsReq,0) * ISNULL(dbo.fn_UOMConversion(ISNULL(ID.numUOMID, 0), I.numItemCode, @numDomainID, ISNULL(I.numBaseUnit, 0)),1)) AS FLOAT),
					ISNULL(ID.numUOMId,0)
				FROM 
					CTE As Temp1
				INNER JOIN
					[ItemDetails] ID
				ON
					ID.numItemKitID = Temp1.numItemCode
				INNER JOIN 
					[Item] I 
				ON 
					ID.[numChildItemID] = I.[numItemCode]
				INNER JOIN
					#TEMPSelectedKitChilds
				ON
					ISNULL(Temp1.bitKitParent,0) = 1
					AND ID.numChildItemID = #TEMPSelectedKitChilds.ChildKitChildItemID
					AND ID.numWareHouseItemId = #TEMPSelectedKitChilds.ChildKitChildWarehouseItemID
			)

			SELECT  
					@monListPrice = ISNULL(  SUM(ISNULL(CASE WHEN I.[charItemType]='P' 
					THEN WI.[monWListPrice] 
					ELSE monListPrice END,0) * ID.[numQtyItemsReq] )  ,0)
			FROM    CTE ID
					INNER JOIN [Item] I ON ID.numItemCode = I.[numItemCode]
					left join  WareHouseItems WI
					on WI.numItemID=I.numItemCode and WI.[numWareHouseItemID]=ID.numWarehouseItemID
			WHERE
				ISNULL(ID.bitKitParent,0) = 0

			DROP TABLE #TEMPSelectedKitChilds
		END

		IF @tintDefaultSalesPricing = 1 AND (SELECT COUNT(*) FROM PricingTable WHERE numItemCode=@numItemCode AND ISNULL(numPriceRuleID,0) = 0) > 0 -- PRICE LEVEL
		BEGIN
			SELECT
				@tintDisountType = tintDiscountType,
				@decDiscount = ISNULL(decDiscount,0),
				@tintRuleType = tintRuleType,
				@monPriceLevelPrice = (CASE tintRuleType WHEN 1 THEN @monListPrice WHEN 2 THEN @monVendorCost ELSE ISNULL(decDiscount,0) END),
				@monPriceFinalPrice = (CASE tintRuleType
										WHEN 1 -- Deduct From List Price
										THEN
											CASE tintDiscountType 
												WHEN 1 -- PERCENT
												THEN (CASE WHEN @monListPrice > 0 THEN @monListPrice - (@monListPrice * (ISNULL(decDiscount,0)/100)) ELSE 0 END)
												WHEN 2 -- FLAT AMOUNT
												THEN (CASE WHEN @monListPrice > 0 THEN (CASE WHEN (@monListPrice * @numQty * @fltUOMConversionFactor) - ISNULL(decDiscount,0) > 0 THEN ((@monListPrice * @numQty * @fltUOMConversionFactor) - ISNULL(decDiscount,0)) / (@numQty * @fltUOMConversionFactor) ELSE 0 END) ELSE 0 END)
												WHEN 3 -- NAMED PRICE
												THEN ISNULL(decDiscount,0)
											END
										WHEN 2 -- Add to primary vendor cost
										THEN
											CASE tintDiscountType 
												WHEN 1  -- PERCENT
												THEN @monVendorCost + (@monVendorCost * (ISNULL(decDiscount,0)/100))  * @fltUOMConversionFactor
												WHEN 2 -- FLAT AMOUNT
												THEN (CASE WHEN (@monVendorCost * @numQty * @fltUOMConversionFactor) + ISNULL(decDiscount,0) > 0 THEN ((@monVendorCost * @numQty * @fltUOMConversionFactor) + ISNULL(decDiscount,0)) / (@numQty * @fltUOMConversionFactor)  ELSE 0 END)
												WHEN 3 -- NAMED PRICE
												THEN ISNULL(decDiscount,0)
											END
										WHEN 3 -- Named price
										THEN
											ISNULL(decDiscount,0)
										END
									) 
			FROM
			(
				SELECT 
					ROW_NUMBER() OVER(PARTITION BY PricingTable.numItemCode ORDER BY numPricingID) Id,
					*
				FROM
					PricingTable
				WHERE 
					PricingTable.numItemCode = @numItemCode
					
			) TEMP
			WHERE
				numItemCode=@numItemCode AND ((tintRuleType = 3 AND Id=@tintPriceLevel) OR @numQty * @fltUOMConversionFactor >= intFromQty AND @numQty * @fltUOMConversionFactor <= intToQty)
		END
		ELSE IF @tintDefaultSalesPricing = 2 AND (SELECT COUNT(*) FROM PriceBookRules WHERE numDomainID=@numDomainID AND tintRuleFor=@tintOppType) > 0 -- PRICE RULE
		BEGIN
			DECLARE @numPriceRuleID NUMERIC(18,0)
			DECLARE @tintPriceRuleType AS TINYINT
			DECLARE @tintPricingMethod TINYINT
			DECLARE @tintPriceBookDiscountType TINYINT
			DECLARE @decPriceBookDiscount FLOAT
			DECLARE @intQntyItems INT
			DECLARE @decMaxDedPerAmt FLOAT

			SELECT 
				TOP 1
				@numPriceRuleID = numPricRuleID,
				@tintPricingMethod = tintPricingMethod,
				@tintPriceRuleType = p.tintRuleType,
				@tintPriceBookDiscountType = P.[tintDiscountType],
				@decPriceBookDiscount = P.[decDiscount],
				@intQntyItems = P.[intQntyItems],
				@decMaxDedPerAmt = P.[decMaxDedPerAmt]
			FROM   
				PriceBookRules P 
			LEFT JOIN 
				PriceBookRuleDTL PDTL 
			ON 
				P.numPricRuleID = PDTL.numRuleID
			LEFT JOIN 
				PriceBookRuleItems PBI 
			ON 
				P.numPricRuleID = PBI.numRuleID
			LEFT JOIN 
				[PriceBookPriorities] PP 
			ON 
				PP.[Step2Value] = P.[tintStep2] 
				AND PP.[Step3Value] = P.[tintStep3]
			WHERE  
				P.numDomainID=@numDomainID 
				AND tintRuleFor=@tintOppType
				AND (
						((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 1 AND PDTL.numValue = @numDivisionID)) -- Priority 1
						OR ((tintStep2 = 2 AND PBI.numValue = @numItemClassification) AND (tintStep3= 1 AND PDTL.numValue = @numDivisionID)) -- Priority 2
						OR ((tintStep2 = 3) AND (tintStep3 = 1 AND PDTL.numValue = @numDivisionID)) -- Priority 3

						OR ((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 4
						OR ((tintStep2 = 2 AND PBI.numValue = @numItemClassification) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 5
						OR ((tintStep2 = 3) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 6

						OR ((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 3)) -- Priority 7
						OR ((tintStep2 = 2 AND PBI.numValue = @numItemClassification) AND (tintStep3 = 3)) -- Priority 8
						OR ((tintStep2 = 3) and (tintStep3 = 3)) -- Priority 9
					)
			ORDER BY 
				PP.Priority ASC


			IF ISNULL(@numPriceRuleID,0) > 0
			BEGIN
				IF @tintPricingMethod = 1 -- BASED ON PRICE TABLE
				BEGIN
					SELECT
						@tintDisountType = tintDiscountType,
						@decDiscount = ISNULL(decDiscount,0),
						@tintRuleType = tintRuleType,
						@monPriceRulePrice = (CASE tintRuleType WHEN 1 THEN @monListPrice WHEN 2 THEN @monVendorCost END) ,
						@monPriceRuleFinalPrice = (CASE tintRuleType
												WHEN 1 -- Deduct From List Price
												THEN
													CASE tintDiscountType 
														WHEN 1 -- PERCENT
														THEN (CASE WHEN @monListPrice > 0 THEN @monListPrice - (@monListPrice * (ISNULL(decDiscount,0)/100)) ELSE 0 END)
														WHEN 2 -- FLAT AMOUNT
														THEN (CASE WHEN @monListPrice > 0 THEN (CASE WHEN (@monListPrice * @numQty * @fltUOMConversionFactor) - ISNULL(decDiscount,0) > 0 THEN ((@monListPrice * @numQty * @fltUOMConversionFactor) - ISNULL(decDiscount,0)) / (@numQty * @fltUOMConversionFactor) ELSE 0 END) ELSE 0 END)
														WHEN 3 -- NAMED PRICE
														THEN ISNULL(decDiscount,0)
													END
												WHEN 2 -- Add to primary vendor cost
												THEN
													CASE tintDiscountType 
														WHEN 1  -- PERCENT
														THEN @monVendorCost + (@monVendorCost * (ISNULL(decDiscount,0)/100))
														WHEN 2 -- FLAT AMOUNT
														THEN (CASE WHEN (@monVendorCost * @numQty * @fltUOMConversionFactor) + ISNULL(decDiscount,0) > 0 THEN ((@monVendorCost * @numQty * @fltUOMConversionFactor) + ISNULL(decDiscount,0)) / (@numQty * @fltUOMConversionFactor)  ELSE 0 END)
														WHEN 3 -- NAMED PRICE
														THEN ISNULL(decDiscount,0)
													END
												WHEN 3 -- Named price
												THEN
													CASE 
													WHEN ISNULL(@tintPriceLevel,0) > 0
													THEN
														(SELECT 
															 ISNULL(decDiscount,0)
														FROM
														(
															SELECT 
																ROW_NUMBER() OVER(PARTITION BY PricingTable.numItemCode ORDER BY numPricingID) Id,
																decDiscount
															FROM 
																PricingTable 
															WHERE 
																PricingTable.numItemCode = @numItemCode 
																AND tintRuleType = 3 
														) TEMP
														WHERE
															Id = @tintPriceLevel)
													ELSE
														ISNULL(decDiscount,0)
													END
												END
											) 
					FROM
						PricingTable
					WHERE
						numPriceRuleID=@numPriceRuleID AND @numQty * @fltUOMConversionFactor >= intFromQty AND @numQty * @fltUOMConversionFactor <= intToQty
				END
				ELSE IF @tintPricingMethod = 2 -- BASED ON RULE
				BEGIN
					SET @tintRuleType = @tintPriceRuleType
					SET @monPriceRulePrice = (CASE @tintPriceRuleType WHEN 1 THEN @monListPrice WHEN 2 THEN @monVendorCost END) 

					IF (@tintPriceBookDiscountType = 1 ) -- Percentage 
					BEGIN
						SET @tintDisountType = @tintPriceBookDiscountType
						SET @decDiscount = @decDiscount * (@numQty/@intQntyItems);
						SET @decDiscount = @decDiscount * (CASE WHEN @tintPriceRuleType = 2 THEN @monVendorCost ELSE @monListPrice END) /100;
						SET @decMaxDedPerAmt = @decMaxDedPerAmt * (CASE WHEN @tintPriceRuleType = 2 THEN @monVendorCost ELSE @monListPrice END) /100;
					
						IF (@decDiscount > @decMaxDedPerAmt)
							SET @decDiscount = @decMaxDedPerAmt;
				
						IF ( @tintPriceRuleType = 1 )
								SET @monPriceRuleFinalPrice = (@monListPrice - @decDiscount);
						IF ( @tintPriceRuleType = 2 )
								SET @monPriceRuleFinalPrice = (@monVendorCost + @decDiscount);
					END
					IF ( @tintPriceBookDiscountType = 2 ) -- Flat discount 
					BEGIN
						SET @decDiscount = @decDiscount * ((@numQty * @fltUOMConversionFactor)/@intQntyItems);

						IF (@decDiscount > @decMaxDedPerAmt)
							SET @decDiscount = @decMaxDedPerAmt;
					
						IF ( @tintPriceRuleType = 1 )
							SET @monPriceRuleFinalPrice =  ((@monListPrice * @numQty * @fltUOMConversionFactor) - @decDiscount)/(@numQty * @fltUOMConversionFactor);
						IF ( @tintPriceRuleType = 2 )
							SET @monPriceRuleFinalPrice = ((@monVendorCost * @numQty * @fltUOMConversionFactor) + @decDiscount)/(@numQty * @fltUOMConversionFactor);
					END
				END
			END
		
		END
	
		-- GET Last Price
		DECLARE @LastDiscountType TINYINT
		DECLARE @LastDiscount FLOAT

		SELECT 
			TOP 1 
			@monLastPrice = monPrice
			,@dtLastOrderDate = dbo.FormatedDateFromDate(OpportunityMaster.bintCreatedDate,@numDomainID)
			,@LastDiscountType = ISNULL(OpportunityItems.bitDiscountType,1)
			,@LastDiscount = ISNULL(OpportunityItems.fltDiscount,0)
		FROM 
			OpportunityItems 
		JOIN 
			OpportunityMaster 
		ON 
			OpportunityItems.numOppId = OpportunityMaster.numOppId 
		WHERE 
			OpportunityMaster.numDomainId= @numDomainID
			AND tintOppType = @tintOppType
			AND tintOppStatus = 1
			AND numItemCode=@numItemCode
		ORDER BY
			OpportunityMaster.numOppId DESC

		IF @tintDefaultSalesPricing = 1 AND ISNULL(@monPriceFinalPrice,0) > 0
		BEGIN
			-- WE ARE NOT DISPLAYING DISCOUNT SEPERATELY IN PRICE LEVEL
			SET @tintPricingBasedOn = 1
			SET @monPrice = @monPriceFinalPrice
			SET @tintDisountType = 1
			SET @decDiscount = 0
		END
		ELSE IF @tintDefaultSalesPricing = 2 AND ISNULL(@monPriceRuleFinalPrice,0) > 0
		BEGIN
			SET @tintPricingBasedOn = 2
		
			IF @tintPriceBookDiscountType = 1 -- Display Disocunt Price As Unit Price
			BEGIN
				SET @monPrice = @monPriceRuleFinalPrice
				SET @tintDisountType = 1
				SET @decDiscount = 0
			END
			ELSE -- Display Unit Price And Disocunt Price Seperately
			BEGIN
				SET @monPrice = @monPriceRulePrice
			END
		
		END
		ELSE IF @tintDefaultSalesPricing = 3 AND ISNULL(@monLastPrice,0) > 0
		BEGIN
			SET @tintPricingBasedOn = 3
			SET @monPrice = @monLastPrice
			SET @tintDisountType = @LastDiscountType
			SET @decDiscount = @LastDiscount
		END
		ELSE
		BEGIN
			SET @tintPricingBasedOn = 0
			SET @tintDisountType = 1
			SET @decDiscount = 0
			SET @tintRuleType = 0

			IF @tintOppType = 1
				SET @monPrice = @monListPrice
			ELSE
				SET @monPrice = @monVendorCost
		END
	END
	ELSE IF @tintOppType = 2
	BEGIN
		SELECT 
			TOP 1
			@numPriceRuleID = numPricRuleID,
			@tintPricingMethod = tintPricingMethod,
			@tintPriceRuleType = p.tintRuleType,
			@tintPriceBookDiscountType = P.[tintDiscountType],
			@decPriceBookDiscount = P.[decDiscount],
			@intQntyItems = P.[intQntyItems],
			@decMaxDedPerAmt = P.[decMaxDedPerAmt]
		FROM   
			PriceBookRules P 
		LEFT JOIN 
			PriceBookRuleDTL PDTL 
		ON 
			P.numPricRuleID = PDTL.numRuleID
		LEFT JOIN 
			PriceBookRuleItems PBI 
		ON 
			P.numPricRuleID = PBI.numRuleID
		LEFT JOIN 
			[PriceBookPriorities] PP 
		ON 
			PP.[Step2Value] = P.[tintStep2] 
			AND PP.[Step3Value] = P.[tintStep3]
		WHERE  
			P.numDomainID=@numDomainID 
			AND tintRuleFor=@tintOppType
			AND (
					((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 1 AND PDTL.numValue = @numDivisionID)) -- Priority 1
					OR ((tintStep2 = 2 AND PBI.numValue = @numItemClassification) AND (tintStep3= 1 AND PDTL.numValue = @numDivisionID)) -- Priority 2
					OR ((tintStep2 = 3) AND (tintStep3 = 1 AND PDTL.numValue = @numDivisionID)) -- Priority 3

					OR ((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 4
					OR ((tintStep2 = 2 AND PBI.numValue = @numItemClassification) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 5
					OR ((tintStep2 = 3) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 6

					OR ((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 3)) -- Priority 7
					OR ((tintStep2 = 2 AND PBI.numValue = @numItemClassification) AND (tintStep3 = 3)) -- Priority 8
					OR ((tintStep2 = 3) and (tintStep3 = 3)) -- Priority 9
				)
		ORDER BY 
			PP.Priority ASC


		IF ISNULL(@numPriceRuleID,0) > 0
		BEGIN
			IF @tintPricingMethod = 1 -- BASED ON PRICE TABLE
			BEGIN
				SELECT
					@tintDisountType = tintDiscountType,
					@decDiscount = ISNULL(decDiscount,0),
					@tintRuleType = tintRuleType,
					@monPriceRulePrice = (CASE tintRuleType WHEN 1 THEN @monListPrice WHEN 2 THEN @monVendorCost END) ,
					@monPriceRuleFinalPrice = (CASE tintRuleType
											WHEN 1 -- Deduct From List Price
											THEN
												CASE tintDiscountType 
													WHEN 1 -- PERCENT
													THEN (CASE WHEN @monListPrice > 0 THEN @monListPrice - (@monListPrice * (ISNULL(decDiscount,0)/100)) ELSE 0 END)
													WHEN 2 -- FLAT AMOUNT
													THEN (CASE WHEN @monListPrice > 0 THEN (CASE WHEN (@monListPrice * @numQty * @fltUOMConversionFactor) - ISNULL(decDiscount,0) > 0 THEN ((@monListPrice * @numQty * @fltUOMConversionFactor) - ISNULL(decDiscount,0)) / (@numQty * @fltUOMConversionFactor) ELSE 0 END) ELSE 0 END)
													WHEN 3 -- NAMED PRICE
													THEN ISNULL(decDiscount,0)
												END
											WHEN 2 -- Add to primary vendor cost
											THEN
												CASE tintDiscountType 
													WHEN 1  -- PERCENT
													THEN @monVendorCost + (@monVendorCost * (ISNULL(decDiscount,0)/100))
													WHEN 2 -- FLAT AMOUNT
													THEN (CASE WHEN (@monVendorCost * @numQty * @fltUOMConversionFactor) + ISNULL(decDiscount,0) > 0 THEN ((@monVendorCost * @numQty * @fltUOMConversionFactor) + ISNULL(decDiscount,0)) / (@numQty * @fltUOMConversionFactor)  ELSE 0 END)
													WHEN 3 -- NAMED PRICE
													THEN ISNULL(decDiscount,0)
												END
											WHEN 3 -- Named price
											THEN
												CASE 
												WHEN ISNULL(@tintPriceLevel,0) > 0
												THEN
													(SELECT 
															ISNULL(decDiscount,0)
													FROM
													(
														SELECT 
															ROW_NUMBER() OVER(PARTITION BY PricingTable.numItemCode ORDER BY numPricingID) Id,
															decDiscount
														FROM 
															PricingTable 
														WHERE 
															PricingTable.numItemCode = @numItemCode 
															AND tintRuleType = 3 
													) TEMP
													WHERE
														Id = @tintPriceLevel)
												ELSE
													ISNULL(decDiscount,0)
												END
											END
										) 
				FROM
					PricingTable
				WHERE
					numPriceRuleID=@numPriceRuleID AND @numQty * @fltUOMConversionFactor >= intFromQty AND @numQty * @fltUOMConversionFactor <= intToQty
			END
			ELSE IF @tintPricingMethod = 2 -- BASED ON RULE
			BEGIN
				SET @tintRuleType = @tintPriceRuleType
				SET @monPriceRulePrice = (CASE @tintPriceRuleType WHEN 1 THEN @monListPrice WHEN 2 THEN @monVendorCost END) 

				IF (@tintPriceBookDiscountType = 1 ) -- Percentage 
				BEGIN
					SET @tintDisountType = @tintPriceBookDiscountType
					SET @decDiscount = @decDiscount * (@numQty/@intQntyItems);
					SET @decDiscount = @decDiscount * (CASE WHEN @tintPriceRuleType = 2 THEN @monVendorCost ELSE @monListPrice END) /100;
					SET @decMaxDedPerAmt = @decMaxDedPerAmt * (CASE WHEN @tintPriceRuleType = 2 THEN @monVendorCost ELSE @monListPrice END) /100;
					
					IF (@decDiscount > @decMaxDedPerAmt)
						SET @decDiscount = @decMaxDedPerAmt;
				
					IF ( @tintPriceRuleType = 1 )
							SET @monPriceRuleFinalPrice = (@monListPrice - @decDiscount);
					IF ( @tintPriceRuleType = 2 )
							SET @monPriceRuleFinalPrice = (@monVendorCost + @decDiscount);
				END
				IF ( @tintPriceBookDiscountType = 2 ) -- Flat discount 
				BEGIN
					SET @decDiscount = @decDiscount * ((@numQty * @fltUOMConversionFactor)/@intQntyItems);

					IF (@decDiscount > @decMaxDedPerAmt)
						SET @decDiscount = @decMaxDedPerAmt;
					
					IF ( @tintPriceRuleType = 1 )
						SET @monPriceRuleFinalPrice =  ((@monListPrice * @numQty * @fltUOMConversionFactor) - @decDiscount)/(@numQty * @fltUOMConversionFactor);
					IF ( @tintPriceRuleType = 2 )
						SET @monPriceRuleFinalPrice = ((@monVendorCost * @numQty * @fltUOMConversionFactor) + @decDiscount)/(@numQty * @fltUOMConversionFactor);
				END
			END
		END

		If @monPriceRuleFinalPrice > 0
		BEGIN
			SET @monPrice = @monPriceRuleFinalPrice
		END
		ELSE
		BEGIN
			SET @monPrice = @monVendorCost
		END


		SET @monPriceLevelPrice = 0
		SET @monPriceFinalPrice = 0
		SET @monPriceRulePrice = 0
		SET @monPriceRuleFinalPrice = 0
		SET @monLastPrice = 0
		SET @dtLastOrderDate = NULL
		SET @tintDisountType = 1
		SET @decDiscount =0 
	END


	

	SELECT
		@vcItemName AS vcItemName,
		@bitDropship AS bitDropship,
		@chrItemType AS ItemType,
		@bitMatrix AS bitMatrix,
		@numItemClassification AS ItemClassification,
		@numItemGroup AS ItemGroup,
		@ItemDesc AS ItemDescription,
		@bitAssembly AS bitAssembly,
		case when ISNULL(@bitAssembly,0)=1 then dbo.fn_GetAssemblyPossibleWOQty(@numItemCode) else 0 end AS MaxWorkOrderQty,
		@tintPricingBasedOn AS tintPriceBaseOn,
		@monPrice AS monPrice,
		@fltUOMConversionFactor AS UOMConversionFactor,
		(CASE WHEN @tintOppType=1 THEN @monListPrice ELSE @monVendorCost END) AS monListPrice,
		@monVendorCost AS VendorCost,
		@numVendorID AS numVendorID,
		@tintRuleType AS tintRuleType,
		@monPriceLevelPrice AS monPriceLevel,
		@monPriceFinalPrice AS monPriceLevelFinalPrice,
		@monPriceRulePrice AS monPriceRule,
		@monPriceRuleFinalPrice AS monPriceRuleFinalPrice,
		@monLastPrice AS monLastPrice,
		@dtLastOrderDate AS LastOrderDate,
		@tintDisountType AS DiscountType,
		@decDiscount AS Discount,
		@numBaseUnit AS numBaseUnit,
		@numSaleUnit AS numSaleUnit,
		@numPurchaseUnit AS numPurchaseUnit,
		dbo.fn_GetUOMName(@numBaseUnit) AS vcBaseUOMName,
		dbo.fn_GetUOMName(@numSaleUnit) AS vcSaleUOMName,
		@numPurchaseUnit AS numPurchaseUnit,
		@numWarehouseItemID AS numWarehouseItemID,
		dbo.fn_GetItemTransitCount(@numItemcode,@numDomainID) AS OrdersInTransist,
		(SELECT COUNT(*) FROM SimilarItems SI INNER JOIN Item I ON I.numItemCode = SI.numItemCode WHERE SI.numDomainId = @numDomainID AND SI.numParentItemCode = @numItemCode) AS RelatedItemsCount,
		@tintPriceLevel AS tintPriceLevel,
		@vcMinOrderQty AS vcMinOrderQty,
		@vcVendorNotes AS vcVendorNotes


	-- CHECK IF PROMOTION CAN BE TRIGGERED FROM THIS ITEM
	DECLARE @numPromotionID AS NUMERIC(18,0)

	SELECT TOP 1
		@numPromotionID = numProId 
	FROM 
		PromotionOffer PO
	WHERE 
		numDomainId=@numDomainID 
		AND ISNULL(bitEnabled,0)=1
		AND ISNULL(bitApplyToInternalOrders,0)=1 
		AND 1 = (CASE WHEN bitRequireCouponCode = 1 THEN CASE WHEN ISNULL(intCouponCodeUsed,0) < ISNULL(tintUsageLimit,0) OR ISNULL(tintUsageLimit,0) = 0  THEN 1 ELSE 0 END ELSE 1 END)
		AND 1 = (CASE WHEN bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN CAST(GETDATE() AS DATE)>=dtValidFrom AND CAST(GETDATE() AS DATE)<=dtValidTo THEN 1 ELSE 0 END) END)
		AND 1 = (CASE 
					WHEN tintOfferBasedOn = 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=1 AND numValue=@numItemCode) > 0 THEN 1 ELSE 0 END)
					WHEN tintOfferBasedOn = 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=2 AND numValue=@numItemClassification) > 0 THEN 1 ELSE 0 END)
					ELSE 0
				END)
	ORDER BY
		CASE 
			WHEN ISNULL(PO.bitRequireCouponCode,0)=1 AND PO.txtCouponCode=@vcCoupon THEN 1
			WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 1 THEN 2
			WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 1 THEN 3
			WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 2 THEN 4
			WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 2 THEN 5
			WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 3 THEN 6
			WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 3 THEN 7
		END


	IF ISNULL(@numPromotionID,0) > 0
	BEGIN
		-- Get Top 1 Promotion Based on priority

		SELECT 
			numProId
			,vcProName
			,bitNeverExpires
			,bitRequireCouponCode
			,txtCouponCode
			,tintUsageLimit
			,intCouponCodeUsed
			,bitFreeShiping
			,monFreeShippingOrderAmount
			,numFreeShippingCountry
			,bitFixShipping1
			,monFixShipping1OrderAmount
			,monFixShipping1Charge
			,bitFixShipping2
			,monFixShipping2OrderAmount
			,monFixShipping2Charge
			,tintOfferTriggerValueType
			,fltOfferTriggerValue
			,tintOfferBasedOn
			,fltDiscountValue
			,tintDiscountType
			,tintDiscoutBaseOn
			,dbo.FormatedDateFromDate(dtValidTo,@numDomainID) AS dtExpire
			,(CASE tintOfferBasedOn
				WHEN 1 THEN (SELECT Stuff((SELECT CONCAT(',',numValue) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=1 FOR XML PATH('')), 1, 1, ''))
				WHEN 2 THEN (SELECT Stuff((SELECT CONCAT(',',numValue) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=2 FOR XML PATH('')), 1, 1, ''))
			END) As vcPromotionItems
			,(CASE tintDiscoutBaseOn
				WHEN 1 THEN (SELECT Stuff((SELECT CONCAT(',',numValue) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=6 AND tintType=1 FOR XML PATH('')), 1, 1, ''))
				WHEN 2 THEN (SELECT Stuff((SELECT CONCAT(',',numValue) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=6 AND tintType=2 FOR XML PATH('')), 1, 1, ''))
				WHEN 3 THEN (SELECT Stuff((SELECT CONCAT(',',SimilarItems.numItemCode) FROM SimilarItems WHERE 1 = (CASE 
																														WHEN tintOfferBasedOn=1 THEN CASE WHEN numParentItemCode IN (SELECT numValue FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=1) THEN 1 ELSE 0 END
																														WHEN tintOfferBasedOn=2 THEN CASE WHEN numParentItemCode IN (SELECT numItemCode FROM Item WHERE numDomainID=@numDomainID AND numItemClassification IN (SELECT numValue FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=2)) THEN 1 ELSE 0 END
																														ELSE 0
																													END) FOR XML PATH('')), 1, 1, ''))
			END) As vcItems
			,CONCAT('Buy '
					,CASE WHEN tintOfferTriggerValueType=1 THEN CAST(fltOfferTriggerValue AS VARCHAR) ELSE CONCAT('$',fltOfferTriggerValue) END
					,CASE tintOfferBasedOn
							WHEN 1 THEN CONCAT(' of "',(SELECT Stuff((SELECT CONCAT(', ',Item.vcItemName) FROM PromotionOfferItems INNER JOIN Item ON PromotionOfferItems.numValue = Item.numItemCode WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=1 FOR XML PATH('')), 1, 2, '')), '"')
							WHEN 2 THEN CONCAT(' of items belonging to classification(s) "',(SELECT Stuff((SELECT CONCAT(', ',ListDetails.vcData) FROM PromotionOfferItems INNER JOIN ListDetails ON PromotionOfferItems.numValue = ListDetails.numListItemID WHERE numListID=36 AND numProId=PO.numProId AND tintRecordType=5 AND tintType=2 FOR XML PATH('')), 1, 2, '')), '"')
						END
					,' & get '
					, CASE tintDiscountType WHEN 1 THEN CONCAT(fltDiscountValue,'% off on') WHEN 2 THEN CONCAT('$',fltDiscountValue, ' off on') WHEN 3 THEN CONCAT(fltDiscountValue,' of') END
					, CASE 
						WHEN tintDiscoutBaseOn = 1 THEN 
							CONCAT
							(
								CONCAT(' "',(SELECT Stuff((SELECT CONCAT(',',Item.vcItemName) FROM PromotionOfferItems INNER JOIN Item ON PromotionOfferItems.numValue = Item.numItemCode WHERE numProId=PO.numProId AND tintRecordType=6 AND tintType=1 FOR XML PATH('')), 1, 1, '')), '"')
								,(CASE WHEN PO.tintDiscountType = 3 THEN ' free' ELSE '' END)
								,'.'
							)
						WHEN tintDiscoutBaseOn = 2 THEN 
							CONCAT
							(
								CONCAT(' items belonging to classification(s) "',(SELECT Stuff((SELECT CONCAT(',',ListDetails.vcData) FROM PromotionOfferItems INNER JOIN ListDetails ON PromotionOfferItems.numValue = ListDetails.numListItemID WHERE numListID=36 AND numProId=PO.numProId AND tintRecordType=6 AND tintType=2 FOR XML PATH('')), 1, 1, '')), '"')
								,(CASE WHEN PO.tintDiscountType = 3 THEN ' free' ELSE '' END)
								,'.'
							)
						WHEN tintDiscoutBaseOn = 3 THEN ' related items.'
						ELSE '' 
					END
				) AS vcPromotionDescription
				,CONCAT
				(
					(CASE WHEN ISNULL(bitFixShipping1,0)=1 THEN CONCAT('Spend $',monFixShipping1OrderAmount,' and get $',monFixShipping1Charge,' shipping. ') ELSE '' END)
					,(CASE WHEN ISNULL(bitFixShipping2,0)=1  THEN CONCAT('Spend $',monFixShipping2OrderAmount,' and get $',monFixShipping2Charge,' shipping. ') ELSE '' END)
					,(CASE WHEN (bitFreeShiping=1 AND numFreeShippingCountry=@numShippingCountry) THEN CONCAT('Spend $',monFreeShippingOrderAmount,' and shipping is FREE! ') ELSE '' END) 
				) AS vcShippingDescription,
				CASE 
					WHEN ISNULL(PO.bitRequireCouponCode,0)=1 AND PO.txtCouponCode=@vcCoupon THEN 1
					WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 1 THEN 2
					WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 1 THEN 3
					WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 2 THEN 4
					WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 2 THEN 5
					WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 3 THEN 6
					WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 3 THEN 7
				END AS tintPriority
		FROM
			PromotionOffer PO
			LEFT JOIN ShippingPromotions SP ON PO.numDomainId = SP.numDomainId
		WHERE
			PO.numDomainId=@numDomainID 
			AND numProId = @numPromotionID
	END
END 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetShippingCharge')
DROP PROCEDURE USP_GetShippingCharge
GO
CREATE PROCEDURE [dbo].[USP_GetShippingCharge]
	@numDomainID NUMERIC(18,0),
	@numShippingCountry AS NUMERIC(18,0),
	@cookieId VARCHAR(MAX)=NULL,
	@numUserCntId numeric=0,
	@monShippingAmount money OUTPUT
AS
BEGIN
	SET @monShippingAmount=-1
	DECLARE @numPromotionID AS NUMERIC(18,0)=0

	DECLARE @decmPrice DECIMAL(18,2)
	SET @decmPrice = (
						SELECT 
							ISNULL(SUM(monTotAmount),0) 
						FROM 
							CartItems 
						WHERE 
							numDomainId=@numDomainID 
							AND vcCookieId=@cookieId 
							AND numUserCntId=@numUserCntId
					)


	SET @numPromotionID= (
							SELECT TOP 1 
								PromotionID 
							FROM 
								CartItems AS C 
							LEFT JOIN 
								PromotionOffer AS P 
							ON 
								C.PromotionID=P.numProId
								LEFT JOIN ShippingPromotions SP ON C.numDomainId = SP.numDomainId
							WHERE
								C.numDomainId=@numDomainID 
								AND C.vcCookieId=@cookieId 
								AND C.numUserCntId=@numUserCntId
								AND (ISNULL(bitFixShipping1,0)=1 OR ISNULL(bitFixShipping2,0)=1 OR ISNULL(bitFreeShiping,0)=1) 
							ORDER BY 
								numCartId 
							)

	IF(@numPromotionID>0)
	BEGIN
		DECLARE @bitFreeShiping BIT
		DECLARE @monFreeShippingOrderAmount money
		DECLARE @numFreeShippingCountry money
		DECLARE @bitFixShipping1 BIT
		DECLARE @monFixShipping1OrderAmount money
		DECLARE @monFixShipping1Charge money
		DECLARE @bitFixShipping2 BIT
		DECLARE @monFixShipping2OrderAmount money
		DECLARE @monFixShipping2Charge money
		SELECT 
			@bitFreeShiping=bitFreeShiping
			,@monFreeShippingOrderAmount=monFreeShippingOrderAmount
			,@numFreeShippingCountry=numFreeShippingCountry
			,@bitFixShipping1=bitFixShipping1
			,@monFixShipping1OrderAmount=monFixShipping1OrderAmount
			,@monFixShipping1Charge=monFixShipping1Charge
			,@bitFixShipping2=bitFixShipping2
			,@monFixShipping2OrderAmount=monFixShipping2OrderAmount
			,@monFixShipping2Charge=monFixShipping2Charge
		FROM
			PromotionOffer PO
			LEFT JOIN ShippingPromotions SP ON PO.numDomainId = SP.numDomainId
		WHERE
			PO.numDomainId=@numDomainID 
			AND numProId = @numPromotionID

		IF(ISNULL(@bitFixShipping1,0)=1 AND @decmPrice > @monFixShipping1OrderAmount)
		BEGIN
			SET @monShippingAmount=@monFixShipping1Charge
		END

		IF(ISNULL(@bitFixShipping2,0)=1 AND @decmPrice > @monFixShipping2OrderAmount)
		BEGIN
			SET @monShippingAmount=@monFixShipping2Charge
		END

		IF((ISNULL(@bitFreeShiping,0)=1 AND @numFreeShippingCountry=@numShippingCountry AND @decmPrice>@monFreeShippingOrderAmount))
		BEGIN
			SET @monShippingAmount=0
		END
	END
	
	SELECT ISNULL(@monShippingAmount,0)
END
--SELECT * FROM Category
--- Alterd By Chintan Prajapati
-- EXEC dbo.USP_ItemsForECommerce 9,60,1,1,15,0,''
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ItemsAvailablityOpenPO' ) 
    DROP PROCEDURE USP_ItemsAvailablityOpenPO
GO
CREATE PROCEDURE [dbo].[USP_ItemsAvailablityOpenPO]
    @numItemCode NUMERIC(18,0)=0,
    @numDomainID NUMERIC(18,0)=0
   
AS 
   BEGIN
		if((select COUNT(W.numOnHand)
			from 
			WarehouseItems  AS W
		LEFT JOIN
			Warehouses AS WH ON WH.numWareHouseId=W.numWareHouseId
		LEFT JOIN
			WarehouseLocation AS WL ON WL.numWLocationID=W.numWLocationID
		WHERE 
			W.numItemID=@numItemCode)=0)
		BEGIN
			SELECT (CASE WHEN @numDomainID=209 THEN 'RFQ' ELSE '0' END) AS numOnHand,0 AS numOnOrder,'NOT AVAILABLE' AS vcWareHouse,NULL AS bintCreatedDate,NULL AS dtDeliveryDate,0 AS LeadDays
		END
		ELSE
		BEGIN
			select distinct
			CASE WHEN ISNULL(W.numOnHand,0) = 0 THEN (CASE WHEN @numDomainID=209 THEN 'RFQ' ELSE '0' END) ELSE CAST(W.numOnHand AS VARCHAR) END numOnHand,
			W.numOnOrder,
			WH.vcWareHouse +' '+ (CASE WHEN W.numWLocationID=-1 THEN 'Global' WHEN W.numWLocationID=0 THEN '' ELSE WL.vcLocation END) AS vcWareHouse,
			(select TOP 1 bintCreatedDate FROM OpportunityMaster where tintShipped=0 AND tintOppType=2 AND numDomainId=@numDomainID AND numOppId IN (SELECT numOppId FROM OpportunityItems where numItemCode=@numItemCode) ORDER BY bintCreatedDate desc) as bintCreatedDate,
			(select TOP 1 dtDeliveryDate FROM OpportunityBizDocs where dtDeliveryDate IS NOT NULL AND numOppId=(select TOP 1 numOppId FROM OpportunityMaster where tintShipped=0 AND tintOppType=2 AND numDomainId=@numDomainID AND numOppId IN (SELECT numOppId FROM OpportunityItems where numItemCode=@numItemCode))) AS dtDeliveryDate,
			ISNULL(DATEDIFF(DAY,CAST((select TOP 1 bintCreatedDate FROM OpportunityMaster where tintShipped=0 AND numDomainId=@numDomainID AND tintOppType=2 AND numOppId IN (SELECT numOppId FROM OpportunityItems where numItemCode=@numItemCode) ORDER BY bintCreatedDate desc) AS DATE),
			CAST((select TOP 1 dtDeliveryDate FROM OpportunityBizDocs where dtDeliveryDate IS NOT NULL AND numOppId=(select TOP 1 numOppId FROM OpportunityMaster where tintShipped=0 AND tintOppType=2 AND numDomainId=@numDomainID AND numOppId IN (SELECT numOppId FROM OpportunityItems where numItemCode=@numItemCode))) AS DATE)),0) AS LeadDays
	from 
			WarehouseItems  AS W
		LEFT JOIN
			Warehouses AS WH ON WH.numWareHouseId=W.numWareHouseId
		LEFT JOIN
			WarehouseLocation AS WL ON WL.numWLocationID=W.numWLocationID
		WHERE 
			W.numItemID=@numItemCode
    END
    END
GO
