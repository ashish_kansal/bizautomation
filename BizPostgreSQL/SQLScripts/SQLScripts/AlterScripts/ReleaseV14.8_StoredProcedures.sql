/******************************************************************
Project: Release 14.8 Date: 26.JAN.2021
Comments: STORE PROCEDURES
*******************************************************************/

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_GetItemAttributes')
DROP FUNCTION fn_GetItemAttributes
GO
CREATE FUNCTION [dbo].[fn_GetItemAttributes] 
(
	@numDomainID NUMERIC(18,0),
	@numItemCode NUMERIC(18,0),
	@bitEcommerce BIT
)
RETURNS VARCHAR(500)
AS
BEGIN
	DECLARE @strAttribute varchar(500) = ''
	DECLARE @numCusFldID NUMERIC(18,0) = 0
	DECLARE @numCusFldValue NUMERIC(18,0) = 0

	SELECT TOP 1
		@numCusFldID=fld_id
		,@numCusFldValue=fld_value 
	FROM 
		ItemAttributes 
	WHERE 
		numDomainID = @numDomainID
		AND numItemCode=@numItemCode 
		AND fld_value != 0
	ORDER BY 
		fld_id

	while @numCusFldID>0
	begin
		IF ISNUMERIC(@numCusFldValue)=1
		begin
			SELECT 
				@strAttribute = CONCAT(@strAttribute,(CASE WHEN LEN(@strAttribute) > 0 THEN (CASE WHEN ISNULL(@bitEcommerce,0) = 1 THEN '&nbsp;&nbsp;&nbsp;&nbsp;' ELSE ',' END) ELSE '' END),(CASE WHEN ISNULL(@bitEcommerce,0) = 1 THEN CONCAT('<b><i>(',Fld_label,')</i></b>&nbsp;') ELSE Fld_label END)) 
			FROM 
				CFW_Fld_Master 
			WHERE 
				Fld_id=@numCusFldID 
				AND Grp_id = 9
                
			IF LEN(@strAttribute) > 0
			BEGIN
				IF ISNULL(@bitEcommerce,0) = 1
				BEGIN
					SELECT 
						@strAttribute=CONCAT(@strAttribute,vcData) 
					FROM 
						ListDetails 
					WHERE 
						numListItemID=@numCusFldValue
				END
				ELSE
				BEGIN
					SELECT @strAttribute=@strAttribute+':'+ vcData FROM ListDetails WHERE numListItemID=@numCusFldValue
				END
			END
		END
		ELSE
		BEGIN
			SELECT 
				@strAttribute = CONCAT(@strAttribute,(CASE WHEN LEN(@strAttribute) > 0 THEN (CASE WHEN ISNULL(@bitEcommerce,0) = 1 THEN '&nbsp;&nbsp;&nbsp;&nbsp;' ELSE ',' END) ELSE '' END),(CASE WHEN ISNULL(@bitEcommerce,0) = 1 THEN CONCAT('<b><i>(',Fld_label,')</i></b>&nbsp;') ELSE Fld_label END)) 
			FROM 
				CFW_Fld_Master 
			WHERE 
				Fld_id=@numCusFldID 
				AND Grp_id = 9

			IF LEN(@strAttribute) > 0
			BEGIN
				IF ISNULL(@bitEcommerce,0) = 1
				BEGIN
					SELECT 
						@strAttribute=CONCAT(@strAttribute,'-') 
					FROM 
						ListDetails 
					WHERE 
						numListItemID=@numCusFldValue
				END
				ELSE
				BEGIN
					SELECT @strAttribute=@strAttribute+':-'
				END
			END
		end


		SELECT TOP 1
			@numCusFldID=fld_id
			,@numCusFldValue=fld_value 
		FROM 
			ItemAttributes 
		WHERE 
			numDomainID = @numDomainID
			AND numItemCode=@numItemCode 
			AND fld_value != 0
			AND fld_id > @numCusFldID
		ORDER BY 
			fld_id

		IF @@rowcount=0 SET @numCusFldID=0
	END

	RETURN @strAttribute
END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='GetBizDocItemProfitAmountOrMargin')
DROP FUNCTION GetBizDocItemProfitAmountOrMargin
GO
CREATE FUNCTION [dbo].[GetBizDocItemProfitAmountOrMargin]
(
	@numDomainID NUMERIC(18,0)
	,@numOppID NUMERIC(18,0)
	,@numOppBizDocID NUMERIC(18,0)
	,@numOppBizDocItemID NUMERIC(18,0)
	,@tintMode TINYINT --1: Profit Amount, 2:Profit Margin
)    
RETURNS DECIMAL(20,5)    
AS    
BEGIN  
	DECLARE @Value AS DECIMAL(20,5)

	DECLARE @numDiscountItemID NUMERIC(18,0)
	DECLARE @ProfitCost INT
	SELECT @numDiscountItemID=numDiscountServiceItemID,@ProfitCost=numCost FROM Domain WHERE numDomainId=@numDomainID

	If @tintMode = 1
	BEGIN
		SELECT
			@Value = SUM(ISNULL(TEMP.monTotAmount,0) - (ISNULL(TEMP.numUnitHour,0) * (CASE 
																				WHEN ISNULL(TEMPMatchedPO.numoppitemtCode,TEMPPO.numoppitemtCode) IS NOT NULL 
																				THEN ISNULL(ISNULL(TEMPMatchedPO.monPrice,TEMPPO.monPrice),0)
																				ELSE
																					(CASE @ProfitCost 
																						WHEN 3 THEN ISNULL(TEMP.monCost,0) * dbo.fn_UOMConversion(TEMP.numBaseUnit,TEMP.numItemCode,@numDomainID,TEMP.numPurchaseUnit) 
																						ELSE ISNULL(TEMP.monAverageCost,0)
																					END)
																			END)))
		FROM
		(
			SELECT
				OM.numOppId
				,OI.numoppitemtCode
				,OI.vcAttrValues
				,OBDI.numUnitHour
				,OBDI.monTotAmount
				,I.numItemCode
				,I.numBaseUnit
				,I.numPurchaseUnit
				,V.monCost
				,(CASE 
					WHEN ISNULL(I.bitKitParent,0) = 1 
					THEN ISNULL((SELECT SUM(ISNULL(OKI.numQtyItemsReq_Orig,0) * ISNULL(OKI.monAvgCost,0)) FROM OpportunityKitItems OKI INNER JOIN Item IInner ON OKI.numChildItemID=IInner.numItemCode WHERE OKI.numOppId=OM.numOppId AND OKI.numOppItemID=OI.numoppitemtCode),0) + 
							ISNULL((SELECT SUM(ISNULL(OKCI.numQtyItemsReq_Orig,0) * ISNULL(OKI.numQtyItemsReq_Orig,0) * ISNULL(OKCI.monAvgCost,0)) FROM OpportunityKitChildItems OKCI INNER JOIN OpportunityKitItems OKI ON OKCI.numOppChildItemID=OKI.numOppChildItemID INNER JOIN Item IInner ON OKCI.numItemID=IInner.numItemCode WHERE OKI.numOppId=OM.numOppId AND OKI.numOppItemID=OI.numoppitemtCode),0)
					ELSE ISNULL(OBDI.monAverageCost,0) 
				END) monAverageCost
			FROM
				OpportunityBizDocs OBD
			INNER JOIN
				OpportunityMaster OM
			ON
				OBD.numOppId = OM.numOppId
			INNER JOIN
				OpportunityBizDocItems OBDI
			ON
				OBD.numOppBizDocsId = OBDI.numOppBizDocID
			INNER JOIN
				OpportunityItems OI
			ON
				OBDI.numOppItemID = OI.numoppitemtCode
			INNER JOIN
				Item I
			ON
				OBDI.numItemCode = I.numItemCode
			LEFT JOIN 
				Vendor V 
			ON 
				V.numVendorID=I.numVendorID 
				AND V.numItemCode=I.numItemCode
			WHERE
				OBD.numOppId = @numOppID
				AND OBD.numOppBizDocsId = @numOppBizDocID
				AND OBDI.numOppBizDocItemID = @numOppBizDocItemID
				AND I.numItemCode NOT IN (@numDiscountItemID)
		) TEMP
		OUTER APPLY
		(
			SELECT TOP 1
				OMInner.numOppID
				,OMInner.vcPOppName
				,OIInner.numoppitemtCode
				,OIInner.monPrice
			FROM
				SalesOrderLineItemsPOLinking SOLIPL
			INNER JOIN
				OpportunityMaster OMInner
			ON
				SOLIPL.numPurchaseOrderID = OMInner.numOppId
			INNER JOIN
				OpportunityItems OIInner
			ON
				OMInner.numOppId = OIInner.numOppId
				AND SOLIPL.numPurchaseOrderItemID = OIInner.numoppitemtCode
			WHERE
				SOLIPL.numSalesOrderID = TEMP.numOppId
				AND SOLIPL.numSalesOrderItemID = TEMP.numoppitemtCode
		) TEMPMatchedPO
		OUTER APPLY
		(
			SELECT TOP 1
				OMInner.numOppID
				,OMInner.vcPOppName
				,OIInner.numoppitemtCode
				,OIInner.monPrice
			FROM
				OpportunityLinking OL
			INNER JOIN
				OpportunityMaster OMInner
			ON
				OL.numChildOppID = OMInner.numOppId
			INNER JOIN
				OpportunityItems OIInner
			ON
				OMInner.numOppId = OIInner.numOppId
			WHERE
				OL.numParentOppID = TEMP.numOppId
				AND OIInner.numItemCode = TEMP.numItemCode
				AND OIInner.vcAttrValues = TEMP.vcAttrValues
		) TEMPPO		
	END
	ELSE IF @tintMode = 2
	BEGIN
		SELECT
			@Value = (SUM(Profit)/CASE WHEN SUM(monTotAmount) = 0 THEN 1.0 ELSE SUM(monTotAmount) END) * 100
		FROM
		(
			SELECT
				ISNULL(OBDI.monTotAmount,0) monTotAmount
				,ISNULL(OBDI.monTotAmount,0) - (ISNULL(OBDI.numUnitHour,0) * (CASE 
																		WHEN ISNULL(TEMPMatchedPO.numoppitemtCode,TEMPPO.numoppitemtCode) IS NOT NULL 
																		THEN ISNULL(ISNULL(TEMPMatchedPO.monPrice,TEMPPO.monPrice),0)
																		ELSE (CASE @ProfitCost 
																				WHEN 3 THEN ISNULL(V.monCost,0) * dbo.fn_UOMConversion(numBaseUnit,I.numItemCode,@numDomainID,numPurchaseUnit) 
																				ELSE (CASE 
																						WHEN ISNULL(I.bitKitParent,0) = 1 
																						THEN ISNULL((SELECT SUM(ISNULL(OKI.numQtyItemsReq_Orig,0) * ISNULL(OKI.monAvgCost,0)) FROM OpportunityKitItems OKI INNER JOIN Item IInner ON OKI.numChildItemID=IInner.numItemCode WHERE OKI.numOppId=OM.numOppId AND OKI.numOppItemID=OI.numoppitemtCode),0) + 
																								ISNULL((SELECT SUM(ISNULL(OKCI.numQtyItemsReq_Orig,0) * ISNULL(OKI.numQtyItemsReq_Orig,0) * ISNULL(OKCI.monAvgCost,0)) FROM OpportunityKitChildItems OKCI INNER JOIN OpportunityKitItems OKI ON OKCI.numOppChildItemID=OKI.numOppChildItemID INNER JOIN Item IInner ON OKCI.numItemID=IInner.numItemCode WHERE OKI.numOppId=OM.numOppId AND OKI.numOppItemID=OI.numoppitemtCode),0)
																						ELSE ISNULL(OBDI.monAverageCost,0) 
																					END)
																				END)
																	END)) Profit
			FROM
				OpportunityBizDocs OBD
			INNER JOIN
				OpportunityMaster OM
			ON
				OBD.numOppId = OM.numOppId
			INNER JOIN
				OpportunityBizDocItems OBDI
			ON
				OBD.numOppBizDocsId = OBDI.numOppBizDocID
			INNER JOIN
				OpportunityItems OI
			ON
				OBDI.numOppItemID = OI.numoppitemtCode
			INNER JOIN
				Item I
			ON
				OBDI.numItemCode = I.numItemCode
			OUTER APPLY
			(
				SELECT TOP 1
					OMInner.numOppID
					,OMInner.vcPOppName
					,OIInner.numoppitemtCode
					,OIInner.monPrice
				FROM
					SalesOrderLineItemsPOLinking SOLIPL
				INNER JOIN
					OpportunityMaster OMInner
				ON
					SOLIPL.numPurchaseOrderID = OMInner.numOppId
				INNER JOIN
					OpportunityItems OIInner
				ON
					OMInner.numOppId = OIInner.numOppId
					AND SOLIPL.numPurchaseOrderItemID = OIInner.numoppitemtCode
				WHERE
					SOLIPL.numSalesOrderID = OM.numOppId
					AND SOLIPL.numSalesOrderItemID = OI.numoppitemtCode
			) TEMPMatchedPO
			OUTER APPLY
			(
				SELECT TOP 1
					OMInner.numOppID
					,OMInner.vcPOppName
					,OIInner.numoppitemtCode
					,OIInner.monPrice
				FROM
					OpportunityLinking OL
				INNER JOIN
					OpportunityMaster OMInner
				ON
					OL.numChildOppID = OMInner.numOppId
				INNER JOIN
					OpportunityItems OIInner
				ON
					OMInner.numOppId = OIInner.numOppId
				WHERE
					OL.numParentOppID = OM.numOppId
					AND OIInner.numItemCode = OI.numItemCode
					AND OIInner.vcAttrValues = OI.vcAttrValues
			) TEMPPO
			Left JOIN 
				Vendor V 
			ON 
				V.numVendorID=I.numVendorID 
				AND V.numItemCode=I.numItemCode
			WHERE
				OBD.numOppId = @numOppID
				AND OBD.numOppBizDocsId = @numOppBizDocID
				AND OBDI.numOppBizDocItemID = @numOppBizDocItemID
				AND I.numItemCode NOT IN (@numDiscountItemID)
		) TEMP
	END

	RETURN CAST(@Value AS DECIMAL(20,5))
END
GO
/****** Object:  StoredProcedure [dbo].[USP_AdvancedSearch]    Script Date: 05/07/2009 22:04:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                              
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_advancedsearch')
DROP PROCEDURE usp_advancedsearch
GO
CREATE PROCEDURE [dbo].[USP_AdvancedSearch] 
@WhereCondition as varchar(4000)='',                            
@AreasofInt as varchar(100)='',                              
@tintUserRightType as tinyint,                              
@numDomainID as numeric(9)=0,                              
@numUserCntID as numeric(9)=0,                                                        
@CurrentPage int,                                                                    
@PageSize int,                                                                    
@TotRecs int output,                                                                                                             
@columnSortOrder as Varchar(10),                              
@ColumnSearch as varchar(100)='',                           
@ColumnName as varchar(20)='',                             
@GetAll as bit,                              
@SortCharacter as char(1),                        
@SortColumnName as varchar(100)='',        
@bitMassUpdate as bit,        
@LookTable as varchar(200)='' ,        
@strMassUpdate as varchar(MAX)='',
@vcRegularSearchCriteria varchar(MAX)='',
@vcCustomSearchCriteria varchar(MAX)='',
@ClientTimeZoneOffset as int,
@vcDisplayColumns VARCHAR(MAX)
as                              
BEGIN
	DECLARE @tintOrder  AS TINYINT
	DECLARE @vcFormFieldName  AS VARCHAR(50)
	DECLARE @vcListItemType  AS VARCHAR(3)
	DECLARE @vcListItemType1  AS VARCHAR(3)
	DECLARE @vcAssociatedControlType VARCHAR(20)
	DECLARE @numListID  AS NUMERIC(9)
	DECLARE @vcDbColumnName VARCHAR(50)
	DECLARE @vcLookBackTableName VARCHAR(50)
	DECLARE @WCondition VARCHAR(1000)
	DECLARE @bitContactAddressJoinAdded AS BIT;
	DECLARE @bitBillAddressJoinAdded AS BIT;
	DECLARE @bitShipAddressJoinAdded AS BIT;
	DECLARE @vcOrigDbColumnName as varchar(50)
	DECLARE @bitAllowSorting AS CHAR(1)
	DECLARE @bitAllowEdit AS CHAR(1)
	DECLARE @bitCustomField AS BIT;
	DECLARE @ListRelID AS NUMERIC(9)
	DECLARE @intColumnWidth INT
	DECLARE @bitAllowFiltering AS BIT;
	DECLARE @vcfieldatatype CHAR(1)
	DECLARE @zipCodeSearch AS VARCHAR(100) = ''


	IF CHARINDEX('$SZ$', @WhereCondition)>0
	BEGIN
		SELECT @zipCodeSearch=SUBSTRING(@WhereCondition, CHARINDEX('$SZ$', @WhereCondition)+4,CHARINDEX('$EZ$', @WhereCondition) - CHARINDEX('$SZ$', @WhereCondition)-4) 

		CREATE TABLE #tempAddress
		(
			[numAddressID] [numeric](18, 0) NOT NULL,
			[vcAddressName] [varchar](50) NULL,
			[vcStreet] [varchar](100) NULL,
			[vcCity] [varchar](50) NULL,
			[vcPostalCode] [varchar](15) NULL,
			[numState] [numeric](18, 0) NULL,
			[numCountry] [numeric](18, 0) NULL,
			[bitIsPrimary] [bit] NOT NULL CONSTRAINT [DF_AddressMaster_bitIsPrimary]  DEFAULT ((0)),
			[tintAddressOf] [tinyint] NOT NULL,
			[tintAddressType] [tinyint] NOT NULL CONSTRAINT [DF_AddressMaster_tintAddressType]  DEFAULT ((0)),
			[numRecordID] [numeric](18, 0) NOT NULL,
			[numDomainID] [numeric](18, 0) NOT NULL
		)

		IF Len(@zipCodeSearch)>0
		BEGIN
			Declare @Strtemp as nvarchar(500);
			SET @Strtemp='insert into #tempAddress select * from AddressDetails  where numDomainID=@numDomainID1  
							and vcPostalCode in (SELECT REPLICATE(''0'', 5 - LEN(ZipCode)) + ZipCode  FROM ' + @zipCodeSearch +')';
	
			EXEC sp_executesql @Strtemp,N'@numDomainID1 numeric(18)',@numDomainID1=@numDomainID

			set @WhereCondition=replace(@WhereCondition,'$SZ$' + @zipCodeSearch + '$EZ$',
			' (ADC.numContactID in (select  numRecordID from #tempAddress where tintAddressOf=1 AND tintAddressType=0)   
			or (DM.numDivisionID in (select  numRecordID from #tempAddress where tintAddressOf=2 AND tintAddressType in(1,2))))')
		END
	END

	SET @WCondition = ''
   
	IF (@SortCharacter <> '0' AND @SortCharacter <> '')
	  SET @ColumnSearch = @SortCharacter

	CREATE TABLE #temptable 
	(
		id INT IDENTITY PRIMARY KEY,
		numcontactid VARCHAR(15)
	)


---------------------Find Duplicate Code Start-----------------------------
	DECLARE  @Sql           VARCHAR(8000),
			 @numMaxFieldId NUMERIC(9),
			 @Where         NVARCHAR(1000),
			 @OrderBy       NVARCHAR(1000),
			 @GroupBy       NVARCHAR(1000),
			 @SelctFields   NVARCHAR(4000),
			 @InneJoinOn    NVARCHAR(1000)
	SET @InneJoinOn = ''
	SET @OrderBy = ''
	SET @Sql = ''
	SET @Where = ''
	SET @GroupBy = ''
	SET @SelctFields = ''
    
	IF (@WhereCondition = 'DUP' OR @WhereCondition = 'DUP-PRIMARY')
	BEGIN
		SELECT @numMaxFieldId= MAX([numFieldID]) FROM View_DynamicColumns
		WHERE [numFormID] =24 AND [numDomainId] = @numDomainID
		--PRINT @numMaxFieldId
		WHILE @numMaxFieldId > 0
		BEGIN
			SELECT @vcFormFieldName=[vcFieldName],@vcDbColumnName=[vcDbColumnName],@vcLookBackTableName=[vcLookBackTableName] 
			FROM View_DynamicColumns 
			WHERE [numFieldID] = @numMaxFieldId and [numFormID] =24 AND [numDomainId] = @numDomainID
			--	PRINT @vcDbColumnName
				SET @SelctFields = @SelctFields +  @vcDbColumnName + ','
				IF(@vcLookBackTableName ='AdditionalContactsInformation')
				BEGIN
					SET @InneJoinOn = @InneJoinOn + 'ADC.'+ @vcDbColumnName +' = a1.' + @vcDbColumnName + ' and '
					SET @GroupBy = @GroupBy + 'ADC.'+  @vcDbColumnName + ','
				END
				ELSE IF (@vcLookBackTableName ='DivisionMaster')
				BEGIN
					SET @InneJoinOn = @InneJoinOn + 'DM.'+ @vcDbColumnName +' = a1.' + @vcDbColumnName + ' and '
					SET @GroupBy = @GroupBy + 'DM.'+  @vcDbColumnName + ','
				END
				ELSE
				BEGIN
					SET @InneJoinOn = @InneJoinOn + 'C.' + @vcDbColumnName +' = a1.' + @vcDbColumnName + ' and '
					SET @GroupBy = @GroupBy + 'C.' + @vcDbColumnName + ','
				END
				SET @Where = @Where + 'and LEN(' + @vcDbColumnName + ')>0 '

	
			SELECT @numMaxFieldId = MAX([numFieldID]) FROM View_DynamicColumns 
			WHERE [numFormID] =24 AND [numDomainId] = @numDomainID AND [numFieldID] < @numMaxFieldId
		END
			
				IF(LEN(@Where)>2)
				BEGIN
					SET @Where =RIGHT(@Where,LEN(@Where)-3)
					SET @OrderBy =  LEFT(@GroupBy,(Len(@GroupBy)- 1))
					SET @GroupBy =  LEFT(@GroupBy,(Len(@GroupBy)- 1))
					SET @InneJoinOn = LEFT(@InneJoinOn,LEN(@InneJoinOn)-3)
					SET @SelctFields = LEFT(@SelctFields,(Len(@SelctFields)- 1))
					SET @Sql = @Sql + ' INSERT INTO [#tempTable] ([numContactID])SELECT  ADC.numContactId FROM additionalcontactsinformation ADC JOIN DivisionMaster DM ON ADC.numDivisionId = DM.numDivisionID JOIN CompanyInfo C ON DM.numCompanyID = C.numCompanyId  
						JOIN (SELECT   '+ @SelctFields + '
					   FROM  additionalcontactsinformation ADC JOIN DivisionMaster DM ON ADC.numDivisionId = DM.numDivisionID JOIN CompanyInfo C ON DM.numCompanyID = C.numCompanyId  
					   WHERE  ' + @Where + ' AND C.[numDomainID] = '+convert(varchar(15),@numDomainID)+ ' AND ADC.[numDomainID] = '+convert(varchar(15),@numDomainID)+ ' AND DM.[numDomainID] = '+convert(varchar(15),@numDomainID)+ 
					   ' GROUP BY ' + @GroupBy + '
					   HAVING   COUNT(* ) > 1) a1
						ON  '+ @InneJoinOn + ' where ADC.[numDomainID] = '+convert(varchar(15),@numDomainID) + ' and DM.[numDomainID] = '+convert(varchar(15),@numDomainID) + ' ORDER BY '+ @OrderBy
				END	
				ELSE
				BEGIN
					SET @Where = ' 1=0 '
					SET @Sql = @Sql + ' INSERT INTO [#tempTable] ([numContactID])SELECT  ADC.numContactId FROM additionalcontactsinformation ADC  '
								+ ' Where ' + @Where
				END 	
			   PRINT @Sql
			   EXEC( @Sql)
			   SET @Sql = ''
			   SET @GroupBy = ''
			   SET @SelctFields = ''
			   SET @Where = ''
	
		IF (@WhereCondition = 'DUP-PRIMARY')
		  BEGIN
			SET @WCondition = ' AND ADC.numContactId IN (SELECT DISTINCT numContactId FROM #tempTable) and ISNULL(ADC.bitPrimaryContact,0)=1'
		  END
		ELSE
		  BEGIN
			SET @WCondition = ' AND ADC.numContactId IN (SELECT DISTINCT numContactId FROM #tempTable)'
		  END
    
		SET @Sql = ''
		SET @WhereCondition =''
		SET @Where= 'DUP' 
	END
-----------------------------End Find Duplicate ------------------------

	declare @strSql as varchar(8000)                                                              
	 set  @strSql='Select ADC.numContactId                    
	  FROM AdditionalContactsInformation ADC                                                   
	  JOIN DivisionMaster DM ON ADC.numDivisionId = DM.numDivisionID                                                  
	  JOIN CompanyInfo C ON DM.numCompanyID = C.numCompanyId'   

	IF @SortColumnName='vcStreet' OR @SortColumnName='vcCity' OR @SortColumnName='vcPostalCode' OR @SortColumnName ='numCountry' OR @SortColumnName ='numState'
	   OR @ColumnName='vcStreet' OR @ColumnName='vcCity' OR @ColumnName='vcPostalCode' OR @ColumnName ='numCountry' OR @ColumnName ='numState'	
	BEGIN 
		set @strSql= @strSql +' left Join AddressDetails AD on AD.numRecordId= ADC.numContactID and AD.tintAddressOf=1 and AD.tintAddressType=0 AND AD.bitIsPrimary=1 and AD.numDomainID= ADC.numDomainID'	
	END 
	ELSE IF @SortColumnName='vcBillStreet' OR @SortColumnName='vcBillCity' OR @SortColumnName='vcBillPostCode' OR @SortColumnName ='numBillCountry' OR @SortColumnName ='numBillState'
		 OR @ColumnName='vcBillStreet' OR @ColumnName='vcBillCity' OR @ColumnName='vcBillPostCode' OR @ColumnName ='numBillCountry' OR @ColumnName ='numBillState'
	BEGIN 	
		set @strSql= @strSql +' left Join AddressDetails AD1 on AD1.numRecordId= DM.numDivisionID and AD1.tintAddressOf=2 and AD1.tintAddressType=1 AND AD1.bitIsPrimary=1 and AD1.numDomainID= ADC.numDomainID '
	END	
	ELSE IF @SortColumnName='vcShipStreet' OR @SortColumnName='vcShipCity' OR @SortColumnName='vcShipPostCode' OR @SortColumnName ='numShipCountry' OR @SortColumnName ='numShipState'
		 OR @ColumnName='vcShipStreet' OR @ColumnName='vcShipCity' OR @ColumnName='vcShipPostCode' OR @ColumnName ='numShipCountry' OR @ColumnName ='numShipState'
	BEGIN
		set @strSql= @strSql +' left Join AddressDetails AD2 on AD2.numRecordId= DM.numDivisionID and AD2.tintAddressOf=2 and AD2.tintAddressType=2 AND AD2.bitIsPrimary=1 and AD2.numDomainID= ADC.numDomainID '
	END	
                         
	if (@ColumnName<>'' and  @ColumnSearch<>'')                        
	begin                          
		SELECT @vcListItemType=vcListItemType,@numListID=numListID from View_DynamicDefaultColumns where vcDbColumnName=@ColumnName and numFormID=1 and numDomainId=@numDomainId                         
    
		if @vcListItemType='LI'                               
		begin                      
			IF @numListID=40--Country
			BEGIN
				IF @ColumnName ='numCountry'
				BEGIN
						set @strSql= @strSql +' left Join ListDetails L1 on L1.numListItemID=AD.numCountry' 
				END
				ELSE IF @ColumnName ='numBillCountry'
				BEGIN
						set @strSql= @strSql +' left Join ListDetails L1 on L1.numListItemID=AD1.numCountry' 
				END
				ELSE IF @ColumnName ='numShipCountry'
				BEGIN
						set @strSql= @strSql +' left Join ListDetails L1 on L1.numListItemID=AD2.numCountry' 
				END
			END                        
			ELSE
			BEGIN
				set @strSql= @strSql +' left Join ListDetails L1 on L1.numListItemID='+@ColumnName                              
			END   
		end                              
		else if @vcListItemType='S'                               
		begin           
		   IF @ColumnName ='numState'
			BEGIN
   				set @strSql= @strSql +' left join State S1 on S1.numStateID=AD.numState'                            
			END
			ELSE IF @ColumnName ='numBillState'
			BEGIN
   				set @strSql= @strSql +' left join State S1 on S1.numStateID=AD1.numState'                            
			END
			ELSE IF @ColumnName ='numShipState'
			BEGIN
   				set @strSql= @strSql +' left join State S1 on S1.numStateID=AD2.numState'                           
			END
		end                              
		else if @vcListItemType='T'                               
		begin                              
		  set @strSql= @strSql +' left Join ListDetails L1 on L1.numListItemID='+@ColumnName                             
		end                           
	end       
                      
	if (@SortColumnName<>'')                        
	begin                          
		select @vcListItemType1=vcListItemType,@numListID=numListID from View_DynamicDefaultColumns where vcDbColumnName=@SortColumnName and numFormID=1 and numDomainId=@numDomainId                         
    
		if @vcListItemType1='LI'                   
		begin     
			IF @numListID=40--Country
			BEGIN
				IF @SortColumnName ='numCountry'
				BEGIN
						set @strSql= @strSql +' left Join ListDetails L2 on L2.numListItemID=AD.numCountry' 
				END
				ELSE IF @SortColumnName ='numBillCountry'
				BEGIN
						set @strSql= @strSql +' left Join ListDetails L2 on L2.numListItemID=AD1.numCountry' 
				END
				ELSE IF @SortColumnName ='numShipCountry'
				BEGIN
						set @strSql= @strSql +' left Join ListDetails L2 on L2.numListItemID=AD2.numCountry' 
				END
			  END                        
			ELSE
			BEGIN
				set @strSql= @strSql +' left Join ListDetails L2 on L2.numListItemID='+@SortColumnName                              
			END
		END                              
		else if @vcListItemType1='S'           
		begin                
			IF @SortColumnName ='numState'
			BEGIN
   				set @strSql= @strSql +' left join State S2 on S2.numStateID=AD.numState'                            
			END
			ELSE IF @SortColumnName ='numBillState'
			BEGIN
   				set @strSql= @strSql +' left join State S2 on S2.numStateID=AD1.numState'                            
			END
			ELSE IF @SortColumnName ='numShipState'
			BEGIN
   				set @strSql= @strSql +' left join State S2 on S2.numStateID=AD2.numState'                           
			END
		end                              
		else if @vcListItemType1='T'                  
		begin                              
			set @strSql= @strSql +' left Join ListDetails L2 on L2.numListItemID='+@SortColumnName                               
		end                           
	end                          
  
	set @strSql=@strSql+' where DM.numDomainID  = '+convert(varchar(15),@numDomainID)+   @WhereCondition                                  
                 
--if @tintUserRightType=1 set @strSql=@strSql + ' AND (ADC.numRecOwner = '+convert(varchar(15),@numUserCntID)+ ')'                                       
--else if @tintUserRightType=2 set @strSql=@strSql + ' AND (DM.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+convert(varchar(15),@numUserCntID)+' ) or DM.numTerID=0) '
                        
IF @vcRegularSearchCriteria<>'' set @strSql=@strSql+' and ' + @vcRegularSearchCriteria 
IF @vcCustomSearchCriteria<>'' set @strSql=@strSql+' and ' + @vcCustomSearchCriteria                          
                                            
if    @AreasofInt<>'' set  @strSql=@strSql+ ' and ADC.numContactID in (select numContactID from  AOIContactLink where numAOIID in('+ @AreasofInt+'))'
                             
if (@ColumnName<>'' and  @ColumnSearch<>'')                         
begin                          
  if @vcListItemType='LI'                               
    begin                                
      set @strSql= @strSql +' and L1.vcData like '''+@ColumnSearch +'%'''                             
    end                              
    else if @vcListItemType='S'                               
    begin                                  
      set @strSql= @strSql +' and S1.vcState like '''+@ColumnSearch+'%'''                          
    end                              
    else if @vcListItemType='T'                               
    begin                              
      set @strSql= @strSql +' and T1.vcTerName like '''+@ColumnSearch  +'%'''                            
    end   
    else  
       BEGIN     
		   IF @ColumnName='vcStreet' OR @ColumnName='vcBillStreet' OR @ColumnName='vcShipStreet'
				set @strSql= @strSql +' and vcStreet like '''+@ColumnSearch  +'%'''                            
		   ELSE IF @ColumnName='vcCity' OR @ColumnName='vcBillCity' OR @ColumnName='vcShipCity'
				set @strSql= @strSql +' and vcCity like '''+@ColumnSearch  +'%'''                            
		   ELSE IF @ColumnName='vcPostalCode' OR @ColumnName='vcBillPostCode' OR @ColumnName='vcShipPostCode'
				set @strSql= @strSql +' and vcPostalCode like '''+@ColumnSearch  +'%'''                            
		   else		                      
				set @strSql= @strSql +' and '+@ColumnName+' like '''+@ColumnSearch  +'%'''                            
        END   
                          
end                          
   
	if (@SortColumnName<>'')                        
	begin                           
		if @vcListItemType1='LI'                               
		begin                                
			set @strSql= @strSql +' order by L2.vcData '+@columnSortOrder                              
		end                              
		else if @vcListItemType1='S'                               
		begin                                  
			set @strSql= @strSql +' order by S2.vcState '+@columnSortOrder                              
		end                              
		else if @vcListItemType1='T'                               
		begin                              
			set @strSql= @strSql +' order by T2.vcTerName '+@columnSortOrder                              
		end  
		else  
       BEGIN     
		   IF @SortColumnName='vcStreet' OR @SortColumnName='vcBillStreet' OR @SortColumnName='vcShipStreet'
				set @strSql= @strSql +' order by vcStreet ' +@columnSortOrder                            
		   ELSE IF @SortColumnName='vcCity' OR @SortColumnName='vcBillCity' OR @SortColumnName='vcShipCity'
				set @strSql= @strSql +' order by vcCity ' +@columnSortOrder                            
		   ELSE IF @SortColumnName='vcPostalCode' OR @SortColumnName='vcBillPostCode' OR @SortColumnName='vcShipPostCode'
				set @strSql= @strSql +' order by vcPostalCode ' +@columnSortOrder
		   else if @SortColumnName='numRecOwner'
				set @strSql= @strSql + ' order by dbo.fn_GetContactName(ADC.numRecOwner) ' +@columnSortOrder
			ELSE IF @SortColumnName='Opp.vcLastSalesOrderDate'
				SET @strSql= @strSql + ' order by (' + CONCAT('SELECT MAX(bintCreatedDate) AS bintCreatedDate FROM OpportunityMaster WHERE numDomainID = ',@numDomainID,' AND numDivisionID=DM.numDivisionID AND tintOppType = 1 AND tintOppStatus = 1') + ') ' +@columnSortOrder	                            
		   else		                      
				set @strSql= @strSql +' order by '+ @SortColumnName +' ' +@columnSortOrder                            
        END                        
	END
	ELSE 
		set @strSql= @strSql +' order by  ADC.numContactID asc '
IF(@Where <>'DUP')
BEGIN
    PRINT @strSql

	INSERT INTO #temptable (numcontactid)
	EXEC( @strSql)
	
	SET @strSql = ''
END

SET @bitBillAddressJoinAdded=0;
SET @bitShipAddressJoinAdded=0;
SET @bitContactAddressJoinAdded=0;
set @tintOrder=0;
set @WhereCondition ='';
set @strSql='select ADC.numContactId,DM.numDivisionID,DM.numTerID,ADC.numRecOwner,DM.tintCRMType '                              
DECLARE @Prefix AS VARCHAR(5)
DECLARE @bitCustom BIT
DECLARE @numFieldGroupID AS INT
DECLARE @vcColumnName AS VARCHAR(200)
DECLARE @numFieldID AS NUMERIC(18,0)

	DECLARE @TEMPSelectedColumns TABLE
	(
		numFieldID NUMERIC(18,0)
		,bitCustomField BIT
		,tintOrder INT
		,intColumnWidth FLOAT
	)
	DECLARE @vcLocationID VARCHAR(100) = '0'
	SELECT @vcLocationID =isnull(vcLocationID,'0') from dynamicFormMaster where numFormID=1    

	-- IF USER HAS SELECTED ITS OWN DISPLAY COLUMNS THEN USE IT
	IF LEN(ISNULL(@vcDisplayColumns,'')) > 0
	BEGIN
		DECLARE @TempIDs TABLE
		(
			ID INT IDENTITY(1,1),
			vcFieldID VARCHAR(300)
		)

		INSERT INTO @TempIDs
		(
			vcFieldID
		)
		SELECT 
			OutParam 
		FROM 
			SplitString(@vcDisplayColumns,',')

		INSERT INTO @TEMPSelectedColumns
		(
			numFieldID,
			bitCustomField,
			tintOrder,
			intColumnWidth
		)
		SELECT
			numFormFieldID
			,bitCustomField
			,(SELECT (ID-1) FROM @TempIDs T3 WHERE T3.vcFieldID = TEMP.vcFieldID)
			,0
		FROM
		(
			SELECT  
				numFieldID as numFormFieldID,
				0 bitCustomField,
				CONCAT(numFieldID,'~0') vcFieldID
			FROM    
				View_DynamicDefaultColumns
			WHERE   
				numFormID = 1
				AND bitInResults = 1
				AND bitDeleted = 0 
				AND numDomainID = @numDomainID
			UNION 
			SELECT  
				c.Fld_id AS numFormFieldID
				,1 bitCustomField
				,CONCAT(Fld_id,'~1') vcFieldID
			FROM    
				CFW_Fld_Master C 
			LEFT JOIN 
				CFW_Validation V ON V.numFieldID = C.Fld_id
			JOIN 
				CFW_Loc_Master L on C.GRP_ID=L.Loc_Id
			WHERE   
				C.numDomainID = @numDomainID
				AND GRP_ID IN (SELECT ID FROM dbo.SplitIDs(@vcLocationID,','))
		) TEMP
		WHERE
			vcFieldID IN (SELECT vcFieldID FROM @TempIDs)
	END

	-- IF USER SELECTED DISPLAY COLUMNS IS NOT AVAILABLE THAN GET COLUMNS CONFIGURED FROM SEARCH RESULT GRID
	IF (SELECT COUNT(*) FROM @TEMPSelectedColumns) = 0
	BEGIN
		INSERT INTO @TEMPSelectedColumns
		(
			numFieldID,
			bitCustomField,
			tintOrder,
			intColumnWidth
		)
		SELECT
			numFormFieldID
			,bitCustomField
			,tintOrder
			,intColumnWidth
		FROM
		(
			SELECT  
				A.numFormFieldID,
				0 bitCustomField,
				A.tintOrder,
				A.intColumnWidth
			FROM    AdvSerViewConf A
					JOIN View_DynamicDefaultColumns D ON D.numFieldID = A.numFormFieldID
			WHERE   A.numFormID = 1
					AND D.bitInResults = 1
					AND D.bitDeleted = 0
					AND D.numDomainID = @numDomainID 
					AND D.numFormID = 1
					AND A.numDomainID = @numDomainID
					AND A.numUserCntID=@numUserCntID
					AND ISNULL(A.bitCustom,0) = 0
			UNION
			SELECT  
				A.numFormFieldID,
				1 bitCustomField,
				A.tintOrder,
				A.intColumnWidth
			FROM    AdvSerViewConf A
					JOIN CFW_Fld_Master C ON C.Fld_id = A.numFormFieldID
			WHERE   A.numFormID = 1
					AND C.numDomainID = @numDomainID 
					AND A.numDomainID = @numDomainID
					AND A.numUserCntID=@numUserCntID
					AND ISNULL(A.bitCustom,0) = 1
		) T1
		ORDER BY
			 tintOrder
	END

	-- IF USER HASN'T CONFIGURED COLUMNS FROM SEARCH RESULT GRID THEN RETURN DEFAULT COLUMNS
	IF (SELECT COUNT(*) FROM @TEMPSelectedColumns) = 0
	BEGIN
		INSERT INTO @TEMPSelectedColumns
		(
			numFieldID,
			bitCustomField,
			tintOrder,
			intColumnWidth
		)
		SELECT  
			numFieldID,
			0,
			1,
			0
		FROM    
			View_DynamicDefaultColumns
		WHERE   
			numFormID = 1
			AND numDomainID = @numDomainID
			AND numFieldID = 3
	END

	SELECT TOP 1
		@tintOrder=tintOrder,
		@numFieldID=numFieldID,
		@vcDbColumnName=vcDbColumnName,
		@vcFormFieldName=vcFieldName,
		@vcAssociatedControlType=vcAssociatedControlType,
		@vcListItemType=vcListItemType,
		@numListID=numListID,
		@vcLookBackTableName=vcLookBackTableName,
		@bitCustom=bitCustom,
		@vcOrigDbColumnName=vcOrigDbColumnName,
		@bitAllowSorting=bitAllowSorting,
		@bitAllowEdit= bitAllowEdit,
		@ListRelID=ListRelID,
        @intColumnWidth=intColumnWidth,
		@bitAllowFiltering=bitAllowFiltering,
		@vcfieldatatype=vcFieldDataType,
		@numFieldGroupID=numGroupID
	FROM
	(
		SELECT
			T1.tintOrder+1 tintOrder,
			D.numFieldID,
			vcDbColumnName,
			vcFieldName,
			vcAssociatedControlType,
			vcListItemType,
			numListID,
			vcLookBackTableName,
			0 AS bitCustom,
			vcOrigDbColumnName,
			bitAllowSorting,
			bitAllowEdit,			
			ListRelID,			
			T1.intColumnWidth as intColumnWidth,
			bitAllowFiltering,
			vcFieldDataType,			
			0 AS numGroupID
		FROM 
			View_DynamicDefaultColumns D
		INNER JOIN
			@TEMPSelectedColumns T1
		ON 
			D.numFieldID=T1.numFieldID                                                     
		WHERE 
			D.numDomainID = @numDomainID 
			AND D.numFormID = 1 
			AND ISNULL(T1.bitCustomField,0) = 0
			AND d.vcDbColumnName not in ('numBillShipCountry','numBillShipState')
		UNION
		SELECT
			T1.tintOrder+1 tintOrder,
			Fld_id AS numFieldID,
			CONCAT('Cust',Fld_id),
			Fld_label,
			c.Fld_type,
			'',
			c.numlistid,
			'',			
			1 AS bitCustom,
			'',
			null,
			null,
			null,
			null,
			null,
			'',
			Grp_id
		FROM    
			CFW_Fld_Master C 
		INNER JOIN
			@TEMPSelectedColumns T1
		ON 
			C.Fld_id=T1.numFieldID 
		WHERE   
			C.numDomainID = @numDomainID 
			AND ISNULL(T1.bitCustomField,0) = 1
	) T1
	ORDER BY 
		tintOrder asc                              

while @tintOrder>0                              
begin                              
     
	
    IF @vcLookBackTableName = 'AdditionalContactsInformation' 
        SET @Prefix = 'ADC.'
    IF @vcLookBackTableName = 'DivisionMaster' 
        SET @Prefix = 'DM.'
     
	IF @bitCustom = 0 
	BEGIN                           
		SET @vcColumnName= @vcDbColumnName+'~'+ convert(varchar(10),@numFieldId) +'~0'

		if @vcAssociatedControlType='SelectBox'                              
		begin        
		
		print @vcDbColumnName
					IF @vcDbColumnName='numDefaultShippingServiceID'
				BEGIN
					SET @strSql=@strSql+ ' ,ISNULL((SELECT vcShipmentService FROM ShippingService WHERE (numDomainID=DM.numDomainID OR ISNULL(numDomainID,0)=0) AND numShippingServiceID = DM.numDefaultShippingServiceID),'''')' + ' [' + @vcColumnName + ']' 
				END                      
                                              
			 else if @vcListItemType='LI'                               
			  begin    
				  IF @numListID=40--Country
				  BEGIN
	  				set @strSql=@strSql+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName+']'                              
					IF @vcDbColumnName ='numCountry'
					BEGIN
						IF @bitContactAddressJoinAdded=0 
					  BEGIN
							set @WhereCondition= @WhereCondition +' left Join AddressDetails AD on AD.numRecordId= ADC.numContactID and AD.tintAddressOf=1 and AD.tintAddressType=0 AND AD.bitIsPrimary=1 and AD.numDomainID= ADC.numDomainID'	
							SET @bitContactAddressJoinAdded=1;
					  END
					  set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID=AD.numCountry'	
					END
					ELSE IF @vcDbColumnName ='numBillCountry'
					BEGIN
						IF @bitBillAddressJoinAdded=0 
						BEGIN
							set @WhereCondition= @WhereCondition +' left Join AddressDetails AD1 on AD1.numRecordId= DM.numDivisionID and AD1.tintAddressOf=2 and AD1.tintAddressType=1 AND AD1.bitIsPrimary=1 and AD1.numDomainID= ADC.numDomainID '
					
							SET @bitBillAddressJoinAdded=1;
						END
						set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID=AD1.numCountry'
					END
					ELSE IF @vcDbColumnName ='numShipCountry'
					BEGIN
						IF @bitShipAddressJoinAdded=0 
						BEGIN
							set @WhereCondition= @WhereCondition +' left Join AddressDetails AD2 on AD2.numRecordId= DM.numDivisionID and AD2.tintAddressOf=2 and AD2.tintAddressType=2 AND AD2.bitIsPrimary=1 and AD2.numDomainID= ADC.numDomainID '
					
							SET @bitShipAddressJoinAdded=1;
						END
						set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID=AD2.numCountry'
					END
				  END                        
				  ELSE
				  BEGIN
						set @strSql=@strSql+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName +']'                              
						set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID='+@vcDbColumnName	
				  END
			  end                              
	                      
			  else if @vcListItemType='T'                               
			  begin                              
				  set @strSql=@strSql+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName +']'                              
			   set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID='+@vcDbColumnName                              
			  end  
			  else if   @vcListItemType='U'                                                     
				begin           
					set @strSql=@strSql+',dbo.fn_GetContactName('+ @Prefix + @vcDbColumnName+') ['+ @vcColumnName +']'
				end   
	
		end  
		ELSE IF @vcAssociatedControlType='ListBox'   
		BEGIN
			if @vcListItemType='S'                               
			  begin                         
				set @strSql=@strSql+',S'+ convert(varchar(3),@tintOrder)+'.vcState'+' ['+ @vcColumnName +']'
				IF @vcDbColumnName ='numState'
				BEGIN
					IF @bitContactAddressJoinAdded=0 
					   BEGIN
							set @WhereCondition= @WhereCondition +' left Join AddressDetails AD on AD.numRecordId= ADC.numContactID and AD.tintAddressOf=1 and AD.tintAddressType=0 AND AD.bitIsPrimary=1 and AD.numDomainID= ADC.numDomainID'	
							SET @bitContactAddressJoinAdded=1;
					  END	
					  set @WhereCondition= @WhereCondition +' left join State S'+ convert(varchar(3),@tintOrder)+ ' on S'+convert(varchar(3),@tintOrder)+ '.numStateID=AD.numState'
				END
				ELSE IF @vcDbColumnName ='numBillState'
				BEGIN
					IF @bitBillAddressJoinAdded=0 
					BEGIN
						set @WhereCondition= @WhereCondition +' left Join AddressDetails AD1 on AD1.numRecordId= DM.numDivisionID and AD1.tintAddressOf=2 and AD1.tintAddressType=1 AND AD1.bitIsPrimary=1 and AD1.numDomainID= ADC.numDomainID '
						SET @bitBillAddressJoinAdded=1;
					END
					set @WhereCondition= @WhereCondition +' left join State S'+ convert(varchar(3),@tintOrder)+ ' on S'+convert(varchar(3),@tintOrder)+ '.numStateID=AD1.numState'
				END
				ELSE IF @vcDbColumnName ='numShipState'
				BEGIN
					IF @bitShipAddressJoinAdded=0 
					BEGIN
						set @WhereCondition= @WhereCondition +' left Join AddressDetails AD2 on AD2.numRecordId= DM.numDivisionID and AD2.tintAddressOf=2 and AD2.tintAddressType=2 AND AD2.bitIsPrimary=1 and AD2.numDomainID= ADC.numDomainID '
						SET @bitShipAddressJoinAdded=1;
					END
					set @WhereCondition= @WhereCondition +' left join State S'+ convert(varchar(3),@tintOrder)+ ' on S'+convert(varchar(3),@tintOrder)+ '.numStateID=AD2.numState'
				END
			  end         
		END 
		ELSE IF @vcAssociatedControlType='TextBox'   
		BEGIN
			IF @vcDbColumnName='vcStreet' OR @vcDbColumnName='vcCity' OR @vcDbColumnName='vcPostalCode'
			begin                              
			   set @strSql=@strSql+',AD.'+ @vcDbColumnName+' ['+ @vcColumnName +']'                              
			   IF @bitContactAddressJoinAdded=0 
			   BEGIN
					set @WhereCondition= @WhereCondition +' left Join AddressDetails AD on AD.numRecordId= ADC.numContactID and AD.tintAddressOf=1 and AD.tintAddressType=0 AND AD.bitIsPrimary=1 and AD.numDomainID= ADC.numDomainID'	
					SET @bitContactAddressJoinAdded=1;
			   END
			end 
			ELSE IF @vcDbColumnName='vcBillStreet' OR @vcDbColumnName='vcBillCity' OR @vcDbColumnName='vcBillPostCode'
			begin                              
			   set @strSql=@strSql+',AD1.'+ REPLACE(REPLACE(@vcDbColumnName,'Bill',''),'PostCode','PostalCode') +' ['+ @vcColumnName +']'                              
			   IF @bitBillAddressJoinAdded=0 
			   BEGIN 
	   				set @WhereCondition= @WhereCondition +' left Join AddressDetails AD1 on AD1.numRecordId= DM.numDivisionID and AD1.tintAddressOf=2 and AD1.tintAddressType=1 AND AD1.bitIsPrimary=1 and AD1.numDomainID= ADC.numDomainID '
	   				SET @bitBillAddressJoinAdded=1;
			   END
			end 
			ELSE IF @vcDbColumnName='vcShipStreet' OR @vcDbColumnName='vcShipCity' OR @vcDbColumnName='vcShipPostCode'
			begin                              
			   set @strSql=@strSql+',AD2.'+ REPLACE(REPLACE(@vcDbColumnName,'Ship',''),'PostCode','PostalCode') +' ['+ @vcColumnName +']'                              
			   IF @bitShipAddressJoinAdded=0 
			   BEGIN
	   				set @WhereCondition= @WhereCondition +' left Join AddressDetails AD2 on AD2.numRecordId= DM.numDivisionID and AD2.tintAddressOf=2 and AD2.tintAddressType=2  AND AD2.bitIsPrimary=1 and AD2.numDomainID= ADC.numDomainID '
	   				SET @bitShipAddressJoinAdded=1;
			   END
			end 
			ELSE
			BEGIN
				set @strSql=@strSql+','+ case  when @vcDbColumnName='numAge' then 'year(getutcdate())-year(bintDOB)' when @vcDbColumnName='numDivisionID' then 'DM.numDivisionID' when @vcDbColumnName='bintCreatedDate' then 'dbo.[FormatedDateFromDate](DateAdd(minute, ' + CAST(-@ClientTimeZoneOffset AS VARCHAR) + ',DM.bintCreatedDate),DM.numDomainID)' else @vcDbColumnName end+' ['+ @vcColumnName +']'		
			END
		END
		ELSE IF @vcAssociatedControlType='DateField' and @vcDbColumnName='vcLastSalesOrderDate'
		BEGIN
			SET @WhereCondition = @WhereCondition
											+ ' OUTER APPLY(' + CONCAT('SELECT MAX(bintCreatedDate) AS bintCreatedDate FROM OpportunityMaster WHERE numDomainID = ',@numDomainID,' AND numDivisionID=DM.numDivisionID AND tintOppType = 1 AND tintOppStatus = 1') + ' ) AS TempLastOrder '

			set @strSql=@strSql+','+ CONCAT('ISNULL(dbo.FormatedDateFromDate(DateAdd(minute, ',-@ClientTimeZoneOffset,',TempLastOrder.bintCreatedDate),',@numDomainID,'),'''')') +' ['+ @vcColumnName+']'
		END	                    
		ELSE 
			SET @strSql=@strSql+','+ case  when @vcDbColumnName='numAge' then 'year(getutcdate())-year(bintDOB)' when @vcDbColumnName='numDivisionID' then 'DM.numDivisionID' when @vcDbColumnName='bintCreatedDate' then 'dbo.[FormatedDateFromDate](DM.bintCreatedDate,DM.numDomainID)' else @vcDbColumnName end+' ['+ @vcColumnName +']'
	END
	ELSE IF @bitCustom = 1                
	BEGIN
		--SET @vcColumnName= CONCAT(@vcFormFieldName,'~','Cust',@numFieldId)
		SET @vcColumnName= CONCAT('Cust',@numFieldId,'~',@numFieldId,'~',1)
			
		IF @vcAssociatedControlType = 'TextBox' or @vcAssociatedControlType = 'TextArea'            
		BEGIN
			SET @strSql= @strSql+',CFW'+ convert(VARCHAR(3),@tintOrder)+'.Fld_Value  ['+ @vcColumnName +']'                   
				
			SET @WhereCondition= @WhereCondition 
								+' left Join ' + (CASE WHEN @numFieldGroupID = 1 THEN 'CFW_FLD_Values' WHEN @numFieldGroupID = 4 THEN 'CFW_FLD_Values_Cont' ELSE 'CFW_FLD_Values' END) + ' CFW'+ convert(varchar(3),@tintOrder)
								+ ' on CFW' + convert(varchar(3),@tintOrder) 
								+ '.Fld_Id='+convert(varchar(10),@numFieldId) 
								+ 'and CFW' + convert(varchar(3),@tintOrder) 
								+ '.RecId= (CASE WHEN ' + CAST(@numFieldGroupID AS VARCHAR) + ' = 1 THEN DM.numDivisionId WHEN ' + CAST(@numFieldGroupID AS VARCHAR) + ' = 4 THEN ADC.numContactID ELSE 0 END) '                                                         
		END   
		ELSE IF @vcAssociatedControlType = 'CheckBox'       
		BEGIN
			set @strSql= @strSql+',case when isnull(CFW' + CONVERT(VARCHAR(3), @tintOrder)+ '.Fld_Value,0)='''' then 0 else  isnull(CFW'
											+ CONVERT(VARCHAR(3), @tintOrder)+ '.Fld_Value,0) end ['+ @vcColumnName + ']'
 
			set @WhereCondition= @WhereCondition +' left Join ' + (CASE WHEN @numFieldGroupID = 1 THEN 'CFW_FLD_Values' WHEN @numFieldGroupID = 4 THEN 'CFW_FLD_Values_Cont' ELSE 'CFW_FLD_Values' END) + ' CFW'+ convert(varchar(3),@tintOrder)+ '             
			on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId= (CASE WHEN ' + CAST(@numFieldGroupID AS VARCHAR) + ' = 1 THEN DM.numDivisionId WHEN ' + CAST(@numFieldGroupID AS VARCHAR) + ' = 4 THEN ADC.numContactID ELSE 0 END) '                                                     
		END                
		ELSE IF @vcAssociatedControlType = 'DateField'            
		BEGIN
			set @strSql= @strSql+',dbo.FormatedDateFromDate(CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value,'+convert(varchar(10),@numDomainId)+')  ['+ @vcColumnName +']'                   
			set @WhereCondition= @WhereCondition +' left Join ' + (CASE WHEN @numFieldGroupID = 1 THEN 'CFW_FLD_Values' WHEN @numFieldGroupID = 4 THEN 'CFW_FLD_Values_Cont' ELSE 'CFW_FLD_Values' END) + ' CFW'+ convert(varchar(3),@tintOrder)+ '                 
			on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId= (CASE WHEN ' + CAST(@numFieldGroupID AS VARCHAR) + ' = 1 THEN DM.numDivisionId WHEN ' + CAST(@numFieldGroupID AS VARCHAR) + ' = 4 THEN ADC.numContactID ELSE 0 END) '                                                         
		END                
		ELSE IF @vcAssociatedControlType = 'SelectBox'             
		BEGIN
			set @vcDbColumnName = 'DCust'+Convert(varchar(10),@numFieldId)                
			set @strSql=@strSql+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName +']'                                                          
			set @WhereCondition= @WhereCondition +' left Join ' + (CASE WHEN @numFieldGroupID = 1 THEN 'CFW_FLD_Values' WHEN @numFieldGroupID = 4 THEN 'CFW_FLD_Values_Cont' ELSE 'CFW_FLD_Values' END) + ' CFW'+ convert(varchar(3),@tintOrder)+ '                 
				on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId= (CASE WHEN ' + CAST(@numFieldGroupID AS VARCHAR) + ' = 1 THEN DM.numDivisionId WHEN ' + CAST(@numFieldGroupID AS VARCHAR) + ' = 4 THEN ADC.numContactID ELSE 0 END) '                                                        
			set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID=CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value'                
		END         
		ELSE IF @vcAssociatedControlType = 'CheckBoxList'
		BEGIN
			PRINT 'Custom1'

			SET @strSql= @strSql+',(SELECT STUFF((SELECT CONCAT('', '', vcData) FROM ListDetails WHERE numListID= ' + CAST(@numListID AS varchar) + ' AND numlistitemid IN (SELECT ISNULL(Id,0) FROM dbo.SplitIDs(CFW'+ convert(VARCHAR(3),@tintOrder)+'.Fld_Value,'','')) FOR XML PATH('''')), 1, 1, ''''))  ['+ @vcColumnName +']'   

			SET @WhereCondition= @WhereCondition 
								+' left Join ' + (CASE WHEN @numFieldGroupID = 1 THEN 'CFW_FLD_Values' WHEN @numFieldGroupID = 4 THEN 'CFW_FLD_Values_Cont' ELSE 'CFW_FLD_Values' END) + ' CFW'+ convert(varchar(3),@tintOrder)
								+ ' on CFW' + convert(varchar(3),@tintOrder) 
								+ '.Fld_Id='+convert(varchar(10),@numFieldId) 
								+ 'and CFW' + convert(varchar(3),@tintOrder) 
								+ '.RecId= (CASE WHEN ' + CAST(@numFieldGroupID AS VARCHAR) + ' = 1 THEN DM.numDivisionId WHEN ' + CAST(@numFieldGroupID AS VARCHAR) + ' = 4 THEN ADC.numContactID ELSE 0 END) '										
		
		END        
	END          
    
	SELECT TOP 1
		@tintOrder=tintOrder,
		@numFieldID=numFieldID,
		@vcDbColumnName=vcDbColumnName,
		@vcFormFieldName=vcFieldName,
		@vcAssociatedControlType=vcAssociatedControlType,
		@vcListItemType=vcListItemType,
		@numListID=numListID,
		@vcLookBackTableName=vcLookBackTableName,
		@bitCustom=bitCustom,
		@vcOrigDbColumnName=vcOrigDbColumnName,
		@bitAllowSorting=bitAllowSorting,
		@bitAllowEdit= bitAllowEdit,
		@ListRelID=ListRelID,
        @intColumnWidth=intColumnWidth,
		@bitAllowFiltering=bitAllowFiltering,
		@vcfieldatatype=vcFieldDataType,
		@numFieldGroupID=numGroupID
	FROM
	(
		SELECT
			T1.tintOrder+1 tintOrder,
			D.numFieldID,
			vcDbColumnName,
			vcFieldName,
			vcAssociatedControlType,
			vcListItemType,
			numListID,
			vcLookBackTableName,
			0 AS bitCustom,
			vcOrigDbColumnName,
			bitAllowSorting,
			bitAllowEdit,			
			ListRelID,			
			T1.intColumnWidth as intColumnWidth,
			bitAllowFiltering,
			vcFieldDataType,			
			0 AS numGroupID
		FROM 
			View_DynamicDefaultColumns D
		INNER JOIN
			@TEMPSelectedColumns T1
		ON 
			D.numFieldID=T1.numFieldID                                                     
		WHERE 
			D.numDomainID = @numDomainID 
			AND D.numFormID = 1 
			AND ISNULL(T1.bitCustomField,0) = 0
			AND d.vcDbColumnName not in ('numBillShipCountry','numBillShipState')
			AND T1.tintOrder > @tintOrder-1
		UNION
		SELECT
			T1.tintOrder+1 tintOrder,
			Fld_id AS numFieldID,
			CONCAT('Cust',Fld_id),
			Fld_label,
			c.Fld_type,
			'',
			c.numlistid,
			'',			
			1 AS bitCustom,
			'',
			null,
			null,
			null,
			null,
			null,
			'',
			Grp_id
		FROM    
			CFW_Fld_Master C 
		INNER JOIN
			@TEMPSelectedColumns T1
		ON 
			C.Fld_id=T1.numFieldID 
		WHERE   
			C.numDomainID = @numDomainID 
			AND ISNULL(T1.bitCustomField,0) = 1
			AND T1.tintOrder > @tintOrder-1
	) T1
	ORDER BY 
		tintOrder asc                    
                                      
 if @@rowcount=0 set @tintOrder=0                              
end                              

                                                                  
  declare @firstRec as integer                                                                    
  declare @lastRec as integer                                                                    
 set @firstRec= (@CurrentPage-1) * @PageSize                      
 set @lastRec= (@CurrentPage*@PageSize+1)                                                                     
set @TotRecs=(select count(*) from #tempTable)                                                   
                                          
if @GetAll=0         
begin        
        
 if @bitMassUpdate=1            
 begin  

   Declare @strReplace as varchar(8000) = ''
    set @strReplace = case when @LookTable='DivisionMaster' then 'DM.numDivisionID' when @LookTable='AdditionalContactsInformation' then 'ADC.numContactId' when @LookTable='ContactAddress' then 'ADC.numContactId' when @LookTable='ShipAddress' then 'DM.numDivisionID' when @LookTable='BillAddress' then 'DM.numDivisionID' when @LookTable='CompanyInfo' then 'C.numCompanyId' end           
   +' FROM AdditionalContactsInformation ADC inner join DivisionMaster DM on ADC.numDivisionID = DM.numDivisionID                  
   JOIN CompanyInfo C ON DM.numCompanyID = C.numCompanyId                                
    '+@WhereCondition        
    set @strMassUpdate=replace(@strMassUpdate,'@$replace',@strReplace)      
    PRINT CONCAT('MassUPDATE:',@strMassUpdate)
  exec (@strMassUpdate)         
 end         
 
 
 IF (@Where = 'DUP')
 BEGIN
	set @strSql=@strSql+' FROM AdditionalContactsInformation ADC
	  JOIN DivisionMaster DM ON ADC.numDivisionId = DM.numDivisionID
	  JOIN CompanyInfo C ON DM.numCompanyID = C.numCompanyId    
	  JOIN OpportunityMaster OM on OM.numDivisionID= DM.numDivisionID                  
	  inner join #tempTable T on T.numContactId=ADC.numContactId ' 
	  + @WhereCondition +'
	  WHERE ID > '+convert(varchar(10),@firstRec)+ ' and ID <'+ convert(varchar(10),@lastRec) +  @WCondition + ' order by T.ID, C.vcCompanyName' -- @OrderBy
	  PRINT @strSql
	  exec(@strSql)                                                                 
	 drop table #tempTable
	 RETURN	
 END
ELSE
BEGIN
	SET @strSql = @strSql + ' FROM AdditionalContactsInformation ADC
		JOIN DivisionMaster DM ON ADC.numDivisionId = DM.numDivisionID   
	  JOIN CompanyInfo C ON DM.numCompanyID = C.numCompanyId                              
	   '+@WhereCondition+' inner join #tempTable T on T.numContactId=ADC.numContactId                                                               
	  WHERE ID > '+convert(varchar(10),@firstRec)+ ' and ID <'+ convert(varchar(10),@lastRec)
	  
	   
	SET @strSql=@strSql + ' order by T.ID, C.vcCompanyName ' 
END
             
end                           
 else        
 begin                      
     set @strSql=@strSql+' FROM AdditionalContactsInformation ADC                                                   
  JOIN DivisionMaster DM ON ADC.numDivisionId = DM.numDivisionID                                                                   
  JOIN CompanyInfo C ON DM.numCompanyID = C.numCompanyId                              
  '+@WhereCondition+' join #tempTable T on T.numContactId=ADC.numContactId'        
  
	   SET @strSql=@strSql + ' order by T.ID, C.vcCompanyName'   
	end       
print @strSql                                                      
exec(@strSql) 
drop table #tempTable


	SELECT
		tintOrder,
		vcDbColumnName,
		vcFieldName,
		vcAssociatedControlType,
		vcListItemType,
		numListID,
		vcLookBackTableName,
		numFieldID,
		numFieldID AS numFormFieldID,
		intColumnWidth,
		bitCustom AS bitCustomField,
		vcOrigDbColumnName,
		bitAllowSorting,
		bitAllowEdit,
		ListRelID,
		bitAllowFiltering,
		vcFieldDataType
	FROM
	(
		SELECT
			T1.tintOrder+1 tintOrder,
			D.numFieldID,
			vcDbColumnName,
			vcFieldName,
			vcAssociatedControlType,
			vcListItemType,
			numListID,
			vcLookBackTableName,
			0 AS bitCustom,
			vcOrigDbColumnName,
			bitAllowSorting,
			bitAllowEdit,			
			ListRelID,			
			T1.intColumnWidth as intColumnWidth,
			bitAllowFiltering,
			vcFieldDataType,			
			0 AS numGroupID
		FROM 
			View_DynamicDefaultColumns D
		INNER JOIN
			@TEMPSelectedColumns T1
		ON 
			D.numFieldID=T1.numFieldID                                                     
		WHERE 
			D.numDomainID = @numDomainID 
			AND D.numFormID = 1 
			AND ISNULL(T1.bitCustomField,0) = 0
			AND d.vcDbColumnName not in ('numBillShipCountry','numBillShipState')
		UNION
		SELECT
			T1.tintOrder+1 tintOrder,
			Fld_id AS numFieldID,
			CONCAT('Cust',Fld_id),
			Fld_label,
			c.Fld_type,
			'',
			c.numlistid,
			'',			
			1 AS bitCustom,
			'',
			null,
			null,
			null,
			null,
			null,
			'',
			Grp_id
		FROM    
			CFW_Fld_Master C 
		INNER JOIN
			@TEMPSelectedColumns T1
		ON 
			C.Fld_id=T1.numFieldID 
		WHERE   
			C.numDomainID = @numDomainID 
			AND ISNULL(T1.bitCustomField,0) = 1
	) T1
	ORDER BY 
		tintOrder asc       

IF OBJECT_ID('tempdb..#tempAddress') IS NOT NULL
BEGIN
    DROP TABLE #tempAddress
END
END
/****** Object:  StoredProcedure [dbo].[USP_AdvancedSearchCase]    Script Date: 07/26/2008 16:14:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_advancedsearchcase')
DROP PROCEDURE usp_advancedsearchcase
GO
CREATE PROCEDURE [dbo].[USP_AdvancedSearchCase]    
@WhereCondition as varchar(1000)='',                                                   
@numDomainID as numeric(9)=0,                                    
@numUserCntID as numeric(9)=0,                                                                       
@CurrentPage int,                                                                          
@PageSize int,                                                                          
@TotRecs int output,                                                                                                                   
@columnSortOrder as Varchar(10),      
@ColumnName as varchar(20)='',      
@SortCharacter as char(1),                              
@SortColumnName as varchar(20)='',      
@LookTable as varchar(10)='',    
@vcDisplayColumns VARCHAR(MAX) = '',
@GetAll BIT = 0 
as     
    
declare @tintOrder as tinyint                                    
declare @vcFormFieldName as varchar(50)                                    
declare @vcListItemType as varchar(3)                               
declare @vcListItemType1 as varchar(3)                                   
declare @vcAssociatedControlType varchar(20)                                    
declare @numListID AS numeric(9)                                    
declare @vcDbColumnName varchar(30)                                     
declare @ColumnSearch as varchar(10)                            
if (@SortCharacter<>'0' and @SortCharacter<>'') set @ColumnSearch=@SortCharacter     
Create table #tempTable ( ID INT IDENTITY PRIMARY KEY,                                                                           
      numCaseID varchar(15)                                                                          
 )    
    
declare @strSql as varchar(8000)                                                                    
 set  @strSql='select numCaseId from     
Cases
Join AdditionalContactsinformation ADC on ADC.numContactId = Cases.numContactId    
join DivisionMaster DM on Dm.numDivisionId = Cases.numDivisionId    
JOIN CompanyInfo C ON DM.numCompanyID = C.numCompanyId     
  
'    
    
                               
   if (@SortColumnName<>'')                          
  begin                                
 select @vcListItemType1=vcListItemType from View_DynamicDefaultColumns where vcDbColumnName=@SortColumnName and numFormID=1 and numDomainId=@numDomainId                              
  if @vcListItemType1='LI'                         
  begin                                      
    set @strSql= @strSql +' left Join ListDetails L2 on L2.numListItemID='+@SortColumnName                                    
  end                              
  end                                
  set @strSql=@strSql+' where Cases.numDomainID  = '+convert(varchar(15),@numDomainID)+   @WhereCondition                                        
       
    
if (@ColumnName<>'' and  @ColumnSearch<>'')                               
begin                                
  if @vcListItemType='LI'                                     
    begin                                      
      set @strSql= @strSql +' and L1.vcData like '''+@ColumnSearch +'%'''                                   
    end                                    
    else set @strSql= @strSql +' and '+  @ColumnName +' like '''+@ColumnSearch  +'%'''                                  
                                
end                                
   if (@SortColumnName<>'')                              
  begin                                 
    if @vcListItemType1='LI'                                     
    begin          
      set @strSql= @strSql +' order by L2.vcData '+@columnSortOrder            
    end                                    
    else  set @strSql= @strSql +' order by '+ case when @SortColumnName='textSubject' then 'convert(varchar(500),textSubject)' 
												   when @SortColumnName = 'textInternalComments' then 'convert(varchar(500),textInternalComments)' else @SortColumnName end  +' ' +@columnSortOrder                              
  end     
    
    
insert into #tempTable(numCaseID)                     
    
exec(@strSql)      
 print @strSql   
 print '===================================================='    
                          
set @strSql=''                                    
   DECLARE @Prefix AS VARCHAR(10)
DECLARE @Table AS VARCHAR(200)                                 
                                   
set @tintOrder=0                                    
set @WhereCondition =''                                 
set @strSql='select Cases.numCaseId '    

	DECLARE @TEMPSelectedColumns TABLE
	(
		numFieldID NUMERIC(18,0)
		,bitCustomField BIT
		,tintOrder INT
		,intColumnWidth FLOAT
	)
	DECLARE @vcLocationID VARCHAR(100) = '0'
	SELECT @vcLocationID =isnull(vcLocationID,'0') from dynamicFormMaster where numFormID=17  

	-- IF USER HAS SELECTED ITS OWN DISPLAY COLUMNS THEN USE IT
	IF LEN(ISNULL(@vcDisplayColumns,'')) > 0
	BEGIN
		DECLARE @TempIDs TABLE
		(
			ID INT IDENTITY(1,1),
			vcFieldID VARCHAR(300)
		)

		INSERT INTO @TempIDs
		(
			vcFieldID
		)
		SELECT 
			OutParam 
		FROM 
			SplitString(@vcDisplayColumns,',')

		INSERT INTO @TEMPSelectedColumns
		(
			numFieldID,
			bitCustomField,
			tintOrder,
			intColumnWidth
		)
		SELECT
			numFormFieldID
			,bitCustomField
			,(SELECT (ID-1) FROM @TempIDs T3 WHERE T3.vcFieldID = TEMP.vcFieldID)
			,0
		FROM
		(
			SELECT  
				numFieldID as numFormFieldID,
				0 bitCustomField,
				CONCAT(numFieldID,'~0') vcFieldID
			FROM    
				View_DynamicDefaultColumns
			WHERE   
				numFormID = 17
				AND bitInResults = 1
				AND bitDeleted = 0 
				AND numDomainID = @numDomainID
			UNION 
			SELECT  
				c.Fld_id AS numFormFieldID
				,1 bitCustomField
				,CONCAT(Fld_id,'~1') vcFieldID
			FROM    
				CFW_Fld_Master C 
			LEFT JOIN 
				CFW_Validation V ON V.numFieldID = C.Fld_id
			JOIN 
				CFW_Loc_Master L on C.GRP_ID=L.Loc_Id
			WHERE   
				C.numDomainID = @numDomainID
				AND GRP_ID IN (SELECT ID FROM dbo.SplitIDs(@vcLocationID,','))
		) TEMP
		WHERE
			vcFieldID IN (SELECT vcFieldID FROM @TempIDs)
	END

	-- IF USER SELECTED DISPLAY COLUMNS IS NOT AVAILABLE THAN GET COLUMNS CONFIGURED FROM SEARCH RESULT GRID
	IF (SELECT COUNT(*) FROM @TEMPSelectedColumns) = 0
	BEGIN
		INSERT INTO @TEMPSelectedColumns
		(
			numFieldID,
			bitCustomField,
			tintOrder,
			intColumnWidth
		)
		SELECT
			numFormFieldID
			,bitCustomField
			,tintOrder
			,intColumnWidth
		FROM
		(
			SELECT  
				A.numFormFieldID,
				0 bitCustomField,
				A.tintOrder,
				A.intColumnWidth
			FROM    AdvSerViewConf A
					JOIN View_DynamicDefaultColumns D ON D.numFieldID = A.numFormFieldID
			WHERE   A.numFormID = 17
					AND D.bitInResults = 1
					AND D.bitDeleted = 0
					AND D.numDomainID = @numDomainID 
					AND D.numFormID = 17
					AND A.numDomainID = @numDomainID
					AND A.numUserCntID=@numUserCntID
					AND ISNULL(A.bitCustom,0) = 0
			UNION
			SELECT  
				A.numFormFieldID,
				1 bitCustomField,
				A.tintOrder,
				A.intColumnWidth
			FROM    AdvSerViewConf A
					JOIN CFW_Fld_Master C ON C.Fld_id = A.numFormFieldID
			WHERE   A.numFormID = 17
					AND C.numDomainID = @numDomainID 
					AND A.numDomainID = @numDomainID
					AND A.numUserCntID=@numUserCntID
					AND ISNULL(A.bitCustom,0) = 1
		) T1
		ORDER BY
			 tintOrder
	END

	-- IF USER HASN'T CONFIGURED COLUMNS FROM SEARCH RESULT GRID THEN RETURN DEFAULT COLUMNS
	IF (SELECT COUNT(*) FROM @TEMPSelectedColumns) = 0
	BEGIN
		INSERT INTO @TEMPSelectedColumns
		(
			numFieldID,
			bitCustomField,
			tintOrder,
			intColumnWidth
		)
		SELECT  
			numFieldID,
			0,
			1,
			0
		FROM    
			View_DynamicDefaultColumns
		WHERE   
			numFormID = 17
			AND numDomainID = @numDomainID
			AND numFieldID = 127
	END                                

DECLARE @bitCustom BIT
DECLARE @numFieldGroupID AS INT
DECLARE @vcColumnName AS VARCHAR(200)
DECLARE @numFieldID AS NUMERIC(18,0)

	SELECT TOP 1
		@tintOrder=tintOrder,
		@numFieldID=numFormFieldID,
		@vcDbColumnName=vcDbColumnName,
		@vcFormFieldName=vcFieldName,
		@vcAssociatedControlType=vcAssociatedControlType,
		@vcListItemType=vcListItemType,
		@numListID=numListID,
		@Table=vcLookBackTableName,
		@bitCustom=bitCustom,
		@numFieldGroupID=numGroupID
	FROM
	(
		SELECT
			T1.tintOrder+1 tintOrder,
			T1.numFieldID AS numFormFieldID,
			vcDbColumnName,
			vcFieldName,
			vcAssociatedControlType,
			vcListItemType,
			numListID,
			vcLookBackTableName,
			0 AS bitCustom,
			0 AS numGroupID
		FROM 
			View_DynamicDefaultColumns D
		INNER JOIN
			@TEMPSelectedColumns T1                            
		ON 
			D.numFieldId=T1.numFieldID                         
		WHERE 
			D.numDomainID = @numDomainID 
			AND D.numFormID = 17
			AND ISNULL(T1.bitCustomField,0) = 0
		UNION
		SELECT
			T1.tintOrder+1 tintOrder,
			Fld_id AS numFormFieldID,
			CONCAT('Cust',Fld_id),
			Fld_label,
			c.Fld_type,
			'',
			c.numlistid,
			'',
			1 AS bitCustom,
			Grp_id
		FROM    
			CFW_Fld_Master C 
		INNER JOIN
			@TEMPSelectedColumns T1                            
		ON 
			C.Fld_id=T1.numFieldID	
		WHERE   
			C.numDomainID = @numDomainID
			AND ISNULL(T1.bitCustomField,0) = 1
	) T1
	ORDER BY 
		tintOrder asc  


while @tintOrder>0                                    
begin                                    
                               
	IF @Table = 'AdditionalContactsInformation' 
        SET @Prefix = 'ADC.'
	IF @Table = 'DivisionMaster' 
        SET @Prefix = 'DM.'
    IF @Table = 'CompanyInfo' 
        SET @Prefix = 'C.'
    IF @Table = 'Cases' 
        SET @Prefix = 'Cases.'

	IF @bitCustom = 0
    BEGIN           
		IF @vcAssociatedControlType='SelectBox'                                    
        BEGIN                                                                           
			IF @vcListItemType='LI'                                     
			BEGIN
				SET @strSql=@strSql+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcFormFieldName+'~'+ @vcDbColumnName+']'                                    
				SET @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID='+@vcDbColumnName                                    
			END                                    
			ELSE IF @vcListItemType='U'                                                     
			BEGIN           
				SET @strSql=@strSql+',dbo.fn_GetContactName(' + @Prefix + @vcDbColumnName+') ['+ @vcFormFieldName+'~'+ @vcDbColumnName+']'
			END                                    
		END                                    
		ELSE 
			SET @strSql = @strSql +',' + CASE 
											WHEN @vcDbColumnName='numAge' THEN 'year(getutcdate())-year(bintDOB)'     
											WHEN @vcDbColumnName='numDivisionID' THEN 'DM.numDivisionID'   
											WHEN @vcDbColumnName='vcProgress' THEN ' dbo.fn_OppTotalProgress(OppMas.numOppId)'  
											WHEN @vcDbColumnName='intTargetResolveDate' OR @vcDbColumnName='dtDateEntered' OR @vcDbColumnName='bintShippedDate' OR @vcDbColumnName='bintCreatedDate' OR @vcDbColumnName='bintModifiedDate' THEN 'dbo.FormatedDateFromDate('+ @Prefix + @vcDbColumnName +','+convert(varchar(10),@numDomainId)+')'    
											ELSE @vcDbColumnName 
										END + ' ['+ @vcFormFieldName+'~'+ @vcDbColumnName+']'                 
    END
	ELSE IF @bitCustom = 1
	BEGIN
		SET @vcColumnName= CONCAT(@vcFormFieldName,'~','Cust',@numFieldId)
			
		IF @vcAssociatedControlType = 'TextBox' or @vcAssociatedControlType = 'TextArea'            
		BEGIN
			SET @strSql= @strSql+',CFW'+ convert(VARCHAR(3),@tintOrder)+'.Fld_Value  ['+ @vcColumnName +']'                   
				
			SET @WhereCondition= @WhereCondition 
								+' left Join CFW_FLD_Values_Case CFW'+ convert(varchar(3),@tintOrder)
								+ ' on CFW' + convert(varchar(3),@tintOrder) 
								+ '.Fld_Id='+convert(varchar(10),@numFieldId) 
								+ 'and CFW' + convert(varchar(3),@tintOrder) 
								+ '.RecId=Cases.numCaseId '                                                         
		END   
		ELSE IF @vcAssociatedControlType = 'CheckBox'       
		BEGIN
			set @strSql= @strSql+',case when isnull(CFW' + CONVERT(VARCHAR(3), @tintOrder)+ '.Fld_Value,0)='''' then 0 else  isnull(CFW'
											+ CONVERT(VARCHAR(3), @tintOrder)+ '.Fld_Value,0) end ['+ @vcColumnName + ']'
 
			set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values_Case CFW'+ convert(varchar(3),@tintOrder)+ '             
			on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=Cases.numCaseId '                                                     
		END                
		ELSE IF @vcAssociatedControlType = 'DateField'            
		BEGIN
			set @strSql= @strSql+',dbo.FormatedDateFromDate(CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value,'+convert(varchar(10),@numDomainId)+')  ['+ @vcColumnName +']'                   
			set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values_Case CFW'+ convert(varchar(3),@tintOrder)+ '                 
			on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=Cases.numCaseId '
		END                
		ELSE IF @vcAssociatedControlType = 'SelectBox'             
		BEGIN
			set @vcDbColumnName = 'DCust'+Convert(varchar(10),@numFieldId)                
			set @strSql=@strSql+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName +']'                                                          
			set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values_Case CFW'+ convert(varchar(3),@tintOrder)+ '                 
				on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=Cases.numCaseId '
			set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID=CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value'                
		END         
		ELSE IF @vcAssociatedControlType = 'CheckBoxList'
		BEGIN
			SET @strSql= @strSql+',(SELECT STUFF((SELECT CONCAT('', '', vcData) FROM ListDetails WHERE numListID= ' + CAST(@numListID AS varchar) + ' AND numlistitemid IN (SELECT ISNULL(Id,0) FROM dbo.SplitIDs(CFW'+ convert(VARCHAR(3),@tintOrder)+'.Fld_Value,'','')) FOR XML PATH('''')), 1, 1, ''''))  ['+ @vcColumnName +']'   

			SET @WhereCondition= @WhereCondition 
								+' left Join CFW_FLD_Values_Case CFW'+ convert(varchar(3),@tintOrder)
								+ ' on CFW' + convert(varchar(3),@tintOrder) 
								+ '.Fld_Id='+convert(varchar(10),@numFieldId) 
								+ 'and CFW' + convert(varchar(3),@tintOrder) 
								+ '.RecId=Cases.numCaseId '
		END
	
	END 
        
	SELECT TOP 1
		@tintOrder=tintOrder,
		@numFieldID=numFormFieldID,
		@vcDbColumnName=vcDbColumnName,
		@vcFormFieldName=vcFieldName,
		@vcAssociatedControlType=vcAssociatedControlType,
		@vcListItemType=vcListItemType,
		@numListID=numListID,
		@Table=vcLookBackTableName,
		@bitCustom=bitCustom,
		@numFieldGroupID=numGroupID
	FROM
	(
		SELECT
			T1.tintOrder+1 tintOrder,
			T1.numFieldID AS numFormFieldID,
			vcDbColumnName,
			vcFieldName,
			vcAssociatedControlType,
			vcListItemType,
			numListID,
			vcLookBackTableName,
			0 AS bitCustom,
			0 AS numGroupID
		FROM 
			View_DynamicDefaultColumns D
		INNER JOIN
			@TEMPSelectedColumns T1                            
		ON 
			D.numFieldId=T1.numFieldID                         
		WHERE 
			D.numDomainID = @numDomainID 
			AND D.numFormID = 17
			AND ISNULL(T1.bitCustomField,0) = 0
			AND T1.tintOrder > @tintOrder-1
		UNION
		SELECT
			T1.tintOrder+1 tintOrder,
			Fld_id AS numFormFieldID,
			CONCAT('Cust',Fld_id),
			Fld_label,
			c.Fld_type,
			'',
			c.numlistid,
			'',
			1 AS bitCustom,
			Grp_id
		FROM    
			CFW_Fld_Master C 
		INNER JOIN
			@TEMPSelectedColumns T1                            
		ON 
			C.Fld_id=T1.numFieldID	
		WHERE   
			C.numDomainID = @numDomainID
			AND ISNULL(T1.bitCustomField,0) = 1
			AND T1.tintOrder > @tintOrder-1
	) T1
	ORDER BY 
		tintOrder asc      
		                              
 if @@rowcount=0 set @tintOrder=0                                    
end     
  declare @firstRec as integer                                                                          
  declare @lastRec as integer                                                                          
 set @firstRec= (@CurrentPage-1) * @PageSize                            
 set @lastRec= (@CurrentPage*@PageSize+1)                                                                           
set @TotRecs=(select count(*) from #tempTable)     
set @strSql=@strSql+'from Cases      
Join AdditionalContactsinformation ADC on ADC.numContactId = Cases.numContactId    
join DivisionMaster DM on Dm.numDivisionId = Cases.numDivisionId    
JOIN CompanyInfo C ON DM.numCompanyID = C.numCompanyId '+@WhereCondition+'    
    
 join #tempTable T on T.numCaseID=Cases.numCaseID'

 IF ISNULL(@GetAll,0) = 0
 BEGIN
	SET @strSql = @strSql + ' WHERE ID > '+convert(varchar(10),@firstRec)+ ' and ID <'+ convert(varchar(10),@lastRec)
 END
 
 print @strSql                                                            
exec(@strSql)                                                                       
drop table #tempTable
GO
/****** Object:  StoredProcedure [dbo].[USP_AdvancedSearchPro]    Script Date: 07/26/2008 16:14:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_advancedsearchpro')
DROP PROCEDURE usp_advancedsearchpro
GO
CREATE PROCEDURE [dbo].[USP_AdvancedSearchPro]
@WhereCondition as varchar(1000)='',                                                        
@numDomainID as numeric(9)=0,                                      
@numUserCntID as numeric(9)=0,                                     
@CurrentPage int,                                                                            
@PageSize int,                                                                            
@TotRecs int output,                                                                                                                     
@columnSortOrder as Varchar(10),        
@ColumnName as varchar(20)='',        
@SortCharacter as char(1),                                
@SortColumnName as varchar(20)='',        
@LookTable as varchar(10)='' ,
@vcDisplayColumns VARCHAR(MAX),
@GetAll BIT
as
     
declare @tintOrder as tinyint                                      
declare @vcFormFieldName as varchar(50)                                      
declare @vcListItemType as varchar(3)                                 
declare @vcListItemType1 as varchar(3)                                     
declare @vcAssociatedControlType varchar(20)                                      
declare @numListID AS numeric(9)                                      
declare @vcDbColumnName varchar(30)                                       
declare @ColumnSearch as varchar(10)                              
if (@SortCharacter<>'0' and @SortCharacter<>'') set @ColumnSearch=@SortCharacter       
Create table #tempTable ( ID INT IDENTITY PRIMARY KEY,                                                                             
      numProID varchar(15)                                                                            
 )      
      
declare @strSql as varchar(8000)                                                                      
 set  @strSql='select numProID from       
ProjectsMaster ProMas
join DivisionMaster DM on Dm.numDivisionId = ProMas.numDivisionId    
JOIN CompanyInfo C ON DM.numCompanyID = C.numCompanyId
LEFT JOIN AdditionalContactsinformation ADC on ADC.numContactId = ProMas.numIntPrjMgr
'      
      
                                 
   if (@SortColumnName<>'')                            
  begin                                  
 select @vcListItemType1=vcListItemType from View_DynamicDefaultColumns where vcDbColumnName=@SortColumnName and numFormID=1  and numDomainId=@numDomainId                                
  if @vcListItemType1='L'                           
  begin                                        
    set @strSql= @strSql +' left Join ListDetails L2 on L2.numListItemID='+@SortColumnName                                      
  end                                
  end                                  
  set @strSql=@strSql+' where ProMas.numDomainID  = '+convert(varchar(15),@numDomainID)+   @WhereCondition                                          
         
      
if (@ColumnName<>'' and  @ColumnSearch<>'')                                 
begin                                  
  if @vcListItemType='L'                                       
    begin                                        
      set @strSql= @strSql +' and L1.vcData like '''+@ColumnSearch +'%'''                                     
    end                                      
    else set @strSql= @strSql +' and '+  @ColumnName +' like '''+@ColumnSearch  +'%'''                                    
                                  
end                                  
   if (@SortColumnName<>'')                                
  begin                                   
    if @vcListItemType1='L'                                       
    begin            
      set @strSql= @strSql +' order by L2.vcData '+@columnSortOrder              
    end                                      
    else  set @strSql= @strSql +' order by '+ case  when @SortColumnName = 'txtComments' then 'convert(varchar(500),txtComments)' 
		  else @SortColumnName end  +' ' +@columnSortOrder                                
  end       
      
      
insert into #tempTable(numProID)                       
      
exec(@strSql)        
 print @strSql     
 print '===================================================='      
                            
set @strSql=''                                      
                                      
                                     
set @tintOrder=0                                      
set @WhereCondition =''                                   
set @strSql='select ProMas.numProID '

	DECLARE @TEMPSelectedColumns TABLE
	(
		numFieldID NUMERIC(18,0)
		,bitCustomField BIT
		,tintOrder INT
		,intColumnWidth FLOAT
	)
	DECLARE @vcLocationID VARCHAR(100) = '0'
	SELECT @vcLocationID =isnull(vcLocationID,'0') from dynamicFormMaster where numFormID=18  

	-- IF USER HAS SELECTED ITS OWN DISPLAY COLUMNS THEN USE IT
	IF LEN(ISNULL(@vcDisplayColumns,'')) > 0
	BEGIN
		DECLARE @TempIDs TABLE
		(
			ID INT IDENTITY(1,1),
			vcFieldID VARCHAR(300)
		)

		INSERT INTO @TempIDs
		(
			vcFieldID
		)
		SELECT 
			OutParam 
		FROM 
			SplitString(@vcDisplayColumns,',')

		INSERT INTO @TEMPSelectedColumns
		(
			numFieldID,
			bitCustomField,
			tintOrder,
			intColumnWidth
		)
		SELECT
			numFormFieldID
			,bitCustomField
			,(SELECT (ID-1) FROM @TempIDs T3 WHERE T3.vcFieldID = TEMP.vcFieldID)
			,0
		FROM
		(
			SELECT  
				numFieldID as numFormFieldID,
				0 bitCustomField,
				CONCAT(numFieldID,'~0') vcFieldID
			FROM    
				View_DynamicDefaultColumns
			WHERE   
				numFormID = 18
				AND bitInResults = 1
				AND bitDeleted = 0 
				AND numDomainID = @numDomainID
			UNION 
			SELECT  
				c.Fld_id AS numFormFieldID
				,1 bitCustomField
				,CONCAT(Fld_id,'~1') vcFieldID
			FROM    
				CFW_Fld_Master C 
			LEFT JOIN 
				CFW_Validation V ON V.numFieldID = C.Fld_id
			JOIN 
				CFW_Loc_Master L on C.GRP_ID=L.Loc_Id
			WHERE   
				C.numDomainID = @numDomainID
				AND GRP_ID IN (SELECT ID FROM dbo.SplitIDs(@vcLocationID,','))
		) TEMP
		WHERE
			vcFieldID IN (SELECT vcFieldID FROM @TempIDs)
	END

	-- IF USER SELECTED DISPLAY COLUMNS IS NOT AVAILABLE THAN GET COLUMNS CONFIGURED FROM SEARCH RESULT GRID
	IF (SELECT COUNT(*) FROM @TEMPSelectedColumns) = 0
	BEGIN
		INSERT INTO @TEMPSelectedColumns
		(
			numFieldID,
			bitCustomField,
			tintOrder,
			intColumnWidth
		)
		SELECT
			numFormFieldID
			,bitCustomField
			,tintOrder
			,intColumnWidth
		FROM
		(
			SELECT  
				A.numFormFieldID,
				0 bitCustomField,
				A.tintOrder,
				A.intColumnWidth
			FROM    AdvSerViewConf A
					JOIN View_DynamicDefaultColumns D ON D.numFieldID = A.numFormFieldID
			WHERE   A.numFormID = 18
					AND D.bitInResults = 1
					AND D.bitDeleted = 0
					AND D.numDomainID = @numDomainID 
					AND D.numFormID = 18
					AND A.numDomainID = @numDomainID
					AND A.numUserCntID=@numUserCntID
					AND ISNULL(A.bitCustom,0) = 0
			UNION
			SELECT  
				A.numFormFieldID,
				1 bitCustomField,
				A.tintOrder,
				A.intColumnWidth
			FROM    AdvSerViewConf A
					JOIN CFW_Fld_Master C ON C.Fld_id = A.numFormFieldID
			WHERE   A.numFormID = 18
					AND C.numDomainID = @numDomainID 
					AND A.numDomainID = @numDomainID
					AND A.numUserCntID=@numUserCntID
					AND ISNULL(A.bitCustom,0) = 1
		) T1
		ORDER BY
			 tintOrder
	END

	-- IF USER HASN'T CONFIGURED COLUMNS FROM SEARCH RESULT GRID THEN RETURN DEFAULT COLUMNS
	IF (SELECT COUNT(*) FROM @TEMPSelectedColumns) = 0
	BEGIN
		INSERT INTO @TEMPSelectedColumns
		(
			numFieldID,
			bitCustomField,
			tintOrder,
			intColumnWidth
		)
		SELECT  
			numFieldID,
			0,
			1,
			0
		FROM    
			View_DynamicDefaultColumns
		WHERE   
			numFormID = 18
			AND numDomainID = @numDomainID
			AND numFieldID = 148
	END                                      


DECLARE @Table AS VARCHAR(200)
DECLARE @bitCustom BIT
DECLARE @numFieldGroupID AS INT
DECLARE @vcColumnName AS VARCHAR(200)
DECLARE @numFieldID AS NUMERIC(18,0)
DECLARE @Prefix AS VARCHAR(10)

	SELECT TOP 1
		@tintOrder=tintOrder,
		@numFieldID=numFormFieldID,
		@vcDbColumnName=vcDbColumnName,
		@vcFormFieldName=vcFieldName,
		@vcAssociatedControlType=vcAssociatedControlType,
		@vcListItemType=vcListItemType,
		@numListID=numListID,
		@Table=vcLookBackTableName,
		@bitCustom=bitCustom,
		@numFieldGroupID=numGroupID
	FROM
	(
		SELECT
			T1.tintOrder+1 tintOrder,
			D.numFieldID AS numFormFieldID,
			vcDbColumnName,
			vcFieldName,
			vcAssociatedControlType,
			vcListItemType,
			numListID,
			vcLookBackTableName,
			0 AS bitCustom,
			0 AS numGroupID
		FROM 
			View_DynamicDefaultColumns D                               
		INNER JOIN	
			@TEMPSelectedColumns T1
		ON
			D.numFieldID=T1.numFieldID
		WHERE 
			D.numDomainID = @numDomainID 
			AND D.numFormID = 18 
			AND ISNULL(T1.bitCustomField,0) = 0
		UNION
		SELECT
			T1.tintOrder+1 tintOrder,
			C.Fld_id AS numFormFieldID,
			CONCAT('Cust',Fld_id),
			Fld_label,
			c.Fld_type,
			'',
			c.numlistid,
			'',
			1 AS bitCustom,
			Grp_id
		FROM    
			CFW_Fld_Master C
		INNER JOIN	
			@TEMPSelectedColumns T1
		ON
			C.Fld_id=T1.numFieldID
		WHERE   
			C.numDomainID = @numDomainID
			AND ISNULL(T1.bitCustomField,0) = 1
	) T1
	ORDER BY 
		tintOrder asc    
                                    
while @tintOrder>0                                      
begin       

	IF @Table = 'AdditionalContactsInformation' 
        SET @Prefix = 'ADC.'
	IF @Table = 'DivisionMaster' 
        SET @Prefix = 'DM.'
    IF @Table = 'CompanyInfo' 
        SET @Prefix = 'C.'
    IF @Table = 'ProjectsMaster' 
        SET @Prefix = 'ProMas.'                               
                                 
   IF @bitCustom = 0
   BEGIN                                    
		if @vcAssociatedControlType='SelectBox'                                      
        begin                                      
                                                      
			if @vcListItemType='LI'                                       
			begin                                      
			set @strSql=@strSql+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcFormFieldName+'~'+ @vcDbColumnName+']'                                      
			set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID='+ (@Prefix + @vcDbColumnName)                                      
			end 
                                     
			if @vcListItemType='U'                                       
			begin                                      
				SET @strSql=@strSql+',dbo.fn_GetContactName(' + @Prefix + @vcDbColumnName+') ['+ @vcFormFieldName+'~'+ @vcDbColumnName+']'
			end 
                                      
			end                                      
		else set @strSql=@strSql+','+ case  when @vcDbColumnName='numAge' then 'year(getutcdate())-year(bintDOB)'       
			when @vcDbColumnName='numDivisionID' then 'DM.numDivisionID'     
			when @vcDbColumnName='vcProgress' then  ' dbo.fn_ProTotalProgress(ProMas.numProId)'    
			when @vcDbColumnName='intTargetResolveDate' or @vcDbColumnName='dtDateEntered' or @vcDbColumnName='bintShippedDate'  then      
			'dbo.FormatedDateFromDate('+ @Prefix + @vcDbColumnName +','+convert(varchar(10),@numDomainId)+')'      
			 else @Prefix + @vcDbColumnName end +' ['+ @vcFormFieldName+'~'+ @vcDbColumnName+']'                   
    END
	ELSE IF @bitCustom = 1
	BEGIN
		SET @vcColumnName= CONCAT(@vcFormFieldName,'~','Cust',@numFieldId)
			
		IF @vcAssociatedControlType = 'TextBox' or @vcAssociatedControlType = 'TextArea'            
		BEGIN
			SET @strSql= @strSql+',CFW'+ convert(VARCHAR(3),@tintOrder)+'.Fld_Value  ['+ @vcColumnName +']'                   
				
			SET @WhereCondition= @WhereCondition 
								+' left Join CFW_FLD_Values_Pro CFW'+ convert(varchar(3),@tintOrder)
								+ ' on CFW' + convert(varchar(3),@tintOrder) 
								+ '.Fld_Id='+convert(varchar(10),@numFieldId) 
								+ 'and CFW' + convert(varchar(3),@tintOrder) 
								+ '.RecId=ProMas.numProID '                                                         
		END   
		ELSE IF @vcAssociatedControlType = 'CheckBox'       
		BEGIN
			set @strSql= @strSql+',case when isnull(CFW' + CONVERT(VARCHAR(3), @tintOrder)+ '.Fld_Value,0)='''' then 0 else  isnull(CFW'
											+ CONVERT(VARCHAR(3), @tintOrder)+ '.Fld_Value,0) end ['+ @vcColumnName + ']'
 
			set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values_Pro CFW'+ convert(varchar(3),@tintOrder)+ '             
			on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=ProMas.numProID '                                                     
		END                
		ELSE IF @vcAssociatedControlType = 'DateField'            
		BEGIN
			set @strSql= @strSql+',dbo.FormatedDateFromDate(CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value,'+convert(varchar(10),@numDomainId)+')  ['+ @vcColumnName +']'                   
			set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values_Pro CFW'+ convert(varchar(3),@tintOrder)+ '                 
			on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=ProMas.numProID '
		END                
		ELSE IF @vcAssociatedControlType = 'SelectBox'             
		BEGIN
			set @vcDbColumnName = 'DCust'+Convert(varchar(10),@numFieldId)                
			set @strSql=@strSql+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName +']'                                                          
			set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values_Pro CFW'+ convert(varchar(3),@tintOrder)+ '                 
				on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=ProMas.numProID '
			set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID=CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value'                
		END         
		ELSE IF @vcAssociatedControlType = 'CheckBoxList'
		BEGIN
			SET @strSql= @strSql+',(SELECT STUFF((SELECT CONCAT('', '', vcData) FROM ListDetails WHERE numListID= ' + CAST(@numListID AS varchar) + ' AND numlistitemid IN (SELECT ISNULL(Id,0) FROM dbo.SplitIDs(CFW'+ convert(VARCHAR(3),@tintOrder)+'.Fld_Value,'','')) FOR XML PATH('''')), 1, 1, ''''))  ['+ @vcColumnName +']'   

			SET @WhereCondition= @WhereCondition 
								+' left Join CFW_FLD_Values_Pro CFW'+ convert(varchar(3),@tintOrder)
								+ ' on CFW' + convert(varchar(3),@tintOrder) 
								+ '.Fld_Id='+convert(varchar(10),@numFieldId) 
								+ 'and CFW' + convert(varchar(3),@tintOrder) 
								+ '.RecId=ProMas.numProID '
		END
	END    
          
                
             
                
                  
                       
                                              
 SELECT TOP 1
		@tintOrder=tintOrder,
		@numFieldID=numFormFieldID,
		@vcDbColumnName=vcDbColumnName,
		@vcFormFieldName=vcFieldName,
		@vcAssociatedControlType=vcAssociatedControlType,
		@vcListItemType=vcListItemType,
		@numListID=numListID,
		@Table=vcLookBackTableName,
		@bitCustom=bitCustom,
		@numFieldGroupID=numGroupID
	FROM
	(
		SELECT
			T1.tintOrder+1 tintOrder,
			D.numFieldID AS numFormFieldID,
			vcDbColumnName,
			vcFieldName,
			vcAssociatedControlType,
			vcListItemType,
			numListID,
			vcLookBackTableName,
			0 AS bitCustom,
			0 AS numGroupID
		FROM 
			View_DynamicDefaultColumns D                               
		INNER JOIN	
			@TEMPSelectedColumns T1
		ON
			D.numFieldID=T1.numFieldID
		WHERE 
			D.numDomainID = @numDomainID 
			AND D.numFormID = 18 
			AND ISNULL(T1.bitCustomField,0) = 0
			AND T1.tintOrder > @tintOrder-1
		UNION
		SELECT
			T1.tintOrder+1 tintOrder,
			C.Fld_id AS numFormFieldID,
			CONCAT('Cust',Fld_id),
			Fld_label,
			c.Fld_type,
			'',
			c.numlistid,
			'',
			1 AS bitCustom,
			Grp_id
		FROM    
			CFW_Fld_Master C
		INNER JOIN	
			@TEMPSelectedColumns T1
		ON
			C.Fld_id=T1.numFieldID
		WHERE   
			C.numDomainID = @numDomainID
			AND ISNULL(T1.bitCustomField,0) = 1
			AND T1.tintOrder > @tintOrder-1
	) T1
	ORDER BY 
		tintOrder asc                                              

 if @@rowcount=0 set @tintOrder=0                                      
end       
  declare @firstRec as integer                                                                            
  declare @lastRec as integer                                                                            
 set @firstRec= (@CurrentPage-1) * @PageSize                              
 set @lastRec= (@CurrentPage*@PageSize+1)                                                                             
set @TotRecs=(select count(*) from #tempTable)       
set @strSql=@strSql+'from ProjectsMaster ProMas join DivisionMaster DM on Dm.numDivisionId = ProMas.numDivisionId    
JOIN CompanyInfo C ON DM.numCompanyID = C.numCompanyId
LEFT JOIN AdditionalContactsinformation ADC on ADC.numContactId = ProMas.numIntPrjMgr     
 '+@WhereCondition+'      
      
 join #tempTable T on T.numProID=ProMas.numProID'    

	IF ISNULL(@GetAll,0) = 0
	BEGIN
		SET @strSql = @strSql + ' WHERE ID > '+convert(varchar(10),@firstRec)+ ' and ID <'+ convert(varchar(10),@lastRec)   
	END

                        
 print @strSql                                                              
exec(@strSql)                                                                         
drop table #tempTable
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_CategoryMatrixGroup_GetByCategoryID')
DROP PROCEDURE dbo.USP_CategoryMatrixGroup_GetByCategoryID
GO
CREATE PROCEDURE [dbo].[USP_CategoryMatrixGroup_GetByCategoryID]
(
	@numDomainID NUMERIC(18,0)
	,@numCategoryID NUMERIC(18,0)
)
AS 
BEGIN
	SELECT * FROM CategoryMatrixGroup WHERE numDomainID=@numDomainID AND numCategoryID=@numCategoryID
END
GO
--Created by anoop jayaraj                                                          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_checkuseratlogin')
DROP PROCEDURE usp_checkuseratlogin
GO
CREATE PROCEDURE [dbo].[USP_CheckUserAtLogin]                                                              
(                                                                                    
	@UserName as varchar(100)='',                                                                        
	@vcPassword as varchar(100)='',
	@vcLinkedinId as varchar(300)='',
	@numDomainID NUMERIC(18,0) = 0
)                                                              
AS             
BEGIN
	DECLARE @listIds VARCHAR(MAX)

	SELECT 
		@listIds = COALESCE(@listIds+',' ,'') + CAST(UDTL.numUserId AS VARCHAR(100)) 
	FROM 
		UnitPriceApprover U
	Join 
		Domain D                              
	on 
		D.numDomainID=U.numDomainID
	Join  
		Subscribers S                            
	on 
		S.numTargetDomainID=D.numDomainID
	LEFT JOIN 
		UserMaster As UDTL
	ON
		U.numUserID=UDTL.numUserDetailId
	WHERE 
		1 = (CASE 
				WHEN @vcLinkedinId='N' THEN (CASE WHEN UDTL.vcEmailID=@UserName and UDTL.vcPassword=@vcPassword THEN 1 ELSE 0 END)
				ELSE (CASE WHEN UDTL.vcLinkedinId=@vcLinkedinId THEN 1 ELSE 0 END)
			END)
       

	DECLARE @bitIsBusinessPortalUser AS BIT = 0
	DECLARE @numUserCount INT = 0 

	-- If request came from domain url so first check whether external user with username and password exists
	IF ISNULL(@numDomainID,0) > 0 
	BEGIN
		SET @numUserCount = ISNULL((SELECT 
										COUNT(*)
									FROM 
										AdditionalContactsInformation A                                  
									INNER JOIN 
										ExtranetAccountsDtl E                                  
									ON 
										A.numContactID=E.numContactID
									INNER JOIN
										ExtarnetAccounts EA
									ON
										E.numExtranetID = EA.numExtranetID
									WHERE 
										A.numDomainID = @numDomainID
										AND EA.numDomainID = @numDomainID
										AND (E.vcUserName=@UserName OR vcemail=@UserName) 
										AND (vcPassword=@vcPassword) 
										AND bitPartnerAccess=1),0)

		/*check subscription validity */
		IF EXISTS(SELECT 
						*
					FROM 
						AdditionalContactsInformation A                                  
					INNER JOIN 
						ExtranetAccountsDtl E                                  
					ON 
						A.numContactID=E.numContactID
					INNER JOIN
						ExtarnetAccounts EA
					ON
						E.numExtranetID = EA.numExtranetID
					INNER JOIN
						Domain D
					ON
						EA.numDomainID = D.numDomainId
					INNER JOIN 
						Subscribers S                            
					ON 
						S.numTargetDomainID=D.numDomainID
					WHERE 
						A.numDomainID = @numDomainID
						AND EA.numDomainID = @numDomainID
						AND (E.vcUserName=@UserName OR vcemail=@UserName) 
						AND (vcPassword=@vcPassword) 
						AND bitPartnerAccess=1 
						AND bitActive=1 
						AND getutcdate() > dtSubEndDate)
		BEGIN
			RAISERROR('SUB_RENEW',16,1)
			RETURN;
		END

		SET @bitIsBusinessPortalUser = 1
	END

	-- If no external user found then check for regular biz users
	IF ISNULL(@numUserCount,0) = 0
	BEGIN
		SET @numUserCount = ISNULL((SELECT 
									COUNT(*)
								FROM 
									UserMaster U                              
								JOIN 
									Domain D                              
								ON 
									D.numDomainID=U.numDomainID
								JOIN 
									Subscribers S                            
								ON 
									S.numTargetDomainID=D.numDomainID
								WHERE 
									1 = (CASE 
											WHEN @vcLinkedinId='N' 
											THEN (CASE WHEN vcEmailID=@UserName and vcPassword=@vcPassword THEN 1 ELSE 0 END)
											ELSE (CASE WHEN vcLinkedinId=@vcLinkedinId THEN 1 ELSE 0 END)
										END) 
									AND bitActive=1),0)

		SET @bitIsBusinessPortalUser = 0

		/*check subscription validity */
		IF EXISTS(SELECT 
					*
				FROM 
					UserMaster U                              
				JOIN 
					Domain D                              
				ON 
					D.numDomainID=U.numDomainID
				JOIN 
					Subscribers S                            
				ON 
					S.numTargetDomainID=D.numDomainID
				WHERE 
					1 = (CASE 
							WHEN @vcLinkedinId='N' THEN (CASE WHEN vcEmailID=@UserName and vcPassword=@vcPassword THEN 1 ELSE 0 END)
							ELSE (CASE WHEN vcLinkedinId=@vcLinkedinId THEN 1 ELSE 0 END)
						END) 
					AND bitActive=1 
					AND getutcdate() > dtSubEndDate)
		BEGIN
			RAISERROR('SUB_RENEW',16,1)
			RETURN;
		END
	END

	IF @numUserCount > 1
	BEGIN
		RAISERROR('MULTIPLE_RECORDS_WITH_SAME_EMAIL',16,1)
		RETURN;
	END   

	IF @bitIsBusinessPortalUser = 1
	BEGIN
		SELECT TOP 1 
			0 numUserID
			,EAD.numExtranetDtlID
			,A.numContactId AS numUserDetailId
			,D.numCost
			,isnull(A.vcEmail,'') vcEmailID
			,'' vcEmailAlias
			,'' vcEmailAliasPassword
			,isnull(numGroupID,0) numGroupID
			,1 AS bitActivateFlag
			,'' vcMailNickName
			,'' txtSignature
			,isnull(EAD.numDomainID,0) numDomainID
			,isnull(EA.numDivisionID,0) numDivisionID
			,isnull(vcCompanyName,'') vcCompanyName
			,0 bitExchangeIntegration
			,0 bitAccessExchange
			,'' vcExchPath
			,'' vcExchDomain
			,'' vcExchUserName
			,isnull(vcFirstname,'') + ' ' + isnull(vcLastName,'') as ContactName
			,'' as vcExchPassword
			,tintCustomPagingRows
			,vcDateFormat
			,numDefCountry
			,tintComposeWindow
			,dateadd(day,-sintStartDate,getutcdate()) as StartDate
			,dateadd(day,sintNoofDaysInterval,dateadd(day,-sintStartDate,getutcdate())) as EndDate
			,tintAssignToCriteria
			,bitIntmedPage
			,tintFiscalStartMonth
			,numAdminID
			,isnull(A.numTeam,0) numTeam
			,isnull(D.vcCurrency,'') as vcCurrency
			,isnull(D.numCurrencyID,0) as numCurrencyID, 
			isnull(D.bitMultiCurrency,0) as bitMultiCurrency,              
			bitTrial,isnull(tintChrForComSearch,0) as tintChrForComSearch,  
			isnull(tintChrForItemSearch,0) as tintChrForItemSearch,              
			'' as vcSMTPServer  ,          
			0 as numSMTPPort      
			,0 bitSMTPAuth    
			,'' vcSmtpPassword    
			,0 bitSMTPSSL
			,0 bitSMTPServer,       
			0 bitImapIntegration, isnull(charUnitSystem,'E')   UnitSystem, datediff(day,getutcdate(),dtSubEndDate) as NoOfDaysLeft,
			ISNULL(bitCreateInvoice,0) bitCreateInvoice,
			ISNULL([numDefaultSalesBizDocId],0) numDefaultSalesBizDocId,
			ISNULL([numDefaultPurchaseBizDocId],0) numDefaultPurchaseBizDocId,
			vcDomainName,
			S.numSubscriberID,
			D.[tintBaseTax],
			'' ProfilePic,
			ISNULL(D.[numShipCompany],0) numShipCompany,
			ISNULL(D.[bitEmbeddedCost],0) bitEmbeddedCost,
			(Select isnull(numAuthoritativeSales,0) From AuthoritativeBizDocs Where numDomainId=D.numDomainID) numAuthoritativeSales,
			(Select isnull(numAuthoritativePurchase,0) From AuthoritativeBizDocs Where numDomainId=D.numDomainID) numAuthoritativePurchase,
			ISNULL(D.[numDefaultSalesOppBizDocId],0) numDefaultSalesOppBizDocId,
			ISNULL(tintDecimalPoints,2) tintDecimalPoints,
			ISNULL(D.tintBillToForPO,0) tintBillToForPO, 
			ISNULL(D.tintShipToForPO,0) tintShipToForPO,
			ISNULL(D.tintOrganizationSearchCriteria,0) tintOrganizationSearchCriteria,
			ISNULL(D.tintLogin,0) tintLogin,
			ISNULL(S.intFullUserConcurrency,0) intFullUserConcurrency,
			ISNULL(bitDocumentRepositary,0) bitDocumentRepositary,
			ISNULL(D.vcPortalName,'') vcPortalName,
			(SELECT COUNT(*) FROM dbo.Subscribers WHERE getutcdate() between dtSubStartDate and dtSubEndDate and bitActive=1),
			ISNULL(D.bitGtoBContact,0) bitGtoBContact,
			ISNULL(D.bitBtoGContact,0) bitBtoGContact,
			ISNULL(D.bitGtoBCalendar,0) bitGtoBCalendar,
			ISNULL(D.bitBtoGCalendar,0) bitBtoGCalendar,
			ISNULL(bitExpenseNonInventoryItem,0) bitExpenseNonInventoryItem,
			ISNULL(D.bitInlineEdit,0) bitInlineEdit,
			0 numDefaultClass,
			ISNULL(D.bitRemoveVendorPOValidation,0) bitRemoveVendorPOValidation,
			ISNULL(D.bitTrakDirtyForm,1) bitTrakDirtyForm,
			isnull(D.tintBaseTaxOnArea,0) tintBaseTaxOnArea,
			ISNULL(D.bitAmountPastDue,0) bitAmountPastDue,
			CONVERT(DECIMAL(18,2),ISNULL(D.monAmountPastDue,0)) monAmountPastDue,
			ISNULL(D.bitSearchOrderCustomerHistory,0) bitSearchOrderCustomerHistory,
			0 as tintTabEvent,
			isnull(D.bitRentalItem,0) as bitRentalItem,
			isnull(D.numRentalItemClass,0) as numRentalItemClass,
			isnull(D.numRentalHourlyUOM,0) as numRentalHourlyUOM,
			isnull(D.numRentalDailyUOM,0) as numRentalDailyUOM,
			isnull(D.tintRentalPriceBasedOn,0) as tintRentalPriceBasedOn,
			isnull(D.tintCalendarTimeFormat,24) as tintCalendarTimeFormat,
			isnull(D.tintDefaultClassType,0) as tintDefaultClassType,
			0 numDefaultWarehouse,
			ISNULL(D.tintPriceBookDiscount,0) tintPriceBookDiscount,
			ISNULL(D.bitAutoSerialNoAssign,0) bitAutoSerialNoAssign,
			ISNULL(IsEnableProjectTracking,0) AS [IsEnableProjectTracking],
			ISNULL(IsEnableCampaignTracking,0) AS [IsEnableCampaignTracking],
			ISNULL(IsEnableResourceScheduling,0) AS [IsEnableResourceScheduling],
			ISNULL(D.numSOBizDocStatus,0) AS numSOBizDocStatus,
			ISNULL(D.bitAutolinkUnappliedPayment,0) AS bitAutolinkUnappliedPayment,
			ISNULL(numShippingServiceItemID,0) AS [numShippingServiceItemID],
			ISNULL(D.numDiscountServiceItemID,0) AS numDiscountServiceItemID,
			ISNULL(D.tintCommissionType,1) AS tintCommissionType,
			ISNULL(D.bitDiscountOnUnitPrice,0) AS bitDiscountOnUnitPrice,
			ISNULL(D.intShippingImageWidth,0) AS intShippingImageWidth ,
			ISNULL(D.intShippingImageHeight,0) AS intShippingImageHeight,
			ISNULL(D.numTotalInsuredValue,0) AS numTotalInsuredValue,
			ISNULL(D.numTotalCustomsValue,0) AS numTotalCustomsValue,
			ISNULL(D.bitUseBizdocAmount,1) AS [bitUseBizdocAmount],
			ISNULL(D.bitDefaultRateType,1) AS [bitDefaultRateType],
			ISNULL(D.bitIncludeTaxAndShippingInCommission,1) AS [bitIncludeTaxAndShippingInCommission],
			ISNULL(D.[bitLandedCost],0) AS bitLandedCost,
			ISNULL(D.vcLanedCostDefault,'') AS vcLanedCostDefault,
			ISNULL(D.bitMinUnitPriceRule,0) AS bitMinUnitPriceRule,
			ISNULL(@listIds,'') AS vcUnitPriceApprover,
			ISNULL(D.numPODropShipBizDoc,0) AS numPODropShipBizDoc,
			ISNULL(D.numPODropShipBizDocTemplate,0) AS numPODropShipBizDocTemplate,
			ISNULL(D.IsEnableDeferredIncome,0) AS IsEnableDeferredIncome,
			ISNULL(D.bitEnableItemLevelUOM,0) AS bitEnableItemLevelUOM,
			ISNULL(D.bitPurchaseTaxCredit,0) AS bitPurchaseTaxCredit,
			NULL AS bintCreatedDate,
			(CASE WHEN LEN(ISNULL(TempDefaultPage.vcDefaultNavURL,'')) = 0 THEN ISNULL(TempDefaultTab.vcDefaultNavURL,'Items/frmItemList.aspx?Page=All Items&ItemGroup=0') ELSE ISNULL(TempDefaultPage.vcDefaultNavURL,'') END) vcDefaultNavURL,
			ISNULL(D.bitApprovalforTImeExpense,0) AS bitApprovalforTImeExpense,
			ISNULL(D.intTimeExpApprovalProcess,0) AS intTimeExpApprovalProcess,ISNULL(D.bitApprovalforOpportunity,0) AS bitApprovalforOpportunity,
			ISNULL(D.intOpportunityApprovalProcess,0) AS intOpportunityApprovalProcess,
			ISNULL(D.numDefaultSalesShippingDoc,0) AS numDefaultSalesShippingDoc,
			ISNULL(D.numDefaultPurchaseShippingDoc,0) AS numDefaultPurchaseShippingDoc,
			ISNULL((SELECT TOP 1 bitMarginPriceViolated FROM ApprovalProcessItemsClassification  WHERE numDomainID=D.numDomainID AND ISNULL(bitMarginPriceViolated,0) = 1),0) AS bitMarginPriceViolated,
			ISNULL((SELECT TOP 1 intViewAllowed FROM GroupAuthorization WHERE numGroupID=EA.numGroupID AND numModuleID=2 AND numPageID=2),0) AS tintLeadRights,
			ISNULL((SELECT TOP 1 intViewAllowed FROM GroupAuthorization WHERE numGroupID=EA.numGroupID AND numModuleID=3 AND numPageID=2),0) AS tintProspectRights,
			ISNULL((SELECT TOP 1 intViewAllowed FROM GroupAuthorization WHERE numGroupID=EA.numGroupID AND numModuleID=4 AND numPageID=2),0) AS tintAccountRights,
			ISNULL((SELECT TOP 1 intViewAllowed FROM GroupAuthorization WHERE numGroupID=EA.numGroupID AND numModuleID=10 AND numPageID=10),0) AS tintSalesOrderListRights,
			ISNULL((SELECT TOP 1 intViewAllowed FROM GroupAuthorization WHERE numGroupID=EA.numGroupID AND numModuleID=10 AND numPageID=11),0) AS tintPurchaseOrderListRights,
			ISNULL((SELECT TOP 1 intViewAllowed FROM GroupAuthorization WHERE numGroupID=EA.numGroupID AND numModuleID=10 AND numPageID=2),0) AS tintSalesOpportunityListRights,
			ISNULL((SELECT TOP 1 intViewAllowed FROM GroupAuthorization WHERE numGroupID=EA.numGroupID AND numModuleID=10 AND numPageID=8),0) AS tintPurchaseOpportunityListRights,
			ISNULL((SELECT TOP 1 intViewAllowed FROM GroupAuthorization WHERE numGroupID=EA.numGroupID AND numModuleID=11 AND numPageID=2),0) AS tintContactListRights,
			ISNULL((SELECT TOP 1 intViewAllowed FROM GroupAuthorization WHERE numGroupID=EA.numGroupID AND numModuleID=7 AND numPageID=2),0) AS tintCaseListRights,
			ISNULL((SELECT TOP 1 intViewAllowed FROM GroupAuthorization WHERE numGroupID=EA.numGroupID AND numModuleID=12 AND numPageID=2),0) AS tintProjectListRights,
			ISNULL((SELECT TOP 1 numCriteria FROM TicklerListFilterConfiguration WHERE CHARINDEX(CONCAT(',',A.numContactId,','),CONCAT(',',vcSelectedEmployee,',')) > 0),0) AS tintActionItemRights,
			ISNULL(D.numAuthorizePercentage,0) AS numAuthorizePercentage,
			ISNULL(D.bitAllowDuplicateLineItems,0) bitAllowDuplicateLineItems,
			ISNULL(D.bitLogoAppliedToBizTheme,0) bitLogoAppliedToBizTheme,
			ISNULL(D.vcLogoForBizTheme,'') vcLogoForBizTheme,
			ISNULL(D.bitLogoAppliedToLoginBizTheme,0) bitLogoAppliedToLoginBizTheme,
			ISNULL(D.vcLogoForLoginBizTheme,'') vcLogoForLoginBizTheme,
			ISNULL(D.vcThemeClass,'1473b4') vcThemeClass,
			ISNULL(D.vcLoginURL,'') AS vcLoginURL,
			ISNULL(D.bitDisplayCustomField,0) AS bitDisplayCustomField,
			ISNULL(D.bitFollowupAnytime,0) AS bitFollowupAnytime,
			ISNULL(D.bitpartycalendarTitle,0) AS bitpartycalendarTitle,
			ISNULL(D.bitpartycalendarLocation,0) AS bitpartycalendarLocation,
			ISNULL(D.bitpartycalendarDescription,0) AS bitpartycalendarDescription,
			ISNULL(D.bitREQPOApproval,0) AS bitREQPOApproval,
			ISNULL(D.bitARInvoiceDue,0) AS bitARInvoiceDue,
			ISNULL(D.bitAPBillsDue,0) AS bitAPBillsDue,
			ISNULL(D.bitItemsToPickPackShip,0) AS bitItemsToPickPackShip,
			ISNULL(D.bitItemsToInvoice,0) AS bitItemsToInvoice,
			ISNULL(D.bitSalesOrderToClose,0) AS bitSalesOrderToClose,
			ISNULL(D.bitItemsToPutAway,0) AS bitItemsToPutAway,
			ISNULL(D.bitItemsToBill,0) AS bitItemsToBill,
			ISNULL(D.bitPosToClose,0) AS bitPosToClose,
			ISNULL(D.bitPOToClose,0) AS bitPOToClose,
			ISNULL(D.bitBOMSToPick,0) AS bitBOMSToPick,
			ISNULL(D.vchREQPOApprovalEmp,'') AS vchREQPOApprovalEmp,
			ISNULL(D.vchARInvoiceDue,'') AS vchARInvoiceDue,
			ISNULL(D.vchAPBillsDue,'') AS vchAPBillsDue,
			ISNULL(D.vchItemsToPickPackShip,'') AS vchItemsToPickPackShip,
			ISNULL(D.vchItemsToInvoice,'') AS vchItemsToInvoice,
			ISNULL(stuff((
			select ',' + CAST(UPC.numUserID AS VARCHAR(10))
			from UnitPriceApprover UPC
			where UPC.numDomainID = D.numDomainId
			order by UPC.numUserID
			for xml path('')),1,1,''),'') AS vchPriceMarginApproval,
			ISNULL(D.vchSalesOrderToClose,'') AS vchSalesOrderToClose,
			ISNULL(D.vchItemsToPutAway,'') AS vchItemsToPutAway,
			ISNULL(D.vchItemsToBill,'') AS vchItemsToBill,
			ISNULL(D.vchPosToClose,'') AS vchPosToClose,
			ISNULL(D.vchPOToClose,'') AS vchPOToClose,
			ISNULL(D.vchBOMSToPick,'') AS vchBOMSToPick,
			ISNULL(D.decReqPOMinValue,0) AS decReqPOMinValue,
			0 tintPayrollType,
			(CASE WHEN EXISTS (SELECT numCategoryHDRID FROM TimeAndExpense WHERE numUserCntID=A.numContactId AND numCategory=1 AND numType=7 AND dtToDate IS NULL) THEN 1 ELSE 0 END) bitUserClockedIn,
			ISNULL(D.bitUseOnlyActionItems,0) AS bitUseOnlyActionItems,
			ISNULL(D.vcElectiveItemFields,'') AS vcElectiveItemFields,
			ISNULL(D.bitDisplayContractElement,0) AS bitDisplayContractElement,
			ISNULL(D.vcEmployeeForContractTimeElement,'') AS vcEmployeeForContractTimeElement,
			ISNULL(D.bitEnableSmartyStreets,0) bitEnableSmartyStreets,
			ISNULL(D.vcSmartyStreetsAPIKeys,'') vcSmartyStreetsAPIKeys
		FROM 
			ExtranetAccountsDtl EAD
		INNER JOIN
			ExtarnetAccounts EA
		ON
			EAD.numExtranetID = EA.numExtranetID                                                
		INNER JOIN 
			Domain D                              
		ON 
			D.numDomainID=EAD.numDomainID
		INNER JOIN 
			AdditionalContactsInformation A                              
		ON 
			A.numContactID=EAD.numContactID
		INNER JOIN 
			Subscribers S            
		ON 
			S.numTargetDomainID=D.numDomainID 
		LEFT JOIN 
			DivisionMaster Div                                            
		ON 
			EA.numDivisionID=Div.numDivisionID                               
		LEFT JOIN 
			CompanyInfo C                              
		ON 
			C.numCompanyID=Div.numCompanyID    
		OUTER APPLY
		(
			SELECT  
				TOP 1 REPLACE(SCB.Link,'../','') vcDefaultNavURL
			FROM    
				TabMaster T
			JOIN 
				GroupTabDetails G ON G.numTabId = T.numTabId
			LEFT JOIN
				ShortCutGrpConf SCUC ON G.numTabId=SCUC.numTabId AND SCUC.numDomainId=EAD.numDomainID AND SCUC.numGroupId=EA.numGroupID AND bitDefaultTab=1
			LEFT JOIN
				ShortCutBar SCB
			ON
				SCB.Id=SCUC.numLinkId
			WHERE   
				(T.numDomainID = EAD.numDomainID OR bitFixed = 1)
				AND G.numGroupID = EA.numGroupID
				AND ISNULL(G.[tintType], 0) <> 1
				AND tintTabType =1
				AND T.numTabID NOT IN (2,68)
			ORDER BY 
				SCUC.bitInitialPage DESC
		)  TempDefaultPage 
		 OUTER APPLY
		 (
			SELECT  
				TOP 1 REPLACE(T.vcURL,'../','') vcDefaultNavURL
			FROM    
				TabMaster T
			JOIN 
				GroupTabDetails G 
			ON 
				G.numTabId = T.numTabId
			WHERE   
				(T.numDomainID = EAD.numDomainID OR bitFixed = 1)
				AND G.numGroupID = EA.numGroupID
				AND ISNULL(G.[tintType], 0) <> 1
				AND tintTabType =1
				AND T.numTabID NOT IN (2,68)
				AND ISNULL(G.bitInitialTab,0) = 1 
				AND ISNULL(bitallowed,0)=1
			ORDER BY 
				G.bitInitialTab DESC
		 ) TEMPDefaultTab                   
		WHERE 
			(EAD.vcUserName=@UserName OR A.vcemail=@UserName) 
			AND (vcPassword=@vcPassword) 
			AND bitPartnerAccess=1 
			AND GETUTCDATE() BETWEEN dtSubStartDate and dtSubEndDate
	END
	ELSE
	BEGIN
	SELECT TOP 1 
		U.numUserID
		,numUserDetailId
		,D.numCost
		,isnull(vcEmailID,'') vcEmailID
		,ISNULL(vcEmailAlias,'') vcEmailAlias
		,ISNULL(vcEmailAliasPassword,'') vcEmailAliasPassword
		,isnull(numGroupID,0) numGroupID
		,bitActivateFlag
		,isnull(vcMailNickName,'') vcMailNickName
		,isnull(txtSignature,'') txtSignature
		,isnull(U.numDomainID,0) numDomainID
		,isnull(Div.numDivisionID,0) numDivisionID
		,isnull(vcCompanyName,'') vcCompanyName
		,isnull(E.bitExchangeIntegration,0) bitExchangeIntegration
		,isnull(E.bitAccessExchange,0) bitAccessExchange
		,isnull(E.vcExchPath,'') vcExchPath
		,isnull(E.vcExchDomain,'') vcExchDomain
		,isnull(D.vcExchUserName,'') vcExchUserName
		,isnull(vcFirstname,'') + ' ' + isnull(vcLastName,'') as ContactName
		,Case when E.bitAccessExchange=0 then  E.vcExchPassword when E.bitAccessExchange=1 then D.vcExchPassword end as vcExchPassword
		,tintCustomPagingRows
		,vcDateFormat
		,numDefCountry
		,tintComposeWindow
		,dateadd(day,-sintStartDate,getutcdate()) as StartDate
		,dateadd(day,sintNoofDaysInterval,dateadd(day,-sintStartDate,getutcdate())) as EndDate
		,tintAssignToCriteria
		,bitIntmedPage
		,tintFiscalStartMonth
		,numAdminID
		,isnull(A.numTeam,0) numTeam
		,isnull(D.vcCurrency,'') as vcCurrency
		,isnull(D.numCurrencyID,0) as numCurrencyID, 
		isnull(D.bitMultiCurrency,0) as bitMultiCurrency,              
		bitTrial,isnull(tintChrForComSearch,0) as tintChrForComSearch,  
		isnull(tintChrForItemSearch,0) as tintChrForItemSearch,              
		case when bitSMTPServer= 1 then vcSMTPServer else '' end as vcSMTPServer  ,          
		case when bitSMTPServer= 1 then  isnull(numSMTPPort,0)  else 0 end as numSMTPPort      
		,isnull(bitSMTPAuth,0) bitSMTPAuth    
		,isnull([vcSmtpPassword],'') vcSmtpPassword    
		,isnull(bitSMTPSSL,0) bitSMTPSSL   ,isnull(bitSMTPServer,0) bitSMTPServer,       
		isnull(IM.bitImap,0) bitImapIntegration, isnull(charUnitSystem,'E')   UnitSystem, datediff(day,getutcdate(),dtSubEndDate) as NoOfDaysLeft,
		ISNULL(bitCreateInvoice,0) bitCreateInvoice,
		ISNULL([numDefaultSalesBizDocId],0) numDefaultSalesBizDocId,
		ISNULL([numDefaultPurchaseBizDocId],0) numDefaultPurchaseBizDocId,
		vcDomainName,
		S.numSubscriberID,
		D.[tintBaseTax],
		u.ProfilePic,
		ISNULL(D.[numShipCompany],0) numShipCompany,
		ISNULL(D.[bitEmbeddedCost],0) bitEmbeddedCost,
		(Select isnull(numAuthoritativeSales,0) From AuthoritativeBizDocs Where numDomainId=D.numDomainID) numAuthoritativeSales,
		(Select isnull(numAuthoritativePurchase,0) From AuthoritativeBizDocs Where numDomainId=D.numDomainID) numAuthoritativePurchase,
		ISNULL(D.[numDefaultSalesOppBizDocId],0) numDefaultSalesOppBizDocId,
		ISNULL(tintDecimalPoints,2) tintDecimalPoints,
		ISNULL(D.tintBillToForPO,0) tintBillToForPO, 
		ISNULL(D.tintShipToForPO,0) tintShipToForPO,
		ISNULL(D.tintOrganizationSearchCriteria,0) tintOrganizationSearchCriteria,
		ISNULL(D.tintLogin,0) tintLogin,
			ISNULL(S.intFullUserConcurrency,0) intFullUserConcurrency,
		ISNULL(bitDocumentRepositary,0) bitDocumentRepositary,
		ISNULL(D.vcPortalName,'') vcPortalName,
		(SELECT COUNT(*) FROM dbo.Subscribers WHERE getutcdate() between dtSubStartDate and dtSubEndDate and bitActive=1),
		ISNULL(D.bitGtoBContact,0) bitGtoBContact,
		ISNULL(D.bitBtoGContact,0) bitBtoGContact,
		ISNULL(D.bitGtoBCalendar,0) bitGtoBCalendar,
		ISNULL(D.bitBtoGCalendar,0) bitBtoGCalendar,
		ISNULL(bitExpenseNonInventoryItem,0) bitExpenseNonInventoryItem,
		ISNULL(D.bitInlineEdit,0) bitInlineEdit,
		ISNULL(U.numDefaultClass,0) numDefaultClass,
		ISNULL(D.bitRemoveVendorPOValidation,0) bitRemoveVendorPOValidation,
		ISNULL(D.bitTrakDirtyForm,1) bitTrakDirtyForm,
		isnull(D.tintBaseTaxOnArea,0) tintBaseTaxOnArea,
		ISNULL(D.bitAmountPastDue,0) bitAmountPastDue,
		CONVERT(DECIMAL(18,2),ISNULL(D.monAmountPastDue,0)) monAmountPastDue,
		ISNULL(D.bitSearchOrderCustomerHistory,0) bitSearchOrderCustomerHistory,
		isnull(U.tintTabEvent,0) as tintTabEvent,
		isnull(D.bitRentalItem,0) as bitRentalItem,
		isnull(D.numRentalItemClass,0) as numRentalItemClass,
		isnull(D.numRentalHourlyUOM,0) as numRentalHourlyUOM,
		isnull(D.numRentalDailyUOM,0) as numRentalDailyUOM,
		isnull(D.tintRentalPriceBasedOn,0) as tintRentalPriceBasedOn,
		isnull(D.tintCalendarTimeFormat,24) as tintCalendarTimeFormat,
		isnull(D.tintDefaultClassType,0) as tintDefaultClassType,
		ISNULL(U.numDefaultWarehouse,0) numDefaultWarehouse,
		ISNULL(D.tintPriceBookDiscount,0) tintPriceBookDiscount,
		ISNULL(D.bitAutoSerialNoAssign,0) bitAutoSerialNoAssign,
		ISNULL(IsEnableProjectTracking,0) AS [IsEnableProjectTracking],
		ISNULL(IsEnableCampaignTracking,0) AS [IsEnableCampaignTracking],
		ISNULL(IsEnableResourceScheduling,0) AS [IsEnableResourceScheduling],
		ISNULL(D.numSOBizDocStatus,0) AS numSOBizDocStatus,
		ISNULL(D.bitAutolinkUnappliedPayment,0) AS bitAutolinkUnappliedPayment,
		ISNULL(numShippingServiceItemID,0) AS [numShippingServiceItemID],
		ISNULL(D.numDiscountServiceItemID,0) AS numDiscountServiceItemID,
		ISNULL(D.tintCommissionType,1) AS tintCommissionType,
		ISNULL(D.bitDiscountOnUnitPrice,0) AS bitDiscountOnUnitPrice,
		ISNULL(D.intShippingImageWidth,0) AS intShippingImageWidth ,
		ISNULL(D.intShippingImageHeight,0) AS intShippingImageHeight,
		ISNULL(D.numTotalInsuredValue,0) AS numTotalInsuredValue,
		ISNULL(D.numTotalCustomsValue,0) AS numTotalCustomsValue,
		ISNULL(D.bitUseBizdocAmount,1) AS [bitUseBizdocAmount],
		ISNULL(D.bitDefaultRateType,1) AS [bitDefaultRateType],
		ISNULL(D.bitIncludeTaxAndShippingInCommission,1) AS [bitIncludeTaxAndShippingInCommission],
		ISNULL(D.[bitLandedCost],0) AS bitLandedCost,
		ISNULL(D.vcLanedCostDefault,'') AS vcLanedCostDefault,
		ISNULL(D.bitMinUnitPriceRule,0) AS bitMinUnitPriceRule,
		ISNULL(@listIds,'') AS vcUnitPriceApprover,
		ISNULL(D.numPODropShipBizDoc,0) AS numPODropShipBizDoc,
		ISNULL(D.numPODropShipBizDocTemplate,0) AS numPODropShipBizDocTemplate,
		ISNULL(D.IsEnableDeferredIncome,0) AS IsEnableDeferredIncome,
		ISNULL(D.bitEnableItemLevelUOM,0) AS bitEnableItemLevelUOM,
		ISNULL(D.bitPurchaseTaxCredit,0) AS bitPurchaseTaxCredit,
		ISNULL(U.bintCreatedDate,0) AS bintCreatedDate,
		(CASE WHEN LEN(ISNULL(TempDefaultPage.vcDefaultNavURL,'')) = 0 THEN ISNULL(TempDefaultTab.vcDefaultNavURL,'Items/frmItemList.aspx?Page=All Items&ItemGroup=0') ELSE ISNULL(TempDefaultPage.vcDefaultNavURL,'') END) vcDefaultNavURL,
		ISNULL(D.bitApprovalforTImeExpense,0) AS bitApprovalforTImeExpense,
		ISNULL(D.intTimeExpApprovalProcess,0) AS intTimeExpApprovalProcess,ISNULL(D.bitApprovalforOpportunity,0) AS bitApprovalforOpportunity,
		ISNULL(D.intOpportunityApprovalProcess,0) AS intOpportunityApprovalProcess,
		ISNULL(D.numDefaultSalesShippingDoc,0) AS numDefaultSalesShippingDoc,
		ISNULL(D.numDefaultPurchaseShippingDoc,0) AS numDefaultPurchaseShippingDoc,
		ISNULL((SELECT TOP 1 bitMarginPriceViolated FROM ApprovalProcessItemsClassification  WHERE numDomainID=D.numDomainID AND ISNULL(bitMarginPriceViolated,0) = 1),0) AS bitMarginPriceViolated,
		ISNULL((SELECT TOP 1 intViewAllowed FROM GroupAuthorization WHERE numGroupID=U.numGroupID AND numModuleID=2 AND numPageID=2),0) AS tintLeadRights,
		ISNULL((SELECT TOP 1 intViewAllowed FROM GroupAuthorization WHERE numGroupID=U.numGroupID AND numModuleID=3 AND numPageID=2),0) AS tintProspectRights,
		ISNULL((SELECT TOP 1 intViewAllowed FROM GroupAuthorization WHERE numGroupID=U.numGroupID AND numModuleID=4 AND numPageID=2),0) AS tintAccountRights,
		ISNULL((SELECT TOP 1 intViewAllowed FROM GroupAuthorization WHERE numGroupID=U.numGroupID AND numModuleID=10 AND numPageID=10),0) AS tintSalesOrderListRights,
		ISNULL((SELECT TOP 1 intViewAllowed FROM GroupAuthorization WHERE numGroupID=U.numGroupID AND numModuleID=10 AND numPageID=11),0) AS tintPurchaseOrderListRights,
		ISNULL((SELECT TOP 1 intViewAllowed FROM GroupAuthorization WHERE numGroupID=U.numGroupID AND numModuleID=10 AND numPageID=2),0) AS tintSalesOpportunityListRights,
		ISNULL((SELECT TOP 1 intViewAllowed FROM GroupAuthorization WHERE numGroupID=U.numGroupID AND numModuleID=10 AND numPageID=8),0) AS tintPurchaseOpportunityListRights,
		ISNULL((SELECT TOP 1 intViewAllowed FROM GroupAuthorization WHERE numGroupID=U.numGroupID AND numModuleID=11 AND numPageID=2),0) AS tintContactListRights,
		ISNULL((SELECT TOP 1 intViewAllowed FROM GroupAuthorization WHERE numGroupID=U.numGroupID AND numModuleID=7 AND numPageID=2),0) AS tintCaseListRights,
		ISNULL((SELECT TOP 1 intViewAllowed FROM GroupAuthorization WHERE numGroupID=U.numGroupID AND numModuleID=12 AND numPageID=2),0) AS tintProjectListRights,
		ISNULL((SELECT TOP 1 numCriteria FROM TicklerListFilterConfiguration WHERE CHARINDEX(CONCAT(',',U.numUserDetailID,','),CONCAT(',',vcSelectedEmployee,',')) > 0),0) AS tintActionItemRights,
		ISNULL(D.numAuthorizePercentage,0) AS numAuthorizePercentage,
		ISNULL(D.bitAllowDuplicateLineItems,0) bitAllowDuplicateLineItems,
		ISNULL(D.bitLogoAppliedToBizTheme,0) bitLogoAppliedToBizTheme,
		ISNULL(D.vcLogoForBizTheme,'') vcLogoForBizTheme,
		ISNULL(D.bitLogoAppliedToLoginBizTheme,0) bitLogoAppliedToLoginBizTheme,
		ISNULL(D.vcLogoForLoginBizTheme,'') vcLogoForLoginBizTheme,
		ISNULL(D.vcThemeClass,'1473b4') vcThemeClass,
		ISNULL(D.vcLoginURL,'') AS vcLoginURL,
		ISNULL(D.bitDisplayCustomField,0) AS bitDisplayCustomField,
		ISNULL(D.bitFollowupAnytime,0) AS bitFollowupAnytime,
		ISNULL(D.bitpartycalendarTitle,0) AS bitpartycalendarTitle,
		ISNULL(D.bitpartycalendarLocation,0) AS bitpartycalendarLocation,
		ISNULL(D.bitpartycalendarDescription,0) AS bitpartycalendarDescription,
		ISNULL(D.bitREQPOApproval,0) AS bitREQPOApproval,
		ISNULL(D.bitARInvoiceDue,0) AS bitARInvoiceDue,
		ISNULL(D.bitAPBillsDue,0) AS bitAPBillsDue,
		ISNULL(D.bitItemsToPickPackShip,0) AS bitItemsToPickPackShip,
		ISNULL(D.bitItemsToInvoice,0) AS bitItemsToInvoice,
		ISNULL(D.bitSalesOrderToClose,0) AS bitSalesOrderToClose,
		ISNULL(D.bitItemsToPutAway,0) AS bitItemsToPutAway,
		ISNULL(D.bitItemsToBill,0) AS bitItemsToBill,
		ISNULL(D.bitPosToClose,0) AS bitPosToClose,
		ISNULL(D.bitPOToClose,0) AS bitPOToClose,
		ISNULL(D.bitBOMSToPick,0) AS bitBOMSToPick,
		ISNULL(D.vchREQPOApprovalEmp,'') AS vchREQPOApprovalEmp,
		ISNULL(D.vchARInvoiceDue,'') AS vchARInvoiceDue,
		ISNULL(D.vchAPBillsDue,'') AS vchAPBillsDue,
		ISNULL(D.vchItemsToPickPackShip,'') AS vchItemsToPickPackShip,
		ISNULL(D.vchItemsToInvoice,'') AS vchItemsToInvoice,
		ISNULL(stuff((
        select ',' + CAST(UPC.numUserID AS VARCHAR(10))
        from UnitPriceApprover UPC
        where UPC.numDomainID = D.numDomainId
        order by UPC.numUserID
			for xml path('')),1,1,''),'') AS vchPriceMarginApproval,
		ISNULL(D.vchSalesOrderToClose,'') AS vchSalesOrderToClose,
		ISNULL(D.vchItemsToPutAway,'') AS vchItemsToPutAway,
		ISNULL(D.vchItemsToBill,'') AS vchItemsToBill,
		ISNULL(D.vchPosToClose,'') AS vchPosToClose,
		ISNULL(D.vchPOToClose,'') AS vchPOToClose,
		ISNULL(D.vchBOMSToPick,'') AS vchBOMSToPick,
		ISNULL(D.decReqPOMinValue,0) AS decReqPOMinValue,
		ISNULL(U.tintPayrollType,1) tintPayrollType,
		(CASE WHEN EXISTS (SELECT numCategoryHDRID FROM TimeAndExpense WHERE numUserCntID=U.numUserDetailId AND numCategory=1 AND numType=7 AND dtToDate IS NULL) THEN 1 ELSE 0 END) bitUserClockedIn,
		ISNULL(D.bitUseOnlyActionItems,0) AS bitUseOnlyActionItems,
		ISNULL(D.vcElectiveItemFields,'') AS vcElectiveItemFields,
		ISNULL(D.bitDisplayContractElement,0) AS bitDisplayContractElement,
		ISNULL(D.vcEmployeeForContractTimeElement,'') AS vcEmployeeForContractTimeElement,
		ISNULL(D.bitEnableSmartyStreets,0) bitEnableSmartyStreets,
		ISNULL(D.vcSmartyStreetsAPIKeys,'') vcSmartyStreetsAPIKeys
	FROM 
		UserMaster U                              
	left join ExchangeUserDetails E                              
	on E.numUserID=U.numUserID                              
	left join ImapUserDetails IM                              
	on IM.numUserCntID=U.numUserDetailID          
	Join Domain D                              
	on D.numDomainID=U.numDomainID                                           
	left join DivisionMaster Div                                            
	on D.numDivisionID=Div.numDivisionID                               
	left join CompanyInfo C                              
	on C.numCompanyID=Div.numCompanyID                               
	left join AdditionalContactsInformation A                              
	on  A.numContactID=U.numUserDetailId                            
	Join  Subscribers S                            
	on S.numTargetDomainID=D.numDomainID    
	 OUTER APPLY
	 (
		SELECT  
			TOP 1 REPLACE(SCB.Link,'../','') vcDefaultNavURL
		FROM    
			TabMaster T
		JOIN 
			GroupTabDetails G ON G.numTabId = T.numTabId
		LEFT JOIN
			ShortCutGrpConf SCUC ON G.numTabId=SCUC.numTabId AND SCUC.numDomainId=U.numDomainID AND SCUC.numGroupId=U.numGroupID AND bitDefaultTab=1
		LEFT JOIN
			ShortCutBar SCB
		ON
			SCB.Id=SCUC.numLinkId
		WHERE   
			(T.numDomainID = U.numDomainID OR bitFixed = 1)
			AND G.numGroupID = U.numGroupID
			AND ISNULL(G.[tintType], 0) <> 1
			AND tintTabType =1
			AND T.numTabID NOT IN (2,68)
		ORDER BY 
			SCUC.bitInitialPage DESC
	 )  TempDefaultPage 
	 OUTER APPLY
	 (
		SELECT  
			TOP 1 REPLACE(T.vcURL,'../','') vcDefaultNavURL
		FROM    
			TabMaster T
		JOIN 
			GroupTabDetails G 
		ON 
			G.numTabId = T.numTabId
		WHERE   
			(T.numDomainID = U.numDomainID OR bitFixed = 1)
			AND G.numGroupID = U.numGroupID
			AND ISNULL(G.[tintType], 0) <> 1
			AND tintTabType =1
			AND T.numTabID NOT IN (2,68)
			AND ISNULL(G.bitInitialTab,0) = 1 
			AND ISNULL(bitallowed,0)=1
		ORDER BY 
			G.bitInitialTab DESC
	 ) TEMPDefaultTab                   
	WHERE 1 = (CASE 
		WHEN @vcLinkedinId='N' THEN (CASE WHEN vcEmailID=@UserName and vcPassword=@vcPassword THEN 1 ELSE 0 END)
		ELSE (CASE WHEN U.vcLinkedinId=@vcLinkedinId THEN 1 ELSE 0 END)
		END) and bitActive IN (1,2) AND getutcdate() between dtSubStartDate and dtSubEndDate
	END
             
	SELECT 
		numTerritoryId 
	FROM
		UserTerritory UT                              
	JOIN 
		UserMaster U                              
	ON 
		numUserDetailId =UT.numUserCntID                                         
	WHERE 
		1 = (CASE 
			WHEN @vcLinkedinId='N' THEN (CASE WHEN vcEmailID=@UserName and vcPassword=@vcPassword THEN 1 ELSE 0 END)
			ELSE (CASE WHEN vcLinkedinId=@vcLinkedinId THEN 1 ELSE 0 END)
			END)

END
GO
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ElasticSearch_GetItems')
DROP PROCEDURE dbo.USP_ElasticSearch_GetItems
GO
CREATE PROCEDURE [dbo].[USP_ElasticSearch_GetItems]
	@numDomainID NUMERIC(18,0)
	,@vcItemCodes VARCHAR(MAX) = NULL
AS 
BEGIN
	DECLARE @CustomerSearchFields AS VARCHAR(MAX) = ''
	DECLARE @CustomerSearchCustomFields AS VARCHAR(MAX) = ''

	DECLARE @CustomerDisplayFields AS VARCHAR(MAX) = ''

	DECLARE @query AS NVARCHAR(MAX);

	----------------------------   Search Fields ---------------------------

	DECLARE @TEMPSearchFields TABLE
	(
		numFieldID NUMERIC(18,0)
		,vcDbColumnName VARCHAR(200)
		,tintOrder TINYINT
		,bitCustom BIT
	)

	INSERT INTO @TEMPSearchFields
	(
		numFieldID
		,vcDbColumnName
		,tintOrder
		,bitCustom
	)
	SELECT    
		numFieldId,
		vcDbColumnName,
        tintRow AS tintOrder,
        0 AS Custom
    FROM 
		View_DynamicColumns
    WHERE     
		numFormId = 22
        AND numDomainID = @numDomainID
        AND tintPageType = 1
        AND bitCustom = 0
        AND ISNULL(bitSettingField, 0) = 1
        AND numRelCntType = 1
    UNION
    SELECT 
		numFieldId,
		vcFieldName,
		tintRow AS tintOrder,
		1 AS Custom
    FROM 
		View_DynamicCustomColumns
    WHERE 
		Grp_id = 5
        AND numFormId = 22
        AND numDomainID = @numDomainID
        AND tintPageType = 1
        AND bitCustom = 1
        AND numRelCntType = 1

	IF NOT EXISTS ( SELECT * FROM @TEMPSearchFields )
    BEGIN
        INSERT INTO @TEMPSearchFields
		(
			numFieldID
			,vcDbColumnName
			,tintOrder
			,bitCustom
		)
        SELECT  
			numFieldId ,
			vcDbColumnName,
			tintorder,
			0
        FROM 
			View_DynamicDefaultColumns
        WHERE 
			numFormId = 22
            AND bitDefault = 1
            AND ISNULL(bitSettingField, 0) = 1
            AND numDomainID = @numDomainID 
    END

	IF EXISTS (SELECT * FROM @TEMPSearchFields)
	BEGIN
		SELECT 
			@CustomerSearchFields = COALESCE (@CustomerSearchFields,'') + CAST(vcDbColumnName AS VARCHAR(100)) + ' AS Search_' + CAST(vcDbColumnName AS VARCHAR(100)) + ', '	
		FROM 
			@TEMPSearchFields 
		WHERE
			ISNULL(bitCustom,0) = 0
		ORDER BY
			tintOrder ASC
		
		-- Removes last , from string
		IF DATALENGTH(@CustomerSearchFields) > 0
			SET @CustomerSearchFields = LEFT(@CustomerSearchFields, LEN(@CustomerSearchFields) - 1)

		IF EXISTS(SELECT * FROM @TEMPSearchFields WHERE ISNULL(bitCustom,0) = 1)
		BEGIN
			SELECT 
				@CustomerSearchCustomFields = COALESCE (@CustomerSearchCustomFields,'') + 'ISNULL(CFW' + CAST(numFieldId AS VARCHAR(100)) + ','''') AS ' + 'Search_CFW' + CAST(numFieldId AS VARCHAR(100)) + ','
			FROM 
				@TEMPSearchFields 
			WHERE
				ISNULL(bitCustom,0) = 1
			ORDER BY
				tintOrder ASC
		END

		-- Removes last , from string
		IF DATALENGTH(@CustomerSearchCustomFields) > 0
			SET @CustomerSearchCustomFields = LEFT(@CustomerSearchCustomFields, LEN(@CustomerSearchCustomFields) - 1)
	END

	IF  LEN(@CustomerSearchFields) = 0 AND LEN(@CustomerSearchCustomFields) = 0
	BEGIN
		SET @CustomerSearchFields = 'vcItemName AS Search_vcItemName'
	END


	----------------------------   Display Fields ---------------------------
	DECLARE @TEMPDisplayFields TABLE
	(
		numFieldID NUMERIC(18,0)
		,vcDbColumnName VARCHAR(200)
		,tintOrder TINYINT
		,bitCustom BIT
	)


	INSERT INTO @TEMPDisplayFields
	(
		numFieldID
		,vcDbColumnName
		,tintOrder
		,bitCustom
	)
	SELECT    
		numFieldId,
		vcDbColumnName,
        tintRow AS tintOrder,
        0 AS Custom
    FROM 
		View_DynamicColumns
    WHERE     
		numFormId = 22
        AND numDomainID = @numDomainID
        AND tintPageType = 1
        AND bitCustom = 0
        AND ISNULL(bitSettingField, 0) = 1
        AND numRelCntType = 0
		AND vcDbColumnName <> 'CustomerPartNo'
    UNION
    SELECT 
		numFieldId,
		vcFieldName,
		tintRow AS tintOrder,
		1 AS Custom
    FROM 
		View_DynamicCustomColumns
    WHERE 
		Grp_id = 5
        AND numFormId = 22
        AND numDomainID = @numDomainID
        AND tintPageType = 1
        AND bitCustom = 1
        AND numRelCntType = 0


	IF EXISTS (SELECT * FROM @TEMPDisplayFields)
	BEGIN
		SELECT 
			@CustomerDisplayFields = COALESCE (@CustomerDisplayFields,'') + CONCAT('(CASE WHEN LEN(',vcDbColumnName,') > 0 THEN CONCAT(',(CASE WHEN LEN(@CustomerDisplayFields) > 0 THEN ''', '',' ELSE ''''',' END),vcDbColumnName, ') ELSE '''' END)') + ', '	
		FROM 
			@TEMPDisplayFields 
		WHERE
			ISNULL(bitCustom,0) = 0
		ORDER BY
			tintOrder ASC

		IF EXISTS(SELECT * FROM @TEMPDisplayFields WHERE ISNULL(bitCustom,0) = 1)
		BEGIN
			SELECT 
				@CustomerDisplayFields = COALESCE (@CustomerDisplayFields,'') + CONCAT('(CASE WHEN LEN(',CONCAT('CFW',numFieldId),') > 0 THEN CONCAT(',(CASE WHEN LEN(@CustomerDisplayFields) > 0 THEN ''', '',' ELSE ''''',' END),CONCAT('CFW',numFieldId), ') ELSE '''' END)') + ', '	
			FROM 
				@TEMPDisplayFields 
			WHERE
				ISNULL(bitCustom,0) = 1
			ORDER BY
				tintOrder ASC
		END

		-- Removes last , from string
		IF DATALENGTH(@CustomerDisplayFields) > 0
			SET @CustomerDisplayFields = LEFT(@CustomerDisplayFields, LEN(@CustomerDisplayFields) - 1)
	END

	IF  LEN(@CustomerDisplayFields) = 0
	BEGIN
		SET @CustomerDisplayFields = 'vcItemName'
	END

	DECLARE @CustomFields AS VARCHAR(1000)

	SELECT @CustomFields = COALESCE (@CustomFields,'') + 'CFW' + CAST(FLD_ID AS VARCHAR) + ', ' FROM CFW_Fld_Master WHERE numDomainID=@numDomainID AND Grp_id=5
	-- Removes last , from string
	IF DATALENGTH(@CustomFields) > 0
		SET @CustomFields = LEFT(@CustomFields, LEN(@CustomFields) - 1)

	SELECT @query = CONCAT('SELECT numItemCode as id,''item'' As module, CONCAT(''/Items/frmKitDetails.aspx?ItemCode='',numItemCode) AS url,CONCAT(''<b style="color:#ff9900">Item ('',(CASE charItemType WHEN ''P'' THEN ''Inventory''  WHEN ''S'' THEN ''Service'' WHEN ''A'' THEN ''Asset'' ELSE ''Non Inventory'' END),'':)</b> '',',@CustomerDisplayFields,') AS displaytext, CONCAT(''Item ('',(CASE charItemType WHEN ''P'' THEN ''Inventory''  WHEN ''S'' THEN ''Service'' WHEN ''A'' THEN ''Asset'' ELSE ''Non Inventory'' END),''): '',',@CustomerDisplayFields,') AS text',
	CASE @CustomerSearchFields
	WHEN '' THEN ''
	ELSE ',' + @CustomerSearchFields
	END
	,
	CASE @CustomerSearchCustomFields
	WHEN '' THEN ''
	ELSE ',' + @CustomerSearchCustomFields
	END
	 ,' FROM (SELECT 
				Item.numItemCode
				,charItemType
				,ISNULL(vcItemName,'''') AS vcItemName
				,ISNULL(txtItemDesc,'''') AS txtItemDesc
				,ISNULL(vcModelID,'''') AS vcModelID
				,ISNULL(vcSKU,'''') AS vcSKU
				,ISNULL(numBarCodeId,'''') AS numBarCodeId
				,ISNULL(CompanyInfo.vcCompanyName,'''') vcCompanyName
				,(CASE WHEN charItemType=''p'' THEN ISNULL((SELECT TOP 1 monWListPrice FROM WarehouseItems WHERE numItemID=Item.numItemCode AND ISNULL(monWListPrice,0) > 0),ISNULL(monListPrice,0)) ELSE ISNULL(monListPrice,0) END) AS monListPrice
				,ISNULL(Vendor.vcPartNo,'''') vcPartNo
				,STUFF((SELECT '','' + CustomerPartNo FROM CustomerPartNumber WHERE numItemCode=Item.numItemCode FOR xml path ('''')), 1, 1, '''') CustomerPartNo
				,(CASE WHEN numItemGroup > 0 AND ISNULL(bitMatrix,0) = 1 THEN dbo.fn_GetItemAttributes(Item.numDomainID,Item.numItemCode,0) ELSE '''' END) AS vcAttributes
				,(CASE WHEN charItemType=''p'' AND numItemGroup > 0 AND ISNULL(bitMatrix,0) = 0 THEN STUFF((SELECT '', '' + vcWHSKU FROM WareHouseItems WHERE numItemID = Item.numItemCode FOR XML PATH('''')),1,2,'''') ELSE '''' END) AS vcWHSKU
				,(CASE WHEN charItemType=''p'' AND numItemGroup > 0 AND ISNULL(bitMatrix,0) = 0 THEN STUFF((SELECT '', '' + vcBarCode FROM WareHouseItems WHERE numItemID = Item.numItemCode FOR XML PATH('''')),1,2,'''') ELSE '''' END) AS vcBarCode
				' + (CASE WHEN DATALENGTH(@CustomFields) > 0 THEN ',CustomFields.*' ELSE '' END) + '
			FROM 
				Item
			LEFT JOIN 
				Vendor
			ON
				Item.numVendorID=Vendor.numVendorID
				AND Item.numItemCode=Vendor.numItemCode
			LEFT JOIN 
				DivisionMaster
			ON
				Vendor.numVendorID = DivisionMaster.numDivisionID
			LEFT JOIN 
				CompanyInfo
			ON
				DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
			' + (CASE WHEN DATALENGTH(@CustomFields) > 0 THEN CONCAT('OUTER APPLY
			(
				SELECT 
				* 
				FROM 
				(
					SELECT  
						CONCAT(''CFW'',CFW_Fld_Values_Item.Fld_ID) Fld_ID,
						ISNULL(dbo.fn_GetCustFldStringValue(CFW_Fld_Values_Item.Fld_ID, CFW_Fld_Values_Item.RecId, CFW_Fld_Values_Item.Fld_Value),'''') Fld_Value 
					FROM 
						CFW_Fld_Master
					LEFT JOIN 
						CFW_Fld_Values_Item
					ON
						CFW_Fld_Values_Item.Fld_ID = CFW_Fld_Master.Fld_id
					WHERE 
						RecId = Item.numItemCode
				) p PIVOT (MAX([Fld_Value]) FOR Fld_ID  IN (',@CustomFields,') ) AS pvt
			) CustomFields') ELSE '' END) + '
			WHERE
				Item.numDomainID =',@numDomainID,' AND (Item.numItemCode IN (',ISNULL(@vcItemCodes,'0'),') OR LEN(''',@vcItemCodes,''') = 0)) TEMP1')

	PRINT @query

	EXEC SP_EXECUTESQL @query
END
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p' AND NAME ='USP_ElasticSearchBizCart_GetItems')
DROP PROCEDURE USP_ElasticSearchBizCart_GetItems
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[USP_ElasticSearchBizCart_GetItems]
	@numDomainID NUMERIC(18,0),
	@vcItemCodes VARCHAR(MAX) = NULL
AS 
BEGIN
	SET NOCOUNT ON;
	-- @vcItemCodes is corrently set for single item
	
    DECLARE @strSQL NVARCHAR(MAX)

    DECLARE @fldList AS NVARCHAR(MAX)
	DECLARE @fldList1 AS NVARCHAR(MAX)
	SELECT @fldList=COALESCE(@fldList + ',','') + QUOTENAME(REPLACE(Fld_label,' ','') + '_C') FROM dbo.CFW_Fld_Master WHERE numDomainID=@numDomainID AND Grp_id=5
	SELECT @fldList1=COALESCE(@fldList1 + ',','') + QUOTENAME(REPLACE(Fld_label,' ','') + '_' + CAST(Fld_Id AS VARCHAR)) FROM dbo.CFW_Fld_Master WHERE numDomainID=@numDomainID AND Grp_id=9
     
	-- Item Details
	SET @strSQL = CONCAT(@strSQL,'
	SELECT --I.*,
	I.numDomainID
	,I.numItemCode
	,ISNULL(I.bitMatrix,0) AS bitMatrix
	,ISNULL(I.bitKitParent,0) AS bitKitParent
	,I.numItemGroup
	,vcItemName
	,I.bintCreatedDate
	,vcManufacturer
	,ISNULL(txtItemDesc,'''') txtItemDesc
	,vcSKU
	,fltHeight
	,fltWidth
	,fltLength
	,vcModelID
	,fltWeight
	,numBaseUnit
	,numSaleUnit
	,numPurchaseUnit
	,bitFreeShipping
	,charItemType
	,monListPrice
	,bitAllowBackOrder
	--,bitShowInStock
	,numVendorID
	,numItemClassification
	--,SUM(numOnHand) numOnHand
	--,SUM(numAllocation) numAllocation
	,ISNULL(I.numManufacturer,0) numManufacturer
	,ISNULL(IED.txtDesc,'''') txtDesc
	,ISNULL(IED.txtShortDesc,'''') txtShortDesc 
	,IM.vcPathForImage
	,IM.vcpathForTImage
	,UOM AS UOMConversionFactor
	,UOMPurchase AS UOMPurchaseConversionFactor
	
	,CONVERT(nvarchar(200),'''') AS vcCategoryName
	,CONVERT(nvarchar(200),'''') AS CategoryDesc
	,CONVERT(NUMERIC(9),0) AS numTotalPromotions
	,CONVERT(nvarchar(200),'''') AS vcPromoDesc
	,CONVERT(bit,0) AS bitRequireCouponCode
	,CONVERT(bit,0) AS bitInStock
	,CONVERT(nvarchar(200),'''') AS InStock

	,CONVERT(NUMERIC(9,2),0) AS monFirstPriceLevelPrice
	,CONVERT(NUMERIC(9,2),0) AS fltFirstPriceLevelDiscount
	,CONVERT(NUMERIC(9,2),0) AS monListPrice
	,CONVERT(NUMERIC(9,2),0) AS monMSRP
	,CONVERT(bit,0) AS bitHasChildKits
	,CONVERT(NUMERIC(9),0) AS [numWareHouseItemID]

	',(CASE WHEN LEN(ISNULL(@fldList,'')) > 0 THEN CONCAT(',',@fldList) ELSE '' END),(CASE WHEN LEN(ISNULL(@fldList1,'')) > 0 THEN CONCAT(',',@fldList1) ELSE '' END),'

	,ISNULL(I.IsArchieve,0) AS IsArchieve
	,(CASE WHEN ISNULL(I.numItemGroup,0) > 0 AND ISNULL(I.bitMatrix,0) = 1 THEN dbo.fn_GetItemAttributes(I.numDomainID,I.numItemCode,1) ELSE '''' END) AS vcAttributes
	,(CASE WHEN ISNULL(I.numItemGroup,0) > 0 AND ISNULL(I.bitMatrix,0) = 1 THEN CONCAT(''['',STUFF((SELECT 
																								CONCAT('',{"FieldID":'',IA.FLD_ID,'',"FieldVaue":'',IA.FLD_Value,''}'')
																							FROM
																								ItemAttributes IA
																							WHERE
																								IA.numItemCode = I.numItemCode
																							ORDER BY 
																								ID
																							FOR XML PATH('''')),1,1,''''),'']'') ELSE ''[]'' END) AS vcAttributeValues
	FROM Item I
	LEFT JOIN ItemExtendedDetails IED ON (IED.numItemCode = I.numItemCode)
	LEFT JOIN ItemImages IM ON I.numItemCode = IM.numItemCode AND IM.bitdefault=1 AND IM.bitIsImage = 1
	INNER JOIN
	(
		SELECT [OutParam] FROM dbo.[SplitString](''' + @vcItemCodes + ''','','')
	) TEMPItem ON I.numItemCode = TEMPItem.OutParam
	CROSS APPLY (SELECT dbo.fn_UOMConversion(ISNULL(I.numSaleUnit, 0),I.numItemCode, I.numDomainId,ISNULL(I.numBaseUnit, 0)) AS UOM) AS UOM
	CROSS APPLY (SELECT dbo.fn_UOMConversion(ISNULL(I.numPurchaseUnit, 0),I.numItemCode, I.numDomainId,ISNULL(I.numBaseUnit, 0)) AS UOMPurchase) AS UOMPurchase
	')

	IF LEN(ISNULL(@fldList,'')) > 0
	BEGIN
		SET @strSQL = @strSQL +'
		left join (
						SELECT  t2.RecId, REPLACE(t1.Fld_label,'' '','''') + ''_C'' as Fld_label, 
							CASE t1.fld_type WHEN ''SelectBox'' THEN isnull(dbo.fn_GetListItemName(t2.Fld_Value),'''')
												WHEN ''CheckBox'' THEN isnull(dbo.fn_getCustomDataValue(t2.Fld_Value,9),'''')
												WHEN ''DateField'' THEN dbo.FormatedDateFromDate(t2.Fld_Value,' + CAST(@numDomainId AS VARCHAR(18)) +')
												ELSE Replace(ISNULL(t2.Fld_Value,''0''),''$'',''$$'') end AS Fld_Value FROM CFW_Fld_Master t1 
						JOIN CFW_FLD_Values_Item AS t2 ON t1.Fld_ID = t2.Fld_ID where t1.Grp_id = 5 and t1.numDomainID=' + CAST(@numDomainId AS VARCHAR(18)) +' AND t1.fld_type <> ''Link''
						--AND t2.RecId IN (SELECT numItemCode FROM #tmpPagedItems )
						AND t2.RecId IN (SELECT [OutParam] FROM dbo.[SplitString](''' + @vcItemCodes + ''','',''))
						) p PIVOT (MAX([Fld_Value]) FOR Fld_label IN (' + @fldList + ')) AS pvt on I.numItemCode=pvt.RecId' 
	END

	IF LEN(ISNULL(@fldList1,'')) > 0
	BEGIN
					
		SET @strSQL = @strSQL +'
		left join (
					SELECT  t2.numItemCode AS RecId, REPLACE(t1.Fld_label,'' '','''') + ''_'' + CAST(t1.Fld_Id AS VARCHAR) as Fld_label, 
						CASE t1.fld_type WHEN ''SelectBox'' THEN isnull(dbo.fn_GetListItemName(t2.Fld_Value),'''')
											WHEN ''CheckBox'' THEN isnull(dbo.fn_getCustomDataValue(t2.Fld_Value,9),'''')
											WHEN ''DateField'' THEN dbo.FormatedDateFromDate(t2.Fld_Value,' + CAST(@numDomainId AS VARCHAR(18)) +')
											ELSE Replace(ISNULL(t2.Fld_Value,''0''),''$'',''$$'') end AS Fld_Value FROM CFW_Fld_Master t1 
					JOIN ItemAttributes AS t2 ON t1.Fld_ID = t2.Fld_ID where t1.Grp_id = 9 and t1.numDomainID=' + CAST(@numDomainId AS VARCHAR(18)) +' AND t1.fld_type <> ''Link''
					--AND t2.numItemCode IN (SELECT numItemCode FROM #tmpPagedItems )
					AND t2.numItemCode IN (SELECT [OutParam] FROM dbo.[SplitString](''' + @vcItemCodes + ''','',''))
					) p PIVOT (MAX([Fld_Value]) FOR Fld_label IN (' + @fldList1 + ')) AS pvt1 on I.numItemCode=pvt1.RecId'
	END

	DECLARE @tmpSQL AS NVARCHAR(MAX)
	SET @tmpSQL = @strSQL
	
	PRINT CAST(@tmpSQL AS NTEXT)
	EXEC sp_executeSQL @strSQL

	-- Item Warehouse Details
	SELECT numItemID,vcWarehouse,numWarehouseItemID,WI.numWareHouseID,ISNULL(SUM(numOnHand),0) AS numOnHand ,ISNULL(SUM(numAllocation),0) AS numAllocation
	FROM WareHouseItems WI
	INNER JOIN Warehouses W ON WI.numwarehouseId = W.numWarehouseID
	INNER JOIN
	(
		SELECT [OutParam] FROM dbo.[SplitString](@vcItemCodes,',')
	) TEMPItem ON WI.numItemID = TEMPItem.OutParam
	GROUP BY numItemID,vcWarehouse,numWarehouseItemID,WI.numWareHouseID

	-- Item Category
	SELECT numItemID,IC.numCategoryID,C.vcCategoryName
	,C.vcDescription as CategoryDesc
	FROM ItemCategory IC
	INNER JOIN Category C ON IC.numCategoryID = C.numCategoryID
	INNER JOIN
	(
		SELECT [OutParam] FROM dbo.[SplitString](@vcItemCodes,',')
	) TEMPItem ON IC.numItemID = TEMPItem.OutParam


END
GO
/****** Object:  StoredProcedure [dbo].[USP_GetAccountReceivableAging_Invoice]    Script Date: 05/24/2010 03:48:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetAccountReceivableAging_Invoice')
DROP PROCEDURE USP_GetAccountReceivableAging_Invoice
GO
CREATE PROCEDURE [dbo].[USP_GetAccountReceivableAging_Invoice]
(
    @numDomainId AS NUMERIC(9) = 0,
	@numDivisionId AS NUMERIC(9) = NULL,
	@vcFlag VARCHAR(20),
	@numAccountClass AS NUMERIC(9)=0,
	@dtFromDate AS DATE = NULL,
	@dtToDate AS DATE = NULL,
	@ClientTimeZoneOffset INT = 0,
	@vcRegularSearch VARCHAR(MAX) = '',
	@bitCustomerStatement BIT = 0,
	@tintBasedOn TINYINT = 1
)
AS
  BEGIN
	DECLARE @numDefaultARAccount NUMERIC(18,0)
	SET @numDefaultARAccount = ISNULL((SELECT numAccountID FROM AccountingCharges WHERE numDomainID=@numDomainId AND numChargeTypeId=4),0)

    DECLARE  @AuthoritativeSalesBizDocId  AS INTEGER
    SELECT @AuthoritativeSalesBizDocId = isnull(numAuthoritativeSales,0)
    FROM   AuthoritativeBizDocs
    WHERE  numDomainId = @numDomainId;--287
    ------------------------------------------      
    DECLARE @baseCurrency AS NUMERIC
	SELECT @baseCurrency=numCurrencyID FROM dbo.Domain WHERE numDomainId=@numDomainId
	/*when any Order is placed in Foreign currency (other than base currency)then show it in purple color */
	------------------------------------------      
	IF @dtFromDate IS NULL
		SET @dtFromDate = '1990-01-01 00:00:00.000'
	IF @dtToDate IS NULL
		SET @dtToDate = GETUTCDATE()

	SET @numDivisionId = ISNULL(@numDivisionId,0)

    DECLARE @strSql VARCHAR(MAX);
	Declare @strSql1 VARCHAR(MAX);
	Declare @strCredit varchar(MAX); SET @strCredit=''
    
    
    SET @strSql = ' SELECT DISTINCT OM.[numOppId],
                        OM.[vcPOppName],
                        OM.[tintOppType],
                        OB.[numOppBizDocsId],
                        OB.[vcBizDocID],
                        isnull(OB.monDealAmount * OM.fltExchangeRate,0) TotalAmount,
                        (ISNULL(TablePayments.monPaidAmount,0) * OM.fltExchangeRate) AmountPaid,
                        isnull(OB.monDealAmount
                                 * OM.fltExchangeRate
                                 - ISNULL(TablePayments.monPaidAmount,0)
                                     * OM.fltExchangeRate,0) BalanceDue,
                        CASE ISNULL(bitBillingTerms,0) 
                          --WHEN 1 THEN [dbo].[FormatedDateFromDate](DATEADD(dd,convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0)),OB.dtFromDate),
                          WHEN 1 THEN [dbo].[FormatedDateFromDate](DATEADD(dd,convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0)),OB.dtFromDate),
                                                                   '+ CONVERT(VARCHAR(15),@numDomainId) +')
                          WHEN 0 THEN [dbo].[FormatedDateFromDate](ob.[dtFromDate],'+ CONVERT(VARCHAR(15),@numDomainId) +')
                        END AS DueDate,
                        bitBillingTerms,
                        intBillingDays,
                        OB.dtCreatedDate,
                        ISNULL(C.[varCurrSymbol],'''') [varCurrSymbol],CO.vcCompanyName,ADC.vcFirstname + '' ''+ ADC.vcLastName AS vcContactName,
                           dbo.fn_GetContactName(OM.numRecOwner) AS RecordOwner,
						   dbo.fn_GetContactName(OM.numAssignedTo) AS AssignedTo
						   ,CASE WHEN ADC.numPhone IS NULL THEN '''' ELSE + ADC.numPhone + (CASE WHEN numPhoneExtension IS NULL THEN '''' WHEN numPhoneExtension='''' THEN '''' ELSE '','' + ADC.numPhoneExtension END) END AS vcCustPhone,
						   OM.numCurrencyID,OB.vcRefOrderNo,[dbo].[FormatedDateFromDate](ob.[dtFromDate],'+ CONVERT(VARCHAR(15),@numDomainId) +') AS dtFromDate,
						    -1 AS [numJournal_Id],-1 as numReturnHeaderID,
							CASE WHEN ISNULL(BT.[numDiscount],0) > 0 AND ISNULL(BT.[numInterestPercentage],0) > 0 
								 THEN '+ '''-''+' + 'CONVERT(VARCHAR(10),ISNULL(BT.[numDiscount],0)) +' + '''% if paid ''+' + ' CONVERT(VARCHAR(10),ISNULL([BT].[numDiscountPaidInDays],0)) +' + ''' days early / ''+' + ' CONVERT(VARCHAR(10),ISNULL([BT].[numInterestPercentage],0)) +' + '''% if paid ''+' + ' CONVERT(VARCHAR(10),ISNULL([BT].[numInterestPercentageIfPaidAfterDays],0)) +' + ''' days or later ''' +
								 ' WHEN ISNULL(BT.[numDiscount],0) > 0 THEN '+ '''-''+' + 'CONVERT(VARCHAR(10),ISNULL(BT.[numDiscount],0)) +' + '''% if paid ''+' + ' CONVERT(VARCHAR(10),ISNULL([BT].[numDiscountPaidInDays],0)) +' + ''' days early ''' + 
								 ' WHEN ISNULL(BT.[numInterestPercentage],0) > 0  THEN +CONVERT(VARCHAR(10),ISNULL([BT].[numInterestPercentage],0))+' + '''% if paid ''+' + ' CONVERT(VARCHAR(10),ISNULL([BT].[numInterestPercentageIfPaidAfterDays],0))+' + ''' days or later ''' +
								' END AS [vcTerms],
							ISNULL(BT.[numDiscount],0) AS [numDiscount],
							ISNULL(BT.[numInterestPercentage],0) AS [numInterest],
							0 AS tintLevel
							,ISNULL(OB.numARAccountID,'+ CAST(@numDefaultARAccount AS VARCHAR) +') AS numChartAcntId
        INTO #TempRecords                
        FROM   [OpportunityMaster] OM
               INNER JOIN [DivisionMaster] DM
                 ON OM.[numDivisionId] = DM.[numDivisionID]
                  INNER JOIN CompanyInfo CO ON CO.numCompanyId = DM.numCompanyID
                  LEFT JOIN AdditionalContactsInformation ADC ON ADC.numDivisionId=DM.numDivisionID AND ADC.numContactId=OM.numContactId
               LEFT OUTER JOIN OpportunityBizDocs OB ON OB.numOppId = OM.numOppID
               LEFT OUTER JOIN [Currency] C ON OM.[numCurrencyID] = C.[numCurrencyID]
			   LEFT JOIN [dbo].[BillingTerms] AS BT ON [OM].[intBillingDays] = [BT].[numTermsID] 
			   OUTER APPLY
			   (
					SELECT 
						SUM(monAmountPaid) AS monPaidAmount
					FROM
						DepositeDetails DD
					INNER JOIN
						DepositMaster DM
					ON
						DD.numDepositID=DM.numDepositId
					WHERE 
						numOppID = OM.numOppID
						AND numOppBizDocsID=OB.numOppBizDocsID
						AND CONVERT(DATE,DM.dtDepositDate) <= ''' + CAST(@dtToDate AS VARCHAr) + '''
				) TablePayments
        WHERE  OM.[tintOppType] = 1
               AND OM.[tintOppStatus] = 1
               AND om.[numDomainId] = '+ CONVERT(VARCHAR(15),@numDomainId) +'
               AND OB.[numBizDocId] = '+ CONVERT(VARCHAR(15),ISNULL(@AuthoritativeSalesBizDocId,0)) +' 
			   AND (OM.numDivisionId = ' + CAST(@numDivisionId AS VARCHAR) + ' OR ' + CAST(@numDivisionId AS VARCHAR) + ' = 0)
				AND (OM.numAccountClass= ' + CAST(@numAccountClass AS VARCHAR) + ' OR ' + CAST(@numAccountClass AS VARCHAR) + '=0)
				AND '+ (CASE 
							WHEN ISNULL(@bitCustomerStatement,0) = 1 
							THEN 'CONVERT(DATE,OB.[dtCreatedDate])' 
							ELSE 'CONVERT(DATE,DATEADD(DAY,CASE WHEN OM.bitBillingTerms = 1 AND ' + CAST(@tintBasedOn AS VARCHAR) + '=1
								 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(OM.intBillingDays,0)), 0))
								 ELSE 0 
									END, dtFromDate))' 
						END) + ' <= ''' + CAST(@dtToDate AS VARCHAr) + '''
               AND OB.bitAuthoritativeBizDocs=1
               AND ISNULL(OB.tintDeferred,0) <> 1
			   AND isnull(OB.monDealAmount
                                 * OM.fltExchangeRate
                                 - ISNULL(TablePayments.monPaidAmount,0)
                                     * OM.fltExchangeRate,0) <> 0'     
       
       SET @strSql1 = '  UNION 
		 SELECT 
				-1 [numOppId],
				''Journal'' [vcPOppName],
				0 [tintOppType],
				0 [numOppBizDocsId],
				''Journal'' [vcBizDocID],
				CASE WHEN GJD.numDebitAmt > 0 THEN GJD.numDebitAmt ELSE -1*GJD.numCreditAmt END TotalAmount,
				0 AmountPaid,
				CASE WHEN GJD.numDebitAmt > 0 THEN GJD.numDebitAmt ELSE -1*GJD.numCreditAmt END BalanceDue,
				[dbo].[FormatedDateFromDate](GJH.datEntry_Date,'+ CONVERT(VARCHAR(15),@numDomainId) +') AS DueDate,
				0 bitBillingTerms,
				0 intBillingDays,
				GJH.datCreatedDate,
                ISNULL(C.[varCurrSymbol], '''') [varCurrSymbol]
                ,CO.vcCompanyName,ADC.vcFirstname + '' ''+ ADC.vcLastName AS vcContactName,
                           '''' AS RecordOwner,
						   '''' AS AssignedTo
						   ,CASE WHEN ADC.numPhone IS NULL THEN '''' ELSE + ADC.numPhone + (CASE WHEN numPhoneExtension IS NULL THEN '''' WHEN numPhoneExtension='''' THEN '''' ELSE '','' + ADC.numPhoneExtension END) END AS vcCustPhone,
						   GJD.numCurrencyID,'''' as vcRefOrderNo,NULL, ISNULL(GJH.numJournal_Id,0) AS [numJournal_Id],-1 as numReturnHeaderID,
							CASE WHEN ISNULL(BT.[numDiscount],0) > 0 AND ISNULL(BT.[numInterestPercentage],0) > 0 
								 THEN '+ '''-''+' + 'CONVERT(VARCHAR(10),ISNULL(BT.[numDiscount],0)) +' + '''% if paid ''+' + ' CONVERT(VARCHAR(10),ISNULL([BT].[numDiscountPaidInDays],0)) +' + ''' days early / ''+' + ' CONVERT(VARCHAR(10),ISNULL([BT].[numInterestPercentage],0)) +' + '''% if paid ''+' + ' CONVERT(VARCHAR(10),ISNULL([BT].[numInterestPercentageIfPaidAfterDays],0)) +' + ''' days or later ''' +
								 ' WHEN ISNULL(BT.[numDiscount],0) > 0 THEN '+ '''-''+' + 'CONVERT(VARCHAR(10),ISNULL(BT.[numDiscount],0)) +' + '''% if paid ''+' + ' CONVERT(VARCHAR(10),ISNULL([BT].[numDiscountPaidInDays],0)) +' + ''' days early ''' + 
								 ' WHEN ISNULL(BT.[numInterestPercentage],0) > 0  THEN +CONVERT(VARCHAR(10),ISNULL([BT].[numInterestPercentage],0))+' + '''% if paid ''+' + ' CONVERT(VARCHAR(10),ISNULL([BT].[numInterestPercentageIfPaidAfterDays],0))+' + ''' days or later ''' +
								' END AS [vcTerms],
							ISNULL(BT.[numDiscount],0) AS [numDiscount],
							ISNULL(BT.[numInterestPercentage],0) AS [numInterest],
							0 AS tintLevel,
							GJD.numChartAcntId
		 FROM   dbo.General_Journal_Header GJH
				INNER JOIN dbo.General_Journal_Details GJD ON GJH.numJournal_Id = GJD.numJournalId
															  AND GJH.numDomainId = GJD.numDomainId
				INNER JOIN dbo.Chart_Of_Accounts COA ON COA.numAccountId = GJD.numChartAcntId
				INNER JOIN [DivisionMaster] DM ON GJD.numCustomerID = DM.[numDivisionID]
				INNER JOIN CompanyInfo CO ON CO.numCompanyId = DM.numCompanyID
                Left JOIN AdditionalContactsInformation ADC ON ADC.numContactId=GJD.numContactId 
				LEFT OUTER JOIN [Currency] C ON GJD.[numCurrencyID] = C.[numCurrencyID]
				LEFT JOIN [dbo].[BillingTerms] AS BT ON [DM].[numBillingDays] = [BT].[numTermsID]
		 WHERE  GJH.numDomainId ='+ CONVERT(VARCHAR(15),@numDomainId) +'
				AND COA.vcAccountCode LIKE ''01010105%'' 
				AND ISNULL(numOppId,0)=0 
				AND ISNULL(numOppBizDocsId,0)=0 
				AND ISNULL(GJH.numDepositID,0)=0 
				AND ISNULL(GJH.numReturnID,0)=0
				AND (GJD.numCustomerId = ' + CAST(@numDivisionId AS VARCHAR) + ' OR ' + CAST(@numDivisionId AS VARCHAR) + '  = 0)
				AND (GJH.numAccountClass= ' + CAST(@numAccountClass AS VARCHAR) + ' OR ' + CAST(@numAccountClass AS VARCHAR) + '=0)
				AND CONVERT(DATE,GJH.[datEntry_Date]) <= ''' + CAST(@dtToDate AS VARCHAr) + ''''
   
	 SET @strCredit = ' UNION SELECT -1 [numOppId],
									CASE WHEN tintDepositePage=2 THEN ''Unapplied Payment'' ELSE ''Credit Memo'' END AS [vcPOppName],
									0 [tintOppType],
									0 [numOppBizDocsId],
									''''  [vcBizDocID],
									(ISNULL(monDepositAmount,0) - ISNULL(monAppliedAmount,0)) * -1 AS TotalAmount,
									0 AmountPaid,
									(ISNULL(monDepositAmount,0) - ISNULL(monAppliedAmount,0)) * -1 AS BalanceDue,
									[dbo].[FormatedDateFromDate](DM.dtDepositDate,'+ CONVERT(VARCHAR(15),@numDomainId) +') AS DueDate,
									0 bitBillingTerms,
									0 intBillingDays,
									DM.dtCreationDate AS [datCreatedDate],
									'''' [varCurrSymbol],
									dbo.fn_GetComapnyName(DM.numDivisionId) vcCompanyName,
									'''' vcContactName,
									'''' AS RecordOwner,
									'''' AS AssignedTo,
									'''' AS vcCustPhone,
									ISNULL(DM.numCurrencyID,0) AS [numCurrencyID],
									'''' as vcRefOrderNo,
									NULL, 
									-1 AS [numJournal_Id],isnull(numReturnHeaderID,0) as numReturnHeaderID,
									'''' AS [vcTerms],
									0 AS [numDiscount],
									0 AS [numInterest],
									0 AS tintLeve,
									0 AS numChartAcntId
					FROM  DepositMaster DM 
					WHERE DM.numDomainId='+ CONVERT(VARCHAR(15),@numDomainId) +' AND tintDepositePage IN(2,3)  
					AND ((ISNULL(monDepositAmount,0) - ISNULL(monAppliedAmount,0)) > 0)
					AND (DM.numDivisionId = ' + CAST(@numDivisionId AS VARCHAR) + ' OR ' + CAST(@numDivisionId AS VARCHAR) + ' = 0)
					AND (DM.numAccountClass= ' + CAST(@numAccountClass AS VARCHAR) + ' OR ' + CAST(@numAccountClass AS VARCHAR) + '=0)
					AND CONVERT(DATE,DM.dtDepositDate) <= ''' + CAST(@dtToDate AS VARCHAr) + '''
					UNION
					SELECT 
						-1 [numOppId]
						,''Refund'' AS [vcPOppName]
						,0 [tintOppType]
						,0 [numOppBizDocsId]
						,''''  [vcBizDocID]
						,(CASE WHEN GJD.numDebitAmt > 0 THEN GJD.numDebitAmt ELSE -1 * GJD.numCreditAmt END) AS TotalAmount
						,0 AmountPaid
						,(CASE WHEN GJD.numDebitAmt > 0 THEN GJD.numDebitAmt ELSE -1 * GJD.numCreditAmt END) AS BalanceDue
						,[dbo].[FormatedDateFromDate](GJH.datEntry_Date,'+ CONVERT(VARCHAR(15),@numDomainId) +') AS DueDate
						,0 bitBillingTerms
						,0 intBillingDays
						,RH.dtCreatedDate AS [datCreatedDate]
						,'''' [varCurrSymbol]
						,dbo.fn_GetComapnyName(DIV.numDivisionId) vcCompanyName
						,'''' vcContactName
						,'''' AS RecordOwner
						,'''' AS AssignedTo
						,'''' AS vcCustPhone
						,ISNULL(DIV.numCurrencyID,0) AS [numCurrencyID]
						,'''' as vcRefOrderNo
						,NULL
						,GJH.numJournal_Id AS [numJournal_Id]
						,isnull(numReturnHeaderID,0) as numReturnHeaderID
						,'''' AS [vcTerms]
						,0 AS [numDiscount]
						,0 AS [numInterest]
						,0 AS tintLevel
						,GJD.numChartAcntId
					 FROM   dbo.ReturnHeader RH JOIN dbo.General_Journal_Header GJH ON RH.numReturnHeaderID = GJH.numReturnID
					 JOIN [dbo].[DivisionMaster] AS DIV ON DIV.[numDivisionID] = RH.[numDivisionId]
					JOIN General_Journal_Details GJD ON GJH.numJournal_Id = GJD.numJournalId 
					JOIN dbo.Chart_Of_Accounts COA ON COA.numAccountId = GJD.numChartAcntId AND COA.vcAccountCode LIKE ''0101010501''
					WHERE 
						RH.tintReturnType=4 
						AND RH.numDomainId=' + CAST(@numDomainId AS VARCHAR) + '  
						AND ISNULL(RH.numParentID,0) = 0
						AND ISNULL(RH.IsUnappliedPayment,0) = 0
						AND (monBizDocAmount > 0) AND ISNULL(RH.numBillPaymentIDRef,0)=0
						AND (RH.numDivisionId = ' + CAST(@numDivisionId AS VARCHAR) + ' OR ' + CAST(@numDivisionId AS VARCHAR) + ' = 0)
						AND (RH.numAccountClass= ' + CAST(@numAccountClass AS VARCHAR) + ' OR ' + CAST(@numAccountClass AS VARCHAR) + '=0)
						AND CONVERT(DATE,GJH.[datEntry_Date]) <= ''' + CAST(@dtToDate AS VARCHAr) + ''''
	            	
    IF (@vcFlag = '0+30')
      BEGIN
      
      SET @strSql =@strSql +
						'AND dateadd(DAY,CASE 
                                 --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                 WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                 ELSE 0
                               END,OB.dtFromDate) BETWEEN dbo.GetUTCDateWithoutTime() AND dateadd(DAY,31,dbo.GetUTCDateWithoutTime())
               AND dbo.GetUTCDateWithoutTime() <= dateadd(DAY,CASE 
                                                                --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                                WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                                ELSE 0
                                                              END,OB.dtFromDate)'
                                                              
		SET @strSql1 =@strSql1 + 'AND GJH.datEntry_Date BETWEEN dbo.GetUTCDateWithoutTime() AND dateadd(DAY,31,dbo.GetUTCDateWithoutTime())'
		
		SET @strCredit =@strCredit + 'AND DM.dtDepositDate BETWEEN dbo.GetUTCDateWithoutTime() AND dateadd(DAY,31,dbo.GetUTCDateWithoutTime())'
      END
    ELSE
      IF (@vcFlag = '30+60')
        BEGIN
         SET @strSql =@strSql +
                 'AND dateadd(DAY,CASE 
                                   --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                   WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                   ELSE 0
                                 END,OB.dtFromDate) BETWEEN dateadd(DAY,31,dbo.GetUTCDateWithoutTime()) AND dateadd(DAY,61,dbo.GetUTCDateWithoutTime())
                 AND dbo.GetUTCDateWithoutTime() <= dateadd(DAY,CASE 
                                                                  --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                                  WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                                  ELSE 0
                                                                END,OB.dtFromDate)'
		SET @strSql1 =@strSql1 + 'AND GJH.datEntry_Date BETWEEN dateadd(DAY,31,dbo.GetUTCDateWithoutTime()) AND dateadd(DAY,61,dbo.GetUTCDateWithoutTime())'
        
        SET @strCredit =@strCredit + 'AND DM.dtDepositDate BETWEEN dateadd(DAY,31,dbo.GetUTCDateWithoutTime()) AND dateadd(DAY,61,dbo.GetUTCDateWithoutTime())'
                 
        END
      ELSE
        IF (@vcFlag = '60+90')
          BEGIN
          SET @strSql =@strSql +
                   'AND dateadd(DAY,CASE 
                                     --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                     WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                     ELSE 0
                                   END,OB.dtFromDate) BETWEEN dateadd(DAY,61,dbo.GetUTCDateWithoutTime()) AND dateadd(DAY,91,dbo.GetUTCDateWithoutTime())
                   AND dbo.GetUTCDateWithoutTime() <= dateadd(DAY,CASE 
                                                                    --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                                    WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                                    ELSE 0
                                                                  END,OB.dtFromDate)'
			SET @strSql1 =@strSql1 + 'AND GJH.datEntry_Date BETWEEN dateadd(DAY,61,dbo.GetUTCDateWithoutTime()) AND dateadd(DAY,91,dbo.GetUTCDateWithoutTime())'
			
			SET @strCredit =@strCredit + 'AND DM.dtDepositDate BETWEEN dateadd(DAY,61,dbo.GetUTCDateWithoutTime()) AND dateadd(DAY,91,dbo.GetUTCDateWithoutTime())'
			
          END
        ELSE
          IF (@vcFlag = '90+')
            BEGIN
             SET @strSql =@strSql +
                     'AND dateadd(DAY,CASE 
                                       --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                       WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                       ELSE 0
                                     END,OB.dtFromDate) > dateadd(DAY,91,dbo.GetUTCDateWithoutTime())
                     AND dbo.GetUTCDateWithoutTime() <= dateadd(DAY,CASE 
                                                                      --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                                      WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                                      ELSE 0
                                                                    END,OB.dtFromDate)'
			SET @strSql1 =@strSql1 + 'AND GJH.datEntry_Date  > dateadd(DAY,91,dbo.GetUTCDateWithoutTime())'
            
            SET @strCredit =@strCredit + 'AND DM.dtDepositDate  > dateadd(DAY,91,dbo.GetUTCDateWithoutTime())'
                     
            END
          ELSE
            IF (@vcFlag = '0-30')
              BEGIN
               SET @strSql =@strSql +
                       'AND dbo.GetUTCDateWithoutTime() > dateadd(DAY,CASE 
                                                                       --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                                       WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                                       ELSE 0
                                                                     END,OB.dtFromDate)
                       AND datediff(DAY,dateadd(DAY,CASE 
                                                      --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                      WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                      ELSE 0
                                                    END,OB.dtFromDate),dbo.GetUTCDateWithoutTime()) BETWEEN 0 AND 30 '
				SET @strSql1 =@strSql1 + 'AND datediff(DAY,GJH.datEntry_Date, dbo.GetUTCDateWithoutTime()) BETWEEN 0 AND 30 '
				
				SET @strCredit =@strCredit + 'AND datediff(DAY,DM.dtDepositDate, dbo.GetUTCDateWithoutTime()) BETWEEN 0 AND 30 '
				
              END
            ELSE
              IF (@vcFlag = '30-60')
                BEGIN
                  SET @strSql =@strSql +
                         'AND dbo.GetUTCDateWithoutTime() > dateadd(DAY,CASE 
                                                                         --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                                         WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                                         ELSE 0
                                                                       END,OB.dtFromDate)
                         AND datediff(DAY,dateadd(DAY,CASE 
                                                        --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                        WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                        ELSE 0
                                                      END,OB.dtFromDate),dbo.GetUTCDateWithoutTime()) BETWEEN 31 AND 60'
                SET @strSql1 =@strSql1 + 'AND datediff(DAY,GJH.datEntry_Date, dbo.GetUTCDateWithoutTime()) BETWEEN 31 AND 60 '
                
                SET @strCredit =@strCredit + 'AND datediff(DAY,DM.dtDepositDate, dbo.GetUTCDateWithoutTime()) BETWEEN 31 AND 60 '
                
                END
              ELSE
                IF (@vcFlag = '60-90')
                  BEGIN
					SET @strSql =@strSql +
                           'AND dbo.GetUTCDateWithoutTime() > dateadd(DAY,CASE 
                                                                           --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                                           WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                                           ELSE 0
                                                                         END,OB.dtFromDate)
                           AND datediff(DAY,dateadd(DAY,CASE 
                                                          --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                          WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                          ELSE 0
                                                        END,OB.dtFromDate),dbo.GetUTCDateWithoutTime()) BETWEEN 61 AND 90'
					SET @strSql1 =@strSql1 + 'AND datediff(DAY,GJH.datEntry_Date, dbo.GetUTCDateWithoutTime()) BETWEEN 61 AND 90 '
                    
                    SET @strCredit =@strCredit + 'AND datediff(DAY,DM.dtDepositDate, dbo.GetUTCDateWithoutTime()) BETWEEN 61 AND 90 '
                           
                  END
                ELSE
                  IF (@vcFlag = '90-')
                    BEGIN
                      SET @strSql =@strSql +
                             'AND dbo.GetUTCDateWithoutTime() > dateadd(DAY,CASE 
                                                                             --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                                             WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																															   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                                             ELSE 0
                                                                           END,OB.dtFromDate)
                             AND datediff(DAY,dateadd(DAY,CASE 
                                                            --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                            WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																				   WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                            ELSE 0
                                                          END,OB.dtFromDate),dbo.GetUTCDateWithoutTime()) > 90'
					SET @strSql1 =@strSql1 + 'AND datediff(DAY,GJH.datEntry_Date, dbo.GetUTCDateWithoutTime()) > 90'
                     
                    SET @strCredit =@strCredit + 'AND datediff(DAY,DM.dtDepositDate, dbo.GetUTCDateWithoutTime()) > 90'   
                    END
     
	        
    SET @strSql =@strSql + @strSql1 +  @strCredit

	

    SET @strSql = CONCAT(@strSql,' SELECT *,CASE WHEN DATEDIFF (day ,[dbo].[FormatedDateFromDate](DueDate,',@numDomainID,') ,GETDATE()) > 0 
										  THEN DATEDIFF (day ,[dbo].[FormatedDateFromDate](DueDate,',@numDomainID,') ,GETDATE()) 
										  ELSE NULL 
									 END AS DaysLate,
									 CASE WHEN DATEDIFF (day ,[dbo].[FormatedDateFromDate](DueDate,',@numDomainID,') ,GETDATE()) > 0 
										  THEN + ''',' Past Due (', ''' + CAST(DATEDIFF (day ,[dbo].[FormatedDateFromDate](DueDate,',@numDomainID,') ,GETDATE()) AS VARCHAR(10))+''',')','''
										  ELSE NULL 
									 END AS [Status(DaysLate)],
									 CASE WHEN ',@baseCurrency,' <> numCurrencyID THEN 2 ELSE 1 END AS CurrencyCount,
									 dbo.fn_GetChart_Of_AccountsName(numChartAcntId) vcARAccount
							FROM #TempRecords WHERE (ISNULL(BalanceDue,0) <> 0 or [numOppId]=-1)')
	
	IF ISNULL(@vcRegularSearch,'') <> ''
	BEGIN
		SET @strSql = CONCAT(@strSql,' AND ',@vcRegularSearch)
	END
 
   SET @strSql = CONCAT(@strSql,' DROP TABLE #TempRecords')
   
	PRINT CAST(@strSql AS NTEXt)
	EXEC(@strSql)
	        
     
  END
/****** Object:  StoredProcedure [dbo].[USP_GetBizDocAttachmnts]    Script Date: 07/26/2008 16:16:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj      
--exec USP_GetBizDocAttachmnts,
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getbizdocattachmnts')
DROP PROCEDURE usp_getbizdocattachmnts
GO
CREATE PROCEDURE [dbo].[usp_getbizdocattachmnts]      
 @numBizDocID as numeric(9)=0,      
 @numDomainId as numeric(9)=0,      
 @numAttachmntID as numeric(9)=0,
 @numBizDocTempID as numeric(9)=0     ,
 @intAddReferenceDocument AS NUMERIC(10)=0
as      
       
if (@numAttachmntID=0 and   @numBizDocID <>0)  
begin    
    
 if not exists(select *,'' AS vcFileName from BizDocAttachments where numBizDocID=@numBizDocID and numDomainID=@numDomainID and vcDocName='BizDoc' and isnull(numBizDocTempID,0)=@numBizDocTempID)        
  begin        
   insert into BizDocAttachments (numBizDocID,numDomainID,vcURL,numOrder,vcDocName,numBizDocTempID)        
   values (@numBizDocID,@numDomainID,'BizDoc',1,'BizDoc',@numBizDocTempID)        
  end       
     
 
  IF(@intAddReferenceDocument=1)
  BEGIN
	 (select numAttachmntID,vcDocName,vcURL,numBizDocID,numBizDocTempID,numOrder,(SELECT TOP 1 vcBizDocId FROM OpportunityBizDocs WHERE numOppBizDocsId=@numBizDocID) AS vcFileName from BizDocAttachments       
	 where numBizDocID=@numBizDocID and numDomainId=@numDomainId and isnull(numBizDocTempID,0)=@numBizDocTempID)
	 UNION
	 (SELECT numAttachmntID,vcDocName,vcURL,numBizDocID,numBizDocTempID,numOrder,'' as vcFileName from BizDocAttachments       
	 where vcURL<>'BizDoc' AND numBizDocID=(SELECT TOP 1 numBizDocId FROM OpportunityBizDocs WHERE numOppBizDocsId=@numBizDocID) and numDomainId=@numDomainId and isnull(numBizDocTempID,0)=(SELECT TOP 1 numBizDocTempID FROM OpportunityBizDocs WHERE numOppBizDocsId=@numBizDocID)  )
	order by numOrder 
  END
  ELSE
  BEGIN
	 select numAttachmntID,vcDocName,vcURL,numBizDocID,numBizDocTempID,'' AS vcFileName from BizDocAttachments       
  where numBizDocID=@numBizDocID and numDomainId=@numDomainId and isnull(numBizDocTempID,0)=@numBizDocTempID order by numOrder      
  END
end    
else if @numAttachmntID>0      
      
select *,'' AS vcFileName from BizDocAttachments where numAttachmntID=@numAttachmntID
GO

/****** Object:  StoredProcedure [dbo].[USP_GetCartItem]    Script Date: 11/08/2011 18:00:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetCartItem' ) 
                    DROP PROCEDURE USP_GetCartItem
                    Go
CREATE PROCEDURE [dbo].[USP_GetCartItem]
(
	@numUserCntId numeric(18,0),
	@numDomainId numeric(18,0),
	@vcCookieId varchar(100),
	@bitUserType BIT =0--if 0 then anonomyous user 1 for logi user
)
AS
	IF @numUserCntId <> 0
	BEGIN
		SELECT DISTINCT numCartId,
			   numParentItemCode,
			   numUserCntId,
			   [dbo].[CartItems].numDomainId,
			   vcCookieId,
			   numOppItemCode,
			   [dbo].[CartItems].numItemCode,
			   (SELECT TOP 1 numCategoryId  FROM dbo.ItemCategory WHERE numItemID =  CartItems.numItemCode AND [dbo].[CartItems].numDomainId = @numDomainId ) AS numCategoryId,
			   (SELECT vcCategoryName FROM [dbo].[Category] WHERE [Category].[numCategoryID]  = (SELECT TOP 1 numCategoryId  FROM dbo.ItemCategory WHERE numItemID =  CartItems.numItemCode AND [dbo].[CartItems].numDomainId = @numDomainId )) AS vcCategoryName,
			   numUnitHour,
			   monPrice,
			   monPrice AS [monListPrice],
			   numSourceId,
			   vcItemDesc,
			   vcItemDesc AS txtItemDesc,
			   [dbo].[CartItems].numWarehouseId,
			   [dbo].[CartItems].vcItemName,
			   vcWarehouse,
			   numWarehouseItmsID,
			   vcItemType,
			   vcAttributes,
			   vcAttrValues,
			   I.bitFreeShipping,
			   fltWeight as numWeight,
			   ISNULL((SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numDomainId=@numDomainId AND bitDefault=1 AND numItemCode=I.numItemCode),'') AS vcPathForTImage,
			   tintOpFlag,
			   bitDiscountType,
			   fltDiscount,
			   monTotAmtBefDiscount,
			   ItemURL,
			   numUOM,
			   vcUOMName,
			   decUOMConversionFactor,
			   numHeight,
			   numLength,
			   numWidth,
			   vcShippingMethod,
			   numServiceTypeId,
			   decShippingCharge,
			   numShippingCompany,
			   vcCoupon,
			   tintServicetype,CAST( dtDeliveryDate AS VARCHAR(30)) dtDeliveryDate,monTotAmount,
			   ISNULL(monTotAmtBefDiscount,0) - ISNULL(monTotAmount,0) AS monTotalDiscountAmount,
			   (SELECT vcModelID FROM dbo.Item WHERE Item.numItemCode =  CartItems.numItemCode) AS [vcModelID]
			   ,(SELECT TOP 1 vcPathForTImage FROM dbo.ItemImages II WHERE II.numItemCode = CartItems.numItemCode AND II.numDomainId= CartItems.numDomainId AND II.bitDefault=1) As vcPathForTImage
			   ,ISNULL(vcUpSellDesc,'') AS [txtUpSellItemDesc],ISNULL(bitPreUpSell,0) [bitPreUpSell], ISNULL(bitPostUpSell,0) [bitPostUpSell], ISNULL(vcUpSellDesc,'') [vcUpSellDesc]
			   ,ISNULL([I].[vcSKU],'') AS [vcSKU]
			   ,I.vcManufacturer
			   ,ISNULL(PromotionID,0) AS PromotionID
			   ,CAST(ISNULL(PromotionDesc,'') AS varchar) AS PromotionDesc
			   ,ISNULL((SELECT IsOrderBasedPromotion FROM PromotionOffer WHERE numProId = [CartItems].PromotionID),0) AS IsOrderBasedPromotion
			   ,I.txtItemDesc AS txtItemDesc,ISNULL(vcChildKitItemSelection,'') vcChildKitItemSelection,
			   (CASE WHEN ISNULL(bitKitParent,0) = 1 THEN dbo.GetCartItemInclusionDetails(CartItems.numCartId) ELSE '' END) vcInclusionDetails,
			   I.numItemClassification
			   FROM CartItems 
			   LEFT JOIN [dbo].[SimilarItems] AS SI ON SI.[numItemCode] = [dbo].[CartItems].[numItemCode] AND SI.numParentItemCode = CartItems.numParentItem
			   LEFT JOIN [dbo].[Item] AS I ON I.[numItemCode] = [dbo].[CartItems].[numItemCode]
			   --LEFT JOIN [dbo].[WareHouseItems] AS WHI ON WHI.[numItemID] = I.[numItemCode]
			   WHERE [dbo].[CartItems].numDomainId = @numDomainId AND numUserCntId = @numUserCntId 	
				
	END
	ELSE
	BEGIN
		SELECT DISTINCT numCartId,
			   numParentItemCode,
			   numUserCntId,
			   [dbo].[CartItems].numDomainId,
			   vcCookieId,
			   numOppItemCode,
			   [dbo].[CartItems].numItemCode,
			   (SELECT TOP 1 numCategoryId  FROM dbo.ItemCategory WHERE numItemID =  CartItems.numItemCode AND CartItems.numDomainId =@numDomainId ) AS numCategoryId,
			   (SELECT vcCategoryName FROM [dbo].[Category] WHERE [Category].[numCategoryID]  = (SELECT TOP 1 numCategoryId  FROM dbo.ItemCategory WHERE numItemID =  CartItems.numItemCode AND [dbo].[CartItems].numDomainId = @numDomainId )) AS vcCategoryName,
			   numUnitHour,
			   monPrice,
			   monPrice AS [monListPrice],
			   numSourceId,
			   vcItemDesc,
			   vcItemDesc AS txtItemDesc,
			   [dbo].[CartItems].numWarehouseId,
			   [dbo].[CartItems].vcItemName,
			   vcWarehouse,
			   numWarehouseItmsID,
			   vcItemType,
			   vcAttributes,
			   vcAttrValues,
			   I.bitFreeShipping,
			   fltWeight as numWeight,
			   ISNULL((SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numDomainId=@numDomainId AND bitDefault=1 AND numItemCode=I.numItemCode),'') AS vcPathForTImage,
			   tintOpFlag,
			   bitDiscountType,
			   fltDiscount,
			   monTotAmtBefDiscount,
			   ItemURL,
			   numUOM,
			   vcUOMName,
			   decUOMConversionFactor,
			   numHeight,
			   numLength,
			   numWidth,
			   vcShippingMethod,
			   numServiceTypeId,
			   decShippingCharge,
			   numShippingCompany,
			   tintServicetype,
			   vcCoupon,
			   CAST( dtDeliveryDate AS VARCHAR(30)) dtDeliveryDate,
			   monTotAmount,
			   ISNULL(monTotAmtBefDiscount,0) - ISNULL(monTotAmount,0) AS monTotalDiscountAmount,
			   (SELECT vcModelID FROM dbo.Item WHERE Item.numItemCode =  CartItems.numItemCode) AS [vcModelID]
			   ,(SELECT TOP 1 vcPathForTImage FROM dbo.ItemImages II WHERE II.numItemCode = CartItems.numItemCode AND II.numDomainId= CartItems.numDomainId AND II.bitDefault=1) As vcPathForTImage
			   , ISNULL(vcUpSellDesc,'') AS [txtUpSellItemDesc],ISNULL(bitPreUpSell,0) [bitPreUpSell], ISNULL(bitPostUpSell,0) [bitPostUpSell], ISNULL(vcUpSellDesc,'') [vcUpSellDesc]
			   ,ISNULL([I].[vcSKU],'') AS [vcSKU]
			   ,I.vcManufacturer
			   ,ISNULL(PromotionID,0) AS PromotionID
			   ,CAST(ISNULL(PromotionDesc,'') AS VARCHAR) AS PromotionDesc
			   ,ISNULL((SELECT IsOrderBasedPromotion FROM PromotionOffer WHERE numProId = [CartItems].PromotionID),0) AS IsOrderBasedPromotion
			   ,ISNULL(vcChildKitItemSelection,'') vcChildKitItemSelection
			   ,(CASE WHEN ISNULL(bitKitParent,0) = 1 THEN dbo.GetCartItemInclusionDetails(CartItems.numCartId) ELSE '' END) vcInclusionDetails,
			   I.numItemClassification
			   FROM CartItems
			   LEFT JOIN [dbo].[SimilarItems] AS SI ON SI.[numItemCode] = [dbo].[CartItems].[numItemCode] AND SI.numParentItemCode = CartItems.numParentItem
			   LEFT JOIN [dbo].[Item] AS I ON I.[numItemCode] = [dbo].[CartItems].[numItemCode]
			   --LEFT JOIN [dbo].[WareHouseItems] AS WHI ON WHI.[numItemID] = I.[numItemCode]
			   WHERE [dbo].[CartItems].numDomainId = @numDomainId AND numUserCntId = @numUserCntId AND vcCookieId = @vcCookieId	
				
	END
	
	

--exec USP_GetCartItem @numUserCntId=1,@numDomainId=1,@vcCookieId='fc3d604b-25fa-4c25-960c-1e317768113e',@bitUserType=NULL


/****** Object:  StoredProcedure [dbo].[USP_GetCategory]    Script Date: 08/08/2009 16:37:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                        
-- exec [dbo].[USP_GetCategory] 0,0,'',0,1,3
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcategory')
DROP PROCEDURE usp_getcategory
GO
CREATE PROCEDURE [dbo].[USP_GetCategory]                        
@numCatergoryId as numeric(9),                        
@byteMode as tinyint ,                  
@str as varchar(1000),                  
@numDomainID as numeric(9)=0,
@numSiteID AS NUMERIC(9)=0 ,
@numCurrentPage as numeric(9)=0,
@PageSize AS INT=20,
@numTotalPage as numeric(9) OUT,    
@numItemCode AS NUMERIC(9,0),
@SortChar CHAR(1) = '0',
@numCategoryProfileID NUMERIC(18,0)
               
as   

set nocount on                     
--selecting all categories                     
declare @strsql as varchar(8000)
DECLARE @strRowCount VARCHAR(8000)  


create table #TempCategory
(PKId numeric(9) identity,
numItemCode numeric(9),
vcItemName varchar(250));

                  
if @byteMode=0                        
BEGIN
	 ;WITH CategoryList
As 
( SELECT C1.numCategoryID,C1.vcCategoryName,C1.vcCategoryNameURL,C1.numDepCategory,C1.tintLevel,case when C1.bitLink=0 then 'No' when C1.bitLink=1 then 'Yes' end as Link,C1.vcLink as vcLink,CAST('' AS VARCHAR(1000)) as DepCategory,
	C1.intDisplayOrder,C1.vcDescription,
	CASE WHEN LEN(ISNULL(C1.vcDescription ,'')) > 0 THEN  
	CASE WHEN LEN(C1.vcDescription) > 40 THEN SUBSTRING(C1.vcDescription,0,40) + ' [...]' 
         ELSE C1.vcDescription + ' [...]'	END 
    ELSE
		 ISNULL(C1.vcDescription ,'')   
    END     
         AS vcShortDescription,1 AS [Level],
	CAST (REPLICATE('.',1) + C1.vcCategoryName as varchar(1000) )  as vcNewCategoryName,
	CAST(C1.numCategoryID AS VARCHAR(MAX)) AS [Order]
 FROM dbo.Category C1 WHERE C1.numDepCategory=0 AND  C1.numDomainID=@numDomainID AND C1.numCategoryProfileID = @numCategoryProfileID
 UNION ALL
 SELECT  C1.numCategoryID,C1.vcCategoryName,C1.vcCategoryNameURL,C1.numDepCategory,C1.tintLevel,case when C1.bitLink=0 then 'No' when C1.bitLink=1 then 'Yes' end as Link,C1.vcLink as vcLink,CL.vcCategoryName as DepCategory ,
	C1.intDisplayOrder,C1.vcDescription,
	CASE WHEN LEN(ISNULL(C1.vcDescription ,'')) > 0 THEN  
	CASE WHEN LEN(C1.vcDescription) > 40 THEN SUBSTRING(C1.vcDescription,0,40) + ' [...]' 
         ELSE C1.vcDescription + ' [...]'	END 
    ELSE
		 ISNULL(C1.vcDescription ,'')   
    END     
         AS vcShortDescription,CL.[Level] + 1 AS [Level],
	CAST (REPLICATE('.',[Level] * 3) + C1.vcCategoryName as varchar(1000) )  as vcNewCategoryName,
	[Order] + '.' + CAST(C1.numCategoryID AS VARCHAR(MAX)) AS [Order]
 FROM dbo.Category C1 
 INNER JOIN CategoryList CL
 ON CL.numCategoryID = C1.numDepCategory  WHERE  C1.numDomainID=@numDomainID  AND C1.numCategoryProfileID = @numCategoryProfileID
)   

SELECT *,(SELECT COUNT(*) FROM [SiteCategories] SC WHERE SC.[numSiteID] =@numSiteID AND SC.[numCategoryID]= C1.[numCategoryID]) AS ShowInECommerce,
ISNULL((SELECT COUNT(*) FROM [ItemCategory] WHERE [numCategoryID] = C1.[numCategoryID]),0) AS ItemCount FROM CategoryList C1 ORDER BY [Order]
end                                                
--deleting a category                      
else if @byteMode=1                        
begin     
	delete from SiteCategories where numCategoryID=@numCatergoryId                     
	delete from ItemCategory where numCategoryID=@numCatergoryId
	delete from Category where numCategoryID=@numCatergoryId       
	select 1                        
end                        
--selecting all catories where level is 1                       
else if @byteMode=2                      
begin                        
	select numCategoryID,vcCategoryName,vcCategoryNameURL,intDisplayOrder,vcDescription from Category                      
	where ((tintlevel=1 )  or (tintlevel<>1 and numDepCategory=@numCatergoryId))  and numCategoryID!=@numCatergoryId                      
	and numDomainID=@numDomainID                    
	ORDER BY [vcCategoryName]
end                      
--selecting subCategories                      
else if @byteMode=3                      
begin                        
	select numCategoryID,vcCategoryName,vcCategoryNameURL,intDisplayOrder,vcDescription  from Category                      
	where numDepCategory=@numCatergoryId                      
	ORDER BY [vcCategoryName]
end                      
                      
--selecting Items                      
if @byteMode=4                      
begin                        
	select numItemCode,vcItemName  from Item           
	where numDomainID=@numDomainID                       
end                      
                      
--selecting Item belongs to a category                      
else if @byteMode=5                      
begin    


	insert into #TempCategory
	                       
	select numItemCode,CONCAT(numItemCode,' - ',vcItemName) AS vcItemName  from Item                      
	join ItemCategory                       
	on numItemCode=numItemID                      
	where numCategoryID=@numCatergoryId           
	and numDomainID=@numDomainID                       
	ORDER BY [vcItemName]


	select @numTotalPage =COUNT(*) from #TempCategory

	if @numTotalPage<@PageSize 
		begin
			set @numTotalPage=1
		end
	else	
		begin
			set @numTotalPage=@numTotalPage/@PageSize
		end

	select * from #TempCategory  where PKId between (@numCurrentPage*@PageSize)- (@PageSize-1) and  @numCurrentPage*@PageSize

	select @numTotalPage

end                      
                    
else if @byteMode=6                    
begin                        

	select C1.numCategoryID,c1.vcCategoryName,((select Count(*) from Category C2 where C2.numDepCategory=C1.numCategoryID)+(select count(*) from ItemCategory C3 where C3.numCategoryID=C1.numCategoryID  ) ) as [Count],convert(integer,bitLink) as Type,vcLink,C1.intDisplayOrder,
	C1.vcDescription   
	from Category C1                     
	where tintlevel=1                     
	and numDomainID=@numDomainID   
	AND numCategoryProfileID=@numCategoryProfileID                   
                    
end                     
                    
else if @byteMode=7                    
begin                        

	select C1.numCategoryID,c1.vcCategoryName,((select Count(*) from Category C2 where C2.numDepCategory=C1.numCategoryID)+(select count(*) from ItemCategory C3 where C3.numCategoryID=C1.numCategoryID ) ) as [Count],convert(integer,bitLink) as Type,vcLink,C1.intDisplayOrder,
	C1.vcDescription,numDepCategory as Category
	from Category C1                     
	where C1.numDepCategory=@numCatergoryId and numDomainID=@numDomainID                      
	union                     
	select numItemCode,vcItemName,0 ,2,'',0,'',0  from Item                      
	join ItemCategory                       
	on numItemCode=numItemID                      
	where numCategoryID=@numCatergoryId  and numDomainID=@numDomainID                    
end                     
                
else if @byteMode=8                     
begin                    
     --kishan               
	set @strsql='select numItemCode,vcItemName,txtItemDesc,0,
	(
	SELECT TOP 1 II.vcPathForTImage 
    FROM dbo.ItemImages II 
        WHERE II.numItemCode = Item.numItemCode 
    AND II.bitDefault = 1 , AND II.bitIsImage = 1 
    AND II.numDomainID ='+ cast(@numDomainId as varchar(50))+') 
	vcPathForTImage ,
	1 as Type,monListPrice as price,0 as numQtyOnHand  from Item                    
	join ItemCategory                       
	on numItemCode=numItemID                      
	where numCategoryID in('+@str+')'                  
	exec (@strsql)                   
end                    
                  
else if @byteMode=9                  
begin                    
                    
	select distinct(numItemCode),vcItemName,txtItemDesc,0 , (SELECT TOP 1 vcPathForImage FROM ItemImages WHERE numDomainId = @numDomainId AND bitDefault = 1 AND numItemCode = Item.numItemCode) AS vcPathForImage,1 as Type,monListPrice as price,0 as numQtyOnHand from Item                      
	join ItemCategory                       
	on numItemCode=numItemID                      
	where ( vcItemName like '%'+@str+'%' or txtItemDesc like '%'+@str+'%') and numDomainID= @numDomainID                                    
end        
      
else if @byteMode=10      
begin       
	select ROW_NUMBER() OVER (ORDER BY Category Asc) AS RowNumber,* from (select   C1.numCategoryID,c1.vcCategoryName,convert(integer,bitLink) as Type,vcLink ,numDepCategory as Category,C1.intDisplayOrder,
	C1.vcDescription         
	from Category C1                     
	where  numDomainID=@numDomainID)X       
      
end 


--selecting Items not present in Category                   
else if @byteMode=11                    
begin     

	insert into #TempCategory
	         
	SELECT I.numItemCode, CONCAT(I.numItemCode,' - ',I.vcItemName) AS vcItemName FROM item  I LEFT OUTER JOIN [ItemCategory] IC ON IC.numItemID = I.numItemCode
	WHERE [numDomainID] =@numDomainID AND ISNULL(IC.numCategoryID,0) <> @numCatergoryId
	ORDER BY [vcItemName]
          
--	select numItemCode,vcItemName  from Item LEFT OUTER JOIN [ItemCategory] ON Item.[numItemCode] = [ItemCategory].[numCategoryID]
--	where [ItemCategory].[numCategoryID] <> @numCatergoryId
----numItemCode not in (select numItemID from ItemCategory where numCategoryID=@numCatergoryId)
--	and numDomainID=@numDomainID                      
--	ORDER BY [vcItemName]



	select @numTotalPage =COUNT(*) from #TempCategory

	set @numTotalPage=@numTotalPage/@PageSize

	select * from #TempCategory  where PKId between (@numCurrentPage*@PageSize)-(@PageSize-1) and  @numCurrentPage*@PageSize

	select @numTotalPage


end   


--selecting Items From Item Groups                  
else if @byteMode=12                
begin   

	insert into #TempCategory
	                     
	select numItemCode,vcItemName  from Item 
	where numItemGroup= @numCatergoryId  
	ORDER BY [vcItemName] 



	select @numTotalPage =COUNT(*) from #TempCategory

	set @numTotalPage=@numTotalPage/@PageSize

	select * from #TempCategory  where PKId between (@numCurrentPage*@PageSize)-(@PageSize-1) and  @numCurrentPage*@PageSize

	select @numTotalPage

              
end

-- Selecting Categories Site Wise and Sorted into Sort Order 
else if @byteMode=13
BEGIN
	SELECT  
		@numCategoryProfileID=CPS.numCategoryProfileID
		,@numDomainID=S.numDomainID
	FROM 
		Sites S
	JOIN
		CategoryProfileSites CPS
	ON
		S.numSiteID=CPS.numSiteID
	JOIN
		CategoryProfile CP
	ON
		CPS.numCategoryProfileID = CP.ID
	WHERE 
		S.numSiteID=@numSiteID


	DECLARE @bParentCatrgory BIT;
	SELECT @bParentCatrgory=vcAttributeValue FROM PageElementDetail WHERE [numSiteID] = @numSiteID AND numAttributeID=39
	SET @bParentCatrgory=ISNULL(@bParentCatrgory,0)

	DECLARE @tintDisplayCategory AS TINYINT
	SELECT @tintDisplayCategory = ISNULL(bitDisplayCategory,0) FROM dbo.eCommerceDTL WHERE numSiteID = @numSiteId

	
	IF ISNULL(@tintDisplayCategory,0) = 0 
	BEGIN
		SELECT C.numCategoryID,
			   C.vcCategoryName,
			   C.tintLevel,
			   convert(integer,C.bitLink) as TYPE,
			   C.vcLink AS vcLink,
			   numDepCategory as Category,C.intDisplayOrder,C.vcDescription 
		FROM   [SiteCategories] SC
			   INNER JOIN [Category] C ON SC.[numCategoryID] = C.[numCategoryID]
		WHERE  SC.[numSiteID] = @numSiteID
			   AND SC.[numCategoryID] = C.[numCategoryID] 
			   AND 1 = ( CASE WHEN @bParentCatrgory = 1 THEN tintLevel ELSE 1 END )
			   ORDER BY tintLevel , 
						ISNULL(C.intDisplayOrder,0),
						Category	
	END	
	ELSE
	BEGIN
		SELECT C.numCategoryID,
			   C.vcCategoryName,
			   C.tintLevel,
			   convert(integer,C.bitLink) as TYPE,
			   C.vcLink AS vcLink,
			   numDepCategory as Category,C.intDisplayOrder,C.vcDescription 
		FROM   [SiteCategories] SC
			   INNER JOIN [Category] C ON SC.[numCategoryID] = C.[numCategoryID]
			   LEFT JOIN ItemCategory IC ON IC.numCategoryID=C.numCategoryID
			   LEFT JOIN WareHouseItems WI ON WI.numDomainID=@numDomainID AND IC.numItemID=WI.numItemID  
		WHERE  SC.[numSiteID] = @numSiteID
			   AND SC.[numCategoryID] = C.[numCategoryID] 
			   AND 1 = ( CASE WHEN @bParentCatrgory = 1 THEN tintLevel ELSE 1 END )
			   AND ((ISNULL(numOnHand,0) > 0 OR ISNULL(numAllocation,0) > 0) OR tintLevel = 1)
		GROUP BY
			C.numCategoryID,
			C.vcCategoryName,
			C.tintLevel,
			C.bitLink,
			C.vcLink,
			numDepCategory,C.intDisplayOrder,C.vcDescription			   
		ORDER BY 
			tintLevel
			,ISNULL(C.intDisplayOrder,0)
			,Category
	END			

END

else if @byteMode=14
BEGIN

	SELECT C.numCategoryID,
		   C.vcCategoryName,
		   C.tintLevel,
		   convert(integer,C.bitLink) as TYPE,
		   C.vcLink AS vcLink,
		   numDepCategory as Category,C.intDisplayOrder,C.vcPathForCategoryImage,C.vcDescription 
	FROM   [SiteCategories] SC
		   INNER JOIN [Category] C
			 ON SC.[numCategoryID] = C.[numCategoryID]
	WHERE  SC.[numSiteID] = @numSiteID
		   AND SC.[numCategoryID] = C.[numCategoryID]
		   AND ISNULL(numDepCategory,0) = 0
		 ORDER BY ISNULL(C.intDisplayOrder,0),Category
END

else if @byteMode=15
BEGIN

DECLARE @bSubCatrgory BIT;

SELECT @bSubCatrgory=vcAttributeValue FROM PageElementDetail WHERE [numSiteID] = @numSiteID AND numAttributeID=40
SET @bSubCatrgory=ISNULL(@bSubCatrgory,0)

	SELECT C.numCategoryID,
		   C.vcCategoryName,
		   C.tintLevel,
		   convert(integer,C.bitLink) as TYPE,
		   C.vcLink AS vcLink,
		   numDepCategory as Category,C.intDisplayOrder,C.vcPathForCategoryImage,C.vcDescription 
		   
		   
	FROM   [SiteCategories] SC
		   INNER JOIN [Category] C
			 ON SC.[numCategoryID] = C.[numCategoryID]
	WHERE  SC.[numSiteID] = @numSiteID
		   AND SC.[numCategoryID] = C.[numCategoryID] AND numDepCategory=@numCatergoryId
		   AND 1=(CASE WHEN @bSubCatrgory=1 THEN 1 ELSE 0 END)
		    ORDER BY ISNULL(C.intDisplayOrder,0),Category
END

ELSE if @byteMode=16
BEGIN
 SELECT vcPathForCategoryImage FROM Category WHERE numCategoryID=@numCatergoryId AND numDomainID=@numDomainID
 
END
ELSE if @byteMode=17
BEGIN
 SELECT numCategoryID ,vcCategoryName, vcCategoryNameURL ,vcDescription,intDisplayOrder,vcPathForCategoryImage,numDepCategory,ISNULL(vcMetaTitle,'') vcMetaTitle,ISNULL(vcMetaKeywords,'') vcMetaKeywords,ISNULL(vcMetaDescription,'') vcMetaDescription FROM Category  WHERE numCategoryID = @numCatergoryId AND numDomainID=@numDomainID
END
ELSE IF @byteMode = 18
BEGIN
	SELECT
		*
	FROM
	(
		-- USED FOR HIERARCHICAL CATEGORY LIST
		SELECT 
			ROW_NUMBER() OVER ( ORDER BY vcCategoryName) AS RowNum,
			C1.numCategoryID,
			C1.vcCategoryName,
			C1.vcCategoryNameURL,
			NULLIF(numDepCategory,0) numDepCategory,
			ISNULL(numDepCategory,0) numDepCategory1,
			0 AS tintLevel,
			ISNULL((SELECT COUNT(*) FROM [ItemCategory] WHERE [numCategoryID] = C1.[numCategoryID]),0) AS ItemCount,
			ISNULL((SELECT COUNT(*) FROM [ItemCategory] WHERE [numCategoryID] IN (SELECT numCategoryID FROM category WHERE numDepCategory = C1.[numCategoryID])),0) AS ItemSubcategoryCount
			,ISNULL(bitVisible,1) bitVisible
			,ISNULL(STUFF((SELECT 
								CONCAT(', ',IG.vcItemGroup)
							FROM
								CategoryMatrixGroup CMG
							INNER JOIN
								ItemGroups IG
							ON
								CMG.numItemGroup = IG.numItemGroupID
							WHERE
								CMG.numDomainID=@numDomainID
								AND CMG.numCategoryID=C1.numCategoryID
							order by IG.vcItemGroup
							for xml path('')
						),1,1,''),'') vcMatrixGroups
		FROM 
			Category C1 
		WHERE 
			numDomainID =@numDomainID AND numCategoryProfileID = @numCategoryProfileID
	) TEMp
	ORDER BY
		vcCategoryName
END

ELSE IF @byteMode=19
BEGIN
	SELECT  
		@numCategoryProfileID=CPS.numCategoryProfileID
	FROM 
		Sites S
	JOIN
		CategoryProfileSites CPS
	ON
		S.numSiteID=CPS.numSiteID
	JOIN
		CategoryProfile CP
	ON
		CPS.numCategoryProfileID = CP.ID
	WHERE 
		S.numSiteID=@numSiteID

	BEGIN
	 ;WITH CategoryList
As 
( SELECT C1.numCategoryID,C1.vcCategoryName,C1.numDepCategory,C1.tintLevel,case when C1.bitLink=0 then 'No' when C1.bitLink=1 then 'Yes' end as Link,C1.vcLink as vcLink,CAST('' AS VARCHAR(1000)) as DepCategory,
	C1.intDisplayOrder,C1.vcDescription,
	CASE WHEN LEN(ISNULL(C1.vcDescription ,'')) > 0 THEN  
	CASE WHEN LEN(C1.vcDescription) > 40 THEN SUBSTRING(C1.vcDescription,0,40) + ' [...]' 
         ELSE C1.vcDescription + ' [...]'	END 
    ELSE
		 ISNULL(C1.vcDescription ,'')   
    END     
         AS vcShortDescription,1 AS [Level],
	CAST (REPLICATE('.',1) + C1.vcCategoryName as varchar(1000) )  as vcNewCategoryName,
	CAST(C1.numCategoryID AS VARCHAR(MAX)) AS [Order]
 FROM dbo.Category C1 WHERE C1.numDepCategory=0 AND  C1.numDomainID=@numDomainID AND C1.numCategoryProfileID=@numCategoryProfileID
 UNION ALL
 SELECT  C1.numCategoryID,C1.vcCategoryName,C1.numDepCategory,C1.tintLevel,case when C1.bitLink=0 then 'No' when C1.bitLink=1 then 'Yes' end as Link,C1.vcLink as vcLink,CL.vcCategoryName as DepCategory ,
	C1.intDisplayOrder,C1.vcDescription,
	CASE WHEN LEN(ISNULL(C1.vcDescription ,'')) > 0 THEN  
	CASE WHEN LEN(C1.vcDescription) > 40 THEN SUBSTRING(C1.vcDescription,0,40) + ' [...]' 
         ELSE C1.vcDescription + ' [...]'	END 
    ELSE
		 ISNULL(C1.vcDescription ,'')   
    END     
         AS vcShortDescription,CL.[Level] + 1 AS [Level],
	CAST (REPLICATE('.',[Level] * 3) + C1.vcCategoryName as varchar(1000) )  as vcNewCategoryName,
	[Order] + '.' + CAST(C1.numCategoryID AS VARCHAR(MAX)) AS [Order]
 FROM dbo.Category C1 
 INNER JOIN CategoryList CL
 ON CL.numCategoryID = C1.numDepCategory  WHERE  C1.numDomainID=@numDomainID AND C1.numCategoryProfileID=@numCategoryProfileID
)   

SELECT numCategoryID,vcCategoryName,(CASE WHEN  numDepCategory=0 THEN NULL ELSE numDepCategory END) AS numDepCategory ,tintLevel,Link,vcLink,DepCategory,(SELECT COUNT(*) FROM [SiteCategories] SC WHERE SC.[numSiteID] =@numSiteID AND SC.[numCategoryID]= C1.[numCategoryID]) AS ShowInECommerce,
ISNULL((SELECT COUNT(*) FROM [ItemCategory] WHERE [numCategoryID] = C1.[numCategoryID]),0) AS ItemCount FROM CategoryList C1 ORDER BY [Order]
END


DROP TABLE #TempCategory
END
/****** Object:  StoredProcedure [dbo].[usp_GetCompanyInfoDtlPl]    Script Date: 07/26/2008 16:16:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--usp_GetCompanyInfoDtlPl 91099,72,-333
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcompanyinfodtlpl')
DROP PROCEDURE usp_getcompanyinfodtlpl
GO
CREATE PROCEDURE [dbo].[usp_GetCompanyInfoDtlPl]                                                                                                                   
@numDivisionID numeric(9) ,                      
@numDomainID numeric(9) ,        
@ClientTimeZoneOffset as int                              
    --                                                                           
AS                                                                            
BEGIN                                                                            
 SELECT CMP.numCompanyID,               
 CMP.vcCompanyName,                
 (select vcdata from listdetails where numListItemID = DM.numStatusID) as numCompanyStatusName,
 DM.numStatusID as numCompanyStatus,                                                                           
 (select vcdata from listdetails where numListItemID = CMP.numCompanyRating) as numCompanyRatingName, 
 CMP.numCompanyRating,             
 (select vcdata from listdetails where numListItemID = CMP.numCompanyType) as numCompanyTypeName,
 CMP.numCompanyType,              
 CMP.numCompanyType as numCmptype,               
 DM.bitPublicFlag,                 
 DM.numDivisionID,                                   
 DM. vcDivisionName,        
dbo.getCompanyAddress(dm.numdivisionId,1,dm.numDomainID) as Address,
dbo.getCompanyAddress(dm.numdivisionId,2,dm.numDomainID) as ShippingAddress,          
  isnull(AD2.vcStreet,'') as vcShipStreet,
  isnull(AD2.vcCity,'') as vcShipCity,
  isnull(dbo.fn_GetState(AD2.numState),'')  as vcShipState,
  ISNULL((SELECT numShippingZone FROM [State] WHERE (numDomainID=@numDomainID OR ConstFlag=1) AND numStateID=AD2.numState),0) AS numShippingZone,
 (select vcdata from listdetails where numListItemID = DM.numFollowUpStatus) as numFollowUpStatusName,
 DM.numFollowUpStatus,                
 CMP.vcWebLabel1,               
 CMP.vcWebLink1,               
 CMP.vcWebLabel2,               
 CMP.vcWebLink2,                                                                             
 CMP.vcWebLabel3,               
 CMP.vcWebLink3,               
 CMP.vcWeblabel4,               
 CMP.vcWebLink4,                                                                       
 DM.tintBillingTerms,              
 DM.numBillingDays,              
 DM.tintInterestType,              
 DM.fltInterest,                                                  
 isnull(AD2.vcPostalCode,'') as vcShipPostCode, 
 AD2.numCountry vcShipCountry,                          
 DM.bitPublicFlag,              
 (select vcdata from listdetails where numListItemID = DM.numTerID)  as numTerIDName,
 DM.numTerID,                                                                         
 (select vcdata from listdetails where numListItemID = CMP.numCompanyIndustry) as numCompanyIndustryName, 
  CMP.numCompanyIndustry,              
 (select vcdata from listdetails where numListItemID = CMP.numCompanyCredit) as  numCompanyCreditName,
 CMP.numCompanyCredit,                                                                             
 isnull(CMP.txtComments,'') as txtComments,               
 isnull(CMP.vcWebSite,'') vcWebSite,              
 (select vcdata from listdetails where numListItemID = CMP.numAnnualRevID) as  numAnnualRevIDName,
 CMP.numAnnualRevID,                
 (select vcdata from listdetails where numListItemID = CMP.numNoOfEmployeesID) as numNoOfEmployeesIDName,               
CMP.numNoOfEmployeesID,                                                                           
    (select vcdata from listdetails where numListItemID = CMP.vcProfile) as vcProfileName, 
 CMP.vcProfile,              
 DM.numCreatedBy,               
 dbo.fn_GetContactName(DM.numRecOwner) as RecOwner,               
 dbo.fn_GetContactName(DM.numCreatedBy)+' ' + convert(varchar(20),dateadd(minute,-@ClientTimeZoneOffset,DM.bintCreatedDate)) as vcCreatedBy,               
 DM.numRecOwner,                
 dbo.fn_GetContactName(DM.numModifiedBy)+' ' +convert(varchar(20),dateadd(minute,-@ClientTimeZoneOffset,DM.bintModifiedDate)) as vcModifiedBy,                    
 isnull(DM.vcComPhone,'') as vcComPhone, 
 DM.vcComFax,                   
 DM.numGrpID,  
 (select vcGrpName from Groups where numGrpId= DM.numGrpID) as numGrpIDName,              
 (select vcdata from listdetails where numListItemID = CMP.vcHow) as vcInfoSourceName,
 CMP.vcHow as vcInfoSource,                                                                            
 DM.tintCRMType,                                                                         
 (SELECT vcCampaignName FROM CampaignMaster WHERE numCampaignID= DM.numCampaignID) as  numCampaignIDName,
 DM.numCampaignID,              
 DM.numAssignedBy, isnull(DM.bitNoTax,0) bitNoTax,                     
 (select A.vcFirstName+' '+A.vcLastName                       
 from AdditionalContactsInformation A                  
 join DivisionMaster D                        
 on D.numDivisionID=A.numDivisionID
 where A.numContactID=DM.numAssignedTo) as numAssignedToName,DM.numAssignedTo,
 (select  count(*) from dbo.GenericDocuments where numRecID= @numDivisionID and vcDocumentSection='A' AND tintDocumentType=2) as DocumentCount ,
 (SELECT count(*)from CompanyAssociations where numDivisionID=@numDivisionID and bitDeleted=0 ) as AssociateCountFrom,                            
 (SELECT count(*)from CompanyAssociations where numAssociateFromDivisionID=@numDivisionID and bitDeleted=0 ) as AssociateCountTo,
 (select vcdata from listdetails where numListItemID = DM.numCompanyDiff) as numCompanyDiffName,               
 DM.numCompanyDiff,DM.vcCompanyDiff,isnull(DM.bitActiveInActive,1) as bitActiveInActive,
(SELECT SUBSTRING(
	(SELECT ','+  A.vcFirstName+' ' +A.vcLastName + CASE WHEN ISNULL(SR.numContactType,0)>0 THEN '(' + dbo.fn_GetListItemName(SR.numContactType) + ')' ELSE '' END
	FROM ShareRecord SR JOIN UserMaster UM ON UM.numUserDetailId=SR.numAssignedTo 
							 JOIN AdditionalContactsInformation A ON UM.numUserDetailId=A.numContactID 
		 WHERE SR.numDomainID=DM.numDomainID AND SR.numModuleID=1 AND SR.numRecordID=DM.numDivisionID
				AND UM.numDomainID=DM.numDomainID and UM.numDomainID=A.numDomainID FOR XML PATH('')),2,200000)) AS numShareWith,
 DM.bitOnCreditHold,
 DM.numDefaultPaymentMethod,
 DM.numAccountClassID,
 DM.tintPriceLevel,DM.vcPartnerCode as vcPartnerCode  ,
  ISNULL(DM.numPartenerSource,0) AS numPartenerSourceId,
 D3.vcPartnerCode+'-'+C3.vcCompanyName as numPartenerSource,
 ISNULL(DM.numPartenerContact,0) AS numPartenerContactId,
A2.vcFirstName+' '+A2.vcLastName AS numPartenerContact,
ISNULL(DM.bitEmailToCase,0) bitEmailToCase,
'' AS vcCreditCards,
----Fetch Last Followup(Template) and EmailSentDate(if any) Value
		  
	dbo.FormatedDateFromDate(DM.dtLastFollowUp,@numDomainID) vcLastFollowup,

		----Fetch Last Followup(Template) Value

		----Fetch Next Followup(Template) and ExecutionDate Value
	(CASE WHEN A2.numECampaignID > 0 THEN dbo.fn_FollowupDetailsInOrgList(A2.numContactID,2,@numDomainID) ELSE '' END) vcNextFollowup,										 

		----Fetch Next Followup(Template) Value
(CASE WHEN (SELECt COUNT(*) FROM ExtarnetAccounts WHERE numDivisionID=DM.numDivisionID) >0 THEN 1 ELSE 0 END) AS bitEcommerceAccess,

ISNULL(DM.numDefaultShippingServiceID,0) numShippingService,
ISNULL((SELECT vcShipmentService FROM ShippingService WHERE (numDomainID=@numDomainID OR ISNULL(numDomainID,0)=0) AND numShippingServiceID = DM.numDefaultShippingServiceID),'') AS vcShippingService,
		CASE 
			WHEN vcSignatureType = 0 THEN 'Service Default'
			WHEN vcSignatureType = 1 THEN 'Adult Signature Required'
			WHEN vcSignatureType = 2 THEN 'Direct Signature'
			WHEN vcSignatureType = 3 THEN 'InDirect Signature'
			WHEN vcSignatureType = 4 THEN 'No Signature Required'
			ELSE ''
		END AS vcSignatureType,
		ISNULL(DM.intShippingCompany,0) intShippingCompany,
		CASE WHEN ISNULL(DM.intShippingCompany,0) = 0 THEN '' ELSE ISNULL((SELECT vcData FROM ListDetails WHERE numListID = 82 AND numListItemID=ISNULL(DM.intShippingCompany,0)),'') END AS vcShipVia,
        A2.vcFirstName AS vcFirstName,
		A2.vcLastName  AS vcLastName,
		A2.vcFirstname + ' ' + A2.vcLastName AS vcGivenName,
		A2.numPhone AS numPhone,
		A2.numPhoneExtension As numPhoneExtension,
		A2.vcEmail As vcEmail	,
 ISNULL(DM.bitAutoCheckCustomerPart,0) AS  bitAutoCheckCustomerPart	    ,
STUFF((SELECT '<br/>' +' <b>Buy</b> '+CASE WHEN u.intType=1 THEN '$'+CAST(u.vcBuyingQty AS VARCHAR(MAX)) ELSE CAST(u.vcBuyingQty AS VARCHAR(MAX)) END+' ' + CASE WHEN u.intType=1 THEN 'Amount' WHEN u.intType=2 THEN 'Quantity' WHEN u.intType=3 THEN 'Lbs' ELSE '' END+' <b>Get</b> ' + (CASE WHEN u.tintDiscountType = 1 THEN CONCAT('$',u.vcIncentives,' off') ELSE CONCAT(u.vcIncentives,'% off') END)
        from PurchaseIncentives u
        where u.numPurchaseIncentiveId = numPurchaseIncentiveId AND numDomainID=@numDomainID AND numDivisionId=@numDivisionID
        order by u.numPurchaseIncentiveId
        for xml path('')),6,6,'') AS Purchaseincentives,
ISNULL(DM.intDropShip,0) as intDropShip,
CASE WHEN ISNULL(DM.intDropShip,0)=1 THEN 'Not Available'
	 WHEN ISNULL(DM.intDropShip,0)=2 THEN 'Blank Available'
	 WHEN ISNULL(DM.intDropShip,0)=3 THEN 'Vendor Label'
	 WHEN ISNULL(DM.intDropShip,0)=4 THEN 'Private Label'
	 ELSE 'Select One'
END AS intDropShipName,
CASE WHEN ISNULL(DM.intDropShip,0)=1 THEN 'Not Available'
	 WHEN ISNULL(DM.intDropShip,0)=2 THEN 'Blank Available'
	 WHEN ISNULL(DM.intDropShip,0)=3 THEN 'Vendor Label'
	 WHEN ISNULL(DM.intDropShip,0)=4 THEN 'Private Label'
	 ELSE 'Select One'
END AS vcFropShip
		                       
 FROM  CompanyInfo CMP    
  join DivisionMaster DM    
  on DM.numCompanyID=CMP.numCompanyID                                                                                                                                        
   LEFT JOIN dbo.AddressDetails AD1 ON AD1.numDomainID=DM.numDomainID 
   AND AD1.numRecordID=DM.numDivisionID AND AD1.tintAddressOf=2 AND AD1.tintAddressType=1 AND AD1.bitIsPrimary=1
   LEFT JOIN dbo.AddressDetails AD2 ON AD2.numDomainID=DM.numDomainID 
   AND AD2.numRecordID= DM.numDivisionID AND AD2.tintAddressOf=2 AND AD2.tintAddressType=2 AND AD2.bitIsPrimary=1
    LEFT JOIN divisionMaster D3 ON DM.numPartenerSource = D3.numDivisionID
  LEFT JOIN CompanyInfo C3 ON C3.numCompanyID = D3.numCompanyID    
  LEft join   AdditionalContactsInformation A2 on   DM.numDivisionID = A2.numDivisionId and A2.bitPrimaryContact = 1
  LEFT JOIN DivisionMasterShippingConfiguration ON DM.numDivisionID=DivisionMasterShippingConfiguration.numDivisionID
 where DM.numDivisionID=@numDivisionID   and DM.numDomainID=@numDomainID                                                                                          
END

GO




/****** Object:  StoredProcedure [dbo].[USP_GetDealsList1]    Script Date: 05/07/2009 22:12:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getdealslist1')
DROP PROCEDURE usp_getdealslist1
GO
CREATE PROCEDURE [dbo].[USP_GetDealsList1]
               @numUserCntID              NUMERIC(9)  = 0,
               @numDomainID               NUMERIC(9)  = 0,
               @tintSalesUserRightType    TINYINT  = 0,
               @tintPurchaseUserRightType TINYINT  = 0,
               @tintSortOrder             TINYINT  = 4,
               @CurrentPage               INT,
               @PageSize                  INT,
               @TotRecs                   INT  OUTPUT,
               @columnName                AS VARCHAR(300),
               @columnSortOrder           AS VARCHAR(10),
               @numCompanyID              AS NUMERIC(9)  = 0,
               @bitPartner                AS BIT  = 0,
               @ClientTimeZoneOffset      AS INT,
               @tintShipped               AS TINYINT,
               @intOrder                  AS TINYINT,
               @intType                   AS TINYINT,
               @tintFilterBy              AS TINYINT  = 0,
               @numOrderStatus			  AS NUMERIC,
			   @vcRegularSearchCriteria varchar(MAX)='',
			   @vcCustomSearchCriteria varchar(MAX)='',
			   @SearchText VARCHAR(300) = '',
			   @SortChar char(1)='0',
			   @tintDashboardReminderType TINYINT = 0
AS
BEGIN
	DECLARE @PageId  AS TINYINT
	DECLARE @numFormId  AS INT 
	DECLARE @tintDecimalPoints TINYINT

	SELECT
		@tintDecimalPoints=ISNULL(tintDecimalPoints,0)
	FROM
		Domain 
	WHERE 
		numDomainId=@numDomainID

	SET @PageId = 0

	IF @inttype = 1
	BEGIN
		SET @PageId = 2
		SET @inttype = 3
		SET @numFormId=39
	END
	ELSE IF @inttype = 2
	BEGIN
		SET @PageId = 6
		SET @inttype = 4
		SET @numFormId=41
	END

	CREATE TABLE #tempForm 
	(
		tintOrder TINYINT,vcDbColumnName nvarchar(200),vcOrigDbColumnName nvarchar(50),vcFieldName nvarchar(50),vcAssociatedControlType nvarchar(50),vcListItemType varchar(3),
		numListID numeric(9),vcLookBackTableName varchar(50),bitCustomField BIT,numFieldId NUMERIC,bitAllowSorting BIT,bitAllowEdit BIT,bitIsRequired BIT,bitIsEmail BIT,
		bitIsAlphaNumeric BIT,bitIsNumeric BIT,bitIsLengthValidation BIT,intMaxLength int,intMinLength int,bitFieldMessage BIT,vcFieldMessage VARCHAR(500),
		ListRelID numeric(9),intColumnWidth int,bitAllowFiltering BIT,vcFieldDataType CHAR(1)
	)

	-- FIRST CHECK IF GRID COLUMNS ARE CONFIGURED
	DECLARE  @Nocolumns  AS TINYINT = 0

	SELECT 
		@Nocolumns=ISNULL(SUM(TotalRow),0) 
	FROM
	(            
		SELECT 
			COUNT(*) TotalRow 
		FROM 
			View_DynamicColumns 
		WHERE 
			numFormId=@numFormId 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND numRelCntType = @numFormId
			AND 1 = (CASE WHEN @numFormId=39 AND numFieldID=771 THEN 0 ELSE 1 END)
		UNION 
		SELECT 
			COUNT(*) TotalRow 
		FROM 
			View_DynamicCustomColumns 
		WHERE 
			numFormId=@numFormId 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND numRelCntType = @numFormId
	) TotalRows

	-- IF GRID COLUMNS ARE NOT CONFIGURES THEN CHECK IF MASTER FORM CONFIGURATION IS CREATED BY ADMINISTRATOR
	DECLARE @IsMasterConfAvailable BIT = 0
	DECLARE @numUserGroup NUMERIC(18,0)

	IF @Nocolumns=0
	BEGIN
		SELECT @numUserGroup = numGroupID FROM UserMaster WHERE numUserDetailId = @numUserCntID 

		IF (SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = @numDomainID AND numFormID = @numFormId AND numRelCntType=@numFormId AND bitGridConfiguration = 1 AND numGroupID = @numUserGroup) > 0
		BEGIN
			SET @IsMasterConfAvailable = 1
		END
	END

	--IF MASTERCONFIGURATION IS AVAILABLE THEN LOAD IT OTHERWISE LOAD DEFAULT COLUMNS
	IF @IsMasterConfAvailable = 1
	BEGIN
		INSERT INTO DycFormConfigurationDetails 
		(
			numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth
		)
		SELECT 
			@numFormId,numFieldId,intColumnNum,intRowNum,@numDomainID,@numUserCntID,@numFormId,1,0,intColumnWidth
		FROM 
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId=@numFormId AND 
			View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicColumnsMasterConfig.numRelCntType=@numFormId AND 
			View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
			ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
			AND 1 = (CASE WHEN @numFormId=39 AND numFieldID=771 THEN 0 ELSE 1 END)
		UNION
		SELECT 
			@numFormId,numFieldId,intColumnNum,intRowNum,@numDomainID,@numUserCntID,@numFormId,1,1,intColumnWidth
		FROM 
			View_DynamicCustomColumnsMasterConfig
		WHERE 
			View_DynamicCustomColumnsMasterConfig.numFormId=@numFormId AND 
			View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicCustomColumnsMasterConfig.numRelCntType=@numFormId AND 
			View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1

		INSERT INTO 
			#tempForm
		SELECT 
			(intRowNum + 1) as tintOrder, vcDbColumnName,	vcOrigDbColumnName,	ISNULL(vcCultureFieldName,vcFieldName), vcAssociatedControlType,
			vcListItemType, numListID, vcLookBackTableName,	bitCustom, numFieldId, bitAllowSorting,	bitInlineEdit, bitIsRequired,
			bitIsEmail,	bitIsAlphaNumeric, bitIsNumeric, bitIsLengthValidation,	intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,
			ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType 
		FROM 
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId=@numFormId AND 
			View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicColumnsMasterConfig.numRelCntType=@numFormId AND 
			View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
			ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
		UNION
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName, vcAssociatedControlType,'' as vcListItemType,numListID,'',
			bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,''
		FROM 
			View_DynamicCustomColumnsMasterConfig
		WHERE 
			View_DynamicCustomColumnsMasterConfig.numFormId=@numFormId AND 
			View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicCustomColumnsMasterConfig.numRelCntType=@numFormId AND 
			View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1
		ORDER BY 
			tintOrder ASC  
	END
	ELSE                                        
	BEGIN
		IF @Nocolumns=0
		BEGIN
			INSERT INTO DycFormConfigurationDetails 
			(
				numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth
			)
			SELECT 
				@numFormId,numFieldId,0,Row_number() over(order by tintRow desc),@numDomainID,@numUserCntID,@numFormId,1,0,intColumnWidth
			FROM 
				View_DynamicDefaultColumns
			WHERE 
				numFormId=@numFormId 
				AND bitDefault=1 
				AND ISNULL(bitSettingField,0)=1 
				AND numDomainID=@numDomainID
				AND 1 = (CASE WHEN @numFormId=39 AND numFieldID=771 THEN 0 ELSE 1 END)
			ORDER BY 
				tintOrder asc   
		END

		INSERT INTO 
			#tempForm
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcOrigDbColumnName,ISNULL(vcCultureFieldName,vcFieldName),                  
			vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName,bitCustom,numFieldId,bitAllowSorting,bitInlineEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength,bitFieldMessage,
			vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType  
		FROM 
			View_DynamicColumns 
		WHERE 
			numFormId=@numFormId 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND ISNULL(bitSettingField,0)=1 
			AND ISNULL(bitCustom,0)=0  
			AND numRelCntType = @numFormId
			AND 1 = (CASE WHEN @numFormId=39 AND numFieldID=771 THEN 0 ELSE 1 END)
		UNION
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName,vcAssociatedControlType,'' as vcListItemType,numListID,'',bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,'' 
		FROM 
			View_DynamicCustomColumns
		WHERE 
			numFormId=@numFormId 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND ISNULL(bitCustom,0)=1 
			AND numRelCntType = @numFormId
		ORDER BY 
			tintOrder asc  
	END
	UPDATE #tempForm SET vcFieldMessage = CAST((SELECT numFieldId FROM DycFieldMaster WHERE vcDbColumnName='intUsedShippingCompany' AND numModuleID=3)  AS varchar)+'#'+CAST((SELECT numFieldId FROM DycFieldMaster WHERE vcDbColumnName='numShippingService' AND numModuleID=3)  AS varchar) 
	WHERE vcDbColumnName='vcOrderedShipped'
	UPDATE #tempForm SET vcFieldMessage = CAST((SELECT numFieldId FROM DycFieldMaster WHERE vcDbColumnName='monAmountPaid' AND numModuleID=3)  AS varchar)
	WHERE vcDbColumnName='vcPOppName'
	DECLARE  @strColumns AS VARCHAR(MAX)
	SET @strColumns = ' ADC.numContactId, ADC.vcEmail, Div.numDivisionID,ISNULL(Div.numTerID,0) numTerID, opp.numRecOwner as numRecOwner, Div.tintCRMType, opp.numOppId, ISNULL(Opp.monDealAmount,0) monDealAmount 
	,ISNULL(opp.numShippingService,0) AS numShippingService,ADC.vcFirstName+'' ''+ADC.vcLastName AS vcContactName,ADC.numPhone AS numContactPhone,ADC.numPhoneExtension AS numContactPhoneExtension,ADC.vcEmail AS vcContactEmail,Opp.vcPOppName,cmp.vcCompanyName,ISNULL((SELECT SUM(ISNULL(monAmountPaid,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0) as monAmountPaid '
	--Corr.bitIsEmailSent,

	DECLARE  @tintOrder  AS TINYINT;SET @tintOrder = 0
	DECLARE  @vcFieldName  AS VARCHAR(50)
	DECLARE  @vcListItemType  AS VARCHAR(3)
	DECLARE  @vcListItemType1  AS VARCHAR(1)
	DECLARE  @vcAssociatedControlType VARCHAR(30)
	DECLARE  @numListID  AS NUMERIC(9)
	DECLARE  @vcDbColumnName VARCHAR(200)
	DECLARE  @WhereCondition VARCHAR(2000);SET @WhereCondition = ''
	DECLARE  @vcLookBackTableName VARCHAR(2000)
	DECLARE  @bitCustom  AS BIT
	Declare @numFieldId as numeric  
	DECLARE @bitAllowSorting AS CHAR(1)   
	DECLARE @bitAllowEdit AS CHAR(1)                 
	DECLARE @vcColumnName AS VARCHAR(500)          
	
	Declare @ListRelID as numeric(9) 
	DECLARE @SearchQuery AS VARCHAR(MAX) = ''        

	SELECT TOP 1 
		@tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName,@vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,
		@numListID=numListID,@vcLookBackTableName=vcLookBackTableName ,@bitCustom=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,@bitAllowEdit=bitAllowEdit,
		@ListRelID=ListRelID
	FROM 
		#tempForm 
	ORDER BY 
		tintOrder ASC            

	WHILE @tintOrder > 0
    BEGIN
		IF @bitCustom = 0
		BEGIN
			DECLARE  @Prefix  AS VARCHAR(10)

			IF @vcLookBackTableName = 'AdditionalContactsInformation'
				SET @Prefix = 'ADC.'
			IF @vcLookBackTableName = 'DivisionMaster'
				SET @Prefix = 'Div.'
			IF @vcLookBackTableName = 'OpportunityMaster'
				SET @PreFix = 'Opp.'
			IF @vcLookBackTableName = 'OpportunityRecurring'
				SET @PreFix = 'OPR.'
			IF @vcLookBackTableName = 'CompanyInfo'
				SET @PreFix = 'cmp.'
			IF @vcLookBackTableName = 'ProjectProgress'
				SET @PreFix = 'PP.'
			SET @vcColumnName= @vcDbColumnName+'~'+ convert(varchar(10),@numFieldId) +'~0'

			IF @vcAssociatedControlType = 'SelectBox'
			BEGIN
				-- KEEP THIS IF CONDITION SEPERATE FROM IF ELSE
				IF @vcDbColumnName = 'numStatus'
				BEGIN
					SET @strColumns=@strColumns+ ' ,ISNULL(TEMPApproval.numApprovalCount,0) ApprovalMarginCount'
				END


				IF @vcDbColumnName = 'numPartner'
				BEGIN
					SET @strColumns=@strColumns+ ' ,(Opp.numPartner) '+' ['+ @vcColumnName+']'    
					SET @strColumns=@strColumns+ ' ,(SELECT D.vcPartnerCode+''-''+C.vcCompanyName FROM DivisionMaster AS D LEFT JOIN CompanyInfo AS C ON D.numCompanyID=C.numCompanyID WHERE D.numDivisionID=Opp.numPartner) AS vcPartner' 
				END
				ELSE IF @vcDbColumnName = 'vcSignatureType'
				BEGIN
					SET @strColumns=@strColumns+ ' ,(SELECT CASE WHEN vcSignatureType = 0 THEN ''Service Default''
																	WHEN vcSignatureType = 1 THEN ''Adult Signature Required''
																	WHEN vcSignatureType = 2 THEN ''Direct Signature''
																	WHEN vcSignatureType = 3 THEN ''InDirect Signature''
																	WHEN vcSignatureType = 4 THEN ''No Signature Required''
																ELSE '''' END 
					FROM DivisionMaster AS D INNER JOIN DivisionMasterShippingConfiguration AS DMSC ON DMSC.numDivisionID=D.numDivisionID 
					WHERE D.numDivisionID=Div.numDivisionID) '  + ' [' + @vcColumnName + ']'
				END
				ELSE IF @vcDbColumnName = 'numShippingService'
				BEGIN
					SET @strColumns=@strColumns+ ' ,ISNULL((SELECT vcShipmentService FROM ShippingService WHERE (numDomainID=Opp.numDomainID OR ISNULL(numDomainID,0)=0) AND numShipmentServiceID = ISNULL(Opp.numShippingService,0)),'''')' + ' [' + @vcColumnName + ']'
				END
				ELSE IF @vcDbColumnName = 'tintEDIStatus'
				BEGIN
					--SET @strColumns = @strColumns + ',(CASE Opp.tintEDIStatus WHEN 1 THEN ''850 Received'' WHEN 2 THEN ''850 Acknowledged'' WHEN 3 THEN ''940 Sent'' WHEN 4 THEN ''940 Acknowledged'' WHEN 5 THEN ''856 Received'' WHEN 6 THEN ''856 Acknowledged'' WHEN 7 THEN ''856 Sent'' ELSE '''' END)' + ' [' + @vcColumnName + ']'
					SET @strColumns = @strColumns + ',(CASE Opp.tintEDIStatus WHEN 2 THEN ''850 Acknowledged'' WHEN 3 THEN ''940 Sent'' WHEN 4 THEN ''940 Acknowledged'' WHEN 5 THEN ''856 Received'' WHEN 6 THEN ''856 Acknowledged'' WHEN 7 THEN ''856 Sent'' WHEN 11 THEN ''850 Partially Created'' WHEN 12 THEN ''850 SO Created'' ELSE '''' END)' + ' [' + @vcColumnName + ']'
				END
				ELSE IF @vcDbColumnName = 'numPartenerContact'
				BEGIN
					SET @strColumns=@strColumns+ ' ,(Opp.numPartenerContact) '+' ['+ @vcColumnName+']'    
					SET @strColumns=@strColumns+ ' ,(SELECT A.vcFirstName+'' ''+ A.vcLastName FROM AdditionalContactsInformation AS A WHERE A.numContactId=Opp.numPartenerContact) AS vcPartenerContact' 
				END
				ELSE IF @vcDbColumnName = 'tintInvoicing'
				BEGIN
					SET @strColumns=CONCAT(@strColumns,', CONCAT(ISNULL((SELECT SUBSTRING((SELECT ''$^$'' + CONCAT(numOppBizDocsId,''#^#'',vcBizDocID,''#^#'',ISNULL(bitisemailsent,0),''#^#'',ISNULL(monDealAmount,0),''#^#'',ISNULL(monAmountPaid,0),''#^#'', convert(varchar(10), cast(dtFromDate as date), 101),''#^#'')
										FROM OpportunityBizDocs WHERE numOppId=Opp.numOppId AND numBizDocId = ISNULL((SELECT TOP 1 numAuthoritativeSales FROM AuthoritativeBizDocs WHERE numDomainId=' + CAST (@numDomainID AS VARCHAR) + '),287) FOR XML PATH('''')),4,200000)),'''')',
										CASE 
										WHEN CHARINDEX('tintInvoicing=3',@vcRegularSearchCriteria) > 0
											THEN ', (SELECT 
													CONCAT(''('',SUM(X.InvoicedQty),'' Out of '',SUM(X.OrderedQty),'')'')
												FROM 
												(
													SELECT
														OI.numoppitemtCode,
														ISNULL(OI.numUnitHour,0) AS OrderedQty,
														ISNULL(TempInvoice.InvoicedQty,0) AS InvoicedQty
													FROM
														OpportunityItems OI
													INNER JOIN
														Item I
													ON
														OI.numItemCode = I.numItemCode
													OUTER APPLY
													(
														SELECT
															SUM(OpportunityBizDocItems.numUnitHour) AS InvoicedQty
														FROM
															OpportunityBizDocs
														INNER JOIN
															OpportunityBizDocItems 
														ON
															OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
														WHERE
															OpportunityBizDocs.numOppId = Opp.numOppId
															AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
															AND ISNULL(OpportunityBizDocs.bitAuthoritativeBizDocs,0) = 1
													) AS TempInvoice
													WHERE
														OI.numOppID = Opp.numOppId
												) X)'

											WHEN CHARINDEX('tintInvoicing=4',@vcRegularSearchCriteria) > 0
											THEN ', (SELECT 
													CONCAT(''('',(X.InvoicedPercentage),'' % '','')'')
												FROM 
												(

													SELECT fltBreakupPercentage  AS InvoicedPercentage
													FROM dbo.OpportunityRecurring OPR 
														INNER JOIN dbo.OpportunityMaster OM ON OPR.numOppID = OM.numOppID
														LEFT JOIN dbo.OpportunityBizDocs OBD ON OBD.numOppBizDocsID = OPR.numOppBizDocID
													WHERE OPR.numOppId = Opp.numOppId 
														and tintRecurringType <> 4
												) X)'

											ELSE 
													','''''
											END,') [', @vcColumnName,']')


				END
				ELSE IF @vcDbColumnName = 'tintSource'
				BEGIN
					SET @strColumns = @strColumns + ',CONCAT(ISNULL(dbo.fn_GetOpportunitySourceValue(ISNULL(Opp.tintSource,0),ISNULL(Opp.tintSourceType,0),Opp.numDomainID),''Internal Order''),(CASE WHEN ISNULL(TEMPMapping.numMappingCount,0) > 0 THEN CONCAT(''&nbsp;<a target="_blank" href="../opportunity/frmOpportunities.aspx?frm=deallist&OpID='',Opp.numOppID,''&SelectedTab=ProductsServices"><i class="fa fa-map-marker" style="color:red;font-size:18px" aria-hidden="true"></i></a>'') ELSE '''' END))' + ' [' + @vcColumnName + ']'

					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'ISNULL(dbo.fn_GetOpportunitySourceValue(ISNULL(Opp.tintSource,0),ISNULL(Opp.tintSourceType,0),Opp.numDomainID),''Internal Order'') LIKE ''%' + @SearchText + '%'''
					END
				END
				ELSE IF @vcDbColumnName = 'numCampainID'
				BEGIN
					SET @strColumns = @strColumns + ', (SELECT vcCampaignName FROM CampaignMaster WHERE numCampaignID= Opp.numCampainID) ' + ' [' + @vcColumnName + ']'
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'ISNULL((SELECT vcCampaignName FROM CampaignMaster WHERE numCampaignID= Opp.numCampainID),'''') LIKE ''%' + @SearchText + '%'''
					END
				END
				ELSE IF @vcDbColumnName = 'numContactID'
				BEGIN
					SET @strColumns = @strColumns + ',dbo.fn_GetContactName(' + @Prefix + @vcDbColumnName + ') [' + @vcColumnName + ']'
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'dbo.fn_GetContactName(' + @Prefix + @vcDbColumnName + ') LIKE ''%' + @SearchText + '%'''
					END
				END
				ELSE IF @vcListItemType = 'LI'
				BEGIN
					SET @strColumns = @strColumns + ',L' + CONVERT(VARCHAR(3),@tintOrder) + '.vcData' + ' [' + @vcColumnName + ']'
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'L' + CONVERT(VARCHAR(3),@tintOrder) + '.vcData LIKE ''%' + @SearchText + '%'''
					END

					SET @WhereCondition = @WhereCondition + ' left Join ListDetails L' + CONVERT(VARCHAR(3),@tintOrder) + ' on L' + CONVERT(VARCHAR(3),@tintOrder) + '.numListItemID=' + @vcDbColumnName
				END
				ELSE IF @vcListItemType = 'S'
				BEGIN
					SET @strColumns = @strColumns + ',S' + CONVERT(VARCHAR(3),@tintOrder) + '.vcState' + ' [' + @vcColumnName + ']'
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'S' + CONVERT(VARCHAR(3),@tintOrder) + '.vcState LIKE ''%' + @SearchText + '%'''
					END

					SET @WhereCondition = @WhereCondition + ' left join State S' + CONVERT(VARCHAR(3),@tintOrder) + ' on S' + CONVERT(VARCHAR(3),@tintOrder) + '.numStateID=' + @vcDbColumnName
				END
				ELSE IF @vcListItemType = 'BP'
				BEGIN
					SET @strColumns = @strColumns + ',SPLM.Slp_Name' + ' [' + @vcColumnName + ']'
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'SPLM.Slp_Name LIKE ''%' + @SearchText + '%'''
					END

					SET @WhereCondition = @WhereCondition + ' LEFT JOIN Sales_process_List_Master SPLM on SPLM.Slp_Id = OPP.numBusinessProcessID '
				END
				ELSE IF @vcListItemType = 'PP'
				BEGIN
					SET @strColumns = @strColumns + ',ISNULL(PP.intTotalProgress,0)' + ' [' + @vcColumnName + ']'

					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'ISNULL(PP.intTotalProgress,0) LIKE ''%' + @SearchText + '%'''
					END
				END
				ELSE IF @vcListItemType = 'T'
				BEGIN
					SET @strColumns = @strColumns + ',L' + CONVERT(VARCHAR(3),@tintOrder) + '.vcData' + ' [' + @vcColumnName + ']'
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'L' + CONVERT(VARCHAR(3),@tintOrder) + '.vcData LIKE ''%' + @SearchText + '%'''
					END

					SET @WhereCondition = @WhereCondition + ' left Join ListDetails L' + CONVERT(VARCHAR(3),@tintOrder) + ' on L' + CONVERT(VARCHAR(3),@tintOrder) + '.numListItemID=' + @vcDbColumnName
				END
				ELSE IF @vcListItemType = 'U'
				BEGIN
					SET @strColumns = @strColumns + ',dbo.fn_GetContactName(' + @Prefix + @vcDbColumnName + ') [' + @vcColumnName + ']'
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'dbo.fn_GetContactName(' + @Prefix + @vcDbColumnName + ') LIKE ''%' + @SearchText + '%'''
					END
				END
				ELSE IF @vcListItemType = 'WI'
				BEGIN
					SET @strColumns = @strColumns + ', Case when isnull(OL.numSiteID,0)>0 then Sites.vcSiteName else WebAPI.vcProviderName end [' + @vcColumnName + ']'
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + '(Case when isnull(OL.numSiteID,0)>0 then Sites.vcSiteName else WebAPI.vcProviderName end) LIKE ''%' + @SearchText + '%'''
					END

					set @WhereCondition= @WhereCondition +' left JOIN OpportunityLinking OL on OL.numChildOppID=Opp.numOppId left join WebAPI on WebAPI.WebApiId=isnull(OL.numWebApiId,0)
													left join Sites on isnull(OL.numSiteID,0)=Sites.numSIteID and Sites.numDomainID=' + convert(varchar(15),@numDomainID)                                                       
				END 
            END
			ELSE IF @vcAssociatedControlType = 'DateField'
			BEGIN
				IF @Prefix ='OPR.'
				BEGIN
					SET @strColumns = @strColumns + ', (SELECT TOP 1 dbo.FormatedDateFromDate( DateAdd(minute,+'+convert(varchar(15),-@ClientTimeZoneOffset)+', dtRecurringDate)'+ ','+ CONVERT(VARCHAR(10),@numDomainId) +') FROM  dbo.OpportunityRecurring WHERE numOppID=OPP.numOppID AND ISNULL(numOppBizDocID,0)=0 and dtRecurringDate > dbo.GetUTCDateWithoutTime()) AS  ['+ @vcColumnName + ']'

					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + '(SELECT TOP 1 dbo.FormatedDateFromDate( DateAdd(minute,+'+convert(varchar(15),-@ClientTimeZoneOffset)+', dtRecurringDate)'+ ','+ CONVERT(VARCHAR(10),@numDomainId) +') FROM  dbo.OpportunityRecurring WHERE numOppID=OPP.numOppID AND ISNULL(numOppBizDocID,0)=0 and dtRecurringDate > dbo.GetUTCDateWithoutTime()) LIKE ''%' + @SearchText + '%'''
					END
				END
				ELSE IF @vcDbColumnName = 'dtReleaseDate'
				BEGIN
					SET @strColumns = @strColumns
                                + ',case when convert(varchar(11),'
                                + @Prefix
                                + @vcDbColumnName
                                + ')= convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',getdate())) then ''<b><font color=red>Today</font></b>'''
					SET @strColumns = @strColumns
									+ 'when convert(varchar(11),'
									+ @Prefix
									+ @vcDbColumnName
									+ ')= convert(varchar(11),dateadd(day,-1,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',getdate()))) then ''<b><font color=purple>Yesterday</font></b>'''
					SET @strColumns = @strColumns
									+ 'when convert(varchar(11),'
									+ @Prefix
									+ @vcDbColumnName
									+ ')= convert(varchar(11),dateadd(day,1,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',getdate()))) then''<b><font color=orange>Tommorow</font></b>'' '
					SET @strColumns = @strColumns
									+ 'else dbo.FormatedDateFromDate('
									+ @Prefix
									+ @vcDbColumnName
									+ ','
									+ CONVERT(VARCHAR(10),@numDomainId)
									+ ') end  ['
									+ @vcColumnName + ']' 	

					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + @Prefix + @vcDbColumnName + ' LIKE ''%' + @SearchText + '%'''
					END

				END
				ELSE
				BEGIN
					SET @strColumns = @strColumns
                                + ',case when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+','
                                + @Prefix
                                + @vcDbColumnName
                                + ') )= convert(varchar(11),getdate()) then ''<b><font color=red>Today</font></b>'''
					SET @strColumns = @strColumns
									+ 'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+','
									+ @Prefix
									+ @vcDbColumnName
									+ ') )= convert(varchar(11),dateadd(day,-1,getdate())) then ''<b><font color=purple>Yesterday</font></b>'''
					SET @strColumns = @strColumns
									+ 'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+','
									+ @Prefix
									+ @vcDbColumnName
									+ ') )= convert(varchar(11),dateadd(day,1,getdate())) then''<b><font color=orange>Tommorow</font></b>'' '
					SET @strColumns = @strColumns
									+ 'else dbo.FormatedDateFromDate(DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+','
									+ @Prefix
									+ @vcDbColumnName
									+ '),'
									+ CONVERT(VARCHAR(10),@numDomainId)
									+ ') end  ['
									+ @vcColumnName + ']' 	

					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + @Prefix + @vcDbColumnName + ' LIKE ''%' + @SearchText + '%'''
					END
				 END	
            END
            ELSE
            IF @vcAssociatedControlType = 'TextBox'
            BEGIN
				IF @vcDbColumnName = 'vcCompactContactDetails'
				BEGIN
					SET @strColumns=@strColumns+ ' ,'''' AS vcCompactContactDetails'   
				END 
				ELSE
				BEGIN 
                SET @strColumns = @strColumns
                                  + ','
                                  + CASE 
                                      WHEN @vcDbColumnName = 'numAge' THEN 'year(getutcdate())-year(bintDOB)'
                                      WHEN @vcDbColumnName = 'monPAmount' THEN 'isnull((SELECT varCurrSymbol FROM [Currency] WHERE numCurrencyID = Opp.[numCurrencyID]),'''') + '' ''+ CONVERT(VARCHAR,CONVERT(DECIMAL(20,5),monPAmount),1)'
                                      WHEN @vcDbColumnName = 'numDivisionID' THEN 'Div.numDivisionID'
                                      WHEN @vcDbColumnName = 'tintOppStatus' THEN 'CASE tintOppStatus WHEN 0 then ''Open'' WHEN 1 THEN ''Closed-Won'' WHEN 2 THEN ''Closed-Lost'' END '
                                      WHEN @vcDbColumnName = 'tintOppType' THEN 'CASE tintOppType WHEN 1 then ''Sales'' WHEN 2 THEN ''Purchase'' Else ''NA'' END '
									  WHEN @vcDbColumnName = 'vcPOppName' THEN 'dbo.CheckChildRecord(Opp.numOppId,Opp.numDomainID)'
									  WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'vcBizDocsList' THEN 'ISNULL((SELECT SUBSTRING((SELECT ''$^$'' + CAST(numOppBizDocsId AS VARCHAR(18)) +''#^#''+ vcBizDocID
										FROM OpportunityBizDocs WHERE numOppId=Opp.numOppId AND 1 = (Case When bitAuthoritativeBizDocs=1 AND 1=' + (CASE WHEN @inttype=1 THEN '1' ELSE '0' END) + '  Then 0 ELSE 1 END) FOR XML PATH('''')),4,200000)),'''')'
                                      ELSE @Prefix + @vcDbColumnName
                                    END
                                  + ' ['
                                  + @vcColumnName + ']'
				END
				IF(@vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'vcBizDocsList')
				BEGIN
					SET @strColumns=@strColumns+','+' (SELECT COUNT(I.vcItemName) FROM 
													 OpportunityItems AS t LEFT JOIN  Item as I ON 
													 t.numItemCode=I.numItemCode WHERE 
													 t.numOppId=Opp.numOppId AND t.numoppItemtCode NOT IN 
													 (SELECT numOppItemId FROM OpportunityBizDocs JOIN 
													 OpportunityBizDocItems ON OpportunityBizDocs.numOppBizDocsId=OpportunityBizDocItems.numOppBizDocID  
													 WHERE OpportunityBizDocs.numOppId=Opp.numOppId 
													 AND 1 = (Case When bitAuthoritativeBizDocs=1 Or numBizDocID=293 Then 0 ELSE 1 END) 
													 AND ISNULL(bitAuthoritativeBizDocs,0)=1)) AS [List_Item_Approval_UNIT] '
				END
				
				IF LEN(@SearchText) > 0
				BEGIN
					SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + (CASE 
                                      WHEN @vcDbColumnName = 'numAge' THEN 'year(getutcdate())-year(bintDOB)'
                                      WHEN @vcDbColumnName = 'monPAmount' THEN 'isnull((SELECT varCurrSymbol FROM [Currency] WHERE numCurrencyID = Opp.[numCurrencyID]),'''') + '' ''+ CONVERT(VARCHAR,CONVERT(DECIMAL(20,5),monPAmount),1)'
                                      WHEN @vcDbColumnName = 'numDivisionID' THEN 'Div.numDivisionID'
                                      WHEN @vcDbColumnName = 'tintOppStatus' THEN 'CASE tintOppStatus WHEN 0 then ''Open'' WHEN 1 THEN ''Closed-Won'' WHEN 2 THEN ''Closed-Lost'' END '
                                      WHEN @vcDbColumnName = 'tintOppType' THEN 'CASE tintOppType WHEN 1 then ''Sales'' WHEN 2 THEN ''Purchase'' Else ''NA'' END '
									  WHEN @vcDbColumnName = 'vcPOppName' THEN 'dbo.CheckChildRecord(Opp.numOppId,Opp.numDomainID)'
									  WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'vcBizDocsList' THEN 'ISNULL((SELECT SUBSTRING((SELECT ''$^$'' + CAST(numOppBizDocsId AS VARCHAR(18)) +''#^#''+ vcBizDocID
										FROM OpportunityBizDocs WHERE numOppId=Opp.numOppId AND 1 = (Case When bitAuthoritativeBizDocs=1 Or numBizDocID=293 Then 0 ELSE 1 END)  FOR XML PATH('''')),4,200000)),'''')'
                                      ELSE @Prefix + @vcDbColumnName
                                    END) + ' LIKE ''%' + @SearchText + '%'''
				END
            END
            ELSE
            IF @vcAssociatedControlType = 'TextArea'
            BEGIN
				SET @strColumns=@strColumns+','+  @Prefix + @vcDbColumnName +' ['+ @vcColumnName+']' 

				IF LEN(@SearchText) > 0
				BEGIN
					SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + @Prefix + @vcDbColumnName + ' LIKE ''%' + @SearchText + '%'''
				END
            END
            ELSE IF  @vcAssociatedControlType='Label'                                              
			BEGIN
				SET @strColumns=@strColumns + ',' + CASE               
				WHEN @vcDbColumnName = 'vcTrackingDetail' THEN 'STUFF((SELECT 
																			CONCAT(''<br/>'',OpportunityBizDocs.vcBizDocID,'': '',vcTrackingDetail)
																		FROM 
																			ShippingReport 
																		INNER JOIN 
																			OpportunityBizDocs 
																		ON 
																			ShippingReport.numOppBizDocId=OpportunityBizDocs.numOppBizDocsId 
																		WHERE 
																			ShippingReport.numOppId=Opp.numOppID
																			AND ISNULL(ShippingReport.vcTrackingDetail,'''') <> ''''
																		FOR XML PATH(''''), TYPE).value(''(./text())[1]'',''varchar(max)''), 1, 5, '''')'     
				WHEN @vcDbColumnName = 'CalAmount' THEN 'Opp.monDealAmount'--'[dbo].[getdealamount](Opp.numOppId,Getutcdate(),0)'
				WHEN @vcDbColumnName = 'vcInventoryStatus' THEN 'ISNULL(dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID,1),'''')'
				WHEN @vcDbColumnName = 'vcOrderedShipped' THEN CONCAT('dbo.GetOrderShipReceiveStatus(Opp.numOppId,1,Opp.tintshipped,',@tintDecimalPoints,')')
				WHEN @vcDbColumnName = 'vcOrderedReceived' THEN CONCAT('dbo.GetOrderShipReceiveStatus(Opp.numOppId,2,Opp.tintshipped,',@tintDecimalPoints,')')
				WHEN @vcDbColumnName = 'vcShipStreet' THEN 'dbo.fn_getOPPState(Opp.numOppId,Opp.numDomainID,2)'
				WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'monDealAmount' THEN 'ISNULL((SELECT SUM(ISNULL(monDealAmount,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)'
				WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'monAmountPaid' THEN 'ISNULL((SELECT SUM(ISNULL(monAmountPaid,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)'
				ELSE @Prefix + @vcDbColumnName END +' ['+ @vcColumnName+']'   
				
				IF @vcDbColumnName='vcOrderedShipped'
				BEGIN
				DECLARE @temp VARCHAR(MAX)
				SET @temp = '(SELECT 
				STUFF((SELECT '', '' + CAST((CAST(numOppBizDocsId AS varchar)+''~''+CAST(ISNULL(numShippingReportId,0) AS varchar) ) AS VARCHAR(MAX)) [text()]
				FROM OpportunityBizDocs
				LEFT JOIN ShippingReport
				ON ShippingReport.numOppBizDocId=OpportunityBizDocs.numOppBizDocsId
				WHERE OpportunityBizDocs.numOppId=Opp.numOppId  AND OpportunityBizDocs.bitShippingGenerated=1  FOR XML PATH(''''), TYPE)
						.value(''.'',''NVARCHAR(MAX)''),1,2,'' ''))'
					SET @strColumns = @strColumns + ' , (SELECT SUBSTRING('+ @temp +', 2, 200000))AS ShippingIcons '
					SET @strColumns = @strColumns + ' , CASE WHEN (SELECT COUNT(*) FROM ShippingBox INNER JOIN ShippingReport ON ShippingBox.numShippingReportId = ShippingReport.numShippingReportId WHERE ShippingReport.numOppID = Opp.numOppID AND LEN(vcTrackingNumber) > 0) > 0 THEN 1 ELSE 0 END [ISTrackingNumGenerated]'
					SET @strColumns = @strColumns + ' ,dbo.fn_GetListItemName(intUsedShippingCompany) AS ShipVia '
					SET @strColumns = @strColumns + ' ,Opp.intUsedShippingCompany AS intUsedShippingCompany '
					--SET @strColumns = @strColumns + ' ,Opp.numShippingService AS numShippingService '
					SET @strColumns = @strColumns + ' ,(SELECT numFieldId FROM DycFieldMaster WHERE vcDbColumnName=''intUsedShippingCompany'' AND numModuleID=3) AS ShipViaFieldId '
					SET @strColumns = @strColumns + ' ,(SELECT numFieldId FROM DycFieldMaster WHERE vcDbColumnName=''numShippingService'' AND numModuleID=3) AS ShippingServiceFieldId '
					SET @strColumns=@strColumns+ ' ,ISNULL((SELECT vcShipmentService FROM ShippingService WHERE (numDomainID=Opp.numDomainID OR ISNULL(numDomainID,0)=0) AND numShipmentServiceID = ISNULL(Opp.numShippingService,0)),'''')' + ' AS vcShipmentService'
				
				END

				IF LEN(@SearchText) > 0
				BEGIN
					SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + (CASE                    
					WHEN @vcDbColumnName = 'CalAmount' THEN 'Opp.monDealAmount'
					WHEN @vcDbColumnName = 'vcInventoryStatus' THEN 'ISNULL(dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID,1),'''')'
					WHEN @vcDbColumnName = 'vcOrderedShipped' THEN CONCAT('dbo.GetOrderShipReceiveStatus(Opp.numOppId,1,Opp.tintshipped,',@tintDecimalPoints,')')
					WHEN @vcDbColumnName = 'vcOrderedReceived' THEN CONCAT('dbo.GetOrderShipReceiveStatus(Opp.numOppId,2,Opp.tintshipped,',@tintDecimalPoints,')')
					WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'monDealAmount' THEN 'ISNULL((SELECT SUM(ISNULL(monDealAmount,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)'
					WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'monAmountPaid' THEN 'ISNULL((SELECT SUM(ISNULL(monAmountPaid,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)'
					else @Prefix + @vcDbColumnName END) + ' LIKE ''%' + @SearchText + '%'''
				END    
			END
            ELSE IF @vcAssociatedControlType='Popup' and @vcDbColumnName='numShareWith'                                                   
			BEGIN
				SET @strColumns = @strColumns + ',(SELECT SUBSTRING(
								(SELECT '','' + A.vcFirstName+'' ''+A.vcLastName + CASE WHEN ISNULL(SR.numContactType,0)>0 THEN ''('' + dbo.fn_GetListItemName(SR.numContactType) + '')'' ELSE '''' END
								FROM ShareRecord SR JOIN UserMaster UM ON UM.numUserDetailId=SR.numAssignedTo 
								JOIN AdditionalContactsInformation A ON UM.numUserDetailId=A.numContactID 
								WHERE SR.numDomainID=Opp.numDomainID AND SR.numModuleID=3 AND SR.numRecordID=Opp.numOppId
								AND UM.numDomainID=Opp.numDomainID and UM.numDomainID=A.numDomainID FOR XML PATH('''')),2,200000)) ['+ @vcColumnName+']'                
			END 
        END
		ELSE
        BEGIN
			SET @vcColumnName= @vcDbColumnName +'~'+ convert(varchar(10),@numFieldId)+'~1'

			IF @vcAssociatedControlType = 'TextBox' or @vcAssociatedControlType = 'TextArea'
            BEGIN
              SET @strColumns = @strColumns
                              + ',CFW'
                              + CONVERT(VARCHAR(3),@tintOrder)
                              + '.Fld_Value  ['
                              + @vcColumnName + ']'
              SET @WhereCondition = @WhereCondition
                                      + ' left Join CFW_FLD_Values_Opp CFW'
                                      + CONVERT(VARCHAR(3),@tintOrder)
                                      + ' on CFW'
                                      + CONVERT(VARCHAR(3),@tintOrder)
                                      + '.Fld_Id='
                                      + CONVERT(VARCHAR(10),@numFieldId)
                                      + 'and CFW'
                                      + CONVERT(VARCHAR(3),@tintOrder)
                                      + '.RecId=Opp.numOppid   '
			END
			ELSE
            IF @vcAssociatedControlType = 'CheckBox' 
            BEGIN
                SET @strColumns = @strColumns
                                + ',case when isnull(CFW'
                                + CONVERT(VARCHAR(3),@tintOrder)
                                + '.Fld_Value,0)=0 then 0 when isnull(CFW'
                                + CONVERT(VARCHAR(3),@tintOrder)
                                + '.Fld_Value,0)=1 then 1 end   ['
                                + @vcColumnName + ']'
                SET @WhereCondition = @WhereCondition
                                        + ' left Join CFW_FLD_Values_Opp CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + ' on CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.Fld_Id='
                                        + CONVERT(VARCHAR(10),@numFieldId)
                                        + 'and CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.RecId=Opp.numOppid   '
            END
            ELSE
            IF @vcAssociatedControlType = 'DateField'
            BEGIN
                  SET @strColumns = @strColumns
                                  + ',dbo.FormatedDateFromDate(CFW'
                                  + CONVERT(VARCHAR(3),@tintOrder)
                                  + '.Fld_Value,'
                                  + CONVERT(VARCHAR(10),@numDomainId)
                                  + ')  ['
                                  + @vcColumnName + ']'
                  SET @WhereCondition = @WhereCondition
                                          + ' left Join CFW_FLD_Values_Opp CFW'
                                          + CONVERT(VARCHAR(3),@tintOrder)
                                          + ' on CFW'
                                          + CONVERT(VARCHAR(3),@tintOrder)
                                          + '.Fld_Id='
                                          + CONVERT(VARCHAR(10),@numFieldId)
                                          + 'and CFW'
                                          + CONVERT(VARCHAR(3),@tintOrder)
                                          + '.RecId=Opp.numOppid    '
            END
            ELSE
            IF @vcAssociatedControlType = 'SelectBox'
            BEGIN
                SET @vcDbColumnName = 'DCust'
                                            + CONVERT(VARCHAR(10),@numFieldId)
                SET @strColumns = @strColumns
                                + ',L'
                                + CONVERT(VARCHAR(3),@tintOrder)
                                + '.vcData'
                                + ' ['
                                + @vcColumnName + ']'

                SET @WhereCondition = @WhereCondition
                                        + ' left Join CFW_FLD_Values_Opp CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + ' on CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.Fld_Id='
                                        + CONVERT(VARCHAR(10),@numFieldId)
                                        + 'and CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.RecId=Opp.numOppid '

                SET @WhereCondition = @WhereCondition
                                        + ' left Join ListDetails L'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + ' on L'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.numListItemID=CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.Fld_Value'
            END
			ELSE
			BEGIN
				SET @strColumns = @strColumns + CONCAT(',dbo.GetCustFldValueOpp(',@numFieldId,',opp.numOppID)') + ' [' + @vcColumnName + ']'
			END

			IF LEN(@SearchText) > 0
			BEGIN
				SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + CONCAT('dbo.GetCustFldValueOpp(',@numFieldId,',opp.numOppID) LIKE ''%',@SearchText,'%''')
			END
		END
      
     
		SELECT TOP 1 
			@tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName,@vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,
			@numListID=numListID,@vcLookBackTableName=vcLookBackTableName,@bitCustom=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,@bitAllowEdit=bitAllowEdit,
			@ListRelID=ListRelID
		FROM 
			#tempForm 
		WHERE 
			tintOrder > @tintOrder-1 
		ORDER BY 
			tintOrder asc            
 
		IF @@rowcount=0 SET @tintOrder=0 
	END 

	

	DECLARE @strShareRedordWith AS VARCHAR(300);
	SET @strShareRedordWith=' Opp.numOppId in (select SR.numRecordID FROM ShareRecord SR WHERE SR.numDomainID='+ convert(varchar(15),@numDomainID )+' AND SR.numModuleID=3 AND SR.numAssignedTo='+convert(varchar(15),@numUserCntId)+') '

	DECLARE @strExternalUser AS VARCHAR(MAX) = ''
	SET @strExternalUser = CONCAT(' (NOT EXISTS (SELECT numUserDetailID FROM UserMaster WHERE numDomainID=',@numDomainID,' AND numUserDetailID=',@numUserCntId,') AND EXISTS (SELECT EAD.numExtranetDtlID FROM ExtranetAccountsDtl EAD INNER JOIN ExtarnetAccounts EA ON EAD.numExtranetID=EA.numExtranetID WHERE EAD.numDomainID=',@numDomainID,' AND EAD.numContactID=',@numUserCntID,' AND EA.numDivisionID=Div.numDivisionID))')

	DECLARE @StrSql AS VARCHAR(MAX) = ''

	SET @StrSql = @StrSql + ' FROM OpportunityMaster Opp                                               
                                 LEFT JOIN AdditionalContactsInformation ADC ON Opp.numContactId = ADC.numContactId   
								 LEFT JOIN (SELECT OIInner.numOppID,COUNT(*) numApprovalCount FROM OpportunityItems OIInner WHERE ISNULL(OIInner.bitItemPriceApprovalRequired,0)=1 GROUP BY OIInner.numOppID) TEMPApproval ON Opp.numOppID=TEMPApproval.numOppID
								 LEFT JOIN (SELECT OIInner.numOppID,COUNT(*) numMappingCount FROM OpportunityItems OIInner WHERE ISNULL(OIInner.bitMappingRequired,0)=1 GROUP BY OIInner.numOppID) TEMPMapping ON Opp.numOppID=TEMPMapping.numOppID                                                            
                                 INNER JOIN DivisionMaster Div ON Opp.numDivisionId = Div.numDivisionID 
								 INNER JOIN CompanyInfo cmp ON Div.numCompanyID = cmp.numCompanyId 
								 LEFT JOIN ProjectProgress PP On PP.numOppId = Opp.numOppId ' + @WhereCondition
								 --INNER JOIN Correspondence Corr ON opp.numOppId = Corr.numOpenRecordID 
	-------Change Row Color-------
	
	DECLARE @vcCSOrigDbCOlumnName AS VARCHAR(50) = ''
	DECLARE @vcCSLookBackTableName AS VARCHAR(50) = ''
	DECLARE @vcCSAssociatedControlType AS VARCHAR(50)

	CREATE TABLE #tempColorScheme
	(
		vcOrigDbCOlumnName VARCHAR(50),
		vcLookBackTableName VARCHAR(50),
		vcFieldValue VARCHAR(50),
		vcFieldValue1 VARCHAR(50),
		vcColorScheme VARCHAR(50),
		vcAssociatedControlType varchar(50)
	)

	INSERT INTO 
		#tempColorScheme 
	SELECT 
		DFM.vcOrigDbCOlumnName,
		DFM.vcLookBackTableName,
		DFCS.vcFieldValue,
		DFCS.vcFieldValue1,
		DFCS.vcColorScheme,
		DFFM.vcAssociatedControlType 
	FROM 
		DycFieldColorScheme DFCS 
	JOIN 
		DycFormField_Mapping DFFM 
	ON 
		DFCS.numFieldID=DFFM.numFieldID
	JOIN 
		DycFieldMaster DFM 
	ON 
		DFM.numModuleID=DFFM.numModuleID and DFM.numFieldID=DFFM.numFieldID
	WHERE
		DFCS.numDomainID=@numDomainID 
		AND DFFM.numFormID=@numFormId 
		AND DFCS.numFormID=@numFormId 
		AND isnull(DFFM.bitAllowGridColor,0)=1

	IF(SELECT COUNT(*) FROM #tempColorScheme)>0
	BEGIN
		SELECT TOP 1 @vcCSOrigDbCOlumnName=vcOrigDbCOlumnName,@vcCSLookBackTableName=vcLookBackTableName,@vcCSAssociatedControlType=vcAssociatedControlType from #tempColorScheme
	END                        

	IF len(@vcCSOrigDbCOlumnName)>0 and len(@vcCSLookBackTableName)>0
	BEGIN
		SET @strColumns=@strColumns + ',tCS.vcColorScheme'
	END

	IF len(@vcCSOrigDbCOlumnName)>0 and len(@vcCSLookBackTableName)>0
	BEGIN
		IF @vcCSLookBackTableName = 'AdditionalContactsInformation'                  
			SET @Prefix = 'ADC.'                  
		IF @vcCSLookBackTableName = 'DivisionMaster'                  
			SET @Prefix = 'Div.'
		if @vcCSLookBackTableName = 'CompanyInfo'                  
			SET @Prefix = 'CMP.'   
		if @vcCSLookBackTableName = 'OpportunityMaster'                  
			SET @Prefix = 'Opp.'   
 
		IF @vcCSAssociatedControlType='DateField'
		BEGIN
			SET @strSql=@strSql+' LEFT JOIN #tempColorScheme tCS ON CONVERT(VARCHAR(10),' + @Prefix + @vcCSOrigDbCOlumnName + ',111) >= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue as int),GETUTCDATE()),111)
				 and CONVERT(VARCHAR(10),' + @Prefix + @vcCSOrigDbCOlumnName + ',111) <= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue1 as int),GETUTCDATE()),111)'
		END
		ELSE IF @vcCSAssociatedControlType='SelectBox' OR @vcCSAssociatedControlType='ListBox' OR @vcCSAssociatedControlType='CheckBox'
		BEGIN
			SET @strSql=@strSql+' LEFT JOIN #tempColorScheme tCS on tCS.vcFieldValue=' + @Prefix + @vcCSOrigDbCOlumnName
		END
	END
                          
	IF @columnName like 'CFW.Cust%'             
	BEGIN            
		SET @strSql = @strSql + ' LEFT JOIN CFW_Fld_Values_Opp CFW on CFW.RecId=Opp.numOppid and CFW.fld_id= '+ REPLACE(@columnName,'CFW.Cust','') +' '                                                     
		DECLARE @vcSortFieldType VARCHAR(50)

		SELECT @vcSortFieldType = Fld_type FROM CFW_Fld_Master WHERE Fld_id=CAST(REPLACE(@columnName,'CFW.Cust','') AS INT)

		IF ISNULL(@vcSortFieldType,'') = 'DateField'
		BEGIN
			SET @columnName='(CASE WHEN ISDATE(CFW.Fld_Value) = 1 THEN CAST(CFW.Fld_Value AS DATE) ELSE NULL END)'
		END
		ELSE IF CAST(REPLACE(@columnName,'CFW.Cust','') AS INT) IN (12745,12846)
		BEGIN
			SET @columnName='(CASE WHEN ISNUMERIC(CFW.Fld_Value) = 1 THEN CAST(CFW.Fld_Value AS FLOAT) ELSE NULL END)'
		END
		ELSE
		BEGIN
			SET @columnName='CFW.Fld_Value'
		END            
	END 

	IF @bitPartner = 1
		SET @strSql = @strSql + ' LEFT JOIN OpportunityContact OppCont ON OppCont.numOppId=Opp.numOppId AND OppCont.bitPartner=1 AND OppCont.numContactId=' + CONVERT(VARCHAR(15),@numUserCntID)
                    
                
	IF @tintFilterBy = 1 --Partially Fulfilled Orders 
		SET @strSql = @strSql + ' LEFT JOIN AdditionalContactsInformation ADC1 ON ADC1.numContactId=Opp.numRecOwner                                                               
                             LEFT OUTER JOIN [OpportunityBizDocs] OB ON OB.[numOppId] = Opp.[numOppId] 
                             WHERE Opp.numDomainID=' + CONVERT(VARCHAR(15),@numDomainID) + ' and Opp.tintOppstatus=1 and (Opp.tintShipped=' + CONVERT(VARCHAR(15),@tintShipped) + ' OR ' + CONVERT(VARCHAR(15),@tintShipped) + '=2)'
	ELSE
		SET @strSql = @strSql + ' LEFT JOIN AdditionalContactsInformation ADC1 on ADC1.numContactId=Opp.numRecOwner                                                               
                             WHERE Opp.numDomainID=' + CONVERT(VARCHAR(15),@numDomainID) + ' and Opp.tintOppstatus=1 and (Opp.tintShipped='+ CONVERT(VARCHAR(15),@tintShipped)  + ' OR ' + CONVERT(VARCHAR(15),@tintShipped) + '=2)'
  
  
	IF @intOrder <> 0
    BEGIN
      IF @intOrder = 1
        SET @strSql = @strSql + ' and Opp.bitOrder = 0 '
      IF @intOrder = 2
        SET @strSql = @strSql + ' and Opp.bitOrder = 1'
    END
    
	IF @numCompanyID <> 0
		SET @strSql = @strSql + ' And Div.numCompanyID ='+ CONVERT(VARCHAR(15),@numCompanyID)

	IF @SortChar <> '0' 
		SET @strSql = @strSql + ' And Opp.vcPOppName like ''' + @SortChar + '%'''
  

	IF @inttype = 3
	BEGIN
		IF @tintSalesUserRightType = 0
			SET @strSql = @strSql + ' AND opp.tintOppType=0'
		ELSE IF @tintSalesUserRightType = 1
			SET @strSql = CONCAT(@strSql,' AND (Opp.numRecOwner= ',@numUserCntID,' or Opp.numAssignedTo=',@numUserCntID
						,(CASE WHEN @bitPartner = 1 THEN ' or ( OppCont.bitPartner=1 and OppCont.numContactId='+ CONVERT(VARCHAR(15),@numUserCntID) + ')' ELSE '' END)
						,' or ',@strShareRedordWith
						,' or ',@strExternalUser,')')
		ELSE IF @tintSalesUserRightType = 2
			SET @strSql = @strSql + ' AND (Div.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+ CONVERT(VARCHAR(15),@numUserCntID) + ' ) 
									 or Opp.numAssignedTo= '+ CONVERT(VARCHAR(15),@numUserCntID)
									+ ' or ' + @strShareRedordWith +')'
	END
	ELSE IF @inttype = 4
	BEGIN
		IF @tintPurchaseUserRightType = 0
			SET @strSql = @strSql + ' AND opp.tintOppType=0'
		ELSE IF @tintPurchaseUserRightType = 1
			SET @strSql = CONCAT(@strSql,' AND (Opp.numRecOwner= ',@numUserCntID
						, ' or Opp.numAssignedTo= ',@numUserCntID
						, CASE WHEN @bitPartner = 1 THEN ' or ( OppCont.bitPartner=1 and OppCont.numContactId='+ CONVERT(VARCHAR(15),@numUserCntID) + ')' ELSE '' END
						, ' or ' + @strShareRedordWith
						,' or ',@strExternalUser,')')
		ELSE IF @tintPurchaseUserRightType = 2
			SET @strSql = @strSql + ' AND (Div.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+ CONVERT(VARCHAR(15),@numUserCntID) + ' ) 
									 or Opp.numAssignedTo= '+ CONVERT(VARCHAR(15),@numUserCntID)
									+ ' or ' + @strShareRedordWith +')'
	END

	
					
	IF @tintSortOrder <> '0'
		SET @strSql = @strSql + '  AND Opp.tintOppType= '+ CONVERT(VARCHAR(1),@tintSortOrder)
    
    
	IF @tintFilterBy = 1 --Partially Fulfilled Orders
		SET @strSql = @strSql + ' AND OB.bitPartialFulfilment = 1 AND OB.bitAuthoritativeBizDocs = 1 '
	ELSE IF @tintFilterBy = 2 --Fulfilled Orders
		SET @strSql = @strSql + ' AND Opp.[numOppId] IN (SELECT OM.numOppId FROM [OpportunityMaster] OM
                      			INNER JOIN [OpportunityBizDocs] OB ON OB.[numOppId] = OM.[numOppId]
                      			INNER JOIN [OpportunityItems] OI ON OM.[numOppId] = OI.[numOppId]
                      			INNER JOIN [OpportunityBizDocItems] BI ON BI.[numOppBizDocID] = OB.[numOppBizDocsId]
                      			Where OM.numDomainId=' + CONVERT(VARCHAR(15),@numDomainId) + 'AND OB.[bitAuthoritativeBizDocs] = 1 GROUP BY OM.[numOppId]
                      			HAVING   COUNT(OI.[numUnitHour]) = COUNT(BI.[numUnitHour])) '
	ELSE IF @tintFilterBy = 3 --Orders without Authoritative-BizDocs
		SET @strSql = @strSql + ' AND Opp.[numOppId] not IN (SELECT DISTINCT OM.[numOppId]
                      		  FROM [OpportunityMaster] OM JOIN [OpportunityBizDocs] OB ON OM.[numOppId] = OB.[numOppId]
                      		  Where OM.numDomainId=' + CONVERT(VARCHAR(15),@numDomainId) + ' AND OM.tintOppstatus=1 AND (OM.tintShipped=' + CONVERT(VARCHAR(15),@tintShipped) + ' OR ' + CONVERT(VARCHAR(15),@tintShipped) + '=2)' + ' AND OM.tintOppType= '+ CONVERT(VARCHAR(1),@tintSortOrder) + ' AND isnull(OB.[bitAuthoritativeBizDocs],0)=1) '
	ELSE IF @tintFilterBy = 4 --Orders with items yet to be added to Invoices
		SET @strSql = @strSql + ' AND Opp.[numOppId] IN (SELECT distinct OPPM.numOppId FROM [OpportunityMaster] OPPM
                                  INNER JOIN [OpportunityItems] OPPI ON OPPM.[numOppId] = OPPI.[numOppId]
                                  Where OPPM.numDomainId=' + CONVERT(VARCHAR(15),@numDomainId) + ' 
								  and OPPI.numoppitemtCode not in (select numOppItemID from [OpportunityBizDocs] OB INNER JOIN [OpportunityBizDocItems] BI
                      			  ON BI.[numOppBizDocID] = OB.[numOppBizDocsId] where OB.[numOppId] = OPPM.[numOppId]))'      
	ELSE IF @tintFilterBy = 6 --Orders with deferred Income/Invoices
		SET @strSql = @strSql + ' AND Opp.[numOppId] IN (SELECT DISTINCT OM.[numOppId]
                      		 FROM [OpportunityMaster] OM LEFT OUTER JOIN [OpportunityBizDocs] OB ON OM.[numOppId] = OB.[numOppId]
                      		 Where OM.numDomainId=' + CONVERT(VARCHAR(15),@numDomainId) + ' AND OB.[bitAuthoritativeBizDocs] =1 and OB.tintDeferred=1)'

	IF @numOrderStatus <>0 
		SET @strSql = @strSql + ' AND Opp.numStatus = ' + CONVERT(VARCHAR(15),@numOrderStatus);

	IF CHARINDEX('OI.vcInventoryStatus',@vcRegularSearchCriteria) > 0
	BEGIN
		IF CHARINDEX('OI.vcInventoryStatus=1',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'OI.vcInventoryStatus=1',' CHARINDEX(''Not Applicable'', dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID,1)) > 0 ')
		IF CHARINDEX('OI.vcInventoryStatus=2',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'OI.vcInventoryStatus=2',' CHARINDEX(''Shipped'', dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID,1)) > 0 ')
		IF CHARINDEX('OI.vcInventoryStatus=3',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'OI.vcInventoryStatus=3',' CHARINDEX(''BO'', dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID,1)) > 0 AND CHARINDEX(''Shippable'', dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID,1)) = 0 ')
		IF CHARINDEX('OI.vcInventoryStatus=4',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'OI.vcInventoryStatus=4',' CHARINDEX(''Shippable'', dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID,1)) > 0 ')
	END
	
	IF CHARINDEX('Opp.tintInvoicing',@vcRegularSearchCriteria) > 0
	BEGIN
		IF CHARINDEX('Opp.tintInvoicing=0',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.tintInvoicing=0',' CHARINDEX(''All'', dbo.CheckOrderInvoicingStatus(Opp.numOppID,Opp.numDomainID,1)) > 0 ')
		IF CHARINDEX('Opp.tintInvoicing=1',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.tintInvoicing=1',' (SELECT (CASE WHEN SUM(ISNULL(InvoicedQty,0)) = SUM(ISNULL(OrderedQty,0)) THEN 1 ELSE 0 END)
	FROM 
	(
		SELECT OI.numoppitemtCode, ISNULL(OI.numUnitHour,0) AS OrderedQty, ISNULL(TempInvoice.InvoicedQty,0) AS InvoicedQty
		FROM OpportunityItems OI
		INNER JOIN Item I ON OI.numItemCode = I.numItemCode
		OUTER APPLY
		(
			SELECT SUM(OpportunityBizDocItems.numUnitHour) AS InvoicedQty
			FROM OpportunityBizDocs
				INNER JOIN OpportunityBizDocItems ON OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
			WHERE OpportunityBizDocs.numOppId = Opp.numOppID
				AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
				AND numBizDocId IN (SELECT numAuthoritativeSales FROM AuthoritativeBizDocs WHERE numDomainId = Opp.numDomainID)
		) AS TempInvoice
		WHERE
		OI.numOppID = Opp.numOppID
	) X) > 0 ')
		IF CHARINDEX('Opp.tintInvoicing=2',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.tintInvoicing=2',' (SELECT COUNT(*) 
		FROM OpportunityBizDocs 
		WHERE numOppId = Opp.numOppID 
			AND numBizDocId IN (SELECT numAuthoritativeSales FROM AuthoritativeBizDocs WHERE numDomainId = Opp.numDomainID)) = 0 ')
		IF CHARINDEX('Opp.tintInvoicing=3',@vcRegularSearchCriteria) > 0 
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.tintInvoicing=3',' (SELECT (CASE WHEN SUM(ISNULL(InvoicedQty,0)) > 0 THEN SUM(ISNULL(OrderedQty,0)) - SUM(ISNULL(InvoicedQty,0)) ELSE 0 END)
	FROM 
	(
		SELECT OI.numoppitemtCode, ISNULL(OI.numUnitHour,0) AS OrderedQty, ISNULL(TempInvoice.InvoicedQty,0) AS InvoicedQty
		FROM OpportunityItems OI
		INNER JOIN Item I ON OI.numItemCode = I.numItemCode
		OUTER APPLY
		(
			SELECT SUM(OpportunityBizDocItems.numUnitHour) AS InvoicedQty
			FROM OpportunityBizDocs
				INNER JOIN OpportunityBizDocItems ON OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
			WHERE OpportunityBizDocs.numOppId = Opp.numOppID
				AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
				AND numBizDocId IN (SELECT numAuthoritativeSales FROM AuthoritativeBizDocs WHERE numDomainId = Opp.numDomainID)
		) AS TempInvoice
		WHERE
		OI.numOppID = Opp.numOppID
	) X) > 0 ')
		
		IF CHARINDEX('Opp.tintInvoicing=4',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.tintInvoicing=4',' (SELECT (CASE WHEN SUM(ISNULL(InvoicedQty,0)) > 0 THEN SUM(ISNULL(OrderedQty,0)) - SUM(ISNULL(InvoicedQty,0)) ELSE 0 END)
	FROM 
	(
		SELECT OI.numoppitemtCode, ISNULL(OI.numUnitHour,0) AS OrderedQty, ISNULL(TempInvoice.InvoicedQty,0) AS InvoicedQty
		FROM OpportunityItems OI
		INNER JOIN Item I ON OI.numItemCode = I.numItemCode
		OUTER APPLY
		(
			SELECT SUM(OpportunityBizDocItems.numUnitHour) AS InvoicedQty
			FROM OpportunityBizDocs
				INNER JOIN OpportunityBizDocItems ON OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
			WHERE OpportunityBizDocs.numOppId = Opp.numOppID
				AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
				AND numBizDocId IN (SELECT numAuthoritativeSales FROM AuthoritativeBizDocs WHERE numDomainId = Opp.numDomainID)
		) AS TempInvoice
		WHERE
		OI.numOppID = Opp.numOppID
	) X) > 0 ')

		IF CHARINDEX('Opp.tintInvoicing=5',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.tintInvoicing=5',' 
				Opp.[numOppId] IN (SELECT DISTINCT OM.[numOppId]
                      		 FROM [OpportunityMaster] OM INNER JOIN [OpportunityBizDocs] OB ON OM.[numOppId] = OB.[numOppId]
                      		 Where OM.numDomainId = Opp.numDomainID AND OB.[bitAuthoritativeBizDocs] = 1 and OB.tintDeferred=1) ')
	END
	
	IF CHARINDEX('Opp.tintEDIStatus',@vcRegularSearchCriteria) > 0
	BEGIN
		--IF CHARINDEX('Opp.tintEDIStatus=1',@vcRegularSearchCriteria) > 0
		--	SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.tintEDIStatus=1',' 1 = (SELECT CASE WHEN Opp.tintEDIStatus = 1 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.tintEDIStatus=2',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.tintEDIStatus=2',' 1 = (SELECT CASE WHEN Opp.tintEDIStatus = 2 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.tintEDIStatus=3',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.tintEDIStatus=3',' 1 = (SELECT CASE WHEN Opp.tintEDIStatus = 3 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.tintEDIStatus=4',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.tintEDIStatus=4',' 1 = (SELECT CASE WHEN Opp.tintEDIStatus = 4 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.tintEDIStatus=5',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.tintEDIStatus=5',' 1 = (SELECT CASE WHEN Opp.tintEDIStatus = 5 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.tintEDIStatus=6',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.tintEDIStatus=6',' 1 = (SELECT CASE WHEN Opp.tintEDIStatus = 6 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.tintEDIStatus=7',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.tintEDIStatus=7',' 1 = (SELECT CASE WHEN Opp.tintEDIStatus = 7 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.tintEDIStatus=7',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.tintEDIStatus=11',' 1 = (SELECT CASE WHEN Opp.tintEDIStatus = 11 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.tintEDIStatus=7',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.tintEDIStatus=12',' 1 = (SELECT CASE WHEN Opp.tintEDIStatus = 12 THEN 1 ELSE 0 END) ')
	END

	IF CHARINDEX('Opp.vcSignatureType',@vcRegularSearchCriteria) > 0
	BEGIN
		IF CHARINDEX('Opp.vcSignatureType=0',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.vcSignatureType=0',' 1 = (SELECT CASE WHEN (SELECT COUNT(*) FROM DivisionMaster AS D 
																			LEFT JOIN DivisionMasterShippingConfiguration AS DMSC ON DMSC.numDivisionID=D.numDivisionID 
																			WHERE D.numDivisionID=Div.numDivisionID AND vcSignatureType=0 ) > 0 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.vcSignatureType=1',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.vcSignatureType=1','  1 = (SELECT CASE WHEN (SELECT COUNT(*) FROM DivisionMaster AS D 
																			LEFT JOIN DivisionMasterShippingConfiguration AS DMSC ON DMSC.numDivisionID=D.numDivisionID 
																			WHERE D.numDivisionID=Div.numDivisionID AND vcSignatureType=1 ) > 0 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.vcSignatureType=2',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.vcSignatureType=2',' 1 = (SELECT CASE WHEN (SELECT COUNT(*) FROM DivisionMaster AS D 
																			LEFT JOIN DivisionMasterShippingConfiguration AS DMSC ON DMSC.numDivisionID=D.numDivisionID 
																			WHERE D.numDivisionID=Div.numDivisionID AND vcSignatureType=2 ) > 0 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.vcSignatureType=3',@vcRegularSearchCriteria) > 0								   
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.vcSignatureType=3',' 1 = (SELECT CASE WHEN (SELECT COUNT(*) FROM DivisionMaster AS D 
																			LEFT JOIN DivisionMasterShippingConfiguration AS DMSC ON DMSC.numDivisionID=D.numDivisionID 
																			WHERE D.numDivisionID=Div.numDivisionID AND vcSignatureType=3 ) > 0 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.vcSignatureType=4',@vcRegularSearchCriteria) > 0								   
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.vcSignatureType=4',' 1 = (SELECT CASE WHEN (SELECT COUNT(*) FROM DivisionMaster AS D 
																			LEFT JOIN DivisionMasterShippingConfiguration AS DMSC ON DMSC.numDivisionID=D.numDivisionID 
																			WHERE D.numDivisionID=Div.numDivisionID AND vcSignatureType=4 ) > 0 THEN 1 ELSE 0 END) ')
	END

	IF CHARINDEX('OBD.monAmountPaid',@vcRegularSearchCriteria) > 0
	BEGIN
		IF CHARINDEX('OBD.monAmountPaid like ''%0%''',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'OBD.monAmountPaid like ''%0%''',' 1 = (SELECT ISNULL((SELECT SUM(ISNULL(monAmountPaid,0)) FROM  OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)) ')
		IF CHARINDEX('OBD.monAmountPaid like ''%1%''',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'OBD.monAmountPaid like ''%1%''','  1 = (SELECT (CASE WHEN (SELECT (CASE WHEN (SUM(ISNULL(monDealAmount,0)) = SUM(ISNULL(monAmountPaid,0)) AND SUM(ISNULL(monAmountPaid,0)) <> 0) THEN 1 ELSE 0 END) FROM OpportunityBizDocs BD WHERE BD.numOppId =  Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0) = 1) > 0 THEN 1 ELSE 0 END))  ')
		IF CHARINDEX('OBD.monAmountPaid like ''%2%''',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'OBD.monAmountPaid like ''%2%''',' 1 = (SELECT CASE WHEN (SELECT (CASE WHEN ((SUM(ISNULL(monDealAmount,0)) - SUM(ISNULL(monAmountPaid,0))) > 0 AND SUM(ISNULL(monAmountPaid,0)) <> 0) THEN 1 ELSE 0 END) FROM OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0) = 1) > 0 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('OBD.monAmountPaid like ''%3%''',@vcRegularSearchCriteria) > 0								   
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'OBD.monAmountPaid like ''%3%''',' 1 = (SELECT CASE WHEN (SELECT (CASE WHEN (SUM(ISNULL(monAmountPaid,0)) = 0) THEN 1 ELSE 0 END) FROM OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0) = 1) > 0 THEN 1 ELSE 0 END) ')
	END
	
	
	IF @vcRegularSearchCriteria<>'' 
	BEGIN
		SET @strSql=@strSql +' AND ' + @vcRegularSearchCriteria 
	END
	
	


	IF @vcCustomSearchCriteria<>'' 
	BEGIN
		SET @strSql = @strSql +' AND ' +  @vcCustomSearchCriteria
	END

	IF LEN(@SearchQuery) > 0
	BEGIN
		SET @strSql = @strSql + ' AND (' + @SearchQuery + ') '
	END

	IF ISNULL(@tintDashboardReminderType,0) = 19
	BEGIN
		SET @strSql = CONCAT(@strSql,' AND Opp.numOppID IN (','SELECT DISTINCT
																	OMInner.numOppID
																FROM 
																	StagePercentageDetails SPDInner
																INNER JOIN 
																	OpportunityMaster OMInner
																ON 
																	SPDInner.numOppID=OMInner.numOppId 
																WHERE 
																	SPDInner.numDomainId=',@numDOmainID,'
																	AND OMInner.numDomainId=',@numDOmainID,' 
																	AND OMInner.tintOppType = 1
																	AND ISNULL(OMInner.tintOppStatus,0) = 1
																	AND tinProgressPercentage <> 100',') ')

	END
	ELSE IF ISNULL(@tintDashboardReminderType,0) = 10
	BEGIN
		SET @strSql = CONCAT(@strSql,' AND Opp.numOppID IN (','SELECT DISTINCT
																	OMInner.numOppId
																FROM
																	OpportunityMaster OMInner
																INNER JOIN
																	OpportunityItems OIInner
																ON
																	OIInner.numOppId = OMInner.numOppID
																INNER JOIN
																	Item IInner
																ON
																	OIInner.numItemCode = IInner.numItemCode
																OUTER APPLY
																(
																	SELECT
																		SUM(OpportunityBizDocItems.numUnitHour) AS BilledQty
																	FROM
																		OpportunityBizDocs
																	INNER JOIN
																		OpportunityBizDocItems 
																	ON
																		OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																	WHERE
																		OpportunityBizDocs.numOppId = OMInner.numOppID
																		AND OpportunityBizDocItems.numOppItemID = OIInner.numoppitemtCode
																		AND OpportunityBizDocs.numBizDocId = (SELECT ISNULL(numAuthoritativePurchase,0) FROM AuthoritativeBizDocs WHERE numDomainId=',@numDomainID,')
																) AS TempBilled
																WHERE
																	OMInner.numDomainId = ',@numDomainID,'
																	AND ISNULL(OMInner.bitStockTransfer,0) = 0
																	AND OMInner.tintOppType = 2
																	AND OMInner.tintOppStatus  = 1
																	AND ISNULL(OIInner.numUnitHour,0) <> ISNULL(TempBilled.BilledQty,0)',') ')
	END
	ELSE IF ISNULL(@tintDashboardReminderType,0) = 11
	BEGIN
		SET @strSql = CONCAT(@strSql,' AND Opp.numOppID IN (','SELECT DISTINCT
																	OMInner.numOppId
																FROM
																	OpportunityMaster OMInner
																INNER JOIN
																	OpportunityItems OIInner
																ON
																	OIInner.numOppId = OMInner.numOppId
																INNER JOIN
																	Item IInner
																ON
																	OIInner.numItemCode = IInner.numItemCode
																OUTER APPLY
																(
																	SELECT
																		SUM(OpportunityBizDocItems.numUnitHour) AS PickPacked
																	FROM
																		OpportunityBizDocs
																	INNER JOIN
																		OpportunityBizDocItems 
																	ON
																		OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																	WHERE
																		OpportunityBizDocs.numOppId = OMInner.numOppId
																		AND ISNULL(OpportunityBizDocs.numBizDocId,0) = ',(SELECT ISNULL(numDefaultSalesShippingDoc,0) FROM Domain WHERE numDomainId=@numDomainID),'
																		AND OpportunityBizDocItems.numOppItemID = OIInner.numoppitemtCode
																) AS TempFulFilled
																WHERE
																	OMInner.numDomainId = ',@numDomainID,'
																	AND OMInner.tintOppType = 1
																	AND OMInner.tintOppStatus = 1
																	AND UPPER(IInner.charItemType) = ''P''
																	AND ISNULL(OMInner.tintshipped,0) = 0
																	AND ISNULL(OIInner.bitDropShip,0) = 0
																	AND ISNULL(OIInner.numUnitHour,0) <> ISNULL(TempFulFilled.PickPacked,0)',') ')
	END
	ELSE IF ISNULL(@tintDashboardReminderType,0) = 12
	BEGIN
		SET @strSql = CONCAT(@strSql,' AND Opp.numOppID IN (','SELECT DISTINCT
																	OMInner.numOppId
																FROM
																	OpportunityMaster OMInner
																INNER JOIN
																	OpportunityItems OIInner
																ON
																	OIInner.numOppId = OMInner.numOppId
																INNER JOIN
																	Item IInner
																ON
																	OIInner.numItemCode = IInner.numItemCode
																OUTER APPLY
																(
																	SELECT
																		SUM(OpportunityBizDocItems.numUnitHour) AS FulFilledQty
																	FROM
																		OpportunityBizDocs
																	INNER JOIN
																		OpportunityBizDocItems 
																	ON
																		OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																	WHERE
																		OpportunityBizDocs.numOppId = OMInner.numOppId
																		AND ISNULL(OpportunityBizDocs.numBizDocId,0) = 296
																		AND ISNULL(OpportunityBizDocs.bitFulFilled,0) = 1
																		AND OpportunityBizDocItems.numOppItemID = OIInner.numoppitemtCode
																) AS TempFulFilled
																WHERE
																	OMInner.numDomainId = ',@numDomainID,'
																	AND OMInner.tintOppType = 1
																	AND OMInner.tintOppStatus = 1
																	AND ISNULL(OMInner.tintshipped,0) = 0
																	AND UPPER(IInner.charItemType) = ''P''
																	AND ISNULL(OIInner.bitDropShip,0) = 0
																	AND ISNULL(OIInner.numUnitHour,0) <> ISNULL(TempFulFilled.FulFilledQty,0)',') ')
	END
	ELSE IF ISNULL(@tintDashboardReminderType,0) = 13
	BEGIN
		SET @strSql = CONCAT(@strSql,' AND Opp.numOppID IN (','SELECT DISTINCT
																	OMInner.numOppId
																FROM
																	OpportunityMaster OMInner
																INNER JOIN
																	OpportunityItems OIInner
																ON
																	OIInner.numOppId = OMInner.numOppID
																INNER JOIN
																	Item IInner
																ON
																	OIInner.numItemCode = IInner.numItemCode
																OUTER APPLY
																(
																	SELECT
																		SUM(OpportunityBizDocItems.numUnitHour) AS InvoicedQty
																	FROM
																		OpportunityBizDocs
																	INNER JOIN
																		OpportunityBizDocItems 
																	ON
																		OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																	WHERE
																		OpportunityBizDocs.numOppId = OMInner.numOppID
																		AND OpportunityBizDocItems.numOppItemID = OIInner.numoppitemtCode
																		AND OpportunityBizDocs.numBizDocId = ISNULL((SELECT TOP 1 numAuthoritativeSales FROM AuthoritativeBizDocs WHERE numDomainId=',@numDomainID,'),287)
																) AS TempInvoice
																WHERE
																	OMInner.numDomainId = ',@numDomainID,'
																	AND OMInner.tintOppType = 1
																	AND OMInner.tintOppStatus  = 1
																	AND ISNULL(OIInner.numUnitHour,0) <> ISNULL(TempInvoice.InvoicedQty,0)',') ')
	END
	ELSE IF ISNULL(@tintDashboardReminderType,0) = 14
	BEGIN
		DECLARE @TEMPOppID TABLE
		(
			numOppID NUMERIC(18,0)
		)

		INSERT INTO @TEMPOppID
		(
			numOppID
		)
		SELECT DISTINCT
			OM.numOppID
		FROM
			OpportunityMaster OM
		INNER JOIN
			OpportunityItems OI
		ON
			OM.numOppId = OI.numOppId
		WHERE   
			OM.numDomainId = @numDomainId
			AND OM.tintOppType = 1
			AND OM.tintOppStatus=1
			AND ISNULL(OI.bitDropShip, 0) = 1
			AND 1 = (CASE WHEN (SELECT COUNT(*) FROM OpportunityLinking OMInner INNER JOIN OpportunityItems OIInner ON OMInner.numChildOppID=OIInner.numOppId WHERE OMInner.numParentOppID = OM.numOppID AND OIInner.numItemCode=OI.numItemCode AND ISNULL(OIInner.numWarehouseItmsID,0)=ISNULL(OI.numWarehouseItmsID,0)) > 0 THEN 0 ELSE 1 END)


		If ISNULL((SELECT bitReOrderPoint FROM Domain WHERE numDomainId=@numDomainID),0) = 1
		BEGIN
			INSERT INTO @TEMPOppID
			(
				numOppID
			)
			SELECT DISTINCT
				OM.numOppID
			FROM
				OpportunityMaster OM
			INNER JOIN
				OpportunityItems OI
			ON
				OI.numOppId = OM.numOppId
			INNER JOIN 
				Item I 
			ON 
				I.numItemCode = OI.numItemCode
			INNER JOIN 
				WarehouseItems WI 
			ON 
				OI.numWarehouseItmsID = WI.numWareHouseItemID
			WHERE   
				OM.numDomainId = @numDomainId
				AND ISNULL((SELECT bitReOrderPoint FROM Domain WHERE numDomainId=@numDomainID),0) = 1
				AND (ISNULL(WI.numOnHand,0) + ISNULL(WI.numOnOrder,0) <= ISNULL(WI.numReOrder,0))
				AND OM.tintOppType = 1
				AND OM.tintOppStatus=1
				AND ISNULL(I.bitAssembly, 0) = 0
				AND ISNULL(I.bitKitParent, 0) = 0
				AND ISNULL(OI.bitDropship,0) = 0
				AND OM.numOppId NOT IN (SELECT numOppID FROM @TEMPOppID)

			IF (SELECT COUNT(*) FROM @TEMPOppID) > 0
			BEGIN
				DECLARE @tmpIds VARCHAR(MAX) = ''
				SELECT @tmpIds = CONCAT(@tmpIds,numOppID,', ') FROM @TEMPOppID
				SET @strSql = CONCAT(@strSql,' AND Opp.numOppID IN (',SUBSTRING(@tmpIds, 0, LEN(@tmpIds)),') ')
			END
			ELSE
			BEGIN
				SET @strSql = CONCAT(@strSql,' AND Opp.numOppID IN (0) ')
			END
		END
	END
	ELSE IF ISNULL(@tintDashboardReminderType,0) = 15
	BEGIN
		SET @strSql = CONCAT(@strSql,' AND Opp.numOppID IN (','SELECT
																	OMInner.numOppID
																FROM
																	OpportunityMaster OMInner
																WHERE
																	numDomainId=',@numDomainID,'
																	AND tintOppType=1
																	AND tintOppStatus=1
																	AND ISNULL(OMInner.tintshipped,0) = 0
																	AND dtReleaseDate IS NOT NULL
																	AND dtReleaseDate < DateAdd(minute,',@ClientTimeZoneOffset * -1,',GETUTCDATE())
																	AND (SELECT 
																			COUNT(*) 
																		FROM 
																		(
																			SELECT
																				OIInner.numoppitemtCode,
																				ISNULL(OIInner.numUnitHour,0) AS OrderedQty,
																				ISNULL(TempFulFilled.FulFilledQty,0) AS FulFilledQty
																			FROM
																				OpportunityItems OIInner
																			INNER JOIN
																				Item IInner
																			ON
																				OIInner.numItemCode = IInner.numItemCode
																			OUTER APPLY
																			(
																				SELECT
																					SUM(OpportunityBizDocItems.numUnitHour) AS FulFilledQty
																				FROM
																					OpportunityBizDocs
																				INNER JOIN
																					OpportunityBizDocItems 
																				ON
																					OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																				WHERE
																					OpportunityBizDocs.numOppId = OMInner.numOppId
																					AND ISNULL(OpportunityBizDocs.numBizDocId,0) = 296
																					AND ISNULL(OpportunityBizDocs.bitFulFilled,0) = 1
																					AND OpportunityBizDocItems.numOppItemID = OIInner.numoppitemtCode
																			) AS TempFulFilled
																			WHERE
																				OIInner.numOppID = OMInner.numOppID
																				AND UPPER(IInner.charItemType) = ''P''
																				AND ISNULL(OIInner.bitDropShip,0) = 0
																		) X
																		WHERE
																			X.OrderedQty <> X.FulFilledQty) > 0',') ')
	END



	DECLARE  @firstRec  AS INTEGER
	DECLARE  @lastRec  AS INTEGER
  
	SET @firstRec = (@CurrentPage - 1) * @PageSize
	SET @lastRec = (@CurrentPage * @PageSize + 1)



	-- EXECUTE FINAL QUERY
	DECLARE @strFinal AS NVARCHAR(MAX)
	IF @CurrentPage = -1 AND @PageSize = -1
	BEGIN
		SET @strFinal = CONCAT('SELECT COUNT(*) OVER() AS TotalRecords, ',@strColumns,@strSql,' ORDER BY ',@columnName,' ',@columnSortOrder,' OFFSET ',0,' ROWS FETCH NEXT ',25,' ROWS ONLY;')
	END
	ELSE
	BEGIN
		SET @strFinal = CONCAT('SELECT COUNT(*) OVER() AS TotalRecords, ',@strColumns,@strSql,' ORDER BY ',@columnName,' ',@columnSortOrder,' OFFSET ',(@CurrentPage - 1) * @PageSize,' ROWS FETCH NEXT ',@PageSize,' ROWS ONLY;')
	END
	PRINT CAST(@strFinal AS NTEXT)
	EXEC sp_executesql @strFinal

	SELECT * FROM #tempForm

	DROP TABLE #tempForm

	DROP TABLE #tempColorScheme
END
GO
--created by anoop jayaraj                                          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getdomaindetails')
DROP PROCEDURE usp_getdomaindetails
GO
CREATE PROCEDURE [dbo].[USP_GetDomainDetails]                                          
@numDomainID as numeric(9)=0                                          
as           

DECLARE @listIds VARCHAR(MAX)
SELECT @listIds = COALESCE(@listIds+',' ,'') + CAST(numUserID AS VARCHAR(100)) FROM UnitPriceApprover WHERE numDomainID = @numDomainID
   
DECLARE @canChangeDiferredIncomeSelection AS BIT = 1

IF EXISTS(SELECT 
			OB.numOppBizDocsId 
		FROM 
			OpportunityBizDocs OB 
		INNER JOIN 
			OpportunityMaster OM 
		ON 
			OB.numOppId=OM.numOppId 
		WHERE 
			numBizDocId=304 
			AND OM.numDomainId=@numDomainID)
BEGIN
	SET @canChangeDiferredIncomeSelection = 0
END   
   
                               
select                                           
vcDomainName,                                           
isnull(vcDomainDesc,'') vcDomainDesc,                                           
isnull(bitExchangeIntegration,0) bitExchangeIntegration,                                           
isnull(bitAccessExchange,0) bitAccessExchange,                                           
isnull(vcExchUserName,'') vcExchUserName,                                           
isnull(vcExchPassword,'') vcExchPassword,                                           
isnull(vcExchPath,'') vcExchPath,                                           
isnull(vcExchDomain,'') vcExchDomain,                                         
tintCustomPagingRows,                                         
vcDateFormat,                                         
numDefCountry,                                         
tintComposeWindow,                                         
sintStartDate,                                         
sintNoofDaysInterval,                                         
tintAssignToCriteria,                                         
bitIntmedPage,
ISNULL(D.numCost,0) as numCost,                                         
tintFiscalStartMonth,                                    
--isnull(bitDeferredIncome,0) as bitDeferredIncome,                                  
isnull(tintPayPeriod,0) as tintPayPeriod,                                
isnull(numCurrencyID,0) as numCurrencyID,                            
isnull(vcCurrency,'') as vcCurrency,                      
isnull(charUnitSystem,'') as charUnitSystem ,                    
isnull(vcPortalLogo,'') as vcPortalLogo,                  
isnull(tintChrForComSearch,2) as tintChrForComSearch,                
isnull(tintChrForItemSearch,'') as tintChrForItemSearch,            
isnull(intPaymentGateWay,0) as intPaymentGateWay  ,        
 case when bitPSMTPServer= 1 then vcPSMTPServer else '' end as vcPSMTPServer  ,              
 case when bitPSMTPServer= 1 then  isnull(numPSMTPPort,0)  else 0 end as numPSMTPPort          
,isnull(bitPSMTPAuth,0) bitPSMTPAuth        
,isnull([vcPSmtpPassword],'')vcPSmtpPassword        
,isnull(bitPSMTPSSL,0) bitPSMTPSSL     
,isnull(bitPSMTPServer,0) bitPSMTPServer  ,isnull(vcPSMTPUserName,'') vcPSMTPUserName,      
--isnull(numDefaultCategory,0) numDefaultCategory,
isnull(numShipCompany,0)   numShipCompany,
isnull(bitAutoPopulateAddress,0) bitAutoPopulateAddress,
isnull(tintPoulateAddressTo,0) tintPoulateAddressTo,
isnull(bitMultiCompany,0) bitMultiCompany,
isnull(bitMultiCurrency,0) bitMultiCurrency,
ISNULL(bitCreateInvoice,0) bitCreateInvoice,
ISNULL([numDefaultSalesBizDocId],0) numDefaultSalesBizDocId,
ISNULL([bitSaveCreditCardInfo],0) bitSaveCreditCardInfo,
ISNULL([intLastViewedRecord],10) intLastViewedRecord,
ISNULL([tintCommissionType],1) tintCommissionType,
ISNULL([tintBaseTax],2) tintBaseTax,
ISNULL([numDefaultPurchaseBizDocId],0) numDefaultPurchaseBizDocId,
ISNULL(bitEmbeddedCost,0) bitEmbeddedCost,
ISNULL(numDefaultSalesOppBizDocId,0) numDefaultSalesOppBizDocId,
ISNULL(tintSessionTimeOut,1) tintSessionTimeOut,
ISNULL(tintDecimalPoints,2) tintDecimalPoints,
ISNULL(bitCustomizePortal,0) bitCustomizePortal,
ISNULL(tintBillToForPO,0) tintBillToForPO,
ISNULL(tintShipToForPO,0) tintShipToForPO,
ISNULL(tintOrganizationSearchCriteria,1) tintOrganizationSearchCriteria,
ISNULL(tintLogin,0) tintLogin,
lower(ISNULL(vcPortalName,'')) vcPortalName,
ISNULL(bitDocumentRepositary,0) bitDocumentRepositary,
D.numDomainID,
ISNULL(vcPSMTPDisplayName,'') vcPSMTPDisplayName,
ISNULL(bitGtoBContact,0) bitGtoBContact,
ISNULL(bitBtoGContact,0) bitBtoGContact,
ISNULL(bitGtoBCalendar,0) bitGtoBCalendar,
ISNULL(bitBtoGCalendar,0) bitBtoGCalendar,
ISNULL(bitExpenseNonInventoryItem,0) bitExpenseNonInventoryItem,
ISNULL(bitInlineEdit,0) bitInlineEdit,
ISNULL(bitRemoveVendorPOValidation,0) bitRemoveVendorPOValidation,
isnull(D.tintBaseTaxOnArea,0) tintBaseTaxOnArea,
ISNULL(D.bitAmountPastDue,0) bitAmountPastDue,
CONVERT(DECIMAL(18,2),ISNULL(D.monAmountPastDue,0)) monAmountPastDue,
ISNULL(bitSearchOrderCustomerHistory,0) bitSearchOrderCustomerHistory,
isnull(D.bitRentalItem,0) as bitRentalItem,
isnull(D.numRentalItemClass,0) as numRentalItemClass,
isnull(D.numRentalHourlyUOM,0) as numRentalHourlyUOM,
isnull(D.numRentalDailyUOM,0) as numRentalDailyUOM,
isnull(D.tintRentalPriceBasedOn,0) as tintRentalPriceBasedOn,
isnull(D.tintCalendarTimeFormat,24) as tintCalendarTimeFormat,
isnull(D.tintDefaultClassType,0) as tintDefaultClassType,
ISNULL(D.tintPriceBookDiscount,0) tintPriceBookDiscount,
ISNULL(D.bitAutoSerialNoAssign,0) as bitAutoSerialNoAssign,
ISNULL(D.bitIsShowBalance,0) AS [bitIsShowBalance],
ISNULL(numIncomeAccID,0) AS [numIncomeAccID],
ISNULL(numCOGSAccID,0) AS [numCOGSAccID],
ISNULL(numAssetAccID,0) AS [numAssetAccID],
ISNULL(IsEnableProjectTracking,0) AS [IsEnableProjectTracking],
ISNULL(IsEnableCampaignTracking,0) AS [IsEnableCampaignTracking],
ISNULL(IsEnableResourceScheduling,0) AS [IsEnableResourceScheduling],
ISNULL(numShippingServiceItemID,0) AS [numShippingServiceItemID],
ISNULL(D.numSOBizDocStatus,0) AS numSOBizDocStatus,
ISNULL(D.bitAutolinkUnappliedPayment,0) AS bitAutolinkUnappliedPayment,
ISNULL(D.numDiscountServiceItemID,0) AS numDiscountServiceItemID,
ISNULL(D.vcShipToPhoneNo,0) AS vcShipToPhoneNo,
ISNULL(D.vcGAUserEMail,'') AS vcGAUserEMail,
ISNULL(D.vcGAUserPassword,'') AS vcGAUserPassword,
ISNULL(D.vcGAUserProfileId,0) AS vcGAUserProfileId,
ISNULL(D.bitDiscountOnUnitPrice,0) AS bitDiscountOnUnitPrice,
ISNULL(D.intShippingImageWidth,0) AS intShippingImageWidth ,
ISNULL(D.intShippingImageHeight,0) AS intShippingImageHeight,
ISNULL(D.numTotalInsuredValue,0) AS numTotalInsuredValue,
ISNULL(D.numTotalCustomsValue,0) AS numTotalCustomsValue,
ISNULL(D.vcHideTabs,'') AS vcHideTabs,
ISNULL(D.bitUseBizdocAmount,0) AS bitUseBizdocAmount,
ISNULL(D.bitDefaultRateType,0) AS bitDefaultRateType,
ISNULL(D.bitIncludeTaxAndShippingInCommission,0) AS bitIncludeTaxAndShippingInCommission,
/*,ISNULL(D.bitAllowPPVariance,0) AS bitAllowPPVariance*/
ISNULL(D.bitPurchaseTaxCredit,0) AS bitPurchaseTaxCredit,
ISNULL(D.bitLandedCost,0) AS bitLandedCost,
ISNULL(D.vcLanedCostDefault,'') AS vcLanedCostDefault,
ISNULL(D.bitMinUnitPriceRule,0) AS bitMinUnitPriceRule,
ISNULL(AP.numAbovePercent,0) AS numAbovePercent,
ISNULL(AP.numBelowPercent,0) AS numBelowPercent,
ISNULL(AP.numBelowPriceField,0) AS numBelowPriceField,
@listIds AS vcUnitPriceApprover,
ISNULL(D.numDefaultSalesPricing,2) AS numDefaultSalesPricing,
ISNULL(D.bitReOrderPoint,0) AS bitReOrderPoint,
ISNULL(D.numReOrderPointOrderStatus,0) AS numReOrderPointOrderStatus,
ISNULL(D.numPODropShipBizDoc,0) AS numPODropShipBizDoc,
ISNULL(D.numPODropShipBizDocTemplate,0) AS numPODropShipBizDocTemplate,
ISNULL(D.IsEnableDeferredIncome,0) AS IsEnableDeferredIncome,
ISNULL(D.bitApprovalforTImeExpense,0) AS bitApprovalforTImeExpense,
ISNULL(D.intTimeExpApprovalProcess,0) AS intTimeExpApprovalProcess,
ISNULL(D.bitApprovalforOpportunity,0) AS bitApprovalforOpportunity,
ISNULL(D.intOpportunityApprovalProcess,0) AS intOpportunityApprovalProcess,
@canChangeDiferredIncomeSelection AS canChangeDiferredIncomeSelection,
ISNULL(CAST(D.numDefaultSalesShippingDoc AS int),0) AS numDefaultSalesShippingDoc,
ISNULL(CAST(D.numDefaultPurchaseShippingDoc AS int),0) AS numDefaultPurchaseShippingDoc,
ISNULL(D.bitchkOverRideAssignto,0) AS bitchkOverRideAssignto,
ISNULL(D.vcPrinterIPAddress,'') vcPrinterIPAddress,
ISNULL(D.vcPrinterPort,'') vcPrinterPort,
ISNULL(AP.bitCostApproval,0) bitCostApproval
,ISNULL(AP.bitListPriceApproval,0) bitListPriceApproval
,ISNULL(AP.bitMarginPriceViolated,0) bitMarginPriceViolated
,ISNULL(vcSalesOrderTabs,'Sales Orders') AS vcSalesOrderTabs
,ISNULL(vcSalesQuotesTabs,'Sales Quotes') AS vcSalesQuotesTabs
,ISNULL(vcItemPurchaseHistoryTabs,'Item Purchase History') AS vcItemPurchaseHistoryTabs
,ISNULL(vcItemsFrequentlyPurchasedTabs,'Items Frequently Purchased') AS vcItemsFrequentlyPurchasedTabs
,ISNULL(vcOpenCasesTabs,'Open Cases') AS vcOpenCasesTabs
,ISNULL(vcOpenRMATabs,'Open RMAs') AS vcOpenRMATabs
,ISNULL(bitSalesOrderTabs,0) AS bitSalesOrderTabs
,ISNULL(bitSalesQuotesTabs,0) AS bitSalesQuotesTabs
,ISNULL(bitItemPurchaseHistoryTabs,0) AS bitItemPurchaseHistoryTabs
,ISNULL(bitItemsFrequentlyPurchasedTabs,0) AS bitItemsFrequentlyPurchasedTabs
,ISNULL(bitOpenCasesTabs,0) AS bitOpenCasesTabs
,ISNULL(bitOpenRMATabs,0) AS bitOpenRMATabs
,ISNULL(bitSupportTabs,0) AS bitSupportTabs
,ISNULL(vcSupportTabs,'Support') AS vcSupportTabs
,ISNULL(D.numDefaultSiteID,0) AS numDefaultSiteID
,ISNULL(tintOppStautsForAutoPOBackOrder,0) AS tintOppStautsForAutoPOBackOrder
,ISNULL(tintUnitsRecommendationForAutoPOBackOrder,1) AS tintUnitsRecommendationForAutoPOBackOrder
,ISNULL(bitRemoveGlobalLocation,0) AS bitRemoveGlobalLocation
,ISNULL(numAuthorizePercentage,0) AS numAuthorizePercentage
,ISNULL(bitEDI,0) AS bitEDI
,ISNULL(bit3PL,0) AS bit3PL
,ISNULL(numListItemID,0) AS numListItemID
,ISNULL(bitAllowDuplicateLineItems,0) bitAllowDuplicateLineItems
,ISNULL(tintCommitAllocation,1) tintCommitAllocation
,ISNULL(tintInvoicing,1) tintInvoicing
,ISNULL(tintMarkupDiscountOption,1) tintMarkupDiscountOption
,ISNULL(tintMarkupDiscountValue,1) tintMarkupDiscountValue
,ISNULL(bitIncludeRequisitions,0) bitIncludeRequisitions
,ISNULL(bitCommissionBasedOn,0) bitCommissionBasedOn
,ISNULL(tintCommissionBasedOn,0) tintCommissionBasedOn
,ISNULL(bitDoNotShowDropshipPOWindow,0) bitDoNotShowDropshipPOWindow
,ISNULL(tintReceivePaymentTo,2) tintReceivePaymentTo
,ISNULL(numReceivePaymentBankAccount,0) numReceivePaymentBankAccount
,ISNULL(numOverheadServiceItemID,0) numOverheadServiceItemID,
ISNULL(D.bitDisplayCustomField,0) AS bitDisplayCustomField,
ISNULL(D.bitFollowupAnytime,0) AS bitFollowupAnytime,
ISNULL(D.bitpartycalendarTitle,0) AS bitpartycalendarTitle,
ISNULL(D.bitpartycalendarLocation,0) AS bitpartycalendarLocation,
ISNULL(D.bitpartycalendarDescription,0) AS bitpartycalendarDescription,
ISNULL(D.bitREQPOApproval,0) AS bitREQPOApproval,
ISNULL(D.bitARInvoiceDue,0) AS bitARInvoiceDue,
ISNULL(D.bitAPBillsDue,0) AS bitAPBillsDue,
ISNULL(D.bitItemsToPickPackShip,0) AS bitItemsToPickPackShip,
ISNULL(D.bitItemsToInvoice,0) AS bitItemsToInvoice,
ISNULL(D.bitSalesOrderToClose,0) AS bitSalesOrderToClose,
ISNULL(D.bitItemsToPutAway,0) AS bitItemsToPutAway,
ISNULL(D.bitItemsToBill,0) AS bitItemsToBill,
ISNULL(D.bitPosToClose,0) AS bitPosToClose,
ISNULL(D.bitPOToClose,0) AS bitPOToClose,
ISNULL(D.bitBOMSToPick,0) AS bitBOMSToPick,

ISNULL(D.vchREQPOApprovalEmp,'') AS vchREQPOApprovalEmp,
ISNULL(D.vchARInvoiceDue,'') AS vchARInvoiceDue,
ISNULL(D.vchAPBillsDue,'') AS vchAPBillsDue,
ISNULL(D.vchItemsToPickPackShip,'') AS vchItemsToPickPackShip,
ISNULL(D.vchItemsToInvoice,'') AS vchItemsToInvoice,
ISNULL(D.vchPriceMarginApproval,'') AS vchPriceMarginApproval,
ISNULL(D.vchSalesOrderToClose,'') AS vchSalesOrderToClose,
ISNULL(D.vchItemsToPutAway,'') AS vchItemsToPutAway,
ISNULL(D.vchItemsToBill,'') AS vchItemsToBill,
ISNULL(D.vchPosToClose,'') AS vchPosToClose,
ISNULL(D.vchPOToClose,'') AS vchPOToClose,
ISNULL(D.vchBOMSToPick,'') AS vchBOMSToPick,
ISNULL(D.decReqPOMinValue,0) AS decReqPOMinValue,
ISNULL(D.bitUseOnlyActionItems,0) AS bitUseOnlyActionItems,
ISNULL(D.vcElectiveItemFields,'') AS vcElectiveItemFields,
ISNULL(tintMailProvider,3) tintMailProvider,
ISNULL(D.bitDisplayContractElement,0) AS bitDisplayContractElement,
ISNULL(D.vcEmployeeForContractTimeElement,'') AS vcEmployeeForContractTimeElement,
ISNULL(numARContactPosition,0) numARContactPosition,
ISNULL(bitShowCardConnectLink,0) bitShowCardConnectLink,
ISNULL(vcBluePayFormName,'') vcBluePayFormName,
ISNULL(vcBluePaySuccessURL,'') vcBluePaySuccessURL,
ISNULL(vcBluePayDeclineURL,'') vcBluePayDeclineURL,
ISNULL(bitUseDeluxeCheckStock,0) bitUseDeluxeCheckStock,
ISNULL(D.bitEnableSmartyStreets,0) bitEnableSmartyStreets,
ISNULL(D.vcSmartyStreetsAPIKeys,'') vcSmartyStreetsAPIKeys,
ISNULL(bitReceiveOrderWithNonMappedItem,0) bitReceiveOrderWithNonMappedItem,
ISNULL(numItemToUseForNonMappedItem,0) numItemToUseForNonMappedItem,
ISNULL(bitUsePredefinedCustomer,0) bitUsePredefinedCustomer,
ISNULL(bitUsePreviousEmailBizDoc,0) bitUsePreviousEmailBizDoc,
CASE WHEN ISNULL((SELECT COUNT(*) FROM [ImapUserDetails] WHERE 
			numUserCntId = -1 
			AND numDomainID = @numDomainID),0) > 0 THEN 1 ELSE 0 END AS bitImapConfigured ,
ISNULL(bitInventoryInvoicing,0) AS bitInventoryInvoicing
from Domain D  
LEFT JOIN eCommerceDTL eComm  on eComm.numDomainID=D.numDomainID  
LEFT JOIN ApprovalProcessItemsClassification AP ON AP.numDomainID = D.numDomainID
where (D.numDomainID=@numDomainID OR @numDomainID=-1)


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
         
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetEstimateShipping')
DROP PROCEDURE USP_GetEstimateShipping
GO
CREATE PROCEDURE [dbo].[USP_GetEstimateShipping]
	@numDomainID NUMERIC(18,0)
	,@numShipVia INT
	,@numShipService INT
AS 
BEGIN
	SELECT 
		numServiceTypeID
		,0 AS numRuleID
		,numShippingCompanyID
		,vcServiceName
		,monRate
		,intNsoftEnum
		,intFrom
		,intTo
		,fltMarkup
		,bitMarkUpType
		,bitEnabled
		,1 AS tintFixedShippingQuotesMode
		,L.vcData AS vcShippingCompanyName
		,S.vcItemClassification
		,SR.bitItemClassification
		,SR.numWareHouseID
		,SR.numSiteID
		,SR.numRuleID AS numItemClassificationRuleID
	FROM 
		ShippingServiceTypes S
	LEFT JOIN 
		ListDetails AS L
	ON
		S.numShippingCompanyID=L.numListItemID
	LEFT JOIN ShippingRules AS SR ON S.numRuleID=SR.numRuleID
	WHERE 
		S.numDomainID=@numDomainID 
		AND 1 = (CASE 
					WHEN ISNULL(@numShipVia,0) > 0 AND ISNULL(@numShipService,0) > 0 
					THEN (CASE WHEN ISNULL(S.numShippingCompanyID,0)=@numShipVia AND ISNULL(S.intNsoftEnum,0) = @numShipService THEN 1 ELSE 0 END)
					ELSE (CASE WHEN ISNULL(bitEnabled,0)=1 THEN 1 ELSE 0 END) 
				END)
END      




SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
         
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetEstimateShippingEcommerce')
DROP PROCEDURE USP_GetEstimateShippingEcommerce
GO
CREATE PROCEDURE [dbo].[USP_GetEstimateShippingEcommerce]
	@numDomainID NUMERIC(18,0)
AS 
BEGIN
	IF @numDomainID IN (187,214)
	BEGIN
		SELECT
			*
		FROM
		(
			SELECT 
				intNsoftEnum AS numServiceTypeID
				,0 AS numRuleID
				,numShippingCompanyID
				,vcServiceName
				,monRate
				,intNsoftEnum
				,intFrom
				,intTo
				,fltMarkup
				,bitMarkUpType
				,bitEnabled
				,S.vcItemClassification
		,SR.bitItemClassification
		,SR.numWareHouseID
		,SR.numSiteID
		,SR.numRuleID AS numItemClassificationRuleID
			FROM 
				ShippingServiceTypes  AS S
				LEFT JOIN 
		ListDetails AS L
	ON
		S.numShippingCompanyID=L.numListItemID
	LEFT JOIN ShippingRules AS SR ON S.numRuleID=SR.numRuleID
			WHERE 
				S.numDomainID=@numDomainID 
				AND ISNULL(bitEnabled,0)=1
			UNION
			SELECT 
				-1 numServiceTypeID
				,0 AS numRuleID
				,92 numShippingCompanyID
				,'Will call' vcServiceName
				,0 monRate
				,0 intNsoftEnum
				,0 intFrom
				,0 intTo
				,0 fltMarkup
				,0 bitMarkUpType
				,1 bitEnabled
				,'' AS vcItemClassification
		,0 bitItemClassification
		,0 numWareHouseID
		,0 numSiteID
		,0 numItemClassificationRuleID
		) TEMP
		ORDER BY
			numShippingCompanyID,intNsoftEnum
	END
	ELSE
	BEGIN
		SELECT 
			numServiceTypeID
			,0 AS numRuleID
			,numShippingCompanyID
			,vcServiceName
			,monRate
			,intNsoftEnum
			,intFrom
			,intTo
			,fltMarkup
			,bitMarkUpType
			,bitEnabled
			,S.vcItemClassification
		,SR.bitItemClassification
		,SR.numWareHouseID
		,SR.numSiteID
		,SR.numRuleID AS numItemClassificationRuleID
		FROM 
			ShippingServiceTypes  AS S
			LEFT JOIN 
		ListDetails AS L
	ON
		S.numShippingCompanyID=L.numListItemID
	LEFT JOIN ShippingRules AS SR ON S.numRuleID=SR.numRuleID
		WHERE 
			S.numDomainID=@numDomainID 
			AND ISNULL(bitEnabled,0)=1
	END
END      
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetGrossProfitEstimate')
DROP PROCEDURE USP_GetGrossProfitEstimate
GO
CREATE PROCEDURE [dbo].[USP_GetGrossProfitEstimate] 
( 
@numDomainID as numeric(9)=0,    
@numOppID AS NUMERIC(9)=0
)
AS
BEGIN
	DECLARE @avgCost INT
	DECLARE @numShippingServiceItemID NUMERIC(18,0)
	SELECT 
		@avgCost=numCost
		,@numShippingServiceItemID=ISNULL(numShippingServiceItemID,0)
	FROM 
		Domain 
	WHERE 
		numDOmainId=@numDomainID

	DECLARE @monShippingProfit DECIMAL(20,5)
	DECLARE @monShippingAverageCost DECIMAL(20,5) = 0
	DECLARE @monMarketplaceShippingCost DECIMAL(20,5)

	SELECT @monMarketplaceShippingCost = ISNULL(monMarketplaceShippingCost,0) FROM OpportunityMaster WHERE numDomainID=@numDomainID AND numOppID=@numOppID 

	IF ISNULL(@monMarketplaceShippingCost,0) > 0
	BEGIN
		IF EXISTS (SELECT * FROM OpportunityItems WHERE numOppID=@numOppID AND numItemCode=@numShippingServiceItemID)
		BEGIN
			SET @monShippingAverageCost = @monMarketplaceShippingCost / ISNULL((SELECT COUNT(*) FROM OpportunityItems WHERE numOppID=@numOppID AND numItemCode=@numShippingServiceItemID),0)
			SET @monShippingProfit = ISNULL((SELECT SUM(monTotAmount) FROM OpportunityItems WHERE numOppID=@numOppID AND numItemCode=@numShippingServiceItemID),0) - @monMarketplaceShippingCost
		END
		ELSE
		BEGIN
			SET @monShippingProfit = 0
		END
	END
	ELSE
	BEGIN
		SET @monShippingProfit = 0
	END

	SELECT 
		numOppId
		,@monShippingProfit AS monShippingProfit
		,vcPOppName
		,vcItemName
		,monAverageCost
		,vcVendor
		,(ISNULL(monTotAmount,0) * fltExchangeRate)/ISNULL(NULLIF(numUnitHour,0),1) AS monPrice
		,VendorCost
		,numUnitHour
		,(ISNULL(monTotAmount,0) * fltExchangeRate) - (CASE WHEN numItemCode=@numShippingServiceItemID THEN (ISNULL(numUnitHour,0) *  @monShippingAverageCost) ELSE (ISNULL(numUnitHour,0) * (CASE @avgCost WHEN 3 THEN VendorCost * dbo.fn_UOMConversion(numBaseUnit,numItemCode,@numDomainID,numPurchaseUnit) WHEN 2 THEN numCost * dbo.fn_UOMConversion(numBaseUnit,numItemCode,@numDomainID,numUOMId) ELSE monAverageCost END)) END) AS Profit
		,((((ISNULL(monTotAmount,0) * fltExchangeRate)/ISNULL(NULLIF(numUnitHour,0),1)) - (CASE 
																							WHEN numItemCode=@numShippingServiceItemID 
																							THEN @monShippingAverageCost
																							ELSE
																								(CASE 
																									WHEN numPOItemID IS NOT NULL 
																									THEN ISNULL(monPOCost,0)
																									ELSE
																										(CASE @avgCost 
																											WHEN 3 THEN VendorCost * dbo.fn_UOMConversion(numBaseUnit,numItemCode,@numDomainID,numPurchaseUnit) 																									
																											ELSE monAverageCost 
																										END) 
																								END)
																							END)) / (CASE WHEN ((ISNULL(monTotAmount,0) * fltExchangeRate)/ISNULL(NULLIF(numUnitHour,0),1))=0 then 1 ELSE ((ISNULL(monTotAmount,0) * fltExchangeRate)/ISNULL(NULLIF(numUnitHour,0),1)) end)) * 100 ProfitPer
		,bitItemPriceApprovalRequired
		,(CASE WHEN numPOID IS NOT NULL THEN CONCAT('<a href="javascript:OpenOpp(',numPOID,')">',vcPOName,'</a>') ELSE '' END) vcPurchaseOrder
	FROM
	(
		SELECT 
			opp.numOppId
			,Opp.vcPOppName
			,OppI.vcItemName
			,(CASE 
				WHEN ISNULL(I.bitVirtualInventory,0) = 1 
				THEN 0 
				ELSE (CASE 
						WHEN ISNULL(I.bitKitParent,0) = 1 
						THEN ISNULL((SELECT SUM(ISNULL(OKI.numQtyItemsReq_Orig,0) * ISNULL(OKI.monAvgCost,0)) FROM OpportunityKitItems OKI INNER JOIN Item IInner ON OKI.numChildItemID=IInner.numItemCode WHERE OKI.numOppId=Opp.numOppId AND OKI.numOppItemID=OppI.numoppitemtCode),0) + 
							 ISNULL((SELECT SUM(ISNULL(OKCI.numQtyItemsReq_Orig,0) * ISNULL(OKI.numQtyItemsReq_Orig,0) * ISNULL(OKCI.monAvgCost,0)) FROM OpportunityKitChildItems OKCI INNER JOIN OpportunityKitItems OKI ON OKCI.numOppChildItemID=OKI.numOppChildItemID INNER JOIN Item IInner ON OKCI.numItemID=IInner.numItemCode WHERE OKI.numOppId=Opp.numOppId AND OKI.numOppItemID=OppI.numoppitemtCode),0)
						ELSE ISNULL(monAverageCost,0) 
					END)
			END) monAverageCost,
			V.numVendorID AS numVendorID
			,dbo.fn_getcomapnyname(V.numVendorID) as vcVendor
			,oppI.numUnitHour
			,ISNULL(OPPI.monPrice,0) AS monPrice
			,ISNULL(OPPI.monTotAmount,0) AS monTotAmount
			,ISNULL(V.monCost,0) VendorCost
			,ISNULL(OppI.bitItemPriceApprovalRequired,0) AS bitItemPriceApprovalRequired
			,ISNULL(OppI.numCost,0) numCost
			,I.numItemCode
			,numBaseUnit
			,numPurchaseUnit
			,ISNULL(OppI.numUOMId,numBaseUnit) AS numUOMId
			,ISNULL(Opp.fltExchangeRate,1) fltExchangeRate
			,ISNULL(TEMPMatchedPO.numOppId,TEMPPO.numOppId) AS numPOID
			,ISNULL(TEMPMatchedPO.vcPOppName,TEMPPO.vcPOppName) vcPOName
			,ISNULL(TEMPMatchedPO.numoppitemtCode,TEMPPO.numoppitemtCode) AS numPOItemID
			,ISNULL(TEMPMatchedPO.monPrice,TEMPPO.monPrice) AS monPOCost
		FROM 
			OpportunityMaster Opp 
		INNER JOIN 
			OpportunityItems OppI 
		ON 
			opp.numOppId=OppI.numOppId 
		OUTER APPLY
		(
			SELECT TOP 1
				OM.numOppID
				,OM.vcPOppName
				,OI.numoppitemtCode
				,OI.monPrice
			FROM
				SalesOrderLineItemsPOLinking SOLIPL
			INNER JOIN
				OpportunityMaster OM
			ON
				SOLIPL.numPurchaseOrderID = OM.numOppId
			INNER JOIN
				OpportunityItems OI
			ON
				OM.numOppId = OI.numOppId
				AND SOLIPL.numPurchaseOrderItemID = OI.numoppitemtCode
			WHERE
				SOLIPL.numSalesOrderID = Opp.numOppId
				AND SOLIPL.numSalesOrderItemID = OppI.numoppitemtCode
		) TEMPMatchedPO
		OUTER APPLY
		(
			SELECT TOP 1
				OM.numOppID
				,OM.vcPOppName
				,OI.numoppitemtCode
				,OI.monPrice
			FROM
				OpportunityLinking OL
			INNER JOIN
				OpportunityMaster OM
			ON
				OL.numChildOppID = OM.numOppId
			INNER JOIN
				OpportunityItems OI
			ON
				OM.numOppId = OI.numOppId
			WHERE
				OL.numParentOppID = Opp.numOppId
				AND OI.numItemCode = OppI.numItemCode
				AND OI.vcAttrValues = OppI.vcAttrValues
		) TEMPPO
		INNER JOIN 
			Item I 
		ON 
			OppI.numItemCode=i.numItemcode
		Left JOIN 
			Vendor V 
		ON 
			V.numVendorID=I.numVendorID 
			AND V.numItemCode=I.numItemCode
		WHERE 
			Opp.numDomainId=@numDomainID 
			AND opp.numOppId=@numOppID
			AND ISNULL(I.bitContainer,0) = 0
	) temp
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetMultiSelectItems')
DROP PROCEDURE USP_GetMultiSelectItems
GO
CREATE PROCEDURE [dbo].[USP_GetMultiSelectItems]    
@numDomainID NUMERIC(18,0),
	@numItemCodes VARCHAR(8000) = '',
	@tintOppType TINYINT
AS    
BEGIN
 
	SELECT 
		I.numItemCode
		,I.vcItemName AS ItemName
		,I.charItemType
		,dbo.fn_GetItemAttributes(@numDomainID,I.numItemCode,0) AS Attr
		,(SELECT SUM(numOnHand)  FROM WareHouseItems WHERE numItemID = I.numItemCode)AS numOnHand
		,(SELECT SUM(numAllocation) FROM WareHouseItems WHERE numItemID = I.numItemCode)AS numAllocation
		,ISNULL(I.numSaleUnit,0) AS numSaleUnit
		,dbo.fn_UOMConversion((CASE WHEN  @tintOppType = 1 THEN ISNULL(I.numSaleUnit,0) ELSE ISNULL(I.numPurchaseUnit,0) END),I.numItemCode,I.numDomainID,ISNULL(I.numBaseUnit,0)) fltUOMConversionFactor
FROM 
	Item I
	WHERE 
		I.numItemCode IN (SELECT  Id FROM dbo.SplitIDs(@numItemCodes, ','))
END
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetOppItemsForPickPackShip' ) 
    DROP PROCEDURE USP_GetOppItemsForPickPackShip
GO
CREATE PROCEDURE USP_GetOppItemsForPickPackShip
    @numDomainID AS NUMERIC(18,0),
    @numOppID AS NUMERIC(18,0),
	@numBizDocID AS NUMERIC(18,0)
AS 
BEGIN
	IF @numBizDocID = 29397 -- Packing Slip
	BEGIN
		SELECT    
			OI.numoppitemtCode,
			OI.numUnitHour AS QtyOrdered,
			OI.numUnitHour - ISNULL(SUM(OBIPackingSlip.numUnitHour),0) AS QtytoFulFillOriginal,
			(OI.numUnitHour - (CASE WHEN ISNULL(OBDFulfillment.numUnitHour,0) > 0 THEN ISNULL(OBDFulfillment.numUnitHour,0) ELSE ISNULL(SUM(OBIPackingSlip.numUnitHour),0) END)) AS QtytoFulFill,
			CONCAT('<ul class="list-unstyled">',STUFF((SELECT 
														CONCAT('<li><a onclick="return OpenBizInvoice(''',OB.numOppId,''',''',OB.numOppBizDocsId,''',0)" href="#">',OB.vcBizDocID,' (',OBI.numUnitHour,')</a></li>') 
													FROM 
														OpportunityBizDocs OB
													INNER JOIN
														OpportunityBizDocItems OBI
													ON
														OB.numOppBizDocsId=OBI.numOppBizDocID
													WHERE 
														OB.numOppId = @numOppID
														AND OB.numBizDocId = @numBizDocID
														AND OBI.numOppItemID=OI.numoppitemtCode
														AND ISNULL(OBI.numUnitHour,0) <> 0
													ORDER BY
														OB.numOppBizDocsId
													FOR XML PATH(''), TYPE).value('.', 'varchar(max)'),1,0,''),'</ul>') vcPickLists
		FROM 
			OpportunityMaster OM
		INNER JOIN
			OpportunityItems OI
		ON
			OM.numOppId = OI.numOppId
		LEFT JOIN
			OpportunityBizDocs OBDPackingSlip
		ON
			OM.numOppId = OBDPackingSlip.numOppId
			AND OBDPackingSlip.numBizDocId = @numBizDocID
		LEFT JOIN
			OpportunityBizDocItems OBIPackingSlip
		ON
			OBDPackingSlip.numOppBizDocsId = OBIPackingSlip.numOppBizDocID
			AND OBIPackingSlip.numOppItemID = OI.numoppitemtCode
		OUTER APPLY
		(
			SELECT
				SUM(numUnitHour) AS numUnitHour
			FROM
				OpportunityBizDocs
			LEFT JOIN
				OpportunityBizDocItems 
			ON
				OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
				AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
			WHERE
				OpportunityBizDocs.numOppId = OM.numOppId
				AND OpportunityBizDocs.numBizDocId = 296
		) OBDFulfillment
		WHERE     
			OI.numOppId = @numOppID
		GROUP BY
			OI.numoppitemtCode
			,OI.numUnitHour
			,OBDFulfillment.numUnitHour
	END
	ELSE IF @numBizDocID = 296 -- Fulfillment
	BEGIN
		DECLARE @tintCommitAllocation AS TINYINT
		DECLARE @bitAllocateInventoryOnPickList AS BIT

		SELECT @tintCommitAllocation=ISNULL(tintCommitAllocation,1) FROM Domain WHERE numDomainId=@numDomainID
		SELECT @bitAllocateInventoryOnPickList=ISNULL(bitAllocateInventoryOnPickList,0) FROM OpportunityMaster INNER JOIN DivisionMaster ON OpportunityMaster.numDivisionId=DivisionMaster.numDivisionID WHERE numOppId=@numOppID

		SELECT    
			OI.numoppitemtCode,
			OI.numUnitHour AS QtyOrdered,
			ISNULL(CASE 
				WHEN @numBizDocId = 296 
				THEN
					(CASE 
						WHEN (I.charItemType = 'p' AND ISNULL(OI.bitDropShip, 0) = 0 AND ISNULL(bitAsset,0)=0)
						THEN 
							(CASE 
								WHEN (OI.numUnitHour - ISNULL(SUM(OBI.numUnitHour),0)) <= dbo.GetOrderItemAvailableQtyToShip(OI.numoppitemtCode,OI.numWarehouseItmsID,ISNULL(I.bitKitParent,0),@tintCommitAllocation,@bitAllocateInventoryOnPickList) 
								THEN (OI.numUnitHour - ISNULL(SUM(OBI.numUnitHour),0)) 
								ELSE dbo.GetOrderItemAvailableQtyToShip(OI.numoppitemtCode,OI.numWarehouseItmsID,ISNULL(I.bitKitParent,0),@tintCommitAllocation,@bitAllocateInventoryOnPickList) 
							END)
						ELSE (OI.numUnitHour - ISNULL(SUM(OBI.numUnitHour),0))
					END)
				ELSE
					(OI.numUnitHour - ISNULL(SUM(OBI.numUnitHour),0))
			END,0) AS QtytoFulFillOriginal,
			(OI.numUnitHour - ISNULL(SUM(OBI.numUnitHour),0)) QtyToFulfillWithoutAllocationCheck,
			ISNULL((CASE 
				WHEN ISNULL(SUM(OBI.numUnitHour),0) >= ISNULL(OBDPackingSlip.numUnitHour,0) 
				THEN (CASE 
						WHEN (I.charItemType = 'p' AND ISNULL(OI.bitDropShip, 0) = 0 AND ISNULL(bitAsset,0)=0)
						THEN 
							(CASE 
								WHEN (OI.numUnitHour - ISNULL(SUM(OBI.numUnitHour),0)) <= dbo.GetOrderItemAvailableQtyToShip(OI.numoppitemtCode,OI.numWarehouseItmsID,ISNULL(I.bitKitParent,0),@tintCommitAllocation,@bitAllocateInventoryOnPickList) 
								THEN (OI.numUnitHour - ISNULL(SUM(OBI.numUnitHour),0)) 
								ELSE dbo.GetOrderItemAvailableQtyToShip(OI.numoppitemtCode,OI.numWarehouseItmsID,ISNULL(I.bitKitParent,0),@tintCommitAllocation,@bitAllocateInventoryOnPickList)
							END)
						ELSE (OI.numUnitHour - ISNULL(SUM(OBI.numUnitHour),0))
					END) 
				ELSE (CASE 
						WHEN (I.charItemType = 'p' AND ISNULL(OI.bitDropShip, 0) = 0 AND ISNULL(bitAsset,0)=0)
						THEN 
							(CASE 
								WHEN (ISNULL(OBDPackingSlip.numUnitHour,0) - ISNULL(SUM(OBI.numUnitHour),0)) <= dbo.GetOrderItemAvailableQtyToShip(OI.numoppitemtCode,OI.numWarehouseItmsID,ISNULL(I.bitKitParent,0),@tintCommitAllocation,@bitAllocateInventoryOnPickList)
								THEN (ISNULL(OBDPackingSlip.numUnitHour,0) - ISNULL(SUM(OBI.numUnitHour),0)) 
								ELSE dbo.GetOrderItemAvailableQtyToShip(OI.numoppitemtCode,OI.numWarehouseItmsID,ISNULL(I.bitKitParent,0),@tintCommitAllocation,@bitAllocateInventoryOnPickList)
							END)
						ELSE ISNULL(OBDPackingSlip.numUnitHour,0) - ISNULL(SUM(OBI.numUnitHour),0)
					END) 
			END),0) AS QtytoFulFill,
			CONCAT('<ul class="list-unstyled">',STUFF((SELECT 
														CONCAT('<li><a onclick="return OpenBizInvoice(''',OB.numOppId,''',''',OB.numOppBizDocsId,''',0)" href="#">',OB.vcBizDocID,' (',OBI.numUnitHour,')</a></li>') 
													FROM 
														OpportunityBizDocs OB
													INNER JOIN
														OpportunityBizDocItems OBI
													ON
														OB.numOppBizDocsId=OBI.numOppBizDocID
													WHERE 
														OB.numOppId = @numOppID
														AND OB.numBizDocId = @numBizDocID
														AND OBI.numOppItemID=OI.numoppitemtCode
														AND ISNULL(OBI.numUnitHour,0) <> 0
													ORDER BY
														OB.numOppBizDocsId
													FOR XML PATH(''), TYPE).value('.', 'varchar(max)'),1,0,''),'</ul>') vcFulfillmentBizDocs
		FROM 
			OpportunityMaster OM
		INNER JOIN
			OpportunityItems OI
		ON
			OM.numOppId = OI.numOppId
		INNER JOIN
			Item I
		ON
			OI.numItemCode = I.numItemCode
		LEFT JOIN
			OpportunityBizDocs OBD
		ON
			OM.numOppId = OBD.numOppId
			AND OBD.numBizDocId = @numBizDocID
		LEFT JOIN
			OpportunityBizDocItems OBI
		ON
			OBD.numOppBizDocsId = OBI.numOppBizDocID
			AND OBI.numOppItemID = OI.numoppitemtCode
		OUTER APPLY
		(
			SELECT
				SUM(numUnitHour) AS numUnitHour
			FROM
				OpportunityBizDocs
			LEFT JOIN
				OpportunityBizDocItems 
			ON
				OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
				AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
			WHERE
				OpportunityBizDocs.numOppId = OM.numOppId
				AND OpportunityBizDocs.numBizDocId = 29397
		) OBDPackingSlip
		WHERE     
			OI.numOppId = @numOppID
		GROUP BY
			OI.numoppitemtCode
			,OI.numUnitHour
			,OI.numWarehouseItmsID
			,OI.bitDropShip
			,I.charItemType
			,I.bitAsset
			,I.bitKitParent
			,OBDPackingSlip.numUnitHour
	END
	ELSE IF @numBizDocID = 287 -- Invoice
	BEGIN
		SELECT    
			OI.numoppitemtCode,
			OI.numUnitHour AS QtyOrdered,
			OI.numUnitHour - ISNULL(SUM(OBI.numUnitHour),0) AS QtytoFulFillOriginal,
			ISNULL((CASE 
				WHEN ISNULL(SUM(OBI.numUnitHour),0) >= ISNULL(OBDFulfillment.numUnitHour,0) 
				THEN OI.numUnitHour - ISNULL(SUM(OBI.numUnitHour),0) 
				ELSE ISNULL(OBDFulfillment.numUnitHour,0) - ISNULL(SUM(OBI.numUnitHour),0) 
			END),0) AS QtytoFulFill,
			CONCAT('<ul class="list-unstyled">',STUFF((SELECT 
														CONCAT('<li><a onclick="return OpenBizInvoice(''',OB.numOppId,''',''',OB.numOppBizDocsId,''',0)" href="#">',OB.vcBizDocID,' (',OBI.numUnitHour,')</a></li>') 
													FROM 
														OpportunityBizDocs OB
													INNER JOIN
														OpportunityBizDocItems OBI
													ON
														OB.numOppBizDocsId=OBI.numOppBizDocID
													WHERE 
														OB.numOppId = @numOppID
														AND OB.numBizDocId = @numBizDocID
														AND OBI.numOppItemID=OI.numoppitemtCode
														AND ISNULL(OBI.numUnitHour,0) <> 0
													ORDER BY
														OB.numOppBizDocsId
													FOR XML PATH(''), TYPE).value('.', 'varchar(max)'),1,0,''),'</ul>') vcInvoices
		FROM 
			OpportunityMaster OM
		INNER JOIN
			OpportunityItems OI
		ON
			OM.numOppId = OI.numOppId
		LEFT JOIN
			OpportunityBizDocs OBD
		ON
			OM.numOppId = OBD.numOppId
			AND OBD.numBizDocId = @numBizDocID
		LEFT JOIN
			OpportunityBizDocItems OBI
		ON
			OBD.numOppBizDocsId = OBI.numOppBizDocID
			AND OBI.numOppItemID = OI.numoppitemtCode
		OUTER APPLY
		(
			SELECT
				SUM(numUnitHour) AS numUnitHour
			FROM
				OpportunityBizDocs
			LEFT JOIN
				OpportunityBizDocItems 
			ON
				OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
				AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
			WHERE
				OpportunityBizDocs.numOppId = OM.numOppId
				AND OpportunityBizDocs.numBizDocId = 296
		) OBDFulfillment
		WHERE     
			OI.numOppId = @numOppID
		GROUP BY
			OI.numoppitemtCode
			,OI.numUnitHour
			,OBDFulfillment.numUnitHour
	END
END
GO
/****** Object:  StoredProcedure [dbo].[USP_GetOpportunitySalesTemplate]    Script Date: 09/01/2009 01:16:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- SELECT * FROM  [OpportunitySalesTemplate]
-- EXEC USP_GetOpportunitySalesTemplate 0,0,1,1,1
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetOpportunitySalesTemplate')
DROP PROCEDURE USP_GetOpportunitySalesTemplate
GO
CREATE PROCEDURE [dbo].[USP_GetOpportunitySalesTemplate]
          @numSalesTemplateID NUMERIC(9)  = 0,
          @numOppId           NUMERIC(9),
          @numDomainID NUMERIC(9),
          @tintMode TINYINT,
          @numDivisionID NUMERIC(9)=0,
          @ClientTimeZoneOffset INT=0,
		  @numSiteID NUMERIC(18,0) = 0
AS
    
IF @tintMode = 0 
BEGIN
	IF @numOppId > 0
  BEGIN
  	SELECT @numDivisionID = numDivisionId
    FROM   OpportunityMaster
    WHERE  numOppId = @numOppId  AND numDomainID= @numDomainID
  END
    PRINT @numDivisionID
    
  IF (@numDivisionID > 0)
    BEGIN
      SELECT numSalesTemplateID,
             vcTemplateName,
             tintType,
             CASE tintType
             WHEN 0 THEN 'Specific'
             WHEN 1 THEN 'Generic'
             END AS TemplateType ,
             numOppId,
             numDivisionID,
             [dbo].[fn_GetComapnyName](numDivisionID) AS OrganizationName,
             numDomainID, tintAppliesTo,
			 dbo.fn_GetContactName(OpportunitySalesTemplate.numCreatedby)+ ' ' + CONVERT(VARCHAR(20),dateadd(MINUTE,-@ClientTimeZoneOffset,OpportunitySalesTemplate.dtCreatedDate)) AS CreatedBy
      FROM   OpportunitySalesTemplate
      WHERE  (numSalesTemplateID = @numSalesTemplateID
               OR @numSalesTemplateID = 0)
             AND numDivisionID = @numDivisionID
             AND numDomainID= @numDomainID
    END	
END  
ELSE IF @tintMode =1 
BEGIN
	IF ISNULL(@numSiteID,0) > 0 AND ISNULL((SELECT COUNT(*) FROM OpportunitySalesTemplate WHERE numDomainID= @numDomainID AND numDivisionID=@numDivisionID AND tintType=0),0) = 0
	BEGIN
		INSERT INTO OpportunitySalesTemplate
		(
			vcTemplateName,tintType,numDivisionID,numDomainID,dtCreatedDate
		)
		VALUES
		(
			CONCAT('List - ',ISNULL((SELECT vcSiteName FROM Sites WHERE numDomainID=@numDomainID AND numSiteID=@numSiteID),'')),0,@numDivisionID,@numDomainID,GETUTCDATE()
		)
	END

    SELECT 
		numSalesTemplateID,
        vcTemplateName,
		CONVERT(VARCHAR(20),numSalesTemplateID) + ',' + CONVERT(VARCHAR(20),ISNULL(numOppId,0)  ) AS numSalesTemplateID1,
        (SELECT COUNT(*) FROM SalesTemplateItems WHERE numSalesTemplateID =OpportunitySalesTemplate.[numSalesTemplateID]) ItemCount, tintAppliesTo, numDivisionID,
		dbo.fn_GetContactName(OpportunitySalesTemplate.numCreatedby)+ ' ' + CONVERT(VARCHAR(20),dateadd(MINUTE,-@ClientTimeZoneOffset,OpportunitySalesTemplate.dtCreatedDate)) AS CreatedBy
    FROM
		OpportunitySalesTemplate
    WHERE  
		numDomainID= @numDomainID
        AND tintType  = 1 -- Generic  
    UNION
    SELECT 
		numSalesTemplateID,
        vcTemplateName,
        CONVERT(VARCHAR(20),numSalesTemplateID) + ',' + CONVERT(VARCHAR(20),ISNULL(numOppId,0)  ) AS numSalesTemplateID1,
        (SELECT COUNT(*) FROM SalesTemplateItems WHERE numSalesTemplateID =OpportunitySalesTemplate.[numSalesTemplateID]) ItemCount, tintAppliesTo, numDivisionID,
		dbo.fn_GetContactName(OpportunitySalesTemplate.numCreatedby)+ ' ' + CONVERT(VARCHAR(20),dateadd(MINUTE,-@ClientTimeZoneOffset,OpportunitySalesTemplate.dtCreatedDate)) AS CreatedBy
    FROM 
		OpportunitySalesTemplate
    WHERE  
		numDomainID= @numDomainID
		AND [numDivisionID] = @numDivisionID
        AND tintType  = 0 -- specific     
END
ELSE IF @tintMode =2 
BEGIN
	   SELECT numSalesTemplateID,
             vcTemplateName,
             tintType,
             CASE tintType
             WHEN 0 THEN 'Specific'
             WHEN 1 THEN 'Generic'
             END AS TemplateType ,
             numOppId,
             numDivisionID,
             [dbo].[fn_GetComapnyName](numDivisionID) AS OrganizationName,
             numDomainID, tintAppliesTo,
			 dbo.fn_GetContactName(OpportunitySalesTemplate.numCreatedby)+ ' ' + CONVERT(VARCHAR(20),dateadd(MINUTE,-@ClientTimeZoneOffset,OpportunitySalesTemplate.dtCreatedDate)) AS CreatedBy
      FROM   OpportunitySalesTemplate
      WHERE  (numSalesTemplateID = @numSalesTemplateID
               OR @numSalesTemplateID = 0)
             AND numDomainID= @numDomainID
END
ELSE IF @tintMode =3 --select all generic template
BEGIN
	   SELECT numSalesTemplateID,
             vcTemplateName,
             tintType,
             CASE tintType
             WHEN 0 THEN 'Specific'
             WHEN 1 THEN 'Generic'
             END AS TemplateType ,
             numOppId,
             numDivisionID,
             [dbo].[fn_GetComapnyName](numDivisionID) AS OrganizationName,
             numDomainID, tintAppliesTo,
			 dbo.fn_GetContactName(OpportunitySalesTemplate.numCreatedby)+ ' ' + CONVERT(VARCHAR(20),dateadd(MINUTE,-@ClientTimeZoneOffset,OpportunitySalesTemplate.dtCreatedDate)) AS CreatedBy
      FROM   OpportunitySalesTemplate
      WHERE  numDomainID= @numDomainID
             AND [tintType] = 1
END

  
  
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetShippingRule' ) 
    DROP PROCEDURE USP_GetShippingRule
GO
CREATE PROCEDURE USP_GetShippingRule
    @numRuleID NUMERIC(9),
    @numDomainID NUMERIC(9),
    @byteMode AS TINYINT,
	@numRelProfileID AS VARCHAR(50)
AS 
BEGIN
    IF @byteMode = 0 
        BEGIN  
            SELECT  numRuleID
                    ,vcRuleName
                    ,vcDescription
                    ,tintBasedOn
                    ,tinShippingMethod
                    ,tintIncludeType
                    ,tintTaxMode
                    ,CASE WHEN tintFixedShippingQuotesMode = 0 THEN 1
                             ELSE tintFixedShippingQuotesMode END 
						AS tintFixedShippingQuotesMode
					,bitFreeShipping
					,FreeShippingOrderAmt
					,numRelationship
					,numProfile
					,numWareHouseID
					,numSiteID
					,bitItemClassification
                    
            FROM    dbo.ShippingRules SR
            WHERE   ( numRuleID = @numRuleID
                      OR @numRuleID = 0
                    )
                    AND numDomainID = @numDomainID
        END
    ELSE IF @byteMode = 1 
    BEGIN 
		DECLARE @numRelationship AS NUMERIC
		DECLARE @numProfile AS NUMERIC
				
		SET @numRelationship=(parsename(replace(@numRelProfileID,'-','.'),2))
		SET @numProfile=(parsename(replace(@numRelProfileID,'-','.'),1))

		SELECT minShippingCost,bitEnableStaticShippingRule,bitEnableShippingExceptions FROM Domain WHERE numDomainID = @numDomainID

		IF @numRelationship > 0 AND @numProfile > 0
		BEGIN
			   SELECT  numRuleID,
                vcRuleName,
                tinShippingMethod,
                CASE WHEN tintTaxMode = 1 THEN 'Do Not Tax'
                        WHEN tintTaxMode = 2 THEN 'Tax when taxable items exists'
                        WHEN tintTaxMode = 3 THEN 'Always Tax'
                        WHEN tintTaxMode = 0 THEN 'Do Not Tax'
                        ELSE 'Tax Mode Not Selected'
                END vcTaxMode,
                CASE WHEN tintFixedShippingQuotesMode= 1 THEN 'Flat rate per item'
                        WHEN tintFixedShippingQuotesMode = 2 THEN 'Ship by weight'
                        WHEN tintFixedShippingQuotesMode = 3 THEN 'Ship by order total'
                        WHEN tintFixedShippingQuotesMode = 4 THEN 'Flat rate per order'
                        WHEN tintFixedShippingQuotesMode= 0 THEN 'Flat rate per item'
                        ELSE 'Fixed rate shipping quotes not selected'
                END  vcFixedShippingQuotesMode
				,(SELECT STUFF((SELECT ', ' + W.vcWareHouse
					FROM   Warehouses W			
					WHERE   W.numWareHouseID  in  ( SELECT  Id
							FROM    dbo.SplitIDs(SR.numWareHouseID, ','))			
					FOR XML PATH ('')),1,2,'')) AS vcShipFrom
                , (SELECT STUFF((SELECT ', ' + dbo.fn_GetState(numStateID) + ' ,' + SS.vcZipPostal
					FROM ShippingRuleStateList SS
					join ShippingRules S on ss.numRuleID = s.numRuleID
					WHERE SR.numRuleID = SS.numRuleID			
					FOR XML PATH ('')),1,2,'')) AS vcShipTo

					,(CASE WHEN ISNULL(SR.bitFreeShipping,0) = 1
						THEN  (SELECT CONCAT ( (SELECT STUFF((SELECT CONCAT (', ' , intfrom , ' - ', intTo, ' pays $',  CAST(monRate AS DECIMAL(20,2)))
								FROM ShippingServiceTypes SS
								join ShippingRules S on ss.numRuleID = s.numRuleID
								WHERE SR.numRuleID = SS.numRuleID			
								FOR XML PATH ('')),1,2,'')) , ' , Free shipping when order amount reaches $' , SR.FreeShippingOrderAmt))
					ELSE (SELECT STUFF((SELECT CONCAT (', ' , intfrom , ' - ', intTo, ' pays $',  CAST(monRate AS DECIMAL(20,2)))
								FROM ShippingServiceTypes SS
								join ShippingRules S on ss.numRuleID = s.numRuleID
								WHERE SR.numRuleID = SS.numRuleID			
								FOR XML PATH ('')),1,2,'')) 
					END ) AS vcShippingCharges
				, (SELECT CONCAT( (SELECT vcData from ListDetails LD 
					JOIN  ShippingRules S ON LD.numListItemID = S.numRelationship 
					WHERE LD.numListItemID = S.numRelationship AND S.numRuleID = SR.numRuleID) , ', ' , (SELECT vcData from ListDetails LD 
					JOIN  ShippingRules S ON LD.numListItemID = S.numProfile 
					WHERE LD.numListItemID = S.numProfile  AND S.numRuleID = SR.numRuleID))) AS vcAppliesTo	
                        
        FROM    dbo.ShippingRules SR
        WHERE  
                numDomainID = @numDomainID AND numRelationship = @numRelationship AND numprofile = @numProfile
		END

		ELSE
		BEGIN
			   SELECT  numRuleID,
                vcRuleName,
                tinShippingMethod,
                CASE WHEN tintTaxMode = 1 THEN 'Do Not Tax'
                        WHEN tintTaxMode = 2 THEN 'Tax when taxable items exists'
                        WHEN tintTaxMode = 3 THEN 'Always Tax'
                        WHEN tintTaxMode = 0 THEN 'Do Not Tax'
                        ELSE 'Tax Mode Not Selected'
                END vcTaxMode,
                CASE WHEN tintFixedShippingQuotesMode= 1 THEN 'Flat rate per item'
                        WHEN tintFixedShippingQuotesMode = 2 THEN 'Ship by weight'
                        WHEN tintFixedShippingQuotesMode = 3 THEN 'Ship by order total'
                        WHEN tintFixedShippingQuotesMode = 4 THEN 'Flat rate per order'
                        WHEN tintFixedShippingQuotesMode= 0 THEN 'Flat rate per item'
                        ELSE 'Fixed rate shipping quotes not selected'
                END  vcFixedShippingQuotesMode

				,(SELECT STUFF((SELECT ', ' + W.vcWareHouse
					FROM   Warehouses W			
					WHERE   W.numWareHouseID  in  ( SELECT  Id
							FROM    dbo.SplitIDs(SR.numWareHouseID, ','))			
					FOR XML PATH ('')),1,2,'')) AS vcShipFrom

                ,  
					STUFF((SELECT  ', ' + dbo.fn_GetListName(numCountryID,0) + ' - ' + 
						STUFF((SELECT ', ' + ISNULL(dbo.fn_GetState(numStateID),'All States') +
								(CASE WHEN LEN( STUFF((SELECT ',' + s.vcZipPostal From ShippingRuleStateList s
														WHERE SR.numRuleID = s.numRuleID AND s.numCountryID =SRSL.numCountryID and s.numStateID = SRSL.numStateID FOR XML PATH ('')),1,1,'')) > 0
						THEN CONCAT (' (',STUFF((SELECT ',' + s.vcZipPostal From ShippingRuleStateList s
															WHERE SR.numRuleID = s.numRuleID AND s.numCountryID =SRSL.numCountryID and s.numStateID = SRSL.numStateID FOR XML PATH ('')),1,1,''), ') ')
									ELSE ''
								END)
						From ShippingRuleStateList SRSL
						WHERE SR.numRuleID = SRSL.numRuleID AND SRSL.numCountryID =SS.numCountryID GROUP BY SRSL.numStateID,numCountryID FOR XML PATH ('')),1,1,'')
										
					FROM ShippingRuleStateList SS					
					WHERE SR.numRuleID = SS.numRuleID
					GROUP BY numCountryID
					FOR XML PATH ('')),1,1,'') AS vcShipTo

				,(CASE WHEN ISNULL(SR.bitFreeShipping,0) = 1
						THEN  (SELECT CONCAT ( (SELECT STUFF((SELECT CONCAT (', ' , intfrom , ' - ', intTo, ' pays $',  CAST(monRate AS DECIMAL(20,2)))
								FROM ShippingServiceTypes SS
								join ShippingRules S on ss.numRuleID = s.numRuleID
								WHERE SR.numRuleID = SS.numRuleID			
								FOR XML PATH ('')),1,2,'')) , ' , Free shipping when order amount reaches $' , SR.FreeShippingOrderAmt))
					ELSE (SELECT STUFF((SELECT CONCAT (', ' , intfrom , ' - ', intTo, ' pays $', CAST(monRate AS DECIMAL(20,2)))
								FROM ShippingServiceTypes SS
								join ShippingRules S on ss.numRuleID = s.numRuleID
								WHERE SR.numRuleID = SS.numRuleID			
								FOR XML PATH ('')),1,2,'')) 
					END ) AS vcShippingCharges
				,(SELECT CONCAT(ISNULL((SELECT vcData from ListDetails LD 
					JOIN  ShippingRules S ON LD.numListItemID = S.numRelationship 
					WHERE LD.numListItemID = S.numRelationship AND S.numRuleID = SR.numRuleID),'All Relationships') , ', ' ,ISNULL((SELECT vcData from ListDetails LD 
					JOIN  ShippingRules S ON LD.numListItemID = S.numProfile 
					WHERE LD.numListItemID = S.numProfile  AND S.numRuleID = SR.numRuleID),'All Profiles'))) AS vcAppliesTo	
                         
        FROM    dbo.ShippingRules SR
        WHERE  
                numDomainID = @numDomainID  
		END

    END 
      ELSE IF @byteMode = 2 --It is for calculating Tax on Shipping Charge (Use it later on Order Summery Page)
            BEGIN
				SELECT tintTaxMode FROM ShippingRules WHERE numRuleID = @numRuleID AND numDomainID = @numDomainID
			END
END
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetShippingServiceTypes' ) 
    DROP PROCEDURE USP_GetShippingServiceTypes
GO
CREATE PROCEDURE USP_GetShippingServiceTypes
    @numDomainID NUMERIC,
    @numRuleID NUMERIC,
    @numServiceTypeID NUMERIC = 0,
    @tintMode TINYINT = 0
AS 
BEGIN
    IF @tintMode = 2 
    BEGIN
		IF NOT EXISTS (SELECT 
							* 
						FROM 
							dbo.ShippingServiceTypes
						WHERE 
							numDomainID = @numDomainID
							AND intNsoftEnum > 0 
							AND ISNULL(numRuleID,0) = 0
						) 
		BEGIN
			INSERT INTO dbo.ShippingServiceTypes
			(	numShippingCompanyID,
				vcServiceName,
				intNsoftEnum,
				intFrom,
				intTo,
				fltMarkup,
				bitMarkupType,
				monRate,
				bitEnabled,
				numRuleID,
				numDomainID 
			)
			SELECT  numShippingCompanyID,
							vcServiceName,
							intNsoftEnum,
							intFrom,
							intTo,
							fltMarkup,
							bitMarkupType,
							monRate,
							bitEnabled,
							NULL,
							@numDomainID
			FROM    dbo.ShippingServiceTypes
			WHERE   numDomainID = 0 AND intNsoftEnum > 0 AND ISNULL(numRuleID,0) = 0
		END

        SELECT DISTINCT 
			* 
		FROM 
		(
			SELECT	
				numServiceTypeID,
				vcServiceName,
				intNsoftEnum,
				intFrom,
				intTo,
				fltMarkup,
				bitMarkupType,
				monRate,
				bitEnabled,
				numRuleID,
				numDomainID,
				numShippingCompanyID,
				CASE WHEN numShippingCompanyID = 91 THEN 'Fedex' 
						WHEN numShippingCompanyID = 88 THEN 'UPS'
						WHEN numShippingCompanyID = 90 THEN 'USPS'
						ELSE 'Other'
				END AS vcShippingCompany	 	
			FROM 
				dbo.ShippingServiceTypes
			WHERE 
				numDomainID = @numDomainID
				AND ISNULL(numRuleID,0) = 0
				AND (numServiceTypeID = @numServiceTypeID OR @numServiceTypeID = 0)
				AND intNsoftEnum > 0							
			UNION ALL	
			SELECT	101,'Amazon Standard',101,0,0,0,1,0,1,0,0,0,'Amazon' vcShippingCompany	 						
			UNION ALL
			SELECT	102,'ShippingMethodStandard',102,0,0,0,1,0,1,0,0,0,'E-Bay' vcShippingCompany	 							
			UNION ALL
			SELECT	103,'Other',103,0,0,0,1,0,1,0,0,0,'E-Bay' vcShippingCompany								
		) TABLE1 
		WHERE 
			(numServiceTypeID = @numServiceTypeID OR @numServiceTypeID = 0) 
		ORDER BY 
			vcShippingCompany
    END
    IF @tintMode=1
    BEGIN
	  	SELECT 
			numServiceTypeID,
            vcServiceName,
            intNsoftEnum,
            intFrom,
            intTo,
            fltMarkup,
            bitMarkupType,
            CAST(monRate AS DECIMAL(20,2)) AS monRate,
            bitEnabled,
            numRuleID,
            numDomainID,
			(select
   distinct  
    stuff((
        select ',' + LDI.vcData
        from ListDetails LDI
        where LDI.vcData = vcData AND LDI.numListID=36 and LDI.numDomainID=@numDomainID  AND numListItemID IN(SELECT Items FROM dbo.Split(vcItemClassification,',') WHERE Items<>'')
        order by LDI.vcData
        for xml path('')
    ),1,1,'') as vcData
from ListDetails AS LD WHERE LD.numListID=36 and LD.numDomainID=@numDomainID AND numListItemID IN(SELECT Items FROM dbo.Split(vcItemClassification,',') WHERE Items<>'')
group by vcData) As vcItemClassificationName
			,vcItemClassification
        FROM
			dbo.ShippingServiceTypes
        WHERE 
			numDomainID = @numDomainID
            AND numRuleID = @numRuleID
            AND (numServiceTypeID = @numServiceTypeID OR @numServiceTypeID = 0)
            AND ISNULL(intNsoftEnum,0)=0
	END
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_Import_SaveItemDetails')
DROP PROCEDURE USP_Import_SaveItemDetails
GO
CREATE PROCEDURE [dbo].[USP_Import_SaveItemDetails]  
	@numDomainID NUMERIC(18,0)
	,@numUserCntID NUMERIC(18,0)
	,@numItemCode NUMERIC(18,0) OUTPUT
	,@numShipClass NUMERIC(18,0)
	,@vcItemName VARCHAR(300)
	,@monListPrice DECIMAL(20,5)
	,@txtItemDesc VARCHAR(1000)
	,@vcModelID VARCHAR(200)
	,@vcManufacturer VARCHAR(250)
	,@numBarCodeId VARCHAR(50)
	,@fltWidth FLOAT
	,@fltHeight FLOAT
	,@fltWeight FLOAT
	,@fltLength FLOAT
	,@monAverageCost DECIMAL(20,5)
	,@bitSerialized BIT
	,@bitKitParent BIT
	,@bitAssembly BIT
	,@IsArchieve BIT
	,@bitLotNo BIT
	,@bitAllowBackOrder BIT
	,@numIncomeChartAcntId NUMERIC(18,0)
	,@numAssetChartAcntId NUMERIC(18,0)
	,@numCOGsChartAcntId NUMERIC(18,0)
	,@numItemClassification NUMERIC(18,0)
	,@numItemGroup NUMERIC(18,0)
	,@numPurchaseUnit NUMERIC(18,0)
	,@numSaleUnit NUMERIC(18,0)
	,@numItemClass NUMERIC(18,0)
	,@numBaseUnit NUMERIC(18,0)
	,@vcSKU VARCHAR(50)
	,@charItemType CHAR(1)
	,@bitTaxable BIT
	,@txtDesc NVARCHAR(MAX)
	,@vcExportToAPI VARCHAR(50)
	,@bitAllowDropShip BIT
	,@bitMatrix BIT
	,@vcItemAttributes VARCHAR(2000)
	,@vcCategories VARCHAR(1000)
	,@txtShortDesc NVARCHAR(MAX)
	,@numReOrder FLOAT
	,@bitAutomateReorderPoint BIT
AS
BEGIN
BEGIN TRY
BEGIN TRANSACTION
	-- IMPORTANT: PLEASE MAKE SURE NEW FIELD WHICH YOUR ARE ADDING IN STORE PROCEDURE FOR UPDATE ARE ADDED IN SELECT IN PROCEDURE USP_Import_GetItemDetails

	IF @numAssetChartAcntId = 0
	BEGIN
		SET @numAssetChartAcntId =NULL
	END

	DECLARE @bitAttributesChanged AS BIT  = 1
	DECLARE @bitOppOrderExists AS BIT = 0
	DECLARE @hDoc AS INT     
	                                                                        	
	DECLARE @TempTable TABLE 
	(
		ID INT IDENTITY(1,1),
		Fld_ID NUMERIC(18,0),
		Fld_Value NUMERIC(18,0)
	)   

	IF ISNULL(@bitMatrix,0) = 1 AND ISNULL(@numItemGroup,0) = 0
	BEGIN
		RAISERROR('ITEM_GROUP_NOT_SELECTED',16,1)
		RETURN
	END
	ELSE IF ISNULL(@bitMatrix,0) = 1 AND LEN(ISNULL(@vcItemAttributes,'')) = 0 AND ISNULL(@numItemCode,0) = 0
	BEGIN
		RAISERROR('ITEM_ATTRIBUTES_NOT_SELECTED',16,1)
		RETURN
	END
	ELSE IF ISNULL(@bitMatrix,0) = 1 AND LEN(ISNULL(@vcItemAttributes,'')) > 0
	BEGIN
		DECLARE @bitDuplicate AS BIT = 0
	
		IF DATALENGTH(ISNULL(@vcItemAttributes,''))>2
		BEGIN
			EXEC sp_xml_preparedocument @hDoc OUTPUT, @vcItemAttributes
	                                      
			INSERT INTO @TempTable 
			(
				Fld_ID,
				Fld_Value
			)    
			SELECT
				*          
			FROM 
				OPENXML (@hDoc,'/NewDataSet/CusFlds',2)                                                  
			WITH 
				(Fld_ID NUMERIC(18,0),Fld_Value NUMERIC(18,0)) 
			ORDER BY 
				Fld_ID       

			DECLARE @strAttribute VARCHAR(500) = ''
			DECLARE @i AS INT = 1
			DECLARE @COUNT AS INT 
			DECLARE @numFldID AS NUMERIC(18,0)
			DECLARE @numFldValue AS NUMERIC(18,0)
			SELECT @COUNT = COUNT(*) FROM @TempTable

			WHILE @i <= @COUNT
			BEGIN
				SELECT @numFldID=Fld_ID,@numFldValue=Fld_Value FROM @TempTable WHERE ID=@i

				IF ISNUMERIC(@numFldValue)=1
				BEGIN
					SELECT @strAttribute=@strAttribute+Fld_label FROM CFW_Fld_Master WHERE Fld_id=@numFldID AND Grp_id = 9
                
					IF LEN(@strAttribute) > 0
					BEGIN
						SELECT @strAttribute=@strAttribute+':'+ vcData +',' FROM ListDetails WHERE numListItemID=@numFldValue
					END
				END

				SET @i = @i + 1
			END

			SET @strAttribute = SUBSTRING(@strAttribute,0,LEN(@strAttribute))

			IF
				(SELECT
					COUNT(*)
				FROM
					Item
				INNER JOIN
					ItemAttributes
				ON
					Item.numItemCode = ItemAttributes.numItemCode
					AND ItemAttributes.numDomainID = @numDomainID                     
				WHERE
					Item.numItemCode <> @numItemCode
					AND Item.vcItemName = @vcItemName
					AND Item.numItemGroup = @numItemGroup
					AND Item.numDomainID = @numDomainID
					AND dbo.fn_GetItemAttributes(@numDomainID,Item.numItemCode,0) = @strAttribute
				) > 0
			BEGIN
				RAISERROR('ITEM_WITH_ATTRIBUTES_ALREADY_EXISTS',16,1)
				RETURN
			END

			If ISNULL(@numItemCode,0) > 0 AND dbo.fn_GetItemAttributes(@numDomainID,@numItemCode,0) = @strAttribute
			BEGIN
				SET @bitAttributesChanged = 0
			END

			EXEC sp_xml_removedocument @hDoc 
		END	
	END
	ELSE IF ISNULL(@bitMatrix,0) = 0
	BEGIN
		-- REMOVE ATTRIBUTES CONFIGURATION OF ITEM AND WAREHOUSE
		DELETE FROM ItemAttributes WHERE numDomainID=@numDomainID AND numItemCode=@numItemCode
	END 


	IF ISNULL(@numItemGroup,0) > 0 AND ISNULL(@bitMatrix,0) = 1
	BEGIN
		IF (SELECT COUNT(*) FROM ItemGroupsDTL WHERE numItemGroupID = @numItemGroup AND tintType=2) = 0
		BEGIN
			RAISERROR('ITEM_GROUP_NOT_CONFIGURED',16,1)
			RETURN
		END
		ELSE IF EXISTS (SELECT 
							CFM.Fld_id
						FROM 
							CFW_Fld_Master CFM
						LEFT JOIN
							ListDetails LD
						ON
							CFM.numlistid = LD.numListID
						WHERE
							CFM.numDomainID = @numDomainID
							AND CFM.Fld_id IN (SELECT numOppAccAttrID FROM ItemGroupsDTL WHERE numItemGroupID=@numItemGroup AND tintType = 2)
						GROUP BY
							CFM.Fld_id HAVING COUNT(LD.numListItemID) = 0)
		BEGIN
			RAISERROR('ITEM_GROUP_NOT_CONFIGURED',16,1)
			RETURN
		END
	END


	IF ISNULL(@numItemCode,0) = 0
	BEGIN
		IF (ISNULL(@vcItemName,'') = '')
		BEGIN
			RAISERROR('ITEM_NAME_NOT_SELECTED',16,1)
			RETURN
		END
        ELSE IF (ISNULL(@charItemType,'') = '' OR ISNULL(@charItemType,'') = '0' )
		BEGIN
			RAISERROR('ITEM_TYPE_NOT_SELECTED',16,1)
			RETURN
		END
        ELSE
		BEGIN 
		    If (@charItemType = 'P')
			BEGIN
				IF ((SELECT COUNT(*) FROM Chart_Of_Accounts WHERE numDomainId=@numDomainID AND numAccountId=@numAssetChartAcntId) = 0)
				BEGIN
					RAISERROR('INVALID_ASSET_ACCOUNT',16,1)
					RETURN
				END
			END
			ELSE IF @numAssetChartAcntId = 0
			BEGIN
				SET @numAssetChartAcntId = NULL
			END

			-- This is common check for CharItemType P and Other
			IF ((SELECT COUNT(*) FROM Chart_Of_Accounts WHERE numDomainId=@numDomainID AND numAccountId=@numCOGSChartAcntId) = 0)
			BEGIN
				RAISERROR('INVALID_COGS_ACCOUNT',16,1)
				RETURN
			END

			IF ((SELECT COUNT(*) FROM Chart_Of_Accounts WHERE numDomainId=@numDomainID AND numAccountId=@numIncomeChartAcntId) = 0)
			BEGIN
				RAISERROR('INVALID_INCOME_ACCOUNT',16,1)
				RETURN
			END
		END


		INSERT INTO Item
		(
			numDomainID
			,numCreatedBy
			,bintCreatedDate
			,bintModifiedDate
			,numModifiedBy
			,numShipClass
			,vcItemName
			,monListPrice
			,txtItemDesc
			,vcModelID
			,vcManufacturer
			,numBarCodeId
			,fltWidth
			,fltHeight
			,fltWeight
			,fltLength
			,monAverageCost
			,bitSerialized
			,bitKitParent
			,bitAssembly
			,IsArchieve
			,bitLotNo
			,bitAllowBackOrder
			,numIncomeChartAcntId
			,numAssetChartAcntId
			,numCOGsChartAcntId
			,numItemClassification
			,numItemGroup
			,numPurchaseUnit
			,numSaleUnit
			,numItemClass
			,numBaseUnit
			,vcSKU
			,charItemType
			,bitTaxable
			,vcExportToAPI
			,bitAllowDropShip
			,bitMatrix
			,numManufacturer
			,bitAutomateReorderPoint
		)
		VALUES
		(
			@numDomainID
			,@numUserCntID
			,GETUTCDATE()
			,GETUTCDATE()
			,@numUserCntID
			,@numShipClass
			,@vcItemName
			,@monListPrice
			,@txtItemDesc
			,@vcModelID
			,@vcManufacturer
			,@numBarCodeId
			,@fltWidth
			,@fltHeight
			,@fltWeight
			,@fltLength
			,@monAverageCost
			,@bitSerialized
			,@bitKitParent
			,@bitAssembly
			,@IsArchieve
			,@bitLotNo
			,@bitAllowBackOrder
			,NULLIF(@numIncomeChartAcntId,0)
			,NULLIF(@numAssetChartAcntId,0)
			,NULLIF(@numCOGsChartAcntId,0)
			,@numItemClassification
			,@numItemGroup 
			,@numPurchaseUnit 
			,@numSaleUnit
			,@numItemClass
			,@numBaseUnit
			,@vcSKU
			,@charItemType
			,@bitTaxable
			,@vcExportToAPI
			,@bitAllowDropShip
			,@bitMatrix
			,(CASE WHEN LEN(ISNULL(@vcManufacturer,'')) > 0 THEN ISNULL((SELECT TOP 1 numDivisionID FROM CompanyInfo INNER JOIN DivisionMaster ON CompanyInfo.numCompanyId=DivisionMaster.numCompanyID WHERE CompanyInfo.numDOmainID=@numDomainID AND LOWER(vcCompanyName) = LOWER(@vcManufacturer)),0) ELSE 0 END)
			,@bitAutomateReorderPoint
		)

		SET @numItemCode = SCOPE_IDENTITY()

		INSERT INTO ItemExtendedDetails (numItemCode,txtDesc,txtShortDesc) VALUES (@numItemCode,@txtDesc,@txtShortDesc)

		IF @charItemType = 'P'
		BEGIN
			
			DECLARE @TEMPWarehouse TABLE
			(
				ID INT IDENTITY(1,1)
				,numWarehouseID NUMERIC(18,0)
			)
		
			INSERT INTO @TEMPWarehouse (numWarehouseID) SELECT numWareHouseID FROM Warehouses WHERE numDomainID=@numDomainID

			DECLARE @j AS INT = 1
			DECLARE @jCount AS INT
			DECLARE @numTempWarehouseID NUMERIC(18,0)
			SELECT @jCount = COUNT(*) FROM @TEMPWarehouse

			WHILE @j <= @jCount
			BEGIN
				SELECT @numTempWarehouseID=numWarehouseID FROM @TEMPWarehouse WHERE ID=@j

				EXEC USP_WarehouseItems_Save @numDomainID,
											@numUserCntID,
											0,
											@numTempWarehouseID,
											0,
											@numItemCode,
											'',
											@monListPrice,
											0,
											0,
											'',
											'',
											'',
											'',
											0
 
				SET @j = @j + 1
			END
		END 
	END
	ELSE IF ISNULL(@numItemCode,0) > 0 AND EXISTS (SELECT numItemCode FROM Item WHERE numDomainID=@numDomainID AND numItemCode=@numItemCode)
	BEGIN
		IF (SELECT COUNT(OI.numOppId) FROM dbo.OpportunityItems OI WHERE OI.numItemCode =@numItemCode) > 0
		BEGIN
			SET @bitOppOrderExists = 1
		END
		ELSE IF (SELECT COUNT(OI.numOppId) FROM dbo.OpportunityKitItems OI WHERE OI.numChildItemID =@numItemCode) > 0
		BEGIN
			SET @bitOppOrderExists = 1
		END
		ELSE IF (SELECT COUNT(OI.numOppId) FROM dbo.OpportunityKitChildItems OI WHERE OI.numItemID =@numItemCode) > 0
		BEGIN
			SET @bitOppOrderExists = 1
		END

		DECLARE @OldGroupID NUMERIC 
		SELECT @OldGroupID = ISNULL(numItemGroup,0) FROM [Item] WHERE numitemcode = @numItemCode AND [numDomainId] = @numDomainID
	
		DECLARE @monOldAverageCost AS DECIMAL(18,4);SET @monOldAverageCost=0
		SELECT @monOldAverageCost=(CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(monAverageCost,0) END) FROM Item WHERE numItemCode =@numItemCode AND [numDomainId] = @numDomainID
	   
		UPDATE 
			Item 
		SET 
			vcItemName=@vcItemName,txtItemDesc=@txtItemDesc,charItemType=@charItemType,monListPrice= @monListPrice,numItemClassification=@numItemClassification,                                             
			bitTaxable=@bitTaxable,vcSKU = @vcSKU,bitKitParent=@bitKitParent,numDomainID=@numDomainID,bintModifiedDate=getutcdate(),numModifiedBy=@numUserCntID
			,bitSerialized=@bitSerialized,vcModelID=@vcModelID,numItemGroup=@numItemGroup,numCOGsChartAcntId=NULLIF(@numCOGsChartAcntId,0),numAssetChartAcntId=NULLIF(@numAssetChartAcntId,0),                                      
			numIncomeChartAcntId=NULLIF(@numIncomeChartAcntId,0),fltWeight=@fltWeight,fltHeight=@fltHeight,fltWidth=@fltWidth,     
			fltLength=@fltLength,bitAllowBackOrder=@bitAllowBackOrder,bitAssembly=@bitAssembly,numBarCodeId=@numBarCodeId,
			vcManufacturer=@vcManufacturer,numBaseUnit=@numBaseUnit,numPurchaseUnit=@numPurchaseUnit,numSaleUnit=@numSaleUnit,bitLotNo=@bitLotNo,
			IsArchieve = @IsArchieve,numItemClass=@numItemClass,vcExportToAPI=@vcExportToAPI,
			numShipClass=@numShipClass,bitAllowDropShip=@bitAllowDropShip, bitArchiveItem = @IsArchieve,bitMatrix=@bitMatrix,
			numManufacturer=(CASE WHEN LEN(ISNULL(@vcManufacturer,'')) > 0 THEN ISNULL((SELECT TOP 1 numDivisionID FROM CompanyInfo INNER JOIN DivisionMaster ON CompanyInfo.numCompanyId=DivisionMaster.numCompanyID WHERE CompanyInfo.numDOmainID=@numDomainID AND LOWER(vcCompanyName) = LOWER(@vcManufacturer)),0) ELSE 0 END)
			,bitAutomateReorderPoint=@bitAutomateReorderPoint
		WHERE 
			numItemCode=@numItemCode 
			AND numDomainID=@numDomainID

		IF NOT EXISTS(SELECT numItemCode FROM dbo.OpportunityItems WHERE numItemCode = @numItemCode)
		BEGIN
			IF (((SELECT ISNULL(SUM(numOnHand),0) FROM WareHouseItems WHERE numItemID = @numItemCode AND numDomainId=@numDomainID) = 0) AND ((SELECT (CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(monAverageCost,0) END) FROM Item  WHERE numItemCode = @numItemCode AND numDomainId=@numDomainID ) = 0))
			BEGIN
				UPDATE Item SET monAverageCost=@monAverageCost  where numItemCode=@numItemCode  AND numDomainID=@numDomainID	
			END
		END
	
		--If Average Cost changed then add in Tracking
		IF ISNULL(@monAverageCost,0) != ISNULL(@monOldAverageCost,0)
		BEGIN
			DECLARE @numWareHouseItemID NUMERIC(9);SET @numWareHouseItemID=0
			DECLARE @numTotal int;SET @numTotal=0
			SET @i=0
			DECLARE @vcDescription AS VARCHAR(100);SET @vcDescription=''
		
			SELECT @numTotal=COUNT(*) FROM WareHouseItems where numItemID=@numItemCode AND numDomainId=@numDomainId
	     
			WHILE @i < @numTotal
			BEGIN
				SELECT @numWareHouseItemID = min(numWareHouseItemID) FROM WareHouseItems 
					WHERE numItemID=@numItemCode AND numDomainId=@numDomainId and numWareHouseItemID > @numWareHouseItemID 
				
				IF @numWareHouseItemID>0
				BEGIN
					SET @vcDescription='Average Cost Changed Manually OLD : ' + CAST(ISNULL(@monOldAverageCost,0) AS VARCHAR(20))
				
					DECLARE @numDomain AS NUMERIC(18,0)
					SET @numDomain = @numDomainID
					EXEC dbo.USP_ManageWareHouseItems_Tracking
						@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)		
						@numReferenceID = @numItemCode, --  numeric(9, 0)
						@tintRefType = 1, --  tinyint
						@vcDescription = @vcDescription, --  varchar(100)
						@tintMode = 0, --  tinyint
						@numModifiedBy = @numUserCntID, --  numeric(9, 0)
						@numDomainID = @numDomain
				END
		    
				SET @i = @i + 1
			END
		END
 
		IF	@bitTaxable = 1
		BEGIN
			IF NOT EXISTS(SELECT * FROM dbo.ItemTax WHERE numItemCode = @numItemCode AND numTaxItemID = 0)
			BEGIN
			    INSERT INTO dbo.ItemTax 
				(
					numItemCode,
					numTaxItemID,
					bitApplicable
				)
				VALUES 
				( 
					@numItemCode,
					0,
					1 
				) 						
			END
		END	 
      
		IF @charItemType='S' or @charItemType='N'                                                                       
		BEGIN
			DELETE FROM WareHouseItmsDTL where numWareHouseItemID in (select numWareHouseItemID from WareHouseItems where numItemID=@numItemCode AND [WareHouseItems].[numDomainID] = @numDomainID)
			DELETE FROM WareHouseItems where numItemID=@numItemCode AND [WareHouseItems].[numDomainID] = @numDomainID
		END

		IF NOT EXISTS (SELECT numItemCode FROM ItemExtendedDetails  where numItemCode=@numItemCode)
		BEGIN
			INSERT INTO ItemExtendedDetails (numItemCode,txtDesc,txtShortDesc) VALUES (@numItemCode,@txtDesc,@txtShortDesc)
		END
		ELSE
		BEGIN
			UPDATE ItemExtendedDetails SET txtDesc=@txtDesc,@txtShortDesc=txtShortDesc WHERE numItemCode=@numItemCode
		END
	END

	IF ISNULL(@numItemGroup,0) = 0 OR ISNULL(@bitMatrix,0) = 1
	BEGIN
		UPDATE WareHouseItems SET monWListPrice = @monListPrice, vcWHSKU=@vcSKU, vcBarCode=@numBarCodeId WHERE numDomainID = @numDomainID AND numItemID = @numItemCode
	END

	IF @vcCategories <> ''	
	BEGIN
		INSERT INTO ItemCategory( numItemID,numCategoryID )  SELECT @numItemCode,ID from dbo.SplitIDs(@vcCategories,',')	
	END

	-- SAVE MATRIX ITEM ATTRIBUTES
	IF @numItemCode > 0 AND ISNULL(@bitAttributesChanged,0)=1 AND ISNULL(@bitMatrix,0) = 1 AND LEN(ISNULL(@vcItemAttributes,'')) > 0 AND ISNULL(@bitOppOrderExists,0) <> 1 AND (SELECT COUNT(*) FROM @TempTable) > 0
	BEGIN
		-- DELETE PREVIOUS ITEM ATTRIBUTES
		DELETE FROM ItemAttributes WHERE numDomainID=@numDomainID AND numItemCode=@numItemCode

		-- INSERT NEW ITEM ATTRIBUTES
		INSERT INTO ItemAttributes
		(
			numDomainID
			,numItemCode
			,FLD_ID
			,FLD_Value
		)
		SELECT
			@numDomainID
			,@numItemCode
			,Fld_ID
			,Fld_Value
		FROM
			@TempTable

		IF (SELECT COUNT(*) FROM WarehouseItems WHERE numDomainID=@numDomainID AND numItemID=@numItemCode) > 0
		BEGIN

			-- DELETE PREVIOUS WAREHOUSE ATTRIBUTES
			DELETE FROM CFW_Fld_Values_Serialized_Items WHERE RecId IN (SELECT ISNULL(numWarehouseItemID,0) FROM WarehouseItems WHERE numDomainID=@numDomainID AND numItemID=@numItemCode)

			-- INSERT NEW WAREHOUSE ATTRIBUTES
			INSERT INTO CFW_Fld_Values_Serialized_Items
			(
				Fld_ID,
				Fld_Value,
				RecId,
				bitSerialized
			)                                                  
			SELECT 
				Fld_ID,
				ISNULL(CAST(Fld_Value AS VARCHAR),'') AS Fld_Value,
				TEMPWarehouse.numWarehouseItemID,
				0 
			FROM 
				@TempTable  
			OUTER APPLY
			(
				SELECT
					numWarehouseItemID
				FROM 
					WarehouseItems 
				WHERE 
					numDomainID=@numDomainID 
					AND numItemID=@numItemCode
			) AS TEMPWarehouse
		END
	END	 

	UPDATE WareHouseItems SET numReorder=ISNULL(@numReOrder,0) WHERE numDomainID=@numDomain AND numItemID=@numItemCode
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH        
END
/****** Object:  StoredProcedure [dbo].[USP_InsertImapUserDtls]    Script Date: 07/26/2008 16:19:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_insertimapuserdtls')
DROP PROCEDURE usp_insertimapuserdtls
GO
CREATE PROCEDURE [dbo].[USP_InsertImapUserDtls]
    @vcImapPassword AS VARCHAR(100),
    @vcImapServerUrl AS VARCHAR(200),
    @numImapSSLPort AS NUMERIC,
    @bitImapSsl AS BIT,
    @bitImap AS BIT,
    @numUserCntId AS NUMERIC(9),--tintMode=0 then supply UserID instead of UserContactID
    @numDomainID AS NUMERIC(9),
    @bitUseUserName AS BIT,
    @numLastUID AS NUMERIC(9),
    @tintMode TINYINT,
    @vcImapUserName AS VARCHAR(50)--For Support Email Address
AS 
DECLARE @numUserDetailId NUMERIC(9);SET @numUserDetailId=@numUserCntId

--IF @numUserCntId=-1 : For Support Email 
IF @numUserCntId>0
BEGIN
	SELECT  @numUserDetailId = numUserDetailId
	FROM    UserMaster
	WHERE   numUserId = @numUserCntId

	UPDATE UserMaster SET tintMailProvider=3 WHERE numUserId=@numUserCntId
END

--delete [ImapUserDetails] where numdomainId = @numDomainID and numUserCntId = @numUserDetailId
IF @tintMode = 0 
    BEGIN
        IF NOT EXISTS ( SELECT  *
                        FROM    [ImapUserDetails]
                        WHERE   numdomainId = @numDomainID
                                AND numUserCntId = @numUserDetailId ) 
            BEGIN
                INSERT  INTO [ImapUserDetails] ( bitImap,
                                                 [vcImapServerUrl],
                                                 [vcImapPassword],
                                                 [bitSSl],
                                                 [numPort],
                                                 [numDomainId],
                                                 [numUserCntId],
                                                 bitUseUserName,
                                                 tintRetryLeft,
                                                 numLastUID,
                                                 bitNotified,vcImapUserName)
                VALUES  (
                          @bitImap,
                          @vcImapServerUrl,
                          @vcImapPassword,
                          @bitImapSsl,
                          @numImapSSLPort,
                          @numDomainID,
                          @numUserDetailId,
                          @bitUseUserName,
                          10,
                          0,
                          1,@vcImapUserName
                        )
            END
        ELSE 
            BEGIN
                UPDATE  ImapUserDetails
                SET     bitImap = @bitImap,
                        [vcImapServerUrl] = @vcImapServerUrl,
                        [vcImapPassword] = @vcImapPassword,
                        [bitSSl] = @bitImapSsl,
                        [numPort] = @numImapSSLPort,
                        bitUseUserName = @bitUseUserName,
                        bitNotified=1,
                        tintRetryLeft=10,vcImapUserName=@vcImapUserName
                WHERE   numdomainId = @numDomainID
                        AND numUserCntId = @numUserDetailId 
            END
            
    END 
ELSE 
    IF @tintMode = 1 
        BEGIN
		
            UPDATE  ImapUserDetails
            SET     numLastUID = @numLastUID
            WHERE   numdomainId = @numDomainID
                    AND numUserCntId = @numUserCntId
        END
        
ELSE IF @tintMode = 2 
        BEGIN
		
            UPDATE  ImapUserDetails
            SET     bitNotified=1
            WHERE   numdomainId = @numDomainID
                    AND numUserCntId = @numUserCntId
        END

GO
/****** Object:  StoredProcedure [dbo].[usp_InsertStagePercentageDetails]    Script Date: 09/17/2010 17:41:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_insertstagepercentagedetails')
DROP PROCEDURE usp_insertstagepercentagedetails
GO
CREATE PROCEDURE [dbo].[usp_InsertStagePercentageDetails]
    @numStagePercentageId NUMERIC,
    @tintConfiguration TINYINT,
    @vcStageName VARCHAR(1000),
    @slpid NUMERIC,
    @numDomainId NUMERIC,
    @numUserCntID NUMERIC,
    @numAssignTo NUMERIC = 0,
    @vcMileStoneName VARCHAR(1000),
    @tintPercentage AS TINYINT,
    @vcDescription VARCHAR(2000),
	@dtStartDate as DATETIME,
	@dtEndDate as DATETIME, 
    @numParentStageID NUMERIC,
	@numProjectID NUMERIC,
	@numOppID NUMERIC
AS 
DECLARE @numStageDetailId NUMERIC               

DECLARE @sortOrder AS NUMERIC(18,0)=0
SET @sortOrder=(SELECT ISNULL(MAX(numStageOrder),0) FROM StagePercentageDetails WHERE numStagePercentageId=@numStagePercentageId)
SET @sortOrder=@sortOrder+1;
INSERT  INTO [StagePercentageDetails] ( [numStagePercentageId],
                                        [tintConfiguration],
                                        [vcStageName],
                                        [numDomainId],
                                        [numCreatedBy],
                                        [bintCreatedDate],
                                        [numModifiedBy],
                                        [bintModifiedDate],
                                        [slp_id],
                                        [numAssignTo],
                                        [vcMileStoneName],
                                        [tintPercentage],
                                        [vcDescription],dtStartDate,dtEndDate,numParentStageID,numProjectID,numOppID,bitclose,numStageOrder
 )
VALUES  (
          @numStagePercentageId,
          @tintConfiguration,
          @vcStageName,
          @numDomainId,
          @numUserCntID,
          GETUTCDATE(),
          @numUserCntID,
          GETUTCDATE(),
          @slpid,
          @numAssignTo,
          @vcMileStoneName,
          @tintPercentage,
          @vcDescription,@dtStartDate,@dtEndDate,@numParentStageID,@numProjectID,@numOppID,0,@sortOrder
  )

  DECLARE @numTempStagePercentageId AS NUMERIC(18,0) 
  SET @numTempStagePercentageId = SCOPE_IDENTITY()

if((select bitClose from StagePercentageDetails where numStageDetailsId=@numParentStageID)=1)
	update StagePercentageDetails set bitclose=1,tinProgressPercentage=100 where @numParentStageID=numParentStageID and numStageDetailsId=@numTempStagePercentageId

	EXEC usp_ManageStageAccessDetail @numProjectID,@numOppID,@numTempStagePercentageId,@numUserCntID,0

	IF ISNULL((SELECT COUNT(*) FROM StagePercentageDetailsTask WHERE numStageDetailsId=@numTempStagePercentageId),0) = 0
	BEGIN
		EXEC USP_ManageStageTask
				@numDomainID,    
				@numTempStagePercentageId,           
				'Place Holder',           
				0,           
				0,           
				@numUserCntID,           
				@numUserCntID,
				@numOppId,   
				@numProjectId,   
				0,
				0,
				0,
				0,
				0,
				0,
				0
	END

--Update total progress
IF @numProjectID>0
	EXEC dbo.USP_GetProjectTotalProgress
	@numDomainID = @numDomainID, --  numeric(9, 0)
	@numProId = @numProjectID, --  numeric(9, 0)
	@tintMode = 1 --  tinyint

IF @numOppID>0
	EXEC dbo.USP_GetProjectTotalProgress
	@numDomainID = @numDomainID, --  numeric(9, 0)
	@numProId = @numOppID, --  numeric(9, 0)
	@tintMode = 0 --  tinyint

  
SELECT  @numTempStagePercentageId
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_Item_GetDetailForStockTransfer' ) 
    DROP PROCEDURE USP_Item_GetDetailForStockTransfer
GO
CREATE PROCEDURE USP_Item_GetDetailForStockTransfer
    @numDomainId NUMERIC(18,0),
    @numItemCode NUMERIC(18,0),
	@numFromWarehouseItemID NUMERIC(18,0),
	@numToWarehouseItemID NUMERIC(18,0)
AS 
BEGIN
	IF ISNULL(@numFromWarehouseItemID,0) > 0 AND ISNULL(@numToWarehouseItemID,0) > 0
	BEGIN
		SELECT
			Item.numItemCode AS numFromItemCode
			,ISNULL(Item.bitSerialized,0) bitSerialized
			,ISNULL(Item.bitLotNo,0) bitLotNo
			,CONCAT(vcItemName,', ',Warehouses.vcWareHouse,(CASE WHEN LEN(ISNULL(WarehouseLocation.vcLocation,'')) > 0 THEN CONCAT(' (',WarehouseLocation.vcLocation,')') ELSE '' END)) AS vcFrom
			,WareHouseItems.numWareHouseItemID AS numFromWarehouseItemID
			,ISNULL(WareHouseItems.numOnHand,0) numFromOnHand
			,ISNULL(WareHouseItems.numOnHand,0) + ISNULL(WareHouseItems.numAllocation,0) numFromAvailable
			,ISNULL(WareHouseItems.numBackOrder,0) numFromBackOrder
			,WarehouseTo.numItemCode AS numToItemCode
			,WarehouseTo.vcTo
			,WarehouseTo.numWareHouseItemID AS numToWarehouseItemID
			,WarehouseTo.numToAvailable
			,WarehouseTo.numToBackOrder
			,WareHouseItems.numWareHouseID AS numFromWarehouseID
			,WarehouseTo.numWareHouseID AS numToWarehouseID
		FROM
			Item 
		INNER JOIN
			WareHouseItems
		ON
			WareHouseItems.numItemID = Item.numItemCode
			AND WareHouseItems.numWareHouseItemID = @numFromWarehouseItemID
		INNER JOIN
			Warehouses
		ON
			WareHouseItems.numWareHouseID = Warehouses.numWareHouseID
		LEFT JOIN
			WarehouseLocation
		ON
			WareHouseItems.numWLocationID = WarehouseLocation.numWLocationID
		CROSS APPLY
		(
			SELECT 
				numItemCode
				,CONCAT(vcItemName,', ',WTo.vcWareHouse,(CASE WHEN LEN(ISNULL(WLTo.vcLocation,'')) > 0 THEN CONCAT(' (',WLTo.vcLocation,')') ELSE '' END)) AS vcTo
				,WHITo.numWareHouseID
				,WHITo.numWareHouseItemID
				,ISNULL(WHITo.numOnHand,0) + ISNULL(WHITo.numAllocation,0) numToAvailable
				,ISNULL(WHITo.numBackOrder,0) numToBackOrder
			FROM
				WareHouseItems WHITo
			INNER JOIN
				Item ITo
			ON	
				WHITo.numItemID = ITo.numItemCode
			INNER JOIN
				Warehouses WTo
			ON
				WHITo.numWareHouseID = WTo.numWareHouseID
			LEFT JOIN
				WarehouseLocation WLTo
			ON
				WHITo.numWLocationID = WLTo.numWLocationID
			WHERE
				WHITo.numWareHouseItemID=@numToWarehouseItemID
		) WarehouseTo
		WHERE
			Item.numDomainID=@numDomainId
			AND Item.numItemCode=@numItemCode
	END
	ELSE
	BEGIN
		SELECT
			ISNULL(vcSKU,'') vcSKU
			,dbo.fn_GetItemAttributes(@numDomainId,@numItemCode,0) vcAttributes
			,ISNULL(Item.bitSerialized,0) bitSerialized
			,ISNULL(Item.bitLotNo,0) bitLotNo
		FROM
			Item
		WHERE
			numDomainID=@numDomainId
			AND numItemCode=@numItemCode
	END

	
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
            
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_Item_GetEcommerceWarehouseAvailability')
DROP PROCEDURE USP_Item_GetEcommerceWarehouseAvailability
GO
CREATE PROCEDURE [dbo].[USP_Item_GetEcommerceWarehouseAvailability]    
(
	@numDomainID NUMERIC(18,0)
	,@numItemCode NUMERIC(18,0)
	,@tintWarehouseAvailability TINYINT
	,@bitDisplayQtyAvailable BIT
)
AS
BEGIN
	IF @tintWarehouseAvailability = 3
	BEGIN
		SELECT 
			FORMAT(ISNULL(SUM(numOnHand),0), '#,##0')
		FROM
			WarehouseItems
		INNER JOIN
			Warehouses
		ON
			WarehouseItems.numWareHouseID = Warehouses.numWareHouseID
		WHERE
			WarehouseItems.numDomainID=@numDomainID
			AND WarehouseItems.numItemID=@numItemCode
	END
	ELSE
	BEGIN
		SELECT STUFF((SELECT
					CONCAT(', ',vcWareHouse,(CASE WHEN @bitDisplayQtyAvailable=1 THEN CONCAT('(',numOnHand,')') ELSE '' END))
				FROM
				(
					SELECT 
						Warehouses.numWareHouseID
						,Warehouses.vcWareHouse
						,SUM(numOnHand) numOnHand
					FROM
						WarehouseItems
					INNER JOIN
						Warehouses
					ON
						WarehouseItems.numWareHouseID = Warehouses.numWareHouseID
					WHERE
						WarehouseItems.numDomainID=@numDomainID
						AND WarehouseItems.numItemID=@numItemCode
					GROUP BY
						Warehouses.numWareHouseID
						,Warehouses.vcWareHouse
				) TEMP
				WHERE
					1 = (CASE WHEN @tintWarehouseAvailability=2 THEN (CASE WHEN Temp.numOnHand > 0 THEN 1 ELSE 0 END) ELSE 1 END)
				ORDER BY
					vcWareHouse
				FOR XML PATH('')),1,1,'')
	END	
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
            
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_Item_GetEcommerceWarehouseInTransit')
DROP PROCEDURE USP_Item_GetEcommerceWarehouseInTransit
GO
CREATE PROCEDURE [dbo].[USP_Item_GetEcommerceWarehouseInTransit]    
(
	@numDomainID NUMERIC(18,0)
	,@numItemCode NUMERIC(18,0)
)
AS
BEGIN
	SELECT 
		FORMAT(ISNULL(SUM(numOnOrder),0), '#,##0')
	FROM
		WarehouseItems
	INNER JOIN
		Warehouses
	ON
		WarehouseItems.numWareHouseID = Warehouses.numWareHouseID
	WHERE
		WarehouseItems.numDomainID=@numDomainID
		AND WarehouseItems.numItemID=@numItemCode
END
GO
/****** Object:  StoredProcedure [dbo].[USP_ItemList1]    Script Date: 05/07/2009 18:14:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Created By Anoop Jayaraj                                        
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'usp_itemlist1' ) 
    DROP PROCEDURE usp_itemlist1
GO
CREATE PROCEDURE [dbo].[USP_ItemList1]
    @ItemClassification AS NUMERIC(9) = 0,
    @IsKit AS TINYINT,
    @SortChar CHAR(1) = '0',
    @CurrentPage INT,
    @PageSize INT,
    @TotRecs INT OUTPUT, 
    @columnName AS VARCHAR(50),
    @columnSortOrder AS VARCHAR(10),
    @numDomainID AS NUMERIC(18,0) = 0,
    @ItemType AS CHAR(1),
    @bitSerialized AS BIT,
    @numItemGroup AS NUMERIC(9),
    @bitAssembly AS BIT,
    @numUserCntID AS NUMERIC(18,0),
    @Where	AS VARCHAR(MAX),
    @IsArchive AS BIT = 0,
    @byteMode AS TINYINT,
	@bitAsset as BIT=0,
	@bitRental as BIT=0,
	@bitContainer AS BIT=0,
	@SearchText VARCHAR(300),
	@vcRegularSearchCriteria varchar(MAX)='',
	@vcCustomSearchCriteria varchar(MAX)='',
	@tintDashboardReminderType TINYINT = 0,
	@bitMultiSelect AS TINYINT,
	@numFilteredWarehouseID AS NUMERIC(18,0)=0,
	@vcAsile AS VARCHAR(500)='',
	@vcRack AS VARCHAR(500)='',
	@vcShelf AS VARCHAR(500)='',
	@vcBin AS VARCHAR(500)='',
	@bitApplyFilter As BIT=0,
	@tintUserRightType TINYINT
AS 
BEGIN	
	DECLARE @numWarehouseID NUMERIC(18,0)
	SELECT TOP 1 @numWarehouseID = numWareHouseID FROM Warehouses WHERE numDomainID = @numDomainID

	SET @vcCustomSearchCriteria = REPLACE(@vcCustomSearchCriteria,'ctl00_ctl00_MainContent_GridPlaceHolder_gvSearch_ctl00_ctl02_ctl00_','')

	CREATE TABLE #tempForm 
	(
		tintOrder TINYINT,vcDbColumnName nvarchar(50),vcOrigDbColumnName nvarchar(50),vcFieldName nvarchar(50),vcAssociatedControlType nvarchar(50),vcListItemType varchar(3),
		numListID numeric(9),vcLookBackTableName varchar(50),bitCustomField BIT,numFieldId NUMERIC,bitAllowSorting BIT,bitAllowEdit BIT,bitIsRequired BIT,bitIsEmail BIT,
		bitIsAlphaNumeric BIT,bitIsNumeric BIT,bitIsLengthValidation BIT,intMaxLength int,intMinLength int,bitFieldMessage BIT,vcFieldMessage VARCHAR(500),
		ListRelID numeric(9),intColumnWidth int,bitAllowFiltering BIT,vcFieldDataType CHAR(1),Grp_Id TINYINT
	)

	-- FIRST CHECK IF GRID COLUMNS ARE CONFIGURED
	DECLARE  @Nocolumns  AS TINYINT = 0

	SELECT 
		@Nocolumns=ISNULL(SUM(TotalRow),0) 
	FROM
	(            
		SELECT 
			COUNT(*) TotalRow 
		FROM 
			View_DynamicColumns 
		WHERE 
			numFormId=21 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND numRelCntType = 0
		UNION 
		SELECT 
			COUNT(*) TotalRow 
		FROM 
			View_DynamicCustomColumns 
		WHERE 
			numFormId=21 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND numRelCntType = 0
	) TotalRows

	-- IF GRID COLUMNS ARE NOT CONFIGURES THEN CHECK IF MASTER FORM CONFIGURATION IS CREATED BY ADMINISTRATOR
	DECLARE @IsMasterConfAvailable BIT = 0
	DECLARE @numUserGroup NUMERIC(18,0)

	IF @Nocolumns=0
	BEGIN
		SELECT @numUserGroup = numGroupID FROM UserMaster WHERE numUserDetailId = @numUserCntID 

		IF (SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = @numDomainID AND numFormID = 21 AND numRelCntType=0 AND bitGridConfiguration = 1 AND numGroupID = @numUserGroup) > 0
		BEGIN
			SET @IsMasterConfAvailable = 1
		END
	END

	--IF MASTERCONFIGURATION IS AVAILABLE THEN LOAD IT OTHERWISE LOAD DEFAULT COLUMNS
	IF @IsMasterConfAvailable = 1
	BEGIN
		INSERT INTO DycFormConfigurationDetails 
		(
			numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth
		)
		SELECT 
			21,numFieldId,intColumnNum,intRowNum,@numDomainID,@numUserCntID,0,1,0,intColumnWidth
		FROM 
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId=21 AND 
			View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicColumnsMasterConfig.numRelCntType=0 AND 
			View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
			ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
		UNION
		SELECT 
			21,numFieldId,intColumnNum,intRowNum,@numDomainID,@numUserCntID,0,1,1,intColumnWidth
		FROM 
			View_DynamicCustomColumnsMasterConfig
		WHERE 
			View_DynamicCustomColumnsMasterConfig.numFormId=21 AND 
			View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicCustomColumnsMasterConfig.numRelCntType=0 AND 
			View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1

		INSERT INTO 
			#tempForm
		SELECT 
			(intRowNum + 1) as tintOrder, vcDbColumnName,	vcOrigDbColumnName,	ISNULL(vcCultureFieldName,vcFieldName), vcAssociatedControlType,
			vcListItemType, numListID, vcLookBackTableName,	bitCustom, numFieldId, bitAllowSorting,	bitInlineEdit, bitIsRequired,
			bitIsEmail,	bitIsAlphaNumeric, bitIsNumeric, bitIsLengthValidation,	intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,
			ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType,0
		FROM 
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId=21 AND 
			View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicColumnsMasterConfig.numRelCntType=0 AND 
			View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
			ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
		UNION
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName, vcAssociatedControlType,'' as vcListItemType,numListID,'',
			bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,'',Grp_Id
		FROM 
			View_DynamicCustomColumnsMasterConfig
		WHERE 
			View_DynamicCustomColumnsMasterConfig.numFormId=21 AND 
			View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicCustomColumnsMasterConfig.numRelCntType=0 AND 
			View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1
		ORDER BY 
			tintOrder ASC  
	END
	ELSE                                        
	BEGIN
		IF @Nocolumns=0
		BEGIN
			INSERT INTO DycFormConfigurationDetails 
			(
				numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth
			)
			SELECT 
				21,numFieldId,0,Row_number() over(order by tintRow desc),@numDomainID,@numUserCntID,0,1,0,intColumnWidth
			FROM 
				View_DynamicDefaultColumns
			WHERE 
				numFormId=21 
				AND bitDefault=1 
				AND ISNULL(bitSettingField,0)=1 
				AND numDomainID=@numDomainID
			ORDER BY 
				tintOrder asc   
		END

		INSERT INTO 
			#tempForm
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcOrigDbColumnName,ISNULL(vcCultureFieldName,vcFieldName),                  
			vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName,bitCustom,numFieldId,bitAllowSorting,bitInlineEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength,bitFieldMessage,
			vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType,0
		FROM 
			View_DynamicColumns 
		WHERE 
			numFormId=21 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND ISNULL(bitSettingField,0)=1 
			AND ISNULL(bitCustom,0)=0  
			AND numRelCntType = 0
		UNION
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName,vcAssociatedControlType,'' as vcListItemType,numListID,'',bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,'',Grp_Id
		FROM 
			View_DynamicCustomColumns
		WHERE 
			numFormId=21 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND ISNULL(bitCustom,0)=1 
			AND numRelCntType = 0
		ORDER BY 
			tintOrder asc  
	END   

	IF @byteMode=0
	BEGIN
		DECLARE @strColumns AS VARCHAR(MAX)
		SET @strColumns = ' I.numItemCode,I.vcItemName,ISNULL(DivisionMaster.numDivisionID,0) numDivisionID,ISNULL(DivisionMaster.tintCRMType,0) tintCRMType, ' + CAST(ISNULL(@numWarehouseID,0) AS VARCHAR) + ' AS numWarehouseID, I.numDomainID,0 AS numRecOwner,0 AS numTerID, I.charItemType,I.bitSerialized,I.bitLotNo, I.numItemCode AS [numItemCode~211~0]'

		DECLARE @tintOrder AS TINYINT                                                  
		DECLARE @vcFieldName AS VARCHAR(50)                                                  
		DECLARE @vcListItemType AS VARCHAR(3)                                             
		DECLARE @vcListItemType1 AS VARCHAR(1)                                                 
		DECLARE @vcAssociatedControlType VARCHAR(10)                                                  
		DECLARE @numListID AS NUMERIC(9)                                                  
		DECLARE @vcDbColumnName VARCHAR(50)                      
		DECLARE @WhereCondition VARCHAR(MAX)                       
		DECLARE @vcLookBackTableName VARCHAR(2000)                
		DECLARE @bitCustom AS BIT                  
		DECLARE @numFieldId AS NUMERIC  
		DECLARE @bitAllowSorting AS CHAR(1)   
		DECLARE @bitAllowEdit AS CHAR(1)                   
        DECLARE @vcColumnName AS VARCHAR(200)
		DECLARE @Grp_ID AS TINYINT
		SET @tintOrder = 0                                                  
		SET @WhereCondition = ''                 
                   
		DECLARE @ListRelID AS NUMERIC(9) 
		DECLARE @SearchQuery AS VARCHAR(MAX) = ''

		IF LEN(@SearchText) > 0
		BEGIN
			SET @SearchQuery = ' I.numItemCode LIKE ''%' + @SearchText + '%'' OR I.vcItemName LIKE ''%' + @SearchText + '%'' OR I.vcModelID LIKE ''%' + @SearchText + '%'' OR I.vcSKU LIKE ''%' + @SearchText + '%'' OR cmp.vcCompanyName LIKE ''%' + @SearchText + '%'' OR I.vcManufacturer LIKE ''%' + @SearchText + '%'''
		END


		DECLARE @j INT = 0
  
		SELECT TOP 1
				@tintOrder = tintOrder + 1,
				@vcDbColumnName = vcDbColumnName,
				@vcFieldName = vcFieldName,
				@vcAssociatedControlType = vcAssociatedControlType,
				@vcListItemType = vcListItemType,
				@numListID = numListID,
				@vcLookBackTableName = vcLookBackTableName,
				@bitCustom = bitCustomField,
				@numFieldId = numFieldId,
				@bitAllowSorting = bitAllowSorting,
				@bitAllowEdit = bitAllowEdit,
				@ListRelID = ListRelID
		FROM    #tempForm --WHERE bitCustomField=1
		ORDER BY tintOrder ASC        
		IF((SELECT COUNt(*) FROM #tempForm WHERE vcDbColumnName='StockQtyCount'  OR  vcDbColumnName='StockQtyAdjust')  >0)
		BEGIN
		IF ISNULL(@numFilteredWarehouseID,0)=0
		BEGIN
			SET @strColumns = @strColumns+ ',0 as numIndividualOnHandQty,0 as numAvailabilityQty,0 AS monAverageCost,0 as numWareHouseItemId,0 AS numAssetChartAcntId'
		END   
		ELSE IF ISNULL(@numFilteredWarehouseID,0)>0
		BEGIN
			SET @strColumns = @strColumns+ ',WarehouseItems.numOnHand as numIndividualOnHandQty,(WarehouseItems.numOnHand + WarehouseItems.numAllocation) as numAvailabilityQty,I.monAverageCost,WarehouseItems.numWareHouseItemId as numWareHouseItemId,I.numAssetChartAcntId,
			((isnull(WarehouseItems.numOnHand,0) + isnull(WarehouseItems.numAllocation,0))*isnull(I.monAverageCost ,0)) as StockValue'
		END  
		END
		WHILE @tintOrder > 0                                                  
		BEGIN
			IF @bitCustom = 0
			BEGIN
				DECLARE  @Prefix  AS VARCHAR(5)

				IF @vcLookBackTableName = 'Item'
					SET @Prefix = 'I.'
				ELSE IF @vcLookBackTableName = 'CompanyInfo'
					SET @Prefix = 'cmp.'
				ELSE IF @vcLookBackTableName = 'Vendor'
					SET @Prefix = 'V.'

				SET @vcColumnName= @vcDbColumnName+'~'+ convert(varchar(10),@numFieldId) +'~0'

				IF @vcAssociatedControlType='SelectBox' 
				BEGIN
					IF @vcListItemType='LI' 
					BEGIN
						SET @strColumns = @strColumns+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName+']'
						SET @WhereCondition= @WhereCondition +' LEFT JOIN ListDetails L'+ convert(varchar(3),@tintOrder)+ ' ON L'+convert(varchar(3),@tintOrder)+ '.numListItemID='+@vcDbColumnName                                                      
					END
					ELSE IF @vcListItemType = 'UOM'
					BEGIN
						SET @strColumns = @strColumns + ',ISNULL((SELECT vcUnitName FROM UOM WHERE numUOMId = I.' + @vcDbColumnName + '),'''')' + ' ['+ @vcColumnName+']'
					END
					ELSE IF @vcListItemType = 'IG'
					BEGIN
						SET @strColumns = @strColumns+ ',ISNULL((SELECT vcItemGroup FROM ItemGroups WHERE numItemGroupID = I.numItemGroup),'''')' + ' ['+ @vcColumnName+']'  
					END
				END
				ELSE IF @vcDbColumnName = 'vcPathForTImage'
		   		BEGIN
					SET @strColumns = @strColumns + ',ISNULL(II.vcPathForTImage,'''') AS ' + ' ['+ @vcColumnName+']'                  
					SET @WhereCondition = @WhereCondition + ' LEFT JOIN ItemImages II ON II.numItemCode = I.numItemCode AND bitDefault = 1'
				END
				ELSE IF @vcLookBackTableName = 'Item' AND @vcDbColumnName = 'monListPrice'
				BEGIN
					SET @strColumns = @strColumns + ',CASE WHEN I.charItemType=''P'' THEN ISNULL((SELECT TOP 1 monWListPrice FROM WarehouseItems WHERE numDomainID=I.numDomainID AND numItemID=I.numItemCode AND monWListPrice > 0),0.00) ELSE ISNULL(I.monListPrice,0.00) END AS ' + ' ['+ @vcColumnName+']'
				END
				ELSE IF @vcLookBackTableName = 'Item' AND @vcDbColumnName = 'ItemType'
				BEGIN
					SET @strColumns = @strColumns + ',' + '(CASE 
																WHEN I.charItemType=''P'' 
																THEN 
																	CASE 
																		WHEN ISNULL(I.bitAssembly,0) = 1 THEN ''Assembly''
																		WHEN ISNULL(I.bitKitParent,0) = 1 THEN ''Kit''
																		WHEN ISNULL(I.bitAsset,0) = 1 AND ISNULL(I.bitRental,0)=0 THEN ''Asset''
																		WHEN ISNULL(I.bitAsset,0) = 1 AND ISNULL(I.bitRental,0)=1 THEN ''Rental Asset''
																		WHEN ISNULL(I.bitSerialized,0) = 1 AND ISNULL(I.bitAsset,0) = 0  THEN ''Serialized''
																		WHEN ISNULL(I.bitSerialized,0) = 1 AND ISNULL(I.bitAsset,0) = 1 AND ISNULL(I.bitRental,0)=0 THEN ''Serialized Asset''
																		WHEN ISNULL(I.bitSerialized,0) = 1 AND ISNULL(I.bitAsset,0) = 1 AND ISNULL(I.bitRental,0)=1 THEN ''Serialized Rental Asset''
																		WHEN ISNULL(I.bitLotNo,0)=1 THEN ''Lot #''
																		ELSE ''Inventory Item'' 
																	END
																WHEN I.charItemType=''N'' THEN ''Non Inventory Item'' 
																WHEN I.charItemType=''S'' THEN ''Service'' 
																WHEN I.charItemType=''A'' THEN ''Accessory'' 
															END)' + ' AS ' + ' [' + @vcColumnName + ']'  
				END
				ELSE IF @vcLookBackTableName = 'Item' AND @vcDbColumnName = 'monStockValue'
				BEGIN
					SET @strColumns = @strColumns+',WarehouseItems.monStockValue'+' ['+ @vcColumnName+']'                                                      
				END
				ELSE IF @vcLookBackTableName = 'Item' AND @vcDbColumnName = 'StockQtyCount'
				BEGIN
					SET @strColumns = @strColumns+',0'+' ['+ @vcColumnName+']'                                                      
				END
				ELSE IF @vcLookBackTableName = 'Item' AND @vcDbColumnName = 'StockQtyAdjust'
				BEGIN
					SET @strColumns = @strColumns+',0'+' ['+ @vcColumnName+']'     
					                                             
				END
				ELSE IF @vcLookBackTableName = 'WareHouseItems' AND @vcDbColumnName = 'vcLocation' AND ISNULL(@numFilteredWarehouseID,0)=0
				BEGIN
					SET @strColumns = @strColumns + ', (SELECT STUFF((SELECT CONCAT('', '', WIL.vcLocation) FROM WareHouseItems WI  LEFT JOIN WarehouseLocation AS WIL ON WI.numWLocationId=WIL.numWLocationId WHERE WI.numItemID=I.numItemCode FOR XML PATH('''')), 1, 1, ''''))' + ' AS ' + ' [' + @vcColumnName + '] '
				END
				ELSE IF @vcLookBackTableName = 'WareHouseItems' AND @vcDbColumnName = 'vcLocation' AND ISNULL(@numFilteredWarehouseID,0)>0
				BEGIN
					SET @strColumns = @strColumns + ', WFL.vcLocation AS ' + ' [' + @vcColumnName + '] '
				END
				ELSE IF @vcLookBackTableName = 'Warehouses' AND @vcDbColumnName = 'vcWareHouse'
				BEGIN
					SET @strColumns = @strColumns + ', (SELECT STUFF((SELECT CONCAT('', '', vcWareHouse) FROM WareHouseItems WI JOIN Warehouses W ON WI.numWareHouseID=W.numWareHouseID WHERE WI.numItemID=I.numItemCode FOR XML PATH('''')), 1, 1, ''''))' + ' AS ' + ' [' + @vcColumnName + ']'
				END
				ELSE IF @vcLookBackTableName = 'WorkOrder' AND @vcDbColumnName = 'WorkOrder'
				BEGIN
					SET @strColumns = @strColumns + ',(CASE WHEN (SELECT SUM(WO.numQtyItemsReq) from WorkOrder WO where WO.numItemCode=I.numItemCode and WO.numWOStatus=0) > 0 THEN 1 ELSE 0 END)' + ' AS ' + ' [' + @vcColumnName + ']'
				END
				ELSE IF @vcDbColumnName = 'vcPriceLevelDetail' 
				BEGIN
					SELECT @strColumns = @strColumns + CONCAT(', dbo.fn_GetItemPriceLevelDetail( ',@numDomainID,',I.numItemCode,',@numWarehouseID,')') +' ['+ @vcColumnName+']' 
				END
				ELSE
				BEGIN
					If @vcDbColumnName <> 'numItemCode' AND @vcDbColumnName <> 'vcPriceLevelDetail' --WE AE ALREADY ADDING COLUMN IN SELECT CLUASE DIRECTLY
					BEGIN
						SET @strColumns = @strColumns + ',' + (CASE @vcLookBackTableName WHEN 'Item' THEN 'I' WHEN 'CompanyInfo' THEN 'cmp' WHEN 'Vendor' THEN 'V' ELSE @vcLookBackTableName END) + '.' + @vcDbColumnName + ' AS ' + ' [' + @vcColumnName + ']' 
					END
				END
			END                              
			ELSE IF @bitCustom = 1 
			BEGIN
				
				SET @vcColumnName= @vcDbColumnName +'~'+ convert(varchar(10),@numFieldId)+'~1'

				SELECT 
					@vcFieldName = FLd_label,
					@vcAssociatedControlType = fld_type,
					@vcDbColumnName = 'Cust' + CONVERT(VARCHAR(10), Fld_Id),
					@Grp_ID = Grp_id
				FROM 
					CFW_Fld_Master
				WHERE 
					CFW_Fld_Master.Fld_Id = @numFieldId                 
        
				IF @Grp_ID = 9
				BEGIN
					SET @strColumns = @strColumns + ',L' + CONVERT(VARCHAR(3), @tintOrder) + '.vcData' + ' [' + @vcColumnName + ']'     
             
					SET @WhereCondition = @WhereCondition + ' left Join ItemAttributes CFW' + CONVERT(VARCHAR(3), @tintOrder)
									+ ' ON CFW' + CONVERT(VARCHAR(3), @tintOrder) + '.numDOmainID=' + CAST(@numDomainID AS VARCHAR) + ' AND CFW' + CONVERT(VARCHAR(3), @tintOrder) + '.Fld_Id='
									+ CONVERT(VARCHAR(10), @numFieldId)
									+ 'and CFW' + CONVERT(VARCHAR(3), @tintOrder)
									+ '.numItemCode=I.numItemCode   '  
					SET @WhereCondition = @WhereCondition
						+ ' left Join ListDetails L'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ ' on L'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ '.numListItemID=CFW'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ '.Fld_Value'      
				END
				ELSE IF @vcAssociatedControlType = 'TextBox' OR @vcAssociatedControlType = 'TextArea' 
				BEGIN
					SET @strColumns = @strColumns + ',CFW'  + CONVERT(VARCHAR(3), @tintOrder)  + '.Fld_Value  [' + @vcColumnName + ']'     
             
					SET @WhereCondition = @WhereCondition + ' left Join CFW_FLD_Values_Item CFW' + CONVERT(VARCHAR(3), @tintOrder)
									+ ' ON CFW' + CONVERT(VARCHAR(3), @tintOrder) + '.Fld_Id='
									+ CONVERT(VARCHAR(10), @numFieldId)
									+ 'and CFW' + CONVERT(VARCHAR(3), @tintOrder)
									+ '.RecId=I.numItemCode   '                                                         
				END   
				ELSE IF @vcAssociatedControlType = 'CheckBox' 
				BEGIN
					SET @strColumns = @strColumns
						+ ',case when isnull(CFW'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ '.Fld_Value,0)=0 then 0 when isnull(CFW'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ '.Fld_Value,0)=1 then 1 end   ['
						+  @vcColumnName
						+ ']'               
 
					SET @WhereCondition = @WhereCondition
						+ ' left Join CFW_FLD_Values_Item CFW'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ ' ON CFW' + CONVERT(VARCHAR(3), @tintOrder) + '.Fld_Id='
						+ CONVERT(VARCHAR(10), @numFieldId)
						+ ' and CFW'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ '.RecId=I.numItemCode   '                                                     
				END                
				ELSE IF @vcAssociatedControlType = 'DateField' 
				BEGIN
					SET @strColumns = @strColumns
						+ ',dbo.FormatedDateFromDate(CFW'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ '.Fld_Value,'
						+ CONVERT(VARCHAR(10), @numDomainId)
						+ ')  [' +@vcColumnName + ']'    
					                  
					SET @WhereCondition = @WhereCondition
						+ ' left Join CFW_FLD_Values_Item CFW'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ ' on CFW' + CONVERT(VARCHAR(3), @tintOrder) + '.Fld_Id='
						+ CONVERT(VARCHAR(10), @numFieldId)
						+ ' and CFW'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ '.RecId=I.numItemCode   '                                                         
				END                
				ELSE IF @vcAssociatedControlType = 'SelectBox' 
				BEGIN
					SET @vcDbColumnName = 'Cust' + CONVERT(VARCHAR(10), @numFieldId)

					SET @strColumns = @strColumns + ',L'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ '.vcData' + ' [' + @vcColumnName + ']'          
					                                                    
					SET @WhereCondition = @WhereCondition
						+ ' left Join CFW_FLD_Values_Item CFW'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ ' on CFW' + CONVERT(VARCHAR(3), @tintOrder) + '.Fld_Id='
						+ CONVERT(VARCHAR(10), @numFieldId)
						+ ' and CFW'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ '.RecId=I.numItemCode    '     
					                                                    
					SET @WhereCondition = @WhereCondition
						+ ' left Join ListDetails L'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ ' on L'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ '.numListItemID=CFW'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ '.Fld_Value'                
				END
				ELSE
				BEGIN
					SET @strColumns = @strColumns + CONCAT(',dbo.GetCustFldValueItem(',@numFieldId,',I.numItemCode)') + ' [' + @vcColumnName + ']'
				END                
			END        
  
			

			SELECT TOP 1
					@tintOrder = tintOrder + 1,
					@vcDbColumnName = vcDbColumnName,
					@vcFieldName = vcFieldName,
					@vcAssociatedControlType = vcAssociatedControlType,
					@vcListItemType = vcListItemType,
					@numListID = numListID,
					@vcLookBackTableName = vcLookBackTableName,
					@bitCustom = bitCustomField,
					@numFieldId = numFieldId,
					@bitAllowSorting = bitAllowSorting,
					@bitAllowEdit = bitAllowEdit,
					@ListRelID = ListRelID
			FROM    #tempForm
			WHERE   tintOrder > @tintOrder - 1 --AND bitCustomField=1
			ORDER BY tintOrder ASC            
 
			IF @@rowcount = 0 
				SET @tintOrder = 0 
		END  
		
		--DECLARE @vcApplyRackFilter as VARCHAR(MAX)=''
		--DECLARE @vcApplyRackFilterSearch as VARCHAR(MAX)=''
		--IF(ISNULL(@numFilteredWarehouseID,0)>0)
		--BEGIN
		--	SET @vcApplyRackFilter=' LEFT JOIN WareHouseItems AS WRF ON WRF.numItemId=I.numItemCode LEFT JOIN WarehouseLocation AS WFL ON WRF.numWLocationId=WFL.numWLocationId AND WRF.numWareHouseId=WFL.numWareHouseId '
		--	SET @vcApplyRackFilterSearch=@vcApplyRackFilterSearch + ' AND WRF.numWareHouseId='''+CAST(@numFilteredWarehouseID AS VARCHAR(500))+''' '
		--	SET @vcApplyRackFilterSearch=@vcApplyRackFilterSearch + ' AND WFL.numWareHouseId='''+CAST(@numFilteredWarehouseID AS VARCHAR(500))+''' '
		--	IF(ISNULL(@vcAsile,'')<>'')
		--	BEGIN
		--		SET @vcApplyRackFilterSearch=@vcApplyRackFilterSearch + ' AND WFL.vcAisle='''+CAST(@vcAsile AS VARCHAR(500))+''' '
		--	END
		--	IF(ISNULL(@vcRack,'')<>'')
		--	BEGIN
		--		SET @vcApplyRackFilterSearch=@vcApplyRackFilterSearch + ' AND WFL.vcRack='''+CAST(@vcRack AS VARCHAR(500))+''' '
		--	END
		--	IF(ISNULL(@vcShelf,'')<>'')
		--	BEGIN
		--		SET @vcApplyRackFilterSearch=@vcApplyRackFilterSearch + ' AND WFL.vcShelf='''+CAST(@vcShelf AS VARCHAR(500))+''' '
		--	END
		--	IF(ISNULL(@vcBin,'')<>'')
		--	BEGIN
		--		SET @vcApplyRackFilterSearch=@vcApplyRackFilterSearch + ' AND WFL.vcBin='''+CAST(@vcBin AS VARCHAR(500))+''' '
		--	END
		--END
		
		DECLARE @strSQL AS VARCHAR(MAX)
		SET @strSQL = CONCAT(' FROM 
							Item I
						LEFT JOIN
							Vendor V
						ON
							I.numItemCode = V.numItemCode
							AND I.numVendorID = V.numVendorID
						LEFT JOIN
							DivisionMaster
						ON
							V.numVendorID = DivisionMaster.numDivisionID
						LEFT JOIN
							CompanyInfo cmp
						ON
							DivisionMaster.numCompanyId = cmp.numCompanyId
						LEFT JOIN
							CustomerPartNumber
						ON
							CustomerPartNumber.numItemCode = I.numItemCode AND CustomerPartNumber.numCompanyId = 0
						',(CASE 
								WHEN ISNULL(@numFilteredWarehouseID,0) > 0 
								THEN CONCAT(' CROSS APPLY (
											SELECT
												WareHouseItems.numWareHouseItemID,
												numOnHand AS numOnHand,
												ISNULL(numOnHand,0) + ISNULL(numAllocation,0) AS numAvailable,
												numBackOrder AS numBackOrder,
												numOnOrder AS numOnOrder,
												numAllocation AS numAllocation,
												numReorder AS numReorder,
												numOnHand * (CASE WHEN ISNULL(I.bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(I.monAverageCost,0) END) AS monStockValue,
												WarehouseLocation.vcLocation
											FROM
												WareHouseItems
											LEFT JOIN 
												WarehouseLocation 
											ON 
												WareHouseItems.numWLocationId=WarehouseLocation.numWLocationId
											WHERE
												numItemID = I.numItemCode
												AND (WareHouseItems.numWareHouseID=',ISNULL(@numFilteredWarehouseID,0),' OR ',ISNULL(@numFilteredWarehouseID,0),'=0)'
												,CASE WHEN ISNULL(@vcAsile,'') <> '' THEN CONCAT(' AND WarehouseLocation.vcAisle=''',@vcAsile+'''') ELSE '' END  
												,CASE WHEN ISNULL(@vcRack,'') <> '' THEN CONCAT(' AND WarehouseLocation.vcRack=''',@vcRack +'''') ELSE '' END  
												,CASE WHEN ISNULL(@vcShelf,'') <> '' THEN CONCAT(' AND WarehouseLocation.vcShelf=''',@vcShelf+'''') ELSE '' END  
												,CASE WHEN ISNULL(@vcBin,'') <> '' THEN CONCAT(' AND WarehouseLocation.vcBin=''',@vcBin+'''') ELSE '' END  
										,') WarehouseItems ')
								ELSE ' OUTER APPLY (
							SELECT
								SUM(numOnHand) AS numOnHand,
								SUM(ISNULL(numOnHand,0) + ISNULL(numAllocation,0)) AS numAvailable,
								SUM(numBackOrder) AS numBackOrder,
								SUM(numOnOrder) AS numOnOrder,
								SUM(numAllocation) AS numAllocation,
								SUM(numReorder) AS numReorder,
								SUM(numOnHand) * (CASE WHEN ISNULL(I.bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(I.monAverageCost,0) END) AS monStockValue
							FROM
								WareHouseItems
							WHERE
								numItemID = I.numItemCode) WarehouseItems ' 
							END),'
						  ', @WhereCondition)

		IF @columnName = 'Backorder' OR @columnName = 'WHI.numBackOrder'
			SET @columnName = 'numBackOrder'
		ELSE IF @columnName = 'OnOrder' OR @columnName = 'WHI.numOnOrder' 
			SET @columnName = 'numOnOrder'
		ELSE IF @columnName = 'OnAllocation' OR @columnName = 'WHI.numAllocation' 
			SET @columnName = 'numAllocation'
		ELSE IF @columnName = 'Reorder' OR @columnName = 'WHI.numReorder' 
			SET @columnName = 'numReorder'
		ELSE IF @columnName = 'WHI.numOnHand'
			SET @columnName = 'numOnHand'
		ELSE IF @columnName = 'WHI.numAvailable'
			SET @columnName = 'numAvailable'
		ELSE IF @columnName = 'monStockValue' 
			SET @columnName = 'sum(numOnHand) * (CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(monAverageCost,0) END)'
		ELSE IF @columnName = 'ItemType' 
			SET @columnName = 'charItemType'
		ELSE IF @columnName = 'numItemCode' 
			SET @columnName = 'I.numItemCode'

		DECLARE @column AS VARCHAR(50)              
		SET @column = @columnName
	         
		IF @columnName LIKE '%Cust%' 
		BEGIN
			DECLARE @CustomFieldType AS VARCHAR(10)
			DECLARE @fldId AS VARCHAR(10)
			
			IF CHARINDEX('CFW.Cust',@columnName) > 0
			BEGIN
				SET @fldId = REPLACE(@columnName, 'CFW.Cust', '')
			END
			ELSE
			BEGIN
				SET @fldId = REPLACE(@columnName, 'Cust', '')
			END
			SELECT TOP 1 @CustomFieldType = ISNULL(vcAssociatedControlType,'') FROM View_DynamicCustomColumns WHERE numFieldID = @fldId
            
			IF ISNULL(@CustomFieldType,'') = 'SelectBox' OR ISNULL(@CustomFieldType,'') = 'ListBox'
			BEGIN
				SET @strSQL = @strSQL + ' left Join CFW_FLD_Values_Item CFW on CFW.RecId=i.numItemCode and CFW.fld_id= ' + @fldId + ' ' 
				SET @strSQL = @strSQL + ' left Join ListDetails LstCF on LstCF.numListItemID = CFW.Fld_Value  '            
				SET @columnName = ' LstCF.vcData ' 	
			END
			ELSE
			BEGIN
				SET @strSQL =  @strSQL + ' left Join CFW_FLD_Values_Item CFW on CFW.RecId=i.numItemCode and CFW.fld_id= ' + @fldId + ' '                                                         
				SET @columnName = 'CFW.Fld_Value'                	
			END
		END                                      
		DECLARE @strWhere AS VARCHAR(8000)
		SET @strWhere = CONCAT(' WHERE I.numDomainID = ',@numDomainID)
    
		IF @ItemType = 'P'
		BEGIN
			SET @strWhere = @strWhere + ' AND I.charItemType= ''P''' 
		END
		ELSE IF @ItemType = 'S'
		BEGIN
			SET @strWhere = @strWhere + ' AND I.charItemType= ''S'''
		END
		ELSE IF @ItemType = 'N'    
		BEGIN
			SET @strWhere = @strWhere + ' AND I.charItemType= ''N'''
		END
	    
		IF @bitContainer= '1'
		BEGIN
			SET @strWhere = @strWhere + ' AND I.bitContainer= ' + CONVERT(VARCHAR(2), @bitContainer) + ''  
		END

		IF @bitAssembly = '1' 
		BEGIN
			SET @strWhere = @strWhere + ' AND I.bitAssembly	= ' + CONVERT(VARCHAR(2), @bitAssembly) + ''  
		END 
		ELSE IF @ItemType = 'P'
		BEGIN
			SET @strWhere = @strWhere + ' AND ISNULL(I.bitAssembly,0)=0 '
		END

		IF @IsKit = '1' 
		BEGIN
			SET @strWhere = @strWhere + ' AND ISNULL(I.bitAssembly,0)=0 AND I.bitKitParent= ' + CONVERT(VARCHAR(2), @IsKit) + ''  
		END           
		ELSE IF @ItemType = 'P'
		BEGIN
			SET @strWhere = @strWhere + ' AND ISNULL(I.bitKitParent,0)=0 '
		END

		IF @bitSerialized = 1 
		BEGIN
			SET @strWhere = @strWhere + ' AND (I.bitSerialized=1 OR I.bitLotNo=1)'
		END
		--ELSE IF @ItemType = 'P'
		--BEGIN
		--	SET @strWhere = @strWhere + ' AND ISNULL(Item.bitSerialized,0)=0 AND ISNULL(Item.bitLotNo,0)=0 '
		--END

		IF @bitAsset = 1 
		BEGIN
			SET @strWhere = @strWhere + ' AND ISNULL(I.bitAsset,0) = 1'
		END
		ELSE IF @ItemType = 'P'
		BEGIN
			SET @strWhere = @strWhere + ' AND ISNULL(I.bitAsset,0) = 0 '
		END

		IF @bitRental = 1
		BEGIN
			SET @strWhere = @strWhere + ' AND ISNULL(I.bitRental,0) = 1 AND ISNULL(I.bitAsset,0) = 1 '
		END
		ELSE IF @ItemType = 'P'
		BEGIN
			SET @strWhere = @strWhere + ' AND ISNULL(I.bitRental,0) = 0 '
		END

		IF @SortChar <> '0' 
		BEGIN
			SET @strWhere = @strWhere + ' AND I.vcItemName like ''' + @SortChar + '%'''                                       
		END

		IF @ItemClassification <> '0' 
		BEGIN
			SET @strWhere = @strWhere + ' AND I.numItemClassification=' + CONVERT(VARCHAR(15), @ItemClassification)    
		END

		IF @IsArchive = 0 
		BEGIN
			SET @strWhere = @strWhere + ' AND ISNULL(I.IsArchieve,0) = 0'       
		END
        
		IF @numItemGroup > 0 
		BEGIN
			SET @strWhere = @strWhere + ' AND I.numItemGroup = ' + CONVERT(VARCHAR(20), @numItemGroup)  
        END  
		
		IF @numItemGroup = -1 
		BEGIN
			SET @strWhere = @strWhere + ' AND I.numItemGroup in (select numItemGroupID from ItemGroups where numDomainID=' + CONVERT(VARCHAR(20), @numDomainID) + ')'                                      
        END

		IF @bitMultiSelect = '1'
		BEGIN
			SET  @strWhere = @strWhere + 'AND ISNULL(I.bitKitParent,0)=0 '
		END

		IF @vcRegularSearchCriteria<>'' 
		BEGIN
			SET @strWhere = @strWhere +' AND ' + @vcRegularSearchCriteria 
		END
	
		IF @vcCustomSearchCriteria<>'' 
		BEGIN 
		--comment
			SET @strWhere = @strWhere + ' AND ' + @vcCustomSearchCriteria  		END
		
		IF LEN(@SearchQuery) > 0
		BEGIN
			SET @strWhere = @strWhere + ' AND (' + @SearchQuery + ') '
		END
		
		SET @strWhere = @strWhere + ISNULL(@Where,'')

		IF ISNULL(@tintDashboardReminderType,0) = 17
		BEGIN
			SET @strWhere = CONCAT(@strWhere,' AND I.numItemCode IN (','SELECT DISTINCT
																		numItemID
																	FROM
																		WareHouseItems WIInner
																	INNER JOIN
																		Item IInner
																	ON
																		WIInner.numItemID = IInner.numItemCode
																	WHERE
																		WIInner.numDomainID=',@numDomainID,'
																		AND IInner.numDomainID = ',@numDomainID,'
																		AND ISNULL(IInner.bitArchiveItem,0) = 0
																		AND ISNULL(IInner.IsArchieve,0) = 0
																		AND ISNULL(WIInner.numReorder,0) > 0 
																		AND ISNULL(WIInner.numOnHand,0) < ISNULL(WIInner.numReorder,0)',') ')

		END

		IF @tintUserRightType=1 AND NOT EXISTS (SELECT numUserDetailId FROM UserMaster WHERE numDomainID=@numDomainID AND numUserDetailId=@numUserCntID)
		BEGIN
			SET @strWhere = CONCAT(@strWhere,' AND I.numItemCode IN (SELECT 
																		V.numItemCode
																	FROM 
																		ExtranetAccountsDtl EAD 
																	INNER JOIN 
																		ExtarnetAccounts EA
																	ON 
																		EAD.numExtranetID=EA.numExtranetID 
																	INNER JOIN 
																		Vendor V 
																	ON 
																		EA.numDivisionID=V.numVendorID 
																	WHERE 
																		EAD.numDomainID=',@numDomainID,' 
																		AND V.numDomainID=',@numDomainID,' 
																		AND EAD.numContactID=',@numUserCntID,')')
		END
  
		DECLARE @firstRec AS INTEGER                                        
		DECLARE @lastRec AS INTEGER                                        
		SET @firstRec = ( @CurrentPage - 1 ) * @PageSize                                        
		SET @lastRec = ( @CurrentPage * @PageSize + 1 )

		-- EXECUTE FINAL QUERY
		DECLARE @strFinal AS NVARCHAR(MAX)=''
		IF ISNULL(@numFilteredWarehouseID,0)>0
		BEGIN
			SET @strFinal = CONCAT(@strFinal,'SELECT ROW_NUMBER() OVER(ORDER BY ',@columnName,' ',@columnSortOrder,',WarehouseItems.vcLocation asc) RowID, ',@strColumns,' INTO #temp2',@strSql,@strWhere,' ;')
			SET @strFinal = @strFinal + ' SELECT @TotalRecords = COUNT(*) FROM #temp2; '
			SET @strFinal = @strFinal + ' SELECT * FROM #temp2 WHERE RowID > '+ CAST(@firstRec AS VARCHAR(100)) +' and RowID <'+CAST(@lastRec AS VARCHAR(100))+';DROP TABLE #temp2; '
		END
		ELSE IF @CurrentPage = -1 AND @PageSize = -1
		BEGIN
			SET @strFinal = CONCAT('SELECT I.numItemCode, I.vcItemName, I.vcModelID INTO #tempTable ',@strSql  , @strWhere ,'; SELECT * FROM #tempTable; SELECT @TotalRecords = COUNT(*) FROM #tempTable; DROP TABLE #tempTable;')
		END
		ELSE
		BEGIN
			SET @strFinal = CONCAT('SELECT ROW_NUMBER() OVER(ORDER BY ',@columnName,' ',@columnSortOrder,') RowID,I.numItemCode INTO #temp2 ',@strSql  , @strWhere,'; SELECT RowID, ',@strColumns,' INTO #tempTable ',@strSql, ' JOIN #temp2 tblAllItems ON I.numItemCode = tblAllItems.numItemCode ',@strWhere,' AND tblAllItems.RowID > ',@firstRec,' and tblAllItems.RowID <',@lastRec,'; SELECT  * FROM  #tempTable ORDER BY RowID; DROP TABLE #tempTable; SELECT @TotalRecords = COUNT(*) FROM #temp2; DROP TABLE #temp2;')
		END

		
		PRINT @strFInal
		exec sp_executesql @strFinal, N'@TotalRecords INT OUT', @TotRecs OUT

	END

	UPDATE  
		#tempForm
    SET 
		intColumnWidth = (CASE WHEN ISNULL(intColumnWidth,0) <= 0 THEN 25 ELSE intColumnWidth END)
                                  
	SELECT * FROM #tempForm
    DROP TABLE #tempForm
END
GO
/****** Object:  StoredProcedure [dbo].[USP_ItemPricingRecomm]    Script Date: 02/19/2009 01:33:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- exec USP_ItemPricingRecomm @numItemCode=173028,@units=1,@numOppID=0,@numDivisionID=7052,@numDomainID=72,@tintOppType=1,@CalPrice=$50,@numWareHouseItemID=130634,@bitMode=0
--created by anoop jayaraj                          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_itempricingrecomm')
DROP PROCEDURE usp_itempricingrecomm
GO
CREATE PROCEDURE [dbo].[USP_ItemPricingRecomm]                                        
@numItemCode as numeric(9),                                        
@units Float,                                  
@numOppID as numeric(9)=0,                                
@numDivisionID as numeric(9)=0,                  
@numDomainID as numeric(9)=0,            
@tintOppType as tinyint,          
@CalPrice as DECIMAL(20,5),      
@numWareHouseItemID as numeric(9),
@bitMode tinyint=0,
@vcSelectedKitChildItems VARCHAR(MAX) = '',
@numOppItemID NUMERIC(18,0) = 0
as                 
BEGIN TRY

DECLARE @numRelationship AS NUMERIC(9)
DECLARE @numProfile AS NUMERIC(9)             
DECLARE @monListPrice DECIMAL(20,5)
DECLARE @bitCalAmtBasedonDepItems BIT
DECLARE @numDefaultSalesPricing TINYINT
DECLARE @tintPriceLevel INT
DECLARE @decmUOMConversion decimal(18,2)
DECLARE @intLeadTimeDays INT
DECLARE @intMinQty INT
DECLARE @tintKitAssemblyPriceBasedOn TINYINT

/*Profile and relationship id */
SELECT 
	@numRelationship=numCompanyType,
	@numProfile=vcProfile,
	@tintPriceLevel = ISNULL(D.tintPriceLevel,0)
FROM 
	DivisionMaster D                  
JOIN 
	CompanyInfo C 
ON 
	C.numCompanyId=D.numCompanyID                  
WHERE 
	numDivisionID =@numDivisionID
            
IF @tintOppType=1            
BEGIN
	SELECT
		@bitCalAmtBasedonDepItems=ISNULL(bitCalAmtBasedonDepItems,0)
		,@tintKitAssemblyPriceBasedOn=ISNULL(tintKitAssemblyPriceBasedOn,1)
	FROM 
		Item 
	WHERE 
		numItemCode = @numItemCode
      
	/*Get List Price for item*/      
	IF((@numWareHouseItemID>0) AND EXISTS(SELECT * FROM Item WHERE numItemCode=@numItemCode AND charItemType='P'))      
	BEGIN
		SELECT 
			@monListPrice=ISNULL(monWListPrice,0) 
		FROM 
			WareHouseItems 
		WHERE 
			numWareHouseItemID=@numWareHouseItemID      
		
		IF @monListPrice=0 
		BEGIN
			SELECT @monListPrice=monListPrice FROM Item WHERE numItemCode=@numItemCode       
		END
	END 
	ELSE
	BEGIN
		 SELECT 
			@monListPrice=monListPrice 
		FROM 
			Item 
		WHERE 
			numItemCode=@numItemCode      
	END      

	/*Use Calculated Price When item is Assembly/Kit,Calculate price based on sum of dependent items.*/
	IF @bitCalAmtBasedonDepItems = 1 
	BEGIN
		DECLARE @TEMPPrice TABLE
		(
			bitSuccess BIT
			,monPrice DECIMAL(20,5)
		)
		INSERT INTO @TEMPPrice
		(
			bitSuccess
			,monPrice
		)
		SELECT
			bitSuccess
			,monMSRPPrice
		FROM
			dbo.fn_GetKitAssemblyCalculatedPrice(@numDomainID,@numDivisionID,@numItemCode,@units,@numWarehouseItemID,@tintKitAssemblyPriceBasedOn,@numOppID,@numOppItemID,@vcSelectedKitChildItems,0,1)

		IF (SELECT bitSuccess FROM @TEMPPrice) = 1
		BEGIN
			SET @CalPrice = (SELECT monPrice FROM @TEMPPrice)
		END
		ELSE
		BEGIN
			RAISERROR('SELECTED_WAREHOUSE_IS_NOT_AVAILABLE_FOR_ALL_CHILD_ITEMS',16,1)
			RETURN
		END
	END


	SELECT 
		@numDefaultSalesPricing = ISNULL(numDefaultSalesPricing,2) 
	FROM 
		Domain 
	WHERE 
		numDomainID = @numDomainID

	IF @numDefaultSalesPricing = 1 -- Use Price Level
	BEGIN
		DECLARE @newPrice DECIMAL(20,5)
		DECLARE @finalUnitPrice FLOAT = 0
		DECLARE @tintRuleType INT
		DECLARE @tintDiscountType INT
		DECLARE @decDiscount FLOAT
		DECLARE @ItemPrice FLOAT

		SET @tintRuleType = 0
		SET @tintDiscountType = 0
		SET @decDiscount  = 0
		SET @ItemPrice = 0

		IF ISNULL(@tintPriceLevel,0) > 0
		BEGIN
			SELECT
				@tintRuleType = tintRuleType,
				@tintDiscountType = tintDiscountType,
				@decDiscount = decDiscount
			FROM
			(
				SELECT 
					ROW_NUMBER() OVER(PARTITION BY PricingTable.numItemCode ORDER BY numPricingID) Id,
					tintRuleType,
					tintDiscountType,
					decDiscount
				FROM 
					PricingTable 
				WHERE 
					PricingTable.numItemCode = @numItemCode
					AND ISNULL(numCurrencyID,0) = 0
			) TEMP
			WHERE
				Id = @tintPriceLevel
		END
		ELSE
		BEGIN
			SELECT TOP 1
				@tintRuleType = tintRuleType,
				@tintDiscountType = tintDiscountType,
				@decDiscount = decDiscount
			FROM 
				PricingTable 
			WHERE 
				numItemCode = @numItemCode 
				AND ISNULL(numCurrencyID,0) = 0
				AND (@units BETWEEN intFromQty AND intToQty)
		END

		IF @tintRuleType > 0 AND (@tintDiscountType > 0 OR @tintRuleType = 3)
		BEGIN
			IF @tintRuleType = 1 -- Deduct from List price
			BEGIN
				IF (SELECT ISNULL(charItemType,'') FROM Item WHERE numDomainID = @numDomainID AND numItemCode = @numItemCode) = 'P'
					If @bitCalAmtBasedonDepItems = 1 
						SELECT @ItemPrice = @CalPrice
					ELSE
						SELECT @ItemPrice = ISNULL(monWListPrice,0) FROM WareHouseItems WHERE numDomainID = @numDomainID AND numItemID = @numItemCode AND numWareHouseItemID = @numWarehouseItemID
				ELSE
					SELECT @ItemPrice = ISNULL(monListPrice,0) FROM Item WHERE numDomainID = @numDomainID AND numItemCode = @numItemCode

				IF @tintDiscountType = 1 -- Percentage
				BEGIN
					IF @decDiscount > 0
						SELECT @finalUnitPrice = @ItemPrice - (@ItemPrice * (@decDiscount/100))
				END
				ELSE IF @tintDiscountType = 2 -- Flat Amount
				BEGIN
					SELECT @finalUnitPrice = @ItemPrice - @decDiscount
				END
				ELSE IF @tintDiscountType = 3 -- Named Price
				BEGIN
					SELECT @finalUnitPrice = @decDiscount
				END
			END
			ELSE IF @tintRuleType = 2 -- Add to Primary Vendor Cost
			BEGIN
				If @bitCalAmtBasedonDepItems = 1 
					SELECT @ItemPrice = @CalPrice
				ELSE
					SELECT @ItemPrice = ISNULL(monCost,0) FROM Vendor WHERE numItemCode = @numItemCode AND numVendorID = (SELECT numVendorID FROM Item WHERE numItemCode = @numItemCode)

				IF @tintDiscountType = 1 AND @ItemPrice > 0 -- Percentage
				BEGIN
					IF @decDiscount > 0
						SELECT @finalUnitPrice = @ItemPrice + (@ItemPrice * (@decDiscount/100))
				END
				ELSE IF @tintDiscountType = 2 AND @ItemPrice > 0 -- Flat Amount
				BEGIN
					SELECT @finalUnitPrice = @ItemPrice + @decDiscount
				END
				ELSE IF @tintDiscountType = 3 -- Named Price
				BEGIN
					SELECT @finalUnitPrice = @decDiscount
				END
			END
			ELSE IF @tintRuleType = 3 -- Named Price
			BEGIN
				IF ISNULL(@decDiscount,0) = 0
				BEGIN
					SET @finalUnitPrice = @monListPrice
				END
				ELSE
				BEGIN
					SET @finalUnitPrice = @decDiscount
					SET @monListPrice = @finalUnitPrice
				END
				
				-- KEEP THIS LINE AT END
				SET @decDiscount = 0
				SET @tintDiscountType = 2		
			END
		END

		IF @finalUnitPrice = 0
			SET @newPrice = @monListPrice
		ELSE
			SET @newPrice = @finalUnitPrice

		If @tintRuleType = 2
		BEGIN
			IF @finalUnitPrice > 0
			BEGIN
				IF @tintDiscountType = 1 -- Percentage
				BEGIN
					IF @newPrice > @monListPrice
						SET @decDiscount = 0
					ELSE
						If @monListPrice > 0
							SET @decDiscount = ((@monListPrice - @newPrice) * 100) / CAST(@monListPrice AS FLOAT)
						ELSE
							SET @decDiscount = 0
				END
				ELSE IF @tintDiscountType = 2 -- Flat Amount
				BEGIN
					If @newPrice > @monListPrice
						SET @decDiscount = 0
					ELSE
						SET @decDiscount = @monListPrice - @newPrice
				END
			END
			ELSE
			BEGIN
				SET @decDiscount = 0
				SET @tintDiscountType = 0
			END
		END

		SELECT 
			1 as numUintHour, 
			ISNULL(@newPrice,0) AS ListPrice,
			(CASE WHEN @bitCalAmtBasedonDepItems = 1 THEN @CalPrice ELSE @monListPrice END) monItemListPrice,
			'' AS vcPOppName,
			'' AS bintCreatedDate,
			CASE 
				WHEN @tintRuleType = 0 THEN 'Price - List price'
				ELSE 
				   CASE @tintRuleType
				   WHEN 1 
						THEN 
							'Deduct from List price ' +  CASE 
														   WHEN @tintDiscountType = 1 THEN CONVERT(VARCHAR(20), @decDiscount) + '%'
														   WHEN @tintDiscountType = 2 THEN 'flat ' + CONVERT(VARCHAR(20), CAST(@decDiscount AS int))
														   ELSE 'Name price ' + CONVERT(VARCHAR(20), CAST(@decDiscount AS int))
														   END 
				   WHEN 2 THEN 'Add to primary vendor cost '  +  CASE 
														   WHEN @tintDiscountType = 1 THEN CONVERT(VARCHAR(20), @decDiscount) + '%'
														   WHEN @tintDiscountType = 2 THEN 'flat ' + CONVERT(VARCHAR(20), CAST(@decDiscount AS int))
														   ELSE 'Name price ' + CONVERT(VARCHAR(20), CAST(@decDiscount AS int))
														   END 
				   ELSE 'Flat ' + CONVERT(VARCHAR(20), @decDiscount)
				   END
			END AS OppStatus,
			CAST(0 AS NUMERIC) AS numPricRuleID,
			CAST(1 AS TINYINT) AS tintPricingMethod,
			CAST(@tintDiscountType AS TINYINT) AS tintDiscountTypeOriginal,
			CAST(@decDiscount AS FLOAT) AS decDiscountOriginal,
			CAST(@tintRuleType AS TINYINT) AS tintRuleType
	END
	ELSE -- Use Price Rule
	BEGIN
		/* Checks Pricebook if exist any.. */      
		SELECT TOP 1 
				1 AS numUnitHour,
				dbo.GetPriceBasedOnPriceBook(P.[numPricRuleID],CASE WHEN @bitCalAmtBasedonDepItems = 1 THEN @CalPrice ELSE @monListPrice END,@units,I.numItemCode) 
				* (CASE WHEN (P.tintRuleType=2 OR (P.tintRuleType=0 AND ISNULL(PBT.tintRuleType,0) = 2)) THEN dbo.fn_UOMConversion(numBaseUnit,@numItemCode,@numDomainId,CASE WHEN ISNULL(numPurchaseUnit,0)>0 THEN numPurchaseUnit ELSE numBaseUnit END) ELSE 1 END)  AS ListPrice,
				(CASE WHEN @bitCalAmtBasedonDepItems = 1 THEN @CalPrice ELSE @monListPrice END) monItemListPrice,
				vcRuleName AS vcPOppName,
				CONVERT(VARCHAR(20), NULL) AS bintCreatedDate,
				CASE WHEN numPricRuleID IS NULL THEN 'Price - List price'
					 ELSE 
					  CASE WHEN P.tintPricingMethod = 1
										 THEN 'Price Book Rule'
										 ELSE CASE PBT.tintRuleType
												WHEN 1 THEN 'Deduct from List price '
												WHEN 2 THEN 'Add to primary vendor cost '
												ELSE ''
											  END
											  + CASE 
													WHEN tintDiscountType = 1 THEN CONVERT(VARCHAR(20), decDiscount) + '%'
													WHEN tintDiscountType = 2 THEN 'flat ' + CONVERT(VARCHAR(20), CAST(decDiscount AS int))
													ELSE 'Named price ' + CONVERT(VARCHAR(20), CAST(decDiscount AS int))
												END + ' for every '
											  + CONVERT(VARCHAR(20), CASE WHEN [intQntyItems] = 0 THEN 1
																		  ELSE intQntyItems
																	 END)
											  + ' units, until maximum of '
											  + CONVERT(VARCHAR(20), decMaxDedPerAmt)
											  + CASE WHEN tintDiscountType = 1 THEN ' %' ELSE ' Flat' END END
				END AS OppStatus,P.numPricRuleID,P.tintPricingMethod,PBT.tintDiscountTypeOriginal,CAST(PBT.decDiscountOriginal AS FLOAT) AS decDiscountOriginal,PBT.tintRuleType
		FROM    Item I
				LEFT JOIN PriceBookRules P ON I.numDomainID = P.numDomainID
				LEFT JOIN PriceBookRuleDTL PDTL ON P.numPricRuleID = PDTL.numRuleID
				LEFT JOIN PriceBookRuleItems PBI ON P.numPricRuleID = PBI.numRuleID
				LEFT JOIN [PriceBookPriorities] PP ON PP.[Step2Value] = P.[tintStep2] AND PP.[Step3Value] = P.[tintStep3]
				CROSS APPLY dbo.GetPriceBasedOnPriceBookTable(P.[numPricRuleID],CASE WHEN bitCalAmtBasedonDepItems=1 then @CalPrice ELSE @monListPrice END ,@units,I.numItemCode) as PBT
		WHERE   numItemCode = @numItemCode AND tintRuleFor=@tintOppType
		AND 
		(
		((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 1 AND PDTL.numValue = @numDivisionID)) -- Priority 1
		OR ((tintStep2 = 2 AND PBI.numValue = I.numItemClassification) AND (tintStep3= 1 AND PDTL.numValue = @numDivisionID)) -- Priority 2
		OR ((tintStep2 = 3) AND (tintStep3 = 1 AND PDTL.numValue = @numDivisionID)) -- Priority 3

		OR ((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 4
		OR ((tintStep2 = 2 AND PBI.numValue = I.numItemClassification) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 5
		OR ((tintStep2 = 3) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 6

		OR ((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 3)) -- Priority 7
		OR ((tintStep2 = 2 AND PBI.numValue = I.numItemClassification) AND (tintStep3 = 3)) -- Priority 8
		OR ((tintStep2 = 3) and (tintStep3 = 3)) -- Priority 9
		)
		ORDER BY PP.Priority ASC
	END

	IF @bitMode=2  -- When this procedure is being called from USP_GetPriceOfItem for e-com
		RETURN 

                                   
	select 1 as numUnitHour,convert(DECIMAL(20,5),ISNULL((monTotAmount/(case when numUnitHour< 1 then 1 else numUnitHour end)),0)) as ListPrice,vcPOppName,convert(varchar(20),bintCreatedDate) as bintCreatedDate,case when tintOppStatus=0 then 'Opportunity' when tintOppStatus=1 then 'Deal won'   
	when tintOppStatus=2 then 'Deal Lost' end as OppStatus,CAST(0 AS NUMERIC) AS numPricRuleID,CAST(0 AS TINYINT) AS tintPricingMethod,CAST(0 AS TINYINT) as tintDiscountTypeOriginal,CAST(0 AS FLOAT) as decDiscountOriginal,CAST(0 AS TINYINT) as tintRuleType                         
	from OpportunityItems itm                                        
	join OpportunityMaster mst                                        
	on mst.numOppId=itm.numOppId              
	where  tintOppType=1 and numItemCode=@numItemCode and mst.numDivisionID=@numDivisionID order by OppStatus desc  
  
  
	select 1 as numUnitHour,CASE WHEN @bitCalAmtBasedonDepItems=1 THEN convert(DECIMAL(20,5),ISNULL(@CalPrice,0)) ELSE convert(DECIMAL(20,5),ISNULL(@monListPrice,0)) End as ListPrice,'' as vcPOppName,convert(varchar(20),null) as bintCreatedDate,CASE WHEN @bitCalAmtBasedonDepItems=1 THEN 'Sum of dependent items' ELSE '' END as OppStatus
	,CAST(0 AS NUMERIC) AS numPricRuleID,CAST(0 AS TINYINT) AS tintPricingMethod ,CAST(0 AS TINYINT) as tintDiscountTypeOriginal,CAST(0 AS FLOAT) as decDiscountOriginal,CAST(0 AS TINYINT) as tintRuleType                    
END             
ELSE            
BEGIN

	If @numDivisionID=0
	BEGIN
		SELECT 
			@numDivisionID=V.numVendorID 
		FROM 
			[Vendor] V 
		INNER JOIN 
			Item I 
		ON 
			V.numVendorID = I.numVendorID 
			AND V.numItemCode=I.numItemCode 
		WHERE 
			V.[numItemCode]=@numItemCode 
			AND V.[numDomainID]=@numDomainID 
	END

	IF EXISTS
	(
		SELECT 
			ISNULL([monCost],0) ListPrice 
		FROM 
			[Vendor] V 
		INNER JOIN 
			dbo.Item I 
		ON 
			V.numItemCode = I.numItemCode
		WHERE
			V.[numItemCode]=@numItemCode 
			AND V.[numDomainID]=@numDomainID 
			AND V.numVendorID=@numDivisionID
	)       
	BEGIN      
		SELECT 
			@monListPrice=ISNULL([monCost],0) 
		FROM 
			[Vendor] V 
		INNER JOIN 
			dbo.Item I 
		ON 
			V.numItemCode = I.numItemCode
		WHERE 
			V.[numItemCode]=@numItemCode 
			AND V.[numDomainID]=@numDomainID 
			AND V.numVendorID=@numDivisionID
	END      
	ELSE
	BEGIN   
		SELECT 
			@monListPrice=ISNULL([monCost],0) 
		FROM 
			[Vendor] 
		WHERE 
			[numItemCode]=@numItemCode 
			AND [numDomainID]=@numDomainID
	END      
 

	SELECT TOP 1 
		1 AS numUnitHour,
		ISNULL(dbo.GetPriceBasedOnPriceBook(P.[numPricRuleID],@monListPrice,@units,I.numItemCode),0) AS ListPrice,
		vcRuleName AS vcPOppName,
		CONVERT(VARCHAR(20), NULL) AS bintCreatedDate,
		CASE 
		WHEN numPricRuleID IS NULL THEN 'Price - List price'
		ELSE  
				CASE 
				WHEN P.tintPricingMethod = 1 THEN 'Price Book Rule'
				ELSE 
					(CASE tintRuleType
						WHEN 1 THEN 'Deduct from List price '
						WHEN 2 THEN 'Add to primary vendor cost '
						ELSE ''
					END) + 
					(CASE 
						WHEN tintDiscountType = 1 THEN CONVERT(VARCHAR(20), decDiscount) + '%'
						ELSE 'flat ' + CONVERT(VARCHAR(20), CAST(decDiscount AS int))
					END) + ' for every ' + 
					CONVERT(VARCHAR(20), (CASE WHEN [intQntyItems] = 0 THEN 1 ELSE intQntyItems END))
					+ ' units, until maximum of '
					+ CONVERT(VARCHAR(20), decMaxDedPerAmt)
					+ (CASE WHEN tintDiscountType = 1 THEN ' %' ELSE ' Flat' END)
				END
		END AS OppStatus,
		P.numPricRuleID,
		P.tintPricingMethod,
		@numDivisionID AS numDivisionID
	FROM
		Item I 
	LEFT JOIN PriceBookRules P ON I.numDomainID = P.numDomainID
	LEFT JOIN PriceBookRuleDTL PDTL ON P.numPricRuleID = PDTL.numRuleID
	LEFT JOIN PriceBookRuleItems PBI ON P.numPricRuleID = PBI.numRuleID
	LEFT JOIN [PriceBookPriorities] PP ON PP.[Step2Value] = P.[tintStep2] AND PP.[Step3Value] = P.[tintStep3]
	WHERE   
		numItemCode = @numItemCode 
		AND tintRuleFor=@tintOppType
		AND (
			((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 1 AND PDTL.numValue = @numDivisionID)) -- Priority 1
			OR ((tintStep2 = 2 AND PBI.numValue = I.numItemClassification) AND (tintStep3= 1 AND PDTL.numValue = @numDivisionID)) -- Priority 2
			OR ((tintStep2 = 3) AND (tintStep3 = 1 AND PDTL.numValue = @numDivisionID)) -- Priority 3

			OR ((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 4
			OR ((tintStep2 = 2 AND PBI.numValue = I.numItemClassification) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 5
			OR ((tintStep2 = 3) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 6

			OR ((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 3)) -- Priority 7
			OR ((tintStep2 = 2 AND PBI.numValue = I.numItemClassification) AND (tintStep3 = 3)) -- Priority 8
			OR ((tintStep2 = 3) and (tintStep3 = 3)) -- Priority 9
			)
	ORDER BY PP.Priority ASC

	SET @decmUOMConversion=(SELECT 
								dbo.fn_UOMConversion(ISNULL(I.numPurchaseUnit, 0) , I.numItemCode,I.numDomainId, ISNULL(I.numBaseUnit, 0)) 
							FROM 
								Item I 
							WHERE 
								I.numItemCode=@numItemCode)

	SELECT
		@intLeadTimeDays = ISNULL(VSM.numListValue,0),
		@intMinQty=ISNULL(intMinQty,0)
	FROM
		Item
	JOIN
		Vendor
	ON
		Item.numVendorID = Vendor.numVendorID
		AND Item.numItemCode=Vendor.numItemCode
	LEFT JOIN
		VendorShipmentMethod VSM
	ON
		Vendor.numVendorID = VSM.numVendorID
		AND VSM.numDomainID=@numDomainID
		AND VSM.bitPrimary = 1
		AND VSM.bitPreferredMethod = 1
	WHERE
		Item.numItemCode = @numItemCode

	select 1 as numUnitHour,convert(DECIMAL(20,5),ISNULL((monTotAmount/(case when numUnitHour< 1 then 1 else numUnitHour end)),0)) as ListPrice,
	@decmUOMConversion *(convert(DECIMAL(20,5),ISNULL((monTotAmount/(case when numUnitHour< 1 then 1 else numUnitHour end)),0)))
	as convrsPrice
	,vcPOppName,convert(varchar(20),bintCreatedDate) as bintCreatedDate,case when tintOppStatus=0 then 'Opportunity' 
		 when tintOppStatus=1 then 'Deal won'  
		 when tintOppStatus=2 then 'Deal Lost' end as OppStatus
		 ,CAST(0 AS NUMERIC) AS numPricRuleID,CAST(0 AS TINYINT) AS tintPricingMethod,@numDivisionID as numDivisionID                         
		 from OpportunityItems itm                                        
		 join OpportunityMaster mst                                        
		 on mst.numOppId=itm.numOppId                                        
		 where  tintOppType=2 and numItemCode=@numItemCode and mst.numDivisionID=@numDivisionID order by OppStatus,bintCreatedDate DESC

 		select 1 as numUnitHour,convert(DECIMAL(20,5),ISNULL(@monListPrice,0)) as ListPrice,
		@decmUOMConversion*(Convert(DECIMAL(20,5),ISNULL(@monListPrice,0))) as convrsPrice
		,@intLeadTimeDays as LeadTime,@intMinQty as intMinQty,
		(SELECT DATEADD(day,@intLeadTimeDays,GETDATE())) AS EstimatedArrival
		,'' as vcPOppName,convert(varchar(20),null) as bintCreatedDate,''  as OppStatus,@numDivisionID as numDivisionID
 		,CAST(0 AS NUMERIC) AS numPricRuleID,CAST(0 AS TINYINT) AS tintPricingMethod
END

END TRY
BEGIN CATCH
    SELECT 0 AS ListPrice
    SELECT 0 AS ListPrice
    SELECT 0 AS ListPrice
END CATCH
--SELECT * FROM Category
--- Alterd By Chintan Prajapati
-- EXEC dbo.USP_ItemsForECommerce 9,60,1,1,15,0,''
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ItemsForECommerce' ) 
    DROP PROCEDURE USP_ItemsForECommerce
GO
CREATE PROCEDURE [dbo].[USP_ItemsForECommerce]
    @numSiteID AS NUMERIC(9) = 0,
    @numCategoryID AS NUMERIC(18) = 0,
    @numWareHouseID AS NUMERIC(9) = 0,
    @SortBy NVARCHAR(MAX) = 0,
    @CurrentPage INT,
    @PageSize INT,
    @TotRecs INT = 0 OUTPUT,
    @SearchText AS NVARCHAR(MAX) = '',--Comma seperated item ids
    @FilterQuery AS NVARCHAR(MAX) = '',----Comma seperated Attributes Ids
    @FilterRegularCondition AS NVARCHAR(MAX),
    @FilterCustomFields AS NVARCHAR(MAX),
    @FilterCustomWhere AS NVARCHAR(MAX),
	@numDivisionID AS NUMERIC(18,0) = 0,
	@numManufacturerID AS NUMERIC(18,0)=0,
	@FilterItemAttributes AS NVARCHAR(MAX),
	@numESItemCodes AS NVARCHAR(MAX)=''
AS 
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	DECLARE @numDomainID AS NUMERIC(9)
	DECLARE @numDefaultRelationship AS NUMERIC(18,0)
	DECLARE @numDefaultProfile AS NUMERIC(18,0)
	DECLARE @tintPreLoginPriceLevel AS TINYINT
	DECLARE @tintPriceLevel AS TINYINT
	DECLARE @tintDisplayCategory AS TINYINT
	DECLARE @bitAutoSelectWarehouse AS BIT

	SELECT  
		@numDomainID = numDomainID
    FROM 
		[Sites]
    WHERE 
		numSiteID = @numSiteID
        
	IF ISNULL(@numDivisionID,0) > 0
	BEGIN
		SELECT 
			@numDefaultRelationship=ISNULL(numCompanyType,0)
			,@numDefaultProfile=ISNULL(vcProfile,0)
			,@tintPriceLevel=ISNULL(tintPriceLevel,0) 
		FROM 
			DivisionMaster 
		INNER JOIN 
			CompanyInfo 
		ON 
			DivisionMaster.numCompanyID=CompanyInfo.numCompanyId 
		WHERE 
			numDivisionID=@numDivisionID 
			AND DivisionMaster.numDomainID=@numDomainID
	END

	--KEEP THIS BELOW ABOVE IF CONDITION
	SELECT 
		@tintDisplayCategory = ISNULL(bitDisplayCategory,0)
		,@numDefaultRelationship=(CASE WHEN ISNULL(@numDivisionID,0) > 0 THEN @numDefaultRelationship ELSE numRelationshipId END)
		,@numDefaultProfile=(CASE WHEN ISNULL(@numDivisionID,0) > 0 THEN @numDefaultProfile ELSE numProfileId END)
		,@tintPreLoginPriceLevel=(CASE WHEN ISNULL(bitShowPriceUsingPriceLevel,0)=1 THEN (CASE WHEN @numDivisionID > 0 THEN (CASE WHEN ISNULL(bitShowPriceUsingPriceLevel,0)=1 THEN ISNULL(@tintPriceLevel,0) ELSE 0 END) ELSE ISNULL(tintPreLoginProceLevel,0) END) ELSE 0 END)
		,@bitAutoSelectWarehouse = ISNULL(bitAutoSelectWarehouse,0)
	FROM 
		dbo.eCommerceDTL 
	WHERE 
		numSiteID = @numSiteId
		
	CREATE TABLE #tmpItemCat(numCategoryID NUMERIC(18,0))
	CREATE TABLE #tmpItemCode(numItemCode NUMERIC(18,0))
		
	IF LEN(@FilterCustomFields) > 0 AND LEN(@FilterCustomWhere) > 0
	BEGIN			
		DECLARE @FilterCustomFieldIds AS NVARCHAR(MAX)
		SET @FilterCustomFieldIds = Replace(@FilterCustomFields,'[','')
		SET @FilterCustomFieldIds = Replace(@FilterCustomFieldIds,']','')
			
		DECLARE @strCustomSql AS NVARCHAR(MAX)
		SET @strCustomSql = 'INSERT INTO #tmpItemCode(numItemCode)
								SELECT DISTINCT T.RecId FROM 
														(
															SELECT * FROM 
																(
																	SELECT  t2.RecId, t1.fld_id, t2.Fld_Value FROM CFW_Fld_Master t1 
																	JOIN CFW_FLD_Values_Item AS t2 ON t1.Fld_ID = t2.Fld_ID 
																	WHERE t1.Grp_id  In (5,9)
																	AND t1.numDomainID = ' + CONVERT(VARCHAR(10),@numDomainID) + '
																	AND t1.fld_type <> ''Link'' 
																	AND t1.fld_id IN (' + @FilterCustomFieldIds + ')
																) p  PIVOT (MAX([Fld_Value]) FOR fld_id IN (' + @FilterCustomFields + ')) AS pvt 
														) T  WHERE 1=1 AND ' + @FilterCustomWhere
								 
		EXEC SP_EXECUTESQL @strCustomSql								 					 
	END
		
    DECLARE @strSQL NVARCHAR(MAX)
    DECLARE @firstRec AS INTEGER
    DECLARE @lastRec AS INTEGER
    DECLARE @Where NVARCHAR(MAX)
    DECLARE @SortString NVARCHAR(MAX)
    DECLARE @searchPositionColumn NVARCHAR(MAX)
	DECLARE @searchPositionColumnGroupBy NVARCHAR(MAX)
    DECLARE @row AS INTEGER
    DECLARE @data AS VARCHAR(100)
    DECLARE @dynamicFilterQuery NVARCHAR(MAX)
    DECLARE @checkFldType VARCHAR(100)
    DECLARE @count NUMERIC(9,0)
    DECLARE @whereNumItemCode NVARCHAR(MAX)
    DECLARE @filterByNumItemCode VARCHAR(100)
        
	SET @SortString = ''
    set @searchPositionColumn = ''
	SET @searchPositionColumnGroupBy = ''
    SET @strSQL = ''
	SET @whereNumItemCode = ''
	SET @dynamicFilterQuery = ''
		 
    SET @firstRec = ( @CurrentPage - 1 ) * @PageSize
    SET @lastRec = ( @CurrentPage * @PageSize + 1 )
                
	DECLARE @vcWarehouseIDs AS VARCHAR(1000)
    IF (@numWareHouseID = 0 OR @bitAutoSelectWarehouse = 1)
    BEGIN
		IF @bitAutoSelectWarehouse = 1
		BEGIN
			SELECT @vcWarehouseIDs = ISNULL( STUFF((SELECT DISTINCT ',' + CAST(numWarehouseID AS VARCHAR(10)) FROM [dbo].[Warehouses] WHERE [Warehouses].[numDomainID] = @numDomainID FOR XML PATH('')),1, 1, '') , '') 
		END
		ELSE
		BEGIN
			SELECT 
				@numWareHouseID = ISNULL(numDefaultWareHouseID,0)
			FROM 
				[eCommerceDTL]
			WHERE 
				[numDomainID] = @numDomainID	

			SET @vcWarehouseIDs = @numWareHouseID
		END
    END
	ELSE
	BEGIN
		SET @vcWarehouseIDs = @numWareHouseID
	END

	--PRIAMRY SORTING USING CART DROPDOWN
	DECLARE @Join AS NVARCHAR(MAX)
	DECLARE @SortFields AS NVARCHAR(MAX)
	DECLARE @vcSort AS VARCHAR(MAX)

	SET @Join = ''
	SET @SortFields = ''
	SET @vcSort = ''

	IF @SortBy = ''
	BEGIN
		SET @SortString = ' vcItemName Asc '
	END
	ELSE IF CHARINDEX('~0',@SortBy) > 0 OR CHARINDEX('~1',@SortBy) > 0
	BEGIN
		DECLARE @fldID AS NUMERIC(18,0)
		DECLARE @RowNumber AS VARCHAR(MAX)
				
		SELECT * INTO #fldValues FROM dbo.SplitIDsWithPosition(@SortBy,'~')
		SELECT @RowNumber = RowNumber, @fldID = id FROM #fldValues WHERE RowNumber = 1
		SELECT @vcSort = CASE WHEN Id = 0 THEN 'ASC' ELSE 'DESC' END FROM #fldValues WHERE RowNumber = 2
				
		SET @Join = @Join + ' LEFT JOIN CFW_FLD_Values_Item CFVI' + @RowNumber + ' ON I.numItemCode = CFVI' + @RowNumber + '.RecID AND CFVI' + @RowNumber + '.fld_Id = ' + CONVERT(VARCHAR(10),@fldID)
		---SET @SortString = 'CFVI' + @RowNumber + '.fld_Value' 
		SET @SortString = '[fld_Value' + @RowNumber + '] ' --+ @vcSort
		SET @SortFields = ', CFVI' + @RowNumber + '.fld_Value [fld_Value' + @RowNumber + ']'
	END
	ELSE
	BEGIN	
		IF CHARINDEX('monListPrice',@SortBy) > 0
		BEGIN
			DECLARE @bitSortPriceMode AS BIT
			SELECT @bitSortPriceMode = ISNULL(bitSortPriceMode,0) FROM dbo.eCommerceDTL WHERE numSiteId = @numSiteID
			
			IF @bitSortPriceMode = 1
			BEGIN
				SET @SortString = ' '
			END
			ELSE
			BEGIN
				SET @SortString = @SortBy	
			END
		END
		ELSE
		BEGIN
			SET @SortString = @SortBy	
		END	
	END	

	--SECONDARY SORTING USING DEFAULT SETTINGS FROM ECOMMERCE
	SELECT 
		ROW_NUMBER() OVER (ORDER BY Custom,vcFieldName) AS [RowID]
		,* 
	INTO 
		#tempSort 
	FROM 
	(
		SELECT 
			CONVERT(VARCHAR(9),numFieldID)+'~0' AS numFieldID,
			ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName,
			0 AS Custom,
			vcDbColumnName 
		FROM 
			View_DynamicColumns
		WHERE 
			numFormID = 84 
			AND ISNULL(bitSettingField,0) = 1 
			AND ISNULL(bitDeleted,0)=0 
			AND ISNULL(bitCustom,0) = 0
			AND numDomainID = @numDomainID	       
		UNION     
		SELECT 
			CONVERT(VARCHAR(9),numFieldID)+'~1' AS numFieldID,
			vcFieldName ,
			1 AS Custom,
			'' AS vcDbColumnName
		FROM 
			View_DynamicCustomColumns          
		WHERE 
			numFormID = 84
			AND numDomainID = @numDomainID
			AND grp_id IN (5,9)
			AND vcAssociatedControlType = 'TextBox' 
			AND ISNULL(bitCustom,0) = 1 
			AND tintPageType=1  
	) TABLE1 
	ORDER BY 
		Custom,vcFieldName
		
	--SELECT * FROM #tempSort
	DECLARE @DefaultSort AS NVARCHAR(MAX)
	DECLARE @DefaultSortFields AS NVARCHAR(MAX)
	DECLARE @DefaultSortJoin AS NVARCHAR(MAX)
	DECLARE @intCnt AS INT
	DECLARE @intTotalCnt AS INT	
	DECLARE @strColumnName AS VARCHAR(100)
	DECLARE @bitCustom AS BIT
	DECLARE @FieldID AS VARCHAR(100)				

	SET @DefaultSort = ''
	SET @DefaultSortFields = ''
	SET @DefaultSortJoin = ''		
	SET @intCnt = 0
	SET @intTotalCnt = ( SELECT COUNT(*) FROM #tempSort )
	SET @strColumnName = ''
		
	IF (SELECT ISNULL(bitEnableSecSorting,0) FROM EcommerceDtl WHERE numSiteID = @numSiteID ) = 1
	BEGIN
		CREATE TABLE #fldDefaultValues(RowNumber INT,Id VARCHAR(1000))

		WHILE(@intCnt < @intTotalCnt)
		BEGIN
			SET @intCnt = @intCnt + 1
			SELECT @FieldID = numFieldID, @strColumnName = vcDbColumnName, @bitCustom = Custom FROM #tempSort WHERE RowID = @intCnt

			IF @bitCustom = 0
			BEGIN
				SET @DefaultSort =  @strColumnName + ',' + @DefaultSort 
			END
			ELSE
			BEGIN
				DECLARE @fldID1 AS NUMERIC(18,0)
				DECLARE @RowNumber1 AS VARCHAR(MAX)
				DECLARE @vcSort1 AS VARCHAR(10)
				DECLARE @vcSortBy AS VARCHAR(1000)
				SET @vcSortBy = CONVERT(VARCHAR(100),@FieldID) + '~' + '0'

				INSERT INTO #fldDefaultValues(RowNumber,Id) SELECT RowNumber, id FROM dbo.SplitIDsWithPosition(@vcSortBy,'~')
				SELECT @RowNumber1 = RowNumber + 100 + @intCnt, @fldID1 = id FROM #fldDefaultValues WHERE RowNumber = 1
				SELECT @vcSort1 = 'ASC' 
						
				SET @DefaultSortJoin = @DefaultSortJoin + ' LEFT JOIN CFW_FLD_Values_Item CFVI' + @RowNumber1 + ' ON I.numItemCode = CFVI' + @RowNumber1 + '.RecID AND CFVI' + @RowNumber1 + '.fld_Id = ' + CONVERT(VARCHAR(10),@fldID1)
				SET @DefaultSort = '[fld_Value' + @RowNumber1 + '] ' + ',' + @DefaultSort 
				SET @DefaultSortFields = @DefaultSortFields + ', CFVI' + @RowNumber1 + '.fld_Value [fld_Value' + @RowNumber1 + ']'
			END
		END

		IF LEN(@DefaultSort) > 0
		BEGIN
			SET  @DefaultSort = LEFT(@DefaultSort,LEN(@DefaultSort) - 1)
			SET @SortString = @SortString + ',' 
		END
	END
                                		
    IF LEN(@SearchText) > 0
	BEGIN
		IF CHARINDEX('SearchOrder',@SortString) > 0
		BEGIN
			SET @searchPositionColumn = ' ,SearchItems.RowNumber AS SearchOrder ' 
			SET @searchPositionColumnGroupBy = ' ,SearchItems.RowNumber ' 
		END

		SET @Where = ' INNER JOIN dbo.SplitIDsWithPosition('''+ @SearchText +''','','') AS SearchItems 
                        ON SearchItems.Id = I.numItemCode  and SC.numSiteID = ' + CONVERT(VARCHAR(15), @numSiteID) + ' WHERE ISNULL(IsArchieve,0) = 0'
	END
	ELSE IF @SearchText = ''
	BEGIN
		SET @Where = ' WHERE 1=2 AND I.numDomainID=' + CONVERT(VARCHAR(15), @numDomainID)
		+ ' and SC.numSiteID = ' + CONVERT(VARCHAR(15), @numSiteID)
		+ ' AND ISNULL(IsArchieve,0) = 0'
	END
	ELSE
	BEGIN
		SET @Where = CONCAT(' WHERE 1=1 AND I.numDomainID=',@numDomainID,' and SC.numSiteID = ',@numSiteID,' AND ISNULL(IsArchieve,0) = 0')

		IF @tintDisplayCategory = 1
		BEGIN
					
			;WITH CTE AS(
			SELECT C.numCategoryID,C.tintLevel,C.numDepCategory,CASE WHEN tintLevel=1 THEN 1 ELSE 0 END AS bitChild 
				FROM SiteCategories SC INNER JOIN Category C ON SC.numCategoryID=C.numCategoryID WHERE SC.numSiteID=@numSiteID AND (C.numCategoryID = @numCategoryID OR ISNULL(@numCategoryID,0) = 0) AND numDomainID = @numDomainID
			UNION ALL
			SELECT  C.numCategoryID,C.tintLevel,C.numDepCategory,CTE.bitChild FROM Category C JOIN CTE ON 
			C.numDepCategory=CTE.numCategoryID AND CTE.bitChild=1 AND numDomainID = @numDomainID)
					
			INSERT INTO #tmpItemCat(numCategoryID)												 
			SELECT DISTINCT C.numCategoryID FROM CTE C JOIN dbo.ItemCategory IC ON C.numCategoryID = IC.numCategoryID 
												JOIN View_Item_Warehouse ON View_Item_Warehouse.numDOmainID=@numDomainID AND  View_Item_Warehouse.numItemID=IC.numItemID
												WHERE ISNULL(View_Item_Warehouse.numAvailable,0) > 0
		END

	END

	SET @strSQL = @strSQL + CONCAT(' WITH Items AS 
									( 
											SELECT
											I.numDomainID
											,I.numItemCode
											,ROW_NUMBER() OVER ( Partition by I.numItemCode Order by I.numItemCode) RowID
											,I.bitMatrix
											,I.bitKitParent
											,I.numItemGroup
											,vcItemName
											,I.bintCreatedDate
											,vcManufacturer
											,ISNULL(txtItemDesc,'''') txtItemDesc
											,vcSKU
											,fltHeight
											,fltWidth
											,fltLength
											,vcModelID
											,fltWeight
											,numBaseUnit
											,numSaleUnit
											,numPurchaseUnit
											,bitFreeShipping
											,C.vcCategoryName
											,C.vcDescription as CategoryDesc
											,charItemType
											,monListPrice
											,bitAllowBackOrder
											,bitShowInStock
											,numVendorID
											,numItemClassification',@searchPositionColumn,@SortFields,@DefaultSortFields,'
											,SUM(numOnHand) numOnHand
											,SUM(numAllocation) numAllocation
											' + (CASE WHEN LEN(ISNULL(@numESItemCodes,'')) > 0 THEN ',ISNULL(C.numCategoryID,0) numCategoryID' ELSE '' END)  + '
											,ISNULL(I.numManufacturer,0) numManufacturer
											FROM      
											Item AS I
											INNER JOIN eCommerceDTL E ON E.numDomainID = I.numDomainID AND E.numSiteId=',@numSiteID,'
											LEFT JOIN WareHouseItems WI ON WI.numDomainID=' + CONVERT(VARCHAR(10),@numDomainID) + ' AND WI.numItemID=I.numItemCode AND numWareHouseID IN (SELECT [OutParam] FROM dbo.[SplitString](''' + @vcWarehouseIDs + ''','',''))
											INNER JOIN ItemCategory IC ON I.numItemCode = IC.numItemID
											INNER JOIN Category C ON IC.numCategoryID = C.numCategoryID
											INNER JOIN SiteCategories SC ON IC.numCategoryID = SC.numCategoryID 
											',@Join,' ',@DefaultSortJoin)

	IF LEN(ISNULL(@FilterItemAttributes,'')) > 0
	BEGIN
		SET @strSQL = @strSQL + @FilterItemAttributes
	END

	IF LEN(@FilterCustomFields) > 0 AND LEN(@FilterCustomWhere) > 0
	BEGIN
		SET @strSQL = @strSQL + ' INNER JOIN #tmpItemCode TP ON TP.numItemCode = I.numItemCode ' 
	END
			
	IF (SELECT COUNT(*) FROM #tmpItemCat) > 0
	BEGIN
		SET @strSQL = @strSQL + ' INNER JOIN #tmpItemCat TC ON TC.numCategoryID = IC.numCategoryID '
	END
	ELSE IF @numManufacturerID>0
	BEGIN
		SET @Where = REPLACE(@Where,'WHERE',' WHERE I.numManufacturer = ' + CONVERT(VARCHAR(10),@numManufacturerID) + ' AND ')
	END 
	ELSE IF @numCategoryID>0
	BEGIN
		SET @Where = REPLACE(@Where,'WHERE',' WHERE IC.numCategoryID = ' + CONVERT(VARCHAR(10),@numCategoryID) + ' AND ')
	END 
			
	IF (SELECT COUNT(*) FROM [dbo].[EcommerceRelationshipProfile] WHERE numSiteID=@numSiteID AND numRelationship = @numDefaultRelationship AND numProfile = @numDefaultProfile) > 0
	BEGIN
		SET @Where = @Where + CONCAT(' AND I.numItemClassification NOT IN (SELECT numItemClassification FROM EcommerceRelationshipProfile ERP INNER JOIN ECommerceItemClassification EIC ON EIC.numECommRelatiionshipProfileID=ERP.ID WHERE ERP.numSiteID=',@numSiteID,' AND ERP.numRelationship=',@numDefaultRelationship,' AND ERP.numProfile=', @numDefaultProfile,')')
	END

	SET @strSQL = @strSQL + @Where
	
	IF LEN(@FilterRegularCondition) > 0
	BEGIN
		SET @strSQL = @strSQL + @FilterRegularCondition
	END                                         

    IF LEN(@whereNumItemCode) > 0
    BEGIN
			SET @strSQL = @strSQL + @filterByNumItemCode	
	END

    SET @strSQL = CONCAT(@strSQL,' GROUP BY
										I.numDomainID
										,I.numItemCode
										,I.bitMatrix
										,I.bitKitParent
										,I.numItemGroup
										,vcItemName
										,I.bintCreatedDate
										,vcManufacturer
										,txtItemDesc
										,vcSKU
										,fltHeight
										,fltWidth
										,fltLength
										,vcModelID
										,fltWeight
										,numBaseUnit
										,numSaleUnit
										,numPurchaseUnit
										,bitFreeShipping
										,C.vcCategoryName
										,C.vcDescription
										,charItemType
										,monListPrice
										,bitAllowBackOrder
										,bitShowInStock
										,numVendorID
										,numItemClassification
										' + (CASE WHEN LEN(ISNULL(@numESItemCodes,'')) > 0 THEN ',C.numCategoryID' ELSE '' END)  + '
										,I.numManufacturer',@searchPositionColumnGroupBy,@SortFields,@DefaultSortFields,(CASE WHEN ISNULL(@tintDisplayCategory,0) = 1 THEN ' HAVING 1 = (CASE WHEN (I.charItemType=''P'' AND ISNULL(I.bitKitParent,0) = 0) THEN CASE WHEN (ISNULL(SUM(numOnHand), 0) > 0 OR ISNULL(SUM(numAllocation), 0) > 0) THEN 1 ELSE 0 END ELSE 1 END) ' ELSE '' END), ') SELECT * INTO #TEMPItems FROM Items;')
                                        
	IF @numDomainID IN (204,214,215)
	BEGIN
		SET @strSQL = @strSQL + 'DELETE FROM 
									#TEMPItems
								WHERE 
									numItemCode IN (
														SELECT 
															F.numItemCode
														FROM 
															#TEMPItems AS F
														WHERE 
															ISNULL(F.bitMatrix,0) = 1 
															AND EXISTS (
																		SELECT 
																			t1.vcItemName, t1.bitMatrix, t1.numItemGroup, Count(t1.numItemCode)
																		FROM 
																			#TEMPItems t1
																		WHERE 
																			t1.vcItemName = F.vcItemName
																			AND t1.bitMatrix = 1
																			AND t1.numItemGroup = F.numItemGroup
																		GROUP BY 
																			t1.vcItemName, t1.bitMatrix, t1.numItemGroup
																		HAVING 
																			Count(t1.numItemCode) > 1
																	)
													)
									AND numItemCode NOT IN (
																			SELECT 
																				Min(numItemCode)
																			FROM 
																				#TEMPItems AS F
																			WHERE
																				ISNULL(F.bitMatrix,0) = 1 
																				AND Exists (
																							SELECT 
																								t2.vcItemName, t2.bitMatrix, t2.numItemGroup, Count(t2.numItemCode)
																							FROM 
																								#TEMPItems t2
																							WHERE 
																								t2.vcItemName = F.vcItemName
																								AND t2.bitMatrix = 1
																								AND t2.numItemGroup = F.numItemGroup
																							GROUP BY 
																								t2.vcItemName, t2.bitMatrix, t2.numItemGroup
																							HAVING 
																								Count(t2.numItemCode) > 1
																						)
																			GROUP BY 
																				vcItemName, bitMatrix, numItemGroup
																		);'

	END
									  
									  
	SET @strSQL = @strSQL + ' WITH ItemSorted AS (SELECT  ROW_NUMBER() OVER ( ORDER BY  ' + @SortString + @DefaultSort + ' ' + @vcSort + ' ) AS Rownumber,* FROM  #TEMPItems WHERE RowID = 1)'

    DECLARE @fldList AS NVARCHAR(MAX)
	DECLARE @fldList1 AS NVARCHAR(MAX)
	SELECT @fldList=COALESCE(@fldList + ',','') + QUOTENAME(REPLACE(Fld_label,' ','') + '_C') FROM dbo.CFW_Fld_Master WHERE numDomainID=@numDomainID AND Grp_id=5
	SELECT @fldList1=COALESCE(@fldList1 + ',','') + QUOTENAME(REPLACE(Fld_label,' ','') + '_' + CAST(Fld_Id AS VARCHAR)) FROM dbo.CFW_Fld_Master WHERE numDomainID=@numDomainID AND Grp_id=9
            
	/***Changed By: Chirag R:****************/
	/***Note: Changes for getting records Elastic Search Affected Items ********/
    --SET @strSQL = @strSQL + 'SELECT * INTO #tmpPagedItems FROM ItemSorted WHERE Rownumber > ' + CONVERT(VARCHAR(10), @firstRec) + '
    --                            AND   Rownumber < ' + CONVERT(VARCHAR(10), @lastRec) + '
    --                            order by Rownumber '

	DECLARE @bitGetItemForElasticSearch BIT;
	SET @bitGetItemForElasticSearch = IIF(LEN(@numESItemCodes) > 0,1,0)
    SET @strSQL = @strSQL + 'SELECT * INTO #tmpPagedItems FROM ItemSorted ';
	
	IF(@bitGetItemForElasticSearch = 1)
	BEGIN
		SET @strSQL = @strSQL + ' WHERE numItemCode IN (' + @numESItemCodes + ')'
	END 
	ELSE
	BEGIN
		SET @strSQL = @strSQL + ' WHERE Rownumber > ' + CONVERT(VARCHAR(10), @firstRec) + '
							AND   Rownumber < ' + CONVERT(VARCHAR(10), @lastRec)
	END
    SET @strSQL = @strSQL + ' order by Rownumber '

    /***Changes Completed: Chirag R:****************/                

	SET @strSQL = CONCAT(@strSQL,'SELECT  
								Rownumber
								,I.numItemCode
								,vcCategoryName
								,CategoryDesc
								,vcItemName
								,txtItemDesc
								,vcManufacturer
								,vcSKU
								,fltHeight
								,fltWidth
								,fltLength
								,vcModelID
								,fltWeight
								,bitFreeShipping
								,', 
									(CASE 
									WHEN @tintPreLoginPriceLevel > 0 
									THEN 'ISNULL((CASE 
											WHEN PricingOption.tintRuleType = 1 AND PricingOption.tintDiscountType = 1
											THEN ISNULL((CASE WHEN I.[charItemType] = ''P'' THEN ( UOM * ISNULL(W1.[monWListPrice],0)) ELSE (UOM * monListPrice) END),0) - (ISNULL((CASE WHEN I.[charItemType] = ''P'' THEN ( UOM * ISNULL(W1.[monWListPrice],0)) ELSE (UOM * monListPrice) END),0) * (PricingOption.decDiscount / 100 ) )
											WHEN PricingOption.tintRuleType = 1 AND PricingOption.tintDiscountType = 2
											THEN ISNULL((CASE WHEN I.[charItemType] = ''P'' THEN ( UOM * ISNULL(W1.[monWListPrice],0)) ELSE (UOM * monListPrice) END),0) - PricingOption.decDiscount
											WHEN PricingOption.tintRuleType = 2 AND PricingOption.tintDiscountType = 1
											THEN ISNULL(Vendor.monCost,0) + (ISNULL(Vendor.monCost,0) * (PricingOption.decDiscount / 100 ) )
											WHEN PricingOption.tintRuleType = 2 AND PricingOption.tintDiscountType = 2
											THEN ISNULL(Vendor.monCost,0) + PricingOption.decDiscount
											WHEN PricingOption.tintRuleType = 3
											THEN PricingOption.decDiscount
										END),ISNULL(CASE WHEN I.[charItemType] = ''P'' THEN ( UOM * ISNULL(W1.[monWListPrice],0)) ELSE (UOM * monListPrice) END, 0))' 
									ELSE 'ISNULL(CASE WHEN I.[charItemType] = ''P'' THEN ( UOM * ISNULL(W1.[monWListPrice],0)) ELSE (UOM * monListPrice) END, 0)' 
									END)
									,' AS monListPrice,
									ISNULL(CASE WHEN I.[charItemType] = ''P'' THEN ( UOM * ISNULL(W1.[monWListPrice],0)) ELSE (UOM * monListPrice) END, 0) monMSRP					   
									',(CASE WHEN @numDomainID = 172 THEN ',ISNULL(CASE 
											WHEN tintRuleType = 1 AND tintDiscountType = 1
											THEN ISNULL((CASE WHEN I.[charItemType] = ''P'' THEN ( UOM * ISNULL(W1.[monWListPrice],0)) ELSE (UOM * monListPrice) END),0) - (ISNULL((CASE WHEN I.[charItemType] = ''P'' THEN ( UOM * ISNULL(W1.[monWListPrice],0)) ELSE (UOM * monListPrice) END),0) * ( decDiscount / 100 ) )
											WHEN tintRuleType = 1 AND tintDiscountType = 2
											THEN ISNULL((CASE WHEN I.[charItemType] = ''P'' THEN ( UOM * ISNULL(W1.[monWListPrice],0)) ELSE (UOM * monListPrice) END),0) - decDiscount
											WHEN tintRuleType = 2 AND tintDiscountType = 1
											THEN ISNULL(Vendor.monCost,0) + (ISNULL(Vendor.monCost,0) * ( decDiscount / 100 ) )
											WHEN tintRuleType = 2 AND tintDiscountType = 2
											THEN ISNULL(Vendor.monCost,0) + decDiscount
											WHEN tintRuleType = 3
											THEN decDiscount
										END, 0) AS monFirstPriceLevelPrice,ISNULL(CASE 
										WHEN tintRuleType = 1 AND tintDiscountType = 1 
										THEN decDiscount 
										WHEN tintRuleType = 1 AND tintDiscountType = 2
										THEN (decDiscount * 100) / (ISNULL((CASE WHEN I.[charItemType] = ''P'' THEN ( UOM * ISNULL(W1.[monWListPrice],0)) ELSE (UOM * monListPrice) END),0) - decDiscount)
										WHEN tintRuleType = 2 AND tintDiscountType = 1
										THEN decDiscount
										WHEN tintRuleType = 2 AND tintDiscountType = 2
										THEN (decDiscount * 100) / (ISNULL(Vendor.monCost,0) + decDiscount)
										WHEN tintRuleType = 3
										THEN 0
									END,'''') AS fltFirstPriceLevelDiscount' ELSE ',0 AS monFirstPriceLevelPrice, 0 AS fltFirstPriceLevelDiscount' END), '  
								,vcPathForImage
								,vcpathForTImage
								,UOM AS UOMConversionFactor
								,UOMPurchase AS UOMPurchaseConversionFactor
								,(CASE WHEN charItemType <> ''S'' AND charItemType <> ''N'' AND ISNULL(bitKitParent,0)=0
											THEN (CASE 
													WHEN bitAllowBackOrder = 1 THEN 1
													WHEN (ISNULL(I.numOnHand,0)<=0) THEN 0
													ELSE 1
												END)
											ELSE 1
								END) AS bitInStock,
								(
									CASE WHEN ISNULL(bitKitParent,0) = 1
									THEN
										(CASE 
											WHEN ((SELECT COUNT(*) FROM ItemDetails ID INNER JOIN Item IInner ON ID.numChildItemID = IInner.numItemCode WHERE ID.numItemKitID = I.numItemCode AND ISNULL(IInner.bitKitParent,0) = 1)) > 0 
											THEN 1 
											ELSE 0 
										END)
									ELSE
										0
									END
								) bitHasChildKits
								,(CASE WHEN charItemType <> ''S'' AND charItemType <> ''N''
											THEN ( CASE WHEN bitShowInStock = 1
														THEN (CASE 
																WHEN bitAllowBackOrder = 1 AND I.numDomainID <> 172 AND ISNULL(I.numOnHand,0)<=0 AND ISNULL(I.numVendorID,0) > 0 AND dbo.GetVendorPreferredMethodLeadTime(I.numVendorID,0) > 0 THEN CONCAT(''<font class="vendorLeadTime" style="color:green">Usually ships in '',dbo.GetVendorPreferredMethodLeadTime(I.numVendorID,0),'' days</font>'')
																WHEN bitAllowBackOrder = 1 AND ISNULL(I.numOnHand,0)<=0 THEN (CASE WHEN I.numDomainID = 172 THEN ''<font style="color:green">AVAILABLE</font>'' ELSE ''<font style="color:green">In Stock</font>'' END)
																WHEN (ISNULL(I.numOnHand,0)<=0) THEN (CASE WHEN I.numDomainID = 172 THEN ''<font style="color:red">ON HOLD</font>'' ELSE ''<font style="color:red">Out Of Stock</font>'' END)
																ELSE (CASE WHEN I.numDomainID = 172 THEN ''<font style="color:green">AVAILABLE</font>'' ELSE ''<font style="color:green">In Stock</font>'' END)  
															END)
														ELSE ''''
													END )
											ELSE ''''
										END ) AS InStock
								, ISNULL(W1.numWareHouseItemID,0) [numWareHouseItemID]
								,ISNULL(TablePromotion.numTotalPromotions,0) numTotalPromotions
								,ISNULL(TablePromotion.vcPromoDesc,'''') vcPromoDesc
								,ISNULL(TablePromotion.bitRequireCouponCode,0) bitRequireCouponCode
								' + (CASE WHEN LEN(ISNULL(@numESItemCodes,'')) > 0 THEN ',I.numCategoryID' ELSE '' END)  + '
								,I.numManufacturer
								,I.bintCreatedDate
								,(CASE WHEN ISNULL(I.numItemGroup,0) > 0 AND ISNULL(I.bitMatrix,0) = 1 THEN dbo.fn_GetItemAttributes(I.numDomainID,I.numItemCode,1) ELSE '''' END) AS vcAttributes
								',(CASE WHEN LEN(ISNULL(@fldList,'')) > 0 THEN CONCAT(',',@fldList) ELSE '' END),(CASE WHEN LEN(ISNULL(@fldList1,'')) > 0 THEN CONCAT(',',@fldList1) ELSE '' END),'
                            FROM 
								#tmpPagedItems I
							LEFT JOIN ItemImages IM ON I.numItemCode = IM.numItemCode AND IM.bitdefault=1 AND IM.bitIsImage = 1
							LEFT JOIN Vendor ON Vendor.numVendorID =I.numVendorID AND Vendor.numItemCode=I.numItemCode
							OUTER APPLY (SELECT * FROM dbo.fn_GetItemPromotions(',@numDomainID,',',@numDivisionID,',I.numItemCode,I.numItemClassification',',',@numDefaultRelationship,',',@numDefaultProfile,',1)) TablePromotion ',
							(CASE 
								WHEN @tintPreLoginPriceLevel > 0 
								THEN CONCAT(' OUTER APPLY(SELECT * FROM PricingTable WHERE numItemCode=I.numItemCode AND ISNULL(numPriceRuleID,0) = 0 AND ISNULL(numCurrencyID,0) = 0 ORDER BY numPricingID OFFSET ',CAST(@tintPreLoginPriceLevel-1 AS VARCHAR),' ROWS FETCH NEXT 1 ROWS ONLY) As PricingOption ' )
								ELSE '' 
							END),(CASE 
								WHEN @numDomainID = 172
								THEN ' OUTER APPLY (SELECT TOP 1 * FROM PricingTable WHERE numItemCode=I.numItemCode AND ISNULL(numCurrencyID,0) = 0) AS PT'
								ELSE '' 
							END),' 
							OUTER APPLY (SELECT TOP 1 numWareHouseItemID,monWListPrice FROM WareHouseItems W 
											WHERE  W.numItemID = I.numItemCode 
											AND W.numDomainID = ',CONVERT(VARCHAR(10),@numDomainID),'
											AND numWareHouseID IN (SELECT [OutParam] FROM dbo.[SplitString](''' + @vcWarehouseIDs + ''','',''))) AS W1 			  
								CROSS APPLY (SELECT dbo.fn_UOMConversion(ISNULL(I.numSaleUnit, 0),I.numItemCode, I.numDomainId,ISNULL(I.numBaseUnit, 0)) AS UOM) AS UOM
								CROSS APPLY (SELECT dbo.fn_UOMConversion(ISNULL(I.numPurchaseUnit, 0),I.numItemCode, I.numDomainId,ISNULL(I.numBaseUnit, 0)) AS UOMPurchase) AS UOMPurchase')
			
	IF LEN(ISNULL(@fldList,'')) > 0
	BEGIN
		SET @strSQL = @strSQL +' left join (
											SELECT  t2.RecId, REPLACE(t1.Fld_label,'' '','''') + ''_C'' as Fld_label, 
												CASE t1.fld_type WHEN ''SelectBox'' THEN isnull(dbo.fn_GetListItemName(t2.Fld_Value),'''')
																	WHEN ''CheckBox'' THEN isnull(dbo.fn_getCustomDataValue(t2.Fld_Value,9),'''')
																	WHEN ''DateField'' THEN dbo.FormatedDateFromDate(t2.Fld_Value,' + CAST(@numDomainId AS VARCHAR(18)) +')
																	ELSE Replace(ISNULL(t2.Fld_Value,''0''),''$'',''$$'') end AS Fld_Value FROM CFW_Fld_Master t1 
											JOIN CFW_FLD_Values_Item AS t2 ON t1.Fld_ID = t2.Fld_ID where t1.Grp_id = 5 and t1.numDomainID=' + CAST(@numDomainId AS VARCHAR(18)) +' AND t1.fld_type <> ''Link''
											AND t2.RecId IN (SELECT numItemCode FROM #tmpPagedItems )
											) p PIVOT (MAX([Fld_Value]) FOR Fld_label IN (' + @fldList + ')) AS pvt on I.numItemCode=pvt.RecId' 
	END

	IF LEN(ISNULL(@fldList1,'')) > 0
	BEGIN
					
		SET @strSQL = @strSQL +' left join (
											SELECT  t2.numItemCode AS RecId, REPLACE(t1.Fld_label,'' '','''') + ''_'' + CAST(t1.Fld_Id AS VARCHAR) as Fld_label, 
												CASE t1.fld_type WHEN ''SelectBox'' THEN isnull(dbo.fn_GetListItemName(t2.Fld_Value),'''')
																	WHEN ''CheckBox'' THEN isnull(dbo.fn_getCustomDataValue(t2.Fld_Value,9),'''')
																	WHEN ''DateField'' THEN dbo.FormatedDateFromDate(t2.Fld_Value,' + CAST(@numDomainId AS VARCHAR(18)) +')
																	ELSE Replace(ISNULL(t2.Fld_Value,''0''),''$'',''$$'') end AS Fld_Value FROM CFW_Fld_Master t1 
											JOIN ItemAttributes AS t2 ON t1.Fld_ID = t2.Fld_ID where t1.Grp_id = 9 and t1.numDomainID=' + CAST(@numDomainId AS VARCHAR(18)) +' AND t1.fld_type <> ''Link''
											AND t2.numItemCode IN (SELECT numItemCode FROM #tmpPagedItems )
											) p PIVOT (MAX([Fld_Value]) FOR Fld_label IN (' + @fldList1 + ')) AS pvt1 on I.numItemCode=pvt1.RecId'
	END
	ELSE
	BEGIN
		SET @strSQL = REPLACE(@strSQL, ',##FLDLIST1##', '');
	END

	SET  @strSQL = @strSQL +' order by Rownumber';                
    SET @strSQL = @strSQL + ' SELECT @TotRecs = COUNT(*) FROM #TEMPItems WHERE RowID=1 '                            


	DECLARE @tmpSQL AS NVARCHAR(MAX)
	SET @tmpSQL = @strSQL
	
	PRINT CAST(@tmpSQL AS NTEXT)
	EXEC sp_executeSQL @strSQL, N'@TotRecs INT OUTPUT', @TotRecs OUTPUT
             
             
    DECLARE @tintOrder AS TINYINT                                                  
	DECLARE @vcFormFieldName AS VARCHAR(50)                                                  
	DECLARE @vcListItemType AS VARCHAR(1)                                             
	DECLARE @vcAssociatedControlType VARCHAR(10)                                                  
	DECLARE @numListID AS NUMERIC(9)                                                  
	DECLARE @WhereCondition VARCHAR(MAX)                       
	DECLARE @numFormFieldId AS NUMERIC  
	--DECLARE @vcFieldType CHAR(1)
	DECLARE @vcFieldType VARCHAR(15)
		                  
	SET @tintOrder=0                                                  
	SET @WhereCondition =''                 
                   
              
	CREATE TABLE #tempAvailableFields
	(
		numFormFieldId  NUMERIC(9)
		,vcFormFieldName NVARCHAR(50)
		,vcFieldType VARCHAR(15)
		,vcAssociatedControlType NVARCHAR(50)
		,numListID NUMERIC(18)
		,vcListItemType CHAR(1)
		,intRowNum INT
		,vcItemValue VARCHAR(3000)
	)
		   
	INSERT INTO #tempAvailableFields
	(
		numFormFieldId
		,vcFormFieldName
		,vcFieldType
		,vcAssociatedControlType
		,numListID
		,vcListItemType
		,intRowNum
	)                         
	SELECT 
		Fld_id
		,REPLACE(Fld_label,' ','') AS vcFormFieldName
		,CASE GRP_ID 
			WHEN 4 then 'C' 
			WHEN 1 THEN 'D'
			WHEN 9 THEN CONVERT(VARCHAR(15), Fld_id)
			ELSE 'C' 
		END AS vcFieldType
		,fld_type as vcAssociatedControlType
		,ISNULL(C.numListID, 0) numListID
		,CASE 
			WHEN C.numListID > 0 THEN 'L'
			ELSE ''
		END vcListItemType
		,ROW_NUMBER() OVER (ORDER BY Fld_id ASC) AS intRowNum
	FROM  
		CFW_Fld_Master C 
	LEFT JOIN 
		CFW_Validation V 
	ON 
		V.numFieldID = C.Fld_id
	WHERE 
		C.numDomainID = @numDomainId
		AND GRP_ID IN (5,9)

		  
	SELECT * FROM #tempAvailableFields
	
	IF OBJECT_ID('tempdb..#tempAvailableFields') IS NOT NULL
	DROP TABLE #tempAvailableFields
		
	IF OBJECT_ID('tempdb..#tmpItemCode') IS NOT NULL
	DROP TABLE #tmpItemCode
		
	IF OBJECT_ID('tempdb..#tmpItemCat') IS NOT NULL
	DROP TABLE #tmpItemCat
		
	IF OBJECT_ID('tempdb..#fldValues') IS NOT NULL
	DROP TABLE #fldValues
		
	IF OBJECT_ID('tempdb..#tempSort') IS NOT NULL
	DROP TABLE #tempSort
		
	IF OBJECT_ID('tempdb..#fldDefaultValues') IS NOT NULL
	DROP TABLE #fldDefaultValues

	IF OBJECT_ID('tempdb..#tmpPagedItems') IS NOT NULL
	DROP TABLE #tmpPagedItems


	IF OBJECT_ID('tempdb..#TEMPItems') IS NOT NULL
	DROP TABLE #TEMPItems
END
GO
/****** Object:  StoredProcedure [dbo].[USP_ManageCategory]    Script Date: 07/26/2008 16:19:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj        
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_managecategory')
DROP PROCEDURE usp_managecategory
GO
CREATE PROCEDURE [dbo].[USP_ManageCategory]        
	@numCatergoryId as numeric(9),        
	@vcCatgoryName as varchar(1000),       
	@numDomainID as numeric(9)=0,
	@vcDescription as VARCHAR(MAX),
	@intDisplayOrder AS INT ,
	@vcPathForCategoryImage AS VARCHAR(100),
	@numDepCategory AS NUMERIC(9,0),
	@numCategoryProfileID AS NUMERIC(18,0),
	@vcCatgoryNameURL VARCHAR(1000),
	@vcMetaTitle as varchar(1000),
	@vcMetaKeywords as varchar(1000),
	@vcMetaDescription as varchar(1000),
	@vcMatrixGroups VARCHAR(300)
AS
BEGIN
	DECLARE @tintLevel AS TINYINT
	SET @tintLevel = 1

	IF	@numDepCategory <> 0
	BEGIN
		SELECT @tintLevel = (tintLevel + 1) FROM dbo.Category WHERE numCategoryID = @numDepCategory
	END       
       
	IF @numCatergoryId=0        
	BEGIN
		INSERT INTO Category 
		(
			vcCategoryName,
			tintLevel,
			numDomainID,
			vcDescription,
			intDisplayOrder,
			vcPathForCategoryImage,
			numDepCategory,
			numCategoryProfileID,
			vcCategoryNameURL,
			vcMetaTitle,
			vcMetaKeywords,
			vcMetaDescription
		)        
		VALUES
		(
			@vcCatgoryName,
			@tintLevel,
			@numDomainID,
			@vcDescription,
			@intDisplayOrder,
			@vcPathForCategoryImage,
			@numDepCategory,
			@numCategoryProfileID,
			@vcCatgoryNameURL,
			@vcMetaTitle,
			@vcMetaKeywords,
			@vcMetaDescription
		) 
		set @numCatergoryId = SCOPE_IDENTITY();   
		insert into SiteCategories 
		(
			numSiteID,
			numCategoryID
		)
		SELECT
			numSiteID
			,@numCatergoryId
		FROM
			CategoryProfileSites
		WHERE
			 numCategoryProfileID=@numCategoryProfileID    
	END        
	ELSE IF @numCatergoryId>0        
	BEGIN
		UPDATE 
			Category 
		SET 
			vcCategoryName=@vcCatgoryName,
			vcDescription=@vcDescription,
			intDisplayOrder = @intDisplayOrder,
			tintLevel = @tintLevel,
			vcPathForCategoryImage = @vcPathForCategoryImage,
			numDepCategory = @numDepCategory,
			vcCategoryNameURL=@vcCatgoryNameURL,
			vcMetaTitle=@vcMetaTitle,
			vcMetaKeywords=@vcMetaKeywords,
			vcMetaDescription=@vcMetaDescription
		WHERE 
			numCategoryID=@numCatergoryId        
	END

	IF ISNULL(@vcMatrixGroups,'') = ''
	BEGIN
		DELETE FROM CategoryMatrixGroup WHERE numDomainID=@numDomainID AND numCategoryID=@numCatergoryId
	END
	ELSE
	BEGIN
		DELETE FROM CategoryMatrixGroup WHERE numDomainID=@numDomainID AND numCategoryID=@numCatergoryId AND numItemGroup NOT IN (SELECT Id FROM dbo.SplitIDs(@vcMatrixGroups,','))
	END

	INSERT INTO CategoryMatrixGroup
	(
		numDomainID,numCategoryID,numItemGroup
	)
	SELECT
		@numDomainID,@numCatergoryId,Temp.Id
	FROM
		dbo.SplitIDs(@vcMatrixGroups,',') Temp
	WHERE 
		Temp.Id NOT IN (SELECT numItemGroup FROM CategoryMatrixGroup WHERE numDomainID=@numDomainID AND numCategoryID=@numCatergoryId)
END	
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'usp_ManageInventory' ) 
    DROP PROCEDURE usp_ManageInventory
GO
CREATE PROCEDURE [dbo].[usp_ManageInventory]
    @itemcode AS NUMERIC(9) = 0,
    @numWareHouseItemID AS NUMERIC(9) = 0,
    @monAmount AS DECIMAL(20,5),
    @tintOpptype AS TINYINT,
    @numUnits AS FLOAT,
    @QtyShipped AS FLOAT,
    @QtyReceived AS FLOAT,
    @monPrice AS DECIMAL(20,5),
    @tintFlag AS TINYINT,  --1: SO/PO insert/edit 2:SO/PO Close 3:SO/PO Re-Open 4:Sales Fulfillment Release 5:Sales Fulfillment Revert
    @numOppID as numeric(9)=0,
    @numoppitemtCode AS NUMERIC(9)=0,
    @numUserCntID AS NUMERIC(9)=0,
	@bitWorkOrder AS BIT = 0   
AS 
	IF ISNULL(@numUserCntID,0) = 0
	BEGIN
		RAISERROR('INVALID_USERID',16,1)
		RETURN
	END

    DECLARE @onHand AS FLOAT                                    
    DECLARE @onOrder AS FLOAT 
    DECLARE @onBackOrder AS FLOAT
    DECLARE @onAllocation AS FLOAT
	DECLARE @onReOrder AS FLOAT
    DECLARE @monAvgCost AS DECIMAL(20,5) 
    DECLARE @bitKitParent BIT
	DECLARE @bitAssembly BIT
	DECLARE @numBuildProcessId NUMERIC(18,0)
	DECLARE @numOrigUnits AS NUMERIC		
	DECLARE @description AS VARCHAR(100)
	DECLARE @bitAsset as BIT
	DECLARE @numDomainID AS NUMERIC 
	DECLARE @dtReleaseDate DATETIME

	SELECT  
		@bitAsset=ISNULL(bitAsset,0)
		,@numDomainID=numDomainID
		,@monAvgCost = (CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(monAverageCost, 0) END)
		,@bitKitParent = (CASE 
							WHEN ISNULL(bitKitParent,0) = 1 AND ISNULL(bitAssembly,0) = 1 THEN 0
							WHEN ISNULL(bitKitParent,0) = 1 THEN 1
							ELSE 0
						END)
		,@bitAssembly=ISNULL(bitAssembly,0)
		,@numBuildProcessId = ISNULL(numBusinessProcessId,0)
    FROM 
		Item
    WHERE 
		numitemcode = @itemcode                                   
    
	SELECT 
		@dtReleaseDate=ItemReleaseDate 
	FROM 
		OpportunityItems 
	WHERE 
		numOppId=@numOppID 
		AND numoppitemtCode = @numoppitemtCode

    IF @numWareHouseItemID>0
    BEGIN                     
		SELECT  @onHand = ISNULL(numOnHand, 0),
				@onAllocation = ISNULL(numAllocation, 0),
				@onOrder = ISNULL(numOnOrder, 0),
				@onBackOrder = ISNULL(numBackOrder, 0),
				@onReOrder = ISNULL(numReorder,0)
		FROM    WareHouseItems
		WHERE   numWareHouseItemID = @numWareHouseItemID 
    END
   
   DECLARE @dtItemReceivedDate AS DATETIME;SET @dtItemReceivedDate=NULL
   
   DECLARE @TotalOnHand AS NUMERIC;SET @TotalOnHand=0  
   SELECT @TotalOnHand=(SUM(ISNULL(numOnHand,0)) + SUM(ISNULL(numAllocation,0))) FROM dbo.WareHouseItems WHERE numItemID=@itemcode
					
   SET @numOrigUnits=@numUnits
            
    IF @tintFlag = 1  --1: SO/PO insert/edit 
    BEGIN
        IF @tintOpptype = 1 
        BEGIN  
		--When change below code also change to USP_ManageWorkOrderStatus  

			/* When QtyShipped has been updated form sales fulfillment then it need to be deducted */
			SET @description=CONCAT('SO insert/edit (Qty:',@numUnits,' Shipped:',@QtyShipped,')')
				
			IF @bitKitParent = 1 
            BEGIN
                EXEC USP_UpdatingInventoryForEmbeddedKits @numoppitemtCode,@numOrigUnits, 0,@numOppID,0,@numUserCntID
            END
            ELSE
			BEGIN       
				SET @numUnits = @numUnits - @QtyShipped
                                                        
                IF @onHand >= @numUnits 
                BEGIN                                    
                    SET @onHand = @onHand - @numUnits                            
                    SET @onAllocation = @onAllocation + @numUnits                                    
                END                                    
                ELSE IF @onHand < @numUnits 
                BEGIN                                    
                    SET @onAllocation = @onAllocation + @onHand                                    
                    SET @onBackOrder = @onBackOrder + @numUnits - @onHand                                    
                    SET @onHand = 0       
                END    
			                                 

				IF @bitAsset=0--Not Asset
				BEGIN 
					UPDATE  WareHouseItems
					SET     numOnHand = @onHand,
							numAllocation = @onAllocation,
							numBackOrder = @onBackOrder,dtModified = GETDATE() 
					WHERE   numWareHouseItemID = @numWareHouseItemID          
				END
			END  
        END                       
        ELSE IF @tintOpptype = 2 
        BEGIN  
			SET @description=CONCAT('PO insert/edit (Qty:',@numUnits,' Received:',@QtyReceived,')')
		            
			/* When QtyReceived has been updated form purchase fulfillment then it need to be deducted */
            SET @numUnits = @numUnits - @QtyReceived   
                                             
            SET @onOrder = @onOrder + @numUnits 
		    
            UPDATE  
				WareHouseItems
            SET     
				numOnHand = @onHand,
                numAllocation = @onAllocation,
                numBackOrder = @onBackOrder,
                numOnOrder = @onOrder,dtModified = GETDATE() 
            WHERE   
				numWareHouseItemID = @numWareHouseItemID   
        END                                                                                                                                         
    END
    ELSE IF @tintFlag = 2 --2:SO/PO Close
    BEGIN
        PRINT '@OppType=' + CONVERT(VARCHAR(5), @tintOpptype)
		    
        IF @tintOpptype = 1 
        BEGIN
			SET @description=CONCAT('SO Close (Qty:',@numUnits,' Shipped:',@QtyShipped,')')
                    
            IF @bitKitParent = 1 
            BEGIN
                EXEC USP_UpdatingInventoryForEmbeddedKits @numoppitemtCode,
                    @numOrigUnits, 2,@numOppID,0,@numUserCntID
            END
            ELSE
            BEGIN       
				/* When QtyShipped has been updated form sales fulfillment then it need to be deducted */
				SET @numUnits = @numUnits - @QtyShipped
				IF @bitAsset=0--Not Asset
				BEGIN
					IF @onAllocation >= @numUnits
					BEGIN
						SET @onAllocation = @onAllocation - @numUnits 
					END
					ELSE
					BEGIN
						RAISERROR ('NOTSUFFICIENTQTY_ALLOCATION',16,1);
					END
				END
				ELSE--Asset
				BEGIN
					SET @onAllocation = @onAllocation  
				END
											           
				UPDATE 
					WareHouseItems
				SET 
					numOnHand = @onHand,
					numAllocation = @onAllocation,
					numBackOrder = @onBackOrder,
					dtModified = GETDATE() 
				WHERE 
					numWareHouseItemID = @numWareHouseItemID   
			END
        END                
        ELSE IF @tintOpptype = 2 
        BEGIN
			SET @description=CONCAT('PO Close (Qty:',@numUnits,' Received:',@QtyReceived,')')
                        
			/* When QtyReceived has been updated form purchase fulfillment then it need to be deducted */
            SET @numUnits = @numUnits - @QtyReceived
            
			If @numUnits > 0
			BEGIN
				IF EXISTS(SELECT 1 FROM dbo.OpportunityMaster WHERE numOppID=@numOppID AND ISNULL(bitStockTransfer,0)=0)
				BEGIN
					--Updating the Average Cost
					IF @TotalOnHand + @numUnits <= 0
					BEGIN
						SET @monAvgCost = @monAvgCost
					END
					ELSE
					BEGIN
						SET @monAvgCost = (( @TotalOnHand * @monAvgCost) + (@numUnits * @monPrice)) / ( @TotalOnHand + @numUnits )
					END    
		                            
					UPDATE 
						Item
					SET 
						monAverageCost = (CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE @monAvgCost END)
					WHERE 
						numItemCode = @itemcode
				END
		    
				IF @onOrder >= @numUnits 
				BEGIN
					SET @onOrder = @onOrder - @numUnits            
                
					IF @onBackOrder >= @numUnits 
					BEGIN            
						SET @onBackOrder = @onBackOrder - @numUnits            
						SET @onAllocation = @onAllocation + @numUnits            
					END            
					ELSE 
					BEGIN            
						SET @onAllocation = @onAllocation + @onBackOrder            
						SET @numUnits = @numUnits - @onBackOrder            
						SET @onBackOrder = 0            
						SET @onHand = @onHand + @numUnits            
					END         
				END            
				ELSE IF @onOrder < @numUnits 
				BEGIN
					RAISERROR('INVALID ON ORDER',16,1)
					RETURN
				END         

				UPDATE
					WareHouseItems
				SET 
					numOnHand = @onHand,
					numAllocation = @onAllocation,
					numBackOrder = @onBackOrder,
					numOnOrder = @onOrder,dtModified = GETDATE() 
				WHERE 
					numWareHouseItemID = @numWareHouseItemID  
			                
				SELECT @dtItemReceivedDate=dtItemReceivedDate FROM OpportunityMaster WHERE numOppId=@numOppId
			END
		END
    END
	ELSE IF @tintFlag = 3 --3:SO/PO Re-Open
    BEGIN
		PRINT '@OppType=' + CONVERT(VARCHAR(5), @tintOpptype)           
                
		IF @tintOpptype = 1 
        BEGIN
			SET @description=CONCAT('SO Re-Open (Qty:',@numUnits,' Shipped:',@QtyShipped,')')
                    
            IF @bitKitParent = 1 
            BEGIN
				EXEC USP_UpdatingInventoryForEmbeddedKits @numoppitemtCode,@numOrigUnits, 3,@numOppID,0,@numUserCntID
            END
            ELSE
            BEGIN
				/* When QtyShipped has been updated form sales fulfillment then it need to be deducted */
                SET @numUnits = @numUnits - @QtyShipped
  
				IF @bitAsset=0--Not Asset
				BEGIN
					SET @onAllocation = @onAllocation + @numUnits
				END
				ELSE--Asset
				BEGIN
					SET @onAllocation = @onAllocation 
				END
                                    
                UPDATE  
					WareHouseItems
                SET 
					numOnHand = @onHand,
                    numAllocation = @onAllocation,
                    numBackOrder = @onBackOrder,dtModified = GETDATE() 
                WHERE 
					numWareHouseItemID = @numWareHouseItemID
			END	               
        END                
        ELSE IF @tintOpptype = 2 
        BEGIN
			DECLARE @numDeletedReceievedQty AS NUMERIC(18,0)
			SELECT @numDeletedReceievedQty=ISNULL(numDeletedReceievedQty,0) FROM OpportunityItems WHERE numoppitemtcode=@numoppitemtCode
			UPDATE OpportunityItems SET numDeletedReceievedQty = 0 WHERE numoppitemtcode=@numoppitemtCode

			/* When QtyReceived has been updated form purchase fulfillment then it need to be deducted */
            SET @numUnits = @numUnits - @QtyReceived
            
			--Updating the Average Cost
			IF EXISTS(SELECT 1 FROM dbo.OpportunityMaster WHERE numOppID=@numOppID AND ISNULL(bitStockTransfer,0)=0)
			BEGIN
				IF @TotalOnHand - @numUnits <= 0
				BEGIN
					SET @monAvgCost = 0
				END
				ELSE
				BEGIN
					SET @monAvgCost = ((@TotalOnHand * @monAvgCost) - (@numUnits * @monPrice)) / ( @TotalOnHand - @numUnits )
				END        
				
				UPDATE  
					item
				SET 
					monAverageCost = (CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE @monAvgCost END)
				WHERE 
					numItemCode = @itemcode
			END


			DECLARE @TEMPReceievedItems TABLE
			(
				ID INT,
				numOIRLID NUMERIC(18,0),
				numWarehouseItemID NUMERIC(18,0),
				numUnitReceieved FLOAT,
				numDeletedReceievedQty FLOAT
			)

			INSERT INTO
				@TEMPReceievedItems
			SELECT
				ROW_NUMBER() OVER(ORDER BY ID),
				ID,
				numWarehouseItemID,
				numUnitReceieved,
				ISNULL(numDeletedReceievedQty,0)
			FROM
				OpportunityItemsReceievedLocation 
			WHERE
				numDomainID=@numDomainID
				AND numOppID=@numOppID
				AND numOppItemID=@numoppitemtCode

			DECLARE @i AS INT = 1
			DECLARE @COUNT AS INT
			DECLARE @numOIRLID NUMERIC(18,0)
			DECLARE @numTempWarehouseItemID NUMERIC(18,0)
			DECLARE @numTempUnitReceieved FLOAT
			DECLARE @numTempDeletedReceievedQty FLOAT

			SELECT @COUNT=COUNT(*) FROM @TEMPReceievedItems

			WHILE @i <= @COUNT
			BEGIN
				SELECT 
					@numOIRLID = numOIRLID,
					@numTempWarehouseItemID=ISNULL(numWarehouseItemID,0),
					@numTempUnitReceieved=ISNULL(numUnitReceieved,0),
					@numTempDeletedReceievedQty = numDeletedReceievedQty
				FROM 
					@TEMPReceievedItems 
				WHERE 
					ID=@i

				SET @description=CONCAT('PO Re-Open (Qty:',@numTempUnitReceieved,' Received:',@QtyReceived,')')

				UPDATE
					WareHouseItems
				SET
					numOnHand = ISNULL(numOnHand,0) - (ISNULL(@numTempUnitReceieved,0) - @numTempDeletedReceievedQty),
					dtModified = GETDATE()
				WHERE
					numWareHouseItemID=@numTempWarehouseItemID

				UPDATE OpportunityItemsReceievedLocation SET numDeletedReceievedQty = 0 WHERE ID=@numOIRLID

				EXEC dbo.USP_ManageWareHouseItems_Tracking
					@numWareHouseItemID = @numTempWarehouseItemID, --  numeric(9, 0)
					@numReferenceID = @numOppID, --  numeric(9, 0)
					@tintRefType = 4, --  tinyint
					@vcDescription = @description, --  varchar(100)
					@numModifiedBy = @numUserCntID,
					@dtRecordDate =  @dtItemReceivedDate,
					@numDomainID = @numDomainID

				SET @i = @i + 1
			END

			DELETE FROM OpportunityItemsReceievedLocation WHERE numOppID=@numOppID AND numOppItemID=@numoppitemtCode

			SET @description=CONCAT('PO Re-Open (Qty:',@numUnits,' Received:',@QtyReceived,' Deleted:',@numDeletedReceievedQty,')')
             
			UPDATE  
				WareHouseItems
            SET 
				numOnHand = @onHand - (@numUnits-@numDeletedReceievedQty)
                ,numOnOrder = @onOrder + @numUnits
				,dtModified = GETDATE() 
            WHERE 
				numWareHouseItemID = @numWareHouseItemID
				AND ISNULL(numWLocationID,0) <> -1    
				
			UPDATE  
				WareHouseItems
            SET 
                numOnOrder = @onOrder + @numUnits
				,dtModified = GETDATE() 
            WHERE 
				numWareHouseItemID = @numWareHouseItemID
				AND ISNULL(numWLocationID,0) = -1    
        END
    END
            
            
            
    IF @numWareHouseItemID>0 AND @description <> 'DO NOT ADD WAREHOUSE TRACKING'
	BEGIN
		DECLARE @tintRefType AS TINYINT
					
		IF @tintOpptype = 1 --SO
			SET @tintRefType=3
		ELSE IF @tintOpptype = 2 --PO
			SET @tintRefType=4
					  
		DECLARE @numDomain AS NUMERIC(18,0)
		SET @numDomain = (SELECT TOP 1 [numDomainID] FROM WareHouseItems WHERE [WareHouseItems].[numWareHouseItemID] = @numWareHouseItemID)

		EXEC dbo.USP_ManageWareHouseItems_Tracking
			@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
			@numReferenceID = @numOppID, --  numeric(9, 0)
			@tintRefType = @tintRefType, --  tinyint
			@vcDescription = @description, --  varchar(100)
			@numModifiedBy = @numUserCntID,
			@dtRecordDate =  @dtItemReceivedDate,
			@numDomainID = @numDomain
    END
GO
GO
/****** Object:  StoredProcedure [dbo].[USP_ManageItemsAndKits]    Script Date: 02/15/2010 21:44:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Anoop Jayaraj                                                          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_manageitemsandkits')
DROP PROCEDURE usp_manageitemsandkits
GO
CREATE PROCEDURE [dbo].[USP_ManageItemsAndKits]                                                                
@numItemCode as numeric(9) output,                                                                
@vcItemName as varchar(300),                                                                
@txtItemDesc as varchar(2000),                                                                
@charItemType as char(1),                                                                
@monListPrice as DECIMAL(20,5),                                                                
@numItemClassification as numeric(9),                                                                
@bitTaxable as bit,                                                                
@vcSKU as varchar(50),                                                                                                                                      
@bitKitParent as bit,                                                                                   
--@dtDateEntered as datetime,                                                                
@numDomainID as numeric(9),                                                                
@numUserCntID as numeric(9),                                                                                                  
@numVendorID as numeric(9),                                                            
@bitSerialized as bit,                                               
@strFieldList as text,                                            
@vcModelID as varchar(200)='' ,                                            
@numItemGroup as numeric(9)=0,                                      
@numCOGSChartAcntId as numeric(9)=0,                                      
@numAssetChartAcntId as numeric(9)=0,                                      
@numIncomeChartAcntId as numeric(9)=0,                            
@monAverageCost as DECIMAL(20,5),                          
@monLabourCost as DECIMAL(20,5),                  
@fltWeight as float,                  
@fltHeight as float,                  
@fltLength as float,                  
@fltWidth as float,                  
@bitFreeshipping as bit,                
@bitAllowBackOrder as bit,              
@UnitofMeasure as varchar(30)='',              
@strChildItems as text,            
@bitShowDeptItem as bit,              
@bitShowDeptItemDesc as bit,        
@bitCalAmtBasedonDepItems as bit,    
@bitAssembly as BIT,
@intWebApiId INT=null,
@vcApiItemId AS VARCHAR(50)=null,
@numBarCodeId as VARCHAR(25)=null,
@vcManufacturer as varchar(250)=NULL,
@numBaseUnit AS NUMERIC(18)=0,
@numPurchaseUnit AS NUMERIC(18)=0,
@numSaleUnit AS NUMERIC(18)=0,
@bitLotNo as BIT,
@IsArchieve AS BIT,
@numItemClass as numeric(9)=0,
@tintStandardProductIDType AS TINYINT=0,
@vcExportToAPI VARCHAR(50),
@numShipClass NUMERIC,
@ProcedureCallFlag SMALLINT =0,
@vcCategories AS VARCHAR(2000) = '',
@bitAllowDropShip AS BIT=0,
@bitArchiveItem AS BIT = 0,
@bitAsset AS BIT = 0,
@bitRental AS BIT = 0,
@numCategoryProfileID AS NUMERIC(18,0) = 0,
@bitVirtualInventory BIT,
@bitContainer BIT,
@numContainer NUMERIC(18,0)=0,
@numNoItemIntoContainer NUMERIC(18,0),
@bitMatrix BIT = 0 ,
@vcItemAttributes VARCHAR(2000) = '',
@numManufacturer NUMERIC(18,0),
@vcASIN as varchar(50),
@tintKitAssemblyPriceBasedOn TINYINT = 1,
@fltReorderQty FLOAT = 0,
@bitSOWorkOrder BIT = 0,
@bitKitSingleSelect BIT = 0,
@bitExpenseItem BIT = 0,
@numExpenseChartAcntId NUMERIC(18,0) = 0,
@numBusinessProcessId NUMERIC(18,0)=0,
@bitTimeContractFromSalesOrder BIT =0 
AS
BEGIN     
	SET @vcManufacturer = CASE WHEN ISNULL(@numManufacturer,0) > 0 THEN ISNULL((SELECT vcCompanyName FROM DivisionMaster INNER JOIN CompanyInfo ON DivisionMaster.numCompanyID=CompanyInfo.numCompanyId WHERE numDivisionID=@numManufacturer),'')  ELSE ISNULL(@vcManufacturer,'') END

	DECLARE @bitAttributesChanged AS BIT  = 1
	DECLARE @bitOppOrderExists AS BIT = 0

	IF ISNULL(@numItemCode,0) > 0
	BEGIN
		IF (SELECT COUNT(OI.numOppId) FROM dbo.OpportunityItems OI WHERE OI.numItemCode =@numItemCode) > 0
		BEGIN
			SET @bitOppOrderExists = 1
		END
		ELSE IF (SELECT COUNT(OI.numOppId) FROM dbo.OpportunityKitItems OI WHERE OI.numChildItemID =@numItemCode) > 0
		BEGIN
			SET @bitOppOrderExists = 1
		END
		ELSE IF (SELECT COUNT(OI.numOppId) FROM dbo.OpportunityKitChildItems OI WHERE OI.numItemID =@numItemCode) > 0
		BEGIN
			SET @bitOppOrderExists = 1
		END
	END
	ELSE 
	BEGIN
		IF (ISNULL(@vcItemName,'') = '')
		BEGIN
			RAISERROR('ITEM_NAME_NOT_SELECTED',16,1)
			RETURN
		END
        ELSE IF (ISNULL(@charItemType,'') = '0')
		BEGIN
			RAISERROR('ITEM_TYPE_NOT_SELECTED',16,1)
			RETURN
		END
	END

	If (@charItemType = 'P' AND ISNULL(@bitKitParent,0) = 0)
	BEGIN
		IF ((SELECT COUNT(*) FROM Chart_Of_Accounts WHERE numDomainId=@numDomainID AND numAccountId=@numAssetChartAcntId) = 0)
		BEGIN
			RAISERROR('INVALID_ASSET_ACCOUNT',16,1)
			RETURN
		END
	END

	If @charItemType = 'P' OR ISNULL((SELECT bitExpenseNonInventoryItem FROM Domain WHERE numDomainID=@numDomainID),0) = 1
	BEGIN
		-- This is common check for CharItemType P and Other
		IF ((SELECT COUNT(*) FROM Chart_Of_Accounts WHERE numDomainId=@numDomainID AND numAccountId=@numCOGSChartAcntId) = 0)
		BEGIN
			RAISERROR('INVALID_COGS_ACCOUNT',16,1)
			RETURN
		END
	END

	IF ((SELECT COUNT(*) FROM Chart_Of_Accounts WHERE numDomainId=@numDomainID AND numAccountId=@numIncomeChartAcntId) = 0)
	BEGIN
		RAISERROR('INVALID_INCOME_ACCOUNT',16,1)
		RETURN
	END

	IF ISNULL(@bitExpenseItem,0) = 1
	BEGIN
		IF ((SELECT COUNT(*) FROM Chart_Of_Accounts WHERE numDomainId=@numDomainID AND numAccountId=@numExpenseChartAcntId) = 0)
		BEGIN
			RAISERROR('INVALID_EXPENSE_ACCOUNT',16,1)
			RETURN
		END
	END

	--INSERT CUSTOM FIELDS BASE ON WAREHOUSEITEMS
	DECLARE @hDoc AS INT     
	                                                                        	
	DECLARE @TempTable TABLE 
	(
		ID INT IDENTITY(1,1),
		Fld_ID NUMERIC(18,0),
		Fld_Value NUMERIC(18,0)
	)   

	IF ISNULL(@bitMatrix,0) = 1 AND ISNULL(@numItemGroup,0) = 0
	BEGIN
		RAISERROR('ITEM_GROUP_NOT_SELECTED',16,1)
		RETURN
	END
	ELSE IF ISNULL(@bitMatrix,0) = 1 AND LEN(ISNULL(@vcItemAttributes,'')) = 0 
	BEGIN
		RAISERROR('ITEM_ATTRIBUTES_NOT_SELECTED',16,1)
		RETURN
	END
	ELSE IF ISNULL(@bitMatrix,0) = 1 
	BEGIN
		DECLARE @bitDuplicate AS BIT = 0
	
		IF DATALENGTH(ISNULL(@vcItemAttributes,''))>2
		BEGIN
			SET @vcItemAttributes = CONCAT('<?xml version="1.0" encoding="iso-8859-1"?>',@vcItemAttributes)

			EXEC sp_xml_preparedocument @hDoc OUTPUT, @vcItemAttributes
	                                      
			INSERT INTO @TempTable 
			(
				Fld_ID,
				Fld_Value
			)    
			SELECT
				*          
			FROM 
				OPENXML (@hDoc,'/NewDataSet/CusFlds',2)                                                  
			WITH 
				(Fld_ID NUMERIC(18,0),Fld_Value NUMERIC(18,0)) 
			ORDER BY 
				Fld_ID       

			DECLARE @strAttribute VARCHAR(500) = ''
			DECLARE @i AS INT = 1
			DECLARE @COUNT AS INT 
			DECLARE @numFldID AS NUMERIC(18,0)
			DECLARE @numFldValue AS NUMERIC(18,0)
			SELECT @COUNT = COUNT(*) FROM @TempTable

			WHILE @i <= @COUNT
			BEGIN
				SELECT @numFldID=Fld_ID,@numFldValue=Fld_Value FROM @TempTable WHERE ID=@i

				IF ISNUMERIC(@numFldValue)=1
				BEGIN
					SELECT @strAttribute=@strAttribute+Fld_label FROM CFW_Fld_Master WHERE Fld_id=@numFldID AND Grp_id = 9
                
					IF LEN(@strAttribute) > 0
					BEGIN
						SELECT @strAttribute=@strAttribute+':'+ vcData +',' FROM ListDetails WHERE numListItemID=@numFldValue
					END
				END

				SET @i = @i + 1
			END

			SET @strAttribute = SUBSTRING(@strAttribute,0,LEN(@strAttribute))

			IF
				(SELECT
					COUNT(*)
				FROM
					Item
				INNER JOIN
					ItemAttributes
				ON
					Item.numItemCode = ItemAttributes.numItemCode
					AND ItemAttributes.numDomainID = @numDomainID                     
				WHERE
					Item.numItemCode <> @numItemCode
					AND Item.vcItemName = @vcItemName
					AND Item.numItemGroup = @numItemGroup
					AND Item.numDomainID = @numDomainID
					AND dbo.fn_GetItemAttributes(@numDomainID,Item.numItemCode,0) = @strAttribute
				) > 0
			BEGIN
				RAISERROR('ITEM_WITH_ATTRIBUTES_ALREADY_EXISTS',16,1)
				RETURN
			END

			If ISNULL(@numItemCode,0) > 0 AND dbo.fn_GetItemAttributes(@numDomainID,@numItemCode,0) = @strAttribute
			BEGIN
				SET @bitAttributesChanged = 0
			END

			EXEC sp_xml_removedocument @hDoc 
		END	
	END
	ELSE IF ISNULL(@bitMatrix,0) = 0
	BEGIN
		-- REMOVE ATTRIBUTES CONFIGURATION OF ITEM AND WAREHOUSE
		DELETE FROM ItemAttributes WHERE numDomainID=@numDomainID AND numItemCode=@numItemCode
	END

	IF ISNULL(@numItemGroup,0) > 0 AND ISNULL(@bitMatrix,0) = 1
	BEGIN
		IF (SELECT COUNT(*) FROM ItemGroupsDTL WHERE numItemGroupID = @numItemGroup AND tintType=2) = 0
		BEGIN
			RAISERROR('ITEM_GROUP_NOT_CONFIGURED',16,1)
			RETURN
		END
		ELSE IF EXISTS (SELECT 
							CFM.Fld_id
						FROM 
							CFW_Fld_Master CFM
						LEFT JOIN
							ListDetails LD
						ON
							CFM.numlistid = LD.numListID
						WHERE
							CFM.numDomainID = @numDomainID
							AND CFM.Fld_id IN (SELECT numOppAccAttrID FROM ItemGroupsDTL WHERE numItemGroupID=@numItemGroup AND tintType = 2)
						GROUP BY
							CFM.Fld_id HAVING COUNT(LD.numListItemID) = 0)
		BEGIN
			RAISERROR('ITEM_GROUP_NOT_CONFIGURED',16,1)
			RETURN
		END
	END

BEGIN TRY
BEGIN TRANSACTION

	

	IF @numCOGSChartAcntId=0 SET @numCOGSChartAcntId=NULL
	IF @numAssetChartAcntId=0 SET @numAssetChartAcntId=NULL  
	IF @numIncomeChartAcntId=0 SET @numIncomeChartAcntId=NULL     
	IF @numExpenseChartAcntId=0 SET @numExpenseChartAcntId=NULL

	DECLARE @ParentSKU VARCHAR(50) = @vcSKU                        
	DECLARE @ItemID AS NUMERIC(9)
	DECLARE @cnt AS INT

	IF ISNULL(@charItemType,'') = '' OR ISNULL(@charItemType,'') = '0'  
		SET @charItemType = 'N'

	IF @intWebApiId = 2 AND LEN(ISNULL(@vcApiItemId, '')) > 0 AND ISNULL(@numItemGroup, 0) > 0 
    BEGIN 
        -- check wether this id already Mapped in ITemAPI 
        SELECT 
			@cnt = COUNT([numItemID])
        FROM 
			[ItemAPI]
        WHERE 
			[WebApiId] = @intWebApiId
            AND [numDomainId] = @numDomainID
            AND [vcAPIItemID] = @vcApiItemId

        IF @cnt > 0 
        BEGIN
            SELECT 
				@ItemID = [numItemID]
            FROM 
				[ItemAPI]
            WHERE 
				[WebApiId] = @intWebApiId
                AND [numDomainId] = @numDomainID
                AND [vcAPIItemID] = @vcApiItemId
        
			--Update Existing Product coming from API
            SET @numItemCode = @ItemID
        END
    END
	ELSE IF @intWebApiId > 1 AND LEN(ISNULL(@vcSKU, '')) > 0 
    BEGIN
        SET @ParentSKU = @vcSKU
		 
        SELECT 
			@cnt = COUNT([numItemCode])
        FROM 
			[Item]
        WHERE 
			[numDomainId] = @numDomainID
            AND (vcSKU = @vcSKU OR @vcSKU IN (SELECT vcWHSKU FROM dbo.WareHouseItems WHERE numItemID = Item.[numItemCode] AND vcWHSKU = @vcSKU))
            
		IF @cnt > 0 
        BEGIN
            SELECT 
				@ItemID = [numItemCode],
                @ParentSKU = vcSKU
            FROM 
				[Item]
            WHERE 
				[numDomainId] = @numDomainID
                AND (vcSKU = @vcSKU OR @vcSKU IN (SELECT vcWHSKU FROM dbo.WareHouseItems WHERE numItemID = Item.[numItemCode] AND vcWHSKU = @vcSKU))

            SET @numItemCode = @ItemID
        END
		ELSE 
		BEGIN
			-- check wether this id already Mapped in ITemAPI 
			SELECT 
				@cnt = COUNT([numItemID])
			FROM 
				[ItemAPI]
			WHERE 
				[WebApiId] = @intWebApiId
				AND [numDomainId] = @numDomainID
				AND [vcAPIItemID] = @vcApiItemId
                    
			IF @cnt > 0 
			BEGIN
				SELECT 
					@ItemID = [numItemID]
				FROM 
					[ItemAPI]
				WHERE 
					[WebApiId] = @intWebApiId
					AND [numDomainId] = @numDomainID
					AND [vcAPIItemID] = @vcApiItemId
        
				--Update Existing Product coming from API
				SET @numItemCode = @ItemID
			END
		END
    END
                                                         
	IF @numItemCode=0 OR @numItemCode IS NULL
	BEGIN
		INSERT INTO Item 
		(                                             
			vcItemName,txtItemDesc,charItemType,monListPrice,numItemClassification,bitTaxable,vcSKU,bitKitParent,
			numVendorID,numDomainID,numCreatedBy,bintCreatedDate,bintModifiedDate,numModifiedBy,bitSerialized,
			vcModelID,numItemGroup,numCOGsChartAcntId,numAssetChartAcntId,numIncomeChartAcntId,monAverageCost,                          
			monCampaignLabourCost,fltWeight,fltHeight,fltWidth,fltLength,bitFreeShipping,bitAllowBackOrder,vcUnitofMeasure,
			bitShowDeptItem,bitShowDeptItemDesc,bitCalAmtBasedonDepItems,bitAssembly,numBarCodeId,vcManufacturer,numBaseUnit,
			numPurchaseUnit,numSaleUnit,bitLotNo,IsArchieve,numItemClass,tintStandardProductIDType,vcExportToAPI,numShipClass,
			bitAllowDropShip,bitArchiveItem,bitAsset,bitRental,bitVirtualInventory,bitContainer,numContainer,numNoItemIntoContainer
			,bitMatrix,numManufacturer,vcASIN,tintKitAssemblyPriceBasedOn,fltReorderQty,bitSOWorkOrder,bitKitSingleSelect,bitExpenseItem,numExpenseChartAcntId,numBusinessProcessId,bitTimeContractFromSalesOrder 
		)                                                              
		VALUES                                                                
		(                                                                
			@vcItemName,@txtItemDesc,@charItemType,@monListPrice,@numItemClassification,@bitTaxable,@ParentSKU,@bitKitParent,
			@numVendorID,@numDomainID,@numUserCntID,getutcdate(),getutcdate(),@numUserCntID,@bitSerialized,@vcModelID,@numItemGroup,                                      
			@numCOGsChartAcntId,@numAssetChartAcntId,@numIncomeChartAcntId,@monAverageCost,@monLabourCost,@fltWeight,@fltHeight,@fltWidth,                  
			@fltLength,@bitFreeshipping,@bitAllowBackOrder,@UnitofMeasure,@bitShowDeptItem,@bitShowDeptItemDesc,@bitCalAmtBasedonDepItems,    
			@bitAssembly,@numBarCodeId,@vcManufacturer,@numBaseUnit,@numPurchaseUnit,@numSaleUnit,@bitLotNo,@IsArchieve,@numItemClass,
			@tintStandardProductIDType,@vcExportToAPI,@numShipClass,@bitAllowDropShip,@bitArchiveItem ,@bitAsset,@bitRental,
			@bitVirtualInventory,@bitContainer,@numContainer,@numNoItemIntoContainer
			,@bitMatrix,@numManufacturer,@vcASIN,@tintKitAssemblyPriceBasedOn,@fltReorderQty,@bitSOWorkOrder,@bitKitSingleSelect,@bitExpenseItem,@numExpenseChartAcntId,@numBusinessProcessId,@bitTimeContractFromSalesOrder 
		)                                             
 
		SET @numItemCode = SCOPE_IDENTITY()
  
		IF  @intWebApiId > 1 
		BEGIN
			 -- insert new product
			 --insert this id into linking table
			 INSERT INTO [ItemAPI] (
	 			[WebApiId],
	 			[numDomainId],
	 			[numItemID],
	 			[vcAPIItemID],
	 			[numCreatedby],
	 			[dtCreated],
	 			[numModifiedby],
	 			[dtModified],vcSKU
			 ) VALUES     
			 (
				@intWebApiId,
				@numDomainID,
				@numItemCode,
				@vcApiItemId,
	 			@numUserCntID,
	 			GETUTCDATE(),
	 			@numUserCntID,
	 			GETUTCDATE(),@vcSKU
	 		) 
		END
 
		IF @charItemType = 'P'
		BEGIN
			
			DECLARE @TEMPWarehouse TABLE
			(
				ID INT IDENTITY(1,1)
				,numWarehouseID NUMERIC(18,0)
			)
		
			INSERT INTO @TEMPWarehouse (numWarehouseID) SELECT numWareHouseID FROM Warehouses WHERE numDomainID=@numDomainID

			DECLARE @j AS INT = 1
			DECLARE @jCount AS INT
			DECLARE @numTempWarehouseID NUMERIC(18,0)
			SELECT @jCount = COUNT(*) FROM @TEMPWarehouse

			WHILE @j <= @jCount
			BEGIN
				SELECT @numTempWarehouseID=numWarehouseID FROM @TEMPWarehouse WHERE ID=@j

				EXEC USP_WarehouseItems_Save @numDomainID,
											@numUserCntID,
											0,
											@numTempWarehouseID,
											0,
											@numItemCode,
											'',
											@monListPrice,
											0,
											0,
											'',
											'',
											'',
											'',
											0
 
				SET @j = @j + 1
			END
		END 
	END         
	ELSE IF @numItemCode>0 AND @intWebApiId > 0 
	BEGIN 
		IF @ProcedureCallFlag = 1 
		BEGIN
			DECLARE @ExportToAPIList AS VARCHAR(30)
			SELECT @ExportToAPIList = vcExportToAPI FROM dbo.Item WHERE numItemCode = @numItemCode

			IF @ExportToAPIList != ''
			BEGIN
				IF NOT EXISTS(select * from (SELECT * FROM dbo.Split(@ExportToAPIList ,','))as A where items= @vcExportToAPI ) 
				BEGIN
					SET @vcExportToAPI = @ExportToAPIList + ',' + @vcExportToAPI
				End
				ELSE
				BEGIN
					SET @vcExportToAPI = @ExportToAPIList 
				End
			END

			--update ExportToAPI String value
			UPDATE dbo.Item SET vcExportToAPI=@vcExportToAPI where numItemCode=@numItemCode  AND numDomainID=@numDomainID
					
			SELECT 
				@cnt = COUNT([numItemID]) 
			FROM 
				[ItemAPI] 
			WHERE  
				[WebApiId] = @intWebApiId 
				AND [numDomainId] = @numDomainID
				AND [vcAPIItemID] = @vcApiItemId
				AND numItemID = @numItemCode

			IF @cnt > 0
			BEGIN
				--update lastUpdated Date 
				UPDATE [ItemAPI] SET [dtModified] =GETUTCDATE() WHERE [WebApiId] =@intWebApiId AND [numDomainId] = @numDomainID
			END
			ELSE
			--Insert ItemAPI mapping
			BEGIN
				INSERT INTO [ItemAPI] 
				(
					[WebApiId],
					[numDomainId],
					[numItemID],
					[vcAPIItemID],
					[numCreatedby],
					[dtCreated],
					[numModifiedby],
					[dtModified],vcSKU
				) VALUES 
				(
					@intWebApiId,
					@numDomainID,
					@numItemCode,
					@vcApiItemId,
					@numUserCntID,
					GETUTCDATE(),
					@numUserCntID,
					GETUTCDATE(),@vcSKU
				)	
			END
		END   
	END                                            
	ELSE IF @numItemCode > 0 AND @intWebApiId <= 0                                                           
	BEGIN
		DECLARE @OldGroupID NUMERIC 
		SELECT @OldGroupID = ISNULL(numItemGroup,0) FROM [Item] WHERE numitemcode = @numItemCode AND [numDomainId] = @numDomainID
	
		DECLARE @monOldAverageCost AS DECIMAL(18,4);SET @monOldAverageCost=0
		SELECT @monOldAverageCost=(CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(monAverageCost,0) END) FROM Item WHERE numItemCode =@numItemCode AND [numDomainId] = @numDomainID
	   
		UPDATE 
			Item 
		SET 
			vcItemName=@vcItemName,txtItemDesc=@txtItemDesc,charItemType=@charItemType,monListPrice= @monListPrice,numItemClassification=@numItemClassification,                                             
			bitTaxable=@bitTaxable,vcSKU = (CASE WHEN ISNULL(@ParentSKU,'') = '' THEN @vcSKU ELSE @ParentSKU END),bitKitParent=@bitKitParent,                                             
			numVendorID=@numVendorID,numDomainID=@numDomainID,bintModifiedDate=getutcdate(),numModifiedBy=@numUserCntID,bitSerialized=@bitSerialized,                                                         
			vcModelID=@vcModelID,numItemGroup=@numItemGroup,numCOGsChartAcntId=@numCOGsChartAcntId,numAssetChartAcntId=@numAssetChartAcntId,                                      
			numIncomeChartAcntId=@numIncomeChartAcntId,monCampaignLabourCost=@monLabourCost,fltWeight=@fltWeight,fltHeight=@fltHeight,fltWidth=@fltWidth,                  
			fltLength=@fltLength,bitFreeShipping=@bitFreeshipping,bitAllowBackOrder=@bitAllowBackOrder,vcUnitofMeasure=@UnitofMeasure,bitShowDeptItem=@bitShowDeptItem,            
			bitShowDeptItemDesc=@bitShowDeptItemDesc,bitCalAmtBasedonDepItems=@bitCalAmtBasedonDepItems,bitAssembly=@bitAssembly,numBarCodeId=@numBarCodeId,
			vcManufacturer=@vcManufacturer,numBaseUnit=@numBaseUnit,numPurchaseUnit=@numPurchaseUnit,numSaleUnit=@numSaleUnit,bitLotNo=@bitLotNo,
			IsArchieve = @IsArchieve,numItemClass=@numItemClass,tintStandardProductIDType=@tintStandardProductIDType,vcExportToAPI=@vcExportToAPI,
			numShipClass=@numShipClass,bitAllowDropShip=@bitAllowDropShip, bitArchiveItem = @bitArchiveItem,bitAsset=@bitAsset,bitRental=@bitRental,
			bitVirtualInventory = @bitVirtualInventory,numContainer=@numContainer,numNoItemIntoContainer=@numNoItemIntoContainer,bitMatrix=@bitMatrix,numManufacturer=@numManufacturer,
			vcASIN = @vcASIN,tintKitAssemblyPriceBasedOn=@tintKitAssemblyPriceBasedOn,fltReorderQty=@fltReorderQty,bitSOWorkOrder=@bitSOWorkOrder,bitKitSingleSelect=@bitKitSingleSelect
			,bitExpenseItem=@bitExpenseItem,numExpenseChartAcntId=@numExpenseChartAcntId,numBusinessProcessId=@numBusinessProcessId,bitTimeContractFromSalesOrder =@bitTimeContractFromSalesOrder 
		WHERE 
			numItemCode=@numItemCode 
			AND numDomainID=@numDomainID

		IF NOT EXISTS(SELECT numItemCode FROM dbo.OpportunityItems WHERE numItemCode = @numItemCode)
		BEGIN
			IF (((SELECT ISNULL(SUM(numOnHand),0) FROM WareHouseItems WHERE numItemID = @numItemCode AND numDomainId=@numDomainID) = 0) AND ((SELECT ISNULL(bitVirtualInventory,0) FROM Item  WHERE numItemCode = @numItemCode AND numDomainId=@numDomainID) = 0))
			BEGIN
				UPDATE Item SET monAverageCost=@monAverageCost  where numItemCode=@numItemCode  AND numDomainID=@numDomainID	
			END
		END
	
		--If Average Cost changed then add in Tracking
		IF ISNULL(@monAverageCost,0) != ISNULL(@monOldAverageCost,0)
		BEGIN
			DECLARE @numWareHouseItemID NUMERIC(9);SET @numWareHouseItemID=0
			DECLARE @numTotal int;SET @numTotal=0
			SET @i=0
			DECLARE @vcDescription AS VARCHAR(100);SET @vcDescription=''
		
			SELECT @numTotal=COUNT(*) FROM WareHouseItems where numItemID=@numItemCode AND numDomainId=@numDomainId
	     
			WHILE @i < @numTotal
			BEGIN
				SELECT @numWareHouseItemID = min(numWareHouseItemID) FROM WareHouseItems 
					WHERE numItemID=@numItemCode AND numDomainId=@numDomainId and numWareHouseItemID > @numWareHouseItemID 
				
				IF @numWareHouseItemID>0
				BEGIN
					SET @vcDescription='Average Cost Changed Manually OLD : ' + CAST(ISNULL(@monOldAverageCost,0) AS VARCHAR(20))
				
					DECLARE @numDomain AS NUMERIC(18,0)
					SET @numDomain = @numDomainID
					EXEC dbo.USP_ManageWareHouseItems_Tracking
						@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)		
						@numReferenceID = @numItemCode, --  numeric(9, 0)
						@tintRefType = 1, --  tinyint
						@vcDescription = @vcDescription, --  varchar(100)
						@tintMode = 0, --  tinyint
						@numModifiedBy = @numUserCntID, --  numeric(9, 0)
						@numDomainID = @numDomain
				END
		    
				SET @i = @i + 1
			END
		END
 
		DECLARE @bitCartFreeShipping as  bit 
		SELECT @bitCartFreeShipping = bitFreeShipping FROM dbo.CartItems where numItemCode = @numItemCode  AND numDomainID = @numDomainID 
 
		IF @bitCartFreeShipping <> @bitFreeshipping
		BEGIN
			UPDATE dbo.CartItems SET bitFreeShipping = @bitFreeshipping WHERE numItemCode = @numItemCode  AND numDomainID=@numDomainID
		END 
 
		IF @intWebApiId > 1 
		BEGIN
			SELECT 
				@cnt = COUNT([numItemID])
			FROM 
				[ItemAPI]
			WHERE 
				[WebApiId] = @intWebApiId
				AND [numDomainId] = @numDomainID
				AND [vcAPIItemID] = @vcApiItemId

			IF @cnt > 0
			BEGIN
				--update lastUpdated Date 
				UPDATE [ItemAPI] SET [dtModified] =GETUTCDATE() WHERE [WebApiId] =@intWebApiId AND [numDomainId] = @numDomainID
			END
			ELSE --Insert ItemAPI mapping
			BEGIN
				INSERT INTO [ItemAPI] 
				(
	 				[WebApiId],
	 				[numDomainId],
	 				[numItemID],
	 				[vcAPIItemID],
	 				[numCreatedby],
	 				[dtCreated],
	 				[numModifiedby],
	 				[dtModified],vcSKU
				 )
				 VALUES 
				 (
					@intWebApiId,
					@numDomainID,
					@numItemCode,
					@vcApiItemId,
	 				@numUserCntID,
	 				GETUTCDATE(),
	 				@numUserCntID,
	 				GETUTCDATE(),@vcSKU
	 			) 
			END
		END   
		                                                                            
		-- kishan It is necessary to add item into itemTax table . 
		IF	@bitTaxable = 1
		BEGIN
			IF NOT EXISTS(SELECT * FROM dbo.ItemTax WHERE numItemCode = @numItemCode AND numTaxItemID = 0)
			BEGIN
			    INSERT INTO dbo.ItemTax 
				(
					numItemCode,
					numTaxItemID,
					bitApplicable
				)
				VALUES 
				( 
					@numItemCode,
					0,
					1 
				) 						
			END
		END	 
      
		IF @charItemType='S' or @charItemType='N'                                                                       
		BEGIN
			DELETE FROM WareHouseItmsDTL where numWareHouseItemID in (select numWareHouseItemID from WareHouseItems where numItemID=@numItemCode AND [WareHouseItems].[numDomainID] = @numDomainID)
			DELETE FROM WareHouseItems where numItemID=@numItemCode AND [WareHouseItems].[numDomainID] = @numDomainID AND numWareHouseItemID NOT IN (SELECT OI.numWarehouseItmsID FROM OpportunityItems OI WHERE OI.numItemCode=@numItemCode)
		END                                      
   
		IF @bitKitParent=1 OR @bitAssembly = 1              
		BEGIN              
			IF DATALENGTH(ISNULL(@strChildItems,'')) > 0
			BEGIN
				SET @strChildItems = CONCAT('<?xml version="1.0" encoding="iso-8859-1"?>',@strChildItems)
			END

			DECLARE @hDoc1 AS INT                                            
			EXEC sp_xml_preparedocument @hDoc1 OUTPUT, @strChildItems                                                                        
                                             
			UPDATE 
				ItemDetails 
			SET
				numQtyItemsReq=X.QtyItemsReq
				,numUOMId=X.numUOMId
				,vcItemDesc=X.vcItemDesc
				,sintOrder=X.sintOrder                                                                                          
			From 
			(
				SELECT 
					QtyItemsReq,ItemKitID,ChildItemID,numUOMId,vcItemDesc,sintOrder, numItemDetailID As ItemDetailID                             
				FROM 
					OPENXML(@hDoc1,'/NewDataSet/Table1',2)                                                                         
				WITH
				(
					ItemKitID NUMERIC(9),                                                          
					ChildItemID NUMERIC(9),                                                                        
					QtyItemsReq DECIMAL(30, 16),
					numUOMId NUMERIC(9),
					vcItemDesc VARCHAR(1000),
					sintOrder int,
					numItemDetailID NUMERIC(18,0)
				)
			)X                                                                         
			WHERE 
				numItemKitID=X.ItemKitID 
				AND numChildItemID=ChildItemID 
				AND numItemDetailID = X.ItemDetailID

			EXEC sp_xml_removedocument @hDoc1
		 END   
		 ELSE
		 BEGIN
 			DELETE FROM ItemDetails WHERE numItemKitID=@numItemCode
		 END                                                
	END

	IF ISNULL(@numItemGroup,0) = 0 OR ISNULL(@bitMatrix,0) = 1
	BEGIN
		UPDATE WareHouseItems SET monWListPrice = @monListPrice, vcWHSKU=@vcSKU, vcBarCode=@numBarCodeId WHERE numDomainID = @numDomainID AND numItemID = @numItemCode
	END

	-- SAVE MATRIX ITEM ATTRIBUTES
	IF @numItemCode > 0 AND ISNULL(@bitAttributesChanged,0)=1 AND ISNULL(@bitMatrix,0) = 1 AND LEN(ISNULL(@vcItemAttributes,'')) > 0 AND ISNULL(@bitOppOrderExists,0) <> 1 AND (SELECT COUNT(*) FROM @TempTable) > 0
	BEGIN
		-- DELETE PREVIOUS ITEM ATTRIBUTES
		DELETE FROM ItemAttributes WHERE numDomainID=@numDomainID AND numItemCode=@numItemCode

		-- INSERT NEW ITEM ATTRIBUTES
		INSERT INTO ItemAttributes
		(
			numDomainID
			,numItemCode
			,FLD_ID
			,FLD_Value
		)
		SELECT
			@numDomainID
			,@numItemCode
			,Fld_ID
			,Fld_Value
		FROM
			@TempTable

		IF (SELECT COUNT(*) FROM WarehouseItems WHERE numDomainID=@numDomainID AND numItemID=@numItemCode) > 0
		BEGIN

			-- DELETE PREVIOUS WAREHOUSE ATTRIBUTES
			DELETE FROM CFW_Fld_Values_Serialized_Items WHERE RecId IN (SELECT ISNULL(numWarehouseItemID,0) FROM WarehouseItems WHERE numDomainID=@numDomainID AND numItemID=@numItemCode)

			-- INSERT NEW WAREHOUSE ATTRIBUTES
			INSERT INTO CFW_Fld_Values_Serialized_Items
			(
				Fld_ID,
				Fld_Value,
				RecId,
				bitSerialized
			)                                                  
			SELECT 
				Fld_ID,
				ISNULL(CAST(Fld_Value AS VARCHAR),'') AS Fld_Value,
				TEMPWarehouse.numWarehouseItemID,
				0 
			FROM 
				@TempTable  
			OUTER APPLY
			(
				SELECT
					numWarehouseItemID
				FROM 
					WarehouseItems 
				WHERE 
					numDomainID=@numDomainID 
					AND numItemID=@numItemCode
			) AS TEMPWarehouse
		END
	END	  
COMMIT

	IF @numItemCode > 0
	BEGIN
		DELETE 
			IC
		FROM 
			dbo.ItemCategory IC
		INNER JOIN 
			Category 
		ON 
			IC.numCategoryID = Category.numCategoryID
		WHERE 
			numItemID = @numItemCode		
			AND numCategoryProfileID = @numCategoryProfileID

		IF @vcCategories <> ''	
		BEGIN
			INSERT INTO ItemCategory( numItemID , numCategoryID )  SELECT  @numItemCode AS numItemID,ID from dbo.SplitIDs(@vcCategories,',')	
		END
	END
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH        
END
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageShippingRule')
DROP PROCEDURE USP_ManageShippingRule
GO
CREATE PROCEDURE USP_ManageShippingRule
	@numRuleID NUMERIC(9),
	@vcRuleName VARCHAR(50),
	@vcDescription VARCHAR(1000),
	@tintBasedOn TINYINT,
	@tinShippingMethod TINYINT,
	@tintType TINYINT,
	@numDomainID NUMERIC(9),
	@tintTaxMode TINYINT,
	@tintFixedShippingQuotesMode TINYINT,
	@bitFreeshipping BIT,
	@FreeShippingAmt INT = NULL,
	@numRelationship NUMERIC(9),
	@numProfile NUMERIC(9),
	@bitItemClassification BIT,
	@numSiteId NUMERIC(18,0),
	@strWarehouseIds VARCHAR(1000)
AS 
BEGIN
	IF(SELECT COUNT(*) FROM dbo.ShippingRules WHERE vcRuleName = @vcRuleName AND numDomainId=@numDomainId AND numRuleID<>@numRuleID)>0
	BEGIN
 		RAISERROR ( 'DUPLICATE',16, 1 )
 		RETURN ;
	END

    IF ISNULL(@numRuleID,0) = 0
    BEGIN  
        INSERT INTO dbo.ShippingRules 
		(	
			vcRuleName
            ,vcDescription
            ,tintBasedOn
            ,tinShippingMethod
            ,tintIncludeType
            ,numDomainID
            ,tintTaxMode
            ,tintFixedShippingQuotesMode 
			,bitFreeShipping
			,FreeShippingOrderAmt
			,numRelationship
			,numProfile
			,numWareHouseID
			,bitItemClassification
			,numSiteId
		)
        VALUES 
		(
            @vcRuleName
            ,@vcDescription
            ,@tintBasedOn
            ,@tinShippingMethod
            ,@tintType
            ,@numDomainID 
            ,@tintTaxMode 
            ,@tintFixedShippingQuotesMode
			,@bitFreeshipping
			,CASE WHEN @bitFreeshipping = 0 THEN NULL ELSE @FreeShippingAmt END
			,@numRelationship
			,@numProfile
			,@strWarehouseIds
			,@bitItemClassification
			,@numSiteId
		)
		   
        SELECT numRuleID = SCOPE_IDENTITY()
    END  
    ELSE 
    BEGIN  
		IF (SELECT 
				COUNT(*) 
			FROM 
				dbo.ShippingRules SR
			WHERE 
				numDomainId=@numDomainId 
				AND numRuleID <> ISNULL(@numRuleID,0) 
				AND ISNULL(numRelationship,0) = ISNULL(@numRelationship,0)
				AND ISNULL(numProfile,0) = ISNULL(@numProfile,0)
				AND EXISTS (SELECT 
								SRSL1.numRuleID
							FROM 
								ShippingRuleStateList SRSL1 
							INNER JOIN
								ShippingRuleStateList SRSL2
							ON
								ISNULL(SRSL1.numStateID,0) = ISNULL(SRSL2.numStateID,0)
								AND ISNULL(SRSL1.vcZipPostal,'') = ISNULL(SRSL2.vcZipPostal,'')
								AND ISNULL(SRSL1.numCountryID,0) = ISNULL(SRSL2.numCountryID,0)
							WHERE 
								SRSL1.numDomainID=@numDomainID 
								AND SRSL1.numRuleID = SR.numRuleID
								AND SRSL2.numRuleID = @numRuleID)
			) > 0
		BEGIN
 			RAISERROR ( 'DUPLICATE RELATIONSHIP, PROFILE AND STATE-ZIP',16, 1 )
 			RETURN ;
		END

		UPDATE 
			dbo.ShippingRules
		SET 
			vcRuleName = @vcRuleName
			,vcDescription = @vcDescription
			,tintBasedOn = @tintBasedOn
			,tinShippingMethod = @tinShippingMethod
			,tintIncludeType = @tintType 
			,tintTaxMode = @tintTaxMode
            ,tintFixedShippingQuotesMode = @tintFixedShippingQuotesMode
			,bitFreeShipping = @bitFreeshipping
			,FreeShippingOrderAmt = (CASE WHEN @bitFreeshipping = 0 THEN NULL Else @FreeShippingAmt End)
			,numRelationship = @numRelationship
			,numProfile = @numProfile
			,numWareHouseID = @strWarehouseIds
			,bitItemClassification=@bitItemClassification
			,numSiteId=@numSiteId
		WHERE 
			numRuleID = @numRuleID
			AND numDomainID = @numDomainID			          

        SELECT  @numRuleID
    END
END
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ManageShippingServiceType' ) 
    DROP PROCEDURE USP_ManageShippingServiceType
GO 
CREATE PROCEDURE USP_ManageShippingServiceType
    @byteMode TINYINT = 0,
    @numServiceTypeID NUMERIC,
    @numDomainID NUMERIC,
    @numRuleID NUMERIC,
    @vcServiceName VARCHAR(100),
    @intNsoftEnum INT=0,
    @intFrom INT,
    @intTo INT,
    @fltMarkup FLOAT,
    @bitMarkupType BIT,
    @monRate DECIMAL(20,5),
    @bitEnabled BIT,
	@vcItemClassification VARCHAR(500),
    @strItems AS text
AS 
BEGIN
	
    IF @byteMode = 0 
        BEGIN
            IF @numServiceTypeID = 0 
                BEGIN
                    INSERT  INTO dbo.ShippingServiceTypes ( vcServiceName,
                                                            intNsoftEnum,
                                                            intFrom,
                                                            intTo,
                                                            fltMarkup,
                                                            bitMarkupType,
                                                            monRate,
                                                            numRuleID,
                                                            numDomainID,
															vcItemClassification
															)
                    VALUES  (
                              @vcServiceName,
                              @intNsoftEnum,
                              @intFrom,
                              @intTo,
                              @fltMarkup,
                              @bitMarkupType,
                              @monRate,
                              @numRuleID,
                              @numDomainID,
							  @vcItemClassification
                            ) 
                END
            ELSE 
                BEGIN
                    UPDATE  dbo.ShippingServiceTypes
                    SET     vcServiceName = @vcServiceName,
                            intNsoftEnum = @intNsoftEnum,
                            intFrom = @intFrom,
                            intTo = @intTo,
                            fltMarkup = @fltMarkup,
                            bitMarkupType = @bitMarkupType,
                            monRate = @monRate,
                            bitEnabled=@bitEnabled,
							vcItemClassification=@vcItemClassification
                    WHERE   numServiceTypeID = @numServiceTypeID
                            AND numDomainID = @numDomainID
			
                END
		
        END
	IF @byteMode = 1
	BEGIN
		DELETE FROM dbo.ShippingServiceTypes WHERE numServiceTypeID=@numServiceTypeID AND numDomainID=@numDomainID
	END
	
	IF @byteMode = 2 
	BEGIN
	    IF @numServiceTypeID = 2
		BEGIN
		    UPDATE dbo.ShippingServiceTypes SET bitEnabled = 0 WHERE intNsoftEnum = 0 AND numRuleID = @numRuleID
		END
	     
		DECLARE  @hDocItem INT

		IF CONVERT(VARCHAR(10),@strItems) <> ''
		BEGIN
			EXEC sp_xml_preparedocument @hDocItem OUTPUT,@strItems
			
			UPDATE 
				dbo.ShippingServiceTypes
			SET 
				vcServiceName=X.vcServiceName,
				intFrom =X.intFrom,
				intTo=X.intTo,
				fltMarkup=X.fltMarkup,
				bitMarkupType=X.bitMarkupType,
				monRate=X.monRate,
				bitEnabled=X.bitEnabled
			FROM 
			(
				SELECT 
					*
				FROM 
					OPENXML (@hDocItem, '/NewDataSet/Item', 2)
				WITH 
				(
					numServiceTypeID NUMERIC(9),
					vcServiceName VARCHAR(100),
					intFrom INT,
					intTo INT,
					fltMarkup FLOAT,
					bitMarkupType BIT,
					monRate DECIMAL(20,5),
					bitEnabled bit
				)
			) X					
			WHERE 
				dbo.ShippingServiceTypes.numServiceTypeID= X.numServiceTypeID
				AND dbo.ShippingServiceTypes.numDomainID=@numDomainID
				
				EXEC sp_xml_removedocument @hDocItem
		END
	END
END
GO
/****** Object:  StoredProcedure [dbo].[USP_ManageSubscribers]    Script Date: 07/26/2008 16:20:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                                    
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'usp_managesubscribers' ) 
    DROP PROCEDURE usp_managesubscribers
GO
CREATE PROCEDURE [dbo].[USP_ManageSubscribers]
    @numSubscriberID AS NUMERIC(9) OUTPUT,
    @numDivisionID AS NUMERIC(9),
    @intNoofUsersSubscribed AS INTEGER,
    @dtSubStartDate AS DATETIME,
    @dtSubEndDate AS DATETIME,
    @bitTrial AS BIT,
    @numAdminContactID AS NUMERIC(9),
    @bitActive AS TINYINT,
    @vcSuspendedReason AS VARCHAR(1000),
    @numTargetDomainID AS NUMERIC(9) OUTPUT,
    @numDomainID AS NUMERIC(9),
    @numUserContactID AS NUMERIC(9),
    @strUsers AS TEXT = '',
    @bitExists AS BIT = 0 OUTPUT,
    @TargetGroupId AS NUMERIC(9) = 0 OUTPUT,
	@bitEnabledAccountingAudit BIT,
	@bitEnabledNotifyAdmin BIT,
	@intNoofLimitedAccessUsers INT = NULL,
	@intNoofBusinessPortalUsers INT = NULL,
	@monFullUserCost DECIMAL(20,5) = NULL,
	@monLimitedAccessUserCost DECIMAL(20,5) = NULL,
	@monBusinessPortalUserCost DECIMAL(20,5) = NULL,
	@monSiteCost DECIMAL(20,5) = NULL,
	@intFullUserConcurrency INT  = 0
AS 
    IF @numSubscriberID = 0 
    BEGIN
		IF NOT EXISTS (SELECT * FROM Subscribers WHERE numDivisionID = @numDivisionID AND numDomainID = @numDomainID) 
        BEGIN
            SET @bitExists = 0                              
            
			DECLARE @vcCompanyName AS VARCHAR(100)                             
            DECLARE @numCompanyID AS NUMERIC(9)                                   
                    
			SELECT 
				@vcCompanyName = vcCompanyName
				,@numCompanyID = C.numCompanyID
            FROM 
				CompanyInfo C
            INNER JOIN 
				DivisionMaster D 
			ON 
				D.numCompanyID = C.numCompanyID
            WHERE 
				D.numDivisionID = @numDivisionID                                    
                                
			DECLARE @numNewCompanyID AS NUMERIC(9)                                   
			DECLARE @numNewDivisionID AS NUMERIC(9)                                    
			DECLARE @numNewContactID AS NUMERIC(9)                                    
                                      
            INSERT INTO Domain
            (
                vcDomainName,
                vcDomainDesc,
                numDivisionID,
                numAdminID,
				tintDecimalPoints,
				tintChrForComSearch,
				tintChrForItemSearch,
				bitIsShowBalance,
				tintComAppliesTo,
				tintCommissionType,
				charUnitSystem,
				bitExpenseNonInventoryItem
            )
			VALUES
			(
                @vcCompanyName,
                @vcCompanyName,
                @numDivisionID,
                @numAdminContactID,
                2,
				1,
				1,
				0,
				3,
				1,
				'E',
				1
            )
			                            
            SET @numTargetDomainID = SCOPE_IDENTITY()                                       
                                       
            INSERT  INTO CompanyInfo
            (
                vcCompanyName,
                numCompanyType,
                numCompanyRating,
                numCompanyIndustry,
                numCompanyCredit,
                vcWebSite,
                vcWebLabel1,
                vcWebLink1,
                vcWebLabel2,
                vcWebLink2,
                vcWebLabel3,
                vcWebLink3,
                vcWeblabel4,
                vcWebLink4,
                numAnnualRevID,
                numNoOfEmployeesId,
                vcHow,
                vcProfile,
                bitPublicFlag,
                numDomainID
            )
			SELECT 
				vcCompanyName,
                93,--i.e employer --numCompanyType,
                numCompanyRating,
                numCompanyIndustry,
                numCompanyCredit,
                vcWebSite,
                vcWebLabel1,
                vcWebLink1,
                vcWebLabel2,
                vcWebLink2,
                vcWebLabel3,
                vcWebLink3,
                vcWeblabel4,
                vcWebLink4,
                numAnnualRevID,
                numNoOfEmployeesId,
                vcHow,
                vcProfile,
                bitPublicFlag,
                numDomainID
			FROM 
				CompanyInfo
			WHERE 
				numCompanyID = @numCompanyID
				                 
            SET @numNewCompanyID = SCOPE_IDENTITY()                            
                                     
            INSERT INTO DivisionMaster
            (
                numCompanyID,
                vcDivisionName,
                numGrpId,
                bitPublicFlag,
                tintCRMType,
                numStatusID,
                tintBillingTerms,
                numBillingDays,
                tintInterestType,
                fltInterest,
                vcComPhone,
                vcComFax,
                numDomainID
            )
            SELECT 
				@numNewCompanyID,
				vcDivisionName,
				numGrpId,
				bitPublicFlag,
				2,
				numStatusID,
				tintBillingTerms,
				numBillingDays,
				tintInterestType,
				fltInterest,
				vcComPhone,
				vcComFax,
				numDomainID
			FROM 
				DivisionMaster
            WHERE 
				numDivisionID = @numDivisionID                     
                    
			SET @numNewDivisionID = SCOPE_IDENTITY()     
                                            
            INSERT INTO dbo.AddressDetails (
				vcAddressName,
				vcStreet,
				vcCity,
				vcPostalCode,
				numState,
				numCountry,
				bitIsPrimary,
				tintAddressOf,
				tintAddressType,
				numRecordID,
				numDomainID
			) 
			SELECT 
				vcAddressName,
				vcStreet,
				vcCity,
				vcPostalCode,
				numState,
				numCountry,
				bitIsPrimary,
				tintAddressOf,
				tintAddressType,
				@numNewDivisionID,
				numDomainID 
			FROM 
				dbo.AddressDetails 
			WHERE 
				numRecordID= @numDivisionID 
				AND tintAddressOf=2
                                
                                    
                    INSERT  INTO AdditionalContactsInformation
                            (
                              vcDepartment,
                              vcCategory,
                              vcGivenName,
                              vcFirstName,
                              vcLastName,
                              numDivisionId,
                              numContactType,
                              numTeam,
                              numPhone,
                              numPhoneExtension,
                              numCell,
                              numHomePhone,
                              vcFax,
                              vcEmail,
                              VcAsstFirstName,
                              vcAsstLastName,
                              numAsstPhone,
                              numAsstExtn,
                              vcAsstEmail,
                              charSex,
                              bintDOB,
                              vcPosition,
                              numEmpStatus,
                              vcTitle,
                              vcAltEmail,
                              numDomainID,
                              bitPrimaryContact
                            )
                            SELECT  vcDepartment,
                                    vcCategory,
                                    vcGivenName,
                                    vcFirstName,
                                    vcLastName,
                                    @numNewDivisionID,
                                    numContactType,
                                    numTeam,
                                    numPhone,
                                    numPhoneExtension,
                                    numCell,
                                    numHomePhone,
                                    vcFax,
                                    vcEmail,
                                    VcAsstFirstName,
                                    vcAsstLastName,
                                    numAsstPhone,
                                    numAsstExtn,
                                    vcAsstEmail,
                                    charSex,
                                    bintDOB,
                                    vcPosition,
                                    numEmpStatus,
                                    vcTitle,
                                    vcAltEmail,
                                    numDomainID,
                                    1
                            FROM    AdditionalContactsInformation
                            WHERE   numContactID = @numAdminContactID                                       
                    SET @numNewContactID = SCOPE_IDENTITY()                             
                             
                    UPDATE  CompanyInfo
                    SET     numCreatedBy = @numNewContactID,
                            bintCreatedDate = GETUTCDATE(),
                            numModifiedBy = @numNewContactID,
                            bintModifiedDate = GETUTCDATE(),
                            numDomainID = @numTargetDomainID
                    WHERE   numCompanyID = @numNewCompanyID                            
                             
                    UPDATE  DivisionMaster
                    SET     numCompanyID = @numNewCompanyID,
                            numCreatedBy = @numNewContactID,
                            bintCreatedDate = GETUTCDATE(),
                            numModifiedBy = @numNewContactID,
                            bintModifiedDate = GETUTCDATE(),
                            numDomainID = @numTargetDomainID,
                            tintCRMType = 2,
                            numRecOwner = @numNewContactID
                    WHERE   numDivisionID = @numNewDivisionID                            
                            
                            
                    UPDATE  AdditionalContactsInformation
                    SET     numDivisionId = @numNewDivisionID,
                            numCreatedBy = @numNewContactID,
                            bintCreatedDate = GETUTCDATE(),
                            numModifiedBy = @numNewContactID,
                            bintModifiedDate = GETUTCDATE(),
                            numDomainID = @numTargetDomainID,
                            numRecOwner = @numNewContactID
                    WHERE   numContactId = @numNewContactID                            
                                
                    DECLARE @vcFirstname AS VARCHAR(50)                            
                    DECLARE @vcEmail AS VARCHAR(100)                            
                    SELECT  @vcFirstname = vcFirstName,
                            @vcEmail = vcEmail
                    FROM    AdditionalContactsInformation
                    WHERE   numContactID = @numNewContactID                            
                    EXEC Resource_Add @vcFirstname, '', @vcEmail, 1,
                        @numTargetDomainID, @numNewContactID   
                        
     -- Added by sandeep to add required fields for New Item form field management in Admnistration section
     
     EXEC USP_ManageNewItemRequiredFields @numDomainID = @numTargetDomainID
     
     --Add Default Subtabs and assign permission to all roles by default --added by chintan

                    EXEC USP_ManageTabsInCuSFields @byteMode = 6, @LocID = 0,
                        @TabName = '', @TabID = 0,
                        @numDomainID = @numTargetDomainID, @vcURL = ''
                        
                    INSERT  INTO RoutingLeads
                            (
                              numDomainId,
                              vcRoutName,
                              tintEqualTo,
                              numValue,
                              numCreatedBy,
                              dtCreatedDate,
                              numModifiedBy,
                              dtModifiedDate,
                              bitDefault,
                              tintPriority
                            )
                    VALUES  (
                              @numTargetDomainID,
                              'Default',
                              0,
                              @numNewContactID,
                              @numNewContactID,
                              GETUTCDATE(),
                              @numNewContactID,
                              GETUTCDATE(),
                              1,
                              0
                            )                          
                    INSERT  INTO RoutingLeadDetails
                            (
                              numRoutID,
                              numEmpId,
                              intAssignOrder
                            )
                    VALUES  (
                              SCOPE_IDENTITY(),
                              @numNewContactID,
                              1
                            )                  
                      
                    IF NOT EXISTS (SELECT numGroupID FROM AuthenticationGroupMaster WHERE numDomainId = @numTargetDomainID AND vcGroupName = 'System Administrator') 
                    BEGIN
                        DECLARE @numGroupId1 AS NUMERIC(9)                      
                        INSERT INTO AuthenticationGroupMaster
                        (
                            vcGroupName,
                            numDomainID,
                            tintGroupType,
                            bitConsFlag 
                        )
                        VALUES  
						(
                            'System Administrator',
                            @numTargetDomainID,
                            1,
                            1
                        )                      
                        
						SET @numGroupId1 = SCOPE_IDENTITY()                        
                        SET @TargetGroupId = @numGroupId1                     
                       
						INSERT INTO GroupAuthorization
						(
							numGroupID
							,numModuleID
							,numPageID
							,intExportAllowed
							,intPrintAllowed
							,intViewAllowed
							,intAddAllowed
							,intUpdateAllowed
							,intDeleteAllowed
							,numDomainID
						)
						SELECT 
							@numGroupId1
							,numModuleID
							,numPageID
							,bitISExportApplicable
							,0
							,bitIsViewApplicable
							,bitIsAddApplicable
							,bitISUpdateApplicable
							,bitISDeleteApplicable
							,@numTargetDomainID
						FROM 
							PageMaster

						INSERT INTO GroupTabDetails (numGroupId,numTabId,numRelationShip,bitallowed,numOrder,tintType) VALUES (@numGroupId1,1,0,1,1,0)
						INSERT INTO GroupTabDetails (numGroupId,numTabId,numRelationShip,bitallowed,numOrder,tintType) VALUES (@numGroupId1,36,0,1,2,0)
						INSERT INTO GroupTabDetails (numGroupId,numTabId,numRelationShip,bitallowed,numOrder,tintType) VALUES (@numGroupId1,7,0,1,3,0)
						INSERT INTO GroupTabDetails (numGroupId,numTabId,numRelationShip,bitallowed,numOrder,tintType) VALUES (@numGroupId1,80,0,1,4,0)
						INSERT INTO GroupTabDetails (numGroupId,numTabId,numRelationShip,bitallowed,numOrder,tintType) VALUES (@numGroupId1,3,0,1,5,0)
						INSERT INTO GroupTabDetails (numGroupId,numTabId,numRelationShip,bitallowed,numOrder,tintType) VALUES (@numGroupId1,45,0,1,6,0)
						INSERT INTO GroupTabDetails (numGroupId,numTabId,numRelationShip,bitallowed,numOrder,tintType) VALUES (@numGroupId1,4,0,1,7,0)
						INSERT INTO GroupTabDetails (numGroupId,numTabId,numRelationShip,bitallowed,numOrder,tintType) VALUES (@numGroupId1,44,0,1,8,0)
						INSERT INTO GroupTabDetails (numGroupId,numTabId,numRelationShip,bitallowed,numOrder,tintType) VALUES (@numGroupId1,6,0,1,9,0)

						--default Sub tab From cfw_grp_master
						INSERT  INTO GroupTabDetails
                        (
                            numGroupId,
                            numTabId,
                            numRelationShip,
                            bitallowed,
                            numOrder,tintType
                        )
                        SELECT  
							@numGroupId1,
                            x.Grp_id,
                            0,
                            1,
                            x.numOrder
							,1
                        FROM
						(
							SELECT
								 *
								 ,ROW_NUMBER() OVER(ORDER BY Grp_id) AS numOrder
                            FROM 
								cfw_grp_master
                            WHERE 
								tintType=2 
								AND numDomainID = @numTargetDomainID
                        ) x 


						--Added by Anoop for Dashboard +++++++++++++++++++
                        IF NOT EXISTS (SELECT numCustomReportID FROM CustomReport WHERE numDomainId = @numTargetDomainID)
                        BEGIN                  
                            EXEC CreateCustomReportsForNewDomain @numTargetDomainID,@numNewContactID              
                        END 

                        DECLARE @numGroupId2 AS NUMERIC(9)                      
                        INSERT  INTO AuthenticationGroupMaster
						(
							vcGroupName,
							numDomainID,
							tintGroupType,
							bitConsFlag 
						)
						VALUES  
						(
							'Executive',
							@numTargetDomainID,
							1,
							0
						)
						SET @numGroupId2 = SCOPE_IDENTITY() 

						EXEC USP_ManageSubscribers_GorupPermission @numTargetDomainID,@numGroupId2,258

                        INSERT INTO AuthenticationGroupMaster
                        (
                            vcGroupName,
                            numDomainID,
                            tintGroupType,
                            bitConsFlag 
                        )
                        VALUES
						(
                            'Sales Staff',
                            @numTargetDomainID,
                            1,
                            0
                        )                      
                        SET @numGroupId2 = SCOPE_IDENTITY() 

						EXEC USP_ManageSubscribers_GorupPermission @numTargetDomainID,@numGroupId2,259

                        INSERT  INTO AuthenticationGroupMaster
                        (
                            vcGroupName,
                            numDomainID,
                            tintGroupType,
                            bitConsFlag 
                        )
                        VALUES  
						(
                            'Warehouse Staff',
                            @numTargetDomainID,
                            1,
                            0
                        )                      
						SET @numGroupId2 = SCOPE_IDENTITY() 

						EXEC USP_ManageSubscribers_GorupPermission @numTargetDomainID,@numGroupId2,260
						
						INSERT INTO AuthenticationGroupMaster (vcGroupName,numDomainID,tintGroupType,bitConsFlag) VALUES ('Limited Access Users',@numTargetDomainID,4,1)
						SET @numGroupId2 = SCOPE_IDENTITY()

						INSERT INTO GroupTabDetails (numGroupId,numTabId,numRelationShip,bitallowed,numOrder,tintType) SELECT numGroupID,135,0,1,1,0 FROM AuthenticationGroupMaster WHERE tintGroupType = @numGroupId2
						INSERT INTO GroupTabDetails (numGroupId,numTabId,numRelationShip,bitallowed,numOrder,tintType) SELECT numGroupID,5,0,1,1,0 FROM AuthenticationGroupMaster WHERE tintGroupType = @numGroupId2
						INSERT INTO GroupTabDetails (numGroupId,numTabId,numRelationShip,bitallowed,numOrder,tintType) SELECT numGroupID,36,0,1,1,0 FROM AuthenticationGroupMaster WHERE tintGroupType = @numGroupId2
						INSERT INTO GroupTabDetails (numGroupId,numTabId,numRelationShip,bitallowed,numOrder,tintType) SELECT numGroupID,132,0,1,1,0 FROM AuthenticationGroupMaster WHERE tintGroupType = @numGroupId2

						INSERT INTO TreeNavigationAuthorization
						(
							numDomainID,numGroupID,numTabID,numPageNavID,bitVisible,tintType
						)
						VALUES
						(@numTargetDomainID,@numGroupId2,5,279,1,1)
						,(@numTargetDomainID,@numGroupId2,5,280,1,1)
						,(@numTargetDomainID,@numGroupId2,135,274,1,1)
						,(@numTargetDomainID,@numGroupId2,135,275,1,1)
						,(@numTargetDomainID,@numGroupId2,132,84,1,1)
						,(@numTargetDomainID,@numGroupId2,132,271,1,1)
						,(@numTargetDomainID,@numGroupId2,36,250,1,1)

						INSERT INTO PageMasterGroupSetting 
						(
							numPageID,numModuleID,numGroupID
							,bitIsViewApplicable,bitViewPermission1Applicable,bitViewPermission2Applicable,bitViewPermission3Applicable
							,bitIsAddApplicable,bitAddPermission1Applicable,bitAddPermission2Applicable,bitAddPermission3Applicable
							,bitIsUpdateApplicable,bitUpdatePermission1Applicable,bitUpdatePermission2Applicable,bitUpdatePermission3Applicable
							,bitIsDeleteApplicable,bitDeletePermission1Applicable,bitDeletePermission2Applicable,bitDeletePermission3Applicable
							,bitIsExportApplicable,bitExportPermission1Applicable,bitExportPermission2Applicable,bitExportPermission3Applicable
						)
						SELECT 152,16,numGroupID,1,1,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 FROM AuthenticationGroupMaster WHERE numGroupID=@numGroupId2
						UNION
						SELECT 153,16,numGroupID,1,1,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 FROM AuthenticationGroupMaster WHERE numGroupID=@numGroupId2
						UNION
						SELECT 154,16,numGroupID,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 FROM AuthenticationGroupMaster WHERE numGroupID=@numGroupId2
						UNION
						SELECT 155,16,numGroupID,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 FROM AuthenticationGroupMaster WHERE numGroupID=@numGroupId2
						UNION
						SELECT 156,16,numGroupID,1,1,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 FROM AuthenticationGroupMaster WHERE numGroupID=@numGroupId2
						UNION
						SELECT 157,16,numGroupID,0,0,0,0,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0 FROM AuthenticationGroupMaster WHERE numGroupID=@numGroupId2
						UNION
						SELECT 158,16,numGroupID,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 FROM AuthenticationGroupMaster WHERE numGroupID=@numGroupId2
						UNION
						SELECT 159,16,numGroupID,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 FROM AuthenticationGroupMaster WHERE numGroupID=@numGroupId2
						UNION
						SELECT 160,16,numGroupID,1,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 FROM AuthenticationGroupMaster WHERE numGroupID=@numGroupId2 
						UNION
						SELECT 161,16,numGroupID,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 FROM AuthenticationGroupMaster WHERE numGroupID=@numGroupId2
						UNION
						SELECT 162,16,numGroupID,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 FROM AuthenticationGroupMaster WHERE numGroupID=@numGroupId2
						UNION
						SELECT 163,16,numGroupID,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 FROM AuthenticationGroupMaster WHERE numGroupID=@numGroupId2
						UNION
						SELECT 164,16,numGroupID,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 FROM AuthenticationGroupMaster WHERE numGroupID=@numGroupId2
						UNION
						SELECT 165,16,numGroupID,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 FROM AuthenticationGroupMaster WHERE numGroupID=@numGroupId2

						INSERT INTO GroupAuthorization
						(
							numDomainID,numGroupID,numPageID,numModuleID,intViewAllowed,intAddAllowed,intUpdateAllowed,intDeleteAllowed,intExportAllowed,intPrintAllowed
						)
						SELECT numDomainID,numGroupID,152,16,1,0,0,0,0,0 FROM AuthenticationGroupMaster WHERE numGroupID=@numGroupId2 
						UNION
						SELECT numDomainID,numGroupID,153,16,1,0,0,0,0,0 FROM AuthenticationGroupMaster WHERE numGroupID=@numGroupId2
						UNION
						SELECT numDomainID,numGroupID,154,16,0,0,0,0,0,0 FROM AuthenticationGroupMaster WHERE numGroupID=@numGroupId2
						UNION
						SELECT numDomainID,numGroupID,155,16,0,0,0,0,0,0 FROM AuthenticationGroupMaster WHERE numGroupID=@numGroupId2
						UNION
						SELECT numDomainID,numGroupID,156,16,1,0,0,0,0,0 FROM AuthenticationGroupMaster WHERE numGroupID=@numGroupId2
						UNION
						SELECT numDomainID,numGroupID,157,16,1,0,1,0,0,0 FROM AuthenticationGroupMaster WHERE numGroupID=@numGroupId2
						UNION
						SELECT numDomainID,numGroupID,158,16,0,0,0,0,0,0 FROM AuthenticationGroupMaster WHERE numGroupID=@numGroupId2
						UNION
						SELECT numDomainID,numGroupID,159,16,0,0,0,0,0,0 FROM AuthenticationGroupMaster WHERE numGroupID=@numGroupId2
						UNION
						SELECT numDomainID,numGroupID,160,16,3,0,0,0,0,0 FROM AuthenticationGroupMaster WHERE numGroupID=@numGroupId2
						UNION
						SELECT numDomainID,numGroupID,161,16,0,0,0,0,0,0 FROM AuthenticationGroupMaster WHERE numGroupID=@numGroupId2
						UNION
						SELECT numDomainID,numGroupID,162,16,0,0,0,0,0,0 FROM AuthenticationGroupMaster WHERE numGroupID=@numGroupId2
						UNION
						SELECT numDomainID,numGroupID,163,16,0,0,0,0,0,0 FROM AuthenticationGroupMaster WHERE numGroupID=@numGroupId2
						UNION
						SELECT numDomainID,numGroupID,164,16,0,0,0,0,0,0 FROM AuthenticationGroupMaster WHERE numGroupID=@numGroupId2
						UNION
						SELECT numDomainID,numGroupID,165,16,0,0,0,0,0,0 FROM AuthenticationGroupMaster WHERE numGroupID=@numGroupId2

						INSERT INTO PageMasterGroupSetting 
						(
							numPageID,numModuleID,numGroupID
							,bitIsViewApplicable,bitViewPermission1Applicable,bitViewPermission2Applicable,bitViewPermission3Applicable
							,bitIsAddApplicable,bitAddPermission1Applicable,bitAddPermission2Applicable,bitAddPermission3Applicable
							,bitIsUpdateApplicable,bitUpdatePermission1Applicable,bitUpdatePermission2Applicable,bitUpdatePermission3Applicable
							,bitIsDeleteApplicable,bitDeletePermission1Applicable,bitDeletePermission2Applicable,bitDeletePermission3Applicable
							,bitIsExportApplicable,bitExportPermission1Applicable,bitExportPermission2Applicable,bitExportPermission3Applicable
						)
						SELECT 117,40,numGroupID,1,1,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0 FROM AuthenticationGroupMaster WHERE numGroupID=@numGroupId2

						INSERT INTO GroupAuthorization
						(
							numDomainID,numGroupID,numPageID,numModuleID,intViewAllowed,intAddAllowed,intUpdateAllowed,intDeleteAllowed,intExportAllowed,intPrintAllowed
						)
						SELECT numDomainID,numGroupID,117,40,1,1,0,0,0,0 FROM AuthenticationGroupMaster WHERE numGroupID=@numGroupId2

						INSERT INTO PageMasterGroupSetting 
						(
							numPageID,numModuleID,numGroupID
							,bitIsViewApplicable,bitViewPermission1Applicable,bitViewPermission2Applicable,bitViewPermission3Applicable
							,bitIsAddApplicable,bitAddPermission1Applicable,bitAddPermission2Applicable,bitAddPermission3Applicable
							,bitIsUpdateApplicable,bitUpdatePermission1Applicable,bitUpdatePermission2Applicable,bitUpdatePermission3Applicable
							,bitIsDeleteApplicable,bitDeletePermission1Applicable,bitDeletePermission2Applicable,bitDeletePermission3Applicable
							,bitIsExportApplicable,bitExportPermission1Applicable,bitExportPermission2Applicable,bitExportPermission3Applicable
						)
						SELECT
							PageMaster.numPageID,PageMaster.numModuleID,@numGroupId2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
						FROM
							PageMaster
						WHERE 
							numModuleID=35
							AND numPageID <> 87
						UNION
						SELECT
							PageMaster.numPageID,PageMaster.numModuleID,@numGroupId2,1,1,0,0,0,0,0,0,1,1,0,0,0,0,0,0,1,1,0,0
						FROM
							PageMaster
						WHERE 
							numModuleID=35
							AND numPageID = 87

						INSERT INTO GroupAuthorization
						(
							numDomainID,numGroupID,numPageID,numModuleID,intViewAllowed,intAddAllowed,intUpdateAllowed,intDeleteAllowed,intExportAllowed,intPrintAllowed
						)
						SELECT
							@numTargetDomainID,@numGroupId2,PageMaster.numPageID,PageMaster.numModuleID,0,0,0,0,0,0
						FROM
							PageMaster
						WHERE 
							numModuleID=35
							AND numPageID <> 87
						UNION
						SELECT
							@numTargetDomainID,@numGroupId2,PageMaster.numPageID,PageMaster.numModuleID,1,0,1,0,1,0
						FROM
							PageMaster
						WHERE 
							numModuleID=35
							AND numPageID = 87

						INSERT INTO PageMasterGroupSetting 
						(
							numPageID,numModuleID,numGroupID
							,bitIsViewApplicable,bitViewPermission1Applicable,bitViewPermission2Applicable,bitViewPermission3Applicable
							,bitIsAddApplicable,bitAddPermission1Applicable,bitAddPermission2Applicable,bitAddPermission3Applicable
							,bitIsUpdateApplicable,bitUpdatePermission1Applicable,bitUpdatePermission2Applicable,bitUpdatePermission3Applicable
							,bitIsDeleteApplicable,bitDeletePermission1Applicable,bitDeletePermission2Applicable,bitDeletePermission3Applicable
							,bitIsExportApplicable,bitExportPermission1Applicable,bitExportPermission2Applicable,bitExportPermission3Applicable
						)
						VALUES
						(1,1,@numGroupId2,1,1,0,0,0,0,0,0,1,1,0,0,1,1,0,0,0,0,0,0)
						,(5,1,@numGroupId2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0)
						,(6,1,@numGroupId2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0)
						,(10,1,@numGroupId2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0)

						INSERT INTO GroupAuthorization
						(
							numDomainID,numGroupID,numPageID,numModuleID,intViewAllowed,intAddAllowed,intUpdateAllowed,intDeleteAllowed,intExportAllowed,intPrintAllowed
						)
						VALUES
						(@numTargetDomainID,@numGroupId2,1,1,1,0,1,1,0,0)
						,(@numTargetDomainID,@numGroupId2,5,1,0,0,0,0,0,0)
						,(@numTargetDomainID,@numGroupId2,6,1,0,0,0,0,0,0)
						,(@numTargetDomainID,@numGroupId2,10,1,0,0,0,0,0,0)
                    END                      
                    
					IF NOT EXISTS (SELECT numGroupID FROM AuthenticationGroupMaster WHERE numDomainId = @numTargetDomainID AND vcGroupName = 'Business Portal Users') 
                    BEGIN
						INSERT INTO AuthenticationGroupMaster
                        (
                            vcGroupName,
                            numDomainID,
                            tintGroupType,
                            bitConsFlag 
                        )
                        VALUES
						(
                            'Business Portal Users',
                            @numTargetDomainID,
                            2,
                            1
                        )                      
                        SET @numGroupId2 = SCOPE_IDENTITY()
						INSERT INTO PageMasterGroupSetting 
						(
							numPageID,numModuleID,numGroupID
							,bitIsViewApplicable,bitViewPermission1Applicable,bitViewPermission2Applicable,bitViewPermission3Applicable
							,bitIsAddApplicable,bitAddPermission1Applicable,bitAddPermission2Applicable,bitAddPermission3Applicable
							,bitIsUpdateApplicable,bitUpdatePermission1Applicable,bitUpdatePermission2Applicable,bitUpdatePermission3Applicable
							,bitIsDeleteApplicable,bitDeletePermission1Applicable,bitDeletePermission2Applicable,bitDeletePermission3Applicable
							,bitIsExportApplicable,bitExportPermission1Applicable,bitExportPermission2Applicable,bitExportPermission3Applicable
						)
						SELECT
							numPageID,numModuleID,@numGroupId2
							,1,1,0,0
							,0,0,0,0
							,0,0,0,0
							,0,0,0,0
							,0,0,0,0
						FROM 
							PageMaster
						WHERE
							numModuleID IN (32,2,3,4,11,37,10,46,12,7,16)
						      
						INSERT INTO GroupAuthorization
						(
							numPageID,numModuleID,numGroupID,intExportAllowed,intPrintAllowed,intViewAllowed,intAddAllowed,intUpdateAllowed,intDeleteAllowed,numDomainID
						)
						SELECT
							numPageID,numModuleID,@numGroupId2,0,0,1,0,0,0,@numTargetDomainID
						FROM 
							PageMaster
						WHERE
							numModuleID IN (32,2,3,4,11,37,10,46,12,7,16)
							AND NOT EXISTS (SELECT numPageID FROM GroupAuthorization GA WHERE GA.numPageID=PageMaster.numPageID AND GA.numModuleID=PageMaster.numModuleID AND GA.numGroupID=@numGroupId2)

						INSERT INTO GroupAuthorization
						(
							numPageID,numModuleID,numGroupID,intExportAllowed,intPrintAllowed,intViewAllowed,intAddAllowed,intUpdateAllowed,intDeleteAllowed,numDomainID
						)
						SELECT
							numListItemID,32,@numGroupId2,0,0,1,0,0,0,@numTargetDomainID
						FROM 
						(	
							SELECT 
								numListItemID 
							FROM 
								ListDetails 
							WHERE 
								numListID=5 
								AND ((constFlag=1 AND numListItemID <> 46) OR constFlag=0)
						) PageMaster
						WHERE
							NOT EXISTS (SELECT numPageID FROM GroupAuthorization GA WHERE GA.numPageID=PageMaster.numListItemID AND GA.numModuleID=32 AND GA.numGroupID=@numGroupId2)
   
        
						UPDATE PageMasterGroupSetting SET bitIsViewApplicable=0,bitViewPermission1Applicable=0 WHERE numModuleID=4 AND numPageID IN (12,13,16,17)
						UPDATE GroupAuthorization SET intViewAllowed=0 WHERE numModuleID=4 AND numPageID IN (12,13,16,17) AND numGroupID = @numGroupId2
						UPDATE PageMasterGroupSetting SET bitIsViewApplicable=0,bitViewPermission1Applicable=0 WHERE numModuleID=11 AND numPageID IN (1,134)
						UPDATE GroupAuthorization SET intViewAllowed=0 WHERE numModuleID=11 AND numPageID IN (1,134) AND numGroupID = @numGroupId2
						UPDATE PageMasterGroupSetting SET bitIsViewApplicable=0,bitViewPermission1Applicable=0 WHERE numModuleID=37 AND numPageID IN (34,130,132,149)
						UPDATE GroupAuthorization SET intViewAllowed=0 WHERE numModuleID=37 AND numPageID IN (34,130,132,149) AND numGroupID = @numGroupId2
						UPDATE PageMasterGroupSetting SET bitIsViewApplicable=0,bitViewPermission1Applicable=0 WHERE numModuleID=2 AND numPageID IN (7,9,10,11,12)
						UPDATE GroupAuthorization SET intViewAllowed=0 WHERE numModuleID=2 AND numPageID IN (7,9,10,11,12) AND numGroupID = @numGroupId2
						UPDATE PageMasterGroupSetting SET bitIsViewApplicable=0,bitViewPermission1Applicable=0 WHERE numModuleID=16 AND numPageID IN (154,155,158,159,161,162,163,164,165)
						UPDATE GroupAuthorization SET intViewAllowed=0 WHERE numModuleID=16 AND numPageID IN (154,155,158,159,161,162,163,164,165) AND numGroupID = @numGroupId2
						UPDATE PageMasterGroupSetting SET bitIsViewApplicable=0,bitViewPermission1Applicable=0 WHERE numModuleID=10 AND numPageID IN (15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35)
						UPDATE GroupAuthorization SET intViewAllowed=0 WHERE numModuleID=10 AND numPageID IN (15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35) AND numGroupID = @numGroupId2
						UPDATE PageMasterGroupSetting SET bitIsViewApplicable=0,bitViewPermission1Applicable=0 WHERE numModuleID=12 AND numPageID IN (5,6,7,8,13,14,16,17)
						UPDATE GroupAuthorization SET intViewAllowed=0 WHERE numModuleID=12 AND numPageID IN (5,6,7,8,13,14,16,17) AND numGroupID = @numGroupId2
						UPDATE PageMasterGroupSetting SET bitIsViewApplicable=0,bitViewPermission1Applicable=0 WHERE numModuleID=3 AND numPageID IN (10,11,13,14,15)
						UPDATE GroupAuthorization SET intViewAllowed=0 WHERE numModuleID=3 AND numPageID IN (10,11,13,14,15) AND numGroupID = @numGroupId2
						UPDATE PageMasterGroupSetting SET bitIsViewApplicable=0,bitViewPermission1Applicable=0 WHERE numModuleID=32 AND numPageID IN (2)
						UPDATE GroupAuthorization SET intViewAllowed=0 WHERE numModuleID=32 AND numPageID IN (2) AND numGroupID = @numGroupId2
						UPDATE PageMasterGroupSetting SET bitIsViewApplicable=0,bitViewPermission1Applicable=0 WHERE numModuleID=7 AND numPageID IN (1,12,13)
						UPDATE GroupAuthorization SET intViewAllowed=0 WHERE numModuleID=7 AND numPageID IN (1,12,13) AND numGroupID = @numGroupId2
						UPDATE PageMasterGroupSetting SET bitIsAddApplicable=1,bitAddPermission1Applicable=1 WHERE numModuleID=7 AND numPageID = 1
						UPDATE GroupAuthorization SET intAddAllowed=1 WHERE numModuleID=7 AND numPageID = 1 AND numGroupID = @numGroupId2
						UPDATE PageMasterGroupSetting SET bitIsAddApplicable=1,bitAddPermission1Applicable=1,bitIsUpdateApplicable=1,bitUpdatePermission1Applicable=1,bitIsDeleteApplicable=1,bitDeletePermission1Applicable=1 WHERE numModuleID=10 AND numPageID = 5
						UPDATE GroupAuthorization SET intAddAllowed=1,intUpdateAllowed=1,intDeleteAllowed=1 WHERE numModuleID=10 AND numPageID = 5 AND numGroupID = @numGroupId2
						UPDATE PageMasterGroupSetting SET bitIsAddApplicable=1,bitAddPermission1Applicable=1 WHERE numModuleID=10 AND numPageID = 28
						UPDATE GroupAuthorization SET intAddAllowed=1 WHERE numModuleID=10 AND numPageID = 28 AND numGroupID = @numGroupId2
						UPDATE PageMasterGroupSetting SET bitIsAddApplicable=1,bitAddPermission1Applicable=1 WHERE numModuleID=10 AND numPageID = 1
						UPDATE GroupAuthorization SET intAddAllowed=1 WHERE numModuleID=10 AND numPageID = 1 AND numGroupID = @numGroupId2

						DELETE FROM GroupTabDetails WHERE numTabId IN (1,4,5,7,80,112,132,133,134,135) AND numGroupId IN (SELECT numGroupId FROM AuthenticationGroupMaster WHERE tintGroupType=2)

						DECLARE @TEMPTabs TABLE
						(
							numTabID INT
							,numOrder INT
						)
						INSERT INTO @TEMPTabs (numTabID,numOrder) VALUES (7,1),(80,2),(1,3),(134,4),(133,5),(135,6),(5,7),(4,8),(112,9),(132,10)

						INSERT INTO GroupTabDetails
						(
							numGroupId,numTabId,numRelationShip,bitallowed,numOrder
						)
						SELECT
							@numGroupId2,numTabID,0,1,numOrder
						FROM
							@TEMPTabs


						DECLARE @TEMP TABLE
						(
							numPageNavID INT
						)

						DELETE FROM @TEMP
						DELETE FROM TreeNavigationAuthorization WHERE numTabID=1 AND numGroupID = @numGroupId2

						INSERT INTO @TEMP (numPageNavID) VALUES (122),(123),(125),(149)

						INSERT INTO TreeNavigationAuthorization
						(
							numDomainID,numGroupID,numTabID,numPageNavID,bitVisible,tintType
						)
						SELECT
							@numTargetDomainID,@numGroupId2,1,numPageNavID,1,1
						FROM
							@TEMP

						-----------------------------------------

						DELETE FROM @TEMP
						DELETE FROM TreeNavigationAuthorization WHERE numTabID=4 AND numGroupID = @numGroupId2

						INSERT INTO @TEMP (numPageNavID) VALUES (246),(247),(248)

						INSERT INTO TreeNavigationAuthorization
						(
							numDomainID,numGroupID,numTabID,numPageNavID,bitVisible,tintType
						)
						SELECT
							@numTargetDomainID,@numGroupId2,4,numPageNavID,1,1
						FROM
							@TEMP

						------------------------------------------

						DELETE FROM @TEMP
						DELETE FROM TreeNavigationAuthorization WHERE numTabID=5 AND numGroupID = @numGroupId2

						INSERT INTO @TEMP (numPageNavID) VALUES (279),(280)

						INSERT INTO TreeNavigationAuthorization
						(
							numDomainID,numGroupID,numTabID,numPageNavID,bitVisible,tintType
						)
						SELECT
							@numTargetDomainID,@numGroupId2,5,numPageNavID,1,1
						FROM
							@TEMP

						-----------------------------------------

						DELETE FROM @TEMP
						DELETE FROM TreeNavigationAuthorization WHERE numTabID=7 AND numGroupID = @numGroupId2

						
						INSERT INTO @TEMP (numPageNavID) VALUES (6),(7),(11),(12)

						INSERT INTO TreeNavigationAuthorization
						(
							numDomainID,numGroupID,numTabID,numPageNavID,bitVisible,tintType
						)
						SELECT
							@numTargetDomainID,@numGroupId2,7,numPageNavID,1,1
						FROM
							@TEMP

						------------------------------------------

						DELETE FROM @TEMP
						DELETE FROM TreeNavigationAuthorization WHERE numTabID=80 AND numGroupID = @numGroupId2

						INSERT INTO @TEMP (numPageNavID) VALUES (62),(1),(2),(3),(4),(71),(72),(76),(171),(240)

						INSERT INTO TreeNavigationAuthorization
						(
							numDomainID,numGroupID,numTabID,numPageNavID,bitVisible,tintType
						)
						SELECT
							@numTargetDomainID,@numGroupId2,80,numPageNavID,1,1
						FROM
							@TEMP

						-----------------------------------------

						DELETE FROM @TEMP
						DELETE FROM TreeNavigationAuthorization WHERE numTabID=132 AND numGroupID = @numGroupId2

						INSERT INTO @TEMP (numPageNavID) VALUES (270),(84)

						INSERT INTO TreeNavigationAuthorization
						(
							numDomainID,numGroupID,numTabID,numPageNavID,bitVisible,tintType
						)
						SELECT
							@numTargetDomainID,@numGroupId2,132,numPageNavID,1,1
						FROM
							@TEMP

						------------------------------------------

						DELETE FROM @TEMP
						DELETE FROM TreeNavigationAuthorization WHERE numTabID=133 AND numGroupID = @numGroupId2

						INSERT INTO @TEMP (numPageNavID) VALUES (273),(124),(126),(207)

						INSERT INTO TreeNavigationAuthorization
						(
							numDomainID,numGroupID,numTabID,numPageNavID,bitVisible,tintType
						)
						SELECT
							@numTargetDomainID,@numGroupId2,133,numPageNavID,1,1
						FROM
							@TEMP

						-----------------------------------------

						DELETE FROM @TEMP
						DELETE FROM TreeNavigationAuthorization WHERE numTabID=134 AND numGroupID = @numGroupId2

						INSERT INTO @TEMP (numPageNavID) VALUES (13)

						INSERT INTO TreeNavigationAuthorization
						(
							numDomainID,numGroupID,numTabID,numPageNavID,bitVisible,tintType
						)
						SELECT
							@numTargetDomainID,@numGroupId2,134,numPageNavID,1,1
						FROM
							@TEMP

						-----------------------------------------

						DELETE FROM @TEMP
						DELETE FROM TreeNavigationAuthorization WHERE numTabID=135 AND numGroupID = @numGroupId2

						INSERT INTO @TEMP (numPageNavID) VALUES (274),(275)

						INSERT INTO TreeNavigationAuthorization
						(
							numDomainID,numGroupID,numTabID,numPageNavID,bitVisible,tintType
						)
						SELECT
							@numTargetDomainID,@numGroupId2,135,numPageNavID,1,1
						FROM
							@TEMP
                    END                      
----                   
                    EXEC USP_ChartAcntDefaultValues @numDomainId = @numTargetDomainID,
                        @numUserCntId = @numNewContactID      
    
    
-- Dashboard Size    
                    INSERT  INTO DashBoardSize
                            SELECT  1,
                                    1,
                                    @numGroupId1,
                                    1
                            UNION
                            SELECT  2,
                                    1,
                                    @numGroupId1,
                                    1
                            UNION
                            SELECT  3,
                                    2,
                                    @numGroupId1,
                                    1     
    
----inserting all the Custome Reports for the newly created group    
    
                    INSERT  INTO DashboardAllowedReports
                            SELECT  numCustomReportID,
                                    @numGroupId1
                            FROM    CustomReport
                            WHERE   numDomainID = @numTargetDomainID    
    
    
           
 --inserting Default BizDocs for new domain
INSERT INTO dbo.AuthoritativeBizDocs
        ( numAuthoritativePurchase ,
          numAuthoritativeSales ,
          numDomainId
        )
SELECT  
ISNULL((SELECT numListItemID  FROM dbo.ListDetails WHERE numListID=27 AND constFlag =1 AND vcData = 'bill'),0),
ISNULL((SELECT numListItemID  FROM dbo.ListDetails WHERE numListID=27 AND constFlag =1 AND vcData = 'invoice'),0),
@numTargetDomainID


IF NOT EXISTS(SELECT * FROM PortalBizDocs WHERE numDomainID=@numTargetDomainID)
BEGIN
	--delete from PortalBizDocs WHERE numDomainID=176
	INSERT INTO dbo.PortalBizDocs
        ( numBizDocID, numDomainID )
	SELECT numListItemID,@numTargetDomainID FROM dbo.ListDetails WHERE numListID=27 AND constFlag=1
END



---Set BizDoc Type Filter for Sales and Purchase BizDoc Type
INSERT INTO dbo.BizDocFilter( numBizDoc ,tintBizocType ,numDomainID)
SELECT numListItemID,1,@numTargetDomainID FROM dbo.ListDetails WHERE numListID=27 AND constFlag=1 AND vcData IN ('Invoice','Sales Opportunity','Sales Order','Sales Credit Memo','Fulfillment Order','Credit Memo','Refund Receipt','RMA','Packing Slip','Pick List')

INSERT INTO dbo.BizDocFilter( numBizDoc ,tintBizocType ,numDomainID)
SELECT numListItemID,2,@numTargetDomainID FROM dbo.ListDetails WHERE numListID=27 AND constFlag=1 AND vcData IN ('Purchase Opportunity','Bill','Purchase Order','Purchase Credit Memo','RMA')



--- Give permission of all tree node to all Groups
exec USP_CreateTreeNavigationForDomain @numTargetDomainID,0

--Create Default BizDoc template for all system bizdocs, Css and template html will be updated though same SP from code.
EXEC dbo.USP_CreateBizDocTemplateByDefault @numDomainID = @numTargetDomainID, -- numeric
    @txtBizDocTemplate = '', -- text
    @txtCss = '', -- text
    @tintMode = 2,
    @txtPackingSlipBizDocTemplate = ''
 

          
 --inserting the details of BizForms Wizard          
                    INSERT  INTO DycFormConfigurationDetails(numFormId,numFieldId,numViewId,intColumnNum,intRowNum,boolAOIField,numAuthGroupID
								,numDomainId,numSurId)
                            SELECT  numFormId,numFieldId,numViewId,intColumnNum,intRowNum,boolAOIField,@numGroupId1,
									@numTargetDomainID,0
                            FROM    DycFormConfigurationDetails
                            WHERE   numDomainID = 0
                                    AND numFormID IN ( 1, 2, 6 )          
          
                    INSERT  INTO DycFormConfigurationDetails(numFormId,numFieldId,numViewId,intColumnNum,intRowNum,boolAOIField,numAuthGroupID
								,numDomainId,numSurId)
                            SELECT  numFormId,numFieldId,numViewId,intColumnNum,intRowNum,boolAOIField,0,
									@numTargetDomainID,0
                            FROM    DycFormConfigurationDetails
                            WHERE   numDomainID = 0
                                    AND numFormID IN ( 3, 4, 5 )          
          
                    INSERT  INTO DycFormConfigurationDetails(numFormId,numFieldId,numViewId,intColumnNum,intRowNum,boolAOIField,numAuthGroupID
								,numDomainId,numSurId)
                            SELECT  numFormId,numFieldId,numViewId,intColumnNum,intRowNum,boolAOIField,numAuthGroupID,
									@numTargetDomainID,0
                            FROM    DycFormConfigurationDetails
                            WHERE   numDomainID = 0
                                    AND numFormID IN ( 7, 8 )          
          
          
          
          ---Insert Default Add Relationship Field Lead/Prospect/Account and Other
          INSERT INTO DycFormConfigurationDetails(numFormId,numFieldId,intColumnNum,intRowNum,
								numDomainId,numUserCntID,numRelCntType,tintPageType,bitCustom)
               SELECT  numFormId,numFieldId,intColumnNum,intRowNum,@numTargetDomainID,0,
								numRelCntType,tintPageType,bitCustom
                     FROM DycFormConfigurationDetails
                     WHERE numDomainID = 0 AND numFormID IN (34,35,36) AND tintPageType=2
          
          ---Set Default Validation for Lead/Prospect/Account
         INSERT INTO DynamicFormField_Validation(numFormId,numFormFieldId,numDomainId,vcNewFormFieldName,
   bitIsRequired,bitIsNumeric,bitIsAlphaNumeric,bitIsEmail,bitIsLengthValidation,intMaxLength,intMinLength,
   bitFieldMessage,vcFieldMessage)
    SELECT numFormId,numFormFieldId,@numTargetDomainID,vcNewFormFieldName,
   bitIsRequired,bitIsNumeric,bitIsAlphaNumeric,bitIsEmail,bitIsLengthValidation,intMaxLength,intMinLength,
   bitFieldMessage,vcFieldMessage
                     FROM DynamicFormField_Validation
                     WHERE numDomainID = 0 AND numFormID IN (34,35,36)
                              
                    
                    DECLARE @numListItemID AS NUMERIC(9)                    
                    DECLARE @numNewListItemID AS NUMERIC(9)                    
                    SELECT TOP 1
                            @numListItemID = numListItemID
                    FROM    ListDetails
                    WHERE   numDomainID = 1
                            AND numListID = 40                    
                    
                    WHILE @numListItemID > 0                    
                        BEGIN                    
                    
                            INSERT  INTO ListDetails
                                    (
                                      numListID,
                                      vcData,
                                      numCreatedBY,
                                      bintCreatedDate,
                                      numModifiedBy,
                                      bintModifiedDate,
                                      bitDelete,
                                      numDomainID,
                                      constFlag,
                                      sintOrder
                                    )
                                    SELECT  numListID,
                                            vcData,
                                            @numNewContactID,
                                            GETUTCDATE(),
                                            @numNewContactID,
                                            GETUTCDATE(),
                                            bitDelete,
                                            @numTargetDomainID,
                                            constFlag,
                                            sintOrder
                                    FROM    ListDetails
                                    WHERE   numListItemID = @numListItemID                    
                            SELECT  @numNewListItemID = SCOPE_IDENTITY()                    
                     
                            INSERT  INTO [state]
                                    (
                                      numCountryID,
                                      vcState,
                                      numCreatedBy,
                                      bintCreatedDate,
                                      numModifiedBy,
                                      bintModifiedDate,
                                      numDomainID,
                                      constFlag,
									  numShippingZone,
									  vcAbbreviations
                                    )
                                    SELECT  @numNewListItemID,
                                            vcState,
                                            @numNewContactID,
                                            GETUTCDATE(),
                                            @numNewContactID,
                                            GETUTCDATE(),
                                            @numTargetDomainID,
                                            constFlag,
											numShippingZone,
											vcAbbreviations
                                    FROM    [State]
                                    WHERE   numCountryID = @numListItemID                    
                            SELECT TOP 1
                                    @numListItemID = numListItemID
                            FROM    ListDetails
                            WHERE   numDomainID = 1
                                    AND numListID = 40
                                    AND numListItemID > @numListItemID                    
                            IF @@rowcount = 0 
                                SET @numListItemID = 0                    
                        END                    
                    
                    --INsert Net Terms
                   	INSERT INTO [ListDetails] ([numListID],[vcData],[numCreatedBY],[bitDelete],[numDomainID],[constFlag],[sintOrder])
								  SELECT 296,'0',1,0,@numTargetDomainID,0,1
						UNION ALL SELECT 296,'7',1,0,@numTargetDomainID,0,2
						UNION ALL SELECT 296,'15',1,0,@numTargetDomainID,0,3
						UNION ALL SELECT 296,'30',1,0,@numTargetDomainID,0,4
						UNION ALL SELECT 296,'45',1,0,@numTargetDomainID,0,5
						UNION ALL SELECT 296,'60',1,0,@numTargetDomainID,0,6
                    --Insert Default email templates
                    INSERT INTO dbo.GenericDocuments (
						VcFileName,
						vcDocName,
						numDocCategory,
						cUrlType,
						vcFileType,
						numDocStatus,
						vcDocDesc,
						numDomainID,
						numCreatedBy,
						bintCreatedDate,
						numModifiedBy,
						bintModifiedDate,
						vcSubject,
						vcDocumentSection,
						numRecID,
						tintCheckOutStatus,
						intDocumentVersion,
						numLastCheckedOutBy,
						dtCheckOutDate,
						dtCheckInDate,
						tintDocumentType,
						numOldSpecificDocID,
						numModuleID
					)  
					SELECT  VcFileName,
							vcDocName,
							numDocCategory,
							cUrlType,
							vcFileType,
							numDocStatus,
							vcDocDesc,
							@numTargetDomainID,
							@numNewContactID,
							GETUTCDATE(),
							@numNewContactID,
							GETUTCDATE(),
							vcSubject,
							vcDocumentSection,
							numRecID,
							tintCheckOutStatus,
							intDocumentVersion,
							numLastCheckedOutBy,
							dtCheckOutDate,
							dtCheckInDate,
							tintDocumentType,
							numOldSpecificDocID,
							numModuleID
					FROM    dbo.GenericDocuments
					WHERE   numDomainID = 0
                       
                    INSERT  INTO UserMaster
                            (
                              vcUserName,
                              vcUserDesc,
                              vcMailNickName,
                              numGroupID,
                              numDomainID,
                              numCreatedBy,
                              bintCreatedDate,
                              numModifiedBy,
                              bintModifiedDate,
                              bitActivateFlag,
                              numUserDetailId,
                              vcEmailID
                            )
                            SELECT  vcFirstName,
                                    vcFirstName,
                                    vcFirstName,
                                    @numGroupId1,
                                    @numTargetDomainID,
                                    @numUserContactID,
                                    GETUTCDATE(),
                                    @numUserContactID,
                                    GETUTCDATE(),
                                    0,
                                    @numNewContactID,
                                    vcEmail
                            FROM    AdditionalContactsInformation
                            WHERE   numContactID = @numAdminContactID                                            
                          
                           INSERT INTO UOM (numDomainId,vcUnitName,tintUnitType,bitEnabled)
			                SELECT @numTargetDomainID, vcUnitName,tintUnitType,bitEnabled FROM UOM WHERE numDomainId=0    
                    
                    if not exists (select * from Currency where numDomainID=@numTargetDomainID)
						insert into Currency (vcCurrencyDesc,chrCurrency,varCurrSymbol,numDomainID,fltExchangeRate,bitEnabled)
							select vcCurrencyDesc, chrCurrency,varCurrSymbol, @numTargetDomainID, fltExchangeRate, bitEnabled from Currency where numDomainID=0
                    
                    ------- Insert Detail For Default Currency In New Domain Entry -------------------------
					DECLARE @numCountryID AS NUMERIC(18,0)
					SELECT @numCountryID = numListItemID FROM dbo.ListDetails WHERE numListID = 40 AND vcData = 'United States' AND numDomainID = @numTargetDomainID

					--IF NOT EXISTS(SELECT * FROM Currency WHERE numCountryId = @numCountryID AND numDomainID = @numTargetDomainID AND vcCurrencyDesc = 'USD-U.S. Dollar')
					IF NOT EXISTS(SELECT * FROM Currency WHERE numDomainID = @numTargetDomainID AND vcCurrencyDesc = 'USD-U.S. Dollar')
					BEGIN
					PRINT 1
						INSERT  INTO dbo.Currency(vcCurrencyDesc,chrCurrency,varCurrSymbol,numDomainID,fltExchangeRate,bitEnabled,numCountryId)
						SELECT  TOP 1 vcCurrencyDesc,chrCurrency,varCurrSymbol,@numTargetDomainID,ISNULL(fltExchangeRate,1),1 AS bitEnabled,@numCountryId FROM dbo.Currency 
						WHERE vcCurrencyDesc = 'USD-U.S. Dollar' AND numDomainID=0
						UNION 
						SELECT  vcCurrencyDesc,chrCurrency,varCurrSymbol,@numTargetDomainID,fltExchangeRate,0 AS bitEnabled,NULL FROM dbo.Currency 
						WHERE vcCurrencyDesc <> 'USD-U.S. Dollar' AND numDomainID=0
					END
					ELSE
						BEGIN
						PRINT 2
							UPDATE Currency SET numCountryId = @numCountryID, bitEnabled = 1, fltExchangeRate=1
							WHERE numDomainID = @numTargetDomainID
							AND vcCurrencyDesc = 'USD-U.S. Dollar'                        
						PRINT 3							
						END
						
					--Set Default Currency
                    DECLARE @numCurrencyID AS NUMERIC(18)
                    SET @numCurrencyID=(SELECT TOP 1 numCurrencyID FROM Currency WHERE (bitEnabled=1 or chrCurrency='USD') AND numDomainID=@numTargetDomainID)
                   
					--Set Default Country
					DECLARE @numDefCountry AS NUMERIC(18)
                    SET @numDefCountry=(SELECT TOP 1 numListItemID FROM ListDetails where numlistid=40 AND numDomainID=@numTargetDomainID and vcdata like '%United States%')		
					                    
                    UPDATE  Domain
                    SET     numDivisionID = @numNewDivisionID,
                            numAdminID = @numNewContactID,numCurrencyID=@numCurrencyID,numDefCountry=isnull(@numDefCountry,0)
                    WHERE   numdomainID = @numTargetDomainID                                      
                         	
					------------------


                         
                    INSERT  INTO Subscribers
                            (
                              numDivisionID,
                              numAdminContactID,
                              numTargetDomainID,
                              numDomainID,
                              numCreatedBy,
                              dtCreatedDate,
                              numModifiedBy,
                              dtModifiedDate,
                              dtEmailStartDate,
                              dtEmailEndDate,
							  bitEnabledAccountingAudit,
							  bitEnabledNotifyAdmin
                            )
                    VALUES  (
                              @numDivisionID,
                              @numNewContactID,
                              @numTargetDomainID,
                              @numDomainID,
                              @numUserContactID,
                              GETUTCDATE(),
                              @numUserContactID,
                              GETUTCDATE(),
                              dbo.GetUTCDateWithoutTime(),
                              dbo.GetUTCDateWithoutTime(),
							  @bitEnabledAccountingAudit,
							  @bitEnabledNotifyAdmin
                            )                                    
                    SET @numSubscriberID = SCOPE_IDENTITY()                        
                      
                      
                              
                END                                
        ELSE 
        BEGIN                        
            SELECT  @numSubscriberID = numSubscriberID
            FROM    Subscribers
            WHERE   numDivisionID = @numDivisionID
                    AND numDomainID = @numDomainID                                
            UPDATE  Subscribers
            SET     bitDeleted = 0
            WHERE   numSubscriberID = @numSubscriberID                                
        END       
    END                                    
    ELSE 
    BEGIN
        DECLARE @bitOldStatus AS BIT                                  
        SET @bitExists = 1                  
            
		SELECT 
			@bitOldStatus = bitActive,
            @numTargetDomainID = numTargetDomainID
        FROM 
			Subscribers
        WHERE 
			numSubscriberID = @numSubscriberID                                    
                                  
        IF (@bitOldStatus = 1 AND @bitActive = 0) 
            UPDATE Subscribers SET dtSuspendedDate = GETUTCDATE() WHERE numSubscriberID = @numSubscriberID                                  
                                  
        UPDATE 
			Subscribers
        SET 
			intNoofUsersSubscribed = @intNoofUsersSubscribed,
            dtSubStartDate = @dtSubStartDate,
            dtSubEndDate = @dtSubEndDate,
            bitTrial = @bitTrial,
            numAdminContactID = @numAdminContactID,
            bitActive = @bitActive,
            vcSuspendedReason = @vcSuspendedReason,
            numModifiedBy = @numUserContactID,
            dtModifiedDate = GETUTCDATE(),
			bitEnabledAccountingAudit = @bitEnabledAccountingAudit,
			bitEnabledNotifyAdmin = @bitEnabledNotifyAdmin,
			intNoofLimitedAccessUsers = @intNoofLimitedAccessUsers,
			intNoofBusinessPortalUsers = @intNoofBusinessPortalUsers,
			monFullUserCost = @monFullUserCost,
			monLimitedAccessUserCost = @monLimitedAccessUserCost,
			monBusinessPortalUserCost = @monBusinessPortalUserCost,
			monSiteCost = @monSiteCost,
			intFullUserConcurrency=@intFullUserConcurrency
        WHERE 
			numSubscriberID = @numSubscriberID                 
                       
        UPDATE 
			Domain
        SET
			numAdminID = @numAdminContactID
        WHERE 
			numdomainID = @numTargetDomainID   
                           
        DECLARE @hDoc1 INT                              
        EXEC sp_xml_preparedocument @hDoc1 OUTPUT, @strUsers                                                              
                                                               
        INSERT INTO UserMaster
        (
            vcUserName,
            vcMailNickName,
            vcUserDesc,
            numGroupID,
            numDomainID,
            numCreatedBy,
            bintCreatedDate,
            numModifiedBy,
            bintModifiedDate,
            bitActivateFlag,
            numUserDetailId,
            vcEmailID,
            vcPassword
        )
        SELECT  UserName,
                UserName,
                UserName,
                0,
                @numTargetDomainID,
                @numUserContactID,
                GETUTCDATE(),
                @numUserContactID,
                GETUTCDATE(),
                Active,
                numContactID,
                Email,
                vcPassword
        FROM    ( SELECT    *
                    FROM      OPENXML (@hDoc1, '/NewDataSet/Users[numUserID=0]',2)
                            WITH ( numContactID NUMERIC(9), numUserID NUMERIC(9), Email VARCHAR(100), vcPassword VARCHAR(100), UserName VARCHAR(100), Active TINYINT )
                ) X                                
                
                              
        UPDATE  UserMaster
        SET     vcUserName = X.UserName,
                numModifiedBy = @numUserContactID,
                bintModifiedDate = GETUTCDATE(),
                vcEmailID = X.Email,
                vcPassword = X.vcPassword,
                bitActivateFlag = Active
        FROM    ( SELECT    numUserID AS UserID,
                            numContactID,
                            Email,
                            vcPassword,
                            UserName,
                            Active
                    FROM      OPENXML (@hDoc1, '/NewDataSet/Users[numUserID>0]',2)
                            WITH ( numContactID NUMERIC(9), numUserID NUMERIC(9), Email VARCHAR(100), vcPassword VARCHAR(100), UserName VARCHAR(100), Active TINYINT )
                ) X
        WHERE   numUserID = X.UserID                                                              
                                                               
                                                               
        EXEC sp_xml_removedocument @hDoc1                               
                                   
    END
	                           
    SELECT  @numSubscriberID
GO
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageSubscribers_GorupPermission')
DROP PROCEDURE dbo.USP_ManageSubscribers_GorupPermission
GO
CREATE PROCEDURE [dbo].[USP_ManageSubscribers_GorupPermission]
(
	@numTargetDomainID NUMERIC(18,0)
	,@numGroupId2 NUMERIC(18,0)
	,@numGroupUserCntID NUMERIC(18,0)
)
AS 
BEGIN

	INSERT  INTO AuthenticationGroupMaster
    (
        vcGroupName,
        numDomainID,
        tintGroupType,
        bitConsFlag 
    )
    VALUES  
	(
        'Executive',
        @numTargetDomainID,
        1,
        0
    )
    SET @numGroupId2 = SCOPE_IDENTITY() 

	INSERT  INTO GroupAuthorization
	(
		numGroupID
		,numModuleID
		,numPageID
		,intExportAllowed
		,intPrintAllowed
		,intViewAllowed
		,intAddAllowed
		,intUpdateAllowed
		,intDeleteAllowed
		,numDomainID
	)
	SELECT 
		@numGroupId2
		,numModuleID
		,numPageID
		,bitISExportApplicable
		,0
		,bitIsViewApplicable
		,bitIsAddApplicable
		,bitISUpdateApplicable
		,bitISDeleteApplicable
		,@numTargetDomainID
	FROM 
		PageMaster         
        
                
	--MainTab From TabMaster 
    INSERT INTO GroupTabDetails
    (
        numGroupId,
        numTabId,
        numRelationShip,
        bitallowed,
        numOrder,tintType
    )
    SELECT 
		@numGroupId2,
        x.numTabId,
        0,
        1,
        x.numOrder
		,0
    FROM 
	(
		SELECT 
			*
			,ROW_NUMBER() OVER(ORDER BY numtabid) AS numOrder
        FROM 
			TabMaster
        WHERE 
			tintTabtype=1 
			AND bitFixed=1
    ) x 

	--default Sub tab From cfw_grp_master
	INSERT  INTO GroupTabDetails
    (
        numGroupId,
        numTabId,
        numRelationShip,
        bitallowed,
        numOrder,tintType
    )
    SELECT 
		@numGroupId2,
        x.Grp_id,
        0,
        1,
        x.numOrder
		,1
    FROM
	(
		SELECT 
			*
			,ROW_NUMBER() OVER(ORDER BY Grp_id) AS numOrder
        FROM 
			cfw_grp_master
        WHERE 
			tintType=2 
			AND numDomainID = @numTargetDomainID
    ) x 

    INSERT  INTO DashBoardSize
    (
        tintColumn,
        tintSize,
        numGroupUserCntID,
        bitGroup
    )
    SELECT  
		tintColumn,
        tintSize,
        @numGroupId2,
        1
    FROM 
		DashBoardSize
    WHERE 
		bitGroup = 1
        AND numGroupUserCntID = @numGroupUserCntID
    ORDER BY 
		tintColumn 

    INSERT INTO Dashboard
    (
        numReportID,
        numGroupUserCntID,
        tintRow,
        tintColumn,
        tintReportType,
        vcHeader,
        vcFooter,
        tintChartType,
        bitGroup
    )
    SELECT 
		numReportID,
        @numGroupId2,
        tintRow,
        tintColumn,
        tintReportType,
        vcHeader,
        vcFooter,
        tintChartType,
        1
    FROM 
		Dashboard
    WHERE 
		bitGroup = 1
        AND numGroupUserCntID = @numGroupUserCntID
    ORDER BY 
		tintColumn,
        tintRow

    INSERT INTO 
		DashboardAllowedReports
    SELECT 
		numCustomReportID,
        @numGroupId2
    FROM 
		CustomReport
    WHERE 
		numDomainID = @numTargetDomainID 

	UPDATE
		Table2
    SET
		Table2.numReportID = Table1.numOrgReportID
    FROM 
		Dashboard Table2
    INNER JOIN 
	(
		SELECT  
			numReportID
			,(SELECT TOP 1 C1.numCustomReportID FROM Customreport C1 JOIN Customreport C2 ON C1.vcReportName = C2.vcReportName WHERE C1.numDomainID = @numTargetDomainID AND C2.numCustomReportID = Dashboard.numReportID AND C2.numDomainID = 1) numOrgReportID
        FROM 
			Dashboard
        WHERE 
			bitGroup = 1
            AND numGroupUserCntID = @numGroupUserCntID
	) Table1 
	ON 
		Table2.numReportID = Table1.numReportID
    WHERE   
		bitGroup = 1
        AND numGroupUserCntID = @numGroupId2
END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_MassSalesFulfillment_GetRecordsForPicking')
DROP PROCEDURE dbo.USP_MassSalesFulfillment_GetRecordsForPicking
GO

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_MassSalesFulfillment_GetRecordsForPicking]
(
	@numDomainID NUMERIC(18,0)
	,@numUserCntID NUMERIC(18,0)
	,@bitGroupByOrder BIT
	,@numBatchID NUMERIC(18,0)
	,@vcSelectedRecords VARCHAR(MAX)
)
AS
BEGIN
	DECLARE @TempFieldsRight TABLE
	(
		ID VARCHAR(50)
		,numFieldID NUMERIC(18,0)
		,vcFieldName VARCHAR(100)
		,vcOrigDbColumnName VARCHAR(100)
		,bitAllowSorting BIT
		,bitAllowFiltering BIT
		,vcAssociatedControlType VARCHAR(20)
		,bitCustomField BIT
		,intRowNum INT
		,intColumnWidth FLOAT
	)

	INSERT INTO @TempFieldsRight
	(
		ID
		,numFieldID
		,vcFieldName
		,vcOrigDbColumnName
		,bitAllowSorting
		,bitAllowFiltering
		,vcAssociatedControlType
		,bitCustomField
		,intRowNum
		,intColumnWidth
	)
	SELECT
		CONCAT(142,'~',DFM.numFieldID,'~',0)
		,DFM.numFieldID
		,DFFM.vcFieldName
		,DFM.vcOrigDbColumnName
		,DFFM.bitAllowSorting
		,DFFM.bitAllowFiltering
		,DFM.vcAssociatedControlType
		,0
		,DFCD.intRowNum
		,ISNULL(DFCD.intColumnWidth,0)
	FROM
		DycFormConfigurationDetails DFCD
	INNER JOIN
		DycFormField_Mapping DFFM
	ON
		DFCD.numFormId = DFFM.numFormID
		AND DFCD.numFieldId = DFFM.numFieldID
	INNER JOIN
		DycFieldMaster DFM
	ON
		DFFM.numFieldID = DFM.numFieldId
	WHERE 
		DFCD.numDomainId=@numDomainID 
		AND DFCD.numUserCntID=@numUserCntID 
		AND DFCD.numFormId=142 
		AND ISNULL(DFFM.bitDeleted,0) = 0
		AND ISNULL(DFFM.bitSettingField,0) = 1
		AND ISNULL(DFCD.bitCustom,0) = 0

	IF (SELECT COUNT(*) FROM @TempFieldsRight) = 0
	BEGIN
		INSERT INTO @TempFieldsRight
		(
			ID
			,numFieldID
			,vcFieldName
			,vcOrigDbColumnName
			,bitAllowSorting
			,bitAllowFiltering
			,vcAssociatedControlType
			,bitCustomField
			,intRowNum
			,intColumnWidth
		)
		SELECT
			CONCAT(142,'~',DFM.numFieldID,'~',0)
			,DFM.numFieldID
			,DFFM.vcFieldName
			,DFM.vcOrigDbColumnName
			,DFFM.bitAllowSorting
			,DFFM.bitAllowFiltering
			,DFM.vcAssociatedControlType
			,0
			,DFFM.[order]
			,100
		FROM
			DycFormField_Mapping DFFM
		INNER JOIN
			DycFieldMaster DFM
		ON
			DFFM.numFieldID = DFM.numFieldId
		WHERE 
			DFFM.numFormId=142 
			AND ISNULL(DFFM.bitDeleted,0) = 0
			AND ISNULL(DFFM.bitSettingField,0) = 1
			AND (ISNULL(DFFM.bitDefault,0) = 1 OR ISNULL(DFFM.bitRequired,0) = 1)
	END
	ELSE
	BEGIN
		INSERT INTO @TempFieldsRight
		(
			ID
			,numFieldID
			,vcFieldName
			,vcOrigDbColumnName
			,bitAllowSorting
			,bitAllowFiltering
			,vcAssociatedControlType
			,bitCustomField
			,intRowNum
			,intColumnWidth
		)
		SELECT
			CONCAT(142,'~',DFM.numFieldID,'~',0)
			,DFM.numFieldID
			,DFFM.vcFieldName
			,DFM.vcOrigDbColumnName
			,DFFM.bitAllowSorting
			,DFFM.bitAllowFiltering
			,DFM.vcAssociatedControlType
			,0
			,DFFM.[order]
			,100
		FROM
			DycFormField_Mapping DFFM
		INNER JOIN
			DycFieldMaster DFM
		ON
			DFFM.numFieldID = DFM.numFieldId
		WHERE 
			DFFM.numFormId=142 
			AND ISNULL(DFFM.bitDeleted,0) = 0
			AND ISNULL(DFFM.bitSettingField,0) = 1
			AND ISNULL(DFFM.bitRequired,0) = 1
			AND DFM.numFieldId NOT IN (SELECT T1.numFieldID FROM @TempFieldsRight T1)
	END


	DECLARE @TEMP TABLE
	(
		ID INT IDENTITY(1,1)
		,numOppID NUMERIC(18,0)
		,numOppItemID NUMERIC(18,0)
	)

	IF ISNULL(@numBatchID,0) > 0
	BEGIN
		INSERT INTO @TEMP
		(
			numOppID
			,numOppItemID
		)
		SELECT
			numOppID
			,ISNULL(numOppItemID,0)
		FROM
			MassSalesFulfillmentBatchOrders
		WHERE
			numBatchID=@numBatchID
	END
	ELSE
	BEGIN
		INSERT INTO @TEMP
		(
			numOppID
			,numOppItemID
		)
		SELECT
			CAST(SUBSTRING(OutParam,0,CHARINDEX('-',OutParam)) AS NUMERIC(18,0)) numOppID
			,CAST(SUBSTRING(OutParam,CHARINDEX('-',OutParam) + 1,LEN(OutParam)) AS NUMERIC(18,0)) numOppItemID
		FROM
			dbo.SplitString(@vcSelectedRecords,',')
	END

	DECLARE @TEMPItems TABLE
	(	
		ID NUMERIC(18,0)
		,numOppID NUMERIC(18,0)
		,numOppItemID NUMERIC(18,0)
		,numOppChildItemID NUMERIC(18,0)
		,numOppKitChildItemID NUMERIC(18,0)
		,numItemCode NUMERIC(18,0)
		,numWarehouseID NUMERIC(18,0)
		,numWareHouseItemID NUMERIC(18,0)
		,vcWarehouse VARCHAR(300)
		,vcLocation VARCHAR(300)
		,vcKitChildItems VARCHAR(MAX)
		,vcKitChildKitItems VARCHAR(MAX)
		,vcPoppName VARCHAR(300)
		,numBarCodeId VARCHAR(300)
		,vcItemName VARCHAR(300)
		,vcItemDesc VARCHAR(MAX)
		,bitSerial BIT
		,bitLot BIT
		,bitKit BIT
		,vcSKU VARCHAR(300)
		,vcPathForTImage VARCHAR(MAX)
		,vcInclusionDetails VARCHAR(MAX)
		,numRemainingQty FLOAT
		,numQtyPicked FLOAT
		,numQtyRequiredKit FLOAT
	)

	INSERT INTO @TEMPItems
	(	
		ID
		,numOppID
		,numOppItemID 
		,numItemCode
		,numWarehouseID
		,numWareHouseItemID
		,vcWarehouse 
		,vcLocation
		,vcKitChildItems
		,vcKitChildKitItems
		,vcPoppName
		,numBarCodeId
		,vcItemName
		,vcItemDesc
		,bitSerial
		,bitLot
		,bitKit
		,vcSKU
		,vcPathForTImage
		,vcInclusionDetails
		,numRemainingQty
		,numQtyPicked
	)
	SELECT 
		T1.ID
		,OpportunityMaster.numOppID
		,OpportunityItems.numoppitemtCode
		,OpportunityItems.numItemCode
		,WareHouseItems.numWareHouseID
		,WareHouseItems.numWareHouseItemID
		,Warehouses.vcWareHouse
		,WarehouseLocation.vcLocation
		,ISNULL(STUFF((SELECT CONCAT(', ',OpportunityItems.numItemCode,'-',OKI.numChildItemID) FROM OpportunityKitItems OKI WHERE OKI.numOppItemID=1 ORDER BY OKI.numChildItemID FOR XML PATH('')),1,1,''),'') AS vcKitChildItems
		,ISNULL(STUFF((SELECT CONCAT(', ',OpportunityItems.numItemCode,'-',OKI.numChildItemID,'-',OKCI.numItemID) FROM OpportunityKitChildItems OKCI INNER JOIN OpportunityKitItems OKI ON OKCI.numOppChildItemID = OKI.numOppChildItemID  WHERE OKCI.numOppItemID=1 ORDER BY OKI.numChildItemID,OKCI.numItemID FOR XML PATH('')),1,1,''),'') vcKitChildKitItems
		,OpportunityMaster.vcPoppName
		,Item.numBarCodeId
		,Item.vcItemName
		,Item.txtItemDesc AS vcItemDesc
		,ISNULL(Item.bitSerialized,0)
		,ISNULL(Item.bitLotNo,0)
		,ISNULL(Item.bitKitParent,0)
		,Item.vcSKU
		,ISNULL(ItemImages.vcPathForTImage,'') vcPathForTImage
		,ISNULL(OpportunityItems.vcInclusionDetail,'') AS vcInclusionDetails
		,ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyPicked,0) numRemainingQty
		,0 numQtyPicked
	FROM
		@TEMP T1
	INNER JOIN
		OpportunityMaster
	ON
		OpportunityMaster.numOppID= T1.numOppID
	INNER JOIN
		OpportunityItems
	ON
		OpportunityMaster.numOppID = OpportunityItems.numOppID
		AND (OpportunityItems.numOppItemtcode = T1.numOppItemID OR ISNULL(T1.numOppItemID,0) = 0)
	INNER JOIN
		Item 
	ON
		OpportunityItems.numItemCode = Item.numItemCode
	LEFT JOIN
		ItemImages
	ON
		ItemImages.numDomainId=@numDomainID
		AND ItemImages.numItemCode = Item.numItemCode
		AND ItemImages.bitDefault=1 
		AND ItemImages.bitIsImage=1
	INNER JOIN
		WareHouseItems
	ON
		OpportunityItems.numWarehouseItmsID = WareHouseItems.numWareHouseItemID
	INNER JOIN
		Warehouses
	ON
		WareHouseItems.numWareHouseID = Warehouses.numWareHouseID
	LEFT JOIN
		WarehouseLocation
	ON
		WareHouseItems.numWLocationID = WarehouseLocation.numWLocationID
	WHERE
		OpportunityMaster.numDomainID=@numDomainID
		AND ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyPicked,0) > 0
	ORDER BY
		T1.ID, OpportunityItems.numSortOrder

	INSERT INTO @TEMPItems
	(	
		ID
		,numOppID
		,numOppItemID
		,numOppChildItemID
		,numItemCode
		,numWarehouseID
		,numWareHouseItemID
		,vcWarehouse 
		,vcLocation
		,vcKitChildItems
		,vcKitChildKitItems
		,vcPoppName
		,numBarCodeId
		,vcItemName
		,vcItemDesc
		,bitSerial
		,bitLot
		,bitKit
		,vcSKU
		,vcPathForTImage
		,vcInclusionDetails
		,numRemainingQty
		,numQtyPicked
		,numQtyRequiredKit
	)
	SELECT 
		T1.ID
		,OpportunityMaster.numOppID
		,OpportunityItems.numoppitemtCode
		,OpportunityKitItems.numOppChildItemID
		,Item.numItemCode
		,WareHouseItems.numWareHouseID
		,WareHouseItems.numWareHouseItemID
		,Warehouses.vcWareHouse
		,WarehouseLocation.vcLocation
		,'' AS vcKitChildItems
		,'' vcKitChildKitItems
		,OpportunityMaster.vcPoppName
		,Item.numBarCodeId
		,Item.vcItemName
		,Item.txtItemDesc AS vcItemDesc
		,ISNULL(Item.bitSerialized,0)
		,ISNULL(Item.bitLotNo,0)
		,ISNULL(Item.bitKitParent,0)
		,Item.vcSKU
		,ISNULL(ItemImages.vcPathForTImage,'') vcPathForTImage
		,ISNULL(OpportunityItems.vcInclusionDetail,'') AS vcInclusionDetails
		,(ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyPicked,0)) * ISNULL(OpportunityKitItems.numQtyItemsReq_Orig,0) numRemainingQty
		,0 numQtyPicked
		,ISNULL(OpportunityKitItems.numQtyItemsReq_Orig,0)
	FROM
		@TEMP T1
	INNER JOIN
		OpportunityMaster
	ON
		OpportunityMaster.numOppID= T1.numOppID
	INNER JOIN
		OpportunityItems
	ON
		OpportunityMaster.numOppID = OpportunityItems.numOppID
		AND (OpportunityItems.numOppItemtcode = T1.numOppItemID OR ISNULL(T1.numOppItemID,0) = 0)
	INNER JOIN
		OpportunityKitItems
	ON
		OpportunityMaster.numOppID = OpportunityItems.numOppID
		AND (OpportunityKitItems.numOppItemID = T1.numOppItemID OR OpportunityKitItems.numOppItemID = OpportunityItems.numoppitemtCode)
	INNER JOIN
		Item 
	ON
		OpportunityKitItems.numChildItemID = Item.numItemCode
	LEFT JOIN
		ItemImages
	ON
		ItemImages.numDomainId=@numDomainID
		AND ItemImages.numItemCode = Item.numItemCode
		AND ItemImages.bitDefault=1 
		AND ItemImages.bitIsImage=1
	INNER JOIN
		WareHouseItems
	ON
		OpportunityKitItems.numWareHouseItemId = WareHouseItems.numWareHouseItemID
	INNER JOIN
		Warehouses
	ON
		WareHouseItems.numWareHouseID = Warehouses.numWareHouseID
	LEFT JOIN
		WarehouseLocation
	ON
		WareHouseItems.numWLocationID = WarehouseLocation.numWLocationID
	WHERE
		OpportunityMaster.numDomainID=@numDomainID
		AND ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyPicked,0) > 0
		AND ISNULL(Item.bitKitParent,0) = 0
	ORDER BY
		T1.ID, OpportunityItems.numSortOrder

	INSERT INTO @TEMPItems
	(	
		ID
		,numOppID
		,numOppItemID
		,numOppChildItemID
		,numOppKitChildItemID
		,numItemCode
		,numWarehouseID
		,numWareHouseItemID
		,vcWarehouse 
		,vcLocation
		,vcKitChildItems
		,vcKitChildKitItems
		,vcPoppName
		,numBarCodeId
		,vcItemName
		,vcItemDesc
		,bitSerial
		,bitLot
		,bitKit
		,vcSKU
		,vcPathForTImage
		,vcInclusionDetails
		,numRemainingQty
		,numQtyPicked
		,numQtyRequiredKit
	)
	SELECT 
		T1.ID
		,OpportunityMaster.numOppID
		,OpportunityItems.numoppitemtCode
		,OpportunityKitChildItems.numOppChildItemID
		,OpportunityKitChildItems.numOppKitChildItemID
		,Item.numItemCode
		,WareHouseItems.numWareHouseID
		,WareHouseItems.numWareHouseItemID
		,Warehouses.vcWareHouse
		,WarehouseLocation.vcLocation
		,'' AS vcKitChildItems
		,'' vcKitChildKitItems
		,OpportunityMaster.vcPoppName
		,Item.numBarCodeId
		,Item.vcItemName
		,Item.txtItemDesc AS vcItemDesc
		,ISNULL(Item.bitSerialized,0)
		,ISNULL(Item.bitLotNo,0)
		,ISNULL(Item.bitKitParent,0)
		,Item.vcSKU
		,ISNULL(ItemImages.vcPathForTImage,'') vcPathForTImage
		,ISNULL(OpportunityItems.vcInclusionDetail,'') AS vcInclusionDetails
		,(ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyPicked,0)) * ISNULL(OpportunityKitItems.numQtyItemsReq_Orig,0) * ISNULL(OpportunityKitChildItems.numQtyItemsReq_Orig,0) numRemainingQty
		,0 numQtyPicked
		,ISNULL(OpportunityKitItems.numQtyItemsReq_Orig,0) * ISNULL(OpportunityKitChildItems.numQtyItemsReq_Orig,0)
	FROM
		@TEMP T1
	INNER JOIN
		OpportunityMaster
	ON
		OpportunityMaster.numOppID= T1.numOppID
	INNER JOIN
		OpportunityItems
	ON
		OpportunityMaster.numOppID = OpportunityItems.numOppID
		AND (OpportunityItems.numOppItemtcode = T1.numOppItemID OR ISNULL(T1.numOppItemID,0) = 0)
	INNER JOIN
		OpportunityKitChildItems
	ON
		OpportunityMaster.numOppID = OpportunityItems.numOppID
		AND (OpportunityKitChildItems.numOppItemID = T1.numOppItemID OR OpportunityKitChildItems.numOppItemID = OpportunityItems.numoppitemtCode)
	INNER JOIN
		OpportunityKitItems
	ON
		OpportunityKitChildItems.numOppChildItemID = OpportunityKitItems.numOppChildItemID
	INNER JOIN
		Item 
	ON
		OpportunityKitChildItems.numItemID = Item.numItemCode
	LEFT JOIN
		ItemImages
	ON
		ItemImages.numDomainId=@numDomainID
		AND ItemImages.numItemCode = Item.numItemCode
		AND ItemImages.bitDefault=1 
		AND ItemImages.bitIsImage=1
	INNER JOIN
		WareHouseItems
	ON
		OpportunityKitChildItems.numWareHouseItemId = WareHouseItems.numWareHouseItemID
	INNER JOIN
		Warehouses
	ON
		WareHouseItems.numWareHouseID = Warehouses.numWareHouseID
	LEFT JOIN
		WarehouseLocation
	ON
		WareHouseItems.numWLocationID = WarehouseLocation.numWLocationID
	WHERE
		OpportunityMaster.numDomainID=@numDomainID
		AND ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyPicked,0) > 0
		AND ISNULL(Item.bitKitParent,0) = 0
	ORDER BY
		T1.ID, OpportunityItems.numSortOrder

	IF ISNULL(@bitGroupByOrder,0) = 1
	BEGIN
		SELECT
			TEMP.numOppId
			,(CASE WHEN TEMP.bitKit = 1 OR ISNULL(TEMP.numOppChildItemID,0) > 0 OR ISNULL(TEMP.numOppKitChildItemID,0) > 0 THEN TEMP.numOppItemID ELSE 0 END) AS numOppItemID
			,TEMP.numOppChildItemID
			,TEMP.numOppKitChildItemID
			,TEMP.numItemCode
			,TEMP.numWareHouseID
			,TEMP.vcKitChildItems
			,TEMP.vcKitChildKitItems
			,STUFF((SELECT 
						CONCAT(',',T1.numOppItemID,'(',T1.numRemainingQty,')') 
					FROM 
						@TEMPItems T1 
					WHERE 
						T1.numOppId = TEMP.numOppId
						AND T1.numItemCode = TEMP.numItemCode
						AND T1.numWareHouseID = TEMP.numWareHouseID
						AND ISNULL(T1.vcKitChildItems,'') = ISNULL(TEMP.vcKitChildItems,'')
						AND ISNULL(T1.vcKitChildKitItems,'') = ISNULL(TEMP.vcKitChildKitItems ,'')
					FOR XML PATH('')),1,1,'') vcOppItemIDs
			,vcPoppName
			,numBarCodeId
			,CONCAT(TEMP.vcWarehouse, (CASE WHEN LEN(ISNULL(TEMP.vcLocation,'')) > 0 THEN CONCAT(' (',TEMP.vcLocation,')') ELSE '' END)) vcLocation
			,vcItemName
			,vcItemDesc
			,bitSerial
			,bitLot
			,bitKit
			,vcSKU
			,dbo.fn_GetItemAttributes(@numDomainID,TEMP.numItemCode,0) vcAttributes
			,vcPathForTImage
			,MIN(vcInclusionDetails) vcInclusionDetails
			,SUM(TEMP.numRemainingQty) numRemainingQty
			,0 numQtyPicked
			,numQtyRequiredKit
			,(CASE 
				WHEN EXISTS (SELECT numWareHouseItemID FROM WareHouseItems WIInner INNER JOIN WarehouseLocation WL ON WIInner.numWLocationID=WL.numWLocationID WHERE WIInner.numDomainID=@numDomainID AND WIInner.numItemID=TEMP.numItemCode AND WIInner.numWareHouseID = TEMP.numWareHouseID)
				THEN CONCAT('[',STUFF((SELECT 
											CONCAT(', {"WarehouseItemID":',WIInner.numWareHouseItemID,', "WarehouseLocationID":',ISNULL(WL.numWLocationID,0),', "Available":',(ISNULL(WIInner.numOnHand,0) + ISNULL(WIInner.numAllocation,0)),', "Location":"',WL.vcLocation,'"}') 
										FROM 
											WareHouseItems WIInner
										LEFT JOIN
											WarehouseLocation WL
										ON
											WIInner.numWLocationID = WL.numWLocationID
										WHERE 
											WIInner.numDomainID=@numDomainID 
											AND WIInner.numItemID=TEMP.numItemCode 
											AND WIInner.numWareHouseID = TEMP.numWareHouseID
											AND (ISNULL(WL.numWLocationID,0) > 0 OR WIInner.numWareHouseItemID=TEMP.numWareHouseItemID)
										ORDER BY
											WL.vcLocation
										FOR XML PATH('')),1,1,''),']')
				ELSE CONCAT('[',STUFF((SELECT 
									CONCAT(', {"WarehouseItemID":',WIInner.numWareHouseItemID,', "WarehouseLocationID":',ISNULL(WL.numWLocationID,0),', "Available":',(ISNULL(WIInner.numOnHand,0) + ISNULL(WIInner.numAllocation,0)),', "Location":""}')
								FROM 
									WareHouseItems WIInner
								LEFT JOIN
									WarehouseLocation WL
								ON
									WIInner.numWLocationID = WL.numWLocationID
								WHERE 
									WIInner.numDomainID=@numDomainID 
									AND WIInner.numItemID=TEMP.numItemCode
									AND WIInner.numWareHouseID = TEMP.numWareHouseID
								FOR XML PATH('')),1,1,''),']')
			END)  vcWarehouseLocations
		FROM
			@TEMPItems TEMP
		GROUP BY
			TEMP.numOppId
			,(CASE WHEN TEMP.bitKit = 1 OR ISNULL(TEMP.numOppChildItemID,0) > 0 OR ISNULL(TEMP.numOppKitChildItemID,0) > 0 THEN TEMP.numOppItemID ELSE 0 END)
			,TEMP.numOppChildItemID
			,TEMP.numOppKitChildItemID
			,TEMP.numItemCode
			,TEMP.numWareHouseID
			,TEMP.numWareHouseItemID
			,TEMP.vcKitChildItems
			,TEMP.vcKitChildKitItems
			,TEMP.vcPoppName
			,TEMP.vcPathForTImage
			,TEMP.vcItemName
			,TEMP.bitSerial
			,TEMP.bitLot
			,TEMP.bitKit
			,TEMP.vcItemDesc
			,TEMP.vcSKU
			,TEMP.numBarCodeId
			,TEMP.vcWarehouse
			,TEMP.vcLocation
			,TEMP.numQtyRequiredKit
		ORDER BY
			MIN(ID)
			,numOppItemID
			,numOppChildItemID
			,numOppKitChildItemID
	END
	ELSE
	BEGIN
		SELECT
			0 AS numOppId
			,(CASE WHEN TEMP.bitKit = 1 OR ISNULL(TEMP.numOppChildItemID,0) > 0 OR ISNULL(TEMP.numOppKitChildItemID,0) > 0 THEN TEMP.numOppItemID ELSE 0 END) AS numOppItemID
			,TEMP.numOppChildItemID
			,TEMP.numOppKitChildItemID
			,TEMP.numItemCode
			,TEMP.numWareHouseID
			,TEMP.vcKitChildItems
			,TEMP.vcKitChildKitItems
			,STUFF((SELECT 
						CONCAT(', ',T1.numOppItemID,'(',T1.numRemainingQty,')') 
					FROM 
						@TEMPItems T1 
					WHERE 
						T1.numItemCode = TEMP.numItemCode
						AND T1.numWareHouseID = TEMP.numWareHouseID
						AND ISNULL(T1.vcKitChildItems,'') = ISNULL(TEMP.vcKitChildItems,'')
						AND ISNULL(T1.vcKitChildKitItems,'') = ISNULL(TEMP.vcKitChildKitItems ,'')
					FOR XML PATH('')),1,1,'') vcOppItemIDs
			,'' AS vcPoppName
			,numBarCodeId
			,CONCAT(TEMP.vcWarehouse, (CASE WHEN LEN(ISNULL(TEMP.vcLocation,'')) > 0 THEN CONCAT(' (',TEMP.vcLocation,')') ELSE '' END)) vcLocation
			,vcItemName
			,vcItemDesc
			,bitSerial
			,bitLot
			,bitKit
			,vcSKU
			,dbo.fn_GetItemAttributes(@numDomainID,TEMP.numItemCode,0) vcAttributes
			,vcPathForTImage
			,MIN(vcInclusionDetails) vcInclusionDetails
			,SUM(TEMP.numRemainingQty) numRemainingQty
			,0 numQtyPicked
			,numQtyRequiredKit
			,(CASE 
				WHEN EXISTS (SELECT numWareHouseItemID FROM WareHouseItems WIInner INNER JOIN WarehouseLocation WL ON WIInner.numWLocationID=WL.numWLocationID WHERE WIInner.numDomainID=@numDomainID AND WIInner.numItemID=TEMP.numItemCode AND WIInner.numWareHouseID = TEMP.numWareHouseID)
				THEN CONCAT('[',STUFF((SELECT 
											CONCAT(', {"WarehouseItemID":',WIInner.numWareHouseItemID,', "WarehouseLocationID":',ISNULL(WL.numWLocationID,0),', "Available":',(ISNULL(WIInner.numOnHand,0) + ISNULL(WIInner.numAllocation,0)),', "Location":"',WL.vcLocation,'"}') 
										FROM 
											WareHouseItems WIInner
										LEFT JOIN
											WarehouseLocation WL
										ON
											WIInner.numWLocationID = WL.numWLocationID
										WHERE 
											WIInner.numDomainID=@numDomainID 
											AND WIInner.numItemID=TEMP.numItemCode 
											AND WIInner.numWareHouseID = TEMP.numWareHouseID
											AND (ISNULL(WL.numWLocationID,0) > 0 OR WIInner.numWareHouseItemID=TEMP.numWareHouseItemID)
										ORDER BY
											WL.vcLocation
										FOR XML PATH('')),1,1,''),']')
				ELSE CONCAT('[',STUFF((SELECT 
									CONCAT(', {"WarehouseItemID":',WIInner.numWareHouseItemID,', "WarehouseLocationID":',ISNULL(WL.numWLocationID,0),', "Available":',(ISNULL(WIInner.numOnHand,0) + ISNULL(WIInner.numAllocation,0)),', "Location":""}')
								FROM 
									WareHouseItems WIInner
								LEFT JOIN
									WarehouseLocation WL
								ON
									WIInner.numWLocationID = WL.numWLocationID
								WHERE 
									WIInner.numDomainID=@numDomainID 
									AND WIInner.numItemID=TEMP.numItemCode
									AND WIInner.numWareHouseID = TEMP.numWareHouseID
								FOR XML PATH('')),1,1,''),']')
			END)  vcWarehouseLocations
		FROM
			@TEMPItems TEMP
		GROUP BY
			(CASE WHEN TEMP.bitKit = 1 OR ISNULL(TEMP.numOppChildItemID,0) > 0 OR ISNULL(TEMP.numOppKitChildItemID,0) > 0 THEN TEMP.numOppItemID ELSE 0 END)
			,TEMP.numOppChildItemID
			,TEMP.numOppKitChildItemID
			,TEMP.numItemCode
			,TEMP.numWareHouseID
			,TEMP.numWareHouseItemID
			,TEMP.vcKitChildItems
			,TEMP.vcKitChildKitItems
			,TEMP.vcPathForTImage
			,TEMP.vcItemName
			,TEMP.vcItemDesc
			,TEMP.bitSerial
			,TEMP.bitLot
			,TEMP.vcSKU
			,TEMP.bitKit
			,TEMP.numBarCodeId
			,TEMP.vcWarehouse
			,TEMP.vcLocation
			,TEMP.numQtyRequiredKit
		ORDER BY
			MIN(ID)
			,numOppItemID
			,numOppChildItemID
			,numOppKitChildItemID
	END
	
	SELECT * FROM @TempFieldsRight ORDER BY intRowNum
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_OPPBizDocItems_GetWithKitChilds' ) 
    DROP PROCEDURE USP_OPPBizDocItems_GetWithKitChilds
GO
CREATE PROCEDURE [dbo].[USP_OPPBizDocItems_GetWithKitChilds]
(
    @numOppId AS NUMERIC(9) = NULL,
    @numOppBizDocsId AS NUMERIC(9) = NULL,
    @numDomainID AS NUMERIC(9) = 0      
)
AS 
BEGIN
    DECLARE @DivisionID AS NUMERIC(9)      
    DECLARE @tintTaxOperator AS TINYINT
	DECLARE @tintCommitAllocation TINYINT
	SELECT @tintCommitAllocation=ISNULL(tintCommitAllocation,1) FROM Domain WHERE numDomainId=@numDomainID
                                                                                                                                                                                                                                                                                                                                          
    SELECT  
		@DivisionID = numDivisionID, 
		@tintTaxOperator = [tintTaxOperator]           
    FROM    
		OpportunityMaster
    WHERE   
		numOppId = @numOppId                                                                                                                                                                          

    
                                                                                          
    DECLARE @strSQL AS VARCHAR(MAX)                                                                                          
    DECLARE @strSQL1 AS VARCHAR(MAX)                                       
    DECLARE @strSQLCusFields VARCHAR(MAX) = ''
    DECLARE @strSQLEmpFlds VARCHAR(MAX) = ''                                                                         
                                                                                              
    DECLARE @tintType AS TINYINT   
    DECLARE @tintOppType AS TINYINT                                                      
    DECLARE @intRowNum AS INT                                                  
    DECLARE @numFldID AS VARCHAR(15)                                          
    DECLARE @vcFldname AS VARCHAR(200)                                            
    DECLARE @numBizDocId AS NUMERIC(9)
    DECLARE @numBizDocTempID AS NUMERIC(9) = 0
	DECLARE @bitDisplayKitChild AS BIT = 0
        
                                           
    SELECT  
		@numBizDocTempID = ISNULL(numBizDocTempID, 0),
        @numBizDocId = numBizDocId
    FROM    
		OpportunityBizDocs
    WHERE   
		numOppBizDocsId = @numOppBizDocsId

	
	SELECT
		@bitDisplayKitChild = ISNULL(bitDisplayKitChild,0)
	FROM	
		BizDocTemplate
	WHERE
		numDomainID = @numDomainID
		AND numBizDocTempID = @numBizDocTempID


    SELECT  
		@tintType = tintOppType,
        @tintOppType = tintOppType
    FROM    
		OpportunityMaster
    WHERE   
		numOppID = @numOppId                                                                                          
                                                                                   
    IF @tintType = 1 
        SET @tintType = 7
    ELSE 
        SET @tintType = 8     
		
		
	IF (SELECT 
			COUNT(*) 
		FROM 
			View_DynamicColumns 
		WHERE   
			numFormID = @tintType 
			AND numDomainID = @numDomainID
            AND numAuthGroupID = @numBizDocId 
			AND vcFieldType = 'R'
			AND ISNULL(numRelCntType, 0) = @numBizDocTempID
		)=0
	BEGIN            
		INSERT INTO DycFormConfigurationDetails 
		(
			numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numAuthGroupID,numRelCntType
		)
		SELECT 
			numFormId,numFieldId,1,tintRow,@numDomainID,@numBizDocId,@numBizDocTempID
		FROM 
			View_DynamicDefaultColumns
		WHERE 
			numFormId=@tintType and bitDefault=1 and numDomainID=@numDomainID order by tintRow ASC	
	 
	 
		IF (SELECT 
				COUNT(*) 
			FROM 
				DycFrmConfigBizDocsSumm Ds 
			WHERE 
				numBizDocID=@numBizDocId 
				AND Ds.numFormID=@tintType 
				AND ISNULL(Ds.numBizDocTempID,0)=@numBizDocTempID 
				AND Ds.numDomainID=@numDomainID
			)=0
		BEGIN
			INSERT INTO DycFrmConfigBizDocsSumm 
			(
				numFormID,numBizDocID,numFieldID,numDomainID,numBizDocTempID
			)
			SELECT 
				@tintType,@numBizDocId,DFM.numFieldID,@numDomainID,@numBizDocTempID 
			FROM
				DycFormField_Mapping DFFM 
			JOIN 
				DycFieldMaster DFM 
			ON 
				DFFM.numModuleID=DFFM.numModuleID and DFFM.numFieldID=DFM.numFieldID
			WHERE 
				numFormID=16 
				AND DFFM.numDomainID IS NULL
		END
	END      

	IF OBJECT_ID('tempdb..#TempBizDocProductGridColumns') IS NOT NULL DROP TABLE #TempBizDocProductGridColumns
	SELECT * INTO #TempBizDocProductGridColumns FROM (
    SELECT  
		vcFieldName AS vcFormFieldName,
        vcDbColumnName,
        vcFieldDataType,
        tintRow AS intRowNum
    FROM    
		View_DynamicColumns
    WHERE   
		numFormID = @tintType
        AND numDomainID = @numDomainID
        AND numAuthGroupID = @numBizDocId
        AND vcFieldType = 'R'
        AND ISNULL(numRelCntType, 0) = @numBizDocTempID
    UNION
    SELECT  
		vcFieldName AS vcFormFieldName,
        vcDbColumnName,
        'C' vcFieldDataType,
        tintRow AS intRowNum
    FROM    
		View_DynamicCustomColumns
    WHERE   
		numFormID = @tintType
        AND Grp_id = 5
        AND numDomainID = @numDomainID
        AND numAuthGroupID = @numBizDocId
        AND bitCustom = 1
        AND ISNULL(numRelCntType, 0) = @numBizDocTempID
    ) t1   
	ORDER BY 
		intRowNum                                                                              
                                                                                
    SELECT  
		*
    INTO    
		#Temp1
    FROM    
	( 
		SELECT   
			Opp.vcitemname AS vcItemName,
            ISNULL(OBD.numOppBizDocItemID, 0) OppBizDocItemID,
			(CASE 
				WHEN OM.tintOppType = 1 AND OM.tintOppStatus = 1 
				THEN 
					(CASE 
						WHEN (I.charItemType = 'p' AND ISNULL(Opp.bitDropShip, 0) = 0 AND ISNULL(I.bitAsset,0)=0)
						THEN 
							CAST(dbo.CheckOrderItemInventoryStatus(Opp.numItemCode,Opp.numUnitHour,Opp.numoppitemtCode,Opp.numWarehouseItmsID,(CASE WHEN ISNULL(bitKitParent,0) = 1 AND ISNULL(bitAssembly,0) = 1 THEN 0  WHEN ISNULL(bitKitParent,0) = 1 THEN 1 ELSE 0 END)) AS VARCHAR)
						ELSE ''
					END) 
				ELSE ''
			END) vcBackordered,
			ISNULL(WI.numBackOrder,0) AS numBackOrder,
            CASE WHEN charitemType = 'P' THEN 'Product'
                    WHEN charitemType = 'S' THEN 'Service'
            END AS charitemType,
            CASE WHEN charitemType = 'P' THEN 'Inventory'
	                WHEN charitemType = 'N' THEN 'Non-Inventory'
                    WHEN charitemType = 'S' THEN 'Service'
            END AS vcItemType,
			CONVERT(VARCHAR(2500), OBD.vcitemDesc) AS txtItemDesc, 
			opp.numUnitHour AS numOrgTotalUnitHour,
			dbo.fn_UOMConversion(ISNULL(i.numBaseUnit, 0),i.numItemCode, @numDomainID,ISNULL(opp.numUOMId, 0)) * opp.numUnitHour AS numTotalUnitHour,                                   
            dbo.fn_UOMConversion(ISNULL(i.numBaseUnit, 0),i.numItemCode, @numDomainID,ISNULL(opp.numUOMId, 0)) * OBD.numUnitHour AS numUnitHour,
            CONVERT(DECIMAL(20,5), (dbo.fn_UOMConversion(ISNULL(opp.numUOMId, 0), i.numItemCode, @numDomainID, ISNULL(i.numBaseUnit, 0)) * OBD.monPrice)) monPrice,
            CONVERT(VARCHAR, CONVERT(DECIMAL(20,5), OBD.monTotAmount)) Amount,
            OBD.monTotAmount/*Fo calculating sum*/,
            ISNULL(CONVERT(VARCHAR, i.monListPrice), 0) AS monListPrice,
            i.numItemCode AS ItemCode,
            opp.numoppitemtCode,
            L.vcdata AS numItemClassification,
            CASE WHEN bitTaxable = 0 THEN 'No'
                    ELSE 'Yes'
            END AS Taxable,
            numIncomeChartAcntId AS itemIncomeAccount,
            'NI' AS ItemType,
            ISNULL(GJH.numJournal_Id, 0) AS numJournalId,
            ( SELECT TOP 1
                        ISNULL(GJD.numTransactionId, 0)
                FROM      General_Journal_Details GJD
                WHERE     GJH.numJournal_Id = GJD.numJournalId
                        AND GJD.chBizDocItems = 'NI'
                        AND GJD.numoppitemtCode = opp.numoppitemtCode
            ) AS numTransactionId,
            ISNULL(Opp.vcModelID, '') vcModelID,
            (CASE WHEN charitemType = 'P' THEN dbo.USP_GetAttributes(OBD.numWarehouseItmsID, bitSerialized) ELSE ISNULL(OBD.vcAttributes,0) END) AS vcAttributes,
            ISNULL(vcPartNo, '') AS vcPartNo,
           (CASE WHEN ISNULL(opp.bitMarkupDiscount,0) = 1 
					THEN 0
					ELSE (ISNULL(OBD.monTotAmtBefDiscount, OBD.monTotAmount) - OBD.monTotAmount ) 
			END)
            AS DiscAmt,		
			(CASE WHEN OBD.numUnitHour = 0 THEN 0 ELSE CONVERT(DECIMAL(20,5),(dbo.fn_UOMConversion(ISNULL(opp.numUOMId, 0), i.numItemCode, @numDomainID, ISNULL(i.numBaseUnit, 0)) * (ISNULL(OBD.monTotAmount,0) / OBD.numUnitHour))) END) As monUnitSalePrice,
            ISNULL(OBD.vcNotes,ISNULL(opp.vcNotes,'')) vcNotes,
            OBD.vcTrackingNo,
            OBD.vcShippingMethod,
            OBD.monShipCost,
            [dbo].[FormatedDateFromDate](OBD.dtDeliveryDate,@numDomainID) dtDeliveryDate,
            Opp.numWarehouseItmsID,
            ISNULL(u.vcUnitName, '') numUOMId,
            ISNULL(Opp.vcManufacturer, '') AS vcManufacturer,
            ISNULL(V.monCost, 0) AS VendorCost,
            Opp.vcPathForTImage,
            i.[fltLength],
            i.[fltWidth],
            i.[fltHeight],
            i.[fltWeight],
            ISNULL(I.numVendorID, 0) numVendorID,
            Opp.bitDropShip AS DropShip,
            SUBSTRING(( SELECT  ' ,' + vcSerialNo
                                + CASE WHEN ISNULL(I.bitLotNo, 0) = 1
                                        THEN ' ('
                                            + CONVERT(VARCHAR(15), oppI.numQty)
                                            + ')'
                                        ELSE ''
                                    END
                        FROM    OppWarehouseSerializedItem oppI
                                JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID = whi.numWareHouseItmsDTLID
                        WHERE   oppI.numOppID = Opp.numOppId
                                AND oppI.numOppItemID = Opp.numoppitemtCode
                        ORDER BY vcSerialNo
                        FOR
                        XML PATH('')
                        ), 3, 200000) AS SerialLotNo,
            ISNULL(opp.numUOMId, 0) AS numUOM,
            ISNULL(u.vcUnitName, '') vcUOMName,
            dbo.fn_UOMConversion(opp.numUOMId, Opp.numItemCode,
                                    @numDomainID, NULL) AS UOMConversionFactor,
            ISNULL(Opp.numSOVendorId, 0) AS numSOVendorId,
            [dbo].FormatedDateTimeFromDate(OBD.dtRentalStartDate,
                                            @numDomainID) dtRentalStartDate,
            [dbo].FormatedDateTimeFromDate(OBD.dtRentalReturnDate,
                                            @numDomainID) dtRentalReturnDate,
            ISNULL(W.vcWareHouse, '') AS Warehouse,
            CASE WHEN charitemType = 'P' AND ISNULL(numItemGroup,0) > 0 AND ISNULL(bitMatrix,0)=0 THEN ISNULL(WI.vcWHSKU,I.vcSKU) ELSE I.vcSKU END vcSKU,
            CASE WHEN charitemType = 'P' AND ISNULL(numItemGroup,0) > 0 AND ISNULL(bitMatrix,0)=0 THEN ISNULL(WI.vcBarCode,I.numBarCodeId) ELSE I.numBarCodeId END vcBarcode,
			CASE WHEN charitemType = 'P' AND ISNULL(numItemGroup,0) > 0 AND ISNULL(bitMatrix,0)=0 THEN ISNULL(WI.vcBarCode,I.numBarCodeId) ELSE I.numBarCodeId END numBarCodeId,
            ISNULL(opp.numClassID, 0) AS numClassID,
            ISNULL(opp.numProjectID, 0) AS numProjectID,
            ISNULL(I.[numShipClass],0) AS numShipClass,OBD.numOppBizDocID
			,(CASE 
				WHEN OB.numBizDocId IN (29397) 
				THEN  STUFF((
					SELECT 
						CONCAT(',',WLInner.vcLocation,' (',ISNULL(WIInner.numOnHand,0) + ISNULL(WIInner.numAllocation,0),')')
					FROM 
						WareHouseItems WIInner
					INNER JOIN
						WarehouseLocation WLInner
					ON
						WIInner.numWLocationID = WLInner.numWLocationID
					WHERE 
						WIInner.numItemID = opp.numItemCode
						AND WIInner.numWareHouseID=WI.numWareHouseID
						AND (ISNULL(WIInner.numOnHand,0) + ISNULL(WIInner.numAllocation,0)) > 0
					FOR XML PATH, TYPE).value(N'.[1]', N'nvarchar(max)'),1,1,'')
				ELSE WL.vcLocation 
			END) vcLocation
			,ISNULL(bitSerialized,0) AS bitSerialized,ISNULL(bitLotNo,0) AS bitLotNo,
            opp.numOppId,OBD.numUnitHour AS numUnitHourOrig,ISNULL(I.numItemCode,0) AS [numItemCode],
			ISNULL(I.bitKitParent,0) AS bitKitParent,
			0 AS numOppChildItemID, 
			0 AS numOppKitChildItemID,
			0 AS bitChildItem,
			0 AS tintLevel,
			ISNULL(opp.vcPromotionDetail,'') AS vcPromotionDetail,
			(CASE WHEN ISNULL(WI.numWareHouseItemID,0) > 0 THEN W.vcWareHouse ELSE '' END) vcWarehouse,
			(CASE WHEN (SELECT COUNT(*) FROM #TempBizDocProductGridColumns WHERE vcDbColumnName='vcInclusionDetails') > 0 THEN dbo.GetOrderAssemblyKitInclusionDetails(opp.numOppID,Opp.numoppitemtCode,OBD.numUnitHour,@tintCommitAllocation,1) ELSE '' END) AS vcInclusionDetails,
			Opp.numSortOrder
			,CAST(ISNULL(Opp.numCost,0) AS DECIMAL(20,5)) AS numCost, 
			ISNULL((SELECT SUM(numOnHand) FROM WarehouseItems WHERE WarehouseItems.numDomainID=@numDomainID AND WarehouseItems.numItemID=I.numItemCode AND WarehouseItems.numWarehouseID=W.numWareHouseID),0) numOnHand
			,dbo.FormatedDateFromDate(opp.ItemReleaseDate,@numDomainID) dtReleaseDate
			,(CASE WHEN I.bitKitParent = 1 THEN ISNULL(dbo.fn_GetKitAvailabilityByWarehouse(I.numItemCode,W.numWareHouseID),0) ELSE (ISNULL(numOnHand,0) + ISNULL(numAllocation,0)) END) AS numAvailable
			,CPN.CustomerPartNo
			,ISNULL(opp.bitWinningBid,0) bitWinningBid
			,ISNULL(opp.numParentOppItemID,0) numParentOppItemID
			,ISNULL(opp.numUnitHour,0) - ISNULL(TEMPBizDocQty.numBizDocQty,0) AS numRemainingQty
			,dbo.FormatedDateFromDate(opp.ItemRequiredDate,@numDomainID) ItemRequiredDate
        FROM      
			OpportunityItems opp
        JOIN 
			OpportunityBizDocItems OBD 
		ON 
			OBD.numOppItemID = opp.numoppitemtCode
		JOIN
			OpportunityBizDocs OB
		ON
			OBD.numOppBizDocID=OB.numOppBizDocsID
        LEFT JOIN 
			item i 
		ON 
			opp.numItemCode = i.numItemCode
        LEFT JOIN 
			ListDetails L 
		ON 
			i.numItemClassification = L.numListItemID
        LEFT JOIN 
			Vendor V 
		ON 
			V.numVendorID = @DivisionID
            AND V.numItemCode = opp.numItemCode
        LEFT JOIN 
			General_Journal_Header GJH 
		ON 
			opp.numoppid = GJH.numOppId
            AND GJH.numBizDocsPaymentDetId IS NULL
            AND GJH.numOppBizDocsId = @numOppBizDocsId
        LEFT JOIN 
			UOM u 
		ON 
			u.numUOMId = opp.numUOMId                                      
        LEFT JOIN 
			dbo.WareHouseItems WI 
		ON 
			opp.numWarehouseItmsID = WI.numWareHouseItemID
			AND WI.numDomainID = @numDomainID
        LEFT JOIN 
			dbo.Warehouses W 
		ON 
			WI.numDomainID = W.numDomainID
            AND WI.numWareHouseID = W.numWareHouseID
        LEFT JOIN 
			dbo.WarehouseLocation WL 
		ON 
			WL.numWLocationID = WI.numWLocationID
		LEFT JOIN
			dbo.OpportunityMaster OM
		ON 
			OM.numOppId = opp.numOppId
		LEFT JOIN	
			DivisionMaster DM
		ON
			OM.numDivisionId= DM.numDivisionID
		LEFT JOIN 
			CustomerPartNumber CPN
		ON 
			opp.numItemCode = CPN.numItemCode AND CPN.numDomainId = @numDomainID AND CPN.numCompanyId = DM.numCompanyID
		OUTER APPLY
		(
			SELECT
				SUM(OBDIInner.numUnitHour) AS numBizDocQty
			FROM
				OpportunityBizDocs OBDInner
			INNER JOIN
				OpportunityBizDocItems OBDIInner
			ON
				OBDInner.numOppBizDocsId = OBDIInner.numOppBizDocID
			WHERE
				OBDInner.numOppId = @numOppId
				AND OBDInner.numBizDocId = OB.numBizDocId
				AND OBDIInner.numOppItemID = OBD.numOppItemID
		) TEMPBizDocQty
        WHERE     
			opp.numOppId = @numOppId
            AND (bitShowDeptItem IS NULL OR bitShowDeptItem = 0)
            AND OBD.numOppBizDocID = @numOppBizDocsId
    ) X
	ORDER BY
		numSortOrder
	OPTION ( OPTIMIZE FOR (@numOppId UNKNOWN, @numOppBizDocsId UNKNOWN))

	--GET KIT CHILD ITEMS (NOW WE ARE SUPPORTING KIT WITHIN KIT BUT ONLY 1 LEVEL MEANS CHILD KIT INSIDE PARENT KIT CAN NOT HAVE ANOTHER KIT AS CHILD)
	IF (@bitDisplayKitChild=1 AND (SELECT COUNT(*) FROM OpportunityKitItems WHERE numOppId = @numOppId ) > 0)
	BEGIN
		DECLARE @TEMPTABLE TABLE(
			numOppItemCode NUMERIC(18,0),
			numOppChildItemID NUMERIC(18,0), 
			numOppKitChildItemID NUMERIC(18,0),
			numItemCode NUMERIC(18,0),
			numWarehouseItemID NUMERIC(18,0),
			numTotalQty FLOAT,
			numQty FLOAT,
			numUOMID NUMERIC(18,0),
			numQtyShipped FLOAT,
			tintLevel TINYINT,
			numSortOrder INT
		);

		--FIRST LEVEL CHILD
		INSERT INTO @TEMPTABLE
		(
			numOppItemCode,numOppChildItemID,numOppKitChildItemID,numItemCode,numWarehouseItemID,numTotalQty,numQty,numUOMID,numQtyShipped,tintLevel,numSortOrder
		)
		SELECT 
			OKI.numOppItemID,
			OKI.numOppChildItemID,
			CAST(0 AS NUMERIC(18,0)),
			OKI.numChildItemID,
			ISNULL(OKI.numWareHouseItemId,0),
			CAST((t1.numOrgTotalUnitHour * OKI.numQtyItemsReq_Orig) AS FLOAT),
			CAST((t1.numUnitHour * OKI.numQtyItemsReq_Orig) AS FLOAT),
			ISNULL(OKI.numUOMID,0),
			ISNULL(numQtyShipped,0)
			,1
			,t1.numSortOrder
		FROM 
			#Temp1 t1
		INNER JOIN
			OpportunityKitItems OKI
		ON
			t1.numoppitemtCode = OKI.numOppItemID
		WHERE
			t1.bitKitParent = 1

		--SECOND LEVEL CHILD
		INSERT INTO @TEMPTABLE
		(
			numOppItemCode,numOppChildItemID,numOppKitChildItemID,numItemCode,numWarehouseItemID,numTotalQty,numQty,numUOMID,numQtyShipped,tintLevel,numSortOrder
		)
		SELECT 
			OKCI.numOppItemID,
			OKCI.numOppChildItemID,
			OKCI.numOppKitChildItemID,
			OKCI.numItemID,
			ISNULL(OKCI.numWareHouseItemId,0),
			CAST((c.numTotalQty * OKCI.numQtyItemsReq_Orig) AS NUMERIC(18,2)),
			CAST((c.numQty * OKCI.numQtyItemsReq_Orig) AS NUMERIC(18,2)),
			ISNULL(OKCI.numUOMID,0),
			ISNULL(OKCI.numQtyShipped,0),
			2
			,c.numSortOrder
		FROM 
			OpportunityKitChildItems OKCI
		INNER JOIN
			@TEMPTABLE c
		ON
			OKCI.numOppID = @numOppId
			AND OKCI.numOppItemID = c.numOppItemCode
			AND OKCI.numOppChildItemID = c.numOppChildItemID

		INSERT INTO 
			#Temp1
		SELECT   
			(CASE WHEN t2.tintLevel = 1 THEN CONCAT('&nbsp;&nbsp;',I.vcItemName) WHEN t2.tintLevel = 2 THEN CONCAT('&nbsp;&nbsp;&nbsp;&nbsp;',I.vcItemName) ELSE I.vcItemName END) AS vcItemName,
            0 OppBizDocItemID
			,(CASE WHEN (t2.numTotalQty-ISNULL(t2.numQtyShipped,0)) > ISNULL(WI.numAllocation,0) THEN ((t2.numTotalQty-ISNULL(t2.numQtyShipped,0)) - ISNULL(WI.numAllocation,0)) * dbo.fn_UOMConversion(ISNULL(i.numBaseUnit, 0),i.numItemCode, @numDomainID,ISNULL(t2.numUOMID, 0))  ELSE 0 END) AS vcBackordered
			,ISNULL(WI.numBackOrder,0) AS numBackOrder
            ,(CASE WHEN I.charitemType = 'P' THEN 'Product' WHEN charitemType = 'S' THEN 'Service' END) AS charitemType,
            (CASE WHEN charitemType = 'P' THEN 'Inventory'
	                WHEN charitemType = 'N' THEN 'Non-Inventory'
                    WHEN charitemType = 'S' THEN 'Service'
            END) AS vcItemType,
			CONVERT(VARCHAR(2500), I.txtItemDesc) AS txtItemDesc,   
			t2.numTotalQty AS numOrigTotalUnitHour,
			t2.numTotalQty AS numTotalUnitHour,
            t2.numQty AS numUnitHour,
            (ISNULL(dbo.fn_UOMConversion(ISNULL(t2.numUOMID, 0), i.numItemCode, @numDomainID, ISNULL(i.numBaseUnit, 0)),1) * (CASE WHEN I.charitemType = 'P' THEN ISNULL(WI.monWListPrice,0) ELSE ISNULL(I.monListPrice,0) END)) monPrice,
            CONVERT(VARCHAR, CONVERT(DECIMAL(20,5),((t2.numQty * ISNULL(dbo.fn_UOMConversion(ISNULL(t2.numUOMID, 0), i.numItemCode, @numDomainID, ISNULL(i.numBaseUnit, 0)),1)) * (CASE WHEN I.charitemType = 'P' THEN ISNULL(WI.monWListPrice,0) ELSE ISNULL(I.monListPrice,0) END)))) Amount,
            ((t2.numQty * ISNULL(dbo.fn_UOMConversion(ISNULL(t2.numUOMID, 0), i.numItemCode, @numDomainID, ISNULL(i.numBaseUnit, 0)),1)) * (CASE WHEN I.charitemType = 'P' THEN ISNULL(WI.monWListPrice,0) ELSE ISNULL(I.monListPrice,0) END)) AS monTotAmount/*Fo calculating sum*/,
            ISNULL(CONVERT(VARCHAR, (CASE WHEN I.charitemType = 'P' THEN ISNULL(WI.monWListPrice,0) ELSE ISNULL(I.monListPrice,0) END)), 0) AS monListPrice,
            I.numItemCode AS ItemCode,
            t2.numOppItemCode,
            L.vcdata AS numItemClassification,
            CASE WHEN bitTaxable = 0 THEN 'No' ELSE 'Yes' END AS Taxable,
            numIncomeChartAcntId AS itemIncomeAccount,
            'NI' AS ItemType,
            0 AS numJournalId,
            0 AS numTransactionId,
            ISNULL(I.vcModelID, '') vcModelID,
            dbo.USP_GetAttributes(t2.numWarehouseItemID, bitSerialized) AS vcAttributes,
            ISNULL(vcPartNo, '') AS vcPartNo,
            0 AS DiscAmt,
			(ISNULL(dbo.fn_UOMConversion(ISNULL(t2.numUOMID, 0), i.numItemCode, @numDomainID, ISNULL(i.numBaseUnit, 0)),1) * (CASE WHEN I.charitemType = 'P' THEN ISNULL(WI.monWListPrice,0) ELSE ISNULL(I.monListPrice,0) END)) As monUnitSalePrice,
            '' AS vcNotes,'' AS vcTrackingNo,'' AS vcShippingMethod,0 AS monShipCost,NULL AS dtDeliveryDate,t2.numWarehouseItemID,
            ISNULL(u.vcUnitName, '') numUOMId,ISNULL(I.vcManufacturer, '') AS vcManufacturer,ISNULL(V.monCost, 0) AS VendorCost,
            ISNULL((SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = I.numItemCode AND bitDefault=1),'') AS vcPathForTImage,
            I.[fltLength],I.[fltWidth],I.[fltHeight],I.[fltWeight],ISNULL(I.numVendorID, 0) numVendorID,0 AS DropShip,
            '' AS SerialLotNo,
            ISNULL(t2.numUOMId, 0) AS numUOM,
            ISNULL(u.vcUnitName, '') vcUOMName,
            dbo.fn_UOMConversion(t2.numUOMId, t2.numItemCode,@numDomainID, NULL) AS UOMConversionFactor,
            0 AS numSOVendorId,
            NULL AS dtRentalStartDate,
            NULL dtRentalReturnDate,
            ISNULL(W.vcWareHouse, '') AS Warehouse,
            CASE WHEN charitemType = 'P' AND ISNULL(numItemGroup,0) > 0 AND ISNULL(bitMatrix,0)=0 THEN ISNULL(WI.vcWHSKU,I.vcSKU) ELSE I.vcSKU END vcSKU,
            CASE WHEN charitemType = 'P' AND ISNULL(numItemGroup,0) > 0 AND ISNULL(bitMatrix,0)=0 THEN ISNULL(WI.vcBarCode,I.numBarCodeId) ELSE I.numBarCodeId END vcBarcode,
			CASE WHEN charitemType = 'P' AND ISNULL(numItemGroup,0) > 0 AND ISNULL(bitMatrix,0)=0 THEN ISNULL(WI.vcBarCode,I.numBarCodeId) ELSE I.numBarCodeId END numBarCodeId,
            ISNULL(I.numItemClass, 0) AS numClassID,
            0 AS numProjectID,
            ISNULL(I.[numShipClass],0) AS numShipClass,
			@numOppBizDocsId AS numOppBizDocID,WL.vcLocation,ISNULL(bitSerialized,0) AS bitSerialized,ISNULL(bitLotNo,0) AS bitLotNo,
            @numOppId AS numOppId,t2.numQty AS numUnitHourOrig,
			ISNULL(I.numItemCode,0) AS [numItemCode],
			ISNULL(I.bitKitParent,0) AS bitKitParent,
			ISNULL(t2.numOppChildItemID,0) AS numOppChildItemID, 
			ISNULL(t2.numOppKitChildItemID,0) AS numOppKitChildItemID,
			1 AS bitChildItem,
			t2.tintLevel AS tintLevel,
			'' AS vcPromotionDetail,
			(CASE WHEN ISNULL(WI.numWareHouseItemID,0) > 0 THEN W.vcWareHouse ELSE '' END) vcWarehouse,
			'' vcInclusionDetails
			,0
			,0
			,ISNULL((SELECT SUM(numOnHand) FROM WarehouseItems WHERE WarehouseItems.numDomainID=@numDomainID AND WarehouseItems.numItemID=I.numItemCode AND WarehouseItems.numWarehouseID=W.numWareHouseID),0) numOnHand
			, ''
			,(CASE WHEN I.bitKitParent = 1 THEN ISNULL(dbo.fn_GetKitAvailabilityByWarehouse(I.numItemCode,W.numWareHouseID),0) ELSE (ISNULL(numOnHand,0) + ISNULL(numAllocation,0)) END) AS numAvailable
			,''
			,0
			,0
			,0
			,NULL
		FROM
			@TEMPTABLE t2
		INNER JOIN
			Item I
		ON
			t2.numItemCode = I.numItemCode
		LEFT JOIN
			WareHouseItems WI
		ON
			WI.numWareHouseItemID = t2.numWarehouseItemID
		LEFT JOIN 
			dbo.Warehouses W 
		ON 
			WI.numDomainID = W.numDomainID
            AND WI.numWareHouseID = W.numWareHouseID
        LEFT JOIN 
			dbo.WarehouseLocation WL 
		ON 
			WL.numWLocationID = WI.numWLocationID
		LEFT JOIN 
			ListDetails L 
		ON 
			I.numItemClassification = L.numListItemID
		LEFT JOIN 
			Vendor V 
		ON 
			V.numVendorID = @DivisionID
            AND V.numItemCode = t2.numItemCode
		LEFT JOIN 
			UOM u 
		ON 
			u.numUOMId = t2.numUOMId 
	END


    DECLARE @strSQLUpdate AS VARCHAR(MAX);SET @strSQLUpdate=''
   
    EXEC ('ALTER TABLE #Temp1 ADD [TotalTax] DECIMAL(20,5)')

    IF @tintTaxOperator = 1 --add Sales tax
	BEGIN
		SET @strSQLUpdate = @strSQLUpdate
            + ',[TotalTax]= (CASE WHEN ISNULL(bitChildItem,0) = 0 THEN dbo.fn_CalBizDocTaxAmt('
            + CONVERT(VARCHAR(20), @numDomainID) + ',0,'
            + CONVERT(VARCHAR(20), @numOppId) +','+
            + 'numoppitemtCode,1,Amount,numUnitHour) ELSE 0 END)'
	END        
    ELSE IF @tintTaxOperator = 2 -- remove sales tax
	BEGIN
        SET @strSQLUpdate = @strSQLUpdate + ',[TotalTax]= 0'
	END
    ELSE 
	BEGIN
        SET @strSQLUpdate = @strSQLUpdate
            + ',[TotalTax]= (CASE WHEN ISNULL(bitChildItem,0) = 0 THEN dbo.fn_CalBizDocTaxAmt('
            + CONVERT(VARCHAR(20), @numDomainID) + ',0,'
            + CONVERT(VARCHAR(20), @numOppId) +','+
            + 'numoppitemtCode,1,Amount,numUnitHour) ELSE 0 END)'
    END

	-- CRV TAX TYPE
	EXEC ('ALTER TABLE #Temp1 ADD [CRV] DECIMAL(20,5)')

	IF @tintTaxOperator = 2
	BEGIN
		SET @strSQLUpdate = @strSQLUpdate
				+ ',[CRV]= (CASE WHEN ISNULL(bitChildItem,0) = 0 THEN dbo.fn_CalBizDocTaxAmt('
				+ CONVERT(VARCHAR(20), @numDomainID) + ',1,'
				+ CONVERT(VARCHAR(20), @numOppId) +','+
				+ 'numoppitemtCode,1,Amount,numUnitHour) ELSE 0 END)'
	END
	ELSE
	BEGIN
		SET @strSQLUpdate = @strSQLUpdate + ',[CRV]= 0'
	END

	DECLARE @vcTaxName AS VARCHAR(200)
    DECLARE @numTaxItemID AS NUMERIC(9)
    SET @numTaxItemID = 0
    
	SELECT TOP 1
        @vcTaxName = vcTaxName,
        @numTaxItemID = numTaxItemID
    FROM    
		TaxItems
    WHERE   
		numDomainID = @numDomainID

    WHILE @numTaxItemID > 0
    BEGIN
        EXEC ('ALTER TABLE #Temp1 ADD [' + @vcTaxName + '] DECIMAL(20,5)')

		IF @tintTaxOperator = 2 -- remove sales tax
		BEGIN
			SET @strSQLUpdate = @strSQLUpdate + ',[' + @vcTaxName + ']=0' 
		END
		ELSE
		BEGIN
			SET @strSQLUpdate = @strSQLUpdate + ',[' + @vcTaxName
				+ ']= (CASE WHEN ISNULL(bitChildItem,0) = 0 THEN dbo.fn_CalBizDocTaxAmt('
				+ CONVERT(VARCHAR(20), @numDomainID) + ','
				+ CONVERT(VARCHAR(20), @numTaxItemID) + ','
				+ CONVERT(VARCHAR(20), @numOppId) +','+
				+ 'numoppitemtCode,1,Amount,numUnitHour) ELSE 0 END)'
		END

        SELECT TOP 1
			@vcTaxName = vcTaxName,
            @numTaxItemID = numTaxItemID
        FROM    
			TaxItems
        WHERE   
			numDomainID = @numDomainID
            AND numTaxItemID > @numTaxItemID

        IF @@rowcount = 0 
            SET @numTaxItemID = 0
    END

    IF @strSQLUpdate <> '' 
    BEGIN
        SET @strSQLUpdate = 'UPDATE #Temp1 SET ItemCode=ItemCode' + @strSQLUpdate + ' WHERE ItemCode > 0 AND bitChildItem=0'
        EXEC (@strSQLUpdate)
    END

    DECLARE @vcDbColumnName NVARCHAR(100)

    SELECT TOP 1
            @numFldID = numfieldID,
            @vcFldname = vcFieldName,
            @vcDbColumnName = vcDbColumnName,
            @intRowNum = ( tintRow + 1 )
    FROM    View_DynamicCustomColumns
    WHERE   numFormID = @tintType
            AND Grp_id = 5
            AND numDomainID = @numDomainID
            AND numAuthGroupID = @numBizDocId
            AND bitCustom = 1
            AND ISNULL(numRelCntType, 0) = @numBizDocTempID
    ORDER BY tintRow     
	                                                                                     
    WHILE @intRowNum > 0                                                                                  
    BEGIN
        EXEC('ALTER TABLE #Temp1 ADD [' + @vcDbColumnName + '] varchar(MAX)')
	
        SET @strSQLUpdate = 'UPDATE #Temp1 set ItemCode=ItemCode,[' + @vcDbColumnName + ']= dbo.GetCustFldValueOppItems(' + @numFldID  + ',numoppitemtCode,ItemCode) WHERE ItemCode > 0 AND bitChildItem=0'
            
		EXEC (@strSQLUpdate)
		                   
        SELECT 
			TOP 1
            @numFldID = numfieldID,
            @vcFldname = vcFieldName,
            @vcDbColumnName = vcDbColumnName,
            @intRowNum = ( tintRow + 1 )
        FROM    
			View_DynamicCustomColumns
        WHERE   
			numFormID = @tintType
            AND Grp_id = 5
            AND numDomainID = @numDomainID
            AND numAuthGroupID = @numBizDocId
            AND bitCustom = 1
            AND tintRow >= @intRowNum
            AND ISNULL(numRelCntType, 0) = @numBizDocTempID
        ORDER BY 
			tintRow                                                                  
		           
        IF @@rowcount = 0 
            SET @intRowNum = 0                                                                                          
    END

    SELECT
		ROW_NUMBER() OVER ( ORDER BY numSortOrder) RowNumber,
        *
    FROM    
		#Temp1
    ORDER BY 
		numSortOrder ASC, tintLevel ASC, numoppitemtCode ASC, numOppChildItemID ASC, numOppKitChildItemID ASC

    DROP TABLE #Temp1

    SELECT
		ISNULL(tintOppStatus, 0)
    FROM    
		OpportunityMaster
    WHERE   
		numOppID = @numOppId                                                                                                 

	SELECT * FROM #TempBizDocProductGridColumns ORDER BY intRowNum

	IF OBJECT_ID('tempdb..#TempBizDocProductGridColumns') IS NOT NULL DROP TABLE #TempBizDocProductGridColumns
END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OPPGetINItems')
DROP PROCEDURE USP_OPPGetINItems
GO
-- =============================================  
-- Modified by: <Author,,Sachin Sadhu>  
-- Create date: <Create Date,,24thDec2013>  
-- Description: <Description,,add 3 fields:vcBizDocImagePath,vcBizDocFooter,vcPurBizDocFooter>  
-- =============================================  
CREATE PROCEDURE [dbo].[USP_OPPGetINItems]
(
	@byteMode AS TINYINT  = NULL,
	@numOppId AS NUMERIC(18,0)  = NULL,
	@numOppBizDocsId AS NUMERIC(18,0)  = NULL,
	@numDomainID AS NUMERIC(18,0)  = 0,
	@numUserCntID AS NUMERIC(9)  = 0,
	@ClientTimeZoneOffset INT=0
)
AS
BEGIN
	-- TRANSFERRED PARAMETER VALUES TO TEMPORARY VARIABLES TO AVAOID PARAMETER SNIFFING
	DECLARE @byteModeTemp AS TINYINT
	DECLARE @numOppIdTemp AS NUMERIC(18,0)
	DECLARE @numOppBizDocsIdTemp AS NUMERIC(18,0)
	DECLARE @numDomainIDTemp AS NUMERIC(18,0)
	DECLARE @numUserCntIDTemp AS NUMERIC(18,0)
	DECLARE @ClientTimeZoneOffsetTemp INT

	SET @byteModeTemp= @byteMode
	SET @numOppIdTemp = @numOppId
	SET @numOppBizDocsIdTemp=@numOppBizDocsId
	SET @numDomainIDTemp = @numDomainID
	SET @numUserCntIDTemp = @numUserCntID
	SET @ClientTimeZoneOffsetTemp = @ClientTimeZoneOffset


	IF @byteModeTemp = 1
    BEGIN
		DECLARE  @BizDcocName  AS VARCHAR(100)
		DECLARE  @OppName  AS VARCHAR(100)
		DECLARE  @Contactid  AS VARCHAR(100)
		DECLARE  @shipAmount  AS DECIMAL(20,5)
		DECLARE  @tintOppType  AS TINYINT
		DECLARE  @numlistitemid  AS VARCHAR(15)
		DECLARE  @RecOwner  AS VARCHAR(15)
		DECLARE  @MonAmount  AS VARCHAR(15)
		DECLARE  @OppBizDocID  AS VARCHAR(100)
		DECLARE  @bizdocOwner  AS VARCHAR(15)
		DECLARE  @tintBillType  AS TINYINT
		DECLARE  @tintShipType  AS TINYINT
		DECLARE  @numCustomerDivID AS NUMERIC(18,0)
		DECLARE @numDivisionID NUMERIC(18,0)

		DECLARE @numARContactPosition NUMERIC(18,0)
		DECLARE @numBizDocID NUMERIC(18,0)

		SELECT @numARContactPosition=ISNULL(numARContactPosition,0) FROM Domain WHERE numDomainId=@numDomainId
      
      SELECT @RecOwner = numRecOwner,@MonAmount = CONVERT(VARCHAR(20),monPAmount),
             @Contactid = numContactId,@OppName = vcPOppName,
             @tintOppType = tintOppType,@tintBillType = tintBillToType,@tintShipType = tintShipToType,@numDivisionID=numDivisionId
      FROM   OpportunityMaster WHERE  numOppId = @numOppIdTemp
      
      -- When Creating PO from SO and Bill type is Customer selected 
      SELECT @numCustomerDivID = ISNULL(numDivisionID,0) FROM dbo.OpportunityMaster WHERE 
			numOppID IN (SELECT  ISNULL(numParentOppID,0) FROM dbo.OpportunityLinking WHERE numChildOppID = @numOppIdTemp)
			
      --select @BizDcocName=vcBizDocID from OpportunityBizDocs where numOppBizDocsId=@numOppBizDocsIdTemp
      SELECT @BizDcocName = vcdata,
             @shipAmount = monShipCost,
             @numlistitemid = numlistitemid,
             @OppBizDocID = vcBizDocID,
             @bizdocOwner = OpportunityBizDocs.numCreatedBy,
			 @numBizDocID = ISNULL(OpportunityBizDocs.numBizDocId,0)
      FROM   listdetails JOIN OpportunityBizDocs ON numBizDocId = numlistitemid
      WHERE  numOppBizDocsId = @numOppBizDocsIdTemp

		IF @numBizDocID=287 AND ISNULL(@numARContactPosition,0) > 0 AND EXISTS (SELECT numContactId FROM AdditionalContactsInformation ADC WHERE ADC.numDivisionId=@numDivisionID AND ADC.vcPosition=@numARContactPosition) 
		BEGIN 
			SELECT TOP 1 @Contactid = numContactId FROM AdditionalContactsInformation ADC WHERE ADC.numDivisionId=@numDivisionID AND ADC.vcPosition=@numARContactPosition
		END
      
      SELECT @bizdocOwner AS BizDocOwner,
             @OppBizDocID AS BizDocID,
             @MonAmount AS Amount,
             isnull(@RecOwner,1) AS Owner,
             @BizDcocName AS BizDcocName,
             @numlistitemid AS BizDoc,
             @OppName AS OppName,
             VcCompanyName AS CompName,
             @shipAmount AS ShipAmount,
             dbo.fn_getOPPAddress(@numOppIdTemp,@numDomainIDTemp,1) AS BillAdd,
             dbo.fn_getOPPAddress(@numOppIdTemp,@numDomainIDTemp,2) AS ShipAdd,
             AD.VcStreet,
             AD.VcCity,
             isnull(dbo.fn_GetState(AD.numState),'') AS vcBilState,
             AD.vcPostalCode,
             isnull(dbo.fn_GetListItemName(AD.numCountry),
                    '') AS vcBillCountry,
             con.vcFirstName AS ConName,
             ISNULL(con.numPhone,'') numPhone,
             con.vcFax,
             isnull(con.vcEmail,'') AS vcEmail,
             div.numDivisionID,
             numContactid,
             isnull(vcFirstFldValue,'') vcFirstFldValue,
             isnull(vcSecndFldValue,'') vcSecndFldValue,
             isnull(bitTest,0) AS bitTest,
			dbo.fn_getOPPAddress(@numOppIdTemp,@numDomainIDTemp,1) AS BillToAddressName,
			dbo.fn_getOPPAddress(@numOppIdTemp,@numDomainIDTemp,1) AS ShipToAddressName,
             CASE 
               WHEN @tintBillType IS NULL THEN Com.vcCompanyName
             	-- When Sales order and bill to is set to customer 
               WHEN @tintBillType = 1 AND @tintOppType = 1 THEN Com.vcCompanyName
             -- When Create PO from SO and Bill to is set to Customer 
			   WHEN @tintBillType = 1 AND @tintOppType = 2 THEN dbo.fn_GetComapnyName(@numCustomerDivID)
               WHEN @tintBillType = 0 THEN (SELECT TOP 1 Com1.vcCompanyName
                                            FROM   companyinfo [Com1]
                                                   JOIN divisionmaster div1
                                                     ON com1.numCompanyID = div1.numCompanyID
                                                   JOIN Domain D1
                                                     ON D1.numDivisionID = div1.numDivisionID
                                                    JOIN dbo.AddressDetails AD1
														ON AD1.numDomainID = div1.numDomainID AND AD1.numRecordID = div1.numDivisionID AND tintAddressOf = 2 AND tintAddressType = 1 AND bitIsPrimary=1
                                            WHERE  D1.numDomainID = @numDomainIDTemp)
               WHEN @tintBillType = 2 THEN (SELECT vcBillCompanyName
                                            FROM   OpportunityAddress
                                            WHERE  numOppID = @numOppIdTemp)
				WHEN @tintBillType = 3 THEN  Com.vcCompanyName
              END AS BillToCompanyName,
             CASE 
               WHEN @tintShipType IS NULL THEN Com.vcCompanyName
               WHEN @tintShipType = 1 AND @tintOppType = 1 THEN Com.vcCompanyName
				-- When Create PO from SO and Ship to is set to Customer 
   			   WHEN @tintShipType = 1 AND @tintOppType = 2 THEN dbo.fn_GetComapnyName(@numCustomerDivID)
               WHEN @tintShipType = 0 THEN (SELECT TOP 1 Com1.vcCompanyName
                                            FROM   companyinfo [Com1]
                                                   JOIN divisionmaster div1
                                                     ON com1.numCompanyID = div1.numCompanyID
                                                   JOIN Domain D1
                                                     ON D1.numDivisionID = div1.numDivisionID
                                                    JOIN dbo.AddressDetails AD1
													 ON AD1.numDomainID = div1.numDomainID AND AD1.numRecordID = div1.numDivisionID AND tintAddressOf = 2 AND tintAddressType = 2 AND bitIsPrimary=1
                                            WHERE  D1.numDomainID = @numDomainIDTemp)
               WHEN @tintShipType = 2 THEN (SELECT vcShipCompanyName
                                            FROM   OpportunityAddress
                                            WHERE  numOppID = @numOppIdTemp)
				WHEN @tintShipType = 3 THEN (SELECT vcShipCompanyName
														FROM   OpportunityAddress
														WHERE  numOppID = @numOppIdTemp)
             END AS ShipToCompanyName
      FROM   companyinfo [Com] JOIN divisionmaster div ON com.numCompanyID = div.numCompanyID
             JOIN AdditionalContactsInformation con ON con.numdivisionId = div.numdivisionId
             JOIN Domain D ON D.numDomainID = div.numDomainID
             LEFT JOIN PaymentGatewayDTLID PGDTL ON PGDTL.intPaymentGateWay = D.intPaymentGateWay AND D.numDomainID = PGDTL.numDomainID
             LEFT JOIN AddressDetails AD --Billing add
				ON AD.numDomainID=DIV.numDomainID AND AD.numRecordID=div.numDivisionID AND AD.tintAddressOf = 2 AND AD.tintAddressType = 1 AND AD.bitIsPrimary=1
			 LEFT JOIN AddressDetails AD2--Shipping address
				ON AD2.numDomainID=DIV.numDomainID AND AD2.numRecordID=div.numDivisionID AND AD2.tintAddressOf = 2 AND AD2.tintAddressType = 2 AND AD2.bitIsPrimary=1	
		WHERE  numContactid = @Contactid AND Div.numDomainID = @numDomainIDTemp
    END
  IF @byteModeTemp = 2
    BEGIN
      SELECT opp.vcBizDocID,
             Mst.fltDiscount AS decDiscount,
             isnull(opp.monAmountPaid,0) AS monAmountPaid,
             isnull(opp.vcComments,'') AS vcComments,
             CONVERT(VARCHAR(20),DATEADD(minute,-@ClientTimeZoneOffsetTemp,opp.dtCreatedDate)) dtCreatedDate,
			 dbo.fn_GetContactName(Opp.numCreatedby)+ ' ' + CONVERT(VARCHAR(20),dateadd(MINUTE,-@ClientTimeZoneOffsetTemp,Opp.dtCreatedDate)) AS CreatedBy,
             dbo.fn_GetContactName(opp.numCreatedby) AS numCreatedby,
             dbo.fn_GetContactName(opp.numModifiedBy) AS numModifiedBy,
             CONVERT(VARCHAR(20),DATEADD(minute,-@ClientTimeZoneOffsetTemp,opp.dtModifiedDate)) dtModifiedDate,
			 dbo.fn_GetContactName(Opp.numModifiedBy) + ' ' + CONVERT(VARCHAR(20),dateadd(MINUTE,-@ClientTimeZoneOffsetTemp,Opp.dtModifiedDate)) AS ModifiedBy,
             CASE 
               WHEN isnull(opp.numViewedBy,0) = 0 THEN ''
               ELSE dbo.fn_GetContactName(opp.numViewedBy)
             END AS numViewedBy,
             CASE 
               WHEN opp.numViewedBy = 0 THEN NULL
               ELSE opp.dtViewedDate
             END AS dtViewedDate,
             (CASE WHEN isnull(Mst.intBillingDays,0) > 0 THEN CAST(1 AS BIT) ELSE isnull(Mst.bitBillingTerms,0) END) AS tintBillingTerms,
             isnull(Mst.intBillingDays,0) AS numBillingDays,
			 ISNULL(BTR.numNetDueInDays,0) AS numBillingDaysName,
			 BTR.vcTerms AS vcBillingTermsName,
             isnull(Mst.bitInterestType,0) AS tintInterestType,
             isnull(Mst.fltInterest,0) AS fltInterest,
             tintOPPType,
			 tintOppStatus,
             Opp.numBizDocId,
             dbo.fn_GetContactName(numApprovedBy) AS ApprovedBy,
             dtApprovedDate,
             Mst.bintAccountClosingDate,
             tintShipToType,
             tintBillToType,
             tintshipped,
             dtShippedDate bintShippedDate,
             isnull(opp.monShipCost,0) AS monShipCost,
             (SELECT COUNT(* )
              FROM   DocumentWorkflow
              WHERE  numDocID = @numOppBizDocsIdTemp
                     AND numContactID = @numUserCntIDTemp
                     AND cDocType = 'B'
                     AND tintApprove = 0) AS AppReq,
             (SELECT COUNT(* )
              FROM   DocumentWorkflow
              WHERE  numDocID = @numOppBizDocsIdTemp
                     AND cDocType = 'B'
                     AND tintApprove = 1) AS Approved,
             (SELECT COUNT(* )
              FROM   DocumentWorkflow
              WHERE  numDocID = @numOppBizDocsIdTemp
                     AND cDocType = 'B'
                     AND tintApprove = 2) AS Declined,
             (SELECT COUNT(* )
              FROM   DocumentWorkflow
              WHERE  numDocID = @numOppBizDocsIdTemp
                     AND cDocType = 'B'
                     AND tintApprove = 0) AS Pending,
             ISNULL(BT.vcBizDocFooter,D.vcBizDocFooter) AS BizdocFooter,
             ISNULL(BT.vcBizDocImagePath,D.vcBizDocImagePath) As vcBizDocImagePath, 
             Mst.numDivisionID,
             ISNULL(BT.vcPurBizDocFooter,D.vcPurBizDocFooter) AS PurBizdocFooter, 
             isnull(Mst.fltDiscount,0) AS fltDiscount,
             isnull(Mst.bitDiscountType,0) AS bitDiscountType,
             isnull(Opp.numShipVia,ISNULL(Mst.intUsedShippingCompany,0)) AS numShipVia,
--             isnull(vcTrackingURL,'') AS vcTrackingURL,
			ISNULL( (SELECT vcShipFieldValue FROM dbo.ShippingFieldValues WHERE numDomainID=MSt.numDomainID AND intShipFieldID IN ( SELECT intShipFieldID FROM dbo.ShippingFields WHERE numListItemID=Opp.numShipVia  AND vcFieldName='Tracking URL')),'')		AS vcTrackingURL,
             isnull(Opp.numBizDocStatus,0) AS numBizDocStatus,
             CASE 
               WHEN Opp.numBizDocStatus IS NULL THEN '-'
               ELSE dbo.fn_GetListItemName(Opp.numBizDocStatus)
             END AS BizDocStatus,
             CASE 
               WHEN ISNULL(Opp.numShipVia,0) = 0 THEN (CASE WHEN Mst.intUsedShippingCompany IS NULL THEN '-' WHEN Mst.intUsedShippingCompany = -1 THEN 'Will-call' ELSE dbo.fn_GetListItemName(Mst.intUsedShippingCompany) END)
			   WHEN Opp.numShipVia = -1 THEN 'Will-call'
               ELSE dbo.fn_GetListItemName(Opp.numShipVia)
             END AS ShipVia,
			 ISNULL((SELECT vcShipmentService FROM ShippingService WHERE (numDomainID=@numDomainIDTemp OR ISNULL(numDomainID,0)=0) AND numShippingServiceID = Mst.numShippingService),'') AS vcShippingService,
             ISNULL(C.varCurrSymbol,'') varCurrSymbol,
             (SELECT COUNT(*) FROM [ShippingReport] WHERE numOppBizDocId = opp.numOppBizDocsId )AS ShippingReportCount,
			 isnull(bitPartialFulfilment,0) bitPartialFulfilment,
			 ISNULL(Opp.vcBizDocName,'') vcBizDocName,
			 dbo.[fn_GetContactName](opp.[numModifiedBy])  + ', '
				+ CONVERT(VARCHAR(20),dateadd(MINUTE,-@ClientTimeZoneOffsetTemp,Opp.dtModifiedDate)) AS ModifiedBy,
			 dbo.[fn_GetContactName](Mst.[numRecOwner]) + ', ' +  dbo.[fn_GetContactName](Mst.[numAssignedTo]) AS OrderRecOwner,
			 dbo.[fn_GetContactName](DM.[numRecOwner]) + ', ' +  dbo.[fn_GetContactName](DM.[numAssignedTo]) AS AccountRecOwner,
			 DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,Opp.dtFromDate) AS dtFromDate,
			 Mst.tintTaxOperator,
			 dbo.GetAccountedEmbeddedCost(opp.numOppBizDocsID,1) AS monTotalEmbeddedCost,
			 dbo.GetAccountedEmbeddedCost(opp.numOppBizDocsID,2) AS monTotalAccountedCost,
			 ISNULL(BT.bitEnabled,0) bitEnabled,
			 ISNULL(BT.txtBizDocTemplate,'') txtBizDocTemplate, 
			 ISNULL(BT.txtCSS,'') txtCSS,
			 ISNULL(BT.numOrientation,0) numOrientation,
			 ISNULL(BT.bitKeepFooterBottom,0) bitKeepFooterBottom,
			 dbo.fn_GetContactName(Mst.numAssignedTo) AS AssigneeName,
			 ISNULL((SELECT vcEmail FROM dbo.AdditionalContactsInformation WHERE numContactID = Mst.numAssignedTo),'') AS AssigneeEmail,
			 ISNULL((SELECT numPhone FROM dbo.AdditionalContactsInformation WHERE numContactID = Mst.numAssignedTo),'') AS AssigneePhone,
			 dbo.fn_GetComapnyName(Mst.numDivisionId) OrganizationName,
			 dbo.fn_GetContactName(Mst.numContactID)  OrgContactName,

--CASE WHEN Mst.tintOppType = 1 THEN 
--dbo.getCompanyAddress((select ISNULL(DM.numDivisionID,0)  as numDivisionID from AdditionalContactsInformation AC INNER JOIN DivisionMaster DM 
--on AC.numDivisionID = DM.numDivisionID where numContactID = Mst.numCreatedBy),1,Mst.numDomainId)
--               ELSE dbo.getCompanyAddress(Mst.numDivisionId,1,Mst.numDomainId)
--             END AS CompanyBillingAddress1,

CASE WHEN Mst.tintOppType = 1 THEN 
dbo.getCompanyAddress((select ISNULL(AC.numDivisionID,0)  as numDivisionID from AdditionalContactsInformation AC
INNER JOIN Domain D ON D.numDomainID = AC.numDomainID where AC.numContactID = D.numAdminID AND D.numDomainID = Mst.numDomainId),1,Mst.numDomainId)
               ELSE dbo.getCompanyAddress(Mst.numDivisionId,1,Mst.numDomainId)
             END AS CompanyBillingAddress,

CASE WHEN Mst.tintOppType = 1 THEN 
dbo.getCompanyAddress((select ISNULL(AC.numDivisionID,0)  as numDivisionID from AdditionalContactsInformation AC
INNER JOIN Domain D ON D.numDomainID = AC.numDomainID where AC.numContactID = D.numAdminID AND D.numDomainID = Mst.numDomainId),1,Mst.numDomainId)
               ELSE dbo.getCompanyAddress(Mst.numDivisionId,2,Mst.numDomainId)
             END AS CompanyShippingAddress,
--
--			 dbo.getCompanyAddress(Mst.numDivisionId,1,Mst.numDomainId) CompanyBillingAddress,
--			 dbo.getCompanyAddress(Mst.numDivisionId,2,Mst.numDomainId) CompanyShippingAddress,
			 case when ACI.numPhone<>'' then ACI.numPhone +case when ACI.numPhoneExtension<>'' then ' - ' + ACI.numPhoneExtension else '' end  else '' END OrgContactPhone,
			 DM.vcComPhone as OrganizationPhone,
             ISNULL(ACI.vcEmail,'') AS OrgContactEmail,
 			 dbo.[fn_GetContactName](Mst.[numRecOwner]) AS OnlyOrderRecOwner,
 			 ISNULL(Opp.bitAuthoritativeBizDocs,0) bitAuthoritativeBizDocs,
			 ISNULL(Opp.tintDeferred,0) tintDeferred,isnull(Opp.monCreditAmount,0) AS monCreditAmount,
			 isnull(Opp.bitRentalBizDoc,0) as bitRentalBizDoc,isnull(BT.numBizDocTempID,0) as numBizDocTempID,isnull(BT.vcTemplateName,'') as vcTemplateName,
		     [dbo].[GetDealAmount](opp.numOppId ,getutcdate(),opp.numOppBizDocsId ) as monPAmount,
			 --(select top 1 SD.vcSignatureFile from SignatureDetail SD join OpportunityBizDocsDetails OBD on SD.numSignID=OBD.numSignID where SD.numDomainID = @numDomainIDTemp and OBD.numDomainID = @numDomainIDTemp and OBD.numSignID IS not null and OBD.numBizDocsId=opp.numOppBizDocsId order by OBD.numBizDocsPaymentDetId desc) as vcSignatureFile,
			 isnull(CMP.txtComments,'') as vcOrganizationComments,
			 isnull(Opp.vcTrackingNo,'') AS  vcTrackingNo
			 ,'' AS  vcShippingMethod
			 ,Opp.dtDeliveryDate,
			 CASE WHEN Opp.vcRefOrderNo IS NULL OR LEN(Opp.vcRefOrderNo)=0 THEN ISNULL(Mst.vcOppRefOrderNo,'') ELSE 
			  isnull(Opp.vcRefOrderNo,'') END AS vcRefOrderNo,ISNULL(Mst.numDiscountAcntType,0) AS numDiscountAcntType,
			 ISNULL(opp.numSequenceId,'') AS numSequenceId,
			 CASE WHEN ISNULL(Opp.numBizDocId,0)=296 THEN ISNULL((SELECT SUBSTRING((SELECT '$^$' + dbo.fn_GetListItemName(numBizDocStatus) +'#^#'+ dbo.fn_GetContactName(numUserCntID) +'#^#'+ dbo.FormatedDateTimeFromDate(DateAdd(minute, -@ClientTimeZoneOffsetTemp , dtCreatedDate),@numDomainIDTemp) + '#^#'+ isnull(DFCS.vcColorScheme,'NoForeColor')
					FROM OppFulfillmentBizDocsStatusHistory OFBS left join DycFieldColorScheme DFCS on OFBS.numBizDocStatus=DFCS.vcFieldValue AND DFCS.numDomainID=@numDomainIDTemp
					WHERE numOppId=opp.numOppId and numOppBizDocsId=opp.numOppBizDocsId FOR XML PATH('')),4,200000)),'') ELSE '' END AS vcBizDocsStatusList,ISNULL(Mst.numCurrencyID,0) AS numCurrencyID,
			ISNULL(Opp.fltExchangeRateBizDoc,Mst.fltExchangeRate) AS fltExchangeRateBizDoc,
			ISNULL(TBLEmployer.vcCompanyName,'') AS EmployerOrganizationName,ISNULL(TBLEmployer.vcComPhone,'') as EmployerOrganizationPhone,ISNULL(opp.bitAutoCreated,0) AS bitAutoCreated,
			ISNULL(RecurrenceConfiguration.numRecConfigID,0) AS numRecConfigID,
			(CASE WHEN RecurrenceConfiguration.numRecConfigID IS NULL THEN 0 ELSE 1 END) AS bitRecur,
			(CASE WHEN RecurrenceTransaction.numRecTranID IS NULL THEN 0 ELSE 1 END) AS bitRecurred,
			ISNULL(Mst.bitRecurred,0) AS bitOrderRecurred,
			dbo.FormatedDateFromDate(RecurrenceConfiguration.dtStartDate,@numDomainIDTemp) AS dtStartDate,
			RecurrenceConfiguration.vcFrequency AS vcFrequency,
			ISNULL(RecurrenceConfiguration.bitDisabled,0) AS bitDisabled,
			ISNULL(opp.bitFulfilled,0) AS bitFulfilled,
			ISNULL(opp.numDeferredBizDocID,0) AS numDeferredBizDocID,
			dbo.GetTotalQtyByUOM(opp.numOppBizDocsId) AS vcTotalQtybyUOM,
			ISNULL(Mst.numShippingService,0) AS numShippingService,
			dbo.FormatedDateFromDate(Mst.dtReleaseDate,@numDomainIDTemp) AS vcReleaseDate,
			dbo.FormatedDateFromDate(Mst.intpEstimatedCloseDate,@numDomainIDTemp) AS vcRequiredDate,
			(CASE WHEN ISNULL(Mst.numPartner,0) > 0 THEN ISNULL((SELECT CONCAT(DivisionMaster.vcPartnerCode,'-',CompanyInfo.vcCompanyName) FROM DivisionMaster INNER JOIN CompanyInfo ON DivisionMaster.numCompanyID=CompanyInfo.numCompanyId WHERE DivisionMaster.numDivisionID=Mst.numPartner),'') ELSE '' END) AS vcPartner,
			dbo.FormatedDateFromDate(mst.dtReleaseDate,@numDomainIDTemp) AS dtReleaseDate,
			(select TOP 1 vcBizDocID from OpportunityBizDocs where numOppId=opp.numOppId and numBizDocId=296) as vcFulFillment,
			(select TOP 1 vcOppRefOrderNo from OpportunityMaster WHERE numOppId=opp.numOppId) AS vcPOName,
			ISNULL(vcCustomerPO#,'') AS vcCustomerPO#,
			ISNULL(Mst.txtComments,'') vcSOComments,
			ISNULL((SELECT DMSA.vcAccountNumber FROm [dbo].[DivisionMasterShippingAccount] DMSA WHERE DMSA.numDivisionID=DM.numDivisionID AND DMSA.numShipViaID=opp.numShipVia),'') AS vcShippersAccountNo,
			CONVERT(VARCHAR(20),DATEADD(minute,-@ClientTimeZoneOffsetTemp,Mst.bintCreatedDate)) OrderCreatedDate,
			(CASE WHEN ISNULL(opp.numSourceBizDocId,0) > 0 THEN ISNULL((SELECT vcVendorInvoice FROM OpportunityBizDocs WHERE OpportunityBizDocs.numOppBizDocsId=opp.numSourceBizDocId),'') ELSE ISNULL(vcVendorInvoice,'') END) vcVendorInvoice,
			(CASE 
				WHEN ISNULL(opp.numSourceBizDocId,0) > 0
				THEN ISNULL((SELECT vcBizDocID FROM OpportunityBizDocs WHERE OpportunityBizDocs.numOppBizDocsId=Opp.numSourceBizDocId),'')
				ELSE ''
			END) vcPackingSlip
			,ISNULL(numARAccountID,0) numARAccountID
			,(CASE WHEN (SELECT COUNT(*) FROM OpportunityBizDocs OBInner WHERE OBInner.numOppID=@numOppID AND OBInner.numBizDocId=296 AND OBInner.numSourceBizDocId=opp.numOppBizDocsId AND opp.numBizDocId=29397) > 0 THEN 1 ELSE 0 END) AS IsFulfillmentGeneratedOnPickList
			,ISNULL((SELECT COUNT(*) FROM OpportunityBizDocs WHERE numOppID=@numOppID AND ISNULL(numDeferredBizDocID,0) = ISNULL(opp.numOppBizDocsId,0)),0) AS intInvoiceForDiferredBizDoc
			,ISNULL(dbo.CheckOrderInventoryStatus(Mst.numOppID,Mst.numDomainID,2),'') vcInventoryStatus,
			CASE WHEN ISNULL(DM.intDropShip,0)=1 THEN 'Not Available'
				 WHEN ISNULL(DM.intDropShip,0)=2 THEN 'Blank Available'
				 WHEN ISNULL(DM.intDropShip,0)=3 THEN 'Vendor Label'
				 WHEN ISNULL(DM.intDropShip,0)=4 THEN 'Private Label'
				 ELSE '-'
			END AS vcDropShip
			,ISNULL(D.numShippingServiceItemID,0) numShippingServiceItemID
			,ISNULL(D.numDiscountServiceItemID,0) numDiscountServiceItemID 
			,CASE WHEN ISNULL(D.bitInventoryInvoicing,0)=1 AND ISNULL(opp.numSourceBizDocId,0)=0 THEN
			ISNULL((SELECT TOP 1 vcBizDocID FROM OpportunityBizDocs WHERE numSourceBizDocId=opp.numOppBizDocsId),'NA') WHEN  ISNULL(D.bitInventoryInvoicing,0)=1 THEN 
			ISNULL((SELECT TOP 1 vcBizDocID FROM OpportunityBizDocs WHERE numOppBizDOcsId=opp.numSourceBizDocId),'NA') ELSE '' END AS vcSourceBizDocID
		FROM   OpportunityBizDocs opp JOIN OpportunityMaster Mst ON Mst.numOppId = opp.numOppId
             JOIN Domain D ON D.numDomainID = Mst.numDomainID
             LEFT OUTER JOIN [Currency] C ON C.numCurrencyID = Mst.numCurrencyID
             LEFT JOIN [DivisionMaster] DM ON DM.numDivisionID = Mst.numDivisionID   
			 LEFT JOIN CompanyInfo CMP ON DM.numCompanyId = CMP.numCompanyId and CMP.numDomainID = @numDomainIDTemp  
             LEFT JOIN dbo.AdditionalContactsInformation ACI ON ACI.numContactID = Mst.numContactID
             LEFT JOIN [BizDocTemplate] BT ON BT.numDomainID = @numDomainIDTemp AND BT.numBizDocTempID=opp.numBizDocTempID
             LEFT JOIN BillingTerms BTR ON BTR.numTermsID = ISNULL(Mst.intBillingDays,0)
             JOIN divisionmaster div1 ON Mst.numDivisionID = div1.numDivisionID
			 JOIN companyinfo [Com1] ON com1.numCompanyID = div1.numCompanyID
			 LEFT JOIN RecurrenceConfiguration ON Opp.numOppBizDocsId = RecurrenceConfiguration.numOppBizDocID
			 LEFT JOIN RecurrenceTransaction ON Opp.numOppBizDocsId = RecurrenceTransaction.numRecurrOppBizDocID
			 OUTER APPLY
			 (
				SELECT TOP 1 
					Com1.vcCompanyName, div1.vcComPhone
                FROM   companyinfo [Com1]
                        JOIN divisionmaster div1
                            ON com1.numCompanyID = div1.numCompanyID
                        JOIN Domain D1
                            ON D1.numDivisionID = div1.numDivisionID
                        JOIN dbo.AddressDetails AD1
							ON AD1.numDomainID = div1.numDomainID AND AD1.numRecordID = div1.numDivisionID AND tintAddressOf = 2 AND tintAddressType = 1 AND bitIsPrimary=1
                WHERE  D1.numDomainID = @numDomainIDTemp
			 ) AS TBLEmployer
      WHERE  opp.numOppBizDocsId = @numOppBizDocsIdTemp
             AND Mst.numDomainID = @numDomainIDTemp
    END
END
GO
/****** Object:  StoredProcedure [dbo].[USP_OPPGetINItemsForAuthorizativeAccounting]    Script Date: 07/26/2008 16:20:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by Siva                                                                                                            
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_oppgetinitemsforauthorizativeaccounting')
DROP PROCEDURE usp_oppgetinitemsforauthorizativeaccounting
GO
CREATE PROCEDURE  [dbo].[USP_OPPGetINItemsForAuthorizativeAccounting]                                                                                                                                                
(                                                                                                                                                
	@numOppId AS NUMERIC(18,0)=null,                                                                                                                                                
	@numOppBizDocsId AS NUMERIC(18,0)=null,                                                                
	@numDomainID AS NUMERIC(18,0)=0 ,                                                      
	@numUserCntID AS NUMERIC(18,0)=0                                                                                                                                               
)                                                                                                                                                                                                                                                                                         
AS 
BEGIN
	DECLARE @numCurrencyID AS NUMERIC(18,0)
	DECLARE @fltExchangeRate AS FLOAT

	DECLARE @fltCurrentExchangeRate AS FLOAT
	DECLARE @tintType AS TINYINT
	DECLARE @vcPOppName AS VARCHAR(100)
	DECLARE @bitPPVariance AS BIT
	DECLARE @numDivisionID AS NUMERIC(18,0)

	SELECT 
		@numCurrencyID=numCurrencyID
		,@fltExchangeRate=fltExchangeRate
		,@tintType=tintOppType
		,@vcPOppName=vcPOppName
		,@bitPPVariance=ISNULL(bitPPVariance,0)
		,@numDivisionID=numDivisionID 
	FROM 
		OpportunityMaster
	WHERE 
		numOppID=@numOppId 

	DECLARE @vcBaseCurrency AS VARCHAR(100)=''
	DECLARE @bitAutolinkUnappliedPayment AS BIT 

	SELECT 
		@vcBaseCurrency=ISNULL(C.varCurrSymbol,'')
		,@bitAutolinkUnappliedPayment=ISNULL(bitAutolinkUnappliedPayment,0) 
	FROM 
		dbo.Domain D 
	LEFT JOIN 
		Currency C 
	ON 
		D.numCurrencyID=C.numCurrencyID 
	WHERE 
		D.numDomainID=@numDomainID

	DECLARE @vcForeignCurrency AS VARCHAR(100) = ''
	SELECT 
		@vcForeignCurrency=ISNULL(C.varCurrSymbol,'') 
	FROM  
		Currency C 
	WHERE 
		numCurrencyID=@numCurrencyID

	DECLARE @fltExchangeRateBizDoc AS FLOAT 
	DECLARE @numBizDocId AS NUMERIC(18,0)                                                           
	DECLARE @vcBizDocID AS VARCHAR(100)

	SELECT 
		@numBizDocId=numBizDocId
		,@fltExchangeRateBizDoc=fltExchangeRateBizDoc
		,@vcBizDocID=vcBizDocID 
	FROM 
		OpportunityBizDocs 
	WHERE 
		numOppBizDocsId=@numOppBizDocsId                                                         

	IF @numCurrencyID > 0 
		SET @fltCurrentExchangeRate=dbo.GetExchangeRate(@numDomainID,@numCurrencyID)
	ELSE 
		SET @fltCurrentExchangeRate=@fltExchangeRate                                                                                                                                          

	IF @numBizDocId = 296 --FULFILLMENT BIZDOC
	BEGIN
		--STORE AVERAGE COST FOR SHIPPED ITEM SO THAT WE CAN CALCULATE INVENTORY IMAPCT IN ACCOUNTING AND ALSO USE IT WHEN IETM RETURN
		UPDATE
			OBDI
		SET
			OBDI.monAverageCost= (CASE 
										WHEN ISNULL(I.bitKitParent,0) = 1 
										THEN ISNULL((SELECT SUM(ISNULL(OKI.numQtyItemsReq_Orig,0) * ISNULL(IInner.monAverageCost,0)) FROM OpportunityKitItems OKI INNER JOIN Item IInner ON OKI.numChildItemID=IInner.numItemCode WHERE OKI.numOppId=OI.numOppId AND OKI.numOppItemID=OI.numoppitemtCode),0) + 
												ISNULL((SELECT SUM(ISNULL(OKCI.numQtyItemsReq_Orig,0) * ISNULL(OKI.numQtyItemsReq_Orig,0) * ISNULL(IInner.monAverageCost,0)) FROM OpportunityKitChildItems OKCI INNER JOIN OpportunityKitItems OKI ON OKCI.numOppChildItemID=OKI.numOppChildItemID INNER JOIN Item IInner ON OKCI.numItemID=IInner.numItemCode WHERE OKI.numOppId=OI.numOppId AND OKI.numOppItemID=OI.numoppitemtCode),0)
										ELSE ISNULL(I.monAverageCost,0) 
									END)
		FROM
			OpportunityBizDocItems OBDI
		INNER JOIN
			OpportunityItems OI
		ON
			OBDI.numOppItemID=OI.numoppitemtCode
		INNER JOIN
			Item I
		ON
			OI.numItemCode = I.numItemCode
		WHERE
			OBDI.numOppBizDocID=@numOppBizDocsId
			AND OBDI.monAverageCost IS NULL
	END
                                                                                                                     
	SELECT 
		I.[vcItemName] as Item
		,charitemType as type
		,OBI.vcitemdesc as [desc]
		,OBI.numUnitHour as Unit
		,ISNULL(OBI.monPrice,0) as Price
		,ISNULL(OBI.monTotAmount,0) as Amount
		,ISNULL((opp.monTotAmount/opp.numUnitHour)*OBI.numUnitHour,0) AS  ItemTotalAmount
		,isnull(monListPrice,0) as listPrice
		,convert(varchar,i.numItemCode) as ItemCode
		,numoppitemtCode
		,L.vcdata as vcItemClassification
		,case when bitTaxable=0 then 'No' else 'Yes' end as Taxable
		,isnull(i.numIncomeChartAcntId,0) as itemIncomeAccount
		,isnull(i.numAssetChartAcntId,0) as itemInventoryAsset,'NI' as ItemType        
		,isnull(i.numCOGsChartAcntId,0) as itemCoGs
		,isnull(i.bitExpenseItem,0) as bitExpenseItem
		,isnull(i.numExpenseChartAcntId,0) as itemExpenseAccount
		,(CASE WHEN ISNULL(i.bitVirtualInventory,0) = 1 THEN 0 ELSE isnull(Opp.monAvgCost,0) END) as AverageCost
		,isnull(OBI.bitDropShip,0) as bitDropShip
		,(OBI.monTotAmtBefDiscount-OBI.monTotAmount) as DiscAmt
		,NULLIF(Opp.numProjectID,0) numProjectID
		,NULLIF(Opp.numClassID,0) numClassID
		,ISNULL(i.bitKitParent,0) AS bitKitParent
		,ISNULL(i.bitAssembly,0) AS bitAssembly
		,(CASE WHEN ISNULL(i.bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(OBI.monAverageCost,0) END) AS ShippedAverageCost
	FROM 
		OpportunityItems opp
	INNER JOIN 
		OpportunityBizDocItems OBI
	ON 
		OBI.numOppItemID=Opp.numoppitemtCode       
	LEFT JOIN 
		Item I 
	ON 
		opp.numItemCode=I.numItemCode        
	LEFT JOIN 
		ListDetails L 
	ON 
		I.numItemClassification=L.numListItemID        
	WHERE 
		Opp.numOppId=@numOppId 
		AND OBI.numOppBizDocID=@numOppBizDocsId
                                                               
	SELECT 
		ISNULL(@numCurrencyID,0) as numCurrencyID
		,ISNULL(@fltExchangeRate,1) as fltExchangeRate
		,ISNULL(@fltCurrentExchangeRate,1) as CurrfltExchangeRate
		,ISNULL(@fltExchangeRateBizDoc,1) AS fltExchangeRateBizDoc
		,@vcBaseCurrency AS vcBaseCurrency
		,@vcForeignCurrency AS vcForeignCurrency
		,ISNULL(@vcPOppName,'') AS vcPOppName
		,ISNULL(@vcBizDocID,'') AS vcBizDocID
		,ISNULL(@bitPPVariance,0) AS bitPPVariance
		,@numDivisionID AS numDivisionID
		,ISNULL(@bitAutolinkUnappliedPayment,0) AS bitAutolinkUnappliedPayment                                            
END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityMaster_CloneOrderLineItem')
DROP PROCEDURE dbo.USP_OpportunityMaster_CloneOrderLineItem
GO
CREATE PROCEDURE [dbo].[USP_OpportunityMaster_CloneOrderLineItem]
(
	@numDomainID NUMERIC(18,0),
	@numUserCntID NUMERIC(18,0),
    @numOppID NUMERIC(18,0),
    @numOppItemID NUMERIC(18,0)
)
AS
BEGIN
	IF EXISTS(SELECT numOppId FROM OpportunityMaster WHERE numOppId=@numOppID AND ISNULL(tintshipped,0)=1)
	BEGIN
		RAISERROR('CLOSED_ORDER',16,1)
	END
	ELSE
	BEGIN
		BEGIN TRY
		BEGIN TRANSACTION
			DECLARE @tintCommitAllocation TINYINT
			DECLARE @tintOppType TINYINT
			DECLARE @tintOppStatus TINYINT
			DECLARE @numDivisionId NUMERIC(18,0)
			DECLARE @bitAllocateInventoryOnPickList BIT

			SELECT @tintCommitAllocation=ISNULL(tintCommitAllocation,1) FROM Domain WHERE numDomainId=@numDomainId
			SELECT @numDivisionId=numDivisionID, @tintOppType=tintOppType,@tintOppStatus=ISNULL(tintOppStatus,0) FROM OpportunityMaster WHERE numOppId=@numOppID

			SELECT 
				@bitAllocateInventoryOnPickList=ISNULL(bitAllocateInventoryOnPickList,0)
			FROM 
				DivisionMaster D
			JOIN 
				CompanyInfo C 
			ON 
				C.numCompanyId=D.numCompanyID
			WHERE 
				D.numDivisionID = @numDivisionId

			--REVERTING BACK THE WAREHOUSE ITEMS                  
			IF @tintOppStatus = 1 AND @tintCommitAllocation=1
			BEGIN               
				EXEC USP_RevertDetailsOpp @numOppID,0,0,@numUserCntID                  
			END       

			DECLARE @vcOppItemsColumns VARCHAR(MAX) = STUFF((SELECT ',' + COLUMN_NAME
															FROM INFORMATION_SCHEMA.COLUMNS
															WHERE TABLE_NAME = N'OpportunityItems' AND COLUMN_NAME NOT IN ('numoppitemtCode','bitMappingRequired','bitRequiredWarehouseCorrection','numQtyReceived','numQtyPicked','numQtyShipped','numUnitHourReceived')
															FOR XML PATH('')),1,1,'')

			DECLARE @vcOppKitItemsColumns VARCHAR(MAX) = STUFF((SELECT ',' + COLUMN_NAME
															FROM INFORMATION_SCHEMA.COLUMNS
															WHERE TABLE_NAME = N'OpportunityKitItems' AND COLUMN_NAME NOT IN ('numOppChildItemID','numOppItemID','numQtyShipped')
															FOR XML PATH('')),1,1,'')

			DECLARE @vcOppKitChildItemsColumns VARCHAR(MAX) = STUFF((SELECT ',' + COLUMN_NAME
																	FROM INFORMATION_SCHEMA.COLUMNS
																	WHERE TABLE_NAME = N'OpportunityKitChildItems' AND COLUMN_NAME NOT IN ('numOppKitChildItemID','numOppItemID','numOppChildItemID','numQtyShipped')
																	FOR XML PATH('')),1,1,'')

			DECLARE @numNewOppItemID NUMERIC(18,0)
			DECLARE @numNewOppChildItemID NUMERIC(18,0)
			DECLARE @numNewOppKitChildItemID NUMERIC(18,0)
			DECLARE @sqlText AS NVARCHAR(MAX) = CONCAT('INSERT INTO OpportunityItems (',@vcOppItemsColumns,') SELECT ',@vcOppItemsColumns,' FROM OpportunityItems WHERE numOppID=',@numOppID,' AND numoppitemtCode=',@numOppItemID,'; SELECT @numNewOppItemID = SCOPE_IDENTITY()')

			EXEC sp_executesql @sqlText, N'@numNewOppItemID NUMERIC(18,0) OUTPUT', @numNewOppItemID OUTPUT

			DECLARE @TEMP TABLE
			(
				ID INT IDENTITY(1,1)
				,numOppChildItemID NUMERIC(18,0)
			)

			INSERT INTO @TEMP
			(
				numOppChildItemID
			)
			SELECT 
				numOppChildItemID 
			FROM 
				OpportunityKitItems 
			WHERE 
				numOppId=@numOppID 
				AND numOppItemID=@numOppItemID

			DECLARE @i AS INT = 1
			DECLARE @iCount AS INT
			DECLARE @numTempOppChildItemID NUMERIC(18,0)
			SELECT @iCount=COUNT(*) FROM @TEMP

			WHILE @i <= @iCount
			BEGIN
				SELECT @numTempOppChildItemID=numOppChildItemID FROM @TEMP WHERE ID=@i

				SET @sqlText = CONCAT('INSERT INTO OpportunityKitItems (',@vcOppKitItemsColumns,',numOppItemID) SELECT ',@vcOppKitItemsColumns,',',@numNewOppItemID,' FROM OpportunityKitItems WHERE numOppID=',@numOppID,' AND numOppItemID=',@numOppItemID,' AND numOppChildItemID=',@numTempOppChildItemID,'; SELECT @numNewOppChildItemID = SCOPE_IDENTITY()')
				EXEC sp_executesql @sqlText, N'@numNewOppChildItemID NUMERIC(18,0) OUTPUT',@numNewOppChildItemID OUTPUT

				SET @sqlText = CONCAT('INSERT INTO OpportunityKitChildItems (',@vcOppKitChildItemsColumns,',numOppItemID,numOppChildItemID) SELECT ',@vcOppKitChildItemsColumns,',',@numNewOppItemID,',',@numNewOppChildItemID,' FROM OpportunityKitChildItems WHERE numOppID=',@numOppID,' AND numOppItemID=',@numOppItemID,' AND numOppChildItemID=',@numTempOppChildItemID,';')
				EXEC sp_executesql @sqlText

				SET @i = @i + 1
			END

			IF NOT (@tintCommitAllocation=2 AND @bitAllocateInventoryOnPickList=1)
			BEGIN
				EXEC USP_ManageSOWorkOrder @numDomainId,@numOppID,@numUserCntID
			END

			IF @tintOppStatus=1 AND @tintCommitAllocation=1             
			BEGIN         
				EXEC USP_UpdatingInventoryonCloseDeal @numOppID,0,@numUserCntID
			END  

			IF @tintOppStatus = 1
			BEGIN
				IF (SELECT COUNT(*) FROM OpportunityItems WHERE numOppId=@numOppID AND ISNULL(numWarehouseItmsID,0) > 0 AND ISNULL(bitDropShip,0)=0) <>
					(SELECT COUNT(*) FROM OpportunityItems INNER JOIN WareHouseItems ON OpportunityItems.numWarehouseItmsID=WareHouseItems.numWareHouseItemID AND OpportunityItems.numItemCode=WareHouseItems.numItemID WHERE numOppId=@numOppID AND OpportunityItems.numWarehouseItmsID > 0 AND ISNULL(bitDropShip,0)=0)
				BEGIN
					RAISERROR('INVALID_ITEM_WAREHOUSES',16,1)
					RETURN
				END
			END

			IF(SELECT 
					COUNT(*) 
				FROM 
					OpportunityBizDocItems OBDI 
				INNER JOIN 
					OpportunityBizDocs OBD 
				ON 
					OBDI.numOppBizDocID=OBD.numOppBizDocsID 
				WHERE 
					OBD.numOppId=@numOppID AND OBDI.numOppItemID NOT IN (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId=@numOppID)) > 0
			BEGIN
				RAISERROR('ITEM_USED_IN_BIZDOC',16,1)
				RETURN
			END

			IF(SELECT 
					COUNT(*)
				FROM
					OpportunityItems OI
				INNER JOIN
				(
					SELECT
						numOppItemID
						,SUM(numUnitHour) numInvoiceBillQty
					FROM 
						OpportunityBizDocItems OBDI 
					INNER JOIN 
						OpportunityBizDocs OBD 
					ON 
						OBDI.numOppBizDocID=OBD.numOppBizDocsID 
					WHERE 
						OBD.numOppId=@numOppID
						AND ISNULL(OBD.bitAuthoritativeBizDocs,0) = 1
					GROUP BY
						numOppItemID
				) AS TEMP
				ON
					TEMP.numOppItemID = OI.numoppitemtCode
				WHERE
					OI.numOppId = @numOppID
					AND OI.numUnitHour < Temp.numInvoiceBillQty
				) > 0
			BEGIN
				RAISERROR('ORDERED_QUANTITY_CAN_NOT_BE_LESS_THEN_INVOICE/BILL_QTY',16,1)
				RETURN
			END

			IF @tintOppType=1 AND @tintCommitAllocation=2 AND @bitAllocateInventoryOnPickList=1
			BEGIN
				IF(SELECT 
						COUNT(*)
					FROM
					(
						SELECT
							numOppItemID
							,SUM(numUnitHour) PackingSlipUnits
						FROM 
							OpportunityBizDocItems OBDI 
						INNER JOIN 
							OpportunityBizDocs OBD 
						ON 
							OBDI.numOppBizDocID=OBD.numOppBizDocsID 
						WHERE 
							OBD.numOppId=@numOppID
							AND OBD.numBizDocId=29397 --Picking Slip
						GROUP BY
							numOppItemID
					) AS TEMP
					INNER JOIN
						OpportunityItems OI
					ON
						TEMP.numOppItemID = OI.numoppitemtCode
					WHERE
						OI.numUnitHour < PackingSlipUnits
					) > 0
				BEGIN
					RAISERROR('ORDERED_QUANTITY_CAN_NOT_BE_LESS_THEN_PACKING_SLIP_QTY',16,1)
					RETURN
				END
			END

			IF EXISTS (SELECT 
					numoppitemtCode 
				FROM 
					OpportunityItems 
				INNER JOIN 
					Item 
				ON 
					OpportunityItems.numItemCode=Item.numItemCode 
				WHERE 
					numOppId=@numOppID 
					AND charItemType='P' 
					AND ISNULL(numWarehouseItmsID,0) = 0 
					AND ISNULL(bitDropShip,0) = 0)
			BEGIN
				RAISERROR('WAREHOUSE_REQUIRED',16,1)
			END

			IF (SELECT COUNT(*) FROM WorkOrder WHERE numDomainID=@numDomainID AND numOppId=@numOppId AND ISNULL(numOppItemID,0) > 0 AND numOppItemID NOT IN (SELECT OIInner.numoppitemtCode FROM OpportunityItems OIInner WHERE OIInner.numOppId=@numOppID)) > 0
			BEGIN
				RAISERROR ( 'WORK_ORDER_EXISTS', 16, 1 ) ;
			END

			IF (SELECT 
					COUNT(*) 
				FROM 
					WorkOrder 
				INNER JOIN 
					OpportunityItems 
				ON 
					OpportunityItems.numOppId=@numOppID
					AND OpportunityItems.numoppitemtCode=WorkOrder.numOppItemID
				WHERE 
					WorkOrder.numDomainID=@numDomainID 
					AND WorkOrder.numOppId=@numOppID
					AND WorkOrder.numWareHouseItemId <> OpportunityItems.numWarehouseItmsID) > 0
			BEGIN
				RAISERROR ('CAN_NOT_CHANGE_ASSEMBLY_WAREHOUSE_WORK_ORDER_EXISTS', 16, 1 ) ;
			END

			--Delete Tax for Opportunity Items if item deleted 
			DELETE FROM OpportunityItemsTaxItems WHERE numOppId=@numOppId AND numOppItemID NOT IN (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId=@numOppID)

			--Insert Tax for Opportunity Items
			INSERT INTO dbo.OpportunityItemsTaxItems 
			(
				numOppId,
				numOppItemID,
				numTaxItemID,
				numTaxID
			) 
			SELECT 
				@numOppId,
				OI.numoppitemtCode,
				TI.numTaxItemID,
				0
			FROM 
				dbo.OpportunityItems OI 
			JOIN 
				dbo.ItemTax IT 
			ON 
				OI.numItemCode=IT.numItemCode 
			JOIN
				TaxItems TI 
			ON 
				TI.numTaxItemID = IT.numTaxItemID 
			WHERE 
				OI.numOppId=@numOppID 
				AND IT.bitApplicable=1 
				AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)
			UNION
			SELECT 
				@numOppId,
				OI.numoppitemtCode,
				0,
				0
			FROM 
				dbo.OpportunityItems OI 
			JOIN 
				dbo.Item I 
			ON 
				OI.numItemCode=I.numItemCode
			WHERE 
				OI.numOppId=@numOppID 
				AND I.bitTaxable=1 
				AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)
			UNION
			SELECT
				@numOppId,
				OI.numoppitemtCode,
				1,
				TD.numTaxID
			FROM
				dbo.OpportunityItems OI 
			INNER JOIN
				ItemTax IT
			ON
				IT.numItemCode = OI.numItemCode
			INNER JOIN
				TaxDetails TD
			ON
				TD.numTaxID = IT.numTaxID
				AND TD.numDomainId = @numDomainId
			WHERE
				OI.numOppId = @numOppID
				AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)

			UPDATE 
				OI
			SET
				OI.vcInclusionDetail = [dbo].[GetOrderAssemblyKitInclusionDetails](OI.numOppID,OI.numoppitemtCode,OI.numUnitHour,1,1)
			FROM 
				OpportunityItems OI
			INNER JOIN
				Item I
			ON
				OI.numItemCode = I.numItemCode
			WHERE
				numOppId=@numOppID
				AND ISNULL(I.bitKitParent,0) = 1
  
			UPDATE OpportunityMaster SET monDealAmount=dbo.GetDealAmount(@numOppID,getdate(),0),numModifiedBy=@numUserCntID,bintModifiedDate=GETUTCDATE() WHERE numOppId=@numOppID
		COMMIT
		END TRY
		BEGIN CATCH
			DECLARE @ErrorMessage NVARCHAR(4000)
			DECLARE @ErrorNumber INT
			DECLARE @ErrorSeverity INT
			DECLARE @ErrorState INT
			DECLARE @ErrorLine INT
			DECLARE @ErrorProcedure NVARCHAR(200)

			IF @@TRANCOUNT > 0
				ROLLBACK TRANSACTION;

			SELECT 
				@ErrorMessage = ERROR_MESSAGE(),
				@ErrorNumber = ERROR_NUMBER(),
				@ErrorSeverity = ERROR_SEVERITY(),
				@ErrorState = ERROR_STATE(),
				@ErrorLine = ERROR_LINE(),
				@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

			RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
		END CATCH
	END

END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_PageElementDetail_GetHtml')
DROP PROCEDURE dbo.USP_PageElementDetail_GetHtml
GO
CREATE PROCEDURE [dbo].[USP_PageElementDetail_GetHtml]
(
	@numDomainID NUMERIC(18,0)
	,@numSiteID NUMERIC(18,0)
	,@numElementID NUMERIC(18,0)
)
AS 
BEGIN
	SELECT
		ISNULL(PageElementDetail.vcHtml,'') vcHtml
	FROM
		PageElementDetail
	INNER JOIN
		PageElementAttributes
	ON
		PageElementDetail.numAttributeID = PageElementAttributes.numAttributeID
	WHERE
		PageElementDetail.numDomainID = @numDomainID
		AND PageElementDetail.numSiteID = @numSiteID
		AND PageElementDetail.numElementID = @numElementID
		AND PageElementAttributes.vcAttributeName='Html Customize'
END
GO
/****** Object:  StoredProcedure [dbo].[USP_Projects]    Script Date: 07/26/2008 16:20:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_projects')
DROP PROCEDURE usp_projects
GO
CREATE PROCEDURE [dbo].[USP_Projects]                          
(                          
@numProId numeric(9)=null ,  
@numDomainID as numeric(9),  
@ClientTimeZoneOffset as int         
                         
)                          
as                          
                          
begin                          
                                          
 select  pro.numProId,                          
  pro.vcProjectName,                         
  pro.numintPrjMgr,                          
  pro.numOppId,                    
  pro.intDueDate,                          
  pro.numCustPrjMgr,                          
  pro.numDivisionId,                          
  pro.txtComments,      
  Div.tintCRMType,                          
  div.vcDivisionName,                          
  com.vcCompanyName,
  A.vcFirstname + ' ' + A.vcLastName AS vcContactName,
  isnull(A.vcEmail,'') AS vcEmail,
         isnull(A.numPhone,'') AS Phone,
         isnull(A.numPhoneExtension,'') AS PhoneExtension,
		 		 (SELECT TOP 1 ISNULL(vcData,'') FROM ListDetails WHERE numListID=5 AND numListItemID=com.numCompanyType AND numDomainId=@numDomainID) AS vcCompanyType,
pro.numcontractId,                       
  dbo.fn_GetContactName(pro.numCreatedBy)+' '+convert(varchar(20),dateadd(minute,-@ClientTimeZoneOffset,pro.bintCreatedDate)) as vcCreatedBy ,                          
  dbo.fn_GetContactName(pro.numModifiedBy)+' '+convert(varchar(20),dateadd(minute,-@ClientTimeZoneOffset,pro.bintModifiedDate)) as vcModifiedby,                        
  dbo.fn_GetContactName(pro.numCompletedBy)+' '+convert(varchar(20),dateadd(minute,-@ClientTimeZoneOffset,pro.dtCompletionDate)) as vcFinishedby,                        
  pro.numDomainId,                          
  dbo.fn_GetContactName(pro.numRecOwner) as vcRecOwner,  pro.numRecOwner,   div.numTerID,       
  pro.numAssignedby,pro.numAssignedTo,          
(select  count(*) from dbo.GenericDocuments where numRecID=@numProId and  vcDocumentSection='P') as DocumentCount,
	
 numAccountID,
 '' TimeAndMaterial,
isnull((select sum(isnull(monTimeBudget,0)) from StagePercentageDetails where numProjectID=@numProId),0) TimeBudget,
isnull((select sum(isnull(monExpenseBudget,0)) from StagePercentageDetails where numProjectID=@numProId),0) ExpenseBudget,
isnull((select min(isnull(dtStartDate,getdate()))-1 from StagePercentageDetails where numProjectID=@numProId),getdate()) dtStartDate,
isnull((select max(isnull(dtEndDate,getdate()))+1 from StagePercentageDetails where numProjectID=@numProId),getdate()) dtEndDate,
ISNULL(numProjectType,0) AS numProjectType,
isnull(dbo.fn_GetExpenseDtlsbyProStgID(pro.numProId,0,0),0) as UsedExpense,                        
isnull(dbo.fn_GeTimeDtlsbyProStgID_BT_NBT(pro.numProId,0,1),'Billable Time (0)  Non Billable Time (0)') as UsedTime,
isnull(Pro.numProjectStatus,0) as numProjectStatus,
(SELECT ISNULL(vcStreet,'') + ',<pre>' + ISNULL(vcCity,'') + ',<pre>' +  dbo.[fn_GetState]([numState]) + ',<pre>' + dbo.fn_GetListItemName(numCountry) + ',<pre>' + vcPostalCode 
 FROM dbo.AddressDetails WHERE numDomainID = pro.numDomainID AND numRecordID = pro.numAddressID AND tintAddressOf = 4 AND tintAddressType = 2 AND bitIsPrimary = 1 )as ShippingAddress,
Pro.numBusinessProcessID,
SLP.Slp_Name AS vcProcessName,
pro.dtCompletionDate AS vcProjectedFinish,
NULL AS dtActualStartDate,
DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,pro.dtmEndDate) dtmEndDate,
DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,pro.dtmStartDate) dtmStartDate,
ISNULL(pro.numClientBudget,0) AS numClientBudget,
0 AS numClientBudgetBalance,
(CASE 
	WHEN EXISTS (SELECT CInner.numContractId FROM Contracts CInner WHERE CInner.numDomainId=@numDomainID AND CInner.numDivisonId=pro.numDivisionID AND intType=1) 
	THEN 1
	ELSE 0
END) AS bitContractExists,
0 AS numClientBudgetBalance,
(CASE 
	WHEN EXISTS (SELECT CInner.numContractId FROM Contracts CInner WHERE CInner.numDomainId=@numDomainID AND CInner.numDivisonId=pro.numDivisionID AND intType=1) 
	THEN dbo.fn_SecondsConversion(ISNULL((SELECT TOP 1 ((ISNULL(numHours,0) * 60 * 60) + (ISNULL(numMinutes,0) * 60)) - ISNULL((SELECT SUM(CLInner.numUsedTime) FROM ContractsLog CLInner WHERE CLInner.numContractId=C.numContractId),0) FROM Contracts C WHERE C.numDomainId=@numDomainID AND C.numDivisonId=pro.numDivisionID AND intType=1 ORDER BY C.numContractID DESC),0))
	ELSE '' 
END) AS timeLeft,
(CASE 
	WHEN EXISTS (SELECT CInner.numContractId FROM Contracts CInner WHERE CInner.numDomainId=@numDomainID AND CInner.numDivisonId=pro.numDivisionID AND intType=1) 
	THEN ISNULL((SELECT TOP 1 ((ISNULL(numHours,0) * 60 * 60) + (ISNULL(numMinutes,0) * 60)) - ISNULL((SELECT SUM(CLInner.numUsedTime) FROM ContractsLog CLInner WHERE CLInner.numContractId=C.numContractId),0) FROM Contracts C WHERE C.numDomainId=@numDomainID AND C.numDivisonId=pro.numDivisionID AND intType=1 ORDER BY C.numContractID DESC),0)
	ELSE 0
END) AS timeLeftInMinutes,
ISNULL(bitDisplayTimeToExternalUsers,0) AS bitDisplayTimeToExternalUsers
from ProjectsMaster pro                          
  left join DivisionMaster div                          
  on pro.numDivisionID=div.numDivisionID    
  LEFT JOIN AdditionalContactsInformation A ON pro.numCustPrjMgr = A.numContactId
  left join CompanyInfo com                          
  on div.numCompanyID=com.numCompanyId         
  LEFT JOIN dbo.AddressDetails AD ON AD.numAddressID = pro.numAddressID       
		LEFT JOIN Sales_process_List_Master AS SLP ON pro.numBusinessProcessID=SLP.Slp_Id          
  where numProId=@numProId    and pro.numDomainID=@numDomainID                      
                          
end
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_PromotionOffer_ApplyItemPromotionToOrder')
DROP PROCEDURE USP_PromotionOffer_ApplyItemPromotionToOrder
GO
CREATE PROCEDURE [dbo].[USP_PromotionOffer_ApplyItemPromotionToOrder]
	@numDomainID NUMERIC(18,0),
	@numDivisionID NUMERIC(18,0),
	@vcItems VARCHAR(MAX),
	@bitBasedOnDiscountCode BIT,
	@vcDiscountCode VARCHAR(100),
	@numCurrencyID NUMERIC(18,0)
AS
BEGIN
	DECLARE @hDocItem INT
	DECLARE @numRelationship NUMERIC(18,0)
	DECLARE @numProfile NUMERIC(18,0)
	DECLARE @fltExchangeRate FLOAT

	SET @fltExchangeRate = ISNULL((SELECT fltExchangeRate FROM Currency WHERE numDomainID=@numDomainID AND numCurrencyID=@numCurrencyID),1)

	IF ISNULL(@fltExchangeRate,0) = 0
	BEGIN
		SET @fltExchangeRate = 1
	END

	SELECT 
		@numRelationship=numCompanyType,
		@numProfile=vcProfile
	FROM 
		DivisionMaster D                  
	JOIN 
		CompanyInfo C 
	ON 
		C.numCompanyId=D.numCompanyID                  
	WHERE 
		numDivisionID =@numDivisionID 		

	DECLARE @TEMP TABLE
	(
		ID INT IDENTITY(1,1)
		,numOppItemID NUMERIC(18,0)
        ,numItemCode NUMERIC(18,0)
        ,numUnits FLOAT
		,numWarehouseItemID NUMERIC(18,0)
        ,monUnitPrice DECIMAL(20,5)
        ,fltDiscount DECIMAL(20,5)
        ,bitDiscountType BIT
        ,monTotalAmount DECIMAL(20,5)
        ,numPromotionID NUMERIC(18,0)
        ,bitPromotionTriggered BIT
        ,vcPromotionDescription VARCHAR(MAX)
		,bitPromotionDiscount BIT
		,vcKitChildItems VARCHAR(MAX)
		,vcInclusionDetail VARCHAR(MAX)
		,numSelectedPromotionID NUMERIC(18,0)
		,bitChanged BIT		
	)

	IF ISNULL(@vcItems,'') <> ''
	BEGIN
		EXEC sp_xml_preparedocument @hDocItem OUTPUT, @vcItems

		INSERT INTO @TEMP
		(
			numOppItemID
			,numItemCode
			,numUnits
			,numWarehouseItemID
			,monUnitPrice
			,fltDiscount
			,bitDiscountType
			,monTotalAmount
			,numPromotionID
			,bitPromotionTriggered
			,vcPromotionDescription
			,bitPromotionDiscount
			,vcKitChildItems
			,vcInclusionDetail
			,numSelectedPromotionID
			,bitChanged
		)

		SELECT 
			numOppItemID
			,numItemCode
			,numUnits
			,numWarehouseItemID
			,monUnitPrice * @fltExchangeRate
			,fltDiscount
			,bitDiscountType
			,monTotalAmount
			,numPromotionID
			,bitPromotionTriggered
			,vcPromotionDescription
			,bitPromotionDiscount
			,vcKitChildItems
			,vcInclusionDetail
			,numSelectedPromotionID
			,0
		FROM 
			OPENXML (@hDocItem,'/NewDataSet/Item',2)                                                                          
		WITH                       
		(                                                                          
			numOppItemID NUMERIC(18,0)
			,numItemCode NUMERIC(18,0)
			,numUnits FLOAT
			,numWarehouseItemID NUMERIC(18,0)
			,monUnitPrice DECIMAL(20,5)
			,fltDiscount DECIMAL(20,5)
			,bitDiscountType BIT
			,monTotalAmount DECIMAL(20,5)
			,numPromotionID NUMERIC(18,0)
			,bitPromotionTriggered BIT
			,bitPromotionDiscount BIT
			,vcKitChildItems VARCHAR(MAX)
			,vcInclusionDetail VARCHAR(MAX)
			,vcPromotionDescription VARCHAR(MAX)
			,numSelectedPromotionID NUMERIC(18,0)
		)

		EXEC sp_xml_removedocument @hDocItem

		DECLARE @i INT = 1
		DECLARE @iCount INT 
		SET @iCount = (SELECT COUNT(*) FROM @TEMP)

		DECLARE @l INT = 1
		DECLARE @lCount INT
		SET @lCount = (SELECT COUNT(*) FROM @TEMP)

		DECLARE @numTempPromotionID NUMERIC(18,0)
		DECLARE @tintOfferTriggerValueType TINYINT
		DECLARE @tintOfferBasedOn TINYINT
		DECLARE @tintOfferTriggerValueTypeRange TINYINT
		DECLARE @fltOfferTriggerValue FLOAT
		DECLARE @fltOfferTriggerValueRange FLOAT
		DECLARE @vcShortDesc VARCHAR(500)
		DECLARE @tintItemCalDiscount TINYINT
		DECLARE @monDiscountedItemPrice DECIMAL(20,5)
		DECLARE @numItemCode NUMERIC(18,0)
		DECLARE @numTempWarehouseItemID NUMERIC(18,0)
		DECLARE @numUnits FLOAT
		DECLARE @monTotalAmount DECIMAL(20,5)
		DECLARE @numItemClassification AS NUMERIC(18,0)

		DECLARE @bitRemainingCheckRquired AS BIT
		DECLARE @numRemainingPromotion FLOAT
		DECLARE @fltDiscountValue FLOAT
		DECLARE @tintDiscountType TINYINT
		DECLARE @tintDiscoutBaseOn TINYINT

		DECLARE @TEMPUsedPromotion TABLE
		(
			ID INT
			,numPromotionID NUMERIC(18,0)
			,numUnits FLOAT
			,monTotalAmount DECIMAL(20,5)
		)

		INSERT INTO @TEMPUsedPromotion
		(
			ID
			,numPromotionID
			,numUnits
			,monTotalAmount
		)
		SELECT
			ROW_NUMBER() OVER(ORDER BY T1.numPromotionID ASC)
			,numPromotionID
			,SUM(numUnits)
			,SUM(monTotalAmount)
		FROM
			@TEMP T1
		WHERE
			ISNULL(numPromotionID,0) > 0
			AND ISNULL(bitPromotionTriggered,0) = 1
		GROUP BY
			numPromotionID

		DECLARE @j INT = 1
		DECLARE @jCount INT
		SET @jCount = ISNULL((SELECT COUNT(*) FROM @TEMPUsedPromotion),0)

		-- FIRST REMOVE ALL PROMOTIONS WHERE ITEM WHICH TRIGERRED PROMOTION IS DELETED
		UPDATE 
			T1
		SET 
			numPromotionID=0
			,bitPromotionTriggered=0
			,vcPromotionDescription=''
			,bitPromotionDiscount=0
			,bitChanged=1
			,monUnitPrice=ISNULL(T2.monPrice,0)
			,fltDiscount=ISNULL(T2.decDiscount,0)
			,bitDiscountType=ISNULL(T2.tintDisountType,0)
		FROM
			@TEMP T1
		CROSS APPLY
			dbo.fn_GetItemPriceBasedOnMethod(@numDomainID,@numDivisionID,T1.numItemCode,T1.numUnits,0,0,T1.numWarehouseItemID,T1.vcKitChildItems,@numCurrencyID) T2
		WHERE 
			numPromotionID > 0
			AND ISNULL(bitPromotionTriggered,0) = 0
			AND (SELECT COUNT(*) FROM @TEMP T3 WHERE T3.numPromotionID=T1.numPromotionID AND ISNULL(T3.bitPromotionTriggered,0) = 1) = 0

		-- FIRST REMOVE APPLIED PROMOTION IF THEY ARE NOT VALIES
		WHILE @j <= @jCount
		BEGIN
			SELECT 
				@numTempPromotionID=numPromotionID
				,@numUnits = numUnits
				,@monTotalAmount=monTotalAmount
			FROM 
				@TEMPUsedPromotion 
			WHERE 
				ID = @j

			IF NOT EXISTS (SELECT 
								PO.numProId 
							FROM 
								PromotionOffer PO
							LEFT JOIN	
								PromotionOffer POOrder
							ON
								PO.numOrderPromotionID = POOrder.numProId
							LEFT JOIN	
								DiscountCodes DC
							ON
								PO.numOrderPromotionID = DC.numPromotionID
							WHERE 
								PO.numDomainId=@numDomainID 
								AND PO.numProId=@numTempPromotionID
								AND ISNULL(PO.bitEnabled,0)=1 
								AND ISNULL(PO.IsOrderBasedPromotion,0) = 0
								AND 1 = (CASE 
											WHEN ISNULL(PO.numOrderPromotionID,0) > 0 
											THEN (CASE WHEN POOrder.bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN GETUTCDATE()>=POOrder.dtValidFrom AND GETUTCDATE()<=POOrder.dtValidTo THEN 1 ELSE 0 END) END)
											ELSE (CASE WHEN PO.bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN GETUTCDATE()>=PO.dtValidFrom AND GETUTCDATE()<=PO.dtValidTo THEN 1 ELSE 0 END) END)
										END)
								AND 1 = (CASE 
											WHEN ISNULL(PO.numOrderPromotionID,0) > 0 
											THEN
												(CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=PO.numOrderPromotionID AND tintType=2 AND numRelationship=@numRelationship AND numProfile=@numProfile) > 0 THEN 1 ELSE 0 END)
											ELSE
												(CASE PO.tintCustomersBasedOn 
													WHEN 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=PO.numProId AND tintType=1 AND numDivisionID=@numDivisionID) > 0 THEN 1 ELSE 0 END)
													WHEN 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=PO.numProId AND tintType=2 AND numRelationship=@numRelationship AND numProfile=@numProfile) > 0 THEN 1 ELSE 0 END)
													WHEN 3 THEN 1
													ELSE 0
												END)
										END)
								AND 1 = (CASE 
											WHEN ISNULL(PO.tintOfferTriggerValueType,1) = 1 --Based on quantity
											THEN
												CASE
													WHEN ISNULL(PO.tintOfferTriggerValueTypeRange,1) = 2
													THEN
														(CASE WHEN ISNULL(@numUnits,0) BETWEEN ISNULL(PO.fltOfferTriggerValue,0) AND ISNULL(PO.fltOfferTriggerValueRange,0) THEN 1 ELSE 0 END)
													ELSE
														(CASE WHEN ISNULL(@numUnits,0) >= PO.fltOfferTriggerValue THEN 1 ELSE 0 END)
												END
											WHEN ISNULL(PO.tintOfferTriggerValueType,1) = 2 --Based on amount
											THEN
												CASE
													WHEN ISNULL(PO.tintOfferTriggerValueTypeRange,1) = 2
													THEN
														(CASE WHEN ISNULL(@monTotalAmount,0) BETWEEN ISNULL(PO.fltOfferTriggerValue,0) AND ISNULL(PO.fltOfferTriggerValueRange,0) THEN 1 ELSE 0 END)
													ELSE
														(CASE WHEN ISNULL(@monTotalAmount,0) >= PO.fltOfferTriggerValue THEN 1 ELSE 0 END)
												END
										END)
								AND 1 = (CASE 
											WHEN ISNULL(PO.numOrderPromotionID,0) > 0 
											THEN 
												(CASE WHEN ISNULL(DC.CodeUsageLimit,0) <> 0 THEN (CASE WHEN ISNULL((SELECT intCodeUsed FROM DiscountCodeUsage WHERE numDiscountId=DC.numDiscountId AND numDivisionId=@numDivisionId),0) >= DC.CodeUsageLimit THEN 0 ELSE 1 END) ELSE 1 END)
											ELSE 1 
										END)
					)
			BEGIN
				-- IF Promotion is not valid than revert line item price to based on default pricing
				UPDATE 
					T1
				SET 
					numPromotionID=0
					,bitPromotionTriggered=0
					,vcPromotionDescription=''
					,bitPromotionDiscount=0
					,bitChanged=1
					,monUnitPrice=ISNULL(T2.monPrice,0)
					,fltDiscount=ISNULL(T2.decDiscount,0)
					,bitDiscountType=ISNULL(T2.tintDisountType,0)
				FROM
					@TEMP T1
				CROSS APPLY
					dbo.fn_GetItemPriceBasedOnMethod(@numDomainID,@numDivisionID,T1.numItemCode,T1.numUnits,0,0,T1.numWarehouseItemID,T1.vcKitChildItems,@numCurrencyID) T2
				WHERE 
					numPromotionID=@numTempPromotionID
			END

			SET @j = @j + 1
		END

		UPDATE
			T1
		SET 
			T1.bitPromotionDiscount = 0
			,T1.bitChanged=1
			,T1.monUnitPrice=ISNULL(T2.monPrice,0)
			,T1.fltDiscount=ISNULL(T2.decDiscount,0)
			,T1.bitDiscountType=ISNULL(T2.tintDisountType,0)
		FROM
			@TEMP T1
		INNER JOIN
			PromotionOffer PO
		ON
			T1.numPromotionID=PO.numProId
		CROSS APPLY
			dbo.fn_GetItemPriceBasedOnMethod(@numDomainID,@numDivisionID,T1.numItemCode,T1.numUnits,0,0,T1.numWarehouseItemID,T1.vcKitChildItems,@numCurrencyID) T2
		WHERE
			ISNULL(numPromotionID,0) > 0
			AND ISNULL(bitPromotionDiscount,0) = 1
			AND ISNULL(PO.tintDiscountType,0) = 3

		IF ISNULL(@bitBasedOnDiscountCode,0) = 1
		BEGIN
			DECLARE @TableItemCouponPromotion TABLE
			(
				ID INT IDENTITY(1,1)
				,numPromotionID NUMERIC(18,0)
				,tintOfferTriggerValueType TINYINT
				,tintOfferTriggerValueTypeRange TINYINT
				,fltOfferTriggerValue FLOAT
				,fltOfferTriggerValueRange FLOAT
				,tintOfferBasedOn TINYINT
				,vcShortDesc VARCHAR(300)
			)

			INSERT INTO @TableItemCouponPromotion
			(
				numPromotionID
				,tintOfferTriggerValueType
				,tintOfferTriggerValueTypeRange
				,fltOfferTriggerValue
				,fltOfferTriggerValueRange
				,tintOfferBasedOn
				,vcShortDesc
			)
			SELECT
				PO.numProId
				,PO.tintOfferTriggerValueType
				,PO.tintOfferTriggerValueTypeRange
				,PO.fltOfferTriggerValue
				,PO.fltOfferTriggerValueRange
				,PO.tintOfferBasedOn
				,ISNULL(PO.vcShortDesc,'-')
			FROM
				PromotionOffer PO
			INNER JOIN
				PromotionOffer POOrder
			ON
				PO.numOrderPromotionID = POOrder.numProId
			INNER JOIN 
				DiscountCodes DC
			ON 
				POOrder.numProId = DC.numPromotionID
			WHERE
				PO.numDomainId = @numDomainID
				AND POOrder.numDomainId=@numDomainID 
				AND ISNULL(PO.bitEnabled,0)=1 
				AND ISNULL(POOrder.bitEnabled,0)=1 
				AND ISNULL(PO.bitUseOrderPromotion,0)=1 
				AND ISNULL(POOrder.IsOrderBasedPromotion,0) = 1
				AND ISNULL(POOrder.bitRequireCouponCode,0) = 1
				AND 1 = (CASE WHEN POOrder.bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN GETUTCDATE()>=POOrder.dtValidFrom AND GETUTCDATE()<=POOrder.dtValidTo THEN 1 ELSE 0 END) END)
				AND 1 = (CASE WHEN ISNULL(DC.CodeUsageLimit,0) <> 0 THEN (CASE WHEN ISNULL((SELECT intCodeUsed FROM DiscountCodeUsage WHERE numDiscountId=DC.numDiscountId AND numDivisionId=@numDivisionId),0) >= DC.CodeUsageLimit THEN 0 ELSE 1 END) ELSE 1 END)
				AND 1 = (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=POOrder.numProId AND tintType=2 AND numRelationship=@numRelationship AND numProfile=@numProfile) > 0 THEN 1 ELSE 0 END)
				AND DC.vcDiscountCode = @vcDiscountCode
			
			SET @l = 1
			DECLARE @k INT = 1
			DECLARE @kCount INT 
			SET @kCount = (SELECT COUNT(*) FROM @TableItemCouponPromotion)

			WHILE @k <= @kCount
			BEGIN
				SELECT
					@numTempPromotionID=numPromotionID
					,@tintOfferTriggerValueType=tintOfferTriggerValueType
					,@tintOfferTriggerValueTypeRange = tintOfferTriggerValueTypeRange
					,@fltOfferTriggerValue=fltOfferTriggerValue
					,@fltOfferTriggerValueRange=fltOfferTriggerValueRange
					,@tintOfferBasedOn=tintOfferBasedOn
					,@vcShortDesc=vcShortDesc
				FROM
					@TableItemCouponPromotion
				WHERE
					ID=@k

				IF (SELECT COUNT(*) FROM @TEMP WHERE numPromotionID=@numTempPromotionID AND ISNULL(bitPromotionTriggered,0)=1) = 0
				BEGIN
					IF ISNULL((SELECT 
								(CASE WHEN @tintOfferTriggerValueType = 2 THEN SUM(ISNULL(monTotalAmount,0)) ELSE SUM(ISNULL(numUnits,0)) END)
							FROM
								@TEMP T1
							INNER JOIN
								Item I
							ON
								T1.numItemCode = I.numItemCode
							WHERE
								ISNULL(numPromotionID,0) = 0
								AND 1 = (CASE 
											WHEN @tintOfferBasedOn = 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=@numTempPromotionID AND tintRecordType=5 AND tintType=1 AND numValue=T1.numItemCode) > 0 THEN 1 ELSE 0 END)
											WHEN @tintOfferBasedOn = 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=@numTempPromotionID AND tintRecordType=5 AND tintType=2 AND numValue=ISNULL(I.numItemClassification,0)) > 0 THEN 1 ELSE 0 END)
											WHEN @tintOfferBasedOn = 4 THEN 1 
											ELSE 0
										END)
							),0) >= ISNULL(@fltOfferTriggerValue,0)
					BEGIN
						SET @i = 1

						WHILE @i <= @iCount
						BEGIN

							IF ISNULL((SELECT 
										(CASE WHEN @tintOfferTriggerValueType = 2 THEN SUM(ISNULL(monTotalAmount,0)) ELSE SUM(ISNULL(numUnits,0)) END)
									FROM
										@TEMP T1
									WHERE
										ISNULL(numPromotionID,0) = @numTempPromotionID
										AND ISNULL(bitPromotionTriggered,0) = 1
									),0) < ISNULL(@fltOfferTriggerValue,0)
							BEGIN
								UPDATE
									T1
								SET
									bitChanged = 1
									,numPromotionID=@numTempPromotionID
									,bitPromotionTriggered=1
									,vcPromotionDescription=ISNULL(@vcShortDesc,'')
								FROM
									@TEMP T1
								INNER JOIN
									Item I
								ON
									T1.numItemCode = I.numItemCode
								WHERE
									T1.ID = @i
									AND ISNULL(numPromotionID,0) = 0
									AND 1 = (CASE 
												WHEN ISNULL(@tintOfferTriggerValueType,1) = 1 --Based on quantity
												THEN
													CASE
														WHEN ISNULL(@tintOfferTriggerValueTypeRange,1) = 2
														THEN
															(CASE WHEN ISNULL(T1.numUnits,0) + ISNULL((SELECT SUM(ISNULL(numUnits,0)) FROM @TEMP WHERE ISNULL(numPromotionID,0) = @numTempPromotionID AND ISNULL(bitPromotionTriggered,0) = 1),0) BETWEEN ISNULL(@fltOfferTriggerValue,0) AND ISNULL(@fltOfferTriggerValueRange,0) THEN 1 ELSE 0 END)
														ELSE
															1
													END
												WHEN ISNULL(@tintOfferTriggerValueType,1) = 2 --Based on amount
												THEN
													CASE
														WHEN ISNULL(@tintOfferTriggerValueTypeRange,1) = 2
														THEN
															(CASE WHEN ISNULL(T1.monTotalAmount,0) + ISNULL((SELECT SUM(ISNULL(monTotalAmount,0)) FROM @TEMP WHERE ISNULL(numPromotionID,0) = @numTempPromotionID AND ISNULL(bitPromotionTriggered,0) = 1),0) BETWEEN ISNULL(@fltOfferTriggerValue,0) AND ISNULL(@fltOfferTriggerValueRange,0) THEN 1 ELSE 0 END)
														ELSE
															1
													END
											END)
									AND 1 = (CASE 
												WHEN @tintOfferBasedOn = 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=@numTempPromotionID AND tintRecordType=5 AND tintType=1 AND numValue=T1.numItemCode) > 0 THEN 1 ELSE 0 END)
												WHEN @tintOfferBasedOn = 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=@numTempPromotionID AND tintRecordType=5 AND tintType=2 AND numValue=ISNULL(I.numItemClassification,0)) > 0 THEN 1 ELSE 0 END)
												WHEN @tintOfferBasedOn = 4 THEN 1 
												ELSE 0
											END)
							END

							SET @i = @i + 1
						END

					END
				END

				SET @k = @k + 1
			END
		END

		-- TRIGGER PROMOTION FOR ITEM WHICH ARE ELIGIBLE
		SET @i = 1
		WHILE @i <= @iCount
		BEGIN
			SET @numTempPromotionID = NULL

			SELECT
				@numTempPromotionID=numProId
				,@tintOfferTriggerValueType=tintOfferTriggerValueType
				,@tintOfferTriggerValueTypeRange = tintOfferTriggerValueTypeRange
				,@fltOfferTriggerValue=fltOfferTriggerValue
				,@fltOfferTriggerValueRange=fltOfferTriggerValueRange
				,@tintOfferBasedOn=tintOfferBasedOn
				,@vcShortDesc=vcShortDesc
			FROM
				@TEMP T1
			INNER JOIN
				Item I
			ON
				T1.numItemCode=I.numItemCode
			CROSS APPLY
			(
				SELECT TOP 1
					PO.numProId
					,PO.tintOfferBasedOn
					,PO.tintOfferTriggerValueType
					,PO.tintOfferTriggerValueTypeRange
					,PO.fltOfferTriggerValue
					,PO.fltOfferTriggerValueRange
					,ISNULL(PO.vcShortDesc,'') vcShortDesc
				FROM 
					PromotionOffer PO
				WHERE
					PO.numDomainId=@numDomainID
					AND 1 = (CASE WHEN ISNULL(T1.numSelectedPromotionID,0) > 0 THEN (CASE WHEN PO.numProId=T1.numSelectedPromotionID THEN 1 ELSE 0 END) ELSE 1 END) 
					AND ISNULL(PO.bitEnabled,0)=1 
					AND ISNULL(PO.IsOrderBasedPromotion,0) = 0
					AND ISNULL(PO.bitUseOrderPromotion,0) = 0
					AND ISNULL(PO.bitRequireCouponCode,0) = 0
					AND 1 = (CASE WHEN PO.bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN GETUTCDATE()>=PO.dtValidFrom AND GETUTCDATE()<=PO.dtValidTo THEN 1 ELSE 0 END) END)
					AND 1 = (CASE 
								WHEN ISNULL(numOrderPromotionID,0) > 0
								THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=numOrderPromotionID AND tintType=2 AND numRelationship=@numRelationship AND numProfile=@numProfile) > 0 THEN 1 ELSE 0 END)
								ELSE
									(CASE tintCustomersBasedOn 
										WHEN 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=PO.numProId AND tintType=1 AND numDivisionID=@numDivisionID) > 0 THEN 1 ELSE 0 END)
										WHEN 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=PO.numProId AND tintType=2 AND numRelationship=@numRelationship AND numProfile=@numProfile) > 0 THEN 1 ELSE 0 END)
										WHEN 3 THEN 1
										ELSE 0
									END)
							END)
					AND 1 = (CASE 
								WHEN PO.tintOfferBasedOn = 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=1 AND numValue=T1.numItemCode) > 0 THEN 1 ELSE 0 END)
								WHEN PO.tintOfferBasedOn = 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=2 AND numValue=ISNULL(I.numItemClassification,0)) > 0 THEN 1 ELSE 0 END)
								WHEN PO.tintOfferBasedOn = 4 THEN 1 
								ELSE 0
							END)
				ORDER BY
					(CASE 
						WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 1 THEN 1
						WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 1 THEN 2
						WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 2 THEN 3
						WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 2 THEN 4
						WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 3 THEN 5
						WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 3 THEN 6
					END) ASC,ISNULL(PO.fltOfferTriggerValue,0) DESC

			) T2
			WHERE
				ISNULL(T1.numPromotionID,0)  = 0
				AND T1.ID = @i
				AND ISNULL((SELECT COUNT(*) FROM @TEMP T3 WHERE T3.numPromotionID=T2.numProId AND T3.bitPromotionTriggered=1),0) = 0

			IF ISNULL(@numTempPromotionID,0) > 0 AND (SELECT COUNT(*) FROM @TEMP WHERE numPromotionID=@numTempPromotionID AND ISNULL(bitPromotionTriggered,0)=1) = 0
			BEGIN
				IF ISNULL((SELECT 
								(CASE WHEN @tintOfferTriggerValueType = 2 THEN SUM(ISNULL(monTotalAmount,0)) ELSE SUM(ISNULL(numUnits,0)) END)
							FROM
								@TEMP T1
							INNER JOIN
								Item I
							ON
								T1.numItemCode = I.numItemCode
							WHERE
								ISNULL(numPromotionID,0) = 0
								AND 1 = (CASE 
											WHEN @tintOfferBasedOn = 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=@numTempPromotionID AND tintRecordType=5 AND tintType=1 AND numValue=T1.numItemCode) > 0 THEN 1 ELSE 0 END)
											WHEN @tintOfferBasedOn = 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=@numTempPromotionID AND tintRecordType=5 AND tintType=2 AND numValue=ISNULL(I.numItemClassification,0)) > 0 THEN 1 ELSE 0 END)
											WHEN @tintOfferBasedOn = 4 THEN 1 
											ELSE 0
										END)
							),0) >= ISNULL(@fltOfferTriggerValue,0)
				BEGIN
					SET @l = 1

					WHILE @l <= @lCount
					BEGIN
						
						IF ISNULL((SELECT 
										(CASE WHEN @tintOfferTriggerValueType = 2 THEN SUM(ISNULL(monTotalAmount,0)) ELSE SUM(ISNULL(numUnits,0)) END)
									FROM
										@TEMP T1
									WHERE
										ISNULL(numPromotionID,0) = @numTempPromotionID
										AND ISNULL(bitPromotionTriggered,0) = 1
									),0) < ISNULL(@fltOfferTriggerValue,0)
						BEGIN
							UPDATE
								T1
							SET
								bitChanged = 1
								,numPromotionID=@numTempPromotionID
								,bitPromotionTriggered=1
								,vcPromotionDescription=ISNULL(@vcShortDesc,'')
							FROM
								@TEMP T1
							INNER JOIN
								Item I
							ON
								T1.numItemCode = I.numItemCode
							WHERE
								T1.ID = @l
								AND ISNULL(numPromotionID,0) = 0
								AND 1 = (CASE 
											WHEN ISNULL(@tintOfferTriggerValueType,1) = 1 --Based on quantity
											THEN
												CASE
													WHEN ISNULL(@tintOfferTriggerValueTypeRange,1) = 2
													THEN
														(CASE WHEN ISNULL(T1.numUnits,0) + ISNULL((SELECT SUM(ISNULL(numUnits,0)) FROM @TEMP WHERE ISNULL(numPromotionID,0) = @numTempPromotionID AND ISNULL(bitPromotionTriggered,0) = 1),0) BETWEEN ISNULL(@fltOfferTriggerValue,0) AND ISNULL(@fltOfferTriggerValueRange,0) THEN 1 ELSE 0 END)
													ELSE
														1
												END
											WHEN ISNULL(@tintOfferTriggerValueType,1) = 2 --Based on amount
											THEN
												CASE
													WHEN ISNULL(@tintOfferTriggerValueTypeRange,1) = 2
													THEN
														(CASE WHEN ISNULL(T1.monTotalAmount,0) + ISNULL((SELECT SUM(ISNULL(monTotalAmount,0)) FROM @TEMP WHERE ISNULL(numPromotionID,0) = @numTempPromotionID AND ISNULL(bitPromotionTriggered,0) = 1),0) BETWEEN ISNULL(@fltOfferTriggerValue,0) AND ISNULL(@fltOfferTriggerValueRange,0) THEN 1 ELSE 0 END)
													ELSE
														1
												END
										END)
								AND 1 = (CASE 
											WHEN @tintOfferBasedOn = 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=@numTempPromotionID AND tintRecordType=5 AND tintType=1 AND numValue=T1.numItemCode) > 0 THEN 1 ELSE 0 END)
											WHEN @tintOfferBasedOn = 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=@numTempPromotionID AND tintRecordType=5 AND tintType=2 AND numValue=ISNULL(I.numItemClassification,0)) > 0 THEN 1 ELSE 0 END)
											WHEN @tintOfferBasedOn = 4 THEN 1 
											ELSE 0
										END)
						END

						SET @l = @l + 1
					END

				END
			END

			SET @i = @i + 1
		END
		

		DECLARE @TEMPPromotion TABLE
		(
			ID INT
			,numPromotionID NUMERIC(18,0)
			,vcShortDesc VARCHAR(500)
			,numTriggerItemCode NUMERIC(18,0)
			,numWarehouseItemID NUMERIC(18,0)
			,numUnits FLOAT
			,monTotalAmount DECIMAL(20,5)
		)

		INSERT INTO @TEMPPromotion
		(
			ID
			,numPromotionID
			,numTriggerItemCode
			,numWarehouseItemID
			,numUnits
			,monTotalAmount
		)
		SELECT
			ROW_NUMBER() OVER(ORDER BY T1.numOppItemID ASC)
			,numPromotionID
			,numItemCode
			,numWarehouseItemID
			,numUnits
			,monTotalAmount
		FROM
			@TEMP T1
		INNER JOIN
			PromotionOffer PO
		ON
			T1.numPromotionID = PO.numProId
		WHERE
			ISNULL(numPromotionID,0) > 0
			AND ISNULL(bitPromotionTriggered,0) = 1
			AND 1 = (CASE WHEN @bitBasedOnDiscountCode=0 THEN (CASE WHEN ISNULL(PO.numOrderPromotionID,0) = 0 THEN 1 ELSE 0 END) ELSE 1 END)

		SET @j = 1
		SET @jCount = ISNULL((SELECT COUNT(*) FROM @TEMPPromotion),0)

		WHILE @j <= @jCount
		BEGIN
			SELECT 
				@numTempPromotionID=numPromotionID
				,@numItemCode=numTriggerItemCode
				,@numTempWarehouseItemID=numWarehouseItemID
				,@numUnits = numUnits
				,@monTotalAmount=monTotalAmount
			FROM 
				@TEMPPromotion 
			WHERE 
				ID = @j

			SELECT
				@fltDiscountValue=ISNULL(fltDiscountValue,0)
				,@tintDiscountType=ISNULL(tintDiscountType,0)
				,@tintDiscoutBaseOn=tintDiscoutBaseOn
				,@vcShortDesc=ISNULL(vcShortDesc,'')
				,@tintItemCalDiscount=ISNULL(tintItemCalDiscount,1)
				,@monDiscountedItemPrice=ISNULL(monDiscountedItemPrice,0)
			FROM
				PromotionOffer
			WHERE
				numProId=@numTempPromotionID

			-- If discount is based on amount or quantity than we have to checked remaining amount or qty left to give as discount 
			IF @tintDiscountType = 2 OR @tintDiscountType = 3
			BEGIN
				SET @bitRemainingCheckRquired = 1
			END
			ELSE
			BEGIN
				SET @bitRemainingCheckRquired = 0
				SET @numRemainingPromotion = 0
			END

			-- If promotion is valid than check whether any item left to apply promotion
			SET @i = 1

			WHILE @i <= @iCount
			BEGIN
				IF @bitRemainingCheckRquired=1
				BEGIN
					IF @tintDiscountType = 2 -- Discount by amount
					BEGIN
						SET @numRemainingPromotion = @fltDiscountValue - ISNULL((SELECT SUM(fltDiscount) FROM @TEMP WHERE numPromotionID = @numTempPromotionID	AND ISNULL(bitDiscountType,0)=1 AND bitPromotionDiscount = 1),0)
					END
					ELSE IF @tintDiscountType = 3 -- Discount by quantity
					BEGIN
						IF @tintDiscoutBaseOn IN (5,6)
						BEGIN
							SET @numRemainingPromotion = @fltDiscountValue - ISNULL((SELECT SUM(numUnits) FROM @TEMP WHERE numPromotionID = @numTempPromotionID	AND ISNULL(bitDiscountType,0)=1 AND bitPromotionDiscount = 1),0)
						END
						ELSE
						BEGIN
							SET @numRemainingPromotion = @fltDiscountValue - ISNULL((SELECT SUM(fltDiscount/monUnitPrice) FROM @TEMP WHERE numPromotionID = @numTempPromotionID	AND ISNULL(bitDiscountType,0)=1 AND bitPromotionDiscount = 1),0)
						END
					END
				END

				IF @bitRemainingCheckRquired=0 OR (@bitRemainingCheckRquired=1 AND @numRemainingPromotion > 0)
				BEGIN
					UPDATE
						T1
					SET
						monUnitPrice= CASE 
											WHEN (ISNULL(bitKitParent,0)=1 OR ISNULL(bitAssembly,0) = 1) AND ISNULL(bitCalAmtBasedonDepItems,0) = 1
											THEN (dbo.GetCalculatedPriceForKitAssembly(@numDomainID,@numDivisionID,I.numItemCode,T1.numUnits,@numTempWarehouseItemID,ISNULL(tintKitAssemblyPriceBasedOn,1),0,0,ISNULL(T1.vcKitChildItems,''),@numCurrencyID,@fltExchangeRate) * @fltExchangeRate)
											ELSE (CASE WHEN @tintItemCalDiscount = 1 THEN (ISNULL(T2.monFinalPrice,0) * @fltExchangeRate) ELSE ISNULL(I.monListPrice,0) END)
										END
						,fltDiscount = (CASE 
											WHEN @bitRemainingCheckRquired=0 
											THEN 
												@fltDiscountValue
											ELSE 
												(CASE 
													WHEN @tintDiscountType = 2 -- Discount by amount
													THEN
														(CASE 
															WHEN (T1.numUnits * (CASE 
																					WHEN (ISNULL(bitKitParent,0)=1 OR ISNULL(bitAssembly,0) = 1) AND ISNULL(bitCalAmtBasedonDepItems,0) = 1
																					THEN (dbo.GetCalculatedPriceForKitAssembly(@numDomainID,@numDivisionID,I.numItemCode,T1.numUnits,@numTempWarehouseItemID,ISNULL(tintKitAssemblyPriceBasedOn,1),0,0,ISNULL(T1.vcKitChildItems,''),@numCurrencyID,@fltExchangeRate) * @fltExchangeRate)
																					ELSE (CASE WHEN @tintItemCalDiscount = 1 THEN (ISNULL(T2.monFinalPrice,0) * @fltExchangeRate) ELSE ISNULL(I.monListPrice,0) END)
																				END)) >= @numRemainingPromotion 
															THEN @numRemainingPromotion 
															ELSE (T1.numUnits * (CASE 
																					WHEN (ISNULL(bitKitParent,0)=1 OR ISNULL(bitAssembly,0) = 1) AND ISNULL(bitCalAmtBasedonDepItems,0) = 1
																					THEN (dbo.GetCalculatedPriceForKitAssembly(@numDomainID,@numDivisionID,I.numItemCode,T1.numUnits,@numTempWarehouseItemID,ISNULL(tintKitAssemblyPriceBasedOn,1),0,0,ISNULL(T1.vcKitChildItems,''),@numCurrencyID,@fltExchangeRate) * @fltExchangeRate)
																					ELSE (CASE WHEN @tintItemCalDiscount = 1 THEN (ISNULL(T2.monFinalPrice,0) * @fltExchangeRate) ELSE ISNULL(I.monListPrice,0) END)
																				END)) 
														END)
													WHEN @tintDiscountType = 3 -- Discount by quantity
													THEN
														(CASE 
															WHEN @tintDiscoutBaseOn IN (5,6)
															THEN
																(CASE 
																	WHEN (CASE WHEN (ISNULL(T1.numPromotionID,0) = @numTempPromotionID AND ISNULL(bitPromotionTriggered,0)=1 AND ISNULL(bitPromotionDiscount,0)=0) 
																		THEN T1.numUnits - ISNULL(PO.fltOfferTriggerValue,0) 
																		ELSE T1.numUnits
																	END) >= @numRemainingPromotion 
																	THEN @numRemainingPromotion 
																	ELSE (CASE WHEN (ISNULL(T1.numPromotionID,0) = @numTempPromotionID AND ISNULL(bitPromotionTriggered,0)=1 AND ISNULL(bitPromotionDiscount,0)=0) 
																		THEN T1.numUnits - ISNULL(PO.fltOfferTriggerValue,0) 
																		ELSE T1.numUnits
																	END)
																END) * (CASE 
																			WHEN (CASE WHEN @tintItemCalDiscount = 1 THEN (ISNULL(T2.monFinalPrice,0) * @fltExchangeRate) ELSE ISNULL(I.monListPrice,0) END) > @monDiscountedItemPrice
																			THEN ((CASE WHEN @tintItemCalDiscount = 1 THEN (ISNULL(T2.monFinalPrice,0) * @fltExchangeRate) ELSE ISNULL(I.monListPrice,0) END) - @monDiscountedItemPrice)
																			ELSE 
																				(CASE WHEN @tintItemCalDiscount = 1 THEN (ISNULL(T2.monFinalPrice,0) * @fltExchangeRate) ELSE ISNULL(I.monListPrice,0) END) 
																		END)
															ELSE
																(CASE 
																	WHEN (CASE WHEN (ISNULL(T1.numPromotionID,0) = @numTempPromotionID AND ISNULL(bitPromotionTriggered,0)=1 AND ISNULL(bitPromotionDiscount,0)=0) 
																			THEN T1.numUnits - ISNULL(PO.fltOfferTriggerValue,0) 
																			ELSE T1.numUnits
																		END) >= @numRemainingPromotion 
																	THEN (@numRemainingPromotion * (CASE 
																										WHEN (ISNULL(bitKitParent,0)=1 OR ISNULL(bitAssembly,0) = 1) AND ISNULL(bitCalAmtBasedonDepItems,0) = 1
																										THEN (dbo.GetCalculatedPriceForKitAssembly(@numDomainID,@numDivisionID,I.numItemCode,T1.numUnits,@numTempWarehouseItemID,ISNULL(tintKitAssemblyPriceBasedOn,1),0,0,ISNULL(T1.vcKitChildItems,''),@numCurrencyID,@fltExchangeRate) * @fltExchangeRate)
																										ELSE (CASE WHEN @tintItemCalDiscount = 1 THEN (ISNULL(T2.monFinalPrice,0) * @fltExchangeRate) ELSE ISNULL(I.monListPrice,0) END)
																									END)) 
																	ELSE ((CASE WHEN (ISNULL(T1.numPromotionID,0) = @numTempPromotionID AND ISNULL(bitPromotionTriggered,0)=1 AND ISNULL(bitPromotionDiscount,0)=0) 
																				THEN T1.numUnits - ISNULL(PO.fltOfferTriggerValue,0) 
																				ELSE T1.numUnits
																			END) * (CASE 
																						WHEN (ISNULL(bitKitParent,0)=1 OR ISNULL(bitAssembly,0) = 1) AND ISNULL(bitCalAmtBasedonDepItems,0) = 1
																						THEN (dbo.GetCalculatedPriceForKitAssembly(@numDomainID,@numDivisionID,I.numItemCode,T1.numUnits,@numTempWarehouseItemID,ISNULL(tintKitAssemblyPriceBasedOn,1),0,0,ISNULL(T1.vcKitChildItems,''),@numCurrencyID,@fltExchangeRate) * @fltExchangeRate)
																						ELSE (CASE WHEN @tintItemCalDiscount = 1 THEN (ISNULL(T2.monFinalPrice,0) * @fltExchangeRate) ELSE ISNULL(I.monListPrice,0) END)
																					END)) 
																END)
														END)
													ELSE 0
												END)
											END
										)
						,bitDiscountType = (CASE WHEN @bitRemainingCheckRquired=0 THEN 0 ELSE 1 END)
						,numPromotionID=@numTempPromotionID
						,bitPromotionTriggered=(CASE WHEN bitPromotionTriggered=1 THEN 1 ELSE 0 END)
						,vcPromotionDescription=@vcShortDesc
						,bitChanged=1
						,bitPromotionDiscount=1
					FROM
						@TEMP T1
					INNER JOIN
						Item I
					ON
						T1.numItemCode = I.numItemCode
					LEFT JOIN
						PromotionOffer PO
					ON
						T1.numPromotionID = PO.numProId
					CROSS APPLY
						dbo.fn_GetItemPriceBasedOnMethod(@numDomainID,@numDivisionID,T1.numItemCode,T1.numUnits,0,0,T1.numWarehouseItemID,T1.vcKitChildItems,@numCurrencyID) T2
					WHERE
						ID=@i
						AND (ISNULL(T1.numPromotionID,0) = 0 OR (ISNULL(T1.numPromotionID,0) = @numTempPromotionID AND ISNULL(bitPromotionDiscount,0)=0))
						AND 1 = (CASE 
										WHEN (ISNULL(T1.numPromotionID,0) = @numTempPromotionID AND ISNULL(bitPromotionTriggered,0)=1 AND ISNULL(bitPromotionDiscount,0)=0) 
										THEN (CASE WHEN (CASE WHEN PO.tintOfferTriggerValueType = 2 THEN ISNULL(monTotalAmount,0) ELSE ISNULL(numUnits,0) END) >= ISNULL(PO.fltOfferTriggerValue,0) THEN 1 ELSE 0 END) 
										ELSE 1 
									END)
						AND 1 = (CASE 
									WHEN @tintDiscoutBaseOn = 1 -- Selected Items
									THEN
										(CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=@numTempPromotionID AND tintRecordType=6 AND tintType=1 AND numValue=I.numItemCode) > 0 THEN 1 ELSE 0 END)
									WHEN @tintDiscoutBaseOn = 2 -- Selected Item Classification
									THEN 
										(CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=@numTempPromotionID AND tintRecordType=6 AND tintType=2 AND numValue=ISNULL(I.numItemClassification,0)) > 0 THEN 1 ELSE 0 END)
									WHEN @tintDiscoutBaseOn = 3 -- Related Items
									THEN
										(CASE WHEN (SELECT COUNT(*) FROM SimilarItems WHERE numDomainId=@numDomainID AND numParentItemCode=@numItemCode AND numItemCode=I.numItemCode) > 0 THEN 1 ELSE 0 END)
									WHEN @tintDiscoutBaseOn IN  (4,5) -- Item with list price lesser or equal
									THEN
										(CASE WHEN ISNULL(I.monListPrice,0) <= ISNULL((SELECT monListPrice FROM Item WHERE numItemCode=@numItemCode),0) AND ISNULL(T1.bitPromotionTriggered,0)=0 THEN 1 ELSE 0 END)
									WHEN @tintDiscoutBaseOn = 6 -- Selected Items
									THEN
										(CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=@numTempPromotionID AND tintRecordType=6 AND tintType=4 AND numValue=I.numItemCode) > 0 THEN 1 ELSE 0 END)
									ELSE 0
								END)
				END

				SET @i = @i + 1
			END

			SET @j = @j + 1
		END

		-- IF ANY COUPON BASE PROMOTION IS TRIGERRED BUT THERE ARE NO ITEMS TO APPLY PROMOTION DISCOUNT THEN CLEAR PROMOTIO TRIGGER
		IF (SELECT 
				COUNT(*) 
			FROM 
				@TEMP T1 
			INNER JOIN
				PromotionOffer
			ON
				T1.numPromotionID=PromotionOffer.numProId
				AND ISNULL(PromotionOffer.bitUseOrderPromotion,0) = 1
			WHERE 
				ISNULL(T1.numPromotionID,0) > 0
				AND ISNULL(T1.bitPromotionTriggered,0)=1 
				AND ISNULL(T1.bitPromotionDiscount,0)=0 
				AND (SELECT COUNT(*) FROM @TEMP T2 WHERE T2.numPromotionID=T1.numPromotionID AND ISNULL(bitPromotionTriggered,0) = 0) = 0) > 0
		BEGIN
			UPDATE
				T1
			SET
				T1.numPromotionID = 0
				,T1.bitPromotionTriggered=0
				,T1.bitPromotionDiscount=0
				,T1.vcPromotionDescription=''
				,T1.bitChanged=1
				,T1.monUnitPrice=ISNULL(T2.monPrice,0)
				,T1.fltDiscount=ISNULL(T2.decDiscount,0)
				,T1.bitDiscountType=ISNULL(T2.tintDisountType,0)
			FROM 
				@TEMP T1 
			INNER JOIN
				PromotionOffer
			ON
				T1.numPromotionID=PromotionOffer.numProId
				AND ISNULL(PromotionOffer.bitUseOrderPromotion,0) = 1
			CROSS APPLY
				dbo.fn_GetItemPriceBasedOnMethod(@numDomainID,@numDivisionID,T1.numItemCode,T1.numUnits,0,0,T1.numWarehouseItemID,T1.vcKitChildItems,@numCurrencyID) T2
			WHERE 
				ISNULL(T1.numPromotionID,0) > 0
				AND ISNULL(T1.bitPromotionTriggered,0)=1 
				AND ISNULL(T1.bitPromotionDiscount,0)=0 
				AND (SELECT COUNT(*) FROM @TEMP T2 WHERE T2.numPromotionID=T1.numPromotionID AND ISNULL(bitPromotionTriggered,0) = 0) = 0

			SET @j = 1
			SET @jCount = ISNULL((SELECT COUNT(*) FROM @TEMPPromotion),0)

			WHILE @j <= @jCount
			BEGIN
				SELECT 
					@numTempPromotionID=numPromotionID
					,@numItemCode=numTriggerItemCode
					,@numTempWarehouseItemID=numWarehouseItemID
					,@numUnits = numUnits
					,@monTotalAmount=monTotalAmount
				FROM 
					@TEMPPromotion 
				WHERE 
					ID = @j

				SELECT
					@fltDiscountValue=ISNULL(fltDiscountValue,0)
					,@tintDiscountType=ISNULL(tintDiscountType,0)
					,@tintDiscoutBaseOn=tintDiscoutBaseOn
					,@vcShortDesc=ISNULL(vcShortDesc,'')
					,@tintItemCalDiscount=ISNULL(tintItemCalDiscount,1)
				FROM
					PromotionOffer
				WHERE
					numProId=@numTempPromotionID

				-- If discount is based on amount or quantity than we have to checked remaining amount or qty left to give as discount 
				IF @tintDiscountType = 2 OR @tintDiscountType = 3
				BEGIN
					SET @bitRemainingCheckRquired = 1
				END
				ELSE
				BEGIN
					SET @bitRemainingCheckRquired = 0
					SET @numRemainingPromotion = 0
				END

				-- If promotion is valid than check whether any item left to apply promotion
				SET @i = 1

				WHILE @i <= @iCount
				BEGIN
					IF @bitRemainingCheckRquired=1
					BEGIN
						IF @tintDiscountType = 2 -- Discount by amount
						BEGIN
							SET @numRemainingPromotion = @fltDiscountValue - ISNULL((SELECT SUM(fltDiscount) FROM @TEMP WHERE numPromotionID = @numTempPromotionID	AND ISNULL(bitDiscountType,0)=1 AND bitPromotionDiscount = 1),0)
						END
						ELSE IF @tintDiscountType = 3 -- Discount by quantity
						BEGIN
							IF @tintDiscoutBaseOn IN (5,6)
							BEGIN
								SET @numRemainingPromotion = @fltDiscountValue - ISNULL((SELECT SUM(numUnits) FROM @TEMP WHERE numPromotionID = @numTempPromotionID	AND ISNULL(bitDiscountType,0)=1 AND bitPromotionDiscount = 1),0)
							END
							ELSE
							BEGIN
								SET @numRemainingPromotion = @fltDiscountValue - ISNULL((SELECT SUM(fltDiscount/monUnitPrice) FROM @TEMP WHERE numPromotionID = @numTempPromotionID	AND ISNULL(bitDiscountType,0)=1 AND bitPromotionDiscount = 1),0)
							END
						END
					END

					IF @bitRemainingCheckRquired=0 OR (@bitRemainingCheckRquired=1 AND @numRemainingPromotion > 0)
					BEGIN
						UPDATE
							T1
						SET
							monUnitPrice= CASE 
												WHEN (ISNULL(bitKitParent,0)=1 OR ISNULL(bitAssembly,0) = 1) AND ISNULL(bitCalAmtBasedonDepItems,0) = 1
												THEN (dbo.GetCalculatedPriceForKitAssembly(@numDomainID,@numDivisionID,I.numItemCode,T1.numUnits,@numTempWarehouseItemID,ISNULL(tintKitAssemblyPriceBasedOn,1),0,0,ISNULL(T1.vcKitChildItems,''),@numCurrencyID,@fltExchangeRate) * @fltExchangeRate)
												ELSE (CASE WHEN @tintItemCalDiscount = 1 THEN (ISNULL(T2.monFinalPrice,0) * @fltExchangeRate) ELSE ISNULL(I.monListPrice,0) END)
											END
							,fltDiscount = (CASE 
												WHEN @bitRemainingCheckRquired=0 
												THEN 
													@fltDiscountValue
												ELSE 
													(CASE 
														WHEN @tintDiscountType = 2 -- Discount by amount
														THEN
															(CASE 
																WHEN (T1.numUnits * (CASE 
																						WHEN (ISNULL(bitKitParent,0)=1 OR ISNULL(bitAssembly,0) = 1) AND ISNULL(bitCalAmtBasedonDepItems,0) = 1
																						THEN (dbo.GetCalculatedPriceForKitAssembly(@numDomainID,@numDivisionID,I.numItemCode,T1.numUnits,@numTempWarehouseItemID,ISNULL(tintKitAssemblyPriceBasedOn,1),0,0,ISNULL(T1.vcKitChildItems,''),@numCurrencyID,@fltExchangeRate) * @fltExchangeRate)
																						ELSE (CASE WHEN @tintItemCalDiscount = 1 THEN (ISNULL(T2.monFinalPrice,0) * @fltExchangeRate) ELSE ISNULL(I.monListPrice,0) END)
																					END)) >= @numRemainingPromotion 
																THEN @numRemainingPromotion 
																ELSE (T1.numUnits * (CASE 
																									WHEN (ISNULL(bitKitParent,0)=1 OR ISNULL(bitAssembly,0) = 1) AND ISNULL(bitCalAmtBasedonDepItems,0) = 1
																									THEN (dbo.GetCalculatedPriceForKitAssembly(@numDomainID,@numDivisionID,I.numItemCode,T1.numUnits,@numTempWarehouseItemID,ISNULL(tintKitAssemblyPriceBasedOn,1),0,0,ISNULL(T1.vcKitChildItems,''),@numCurrencyID,@fltExchangeRate) * @fltExchangeRate)
																									ELSE (CASE WHEN @tintItemCalDiscount = 1 THEN (ISNULL(T2.monFinalPrice,0) * @fltExchangeRate) ELSE ISNULL(I.monListPrice,0) END)
																								END)) 
															END)
														WHEN @tintDiscountType = 3 -- Discount by quantity
														THEN
															(CASE 
																WHEN @tintDiscoutBaseOn IN (5,6)
																THEN
																	(CASE 
																		WHEN (CASE WHEN (ISNULL(T1.numPromotionID,0) = @numTempPromotionID AND ISNULL(bitPromotionTriggered,0)=1 AND ISNULL(bitPromotionDiscount,0)=0) 
																				THEN T1.numUnits - ISNULL(PO.fltOfferTriggerValue,0) 
																				ELSE T1.numUnits
																			END) >= @numRemainingPromotion 
																		THEN @numRemainingPromotion 
																		ELSE (CASE WHEN (ISNULL(T1.numPromotionID,0) = @numTempPromotionID AND ISNULL(bitPromotionTriggered,0)=1 AND ISNULL(bitPromotionDiscount,0)=0) 
																				THEN T1.numUnits - ISNULL(PO.fltOfferTriggerValue,0) 
																				ELSE T1.numUnits
																			END )
																	END) * (CASE 
																				WHEN (CASE WHEN @tintItemCalDiscount = 1 THEN (ISNULL(T2.monFinalPrice,0) * @fltExchangeRate) ELSE ISNULL(I.monListPrice,0) END) > @monDiscountedItemPrice
																				THEN ((CASE WHEN @tintItemCalDiscount = 1 THEN (ISNULL(T2.monFinalPrice,0) * @fltExchangeRate) ELSE ISNULL(I.monListPrice,0) END) - @monDiscountedItemPrice)
																				ELSE 
																					(CASE WHEN @tintItemCalDiscount = 1 THEN (ISNULL(T2.monFinalPrice,0) * @fltExchangeRate) ELSE ISNULL(I.monListPrice,0) END) 
																			END)
																ELSE
																	(CASE 
																		WHEN (CASE WHEN (ISNULL(T1.numPromotionID,0) = @numTempPromotionID AND ISNULL(bitPromotionTriggered,0)=1 AND ISNULL(bitPromotionDiscount,0)=0) 
																		THEN T1.numUnits - ISNULL(PO.fltOfferTriggerValue,0) 
																		ELSE T1.numUnits
																	END) >= @numRemainingPromotion 
																		THEN (@numRemainingPromotion * (CASE 
																											WHEN (ISNULL(bitKitParent,0)=1 OR ISNULL(bitAssembly,0) = 1) AND ISNULL(bitCalAmtBasedonDepItems,0) = 1
																											THEN (dbo.GetCalculatedPriceForKitAssembly(@numDomainID,@numDivisionID,I.numItemCode,T1.numUnits,@numTempWarehouseItemID,ISNULL(tintKitAssemblyPriceBasedOn,1),0,0,ISNULL(T1.vcKitChildItems,''),@numCurrencyID,@fltExchangeRate) * @fltExchangeRate)
																											ELSE (CASE WHEN @tintItemCalDiscount = 1 THEN (ISNULL(T2.monFinalPrice,0) * @fltExchangeRate) ELSE ISNULL(I.monListPrice,0) END)
																										END)) 
																		ELSE ((CASE WHEN (ISNULL(T1.numPromotionID,0) = @numTempPromotionID AND ISNULL(bitPromotionTriggered,0)=1 AND ISNULL(bitPromotionDiscount,0)=0) 
																		THEN T1.numUnits - ISNULL(PO.fltOfferTriggerValue,0) 
																		ELSE T1.numUnits
																	END) * (CASE 
																								WHEN (ISNULL(bitKitParent,0)=1 OR ISNULL(bitAssembly,0) = 1) AND ISNULL(bitCalAmtBasedonDepItems,0) = 1
																								THEN (dbo.GetCalculatedPriceForKitAssembly(@numDomainID,@numDivisionID,I.numItemCode,T1.numUnits,@numTempWarehouseItemID,ISNULL(tintKitAssemblyPriceBasedOn,1),0,0,ISNULL(T1.vcKitChildItems,''),@numCurrencyID,@fltExchangeRate) * @fltExchangeRate)
																								ELSE (CASE WHEN @tintItemCalDiscount = 1 THEN (ISNULL(T2.monFinalPrice,0) * @fltExchangeRate) ELSE ISNULL(I.monListPrice,0) END)
																							END)) 
																	END)
															END)
														ELSE 0
													END)
												END
											)
							,bitDiscountType = (CASE WHEN @bitRemainingCheckRquired=0 THEN 0 ELSE 1 END)
							,numPromotionID=@numTempPromotionID
							,bitPromotionTriggered=(CASE WHEN bitPromotionTriggered=1 THEN 1 ELSE 0 END)
							,vcPromotionDescription=@vcShortDesc
							,bitChanged=1
							,bitPromotionDiscount=1
						FROM
							@TEMP T1
						INNER JOIN
							Item I
						ON
							T1.numItemCode = I.numItemCode
						LEFT JOIN
							PromotionOffer PO
						ON
							T1.numPromotionID = PO.numProId
						CROSS APPLY
							dbo.fn_GetItemPriceBasedOnMethod(@numDomainID,@numDivisionID,T1.numItemCode,T1.numUnits,0,0,T1.numWarehouseItemID,T1.vcKitChildItems,@numCurrencyID) T2
						WHERE
							ID=@i
							AND (ISNULL(T1.numPromotionID,0) = 0 OR (ISNULL(T1.numPromotionID,0) = @numTempPromotionID AND ISNULL(bitPromotionDiscount,0)=0))
							AND 1 = (CASE 
										WHEN (ISNULL(T1.numPromotionID,0) = @numTempPromotionID AND ISNULL(bitPromotionDiscount,0)=0) 
										THEN (CASE WHEN (CASE WHEN PO.tintOfferTriggerValueType = 2 THEN ISNULL(monTotalAmount,0) ELSE ISNULL(numUnits,0) END) >= ISNULL(PO.fltOfferTriggerValue,0) THEN 1 ELSE 0 END) 
										ELSE 1 
									END)
							AND 1 = (CASE 
										WHEN @tintDiscoutBaseOn = 1 -- Selected Items
										THEN
											(CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=@numTempPromotionID AND tintRecordType=6 AND tintType=1 AND numValue=I.numItemCode) > 0 THEN 1 ELSE 0 END)
										WHEN @tintDiscoutBaseOn = 2 -- Selected Item Classification
										THEN 
											(CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=@numTempPromotionID AND tintRecordType=6 AND tintType=2 AND numValue=ISNULL(I.numItemClassification,0)) > 0 THEN 1 ELSE 0 END)
										WHEN @tintDiscoutBaseOn = 3 -- Related Items
										THEN
											(CASE WHEN (SELECT COUNT(*) FROM SimilarItems WHERE numDomainId=@numDomainID AND numParentItemCode=@numItemCode AND numItemCode=I.numItemCode) > 0 THEN 1 ELSE 0 END)
										WHEN @tintDiscoutBaseOn IN (4,5) -- Item with list price lesser or equal
										THEN
											(CASE WHEN ISNULL(I.monListPrice,0) <= ISNULL((SELECT monListPrice FROM Item WHERE numItemCode=@numItemCode),0) AND ISNULL(T1.bitPromotionTriggered,0)=0 THEN 1 ELSE 0 END)
										WHEN @tintDiscoutBaseOn = 6 -- Selected Items
										THEN
											(CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=@numTempPromotionID AND tintRecordType=6 AND tintType=4 AND numValue=I.numItemCode) > 0 THEN 1 ELSE 0 END)
										ELSE 0
									END)
					END

					SET @i = @i + 1
				END

				SET @j = @j + 1
			END
		END
	END

	UPDATE 
		@TEMP 
	SET 
		monUnitPrice = (CASE WHEN @fltExchangeRate <> 1 THEN CAST((ISNULL(monUnitPrice,0) / @fltExchangeRate) AS INT) ELSE ISNULL(monUnitPrice,0) END) 
		,fltDiscount = (CASE WHEN ISNULL(bitDiscountType,0) = 0 THEN (CASE WHEN @fltExchangeRate <> 1 THEN CAST((ISNULL(fltDiscount,0) / @fltExchangeRate) AS INT) ELSE fltDiscount END) ELSE fltDiscount END)
	WHERE 
		bitChanged=1

	SELECT * FROM @TEMP WHERE bitChanged=1
END
GO
/****** Object:  StoredProcedure [dbo].[usp_SetUsersWithDomains]    Script Date: 07/26/2008 16:21:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_setuserswithdomains')
DROP PROCEDURE usp_setuserswithdomains
GO
CREATE  PROCEDURE [dbo].[usp_SetUsersWithDomains]
	@numUserID NUMERIC(9),                                    
	@vcUserName VARCHAR(50),                                    
	@vcUserDesc VARCHAR(250),                              
	@numGroupId as numeric(9),                                           
	@numUserDetailID numeric ,                              
	@numUserCntID as numeric(9),                              
	@strTerritory as varchar(4000) ,                              
	@numDomainID as numeric(9),
	@strTeam as varchar(4000),
	@vcEmail as varchar(100),
	@vcPassword as varchar(100),          
	@Active as BIT,
	@numDefaultClass NUMERIC,
	@numDefaultWarehouse as numeric(18),
	@vcLinkedinId varchar(300)=null,
	@intAssociate int=0,
	@ProfilePic varchar(100)=null
	,@bitPayroll BIT = 0
	,@monHourlyRate DECIMAL(20,5) = 0
	,@tintPayrollType TINYINT = 0
	,@tintHourType TINYINT = 0
	,@monOverTimeRate DECIMAL(20,5) = 0
	,@bitOauthImap BIT = 0
	,@tintMailProvider TINYINT = 3
AS
BEGIN
	DECLARE @numNewFullUsers AS INT
	DECLARE @numNewLimitedAccessUsers AS INT

	SET @numNewFullUsers = ISNULL((SELECT
										COUNT(*)
									FROM
										UserMaster 
									INNER JOIN
										AuthenticationGroupMaster
									ON
										UserMaster.numGroupID = AuthenticationGroupMaster.numGroupID
									WHERE
										UserMaster.numDomainID = @numDomainID
										AND AuthenticationGroupMaster.tintGroupType=1
										AND ISNULL(UserMaster.bitActivateFlag,0)=1
										AND UserMaster.numUserId <> ISNULL(@numUserID,0)),0)

	SET @numNewLimitedAccessUsers = ISNULL((SELECT
										COUNT(*)
									FROM
										UserMaster 
									INNER JOIN
										AuthenticationGroupMaster
									ON
										UserMaster.numGroupID = AuthenticationGroupMaster.numGroupID
									WHERE
										UserMaster.numDomainID = @numDomainID
										AND AuthenticationGroupMaster.tintGroupType=4
										AND ISNULL(UserMaster.bitActivateFlag,0)=1
										AND UserMaster.numUserId <> ISNULL(@numUserID,0)),0)
		
	IF ISNULL(@Active,0)=1 AND ISNULL((SELECT tintGroupType FROM AuthenticationGroupMaster WHERE numGroupID=@numGroupId),0)=1 AND EXISTS (SELECT numSubscriberID FROM Subscribers WHERE numTargetDomainID=@numDomainID AND (@numNewFullUsers + 1) > ISNULL(intNoofUsersSubscribed,0))
	BEGIN
		RAISERROR('FULL_USERS_EXCEED',16,1)
		RETURN
	END
	ELSE IF ISNULL(@Active,0)=1 AND ISNULL((SELECT tintGroupType FROM AuthenticationGroupMaster WHERE numGroupID=@numGroupId),0)=4 AND EXISTS (SELECT numSubscriberID FROM Subscribers WHERE numTargetDomainID=@numDomainID AND (@numNewLimitedAccessUsers + 1) > ISNULL(intNoofLimitedAccessUsers,0))
	BEGIN
		RAISERROR('LIMITED_ACCESS_USERS_EXCEED',16,1)
		RETURN
	END

	IF @numUserID=0             
	BEGIN 
		DECLARE @APIPublicKey varchar(20)
		SELECT @APIPublicKey = dbo.GenerateBizAPIPublicKey() 
           
		INSERT INTO UserMaster
		(
			vcUserName
			,vcUserDesc
			,numGroupId
			,numUserDetailId
			,numModifiedBy
			,bintModifiedDate
			,vcEmailID
			,vcPassword
			,numDomainID
			,numCreatedBy
			,bintCreatedDate
			,bitActivateFlag
			,numDefaultClass
			,numDefaultWarehouse
			,vcBizAPIPublicKey
			,vcBizAPISecretKey
			,bitBizAPIAccessEnabled
			,vcLinkedinId
			,intAssociate
			,bitPayroll
			,monHourlyRate
			,tintPayrollType
			,tintHourType
			,monOverTimeRate
			,bitOauthImap
			,tintMailProvider
		)
		VALUES
		(
			@vcUserName
			,@vcUserDesc
			,@numGroupId
			,@numUserDetailID
			,@numUserCntID
			,GETUTCDATE()
			,@vcEmail
			,@vcPassword
			,@numDomainID
			,@numUserCntID
			,GETUTCDATE()
			,@Active
			,@numDefaultClass
			,@numDefaultWarehouse
			,@APIPublicKey
			,NEWID()
			,0
			,@vcLinkedinId
			,@intAssociate
			,@bitPayroll
			,@monHourlyRate
			,@tintPayrollType
			,@tintHourType
			,(CASE WHEN ISNULL(@monOverTimeRate,0) = 0 THEN @monHourlyRate ELSE @monOverTimeRate END)
			,@bitOauthImap
			,ISNULL(@tintMailProvider,3)
		)            
		
		SET @numUserID=@@identity          
           
		INSERT INTO BizAPIThrottlePolicy 
		(
			[RateLimitKey],
			[bitIsIPAddress],
			[numPerSecond],
			[numPerMinute],
			[numPerHour],
			[numPerDay],
			[numPerWeek]
		)
		VALUES
		(
			@APIPublicKey,
			0,
			3,
			60,
			1200,
			28800,
			200000
		)
    
		EXECUTE [dbo].[Resource_Add]   @vcUserName  ,@vcUserDesc  ,@vcEmail  ,1  ,@numDomainID  ,@numUserDetailID  
	END
	ELSE            
	BEGIN
		UPDATE 
			UserMaster 
		SET 
			vcUserName = @vcUserName,                                    
			vcUserDesc = @vcUserDesc,                              
			numGroupId =@numGroupId,                                    
			numUserDetailId = @numUserDetailID ,                              
			numModifiedBy= @numUserCntID ,                              
			bintModifiedDate= getutcdate(),                          
			vcEmailID=@vcEmail,            
			vcPassword=@vcPassword,          
			bitActivateFlag=@Active,
			numDefaultClass=@numDefaultClass,
			numDefaultWarehouse=@numDefaultWarehouse,
			vcLinkedinId=@vcLinkedinId,
			intAssociate=@intAssociate
			,bitPayroll=@bitPayroll
			,monHourlyRate=@monHourlyRate
			,tintPayrollType=@tintPayrollType
			,tintHourType=@tintHourType
			,monOverTimeRate=(CASE WHEN ISNULL(@monOverTimeRate,0) = 0 THEN @monHourlyRate ELSE @monOverTimeRate END)
			,bitOauthImap=@bitOauthImap
			,tintMailProvider = ISNULL(@tintMailProvider,3)
		WHERE 
			numUserID = @numUserID          
          
  
		IF NOT EXISTS (SELECT * FROM RESOURCE WHERE numUserCntId = @numUserDetailID and numdomainId = @numDomainId)  
		BEGIN   
			EXECUTE [dbo].[Resource_Add]   @vcUserName  ,@vcUserDesc  ,@vcEmail  ,1  ,@numDomainID  ,@numUserDetailID     
		END      
	END
                       
	DECLARE @separator_position AS INTEGER
	DECLARE @strPosition AS VARCHAR(1000)                 
                                   
	DELETE FROM UserTerritory WHERE numUserCntID = @numUserDetailID and numDomainId=@numDomainID                              
          
	WHILE PATINDEX('%,%' , @strTerritory) <> 0                              
    BEGIN -- Begin for While Loop                              
		SELECT @separator_position = PATINDEX('%,%' , @strTerritory)                        
		SELECT @strPosition = LEFT(@strTerritory, @separator_position - 1)      
		SELECT @strTerritory = STUFF(@strTerritory, 1, @separator_position,'')                              
     
		INSERT INTO UserTerritory 
		(
			numUserCntID
			,numTerritoryID
			,numDomainID
		)                              
		VALUES
		(
			@numUserDetailID
			,@strPosition
			,@numDomainID
		)                        
	END                              
                                           
	DELETE FROM UserTeams WHERE numUserCntID = @numUserDetailID AND numDomainId=@numDomainID                              
                               
	WHILE PATINDEX('%,%' , @strTeam) <> 0                              
    BEGIN -- Begin for While Loop                              
		SELECT @separator_position =  PATINDEX('%,%' , @strTeam)                              
		SELECT @strPosition = left(@strTeam, @separator_position - 1)                  
		SELECT @strTeam = stuff(@strTeam, 1, @separator_position,'')                              
     
		INSERT INTO UserTeams 
		(
			numUserCntID
			,numTeam
			,numDomainID
		)                              
		VALUES 
		(
			@numUserDetailID
			,@strPosition
			,@numDomainID
		)                        
	END              
                
                                      
                              
	DELETE FROM 
		ForReportsByTeam 
	WHERE 
		numUserCntID = @numUserDetailID                              
		AND numDomainId=@numDomainID AND numTeam NOT IN (SELECT numTeam FROM UserTeams WHERE numUserCntID = @numUserCntID AND numDomainId=@numDomainID)                                                
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_Sites_GetCategoryFilters')
DROP PROCEDURE dbo.USP_Sites_GetCategoryFilters
GO
CREATE PROCEDURE [dbo].[USP_Sites_GetCategoryFilters]
(
	@numDomainID NUMERIC(18,0)
	,@numCategoryID NUMERIC(18,0)
)
AS 
BEGIN
	SELECT
		CFW_Fld_Master.Fld_id
		,CFW_Fld_Master.Fld_label
		,CFW_Fld_Master.numlistid
	FROM
		Category
	INNER JOIN
		CategoryMatrixGroup 
	ON
		Category.numCategoryID = CategoryMatrixGroup.numCategoryID
	INNER JOIN
		ItemGroups
	ON
		CategoryMatrixGroup.numItemGroup = ItemGroups.numItemGroupID
	INNER JOIN
		ItemGroupsDTL
	ON
		ItemGroups.numItemGroupID = ItemGroupsDTL.numItemGroupID
		AND ItemGroupsDTL.tintType = 2
	INNER JOIN
		CFW_Fld_Master
	ON
		ItemGroupsDTL.numOppAccAttrID=CFW_Fld_Master.Fld_id    
	WHERE
		Category.numDomainID = @numDomainID
		AND Category.numCategoryID = @numCategoryID
	GROUP BY
		CFW_Fld_Master.Fld_id
		,CFW_Fld_Master.Fld_label
		,CFW_Fld_Master.numlistid
END
GO
/****** Object:  StoredProcedure [dbo].[USP_UpdatedomainDetails]    Script Date: 05/07/2009 21:56:59 ******/
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                                      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_updatedomaindetails')
DROP PROCEDURE usp_updatedomaindetails
GO
CREATE PROCEDURE [dbo].[USP_UpdatedomainDetails]                                      
@numDomainID as numeric(9)=0,                                      
@vcDomainName as varchar(50)='',                                      
@vcDomainDesc as varchar(100)='',                                      
@bitExchangeIntegration as bit,                                      
@bitAccessExchange as bit,                                      
@vcExchUserName as varchar(100)='',                                      
@vcExchPassword as varchar(100)='',                                      
@vcExchPath as varchar(200)='',                                      
@vcExchDomain as varchar(100)='',                                    
@tintCustomPagingRows as tinyint,                                    
@vcDateFormat as varchar(30)='',                                    
@numDefCountry as numeric(9),                                    
@tintComposeWindow as tinyint,                                    
@sintStartDate as smallint,                                    
@sintNoofDaysInterval as smallint,                                    
@tintAssignToCriteria as tinyint,                                    
@bitIntmedPage as bit,                                    
@tintFiscalStartMonth as tinyint,                                      
@tintPayPeriod as tinyint,                        
@numCurrencyID as numeric(9) ,                
@charUnitSystem as char(1)='',              
@vcPortalLogo as varchar(100)='',            
@tintChrForComSearch as tinyint  ,            
@intPaymentGateWay as int, -- DO NOT UPDATE VALUE FROM THIS PARAMETER
@tintChrForItemSearch as tinyint,
@ShipCompany as numeric(9),
@bitMultiCurrency as bit,
@bitCreateInvoice AS tinyint = null,
@numDefaultSalesBizDocID AS NUMERIC(9)=NULL,
@numDefaultPurchaseBizDocID AS NUMERIC(9)=NULL,
@bitSaveCreditCardInfo AS BIT = NULL,
@intLastViewedRecord AS INT=10,
@tintCommissionType AS TINYINT,
@tintBaseTax AS TINYINT,
@bitEmbeddedCost AS BIT,
@numDefaultSalesOppBizDocId NUMERIC=NULL,
@tintSessionTimeOut TINYINT=1,
@tintDecimalPoints TINYINT,
@bitCustomizePortal BIT=0,
@tintBillToForPO TINYINT,
@tintShipToForPO TINYINT,
@tintOrganizationSearchCriteria TINYINT,
@bitDocumentRepositary BIT=0,
@tintComAppliesTo TINYINT=0,
@bitGtoBContact BIT=0,
@bitBtoGContact BIT=0,
@bitGtoBCalendar BIT=0,
@bitBtoGCalendar BIT=0,
@bitExpenseNonInventoryItem BIT=0,
@bitInlineEdit BIT=0,
@bitRemoveVendorPOValidation BIT=0,
@tintBaseTaxOnArea tinyint=0,
@bitAmountPastDue BIT=0,
@monAmountPastDue DECIMAL(20,5),
@bitSearchOrderCustomerHistory BIT=0,
@tintCalendarTimeFormat as tinyint=NULL,
@tintDefaultClassType as tinyint=NULL,
@tintPriceBookDiscount as tinyint=0,
@bitAutoSerialNoAssign as bit=0,
@bitIsShowBalance AS BIT = 0,
@numIncomeAccID NUMERIC(18, 0) = 0,
@numCOGSAccID NUMERIC(18, 0) = 0,
@numAssetAccID NUMERIC(18, 0) = 0,
@IsEnableProjectTracking AS BIT = 0,
@IsEnableCampaignTracking AS BIT = 0,
@IsEnableResourceScheduling AS BIT = 0,
@ShippingServiceItem  AS NUMERIC(9)=0,
@numSOBizDocStatus AS NUMERIC(9)=0,
@bitAutolinkUnappliedPayment AS BIT=0,
@numDiscountServiceItemID AS NUMERIC(9)=0,
@vcShipToPhoneNumber as VARCHAR(50)='',
@vcGAUserEmailId as VARCHAR(50)='',
@vcGAUserPassword as VARCHAR(50)='',
@vcGAUserProfileId as VARCHAR(50)='',
@bitDiscountOnUnitPrice AS BIT=0,
@intShippingImageWidth INT = 0,
@intShippingImageHeight INT = 0,
@numTotalInsuredValue NUMERIC(10,2) = 0,
@numTotalCustomsValue NUMERIC(10,2) = 0,
@vcHideTabs VARCHAR(1000) = '',
@bitUseBizdocAmount BIT = 0,
@bitDefaultRateType BIT = 0,
@bitIncludeTaxAndShippingInCommission  BIT = 0,
@bitPurchaseTaxCredit BIT=0,
@bitLandedCost BIT=0,
@vcLanedCostDefault VARCHAR(10)='',
@bitMinUnitPriceRule BIT = 0,
@numAbovePercent AS NUMERIC(18,2),
@numBelowPercent AS NUMERIC(18,2),
@numBelowPriceField AS NUMERIC(18,0),
@vcUnitPriceApprover AS VARCHAR(100),
@numDefaultSalesPricing AS TINYINT,
@bitReOrderPoint AS BIT = 0,
@numReOrderPointOrderStatus AS NUMERIC(18,0) = 0,
@numPODropShipBizDoc AS NUMERIC(18,0) = 0,
@numPODropShipBizDocTemplate AS NUMERIC(18,0) = 0,
@IsEnableDeferredIncome AS BIT = 0,
@bitApprovalforTImeExpense AS BIT=0,
@numCost AS Numeric(9,0)=0,
@intTimeExpApprovalProcess int=0,
@bitApprovalforOpportunity AS BIT=0,
@intOpportunityApprovalProcess int=0,
@numDefaultSalesShippingDoc NUMERIC(18,0)=0,
@numDefaultPurchaseShippingDoc NUMERIC(18,0)=0,
@bitchkOverRideAssignto AS BIT=0,
@vcPrinterIPAddress VARCHAR(15) = '',
@vcPrinterPort VARCHAR(5) = '',
@numDefaultSiteID NUMERIC(18,0) = 0,
@bitRemoveGlobalLocation BIT = 0,
@numAuthorizePercentage NUMERIC(18,0)=0,
@bitEDI BIT = 0,
@bit3PL BIT = 0,
@bitAllowDuplicateLineItems BIT = 0,
@tintCommitAllocation TINYINT = 1,
@tintInvoicing TINYINT = 1,
@tintMarkupDiscountOption TINYINT = 1,
@tintMarkupDiscountValue TINYINT = 1,
@bitCommissionBasedOn BIT = 0,
@tintCommissionBasedOn TINYINT = 1,
@bitDoNotShowDropshipPOWindow BIT = 0,
@tintReceivePaymentTo TINYINT = 2,
@numReceivePaymentBankAccount NUMERIC(18,0) = 0,
@numOverheadServiceItemID NUMERIC(18,0) = 0,
@bitDisplayCustomField BIT = 0,
@bitFollowupAnytime BIT = 0,
@bitpartycalendarTitle BIT = 0,
@bitpartycalendarLocation BIT = 0,
@bitpartycalendarDescription BIT = 0,
@bitREQPOApproval BIT = 0,
@bitARInvoiceDue BIT = 0,
@bitAPBillsDue BIT = 0,
@bitItemsToPickPackShip BIT = 0,
@bitItemsToInvoice BIT = 0,
@bitSalesOrderToClose BIT = 0,
@bitItemsToPutAway BIT = 0,
@bitItemsToBill BIT = 0,
@bitPosToClose BIT = 0,
@bitPOToClose BIT = 0,
@bitBOMSToPick BIT = 0,
@vchREQPOApprovalEmp  VARCHAR(500)='',
@vchARInvoiceDue VARCHAR(500)='',
@vchAPBillsDue VARCHAR(500)='',
@vchItemsToPickPackShip VARCHAR(500)='',
@vchItemsToInvoice VARCHAR(500)='',
@vchPriceMarginApproval VARCHAR(500)='',
@vchSalesOrderToClose VARCHAR(500)='',
@vchItemsToPutAway VARCHAR(500)='',
@vchItemsToBill VARCHAR(500)='',
@vchPosToClose VARCHAR(500)='',
@vchPOToClose VARCHAR(500)='',
@vchBOMSToPick VARCHAR(500)='',
@decReqPOMinValue VARCHAR(500)='',
@bitUseOnlyActionItems BIT = 0,
@bitDisplayContractElement BIT,
@vcEmployeeForContractTimeElement VARCHAR(500)='',
@numARContactPosition NUMERIC(18,0) = 0,
@bitShowCardConnectLink BIT = 0,
@vcBluePayFormName VARCHAR(500)='',
@vcBluePaySuccessURL VARCHAR(500)='',
@vcBluePayDeclineURL VARCHAR(500)='',
@bitUseDeluxeCheckStock BIT = 1,
@bitEnableSmartyStreets BIT=0,
@vcSmartyStreetsAPIKeys VARCHAR(500)='',
@bitUsePreviousEmailBizDoc BIT = 0,
@bitInventoryInvoicing BIT=0
as                                      
BEGIN
BEGIN TRY
	IF ISNULL(@bitMinUnitPriceRule,0) = 1 AND ((SELECT COUNT(*) FROM UnitPriceApprover WHERE numDomainID=@numDomainID) = 0 OR ISNULL(@intOpportunityApprovalProcess,0) = 0)
	BEGIN
		RAISERROR('INCOMPLETE_MIN_UNIT_PRICE_CONFIGURATION',16,1)
		RETURN
	END

	If ISNULL(@bitApprovalforTImeExpense,0) = 1 AND (SELECT
														COUNT(*) 
													FROM
														UserMaster
													LEFT JOIN
														ApprovalConfig
													ON
														ApprovalConfig.numUserId = UserMaster.numUserDetailId
													WHERE
														UserMaster.numDomainID=@numDomainID
														AND numModule=1
														AND ISNULL(numGroupID,0) > 0
														AND ISNULL(bitActivateFlag,0) = 1
														AND ISNULL(numLevel1Authority,0) = 0
														AND ISNULL(numLevel2Authority,0) = 0
														AND ISNULL(numLevel3Authority,0) = 0
														AND ISNULL(numLevel4Authority,0) = 0
														AND ISNULL(numLevel5Authority,0) = 0) > 0
	BEGIN
		RAISERROR('INCOMPLETE_TIMEEXPENSE_APPROVAL',16,1)
		RETURN
	END

	DECLARE @tintCommitAllocationOld AS TINYINT
	SELECT @tintCommitAllocationOld=ISNULL(tintCommitAllocation,1) FROM Domain WHERE numDomainID=@numDomainID

	IF @tintCommitAllocation <> @tintCommitAllocationOld AND (SELECT COUNT(*) FROM OpportunityMaster WHERE numDomainId=@numDomainID AND tintOppType=1 AND tintOppStatus=1 AND ISNULL(tintshipped,0)=0) > 0
	BEGIN
		RAISERROR('NOT_ALLOWED_CHANGE_COMMIT_ALLOCATION_OPEN_SALES_ORDER',16,1)
		RETURN
	END
                                  
	UPDATE 
		Domain                                       
	SET                                       
		vcDomainName=@vcDomainName,                                      
		vcDomainDesc=@vcDomainDesc,                                      
		bitExchangeIntegration=@bitExchangeIntegration,                                      
		bitAccessExchange=@bitAccessExchange,                                      
		vcExchUserName=@vcExchUserName,                                      
		vcExchPassword=@vcExchPassword,                                      
		vcExchPath=@vcExchPath,                                      
		vcExchDomain=@vcExchDomain,                                    
		tintCustomPagingRows =@tintCustomPagingRows,                                    
		vcDateFormat=@vcDateFormat,                                    
		numDefCountry =@numDefCountry,                                    
		tintComposeWindow=@tintComposeWindow,                                    
		sintStartDate =@sintStartDate,                                    
		sintNoofDaysInterval=@sintNoofDaysInterval,                                    
		tintAssignToCriteria=@tintAssignToCriteria,                                    
		bitIntmedPage=@bitIntmedPage,                                    
		tintFiscalStartMonth=@tintFiscalStartMonth,                                                      
		tintPayPeriod=@tintPayPeriod,
		vcCurrency=isnull((select varCurrSymbol from Currency Where numCurrencyID=@numCurrencyID  ),'')  ,                      
		numCurrencyID=@numCurrencyID,                
		charUnitSystem= @charUnitSystem,              
		vcPortalLogo=@vcPortalLogo ,            
		tintChrForComSearch=@tintChrForComSearch  ,      
		tintChrForItemSearch=@tintChrForItemSearch,
		numShipCompany= @ShipCompany,
		bitMultiCurrency=@bitMultiCurrency,
		bitCreateInvoice= @bitCreateInvoice,
		[numDefaultSalesBizDocId] =@numDefaultSalesBizDocId,
		bitSaveCreditCardInfo=@bitSaveCreditCardInfo,
		intLastViewedRecord=@intLastViewedRecord,
		[tintCommissionType]=@tintCommissionType,
		[tintBaseTax]=@tintBaseTax,
		[numDefaultPurchaseBizDocId]=@numDefaultPurchaseBizDocID,
		bitEmbeddedCost=@bitEmbeddedCost,
		numDefaultSalesOppBizDocId=@numDefaultSalesOppBizDocId,
		tintSessionTimeOut=@tintSessionTimeOut,
		tintDecimalPoints=@tintDecimalPoints,
		bitCustomizePortal=@bitCustomizePortal,
		tintBillToForPO = @tintBillToForPO,
		tintShipToForPO = @tintShipToForPO,
		tintOrganizationSearchCriteria=@tintOrganizationSearchCriteria,
		bitDocumentRepositary=@bitDocumentRepositary,
		bitGtoBContact=@bitGtoBContact,
		bitBtoGContact=@bitBtoGContact,
		bitGtoBCalendar=@bitGtoBCalendar,
		bitBtoGCalendar=@bitBtoGCalendar,
		bitExpenseNonInventoryItem=@bitExpenseNonInventoryItem,
		bitInlineEdit=@bitInlineEdit,
		bitRemoveVendorPOValidation=@bitRemoveVendorPOValidation,
		tintBaseTaxOnArea=@tintBaseTaxOnArea,
		bitAmountPastDue = @bitAmountPastDue,
		monAmountPastDue = @monAmountPastDue,
		bitSearchOrderCustomerHistory=@bitSearchOrderCustomerHistory,
		tintCalendarTimeFormat=@tintCalendarTimeFormat,
		tintDefaultClassType=@tintDefaultClassType,
		tintPriceBookDiscount=@tintPriceBookDiscount,
		bitAutoSerialNoAssign=@bitAutoSerialNoAssign,
		bitIsShowBalance = @bitIsShowBalance,
		numIncomeAccID = @numIncomeAccID,
		numCOGSAccID = @numCOGSAccID,
		numAssetAccID = @numAssetAccID,
		IsEnableProjectTracking = @IsEnableProjectTracking,
		IsEnableCampaignTracking = @IsEnableCampaignTracking,
		IsEnableResourceScheduling = @IsEnableResourceScheduling,
		numShippingServiceItemID = @ShippingServiceItem,
		numSOBizDocStatus=@numSOBizDocStatus,
		bitAutolinkUnappliedPayment=@bitAutolinkUnappliedPayment,
		numDiscountServiceItemID=@numDiscountServiceItemID,
		vcShipToPhoneNo = @vcShipToPhoneNumber,
		vcGAUserEMail = @vcGAUserEmailId,
		vcGAUserPassword = @vcGAUserPassword,
		vcGAUserProfileId = @vcGAUserProfileId,
		bitDiscountOnUnitPrice=@bitDiscountOnUnitPrice,
		intShippingImageWidth = @intShippingImageWidth ,
		intShippingImageHeight = @intShippingImageHeight,
		numTotalInsuredValue = @numTotalInsuredValue,
		numTotalCustomsValue = @numTotalCustomsValue,
		vcHideTabs = @vcHideTabs,
		bitUseBizdocAmount = @bitUseBizdocAmount,
		bitDefaultRateType = @bitDefaultRateType,
		bitIncludeTaxAndShippingInCommission = @bitIncludeTaxAndShippingInCommission,
		bitPurchaseTaxCredit=@bitPurchaseTaxCredit,
		bitLandedCost=@bitLandedCost,
		vcLanedCostDefault=@vcLanedCostDefault,
		bitMinUnitPriceRule = @bitMinUnitPriceRule,
		numDefaultSalesPricing = @numDefaultSalesPricing,
		numPODropShipBizDoc=@numPODropShipBizDoc,
		numPODropShipBizDocTemplate=@numPODropShipBizDocTemplate,
		IsEnableDeferredIncome=@IsEnableDeferredIncome,
		bitApprovalforTImeExpense=@bitApprovalforTImeExpense,
		numCost= @numCost,intTimeExpApprovalProcess=@intTimeExpApprovalProcess,
		bitApprovalforOpportunity=@bitApprovalforOpportunity,intOpportunityApprovalProcess=@intOpportunityApprovalProcess,
		numDefaultPurchaseShippingDoc=@numDefaultPurchaseShippingDoc,numDefaultSalesShippingDoc=@numDefaultSalesShippingDoc,
		bitchkOverRideAssignto=@bitchkOverRideAssignto,
		vcPrinterIPAddress=@vcPrinterIPAddress,
		vcPrinterPort=@vcPrinterPort,
		numDefaultSiteID=ISNULL(@numDefaultSiteID,0),
		bitRemoveGlobalLocation=@bitRemoveGlobalLocation,
		numAuthorizePercentage=@numAuthorizePercentage,
		bitEDI=ISNULL(@bitEDI,0),
		bit3PL=ISNULL(@bit3PL,0),
		bitAllowDuplicateLineItems=ISNULL(@bitAllowDuplicateLineItems,0),
		tintCommitAllocation=ISNULL(@tintCommitAllocation,1),
		tintInvoicing = ISNULL(@tintInvoicing,1),
		tintMarkupDiscountOption = ISNULL(@tintMarkupDiscountOption,1),
		tintMarkupDiscountValue = ISNULL(@tintMarkupDiscountValue,1)
		,bitCommissionBasedOn=ISNULL(@bitCommissionBasedOn,0)
		,tintCommissionBasedOn=ISNULL(@tintCommissionBasedOn,1)
		,bitDoNotShowDropshipPOWindow=ISNULL(@bitDoNotShowDropshipPOWindow,0)
		,tintReceivePaymentTo=ISNULL(@tintReceivePaymentTo,2)
		,numReceivePaymentBankAccount = ISNULL(@numReceivePaymentBankAccount,0)
		,numOverheadServiceItemID=@numOverheadServiceItemID,
		bitDisplayCustomField=@bitDisplayCustomField,
		bitFollowupAnytime=@bitFollowupAnytime,
		bitpartycalendarTitle=@bitpartycalendarTitle,
		bitpartycalendarLocation=@bitpartycalendarLocation,
		bitpartycalendarDescription=@bitpartycalendarDescription,
		bitREQPOApproval=@bitREQPOApproval,
		bitARInvoiceDue=@bitARInvoiceDue,
		bitAPBillsDue=@bitAPBillsDue,
		bitItemsToPickPackShip=@bitItemsToPickPackShip,
		bitItemsToInvoice=@bitItemsToInvoice,
		bitSalesOrderToClose=@bitSalesOrderToClose,
		bitItemsToPutAway=@bitItemsToPutAway,
		bitItemsToBill=@bitItemsToBill,
		bitPosToClose=@bitPosToClose,
		bitPOToClose=@bitPOToClose,
		bitBOMSToPick=@bitBOMSToPick,
		vchREQPOApprovalEmp=@vchREQPOApprovalEmp,
		vchARInvoiceDue=@vchARInvoiceDue,
		vchAPBillsDue=@vchAPBillsDue,
		vchItemsToPickPackShip=@vchItemsToPickPackShip,
		vchItemsToInvoice=@vchItemsToInvoice,
		vchSalesOrderToClose=@vchSalesOrderToClose,
		vchItemsToPutAway=@vchItemsToPutAway,
		vchItemsToBill=@vchItemsToBill,
		vchPosToClose=@vchPosToClose,
		vchPOToClose=@vchPOToClose,
		vchBOMSToPick=@vchBOMSToPick,
		decReqPOMinValue=@decReqPOMinValue,
		bitUseOnlyActionItems = @bitUseOnlyActionItems,
		bitDisplayContractElement = @bitDisplayContractElement,
		vcEmployeeForContractTimeElement=@vcEmployeeForContractTimeElement,
		numARContactPosition= @numARContactPosition,
		bitShowCardConnectLink = @bitShowCardConnectLink,
		vcBluePayFormName=@vcBluePayFormName,
		vcBluePaySuccessURL=@vcBluePaySuccessURL,
		vcBluePayDeclineURL=@vcBluePayDeclineURL,
		bitUseDeluxeCheckStock=@bitUseDeluxeCheckStock,
		bitEnableSmartyStreets = @bitEnableSmartyStreets,
		vcSmartyStreetsAPIKeys = @vcSmartyStreetsAPIKeys,
		bitUsePreviousEmailBizDoc=@bitUsePreviousEmailBizDoc,
		bitInventoryInvoicing=@bitInventoryInvoicing
	WHERE 
		numDomainId=@numDomainID

	IF(LEN(@vchPriceMarginApproval)>0)
	BEGIN
		DELETE FROM UnitPriceApprover WHERE numDomainID = @numDomainID

		INSERT INTO UnitPriceApprover
			(
				numDomainID,
				numUserID
			)SELECT @numDomainID,Items FROM Split(@vchPriceMarginApproval,',') WHERE Items<>''
	END
	IF ISNULL(@bitRemoveGlobalLocation,0) = 1
	BEGIN
		DECLARE @TEMPGlobalWarehouse TABLE
		(
			ID INT IDENTITY(1,1)
			,numItemCode NUMERIC(18,0)
			,numWarehouseID NUMERIC(18,0)
			,numWarehouseItemID NUMERIC(28,0)
		)

		INSERT INTO @TEMPGlobalWarehouse
		(
			numItemCode
			,numWarehouseID
			,numWarehouseItemID
		)
		SELECT
			numItemID
			,numWareHouseID
			,numWareHouseItemID
		FROM
			WareHouseItems
		WHERE
			numDomainID=@numDomainID
			AND numWLocationID = -1
			AND ISNULL(numOnHand,0) = 0
			AND ISNULL(numOnOrder,0)=0
			AND ISNULL(numAllocation,0)=0
			AND ISNULL(numBackOrder,0)=0


		DECLARE @i AS INT = 1
		DECLARE @iCount AS INT
		DECLARE @numTempItemCode AS NUMERIC(18,0)
		DECLARE @numTempWareHouseID AS NUMERIC(18,0)
		DECLARE @numTempWareHouseItemID AS NUMERIC(18,0)
		SELECT @iCount=COUNT(*) FROM @TEMPGlobalWarehouse

		WHILE @i <= @iCount
		BEGIN
			SELECT @numTempItemCode=numItemCode,@numTempWareHouseID=numWarehouseID,@numTempWareHouseItemID=numWarehouseItemID FROM @TEMPGlobalWarehouse WHERE ID=@i

			BEGIN TRY
				EXEC USP_AddUpdateWareHouseForItems @numTempItemCode,  
													@numTempWareHouseID,
													@numTempWareHouseItemID,
													'',
													0,
													0,
													0,
													'',
													'',
													@numDomainID,
													'',
													'',
													'',
													0,
													3,
													0,
													0,
													NULL,
													0
			END TRY
			BEGIN CATCH

			END CATCH

			SET @i = @i + 1
		END
	END
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH

END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_UpdatingInventoryForEmbeddedKits')
DROP PROCEDURE USP_UpdatingInventoryForEmbeddedKits
GO
CREATE PROCEDURE [dbo].[USP_UpdatingInventoryForEmbeddedKits]    
	@numOppItemID AS NUMERIC(18,0),
	@Units AS FLOAT,
	@Mode AS TINYINT,
	@numOppID AS NUMERIC(9),
	@tintMode AS TINYINT=0, -- 0:Add/Edit,1:Delete,2: DEMOTED TO OPPORTUNITY
	@numUserCntID AS NUMERIC(9)
AS                            
BEGIN

	DECLARE @numDomain AS NUMERIC(18,0)
	DECLARE @tintOppType AS TINYINT = 0

	SELECT 
		@numDomain = numDomainID, 
		@tintOppType=tintOppType 
	FROM 
		OpportunityMaster
	WHERE 
		numOppId = @numOppID

	SELECT 
		OKI.numOppChildItemID,
		I.numItemCode,
		I.bitAssembly,
		numWareHouseItemId,
		numQtyItemsReq,
		numQtyShipped,
		ROW_NUMBER() OVER(ORDER BY numOppChildItemID) AS RowNumber 
	INTO 
		#tempKits
	FROM 
		OpportunityKitItems OKI 
	JOIN 
		Item I 
	ON 
		OKI.numChildItemID=I.numItemCode    
	WHERE 
		charitemtype='P' 
		AND numWareHouseItemId > 0 
		AND numWareHouseItemId IS NOT NULL 
		AND OKI.numOppID=@numOppID 
		AND OKI.numOppItemID=@numOppItemID 
  

	DECLARE @minRowNumber INT
	DECLARE @maxRowNumber INT

	DECLARE @numOppChildItemID AS NUMERIC(18,0)
	DECLARE @numItemCode NUMERIC(18,0)
	DECLARE @bitAssembly BIT
	DECLARE @numWareHouseItemID NUMERIC(18,0)
	
	DECLARE @onHand FLOAT
	DECLARE @onAllocation FLOAT
	DECLARE @onOrder FLOAT
	DECLARE @onBackOrder FLOAT
	DECLARE @onReOrder FLOAT
	DECLARE @numUnits FLOAT
	DECLARE @QtyShipped FLOAT
	

	SELECT  
		@minRowNumber = MIN(RowNumber),
		@maxRowNumber = MAX(RowNumber) 
	FROM 
		#tempKits


	DECLARE @description AS VARCHAR(300)

	WHILE  @minRowNumber <= @maxRowNumber
    BEGIN
		SELECT 
			@numOppChildItemID = numOppChildItemID,
			@numItemCode=numItemCode,
			@bitAssembly=ISNULL(bitAssembly,0),
			@numWareHouseItemID=numWareHouseItemID,
			@numUnits=numQtyItemsReq,
			@QtyShipped=numQtyShipped 
		FROM 
			#tempKits 
		WHERE 
			RowNumber=@minRowNumber
    
		IF @Mode=0
				SET @description=CONCAT('SO KIT insert/edit (Qty:',@numUnits,' Shipped:',@QtyShipped,')')
		ELSE IF @Mode=1
			IF @tintMode=2
			BEGIN
				SET @description=CONCAT('SO DEMOTED TO OPPORTUNITY KIT (Qty:',@numUnits,' Shipped:',@QtyShipped,')')
			END
			ELSE
			BEGIN
				SET @description=CONCAT('SO KIT Deleted (Qty:',@numUnits,' Shipped:',@QtyShipped,')')
			END
		ELSE IF @Mode=2
				SET @description=CONCAT('SO KIT Close (Qty:',@numUnits,' Shipped:',@QtyShipped,')')
		ELSE IF @Mode=3
				SET @description=CONCAT('SO KIT Re-Open (Qty:',@numUnits,' Shipped:',@QtyShipped,')')

		-- IF KIT ITEM CONTAINS ANOTHER KIT AS CHILD THEN WE HAVE TO UPDATE INVENTORY OF ITEMS OF KIT CHILD ITEMS
		IF EXISTS (SELECT [numOppKitChildItemID] FROM OpportunityKitChildItems WHERE numOppID=@numOppID AND numOppItemID=@numOppItemID AND numOppChildItemID = @numOppChildItemID)
		BEGIN
			EXEC dbo.USP_ManageWareHouseItems_Tracking
				@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
				@numReferenceID = @numOppID, --  numeric(9, 0)
				@tintRefType = 3, --  tinyint
				@vcDescription = @description, --  varchar(100)
				@numModifiedBy = @numUserCntID,
				@numDomainID = @numDomain


			EXEC USP_WareHouseItems_ManageInventoryKitWithinKit
				@numOppID = @numOppID,
				@numOppItemID = @numOppItemID,
				@numOppChildItemID = @numOppChildItemID,
				@Mode = @Mode,
				@tintMode = @tintMode,
				@numDomainID = @numDomain,
				@numUserCntID = @numUserCntID,
				@tintOppType = @tintOppType
		END
		ELSE
		BEGIN
			SELECT 
				@onHand=ISNULL(numOnHand,0),
				@onAllocation=ISNULL(numAllocation,0),                                    
				@onOrder=ISNULL(numOnOrder,0),
				@onBackOrder=ISNULL(numBackOrder,0),
				@onReOrder = ISNULL(numReorder,0)                                     
			FROM 
				WareHouseItems 
			WHERE 
				numWareHouseItemID=@numWareHouseItemID

    		IF @Mode=0 --insert/edit
    		BEGIN
    			SET @numUnits = @numUnits - @QtyShipped
    	
				IF @onHand>=@numUnits                                    
				BEGIN                                    
					SET @onHand=@onHand-@numUnits                            
					SET @onAllocation=@onAllocation+@numUnits                                    
				END                                    
				ELSE IF @onHand<@numUnits                                    
				BEGIN                                    
					SET @onAllocation=@onAllocation+@onHand                                    
					SET @onBackOrder=@onBackOrder+@numUnits-@onHand                                    
					SET @onHand=0                                    
				END                        
    		END
			ELSE IF @Mode=1 --Revert
			BEGIN
				IF @QtyShipped>0
				BEGIN
					IF @tintMode=0
						SET @numUnits = @numUnits - @QtyShipped
					ELSE IF @tintmode=1 OR @tintMode=2
						SET @onAllocation = @onAllocation + @QtyShipped 
				END 
								                    
				IF @numUnits >= @onBackOrder 
				BEGIN
					SET @numUnits = @numUnits - @onBackOrder
					SET @onBackOrder = 0
                            
					IF (@onAllocation - @numUnits >= 0)
						SET @onAllocation = @onAllocation - @numUnits
					SET @onHand = @onHand + @numUnits                                            
				END                                            
				ELSE IF @numUnits < @onBackOrder 
				BEGIN                  
					IF (@onBackOrder - @numUnits >0)
						SET @onBackOrder = @onBackOrder - @numUnits
				END 
			END
			ELSE IF @Mode=2 --Close
			BEGIN
				SET @numUnits = @numUnits - @QtyShipped  
				IF @onAllocation >= @numUnits
				BEGIN
					SET @onAllocation = @onAllocation - @numUnits            
				END
				ELSE
				BEGIN
					RAISERROR ('NOTSUFFICIENTQTY_ALLOCATION',16,1);
				END
			END
			ELSE IF @Mode=3 --Re-Open
			BEGIN
				SET @numUnits = @numUnits - @QtyShipped
				SET @onAllocation = @onAllocation + @numUnits            
			END

			UPDATE 
				WareHouseItems 
			SET 
				numOnHand=@onHand,
				numAllocation=@onAllocation,                                    
				numBackOrder=@onBackOrder,
				dtModified = GETDATE()  
			WHERE 
				numWareHouseItemID=@numWareHouseItemID 
	
			EXEC dbo.USP_ManageWareHouseItems_Tracking
				@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
				@numReferenceID = @numOppID, --  numeric(9, 0)
				@tintRefType = 3, --  tinyint
				@vcDescription = @description, --  varchar(100)
				@numModifiedBy = @numUserCntID,
				@numDomainID = @numDomain
		END
		
		SET @minRowNumber=@minRowNumber + 1
    END	
  
	DROP TABLE #tempKits
END
GO

GO
/****** Object:  StoredProcedure [dbo].[usp_UpdateStagePercentageDetails]    Script Date: 09/21/2010 12:00:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----Created By Gangadhar    
    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DeleteTask')
DROP PROCEDURE USP_DeleteTask
GO
CREATE PROCEDURE [dbo].[USP_DeleteTask]
@numDomainID as numeric(9)=0,    
@numTaskId as numeric(18)=0 
as    
BEGIN   
	IF EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numDomainID=@numDomainID AND numTaskID=@numTaskId AND tintAction = 4)
	BEGIN
		RAISERROR('TASK_IS_MARKED_AS_FINISHED',16,1)
	END
	ELSE IF EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numDomainID=@numDomainID AND numTaskID=@numTaskId)
	BEGIN
		RAISERROR('TASK_IS_ALREADY_STARTED',16,1)
	END
	ELSE IF EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numDomainID=@numDomainID AND tintAction = 4 AND numTaskID IN (SELECT SPDT.numTaskId FROM StagePercentageDetailsTask SPDT WHERE SPDT.numDomainID=@numDomainID AND SPDT.numParentTaskId=@numTaskId))
	BEGIN
		RAISERROR('CHILD_TASK_IS_MARKED_AS_FINISHED',16,1)
	END
	ELSE IF EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numDomainID=@numDomainID AND numTaskID IN (SELECT SPDT.numTaskId FROM StagePercentageDetailsTask SPDT WHERE SPDT.numDomainID=@numDomainID AND SPDT.numParentTaskId=@numTaskId))
	BEGIN
		RAISERROR('CHILD_TASK_IS_ALREADY_STARTED',16,1)
	END
	ELSE IF ISNULL((SELECT numStageDetailsId FROM StagePercentageDetailsTask WHERE numTaskId=@numTaskId),0) > 0 AND NOT EXISTS (SELECT * FROM StagePercentageDetailsTask WHERE numStageDetailsId=ISNULL((SELECT numStageDetailsId FROM StagePercentageDetailsTask WHERE numTaskId=@numTaskId),0) AND numTaskId <> @numTaskId)
	BEGIN
		RAISERROR('STAGE_MUST_HAVE_AT_LEAST_ONE_TASK',16,1)
		RETURN
	END

	DELETE FROM 
		StagePercentageDetailsTask
	WHERE
		numDomainID=@numDomainID AND numParentTaskId=@numTaskId

	DELETE FROM 
		StagePercentageDetailsTask
	WHERE
		numDomainID=@numDomainID AND numTaskId=@numTaskId
END 
GO
