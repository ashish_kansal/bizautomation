/******************************************************************
Project: Release 11.0 Date: 06.FEB.2019
Comments: STORE PROCEDURES
*******************************************************************/

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='tf'AND NAME ='fn_GetItemPriceBasedOnMethod')
DROP FUNCTION fn_GetItemPriceBasedOnMethod
GO
CREATE FUNCTION [dbo].[fn_GetItemPriceBasedOnMethod] 
(
    @numDomainID NUMERIC(18,0)
	,@numDivisionID NUMERIC(18,0)
	,@numItemCode NUMERIC(18,0)
	,@numQty FLOAT
)
RETURNS @ItemPrice TABLE
(
	monPrice DECIMAL(20,5)
	,tintDisountType TINYINT
	,decDiscount FLOAT
	,monFinalPrice DECIMAL(20,5) 
)
AS 
BEGIN	
	DECLARE @monListPrice DECIMAL(20,5)
	DECLARE @monVendorCost DECIMAL(20,5)
	DECLARE @tintPriceLevel AS TINYINT
	DECLARE @tintRuleType AS TINYINT
	DECLARE @tintDisountType AS TINYINT
	DECLARE @decDiscount AS FLOAT
	DECLARE @monPrice AS DECIMAL(20,5) = 0
	DECLARE @monFinalPrice AS DECIMAL(20,5) = 0
	DECLARE @numRelationship NUMERIC(18,0)
	DECLARE @numProfile NUMERIC(18,0)
	DECLARE @numItemClassification AS NUMERIC(18,0)
	DECLARE @tintPriceBookDiscount AS TINYINT
	DECLARE @tintPriceMethod TINYINT
	DECLARE @fltUOMConversionFactor INT = 1

	SELECT 
		@monListPrice = ISNULL(monListPrice,0)
		,@numItemClassification=numItemClassification
		,@monVendorCost=ISNULL(@monVendorCost,0)
	FROM 
		Item 
	LEFT JOIN
		Vendor
	ON
		Item.numVendorID = Vendor.numVendorID
		AND Item.numItemCode = Vendor.numItemCode
	WHERE
		Item.numItemCode=@numItemCode

	SELECT 
		@tintPriceMethod=ISNULL(numDefaultSalesPricing,0)
		,@tintPriceBookDiscount=tintPriceBookDiscount 
	FROM 
		Domain 
	WHERE 
		numDomainId=@numDomainID

	-- GET ORGANIZATION DETAL
	SELECT 
		@numRelationship=numCompanyType,
		@numProfile=vcProfile,
		@tintPriceLevel = ISNULL(D.tintPriceLevel,0)
	FROM 
		DivisionMaster D                  
	JOIN 
		CompanyInfo C 
	ON 
		C.numCompanyId=D.numCompanyID                  
	WHERE 
		numDivisionID =@numDivisionID       

	IF @tintPriceMethod= 1 -- PRICE LEVEL
	BEGIN
		IF (SELECT COUNT(*) FROM PricingTable WHERE numItemCode=@numItemCode AND ISNULL(numPriceRuleID,0) = 0) > 0
		BEGIN
			SELECT
				@tintDisountType = 1,
				@decDiscount = 0,
				@tintRuleType = tintRuleType,
				@monPrice = (CASE tintRuleType
										WHEN 1 -- Deduct From List Price
										THEN
											CASE tintDiscountType 
												WHEN 1 -- PERCENT
												THEN (CASE WHEN @monListPrice > 0 THEN @monListPrice - (@monListPrice * (ISNULL(decDiscount,0)/100)) ELSE 0 END)
												WHEN 2 -- FLAT AMOUNT
												THEN (CASE WHEN @monListPrice > 0 THEN (CASE WHEN (@monListPrice * @numQty * @fltUOMConversionFactor) - ISNULL(decDiscount,0) > 0 THEN ((@monListPrice * @numQty * @fltUOMConversionFactor) - ISNULL(decDiscount,0)) / (@numQty * @fltUOMConversionFactor) ELSE 0 END) ELSE 0 END)
												WHEN 3 -- NAMED PRICE
												THEN ISNULL(decDiscount,0)
											END
										WHEN 2 -- Add to primary vendor cost
										THEN
											CASE tintDiscountType 
												WHEN 1  -- PERCENT
												THEN @monVendorCost + (@monVendorCost * (ISNULL(decDiscount,0)/100)) * @fltUOMConversionFactor
												WHEN 2 -- FLAT AMOUNT
												THEN (CASE WHEN (@monVendorCost * @numQty * @fltUOMConversionFactor) + ISNULL(decDiscount,0) > 0 THEN ((@monVendorCost * @numQty * @fltUOMConversionFactor) + ISNULL(decDiscount,0)) / (@numQty * @fltUOMConversionFactor)  ELSE 0 END)
												WHEN 3 -- NAMED PRICE
												THEN ISNULL(decDiscount,0)
											END
										WHEN 3 -- Named price
										THEN
											CASE 
											WHEN ISNULL(@tintPriceLevel,0) > 0
											THEN
												(SELECT 
													 ISNULL(decDiscount,0)
												FROM
												(
													SELECT 
														ROW_NUMBER() OVER(PARTITION BY PricingTable.numItemCode ORDER BY numPricingID) Id,
														decDiscount
													FROM 
														PricingTable 
													WHERE 
														PricingTable.numItemCode = @numItemCode 
														AND tintRuleType = 3 
												) TEMP
												WHERE
													Id = @tintPriceLevel)
											ELSE
												ISNULL(decDiscount,0)
											END
										END
									) 
			FROM
				PricingTable
			WHERE
				numItemCode=@numItemCode AND (@numQty * @fltUOMConversionFactor) >= intFromQty AND (@numQty * @fltUOMConversionFactor) <= intToQty
		END

		INSERT INTO @ItemPrice
		(
			monPrice
			,tintDisountType
			,decDiscount
			,monFinalPrice
		)
		SELECT 
			ISNULL(@monPrice,@monListPrice) AS monPrice,
			1 AS tintDisountType,
			0 AS decDiscount,
			ISNULL(@monPrice,@monListPrice)
	END
	ELSE IF @tintPriceMethod= 2 AND (SELECT COUNT(*) FROM PriceBookRules WHERE numDomainID=@numDomainID AND tintRuleFor=1) > 0 -- PRICE RULE
	BEGIN
		DECLARE @numPriceRuleID NUMERIC(18,0)
		DECLARE @tintPriceRuleType AS TINYINT
		DECLARE @tintPricingMethod TINYINT
		DECLARE @tintPriceBookDiscountType TINYINT
		DECLARE @decPriceBookDiscount FLOAT
		DECLARE @intQntyItems INT
		DECLARE @decMaxDedPerAmt FLOAT

		SELECT 
			TOP 1
			@numPriceRuleID = numPricRuleID,
			@tintPricingMethod = tintPricingMethod,
			@tintPriceRuleType = p.tintRuleType,
			@tintPriceBookDiscountType = P.[tintDiscountType],
			@decPriceBookDiscount = P.[decDiscount],
			@intQntyItems = P.[intQntyItems],
			@decMaxDedPerAmt = P.[decMaxDedPerAmt]
		FROM   
			PriceBookRules P 
		LEFT JOIN 
			PriceBookRuleDTL PDTL 
		ON 
			P.numPricRuleID = PDTL.numRuleID
		LEFT JOIN 
			PriceBookRuleItems PBI 
		ON 
			P.numPricRuleID = PBI.numRuleID
		LEFT JOIN 
			[PriceBookPriorities] PP 
		ON 
			PP.[Step2Value] = P.[tintStep2] 
			AND PP.[Step3Value] = P.[tintStep3]
		WHERE  
			P.numDomainID=@numDomainID 
			AND tintRuleFor=1
			AND (
					((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 1 AND PDTL.numValue = @numDivisionID)) -- Priority 1
					OR ((tintStep2 = 2 AND PBI.numValue = @numItemClassification) AND (tintStep3= 1 AND PDTL.numValue = @numDivisionID)) -- Priority 2
					OR ((tintStep2 = 3) AND (tintStep3 = 1 AND PDTL.numValue = @numDivisionID)) -- Priority 3

					OR ((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 4
					OR ((tintStep2 = 2 AND PBI.numValue = @numItemClassification) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 5
					OR ((tintStep2 = 3) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 6

					OR ((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 3)) -- Priority 7
					OR ((tintStep2 = 2 AND PBI.numValue = @numItemClassification) AND (tintStep3 = 3)) -- Priority 8
					OR ((tintStep2 = 3) and (tintStep3 = 3)) -- Priority 9
				)
		ORDER BY 
			PP.Priority ASC


		IF ISNULL(@numPriceRuleID,0) > 0
		BEGIN
			IF @tintPricingMethod = 1 -- BASED ON PRICE TABLE
			BEGIN
				SELECT
					@tintDisountType = tintDiscountType,
					@decDiscount = ISNULL(decDiscount,0),
					@tintRuleType = tintRuleType,
					@monPrice = (CASE tintRuleType WHEN 1 THEN @monListPrice WHEN 2 THEN @monVendorCost END) ,
					@monFinalPrice = (CASE tintRuleType
											WHEN 1 -- Deduct From List Price
											THEN
												CASE tintDiscountType 
													WHEN 1 -- PERCENT
													THEN (CASE WHEN @monListPrice > 0 THEN @monListPrice - (@monListPrice * (ISNULL(decDiscount,0)/100)) ELSE 0 END)
													WHEN 2 -- FLAT AMOUNT
													THEN (CASE WHEN @monListPrice > 0 THEN (CASE WHEN (@monListPrice * @numQty * @fltUOMConversionFactor) - ISNULL(decDiscount,0) > 0 THEN ((@monListPrice * @numQty * @fltUOMConversionFactor) - ISNULL(decDiscount,0)) / (@numQty * @fltUOMConversionFactor) ELSE 0 END) ELSE 0 END)
													WHEN 3 -- NAMED PRICE
													THEN ISNULL(decDiscount,0)
												END
											WHEN 2 -- Add to primary vendor cost
											THEN
												CASE tintDiscountType 
													WHEN 1  -- PERCENT
													THEN @monVendorCost + (@monVendorCost * (ISNULL(decDiscount,0)/100))
													WHEN 2 -- FLAT AMOUNT
													THEN (CASE WHEN (@monVendorCost * @numQty * @fltUOMConversionFactor) + ISNULL(decDiscount,0) > 0 THEN ((@monVendorCost * @numQty * @fltUOMConversionFactor) + ISNULL(decDiscount,0)) / (@numQty * @fltUOMConversionFactor)  ELSE 0 END)
													WHEN 3 -- NAMED PRICE
													THEN ISNULL(decDiscount,0)
												END
											WHEN 3 -- Named price
											THEN
												CASE 
												WHEN ISNULL(@tintPriceLevel,0) > 0
												THEN
													(SELECT 
														 ISNULL(decDiscount,0)
													FROM
													(
														SELECT 
															ROW_NUMBER() OVER(PARTITION BY PricingTable.numItemCode ORDER BY numPricingID) Id,
															decDiscount
														FROM 
															PricingTable 
														WHERE 
															PricingTable.numItemCode = @numItemCode 
															AND tintRuleType = 3 
													) TEMP
													WHERE
														Id = @tintPriceLevel)
												ELSE
													ISNULL(decDiscount,0)
												END
											END
										) 
				FROM
					PricingTable
				WHERE
					numPriceRuleID=@numPriceRuleID AND @numQty * @fltUOMConversionFactor >= intFromQty AND @numQty * @fltUOMConversionFactor <= intToQty
			END
			ELSE IF @tintPricingMethod = 2 -- BASED ON RULE
			BEGIN
				SET @monPrice = (CASE WHEN @tintPriceRuleType = 2 THEN @monVendorCost ELSE @monListPrice END)

				IF (@tintPriceBookDiscountType = 1 ) -- Percentage 
				BEGIN
					SET @tintDisountType = @tintPriceBookDiscountType
					SET @decDiscount = @decPriceBookDiscount * (@numQty/@intQntyItems);
					SET @decDiscount = @decPriceBookDiscount * (CASE WHEN @tintPriceRuleType = 2 THEN @monVendorCost ELSE @monListPrice END) /100;
					SET @decMaxDedPerAmt = @decMaxDedPerAmt * (CASE WHEN @tintPriceRuleType = 2 THEN @monVendorCost ELSE @monListPrice END) /100;
					
					IF (@decDiscount > @decMaxDedPerAmt)
						SET @decDiscount = @decMaxDedPerAmt;
				
					IF ( @tintPriceRuleType = 1 )
							SET @monFinalPrice = (@monListPrice - @decDiscount);
					IF ( @tintPriceRuleType = 2 )
							SET @monFinalPrice = (@monVendorCost + @decDiscount);
				END
				IF ( @tintPriceBookDiscountType = 2 ) -- Flat discount 
				BEGIN
					SET @decDiscount = @decPriceBookDiscount * ((@numQty * @fltUOMConversionFactor)/@intQntyItems);

					IF (@decDiscount > @decMaxDedPerAmt)
						SET @decDiscount = @decMaxDedPerAmt;
					
					IF ( @tintPriceRuleType = 1 )
						SET @monFinalPrice =  ((@monListPrice * @numQty * @fltUOMConversionFactor) - @decDiscount) / (@numQty * @fltUOMConversionFactor);
						
					IF ( @tintPriceRuleType = 2 )
						SET @monFinalPrice = ((@monVendorCost * @numQty * @fltUOMConversionFactor) + @decDiscount) / (@numQty * @fltUOMConversionFactor);
				END
			END
		END
		
		INSERT INTO @ItemPrice
		(
			monPrice
			,tintDisountType
			,decDiscount
			,monFinalPrice
		)
		SELECT 
			(CASE WHEN @monFinalPrice IS NULL THEN @monListPrice ELSE (CASE WHEN @tintPriceBookDiscount = 1 THEN ISNULL(@monPrice,0) ELSE ISNULL(@monFinalPrice,0) END) END),
			(CASE WHEN @monFinalPrice IS NULL THEN 1 ELSE (CASE WHEN @tintPriceBookDiscount = 1 THEN ISNULL(@tintPriceBookDiscountType,1) ELSE 1 END) END),
			(CASE WHEN @monFinalPrice IS NULL THEN 0 ELSE (CASE WHEN @tintPriceBookDiscount = 1 THEN ISNULL(@decPriceBookDiscount,0) ELSE 0 END) END),
			ISNULL(@monFinalPrice,0)
	END
	ELSE IF @tintPriceMethod = 3 -- Last Price
	BEGIN
		SELECT 
			TOP 1 
			@monPrice = monPrice
			,@tintDisountType = ISNULL(OpportunityItems.bitDiscountType,1)
			,@decDiscount = ISNULL(OpportunityItems.fltDiscount,0)
			,@monFinalPrice = (CASE 
								WHEN ISNULL(OpportunityItems.fltDiscount,0) > 0 AND ISNULL(numUnitHour,0) > 0 
								THEN (CASE 
										WHEN ISNULL(OpportunityItems.bitDiscountType,1)=1 
										THEN CAST(((ISNULL(monTotAmtBefDiscount,0) - ISNULL(monTotAmount,0))/numUnitHour) AS DECIMAL(20,5))
										ELSE CAST(monPrice - (monPrice * (ISNULL(OpportunityItems.fltDiscount,0)/100)) AS DECIMAL(20,5)) END) 
								ELSE monPrice 
								END)
			,@tintRuleType = 3
		FROM 
			OpportunityItems 
		JOIN 
			OpportunityMaster 
		ON 
			OpportunityItems.numOppId = OpportunityMaster.numOppId 
		WHERE 
			OpportunityMaster.numDomainId= @numDomainID
			AND tintOppType = 1
			AND tintOppStatus = 1
			AND numItemCode=@numItemCode
		ORDER BY
			OpportunityMaster.numOppId DESC

		INSERT INTO @ItemPrice
		(
			monPrice
			,tintDisountType
			,decDiscount
			,monFinalPrice
		)
		VALUES
		( 
			ISNULL(@monPrice,0),
			@tintDisountType,
			@decDiscount,
			@monFinalPrice
		)
	END
	ELSE
	BEGIN
		INSERT INTO @ItemPrice
		(
			monPrice
			,tintDisountType
			,decDiscount
			,monFinalPrice
		)
		VALUES
		( 
			ISNULL(@monListPrice,0),
			1,
			0,
			ISNULL(@monListPrice,0)
		)
	END

	RETURN
END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_getOPPState')
DROP FUNCTION fn_getOPPState
GO
CREATE FUNCTION [dbo].[fn_getOPPState] (@numOppId numeric,@numDomainID  NUMERIC,@tintMode AS TINYINT)
RETURNS VARCHAR(1000)
AS
BEGIN
	DECLARE @strState varchar(50)
	DECLARE  @tintOppType  AS TINYINT
	DECLARE  @tintBillType  AS TINYINT
	DECLARE  @tintShipType  AS TINYINT
	DECLARE @numBillToAddressID AS NUMERIC(18,0)
	DECLARE @numShipToAddressID AS NUMERIC(18,0)
 
	DECLARE @numParentOppID AS NUMERIC,@numDivisionID AS NUMERIC 
      
	SELECT  
		@tintOppType = tintOppType,
		@tintBillType = tintBillToType,
		@numBillToAddressID = numBillToAddressID,
		@tintShipType = tintShipToType,
		@numShipToAddressID = numShipToAddressID,
		@numDivisionID = numDivisionID
	FROM   
		OpportunityMaster 
	WHERE  
		numOppId = @numOppId

	-- When Creating PO from SO and Bill type is Customer selected 
	SELECT @numParentOppID=ISNULL(numParentOppID,0) FROM dbo.OpportunityLinking WHERE numChildOppID = @numOppId
            
	IF @tintMode=1 --Billing Address
	BEGIN
		If ISNULL(@numBillToAddressID,0) > 0
		BEGIN
			SELECT  
				@strState = isnull(dbo.fn_GetState(AD.numState),'') 
			FROM 
				AddressDetails AD 
			WHERE
				AD.numDomainID=@numDomainID 
				AND AD.numAddressID = @numBillToAddressID
		END
		ELSE
		BEGIN
				IF @tintBillType IS NULL OR (@tintBillType = 1 AND @tintOppType = 1) --Primary Bill Address or When Sales order and bill to is set to customer	 
				BEGIN
					SELECT  
						@strState =  isnull(dbo.fn_GetState(AD.numState),'') 
									 
					FROM 
						AddressDetails AD 
					WHERE 
						AD.numDomainID=@numDomainID AND AD.numRecordID=@numDivisionID 
							AND AD.tintAddressOf = 2 AND AD.tintAddressType = 1 AND AD.bitIsPrimary=1                    
				END
				ELSE IF @tintBillType = 1 AND @tintOppType = 2 -- When Create PO from SO and Bill to is set to Customer
				BEGIN
					SELECT @strState = dbo.fn_getOPPState(@numParentOppID,@numDomainID,@tintMode)
				END
				ELSE IF @tintBillType = 0
				BEGIN
					SELECT @strState = isnull(dbo.fn_GetState(AD.numState),'') 
										 FROM   companyinfo [Com1] JOIN divisionmaster div1 ON com1.numCompanyID = div1.numCompanyID
												JOIN Domain D1 ON D1.numDivisionID = div1.numDivisionID
												JOIN dbo.AddressDetails AD ON AD.numDomainID = div1.numDomainID AND AD.numRecordID = div1.numDivisionID AND tintAddressOf = 2 AND tintAddressType = 1 AND bitIsPrimary=1
														WHERE  D1.numDomainID = @numDomainID
				END
				ELSE IF @tintBillType = 2 OR @tintBillType = 3
				BEGIN
					SELECT @strState = isnull(dbo.fn_GetState(numBillState),'') 
									FROM   OpportunityAddress WHERE  numOppID = @numOppId
				END
		END
	END
	ELSE IF @tintMode=2 --Shipping Address
	BEGIN
		If ISNULL(@numShipToAddressID,0) > 0
		BEGIN
			SELECT  
				@strState = isnull(dbo.fn_GetState(AD.numState),'')
			FROM 
				AddressDetails AD 
			WHERE
				AD.numDomainID=@numDomainID 
				AND AD.numAddressID = @numShipToAddressID
		END
		ELSE
		BEGIN
			IF @tintShipType IS NULL OR (@tintShipType = 1 AND @tintOppType = 1)
			BEGIN
					SELECT  @strState = isnull(dbo.fn_GetState(AD.numState),'') 
					FROM AddressDetails AD 
						WHERE AD.numDomainID=@numDomainID AND AD.numRecordID=@numDivisionID 
						AND AD.tintAddressOf = 2 AND AD.tintAddressType = 2 AND AD.bitIsPrimary=1
			END
			ELSE IF @tintShipType = 1 AND @tintOppType = 2 -- When Create PO from SO and Ship to is set to Customer 
			BEGIN
				SELECT @strState=dbo.fn_getOPPState(@numParentOppID,@numDomainID,@tintMode)
			END
			ELSE IF @tintShipType = 0
			BEGIN
				SELECT @strState = isnull(dbo.fn_GetState(AD.numState),'') 
									 FROM   companyinfo [Com1] JOIN divisionmaster div1 ON com1.numCompanyID = div1.numCompanyID
											JOIN Domain D1 ON D1.numDivisionID = div1.numDivisionID
											JOIN dbo.AddressDetails AD ON AD.numDomainID = div1.numDomainID AND AD.numRecordID = div1.numDivisionID AND tintAddressOf = 2 AND tintAddressType = 2 AND bitIsPrimary=1
													WHERE  D1.numDomainID = @numDomainID
			END
			ELSE IF @tintShipType = 2 OR @tintShipType = 3
			BEGIN
				SELECT @strState = isnull(dbo.fn_GetState(numShipState),'')
								FROM   OpportunityAddress WHERE  numOppID = @numOppId
			END
		END
	END

return @strState
end
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='GetPromotionDiscountDescription')
DROP FUNCTION GetPromotionDiscountDescription
GO
CREATE FUNCTION GetPromotionDiscountDescription
(
	@numPromotionID NUMERIC(18,0)
	,@vcCurrency VARCHAR(10)
)
RETURNS VARCHAR(1000) 
AS
BEGIN
	DECLARE @vcValue AS VARCHAR(1000) = ''

	SELECT
		@vcValue = (CASE 
						WHEN tintDiscountType = 1 
						THEN
							CONCAT('Get ',ISNULL(fltDiscountValue,0),'% off any of these item�s ',(CASE WHEN tintItemCalDiscount = 1 THEN 'sale price' ELSE 'list price' END))
						WHEN tintDiscountType = 2 
						THEN
							CONCAT('Get ',@vcCurrency,ISNULL(fltDiscountValue,0),' off any of these item�s ',(CASE WHEN tintItemCalDiscount = 1 THEN 'sale price' ELSE 'list price' END))
						WHEN tintDiscountType = 3 
						THEN
							CONCAT('Get any ',ISNULL(fltDiscountValue,0),' of these items for FREE !')
					END)
	FROM
		PromotionOffer
	WHERE
		numProId=@numPromotionID

	RETURN ISNULL(@vcValue,'')
END


										
									
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='GetStoredCustFldValueOppItems')
DROP FUNCTION GetStoredCustFldValueOppItems
GO
CREATE FUNCTION GetStoredCustFldValueOppItems
(
 @numFldId NUMERIC(18,0),
 @numRecordId NUMERIC(18,0),
 @numItemCode NUMERIC(18,0)
)
RETURNS VARCHAR(MAX) 
AS
BEGIN
	DECLARE @vcValue AS VARCHAR(MAX) = ''
       
	IF EXISTS (SELECT Fld_Value FROM CFW_Fld_Values_OppItems WHERE Fld_ID = @numFldId  AND RecId = @numRecordId)
	BEGIN
		SELECT 
			@vcValue = ISNULL(Fld_Value,'') 
		FROM 
			CFW_Fld_Values_OppItems
		WHERE 
			Fld_ID = @numFldId 
			AND RecId = @numRecordId
	END
	ELSE
	BEGIN
		SELECT 
			@vcValue = ISNULL(Fld_Value,'') 
		FROM 
			CFW_Fld_Values_Item 
		WHERE 
			Fld_ID = @numFldId 
			AND RecId = @numItemCode
	END

	RETURN @vcValue
END
        
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_AddParentChildCustomFieldMap')
	DROP PROCEDURE USP_AddParentChildCustomFieldMap
GO

CREATE PROCEDURE [dbo].[USP_AddParentChildCustomFieldMap]
	@numDomainID numeric(18, 0),
	@numRecordID numeric(18, 0),
	@numParentRecId numeric(18, 0),
	@tintPageID TINYINT 
AS
BEGIN
	SELECT 
		tintParentModule
		,numParentFieldID
		,tintChildModule
		,numChildFieldID
		,PFM.Fld_type AS ParentFld_type
		,ISNULL(PFM.numlistid,0) AS Parentlistid
		,CFM.Fld_type AS ChildFld_type
		,ISNULL(CFM.numlistid,0) AS Childlistid
		,ROW_NUMBER() OVER(ORDER BY numParentFieldID) RowNO 
	INTO 
		#temp
	FROM 
		ParentChildCustomFieldMap PCFM 
	JOIN CFW_Loc_Master PM ON PCFM.tintParentModule=PM.Loc_id
	JOIN CFW_Fld_Master PFM ON PCFM.numParentFieldID=PFM.Fld_id AND PCFM.numDomainID=PFM.numDomainID AND PCFM.tintParentModule=PFM.Grp_id
	JOIN CFW_Loc_Master CM ON PCFM.tintChildModule=CM.Loc_id
	JOIN CFW_Fld_Master CFM ON PCFM.numChildFieldID=CFM.Fld_id AND PCFM.numDomainID=CFM.numDomainID AND PCFM.tintChildModule=CFM.Grp_id
	WHERE 
		PCFM.numDomainID=@numDomainID 
		AND PCFM.tintChildModule=@tintPageID 
		AND PFM.Fld_type=CFM.Fld_type

	DECLARE @minNo AS NUMERIC,@maxNo AS NUMERIC;
	SELECT @minNo=MIN(RowNO),@maxNo=MAX(RowNO) FROM #temp

	DECLARE @tintParentModule AS TINYINT,@numParentFieldID AS NUMERIC,@ParentFld_type AS VARCHAR(20),@Parentlistid AS NUMERIC
	DECLARE @tintChildModule AS TINYINT,@numChildFieldID AS NUMERIC,@ChildFld_type AS VARCHAR(20),@Childlistid AS NUMERIC
	DECLARE @ParentRecId AS NUMERIC,@Fld_Value AS VARCHAR(1000),@vcData VARCHAR(50)

	WHILE @minNo <= @maxNo
	BEGIN
		SELECT 
			@tintParentModule=tintParentModule
			,@numParentFieldID=numParentFieldID
			,@ParentFld_type=ParentFld_type
			,@Parentlistid=Parentlistid
			,@tintChildModule=tintChildModule
			,@numChildFieldID=numChildFieldID
			,@ChildFld_type=ChildFld_type
			,@Childlistid=Childlistid 
		FROM 
			#temp 
		WHERE 
			RowNO=@minNo 	

		IF @ParentFld_type='SelectBox' AND @ChildFld_type='SelectBox'
		BEGIN
			SET @Fld_Value = ''
			
			IF @tintParentModule=1 --Organizations
			BEGIN
				SELECT @Fld_Value=Fld_Value FROM CFW_FLD_Values WHERE RecId=@numParentRecId AND Fld_ID=@numParentFieldID
			END
			ELSE IF (@tintParentModule=6 OR @tintParentModule = 2)   --Sales Opportunities / Orders
			BEGIN
				SELECT @Fld_Value=Fld_Value FROM CFW_Fld_Values_Opp WHERE RecId=@numParentRecId AND Fld_ID=@numParentFieldID
			END

			IF @Parentlistid>0 AND @Childlistid>0 AND @Fld_Value <> ''
			BEGIN
				SELECT @vcData=vcData FROM dbo.ListDetails WHERE numDomainID=@numDomainID AND numListID=@Parentlistid AND numListItemID=CAST(@Fld_Value AS NUMERIC(18))
				
				SET @Fld_Value=NULL
				
				SELECT @Fld_Value=numListItemID FROM dbo.ListDetails WHERE numDomainID=@numDomainID AND numListID=@Childlistid AND LOWER(vcData)=LOWER(ISNULL(@vcData,''))
			END
			ELSE
			BEGIN
				SET @Fld_Value=NULL
			END

			IF @Fld_Value IS NOT NULL 
			BEGIN
				IF @tintChildModule=2 OR @tintChildModule=6 --Sales Opportunities / Orders
				BEGIN
					DELETE FROM CFW_Fld_Values_Opp WHERE RecId = @numRecordID AND Fld_ID=@numChildFieldID

					INSERT INTO CFW_Fld_Values_Opp 
					(Fld_ID,Fld_Value,RecId) 
					SELECT 
					@numChildFieldID,@Fld_Value,@numRecordID
				END
				ELSE IF @tintChildModule=3 --Case
				BEGIN
					DELETE FROM CFW_FLD_Values_Case WHERE RecId = @numRecordID AND Fld_ID=@numChildFieldID

					INSERT INTO CFW_FLD_Values_Case 
					(Fld_ID,Fld_Value,RecId)          
					SELECT 
					@numChildFieldID,@Fld_Value,@numRecordID
				END
				ELSE IF @tintChildModule=11 --Projects
				BEGIN
					DELETE FROM CFW_Fld_Values_Pro WHERE RecId = @numRecordID AND Fld_ID=@numChildFieldID

					INSERT INTO CFW_Fld_Values_Pro 
					(Fld_ID,Fld_Value,RecId)          
					SELECT 
					@numChildFieldID,@Fld_Value,@numRecordID
				END
			END
		END
		ELSE IF @ParentFld_type='CheckBoxList' AND @ChildFld_type='CheckBoxList'
		BEGIN
			IF @Parentlistid>0 AND @Childlistid>0
			BEGIN
				DECLARE @TEMP TABLE
				(
					ID INT IDENTITY(1,1)
					,ListItemID NUMERIC(18,0)
				)

				IF @tintParentModule=1 --Organizations
				BEGIN
					INSERT INTO @TEMP
					(
						ListItemID
					)
					SELECT * FROM dbo.SplitIDs(ISNULL((SELECT Fld_Value FROM CFW_FLD_Values WHERE RecId=@numParentRecId AND Fld_ID=@numParentFieldID),''),',')
				END
				ELSE IF (@tintParentModule = 6 OR @tintParentModule = 2) --Sales Opportunities / Orders
				BEGIN
					INSERT INTO @TEMP
					(
						ListItemID
					)
					SELECT Id FROM dbo.SplitIDs(ISNULL((SELECT Fld_Value FROM CFW_Fld_Values_Opp WHERE RecId=@numParentRecId AND Fld_ID=@numParentFieldID),''),',')
				END

				DECLARE @i INT = 1
				DECLARE @iCount INT 
				DECLARE @Fld_VlaueTemp NUMERIC(18,0)
				DECLARE @Fld_ValueCheckBoxList VARCHAR(MAX) = ''
				SET @Fld_ValueCheckBoxList=NULL

				SELECT @iCount = COUNT(*) FROM @TEMP
				
				WHILE @i <= @iCount
				BEGIN
					SELECT @Fld_VlaueTemp = ListItemID FROM @TEMP WHERE ID = @i
		
					SELECT @vcData=vcData FROM dbo.ListDetails WHERE numDomainID=@numDomainID AND numListID=@Parentlistid AND numListItemID=@Fld_VlaueTemp

					IF EXISTS (SELECT numListItemID FROM dbo.ListDetails WHERE numDomainID=@numDomainID AND numListID=@Childlistid AND LOWER(vcData)=LOWER(ISNULL(@vcData,'')))
					BEGIN
						SET @Fld_ValueCheckBoxList = CONCAT(@Fld_ValueCheckBoxList,(CASE WHEN LEN(ISNULL(@Fld_ValueCheckBoxList,'')) > 0 THEN CONCAT(',',(SELECT numListItemID FROM dbo.ListDetails WHERE numDomainID=@numDomainID AND numListID=@Childlistid AND LOWER(vcData)=LOWER(ISNULL(@vcData,'')))) ELSE CAST((SELECT numListItemID FROM dbo.ListDetails WHERE numDomainID=@numDomainID AND numListID=@Childlistid AND LOWER(vcData)=LOWER(ISNULL(@vcData,''))) AS VARCHAR) END))
					END

					SET @i = @i + 1
				END

				IF @Fld_ValueCheckBoxList IS NOT NULL 
				BEGIN
					IF @tintChildModule=2 OR @tintChildModule=6 --Sales Opportunities / Orders
					BEGIN
						DELETE FROM CFW_Fld_Values_Opp WHERE RecId = @numRecordID AND Fld_ID=@numChildFieldID

						INSERT INTO CFW_Fld_Values_Opp 
						(Fld_ID,Fld_Value,RecId) 
						SELECT 
						@numChildFieldID,@Fld_ValueCheckBoxList,@numRecordID
					END
					ELSE IF @tintChildModule=3 --Case
					BEGIN
						DELETE FROM CFW_FLD_Values_Case WHERE RecId = @numRecordID AND Fld_ID=@numChildFieldID

						INSERT INTO CFW_FLD_Values_Case 
						(Fld_ID,Fld_Value,RecId)          
						SELECT 
						@numChildFieldID,@Fld_ValueCheckBoxList,@numRecordID
					END
					ELSE IF @tintChildModule=11 --Projects
					BEGIN
						DELETE FROM CFW_Fld_Values_Pro WHERE RecId = @numRecordID AND Fld_ID=@numChildFieldID

						INSERT INTO CFW_Fld_Values_Pro 
						(Fld_ID,Fld_Value,RecId)          
						SELECT 
						@numChildFieldID,@Fld_ValueCheckBoxList,@numRecordID
					END
				END
			END
		END
		ELSE
		BEGIN
			IF @tintParentModule=1 --Organizations
			BEGIN
				SELECT @Fld_Value=Fld_Value FROM CFW_FLD_Values WHERE RecId=@numParentRecId AND Fld_ID=@numParentFieldID
			END
			ELSE IF (@tintParentModule = 6 OR @tintParentModule = 2) --Sales Opportunities / Orders
			BEGIN
				SELECT @Fld_Value=Fld_Value FROM CFW_Fld_Values_Opp WHERE RecId=@numParentRecId AND Fld_ID=@numParentFieldID
			END

			IF @Fld_Value IS NOT NULL 
			BEGIN
				IF @tintChildModule=2 OR @tintChildModule=6 --Sales Opportunities / Orders
				BEGIN
					DELETE FROM CFW_Fld_Values_Opp WHERE RecId = @numRecordID AND Fld_ID=@numChildFieldID
				
					INSERT INTO CFW_Fld_Values_Opp 
					(Fld_ID,Fld_Value,RecId) 
					SELECT 
					@numChildFieldID,@Fld_Value,@numRecordID
				END
				ELSE IF @tintChildModule=3 --Case
				BEGIN
					DELETE FROM CFW_FLD_Values_Case WHERE RecId = @numRecordID AND Fld_ID=@numChildFieldID

					INSERT INTO CFW_FLD_Values_Case 
					(Fld_ID,Fld_Value,RecId)          
					SELECT 
					@numChildFieldID,@Fld_Value,@numRecordID
				END
				ELSE IF @tintChildModule=11 --Projects
				BEGIN
					DELETE FROM CFW_Fld_Values_Pro WHERE RecId = @numRecordID AND Fld_ID=@numChildFieldID

					INSERT INTO CFW_Fld_Values_Pro 
					(Fld_ID,Fld_Value,RecId)          
					SELECT 
					@numChildFieldID,@Fld_Value,@numRecordID
				END
			END
		END
	
		SET @minNo = @minNo + 1
	END	
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ApplyPromotionToOrderedItems')
DROP PROCEDURE USP_ApplyPromotionToOrderedItems
GO
CREATE PROCEDURE [dbo].[USP_ApplyPromotionToOrderedItems]
	@numDomainID NUMERIC(18,0),
	@numDivisionID NUMERIC(18,0),
	@vcItems VARCHAR(MAX)
AS
BEGIN
	DECLARE @hDocItem INT
	DECLARE @numRelationship NUMERIC(18,0)
	DECLARE @numProfile NUMERIC(18,0)

	SELECT 
		@numRelationship=numCompanyType,
		@numProfile=vcProfile
	FROM 
		DivisionMaster D                  
	JOIN 
		CompanyInfo C 
	ON 
		C.numCompanyId=D.numCompanyID                  
	WHERE 
		numDivisionID =@numDivisionID 		

	DECLARE @TEMP TABLE
	(
		ID INT IDENTITY(1,1)
		,numOppItemID NUMERIC(18,0)
        ,numItemCode NUMERIC(18,0)
        ,numUnits FLOAT
		,numWarehouseItemID NUMERIC(18,0)
        ,monUnitPrice DECIMAL(20,5)
        ,fltDiscount DECIMAL(20,5)
        ,bitDiscountType BIT
        ,monTotalAmount DECIMAL(20,5)
        ,numPromotionID NUMERIC(18,0)
        ,bitPromotionTriggered BIT
        ,vcPromotionDescription VARCHAR(MAX)
		,vcKitChildItems VARCHAR(MAX)
		,vcInclusionDetail VARCHAR(MAX)
		,bitChanged BIT		
	)

	IF ISNULL(@vcItems,'') <> ''
	BEGIN
		EXEC sp_xml_preparedocument @hDocItem OUTPUT, @vcItems

		INSERT INTO @TEMP
		(
			numOppItemID
			,numItemCode
			,numUnits
			,numWarehouseItemID
			,monUnitPrice
			,fltDiscount
			,bitDiscountType
			,monTotalAmount
			,numPromotionID
			,bitPromotionTriggered
			,vcPromotionDescription
			,vcKitChildItems
			,vcInclusionDetail
			,bitChanged
		)

		SELECT 
			numOppItemID
			,numItemCode
			,numUnits
			,numWarehouseItemID
			,monUnitPrice
			,fltDiscount
			,bitDiscountType
			,monTotalAmount
			,numPromotionID
			,bitPromotionTriggered
			,vcPromotionDescription
			,vcKitChildItems
			,vcInclusionDetail
			,0
		FROM 
			OPENXML (@hDocItem,'/NewDataSet/Item',2)                                                                          
		WITH                       
		(                                                                          
			numOppItemID NUMERIC(18,0)
			,numItemCode NUMERIC(18,0)
			,numUnits FLOAT
			,numWarehouseItemID NUMERIC(18,0)
			,monUnitPrice DECIMAL(20,5)
			,fltDiscount DECIMAL(20,5)
			,bitDiscountType BIT
			,monTotalAmount DECIMAL(20,5)
			,numPromotionID NUMERIC(18,0)
			,bitPromotionTriggered BIT
			,vcKitChildItems VARCHAR(MAX)
			,vcInclusionDetail VARCHAR(MAX)
			,vcPromotionDescription VARCHAR(MAX)
		)

		EXEC sp_xml_removedocument @hDocItem

		DECLARE @i INT = 1
		DECLARE @iCount INT 
		SET @iCount = (SELECT COUNT(*) FROM @TEMP)

		DECLARE @j INT = 1
		DECLARE @jCount INT
		DECLARE @numTempPromotionID NUMERIC(18,0)
		DECLARE @vcShortDesc VARCHAR(500)
		DECLARE @tintItemCalDiscount TINYINT
		DECLARE @numItemCode NUMERIC(18,0)
		DECLARE @numTempWarehouseItemID NUMERIC(18,0)
		DECLARE @numUnits FLOAT
		DECLARE @monTotalAmount DECIMAL(20,5)
		DECLARE @numItemClassification AS NUMERIC(18,0)

		DECLARE @bitRemainingCheckRquired AS BIT
		DECLARE @numRemainingPromotion FLOAT
		DECLARE @fltDiscountValue FLOAT
		DECLARE @tintDiscountType TINYINT
		DECLARE @tintDiscoutBaseOn TINYINT

		DECLARE @TEMPPromotion TABLE
		(
			ID INT IDENTITY(1,1)
			,numPromotionID NUMERIC(18,0)
			,vcShortDesc VARCHAR(500)
			,numTriggerItemCode NUMERIC(18,0)
			,numWarehouseItemID NUMERIC(18,0)
			,numUnits FLOAT
			,monTotalAmount DECIMAL(20,5)
		)

		INSERT INTO @TEMPPromotion
		(
			numPromotionID
			,numTriggerItemCode
			,numWarehouseItemID
			,numUnits
			,monTotalAmount
		)
		SELECT
			numPromotionID
			,numItemCode
			,numWarehouseItemID
			,numUnits
			,monTotalAmount
		FROM
			@TEMP
		WHERE
			ISNULL(numPromotionID,0) > 0
			AND ISNULL(bitPromotionTriggered,0) = 1

		SET @jCount = ISNULL((SELECT COUNT(*) FROM @TEMPPromotion),0)

		WHILE @j <= @jCount
		BEGIN
			SELECT 
				@numTempPromotionID=numPromotionID
				,@numItemCode=numTriggerItemCode
				,@numTempWarehouseItemID=numWarehouseItemID
				,@numUnits = numUnits
				,@monTotalAmount=monTotalAmount
			FROM 
				@TEMPPromotion 
			WHERE 
				ID = @j

			
			SET @numItemClassification = ISNULL((SELECT numItemClassification FROM Item WHERE Item.numItemCode = @numItemClassification),0)

			-- FIRST CHECK WHETHER PROMOTION IS STILL VALID
			IF EXISTS (SELECT 
							numProId 
						FROM 
							PromotionOffer PO
						WHERE 
							numDomainId=@numDomainID 
							AND numProId=@numTempPromotionID
							AND ISNULL(bitEnabled,0)=1 
							AND ISNULL(IsOrderBasedPromotion,0) = 0
							AND 1 = (CASE WHEN bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN GETUTCDATE()>=dtValidFrom AND GETUTCDATE()<=dtValidTo THEN 1 ELSE 0 END) END)
							AND 1 = (CASE tintCustomersBasedOn 
										WHEN 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=PO.numProId AND tintType=1 AND numDivisionID=@numDivisionID) > 0 THEN 1 ELSE 0 END)
										WHEN 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=PO.numProId AND tintType=2 AND numRelationship=@numRelationship AND numProfile=@numProfile) > 0 THEN 1 ELSE 0 END)
										WHEN 3 THEN 1
										ELSE 0
									END)
							AND 1 = (CASE 
										WHEN tintOfferBasedOn = 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=1 AND numValue=@numItemCode) > 0 THEN 1 ELSE 0 END)
										WHEN tintOfferBasedOn = 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=2 AND numValue=@numItemClassification) > 0 THEN 1 ELSE 0 END)
										WHEN tintOfferBasedOn = 4 THEN 1 
										ELSE 0
									END)
							AND 1 = (CASE 
										WHEN ISNULL(tintOfferTriggerValueType,1) = 1 --Based on quantity
										THEN
											CASE
												WHEN ISNULL(tintOfferTriggerValueTypeRange,1) = 2
												THEN
													(CASE WHEN ISNULL(@numUnits,0) BETWEEN ISNULL(fltOfferTriggerValue,0) AND ISNULL(fltOfferTriggerValueRange,0) THEN 1 ELSE 0 END)
												ELSE
													(CASE WHEN ISNULL(@numUnits,0) >= fltOfferTriggerValue THEN 1 ELSE 0 END)
											END
										WHEN ISNULL(tintOfferTriggerValueType,1) = 2 --Based on amount
										THEN
											CASE
												WHEN ISNULL(tintOfferTriggerValueTypeRange,1) = 2
												THEN
													(CASE WHEN ISNULL(@monTotalAmount,0) BETWEEN ISNULL(fltOfferTriggerValue,0) AND ISNULL(fltOfferTriggerValueRange,0) THEN 1 ELSE 0 END)
												ELSE
													(CASE WHEN ISNULL(@monTotalAmount,0) >= fltOfferTriggerValue THEN 1 ELSE 0 END)
											END
									END)
					)
			BEGIN
				SELECT
					@fltDiscountValue=ISNULL(fltDiscountValue,0)
					,@tintDiscountType=ISNULL(tintDiscountType,0)
					,@tintDiscoutBaseOn=tintDiscoutBaseOn
					,@vcShortDesc=ISNULL(vcShortDesc,'')
					,@tintItemCalDiscount=ISNULL(tintItemCalDiscount,1)
				FROM
					PromotionOffer
				WHERE
					numProId=@numTempPromotionID

				-- If discount is based on amount or quantity than we have to checked remaining amount or qty left to give as discount 
				IF @tintDiscountType = 2 OR @tintDiscountType = 3
				BEGIN
					SET @bitRemainingCheckRquired = 1
				END
				ELSE
				BEGIN
					SET @bitRemainingCheckRquired = 0
					SET @numRemainingPromotion = 0
				END

				-- If promotion is valid than check whether any item left to apply promotion
				SET @i = 1

				WHILE @i <= @iCount
				BEGIN
					IF @bitRemainingCheckRquired=1
					BEGIN
						IF @tintDiscountType = 2 -- Discount by amount
						BEGIN
							SET @numRemainingPromotion = @fltDiscountValue - ISNULL((SELECT SUM(fltDiscount) FROM @TEMP WHERE numPromotionID = @numTempPromotionID	AND ISNULL(bitDiscountType,0)=1 AND bitPromotionTriggered = 0),0)
						END
						ELSE IF @tintDiscountType = 3 -- Discount by quantity
						BEGIN
							SET @numRemainingPromotion = @fltDiscountValue - ISNULL((SELECT SUM(fltDiscount/monUnitPrice) FROM @TEMP WHERE numPromotionID = @numTempPromotionID	AND ISNULL(bitDiscountType,0)=1 AND bitPromotionTriggered = 0),0)
						END
					END

					IF @bitRemainingCheckRquired=0 OR (@bitRemainingCheckRquired=1 AND @numRemainingPromotion > 0)
					BEGIN
						PRINT @i
						UPDATE
							T1
						SET
							monUnitPrice= CASE 
												WHEN (ISNULL(bitKitParent,0)=1 OR ISNULL(bitAssembly,0) = 1) AND ISNULL(bitCalAmtBasedonDepItems,0) = 1
												THEN dbo.GetCalculatedPriceForKitAssembly(@numDomainID,I.numItemCode,@numTempWarehouseItemID,ISNULL(tintKitAssemblyPriceBasedOn,1),0,0,ISNULL(T1.vcKitChildItems,''))
												ELSE (CASE WHEN @tintItemCalDiscount = 1 THEN ISNULL(T2.monFinalPrice,0) ELSE ISNULL(I.monListPrice,0) END)
											END
							,fltDiscount = (CASE 
												WHEN @bitRemainingCheckRquired=0 
												THEN 
													@fltDiscountValue
												ELSE 
													(CASE 
														WHEN @tintDiscountType = 2 -- Discount by amount
														THEN
															(CASE 
																WHEN (T1.numUnits * (CASE 
																									WHEN (ISNULL(bitKitParent,0)=1 OR ISNULL(bitAssembly,0) = 1) AND ISNULL(bitCalAmtBasedonDepItems,0) = 1
																									THEN dbo.GetCalculatedPriceForKitAssembly(@numDomainID,I.numItemCode,@numTempWarehouseItemID,ISNULL(tintKitAssemblyPriceBasedOn,1),0,0,ISNULL(T1.vcKitChildItems,''))
																									ELSE (CASE WHEN @tintItemCalDiscount = 1 THEN ISNULL(T2.monFinalPrice,0) ELSE ISNULL(I.monListPrice,0) END)
																								END)) >= @numRemainingPromotion 
																THEN @numRemainingPromotion 
																ELSE (T1.numUnits * (CASE 
																									WHEN (ISNULL(bitKitParent,0)=1 OR ISNULL(bitAssembly,0) = 1) AND ISNULL(bitCalAmtBasedonDepItems,0) = 1
																									THEN dbo.GetCalculatedPriceForKitAssembly(@numDomainID,I.numItemCode,@numTempWarehouseItemID,ISNULL(tintKitAssemblyPriceBasedOn,1),0,0,ISNULL(T1.vcKitChildItems,''))
																									ELSE (CASE WHEN @tintItemCalDiscount = 1 THEN ISNULL(T2.monFinalPrice,0) ELSE ISNULL(I.monListPrice,0) END)
																								END)) 
															END)
														WHEN @tintDiscountType = 3 -- Discount by quantity
														THEN
															(CASE 
																WHEN T1.numUnits >= @numRemainingPromotion 
																THEN (@numRemainingPromotion * (CASE 
																									WHEN (ISNULL(bitKitParent,0)=1 OR ISNULL(bitAssembly,0) = 1) AND ISNULL(bitCalAmtBasedonDepItems,0) = 1
																									THEN dbo.GetCalculatedPriceForKitAssembly(@numDomainID,I.numItemCode,@numTempWarehouseItemID,ISNULL(tintKitAssemblyPriceBasedOn,1),0,0,ISNULL(T1.vcKitChildItems,''))
																									ELSE (CASE WHEN @tintItemCalDiscount = 1 THEN ISNULL(T2.monFinalPrice,0) ELSE ISNULL(I.monListPrice,0) END)
																								END)) 
																ELSE (T1.numUnits * (CASE 
																						WHEN (ISNULL(bitKitParent,0)=1 OR ISNULL(bitAssembly,0) = 1) AND ISNULL(bitCalAmtBasedonDepItems,0) = 1
																						THEN dbo.GetCalculatedPriceForKitAssembly(@numDomainID,I.numItemCode,@numTempWarehouseItemID,ISNULL(tintKitAssemblyPriceBasedOn,1),0,0,ISNULL(T1.vcKitChildItems,''))
																						ELSE (CASE WHEN @tintItemCalDiscount = 1 THEN ISNULL(T2.monFinalPrice,0) ELSE ISNULL(I.monListPrice,0) END)
																					END)) 
															END)
														ELSE 0
													END)
												END
											)
							,bitDiscountType = (CASE WHEN @bitRemainingCheckRquired=0 THEN 0 ELSE 1 END)
							,numPromotionID=@numTempPromotionID
							,bitPromotionTriggered=0
							,vcPromotionDescription=@vcShortDesc
							,bitChanged=1
						FROM
							@TEMP T1
						INNER JOIN
							Item I
						ON
							T1.numItemCode = I.numItemCode
						CROSS APPLY
							dbo.fn_GetItemPriceBasedOnMethod(@numDomainID,@numDivisionID,T1.numItemCode,T1.numUnits) T2
						WHERE
							ID=@i
							AND ISNULL(T1.numPromotionID,0) = 0
							AND 1 = (CASE 
										WHEN @tintDiscoutBaseOn = 1 -- Selected Items
										THEN
											(CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=@numTempPromotionID AND tintRecordType=6 AND tintType=1 AND numValue=I.numItemCode) > 0 THEN 1 ELSE 0 END)
										WHEN @tintDiscoutBaseOn = 2 -- Selected Item Classification
										THEN 
											(CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=@numTempPromotionID AND tintRecordType=6 AND tintType=2 AND numValue=ISNULL(I.numItemClassification,0)) > 0 THEN 1 ELSE 0 END)
										WHEN @tintDiscoutBaseOn = 3 -- Related Items
										THEN
											(CASE WHEN (SELECT COUNT(*) FROM SimilarItems WHERE numDomainId=@numDomainID AND numParentItemCode=@numItemCode AND numItemCode=I.numItemCode) > 0 THEN 1 ELSE 0 END)
										WHEN @tintDiscoutBaseOn = 4 -- Item with list price lesser or equal
										THEN
											(CASE WHEN ISNULL(I.monListPrice,0) <= ISNULL((SELECT monListPrice FROM Item WHERE numItemCode=@numItemCode),0) THEN 1 ELSE 0 END)
										ELSE 0
									END)
					END

					SET @i = @i + 1
				END
			END
			ELSE
			BEGIN
				-- IF Promotion is not valid than revert line item price to based on default pricing
				UPDATE 
					T1
				SET 
					numPromotionID=0
					,bitPromotionTriggered=0
					,vcPromotionDescription=''
					,bitChanged=1
					,monUnitPrice=ISNULL(T2.monPrice,0)
					,fltDiscount=ISNULL(T2.decDiscount,0)
					,bitDiscountType=ISNULL(T2.tintDisountType,0)
				FROM
					@TEMP T1
				CROSS APPLY
					dbo.fn_GetItemPriceBasedOnMethod(@numDomainID,@numDivisionID,T1.numItemCode,T1.numUnits) T2
				WHERE 
					numPromotionID=@numTempPromotionID
			END

			SET @j = @j + 1
		END
			
	END

	SELECT * FROM @TEMP WHERE bitChanged=1
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DiscountCodes_Validate')
DROP PROCEDURE USP_DiscountCodes_Validate
GO
CREATE PROCEDURE [dbo].[USP_DiscountCodes_Validate]
	@numDomainID NUMERIC(18,0)
	,@numSiteID NUMERIC(18,0)
	,@numDivisionId NUMERIC(18,0)
	,@txtCouponCode VARCHAR(100)
AS
BEGIN
	DECLARE @numRelationship NUMERIC(18,0) = 0
	DECLARE @numProfile NUMERIC(18,0) = 0

	IF ISNULL(@numDivisionID,0) > 0
	BEGIN
		SELECT 
			@numRelationship=ISNULL(numCompanyType,0)
			,@numProfile=ISNULL(vcProfile,0) 
		FROM 
			DivisionMaster 
		INNER JOIN 
			CompanyInfo 
		ON 
			DivisionMaster.numCompanyID=CompanyInfo.numCompanyId 
		WHERE 
			numDivisionID=@numDivisionID
	END
	ELSE IF ISNULL(@numSiteID,0) > 0
	BEGIN
		SELECT
			@numRelationship=ISNULL(numRelationshipId,0)
			,@numProfile=ISNULL(numProfileId,0)
		FROM
			eCommerceDTL
		WHERE
			numSiteID=@numSiteID
	END

	DECLARE @numPromotionID NUMERIC(18,0)
	DECLARE @bitPromotionNeverExpires BIT
	DECLARE @dtPromotionStartDate DATETIME
	DECLARE @dtPromotionEndDate DATETIME
	DECLARE @intUsageLimit INT
	DECLARE @numDisocuntID NUMERIC(18,0)

	IF @numDomainID <> 204
	BEGIN
		-- FIRST CHECK WHETHER COUNPON CODE IS AVAILABLE
		SELECT 
			@numPromotionID=PO.numProId
			,@intUsageLimit=ISNULL(DC.CodeUsageLimit,0)
			,@bitPromotionNeverExpires=ISNULL(bitNeverExpires,0)
			,@dtPromotionStartDate=PO.dtValidFrom
			,@dtPromotionEndDate=PO.dtValidTo
			,@numDisocuntID=numDiscountId
		FROM 
			PromotionOffer PO
		INNER JOIN 
			PromotionOfferOrganizations PORG
		ON 
			PO.numProId = PORG.numProId
		INNER JOIN 
			DiscountCodes DC
		ON 
			PO.numProId = DC.numPromotionID
		WHERE
			PO.numDomainId = @numDomainID
			AND numRelationship=@numRelationship 
			AND numProfile=@numProfile
			AND DC.vcDiscountCode = @txtCouponCode

		IF ISNULL(@numPromotionID,0) > 0
		BEGIN
			-- NOW CHECK IF PROMOTION EXPIRES OR NOT
			IF ISNULL(@bitPromotionNeverExpires,0) = 0 AND (@dtPromotionStartDate <= GETUTCDATE() AND @dtPromotionEndDate >= GETUTCDATE())
			BEGIN
				IF ISNULL(@intUsageLimit,0) <> 0
				BEGIN
					IF ISNULL((SELECT intCodeUsed FROM DiscountCodeUsage WHERE numDiscountId=@numDisocuntID AND numDivisionId=@numDivisionId),0) >= @intUsageLimit
					BEGIN
						RAISERROR('COUPON_USAGE_LIMIT_EXCEEDED',16,1)
						RETURN
					END
				END
			END
			ELSE
			BEGIN
				RAISERROR('COUPON_CODE_EXPIRED',16,1)
				RETURN
			END
		END
		ELSE
		BEGIN
			RAISERROR('INVALID_COUPON_CODE',16,1)
			RETURN
		END
	END

	SELECT 
		@numPromotionID AS numPromotionID
		,@numDisocuntID AS numDisocuntID
		,@txtCouponCode AS vcCouponCode
END
GO
GO
/****** Object:  StoredProcedure [dbo].[USP_GET_PACKAGES]    Script Date: 30/07/2012 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO

IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GET_PACKAGES' ) 
    DROP PROCEDURE USP_GET_PACKAGES
GO
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE USP_GET_PACKAGES
@DomainId NUMERIC(18,0)
AS 
    BEGIN
		
		SELECT
			0 AS numCustomPackageID,
			0 AS numPackageTypeID,
			'None' AS vcPackageName,
			0.00 AS fltWidth,
			0.00 AS fltHeight,
			0.00 AS fltLength,
			0.00 AS fltTotalWeight,
			0 AS numShippingCompanyID,
			'' AS vcShippingCompany,
			CONVERT(VARCHAR(10),0) + ',' + CONVERT(VARCHAR(10),0.00) +  ',' + CONVERT(VARCHAR(10),0.00) +  ',' + CONVERT(VARCHAR(10),0.00) +  ',' + CONVERT(VARCHAR(10),0.00)  AS [ValueDimension]
UNION 
		SELECT
			numItemCode AS numCustomPackageID,
			0 AS numPackageTypeID,
			vcItemName AS vcPackageName,
			fltWidth,
			fltHeight,
			fltLength,
			fltWeight AS fltTotalWeight,
			0 AS numShippingCompanyID,
			'Custom Box' AS vcShippingCompany,
			CONVERT(VARCHAR(10),0) + ',' + CONVERT(VARCHAR(10),fltWidth) +  ',' + CONVERT(VARCHAR(10),fltHeight) +  ',' + CONVERT(VARCHAR(10),fltLength) +  ',' + CONVERT(VARCHAR(10),fltWeight)  AS [ValueDimension]
		FROM 
			Item 
		where 
			bitContainer=1 AND
			numDomainID=@DomainId

        --SELECT  numCustomPackageID,
        --        numPackageTypeID,
        --        vcPackageName,
        --        fltWidth,
        --        fltHeight,
        --        fltLength,
        --        fltTotalWeight,
        --        ISNULL(numShippingCompanyID,0) AS [numShippingCompanyID],
        --        CASE WHEN numShippingCompanyID = 91 THEN 'Fedex'
        --             WHEN numShippingCompanyID = 88 THEN 'UPS'
        --             WHEN numShippingCompanyID = 90 THEN 'USPS'
        --             ELSE 'Custom Box'
        --        END AS vcShippingCompany,
        --        CONVERT(VARCHAR(10),numPackageTypeID) + ',' + CONVERT(VARCHAR(10),fltWidth) +  ',' + CONVERT(VARCHAR(10),fltHeight) +  ',' + CONVERT(VARCHAR(10),fltLength) +  ',' + CONVERT(VARCHAR(10),fltTotalWeight)  AS [ValueDimension]
        --FROM    dbo.CustomPackages CP
        --ORDER BY vcShippingCompany ASC

    END

/****** Object:  StoredProcedure [dbo].[USP_GetDealsList1]    Script Date: 05/07/2009 22:12:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getdealslist1')
DROP PROCEDURE usp_getdealslist1
GO
CREATE PROCEDURE [dbo].[USP_GetDealsList1]
               @numUserCntID              NUMERIC(9)  = 0,
               @numDomainID               NUMERIC(9)  = 0,
               @tintSalesUserRightType    TINYINT  = 0,
               @tintPurchaseUserRightType TINYINT  = 0,
               @tintSortOrder             TINYINT  = 4,
               @CurrentPage               INT,
               @PageSize                  INT,
               @TotRecs                   INT  OUTPUT,
               @columnName                AS VARCHAR(50),
               @columnSortOrder           AS VARCHAR(10),
               @numCompanyID              AS NUMERIC(9)  = 0,
               @bitPartner                AS BIT  = 0,
               @ClientTimeZoneOffset      AS INT,
               @tintShipped               AS TINYINT,
               @intOrder                  AS TINYINT,
               @intType                   AS TINYINT,
               @tintFilterBy              AS TINYINT  = 0,
               @numOrderStatus			  AS NUMERIC,
			   @vcRegularSearchCriteria varchar(MAX)='',
			   @vcCustomSearchCriteria varchar(MAX)='',
			   @SearchText VARCHAR(300) = '',
			   @SortChar char(1)='0',
			   @tintDashboardReminderType TINYINT = 0
AS
	DECLARE  @PageId  AS TINYINT
	DECLARE  @numFormId  AS INT 
  
	SET @PageId = 0

	IF @inttype = 1
	BEGIN
		SET @PageId = 2
		SET @inttype = 3
		SET @numFormId=39
	END
	ELSE IF @inttype = 2
	BEGIN
		SET @PageId = 6
		SET @inttype = 4
		SET @numFormId=41
	END

	CREATE TABLE #tempForm 
	(
		tintOrder TINYINT,vcDbColumnName nvarchar(50),vcOrigDbColumnName nvarchar(50),vcFieldName nvarchar(50),vcAssociatedControlType nvarchar(50),vcListItemType varchar(3),
		numListID numeric(9),vcLookBackTableName varchar(50),bitCustomField BIT,numFieldId NUMERIC,bitAllowSorting BIT,bitAllowEdit BIT,bitIsRequired BIT,bitIsEmail BIT,
		bitIsAlphaNumeric BIT,bitIsNumeric BIT,bitIsLengthValidation BIT,intMaxLength int,intMinLength int,bitFieldMessage BIT,vcFieldMessage VARCHAR(500),
		ListRelID numeric(9),intColumnWidth int,bitAllowFiltering BIT,vcFieldDataType CHAR(1)
	)

	-- FIRST CHECK IF GRID COLUMNS ARE CONFIGURED
	DECLARE  @Nocolumns  AS TINYINT = 0

	SELECT 
		@Nocolumns=ISNULL(SUM(TotalRow),0) 
	FROM
	(            
		SELECT 
			COUNT(*) TotalRow 
		FROM 
			View_DynamicColumns 
		WHERE 
			numFormId=@numFormId 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND numRelCntType = @numFormId
		UNION 
		SELECT 
			COUNT(*) TotalRow 
		FROM 
			View_DynamicCustomColumns 
		WHERE 
			numFormId=@numFormId 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND numRelCntType = @numFormId
	) TotalRows

	-- IF GRID COLUMNS ARE NOT CONFIGURES THEN CHECK IF MASTER FORM CONFIGURATION IS CREATED BY ADMINISTRATOR
	DECLARE @IsMasterConfAvailable BIT = 0
	DECLARE @numUserGroup NUMERIC(18,0)

	IF @Nocolumns=0
	BEGIN
		SELECT @numUserGroup = numGroupID FROM UserMaster WHERE numUserDetailId = @numUserCntID 

		IF (SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = @numDomainID AND numFormID = @numFormId AND numRelCntType=@numFormId AND bitGridConfiguration = 1 AND numGroupID = @numUserGroup) > 0
		BEGIN
			SET @IsMasterConfAvailable = 1
		END
	END

	--IF MASTERCONFIGURATION IS AVAILABLE THEN LOAD IT OTHERWISE LOAD DEFAULT COLUMNS
	IF @IsMasterConfAvailable = 1
	BEGIN
		INSERT INTO DycFormConfigurationDetails 
		(
			numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth
		)
		SELECT 
			@numFormId,numFieldId,intColumnNum,intRowNum,@numDomainID,@numUserCntID,@numFormId,1,0,intColumnWidth
		FROM 
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId=@numFormId AND 
			View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicColumnsMasterConfig.numRelCntType=@numFormId AND 
			View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
			ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
		UNION
		SELECT 
			@numFormId,numFieldId,intColumnNum,intRowNum,@numDomainID,@numUserCntID,@numFormId,1,1,intColumnWidth
		FROM 
			View_DynamicCustomColumnsMasterConfig
		WHERE 
			View_DynamicCustomColumnsMasterConfig.numFormId=@numFormId AND 
			View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicCustomColumnsMasterConfig.numRelCntType=@numFormId AND 
			View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1


		INSERT INTO 
			#tempForm
		SELECT 
			(intRowNum + 1) as tintOrder, vcDbColumnName,	vcOrigDbColumnName,	ISNULL(vcCultureFieldName,vcFieldName), vcAssociatedControlType,
			vcListItemType, numListID, vcLookBackTableName,	bitCustom, numFieldId, bitAllowSorting,	bitInlineEdit, bitIsRequired,
			bitIsEmail,	bitIsAlphaNumeric, bitIsNumeric, bitIsLengthValidation,	intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,
			ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType 
		FROM 
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId=@numFormId AND 
			View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicColumnsMasterConfig.numRelCntType=@numFormId AND 
			View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
			ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
		UNION
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName, vcAssociatedControlType,'' as vcListItemType,numListID,'',
			bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,''
		FROM 
			View_DynamicCustomColumnsMasterConfig
		WHERE 
			View_DynamicCustomColumnsMasterConfig.numFormId=@numFormId AND 
			View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicCustomColumnsMasterConfig.numRelCntType=@numFormId AND 
			View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1
		ORDER BY 
			tintOrder ASC  
	END
	ELSE                                        
	BEGIN
		IF @Nocolumns=0
		BEGIN
			INSERT INTO DycFormConfigurationDetails 
			(
				numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth
			)
			SELECT 
				@numFormId,numFieldId,0,Row_number() over(order by tintRow desc),@numDomainID,@numUserCntID,@numFormId,1,0,intColumnWidth
			FROM 
				View_DynamicDefaultColumns
			WHERE 
				numFormId=@numFormId 
				AND bitDefault=1 
				AND ISNULL(bitSettingField,0)=1 
				AND numDomainID=@numDomainID
			ORDER BY 
				tintOrder asc   
		END

		INSERT INTO 
			#tempForm
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcOrigDbColumnName,ISNULL(vcCultureFieldName,vcFieldName),                  
			vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName,bitCustom,numFieldId,bitAllowSorting,bitInlineEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength,bitFieldMessage,
			vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType  
		FROM 
			View_DynamicColumns 
		WHERE 
			numFormId=@numFormId 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND ISNULL(bitSettingField,0)=1 
			AND ISNULL(bitCustom,0)=0  
			AND numRelCntType = @numFormId
		UNION
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName,vcAssociatedControlType,'' as vcListItemType,numListID,'',bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,'' 
		FROM 
			View_DynamicCustomColumns
		WHERE 
			numFormId=@numFormId 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND ISNULL(bitCustom,0)=1 
			AND numRelCntType = @numFormId
		ORDER BY 
			tintOrder asc  
	END

	DECLARE  @strColumns AS VARCHAR(MAX)
	SET @strColumns = ' ADC.numContactId, ADC.vcEmail, Div.numDivisionID,ISNULL(Div.numTerID,0) numTerID, opp.numRecOwner as numRecOwner, Div.tintCRMType, opp.numOppId, ISNULL(Opp.monDealAmount,0) monDealAmount 
	,ISNULL(opp.numShippingService,0) AS numShippingService'
	--Corr.bitIsEmailSent,

	DECLARE  @tintOrder  AS TINYINT;SET @tintOrder = 0
	DECLARE  @vcFieldName  AS VARCHAR(50)
	DECLARE  @vcListItemType  AS VARCHAR(3)
	DECLARE  @vcListItemType1  AS VARCHAR(1)
	DECLARE  @vcAssociatedControlType VARCHAR(30)
	DECLARE  @numListID  AS NUMERIC(9)
	DECLARE  @vcDbColumnName VARCHAR(40)
	DECLARE  @WhereCondition VARCHAR(2000);SET @WhereCondition = ''
	DECLARE  @vcLookBackTableName VARCHAR(2000)
	DECLARE  @bitCustom  AS BIT
	Declare @numFieldId as numeric  
	DECLARE @bitAllowSorting AS CHAR(1)   
	DECLARE @bitAllowEdit AS CHAR(1)                 
	DECLARE @vcColumnName AS VARCHAR(500)          
	
	Declare @ListRelID as numeric(9) 
	DECLARE @SearchQuery AS VARCHAR(MAX) = ''        

	SELECT TOP 1 
		@tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName,@vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,
		@numListID=numListID,@vcLookBackTableName=vcLookBackTableName ,@bitCustom=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,@bitAllowEdit=bitAllowEdit,
		@ListRelID=ListRelID
	FROM 
		#tempForm 
	ORDER BY 
		tintOrder ASC            
--	SET @strColumns = @strColumns + ' , (SELECT SUBSTRING((SELECT '', '' + cast(I.vcItemName as varchar)
--                      FROM OpportunityItems AS t
--					  LEFT JOIN OpportunityBizDocs as BC
--					  ON t.numOppId=BC.numOppId
--LEFT JOIN Item as I
--ON t.numItemCode=I.numItemCode
--WHERE t.numOppId=Opp.numOppId AND t.numoppItemtCode NOT IN (SELECT numOppItemId FROM OpportunityBizDocItems WHERE numOppBizDocID=BC.numOppBizDocsId)
--                     FOR XML PATH('''')), 2, 200000)  )[List_Item_Approval_UNIT] '
--SET @strColumns = @strColumns + ' , (SELECT 
-- SUBSTRING((SELECT '', '' + cast(I.vcItemName as varchar)
--FROM 
-- OpportunityItems AS t
--LEFT JOIN 
-- Item as I
--ON 
-- t.numItemCode=I.numItemCode
--WHERE 
-- t.numOppId=Opp.numOppId AND t.numoppItemtCode NOT IN (SELECT numOppItemId FROM OpportunityBizDocs JOIN OpportunityBizDocItems ON OpportunityBizDocs.numOppBizDocsId=OpportunityBizDocItems.numOppBizDocID  WHERE OpportunityBizDocs.numOppId=Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1)
--                     FOR XML PATH('''')), 2, 200000)  )[List_Item_Approval_UNIT] '
	WHILE @tintOrder > 0
    BEGIN
		IF @bitCustom = 0
		BEGIN
			DECLARE  @Prefix  AS VARCHAR(10)

			IF @vcLookBackTableName = 'AdditionalContactsInformation'
				SET @Prefix = 'ADC.'
			IF @vcLookBackTableName = 'DivisionMaster'
				SET @Prefix = 'Div.'
			IF @vcLookBackTableName = 'OpportunityMaster'
				SET @PreFix = 'Opp.'
			IF @vcLookBackTableName = 'OpportunityRecurring'
				SET @PreFix = 'OPR.'
			IF @vcLookBackTableName = 'CompanyInfo'
				SET @PreFix = 'cmp.'
			IF @vcLookBackTableName = 'ProjectProgress'
				SET @PreFix = 'PP.'
			SET @vcColumnName= @vcDbColumnName+'~'+ convert(varchar(10),@numFieldId) +'~0'

			IF @vcAssociatedControlType = 'SelectBox'
			BEGIN
				-- KEEP THIS IF CONDITION SEPERATE FROM IF ELSE
				IF @vcDbColumnName = 'numStatus'
				BEGIN
					SET @strColumns=@strColumns+ ' ,(SELECT COUNT(OpportunityItems.numoppitemtCode) FROM OpportunityItems WHERE OpportunityItems.bitItemPriceApprovalRequired=1 AND OpportunityItems.numOppId=Opp.numOppId) ApprovalMarginCount'
				END


				IF @vcDbColumnName = 'numPartner'
				BEGIN
					SET @strColumns=@strColumns+ ' ,(Opp.numPartner) '+' ['+ @vcColumnName+']'    
					SET @strColumns=@strColumns+ ' ,(SELECT D.vcPartnerCode+''-''+C.vcCompanyName FROM DivisionMaster AS D LEFT JOIN CompanyInfo AS C ON D.numCompanyID=C.numCompanyID WHERE D.numDivisionID=Opp.numPartner) AS vcPartner' 
				END
				ELSE IF @vcDbColumnName = 'vcSignatureType'
				BEGIN
					SET @strColumns=@strColumns+ ' ,(SELECT CASE WHEN vcSignatureType = 0 THEN ''Service Default''
																	WHEN vcSignatureType = 1 THEN ''Adult Signature Required''
																	WHEN vcSignatureType = 2 THEN ''Direct Signature''
																	WHEN vcSignatureType = 3 THEN ''InDirect Signature''
																	WHEN vcSignatureType = 4 THEN ''No Signature Required''
																ELSE '''' END 
					FROM DivisionMaster AS D INNER JOIN DivisionMasterShippingConfiguration AS DMSC ON DMSC.numDivisionID=D.numDivisionID 
					WHERE D.numDivisionID=Div.numDivisionID) '  + ' [' + @vcColumnName + ']'
				END
				ELSE IF @vcDbColumnName = 'numDefaultShippingServiceID'
				BEGIN
					SET @strColumns=@strColumns+ ' ,ISNULL((SELECT vcShipmentService FROM ShippingService WHERE (numDomainID=D.numDomainID OR ISNULL(numDomainID,0)=0) AND numShippingServiceID = Div.numDefaultShippingServiceID),'''')' + ' [' + @vcColumnName + ']'
				END
				ELSE IF @vcDbColumnName = 'tintEDIStatus'
				BEGIN
					--SET @strColumns = @strColumns + ',(CASE Opp.tintEDIStatus WHEN 1 THEN ''850 Received'' WHEN 2 THEN ''850 Acknowledged'' WHEN 3 THEN ''940 Sent'' WHEN 4 THEN ''940 Acknowledged'' WHEN 5 THEN ''856 Received'' WHEN 6 THEN ''856 Acknowledged'' WHEN 7 THEN ''856 Sent'' ELSE '''' END)' + ' [' + @vcColumnName + ']'
					SET @strColumns = @strColumns + ',(CASE Opp.tintEDIStatus WHEN 2 THEN ''850 Acknowledged'' WHEN 3 THEN ''940 Sent'' WHEN 4 THEN ''940 Acknowledged'' WHEN 5 THEN ''856 Received'' WHEN 6 THEN ''856 Acknowledged'' WHEN 7 THEN ''856 Sent'' WHEN 11 THEN ''850 Partially Created'' WHEN 12 THEN ''850 SO Created'' ELSE '''' END)' + ' [' + @vcColumnName + ']'
				END
				ELSE IF @vcDbColumnName = 'numPartenerContact'
				BEGIN
					SET @strColumns=@strColumns+ ' ,(Opp.numPartenerContact) '+' ['+ @vcColumnName+']'    
					SET @strColumns=@strColumns+ ' ,(SELECT A.vcFirstName+'' ''+ A.vcLastName FROM AdditionalContactsInformation AS A WHERE A.numContactId=Opp.numPartenerContact) AS vcPartenerContact' 
				END
				ELSE IF @vcDbColumnName = 'tintInvoicing'
				BEGIN
					SET @strColumns=CONCAT(@strColumns,', CONCAT(ISNULL((SELECT SUBSTRING((SELECT ''$^$'' + CONCAT(numOppBizDocsId,''#^#'',vcBizDocID,''#^#'',ISNULL(bitisemailsent,0),''#^#'')
										FROM OpportunityBizDocs WHERE numOppId=Opp.numOppId AND numBizDocId = ISNULL((SELECT TOP 1 numAuthoritativeSales FROM AuthoritativeBizDocs WHERE numDomainId=' + CAST (@numDomainID AS VARCHAR) + '),287) FOR XML PATH('''')),4,200000)),'''')',
										CASE 
										WHEN CHARINDEX('tintInvoicing=3',@vcRegularSearchCriteria) > 0
											THEN ', (SELECT 
													CONCAT(''('',SUM(X.InvoicedQty),'' Out of '',SUM(X.OrderedQty),'')'')
												FROM 
												(
													SELECT
														OI.numoppitemtCode,
														ISNULL(OI.numUnitHour,0) AS OrderedQty,
														ISNULL(TempInvoice.InvoicedQty,0) AS InvoicedQty
													FROM
														OpportunityItems OI
													INNER JOIN
														Item I
													ON
														OI.numItemCode = I.numItemCode
													OUTER APPLY
													(
														SELECT
															SUM(OpportunityBizDocItems.numUnitHour) AS InvoicedQty
														FROM
															OpportunityBizDocs
														INNER JOIN
															OpportunityBizDocItems 
														ON
															OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
														WHERE
															OpportunityBizDocs.numOppId = Opp.numOppId
															AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
															AND ISNULL(OpportunityBizDocs.bitAuthoritativeBizDocs,0) = 1
													) AS TempInvoice
													WHERE
														OI.numOppID = Opp.numOppId
												) X)'

											WHEN CHARINDEX('tintInvoicing=4',@vcRegularSearchCriteria) > 0
											THEN ', (SELECT 
													CONCAT(''('',(X.InvoicedPercentage),'' % '','')'')
												FROM 
												(

													SELECT fltBreakupPercentage  AS InvoicedPercentage
													FROM dbo.OpportunityRecurring OPR 
														INNER JOIN dbo.OpportunityMaster OM ON OPR.numOppID = OM.numOppID
														LEFT JOIN dbo.OpportunityBizDocs OBD ON OBD.numOppBizDocsID = OPR.numOppBizDocID
													WHERE OPR.numOppId = Opp.numOppId 
														and tintRecurringType <> 4
												) X)'

											ELSE 
													','''''
											END,') [', @vcColumnName,']')


				END
				ELSE IF @vcDbColumnName = 'tintSource'
				BEGIN
					SET @strColumns = @strColumns + ',ISNULL(dbo.fn_GetOpportunitySourceValue(ISNULL(Opp.tintSource,0),ISNULL(Opp.tintSourceType,0),Opp.numDomainID),''Internal Order'')' + ' [' + @vcColumnName + ']'

					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'ISNULL(dbo.fn_GetOpportunitySourceValue(ISNULL(Opp.tintSource,0),ISNULL(Opp.tintSourceType,0),Opp.numDomainID),''Internal Order'') LIKE ''%' + @SearchText + '%'''
					END
				END
				ELSE IF @vcDbColumnName = 'numCampainID'
				BEGIN
					SET @strColumns = @strColumns + ', (SELECT vcCampaignName FROM CampaignMaster WHERE numCampaignID= Opp.numCampainID) ' + ' [' + @vcColumnName + ']'
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'ISNULL((SELECT vcCampaignName FROM CampaignMaster WHERE numCampaignID= Opp.numCampainID),'''') LIKE ''%' + @SearchText + '%'''
					END
				END
				ELSE IF @vcDbColumnName = 'numContactID'
				BEGIN
					SET @strColumns = @strColumns + ',dbo.fn_GetContactName(' + @Prefix + @vcDbColumnName + ') [' + @vcColumnName + ']'
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'dbo.fn_GetContactName(' + @Prefix + @vcDbColumnName + ') LIKE ''%' + @SearchText + '%'''
					END
				END
				ELSE IF @vcListItemType = 'LI'
				BEGIN
					SET @strColumns = @strColumns + ',L' + CONVERT(VARCHAR(3),@tintOrder) + '.vcData' + ' [' + @vcColumnName + ']'
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'L' + CONVERT(VARCHAR(3),@tintOrder) + '.vcData LIKE ''%' + @SearchText + '%'''
					END

					SET @WhereCondition = @WhereCondition + ' left Join ListDetails L' + CONVERT(VARCHAR(3),@tintOrder) + ' on L' + CONVERT(VARCHAR(3),@tintOrder) + '.numListItemID=' + @vcDbColumnName
				END
				ELSE IF @vcListItemType = 'S'
				BEGIN
					SET @strColumns = @strColumns + ',S' + CONVERT(VARCHAR(3),@tintOrder) + '.vcState' + ' [' + @vcColumnName + ']'
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'S' + CONVERT(VARCHAR(3),@tintOrder) + '.vcState LIKE ''%' + @SearchText + '%'''
					END

					SET @WhereCondition = @WhereCondition + ' left join State S' + CONVERT(VARCHAR(3),@tintOrder) + ' on S' + CONVERT(VARCHAR(3),@tintOrder) + '.numStateID=' + @vcDbColumnName
				END
				ELSE IF @vcListItemType = 'BP'
				BEGIN
					SET @strColumns = @strColumns + ',SPLM.Slp_Name' + ' [' + @vcColumnName + ']'
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'SPLM.Slp_Name LIKE ''%' + @SearchText + '%'''
					END

					SET @WhereCondition = @WhereCondition + ' LEFT JOIN Sales_process_List_Master SPLM on SPLM.Slp_Id = OPP.numBusinessProcessID '
				END
				ELSE IF @vcListItemType = 'PP'
				BEGIN
					SET @strColumns = @strColumns + ',ISNULL(PP.intTotalProgress,0)' + ' [' + @vcColumnName + ']'

					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'ISNULL(PP.intTotalProgress,0) LIKE ''%' + @SearchText + '%'''
					END
				END
				ELSE IF @vcListItemType = 'T'
				BEGIN
					SET @strColumns = @strColumns + ',L' + CONVERT(VARCHAR(3),@tintOrder) + '.vcData' + ' [' + @vcColumnName + ']'
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'L' + CONVERT(VARCHAR(3),@tintOrder) + '.vcData LIKE ''%' + @SearchText + '%'''
					END

					SET @WhereCondition = @WhereCondition + ' left Join ListDetails L' + CONVERT(VARCHAR(3),@tintOrder) + ' on L' + CONVERT(VARCHAR(3),@tintOrder) + '.numListItemID=' + @vcDbColumnName
				END
				ELSE IF @vcListItemType = 'U'
				BEGIN
					SET @strColumns = @strColumns + ',dbo.fn_GetContactName(' + @Prefix + @vcDbColumnName + ') [' + @vcColumnName + ']'
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'dbo.fn_GetContactName(' + @Prefix + @vcDbColumnName + ') LIKE ''%' + @SearchText + '%'''
					END
				END
				ELSE IF @vcListItemType = 'WI'
				BEGIN
					SET @strColumns = @strColumns + ', Case when isnull(OL.numSiteID,0)>0 then Sites.vcSiteName else WebAPI.vcProviderName end [' + @vcColumnName + ']'
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + '(Case when isnull(OL.numSiteID,0)>0 then Sites.vcSiteName else WebAPI.vcProviderName end) LIKE ''%' + @SearchText + '%'''
					END

					set @WhereCondition= @WhereCondition +' left JOIN OpportunityLinking OL on OL.numChildOppID=Opp.numOppId left join WebAPI on WebAPI.WebApiId=isnull(OL.numWebApiId,0)
													left join Sites on isnull(OL.numSiteID,0)=Sites.numSIteID and Sites.numDomainID=' + convert(varchar(15),@numDomainID)                                                       
				END 
            END
			ELSE IF @vcAssociatedControlType = 'DateField'
			BEGIN
				IF @Prefix ='OPR.'
				BEGIN
					SET @strColumns = @strColumns + ', (SELECT TOP 1 dbo.FormatedDateFromDate( DateAdd(minute,+'+convert(varchar(15),-@ClientTimeZoneOffset)+', dtRecurringDate)'+ ','+ CONVERT(VARCHAR(10),@numDomainId) +') FROM  dbo.OpportunityRecurring WHERE numOppID=OPP.numOppID AND ISNULL(numOppBizDocID,0)=0 and dtRecurringDate > dbo.GetUTCDateWithoutTime()) AS  ['+ @vcColumnName + ']'

					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + '(SELECT TOP 1 dbo.FormatedDateFromDate( DateAdd(minute,+'+convert(varchar(15),-@ClientTimeZoneOffset)+', dtRecurringDate)'+ ','+ CONVERT(VARCHAR(10),@numDomainId) +') FROM  dbo.OpportunityRecurring WHERE numOppID=OPP.numOppID AND ISNULL(numOppBizDocID,0)=0 and dtRecurringDate > dbo.GetUTCDateWithoutTime()) LIKE ''%' + @SearchText + '%'''
					END
				END
				ELSE IF @vcDbColumnName = 'dtReleaseDate'
				BEGIN
					SET @strColumns = @strColumns
                                + ',case when convert(varchar(11),'
                                + @Prefix
                                + @vcDbColumnName
                                + ')= convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',getdate())) then ''<b><font color=red>Today</font></b>'''
					SET @strColumns = @strColumns
									+ 'when convert(varchar(11),'
									+ @Prefix
									+ @vcDbColumnName
									+ ')= convert(varchar(11),dateadd(day,-1,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',getdate()))) then ''<b><font color=purple>Yesterday</font></b>'''
					SET @strColumns = @strColumns
									+ 'when convert(varchar(11),'
									+ @Prefix
									+ @vcDbColumnName
									+ ')= convert(varchar(11),dateadd(day,1,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',getdate()))) then''<b><font color=orange>Tommorow</font></b>'' '
					SET @strColumns = @strColumns
									+ 'else dbo.FormatedDateFromDate('
									+ @Prefix
									+ @vcDbColumnName
									+ ','
									+ CONVERT(VARCHAR(10),@numDomainId)
									+ ') end  ['
									+ @vcColumnName + ']' 	

					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + @Prefix + @vcDbColumnName + ' LIKE ''%' + @SearchText + '%'''
					END

				END
				ELSE
				BEGIN
					SET @strColumns = @strColumns
                                + ',case when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+','
                                + @Prefix
                                + @vcDbColumnName
                                + ') )= convert(varchar(11),getdate()) then ''<b><font color=red>Today</font></b>'''
					SET @strColumns = @strColumns
									+ 'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+','
									+ @Prefix
									+ @vcDbColumnName
									+ ') )= convert(varchar(11),dateadd(day,-1,getdate())) then ''<b><font color=purple>Yesterday</font></b>'''
					SET @strColumns = @strColumns
									+ 'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+','
									+ @Prefix
									+ @vcDbColumnName
									+ ') )= convert(varchar(11),dateadd(day,1,getdate())) then''<b><font color=orange>Tommorow</font></b>'' '
					SET @strColumns = @strColumns
									+ 'else dbo.FormatedDateFromDate('
									+ @Prefix
									+ @vcDbColumnName
									+ ','
									+ CONVERT(VARCHAR(10),@numDomainId)
									+ ') end  ['
									+ @vcColumnName + ']' 	

					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + @Prefix + @vcDbColumnName + ' LIKE ''%' + @SearchText + '%'''
					END
				 END	
            END
            ELSE
            IF @vcAssociatedControlType = 'TextBox'
            BEGIN
				
                SET @strColumns = @strColumns
                                  + ','
                                  + CASE 
                                      WHEN @vcDbColumnName = 'numAge' THEN 'year(getutcdate())-year(bintDOB)'
                                      WHEN @vcDbColumnName = 'monPAmount' THEN 'isnull((SELECT varCurrSymbol FROM [Currency] WHERE numCurrencyID = Opp.[numCurrencyID]),'''') + '' ''+ CONVERT(VARCHAR,CONVERT(DECIMAL(20,5),monPAmount),1)'
                                      WHEN @vcDbColumnName = 'numDivisionID' THEN 'Div.numDivisionID'
                                      WHEN @vcDbColumnName = 'tintOppStatus' THEN 'CASE tintOppStatus WHEN 0 then ''Open'' WHEN 1 THEN ''Closed-Won'' WHEN 2 THEN ''Closed-Lost'' END '
                                      WHEN @vcDbColumnName = 'tintOppType' THEN 'CASE tintOppType WHEN 1 then ''Sales'' WHEN 2 THEN ''Purchase'' Else ''NA'' END '
									  WHEN @vcDbColumnName = 'vcPOppName' THEN 'dbo.CheckChildRecord(Opp.numOppId,Opp.numDomainID)'
									  WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'vcBizDocsList' THEN 'ISNULL((SELECT SUBSTRING((SELECT ''$^$'' + CAST(numOppBizDocsId AS VARCHAR(18)) +''#^#''+ vcBizDocID
										FROM OpportunityBizDocs WHERE numOppId=Opp.numOppId AND 1 = (Case When bitAuthoritativeBizDocs=1 AND 1=' + (CASE WHEN @inttype=1 THEN '1' ELSE '0' END) + '  Then 0 ELSE 1 END) FOR XML PATH('''')),4,200000)),'''')'
                                      ELSE @Prefix + @vcDbColumnName
                                    END
                                  + ' ['
                                  + @vcColumnName + ']'
				IF(@vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'vcBizDocsList')
				BEGIN
					SET @strColumns=@strColumns+','+' (SELECT COUNT(I.vcItemName) FROM 
													 OpportunityItems AS t LEFT JOIN  Item as I ON 
													 t.numItemCode=I.numItemCode WHERE 
													 t.numOppId=Opp.numOppId AND t.numoppItemtCode NOT IN 
													 (SELECT numOppItemId FROM OpportunityBizDocs JOIN 
													 OpportunityBizDocItems ON OpportunityBizDocs.numOppBizDocsId=OpportunityBizDocItems.numOppBizDocID  
													 WHERE OpportunityBizDocs.numOppId=Opp.numOppId 
													 AND 1 = (Case When bitAuthoritativeBizDocs=1 Or numBizDocID=293 Then 0 ELSE 1 END) 
													 AND ISNULL(bitAuthoritativeBizDocs,0)=1)) AS [List_Item_Approval_UNIT] '
				END
				
				IF LEN(@SearchText) > 0
				BEGIN
					SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + (CASE 
                                      WHEN @vcDbColumnName = 'numAge' THEN 'year(getutcdate())-year(bintDOB)'
                                      WHEN @vcDbColumnName = 'monPAmount' THEN 'isnull((SELECT varCurrSymbol FROM [Currency] WHERE numCurrencyID = Opp.[numCurrencyID]),'''') + '' ''+ CONVERT(VARCHAR,CONVERT(DECIMAL(20,5),monPAmount),1)'
                                      WHEN @vcDbColumnName = 'numDivisionID' THEN 'Div.numDivisionID'
                                      WHEN @vcDbColumnName = 'tintOppStatus' THEN 'CASE tintOppStatus WHEN 0 then ''Open'' WHEN 1 THEN ''Closed-Won'' WHEN 2 THEN ''Closed-Lost'' END '
                                      WHEN @vcDbColumnName = 'tintOppType' THEN 'CASE tintOppType WHEN 1 then ''Sales'' WHEN 2 THEN ''Purchase'' Else ''NA'' END '
									  WHEN @vcDbColumnName = 'vcPOppName' THEN 'dbo.CheckChildRecord(Opp.numOppId,Opp.numDomainID)'
									  WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'vcBizDocsList' THEN 'ISNULL((SELECT SUBSTRING((SELECT ''$^$'' + CAST(numOppBizDocsId AS VARCHAR(18)) +''#^#''+ vcBizDocID
										FROM OpportunityBizDocs WHERE numOppId=Opp.numOppId AND 1 = (Case When bitAuthoritativeBizDocs=1 Or numBizDocID=293 Then 0 ELSE 1 END)  FOR XML PATH('''')),4,200000)),'''')'
                                      ELSE @Prefix + @vcDbColumnName
                                    END) + ' LIKE ''%' + @SearchText + '%'''
				END
            END
            ELSE
            IF @vcAssociatedControlType = 'TextArea'
            BEGIN
				SET @strColumns=@strColumns+','+  @Prefix + @vcDbColumnName +' ['+ @vcColumnName+']' 

				IF LEN(@SearchText) > 0
				BEGIN
					SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + @Prefix + @vcDbColumnName + ' LIKE ''%' + @SearchText + '%'''
				END
            END
            ELSE IF  @vcAssociatedControlType='Label'                                              
			BEGIN
				SET @strColumns=@strColumns + ',' + CASE                    
				WHEN @vcDbColumnName = 'CalAmount' THEN 'Opp.monDealAmount'--'[dbo].[getdealamount](Opp.numOppId,Getutcdate(),0)'
				WHEN @vcDbColumnName = 'vcInventoryStatus' THEN 'ISNULL(dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID),'''')'
				WHEN @vcDbColumnName = 'vcOrderedShipped' THEN 'dbo.GetOrderShipReceiveStatus(Opp.numOppId,1,Opp.tintshipped)'
				WHEN @vcDbColumnName = 'vcOrderedReceived' THEN 'dbo.GetOrderShipReceiveStatus(Opp.numOppId,2,Opp.tintshipped)'
				WHEN @vcDbColumnName = 'vcShipStreet' THEN 'dbo.fn_getOPPState(Opp.numOppId,Opp.numDomainID,2)'
				WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'monDealAmount' THEN 'ISNULL((SELECT SUM(ISNULL(monDealAmount,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)'
				WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'monAmountPaid' THEN 'ISNULL((SELECT SUM(ISNULL(monAmountPaid,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)'
				ELSE @Prefix + @vcDbColumnName END +' ['+ @vcColumnName+']'   
				
				IF @vcDbColumnName='vcOrderedShipped'
				BEGIN
				DECLARE @temp VARCHAR(MAX)
				SET @temp = '(SELECT 
				STUFF((SELECT '', '' + CAST((CAST(numOppBizDocsId AS varchar)+''~''+CAST(ISNULL(numShippingReportId,0) AS varchar) ) AS VARCHAR(MAX)) [text()]
				FROM OpportunityBizDocs
				LEFT JOIN ShippingReport
				ON ShippingReport.numOppBizDocId=OpportunityBizDocs.numOppBizDocsId
				WHERE OpportunityBizDocs.numOppId=Opp.numOppId  AND OpportunityBizDocs.bitShippingGenerated=1  FOR XML PATH(''''), TYPE)
						.value(''.'',''NVARCHAR(MAX)''),1,2,'' ''))'
					SET @strColumns = @strColumns + ' , (SELECT SUBSTRING('+ @temp +', 2, 200000))AS ShippingIcons '
					SET @strColumns = @strColumns + ' , CASE WHEN (SELECT COUNT(*) FROM ShippingBox INNER JOIN ShippingReport ON ShippingBox.numShippingReportId = ShippingReport.numShippingReportId WHERE ShippingReport.numOppID = Opp.numOppID AND LEN(vcTrackingNumber) > 0) > 0 THEN 1 ELSE 0 END [ISTrackingNumGenerated]'
				END

				IF LEN(@SearchText) > 0
				BEGIN
					SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + (CASE                    
					WHEN @vcDbColumnName = 'CalAmount' THEN 'Opp.monDealAmount'
					WHEN @vcDbColumnName = 'vcInventoryStatus' THEN 'ISNULL(dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID),'''')'
					WHEN @vcDbColumnName = 'vcOrderedShipped' THEN 'dbo.GetOrderShipReceiveStatus(Opp.numOppId,1,Opp.tintshipped)'
					WHEN @vcDbColumnName = 'vcOrderedReceived' THEN 'dbo.GetOrderShipReceiveStatus(Opp.numOppId,2,Opp.tintshipped)'
					WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'monDealAmount' THEN 'ISNULL((SELECT SUM(ISNULL(monDealAmount,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)'
					WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'monAmountPaid' THEN 'ISNULL((SELECT SUM(ISNULL(monAmountPaid,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)'
					else @Prefix + @vcDbColumnName END) + ' LIKE ''%' + @SearchText + '%'''
				END    
			END
            ELSE IF @vcAssociatedControlType='Popup' and @vcDbColumnName='numShareWith'                                                   
			BEGIN
				SET @strColumns = @strColumns + ',(SELECT SUBSTRING(
								(SELECT '','' + A.vcFirstName+'' ''+A.vcLastName + CASE WHEN ISNULL(SR.numContactType,0)>0 THEN ''('' + dbo.fn_GetListItemName(SR.numContactType) + '')'' ELSE '''' END
								FROM ShareRecord SR JOIN UserMaster UM ON UM.numUserDetailId=SR.numAssignedTo 
								JOIN AdditionalContactsInformation A ON UM.numUserDetailId=A.numContactID 
								WHERE SR.numDomainID=Opp.numDomainID AND SR.numModuleID=3 AND SR.numRecordID=Opp.numOppId
								AND UM.numDomainID=Opp.numDomainID and UM.numDomainID=A.numDomainID FOR XML PATH('''')),2,200000)) ['+ @vcColumnName+']'                
			END 
        END
		ELSE
        BEGIN
			SET @vcColumnName= @vcDbColumnName +'~'+ convert(varchar(10),@numFieldId)+'~1'

			IF @vcAssociatedControlType = 'TextBox' or @vcAssociatedControlType = 'TextArea'
            BEGIN
              SET @strColumns = @strColumns
                              + ',CFW'
                              + CONVERT(VARCHAR(3),@tintOrder)
                              + '.Fld_Value  ['
                              + @vcColumnName + ']'
              SET @WhereCondition = @WhereCondition
                                      + ' left Join CFW_FLD_Values_Opp CFW'
                                      + CONVERT(VARCHAR(3),@tintOrder)
                                      + ' on CFW'
                                      + CONVERT(VARCHAR(3),@tintOrder)
                                      + '.Fld_Id='
                                      + CONVERT(VARCHAR(10),@numFieldId)
                                      + 'and CFW'
                                      + CONVERT(VARCHAR(3),@tintOrder)
                                      + '.RecId=Opp.numOppid   '
			END
			ELSE
            IF @vcAssociatedControlType = 'CheckBox' 
            BEGIN
                SET @strColumns = @strColumns
                                + ',case when isnull(CFW'
                                + CONVERT(VARCHAR(3),@tintOrder)
                                + '.Fld_Value,0)=0 then 0 when isnull(CFW'
                                + CONVERT(VARCHAR(3),@tintOrder)
                                + '.Fld_Value,0)=1 then 1 end   ['
                                + @vcColumnName + ']'
                SET @WhereCondition = @WhereCondition
                                        + ' left Join CFW_FLD_Values_Opp CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + ' on CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.Fld_Id='
                                        + CONVERT(VARCHAR(10),@numFieldId)
                                        + 'and CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.RecId=Opp.numOppid   '
            END
            ELSE
            IF @vcAssociatedControlType = 'DateField'
            BEGIN
                  SET @strColumns = @strColumns
                                  + ',dbo.FormatedDateFromDate(CFW'
                                  + CONVERT(VARCHAR(3),@tintOrder)
                                  + '.Fld_Value,'
                                  + CONVERT(VARCHAR(10),@numDomainId)
                                  + ')  ['
                                  + @vcColumnName + ']'
                  SET @WhereCondition = @WhereCondition
                                          + ' left Join CFW_FLD_Values_Opp CFW'
                                          + CONVERT(VARCHAR(3),@tintOrder)
                                          + ' on CFW'
                                          + CONVERT(VARCHAR(3),@tintOrder)
                                          + '.Fld_Id='
                                          + CONVERT(VARCHAR(10),@numFieldId)
                                          + 'and CFW'
                                          + CONVERT(VARCHAR(3),@tintOrder)
                                          + '.RecId=Opp.numOppid    '
            END
            ELSE
            IF @vcAssociatedControlType = 'SelectBox'
            BEGIN
                SET @vcDbColumnName = 'DCust'
                                            + CONVERT(VARCHAR(10),@numFieldId)
                SET @strColumns = @strColumns
                                + ',L'
                                + CONVERT(VARCHAR(3),@tintOrder)
                                + '.vcData'
                                + ' ['
                                + @vcColumnName + ']'

                SET @WhereCondition = @WhereCondition
                                        + ' left Join CFW_FLD_Values_Opp CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + ' on CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.Fld_Id='
                                        + CONVERT(VARCHAR(10),@numFieldId)
                                        + 'and CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.RecId=Opp.numOppid '

                SET @WhereCondition = @WhereCondition
                                        + ' left Join ListDetails L'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + ' on L'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.numListItemID=CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.Fld_Value'
            END
			ELSE
			BEGIN
				SET @strColumns = @strColumns + CONCAT(',dbo.GetCustFldValueOpp(',@numFieldId,',opp.numOppID)') + ' [' + @vcColumnName + ']'
			END

			IF LEN(@SearchText) > 0
			BEGIN
				SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + CONCAT('dbo.GetCustFldValueOpp(',@numFieldId,',opp.numOppID) LIKE ''%',@SearchText,'%''')
			END
		END
      
     
		SELECT TOP 1 
			@tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName,@vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,
			@numListID=numListID,@vcLookBackTableName=vcLookBackTableName,@bitCustom=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,@bitAllowEdit=bitAllowEdit,
			@ListRelID=ListRelID
		FROM 
			#tempForm 
		WHERE 
			tintOrder > @tintOrder-1 
		ORDER BY 
			tintOrder asc            
 
		IF @@rowcount=0 SET @tintOrder=0 
	END 

	

	DECLARE @strShareRedordWith AS VARCHAR(300);
	SET @strShareRedordWith=' Opp.numOppId in (select SR.numRecordID FROM ShareRecord SR WHERE SR.numDomainID='+ convert(varchar(15),@numDomainID )+' AND SR.numModuleID=3 AND SR.numAssignedTo='+convert(varchar(15),@numUserCntId)+') '


	DECLARE @StrSql AS VARCHAR(MAX) = ''

	SET @StrSql = @StrSql + ' FROM OpportunityMaster Opp            
								 ##PLACEHOLDER##                                                   
                                 LEFT JOIN AdditionalContactsInformation ADC ON Opp.numContactId = ADC.numContactId                                                               
                                 INNER JOIN DivisionMaster Div ON Opp.numDivisionId = Div.numDivisionID 
								 INNER JOIN CompanyInfo cmp ON Div.numCompanyID = cmp.numCompanyId 
								 LEFT JOIN ProjectProgress PP On PP.numOppId = Opp.numOppId ' + @WhereCondition
								 --INNER JOIN Correspondence Corr ON opp.numOppId = Corr.numOpenRecordID 
	-------Change Row Color-------
	
	DECLARE @vcCSOrigDbCOlumnName AS VARCHAR(50) = ''
	DECLARE @vcCSLookBackTableName AS VARCHAR(50) = ''
	DECLARE @vcCSAssociatedControlType AS VARCHAR(50)

	CREATE TABLE #tempColorScheme
	(
		vcOrigDbCOlumnName VARCHAR(50),
		vcLookBackTableName VARCHAR(50),
		vcFieldValue VARCHAR(50),
		vcFieldValue1 VARCHAR(50),
		vcColorScheme VARCHAR(50),
		vcAssociatedControlType varchar(50)
	)

	INSERT INTO 
		#tempColorScheme 
	SELECT 
		DFM.vcOrigDbCOlumnName,
		DFM.vcLookBackTableName,
		DFCS.vcFieldValue,
		DFCS.vcFieldValue1,
		DFCS.vcColorScheme,
		DFFM.vcAssociatedControlType 
	FROM 
		DycFieldColorScheme DFCS 
	JOIN 
		DycFormField_Mapping DFFM 
	ON 
		DFCS.numFieldID=DFFM.numFieldID
	JOIN 
		DycFieldMaster DFM 
	ON 
		DFM.numModuleID=DFFM.numModuleID and DFM.numFieldID=DFFM.numFieldID
	WHERE
		DFCS.numDomainID=@numDomainID 
		AND DFFM.numFormID=@numFormId 
		AND DFCS.numFormID=@numFormId 
		AND isnull(DFFM.bitAllowGridColor,0)=1

	IF(SELECT COUNT(*) FROM #tempColorScheme)>0
	BEGIN
		SELECT TOP 1 @vcCSOrigDbCOlumnName=vcOrigDbCOlumnName,@vcCSLookBackTableName=vcLookBackTableName,@vcCSAssociatedControlType=vcAssociatedControlType from #tempColorScheme
	END                        

	IF len(@vcCSOrigDbCOlumnName)>0 and len(@vcCSLookBackTableName)>0
	BEGIN
		SET @strColumns=@strColumns + ',tCS.vcColorScheme'
	END

	IF len(@vcCSOrigDbCOlumnName)>0 and len(@vcCSLookBackTableName)>0
	BEGIN
		IF @vcCSLookBackTableName = 'AdditionalContactsInformation'                  
			SET @Prefix = 'ADC.'                  
		IF @vcCSLookBackTableName = 'DivisionMaster'                  
			SET @Prefix = 'Div.'
		if @vcCSLookBackTableName = 'CompanyInfo'                  
			SET @Prefix = 'CMP.'   
		if @vcCSLookBackTableName = 'OpportunityMaster'                  
			SET @Prefix = 'Opp.'   
 
		IF @vcCSAssociatedControlType='DateField'
		BEGIN
			SET @strSql=@strSql+' LEFT JOIN #tempColorScheme tCS ON CONVERT(VARCHAR(10),' + @Prefix + @vcCSOrigDbCOlumnName + ',111) >= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue as int),GETUTCDATE()),111)
				 and CONVERT(VARCHAR(10),' + @Prefix + @vcCSOrigDbCOlumnName + ',111) <= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue1 as int),GETUTCDATE()),111)'
		END
		ELSE IF @vcCSAssociatedControlType='SelectBox' OR @vcCSAssociatedControlType='ListBox' OR @vcCSAssociatedControlType='CheckBox'
		BEGIN
			SET @strSql=@strSql+' LEFT JOIN #tempColorScheme tCS on tCS.vcFieldValue=' + @Prefix + @vcCSOrigDbCOlumnName
		END
	END
                          
	IF @columnName like 'CFW.Cust%'             
	BEGIN            
		SET @strSql = @strSql + ' LEFT JOIN CFW_Fld_Values_Opp CFW on CFW.RecId=Opp.numOppid and CFW.fld_id= '+ REPLACE(@columnName,'CFW.Cust','') +' '                                                     
		SET @columnName='CFW.Fld_Value'            
	END 

	IF @bitPartner = 1
		SET @strSql = @strSql + ' LEFT JOIN OpportunityContact OppCont ON OppCont.numOppId=Opp.numOppId AND OppCont.bitPartner=1 AND OppCont.numContactId=' + CONVERT(VARCHAR(15),@numUserCntID)
                    
                
	IF @tintFilterBy = 1 --Partially Fulfilled Orders 
		SET @strSql = @strSql + ' LEFT JOIN AdditionalContactsInformation ADC1 ON ADC1.numContactId=Opp.numRecOwner                                                               
                             LEFT OUTER JOIN [OpportunityBizDocs] OB ON OB.[numOppId] = Opp.[numOppId] 
                             WHERE Opp.numDomainID=' + CONVERT(VARCHAR(15),@numDomainID) + ' and Opp.tintOppstatus=1 and (Opp.tintShipped=' + CONVERT(VARCHAR(15),@tintShipped) + ' OR ' + CONVERT(VARCHAR(15),@tintShipped) + '=2)'
	ELSE
		SET @strSql = @strSql + ' LEFT JOIN AdditionalContactsInformation ADC1 on ADC1.numContactId=Opp.numRecOwner                                                               
                             WHERE Opp.numDomainID=' + CONVERT(VARCHAR(15),@numDomainID) + ' and Opp.tintOppstatus=1 and (Opp.tintShipped='+ CONVERT(VARCHAR(15),@tintShipped)  + ' OR ' + CONVERT(VARCHAR(15),@tintShipped) + '=2)'
  
  
	IF @intOrder <> 0
    BEGIN
      IF @intOrder = 1
        SET @strSql = @strSql + ' and Opp.bitOrder = 0 '
      IF @intOrder = 2
        SET @strSql = @strSql + ' and Opp.bitOrder = 1'
    END
    
	IF @numCompanyID <> 0
		SET @strSql = @strSql + ' And Div.numCompanyID ='+ CONVERT(VARCHAR(15),@numCompanyID)

	IF @SortChar <> '0' 
		SET @strSql = @strSql + ' And Opp.vcPOppName like ''' + @SortChar + '%'''
  

	IF @inttype = 3
	BEGIN
		IF @tintSalesUserRightType = 0
			SET @strSql = @strSql + ' AND opp.tintOppType=0'
		ELSE IF @tintSalesUserRightType = 1
			SET @strSql = @strSql + ' AND (Opp.numRecOwner= ' + CONVERT(VARCHAR(15),@numUserCntID)
						+ ' or Opp.numAssignedTo= '+ CONVERT(VARCHAR(15),@numUserCntID)
						+ CASE WHEN @bitPartner = 1 THEN ' or ( OppCont.bitPartner=1 and OppCont.numContactId='+ CONVERT(VARCHAR(15),@numUserCntID) + ')' ELSE '' END
						+ ' or ' + @strShareRedordWith +')'
		ELSE IF @tintSalesUserRightType = 2
			SET @strSql = @strSql + ' AND (Div.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+ CONVERT(VARCHAR(15),@numUserCntID) + ' ) 
									 or Opp.numAssignedTo= '+ CONVERT(VARCHAR(15),@numUserCntID)
									+ ' or ' + @strShareRedordWith +')'
	END
	ELSE IF @inttype = 4
	BEGIN
		IF @tintPurchaseUserRightType = 0
			SET @strSql = @strSql + ' AND opp.tintOppType=0'
		ELSE IF @tintPurchaseUserRightType = 1
			SET @strSql = @strSql + ' AND (Opp.numRecOwner= ' + CONVERT(VARCHAR(15),@numUserCntID)
						+ ' or Opp.numAssignedTo= '+ CONVERT(VARCHAR(15),@numUserCntID)
						+ CASE WHEN @bitPartner = 1 THEN ' or ( OppCont.bitPartner=1 and OppCont.numContactId='+ CONVERT(VARCHAR(15),@numUserCntID) + ')' ELSE '' END
						+ ' or ' + @strShareRedordWith +')'
		ELSE IF @tintPurchaseUserRightType = 2
			SET @strSql = @strSql + ' AND (Div.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+ CONVERT(VARCHAR(15),@numUserCntID) + ' ) 
									 or Opp.numAssignedTo= '+ CONVERT(VARCHAR(15),@numUserCntID)
									+ ' or ' + @strShareRedordWith +')'
	END

	
					
	IF @tintSortOrder <> '0'
		SET @strSql = @strSql + '  AND Opp.tintOppType= '+ CONVERT(VARCHAR(1),@tintSortOrder)
    
    
	IF @tintFilterBy = 1 --Partially Fulfilled Orders
		SET @strSql = @strSql + ' AND OB.bitPartialFulfilment = 1 AND OB.bitAuthoritativeBizDocs = 1 '
	ELSE IF @tintFilterBy = 2 --Fulfilled Orders
		SET @strSql = @strSql + ' AND Opp.[numOppId] IN (SELECT OM.numOppId FROM [OpportunityMaster] OM
                      			INNER JOIN [OpportunityBizDocs] OB ON OB.[numOppId] = OM.[numOppId]
                      			INNER JOIN [OpportunityItems] OI ON OM.[numOppId] = OI.[numOppId]
                      			INNER JOIN [OpportunityBizDocItems] BI ON BI.[numOppBizDocID] = OB.[numOppBizDocsId]
                      			Where OM.numDomainId=' + CONVERT(VARCHAR(15),@numDomainId) + 'AND OB.[bitAuthoritativeBizDocs] = 1 GROUP BY OM.[numOppId]
                      			HAVING   COUNT(OI.[numUnitHour]) = COUNT(BI.[numUnitHour])) '
	ELSE IF @tintFilterBy = 3 --Orders without Authoritative-BizDocs
		SET @strSql = @strSql + ' AND Opp.[numOppId] not IN (SELECT DISTINCT OM.[numOppId]
                      		  FROM [OpportunityMaster] OM JOIN [OpportunityBizDocs] OB ON OM.[numOppId] = OB.[numOppId]
                      		  Where OM.numDomainId=' + CONVERT(VARCHAR(15),@numDomainId) + ' AND OM.tintOppstatus=1 AND (OM.tintShipped=' + CONVERT(VARCHAR(15),@tintShipped) + ' OR ' + CONVERT(VARCHAR(15),@tintShipped) + '=2)' + ' AND OM.tintOppType= '+ CONVERT(VARCHAR(1),@tintSortOrder) + ' AND isnull(OB.[bitAuthoritativeBizDocs],0)=1) '
	ELSE IF @tintFilterBy = 4 --Orders with items yet to be added to Invoices
		SET @strSql = @strSql + ' AND Opp.[numOppId] IN (SELECT distinct OPPM.numOppId FROM [OpportunityMaster] OPPM
                                  INNER JOIN [OpportunityItems] OPPI ON OPPM.[numOppId] = OPPI.[numOppId]
                                  Where OPPM.numDomainId=' + CONVERT(VARCHAR(15),@numDomainId) + ' 
								  and OPPI.numoppitemtCode not in (select numOppItemID from [OpportunityBizDocs] OB INNER JOIN [OpportunityBizDocItems] BI
                      			  ON BI.[numOppBizDocID] = OB.[numOppBizDocsId] where OB.[numOppId] = OPPM.[numOppId]))'      
	ELSE IF @tintFilterBy = 6 --Orders with deferred Income/Invoices
		SET @strSql = @strSql + ' AND Opp.[numOppId] IN (SELECT DISTINCT OM.[numOppId]
                      		 FROM [OpportunityMaster] OM LEFT OUTER JOIN [OpportunityBizDocs] OB ON OM.[numOppId] = OB.[numOppId]
                      		 Where OM.numDomainId=' + CONVERT(VARCHAR(15),@numDomainId) + ' AND OB.[bitAuthoritativeBizDocs] =1 and OB.tintDeferred=1)'

	IF @numOrderStatus <>0 
		SET @strSql = @strSql + ' AND Opp.numStatus = ' + CONVERT(VARCHAR(15),@numOrderStatus);

	IF CHARINDEX('OI.vcInventoryStatus',@vcRegularSearchCriteria) > 0
	BEGIN
		IF CHARINDEX('OI.vcInventoryStatus=1',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'OI.vcInventoryStatus=1',' CHARINDEX(''Not Applicable'', dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID)) > 0 ')
		IF CHARINDEX('OI.vcInventoryStatus=2',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'OI.vcInventoryStatus=2',' CHARINDEX(''Allocation Cleared'', dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID)) > 0 ')
		IF CHARINDEX('OI.vcInventoryStatus=3',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'OI.vcInventoryStatus=3',' CHARINDEX(''Back Order'', dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID)) > 0 ')
		IF CHARINDEX('OI.vcInventoryStatus=4',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'OI.vcInventoryStatus=4',' CHARINDEX(''Ready to Ship'', dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID)) > 0 ')
	END
	
	IF CHARINDEX('Opp.tintInvoicing',@vcRegularSearchCriteria) > 0
	BEGIN
		IF CHARINDEX('Opp.tintInvoicing=0',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.tintInvoicing=0',' CHARINDEX(''All'', dbo.CheckOrderInvoicingStatus(Opp.numOppID,Opp.numDomainID)) > 0 ')
		IF CHARINDEX('Opp.tintInvoicing=1',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.tintInvoicing=1',' (SELECT (CASE WHEN SUM(ISNULL(InvoicedQty,0)) = SUM(ISNULL(OrderedQty,0)) THEN 1 ELSE 0 END)
	FROM 
	(
		SELECT OI.numoppitemtCode, ISNULL(OI.numUnitHour,0) AS OrderedQty, ISNULL(TempInvoice.InvoicedQty,0) AS InvoicedQty
		FROM OpportunityItems OI
		INNER JOIN Item I ON OI.numItemCode = I.numItemCode
		OUTER APPLY
		(
			SELECT SUM(OpportunityBizDocItems.numUnitHour) AS InvoicedQty
			FROM OpportunityBizDocs
				INNER JOIN OpportunityBizDocItems ON OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
			WHERE OpportunityBizDocs.numOppId = Opp.numOppID
				AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
				AND numBizDocId IN (SELECT numAuthoritativeSales FROM AuthoritativeBizDocs WHERE numDomainId = Opp.numDomainID)
		) AS TempInvoice
		WHERE
		OI.numOppID = Opp.numOppID
	) X) > 0 ')
		IF CHARINDEX('Opp.tintInvoicing=2',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.tintInvoicing=2',' (SELECT COUNT(*) 
		FROM OpportunityBizDocs 
		WHERE numOppId = Opp.numOppID 
			AND numBizDocId IN (SELECT numAuthoritativeSales FROM AuthoritativeBizDocs WHERE numDomainId = Opp.numDomainID)) = 0 ')
		IF CHARINDEX('Opp.tintInvoicing=3',@vcRegularSearchCriteria) > 0 
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.tintInvoicing=3',' (SELECT (CASE WHEN SUM(ISNULL(InvoicedQty,0)) > 0 THEN SUM(ISNULL(OrderedQty,0)) - SUM(ISNULL(InvoicedQty,0)) ELSE 0 END)
	FROM 
	(
		SELECT OI.numoppitemtCode, ISNULL(OI.numUnitHour,0) AS OrderedQty, ISNULL(TempInvoice.InvoicedQty,0) AS InvoicedQty
		FROM OpportunityItems OI
		INNER JOIN Item I ON OI.numItemCode = I.numItemCode
		OUTER APPLY
		(
			SELECT SUM(OpportunityBizDocItems.numUnitHour) AS InvoicedQty
			FROM OpportunityBizDocs
				INNER JOIN OpportunityBizDocItems ON OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
			WHERE OpportunityBizDocs.numOppId = Opp.numOppID
				AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
				AND numBizDocId IN (SELECT numAuthoritativeSales FROM AuthoritativeBizDocs WHERE numDomainId = Opp.numDomainID)
		) AS TempInvoice
		WHERE
		OI.numOppID = Opp.numOppID
	) X) > 0 ')
		
		IF CHARINDEX('Opp.tintInvoicing=4',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.tintInvoicing=4',' (SELECT (CASE WHEN SUM(ISNULL(InvoicedQty,0)) > 0 THEN SUM(ISNULL(OrderedQty,0)) - SUM(ISNULL(InvoicedQty,0)) ELSE 0 END)
	FROM 
	(
		SELECT OI.numoppitemtCode, ISNULL(OI.numUnitHour,0) AS OrderedQty, ISNULL(TempInvoice.InvoicedQty,0) AS InvoicedQty
		FROM OpportunityItems OI
		INNER JOIN Item I ON OI.numItemCode = I.numItemCode
		OUTER APPLY
		(
			SELECT SUM(OpportunityBizDocItems.numUnitHour) AS InvoicedQty
			FROM OpportunityBizDocs
				INNER JOIN OpportunityBizDocItems ON OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
			WHERE OpportunityBizDocs.numOppId = Opp.numOppID
				AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
				AND numBizDocId IN (SELECT numAuthoritativeSales FROM AuthoritativeBizDocs WHERE numDomainId = Opp.numDomainID)
		) AS TempInvoice
		WHERE
		OI.numOppID = Opp.numOppID
	) X) > 0 ')

		IF CHARINDEX('Opp.tintInvoicing=5',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.tintInvoicing=5',' 
				Opp.[numOppId] IN (SELECT DISTINCT OM.[numOppId]
                      		 FROM [OpportunityMaster] OM INNER JOIN [OpportunityBizDocs] OB ON OM.[numOppId] = OB.[numOppId]
                      		 Where OM.numDomainId = Opp.numDomainID AND OB.[bitAuthoritativeBizDocs] = 1 and OB.tintDeferred=1) ')
	END
	
	IF CHARINDEX('Opp.tintEDIStatus',@vcRegularSearchCriteria) > 0
	BEGIN
		--IF CHARINDEX('Opp.tintEDIStatus=1',@vcRegularSearchCriteria) > 0
		--	SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.tintEDIStatus=1',' 1 = (SELECT CASE WHEN Opp.tintEDIStatus = 1 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.tintEDIStatus=2',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.tintEDIStatus=2',' 1 = (SELECT CASE WHEN Opp.tintEDIStatus = 2 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.tintEDIStatus=3',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.tintEDIStatus=3',' 1 = (SELECT CASE WHEN Opp.tintEDIStatus = 3 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.tintEDIStatus=4',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.tintEDIStatus=4',' 1 = (SELECT CASE WHEN Opp.tintEDIStatus = 4 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.tintEDIStatus=5',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.tintEDIStatus=5',' 1 = (SELECT CASE WHEN Opp.tintEDIStatus = 5 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.tintEDIStatus=6',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.tintEDIStatus=6',' 1 = (SELECT CASE WHEN Opp.tintEDIStatus = 6 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.tintEDIStatus=7',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.tintEDIStatus=7',' 1 = (SELECT CASE WHEN Opp.tintEDIStatus = 7 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.tintEDIStatus=7',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.tintEDIStatus=11',' 1 = (SELECT CASE WHEN Opp.tintEDIStatus = 11 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.tintEDIStatus=7',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.tintEDIStatus=12',' 1 = (SELECT CASE WHEN Opp.tintEDIStatus = 12 THEN 1 ELSE 0 END) ')
	END

	IF CHARINDEX('Opp.vcSignatureType',@vcRegularSearchCriteria) > 0
	BEGIN
		IF CHARINDEX('Opp.vcSignatureType=0',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.vcSignatureType=0',' 1 = (SELECT CASE WHEN (SELECT COUNT(*) FROM DivisionMaster AS D 
																			LEFT JOIN DivisionMasterShippingConfiguration AS DMSC ON DMSC.numDivisionID=D.numDivisionID 
																			WHERE D.numDivisionID=Div.numDivisionID AND vcSignatureType=0 ) > 0 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.vcSignatureType=1',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.vcSignatureType=1','  1 = (SELECT CASE WHEN (SELECT COUNT(*) FROM DivisionMaster AS D 
																			LEFT JOIN DivisionMasterShippingConfiguration AS DMSC ON DMSC.numDivisionID=D.numDivisionID 
																			WHERE D.numDivisionID=Div.numDivisionID AND vcSignatureType=1 ) > 0 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.vcSignatureType=2',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.vcSignatureType=2',' 1 = (SELECT CASE WHEN (SELECT COUNT(*) FROM DivisionMaster AS D 
																			LEFT JOIN DivisionMasterShippingConfiguration AS DMSC ON DMSC.numDivisionID=D.numDivisionID 
																			WHERE D.numDivisionID=Div.numDivisionID AND vcSignatureType=2 ) > 0 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.vcSignatureType=3',@vcRegularSearchCriteria) > 0								   
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.vcSignatureType=3',' 1 = (SELECT CASE WHEN (SELECT COUNT(*) FROM DivisionMaster AS D 
																			LEFT JOIN DivisionMasterShippingConfiguration AS DMSC ON DMSC.numDivisionID=D.numDivisionID 
																			WHERE D.numDivisionID=Div.numDivisionID AND vcSignatureType=3 ) > 0 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.vcSignatureType=4',@vcRegularSearchCriteria) > 0								   
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.vcSignatureType=4',' 1 = (SELECT CASE WHEN (SELECT COUNT(*) FROM DivisionMaster AS D 
																			LEFT JOIN DivisionMasterShippingConfiguration AS DMSC ON DMSC.numDivisionID=D.numDivisionID 
																			WHERE D.numDivisionID=Div.numDivisionID AND vcSignatureType=4 ) > 0 THEN 1 ELSE 0 END) ')
	END

	IF CHARINDEX('Opp.numDefaultShippingServiceID',@vcRegularSearchCriteria) > 0
	BEGIN
		IF CHARINDEX('Opp.numDefaultShippingServiceID=10',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.numDefaultShippingServiceID=10',' 1 = (SELECT CASE WHEN (SELECT COUNT(*) FROM DivisionMaster WHERE numDivisionID=Div.numDivisionID AND numDefaultShippingServiceID=10 ) > 0 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.numDefaultShippingServiceID=11',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.numDefaultShippingServiceID=11',' 1 = (SELECT CASE WHEN (SELECT COUNT(*) FROM DivisionMaster WHERE numDivisionID=Div.numDivisionID AND numDefaultShippingServiceID=11 ) > 0 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.numDefaultShippingServiceID=12',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.numDefaultShippingServiceID=12',' 1 = (SELECT CASE WHEN (SELECT COUNT(*) FROM DivisionMaster WHERE numDivisionID=Div.numDivisionID AND numDefaultShippingServiceID=12 ) > 0 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.numDefaultShippingServiceID=13',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.numDefaultShippingServiceID=13',' 1 = (SELECT CASE WHEN (SELECT COUNT(*) FROM DivisionMaster WHERE numDivisionID=Div.numDivisionID AND numDefaultShippingServiceID=13 ) > 0 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.numDefaultShippingServiceID=14',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.numDefaultShippingServiceID=14',' 1 = (SELECT CASE WHEN (SELECT COUNT(*) FROM DivisionMaster WHERE numDivisionID=Div.numDivisionID AND numDefaultShippingServiceID=14 ) > 0 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.numDefaultShippingServiceID=15',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.numDefaultShippingServiceID=15',' 1 = (SELECT CASE WHEN (SELECT COUNT(*) FROM DivisionMaster WHERE numDivisionID=Div.numDivisionID AND numDefaultShippingServiceID=15 ) > 0 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.numDefaultShippingServiceID=16',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.numDefaultShippingServiceID=16',' 1 = (SELECT CASE WHEN (SELECT COUNT(*) FROM DivisionMaster WHERE numDivisionID=Div.numDivisionID AND numDefaultShippingServiceID=16 ) > 0 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.numDefaultShippingServiceID=17',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.numDefaultShippingServiceID=17',' 1 = (SELECT CASE WHEN (SELECT COUNT(*) FROM DivisionMaster WHERE numDivisionID=Div.numDivisionID AND numDefaultShippingServiceID=17 ) > 0 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.numDefaultShippingServiceID=18',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.numDefaultShippingServiceID=18',' 1 = (SELECT CASE WHEN (SELECT COUNT(*) FROM DivisionMaster WHERE numDivisionID=Div.numDivisionID AND numDefaultShippingServiceID=18 ) > 0 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.numDefaultShippingServiceID=19',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.numDefaultShippingServiceID=19',' 1 = (SELECT CASE WHEN (SELECT COUNT(*) FROM DivisionMaster WHERE numDivisionID=Div.numDivisionID AND numDefaultShippingServiceID=19 ) > 0 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.numDefaultShippingServiceID=20',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.numDefaultShippingServiceID=20',' 1 = (SELECT CASE WHEN (SELECT COUNT(*) FROM DivisionMaster WHERE numDivisionID=Div.numDivisionID AND numDefaultShippingServiceID=20 ) > 0 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.numDefaultShippingServiceID=21',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.numDefaultShippingServiceID=21',' 1 = (SELECT CASE WHEN (SELECT COUNT(*) FROM DivisionMaster WHERE numDivisionID=Div.numDivisionID AND numDefaultShippingServiceID=21 ) > 0 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.numDefaultShippingServiceID=22',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.numDefaultShippingServiceID=22',' 1 = (SELECT CASE WHEN (SELECT COUNT(*) FROM DivisionMaster WHERE numDivisionID=Div.numDivisionID AND numDefaultShippingServiceID=22 ) > 0 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.numDefaultShippingServiceID=23',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.numDefaultShippingServiceID=23',' 1 = (SELECT CASE WHEN (SELECT COUNT(*) FROM DivisionMaster WHERE numDivisionID=Div.numDivisionID AND numDefaultShippingServiceID=23 ) > 0 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.numDefaultShippingServiceID=24',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.numDefaultShippingServiceID=24',' 1 = (SELECT CASE WHEN (SELECT COUNT(*) FROM DivisionMaster WHERE numDivisionID=Div.numDivisionID AND numDefaultShippingServiceID=24 ) > 0 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.numDefaultShippingServiceID=25',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.numDefaultShippingServiceID=25',' 1 = (SELECT CASE WHEN (SELECT COUNT(*) FROM DivisionMaster WHERE numDivisionID=Div.numDivisionID AND numDefaultShippingServiceID=25 ) > 0 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.numDefaultShippingServiceID=26',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.numDefaultShippingServiceID=26',' 1 = (SELECT CASE WHEN (SELECT COUNT(*) FROM DivisionMaster WHERE numDivisionID=Div.numDivisionID AND numDefaultShippingServiceID=26 ) > 0 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.numDefaultShippingServiceID=27',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.numDefaultShippingServiceID=27',' 1 = (SELECT CASE WHEN (SELECT COUNT(*) FROM DivisionMaster WHERE numDivisionID=Div.numDivisionID AND numDefaultShippingServiceID=27 ) > 0 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.numDefaultShippingServiceID=28',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.numDefaultShippingServiceID=28',' 1 = (SELECT CASE WHEN (SELECT COUNT(*) FROM DivisionMaster WHERE numDivisionID=Div.numDivisionID AND numDefaultShippingServiceID=28 ) > 0 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.numDefaultShippingServiceID=40',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.numDefaultShippingServiceID=40',' 1 = (SELECT CASE WHEN (SELECT COUNT(*) FROM DivisionMaster WHERE numDivisionID=Div.numDivisionID AND numDefaultShippingServiceID=40 ) > 0 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.numDefaultShippingServiceID=42',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.numDefaultShippingServiceID=42',' 1 = (SELECT CASE WHEN (SELECT COUNT(*) FROM DivisionMaster WHERE numDivisionID=Div.numDivisionID AND numDefaultShippingServiceID=42 ) > 0 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.numDefaultShippingServiceID=43',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.numDefaultShippingServiceID=43',' 1 = (SELECT CASE WHEN (SELECT COUNT(*) FROM DivisionMaster WHERE numDivisionID=Div.numDivisionID AND numDefaultShippingServiceID=43 ) > 0 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.numDefaultShippingServiceID=48',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.numDefaultShippingServiceID=48',' 1 = (SELECT CASE WHEN (SELECT COUNT(*) FROM DivisionMaster WHERE numDivisionID=Div.numDivisionID AND numDefaultShippingServiceID=48 ) > 0 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.numDefaultShippingServiceID=49',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.numDefaultShippingServiceID=49',' 1 = (SELECT CASE WHEN (SELECT COUNT(*) FROM DivisionMaster WHERE numDivisionID=Div.numDivisionID AND numDefaultShippingServiceID=49 ) > 0 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.numDefaultShippingServiceID=50',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.numDefaultShippingServiceID=50',' 1 = (SELECT CASE WHEN (SELECT COUNT(*) FROM DivisionMaster WHERE numDivisionID=Div.numDivisionID AND numDefaultShippingServiceID=50 ) > 0 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.numDefaultShippingServiceID=51',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.numDefaultShippingServiceID=51',' 1 = (SELECT CASE WHEN (SELECT COUNT(*) FROM DivisionMaster WHERE numDivisionID=Div.numDivisionID AND numDefaultShippingServiceID=51 ) > 0 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.numDefaultShippingServiceID=55',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.numDefaultShippingServiceID=55',' 1 = (SELECT CASE WHEN (SELECT COUNT(*) FROM DivisionMaster WHERE numDivisionID=Div.numDivisionID AND numDefaultShippingServiceID=55 ) > 0 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.numDefaultShippingServiceID=70',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.numDefaultShippingServiceID=70',' 1 = (SELECT CASE WHEN (SELECT COUNT(*) FROM DivisionMaster WHERE numDivisionID=Div.numDivisionID AND numDefaultShippingServiceID=70 ) > 0 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.numDefaultShippingServiceID=71',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.numDefaultShippingServiceID=71',' 1 = (SELECT CASE WHEN (SELECT COUNT(*) FROM DivisionMaster WHERE numDivisionID=Div.numDivisionID AND numDefaultShippingServiceID=71 ) > 0 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.numDefaultShippingServiceID=72',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.numDefaultShippingServiceID=72',' 1 = (SELECT CASE WHEN (SELECT COUNT(*) FROM DivisionMaster WHERE numDivisionID=Div.numDivisionID AND numDefaultShippingServiceID=72 ) > 0 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.numDefaultShippingServiceID=73',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.numDefaultShippingServiceID=73',' 1 = (SELECT CASE WHEN (SELECT COUNT(*) FROM DivisionMaster WHERE numDivisionID=Div.numDivisionID AND numDefaultShippingServiceID=73 ) > 0 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.numDefaultShippingServiceID=74',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.numDefaultShippingServiceID=74',' 1 = (SELECT CASE WHEN (SELECT COUNT(*) FROM DivisionMaster WHERE numDivisionID=Div.numDivisionID AND numDefaultShippingServiceID=74 ) > 0 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.numDefaultShippingServiceID=75',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.numDefaultShippingServiceID=75',' 1 = (SELECT CASE WHEN (SELECT COUNT(*) FROM DivisionMaster WHERE numDivisionID=Div.numDivisionID AND numDefaultShippingServiceID=75 ) > 0 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.numDefaultShippingServiceID=76',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.numDefaultShippingServiceID=76',' 1 = (SELECT CASE WHEN (SELECT COUNT(*) FROM DivisionMaster WHERE numDivisionID=Div.numDivisionID AND numDefaultShippingServiceID=76 ) > 0 THEN 1 ELSE 0 END) ')
	END

	IF CHARINDEX('OBD.monAmountPaid',@vcRegularSearchCriteria) > 0
	BEGIN
		IF CHARINDEX('OBD.monAmountPaid like ''%0%''',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'OBD.monAmountPaid like ''%0%''',' 1 = (SELECT ISNULL((SELECT SUM(ISNULL(monAmountPaid,0)) FROM  OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)) ')
		IF CHARINDEX('OBD.monAmountPaid like ''%1%''',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'OBD.monAmountPaid like ''%1%''','  1 = (SELECT (CASE WHEN (SELECT (CASE WHEN (SUM(ISNULL(monDealAmount,0)) = SUM(ISNULL(monAmountPaid,0)) AND SUM(ISNULL(monAmountPaid,0)) <> 0) THEN 1 ELSE 0 END) FROM OpportunityBizDocs BD WHERE BD.numOppId =  Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0) = 1) > 0 THEN 1 ELSE 0 END))  ')
		IF CHARINDEX('OBD.monAmountPaid like ''%2%''',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'OBD.monAmountPaid like ''%2%''',' 1 = (SELECT CASE WHEN (SELECT (CASE WHEN ((SUM(ISNULL(monDealAmount,0)) - SUM(ISNULL(monAmountPaid,0))) > 0 AND SUM(ISNULL(monAmountPaid,0)) <> 0) THEN 1 ELSE 0 END) FROM OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0) = 1) > 0 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('OBD.monAmountPaid like ''%3%''',@vcRegularSearchCriteria) > 0								   
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'OBD.monAmountPaid like ''%3%''',' 1 = (SELECT CASE WHEN (SELECT (CASE WHEN (SUM(ISNULL(monAmountPaid,0)) = 0) THEN 1 ELSE 0 END) FROM OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0) = 1) > 0 THEN 1 ELSE 0 END) ')
	END
	
	
	IF @vcRegularSearchCriteria<>'' 
	BEGIN
		SET @strSql=@strSql +' AND ' + @vcRegularSearchCriteria 
	END
	
	


	IF @vcCustomSearchCriteria<>'' 
	BEGIN
		SET @strSql = @strSql +' AND ' +  @vcCustomSearchCriteria
	END

	IF LEN(@SearchQuery) > 0
	BEGIN
		SET @strSql = @strSql + ' AND (' + @SearchQuery + ') '
	END

	IF ISNULL(@tintDashboardReminderType,0) = 19
	BEGIN
		SET @strSql = CONCAT(@strSql,' AND Opp.numOppID IN (','SELECT DISTINCT
																	OMInner.numOppID
																FROM 
																	StagePercentageDetails SPDInner
																INNER JOIN 
																	OpportunityMaster OMInner
																ON 
																	SPDInner.numOppID=OMInner.numOppId 
																WHERE 
																	SPDInner.numDomainId=',@numDOmainID,'
																	AND OMInner.numDomainId=',@numDOmainID,' 
																	AND OMInner.tintOppType = 1
																	AND ISNULL(OMInner.tintOppStatus,0) = 1
																	AND tinProgressPercentage <> 100',') ')

	END
	ELSE IF ISNULL(@tintDashboardReminderType,0) = 10
	BEGIN
		SET @strSql = CONCAT(@strSql,' AND Opp.numOppID IN (','SELECT DISTINCT
																	OMInner.numOppId
																FROM
																	OpportunityMaster OMInner
																INNER JOIN
																	OpportunityItems OIInner
																ON
																	OIInner.numOppId = OMInner.numOppID
																INNER JOIN
																	Item IInner
																ON
																	OIInner.numItemCode = IInner.numItemCode
																OUTER APPLY
																(
																	SELECT
																		SUM(OpportunityBizDocItems.numUnitHour) AS BilledQty
																	FROM
																		OpportunityBizDocs
																	INNER JOIN
																		OpportunityBizDocItems 
																	ON
																		OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																	WHERE
																		OpportunityBizDocs.numOppId = OMInner.numOppID
																		AND OpportunityBizDocItems.numOppItemID = OIInner.numoppitemtCode
																		AND OpportunityBizDocs.numBizDocId = (SELECT ISNULL(numAuthoritativePurchase,0) FROM AuthoritativeBizDocs WHERE numDomainId=',@numDomainID,')
																) AS TempBilled
																WHERE
																	OMInner.numDomainId = ',@numDomainID,'
																	AND ISNULL(OMInner.bitStockTransfer,0) = 0
																	AND OMInner.tintOppType = 2
																	AND OMInner.tintOppStatus  = 1
																	AND ISNULL(OIInner.numUnitHour,0) <> ISNULL(TempBilled.BilledQty,0)',') ')
	END
	ELSE IF ISNULL(@tintDashboardReminderType,0) = 11
	BEGIN
		SET @strSql = CONCAT(@strSql,' AND Opp.numOppID IN (','SELECT DISTINCT
																	OMInner.numOppId
																FROM
																	OpportunityMaster OMInner
																INNER JOIN
																	OpportunityItems OIInner
																ON
																	OIInner.numOppId = OMInner.numOppId
																INNER JOIN
																	Item IInner
																ON
																	OIInner.numItemCode = IInner.numItemCode
																OUTER APPLY
																(
																	SELECT
																		SUM(OpportunityBizDocItems.numUnitHour) AS PickPacked
																	FROM
																		OpportunityBizDocs
																	INNER JOIN
																		OpportunityBizDocItems 
																	ON
																		OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																	WHERE
																		OpportunityBizDocs.numOppId = OMInner.numOppId
																		AND ISNULL(OpportunityBizDocs.numBizDocId,0) = ',(SELECT ISNULL(numDefaultSalesShippingDoc,0) FROM Domain WHERE numDomainId=@numDomainID),'
																		AND OpportunityBizDocItems.numOppItemID = OIInner.numoppitemtCode
																) AS TempFulFilled
																WHERE
																	OMInner.numDomainId = ',@numDomainID,'
																	AND OMInner.tintOppType = 1
																	AND OMInner.tintOppStatus = 1
																	AND UPPER(IInner.charItemType) = ''P''
																	AND ISNULL(OMInner.tintshipped,0) = 0
																	AND ISNULL(OIInner.bitDropShip,0) = 0
																	AND ISNULL(OIInner.numUnitHour,0) <> ISNULL(TempFulFilled.PickPacked,0)',') ')
	END
	ELSE IF ISNULL(@tintDashboardReminderType,0) = 12
	BEGIN
		SET @strSql = CONCAT(@strSql,' AND Opp.numOppID IN (','SELECT DISTINCT
																	OMInner.numOppId
																FROM
																	OpportunityMaster OMInner
																INNER JOIN
																	OpportunityItems OIInner
																ON
																	OIInner.numOppId = OMInner.numOppId
																INNER JOIN
																	Item IInner
																ON
																	OIInner.numItemCode = IInner.numItemCode
																OUTER APPLY
																(
																	SELECT
																		SUM(OpportunityBizDocItems.numUnitHour) AS FulFilledQty
																	FROM
																		OpportunityBizDocs
																	INNER JOIN
																		OpportunityBizDocItems 
																	ON
																		OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																	WHERE
																		OpportunityBizDocs.numOppId = OMInner.numOppId
																		AND ISNULL(OpportunityBizDocs.numBizDocId,0) = 296
																		AND ISNULL(OpportunityBizDocs.bitFulFilled,0) = 1
																		AND OpportunityBizDocItems.numOppItemID = OIInner.numoppitemtCode
																) AS TempFulFilled
																WHERE
																	OMInner.numDomainId = ',@numDomainID,'
																	AND OMInner.tintOppType = 1
																	AND OMInner.tintOppStatus = 1
																	AND ISNULL(OMInner.tintshipped,0) = 0
																	AND UPPER(IInner.charItemType) = ''P''
																	AND ISNULL(OIInner.bitDropShip,0) = 0
																	AND ISNULL(OIInner.numUnitHour,0) <> ISNULL(TempFulFilled.FulFilledQty,0)',') ')
	END
	ELSE IF ISNULL(@tintDashboardReminderType,0) = 13
	BEGIN
		SET @strSql = CONCAT(@strSql,' AND Opp.numOppID IN (','SELECT DISTINCT
																	OMInner.numOppId
																FROM
																	OpportunityMaster OMInner
																INNER JOIN
																	OpportunityItems OIInner
																ON
																	OIInner.numOppId = OMInner.numOppID
																INNER JOIN
																	Item IInner
																ON
																	OIInner.numItemCode = IInner.numItemCode
																OUTER APPLY
																(
																	SELECT
																		SUM(OpportunityBizDocItems.numUnitHour) AS InvoicedQty
																	FROM
																		OpportunityBizDocs
																	INNER JOIN
																		OpportunityBizDocItems 
																	ON
																		OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																	WHERE
																		OpportunityBizDocs.numOppId = OMInner.numOppID
																		AND OpportunityBizDocItems.numOppItemID = OIInner.numoppitemtCode
																		AND OpportunityBizDocs.numBizDocId = ISNULL((SELECT TOP 1 numAuthoritativeSales FROM AuthoritativeBizDocs WHERE numDomainId=',@numDomainID,'),287)
																) AS TempInvoice
																WHERE
																	OMInner.numDomainId = ',@numDomainID,'
																	AND OMInner.tintOppType = 1
																	AND OMInner.tintOppStatus  = 1
																	AND ISNULL(OIInner.numUnitHour,0) <> ISNULL(TempInvoice.InvoicedQty,0)',') ')
	END
	ELSE IF ISNULL(@tintDashboardReminderType,0) = 14
	BEGIN
		DECLARE @TEMPOppID TABLE
		(
			numOppID NUMERIC(18,0)
		)

		INSERT INTO @TEMPOppID
		(
			numOppID
		)
		SELECT DISTINCT
			OM.numOppID
		FROM
			OpportunityMaster OM
		INNER JOIN
			OpportunityItems OI
		ON
			OM.numOppId = OI.numOppId
		WHERE   
			OM.numDomainId = @numDomainId
			AND OM.tintOppType = 1
			AND OM.tintOppStatus=1
			AND ISNULL(OI.bitDropShip, 0) = 1
			AND 1 = (CASE WHEN (SELECT COUNT(*) FROM OpportunityLinking OMInner INNER JOIN OpportunityItems OIInner ON OMInner.numChildOppID=OIInner.numOppId WHERE OMInner.numParentOppID = OM.numOppID AND OIInner.numItemCode=OI.numItemCode AND ISNULL(OIInner.numWarehouseItmsID,0)=ISNULL(OI.numWarehouseItmsID,0)) > 0 THEN 0 ELSE 1 END)


		If ISNULL((SELECT bitReOrderPoint FROM Domain WHERE numDomainId=@numDomainID),0) = 1
		BEGIN
			INSERT INTO @TEMPOppID
			(
				numOppID
			)
			SELECT DISTINCT
				OM.numOppID
			FROM
				OpportunityMaster OM
			INNER JOIN
				OpportunityItems OI
			ON
				OI.numOppId = OM.numOppId
			INNER JOIN 
				Item I 
			ON 
				I.numItemCode = OI.numItemCode
			INNER JOIN 
				WarehouseItems WI 
			ON 
				OI.numWarehouseItmsID = WI.numWareHouseItemID
			WHERE   
				OM.numDomainId = @numDomainId
				AND ISNULL((SELECT bitReOrderPoint FROM Domain WHERE numDomainId=@numDomainID),0) = 1
				AND (ISNULL(WI.numOnHand,0) + ISNULL(WI.numOnOrder,0) <= ISNULL(WI.numReOrder,0))
				AND OM.tintOppType = 1
				AND OM.tintOppStatus=1
				AND ISNULL(I.bitAssembly, 0) = 0
				AND ISNULL(I.bitKitParent, 0) = 0
				AND ISNULL(OI.bitDropship,0) = 0
				AND OM.numOppId NOT IN (SELECT numOppID FROM @TEMPOppID)

			IF (SELECT COUNT(*) FROM @TEMPOppID) > 0
			BEGIN
				DECLARE @tmpIds VARCHAR(MAX) = ''
				SELECT @tmpIds = CONCAT(@tmpIds,numOppID,', ') FROM @TEMPOppID
				SET @strSql = CONCAT(@strSql,' AND Opp.numOppID IN (',SUBSTRING(@tmpIds, 0, LEN(@tmpIds)),') ')
			END
			ELSE
			BEGIN
				SET @strSql = CONCAT(@strSql,' AND Opp.numOppID IN (0) ')
			END
		END
	END
	ELSE IF ISNULL(@tintDashboardReminderType,0) = 15
	BEGIN
		SET @strSql = CONCAT(@strSql,' AND Opp.numOppID IN (','SELECT
																	OMInner.numOppID
																FROM
																	OpportunityMaster OMInner
																WHERE
																	numDomainId=',@numDomainID,'
																	AND tintOppType=1
																	AND tintOppStatus=1
																	AND ISNULL(OMInner.tintshipped,0) = 0
																	AND dtReleaseDate IS NOT NULL
																	AND dtReleaseDate < DateAdd(minute,',@ClientTimeZoneOffset * -1,',GETUTCDATE())
																	AND (SELECT 
																			COUNT(*) 
																		FROM 
																		(
																			SELECT
																				OIInner.numoppitemtCode,
																				ISNULL(OIInner.numUnitHour,0) AS OrderedQty,
																				ISNULL(TempFulFilled.FulFilledQty,0) AS FulFilledQty
																			FROM
																				OpportunityItems OIInner
																			INNER JOIN
																				Item IInner
																			ON
																				OIInner.numItemCode = IInner.numItemCode
																			OUTER APPLY
																			(
																				SELECT
																					SUM(OpportunityBizDocItems.numUnitHour) AS FulFilledQty
																				FROM
																					OpportunityBizDocs
																				INNER JOIN
																					OpportunityBizDocItems 
																				ON
																					OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																				WHERE
																					OpportunityBizDocs.numOppId = OMInner.numOppId
																					AND ISNULL(OpportunityBizDocs.numBizDocId,0) = 296
																					AND ISNULL(OpportunityBizDocs.bitFulFilled,0) = 1
																					AND OpportunityBizDocItems.numOppItemID = OIInner.numoppitemtCode
																			) AS TempFulFilled
																			WHERE
																				OIInner.numOppID = OMInner.numOppID
																				AND UPPER(IInner.charItemType) = ''P''
																				AND ISNULL(OIInner.bitDropShip,0) = 0
																		) X
																		WHERE
																			X.OrderedQty <> X.FulFilledQty) > 0',') ')
	END



	DECLARE  @firstRec  AS INTEGER
	DECLARE  @lastRec  AS INTEGER
  
	SET @firstRec = (@CurrentPage - 1) * @PageSize
	SET @lastRec = (@CurrentPage * @PageSize + 1)



	-- EXECUTE FINAL QUERY
	DECLARE @strFinal AS NVARCHAR(MAX)
	SET @strFinal = CONCAT('SELECT ROW_NUMBER() OVER(ORDER BY ',@columnName,' ',@columnSortOrder,') ID, Opp.numOppID INTO #temp2', REPLACE(@strSql,'##PLACEHOLDER##',''),'; SELECT ID,',@strColumns,' INTO #tempTable',REPLACE(@strSql,'##PLACEHOLDER##',' JOIN #temp2 tblAllOrders ON Opp.numOppID = tblAllOrders.numOppID '),' AND tblAllOrders.ID > ',@firstRec,' and tblAllOrders.ID <',@lastRec,' ORDER BY ID; SELECT * FROM  #tempTable; DROP TABLE #tempTable; SELECT @TotalRecords = COUNT(*) FROM #temp2; DROP TABLE #temp2;')
	--SET @strFinal = CONCAT('SELECT ROW_NUMBER() OVER(ORDER BY ',@columnName,' ',@columnSortOrder,') ID, ',@strColumns,' INTO #tempTable',@strSql,'; SELECT * FROM #tempTable WHERE ID > ',@firstRec,' and ID <',@lastRec,' ORDER BY ID; SELECT @TotalRecords = COUNT(*) FROM #tempTable; DROP TABLE #tempTable;')

	PRINT CAST(@strFinal AS NTEXT)
	EXEC sp_executesql @strFinal, N'@TotalRecords INT OUT', @TotRecs OUT

	SELECT * FROM #tempForm

	DROP TABLE #tempForm

	DROP TABLE #tempColorScheme


--Created by Anoop Jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetFieldRelationship')
DROP PROCEDURE USP_GetFieldRelationship
GO
CREATE PROCEDURE USP_GetFieldRelationship
@byteMode as tinyint,
@numFieldRelID as numeric(9),
@numDomainID as numeric(9),
@numPrimaryListItemID as numeric(9),
@numSecondaryListID as numeric(9),
@bitTaskRelation AS BIT
as
BEGIN
	if @byteMode=1
	begin
	
	IF(@bitTaskRelation=1)
	BEGIN
		SELECT 
			FR.numFieldRelID,
			FR.numPrimaryListID,
			SD.vcStageName as RelationshipName
		FROM
			FieldRelationship AS FR
		LEFT JOIN
			StagePercentageDetails AS SD
		ON
			FR.numPrimaryListID=SD.numStageDetailsId
		WHERE
			FR.numDomainId=@numDomainID AND
			ISNULL(FR.bitTaskRelation,0)=1
	END
	ELSE
	BEGIN
		SELECT 
			numFieldRelID,L1.vcListName +' - ' + L2.vcListName as RelationshipName 
		FROM 
			FieldRelationship 
		JOIN 
			ListMaster L1
		ON 
			L1.numListID=numPrimaryListID
		JOIN 
			ListMaster L2
		ON
			L2.numListID=numSecondaryListID
		WHERE 
			FieldRelationship.numDomainID=@numDomainID 
			AND ISNULL(FieldRelationship.numModuleID,0) <> 14
			AND ISNULL(bitTaskRelation,0)=0
		UNION
		SELECT 
			numFieldRelID,L1.vcItemName +' - ' + L2.vcItemName as RelationshipName 
		FROM 
			FieldRelationship 
		JOIN 
			Item L1
		ON 
			L1.numItemCode=numPrimaryListID
		JOIN 
			Item L2
		ON 
			L2.numItemCode=numSecondaryListID
		WHERE 
			FieldRelationship.numDomainID=@numDomainID
			AND ISNULL(FieldRelationship.numModuleID,0) = 14
			AND ISNULL(bitTaskRelation,0) = 0
	END
	end
	else if @byteMode=2
	begin

	Select numFieldRelID,ISNULL(FieldRelationship.numModuleID,0) numModuleID,numPrimaryListID,numSecondaryListID,L1.vcListName +' - ' + L2.vcListName as RelationshipName,
	L1.vcListName as Item1,  L2.vcListName as Item2 from FieldRelationship 
	Join ListMaster L1
	on L1.numListID=numPrimaryListID
	Join ListMaster L2
	on L2.numListID=numSecondaryListID
	where  numFieldRelID=@numFieldRelID AND ISNULL(FieldRelationship.numModuleID,0) <> 14 AND ISNULL(bitTaskRelation,0)=0
	UNION
		SELECT 
			numFieldRelID
			,ISNULL(numModuleID,0) numModuleID
			,numPrimaryListID
			,numSecondaryListID
			,L1.vcItemName +' - ' + L2.vcItemName AS RelationshipName
			,L1.vcItemName as Item1
			,L2.vcItemName as Item2 
		FROM 
			FieldRelationship 
		JOIN 
			Item L1
		ON 
			L1.numItemCode=numPrimaryListID
		JOIN 
			Item L2
		ON 
			L2.numItemCode=numSecondaryListID
		WHERE 
			numFieldRelID=@numFieldRelID 
			AND ISNULL(numModuleID,0) = 14
			 AND ISNULL(bitTaskRelation,0)=0

	if @numPrimaryListItemID >0  AND @bitTaskRelation=1
	begin
		Select FieldRelationship.numFieldRelID,numFieldRelDTLID,SLI1.vcTaskName as PrimaryListItem,SLI2.vcTaskName as SecondaryListItem   from FieldRelationship
		Join FieldRelationshipDTL 
		on FieldRelationshipDTL.numFieldRelID=FieldRelationship.numFieldRelID
		Join StagePercentageDetailsTask SLI1
		on SLI1.numTaskId=numPrimaryListItemID
		Join StagePercentageDetailsTask SLI2
		on SLI2.numTaskId=numSecondaryListItemID
		where FieldRelationship.numFieldRelID=@numFieldRelID and numPrimaryListItemID=@numPrimaryListItemID AND ISNULL(bitTaskRelation,0)=1
	end
	ELSE if @numPrimaryListItemID >0 
	begin
		SELECT 
			FieldRelationship.numFieldRelID
			,numFieldRelDTLID
			,L1.vcData AS PrimaryListItem
			,L2.vcData AS SecondaryListItem 
		FROM 
			FieldRelationship
		JOIN 
			FieldRelationshipDTL 
		ON 
			FieldRelationshipDTL.numFieldRelID=FieldRelationship.numFieldRelID
		JOIN 
			ListDetails L1
		ON 
			L1.numListItemID=numPrimaryListItemID
		JOIN 
			ListDetails L2
		ON 
			L2.numListItemID=numSecondaryListItemID
		WHERE 
			FieldRelationship.numFieldRelID=@numFieldRelID 
			AND numPrimaryListItemID=@numPrimaryListItemID
			AND ISNULL(numModuleID,0) <> 14
			AND ISNULL(bitTaskRelation,0)=0
		UNION
		SELECT 
			FieldRelationship.numFieldRelID
			,numFieldRelDTLID
			,L1.vcItemName AS PrimaryListItem
			,L2.vcItemName AS SecondaryListItem 
		FROM 
			FieldRelationship
		JOIN 
			FieldRelationshipDTL 
		ON 
			FieldRelationshipDTL.numFieldRelID=FieldRelationship.numFieldRelID
		JOIN 
			Item L1
		ON 
			L1.numItemCode=numPrimaryListItemID
		JOIN 
			Item L2
		ON 
			L2.numItemCode=numSecondaryListItemID
		WHERE 
			FieldRelationship.numFieldRelID=@numFieldRelID 
			AND numPrimaryListItemID=@numPrimaryListItemID
			AND ISNULL(numModuleID,0) = 14
			AND ISNULL(bitTaskRelation,0)=0
	end

	end
	else if @byteMode=3
	begin


		Select L2.numListItemID,L2.vcData as SecondaryListItem   from FieldRelationship
		Join FieldRelationshipDTL 
		on FieldRelationshipDTL.numFieldRelID=FieldRelationship.numFieldRelID
		Join ListDetails L2
		on L2.numListItemID=numSecondaryListItemID
		where FieldRelationship.numDomainID=@numDomainID and numPrimaryListItemID=@numPrimaryListItemID
		and FieldRelationship.numSecondaryListID=@numSecondaryListID AND ISNULL(bitTaskRelation,0)=0
	end
END
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetItemPromotionDetails')
DROP PROCEDURE USP_GetItemPromotionDetails
GO
CREATE PROCEDURE [dbo].[USP_GetItemPromotionDetails]
	@numDomainID NUMERIC(18,0),
	@numDivisionID NUMERIC(18,0),
	@numItemCode NUMERIC(18,0),
	@numPromotionID NUMERIC(18,0),
	@numUnitHour FLOAT,
	@monTotalAmount DECIMAL(20,5)
AS
BEGIN
	IF ISNULL(@numPromotionID,0) > 0
	BEGIN
		DECLARE @vcCurrency VARCHAR(10)
		SELECT @vcCurrency=ISNULL(vcCurrency,'$') FROm Domain WHERE numDomainId=187

		SELECT TOP 1
			numProId 
			,ISNULL(vcLongDesc,'-') vcLongDesc
			,ISNULL(vcShortDesc,'-') vcShortDesc
			,ISNULL(tintOfferTriggerValueTypeRange,1) tintOfferTriggerValueTypeRange
			,ISNULL(tintOfferTriggerValueType,1) tintOfferTriggerValueType
			,ISNULL(fltOfferTriggerValue,0) fltOfferTriggerValue
			,ISNULL(fltOfferTriggerValueRange,0) fltOfferTriggerValueRange
			,(CASE 
				WHEN ISNULL(tintOfferTriggerValueType,1) = 1 --Based on quantity
				THEN
					CASE
						WHEN ISNULL(tintOfferTriggerValueTypeRange,1) = 2
						THEN
							(CASE WHEN ISNULL(@numUnitHour,0) BETWEEN ISNULL(fltOfferTriggerValue,0) AND ISNULL(fltOfferTriggerValueRange,0) THEN 1 ELSE 0 END)
						ELSE
							(CASE WHEN ISNULL(@numUnitHour,0) >= fltOfferTriggerValue THEN 1 ELSE 0 END)
					END
				WHEN ISNULL(tintOfferTriggerValueType,1) = 2 --Based on amount
				THEN
					CASE
						WHEN ISNULL(tintOfferTriggerValueTypeRange,1) = 2
						THEN
							(CASE WHEN ISNULL(@monTotalAmount,0) BETWEEN ISNULL(fltOfferTriggerValue,0) AND ISNULL(fltOfferTriggerValueRange,0) THEN 1 ELSE 0 END)
						ELSE
							(CASE WHEN ISNULL(@monTotalAmount,0) >= fltOfferTriggerValue THEN 1 ELSE 0 END)
					END
				END) canUsePromotion
			,(CASE 
				WHEN ISNULL(tintOfferTriggerValueType,1) = 1 --Based on quantity
				THEN
					CASE
						WHEN ISNULL(tintOfferTriggerValueTypeRange,1) = 2
						THEN
							(CASE 
								WHEN ISNULL(@numUnitHour,0) BETWEEN ISNULL(fltOfferTriggerValue,0) AND ISNULL(fltOfferTriggerValueRange,0) 
								THEN dbo.GetPromotionDiscountDescription(numProId,@vcCurrency)
								ELSE CONCAT('Increase qty to between ',ISNULL(fltOfferTriggerValue,0),' & ',ISNULL(fltOfferTriggerValueRange,0),' to use promotion') 
							END)
						ELSE
							(CASE 
								WHEN ISNULL(@numUnitHour,0) >= fltOfferTriggerValue 
								THEN dbo.GetPromotionDiscountDescription(numProId,@vcCurrency)
								ELSE CONCAT('Increase qty to ',ISNULL(fltOfferTriggerValue,0),' to use promotion') 
							END)
					END
				WHEN ISNULL(tintOfferTriggerValueType,1) = 2 --Based on amount
				THEN
					CASE
						WHEN ISNULL(tintOfferTriggerValueTypeRange,1) = 2
						THEN
							(CASE 
								WHEN ISNULL(@monTotalAmount,0) BETWEEN ISNULL(fltOfferTriggerValue,0) AND ISNULL(fltOfferTriggerValueRange,0) 
								THEN dbo.GetPromotionDiscountDescription(numProId,@vcCurrency)
								ELSE CONCAT('Increase amount to between ',@vcCurrency,ISNULL(fltOfferTriggerValue,0),' & ',@vcCurrency,ISNULL(fltOfferTriggerValueRange,0),' to use promotion') 
							END)
						ELSE
							(CASE 
								WHEN ISNULL(@monTotalAmount,0) >= fltOfferTriggerValue 
								THEN dbo.GetPromotionDiscountDescription(numProId,@vcCurrency) 
								ELSE CONCAT('Increase amount to ',@vcCurrency,ISNULL(fltOfferTriggerValue,0),' to use promotion') 
							END)
					END
			END) vcPromotionStatus
			,tintOfferBasedOn
		FROM 
			PromotionOffer PO
		WHERE 
			numDomainId=@numDomainID 
			AND numProId = @numPromotionID	
	END
	ELSE
	BEGIN
		DECLARE @numRelationship NUMERIC(18,0)
		DECLARE @numProfile NUMERIC(18,0)

		SELECT 
			@numRelationship=numCompanyType,
			@numProfile=vcProfile
		FROM 
			DivisionMaster D                  
		JOIN 
			CompanyInfo C 
		ON 
			C.numCompanyId=D.numCompanyID                  
		WHERE 
			numDivisionID =@numDivisionID 

		DECLARE @numItemClassification AS NUMERIC(18,0)
		SELECT @numItemClassification = numItemClassification FROM Item WHERE Item.numItemCode = @numItemCode

		SELECT TOP 1
			numProId 
			,ISNULL(vcShortDesc,'-') vcShortDesc
			,(CASE 
				WHEN ISNULL(tintOfferTriggerValueType,1) = 1 --Based on quantity
				THEN
					CASE
						WHEN ISNULL(tintOfferTriggerValueTypeRange,1) = 2
						THEN
							(CASE WHEN ISNULL(@numUnitHour,0) BETWEEN ISNULL(fltOfferTriggerValue,0) AND ISNULL(fltOfferTriggerValueRange,0) THEN 1 ELSE 0 END)
						ELSE
							(CASE WHEN ISNULL(@numUnitHour,0) >= fltOfferTriggerValue THEN 1 ELSE 0 END)
					END
				WHEN ISNULL(tintOfferTriggerValueType,1) = 2 --Based on amount
				THEN
					CASE
						WHEN ISNULL(tintOfferTriggerValueTypeRange,1) = 2
						THEN
							(CASE WHEN ISNULL(@monTotalAmount,0) BETWEEN ISNULL(fltOfferTriggerValue,0) AND ISNULL(fltOfferTriggerValueRange,0) THEN 1 ELSE 0 END)
						ELSE
							(CASE WHEN ISNULL(@monTotalAmount,0) >= fltOfferTriggerValue THEN 1 ELSE 0 END)
					END
			END) canUsePromotion
		FROM 
			PromotionOffer PO
		WHERE 
			numDomainId=@numDomainID 
			AND ISNULL(bitEnabled,0)=1 
			AND ISNULL(IsOrderBasedPromotion,0) = 0
			AND 1 = (CASE WHEN bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN GETUTCDATE()>=dtValidFrom AND GETUTCDATE()<=dtValidTo THEN 1 ELSE 0 END) END)
			AND 1 = (CASE tintCustomersBasedOn 
						WHEN 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=PO.numProId AND tintType=1 AND numDivisionID=@numDivisionID) > 0 THEN 1 ELSE 0 END)
						WHEN 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=PO.numProId AND tintType=2 AND numRelationship=@numRelationship AND numProfile=@numProfile) > 0 THEN 1 ELSE 0 END)
						WHEN 3 THEN 1
						ELSE 0
					END)
			AND 1 = (CASE 
						WHEN tintOfferBasedOn = 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=1 AND numValue=@numItemCode) > 0 THEN 1 ELSE 0 END)
						WHEN tintOfferBasedOn = 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=2 AND numValue=@numItemClassification) > 0 THEN 1 ELSE 0 END)
						WHEN tintOfferBasedOn = 4 THEN 1 
						ELSE 0
					END)
		ORDER BY
			CASE 
				WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 1 THEN 1
				WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 1 THEN 2
				WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 2 THEN 3
				WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 2 THEN 4
				WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 3 THEN 5
				WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 3 THEN 6
			END
	END
END 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetItemPromotionDiscountItems')
DROP PROCEDURE USP_GetItemPromotionDiscountItems
GO
CREATE PROCEDURE [dbo].[USP_GetItemPromotionDiscountItems]
	@numDomainID NUMERIC(18,0), 
	@numPromotionID NUMERIC(18,0),
	@numItemCode NUMERIC(18,0),
	@numWarehouseItemID NUMERIC(18,0),
	@numPageIndex INT,
	@numPageSize INT
AS
BEGIN
	DECLARE @tintDiscountBasedOn TINYINT
	DECLARE @tintDiscountType TINYINT
	DECLARE @fltDiscountValue FLOAT
	DECLARE @vcCurrency VARCHAR(10)
	DECLARE @numWarehouseID NUMERIC(18,0)
	DECLARE @tintItemCalDiscount TINYINT
	
	SELECT @vcCurrency=ISNULL(vcCurrency,'$') FROm Domain WHERE numDomainId=187

	SELECT @numWarehouseID=ISNULL(numWareHouseID,0) FROM WarehouseItems WHERE numWarehouseItemID = @numWarehouseItemID
	
	SELECT 
		@tintDiscountBasedOn=tintDiscoutBaseOn
		,@tintDiscountType=tintDiscountType 
		,@fltDiscountValue=ISNULL(fltDiscountValue,0)
		,@tintItemCalDiscount=ISNULL(tintItemCalDiscount,0)
	FROM 
		PromotionOffer 
	WHERE 
		numProId = @numPromotionID 

	DECLARE @TempItem TABLE
	(
		numItemCode NUMERIC(18,0)
	)

	PRINT @tintDiscountBasedOn

	IF @tintDiscountBasedOn = 1 -- Based on individual item(s)
	BEGIN
		INSERT INTO @TempItem
		(
			numItemCode
		)
		SELECT DISTINCT
			numValue
		FROM
			PromotionOfferItems
		WHERE
			numProId=@numPromotionID
			AND tintRecordType = 6
			AND tintType = 1
	END
	ELSE IF @tintDiscountBasedOn = 2 -- Based on individual item classification(s)
	BEGIN
		INSERT INTO @TempItem
		(
			numItemCode
		)
		SELECT DISTINCT
			numItemCode
		FROM
			Item
		WHERE
			numDomainID=@numDomainID
			AND numItemClassification IN (SELECT
												numValue
											FROM
												PromotionOfferItems
											WHERE
												numProId=@numPromotionID
												AND tintRecordType = 6
												AND tintType = 2)
	END
	ELSE IF @tintDiscountBasedOn = 3  -- Based on related item(s)
	BEGIN
		INSERT INTO @TempItem
		(
			numItemCode
		)
		SELECT DISTINCT
			numItemCode
		FROM
			SimilarItems
		WHERE
			numParentItemCode =@numItemCode
	END

	SELECT
		COUNT(Item.numItemCode) OVER() AS TotalRecords
		,Item.numItemCode
		,ISNULL(Item.vcItemName,'') vcItemName
		,ISNULL(Item.txtItemDesc,'') txtItemDesc
		,CASE 
			WHEN (ISNULL(bitKitParent,0)=1 OR ISNULL(bitAssembly,0) = 1) AND ISNULL(bitCalAmtBasedonDepItems,0) = 1
			THEN [dbo].[GetCalculatedPriceForKitAssembly](@numDomainID,Item.numItemCode,@numWarehouseItemID,ISNULL(tintKitAssemblyPriceBasedOn,1),0,0,'')
			ELSE ISNULL(monListPrice,0)
		END monListPrice
		,(CASE @tintDiscountType
			WHEN 1 THEN 0
			WHEN 2 THEN 1
			WHEN 3 THEN 0
		END) tintDiscountType
		,(CASE @tintDiscountType
			WHEN 1 THEN CONCAT(@fltDiscountValue,'%')
			WHEN 2 THEN CONCAT(@vcCurrency,@fltDiscountValue)
			WHEN 3 THEN '100%'
		END) vcDiscountType
		,(CASE @tintDiscountType
			WHEN 1 THEN @fltDiscountValue
			WHEN 2 THEN @fltDiscountValue
			WHEN 3 THEN 100
		END) fltDiscount
		,(CASE @tintDiscountType
			WHEN 1 THEN (CASE WHEN @fltDiscountValue > 0 THEN ISNULL(monListPrice,0) - (ISNULL(monListPrice,0) * (@fltDiscountValue/100))  ELSE ISNULL(monListPrice,0) END)
			WHEN 2 THEN (CASE WHEN @fltDiscountValue > 0 THEN (CASE WHEN ISNULL(monListPrice,0) > @fltDiscountValue THEN (ISNULL(monListPrice,0) - @fltDiscountValue) ELSE 0 END)  ELSE ISNULL(monListPrice,0) END)
			WHEN 3 THEN 0
		END) monSalePrice
		,(CASE @tintDiscountType
			WHEN 1 THEN (CASE WHEN @fltDiscountValue > 0 THEN ISNULL(monListPrice,0) - (ISNULL(monListPrice,0) * (@fltDiscountValue/100))  ELSE ISNULL(monListPrice,0) END)
			WHEN 2 THEN (CASE WHEN @fltDiscountValue > 0 THEN (CASE WHEN ISNULL(monListPrice,0) > @fltDiscountValue THEN (ISNULL(monListPrice,0) - @fltDiscountValue) ELSE 0 END)  ELSE ISNULL(monListPrice,0) END)
			WHEN 3 THEN 0
		END) monTotalAmount
		,numWareHouseItemID
		,ISNULL(Item.bitMatrix,0) bitMatrix
		,ISNULL(numItemGroup,0) numItemGroup
		,(CASE WHEN ISNULL(bitKitParent,0) = 1 THEN (CASE WHEN ISNULL((SELECT COUNT(*) FROM ItemDetails INNER JOIN Item IInner ON ItemDetails.numChildItemID=IInner.numItemCode WHERE numItemKitID=Item.numItemCode AND ISNULL(IInner.bitKitParent,0) = 1),0) > 0 THEN 1 ELSE 0 END) ELSE 0 END) bitHasChildKits
		,(CASE WHEN ISNULL(bitKitParent,0) = 1 OR ISNULL(bitAssembly,0) = 1 THEN ISNULL(bitCalAmtBasedonDepItems,0) ELSE 0 END) bitCalAmtBasedonDepItems
	FROM
		Item
	INNER JOIN
		@TempItem T1
	ON
		Item.numItemCode = T1.numItemCode
	OUTER APPLY
	(
		SELECT TOP 1
			numWareHouseItemID
		FROM
			WareHouseItems
		WHERE
			numItemID=Item.numItemCode
			AND (numWareHouseID=@numWarehouseID OR @numWarehouseItemID=0)
	) T2
	WHERE
		numDomainID=@numDomainID
	ORDER BY
		vcItemName
	OFFSET
		((@numPageIndex - 1) * @numPageSize) ROWS
	FETCH NEXT
		@numPageSize ROWS ONLY
END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetNamedPriceLevel')
DROP PROCEDURE USP_GetNamedPriceLevel
GO
CREATE PROCEDURE [dbo].[USP_GetNamedPriceLevel]
(
	@numDomainID NUMERIC(18,0)
)
AS 
BEGIN
	SELECT 
		pnt.tintPriceLevel AS Id
		,ISNULL(NULLIF(pnt.vcPriceLevelName, ''), CONCAT('Price Level ',pnt.tintPriceLevel)) Value
	FROM 
		PricingNamesTable pnt
	WHERE
		pnt.numDomainID = @numDomainID
	ORDER BY
		pnt.tintPriceLevel
END

GO
/****** Object:  StoredProcedure [dbo].[USP_GetOpportunityList1]    Script Date: 03/06/2009 00:37:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- created by anoop jayaraj                                                               
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getopportunitylist1')
DROP PROCEDURE usp_getopportunitylist1
GO
CREATE PROCEDURE [dbo].[USP_GetOpportunityList1]                                                                    
	@numUserCntID numeric(9)=0,
	@numDomainID numeric(9)=0,
	@tintUserRightType tinyint=0,
	@tintSortOrder tinyint=4,
	@dtLastDate datetime,
	@OppType as tinyint,
	@CurrentPage int,
	@PageSize int,
	@TotRecs int output,
	@columnName as Varchar(MAX),
	@columnSortOrder as Varchar(10),
	@numDivisionID as numeric(9)=0,
	@bitPartner as bit=0,
	@ClientTimeZoneOffset as int,
	@inttype as tinyint,
	@vcRegularSearchCriteria varchar(1000)='',
	@vcCustomSearchCriteria varchar(1000)='',
	@SearchText VARCHAR(300) = '',
	@SortChar char(1)='0',
	@tintDashboardReminderType TINYINT = 0                                                              
AS                
BEGIN
         
	DECLARE @PageId as TINYINT = 0
	DECLARE @numFormId  AS INT
           
	IF @inttype = 1 
	BEGIN           
		SET @PageId =2  
		SET @numFormId=38          
	END
	ELSE IF @inttype = 2            
	BEGIN
		SET @PageId =6 
		SET @numFormId=40           
	END

	CREATE TABLE #tempForm 
	(
		tintOrder TINYINT,vcDbColumnName nvarchar(50),vcOrigDbColumnName nvarchar(50),vcFieldName nvarchar(50),vcAssociatedControlType nvarchar(50),vcListItemType varchar(3),
		numListID numeric(9),vcLookBackTableName varchar(50),bitCustomField BIT,numFieldId NUMERIC,bitAllowSorting BIT,bitAllowEdit BIT,bitIsRequired BIT,bitIsEmail BIT,
		bitIsAlphaNumeric BIT,bitIsNumeric BIT,bitIsLengthValidation BIT,intMaxLength int,intMinLength int,bitFieldMessage BIT,vcFieldMessage VARCHAR(500),ListRelID numeric(9),
		intColumnWidth int,bitAllowFiltering BIT,vcFieldDataType CHAR(1)
	)

	-- FIRST CHECK IF GRID COLUMNS ARE CONFIGURED
	DECLARE @Nocolumns AS TINYINT = 0  

	SELECT 
		@Nocolumns = ISNULL(SUM(TotalRow),0)  
	FROM
	(            
		SELECT 
			COUNT(*) TotalRow 
		FROM 
			View_DynamicColumns 
		WHERE 
			numFormId=@numFormId 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND numRelCntType = @numFormId
		UNION 
		SELECT 
			COUNT(*) TotalRow 
		FROM 
			View_DynamicCustomColumns 
		WHERE 
			numFormId=@numFormId 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND numRelCntType = @numFormId
	) TotalRows
	
	DECLARE @IsMasterConfAvailable BIT = 0
	DECLARE @numUserGroup NUMERIC(18,0)

	-- IF NoColumns=0 CHECK IF MASTER FORM CONFIGURATION IS CREATED BY ADMINISTRATOR
	IF @Nocolumns=0
	BEGIN
		SELECT @numUserGroup = numGroupID FROM UserMaster WHERE numUserDetailId = @numUserCntID 

		IF (SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = @numDomainID AND numFormID = @numFormId AND numRelCntType=@numFormId AND bitGridConfiguration = 1 AND numGroupID = @numUserGroup) > 0
		BEGIN
			SET @IsMasterConfAvailable = 1
		END
	END

	--IF MASTERCONFIGURATION IS AVAILABLE THEN LOAD IT OTHERWISE LOAD DEFAULT COLUMNS
	IF @IsMasterConfAvailable = 1
	BEGIN
		INSERT INTO DycFormConfigurationDetails 
		(
			numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth
		)
		SELECT 
			@numFormId,numFieldId,intColumnNum,intRowNum,@numDomainID,@numUserCntID,@numFormId,1,0,intColumnWidth
		FROM 
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId=@numFormId AND 
			View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicColumnsMasterConfig.numRelCntType=@numFormId AND 
			View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
			ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
		UNION
		SELECT 
			@numFormId,numFieldId,intColumnNum,intRowNum,@numDomainID,@numUserCntID,@numFormId,1,1,intColumnWidth
		FROM 
			View_DynamicCustomColumnsMasterConfig
		WHERE 
			View_DynamicCustomColumnsMasterConfig.numFormId=@numFormId AND 
			View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicCustomColumnsMasterConfig.numRelCntType=@numFormId AND 
			View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1

		INSERT INTO 
			#tempForm
		SELECT 
			(intRowNum + 1) as tintOrder, vcDbColumnName,	vcOrigDbColumnName,	ISNULL(vcCultureFieldName,vcFieldName), vcAssociatedControlType,
			vcListItemType, numListID, vcLookBackTableName,	bitCustom, numFieldId, bitAllowSorting,	bitInlineEdit, bitIsRequired,
			bitIsEmail,	bitIsAlphaNumeric, bitIsNumeric, bitIsLengthValidation,	intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,
			ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType 
		FROM 
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId=@numFormId AND 
			View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicColumnsMasterConfig.numRelCntType=@numFormId AND 
			View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
			ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
		UNION
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName, vcAssociatedControlType,'' as vcListItemType,numListID,'',
			bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,''
		FROM 
			View_DynamicCustomColumnsMasterConfig
		WHERE 
			View_DynamicCustomColumnsMasterConfig.numFormId=@numFormId AND 
			View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicCustomColumnsMasterConfig.numRelCntType=@numFormId AND 
			View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1
		ORDER BY 
			tintOrder ASC  
	END
	ELSE                                        
	BEGIN 
		IF @Nocolumns=0
		BEGIN
			INSERT INTO DycFormConfigurationDetails 
			(
				numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth
			)
			SELECT 
				@numFormId,numFieldId,0,Row_number() over(order by tintRow desc),@numDomainID,@numUserCntID,@numFormId,1,0,intColumnWidth
			FROM 
				View_DynamicDefaultColumns
			WHERE 
				numFormId=@numFormId 
				AND bitDefault=1 
				AND ISNULL(bitSettingField,0)=1 
				AND numDomainID=@numDomainID
			ORDER BY 
				tintOrder ASC  
		END
  

		INSERT INTO 
			#tempForm
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcOrigDbColumnName,ISNULL(vcCultureFieldName,vcFieldName),vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName,
			bitCustom,numFieldId,bitAllowSorting,bitInlineEdit,bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength,bitFieldMessage,
			vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType  
		FROM 
			View_DynamicColumns 
		WHERE 
			numFormId=@numFormId 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND ISNULL(bitSettingField,0)=1 
			AND ISNULL(bitCustom,0)=0  
			AND numRelCntType = @numFormId
		UNION
		SELECT 
			tintRow+1 AS tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName,vcAssociatedControlType,'' as vcListItemType,numListID,'',bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,'' 
		FROM 
			View_DynamicCustomColumns
		WHERE 
			numFormId=@numFormId 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND ISNULL(bitCustom,0)=1 
			AND numRelCntType = @numFormId
		ORDER BY 
			tintOrder ASC             
	END
	
	DECLARE  @strColumns AS VARCHAR(MAX)
	SET @strColumns = ' ADC.numContactId, DM.numDivisionID, ISNULL(DM.numTerID,0) as numTerID, Opp.numRecOwner, DM.tintCRMType, Opp.numOppID, ISNULL(Opp.numBusinessProcessID,0) AS numBusinessProcessID'
        
	
	DECLARE @tintOrder as tinyint  = 0                                                  
	DECLARE @vcFieldName as varchar(50)                                                      
	DECLARE @vcListItemType as varchar(3)                                                 
	DECLARE @vcListItemType1 as varchar(1)                                                     
	DECLARE @vcAssociatedControlType varchar(10)                                                      
	DECLARE @numListID AS numeric(9)                                                      
	DECLARE @vcDbColumnName varchar(30)                          
	DECLARE @WhereCondition varchar(2000) = ''                          
	DECLARE @vcLookBackTableName varchar(2000)                    
	DECLARE @bitCustom as bit              
	DECLARE @numFieldId as numeric  
	DECLARE @bitAllowSorting AS CHAR(1)   
	DECLARE @bitAllowEdit AS CHAR(1)                 
	DECLARE @vcColumnName AS VARCHAR(500)                  
	Declare @ListRelID AS NUMERIC(9) 
	DECLARE @SearchQuery AS VARCHAR(MAX) = ''   

	SELECT TOP 1
		@tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName, @vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,
		@numListID=numListID,@vcLookBackTableName=vcLookBackTableName,@bitCustom=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,@bitAllowEdit=bitAllowEdit,
		@ListRelID=ListRelID
	FROM 
		#tempForm 
	ORDER BY 
		tintOrder asc            
                                                   
	WHILE @tintOrder>0                                                      
	BEGIN 
		IF @bitCustom = 0            
		BEGIN 
			DECLARE @Prefix as varchar(5)                                               
			
			IF @vcLookBackTableName = 'AdditionalContactsInformation'                    
				SET @Prefix = 'ADC.'                    
			IF @vcLookBackTableName = 'DivisionMaster'                    
				SET @Prefix = 'DM.'                    
			IF @vcLookBackTableName = 'OpportunityMaster'                    
				SET @PreFix ='Opp.'      
			IF @vcLookBackTableName = 'OpportunityRecurring'
				SET @PreFix = 'OPR.'
			IF @vcLookBackTableName = 'CompanyInfo'
				SET @PreFix = 'cmp.'
			IF @vcLookBackTableName = 'ProjectProgress'
				SET @PreFix = 'PP'

			SET @vcColumnName= @vcDbColumnName+'~'+ convert(varchar(10),@numFieldId) +'~0'
      
			IF @vcAssociatedControlType='SelectBox'                                                      
			BEGIN  
				IF @vcDbColumnName = 'numStatus'
				BEGIN
					SET @strColumns=@strColumns+ ' ,(SELECT COUNT(OpportunityItems.numoppitemtCode) FROM OpportunityItems 
					WHERE OpportunityItems.bitItemPriceApprovalRequired=1 AND OpportunityItems.numOppId=Opp.numOppId) ApprovalMarginCount,ISNULL(Opp.intPromotionApprovalStatus,0) AS intPromotionApprovalStatus '
				END
				
				IF @vcDbColumnName='numPartner'
				BEGIN
					SET @strColumns=@strColumns+ ' ,(Opp.numPartner) '+' ['+ @vcColumnName+']'    
					SET @strColumns=@strColumns+ ' ,(SELECT D.vcPartnerCode+''-''+C.vcCompanyName FROM DivisionMaster AS D LEFT JOIN CompanyInfo AS C ON D.numCompanyID=C.numCompanyID WHERE D.numDivisionID=Opp.numPartner) AS vcPartner' 
				END
				ELSE IF @vcDbColumnName = 'numPartnerContact'
				BEGIN
					SET @strColumns=@strColumns+ ' ,(Opp.numPartenerContact) '+' ['+ @vcColumnName+']'    
					SET @strColumns=@strColumns+ ' ,(SELECT A.vcFirstName+'' ''+ A.vcLastName FROM AdditionalContactsInformation AS A WHERE A.numContactId=Opp.numPartenerContact) AS vcPartnerContact' 
				END
				ELSE IF @vcDbColumnName = 'tintSource'
				BEGIN
					SET @strColumns = @strColumns + ',dbo.fn_GetOpportunitySourceValue(ISNULL(Opp.tintSource,0),ISNULL(Opp.tintSourceType,0),Opp.numDomainID) ' + ' [' + @vcColumnName + ']'

					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'dbo.fn_GetOpportunitySourceValue(ISNULL(Opp.tintSource,0),ISNULL(Opp.tintSourceType,0),Opp.numDomainID) LIKE ''%' + @SearchText + '%'''
					END
				END
				ELSE IF @vcDbColumnName = 'tintOppStatus'
				BEGIN
					SET @strColumns=@strColumns+ ' ,(CASE ISNULL(Opp.tintOppStatus,0) WHEN 1 THEN ''Deal Won'' WHEN 2 THEN ''Deal Lost'' ELSE ''Open'' END)' + ' [' + @vcColumnName + ']'
				END
				ELSE IF @vcDbColumnName = 'numContactId'
				BEGIN
					SET @strColumns = @strColumns + ',dbo.fn_GetContactName('+@Prefix+@vcDbColumnName+') ['+ @vcColumnName+']'     
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'dbo.fn_GetContactName('+@Prefix+@vcDbColumnName+') LIKE ''%' + @SearchText + '%'''
					END             
				END
				ELSE if @vcListItemType='LI'                                                       
				BEGIN
					SET @strColumns = @strColumns+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName+']'                                                      
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'L' + CONVERT(VARCHAR(3),@tintOrder) + '.vcData LIKE ''%' + @SearchText + '%'''
					END

					SET @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID='+@vcDbColumnName                                                      
				END                                                      
				ELSE IF @vcListItemType='S'                                                       
				BEGIN
					SET @strColumns = @strColumns+',S'+ convert(varchar(3),@tintOrder)+'.vcState'+' ['+ @vcColumnName+']'   
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'S'+ convert(varchar(3),@tintOrder)+'.vcState LIKE ''%' + @SearchText + '%'''
					END
					                                                   
					SET @WhereCondition= @WhereCondition +' left join State S'+ convert(varchar(3),@tintOrder)+ ' on S'+convert(varchar(3),@tintOrder)+ '.numStateID='+@vcDbColumnName                                                      
				END     
				ELSE IF @vcListItemType = 'BP'
				BEGIN
					SET @strColumns = @strColumns + ',SPLM.Slp_Name' + ' ['	+ @vcColumnName + ']'

					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'SPLM.Slp_Name LIKE ''%' + @SearchText + '%'''
					END

					SET @WhereCondition = @WhereCondition + ' LEFT JOIN Sales_process_List_Master SPLM on SPLM.Slp_Id = OPP.numBusinessProcessID '
				END
				ELSE IF @vcListItemType='PP'
				BEGIN
					SET @strColumns = @strColumns+', CASE WHEN PP.numOppID >0 then ISNULL(PP.intTotalProgress,0) ELSE dbo.GetListIemName(OPP.numPercentageComplete) END '+' ['+ @vcColumnName+']'

					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + '(CASE WHEN PP.numOppID >0 then CONCAT(ISNULL(PP.intTotalProgress,0),''%'') ELSE dbo.GetListIemName(OPP.numPercentageComplete) END) LIKE ''%' + @SearchText + '%'''
					END
				END  
				ELSE IF @vcListItemType='T'                                                       
				BEGIN
					SET @strColumns = @strColumns+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName+']' 
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'L'+ convert(varchar(3),@tintOrder)+'.vcData LIKE ''%' + @SearchText + '%'''
					END
					                                                     
					SET @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID='+@vcDbColumnName                                                      
				END                     
				ELSE IF @vcListItemType='U'                                                   
				BEGIN
					SET @strColumns = @strColumns + ',dbo.fn_GetContactName('+@Prefix+@vcDbColumnName+') ['+ @vcColumnName+']'     
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'dbo.fn_GetContactName('+@Prefix+@vcDbColumnName+') LIKE ''%' + @SearchText + '%'''
					END              
				END  
				ELSE IF @vcListItemType = 'WI'
				BEGIN
					SET @strColumns = @strColumns + ', Case when isnull(OL.numSiteID,0)>0 then Sites.vcSiteName else WebAPI.vcProviderName end [' + @vcColumnName + ']'
				  
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + '(Case when isnull(OL.numSiteID,0)>0 then Sites.vcSiteName else WebAPI.vcProviderName end) LIKE ''%' + @SearchText + '%'''
					END 

					SET @WhereCondition= @WhereCondition +' left join WebAPI on WebAPI.WebApiId=isnull(OL.numWebApiId,0)
															left join Sites on isnull(OL.numSiteID,0)=Sites.numSIteID and Sites.numDomainID=' + convert(varchar(15),@numDomainID)                                                    
			   END                  
			END               
			ELSE IF @vcAssociatedControlType='DateField'                                                      
			BEGIN  
				SET @strColumns=@strColumns+',case when convert(varchar(11),'+@Prefix+@vcDbColumnName+')= convert(varchar(11),getdate()) then ''<b><font color=red>Today</font></b>'''      
				SET @strColumns=@strColumns+'when convert(varchar(11),'+@Prefix+@vcDbColumnName+')= convert(varchar(11),dateadd(day,-1,getdate())) then ''<b><font color=purple>YesterDay</font></b>'''      
				SET @strColumns=@strColumns+'when convert(varchar(11),'+@Prefix+@vcDbColumnName+')= convert(varchar(11),dateadd(day,1,getdate())) then''<b><font color=orange>Tommorow</font></b>'' '      
				SET @strColumns=@strColumns+'else dbo.FormatedDateFromDate('+@Prefix+@vcDbColumnName+','+convert(varchar(10),@numDomainId)+') end  ['+ @vcColumnName+']'               

				IF LEN(@SearchText) > 0
				BEGIN
					SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'dbo.FormatedDateFromDate('+@Prefix+@vcDbColumnName+','+ CONVERT(VARCHAR(10),@numDomainId)+') LIKE ''%' + @SearchText + '%'''
				END
			END              
			ELSE IF @vcAssociatedControlType='TextBox'                                                      
			BEGIN  
				SET @strColumns = @strColumns + ','+ CASE 
														WHEN @vcDbColumnName='numAge' then 'year(getutcdate())-year(bintDOB)'                   
														WHEN @vcDbColumnName='monPAmount' then 'isnull((SELECT varCurrSymbol FROM [Currency] WHERE numCurrencyID = Opp.[numCurrencyID]),'''') + '' ''+ CONVERT(VARCHAR,CONVERT(DECIMAL(20,5),monPAmount),1)'
														WHEN @vcDbColumnName='numDivisionID' then 'DM.numDivisionID'
														WHEN @vcDbColumnName = 'tintOppStatus' THEN 'CASE tintOppStatus WHEN 0 then ''Open'' WHEN 1 THEN ''Closed-Won'' WHEN 2 THEN ''Closed-Lost'' END '
														WHEN @vcDbColumnName = 'tintOppType' THEN 'CASE tintOppType WHEN 1 then ''Sales'' WHEN 2 THEN ''Purchase'' Else ''NA'' END ' 
														ELSE @Prefix + @vcDbColumnName 
													END +' ['+ @vcColumnName+']'  
				
				IF LEN(@SearchText) > 0
				BEGIN
					SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + CASE 
														WHEN @vcDbColumnName='numAge' then 'year(getutcdate())-year(bintDOB)'                   
														WHEN @vcDbColumnName='monPAmount' then 'isnull((SELECT varCurrSymbol FROM [Currency] WHERE numCurrencyID = Opp.[numCurrencyID]),'''') + '' ''+ CONVERT(VARCHAR,CONVERT(DECIMAL(20,5),monPAmount),1)'
														WHEN @vcDbColumnName='numDivisionID' then 'DM.numDivisionID'
														WHEN @vcDbColumnName = 'tintOppStatus' THEN 'CASE tintOppStatus WHEN 0 then ''Open'' WHEN 1 THEN ''Closed-Won'' WHEN 2 THEN ''Closed-Lost'' END '
														WHEN @vcDbColumnName = 'tintOppType' THEN 'CASE tintOppType WHEN 1 then ''Sales'' WHEN 2 THEN ''Purchase'' Else ''NA'' END ' 
														ELSE @Prefix + @vcDbColumnName 
													END + ' LIKE ''%' + @SearchText + '%'''
				END              
			END  
			ELSE IF @vcAssociatedControlType='TextArea'                                              
			BEGIN  
				SET @strColumns=@strColumns+','+  @Prefix + @vcDbColumnName +' ['+ @vcColumnName+']' 

				IF LEN(@SearchText) > 0
				BEGIN
					SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + @Prefix + @vcDbColumnName + ' LIKE ''%' + @SearchText + '%'''
				END     
			END
			ELSE IF @vcAssociatedControlType='Label'                                              
			BEGIN  
				SET @strColumns = @strColumns + ',' + CASE 
												WHEN @vcDbColumnName = 'CalAmount' THEN '[dbo].[getdealamount](Opp.numOppId,Getutcdate(),0)'
												WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'monDealAmount' THEN 'ISNULL((SELECT SUM(ISNULL(monDealAmount,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)'
												WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'monAmountPaid' THEN 'ISNULL((SELECT SUM(ISNULL(monAmountPaid,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)'
												WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'vcBizDocsList' THEN 'ISNULL((SELECT SUBSTRING((SELECT ''$^$'' + CAST(numOppBizDocsId AS VARCHAR(18)) +''#^#''+ vcBizDocID
												FROM OpportunityBizDocs WHERE numOppId=Opp.numOppId FOR XML PATH('''')),4,200000)),'''')'
												else @Prefix + @vcDbColumnName 
											END +' ['+ @vcColumnName+']'     
											
				IF LEN(@SearchText) > 0
				BEGIN
					SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + CASE 
												WHEN @vcDbColumnName = 'CalAmount' THEN '[dbo].[getdealamount](Opp.numOppId,Getutcdate(),0)'
												WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'monDealAmount' THEN 'ISNULL((SELECT SUM(ISNULL(monDealAmount,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)'
												WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'monAmountPaid' THEN 'ISNULL((SELECT SUM(ISNULL(monAmountPaid,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)'
												WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'vcBizDocsList' THEN 'ISNULL((SELECT SUBSTRING((SELECT ''$^$'' + CAST(numOppBizDocsId AS VARCHAR(18)) +''#^#''+ vcBizDocID
												FROM OpportunityBizDocs WHERE numOppId=Opp.numOppId FOR XML PATH('''')),4,200000)),'''')'
												else @Prefix + @vcDbColumnName
											END + ' LIKE ''%' + @SearchText + '%'''
				END           
			END
  			ELSE IF @vcAssociatedControlType='Popup' and @vcDbColumnName='numShareWith'                                                   
			BEGIN  
				SET @strColumns = @strColumns+',(SELECT SUBSTRING(
				(SELECT '','' + A.vcFirstName+'' ''+A.vcLastName + CASE WHEN ISNULL(SR.numContactType,0)>0 THEN ''('' + dbo.fn_GetListItemName(SR.numContactType) + '')'' ELSE '''' END
				FROM ShareRecord SR JOIN UserMaster UM ON UM.numUserDetailId=SR.numAssignedTo 
											JOIN AdditionalContactsInformation A ON UM.numUserDetailId=A.numContactID 
						WHERE SR.numDomainID=Opp.numDomainID AND SR.numModuleID=3 AND SR.numRecordID=Opp.numOppId
							AND UM.numDomainID=Opp.numDomainID and UM.numDomainID=A.numDomainID FOR XML PATH('''')),2,200000)) ['+ @vcColumnName+']'    
				
				IF LEN(@SearchText) > 0
				BEGIN
					SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + '(SELECT SUBSTRING(
						(SELECT '','' + A.vcFirstName+'' ''+A.vcLastName + CASE WHEN ISNULL(SR.numContactType,0)>0 THEN ''('' + dbo.fn_GetListItemName(SR.numContactType) + '')'' ELSE '''' END
						FROM ShareRecord SR JOIN UserMaster UM ON UM.numUserDetailId=SR.numAssignedTo 
													JOIN AdditionalContactsInformation A ON UM.numUserDetailId=A.numContactID 
								WHERE SR.numDomainID=Opp.numDomainID AND SR.numModuleID=3 AND SR.numRecordID=Opp.numOppId
									AND UM.numDomainID=Opp.numDomainID and UM.numDomainID=A.numDomainID FOR XML PATH('''')),2,200000)) LIKE ''%' + @SearchText + '%'''
				END            
			END  
		END               
		Else                          
		BEGIN 
			SET @vcColumnName= @vcDbColumnName +'~'+ convert(varchar(10),@numFieldId)+'~1'
               
			IF @vcAssociatedControlType = 'TextBox' or @vcAssociatedControlType = 'TextArea'          
			BEGIN              
				SET @strColumns= @strColumns+',CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value  ['+ @vcColumnName + ']'               
				SET @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values_Opp CFW'+ convert(varchar(3),@tintOrder)+ '             
				on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=Opp.numOppid   '                                                     
			END            
			ELSE IF @vcAssociatedControlType = 'CheckBox'         
			BEGIN          
				SET @strColumns = @strColumns + ',case when isnull(CFW'+ CONVERT(VARCHAR(3),@tintOrder)+'.Fld_Value,0)=0 then ''No'' when isnull(CFW'+ CONVERT(VARCHAR(3),@tintOrder)+'.Fld_Value,0)=1 then ''Yes'' end   ['+ @vcColumnName + ']'             
				SET @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values_Opp CFW'+ convert(varchar(3),@tintOrder)+ '           
					on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=Opp.numOppid   '                                                   
			END          
			ELSE IF @vcAssociatedControlType = 'DateField'       
			BEGIN            
				set @strColumns= @strColumns+',dbo.FormatedDateFromDate(CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value,'+convert(varchar(10),@numDomainId)+')  ['+ @vcColumnName + ']'               
				set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values_Opp CFW'+ convert(varchar(3),@tintOrder)+ '             
				on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=Opp.numOppid    '                                                     
			END            
			ELSE IF @vcAssociatedControlType = 'SelectBox'         
			BEGIN            
				SET @vcDbColumnName = 'DCust'+Convert(varchar(10),@numFieldId)    
				        
				SET @strColumns = @strColumns+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName + ']'                                                      
				SET @WhereCondition = @WhereCondition +' left Join CFW_FLD_Values_Opp CFW'+ convert(varchar(3),@tintOrder)+ '             
									on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=Opp.numOppid     '                                                     
				SET @WhereCondition = @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID=CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value' 
			END
			ELSE
			BEGIN
				SET @strColumns = @strColumns + CONCAT(',dbo.GetCustFldValueOpp(',@numFieldId,',opp.numOppID)') + ' [' + @vcColumnName + ']'
			END
		   
			IF LEN(@SearchText) > 0
			BEGIN
				SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + CONCAT('dbo.GetCustFldValueOpp(',@numFieldId,',opp.numOppID) LIKE ''%',@SearchText,'%''')
			END        
		END 
  
		SELECT TOP 1 
			@tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName,@vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,
			@numListID=numListID,@vcLookBackTableName=vcLookBackTableName,@bitCustom=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,@bitAllowEdit=bitAllowEdit,
			@ListRelID=ListRelID
		FROM 
			#tempForm 
		WHERE 
			tintOrder > @tintOrder-1 
		ORDER BY 
			tintOrder asc            
 
		IF @@rowcount=0 
			SET @tintOrder=0 
	END                            
		
	 
	DECLARE @strShareRedordWith AS VARCHAR(300);
	SET @strShareRedordWith=' Opp.numOppId in (select SR.numRecordID FROM ShareRecord SR WHERE SR.numDomainID=' + CONVERT(VARCHAR(15),@numDomainID) + ' AND SR.numModuleID=3 AND SR.numAssignedTo=' + CONVERT(VARCHAR(15),@numUserCntId) + ') '
                                             
	DECLARE @strSql AS VARCHAR(MAX) = ''                                       
                                                                                 
	SET @strSql = ' FROM 
						OpportunityMaster Opp                                                                     
					INNER JOIN 
						AdditionalContactsInformation ADC                                                                     
					ON
						Opp.numContactId = ADC.numContactId                                  
					INNER JOIN 
						DivisionMaster DM                                                                     
					ON 
						Opp.numDivisionId = DM.numDivisionID AND ADC.numDivisionId = DM.numDivisionID                                                                     
					INNER JOIN 
						CompanyInfo cmp                                                                     
					ON 
						DM.numCompanyID = cmp.numCompanyId                                                                         
					LEFT JOIN 
						AdditionalContactsInformation ADC1 
					ON 
						ADC1.numContactId=Opp.numRecOwner 
					LEFT JOIN 
						ProjectProgress PP 
					ON 
						PP.numOppID = Opp.numOppID
					LEFT JOIN 
						OpportunityLinking OL 
					on 
						OL.numChildOppID=Opp.numOppId ' + @WhereCondition
                                     

	-------Change Row Color-------
	DECLARE @vcCSOrigDbCOlumnName AS VARCHAR(50) = ''
	DECLARE @vcCSLookBackTableName AS VARCHAR(50) = ''
	DECLARE @vcCSAssociatedControlType AS VARCHAR(50) = ''

	CREATE TABLE #tempColorScheme
	(
		vcOrigDbCOlumnName VARCHAR(50),
		vcLookBackTableName VARCHAR(50),
		vcFieldValue VARCHAR(50),
		vcFieldValue1 VARCHAR(50),
		vcColorScheme VARCHAR(50),
		vcAssociatedControlType VARCHAR(50)
	)

	INSERT INTO 
		#tempColorScheme 
	SELECT 
		DFM.vcOrigDbCOlumnName,
		DFM.vcLookBackTableName,
		DFCS.vcFieldValue,
		DFCS.vcFieldValue1,
		DFCS.vcColorScheme,
		DFFM.vcAssociatedControlType 
	FROM 
		DycFieldColorScheme DFCS 
	JOIN 
		DycFormField_Mapping DFFM 
	ON 
		DFCS.numFieldID=DFFM.numFieldID
	JOIN 
		DycFieldMaster DFM 
	ON 
		DFM.numModuleID=DFFM.numModuleID 
		AND DFM.numFieldID=DFFM.numFieldID
	WHERE 
		DFCS.numDomainID=@numDomainID 
		AND DFFM.numFormID=@numFormId 
		AND DFCS.numFormID=@numFormId 
		AND ISNULL(DFFM.bitAllowGridColor,0)=1

	IF(SELECT COUNT(*) FROM #tempColorScheme)>0
	BEGIN
		SELECT TOP 1 @vcCSOrigDbCOlumnName=vcOrigDbCOlumnName,@vcCSLookBackTableName=vcLookBackTableName,@vcCSAssociatedControlType=vcAssociatedControlType FROM #tempColorScheme
	END   

	IF len(@vcCSOrigDbCOlumnName)>0 and len(@vcCSLookBackTableName)>0
	BEGIN
		SET @strColumns = @strColumns + ',tCS.vcColorScheme'
	END

	IF len(@vcCSOrigDbCOlumnName)>0 and len(@vcCSLookBackTableName)>0
	BEGIN
		IF @vcCSLookBackTableName = 'AdditionalContactsInformation'                  
			SET @Prefix = 'ADC.'                  
		IF @vcCSLookBackTableName = 'DivisionMaster'                  
			SET @Prefix = 'DM.'
		IF @vcCSLookBackTableName = 'CompanyInfo'                  
			SET @Prefix = 'CMP.' 
		IF @vcCSLookBackTableName = 'OpportunityMaster'                  
			SET @Prefix = 'Opp.'
		
		IF @vcCSAssociatedControlType='DateField'
		BEGIN
			SET @strSql=@strSql+' LEFT JOIN #tempColorScheme tCS on CONVERT(VARCHAR(10),' + @Prefix + @vcCSOrigDbCOlumnName + ',111) >= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue as int),GETUTCDATE()),111)
					and CONVERT(VARCHAR(10),' + @Prefix + @vcCSOrigDbCOlumnName + ',111) <= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue1 as int),GETUTCDATE()),111)'
		END
		ELSE IF @vcCSAssociatedControlType='SelectBox' OR @vcCSAssociatedControlType='ListBox' OR @vcCSAssociatedControlType='CheckBox'
		BEGIN
			SET @strSql=@strSql+' LEFT JOIN #tempColorScheme tCS on tCS.vcFieldValue=' + @Prefix + @vcCSOrigDbCOlumnName
		END
	END      

    DECLARE @CustomControlType AS VARCHAR(10)      
	IF @columnName like 'CFW.Cust%'             
	BEGIN

		SET @strSql = @strSql +' LEFT JOIN CFW_Fld_Values_Opp CFW on CFW.RecId=Opp.numOppid and CFW.fld_id= ' + REPLACE(@columnName,'CFW.Cust','') +' ' 
		
		SET @CustomControlType = (SELECT Fld_type FROM CFW_Fld_Master CFWM where CFWM.Fld_id = REPLACE(@columnName,'CFW.Cust',''))

		IF @CustomControlType = 'DateField'
		BEGIN
			 SET @columnName='CAST((CASE WHEN CFW.Fld_Value = ''0'' THEN NULL ELSE CFW.Fld_Value END) AS Date)'   
		END
		ELSE
		BEGIN
			SET @columnName='CFW.Fld_Value'   
		END
	END
	
	ELSE IF @columnName like 'DCust%'            
	BEGIN            
		SET @strSql = @strSql + ' left Join CFW_Fld_Values_Opp CFW on CFW.RecId=Opp.numOppid   and CFW.fld_id= '+replace(@columnName,'DCust','')                                                                                               
		SET @strSql = @strSql + ' left Join ListDetails LstCF on LstCF.numListItemID=CFW.Fld_Value  '            
		set @columnName='LstCF.vcData'                   
	END
                                        
	IF @bitPartner=1 
	BEGIN
		SET @strSql=@strSql+' LEFT JOIN OpportunityContact OppCont ON OppCont.numOppId=Opp.numOppId'                                                                               
	END

	SET @strSql = @strSql +' WHERE Opp.tintOppstatus=0 AND DM.numDomainID=' + CONVERT(VARCHAR(15),@numDomainID) + ' AND Opp.tintOppType= ' + convert(varchar(1),@OppType)                                                              

	IF @SortChar <> '0' 
		SET @strSql = @strSql + ' And Opp.vcPOppName like ''' + @SortChar + '%'''
                                
	IF @numDivisionID <> 0 
	BEGIN
		SET @strSql = @strSql + ' AND DM.numDivisionID =' + CONVERT(VARCHAR(15),@numDivisionID)                              
    END
	                                                                
	IF @tintUserRightType=1 
	BEGIN
		SET @strSql = @strSql + ' AND (Opp.numRecOwner = '
							+ CONVERT(VARCHAR(15),@numUserCntID) 
							+ ' or Opp.numAssignedTo= '
							+ convert(varchar(15),@numUserCntID)  
							+' or  Opp.numOppId in (select distinct(numOppId) from OpportunityStageDetails where numAssignTo ='
							+ convert(varchar(15),@numUserCntID)+')'
							+ case when @bitPartner=1 then ' or ( OppCont.bitPartner=1 and OppCont.numContactId='+convert(varchar(15),@numUserCntID)+ ')' else '' end 
							+ ' or ' + @strShareRedordWith +')'
    END                                        
	ELSE IF @tintUserRightType=2 
	BEGIN
		SET @strSql = @strSql + ' AND (DM.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '
							+ convert(varchar(15),@numUserCntID)+' ) or DM.numTerID=0 or Opp.numAssignedTo= '
							+ convert(varchar(15),@numUserCntID)  +' or  Opp.numOppId in (select distinct(numOppId) from OpportunityStageDetails where numAssignTo ='
							+ convert(varchar(15),@numUserCntID)+')' + ' or ' + @strShareRedordWith +')'                                                                        
	END           
	
	                                                  
	IF @tintSortOrder=1 
	BEGIN 
		SET @strSql = @strSql + ' AND (Opp.numRecOwner = ' 
							+ CONVERT(VARCHAR(15),@numUserCntID) 
							+ ' or Opp.numAssignedTo= '+ convert(varchar(15),@numUserCntID) 
							+ ' or  Opp.numOppId in (select distinct(numOppId) from OpportunityStageDetails where numAssignTo ='
							+ convert(varchar(15),@numUserCntID)+')'
							+ case when @bitPartner=1 then ' or ( OppCont.bitPartner=1 and OppCont.numContactId='+convert(varchar(15),@numUserCntID)+ ')' else '' end 
							+ ' or ' + @strShareRedordWith +')'                        
    END
	ELSE IF @tintSortOrder=2  
	BEGIN
		SET @strSql = @strSql + 'AND (Opp.numRecOwner in (select A.numContactID from AdditionalContactsInformation A where A.numManagerID='
							+ convert(varchar(15),@numUserCntID) 
							+') or Opp.numAssignedTo in (select A.numContactID from AdditionalContactsInformation A where A.numManagerID='
							+ convert(varchar(15),@numUserCntID)  
							+') or  Opp.numOppId in (select distinct(numOppId) from OpportunityStageDetails where numAssignTo in (select A.numContactID from AdditionalContactsInformation A where A.numManagerID='
							+ convert(varchar(15),@numUserCntID)+'))'
							+ case when @bitPartner=1 then ' or ( OppCont.bitPartner=1 and OppCont.numContactId in (select A.numContactID from AdditionalContactsInformation A where A.numManagerID='+ convert(varchar(15),@numUserCntID)+ '))' else '' end 
							+ ' or ' + @strShareRedordWith +')'                                                                    
	END
	ELSE IF @tintSortOrder=4  
	BEGIN
		SET @strSql=@strSql + ' AND Opp.bintCreatedDate> '''+convert(varchar(20),dateadd(day,-8,@dtLastDate))+''''                                                                    
	END
                                                 

	IF @vcRegularSearchCriteria <> '' 
	BEGIN
		SET @strSql = @strSql + ' AND ' + @vcRegularSearchCriteria 
	END

	IF @vcCustomSearchCriteria <> '' 
	BEGIN
		SET @strSql = @strSql + ' AND Opp.numOppid IN (SELECT DISTINCT CFW.RecId FROM CFW_Fld_Values_Opp CFW where ' + @vcCustomSearchCriteria + ')'
	END

	IF LEN(@SearchQuery) > 0
	BEGIN
		SET @strSql = @strSql + ' AND (' + @SearchQuery + ') '
	END

	IF ISNULL(@tintDashboardReminderType,0) = 4
	BEGIN
		SET @strSql = CONCAT(@strSql,' AND Opp.numOppID IN (','SELECT DISTINCT
																	OMInner.numOppID
																FROM 
																	StagePercentageDetails SPDInner
																INNER JOIN 
																	OpportunityMaster OMInner
																ON 
																	SPDInner.numOppID=OMInner.numOppId 
																WHERE 
																	SPDInner.numDomainId=',@numDOmainID,'
																	AND OMInner.numDomainId=',@numDOmainID,' 
																	AND OMInner.tintOppType = 1
																	AND ISNULL(OMInner.tintOppStatus,0) = 0
																	AND tinProgressPercentage <> 100',') ')

	END
	ELSE IF ISNULL(@tintDashboardReminderType,0) = 18
	BEGIN
		SET @strSql = CONCAT(@strSql,' AND Opp.numOppID IN (','SELECT DISTINCT
																	OMInner.numOppID
																FROM 
																	StagePercentageDetails SPDInner
																INNER JOIN 
																	OpportunityMaster OMInner
																ON 
																	SPDInner.numOppID=OMInner.numOppId 
																WHERE 
																	SPDInner.numDomainId=',@numDOmainID,'
																	AND OMInner.numDomainId=',@numDOmainID,' 
																	AND OMInner.tintOppType = 2
																	AND ISNULL(OMInner.tintOppStatus,0) = 0
																	AND tinProgressPercentage <> 100',') ')

	END

	DECLARE @firstRec AS INTEGER                                                              
	DECLARE @lastRec AS INTEGER    
	                                                 
	SET @firstRec= (@CurrentPage-1) * @PageSize                             
	SET @lastRec= (@CurrentPage*@PageSize+1)                                                                    


    -- GETS ROWS COUNT
	DECLARE @strTotal NVARCHAR(MAX) = 'SELECT @TotalRecords = COUNT(*) ' + @strSql
	exec sp_executesql @strTotal, N'@TotalRecords INT OUT', @TotRecs OUT
               
                              
	-- EXECUTE FINAL QUERY
	DECLARE @strFinal AS VARCHAR(MAX)
	SET @strFinal = CONCAT('SELECT ROW_NUMBER() OVER(ORDER BY ',@columnName,' ',@columnSortOrder,') ID, ',@strColumns,' INTO #tempTable',@strSql,'; SELECT * FROM #tempTable WHERE ID > ',@firstRec,' and ID <',@lastRec,' ORDER BY ID; DROP TABLE #tempTable;')
	PRINT @strFinal
	EXEC (@strFinal) 

	SELECT * FROM #tempForm

	DROP TABLE #tempForm
	DROP TABLE #tempColorScheme
END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetPromotionOffer')
DROP PROCEDURE USP_GetPromotionOffer
GO
CREATE PROCEDURE [dbo].[USP_GetPromotionOffer]
    @numProId AS NUMERIC(9) = 0,
    @byteMode AS TINYINT = 0,
    @numDomainID AS NUMERIC(9) = 0,
	@ClientTimeZoneOffset AS INT
AS 
BEGIN
    IF @byteMode = 0 
    BEGIN    
		SELECT
			[numProId]
			,[vcProName]
			,[numDomainId]
			,(CASE WHEN [dtValidFrom] IS NOT NULL AND ISNULL(bitNeverExpires,0) = 0 THEN DATEADD(MINUTE,-@ClientTimeZoneOffset,dtValidFrom) ELSE dtValidFrom END) dtValidFrom
			,(CASE WHEN [dtValidTo] IS NOT NULL AND ISNULL(bitNeverExpires,0) = 0 THEN DATEADD(MINUTE,-@ClientTimeZoneOffset,dtValidTo) ELSE dtValidTo END) dtValidTo
			,[bitNeverExpires]
			,[tintOfferTriggerValueType]
			,[fltOfferTriggerValue]
			,[tintOfferBasedOn]
			,[fltDiscountValue]
			,[tintDiscountType]
			,[tintDiscoutBaseOn]
			,[numCreatedBy]
			,[dtCreated]
			,[numModifiedBy]
			,[dtModified]
			,[tintCustomersBasedOn]
			,[tintOfferTriggerValueTypeRange]
			,[fltOfferTriggerValueRange]
			,[IsOrderBasedPromotion]
			,[vcShortDesc]
			,[vcLongDesc]
			,[tintItemCalDiscount]
        FROM 
			PromotionOffer
        WHERE 
			numProId = @numProId AND ISNULL(IsOrderBasedPromotion,0) = 0
    END  
	ELSE IF @byteMode = 1
    BEGIN  
	   
        SELECT  
			numProId,
            vcProName,
			(CASE WHEN ISNULL(bitNeverExpires,0) = 1 THEN 'Promotion Never Expires' ELSE CONCAT(dbo.FormatedDateFromDate((CASE WHEN [dtValidFrom] IS NOT NULL THEN DATEADD(MINUTE,-@ClientTimeZoneOffset,dtValidFrom) ELSE dtValidFrom END),@numDomainID),' to ',dbo.FormatedDateFromDate((CASE WHEN [dtValidTo] IS NOT NULL THEN DATEADD(MINUTE,-@ClientTimeZoneOffset,dtValidTo) ELSE dtValidTo END),@numDomainID)) END) AS vcDateValidation,
			(CASE WHEN ISNULL(bitRequireCouponCode,0) = 1 THEN
			              (CASE WHEN ISNULL(IsOrderBasedPromotion,0) = 1 THEN
								 STUFF((SELECT CONCAT (', ' , vcDiscountCode , ' (', ISNULL(SUM(DCU.intCodeUsed),0),')')
									   FROM DiscountCodes  D
									   LEFT JOIN DiscountCodeUsage DCU ON D.numDiscountId = DCU.numDiscountId
									   WHERE D.numPromotionID = PO.numProId GROUP BY D.vcDiscountCode, DCU.numDiscountId
									   FOR XML PATH ('')), 1, 1, '')   
						      ELSE CONCAT(txtCouponCode,' (', ISNULL(intCouponCodeUsed,0),')') END)
						  
			ELSE '' END) AS vcCouponCode			
			
			,(CASE WHEN ISNULL(bitApplyToInternalOrders,0) = 1 THEN 'Yes' ELSE 'No' END) AS vcInternalOrders
			,(CASE WHEN ISNULL(bitAppliesToSite,0)=1 THEN (SELECT Stuff((SELECT CONCAT(', ',Sites.vcSiteName) FROM PromotionOfferSites POS INNER JOIN Sites ON pos.numSiteID=Sites.numSiteID WHERE POS.numPromotionID=PO.numProId FOR XML PATH('')), 1, 2, '')) ELSE '' END) AS vcSites

			,CASE WHEN ISNULL(IsOrderBasedPromotion,0) = 1 THEN
						CONCAT('Buy ', 
							CASE tintDiscountType WHEN 1 THEN
										STUFF((SELECT CONCAT (', ' , '$', numOrderAmount , ' & get ', ISNULL(fltDiscountValue,0),' % off')
										   FROM PromotionOfferOrder POO					   
										   WHERE POO.numPromotionID =  PO.numProId
										   FOR XML PATH ('')), 1, 1, '')  
									WHEN 2 THEN 
										STUFF((SELECT CONCAT (', ','$', numOrderAmount , ' & get $', ISNULL(fltDiscountValue,0),' off')
												   FROM PromotionOfferOrder POO					   
												   WHERE POO.numPromotionID =  PO.numProId
												   FOR XML PATH ('')), 1, 1, '')
							ELSE '' END)
			ELSE
			CONCAT('Buy '
					,CASE WHEN tintOfferTriggerValueType=1 THEN CAST(fltOfferTriggerValue AS VARCHAR) ELSE CONCAT('$',fltOfferTriggerValue) END
					,CASE tintOfferBasedOn
							WHEN 1 THEN CONCAT(' of "',(SELECT Stuff((SELECT CONCAT(', ',Item.vcItemName) FROM PromotionOfferItems INNER JOIN Item ON PromotionOfferItems.numValue = Item.numItemCode WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=1 FOR XML PATH('')), 1, 2, '')), '"')
							WHEN 2 THEN CONCAT(' of items belonging to classification(s) "',(SELECT Stuff((SELECT CONCAT(', ',ListDetails.vcData) FROM PromotionOfferItems INNER JOIN ListDetails ON PromotionOfferItems.numValue = ListDetails.numListItemID WHERE numListID=36 AND numProId=PO.numProId AND tintRecordType=5 AND tintType=2 FOR XML PATH('')), 1, 2, '')), '"')
						END
					,' & get '
					, CASE  
						WHEN (tintDiscountType = 1 AND tintDiscoutBaseOn <> 4) THEN 
							CONCAT(fltDiscountValue,'% off on') 
						WHEN (tintDiscountType = 2 AND tintDiscoutBaseOn <> 4) THEN 
							CONCAT('$',fltDiscountValue, ' off on')
						WHEN (tintDiscountType = 3 AND tintDiscoutBaseOn <> 4) THEN 
						CONCAT(fltDiscountValue,' of') END
					, CASE 
						WHEN tintDiscoutBaseOn = 1 THEN 
							CONCAT
							(
								CONCAT(' "',(SELECT Stuff((SELECT CONCAT(', ',Item.vcItemName) FROM PromotionOfferItems INNER JOIN Item ON PromotionOfferItems.numValue = Item.numItemCode WHERE numProId=PO.numProId AND tintRecordType=6 AND tintType=1 FOR XML PATH('')), 1, 2, '')), '"')
								,(CASE WHEN PO.tintDiscountType = 3 THEN ' free' ELSE '' END)
								,'.'
							)
						WHEN tintDiscoutBaseOn = 2 THEN 
							CONCAT
							(
								CONCAT(' items belonging to classification(s) "',(SELECT Stuff((SELECT CONCAT(', ',ListDetails.vcData) FROM PromotionOfferItems INNER JOIN ListDetails ON PromotionOfferItems.numValue = ListDetails.numListItemID WHERE numListID=36 AND numProId=PO.numProId AND tintRecordType=6 AND tintType=2 FOR XML PATH('')), 1, 2, '')), '"')
								,(CASE WHEN PO.tintDiscountType = 3 THEN ' free' ELSE '' END)
								,'.'
							)
						WHEN tintDiscoutBaseOn = 3 THEN ' related items.'
						WHEN tintDiscoutBaseOn = 4 THEN CONCAT(fltDiscountValue,' quantity free for any item of equal or lesser value')
						ELSE '' 
					END)
				 END AS vcPromotionRule,
				(CASE WHEN (SELECT COUNT(*) FROM OpportunityItems JOIN OpportunityMaster ON OpportunityItems.numOppId=OpportunityMaster.numOppId WHERE numDomainId=@numDomainID AND OpportunityItems.numPromotionID=PO.numProId) > 0 THEN 0 ELSE 1 END) AS bitCanEdit
				,bitEnabled, tintCustomersBasedOn, tintOfferTriggerValueTypeRange, fltOfferTriggerValueRange, [IsOrderBasedPromotion]
        FROM  
			PromotionOffer PO
		WHERE 
			numDomainID = @numDomainID
			
		ORDER BY
			dtCreated DESC
    END
	  
   	
END      
  
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_insertupdatepromotionofferfororder')
DROP PROCEDURE usp_insertupdatepromotionofferfororder
GO
CREATE PROCEDURE [dbo].[USP_InsertUpdatePromotionOfferForOrder]
    @numProId AS NUMERIC(9) = 0,
    @vcProName AS VARCHAR(100),
    @numDomainId AS NUMERIC(18,0),
    @dtValidFrom AS DATETIME,
    @dtValidTo AS DATETIME,
	@bitNeverExpires BIT,
	@bitApplyToInternalOrders BIT,
	@bitAppliesToSite BIT,
	@bitRequireCouponCode BIT,
	@txtCouponCode VARCHAR(MAX),
	@tintUsageLimit TINYINT,
	@numUserCntID NUMERIC(18,0),
	@tintCustomersBasedOn TINYINT,
	@strOrderAmount VARCHAR(MAX),
	@strfltDiscount VARCHAR(MAX),
	@tintDiscountType TINYINT
AS 
BEGIN

	IF @numProId = 0 
	BEGIN
		INSERT INTO PromotionOffer
        (
            vcProName
			,numDomainId
			,numCreatedBy
			,dtCreated
			,bitEnabled
			,IsOrderBasedPromotion 
        )
        VALUES  
		(
			@vcProName
           ,@numDomainId
           ,@numUserCntID
		   ,GETUTCDATE()
		   ,1
		   ,1
        )
            
		SELECT SCOPE_IDENTITY()
	END
	ELSE
	BEGIN	
		IF ISNULL(@bitRequireCouponCode,0) = 1 AND (SELECT 
														COUNT(*) 
													FROM 
														PromotionOffer POInner 
													INNER JOIN 
														DiscountCodes DCInner 
													ON 
														POInner.numProId=DCInner.numPromotionID 
													WHERE 
														POInner.numDomainId=@numDomainId 
														AND ISNULL(POInner.bitEnabled,0) = 1
														AND DCInner.numPromotionID <> @numProId 
														AND DCInner.vcDiscountCode IN (SELECT OutParam FROM dbo.SplitString(@txtCouponCode,','))) > 0
		BEGIN
			RAISERROR ( 'DUPLICATE_COUPON_CODE',16, 1 )
 			RETURN;
		END 
		
		IF @tintCustomersBasedOn = 1
		BEGIN
			IF ISNULL(@tintCustomersBasedOn,0) = 1 AND (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId = @numProId AND tintType = 1) = 0
			BEGIN
				UPDATE PromotionOffer SET bitEnabled=0 WHERE numProId=@numProId
				RAISERROR ( 'SELECT_INDIVIDUAL_CUSTOMER',16, 1 )
 				RETURN;
			END
		END
			
			/* 2- The customer is targeted at the group level (level 2), and the item is specific (level 1). If the user tries to create another rule that targets this same Relationship / Profile combination and that same item in level 1 (L2 customer and L1 item both match), validation would say �You�ve already created a promotion rule that targets this relationship / profile and one or more item specifically�*/
		ELSE IF @tintCustomersBasedOn = 2
		BEGIN
			IF ISNULL(@tintCustomersBasedOn,0) = 2 AND (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId = @numProId AND tintType = 2) = 0
			BEGIN
				UPDATE PromotionOffer SET bitEnabled=0 WHERE numProId=@numProId
				RAISERROR ( 'SELECT_RELATIONSHIP_PROFILE',16, 1 )
 				RETURN;
			END
		END

		ELSE IF @tintCustomersBasedOn = 0
		BEGIN
			UPDATE PromotionOffer SET bitEnabled=0 WHERE numProId=@numProId
			RAISERROR ( 'SELECT_CUSTOMERS',16, 1 )
 			RETURN;
		END

		IF @tintCustomersBasedOn = 3
		BEGIN
			DELETE FROM PromotionOfferorganizations 
				WHERE tintType IN (1, 2) 
					AND numProId = @numProId
		END

		IF (
			SELECT
				COUNT(*)
			FROM
				PromotionOffer
			LEFT JOIN
				PromotionOfferOrganizations
			ON
				PromotionOffer.numProId = PromotionOfferOrganizations.numProId
				AND 1 = (CASE 
							WHEN @tintCustomersBasedOn=1 THEN (CASE WHEN PromotionOfferOrganizations.tintType=1 THEN 1 ELSE 0 END) 
							WHEN @tintCustomersBasedOn=2 THEN (CASE WHEN PromotionOfferOrganizations.tintType=2 THEN 1 ELSE 0 END) 
							ELSE 1
						END)
			WHERE
				PromotionOffer.numDomainId=@numDomainID
				AND PromotionOffer.numProId <> @numProId
				AND tintCustomersBasedOn = @tintCustomersBasedOn 
				AND ISNULL(bitEnabled,0) = 1			 
				AND 1 = (CASE 							
							WHEN @tintCustomersBasedOn=2 AND IsOrderBasedPromotion = 1 THEN
								(CASE 
									WHEN 
										(SELECT COUNT(*) FROM PromotionOfferOrganizations POOInner WHERE POOInner.numProId=@numProId AND POOInner.tintType=2 AND POOInner.numRelationship=PromotionOfferOrganizations.numRelationship AND POOInner.numProfile=PromotionOfferOrganizations.numProfile) > 0 AND ISNULL(@bitRequireCouponCode,0) = 0
									THEN 
										1 
									ELSE 
										0 
								END)
							
						END)
			) > 0
		BEGIN
			UPDATE PromotionOffer SET bitEnabled=0 WHERE numProId=@numProId
			RAISERROR('RELATIONSHIPPROFILE ALREADY EXISTS',16,1)
			RETURN
		END

		IF ISNULL(@bitAppliesToSite,0) = 0
		BEGIN
			DELETE FROM PromotionOfferSites WHERE numPromotionID=@numProId
		END

		UPDATE 
			PromotionOffer
		SET 
			vcProName = @vcProName
			,numDomainId = @numDomainId
			,dtValidFrom = @dtValidFrom
			,dtValidTo = @dtValidTo
			,bitNeverExpires = @bitNeverExpires
			,bitApplyToInternalOrders = @bitApplyToInternalOrders
			,bitAppliesToSite = @bitAppliesToSite
			,bitRequireCouponCode = @bitRequireCouponCode			
			,numModifiedBy = @numUserCntID
			,dtModified = GETUTCDATE()
			,tintCustomersBasedOn = @tintCustomersBasedOn
			,tintDiscountType = @tintDiscountType
		WHERE 
			numProId=@numProId

		IF (SELECT COUNT(*) FROM PromotionOfferOrder WHERE numPromotionID = @numProId AND numDomainId = @numDomainId) > 0
		BEGIN
			DELETE FROM PromotionOfferOrder WHERE numPromotionID = @numProId AND numDomainId = @numDomainId
		END

		DECLARE @Tempdata TABLE
		(
			numPromotionID NUMERIC,
			numDomainID NUMERIC,
			numOrderAmount VARCHAR(MAX),
			fltDiscountValue VARCHAR(MAX)
		)

		INSERT @Tempdata SELECT @numProId, @numDomainID, @strOrderAmount, @strfltDiscount

		;WITH tmp(numPromotionID, numDomainID, numOrderAmount, OrderAmount, numfltDiscountValue,fltDiscountValue) AS
		(
			SELECT
				numPromotionID,
				numDomainID,
				LEFT(numOrderAmount, CHARINDEX(',', numOrderAmount + ',') - 1),
				STUFF(numOrderAmount, 1, CHARINDEX(',', numOrderAmount + ','), ''),
				LEFT(fltDiscountValue, CHARINDEX(',', fltDiscountValue + ',') - 1),
				STUFF(fltDiscountValue, 1, CHARINDEX(',', fltDiscountValue + ','), '')
			FROM @Tempdata
			UNION all

			SELECT
				numPromotionID,
				numDomainID,
				LEFT(OrderAmount, CHARINDEX(',', OrderAmount + ',') - 1),
				STUFF(OrderAmount, 1, CHARINDEX(',', OrderAmount + ','), ''),
				LEFT(fltDiscountValue, CHARINDEX(',', fltDiscountValue + ',') - 1),
				STUFF(fltDiscountValue, 1, CHARINDEX(',', fltDiscountValue + ','), '')
			FROM tmp
			WHERE
				OrderAmount > '' AND fltDiscountValue > ''
		)

		INSERT INTO PromotionOfferOrder
		SELECT
			numPromotionID,				
			numOrderAmount,
			numfltDiscountValue,
			numDomainID

		FROM tmp
				
		IF @bitRequireCouponCode = 1
		BEGIN
			DECLARE @DiscountTempdata TABLE
			(
				numPromotionID NUMERIC,
				CodeUsageLimit TINYINT,
				vcDiscountCode VARCHAR(MAX)			
			)

			INSERT INTO @DiscountTempdata
			(
				numPromotionID,
				CodeUsageLimit,
				vcDiscountCode
			)
			SELECT 
				@numProId
				,@tintUsageLimit
				,OutParam 
			FROM 
				dbo.SplitString(@txtCouponCode,',')
			 
			INSERT INTO DiscountCodes
			(
				numPromotionID
				,vcDiscountCode
				,CodeUsageLimit
			)
			SELECT
				tmp1.numPromotionID,				
				tmp1.vcDiscountCode,
				tmp1.CodeUsageLimit
			FROM 
				@DiscountTempdata tmp1
			WHERE 
				tmp1.vcDiscountCode NOT IN ( SELECT vcDiscountCode FROM DiscountCodes DC INNER JOIN DiscountCodeUsage DCU ON DC.numDiscountId = DCU.numDiscountId)

			UPDATE DiscountCodes SET CodeUsageLimit = @tintUsageLimit WHERE numPromotionID = @numProId
		END	

        SELECT  @numProId
	END
END


GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_Item_GetPriceBasedOnPricingMethod')
DROP PROCEDURE USP_Item_GetPriceBasedOnPricingMethod
GO
CREATE PROCEDURE [dbo].[USP_Item_GetPriceBasedOnPricingMethod]
	@numDomainID NUMERIC(18,0)
	,@numDivisionID NUMERIC(18,0)
	,@numItemCode NUMERIC(18,0)
	,@numWarehouseItemID NUMERIC(18,0) 
	,@numQty FLOAT
	,@vcChildKits VARCHAR(MAX)
AS
BEGIN
	DECLARE @monPrice DECIMAL(20,5)
	SELECT @monPrice=ISNULL(monListPrice,0) FROM Item WHERE numItemCode=@numItemCode

	IF EXISTS (SELECT 
					numItemCode 
				FROM 
					Item 
				WHERE 
					numDomainID=@numDomainID 
					AND numItemCode=@numItemCode 
					AND ISNULL(bitCalAmtBasedonDepItems,0)=1 
					AND (ISNULL(bitKitParent,0) = 1 OR ISNULL(bitAssembly,0)=1))
	BEGIN
		SET @monPrice = dbo.GetCalculatedPriceForKitAssembly(@numDomainID,@numItemCode,@numWarehouseItemID,1,0,0,'')
	END
	ELSE
	BEGIN
		SELECT
			@monPrice=monFinalPrice
		FROM
			dbo.fn_GetItemPriceBasedOnMethod(@numDomainID,@numDivisionID,@numItemCode,@numQty)
	END

	SELECT @monPrice AS monPrice
END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManagePromotionOffer')
DROP PROCEDURE USP_ManagePromotionOffer
GO
CREATE PROCEDURE [dbo].[USP_ManagePromotionOffer]
    @numProId AS NUMERIC(9) = 0,
    @vcProName AS VARCHAR(100),
    @numDomainId AS NUMERIC(18,0),
    @dtValidFrom AS DATE,
    @dtValidTo AS DATE,
	@bitNeverExpires BIT,
	@tintOfferTriggerValueType TINYINT,
	@fltOfferTriggerValue INT,
	@tintOfferBasedOn TINYINT,
	@tintDiscountType TINYINT,
	@fltDiscountValue FLOAT,
	@tintDiscoutBaseOn TINYINT,
	@numUserCntID NUMERIC(18,0),
	@tintCustomersBasedOn TINYINT,
	@tintOfferTriggerValueTypeRange TINYINT,
	@fltOfferTriggerValueRange FLOAT,
	@vcShortDesc VARCHAR(100),
	@vcLongDesc VARCHAR(500),
	@tintItemCalDiscount TINYINT
AS 
BEGIN
	IF @numProId = 0 
	BEGIN
		INSERT INTO PromotionOffer
        (
            vcProName
			,numDomainId
			, numCreatedBy
			,dtCreated
			,bitEnabled
			,IsOrderBasedPromotion
        )
        VALUES  
		(
			@vcProName
           ,@numDomainId
           ,@numUserCntID
		   ,GETUTCDATE()
		   ,1
		   ,0
        )
            
		SELECT SCOPE_IDENTITY()
	END
	ELSE
	BEGIN
		IF @tintOfferBasedOn <> 4
		BEGIN
			IF (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=@numProId AND tintType=ISNULL(@tintOfferBasedOn,0) AND tintRecordType=5) = 0
			BEGIN
				RAISERROR ( 'SELECT_ITEMS_FOR_PROMOTIONS',16, 1 )
 				RETURN;
			END
		END

		IF (ISNULL(@tintDiscoutBaseOn,0) <> 3 AND ISNULL(@tintDiscoutBaseOn,0) <> 4) AND (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=@numProId AND tintType=ISNULL(@tintDiscoutBaseOn,0) AND tintRecordType=6) = 0
		BEGIN
			RAISERROR ( 'SELECT_DISCOUNTED_ITEMS',16, 1 )
 			RETURN;
		END

		/*Check For Duplicate Customer-Item relationship 
			1- The customer is specific (level 1), and the item is specific (also level 1). If the user tries to create another rule that targets the customer by name, or that same item by name, validation would say �You�ve already created a promotion rule that targets this customer and item by name�*/
		IF @tintCustomersBasedOn = 1
		BEGIN
			IF ISNULL(@tintCustomersBasedOn,0) = 1 AND (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId = @numProId AND tintType = 1) = 0
			BEGIN
				RAISERROR ( 'SELECT_INDIVIDUAL_CUSTOMER',16, 1 )
 				RETURN;
			END
		END
			
			/* 2- The customer is targeted at the group level (level 2), and the item is specific (level 1). If the user tries to create another rule that targets this same Relationship / Profile combination and that same item in level 1 (L2 customer and L1 item both match), validation would say �You�ve already created a promotion rule that targets this relationship / profile and one or more item specifically�*/
		ELSE IF @tintCustomersBasedOn = 2
		BEGIN
			IF ISNULL(@tintCustomersBasedOn,0) = 2 AND (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId = @numProId AND tintType = 2) = 0
			BEGIN
				RAISERROR ( 'SELECT_RELATIONSHIP_PROFILE',16, 1 )
 				RETURN;
			END
		END

		ELSE IF @tintCustomersBasedOn = 0
		BEGIN
			RAISERROR ( 'SELECT_CUSTOMERS',16, 1 )
 			RETURN;
		END

		IF @tintCustomersBasedOn = 3
		BEGIN
			DELETE FROM PromotionOfferorganizations 
				WHERE tintType IN (1, 2) 
					AND numProId = @numProId
		END


		IF (
			SELECT
				COUNT(*)
			FROM
				PromotionOffer
			LEFT JOIN
				PromotionOfferOrganizations
			ON
				PromotionOffer.numProId = PromotionOfferOrganizations.numProId
				AND 1 = (CASE 
							WHEN @tintCustomersBasedOn=1 THEN (CASE WHEN PromotionOfferOrganizations.tintType=1 THEN 1 ELSE 0 END) 
							WHEN @tintCustomersBasedOn=2 THEN (CASE WHEN PromotionOfferOrganizations.tintType=2 THEN 1 ELSE 0 END) 
							ELSE 1
						END)
			LEFT JOIN
				PromotionOfferItems
			ON
				PromotionOffer.numProId = PromotionOfferItems.numProId
				AND 1 = (CASE 
							WHEN @tintOfferBasedOn=1 THEN (CASE WHEN PromotionOfferItems.tintType=1 THEN 1 ELSE 0 END) 
							WHEN @tintOfferBasedOn=2 THEN (CASE WHEN PromotionOfferItems.tintType=2 THEN 1 ELSE 0 END) 
							ELSE 1
						END)
				AND PromotionOfferItems.tintRecordType = 5
			LEFT JOIN
				PromotionOfferItems PromotionOfferItemsDiscount
			ON
				PromotionOffer.numProId = PromotionOfferItemsDiscount.numProId
				AND 1 = (CASE 
							WHEN @tintDiscoutBaseOn=1 THEN (CASE WHEN PromotionOfferItemsDiscount.tintType=1 THEN 1 ELSE 0 END) 
							WHEN @tintDiscoutBaseOn=2 THEN (CASE WHEN PromotionOfferItemsDiscount.tintType=2 THEN 1 ELSE 0 END) 
							ELSE 1
						END)
				AND PromotionOfferItemsDiscount.tintRecordType = 6
			WHERE
				PromotionOffer.numDomainId=@numDomainID
				AND PromotionOffer.numProId <> @numProId
				AND tintCustomersBasedOn = @tintCustomersBasedOn
				AND tintDiscoutBaseOn = @tintDiscoutBaseOn
				AND tintOfferBasedOn = @tintOfferBasedOn
				AND tintOfferTriggerValueType = @tintOfferTriggerValueType
				AND 1 = (CASE 
							WHEN tintOfferTriggerValueTypeRange = 1 --FIXED
							THEN
								(CASE 
									WHEN @tintOfferTriggerValueTypeRange = 1 THEN 
										(CASE WHEN fltOfferTriggerValue = @fltOfferTriggerValue THEN 1 ELSE 0 END)
									WHEN @tintOfferTriggerValueTypeRange = 2 THEN 
										(CASE WHEN fltOfferTriggerValue BETWEEN @fltOfferTriggerValue AND @fltOfferTriggerValueRange THEN 1 ELSE 0 END)
									ELSE 0 
								END)
							WHEN tintOfferTriggerValueTypeRange = 2 --BETWEEN
							THEN
								(CASE 
									WHEN @tintOfferTriggerValueTypeRange = 1 THEN 
										(CASE WHEN @fltOfferTriggerValue BETWEEN fltOfferTriggerValue AND fltOfferTriggerValueRange THEN 1 ELSE 0 END)
									WHEN @tintOfferTriggerValueTypeRange = 2 THEN 
										(CASE 
											WHEN (@fltOfferTriggerValue BETWEEN fltOfferTriggerValue AND fltOfferTriggerValueRange) OR (@fltOfferTriggerValueRange BETWEEN fltOfferTriggerValue AND fltOfferTriggerValueRange) THEN 1 ELSE 0 END)
									ELSE 0 
								END)
							ELSE 0
						END)
				AND 1 = (CASE 
							WHEN @tintCustomersBasedOn=1 AND @tintOfferBasedOn=1 AND @tintDiscoutBaseOn=1 THEN
								(CASE 
									WHEN 
										(SELECT COUNT(*) FROM PromotionOfferOrganizations POOInner WHERE POOInner.numProId=@numProId AND POOInner.tintType=1 AND POOInner.numDivisionID=PromotionOfferOrganizations.numDivisionID) > 0 
										AND (SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=5 AND POIInner.tintType=1 AND POIInner.numValue=PromotionOfferItems.numValue) > 0 
										AND (SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=6 AND POIInner.tintType=1 AND POIInner.numValue=PromotionOfferItemsDiscount.numValue) > 0 
									THEN 
										1 
									ELSE 
										0 
								END)
							WHEN @tintCustomersBasedOn=1 AND @tintOfferBasedOn=1 AND @tintDiscoutBaseOn=2 THEN
								(CASE 
									WHEN 
										(SELECT COUNT(*) FROM PromotionOfferOrganizations POOInner WHERE POOInner.numProId=@numProId AND POOInner.tintType=1 AND POOInner.numDivisionID=PromotionOfferOrganizations.numDivisionID) > 0 
										AND (SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=5 AND POIInner.tintType=1 AND POIInner.numValue=PromotionOfferItems.numValue) > 0 
										AND (SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=6 AND POIInner.tintType=2 AND POIInner.numValue=PromotionOfferItemsDiscount.numValue) > 0 
									THEN 
										1 
									ELSE 
										0 
								END)
							WHEN @tintCustomersBasedOn=1 AND @tintOfferBasedOn=1 AND (@tintDiscoutBaseOn=3 OR @tintDiscoutBaseOn=4)  THEN
								(CASE 
									WHEN 
										(SELECT COUNT(*) FROM PromotionOfferOrganizations POOInner WHERE POOInner.numProId=@numProId AND POOInner.tintType=1 AND POOInner.numDivisionID=PromotionOfferOrganizations.numDivisionID) > 0 
										AND (SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=5 AND POIInner.tintType=1 AND POIInner.numValue=PromotionOfferItems.numValue) > 0 
									THEN 
										1 
									ELSE 
										0 
								END)
							WHEN @tintCustomersBasedOn=1 AND @tintOfferBasedOn=2  AND @tintDiscoutBaseOn=1 THEN
								(CASE 
									WHEN 
										(SELECT COUNT(*) FROM PromotionOfferOrganizations POOInner WHERE POOInner.numProId=@numProId AND POOInner.tintType=1 AND POOInner.numDivisionID=PromotionOfferOrganizations.numDivisionID) > 0 
										AND (SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=5 AND POIInner.tintType=2 AND POIInner.numValue=PromotionOfferItems.numValue) > 0 
										AND (SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=6 AND POIInner.tintType=1 AND POIInner.numValue=PromotionOfferItemsDiscount.numValue) > 0  
									THEN 
										1 
									ELSE 
										0 
								END)
							WHEN @tintCustomersBasedOn=1 AND @tintOfferBasedOn=2  AND @tintDiscoutBaseOn=2 THEN
								(CASE 
									WHEN 
										(SELECT COUNT(*) FROM PromotionOfferOrganizations POOInner WHERE POOInner.numProId=@numProId AND POOInner.tintType=1 AND POOInner.numDivisionID=PromotionOfferOrganizations.numDivisionID) > 0 
										AND (SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=5 AND POIInner.tintType=2 AND POIInner.numValue=PromotionOfferItems.numValue) > 0 
										AND (SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=6 AND POIInner.tintType=2 AND POIInner.numValue=PromotionOfferItemsDiscount.numValue) > 0  
									THEN 
										1 
									ELSE 
										0 
								END)
							WHEN @tintCustomersBasedOn=1 AND @tintOfferBasedOn=2 AND (@tintDiscoutBaseOn=3 OR @tintDiscoutBaseOn=4) THEN
								(CASE 
									WHEN 
										(SELECT COUNT(*) FROM PromotionOfferOrganizations POOInner WHERE POOInner.numProId=@numProId AND POOInner.tintType=1 AND POOInner.numDivisionID=PromotionOfferOrganizations.numDivisionID) > 0 
										AND (SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=5 AND POIInner.tintType=2 AND POIInner.numValue=PromotionOfferItems.numValue) > 0 
									THEN 
										1 
									ELSE 
										0 
								END)
							WHEN @tintCustomersBasedOn=1 AND @tintOfferBasedOn=4 AND @tintDiscoutBaseOn=1 THEN
								(CASE 
									WHEN 
										(SELECT COUNT(*) FROM PromotionOfferOrganizations POOInner WHERE POOInner.numProId=@numProId AND POOInner.tintType=1 AND POOInner.numDivisionID=PromotionOfferOrganizations.numDivisionID) > 0
										AND (SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=6 AND POIInner.tintType=1 AND POIInner.numValue=PromotionOfferItemsDiscount.numValue) > 0
									THEN 
										1 
									ELSE 
										0 
								END) 
							WHEN @tintCustomersBasedOn=1 AND @tintOfferBasedOn=4 AND @tintDiscoutBaseOn=2 THEN
								(CASE 
									WHEN 
										(SELECT COUNT(*) FROM PromotionOfferOrganizations POOInner WHERE POOInner.numProId=@numProId AND POOInner.tintType=1 AND POOInner.numDivisionID=PromotionOfferOrganizations.numDivisionID) > 0
										AND (SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=6 AND POIInner.tintType=2 AND POIInner.numValue=PromotionOfferItemsDiscount.numValue) > 0
									THEN 
										1 
									ELSE 
										0 
								END) 
							WHEN @tintCustomersBasedOn=1 AND @tintOfferBasedOn=4 AND (@tintDiscoutBaseOn=3 OR @tintDiscoutBaseOn=4)  THEN
								(CASE 
									WHEN 
										(SELECT COUNT(*) FROM PromotionOfferOrganizations POOInner WHERE POOInner.numProId=@numProId AND POOInner.tintType=1 AND POOInner.numDivisionID=PromotionOfferOrganizations.numDivisionID) > 0 
									THEN 
										1 
									ELSE 
										0 
								END) 
							WHEN @tintCustomersBasedOn=2 AND @tintOfferBasedOn=1 AND @tintDiscoutBaseOn=1 THEN
								(CASE 
									WHEN 
										(SELECT COUNT(*) FROM PromotionOfferOrganizations POOInner WHERE POOInner.numProId=@numProId AND POOInner.tintType=2 AND POOInner.numRelationship=PromotionOfferOrganizations.numRelationship AND POOInner.numProfile=PromotionOfferOrganizations.numProfile) > 0 
										AND (SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=5 AND POIInner.tintType=1 AND POIInner.numValue=PromotionOfferItems.numValue) > 0 
										AND (SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=6 AND POIInner.tintType=1 AND POIInner.numValue=PromotionOfferItemsDiscount.numValue) > 0
									THEN 
										1 
									ELSE 
										0 
								END)
							WHEN @tintCustomersBasedOn=2 AND @tintOfferBasedOn=1 AND @tintDiscoutBaseOn=2 THEN
								(CASE 
									WHEN 
										(SELECT COUNT(*) FROM PromotionOfferOrganizations POOInner WHERE POOInner.numProId=@numProId AND POOInner.tintType=2 AND POOInner.numRelationship=PromotionOfferOrganizations.numRelationship AND POOInner.numProfile=PromotionOfferOrganizations.numProfile) > 0 
										AND (SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=5 AND POIInner.tintType=1 AND POIInner.numValue=PromotionOfferItems.numValue) > 0 
										AND (SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=6 AND POIInner.tintType=2 AND POIInner.numValue=PromotionOfferItemsDiscount.numValue) > 0
									THEN 
										1 
									ELSE 
										0 
								END)
							WHEN @tintCustomersBasedOn=2 AND @tintOfferBasedOn=1 AND (@tintDiscoutBaseOn=3 OR @tintDiscoutBaseOn=4)  THEN
								(CASE 
									WHEN 
										(SELECT COUNT(*) FROM PromotionOfferOrganizations POOInner WHERE POOInner.numProId=@numProId AND POOInner.tintType=2 AND POOInner.numRelationship=PromotionOfferOrganizations.numRelationship AND POOInner.numProfile=PromotionOfferOrganizations.numProfile) > 0 
										AND (SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=5 AND POIInner.tintType=1 AND POIInner.numValue=PromotionOfferItems.numValue) > 0 
									THEN 
										1 
									ELSE 
										0 
								END)
							WHEN @tintCustomersBasedOn=2 AND @tintOfferBasedOn=2 AND @tintDiscoutBaseOn=1 THEN
								(CASE 
									WHEN 
										(SELECT COUNT(*) FROM PromotionOfferOrganizations POOInner WHERE POOInner.numProId=@numProId AND POOInner.tintType=2 AND POOInner.numRelationship=PromotionOfferOrganizations.numRelationship AND POOInner.numProfile=PromotionOfferOrganizations.numProfile) > 0 
										AND (SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=5 AND POIInner.tintType=2 AND POIInner.numValue=PromotionOfferItems.numValue) > 0
										AND (SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=6 AND POIInner.tintType=1 AND POIInner.numValue=PromotionOfferItemsDiscount.numValue) > 0
									THEN 
										1 
									ELSE 
										0 
								END)
							WHEN @tintCustomersBasedOn=2 AND @tintOfferBasedOn=2 AND @tintDiscoutBaseOn=2 THEN
								(CASE 
									WHEN 
										(SELECT COUNT(*) FROM PromotionOfferOrganizations POOInner WHERE POOInner.numProId=@numProId AND POOInner.tintType=2 AND POOInner.numRelationship=PromotionOfferOrganizations.numRelationship AND POOInner.numProfile=PromotionOfferOrganizations.numProfile) > 0 
										AND (SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=5 AND POIInner.tintType=2 AND POIInner.numValue=PromotionOfferItems.numValue) > 0 
										AND (SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=6 AND POIInner.tintType=2 AND POIInner.numValue=PromotionOfferItemsDiscount.numValue) > 0
									THEN 
										1 
									ELSE 
										0 
								END)
							WHEN @tintCustomersBasedOn=2 AND @tintOfferBasedOn=2 AND (@tintDiscoutBaseOn=3 OR @tintDiscoutBaseOn=4)  THEN
								(CASE 
									WHEN 
										(SELECT COUNT(*) FROM PromotionOfferOrganizations POOInner WHERE POOInner.numProId=@numProId AND POOInner.tintType=2 AND POOInner.numRelationship=PromotionOfferOrganizations.numRelationship AND POOInner.numProfile=PromotionOfferOrganizations.numProfile) > 0 
										AND (SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=5 AND POIInner.tintType=2 AND POIInner.numValue=PromotionOfferItems.numValue) > 0 
									THEN 
										1 
									ELSE 
										0 
								END)
							WHEN @tintCustomersBasedOn=2 AND @tintOfferBasedOn=4 AND @tintDiscoutBaseOn=1 THEN
								(CASE 
									WHEN 
										(SELECT COUNT(*) FROM PromotionOfferOrganizations POOInner WHERE POOInner.numProId=@numProId AND POOInner.tintType=2 AND POOInner.numRelationship=PromotionOfferOrganizations.numRelationship AND POOInner.numProfile=PromotionOfferOrganizations.numProfile) > 0 
										AND (SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=6 AND POIInner.tintType=1 AND POIInner.numValue=PromotionOfferItemsDiscount.numValue) > 0
									THEN 
										1 
									ELSE 
										0 
								END)
							WHEN @tintCustomersBasedOn=2 AND @tintOfferBasedOn=4 AND @tintDiscoutBaseOn=2 THEN
								(CASE 
									WHEN 
										(SELECT COUNT(*) FROM PromotionOfferOrganizations POOInner WHERE POOInner.numProId=@numProId AND POOInner.tintType=2 AND POOInner.numRelationship=PromotionOfferOrganizations.numRelationship AND POOInner.numProfile=PromotionOfferOrganizations.numProfile) > 0
										AND (SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=6 AND POIInner.tintType=2 AND POIInner.numValue=PromotionOfferItemsDiscount.numValue) > 0
									THEN 
										1 
									ELSE 
										0 
								END)
							WHEN @tintCustomersBasedOn=2 AND @tintOfferBasedOn=4 AND (@tintDiscoutBaseOn=3 OR @tintDiscoutBaseOn=4)  THEN
								(CASE 
									WHEN 
										(SELECT COUNT(*) FROM PromotionOfferOrganizations POOInner WHERE POOInner.numProId=@numProId AND POOInner.tintType=2 AND POOInner.numRelationship=PromotionOfferOrganizations.numRelationship AND POOInner.numProfile=PromotionOfferOrganizations.numProfile) > 0 
									THEN 
										1 
									ELSE 
										0 
								END)
							WHEN @tintCustomersBasedOn=3 AND @tintOfferBasedOn=1 AND @tintDiscoutBaseOn=1 THEN
								(CASE 
									WHEN 
										(SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=5 AND POIInner.tintType=1 AND POIInner.numValue=PromotionOfferItems.numValue) > 0
										AND (SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=6 AND POIInner.tintType=1 AND POIInner.numValue=PromotionOfferItemsDiscount.numValue) > 0
									THEN 
										1 
									ELSE 
										0 
								END)
							WHEN @tintCustomersBasedOn=3 AND @tintOfferBasedOn=1 AND @tintDiscoutBaseOn=2 THEN
								(CASE 
									WHEN 
										(SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=5 AND POIInner.tintType=1 AND POIInner.numValue=PromotionOfferItems.numValue) > 0 
										AND (SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=6 AND POIInner.tintType=2 AND POIInner.numValue=PromotionOfferItemsDiscount.numValue) > 0
									THEN 
										1 
									ELSE 
										0 
								END)
							WHEN @tintCustomersBasedOn=3 AND @tintOfferBasedOn=1 AND (@tintDiscoutBaseOn=3 OR @tintDiscoutBaseOn=4)  THEN
								(CASE 
									WHEN 
										(SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=5 AND POIInner.tintType=1 AND POIInner.numValue=PromotionOfferItems.numValue) > 0 
									THEN 
										1 
									ELSE 
										0 
								END)
							WHEN @tintCustomersBasedOn=3 AND @tintOfferBasedOn=2 AND @tintDiscoutBaseOn=1 THEN
								(CASE 
									WHEN 
										(SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=5 AND POIInner.tintType=2 AND POIInner.numValue=PromotionOfferItems.numValue) > 0
										AND (SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=6 AND POIInner.tintType=1 AND POIInner.numValue=PromotionOfferItemsDiscount.numValue) > 0
									THEN 
										1 
									ELSE 
										0 
								END)
							WHEN @tintCustomersBasedOn=3 AND @tintOfferBasedOn=2 AND @tintDiscoutBaseOn=2 THEN
								(CASE 
									WHEN 
										(SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=5 AND POIInner.tintType=2 AND POIInner.numValue=PromotionOfferItems.numValue) > 0 
										AND (SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=6 AND POIInner.tintType=2 AND POIInner.numValue=PromotionOfferItemsDiscount.numValue) > 0
									THEN 
										1 
									ELSE 
										0 
								END)
							WHEN @tintCustomersBasedOn=3 AND @tintOfferBasedOn=2 AND (@tintDiscoutBaseOn=3 OR @tintDiscoutBaseOn=4) THEN
								(CASE 
									WHEN 
										(SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=5 AND POIInner.tintType=2 AND POIInner.numValue=PromotionOfferItems.numValue) > 0 
									THEN 
										1 
									ELSE 
										0 
								END)
							WHEN @tintCustomersBasedOn=3 AND @tintOfferBasedOn=4 AND @tintDiscoutBaseOn=1 THEN
								(CASE 
									WHEN 
										(SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=6 AND POIInner.tintType=1 AND POIInner.numValue=PromotionOfferItemsDiscount.numValue) > 0
									THEN 
										1 
									ELSE 
										0 
								END)
							WHEN @tintCustomersBasedOn=3 AND @tintOfferBasedOn=4 AND @tintDiscoutBaseOn=2 THEN
								(CASE 
									WHEN 
										(SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=6 AND POIInner.tintType=2 AND POIInner.numValue=PromotionOfferItemsDiscount.numValue) > 0
									THEN 
										1 
									ELSE 
										0 
								END)
							WHEN @tintCustomersBasedOn=3 AND @tintOfferBasedOn=4 AND (@tintDiscoutBaseOn=3 OR @tintDiscoutBaseOn=4)  THEN 3
						END)
			) > 0
		BEGIN
			DECLARE @ERRORMESSAGE AS VARCHAR(50)
			SET @ERRORMESSAGE = (CASE 
							WHEN @tintCustomersBasedOn=1 AND @tintOfferBasedOn=1 THEN 'ORGANIZATION-ITEM'
							WHEN @tintCustomersBasedOn=1 AND @tintOfferBasedOn=2 THEN 'ORGANIZATION-ITEMCLASSIFICATIONS'
							WHEN @tintCustomersBasedOn=1 AND @tintOfferBasedOn=4 THEN 'ORGANIZATION-ALLITEMS'
							WHEN @tintCustomersBasedOn=2 AND @tintOfferBasedOn=1 THEN 'RELATIONSHIPPROFILE-ITEMS'
							WHEN @tintCustomersBasedOn=2 AND @tintOfferBasedOn=2 THEN 'RELATIONSHIPPROFILE-ITEMCLASSIFICATIONS'
							WHEN @tintCustomersBasedOn=2 AND @tintOfferBasedOn=4 THEN 'RELATIONSHIPPROFILE-ALLITEMS'
							WHEN @tintCustomersBasedOn=3 AND @tintOfferBasedOn=1 THEN 'ALLORGANIZATIONS-ITEMS'
							WHEN @tintCustomersBasedOn=3 AND @tintOfferBasedOn=2 THEN 'ALLORGANIZATIONS-ITEMCLASSIFICATIONS'
							WHEN @tintCustomersBasedOn=3 AND @tintOfferBasedOn=4 THEN 'ALLORGANIZATIONS-ALLITEMS'
					END)
			RAISERROR(@ERRORMESSAGE,16,1)
			RETURN

		END

		UPDATE 
			PromotionOffer
		SET 
			vcProName = @vcProName
			,numDomainId = @numDomainId
			,dtValidFrom = @dtValidFrom
			,dtValidTo = @dtValidTo
			,bitNeverExpires = @bitNeverExpires
			,tintOfferTriggerValueType = @tintOfferTriggerValueType
			,fltOfferTriggerValue = @fltOfferTriggerValue
			,tintOfferBasedOn = @tintOfferBasedOn
			,tintDiscountType = @tintDiscountType
			,fltDiscountValue = @fltDiscountValue
			,tintDiscoutBaseOn = @tintDiscoutBaseOn
			,numModifiedBy = @numUserCntID
			,dtModified = GETUTCDATE()
			,tintCustomersBasedOn = @tintCustomersBasedOn
			,tintOfferTriggerValueTypeRange = @tintOfferTriggerValueTypeRange
			,fltOfferTriggerValueRange = @fltOfferTriggerValueRange
			,vcShortDesc = @vcShortDesc
			,vcLongDesc = @vcLongDesc
			,tintItemCalDiscount = @tintItemCalDiscount
		WHERE 
			numProId=@numProId
       
        SELECT  @numProId
	END
END

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_oppdetailsdtlpl')
DROP PROCEDURE usp_oppdetailsdtlpl
GO
CREATE PROCEDURE [dbo].[USP_OPPDetailsDTLPL]
(
               @OpportunityId        AS NUMERIC(9)  = NULL,
               @numDomainID          AS NUMERIC(9),
               @ClientTimeZoneOffset AS INT)
AS
  SELECT Opp.numoppid,numCampainID,
		 CAMP.vcCampaignName AS [numCampainIDName],
         dbo.Fn_getcontactname(Opp.numContactId) numContactIdName,
		 Opp.numContactId,
         Opp.txtComments,
         Opp.numDivisionID,
         tintOppType,
         Dateadd(MINUTE,-@ClientTimeZoneOffset,bintAccountClosingDate) AS bintAccountClosingDate,
         dbo.fn_GetOpportunitySourceValue(ISNULL(Opp.tintSource,0),ISNULL(Opp.tintSourceType,0),Opp.numDomainID) AS tintSourceName,
          tintSource,
         Opp.VcPoppName,
         intpEstimatedCloseDate,
         [dbo].[getdealamount](@OpportunityId,Getutcdate(),0) AS CalAmount,
         --monPAmount,
         CONVERT(DECIMAL(10,2),Isnull(monPAmount,0)) AS monPAmount,
		lngPConclAnalysis,
         (SELECT vcdata
          FROM   listdetails
          WHERE  numlistitemid = lngPConclAnalysis) AS lngPConclAnalysisName,
         (SELECT vcdata
          FROM   listdetails
          WHERE  numlistitemid = numSalesOrPurType) AS numSalesOrPurTypeName,
		  numSalesOrPurType,
		 Opp.vcOppRefOrderNo,
		 Opp.vcMarketplaceOrderID,
         C2.vcCompanyName,
         D2.tintCRMType,
         Opp.tintActive,
         dbo.Fn_getcontactname(Opp.numCreatedby)
           + ' '
           + CONVERT(VARCHAR(20),Dateadd(MINUTE,-@ClientTimeZoneOffset,Opp.bintCreatedDate)) AS CreatedBy,
         dbo.Fn_getcontactname(Opp.numModifiedBy)
           + ' '
           + CONVERT(VARCHAR(20),Dateadd(MINUTE,-@ClientTimeZoneOffset,Opp.bintModifiedDate)) AS ModifiedBy,
         dbo.Fn_getcontactname(Opp.numRecOwner) AS RecordOwner,
         Opp.numRecOwner,
         Isnull(tintshipped,0) AS tintshipped,
         Isnull(tintOppStatus,0) AS tintOppStatus,
         [dbo].[GetOppStatus](Opp.numOppId) AS vcDealStatus,
         dbo.Opportunitylinkeditems(@OpportunityId) +
         (SELECT COUNT(*) FROM  dbo.Communication Comm JOIN Correspondence Corr ON Comm.numCommId=Corr.numCommID
			WHERE Comm.numDomainId=Opp.numDomainId AND Corr.numDomainID=Comm.numDomainID AND Corr.tintCorrType IN (1,2,3,4) AND Corr.numOpenRecordID=Opp.numOppId) AS NoOfProjects,
         (SELECT A.vcFirstName
                   + ' '
                   + A.vcLastName
          FROM   AdditionalContactsInformation A
          WHERE  A.numcontactid = opp.numAssignedTo) AS numAssignedToName,
		  opp.numAssignedTo,
         Opp.numAssignedBy,
         (SELECT COUNT(* )
          FROM   GenericDocuments
          WHERE  numRecID = @OpportunityId
                 AND vcDocumentSection = 'O') AS DocumentCount,
         CASE 
           WHEN tintOppStatus = 1 THEN 100
           ELSE isnull((SELECT SUM(tintPercentage)
                 FROM   OpportunityStageDetails
                 WHERE  numOppId = @OpportunityId
                        AND bitStageCompleted = 1),0)
         END AS TProgress,
         (SELECT TOP 1 Isnull(varRecurringTemplateName,'')
          FROM   RecurringTemplate
          WHERE  numRecurringId = OPR.numRecurringId) AS numRecurringIdName,
		 OPR.numRecurringId,
         Link.numOppID AS numParentOppID,
         Link.vcPoppName AS ParentOppName,
         Link.vcSource AS Source,
         ISNULL(C.varCurrSymbol,'') varCurrSymbol,
          ISNULL(Opp.fltExchangeRate,0) AS [fltExchangeRate],
           ISNULL(C.chrCurrency,'') AS [chrCurrency],
         ISNULL(C.numCurrencyID,0) AS [numCurrencyID],
         (SELECT COUNT(*) FROM [OpportunityLinking] OL WHERE OL.[numParentOppID] = Opp.numOppId) AS ChildCount,
         (SELECT COUNT(*) FROM [RecurringTransactionReport] RTR WHERE RTR.[numRecTranSeedId] = Opp.numOppId)  AS RecurringChildCount,
         (SELECT COUNT(*) FROM [ShippingReport] WHERE numOppBizDocId IN (SELECT numOppBizDocsId FROM   OpportunityBizDocs BD WHERE  BD.numOppId = @OpportunityId))AS ShippingReportCount,
         ISNULL(Opp.numStatus ,0) numStatus,
         (SELECT vcdata
          FROM   listdetails
          WHERE  numlistitemid = Opp.numStatus) AS numStatusName,
          (SELECT SUBSTRING(
	(SELECT ','+  A.vcFirstName+' ' +A.vcLastName + CASE WHEN ISNULL(SR.numContactType,0)>0 THEN '(' + dbo.fn_GetListItemName(SR.numContactType) + ')' ELSE '' END
	FROM ShareRecord SR JOIN UserMaster UM ON UM.numUserDetailId=SR.numAssignedTo 
							 JOIN AdditionalContactsInformation A ON UM.numUserDetailId=A.numContactID 
		 WHERE SR.numDomainID=Opp.numDomainID AND SR.numModuleID=3 AND SR.numRecordID=Opp.numOppId
				AND UM.numDomainID=Opp.numDomainID and UM.numDomainID=A.numDomainID FOR XML PATH('')),2,200000)) AS numShareWith,
		ISNULL((SELECT SUM(ISNULL(monDealAmount,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = @OpportunityId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0) AS monDealAmount,
		ISNULL((SELECT SUM(ISNULL(monAmountPaid,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = @OpportunityId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0) AS monAmountPaid,
		ISNULL(Opp.tintSourceType,0) AS tintSourceType,
		ISNULL(Opp.tintTaxOperator,0) AS tintTaxOperator,  
             isnull(Opp.intBillingDays,0) AS numBillingDays,isnull(Opp.bitInterestType,0) AS bitInterestType,
             isnull(opp.fltInterest,0) AS fltInterest,  isnull(Opp.fltDiscount,0) AS fltDiscount,
             isnull(Opp.bitDiscountType,0) AS bitDiscountType,ISNULL(Opp.bitBillingTerms,0) AS bitBillingTerms,
             ISNULL(OPP.numDiscountAcntType,0) AS numDiscountAcntType,
              dbo.fn_getOPPAddress(Opp.numOppId,@numDomainID,1) AS BillingAddress,
            dbo.fn_getOPPAddress(Opp.numOppId,@numDomainID,2) AS ShippingAddress,

            -- ISNULL((SELECT SUBSTRING((SELECT '$^$' + ISNULL( (SELECT vcShipFieldValue FROM dbo.ShippingFieldValues WHERE numDomainID=Opp.numDomainID AND intShipFieldID IN ( SELECT intShipFieldID FROM dbo.ShippingFields WHERE numListItemID=OBD.numShipVia  AND vcFieldName='Tracking URL')),'') +'#^#'+ RTRIM(LTRIM(vcTrackingNo) )  FROM dbo.OpportunityBizDocs OBD WHERE OBD.numOppId=Opp.numOppID FOR XML PATH('')),4,200000)),'') AS  vcTrackingNo,
			 (SELECT  
				SUBSTRING(
							(SELECT '$^$' + 
							(SELECT vcShipFieldValue FROM dbo.ShippingFieldValues 
								WHERE numDomainID=Opp.numDomainID AND intShipFieldID IN ( SELECT intShipFieldID FROM dbo.ShippingFields WHERE numListItemID=OBD.numShipVia  AND vcFieldName='Tracking URL')
							) +'#^#'+ RTRIM(LTRIM(vcTrackingNo))						
							FROM dbo.OpportunityBizDocs OBD WHERE OBD.numOppId= Opp.numOppID and ISNULL(OBD.numShipVia,0) <> 0
				FOR XML PATH('')),4,200000)
						
			 ) AS  vcTrackingNo,

            ISNULL(numPercentageComplete,0) numPercentageComplete,
            dbo.GetListIemName(ISNULL(numPercentageComplete,0)) AS PercentageCompleteName,
            ISNULL(numBusinessProcessID,0) AS [numBusinessProcessID],
            CASE WHEN opp.tintOppType=1 THEN dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID) ELSE '' END AS vcInventoryStatus
            ,ISNULL(bitUseShippersAccountNo,0) [bitUseShippersAccountNo], ISNULL(D2.vcShippersAccountNo,'') [vcShippersAccountNo] ,
			(SELECT SUM((ISNULL(monTotAmtBefDiscount, ISNULL(monTotAmount,0))- ISNULL(monTotAmount,0))) FROM OpportunityBizDocItems WHERE numOppBizDocID IN (SELECT numOppBizDocsId FROM OpportunityBizDocs WHERE numOppId = @OpportunityId AND bitAuthoritativeBizDocs = 1)) AS numTotalDiscount,
			ISNULL(Opp.vcRecurrenceType,'') AS vcRecurrenceType,
			ISNULL(Opp.bitRecurred,0) AS bitRecurred,
			(
				CASE 
				WHEN opp.tintOppType=1 AND opp.tintOppStatus = 1 
				THEN  
					dbo.GetOrderShipReceiveStatus(opp.numOppId,1,opp.tintShipped)
				ELSE 
						'' 
				END
			) AS vcOrderedShipped,
			(
				CASE 
				WHEN opp.tintOppType = 2 AND opp.tintOppStatus = 1 
				THEN  
					dbo.GetOrderShipReceiveStatus(opp.numOppId,2,opp.tintShipped)
				ELSE 
						'' 
				END
			) AS vcOrderedReceived,
			ISNULL(C2.numCompanyType,0) AS numCompanyType,
		ISNULL(C2.vcProfile,0) AS vcProfile,
		ISNULL(Opp.numAccountClass,0) AS numAccountClass,D3.vcPartnerCode+'-'+C3.vcCompanyName as numPartner,
		ISNULL(Opp.numPartner,0) AS numPartnerId,
		ISNULL(Opp.bitIsInitalSalesOrder,0) AS bitIsInitalSalesOrder,
		ISNULL(Opp.dtReleaseDate,'') AS dtReleaseDate,
		ISNULL(Opp.numPartenerContact,0) AS numPartenerContactId,
		A.vcGivenName AS numPartenerContact,
		Opp.numReleaseStatus,
		(CASE Opp.numReleaseStatus WHEN 1 THEN 'Open' WHEN 2 THEN 'Purchased' ELSE '' END) numReleaseStatusName,
		ISNULL(Opp.intUsedShippingCompany,0) intUsedShippingCompany,
		CASE WHEN ISNULL(Opp.intUsedShippingCompany,0) = 0 THEN '' ELSE ISNULL((SELECT vcData FROM ListDetails WHERE numListID = 82 AND numListItemID=ISNULL(Opp.intUsedShippingCompany,0)),'') END AS vcShippingCompany,
		ISNULL(Opp.numShipmentMethod,0) numShipmentMethod
		,CASE WHEN ISNULL(Opp.numShipmentMethod,0) = 0 THEN '' ELSE ISNULL((SELECT vcData FROM ListDetails WHERE numListID = 338 AND numListItemID=ISNULL(Opp.numShipmentMethod,0)),'') END AS vcShipmentMethod
		,ISNULL(Opp.vcCustomerPO#,'') vcCustomerPO#,
		CONCAT(CASE tintEDIStatus 
			--WHEN 1 THEN '850 Received'
			WHEN 2 THEN '850 Acknowledged'
			WHEN 3 THEN '940 Sent'
			WHEN 4 THEN '940 Acknowledged'
			WHEN 5 THEN '856 Received'
			WHEN 6 THEN '856 Acknowledged'
			WHEN 7 THEN '856 Sent'
			WHEN 8 THEN 'Send 940 Failed'
			WHEN 9 THEN 'Send 856 & 810 Failed'
			WHEN 11 THEN '850 Partially Created'
			WHEN 12 THEN '850 SO Created'
			ELSE '' 
		END,'  ','<a onclick="return Open3PLEDIHistory(',Opp.numOppID,')"><img src="../images/GLReport.png" border="0"></a>') tintEDIStatusName,
		ISNULL(Opp.numShippingService,0) numShippingService,
		ISNULL((SELECT vcShipmentService FROM ShippingService WHERE (numDomainID=@numDomainID OR ISNULL(numDomainID,0)=0) AND numShippingServiceID = Opp.numShippingService),'') AS vcShippingService,
		CASE 
			WHEN vcSignatureType = 0 THEN 'Service Default'
			WHEN vcSignatureType = 1 THEN 'Adult Signature Required'
			WHEN vcSignatureType = 2 THEN 'Direct Signature'
			WHEN vcSignatureType = 3 THEN 'InDirect Signature'
			WHEN vcSignatureType = 4 THEN 'No Signature Required'
			ELSE ''
		END AS vcSignatureType
		,dbo.fn_getOPPState(Opp.numOppId,Opp.numDomainID,2) vcShipState
  FROM   OpportunityMaster Opp
         JOIN divisionMaster D2 ON Opp.numDivisionID = D2.numDivisionID
         JOIN CompanyInfo C2 ON C2.numCompanyID = D2.numCompanyID
		 LEFT JOIN DivisionMasterShippingConfiguration ON D2.numDivisionID=DivisionMasterShippingConfiguration.numDivisionID
		 LEFT JOIN divisionMaster D3 ON Opp.numPartner = D3.numDivisionID
		 LEFT JOIN AdditionalContactsInformation A ON Opp.numPartenerContact = A.numContactId
         LEFT JOIN CompanyInfo C3 ON C3.numCompanyID = D3.numCompanyID
         LEFT JOIN (SELECT TOP 1 vcPoppName,
                                 numOppID,
                                 numChildOppID,
                                 vcSource,numParentOppID,OpportunityMaster.numDivisionID AS numParentDivisionID 
                    FROM   OpportunityLinking
                          left JOIN OpportunityMaster
                             ON numOppID = numParentOppID
                    WHERE  numChildOppID = @OpportunityId) Link
           ON Link.numChildOppID = Opp.numOppID
           LEFT OUTER JOIN [Currency] C
					ON C.numCurrencyID = Opp.numCurrencyID
			LEFT OUTER JOIN [OpportunityRecurring] OPR
					ON OPR.numOppId = Opp.numOppId
					 LEFT JOIN AddressDetails AD --Billing add
				ON AD.numDomainID=D2.numDomainID AND AD.numRecordID=D2.numDivisionID AND AD.tintAddressOf = 2 AND AD.tintAddressType = 1 AND AD.bitIsPrimary=1
			 LEFT JOIN AddressDetails AD2--Shipping address
				ON AD2.numDomainID=D2.numDomainID AND AD2.numRecordID=D2.numDivisionID AND AD2.tintAddressOf = 2 AND AD2.tintAddressType = 2 AND AD2.bitIsPrimary=1	
		LEFT JOIN dbo.CampaignMaster CAMP ON CAMP.numCampaignID = Opp.numCampainID 
  WHERE  Opp.numOppId = @OpportunityId
         AND Opp.numDomainID = @numDomainID
/****** Object:  StoredProcedure [dbo].[USP_OppManage]    Script Date: 07/26/2008 16:20:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OppManage')
DROP PROCEDURE usp_oppmanage
GO
CREATE PROCEDURE [dbo].[USP_OppManage] 
(                                                                          
  @numOppID as numeric=0 OUTPUT,                                                                          
  @numContactId numeric(9)=null,                                                                          
  @numDivisionId numeric(9)=null,                                                                          
  @tintSource numeric(9)=null,                                                                          
  @vcPOppName Varchar(100)='' OUTPUT,                                                                          
  @Comments varchar(1000)='',                                                                          
  @bitPublicFlag bit=0,                                                                          
  @numUserCntID numeric(9)=0,                                                                                 
  @monPAmount DECIMAL(20,5) =0,                                                 
  @numAssignedTo as numeric(9)=0,                                                                                                        
  @numDomainId numeric(9)=0,                                                                                                                                                                                   
  @strItems as text=null,                                                                          
  @strMilestone as text=null,                                                                          
  @dtEstimatedCloseDate datetime,                                                                          
  @CampaignID as numeric(9)=null,                                                                          
  @lngPConclAnalysis as numeric(9)=null,                                                                         
  @tintOppType as tinyint,                                                                                                                                             
  @tintActive as tinyint,                                                              
  @numSalesOrPurType as numeric(9),              
  @numRecurringId as numeric(9),
  @numCurrencyID as numeric(9)=0,
  @DealStatus as tinyint =0,-- 0=Open,1=Won,2=Lost
  @numStatus AS NUMERIC(18,0),
  @vcOppRefOrderNo VARCHAR(100),
  @numBillAddressId numeric(9)=0,
  @numShipAddressId numeric(9)=0,
  @bitStockTransfer BIT=0,
  @WebApiId INT = 0,
  @tintSourceType TINYINT=0,
  @bitDiscountType as bit,
  @fltDiscount  as float,
  @bitBillingTerms as bit,
  @intBillingDays as integer,
  @bitInterestType as bit,
  @fltInterest as float,
  @tintTaxOperator AS TINYINT,
  @numDiscountAcntType AS NUMERIC(9),
  @vcWebApiOrderNo VARCHAR(100)=NULL,
  @vcCouponCode		VARCHAR(100) = '',
  @vcMarketplaceOrderID VARCHAR(100)=NULL,
  @vcMarketplaceOrderDate  datetime=NULL,
  @vcMarketplaceOrderReportId VARCHAR(100)=NULL,
  @numPercentageComplete NUMERIC(9),
  @bitUseShippersAccountNo BIT = 0,
  @bitUseMarkupShippingRate BIT = 0,
  @numMarkupShippingRate NUMERIC(19,2) = 0,
  @intUsedShippingCompany INT = 0,
  @numShipmentMethod NUMERIC(18,0)=0,
  @dtReleaseDate DATE = NULL,
  @numPartner NUMERIC(18,0)=0,
  @tintClickBtn INT=0,
  @numPartenerContactId NUMERIC(18,0)=0,
  @numAccountClass NUMERIC(18,0) = 0,
  @numWillCallWarehouseID NUMERIC(18,0) = 0,
  @numVendorAddressID NUMERIC(18,0) = 0,
  @numShipFromWarehouse NUMERIC(18,0) = 0,
  @numShippingService NUMERIC(18,0) = 0,
  @ClientTimeZoneOffset INT = 0,
  @vcCustomerPO VARCHAR(100)=NULL,
  @bitDropShipAddress BIT = 0,
  @PromCouponCode AS VARCHAR(100) = NULL,
  @numPromotionId AS NUMERIC(18,0) = 0
  -- USE PARAMETER WITH OPTIONAL VALUE BECAUSE PROCEDURE IS CALLED FROM OTHER PROCEDURE WHICH WILL NOT PASS NEW PARAMETER VALUE
)                                                                          
                                                                          
as  
BEGIN TRY
BEGIN TRANSACTION
	DECLARE @fltExchangeRate float                                 
	DECLARE @hDocItem int                                                                                                                            
	DECLARE @TotalAmount as DECIMAL(20,5)                                                                          
	DECLARE @tintOppStatus as tinyint
	DECLARE @bitNewOrder AS BIT = (CASE WHEN ISNULL(@numOppID,0) = 0 THEN 1 ELSE 0 END)
	
	IF ISNULL(@numContactId,0) = 0
	BEGIN
		RAISERROR('CONCAT_REQUIRED',16,1)
		RETURN
	END  
	
	IF ISNULL(@tintOppType,0) = 0
	BEGIN
		RAISERROR('OPP_TYPE_CAN_NOT_BE_0',16,1)
		RETURN
	END             

	IF ISNULL(@numOppID,0) > 0 AND EXISTS (SELECT numOppId FROM OpportunityMaster WHERE numDomainId=@numDomainId AND numOppId=@numOppID AND ISNULL(tintshipped,0) = 1) 
	BEGIN	 
		RAISERROR('OPP/ORDER_IS_CLOSED',16,1)
		RETURN
	END

	DECLARE @dtItemRelease AS DATE
	SET @dtItemRelease = DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE())


	IF CONVERT(VARCHAR(10),@strItems) <> ''
	BEGIN
		EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems

		--VALIDATE PROMOTION OF COUPON CODE
		DECLARE @TEMP TABLE
		(
			ID INT IDENTITY(1,1),
			numOppItemID NUMERIC(18,0),
			numPromotionID NUMERIC(18,0)
		)

		INSERT INTO @TEMP
		(
			numOppItemID,
			numPromotionID
		)
		SELECT
			numoppitemtCode,
			numPromotionID
		FROM
			OPENXML (@hDocItem,'/NewDataSet/Item',2)
		WITH
		(
			numoppitemtCode NUMERIC(18,0),
			numPromotionID NUMERIC(18,0)
		)

		IF (SELECT
				COUNT(*)
			FROM
				PromotionOffer PO
			WHERE
				numDomainId=@numDomainId
				AND bitRequireCouponCode = 1
				AND ISNULL(bitEnabled,0) = 1
				AND ISNULL(tintUsageLimit,0) > 0
				AND (intCouponCodeUsed 
					- (CASE WHEN ISNULL((SELECT COUNT(*) FROM OpportunityItems WHERE numOppID=ISNULL(@numOppID,0) AND numPromotionID=PO.numProId),0) > 0 THEN 1 ELSE 0 END) 
					+ (CASE WHEN ISNULL((SELECT COUNT(*) FROM @TEMP WHERE numPromotionID=PO.numProId),0) > 0 THEN 1 ELSE 0 END)) > tintUsageLimit) > 0
		BEGIN
			RAISERROR('USAGE_OF_COUPON_EXCEED',16,1)
			RETURN
		END
		ELSE
		BEGIN
			UPDATE
				PO
			SET 
				intCouponCodeUsed = ISNULL(intCouponCodeUsed,0) 
									- (CASE WHEN ISNULL((SELECT COUNT(*) FROM OpportunityItems WHERE numOppID=ISNULL(@numOppID,0) AND numPromotionID=PO.numProId),0) > 0 THEN 1 ELSE 0 END) 
									+ (CASE WHEN ISNULL(T1.intUsed,0) > 0 THEN 1 ELSE 0 END) 
			FROM
				PromotionOffer PO
			INNER JOIN
				(
					SELECT
						numPromotionID,
						COUNT(*) AS intUsed
					FROM
						@TEMP
					GROUP BY
						numPromotionID
				) AS T1
			ON
				PO.numProId = T1.numPromotionID
			WHERE
				numDomainId=@numDomainId
				AND bitRequireCouponCode = 1
				AND ISNULL(bitEnabled,0) = 1
		END
	END

	DECLARE @numCompany AS NUMERIC(18)
	SELECT @numCompany = numCompanyID FROM DivisionMaster WHERE numDivisionID = @numDivisionId

	DECLARE @tintCommitAllocation AS TINYINT
	DECLARE @bitAllocateInventoryOnPickList AS BIT

	SELECT @tintCommitAllocation=ISNULL(tintCommitAllocation,1) FROM Domain WHERE numDomainId=@numDomainId
	SELECT @bitAllocateInventoryOnPickList=ISNULL(bitAllocateInventoryOnPickList,0) FROM DivisionMaster WHERE numDivisionID=@numDivisionId

	--IF NEW OPPERTUNITY
	IF @numOppID = 0                                                                          
	BEGIN
		DECLARE @bitIsInitalSalesOrder BIT
		IF(@DealStatus=1 AND @tintOppType=1)
		BEGIN
			SET @bitIsInitalSalesOrder=1
		END

		DECLARE @intOppTcode AS NUMERIC(9)  
  
		SELECT @intOppTcode = MAX(numOppId) FROM OpportunityMaster                

		SET @intOppTcode = ISNULL(@intOppTcode,0) + 1                                                
		
		SET @vcPOppName = @vcPOppName + '-'+ CONVERT(varchar(4),YEAR(GETUTCDATE()))  +'-A'+ CONVERT(varchar(10),@intOppTcode)                                  
  
		IF ISNULL(@numCurrencyID,0) = 0 
		BEGIN
			SET @fltExchangeRate=1
			SELECT @numCurrencyID= ISNULL(numCurrencyID,0) FROM dbo.Domain WHERE numDomainId=@numDomainId	
		END
		ELSE
		BEGIN 
			SET @fltExchangeRate = dbo.GetExchangeRate(@numDomainID,@numCurrencyID)
        END  
		                                                        
		IF ISNULL(@numAccountClass,0) = 0
		BEGIN                                                  
		DECLARE @tintDefaultClassType AS INT = 0
		SELECT @tintDefaultClassType = ISNULL(tintDefaultClassType,0) FROM Domain WHERE numDomainId = @numDomainID 

		IF @tintDefaultClassType = 1 --USER
		BEGIN
			SELECT 
				@numAccountClass=ISNULL(numDefaultClass,0) 
			FROM 
				dbo.UserMaster UM 
			JOIN 
				dbo.Domain D 
			ON 
				UM.numDomainID=D.numDomainId
			WHERE 
				D.numDomainId=@numDomainId 
				AND UM.numUserDetailId=@numUserCntID
		END
		ELSE IF @tintDefaultClassType = 2 --COMPANY
		BEGIN
			SELECT 
				@numAccountClass=ISNULL(numAccountClassID,0) 
			FROM 
				dbo.DivisionMaster DM 
			WHERE 
				DM.numDomainId=@numDomainId 
				AND DM.numDivisionID=@numDivisionID
		END
		ELSE
		BEGIN
			SET @numAccountClass = 0
		END
		END

        IF(@DealStatus=1 AND @tintOppType=1 AND @tintClickBtn=1)
		BEGIN
			SET @DealStatus=0
		END         
		--PRINT ('Deal Staus ' +@DealStatus)  

		-------- ORDER BASED Promotion Logic-------------

		DECLARE @numDiscountID NUMERIC(18,0) = NULL
		IF @numPromotionId > 0
		BEGIN
			IF ISNULL(@PromCouponCode,'') <> ''
			BEGIN
				DECLARE @numCouponCode NUMERIC(18,0)
				DECLARE @intCodeUsageLimit NUMERIC(18,0)
				DECLARE @intCodeUsed INT		
				DECLARE @intUsedCountForDiv INT

				SET @numCouponCode = (SELECT COUNT(DC.vcDiscountCode) FROM PromotionOffer PO
									INNER JOIN DiscountCodes DC
									ON PO.numProId = DC.numPromotionID
									WHERE numProId = @numPromotionId AND numDomainId = @numDomainID AND bitRequireCouponCode = 1 
									AND DC.vcDiscountCode = @PromCouponCode)
		
				IF @numCouponCode = 0 
				BEGIN	 
					RAISERROR('INVALID_CODE',16,1)
					RETURN
				END
				ELSE 
				BEGIN 
					SELECT 
						@numDiscountID= DC.numDiscountId 
						,@intCodeUsageLimit = DC.CodeUsageLimit 
					FROM 
						PromotionOffer PO
					INNER JOIN 
						DiscountCodes DC
					ON 
						PO.numProId = DC.numPromotionID
					WHERE 
						numProId = @numPromotionId 
						AND numDomainId = @numDomainID 
						AND bitRequireCouponCode = 1 
						AND DC.vcDiscountCode = @PromCouponCode

					SET @intUsedCountForDiv = (SELECT COUNT(DCU.numDivisionId) FROM DiscountCodeUsage DCU
											INNER JOIN DiscountCodes DC
											ON  DCU.numDiscountId = DC.numDiscountId
											WHERE DCU.numDivisionId = @numDivisionId AND DC.vcDiscountCode = @PromCouponCode)			

					IF @intUsedCountForDiv = 0
					BEGIN
						INSERT INTO DiscountCodeUsage
						(numDiscountId, numDivisionId, intCodeUsed)
						VALUES
						(@numDiscountID, @numDivisionId, 1)
					END
					ELSE IF @intUsedCountForDiv > 0 AND @intCodeUsageLimit = 0
					BEGIN
						UPDATE DiscountCodeUsage
							SET intCodeUsed = @intCodeUsed + 1
						WHERE numDivisionId = @numDivisionId AND numDiscountId = @numDiscountID
					END
					ELSE IF @intUsedCountForDiv > 0 AND @intCodeUsageLimit > 0
					BEGIN
						SET @intCodeUsed = (SELECT intCodeUsed FROM DiscountCodeUsage DCU
											INNER JOIN DiscountCodes DC
											ON  DCU.numDiscountId = DC.numDiscountId
											WHERE DCU.numDivisionId = @numDivisionId AND DC.vcDiscountCode = @PromCouponCode)

						IF @intCodeUsed < @intCodeUsageLimit
						BEGIN				
							UPDATE DiscountCodeUsage
								SET intCodeUsed = @intCodeUsed + 1
							WHERE numDivisionId = @numDivisionId AND numDiscountId = @numDiscountID
						END
						ELSE
						BEGIN	
							RAISERROR('CODE_USAGE_LIMIT_EXCEEDS',16,1)
							RETURN
						END
					END
				END	
			END	
		END

		-------- ORDER BASED Promotion Logic-------------
		                                                    
		INSERT INTO OpportunityMaster
		(                                                                             
			numContactId,numDivisionId,txtComments,numCampainID,bitPublicFlag,tintSource,tintSourceType,vcPOppName,                                                                          
			intPEstimatedCloseDate,monPAmount,numCreatedBy,bintCreatedDate,numModifiedBy,bintModifiedDate,numDomainId,                                                                                                             
			numRecOwner,lngPConclAnalysis,tintOppType,numSalesOrPurType,numCurrencyID,fltExchangeRate,numAssignedTo,
			numAssignedBy,[tintOppStatus],numStatus,vcOppRefOrderNo,bitStockTransfer,bitBillingTerms,intBillingDays,
			bitInterestType,fltInterest,vcCouponCode,bitDiscountType,fltDiscount,bitPPVariance,vcMarketplaceOrderID,
			vcMarketplaceOrderReportId,numPercentageComplete,numAccountClass,tintTaxOperator,bitUseShippersAccountNo,
			bitUseMarkupShippingRate,numMarkupShippingRate,intUsedShippingCompany,numShipmentMethod,numShippingService,numPartner,numPartenerContact,dtReleaseDate,bitIsInitalSalesOrder,numVendorAddressID,numShipFromWarehouse
			,vcCustomerPO#,bitDropShipAddress,numDiscountID
		)                                                                          
		Values                                                                          
		(                                                                          
			@numContactId,@numDivisionId,ISNULL(@Comments,''),@CampaignID,@bitPublicFlag,@tintSource,@tintSourceType,@vcPOppName,                                                                          
			@dtEstimatedCloseDate,@monPAmount,@numUserCntID,(CASE @tintSourceType WHEN 3 THEN @vcMarketplaceOrderDate ELSE GETUTCDATE() END),                                                                          
			@numUserCntID,GETUTCDATE(),@numDomainId,@numUserCntID,@lngPConclAnalysis,@tintOppType,@numSalesOrPurType,@numCurrencyID, 
			@fltExchangeRate,(CASE WHEN @numAssignedTo > 0 THEN @numAssignedTo ELSE NULL END),(CASE WHEN @numAssignedTo>0 THEN @numUserCntID ELSE NULL END),
			@DealStatus,@numStatus,@vcOppRefOrderNo,@bitStockTransfer,@bitBillingTerms,@intBillingDays,@bitInterestType,@fltInterest,
			@vcCouponCode,@bitDiscountType,@fltDiscount,1,@vcMarketplaceOrderID,@vcMarketplaceOrderReportId,@numPercentageComplete,
			@numAccountClass,@tintTaxOperator,@bitUseShippersAccountNo,@bitUseMarkupShippingRate,@numMarkupShippingRate,@intUsedShippingCompany,@numShipmentMethod,@numShippingService,@numPartner,@numPartenerContactId,@dtReleaseDate,@bitIsInitalSalesOrder,@numVendorAddressID,@numShipFromWarehouse
			,@vcCustomerPO,@bitDropShipAddress,@numDiscountID
		)                                                                                                                      
		
		SET @numOppID=(SELECT SCOPE_IDENTITY())                
                            
		--UPDATE OPPNAME AS PER NAME TEMPLATE
		EXEC dbo.USP_UpdateNameTemplateValue @tintOppType,@numDomainID,@numOppID

		SELECT @vcPOppName=ISNULL(vcPOppName,'') FROM OpportunityMaster WHERE numOppId=@numOppID

		--MAP CUSTOM FIELD	
		DECLARE @tintPageID AS TINYINT;
		SET @tintPageID = CASE WHEN @tintOppType=1 THEN 2 ELSE 6 END 
  	
		EXEC dbo.USP_AddParentChildCustomFieldMap
				@numDomainID = @numDomainID, --  numeric(18, 0)
				@numRecordID = @numOppID, --  numeric(18, 0)
				@numParentRecId = @numDivisionId, --  numeric(18, 0)
				@tintPageID = @tintPageID --  tinyint
 	
		IF ISNULL(@numStatus,0) > 0 AND @tintOppType=1 AND isnull(@DealStatus,0)=1
		BEGIN
	   		EXEC USP_ManageOpportunityAutomationQueue
					@numOppQueueID = 0, -- numeric(18, 0)
					@numDomainID = @numDomainID, -- numeric(18, 0)
					@numOppId = @numOppID, -- numeric(18, 0)
					@numOppBizDocsId = 0, -- numeric(18, 0)
					@numOrderStatus = @numStatus, -- numeric(18, 0)
					@numUserCntID = @numUserCntID, -- numeric(18, 0)
					@tintProcessStatus = 1, -- tinyint
					@tintMode = 1 -- TINYINT
		END
  
		-- INSERTING ITEMS                                                                                                                    
		IF CONVERT(VARCHAR(10),@strItems) <> '' AND @numOppID > 0
		BEGIN
			EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems       
	
			INSERT INTO OpportunityItems
			(
				numOppId,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,bitDropShip,bitDiscountType,fltDiscount,
				monTotAmtBefDiscount,vcItemName,vcModelID,vcManufacturer,vcPathForTImage,monVendorCost,numUOMId,bitWorkOrder,numVendorWareHouse,
				numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,numClassID,numToWarehouseItemID,vcAttributes,vcAttrValues,monAvgCost,bitItemPriceApprovalRequired,
				numPromotionID,bitPromotionTriggered,vcPromotionDetail,numSortOrder,numCost,vcNotes,vcInstruction,bintCompliationDate,numWOAssignedTo,numShipToAddressID,ItemReleaseDate
				,bitMarkupDiscount,vcChildKitSelectedItems
			)
			SELECT 
				@numOppID,X.numItemCode,dbo.fn_UOMConversion(X.numUOM,numItemCode,@numDomainId,null) * x.numUnitHour,x.monPrice,x.monTotAmount,X.vcItemDesc,
				(CASE X.numWarehouseItmsID 
				WHEN -1 
				THEN 
					CASE 
						WHEN ((SELECT COUNT(*) FROM Item WHERE numDomainID=@numDomainId AND vcSKU= X.vcSKU) > 0)
							THEN
								(SELECT TOP 1 
									numWareHouseItemID 
								FROM 
									WareHouseItems 
								WHERE 
									[numItemID] = X.numItemCode 
									AND numWareHouseID IN (SELECT TOP 1 ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail WHERE numDomainID =@numDomainId AND WebApiId = @WebApiId))
						ELSE
							(SELECT 
								[numWareHouseItemID] 
							FROM 
								[WareHouseItems] 
							WHERE 
								[numItemID] = X.numItemCode 
								AND ISNULL(WareHouseItems.vcWHSKU,'') = ISNULL(X.vcSKU,'')
								AND numWareHouseID IN (SELECT TOP 1 ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail WHERE numDomainID =@numDomainId AND WebApiId = @WebApiId)) 
					END
				WHEN 0 
				THEN 
					CASE 
					WHEN ((SELECT COUNT(*) FROM Item WHERE numDomainID=@numDomainId AND vcSKU=X.vcSKU) > 0)
						THEN
							(SELECT TOP 1 
								numWareHouseItemID 
							FROM 
								WareHouseItems 
							WHERE 
								[numItemID] = X.numItemCode 
								AND numWareHouseID IN (SELECT TOP 1 ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail WHERE numDomainID =@numDomainId AND WebApiId = @WebApiId))
					ELSE
						(SELECT 
							[numWareHouseItemID] 
						FROM 
							[WareHouseItems] 
						WHERE 
							[numItemID] = X.numItemCode 
							AND ISNULL(WareHouseItems.vcWHSKU,'') = ISNULL(X.vcSKU,'')
							AND numWareHouseID IN (SELECT TOP 1 ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail WHERE numDomainID =@numDomainId AND WebApiId = @WebApiId)) 
					END
				ELSE  
					X.numWarehouseItmsID 
				END) AS numWarehouseItmsID,
				X.ItemType,X.DropShip,X.bitDiscountType,X.fltDiscount,X.monTotAmtBefDiscount,X.vcItemName,(SELECT vcModelID FROM item WHERE numItemCode = X.numItemCode),
				(SELECT vcManufacturer FROM item WHERE numItemCode = X.numItemCode),(SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = X.numItemCode AND BitDefault = 1 AND numdomainId = @numdomainId),
				(SELECT isnull(VN.monCost,0) FROM Vendor VN INNER JOIN Item IT ON IT.numItemCode=VN.numItemCode AND VN.numVendorID=IT.numVendorID WHERE  VN.numItemCode=X.numItemCode),
				X.numUOM,X.bitWorkOrder,X.numVendorWareHouse,X.numShipmentMethod,X.numSOVendorId,X.numProjectID,X.numProjectStageID,
				0,
				X.numToWarehouseItemID,X.Attributes,X.AttributeIDs,(SELECT (CASE WHEN ISNULL(I.bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(I.monAverageCost,0) END) FROM Item I WHERE I.numDomainId=@numDomainId AND I.numItemCode=X.numItemCode),
				X.bitItemPriceApprovalRequired,X.numPromotionID,X.bitPromotionTriggered,X.vcPromotionDetail,X.numSortOrder,ISNULL(X.numCost,0),ISNULL(X.vcVendorNotes,'')
				,X.vcInstruction,X.bintCompliationDate,X.numWOAssignedTo,X.numShipToAddressID,ISNULL(ItemReleaseDate,@dtItemRelease),X.bitMarkupDiscount,ISNULL(X.KitChildItems,'')
			FROM
				(
					SELECT 
						*
					FROM 
						OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=1]',2)                                                                          
					WITH                       
					(                                                                          
						numoppitemtCode NUMERIC(9),numItemCode NUMERIC(9), numUnitHour FLOAT, monPrice DECIMAL(30,16), monTotAmount DECIMAL(20,5),vcItemDesc VARCHAR(1000),                                    
						numWarehouseItmsID NUMERIC(9), ItemType VARCHAR(30), DropShip BIT, bitDiscountType BIT,	fltDiscount DECIMAL(30,16), monTotAmtBefDiscount DECIMAL(20,5),
						vcItemName VARCHAR(300),numUOM NUMERIC(9),bitWorkOrder BIT,numVendorWareHouse NUMERIC(9),numShipmentMethod NUMERIC(9),numSOVendorId NUMERIC(9),
						numProjectID numeric(9),numProjectStageID numeric(9),numToWarehouseItemID NUMERIC(9),Attributes varchar(500),AttributeIDs VARCHAR(500),vcSKU VARCHAR(100)
						,bitItemPriceApprovalRequired BIT,numPromotionID NUMERIC(18,0),bitPromotionTriggered BIT,vcPromotionDetail VARCHAR(2000),numSortOrder NUMERIC(18,0)
						,numCost DECIMAL(20,5),vcVendorNotes VARCHAR(1000),vcInstruction VARCHAR(1000),bintCompliationDate DATETIME,numWOAssignedTo NUMERIC(18,0)
						,numShipToAddressID  NUMERIC(18,0),ItemReleaseDate DATETIME,bitMarkupDiscount BIT,KitChildItems VARCHAR(MAX)
					)
				)X    
			ORDER BY 
				X.numoppitemtCode
    
			-- UPDATE EXISTING CUSTOMER PART NO
			UPDATE
				CPN
			SET
				CPN.CustomerPartNo = ISNULL(TEMPCPN.CustomerPartNo,'')
			FROM
				CustomerPartNumber CPN
			INNER JOIN
			(
				SELECT 
					numItemCode
					,MIN(CustomerPartNo) AS CustomerPartNo
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=1]',2)                                                                          
				WITH                       
				(                                                                          
					CustomerPartNo VARCHAR(300), numItemCode NUMERIC(18,0)
				)
				GROUP BY
					numItemCode
			) AS TEMPCPN
			ON
				CPN.numCompanyId = @numCompany
				AND CPN.numItemCode = TEMPCPN.numItemCode
			WHERE
				CPN.numDomainID=@numDomainID
				AND CPN.numCompanyId = @numCompany
				AND LEN(ISNULL(TEMPCPN.CustomerPartNo,'')) > 0
				AND ISNULL(TEMPCPN.CustomerPartNo,'') <> '0'
			
			-- INSERT NEW CUSTOMER PART NO
			INSERT INTO CustomerPartNumber
			(
				numCompanyId, numDomainID, CustomerPartNo, numItemCode 
			)
			SELECT 
				@numCompany, @numDomainID, TEMPCPN.CustomerPartNo,TEMPCPN.numItemCode
			FROM
			(
				SELECT 
					numItemCode
					,MIN(CustomerPartNo) AS CustomerPartNo
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=1]',2)                                                                          
				WITH                       
				(                                                                          
					CustomerPartNo VARCHAR(300), numItemCode NUMERIC(18,0)
				)
				GROUP BY
					numItemCode
			) TEMPCPN 
			LEFT JOIN
				CustomerPartNumber CPN
			ON
				CPN.numCompanyId = @numCompany
				AND CPN.numItemCode = TEMPCPN.numItemCode
			WHERE
				CPN.CustomerPartNoID IS NULL
				AND LEN(ISNULL(TEMPCPN.CustomerPartNo,'')) > 0
				AND ISNULL(TEMPCPN.CustomerPartNo,'') <> '0'

			UPDATE 
				OpportunityItems                       
			SET 
				numItemCode=X.numItemCode,numOppId=@numOppID,numUnitHour=dbo.fn_UOMConversion(X.numUOM,X.numItemCode,@numDomainId,null) * X.numUnitHour,                      
				monPrice=x.monPrice,monTotAmount=x.monTotAmount,vcItemDesc=X.vcItemDesc,numWarehouseItmsID=X.numWarehouseItmsID,bitDropShip=X.DropShip,
				bitDiscountType=X.bitDiscountType,fltDiscount=X.fltDiscount,monTotAmtBefDiscount=X.monTotAmtBefDiscount,vcItemName=X.vcItemName,numUOMId=numUOM,
				bitWorkOrder=X.bitWorkOrder,numVendorWareHouse=X.numVendorWareHouse,numShipmentMethod=X.numShipmentMethod,numSOVendorId=X.numSOVendorId,
				numProjectId=X.numProjectID,numProjectStageId=X.numProjectStageID,numToWarehouseItemID = X.numToWarehouseItemID,vcAttributes=X.Attributes,
				numClassID=0,vcModelID=X.vcModelID,
				bitItemPriceApprovalRequired = X.bitItemPriceApprovalRequired,vcAttrValues=X.AttributeIDs,
				numPromotionID=X.numPromotionID,bitPromotionTriggered=X.bitPromotionTriggered,vcPromotionDetail=X.vcPromotionDetail,numSortOrder=X.numSortOrder,numCost=X.numCost
				,vcNotes=X.vcVendorNotes,vcInstruction=X.vcInstruction,bintCompliationDate=X.bintCompliationDate,numWOAssignedTo=X.numWOAssignedTo,ItemRequiredDate=X.ItemRequiredDate
				,bitMarkupDiscount=X.bitMarkupDiscount
			FROM 
			(
				SELECT 
					numoppitemtCode as numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,vcModelID,numWarehouseItmsID,DropShip,bitDiscountType,fltDiscount,
					monTotAmtBefDiscount,vcItemName,numUOM,bitWorkOrder,numVendorWareHouse,numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,numToWarehouseItemID,
					Attributes,bitItemPriceApprovalRequired,AttributeIDs,numPromotionID,bitPromotionTriggered,vcPromotionDetail,numSortOrder,ISNULL(numCost,0) numCost
					,ISNULL(vcVendorNotes,'') vcVendorNotes,vcInstruction,bintCompliationDate,numWOAssignedTo,ItemRequiredDate,bitMarkupDiscount
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=2]',2)
				WITH  
					(                      
						numoppitemtCode NUMERIC(9),numItemCode NUMERIC(9),numUnitHour FLOAT,monPrice DECIMAL(30,16),monTotAmount DECIMAL(20,5),vcItemDesc VARCHAR(1000), vcModelID VARCHAR(300),
						numWarehouseItmsID NUMERIC(9),DropShip BIT,bitDiscountType BIT,fltDiscount DECIMAL(30,16),monTotAmtBefDiscount DECIMAL(20,5),vcItemName VARCHAR(300),
						numUOM NUMERIC(9),bitWorkOrder BIT,numVendorWareHouse NUMERIC(9),numShipmentMethod NUMERIC(9),numSOVendorId NUMERIC(9),numProjectID NUMERIC(9),
						numProjectStageID numeric(9),numToWarehouseItemID NUMERIC(9),Attributes varchar(500),bitItemPriceApprovalRequired BIT,AttributeIDs VARCHAR(500),
						numPromotionID NUMERIC(18,0),bitPromotionTriggered BIT,vcPromotionDetail VARCHAR(2000),numSortOrder NUMERIC(18,0),numCost DECIMAL(20,5),vcVendorNotes VARCHAR(1000)
						,vcInstruction VARCHAR(1000),bintCompliationDate DATETIME,numWOAssignedTo NUMERIC(18,0),ItemRequiredDate DATETIME,bitMarkupDiscount BIT
					)
			)X 
			WHERE 
				numoppitemtCode=X.numOppItemID AND numOppID=@numOppID
				
			--INSERT KIT ITEM HAS ANOTHER KIT AS CHILD THEN SAVE CONFIGURATION OF KIT
			DECLARE @TempKitConfiguration TABLE
			(
				numOppItemID NUMERIC(18,0),
				numItemCode NUMERIC(18,0),
				KitChildItems VARCHAR(MAX)
			)

			INSERT INTO 
				@TempKitConfiguration
			SELECT
				numoppitemtCode,
				numItemCode,
				vcChildKitSelectedItems
			FROM
				OpportunityItems
			WHERE
				numOppId = @numOppID
				AND LEN(ISNULL(vcChildKitSelectedItems,'')) > 0

			--INSERT KIT ITEMS 
			IF (SELECT 
					COUNT(*) 
				FROM 
					OpportunityItems OI 
				LEFT JOIN
					WarehouseItems WI
				ON
					OI.numWarehouseItmsID = WI.numWarehouseItemID
				LEFT JOIN
					Warehouses W
				ON
					WI.numWarehouseID = W.numWarehouseID
				JOIN 
					ItemDetails ID 
				ON 
					OI.numItemCode=ID.numItemKitID 
				JOIN
					Item I
				ON
					ID.numChildItemID = I.numItemCode
				WHERE 
					OI.numOppId=@numOppId  
					AND ISNULL(OI.bitDropShip,0) = 0
					AND ISNULL(OI.numWarehouseItmsID,0) > 0
					AND I.charItemType = 'P'
					AND 1 = (CASE 
								WHEN (SELECT 
											COUNT(*) 
										FROM 
											@TempKitConfiguration t1
										CROSS APPLY
										(
											SELECT
												*
											FROM 
												dbo.Split((SELECT KitChildItems FROM @TempKitConfiguration t3 WHERE t3.numOppItemID=OI.numoppitemtCode AND t3.numItemCode=OI.numItemCode),',') 
										) AS t2 
										WHERE 
											SUBSTRING(items,0,CHARINDEX('-',items)) = '0') > 0 
								THEN 
									(CASE 
										WHEN I.numItemCode IN (SELECT 
																	SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items))
																FROM 
																	@TempKitConfiguration t1
																CROSS APPLY
																(
																	SELECT
																		*
																	FROM 
																		dbo.Split((SELECT KitChildItems FROM @TempKitConfiguration t3 WHERE t3.numOppItemID=OI.numoppitemtCode AND t3.numItemCode=OI.numItemCode),',') 
																) AS t2 
																WHERE 
																	SUBSTRING(items,0,CHARINDEX('-',items)) = '0') 
										THEN 1 
										ELSE 0 
									END)
								ELSE 1 END)
					AND ISNULL((SELECT TOP 1 numWarehouseItemID FROM WareHouseItems WHERE numItemID=ID.numChildItemID AND numWareHouseID = W.numWarehouseID),0) = 0) > 0
			BEGIN
				RAISERROR('SELECTED_WAREHOUSE_IS_NOT_AVAILABLE_FOR_ALL_CHILD_ITEMS',16,1)
			END

			           		        
			INSERT INTO OpportunityKitItems
			(
				numOppId,
				numOppItemID,
				numChildItemID,
				numWareHouseItemId,
				numQtyItemsReq,
				numQtyItemsReq_Orig,
				numUOMId,
				numQtyShipped,
				monAvgCost
			)
			SELECT
				@numOppId,
				OI.numoppitemtCode,
				ID.numChildItemID,
				(SELECT TOP 1 numWarehouseItemID FROM WareHouseItems WHERE numItemID=ID.numChildItemID AND WareHouseItems.numWareHouseID = W.numWarehouseID ORDER BY numWareHouseItemID),
				(ID.numQtyItemsReq * ISNULL(dbo.fn_UOMConversion(ID.numUOMId,ID.numChildItemID,@numDomainID,I.numBaseUnit),1)) * OI.numUnitHour,
				ID.numQtyItemsReq,
				ID.numUOMId,
				0,
				ISNULL(I.monAverageCost,0)
			FROM 
				OpportunityItems OI 
			LEFT JOIN
				WarehouseItems WI
			ON
				OI.numWarehouseItmsID = WI.numWarehouseItemID
			LEFT JOIN
				Warehouses W
			ON
				WI.numWarehouseID = W.numWarehouseID
			JOIN 
				ItemDetails ID 
			ON 
				OI.numItemCode=ID.numItemKitID 
			JOIN
				Item I
			ON
				ID.numChildItemID = I.numItemCode
			WHERE 
				OI.numOppId=@numOppId
				AND 1 = (CASE 
								WHEN (SELECT 
											COUNT(*) 
										FROM 
											@TempKitConfiguration t1
										CROSS APPLY
										(
											SELECT
												*
											FROM 
												dbo.Split((SELECT KitChildItems FROM @TempKitConfiguration t3 WHERE t3.numOppItemID=OI.numoppitemtCode AND t3.numItemCode=OI.numItemCode),',') 
										) AS t2 
										WHERE 
											SUBSTRING(items,0,CHARINDEX('-',items)) = '0') > 0 
								THEN 
									(CASE 
										WHEN I.numItemCode IN (SELECT 
																	SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items))
																FROM 
																	@TempKitConfiguration t1
																CROSS APPLY
																(
																	SELECT
																		*
																	FROM 
																		dbo.Split((SELECT KitChildItems FROM @TempKitConfiguration t3 WHERE t3.numOppItemID=OI.numoppitemtCode AND t3.numItemCode=OI.numItemCode),',') 
																) AS t2 
																WHERE 
																	SUBSTRING(items,0,CHARINDEX('-',items)) = '0') 
										THEN 1 
										ELSE 0 
									END)
								ELSE 1 END)

			IF (SELECT COUNT(*) FROM OPENXML (@hDocItem,'/NewDataSet/Item[bitHasKitAsChild=''true'']',2)) > 0
			BEGIN
				IF (SELECT 
						COUNT(*) 
					FROM 
					(
						SELECT 
							t1.numOppItemID
							,t1.numItemCode
							,SUBSTRING(items,0,CHARINDEX('-',items)) AS numKitItemID
							,SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)) As numKitChildItemID
						FROM 
							@TempKitConfiguration t1
						CROSS APPLY
						(
							SELECT
								*
							FROM 
								dbo.Split((SELECT KitChildItems FROM @TempKitConfiguration t3 WHERE t3.numOppItemID=t1.numOppItemID AND t3.numItemCode=t1.numItemCode),',') 
						) as t2
					) TempChildItems
					INNER JOIN
						OpportunityKitItems OKI
					ON
						OKI.numOppId=@numOppId
						AND OKI.numOppItemID=TempChildItems.numOppItemID
						AND OKI.numChildItemID=TempChildItems.numKitItemID
					INNER JOIN
						OpportunityItems OI
					ON
						OI.numItemCode = TempChildItems.numItemCode
						AND OI.numoppitemtcode = OKI.numOppItemID
					LEFT JOIN
						WarehouseItems WI
					ON
						OI.numWarehouseItmsID = WI.numWarehouseItemID
					LEFT JOIN
						Warehouses W
					ON
						WI.numWarehouseID = W.numWarehouseID
					INNER JOIN
						ItemDetails
					ON
						TempChildItems.numKitItemID = ItemDetails.numItemKitID
						AND TempChildItems.numKitChildItemID = ItemDetails.numChildItemID
					INNER JOIN
						Item 
					ON
						ItemDetails.numChildItemID = Item.numItemCode
					WHERE
						Item.charItemType = 'P'
						AND ISNULL((SELECT TOP 1 numWarehouseItemID FROM WareHouseItems WHERE numItemID=ItemDetails.numChildItemID AND numWareHouseID = W.numWarehouseID),0) = 0) > 0
				BEGIN
					RAISERROR('SELECTED_WAREHOUSE_IS_NOT_AVAILABLE_FOR_ALL_CHILD_ITEMS',16,1)
				END

				INSERT INTO OpportunityKitChildItems
				(
					numOppID,
					numOppItemID,
					numOppChildItemID,
					numItemID,
					numWareHouseItemId,
					numQtyItemsReq,
					numQtyItemsReq_Orig,
					numUOMId,
					numQtyShipped,
					monAvgCost
				)
				SELECT 
					@numOppID,
					OKI.numOppItemID,
					OKI.numOppChildItemID,
					ItemDetails.numChildItemID,
					(SELECT TOP 1 numWarehouseItemID FROM WareHouseItems WHERE numItemID=ItemDetails.numChildItemID AND WareHouseItems.numWareHouseID = W.numWarehouseID ORDER BY numWareHouseItemID),
					(ItemDetails.numQtyItemsReq * ISNULL(dbo.fn_UOMConversion(ItemDetails.numUOMId,ItemDetails.numChildItemID,@numDomainID,Item.numBaseUnit),1)) * OKI.numQtyItemsReq,
					ItemDetails.numQtyItemsReq,
					ItemDetails.numUOMId,
					0,
					ISNULL(Item.monAverageCost,0)
				FROM
				(
					SELECT 
						t1.numOppItemID
						,t1.numItemCode
						,SUBSTRING(items,0,CHARINDEX('-',items)) AS numKitItemID
						,SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)) As numKitChildItemID
					FROM 
						@TempKitConfiguration t1
					CROSS APPLY
					(
						SELECT
							*
						FROM 
							dbo.Split((SELECT KitChildItems FROM @TempKitConfiguration t3 WHERE t3.numOppItemID=t1.numOppItemID AND t3.numItemCode=t1.numItemCode),',') 
					) as t2
				) TempChildItems
				INNER JOIN
					OpportunityKitItems OKI
				ON
					OKI.numOppId=@numOppId
					AND OKI.numOppItemID=TempChildItems.numOppItemID
					AND OKI.numChildItemID=TempChildItems.numKitItemID
				INNER JOIN
					OpportunityItems OI
				ON
					OI.numItemCode = TempChildItems.numItemCode
					AND OI.numoppitemtcode = OKI.numOppItemID
				LEFT JOIN
					WarehouseItems WI
				ON
					OI.numWarehouseItmsID = WI.numWarehouseItemID
				LEFT JOIN
					Warehouses W
				ON
					WI.numWarehouseID = W.numWarehouseID
				INNER JOIN
					ItemDetails
				ON
					TempChildItems.numKitItemID = ItemDetails.numItemKitID
					AND TempChildItems.numKitChildItemID = ItemDetails.numChildItemID
				INNER JOIN
					Item 
				ON
					ItemDetails.numChildItemID = Item.numItemCode
			END

			-- UPDATE UOM OF OPPORTUNITY ITEMS HAVE NOT UOM SET WHILE EXPORTED FROM MARKETPLACE (MEANS TINTSOURCETYPE = 3)
			IF ISNULL(@tintSourceType,0) = 3
			BEGIN
				UPDATE 
					OI 
				SET 
					numUOMID = ISNULL(I.numSaleUnit,0), 
					numUnitHour = dbo.fn_UOMConversion(ISNULL(I.numSaleUnit,0),OI.numItemCode,@numDomainId,null) * OI.numUnitHour
				FROM 
					OpportunityItems OI
				JOIN 
					Item I 
				ON 
					OI.numItemCode = I.numItemCode
				WHERE 
					numOppId = @numOppID
					AND I.numDomainID = @numDomainId
			END	
                                     
			INSERT INTO OppWarehouseSerializedItem
			(
				numOppID,
				numWarehouseItmsDTLID,
				numOppItemID,
				numWarehouseItmsID
			)                      
			SELECT 
				@numOppID,
				X.numWarehouseItmsDTLID,
				X.numoppitemtCode,
				X.numWItmsID 
			FROM
			(                                                                          
				SELECT
					* 
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/SerialNo',2)                                                                          
				WITH                        
				(                                               
				numWarehouseItmsDTLID numeric(9),                      
				numoppitemtCode numeric(9),                      
				numWItmsID numeric(9)                                                     
				)
			)X                                    
                                   
			UPDATE 
				OppWarehouseSerializedItem                       
			SET 
				numOppItemID=X.numoppitemtCode                                
			FROM                       
			(
				SELECT 
					numWarehouseItmsDTLID AS DTLID,
					O.numoppitemtCode 
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/SerialNo',2)                                                                         
				WITH
				(
					numWarehouseItmsDTLID NUMERIC(9),numoppitemtCode NUMERIC(9),numWItmsID NUMERIC(9)
				)                                
				JOIN 
					OpportunityItems O 
				ON 
					O.numWarehouseItmsID=numWItmsID and O.numOppID=@numOppID
			)X                 
			WHERE 
				numOppID=@numOppID 
				AND numWarehouseItmsDTLID=X.DTLID      
    
			IF NOT (@tintCommitAllocation=2 AND @bitAllocateInventoryOnPickList=1)
			BEGIN
				EXEC USP_ManageSOWorkOrder @numDomainId,@numOppID,@strItems,@numUserCntID
			END
                     
			EXEC sp_xml_removedocument @hDocItem                                     
		END          
	END
	ELSE                                                                                                                          
	BEGIN

		DECLARE @tempAssignedTo AS NUMERIC(9) = NULL                                                    
		SELECT 
			@tintOppStatus = tintOppStatus,
			@tempAssignedTo=ISNULL(numAssignedTo,0) 
		FROM   
			OpportunityMaster 
		WHERE  
			numOppID = @numOppID

		IF @tempAssignedTo <> @numAssignedTo
		BEGIN
			IF (SELECT COUNT(*) FROM BizDocComission WHERE ISNULL(numOppID,0) = @numOppID AND ISNULL(bitCommisionPaid,0) = 1) > 0
			BEGIN
				RAISERROR('COMMISSION_PAID_AGAINST_BIZDOC',16,1)
			END
		END
 
		--REVERTING BACK THE WAREHOUSE ITEMS                  
		IF @tintOppStatus = 1 AND @tintCommitAllocation=1
		BEGIN               
			EXEC USP_RevertDetailsOpp @numOppID,0,@numUserCntID                  
		END                  

		IF LEN(ISNULL(@vcOppRefOrderNo,''))>0
		BEGIN
			UPDATE dbo.OpportunityBizDocs SET vcRefOrderNo=@vcOppRefOrderNo WHERE numOppId=@numOppID
		END
		
		IF EXISTS (SELECT 1 FROM dbo.OpportunityMaster WHERE numOppId = @numOppID AND tintOppType=1 AND isnull(tintOppStatus,0)=1 AND ISNULL(numStatus,0)!= @numStatus)
		BEGIN
			EXEC USP_ManageOpportunityAutomationQueue
					@numOppQueueID = 0, -- numeric(18, 0)
					@numDomainID = @numDomainID, -- numeric(18, 0)
					@numOppId = @numOppID, -- numeric(18, 0)
					@numOppBizDocsId = 0, -- numeric(18, 0)
					@numOrderStatus = @numStatus, -- numeric(18, 0)
					@numUserCntID = @numUserCntID, -- numeric(18, 0)
					@tintProcessStatus = 1, -- tinyint
					@tintMode = 1 -- TINYINT
		END 
						
		UPDATE   
			OpportunityMaster
		SET      
			vcPOppName = @vcPOppName,txtComments = ISNULL(@Comments,''),bitPublicFlag = @bitPublicFlag,numCampainID = @CampaignID,tintSource = @tintSource,tintSourceType=@tintSourceType,
			intPEstimatedCloseDate = @dtEstimatedCloseDate,numModifiedBy = @numUserCntID,bintModifiedDate = GETUTCDATE(),lngPConclAnalysis = @lngPConclAnalysis,monPAmount = @monPAmount,
			tintActive = @tintActive,numSalesOrPurType = @numSalesOrPurType,numStatus = @numStatus,vcOppRefOrderNo=@vcOppRefOrderNo,
			bintOppToOrder=(CASE WHEN @tintOppStatus=0 AND @DealStatus=1 THEN GETUTCDATE() ELSE bintOppToOrder END),bitDiscountType=@bitDiscountType,fltDiscount=@fltDiscount,
			bitBillingTerms=@bitBillingTerms,intBillingDays=@intBillingDays,bitInterestType=@bitInterestType,fltInterest=@fltInterest,tintTaxOperator=@tintTaxOperator,
			numDiscountAcntType=@numDiscountAcntType,vcCouponCode = @vcCouponCode,numPercentageComplete =@numPercentageComplete,bitUseShippersAccountNo = @bitUseShippersAccountNo,
			intUsedShippingCompany = @intUsedShippingCompany, numContactID=@numContactId,numShipmentMethod=@numShipmentMethod,numShippingService=@numShippingService,numPartner=@numPartner,numPartenerContact=@numPartenerContactId
		WHERE
			numOppId = @numOppID   
	   
		-- UPDATING IF ORGANIZATION IS ASSIGNED TO SOMEONE                                                      
		IF ( @tempAssignedTo <> @numAssignedTo AND @numAssignedTo <> '0' ) 
		BEGIN
			UPDATE
				OpportunityMaster 
			SET	
				numAssignedTo = @numAssignedTo, 
				numAssignedBy = @numUserCntID 
			WHERE 
				numOppId = @numOppID                                                    
		END                                                     
		ELSE IF(@numAssignedTo = 0) 
		BEGIN
			UPDATE 
				OpportunityMaster 
			SET 
				numAssignedTo = 0, 
				numAssignedBy = 0 
			WHERE
				numOppId = @numOppID
		END  
			                                                                    
		-- UPDATING OPP ITEMS
		IF CONVERT(VARCHAR(10),@strItems) <> '' AND @numOppID > 0                                                                         
		BEGIN
			EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems                                      
			   
			IF EXISTS 
			(
				SELECT 
					OI.numoppitemtCode
				FROM 
					OpportunityItems OI
				WHERE 
					numOppID=@numOppID 
					AND OI.numoppitemtCode NOT IN (SELECT numoppitemtCode FROM OPENXML (@hDocItem,'/NewDataSet/Item',2) WITH (numoppitemtCode NUMERIC(9)))
					AND ISNULL((SELECT COUNT(*) FROM OppWarehouseSerializedItem WHERE numOppId=@numOppID AND numOppItemID=OI.numoppitemtCode),0) > 0
			)
			BEGIN
				RAISERROR('SERIAL/LOT#_ASSIGNED_TO_ITEM_DELETED',16,1)
			END

			--DELETE CHILD ITEMS OF KIT WITHIN KIT
			DELETE FROM 
				OpportunityKitChildItems 
			WHERE 
				numOppID=@numOppID AND 
				numOppItemID NOT IN (SELECT ISNULL(numoppitemtCode,0) FROM OPENXML (@hDocItem,'/NewDataSet/Item',2) WITH (numoppitemtCode NUMERIC(9)))   

			--DELETE ITEMS
			DELETE FROM 
				OpportunityKitItems 
			WHERE 
				numOppID=@numOppID AND 
				numOppItemID NOT IN (SELECT ISNULL(numoppitemtCode,0) FROM OPENXML (@hDocItem,'/NewDataSet/Item',2) WITH (numoppitemtCode NUMERIC(9)))                      
			                            
			DELETE FROM 
				OpportunityItems 
			WHERE 
				numOppID=@numOppID 
				AND numoppitemtCode NOT IN (SELECT numoppitemtCode FROM OPENXML (@hDocItem,'/NewDataSet/Item',2) WITH (numoppitemtCode NUMERIC(9)))   
	          
			-- DELETE CORRESPONSING TIME AND EXPENSE ENTRIES OF DELETED ITEMS
			DELETE FROM
				TimeAndExpense
			WHERE
				numDomainID=@numDomainId
				AND numOppId=@numOppID
				AND numOppItemID NOT IN (SELECT numoppitemtCode FROM OPENXML (@hDocItem,'/NewDataSet/Item',2) WITH (numoppitemtCode NUMERIC(9))) 

			--INSERT NEW ADDED ITEMS                        
			INSERT INTO OpportunityItems
			(
				numOppId,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount,
				[vcItemName],[vcModelID],[vcManufacturer],vcPathForTImage,monVendorCost,numUOMId,bitWorkOrder,numVendorWareHouse,numToWarehouseItemID,numShipmentMethod,
				numSOVendorId,numProjectID,numProjectStageID,vcAttributes,numClassID,monAvgCost,bitItemPriceApprovalRequired,vcAttrValues,numPromotionID,bitPromotionTriggered
				,vcPromotionDetail,numSortOrder,numCost,vcNotes,vcInstruction,bintCompliationDate,numWOAssignedTo,bitMarkupDiscount,ItemReleaseDate,vcChildKitSelectedItems
			)
			SELECT 
				@numOppID,X.numItemCode,dbo.fn_UOMConversion(X.numUOM,numItemCode,@numDomainId,null) * x.numUnitHour,x.monPrice,x.monTotAmount,X.vcItemDesc,X.numWarehouseItmsID,
				X.ItemType,X.DropShip,X.bitDiscountType,X.fltDiscount,X.monTotAmtBefDiscount,X.vcItemName,(SELECT vcModelID FROM item WHERE numItemCode = X.numItemCode),
				(SELECT vcManufacturer FROM item WHERE numItemCode = X.numItemCode),
				(SELECT TOP 1 vcPathForTImage FROM dbo.ItemImages WHERE numItemCode = X.numItemCode AND BitDefault = 1 AND numDomainId = @numDomainId),
				(select isnull(VN.monCost,0) from Vendor VN INNER JOIN Item IT ON IT.numItemCode=VN.numItemCode and VN.numVendorID=IT.numVendorID where VN.numItemCode=X.numItemCode),
				X.numUOM,X.bitWorkOrder,X.numVendorWareHouse,X.numToWarehouseItemID,X.numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,Attributes,
				0,
				(SELECT (CASE WHEN ISNULL(I.bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(I.monAverageCost,0) END) FROM Item I WHERE I.numDomainId=@numDomainId AND I.numItemCode=X.numItemCode), X.bitItemPriceApprovalRequired, X.AttributeIDs,
				X.numPromotionID,X.bitPromotionTriggered,X.vcPromotionDetail,X.numSortOrder,ISNULL(X.numCost,0),ISNULL(vcVendorNotes,'')
				,X.vcInstruction,X.bintCompliationDate,X.numWOAssignedTo,X.bitMarkupDiscount,@dtItemRelease,ISNULL(KitChildItems,'')
			FROM
			(
				SELECT 
					*
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=1]',2)                                                                          
				WITH  
				(                      
					numoppitemtCode numeric(9),numItemCode numeric(9),numUnitHour FLOAT,monPrice DECIMAL(30,16),monTotAmount DECIMAL(20,5),vcItemDesc varchar(1000),                                    
					numWarehouseItmsID numeric(9),numToWarehouseItemID numeric(9),Op_Flag tinyint,ItemType varchar(30),DropShip bit,bitDiscountType bit,
					fltDiscount DECIMAL(30,16),monTotAmtBefDiscount DECIMAL(20,5),vcItemName VARCHAR(300),numUOM NUMERIC(9),bitWorkOrder BIT,numVendorWareHouse NUMERIC(9),
					numShipmentMethod NUMERIC(9),numSOVendorId numeric(9),numProjectID numeric(9),numProjectStageID numeric(9),Attributes varchar(500), 
					bitItemPriceApprovalRequired BIT,AttributeIDs VARCHAR(500),numPromotionID NUMERIC(18,0),bitPromotionTriggered BIT,vcPromotionDetail VARCHAR(2000)
					,numSortOrder NUMERIC(18,0),numCost DECIMAL(20,5),vcVendorNotes VARCHAR(1000),vcInstruction VARCHAR(1000),bintCompliationDate DATETIME,numWOAssignedTo NUMERIC(18,0)
					,bitMarkupDiscount BIT,KitChildItems VARCHAR(MAX)
				)
			)X 
			
			-- UPDATE EXISTING CUSTOMER PART NO
			UPDATE
				CPN
			SET
				CPN.CustomerPartNo = ISNULL(TEMPCPN.CustomerPartNo,'')
			FROM
				CustomerPartNumber CPN
			INNER JOIN
			(
				SELECT 
					numItemCode
					,MIN(CustomerPartNo) AS CustomerPartNo
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item',2)                                                                          
				WITH                       
				(                                                                          
					CustomerPartNo VARCHAR(300), numItemCode NUMERIC(18,0)
				)
				GROUP BY
					numItemCode
			) AS TEMPCPN
			ON
				CPN.numCompanyId = @numCompany
				AND CPN.numItemCode = TEMPCPN.numItemCode
			WHERE
				CPN.numDomainID=@numDomainID
				AND CPN.numCompanyId = @numCompany
				AND LEN(ISNULL(TEMPCPN.CustomerPartNo,'')) > 0
				AND ISNULL(TEMPCPN.CustomerPartNo,'') <> '0'
			
			-- INSERT NEW CUSTOMER PART NO
			INSERT INTO CustomerPartNumber
			(
				numCompanyId, numDomainID, CustomerPartNo, numItemCode 
			)
			SELECT 
				@numCompany, @numDomainID, TEMPCPN.CustomerPartNo,TEMPCPN.numItemCode
			FROM
			(
				SELECT 
					numItemCode
					,MIN(CustomerPartNo) AS CustomerPartNo
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item',2)                                                                          
				WITH                       
				(                                                                          
					CustomerPartNo VARCHAR(300), numItemCode NUMERIC(18,0)
				)
				GROUP BY
					numItemCode
			) TEMPCPN 
			LEFT JOIN
				CustomerPartNumber CPN
			ON
				CPN.numCompanyId = @numCompany
				AND CPN.numItemCode = TEMPCPN.numItemCode
			WHERE
				CPN.CustomerPartNoID IS NULL
				AND LEN(ISNULL(TEMPCPN.CustomerPartNo,'')) > 0
				AND ISNULL(TEMPCPN.CustomerPartNo,'') <> '0'
			
			-- UPDATE ITEMS
			UPDATE 
				OpportunityItems                       
			SET 
				numItemCode=X.numItemCode,numOppId=@numOppID,numUnitHour=dbo.fn_UOMConversion(X.numUOM,X.numItemCode,@numDomainId,null) * x.numUnitHour,                      
				monPrice=x.monPrice,monTotAmount=x.monTotAmount,vcItemDesc=X.vcItemDesc,vcModelID=X.vcModelID,numWarehouseItmsID=X.numWarehouseItmsID,numToWarehouseItemID = X.numToWarehouseItemID,             
				bitDropShip=X.DropShip,bitDiscountType=X.bitDiscountType,fltDiscount=X.fltDiscount,monTotAmtBefDiscount=X.monTotAmtBefDiscount,vcItemName=X.vcItemName,
				numUOMId=numUOM,numVendorWareHouse=X.numVendorWareHouse,numShipmentMethod=X.numShipmentMethod,numSOVendorId=X.numSOVendorId,numProjectId=X.numProjectID,
				numProjectStageId=X.numProjectStageID,vcAttributes=X.Attributes,bitItemPriceApprovalRequired=X.bitItemPriceApprovalRequired,bitWorkOrder=X.bitWorkOrder,vcAttrValues=AttributeIDs,
				numPromotionID=X.numPromotionID,bitPromotionTriggered=X.bitPromotionTriggered,vcPromotionDetail=X.vcPromotionDetail,numSortOrder=X.numSortOrder,numCost=X.numCost,vcNotes=vcVendorNotes
				,bitMarkupDiscount=X.bitMarkupDiscount,ItemRequiredDate=X.ItemRequiredDate
			FROM 
			(
				SELECT 
					numoppitemtCode as numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,vcModelID,numWarehouseItmsID,numToWarehouseItemID,DropShip,bitDiscountType,
					fltDiscount,monTotAmtBefDiscount,vcItemName,numUOM,numVendorWareHouse,numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,Attributes,
					bitItemPriceApprovalRequired,bitWorkOrder,AttributeIDs,numPromotionID,bitPromotionTriggered,vcPromotionDetail,numSortOrder,ISNULL(numCost,0) AS numCost,ISNULL(vcVendorNotes,'') vcVendorNotes,
					ItemRequiredDate,bitMarkupDiscount
				FROM 
					OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=2]',2)                                                                
				WITH  
				(                      
					numoppitemtCode NUMERIC(9),numItemCode NUMERIC(9),numUnitHour FLOAT,monPrice DECIMAL(30,16),monTotAmount DECIMAL(20,5),vcItemDesc VARCHAR(1000),vcModelID VARCHAR(300), numWarehouseItmsID NUMERIC(9), 
					numToWarehouseItemID NUMERIC(18,0),DropShip BIT,bitDiscountType BIT,fltDiscount DECIMAL(30,16),monTotAmtBefDiscount DECIMAL(20,5),vcItemName VARCHAR(300),numUOM NUMERIC(9),
					numVendorWareHouse NUMERIC(9),numShipmentMethod NUMERIC(9),numSOVendorId NUMERIC(9),numProjectID NUMERIC(9),numProjectStageID NUMERIC(9),Attributes VARCHAR(500),
					bitItemPriceApprovalRequired BIT, bitWorkOrder BIT,AttributeIDs VARCHAR(500),numPromotionID NUMERIC(18,0),bitPromotionTriggered BIT,vcPromotionDetail VARCHAR(2000),numSortOrder NUMERIC(18,0),numCost DECIMAL(20,5),vcVendorNotes VARCHAR(1000),
					ItemRequiredDate DATETIME, bitMarkupDiscount BIT
				)
			)X 
			WHERE 
				numoppitemtCode=X.numOppItemID AND numOppId=@numOppID

			--INSERT KIT ITEM HAS ANOTHER KIT AS CHILD THEN SAVE CONFIGURATION OF KIT
			DELETE FROM @TempKitConfiguration

			INSERT INTO @TempKitConfiguration
			SELECT
				numoppitemtCode,
				numItemCode,
				vcChildKitSelectedItems
			FROM
				OpportunityItems OI
			WHERE
				numOppId = @numOppID
				AND LEN(ISNULL(vcChildKitSelectedItems,'')) > 0
				AND OI.numoppitemtCode NOT IN (SELECT ISNULL(numOppItemID,0) FROM OpportunityKitItems OKI WHERE OKI.numOppId=@numOppID)

			--INSERT NEW ADDED KIT ITEMS
			IF (SELECT 
					COUNT(*) 
				FROM 
					OpportunityItems OI 
				LEFT JOIN
					WarehouseItems WI
				ON
					OI.numWarehouseItmsID = WI.numWarehouseItemID
				LEFT JOIN
					Warehouses W
				ON
					WI.numWarehouseID = W.numWarehouseID
				JOIN 
					ItemDetails ID 
				ON 
					OI.numItemCode=ID.numItemKitID 
				JOIN
					Item I
				ON
					ID.numChildItemID = I.numItemCode
				WHERE 
					OI.numOppId=@numOppId  
					AND ISNULL(OI.bitDropShip,0) = 0
					AND ISNULL(OI.numWarehouseItmsID,0) > 0
					AND I.charItemType = 'P'
					AND OI.numoppitemtCode NOT IN (SELECT ISNULL(numOppItemID,0) FROM OpportunityKitItems OKI WHERE OKI.numOppId=@numOppID)
					AND 1 = (CASE 
								WHEN (SELECT 
											COUNT(*) 
										FROM 
											@TempKitConfiguration t1
										CROSS APPLY
										(
											SELECT
												*
											FROM 
												dbo.Split((SELECT KitChildItems FROM @TempKitConfiguration t3 WHERE t3.numOppItemID=OI.numoppitemtCode AND t3.numItemCode=OI.numItemCode),',') 
										) AS t2 
										WHERE 
											SUBSTRING(items,0,CHARINDEX('-',items)) = '0') > 0 
								THEN 
									(CASE 
										WHEN I.numItemCode IN (SELECT 
																	SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items))
																FROM 
																	@TempKitConfiguration t1
																CROSS APPLY
																(
																	SELECT
																		*
																	FROM 
																		dbo.Split((SELECT KitChildItems FROM @TempKitConfiguration t3 WHERE t3.numOppItemID=OI.numoppitemtCode AND t3.numItemCode=OI.numItemCode),',') 
																) AS t2 
																WHERE 
																	SUBSTRING(items,0,CHARINDEX('-',items)) = '0') 
										THEN 1 
										ELSE 0 
									END)
								ELSE 1 END)
					AND ISNULL((SELECT TOP 1 numWarehouseItemID FROM WareHouseItems WHERE numItemID=ID.numChildItemID AND numWareHouseID = W.numWarehouseID),0) = 0) > 0
			BEGIN
				RAISERROR('SELECTED_WAREHOUSE_IS_NOT_AVAILABLE_FOR_ALL_CHILD_ITEMS',16,1)
			END
			             
			INSERT INTO OpportunityKitItems
			(
				numOppId,
				numOppItemID,
				numChildItemID,
				numWareHouseItemId,
				numQtyItemsReq,
				numQtyItemsReq_Orig,
				numUOMId,
				numQtyShipped,
				monAvgCost
			)
			SELECT
				@numOppId,
				OI.numoppitemtCode,
				ID.numChildItemID,
				(SELECT TOP 1 numWarehouseItemID FROM WareHouseItems WHERE numItemID=ID.numChildItemID AND WareHouseItems.numWareHouseID = W.numWarehouseID ORDER BY numWareHouseItemID),
				(ID.numQtyItemsReq * ISNULL(dbo.fn_UOMConversion(ID.numUOMId,ID.numChildItemID,@numDomainID,I.numBaseUnit),1)) * OI.numUnitHour,
				ID.numQtyItemsReq,
				ID.numUOMId,
				0,
				ISNULL(I.monAverageCost,0)
			FROM 
				OpportunityItems OI 
			LEFT JOIN
				WarehouseItems WI
			ON
				OI.numWarehouseItmsID = WI.numWarehouseItemID
			LEFT JOIN
				Warehouses W
			ON
				WI.numWarehouseID = W.numWarehouseID
			JOIN 
				ItemDetails ID 
			ON 
				OI.numItemCode=ID.numItemKitID 
			JOIN
				Item I
			ON
				ID.numChildItemID = I.numItemCode
			WHERE 
				OI.numOppId=@numOppId
				AND 1 = (CASE 
								WHEN (SELECT 
											COUNT(*) 
										FROM 
											@TempKitConfiguration t1
										CROSS APPLY
										(
											SELECT
												*
											FROM 
												dbo.Split((SELECT KitChildItems FROM @TempKitConfiguration t3 WHERE t3.numOppItemID=OI.numoppitemtCode AND t3.numItemCode=OI.numItemCode),',') 
										) AS t2 
										WHERE 
											SUBSTRING(items,0,CHARINDEX('-',items)) = '0') > 0 
								THEN 
									(CASE 
										WHEN I.numItemCode IN (SELECT 
																	SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items))
																FROM 
																	@TempKitConfiguration t1
																CROSS APPLY
																(
																	SELECT
																		*
																	FROM 
																		dbo.Split((SELECT KitChildItems FROM @TempKitConfiguration t3 WHERE t3.numOppItemID=OI.numoppitemtCode AND t3.numItemCode=OI.numItemCode),',') 
																) AS t2 
																WHERE 
																	SUBSTRING(items,0,CHARINDEX('-',items)) = '0') 
										THEN 1 
										ELSE 0 
									END)
								ELSE 1 END)
				AND OI.numoppitemtCode NOT IN (SELECT ISNULL(numOppItemID,0) FROM OpportunityKitItems OKI WHERE OKI.numOppId=@numOppID) 
		
			IF (SELECT COUNT(*) FROM OPENXML (@hDocItem,'/NewDataSet/Item[bitHasKitAsChild=''true'']',2)) > 0
			BEGIN
				IF (SELECT 
						COUNT(*) 
					FROM 
					(
						SELECT 
							t1.numOppItemID,
							t1.numItemCode,
							SUBSTRING(items,0,CHARINDEX('-',items)) AS numKitItemID,
							SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)) As numKitChildItemID
						FROM 
							@TempKitConfiguration t1
						CROSS APPLY
						(
							SELECT
								*
							FROM 
								dbo.Split((SELECT KitChildItems FROM @TempKitConfiguration t3 WHERE t3.numOppItemID=t1.numOppItemID AND t3.numItemCode=t1.numItemCode),',') 
						) as t2
					) TempChildItems
					INNER JOIN
						OpportunityKitItems OKI
					ON
						OKI.numOppId=@numOppId
						AND OKI.numOppItemID = TempChildItems.numOppItemID
						AND OKI.numChildItemID=TempChildItems.numKitItemID
					INNER JOIN
						OpportunityItems OI
					ON
						OI.numItemCode = TempChildItems.numItemCode
						AND OI.numoppitemtcode = OKI.numOppItemID
					LEFT JOIN
						WarehouseItems WI
					ON
						OI.numWarehouseItmsID = WI.numWarehouseItemID
					LEFT JOIN
						Warehouses W
					ON
						WI.numWarehouseID = W.numWarehouseID
					INNER JOIN
						ItemDetails
					ON
						TempChildItems.numKitItemID = ItemDetails.numItemKitID
						AND TempChildItems.numKitChildItemID = ItemDetails.numChildItemID
					INNER JOIN
						Item 
					ON
						ItemDetails.numChildItemID = Item.numItemCode
					WHERE
						Item.charItemType = 'P'
						AND ISNULL((SELECT TOP 1 numWarehouseItemID FROM WareHouseItems WHERE numItemID=ItemDetails.numChildItemID AND numWareHouseID = W.numWarehouseID),0) = 0) > 0
				BEGIN
					RAISERROR('SELECTED_WAREHOUSE_IS_NOT_AVAILABLE_FOR_ALL_CHILD_ITEMS',16,1)
				END

				INSERT INTO OpportunityKitChildItems
				(
					numOppID,
					numOppItemID,
					numOppChildItemID,
					numItemID,
					numWareHouseItemId,
					numQtyItemsReq,
					numQtyItemsReq_Orig,
					numUOMId,
					numQtyShipped,
					monAvgCost
				)
				SELECT 
					@numOppID,
					OKI.numOppItemID,
					OKI.numOppChildItemID,
					ItemDetails.numChildItemID,
					(SELECT TOP 1 numWarehouseItemID FROM WareHouseItems WHERE numItemID=ItemDetails.numChildItemID AND WareHouseItems.numWareHouseID = W.numWarehouseID ORDER BY numWarehouseItemID),
					(ItemDetails.numQtyItemsReq * ISNULL(dbo.fn_UOMConversion(ItemDetails.numUOMId,ItemDetails.numChildItemID,@numDomainID,Item.numBaseUnit),1)) * OKI.numQtyItemsReq,
					ItemDetails.numQtyItemsReq,
					ItemDetails.numUOMId,
					0,
					ISNULL(Item.monAverageCost,0)
				FROM 
				(
					SELECT 
						t1.numOppItemID,
						t1.numItemCode,
						SUBSTRING(items,0,CHARINDEX('-',items)) AS numKitItemID,
						SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)) As numKitChildItemID
					FROM 
						@TempKitConfiguration t1
					CROSS APPLY
					(
						SELECT
							*
						FROM 
							dbo.Split((SELECT KitChildItems FROM @TempKitConfiguration t3 WHERE t3.numOppItemID=t1.numOppItemID AND t3.numItemCode=t1.numItemCode),',') 
					) as t2
				) TempChildItems
				INNER JOIN
					OpportunityKitItems OKI
				ON
					OKI.numOppId=@numOppId
					AND OKI.numOppItemID = TempChildItems.numOppItemID
					AND OKI.numChildItemID=TempChildItems.numKitItemID
				INNER JOIN
					OpportunityItems OI
				ON
					OI.numItemCode = TempChildItems.numItemCode
					AND OI.numoppitemtcode = OKI.numOppItemID
				LEFT JOIN
					WarehouseItems WI
				ON
					OI.numWarehouseItmsID = WI.numWarehouseItemID
				LEFT JOIN
					Warehouses W
				ON
					WI.numWarehouseID = W.numWarehouseID
				INNER JOIN
					ItemDetails
				ON
					TempChildItems.numKitItemID = ItemDetails.numItemKitID
					AND TempChildItems.numKitChildItemID = ItemDetails.numChildItemID
				INNER JOIN
					Item 
				ON
					ItemDetails.numChildItemID = Item.numItemCode
			END                                  
			
			--UPDATE KIT ITEMS                 
			UPDATE 
				OKI 
			SET 
				numQtyItemsReq = numQtyItemsReq_Orig * OI.numUnitHour
			FROM 
				OpportunityKitItems OKI 
			JOIN 
				OpportunityItems OI 
			ON 
				OKI.numOppItemID=OI.numoppitemtCode 
				AND OKI.numOppId=OI.numOppId  
			WHERE 
				OI.numOppId=@numOppId 

			-- UPDATE CHILD ITEMS OF KIT WITHIN KIT
			UPDATE
				OKCI
			SET
				OKCI.numQtyItemsReq = (OKI.numQtyItemsReq * OKCI.numQtyItemsReq_Orig)
			FROM
				OpportunityKitChildItems OKCI
			INNER JOIN
				OpportunityKitItems OKI
			ON
				OKCI.numOppID = @numOppId
				AND OKCI.numOppItemID = OKI.numOppItemID
				AND OKCI.numOppChildItemID = OKI.numOppChildItemID                     
			                                    
			IF NOT (@tintCommitAllocation=2 AND @bitAllocateInventoryOnPickList=1)
			BEGIN
				IF @tintCommitAllocation = 2
				BEGIN
					DELETE FROM WorkOrderDetails WHERE numWOId IN (SELECT numWOId FROM WorkOrder WHERE numOppId=@numOppID AND numWOStatus <> 23184)
					DELETE FROM WorkOrder WHERE numOppId=@numOppID AND numWOStatus <> 23184
				END

				EXEC USP_ManageSOWorkOrder @numDomainId,@numOppID,@strItems,@numUserCntID
			END
			   		                                                                      
			EXEC sp_xml_removedocument @hDocItem                                               
		END     

		IF @tintOppType=1 AND @tintOppStatus=1 --Sales Order
		BEGIN
			IF EXISTS (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId=@numOppID AND ISNULL(numWarehouseItmsID,0) > 0 AND ISNULL(bitDropShip,0) = 0 AND (ISNULL(numUnitHour,0) - ISNULL(numQtyShipped,0)) < 0)
			BEGIN
				RAISERROR('EDITED_ITEMS_QTY_IS_LESS_THEN_SHIPPED_QTY',16,1)
				RETURN
			END

			IF EXISTS (SELECT numoppitemtCode FROM OpportunityItems INNER JOIN WorkOrder ON OpportunityItems.numOppID=WorkOrder.numOppID AND OpportunityItems.numoppitemtCode=WorkOrder.numOppItemID AND ISNULL(WorkOrder.numParentWOID,0)=0 AND WorkOrder.numWOStatus=23184 AND ISNULL(OpportunityItems.numUnitHour,0) > ISNULL(WorkOrder.numQtyItemsReq,0) WHERE OpportunityItems.numOppID=@numOppID)
			BEGIN
				RAISERROR('EDITED_ITEMS_QTY_CAN_NOT_BE_GREATER_THEN_COMPLETED_WORK_ORDER',16,1)
				RETURN
			END
		END
		ELSE IF @tintOppType=2 AND @tintOppStatus=1 --Purchase Order
		BEGIN
			IF EXISTS (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId=@numOppID AND ISNULL(numWarehouseItmsID,0) > 0 AND ISNULL(bitDropShip,0) = 0 AND (ISNULL(numUnitHour,0) - ISNULL(numUnitHourReceived,0)) < 0)
			BEGIN
				RAISERROR('EDITED_ITEMS_QTY_IS_LESS_THEN_RECEIVED_QTY',16,1)
				RETURN
			END
		END

		--FOLLOWING DELETE STATEMENT IS COMMENTED BECUASE NOW WE ARE NOT ALLOWING USER TO EIDT SERIAL/LOT ITEM UNTILL USER REMOVED ASSIGNED SERIAL/LOT NUMBERS
	    --Delete previously added serial lot number added for opportunity
		--DELETE FROM OppWarehouseSerializedItem WHERE numOppID = @numOppID                                                         
	END

	IF @tintOppStatus = 1
	BEGIN
		IF (SELECT COUNT(*) FROM OpportunityItems WHERE numOppId=@numOppID AND ISNULL(numWarehouseItmsID,0) > 0 AND ISNULL(bitDropShip,0)=0) <>
			(SELECT COUNT(*) FROM OpportunityItems INNER JOIN WareHouseItems ON OpportunityItems.numWarehouseItmsID=WareHouseItems.numWareHouseItemID AND OpportunityItems.numItemCode=WareHouseItems.numItemID WHERE numOppId=@numOppID AND OpportunityItems.numWarehouseItmsID > 0 AND ISNULL(bitDropShip,0)=0)
		BEGIN
			RAISERROR('INVALID_ITEM_WAREHOUSES',16,1)
			RETURN
		END
	END

	IF(SELECT 
			COUNT(*) 
		FROM 
			OpportunityBizDocItems OBDI 
		INNER JOIN 
			OpportunityBizDocs OBD 
		ON 
			OBDI.numOppBizDocID=OBD.numOppBizDocsID 
		WHERE 
			OBD.numOppId=@numOppID AND OBDI.numOppItemID NOT IN (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId=@numOppID)) > 0
	BEGIN
		RAISERROR('ITEM_USED_IN_BIZDOC',16,1)
		RETURN
	END

	IF @tintOppType=1 AND @tintCommitAllocation=2 AND @bitAllocateInventoryOnPickList=1
	BEGIN
		IF(SELECT 
				COUNT(*)
			FROM
			(
				SELECT
					numOppItemID
					,SUM(numUnitHour) PackingSlipUnits
				FROM 
					OpportunityBizDocItems OBDI 
				INNER JOIN 
					OpportunityBizDocs OBD 
				ON 
					OBDI.numOppBizDocID=OBD.numOppBizDocsID 
				WHERE 
					OBD.numOppId=@numOppID
					AND OBD.numBizDocId=29397 --Picking Slip
				GROUP BY
					numOppItemID
			) AS TEMP
			INNER JOIN
				OpportunityItems OI
			ON
				TEMP.numOppItemID = OI.numoppitemtCode
			WHERE
				OI.numUnitHour < PackingSlipUnits
			) > 0
		BEGIN
			RAISERROR('ORDERED_QUANTITY_CAN_NOT_BE_LESS_THEN_PACKING_SLIP_QTY',16,1)
			RETURN
		END
	END

	SET @TotalAmount=0                                                           
	SELECT @TotalAmount = SUM(monTotAmount) FROM OpportunityItems WHERE numOppId=@numOppID                                                                          
  
	UPDATE OpportunityMaster SET monPamount=@TotalAmount WHERE numOppId=@numOppID

/*Added by chintan - While creating SO from PO it wasn't puting Item Qty to Allocation. this segment was being called from USP_OppSaveMilestone which No longer in use after changes anoop did. */     
--Updating the warehouse items              
            
			
			DECLARE @bitViolatePrice BIT
			DECLARE @intPendingApprovePending INT

			SET @bitIsInitalSalesOrder=(SELECT bitIsInitalSalesOrder FROM OpportunityMaster WHERE numOppId=@numOppID AND numDomainId=@numDomainID)
			--SET @bitViolatePrice=(SELECT bitMarginPriceViolated FROM Domain WHERE numDomainId=@numDomainID)
			IF EXISTS (SELECT numApprovalRuleID FROM ApprovalProcessItemsClassification WHERE numDomainId=@numDomainID AND ISNULL(bitMarginPriceViolated,0) = 1)
			BEGIN
				SET @bitViolatePrice = 1
			END
			
			IF(@bitViolatePrice=1 AND @bitIsInitalSalesOrder=1 AND @tintClickBtn = 1)
			BEGIN
				UPDATE OpportunityMaster SET tintOppStatus=@DealStatus WHERE numOppId=@numOppID
			END
			ELSE
			BEGIN
				UPDATE OpportunityMaster SET tintOppStatus = @DealStatus WHERE numOppId=@numOppID
				DECLARE @tintShipped AS TINYINT               
				SELECT @tintOppStatus=tintOppStatus,@tintOppType=tintOppType,@tintShipped=tintShipped from OpportunityMaster where numOppID=@numOppID               
				
				IF @tintOppStatus=1 AND @tintCommitAllocation=1             
				BEGIN         
					EXEC USP_UpdatingInventoryonCloseDeal @numOppID,@numUserCntID
				END  
				declare @tintCRMType as numeric(9)        
				declare @AccountClosingDate as datetime        

				select @numDivisionID=numDivisionID,@AccountClosingDate=bintAccountClosingDate from OpportunityMaster where numOppID=@numOppID         
				select @tintCRMType=tintCRMType from divisionmaster where numDivisionID =@numDivisionID 

				--When deal is won                                         
				IF @DealStatus = 1 
				BEGIN
					IF @AccountClosingDate IS NULL                   
					BEGIN
						UPDATE OpportunityMaster SET bintAccountClosingDate=GETUTCDATE() WHERE numOppID=@numOppID              
					END

					-- Promote Lead to Account when Sales/Purchase Order is created against it
					IF @tintCRMType=0 --Lead & Order
					BEGIN
						UPDATE 
							divisionmaster 
						SET 
							tintCRMType=2
							,bintLeadProm=getutcdate()
							,bintLeadPromBy=@numUserCntID
							,bintProsProm=getutcdate()
							,bintProsPromBy=@numUserCntID        
						WHERE 
							numDivisionID=@numDivisionID        
					END
					--Promote Prospect to Account
					ELSE IF @tintCRMType=1 
					BEGIN
						UPDATE 
							divisionmaster 
						SET 
							tintCRMType=2
							,bintProsProm=getutcdate()
							,bintProsPromBy=@numUserCntID        
						WHERE 
							numDivisionID=@numDivisionID        
					END
				END         
				--When Deal is Lost
				ELSE IF @DealStatus = 2
				BEGIN
					SELECT @AccountClosingDate=bintAccountClosingDate FROM OpportunityMaster WHERE numOppID=@numOppID         
					
					IF @AccountClosingDate IS NULL                   
					BEGIN
						UPDATE OpportunityMaster SET bintAccountClosingDate=GETUTCDATE() WHERE numOppID=@numOppID
					END
				END 
			END

			/*Added by chintan -  Biz usually automatically promotes the record from Lead/Prospect to Account When a Sales Opp/Order is made against it*/  
			-- Promote Lead to Prospect when Sales/Purchase Opp is created against it
			if @tintCRMType=0 AND @DealStatus = 0 --Lead & Open Opp
			begin        
				update divisionmaster set tintCRMType=1,bintLeadProm=getutcdate(),bintLeadPromBy=@numUserCntID,bintProsProm=getutcdate(),bintProsPromBy=@numUserCntID        
				where numDivisionID=@numDivisionID        
			end        

--Add/Update Address Details
DECLARE @vcStreet  varchar(100),@vcCity  varchar(50),@vcPostalCode  varchar(15),@vcCompanyName  varchar(100),@vcAddressName VARCHAR(50), @vcAltContact VARCHAR(200)      
DECLARE @numState  numeric(9),@numCountry  numeric(9), @numCompanyId  numeric(9), @numContact NUMERIC(18,0)
DECLARE @bitIsPrimary BIT, @bitAltContact BIT;
DECLARE @tintAddressOf TINYINT

select  @vcCompanyName=vcCompanyName,@numCompanyId=div.numCompanyID from CompanyInfo Com                            
join divisionMaster Div                            
on div.numCompanyID=com.numCompanyID                            
where div.numdivisionID=@numDivisionId

--Bill Address
IF @numBillAddressId>0
BEGIN
	SELECT 
		@vcStreet=ISNULL(vcStreet,'')
		,@vcCity=ISNULL(vcCity,'')
		,@vcPostalCode=ISNULL(vcPostalCode,'')
		,@numState=ISNULL(numState,0)
		,@numCountry=ISNULL(numCountry,0)
		,@bitIsPrimary=bitIsPrimary
		,@vcAddressName=vcAddressName
		,@tintAddressOf=ISNULL(tintAddressOf,0)
		,@numContact=numContact
		,@bitAltContact=bitAltContact
		,@vcAltContact=vcAltContact
    FROM 
		dbo.AddressDetails 
	WHERE 
		numDomainID = @numDomainID 
		AND numAddressID = @numBillAddressId

	IF @tintAddressOf = 2 AND @tintOppType = 2
	BEGIN
		SELECT
			@vcCompanyName = ISNULL(vcCompanyName,'')
			,@numCompanyId = ISNULL(CompanyInfo.numCompanyId,0)
		FROM
			AddressDetails
		INNER JOIN
			DivisionMaster 
		ON
			AddressDetails.numRecordID = DivisionMaster.numDivisionID
		INNER JOIN
			CompanyInfo
		ON
			DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
		WHERE 
			AddressDetails.numDomainID = @numDomainID 
			AND numAddressID = @numBillAddressId
	END

  	EXEC dbo.USP_UpdateOppAddress
	@numOppID = @numOppID, --  numeric(9, 0)
	@byteMode = 0, --  tinyint
	@vcStreet = @vcStreet, --  varchar(100)
	@vcCity = @vcCity, --  varchar(50)
	@vcPostalCode = @vcPostalCode, --  varchar(15)
	@numState = @numState, --  numeric(9, 0)
	@numCountry = @numCountry, --  numeric(9, 0)
	@vcCompanyName = @vcCompanyName, --  varchar(100)
	@numCompanyId = 0, --  numeric(9, 0)
	@vcAddressName = @vcAddressName,
	@bitCalledFromProcedure = 1,
	@numContact = @numContact,
	@bitAltContact = @bitAltContact,
	@vcAltContact = @vcAltContact
END
  
  
--Ship Address
IF @intUsedShippingCompany = 92 -- WILL CALL (PICK UP FROM Warehouse)
BEGIN
	IF EXISTS (SELECT numWareHouseID FROM Warehouses WHERE Warehouses.numDomainID = @numDomainID AND numWareHouseID = @numWillCallWarehouseID AND ISNULL(numAddressID,0) > 0)
	BEGIN
		SELECT  
			@vcStreet=ISNULL(vcStreet,'')
			,@vcCity=ISNULL(vcCity,'')
			,@vcPostalCode=ISNULL(AddressDetails.vcPostalCode,'')
			,@numState=ISNULL(numState,0)
			,@numCountry=ISNULL(numCountry,0)
			,@vcAddressName=vcAddressName
		FROM 
			dbo.Warehouses 
		INNER JOIN
			AddressDetails
		ON
			Warehouses.numAddressID = AddressDetails.numAddressID
		WHERE 
			Warehouses.numDomainID = @numDomainID 
			AND numWareHouseID = @numWillCallWarehouseID
	END
	ELSE
	BEGIN
	SELECT  
		@vcStreet=ISNULL(vcWStreet,'')
		,@vcCity=ISNULL(vcWCity,'')
		,@vcPostalCode=ISNULL(vcWPinCode,'')
		,@numState=ISNULL(numWState,0)
		,@numCountry=ISNULL(numWCountry,0)
		,@vcAddressName=''
	FROM 
		dbo.Warehouses 
	WHERE 
		numDomainID = @numDomainID 
		AND numWareHouseID = @numWillCallWarehouseID
	END

	UPDATE OpportunityMaster SET tintShipToType=2 WHERE numOppId=@numOppID
 
  	EXEC dbo.USP_UpdateOppAddress
	@numOppID = @numOppID, --  numeric(9, 0)
	@byteMode = 1, --  tinyint
	@vcStreet = @vcStreet, --  varchar(100)
	@vcCity = @vcCity, --  varchar(50)
	@vcPostalCode = @vcPostalCode, --  varchar(15)
	@numState = @numState, --  numeric(9, 0)
	@numCountry = @numCountry, --  numeric(9, 0)
	@vcCompanyName = '', --  varchar(100)
	@numCompanyId =0, --  numeric(9, 0)
	@vcAddressName = @vcAddressName,
	@bitCalledFromProcedure = 1,
	@numContact = 0,
	@bitAltContact = 0,
	@vcAltContact = ''
END
ELSE IF @numShipAddressId>0
BEGIN
	SELECT
		@vcStreet=ISNULL(vcStreet,'')
		,@vcCity=ISNULL(vcCity,'')
		,@vcPostalCode=ISNULL(vcPostalCode,'')
		,@numState=ISNULL(numState,0)
		,@numCountry=ISNULL(numCountry,0)
		,@bitIsPrimary=bitIsPrimary
		,@vcAddressName=vcAddressName
		,@tintAddressOf=ISNULL(tintAddressOf,0)
		,@numContact=numContact
		,@bitAltContact=bitAltContact
		,@vcAltContact=vcAltContact
    FROM  
		dbo.AddressDetails 
	WHERE 
		numDomainID = @numDomainID 
		AND numAddressID = @numShipAddressId
 

	IF @tintAddressOf = 2 AND @tintOppType = 2
	BEGIN
		SELECT
			@vcCompanyName = ISNULL(vcCompanyName,'')
			,@numCompanyId = ISNULL(CompanyInfo.numCompanyId,0)
		FROM
			AddressDetails
		INNER JOIN
			DivisionMaster 
		ON
			AddressDetails.numRecordID = DivisionMaster.numDivisionID
		INNER JOIN
			CompanyInfo
		ON
			DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
		WHERE 
			AddressDetails.numDomainID = @numDomainID 
			AND numAddressID = @numShipAddressId
	END


  	EXEC dbo.USP_UpdateOppAddress
	@numOppID = @numOppID, --  numeric(9, 0)
	@byteMode = 1, --  tinyint
	@vcStreet = @vcStreet, --  varchar(100)
	@vcCity = @vcCity, --  varchar(50)
	@vcPostalCode = @vcPostalCode, --  varchar(15)
	@numState = @numState, --  numeric(9, 0)
	@numCountry = @numCountry, --  numeric(9, 0)
	@vcCompanyName = @vcCompanyName, --  varchar(100)
	@numCompanyId =@numCompanyId, --  numeric(9, 0)
	@vcAddressName = @vcAddressName,
	@bitCalledFromProcedure = 1,
	@numContact = @numContact,
	@bitAltContact = @bitAltContact,
	@vcAltContact = @vcAltContact
END

-- IMPORTANT:KEEP THIS BELOW ADDRESS UPDATE LOGIC
-- GET TAX APPLICABLE TO DIVISION WHEN ORDER IS CREATED AFTER THAT NOT NEED TO CHANGE IT
IF @bitNewOrder = 1
BEGIN
	--INSERT TAX FOR DIVISION   
	IF @tintOppType=1 OR (@tintOppType=2 and (select isnull(bitPurchaseTaxCredit,0) from Domain where numDomainId=@numDomainId)=1)
	BEGIN
		INSERT INTO dbo.OpportunityMasterTaxItems 
		(
			numOppId,
			numTaxItemID,
			fltPercentage,
			tintTaxType,
			numTaxID
		) 
		SELECT 
			@numOppID,
			TI.numTaxItemID,
			TEMPTax.decTaxValue,
			TEMPTax.tintTaxType,
			0
		FROM 
			TaxItems TI 
		JOIN 
			DivisionTaxTypes DTT 
		ON 
			TI.numTaxItemID = DTT.numTaxItemID
		CROSS APPLY
		(
			SELECT
				decTaxValue,
				tintTaxType
			FROM
				dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,TI.numTaxItemID,@numOppId,0,NULL)
		) AS TEMPTax
		WHERE 
			DTT.numDivisionID=@numDivisionId 
			AND DTT.bitApplicable=1
		UNION 
		SELECT 
			@numOppID,
			0,
			TEMPTax.decTaxValue,
			TEMPTax.tintTaxType,
			0
		FROM 
			dbo.DivisionMaster 
		CROSS APPLY
		(
			SELECT
				decTaxValue,
				tintTaxType
			FROM
				dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,0,@numOppId,1,NULL)
		) AS TEMPTax
		WHERE 
			bitNoTax=0 
			AND numDivisionID=@numDivisionID	
		UNION 
		SELECT
			@numOppId
			,1
			,decTaxValue
			,tintTaxType
			,numTaxID
		FROM
			dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,1,@numOppId,1,NULL)		
	END	
END

--INSERT TAX FOR DIVISION   
IF @tintOppType=1 OR (@tintOppType=2 and (select isnull(bitPurchaseTaxCredit,0) from Domain where numDomainId=@numDomainId)=1)
BEGIN
--Delete Tax for Opportunity Items if item deleted 
DELETE FROM OpportunityItemsTaxItems WHERE numOppId=@numOppId AND numOppItemID NOT IN (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId=@numOppID)

--Insert Tax for Opportunity Items
INSERT INTO dbo.OpportunityItemsTaxItems 
(
	numOppId,
	numOppItemID,
	numTaxItemID,
	numTaxID
) 
SELECT 
	@numOppId,
	OI.numoppitemtCode,
	TI.numTaxItemID,
	0
FROM 
	dbo.OpportunityItems OI 
JOIN 
	dbo.ItemTax IT 
ON 
	OI.numItemCode=IT.numItemCode 
JOIN
	TaxItems TI 
ON 
	TI.numTaxItemID = IT.numTaxItemID 
WHERE 
	OI.numOppId=@numOppID 
	AND IT.bitApplicable=1 
	AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)
UNION
SELECT 
	@numOppId,
	OI.numoppitemtCode,
	0,
	0
FROM 
	dbo.OpportunityItems OI 
JOIN 
	dbo.Item I 
ON 
	OI.numItemCode=I.numItemCode
WHERE 
	OI.numOppId=@numOppID 
	AND I.bitTaxable=1 
	AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)
UNION
SELECT
	@numOppId,
	OI.numoppitemtCode,
	1,
	TD.numTaxID
FROM
	dbo.OpportunityItems OI 
INNER JOIN
	ItemTax IT
ON
	IT.numItemCode = OI.numItemCode
INNER JOIN
	TaxDetails TD
ON
	TD.numTaxID = IT.numTaxID
	AND TD.numDomainId = @numDomainId
WHERE
	OI.numOppId = @numOppID
	AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)

END
  
 UPDATE OpportunityMaster SET monDealAmount=dbo.GetDealAmount(@numOppID,getdate(),0) WHERE numOppId=@numOppID

	IF @tintOppType=1 AND @tintOppStatus=1 
	BEGIN
		EXEC USP_SalesFulfillmentQueue_Save @numDomainID,@numUserCntID,@numOppID,@numStatus
		EXEC USP_OpportunityMaster_CreateBackOrderPO @numDomainID,@numUserCntID,@numOppID
	END
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SET @numOppID = 0

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ShippingPackageType_GetByShippingCompany')
DROP PROCEDURE USP_ShippingPackageType_GetByShippingCompany
GO
CREATE PROCEDURE [dbo].[USP_ShippingPackageType_GetByShippingCompany]  
	@numShippingCompanyID NUMERIC(18,0)
	,@bitEndicia BIT
AS  
BEGIN  
	SELECT
		ID
		,numNsoftwarePackageTypeID
		,vcPackageName
	FROM
		ShippingPackageType
	WHERE
		(numShippingCompanyID=ISNULL(numShippingCompanyID,0) OR @numShippingCompanyID = 0)
		AND 1 = (CASE 
					WHEN (numShippingCompanyID = 88 OR numShippingCompanyID=91) THEN 1 
					ELSE (CASE WHEN bitEndicia = @bitEndicia THEN 1 ELSE 0 END) 
				END)
END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- exec USP_OPPItemDetails @numItemCode=31455,@OpportunityId=12387,@numDomainId=110
--created by anooop jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ShippingReport_Save')
DROP PROCEDURE USP_ShippingReport_Save
GO
CREATE PROCEDURE [dbo].[USP_ShippingReport_Save]
	@numDomainId NUMERIC(18,0)
	,@numUserCntID NUMERIC(18,0)
	,@numOppID NUMERIC(18,0)
    ,@numOppBizDocId NUMERIC(18,0)
AS
BEGIN
BEGIN TRY
BEGIN TRANSACTION
	
	DECLARE @numShippingReportID NUMERIC(18,0)
	DECLARE @IsCOD BIT,@IsDryIce BIT,@IsHoldSaturday BIT,@IsHomeDelivery BIT,@IsInsideDelevery BIT,@IsInsidePickup BIT,@IsReturnShipment BIT,@IsSaturdayDelivery BIT,@IsSaturdayPickup BIT,@IsAdditionalHandling BIT,@IsLargePackage BIT
	DECLARE @numCODAmount NUMERIC(18,2),@vcCODType VARCHAR(50),@numTotalInsuredValue NUMERIC(18,2),@vcDeliveryConfirmation VARCHAR(1000),@vcDescription VARCHAR(MAX),@numTotalCustomsValue NUMERIC(18,2)
	DECLARE @tintSignatureType TINYINT
	DECLARE @numOrderShippingCompany NUMERIC(18,0)
	DECLARE @numOrderShippingService NUMERIC(18,0)
	DECLARE @numDivisionShippingCompany NUMERIC(18,0)
    DECLARE @numDivisionShippingService NUMERIC(18,0)

	SELECT
		@IsCOD = IsCOD
		,@IsHomeDelivery = IsHomeDelivery
		,@IsInsideDelevery = IsInsideDelevery
		,@IsInsidePickup = IsInsidePickup
		,@IsSaturdayDelivery = IsSaturdayDelivery
		,@IsSaturdayPickup = IsSaturdayPickup
		,@IsAdditionalHandling = IsAdditionalHandling
		,@IsLargePackage=IsLargePackage
		,@vcCODType = vcCODType
		,@vcDeliveryConfirmation = vcDeliveryConfirmation
		,@vcDescription = vcDescription
		,@tintSignatureType = CAST(ISNULL(vcSignatureType,'0') AS TINYINT)
		,@numOrderShippingCompany = ISNULL(intUsedShippingCompany,0)
		,@numOrderShippingService = ISNULL(numShippingService,0)
		,@numDivisionShippingCompany = ISNULL(intShippingCompany,0)
		,@numDivisionShippingService = ISNULL(numDefaultShippingServiceID,0)
	FROM
		OpportunityMaster
	INNER JOIN
		DivisionMaster
	ON
		OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
	LEFT JOIN
		DivisionMasterShippingConfiguration
	ON
		DivisionMasterShippingConfiguration.numDomainID=@numDomainId
		AND DivisionMaster.numDivisionID = DivisionMasterShippingConfiguration.numDivisionID
	WHERE
		OpportunityMaster.numDomainId = @numDomainId
		AND DivisionMaster.numDomainID=@numDomainId
		AND OpportunityMaster.numOppId = @numOppID

	DECLARE @vcFromName VARCHAR(1000)
    DECLARE @vcFromCompany VARCHAR(1000)
    DECLARE @vcFromPhone VARCHAR(100)
    DECLARE @vcFromAddressLine1 VARCHAR(50)
    DECLARE @vcFromAddressLine2 VARCHAR(50) = ''
    DECLARE @vcFromCity VARCHAR(50)
	DECLARE @vcFromState VARCHAR(50)
    DECLARE @vcFromZip VARCHAR(50)
    DECLARE @vcFromCountry VARCHAR(50)
	DECLARE @bitFromResidential BIT = 0

	DECLARE @vcToName VARCHAR(1000)
    DECLARE @vcToCompany VARCHAR(1000)
    DECLARE @vcToPhone VARCHAR(100)
    DECLARE @vcToAddressLine1 VARCHAR(50)
    DECLARE @vcToAddressLine2 VARCHAR(50) = ''
	DECLARE @vcToCity VARCHAR(50)
	DECLARE @vcToState VARCHAR(50)
    DECLARE @vcToZip VARCHAR(50)
    DECLARE @vcToCountry VARCHAR(50)
    DECLARE @bitToResidential BIT = 0

	-- GET FROM ADDRESS
	SELECT
		@vcFromName=vcName
		,@vcFromCompany=vcCompanyName
		,@vcFromPhone=vcPhone
		,@vcFromAddressLine1=vcStreet
		,@vcFromCity=vcCity
		,@vcFromState=CAST(vcState AS VARCHAR(18))
		,@vcFromZip=vcZipCode
		,@vcFromCountry=CAST(vcCountry AS VARCHAR(18))
	FROM
		dbo.fn_GetShippingReportAddress(1,@numDomainId,@numOppID)

	-- GET TO ADDRESS
	SELECT
		@vcToName=vcName
		,@vcToCompany=vcCompanyName
		,@vcToPhone=vcPhone
		,@vcToAddressLine1=vcStreet
		,@vcToCity=vcCity
		,@vcToState=CAST(vcState AS VARCHAR(18))
		,@vcToZip=vcZipCode
		,@vcToCountry=CAST(vcCountry AS VARCHAR(18))
	FROM
		dbo.fn_GetShippingReportAddress(2,@numDomainId,@numOppID)

	DECLARE @numShippingCompany NUMERIC(18,0)
	DECLARE @numShippingService NUMERIC(18,0)
	

	-- FIRST CHECK IF ORDER LEVEL SHIPPING FIELDS HAVE VALUES
	IF ISNULL(@numOrderShippingCompany,0) > 0
	BEGIN
		IF ISNULL(@numOrderShippingService,0) = 0
		BEGIN
			SET @numOrderShippingService = CASE @numOrderShippingCompany 
									WHEN 91 THEN 15 --FEDEX
									WHEN 88 THEN 43 --UPS
									WHEN 90 THEN 70 --USPS
								 END
		END

		SET @numShippingCompany = @numOrderShippingCompany
		SET @numShippingService = @numOrderShippingService
	END
	ELSE IF ISNULL(@numDivisionShippingCompany,0) > 0 -- IF DIVISION SHIPPIGN SETTING AVAILABLE
	BEGIN
		IF ISNULL(@numDivisionShippingService,0) = 0
		BEGIN
			SET @numDivisionShippingService = CASE @numDivisionShippingCompany 
									WHEN 91 THEN 15 --FEDEX
									WHEN 88 THEN 43 --UPS
									WHEN 90 THEN 70 --USPS
								 END
		END

		SET @numShippingCompany = @numDivisionShippingCompany
		SET @numShippingService = @numDivisionShippingService
	END
	ELSE -- USE DOMAIN DEFAULT
	BEGIN
		SELECT @numShippingCompany=ISNULL(numShipCompany,0) FROM Domain WHERE numDomainId=@numDomainId
		
		IF @numShippingCompany <> 91 OR @numShippingCompany <> 88 OR @numShippingCompany <> 90
		BEGIN
			SET @numShippingCompany = 91
		END

		SET @numShippingService = CASE @numShippingCompany 
									WHEN 91 THEN 15 --FEDEX
									WHEN 88 THEN 43 --UPS
									WHEN 90 THEN 70 --USPS
								 END
	END



	DECLARE @bitUseBizdocAmount BIT

	SELECT @bitUseBizdocAmount=bitUseBizdocAmount,@numTotalCustomsValue=ISNULL(numTotalInsuredValue,0),@numTotalInsuredValue=ISNULL(numTotalCustomsValue,0) FROM Domain WHERE numDomainId=@numDomainId

	IF @bitUseBizdocAmount = 1
	BEGIN
		SELECT @numTotalCustomsValue=ISNULL(monDealAmount,0),@numTotalInsuredValue=ISNULL(monDealAmount,0),@numCODAmount=ISNULL(monDealAmount,0) FROM OpportunityBizDocs WHERE numOppBizDocsId=@numOppBizDocId 
	END
	ELSE
	BEGIN
		SELECT @numCODAmount=ISNULL(monDealAmount,0) FROM OpportunityBizDocs WHERE numOppBizDocsId=@numOppBizDocId 
	END

	-- MAKE ENTRY IN ShippingReport TABLE
	INSERT INTO ShippingReport
	(
		numDomainId,numCreatedBy,dtCreateDate,numOppID,numOppBizDocId,numShippingCompany,vcValue2
		,vcFromName,vcFromCompany,vcFromPhone,vcFromAddressLine1,vcFromAddressLine2,vcFromCity,vcFromState,vcFromZip,vcFromCountry,bitFromResidential
		,vcToName,vcToCompany,vcToPhone,vcToAddressLine1,vcToAddressLine2,vcToCity,vcToState,vcToZip,vcToCountry,bitToResidential
		,IsCOD,IsDryIce,IsHoldSaturday,IsHomeDelivery,IsInsideDelevery,IsInsidePickup,IsReturnShipment,IsSaturdayDelivery,IsSaturdayPickup
		,numCODAmount,vcCODType,numTotalInsuredValue,IsAdditionalHandling,IsLargePackage,vcDeliveryConfirmation,vcDescription,numTotalCustomsValue,tintSignatureType
	)
	VALUES
	(
		@numDomainId,@numUserCntID,GETUTCDATE(),@numOppID,@numOppBizDocId,@numShippingCompany,CAST(@numShippingService AS VARCHAR)
		,@vcFromName,@vcFromCompany,@vcFromPhone,@vcFromAddressLine1,@vcFromAddressLine2,@vcFromCity,@vcFromState,@vcFromZip,@vcFromCountry,@bitFromResidential
		,@vcToName,@vcToCompany,@vcToPhone,@vcToAddressLine1,@vcToAddressLine2,@vcToCity,@vcToState,@vcToZip,@vcToCountry,@bitToResidential
		,@IsCOD,@IsDryIce,@IsHoldSaturday,@IsHomeDelivery,@IsInsideDelevery,@IsInsidePickup,@IsReturnShipment,@IsSaturdayDelivery,@IsSaturdayPickup
		,@numCODAmount,@vcCODType,@numTotalInsuredValue,@IsAdditionalHandling,@IsLargePackage,@vcDeliveryConfirmation,@vcDescription,@numTotalCustomsValue,@tintSignatureType
	)

	SET @numShippingReportID = SCOPE_IDENTITY()

	-- GET CONTAINER BY WAREHOUSE AND NOOFITEMSCANBEADDED
	DECLARE @TEMPContainer TABLE
	(
		ID INT IDENTITY(1,1)
		,numContainer NUMERIC(18,0)
		,numWareHouseID NUMERIC(18,0)
		,numContainerQty NUMERIC(18,0)
		,numNoItemIntoContainer NUMERIC(18,0)
		,numTotalContainer NUMERIC(18,0)
		,numUsedContainer NUMERIC(18,0)
		,bitItemAdded BIT
		,fltWeight FLOAT
		,fltHeight FLOAT
		,fltWidth FLOAT
		,fltLength FLOAT
	)

	INSERT INTO 
		@TEMPContainer
	SELECT
		T1.numContainer
		,T1.numWareHouseID
		,T1.numNoItemIntoContainer
		,T1.numNoItemIntoContainer
		,T1.numTotalContainer
		,0 AS numTotalContainer
		,0 AS bitItemAdded
		,T2.fltWeight
		,T2.fltHeight
		,T2.fltWidth
		,T2.fltLength
	FROM
	(
		SELECT 
			I.numContainer
			,WI.numWareHouseID
			,I.numNoItemIntoContainer
			,CEILING(SUM(OBI.numUnitHour)/CAST(I.numNoItemIntoContainer AS FLOAT)) AS numTotalContainer
			,0 AS numUsedContainer
		FROM 
			OpportunityBizDocItems OBI
		INNER JOIN 
			Item AS I
		ON 
			OBI.numItemCode = I.numItemCode
		INNER JOIN
			WareHouseItems WI
		ON
			OBI.numWarehouseItmsID = WI.numWareHouseItemID
		WHERE
			numOppBizDocID=@numOppBizDocID
			AND I.charItemType ='P'
			AND ISNULL(numContainer,0) > 0
		GROUP BY
			I.numContainer
			,WI.numWareHouseID
			,I.numNoItemIntoContainer
	) AS T1
	INNER JOIN
	(
		SELECT
			I.numItemCode
			,WI.numWareHouseID
			,ISNULL(I.fltWeight,0) fltWeight
			,ISNULL(I.fltHeight,0) fltHeight
			,ISNULL(I.fltWidth,0) fltWidth
			,ISNULL(I.fltLength,0) fltLength
		FROM 
			OpportunityBizDocItems OBI
		INNER JOIN 
			Item AS I
		ON 
			OBI.numItemCode = I.numItemCode
		INNER JOIN
			WareHouseItems WI
		ON
			OBI.numWarehouseItmsID = WI.numWareHouseItemID
		WHERE
			numOppBizDocID=@numOppBizDocID
			AND I.charItemType ='P'
			AND ISNULL(I.bitContainer,0) = 1
		GROUP BY
			I.numItemCode
			,WI.numWareHouseID
			,ISNULL(I.fltWeight,0)
			,ISNULL(I.fltHeight,0)
			,ISNULL(I.fltWidth,0)
			,ISNULL(I.fltLength,0)
	) AS T2
	ON
		T1.numContainer = T2.numItemCode
		AND T1.numWareHouseID = T2.numWareHouseID

	IF (SELECT COUNT(*) FROM @TEMPContainer) = 0
	BEGIN
		RAISERROR('Container(s) are not available in bizdoc.',16,1)
	END
	ELSE
	BEGIN
		DECLARE @TEMPItems TABLE
		(
			ID INT IDENTITY(1,1)
			,numOppBizDocItemID NUMERIC(18,0)
			,numItemCode NUMERIC(18,0)
			,numWarehouseID NUMERIC(18,0)
			,numUnitHour FLOAT
			,numContainer NUMERIC(18,0)
			,numNoItemIntoContainer NUMERIC(18,0)
			,fltItemWidth FLOAT
			,fltItemHeight FLOAT
			,fltItemLength FLOAT
			,fltItemWeight FLOAT
		)

		INSERT INTO 
			@TEMPItems
		SELECT
			OBDI.numOppBizDocItemID
			,I.numItemCode
			,ISNULL(WI.numWareHouseID,0)
			,ISNULL(OBDI.numUnitHour,0)
			,ISNULL(I.numContainer,0)
			,ISNULL(I.numNoItemIntoContainer,0)
			,ISNULL(fltWidth,0)
			,ISNULL(fltHeight,0)
			,ISNULL(fltLength,0)
			,ISNULL(fltWeight,0)
		FROM
			OpportunityBizDocs OBD
		INNER JOIN
			OpportunityBizDocItems OBDI
		ON
			OBD.numOppBizDocsId=OBDI.numOppBizDocID
		INNER JOIN
			WareHouseItems WI
		ON
			OBDI.numWarehouseItmsID=WI.numWareHouseItemID
		INNER JOIN
			Item I
		ON
			OBDI.numItemCode=I.numItemCode
		WHERE
			numOppId=@numOppID
			AND numOppBizDocsId=@numOppBizDocId
			AND ISNULL(I.numContainer,0) > 0

		DECLARE @i AS INT = 1
		DEClARE @j AS INT = 1
		DECLARE @iCount as INT
		SELECT @iCount = COUNT(*) FROM @TEMPItems 

		DECLARE @numBoxID NUMERIC(18,0)
		DECLARE @numTempQty AS FLOAT
		DECLARE @numTempOppBizDocItemID AS NUMERIC(18,0)
		DECLARE @numTempContainerQty AS NUMERIC(18,0)
		DECLARE @numtempContainer AS NUMERIC(18,0)
		DECLARE @numTempItemCode AS NUMERIC(18,0)
		DECLARE @numTempWarehouseID AS NUMERIC(18,0)
		DECLARE @fltTempItemWidth AS FLOAT
		DECLARE @fltTempItemHeight AS FLOAT
		DECLARE @fltTempItemLength AS FLOAT
		DECLARE @fltTempItemWeight AS FLOAT

		IF ISNULL(@iCount,0) > 0
		BEGIN
			WHILE @i <= @iCount
			BEGIN
				SELECT 
					@numTempOppBizDocItemID=numOppBizDocItemID
					,@numtempContainer=numContainer
					,@numTempItemCode=numItemCode
					,@numTempWarehouseID=numWarehouseID
					,@numTempQty=numUnitHour
					,@numTempContainerQty=numNoItemIntoContainer
					,@fltTempItemWidth=fltItemWidth
					,@fltTempItemHeight=fltItemHeight
					,@fltTempItemLength=fltItemLength
					,@fltTempItemWeight=fltItemWeight
				FROM 
					@TEMPItems 
				WHERE 
					ID=@i

				IF EXISTS (SELECT * FROM @TEMPContainer WHERE numContainer=@numtempContainer AND numWareHouseID=@numTempWarehouseID AND numContainerQty=@numTempContainerQty AND numUsedContainer < numTotalContainer)
				BEGIN
					DECLARE @numID AS INT
					DECLARE @numTempTotalContainer NUMERIC(18,0)
					DECLARE @numTempMainContainerQty NUMERIC(18,0)
					DECLARE @bitTempItemAdded BIT
					DECLARE @fltTempWeight FLOAT
					DECLARE @fltTempHeight FLOAT
					DECLARE @fltTempWidth FLOAT
					DECLARE @fltTempLength FLOAT

					SELECT
						@numID=ID
						,@numTempTotalContainer=numTotalContainer
						,@numTempMainContainerQty=numNoItemIntoContainer
						,@bitTempItemAdded=bitItemAdded
						,@fltTempWeight=fltWeight
						,@fltTempHeight=fltHeight
						,@fltTempWidth=fltWidth
						,@fltTempLength=fltLength
					FROM
						@TEMPContainer 
					WHERE 
						numContainer=@numtempContainer 
						AND numWareHouseID=@numTempWarehouseID 
						AND numContainerQty=@numTempContainerQty

					WHILE @numTempQty > 0
					BEGIN
						 If @numTempTotalContainer > 0
						 BEGIN
							IF @numTempQty <= @numTempMainContainerQty
							BEGIN
								IF ISNULL(@bitTempItemAdded,0) = 0 OR @numTempMainContainerQty=@numTempContainerQty
								BEGIN
									-- CREATE NEW SHIPING BOX
									INSERT INTO ShippingBox
									(
										vcBoxName
										,numShippingReportId
										,fltTotalWeight
										,fltHeight
										,fltWidth
										,fltLength
										,dtCreateDate
										,numCreatedBy
										,numPackageTypeID
										,numServiceTypeID
										,fltDimensionalWeight
										,numShipCompany
									)
									VALUES
									(
										CONCAT('Box',@j)
										,@numShippingReportID
										,(CASE WHEN ISNULL(@fltTempWeight,0) = 0 THEN CEILING(CAST((CASE WHEN ISNULL(@fltTempHeight,0)=0 THEN 1 ELSE @fltTempHeight END) * (CASE WHEN ISNULL(@fltTempWidth,0)=0 THEN 1 ELSE @fltTempWidth END) * (CASE WHEN ISNULL(@fltTempLength,0)=0 THEN 1 ELSE @fltTempLength END) AS FLOAT)/166) ELSE @fltTempWeight END)
										,@fltTempHeight
										,@fltTempWidth
										,@fltTempLength
										,GETUTCDATE()
										,@numUserCntID
										,(CASE 
											WHEN @numShippingCompany = 88 THEN 19
											WHEN @numShippingCompany = 90 THEN (CASE WHEN ISNULL((SELECT TOP 1 vcShipFieldValue FROM ShippingFieldValues WHERE numDomainID=@numDomainId AND intShipFieldID=21 AND numListItemID=90),0) = '1' THEN 69 ELSE 50 END)
											WHEN @numShippingCompany = 91 THEN 7
										END)
										,@numShippingService
										,(CASE WHEN ISNULL(@fltTempWeight,0) = 0 THEN CEILING(CAST((CASE WHEN ISNULL(@fltTempHeight,0)=0 THEN 1 ELSE @fltTempHeight END) * (CASE WHEN ISNULL(@fltTempWidth,0)=0 THEN 1 ELSE @fltTempWidth END) * (CASE WHEN ISNULL(@fltTempLength,0)=0 THEN 1 ELSE @fltTempLength END) AS FLOAT)/166) ELSE @fltTempWeight END)
										,@numShippingCompany
										
									)

									SELECT @numBoxID = SCOPE_IDENTITY()

									UPDATE @TEMPContainer SET bitItemAdded = 1 WHERE ID=@numID
									
									SET @j = @j + 1
								END
								ELSE
								BEGIN
									SELECT @numBoxID=numBoxID FROM ShippingBox WHERE numShippingReportId=@numShippingReportID AND vcBoxName=CONCAT('Box',@j)
								END

								SET @fltTempItemWeight = (CASE WHEN ISNULL(@fltTempItemWeight,0) > 0 THEN @fltTempItemWeight ELSE CEILING(CAST(@fltTempItemHeight * @fltTempItemWidth * @fltTempItemLength AS FLOAT)/166) END)

								UPDATE ShippingBox SET fltTotalWeight = ISNULL(fltTotalWeight,0) + (@fltTempItemWeight * @numTempQty) WHERE numBoxID=@numBoxID

								-- MAKE ENTRY IN ShippingReportItems Table
								INSERT INTO ShippingReportItems
								(
									numShippingReportId
									,numItemCode
									,tintServiceType
									,monShippingRate
									,fltTotalWeight
									,intNoOfBox
									,fltHeight
									,fltWidth
									,fltLength
									,dtCreateDate
									,numCreatedBy
									,numBoxID
									,numOppBizDocItemID
									,intBoxQty
								)
								VALUES
								(
									@numShippingReportID
									,@numTempItemCode
									,@numShippingService
									,0
									,@fltTempItemWeight
									,1
									,@fltTempHeight
									,@fltTempItemWidth
									,@fltTempItemLength
									,GETUTCDATE()
									,@numUserCntID
									,@numBoxID
									,@numTempOppBizDocItemID
									,@numTempQty
								)

								UPDATE @TEMPContainer SET numNoItemIntoContainer=numNoItemIntoContainer - @numTempQty WHERE ID=@numID
								SET @numTempMainContainerQty = @numTempMainContainerQty - @numTempQty

								IF @numTempMainContainerQty = 0
								BEGIN
									-- IF NO QTY LEFT TO ADD IN CONTAINER RESET CONAINER QTY AND MAKE FLAG bitItemAdded=0 SO IF numUsedContainer < numTotalContainer THEN NEW SHIPPING BOX WILL BE CREATED
									UPDATE @TEMPContainer SET numNoItemIntoContainer=numContainerQty,bitItemAdded=0,numUsedContainer = numUsedContainer + 1 WHERE ID=@numID
								END

								SET @numTempQty = 0
							END
							ELSE 
							BEGIN
								-- CREATE NEW SHIPING BOX
								INSERT INTO ShippingBox
								(
									vcBoxName
									,numShippingReportId
									,fltTotalWeight
									,fltHeight
									,fltWidth
									,fltLength
									,dtCreateDate
									,numCreatedBy
									,numPackageTypeID
									,numServiceTypeID
									,fltDimensionalWeight
									,numShipCompany
								)
								VALUES
								(
									CONCAT('Box',@j)
									,@numShippingReportID
									,(CASE WHEN ISNULL(@fltTempWeight,0) = 0 THEN CEILING(CAST((CASE WHEN ISNULL(@fltTempHeight,0)=0 THEN 1 ELSE @fltTempHeight END) * (CASE WHEN ISNULL(@fltTempWidth,0)=0 THEN 1 ELSE @fltTempWidth END) * (CASE WHEN ISNULL(@fltTempLength,0)=0 THEN 1 ELSE @fltTempLength END) AS FLOAT)/166) ELSE @fltTempWeight END)
									,@fltTempHeight
									,@fltTempWidth
									,@fltTempLength
									,GETUTCDATE()
									,@numUserCntID
									,(CASE 
										WHEN @numShippingCompany = 88 THEN 19
										WHEN @numShippingCompany = 90 THEN (CASE WHEN ISNULL((SELECT TOP 1 vcShipFieldValue FROM ShippingFieldValues WHERE numDomainID=@numDomainId AND intShipFieldID=21 AND numListItemID=90),0) = '1' THEN 69 ELSE 50 END)
										WHEN @numShippingCompany = 91 THEN 7
									END)
									,@numShippingService
									,(CASE WHEN ISNULL(@fltTempWeight,0) = 0 THEN CEILING(CAST((CASE WHEN ISNULL(@fltTempHeight,0)=0 THEN 1 ELSE @fltTempHeight END) * (CASE WHEN ISNULL(@fltTempWidth,0)=0 THEN 1 ELSE @fltTempWidth END) * (CASE WHEN ISNULL(@fltTempLength,0)=0 THEN 1 ELSE @fltTempLength END) AS FLOAT)/166) ELSE @fltTempWeight END)
									,@numShippingCompany
										
								)

								SELECT @numBoxID = SCOPE_IDENTITY()

								UPDATE @TEMPContainer SET numNoItemIntoContainer=numContainerQty,bitItemAdded=0,numUsedContainer = numUsedContainer + 1 WHERE ID=@numID
									
								SET @j = @j + 1

								--SELECT @numBoxID=numBoxID FROM ShippingBox WHERE numShippingReportId=@numShippingReportID AND vcBoxName=CONCAT('Box',@j)

								SET @fltTempItemWeight = (CASE WHEN ISNULL(@fltTempItemWeight,0) > CEILING(CAST(@fltTempItemHeight * @fltTempItemWidth * @fltTempItemLength AS FLOAT)/139) THEN @fltTempItemWeight ELSE CEILING(CAST(@fltTempItemHeight * @fltTempItemWidth * @fltTempItemLength AS FLOAT)/139) END)

								UPDATE ShippingBox SET fltTotalWeight = ISNULL(fltTotalWeight,0) + (@fltTempItemWeight * @numTempMainContainerQty) WHERE numBoxID=@numBoxID

								-- MAKE ENTRY IN ShippingReportItems Table
								INSERT INTO ShippingReportItems
								(
									numShippingReportId
									,numItemCode
									,tintServiceType
									,monShippingRate
									,fltTotalWeight
									,intNoOfBox
									,fltHeight
									,fltWidth
									,fltLength
									,dtCreateDate
									,numCreatedBy
									,numBoxID
									,numOppBizDocItemID
									,intBoxQty
								)
								VALUES
								(
									@numShippingReportID
									,@numTempItemCode
									,@numShippingService
									,0
									,@fltTempItemWeight
									,1
									,@fltTempHeight
									,@fltTempItemWidth
									,@fltTempItemLength
									,GETUTCDATE()
									,@numUserCntID
									,@numBoxID
									,@numTempOppBizDocItemID
									,@numTempMainContainerQty
								)

								UPDATE @TEMPContainer SET numNoItemIntoContainer=@numTempContainerQty,numTotalContainer=numTotalContainer-1 WHERE ID=@numID 

								SET @numTempQty = @numTempQty - @numTempMainContainerQty
							END
						 END
						 ELSE
						 BEGIN
							RAISERROR('Sufficient container(s) are not available.',16,1)
							BREAK
						 END
					END
				END
				ELSE
				BEGIN
					RAISERROR('Sufficient container(s) require for item and warehouse are not available.',16,1)
					BREAK
				END


				SET @i = @i + 1
			END
		END
		ELSE
		BEGIN
			RAISERROR('Inventory item(s) are not available in bizdoc.',16,1)
		END
	END

	SELECT @numShippingReportID
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);

END CATCH
END
GO
/****** Object:  StoredProcedure [dbo].[USP_UpdateOppLinkingDTls]    Script Date: 05/07/2009 22:12:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_updateopplinkingdtls')
DROP PROCEDURE usp_updateopplinkingdtls
GO
CREATE PROCEDURE [dbo].[USP_UpdateOppLinkingDTls]        
@numOppID as numeric(9)=0,            
@tintOppType as tinyint,        
@tintBillToType as tinyint,        
@tintShipToType as tinyint,        
@vcBillStreet as varchar(50),                  
@vcBillCity as varchar(50),                  
@numBillState as numeric(9),                  
@vcBillPostCode as varchar(15),                  
@numBillCountry as numeric(9),                          
@vcShipStreet as varchar(50),                  
@vcShipCity as varchar(50),                  
@numShipState as numeric(9),                  
@vcShipPostCode as varchar(15),                  
@numShipCountry as numeric(9),    
@vcBillCompanyName as varchar(100),    
@vcShipCompanyName as varchar(100),
@numParentOppID as numeric(9),
@numParentOppBizDocID AS NUMERIC(9),
@numBillToAddressID AS NUMERIC(18,0) = 0,
@numShipToAddressID AS NUMERIC(18,0) = 0,
@numBillingContact NUMERIC(18,0) = 0,
@bitAltBillingContact BIT = 0,
@vcAltBillingContact VARCHAR(200) = '',
@numShippingContact NUMERIC(18,0) = 0,
@bitAltShippingContact BIT = 0,
@vcAltShippingContact VARCHAR(200) = ''
AS        
BEGIN     

	IF ISNULL(@numBillToAddressID,0) > 0
	BEGIN
		DECLARE @numBillCompany NUMERIC(18,0)

		SELECT
			@numBillCompany = (CASE WHEN tintAddressOf=2 THEN AddressDetails.numRecordID ELSE 0 END),
			@vcBillCompanyName = (CASE WHEN tintAddressOf=2 THEN ISNULL((SELECT vcCompanyName FROM DivisionMaster INNER JOIN CompanyInfo ON DivisionMaster.numCompanyID=CompanyInfo.numCompanyId WHERE numDivisionID=AddressDetails.numRecordID),'') ELSE '' END),
			@vcBillStreet = vcStreet,
			@vcBillCity = vcCity,
			@numBillState = numState,
			@vcBillPostCode = vcPostalCode,
			@numBillCountry = numCountry 
		FROM
			AddressDetails
		WHERE
			numAddressID=@numBillToAddressID

		UPDATE 
			OpportunityMaster 
		SET 
			tintBillToType=2
		WHERE 
			numOppID=@numOppID

		IF (SELECT COUNT(*) FROM OpportunityAddress WHERE [numOppID]=@numOppID) = 0
		BEGIN
			INSERT INTO OpportunityAddress (numOppID) VALUES (@numOppID)      
		END

		UPDATE
			OpportunityAddress
		SET 
			numBillCompanyID=@numBillCompany,
			vcBillCompanyName = @vcBillCompanyName,
			vcBillStreet = @vcBillStreet,
			vcBillCity = @vcBillCity,
			numBillState = @numBillState,
			vcBillPostCode = @vcBillPostCode,
			numBillCountry = @numBillCountry,
			numBillingContact = @numBillingContact,
			bitAltBillingContact=@bitAltBillingContact,
			vcAltBillingContact=@vcAltBillingContact
		WHERE 
			numOppID = @numOppID
	END
	ELSE
	BEGIN
		UPDATE 
			OpportunityMaster 
		SET 
			tintBillToType=@tintBillToType,
			numBillToAddressID = @numBillToAddressID
		WHERE 
			numOppID=@numOppID
	END

	IF ISNULL(@numShipToAddressID,0) > 0
	BEGIN
		DECLARE @numShipCompany NUMERIC(18,0)

		SELECT
			@numShipCompany = (CASE WHEN tintAddressOf=2 THEN AddressDetails.numRecordID ELSE 0 END),
			@vcShipCompanyName = (CASE WHEN tintAddressOf=2 THEN ISNULL((SELECT vcCompanyName FROM DivisionMaster INNER JOIN CompanyInfo ON DivisionMaster.numCompanyID=CompanyInfo.numCompanyId WHERE numDivisionID=AddressDetails.numRecordID),'') ELSE '' END),
			@vcShipStreet = vcStreet,
			@vcShipCity = vcCity,
			@numShipState = numState,
			@vcShipPostCode = vcPostalCode,
			@numShipCountry = numCountry 
		FROM
			AddressDetails
		WHERE
			numAddressID=@numShipToAddressID

		UPDATE 
			OpportunityMaster 
		SET
			tintShipToType=2
		WHERE 
			numOppID=@numOppID

		IF (SELECT COUNT(*) FROM OpportunityAddress WHERE [numOppID]=@numOppID) = 0
		BEGIN
			INSERT INTO OpportunityAddress (numOppID) VALUES (@numOppID)      
		END

		UPDATE 
			OpportunityAddress
		SET 
			numShipCompanyID=@numShipCompany,
			vcShipCompanyName = @vcShipCompanyName,
			vcShipStreet = @vcShipStreet,
			vcShipCity = @vcShipCity,
			numShipState = @numShipState,
			vcShipPostCode = @vcShipPostCode,
			numShipCountry = @numShipCountry,
			numShippingContact = @numShippingContact,
			bitAltShippingContact=@bitAltShippingContact,
			vcAltShippingContact=@vcAltShippingContact
		WHERE 
			numOppID = @numOppID
	END
	ELSE
	BEGIN
		UPDATE 
			OpportunityMaster 
		SET 
			tintShipToType=@tintShipToType,
			numShipToAddressID = @numShipToAddressID
		WHERE 
			numOppID=@numOppID        
	END           
	        
	IF (@tintBillToType = 2 AND ISNULL(@numBillToAddressID,0) = 0) OR (@tintShipToType = 2  AND ISNULL(@numShipToAddressID,0) = 0)
	BEGIN
		IF (SELECT COUNT(*) FROM OpportunityAddress WHERE [numOppID]=@numOppID) = 0
		BEGIN
			INSERT  INTO OpportunityAddress ( numOppID )
			VALUES  ( @numOppID )      
		END
	END

	IF @tintBillToType = 2 AND ISNULL(@numBillToAddressID,0) = 0
	BEGIN
		UPDATE  OpportunityAddress
		SET     vcBillCompanyName = @vcBillCompanyName,
				vcBillStreet = @vcBillStreet,
				vcBillCity = @vcBillCity,
				numBillState = @numBillState,
				vcBillPostCode = @vcBillPostCode,
				numBillCountry = @numBillCountry,
				bitAltBillingContact=@bitAltBillingContact,
				vcAltBillingContact=@vcAltBillingContact
		WHERE   numOppID = @numOppID
	
	END
    
	IF @tintShipToType = 2 AND ISNULL(@numShipToAddressID,0) = 0
	BEGIN
	PRINT 'hi'
		UPDATE  OpportunityAddress
		SET     vcShipCompanyName = @vcShipCompanyName,
				vcShipStreet = @vcShipStreet,
				vcShipCity = @vcShipCity,
				numShipState = @numShipState,
				vcShipPostCode = @vcShipPostCode,
				numShipCountry = @numShipCountry,
				bitAltShippingContact=@bitAltShippingContact,
				vcAltShippingContact=@vcAltShippingContact
		WHERE   numOppID = @numOppID

	END

	DECLARE @numDomainID NUMERIC(18,0)
	DECLARE @numDivisionID NUMERIC(18,0)

	SELECT 
		@numDomainID=numDomainID
		,@numDivisionID=numDivisionID
	FROM 
		OpportunityMaster 
	WHERE 
		numOppID=@numOppId

	DELETE FROM OpportunityMasterTaxItems WHERE numOppId=@numOppID

	--INSERT TAX FOR DIVISION   
	IF @tintOppType=1 OR (@tintOppType=2 and (select isnull(bitPurchaseTaxCredit,0) from Domain where numDomainId=@numDomainId)=1)
	BEGIN
		INSERT INTO dbo.OpportunityMasterTaxItems 
		(
			numOppId,
			numTaxItemID,
			fltPercentage,
			tintTaxType,
			numTaxID
		) 
		SELECT 
			@numOppID,
			TI.numTaxItemID,
			TEMPTax.decTaxValue,
			TEMPTax.tintTaxType,
			0
		FROM 
			TaxItems TI 
		JOIN 
			DivisionTaxTypes DTT 
		ON 
			TI.numTaxItemID = DTT.numTaxItemID
		CROSS APPLY
		(
			SELECT
				decTaxValue,
				tintTaxType
			FROM
				dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,TI.numTaxItemID,@numOppId,0,NULL)
		) AS TEMPTax
		WHERE 
			DTT.numDivisionID=@numDivisionId 
			AND DTT.bitApplicable=1
		UNION 
		SELECT 
			@numOppID,
			0,
			TEMPTax.decTaxValue,
			TEMPTax.tintTaxType,
			0
		FROM 
			dbo.DivisionMaster 
		CROSS APPLY
		(
			SELECT
				decTaxValue,
				tintTaxType
			FROM
				dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,0,@numOppId,1,NULL)
		) AS TEMPTax
		WHERE 
			bitNoTax=0 
			AND numDivisionID=@numDivisionID	
		UNION 
		SELECT
			@numOppId
			,1
			,decTaxValue
			,tintTaxType
			,numTaxID
		FROM
			dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,1,@numOppId,1,NULL)		
	END	


	if @numParentOppID>0 
	BEGIN
		IF @numParentOppBizDocID = 0 
			SET @numParentOppBizDocID = NULL       

		insert into OpportunityLinking (numParentOppID,numChildOppID,numParentOppBizDocID) values(@numParentOppID,@numOppID,@numParentOppBizDocID)



		If (@tintOppType = 2 OR @tintOppType = 1)
		BEGIN

			UPDATE 
				OpportunityMaster 
			SET 
				vcCustomerPO#=ISNULL((SELECT vcOppRefOrderNo FROM OpportunityMaster WHERE numOppId=@numParentOppID),'')
			WHERE 
				numOppID=@numOppID 


			IF EXISTS (SELECT numParentChildFieldID FROM ParentChildCustomFieldMap WHERE numDomainID=@numDomainID AND numParentFieldID=100 AND tintParentModule=2 AND numChildFieldID=100 AND (tintChildModule=6 OR tintChildModule = 2))
			BEGIN
				UPDATE 
					OpportunityMaster 
				SET 
					numAssignedTo=ISNULL((SELECT numAssignedTo FROM OpportunityMaster WHERE numOppId=@numParentOppID),0)
				WHERE 
					numOppID=@numOppID 
			END

			IF EXISTS (SELECT numParentChildFieldID FROM ParentChildCustomFieldMap WHERE numDomainID=@numDomainID AND numParentFieldID=122 AND tintParentModule=2 AND numChildFieldID=122 AND (tintChildModule=6 OR tintChildModule = 2))
			BEGIN
				UPDATE 
					OpportunityMaster 
				SET 
					txtComments=ISNULL((SELECT txtComments FROM OpportunityMaster WHERE numOppId=@numParentOppID),'')
				WHERE 
					numOppID=@numOppID 
			END

			IF EXISTS (SELECT numParentChildFieldID FROM ParentChildCustomFieldMap WHERE numDomainID=@numDomainID AND numParentFieldID=771 AND tintParentModule=2 AND numChildFieldID=771 AND (tintChildModule=6 OR tintChildModule = 2))
			BEGIN
				UPDATE 
					OpportunityMaster 
				SET 
					intUsedShippingCompany=ISNULL((SELECT intUsedShippingCompany FROM OpportunityMaster WHERE numOppId=@numParentOppID),0)
					,numShipmentMethod=ISNULL((SELECT numShipmentMethod FROM OpportunityMaster WHERE numOppId=@numParentOppID),0)
					,numShippingService=ISNULL((SELECT numShippingService FROM OpportunityMaster WHERE numOppId=@numParentOppID),0)
				WHERE 
					numOppID=@numOppID 
			END

			If (@tintOppType = 2)
			BEGIN
				EXEC dbo.USP_AddParentChildCustomFieldMap
							@numDomainID = @numDOmainID, --  numeric(18, 0)
							@numRecordID = @numOppID, --  numeric(18, 0)
							@numParentRecId = @numParentOppID, --  numeric(18, 0)
							@tintPageID = 6 --  tinyin

				EXEC dbo.USP_AddParentChildCustomFieldMap
							@numDomainID = @numDOmainID, --  numeric(18, 0)
							@numRecordID = @numOppID, --  numeric(18, 0)
							@numParentRecId = @numDivisionID, --  numeric(18, 0)
							@tintPageID = 6 --  tinyin
			END
			ELSE IF (@tintOppType = 1)
			BEGIN
				EXEC dbo.USP_AddParentChildCustomFieldMap
							@numDomainID = @numDOmainID, --  numeric(18, 0)
							@numRecordID = @numOppID, --  numeric(18, 0)
							@numParentRecId = @numParentOppID, --  numeric(18, 0)
							@tintPageID = 2 --  tinyin

				EXEC dbo.USP_AddParentChildCustomFieldMap
							@numDomainID = @numDOmainID, --  numeric(18, 0)
							@numRecordID = @numOppID, --  numeric(18, 0)
							@numParentRecId = @numDivisionID, --  numeric(18, 0)
							@tintPageID = 2 --  tinyin
			END
		END
	end
END
GO



GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='GetOrderShipReceiveStatusForIndividualItem')
DROP FUNCTION GetOrderShipReceiveStatusForIndividualItem
GO
CREATE FUNCTION [dbo].[GetOrderShipReceiveStatusForIndividualItem] 
(
      @numOppID AS NUMERIC(18,0),
	  @tintOppType AS NUMERIC(18,0),
	  @tintshipped AS TINYINT,
	  @numItemCode AS NUMERIC(18,0)
)
RETURNS VARCHAR(100)
AS 
BEGIN	
	DECLARE @vcShippedReceivedStatus VARCHAR(100) = ''

	DECLARE @TotalQty AS FLOAT  = 0

	IF @tintOppType = 1 --SALES ORDER
	BEGIN
		DECLARE @numQtyShipped AS FLOAT = 0

		SELECT @TotalQty = ISNULL((SELECT SUM(ISNULL(dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), OI.numItemCode,I.numDomainId, ISNULL(OI.numUOMId, 0)),1) * numUnitHour) FROM OpportunityItems OI INNER JOIN Item I ON OI.numItemCode = I.numItemCode WHERE UPPER(I.charItemType) = 'P' AND ISNULL(bitDropship,0) = 0 AND numOppId = @numOppID AND I.numItemCode=@numItemCode),0)

		-- IF ORDER IS CLOSED WITHOUT FULFILLMENT MARK ALL QTY IS FULFILLED
		IF ISNULL(@tintshipped,0) = 1
		BEGIN
			SET @numQtyShipped = @TotalQty
		END
		ELSE
		BEGIN
			SELECT @numQtyShipped = ISNULL((SELECT SUM(ISNULL(dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), OI.numItemCode,I.numDomainId, ISNULL(OI.numUOMId, 0)),1) * numQtyShipped) FROM OpportunityItems OI INNER JOIN Item I ON OI.numItemCode = I.numItemCode  WHERE UPPER(I.charItemType) = 'P' AND ISNULL(bitDropship,0) = 0 AND numOppId = @numOppID  AND I.numItemCode=@numItemCode),0)
		END

		SELECT @vcShippedReceivedStatus =(CASE WHEN  @TotalQty = @numQtyShipped THEN '<b><font color="green">' ELSE '<b><font color="red">' END) + CONCAT(CAST(@TotalQty AS FLOAT),' / ',CAST(@numQtyShipped AS FLOAT)) + '</font></b>'
	END
	ELSE IF @tintOppType = 2 --PURCHASE ORDER
	BEGIN
		DECLARE @numQtyReceived AS FLOAT = 0

		SELECT @TotalQty = ISNULL((SELECT SUM(ISNULL(dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), OI.numItemCode,I.numDomainId, ISNULL(OI.numUOMId, 0)),1) * numUnitHour) FROM OpportunityItems OI INNER JOIN Item I ON OI.numItemCode = I.numItemCode  WHERE UPPER(I.charItemType) = 'P' AND ISNULL(bitDropship,0) = 0 AND numOppId = @numOppID  AND I.numItemCode=@numItemCode),0)
		
		-- IF ORDER IS CLOSED WITHOUT FULFILLMENT MARK ALL QTY IS FULFILLED
		IF ISNULL(@tintshipped,0) = 1
		BEGIN
			SET @numQtyReceived = @TotalQty
		END
		ELSE
		BEGIN
			SELECT @numQtyReceived = ISNULL((SELECT SUM(ISNULL(dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), OI.numItemCode,I.numDomainId, ISNULL(OI.numUOMId, 0)),1) * numUnitHourReceived) FROM OpportunityItems OI INNER JOIN Item I ON OI.numItemCode = I.numItemCode  WHERE UPPER(I.charItemType) = 'P' AND ISNULL(bitDropship,0) = 0 AND numOppId = @numOppID AND I.numItemCode=@numItemCode),0)
		END

		SELECT @vcShippedReceivedStatus =(CASE WHEN  @TotalQty = @numQtyReceived THEN '<b><font color="green">' ELSE '<b><font color="red">' END) + CONCAT(CAST(@TotalQty AS FLOAT),' / ',CAST(@numQtyReceived AS FLOAT)) + '</font></b>'
	END

	RETURN @vcShippedReceivedStatus
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetItemPriceBasedOnMethod')
DROP PROCEDURE USP_GetItemPriceBasedOnMethod
GO
CREATE PROCEDURE [dbo].[USP_GetItemPriceBasedOnMethod]
	@numDomainID NUMERIC(18,0),
	@numDivisionID NUMERIC(18,0),
	@tintOppType AS TINYINT,
	@numItemCode NUMERIC(18,0),
	@tintPriceMethod TINYINT,
	@monListPrice AS DECIMAL(20,5),
	@monVendorCost AS DECIMAL(20,5),
	@numQty FLOAT,
	@fltUOMConversionFactor AS FLOAT
AS
BEGIN
	DECLARE @tintPriceLevel AS TINYINT
	DECLARE @tintRuleType AS TINYINT
	DECLARE @tintDisountType AS TINYINT
	DECLARE @decDiscount AS FLOAT
	DECLARE @monPrice AS DECIMAL(20,5) = 0
	DECLARE @monFinalPrice AS DECIMAL(20,5) = 0
	DECLARE @numRelationship NUMERIC(18,0)
	DECLARE @numProfile NUMERIC(18,0)
	DECLARE @numItemClassification AS NUMERIC(18,0)
	DECLARE @tintPriceBookDiscount AS TINYINT

	SELECT @numItemClassification=numItemClassification FROM Item WHERE numItemCode=@numItemCode

	SELECT @tintPriceBookDiscount=tintPriceBookDiscount FROM Domain WHERE numDomainId=@numDomainID

	-- GET ORGANIZATION DETAL
	SELECT 
		@numRelationship=numCompanyType,
		@numProfile=vcProfile,
		@tintPriceLevel = ISNULL(D.tintPriceLevel,0)
	FROM 
		DivisionMaster D                  
	JOIN 
		CompanyInfo C 
	ON 
		C.numCompanyId=D.numCompanyID                  
	WHERE 
		numDivisionID =@numDivisionID       

	IF @tintPriceMethod= 1 -- PRICE LEVEL
	BEGIN
		IF (SELECT COUNT(*) FROM PricingTable WHERE numItemCode=@numItemCode AND ISNULL(numPriceRuleID,0) = 0) > 0
		BEGIN
			SELECT
				@tintDisountType = 1,
				@decDiscount = 0,
				@tintRuleType = tintRuleType,
				@monPrice = (CASE tintRuleType
										WHEN 1 -- Deduct From List Price
										THEN
											CASE tintDiscountType 
												WHEN 1 -- PERCENT
												THEN (CASE WHEN @monListPrice > 0 THEN @monListPrice - (@monListPrice * (ISNULL(decDiscount,0)/100)) ELSE 0 END)
												WHEN 2 -- FLAT AMOUNT
												THEN (CASE WHEN @monListPrice > 0 THEN (CASE WHEN (@monListPrice * @numQty * @fltUOMConversionFactor) - ISNULL(decDiscount,0) > 0 THEN ((@monListPrice * @numQty * @fltUOMConversionFactor) - ISNULL(decDiscount,0)) / (@numQty * @fltUOMConversionFactor) ELSE 0 END) ELSE 0 END)
												WHEN 3 -- NAMED PRICE
												THEN ISNULL(decDiscount,0)
											END
										WHEN 2 -- Add to primary vendor cost
										THEN
											CASE tintDiscountType 
												WHEN 1  -- PERCENT
												THEN @monVendorCost + (@monVendorCost * (ISNULL(decDiscount,0)/100)) * @fltUOMConversionFactor
												WHEN 2 -- FLAT AMOUNT
												THEN (CASE WHEN (@monVendorCost * @numQty * @fltUOMConversionFactor) + ISNULL(decDiscount,0) > 0 THEN ((@monVendorCost * @numQty * @fltUOMConversionFactor) + ISNULL(decDiscount,0)) / (@numQty * @fltUOMConversionFactor)  ELSE 0 END)
												WHEN 3 -- NAMED PRICE
												THEN ISNULL(decDiscount,0)
											END
										WHEN 3 -- Named price
										THEN
											CASE 
											WHEN ISNULL(@tintPriceLevel,0) > 0
											THEN
												(SELECT 
													 ISNULL(decDiscount,0)
												FROM
												(
													SELECT 
														ROW_NUMBER() OVER(PARTITION BY PricingTable.numItemCode ORDER BY numPricingID) Id,
														decDiscount
													FROM 
														PricingTable 
													WHERE 
														PricingTable.numItemCode = @numItemCode 
														AND tintRuleType = 3 
												) TEMP
												WHERE
													Id = @tintPriceLevel)
											ELSE
												ISNULL(decDiscount,0)
											END
										END
									) 
			FROM
				PricingTable
			WHERE
				numItemCode=@numItemCode AND (@numQty * @fltUOMConversionFactor) >= intFromQty AND (@numQty * @fltUOMConversionFactor) <= intToQty
		END

		SELECT 
			ISNULL(@monPrice,0) AS monPrice,
			ISNULL(@tintRuleType,1) AS tintRuleType,
			1 AS tintDisountType,
			0 AS decDiscount
	END
	ELSE IF @tintPriceMethod= 2 AND (SELECT COUNT(*) FROM PriceBookRules WHERE numDomainID=@numDomainID AND tintRuleFor=@tintOppType) > 0 -- PRICE RULE
	BEGIN
		DECLARE @numPriceRuleID NUMERIC(18,0)
		DECLARE @tintPriceRuleType AS TINYINT
		DECLARE @tintPricingMethod TINYINT
		DECLARE @tintPriceBookDiscountType TINYINT
		DECLARE @decPriceBookDiscount FLOAT
		DECLARE @intQntyItems INT
		DECLARE @decMaxDedPerAmt FLOAT

		SELECT 
			TOP 1
			@numPriceRuleID = numPricRuleID,
			@tintPricingMethod = tintPricingMethod,
			@tintPriceRuleType = p.tintRuleType,
			@tintPriceBookDiscountType = P.[tintDiscountType],
			@decPriceBookDiscount = P.[decDiscount],
			@intQntyItems = P.[intQntyItems],
			@decMaxDedPerAmt = P.[decMaxDedPerAmt]
		FROM   
			PriceBookRules P 
		LEFT JOIN 
			PriceBookRuleDTL PDTL 
		ON 
			P.numPricRuleID = PDTL.numRuleID
		LEFT JOIN 
			PriceBookRuleItems PBI 
		ON 
			P.numPricRuleID = PBI.numRuleID
		LEFT JOIN 
			[PriceBookPriorities] PP 
		ON 
			PP.[Step2Value] = P.[tintStep2] 
			AND PP.[Step3Value] = P.[tintStep3]
		WHERE  
			P.numDomainID=@numDomainID 
			AND tintRuleFor=@tintOppType
			AND (
					((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 1 AND PDTL.numValue = @numDivisionID)) -- Priority 1
					OR ((tintStep2 = 2 AND PBI.numValue = @numItemClassification) AND (tintStep3= 1 AND PDTL.numValue = @numDivisionID)) -- Priority 2
					OR ((tintStep2 = 3) AND (tintStep3 = 1 AND PDTL.numValue = @numDivisionID)) -- Priority 3

					OR ((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 4
					OR ((tintStep2 = 2 AND PBI.numValue = @numItemClassification) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 5
					OR ((tintStep2 = 3) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 6

					OR ((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 3)) -- Priority 7
					OR ((tintStep2 = 2 AND PBI.numValue = @numItemClassification) AND (tintStep3 = 3)) -- Priority 8
					OR ((tintStep2 = 3) and (tintStep3 = 3)) -- Priority 9
				)
		ORDER BY 
			PP.Priority ASC


		IF ISNULL(@numPriceRuleID,0) > 0
		BEGIN
			IF @tintPricingMethod = 1 -- BASED ON PRICE TABLE
			BEGIN
				SELECT
					@tintDisountType = tintDiscountType,
					@decDiscount = ISNULL(decDiscount,0),
					@tintRuleType = tintRuleType,
					@monPrice = (CASE tintRuleType WHEN 1 THEN @monListPrice WHEN 2 THEN @monVendorCost END) ,
					@monFinalPrice = (CASE tintRuleType
											WHEN 1 -- Deduct From List Price
											THEN
												CASE tintDiscountType 
													WHEN 1 -- PERCENT
													THEN (CASE WHEN @monListPrice > 0 THEN @monListPrice - (@monListPrice * (ISNULL(decDiscount,0)/100)) ELSE 0 END)
													WHEN 2 -- FLAT AMOUNT
													THEN (CASE WHEN @monListPrice > 0 THEN (CASE WHEN (@monListPrice * @numQty * @fltUOMConversionFactor) - ISNULL(decDiscount,0) > 0 THEN ((@monListPrice * @numQty * @fltUOMConversionFactor) - ISNULL(decDiscount,0)) / (@numQty * @fltUOMConversionFactor) ELSE 0 END) ELSE 0 END)
													WHEN 3 -- NAMED PRICE
													THEN ISNULL(decDiscount,0)
												END
											WHEN 2 -- Add to primary vendor cost
											THEN
												CASE tintDiscountType 
													WHEN 1  -- PERCENT
													THEN @monVendorCost + (@monVendorCost * (ISNULL(decDiscount,0)/100))
													WHEN 2 -- FLAT AMOUNT
													THEN (CASE WHEN (@monVendorCost * @numQty * @fltUOMConversionFactor) + ISNULL(decDiscount,0) > 0 THEN ((@monVendorCost * @numQty * @fltUOMConversionFactor) + ISNULL(decDiscount,0)) / (@numQty * @fltUOMConversionFactor)  ELSE 0 END)
													WHEN 3 -- NAMED PRICE
													THEN ISNULL(decDiscount,0)
												END
											WHEN 3 -- Named price
											THEN
												CASE 
												WHEN ISNULL(@tintPriceLevel,0) > 0
												THEN
													(SELECT 
														 ISNULL(decDiscount,0)
													FROM
													(
														SELECT 
															ROW_NUMBER() OVER(PARTITION BY PricingTable.numItemCode ORDER BY numPricingID) Id,
															decDiscount
														FROM 
															PricingTable 
														WHERE 
															PricingTable.numItemCode = @numItemCode 
															AND tintRuleType = 3 
													) TEMP
													WHERE
														Id = @tintPriceLevel)
												ELSE
													ISNULL(decDiscount,0)
												END
											END
										) 
				FROM
					PricingTable
				WHERE
					numPriceRuleID=@numPriceRuleID AND @numQty * @fltUOMConversionFactor >= intFromQty AND @numQty * @fltUOMConversionFactor <= intToQty
			END
			ELSE IF @tintPricingMethod = 2 -- BASED ON RULE
			BEGIN
				SET @monPrice = (CASE WHEN @tintPriceRuleType = 2 THEN @monVendorCost ELSE @monListPrice END)

				IF (@tintPriceBookDiscountType = 1 ) -- Percentage 
				BEGIN
					SET @tintDisountType = @tintPriceBookDiscountType
					SET @decDiscount = @decPriceBookDiscount * (@numQty/@intQntyItems);
					SET @decDiscount = @decPriceBookDiscount * (CASE WHEN @tintPriceRuleType = 2 THEN @monVendorCost ELSE @monListPrice END) /100;
					SET @decMaxDedPerAmt = @decMaxDedPerAmt * (CASE WHEN @tintPriceRuleType = 2 THEN @monVendorCost ELSE @monListPrice END) /100;
					
					IF (@decDiscount > @decMaxDedPerAmt)
						SET @decDiscount = @decMaxDedPerAmt;
				
					IF ( @tintPriceRuleType = 1 )
							SET @monFinalPrice = (@monListPrice - @decDiscount);
					IF ( @tintPriceRuleType = 2 )
							SET @monFinalPrice = (@monVendorCost + @decDiscount);
				END
				IF ( @tintPriceBookDiscountType = 2 ) -- Flat discount 
				BEGIN
					SET @decDiscount = @decPriceBookDiscount * ((@numQty * @fltUOMConversionFactor)/@intQntyItems);

					IF (@decDiscount > @decMaxDedPerAmt)
						SET @decDiscount = @decMaxDedPerAmt;
					
					IF ( @tintPriceRuleType = 1 )
						SET @monFinalPrice =  ((@monListPrice * @numQty * @fltUOMConversionFactor) - @decDiscount) / (@numQty * @fltUOMConversionFactor);
						
					IF ( @tintPriceRuleType = 2 )
						SET @monFinalPrice = ((@monVendorCost * @numQty * @fltUOMConversionFactor) + @decDiscount) / (@numQty * @fltUOMConversionFactor);
				END
			END
		END
		

		SELECT 
			(CASE WHEN @tintPriceBookDiscount = 1 THEN ISNULL(@monPrice,0) ELSE ISNULL(@monFinalPrice,0) END) AS monPrice,
			ISNULL(@tintRuleType,1) AS tintRuleType,
			(CASE WHEN @tintPriceBookDiscount = 1 THEN ISNULL(@tintPriceBookDiscountType,1) ELSE 1 END) AS tintDisountType,
			(CASE WHEN @tintPriceBookDiscount = 1 THEN ISNULL(@decPriceBookDiscount,0) ELSE 0 END) AS decDiscount
	END
	ELSE IF @tintPriceMethod = 3 -- Last Price
	BEGIN
		SELECT 
			TOP 1 
			@monPrice = monPrice
			,@tintDisountType = ISNULL(OpportunityItems.bitDiscountType,1)
			,@decDiscount = ISNULL(OpportunityItems.fltDiscount,0)
			,@tintRuleType = 3
		FROM 
			OpportunityItems 
		JOIN 
			OpportunityMaster 
		ON 
			OpportunityItems.numOppId = OpportunityMaster.numOppId 
		WHERE 
			OpportunityMaster.numDomainId= @numDomainID
			AND tintOppType = @tintOppType
			AND tintOppStatus = 1
			AND numItemCode=@numItemCode
		ORDER BY
			OpportunityMaster.numOppId DESC

		SELECT 
			(CASE WHEN @tintPriceBookDiscount = 1 THEN ISNULL(@monPrice,0) ELSE ISNULL(@monFinalPrice,0) END) AS monPrice,
			ISNULL(@tintRuleType,1) AS tintRuleType,
			(CASE WHEN @tintPriceBookDiscount = 1 THEN ISNULL(@tintDisountType,1) ELSE 1 END) AS tintDisountType,
			(CASE WHEN @tintPriceBookDiscount = 1 THEN ISNULL(@decDiscount,0) ELSE 0 END) AS decDiscount
	END
END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetOrderSelectedItemDetail')
DROP PROCEDURE USP_GetOrderSelectedItemDetail
GO
CREATE PROCEDURE [dbo].[USP_GetOrderSelectedItemDetail]
	@numDomainID NUMERIC(18,0),
	@numDivisionID NUMERIC(18,0),
	@tintOppType AS TINYINT,
	@numItemCode NUMERIC(18,0),
	@numUOMID NUMERIC(18,0),
	@numWarehouseItemID AS NUMERIC(18,0),
	@numQty INT,
	@monPrice FLOAT,
	@numOppID NUMERIC(18,0),
	@numOppItemID NUMERIC(18,0),
	@vcSelectedKitChildItems VARCHAR(MAX),
	@numShippingCountry AS NUMERIC(18,0),
	@numWarehouseID AS NUMERIC(18,0),
	@vcCoupon AS VARCHAR(200)
AS
BEGIN
	DECLARE @vcItemName AS VARCHAR(500)
	DECLARE @bitDropship AS BIT
	DECLARE @bitMatrix AS BIT
	DECLARE @chrItemType AS CHAR(1)
	DECLARE @numItemGroup AS NUMERIC(18,0)
	DECLARE @numBaseUnit AS NUMERIC(18,0)
	DECLARE @numSaleUnit AS NUMERIC(18,0)
	DECLARE @fltUOMConversionFactor AS FLOAT
	DECLARE @numPurchaseUnit AS NUMERIC(18,0)
	DECLARE @bitAssemblyOrKit BIT
	DECLARE @bitAssembly AS BIT
	DECLARE @bitKit AS BIT
	DECLARE @bitHasChildKits BIT
	DECLARE @bitSOWorkOrder BIT
	DECLARE @bitCalAmtBasedonDepItems AS BIT
	DECLARE @ItemDesc AS VARCHAR(1000)
	DECLARE @numItemClassification AS NUMERIC(18,0)

	DECLARE @monListPrice AS DECIMAL(20,5) = 0.0000
	DECLARE @monVendorCost AS DECIMAL(20,5) = 0.0000
	DECLARE @tintPricingBasedOn AS TINYINT = 0.0000

	DECLARE @tintRuleType AS TINYINT
	DECLARE @monPriceLevelPrice AS DECIMAL(20,5) = 0.0000
	DECLARE @monPriceFinalPrice AS DECIMAL(20,5) = 0.0000
	DECLARE @monPriceRulePrice AS DECIMAL(20,5) = 0.0000
	DECLARE @monPriceRuleFinalPrice AS DECIMAL(20,5) = 0.0000
	DECLARE @monLastPrice AS DECIMAL(20,5) = 0.0000
	DECLARE @dtLastOrderDate VARCHAR(200) = ''

	DECLARE @tintDisountType TINYINT
	DECLARE @decDiscount AS FLOAT

	DECLARE @numVendorID AS NUMERIC(18,0)
	DECLARE @vcMinOrderQty AS VARCHAR(5) = '-'
	DECLARE @vcVendorNotes AS VARCHAR(300) = ''
	DECLARE @numRelationship NUMERIC(18,0)
	DECLARE @numProfile NUMERIC(18,0)
	DECLARE @tintPriceLevel AS TINYINT
	DECLARE @tintDefaultSalesPricing AS TINYINT
	DECLARE @tintPriceBookDiscount AS TINYINT
	DECLARE @tintKitAssemblyPriceBasedOn TINYINT

	IF ISNULL(@numDivisionID,0) > 0
	BEGIN
		SELECT @vcMinOrderQty=ISNULL(CAST(intMinQty AS VARCHAR),'-'),@vcVendorNotes=ISNULL(vcNotes,'') FROM Vendor WHERE numVendorID=@numDivisionID AND numItemCode=@numItemCode
	END

	SELECT @tintDefaultSalesPricing = numDefaultSalesPricing,@tintPriceBookDiscount=tintPriceBookDiscount FROM Domain WHERE numDomainId=@numDomainID

	IF ISNULL(@numWarehouseID,0) > 0
	BEGIN
		SELECT TOP 1 @numWarehouseItemID=numWareHouseItemID FROM WareHouseItems WHERE numItemID=@numItemCode AND numWareHouseID=@numWarehouseID ORDER BY numWareHouseItemID ASC
	END

	-- GET ORGANIZATION DETAL
	SELECT 
		@numRelationship=numCompanyType,
		@numProfile=vcProfile,
		@tintPriceLevel = ISNULL(D.tintPriceLevel,0)
	FROM 
		DivisionMaster D                  
	JOIN 
		CompanyInfo C 
	ON 
		C.numCompanyId=D.numCompanyID                  
	WHERE 
		numDivisionID =@numDivisionID       

	-- GET ITEM DETAILS
	SELECT
		@vcItemName = vcItemName,
		@bitDropship = bitAllowDropShip,
		@ItemDesc = Item.txtItemDesc,
		@bitMatrix = ISNULL(Item.bitMatrix,0),
		@chrItemType = charItemType,
		@numItemGroup = numItemGroup,
		@bitAssembly = ISNULL(bitAssembly,0),
		@bitSOWorkOrder = ISNULL(bitSOWorkOrder,0),
		@numBaseUnit = ISNULL(numBaseUnit,0),
		@numSaleUnit = (CASE WHEN ISNULL(@numUOMID,0) > 0 THEN @numUOMID ELSE ISNULL(numSaleUnit,0) END),
		@numPurchaseUnit = (CASE WHEN ISNULL(@numUOMID,0) > 0 THEN @numUOMID ELSE ISNULL(numPurchaseUnit,0) END),
		@numItemClassification = numItemClassification,
		@bitAssemblyOrKit = (CASE WHEN ISNULL(bitKitParent,0)=1 OR ISNULL(bitAssembly,0)=1 THEN 1 ELSE 0 END),
		@monListPrice = (CASE WHEN Item.charItemType='P' THEN ISNULL(WareHouseItems.monWListPrice,0) ELSE ISNULL(Item.monListPrice,0) END),
		@monVendorCost = ISNULL((SELECT ISNULL(monCost,0) * dbo.fn_UOMConversion(Item.numBaseUnit,Item.numItemCode,Item.numDomainID,Item.numPurchaseUnit) FROM Vendor WHERE numVendorID=Item.numVendorID AND Vendor.numItemCode=Item.numItemCode),0),
		@numVendorID = ISNULL(Item.numVendorID,0),
		@numWarehouseItemID = (CASE WHEN ISNULL(@numWarehouseItemID,0) > 0 THEN @numWarehouseItemID ELSE WareHouseItems.numWareHouseItemID END),
		@bitCalAmtBasedonDepItems = ISNULL(bitCalAmtBasedonDepItems,0),
		@tintKitAssemblyPriceBasedOn=ISNULL(tintKitAssemblyPriceBasedOn,1),
		@bitKit=ISNULL(bitKitParent,0),
		@bitHasChildKits = (CASE 
								WHEN ISNULL(bitKitParent,0) = 1 
								THEN 
									(CASE 
										WHEN EXISTS (SELECT ID.numItemDetailID FROM ItemDetails ID INNER JOIN Item IInner ON ID.numChildItemID=IInner.numItemCode WHERE ID.numItemKitID=Item.numItemCode AND ISNULL(IInner.bitKitParent,0)=1)
										THEN 1
										ELSE 0
									END)
								ELSE 0 
							END)
	FROM
		Item
	OUTER APPLY
	(
		SELECT TOP 1 numWareHouseItemID,monWListPrice FROM WareHouseItems WHERE numItemID=@numItemCode AND (numWareHouseItemID = @numWarehouseItemID OR ISNULL(@numWarehouseItemID,0) = 0)
	) AS WareHouseItems
	WHERE
		Item.numItemCode = @numItemCode

	IF ISNULL(@numDivisionID,0) > 0 AND EXISTS (SELECT * FROM Vendor WHERE numItemCode=@numItemCode AND numVendorID=@numDivisionID)
	BEGIN
		SELECT @monVendorCost=ISNULL(monCost,0) FROM Vendor WHERE numVendorID=@numDivisionID AND numItemCode=@numItemCode
	END


	IF @chrItemType = 'P' AND ISNULL(@numWarehouseItemID,0) = 0
	BEGIN
		RAISERROR('WAREHOUSE_DOES_NOT_EXISTS',16,1)
	END

	SET @fltUOMConversionFactor = dbo.fn_UOMConversion((CASE WHEN  @tintOppType = 1 THEN @numSaleUnit ELSE @numPurchaseUnit END),@numItemCode,@numDomainId,@numBaseUnit)

	IF @tintOppType = 1
	BEGIN
		/*Use Calculated Price When item is Assembly/Kit,Calculate price based on sum of dependent items.*/
		IF @bitCalAmtBasedonDepItems = 1 
		BEGIN
			PRINT 'IN DEPENDED ITEM'

			DECLARE @TEMPPrice TABLE
			(
				bitSuccess BIT
				,monPrice DECIMAL(20,5)
			)
			INSERT INTO @TEMPPrice
			(
				bitSuccess
				,monPrice
			)
			SELECT
				bitSuccess
				,monPrice
			FROM
				dbo.fn_GetKitAssemblyCalculatedPrice(@numDomainID,@numItemCode,@numWarehouseItemID,@tintKitAssemblyPriceBasedOn,@numOppID,@numOppItemID,@vcSelectedKitChildItems)

			IF (SELECT bitSuccess FROM @TEMPPrice) = 1
			BEGIN
				SET @monListPrice = (SELECT monPrice FROM @TEMPPrice)
			END
			ELSE
			BEGIN
				RAISERROR('SELECTED_WAREHOUSE_IS_NOT_AVAILABLE_FOR_ALL_CHILD_ITEMS',16,1)
			END
		END

		IF @tintDefaultSalesPricing = 1 AND (SELECT COUNT(*) FROM PricingTable WHERE numItemCode=@numItemCode AND ISNULL(numPriceRuleID,0) = 0) > 0 -- PRICE LEVEL
		BEGIN
			SELECT
				@tintDisountType = tintDiscountType,
				@decDiscount = ISNULL(decDiscount,0),
				@tintRuleType = tintRuleType,
				@monPriceLevelPrice = (CASE tintRuleType WHEN 1 THEN @monListPrice WHEN 2 THEN @monVendorCost ELSE ISNULL(decDiscount,0) END),
				@monPriceFinalPrice = (CASE tintRuleType
										WHEN 1 -- Deduct From List Price
										THEN
											CASE tintDiscountType 
												WHEN 1 -- PERCENT
												THEN (CASE WHEN @monListPrice > 0 THEN @monListPrice - (@monListPrice * (ISNULL(decDiscount,0)/100)) ELSE 0 END)
												WHEN 2 -- FLAT AMOUNT
												THEN (CASE WHEN @monListPrice > 0 THEN (CASE WHEN (@monListPrice * @numQty * @fltUOMConversionFactor) - ISNULL(decDiscount,0) > 0 THEN ((@monListPrice * @numQty * @fltUOMConversionFactor) - ISNULL(decDiscount,0)) / (@numQty * @fltUOMConversionFactor) ELSE 0 END) ELSE 0 END)
												WHEN 3 -- NAMED PRICE
												THEN ISNULL(decDiscount,0)
											END
										WHEN 2 -- Add to primary vendor cost
										THEN
											CASE tintDiscountType 
												WHEN 1  -- PERCENT
												THEN @monVendorCost + (@monVendorCost * (ISNULL(decDiscount,0)/100))  * @fltUOMConversionFactor
												WHEN 2 -- FLAT AMOUNT
												THEN (CASE WHEN (@monVendorCost * @numQty * @fltUOMConversionFactor) + ISNULL(decDiscount,0) > 0 THEN ((@monVendorCost * @numQty * @fltUOMConversionFactor) + ISNULL(decDiscount,0)) / (@numQty * @fltUOMConversionFactor)  ELSE 0 END)
												WHEN 3 -- NAMED PRICE
												THEN ISNULL(decDiscount,0)
											END
										WHEN 3 -- Named price
										THEN
											ISNULL(decDiscount,0)
										END
									) 
			FROM
			(
				SELECT 
					ROW_NUMBER() OVER(PARTITION BY PricingTable.numItemCode ORDER BY numPricingID) Id,
					*
				FROM
					PricingTable
				WHERE 
					PricingTable.numItemCode = @numItemCode
					
			) TEMP
			WHERE
				numItemCode=@numItemCode AND ((tintRuleType = 3 AND Id=@tintPriceLevel) OR (tintRuleType <> 3 AND @numQty * @fltUOMConversionFactor >= intFromQty AND @numQty * @fltUOMConversionFactor <= intToQty))
		END
		ELSE IF @tintDefaultSalesPricing = 2 AND (SELECT COUNT(*) FROM PriceBookRules WHERE numDomainID=@numDomainID AND tintRuleFor=@tintOppType) > 0 -- PRICE RULE
		BEGIN
			DECLARE @numPriceRuleID NUMERIC(18,0)
			DECLARE @tintPriceRuleType AS TINYINT
			DECLARE @tintPricingMethod TINYINT
			DECLARE @tintPriceBookDiscountType TINYINT
			DECLARE @decPriceBookDiscount FLOAT
			DECLARE @intQntyItems INT
			DECLARE @decMaxDedPerAmt FLOAT

			SELECT 
				TOP 1
				@numPriceRuleID = numPricRuleID,
				@tintPricingMethod = tintPricingMethod,
				@tintPriceRuleType = p.tintRuleType,
				@tintPriceBookDiscountType = P.[tintDiscountType],
				@decPriceBookDiscount = P.[decDiscount],
				@intQntyItems = P.[intQntyItems],
				@decMaxDedPerAmt = P.[decMaxDedPerAmt]
			FROM   
				PriceBookRules P 
			LEFT JOIN 
				PriceBookRuleDTL PDTL 
			ON 
				P.numPricRuleID = PDTL.numRuleID
			LEFT JOIN 
				PriceBookRuleItems PBI 
			ON 
				P.numPricRuleID = PBI.numRuleID
			LEFT JOIN 
				[PriceBookPriorities] PP 
			ON 
				PP.[Step2Value] = P.[tintStep2] 
				AND PP.[Step3Value] = P.[tintStep3]
			WHERE  
				P.numDomainID=@numDomainID 
				AND tintRuleFor=@tintOppType
				AND (
						((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 1 AND PDTL.numValue = @numDivisionID)) -- Priority 1
						OR ((tintStep2 = 2 AND PBI.numValue = @numItemClassification) AND (tintStep3= 1 AND PDTL.numValue = @numDivisionID)) -- Priority 2
						OR ((tintStep2 = 3) AND (tintStep3 = 1 AND PDTL.numValue = @numDivisionID)) -- Priority 3

						OR ((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 4
						OR ((tintStep2 = 2 AND PBI.numValue = @numItemClassification) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 5
						OR ((tintStep2 = 3) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 6

						OR ((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 3)) -- Priority 7
						OR ((tintStep2 = 2 AND PBI.numValue = @numItemClassification) AND (tintStep3 = 3)) -- Priority 8
						OR ((tintStep2 = 3) and (tintStep3 = 3)) -- Priority 9
					)
			ORDER BY 
				PP.Priority ASC


			IF ISNULL(@numPriceRuleID,0) > 0
			BEGIN
				IF @tintPricingMethod = 1 -- BASED ON PRICE TABLE
				BEGIN
					SELECT
						@tintDisountType = tintDiscountType,
						@decDiscount = ISNULL(decDiscount,0),
						@tintRuleType = tintRuleType,
						@monPriceRulePrice = (CASE tintRuleType WHEN 1 THEN @monListPrice WHEN 2 THEN @monVendorCost END) ,
						@monPriceRuleFinalPrice = (CASE tintRuleType
												WHEN 1 -- Deduct From List Price
												THEN
													CASE tintDiscountType 
														WHEN 1 -- PERCENT
														THEN (CASE WHEN @monListPrice > 0 THEN @monListPrice - (@monListPrice * (ISNULL(decDiscount,0)/100)) ELSE 0 END)
														WHEN 2 -- FLAT AMOUNT
														THEN (CASE WHEN @monListPrice > 0 THEN (CASE WHEN (@monListPrice * @numQty * @fltUOMConversionFactor) - ISNULL(decDiscount,0) > 0 THEN ((@monListPrice * @numQty * @fltUOMConversionFactor) - ISNULL(decDiscount,0)) / (@numQty * @fltUOMConversionFactor) ELSE 0 END) ELSE 0 END)
														WHEN 3 -- NAMED PRICE
														THEN ISNULL(decDiscount,0)
													END
												WHEN 2 -- Add to primary vendor cost
												THEN
													CASE tintDiscountType 
														WHEN 1  -- PERCENT
														THEN @monVendorCost + (@monVendorCost * (ISNULL(decDiscount,0)/100))
														WHEN 2 -- FLAT AMOUNT
														THEN (CASE WHEN (@monVendorCost * @numQty * @fltUOMConversionFactor) + ISNULL(decDiscount,0) > 0 THEN ((@monVendorCost * @numQty * @fltUOMConversionFactor) + ISNULL(decDiscount,0)) / (@numQty * @fltUOMConversionFactor)  ELSE 0 END)
														WHEN 3 -- NAMED PRICE
														THEN ISNULL(decDiscount,0)
													END
												WHEN 3 -- Named price
												THEN
													CASE 
													WHEN ISNULL(@tintPriceLevel,0) > 0
													THEN
														(SELECT 
															 ISNULL(decDiscount,0)
														FROM
														(
															SELECT 
																ROW_NUMBER() OVER(PARTITION BY PricingTable.numItemCode ORDER BY numPricingID) Id,
																decDiscount
															FROM 
																PricingTable 
															WHERE 
																PricingTable.numItemCode = @numItemCode 
																AND tintRuleType = 3 
														) TEMP
														WHERE
															Id = @tintPriceLevel)
													ELSE
														ISNULL(decDiscount,0)
													END
												END
											) 
					FROM
						PricingTable
					WHERE
						numPriceRuleID=@numPriceRuleID AND @numQty * @fltUOMConversionFactor >= intFromQty AND @numQty * @fltUOMConversionFactor <= intToQty
				END
				ELSE IF @tintPricingMethod = 2 -- BASED ON RULE
				BEGIN
					SET @tintRuleType = @tintPriceRuleType
					SET @monPriceRulePrice = (CASE @tintPriceRuleType WHEN 1 THEN @monListPrice WHEN 2 THEN @monVendorCost END) 

					IF (@tintPriceBookDiscountType = 1 ) -- Percentage 
					BEGIN
						SET @tintDisountType = @tintPriceBookDiscountType
						SET @decDiscount = @decPriceBookDiscount * (@numQty/@intQntyItems);
						SET @decDiscount = @decPriceBookDiscount * (CASE WHEN @tintPriceRuleType = 2 THEN @monVendorCost ELSE @monListPrice END) /100;
						SET @decMaxDedPerAmt = @decMaxDedPerAmt * (CASE WHEN @tintPriceRuleType = 2 THEN @monVendorCost ELSE @monListPrice END) /100;
						
						IF (@decDiscount > @decMaxDedPerAmt)
							SET @decDiscount = @decMaxDedPerAmt;
				
						IF ( @tintPriceRuleType = 1 )
								SET @monPriceRuleFinalPrice = (@monListPrice - @decDiscount);
						IF ( @tintPriceRuleType = 2 )
								SET @monPriceRuleFinalPrice = (@monVendorCost + @decDiscount);						
					END
					IF ( @tintPriceBookDiscountType = 2 ) -- Flat discount 
					BEGIN
						SET @decDiscount = @decPriceBookDiscount * ((@numQty * @fltUOMConversionFactor)/@intQntyItems);

						IF (@decDiscount > @decMaxDedPerAmt)
							SET @decDiscount = @decMaxDedPerAmt;
					
						IF ( @tintPriceRuleType = 1 )
							SET @monPriceRuleFinalPrice =  ((@monListPrice * @numQty * @fltUOMConversionFactor) - @decDiscount)/(@numQty * @fltUOMConversionFactor);
						IF ( @tintPriceRuleType = 2 )
							SET @monPriceRuleFinalPrice = ((@monVendorCost * @numQty * @fltUOMConversionFactor) + @decDiscount)/(@numQty * @fltUOMConversionFactor);
					END
				END
			END
		
		END
	
		-- GET Last Price
		DECLARE @LastDiscountType TINYINT
		DECLARE @LastDiscount FLOAT

		SELECT 
			TOP 1 
			@monLastPrice = monPrice
			,@dtLastOrderDate = dbo.FormatedDateFromDate(OpportunityMaster.bintCreatedDate,@numDomainID)
			,@LastDiscountType = ISNULL(OpportunityItems.bitDiscountType,1)
			,@LastDiscount = ISNULL(OpportunityItems.fltDiscount,0)
		FROM 
			OpportunityItems 
		JOIN 
			OpportunityMaster 
		ON 
			OpportunityItems.numOppId = OpportunityMaster.numOppId 
		WHERE 
			OpportunityMaster.numDomainId= @numDomainID
			AND tintOppType = @tintOppType
			AND tintOppStatus = 1
			AND numItemCode=@numItemCode
		ORDER BY
			OpportunityMaster.numOppId DESC

		IF @tintDefaultSalesPricing = 1 AND ISNULL(@monPriceFinalPrice,0) > 0
		BEGIN
			-- WE ARE NOT DISPLAYING DISCOUNT SEPERATELY IN PRICE LEVEL
			SET @tintPricingBasedOn = 1
			SET @monPrice = @monPriceFinalPrice
			SET @tintDisountType = 1
			SET @decDiscount = 0
		END
		ELSE IF @tintDefaultSalesPricing = 2 AND ISNULL(@monPriceRuleFinalPrice,0) > 0
		BEGIN
			SET @tintPricingBasedOn = 2
		
			IF @tintPriceBookDiscountType = 1 -- Display Disocunt Price As Unit Price
			BEGIN
				SET @monPrice = @monPriceRuleFinalPrice
				SET @tintDisountType = 1
				SET @decDiscount = 0
			END
			ELSE -- Display Unit Price And Disocunt Price Seperately
			BEGIN
				SET @monPrice = @monPriceRulePrice
			END
		
		END
		ELSE IF @tintDefaultSalesPricing = 3 AND ISNULL(@monLastPrice,0) > 0
		BEGIN
			SET @tintPricingBasedOn = 3
			SET @monPrice = @monLastPrice
			SET @tintDisountType = @LastDiscountType
			SET @decDiscount = @LastDiscount
		END
		ELSE
		BEGIN
			SET @tintPricingBasedOn = 0
			SET @tintDisountType = 1
			SET @decDiscount = 0
			SET @tintRuleType = 0

			IF @tintOppType = 1
				SET @monPrice = @monListPrice
			ELSE
				SET @monPrice = @monVendorCost
		END
	END
	ELSE IF @tintOppType = 2
	BEGIN
		SELECT 
			TOP 1
			@numPriceRuleID = numPricRuleID,
			@tintPricingMethod = tintPricingMethod,
			@tintPriceRuleType = p.tintRuleType,
			@tintPriceBookDiscountType = P.[tintDiscountType],
			@decPriceBookDiscount = P.[decDiscount],
			@intQntyItems = P.[intQntyItems],
			@decMaxDedPerAmt = P.[decMaxDedPerAmt]
		FROM   
			PriceBookRules P 
		LEFT JOIN 
			PriceBookRuleDTL PDTL 
		ON 
			P.numPricRuleID = PDTL.numRuleID
		LEFT JOIN 
			PriceBookRuleItems PBI 
		ON 
			P.numPricRuleID = PBI.numRuleID
		LEFT JOIN 
			[PriceBookPriorities] PP 
		ON 
			PP.[Step2Value] = P.[tintStep2] 
			AND PP.[Step3Value] = P.[tintStep3]
		WHERE  
			P.numDomainID=@numDomainID 
			AND tintRuleFor=@tintOppType
			AND (
					((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 1 AND PDTL.numValue = @numDivisionID)) -- Priority 1
					OR ((tintStep2 = 2 AND PBI.numValue = @numItemClassification) AND (tintStep3= 1 AND PDTL.numValue = @numDivisionID)) -- Priority 2
					OR ((tintStep2 = 3) AND (tintStep3 = 1 AND PDTL.numValue = @numDivisionID)) -- Priority 3

					OR ((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 4
					OR ((tintStep2 = 2 AND PBI.numValue = @numItemClassification) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 5
					OR ((tintStep2 = 3) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 6

					OR ((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 3)) -- Priority 7
					OR ((tintStep2 = 2 AND PBI.numValue = @numItemClassification) AND (tintStep3 = 3)) -- Priority 8
					OR ((tintStep2 = 3) and (tintStep3 = 3)) -- Priority 9
				)
		ORDER BY 
			PP.Priority ASC


		IF ISNULL(@numPriceRuleID,0) > 0
		BEGIN
			IF @tintPricingMethod = 1 -- BASED ON PRICE TABLE
			BEGIN
				SELECT
					@tintDisountType = tintDiscountType,
					@decDiscount = ISNULL(decDiscount,0),
					@tintRuleType = tintRuleType,
					@monPriceRulePrice = (CASE tintRuleType WHEN 1 THEN @monListPrice WHEN 2 THEN @monVendorCost END) ,
					@monPriceRuleFinalPrice = (CASE tintRuleType
											WHEN 1 -- Deduct From List Price
											THEN
												CASE tintDiscountType 
													WHEN 1 -- PERCENT
													THEN (CASE WHEN @monListPrice > 0 THEN @monListPrice - (@monListPrice * (ISNULL(decDiscount,0)/100)) ELSE 0 END)
													WHEN 2 -- FLAT AMOUNT
													THEN (CASE WHEN @monListPrice > 0 THEN (CASE WHEN (@monListPrice * @numQty * @fltUOMConversionFactor) - ISNULL(decDiscount,0) > 0 THEN ((@monListPrice * @numQty * @fltUOMConversionFactor) - ISNULL(decDiscount,0)) / (@numQty * @fltUOMConversionFactor) ELSE 0 END) ELSE 0 END)
													WHEN 3 -- NAMED PRICE
													THEN ISNULL(decDiscount,0)
												END
											WHEN 2 -- Add to primary vendor cost
											THEN
												CASE tintDiscountType 
													WHEN 1  -- PERCENT
													THEN @monVendorCost + (@monVendorCost * (ISNULL(decDiscount,0)/100))
													WHEN 2 -- FLAT AMOUNT
													THEN (CASE WHEN (@monVendorCost * @numQty * @fltUOMConversionFactor) + ISNULL(decDiscount,0) > 0 THEN ((@monVendorCost * @numQty * @fltUOMConversionFactor) + ISNULL(decDiscount,0)) / (@numQty * @fltUOMConversionFactor)  ELSE 0 END)
													WHEN 3 -- NAMED PRICE
													THEN ISNULL(decDiscount,0)
												END
											WHEN 3 -- Named price
											THEN
												CASE 
												WHEN ISNULL(@tintPriceLevel,0) > 0
												THEN
													(SELECT 
															ISNULL(decDiscount,0)
													FROM
													(
														SELECT 
															ROW_NUMBER() OVER(PARTITION BY PricingTable.numItemCode ORDER BY numPricingID) Id,
															decDiscount
														FROM 
															PricingTable 
														WHERE 
															PricingTable.numItemCode = @numItemCode 
															AND tintRuleType = 3 
													) TEMP
													WHERE
														Id = @tintPriceLevel)
												ELSE
													ISNULL(decDiscount,0)
												END
											END
										) 
				FROM
					PricingTable
				WHERE
					numPriceRuleID=@numPriceRuleID AND @numQty * @fltUOMConversionFactor >= intFromQty AND @numQty * @fltUOMConversionFactor <= intToQty
			END
			ELSE IF @tintPricingMethod = 2 -- BASED ON RULE
			BEGIN
				SET @tintRuleType = @tintPriceRuleType
				SET @monPriceRulePrice = (CASE @tintPriceRuleType WHEN 1 THEN @monListPrice WHEN 2 THEN @monVendorCost END) 

				IF (@tintPriceBookDiscountType = 1 ) -- Percentage 
				BEGIN
					SET @tintDisountType = @tintPriceBookDiscountType
					SET @decDiscount = @decDiscount * (@numQty/@intQntyItems);
					SET @decDiscount = @decDiscount * (CASE WHEN @tintPriceRuleType = 2 THEN @monVendorCost ELSE @monListPrice END) /100;
					SET @decMaxDedPerAmt = @decMaxDedPerAmt * (CASE WHEN @tintPriceRuleType = 2 THEN @monVendorCost ELSE @monListPrice END) /100;
					
					IF (@decDiscount > @decMaxDedPerAmt)
						SET @decDiscount = @decMaxDedPerAmt;
				
					IF ( @tintPriceRuleType = 1 )
							SET @monPriceRuleFinalPrice = (@monListPrice - @decDiscount);
					IF ( @tintPriceRuleType = 2 )
							SET @monPriceRuleFinalPrice = (@monVendorCost + @decDiscount);
				END
				IF ( @tintPriceBookDiscountType = 2 ) -- Flat discount 
				BEGIN
					SET @decDiscount = @decDiscount * ((@numQty * @fltUOMConversionFactor)/@intQntyItems);

					IF (@decDiscount > @decMaxDedPerAmt)
						SET @decDiscount = @decMaxDedPerAmt;
					
					IF ( @tintPriceRuleType = 1 )
						SET @monPriceRuleFinalPrice =  ((@monListPrice * @numQty * @fltUOMConversionFactor) - @decDiscount)/(@numQty * @fltUOMConversionFactor);
					IF ( @tintPriceRuleType = 2 )
						SET @monPriceRuleFinalPrice = ((@monVendorCost * @numQty * @fltUOMConversionFactor) + @decDiscount)/(@numQty * @fltUOMConversionFactor);
				END
			END
		END

		If @monPriceRuleFinalPrice > 0
		BEGIN
			SET @monPrice = @monPriceRuleFinalPrice
		END
		ELSE
		BEGIN
			SET @monPrice = @monVendorCost
		END


		SET @monPriceLevelPrice = 0
		SET @monPriceFinalPrice = 0
		SET @monPriceRulePrice = 0
		SET @monPriceRuleFinalPrice = 0
		SET @monLastPrice = 0
		SET @dtLastOrderDate = NULL
		SET @tintDisountType = 1
		SET @decDiscount =0 
	END

	DECLARE @numTempWarehouseID NUMERIC(18,0)
	SELECT @numTempWarehouseID=numWareHouseID FROM WareHouseItems WHERE numWareHouseItemID=@numWarehouseItemID
	

	SELECT
		@vcItemName AS vcItemName,
		@bitDropship AS bitDropship,
		@chrItemType AS ItemType,
		@bitMatrix AS bitMatrix,
		@numItemClassification AS ItemClassification,
		@numItemGroup AS ItemGroup,
		@ItemDesc AS ItemDescription,
		@bitAssembly AS bitAssembly,
		@bitKit AS bitKitParent,
		@bitHasChildKits AS bitHasChildKits,
		@bitSOWorkOrder AS bitSOWorkOrder,
		case when ISNULL(@bitAssembly,0)=1 then dbo.fn_GetAssemblyPossibleWOQty(@numItemCode,@numTempWarehouseID) else 0 end AS MaxWorkOrderQty,
		@tintPricingBasedOn AS tintPriceBaseOn,
		@monPrice AS monPrice,
		@fltUOMConversionFactor AS UOMConversionFactor,
		(CASE WHEN @tintOppType=1 THEN @monListPrice ELSE @monVendorCost END) AS monListPrice,
		@monVendorCost AS VendorCost,
		@numVendorID AS numVendorID,
		@tintRuleType AS tintRuleType,
		@monPriceLevelPrice AS monPriceLevel,
		@monPriceFinalPrice AS monPriceLevelFinalPrice,
		@monPriceRulePrice AS monPriceRule,
		@monPriceRuleFinalPrice AS monPriceRuleFinalPrice,
		@monLastPrice AS monLastPrice,
		@dtLastOrderDate AS LastOrderDate,
		@tintDisountType AS DiscountType,
		@decDiscount AS Discount,
		@numBaseUnit AS numBaseUnit,
		@numSaleUnit AS numSaleUnit,
		@numPurchaseUnit AS numPurchaseUnit,
		dbo.fn_GetUOMName(@numBaseUnit) AS vcBaseUOMName,
		dbo.fn_GetUOMName(@numSaleUnit) AS vcSaleUOMName,
		@numPurchaseUnit AS numPurchaseUnit,
		@numWarehouseItemID AS numWarehouseItemID,
		dbo.fn_GetItemTransitCount(@numItemcode,@numDomainID) AS OrdersInTransist,
		(SELECT COUNT(*) FROM SimilarItems SI INNER JOIN Item I ON I.numItemCode = SI.numItemCode WHERE SI.numDomainId = @numDomainID AND SI.numParentItemCode = @numItemCode) AS RelatedItemsCount,
		@tintPriceLevel AS tintPriceLevel,
		@vcMinOrderQty AS vcMinOrderQty,
		@vcVendorNotes AS vcVendorNotes


	-- CHECK IF PROMOTION CAN BE TRIGGERED FROM THIS ITEM
	DECLARE @numPromotionID AS NUMERIC(18,0)

	SELECT TOP 1
		@numPromotionID = numProId 
	FROM 
		PromotionOffer PO
	WHERE 
		numDomainId=@numDomainID 
		AND ISNULL(bitEnabled,0)=1 
		AND ISNULL(IsOrderBasedPromotion,0) = 0
		AND 1 = (CASE WHEN bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN GETUTCDATE()>=dtValidFrom AND GETUTCDATE()<=dtValidTo THEN 1 ELSE 0 END) END)
		AND 1 = (CASE tintCustomersBasedOn 
					WHEN 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=PO.numProId AND tintType=1 AND numDivisionID=@numDivisionID) > 0 THEN 1 ELSE 0 END)
					WHEN 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=PO.numProId AND tintType=2 AND numRelationship=@numRelationship AND numProfile=@numProfile) > 0 THEN 1 ELSE 0 END)
					WHEN 3 THEN 1
					ELSE 0
				END)
		AND 1 = (CASE 
					WHEN tintOfferBasedOn = 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=1 AND numValue=@numItemCode) > 0 THEN 1 ELSE 0 END)
					WHEN tintOfferBasedOn = 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=2 AND numValue=@numItemClassification) > 0 THEN 1 ELSE 0 END)
					WHEN tintOfferBasedOn = 4 THEN 1 
					ELSE 0
				END)
	ORDER BY
		CASE 
			WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 1 THEN 1
			WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 1 THEN 2
			WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 2 THEN 3
			WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 2 THEN 4
			WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 3 THEN 5
			WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 3 THEN 6
		END


	IF ISNULL(@numPromotionID,0) > 0
	BEGIN
		-- Get Top 1 Promotion Based on priority

		SELECT 
			numProId
			,ISNULL(vcShortDesc,'') AS vcShortDesc
			,ISNULL(vcLongDesc,'') AS vcLongDesc
		FROM
			PromotionOffer PO
		WHERE
			PO.numDomainId=@numDomainID 
			AND numProId = @numPromotionID
	END
END 
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_OpportunityItems_OrderwiseItems' ) 
    DROP PROCEDURE USP_OpportunityItems_OrderwiseItems
GO
-- =============================================        
-- Author:  <Author,,Sachin Sadhu>        
-- Create date: <Create Date,,30Dec2013>        
-- Description: <Description,,To get Items Customer wise n order wise>        
-- Module:Accounts/Items tab        
-- ============================================= 
CREATE PROCEDURE [dbo].[USP_OpportunityItems_OrderwiseItems]         
 -- Add the parameters for the stored procedure here        
    @numDomainId NUMERIC(9) ,    
    @numDivisionId NUMERIC(9) ,    
    @keyword VARCHAR(50) ,    
    @startDate AS DATETIME =NULL,    
    @endDate AS DATETIME =NULL,    
    @imode AS INT ,    
    @CurrentPage INT = 0 ,    
    @PageSize INT = 0 ,    
    @TotRecs INT = 0 OUTPUT    
AS     
    BEGIN        
 -- SET NOCOUNT ON added to prevent extra result sets from        
 -- interfering with SELECT statements.        
        SET NOCOUNT ON;        
                
	DECLARE @numCompanyID NUMERIC(18,0)
	SELECT @numCompanyID = numCompanyID FROM DivisionMaster where numDivisionID=@numDivisionId and numDomainID = @numDomainId 

    -- Insert statements for procedure here        
        IF ( @imode = 1 )     
            BEGIN        
                   
                CREATE TABLE #tempItems    
                    (    
                      Units FLOAT,    
                      BasePrice DECIMAL(18,2) ,    
                      ItemName VARCHAR(300) ,
					  vcSKU VARCHAR(300),
					  vcUPC VARCHAR(300),    
                      base DECIMAL(18,2) ,    
                      ChargedAmount DECIMAL(18,2) ,    
                      ORDERID NUMERIC(9) ,    
                      ModelId VARCHAR(200) ,    
                      OrderDate VARCHAR(30) ,    
                      tintOppType TINYINT ,    
                      tintOppStatus TINYINT ,    
                      numContactId NUMERIC ,    
                      numDivisionId NUMERIC ,    
                      OppId NUMERIC ,    
                      OrderNo VARCHAR(300)   ,
					  CustomerPartNo  VARCHAR(300),
					  numItemCode NUMERIc(18,0)
                    )        
                                
                INSERT  INTO #tempItems    
                        SELECT  OI.numUnitHour AS Units ,    
                                OI.monPrice AS BasePrice ,    
                                OI.vcItemName AS ItemName ,
								CASE WHEN ISNULL(I.numItemGroup,0) > 0 AND ISNULL(bitMatrix,0)=0 THEN ISNULL(WI.vcWHSKU,ISNULL(I.vcSKU,''))ELSE I.vcSKU END,
								CASE WHEN ISNULL(I.numItemGroup,0) > 0 AND ISNULL(bitMatrix,0)=0 THEN ISNULL(WI.vcBarCode,ISNULL(I.numBarCodeId,''))ELSE I.numBarCodeId END,
                                OI.monAvgCost AS base ,    
                                OI.monTotAmount AS ChargedAmount ,    
                                Oi.numItemCode AS ORDERID ,    
                                ISNULL(oi.vcModelID, 'NA') AS ModelId ,    
                                dbo.FormatedDateFromDate(OM.bintCreatedDate,OM.numDomainId)   AS OrderDate,     
                                om.tintOppType ,    
                                om.tintOppStatus ,    
                                Om.numContactId ,    
                                ACI.numDivisionId ,    
                                Om.numOppId AS OppId ,    
								 Om.vcPOppName AS OrderNo ,
								 ISNULL(CPN.CustomerPartNo,'') AS  CustomerPartNo,
								 OI.numItemCode
                        FROM    dbo.OpportunityItems AS OI    
								INNER JOIN Item I ON OI.numItemCode = I.numItemCode
                                INNER JOIN dbo.OpportunityMaster OM ON OI.numOppId = OM.numOppId    
                                INNER JOIN dbo.AdditionalContactsInformation ACI ON ACI.numContactId = om.numContactId   
								LEFT JOIN WareHouseItems WI ON OI.numWarehouseItmsID = WI.numWareHouseItemID
								LEFT JOIN  CustomerPartNumber CPN  ON OI.numItemCode = CPN.numItemCode AND numCompanyId = @numCompanyID and CPN.numDomainId = @numDomainId 
                        WHERE   Om.numDomainId = @numDomainId    
                                AND ACI.numDivisionId = @numDivisionId    
                                AND ( OI.vcItemName LIKE @keyword + '%'    
                                      OR OI.vcModelID LIKE @keyword + '%'    
      )    
                                AND ( OM.bintCreatedDate >= ISNULL(@startDate,OM.bintCreatedDate)    
                                      AND Om.bintCreatedDate <=ISNULL(@endDate,OM.bintCreatedDate)     
                                    )         
                                            
                DECLARE @firstRec AS INTEGER          
                DECLARE @lastRec AS INTEGER          
          
                SET @firstRec = ( @CurrentPage - 1 ) * @PageSize          
                SET @lastRec = ( @CurrentPage * @PageSize + 1 )          
                SET @TotRecs = ( SELECT COUNT(*)    
                                 FROM   #tempItems    
                               )          
                   
                SELECT  *    
                FROM    ( SELECT    * ,    
                                    ROW_NUMBER() OVER ( ORDER BY ItemName ) AS RowNumber    
                          FROM      #tempItems    
                        ) a    
                WHERE   RowNumber > @firstRec    
                        AND RowNumber < @lastRec    
                ORDER BY ItemName         
                            
                     
                DROP TABLE #tempItems          
            END        
                   
        ELSE     
            BEGIN       
                   
                CREATE TABLE #tempVItems    
                    (    
					  numDivisionID NUMERIC(18,0),
                      ItemName VARCHAR(300) ,   
					  vcSKU VARCHAR(300),
					  vcUPC VARCHAR(300), 
                      MinOrderQty INT ,    
                      MinCost DECIMAL(18,2) ,    
                      ModelId VARCHAR(200) ,    
                      PartNo VARCHAR(100) ,    
                      numItemCode NUMERIC    ,
					  CustomerPartNo  VARCHAR(100)  
                    )        
                INSERT  INTO #tempVItems    
                        SELECT  @numDivisionId,
								i.vcItemName AS ItemName,
								ISNULL(I.vcSKU,''),
								ISNULL(I.numBarCodeId,''),
                                v.intMinQty AS MinOrderQty,
                                v.monCost AS MinCost,
                                I.vcModelID AS ModelId,
                                v.vcPartNo AS PartNo,
                                v.numItemCode AS numItemCode,
								ISNULL(CPN.CustomerPartNo,'') AS  CustomerPartNo     
                        FROM    dbo.Item AS I    
                                INNER JOIN dbo.Vendor AS V ON I.numVendorID = V.numVendorID    
								LEFT JOIN  CustomerPartNumber CPN  ON I.numItemCode = CPN.numItemCode AND numCompanyId = @numCompanyID and CPN.numDomainId = @numDomainId 
                        WHERE   i.numVendorID = @numDivisionId    
                                AND v.numItemCode = i.numItemCode    
                                AND v.numDomainID = @numDomainId        
                              AND ( I.vcItemName LIKE @keyword + '%'    
                                      OR I.vcModelID LIKE @keyword + '%'    
                                    )    
                 
                 
                DECLARE @VfirstRec AS INTEGER          
                DECLARE @VlastRec AS INTEGER          
          
                SET @VfirstRec = ( @CurrentPage - 1 ) * @PageSize          
                SET @VlastRec = ( @CurrentPage * @PageSize + 1 )          
                SET @TotRecs = ( SELECT COUNT(*)    
                                 FROM   #tempVItems    
                               )          
                     
                            
                SELECT  *    
                FROM    ( SELECT    * ,    
                                    ROW_NUMBER() OVER ( ORDER BY ItemName ) AS RowNumber    
                          FROM      #tempVItems    
                        ) a    
                WHERE   RowNumber > @VfirstRec    
                        AND RowNumber < @VlastRec    
                ORDER BY ItemName               
                            
                    
                DROP TABLE #tempVItems          
                 
                 
            END           
                    
                    
                      
    END 


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- created By Anoop Jayaraj  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE  xtype = 'V' AND NAME ='View_ShippingBox')
DROP VIEW View_ShippingBox
GO
CREATE VIEW View_ShippingBox
AS 

    SELECT  SB.[numBoxID],
			SRI.ShippingReportItemId,
            SB.[vcBoxName],
            SB.[numShippingReportId],
            SB.[fltTotalWeight],
            SB.[fltHeight],
            SB.[fltWidth],
            SB.[fltLength],
			SB.dtDeliveryDate,
			ISNULL(SRI.monShippingRate,ISNULL(SB.monShippingRate,0)) [monShippingRate],
			SB.vcShippingLabelImage,
            SB.vcTrackingNumber,
            SRI.[numOppBizDocItemID],
            OBI.[numItemCode],
			SRI.[fltTotalWeight] fltTotalWeightItem,
            SRI.[fltHeight] fltHeightItem,
            SRI.[fltWidth] fltWidthItem,
            SRI.[fltLength] fltLengthItem,
			OI.numOppId,
			OBI.numOppBizDocID,
            OI.[numoppitemtCode],
--            I.[fltHeight],
--            I.[fltWidth],
--            I.[fltLength],
            I.[fltWeight],
            ISNULL(OI.vcItemName,'') AS [vcItemName],
            OI.[vcModelID],
            ISNULL(OBI.[vcItemDesc],I.[vcItemName]) [vcItemDesc],
			SR.numDomainId,
			SR.tintPayorType,
			ISNULL(SR.vcPayorAccountNo,'') vcPayorAccountNo,
			ISNULL(SR.vcPayorZip,'') vcPayorZip,
			ISNULL(SR.numPayorCountry,0) numPayorCountry,
			ISNULL(SR.vcValue2,0) AS tintServiceType,
			ISNULL(SPT.numNsoftwarePackageTypeID,31) AS [numPackageTypeID],
			ISNULL(SPT.vcPackageName,'-') vcPackageName,
			ISNULL(SRI.intBoxQty,0) AS [intBoxQty],
			ISNULL(SB.numServiceTypeID,0) AS [numServiceTypeID],
			ISNULL(fltDimensionalWeight,0) AS [fltDimensionalWeight],
			CAST(ROUND((ISNULL(SRI.fltTotalWeight,0) * SRI.intBoxQty), 2) AS DECIMAL(9,2)) AS [fltTotalRegularWeight],
			ISNULL(SB.numShipCompany,0) AS [numShipCompany],
			CASE WHEN ISNULL(OI.numUOMId,0) = 0 AND ISNULL(I.[numSaleUnit],0) > 0
			THEN 
				(SELECT ISNULL(vcUnitName,'') FROM dbo.UOM 
				WHERE dbo.UOM.numUOMId = I.[numSaleUnit]
				AND dbo.UOM.numDomainId = SR.numDomainId) 
			WHEN ISNULL(OI.numUOMId,0) > 0 
			THEN
				(SELECT ISNULL(vcUnitName,'') FROM dbo.UOM 
				WHERE dbo.UOM.numUOMId = OI.numUOMId 
				AND dbo.UOM.numDomainId = SR.numDomainId) 
			ELSE
				(
					(CASE WHEN D.[charUnitSystem] = 'E' 
						  THEN (SELECT TOP 1 [U].[vcUnitName] FROM [dbo].[UOM] AS U WHERE [U].[vcUnitName] LIKE 'Unit%' AND [U].[numDomainId] = D.[numDomainId] AND [U].[tintUnitType] = 1) 
						  ELSE (SELECT TOP 1 [U].[vcUnitName] FROM [dbo].[UOM] AS U WHERE [U].[vcUnitName] LIKE 'Unit%' AND [U].[numDomainId] = D.[numDomainId] AND [U].[tintUnitType] = 2) 
					END )
				)
			END AS [vcUnitName],
			 CONVERT(VARCHAR, CONVERT(DECIMAL(18, 4), dbo.fn_UOMConversion(ISNULL(OI.numUOMId, 0), I.numItemCode,SR.numDomainId, ISNULL(I.numBaseUnit, 0)) * OBI.monPrice)) monUnitPrice
    FROM    [ShippingBox] SB
            LEFT JOIN [ShippingReportItems] SRI ON SB.[numBoxID] = SRI.[numBoxID]
			LEFT JOIN dbo.ShippingReport SR ON SRI.numShippingReportId = SR.numShippingReportId
            LEFT JOIN [OpportunityBizDocItems] OBI ON SRI.[numOppBizDocItemID] = OBI.[numOppBizDocItemID]
            LEFT JOIN [OpportunityItems] OI ON OBI.[numOppItemID] = OI.[numoppitemtCode]
			LEFT JOIN  ShippingPackageType SPT ON SB.numPackageTypeID = SPT.ID AND SB.numShipCompany = SPT.numShippingCompanyID
            LEFT JOIN [Item] I ON OBI.[numItemCode] = I.[numItemCode]	
			JOIN [dbo].[Domain] AS D ON I.[numDomainID] = D.[numDomainId]	
GO            
