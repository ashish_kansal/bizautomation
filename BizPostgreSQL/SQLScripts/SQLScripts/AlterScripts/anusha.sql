
------------------------------------------------------
------------- ADD NEW ALTER SCRIPTS ABOVE ------------
------------------------------------------------------

--BEGIN TRY
--BEGIN TRANSACTION
--begin
-- --add new values for slide 1
-- DECLARE @numFieldID AS NUMERIC(18,0)
-- DECLARE @numFormFieldID AS NUMERIC(18,0)

-- INSERT INTO DycFieldMaster
-- (numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcListItemType,numListID,[order],bitInResults,
-- bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting)
-- VALUES
-- (6,'To','vcTo','vcTo','EmailHistory','V','R','TextArea','',0,10,1,0,0,1,1,0,0,1)
 
-- SELECT @numFieldID = SCOPE_IDENTITY()

-- INSERT INTO DynamicFormFieldMaster
-- (numFormID,vcFormFieldName,vcFieldType,vcAssociatedControlType,vcDbColumnName,vcListItemType,numListID,bitDeleted,vcFieldDataType,vcOrigDbColumnName,bitAllowEdit,
-- vcLookBackTableName,bitDefault,[order],bitInResults,numDomainID,bitSettingField,bitAddField,bitDetailField,bitAllowSorting)
-- VALUES
-- (44,'To','R','TextBox','vcTo','',0,0,'V','vcTo',0,'EmailHistory',1,10,1,0,1,0,0,1)

-- SELECT @numFormFieldID = SCOPE_IDENTITY()

-- INSERT INTO DycFormField_Mapping
-- (numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,
-- bitDetailField,bitAllowSorting,bitAllowFiltering,numFormFieldID)
-- VALUES
-- (6,@numFieldID,44,0,0,'To','TextArea',10,1,0,1,1,0,0,1,1,@numFormFieldID)
 
--end
--COMMIT
--END TRY
--BEGIN CATCH
-- IF @@TRANCOUNT > 0
--  ROLLBACK TRANSACTION;

-- SELECT 
--  ERROR_MESSAGE(),
--  ERROR_NUMBER(),
--  ERROR_SEVERITY(),
--  ERROR_STATE(),
--  ERROR_LINE(),
--  ISNULL(ERROR_PROCEDURE(), '-');
--END CATCH

--GO


------------------------------------------------------
------------- ADD NEW ALTER SCRIPTS ABOVE ------------
------------------------------------------------------

--Update DycFieldMaster set vcFieldName = 'Contact First Name' WHERE numFieldId  = 51
--Update DycFieldMaster set vcFieldName = 'Contact Last Name' WHERE numFieldId  = 52

--Update DynamicFormFieldMaster set vcFormFieldName = 'Contact First Name' where numFormFieldId in (SELECT numFormFieldID FROM DycFormField_Mapping WHERE numFieldID IN (51) and numFormID in  (1,10,56,36))
--Update DynamicFormFieldMaster set vcFormFieldName = 'Contact Last Name' where numFormFieldId in (SELECT numFormFieldID FROM DycFormField_Mapping WHERE numFieldID IN (52) and numFormID in  (1,10,56,36))

--UPDATE DynamicFormField_Validation SET vcNewFormFieldName = 'Contact First Name' WHERE numFormFieldId = 51 AND vcNewFormFieldName = 'First Name'
--UPDATE DynamicFormField_Validation SET vcNewFormFieldName = 'Contact Last Name' WHERE numFormFieldId = 52 AND vcNewFormFieldName = 'Last Name'


--UPDATE DycFormField_Mapping SET vcFieldName = 'Contact First Name' WHERE numFieldID = 51 AND vcFieldName = 'First Name'
--UPDATE DycFormField_Mapping SET vcFieldName = 'Contact Last Name' WHERE numFieldID = 52 AND vcFieldName = 'Last Name'



--delete from BizFormWizardModule where vcModule like '%Opportunities%'
--delete from BizFormWizardModule where vcModule like '%Customer & Partner Portal%' 
--delete from ModuleMaster where vcModuleName like '%Subscription%'
--update ModuleMaster set vcModuleName = 'Messaging Module' where vcModuleName like '%Outlook%'



------------------------------------------------------
------------- ADD NEW ALTER SCRIPTS ABOVE ------------
------------------------------------------------------

--/****** Object:  Table [dbo].[PricingTable]    Script Date: 08-05-2017 00:11:38 ******/
--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

--SET ANSI_PADDING ON
--GO

--CREATE TABLE [dbo].[PricingNamesTable](
--	[pricingNamesID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
--	[numDomainID] [numeric](18, 0) NOT NULL,
--	[numCreatedBy] [numeric](18, 0) NOT NULL,
--	[dtCreatedDate] [datetime] NOT NULL,
--	[numModifiedBy] [numeric](18, 0) NULL,
--	[dtModifiedDate] [datetime] NULL,
--	[tintPriceLevel] [tinyint] NOT NULL,
--	[vcPriceLevelName] [varchar](300) NULL
-- CONSTRAINT [PK_PricingNamesTable] PRIMARY KEY CLUSTERED 
--(
--	[pricingNamesID] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
--) ON [PRIMARY]

--GO

--SET ANSI_PADDING ON
--GO
