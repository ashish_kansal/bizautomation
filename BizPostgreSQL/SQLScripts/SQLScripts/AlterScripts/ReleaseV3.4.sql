/******************************************************************
Project: Release 3.4 Date: 07.08.2014
Comments: 
*******************************************************************/

------------------- SACHIN ------------------
--contacts

ALTER TABLE dbo.ListDetails ADD
	numListType numeric(9,0) NULL
		
ALTER TABLE dbo.AdditionalContactsInformation
ENABLE CHANGE_TRACKING 
WITH (TRACK_COLUMNS_UPDATED = ON);


--Projects
ALTER TABLE dbo.ProjectsMaster
ENABLE CHANGE_TRACKING 
WITH (TRACK_COLUMNS_UPDATED = ON);

--Communication
INSERT INTO dbo.DynamicFormMaster	
        ( numFormId ,
          vcFormName ,
          cCustomFieldsAssociated ,
          cAOIAssociated ,
          bitDeleted ,
          tintFlag ,
          bitWorkFlow ,
          vcLocationID ,
          bitAllowGridColor
        )
VALUES  ( 124 , -- numFormId - numeric
          'Action Items' , -- vcFormName - nvarchar(50)
          'Y' , -- cCustomFieldsAssociated - char(1)
          'N' , -- cAOIAssociated - char(1)
          0 , -- bitDeleted - bit
          3 , -- tintFlag - tinyint
          0 , -- bitWorkFlow - bit
          '2,6' , -- vcLocationID - varchar(50)
          0  -- bitAllowGridColor - bit
        )


ALTER TABLE dbo.Communication
ENABLE CHANGE_TRACKING 
WITH (TRACK_COLUMNS_UPDATED = ON);

ALTER TABLE dbo.PackagingRules ADD
	numShippingRuleID numeric(18,0) NULL
Update  PageNavigationDTL set vcPageNavName= 'Packaging Automation' where numPageNavID=199
Update  PageNavigationDTL set vcPageNavName= 'Packaging/Box Rules' where numPageNavID=201
--UPDATE  PageNavigationDTL set bitVisible=0 where numPageNavID=202

---------------------------------------------

------------------- SANDEEP ------------------


--------------------------------------------------------------------------
/**** New Order Form- Configuration to show hide fields in section 1 ****/
--------------------------------------------------------------------------

ALTER TABLE SalesOrderConfiguration ADD
bitDisplayCurrency BIT NOT NULL DEFAULT (1),
bitDisplayAssignTo BIT NOT NULL DEFAULT (1),
bitDisplayTemplate BIT NOT NULL DEFAULT (1)


---------------------------------------------------------------------------------------------
/**** Manage Authorization - Add Layout Button and Grid Configuration Visibility Rights ****/
---------------------------------------------------------------------------------------------

BEGIN TRANSACTION
-- 1. Leads - ModileID = 2

	INSERT INTO PageMaster 
		(numPageID, numModuleID, vcFileName, vcPageDesc, bitIsViewApplicable, bitIsAddApplicable, bitIsUpdateApplicable, bitIsDeleteApplicable, bitIsExportApplicable)
	VALUES
		(11,2,'','Layout Button',1,0,0,0,0)

	INSERT INTO PageMaster 
		(numPageID, numModuleID, vcFileName, vcPageDesc, bitIsViewApplicable, bitIsAddApplicable, bitIsUpdateApplicable, bitIsDeleteApplicable, bitIsExportApplicable)
	VALUES
		(12,2,'','Grid Configuration',1,0,0,0,0)


-- 2. Prospects - ModileID = 3

	INSERT INTO PageMaster 
		(numPageID, numModuleID, vcFileName, vcPageDesc, bitIsViewApplicable, bitIsAddApplicable, bitIsUpdateApplicable, bitIsDeleteApplicable, bitIsExportApplicable)
	VALUES
		(14,3,'','Layout Button',1,0,0,0,0)

	INSERT INTO PageMaster 
		(numPageID, numModuleID, vcFileName, vcPageDesc, bitIsViewApplicable, bitIsAddApplicable, bitIsUpdateApplicable, bitIsDeleteApplicable, bitIsExportApplicable)
	VALUES
		(15,3,'','Grid Configuration',1,0,0,0,0)
	
-- 3. Accounts - ModileID = 4

	INSERT INTO PageMaster 
		(numPageID, numModuleID, vcFileName, vcPageDesc, bitIsViewApplicable, bitIsAddApplicable, bitIsUpdateApplicable, bitIsDeleteApplicable, bitIsExportApplicable)
	VALUES
		(16,4,'','Layout Button',1,0,0,0,0)

	INSERT INTO PageMaster 
		(numPageID, numModuleID, vcFileName, vcPageDesc, bitIsViewApplicable, bitIsAddApplicable, bitIsUpdateApplicable, bitIsDeleteApplicable, bitIsExportApplicable)
	VALUES
		(17,4,'','Grid Configuration',1,0,0,0,0)

-- 4. Support - ModileID = 7

	INSERT INTO PageMaster 
		(numPageID, numModuleID, vcFileName, vcPageDesc, bitIsViewApplicable, bitIsAddApplicable, bitIsUpdateApplicable, bitIsDeleteApplicable, bitIsExportApplicable)
	VALUES
		(12,7,'','Layout Button',1,0,0,0,0)

	INSERT INTO PageMaster	
		(numPageID, numModuleID, vcFileName, vcPageDesc, bitIsViewApplicable, bitIsAddApplicable, bitIsUpdateApplicable, bitIsDeleteApplicable, bitIsExportApplicable)
	VALUES
		(13,7,'','Grid Configuration',1,0,0,0,0)

-- 5. Projects - ModileID = 12

	INSERT INTO PageMaster 
		(numPageID, numModuleID, vcFileName, vcPageDesc, bitIsViewApplicable, bitIsAddApplicable, bitIsUpdateApplicable, bitIsDeleteApplicable, bitIsExportApplicable)
	VALUES
		(13,12,'','Layout Button',1,0,0,0,0)

	INSERT INTO PageMaster 
		(numPageID, numModuleID, vcFileName, vcPageDesc, bitIsViewApplicable, bitIsAddApplicable, bitIsUpdateApplicable, bitIsDeleteApplicable, bitIsExportApplicable)
	VALUES
		(14,12,'','Grid Configuration',1,0,0,0,0)

-- 6. Opportunities/Orders - ModileID = 10

	INSERT INTO PageMaster 
		(numPageID, numModuleID, vcFileName, vcPageDesc, bitIsViewApplicable, bitIsAddApplicable, bitIsUpdateApplicable, bitIsDeleteApplicable, bitIsExportApplicable)
	VALUES
		(32,10,'','Layout Button',1,0,0,0,0)

	INSERT INTO PageMaster 
		(numPageID, numModuleID, vcFileName, vcPageDesc, bitIsViewApplicable, bitIsAddApplicable, bitIsUpdateApplicable, bitIsDeleteApplicable, bitIsExportApplicable)
	VALUES
		(33,10,'','Grid Configuration',1,0,0,0,0)

-- 7. Tickler - ModileID = 1

	INSERT INTO PageMaster 
		(numPageID, numModuleID, vcFileName, vcPageDesc, bitIsViewApplicable, bitIsAddApplicable, bitIsUpdateApplicable, bitIsDeleteApplicable, bitIsExportApplicable)
	VALUES
		(10,1,'','Grid Configuration',1,0,0,0,0)


/*************************** Allow Edit Unit Price access to all users for all domains *******************************/

SELECT * INTO #temp FROM
(
	SELECT 
		ROW_NUMBER() OVER(ORDER BY numDomainId DESC) AS Row, 
		numDomainId 
	FROM 
		Domain 
	WHERE 
		numDomainId <> -255
) TABLE2
	
DECLARE @RowCount INT
SET @RowCount = (SELECT COUNT(Row) FROM #temp) 
	
DECLARE @I INT
DECLARE @numDomainId NUMERIC(18,0)
	
SET @I = 1

WHILE (@I <= @RowCount)
BEGIN
			
	SELECT @numDomainId = numDomainID FROM #temp WHERE Row = @I
	
	-- 1. Leads - ModileID = 2
	INSERT INTO dbo.GroupAuthorization
		(numDomainID,numModuleID,numPageID,numGroupID,intExportAllowed,intPrintAllowed,intViewAllowed,intAddAllowed,intUpdateAllowed,intDeleteAllowed)
	SELECT
		@numDomainId,2,11,numGroupID,0,0,3,0,0,0
	FROM
		AuthenticationGroupMaster
	WHERE
		numDomainID = @numDomainId AND
		tintGroupType = 1
	INSERT INTO dbo.GroupAuthorization
		(numDomainID,numModuleID,numPageID,numGroupID,intExportAllowed,intPrintAllowed,intViewAllowed,intAddAllowed,intUpdateAllowed,intDeleteAllowed)
	SELECT
		@numDomainId,2,12,numGroupID,0,0,3,0,0,0
	FROM
		AuthenticationGroupMaster
	WHERE
		numDomainID = @numDomainId AND
		tintGroupType = 1

	-- 2. Prospects - ModileID = 3
	INSERT INTO dbo.GroupAuthorization
		(numDomainID,numModuleID,numPageID,numGroupID,intExportAllowed,intPrintAllowed,intViewAllowed,intAddAllowed,intUpdateAllowed,intDeleteAllowed)
	SELECT
		@numDomainId,3,14,numGroupID,0,0,3,0,0,0
	FROM
		AuthenticationGroupMaster
	WHERE
		numDomainID = @numDomainId AND
		tintGroupType = 1
	INSERT INTO dbo.GroupAuthorization
		(numDomainID,numModuleID,numPageID,numGroupID,intExportAllowed,intPrintAllowed,intViewAllowed,intAddAllowed,intUpdateAllowed,intDeleteAllowed)
	SELECT
		@numDomainId,3,15,numGroupID,0,0,3,0,0,0
	FROM
		AuthenticationGroupMaster
	WHERE
		numDomainID = @numDomainId AND
		tintGroupType = 1

	-- 3. Accounts - ModileID = 4
	INSERT INTO dbo.GroupAuthorization
		(numDomainID,numModuleID,numPageID,numGroupID,intExportAllowed,intPrintAllowed,intViewAllowed,intAddAllowed,intUpdateAllowed,intDeleteAllowed)
	SELECT
		@numDomainId,4,16,numGroupID,0,0,3,0,0,0
	FROM
		AuthenticationGroupMaster
	WHERE
		numDomainID = @numDomainId AND
		tintGroupType = 1
	INSERT INTO dbo.GroupAuthorization
		(numDomainID,numModuleID,numPageID,numGroupID,intExportAllowed,intPrintAllowed,intViewAllowed,intAddAllowed,intUpdateAllowed,intDeleteAllowed)
	SELECT
		@numDomainId,4,17,numGroupID,0,0,3,0,0,0
	FROM
		AuthenticationGroupMaster
	WHERE
		numDomainID = @numDomainId AND
		tintGroupType = 1

	-- 4. Support - ModileID = 7	
	INSERT INTO dbo.GroupAuthorization
		(numDomainID,numModuleID,numPageID,numGroupID,intExportAllowed,intPrintAllowed,intViewAllowed,intAddAllowed,intUpdateAllowed,intDeleteAllowed)
	SELECT
		@numDomainId,7,12,numGroupID,0,0,3,0,0,0
	FROM
		AuthenticationGroupMaster
	WHERE
		numDomainID = @numDomainId AND
		tintGroupType = 1
	INSERT INTO dbo.GroupAuthorization
		(numDomainID,numModuleID,numPageID,numGroupID,intExportAllowed,intPrintAllowed,intViewAllowed,intAddAllowed,intUpdateAllowed,intDeleteAllowed)
	SELECT
		@numDomainId,7,13,numGroupID,0,0,3,0,0,0
	FROM
		AuthenticationGroupMaster
	WHERE
		numDomainID = @numDomainId AND
		tintGroupType = 1

	-- 5. Projects - ModileID = 12
	INSERT INTO dbo.GroupAuthorization
		(numDomainID,numModuleID,numPageID,numGroupID,intExportAllowed,intPrintAllowed,intViewAllowed,intAddAllowed,intUpdateAllowed,intDeleteAllowed)
	SELECT
		@numDomainId,12,13,numGroupID,0,0,3,0,0,0
	FROM
		AuthenticationGroupMaster
	WHERE
		numDomainID = @numDomainId AND
		tintGroupType = 1
	INSERT INTO dbo.GroupAuthorization
		(numDomainID,numModuleID,numPageID,numGroupID,intExportAllowed,intPrintAllowed,intViewAllowed,intAddAllowed,intUpdateAllowed,intDeleteAllowed)
	SELECT
		@numDomainId,12,14,numGroupID,0,0,3,0,0,0
	FROM
		AuthenticationGroupMaster
	WHERE
		numDomainID = @numDomainId AND
		tintGroupType = 1

	-- 6. Opportunities/Orders - ModileID = 10
	INSERT INTO dbo.GroupAuthorization
		(numDomainID,numModuleID,numPageID,numGroupID,intExportAllowed,intPrintAllowed,intViewAllowed,intAddAllowed,intUpdateAllowed,intDeleteAllowed)
	SELECT
		@numDomainId,10,32,numGroupID,0,0,3,0,0,0
	FROM
		AuthenticationGroupMaster
	WHERE
		numDomainID = @numDomainId AND
		tintGroupType = 1
	INSERT INTO dbo.GroupAuthorization
		(numDomainID,numModuleID,numPageID,numGroupID,intExportAllowed,intPrintAllowed,intViewAllowed,intAddAllowed,intUpdateAllowed,intDeleteAllowed)
	SELECT
		@numDomainId,10,33,numGroupID,0,0,3,0,0,0
	FROM
		AuthenticationGroupMaster
	WHERE
		numDomainID = @numDomainId AND
		tintGroupType = 1

	-- 7. Tickler - ModileID = 1
	INSERT INTO dbo.GroupAuthorization
		(numDomainID,numModuleID,numPageID,numGroupID,intExportAllowed,intPrintAllowed,intViewAllowed,intAddAllowed,intUpdateAllowed,intDeleteAllowed)
	SELECT
		@numDomainId,1,10,numGroupID,0,0,3,0,0,0
	FROM
		AuthenticationGroupMaster
	WHERE
		numDomainID = @numDomainId AND
		tintGroupType = 1

	SET @I = @I  + 1
END
	
DROP TABLE #temp


ROLLBACK

----------------------------------------------
/**** BizForm Wizard add module dropdown ****/
----------------------------------------------

/****** Object:  Table [dbo].[BizFormWizardModule]    Script Date: 21-Jul-14 2:01:33 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[BizFormWizardModule](
	[numBizFormModuleID] [int] IDENTITY(1,1) NOT NULL,
	[vcModule] [nvarchar](200) NOT NULL,
 CONSTRAINT [PK_BizFormWizardModule] PRIMARY KEY CLUSTERED 
(
	[numBizFormModuleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

INSERT INTO BizFormWizardModule VALUES ('Search (All Modules)')
INSERT INTO BizFormWizardModule VALUES ('Organizations & Contacts')
INSERT INTO BizFormWizardModule VALUES ('Projects, Cases, & Tickler')
INSERT INTO BizFormWizardModule VALUES ('Opportunities, Orders, & BizDocs')
INSERT INTO BizFormWizardModule VALUES ('Items')
INSERT INTO BizFormWizardModule VALUES ('E-Commerce')
INSERT INTO BizFormWizardModule VALUES ('Web to Lead & Survey Forms')
INSERT INTO BizFormWizardModule VALUES ('Customer & Partner Portal')
INSERT INTO BizFormWizardModule VALUES ('Custom Relationships')

/*** Add numBizFormModuleID column to DynamicFormMaster for parent child relationship in BizForm Wizard in domain details ****/

ALTER TABLE DynamicFormMaster ADD
numBizFormModuleID INT NULL


UPDATE DynamicFormMaster SET numBizFormModuleID = 1 WHERE numFormId IN (1,6,15,17,18,29,30,59)
UPDATE DynamicFormMaster SET numBizFormModuleID = 2 WHERE numFormId IN (56)
UPDATE DynamicFormMaster SET numBizFormModuleID = 4 WHERE numFormId IN (7,8,58)
UPDATE DynamicFormMaster SET numBizFormModuleID = 5 WHERE numFormId IN (67)
UPDATE DynamicFormMaster SET numBizFormModuleID = 6 WHERE numFormId IN (77,84,85)
UPDATE DynamicFormMaster SET numBizFormModuleID = 7 WHERE numFormId IN (3,5)
UPDATE DynamicFormMaster SET numBizFormModuleID = 8 WHERE numFormId IN (78,79,80,82)


INSERT INTO DynamicFormMaster ([numFormId],[vcFormName],[cCustomFieldsAssociated],[cAOIAssociated],[bitDeleted],[tintFlag],numBizFormModuleID) VALUES (99,'Lead Details','Y','N',0,0,2)
INSERT INTO DynamicFormMaster ([numFormId],[vcFormName],[cCustomFieldsAssociated],[cAOIAssociated],[bitDeleted],[tintFlag],numBizFormModuleID) VALUES (100,'Prospect & Custom Relationships Details','Y','N',0,0,2)
INSERT INTO DynamicFormMaster ([numFormId],[vcFormName],[cCustomFieldsAssociated],[cAOIAssociated],[bitDeleted],[tintFlag],numBizFormModuleID) VALUES (101,'Account Details','Y','N',0,0,2)
INSERT INTO DynamicFormMaster ([numFormId],[vcFormName],[cCustomFieldsAssociated],[cAOIAssociated],[bitDeleted],[tintFlag],numBizFormModuleID) VALUES (102,'Contact Details','Y','N',0,0,2)
INSERT INTO DynamicFormMaster ([numFormId],[vcFormName],[cCustomFieldsAssociated],[cAOIAssociated],[bitDeleted],[tintFlag],numBizFormModuleID) VALUES (103,'Project Details','Y','N',0,0,3)
INSERT INTO DynamicFormMaster ([numFormId],[vcFormName],[cCustomFieldsAssociated],[cAOIAssociated],[bitDeleted],[tintFlag],numBizFormModuleID) VALUES (104,'Case Details','Y','N',0,0,3)
INSERT INTO DynamicFormMaster ([numFormId],[vcFormName],[cCustomFieldsAssociated],[cAOIAssociated],[bitDeleted],[tintFlag],numBizFormModuleID) VALUES (105,'Sales Opportunity Details','Y','N',0,0,4)
INSERT INTO DynamicFormMaster ([numFormId],[vcFormName],[cCustomFieldsAssociated],[cAOIAssociated],[bitDeleted],[tintFlag],numBizFormModuleID) VALUES (106,'Sales Order Details','Y','N',0,0,4)
INSERT INTO DynamicFormMaster ([numFormId],[vcFormName],[cCustomFieldsAssociated],[cAOIAssociated],[bitDeleted],[tintFlag],numBizFormModuleID) VALUES (107,'Purchase Opportunity Details','Y','N',0,0,4)
INSERT INTO DynamicFormMaster ([numFormId],[vcFormName],[cCustomFieldsAssociated],[cAOIAssociated],[bitDeleted],[tintFlag],numBizFormModuleID) VALUES (108,'Purchase Order Details','Y','N',0,0,4)
INSERT INTO DynamicFormMaster ([numFormId],[vcFormName],[cCustomFieldsAssociated],[cAOIAssociated],[bitDeleted],[tintFlag],numBizFormModuleID) VALUES (109,'Lead Grid Columns','Y','N',0,0,2)
INSERT INTO DynamicFormMaster ([numFormId],[vcFormName],[cCustomFieldsAssociated],[cAOIAssociated],[bitDeleted],[tintFlag],numBizFormModuleID) VALUES (110,'Prospect Grid Columns','Y','N',0,0,2)
INSERT INTO DynamicFormMaster ([numFormId],[vcFormName],[cCustomFieldsAssociated],[cAOIAssociated],[bitDeleted],[tintFlag],numBizFormModuleID) VALUES (111,'Account Grid Columns','Y','N',0,0,2)
INSERT INTO DynamicFormMaster ([numFormId],[vcFormName],[cCustomFieldsAssociated],[cAOIAssociated],[bitDeleted],[tintFlag],numBizFormModuleID) VALUES (112,'Vendor Grid Columns','Y','N',0,0,2)
INSERT INTO DynamicFormMaster ([numFormId],[vcFormName],[cCustomFieldsAssociated],[cAOIAssociated],[bitDeleted],[tintFlag],numBizFormModuleID) VALUES (113,'Employer Grid Columns','Y','N',0,0,2)
INSERT INTO DynamicFormMaster ([numFormId],[vcFormName],[cCustomFieldsAssociated],[cAOIAssociated],[bitDeleted],[tintFlag],numBizFormModuleID) VALUES (114,'Contact Grid Columns','Y','N',0,0,2)
INSERT INTO DynamicFormMaster ([numFormId],[vcFormName],[cCustomFieldsAssociated],[cAOIAssociated],[bitDeleted],[tintFlag],numBizFormModuleID) VALUES (115,'Project Grid Columns','Y','N',0,0,3)
INSERT INTO DynamicFormMaster ([numFormId],[vcFormName],[cCustomFieldsAssociated],[cAOIAssociated],[bitDeleted],[tintFlag],numBizFormModuleID) VALUES (116,'Case Grid Columns','Y','N',0,0,3)
INSERT INTO DynamicFormMaster ([numFormId],[vcFormName],[cCustomFieldsAssociated],[cAOIAssociated],[bitDeleted],[tintFlag],numBizFormModuleID) VALUES (117,'Tickler Grid Columns','Y','N',0,0,3)
INSERT INTO DynamicFormMaster ([numFormId],[vcFormName],[cCustomFieldsAssociated],[cAOIAssociated],[bitDeleted],[tintFlag],numBizFormModuleID) VALUES (118,'Sales Opportunity Grid Columns','Y','N',0,0,4)
INSERT INTO DynamicFormMaster ([numFormId],[vcFormName],[cCustomFieldsAssociated],[cAOIAssociated],[bitDeleted],[tintFlag],numBizFormModuleID) VALUES (119,'Sales Order Grid Columns','Y','N',0,0,4)
INSERT INTO DynamicFormMaster ([numFormId],[vcFormName],[cCustomFieldsAssociated],[cAOIAssociated],[bitDeleted],[tintFlag],numBizFormModuleID) VALUES (120,'Purchase Opportunity Grid Columns','Y','N',0,0,4)
INSERT INTO DynamicFormMaster ([numFormId],[vcFormName],[cCustomFieldsAssociated],[cAOIAssociated],[bitDeleted],[tintFlag],numBizFormModuleID) VALUES (121,'Purchase Order Grid Columns','Y','N',0,0,4)
INSERT INTO DynamicFormMaster ([numFormId],[vcFormName],[cCustomFieldsAssociated],[cAOIAssociated],[bitDeleted],[tintFlag],numBizFormModuleID) VALUES (122,'Prods.& Srvcs.Grid (Opp/Order)','Y','N',0,0,4)

GO

/****** Object:  Table [dbo].[BizFormWizardMasterConfiguration]    Script Date: 23-Jul-14 12:17:41 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[BizFormWizardMasterConfiguration](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[numFormID] [numeric](18, 0) NOT NULL,
	[numFieldID] [numeric](18, 0) NOT NULL,
	[intColumnNum] [int] NOT NULL,
	[intRowNum] [int] NOT NULL,
	[numDomainID] [numeric](18, 0) NOT NULL,
	[numGroupID] [numeric](18, 0) NOT NULL,
	[numRelCntType] [numeric](18, 0) NOT NULL,
	[tintPageType] [tinyint] NULL,
	[bitCustom] [bit] NOT NULL,
	[bitGridConfiguration] [bit] NOT NULL,
 CONSTRAINT [PK_BizFormWizardMasterConfiguration] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO



------------------------------------------
---- Add/Edit Order Item Grid Columns ----
------------------------------------------

	INSERT INTO DynamicFormMaster ([numFormId],[vcFormName],[cCustomFieldsAssociated],[cAOIAssociated],[bitDeleted],[tintFlag]) VALUES (123,'Add/Edit Order - Item Grid Column Settings','Y','N',0,1)

	---- Maps fields for Add/Edit Order Item Grid Columns

	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 203 AND numFormID = 123)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (4,203,123,1,0,0,1)
	END
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 193 AND numFormID = 123)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (4,193,123,1,0,0,1)
	END
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 281 AND numFormID = 123)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (4,281,123,1,0,0,1)
	END
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 202 AND numFormID = 123)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (4,202,123,1,0,0,1)
	END
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 215 AND numFormID = 123)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (4,215,123,1,0,0,1)
	END
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 191 AND numFormID = 123)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (4,191,123,1,0,0,1)
	END
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 206 AND numFormID = 123)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (4,206,123,1,0,0,1)
	END
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 207 AND numFormID = 123)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (4,207,123,1,0,0,1)
	END
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 205 AND numFormID = 123)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (4,205,123,1,0,0,1)
	END
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 204 AND numFormID = 123)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (4,204,123,1,0,0,1)
	END
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 209 AND numFormID = 123)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (4,209,123,1,0,0,1)
	END
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 313 AND numFormID = 123)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (4,313,123,1,0,0,1)
	END
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 234 AND numFormID = 123)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (4,234,123,1,0,0,1)
	END
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 212 AND numFormID = 123)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (4,212,123,1,0,0,1)
	END
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 213 AND numFormID = 123)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (4,213,123,1,0,0,1)
	END
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 214 AND numFormID = 123)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (4,214,123,1,0,0,1)
	END
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 216 AND numFormID = 123)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (4,216,123,1,0,0,1)
	END
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 195 AND numFormID = 123)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (4,195,123,1,0,0,1)
	END
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 196 AND numFormID = 123)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (4,196,123,1,0,0,1)
	END
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 197 AND numFormID = 123)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (4,197,123,1,0,0,1)
	END
	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 198 AND numFormID = 123)
	BEGIN
		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (4,198,123,1,0,0,1)
	END


---------------------------------------------

------------------- MANISH ------------------

------/******************************************************************
------Project: BACRMUI   Date: 06.08.2014
------Comments: Adding Endicia Account mode into Shipping for specific use.
------*******************************************************************/
BEGIN TRANSACTION

SET IDENTITY_INSERT [dbo].[ShippingFields] ON
INSERT INTO [dbo].[ShippingFields]( [intShipFieldID] ,[vcFieldName] ,[numListItemID] ,[vcToolTip])
SELECT 21,'Is Endicia',90,'Used for identifying whether configured account is Endicia or not'
UNION
SELECT 25,'Endicia UserID',90,'Endicia UserID'
UNION
SELECT 23,'Endicia Password',90,'Endicia Password'
UNION
SELECT 22,'Endicia Label Server URL',90,'Used for Endicia Label Server URL'
UNION
SELECT 24,'Endicia Tracking URL',90,'Used for Endicia Tracking URL'
SET IDENTITY_INSERT dbo.DycFieldMaster OFF


--SELECT * FROM [dbo].[ShippingFieldValues] AS SFV WHERE [SFV].[numDomainID] = 1 AND [SFV].[numListItemID] = 90

DECLARE @numDomainID AS NUMERIC(18)
SELECT ROW_NUMBER() OVER (ORDER BY numDomainID)AS [RowID], numDomainID  INTO #tempDomains FROM dbo.Domain WHERE numDomainId > 0
DECLARE @intCnt AS INT
DECLARE @intCntDomain AS INT
SET @intCnt = 0
SET @intCntDomain = (SELECT COUNT(*) FROM #tempDomains)

IF @intCntDomain > 0
BEGIN
WHILE(@intCnt < @intCntDomain)
BEGIN

SET @intCnt = @intCnt + 1
SELECT @numDomainID = numDomainID FROM #tempDomains WHERE RowID = @intCnt
PRINT @numDomainID

INSERT INTO [dbo].[ShippingFieldValues]( [intShipFieldID] ,[vcShipFieldValue] ,[numDomainID] ,[numListItemID])
SELECT 21,'1',@numDomainID,90
UNION
SELECT 25,[SFV].[vcShipFieldValue],[SFV].[numDomainID],[SFV].[numListItemID] FROM [dbo].[ShippingFieldValues] AS SFV 
WHERE [SFV].[numDomainID] = @numDomainID 
AND [SFV].[numListItemID] = 90
AND sfv.[intShipFieldID] = 15
UNION
SELECT 23,[SFV].[vcShipFieldValue],[SFV].[numDomainID],[SFV].[numListItemID] FROM [dbo].[ShippingFieldValues] AS SFV 
WHERE [SFV].[numDomainID] = @numDomainID 
AND [SFV].[numListItemID] = 90
AND sfv.[intShipFieldID] = 16
UNION
SELECT 22,[SFV].[vcShipFieldValue],[SFV].[numDomainID],[SFV].[numListItemID] FROM [dbo].[ShippingFieldValues] AS SFV 
WHERE [SFV].[numDomainID] = @numDomainID 
AND [SFV].[numListItemID] = 90
AND sfv.[intShipFieldID] = 17
UNION
SELECT 24,[SFV].[vcShipFieldValue],[SFV].[numDomainID],[SFV].[numListItemID] FROM [dbo].[ShippingFieldValues] AS SFV 
WHERE [SFV].[numDomainID] = @numDomainID 
AND [SFV].[numListItemID] = 90
AND sfv.[intShipFieldID] = 18

END 

SELECT * FROM [dbo].[ShippingFieldValues] AS SFV 

END
DROP TABLE #tempDomains

ROLLBACK

--------------------------------------------------------------------------

------/******************************************************************
------Project: BACRMUI   Date: 04.08.2014
------Comments: ACCOUNTING ISSUE TRACING SCRIPTS
------*******************************************************************/

GO
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[GenealEntryAudit](
	[numDomainID] [numeric](18, 0) NOT NULL,
	[numJournalID] [numeric](18, 0) NOT NULL,
	[numTransactionID] [numeric](18, 0) NOT NULL,
	[dtCreatedDate] [datetime] NOT NULL,
	[varDescription] [varchar](1000) NOT NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING ON
GO

---------------------------------------------