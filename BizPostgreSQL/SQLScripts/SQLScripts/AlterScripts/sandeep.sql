ALTER TABLE PriceBookRules ADD bitRoundTo BOOLEAN DEFAULT FALSE;
ALTER TABLE PriceBookRules ADD tintRoundTo SMALLINT;
ALTER TABLE PriceBookRules ADD tintVendorCostType SMALLINT DEFAULT 1;
ALTER TABLE PricingTable ADD tintVendorCostType SMALLINT DEFAULT 1;
ALTER TABLE Domain ADD tintCommissionVendorCostType SMALLINT DEFAULT 1;
UPDATE DycFormField_Mapping SET vcFieldName='Ship-Via (Lead Time)' WHERE numFormID=139 AND vcFieldName = 'Method of Shipment (Lead Days)';
UPDATE PageMaster SET vcpagedesc='Manually Update Inventory',bitupdatepermission1applicable=false,bitupdatepermission2applicable=false WHERE numPageID=149 AND numModuleid=37;

------------------------------------------

UPDATE ListMaster SET bitDeleted=true WHERE numListID=338;

------------------------------------------

ALTER TABLE ITEM ADD bitPreventOrphanedParents BOOLEAN DEFAULT true;
ALTER TABLE ItemDetails ADD bitUseInNewItemConfiguration BOOLEAN DEFAULT true;
ALTER TABLE Domain ADD vcDefaultCostRange JSONB;

------------------------------------------

INSERT INTO CFw_Grp_Master
(
	Grp_Name,Loc_Id,numDomainID,tintType
)
SELECT 
	'Vendor Lead-Times'
	,13
	,numDomainID
	,2 
FROM 
	Domain;

------------------------------------------

INSERT INTO GroupTabDetails
(
	numGroupId,numTabId,numRelationShip,bitallowed,numOrder,tintType
)
SELECT
	T2.numGroupID,T1.Grp_id,0,true,13,1
FROM
	CFw_Grp_Master T1
INNER JOIN
	AuthenticationGroupMaster T2
ON	
	T1.numDomainID = T2.numDomainID
WHERE
	Loc_Id = 13
	AND Grp_Name='Vendor Lead-Times'
	AND tintType = 2
	AND T2.numDomainID NOT IN (-255,-1)
ORDER BY
	T2.numDomainID,T2.numGroupID;

------------------------------------------

INSERT INTO CFw_Grp_Master
(
	Grp_Name,Loc_Id,numDomainID,tintType
)
SELECT 
	'Vendor Lead-Times'
	,12
	,numDomainID
	,2 
FROM 
	Domain;

-------------------------------------------

INSERT INTO GroupTabDetails
(
	numGroupId,numTabId,numRelationShip,bitallowed,numOrder,tintType
)
SELECT
	T2.numGroupID,T1.Grp_id,0,true,10,1
FROM
	CFw_Grp_Master T1
INNER JOIN
	AuthenticationGroupMaster T2
ON	
	T1.numDomainID = T2.numDomainID
WHERE
	Loc_Id = 12
	AND Grp_Name='Vendor Lead-Times'
	AND tintType = 2
	AND T2.numDomainID NOT IN (-255,-1)
ORDER BY
	T2.numDomainID,T2.numGroupID;

---------------------------------------------

CREATE TABLE VendorLeadTimes (
  numvendorleadtimesid serial NOT NULL,
  numdivisionid numeric(18,0) NOT NULL,
  vcdata jsonb
);

---------------------------------------------

CREATE TABLE VendorCostTable (
  numvendorcosttableid serial NOT NULL,
  numVendorTcode numeric(18,0) NOT NULL REFERENCES Vendor (numVendorTcode) ON DELETE CASCADE,
  numCurrencyID NUMERIC(18,0) NOT NULL,
  intRow INT NOT NULL,
  intFromQty INT NOT NULL,
  intToQty INT NOT NULL,
  monStaticCost DECIMAL(20,5) NOT NULL,
  monDynamicCost DECIMAL(20,5) NOT NULL,
  numStaticCostModifiedBy numeric(18,0) NOT NULL,
  dtStaticCostModifiedDate TIMESTAMP NOT NULL,
  numDynamicCostModifiedBy numeric(18,0),
  dtDynamicCostModifiedDate TIMESTAMP,
  bitIsDynamicCostByOrder BOOLEAN
);

---------------------------------------------

-- New procedures

usp_item_createnewitemfromkitconfiguration
usp_vendorleadtimes_save
usp_vendorleadtimes_delete
usp_vendorleadtimes_getbyid
usp_vendorleadtimes_getbyvendor
fn_GetPOItemVendorLeadTimesDays

---------------------------------------------

-- UPGRADE IN POSTGRESQL VERSION
INSERT INTO DycFormField_Mapping
(
	nummoduleid,numfieldid,numformid,bitallowedit,bitinlineedit,vcfieldname,vcassociatedcontroltype,bitinresults,bitdeleted,bitdefault,bitsettingfield,bitaddfield,bitdetailfield,bitallowsorting,bitallowfiltering
)
VALUES
(
	3,968,38,true,true,'Project','SelectBox',true,false,false,true,false,true,true,true
)

-------------------------------------------------------------------------------------------------------------------------
------------------------------------------ PLECE NEW SCRIPTS ABOVE IT ---------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------

--ALTER TABLE ItemDetails ADD bitOrderEditable BIT DEFAULT 0

-------------------------------------------------------------------------------------------------------------------------
------------------------------------------ PLECE NEW SCRIPTS ABOVE IT ---------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------

--ALTER TABLE ItemDetails ADD bitUseInDynamicSKU BIT DEFAULT 0

---- ALREADY UPDATED ON PRODUCTION

--ALTER TABLE DivisionMaster ADD vcBuiltWithJson NVARCHAR(MAX)

--UPDATE DynamicFormMaster SET tintPageType=1 WHERE numBizFormModuleID=4 AND numFormId IN (7,8,26,58,118,119,120,121,129)
--UPDATE DynamicFormMaster SET tintPageType=3 WHERE numBizFormModuleID=4 AND numFormId IN (105,106,107,108,113)
-------------------------------------------------------------------------------------------------------------------------
------------------------------------------ PLECE NEW SCRIPTS ABOVE IT ---------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------

--UPDATE BizFormWizardModule SET tintSortOrder = tintSortOrder + 1 WHERE tintSortOrder >= 3

----------------------------------

--UPDATE BizFormWizardModule SET vcModule='Organizations' WHERE numBizFormModuleID=2

--UPDATE BizFormWizardModule SET vcModule='Projects & Cases' WHERE numBizFormModuleID=3

----------------------------------

--SET IDENTITY_INSERT BizFormWizardModule ON

--INSERT INTO BizFormWizardModule
--(
--	numBizFormModuleID,vcModule,tintSortOrder
--)
--VALUES
--(
--	12,'Contacts',3
--)

--SET IDENTITY_INSERT BizFormWizardModule OFF

---------------------------------

--UPDATE DynamicFormMaster SET numBizFormModuleID=12 WHERE numFormId IN (56,102,114)
--UPDATE DynamicFormMaster SET numBizFormModuleID=11 WHERE numFormId=117
--UPDATE BizFormWizardModule SET bitActive=0 WHERE numBizFormModuleID=9
--ALTER TABLE DynamicFormMaster ADD tintPageType TINYINT 


----------------------------------------

----TODO: Check form id on live before update
--UPDATE DynamicFormMaster SET tintPageType=3 WHERE numBizFormModuleID=2 AND numFormId IN (99,100,101,146,147)
--UPDATE DynamicFormMaster SET tintPageType=1 WHERE numBizFormModuleID=2 AND numFormId IN (109,110,111,112,113)

----------------------------------------

--INSERT INTO ReportFieldGroupMappingMaster
--(
--	numReportFieldGroupID,numFieldID,vcFieldName,vcAssociatedControlType,vcFieldDataType,bitAllowSorting,bitAllowGrouping,bitAllowAggregate,bitAllowFiltering
--)
--VALUES
--(
--	1,(SELECT TOP 1 numFieldId FROM DycFieldMaster WHERE vcOrigDbColumnName='vcLastSalesOrderDate'),'Last Order Date','DateField','D',1,0,0,1
--)


----------------------

--ALTER TABLE ItemDetails ADD tintView TINYINT DEFAULT 1
--ALTER TABLE ItemDetails ADD vcFields VARCHAR(500)

--UPDATE ItemDetails SET tintView = 1
------------------------------------------------------------------------------------------------------------------------
------------------------------------------ PLECE NEW SCRIPTS ABOVE IT --------------------------------------------------
------------------------------------------------------------------------------------------------------------------------

--ALTER TABLE Category ADD numItemGroup NUMERIC(18,0)

--INSERT INTO PageElementAttributes
--(
--	numElementID,vcAttributeName,vcControlType
--)
--VALUES
--(
--	7,'Apply PriceBook Price','CheckBox'
--)
------------------------------------------------------------------------------------------------------------------------
------------------------------------------ PLECE NEW SCRIPTS ABOVE IT --------------------------------------------------
------------------------------------------------------------------------------------------------------------------------

--INSERT INTO DynamicFormMaster
--(
--	numFormId,vcFormName,cCustomFieldsAssociated,cAOIAssociated,bitDeleted,tintFlag,numBizFormModuleID
--)
--VALUES
--(
--	146,'Vendor Details','Y','N',0,0,2
--)

--INSERT INTO DynamicFormMaster
--(
--	numFormId,vcFormName,cCustomFieldsAssociated,cAOIAssociated,bitDeleted,tintFlag,numBizFormModuleID
--)
--VALUES
--(
--	147,'Employer Details','Y','N',0,0,2
--)

----------------------

--UPDATE BizFormWizardMasterConfiguration SET numFormID=36 WHERE numRelCntType NOT IN (46,47,93) AND numRelCntType IN (SELECT numListItemID FROM ListDetails WHERE numListID=5 AND ISNULL(constFlag,0)=0) AND numFormID=35

---------------------------

--UPDATE DynamicFormMaster SET vcFormName='Prospect Details' WHERE numFormId=100

----------------------------

--ALTER TABLE Domain ADD tintReorderPointBasedOn TINYINT DEFAULT 1
--ALTER TABLE Domain ADD intReorderPointDays INT DEFAULT 30
--ALTER TABLE Domain ADD intReorderPointPercent INT DEFAULT 0
--ALTER TABLE Domain ADD bitUsePreviousEmailBizDoc BIT DEFAULT 1
--ALTER TABLE Item ADD bitAutomateReorderPoint BIT 
--ALTER TABLE Item ADD dtLastAutomateReorderPoint DATE
----------------------------------

--DECLARE @numFieldID INT

--INSERT INTO DycFieldMaster
--(
--	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcListItemType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault
--)
--VALUES
--(
--	4,'Automate Re-Order Point & Qty','bitAutomateReorderPoint','bitAutomateReorderPoint','IsAutomateReorderPoint','Item','Y','R','CheckBox','',0,1,0,1,0
--)

--SET @numFieldID = SCOPE_IDENTITY()

--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,bitInResults,bitDeleted,bitDefault,bitImport,bitAllowFiltering,intSectionID
--)
--VALUES
--(
--	4,@numFieldID,20,0,0,'Automate Re-Order Point & Qty','CheckBox',1,0,0,1,1,1
--)

----------------------------------

--ALTER TABLE MetaTags ADD vcPageTitle VARCHAR(4000)

----------------------------------

--USE [Production.2014]
--GO

--/****** Object:  Table [dbo].[DivisionMasterBizDocEmail]    Script Date: 03-Dec-20 10:18:53 AM ******/
--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

--SET ANSI_PADDING ON
--GO

--CREATE TABLE [dbo].[DivisionMasterBizDocEmail](
--	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
--	[numDomainID] [numeric](18, 0) NOT NULL,
--	[numDivisionID] [numeric](18, 0) NOT NULL,
--	[numBizDocID] [numeric](18, 0) NOT NULL,
--	[vcTo] [varchar](5000) NULL,
--	[vcCC] [varchar](5000) NULL,
--	[vcBCC] [varchar](5000) NULL,
-- CONSTRAINT [PK_DivisionMasterBizDocEmail] PRIMARY KEY CLUSTERED 
--(
--	[ID] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
--) ON [PRIMARY]

--GO

--SET ANSI_PADDING ON
--GO

-------------------------------------------

--DELETE FROM CFw_Grp_Master WHERE Loc_Id=2 AND tintType=2 AND Grp_Name='Sales Forecasts'

------------------------------------------------------------------------------------------------------------------------
------------------------------------------ PLECE NEW SCRIPTS ABOVE IT --------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
--UPDATE ErrorMaster SET vcErrorDesc = 'Password must have at least 8 characters and one upper case, lower case and special character (e.g. ! @ # $ & *)' WHERE numErrorID=52
--UPDATE ErrorMaster SET vcErrorDesc = 'Password and confirm password do not match' WHERE numErrorID=53

----------------------

--SET IDENTITY_INSERT ErrorMaster ON

--INSERT INTO ErrorMaster
--(
--	numErrorID,vcErrorCode,vcErrorDesc,tintApplication
--)
--VALUES
--(
--	146,'ERR082','Password must have at least 8 characters and one upper case, lower case and special character (e.g. ! @ # $ & *)',3
--)

--SET IDENTITY_INSERT ErrorMaster OFF

--UPDATE PageNavigationDTL SET vcPageNavName='Matrix Groups' WHERE vcPageNavName='Item Groups'
--UPDATE echannelhub SET vcMarketplace=CONCAT(vcMarketplace,' ','SellerCentral') WHERE ID=1
--UPDATE echannelhub SET vcMarketplace=CONCAT(vcMarketplace,' ','SellerCentral') WHERE ID=2
--UPDATE echannelhub SET vcMarketplace=CONCAT(vcMarketplace,' ','SellerCentral') WHERE ID=3
--UPDATE echannelhub SET vcMarketplace=CONCAT(vcMarketplace,' ','SellerCentral') WHERE ID=4

--INSERT INTO eChannelHub (ID,vcMarketplace,vcType) VALUES (59,'Amazon AU VendorCentral','Marketplaces')
--INSERT INTO eChannelHub (ID,vcMarketplace,vcType) VALUES (60,'Amazon CAN VendorCentral','Marketplaces')
--INSERT INTO eChannelHub (ID,vcMarketplace,vcType) VALUES (61,'Amazon UK VendorCentral','Marketplaces')
--INSERT INTO eChannelHub (ID,vcMarketplace,vcType) VALUES (62,'Amazon US VendorCentral','Marketplaces')

-------------------------------------------

--ALTER TABLE StagePercentageDetailsTaskNotes ADD bitDone BIT DEFAULT 0

-------------------------------------------

--UPDATE PaymentGateway SET vcGateWayName='CardConnect Account #1',vcThirdFldName='Display Name' WHERE intPaymentGateWay=9

-------------------------------------------

--SET IDENTITY_INSERT PaymentGateway ON

--INSERT INTO PaymentGateway
--(
--	intPaymentGateWay,vcGateWayName,vcFirstFldName,vcSecndFldName,vcThirdFldName
--)
--VALUES
--(
--	10,'CardConnect Account #2','Login','Password','Display Name'
--)

--SET IDENTITY_INSERT PaymentGateway OFF

-------------------------------------------

--ALTER TABLE TransactionHistory ADD intPaymentGateWay INT DEFAULT 0

-------------------------------------------

--ALTER TABLE OpportunityItems ADD vcASIN VARCHAR(100)

-------------------------------------------

--ALTER TABLE Domain ADD bitUsePredefinedCustomer BIT DEFAULT 0

-------------------------------------------

--USE [Production.2014]
--GO

--/****** Object:  Table [dbo].[DomainMarketplace]    Script Date: 05-Nov-20 2:26:29 PM ******/
--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

--CREATE TABLE [dbo].[DomainMarketplace](
--	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
--	[numDomainID] [numeric](18, 0) NOT NULL,
--	[numMarketplaceID] [numeric](18, 0) NOT NULL,
--	[numDivisionID] [numeric](18, 0) NULL,
-- CONSTRAINT [PK_DomainMarketplace] PRIMARY KEY CLUSTERED 
--(
--	[ID] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
--) ON [PRIMARY]

--GO

------------------------------------------------------------------------------------------------------------------------
------------------------------------------ PLECE NEW SCRIPTS ABOVE IT --------------------------------------------------
------------------------------------------------------------------------------------------------------------------------

--INSERT INTO dbo.EmailMergeFields
--(
--	vcMergeField,vcMergeFieldValue,numModuleID,tintModuleType
--)
--VALUES
--(
--	'Price Level Table','##PriceLevelTable##',3,1
--)

-------------------------------------------------

--ALTER TABLE Domain ADD bitReceiveOrderWithNonMappedItem BIT DEFAULT 0
--ALTER TABLE Domain ADD numItemToUseForNonMappedItem NUMERIC(18,0) DEFAULT 0
--ALTER TABLE OpportunityItems ADD bitMappingRequired BIT DEFAULT 0

-------------------------------------------------

--UPDATE DycFormField_Mapping SET bitDefault=0 WHERE numFormID=43 AND bitDefault=1

-------------------------------------------------

--DECLARE @numFieldID NUMERIC(18,0)

--INSERT INTO DycFieldMaster
--(
--	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcListItemType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering
--)
--VALUES
--(
--	8,'Type','TaskTypeName','TaskTypeName','TaskTypeName','T','V','R','SelectBox','',0,1,0,0,1,1,0,0,0,1
--)

--SET @numFieldID = SCOPE_IDENTITY()

--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering
--)
--VALUES
--(
--	8,@numFieldID,43,0,0,'Type','SelectBox',1,0,1,1,0,0,0,1
--)

--UPDATE DycFormConfigurationDetails SET numFieldId=@numFieldID WHERE numFormId=43 AND numFieldId=1

------------------------------------------------

--DECLARE @numFieldID NUMERIC(18,0)

--INSERT INTO DycFieldMaster
--(
--	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcListItemType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering
--)
--VALUES
--(
--	8,'Description','Description','Description','Description','T','V','R','TextArea','',0,1,0,1,0,1,0,0,0,1
--)

--SET @numFieldID = SCOPE_IDENTITY()

--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering
--)
--VALUES
--(
--	8,@numFieldID,43,0,0,'Description','TextArea',1,0,0,1,0,0,0,1
--)

--UPDATE DycFormConfigurationDetails SET numFieldId=@numFieldID WHERE numFormId=43 AND numFieldId=184

-------------------------------------------------

--DECLARE @numFieldID NUMERIC(18,0)

--INSERT INTO DycFieldMaster
--(
--	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcListItemType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering
--)
--VALUES
--(
--	8,'Due Date','DueDate','DueDate','DueDate','T','V','R','DateField','',0,1,0,0,1,1,0,0,1,1
--)

--SET @numFieldID = SCOPE_IDENTITY()

--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering
--)
--VALUES
--(
--	8,@numFieldID,43,0,0,'Due Date','DateField',1,0,1,1,0,0,1,1
--)

--UPDATE DycFormConfigurationDetails SET numFieldId=@numFieldID WHERE numFormId=43 AND numFieldId=3

-----------------------------------------------------------

--DECLARE @numFieldID NUMERIC(18,0)

--INSERT INTO DycFieldMaster
--(
--	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcListItemType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering
--)
--VALUES
--(
--	8,'Progress','TotalProgress','TotalProgress','TotalProgress','T','V','R','TextBox','',0,1,0,0,0,1,0,0,1,0
--)

--SET @numFieldID = SCOPE_IDENTITY()

--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering
--)
--VALUES
--(
--	8,@numFieldID,43,0,0,'Progress','TextBox',1,0,0,1,0,0,1,0
--)

--UPDATE DycFormConfigurationDetails SET numFieldId=@numFieldID WHERE numFormId=43 AND numFieldId=4

-------------------------------------------------

--DECLARE @numFieldID NUMERIC(18,0)

--INSERT INTO DycFieldMaster
--(
--	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcListItemType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering
--)
--VALUES
--(
--	8,'Assigned to','numAssignedTo','numAssignedTo','numAssignedTo','T','V','R','SelectBox','',0,1,0,0,0,1,0,0,0,1
--)

--SET @numFieldID = SCOPE_IDENTITY()

--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering
--)
--VALUES
--(
--	8,@numFieldID,43,0,0,'Assigned to','SelectBox',1,0,0,1,0,0,0,1
--)

--UPDATE DycFormConfigurationDetails SET numFieldId=@numFieldID WHERE numFormId=43 AND numFieldId=5

--------------------------------------------------------
--DECLARE @numFieldID NUMERIC(18,0)

--INSERT INTO DycFieldMaster
--(
--	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcListItemType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering
--)
--VALUES
--(
--	8,'Priority','Priority','Priority','Priority','T','V','R','SelectBox','',0,1,0,0,0,1,0,0,0,1
--)

--SET @numFieldID = SCOPE_IDENTITY()

--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering
--)
--VALUES
--(
--	8,@numFieldID,43,0,0,'Priority','SelectBox',1,0,0,1,0,0,0,1
--)

--UPDATE DycFormConfigurationDetails SET numFieldId=@numFieldID WHERE numFormId=43 AND numFieldId=183

-----------------------------------------------------------

--DECLARE @numFieldID NUMERIC(18,0)

--INSERT INTO DycFieldMaster
--(
--	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcListItemType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering
--)
--VALUES
--(
--	8,'Activity','Activity','Activity','Activity','T','V','R','SelectBox','',0,1,0,0,0,1,0,0,0,1
--)

--SET @numFieldID = SCOPE_IDENTITY()

--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering
--)
--VALUES
--(
--	8,@numFieldID,43,0,0,'Activity','SelectBox',1,0,0,1,0,0,0,1
--)

--UPDATE DycFormConfigurationDetails SET numFieldId=@numFieldID WHERE numFormId=43 AND numFieldId=182

--------------------------------------------------

--DECLARE @numFieldID NUMERIC(18,0)

--INSERT INTO DycFieldMaster
--(
--	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcListItemType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering
--)
--VALUES
--(
--	8,'Organization (Rating)','OrgName','OrgName','OrgName','T','V','R','TextBox','',0,1,0,0,0,1,0,0,1,1
--)

--SET @numFieldID = SCOPE_IDENTITY()

--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering
--)
--VALUES
--(
--	8,@numFieldID,43,0,0,'Organization (Rating)','TextBox',1,0,0,1,0,0,1,1
--)

--UPDATE DycFormConfigurationDetails SET numFieldId=@numFieldID WHERE numFormId=43 AND numFieldId=8

------------------------------------------------------------------------------------------------------------------------
------------------------------------------ PLECE NEW SCRIPTS ABOVE IT --------------------------------------------------
------------------------------------------------------------------------------------------------------------------------

--UPDATE OpportunityBizDocs SET dtFromDate=dtCreatedDate WHERE dtCreatedDate <> dtFromDate AND CAST(dtCreatedDate AS DATE) = CAST(dtFromDate AS DATE)

-------------------------------

--UPDATE PageNavigationDTL SET vcPageNavName='My Reports & Saved Searches' WHERE numParentID=22 AND vcPageNavName ='My Reports'
--UPDATE PageNavigationDTL SET bitVisible=0 WHERE numParentID=22 AND vcPageNavName = 'Recurring Transactions Report'
--UPDATE PageNavigationDTL SET bitVisible=0 WHERE numParentID=22 AND vcPageNavName = 'Sales Forecasts'
--UPDATE PageNavigationDTL SET vcPageNavName='Legacy Reports' WHERE numParentID=22 AND vcPageNavName = 'E-Commerce Reports'

-------------------------

--UPDATE PageNavigationDTL SET intSortOrder=1 WHERE numParentID=22 AND numPageNavID=24
--UPDATE PageNavigationDTL SET intSortOrder=2 WHERE numParentID=22 AND numPageNavID=92
--UPDATE PageNavigationDTL SET intSortOrder=3 WHERE numParentID=22 AND numPageNavID=245
--UPDATE PageNavigationDTL SET intSortOrder=4 WHERE numParentID=22 AND numPageNavID=246

-------------------------

--BEGIN TRY
--BEGIN TRANSACTION

--	DECLARE @numMAXPageNavID INT
--	SELECT @numMAXPageNavID = MAX(numPageNavID) FROM PageNavigationDTL


--	INSERT INTO PageNavigationDTL
--	(
--		numPageNavID,numModuleID,numParentID,vcPageNavName,vcNavURL,bitVisible,numTabID,intSortOrder
--	)
--	VALUES
--	(
--		(@numMAXPageNavID + 1),8,22,'Scheduled Reports & Saved Searches','../reports/frmScheduledReports.aspx',1,6,5
--	)

--	INSERT INTO dbo.TreeNavigationAuthorization
--	(
--		numGroupID,
--		numTabID,
--		numPageNavID,
--		bitVisible,
--		numDomainID,
--		tintType
--	)
--	SELECT  
--		numGroupID,
--		6,
--		(@numMAXPageNavID + 1),
--		1,
--		numDomainID,
--		1
--	FROM    
--		AuthenticationGroupMaster
--COMMIT
--END TRY
--BEGIN CATCH
--	IF @@TRANCOUNT > 0
--		ROLLBACK TRANSACTION;

--	SELECT 
--		ERROR_MESSAGE(),
--		ERROR_NUMBER(),
--		ERROR_SEVERITY(),
--		ERROR_STATE(),
--		ERROR_LINE(),
--		ISNULL(ERROR_PROCEDURE(), '-');
--END CATCH

-----------------------------------------------------------------

--INSERT INTO CFw_Grp_Master
--(
--	Grp_Name,Loc_Id,numDomainID,tintType,vcURLF
--)
--SELECT
--	'Sales Forecasts',2,numDomainId,2,''
--FROM
--	Domain

--------------------------

--ALTER TABLE ReportDashboard ADD dtLastCacheDate DATE

----------------------------

--USE [Production.2014]
--GO

--/****** Object:  Table [dbo].[ScheduledReportsGroup]    Script Date: 13-Oct-20 12:11:33 PM ******/
--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

--CREATE TABLE [dbo].[ScheduledReportsGroup](
--	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
--	[numDomainID] [numeric](18, 0) NOT NULL,
--	[numCreatedBy] [numeric](18, 0) NOT NULL,
--	[vcName] [nvarchar](200) NOT NULL,
--	[tintFrequency] [tinyint] NOT NULL,
--	[dtStartDate] [datetime] NOT NULL,
--	[numEmailTemplate] [numeric](18, 0) NOT NULL,
--	[vcSelectedTokens] [nvarchar](max) NOT NULL,
--	[vcRecipientsEmail] [nvarchar](max) NOT NULL,
--	[vcReceipientsContactID] [nvarchar](max) NOT NULL,
--	[tintDataCacheStatus] [tinyint] NOT NULL CONSTRAINT [DF_ScheduledReportsGroup_tintDataCacheStatus]  DEFAULT ((0)),
--	[dtNextDate] [datetime] NULL,
--	[vcExceptionMessage] [nvarchar](max) NULL,
--	[intTimeZoneOffset] [int] NULL,
--	[intNotOfTimeTried] [int] NULL,
-- CONSTRAINT [PK_ScheduledReportsGroup] PRIMARY KEY CLUSTERED 
--(
--	[ID] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
--) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

--GO

---------------------------------------------

--USE [Production.2014]
--GO

--/****** Object:  Table [dbo].[ScheduledReportsGroupLog]    Script Date: 13-Oct-20 12:11:50 PM ******/
--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

--CREATE TABLE [dbo].[ScheduledReportsGroupLog](
--	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
--	[numSRGID] [numeric](18, 0) NOT NULL,
--	[dtExecutionDate] [datetime] NOT NULL,
--	[vcException] [nvarchar](max) NOT NULL,
-- CONSTRAINT [PK_ScheduledReportsGroupLog] PRIMARY KEY CLUSTERED 
--(
--	[ID] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
--) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

--GO

--------------------------------------------------

--USE [Production.2014]
--GO

--/****** Object:  Table [dbo].[ScheduledReportsGroupReports]    Script Date: 13-Oct-20 12:12:11 PM ******/
--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

--CREATE TABLE [dbo].[ScheduledReportsGroupReports](
--	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
--	[numSRGID] [numeric](18, 0) NOT NULL,
--	[numReportID] [numeric](18, 0) NOT NULL,
--	[tintReportType] [tinyint] NOT NULL,
--	[intSortOrder] [int] NOT NULL,
-- CONSTRAINT [PK_ScheduledReportsGroupReports] PRIMARY KEY CLUSTERED 
--(
--	[ID] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
--) ON [PRIMARY]

--GO

--------------------------------------------------

--ALTER TABLE ContractsLog ADD tintRecordType TINYINT

------------------------------------------------------------------------------------------------------------------------
------------------------------------------ PLECE NEW SCRIPTS ABOVE IT --------------------------------------------------
------------------------------------------------------------------------------------------------------------------------

--USE [Production.2014]
--GO

--/****** Object:  Table [dbo].[SalesOrderLineItemsPOLinking]    Script Date: 17-Sep-20 5:23:14 PM ******/
--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

--CREATE TABLE [dbo].[SalesOrderLineItemsPOLinking](
--	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
--	[numDomainID] [numeric](18, 0) NOT NULL,
--	[numSalesOrderID] [numeric](18, 0) NOT NULL,
--	[numSalesOrderItemID] [numeric](18, 0) NOT NULL,
--	[numPurchaseOrderID] [numeric](18, 0) NOT NULL,
--	[numPurchaseOrderItemID] [numeric](18, 0) NOT NULL,
-- CONSTRAINT [PK_SalesOrderLineItemsPOLinking] PRIMARY KEY CLUSTERED 
--(
--	[ID] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
--) ON [PRIMARY]

--GO

-------------------------------


--USE [Production.2014]
--GO

--/****** Object:  Table [dbo].[ItemMarketplaceMapping]    Script Date: 21-Sep-20 10:43:51 AM ******/
--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

--SET ANSI_PADDING ON
--GO

--CREATE TABLE [dbo].[ItemMarketplaceMapping](
--	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
--	[numDomainID] [numeric](18, 0) NOT NULL,
--	[numItemCode] [numeric](18, 0) NOT NULL,
--	[numMarketplaceID] [numeric](18, 0) NOT NULL,
--	[vcMarketplaceUniqueID] [varchar](300) NOT NULL,
-- CONSTRAINT [PK_ItemMarketplaceMapping] PRIMARY KEY CLUSTERED 
--(
--	[ID] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
--) ON [PRIMARY]

--GO

--SET ANSI_PADDING ON
--GO

------------------------------------------------------------------------------------------------------------------------
------------------------------------------ PLECE NEW SCRIPTS ABOVE IT --------------------------------------------------
------------------------------------------------------------------------------------------------------------------------

--UPDATE GenericDocuments SET vcDocDesc='Hi ##CustomerContactName##<br /> Here is a summary of what we did today.<br /> <br /> <strong><span style="font-size: 16px; color: #3366ff;">Work Description:</span></strong> ##TaskTitle## ##TaskNotes## <strong><span style="font-size: 16px; color: #3366ff;">Duration &amp; Schedule:</span></strong> ##TaskDuration##,##TaskStartEndTime##, on ##TaskDate##<br /> <strong><span style="font-size: 16px; color: #3366ff;">Balance Remaining:</span></strong> ##TimeContractBalance##' WHERE VcFileName='#SYS#EMAIL_ALERT:TIMECONTRACT_USED'

--ALTER TABLE BillDetails ADD numOppID NUMERIC(18,0)
--ALTER TABLE BillDetails ADD numOppItemID NUMERIC(18,0)
-------------------------------

--USE [Production.2014]
--GO

--/****** Object:  Table [dbo].[StagePercentageDetailsTaskNotes]    Script Date: 09-Sep-20 2:51:16 PM ******/
--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

--CREATE TABLE [dbo].[StagePercentageDetailsTaskNotes](
--	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
--	[numTaskID] [numeric](18, 0) NOT NULL,
--	[vcNotes] [nvarchar](max) NOT NULL,
-- CONSTRAINT [PK_StagePercentageDetailsTaskNotes] PRIMARY KEY CLUSTERED 
--(
--	[ID] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
--) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

--GO

--ALTER TABLE [dbo].[StagePercentageDetailsTaskNotes]  WITH CHECK ADD  CONSTRAINT [FK_StagePercentageDetailsTaskNotes_StagePercentageDetailsTask] FOREIGN KEY([numTaskID])
--REFERENCES [dbo].[StagePercentageDetailsTask] ([numTaskId])
--ON DELETE CASCADE
--GO

--ALTER TABLE [dbo].[StagePercentageDetailsTaskNotes] CHECK CONSTRAINT [FK_StagePercentageDetailsTaskNotes_StagePercentageDetailsTask]
--GO

------------------------------------------------------------------------------------------------------------------------
------------------------------------------ PLECE NEW SCRIPTS ABOVE IT --------------------------------------------------
------------------------------------------------------------------------------------------------------------------------

---- ALREADY EXECUTED ON PRODUCTION 
--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,[order],bitInResults,bitDeleted,bitDefault,bitSettingField
--)
--VALUES
--(
--	4,196,129,0,0,'On-Order','TextBox',0,1,0,0,1
--)

------------------------------------------------------------------------------------------------------------------------
------------------------------------------ PLECE NEW SCRIPTS ABOVE IT --------------------------------------------------
------------------------------------------------------------------------------------------------------------------------

--ALTER TABLE MassSalesFulfillmentConfiguration ADD bitAutoWarehouseSelection BIT DEFAULT 0
--ALTER TABLE MassSalesFulfillmentConfiguration ADD bitBOOrderStatus BIT DEFAULT 0
--ALTER TABLE MassSalesFulfillmentConfiguration DROP COLUMN numUserCntID
--ALTER TABLE OpportunityMaster ADD monMarketplaceShippingCost DECIMAL(20,5)
--ALTER TABLE OpportunityItems ADD bitRequiredWarehouseCorrection BIT DEFAULT NULL
--ALTER TABLE OpportunityItems ADD numShipFromWarehouse NUMERIC(18,0)
--ALTER TABLE Domain ADD vcMarketplaces VARCHAR(500)

--UPDATE DycFormField_Mapping SET vcFieldName='Deliver By (Status)',bitAllowSorting=0,bitAllowFiltering=1,vcAssociatedControlType='SelectBox' WHERE numFormID=141 AND numFieldID IN (SELECT numFieldID FROM DycFieldMaster WHERE vcOrigDbColumnName='dtAnticipatedDelivery')

--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,[order],tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
--)
--VALUES
--(
--	3,97,141,0,0,'Order Source','SelectBox',39,39,1,1,0,0,1,1,1
--),
--(
--	3,199,141,0,1,'Ship-From Location','SelectBox',40,40,1,1,0,0,1,1,0
--)

--UPDATE DycFormField_Mapping SET bitAllowFiltering=1 WHERE numFormID=141 AND numFieldID=101

-------------------------------------------------------

--USE [Production.2014]
--GO

--/****** Object:  Table [dbo].[eChannelHub]    Script Date: 12-Aug-20 4:40:25 PM ******/
--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

--SET ANSI_PADDING ON
--GO

--CREATE TABLE [dbo].[eChannelHub](
--	[ID] [int] NOT NULL,
--	[vcMarketplace] [varchar](100) NOT NULL,
--	[vcType] [varchar](100) NOT NULL,
-- CONSTRAINT [PK_eChannelHub] PRIMARY KEY CLUSTERED 
--(
--	[ID] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
--) ON [PRIMARY]

--GO

--SET ANSI_PADDING ON
--GO

---------------------------------------------------------------

--INSERT INTO eChannelHub (ID,vcMarketplace,vcType) VALUES (1,'Amazon AU','Marketplaces')
--INSERT INTO eChannelHub (ID,vcMarketplace,vcType) VALUES (2,'Amazon CAN','Marketplaces')
--INSERT INTO eChannelHub (ID,vcMarketplace,vcType) VALUES (3,'Amazon UK','Marketplaces')
--INSERT INTO eChannelHub (ID,vcMarketplace,vcType) VALUES (4,'Amazon US','Marketplaces')
--INSERT INTO eChannelHub (ID,vcMarketplace,vcType) VALUES (5,'Bol.com','Marketplaces')
--INSERT INTO eChannelHub (ID,vcMarketplace,vcType) VALUES (6,'Bonanza','Marketplaces')
--INSERT INTO eChannelHub (ID,vcMarketplace,vcType) VALUES (7,'Cdiscount','Marketplaces')
--INSERT INTO eChannelHub (ID,vcMarketplace,vcType) VALUES (8,'eBay AU','Marketplaces')
--INSERT INTO eChannelHub (ID,vcMarketplace,vcType) VALUES (9,'eBay CAN','Marketplaces')
--INSERT INTO eChannelHub (ID,vcMarketplace,vcType) VALUES (10,'eBay UK','Marketplaces')
--INSERT INTO eChannelHub (ID,vcMarketplace,vcType) VALUES (11,'eBay US','Marketplaces')
--INSERT INTO eChannelHub (ID,vcMarketplace,vcType) VALUES (12,'eCrater','Marketplaces')
--INSERT INTO eChannelHub (ID,vcMarketplace,vcType) VALUES (13,'Etsy','Marketplaces')
--INSERT INTO eChannelHub (ID,vcMarketplace,vcType) VALUES (14,'Google Express','Marketplaces')
--INSERT INTO eChannelHub (ID,vcMarketplace,vcType) VALUES (15,'Hayneedle','Marketplaces')
--INSERT INTO eChannelHub (ID,vcMarketplace,vcType) VALUES (16,'Houzz','Marketplaces')
--INSERT INTO eChannelHub (ID,vcMarketplace,vcType) VALUES (17,'iOffer','Marketplaces')
--INSERT INTO eChannelHub (ID,vcMarketplace,vcType) VALUES (18,'Massgenie','Marketplaces')
--INSERT INTO eChannelHub (ID,vcMarketplace,vcType) VALUES (19,'Mercado libre','Marketplaces')
--INSERT INTO eChannelHub (ID,vcMarketplace,vcType) VALUES (20,'Newegg','Marketplaces')
--INSERT INTO eChannelHub (ID,vcMarketplace,vcType) VALUES (21,'Opensky','Marketplaces')
--INSERT INTO eChannelHub (ID,vcMarketplace,vcType) VALUES (22,'Overstock','Marketplaces')
--INSERT INTO eChannelHub (ID,vcMarketplace,vcType) VALUES (23,'Pricefalls','Marketplaces')
--INSERT INTO eChannelHub (ID,vcMarketplace,vcType) VALUES (24,'Rakuten','Marketplaces')
--INSERT INTO eChannelHub (ID,vcMarketplace,vcType) VALUES (25,'Reverb','Marketplaces')
--INSERT INTO eChannelHub (ID,vcMarketplace,vcType) VALUES (26,'Sears','Marketplaces')
--INSERT INTO eChannelHub (ID,vcMarketplace,vcType) VALUES (27,'Storenvy','Marketplaces')
--INSERT INTO eChannelHub (ID,vcMarketplace,vcType) VALUES (28,'Tanga','Marketplaces')
--INSERT INTO eChannelHub (ID,vcMarketplace,vcType) VALUES (29,'Tophatter','Marketplaces')
--INSERT INTO eChannelHub (ID,vcMarketplace,vcType) VALUES (30,'Trademe','Marketplaces')
--INSERT INTO eChannelHub (ID,vcMarketplace,vcType) VALUES (31,'Walmart AU','Marketplaces')
--INSERT INTO eChannelHub (ID,vcMarketplace,vcType) VALUES (32,'Walmart CAN','Marketplaces')
--INSERT INTO eChannelHub (ID,vcMarketplace,vcType) VALUES (33,'Walmart UK','Marketplaces')
--INSERT INTO eChannelHub (ID,vcMarketplace,vcType) VALUES (34,'Walmart US','Marketplaces')
--INSERT INTO eChannelHub (ID,vcMarketplace,vcType) VALUES (35,'Wish','Marketplaces')

--INSERT INTO eChannelHub (ID,vcMarketplace,vcType) VALUES (36,'Bing Shopping','Comparison Shopping')
--INSERT INTO eChannelHub (ID,vcMarketplace,vcType) VALUES (37,'Connexity','Comparison Shopping')
--INSERT INTO eChannelHub (ID,vcMarketplace,vcType) VALUES (38,'Gifts.com','Comparison Shopping')
--INSERT INTO eChannelHub (ID,vcMarketplace,vcType) VALUES (39,'Google Shopping','Comparison Shopping')
--INSERT INTO eChannelHub (ID,vcMarketplace,vcType) VALUES (40,'LensShopper','Comparison Shopping')
--INSERT INTO eChannelHub (ID,vcMarketplace,vcType) VALUES (41,'NexTag','Comparison Shopping')
--INSERT INTO eChannelHub (ID,vcMarketplace,vcType) VALUES (42,'PriceGrabber','Comparison Shopping')
--INSERT INTO eChannelHub (ID,vcMarketplace,vcType) VALUES (43,'PriceRunner','Comparison Shopping')
--INSERT INTO eChannelHub (ID,vcMarketplace,vcType) VALUES (44,'Shopping.com','Comparison Shopping')
--INSERT INTO eChannelHub (ID,vcMarketplace,vcType) VALUES (45,'Shopzilla','Comparison Shopping')


--INSERT INTO eChannelHub (ID,vcMarketplace,vcType) VALUES (46,'3D Cart','3rd Party Shopping Carts')
--INSERT INTO eChannelHub (ID,vcMarketplace,vcType) VALUES (47,'AmeriCommerce','3rd Party Shopping Carts') 
--INSERT INTO eChannelHub (ID,vcMarketplace,vcType) VALUES (48,'Big Commerce','3rd Party Shopping Carts')
--INSERT INTO eChannelHub (ID,vcMarketplace,vcType) VALUES (49,'Magento','3rd Party Shopping Carts')
--INSERT INTO eChannelHub (ID,vcMarketplace,vcType) VALUES (50,'OpenCart','3rd Party Shopping Carts')
--INSERT INTO eChannelHub (ID,vcMarketplace,vcType) VALUES (51,'OS Commerce','3rd Party Shopping Carts')
--INSERT INTO eChannelHub (ID,vcMarketplace,vcType) VALUES (52,'Presta Shop','3rd Party Shopping Carts')
--INSERT INTO eChannelHub (ID,vcMarketplace,vcType) VALUES (53,'Shopify','3rd Party Shopping Carts')
--INSERT INTO eChannelHub (ID,vcMarketplace,vcType) VALUES (54,'Shopify Plus','3rd Party Shopping Carts')
--INSERT INTO eChannelHub (ID,vcMarketplace,vcType) VALUES (55,'Volusion','3rd Party Shopping Carts')
--INSERT INTO eChannelHub (ID,vcMarketplace,vcType) VALUES (56,'Woo Commerce','3rd Party Shopping Carts')
--INSERT INTO eChannelHub (ID,vcMarketplace,vcType) VALUES (57,'X Cart','3rd Party Shopping Carts')
--INSERT INTO eChannelHub (ID,vcMarketplace,vcType) VALUES (58,'ZenCart','3rd Party Shopping Carts')

---------------------------------

--USE [Production.2014]
--GO

--/****** Object:  Table [dbo].[OpportunityMasterShippingRateError]    Script Date: 26-Aug-20 11:57:56 AM ******/
--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

--SET ANSI_PADDING ON
--GO

--CREATE TABLE [dbo].[OpportunityMasterShippingRateError](
--	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
--	[numDomainID] [numeric](18, 0) NOT NULL,
--	[numOppID] [numeric](18, 0) NOT NULL,
--	[intNoOfTimeFailed] [tinyint] NOT NULL,
--	[vcErrorMessage] [varchar](max) NOT NULL,
--	[dtLastExecutedDate] [datetime] NOT NULL,
-- CONSTRAINT [PK_OpportunityMasterShippingRateError] PRIMARY KEY CLUSTERED 
--(
--	[ID] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
--) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

--GO

--SET ANSI_PADDING ON
--GO


-----------------------------

--USE [Production.2014]
--GO

--/****** Object:  Table [dbo].[MassSalesFulfillmentShippingConfig]    Script Date: 26-Aug-20 12:00:47 PM ******/
--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

--SET ANSI_PADDING ON
--GO

--CREATE TABLE [dbo].[MassSalesFulfillmentShippingConfig](
--	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
--	[numDomainID] [numeric](18, 0) NOT NULL,
--	[numOrderSource] [numeric](18, 0) NOT NULL,
--	[tintSourceType] [tinyint] NOT NULL,
--	[tintType] [tinyint] NOT NULL,
--	[vcPriorities] [varchar](50) NOT NULL,
--	[numShipVia] [numeric](18, 0) NOT NULL,
--	[numShipService] [numeric](18, 0) NOT NULL,
--	[bitOverride] [bit] NOT NULL,
--	[fltWeight] [float] NOT NULL,
--	[numShipViaOverride] [numeric](18, 0) NOT NULL,
-- CONSTRAINT [PK_MassSalesFulfillmentShippingConfig] PRIMARY KEY CLUSTERED 
--(
--	[ID] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
--) ON [PRIMARY]

--GO

--SET ANSI_PADDING ON
--GO

------------------------------------------------------------------------------------------------------------------------
------------------------------------------ PLECE NEW SCRIPTS ABOVE IT ---------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------

--ALTER TABLE PurchaseIncentives ADD tintDiscountType TINYINT
--ALTER TABLE PurchaseIncentives ALTER COLUMN vcIncentives FLOAT
--ALTER TABLE PurchaseIncentives ALTER COLUMN vcbuyingqty FLOAT
--ALTER TABLE VendorShipmentMethod ADD numWarehouseID NUMERIC(18,0)
--UPDATE PurchaseIncentives SET tintDiscountType = 0
--ALTER TABLE Domain ADD bitUseDeluxeCheckStock BIT DEFAULT 1
--ALTER TABLE SalesOrderConfiguration ADD vcPOHiddenColumns VARCHAR(200)

--INSERT INTO PageMaster
--(
--	numPageID,numModuleID,vcFileName,vcPageDesc,bitIsViewApplicable,bitIsAddApplicable,bitIsUpdateApplicable,bitIsDeleteApplicable,bitIsExportApplicable,bitViewPermission1Applicable,bitViewPermission2Applicable,bitViewPermission3Applicable
--)
--VALUES
--(
--	1,46,'frmPOCostOptimizationAndMerge.aspx','Merging & Cost Optimization',1,0,0,0,0,0,0,1
--)

--BEGIN TRY
--BEGIN TRANSACTION

--	DECLARE @numMAXPageNavID INT
--	SELECT @numMAXPageNavID = MAX(numPageNavID) FROM PageNavigationDTL


--	INSERT INTO PageNavigationDTL
--	(
--		numPageNavID,numModuleID,numParentID,vcPageNavName,vcNavURL,bitVisible,numTabID
--	)
--	VALUES
--	(
--		(@numMAXPageNavID + 1),(SELECT TOP 1 numModuleID FROM ModuleMaster WHERE vcModuleName='Procurement'),(SELECT TOP 1 numPageNavID FROM PageNavigationDTL WHERE vcPageNavName='Procurement'),'Merging & Cost Optimization','../Opportunity/frmPOCostOptimizationAndMerge.aspx',1,(SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Procurement' AND numDomainID=1)
--	)

--	INSERT INTO dbo.TreeNavigationAuthorization
--	(
--		numGroupID,
--		numTabID,
--		numPageNavID,
--		bitVisible,
--		numDomainID,
--		tintType
--	)
--	SELECT  
--		numGroupID,
--		(SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Procurement' AND numDomainID=1),
--		(@numMAXPageNavID + 1),
--		1,
--		numDomainID,
--		1
--	FROM    
--		AuthenticationGroupMaster
--	WHERE
--		vcGroupName='System Administrator'

--	INSERT INTO dbo.GroupAuthorization
--	(numDomainID,numModuleID,numPageID,numGroupID,intExportAllowed,intPrintAllowed,intViewAllowed,intAddAllowed,intUpdateAllowed,intDeleteAllowed)
--	SELECT
--		numDomainID,46,1,numGroupID
--		,0
--		,0
--		,3
--		,0
--		,0
--		,0
--	FROM
--		AuthenticationGroupMaster
--	WHERE
--		vcGroupName='System Administrator'
--COMMIT
--END TRY
--BEGIN CATCH
--	IF @@TRANCOUNT > 0
--		ROLLBACK TRANSACTION;

--	SELECT 
--		ERROR_MESSAGE(),
--		ERROR_NUMBER(),
--		ERROR_SEVERITY(),
--		ERROR_STATE(),
--		ERROR_LINE(),
--		ISNULL(ERROR_PROCEDURE(), '-');
--END CATCH


----------------------------------------------------------

---- FOLLOWING ALREAY EXECUTED ON LIVE
--ALTER TABLE MassSalesFulfillmentConfiguration ADD bitEnablePickListMapping BIT DEFAULT 0
--ALTER TABLE MassSalesFulfillmentConfiguration ADD bitEnableFulfillmentBizDocMapping BIT DEFAULT 0

--DECLARE @numFieldID NUMERIC(18,0)

--INSERT INTO DycFieldMaster
--(
--	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcLookBackTableName,vcPropertyName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcListItemType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitDetailField,bitAllowSorting,bitAllowFiltering,bitWorkFlowField
--)
--VALUES
--(
--	3,'Remaining qty for pick','numRemainingQtyToPick','numRemainingQtyToPick','OpportunityItems','RemainingQtyToPick','N','R','Label','',0,1,0,0,0,1,0,1,1,0
--)

--SET @numFieldID = SCOPE_IDENTITY()

--INSERT INTO ReportFieldGroupMappingMaster
--(
--	numReportFieldGroupID,numFieldID,vcFieldName,vcAssociatedControlType,vcFieldDataType,bitAllowSorting,bitAllowGrouping,bitAllowAggregate,bitAllowFiltering
--)
--VALUES
--(
--	4,@numFieldID,'Remaining qty for pick','TextBox','N',1,0,0,1
--)

------------------------------------------------

--DECLARE @numFieldID NUMERIC(18,0)

--INSERT INTO DycFieldMaster
--(
--	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcLookBackTableName,vcPropertyName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcListItemType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitDetailField,bitAllowSorting,bitAllowFiltering,bitWorkFlowField
--)
--VALUES
--(
--	3,'Remaining qty for ship','numRemainingQtyToShip','numRemainingQtyToShip','OpportunityItems','RemainingQtyToShip','N','R','Label','',0,1,0,0,0,1,0,1,1,0
--)

--SET @numFieldID = SCOPE_IDENTITY()

--INSERT INTO ReportFieldGroupMappingMaster
--(
--	numReportFieldGroupID,numFieldID,vcFieldName,vcAssociatedControlType,vcFieldDataType,bitAllowSorting,bitAllowGrouping,bitAllowAggregate,bitAllowFiltering
--)
--VALUES
--(
--	4,@numFieldID,'Remaining qty for ship','TextBox','N',1,0,0,1
--)

------------------------------------------------

--DECLARE @numFieldID NUMERIC(18,0)

--INSERT INTO DycFieldMaster
--(
--	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcLookBackTableName,vcPropertyName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcListItemType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitDetailField,bitAllowSorting,bitAllowFiltering,bitWorkFlowField
--)
--VALUES
--(
--	3,'Remaining qty for invoicing','numRemainingQtyToInvoice','numRemainingQtyToInvoice','OpportunityItems','RemainingQtyToInvoice','N','R','Label','',0,1,0,0,0,1,0,1,1,0
--)

--SET @numFieldID = SCOPE_IDENTITY()

--INSERT INTO ReportFieldGroupMappingMaster
--(
--	numReportFieldGroupID,numFieldID,vcFieldName,vcAssociatedControlType,vcFieldDataType,bitAllowSorting,bitAllowGrouping,bitAllowAggregate,bitAllowFiltering
--)
--VALUES
--(
--	4,@numFieldID,'Remaining qty for invoicing','TextBox','N',1,0,0,1
--)

-------------------------------------------------------

--ALTER TABLE MassSalesFulfillmentConfiguration ADD bitPickWithoutInventoryCheck BIT DEFAULT 0

-------------------------------------------------------------------------------------------------------------------------
------------------------------------------ PLECE NEW SCRIPTS ABOVE IT ---------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------

-- INDEX DROPPED ON LIVE

--USE [Production.2014]
--GO

--/****** Object:  Index [PIItemName]    Script Date: 7/22/2020 2:32:46 AM ******/
--DROP INDEX [PIItemName] ON [dbo].[Item]
--GO

--/****** Object:  Index [PIItemName]    Script Date: 7/22/2020 2:32:46 AM ******/
--CREATE NONCLUSTERED INDEX [PIItemName] ON [dbo].[Item]
--(
--	[vcItemName] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
--GO

----------------------------------------------------------

--USE [Production.2014]
--GO

--/****** Object:  Index [NonClusteredIndex-20160119-124733]    Script Date: 7/22/2020 2:37:35 AM ******/
--DROP INDEX [NonClusteredIndex-20160119-124733] ON [dbo].[RecordHistory]
--GO

--/****** Object:  Index [NonClusteredIndex-20160119-124733]    Script Date: 7/22/2020 2:37:35 AM ******/
--CREATE NONCLUSTERED INDEX [NonClusteredIndex-20160119-124733] ON [dbo].[RecordHistory]
--(
--	[numDomainID] ASC,
--	[dtDate] DESC,
--	[numRecordID] ASC,
--	[numRHModuleMasterID] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
--GO

----------------------------------------------------------

--USE [Production.2014]
--GO

--/****** Object:  Index [NCIX_SalesFulfillmentLog_201803211629]    Script Date: 7/22/2020 2:39:29 AM ******/
--DROP INDEX [NCIX_SalesFulfillmentLog_201803211629] ON [dbo].[SalesFulfillmentLog]
--GO

--/****** Object:  Index [NCIX_SalesFulfillmentLog_201803211629]    Script Date: 7/22/2020 2:39:29 AM ******/
--CREATE NONCLUSTERED INDEX [NCIX_SalesFulfillmentLog_201803211629] ON [dbo].[SalesFulfillmentLog]
--(
--	[numDomainID] ASC,
--	[numOppID] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
--GO

-----------------------------------

--ALTER TABLE PricingTable ADD numCurrencyID NUMERIC(18,0) DEFAULT 0
--UPDATE PricingTable SET numCurrencyID=0
---------------------------------

--USE [Production.2014]
--GO

--/****** Object:  Table [dbo].[ItemCurrencyPrice]    Script Date: 17-Jun-20 3:21:10 PM ******/
--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

--CREATE TABLE [dbo].[ItemCurrencyPrice](
--	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
--	[numDomainID] [numeric](18, 0) NOT NULL,
--	[numItemCode] [numeric](18, 0) NOT NULL,
--	[numCurrencyID] [numeric](18, 0) NOT NULL,
--	[monListPrice] [decimal](20, 5) NOT NULL,
-- CONSTRAINT [PK_ItemCurrencyPrice] PRIMARY KEY CLUSTERED 
--(
--	[ID] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
--) ON [PRIMARY]

--GO

------------------------------------------------

--SET IDENTITY_INSERT ListDetails ON

--INSERT INTO ListDetails
--(
--	numListItemID,numListID,vcData,bitDelete,numDomainID,constFlag,sintOrder
--)
--VALUES
--(
--	2,31,'ACH',0,1,1,0
--)

--SET IDENTITY_INSERT ListDetails OFF

-----------------------------------------------------

--UPDATE DycFormField_Mapping SET vcAssociatedControlType='SelectBox' WHERE numFormID=29 AND numFieldID=311

-----------------------------------------------------

---- EXECUTE AFTER EXECUTING EXEC TO CONVERT PASSWORD TO ENCTYPTED PASWORD
--UPDATE ExtranetAccountsDtl SET vcPassword=NULL WHERE vcPassword='+XzdWtmW1RDGNvofcN89Fw=='

-------------------------------------------

--ALTER TABLE Domain ADD numARContactPosition NUMERIC(18,0)
--ALTER TABLE Domain ADD bitShowCardConnectLink BIT
--ALTER TABLE Domain ADD vcBluePayFormName VARCHAR(500)
--ALTER TABLE Domain ADD vcBluePaySuccessURL VARCHAR(500)
--ALTER TABLE Domain ADD vcBluePayDeclineURL VARCHAR(500)

-------------------------------------------

--UPDATE GenericDocuments SET vcSubject='Customer Statement' WHERE VcFileName='#SYS#CUSTOMER_STATEMENT'

-------------------------------------------

--UPDATE GenericDocuments SET numModuleID=45, vcDocDesc='Dear ##ContactFirstName## ##ContactLastName##,
--<br />
--<br />
--Below please find your customer statement. If you have any questions, Please feel free to call us.
--<br />
--<br />
--##Signature##
--<br />
--<br />
--##ARStatement##' WHERE VcFileName='#SYS#CUSTOMER_STATEMENT'

-------------------------------------------

--INSERT INTO EmailMergeFields 
--(
--	vcMergeField,vcMergeFieldValue,numModuleID,tintModuleType
--)
--VALUES
--(
--	'Org. A/R Statement','##ARStatement##',45,0
--)

-------------------------------------------

--ALTER TABLE Subscribers ADD intNoofLimitedAccessUsers INT
--ALTER TABLE Subscribers ADD intNoofBusinessPortalUsers INT
--ALTER TABLE Subscribers ADD monFullUserCost DECIMAL(20,5)
--ALTER TABLE Subscribers ADD monLimitedAccessUserCost DECIMAL(20,5)
--ALTER TABLE Subscribers ADD monBusinessPortalUserCost DECIMAL(20,5)
--ALTER TABLE Subscribers ADD monSiteCost DECIMAL(20,5)
--ALTER TABLE Subscribers ADD intFullUserConcurrency INT
-------------------------------------------

--ALTER TABLE PageMaster ADD bitViewPermission1Applicable BIT DEFAULT 1
--ALTER TABLE PageMaster ADD bitViewPermission2Applicable BIT DEFAULT 1
--ALTER TABLE PageMaster ADD bitViewPermission3Applicable BIT DEFAULT 1
--ALTER TABLE PageMaster ADD bitAddPermission1Applicable BIT DEFAULT 1
--ALTER TABLE PageMaster ADD bitAddPermission2Applicable BIT DEFAULT 1
--ALTER TABLE PageMaster ADD bitAddPermission3Applicable BIT DEFAULT 1
--ALTER TABLE PageMaster ADD bitUpdatePermission1Applicable BIT DEFAULT 1
--ALTER TABLE PageMaster ADD bitUpdatePermission2Applicable BIT DEFAULT 1
--ALTER TABLE PageMaster ADD bitUpdatePermission3Applicable BIT DEFAULT 1
--ALTER TABLE PageMaster ADD bitDeletePermission1Applicable BIT DEFAULT 1
--ALTER TABLE PageMaster ADD bitDeletePermission2Applicable BIT DEFAULT 1
--ALTER TABLE PageMaster ADD bitDeletePermission3Applicable BIT DEFAULT 1
--ALTER TABLE PageMaster ADD bitExportPermission1Applicable BIT DEFAULT 1
--ALTER TABLE PageMaster ADD bitExportPermission2Applicable BIT DEFAULT 1
--ALTER TABLE PageMaster ADD bitExportPermission3Applicable BIT DEFAULT 1

-------------------------------------------

--UPDATE 
--	PageMaster 
--SET 
--	bitViewPermission1Applicable=1,bitViewPermission2Applicable=1,bitViewPermission3Applicable=1
--	,bitAddPermission1Applicable=1,bitAddPermission2Applicable=1,bitAddPermission3Applicable=1
--	,bitUpdatePermission1Applicable=1,bitUpdatePermission2Applicable=1,bitUpdatePermission3Applicable=1
--	,bitDeletePermission1Applicable=1,bitDeletePermission2Applicable=1,bitDeletePermission3Applicable=1
--	,bitExportPermission1Applicable=1,bitExportPermission2Applicable=1,bitExportPermission3Applicable=1

-------------------------------------------------------

--USE [Production.2014]
--GO

--/****** Object:  Table [dbo].[ModuleMasterGroupType]    Script Date: 13-May-20 5:29:09 PM ******/
--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

--CREATE TABLE [dbo].[ModuleMasterGroupType](
--	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
--	[numModuleID] [numeric](18, 0) NOT NULL,
--	[tintGroupType] [tinyint] NOT NULL,
-- CONSTRAINT [PK_ModuleMasterGroupType] PRIMARY KEY CLUSTERED 
--(
--	[ID] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
--) ON [PRIMARY]

--GO

--ALTER TABLE [dbo].[ModuleMasterGroupType]  WITH CHECK ADD  CONSTRAINT [FK_ModuleMasterGroupType_ModuleMaster] FOREIGN KEY([numModuleID])
--REFERENCES [dbo].[ModuleMaster] ([numModuleID])
--ON DELETE CASCADE
--GO

--ALTER TABLE [dbo].[ModuleMasterGroupType] CHECK CONSTRAINT [FK_ModuleMasterGroupType_ModuleMaster]
--GO

-------------------------------------------------------------------


--USE [Production.2014]
--GO

--/****** Object:  Table [dbo].[PageMasterGroupSetting]    Script Date: 14-May-20 4:49:35 PM ******/
--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

--CREATE TABLE [dbo].[PageMasterGroupSetting](
--	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
--	[numPageID] [numeric](18, 0) NOT NULL,
--	[numModuleID] [numeric](18, 0) NOT NULL,
--	[numGroupID] [numeric](18, 0) NOT NULL,
--	[bitIsViewApplicable] [bit] NULL,
--	[bitViewPermission1Applicable] [bit] NULL,
--	[bitViewPermission2Applicable] [bit] NULL,
--	[bitViewPermission3Applicable] [bit] NULL,
--	[bitIsAddApplicable] [bit] NULL,
--	[bitAddPermission1Applicable] [bit] NULL,
--	[bitAddPermission2Applicable] [bit] NULL,
--	[bitAddPermission3Applicable] [bit] NULL,
--	[bitIsUpdateApplicable] [bit] NULL,
--	[bitUpdatePermission1Applicable] [bit] NULL,
--	[bitUpdatePermission2Applicable] [bit] NULL,
--	[bitUpdatePermission3Applicable] [bit] NULL,
--	[bitIsDeleteApplicable] [bit] NULL,
--	[bitDeletePermission1Applicable] [bit] NULL,
--	[bitDeletePermission2Applicable] [bit] NULL,
--	[bitDeletePermission3Applicable] [bit] NULL,
--	[bitIsExportApplicable] [bit] NULL,
--	[bitExportPermission1Applicable] [bit] NULL,
--	[bitExportPermission2Applicable] [bit] NULL,
--	[bitExportPermission3Applicable] [bit] NULL,
-- CONSTRAINT [PK_PageMasterGroupSetting] PRIMARY KEY CLUSTERED 
--(
--	[ID] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
--) ON [PRIMARY]

--GO

-------------------------------------------------------------------

--UPDATE AuthenticationGroupMaster SET bitConsFlag=1 WHERE tintGroupType=1 AND numGroupID IN (SELECT MIN(numGroupID) FROM AuthenticationGroupMaster WHERE tintGroupType=1 GROUP BY numDomainID)
--UPDATE AuthenticationGroupMaster SET bitConsFlag=1 WHERE tintGroupType=2 AND numGroupID IN (SELECT MIN(numGroupID) FROM AuthenticationGroupMaster WHERE tintGroupType=2 GROUP BY numDOmainID)
--UPDATE AuthenticationGroupMaster SET vcGroupName='Business Portal Users' WHERE tintGroupType=2 AND vcGroupName='Default Extranet'


--INSERT INTO AuthenticationGroupMaster (vcGroupName,numDomainID,tintGroupType,bitConsFlag) SELECT 'Limited Access Users',numDomainId,4,1 FROM Domain
----INSERT INTO AuthenticationGroupMaster (vcGroupName,numDomainID,tintGroupType,bitConsFlag) SELECT 'Time & Expense Users',numDomainId,5,1 FROM Domain
----INSERT INTO AuthenticationGroupMaster (vcGroupName,numDomainID,tintGroupType,bitConsFlag) SELECT 'Associated Contact User',numDomainId,6,1 FROM Domain

--INSERT INTO GroupTabDetails (numGroupId,numTabId,numRelationShip,bitallowed,numOrder,tintType) SELECT numGroupID,135,0,1,1,0 FROM AuthenticationGroupMaster WHERE tintGroupType = 4
--INSERT INTO GroupTabDetails (numGroupId,numTabId,numRelationShip,bitallowed,numOrder,tintType) SELECT numGroupID,5,0,1,1,0 FROM AuthenticationGroupMaster WHERE tintGroupType = 4
--INSERT INTO GroupTabDetails (numGroupId,numTabId,numRelationShip,bitallowed,numOrder,tintType) SELECT numGroupID,36,0,1,1,0 FROM AuthenticationGroupMaster WHERE tintGroupType = 4
--INSERT INTO GroupTabDetails (numGroupId,numTabId,numRelationShip,bitallowed,numOrder,tintType) SELECT numGroupID,132,0,1,1,0 FROM AuthenticationGroupMaster WHERE tintGroupType = 4


--INSERT INTO TreeNavigationAuthorization
--(
--	numDomainID,numGroupID,numTabID,numPageNavID,bitVisible,tintType
--)
--SELECT 
--	numDomainID,numGroupID,5,279,1,1
--FROM
--	AuthenticationGroupMaster
--WHERE
--	tintGroupType=4
--UNION
--SELECT 
--	numDomainID,numGroupID,5,280,1,1
--FROM
--	AuthenticationGroupMaster
--WHERE
--	tintGroupType=4

--INSERT INTO TreeNavigationAuthorization
--(
--	numDomainID,numGroupID,numTabID,numPageNavID,bitVisible,tintType
--)
--SELECT 
--	numDomainID,numGroupID,135,274,1,1
--FROM
--	AuthenticationGroupMaster
--WHERE
--	tintGroupType=4
--UNION
--SELECT 
--	numDomainID,numGroupID,135,275,1,1
--FROM
--	AuthenticationGroupMaster
--WHERE
--	tintGroupType=4

--INSERT INTO TreeNavigationAuthorization
--(
--	numDomainID,numGroupID,numTabID,numPageNavID,bitVisible,tintType
--)
--SELECT 
--	numDomainID,numGroupID,132,84,1,1
--FROM
--	AuthenticationGroupMaster
--WHERE
--	tintGroupType=4
--UNION
--SELECT 
--	numDomainID,numGroupID,132,271,1,1
--FROM
--	AuthenticationGroupMaster
--WHERE
--	tintGroupType=4

--INSERT INTO TreeNavigationAuthorization
--(
--	numDomainID,numGroupID,numTabID,numPageNavID,bitVisible,tintType
--)
--SELECT 
--	numDomainID,numGroupID,36,250,1,1
--FROM
--	AuthenticationGroupMaster
--WHERE
--	tintGroupType=4
-------------------------------------------

--UPDATE PageMaster SET bitDeleted=1 WHERE numModuleID=10 AND numPageID IN (24,25)
--UPDATE PageMaster SET vcPageDesc='Work Orders Grid (Open View)',bitIsUpdateApplicable=0,bitIsDeleteApplicable=0 WHERE numModuleID=16 AND numPageID=152 
--UPDATE PageMaster SET bitIsDeleteApplicable=0 WHERE numModuleID=16 AND numPageID=153

--INSERT INTO PageMaster
--(
--	numPageID,numModuleID,vcFileName,vcPageDesc,bitIsViewApplicable,bitIsAddApplicable,bitIsUpdateApplicable,bitIsDeleteApplicable,bitIsExportApplicable
--)
--VALUES
--(154,16,'frmWorkOrderList.aspx','"Finish Work Order" button from initial grid (Open view) and detail page',1,0,0,0,0),
--(155,16,'frmWorkOrderList.aspx','"Remover" button from initial grid (Open view) and detail page',1,0,0,0,0)
--,(156,16,'frmWorkOrder.aspx','Work Orders Grid (Finished view)',1,0,0,0,0)


--INSERT INTO PageMaster
--(
--	numPageID,numModuleID,vcFileName,vcPageDesc,bitIsViewApplicable,bitIsAddApplicable,bitIsUpdateApplicable,bitIsDeleteApplicable,bitIsExportApplicable
--)
--VALUES
--(157,16,'frmWorkOrder.aspx','Work In Progress (WIP) Form',0,0,1,0,0)
--,(158,16,'frmWorkOrder.aspx','Manage WIP Form',1,0,1,0,0)
--,(159,16,'frmWorkOrder.aspx','Edit Source/Default Processes from the Manage WIP Form',0,0,1,0,0)
--,(160,16,'frmWorkOrder.aspx','Bill Of Materials (BOM) Form',1,0,0,0,0)


--INSERT INTO PageMaster
--(
--	numPageID,numModuleID,vcFileName,vcPageDesc,bitIsViewApplicable,bitIsAddApplicable,bitIsUpdateApplicable,bitIsDeleteApplicable,bitIsExportApplicable
--	,bitViewPermission1Applicable,bitViewPermission2Applicable,bitViewPermission3Applicable
--	,bitAddPermission1Applicable,bitAddPermission2Applicable,bitAddPermission3Applicable
--	,bitUpdatePermission1Applicable,bitUpdatePermission2Applicable,bitUpdatePermission3Applicable
--	,bitDeletePermission1Applicable,bitDeletePermission2Applicable,bitDeletePermission3Applicable
--	,bitExportPermission1Applicable,bitExportPermission2Applicable,bitExportPermission3Applicable
--)
--VALUES
--(161,16,'frmWorkOrderList.aspx','"Pick List" and "Pick Selected" button from initial grid (Open view)',1,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0)
--,(162,16,'frmWorkOrder.aspx','"Pick BOM" button from work order detail',1,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0)
--,(163,16,'frmWorkOrder.aspx','"Open Selected Item(s) in Demand Plan" button from Bill Of Materials (BOM) tab in work order detail',1,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0)
--,(164,16,'frmWorkOrderList.aspx','Grid Configuration',1,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0)
--,(165,16,'frmWorkOrder.aspx','Layout',1,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0)

-------------------------------------------


--INSERT INTO PageMasterGroupSetting 
--(
--	numPageID
--	,numModuleID
--	,numGroupID
--	,bitIsViewApplicable
--	,bitViewPermission1Applicable
--	,bitViewPermission2Applicable
--	,bitViewPermission3Applicable
--	,bitIsAddApplicable
--	,bitAddPermission1Applicable
--	,bitAddPermission2Applicable
--	,bitAddPermission3Applicable
--	,bitIsUpdateApplicable
--	,bitUpdatePermission1Applicable
--	,bitUpdatePermission2Applicable
--	,bitUpdatePermission3Applicable
--	,bitIsDeleteApplicable
--	,bitDeletePermission1Applicable
--	,bitDeletePermission2Applicable
--	,bitDeletePermission3Applicable
--	,bitIsExportApplicable
--	,bitExportPermission1Applicable
--	,bitExportPermission2Applicable
--	,bitExportPermission3Applicable
--)
--SELECT 152,16,numGroupID,1,1,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 FROM AuthenticationGroupMaster WHERE tintGroupType=4 
--UNION
--SELECT 153,16,numGroupID,1,1,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 FROM AuthenticationGroupMaster WHERE tintGroupType=4
--UNION
--SELECT 154,16,numGroupID,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 FROM AuthenticationGroupMaster WHERE tintGroupType=4
--UNION
--SELECT 155,16,numGroupID,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 FROM AuthenticationGroupMaster WHERE tintGroupType=4
--UNION
--SELECT 156,16,numGroupID,1,1,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 FROM AuthenticationGroupMaster WHERE tintGroupType=4
--UNION
--SELECT 157,16,numGroupID,0,0,0,0,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0 FROM AuthenticationGroupMaster WHERE tintGroupType=4
--UNION
--SELECT 158,16,numGroupID,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 FROM AuthenticationGroupMaster WHERE tintGroupType=4
--UNION
--SELECT 159,16,numGroupID,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 FROM AuthenticationGroupMaster WHERE tintGroupType=4
--UNION
--SELECT 160,16,numGroupID,1,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 FROM AuthenticationGroupMaster WHERE tintGroupType=4 
--UNION
--SELECT 161,16,numGroupID,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 FROM AuthenticationGroupMaster WHERE tintGroupType=4
--UNION
--SELECT 162,16,numGroupID,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 FROM AuthenticationGroupMaster WHERE tintGroupType=4
--UNION
--SELECT 163,16,numGroupID,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 FROM AuthenticationGroupMaster WHERE tintGroupType=4
--UNION
--SELECT 164,16,numGroupID,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 FROM AuthenticationGroupMaster WHERE tintGroupType=4
--UNION
--SELECT 165,16,numGroupID,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 FROM AuthenticationGroupMaster WHERE tintGroupType=4

------------------------------------------

--INSERT INTO GroupAuthorization
--(
--	numDomainID,numGroupID,numPageID,numModuleID,intViewAllowed,intAddAllowed,intUpdateAllowed,intDeleteAllowed,intExportAllowed,intPrintAllowed
--)
--SELECT numDomainID,numGroupID,152,16,1,0,0,0,0,0 FROM AuthenticationGroupMaster WHERE tintGroupType=4 
--UNION
--SELECT numDomainID,numGroupID,153,16,1,0,0,0,0,0 FROM AuthenticationGroupMaster WHERE tintGroupType=4
--UNION
--SELECT numDomainID,numGroupID,154,16,0,0,0,0,0,0 FROM AuthenticationGroupMaster WHERE tintGroupType=4
--UNION
--SELECT numDomainID,numGroupID,155,16,0,0,0,0,0,0 FROM AuthenticationGroupMaster WHERE tintGroupType=4
--UNION
--SELECT numDomainID,numGroupID,156,16,1,0,0,0,0,0 FROM AuthenticationGroupMaster WHERE tintGroupType=4
--UNION
--SELECT numDomainID,numGroupID,157,16,1,0,1,0,0,0 FROM AuthenticationGroupMaster WHERE tintGroupType=4
--UNION
--SELECT numDomainID,numGroupID,158,16,0,0,0,0,0,0 FROM AuthenticationGroupMaster WHERE tintGroupType=4
--UNION
--SELECT numDomainID,numGroupID,159,16,0,0,0,0,0,0 FROM AuthenticationGroupMaster WHERE tintGroupType=4
--UNION
--SELECT numDomainID,numGroupID,160,16,3,0,0,0,0,0 FROM AuthenticationGroupMaster WHERE tintGroupType=4 
--UNION
--SELECT numDomainID,numGroupID,161,16,0,0,0,0,0,0 FROM AuthenticationGroupMaster WHERE tintGroupType=4
--UNION
--SELECT numDomainID,numGroupID,162,16,0,0,0,0,0,0 FROM AuthenticationGroupMaster WHERE tintGroupType=4
--UNION
--SELECT numDomainID,numGroupID,163,16,0,0,0,0,0,0 FROM AuthenticationGroupMaster WHERE tintGroupType=4
--UNION
--SELECT numDomainID,numGroupID,164,16,0,0,0,0,0,0 FROM AuthenticationGroupMaster WHERE tintGroupType=4
--UNION
--SELECT numDomainID,numGroupID,165,16,0,0,0,0,0,0 FROM AuthenticationGroupMaster WHERE tintGroupType=4


-------------------------------------------------
---- TIME & EXPENSE --

--INSERT INTO PageMasterGroupSetting 
--(
--	numPageID
--	,numModuleID
--	,numGroupID
--	,bitIsViewApplicable
--	,bitViewPermission1Applicable
--	,bitViewPermission2Applicable
--	,bitViewPermission3Applicable
--	,bitIsAddApplicable
--	,bitAddPermission1Applicable
--	,bitAddPermission2Applicable
--	,bitAddPermission3Applicable
--	,bitIsUpdateApplicable
--	,bitUpdatePermission1Applicable
--	,bitUpdatePermission2Applicable
--	,bitUpdatePermission3Applicable
--	,bitIsDeleteApplicable
--	,bitDeletePermission1Applicable
--	,bitDeletePermission2Applicable
--	,bitDeletePermission3Applicable
--	,bitIsExportApplicable
--	,bitExportPermission1Applicable
--	,bitExportPermission2Applicable
--	,bitExportPermission3Applicable
--)
--SELECT 117,40,numGroupID,1,1,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0 FROM AuthenticationGroupMaster WHERE tintGroupType=4 

--------------------------------------------

--INSERT INTO GroupAuthorization
--(
--	numDomainID,numGroupID,numPageID,numModuleID,intViewAllowed,intAddAllowed,intUpdateAllowed,intDeleteAllowed,intExportAllowed,intPrintAllowed
--)
--SELECT numDomainID,numGroupID,117,40,1,1,0,0,0,0 FROM AuthenticationGroupMaster WHERE tintGroupType=4 

----------------------------------------------------
----- ACCOUNTING ---

--INSERT INTO PageMasterGroupSetting 
--(
--	numPageID,numModuleID,numGroupID
--	,bitIsViewApplicable,bitViewPermission1Applicable,bitViewPermission2Applicable,bitViewPermission3Applicable
--	,bitIsAddApplicable,bitAddPermission1Applicable,bitAddPermission2Applicable,bitAddPermission3Applicable
--	,bitIsUpdateApplicable,bitUpdatePermission1Applicable,bitUpdatePermission2Applicable,bitUpdatePermission3Applicable
--	,bitIsDeleteApplicable,bitDeletePermission1Applicable,bitDeletePermission2Applicable,bitDeletePermission3Applicable
--	,bitIsExportApplicable,bitExportPermission1Applicable,bitExportPermission2Applicable,bitExportPermission3Applicable
--)
--SELECT
--	PageMaster.numPageID,PageMaster.numModuleID,numGroupID,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
--FROM
--	AuthenticationGroupMaster, PageMaster
--WHERE 
--	tintGroupType=4 
--	AND numModuleID=35
--	AND numPageID <> 87
--UNION
--SELECT
--	PageMaster.numPageID,PageMaster.numModuleID,numGroupID,1,1,0,0,0,0,0,0,1,1,0,0,0,0,0,0,1,1,0,0
--FROM
--	AuthenticationGroupMaster, PageMaster
--WHERE 
--	tintGroupType=4 
--	AND numModuleID=35
--	AND numPageID = 87

----

--INSERT INTO GroupAuthorization
--(
--	numDomainID,numGroupID,numPageID,numModuleID,intViewAllowed,intAddAllowed,intUpdateAllowed,intDeleteAllowed,intExportAllowed,intPrintAllowed
--)
--SELECT
--	numDomainID,numGroupID,PageMaster.numPageID,PageMaster.numModuleID,0,0,0,0,0,0
--FROM
--	AuthenticationGroupMaster, PageMaster
--WHERE 
--	tintGroupType=4 
--	AND numModuleID=35
--	AND numPageID <> 87
--UNION
--SELECT
--	numDomainID,numGroupID,PageMaster.numPageID,PageMaster.numModuleID,1,0,1,0,1,0
--FROM
--	AuthenticationGroupMaster, PageMaster
--WHERE 
--	tintGroupType=4 
--	AND numModuleID=35
--	AND numPageID = 87

-------------------------------------------

--INSERT INTO ModuleMasterGroupType (numModuleID,tintGroupType) VALUES (1,4),(12,4),(16,4),(35,4),(40,4)

--------------------------------------------

--UPDATE PageMaster SET bitViewPermission3Applicable=1,bitViewPermission2Applicable=0,bitViewPermission1Applicable=1,bitAddPermission3Applicable=1,bitAddPermission2Applicable=0,bitAddPermission1Applicable=0 WHERE numModuleID=16 AND numPageID=152
--UPDATE PageMaster SET bitViewPermission3Applicable=1,bitViewPermission2Applicable=0,bitViewPermission1Applicable=1,bitUpdatePermission2Applicable=0 WHERE numModuleID=16 AND numPageID=153
--UPDATE PageMaster SET bitViewPermission3Applicable=1,bitViewPermission2Applicable=0,bitViewPermission1Applicable=0 WHERE numModuleID=16 AND numPageID=154
--UPDATE PageMaster SET bitViewPermission3Applicable=1,bitViewPermission2Applicable=0,bitViewPermission1Applicable=0 WHERE numModuleID=16 AND numPageID=155
--UPDATE PageMaster SET bitViewPermission3Applicable=1,bitViewPermission2Applicable=0,bitViewPermission1Applicable=1 WHERE numModuleID=16 AND numPageID=156
--UPDATE PageMaster SET bitUpdatePermission3Applicable=1,bitUpdatePermission2Applicable=0,bitUpdatePermission1Applicable=1 WHERE numModuleID=16 AND numPageID=157
--UPDATE PageMaster SET bitViewPermission3Applicable=1,bitViewPermission2Applicable=0,bitViewPermission1Applicable=0,bitUpdatePermission3Applicable=1,bitUpdatePermission2Applicable=0,bitUpdatePermission1Applicable=1 WHERE numModuleID=16 AND numPageID=158
--UPDATE PageMaster SET bitUpdatePermission3Applicable=1,bitUpdatePermission2Applicable=0,bitUpdatePermission1Applicable=0 WHERE numModuleID=16 AND numPageID=159
--UPDATE PageMaster SET bitViewPermission3Applicable=1,bitViewPermission2Applicable=0,bitViewPermission1Applicable=0 WHERE numModuleID=16 AND numPageID=160
--UPDATE PageMaster SET bitViewPermission3Applicable=1,bitViewPermission2Applicable=0,bitViewPermission1Applicable=0 WHERE numModuleID=16 AND numPageID=161
--UPDATE PageMaster SET bitViewPermission3Applicable=1,bitViewPermission2Applicable=0,bitViewPermission1Applicable=0 WHERE numModuleID=16 AND numPageID=162
--UPDATE PageMaster SET bitViewPermission3Applicable=1,bitViewPermission2Applicable=0,bitViewPermission1Applicable=0 WHERE numModuleID=16 AND numPageID=163

------------------------------------------------

---- PROJECTS --

--UPDATE 
--	PageMaster 
--SET 
--	vcPageDesc='Projects Grid (Open view)'
--	,vcFileName='frmProjectList.aspx'
--	,bitIsViewApplicable=1,bitViewPermission1Applicable=1,bitViewPermission2Applicable=1,bitViewPermission3Applicable=1
--	,bitIsAddApplicable=1,bitAddPermission1Applicable=0,bitAddPermission2Applicable=0,bitAddPermission3Applicable=1
--	,bitIsUpdateApplicable=1,bitUpdatePermission1Applicable=1,bitUpdatePermission2Applicable=1,bitUpdatePermission3Applicable=1
--	,bitIsDeleteApplicable=0,bitDeletePermission1Applicable=0,bitDeletePermission2Applicable=0,bitDeletePermission3Applicable=0
--	,bitIsExportApplicable=0,bitExportPermission1Applicable=0,bitExportPermission2Applicable=0,bitExportPermission3Applicable=0
--WHERE 
--	numModuleID=12 
--	AND numPageID=1

------------------

--UPDATE 
--	PageMaster 
--SET 
--	vcPageDesc='Projects Grid (Finished view)'
--	,bitIsViewApplicable=1,bitViewPermission1Applicable=1,bitViewPermission2Applicable=1,bitViewPermission3Applicable=1
--	,bitIsAddApplicable=0,bitAddPermission1Applicable=0,bitAddPermission2Applicable=0,bitAddPermission3Applicable=0
--	,bitIsUpdateApplicable=0,bitUpdatePermission1Applicable=0,bitUpdatePermission2Applicable=0,bitUpdatePermission3Applicable=0
--	,bitIsDeleteApplicable=0,bitDeletePermission1Applicable=0,bitDeletePermission2Applicable=0,bitDeletePermission3Applicable=0
--	,bitIsExportApplicable=0,bitExportPermission1Applicable=0,bitExportPermission2Applicable=0,bitExportPermission3Applicable=0
--WHERE 
--	numModuleID=12 
--	AND numPageID=2

--------------------

--INSERT INTO PageMaster
--(
--	numPageID,numModuleID,vcFileName,vcPageDesc
--	,bitIsViewApplicable,bitViewPermission1Applicable,bitViewPermission2Applicable,bitViewPermission3Applicable
--	,bitIsAddApplicable,bitAddPermission1Applicable,bitAddPermission2Applicable,bitAddPermission3Applicable
--	,bitIsUpdateApplicable,bitUpdatePermission1Applicable,bitUpdatePermission2Applicable,bitUpdatePermission3Applicable
--	,bitIsDeleteApplicable,bitDeletePermission1Applicable,bitDeletePermission2Applicable,bitDeletePermission3Applicable
--	,bitIsExportApplicable,bitExportPermission1Applicable,bitExportPermission2Applicable,bitExportPermission3Applicable
--)
--VALUES
--(
--	15,12,'frmProjectList.aspx','Projects Grid (Canceled view)'
--	,1,1,1,1
--	,0,0,0,0
--	,0,0,0,0
--	,0,0,0,0
--	,0,0,0,0
--)

--------------------

--INSERT INTO PageMaster
--(
--	numPageID,numModuleID,vcFileName,vcPageDesc
--	,bitIsViewApplicable,bitViewPermission1Applicable,bitViewPermission2Applicable,bitViewPermission3Applicable
--	,bitIsAddApplicable,bitAddPermission1Applicable,bitAddPermission2Applicable,bitAddPermission3Applicable
--	,bitIsUpdateApplicable,bitUpdatePermission1Applicable,bitUpdatePermission2Applicable,bitUpdatePermission3Applicable
--	,bitIsDeleteApplicable,bitDeletePermission1Applicable,bitDeletePermission2Applicable,bitDeletePermission3Applicable
--	,bitIsExportApplicable,bitExportPermission1Applicable,bitExportPermission2Applicable,bitExportPermission3Applicable
--)
--VALUES
--(
--	16,12,'frmProjectList.aspx','�Remove� button from initial grid (Open view)'
--	,1,0,0,1
--	,0,0,0,0
--	,0,0,0,0
--	,0,0,0,0
--	,0,0,0,0
--)

---------------------

--UPDATE
--	PageMaster
--SET 
--	bitIsViewApplicable=1,bitViewPermission1Applicable=1,bitViewPermission2Applicable=1,bitViewPermission3Applicable=1
--	,bitIsAddApplicable=0,bitAddPermission1Applicable=0,bitAddPermission2Applicable=0,bitAddPermission3Applicable=0
--	,bitIsUpdateApplicable=1,bitUpdatePermission1Applicable=1,bitUpdatePermission2Applicable=1,bitUpdatePermission3Applicable=1
--	,bitIsDeleteApplicable=1,bitDeletePermission1Applicable=1,bitDeletePermission2Applicable=1,bitDeletePermission3Applicable=1
--	,bitIsExportApplicable=0,bitExportPermission1Applicable=0,bitExportPermission2Applicable=0,bitExportPermission3Applicable=0
--WHERE
--	numModuleID=12
--	AND numPageID=3

---------------------

--UPDATE
--	PageMaster
--SET 
--	vcPageDesc='Project Tasks Form'
--	,bitIsViewApplicable=0,bitViewPermission1Applicable=0,bitViewPermission2Applicable=0,bitViewPermission3Applicable=0
--	,bitIsAddApplicable=0,bitAddPermission1Applicable=0,bitAddPermission2Applicable=0,bitAddPermission3Applicable=0
--	,bitIsUpdateApplicable=1,bitUpdatePermission1Applicable=1,bitUpdatePermission2Applicable=0,bitUpdatePermission3Applicable=1
--	,bitIsDeleteApplicable=0,bitDeletePermission1Applicable=0,bitDeletePermission2Applicable=0,bitDeletePermission3Applicable=0
--	,bitIsExportApplicable=0,bitExportPermission1Applicable=0,bitExportPermission2Applicable=0,bitExportPermission3Applicable=0
--WHERE
--	numModuleID=12
--	AND numPageID=4

-----------------

--UPDATE
--	PageMaster
--SET 
--	vcPageDesc='Manage Project Tasks From'
--	,bitIsViewApplicable=1,bitViewPermission1Applicable=1,bitViewPermission2Applicable=1,bitViewPermission3Applicable=1
--	,bitIsAddApplicable=0,bitAddPermission1Applicable=0,bitAddPermission2Applicable=0,bitAddPermission3Applicable=0
--	,bitIsUpdateApplicable=1,bitUpdatePermission1Applicable=1,bitUpdatePermission2Applicable=1,bitUpdatePermission3Applicable=1
--	,bitIsDeleteApplicable=0,bitDeletePermission1Applicable=0,bitDeletePermission2Applicable=0,bitDeletePermission3Applicable=0
--	,bitIsExportApplicable=0,bitExportPermission1Applicable=0,bitExportPermission2Applicable=0,bitExportPermission3Applicable=0
--WHERE
--	numModuleID=12
--	AND numPageID=5

---------------------

--UPDATE
--	PageMaster
--SET 
--	vcPageDesc='Project Teams From'
--	,bitIsViewApplicable=1,bitViewPermission1Applicable=1,bitViewPermission2Applicable=1,bitViewPermission3Applicable=1
--	,bitIsAddApplicable=0,bitAddPermission1Applicable=0,bitAddPermission2Applicable=0,bitAddPermission3Applicable=0
--	,bitIsUpdateApplicable=1,bitUpdatePermission1Applicable=1,bitUpdatePermission2Applicable=1,bitUpdatePermission3Applicable=1
--	,bitIsDeleteApplicable=0,bitDeletePermission1Applicable=0,bitDeletePermission2Applicable=0,bitDeletePermission3Applicable=0
--	,bitIsExportApplicable=0,bitExportPermission1Applicable=0,bitExportPermission2Applicable=0,bitExportPermission3Applicable=0
--WHERE
--	numModuleID=12
--	AND numPageID=6

------------------

--UPDATE
--	PageMaster
--SET 
--	vcPageDesc='P&L From'
--	,bitIsViewApplicable=1,bitViewPermission1Applicable=1,bitViewPermission2Applicable=1,bitViewPermission3Applicable=1
--	,bitIsAddApplicable=0,bitAddPermission1Applicable=0,bitAddPermission2Applicable=0,bitAddPermission3Applicable=0
--	,bitIsUpdateApplicable=1,bitUpdatePermission1Applicable=1,bitUpdatePermission2Applicable=1,bitUpdatePermission3Applicable=1
--	,bitIsDeleteApplicable=0,bitDeletePermission1Applicable=0,bitDeletePermission2Applicable=0,bitDeletePermission3Applicable=0
--	,bitIsExportApplicable=0,bitExportPermission1Applicable=0,bitExportPermission2Applicable=0,bitExportPermission3Applicable=0
--WHERE
--	numModuleID=12
--	AND numPageID=7

-------------------

--UPDATE
--	PageMaster
--SET 
--	vcPageDesc='Edit Source/Default Processes from the Manage Project Tasks From'
--	,bitIsViewApplicable=0,bitViewPermission1Applicable=0,bitViewPermission2Applicable=0,bitViewPermission3Applicable=0
--	,bitIsAddApplicable=0,bitAddPermission1Applicable=0,bitAddPermission2Applicable=0,bitAddPermission3Applicable=0
--	,bitIsUpdateApplicable=1,bitUpdatePermission1Applicable=1,bitUpdatePermission2Applicable=1,bitUpdatePermission3Applicable=1
--	,bitIsDeleteApplicable=0,bitDeletePermission1Applicable=0,bitDeletePermission2Applicable=0,bitDeletePermission3Applicable=0
--	,bitIsExportApplicable=0,bitExportPermission1Applicable=0,bitExportPermission2Applicable=0,bitExportPermission3Applicable=0
--WHERE
--	numModuleID=12
--	AND numPageID=8

----------------------

--INSERT INTO PageMaster
--(
--	numPageID,numModuleID,vcFileName,vcPageDesc
--	,bitIsViewApplicable,bitViewPermission1Applicable,bitViewPermission2Applicable,bitViewPermission3Applicable
--	,bitIsAddApplicable,bitAddPermission1Applicable,bitAddPermission2Applicable,bitAddPermission3Applicable
--	,bitIsUpdateApplicable,bitUpdatePermission1Applicable,bitUpdatePermission2Applicable,bitUpdatePermission3Applicable
--	,bitIsDeleteApplicable,bitDeletePermission1Applicable,bitDeletePermission2Applicable,bitDeletePermission3Applicable
--	,bitIsExportApplicable,bitExportPermission1Applicable,bitExportPermission2Applicable,bitExportPermission3Applicable
--)
--VALUES
--(
--	17,12,'frmProjects.aspx','�Finish Project� button from Project Details'
--	,1,1,1,1
--	,0,0,0,0
--	,0,0,0,0
--	,0,0,0,0
--	,0,0,0,0
--)

----------------------

--DELETE FROM PageMaster WHERE numModuleID=12 AND numPageID IN (9,10,11,12,135)

----------------------

--UPDATE PageMaster SET vcFileName='frmProjects.aspx' WHERE numModuleID=12 AND numPageID=13
--UPDATE PageMaster SET vcFileName='frmProjectList.aspx' WHERE numModuleID=12 AND numPageID=14

----------------------

--UPDATE 
--	PageMaster 
--SET 
--	bitIsAddApplicable=1,bitAddPermission1Applicable=0,bitAddPermission2Applicable=0,bitAddPermission3Applicable=1
--	,bitIsUpdateApplicable=1,bitUpdatePermission1Applicable=1,bitUpdatePermission2Applicable=0,bitUpdatePermission3Applicable=1
--	,bitIsDeleteApplicable=1,bitDeletePermission1Applicable=1,bitDeletePermission2Applicable=0,bitDeletePermission3Applicable=1
--WHERE 
--	numModuleID=1 AND numPageID=1

------------------------

--UPDATE 
--	PageMaster 
--SET 
--	bitIsAddApplicable=1,bitAddPermission1Applicable=0,bitAddPermission2Applicable=0,bitAddPermission3Applicable=1
--WHERE 
--	numModuleID=1 AND numPageID=5

--UPDATE 
--	PageMaster 
--SET 
--	bitIsAddApplicable=1,bitAddPermission1Applicable=0,bitAddPermission2Applicable=0,bitAddPermission3Applicable=1
--WHERE 
--	numModuleID=1 AND numPageID=6

------------------------

--UPDATE 
--	PageMaster 
--SET 
--	vcFileName='frmTicklerdisplay.aspx'
--WHERE 
--	numModuleID=1 
--	AND vcFileName='ticklerdisplay.aspx'

--UPDATE 
--	PageMaster 
--SET 
--	vcFileName='frmTicklerdisplay.aspx',bitViewPermission1Applicable=0,bitViewPermission2Applicable=0,bitViewPermission3Applicable=1
--WHERE 
--	numModuleID=1 AND numPageID=10

----------------------------------------

--UPDATE PageMaster SET bitDeleted=1 WHERE numModuleID=1 AND numPageID IN (2,3,4,7,9)

----------------------------------------

--UPDATE
--	GroupAuthorization
--SET
--	intViewAllowed=3
--	,intAddAllowed=3
--	,intUpdateAllowed=3
--	,intDeleteAllowed=3
--	,intExportAllowed=0
--	,intPrintAllowed=0
--WHERE	
--	numPageID=1
--	AND numModuleID=1
--	AND numGroupID IN (SELECT MIN(numGroupID) FROM AuthenticationGroupMaster WHERE tintGroupType=1 GROUP BY numDomainID)

-------------------------------------------

--UPDATE
--	GroupAuthorization
--SET
--	intViewAllowed=3
--	,intAddAllowed=3
--	,intUpdateAllowed=3
--	,intDeleteAllowed=3
--	,intExportAllowed=0
--	,intPrintAllowed=0
--WHERE	
--	numPageID=10
--	AND numModuleID=1
--	AND numGroupID IN (SELECT MIN(numGroupID) FROM AuthenticationGroupMaster WHERE tintGroupType=1 GROUP BY numDomainID)

--------------------------------------------

--INSERT INTO PageMasterGroupSetting 
--(
--	numPageID,numModuleID,numGroupID
--	,bitIsViewApplicable,bitViewPermission1Applicable,bitViewPermission2Applicable,bitViewPermission3Applicable
--	,bitIsAddApplicable,bitAddPermission1Applicable,bitAddPermission2Applicable,bitAddPermission3Applicable
--	,bitIsUpdateApplicable,bitUpdatePermission1Applicable,bitUpdatePermission2Applicable,bitUpdatePermission3Applicable
--	,bitIsDeleteApplicable,bitDeletePermission1Applicable,bitDeletePermission2Applicable,bitDeletePermission3Applicable
--	,bitIsExportApplicable,bitExportPermission1Applicable,bitExportPermission2Applicable,bitExportPermission3Applicable
--)
--SELECT
--	1,1,numGroupID,1,1,0,0,0,0,0,0,1,1,0,0,1,1,0,0,0,0,0,0
--FROM
--	AuthenticationGroupMaster
--WHERE 
--	tintGroupType=4 
--UNION
--SELECT
--	5,1,numGroupID,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
--FROM
--	AuthenticationGroupMaster
--WHERE 
--	tintGroupType=4 
--UNION
--SELECT
--	6,1,numGroupID,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
--FROM
--	AuthenticationGroupMaster
--WHERE 
--	tintGroupType=4 
--UNION
--SELECT
--	10,1,numGroupID,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
--FROM
--	AuthenticationGroupMaster
--WHERE 
--	tintGroupType=4 

------------------------------------

---- BUSINESS PORTAL USER ACCESS --

--INSERT INTO ModuleMasterGroupType
--(
--	numModuleID
--	,tintGroupType
--)
--VALUES
--(32,2)
--,(2,2)
--,(3,2)
--,(4,2)
--,(11,2)
--,(37,2)
--,(10,2)
--,(46,2)
--,(12,2)
--,(7,2)
--,(16,2)

-----------------------------------

--INSERT INTO PageMasterGroupSetting 
--(
--	numPageID,numModuleID,numGroupID
--	,bitIsViewApplicable,bitViewPermission1Applicable,bitViewPermission2Applicable,bitViewPermission3Applicable
--	,bitIsAddApplicable,bitAddPermission1Applicable,bitAddPermission2Applicable,bitAddPermission3Applicable
--	,bitIsUpdateApplicable,bitUpdatePermission1Applicable,bitUpdatePermission2Applicable,bitUpdatePermission3Applicable
--	,bitIsDeleteApplicable,bitDeletePermission1Applicable,bitDeletePermission2Applicable,bitDeletePermission3Applicable
--	,bitIsExportApplicable,bitExportPermission1Applicable,bitExportPermission2Applicable,bitExportPermission3Applicable
--)
--SELECT
--	numPageID,numModuleID,numGroupID
--	,1,1,0,0
--	,0,0,0,0
--	,0,0,0,0
--	,0,0,0,0
--	,0,0,0,0
--FROM 
--	AuthenticationGroupMaster, PageMaster
--WHERE
--	tintGroupType=2
--	AND numModuleID IN (32,2,3,4,11,37,10,46,12,7,16)

------------------------------------

--UPDATE
--	GA
--SET 
--	intExportAllowed=0,intPrintAllowed=0,intViewAllowed=1,intAddAllowed=0,intUpdateAllowed=0,intDeleteAllowed=0
--FROM
--	GroupAuthorization GA
--INNER JOIN
--	AuthenticationGroupMaster
--ON
--	GA.numGroupID=AuthenticationGroupMaster.numGroupID
--WHERE
--	tintGroupType=2
--	AND numPageID IN (SELECT numPageID FROM PageMaster WHERE numModuleID IN (32,2,3,4,11,37,10,46,12,7,16))

--INSERT INTO GroupAuthorization
--(
--	numPageID,numModuleID,numGroupID,intExportAllowed,intPrintAllowed,intViewAllowed,intAddAllowed,intUpdateAllowed,intDeleteAllowed,numDomainID
--)
--SELECT
--	numPageID,numModuleID,numGroupID,0,0,1,0,0,0,numDomainID
--FROM 
--	AuthenticationGroupMaster, PageMaster
--WHERE
--	tintGroupType=2
--	AND numModuleID IN (32,2,3,4,11,37,10,46,12,7,16)
--	AND NOT EXISTS (SELECT numPageID FROM GroupAuthorization GA WHERE GA.numPageID=PageMaster.numPageID AND GA.numModuleID=PageMaster.numModuleID AND GA.numGroupID =AuthenticationGroupMaster.numGroupID)

--INSERT INTO GroupAuthorization
--(
--	numPageID,numModuleID,numGroupID,intExportAllowed,intPrintAllowed,intViewAllowed,intAddAllowed,intUpdateAllowed,intDeleteAllowed,numDomainID
--)
--SELECT
--	numListItemID,32,numGroupID,0,0,1,0,0,0,numDomainID
--FROM 
--	AuthenticationGroupMaster
--CROSS APPLY
--(	
--	SELECT 
--		numListItemID 
--	FROM 
--		ListDetails 
--	WHERE 
--		numListID=5 
--		AND ((constFlag=1 AND numListItemID <> 46) OR constFlag=0)
--) PageMaster
--WHERE
--	tintGroupType=2
--	AND NOT EXISTS (SELECT numPageID FROM GroupAuthorization GA WHERE GA.numPageID=PageMaster.numListItemID AND GA.numModuleID=32 AND GA.numGroupID =AuthenticationGroupMaster.numGroupID)
-----------------------------------------

--UPDATE PageMasterGroupSetting SET bitIsViewApplicable=0,bitViewPermission1Applicable=0 WHERE numModuleID=4 AND numPageID IN (12,13,16,17)
--UPDATE GroupAuthorization SET intViewAllowed=0 WHERE numModuleID=4 AND numPageID IN (12,13,16,17) AND numGroupID IN (SELECT numGroupID FROM AuthenticationGroupMaster WHERE tintGroupType=2)
--UPDATE PageMasterGroupSetting SET bitIsViewApplicable=0,bitViewPermission1Applicable=0 WHERE numModuleID=11 AND numPageID IN (1,134)
--UPDATE GroupAuthorization SET intViewAllowed=0 WHERE numModuleID=11 AND numPageID IN (1,134) AND numGroupID IN (SELECT numGroupID FROM AuthenticationGroupMaster WHERE tintGroupType=2)
--UPDATE PageMasterGroupSetting SET bitIsViewApplicable=0,bitViewPermission1Applicable=0 WHERE numModuleID=37 AND numPageID IN (34,130,132,149)
--UPDATE GroupAuthorization SET intViewAllowed=0 WHERE numModuleID=37 AND numPageID IN (34,130,132,149) AND numGroupID IN (SELECT numGroupID FROM AuthenticationGroupMaster WHERE tintGroupType=2)
--UPDATE PageMasterGroupSetting SET bitIsViewApplicable=0,bitViewPermission1Applicable=0 WHERE numModuleID=2 AND numPageID IN (7,9,10,11,12)
--UPDATE GroupAuthorization SET intViewAllowed=0 WHERE numModuleID=2 AND numPageID IN (7,9,10,11,12) AND numGroupID IN (SELECT numGroupID FROM AuthenticationGroupMaster WHERE tintGroupType=2)
--UPDATE PageMasterGroupSetting SET bitIsViewApplicable=0,bitViewPermission1Applicable=0 WHERE numModuleID=16 AND numPageID IN (154,155,158,159,161,162,163,164,165)
--UPDATE GroupAuthorization SET intViewAllowed=0 WHERE numModuleID=16 AND numPageID IN (154,155,158,159,161,162,163,164,165) AND numGroupID IN (SELECT numGroupID FROM AuthenticationGroupMaster WHERE tintGroupType=2)
--UPDATE PageMasterGroupSetting SET bitIsViewApplicable=0,bitViewPermission1Applicable=0 WHERE numModuleID=10 AND numPageID IN (15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35)
--UPDATE GroupAuthorization SET intViewAllowed=0 WHERE numModuleID=10 AND numPageID IN (15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35) AND numGroupID IN (SELECT numGroupID FROM AuthenticationGroupMaster WHERE tintGroupType=2)
--UPDATE PageMasterGroupSetting SET bitIsViewApplicable=0,bitViewPermission1Applicable=0 WHERE numModuleID=12 AND numPageID IN (5,6,7,8,13,14,16,17)
--UPDATE GroupAuthorization SET intViewAllowed=0 WHERE numModuleID=12 AND numPageID IN (5,6,7,8,13,14,16,17) AND numGroupID IN (SELECT numGroupID FROM AuthenticationGroupMaster WHERE tintGroupType=2)
--UPDATE PageMasterGroupSetting SET bitIsViewApplicable=0,bitViewPermission1Applicable=0 WHERE numModuleID=3 AND numPageID IN (10,11,13,14,15)
--UPDATE GroupAuthorization SET intViewAllowed=0 WHERE numModuleID=3 AND numPageID IN (10,11,13,14,15) AND numGroupID IN (SELECT numGroupID FROM AuthenticationGroupMaster WHERE tintGroupType=2)
--UPDATE PageMasterGroupSetting SET bitIsViewApplicable=0,bitViewPermission1Applicable=0 WHERE numModuleID=32 AND numPageID IN (2)
--UPDATE GroupAuthorization SET intViewAllowed=0 WHERE numModuleID=32 AND numPageID IN (2) AND numGroupID IN (SELECT numGroupID FROM AuthenticationGroupMaster WHERE tintGroupType=2)
--UPDATE PageMasterGroupSetting SET bitIsViewApplicable=0,bitViewPermission1Applicable=0 WHERE numModuleID=7 AND numPageID IN (1,12,13)
--UPDATE GroupAuthorization SET intViewAllowed=0 WHERE numModuleID=7 AND numPageID IN (1,12,13) AND numGroupID IN (SELECT numGroupID FROM AuthenticationGroupMaster WHERE tintGroupType=2)
--UPDATE PageMasterGroupSetting SET bitIsAddApplicable=1,bitAddPermission1Applicable=1 WHERE numModuleID=7 AND numPageID = 1
--UPDATE GroupAuthorization SET intAddAllowed=1 WHERE numModuleID=7 AND numPageID = 1 AND numGroupID IN (SELECT numGroupID FROM AuthenticationGroupMaster WHERE tintGroupType=2)
--UPDATE PageMasterGroupSetting SET bitIsAddApplicable=1,bitAddPermission1Applicable=1,bitIsUpdateApplicable=1,bitUpdatePermission1Applicable=1,bitIsDeleteApplicable=1,bitDeletePermission1Applicable=1 WHERE numModuleID=10 AND numPageID = 5
--UPDATE GroupAuthorization SET intAddAllowed=1,intUpdateAllowed=1,intDeleteAllowed=1 WHERE numModuleID=10 AND numPageID = 5 AND numGroupID IN (SELECT numGroupID FROM AuthenticationGroupMaster WHERE tintGroupType=2)
--UPDATE PageMasterGroupSetting SET bitIsAddApplicable=1,bitAddPermission1Applicable=1 WHERE numModuleID=10 AND numPageID = 28
--UPDATE GroupAuthorization SET intAddAllowed=1 WHERE numModuleID=10 AND numPageID = 28 AND numGroupID IN (SELECT numGroupID FROM AuthenticationGroupMaster WHERE tintGroupType=2)
--UPDATE PageMasterGroupSetting SET bitIsAddApplicable=1,bitAddPermission1Applicable=1 WHERE numModuleID=10 AND numPageID = 1
--UPDATE GroupAuthorization SET intAddAllowed=1 WHERE numModuleID=10 AND numPageID = 1 AND numGroupID IN (SELECT numGroupID FROM AuthenticationGroupMaster WHERE tintGroupType=2)
--------------------------------------------

--DELETE FROM GroupTabDetails WHERE numTabId IN (1,4,5,7,80,112,132,133,134,135) AND numGroupId IN (SELECT numGroupId FROM AuthenticationGroupMaster WHERE tintGroupType=2)

-------------------------------------------

--DECLARE @TEMP TABLE
--(
--	numTabID INT
--	,numOrder INT
--)
--INSERT INTO @TEMP (numTabID,numOrder) VALUES (7,1),(80,2),(1,3),(134,4),(133,5),(135,6),(5,7),(4,8),(112,9),(132,10)

--INSERT INTO GroupTabDetails
--(
--	numGroupId,numTabId,numRelationShip,bitallowed,numOrder
--)
--SELECT
--	numGroupID,numTabID,0,1,numOrder
--FROM
--	AuthenticationGroupMaster,@TEMP
--WHERE
--	tintGroupType=2

-------------------------------------------

--DELETE FROM TreeNavigationAuthorization WHERE numTabID=1 AND numGroupID IN (SELECT numGroupID FROM AuthenticationGroupMaster WHERE tintGroupType=2)

--------------------------------------------

--DECLARE @TEMP TABLE
--(
--	numPageNavID INT
--)
--INSERT INTO @TEMP (numPageNavID) VALUES (122),(123),(125),(149)

--INSERT INTO TreeNavigationAuthorization
--(
--	numDomainID,numGroupID,numTabID,numPageNavID,bitVisible,tintType
--)
--SELECT
--	numDomainID,numGroupID,1,numPageNavID,1,1
--FROM
--	AuthenticationGroupMaster,@TEMP
--WHERE
--	tintGroupType=2

-------------------------------------------

--DELETE FROM TreeNavigationAuthorization WHERE numTabID=4 AND numGroupID IN (SELECT numGroupID FROM AuthenticationGroupMaster WHERE tintGroupType=2)

--------------------------------------------

--DECLARE @TEMP TABLE
--(
--	numPageNavID INT
--)
--INSERT INTO @TEMP (numPageNavID) VALUES (246),(247),(248)

--INSERT INTO TreeNavigationAuthorization
--(
--	numDomainID,numGroupID,numTabID,numPageNavID,bitVisible,tintType
--)
--SELECT
--	numDomainID,numGroupID,4,numPageNavID,1,1
--FROM
--	AuthenticationGroupMaster,@TEMP
--WHERE
--	tintGroupType=2

-------------------------------------------

--DELETE FROM TreeNavigationAuthorization WHERE numTabID=5 AND numGroupID IN (SELECT numGroupID FROM AuthenticationGroupMaster WHERE tintGroupType=2)

--------------------------------------------

--DECLARE @TEMP TABLE
--(
--	numPageNavID INT
--)
--INSERT INTO @TEMP (numPageNavID) VALUES (279),(280)

--INSERT INTO TreeNavigationAuthorization
--(
--	numDomainID,numGroupID,numTabID,numPageNavID,bitVisible,tintType
--)
--SELECT
--	numDomainID,numGroupID,5,numPageNavID,1,1
--FROM
--	AuthenticationGroupMaster,@TEMP
--WHERE
--	tintGroupType=2

-------------------------------------------

--DELETE FROM TreeNavigationAuthorization WHERE numTabID=7 AND numGroupID IN (SELECT numGroupID FROM AuthenticationGroupMaster WHERE tintGroupType=2)

--------------------------------------------

--DECLARE @TEMP TABLE
--(
--	numPageNavID INT
--)
--INSERT INTO @TEMP (numPageNavID) VALUES (6),(7),(11),(12)

--INSERT INTO TreeNavigationAuthorization
--(
--	numDomainID,numGroupID,numTabID,numPageNavID,bitVisible,tintType
--)
--SELECT
--	numDomainID,numGroupID,7,numPageNavID,1,1
--FROM
--	AuthenticationGroupMaster,@TEMP
--WHERE
--	tintGroupType=2

-------------------------------------------

--DELETE FROM TreeNavigationAuthorization WHERE numTabID=80 AND numGroupID IN (SELECT numGroupID FROM AuthenticationGroupMaster WHERE tintGroupType=2)

--------------------------------------------

--DECLARE @TEMP TABLE
--(
--	numPageNavID INT
--)
--INSERT INTO @TEMP (numPageNavID) VALUES (62),(1),(2),(3),(4),(71),(72),(76),(171),(240)

--INSERT INTO TreeNavigationAuthorization
--(
--	numDomainID,numGroupID,numTabID,numPageNavID,bitVisible,tintType
--)
--SELECT
--	numDomainID,numGroupID,80,numPageNavID,1,1
--FROM
--	AuthenticationGroupMaster,@TEMP
--WHERE
--	tintGroupType=2

-------------------------------------------

--DELETE FROM TreeNavigationAuthorization WHERE numTabID=132 AND numGroupID IN (SELECT numGroupID FROM AuthenticationGroupMaster WHERE tintGroupType=2)

--------------------------------------------

--DECLARE @TEMP TABLE
--(
--	numPageNavID INT
--)
--INSERT INTO @TEMP (numPageNavID) VALUES (270),(84)

--INSERT INTO TreeNavigationAuthorization
--(
--	numDomainID,numGroupID,numTabID,numPageNavID,bitVisible,tintType
--)
--SELECT
--	numDomainID,numGroupID,132,numPageNavID,1,1
--FROM
--	AuthenticationGroupMaster,@TEMP
--WHERE
--	tintGroupType=2

-------------------------------------------

--DELETE FROM TreeNavigationAuthorization WHERE numTabID=133 AND numGroupID IN (SELECT numGroupID FROM AuthenticationGroupMaster WHERE tintGroupType=2)

--------------------------------------------

--DECLARE @TEMP TABLE
--(
--	numPageNavID INT
--)
--INSERT INTO @TEMP (numPageNavID) VALUES (273),(124),(126),(207)

--INSERT INTO TreeNavigationAuthorization
--(
--	numDomainID,numGroupID,numTabID,numPageNavID,bitVisible,tintType
--)
--SELECT
--	numDomainID,numGroupID,133,numPageNavID,1,1
--FROM
--	AuthenticationGroupMaster,@TEMP
--WHERE
--	tintGroupType=2

-------------------------------------------

--DELETE FROM TreeNavigationAuthorization WHERE numTabID=134 AND numGroupID IN (SELECT numGroupID FROM AuthenticationGroupMaster WHERE tintGroupType=2)

--------------------------------------------

--DECLARE @TEMP TABLE
--(
--	numPageNavID INT
--)
--INSERT INTO @TEMP (numPageNavID) VALUES (13)

--INSERT INTO TreeNavigationAuthorization
--(
--	numDomainID,numGroupID,numTabID,numPageNavID,bitVisible,tintType
--)
--SELECT
--	numDomainID,numGroupID,134,numPageNavID,1,1
--FROM
--	AuthenticationGroupMaster,@TEMP
--WHERE
--	tintGroupType=2

-------------------------------------------

--DELETE FROM TreeNavigationAuthorization WHERE numTabID=135 AND numGroupID IN (SELECT numGroupID FROM AuthenticationGroupMaster WHERE tintGroupType=2)

--------------------------------------------

--DECLARE @TEMP TABLE
--(
--	numPageNavID INT
--)
--INSERT INTO @TEMP (numPageNavID) VALUES (274),(275)

--INSERT INTO TreeNavigationAuthorization
--(
--	numDomainID,numGroupID,numTabID,numPageNavID,bitVisible,tintType
--)
--SELECT
--	numDomainID,numGroupID,135,numPageNavID,1,1
--FROM
--	AuthenticationGroupMaster,@TEMP
--WHERE
--	tintGroupType=2

-------------------------------------------------------------------------------------------------------------------------
------------------------------------------ PLECE NEW SCRIPTS ABOVE IT ---------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------

--ALTER TABLE GenericDocuments ADD numFormFieldGroupId NUMERIC(18,0)

--ALTER TABLE UserMaster ADD vcMailAccessToken VARCHAR(MAX)
--ALTER TABLE UserMaster ADD vcMailRefreshToken VARCHAR(MAX)
--ALTER TABLE UserMaster ADD tintMailProvider TINYINT DEFAULT 1
--ALTER TABLE UserMaster ADD bitOauthImap BIT
--ALTER TABLE UserMaster ADD dtTokenExpiration DATETIME
--ALTER TABLE UserMaster ADD vcPSMTPUserName VARCHAR(100)

--ALTER TABLE Domain ADD vcMailAccessToken VARCHAR(MAX)
--ALTER TABLE Domain ADD vcMailRefreshToken VARCHAR(MAX)
--ALTER TABLE Domain ADD tintMailProvider TINYINT DEFAULT 1
--ALTER TABLE Domain ADD dtTokenExpiration DATETIME


--UPDATE Domain SET tintMailProvider=3 
--UPDATE UserMaster SET tintMailProvider=3  

-----------------------------------

--UPDATE DycFormField_Mapping SET bitAllowFiltering=1,vcAssociatedControlType='DateField' WHERE numFormID=141 AND vcFieldName LIKE '%Release%'

---------------------------------------

-- DO NOT EXECUTE ON PRODUCTION
--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,bitAddField
--)
--SELECT 
--	4,numFieldId,86,1,1,1
--FROM 
--	DycFieldMaster 
--WHERE 
--	numFieldId IN (289,290,291,349,393)

--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,bitAddField
--)
--SELECT 
--	4,numFieldId,88,1,1,1
--FROM 
--	DycFieldMaster 
--WHERE 
--	numFieldId IN (289,290,291,349,393)

-------------------------------------------------------------------------------------------------------------------------
------------------------------------------ PLECE NEW SCRIPTS ABOVE IT ---------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------

--DECLARE @numFieldID NUMERIC(18,0)

--INSERT INTO DycFieldMaster
--(
--	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField
--)
--VALUES
--(
--	4,'Buildable Qty','numBuildableQty','numBuildableQty','Item','N','R','Label',0,1,0,0,0,1
--)

--SET @numFieldID = SCOPE_IDENTITY()

--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering
--)
--VALUES
--(
--	4,@numFieldID,139,0,0,'Buildable Qty','Label',10,1,0,0,1,0,0,0,0
--)
-------------------------------------------------------------------------------------------------------------------------
------------------------------------------ PLECE NEW SCRIPTS ABOVE IT ---------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------

--BEGIN TRY
--BEGIN TRANSACTION

--	DECLARE @numMAXPageNavID INT
--	SELECT @numMAXPageNavID = MAX(numPageNavID) FROM PageNavigationDTL


--	INSERT INTO PageNavigationDTL
--	(
--		numPageNavID,numModuleID,numParentID,vcPageNavName,vcNavURL,bitVisible,numTabID,vcAddURL,bitAddIsPopup,intSortOrder
--	)
--	VALUES
--	(
--		(@numMAXPageNavID + 1),(SELECT TOP 1 numModuleID FROM ModuleMaster WHERE vcModuleName='Manufacturing'),(SELECT TOP 1 numPageNavID FROM PageNavigationDTL WHERE vcPageNavName='Manufacturing'),'Demand Plan','../Opportunity/frmPlangProcurement.aspx?PlanType=2',1,
--		(SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Manufacturing' AND numDomainID=1),'',0,8
--	)

--	INSERT INTO dbo.TreeNavigationAuthorization
--	(
--		numGroupID,
--		numTabID,
--		numPageNavID,
--		bitVisible,
--		numDomainID,
--		tintType
--	)
--	SELECT
--		MIN(numGroupID),
--		(SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Manufacturing' AND numDomainID=1),
--		(@numMAXPageNavID + 1),
--		1,
--		numDomainID,
--		1
--	FROM    
--		AuthenticationGroupMaster
--	GROUP BY 
--		numDomainID
--COMMIT
--END TRY
--BEGIN CATCH
--	IF @@TRANCOUNT > 0
--		ROLLBACK TRANSACTION;

--	SELECT 
--		ERROR_MESSAGE(),
--		ERROR_NUMBER(),
--		ERROR_SEVERITY(),
--		ERROR_STATE(),
--		ERROR_LINE(),
--		ISNULL(ERROR_PROCEDURE(), '-');
--END CATCH
-------------------------------------------------------------------------------------------------------------------------
------------------------------------------ PLECE NEW SCRIPTS ABOVE IT ---------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------

--USE [Production.2014]
--GO

--/****** Object:  Table [dbo].[DycFieldMasterSynonym]    Script Date: 14-Apr-20 11:13:41 AM ******/
--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

--SET ANSI_PADDING ON
--GO

--CREATE TABLE [dbo].[DycFieldMasterSynonym](
--	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
--	[numDomainID] [numeric](18, 0) NOT NULL,
--	[numFieldID] [numeric](18, 0) NOT NULL,
--	[bitCustomField] [bit] NOT NULL,
--	[vcSynonym] [varchar](200) NOT NULL,
--	[bitDefault] [bit] NOT NULL,
-- CONSTRAINT [PK_DycFieldMasterSynonym] PRIMARY KEY CLUSTERED 
--(
--	[ID] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
--) ON [PRIMARY]

--GO

--SET ANSI_PADDING ON
--GO

--------------------------------------------

--INSERT INTO DycFieldMasterSynonym 
--(numDomainID,numFieldID,bitCustomField,vcSynonym,bitDefault)
--VALUES
--(0,270,0,'Income',1)
--,(0,271,0,'Asset',1)
--,(0,272,0,'COGS',1)
--,(0,294,0,'Item Type',1)
--,(0,6,0,'Relationship',1)
--,(0,451,0,'Relationship Type',1)
--,(0,19,0,'Group',1)

------------------------------------------------


--UPDATE DycFieldMaster SET intFieldMaxLength=7 WHERE numFieldId IN (60)
--UPDATE DycFieldMaster SET intFieldMaxLength=15 WHERE numFieldId IN (220,225)
--UPDATE DycFieldMaster SET intFieldMaxLength=50 WHERE numFieldId IN (10,32,218,223,203,281,51,52,53,59,61,62,63,530)
--UPDATE DycFieldMaster SET intFieldMaxLength=100 WHERE numFieldId IN (3,203,217,222)
--UPDATE DycFieldMaster SET intFieldMaxLength=200 WHERE numFieldId IN (193)
--UPDATE DycFieldMaster SET intFieldMaxLength=250 WHERE numFieldId IN (202)
--UPDATE DycFieldMaster SET intFieldMaxLength=255 WHERE numFieldId IN (47,48,49,50)
--UPDATE DycFieldMaster SET intFieldMaxLength=300 WHERE numFieldId IN (189)


--UPDATE DycFieldMaster SET vcFieldDataType='V' WHERE numFieldId IN (203,380,386,287)
--UPDATE DycFieldMaster SET vcFieldDataType='M' WHERE numFieldId IN (209,289)
--UPDATE DycFieldMaster SET vcFieldDataType='M' WHERE numFieldId IN (195,200)


-------------------------------------------------------------------------------------------------------------------------
------------------------------------------ PLECE NEW SCRIPTS ABOVE IT ---------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------

--ALTER TABLE Domain ADD bitCloneLocationWithItem BIT
--ALTER TABLE OpportunityItems ADD dtPlannedStart DATETIME

--ALTER TABLE WorkOrder ADD numQtyReceived FLOAT DEFAULT 0
--ALTER TABLE WorkOrder ADD numUnitHourReceived FLOAT DEFAULT 0
--ALTER TABLE OppWarehouseSerializedItem ADD numWOID NUMERIC(18,0)
--ALTER TABLE Sales_process_List_Master ADD numBuildManager NUMERIC(18,0)

------------------------------------------------

--UPDATE PageNavigationDtl SET vcPageNavName='Assemblies & BOMs' WHERE numPageNavID=3 AND numModuleID=37 AND numParentID=62 AND vcPageNavName='Assemblies'

------------------------------------------------

----CHECK IF FIELD IS NOT ALREADY ADDED ON PRODUCTION
--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,[order],tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
--)
--VALUES
--(
--	3,314,143,0,0,'Serial/Lot #s','TextBox',11,11,1,1,0,0,1,0,0
--)

--------------------------------

--BEGIN TRY
--BEGIN TRANSACTION

--	DECLARE @numMAXPageNavID INT
--	SELECT @numMAXPageNavID = MAX(numPageNavID) FROM PageNavigationDTL


--	INSERT INTO PageNavigationDTL
--	(
--		numPageNavID,numModuleID,numParentID,vcPageNavName,vcNavURL,bitVisible,numTabID,vcAddURL,bitAddIsPopup
--	)
--	VALUES
--	(
--		(@numMAXPageNavID + 1)
--		,(SELECT TOP 1 numModuleID FROM ModuleMaster WHERE vcModuleName='Manufacturing')
--		,(SELECT TOP 1 numPageNavID FROM PageNavigationDTL WHERE vcPageNavName='Manufacturing')
--		,'Routings'
--		,'../admin/frmAdminBusinessProcess.aspx?tintProcessType=3'
--		,1
--		,(SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Manufacturing' AND numDomainID=1)
--		,''
--		,0
--	)

--	INSERT INTO dbo.TreeNavigationAuthorization
--	(
--		numGroupID,
--		numTabID,
--		numPageNavID,
--		bitVisible,
--		numDomainID,
--		tintType
--	)
--	SELECT  
--		MIN(numGroupID),
--		(SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Manufacturing' AND numDomainID=1),
--		(@numMAXPageNavID + 1),
--		1,
--		numDomainID,
--		1
--	FROM    
--		AuthenticationGroupMaster
--	GROUP BY
--		numDomainID
--COMMIT
--END TRY
--BEGIN CATCH
--	IF @@TRANCOUNT > 0
--		ROLLBACK TRANSACTION;

--	SELECT 
--		ERROR_MESSAGE(),
--		ERROR_NUMBER(),
--		ERROR_SEVERITY(),
--		ERROR_STATE(),
--		ERROR_LINE(),
--		ISNULL(ERROR_PROCEDURE(), '-');
--END CATCH

--------------------------------

--BEGIN TRY
--BEGIN TRANSACTION

--	DECLARE @numMAXPageNavID INT
--	SELECT @numMAXPageNavID = MAX(numPageNavID) FROM PageNavigationDTL


--	INSERT INTO PageNavigationDTL
--	(
--		numPageNavID,numModuleID,numParentID,vcPageNavName,vcNavURL,bitVisible,numTabID,vcAddURL,bitAddIsPopup
--	)
--	VALUES
--	(
--		(@numMAXPageNavID + 1)
--		,(SELECT TOP 1 numModuleID FROM ModuleMaster WHERE vcModuleName='Manufacturing')
--		,(SELECT TOP 1 numPageNavID FROM PageNavigationDTL WHERE vcPageNavName='Manufacturing')
--		,'Work Centers'
--		,'../admin/frmMasterList.aspx?ModuleId=9&ListId=35'
--		,1
--		,(SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Manufacturing' AND numDomainID=1)
--		,''
--		,0
--	)

--	INSERT INTO dbo.TreeNavigationAuthorization
--	(
--		numGroupID,
--		numTabID,
--		numPageNavID,
--		bitVisible,
--		numDomainID,
--		tintType
--	)
--	SELECT  
--		MIN(numGroupID),
--		(SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Manufacturing' AND numDomainID=1),
--		(@numMAXPageNavID + 1),
--		1,
--		numDomainID,
--		1
--	FROM    
--		AuthenticationGroupMaster
--	GROUP BY
--		numDomainID
--COMMIT
--END TRY
--BEGIN CATCH
--	IF @@TRANCOUNT > 0
--		ROLLBACK TRANSACTION;

--	SELECT 
--		ERROR_MESSAGE(),
--		ERROR_NUMBER(),
--		ERROR_SEVERITY(),
--		ERROR_STATE(),
--		ERROR_LINE(),
--		ISNULL(ERROR_PROCEDURE(), '-');
--END CATCH

-------------------------------------------------------------------------------------------------------------------------
------------------------------------------ PLECE NEW SCRIPTS ABOVE IT ---------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------

--ALTER TABLE OpportunityItems ADD numWOQty FLOAT

---------------------------------------------------------------

--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField
--)
--VALUES
--(
--	3,296,144,0,0,'Status','Label','Status',1,0,0,1,0,1
--)


----------------------------------------------------------------

--ALTER TABLE WorkOrder ADD numBuildPlan NUMERIC(18,0) DEFAULT 201

--ALTER TABLE WorkOrder ADD numQtyBuilt FLOAT DEFAULT 0

----------------------------------------------------------------

--UPDATE WorkOrder SET numBuildPlan = 201

----------------------------------------------------------------

--DECLARE @numFieldID NUMERIC(18,0)

--INSERT INTO DycFieldMaster
--(
--	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcListItemType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering
--)
--VALUES
--(
--	3,'Build Plan','numBuildPlan','numBuildPlan','BuildPlan','WorkOrder','N','R','SelectBox','LI',53,1,0,1,0,1,1,1,1,1
--)

--SET @numFieldID = SCOPE_IDENTITY()

--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField
--)
--VALUES
--(
--	3,@numFieldID,144,1,1,'Build Plan','SelectBox',1,0,0,1,1,1
--)

----------------------------------------------------------------

--UPDATE DycFieldMaster SET vcPropertyName='Quantity' WHERE numFieldId=299

----------------------------------------------------------------

--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField
--)
--VALUES
--(
--	3,299,144,1,1,'Quantity','TextBox',1,0,1,1,1,1
--)

----------------------------------------------------------------

---- DO NOT EXECUTE ON LIVE ALREADY EXECUTED
--SET IDENTITY_INSERT ListMaster ON

--INSERT INTO ListMaster
--(
--	numListID,vcListName,numCreatedBy,bintCreatedDate,numModifiedBy,bintModifiedDate,bitDeleted,bitFixed,numDomainID,bitFlag,numModuleID
--)
--VALUES
--(
--	53,'Build Plan',1,GETUTCDATE(),1,GETUTCDATE(),0,1,1,0,16
--)

--SET IDENTITY_INSERT ListMaster OFF

--------------------------------------------------------------
---- DO NOT EXECUTE ON LIVE ALREADY EXECUTED
--SET IDENTITY_INSERT ListDetails ON

--INSERT INTO ListDetails
--(
--	numListItemID,numListID,vcData,numCreatedBY,bitDelete,numDomainID,constFlag,sintOrder
--)
--VALUES
--(
--	201,53,'Based on whatever is picked ',1,0,1,1,1
--),
--(
--	202,53,'Only after full qty has been picked',1,0,1,1,2
--)

--SET IDENTITY_INSERT ListDetails OFF
-------------------------------------------------------------------------------------------------------------------------
------------------------------------------ PLECE NEW SCRIPTS ABOVE IT ---------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------

--DECLARE @numFieldID NUMERIC(18,0)

--INSERT INTO DycFieldMaster
--(
--	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcLookBackTableName,vcPropertyName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcListItemType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitDetailField,bitAllowSorting,bitAllowFiltering,bitWorkFlowField,vcGroup
--)
--VALUES
--(
--	3,'Deal-won date','bintOppToOrder','bintOppToOrder','OpportunityMaster','DealWonDate','V','R','Label','',0,1,0,0,0,1,1,1,1,1,'Order Fields'
--)

--SET @numFieldID = SCOPE_IDENTITY()


--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering
--)
--VALUES
--(
--	3,@numFieldID,39,0,0,'Deal-won date','DateField',1,0,0,1,0,1,1,1
--)


--INSERT INTO ReportFieldGroupMappingMaster
--(
--	numReportFieldGroupID,numFieldID,vcFieldName,vcAssociatedControlType,vcFieldDataType,bitAllowSorting,bitAllowGrouping,bitAllowAggregate,bitAllowFiltering
--)
--VALUES
--(
--	3,@numFieldID,'Deal-won date','DateField','D',1,1,0,1
--)

----------------------------------------------------------------

--INSERT INTO DynamicFormMaster
--(
--	numFormId, vcFormName,cCustomFieldsAssociated,cAOIAssociated,bitDeleted,tintFlag
--)
--VALUES
--(
--	145,'Work Order List Page Column Configuration','N','N',0,0
--)

--ALTER TABLE WorkOrder ADD numModifiedBy NUMERIC(18,0)
--ALTER TABLE WorkOrder ADD bintModifiedDate DATETIME
--ALTER TABLE WorkOrder ADD numCompletedBy NUMERIC(18,0)
--ALTER TABLE WorkOrder ADD monLabourCost DECIMAL(20,5)
--ALTER TABLE WorkOrder ADD monOverheadCost DECIMAL(20,5)
--ALTER TABLE StagePercentageDetailsTask ADD monHourlyRate DECIMAL(20,5)
--ALTER TABLE Domain ADD numOverheadServiceItemID NUMERIC(18,0)
--ALTER TABLE ProjectProgress ADD numWorkOrderID NUMERIC(18,0)

--ALTER TABLE StagePercentageDetailsTask ADD dtStartTime DATETIME
--ALTER TABLE StagePercentageDetailsTask ADD dtEndTime DATETIME

----------------------------------------------

--UPDATE UserMaster SET monOverTimeRate = monHourlyRate WHERE ISNULL(monOverTimeRate,0) = 0

----------------------------------------------

--INSERT INTO ListModule
--(
--	numModuleID,vcModuleName,bitVisible
--)
--VALUES
--(
--	15,'Process time entry',1
--)

------------------------------------------------------------------------

--SET IDENTITY_INSERT ListMaster ON

--INSERT INTO ListMaster
--(
--	numListID,vcListName,numCreatedBy,bitDeleted,bitFixed,numDomainID,bitFlag,vcDataType,numModuleID
--)
--VALUES
--(
--	52,'Reason for Pause',1,0,0,1,1,'string',15
--)

--SET IDENTITY_INSERT ListDetails OFF

-------------------------------------------------------------------------

--UPDATE DynamicFOrmMaster SET vcFormName='Work Order Grid Column Settings' WHERE vcFormName LIKE '%work order%' AND numFormId=33

--------------------------------------------------------------------------

--INSERT INTO DynamicFormMaster
--(
--	numFormId,vcFormName,cCustomFieldsAssociated,cAOIAssociated,bitDeleted,tintFlag
--)
--VALUES
--(
--	144,'Work Order Detail Page Layout Setting','Y','N',0,0
--)

---------------------------------------------------------------------------------

--DECLARE @numFieldID NUMERIC(18,0)
--INSERT INTO DycFieldMaster
--(
--	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcListItemType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitAllowFiltering
--)
--VALUES
--(
--	3,'Planned Start Date','dtmStartDate','dtmStartDate','PlannedStartDate','WorkOrder','V','R','DateField','',0,1,0,1,1,1,1,1,1,1,1
--)

--SET @numFieldID = SCOPE_IDENTITY()

--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField
--)
--VALUES
--(
--	3,@numFieldID,144,1,1,'Planned Start Date','DateField',1,0,1,1,1,1
--)

--------------------------------------------------------------------------

--DECLARE @numFieldID NUMERIC(18,0)
--INSERT INTO DycFieldMaster
--(
--	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcListItemType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitAllowFiltering
--)
--VALUES
--(
--	3,'Requested Finish','dtmEndDate','dtmEndDate','RequestedFinish','WorkOrder','V','R','DateField','',0,1,0,1,1,1,1,1,1,1,1
--)

--SET @numFieldID = SCOPE_IDENTITY()

--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField
--)
--VALUES
--(
--	3,@numFieldID,144,1,1,'Requested Finish','DateField',1,0,1,1,1,1
--)

--------------------------------------------------------------------------

--DECLARE @numFieldID NUMERIC(18,0)
--INSERT INTO DycFieldMaster
--(
--	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcListItemType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitAllowFiltering
--)
--VALUES
--(
--	3,'Actual Start','dtActualStartDate','dtActualStartDate','ActualStartDate','WorkOrder','V','R','DateField','',0,1,0,0,0,1,0,1,1,1,1
--)

--SET @numFieldID = SCOPE_IDENTITY()

--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField
--)
--VALUES
--(
--	3,@numFieldID,144,0,0,'Actual Start','DateField',1,0,0,1,0,1
--)


--------------------------------------------------------------------------

--DECLARE @numFieldID NUMERIC(18,0)
--INSERT INTO DycFieldMaster
--(
--	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcListItemType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitAllowFiltering
--)
--VALUES
--(
--	3,'Projected Finish','vcProjectedFinish','vcProjectedFinish','ProjectedFinish','WorkOrder','V','R','Label','',0,1,0,0,0,1,0,1,1,1,1
--)

--SET @numFieldID = SCOPE_IDENTITY()

--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField
--)
--VALUES
--(
--	3,@numFieldID,144,0,0,'Projected Finish','Label',1,0,0,1,0,1
--)

--------------------------------------------------------------------------

--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField
--)
--VALUES
--(
--	3,298,144,0,0,'Warehouse','Label',1,0,1,1,0,1
--)

---------------------------------------------------------------------------

--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField
--)
--VALUES
--(
--	3,96,144,0,0,'Sales Order','Label',1,0,1,1,0,1
--)

---------------------------------------------------------------------------

--DECLARE @numFieldID NUMERIC(18,0)
--INSERT INTO DycFieldMaster
--(
--	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcListItemType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitAllowFiltering
--)
--VALUES
--(
--	3,'Build Manager','numAssignedTo','numAssignedTo','AssignedTo','WorkOrder','N','R','SelectBox','U',0,1,0,1,1,1,1,1,1,1,1
--)

--SET @numFieldID = SCOPE_IDENTITY()

--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField
--)
--VALUES
--(
--	3,@numFieldID,144,1,1,'Build Manager','SelectBox',1,0,1,1,1,1
--)

---------------------------------------------------------------------------


--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField
--)
--VALUES
--(
--	3,302,144,1,1,'Comments','TextArea',1,0,1,1,1,1
--)

--------------------------------------------------------------------------

--UPDATE DycFieldMaster SET vcPropertyName='WarehouseName' WHERE numFieldId=298
--UPDATE DycFieldMaster SET vcPropertyName='Comments' WHERE numFieldId=302

--------------------------------------------------------------------------

--SET IDENTITY_INSERT ModuleMaster ON

--INSERT INTO ModuleMaster
--(
--	numModuleID,vcModuleName,tintGroupType
--)
--VALUES
--(
--	16,'Manufacturing',1
--)

--SET IDENTITY_INSERT ModuleMaster OFF

-------------------------------------------------------------------------------


--INSERT INTO PageMaster
--(
--	numPageID,numModuleID,vcFileName,vcPageDesc,bitIsViewApplicable,bitIsAddApplicable,bitIsUpdateApplicable,bitIsDeleteApplicable,bitIsExportApplicable
--)
--VALUES
--(152,16,'frmWorkOrderList.aspx','Work Orders List',1,1,1,1,0)
--,(153,16,'frmWorkOrder.aspx','Work Order Detail',1,0,1,1,0)


---------------------------------------------------------------------------

--USE [Production.2014]
--GO

--/****** Object:  Table [dbo].[WorkSchedule]    Script Date: 05-Dec-19 11:24:52 AM ******/
--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

--SET ANSI_PADDING ON
--GO

--CREATE TABLE [dbo].[WorkSchedule](
--	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
--	[numDomainID] [numeric](18, 0) NOT NULL,
--	[numUserCntID] [numeric](18, 0) NOT NULL,
--	[numWorkHours] [tinyint] NOT NULL,
--	[numWorkMinutes] [tinyint] NOT NULL,
--	[numProductiveHours] [tinyint] NOT NULL,
--	[numProductiveMinutes] [tinyint] NOT NULL,
--	[tmStartOfDay] [time](7) NOT NULL,
--	[vcWorkDays] [varchar](20) NOT NULL,
-- CONSTRAINT [PK_WorkSchedule] PRIMARY KEY CLUSTERED 
--(
--	[ID] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
--) ON [PRIMARY]

--GO

--SET ANSI_PADDING ON
--GO


-------------------------------------------------

--USE [Production.2014]
--GO

--/****** Object:  Table [dbo].[WorkScheduleDaysOff]    Script Date: 05-Dec-19 11:25:16 AM ******/
--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

--CREATE TABLE [dbo].[WorkScheduleDaysOff](
--	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
--	[numWorkScheduleID] [numeric](18, 0) NOT NULL,
--	[dtDayOffFrom] [date] NOT NULL,
--	[dtDayOffTo] [date] NOT NULL,
-- CONSTRAINT [PK_WorkScheduleDaysOff] PRIMARY KEY CLUSTERED 
--(
--	[ID] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
--) ON [PRIMARY]

--GO

--ALTER TABLE [dbo].[WorkScheduleDaysOff]  WITH CHECK ADD  CONSTRAINT [FK_WorkScheduleDaysOff_WorkSchedule] FOREIGN KEY([numWorkScheduleID])
--REFERENCES [dbo].[WorkSchedule] ([ID])
--ON DELETE CASCADE
--GO

--ALTER TABLE [dbo].[WorkScheduleDaysOff] CHECK CONSTRAINT [FK_WorkScheduleDaysOff_WorkSchedule]
--GO

---------------------------------------------------------------

--ALTER TABLE UserMaster ADD tintPayrollType TINYINT
--ALTER TABLE UserMaster ADD tintHourType TINYINT

----------------------------------------------------------------

--USE [Production.2014]
--GO

--/****** Object:  Table [dbo].[StagePercentageDetailsTaskTimeLog]    Script Date: 28-Jan-20 11:17:36 AM ******/
--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

--SET ANSI_PADDING ON
--GO

--CREATE TABLE [dbo].[StagePercentageDetailsTaskTimeLog](
--	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
--	[numDomainID] [numeric](18, 0) NOT NULL,
--	[numUserCntID] [numeric](18, 0) NOT NULL,
--	[numTaskID] [numeric](18, 0) NOT NULL,
--	[tintAction] [tinyint] NOT NULL,
--	[dtActionTime] [datetime] NOT NULL,
--	[numProcessedQty] [float] NOT NULL,
--	[numReasonForPause] [numeric](18, 0) NOT NULL,
--	[vcNotes] [varchar](1000) NOT NULL,
--	[bitManualEntry] [bit] NOT NULL,
--	[numModifiedBy] [numeric](18, 0) NULL,
--	[dtModifiedDate] [datetime] NULL,
-- CONSTRAINT [PK_StagePercentageDetailsTaskTimeLog] PRIMARY KEY CLUSTERED 
--(
--	[ID] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
--) ON [PRIMARY]

--GO

--SET ANSI_PADDING ON
--GO

--ALTER TABLE [dbo].[StagePercentageDetailsTaskTimeLog]  WITH CHECK ADD  CONSTRAINT [FK_StagePercentageDetailsTaskTimeLog_StagePercentageDetailsTask] FOREIGN KEY([numTaskID])
--REFERENCES [dbo].[StagePercentageDetailsTask] ([numTaskId])
--GO

--ALTER TABLE [dbo].[StagePercentageDetailsTaskTimeLog] CHECK CONSTRAINT [FK_StagePercentageDetailsTaskTimeLog_StagePercentageDetailsTask]
--GO

-------------------------------------------------------------------------------------------------------------------------
------------------------------------------ PLECE NEW SCRIPTS ABOVE IT ---------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------

--USE [Production.2014]
--GO

--/****** Object:  Table [dbo].[RecordOrganizationChangeHistory]    Script Date: 29-Nov-19 10:24:12 AM ******/
--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

--CREATE TABLE [dbo].[RecordOrganizationChangeHistory](
--	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
--	[numDomainID] [numeric](18, 0) NOT NULL,
--	[numOppID] [numeric](18, 0) NULL,
--	[numProjectID] [numeric](18, 0) NULL,
--	[numCaseID] [numeric](18, 0) NULL,
--	[numOldDivisionID] [numeric](18, 0) NOT NULL,
--	[numNewDivisionID] [numeric](18, 0) NOT NULL,
--	[numModifiedBy] [numeric](18, 0) NOT NULL,
--	[dtModified] [datetime] NOT NULL,
-- CONSTRAINT [PK_RecordOrganizationChangeHistory] PRIMARY KEY CLUSTERED 
--(
--	[ID] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
--) ON [PRIMARY]

--GO



-----------------------------------------------

--ALTER TABLE SalesOrderConfiguration ADD bitDisplayItemGridItemPromotion BIT
--ALTER TABLE SalesOrderConfiguration ADD bitDisplayApplyPromotionCode BIT
--ALTER TABLE SalesOrderConfiguration ADD bitDisplayCreateOrderStartNewButton BIT
--ALTER TABLE SalesOrderConfiguration ADD bitDisplayCreateOpenUnpaidInvoiceButton BIT
--ALTER TABLE SalesOrderConfiguration ADD bitDisplayPayButton BIT
--ALTER TABLE SalesOrderConfiguration ADD bitDisplayPOSPay BIT

--------------------------------------------------

--DELETE FROM TabMaster WHERE numTabId IN (76,103) AND bitFixed=1

--UPDATE TabMaster SET numTabName='Opportunities & Orders',Remarks='Opportunities & Orders' WHERE numTabId=1
----------------------------------------------------

--UPDATE BizFormWizardModule SET vcModule='Projects, Cases & Activities' WHERE numBizFormModuleID=3
--UPDATE BizFormWizardModule SET vcModule='Web to Lead Forms' WHERE numBizFormModuleID=7
--UPDATE BizFormWizardModule SET vcModule='Organizations (Custom Relationships)' WHERE numBizFormModuleID=9
--UPDATE BizFormWizardModule SET vcModule='Activities' WHERE numBizFormModuleID=11

------------------------------------------------------------

--ALTER TABLE BizFormWizardModule ADD tintSortOrder INT

-----------------------------------------------------------------------

--UPDATE BizFormWizardModule SET tintSortOrder=1 WHERE numBizFormModuleID=1
--UPDATE BizFormWizardModule SET tintSortOrder=2 WHERE numBizFormModuleID=2
--UPDATE BizFormWizardModule SET tintSortOrder=3 WHERE numBizFormModuleID=9
--UPDATE BizFormWizardModule SET tintSortOrder=4 WHERE numBizFormModuleID=3
--UPDATE BizFormWizardModule SET tintSortOrder=5 WHERE numBizFormModuleID=11
--UPDATE BizFormWizardModule SET tintSortOrder=6 WHERE numBizFormModuleID=4
--UPDATE BizFormWizardModule SET tintSortOrder=7 WHERE numBizFormModuleID=5
--UPDATE BizFormWizardModule SET tintSortOrder=8 WHERE numBizFormModuleID=6
--UPDATE BizFormWizardModule SET tintSortOrder=9 WHERE numBizFormModuleID=7
--UPDATE BizFormWizardModule SET tintSortOrder=10 WHERE numBizFormModuleID=8
--UPDATE BizFormWizardModule SET tintSortOrder=11 WHERE numBizFormModuleID=10

------------------------------------------------------------------------

--UPDATE DycFieldMaster SET vcListItemType='LI',numListID=18 WHERE numFieldId IN (7,116)
--UPDATE DycFieldMaster SET vcListItemType='LI',numListID=18 WHERE vcOrigDbColumnName='numCampaignID' AND numListID=24


--UPDATE DycFormField_Mapping SET vcFieldName='Campaign' WHERE numFieldId IN (SELECT ISNULL(numFieldID,0) FROM DycFieldMaster WHERE ISNULL(vcOrigDbColumnName,'')='vcHow')
--UPDATE DycFieldMaster SET vcFieldName='Campaign' WHERE vcOrigDbColumnName='vcHow'


--DELETE FROM BizFormWizardMasterConfiguration WHERE numFormID IN (SELECT numFormID FROM DycFormField_Mapping WHERE vcFieldName='Campaign' GROUP BY numFormID,vcFieldName HAVING COUNT(*) > 1) AND numFieldID IN (7,116)
--DELETE FROM DycFormConfigurationDetails WHERE numFormID IN (SELECT numFormID FROM DycFormField_Mapping WHERE vcFieldName='Campaign' GROUP BY numFormID,vcFieldName HAVING COUNT(*) > 1) AND numFieldID IN (7,116)
--DELETE FROM DycFormField_Mapping WHERE numFormID IN (SELECT numFormID FROM DycFormField_Mapping WHERE vcFieldName='Campaign' GROUP BY numFormID,vcFieldName HAVING COUNT(*) > 1) AND numFieldID IN (7,116)

--UPDATE DynamicFormField_Validation SET vcNewFormFieldName='Campaign' WHERE numFormFieldId IN (SELECT ISNULL(numFieldID,0) FROM DycFieldMaster WHERE ISNULL(vcOrigDbColumnName,'')='vcHow')
--UPDATE DynamicFormField_Validation SET vcNewFormFieldName='Campaign' WHERE numFormFieldId IN (SELECT ISNULL(numFieldID,0) FROM DycFieldMaster WHERE ISNULL(vcOrigDbColumnName,'')='vcHow')


--DELETE FROM TreeNavigationAuthorization WHERE numPageNavID IN (SELECT numPageNavID FROM PageNavigationDTL WHERE numParentID=16)
--DELETE FROM PageNavigationDTL WHERE numParentID=16
--UPDATE PageNavigationDTL SET vcAddURL='',bitAddIsPopUp=0,vcNavURL='../Marketing/frmCampaignList.aspx' WHERE numPageNavID=16

----------------------------------


--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,tintROw,tintColumn,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
--)
--VALUES
--(
--	3,274,141,0,0,'Item Group','SelectBox',38,1,38,1,0,0,1,1,1
--)

-----------------------------------

--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,PopupFunctionName,[order],bitInResults,bitDeleted,bitDefault,bitImport,bitAllowFiltering,intSectionID
--)
--SELECT
--	numModuleID,numFieldID,20,bitAllowEdit,bitInlineEdit,'Re-Order Point',vcAssociatedControlType,vcPropertyName,PopupFunctionName,[order],bitInResults,bitDeleted,bitDefault,bitImport,bitAllowFiltering,intSectionID
--FROM 
--	DycFormField_Mapping
--WHERE 
--	numFormID=48 AND numFieldID=200

------------------------------------

--ALTER TABLE OpportunityMaster ADD numProjectID NUMERIC(18,0)

-------------------------------------

--DECLARE @numFieldID NUMERIC(18,0)

--INSERT INTO [dbo].[DycFieldMaster]
--(
--	[numModuleID],[numDomainID],[vcFieldName],[vcDbColumnName],[vcOrigDbColumnName],[vcPropertyName],[vcLookBackTableName],[vcFieldDataType],[vcFieldType],[vcAssociatedControlType],[vcToolTip],[vcListItemType],[numListID],[PopupFunctionName],[order],[tintRow],[tintColumn],[bitInResults],[bitDeleted],[bitAllowEdit],[bitDefault],[bitSettingField],[bitAddField],[bitDetailField],[bitAllowSorting],[bitWorkFlowField],[bitImport],[bitExport],[bitAllowFiltering],[bitInlineEdit],[bitRequired],[intColumnWidth],[intFieldMaxLength],[intWFCompare],[vcGroup],[vcWFCompareField]
--)     
--SELECT 
--	[numModuleID],[numDomainID],[vcFieldName],[vcDbColumnName],[vcOrigDbColumnName],'ProjectID','OpportunityMaster',[vcFieldDataType],[vcFieldType],[vcAssociatedControlType],[vcToolTip],[vcListItemType],[numListID],[PopupFunctionName],[order],[tintRow],[tintColumn],[bitInResults],[bitDeleted],[bitAllowEdit],[bitDefault],[bitSettingField],[bitAddField],[bitDetailField],[bitAllowSorting],[bitWorkFlowField],[bitImport],[bitExport],[bitAllowFiltering],[bitInlineEdit],[bitRequired],[intColumnWidth],[intFieldMaxLength],[intWFCompare],[vcGroup],[vcWFCompareField] 
--FROM 
--	DycFieldMaster 
--WHERE 
--	vcDbColumnName='numProjectID' 
--	AND vcLookBackTableName='OpportunityItems'

--SET @numFieldID = SCOPE_IDENTITY()

--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering
--)
--VALUES
--(
--	3,@numFieldID,39,1,1,'Project','SelectBox',1,0,0,1,0,1,1,1
--)

-------------------------------------------------------------------------------------------------------------------------
------------------------------------------ PLECE NEW SCRIPTS ABOVE IT ---------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------

--DECLARE @numFieldID NUMERIC(18,0)
--INSERT INTO DycFieldMaster
--(
--	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcListItemType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitAllowFiltering
--)
--VALUES
--(
--	3,'BizDoc Paid Date (fully Paid)','dtFullPaymentDate','dtFullPaymentDate','FullPaymentDate','OpportunityBizDocs','V','R','DateField','',0,1,0,0,0,1,0,0,1,0,1
--)

--SET @numFieldID = SCOPE_IDENTITY()

--INSERT INTO ReportFieldGroupMappingMaster
--(
--	numReportFieldGroupID,numFieldID,vcFieldName,vcAssociatedControlType,vcFieldDataType,bitAllowSorting,bitAllowGrouping,bitAllowAggregate,bitAllowFiltering
--)
--VALUES
--(
--	5,@numFieldID,'BizDoc Paid Date (fully Paid)','DateField','D',1,1,0,1
--)

-------------------------------------

--ALTER TABLE ECommerceDtl ADD tintWarehouseAvailability TINYINT
--ALTER TABLE ECommerceDtl ADD bitDisplayQtyAvailable BIT


--ALTER TABLE AccountTypeDetail ADD tintSortOrder INT
--ALTER TABLE AccountTypeDetail ADD bitActive BIT


--UPDATE AccountTypeDetail SET bitActive = 1

-------------------------------------------------------------------------------------------------------------------------
------------------------------------------ PLECE NEW SCRIPTS ABOVE IT ---------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------

--ALTER TABLE ImapUserDetails ALTER COLUMN vcGoogleRefreshToken VARCHAR(MAX)
--ALTER TABLE Activity ALTER COLUMN GoogleEventId VARCHAR(MAX)
--ALTER TABLE MassPurchaseFulfillmentConfiguration ADD bitReceiveBillOnClose BIT
--ALTER TABLE MassPurchaseFulfillmentConfiguration ADD tintBillType TINYINT 
--ALTER TABLE ShippingReport ADD vcPayerStreet VARCHAR(300)
--ALTER TABLE ShippingReport ADD vcPayerCity VARCHAR(100)
--ALTER TABLE ShippingReport ADD numPayerState NUMERIC(18,0)

--UPDATE MassPurchaseFulfillmentConfiguration SET tintBillType=1


--DECLARE @numFieldID NUMERIC(18,0)
--SELECT @numFieldID=numFieldID FROM DycFieldMaster WHERE vcDBColumnName='dtReleaseDate'

--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,bitAddField,intSectionID
--)
--VALUES
--(
--	3,@numFieldID,90,1,1,1,1
--)


--UPDATE DycFormField_Mapping SET vcFieldName='Shipped' WHERE numFOrmID=26 AND vcPropertyName='vcShippedReceived'
--UPDATE DycFormField_Mapping SET vcFieldName='Put Away' WHERE numFOrmID=129 AND vcPropertyName='vcShippedReceived'
--UPDATE DycFormField_Mapping SET vcFieldName='WH On-Hand' WHERE numFormID=139 AND numFieldID=900
-------------------------------------------

--DECLARE @numFieldID NUMERIC(18,0)
--SELECT @numFieldID=numFieldId FROM DycFieldMaster WHERE vcDbColumnName='numQtyReceived' AND vcLookBackTableName='OpportunityItems'

--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField
--)
--VALUES
--(
--	4,@numFieldID,129,0,0,'Received','Label',1,0,0,1,0,0
--)

-------------------------------------------

--UPDATE DycFormField_Mapping SET vcFieldName='Qty Put Away' WHERE numFormID=135 AND numFieldID=493

---------------------------------------------

--UPDATE DycFormField_Mapping SET vcFieldName='Item' WHERE numFormID=135 AND numFieldID=189

-----------------------------------------------

--DECLARE @numFieldID NUMERIC(18,0)
--SELECT @numFieldID=numFieldId FROM DycFieldMaster WHERE vcOrigDbColumnName  = 'vcBilled'

--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField
--)
--VALUES
--(
--	4,@numFieldID,135,0,0,'Qty Billed','Label',1,0,0,1,0,0
--)

----------------------------------------------------

--USE [Production.2014]
--GO

--/****** Object:  Table [dbo].[DivisionMasterShippingAccount]    Script Date: 10-Oct-19 5:14:02 PM ******/
--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

--SET ANSI_PADDING ON
--GO

--CREATE TABLE [dbo].[DivisionMasterShippingAccount](
--	[ID] [int] IDENTITY(1,1) NOT NULL,
--	[numDivisionID] [numeric](18, 0) NOT NULL,
--	[numShipViaID] [numeric](18, 0) NOT NULL,
--	[vcAccountNumber] [varchar](100) NOT NULL,
--	[vcStreet] [varchar](300) NOT NULL,
--	[vcCity] [varchar](100) NOT NULL,
--	[numCountry] [numeric](18, 0) NOT NULL,
--	[numState] [numeric](18, 0) NOT NULL,
--	[vcZipCode] [varchar](50) NOT NULL,
--	[numCreatedBy] [numeric](18, 0) NOT NULL,
--	[dtCreatedDate] [datetime] NOT NULL,
--	[numModifiedBy] [numeric](18, 0) NULL,
--	[dtModifiedDate] [datetime] NULL,
-- CONSTRAINT [PK_DivisionMasterShippingAccount] PRIMARY KEY CLUSTERED 
--(
--	[ID] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
--) ON [PRIMARY]

--GO

--SET ANSI_PADDING ON
--GO

--------------------------------------

--DECLARE @numFieldID NUMERIC(18,0)
--INSERT INTO DycFieldMaster
--(
--	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcListItemType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitAllowFiltering
--)
--VALUES
--(
--	4,'Total On-Hand','numTotalOnHand','numTotalOnHand','TotalOnHand','WareHouseItems','N','R','Label','',0,1,0,0,0,1,0,0,1,0,0
--)

--SET @numFieldID = SCOPE_IDENTITY()

--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFormID,numFieldID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering
--)
--VALUES
--(
--	4,139,@numFieldID,0,0,'Total On-Hand','Label','TotalOnHand',1,0,0,1,0,0,0,0
--)


-------------------------------------------------------------------------------------------------------------------------
------------------------------------------ PLECE NEW SCRIPTS ABOVE IT ---------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------


--UPDATE ReportFieldGroupMappingMaster SET vcFieldDataType='N'  WHERE numFieldID=258

--DECLARE @numFieldID NUMERIC(18,0)
--SET @numFieldID = (SELECT TOP 1 numFieldID FROM DycFieldMaster WHERE vcOrigDbCOlumnName='numRemainingQty' AND vcLookBackTableName='OpportunityItems')

--DECLARE @numFormFieldID NUMERIC(18,0)
--INSERT INTO DynamicFormFieldMaster
--(
--	numFormID,vcFormFieldName,vcFieldType,vcAssociatedControlType,numListID,bitDeleted,vcFieldDataType,bitAllowEdit,bitDefault,bitInResults,bitSettingField,vcDbColumnName
--)
--VALUES
--(
--	7,'Remaining','R','Label',0,0,'N',0,0,1,1,''
--)

--SET @numFormFieldID = SCOPE_IDENTITY()

--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,bitInResults,bitDeleted,bitDefault,numFormFieldID
--)
--VALUES
--(
--	3,@numFieldID,7,0,0,'Remaining','Label',1,0,0,@numFormFieldID
--)

----------------------------------

--ALTER TABLE MassPurchaseFulfillmentConfiguration ADD bitShowOnlyFullyReceived BIT

-------------------------------------

--SET IDENTITY_INSERT ReportFieldGroupMaster ON

--INSERT INTO ReportFieldGroupMaster
--(
--	numReportFieldGroupID,vcFieldGroupName,bitActive
--)
--VALUES
--(
--	35,'Associated Contacts',1
--)

--SET IDENTITY_INSERT ReportFieldGroupMaster OFF

----------------------------------------------------

--INSERT INTO ReportFieldGroupMappingMaster
--(
--	numReportFieldGroupID,numFieldID,vcFieldName,vcAssociatedControlType,vcFieldDataType,bitAllowSorting,bitAllowGrouping,bitAllowAggregate,bitAllowFiltering
--)
--VALUES
--(35,3,'(AC) Organization','TextBox','V',1,1,0,1)
--,(35,51,'(AC) First Name','TextBox','V',1,1,0,1)
--,(35,52,'(AC) Last Name','TextBox','V',1,1,0,1)
--,(35,53,'(AC) Email','TextBox','V',1,1,0,1)
--,(35,59,'(AC) Phone','TextBox','V',1,1,0,1)
--,(35,60,'(AC) Ext','TextBox','V',1,1,0,1)
--,(35,347,'(AC) Contact Role','SelectBox','N',1,1,0,1)

-------------------------------------

--INSERT INTO ReportModuleGroupFieldMappingMaster
--(
--	numReportModuleGroupID,numReportFieldGroupID
--)
--VALUES
--(8,35)
--,(9,35)
--,(10,35)
--,(13,35)

-----------------------------

--UPDATE DycFieldMaster SET vcDbColumnName='numRole',vcOrigDbColumnName='numRole'  WHERE numFieldId=347

-------------------------------------------------------------------------------------------------------------------------
------------------------------------------ PLECE NEW SCRIPTS ABOVE IT ---------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------

--UPDATE BizDocTemplate SET txtBizDocTemplate=REPLACE(txtBizDocTemplate,'#Customer/VendorBillToCompanyName#','#OppOrderBillToCompanyName#')
--UPDATE BizDocTemplate SET txtBizDocTemplate=REPLACE(txtBizDocTemplate,'#Customer/VendorBillToAddress#','#OppOrderBillToAddress#')
--UPDATE BizDocTemplate SET txtBizDocTemplate=REPLACE(txtBizDocTemplate,'#Customer/VendorBillToStreet#','#OppOrderBillToStreet#')
--UPDATE BizDocTemplate SET txtBizDocTemplate=REPLACE(txtBizDocTemplate,'#Customer/VendorBillToCity#','#OppOrderBillToCity#')
--UPDATE BizDocTemplate SET txtBizDocTemplate=REPLACE(txtBizDocTemplate,'#Customer/VendorBillToPostal#','#OppOrderBillToPostal#')
--UPDATE BizDocTemplate SET txtBizDocTemplate=REPLACE(txtBizDocTemplate,'#Customer/VendorBillToState#','#OppOrderBillToState#')
--UPDATE BizDocTemplate SET txtBizDocTemplate=REPLACE(txtBizDocTemplate,'#Customer/VendorBillToCountry#','#OppOrderBillToCountry#')
--UPDATE BizDocTemplate SET txtBizDocTemplate=REPLACE(txtBizDocTemplate,'#Customer/VendorBillToAddressName#','#OppOrderBillToAddressName#')
--UPDATE BizDocTemplate SET txtBizDocTemplate=REPLACE(txtBizDocTemplate,'#Customer/VendorShipToCompanyName#','#OppOrderShipToCompanyName#')
--UPDATE BizDocTemplate SET txtBizDocTemplate=REPLACE(txtBizDocTemplate,'#Customer/VendorShipToAddress#','#OppOrderShipToAddress#')
--UPDATE BizDocTemplate SET txtBizDocTemplate=REPLACE(txtBizDocTemplate,'#Customer/VendorShipToStreet#','#OppOrderShipToStreet#')
--UPDATE BizDocTemplate SET txtBizDocTemplate=REPLACE(txtBizDocTemplate,'#Customer/VendorShipToCity#','#OppOrderShipToCity#')
--UPDATE BizDocTemplate SET txtBizDocTemplate=REPLACE(txtBizDocTemplate,'#Customer/VendorShipToPostal#','#OppOrderShipToPostal#')
--UPDATE BizDocTemplate SET txtBizDocTemplate=REPLACE(txtBizDocTemplate,'#Customer/VendorShipToState#','#OppOrderShipToState#')
--UPDATE BizDocTemplate SET txtBizDocTemplate=REPLACE(txtBizDocTemplate,'#Customer/VendorShipToCountry#','#OppOrderShipToCountry#')
--UPDATE BizDocTemplate SET txtBizDocTemplate=REPLACE(txtBizDocTemplate,'#Customer/VendorShipToAddressName#','#OppOrderShipToAddressName#')
--UPDATE BizDocTemplate SET txtBizDocTemplate=REPLACE(txtBizDocTemplate,'#Customer/VendorChangeShipToHeader(Ship To)#','#OppOrderChangeShipToHeader(Ship To)#')
--UPDATE BizDocTemplate SET txtBizDocTemplate=REPLACE(txtBizDocTemplate,'#Customer/VendorChangeShipToHeader#','#OppOrderChangeShipToHeader#')
--UPDATE BizDocTemplate SET txtBizDocTemplate=REPLACE(txtBizDocTemplate,'#Customer/VendorChangeBillToHeader(Bill To)#','#OppOrderChangeBillToHeader(Bill To)#')
--UPDATE BizDocTemplate SET txtBizDocTemplate=REPLACE(txtBizDocTemplate,'#Customer/VendorChangeBillToHeader#','#OppOrderChangeBillToHeader#')

-------------------------

--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,tintROw,tintColumn,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
--)
--VALUES
--(
--	3,259,135,0,0,'Unit Price','TextBox',38,1,38,1,0,0,1,0,0
--),
--(
--	3,260,135,0,0,'Amount','TextBox',39,1,39,1,0,0,1,0,0
--)
---------------------------------------


--ALTER TABLE Domain ADD bitDoNotShowDropshipPOWindow BIT DEFAULT 0
--ALTER TABLE Domain ADD tintReceivePaymentTo TINYINT DEFAULT 2
--ALTER TABLE Domain ADD numReceivePaymentBankAccount NUMERIC(18,0) DEFAULT 0
-------------------------------------------

--UPDATE Domain SET bitDoNotShowDropshipPOWindow=0

------------------------------------------

--BEGIN TRY
--BEGIN TRANSACTION

--	INSERT INTO PageMaster 
--	(
--		numPageID,numModuleID,vcFileName,vcPageDesc,bitIsViewApplicable,bitIsAddApplicable,bitIsUpdateApplicable,bitIsDeleteApplicable,bitIsExportApplicable
--	)
--	VALUES
--	(
--		150,35,'frmAmtPaid.aspx','Receive Payment: Group with other undeposited Funds',1,0,0,0,0
--	),
--	(
--		151,35,'frmAmtPaid.aspx','Receive Payment: Deposit to drop down',1,0,0,0,0
--	)

--	SELECT 
--		* 
--	INTO 
--		#temp 
--	FROM
--	(
--		SELECT 
--			ROW_NUMBER() OVER(ORDER BY numDomainId DESC) AS Row, 
--			numDomainId 
--		FROM 
--			Domain 
--		WHERE 
--			numDomainId <> -255
--	) TABLE2
	
--	DECLARE @RowCount INT
--	SET @RowCount = (SELECT COUNT(Row) FROM #temp) 
	
--	DECLARE @I INT
--	DECLARE @numDomainId NUMERIC(18,0)
	
--	SET @I = 1

--	WHILE (@I <= @RowCount)
--	BEGIN
			
--		SELECT @numDomainId = numDomainID FROM #temp WHERE Row = @I
	
--		INSERT INTO dbo.GroupAuthorization
--			(numDomainID,numModuleID,numPageID,numGroupID,intExportAllowed,intPrintAllowed,intViewAllowed,intAddAllowed,intUpdateAllowed,intDeleteAllowed)
--		SELECT
--			@numDomainId,35,150,numGroupID,0,0,3,0,0,0
--		FROM
--			AuthenticationGroupMaster
--		WHERE
--			numDomainID = @numDomainId AND
--			tintGroupType = 1

--		INSERT INTO dbo.GroupAuthorization
--			(numDomainID,numModuleID,numPageID,numGroupID,intExportAllowed,intPrintAllowed,intViewAllowed,intAddAllowed,intUpdateAllowed,intDeleteAllowed)
--		SELECT
--			@numDomainId,35,151,numGroupID,0,0,3,0,0,0
--		FROM
--			AuthenticationGroupMaster
--		WHERE
--			numDomainID = @numDomainId AND
--			tintGroupType = 1

--		SET @i = @i + 1
--	END
--COMMIT
--END TRY
--BEGIN CATCH
--	IF @@TRANCOUNT > 0
--		ROLLBACK TRANSACTION;

--	SELECT 
--		ERROR_MESSAGE(),
--		ERROR_NUMBER(),
--		ERROR_SEVERITY(),
--		ERROR_STATE(),
--		ERROR_LINE(),
--		ISNULL(ERROR_PROCEDURE(), '-');
--END CATCH

-------------------------------------------------------------------------------------------------------------------------
------------------------------------------ PLECE NEW SCRIPTS ABOVE IT ---------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------

--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,tintROw,tintColumn,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
--)
--VALUES
--(
--	3,476,135,0,0,'Billing Date','DateField',37,1,37,1,0,0,1,0,0
--)


-------- Following update on production


--DECLARE @numFieldID NUMERIC(18,0)

--INSERT INTO DycFieldMaster
--(
--	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcListItemType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering
--)
--VALUES
--(
--	3,'Profit','monProfit','monProfit','Profit','OpportunityBizDocItems','M','R','Label','',0,0,0,0,0,0,0,0,1,1
--)


--SET @numFieldID = SCOPE_IDENTITY()


--INSERT INTO ReportFieldGroupMappingMaster
--(
--	numReportFieldGroupID,numFieldID,vcFieldName,vcAssociatedControlType,vcFieldDataType,bitAllowSorting,bitAllowFiltering,bitAllowGrouping,bitAllowAggregate
--)
--VALUES
--(
--	6,@numFieldID,'Profit','TextBox','M',1,1,0,0
--)

-----------------------------

--DECLARE @numFieldID NUMERIC(18,0)

--INSERT INTO DycFieldMaster
--(
--	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcListItemType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering
--)
--VALUES
--(
--	3,'Profit Percent','numProfitPercent','numProfitPercent','ProfitPercent','OpportunityBizDocItems','N','R','Label','',0,0,0,0,0,0,0,0,1,1
--)


--SET @numFieldID = SCOPE_IDENTITY()


--INSERT INTO ReportFieldGroupMappingMaster
--(
--	numReportFieldGroupID,numFieldID,vcFieldName,vcAssociatedControlType,vcFieldDataType,bitAllowSorting,bitAllowFiltering,bitAllowGrouping,bitAllowAggregate
--)
--VALUES
--(
--	6,@numFieldID,'Profit Percent','TextBox','N',1,1,0,0
--)

----------------------------
--DECLARE @numFieldID NUMERIC(18,0)
--INSERT INTO DycFieldMaster
--(
--	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcListItemType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitAllowFiltering
--)
--VALUES
--(
--	3,'Average Cost','monAverageCost','monAverageCost','AverageCost','OpportunityBizDocItems','M','R','TextBox','',0,1,0,0,0,1,0,0,1,0,1
--)

--SET @numFieldID = SCOPE_IDENTITY()

--INSERT INTO ReportFieldGroupMappingMaster
--(
--	numReportFieldGroupID,numFieldID,vcFieldName,vcAssociatedControlType,vcFieldDataType,bitAllowSorting,bitAllowFiltering,bitAllowGrouping,bitAllowAggregate
--)
--VALUES
--(
--	6,@numFieldID,'Average Cost','TextBox','M',1,1,0,0
--)
-------------------------------------------------------------------------------------------------------------------------
------------------------------------------ PLECE NEW SCRIPTS ABOVE IT ---------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------

--insert into dycformfield_mapping
--(
--	nummoduleid,numfieldid,numformid,bitallowedit,bitinlineedit,vcfieldname,vcassociatedcontroltype,tintrow,tintcolumn,[order],bitinresults,bitdeleted,bitdefault,bitsettingfield,bitallowsorting,bitallowfiltering
--)
--values
--(
--	4,110,135,0,0,'Invoice #','TextBox',11,1,11,1,0,1,1,0,0
--)


--ALTER TABLE MassSalesFulfillmentConfiguration ADD
--numOrderStatusPicked NUMERIC(18,0) DEFAULT 0
--,numOrderStatusPacked1 NUMERIC(18,0) DEFAULT 0
--,numOrderStatusPacked2 NUMERIC(18,0) DEFAULT 0
--,numOrderStatusPacked3 NUMERIC(18,0) DEFAULT 0
--,numOrderStatusPacked4 NUMERIC(18,0) DEFAULT 0
--,numOrderStatusInvoiced NUMERIC(18,0) DEFAULT 0
--,numOrderStatusPaid NUMERIC(18,0) DEFAULT 0
--,numOrderStatusClosed NUMERIC(18,0) DEFAULT 0

-------------------------------------------------------------------------------------------------------------------------
------------------------------------------ PLECE NEW SCRIPTS ABOVE IT ---------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------

--BEGIN TRY
--BEGIN TRANSACTION

--	DECLARE @numMAXPageNavID INT
--	SELECT @numMAXPageNavID = MAX(numPageNavID) FROM PageNavigationDTL


--	INSERT INTO PageNavigationDTL
--	(
--		numPageNavID,numModuleID,numParentID,vcPageNavName,vcNavURL,bitVisible,numTabID
--	)
--	VALUES
--	(
--		(@numMAXPageNavID + 1),10,122,'Purchase Fulfillment 2.0','../opportunity/frmMassPurchaseFulfillment.aspx',1,1
--	)

--	INSERT INTO dbo.TreeNavigationAuthorization
--	(
--		numGroupID,
--		numTabID,
--		numPageNavID,
--		bitVisible,
--		numDomainID,
--		tintType
--	)
--	SELECT  
--		numGroupID,
--		1,
--		(@numMAXPageNavID + 1),
--		1,
--		numDomainID,
--		1
--	FROM    
--		AuthenticationGroupMaster
--	WHERE
--		tintGroupType=1
--COMMIT
--END TRY
--BEGIN CATCH
--	IF @@TRANCOUNT > 0
--		ROLLBACK TRANSACTION;

--	SELECT 
--		ERROR_MESSAGE(),
--		ERROR_NUMBER(),
--		ERROR_SEVERITY(),
--		ERROR_STATE(),
--		ERROR_LINE(),
--		ISNULL(ERROR_PROCEDURE(), '-');
--END CATCH

-------------------------------

--ALTER TABLE OpportunityItems ADD numQtyReceived FLOAT
----ALTER TABLE OpportunityItems ADD dtReceivedDate DATE
--ALTER TABLE MassSalesFulfillmentConfiguration ADD bitGeneratePickListByOrder BIT DEFAULT 0

------------------------------

--UPDATE DycFormField_Mapping SET vcFieldName='Qty Received & Put-Away' WHERE numFormID=135 AND numFieldID=493

------------------------------

--DECLARE @numFieldID NUMERIC(18,0)

--INSERT INTO [dbo].[DycFieldMaster]
--(
--	[numModuleID],[numDomainID],[vcFieldName],[vcDbColumnName],[vcOrigDbColumnName],[vcPropertyName],[vcLookBackTableName],[vcFieldDataType],[vcFieldType],[vcAssociatedControlType],[vcToolTip],[vcListItemType],[numListID],[PopupFunctionName],[order],[tintRow],[tintColumn],[bitInResults],[bitDeleted],[bitAllowEdit],[bitDefault],[bitSettingField],[bitAddField],[bitDetailField],[bitAllowSorting],[bitWorkFlowField],[bitImport],[bitExport],[bitAllowFiltering],[bitInlineEdit],[bitRequired]
--)
--SELECT
--    [numModuleID],[numDomainID],'Qty Received Only','numQtyReceived','numQtyReceived','QtyReceivedOnly',[vcLookBackTableName],[vcFieldDataType],[vcFieldType],[vcAssociatedControlType],'Received Only Quantity',[vcListItemType],[numListID],[PopupFunctionName],[order],[tintRow],[tintColumn],[bitInResults],[bitDeleted],0,0,1,0,0,1,0,0,0,0,0,0
--FROM 
--	DycFieldMaster 
--WHERE 
--	numFieldId=493

--SET @numFieldID = SCOPE_IDENTITY()

--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting
--)
--VALUES
--(
--	3,@numFieldID,135,0,0,'Qty Received','Label',1,0,0,1,1
--)


------------------------------------------------

--USE [Production.2014]
--GO

--/****** Object:  Table [dbo].[MassPurchaseFulfillmentConfiguration]    Script Date: 12-AUG-19 9:10:43 AM ******/
--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

--CREATE TABLE [dbo].[MassPurchaseFulfillmentConfiguration](
--	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
--	[numDomainID] [numeric](18, 0) NOT NULL,
--	[numUserCntID] [numeric](18, 0) NOT NULL,
--	[bitGroupByOrderForReceive] [bit] NOT NULL,
--	[bitGroupByOrderForPutAway] [bit] NOT NULL,
--	[bitGroupByOrderForBill] [bit] NOT NULL,
--	[bitGroupByOrderForClose] [bit] NOT NULL,
--	[tintScanValue] [TINYINT] NOT NULL DEFAULT 1,
-- CONSTRAINT [PK_MassPurchaseFulfillmentConfiguration] PRIMARY KEY CLUSTERED 
--(
--	[ID] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
--) ON [PRIMARY]

--GO

------------------------

--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,tintROw,tintColumn,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
--)
--VALUES
--(
--	3,119,135,0,0,'Created','DateField',32,1,32,1,0,0,1,1,1
--)



--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,tintROw,tintColumn,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
--)
--VALUES
--(
--	3,101,135,0,0,'Order status','SelectBox',17,1,17,1,0,0,1,1,0
--),
--(
--	3,100,135,0,0,'Assigned To','SelectBox',35,1,35,1,0,0,1,1,1
--),
--(
--	3,111,135,0,0,'Rec Owner','SelectBox',36,1,36,1,0,0,1,1,1
--)

--UPDATE DycFormField_Mapping SET bitAllowSorting=1 WHERE numFormID=135 AND numFieldID=189
--UPDATE DycFormField_Mapping SET bitAllowFiltering=1 WHERE numFormID=135 AND numFieldID IN (3,96,189) -- Include SKU field on production

-------------------------

--INSERT INTO DynamicFormMaster
--(
--	numFormId,vcFormName,cCustomFieldsAssociated,cAOIAssociated,bitDeleted,tintFlag
--)
--VALUES
--(
--	143,'Mass Purchase Fulfillment Right Configuration','Y','N',0,0
--)

--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,tintROw,tintColumn,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
--)
--VALUES

------------------------

--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,tintROw,tintColumn,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
--)
--VALUES
--(
--	4,351,143,0,0,'Image','TextBox',1,1,1,1,0,1,1,0,0
--),
--(
--	4,251,143,0,0,'Item Name','TextBox',2,1,2,1,0,1,1,1,1
--),
--(
--	3,96,143,0,0,'Order Name','TextBox',3,1,3,1,0,0,1,1,1
--),
--(
--	3,253,143,0,0,'Description','TextBox',4,1,4,1,0,0,1,0,0
--),
--(
--	3,350,143,0,0,'Attributes','Label',5,1,5,1,0,0,1,0,0
--),
--(
--	3,281,143,0,0,'SKU','Label',6,1,6,1,0,1,1,1,1
--),
--(
--	3,203,143,0,0,'UPC','Label',7,1,7,1,0,0,1,1,1
--),
--(
--	3,233,143,0,0,'Put-Away Location','SelectBox',10,1,10,1,0,0,1,1,0
--)


--------------------------

--DECLARE @numFieldID INT = (SELECT TOP 1 numFieldId FROM DycFieldMaster WHERE vcLookBackTableName='OpportunityItems' AND vcOrigDbColumnName='numQtyToShipReceive')

--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,tintROw,tintColumn,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering,bitRequired
--)
--VALUES
--(
--	3,@numFieldID,143,0,0,'Put-Away','TextBox',8,1,8,1,0,1,1,1,0,1
--)

----------------------------------------------------


--DECLARE @numFieldID INT = (SELECT TOP 1 numFieldId FROM DycFieldMaster WHERE vcLookBackTableName='OpportunityItems' AND vcOrigDbColumnName='numRemainingQty')

--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,tintROw,tintColumn,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering,bitRequired
--)
--VALUES
--(
--	3,@numFieldID,143,0,0,'Remaining','Label',9,1,9,1,0,1,1,1,0,1
--)

---------------------------------------------------------

--DECLARE @numFieldID NUMERIC(18,0)
--SELECT @numFieldID=numFieldId FROM DycFieldMaster WHERE vcOrigDbColumnName='SerialLotNo' AND vcLookBackTableName='OpportunityItems'
--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering,bitRequired
--)
--VALUES
--(
--	3,@numFieldID,143,0,0,'Serial/Lot #s','TextBox',10,1,10,0,1,0,0,1
--)


---------------------------------------------------------

---- Following updated on production

--DECLARE @numFieldID NUMERIC(18,0)

--INSERT INTO DycFieldMaster
--(
--	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitDetailField,bitAllowSorting,bitAllowFiltering
--)
--VALUES
--(
--	3,'Received Date','dtItemReceivedDate','dtItemReceivedDate','','OpportunityMaster','V','R','DateField',0,1,0,1,0,1,1,1,1
--)

--SET @numFieldID = SCOPE_IDENTITY()

--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,tintROw,tintColumn,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
--)
--VALUES
--(
--	3,@numFieldID,141,0,0,'Received Date','DateField',37,1,37,1,0,0,1,1,1
--)

--SELECT * FROM DycFieldMaster WHERE vcOrigDbColumnName='dtItemReceivedDate'

-------------------------------------------------------------------------------------------------------------------------
------------------------------------------ PLECE NEW SCRIPTS ABOVE IT ---------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------

--ALTER TABLE OpportunityItems ADD bitWinningBid BIT
--ALTER TABLE OpportunityItems ADD numParentOppItemID NUMERIC(18,0)


--DECLARE @numFieldID NUMERIC(18,0)

--INSERT INTO DycFieldMaster
--(
--	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcListItemType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField
--)
--VALUES
--(
--	3,'Group','vcGroupNonDbField','vcGroupNonDbField','OpportunityItems','V','R','Label','',0,0,0,0,0,1
--)

--SET @numFieldID = SCOPE_IDENTITY()

--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,bitInResults,bitDeleted,bitDefault,bitSettingField
--)
--VALUES
--(
--	3,@numFieldID,26,0,0,'Group','Label',0,0,0,1
--)

-------------------------------------------------------------------------------------------------------------------------
------------------------------------------ PLECE NEW SCRIPTS ABOVE IT ---------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------

--ALTER TABLE MassSalesFulfillmentConfiguration ADD tintPendingCloseFilter TINYINT 

--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,tintROw,tintColumn,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
--)
--VALUES
--(
--	3,100,141,0,0,'Assigned To','SelectBox',35,1,35,1,0,0,1,1,1
--),
--(
--	3,111,141,0,0,'Rec Owner','SelectBox',36,1,36,1,0,0,1,1,1
--)

--ALTER TABLE Item ALTER COLUMN txtItemDesc VARCHAR(2000)
--ALTER TABLE OpportunityItems ALTER COLUMN vcItemDesc VARCHAR(2000)


---- FOLLOWING ARE EXECUTED ON PRODUCTION

--ALTER TABLE SalesReturnCommission ADD numComRuleID NUMERIC(18,0)

----------------------------------------

--DECLARE @numFieldID NUMERIC(18,0)

--INSERT INTO DycFieldMaster
--(
--	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcListItemType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering
--)
--VALUES
--(
--	3,'Profit','monProfit','monProfit','Profit','OpportunityMaster','M','R','Label','',0,0,0,0,0,0,0,0,1,1
--)


--SET @numFieldID = SCOPE_IDENTITY()


--INSERT INTO ReportFieldGroupMappingMaster
--(
--	numReportFieldGroupID,numFieldID,vcFieldName,vcAssociatedControlType,vcFieldDataType,bitAllowSorting,bitAllowFiltering,bitAllowGrouping,bitAllowAggregate
--)
--VALUES
--(
--	4,@numFieldID,'Profit','TextBox','M',1,1,0,0
--)

-----------------------------

--DECLARE @numFieldID NUMERIC(18,0)

--INSERT INTO DycFieldMaster
--(
--	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcListItemType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering
--)
--VALUES
--(
--	3,'Profit Percent','numProfitPercent','numProfitPercent','ProfitPercent','OpportunityMaster','N','R','Label','',0,0,0,0,0,0,0,0,1,1
--)


--SET @numFieldID = SCOPE_IDENTITY()


--INSERT INTO ReportFieldGroupMappingMaster
--(
--	numReportFieldGroupID,numFieldID,vcFieldName,vcAssociatedControlType,vcFieldDataType,bitAllowSorting,bitAllowFiltering,bitAllowGrouping,bitAllowAggregate
--)
--VALUES
--(
--	4,@numFieldID,'Profit Percent','TextBox','N',1,1,0,0
--)

-------------------------------------------------------------------------------------------------------------------------
------------------------------------------ PLECE NEW SCRIPTS ABOVE IT ---------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------

--BEGIN TRY
--BEGIN TRANSACTION

--	DECLARE @numMAXPageNavID INT
--	SELECT @numMAXPageNavID = MAX(numPageNavID) FROM PageNavigationDTL


--	INSERT INTO PageNavigationDTL
--	(
--		numPageNavID,numModuleID,numParentID,vcPageNavName,vcNavURL,bitVisible,numTabID
--	)
--	VALUES
--	(
--		(@numMAXPageNavID + 1),10,122,'Sales Fulfillment 2.0','../opportunity/frmMassSalesFulfillment.aspx',1,1
--	)

--	INSERT INTO dbo.TreeNavigationAuthorization
--	(
--		numGroupID,
--		numTabID,
--		numPageNavID,
--		bitVisible,
--		numDomainID,
--		tintType
--	)
--	SELECT  
--		numGroupID,
--		1,
--		(@numMAXPageNavID + 1),
--		1,
--		numDomainID,
--		1
--	FROM    
--		AuthenticationGroupMaster
--	WHERE
--		tintGroupType=1
--COMMIT
--END TRY
--BEGIN CATCH
--	IF @@TRANCOUNT > 0
--		ROLLBACK TRANSACTION;

--	SELECT 
--		ERROR_MESSAGE(),
--		ERROR_NUMBER(),
--		ERROR_SEVERITY(),
--		ERROR_STATE(),
--		ERROR_LINE(),
--		ISNULL(ERROR_PROCEDURE(), '-');
--END CATCH


---------------------------------------------

--ALTER TABLE OpportunityMaster ADD dtExpectedDate DATE
--ALTER TABLE OpportunityItems ADD numQtyPicked FLOAT
--ALTER TABLE OpportunityItems ADD vcInclusionDetail VARCHAR(MAX)
--ALTER TABLE ShippingReport ADD bitDelivered BIT DEFAULT 0
--ALTER TABLE ShippingReport ADD vcTrackingDetail VARCHAR(300)
--ALTER TABLE ShippingReport ADD dtLastTrackingUpdate DATETIME
----------------------------

--DECLARE @numFieldID NUMERIC(18,0)

--INSERT INTO DycFieldMaster
--(
--	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcListItemType,numListID,[order],tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,bitAllowEdit,bitSettingField,bitDetailField,bitAllowSorting,bitAllowFiltering,bitWorkFlowField
--)
--VALUES
--(
--	3,'Tracking Detail','vcTrackingDetail','vcTrackingDetail','TrackingDetail','ShippingReport','V','R','Label','',0,1,1,1,1,0,0,0,1,1,0,0,0
--)

--SET @numFieldID = SCOPE_IDENTITY()

--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitImport,bitAllowFiltering
--)
--VALUES
--(
--	3,@numFieldID,39,0,0,'Tracking Detail','Label','TrackingDetail',1,1,1,1,0,0,1,0,1,0,0,0
--)

----------------------------

--UPDATE 
--	OI
--SET
--	OI.vcInclusionDetail = [dbo].[GetOrderAssemblyKitInclusionDetails](OI.numOppID,OI.numoppitemtCode,OI.numUnitHour,1,1)
--FROM 
--	OpportunityItems OI
--INNER JOIN
--	Item I
--ON
--	OI.numItemCode = I.numItemCode
--WHERE
--	ISNULL(I.bitKitParent,0) = 1

----------------------------

--INSERT INTO DynamicFormMaster
--(
--	numFormId,vcFormName,cCustomFieldsAssociated,cAOIAssociated,bitDeleted,tintFlag
--)
--VALUES
--(
--	141,'Mass Sales Fulfillment Left Configuration','Y','N',0,0
--),
--(
--	142,'Mass Sales Fulfillment Right Configuration','Y','N',0,0
--)

----------------------------

--DECLARE @numFieldID NUMERIC(18,0)

--INSERT INTO DycFieldMaster
--(
--	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering
--)
--VALUES
--(
--	3,'Age','numAge','numAge','OrderAge','OpportunityMaster','N','R','Label',0,1,0,0,0,1,0,1,1,1
--)


--SET @numFieldID = SCOPE_IDENTITY()

--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,tintROw,tintColumn,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
--)
--VALUES
--(
--	3,@numFieldID,141,0,0,'Age','Label',1,1,1,1,0,0,1,1,0
--)

----------------------------

--DECLARE @numFieldID NUMERIC(18,0)

--INSERT INTO DycFieldMaster
--(
--	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering
--)
--VALUES
--(
--	3,'Anticipated Delivery','dtAnticipatedDelivery','dtAnticipatedDelivery','AnticipatedDelivery','OpportunityBizDocs','V','R','Label',0,1,0,0,0,1,0,1,1,1
--)


--SET @numFieldID = SCOPE_IDENTITY()

--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,tintROw,tintColumn,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
--)
--VALUES
--(
--	3,@numFieldID,141,0,0,'Anticipated Delivery','Label',2,1,2,1,0,0,1,0,0
--)

----------------------------

--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,tintROw,tintColumn,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
--)
--VALUES
--(
--	4,251,141,0,0,'Item Name','TextBox',3,1,3,1,0,1,1,1,1
--),
--(
--	4,273,141,0,0,'Classification','SelectBox',4,1,4,1,0,0,1,1,1
--),
--(
--	4,253,141,0,0,'Description','TextBox',5,1,5,1,0,0,1,0,0
--),
--(
--	4,350,141,0,0,'Attributes','Label',6,1,6,1,0,0,1,0,0
--),
--(
--	4,281,141,0,0,'SKU','TextBox',7,1,7,1,0,1,1,1,1
--)
--,
--(
--	4,203,141,0,0,'UPC','TextBox',8,1,8,1,0,0,1,1,1
--)

---------------------------


--DECLARE @numFieldID INT = (SELECT TOP 1 numFieldId FROM DycFieldMaster WHERE vcLookBackTableName='OpportunityItems' AND vcOrigDbColumnName='vcNotes')

--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,tintROw,tintColumn,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
--)
--VALUES
--(
--	3,@numFieldID,141,0,0,'Item Notes','TextBox',9,1,9,1,0,0,1,0,0
--)


--------------------------


--DECLARE @numFieldID INT = (SELECT TOP 1 numFieldId FROM DycFieldMaster WHERE vcLookBackTableName='OpportunityItems' AND vcOrigDbColumnName='vcInclusionDetails')

--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,tintROw,tintColumn,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
--)
--VALUES
--(
--	3,@numFieldID,141,0,0,'Inclusion Details','Label',10,1,10,1,0,0,1,0,0
--)

---------------------------

--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,tintROw,tintColumn,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
--)
--VALUES
--(
--	4,294,141,0,0,'Item Type','Label',11,1,11,1,0,0,1,1,0
--)


----------------------------

--DECLARE @numFieldID NUMERIC(18,0)

--INSERT INTO DycFieldMaster
--(
--	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering
--)
--VALUES
--(
--	3,'Ship Status','vcShipStatus','vcShipStatus','ShipStatus','OpportunityBizDocs','V','R','Label',0,1,0,0,0,1,0,1,1,0
--)


--SET @numFieldID = SCOPE_IDENTITY()

--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,tintROw,tintColumn,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
--)
--VALUES
--(
--	3,@numFieldID,141,0,0,'Ship Status','Label',12,1,12,1,0,0,1,0,0
--)

-------------------------------

--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,tintROw,tintColumn,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
--)
--VALUES
--(
--	4,233,141,0,0,'Location','TextBox',13,1,13,1,0,0,1,1,1
--)

----------------------------
---- DON'T EXECUTE ON SERVER
----DECLARE @numFieldID NUMERIC(18,0)

----INSERT INTO DycFieldMaster
----(
----	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering
----)
----VALUES
----(
----	3,'Left-to','numLeftTo','numLeftTo','LeftTo','OpportunityItems','N','R','Label',0,1,0,0,0,1,0,1,1,0
----)


----SET @numFieldID = SCOPE_IDENTITY()

----INSERT INTO DycFormField_Mapping
----(
----	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,tintROw,tintColumn,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
----)
----VALUES
----(
----	3,@numFieldID,141,0,0,'Left-to','Label',14,1,14,1,0,0,1,0,0
----)

---------------------------------------------

--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,tintROw,tintColumn,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
--)
--VALUES
--(
--	3,3,141,0,0,'Organization','TextBox',15,1,15,1,0,0,1,1,1
--),
--(
--	3,96,141,0,0,'Order Name','TextBox',16,1,16,1,0,1,1,1,1
--),
--(
--	3,101,141,0,0,'Order status','SelectBox',17,1,17,1,0,0,1,1,0
--)
--,
--(
--	3,122,141,0,0,'Comments','TextBox',18,1,18,1,0,0,1,0,0
--)

--------------------------


--DECLARE @numFieldID INT = (SELECT TOP 1 numFieldId FROM DycFieldMaster WHERE vcLookBackTableName='OpportunityItems' AND vcOrigDbColumnName='vcItemReleaseDate')

--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,tintROw,tintColumn,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
--)
--VALUES
--(
--	3,@numFieldID,141,0,0,'Release','DateField',19,1,19,1,0,0,1,1,0
--)

---------------------------------------------

--DECLARE @numFieldID NUMERIC(18,0)

--INSERT INTO DycFieldMaster
--(
--	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering
--)
--VALUES
--(
--	3,'Expected Date','dtExpectedDate','dtExpectedDate','ExpectedDate','OpportunityMaster','V','R','DateField',0,1,0,0,0,1,0,1,1,0
--)


--SET @numFieldID = SCOPE_IDENTITY()

--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,tintROw,tintColumn,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
--)
--VALUES
--(
--	3,@numFieldID,141,1,1,'Expected Date','DateField',20,1,20,1,0,1,1,1,1
--)

--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,tintROw,tintColumn,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering,bitDetailField
--)
--VALUES
--(
--	3,@numFieldID,39,1,1,'Expected Date','DateField',20,1,20,1,0,1,1,1,1,1
--)



--------------------------

--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,tintROw,tintColumn,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
--)
--VALUES
--(
--	3,269,141,0,0,'Amt Paid','Label',21,1,21,1,0,0,1,1,0
--)

--------------------------


--DECLARE @numFieldID INT = (SELECT TOP 1 numFieldId FROM DycFieldMaster WHERE vcLookBackTableName='OpportunityItems' AND vcOrigDbColumnName='numRemainingQty')

--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,tintROw,tintColumn,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
--)
--VALUES
--(
--	3,@numFieldID,141,0,0,'Remaining','Label',22,1,22,1,0,0,1,1,0
--)

---------------------------------------------

--DECLARE @numFieldID NUMERIC(18,0)

--INSERT INTO DycFieldMaster
--(
--	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering
--)
--VALUES
--(
--	3,'Picked','numQtyPicked','numQtyPicked','QtyPicked','OpportunityItems','V','R','Label',0,1,0,0,0,1,0,1,1,0
--)


--SET @numFieldID = SCOPE_IDENTITY()

--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,tintROw,tintColumn,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
--)
--VALUES
--(
--	3,@numFieldID,141,0,0,'Picked','Label',23,1,23,1,0,0,1,1,0
--)

---------------------------------------------

--DECLARE @numFieldID NUMERIC(18,0)

--INSERT INTO DycFieldMaster
--(
--	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering
--)
--VALUES
--(
--	3,'Packed','numQtyPacked','numQtyPacked','QtyPacked','OpportunityItems','V','R','Label',0,1,0,0,0,1,0,1,1,0
--)


--SET @numFieldID = SCOPE_IDENTITY()

--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,tintROw,tintColumn,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
--)
--VALUES
--(
--	3,@numFieldID,141,0,0,'Packed','Label',24,1,24,1,0,0,1,1,0
--)


------------------------------

--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,tintROw,tintColumn,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
--)
--VALUES
--(
--	3,255,141,0,0,'Shipped','Label',25,1,25,1,0,0,1,1,0
--)

--------------------------


--DECLARE @numFieldID INT = (SELECT TOP 1 numFieldId FROM DycFieldMaster WHERE vcLookBackTableName='OpportunityItems' AND vcOrigDbColumnName='vcInvoiced')

--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,tintROw,tintColumn,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
--)
--VALUES
--(
--	3,@numFieldID,141,0,0,'Invoiced','Label',26,1,26,1,0,0,1,1,0
--)

--------------------------


--DECLARE @numFieldID INT = (SELECT TOP 1 numFieldId FROM DycFieldMaster WHERE vcLookBackTableName='OpportunityMaster' AND vcOrigDbColumnName='bitPaidInFull')

--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,tintROw,tintColumn,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
--)
--VALUES
--(
--	3,@numFieldID,141,0,0,'Paid','Label',27,1,27,1,0,0,1,1,0
--)

--------------------------

--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,tintROw,tintColumn,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
--)
--VALUES
--(
--	3,258,141,0,0,'Ordered','TextBox',28,1,28,1,0,0,1,1,0
--)

----------------------------

--DECLARE @numFieldID NUMERIC(18,0)

--INSERT INTO DycFieldMaster
--(
--	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering
--)
--VALUES
--(
--	3,'Payment Status','vcPaymentStatus','vcPaymentStatus','PaymentStatus','OpportunityMaster','V','R','Label',0,1,0,0,0,1,0,1,1,0
--)


--SET @numFieldID = SCOPE_IDENTITY()

--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,tintROw,tintColumn,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
--)
--VALUES
--(
--	3,@numFieldID,141,0,0,'Payment Status','Label',29,1,29,1,0,0,1,0,0
--)

--------------------------


--DECLARE @numFieldID INT = (SELECT TOP 1 numFieldId FROM DycFieldMaster WHERE vcLookBackTableName='OpportunityMaster' AND vcOrigDbColumnName='intUsedShippingCompany')

--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,tintROw,tintColumn,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
--)
--VALUES
--(
--	3,@numFieldID,141,0,0,'Ship Via, Service','Label',30,1,30,1,0,0,1,0,0
--)

--------------------------


--DECLARE @numFieldID INT = (SELECT TOP 1 numFieldId FROM DycFieldMaster WHERE vcLookBackTableName='DivisionMaster' AND vcOrigDbColumnName='intShippingCompany')

--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,tintROw,tintColumn,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
--)
--VALUES
--(
--	3,@numFieldID,141,0,0,'Prefered Ship Via, Service','Label',31,1,31,1,0,0,1,0,0
--)


------------------------------------------


--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,tintROw,tintColumn,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
--)
--VALUES
--(
--	3,119,141,0,0,'Created','DateField',32,1,32,1,0,0,1,1,1
--)


---------------------------------------------

--DECLARE @numFieldID NUMERIC(18,0)

--INSERT INTO DycFieldMaster
--(
--	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering
--)
--VALUES
--(
--	3,'Ship Rate','numShipRate','numShipRate','ShipRate','OpportunityMaster','V','R','Label',0,1,0,0,0,1,0,0,0,0
--)


--SET @numFieldID = SCOPE_IDENTITY()

--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,tintROw,tintColumn,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
--)
--VALUES
--(
--	3,@numFieldID,141,0,0,'Ship Rate','Label',33,1,33,1,0,0,1,0,0
--)

--------------------------------


--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,tintROw,tintColumn,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
--)
--VALUES
--(
--	3,248,141,0,0,'Invoice','Label',34,1,34,1,0,0,1,1,1
--)


--------------------------------

--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,tintROw,tintColumn,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
--)
--VALUES
--(
--	4,351,142,0,0,'Image','TextBox',1,1,1,1,0,1,1,0,0
--)
---------------------


--DECLARE @numFieldID INT = (SELECT TOP 1 numFieldId FROM DycFieldMaster WHERE vcLookBackTableName='OpportunityItems' AND vcOrigDbColumnName='vcInclusionDetails')

--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,tintROw,tintColumn,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
--)
--VALUES
--(
--	3,@numFieldID,142,0,0,'Inclusion Details','Label',2,1,2,1,0,0,1,0,0
--)

------------------------

--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,tintROw,tintColumn,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
--)
--VALUES
--(
--	4,251,142,0,0,'Item Name','TextBox',3,1,3,1,0,1,1,1,1
--),
--(
--	3,96,142,0,0,'Order Name','TextBox',4,1,4,1,0,0,1,1,1
--),
--(
--	3,253,142,0,0,'Description','TextBox',5,1,5,1,0,0,1,0,0
--),
--(
--	3,350,142,0,0,'Attributes','Label',6,1,6,1,0,0,1,0,0
--),
--(
--	3,281,142,0,0,'SKU','Label',7,1,7,1,0,1,1,1,1
--),
--(
--	3,203,142,0,0,'UPC','Label',8,1,8,1,0,0,1,1,1
--),
--(
--	3,233,142,0,0,'Location','Label',9,1,9,1,0,0,1,1,1
--)


--------------------------


--DECLARE @numFieldID INT = (SELECT TOP 1 numFieldId FROM DycFieldMaster WHERE vcLookBackTableName='OpportunityItems' AND vcOrigDbColumnName='numRemainingQty')

--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,tintROw,tintColumn,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering,bitRequired
--)
--VALUES
--(
--	3,@numFieldID,142,0,0,'Remaining','Label',10,1,10,1,0,1,1,1,0,1
--)

--------------------------


--DECLARE @numFieldID INT = (SELECT TOP 1 numFieldId FROM DycFieldMaster WHERE vcLookBackTableName='OpportunityItems' AND vcOrigDbColumnName='numQtyPicked')

--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,tintROw,tintColumn,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering,bitRequired
--)
--VALUES
--(
--	3,@numFieldID,142,0,0,'Picked','Label',11,1,11,1,0,1,1,1,0,1
--)


---------------------------------------------------------------

--UPDATE DycFormField_Mapping SET bitAllowFiltering=0 WHERE numFormID=141 AND numFieldID=273

--------------------------

--USE [Production.2014]
--GO

--/****** Object:  Table [dbo].[MassSalesFulfillmentConfiguration]    Script Date: 03-Jun-19 9:10:43 AM ******/
--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

--CREATE TABLE [dbo].[MassSalesFulfillmentConfiguration](
--	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
--	[numDomainID] [numeric](18, 0) NOT NULL,
--	[numUserCntID] [numeric](18, 0) NOT NULL,
--	[bitGroupByOrderForPick] [bit] NOT NULL,
--	[bitGroupByOrderForShip] [bit] NOT NULL,
--	[bitGroupByOrderForInvoice] [bit] NOT NULL,
--	[bitGroupByOrderForPay] [bit] NOT NULL,
--	[bitGroupByOrderForClose] [bit] NOT NULL,
--	[tintInvoicingType] [tinyint] NOT NULL,
--	[tintScanValue] [tinyint] NOT NULL,
--	[tintPackingMode] [tinyint] NOT NULL,
-- CONSTRAINT [PK_MassSalesFulfillmentConfiguration] PRIMARY KEY CLUSTERED 
--(
--	[ID] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
--) ON [PRIMARY]

--GO

------------------------------------------------

--USE [Production.2014]
--GO

--/****** Object:  Table [dbo].[MassSalesFulfillmentBatch]    Script Date: 13-Jul-19 5:31:13 PM ******/
--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

--SET ANSI_PADDING ON
--GO

--CREATE TABLE [dbo].[MassSalesFulfillmentBatch](
--	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
--	[numDomainID] [numeric](18, 0) NOT NULL,
--	[numCreatedBy] [numeric](18, 0) NOT NULL,
--	[dtCreatedDate] [datetime] NOT NULL,
--	[vcName] [varchar](100) NOT NULL,
--	[bitEnabled] [bit] NOT NULL CONSTRAINT [DF_MassSalesFulfillmentBatch_bitEnabled]  DEFAULT ((1)),
-- CONSTRAINT [PK_MassSalesFulfillmentBatch] PRIMARY KEY CLUSTERED 
--(
--	[ID] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
--) ON [PRIMARY]

--GO

--SET ANSI_PADDING ON
--GO

-------------------------------------------------------

--USE [Production.2014]
--GO

--/****** Object:  Table [dbo].[MassSalesFulfillmentBatchOrders]    Script Date: 13-Jul-19 5:32:37 PM ******/
--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

--CREATE TABLE [dbo].[MassSalesFulfillmentBatchOrders](
--	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
--	[numBatchID] [numeric](18, 0) NOT NULL,
--	[numOppID] [numeric](18, 0) NOT NULL,
--	[numOppItemID] [numeric](18, 0) NOT NULL,
-- CONSTRAINT [PK_MassSalesFulfillmentBatchOrders] PRIMARY KEY CLUSTERED 
--(
--	[ID] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
--) ON [PRIMARY]

--GO

-------------------------------------
---- Below is commission changes
--------------------------------------
--UPDATE UserMaster SET bitPayroll = 1
--UPDATE PageMaster SET vcPageDesc='Payroll Expenses' WHERE vcPageDesc = 'Payroll Expences'

--ALTER TABLE Domain ADD bitCommissionBasedOn BIT DEFAULT 0
--ALTER TABLE Domain ADD tintCommissionBasedOn TINYINT DEFAULT 1

--ALTER TABLE BizDocComission ADD numOppItemID NUMERIC(18,0)
--ALTER TABLE BizDocComission ADD monInvoiceSubTotal DECIMAL(20,5)
--ALTER TABLE BizDocComission ADD monOrderSubTotal DECIMAL(20,5)

--ALTER TABLE PayrollHeader ADD numComPayPeriodID NUMERIC(18,0)

--ALTER TABLE PayrollDetail ADD monHourlyAmt DECIMAL(20,5)
--ALTER TABLE PayrollDetail ADD monAdditionalAmt DECIMAL(20,5)
--ALTER TABLE PayrollDetail ADD monSalesReturn DECIMAL(20,5)
--ALTER TABLE PayrollDetail ADD monOverPayment DECIMAL(20,5)
--ALTER TABLE PayrollDetail ADD numDivisionID NUMERIC(18,0)

--ALTER TABLE BizDocComission ADD monCommissionPaid DECIMAL(20,5) DEFAULT 0
--ALTER TABLE BizDocComission ADD bitNewImplementation BIT DEFAULT 1

--ALTER TABLE BizDocComission ADD bitDomainCommissionBasedOn BIT
--ALTER TABLE BizDocComission ADD tintDomainCommissionBasedOn TINYINT

--UPDATE
--	BDC
--SET
--	BDC.numOppItemID = OpportunityBizDocItems.numOppItemID
--FROM
--	BizDocComission BDC
--INNER JOIN
--	OpportunityBizDocItems 
--ON
--	BDC.numOppBizDocItemID = OpportunityBizDocItems.numOppBizDocItemID

-----------------------------------

--USE [Production.2014]
--GO

--/****** Object:  Table [dbo].[TimeAndExpenseCommission]    Script Date: 20-May-19 12:19:10 PM ******/
--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

--CREATE TABLE [dbo].[TimeAndExpenseCommission](
--	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
--	[numDomainID] [numeric](18, 0) NOT NULL,
--	[numComPayPeriodID] [numeric](18, 0) NOT NULL,
--	[numTimeAndExpenseID] [numeric](18, 0) NOT NULL,
--	[numUserCntID] [numeric](18, 0) NOT NULL,
--	[dtFromDate] [datetime] NOT NULL,
--	[dtToDate] [datetime] NOT NULL,
--	[numType] [tinyint] NOT NULL,
--	[numCategory] [tinyint] NOT NULL,
--	[bitReimburse] [bit] NOT NULL,
--	[numHours] [float] NOT NULL,
--	[monHourlyRate] [decimal](20, 5) NOT NULL,
--	[monTotalAmount] [decimal](20, 5) NOT NULL,
--	[bitCommissionPaid] [bit] NOT NULL,
--	[monAmountPaid] [decimal](20, 5) NULL CONSTRAINT [DF__TimeAndEx__monAm__391F47BF]  DEFAULT ((0)),
-- CONSTRAINT [PK_TimeAndExpenseCommission] PRIMARY KEY CLUSTERED 
--(
--	[ID] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
--) ON [PRIMARY]

--GO

-----------------------------

--USE [Production.2014]
--GO

--/****** Object:  Table [dbo].[StagePercentageDetailsTaskCommission]    Script Date: 20-May-19 12:19:32 PM ******/
--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

--CREATE TABLE [dbo].[StagePercentageDetailsTaskCommission](
--	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
--	[numDomainID] [numeric](18, 0) NOT NULL,
--	[numComPayPeriodID] [numeric](18, 0) NOT NULL,
--	[numTaskID] [numeric](18, 0) NOT NULL,
--	[numUserCntID] [numeric](18, 0) NOT NULL,
--	[numHours] [float] NOT NULL,
--	[monHourlyRate] [decimal](20, 5) NOT NULL,
--	[monTotalAmount] [decimal](20, 5) NOT NULL,
--	[bitCommissionPaid] [bit] NOT NULL,
--	[monAmountPaid] [decimal](20, 5) NOT NULL,
-- CONSTRAINT [PK_StagePercentageDetailsTaskCommission] PRIMARY KEY CLUSTERED 
--(
--	[ID] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
--) ON [PRIMARY]

--GO

-----------------------------------

--USE [Production.2014]
--GO

--/****** Object:  Table [dbo].[SalesReturnCommission]    Script Date: 20-May-19 12:19:55 PM ******/
--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

--CREATE TABLE [dbo].[SalesReturnCommission](
--	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
--	[numDomainID] [numeric](18, 0) NOT NULL,
--	[numComPayPeriodID] [numeric](18, 0) NOT NULL,
--	[numUserCntID] [numeric](18, 0) NOT NULL,
--	[tintAssignTo] [tinyint] NOT NULL,
--	[numReturnHeaderID] [numeric](18, 0) NOT NULL,
--	[numReturnItemID] [numeric](18, 0) NOT NULL,
--	[tintComType] [tinyint] NOT NULL,
--	[tintComBasedOn] [tinyint] NOT NULL,
--	[decCommission] [decimal](18, 2) NOT NULL,
--	[monCommissionReversed] [decimal](20, 5) NOT NULL,
--	[bitCommissionReversed] [bit] NOT NULL,
-- CONSTRAINT [PK_SalesReturnCommission] PRIMARY KEY CLUSTERED 
--(
--	[ID] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
--) ON [PRIMARY]

--GO

-----------------------------------------------------------

--USE [Production.2014]
--GO

--/****** Object:  Table [dbo].[BizDocComissionPaymentDifference]    Script Date: 13-Jul-19 12:48:34 PM ******/
--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

--SET ANSI_PADDING ON
--GO

--CREATE TABLE [dbo].[BizDocComissionPaymentDifference](
--	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
--	[numComissionID] [numeric](18, 0) NOT NULL,
--	[tintReason] [tinyint] NOT NULL,
--	[vcReason] [varchar](100) NOT NULL,
--	[monDifference] [decimal](20, 5) NOT NULL,
--	[bitDifferencePaid] [bit] NOT NULL,
--	[numComPayPeriodID] [numeric](18, 0) NULL,
-- CONSTRAINT [PK_BizDocComissionPaymentDifference] PRIMARY KEY CLUSTERED 
--(
--	[ID] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
--) ON [PRIMARY]

--GO

--SET ANSI_PADDING ON
--GO

-------------------------------------------------------------------------------------------------------------------------
------------------------------------------ PLECE NEW SCRIPTS ABOVE IT ---------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------

--ALTER TABLE PromotionOffer ADD monDiscountedItemPrice DECIMAL(20,5)

--ALTER TABLE Domain ADD bitIncludeRequisitions BIT

--INSERT INTO ErrorMaster
--(
--	vcErrorCode,vcErrorDesc,tintApplication
--)
--VALUES
--(
--	'ERR081','Contact name is required.',3
--)

-------------------------------------------------------------------------------------------------------------------------
------------------------------------------ PLECE NEW SCRIPTS ABOVE IT ---------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------

--ALTER TABLE CartItems ADD bitPromotionDiscount BIT DEFAULT 0

-------------------------------------------------------------------------------------------------------------------------
------------------------------------------ PLECE NEW SCRIPTS ABOVE IT ---------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------

--ALTER TABLE OpportunityBizDocs ADD numARAccountID NUMERIC(18,0)
--ALTER TABLE ReturnHeader ADD numOppBizDocID NUMERIC(18,0)

-------------------------------------------------------------------------------------------------------------------------
------------------------------------------ PLECE NEW SCRIPTS ABOVE IT ---------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------

--ALTER TABLE eCommerceDTL ADD bitShowPromoDetailsLink BIT DEFAULT 1

--------------------

--UPDATE eCommerceDTL SET bitShowPromoDetailsLink=1

-----------------------

--INSERT INTO PageElementMaster
--(
--	numElementID,vcElementName,vcUserControlPath,vcTagName,bitCustomization,bitAdd,bitDelete,numSiteID
--)
--VALUES
--(
--	61,'ItemPromotionPopup','~/UserControls/ItemPromotionPopup.ascx','{#ItemPromotionPopup#}',1,0,0,0
--)

--INSERT INTO PageElementAttributes
--(
--	numElementID,vcAttributeName,vcControlType,bitEditor
--)
--VALUES
--(
--	61,'Html Customize','HtmlEditor',1
--)

--------------------------

--ALTER TABLE CartItems ADD numPreferredPromotionID NUMERIC(18,0) DEFAULT 0
--ALTER TABLE CartItems ADD bitPromotionTrigerred BIT DEFAULT 0
--ALTER TABLE CartItems ADD monInsertPrice DECIMAL(20,5)
--ALTER TABLE CartItems ADD fltInsertDiscount FLOAT
--ALTER TABLE CartItems ADD bitInsertDiscountType BIT

----------------------------------

--DROP PROCEDURE USP_ManageECommercePromotion

--------------------------------

----POWERSHELL SCRIPT TO COPY NEW IMAGE ADDED TO BIZCART PROJECT TO ALL CURRENT WEBSITE FOLDER. CHANGE PATH ON PRODUCTION
--Get-ChildItem | where-object {$_.Psiscontainer -eq "True"} | ForEach-Object { If ((Test-Path (Join-Path -Path $_.FullName -ChildPath 'Images'))) { Copy-Item -Path "D:\Biz2016\Vss\BizCart\Default\images\item-promotion.png" -Destination "$($_.FullName)\Images" }}


--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitImport,bitExport,bitAllowFiltering,intSectionID
--)
--VALUES
--(
--	1,533,133,0,0,'Is Primary Address','CheckBox','IsPrimaryAddress',1,1,0,0,1,1,1,0,1
--)
-------------------------------------------------------------------------------------------------------------------------
------------------------------------------ PLECE NEW SCRIPTS ABOVE IT ---------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------

--ALTER TABLE PromotionOffer ADD bitError BIT
--ALTER TABLE PromotionOffer ADD bitUseForCouponManagement BIT
--ALTER TABLE PromotionOffer ADD bitUseOrderPromotion BIT
--ALTER TABLE PromotionOffer ADD numOrderPromotionID NUMERIC(18,0)

-------------------------------------------------------------------------------------------------------------------------
------------------------------------------ PLECE NEW SCRIPTS ABOVE IT ---------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------

--ALTER TABLE PromotionOffer ADD vcShortDesc VARCHAR(300)
--ALTER TABLE PromotionOffer ADD vcLongDesc VARCHAR(MAX)
--ALTER TABLE PromotionOffer ADD tintItemCalDiscount TINYINT

--SET IDENTITY_INSERT ShippingFields ON

--INSERT INTO ShippingFields
--(
--	intShipFieldID
--	,vcFieldName
--	,numListItemID
--	,vcToolTip
--)
--VALUES
--(
--	29,
--	'Shipping Label Type',
--	90,
--	'Generate shipping label as GIF or ZPL'
--)

--SET IDENTITY_INSERT ShippingFields OFF

--INSERT INTO DycFormField_Mapping 
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcPropertyName,vcAssociatedControlType,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering
--)
--VALUES
--(
--	3,217,38,0,0,'Ship-to State','ShipStateName','Label',1,0,0,1,0,1,1,0
--),
--(
--	3,217,39,0,0,'Ship-to State','ShipStateName','Label',1,0,0,1,0,1,1,0
--),
--(
--	3,217,40,0,0,'Ship-to State','ShipStateName','Label',1,0,0,1,0,1,1,0
--),
--(
--	3,217,41,0,0,'Ship-to State','ShipStateName','Label',1,0,0,1,0,1,1,0
--)

-------------------------------------------------------------------------------------------------------------------------
------------------------------------------ PLECE NEW SCRIPTS ABOVE IT ---------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------


--INSERT INTO ReportListMaster
--(
--	vcReportName,vcReportDescription,numDomainID,tintReportType,bitDefault,intDefaultReportID
--)
--VALUES
--(
--	'Sales Opportunity Won/Lost Report','Sales Opportunity Won/Lost Report',0,1,1,34
--),
--(
--	'Sales Opportunity Pipeline Report','Sales Opportunity Pipeline Report',0,1,1,35
--)

--ALTER TABLE ReportDashboard ADD
--vcTimeLine VARCHAR(50)
--,vcGroupBy VARCHAR(50)
--,vcTeritorry VARCHAR(MAX)
--,vcFilterBy TINYINT
--,vcFilterValue VARCHAR(MAX)
--,dtFromDate DATE
--,dtToDate DATE


--ALTER TABLE OpportunityMaster ADD dtDealLost DATETIME


--DECLARE @numFieldID NUMERIC(18,0)

--SELECT @numFieldID=numFieldId FROM DycFieldMaster WHERE vcDbColumnName = 'vcItemReleaseDate'


--INSERT INTO ReportFieldGroupMappingMaster
--(
--	numReportFieldGroupID
--	,numFieldID
--	,vcFieldName
--	,vcAssociatedControlType
--	,vcFieldDataType
--	,bitAllowSorting
--	,bitAllowGrouping
--	,bitAllowAggregate
--	,bitAllowFiltering
--)
--VALUES
--(
--	4
--	,@numFieldID
--	,'Item Release Date'
--	,'DateField'
--	,'D'
--	,1
--	,1
--	,0
--	,1
--)

--------------------------------------------------

--USE [Production.2014]
--GO

--/****** Object:  Table [dbo].[ShippingPackageType]    Script Date: 02-Feb-19 2:35:06 PM ******/
--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

--SET ANSI_PADDING ON
--GO

--CREATE TABLE [dbo].[ShippingPackageType](
--	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
--	[numShippingCompanyID] [numeric](18, 0) NOT NULL,
--	[numNsoftwarePackageTypeID] [numeric](18, 0) NOT NULL,
--	[vcPackageName] [varchar](100) NOT NULL,
--	[bitEndicia] [bit] NOT NULL,
-- CONSTRAINT [PK_ShippingPackageType] PRIMARY KEY CLUSTERED 
--(
--	[ID] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
--) ON [PRIMARY]

--GO

--SET ANSI_PADDING ON
--GO


--------------------------------------------------

--INSERT INTO ShippingPackageType (numShippingCompanyID,vcPackageName,numNsoftwarePackageTypeID,bitEndicia) VALUES (91,'FedEx Letter',2,0)
--INSERT INTO ShippingPackageType (numShippingCompanyID,vcPackageName,numNsoftwarePackageTypeID,bitEndicia) VALUES (91,'FedEx Pak',11,0)
--INSERT INTO ShippingPackageType (numShippingCompanyID,vcPackageName,numNsoftwarePackageTypeID,bitEndicia) VALUES (91,'FedEx Box',12,0)
--INSERT INTO ShippingPackageType (numShippingCompanyID,vcPackageName,numNsoftwarePackageTypeID,bitEndicia) VALUES (91,'FedEx 10kg Box',13,0)
--INSERT INTO ShippingPackageType (numShippingCompanyID,vcPackageName,numNsoftwarePackageTypeID,bitEndicia) VALUES (91,'FedEx 25kg Box',14,0)
--INSERT INTO ShippingPackageType (numShippingCompanyID,vcPackageName,numNsoftwarePackageTypeID,bitEndicia) VALUES (91,'FedEx Tube',28,0)
--INSERT INTO ShippingPackageType (numShippingCompanyID,vcPackageName,numNsoftwarePackageTypeID,bitEndicia) VALUES (91,'Your Packaging',31,0)

--INSERT INTO ShippingPackageType (numShippingCompanyID,vcPackageName,numNsoftwarePackageTypeID,bitEndicia) VALUES (88,'None',0,0)
--INSERT INTO ShippingPackageType (numShippingCompanyID,vcPackageName,numNsoftwarePackageTypeID,bitEndicia) VALUES (88,'UPS letter',2,0)
--INSERT INTO ShippingPackageType (numShippingCompanyID,vcPackageName,numNsoftwarePackageTypeID,bitEndicia) VALUES (88,'UPS Pak',11,0)
--INSERT INTO ShippingPackageType (numShippingCompanyID,vcPackageName,numNsoftwarePackageTypeID,bitEndicia) VALUES (88,'UPS Box',12,0)
--INSERT INTO ShippingPackageType (numShippingCompanyID,vcPackageName,numNsoftwarePackageTypeID,bitEndicia) VALUES (88,'UPS 10kg Box',13,0)
--INSERT INTO ShippingPackageType (numShippingCompanyID,vcPackageName,numNsoftwarePackageTypeID,bitEndicia) VALUES (88,'UPS 25kg Box',14,0)
--INSERT INTO ShippingPackageType (numShippingCompanyID,vcPackageName,numNsoftwarePackageTypeID,bitEndicia) VALUES (88,'UPS Small Express Box',15,0)
--INSERT INTO ShippingPackageType (numShippingCompanyID,vcPackageName,numNsoftwarePackageTypeID,bitEndicia) VALUES (88,'UPS Medium Express Box',16,0)
--INSERT INTO ShippingPackageType (numShippingCompanyID,vcPackageName,numNsoftwarePackageTypeID,bitEndicia) VALUES (88,'UPS Large Express Box',17,0)
--INSERT INTO ShippingPackageType (numShippingCompanyID,vcPackageName,numNsoftwarePackageTypeID,bitEndicia) VALUES (88,'UPS Tube',28,0)
--INSERT INTO ShippingPackageType (numShippingCompanyID,vcPackageName,numNsoftwarePackageTypeID,bitEndicia) VALUES (88,'UPS Pallet',30,0)
--INSERT INTO ShippingPackageType (numShippingCompanyID,vcPackageName,numNsoftwarePackageTypeID,bitEndicia) VALUES (88,'Your Packaging',31,0)
--INSERT INTO ShippingPackageType (numShippingCompanyID,vcPackageName,numNsoftwarePackageTypeID,bitEndicia) VALUES (88,'UPS Flats',33,0)
--INSERT INTO ShippingPackageType (numShippingCompanyID,vcPackageName,numNsoftwarePackageTypeID,bitEndicia) VALUES (88,'UPS Parcels',34,0)
--INSERT INTO ShippingPackageType (numShippingCompanyID,vcPackageName,numNsoftwarePackageTypeID,bitEndicia) VALUES (88,'UPS BPM',35,0)
--INSERT INTO ShippingPackageType (numShippingCompanyID,vcPackageName,numNsoftwarePackageTypeID,bitEndicia) VALUES (88,'UPS First Class',36,0)
--INSERT INTO ShippingPackageType (numShippingCompanyID,vcPackageName,numNsoftwarePackageTypeID,bitEndicia) VALUES (88,'UPS Priority',37,0)
--INSERT INTO ShippingPackageType (numShippingCompanyID,vcPackageName,numNsoftwarePackageTypeID,bitEndicia) VALUES (88,'UPS Machinables',38,0)
--INSERT INTO ShippingPackageType (numShippingCompanyID,vcPackageName,numNsoftwarePackageTypeID,bitEndicia) VALUES (88,'UPS Irregulars',39,0)
--INSERT INTO ShippingPackageType (numShippingCompanyID,vcPackageName,numNsoftwarePackageTypeID,bitEndicia) VALUES (88,'UPS Parcel Post',40,0)
--INSERT INTO ShippingPackageType (numShippingCompanyID,vcPackageName,numNsoftwarePackageTypeID,bitEndicia) VALUES (88,'UPS BPM Parcel',41,0)
--INSERT INTO ShippingPackageType (numShippingCompanyID,vcPackageName,numNsoftwarePackageTypeID,bitEndicia) VALUES (88,'UPS Media Mail',42,0)
--INSERT INTO ShippingPackageType (numShippingCompanyID,vcPackageName,numNsoftwarePackageTypeID,bitEndicia) VALUES (88,'UPS BPM Flat',43,0)
--INSERT INTO ShippingPackageType (numShippingCompanyID,vcPackageName,numNsoftwarePackageTypeID,bitEndicia) VALUES (88,'UPS Standard Flat',44,0)

--INSERT INTO ShippingPackageType (numShippingCompanyID,vcPackageName,numNsoftwarePackageTypeID,bitEndicia) VALUES (90,'None',0,0)
--INSERT INTO ShippingPackageType (numShippingCompanyID,vcPackageName,numNsoftwarePackageTypeID,bitEndicia) VALUES (90,'USPS Postcards',1,0)
--INSERT INTO ShippingPackageType (numShippingCompanyID,vcPackageName,numNsoftwarePackageTypeID,bitEndicia) VALUES (90,'USPS Large Envelope',3,0)
--INSERT INTO ShippingPackageType (numShippingCompanyID,vcPackageName,numNsoftwarePackageTypeID,bitEndicia) VALUES (90,'USPS Flat Rate Envelope',4,0)
--INSERT INTO ShippingPackageType (numShippingCompanyID,vcPackageName,numNsoftwarePackageTypeID,bitEndicia) VALUES (90,'USPS Flat Rate Legal Envelope',5,0)
--INSERT INTO ShippingPackageType (numShippingCompanyID,vcPackageName,numNsoftwarePackageTypeID,bitEndicia) VALUES (90,'USPS Flat Rate Padded Envelope',6,0)
--INSERT INTO ShippingPackageType (numShippingCompanyID,vcPackageName,numNsoftwarePackageTypeID,bitEndicia) VALUES (90,'USPS Flat Rate GiftCard Envelope',7,0)
--INSERT INTO ShippingPackageType (numShippingCompanyID,vcPackageName,numNsoftwarePackageTypeID,bitEndicia) VALUES (90,'USPS Flat Rate Window Envelope',8,0)
--INSERT INTO ShippingPackageType (numShippingCompanyID,vcPackageName,numNsoftwarePackageTypeID,bitEndicia) VALUES (90,'USPS Small Flat Rate Envelope',10,0)
--INSERT INTO ShippingPackageType (numShippingCompanyID,vcPackageName,numNsoftwarePackageTypeID,bitEndicia) VALUES (90,'USPS Flat Rate Box',18,0)
--INSERT INTO ShippingPackageType (numShippingCompanyID,vcPackageName,numNsoftwarePackageTypeID,bitEndicia) VALUES (90,'USPS Small Flat Rate Box',19,0)
--INSERT INTO ShippingPackageType (numShippingCompanyID,vcPackageName,numNsoftwarePackageTypeID,bitEndicia) VALUES (90,'USPS Medium Flat Rate Box',20,0)
--INSERT INTO ShippingPackageType (numShippingCompanyID,vcPackageName,numNsoftwarePackageTypeID,bitEndicia) VALUES (90,'USPS Large Flat Rate Box',21,0)
--INSERT INTO ShippingPackageType (numShippingCompanyID,vcPackageName,numNsoftwarePackageTypeID,bitEndicia) VALUES (90,'USPS Regional Rate Box A',24,0)
--INSERT INTO ShippingPackageType (numShippingCompanyID,vcPackageName,numNsoftwarePackageTypeID,bitEndicia) VALUES (90,'USPS Regional Rate Box B',25,0)
--INSERT INTO ShippingPackageType (numShippingCompanyID,vcPackageName,numNsoftwarePackageTypeID,bitEndicia) VALUES (90,'USPS Rectangular',26,0)
--INSERT INTO ShippingPackageType (numShippingCompanyID,vcPackageName,numNsoftwarePackageTypeID,bitEndicia) VALUES (90,'USPS Non Rectangular',27,0)
--INSERT INTO ShippingPackageType (numShippingCompanyID,vcPackageName,numNsoftwarePackageTypeID,bitEndicia) VALUES (90,'USPS Matter For The Blind',29,0)
--INSERT INTO ShippingPackageType (numShippingCompanyID,vcPackageName,numNsoftwarePackageTypeID,bitEndicia) VALUES (90,'Your Packaging',31,0)
--INSERT INTO ShippingPackageType (numShippingCompanyID,vcPackageName,numNsoftwarePackageTypeID,bitEndicia) VALUES (90,'USPS Regional Rate Box C',45,0)

--INSERT INTO ShippingPackageType (numShippingCompanyID,vcPackageName,numNsoftwarePackageTypeID,bitEndicia) VALUES (90,'USPS Postcards',1,1)
--INSERT INTO ShippingPackageType (numShippingCompanyID,vcPackageName,numNsoftwarePackageTypeID,bitEndicia) VALUES (90,'USPS Letter',2,1)
--INSERT INTO ShippingPackageType (numShippingCompanyID,vcPackageName,numNsoftwarePackageTypeID,bitEndicia) VALUES (90,'USPS Large Envelope',3,1)
--INSERT INTO ShippingPackageType (numShippingCompanyID,vcPackageName,numNsoftwarePackageTypeID,bitEndicia) VALUES (90,'USPS Flat Rate Envelope',4,1)
--INSERT INTO ShippingPackageType (numShippingCompanyID,vcPackageName,numNsoftwarePackageTypeID,bitEndicia) VALUES (90,'USPS Flat Rate Legal Envelope',5,1)
--INSERT INTO ShippingPackageType (numShippingCompanyID,vcPackageName,numNsoftwarePackageTypeID,bitEndicia) VALUES (90,'USPS Flat Rate Padded Envelope',6,1)
--INSERT INTO ShippingPackageType (numShippingCompanyID,vcPackageName,numNsoftwarePackageTypeID,bitEndicia) VALUES (90,'USPS Flat Rate GiftCard Envelope',7,1)
--INSERT INTO ShippingPackageType (numShippingCompanyID,vcPackageName,numNsoftwarePackageTypeID,bitEndicia) VALUES (90,'USPS Flat Rate Window Envelope',8,1)
--INSERT INTO ShippingPackageType (numShippingCompanyID,vcPackageName,numNsoftwarePackageTypeID,bitEndicia) VALUES (90,'USPS Flat Rate Cardboard Envelope',9,1)
--INSERT INTO ShippingPackageType (numShippingCompanyID,vcPackageName,numNsoftwarePackageTypeID,bitEndicia) VALUES (90,'USPS Small Flat Rate Envelope',10,1)
--INSERT INTO ShippingPackageType (numShippingCompanyID,vcPackageName,numNsoftwarePackageTypeID,bitEndicia) VALUES (90,'USPS Small Flat Rate Box',19,1)
--INSERT INTO ShippingPackageType (numShippingCompanyID,vcPackageName,numNsoftwarePackageTypeID,bitEndicia) VALUES (90,'USPS Medium Flat Rate Box',20,1)
--INSERT INTO ShippingPackageType (numShippingCompanyID,vcPackageName,numNsoftwarePackageTypeID,bitEndicia) VALUES (90,'USPS Large Flat Rate Box',21,1)
--INSERT INTO ShippingPackageType (numShippingCompanyID,vcPackageName,numNsoftwarePackageTypeID,bitEndicia) VALUES (90,'USPS DVD Flat Rate Box',22,1)
--INSERT INTO ShippingPackageType (numShippingCompanyID,vcPackageName,numNsoftwarePackageTypeID,bitEndicia) VALUES (90,'USPS Large Video Flat Rate Box',23,1)
--INSERT INTO ShippingPackageType (numShippingCompanyID,vcPackageName,numNsoftwarePackageTypeID,bitEndicia) VALUES (90,'USPS Regional Rate Box A',24,1)
--INSERT INTO ShippingPackageType (numShippingCompanyID,vcPackageName,numNsoftwarePackageTypeID,bitEndicia) VALUES (90,'USPS Regional Rate Box B',25,1)
--INSERT INTO ShippingPackageType (numShippingCompanyID,vcPackageName,numNsoftwarePackageTypeID,bitEndicia) VALUES (90,'Your Packaging',31,1)
--INSERT INTO ShippingPackageType (numShippingCompanyID,vcPackageName,numNsoftwarePackageTypeID,bitEndicia) VALUES (90,'USPS Regional Rate Box C',45,1)

-------------------------------------------------------------------------------------------------------------------------
------------------------------------------ PLECE NEW SCRIPTS ABOVE IT ---------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------

--INSERT INTO PageElementMaster
--(
--	numElementID,vcElementName,vcUserControlPath,vcTagName,bitCustomization,bitAdd,bitDelete,numSiteID
--)
--VALUES
--(
--	60,'OrderPromotion','~/UserControls/OrderPromotion.ascx','{#OrderPromotion#}',1,0,0,0
--)

--INSERT INTO PageElementAttributes
--(
--	numElementID,vcAttributeName,vcControlType,bitEditor
--)
--VALUES
--(
--	60,'Html Customize','HtmlEditor',1
--)

--UPDATE PromotionOffer SET dtValidFrom=NULL WHERE dtValidFrom='0001-01-01'
--UPDATE PromotionOffer SET dtValidTo=NULL WHERE dtValidTo='0001-01-01'
--ALTER TABLE PromotionOffer ALTER COLUMN [dtValidFrom] DATETIME
--ALTER TABLE PromotionOffer ALTER COLUMN [dtValidTo] DATETIME


-------------------------------------------------------------------------------------------------------------------------
------------------------------------------ PLECE NEW SCRIPTS ABOVE IT ---------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------

--INSERT INTO ReportFieldGroupMappingMaster
--(
--	numReportFieldGroupID
--	,numFieldID
--	,vcFieldName
--	,vcAssociatedControlType
--	,vcFieldDataType
--	,bitAllowSorting
--	,bitAllowGrouping
--	,bitAllowAggregate
--	,bitAllowFiltering
--)
--SELECT
--	7
--	,numFieldID
--	,vcFieldName
--	,vcAssociatedControlType
--	,vcFieldDataType
--	,bitAllowSorting
--	,bitAllowGrouping
--	,bitAllowAggregate
--	,bitAllowFiltering
--FROM
--	ReportFieldGroupMappingMaster
--WHERE
--	numReportFieldGroupID=11 AND numFieldID IN (195,196,197,198,200)


--INSERT INTO ReportFieldGroupMappingMaster
--(
--	numReportFieldGroupID
--	,numFieldID
--	,vcFieldName
--	,vcAssociatedControlType
--	,vcFieldDataType
--	,bitAllowSorting
--	,bitAllowGrouping
--	,bitAllowAggregate
--	,bitAllowFiltering
--)
--VALUES
--(
--	7
--	,900
--	,'Available'
--	,'TextBox'
--	,'M'
--	,1
--	,1
--	,1
--	,1
--)

-------------------------------------------------------------------------------------------------------------------------
------------------------------------------ PLECE NEW SCRIPTS ABOVE IT ---------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------

--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,numFormID
--)
--VALUES
--(
--	35,409,'Class','SelectBox','',1,1,1,140
--)

--UPDATE DycFormField_Mapping SET bitImport=1,intSectionID=1 WHERE numFormID=140 AND numFieldId=409


--DECLARE @numInclutionDetailFieldID INT
--SELECT @numInclutionDetailFieldID=numFieldId FROM DycFieldMaster WHERE vcOrigDbColumnName='vcInclusionDetails'
--UPDATE DycFieldMaster SET bitImport=1 WHERE numFieldID=@numInclutionDetailFieldID
--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,numFormID,intSectionID
--)
--VALUES
--(
--	3,@numInclutionDetailFieldID,'Inclusion Details','TextBox','vcInclusionDetails',1,1,1,136,1
--)

--ALTER TABLE Item ADD numExpenseChartAcntId NUMERIC(18,0)
--ALTER TABLE Item ADD bitExpenseItem BIT
-------------------------------------------------------------------------------------------------------------------------
------------------------------------------ PLECE NEW SCRIPTS ABOVE IT ---------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------
--UPDATE ReportFieldsList SET numFieldID=522 WHERE numFieldID=233
--UPDATE ReportFieldGroupMappingMaster SET numFieldID=522 WHERE numFieldID=233
--UPDATE ReportFilterList SET numFieldID=522 WHERE numFieldID=233


--INSERT INTO ErrorMaster
--(
--	vcErrorCode,vcErrorDesc,tintApplication
--)
--VALUES
--(
--	'ERR080','Not Available.',3
--)

--ALTER TABLE UserMaster ADD vcEmailAlias VARCHAR(100)
--ALTER TABLE UserMaster ADD vcEmailAliasPassword VARCHAR(100)

--USE [Production.2014]
--GO

--/****** Object:  Table [dbo].[UniversalSMTPIMAP]    Script Date: 04-Dec-18 1:17:56 PM ******/
--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

--SET ANSI_PADDING ON
--GO

--CREATE TABLE [dbo].[UniversalSMTPIMAP](
--	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
--	[numDomainID] [numeric](18, 0) NOT NULL,
--	[vcSMTPServer] [varchar](100) NOT NULL,
--	[numSMTPPort] [numeric](18, 0) NOT NULL,
--	[bitSMTPSSL] [bit] NOT NULL,
--	[bitSMTPAuth] [bit] NOT NULL,
--	[vcIMAPServer] [varchar](100) NOT NULL,
--	[numIMAPPort] [numeric](18, 0) NOT NULL,
--	[bitIMAPSSL] [bit] NOT NULL,
--	[bitIMAPAuth] [bit] NOT NULL,
-- CONSTRAINT [PK_UniversalSMTPIMAP] PRIMARY KEY CLUSTERED 
--(
--	[ID] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
--) ON [PRIMARY]

--GO

--SET ANSI_PADDING ON
--GO


--INSERT INTO ErrorMaster
--(
--	vcErrorCode,vcErrorDesc,tintApplication
--)
--VALUES
--(
--	'ERR079','We�re unable to generate a rate. Your option is to click the get rate from the check-out page, or contact us.',3
--)

----POWERSHELL SCRIPT TO COPY NEW IMAGE ADDED TO BIZCART PROJECT TO ALL CURRENT WEBSITE FOLDER. CHANGE PATH ON PRODUCTION
--Get-ChildItem | where-object {$_.Psiscontainer -eq "True"} | ForEach-Object { If ((Test-Path (Join-Path -Path $_.FullName -ChildPath 'Images'))) { Copy-Item -Path "D:\Biz2016\Vss\BizCart\Default\images\ShippingBox.png" -Destination "$($_.FullName)\Images" }}

-------------------------------------------------------------------------------------------------------------------------
------------------------------------------ PLECE NEW SCRIPTS ABOVE IT ---------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------

--INSERT INTO DynamicFormMaster
--(
--	numFormId,vcFormName,cCustomFieldsAssociated,cAOIAssociated,bitDeleted,tintFlag
--)
--VALUES
--(
--	140,'Import Accounting Entries','N','N',0,2
--)

---------------------------------------

--INSERT INTO DycFormSectionDetail
--(
--	numFormID,intSectionID,vcSectionName,Loc_id
--)
--VALUES
--(
--	140,1,'Accounting Fields',0
--)

-------------------------------------

--INSERT INTO ErrorMaster
--(
--	vcErrorCode,vcErrorDesc,tintApplication
--)
--VALUES
--(
--	'ERR077','Please select all required option(s).',3
--)

-------------------------------

--INSERT INTO ErrorMaster
--(
--	vcErrorCode,vcErrorDesc,tintApplication
--)
--VALUES
--(
--	'ERR078','Reset password is link is valid for 10 minutes only. Please try again.',3
--)

-------------------------------------------------

--ALTER TABLE CartItems ADD vcChildKitItemSelection VARCHAR(MAX)

--ALTER TABLE OpportunityItems ADD vcChildKitSelectedItems VARCHAR(MAX)
----------------------------------------------

--ALTER TABLE ExtranetAccountsDtl ADD
--vcResetLinkID VARCHAR(300)
--,vcResetLinkCreatedTime DATETIME
---------------------------------------------------

--INSERT INTO PageElementMaster
--(
--	numElementID,vcElementName,vcUserControlPath,vcTagName,bitCustomization,bitAdd,bitDelete,numSiteID
--)
--VALUES
--(
--	58,'ResetPassword','~/UserControls/ResetPassword.ascx','{#ResetPassword#}',1,0,0,0
--)

-------------------------------------------

--DECLARE @TEMP TABLE
--(
--	ID INT IDENTITY(1,1)
--	,numSiteID NUMERIC(18,0)
--	,numDomainID NUMERIC(18,0)
--)

--INSERT INTO @TEMP
--(
--	numSiteID
--	,numDomainID
--)
--SELECT
--	numSiteID
--	,numDomainID
--FROM
--	Sites

--DECLARE @i INT = 1
--DECLARE @iCount INT
--DECLARE @numSiteID NUMERIC(18,0)
--DECLARE @numDomainID NUMERIC(18,0)
--DECLARE @numTemplateID NUMERIC(18,0)

--SELECT @iCount = COUNT(*) FROM @TEMP

--WHILE @i <= @iCount
--BEGIN
--	SELECT @numSiteID=numSiteID,@numDomainID=numDomainID FROM @TEMP WHERE ID=@i

--	INSERT INTO SiteTemplates
--	(
--		vcTemplateName,numSiteID,numDomainID,dtCreateDate,dtModifiedDate,numCreatedBy,numModifiedBy,txtTemplateHTML
--	)
--	VALUES
--	(
--		'ResetPassword',@numSiteID,@numDomainID,GETUTCDATE(),GETUTCDATE(),0,0,'<title></title>
--<center>
--    <table cellpadding="0" cellspacing="3" class="MainPanel" border="0">
--        <tr>
--            <td valign="top">
--                <!--header starts-->
--                {template:header}
--                <!--header ends-->
--            </td>
--        </tr>
--        <tr>
--            <td valign="top">
--                <table width="100%">
--                    <tr>
--                        <td width="260px" valign="top">
--                            <!--left panel starts-->
--                            {template:leftpanel}
--                            <!--left panel ends-->
--                        </td>
--                        <td valign="top" style="padding-left: 15px;">
--                            <!--Content panel starts-->
--							{#Error#}<br>
--                            {#ResetPassword#}
--                            <!--Content panel ends-->
--                        </td>
--                    </tr>
--                </table>
--            </td>
--        </tr>
--        <tr>
--            <td>
--                <!--footer starts-->
--                {template:footer}
--                <!--footer ends-->
--            </td>
--        </tr>
--    </table>
--</center>'
--	)

--	SET @numTemplateID = SCOPE_IDENTITY()

--	INSERT INTO SitePages
--	(
--		vcPageName,vcPageURL,tintPageType,vcPageTitle,numTemplateID,numSiteID,numDomainID,dtCreateDate,dtModifiedDate,bitIsActive,bitIsMaintainScroll,numCreatedBy,numModifiedBy
--	)
--	VALUES
--	(
--		'ResetPassword','ResetPassword.aspx',1,'Reset Password',@numTemplateID,@numSiteID,@numDomainID,GETUTCDATE(),GETUTCDATE(),1,0,0,0
--	)

--	SET @i = @i + 1
--END

------------------------------------------------

--INSERT INTO PageElementAttributes
--(
--	numElementID,vcAttributeName,vcControlType,bitEditor
--)
--VALUES
--(
--	58,'Html Customize','HtmlEditor',1
--)
-----------------------------------------------------

--SET IDENTITY_INSERT DycFieldMaster ON

--INSERT INTO DycFieldMaster
--(
--	numFieldID,numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcListItemType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering
--)
--VALUES
--(
--	931,35,'Date (MM/dd/yyyy)','datEntry_Date','datEntry_Date','General_Journal_Header','V','R','DateField','',0,0,0,0,0,0,0,0,1,1
--),
--(
--	932,35,'Chart of Accounts to Credit','numChartAcntId','numChartAcntId','General_Journal_Details','N','R','SelectBox','COA',0,0,0,0,0,0,0,0,1,1
--),
--(
--	933,35,'Chart of Accounts to Debit','numChartAcntId','numChartAcntId','General_Journal_Details','N','R','SelectBox','COA',0,0,0,0,0,0,0,0,1,1
--),
--(
--	934,35,'Amount','numAmount','numAmount','General_Journal_Header','N','R','TextBox','',0,0,0,0,0,0,0,0,1,1
--),
--(
--	935,35,'Memo','varDescription','varDescription','General_Journal_Details','V','R','TextBox','',0,0,0,0,0,0,0,0,0,0
--)

--SET IDENTITY_INSERT DycFieldMaster OFF

--------------------------------------------------


--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,numFormID
--)
--VALUES
--(
--	35,931,'Date (MM/dd/yyyy)','DateField','',1,1,1,140
--),
--(
--	35,932,'Chart of Accounts to Credit','SelectBox','',1,1,1,140
--),
--(
--	35,933,'Chart of Accounts to Debit','SelectBox','',1,1,1,140
--),
--(
--	35,934,'Amount','TextBox','',1,1,1,140
--),
--(
--	35,3,'Vendor','TextBox','',1,1,1,140
--),
--(
--	35,935,'Memo','TextBox','',1,1,1,140
--)

--UPDATE DycFormField_Mapping SET bitImport=1,intSectionID=1 WHERE numFormID=140

--UPDATE DycFieldMaster SET vcAssociatedControlType='TextBox' WHERE numFieldId=934

-------------------------------------------------------------------------------------------------------------------------
------------------------------------------ PLECE NEW SCRIPTS ABOVE IT ---------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------

--ALTER TABLE WorkOrder ADD numOppChildItemID NUMERIC(18,0) 
--ALTER TABLE WorkOrder ADD numOppKitChildItemID NUMERIC(18,0) 

--ALTER TABLE Item ADD bitSOWorkOrder BIT DEFAULT 0
--ALTER TABLE Item ADD bitKitSingleSelect BIT DEFAULT 0

--UPDATE Item SET bitKitSingleSelect=0 WHERE bitKitParent=1
-------------------------------------------------------------------------------------------------------------------------
------------------------------------------ PLECE NEW SCRIPTS ABOVE IT ---------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------

--ALTER TABLE OpportunityBizDocItems ADD monAverageCost DECIMAL(20,5) DEFAULT NULL


--USE [Production.2014]
--GO

--/****** Object:  Table [dbo].[OpportunityBizDocKitItems]    Script Date: 02-Oct-18 4:43:53 PM ******/
--DROP TABLE [dbo].[OpportunityBizDocKitItems]
--GO

--/****** Object:  Table [dbo].[OpportunityBizDocKitItems]    Script Date: 02-Oct-18 4:43:53 PM ******/
--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

--CREATE TABLE [dbo].[OpportunityBizDocKitItems](
--	[numOppBizDocKitItemID] NUMERIC(18,0) NOT NULL IDENTITY(1,1) PRIMARY KEY,
--	[numOppBizDocID] NUMERIC(18,0) NOT NULL,
--	[numOppBizDocItemID] [numeric](18, 0) NOT NULL,
--	[numOppChildItemID] [numeric](18, 0) NOT NULL,
--	[numChildItemID] [numeric](18, 0) NOT NULL,
--	[monAverageCost] DECIMAL(20,5) NULL
--) ON [PRIMARY]

--GO

--------------------


--/****** Object:  Table [dbo].[OpportunityBizDocKitItems]    Script Date: 02-Oct-18 4:43:53 PM ******/
--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

--CREATE TABLE [dbo].[OpportunityBizDocKitChildItems](
--	[numOppBizDocKitChildItemID] NUMERIC(18,0) NOT NULL IDENTITY(1,1) PRIMARY KEY,
--	[numOppBizDocID] NUMERIC(18,0) NOT NULL,
--	[numOppBizDocItemID] [numeric](18, 0) NOT NULL,
--	[numOppBizDocKitItemID] NUMERIC(18,0) NOT NULL,
--	[numOppKitChildItemID] NUMERIC(18,0) NOT NULL,
--	[numChildItemID] [numeric](18, 0) NOT NULL,
--	[monAverageCost] DECIMAL(20,5) NULL
--) ON [PRIMARY]

--GO

--ALTER TABLE ReturnItems ADD numOppBizDocItemID NUMERIC(18,0)


--USP_OPPGetINItemsForAuthorizativeAccounting
--USP_OpportunityBizDocKitItemsForAuthorizativeAccounting
--USP_General_Journal_Details_CheckIfSalesClearingEntryExists
--USP_ProjectOpp
--USP_OpportunityBizDocs_GetItemsToReturn
--USP_ManageReturnHeaderItems
--USP_GetHeaderReturnDetails

--usp_ManageRMAInventory - Pending
--====================================================

--ALTER TABLE Item ADD tintKitAssemblyPriceBasedOn TINYINT
--ALTER TABLE Item Add fltReorderQty FLOAT

--UPDATE Item SET tintKitAssemblyPriceBasedOn=1 WHERE ISNULL(bitCalAmtBasedonDepItems,0)=1

--UPDATE ListDetails SET vcData='Build Completed' WHERE numListID=302 AND constFlag=1 AND vcData='Completed'


--UPDATE WorkOrder SET numWOStatus=0 WHERE numWOStatus IN (40722,40723)
--DELETE FROM ListDetails WHERE numListId=302 AND numListItemID IN (40722,40723)


--EXEC sp_rename 'ItemDetails.numWareHouseItemId', 'numWareHouseItemId1', 'COLUMN'

--BEGIN TRY
--BEGIN TRANSACTION

--	INSERT INTO PageMaster 
--	(
--		numPageID,numModuleID,vcFileName,vcPageDesc,bitIsViewApplicable,bitIsAddApplicable,bitIsUpdateApplicable,bitIsDeleteApplicable,bitIsExportApplicable
--	)
--	VALUES
--	(
--		35,10,'frmManageWorkOrder.aspx','Start Purchase Order(s) in Work Orders',1,0,0,0,0
--	)
--	SELECT 
--		* 
--	INTO 
--		#temp 
--	FROM
--	(
--		SELECT 
--			ROW_NUMBER() OVER(ORDER BY numDomainId DESC) AS Row, 
--			numDomainId 
--		FROM 
--			Domain 
--		WHERE 
--			numDomainId <> -255
--	) TABLE2
	
--	DECLARE @RowCount INT
--	SET @RowCount = (SELECT COUNT(Row) FROM #temp) 
	
--	DECLARE @I INT
--	DECLARE @numDomainId NUMERIC(18,0)
	
--	SET @I = 1

--	WHILE (@I <= @RowCount)
--	BEGIN
			
--		SELECT @numDomainId = numDomainID FROM #temp WHERE Row = @I
	
--		INSERT INTO dbo.GroupAuthorization
--			(numDomainID,numModuleID,numPageID,numGroupID,intExportAllowed,intPrintAllowed,intViewAllowed,intAddAllowed,intUpdateAllowed,intDeleteAllowed)
--		SELECT
--			@numDomainId,10,35,numGroupID,0,0,3,0,0,0
--		FROM
--			AuthenticationGroupMaster
--		WHERE
--			numDomainID = @numDomainId AND
--			tintGroupType = 1

--		SET @i = @i + 1
--	END
--COMMIT
--END TRY
--BEGIN CATCH
--	IF @@TRANCOUNT > 0
--		ROLLBACK TRANSACTION;

--	SELECT 
--		ERROR_MESSAGE(),
--		ERROR_NUMBER(),
--		ERROR_SEVERITY(),
--		ERROR_STATE(),
--		ERROR_LINE(),
--		ISNULL(ERROR_PROCEDURE(), '-');
--END CATCH


---- CHECK FIELD IDS ON LIVE
--UPDATE DycFormField_Mapping SET bitDefault=1 WHERE numFormID=139 AND numFieldID IN (189,349,900,50904,50905,50911)



--BEGIN TRY
--BEGIN TRANSACTION

--DECLARE @numFieldID NUMERIC(18,0)

--INSERT INTO DycFieldMaster
--(
--	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering
--)
--VALUES
--(
--	4,'Adjusted Value ($)','monAdjustedValue','monAdjustedValue','AdjustedValue','WareHouseItems','N','R','Label',0,1,0,0,0,0,0,0,1,1
--)

--SET @numFieldID = SCOPE_IDENTITY()


--INSERT INTO ReportFieldGroupMappingMaster
--(
--	numReportFieldGroupID,numFieldID,vcFieldName,vcAssociatedControlType,vcFieldDataType,bitAllowSorting,bitAllowGrouping,bitAllowAggregate,bitAllowFiltering
--)
--VALUES
--(
--	11,@numFieldID,'Adjusted Value ($)','TextBox','M',1,1,1,1
--)

--COMMIT
--END TRY
--BEGIN CATCH
--	IF @@TRANCOUNT > 0
--		ROLLBACK TRANSACTION;

--	SELECT 
--		ERROR_MESSAGE(),
--		ERROR_NUMBER(),
--		ERROR_SEVERITY(),
--		ERROR_STATE(),
--		ERROR_LINE(),
--		ISNULL(ERROR_PROCEDURE(), '-');
--END CATCH

--ALTER TABLE Category ADD bitVisible BIT DEFAULT 1
--UPDATE Category SET bitVisible=1


--DECLARE @numFieldID NUMERIC(18,0)
--SELECT @numFieldID=numFieldID FROM DycFieldMaster WHERE vcDbColumnName='vcItemRequiredDate' AND vcOrigDbColumnName='vcItemRequiredDate'
--UPDATE DycFieldMaster SET vcDbColumnName='ItemRequiredDate',vcOrigDbColumnName='ItemRequiredDate' WHERE numFieldId=@numFieldID

--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering
--)
--VALUES
--(
--	3,@numFieldID,139,0,0,'Required Date','DateField',10,1,0,1,1,0,0,1,1
--)
-----------------------------------------------------------------------------------
-----------------------------------------------------------------------------------
-----------------------------------------------------------------------------------

--UPDATE DynamicFormMaster SET vcFormName='Organizations & Contacts' WHERE numFormId=1 -- OLD VALUE: Advanced Search Page Configuration
--UPDATE DynamicFormMaster SET vcFormName='Opportunities & Orders (with items)' WHERE numFormId=15 -- OLD VALUE: Opportunities & Orders / Deals Search
--UPDATE DynamicFormMaster SET vcFormName='Items' WHERE numFormId=29 -- OLD VALUE: Item Search
--UPDATE DynamicFormMaster SET vcFormName='Cases' WHERE numFormId=17 -- OLD VALUE: Cases Search
--UPDATE DynamicFormMaster SET vcFormName='Projects' WHERE numFormId=18 -- OLD VALUE: Projects Search
--UPDATE DynamicFormMaster SET vcFormName='Financial Transactions' WHERE numFormId=59 -- OLD VALUE: Advanced Financial Transaction Search

---- FIRST RUN SELECT ON PRODUCTION SERVER
--DELETE FROM DycFieldMaster WHERE numFieldId IN (SELECT numFieldID FROM DycFormField_Mapping WHERE numFormID=15) AND numModuleID IN (1,2,4) AND numFieldId NOT IN (219,221,224,226)
--DELETE FROM DycFormConfigurationDetails WHERE numFormId=15 AND numFieldId IN (SELECT numFieldId FROM DycFieldMaster WHERE numFieldId IN (SELECT numFieldID FROM DycFormField_Mapping WHERE numFormID=15) AND numModuleID IN (1,2,4) AND numFieldId NOT IN (219,221,224,226))

--UPDATE DycFormConfigurationDetails SET intColumnNum=1 WHERE numFormId IN (1,15,29,17,18,59)

--DELETE FROM DycFormField_Mapping WHERE numFormID=59 AND numFieldID IN (6,14)

--DELETE FROM DycFormConfigurationDetails WHERE numFormId=59 AND numFieldID IN (6,14)

--ALTER TABLE SavedSearch ADD vcSearchConditionJson VARCHAR(MAX)
--ALTER TABLE SavedSearch ADD numSharedFromSearchID NUMERIC(18,0)
--ALTER TABLE SavedSearch ADD vcDisplayColumns VARCHAR(MAX)
--ALTER TABLE SavedSearch ALTER COLUMN vcSearchQuery VARCHAR(MAX) NULL
--ALTER TABLE SavedSearch ALTER COLUMN intSlidingDays INT NULL
--ALTER TABLE AdvSerViewConf ADD numUserCntID NUMERIC(18,0)

--SET IDENTITY_INSERT DycFieldMaster ON

--INSERT INTO DycFieldMaster
--(
--	numFieldId,numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcListItemType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting
--)
--VALUES
--(
--	33,1,'Organization Modified Date','bintModifiedDate','bintModifiedDate','DivisionMaster','V','R','DateField','',0,0,0,0,0,0,0,0,1
--),
--(
--	34,1,'Organization Visited Date','bintVisitedDate','bintVisitedDate','DivisionMaster','V','R','DateField','',0,0,0,0,0,0,0,0,1
--)

--SET IDENTITY_INSERT DycFieldMaster OFF

--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,bitInResults,bitDeleted,bitDefault,bitAllowFiltering
--)
--VALUES
--(
--	1,33,1,0,0,'Organization Modified Date','DateField',0,0,0,1
--),
--(
--	1,34,1,0,0,'Organization Visited Date','DateField',0,0,0,1
--)

--------------------

--DECLARE @numFieldID NUMERIC(18,0)

--INSERT INTO DycFieldMaster
--(
--	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcListItemType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting
--)
--VALUES
--(
--	1,'Exclude Organizations with Sales Orders Created Date','dtExcludeOrgWithSalesCreatedDate','dtExcludeOrgWithSalesCreatedDate','DivisionMaster','V','R','DateField','',0,0,0,0,0,0,0,0,1
--)

--SET @numFieldID = SCOPE_IDENTITY()

--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,bitInResults,bitDeleted,bitDefault,bitAllowFiltering
--)
--VALUES
--(
--	1,@numFieldID,1,0,0,'Exclude Organizations with Sales Orders Created Date','DateField',0,0,0,0
--)

-----------------------

--DECLARE @numFieldID NUMERIC(18,0)

--INSERT INTO DycFieldMaster
--(
--	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcListItemType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting
--)
--VALUES
--(
--	1,'Exclude Organizations with no sales orders','bitExcludeOrgWithNoSalesOrders','bitExcludeOrgWithNoSalesOrders','DivisionMaster','Y','R','CheckBox','',0,0,0,0,0,0,0,0,1
--)

--SET @numFieldID = SCOPE_IDENTITY()

--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,bitInResults,bitDeleted,bitDefault,bitAllowFiltering
--)
--VALUES
--(
--	1,@numFieldID,1,0,0,'Exclude Organizations with no sales orders','CheckBox',0,0,0,0
--)

---------------------------------------------------------

--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,bitInResults,bitDeleted,bitDefault,bitAllowFiltering
--)
--VALUES
--(
--	3,119,15,0,0,'Order Created Date','DateField',0,0,0,1
--)

--------------------------------------------------------------

--SET IDENTITY_INSERT DycFieldMaster ON

--INSERT INTO DycFieldMaster
--(
--	numFieldId,numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcListItemType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting
--)
--VALUES
--(
--	124,3,'Order Modified Date','bintModifiedDate','bintModifiedDate','OpportunityMaster','V','R','DateField','',0,0,0,0,0,0,0,0,1
--)

--SET IDENTITY_INSERT DycFieldMaster OFF

--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,bitInResults,bitDeleted,bitDefault,bitAllowFiltering
--)
--VALUES
--(
--	3,124,15,0,0,'Order Modified Date','DateField',0,0,0,1
--)

-----------------------------------------------------------------

--UPDATE DycFieldMaster SET vcListItemType='OT' WHERE numFieldId=577

-----------------------------------------------------------------

--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,bitInResults,bitDeleted,bitDefault,bitAllowFiltering
--)
--VALUES
--(
--	3,577,15,0,0,'Opp/Order Type','SelectBox',0,0,0,1
--)

--------------------------------------------------------------------

--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,bitInResults,bitDeleted,bitDefault,bitAllowFiltering
--)
--VALUES
--(
--	7,143,17,0,0,'Case Created Date','DateField',0,0,0,1
--),
--(
--	7,488,17,0,0,'Case Modified Date','DateField',0,0,0,1
--)

--UPDATE DycFormField_Mapping SET vcAssociatedControlType='DateField' WHERE numFieldID = 751 AND numFormID = 1


--UPDATE DycFormField_Mapping SET vcAssociatedControlType='DateField' WHERE numFormID=15 AND numFieldID=108


--UPDATE DycFormField_Mapping SET vcAssociatedControlType='CheckBox' WHERE numFormID=15 AND numFieldID=313

--UPDATE DycFormField_Mapping SET vcAssociatedControlType='TextBox' WHERE numFormID=18 AND numFieldId=152

--UPDATE DycFieldMaster SET vcListItemType='IC' WHERE numFieldId=311

--DELETE FROM DycFormField_Mapping WHERE numFormID=29 AND numFieldID=349
-----------------------------------------------------------------------------------
-----------------------------------------------------------------------------------
-----------------------------------------------------------------------------------

---- DO NOT EXECUTE ON SERVER
--INSERT INTO DynamicFormMaster
--(
--	numFormId,vcFormName,cCustomFieldsAssociated,cAOIAssociated,bitDeleted,tintFlag
--)
--VALUES
--(
--	136,'Import Sales Order(s)','N','N',0,2
--),
--(
--	137,'Import Purchase Order(s)','N','N',0,2
--)


--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,numFormID
--)
--VALUES
--(
--	1,539,'Organization ID','Label','DivisionID',1,1,1,136
--),
--(
--	1,3,'Organization Name','TextBox','CompanyName',1,1,1,136
--),
--(
--	1,5,'Organization Profile','SelectBox','Profile',1,1,1,136
--),
--(
--	1,6,'Relationship (e.g. Customer, Vendor, etc�)','SelectBox','CompanyType',1,1,1,136
--),
--(
--	1,10,'Organization Phone','TextBox','ComPhone',1,1,1,136
--),
--(
--	1,14,'Organization Assigned To','SelectBox','AssignedTo',1,1,1,136
--),
--(
--	1,21,'Territory','SelectBox','TerritoryID',1,1,1,136
--),
--(
--	1,28,'Industry','SelectBox','CompanyIndustry',1,1,1,136
--),
--(
--	1,30,'Organization Status','SelectBox','StatusID',1,1,1,136
--),
--(
--	1,451,'Relationship Type (e.g. Leads, Prospect or Accounts)','SelectBox','CRMType',1,1,1,136
--),
--(
--	1,217,'Ship To Street','TextBox','SStreet',1,1,1,136
--),
--(
--	1,218,'Ship To City','TextBox','SCity',1,1,1,136
--),
--(
--	1,219,'Ship To State','SelectBox',NULL,1,1,1,136
--),
--(
--	1,220,'Ship To Postal Code','TextBox','SPostalCode',1,1,1,136
--),
--(
--	1,221,'Ship To Country','SelectBox',NULL,1,1,1,136
--),
--(
--	1,222,'Bill To Street','TextBox','Street',1,1,1,136
--),
--(
--	1,223,'Bill To City','TextBox','City',1,1,1,136
--),
--(
--	1,224,'Bill To State','SelectBox',NULL,1,1,1,136
--),
--(
--	1,225,'Bill To Postal Code','TextBox','PostalCode',1,1,1,136
--),
--(
--	1,226,'Bill To Country','SelectBox','Country',1,1,1,136
--),
--(
--	4,189,'Item Name','TextBox',NULL,1,1,1,136
--),
--(
--	4,281,'SKU','TextBox',NULL,1,1,1,136
--),
--(
--	4,203,'UPC','TextBox',NULL,1,1,1,136
--),
--(
--	4,899,'Customer Part#','TextBox',NULL,1,1,1,136
--),
--(
--	4,211,'Item ID','TextBox',NULL,1,1,1,136
--),
--(
--	4,291,'Vendor Part#','TextBox',NULL,1,1,1,136
--),
--(
--	3,258,'Units','TextBox',NULL,1,1,1,136
--),
--(
--	3,259,'Unit Price','TextBox',NULL,1,1,1,136
--),
--(
--	3,348,'UOM','SelectBox',NULL,1,1,1,136
--),
--(
--	3,262,'Discount','TextBox',NULL,1,1,1,136
--),
--(
--	3,257,'Drop Ship','CheckBox',NULL,1,1,1,136
--),
--(
--	3,293,'Warehouse','SelectBox',NULL,1,1,1,136
--),
--(
--	3,233,'Warehouse Location','SelectBox',NULL,1,1,1,136
--),
--(
--	3,(SELECT TOP 1 numFieldId FROM DycFieldMaster WHERE vcDbColumnName='intUsedShippingCompany' AND vcLookBackTableName='OpportunityMaster'),'Ship Via (e.g. Fedex, UPS, USPS etc...)','SelectBox',NULL,1,1,1,136
--),
--(
--	3,(SELECT TOP 1 numFieldId FROM DycFieldMaster WHERE vcDbColumnName='numShippingService' AND vcLookBackTableName='OpportunityMaster'),'Shipping Service (e.g. Fedex Ground, UPS Ground etc...)','SelectBox',NULL,1,1,1,136
--),
--(
--	3,101,'Order Status','SelectBox',NULL,1,1,1,136
--),
--(
--	3,742,'Cost','TextBox',NULL,1,1,1,136
--),
--(
--	3,535,'Customer PO#','TextBox',NULL,1,1,1,136
--)

--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,numFormID
--)
--VALUES
--(
--	3,914,'Is Work Order','CheckBox',NULL,1,1,1,136
--)

--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,numFormID
--)
--VALUES
--(
--	3,915,'Discount Type (Percentage, Flat Amount)','TextBox',NULL,1,1,1,136
--)

----------------------------------


--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,numFormID
--)
--VALUES
--(
--	1,539,'Organization ID','Label','DivisionID',1,1,1,137
--),
--(
--	1,3,'Organization Name','TextBox','CompanyName',1,1,1,137
--),
--(
--	1,5,'Organization Profile','SelectBox','Profile',1,1,1,137
--),
--(
--	1,6,'Relationship (e.g. Customer, Vendor, etc�)','SelectBox','CompanyType',1,1,1,137
--),
--(
--	1,10,'Organization Phone','TextBox','ComPhone',1,1,1,137
--),
--(
--	1,14,'Assigned To','SelectBox','AssignedTo',1,1,1,137
--),
--(
--	1,21,'Territory','SelectBox','TerritoryID',1,1,1,137
--),
--(
--	1,28,'Industry','SelectBox','CompanyIndustry',1,1,1,137
--),
--(
--	1,30,'Organization Status','SelectBox','StatusID',1,1,1,137
--),
--(
--	1,451,'Relationship Type (e.g. Leads, Prospect or Accounts)','SelectBox','CRMType',1,1,1,137
--),
--(
--	1,217,'Ship To Street','TextBox','SStreet',1,1,1,137
--),
--(
--	1,218,'Ship To City','TextBox','SCity',1,1,1,137
--),
--(
--	1,219,'Ship To State','SelectBox',NULL,1,1,1,137
--),
--(
--	1,220,'Ship To Postal Code','TextBox','SPostalCode',1,1,1,137
--),
--(
--	1,221,'Ship To Country','SelectBox',NULL,1,1,1,137
--),
--(
--	1,222,'Bill To Street','TextBox','Street',1,1,1,137
--),
--(
--	1,223,'Bill To City','TextBox','City',1,1,1,137
--),
--(
--	1,224,'Bill To State','SelectBox',NULL,1,1,1,137
--),
--(
--	1,225,'Bill To Postal Code','TextBox','PostalCode',1,1,1,137
--),
--(
--	1,226,'Bill To Country','SelectBox','Country',1,1,1,137
--),
--(
--	4,189,'Item Name','TextBox',NULL,1,1,1,137
--),
--(
--	4,281,'SKU','TextBox',NULL,1,1,1,137
--),
--(
--	4,203,'UPC','TextBox',NULL,1,1,1,137
--),
--(
--	4,899,'Customer Part#','TextBox',NULL,1,1,1,137
--),
--(
--	4,211,'Item ID','TextBox',NULL,1,1,1,137
--),
--(
--	4,291,'Vendor Part#','TextBox',NULL,1,1,1,137
--),
--(
--	3,258,'Units','TextBox',NULL,1,1,1,137
--),
--(
--	3,259,'Unit Price','TextBox',NULL,1,1,1,137
--),
--(
--	3,348,'UOM','SelectBox',NULL,1,1,1,137
--),
--(
--	3,293,'Warehouse','SelectBox',NULL,1,1,1,137
--),
--(
--	3,233,'Warehouse Location','SelectBox',NULL,1,1,1,137
--),
--(
--	3,(SELECT TOP 1 numFieldId FROM DycFieldMaster WHERE vcDbColumnName='intUsedShippingCompany' AND vcLookBackTableName='OpportunityMaster'),'Ship Via(e.g. Fedex, UPS, USPS etc...)','SelectBox',NULL,1,1,1,137
--),
--(
--	3,(SELECT TOP 1 numFieldId FROM DycFieldMaster WHERE vcDbColumnName='numShippingService' AND vcLookBackTableName='OpportunityMaster'),'Shipping Service(e.g. Fedex Ground, UPS Ground etc...)','SelectBox',NULL,1,1,1,137
--),
--(
--	3,101,'Order Status','SelectBox',NULL,1,1,1,137
--)


--UPDATE DycFormField_Mapping SET intSectionID=1 WHERE numFormID IN (136,137) AND numFieldID IN (3,5,6,10,14,21,28,30,451,539)
--UPDATE DycFormField_Mapping SET intSectionID=2 WHERE numFormID IN (136,137) AND numFieldID IN (217,218,219,220,221,222,223,224,225,226,101,100,(SELECT TOP 1 numFieldId FROM DycFieldMaster WHERE vcDbColumnName='intUsedShippingCompany' AND vcLookBackTableName='OpportunityMaster'),(SELECT TOP 1 numFieldId FROM DycFieldMaster WHERE vcDbColumnName='numShippingService' AND vcLookBackTableName='OpportunityMaster'),535)
--UPDATE DycFormField_Mapping SET intSectionID=3 WHERE numFormID IN (136,137) AND numFieldID IN (293,233,257,258,259,262,306,189,203,211,281,291,899,@numFieldIDWorkOrder,@numFieldIDDiscountType,742)

--UPDATE DycFormField_Mapping SET bitImport=1 WHERE numFormID IN (136,137)

--ALTER TABLE Import_File_Master ADD bitSingleOrder BIT
--ALTER TABLE Import_File_Master ADD bitExistingOrganization BIT
--ALTER TABLE Import_File_Master ADD numDivisionID NUMERIC(18,0)
--ALTER TABLE Import_File_Master ADD numContactID NUMERIC(18,0)
--ALTER TABLE Import_File_Master ADD vcCompanyName VARCHAR(300)
--ALTER TABLE Import_File_Master ADD vcContactFirstName VARCHAR(100)
--ALTER TABLE Import_File_Master ADD vcContactLastName VARCHAR(100)
--ALTER TABLE Import_File_Master ADD vcContactEmail VARCHAR(100)
--ALTER TABLE Import_File_Master ADD bitMatchOrganizationID BIT
--ALTER TABLE Import_File_Master ADD numMatchFieldID NUMERIC(18,0)

--INSERT INTO DycFormSectionDetail
--(
--	numFormID,intSectionID,vcSectionName,Loc_id
--)
--VALUES
--(
--	136,1,'Organization Fields',0
--),
--(
--	136,2,'Order Fields',0
--),
--(
--	136,3,'Order Item(s) Fields',0
--)


--INSERT INTO DycFormSectionDetail
--(
--	numFormID,intSectionID,vcSectionName,Loc_id
--)
--VALUES
--(
--	137,1,'Organization Fields',0
--),
--(
--	137,2,'Order Fields',0
--),
--(
--	137,3,'Order Item(s) Fields',0
--)
-----------------------------------------------------------------------------------
-----------------------------------------------------------------------------------
-----------------------------------------------------------------------------------

--DELETE FROM DycFormField_Mapping WHERE numFormID=23 AND numFieldID = 248
--DELETE FROM DycFormConfigurationDetails WHERE numFormId=23 AND numFieldID = 248

--DELETE FROM DycFormField_Mapping WHERE numFormID=23 AND vcFieldName='Inventory Status'
--DELETE FROM DycFormConfigurationDetails WHERE numFormId=23 AND numFieldID IN (SELECT numFieldId FROM DycFormField_Mapping WHERE numFormID=23 AND vcFieldName='Inventory Status')

--UPDATE SalesFulfillmentConfiguration SET bitRule1IsActive=0
--UPDATE SalesFulfillmentConfiguration SET bitRule4IsActive=0

--ALTER TABLE Domain ADD tintCommitAllocation TINYINT DEFAULT 1
--UPDATE Domain SET tintCommitAllocation=1
--ALTER TABLE DivisionMaster ADD bitAllocateInventoryOnPickList BIT DEFAULT 0
--UPDATE DivisionMaster SET bitAllocateInventoryOnPickList=0
--ALTER TABLE Domain ADD tintInvoicing TINYINT DEFAULT 1
--UPDATE Domain SET tintInvoicing=1
-----------------------------------------------------------------------------------
-----------------------------------------------------------------------------------
-----------------------------------------------------------------------------------

--USE [Production.2014]
--GO

--/****** Object:  Table [dbo].[OrgOppAddressModificationHistory]    Script Date: 06-Jun-18 10:33:51 AM ******/
--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

--CREATE TABLE [dbo].[OrgOppAddressModificationHistory](
--	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
--	[tintAddressOf] [tinyint] NOT NULL,
--	[tintAddressType] [tinyint] NOT NULL,
--	[numRecordID] [numeric](18, 0) NOT NULL,
--	[numAddressID] [numeric](18, 0) NULL,
--	[numModifiedBy] [numeric](18, 0) NOT NULL,
--	[dtModifiedDate] [datetime] NOT NULL,
-- CONSTRAINT [PK_OrgOppAddressModificationHistory] PRIMARY KEY CLUSTERED 
--(
--	[ID] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
--) ON [PRIMARY]

--GO

--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Values= 1:Organization 2:Opp/Order' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'OrgOppAddressModificationHistory', @level2type=N'COLUMN',@level2name=N'tintAddressOf'
--GO

--EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Values = 1: Billing Address, 2: Shipping Address' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'OrgOppAddressModificationHistory', @level2type=N'COLUMN',@level2name=N'tintAddressType'
--GO





--DELETE ReportFieldsList WHERE numReportFieldGroupID=12 AND numFieldID=189

--DELETE FROm ReportFieldGroupMappingMaster WHERE numReportFieldGroupID=12 AND numFieldID=189


--UPDATE ReportFieldGroupMappingMaster SET vcFieldName = 'From' WHERE numReportFieldGroupID=12 AND vcFieldName='From Warehouse Location'	
--UPDATE ReportFieldGroupMappingMaster SET vcFieldName = 'To' WHERE numReportFieldGroupID=12 AND vcFieldName='To Warehouse Location'	

--ALTER TABLE MassStockTransfer ADD numToItemCode NUMERIC(18,0)
--UPDATE MassStockTransfer SET numToItemCode=numItemCode


--INSERT INTO ReportFieldGroupMappingMaster
--(
--	numReportFieldGroupID,numFieldID,vcFieldName,vcAssociatedControlType,vcFieldDataType,bitAllowSorting,bitAllowGrouping,bitAllowAggregate,bitAllowFiltering
--)
--VALUES
--(
--	7,500,'Discount Type','TextBox','V',0,0,0,0
--),
--(
--	7,502,'Price Level Type','TextBox','V',0,0,0,0
--)

--INSERT INTO ReportFieldGroupMappingMaster
--(
--	numReportFieldGroupID,numFieldID,vcFieldName,vcAssociatedControlType,vcFieldDataType,bitAllowSorting,bitAllowGrouping,bitAllowAggregate,bitAllowFiltering
--)
--VALUES
--(
--	7,(SELECT numFieldId FROM DycFieldMaster WHERE vcDbColumnName='monPriceLevel1' AND vcLookBackTableName='PricingTable'),'Price Level 1','TextBox','M',0,0,0,0
--),
--(
--	7,(SELECT numFieldId FROM DycFieldMaster WHERE vcDbColumnName='monPriceLevel2' AND vcLookBackTableName='PricingTable'),'Price Level 2','TextBox','M',0,0,0,0
--),
--(
--	7,(SELECT numFieldId FROM DycFieldMaster WHERE vcDbColumnName='monPriceLevel3' AND vcLookBackTableName='PricingTable'),'Price Level 3','TextBox','M',0,0,0,0
--),
--(
--	7,(SELECT numFieldId FROM DycFieldMaster WHERE vcDbColumnName='monPriceLevel4' AND vcLookBackTableName='PricingTable'),'Price Level 4','TextBox','M',0,0,0,0
--),
--(
--	7,(SELECT numFieldId FROM DycFieldMaster WHERE vcDbColumnName='monPriceLevel5' AND vcLookBackTableName='PricingTable'),'Price Level 5','TextBox','M',0,0,0,0
--),
--(
--	7,(SELECT numFieldId FROM DycFieldMaster WHERE vcDbColumnName='monPriceLevel6' AND vcLookBackTableName='PricingTable'),'Price Level 6','TextBox','M',0,0,0,0
--),
--(
--	7,(SELECT numFieldId FROM DycFieldMaster WHERE vcDbColumnName='monPriceLevel7' AND vcLookBackTableName='PricingTable'),'Price Level 7','TextBox','M',0,0,0,0
--),
--(
--	7,(SELECT numFieldId FROM DycFieldMaster WHERE vcDbColumnName='monPriceLevel8' AND vcLookBackTableName='PricingTable'),'Price Level 8','TextBox','M',0,0,0,0
--),
--(
--	7,(SELECT numFieldId FROM DycFieldMaster WHERE vcDbColumnName='monPriceLevel9' AND vcLookBackTableName='PricingTable'),'Price Level 9','TextBox','M',0,0,0,0
--),
--(
--	7,(SELECT numFieldId FROM DycFieldMaster WHERE vcDbColumnName='monPriceLevel10' AND vcLookBackTableName='PricingTable'),'Price Level 10','TextBox','M',0,0,0,0
--),
--(
--	7,(SELECT numFieldId FROM DycFieldMaster WHERE vcDbColumnName='monPriceLevel11' AND vcLookBackTableName='PricingTable'),'Price Level 11','TextBox','M',0,0,0,0
--),
--(
--	7,(SELECT numFieldId FROM DycFieldMaster WHERE vcDbColumnName='monPriceLevel12' AND vcLookBackTableName='PricingTable'),'Price Level 12','TextBox','M',0,0,0,0
--),
--(
--	7,(SELECT numFieldId FROM DycFieldMaster WHERE vcDbColumnName='monPriceLevel13' AND vcLookBackTableName='PricingTable'),'Price Level 13','TextBox','M',0,0,0,0
--),
--(
--	7,(SELECT numFieldId FROM DycFieldMaster WHERE vcDbColumnName='monPriceLevel14' AND vcLookBackTableName='PricingTable'),'Price Level 14','TextBox','M',0,0,0,0
--),
--(
--	7,(SELECT numFieldId FROM DycFieldMaster WHERE vcDbColumnName='monPriceLevel15' AND vcLookBackTableName='PricingTable'),'Price Level 15','TextBox','M',0,0,0,0
--),
--(
--	7,(SELECT numFieldId FROM DycFieldMaster WHERE vcDbColumnName='monPriceLevel16' AND vcLookBackTableName='PricingTable'),'Price Level 16','TextBox','M',0,0,0,0
--),
--(
--	7,(SELECT numFieldId FROM DycFieldMaster WHERE vcDbColumnName='monPriceLevel17' AND vcLookBackTableName='PricingTable'),'Price Level 17','TextBox','M',0,0,0,0
--),
--(
--	7,(SELECT numFieldId FROM DycFieldMaster WHERE vcDbColumnName='monPriceLevel18' AND vcLookBackTableName='PricingTable'),'Price Level 18','TextBox','M',0,0,0,0
--),
--(
--	7,(SELECT numFieldId FROM DycFieldMaster WHERE vcDbColumnName='monPriceLevel19' AND vcLookBackTableName='PricingTable'),'Price Level 19','TextBox','M',0,0,0,0
--),
--(
--	7,(SELECT numFieldId FROM DycFieldMaster WHERE vcDbColumnName='monPriceLevel20' AND vcLookBackTableName='PricingTable'),'Price Level 20','TextBox','M',0,0,0,0
--)

--INSERT INTO ReportFieldGroupMappingMaster
--(
--	numReportFieldGroupID,numFieldID,vcFieldName,vcAssociatedControlType,vcFieldDataType,bitAllowSorting,bitAllowGrouping,bitAllowAggregate,bitAllowFiltering
--)
--VALUES
--(
--	12,289,'Vendor Cost','TextBox','M',0,0,0,0
--)

--SET IDENTITY_INSERT ReportFieldGroupMaster ON
--INSERT INTO ReportFieldGroupMaster
--(
--	numReportFieldGroupID,vcFieldGroupName,bitActive
--)
--VALUES
--(
--	12,'Stock Transfer',1
--)
--SET IDENTITY_INSERT ReportFieldGroupMaster OFF


--SET IDENTITY_INSERT ReportModuleMaster ON
--INSERT INTO ReportModuleMaster
--(
--	numReportModuleID,vcModuleName,bitActive
--)
--VALUES
--(
--	8,'Stock Transfer',1
--)
--SET IDENTITY_INSERT ReportModuleMaster OFF

--SET IDENTITY_INSERT ReportModuleGroupMaster ON
--INSERT INTO ReportModuleGroupMaster
--(
--	numReportModuleGroupID,numReportModuleID,vcGroupName,bitActive
--)
--VALUES
--(
--	25,8,'Stock Transfer',1
--)
--SET IDENTITY_INSERT ReportModuleGroupMaster OFF

--INSERT INTO ReportModuleGroupFieldMappingMaster
--(
--	numReportModuleGroupID,numReportFieldGroupID
--)
--VALUES
--(
--	25,12
--)

--------------

--BEGIN TRY
--BEGIN TRANSACTION
--	DECLARE @numFieldID INT
--	INSERT INTO DycFieldMaster
--	(
--		numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering,numListID
--	)
--	VALUES
--	(
--		4,'Transferred Date','dtTransferredDate','dtTransferredDate','TransferredDate','MassStockTransfer','V','R','DateField',1,0,0,0,0,1,1,0
--	)

--	SET @numFieldID = SCOPE_IDENTITY()

--	INSERT INTO ReportFieldGroupMappingMaster
--	(
--		numReportFieldGroupID,numFieldID,vcFieldName,vcAssociatedControlType,vcFieldDataType,bitAllowSorting,bitAllowGrouping,bitAllowAggregate,bitAllowFiltering
--	)
--	VALUES
--	(
--		12,@numFieldID,'Transferred Date','DateField','D',1,1,0,1
--	)
--COMMIT
--END TRY
--BEGIN CATCH
-- IF @@TRANCOUNT > 0
--  ROLLBACK TRANSACTION;

-- SELECT 
--  ERROR_MESSAGE(),
--  ERROR_NUMBER(),
--  ERROR_SEVERITY(),
--  ERROR_STATE(),
--  ERROR_LINE(),
--  ISNULL(ERROR_PROCEDURE(), '-');
--END CATCH
----------------------------

--BEGIN TRY
--BEGIN TRANSACTION
--	DECLARE @numFieldID INT
--	INSERT INTO DycFieldMaster
--	(
--		numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering,numListID
--	)
--	VALUES
--	(
--		4,'Transferred Qty','numQty','numQty','TransferredQty','MassStockTransfer','N','R','TextBox',1,0,0,0,0,1,1,0
--	)

--	SET @numFieldID = SCOPE_IDENTITY()

--	INSERT INTO ReportFieldGroupMappingMaster
--	(
--		numReportFieldGroupID,numFieldID,vcFieldName,vcAssociatedControlType,vcFieldDataType,bitAllowSorting,bitAllowGrouping,bitAllowAggregate,bitAllowFiltering
--	)
--	VALUES
--	(
--		12,@numFieldID,'Transferred Qty','TextBox','N',1,1,0,1
--	)
--COMMIT
--END TRY
--BEGIN CATCH
-- IF @@TRANCOUNT > 0
--  ROLLBACK TRANSACTION;

-- SELECT 
--  ERROR_MESSAGE(),
--  ERROR_NUMBER(),
--  ERROR_SEVERITY(),
--  ERROR_STATE(),
--  ERROR_LINE(),
--  ISNULL(ERROR_PROCEDURE(), '-');
--END CATCH

---------------------------

--INSERT INTO ReportFieldGroupMappingMaster
--(
--	numReportFieldGroupID,numFieldID,vcFieldName,vcAssociatedControlType,vcFieldDataType,bitAllowSorting,bitAllowGrouping,bitAllowAggregate,bitAllowFiltering
--)
--VALUES
--(
--	12,189,'Item','TextBox','V',1,1,0,0
--)

--INSERT INTO ReportFieldGroupMappingMaster
--(
--	numReportFieldGroupID,numFieldID,vcFieldName,vcAssociatedControlType,vcFieldDataType,bitAllowSorting,bitAllowGrouping,bitAllowAggregate,bitAllowFiltering
--)
--VALUES
--(
--	12,274,'Item Group','SelectBox','V',1,1,0,1
--)

----------------

--BEGIN TRY
--BEGIN TRANSACTION
--	DECLARE @numFieldID INT
--	INSERT INTO DycFieldMaster
--	(
--		numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering,numListID
--	)
--	VALUES
--	(
--		4,'From Qty Before','numFromQtyBefore','numFromQtyBefore','FromQtyBefore','MassStockTransfer','N','R','TextBox',1,0,0,0,0,1,1,0
--	)

--	SET @numFieldID = SCOPE_IDENTITY()

--	INSERT INTO ReportFieldGroupMappingMaster
--	(
--		numReportFieldGroupID,numFieldID,vcFieldName,vcAssociatedControlType,vcFieldDataType,bitAllowSorting,bitAllowGrouping,bitAllowAggregate,bitAllowFiltering
--	)
--	VALUES
--	(
--		12,@numFieldID,'From Qty Before','TextBox','N',1,1,1,1
--	)
--COMMIT
--END TRY
--BEGIN CATCH
-- IF @@TRANCOUNT > 0
--  ROLLBACK TRANSACTION;

-- SELECT 
--  ERROR_MESSAGE(),
--  ERROR_NUMBER(),
--  ERROR_SEVERITY(),
--  ERROR_STATE(),
--  ERROR_LINE(),
--  ISNULL(ERROR_PROCEDURE(), '-');
--END CATCH

-------------------

--BEGIN TRY
--BEGIN TRANSACTION
--	DECLARE @numFieldID INT
--	INSERT INTO DycFieldMaster
--	(
--		numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering,numListID
--	)
--	VALUES
--	(
--		4,'From Qty After','numFromQtyAfter','numFromQtyAfter','FromQtyAfter','MassStockTransfer','N','R','TextBox',1,0,0,0,0,1,1,0
--	)

--	SET @numFieldID = SCOPE_IDENTITY()

--	INSERT INTO ReportFieldGroupMappingMaster
--	(
--		numReportFieldGroupID,numFieldID,vcFieldName,vcAssociatedControlType,vcFieldDataType,bitAllowSorting,bitAllowGrouping,bitAllowAggregate,bitAllowFiltering
--	)
--	VALUES
--	(
--		12,@numFieldID,'From Qty After','TextBox','N',1,1,1,1
--	)
--COMMIT
--END TRY
--BEGIN CATCH
-- IF @@TRANCOUNT > 0
--  ROLLBACK TRANSACTION;

-- SELECT 
--  ERROR_MESSAGE(),
--  ERROR_NUMBER(),
--  ERROR_SEVERITY(),
--  ERROR_STATE(),
--  ERROR_LINE(),
--  ISNULL(ERROR_PROCEDURE(), '-');
--END CATCH

--------------------------

--BEGIN TRY
--BEGIN TRANSACTION
--	DECLARE @numFieldID INT
--	INSERT INTO DycFieldMaster
--	(
--		numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering,numListID
--	)
--	VALUES
--	(
--		4,'To Qty Before','numToQtyBefore','numToQtyBefore','ToQtyBefore','MassStockTransfer','N','R','TextBox',1,0,0,0,0,1,1,0
--	)

--	SET @numFieldID = SCOPE_IDENTITY()

--	INSERT INTO ReportFieldGroupMappingMaster
--	(
--		numReportFieldGroupID,numFieldID,vcFieldName,vcAssociatedControlType,vcFieldDataType,bitAllowSorting,bitAllowGrouping,bitAllowAggregate,bitAllowFiltering
--	)
--	VALUES
--	(
--		12,@numFieldID,'To Qty Before','TextBox','N',1,1,1,1
--	)
--COMMIT
--END TRY
--BEGIN CATCH
-- IF @@TRANCOUNT > 0
--  ROLLBACK TRANSACTION;

-- SELECT 
--  ERROR_MESSAGE(),
--  ERROR_NUMBER(),
--  ERROR_SEVERITY(),
--  ERROR_STATE(),
--  ERROR_LINE(),
--  ISNULL(ERROR_PROCEDURE(), '-');
--END CATCH

----------------------

--BEGIN TRY
--BEGIN TRANSACTION
--	DECLARE @numFieldID INT
--	INSERT INTO DycFieldMaster
--	(
--		numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering,numListID
--	)
--	VALUES
--	(
--		4,'To Qty After','numToQtyAfter','numToQtyAfter','ToQtyAfter','MassStockTransfer','N','R','TextBox',1,0,0,0,0,1,1,0
--	)

--	SET @numFieldID = SCOPE_IDENTITY()

--	INSERT INTO ReportFieldGroupMappingMaster
--	(
--		numReportFieldGroupID,numFieldID,vcFieldName,vcAssociatedControlType,vcFieldDataType,bitAllowSorting,bitAllowGrouping,bitAllowAggregate,bitAllowFiltering
--	)
--	VALUES
--	(
--		12,@numFieldID,'To Qty After','TextBox','N',1,1,1,1
--	)
--COMMIT
--END TRY
--BEGIN CATCH
-- IF @@TRANCOUNT > 0
--  ROLLBACK TRANSACTION;

-- SELECT 
--  ERROR_MESSAGE(),
--  ERROR_NUMBER(),
--  ERROR_SEVERITY(),
--  ERROR_STATE(),
--  ERROR_LINE(),
--  ISNULL(ERROR_PROCEDURE(), '-');
--END CATCH

---------------------

--BEGIN TRY
--BEGIN TRANSACTION
--	DECLARE @numFieldID INT
--	INSERT INTO DycFieldMaster
--	(
--		numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering,numListID
--	)
--	VALUES
--	(
--		4,'From Warehouse','vcWareHouse','vcFromWareHouse','FromWarehouse','Warehouses','V','R','TextBox',1,0,0,0,0,1,0,0
--	)

--	SET @numFieldID = SCOPE_IDENTITY()

--	INSERT INTO ReportFieldGroupMappingMaster
--	(
--		numReportFieldGroupID,numFieldID,vcFieldName,vcAssociatedControlType,vcFieldDataType,bitAllowSorting,bitAllowGrouping,bitAllowAggregate,bitAllowFiltering
--	)
--	VALUES
--	(
--		12,@numFieldID,'From Warehouse Location','TextBox','V',1,1,0,1
--	)
--COMMIT
--END TRY
--BEGIN CATCH
-- IF @@TRANCOUNT > 0
--  ROLLBACK TRANSACTION;

-- SELECT 
--  ERROR_MESSAGE(),
--  ERROR_NUMBER(),
--  ERROR_SEVERITY(),
--  ERROR_STATE(),
--  ERROR_LINE(),
--  ISNULL(ERROR_PROCEDURE(), '-');
--END CATCH

----------------------------------


--BEGIN TRY
--BEGIN TRANSACTION
--	DECLARE @numFieldID INT
--	INSERT INTO DycFieldMaster
--	(
--		numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering,numListID
--	)
--	VALUES
--	(
--		4,'To Warehouse','vcWareHouse','vcToWareHouse','ToWarehouse','Warehouses','V','R','TextBox',1,0,0,0,0,1,0,0
--	)

--	SET @numFieldID = SCOPE_IDENTITY()

--	INSERT INTO ReportFieldGroupMappingMaster
--	(
--		numReportFieldGroupID,numFieldID,vcFieldName,vcAssociatedControlType,vcFieldDataType,bitAllowSorting,bitAllowGrouping,bitAllowAggregate,bitAllowFiltering
--	)
--	VALUES
--	(
--		12,@numFieldID,'To Warehouse Location','TextBox','V',1,1,0,1
--	)
--COMMIT
--END TRY
--BEGIN CATCH
-- IF @@TRANCOUNT > 0
--  ROLLBACK TRANSACTION;

-- SELECT 
--  ERROR_MESSAGE(),
--  ERROR_NUMBER(),
--  ERROR_SEVERITY(),
--  ERROR_STATE(),
--  ERROR_LINE(),
--  ISNULL(ERROR_PROCEDURE(), '-');
--END CATCH

----------------------


--BEGIN TRY
--BEGIN TRANSACTION
--	DECLARE @numFieldID INT
--	SELECT @numFieldID=numFieldID FROM DycFieldMaster WHERE vcFieldName='Customer Part#'

--	INSERT INTO ReportFieldGroupMappingMaster
--	(
--		numReportFieldGroupID,numFieldID,vcFieldName,vcAssociatedControlType,vcFieldDataType,bitAllowSorting,bitAllowGrouping,bitAllowAggregate,bitAllowFiltering
--	)
--	VALUES
--	(
--		7,@numFieldID,'Customer Part#','TextBox','V',1,1,0,1
--	)
--COMMIT
--END TRY
--BEGIN CATCH
-- IF @@TRANCOUNT > 0
--  ROLLBACK TRANSACTION;

-- SELECT 
--  ERROR_MESSAGE(),
--  ERROR_NUMBER(),
--  ERROR_SEVERITY(),
--  ERROR_STATE(),
--  ERROR_LINE(),
--  ISNULL(ERROR_PROCEDURE(), '-');
--END CATCH

-----------------------------------------------------------------------------------
-----------------------------------------------------------------------------------
-----------------------------------------------------------------------------------

--DECLARE @numFieldID NUMERIC(18,0)

--SELECT @numFieldID=numFieldId FROM DycFieldMaster WHERE vcFieldName = 'Inventory Status'

--IF ISNULL(@numFieldID,0) > 0
--BEGIN
--	INSERT INTO ReportFieldGroupMappingMaster
--	(
--		numReportFieldGroupID,numFieldID,vcFieldName,vcAssociatedControlType,vcFieldDataType,bitAllowSorting,bitAllowGrouping,bitAllowAggregate,bitAllowFiltering
--	)
--	VALUES
--	(
--		3,@numFieldID,'Inventory Status','SelectBox','V',1,1,0,1
--	)
--END


--DECLARE @numFieldID NUMERIC(18,0)
--SELECT @numFieldID=numFieldId FROM DycFieldMaster WHERE vcFieldName='Available'
--IF ISNULL(@numFieldID,0) > 0
--BEGIN
--	INSERT INTO ReportFieldGroupMappingMaster
--	(
--		numReportFieldGroupID,numFieldID,vcFieldName,vcAssociatedControlType,vcFieldDataType,bitAllowSorting,bitAllowGrouping,bitAllowAggregate,bitAllowFiltering
--	)
--	VALUES
--	(
--		11,@numFieldID,'Quantity Available','TextBox','N',1,1,1,1
--	)
--END

-----------------------------------------------------------------------------------
-----------------------------------------------------------------------------------
-----------------------------------------------------------------------------------

----COMMENT TRIGGER IN OpportunityMaster table

--UPDATE OpportunityMaster SET numShippingService=intUsedShippingCompany
--UPDATE OpportunityMaster SET intUsedShippingCompany=numShipmentMethod
--UPDATE DycFieldMaster SET vcDbColumnName='intUsedShippingCompany',vcOrigDbColumnName='intUsedShippingCompany',vcPropertyName='ShippingCompany' WHERE vcLookBackTableName='OpportunityMaster' AND vcDbColumnName='numShipmentMethod' AND vcFieldName = 'Ship Via'

--BEGIN TRY
--BEGIN TRANSACTION

--	 --add new values for slide 1
--	 DECLARE @numFieldID AS NUMERIC(18,0)
--	 DECLARE @numFormFieldID AS NUMERIC(18,0)

--	 INSERT INTO DycFieldMaster 
--	(
--		numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcListItemType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitAllowFiltering,bitInlineEdit
--	)
--	VALUES
--	(
--		3,'Shipping Service','numShippingService','numShippingService','ShippingService','OpportunityMaster','N','R','SelectBox','',0,1,0,1,0,1,1,1,0,1,1
--	)


--	 SELECT @numFieldID = SCOPE_IDENTITY()



--	 INSERT INTO DycFormField_Mapping
--	 (numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,bitInResults,bitDeleted,
--	 bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering)
--	 VALUES
--	 (3,@numFieldID,41,1,1,'Shipping Service','SelectBox','ShippingService',48,1,1,1,0,0,1,1,1,1,1)

--	 UPDATE DycFormField_Mapping SET numFieldID=@numFieldID,bitAllowEdit=1,bitInlineEdit=1 WHERE numFormID=39 AND numFieldID IN (SELECT numFieldID FROM DycFieldMaster WHERE vcLookBackTableName='OpportunityMaster' AND vcDbColumnName='numDefaultShippingServiceID' AND vcFieldName='Shipping Service')
--	 UPDATE DycFormField_Mapping SET vcPropertyName=NULL WHERE numFormID IN (39,41) AND numFieldID IN (SELECT numFieldID FROM DycFieldMaster WHERE vcLookBackTableName='OpportunityMaster' AND vcDbColumnName='intUsedShippingCompany' AND vcFieldName='Ship Via')
--COMMIT
--END TRY
--BEGIN CATCH
-- IF @@TRANCOUNT > 0
--  ROLLBACK TRANSACTION;

-- SELECT 
--  ERROR_MESSAGE(),
--  ERROR_NUMBER(),
--  ERROR_SEVERITY(),
--  ERROR_STATE(),
--  ERROR_LINE(),
--  ISNULL(ERROR_PROCEDURE(), '-');
--END CATCH


--UPDATE DycFormField_Mapping SET numFieldID=198,vcFieldName='Warehouse BO' WHERE vcFieldName='Back Order' AND vcPropertyName='BackOrder' AND numFormID=26
--UPDATE DycFormConfigurationDetails SET numFieldId=198 WHERE numFormId=26 AND numFieldId=40720

-------------------------------

--DECLARE @numFieldID NUMERIC(18,0)
--SELECT @numFieldID=numFieldId FROM DycFieldMaster WHERE vcDbColumnName='vcBackordered' AND vcLookBackTableName='OpportunityItems'


--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowFiltering
--)
--VALUES
--(
--	3,@numFieldID,26,0,0,'Order BO','Label',1,0,0,1,0
--)

------------------------------

--ALTER TABLE WorkOrder ADD numOppItemID NUMERIC(18,0)
--ALTER TABLE OpportunityItems ADD vcInstruction VARCHAR(1000)
--ALTER TABLE OpportunityItems ADD bintCompliationDate DATETIME
--ALTER TABLE OpportunityItems ADD numWOAssignedTo NUMERIC(18,0)
-----------------------------------------------------------------------------------
-----------------------------------------------------------------------------------
-----------------------------------------------------------------------------------

--ALTER TABLE Domain ADD bitAllowDuplicateLineItems BIT DEFAULT 0
--ALTER TABLE [dbo].[OpportunityItemsReleaseDates] ALTER COLUMN [numQty] NUMERIC(18,0) NULL
--ALTER TABLE [dbo].[OpportunityItemsReleaseDates] ALTER COLUMN [tintStatus] TINYINT NULL

-----------------------------------------------------------------------------------
-----------------------------------------------------------------------------------
-----------------------------------------------------------------------------------

--DECLARE @numFieldID INT
--SELECT @numFieldID=numFieldId FROM DycFieldMaster WHERE vcDbColumnName='dtReleaseDate' AND vcLookBackTableName='OpportunityMaster'

--INSERT INTO ReportFieldGroupMappingMaster
--(
--	numReportFieldGroupID,numFieldID,vcFieldName,vcAssociatedControlType,vcFieldDataType,bitAllowSorting,bitAllowGrouping,bitAllowAggregate,bitAllowFiltering
--)
--VALUES
--(
--	3,@numFieldID,'Release Date','DateField','D',1,1,0,1
--)


--ALTER TABLE ItemDetails ALTER COLUMN [numQtyItemsReq] FLOAT

---------------------------------

--ALTER TABLE OpportunityBizDocs ADD vcVendorInvoice VARCHAR(100)
--ALTER TABLE OpportunityBizDocItems ADD numVendorInvoiceBizDocID NUMERIC(18,0)
--ALTER TABLE OpportunityBizDocItems ADD numVendorInvoiceUnitReceived FLOAT

--ALTER TABLE WareHouseItems_Tracking ALTER COLUMN [numOnHand] FLOAT
--ALTER TABLE WareHouseItems_Tracking ALTER COLUMN [numOnOrder] FLOAT
--ALTER TABLE WareHouseItems_Tracking ALTER COLUMN [numReorder] FLOAT
--ALTER TABLE WareHouseItems_Tracking ALTER COLUMN [numAllocation] FLOAT
--ALTER TABLE WareHouseItems_Tracking ALTER COLUMN [numBackOrder] FLOAT

--------------------------------

--BEGIN TRY
--BEGIN TRANSACTION
--	SELECT 
--		* 
--	INTO 
--		#temp 
--	FROM
--	(
--		SELECT 
--			ROW_NUMBER() OVER(ORDER BY numDomainId ASC) AS Row, 
--			numDomainId 
--		FROM 
--			Domain 
--		WHERE 
--			numDomainId > 0
--	) TABLE2
	
--	DECLARE @RowCount INT
--	SET @RowCount = (SELECT COUNT(Row) FROM #temp) 
	
--	DECLARE @I INT
--	DECLARE @numDomainId NUMERIC(18,0)
	
--	SET @I = 1

--	WHILE (@I <= @RowCount)
--	BEGIN
			
--		SELECT @numDomainId = numDomainID FROM #temp WHERE Row = @I
	
--		INSERT INTO BizDocTemplate
--		(
--			numDomainID,numBizDocID,numOppType,txtBizDocTemplate,txtCSS,bitEnabled,tintTemplateType,vcTemplateName,bitDefault,txtNewBizDocTemplate
--		)
--		VALUES
--		(
--			@numDomainId,305,2,'&lt;table width=&quot;100%&quot; border=&quot;0&quot; cellpadding=&quot;0&quot;&gt;
--    &lt;tbody&gt;
--        &lt;tr&gt;
--            &lt;td valign=&quot;top&quot; align=&quot;left&quot;&gt;
--            #Logo#
--            &lt;/td&gt;
--            &lt;td align=&quot;right&quot; class=&quot;title&quot; style=&quot;white-space: nowrap;&quot;&gt;
--            #Customer/VendorOrganizationName#
--            &lt;/td&gt;
--            &lt;td align=&quot;right&quot; class=&quot;title&quot; style=&quot;white-space: nowrap;&quot;&gt;
--            #BizDocTemplateName#
--            &lt;/td&gt;
--        &lt;/tr&gt;
--    &lt;/tbody&gt;
--&lt;/table&gt;
--&lt;table width=&quot;100%&quot; border=&quot;0&quot; cellpadding=&quot;0&quot;&gt;
--    &lt;tbody&gt;
--        &lt;tr&gt;
--            &lt;td align=&quot;right&quot;&gt;
--            &lt;table width=&quot;100%&quot;&gt;
--                &lt;tbody&gt;
--                    &lt;tr&gt;
--                        &lt;td class=&quot;RowHeader&quot;&gt;
--                        Discount
--                        &lt;/td&gt;
--                        &lt;td class=&quot;RowHeader&quot;&gt;
--                        Billing Terms
--                        &lt;/td&gt;
--                        &lt;td class=&quot;RowHeader&quot;&gt;
--                        #ChangeDueDate#
--                        &lt;/td&gt;
--                        &lt;td class=&quot;RowHeader&quot;&gt;
--                        ID#
--                        &lt;/td&gt;
--                    &lt;/tr&gt;
--                    &lt;tr&gt;
--                        &lt;td class=&quot;normal1&quot;&gt;
--                        #Discount#
--                        &lt;/td&gt;
--                        &lt;td class=&quot;normal1&quot;&gt;
--                        #BillingTerms#
--                        &lt;/td&gt;
--                        &lt;td class=&quot;normal1&quot;&gt;
--                        #DueDate#
--                        &lt;/td&gt;
--                        &lt;td class=&quot;normal1&quot;&gt;
--                        #BizDocID#
--                        &lt;/td&gt;
--                    &lt;/tr&gt;
--                &lt;/tbody&gt;
--            &lt;/table&gt;
--            &lt;/td&gt;
--        &lt;/tr&gt;
--        &lt;tr&gt;
--            &lt;td&gt;
--            &lt;table height=&quot;100%&quot; width=&quot;100%&quot;&gt;
--                &lt;tbody&gt;
--                    &lt;tr&gt;
--                        &lt;td class=&quot;RowHeader&quot;&gt;
--                        #EmployerChangeBillToHeader#
--                        &lt;/td&gt;
--                        &lt;td class=&quot;RowHeader&quot;&gt;
--                        #EmployerChangeShipToHeader#
--                        &lt;/td&gt;
--                        &lt;td class=&quot;RowHeader&quot;&gt;
--                        Status
--                        &lt;/td&gt;
--                    &lt;/tr&gt;
--                    &lt;tr&gt;
--                        &lt;td class=&quot;normal1&quot;&gt;
--                        #Customer/VendorOrganizationName#&lt;br /&gt;
--                        #EmployerBillToAddress#
--                        &lt;/td&gt;
--                        &lt;td class=&quot;normal1&quot;&gt;
--                        #EmployerShipToAddress#
--                        &lt;/td&gt;
--                        &lt;td&gt;
--                        &lt;table height=&quot;100%&quot; width=&quot;100%&quot;&gt;
--                            &lt;tbody&gt;
--                                &lt;tr&gt;
--                                    &lt;td colspan=&quot;2&quot; class=&quot;normal1&quot;&gt;
--                                    #BizDocStatus#
--                                    &lt;/td&gt;
--                                &lt;/tr&gt;
--                                &lt;tr&gt;
--                                    &lt;td class=&quot;normal1&quot;&gt;
--                                    #AmountPaidPopUp#
--                                    &lt;/td&gt;
--                                    &lt;td class=&quot;normal1&quot;&gt;
--                                    #Currency#&amp;nbsp;#AmountPaid#
--                                    &lt;/td&gt;
--                                &lt;/tr&gt;
--                                &lt;tr&gt;
--                                    &lt;td class=&quot;normal1&quot;&gt;
--                                    Balance Due:
--                                    &lt;/td&gt;
--                                    &lt;td class=&quot;normal1&quot;&gt;
--                                    #Currency#&amp;nbsp;#BalanceDue#
--                                    &lt;/td&gt;
--                                &lt;/tr&gt;
--                            &lt;/tbody&gt;
--                        &lt;/table&gt;
--                        &lt;/td&gt;
--                    &lt;/tr&gt;
--                &lt;/tbody&gt;
--            &lt;/table&gt;
--            &lt;/td&gt;
--        &lt;/tr&gt;
--        &lt;tr class=&quot;normal1&quot;&gt;
--            &lt;td&gt;
--            &lt;table height=&quot;100%&quot; width=&quot;100%&quot;&gt;
--                &lt;tbody&gt;
--                    &lt;tr&gt;
--                        &lt;td class=&quot;RowHeader&quot;&gt;
--                        &lt;asp:label id=&quot;Label1&quot; runat=&quot;server&quot;&gt;&lt;/asp:label&gt;
--                        &amp;nbsp;#
--                        &lt;/td&gt;
--                        &lt;td class=&quot;RowHeader&quot;&gt;
--                        Deal or Order ID
--                        &lt;/td&gt;
--                    &lt;/tr&gt;
--                    &lt;tr&gt;
--                        &lt;td&gt;
--                        #P.O.NO#
--                        &lt;/td&gt;
--                        &lt;td class=&quot;normal1&quot;&gt;
--                        #OrderID#
--                        &lt;/td&gt;
--                    &lt;/tr&gt;
--                &lt;/tbody&gt;
--            &lt;/table&gt;
--            &lt;/td&gt;
--        &lt;/tr&gt;
--        &lt;tr&gt;
--            &lt;td class=&quot;RowHeader&quot;&gt;
--            Comments
--            &lt;/td&gt;
--        &lt;/tr&gt;
--        &lt;tr class=&quot;normal1&quot;&gt;
--            &lt;td&gt;
--            #Comments#
--            &lt;/td&gt;
--        &lt;/tr&gt;
--        &lt;tr class=&quot;normal1&quot;&gt;
--            &lt;td&gt;
--            #Products#
--            &lt;/td&gt;
--        &lt;/tr&gt;
--    &lt;/tbody&gt;
--&lt;/table&gt;
--&lt;table width=&quot;100%&quot;&gt;
--    &lt;tbody&gt;
--        &lt;tr&gt;
--            &lt;td align=&quot;right&quot;&gt;
--            #BizDocSummary#
--            &lt;/td&gt;
--        &lt;/tr&gt;
--        &lt;tr&gt;
--            &lt;td&gt;
--            #FooterImage#
--            &lt;/td&gt;
--        &lt;/tr&gt;
--    &lt;/tbody&gt;
--&lt;/table&gt;','.title{color: #696969; /*padding:10px;*/font: bold 30px Arial, Helvetica, sans-serif;}
--.RowHeader{background-color: #dedede;color: #333;font-family: Arial;font-size: 8pt;}
--.RowHeader.hyperlink{color: #333;}
--.ItemHeader, .ItemStyle, .AltItemStyle{border-width: 1px;padding: 8px;font: normal 12px/17px arial;border-style: solid;border-color: #666666;background-color: #dedede;}
--.ItemHeader{border-width: 1px;padding: 8px;border-style: solid;border-color: #666666;}
--.ItemStyle td{border-width: 1px;padding: 8px;border-style: solid;border-color: #666666;background-color: #ffffff;}
--.AltItemStyle{background-color: White;border-color: black;padding: 8px;}
--.AltItemStyle td{padding: 8px;}
--.ItemHeader td{border-width: 1px;padding: 8px;font-weight: bold;border-style: solid;border-color: #666666;background-color: #dedede;}
--.ItemHeader td th{border-width: 1px;padding: 8px;border-style: solid;border-color: #666666;background-color: #dedede;}
--#tblBizDocSumm{font: normal 12px verdana;color: #333;border-width: 1px;border-color: #666666;border-collapse: collapse;background-color: #dedede;}
--#tblBizDocSumm td{border-width: 1px;padding: 8px;border-style: solid;border-color: #666666;}
--.WordWrapSerialNo{width: 30%;word-break: break-all;}',1,0,'Vendor Invoice',1,'&lt;table width=&quot;100%&quot; border=&quot;0&quot; cellpadding=&quot;0&quot;&gt;
--    &lt;tbody&gt;
--        &lt;tr&gt;
--            &lt;td valign=&quot;top&quot; align=&quot;left&quot;&gt;
--            #Logo#
--            &lt;/td&gt;
--            &lt;td align=&quot;right&quot; class=&quot;title&quot; style=&quot;white-space: nowrap;&quot;&gt;
--            #Customer/VendorOrganizationName#
--            &lt;/td&gt;
--            &lt;td align=&quot;right&quot; class=&quot;title&quot; style=&quot;white-space: nowrap;&quot;&gt;
--            #BizDocTemplateName#
--            &lt;/td&gt;
--        &lt;/tr&gt;
--    &lt;/tbody&gt;
--&lt;/table&gt;
--&lt;table width=&quot;100%&quot; border=&quot;0&quot; cellpadding=&quot;0&quot;&gt;
--    &lt;tbody&gt;
--        &lt;tr&gt;
--            &lt;td align=&quot;right&quot;&gt;
--            &lt;table width=&quot;100%&quot;&gt;
--                &lt;tbody&gt;
--                    &lt;tr&gt;
--                        &lt;td class=&quot;RowHeader&quot;&gt;
--                        Discount
--                        &lt;/td&gt;
--                        &lt;td class=&quot;RowHeader&quot;&gt;
--                        Billing Terms
--                        &lt;/td&gt;
--                        &lt;td class=&quot;RowHeader&quot;&gt;
--                        #ChangeDueDate#
--                        &lt;/td&gt;
--                        &lt;td class=&quot;RowHeader&quot;&gt;
--                        ID#
--                        &lt;/td&gt;
--                    &lt;/tr&gt;
--                    &lt;tr&gt;
--                        &lt;td class=&quot;normal1&quot;&gt;
--                        #Discount#
--                        &lt;/td&gt;
--                        &lt;td class=&quot;normal1&quot;&gt;
--                        #BillingTerms#
--                        &lt;/td&gt;
--                        &lt;td class=&quot;normal1&quot;&gt;
--                        #DueDate#
--                        &lt;/td&gt;
--                        &lt;td class=&quot;normal1&quot;&gt;
--                        #BizDocID#
--                        &lt;/td&gt;
--                    &lt;/tr&gt;
--                &lt;/tbody&gt;
--            &lt;/table&gt;
--            &lt;/td&gt;
--        &lt;/tr&gt;
--        &lt;tr&gt;
--            &lt;td&gt;
--            &lt;table height=&quot;100%&quot; width=&quot;100%&quot;&gt;
--                &lt;tbody&gt;
--                    &lt;tr&gt;
--                        &lt;td class=&quot;RowHeader&quot;&gt;
--                        #EmployerChangeBillToHeader#
--                        &lt;/td&gt;
--                        &lt;td class=&quot;RowHeader&quot;&gt;
--                        #EmployerChangeShipToHeader#
--                        &lt;/td&gt;
--                        &lt;td class=&quot;RowHeader&quot;&gt;
--                        Status
--                        &lt;/td&gt;
--                    &lt;/tr&gt;
--                    &lt;tr&gt;
--                        &lt;td class=&quot;normal1&quot;&gt;
--                        #Customer/VendorOrganizationName#&lt;br /&gt;
--                        #EmployerBillToAddress#
--                        &lt;/td&gt;
--                        &lt;td class=&quot;normal1&quot;&gt;
--                        #EmployerShipToAddress#
--                        &lt;/td&gt;
--                        &lt;td&gt;
--                        &lt;table height=&quot;100%&quot; width=&quot;100%&quot;&gt;
--                            &lt;tbody&gt;
--                                &lt;tr&gt;
--                                    &lt;td colspan=&quot;2&quot; class=&quot;normal1&quot;&gt;
--                                    #BizDocStatus#
--                                    &lt;/td&gt;
--                                &lt;/tr&gt;
--                                &lt;tr&gt;
--                                    &lt;td class=&quot;normal1&quot;&gt;
--                                    #AmountPaidPopUp#
--                                    &lt;/td&gt;
--                                    &lt;td class=&quot;normal1&quot;&gt;
--                                    #Currency#&amp;nbsp;#AmountPaid#
--                                    &lt;/td&gt;
--                                &lt;/tr&gt;
--                                &lt;tr&gt;
--                                    &lt;td class=&quot;normal1&quot;&gt;
--                                    Balance Due:
--                                    &lt;/td&gt;
--                                    &lt;td class=&quot;normal1&quot;&gt;
--                                    #Currency#&amp;nbsp;#BalanceDue#
--                                    &lt;/td&gt;
--                                &lt;/tr&gt;
--                            &lt;/tbody&gt;
--                        &lt;/table&gt;
--                        &lt;/td&gt;
--                    &lt;/tr&gt;
--                &lt;/tbody&gt;
--            &lt;/table&gt;
--            &lt;/td&gt;
--        &lt;/tr&gt;
--        &lt;tr class=&quot;normal1&quot;&gt;
--            &lt;td&gt;
--            &lt;table height=&quot;100%&quot; width=&quot;100%&quot;&gt;
--                &lt;tbody&gt;
--                    &lt;tr&gt;
--                        &lt;td class=&quot;RowHeader&quot;&gt;
--                        &lt;asp:label id=&quot;Label1&quot; runat=&quot;server&quot;&gt;&lt;/asp:label&gt;
--                        &amp;nbsp;#
--                        &lt;/td&gt;
--                        &lt;td class=&quot;RowHeader&quot;&gt;
--                        Deal or Order ID
--                        &lt;/td&gt;
--                    &lt;/tr&gt;
--                    &lt;tr&gt;
--                        &lt;td&gt;
--                        #P.O.NO#
--                        &lt;/td&gt;
--                        &lt;td class=&quot;normal1&quot;&gt;
--                        #OrderID#
--                        &lt;/td&gt;
--                    &lt;/tr&gt;
--                &lt;/tbody&gt;
--            &lt;/table&gt;
--            &lt;/td&gt;
--        &lt;/tr&gt;
--        &lt;tr&gt;
--            &lt;td class=&quot;RowHeader&quot;&gt;
--            Comments
--            &lt;/td&gt;
--        &lt;/tr&gt;
--        &lt;tr class=&quot;normal1&quot;&gt;
--            &lt;td&gt;
--            #Comments#
--            &lt;/td&gt;
--        &lt;/tr&gt;
--        &lt;tr class=&quot;normal1&quot;&gt;
--            &lt;td&gt;
--            #Products#
--            &lt;/td&gt;
--        &lt;/tr&gt;
--    &lt;/tbody&gt;
--&lt;/table&gt;
--&lt;table width=&quot;100%&quot;&gt;
--    &lt;tbody&gt;
--        &lt;tr&gt;
--            &lt;td align=&quot;right&quot;&gt;
--            #BizDocSummary#
--            &lt;/td&gt;
--        &lt;/tr&gt;
--        &lt;tr&gt;
--            &lt;td&gt;
--            #FooterImage#
--            &lt;/td&gt;
--        &lt;/tr&gt;
--    &lt;/tbody&gt;
--&lt;/table&gt;'
--		)

--		SET @I = @I  + 1
--	END
	
--	DROP TABLE #temp
--COMMIT
--END TRY
--BEGIN CATCH
--	IF @@TRANCOUNT > 0
--		ROLLBACK TRANSACTION;

--	SELECT 
--		ERROR_MESSAGE(),
--		ERROR_NUMBER(),
--		ERROR_SEVERITY(),
--		ERROR_STATE(),
--		ERROR_LINE(),
--		ISNULL(ERROR_PROCEDURE(), '-');
--END CATCH

-------------------------------------------------------

--SET IDENTITY_INSERT ListDetails ON
--INSERT INTO ListDetails
--( numListItemID,numListID,vcData,numCreatedBY,bitDelete,numDomainID,constFlag,sintOrder)
--VALUES
--(305,27,'Vendor Invoice',1,0,1,1,1)
--SET IDENTITY_INSERT ListDetails OFF

-----------------------------------------------

--UPDATE DycFieldMaster SET vcFieldType='R',vcFieldDataType='N' WHERE numFieldID=258

--INSERT INTO DynamicFormMaster
--(
--	numFormId,vcFormName,cCustomFieldsAssociated,cAOIAssociated,bitDeleted,tintFlag
--)
--VALUES
--(
--	135,'Purchase Fullfilment','N','N',0,1
--)

--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
--)
--VALUES
--(
--	4,211,135,0,0,'Item Code','Label',1,1,0,0,1,0,0
--)

--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
--)
--VALUES
--(
--	4,189,135,0,0,'Item to receive','TextBox',2,1,0,1,1,0,0
--)

--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
--)
--VALUES
--(
--	3,252,135,0,0,'Model ID','TextBox',3,1,0,0,1,0,0
--)

-------------------------

--DECLARE @numFieldID NUMERIC(18,0)
--INSERT INTO DycFieldMaster
--(
--	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType
--	,vcListItemType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitDetailField,bitAllowFiltering
--)
--VALUES
--(
--	3,'SKU','vcSKU','vcSKU','SKU','OpportunityItems','V','R','TextBox','',0,1,0,1,0,1,1,1
--)

--SET @numFieldID = SCOPE_IDENTITY()
--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
--)
--VALUES
--(
--	3,@numFieldID,135,0,0,'SKU','TextBox',4,1,0,0,1,0,0
--)

-------------------------------

--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
--)
--VALUES
--(
--	4,203,135,0,0,'UPC','TextBox',5,1,0,0,1,0,0
--)

--------------------------------------

--DECLARE @numFieldID NUMERIC(18,0)
--SELECT @numFieldID=numFieldId FROM DycFieldMaster WHERE vcOrigDbColumnName='vcNotes' AND vcLookBackTableName='OpportunityItems'
--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
--)
--VALUES
--(
--	3,@numFieldID,135,0,0,'Notes','TextArea',6,1,0,0,1,0,0
--)

------------------------------------------

--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
--)
--VALUES
--(
--	3,253,135,0,0,'Description','TextArea',7,1,0,0,1,0,0
--),
--(
--	3,348,135,0,0,'Unit of Measure','TextBox',8,1,0,0,1,0,0
--)

-------------------------------------------

--DECLARE @numFieldID NUMERIC(18,0)
--SELECT @numFieldID=numFieldId FROM DycFieldMaster WHERE vcOrigDbColumnName='vcAttributes' AND vcLookBackTableName='OpportunityItems'
--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
--)
--VALUES
--(
--	3,@numFieldID,135,0,0,'Attributes','TextBox',9,1,0,0,1,0,0
--)

----------------------------------

--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
--)
--VALUES
--(
--	4,352,135,0,0,'Image','TextBox',10,1,0,0,1,0,0
--)


------------------------------

--DECLARE @numFieldID NUMERIC(18,0)
--SELECT @numFieldID=numFieldId FROM DycFieldMaster WHERE vcOrigDbColumnName='SerialLotNo' AND vcLookBackTableName='OpportunityItems'
--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
--)
--VALUES
--(
--	3,@numFieldID,135,0,0,'Serial/Lot #s','TextBox',11,1,0,0,1,0,0
--)


----------------------------------------

--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
--)
--VALUES
--(
--	4,233,135,0,0,'Location','SelectBox',12,1,0,1,1,0,0
--),
--(
--	4,195,135,0,0,'On Hand','TextBox',13,1,0,1,1,1,0
--),
--(
--	4,196,135,0,0,'On Order','TextBox',14,1,0,1,1,1,0
--),
--(
--	4,197,135,0,0,'On Allocation','TextBox',15,1,0,0,1,1,0
--),
--(
--	4,198,135,0,0,'On Backorder','TextBox',16,1,0,0,1,1,0
--)

-------------------------------------------

--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
--)
--VALUES
--(
--	3,258,135,0,0,'Qty Ordered','TextBox',17,1,0,1,1,1,0
--),
--(
--	3,493,135,0,0,'Qty Received','TextBox',18,1,0,1,1,1,0
--)

--------------------------------------------------------------

--DECLARE @numFieldID NUMERIC(18,0)
--INSERT INTO DycFieldMaster
--(
--	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType
--	,vcListItemType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitDetailField,bitAllowFiltering
--)
--VALUES
--(
--	3,'Remaining Qty','numRemainingQty','numRemainingQty','RemainingQty','OpportunityItems','V','R','Label','',0,1,0,0,0,1,0,0
--)

--SET @numFieldID = SCOPE_IDENTITY()
--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
--)
--VALUES
--(
--	3,@numFieldID,135,0,0,'Remaining Qty','Label',19,1,0,1,1,0,0
--)

---------------------------------------------------------------------

--DECLARE @numFieldID NUMERIC(18,0)
--INSERT INTO DycFieldMaster
--(
--	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType
--	,vcListItemType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitDetailField,bitAllowFiltering
--)
--VALUES
--(
--	3,'Qty to Ship/Receive','numQtyToShipReceive','numQtyToShipReceive','QtyToShipReceive','OpportunityItems','V','R','TextBox','',0,1,0,0,0,1,0,0
--)

--SET @numFieldID = SCOPE_IDENTITY()
--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
--)
--VALUES
--(
--	3,@numFieldID,135,0,0,'Qty to Receive','TextBox',20,1,0,1,1,0,0
--)

--------------------------------------------------------------------------------

--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
--)
--VALUES
--(
--	1,3,135,0,0,'Vendor','TextBox',21,1,0,1,1,1,0
--)

-----------------------------------------------------------------------------------

--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
--)
--VALUES
--(
--	3,96,135,0,0,'P.O.','TextBox',22,1,0,1,1,0,0
--)

----------------------------------------
----------- Sales Fulfillment ----------
----------------------------------------

--DELETE FROM DycFormConfigurationDetails WHERE numFormId=23
--DELETE FROm DynamicFormField_Validation WHERE numFormId=23
--DELETE FROM DynamicFormConfigurationDetails WHERE numFormId=23
--DELETE FROM DycFormField_Mapping WHERE numFormID=23 AND numFieldID IN (51,52,195,196,197,198,199,251,252,253,254,255,258,265,108,116,117,109,119,243)

------------------

--UPDATE DycFormField_Mapping SET bitDefault = 0, bitAllowSorting=0, bitAllowFiltering=0 WHERE numFormID=23

------------------

--DECLARE @numFieldID NUMERIC(18,0)
--SELECT @numFieldID=numFieldId FROM DycFieldMaster WHERE vcOrigDbColumnName='dtReleaseDate' AND vcLookBackTableName='OpportunityMaster'
--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
--)
--VALUES
--(
--	3,@numFieldID,23,0,0,'Order Release Date','DateField',1,1,0,1,1,1,1
--)

------------------
--DECLARE @numFieldID NUMERIC(18,0)
--SELECT @numFieldID=numFieldId FROM DycFieldMaster WHERE vcOrigDbColumnName='vcBizDocID' AND vcLookBackTableName='OpportunityBizDocs'
--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
--)
--VALUES
--(
--	3,@numFieldID,23,0,0,'FO','TextBox',2,1,0,1,1,1,1
--)

------------------

--DECLARE @numFieldID NUMERIC(18,0)
--INSERT INTO DycFieldMaster
--(
--	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcListItemType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitAllowFiltering
--)
--VALUES
--(
--	3,'Priced/Boxed/Tracked','vcPricedBoxedTracked','vcPricedBoxedTracked','OpportunityMaster','V','R','Label','',0,1,0,0,0,1,0,0,0,0,0
--)
--SET @numFieldID = SCOPE_IDENTITY()

--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
--)
--VALUES
--(
--	3,@numFieldID,23,0,0,'Priced/Boxed/Tracked','Label',3,1,0,1,1,0,0
--)

--------------------------

--UPDATE DycFormField_Mapping SET vcFieldName='Order / Status',bitDefault=1, bitAllowSorting=1, bitAllowFiltering=1,[order]=4 WHERE numFormID=23 AND numFieldID=96
--UPDATE DycFormField_Mapping SET vcFieldName='Customer',bitDefault=1, bitAllowSorting=1, bitAllowFiltering=1,[order]=5 WHERE numFormID=23 AND numFieldID=3

---------------------------

--DECLARE @numFieldID NUMERIC(18,0)
--INSERT INTO DycFieldMaster
--(
--	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcListItemType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitAllowFiltering
--)
--VALUES
--(
--	3,'Fulfillment Completed','vcFulfillmentStatus','vcFulfillmentStatus','OpportunityMaster','V','R','Label','',0,1,0,0,0,1,0,0,0,0,0
--)
--SET @numFieldID = SCOPE_IDENTITY()

--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
--)
--VALUES
--(
--	3,@numFieldID,23,0,0,'Fulfillment Completed','Label',6,1,0,1,1,0,0
--)

-------------------------

--DECLARE @numFieldID NUMERIC(18,0)
--SELECT @numFieldID=numFieldId FROM DycFieldMaster WHERE vcOrigDbColumnName='vcInventoryStatus' AND vcLookBackTableName='OpportunityItems'
--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
--)
--VALUES
--(
--	3,@numFieldID,23,0,0,'Inventory Status','Label',7,1,0,0,1,0,1
--)

--------------------

--DECLARE @numFieldID NUMERIC(18,0)
--SELECT @numFieldID=numFieldId FROM DycFieldMaster WHERE vcOrigDbColumnName='numShipmentMethod' AND vcLookBackTableName='OpportunityMaster' AND numListID=82
--UPDATE DycFormField_Mapping SET numFieldID=@numFieldID,vcFieldName='Ship Via',[order]=8,bitAllowFiltering=1 WHERE numFieldID=246 AND numFormID=23

-----------------------------------------------

--DECLARE @numFieldID NUMERIC(18,0)
--SELECT @numFieldID=numFieldId FROM DycFieldMaster WHERE vcOrigDbColumnName='numDefaultShippingServiceID' AND vcLookBackTableName='DivisionMaster'
--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
--)
--VALUES
--(
--	3,@numFieldID,23,0,0,'Shipping Service','SelectBox',9,1,0,0,1,0,1
--)

--------------------------------------------------

--DECLARE @numFieldID NUMERIC(18,0)
--INSERT INTO DycFieldMaster
--(
--	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcListItemType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitAllowFiltering
--)
--VALUES
--(
--	1,'Shipping Instructions','vcDescription','vcDescription','DivisionMasterShippingConfiguration','V','R','TextBox','',0,1,0,1,0,1,0,0,0,0,0
--)
--SET @numFieldID = SCOPE_IDENTITY()

--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
--)
--VALUES
--(
--	3,@numFieldID,23,0,0,'Shipping Instructions','TextBox',10,1,0,0,1,0,0
--)

------------------------------------------------------


--DECLARE @numFieldID NUMERIC(18,0)
--INSERT INTO DycFieldMaster
--(
--	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcListItemType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitAllowFiltering
--)
--VALUES
--(
--	1,'Signature Type','vcSignatureType','vcSignatureType','DivisionMasterShippingConfiguration','N','R','SelectBox','SGT',0,1,0,1,0,1,0,0,1,0,1
--)
--SET @numFieldID = SCOPE_IDENTITY()

--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
--)
--VALUES
--(
--	3,@numFieldID,23,0,0,'Signature Type','SelectBox',11,1,0,0,1,0,1
--)


--------------------------------------------------------

--UPDATE DycFormField_Mapping SET [order]=12 WHERE numFieldID=111 AND numFormID=23
--UPDATE DycFormField_Mapping SET vcFieldName='Total Amount Paid',numFieldID=269,[order]=13 WHERE numFieldID=99 AND numFormID=23

----------------------------------------

--DECLARE @numFieldID NUMERIC(18,0)
--SELECT @numFieldID=numFieldId FROM DycFieldMaster WHERE vcOrigDbColumnName='numContactID' AND vcLookBackTableName='OpportunityMaster'
--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
--)
--VALUES
--(
--	3,@numFieldID,23,0,0,'Contact','SelectBox',14,1,0,0,1,0,0
--)

--------------------------------------------

--DECLARE @numFieldID NUMERIC(18,0)
--SELECT @numFieldID=numFieldId FROM DycFieldMaster WHERE vcOrigDbColumnName='vcOrderedShipped' AND vcLookBackTableName='OpportunityMaster'
--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
--)
--VALUES
--(
--	3,@numFieldID,23,0,0,'Ordered / Shipped','Label',15,1,0,0,1,0,0
--)


--------------------------------------------

--UPDATE DycFormField_Mapping SET [order]=16 WHERE numFieldID=97 AND numFormID=23

---------------------------------------------

--DECLARE @numFieldID NUMERIC(18,0)
--SELECT @numFieldID=numFieldId FROM DycFieldMaster WHERE vcOrigDbColumnName='numPartner' AND vcLookBackTableName='OpportunityMaster'
--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
--)
--VALUES
--(
--	3,@numFieldID,23,0,0,'Partner Source','SelectBox',17,1,0,0,1,0,1
--)

-----------------------------------------------------

--UPDATE DycFormField_Mapping SET [order]=18 WHERE numFieldID=100 AND numFormID=23
--UPDATE DycFormField_Mapping SET [order]=19 WHERE numFieldID=112 AND numFormID=23

------------------------------------------------------

--DECLARE @numFieldID NUMERIC(18,0)
--SELECT @numFieldID=numFieldId FROM DycFieldMaster WHERE vcOrigDbColumnName='vcOppRefOrderNo' AND vcLookBackTableName='OpportunityMaster'
--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
--)
--VALUES
--(
--	3,@numFieldID,23,0,0,'Customer PO#','TextBox',20,1,0,0,1,0,1
--)

--------------------------------------------------------

--DECLARE @numFieldID NUMERIC(18,0)
--SELECT @numFieldID=numFieldId FROM DycFieldMaster WHERE vcOrigDbColumnName='txtComments' AND vcLookBackTableName='OpportunityMaster'
--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
--)
--VALUES
--(
--	3,@numFieldID,23,0,0,'Sales Order Comments','TextArea',21,1,0,0,1,0,0
--)

------------------------------------------------------------

--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
--)
--VALUES
--(
--	3,219,23,0,0,'Ship To State','TextBox',22,1,0,0,1,0,1
--)


--UPDATE DycFormField_Mapping SET vcAssociatedControlType='Label' WHERE numFieldID=269 AND numFormID=23
-----------------------------------------------------------------------------
-----------------------------------------------------------------------------
-----------------------------------------------------------------------------

--ALTER TABLE DivisionMaster ADD bitShippingLabelRequired BIT

-----------------------------------------------------------------------------
-----------------------------------------------------------------------------
-----------------------------------------------------------------------------

--UPDATE CFw_Grp_Master SET Grp_Name='Default Settings' WHERE Grp_Name='Accounting' AND Loc_Id=13

-----------------------------------------------------------------------------
-----------------------------------------------------------------------------
-----------------------------------------------------------------------------

--ALTER TABLE Domain ADD bit3PL BIT DEFAULT 0


--ALTER TABLE [dbo].[OpportunityItems] ALTER COLUMN [monTotAmount] DECIMAL(20,5)
--ALTER TABLE [dbo].[OpportunityItems] ALTER COLUMN [monTotAmtBefDiscount] DECIMAL(20,5)

--ALTER TABLE [dbo].[OpportunityMaster] ALTER COLUMN [numPClosingPercent] DECIMAL(20,5)
--ALTER TABLE [dbo].[OpportunityMaster] DROP CONSTRAINT [DF_OpportunityMaster_monPAmount]
--GO
--ALTER TABLE [dbo].[OpportunityMaster] ALTER COLUMN [monPAmount] DECIMAL(20,5)
--ALTER TABLE [dbo].[OpportunityMaster] ADD  CONSTRAINT [DF_OpportunityMaster_monPAmount]  DEFAULT ((0)) FOR [monPAmount]
--GO
--ALTER TABLE [dbo].[OpportunityMaster] ALTER COLUMN [monDealAmount] DECIMAL(20,5)
--ALTER TABLE [dbo].[OpportunityMaster] ALTER COLUMN [monShipCost] DECIMAL(20,5)
--ALTER TABLE [dbo].[OpportunityMaster] ALTER COLUMN [monLandedCostTotal] DECIMAL(20,5)
--ALTER TABLE [dbo].[OpportunityMaster] ALTER COLUMN [monDealAmountWith4Decimals] DECIMAL(20,5)

--ALTER TABLE [dbo].[BizDocComission] ALTER COLUMN [monProTotalExpense] DECIMAL(20,5)
--ALTER TABLE [dbo].[BizDocComission] ALTER COLUMN [monProTotalIncome] DECIMAL(20,5)
--ALTER TABLE [dbo].[BizDocComission] ALTER COLUMN [monProTotalGrossProfit] DECIMAL(20,5)
--ALTER TABLE [dbo].[DepositeDetails] ALTER COLUMN [monAmountPaid] DECIMAL(20,5)

--ALTER TABLE [dbo].[BillPaymentHeader] ALTER COLUMN monPaymentAmount DECIMAL(20,5)
--ALTER TABLE [dbo].[BillPaymentHeader] ALTER COLUMN monAppliedAmount DECIMAL(20,5)
--ALTER TABLE [dbo].[BillPaymentHeader] ALTER COLUMN monRefundAmount DECIMAL(20,5)

--ALTER TABLE [dbo].[BankReconcileMaster] ALTER COLUMN monBeginBalance DECIMAL(20,5)
--ALTER TABLE [dbo].[BankReconcileMaster] ALTER COLUMN monEndBalance DECIMAL(20,5)
--ALTER TABLE [dbo].[BankReconcileMaster] ALTER COLUMN monServiceChargeAmount DECIMAL(20,5)
--ALTER TABLE [dbo].[BankReconcileMaster] ALTER COLUMN monInterestEarnedAmount DECIMAL(20,5)
--ALTER TABLE [dbo].[BankStatementHeader] ALTER COLUMN monAvailableBalance DECIMAL(20,5)
--ALTER TABLE [dbo].[BankStatementHeader] ALTER COLUMN monLedgerBalance DECIMAL(20,5)

--ALTER TABLE [dbo].[BankStatementTransactions] ALTER COLUMN monAmount DECIMAL(20,5)

--ALTER TABLE [dbo].[BillDetails] ALTER COLUMN monAmount DECIMAL(20,5)
--ALTER TABLE [dbo].[BillHeader] ALTER COLUMN monAmountDue DECIMAL(20,5)

--ALTER TABLE [dbo].[BillHeader] DROP CONSTRAINT [DF_BillHeader_monAmtPaid]
--GO
--ALTER TABLE [dbo].[BillHeader] ALTER COLUMN monAmtPaid DECIMAL(20,5)

--ALTER TABLE [dbo].[BillHeader] ADD  CONSTRAINT [DF_BillHeader_monAmtPaid]  DEFAULT ((0)) FOR [monAmtPaid]
--GO
--ALTER TABLE [dbo].[BillPaymentDetails] ALTER COLUMN monAmount DECIMAL(20,5)

--ALTER TABLE [dbo].[CampaignMaster] ALTER COLUMN monCampaignCost DECIMAL(20,5)
--ALTER TABLE [dbo].[CartItems] ALTER COLUMN monPrice DECIMAL(20,5)
--ALTER TABLE [dbo].[CartItems] ALTER COLUMN monTotAmtBefDiscount DECIMAL(20,5)
--ALTER TABLE [dbo].[CartItems] ALTER COLUMN decShippingCharge DECIMAL(20,5)
--ALTER TABLE [dbo].[CartItems] ALTER COLUMN monTotAmount DECIMAL(20,5)

--ALTER TABLE [dbo].[Chart_Of_Accounts] ALTER COLUMN numOriginalOpeningBal DECIMAL(20,5)
--ALTER TABLE [dbo].[Chart_Of_Accounts] ALTER COLUMN numOpeningBal DECIMAL(20,5)
--ALTER TABLE [dbo].[Chart_Of_Accounts] ALTER COLUMN monEndingOpeningBal DECIMAL(20,5)
--ALTER TABLE [dbo].[Chart_Of_Accounts] ALTER COLUMN monEndingBal DECIMAL(20,5)
--ALTER TABLE [dbo].[Chart_Of_Accounts] ALTER COLUMN monDepreciationCost DECIMAL(20,5)

--ALTER TABLE [dbo].[CheckDetails] ALTER COLUMN monAmount DECIMAL(20,5)
--DROP INDEX [IX_CheckHeader_numReferenceID] ON [dbo].[CheckHeader]
--GO
--ALTER TABLE [dbo].[CheckHeader] ALTER COLUMN monAmount DECIMAL(20,5)
--CREATE NONCLUSTERED INDEX [IX_CheckHeader_numReferenceID] ON [dbo].[CheckHeader]
--(
--	[numReferenceID] ASC,
--	[tintReferenceType] ASC,
--	[numCheckHeaderID] ASC
--)
--INCLUDE ( 	[vcMemo],
--	[monAmount],
--	[numCheckNo]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80)
--GO

--ALTER TABLE [dbo].[DepositMaster] ALTER COLUMN monDepositAmount DECIMAL(20,5)
--ALTER TABLE [dbo].[DepositMaster] ALTER COLUMN monAppliedAmount DECIMAL(20,5)
--ALTER TABLE [dbo].[DepositMaster] ALTER COLUMN monRefundAmount DECIMAL(20,5)
--ALTER TABLE [dbo].[DepositMaster] ALTER COLUMN monAppliedAmountWith4Decimals DECIMAL(20,5)
--ALTER TABLE [dbo].[DepositMaster] ALTER COLUMN monDepositAmountWith4Decimals DECIMAL(20,5)
--ALTER TABLE [dbo].[DepositMaster] ALTER COLUMN monRefundAmountWith4Decimals DECIMAL(20,5)

--ALTER TABLE [dbo].[General_Journal_Header] ALTER COLUMN numAmount DECIMAL(20,5)

--ALTER TABLE [dbo].[Item] ALTER COLUMN monListPrice DECIMAL(20,5)
--ALTER TABLE [dbo].[Item] ALTER COLUMN monAverageCost DECIMAL(20,5)
--ALTER TABLE [dbo].[Item] ALTER COLUMN monCampaignLabourCost DECIMAL(20,5)

--ALTER TABLE [dbo].[OpportunityBizDocDtl] ALTER COLUMN monItemPrice DECIMAL(20,5)
--ALTER TABLE [dbo].[OpportunityBizDocDtl] ALTER COLUMN monAmount DECIMAL(20,5)
--ALTER TABLE [dbo].[OpportunityBizDocDtl] ALTER COLUMN monTax DECIMAL(20,5)

--ALTER TABLE [dbo].[OpportunityBizDocItems] ALTER COLUMN monTotAmount DECIMAL(20,5)
--ALTER TABLE [dbo].[OpportunityBizDocItems] ALTER COLUMN monTotAmtBefDiscount DECIMAL(20,5)
--ALTER TABLE [dbo].[OpportunityBizDocItems] DROP CONSTRAINT [DF_OpportunityBizDocItems_monEmbeddedCostPerItem]
--GO
--ALTER TABLE [dbo].[OpportunityBizDocItems] ALTER COLUMN monEmbeddedCost DECIMAL(20,5)
--ALTER TABLE [dbo].[OpportunityBizDocItems] ADD  CONSTRAINT [DF_OpportunityBizDocItems_monEmbeddedCostPerItem]  DEFAULT ((0)) FOR [monEmbeddedCost]
--GO
--ALTER TABLE [dbo].[OpportunityBizDocItems] ALTER COLUMN monShipCost DECIMAL(20,5)

--ALTER TABLE [dbo].[OpportunityBizDocs] DROP CONSTRAINT [DF_OpportunityBizDocs_monAmountPaid]
--GO
--ALTER TABLE [dbo].[OpportunityBizDocs] ALTER COLUMN monAmountPaid DECIMAL(20,5)
--ALTER TABLE [dbo].[OpportunityBizDocs] ADD  CONSTRAINT [DF_OpportunityBizDocs_monAmountPaid]  DEFAULT ((0)) FOR [monAmountPaid]
--GO
--ALTER TABLE [dbo].[OpportunityBizDocs] ALTER COLUMN monShipCost DECIMAL(20,5)
--ALTER TABLE [dbo].[OpportunityBizDocs] ALTER COLUMN monDealAmount DECIMAL(20,5)
--ALTER TABLE [dbo].[OpportunityBizDocs] DROP CONSTRAINT [DF_OpportunityBizDocs_monEmbeddedCostPerItem]
--GO
--ALTER TABLE [dbo].[OpportunityBizDocs] ALTER COLUMN monEmbeddedCostPerItem DECIMAL(20,5)
--ALTER TABLE [dbo].[OpportunityBizDocs] ADD  CONSTRAINT [DF_OpportunityBizDocs_monEmbeddedCostPerItem]  DEFAULT ((0)) FOR [monEmbeddedCostPerItem]
--GO
--ALTER TABLE [dbo].[OpportunityBizDocs] ALTER COLUMN monCreditAmount DECIMAL(20,5)
--ALTER TABLE [dbo].[OpportunityBizDocs] ALTER COLUMN monDealAmountWith4Decimals DECIMAL(20,5)

--ALTER TABLE [dbo].[TransactionHistory] ALTER COLUMN monAuthorizedAmt DECIMAL(20,5)
--ALTER TABLE [dbo].[TransactionHistory] ALTER COLUMN monCapturedAmt DECIMAL(20,5)
--ALTER TABLE [dbo].[TransactionHistory] ALTER COLUMN monRefundAmt DECIMAL(20,5)
--ALTER TABLE [dbo].[Vendor] ALTER COLUMN monCost DECIMAL(20,5)

--ALTER TABLE [dbo].[TimeAndExpense] ALTER COLUMN monAmount DECIMAL(20,5)
--ALTER TABLE [dbo].[TimeExpAddAmount] ALTER COLUMN monAmount DECIMAL(20,5)

--ALTER TABLE [dbo].[TempARRecord1] ALTER COLUMN numThirtyDays DECIMAL(20,5)
--ALTER TABLE [dbo].[TempARRecord1] ALTER COLUMN numSixtyDays DECIMAL(20,5)
--ALTER TABLE [dbo].[TempARRecord1] ALTER COLUMN numNinetyDays DECIMAL(20,5)
--ALTER TABLE [dbo].[TempARRecord1] ALTER COLUMN numOverNinetyDays DECIMAL(20,5)
--ALTER TABLE [dbo].[TempARRecord1] ALTER COLUMN numThirtyDaysOverDue DECIMAL(20,5)
--ALTER TABLE [dbo].[TempARRecord1] ALTER COLUMN numSixtyDaysOverDue DECIMAL(20,5)
--ALTER TABLE [dbo].[TempARRecord1] ALTER COLUMN numNinetyDaysOverDue DECIMAL(20,5)
--ALTER TABLE [dbo].[TempARRecord1] ALTER COLUMN numOverNinetyDaysOverDue DECIMAL(20,5)
--ALTER TABLE [dbo].[TempARRecord1] ALTER COLUMN numThirtyDaysPaid DECIMAL(20,5)
--ALTER TABLE [dbo].[TempARRecord1] ALTER COLUMN numSixtyDaysPaid DECIMAL(20,5)
--ALTER TABLE [dbo].[TempARRecord1] ALTER COLUMN numNinetyDaysPaid DECIMAL(20,5)
--ALTER TABLE [dbo].[TempARRecord1] ALTER COLUMN numOverNinetyDaysPaid DECIMAL(20,5)
--ALTER TABLE [dbo].[TempARRecord1] ALTER COLUMN numThirtyDaysOverDuePaid DECIMAL(20,5)
--ALTER TABLE [dbo].[TempARRecord1] ALTER COLUMN numSixtyDaysOverDuePaid DECIMAL(20,5)
--ALTER TABLE [dbo].[TempARRecord1] ALTER COLUMN numNinetyDaysOverDuePaid DECIMAL(20,5)
--ALTER TABLE [dbo].[TempARRecord1] ALTER COLUMN numOverNinetyDaysOverDuePaid DECIMAL(20,5)
--ALTER TABLE [dbo].[TempARRecord1] ALTER COLUMN monUnAppliedAmount DECIMAL(20,5)
--ALTER TABLE [dbo].[TempARRecord1] ALTER COLUMN numTotal DECIMAL(20,5)

--ALTER TABLE [dbo].[TempARRecord] ALTER COLUMN DealAmount DECIMAL(20,5)
--ALTER TABLE [dbo].[TempARRecord] ALTER COLUMN AmountPaid DECIMAL(20,5)
--ALTER TABLE [dbo].[TempARRecord] ALTER COLUMN monUnAppliedAmount DECIMAL(20,5)

--ALTER TABLE [dbo].[WareHouseItems] ALTER COLUMN monWListPrice DECIMAL(20,5)
--ALTER TABLE [dbo].[WareHouseItems_Tracking] ALTER COLUMN monAverageCost DECIMAL(20,5)

--ALTER TABLE [dbo].[ReturnHeader] ALTER COLUMN monAmount DECIMAL(20,5)
--ALTER TABLE [dbo].[ReturnHeader] ALTER COLUMN monBizDocAmount DECIMAL(20,5)
--ALTER TABLE [dbo].[ReturnHeader] ALTER COLUMN monBizDocUsedAmount DECIMAL(20,5)
--ALTER TABLE [dbo].[ReturnHeader] ALTER COLUMN monTotalTax DECIMAL(20,5)
--ALTER TABLE [dbo].[ReturnHeader] ALTER COLUMN monTotalDiscount DECIMAL(20,5)
--ALTER TABLE [dbo].[ReturnHeader] ALTER COLUMN monAmountWith4Decimals DECIMAL(20,5)
--ALTER TABLE [dbo].[ReturnHeader] ALTER COLUMN monBizDocAmountWith4Decimals DECIMAL(20,5)
--ALTER TABLE [dbo].[ReturnHeader] ALTER COLUMN monBizDocUsedAmountWith4Decimals DECIMAL(20,5)
--ALTER TABLE [dbo].[ReturnHeader] ALTER COLUMN monTotalTaxWith4Decimals DECIMAL(20,5)
--ALTER TABLE [dbo].[ReturnHeader] ALTER COLUMN monTotalDiscountWith4Decimals DECIMAL(20,5)

--ALTER TABLE [dbo].[ReturnItems] ALTER COLUMN monTotAmount DECIMAL(20,5)
--ALTER TABLE [dbo].[ReturnItems] ALTER COLUMN monVendorCost DECIMAL(20,5)
--ALTER TABLE [dbo].[ReturnItems] ALTER COLUMN monAverageCost DECIMAL(20,5)

--ALTER TABLE [dbo].[OpportunityItems] DROP CONSTRAINT [DF__Opportuni__monVe__781663CB]
--GO
--ALTER TABLE [dbo].[OpportunityItems] ALTER COLUMN monVendorCost DECIMAL(20,5)
--ALTER TABLE [dbo].[OpportunityItems] ADD  DEFAULT ((0)) FOR [monVendorCost]
--GO
--ALTER TABLE [dbo].[OpportunityItems] ALTER COLUMN monAvgCost DECIMAL(20,5)
--ALTER TABLE [dbo].[OpportunityItems] ALTER COLUMN monLandedCost DECIMAL(20,5)
--ALTER TABLE [dbo].[OpportunityItems] ALTER COLUMN monAvgLandedCost DECIMAL(20,5)
--ALTER TABLE [dbo].[OpportunityKitChildItems] ALTER COLUMN monAvgCost DECIMAL(20,5)
--ALTER TABLE [dbo].[OpportunityKitItems] ALTER COLUMN monAvgCost DECIMAL(20,5)

--ALTER TABLE [dbo].[OpportunityBizDocsDetails] ALTER COLUMN monAmount DECIMAL(20,5)
--ALTER TABLE [dbo].[OpportunityBizDocsPaymentDetails] ALTER COLUMN monAmount DECIMAL(20,5)

--ALTER TABLE DivisionMaster ALTER COLUMN monPCreditBalance DECIMAL(20,5)
--ALTER TABLE DivisionMaster ALTER COLUMN monSCreditBalance DECIMAL(20,5)
--ALTER TABLE Domain ALTER COLUMN monAmountPastDue DECIMAL(20,5)

--ALTER TABLE ProjectsMaster ALTER COLUMN monTotalExpense DECIMAL(20,5)
--ALTER TABLE ProjectsMaster ALTER COLUMN monTotalIncome DECIMAL(20,5)
--ALTER TABLE ProjectsMaster ALTER COLUMN monTotalGrossProfit DECIMAL(20,5)

--ALTER TABLE SalesTemplateItems ALTER COLUMN monPrice DECIMAL(20,5)
--ALTER TABLE SalesTemplateItems ALTER COLUMN monTotAmount DECIMAL(20,5)
--ALTER TABLE SalesTemplateItems ALTER COLUMN monTotAmtBefDiscount DECIMAL(20,5)

--ALTER TABLE ShippingPromotions ALTER COLUMN monFixShipping1OrderAmount DECIMAL(20,5)
--ALTER TABLE ShippingPromotions ALTER COLUMN monFixShipping1Charge DECIMAL(20,5)
--ALTER TABLE ShippingPromotions ALTER COLUMN monFixShipping2OrderAmount DECIMAL(20,5)
--ALTER TABLE ShippingPromotions ALTER COLUMN monFixShipping2Charge DECIMAL(20,5)
--ALTER TABLE ShippingPromotions ALTER COLUMN monFreeShippingOrderAmount DECIMAL(20,5)
--ALTER TABLE ShippingReportItems ALTER COLUMN monShippingRate DECIMAL(20,5)

--ALTER TABLE UserMaster ALTER COLUMN monHourlyRate DECIMAL(20,5)
--ALTER TABLE UserMaster ALTER COLUMN monOverTimeRate DECIMAL(20,5)
--ALTER TABLE UserMaster ALTER COLUMN monOverTimeWeeklyRate DECIMAL(20,5)
--ALTER TABLE UserMaster ALTER COLUMN monNetPay DECIMAL(20,5)
--ALTER TABLE UserMaster ALTER COLUMN monAmtWithheld DECIMAL(20,5)

--ALTER TABLE ShippingBox ALTER COLUMN monShippingRate DECIMAL(20,5)
--ALTER TABLE ShippingServiceTypes ALTER COLUMN monRate DECIMAL(20,5)

--ALTER TABLE [dbo].[StagePercentageDetails] DROP CONSTRAINT [DF_StagePercentageDetails_monTimeBudget]
--ALTER TABLE [dbo].[StagePercentageDetails] DROP CONSTRAINT [DF_StagePercentageDetails_monExpenseBudget]
--GO
--ALTER TABLE StagePercentageDetails ALTER COLUMN monExpenseBudget DECIMAL(20,5)
--ALTER TABLE StagePercentageDetails ALTER COLUMN monTimeBudget DECIMAL(20,5)
--ALTER TABLE [dbo].[StagePercentageDetails] ADD  CONSTRAINT [DF_StagePercentageDetails_monExpenseBudget]  DEFAULT ((0)) FOR [monExpenseBudget]
--ALTER TABLE [dbo].[StagePercentageDetails] ADD  CONSTRAINT [DF_StagePercentageDetails_monTimeBudget]  DEFAULT ((0)) FOR [monTimeBudget]
--GO

--ALTER TABLE WorkOrder ALTER COLUMN monAverageCost DECIMAL(20,5)
--ALTER TABLE WorkOrderDetails ALTER COLUMN monAverageCost DECIMAL(20,5)

--SELECT TABLE_NAME,COLUMN_NAME,DATA_TYPE FROM INFORMATION_SCHEMA.COLUMNS WHERE DATA_TYPE='money' ORDER BY TABLE_NAME
-----------------------------------------------------------------------------
-----------------------------------------------------------------------------
-----------------------------------------------------------------------------

-- SET IDENTITY_INSERT ListMaster ON;

--INSERT INTO ListMaster 
--(
--	numListID,vcListName,bitDeleted,bitFixed,numDomainID,bitFlag,vcDataType,numModuleID,numCreatedBy
--)
--VALUES
--(
--	51,'EDI Status',0,1,1,1,'string',2,1
--)

--SET IDENTITY_INSERT ListMaster OFF;

--INSERT INTO ListDetails
--(
--	numListID,vcData,numCreatedBY,bitDelete,numDomainID,constFlag
--)
--VALUES
--(
--	51,'850 Received',1,0,1,1
--)
--,
--(
--	51,'850 Acknowledged',1,0,1,1
--)
--,
--(
--	51,'940 Sent',1,0,1,1
--)
--,
--(
--	51,'940 Acknowledged',1,0,1,1
--)
--,
--(
--	51,'945 Received',1,0,1,1
--)
--,
--(
--	51,'945 Acknowledged',1,0,1,1
--)

---------------------------------------------------------

--ALTER TABLE OpportunityMaster ADD tintEDIStatus TINYINT

-------------------------------------------------------------

--BEGIN TRY
--BEGIN TRANSACTION

--	DECLARE @numFieldID NUMERIC(18,0)

--	INSERT INTO DycFieldMaster
--	(
--		numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcListItemType,numListID,tintRow,tintColumn,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitDetailField,bitAllowSorting,bitAllowFiltering
--	)
--	VALUES
--	(
--		3,'EDI Status','tintEDIStatus','tintEDIStatus','EDIStatus','OpportunityMaster','N','R','SelectBox','',0,1,1,1,0,0,0,1,1,1,1
--	)

--	SET @numFieldID = SCOPE_IDENTITY()

--	INSERT INTO DycFormField_Mapping
--	(
--		numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering
--	)
--	VALUES
--	(
--		3,@numFieldID,39,0,0,'EDI Status','SelectBox','EDIStatus',1,1,1,0,0,1,0,1,1,1
--	)
--COMMIT
--END TRY
--BEGIN CATCH
--	IF @@TRANCOUNT > 0
--		ROLLBACK TRANSACTION;

--	SELECT 
--		ERROR_MESSAGE(),
--		ERROR_NUMBER(),
--		ERROR_SEVERITY(),
--		ERROR_STATE(),
--		ERROR_LINE(),
--		ISNULL(ERROR_PROCEDURE(), '-');
--END CATCH

--ALTER TABLE NameTemplate ADD dtCreatedDate DATETIME

--------------------------------------------------

--SET IDENTITY_INSERT dbo.ListDetails ON;
--INSERT INTO ListDetails
--(
--	numListItemID,numListID,vcData,numCreatedBY,bintCreatedDate,numModifiedBy,bintModifiedDate,bitDelete,numDomainID,constFlag,sintOrder,numListType,tintOppOrOrder
--)
--VALUES
--(
--	15445,176,'Send Shipment Request (940)',1,GETUTCDATE(),1,GETUTCDATE(),0,1,1,1,1,2
--)

--INSERT INTO ListDetails
--(
--	numListItemID,numListID,vcData,numCreatedBY,bintCreatedDate,numModifiedBy,bintModifiedDate,bitDelete,numDomainID,constFlag,sintOrder,numListType,tintOppOrOrder
--)
--VALUES
--(
--	15446,176,'Shipment Request (940) Failed',1,GETUTCDATE(),1,GETUTCDATE(),0,1,1,1,1,2
--)

--INSERT INTO ListDetails
--(
--	numListItemID,numListID,vcData,numCreatedBY,bintCreatedDate,numModifiedBy,bintModifiedDate,bitDelete,numDomainID,constFlag,sintOrder,numListType,tintOppOrOrder
--)
--VALUES
--(
--	15447,176,'Send 856',1,GETUTCDATE(),1,GETUTCDATE(),0,1,1,1,1,2
--)

--INSERT INTO ListDetails
--(
--	numListItemID,numListID,vcData,numCreatedBY,bintCreatedDate,numModifiedBy,bintModifiedDate,bitDelete,numDomainID,constFlag,sintOrder,numListType,tintOppOrOrder
--)
--VALUES
--(
--	15448,176,'Send 856 Failed',1,GETUTCDATE(),1,GETUTCDATE(),0,1,1,1,1,2
--)

--SET IDENTITY_INSERT dbo.ListDetails OFF;

--------------------------------------------------------------------------------

--BEGIN TRY
--BEGIN TRANSACTION
--	SELECT 
--		* 
--	INTO 
--		#temp 
--	FROM
--	(
--		SELECT 
--			ROW_NUMBER() OVER(ORDER BY numDomainId ASC) AS Row, 
--			numDomainId 
--		FROM 
--			Domain 
--		WHERE 
--			numDomainId > 0
--	) TABLE2
	
--	DECLARE @RowCount INT
--	SET @RowCount = (SELECT COUNT(Row) FROM #temp) 
	
--	DECLARE @I INT
--	DECLARE @numDomainId NUMERIC(18,0)
	
--	SET @I = 1

--	WHILE (@I <= @RowCount)
--	BEGIN
			
--		SELECT @numDomainId = numDomainID FROM #temp WHERE Row = @I
	
--		EXEC USP_DomainSFTPDetail_Generate @numDomainId

--		SET @I = @I  + 1
--	END
	
--	DROP TABLE #temp
--COMMIT
--END TRY
--BEGIN CATCH
--	IF @@TRANCOUNT > 0
--		ROLLBACK TRANSACTION;

--	SELECT 
--		ERROR_MESSAGE(),
--		ERROR_NUMBER(),
--		ERROR_SEVERITY(),
--		ERROR_STATE(),
--		ERROR_LINE(),
--		ISNULL(ERROR_PROCEDURE(), '-');
--END CATCH


--ALTER TABLE Domain ADD bitEDI BIT DEFAULT 0
--ALTER TABLE ShippingReport ADD bitSendToTP2 BIT
-----------------------------------------------------------------------------
-----------------------------------------------------------------------------
-----------------------------------------------------------------------------

--ALTER TABLE OpportunityBizDocItems ALTER COLUMN vcNotes VARCHAR(1000)


--UPDATE ListMaster SET vcListName='Status' WHERE numListID=176

--ALTER TABLE ListDetails ADD tintOppOrOrder TINYINT

--UPDATE DycFormField_Mapping SET bitWorkFlowField=0 WHERE numFormID=68 AND numFieldID IN (82,83,84,85,86,222,473,24,386,38,32,10,387,217,17,47,48,49,50)
--UPDATE DycFormField_Mapping SET bitWorkFlowField=0 WHERE numFormID=69 AND numFieldID IN (58,77,75,76,78,79,94,54,61,92,53,63,51,62,52,59,60,67,71,74,80,82,83,84,85,86)
--UPDATE DycFormField_Mapping SET bitWorkFlowField=0 WHERE numFormID=70 AND numFieldID IN (99,122,448,464,691,96,694)
--UPDATE DycFormField_Mapping SET bitWorkFlowField=0 WHERE numFormID=124 AND numFieldID IN (547,546)

--UPDATE DycFormField_Mapping SET bitWorkFlowField=0 WHERE numFormID=72 AND numFieldID IN (139,128,369,145,494,144,132)
--UPDATE DycFormField_Mapping SET bitWorkFlowField=0 WHERE numFormID=73 AND numFieldID IN (163,155,152,154,149,146,148,147,162)
--UPDATE DycFormField_Mapping SET bitWorkFlowField=0 WHERE numFormID=49 AND numFieldID IN (268)





--UPDATE DycFieldMaster SET vcGroup='Org. | Accounting Fields' WHERE numFieldId IN (31,417,418)


--UPDATE DycFieldMaster SET vcGroup='Org/Accounting sub-tab' WHERE vcGroup='Org. | Accounting Fields'
--UPDATE DycFieldMaster SET vcGroup='Organization Detail Fields' WHERE vcGroup='Org.Details Fields'
--UPDATE DycFieldMaster SET bitWorkFlowField=1,vcGroup='Org/Accounting sub-tab' WHERE vcFieldName='Class' AND numModuleID = 1 AND vcOrigDbColumnName='numAccountClassID' AND vcLookBackTableName='DivisionMaster'
--UPDATE DycFieldMaster SET bitWorkFlowField=1,vcGroup='Org/Accounting sub-tab' WHERE vcFieldName='Price Level' AND numModuleID = 1 AND vcOrigDbColumnName='tintPriceLevel' AND vcLookBackTableName='DivisionMaster'
--UPDATE DycFieldMaster SET bitWorkFlowField=1,vcGroup='Org/Accounting sub-tab' WHERE numFieldId=538
--UPDATE DycFieldMaster SET bitWorkFlowField=1,vcGroup='Org/Accounting sub-tab' WHERE vcFieldName='Ship Via' AND numModuleID = 1 AND vcOrigDbColumnName='intShippingCompany' AND vcLookBackTableName='DivisionMaster'



---------------------------------------

--DECLARE @numFieldID NUMERIC(18,0)
--SELECT @numFieldID=numFieldId FROM DycFieldMaster WHERE vcFieldName='Class' AND numModuleID = 1 AND vcOrigDbColumnName='numAccountClassID' AND vcLookBackTableName='DivisionMaster'

--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitImport,bitExport,bitAllowFiltering,intSectionID,bitAllowGridColor
--)
--VALUES
--(
--	1,@numFieldID,68,1,0,'Class','SelectBox','AccountClass',1,1,1,0,0,0,0,0,0,1,0,0,0,1,0
--)

---------------------------------------

--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitImport,bitExport,bitAllowFiltering,intSectionID,bitAllowGridColor
--)
--VALUES
--(
--	3,573,68,1,0,'Net Terms','SelectBox','BillingDays',1,1,1,0,0,0,0,0,0,1,0,0,0,1,0
--)

---------------------------------------

--DECLARE @numFieldID NUMERIC(18,0)
--SELECT @numFieldID=numFieldId FROM DycFieldMaster WHERE vcFieldName='Price Level' AND numModuleID = 1 AND vcOrigDbColumnName='tintPriceLevel' AND vcLookBackTableName='DivisionMaster'

--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitImport,bitExport,bitAllowFiltering,intSectionID,bitAllowGridColor
--)
--VALUES
--(
--	1,@numFieldID,68,1,0,'Item Price Level','SelectBox','PriceLevel',1,1,1,0,0,0,0,0,0,1,0,0,0,1,0
--)

---------------------------------------

--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitImport,bitExport,bitAllowFiltering,intSectionID,bitAllowGridColor
--)
--VALUES
--(
--	1,538,68,1,0,'Preferred Payment Method','SelectBox','DefaultPaymentMethod',1,1,1,0,0,0,0,0,0,1,0,0,0,1,0
--)
--------------------------------------

--DECLARE @numFieldID NUMERIC(18,0)
--SELECT @numFieldID=numFieldId FROM DycFieldMaster WHERE vcFieldName='Ship Via' AND numModuleID = 1 AND vcOrigDbColumnName='intShippingCompany' AND vcLookBackTableName='DivisionMaster'

--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitImport,bitExport,bitAllowFiltering,intSectionID,bitAllowGridColor
--)
--VALUES
--(
--	1,@numFieldID,68,1,0,'Preferred Ship Via','SelectBox','ShippingCompany',1,1,1,0,0,0,0,0,0,1,0,0,0,1,0
--)

-------------------------------

--DECLARE @numFieldID NUMERIC(18,0)
--INSERT INTO DycFieldMaster
--(
--	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcListItemType,numListID,[order],bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitWorkFlowField,bitAllowFiltering,vcGroup
--)
--VALUES
--(
--	1,'Preferred Parcel Shipping Service','numDefaultShippingServiceID','numDefaultShippingServiceID','DefaultShippingService','DivisionMaster','N','R','SelectBox','PSS',0,1,1,0,1,0,1,1,1,'Org/Accounting sub-tab'
--)
--SELECT @numFieldID = SCOPE_IDENTITY()


--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitImport,bitExport,bitAllowFiltering,intSectionID,bitAllowGridColor
--)
--VALUES
--(
--	1,@numFieldID,68,1,0,'Preferred Parcel Shipping Service','SelectBox','DefaultShippingService',1,1,1,0,0,0,0,0,0,1,0,0,0,1,0
--)

-----------------------------
--DECLARE @numClassFieldID NUMERIC(18,0)
--DECLARE @numShipViaFieldID NUMERIC(18,0)
--DECLARE @numParcelServiceFieldID NUMERIC(18,0)
--DECLARE @numPriceLevelFieldID NUMERIC(18,0)
--SELECT @numClassFieldID=numFieldId FROM DycFieldMaster WHERE vcFieldName='Class' AND numModuleID = 1 AND vcOrigDbColumnName='numAccountClassID' AND vcLookBackTableName='DivisionMaster'
--SELECT @numShipViaFieldID=numFieldId FROM DycFieldMaster WHERE vcFieldName='Ship Via' AND numModuleID = 1 AND vcOrigDbColumnName='intShippingCompany' AND vcLookBackTableName='DivisionMaster'
--SELECT @numParcelServiceFieldID=numFieldId FROM DycFieldMaster WHERE vcFieldName='Preferred Parcel Shipping Service' AND numModuleID = 1 AND vcOrigDbColumnName='numDefaultShippingServiceID' AND vcLookBackTableName='DivisionMaster'
--SELECT @numPriceLevelFieldID=numFieldId FROM DycFieldMaster WHERE vcFieldName='Price Level' AND numModuleID = 1 AND vcOrigDbColumnName='tintPriceLevel' AND vcLookBackTableName='DivisionMaster'



--DELETE D1 FROM DycFormField_Mapping D1 INNER JOIN DycFieldMaster ON D1.numFieldID=DycFieldMaster.numFieldId WHERE numFormID IN (69,70,72,73,124,49) AND vcGroup IN ('Organization Detail Fields','Org/Accounting sub-tab')

--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitImport,bitExport,bitAllowFiltering
--)
--SELECT
--	1,numFieldID,69,1,0,vcFieldName,vcAssociatedControlType,1,1,0,0,0,0,0,0,1,0,0,0
--FROM
--	DycFormField_Mapping
--WHERE
--	numFormID=68
--	AND numFieldID IN (16,14,224,226,7,15,18,19,28,22,3,30,29,5,6,451,221,219,677,677,@numClassFieldID,418,573,538,@numShipViaFieldID,@numParcelServiceFieldID,678,417,@numPriceLevelFieldID)

--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitImport,bitExport,bitAllowFiltering
--)
--SELECT
--	1,numFieldID,70,1,0,vcFieldName,vcAssociatedControlType,1,1,0,0,0,0,0,0,1,0,0,0
--FROM
--	DycFormField_Mapping
--WHERE
--	numFormID=68
--	AND numFieldID IN (16,14,224,226,7,15,18,19,28,22,3,30,29,5,6,451,221,219,677,677,@numClassFieldID,418,573,538,@numShipViaFieldID,@numParcelServiceFieldID,678,417,@numPriceLevelFieldID)

--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitImport,bitExport,bitAllowFiltering
--)
--SELECT
--	1,numFieldID,72,1,0,vcFieldName,vcAssociatedControlType,1,1,0,0,0,0,0,0,1,0,0,0
--FROM
--	DycFormField_Mapping
--WHERE
--	numFormID=68
--	AND numFieldID IN (16,14,224,226,7,15,18,19,28,22,3,30,29,5,6,451,221,219,677,677,@numClassFieldID,418,573,538,@numShipViaFieldID,@numParcelServiceFieldID,678,417,@numPriceLevelFieldID)

--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitImport,bitExport,bitAllowFiltering
--)
--SELECT
--	1,numFieldID,73,1,0,vcFieldName,vcAssociatedControlType,1,1,0,0,0,0,0,0,1,0,0,0
--FROM
--	DycFormField_Mapping
--WHERE
--	numFormID=68
--	AND numFieldID IN (16,14,224,226,7,15,18,19,28,22,3,30,29,5,6,451,221,219,677,677,@numClassFieldID,418,573,538,@numShipViaFieldID,@numParcelServiceFieldID,678,417,@numPriceLevelFieldID)

--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitImport,bitExport,bitAllowFiltering
--)
--SELECT
--	1,numFieldID,124,1,0,vcFieldName,vcAssociatedControlType,1,1,0,0,0,0,0,0,1,0,0,0
--FROM
--	DycFormField_Mapping
--WHERE
--	numFormID=68
--	AND numFieldID IN (16,14,224,226,7,15,18,19,28,22,3,30,29,5,6,451,221,219,677,677,@numClassFieldID,418,573,538,@numShipViaFieldID,@numParcelServiceFieldID,678,417,@numPriceLevelFieldID)

--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitImport,bitExport,bitAllowFiltering
--)
--SELECT
--	1,numFieldID,49,1,0,vcFieldName,vcAssociatedControlType,1,1,0,0,0,0,0,0,1,0,0,0
--FROM
--	DycFormField_Mapping
--WHERE
--	numFormID=68
--	AND numFieldID IN (16,14,224,226,7,15,18,19,28,22,3,30,29,5,6,451,221,219,677,677,@numClassFieldID,418,573,538,@numShipViaFieldID,@numParcelServiceFieldID,678,417,@numPriceLevelFieldID)

------------------------

--DECLARE @numFieldPartner NUMERIC(18,0)
--DECLARE @numFieldPartnerContact NUMERIC(18,0)
--DECLARE @numFieldReleaseDate NUMERIC(18,0)
--DECLARE @numFieldReleaseStatus NUMERIC(18,0)
--DECLARE @numFieldShipVia NUMERIC(18,0)
--DECLARE @numFieldInventoryStatus NUMERIC(18,0)
--DECLARE @numFieldPerComplete NUMERIC(18,0)

--SELECT @numFieldPartner=numFieldId FROM DycFieldMaster WHERE vcOrigDbColumnName='numPartner' AND vcLookBackTableName='OpportunityMaster'
--SELECT @numFieldPartnerContact=numFieldId FROM DycFieldMaster WHERE vcOrigDbColumnName='numPartenerContact' AND vcLookBackTableName='OpportunityMaster'
--SELECT @numFieldReleaseDate=numFieldId FROM DycFieldMaster WHERE vcOrigDbColumnName='dtReleaseDate' AND vcLookBackTableName='OpportunityMaster'
--SELECT @numFieldReleaseStatus=numFieldId FROM DycFieldMaster WHERE vcOrigDbColumnName='numReleaseStatus' AND vcLookBackTableName='OpportunityMaster'
--SELECT @numFieldShipVia=numFieldId FROM DycFieldMaster WHERE vcOrigDbColumnName='numShipmentMethod' AND vcLookBackTableName='OpportunityMaster' AND numListID=82
--SELECT @numFieldInventoryStatus=numFieldId FROM DycFieldMaster WHERE vcOrigDbColumnName='vcInventoryStatus' AND vcLookBackTableName='OpportunityItems'
--SELECT @numFieldPerComplete=numFieldId FROM DycFieldMaster WHERE vcFieldName='Percentage Complete' ANd vcLookBackTableName='OpportunityMaster'

--UPDATE DycFieldMaster SET bitWorkFlowField=1,vcGroup='Order Fields' WHERE numFieldId IN (@numFieldPartner,@numFieldPartnerContact,@numFieldReleaseDate,@numFieldReleaseStatus,@numFieldShipVia,@numFieldInventoryStatus,@numFieldPerComplete)

--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitImport,bitExport,bitAllowFiltering
--)
--VALUES
--(
--	3,@numFieldPartner,70,1,0,'Partner Source','SelectBox',1,1,0,0,0,0,0,0,1,0,0,0
--),
--(
--	3,@numFieldPartnerContact,70,1,0,'Partner Contact','SelectBox',1,1,0,0,0,0,0,0,1,0,0,0
--),
--(
--	3,@numFieldReleaseDate,70,1,0,'Release Date','DateField',1,1,0,0,0,0,0,0,1,0,0,0
--),
--(
--	3,@numFieldReleaseStatus,70,1,0,'Release Status','SelectBox',1,1,0,0,0,0,0,0,1,0,0,0
--),
--(
--	3,@numFieldShipVia,70,1,0,'Ship Via','SelectBox',1,1,0,0,0,0,0,0,1,0,0,0
--),
--(
--	3,@numFieldInventoryStatus,70,1,0,'Inventory Status','SelectBox',1,1,0,0,0,0,0,0,1,0,0,0
--),
--(
--	3,@numFieldPerComplete,70,1,0,'Percentage Complete','SelectBox',1,1,0,0,0,0,0,0,1,0,0,0
--)

------------------------------

--UPDATE DycFormField_Mapping SET vcFieldName='Opportunity/Order Type' WHERE numFormID=70 AND vcFieldName='Opp Type'
--UPDATE DycFormField_Mapping SET vcFieldName='Opportunity/Order Status' WHERE numFormID=70 AND vcFieldName='Order Status'
--UPDATE DycFormField_Mapping SET vcFieldName='Opportunity/Order Sub-total' WHERE numFormID=70 AND vcFieldName='Order Sub-total'

----------------------------------

--BEGIN TRY
--BEGIN TRANSACTION
--	INSERT INTO PageMaster
--	(
--		numPageID,numModuleID,vcFileName,vcPageDesc,bitIsViewApplicable,bitIsAddApplicable,bitIsUpdateApplicable,bitIsDeleteApplicable,bitIsExportApplicable
--	)
--	VALUES
--	(
--		135,2,'frmCorrespondence.ascx','Correspondance',1,0,0,1,0
--	),
--	(
--		135,3,'frmCorrespondence.ascx','Correspondance',1,0,0,1,0
--	),
--	(
--		135,4,'frmCorrespondence.ascx','Correspondance',1,0,0,1,0
--	),
--	(
--		135,11,'frmCorrespondence.ascx','Correspondance',1,0,0,1,0
--	),
--	(
--		135,7,'frmCorrespondence.ascx','Correspondance',1,0,0,1,0
--	),
--	(
--		135,12,'frmCorrespondence.ascx','Correspondance',1,0,0,1,0
--	),
--	(
--		135,10,'frmCorrespondence.ascx','Correspondance',1,0,0,1,0
--	)


--	SELECT 
--		* 
--	INTO 
--		#temp 
--	FROM
--	(
--		SELECT 
--			ROW_NUMBER() OVER(ORDER BY numDomainId DESC) AS Row, 
--			numDomainId 
--		FROM 
--			Domain 
--		WHERE 
--			numDomainId <> -255
--	) TABLE2
	
--	DECLARE @RowCount INT
--	SET @RowCount = (SELECT COUNT(Row) FROM #temp) 
	
--	DECLARE @I INT
--	DECLARE @numDomainId NUMERIC(18,0)
	
--	SET @I = 1

--	WHILE (@I <= @RowCount)
--	BEGIN
			
--		SELECT @numDomainId = numDomainID FROM #temp WHERE Row = @I
	
--		-- 1. Leads - ModileID = 2
--		INSERT INTO dbo.GroupAuthorization
--			(numDomainID,numModuleID,numPageID,numGroupID,intExportAllowed,intPrintAllowed,intViewAllowed,intAddAllowed,intUpdateAllowed,intDeleteAllowed)
--		SELECT
--			@numDomainId
--			,2
--			,135
--			,numGroupID
--			,0
--			,0
--			,ISNULL((SELECT intViewAllowed FROM GroupAuthorization WHERE numDomainID=@numDomainId AND GroupAuthorization.numGroupID=AuthenticationGroupMaster.numGroupID AND numModuleID=13 AND numPageID=24),1)
--			,0
--			,0
--			,ISNULL((SELECT intDeleteAllowed FROM GroupAuthorization WHERE numDomainID=@numDomainId AND GroupAuthorization.numGroupID=AuthenticationGroupMaster.numGroupID AND numModuleID=13 AND numPageID=24),1)
--		FROM
--			AuthenticationGroupMaster
--		WHERE
--			numDomainID = @numDomainId AND
--			tintGroupType = 1

--		-- 2. Prospects - ModileID = 3
--		INSERT INTO dbo.GroupAuthorization
--			(numDomainID,numModuleID,numPageID,numGroupID,intExportAllowed,intPrintAllowed,intViewAllowed,intAddAllowed,intUpdateAllowed,intDeleteAllowed)
--		SELECT
--			@numDomainId,3,135,numGroupID
--			,0
--			,0
--			,ISNULL((SELECT intViewAllowed FROM GroupAuthorization WHERE numDomainID=@numDomainId AND GroupAuthorization.numGroupID=AuthenticationGroupMaster.numGroupID AND numModuleID=13 AND numPageID=24),1)
--			,0
--			,0
--			,ISNULL((SELECT intDeleteAllowed FROM GroupAuthorization WHERE numDomainID=@numDomainId AND GroupAuthorization.numGroupID=AuthenticationGroupMaster.numGroupID AND numModuleID=13 AND numPageID=24),1)
--		FROM
--			AuthenticationGroupMaster
--		WHERE
--			numDomainID = @numDomainId AND
--			tintGroupType = 1

--		-- 3. Accounts - ModileID = 4
--		INSERT INTO dbo.GroupAuthorization
--			(numDomainID,numModuleID,numPageID,numGroupID,intExportAllowed,intPrintAllowed,intViewAllowed,intAddAllowed,intUpdateAllowed,intDeleteAllowed)
--		SELECT
--			@numDomainId,4,135,numGroupID
--			,0
--			,0
--			,ISNULL((SELECT intViewAllowed FROM GroupAuthorization WHERE numDomainID=@numDomainId AND GroupAuthorization.numGroupID=AuthenticationGroupMaster.numGroupID AND numModuleID=13 AND numPageID=24),1)
--			,0
--			,0
--			,ISNULL((SELECT intDeleteAllowed FROM GroupAuthorization WHERE numDomainID=@numDomainId AND GroupAuthorization.numGroupID=AuthenticationGroupMaster.numGroupID AND numModuleID=13 AND numPageID=24),1)
--		FROM
--			AuthenticationGroupMaster
--		WHERE
--			numDomainID = @numDomainId AND
--			tintGroupType = 1

--		-- 4. Contacts - ModileID = 11
--		INSERT INTO dbo.GroupAuthorization
--			(numDomainID,numModuleID,numPageID,numGroupID,intExportAllowed,intPrintAllowed,intViewAllowed,intAddAllowed,intUpdateAllowed,intDeleteAllowed)
--		SELECT
--			@numDomainId,11,135,numGroupID
--			,0
--			,0
--			,ISNULL((SELECT intViewAllowed FROM GroupAuthorization WHERE numDomainID=@numDomainId AND GroupAuthorization.numGroupID=AuthenticationGroupMaster.numGroupID AND numModuleID=13 AND numPageID=24),1)
--			,0
--			,0
--			,ISNULL((SELECT intDeleteAllowed FROM GroupAuthorization WHERE numDomainID=@numDomainId AND GroupAuthorization.numGroupID=AuthenticationGroupMaster.numGroupID AND numModuleID=13 AND numPageID=24),1)
--		FROM
--			AuthenticationGroupMaster
--		WHERE
--			numDomainID = @numDomainId AND
--			tintGroupType = 1

--		-- 5. Cases - ModileID = 7
--		INSERT INTO dbo.GroupAuthorization
--			(numDomainID,numModuleID,numPageID,numGroupID,intExportAllowed,intPrintAllowed,intViewAllowed,intAddAllowed,intUpdateAllowed,intDeleteAllowed)
--		SELECT
--			@numDomainId,7,135,numGroupID
--			,0
--			,0
--			,ISNULL((SELECT intViewAllowed FROM GroupAuthorization WHERE numDomainID=@numDomainId AND GroupAuthorization.numGroupID=AuthenticationGroupMaster.numGroupID AND numModuleID=13 AND numPageID=24),1)
--			,0
--			,0
--			,ISNULL((SELECT intDeleteAllowed FROM GroupAuthorization WHERE numDomainID=@numDomainId AND GroupAuthorization.numGroupID=AuthenticationGroupMaster.numGroupID AND numModuleID=13 AND numPageID=24),1)
--		FROM
--			AuthenticationGroupMaster
--		WHERE
--			numDomainID = @numDomainId AND
--			tintGroupType = 1

--		-- 6. Projects - ModileID = 12
--		INSERT INTO dbo.GroupAuthorization
--			(numDomainID,numModuleID,numPageID,numGroupID,intExportAllowed,intPrintAllowed,intViewAllowed,intAddAllowed,intUpdateAllowed,intDeleteAllowed)
--		SELECT
--			@numDomainId,12,135,numGroupID
--			,0
--			,0
--			,ISNULL((SELECT intViewAllowed FROM GroupAuthorization WHERE numDomainID=@numDomainId AND GroupAuthorization.numGroupID=AuthenticationGroupMaster.numGroupID AND numModuleID=13 AND numPageID=24),1)
--			,0
--			,0
--			,ISNULL((SELECT intDeleteAllowed FROM GroupAuthorization WHERE numDomainID=@numDomainId AND GroupAuthorization.numGroupID=AuthenticationGroupMaster.numGroupID AND numModuleID=13 AND numPageID=24),1)
--		FROM
--			AuthenticationGroupMaster
--		WHERE
--			numDomainID = @numDomainId AND
--			tintGroupType = 1

--		-- 7. Opportunity/Orders - ModileID = 10
--		INSERT INTO dbo.GroupAuthorization
--			(numDomainID,numModuleID,numPageID,numGroupID,intExportAllowed,intPrintAllowed,intViewAllowed,intAddAllowed,intUpdateAllowed,intDeleteAllowed)
--		SELECT
--			@numDomainId,10,135,numGroupID
--			,0
--			,0
--			,ISNULL((SELECT intViewAllowed FROM GroupAuthorization WHERE numDomainID=@numDomainId AND GroupAuthorization.numGroupID=AuthenticationGroupMaster.numGroupID AND numModuleID=13 AND numPageID=24),1)
--			,0
--			,0
--			,ISNULL((SELECT intDeleteAllowed FROM GroupAuthorization WHERE numDomainID=@numDomainId AND GroupAuthorization.numGroupID=AuthenticationGroupMaster.numGroupID AND numModuleID=13 AND numPageID=24),1)
--		FROM
--			AuthenticationGroupMaster
--		WHERE
--			numDomainID = @numDomainId AND
--			tintGroupType = 1

--		SET @I = @I  + 1
--	END
	
--	DROP TABLE #temp
--COMMIT
--END TRY
--BEGIN CATCH
--	IF @@TRANCOUNT > 0
--		ROLLBACK TRANSACTION;

--	SELECT 
--		ERROR_MESSAGE(),
--		ERROR_NUMBER(),
--		ERROR_SEVERITY(),
--		ERROR_STATE(),
--		ERROR_LINE(),
--		ISNULL(ERROR_PROCEDURE(), '-');
--END CATCH

-----------------------------------------------

--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintColumn,tintRow,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering
--)
--VALUES
--(
--	2,56,34,1,1,'Contact Type','SelectBox','ContactType',1,1,1,1,0,0,1,1,0,1,1
--),
--(
--	2,56,35,1,1,'Contact Type','SelectBox','ContactType',1,1,1,1,0,0,1,1,0,1,1
--)


--UPDATE DycFormField_Mapping SET bitInResults=1,bitInlineEdit=1,bitAllowEdit=1,bitDefault=0,bitDeleted=0,bitAllowFiltering=1,bitAllowSorting=1,bitAddField=1,bitDetailField=0 WHERE numFormID=36 AND numFieldID=56

--------------------------------------------------------------------------------------------------------------

--INSERT INTO ReportDashboardAllowedReports
--(
--	numReportID
--	,numDomainID
--	,numGrpID
--	,tintReportCategory
--)
--SELECT intDefaultReportID,numDomainID,numGroupID,2 FROM AuthenticationGroupMaster OUTER APPLY (
--SELECT
--	intDefaultReportID
--FROM
--	ReportListMaster
--WHERE
--	ISNULL(numDomainID,0) = 0
--	AND bitDefault=1) AS Test

-------------------------------------------------------

--BEGIN TRY
--BEGIN TRANSACTION

--DECLARE @numFieldID NUMERIC(18,0)

--INSERT INTO DycFieldMaster
--(
--	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcListItemType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField
--)
--VALUES
--(
--	1,'Modified By','numModifiedBy','numModifiedBy','Communication','N','R','SelectBox','U',0,1,0,0,0,1
--)

--SET @numFieldID = SCOPE_IDENTITY()

--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField
--)
--VALUES
--(
--	1,@numFieldID,43,0,0,'Modified By','SelectBox',1,0,0,1,0,1
--)

--COMMIT
--END TRY
--BEGIN CATCH
--	IF @@TRANCOUNT > 0
--		ROLLBACK TRANSACTION;

--	SELECT 
--		ERROR_MESSAGE(),
--		ERROR_NUMBER(),
--		ERROR_SEVERITY(),
--		ERROR_STATE(),
--		ERROR_LINE(),
--		ISNULL(ERROR_PROCEDURE(), '-');
--END CATCH


-----------------------------------------------------------------------------
-----------------------------------------------------------------------------
-----------------------------------------------------------------------------


--INSERT INTO DycFieldMaster
--(
--	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcListItemType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAllowFiltering
--)
--VALUES
--(
--	39,'Units','numUnitHour','numUnitHour','ReturnItems','M','R','TextBox','',0,0,0,1,0,1,1
--),
--(
--	39,'Unit Price','monPrice','monPrice','ReturnItems','M','R','TextBox','',0,0,0,1,0,1,1
--),
--(
--	39,'Amount','monTotAmount','monTotAmount','ReturnItems','M','R','TextBox','',0,0,0,1,0,1,1
--)

--SET IDENTITY_INSERT ReportFieldGroupMaster ON

--INSERT INTO ReportFieldGroupMaster
--(
--	numReportFieldGroupID,vcFieldGroupName,bitActive
--)
--VALUES
--(
--	33,'Return Items',1
--)

--SET IDENTITY_INSERT ReportFieldGroupMaster OFF

--INSERT INTO ReportFieldGroupMappingMaster
--(
--	numReportFieldGroupID,numFieldID,vcFieldName,vcAssociatedControlType,vcFieldDataType,bitAllowSorting,bitAllowGrouping,bitAllowAggregate,bitAllowFiltering
--)
--SELECT
--	33,numFieldId,vcFieldName,vcAssociatedControlType,vcFieldDataType,1,1,1,1
--FROM
--	DycFieldMaster
--WHERE
--	vcLookBackTableName='ReturnItems'


--INSERT INTO ReportModuleGroupFieldMappingMaster
--(
--	numReportModuleGroupID,numReportFieldGroupID
--)
--VALUES
--(
--	24,33
--)

--ALTER TABLE Warehouses ADD vcPrintNodeAPIKey VARCHAR(100),  vcPrintNodePrinterID VARCHAR(100)


--ALTER TABLE OpportunityMaster ADD numShipFromWarehouse NUMERIC(18,0)
-----------------------------------------------------------------------------
-----------------------------------------------------------------------------
-----------------------------------------------------------------------------


--DECLARE @numFieldID NUMERIC(18,0)

--INSERT INTO DycFieldMaster
--(
--	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcListItemType,numListID,bitWorkFlowField,vcGroup,bitInResults,bitDeleted,bitAllowEdit,bitInlineEdit,bitDefault
--)
--VALUES
--(
--	3,'PO from SO / SO from PO','bitPOFromSOOrSOFromPO','bitPOFromSOOrSOFromPO','POFromSOOrSOFromPO','OpportunityMaster','Y','R','CheckBox','',0,1,'Order Fields',0,0,0,0,0
--)

--SET @numFieldID = SCOPE_IDENTITY()

--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,bitWorkFlowField,bitAllowFiltering
--)
--VALUES
--(
--	3,@numFieldID,70,0,0,'PO from SO / SO from PO','CheckBox','POFromSOOrSOFromPO',1,1,2,1,1
--)


--ALTER TABLE GroupTabDetails ADD bitInitialTab BIT


--UPDATE DynamicFormMaster SET numBizFormModuleID=4,tintFlag=0 WHERE numFormId IN (26,129)
--UPDATE DynamicFormMaster SET numBizFormModuleID=NULL WHERE numFormId IN (122)
--UPDATE BizFormWizardMasterConfiguration SET tintPageType=1 WHERE numFormId = 26

--ALTER TABLE AddressDetails ADD numContact NUMERIC(18,0)
--ALTER TABLE AddressDetails ADD bitAltContact BIT
--ALTER TABLE AddressDetails ADD vcAltContact VARCHAR(200)

--ALTER TABLE OpportunityAddress ADD numBillingContact NUMERIC(18,0)
--ALTER TABLE OpportunityAddress ADD bitAltBillingContact BIT
--ALTER TABLE OpportunityAddress ADD vcAltBillingContact VARCHAR(200)
--ALTER TABLE OpportunityAddress ADD numShippingContact NUMERIC(18,0)
--ALTER TABLE OpportunityAddress ADD bitAltShippingContact BIT
--ALTER TABLE OpportunityAddress ADD vcAltShippingContact VARCHAR(200)

--ALTER TABLE ShippingReport ADD tintSignatureType TINYINT


--USE [Production.2014]
--GO

--/****** Object:  Table [dbo].[DivisionMasterShippingConfiguration]    Script Date: 13-Oct-17 10:19:14 AM ******/
--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

--CREATE TABLE [dbo].[DivisionMasterShippingConfiguration](
--	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
--	[numDomainID] [numeric](18, 0) NOT NULL,
--	[numDivisionID] [numeric](18, 0) NOT NULL,
--	[IsAdditionalHandling] [bit] NULL,
--	[IsCOD] [bit] NULL,
--	[IsHomeDelivery] [bit] NULL,
--	[IsInsideDelevery] [bit] NULL,
--	[IsInsidePickup] [bit] NULL,
--	[IsSaturdayDelivery] [bit] NULL,
--	[IsSaturdayPickup] [bit] NULL,
--	[IsLargePackage] [bit] NULL,
--	[vcDeliveryConfirmation] [nvarchar](1000) NULL,
--	[vcDescription] [nvarchar](max) NULL,
--	[vcSignatureType] [nvarchar](300) NULL,
--	[vcCODType] [nvarchar](50) NULL,
-- CONSTRAINT [PK_DivisionMasterShippingConfiguration] PRIMARY KEY CLUSTERED 
--(
--	[ID] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
--) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

--GO


--USE [Production.2014]
--GO

--/****** Object:  Index [NonClusteredIndex-20171012-140021]    Script Date: 12-Oct-17 2:01:12 PM ******/
--CREATE NONCLUSTERED INDEX [NonClusteredIndex-20171012-140021] ON [dbo].[DivisionMasterShippingConfiguration]
--(
--	[numDomainID] ASC
--)
--INCLUDE ( 	[numDivisionID]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
--GO



--ALTER TABLE ElasticSearchLastReindex ADD vcModule VARCHAR(300)

--UPDATE ElasticSearchLastReindex SET vcModule = 'ElasticSearchReindex' WHERE ID=1

--INSERT INTO ElasticSearchLastReindex
--(
--	ID
--	,LastReindexDate
--	,vcModule
--)
--VALUES
--(
--	2
--	,DATEADD(DAY,-1,GETDATE())
--	,'DashboardCommissonReportCalculation'
--)

--INSERT INTO ReportListMaster
--(
--	vcReportName,vcReportDescription,numDomainID,tintReportType,textQuery,bitDefault,intDefaultReportID
--)
--VALUES
--(
--	'Top reasons why we�re wining deals','Top reasons why we�re wining deals',0,1,'EXEC USP_ReportListMaster_Top10ReasonsDealWins @numDomainID',1,26
--),
--(
--	'Top reasons why we�re loosing deals','Top reasons why we�re loosing deals',0,1,'EXEC USP_ReportListMaster_Top10ReasonsDealLost @numDomainID',1,27
--),
--(
--	'Top 10 Reasons for Sales Returns','Top 10 Reasons for Sales Returns',0,1,'EXEC USP_ReportListMaster_Top10ReasonsForSalesReturns @numDomainID',1,28
--),
--(
--	'Employee Sales Performance Panel 1','Employee Sales Performance Panel 1',0,1,'EXEC USP_ReportListMaster_EmployeeSalesPerformancePanel1 @numDomainID',1,29
--),
--(
--	'Partner Revenues, Margins & Profits (Last 12 months)','Partner Revenues, Margins & Profits (Last 12 months)',0,1,'EXEC USP_ReportListMaster_PartnerRMPLast12Months @numDomainID',1,30
--),
--(
--	'Employee Sales Performance pt 1 (Last 12 months)','Employee Sales Performance pt 1 (Last 12 months)',0,1,'EXEC USP_ReportListMaster_EmployeeSalesPerformance1 @numDomainID',1,31
--),
--(
--	'Employee benefit to company (Last 12 months)','Employee benefit to company (Last 12 months)',0,1,'EXEC USP_ReportListMaster_EmployeeBenefitToCompany @numDomainID,@ClientTimeZoneOffset',1,32
--),
--(
--	'First 10 Saved Searches','First 10 Saved Searches',0,1,'EXEC USP_ReportListMaster_First10SavedSearch @numDomainID',1,33
--)

-----------------------------------------------------------------------------
-----------------------------------------------------------------------------
-----------------------------------------------------------------------------

--ALTER TABLE ParentChildCustomFieldMap ADD bitCustomField BIT
--UPDATE ParentChildCustomFieldMap SET bitCustomField=1

---------------------------------------------------------------------------------------------------------------------------------

--BEGIN TRY
--BEGIN TRANSACTION

--DECLARE @numFieldID NUMERIC(18,0)

--INSERT INTO DycFieldMaster
--(
--	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcListItemType,numListID,[order],tintRow,tintColumn,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitAllowFiltering,vcGroup
--)
--VALUES
--(
--	1,'Price Level','tintPriceLevel','tintPriceLevel','PriceLevel','DivisionMaster','N','R','SelectBox','',0,50,1,1,1,0,1,0,0,0,0,1,1,1,'Org.Details Fields'
--)

--SET @numFieldID = SCOPE_IDENTITY()


--INSERT INTO ReportFieldGroupMappingMaster
--(
--	numReportFieldGroupID,numFieldID,vcFieldName,vcAssociatedControlType,vcFieldDataType,bitAllowSorting,bitAllowGrouping,bitAllowAggregate,bitAllowFiltering
--)
--VALUES
--(
--	1,@numFieldID,'Price Level','SelectBox','N',1,1,1,1
--)

--COMMIT
--END TRY
--BEGIN CATCH
-- IF @@TRANCOUNT > 0
--  ROLLBACK TRANSACTION;

-- SELECT 
--  ERROR_MESSAGE(),
--  ERROR_NUMBER(),
--  ERROR_SEVERITY(),
--  ERROR_STATE(),
--  ERROR_LINE(),
--  ISNULL(ERROR_PROCEDURE(), '-');
--END CATCH

------------------------------------------------------------------------------------------------------------------------------

--ALTER TABLE ReturnHeader ADD numReferenceSalesOrder NUMERIC(18,0)
--ALTER TABLE ReturnItems ADD monVendorCost MONEY
--ALTER TABLE ReturnItems ADD monAverageCost MONEY
--ALTER TABLE AdditionalContactsInformation ADD vcTaxID VARCHAR(100)

------------------------------------------------------------------------------------------------------------------------------

--BEGIN TRY
--BEGIN TRANSACTION

--	 --add new values for slide 1
--	 DECLARE @numFieldID AS NUMERIC(18,0)
--	 DECLARE @numFormFieldID AS NUMERIC(18,0)

--	 INSERT INTO DycFieldMaster
--	 (numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,[order],tintRow,tintColumn,
--	 bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numListID)
--	 VALUES
--	 (2,'Tax ID','vcTaxID','vcTaxID','TaxID','AdditionalContactsInformation','V','R','TextBox',48,1,1,1,0,1,0,1,1,1,1,1,0)

--	 SELECT @numFieldID = SCOPE_IDENTITY()

--	 INSERT INTO DynamicFormFieldMaster
--	 (numFormID,vcFormFieldName,vcFieldType,vcAssociatedControlType,vcDbColumnName,numListID,bitDeleted,vcFieldDataType,vcOrigDbColumnName,bitAllowEdit,vcLookBackTableName,
--	  bitDefault,[order],bitInResults,vcPropertyName,tintRow,tintColumn,bitSettingField,bitAddField,bitDetailField,bitAllowSorting)
--	 VALUES
--	 (10,'Tax ID','R','TextBox','vcCustomerPO#',0,0,'V','vcCustomerPO#',1,'AdditionalContactsInformation',0,48,1,'TaxID',47,1,1,1,1,1)

--	 SELECT @numFormFieldID = SCOPE_IDENTITY()

--	 INSERT INTO DycFormField_Mapping
--	 (numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,bitInResults,bitDeleted,
--	 bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numFormFieldID)
--	 VALUES
--	 (2,@numFieldID,10,1,1,'Tax ID','TextBox','TaxID',48,1,1,1,0,0,1,1,1,1,1,@numFormFieldID)
--COMMIT
--END TRY
--BEGIN CATCH
-- IF @@TRANCOUNT > 0
--  ROLLBACK TRANSACTION;

-- SELECT 
--  ERROR_MESSAGE(),
--  ERROR_NUMBER(),
--  ERROR_SEVERITY(),
--  ERROR_STATE(),
--  ERROR_LINE(),
--  ISNULL(ERROR_PROCEDURE(), '-');
--END CATCH

--=============================

--BEGIN TRY
--BEGIN TRANSACTION

--	 --add new values for slide 1
--	 DECLARE @numFieldID AS NUMERIC(18,0)
--	 DECLARE @numFormFieldID AS NUMERIC(18,0)

--	 INSERT INTO DycFieldMaster
--	 (numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,[order],tintRow,tintColumn,
--	 bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numListID)
--	 VALUES
--	 (3,'Serial/Lot #s','SerialLotNo','SerialLotNo','SerialLotNo','OpportunityItems','V','R','HyperLink',48,1,1,1,0,1,0,1,0,0,0,0,0)

--	 SELECT @numFieldID = SCOPE_IDENTITY()

--	 INSERT INTO DynamicFormFieldMaster
--	 (numFormID,vcFormFieldName,vcFieldType,vcAssociatedControlType,vcDbColumnName,numListID,bitDeleted,vcFieldDataType,vcOrigDbColumnName,bitAllowEdit,vcLookBackTableName,
--	  bitDefault,[order],bitInResults,vcPropertyName,tintRow,tintColumn,bitSettingField,bitAddField,bitDetailField,bitAllowSorting)
--	 VALUES
--	 (26,'Serial/Lot #s','R','HyperLink','SerialLotNo',0,0,'V','SerialLotNo',1,'OpportunityItems',0,48,1,'SerialLotNo',47,1,1,0,0,0)

--	 SELECT @numFormFieldID = SCOPE_IDENTITY()

--	 INSERT INTO DycFormField_Mapping
--	 (numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,bitInResults,bitDeleted,
--	 bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numFormFieldID)
--	 VALUES
--	 (3,@numFieldID,26,1,1,'Serial/Lot #s','HyperLink','SerialLotNo',48,1,1,1,0,0,1,0,0,0,0,@numFormFieldID)

--	 INSERT INTO DynamicFormFieldMaster
--	 (numFormID,vcFormFieldName,vcFieldType,vcAssociatedControlType,vcDbColumnName,numListID,bitDeleted,vcFieldDataType,vcOrigDbColumnName,bitAllowEdit,vcLookBackTableName,
--	  bitDefault,[order],bitInResults,vcPropertyName,tintRow,tintColumn,bitSettingField,bitAddField,bitDetailField,bitAllowSorting)
--	 VALUES
--	 (129,'Serial/Lot #s','R','HyperLink','SerialLotNo',0,0,'V','SerialLotNo',1,'OpportunityItems',0,48,1,'SerialLotNo',47,1,1,0,0,0)

--	 SELECT @numFormFieldID = SCOPE_IDENTITY()

--	 INSERT INTO DycFormField_Mapping
--	 (numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,bitInResults,bitDeleted,
--	 bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numFormFieldID)
--	 VALUES
--	 (3,@numFieldID,129,1,1,'Serial/Lot #s','HyperLink','SerialLotNo',48,1,1,1,0,0,1,0,0,0,0,@numFormFieldID)
--COMMIT
--END TRY
--BEGIN CATCH
-- IF @@TRANCOUNT > 0
--  ROLLBACK TRANSACTION;

-- SELECT 
--  ERROR_MESSAGE(),
--  ERROR_NUMBER(),
--  ERROR_SEVERITY(),
--  ERROR_STATE(),
--  ERROR_LINE(),
--  ISNULL(ERROR_PROCEDURE(), '-');
--END CATCH

----------------------------------------------------------------------------------------------------------------

--UPDATE PageNavigationDTL SET bitVisible=0 WHERE vcPageNavName='Predefined Reports'
--UPDATE PageNavigationDTL SET bitVisible=0 WHERE vcPageNavName='Commission Item Reports'
--UPDATE PageNavigationDTL SET bitVisible=0 WHERE vcPageNavName='Commission Contacts Item Reports'
--UPDATE PageNavigationDTL SET bitVisible=0 WHERE vcPageNavName='Dashboard' AND numTabID=6
--UPDATE PageNavigationDTL SET vcPageNavName='Reports & Dashboards',vcImageURL='../images/CustomReports.png' WHERE vcPageNavName='Custom Reports'
--UPDATE PageNavigationDTL SET vcImageURL='../images/MyReports.png' WHERE vcPageNavName='My Reports'
--UPDATE ModuleMaster SET vcModuleName='Reports & Dashboards' WHERE numModuleID=24
----------------------------------------------------------------------
--ALTER TABLE ReportListMaster ADD bitDefault BIT
--ALTER TABLE ReportListMaster ADD intDefaultReportID INT
----------------------------------------------------------------------
--DELETE FROM GroupAuthorization WHERE numModuleID=24 AND numPageID IN (SELECT numPageID FROM PageMaster WHERE numModuleID=24)
--DELETE FROM PageMaster WHERE numModuleID=24
-------------------------------------------------------------------------------------------------------------
--INSERT INTO PageMaster 
--(
--	numPageID,numModuleID,vcFileName,vcPageDesc,bitIsViewApplicable,bitIsAddApplicable,bitIsUpdateApplicable,bitIsDeleteApplicable,bitIsExportApplicable
--)
--VALUES
--(
--	1,24,'frmNewDashBoard.aspx','Ability to add new dashboard templates (from dashboard)',1,0,0,0,0
--),
--(
--	2,24,'frmCustomReportList.aspx','Ability to create new custom reports',1,0,0,0,0
--),
--(
--	3,24,'frmCustomReportList.aspx','Ability to set reports allowed by permission group',1,0,0,0,0
--),
--(
--	4,24,'frmCustomReportList.aspx','Ability to build KPI reports',1,0,0,0,0
--),
--(
--	5,24,'frmCustomReportList.aspx','Ability to Delete Dashboard templates',1,0,0,0,0
--),
--(
--	6,24,'frmNewDashBoard.aspx','Ability to Update Dashboard templates',1,0,0,0,0
--)
---------------------------------------------------------------------------------------------------------------------------------------
--BEGIN TRY
--BEGIN TRANSACTION
--	SELECT 
--		* 
--	INTO 
--		#temp 
--	FROM
--	(
--		SELECT 
--			ROW_NUMBER() OVER(ORDER BY numDomainId DESC) AS Row, 
--			numDomainId 
--		FROM 
--			Domain 
--		WHERE 
--			numDomainId <> -255
--	) TABLE2
	
--	DECLARE @RowCount INT
--	SET @RowCount = (SELECT COUNT(Row) FROM #temp) 
	
--	DECLARE @I INT
--	DECLARE @numDomainId NUMERIC(18,0)
	
--	SET @I = 1

--	WHILE (@I <= @RowCount)
--	BEGIN
			
--		SELECT @numDomainId = numDomainID FROM #temp WHERE Row = @I
	
--		INSERT INTO dbo.GroupAuthorization
--			(numDomainID,numModuleID,numPageID,numGroupID,intExportAllowed,intPrintAllowed,intViewAllowed,intAddAllowed,intUpdateAllowed,intDeleteAllowed)
--		SELECT
--			@numDomainId,24,1,numGroupID,0,0,3,0,0,0
--		FROM
--			AuthenticationGroupMaster
--		WHERE
--			numDomainID = @numDomainId AND
--			tintGroupType = 1
--		UNION
--		SELECT
--			@numDomainId,24,2,numGroupID,0,0,3,0,0,0
--		FROM
--			AuthenticationGroupMaster
--		WHERE
--			numDomainID = @numDomainId AND
--			tintGroupType = 1
--		UNION
--		SELECT
--			@numDomainId,24,3,numGroupID,0,0,3,0,0,0
--		FROM
--			AuthenticationGroupMaster
--		WHERE
--			numDomainID = @numDomainId AND
--			tintGroupType = 1
--		UNION
--		SELECT
--			@numDomainId,24,4,numGroupID,0,0,3,0,0,0
--		FROM
--			AuthenticationGroupMaster
--		WHERE
--			numDomainID = @numDomainId AND
--			tintGroupType = 1
--		UNION
--		SELECT
--			@numDomainId,24,5,numGroupID,0,0,3,0,0,0
--		FROM
--			AuthenticationGroupMaster
--		WHERE
--			numDomainID = @numDomainId AND
--			tintGroupType = 1
--		UNION
--		SELECT
--			@numDomainId,24,6,numGroupID,0,0,3,0,0,0
--		FROM
--			AuthenticationGroupMaster
--		WHERE
--			numDomainID = @numDomainId AND
--			tintGroupType = 1

--		SET @I = @I  + 1
--	END
	
--	DROP TABLE #temp
--COMMIT
--END TRY
--BEGIN CATCH
--	IF @@TRANCOUNT > 0
--		ROLLBACK TRANSACTION;

--	SELECT 
--		ERROR_MESSAGE(),
--		ERROR_NUMBER(),
--		ERROR_SEVERITY(),
--		ERROR_STATE(),
--		ERROR_LINE(),
--		ISNULL(ERROR_PROCEDURE(), '-');
--END CATCH
------------------------------------------------------------------------------------------------------------------
--USE [Production.2014]
--GO

--/****** Object:  Table [dbo].[DashboardTemplate]    Script Date: 06-Sep-17 4:06:29 PM ******/
--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

--SET ANSI_PADDING ON
--GO

--CREATE TABLE [dbo].[DashboardTemplate](
--	[numTemplateID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
--	[numDomainID] [numeric](18, 0) NOT NULL,
--	[vcTemplateName] [varchar](300) NOT NULL,
--	[dtCreatedDate] [datetime] NOT NULL,
--	[numCreatedBy] [numeric](18, 0) NOT NULL,
--	[dtModifiedDate] [datetime] NULL,
--	[numModifiedBy] [numeric](18, 0) NULL,
-- CONSTRAINT [PK_DashboardTemplate] PRIMARY KEY CLUSTERED 
--(
--	[numTemplateID] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
--) ON [PRIMARY]

--GO

--SET ANSI_PADDING ON
--GO
-------------------------------------------------------------------------------------------------------------
--USE [Production.2014]
--GO

--/****** Object:  Table [dbo].[DashboardTemplateReports]    Script Date: 06-Sep-17 4:06:22 PM ******/
--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

--SET ANSI_PADDING ON
--GO

--CREATE TABLE [dbo].[DashboardTemplateReports](
--	[numDTRID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
--	[numDomainID] [numeric](18, 0) NOT NULL,
--	[numDashboardTemplateID] [numeric](18, 0) NOT NULL,
--	[numReportID] [numeric](18, 0) NOT NULL,
--	[tintReportType] [tinyint] NOT NULL,
--	[tintChartType] [tinyint] NOT NULL,
--	[vcHeaderText] [varchar](100) NULL,
--	[vcFooterText] [varchar](100) NULL,
--	[tintRow] [tinyint] NOT NULL,
--	[tintColumn] [tinyint] NOT NULL,
--	[vcXAxis] [varchar](50) NULL,
--	[vcYAxis] [varchar](50) NULL,
--	[vcAggType] [varchar](50) NULL,
--	[tintReportCategory] [tinyint] NULL,
--	[intHeight] [int] NULL,
--	[intWidth] [int] NULL,
-- CONSTRAINT [PK_DashboardTemplateReports] PRIMARY KEY CLUSTERED 
--(
--	[numDTRID] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
--) ON [PRIMARY]

--GO

--SET ANSI_PADDING ON
--GO

--ALTER TABLE [dbo].[DashboardTemplateReports]  WITH CHECK ADD  CONSTRAINT [FK_DashboardTemplateReports_DashboardTemplate] FOREIGN KEY([numDashboardTemplateID])
--REFERENCES [dbo].[DashboardTemplate] ([numTemplateID])
--ON DELETE CASCADE
--GO

--ALTER TABLE [dbo].[DashboardTemplateReports] CHECK CONSTRAINT [FK_DashboardTemplateReports_DashboardTemplate]
--GO

-------------------------------------------------------------------------------------------------------------
--ALTER TABLE ReportDashboard ADD numDashboardTemplateID NUMERIC(18,0)
--ALTER TABLE ReportDashboard ADD bitNewAdded BIT
-------------------------------------------------------------------------------------------------------------
--ALTER TABLE UserMaster ADD numDashboardTemplateID NUMERIC(18,0)
-------------------------------------------------------------------------------------------------------------
--INSERT INTO DashboardTemplate 
--(
--	numDomainID
--	,vcTemplateName
--	,dtCreatedDate
--	,numCreatedBy
--)
--SELECT 
--	numDomainID
--	,CONCAT('Default_Template_',numUserCntID)
--	,GETUTCDATE()
--	,numUserCntID 
--FROM 
--	ReportDashboard 
--GROUP BY 
--	numDomainID,numUserCntID

--INSERT INTO DashboardTemplate 
--(
--	numDomainID
--	,vcTemplateName
--	,dtCreatedDate
--	,numCreatedBy
--)
--SELECT
--	numDomainId
--	,'Default Template'
--	,GETUTCDATE()
--	,numAdminID
--FROM
--	Domain
--WHERE
--	numDomainId <> -255
--	AND numDomainId NOT IN (SELECT numDomainID FROM DashboardTemplate)
-------------------------------------------------------------------------------------------------------------
--UPDATE
--	ReportDashboard
--SET
--	numDashboardTemplateID = (CASE WHEN EXISTS (SELECT numTemplateID FROM DashboardTemplate WHERE numDomainID=ReportDashboard.numDomainID AND CHARINDEX(CONCAT('_',ReportDashboard.numUserCntID,'_'),CONCAT(vcTemplateName,'_')) > 0) THEN (SELECT numTemplateID FROM DashboardTemplate WHERE numDomainID=ReportDashboard.numDomainID AND CHARINDEX(CONCAT('_',ReportDashboard.numUserCntID,'_'),CONCAT(vcTemplateName,'_')) > 0) ELSE (SELECT numTemplateID FROM DashboardTemplate WHERE numDomainID=ReportDashboard.numDomainID) END)
-------------------------------------------------------------------------------------------------------------
--UPDATE
--	UserMaster
--SET
--	numDashboardTemplateID = (CASE WHEN EXISTS (SELECT numTemplateID FROM DashboardTemplate WHERE numDomainID=UserMaster.numDomainID AND CHARINDEX(CONCAT('_',UserMaster.numUserDetailId,'_'),CONCAT(vcTemplateName,'_')) > 0) THEN (SELECT numTemplateID FROM DashboardTemplate WHERE numDomainID=UserMaster.numDomainID AND CHARINDEX(CONCAT('_',UserMaster.numUserDetailId,'_'),CONCAT(vcTemplateName,'_')) > 0) ELSE (SELECT TOP 1 numTemplateID FROM DashboardTemplate WHERE numDomainID=UserMaster.numDomainID) END)
-------------------------------------------------------------------------------------------------------------
--INSERT INTO [dbo].[DashboardTemplateReports]
--(
--	[numDomainID]
--    ,[numDashboardTemplateID]
--    ,[numReportID]
--    ,[tintReportType]
--    ,[tintChartType]
--    ,[vcHeaderText]
--    ,[vcFooterText]
--    ,[tintRow]
--    ,[tintColumn]
--    ,[vcXAxis]
--    ,[vcYAxis]
--    ,[vcAggType]
--    ,[tintReportCategory]
--    ,[intHeight]
--    ,[intWidth]
--)
--SELECT
--	[numDomainID]
--    ,[numDashboardTemplateID]
--    ,[numReportID]
--    ,[tintReportType]
--    ,[tintChartType]
--    ,[vcHeaderText]
--    ,[vcFooterText]
--    ,[tintRow]
--    ,[tintColumn]
--    ,[vcXAxis]
--    ,[vcYAxis]
--    ,[vcAggType]
--    ,[tintReportCategory]
--    ,[intHeight]
--    ,[intWidth]
--FROM
--	ReportDashboard
------------------------------------------------------------------------------------------------------------
--INSERT INTO ReportListMaster
--(
--	vcReportName,vcReportDescription,numDomainID,tintReportType,textQuery,bitDefault,intDefaultReportID
--)
--VALUES
--(
--	'A/R','Account Receivable',0,1,'EXEC USP_ReportListMaster_ARPreBuildReport @numDomainID',1,1
--)
--,
--(
--	'A/P','Account Payable',0,1,'EXEC USP_ReportListMaster_APPreBuildReport @numDomainID',1,2
--),
--(
--	'Accounting Reports','Accounting Reports',0,1,'',1,4
--),
--(
--	'Money in the Bank','Money in the Bank',0,1,'EXEC USP_ReportListMaster_BankPreBuildReport @numDomainID',1,3
--),
--(
--	'Top 10 Items by profit amount (Last 12 months)','Top 10 Items by profit amount (Last 12 months)',0,2,'EXEC USP_ReportListMaster_Top10ItemByProfitPreBuildReport @numDomainID',1,5
--),
--(
--	'Top 10 Items by revenue sold (Last 12 months)','Top 10 Items by revenue sold (Last 12 months)',0,2,'EXEC USP_ReportListMaster_Top10ItemByRevenuePreBuildReport @numDomainID',1,6
--),
--(
--	'Profit margin by item classification (Last 12 months)','Profit margin by item classification (Last 12 months)',0,2,'EXEC USP_ReportListMaster_ItemClassificationProfitPreBuildReport @numDomainID',1,7
--),
--(
--	'Top 10 Customers by profit margin','Top 10 Customers by profit margin',0,1,'EXEC USP_ReportListMaster_Top10CustomerByProfitPercentPreBuildReport @numDomainID',1,8
--),
--(
--	'Top 10 Customers by profit amount','Top 10 Customers by profit amount',0,1,'EXEC USP_ReportListMaster_Top10CustomerByProfitAmountPreBuildReport @numDomainID',1,9
--),
--(
--	'Action Items & Meetings due today','Action Items & Meetings due today',0,1,'EXEC USP_ReportListMaster_ActionItemPreBuildReport @numDomainID,@numUserCntID,@ClientTimeZoneOffset',1,10
--),
--(
--	'YTD vs same period last year','YTD vs same period last year',0,1,'EXEC USP_ReportListMaster_RPEYTDAndSamePeriodLastYear @numDomainID',1,11
--),
--(
--	'Last month vs same period last month','Last month vs same period last month',0,1,'EXEC USP_ReportListMaster_RPELastMonthAndSamePeriodLastYear @numDomainID',1,12
--),
--(
--	'Sales vs Expenses (Last 12 months)','Sales vs Expenses (Last 12 months)',0,2,'EXEC USP_ReportListMaster_SalesVsExpenseLast12Months @numDomainID',1,13
--),
--(
--	'Top sources of Sales Orders (Last 12 months)','Top sources of Sales Orders (Last 12 months)',0,1,'EXEC USP_ReportListMaster_TopSourceOfSalesOrder @numDomainID',1,14
--),
--(
--	'Top 10 items by profit margin','Top 10 items by profit margin',0,1,'EXEC USP_ReportListMaster_Top10ItemByProfitMargin @numDomainID',1,15
--),
--(
--	'Top 10 Sales Opportunities by Revenue (Last 12 months)','Top 10 Sales Opportunities by Revenue (Last 12 months)',0,1,'EXEC USP_ReportListMaster_Top10SalesOpportunityByRevenue @numDomainID',1,16
--),
--(
--	'Top 10 Sales Opportunities by total Progress','Top 10 Sales Opportunities by total Progress',0,1,'EXEC USP_ReportListMaster_Top10SalesOpportunityByTotalProgress @numDomainID',1,17
--),
--(
--	'Top 10 items returned vs Qty Sold','Top 10 items returned vs Qty Sold',0,1,'EXEC USP_ReportListMaster_Top10ItemsByReturnedVsSold @numDomainID',1,18
--),
--(
--	'Largest 10 Sales Opportunities past their due date','Largest 10 Sales Opportunities past their due date',0,1,'EXEC USP_ReportListMaster_Top10SalesOpportunityByPastDue @numDomainID,@ClientTimeZoneOffset',1,19
--),
--(
--	'Top 10 Campaigns by ROI','Top 10 Campaigns by ROI',0,1,'EXEC USP_ReportListMaster_Top10CampaignsByROI @numDomainID',1,20
--),
--(
--	'Market Fragmentation � Top 10 Customers, as a portion total sales','Market Fragmentation � Top 10 Customers, as a portion total sales',0,2,'EXEC USP_ReportListMaster_TopCustomersByPortionOfTotalSales @numDomainID',1,21
--),
--(
--	'Scorecard','Scorecard',0,1,'EXEC USP_ReportListMaster_PrebuildScoreCard @numDomainID',1,22
--),
--(
--	'Top 10 Lead Sources (New Leads)','Top 10 Lead Sources (New Leads)',0,1,'EXEC USP_ReportListMaster_Top10LeadSource @numDomainID',1,23
--),
--(
--	'Last 10 email messages from people I do business with','Last 10 email messages from people I do business with',0,1,'EXEC USP_ReportListMaster_Top10LeadSource @numDomainID',1,24
--),
--(
--	'Reminders','Reminders',0,1,'EXEC USP_ReportListMaster_RemindersPreBuildReport @numDomainID,@ClientTimeZoneOffset',1,25
--)


-----------------------------------------------------------------------------
-----------------------------------------------------------------------------
-----------------------------------------------------------------------------


--BEGIN TRY
--BEGIN TRANSACTION

--	 --add new values for slide 1
--	 DECLARE @numFieldID AS NUMERIC(18,0)
--	 DECLARE @numFormFieldID AS NUMERIC(18,0)

--	 INSERT INTO DycFieldMaster
--	 (numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,[order],tintRow,tintColumn,
--	 bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numListID)
--	 VALUES
--	 (3,'Customer PO#','vcCustomerPO#','CustomerPO','','OpportunityMaster','V','R','TextBox',48,1,1,1,0,1,0,1,1,1,0,0,0)

--	 SELECT @numFieldID = SCOPE_IDENTITY()

--	 INSERT INTO DynamicFormFieldMaster
--	 (numFormID,vcFormFieldName,vcFieldType,vcAssociatedControlType,vcDbColumnName,numListID,bitDeleted,vcFieldDataType,vcOrigDbColumnName,bitAllowEdit,vcLookBackTableName,
--	  bitDefault,[order],bitInResults,vcPropertyName,tintRow,tintColumn,bitSettingField,bitAddField,bitDetailField,bitAllowSorting)
--	 VALUES
--	 (40,'Customer PO#','R','TextBox','vcCustomerPO#',0,0,'V','vcCustomerPO#',1,'OpportunityMaster',0,48,1,'CustomerPO',47,1,1,1,0,0)

--	 SELECT @numFormFieldID = SCOPE_IDENTITY()

--	 INSERT INTO DycFormField_Mapping
--	 (numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,bitInResults,bitDeleted,
--	 bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numFormFieldID)
--	 VALUES
--	 (3,@numFieldID,40,1,1,'Customer PO#','TextBox','CustomerPO',48,1,1,1,0,0,1,1,1,0,0,@numFormFieldID)


--	 INSERT INTO DynamicFormFieldMaster
--	 (numFormID,vcFormFieldName,vcFieldType,vcAssociatedControlType,vcDbColumnName,numListID,bitDeleted,vcFieldDataType,vcOrigDbColumnName,bitAllowEdit,vcLookBackTableName,
--	  bitDefault,[order],bitInResults,vcPropertyName,tintRow,tintColumn,bitSettingField,bitAddField,bitDetailField,bitAllowSorting)
--	 VALUES
--	 (41,'Customer PO#','R','TextBox','vcCustomerPO#',0,0,'V','vcCustomerPO#',1,'OpportunityMaster',0,48,1,'CustomerPO#',47,1,1,1,0,0)

--	 SELECT @numFormFieldID = SCOPE_IDENTITY()

--	 INSERT INTO DycFormField_Mapping
--	 (numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,bitInResults,bitDeleted,
--	 bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numFormFieldID)
--	 VALUES
--	 (3,@numFieldID,41,1,1,'Customer PO#','TextBox','CustomerPO',48,1,1,1,0,0,1,1,1,0,0,@numFormFieldID)
--COMMIT
--END TRY
--BEGIN CATCH
-- IF @@TRANCOUNT > 0
--  ROLLBACK TRANSACTION;

-- SELECT 
--  ERROR_MESSAGE(),
--  ERROR_NUMBER(),
--  ERROR_SEVERITY(),
--  ERROR_STATE(),
--  ERROR_LINE(),
--  ISNULL(ERROR_PROCEDURE(), '-');
--END CATCH

-----------------------------------------------------------------------------
-----------------------------------------------------------------------------
-----------------------------------------------------------------------------


--ALTER TABLE Category ADD vcMetaTitle VARCHAR(1000)
--ALTER TABLE Category ADD vcMetaKeywords VARCHAR(1000)
--ALTER TABLE Category ADD vcMetaDescription VARCHAR(1000)

--BEGIN TRY
--BEGIN TRANSACTION

--	 --add new values for slide 1
--	 DECLARE @numFieldID AS NUMERIC(18,0)
--	 DECLARE @numFormFieldID AS NUMERIC(18,0)

--	 INSERT INTO DycFieldMaster
--	 (numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,[order],tintRow,tintColumn,
--	 bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numListID)
--	 VALUES
--	 (3,'Inclusion details','vcInclusionDetails','vcInclusionDetails','','OpportunityItems','V','R','Label',48,1,1,1,0,0,0,1,0,0,0,0,0)

--	 SELECT @numFieldID = SCOPE_IDENTITY()

--	 INSERT INTO DynamicFormFieldMaster
--	 (numFormID,vcFormFieldName,vcFieldType,vcAssociatedControlType,vcDbColumnName,numListID,bitDeleted,vcFieldDataType,vcOrigDbColumnName,bitAllowEdit,vcLookBackTableName,
--	  bitDefault,[order],bitInResults,vcPropertyName,tintRow,tintColumn,bitSettingField,bitAddField,bitDetailField,bitAllowSorting)
--	 VALUES
--	 (26,'Inclusion details','R','Label','',0,0,'V','',0,'',0,48,1,'',47,1,1,0,0,0)

--	 SELECT @numFormFieldID = SCOPE_IDENTITY()

--	 INSERT INTO DycFormField_Mapping
--	 (numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,bitInResults,bitDeleted,
--	 bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numFormFieldID)
--	 VALUES
--	 (3,@numFieldID,26,0,0,'Inclusion details','Label','',48,1,1,1,0,0,1,0,0,0,0,@numFormFieldID)

--	  INSERT INTO DynamicFormFieldMaster
--	 (numFormID,vcFormFieldName,vcFieldType,vcAssociatedControlType,vcDbColumnName,numListID,bitDeleted,vcFieldDataType,vcOrigDbColumnName,bitAllowEdit,vcLookBackTableName,
--	  bitDefault,[order],bitInResults,vcPropertyName,tintRow,tintColumn,bitSettingField,bitAddField,bitDetailField,bitAllowSorting)
--	 VALUES
--	 (7,'Inclusion details','R','Label','',0,0,'V','',0,'',0,48,1,'',47,1,1,0,0,0)

--	 SELECT @numFormFieldID = SCOPE_IDENTITY()

--	 INSERT INTO DycFormField_Mapping
--	 (numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,bitInResults,bitDeleted,
--	 bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numFormFieldID)
--	 VALUES
--	 (3,@numFieldID,7,0,0,'Inclusion details','Label','',48,1,1,1,0,0,1,0,0,0,0,@numFormFieldID)
--COMMIT
--END TRY
--BEGIN CATCH
-- IF @@TRANCOUNT > 0
--  ROLLBACK TRANSACTION;

-- SELECT 
--  ERROR_MESSAGE(),
--  ERROR_NUMBER(),
--  ERROR_SEVERITY(),
--  ERROR_STATE(),
--  ERROR_LINE(),
--  ISNULL(ERROR_PROCEDURE(), '-');
--END CATCH

-----------------------------------------------------------------------------
-----------------------------------------------------------------------------
-----------------------------------------------------------------------------


--USE [Production.2014]
--GO

--/****** Object:  Table [dbo].[ElasticSearchLastReindex]    Script Date: 10-Aug-17 12:25:10 PM ******/
--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

--CREATE TABLE [dbo].[ElasticSearchLastReindex](
--	[ID] [int] NOT NULL,
--	[LastReindexDate] [date] NOT NULL
--) ON [PRIMARY]

--GO


--INSERT INTO ElasticSearchLastReindex (ID,LastReindexDate) VALUES (1,GETDATE())

--UPDATE DycFormField_Mapping SET numFieldID=190 WHERE numFormID=86 AND numFieldID=234
--UPDATE DycFormConfigurationDetails SET numFieldID=190 WHERE numFormID=86 AND numFieldID=234

-----------------------------------------------------------------------------
-----------------------------------------------------------------------------
-----------------------------------------------------------------------------

--BEGIN TRY
--BEGIN TRANSACTION

--DECLARE @numFormFieldID NUMERIC(18,0)

--INSERT INTO DynamicFormFieldMaster
--(
--	numFormID,vcFormFieldName,vcFieldType,vcAssociatedControlType,vcDbColumnName,vcListItemType,numListID,bitDeleted,vcFieldDataType,vcOrigDbColumnName,bitAllowEdit,vcLookBackTableName,bitDefault,bitInResults
--)
--VALUES
--(
--	29,'Item Category','R','Label','numCategoryID','',0,0,'N','numCategoryID',1,'category',0,1
--)

--SET @numFormFieldID = SCOPE_IDENTITY()

--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,bitInResults,bitDeleted,bitDefault,bitAllowFiltering,numFormFieldID,intSectionID
--)
--VALUES
--(
--	4,311,29,1,1,'Item Category','Label',1,0,0,1,@numFormFieldID,1
--)


--COMMIT
--END TRY
--BEGIN CATCH
--	DECLARE @ErrorMessage NVARCHAR(4000)
--	DECLARE @ErrorNumber INT
--	DECLARE @ErrorSeverity INT
--	DECLARE @ErrorState INT
--	DECLARE @ErrorLine INT
--	DECLARE @ErrorProcedure NVARCHAR(200)

--	IF @@TRANCOUNT > 0
--		ROLLBACK TRANSACTION;

--	SELECT 
--		@ErrorMessage = ERROR_MESSAGE(),
--		@ErrorNumber = ERROR_NUMBER(),
--		@ErrorSeverity = ERROR_SEVERITY(),
--		@ErrorState = ERROR_STATE(),
--		@ErrorLine = ERROR_LINE(),
--		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

--	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
--END CATCH

-----------------------------------------------------------------------------


--ALTER TABLE Sites ADD intPaymentGateWay NUMERIC(18,0)


--BEGIN TRY
--BEGIN TRANSACTION

--DECLARE @numFieldID NUMERIC(18,0)

--INSERT INTO DycFieldMaster
--(
--	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAllowFiltering
--)
--VALUES
--(
--	3,'Invoiced','vcInvoiced','vcInvoiced','OpportunityItems','N','R','Label',0,0,0,0,0,0,0
--)

--SET @numFieldID = SCOPE_IDENTITY()

--INSERT INTO ReportFieldGroupMappingMaster
--(
--	numReportFieldGroupID,numFieldID,vcFieldName,vcAssociatedControlType,vcFieldDataType,bitAllowSorting,bitAllowGrouping,bitAllowAggregate,bitAllowFiltering
--)
--VALUES
--(
--	4,@numFieldID,'Invoiced','Label','N',1,0,1,0
--)

--INSERT INTO DycFieldMaster
--(
--	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAllowFiltering
--)
--VALUES
--(
--	3,'Backordered','vcBackordered','vcBackordered','OpportunityItems','N','R','Label',0,0,0,0,0,0,0
--)

--SET @numFieldID = SCOPE_IDENTITY()

--INSERT INTO ReportFieldGroupMappingMaster
--(
--	numReportFieldGroupID,numFieldID,vcFieldName,vcAssociatedControlType,vcFieldDataType,bitAllowSorting,bitAllowGrouping,bitAllowAggregate,bitAllowFiltering
--)
--VALUES
--(
--	4,@numFieldID,'Backordered','Label','N',1,0,1,0
--)

--INSERT INTO DycFieldMaster
--(
--	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAllowFiltering
--)
--VALUES
--(
--	3,'Open Balance','vcOpenBalance','vcOpenBalance','OpportunityItems','N','R','Label',0,0,0,0,0,0,0
--)

--SET @numFieldID = SCOPE_IDENTITY()

--INSERT INTO ReportFieldGroupMappingMaster
--(
--	numReportFieldGroupID,numFieldID,vcFieldName,vcAssociatedControlType,vcFieldDataType,bitAllowSorting,bitAllowGrouping,bitAllowAggregate,bitAllowFiltering
--)
--VALUES
--(
--	4,@numFieldID,'Open Balance','Label','N',1,0,1,0
--)

--COMMIT
--END TRY
--BEGIN CATCH
--	DECLARE @ErrorMessage NVARCHAR(4000)
--	DECLARE @ErrorNumber INT
--	DECLARE @ErrorSeverity INT
--	DECLARE @ErrorState INT
--	DECLARE @ErrorLine INT
--	DECLARE @ErrorProcedure NVARCHAR(200)

--	IF @@TRANCOUNT > 0
--		ROLLBACK TRANSACTION;

--	SELECT 
--		@ErrorMessage = ERROR_MESSAGE(),
--		@ErrorNumber = ERROR_NUMBER(),
--		@ErrorSeverity = ERROR_SEVERITY(),
--		@ErrorState = ERROR_STATE(),
--		@ErrorLine = ERROR_LINE(),
--		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

--	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
--END CATCH


----ALTER TABLE DepositeDetails ADD numTempOrigDepositID UNIQUEIDENTIFIER

--ALTER TABLE Domain ADD bitElasticSearch BIT DEFAULT 1

--USE [Production.2014]
--GO

--/****** Object:  Table [dbo].[ElasticSearchModifiedRecords]    Script Date: 10-Jul-17 10:41:46 AM ******/
--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

--SET ANSI_PADDING ON
--GO

--CREATE TABLE [dbo].[ElasticSearchModifiedRecords](
--	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
--	[numDomainID] [numeric](18, 0) NOT NULL,
--	[vcModule] [varchar](15) NOT NULL,
--	[numRecordID] [numeric](18, 0) NOT NULL,
--	[vcAction] [varchar](10) NOT NULL,
-- CONSTRAINT [PK_ElasticSearchModifiedRecords] PRIMARY KEY CLUSTERED 
--(
--	[ID] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
--) ON [PRIMARY]

--GO

--SET ANSI_PADDING ON
--GO




----------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------

-- CHECK FORM CONFIGURATION FOR ITEM DISPLAY AND SEARCH SETTING FOR ALL DOMAIN AND REMOVE DUPLICATE BY USERCNTID BECAUSE ITS A DOMAIN LEVEL SETTING AND SET USERCNTID TO 0
--SELECT Distinct numDomainId,numFormId,numUserCntID,numRelCntType FROM DycFormConfigurationDetails WHERE numFormId=22 ORDER BY numDomainId


----------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------

--INSERT INTO DynamicFormMaster
--(
--	numFormId,vcFormName,cCustomFieldsAssociated,cAOIAssociated,bitDeleted,tintFlag
--)
--VALUES
--(
--	133,'Import/Update Organization','Y','N',0,2
--),
--(
--	134,'Import/Update Contact','Y','N',0,2
--)

--------------------------------------------------------------------------

--INSERT INTO DycFormField_Mapping
--(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,PopupFunctionName,[order],tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitImport,bitAllowFiltering,numFormFieldID,intSectionID,bitAllowGridColor) 
--SELECT 
--	numModuleID,numFieldID,133,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,PopupFunctionName,[order],tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitImport,bitAllowFiltering,numFormFieldID,intSectionID,bitAllowGridColor
--FROM 
--	DycFormField_Mapping 
--WHERE 
--	numFormID=36 
--	AND bitImport=1 
--	AND numFieldID NOT IN (51,52,53,56,59,60,61,62,63,64,65,66,67,68,82,83,84,85,86,94,386,387,89,347)

------------------------------------------------------------------------

--INSERT INTO DycFormField_Mapping
--(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,PopupFunctionName,[order],tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitImport,bitAllowFiltering,numFormFieldID,intSectionID,bitAllowGridColor) 
--SELECT 
--	2,numFieldID,134,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,PopupFunctionName,[order],tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitImport,bitAllowFiltering,numFormFieldID,intSectionID,bitAllowGridColor
--FROM 
--	DycFormField_Mapping 
--WHERE 
--	numFormID=36 
--	AND bitImport=1 
--	AND numFieldID IN (51,52,53,56,59,60,61,62,63,64,65,66,67,68,82,83,84,85,86,94,386,387,380)


----------------------------------------------------------------------------------------

--BEGIN TRY
--BEGIN TRANSACTION
--	UPDATE DycFieldMaster SET vcDbColumnName='numDivisionID',vcOrigDbColumnName='numDivisionID',vcPropertyName='numDivisionID',vcLookBackTableName='DivisionMaster' WHERE numFieldId=539

--	INSERT INTO DycFormField_Mapping
--	(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitImport,bitAllowFiltering,intSectionID,bitAllowGridColor) 
--	VALUES
--	(1,539,133,0,0,'Organization ID','Label','DivisionID',1,0,1,1,0,1,1,1,1,1,0)

--	INSERT INTO DycFormField_Mapping
--	(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitImport,bitAllowFiltering,intSectionID,bitAllowGridColor) 
--	VALUES
--	(1,539,134,0,0,'Organization ID','Label','DivisionID',1,0,1,1,0,1,1,1,1,1,0)

--	INSERT INTO DycFormField_Mapping
--	(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitImport,bitAllowFiltering,intSectionID,bitAllowGridColor) 
--	VALUES
--	(1,860,134,0,0,'Contact ID','Label','ContactID',1,0,1,1,0,1,1,1,1,1,0)
--COMMIT
--END TRY
--BEGIN CATCH
--	DECLARE @ErrorMessage NVARCHAR(4000)
--	DECLARE @ErrorNumber INT
--	DECLARE @ErrorSeverity INT
--	DECLARE @ErrorState INT
--	DECLARE @ErrorLine INT
--	DECLARE @ErrorProcedure NVARCHAR(200)

--	IF @@TRANCOUNT > 0
--		ROLLBACK TRANSACTION;

--	SELECT 
--		@ErrorMessage = ERROR_MESSAGE(),
--		@ErrorNumber = ERROR_NUMBER(),
--		@ErrorSeverity = ERROR_SEVERITY(),
--		@ErrorState = ERROR_STATE(),
--		@ErrorLine = ERROR_LINE(),
--		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

--	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
--END CATCH

------------------------------------------------------------------------
--ALTER TABLE DycFieldMaster ALTER COLUMN vcFieldName VARCHAR(100)
--ALTER TABLE DynamicFormField_Validation ALTER COLUMN vcNewFormFieldName VARCHAR(100)
--ALTER TABLE DycFormField_Mapping ALTER COLUMN vcFieldName VARCHAR(100)
--ALTER TABLE Import_File_Master ADD tintImportType TINYINT

--UPDATE DycFieldMaster SET vcPropertyName = 'DivisionID' WHERE numFieldId=539
--UPDATE DycFormField_Mapping SET vcPropertyName = 'DivisionID' WHERE numFieldId=539
--UPDATE DycFieldMaster SET vcPropertyName = 'ContactID' WHERE numFieldId=860
--UPDATE DycFormField_Mapping SET vcPropertyName = 'ContactID' WHERE numFieldId=860
--UPDATE DycFormField_Mapping SET vcPropertyName = 'ContactPhoneExt' WHERE numFormID=134 AND numFieldID=60
--UPDATE DycFormField_Mapping SET vcPropertyName = 'CellPhone' WHERE numFormID=134 AND numFieldID=61
--UPDATE DycFormField_Mapping SET vcPropertyName = 'Team' WHERE numFormID=134 AND numFieldID=65
--UPDATE DycFormField_Mapping SET vcPropertyName = 'Department' WHERE numFormID=134 AND numFieldID=68

--UPDATE DycFormField_Mapping SET vcFieldName = 'Relationship (e.g. Customer, Vendor, etc�)' WHERE numFormID=133 AND numFieldID=6
--UPDATE DycFormField_Mapping SET vcFieldName = 'Relationship Type (e.g. Leads, Prospect or Accounts)' WHERE numFormID=133 AND numFieldID=451
--UPDATE DycFormField_Mapping SET vcFieldName = 'Group (e.g. My Lead, Web Lead  or Public Lead)' WHERE numFormID=133 AND numFieldID=19
--UPDATE DycFormField_Mapping SET vcFieldName = 'Item Type (e.g. Inventory Item, Non-Inventory Item or Service)' WHERE numFormID=20 AND numFieldID=294

--DELETE FROM DycFormField_Mapping WHERE numFormID=20 AND numFieldID IN (195,197,200,235,293,233,469,470,471,320,390,391,236,237)

--INSERT INTO DycFormSectionDetail (numFormID,intSectionID,vcSectionName) VALUES (20,5,'Item Price Level Details')

--UPDATE DycFormField_Mapping SET numFormID=20,intSectionID=5 WHERE numFormID=132 AND numFieldID NOT IN (193,203,211,236,237,281)


--UPDATE DynamicFormMaster SET vcFormName='Items' WHERE numFormId=20
--UPDATE DynamicFormMaster SET vcFormName='Organizations' WHERE numFormId=133
--UPDATE DynamicFormMaster SET vcFormName='Contacts' WHERE numFormId=134
--UPDATE DynamicFormMaster SET vcFormName='Item Warehouses' WHERE numFormId=48
--UPDATE DynamicFormMaster SET vcFormName='Assembly/Kit Child Items' WHERE numFormId=31
--UPDATE DynamicFormMaster SET vcFormName='Item Images' WHERE numFormId=54
--UPDATE DynamicFormMaster SET vcFormName='Item Vendors' WHERE numFormId=55
--UPDATE DynamicFormMaster SET vcFormName='Organization Correspondence' WHERE numFormId=43
--UPDATE DynamicFormMaster SET vcFormName='Address Details' WHERE numFormId=98



--INSERT INTO DycFormSectionDetail (numFormID,intSectionID,vcSectionName,Loc_id) VALUES (133,1,'Organization Detail',0),(133,2,'Organization Custom Fields',1)
--INSERT INTO DycFormSectionDetail (numFormID,intSectionID,vcSectionName,Loc_id) VALUES (134,1,'Contact Detail',0),(134,2,'Contact Custom Fields',4)
 
------------------------------------------------

--DELETE FROM ImportOrganizationContactReference WHERE numDivisionID NOT IN (SELECT numDivisionID FROM DivisionMaster)

--USE [Production.2014]
--GO

--ALTER TABLE [dbo].[ImportOrganizationContactReference]  WITH CHECK ADD  CONSTRAINT [FK_ImportOrganizationContactReference_DivisionMaster] FOREIGN KEY([numDivisionID])
--REFERENCES [dbo].[DivisionMaster] ([numDivisionID])
--ON DELETE CASCADE
--GO

--ALTER TABLE [dbo].[ImportOrganizationContactReference] CHECK CONSTRAINT [FK_ImportOrganizationContactReference_DivisionMaster]
--GO


-------------------------------------------------------
--DELETE FROM ImportActionItemReference WHERE numDivisionID NOT IN (SELECT numDivisionID FROM DivisionMaster)
--DELETE FROM ImportActionItemReference WHERE numContactID NOT IN (SELECT numContactID FROM AdditionalContactsInformation)

--USE [Production.2014]
--GO

--ALTER TABLE [dbo].[ImportActionItemReference]  WITH CHECK ADD  CONSTRAINT [FK_ImportActionItemReference_AdditionalContactsInformation] FOREIGN KEY([numContactID])
--REFERENCES [dbo].[AdditionalContactsInformation] ([numContactId])
--ON DELETE CASCADE
--GO

--ALTER TABLE [dbo].[ImportActionItemReference] CHECK CONSTRAINT [FK_ImportActionItemReference_AdditionalContactsInformation]
--GO


--USE [Production.2014]
--GO

--ALTER TABLE [dbo].[ImportActionItemReference]  WITH CHECK ADD  CONSTRAINT [FK_ImportActionItemReference_DivisionMaster] FOREIGN KEY([numDivisionID])
--REFERENCES [dbo].[DivisionMaster] ([numDivisionID])
--ON DELETE CASCADE
--GO

--ALTER TABLE [dbo].[ImportActionItemReference] CHECK CONSTRAINT [FK_ImportActionItemReference_DivisionMaster]
--GO


--UPDATE DycFormField_Mapping SET numFieldID=211 WHERE numFieldID=382 AND numFormID=54
--DELETE FROM DycFormField_Mapping WHERE numFormID=55 AND vcPropertyName='LeadTimeDays'


--ALTER TABLE Domain ADD bitElasticSearch BIT DEFAULT 1

--UPDATE Domain SET bitElasticSearch=1 WHERE numDomainId IN (1,72,153,170,172,204,206)

---------------------------------------------------------------
---------------------------------------------------------------
---------------------------------------------------------------

--USE [Production.2014]
--GO

--/****** Object:  Table [dbo].[MassStockTransfer]    Script Date: 01-May-17 6:16:25 PM ******/
--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

--SET ANSI_PADDING ON
--GO

--CREATE TABLE [dbo].[MassStockTransfer](
--	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
--	[numDomainID] [numeric](18, 0) NOT NULL,
--	[numItemCode] [numeric](18, 0) NOT NULL,
--	[numFromWarehouseItemID] [numeric](18, 0) NOT NULL,
--	[numToWarehouseItemID] [numeric](18, 0) NOT NULL,
--	[numQty] [numeric](18, 0) NOT NULL,
--	[numTransferredBy] [numeric](18, 0) NOT NULL,
--	[dtTransferredDate] [datetime] NOT NULL,
--	[bitMassTransfer] [bit] NOT NULL,
--	[vcSerialLot] [varchar](max) NULL,
--	[numFromQtyBefore] [decimal](18, 4) NOT NULL,
--	[numToQtyBefore] [decimal](18, 4) NOT NULL,
--	[numFromQtyAfter] [decimal](18, 4) NOT NULL,
--	[numToQtyAfter] [decimal](18, 4) NOT NULL,
-- CONSTRAINT [PK_MassStockTransfer] PRIMARY KEY CLUSTERED 
--(
--	[ID] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
--) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

--GO

--SET ANSI_PADDING ON
--GO

----------------------------------------------------------------------------------


--UPDATE PageNavigationDTL SET vcAddURL='~/Opportunity/frmMassStockTransfer.aspx' WHERE vcPageNavName='Stock Transfer'

-----------------------------------------------------------------------------------

--ALTER TABLE Domain ADD bitRemoveGlobalLocation BIT DEFAULT 1

-------------------------------------------------------------------------------------


--BEGIN TRY
--BEGIN TRANSACTION
--	DECLARE @numFieldID NUMERIC(18,0)

--	INSERT INTO DycFieldMaster
--	(
--		numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,numListID,bitInlineEdit,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,vcGroup
--	)
--	VALUES
--	(
--		1,'e-commerce Access','bitEcommerceAccess','bitEcommerceAccess','IsEcommerceAccess','ExtarnetAccounts','Y','R','CheckBox',0,1,0,1,0,1,1,1,1,1,'Org.Details Fields'
--	)

--	SELECT @numFieldID = SCOPE_IDENTITY()

--	INSERT INTO DycFormField_Mapping
--	(
--		numModuleID, numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting
--	)
--	VALUES
--	(
--		1,@numFieldID,34,1,1,'e-commerce Access','CheckBox','IsEcommerceAccess',1,0,0,1,1,1,0
--	),
--	(
--		1,@numFieldID,35,1,1,'e-commerce Access','CheckBox','IsEcommerceAccess',1,0,0,1,1,1,0
--	),
--	(
--		1,@numFieldID,36,1,1,'e-commerce Access','CheckBox','IsEcommerceAccess',1,0,0,1,1,1,0
--	)


--	INSERT INTO DycFieldMaster
--	(
--		numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,numListID,bitInlineEdit,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,vcGroup
--	)
--	VALUES
--	(
--		2,'e-commerce password','vcPassword','vcPassword','vcPassword','ExtranetAccountsDtl','V','R','TextBox',0,1,0,1,0,1,1,1,1,1,'Contact Fields'
--	)

--	SELECT @numFieldID = SCOPE_IDENTITY()

--	INSERT INTO DycFormField_Mapping
--	(
--		numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting
--	)
--	VALUES
--	(
--		2,@numFieldID,10,1,1,'e-commerce password','TextBox','vcPassword',1,0,0,1,1,1,0
--	)

--COMMIT
--END TRY
--BEGIN CATCH
--	DECLARE @ErrorMessage NVARCHAR(4000)
--	DECLARE @ErrorNumber INT
--	DECLARE @ErrorSeverity INT
--	DECLARE @ErrorState INT
--	DECLARE @ErrorLine INT
--	DECLARE @ErrorProcedure NVARCHAR(200)

--	IF @@TRANCOUNT > 0
--		ROLLBACK TRANSACTION;

--	SELECT 
--		@ErrorMessage = ERROR_MESSAGE(),
--		@ErrorNumber = ERROR_NUMBER(),
--		@ErrorSeverity = ERROR_SEVERITY(),
--		@ErrorState = ERROR_STATE(),
--		@ErrorLine = ERROR_LINE(),
--		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

--	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
--END CATCH


--------------------------------------------

--ALTER TABLE Item ADD numManufacturer NUMERIC(18,0)

---------------------------------------------------------------
---------------------------------------------------------------
---------------------------------------------------------------

--ALTER TABLE Import_File_Master ADD tintItemLinkingID TINYINT


--INSERT INTO DynamicFormMaster 
--(numFormId,vcFormName,cCustomFieldsAssociated,cAOIAssociated,bitDeleted,tintFlag)
--VALUES
--(132,'Item Price Level','N','N',0,2)

---------------------------------------------------------------------------------

--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,bitInResults,bitDeleted,bitDefault,bitImport
--)
--VALUES
--(
--	4,193,132,0,0,'Model ID','TextBox',1,0,0,1
--),
--(
--	4,203,132,0,0,'UPC','TextBox',1,0,0,1
--),
--(
--	4,211,132,0,0,'Item ID','TextBox',1,0,0,1
--),
--(
--	4,236,132,0,0,'UPC (M)','TextBox',1,0,0,1
--),
--(
--	4,237,132,0,0,'SKU (M)','TextBox',1,0,0,1
--),
--(
--	4,281,132,0,0,'SKU','TextBox',1,0,0,1
--),
--(
--	4,470,132,0,0,'Price Rule Type','SelectBox',1,0,0,1
--),
--(
--	4,471,132,0,0,'Price Level Discount Type','SelectBox',1,0,0,1
--)



--BEGIN TRY
--	BEGIN TRANSACTION
--		DECLARE @numFieldID NUMERIC(18,0)
--		DECLARE @i AS INT = 1
--		WHILE @i <= 20
--		BEGIN

--			INSERT INTO DycFieldMaster
--			(
--				numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcListItemType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitImport,bitAllowFiltering
--			)
--			VALUES
--			(
--				4,CONCAT('Price Level ',@i),CONCAT('monPriceLevel',@i),CONCAT('monPriceLevel',@i),CONCAT('PriceLevel',@i),'PricingTable','N','R','TextBox','',0,1,0,1,0,1,1,1,1,1,1
--			)

--			SET @numFieldID = SCOPE_IDENTITY();

--			INSERT INTO DycFormField_Mapping
--			(
--				numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,bitInResults,bitDeleted,bitDefault,bitImport
--			)
--			VALUES
--			(
--				4,@numFieldID,132,0,0,CONCAT('Price Level ',@i),'TextBox',1,0,0,1
--			)

--			SET @i = @i + 1;
--		END
--	COMMIT
--END TRY
--BEGIN CATCH
--	ROLLBACK
--END CATCH


--BEGIN TRY
--	BEGIN TRANSACTION

--		DECLARE @numFieldID NUMERIC(18,0)
--		DECLARE @i AS INT = 1
--		WHILE @i <= 20
--		BEGIN

--			INSERT INTO DycFieldMaster
--			(
--				numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcListItemType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitImport,bitAllowFiltering
--			)
--			VALUES
--			(
--				4,CONCAT('Price Level ',@i,' Name'),CONCAT('vcPriceLevel',@i,'Name'),CONCAT('vcPriceLevel',@i,'Name'),CONCAT('PriceLevel',@i,'Name'),'PricingTable','V','R','TextBox','',0,1,0,1,0,1,1,1,1,1,1
--			)

--			SET @numFieldID = SCOPE_IDENTITY();

--			INSERT INTO DycFormField_Mapping
--			(
--				numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,bitInResults,bitDeleted,bitDefault,bitImport
--			)
--			VALUES
--			(
--				4,@numFieldID,132,0,0,CONCAT('Price Level ',@i,' Name'),'TextBox',1,0,0,1
--			)

--			SET @i = @i + 1;
--		END
--	COMMIT
--END TRY
--BEGIN CATCH
--	ROLLBACK
--END CATCH


--BEGIN TRY
--	BEGIN TRANSACTION

--		DECLARE @numFieldID NUMERIC(18,0)
--		DECLARE @i AS INT = 1
--		WHILE @i <= 20
--		BEGIN

--			INSERT INTO DycFieldMaster
--			(
--				numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcListItemType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitImport,bitAllowFiltering
--			)
--			VALUES
--			(
--				4,CONCAT('Price Level ',@i,' Qty From'),CONCAT('intPriceLevel',@i,'FromQty'),CONCAT('intPriceLevel',@i,'FromQty'),CONCAT('PriceLevel',@i,'QtyFrom'),'PricingTable','N','R','TextBox','',0,1,0,1,0,1,1,1,1,1,1
--			)

--			SET @numFieldID = SCOPE_IDENTITY();

--			INSERT INTO DycFormField_Mapping
--			(
--				numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,bitInResults,bitDeleted,bitDefault,bitImport
--			)
--			VALUES
--			(
--				4,@numFieldID,132,0,0,CONCAT('PriceLevel ',@i,' Qty From'),'TextBox',1,0,0,1
--			)

--			INSERT INTO DycFieldMaster
--			(
--				numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcListItemType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitImport,bitAllowFiltering
--			)
--			VALUES
--			(
--				4,CONCAT('Price Level ',@i,' Qty To'),CONCAT('intPriceLevel',@i,'ToQty'),CONCAT('intPriceLevel',@i,'ToQty'),CONCAT('PriceLevel',@i,'QtyTo'),'PricingTable','N','R','TextBox','',0,1,0,1,0,1,1,1,1,1,1
--			)

--			SET @numFieldID = SCOPE_IDENTITY();

--			INSERT INTO DycFormField_Mapping
--			(
--				numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,bitInResults,bitDeleted,bitDefault,bitImport
--			)
--			VALUES
--			(
--				4,@numFieldID,132,0,0,CONCAT('PriceLevel ',@i,' Qty To'),'TextBox',1,0,0,1
--			)

--			SET @i = @i + 1;
--		END

--	COMMIT
--END TRY
--BEGIN CATCH
--	ROLLBACK
--END CATCH


--UPDATE DycFormField_Mapping SET intSectionID=1 WHERE numFormID=132

--INSERT INTO DycFormSectionDetail
--(
--	numFormID,intSectionID,vcSectionName,Loc_id
--)
--VALUES
--(
--	132,1,'Item Price Level Detail',0
--)


--ALTER TABLE Warehouses ADD numAddressID NUMERIC(18,0)


--ALTER TABLE Domain ADD tintOppStautsForAutoPOBackOrder TINYINT
--ALTER TABLE Domain ADD tintUnitsRecommendationForAutoPOBackOrder TINYINT
--ALTER TABLE VendorShipmentMethod ADD bitPreferredMethod BIT
--ALTER TABLE Vendor DROP COLUMN [intLeadTimeDays]
--ALTER TABLE Vendor ADD vcNotes VARCHAR(300)


--UPDATE Domain SET tintOppStautsForAutoPOBackOrder=0, tintUnitsRecommendationForAutoPOBackOrder=1


--ALTER TABLE OpportunityMaster ADD numVendorAddressID NUMERIC(18,0)
---------------------------------------------------------------
---------------------------------------------------------------
---------------------------------------------------------------


--INSERT INTO ReportModuleGroupFieldMappingMaster
--(
--	numReportModuleGroupID,numReportFieldGroupID
--)
--VALUES
--(19,20),(9,20),(10,20)



--DECLARE @numFieldID NUMERIC(18,0)
--SELECT @numFieldID=numFieldId FROM DycFieldMaster WHERE vcDbColumnName='numPartenerSource' AND vcLookBackTableName='DivisionMaster'


--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering
--)
--VALUES
--(
--	3,@numFieldID,35,1,1,'Partner Source','SelectBox','numPartenerSource',1,0,0,1,1,1,1,1
--),
--(
--	3,@numFieldID,36,1,1,'Partner Source','SelectBox','numPartenerSource',1,0,0,1,1,1,1,1
--)


-------------------------------

--USE [Production.2014]
--GO

--/****** Object:  Table [dbo].[EcommerceRelationshipProfile]    Script Date: 06-Mar-17 12:29:02 PM ******/
--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

--CREATE TABLE [dbo].[EcommerceRelationshipProfile](
--	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
--	[numSiteID] [numeric](18, 0) NOT NULL,
--	[numRelationship] [numeric](18, 0) NOT NULL,
--	[numProfile] [numeric](18, 0) NOT NULL,
-- CONSTRAINT [PK_EcommerceRelationshipProfile] PRIMARY KEY CLUSTERED 
--(
--	[ID] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
--) ON [PRIMARY]

--GO

--ALTER TABLE [dbo].[EcommerceRelationshipProfile]  WITH CHECK ADD  CONSTRAINT [FK_EcommerceRelationshipProfile_Sites] FOREIGN KEY([numSiteID])
--REFERENCES [dbo].[Sites] ([numSiteID])
--ON DELETE CASCADE
--GO

--ALTER TABLE [dbo].[EcommerceRelationshipProfile] CHECK CONSTRAINT [FK_EcommerceRelationshipProfile_Sites]
--GO


------------------------------------


--USE [Production.2014]
--GO

--/****** Object:  Table [dbo].[ECommerceItemClassification]    Script Date: 06-Mar-17 12:29:19 PM ******/
--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

--CREATE TABLE [dbo].[ECommerceItemClassification](
--	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
--	[numECommRelatiionshipProfileID] [numeric](18, 0) NOT NULL,
--	[numItemClassification] [numeric](18, 0) NOT NULL,
-- CONSTRAINT [PK_ECommerceItemClassification] PRIMARY KEY CLUSTERED 
--(
--	[ID] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
--) ON [PRIMARY]

--GO

--ALTER TABLE [dbo].[ECommerceItemClassification]  WITH CHECK ADD  CONSTRAINT [FK_ECommerceItemClassification_EcommerceRelationshipProfile] FOREIGN KEY([numECommRelatiionshipProfileID])
--REFERENCES [dbo].[EcommerceRelationshipProfile] ([ID])
--ON DELETE CASCADE
--GO

--ALTER TABLE [dbo].[ECommerceItemClassification] CHECK CONSTRAINT [FK_ECommerceItemClassification_EcommerceRelationshipProfile]
--GO


------------------------------------------------

--USE [Production.2014]
--GO

--/****** Object:  Index [NonClusteredIndex-20170304-104654]    Script Date: 06-Mar-17 12:29:43 PM ******/
--CREATE NONCLUSTERED INDEX [NonClusteredIndex-20170304-104654] ON [dbo].[EcommerceRelationshipProfile]
--(
--	[numSiteID] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
--GO


-----------------------------------

--ALTER TABLE BizDocComission ADD tintAssignTo TINYINT
--ALTER TABLE ECommerceDTL ADD tintPreLoginProceLevel TINYINT
--UPDATE ECommerceDTL SET tintPreLoginProceLevel = 0

---------------------------------------------------------------
---------------------------------------------------------------
---------------------------------------------------------------

--SET IDENTITY_INSERT dbo.PaymentGateway ON 

--INSERT INTO PaymentGateway 
--(
--	intPaymentGateWay,vcGateWayName,vcFirstFldName,vcSecndFldName
--)
--VALUES
--(
--	9,'BluePay','Login','Password'
--)

--SET IDENTITY_INSERT dbo.PaymentGateway OFF 


------------------------------------------

--ALTER TABLE [dbo].[eCommerceDTL] DROP CONSTRAINT [DF_eCommerceDTL_bitHidePriceUser]
--ALTER TABLE eCommerceDTL DROP COLUMN bitHidePriceAfterLogin
--ALTER TABLE eCommerceDTL DROP COLUMN numRelationshipIdHidePrice
--ALTER TABLE eCommerceDTL DROP COLUMN numProfileIDHidePrice
--ALTER TABLE eCommerceDTL DROP COLUMN bitEnableDefaultAccounts
--ALTER TABLE eCommerceDTL DROP COLUMN numDefaultAssetChartAcntId
--ALTER TABLE eCommerceDTL DROP COLUMN numDefaultIncomeChartAcntId
--ALTER TABLE eCommerceDTL DROP COLUMN numDefaultCOGSChartAcntId

--ALTER TABLE Item ADD bitMatrix BIT

------------------------------------------------------------------
--USE [Production.2014]
--GO

--/****** Object:  Table [dbo].[ItemAttributes]    Script Date: 27-Feb-17 2:41:55 PM ******/
--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

--CREATE TABLE [dbo].[ItemAttributes](
--	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
--	[numDomainID] [numeric](18, 0) NOT NULL,
--	[numItemCode] [numeric](18, 0) NOT NULL,
--	[FLD_ID] [numeric](18, 0) NOT NULL,
--	[FLD_Value] [numeric](18, 0) NOT NULL,
-- CONSTRAINT [PK_ItemAttributes] PRIMARY KEY CLUSTERED 
--(
--	[ID] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
--) ON [PRIMARY]

--GO

-------------------------------------------------


--USE [Production.2014]
--GO

--/****** Object:  Index [NCIX_20170220-202158]    Script Date: 27-Feb-17 2:51:43 PM ******/
--CREATE NONCLUSTERED INDEX [NCIX_20170220-202158] ON [dbo].[ItemAttributes]
--(
--	[numDomainID] ASC,
--	[numItemCode] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
--GO


------------------------------------------------


--BEGIN TRY
--BEGIN TRANSACTION

--	DECLARE @numFieldID INT

--	INSERT INTO DycFieldMaster
--	(
--		numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcLookBackTableName,vcPropertyName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcListItemType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting
--	)
--	VALUES
--	(
--		4,'Matrix','bitMatrix','bitMatrix','Item','IsMatrix','Y','R','CheckBox','',0,1,0,1,1,1,1,1,1
--	) 


--	SELECT @numFieldID = SCOPE_IDENTITY()


--	INSERT INTO DycFormField_Mapping
--	(
--		numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcPropertyName,vcAssociatedControlType,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,bitAllowGridColor,bitImport,intSectionID
--	)
--	VALUES
--	(
--		4,@numFieldID,20,1,1,'Matrix','IsMatrix','CheckBox',NULL,1,0,0,1,0,1,1,1,0,1,1
--	)

--COMMIT
--END TRY
--BEGIN CATCH
--	IF @@TRANCOUNT > 0
--		ROLLBACK TRANSACTION;

--	SELECT 
--		ERROR_MESSAGE(),
--		ERROR_NUMBER(),
--		ERROR_SEVERITY(),
--		ERROR_STATE(),
--		ERROR_LINE(),
--		ISNULL(ERROR_PROCEDURE(), '-');
--END CATCH

---------------------------------------------------------------
---------------------------------------------------------------
---------------------------------------------------------------

--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowFiltering,bitAllowSorting
--)
--VALUES
--(
--	1,5,43,0,0,'Organization Profile','SelectBox','',1,0,1,1,0,0,1,1
--)

--UPDATE DycFormField_Mapping SET vcFieldName='Active' WHERE numFormID=1 AND numFieldID=41


--UPDATE BizFormWizardMasterConfiguration SET tintPageType=1 WHERE numFormID IN (34,35,36,10,13,12,43,38,39,40,41) AND bitGridConfiguration=1 AND tintPageType=0


--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
--)
--VALUES
--(
--	4,273,21,1,1,'Item Classification','SelectBox',1,0,1,1,1,1
--)


--DELETE 
--	W 
--FROM 
--	DycFormConfigurationDetails w
--INNER JOIN
--	Domain 
--ON
--	Domain.numDomainId = w.numDomainId
--WHERE
--	w.numFormId = 96
--	AND w.numUserCntID <> Domain.numAdminID

--DELETE 
--	W 
--FROM 
--	DycFormConfigurationDetails w
--INNER JOIN
--	Domain 
--ON
--	Domain.numDomainId = w.numDomainId
--WHERE
--	w.numFormId = 97
--	AND w.numUserCntID <> Domain.numAdminID


--INSERT INTO ReportFieldGroupMappingMaster
--(
--	numReportFieldGroupID,numFieldID,vcFieldName,vcAssociatedControlType,vcFieldDataType,bitAllowSorting,bitAllowGrouping,bitAllowAggregate,bitAllowFiltering
--)
--VALUES
--(
--	3,262,'Discount','TextBox','M',1,1,1,1
--),
--(
--	4,262,'Discount','TextBox','M',1,1,1,1
--)

--UPDATE DycFormField_Mapping SET vcFieldName='List Price' WHERE numFormID=21 AND numFieldID=190

--UPDATE PageMaster SET bitIsUpdateApplicable=1 WHERE numPageID=87 AND numModuleID=35

--UPDATE GroupAuthorization SET intUpdateAllowed=3 WHERE numPageID=87 AND numModuleID=35


--BEGIN TRY
--BEGIN TRANSACTION

--	DECLARE @numFieldID INT

--	INSERT INTO DycFieldMaster
--	(
--		numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcLookBackTableName,vcPropertyName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcListItemType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting
--	)
--	VALUES
--	(
--		1,'Email to Case','bitEmailToCase','bitEmailToCase','DivisionMaster','bitEmailToCase','Y','R','CheckBox','',0,1,0,1,1,1,1,1,1
--	) 


--	SELECT @numFieldID = SCOPE_IDENTITY()


--	INSERT INTO DycFormField_Mapping
--	(
--		numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcPropertyName,vcAssociatedControlType,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,bitAllowGridColor
--	)
--	VALUES
--	(
--		1,@numFieldID,36,1,1,'Email to Case','bitEmailToCase','CheckBox',16,1,0,0,1,0,1,1,1,0
--	)

--COMMIT
--END TRY
--BEGIN CATCH
--	IF @@TRANCOUNT > 0
--		ROLLBACK TRANSACTION;

--	SELECT 
--		ERROR_MESSAGE(),
--		ERROR_NUMBER(),
--		ERROR_SEVERITY(),
--		ERROR_STATE(),
--		ERROR_LINE(),
--		ISNULL(ERROR_PROCEDURE(), '-');
--END CATCH


--ALTER TABLE SalesOrderConfiguration ADD bitDisplayShipVia BIT

--UPDATE ListDetails SET numListID=82,vcData='Will-call',bintCreatedDate='2007-12-27 12:10:48.000',numModifiedBy=1,bintModifiedDate='2007-12-27 12:10:48.000',bitDelete=0,constFlag=1 WHERE numListItemID=92


--BEGIN TRY
--BEGIN TRANSACTION

--	DECLARE @numFieldID INT

--	INSERT INTO DycFieldMaster
--	(
--		numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcListItemType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitDetailField,bitAllowSorting,bitAllowFiltering,bitInlineEdit
--	)
--	VALUES
--	(
--		3,'Ship Via','numShipmentMethod','numShipmentMethod','ShipmentMethod','OpportunityMaster','N','R','SelectBox','LI',82,1,0,1,0,1,1,1,1,1
--	)


--	SELECT @numFieldID = SCOPE_IDENTITY()


--	INSERT INTO DycFormField_Mapping
--	(
--		numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcPropertyName,vcAssociatedControlType,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,bitAllowGridColor
--	)
--	VALUES
--	(
--		3,@numFieldID,39,1,1,'Ship Via','ShipmentMethod','SelectBox',16,1,0,0,1,0,1,1,1,0
--	)

--COMMIT
--END TRY
--BEGIN CATCH
--	IF @@TRANCOUNT > 0
--		ROLLBACK TRANSACTION;

--	SELECT 
--		ERROR_MESSAGE(),
--		ERROR_NUMBER(),
--		ERROR_SEVERITY(),
--		ERROR_STATE(),
--		ERROR_LINE(),
--		ISNULL(ERROR_PROCEDURE(), '-');
--END CATCH

--BEGIN TRY
--BEGIN TRANSACTION

--	DECLARE @numFieldID INT

--	INSERT INTO DycFieldMaster
--	(
--		numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcLookBackTableName,vcPropertyName,vcFieldDataType,vcFieldType,vcAssociatedControlType,PopupFunctionName,vcListItemType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting
--	)
--	VALUES
--	(
--		1,'Credit Cards','vcCreditCards','vcCreditCards','CustomerCreditCardInfo','vcCreditCards','V','R','Popup','OpenManageCC','',0,1,0,1,0,0,1,1,0
--	) 


--	SELECT @numFieldID = SCOPE_IDENTITY()


--	INSERT INTO DycFormField_Mapping
--	(
--		numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcPropertyName,vcAssociatedControlType,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,bitAllowGridColor
--	)
--	VALUES
--	(
--		1,@numFieldID,36,1,1,'Credit Cards','vcCreditCards','Popup',16,1,0,0,0,0,1,0,0,0
--	)

--COMMIT
--END TRY
--BEGIN CATCH
--	IF @@TRANCOUNT > 0
--		ROLLBACK TRANSACTION;

--	SELECT 
--		ERROR_MESSAGE(),
--		ERROR_NUMBER(),
--		ERROR_SEVERITY(),
--		ERROR_STATE(),
--		ERROR_LINE(),
--		ISNULL(ERROR_PROCEDURE(), '-');
--END CATCH

---------------------------------------------------------------
---------------------------------------------------------------
---------------------------------------------------------------

--UPDATE DycFormField_Mapping SET bitAllowFiltering=0,bitAllowSorting=0 WHERE numFormID=43 AND numFieldID IN (15,120,121)
--UPDATE DycFormField_Mapping SET bitAllowSorting=0 WHERE numFormID=43 AND numFieldID=60


--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,bitSettingField,bitAddField,bitRequired
--)
--VALUES
--(
--	1,3,96,0,0,'Organization Name',1,0,0
--)


--BEGIN TRY
--BEGIN TRANSACTION

	
-- -- 1. Leads - ModileID = 2

--	INSERT INTO PageMaster 
--		(numPageID, numModuleID, vcFileName, vcPageDesc, bitIsViewApplicable, bitIsAddApplicable, bitIsUpdateApplicable, bitIsDeleteApplicable, bitIsExportApplicable)
--	VALUES
--		(132,37,'frmInventoryAdjustmentInBatch.aspx','Manage Inventory',1,0,0,0,0)


--	SELECT 
--		* 
--	INTO 
--		#temp 
--	FROM
--	(
--		SELECT 
--			ROW_NUMBER() OVER(ORDER BY numDomainId DESC) AS Row, 
--			numDomainId 
--		FROM 
--			Domain 
--		WHERE 
--			numDomainId <> -255
--	) TABLE2
	
--	DECLARE @RowCount INT
--	SET @RowCount = (SELECT COUNT(Row) FROM #temp) 
	
--	DECLARE @I INT
--	DECLARE @numDomainId NUMERIC(18,0)
	
--	SET @I = 1

--	WHILE (@I <= @RowCount)
--	BEGIN
			
--		SELECT @numDomainId = numDomainID FROM #temp WHERE Row = @I
	
--		INSERT INTO dbo.GroupAuthorization
--			(numDomainID,numModuleID,numPageID,numGroupID,intExportAllowed,intPrintAllowed,intViewAllowed,intAddAllowed,intUpdateAllowed,intDeleteAllowed)
--		SELECT
--			@numDomainId,37,132,numGroupID,0,0,3,0,0,0
--		FROM
--			AuthenticationGroupMaster
--		WHERE
--			numDomainID = @numDomainId AND
--			tintGroupType = 1


--		SET @I = @I  + 1
--	END
	
--	DROP TABLE #temp
--COMMIT
--END TRY
--BEGIN CATCH
--	IF @@TRANCOUNT > 0
--		ROLLBACK TRANSACTION;

--	SELECT 
--		ERROR_MESSAGE(),
--		ERROR_NUMBER(),
--		ERROR_SEVERITY(),
--		ERROR_STATE(),
--		ERROR_LINE(),
--		ISNULL(ERROR_PROCEDURE(), '-');
--END CATCH


--BEGIN TRY
--BEGIN TRANSACTION

	
-- -- 1. Leads - ModileID = 2

--	INSERT INTO PageMaster 
--		(numPageID, numModuleID, vcFileName, vcPageDesc, bitIsViewApplicable, bitIsAddApplicable, bitIsUpdateApplicable, bitIsDeleteApplicable, bitIsExportApplicable)
--	VALUES
--		(133,8,'frmItemInventoryAndOppOrderReport.aspx','Items Inventory and Total Sales/Purchase Orders Quantity',1,0,0,0,0)


--	SELECT 
--		* 
--	INTO 
--		#temp 
--	FROM
--	(
--		SELECT 
--			ROW_NUMBER() OVER(ORDER BY numDomainId DESC) AS Row, 
--			numDomainId 
--		FROM 
--			Domain 
--		WHERE 
--			numDomainId <> -255
--	) TABLE2
	
--	DECLARE @RowCount INT
--	SET @RowCount = (SELECT COUNT(Row) FROM #temp) 
	
--	DECLARE @I INT
--	DECLARE @numDomainId NUMERIC(18,0)
	
--	SET @I = 1

--	WHILE (@I <= @RowCount)
--	BEGIN
			
--		SELECT @numDomainId = numDomainID FROM #temp WHERE Row = @I
	
--		INSERT INTO dbo.GroupAuthorization
--			(numDomainID,numModuleID,numPageID,numGroupID,intExportAllowed,intPrintAllowed,intViewAllowed,intAddAllowed,intUpdateAllowed,intDeleteAllowed)
--		SELECT
--			@numDomainId,8,133,numGroupID,0,0,3,0,0,0
--		FROM
--			AuthenticationGroupMaster
--		WHERE
--			numDomainID = @numDomainId AND
--			tintGroupType = 1


--		SET @I = @I  + 1
--	END
	
--	DROP TABLE #temp

--	INSERT INTO ReportList
--	( 
--		RptId,NumId,RptHeading,RptDesc,rptGroup,rptSequence,bitEnable
--	)
--	VALUES
--	(
--		(SELECT MAX(RptId) FROM ReportList) + 1,133,'Items Inventory and Total Opp/Orders Quantity','Shows item total inventoty and also quantity sold and purchased.',32,7,1
--	)
--COMMIT
--END TRY
--BEGIN CATCH
--	IF @@TRANCOUNT > 0
--		ROLLBACK TRANSACTION;

--	SELECT 
--		ERROR_MESSAGE(),
--		ERROR_NUMBER(),
--		ERROR_SEVERITY(),
--		ERROR_STATE(),
--		ERROR_LINE(),
--		ISNULL(ERROR_PROCEDURE(), '-');
--END CATCH


-----------------------------------------------------------------


--BEGIN TRY
--BEGIN TRANSACTION

	
-- -- 1. Leads - ModileID = 2

--	INSERT INTO PageMaster 
--		(numPageID, numModuleID, vcFileName, vcPageDesc, bitIsViewApplicable, bitIsAddApplicable, bitIsUpdateApplicable, bitIsDeleteApplicable, bitIsExportApplicable)
--	VALUES
--		(134,11,'','Layout Button',1,0,0,0,0)


--	SELECT 
--		* 
--	INTO 
--		#temp 
--	FROM
--	(
--		SELECT 
--			ROW_NUMBER() OVER(ORDER BY numDomainId DESC) AS Row, 
--			numDomainId 
--		FROM 
--			Domain 
--		WHERE 
--			numDomainId <> -255
--	) TABLE2
	
--	DECLARE @RowCount INT
--	SET @RowCount = (SELECT COUNT(Row) FROM #temp) 
	
--	DECLARE @I INT
--	DECLARE @numDomainId NUMERIC(18,0)
	
--	SET @I = 1

--	WHILE (@I <= @RowCount)
--	BEGIN
			
--		SELECT @numDomainId = numDomainID FROM #temp WHERE Row = @I
	
--		INSERT INTO dbo.GroupAuthorization
--			(numDomainID,numModuleID,numPageID,numGroupID,intExportAllowed,intPrintAllowed,intViewAllowed,intAddAllowed,intUpdateAllowed,intDeleteAllowed)
--		SELECT
--			@numDomainId,11,134,numGroupID,0,0,3,0,0,0
--		FROM
--			AuthenticationGroupMaster
--		WHERE
--			numDomainID = @numDomainId AND
--			tintGroupType = 1


--		SET @I = @I  + 1
--	END
	
--	DROP TABLE #temp
--COMMIT
--END TRY
--BEGIN CATCH
--	IF @@TRANCOUNT > 0
--		ROLLBACK TRANSACTION;

--	SELECT 
--		ERROR_MESSAGE(),
--		ERROR_NUMBER(),
--		ERROR_SEVERITY(),
--		ERROR_STATE(),
--		ERROR_LINE(),
--		ISNULL(ERROR_PROCEDURE(), '-');
--END CATCH


----------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------

--INSERT INTO DynamicFormMaster
--(
--	numFormId,vcFormName,cCustomFieldsAssociated,cAOIAssociated,bitDeleted,tintFlag,vcLocationID,numBizFormModuleID
--)
--VALUES
--(
--	130,'Item Grid Columns','Y','N',0,0,5,5
--)



-----------------------------------------

--BEGIN TRY
--BEGIN TRANSACTION

--	DECLARE @numFieldID INT

--	INSERT INTO DycFieldMaster
--	(
--		numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcListItemType,numListID,[order],bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitDetailField,bitAllowSorting,bitAllowFiltering
--	)
--	VALUES
--	(
--		8,'Record Owner','numRecOwner','numRecOwner','UserCntID','Communication','N','R','SelectBox','U',0,0,1,0,0,0,1,0,1,1
--	) 


--	SELECT @numFieldID = SCOPE_IDENTITY()


--	INSERT INTO DycFormField_Mapping
--	(
--		numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,bitAllowGridColor
--	)
--	VALUES
--	(
--		8,@numFieldID,43,0,0,'Record Owner','SelectBox',16,1,0,1,1,0,0,1,1,1
--	)

--COMMIT
--END TRY
--BEGIN CATCH
--	IF @@TRANCOUNT > 0
--		ROLLBACK TRANSACTION;

--	SELECT 
--		ERROR_MESSAGE(),
--		ERROR_NUMBER(),
--		ERROR_SEVERITY(),
--		ERROR_STATE(),
--		ERROR_LINE(),
--		ISNULL(ERROR_PROCEDURE(), '-');
--END CATCH

-------------------------------------------------------------------------
-------------------------------------------------------------------------
-------------------------------------------------------------------------

--ALTER TABLE Domain DROP COLUMN numAbovePriceField
--ALTER TABLE Domain DROP COLUMN numOrderStatusBeforeApproval
--ALTER TABLE Domain DROP COLUMN numOrderStatusAfterApproval
--ALTER TABLE Domain ADD bitCostApproval BIT
--ALTER TABLE Domain ADD bitListPriceApproval BIT

--UPDATE Domain SET bitCostApproval=1,bitListPriceApproval=1,bitMarginPriceViolated=1




--BEGIN TRY
--BEGIN TRANSACTION

--	DECLARE @LocID NUMERIC(18,0) 

--	INSERT INTO CFW_Loc_Master
--	(
--		Loc_name
--		,vcFieldType
--		,vcCustomLookBackTableName
--	)
--	VALUES
--	(
--		'Activities'
--		,'A'
--		,''
--	)

--	SELECT @LocID = SCOPE_IDENTITY()


--	SELECT 
--		* 
--	INTO 
--		#temp 
--	FROM
--	(
--		SELECT 
--			ROW_NUMBER() OVER(ORDER BY numDomainId DESC) AS Row, 
--			numDomainId 
--		FROM 
--			Domain 
--		WHERE 
--			numDomainId <> -255
--	) TABLE2
	
--	DECLARE @RowCount INT
--	SET @RowCount = (SELECT COUNT(Row) FROM #temp) 
	
--	DECLARE @I INT
--	DECLARE @numDomainId NUMERIC(18,0)
	
--	SET @I = 1

--	WHILE (@I <= @RowCount)
--	BEGIN
			
--		SELECT @numDomainId = numDomainID FROM #temp WHERE Row = @I

--		INSERT INTO CFw_Grp_Master
--		(
--			Grp_Name
--			,Loc_Id
--			,numDomainID
--			,tintType
--			,vcURLF
--		)
--		VALUES
--		('Action Items & Meetings',@LocID,@numDomainId,2,'')


--		INSERT INTO GroupTabDetails
--		(
--			numGroupId,numTabId,numRelationShip,bitallowed,numOrder,tintType
--		)
--		SELECT
--			numGroupID,SCOPE_IDENTITY(),0,1,1,1
--		FROM
--			AuthenticationGroupMaster
--		WHERE
--			numDomainID = @numDomainId AND
--			tintGroupType = 1

--		INSERT INTO CFw_Grp_Master
--		(
--			Grp_Name
--			,Loc_Id
--			,numDomainID
--			,tintType
--			,vcURLF
--		)
--		VALUES
--		('Opportunities & Projects',@LocID,@numDomainId,2,'')


--		INSERT INTO GroupTabDetails
--		(
--			numGroupId,numTabId,numRelationShip,bitallowed,numOrder,tintType
--		)
--		SELECT
--			numGroupID,SCOPE_IDENTITY(),0,1,2,1
--		FROM
--			AuthenticationGroupMaster
--		WHERE
--			numDomainID = @numDomainId AND
--			tintGroupType = 1

--		INSERT INTO CFw_Grp_Master
--		(
--			Grp_Name
--			,Loc_Id
--			,numDomainID
--			,tintType
--			,vcURLF
--		)
--		VALUES
--		('Cases',@LocID,@numDomainId,2,'')


--		INSERT INTO GroupTabDetails
--		(
--			numGroupId,numTabId,numRelationShip,bitallowed,numOrder,tintType
--		)
--		SELECT
--			numGroupID,SCOPE_IDENTITY(),0,1,3,1
--		FROM
--			AuthenticationGroupMaster
--		WHERE
--			numDomainID = @numDomainId AND
--			tintGroupType = 1

--		INSERT INTO CFw_Grp_Master
--		(
--			Grp_Name
--			,Loc_Id
--			,numDomainID
--			,tintType
--			,vcURLF
--		)
--		VALUES
--		('Approval Requests',@LocID,@numDomainId,2,'')


--		INSERT INTO GroupTabDetails
--		(
--			numGroupId,numTabId,numRelationShip,bitallowed,numOrder,tintType
--		)
--		SELECT
--			numGroupID,SCOPE_IDENTITY(),0,1,4,1
--		FROM
--			AuthenticationGroupMaster
--		WHERE
--			numDomainID = @numDomainId AND
--			tintGroupType = 1

--		SET @I = @I  + 1
--	END
	
--	DROP TABLE #temp
--COMMIT
--END TRY
--BEGIN CATCH
--	IF @@TRANCOUNT > 0
--		ROLLBACK TRANSACTION;

--	SELECT 
--		ERROR_MESSAGE(),
--		ERROR_NUMBER(),
--		ERROR_SEVERITY(),
--		ERROR_STATE(),
--		ERROR_LINE(),
--		ISNULL(ERROR_PROCEDURE(), '-');
--END CATCH



--------------------------------------------
--------------------------------------------
--------------------------------------------

--BEGIN TRY
--BEGIN TRANSACTION

	
-- -- 1. Leads - ModileID = 2

--	INSERT INTO PageMaster 
--		(numPageID, numModuleID, vcFileName, vcPageDesc, bitIsViewApplicable, bitIsAddApplicable, bitIsUpdateApplicable, bitIsDeleteApplicable, bitIsExportApplicable)
--	VALUES
--		(129,37,'frmKitDetails.aspx','Item Details',1,0,1,1,0)


--	SELECT 
--		* 
--	INTO 
--		#temp 
--	FROM
--	(
--		SELECT 
--			ROW_NUMBER() OVER(ORDER BY numDomainId DESC) AS Row, 
--			numDomainId 
--		FROM 
--			Domain 
--		WHERE 
--			numDomainId <> -255
--	) TABLE2
	
--	DECLARE @RowCount INT
--	SET @RowCount = (SELECT COUNT(Row) FROM #temp) 
	
--	DECLARE @I INT
--	DECLARE @numDomainId NUMERIC(18,0)
	
--	SET @I = 1

--	WHILE (@I <= @RowCount)
--	BEGIN
			
--		SELECT @numDomainId = numDomainID FROM #temp WHERE Row = @I
	
--		INSERT INTO dbo.GroupAuthorization
--			(numDomainID,numModuleID,numPageID,numGroupID,intExportAllowed,intPrintAllowed,intViewAllowed,intAddAllowed,intUpdateAllowed,intDeleteAllowed)
--		SELECT
--			@numDomainId,37,129,numGroupID,0,0,3,0,3,3
--		FROM
--			AuthenticationGroupMaster
--		WHERE
--			numDomainID = @numDomainId AND
--			tintGroupType = 1


--		SET @I = @I  + 1
--	END
	
--	DROP TABLE #temp
--COMMIT
--END TRY
--BEGIN CATCH
--	IF @@TRANCOUNT > 0
--		ROLLBACK TRANSACTION;

--	SELECT 
--		ERROR_MESSAGE(),
--		ERROR_NUMBER(),
--		ERROR_SEVERITY(),
--		ERROR_STATE(),
--		ERROR_LINE(),
--		ISNULL(ERROR_PROCEDURE(), '-');
--END CATCH
------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------

--CHECK Field ID ON SERVER

--UPDATE DycFormField_Mapping SET bitAllowFiltering = 0 WHERE numFieldID=351 AND numFormID=21 --vcPathForImage
--UPDATE DycFormField_Mapping SET bitAllowFiltering = 0 WHERE numFieldID=195 AND numFormID=21 --OnHand
--UPDATE DycFormField_Mapping SET bitAllowFiltering = 0 WHERE numFieldID=196 AND numFormID=21 --OnOrder
--UPDATE DycFormField_Mapping SET bitAllowFiltering = 0 WHERE numFieldID=197 AND numFormID=21 --On Allocation
--UPDATE DycFormField_Mapping SET bitAllowFiltering = 0 WHERE numFieldID=198 AND numFormID=21 --On Backorder
--UPDATE DycFormField_Mapping SET bitAllowFiltering = 0 WHERE numFieldID=199 AND numFormID=21 --Location
--UPDATE DycFormField_Mapping SET bitAllowFiltering = 0 WHERE numFieldID=200 AND numFormID=21 --Reorder
--UPDATE DycFormField_Mapping SET vcAssociatedControlType='CheckBox' WHERE numFieldID=295 AND numFormID=21 --In Work Order
--UPDATE DycFormField_Mapping SET bitAllowFiltering = 0 WHERE numFieldID=469 AND numFormID=21 -- Price Level
--UPDATE DycFormField_Mapping SET bitAllowFiltering = 0 WHERE numFieldID=210 AND numFormID=21 -- Stock Value
--UPDATE DycFormField_Mapping SET bitAllowFiltering = 0 WHERE numFieldID=192 AND numFormID=21 -- Item Type
--UPDATE DycFormField_Mapping SET bitAllowFiltering=1 WHERE numFieldID=40730 --Release Date

-------------------------------

--BEGIN TRY
--BEGIN TRANSACTION
--begin
-- --add new values for slide 1
-- DECLARE @numFieldID AS NUMERIC(18,0) = 349
-- DECLARE @numFormFieldID AS NUMERIC(18,0)

-- SELECT TOP 1 @numFieldID=numFieldID  FROM DycFieldMaster WHERE vcFieldName = 'Vendor' AND vcDbColumnName = 'numVendorID'

-- INSERT INTO DynamicFormFieldMaster
-- (numFormID,vcFormFieldName,vcFieldType,vcAssociatedControlType,vcDbColumnName,numListID,bitDeleted,vcFieldDataType,vcOrigDbColumnName,bitAllowEdit,vcLookBackTableName,
--  bitDefault,[order],bitInResults,vcPropertyName,tintRow,tintColumn,bitSettingField,bitAddField,bitDetailField,bitAllowSorting)
-- VALUES
-- (26,'Vendor','R','SelectBox','numVendorID',0,0,'V','numVendorID',1,'Vendor',0,47,0,'',47,1,0,0,0,0)

-- SELECT @numFormFieldID = SCOPE_IDENTITY()

-- INSERT INTO DycFormField_Mapping
-- (numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,bitInResults,bitDeleted,
-- bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numFormFieldID)
-- VALUES
-- (4,@numFieldID,26,1,1,'Vendor','SelectBox','',47,47,1,1,0,0,1,0,0,0,0,@numFormFieldID)

--end
--COMMIT
--END TRY
--BEGIN CATCH
-- IF @@TRANCOUNT > 0
--  ROLLBACK TRANSACTION;

-- SELECT 
--  ERROR_MESSAGE(),
--  ERROR_NUMBER(),
--  ERROR_SEVERITY(),
--  ERROR_STATE(),
--  ERROR_LINE(),
--  ISNULL(ERROR_PROCEDURE(), '-');
--END CATCH

----------------------------------------------------------------------


--BEGIN TRY
--BEGIN TRANSACTION
--begin
-- --add new values for slide 1
-- DECLARE @numFieldID AS NUMERIC(18,0)
-- DECLARE @numFormFieldID AS NUMERIC(18,0)

-- INSERT INTO DycFieldMaster
-- (numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,[order],tintRow,tintColumn,
-- bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numListID)
-- VALUES
-- (4,'Margin','vcMargin','vcMargin','','OpportunityItems','V','R','Label',48,1,1,1,0,0,0,1,0,0,1,1,0)

-- SELECT @numFieldID = SCOPE_IDENTITY()

-- INSERT INTO DynamicFormFieldMaster
-- (numFormID,vcFormFieldName,vcFieldType,vcAssociatedControlType,vcDbColumnName,numListID,bitDeleted,vcFieldDataType,vcOrigDbColumnName,bitAllowEdit,vcLookBackTableName,
--  bitDefault,[order],bitInResults,vcPropertyName,tintRow,tintColumn,bitSettingField,bitAddField,bitDetailField,bitAllowSorting)
-- VALUES
-- (26,'Margin','R','Label','vcMargin',0,0,'V','vcMargin',1,'OpportunityItems',0,47,1,'',47,1,1,0,0,1)

-- SELECT @numFormFieldID = SCOPE_IDENTITY()

-- INSERT INTO DycFormField_Mapping
-- (numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,bitInResults,bitDeleted,
-- bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numFormFieldID)
-- VALUES
-- (4,@numFieldID,26,0,0,'Margin','Label','',47,47,1,1,0,0,1,0,0,0,0,@numFormFieldID)

--end
--COMMIT
--END TRY
--BEGIN CATCH
-- IF @@TRANCOUNT > 0
--  ROLLBACK TRANSACTION;

-- SELECT 
--  ERROR_MESSAGE(),
--  ERROR_NUMBER(),
--  ERROR_SEVERITY(),
--  ERROR_STATE(),
--  ERROR_LINE(),
--  ISNULL(ERROR_PROCEDURE(), '-');
--END CATCH


---------------------------------------------------------------------

--ALTER TABLE OpportunityItems ADD numDeletedReceievedQty FLOAT 
--ALTER TABLE OpportunityItemsReceievedLocation ADD numDeletedReceievedQty FLOAT

------------------------------------------------------------------

--UPDATE DycFieldMaster SET vcDbColumnName='vcLastSalesOrderDate',vcLookBackTableName='OpportunityMaster',vcOrigDbColumnName='vcLastSalesOrderDate' WHERE vcFieldName = 'Last Sales Order Date'

------------------------------------------------------------------

--BEGIN TRY
--BEGIN TRANSACTION
--begin
-- --add new values for slide 1
-- DECLARE @numFieldID AS NUMERIC(18,0)
-- DECLARE @numFormFieldID AS NUMERIC(18,0)

-- SELECT @numFieldID=numFieldID  FROM DycFieldMaster WHERE vcFieldName = 'Last Sales Order Date'

-- INSERT INTO DynamicFormFieldMaster
-- (numFormID,vcFormFieldName,vcFieldType,vcAssociatedControlType,vcDbColumnName,numListID,bitDeleted,vcFieldDataType,vcOrigDbColumnName,bitAllowEdit,vcLookBackTableName,
--  bitDefault,[order],bitInResults,vcPropertyName,tintRow,tintColumn,bitSettingField,bitAddField,bitDetailField,bitAllowSorting)
-- VALUES
-- (36,'Last Sales Order Date','R','Label','',0,0,'V','',0,'',0,47,1,'',47,1,1,0,0,0)

-- SELECT @numFormFieldID = SCOPE_IDENTITY()

-- INSERT INTO DycFormField_Mapping
-- (numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,bitInResults,bitDeleted,
-- bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numFormFieldID)
-- VALUES
-- (1,@numFieldID,36,0,0,'Last Sales Order Date','Label','',47,47,1,1,0,0,1,0,0,1,0,@numFormFieldID)

--end
--COMMIT
--END TRY
--BEGIN CATCH
-- IF @@TRANCOUNT > 0
--  ROLLBACK TRANSACTION;

-- SELECT 
--  ERROR_MESSAGE(),
--  ERROR_NUMBER(),
--  ERROR_SEVERITY(),
--  ERROR_STATE(),
--  ERROR_LINE(),
--  ISNULL(ERROR_PROCEDURE(), '-');
--END CATCH

------------------------------------------------------------------

--BEGIN TRY
--BEGIN TRANSACTION
--begin
-- --add new values for slide 1
-- DECLARE @numFieldID AS NUMERIC(18,0)
-- DECLARE @numFormFieldID AS NUMERIC(18,0)

--SELECT @numFieldID=numFieldID  FROM DycFieldMaster WHERE vcFieldName = 'Last Sales Order Date'

-- INSERT INTO DynamicFormFieldMaster
-- (numFormID,vcFormFieldName,vcFieldType,vcAssociatedControlType,vcDbColumnName,numListID,bitDeleted,vcFieldDataType,vcOrigDbColumnName,bitAllowEdit,vcLookBackTableName,
--  bitDefault,[order],bitInResults,vcPropertyName,tintRow,tintColumn,bitSettingField,bitAddField,bitDetailField,bitAllowSorting)
-- VALUES
-- (43,'Last Sales Order Date','R','Label','',0,0,'V','',0,'',0,47,1,'',47,1,1,0,0,0)

-- SELECT @numFormFieldID = SCOPE_IDENTITY()

-- INSERT INTO DycFormField_Mapping
-- (numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,bitInResults,bitDeleted,
-- bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numFormFieldID)
-- VALUES
-- (1,@numFieldID,43,0,0,'Last Sales Order Date','Label','',47,47,1,1,0,0,1,0,0,1,0,@numFormFieldID)

--end
--COMMIT
--END TRY
--BEGIN CATCH
-- IF @@TRANCOUNT > 0
--  ROLLBACK TRANSACTION;

-- SELECT 
--  ERROR_MESSAGE(),
--  ERROR_NUMBER(),
--  ERROR_SEVERITY(),
--  ERROR_STATE(),
--  ERROR_LINE(),
--  ISNULL(ERROR_PROCEDURE(), '-');
--END CATCH


------------------------------------------------------------------


--BEGIN TRY
--BEGIN TRANSACTION
--begin
-- --add new values for slide 1
-- DECLARE @numFieldID AS NUMERIC(18,0)
-- DECLARE @numFormFieldID AS NUMERIC(18,0)

-- INSERT INTO DycFieldMaster
-- (numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,[order],tintRow,tintColumn,
-- bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numListID)
-- VALUES
-- (1,'Performance','vcPerformance','vcPerformance','','CompanyInfo','V','R','Label',48,1,1,1,0,0,0,1,0,0,1,1,0)

-- SELECT @numFieldID = SCOPE_IDENTITY()

-- INSERT INTO DynamicFormFieldMaster
-- (numFormID,vcFormFieldName,vcFieldType,vcAssociatedControlType,vcDbColumnName,numListID,bitDeleted,vcFieldDataType,vcOrigDbColumnName,bitAllowEdit,vcLookBackTableName,
--  bitDefault,[order],bitInResults,vcPropertyName,tintRow,tintColumn,bitSettingField,bitAddField,bitDetailField,bitAllowSorting)
-- VALUES
-- (36,'Performance','R','Label','',0,0,'V','',0,'',0,48,1,'',47,1,1,0,0,1)

-- SELECT @numFormFieldID = SCOPE_IDENTITY()

-- INSERT INTO DycFormField_Mapping
-- (numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,bitInResults,bitDeleted,
-- bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numFormFieldID)
-- VALUES
-- (1,@numFieldID,36,0,0,'Performance','Label','',48,1,1,1,0,0,1,0,0,1,1,@numFormFieldID)

--  INSERT INTO DynamicFormFieldMaster
-- (numFormID,vcFormFieldName,vcFieldType,vcAssociatedControlType,vcDbColumnName,numListID,bitDeleted,vcFieldDataType,vcOrigDbColumnName,bitAllowEdit,vcLookBackTableName,
--  bitDefault,[order],bitInResults,vcPropertyName,tintRow,tintColumn,bitSettingField,bitAddField,bitDetailField,bitAllowSorting)
-- VALUES
-- (43,'Performance','R','Label','',0,0,'V','',0,'',0,48,1,'',47,1,1,0,0,1)

-- SELECT @numFormFieldID = SCOPE_IDENTITY()

-- INSERT INTO DycFormField_Mapping
-- (numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,bitInResults,bitDeleted,
-- bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numFormFieldID)
-- VALUES
-- (1,@numFieldID,43,0,0,'Performance','Label','',48,1,1,1,0,0,1,0,0,1,1,@numFormFieldID)

--end
--COMMIT
--END TRY
--BEGIN CATCH
-- IF @@TRANCOUNT > 0
--  ROLLBACK TRANSACTION;

-- SELECT 
--  ERROR_MESSAGE(),
--  ERROR_NUMBER(),
--  ERROR_SEVERITY(),
--  ERROR_STATE(),
--  ERROR_LINE(),
--  ISNULL(ERROR_PROCEDURE(), '-');
--END CATCH


------------------------------------------------------------------

--BEGIN TRY
--BEGIN TRANSACTION

--	INSERT INTO PageNavigationDTL 
--	(numPageNavID,numModuleID,numParentID,vcPageNavName,vcNavURL,bitVisible,numTabID)
--	VALUES
--	(258,35,83,'Matching Rules','../Accounting/frmBankReconcileMatchRulesList.aspx',1,45)

--	INSERT INTO dbo.TreeNavigationAuthorization
--	(
--		numGroupID,
--		numTabID,
--		numPageNavID,
--		bitVisible,
--		numDomainID,
--		tintType
--	)
--	SELECT  
--		numGroupID,
--		45,
--		258,
--		1,
--		numDomainID,
--		1
--	FROM    
--		AuthenticationGroupMaster
--	WHERE
--		tintGroupType=1
--COMMIT
--END TRY
--BEGIN CATCH
--	IF @@TRANCOUNT > 0
--		ROLLBACK TRANSACTION;

--	SELECT 
--		ERROR_MESSAGE(),
--		ERROR_NUMBER(),
--		ERROR_SEVERITY(),
--		ERROR_STATE(),
--		ERROR_LINE(),
--		ISNULL(ERROR_PROCEDURE(), '-');
--END CATCH

-----------------------------------------

--USE [Production.2014]
--GO

--/****** Object:  Table [dbo].[BankReconcileMatchRule]    Script Date: 01-Dec-16 5:43:37 PM ******/
--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

--SET ANSI_PADDING ON
--GO

--CREATE TABLE [dbo].[BankReconcileMatchRule](
--	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
--	[numDomainID] [numeric](18, 0) NOT NULL,
--	[vcName] [varchar](200) NOT NULL,
--	[vcBankAccounts] [varchar](max) NOT NULL,
--	[bitMatchAllConditions] [bit] NOT NULL,
--	[numCreatedBy] [numeric](18, 0) NOT NULL,
--	[dtCreatedDate] [datetime] NOT NULL,
--	[tintOrder] [int] NOT NULL,
-- CONSTRAINT [PK_BankReconcileMathRule] PRIMARY KEY CLUSTERED 
--(
--	[ID] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
--) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

--GO

--SET ANSI_PADDING ON
--GO


-----------------------------------------------

--USE [Production.2014]
--GO

--/****** Object:  Table [dbo].[BankReconcileMatchRuleCondition]    Script Date: 01-Dec-16 5:43:43 PM ******/
--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

--SET ANSI_PADDING ON
--GO

--CREATE TABLE [dbo].[BankReconcileMatchRuleCondition](
--	[numConditionID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
--	[numRuleID] [numeric](18, 0) NOT NULL,
--	[tintColumn] [tinyint] NOT NULL,
--	[tintConditionOperator] [tinyint] NOT NULL,
--	[vcTextToMatch] [varchar](max) NOT NULL,
--	[numDivisionID] [numeric](18, 0) NOT NULL,
-- CONSTRAINT [PK_BankReconcileMathRuleCondition] PRIMARY KEY CLUSTERED 
--(
--	[numConditionID] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
--) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

--GO

--SET ANSI_PADDING ON
--GO

--ALTER TABLE [dbo].[BankReconcileMatchRuleCondition]  WITH CHECK ADD  CONSTRAINT [FK_BankReconcileMathRuleCondition_BankReconcileMathRule] FOREIGN KEY([numRuleID])
--REFERENCES [dbo].[BankReconcileMatchRule] ([ID])
--GO

--ALTER TABLE [dbo].[BankReconcileMatchRuleCondition] CHECK CONSTRAINT [FK_BankReconcileMathRuleCondition_BankReconcileMathRule]
--GO



------------------------------------------
------------------------------------------
------------------------------------------

--USE [Production.2014]
--GO

--/****** Object:  Table [dbo].[ListDetailsName]    Script Date: 18-Nov-16 5:18:36 PM ******/
--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

--SET ANSI_PADDING ON
--GO

--CREATE TABLE [dbo].[ListDetailsName](
--	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
--	[numDomainID] [numeric](18, 0) NOT NULL,
--	[numListID] [numeric](18, 0) NOT NULL,
--	[numListItemID] [numeric](18, 0) NOT NULL,
--	[vcName] [varchar](300) NOT NULL,
-- CONSTRAINT [PK_ListDetailsName] PRIMARY KEY CLUSTERED 
--(
--	[ID] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
--) ON [PRIMARY]

--GO

--SET ANSI_PADDING ON
--GO

--ALTER TABLE [dbo].[ListDetailsName]  WITH CHECK ADD  CONSTRAINT [FK_ListDetailsName_ListDetails] FOREIGN KEY([numListItemID])
--REFERENCES [dbo].[ListDetails] ([numListItemID])
--ON DELETE CASCADE
--GO

--ALTER TABLE [dbo].[ListDetailsName] CHECK CONSTRAINT [FK_ListDetailsName_ListDetails]
--GO

--ALTER TABLE [dbo].[ListDetailsName]  WITH CHECK ADD  CONSTRAINT [FK_ListDetailsName_ListMaster] FOREIGN KEY([numListID])
--REFERENCES [dbo].[ListMaster] ([numListID])
--ON DELETE CASCADE
--GO

--ALTER TABLE [dbo].[ListDetailsName] CHECK CONSTRAINT [FK_ListDetailsName_ListMaster]
--GO


-------------------------------------------------

--USE [Production.2014]
--GO

--/****** Object:  Index [NCIX_ListDetailsName_20161118_171738]    Script Date: 18-Nov-16 5:19:31 PM ******/
--CREATE NONCLUSTERED INDEX [NCIX_ListDetailsName_20161118_171738] ON [dbo].[ListDetailsName]
--(
--	[numDomainID] ASC
--)
--INCLUDE ( 	[numListID],
--	[numListItemID]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
--GO

---------------------------------------

--BEGIN TRY
--BEGIN TRANSACTION

	
-- -- 1. Leads - ModileID = 2

--	INSERT INTO PageMaster 
--		(numPageID, numModuleID, vcFileName, vcPageDesc, bitIsViewApplicable, bitIsAddApplicable, bitIsUpdateApplicable, bitIsDeleteApplicable, bitIsExportApplicable)
--	VALUES
--		(128,2,'','Assign To',0,0,1,0,0)


---- 2. Prospects - ModileID = 3

--	INSERT INTO PageMaster 
--		(numPageID, numModuleID, vcFileName, vcPageDesc, bitIsViewApplicable, bitIsAddApplicable, bitIsUpdateApplicable, bitIsDeleteApplicable, bitIsExportApplicable)
--	VALUES
--		(128,3,'','Assign To',0,0,1,0,0)
	
---- 3. Accounts - ModileID = 4

--	INSERT INTO PageMaster 
--		(numPageID, numModuleID, vcFileName, vcPageDesc, bitIsViewApplicable, bitIsAddApplicable, bitIsUpdateApplicable, bitIsDeleteApplicable, bitIsExportApplicable)
--	VALUES
--		(128,4,'','Assign To',0,0,1,0,0)

---- 6. Opportunities/Orders - ModileID = 10

--	INSERT INTO PageMaster 
--		(numPageID, numModuleID, vcFileName, vcPageDesc, bitIsViewApplicable, bitIsAddApplicable, bitIsUpdateApplicable, bitIsDeleteApplicable, bitIsExportApplicable)
--	VALUES
--		(128,10,'','Assign To',0,0,1,0,0)


--	SELECT 
--		* 
--	INTO 
--		#temp 
--	FROM
--	(
--		SELECT 
--			ROW_NUMBER() OVER(ORDER BY numDomainId DESC) AS Row, 
--			numDomainId 
--		FROM 
--			Domain 
--		WHERE 
--			numDomainId <> -255
--	) TABLE2
	
--	DECLARE @RowCount INT
--	SET @RowCount = (SELECT COUNT(Row) FROM #temp) 
	
--	DECLARE @I INT
--	DECLARE @numDomainId NUMERIC(18,0)
	
--	SET @I = 1

--	WHILE (@I <= @RowCount)
--	BEGIN
			
--		SELECT @numDomainId = numDomainID FROM #temp WHERE Row = @I
	
--		-- 1. Leads - ModileID = 2
--		INSERT INTO dbo.GroupAuthorization
--			(numDomainID,numModuleID,numPageID,numGroupID,intExportAllowed,intPrintAllowed,intViewAllowed,intAddAllowed,intUpdateAllowed,intDeleteAllowed)
--		SELECT
--			@numDomainId,2,128,numGroupID,0,0,0,0,3,0
--		FROM
--			AuthenticationGroupMaster
--		WHERE
--			numDomainID = @numDomainId AND
--			tintGroupType = 1

--		-- 2. Prospects - ModileID = 3
--		INSERT INTO dbo.GroupAuthorization
--			(numDomainID,numModuleID,numPageID,numGroupID,intExportAllowed,intPrintAllowed,intViewAllowed,intAddAllowed,intUpdateAllowed,intDeleteAllowed)
--		SELECT
--			@numDomainId,3,128,numGroupID,0,0,0,0,3,0
--		FROM
--			AuthenticationGroupMaster
--		WHERE
--			numDomainID = @numDomainId AND
--			tintGroupType = 1

--		-- 3. Accounts - ModileID = 4
--		INSERT INTO dbo.GroupAuthorization
--			(numDomainID,numModuleID,numPageID,numGroupID,intExportAllowed,intPrintAllowed,intViewAllowed,intAddAllowed,intUpdateAllowed,intDeleteAllowed)
--		SELECT
--			@numDomainId,4,128,numGroupID,0,0,0,0,3,0
--		FROM
--			AuthenticationGroupMaster
--		WHERE
--			numDomainID = @numDomainId AND
--			tintGroupType = 1

--		-- 6. Opportunities/Orders - ModileID = 10
--		INSERT INTO dbo.GroupAuthorization
--			(numDomainID,numModuleID,numPageID,numGroupID,intExportAllowed,intPrintAllowed,intViewAllowed,intAddAllowed,intUpdateAllowed,intDeleteAllowed)
--		SELECT
--			@numDomainId,10,128,numGroupID,0,0,0,0,3,0
--		FROM
--			AuthenticationGroupMaster
--		WHERE
--			numDomainID = @numDomainId AND
--			tintGroupType = 1

--		SET @I = @I  + 1
--	END
	
--	DROP TABLE #temp
--COMMIT
--END TRY
--BEGIN CATCH
--	IF @@TRANCOUNT > 0
--		ROLLBACK TRANSACTION;

--	SELECT 
--		ERROR_MESSAGE(),
--		ERROR_NUMBER(),
--		ERROR_SEVERITY(),
--		ERROR_STATE(),
--		ERROR_LINE(),
--		ISNULL(ERROR_PROCEDURE(), '-');
--END CATCH


/***************************************************************************/
/*********************** LAST UPDATE ON 10 SEP 2016 ******************/
/***************************************************************************/

--UPDATE DycFormField_Mapping SET bitAllowFiltering=1 WHERE numFormID=39 AND vcPropertyName='InventoryStatus'
--ALTER TABLE OpportunityMaster ADD bitPostSellDiscountUsed BIT

---------------------------------------------------------------


--USE [Production.2014]
--GO

--/****** Object:  Table [dbo].[OpportunityItemsReceievedLocation]    Script Date: 10-Sep-16 10:24:51 AM ******/
--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

--CREATE TABLE [dbo].[OpportunityItemsReceievedLocation](
--	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
--	[numDomainID] [numeric](18, 0) NOT NULL,
--	[numOppID] [numeric](18, 0) NOT NULL,
--	[numOppItemID] [numeric](18, 0) NOT NULL,
--	[numWarehouseItemID] [numeric](18, 0) NOT NULL,
--	[numUnitReceieved] [float] NOT NULL,
-- CONSTRAINT [PK_OpportunityItemsReceievedLocation] PRIMARY KEY CLUSTERED 
--(
--	[ID] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
--) ON [PRIMARY]

--GO

--ALTER TABLE [dbo].[OpportunityItemsReceievedLocation]  WITH CHECK ADD  CONSTRAINT [FK_OpportunityItemsReceievedLocation_OpportunityItems] FOREIGN KEY([numOppItemID])
--REFERENCES [dbo].[OpportunityItems] ([numoppitemtCode])
--ON DELETE CASCADE
--GO

--ALTER TABLE [dbo].[OpportunityItemsReceievedLocation] CHECK CONSTRAINT [FK_OpportunityItemsReceievedLocation_OpportunityItems]
--GO

--------------------------------------------------------------------

--USE [Production.2014]
--GO

--/****** Object:  Table [dbo].[OpportunityItemsReceievedLocationReturn]    Script Date: 10-Sep-16 10:25:22 AM ******/
--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

--CREATE TABLE [dbo].[OpportunityItemsReceievedLocationReturn](
--	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
--	[numOIRLID] [numeric](18, 0) NOT NULL,
--	[numReturnID] [numeric](18, 0) NOT NULL,
--	[numReturnItemID] [numeric](18, 0) NOT NULL,
--	[numReturnedQty] [float] NOT NULL,
-- CONSTRAINT [PK_OpportunityItemsReceievedLocationReturn] PRIMARY KEY CLUSTERED 
--(
--	[ID] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
--) ON [PRIMARY]

--GO

--ALTER TABLE [dbo].[OpportunityItemsReceievedLocationReturn]  WITH CHECK ADD  CONSTRAINT [FK_OpportunityItemsReceievedLocationReturn_OpportunityItemsReceievedLocation] FOREIGN KEY([numOIRLID])
--REFERENCES [dbo].[OpportunityItemsReceievedLocation] ([ID])
--ON DELETE CASCADE
--GO

--ALTER TABLE [dbo].[OpportunityItemsReceievedLocationReturn] CHECK CONSTRAINT [FK_OpportunityItemsReceievedLocationReturn_OpportunityItemsReceievedLocation]
--GO


--ALTER TABLE WorkOrder ADD
--monAverageCost DECIMAL(19,4)

--=====================================================

--ALTER TABLE WorkOrderDetails ADD
--monAverageCost DECIMAL(19,4)

--=====================================================

--USE [Production.2014]
--GO

--/****** Object:  Table [dbo].[AssembledItem]    Script Date: 02-Jul-16 1:03:12 PM ******/
--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

--CREATE TABLE [dbo].[AssembledItem](
--	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
--	[numDomainID] [numeric](18, 0) NOT NULL,
--	[numItemCode] [numeric](18, 0) NOT NULL,
--	[numWarehouseItemID] [numeric](18, 0) NOT NULL,
--	[numAssembledQty] [numeric](18, 0) NOT NULL,
--	[monAverageCost] [decimal](19, 4) NULL,
--	[numCreatedBy] [numeric](18, 0) NOT NULL,
--	[dtCreatedDate] [datetime] NOT NULL,
--	[numDisassembledQty] [numeric](18, 0) NULL,
-- CONSTRAINT [PK_ItemAssembled] PRIMARY KEY CLUSTERED 
--(
--	[ID] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
--) ON [PRIMARY]

--GO

--ALTER TABLE [dbo].[AssembledItem]  WITH CHECK ADD  CONSTRAINT [FK_ItemAssembled_Item] FOREIGN KEY([numItemCode])
--REFERENCES [dbo].[Item] ([numItemCode])
--GO

--ALTER TABLE [dbo].[AssembledItem] CHECK CONSTRAINT [FK_ItemAssembled_Item]
--GO

--============================================================================


--USE [Production.2014]
--GO

--/****** Object:  Index [NCI_AssembledItem_DomainID]    Script Date: 24-Jun-16 1:48:56 PM ******/
--CREATE NONCLUSTERED INDEX [NCI_AssembledItem_DomainID] ON [dbo].[AssembledItem]
--(
--	[numDomainID] ASC,
--	[numItemCode] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
--GO

--========================================================================================

--USE [Production.2014]
--GO

--/****** Object:  Table [dbo].[AssembledItemChilds]    Script Date: 24-Jun-16 1:49:23 PM ******/
--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

--CREATE TABLE [dbo].[AssembledItemChilds](
--	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
--	[numAssembledItemID] [numeric](18, 0) NOT NULL,
--	[numItemCode] [numeric](18, 0) NOT NULL,
--	[numQuantity] [float] NOT NULL,
--	[numWarehouseItemID] [numeric](18, 0) NOT NULL,
--	[monAverageCost] [decimal](19, 4) NOT NULL,
--	[fltQtyRequiredForSingleBuild] [float] NOT NULL,
-- CONSTRAINT [PK_AssembledItemChilds] PRIMARY KEY CLUSTERED 
--(
--	[ID] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
--) ON [PRIMARY]

--GO

--ALTER TABLE [dbo].[AssembledItemChilds]  WITH CHECK ADD  CONSTRAINT [FK_AssembledItemChilds_AssembledItem] FOREIGN KEY([numAssembledItemID])
--REFERENCES [dbo].[AssembledItem] ([ID])
--ON DELETE CASCADE
--GO

--ALTER TABLE [dbo].[AssembledItemChilds] CHECK CONSTRAINT [FK_AssembledItemChilds_AssembledItem]
--GO

--ALTER TABLE [dbo].[AssembledItemChilds]  WITH CHECK ADD  CONSTRAINT [FK_AssembledItemChilds_Item] FOREIGN KEY([numItemCode])
--REFERENCES [dbo].[Item] ([numItemCode])
--GO

--ALTER TABLE [dbo].[AssembledItemChilds] CHECK CONSTRAINT [FK_AssembledItemChilds_Item]
--GO

--=================================================================================================================================

--USE [Production.2014]
--GO

--/****** Object:  Index [NIC_AssembledItemChilds_numAssembledItemID]    Script Date: 24-Jun-16 1:50:09 PM ******/
--CREATE NONCLUSTERED INDEX [NIC_AssembledItemChilds_numAssembledItemID] ON [dbo].[AssembledItemChilds]
--(
--	[numAssembledItemID] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
--GO

--=======================================================================================================================================

--INSERT INTO AccountTypeMaster(vcAccountCode,vcAccountType,numParentID) VALUES ('010105','Other Current Assets',2)

--========================================================================================================================================
---- TO CHECK ACCOUNT TYE CODE
----SELECT * FROM AccountTypeDetail WHERE vcAccountCode  LIKE '0101%' AND LEN(vcAccountCode) = 6 ORDER BY vcAccountCode DESC
--BEGIN TRY
--BEGIN TRANSACTION

--	DECLARE @TEMP TABLE
--	(
--		ID INT IDENTITY(1,1),
--		numDomainID NUMERIC(18,0)
--	)

--	INSERT INTO @TEMP (numDomainID) SELECT numDomainId FROM Domain WHERE numDomainId > 0

--	DECLARE @I AS INT = 1
--	DECLARE @Count AS INT
--	DECLARE @numDomainID AS INT

--	SELECT @Count=COUNT(*) FROM @TEMP


--	WHILE @i <= @Count
--	BEGIN
--		SELECT @numDomainID=numDomainID FROM @TEMP WHERE ID=@i

--		IF EXISTS(SELECT * FROM AccountTypeDetail WHERE numDomainID=@numDomainID AND vcAccountCode='010105')
--		BEGIN
--			UPDATE AccountTypeDetail SET bitSystem=1 WHERE numDomainID=@numDomainID AND vcAccountCode='010105'
--		END
--		ELSE
--		BEGIN
--			INSERT INTO AccountTypeDetail
--			(
--				vcAccountCode,
--				vcAccountType,
--				numParentID,
--				numDomainID,
--				dtCreateDate,
--				dtModifiedDate,
--				bitSystem
--			)
--			VALUES
--			(
--				'010105',
--				'Other Current Assets',
--				(SELECT numAccountTypeID FROM AccountTypeDetail WHERE numDomainID=@numDomainID AND vcAccountCode='0101'),
--				@numDomainID,
--				GETUTCDATE(),
--				GETUTCDATE(),
--				1
--			)
--		END


--		SET @i = @i + 1
--	END

--COMMIT
--END TRY
--BEGIN CATCH
--	IF @@TRANCOUNT > 0
--		ROLLBACK TRANSACTION;

--	SELECT 
--		ERROR_MESSAGE(),
--		ERROR_NUMBER(),
--		ERROR_SEVERITY(),
--		ERROR_STATE(),
--		ERROR_LINE(),
--		ISNULL(ERROR_PROCEDURE(), '-');
--END CATCH

--======================================================
--DECLARE @numFieldID AS NUMERIC(18,0)

--SELECT TOP 1 @numFieldID=numFieldId FROM DycFieldMaster WHERE vcDbColumnName LIKE '%vcPromotionDetail%'

--INSERT INTO DycFormField_Mapping(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,bitAddField) VALUES (4,@numFieldID,92,0,0,1)

--===================================================================================

--DECLARE @numFieldID AS NUMERIC(18,0)

--INSERT INTO DycFieldMaster(numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,bitInlineEdit,bitDeleted,bitAllowEdit,bitDefault,numListID,bitInResults)
--VALUES (4,'Sales Tax','vcTax','vcTax','Item','M','R','Label',1,0,0,0,0,1)

--SELECT @numFieldID = SCOPE_IDENTITY()
--INSERT INTO DycFormField_Mapping(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,bitAddField) VALUES (4,@numFieldID,92,0,0,1)



/***************************************************************************/
/*********************** LAST UPDATE ON 08 JUNE 2016 ******************/
/***************************************************************************/

--=========================================================================================

--ALTER TABLE PromotionOffer DROP COLUMN [vcTerms]
--ALTER TABLE PromotionOffer DROP COLUMN [numSiteId]
--ALTER TABLE PromotionOffer DROP COLUMN [bintStartDate]
--ALTER TABLE PromotionOffer DROP COLUMN [bintEndDate]
--ALTER TABLE PromotionOffer DROP COLUMN [tintAppliesTo]
--ALTER TABLE PromotionOffer DROP COLUMN [intLimitation]
--ALTER TABLE PromotionOffer DROP COLUMN [tintLimitationBasedOn]
--ALTER TABLE PromotionOffer DROP COLUMN [vcCouponCode]
--ALTER TABLE PromotionOffer DROP COLUMN [tintDiscountType]
--ALTER TABLE PromotionOffer DROP COLUMN [decDiscount]
--ALTER TABLE PromotionOffer DROP COLUMN [tintBasedOn]
--ALTER TABLE PromotionOffer DROP COLUMN [tintContactsType]
--ALTER TABLE PromotionOffer DROP COLUMN [decBasedOnValue]

--=========================================================================================

--ALTER TABLE PromotionOffer ADD
--dtValidFrom DATE,
--dtValidTo DATE,
--bitNeverExpires BIT,
--bitApplyToInternalOrders BIT,
--bitAppliesToSite BIT,
--bitRequireCouponCode BIT,
--txtCouponCode VARCHAR(100),
--tintUsageLimit INT,
--bitFreeShiping BIT,
--monFreeShippingOrderAmount MONEY,
--numFreeShippingCountry NUMERIC(18,0),
--bitFixShipping1 BIT,
--monFixShipping1OrderAmount MONEY,
--monFixShipping1Charge MONEY,
--bitFixShipping2 BIT,
--monFixShipping2OrderAmount MONEY,
--monFixShipping2Charge MONEY,
--bitDisplayPostUpSell BIT,
--intPostSellDiscount INT,
--tintOfferTriggerValueType TINYINT,
--fltOfferTriggerValue FLOAT,
--tintOfferBasedOn TINYINT,
--fltDiscountValue FLOAT,
--tintDiscountType TINYINT,
--tintDiscoutBaseOn TINYINT,
--numCreatedBy NUMERIC(18,0),
--dtCreated DATETIME,
--numModifiedBy NUMERIC(18,0),
--dtModified DATETIME,
--intCouponCodeUsed INT,
--bitEnabled BIT
--==========================================================================================

--USE [Production.2014]
--GO

--/****** Object:  Table [dbo].[PromotionOfferSites]    Script Date: 22-Apr-16 2:02:08 PM ******/
--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

--CREATE TABLE [dbo].[PromotionOfferSites](
--	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
--	[numPromotionID] [numeric](18, 0) NOT NULL,
--	[numSiteID] [numeric](18, 0) NOT NULL,
-- CONSTRAINT [PK_PromotionOfferSites] PRIMARY KEY CLUSTERED 
--(
--	[ID] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
--) ON [PRIMARY]

--GO

--ALTER TABLE [dbo].[PromotionOfferSites]  WITH CHECK ADD  CONSTRAINT [FK_PromotionOfferSites_PromotionOffer] FOREIGN KEY([numPromotionID])
--REFERENCES [dbo].[PromotionOffer] ([numProId])
--ON DELETE CASCADE
--GO

--ALTER TABLE [dbo].[PromotionOfferSites] CHECK CONSTRAINT [FK_PromotionOfferSites_PromotionOffer]
--GO

--ALTER TABLE [dbo].[PromotionOfferSites]  WITH CHECK ADD  CONSTRAINT [FK_PromotionOfferSites_Sites] FOREIGN KEY([numSiteID])
--REFERENCES [dbo].[Sites] ([numSiteID])
--ON DELETE CASCADE
--GO

--ALTER TABLE [dbo].[PromotionOfferSites] CHECK CONSTRAINT [FK_PromotionOfferSites_Sites]
--GO

--====================================================================================================
--ALTER TABLE OpportunityItems ADD
--numPromotionID NUMERIC(18,0),
--bitPromotionTriggered BIT,
--vcPromotionDetail VARCHAR(2000)


--ALTER TABLE OpportunityMaster ADD
--dtReleaseDate DATE
--====================================================================================================

--BEGIN TRY
--BEGIN TRANSACTION

--	DECLARE @numFieldID AS NUMERIC(18,0)
--	DECLARE @numFormFieldID AS NUMERIC(18,0)

--	INSERT INTO DycFieldMaster
--	(numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,[order],tintRow,tintColumn,
--	bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numListID)
--	VALUES
--	(3,'Promotion Detail','vcPromotionDetail','vcPromotionDetail','vcPromotionDetail','OpportunityItems','V','R','Label',47,1,1,1,0,0,,1,0,0,0,0,0)

--	SELECT @numFieldID = SCOPE_IDENTITY()

--	INSERT INTO DynamicFormFieldMaster
--	(numFormID,vcFormFieldName,vcFieldType,vcAssociatedControlType,vcDbColumnName,numListID,bitDeleted,vcFieldDataType,vcOrigDbColumnName,bitAllowEdit,vcLookBackTableName,
--	 bitDefault,[order],bitInResults,vcPropertyName,tintRow,tintColumn,bitSettingField,bitAddField,bitDetailField,bitAllowSorting)
--	VALUES
--	(26,'Promotion Detail','R','Label','vcPromotionDetail',0,0,'V','vcPromotionDetail',0,'OpportunityItems',0,47,1,'vcPromotionDetail',47,1,1,0,0,0)

--	SELECT @numFormFieldID = SCOPE_IDENTITY()

--	INSERT INTO DycFormField_Mapping
--	(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,bitInResults,bitDeleted,
--	bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numFormFieldID)
--	VALUES
--	(3,@numFieldID,26,0,0,'Promotion Detail','Label','vcPromotionDetail',47,47,1,1,0,0,1,0,0,0,0,@numFormFieldID)

--	INSERT INTO DynamicFormFieldMaster
--	(numFormID,vcFormFieldName,vcFieldType,vcAssociatedControlType,vcDbColumnName,numListID,bitDeleted,vcFieldDataType,vcOrigDbColumnName,bitAllowEdit,vcLookBackTableName,
--	 bitDefault,[order],bitInResults,vcPropertyName,tintRow,tintColumn,bitSettingField,bitAddField,bitDetailField,bitAllowSorting)
--	VALUES
--	(7,'Promotion Detail','R','Label','vcPromotionDetail',0,0,'V','vcPromotionDetail',0,'OpportunityItems',0,47,1,'vcPromotionDetail',47,1,1,0,0,0)

--	SELECT @numFormFieldID = SCOPE_IDENTITY()

--	INSERT INTO DycFormField_Mapping
--	(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,bitInResults,bitDeleted,
--	bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numFormFieldID)
--	VALUES
--	(3,@numFieldID,7,0,0,'Promotion Detail','Label','vcPromotionDetail',47,47,1,1,0,0,1,0,0,0,0,@numFormFieldID)

--COMMIT
--END TRY
--BEGIN CATCH
--	IF @@TRANCOUNT > 0
--		ROLLBACK TRANSACTION;

--	SELECT 
--		ERROR_MESSAGE(),
--		ERROR_NUMBER(),
--		ERROR_SEVERITY(),
--		ERROR_STATE(),
--		ERROR_LINE(),
--		ISNULL(ERROR_PROCEDURE(), '-');
--END CATCH

--=================================================================================================

--ALTER TABLE PromotionOfferItems 
--ALTER COLUMN numValue NUMERIC(18,0)

--====================================================
--BEGIN TRY
--BEGIN TRANSACTION

--	DECLARE @numMAXPageNavID INT
--	SELECT @numMAXPageNavID = MAX(numPageNavID) FROM PageNavigationDTL


--	INSERT INTO PageNavigationDTL
--	(
--		numPageNavID,numModuleID,numParentID,vcPageNavName,vcNavURL,bitVisible,numTabID
--	)
--	VALUES
--	(
--		(@numMAXPageNavID + 1),13,78,'Promotion Management','../ECommerce/frmPromotionOfferList.aspx',1,-1
--	)

--	INSERT INTO dbo.TreeNavigationAuthorization
--	(
--		numGroupID,
--		numTabID,
--		numPageNavID,
--		bitVisible,
--		numDomainID,
--		tintType
--	)
--	SELECT  
--		numGroupID,
--		-1,
--		(@numMAXPageNavID + 1),
--		1,
--		numDomainID,
--		1
--	FROM    
--		AuthenticationGroupMaster
--	WHERE
--		tintGroupType=1
--COMMIT
--END TRY
--BEGIN CATCH
--	IF @@TRANCOUNT > 0
--		ROLLBACK TRANSACTION;

--	SELECT 
--		ERROR_MESSAGE(),
--		ERROR_NUMBER(),
--		ERROR_SEVERITY(),
--		ERROR_STATE(),
--		ERROR_LINE(),
--		ISNULL(ERROR_PROCEDURE(), '-');
--END CATCH

--SELECT * FROM PageNavigationDTL WHERE numTabID=-1 ANd numParentID=35


--UPDATE PageNavigationDTL SET intSortOrder=1 WHERE numPageNavID=57
--UPDATE PageNavigationDTL SET intSortOrder=2 WHERE numPageNavID=61
--UPDATE PageNavigationDTL SET intSortOrder=3 WHERE numPageNavID=64
--UPDATE PageNavigationDTL SET intSortOrder=4 WHERE numPageNavID=66
--UPDATE PageNavigationDTL SET intSortOrder=5 WHERE numPageNavID=73
--UPDATE PageNavigationDTL SET intSortOrder=6 WHERE numPageNavID=78
--UPDATE PageNavigationDTL SET intSortOrder=7 WHERE numPageNavID=260
--UPDATE PageNavigationDTL SET intSortOrder=8 WHERE numPageNavID=109
--UPDATE PageNavigationDTL SET intSortOrder=9 WHERE numPageNavID=181
--UPDATE PageNavigationDTL SET intSortOrder=10 WHERE numPageNavID=205
--UPDATE PageNavigationDTL SET intSortOrder=11 WHERE numPageNavID=79
--UPDATE PageNavigationDTL SET intSortOrder=12 WHERE numPageNavID=101


/***************************************************************************/
/*********************** LAST UPDATE ON 24 APRIL 2016 ******************/
/***************************************************************************/

--ALTER TABLE Item ADD 
--bitVirtualInventory BIT

--ALTER TABLE BizDocTemplate ADD
--numAccountClass NUMERIC(18,0)

--=======================================================================

--ALTER TABLE TabMaster ADD
--bitAddIsPopUp BIT,
--vcAddURL VARCHAR(500)

--======================================================================

--UPDATE TabMaster SET numTabName='Email',Remarks='Email',vcAddURL='contact/frmComposeWindow.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&LsEmail=&pqwRT=' WHERE numTabId=44
--UPDATE TabMaster SET vcAddURL='cases/frmAddCases.aspx' WHERE numTabId=4
--UPDATE TabMaster SET vcAddURL='Documents/frmGenDocPopUp.aspx'  WHERE numTabId=8
--UPDATE TabMaster SET vcAddURL='Projects/frmProjectAdd.aspx'  WHERE numTabId=5


--=========================================================================

--UPDATE TabMaster SET bitAddIsPopUp=1 WHERE LEN(vcAddURL) > 0


--UPDATE TabMaster SET numTabName='Activities',Remarks='Activities',vcAddURL='Admin/actionitemdetails.aspx',bitAddIsPopUp=0 WHERE numTabId=36
--UPDATE TabMaster SET vcAddURL='ContractManagement/frmcontract.aspx',bitAddIsPopUp=0  WHERE numTabId=46

--=============================================================================
--BEGIN TRY
--BEGIN TRANSACTION

--	DECLARE @numMAXPageNavID INT
--	SELECT @numMAXPageNavID = MAX(numPageNavID) FROM PageNavigationDTL


--	INSERT INTO PageNavigationDTL
--	(
--		numPageNavID,numModuleID,numParentID,vcPageNavName,vcNavURL,bitVisible,numTabID,intSortOrder
--	)
--	VALUES
--	(
--		(@numMAXPageNavID + 1),7,0,'Support','',1,4,1
--	),
--	(
--		(@numMAXPageNavID + 2),7,(@numMAXPageNavID + 1),'Cases','../cases/frmCaseList.aspx',1,4,1
--	),
--	(
--		(@numMAXPageNavID + 3),7,(@numMAXPageNavID + 1),'Knowledge Base','../cases/frmKnowledgeBaseList.aspx',1,4,1
--	)

--	INSERT INTO dbo.TreeNavigationAuthorization
--	(
--		numGroupID,
--		numTabID,
--		numPageNavID,
--		bitVisible,
--		numDomainID,
--		tintType
--	)
--	SELECT  
--		numGroupID,
--		4,
--		(@numMAXPageNavID + 1),
--		1,
--		numDomainID,
--		1
--	FROM    
--		AuthenticationGroupMaster
--	WHERE
--		tintGroupType=1

--	INSERT INTO dbo.TreeNavigationAuthorization
--	(
--		numGroupID,
--		numTabID,
--		numPageNavID,
--		bitVisible,
--		numDomainID,
--		tintType
--	)
--	SELECT  
--		numGroupID,
--		4,
--		(@numMAXPageNavID + 2),
--		1,
--		numDomainID,
--		1
--	FROM    
--		AuthenticationGroupMaster
--	WHERE
--		tintGroupType=1

--	INSERT INTO dbo.TreeNavigationAuthorization
--	(
--		numGroupID,
--		numTabID,
--		numPageNavID,
--		bitVisible,
--		numDomainID,
--		tintType
--	)
--	SELECT  
--		numGroupID,
--		4,
--		(@numMAXPageNavID + 3),
--		1,
--		numDomainID,
--		1
--	FROM    
--		AuthenticationGroupMaster
--	WHERE
--		tintGroupType=1

--COMMIT
--END TRY
--BEGIN CATCH
--	IF @@TRANCOUNT > 0
--		ROLLBACK TRANSACTION;

--	SELECT 
--		ERROR_MESSAGE(),
--		ERROR_NUMBER(),
--		ERROR_SEVERITY(),
--		ERROR_STATE(),
--		ERROR_LINE(),
--		ISNULL(ERROR_PROCEDURE(), '-');
--END CATCH
--=========================================================================
--BEGIN TRY
--BEGIN TRANSACTION
--	DECLARE @numMAXPageNavID INT
--	SELECT @numMAXPageNavID = MAX(numPageNavID) FROM PageNavigationDTL

--	INSERT INTO PageNavigationDTL
--	(
--		numPageNavID,numModuleID,numParentID,vcPageNavName,vcNavURL,bitVisible,numTabID,intSortOrder
--	)
--	VALUES
--	(
--		(@numMAXPageNavID + 1),8,22,'Old Custom Reports','../reports/frmCustomRptList.aspx',1,6,1
--	)

--	INSERT INTO dbo.TreeNavigationAuthorization
--	(
--		numGroupID,
--		numTabID,
--		numPageNavID,
--		bitVisible,
--		numDomainID,
--		tintType
--	)
--	SELECT  
--		numGroupID,
--		6,
--		(@numMAXPageNavID + 1),
--		1,
--		numDomainID,
--		1
--	FROM    
--		AuthenticationGroupMaster
--	WHERE
--		tintGroupType=1
--COMMIT
--END TRY
--BEGIN CATCH
--	IF @@TRANCOUNT > 0
--		ROLLBACK TRANSACTION;

--	SELECT 
--		ERROR_MESSAGE(),
--		ERROR_NUMBER(),
--		ERROR_SEVERITY(),
--		ERROR_STATE(),
--		ERROR_LINE(),
--		ISNULL(ERROR_PROCEDURE(), '-');
--END CATCH
--=================================================================================
--BEGIN TRY
--BEGIN TRANSACTION
--	DECLARE @numMAXPageNavID INT
--	SELECT @numMAXPageNavID = MAX(numPageNavID) FROM PageNavigationDTL

--	INSERT INTO PageNavigationDTL
--	(
--		numPageNavID,numModuleID,numParentID,vcPageNavName,vcNavURL,bitVisible,numTabID,intSortOrder
--	)
--	VALUES
--	(
--		(@numMAXPageNavID + 1),8,22,'Sales Forecasts','../ForeCasting/frmForeCasting.aspx',1,6,1
--	)

--	INSERT INTO dbo.TreeNavigationAuthorization
--	(
--		numGroupID,
--		numTabID,
--		numPageNavID,
--		bitVisible,
--		numDomainID,
--		tintType
--	)
--	SELECT  
--		numGroupID,
--		6,
--		(@numMAXPageNavID + 1),
--		1,
--		numDomainID,
--		1
--	FROM    
--		AuthenticationGroupMaster
--	WHERE
--		tintGroupType=1
--COMMIT
--END TRY
--BEGIN CATCH
--	IF @@TRANCOUNT > 0
--		ROLLBACK TRANSACTION;

--	SELECT 
--		ERROR_MESSAGE(),
--		ERROR_NUMBER(),
--		ERROR_SEVERITY(),
--		ERROR_STATE(),
--		ERROR_LINE(),
--		ISNULL(ERROR_PROCEDURE(), '-');
--END CATCH

--================================================================================================
---- TODO: CHECK IF numTabID 2 = Forecasting, numTabID 68 = Dashboard

--DELETE FROM TreeNavigationAuthorization WHERE numTabID IN (2,68)
--DELETE FROM PageNavigationDTL WHERE numTabID IN (2,68)
--DELETE FROM TabMaster WHERE numTabId IN (2,68)

--================================================================================================

--ALTER TABLE PageNavigationDTL ADD
--bitAddIsPopUp BIT,
--vcAddURL VARCHAR(1000)

--================================================================================================

---- Sales Opportunity
--UPDATE PageNavigationDTL SET vcAddURL='~/opportunity/frmNewOrder.aspx?OppStatus=0&OppType=1' WHERE numPageNavID=123
---- Purchase Opportunity
--UPDATE PageNavigationDTL SET vcAddURL='~/opportunity/frmAddOpportunity.aspx?OppType=2' WHERE numPageNavID=124
---- Sales Order
--UPDATE PageNavigationDTL SET vcAddURL='~/opportunity/frmNewOrder.aspx?OppStatus=1&OppType=1' WHERE numPageNavID=125
---- Purchase Order
--UPDATE PageNavigationDTL SET vcAddURL='~/Opportunity/frmNewPurchaseOrder.aspx' WHERE numPageNavID=126
---- Sales Return
--UPDATE PageNavigationDTL SET vcAddURL='~/Opportunity/frmNewReturn.aspx?ReturnType=1' WHERE numPageNavID=149
---- Purchase Return
--UPDATE PageNavigationDTL SET vcAddURL='~/Opportunity/frmNewReturn.aspx?ReturnType=2' WHERE numPageNavID=207


---- Campaigns
--UPDATE PageNavigationDTL SET vcAddURL='~/Marketing/frmNewCampaign.aspx' WHERE numPageNavID=16
---- Drip Campaigns
--UPDATE PageNavigationDTL SET vcAddURL='~/Marketing/frmECampaignDtls.aspx' WHERE numPageNavID=168
---- Surveys/Questionaires
--UPDATE PageNavigationDTL SET vcAddURL='~/Marketing/frmCampaignSurveyEditor.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&numSurId=0' WHERE numPageNavID=18

---- TODO: CHECK numPageNavID AS NEW LINK Drip Cases
--UPDATE PageNavigationDTL SET vcAddURL='~/cases/frmAddCases.aspx' WHERE numPageNavID=247
---- TODO: CHECK numPageNavID AS NEW LINK Knowledge Base
--UPDATE PageNavigationDTL SET vcAddURL='~/cases/frmAddSolution.aspx' WHERE numPageNavID=248


---- My Leads
--UPDATE PageNavigationDTL SET vcAddURL='~/include/frmAddOrganization.aspx?GrpID=5&RelID=1&FormID=34' WHERE numPageNavID=8
---- Web Leads
--UPDATE PageNavigationDTL SET vcAddURL='~/include/frmAddOrganization.aspx?GrpID=1&RelID=1&FormID=34' WHERE numPageNavID=9
---- Public Leads
--UPDATE PageNavigationDTL SET vcAddURL='~/include/frmAddOrganization.aspx?GrpID=2&RelID=1&FormID=34' WHERE numPageNavID=10
---- Prospects
--UPDATE PageNavigationDTL SET vcAddURL='~/include/frmAddOrganization.aspx?RelID=3&FormID=35' WHERE numPageNavID=11
---- Accounts
--UPDATE PageNavigationDTL SET vcAddURL='~/include/frmAddOrganization.aspx?RelID=2&FormID=36' WHERE numPageNavID=12
---- Contacts
--UPDATE PageNavigationDTL SET vcAddURL='~/contact/newcontact.aspx' WHERE numPageNavID=13

---- Credit Memo
--UPDATE PageNavigationDTL SET vcAddURL='~/Opportunity/frmNewReturn.aspx?ReturnType=3' WHERE numPageNavID=211
---- Refund
--UPDATE PageNavigationDTL SET vcAddURL='~/Opportunity/frmNewReturn.aspx?ReturnType=4' WHERE numPageNavID=212


---- Services
--UPDATE PageNavigationDTL SET vcAddURL='~/Items/frmNewItem.aspx?FormID=87&ItemType=Services' WHERE numPageNavID=1
---- Kits
--UPDATE PageNavigationDTL SET vcAddURL='~/Items/frmNewItem.aspx?FormID=86&ItemType=Kits' WHERE numPageNavID=2
---- Assemblies
--UPDATE PageNavigationDTL SET vcAddURL='~/Items/frmNewItem.aspx?FormID=86&ItemType=Assembly' WHERE numPageNavID=3
---- Inventory item
--UPDATE PageNavigationDTL SET vcAddURL='~/Items/frmNewItem.aspx?FormID=86' WHERE numPageNavID=4
---- Non Inventory item
--UPDATE PageNavigationDTL SET vcAddURL='~/Items/frmNewItem.aspx?FormID=87' WHERE numPageNavID=71
---- Serialized/LOT #s Items
--UPDATE PageNavigationDTL SET vcAddURL='~/Items/frmNewItem.aspx?FormID=88' WHERE numPageNavID=72
---- Asset Items
--UPDATE PageNavigationDTL SET vcAddURL='~/Items/frmNewItem.aspx?FormID=86' WHERE numPageNavID=171


--================================================================================================================

--UPDATE PageNavigationDTL SET bitAddIsPopUp=1 WHERE vcAddURL IS NOT NULL

--===============================================================================================================

--UPDATE PageNavigationDTL SET bitAddIsPopUp=0 WHERE numPageNavID = 18

--=========================================================================
--BEGIN TRY
--BEGIN TRANSACTION
--	DECLARE @numMAXPageNavID INT
--	SELECT @numMAXPageNavID = MAX(numPageNavID) FROM PageNavigationDTL

--	INSERT INTO PageNavigationDTL
--	(
--		numPageNavID,numModuleID,numParentID,vcPageNavName,vcNavURL,bitVisible,numTabID,intSortOrder,vcAddURL,bitAddIsPopUp
--	)
--	VALUES
--	(
--		(@numMAXPageNavID + 1),35,54,'Add Bill','',1,45,1,'~/Accounting/frmAddBill.aspx',1
--	)

--	INSERT INTO dbo.TreeNavigationAuthorization
--	(
--		numGroupID,
--		numTabID,
--		numPageNavID,
--		bitVisible,
--		numDomainID,
--		tintType
--	)
--	SELECT  
--		numGroupID,
--		45,
--		(@numMAXPageNavID + 1),
--		1,
--		numDomainID,
--		1
--	FROM    
--		AuthenticationGroupMaster
--	WHERE
--		tintGroupType=1
--COMMIT
--END TRY
--BEGIN CATCH
--	IF @@TRANCOUNT > 0
--		ROLLBACK TRANSACTION;

--	SELECT 
--		ERROR_MESSAGE(),
--		ERROR_NUMBER(),
--		ERROR_SEVERITY(),
--		ERROR_STATE(),
--		ERROR_LINE(),
--		ISNULL(ERROR_PROCEDURE(), '-');
--END CATCH
--=================================================================================

--UPDATE PageNavigationDTL SET vcImageURL='' WHERE numTabID=1 AND vcImageURL='../images/tf_note.gif'

--==================================================================================

--UPDATE PageNavigationDTL SET bitVisible=0 WHERE numTabID=-1 AND numParentID=157

--==================================================================================

--UPDATE PageNavigationDtl SET vcImageURL='~/images/Admin-User.png' WHERE numPageNavID=57
--UPDATE PageNavigationDtl SET vcImageURL='~/images/Admin-List.png',vcPageNavName='Add & Edit Drop-Down Lists' WHERE numPageNavID=61
--UPDATE PageNavigationDtl SET vcImageURL='~/images/Admin-BizForm.png' WHERE numPageNavID=64
--UPDATE PageNavigationDtl SET vcImageURL='~/images/Admin-Workflow.png' WHERE numPageNavID=66
--UPDATE PageNavigationDtl SET vcImageURL='~/images/Admin-ECommerce.png' WHERE numPageNavID=73
--UPDATE PageNavigationDtl SET vcImageURL='~/images/Admin-Price-Control.png' WHERE numPageNavID=78
--UPDATE PageNavigationDtl SET vcImageURL='~/images/Admin-Global.png' WHERE numPageNavID=79
--UPDATE PageNavigationDtl SET vcImageURL='~/images/Admin-Field-Management.png' WHERE numPageNavID=109
--UPDATE PageNavigationDtl SET vcImageURL='~/images/Admin-Import-Export.png' WHERE numPageNavID=205
--UPDATE PageNavigationDtl SET vcImageURL='~/images/Admin-Commission.png' WHERE numPageNavID=181

--==================================================================================

--UPDATE TabMaster SET vcURL='Opportunity/frmDealList.aspx?type=1' WHERE numTabId=1 --opportunity/frmOppNavigation.aspx
--UPDATE TabMaster SET vcURL='' WHERE numTabId=3 --Marketing/frmMarNavigation.aspx
--UPDATE TabMaster SET vcURL='reports/reportslinks.aspx' WHERE numTabId=6 --reports/frmrepNavigation.aspx
--UPDATE TabMaster SET vcURL='account/frmAccountList.aspx' WHERE numTabId=7 --prospects/frmRelNavigation.aspx
--UPDATE TabMaster SET vcURL='' WHERE numTabId=8 --Documents/frmDocNavigation.aspx
--UPDATE TabMaster SET vcURL='Items/frmItemList.aspx?Page=All Items&ItemGroup=0' WHERE numTabId=80 --Items/ItemNavigation.aspx
--UPDATE TabMaster SET vcURL='Accounting/frmChartofAccounts.aspx' WHERE numTabId=45 --Accounting/frmActNavigation.aspx
--UPDATE TabMaster SET vcURL='Outlook/frmInboxItems.aspx' WHERE numTabID=44 --Outlook/frmBizOWA.aspx

--===============================================================================================================

--UPDATE PageNavigationDTL SET vcImageURL='~/images/Icon/Item.png' WHERE numPageNavID=177 --ITEM
--UPDATE PageNavigationDTL SET vcImageURL='~/images/Icon/organization.png' WHERE numPageNavID=135 --Companies & Contacts
--UPDATE PageNavigationDTL SET vcImageURL='~/images/Icon/Sales Order.png' WHERE numPageNavID=136 --Orders/Opps
--UPDATE PageNavigationDTL SET vcImageURL='~/images/Icon/Case.png' WHERE numPageNavID=138 --Cases
--UPDATE PageNavigationDTL SET vcImageURL='~/images/Icon/project.png' WHERE numPageNavID=139 --Projects
--UPDATE PageNavigationDTL SET vcImageURL='~/images/Icon/campaign.png' WHERE numPageNavID=140 --Surveys
--UPDATE PageNavigationDTL SET vcImageURL='~/images/Icon/Search.png' WHERE numPageNavID=178 --My Saved Search
--UPDATE PageNavigationDTL SET vcImageURL='~/images/Icon/Accounting.png' WHERE numPageNavID=204 --Transaction Search

--====================================================================================================================

--UPDATE PageNavigationDTl SET vcAddURL='~/opportunity/frmAmtPaid.aspx?frm=topTab',bitAddIsPopUp=1 WHERE numPageNavID=55

--UPDATE PageNavigationDTl SET vcNavURL='' WHERE numPageNavID=55

--========================================================================================================================

--UPDATE PageNavigationDTL SET numTabID=45 WHERE numPageNavID IN (211,212)
--UPDATE TreeNavigationAuthorization SET numTabID=45 WHERE numPageNavID IN (211,212)


--=========================================================================================================================

--UPDATE PageNavigationDTl SET vcNavURL='../pagelayout/frmCustomisePageLayoutpopup.aspx?Ctype=L&type=1&PType=2&FormId=34' WHERE numPageNavID=188
--UPDATE PageNavigationDTl SET vcNavURL='../pagelayout/frmCustomisePageLayoutpopup.aspx?Ctype=c&type=0&PType=2&FormId=10' WHERE numPageNavID=191
--UPDATE PageNavigationDTl SET vcNavURL='../pagelayout/frmCustomisePageLayoutpopup.aspx?Ctype=S&type=0&PType=2&FormId=12' WHERE numPageNavID=192
--UPDATE PageNavigationDTl SET vcNavURL='../pagelayout/frmCustomisePageLayoutpopup.aspx?Ctype=I&type=1&PType=2&FormId=86' WHERE numPageNavID=231
--=====================================

--UPDATE PageNavigationDTL SET bitVisible=0 WHERE numPageNavID IN (38,42,39,40,41,43,44)

/***************************************************************************/
/*********************** LAST UPDATE ON 04 FEBRUARY 2016 ******************/
/***************************************************************************/

--ALTER TABLE OpportunityItems ADD
--[vcAttrValues] VARCHAR(500)

--=============================================================================

--ALTER TABLE eCommerceDTL ADD
--numDefaultClass NUMERIC(18,0)

--===============================================================================

--USE [Production.2014]
--GO

--/****** Object:  Table [dbo].[CategoryProfile]    Script Date: 22-Jan-16 12:12:30 PM ******/
--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

--SET ANSI_PADDING ON
--GO

--CREATE TABLE [dbo].[CategoryProfile](
--	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
--	[vcName] [varchar](200) NOT NULL,
--	[numDomainID] [numeric](18, 0) NOT NULL,
--	[dtCreated] [datetime] NOT NULL,
--	[numCreatedBy] [numeric](18, 0) NOT NULL,
--	[dtModified] [datetime] NULL,
--	[numModifiedBy] [numeric](18, 0) NULL,
-- CONSTRAINT [PK_CategoryProfile] PRIMARY KEY CLUSTERED 
--(
--	[ID] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
--) ON [PRIMARY]

--GO

--SET ANSI_PADDING ON
--GO

--======================================================================================================


--USE [Production.2014]
--GO

--/****** Object:  Table [dbo].[CategoryProfileSites]    Script Date: 22-Jan-16 12:12:40 PM ******/
--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

--CREATE TABLE [dbo].[CategoryProfileSites](
--	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
--	[numCategoryProfileID] [numeric](18, 0) NOT NULL,
--	[numSiteID] [numeric](18, 0) NOT NULL,
-- CONSTRAINT [PK_CategoryProfileSites] PRIMARY KEY CLUSTERED 
--(
--	[ID] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
--) ON [PRIMARY]

--GO

--ALTER TABLE [dbo].[CategoryProfileSites]  WITH CHECK ADD  CONSTRAINT [FK_CategoryProfileSites_Sites] FOREIGN KEY([numSiteID])
--REFERENCES [dbo].[Sites] ([numSiteID])
--ON DELETE CASCADE
--GO

--ALTER TABLE [dbo].[CategoryProfileSites] CHECK CONSTRAINT [FK_CategoryProfileSites_Sites]
--GO

--===============================================================================================================

--BEGIN TRY
--BEGIN TRANSACTION

--DECLARE @TEMP TABLE
--(
--	ID NUMERIC(18,0) IDENTITY(1,1),
--	numDomainID NUMERIC(18,0)
--)

--INSERT INTO @TEMP (numDomainID) SELECT numDomainID FROM Domain

--DECLARE @i AS INT = 1
--DECLARE @COUNT AS INT
--DECLARE @numDomainID AS NUMERIC(18,0)

--SELECT @COUNT=COUNT(*) FROM @TEMP


--WHILE @i <= @COUNT
--BEGIN
--	SELECT @numDomainID=numDomainID FROM @TEMP WHERE ID=@i


--	INSERT INTO CategoryProfile
--	(
--		vcName,
--		numDomainID,
--		dtCreated,
--		numCreatedBy
--	)
--	VALUES
--	(
--		'Default Category',
--		@numDomainID,
--		GETUTCDATE(),
--		ISNULL((SELECT TOP 1 numUserDetailID FROM UserMaster WHERE numDomainID=@numDomainID),0)
--	)


--	SET @i = @i + 1
--END

--COMMIT
--END TRY
--BEGIN CATCH
--	IF @@TRANCOUNT > 0
--		ROLLBACK TRANSACTION;

--	SELECT 
--		ERROR_MESSAGE(),
--		ERROR_NUMBER(),
--		ERROR_SEVERITY(),
--		ERROR_STATE(),
--		ERROR_LINE(),
--		ISNULL(ERROR_PROCEDURE(), '-');
--END CATCH

--================================================================================

--BEGIN TRY
--BEGIN TRANSACTION

--INSERT INTO CategoryProfileSites
--(
--	numCategoryProfileID,
--	numSiteID
--)
--SELECT 
--	ISNULL((SELECT TOP 1 CP.ID FROM CategoryProfile CP WHERE CP.numDomainID=S.numDomainID),0),
--	numSiteID
--FROM 
--	Sites S

--COMMIT
--END TRY
--BEGIN CATCH
--	IF @@TRANCOUNT > 0
--		ROLLBACK TRANSACTION;

--	SELECT 
--		ERROR_MESSAGE(),
--		ERROR_NUMBER(),
--		ERROR_SEVERITY(),
--		ERROR_STATE(),
--		ERROR_LINE(),
--		ISNULL(ERROR_PROCEDURE(), '-');
--END CATCH

--========================================================================================

--ALTER TABLE Category ADD
--numCategoryProfileID NUMERIC(18,0)

--========================================================================================

--UPDATE
--	C
--SET
--	C.numCategoryProfileID=CP.ID
--FROM 
--	Category C
--JOIN
--	CategoryProfile CP
--ON
--	C.numDomainID = CP.numDomainID

--=========================================================================================

----ADD FOLLOWING LINE IN BIZSERVICE CONFIG
-- <add key="RecordHistoryRate" value="5"/>


/***************************************************************************/
/*********************** LAST UPDATE ON 13 JANUARY 2016 ******************/
/***************************************************************************/

--BEGIN TRANSACTION

--DECLARE @numListItemID AS INT

--INSERT INTO ListDetails 
--(
--	numListID,
--	vcData,
--	numCreatedBY,
--	bitDelete,
--	numDomainID,
--	constFlag,
--	sintOrder
--)
--VALUES
--(
--	27,
--	'Pick List',
--	1,
--	0,
--	1,
--	1,
--	1
--)


--SELECT @numListItemID = SCOPE_IDENTITY()


--DECLARE @TEMP TABLE
--(
--	ID INT IDENTITY(1,1),
--	numDomainID INT
--)


--INSERT INTO @TEMP (numDomainID) SELECT numDomainID FROM Domain

--DECLARE @i AS INT = 1
--DECLARE @COUNT AS INT 
--DECLARE @numDomainID AS INT

--SELECT @COUNT=COUNT(*) FROM @TEMP

--WHILE @i <= @COUNT
--BEGIN
--	SELECT @numDomainID = numDomainID FROM  @TEMP WHERE ID = @i

--	INSERT INTO BizDocTemplate
--	(
--		numDomainID,numBizDocID,numOppType,txtBizDocTemplate,txtCSS,bitEnabled,tintTemplateType,vcTemplateName,bitDefault
--	)
--	VALUES
--	(
--		@numDomainID,@numListItemID,1,'','',1,0,'Pick List',1
--	)

--	SET @i = @i + 1
--END


--UPDATE BizDocTemplate SET txtBizDocTemplate='<table width="100%" cellpadding="0" border="0">
--    <tbody>
--        <tr>
--            <td valign="top" align="left">#Logo#
--            </td>
--            <td align="right" style="white-space: nowrap;">#BizDocType#
--            </td>
--        </tr>
--    </tbody>
--</table>
--<table width="100%" cellpadding="0" border="0">
--    <tbody>
--        <tr>
--            <td align="right">
--            <table width="100%">
--                <tbody>
--                    <tr>
--                        <td class="RowHeader">Order
--                        </td>
--                        <td class="RowHeader">Date
--                        </td>
--                        <td class="RowHeader">Assigned To
--                        </td>
--                    </tr>
--                    <tr>
--                        <td class="normal1">#OrderID#
--                        </td>
--                        <td class="normal1">#BizDocCreatedDate#
--                        </td>
--                        <td class="normal1">#AssigneeName#
--                        </td>
--                    </tr>
--                </tbody>
--            </table>
--            </td>
--        </tr>
--        <tr>
--            <td>
--            <table width="100%" height="100%">
--                <tbody>
--                    <tr>
--                        <td class="RowHeader">Sold To
--                        </td>
--                        <td class="RowHeader">Ship To
--                        </td>
--                    </tr>
--                    <tr>
--                        <td class="normal1">#Customer/VendorOrganizationName#<br />
--                        #Customer/VendorBillToAddress#
--                        </td>
--                        <td class="normal1">#Customer/VendorOrganizationName#<br />
--                        #Customer/VendorShipToAddress#
--                        </td>
--                    </tr>
--                </tbody>
--            </table>
--            </td>
--        </tr>
--        <tr class="normal1">
--            <td>#Products#
--            </td>
--        </tr>
--    </tbody>
--</table>
--<table width="100%">
--    <tbody>
--        <tr>
--            <td>#FooterImage#
--            </td>
--        </tr>
--    </tbody>
--</table>' WHERE numBizDocID = @numListItemID


--UPDATE BizDocTemplate SET txtCSS='.title{color: #696969; /*padding:10px;*/font: bold 30px Arial, Helvetica, sans-serif;}
--.RowHeader{background-color: #dedede;color: #333;font-family: Arial;font-size: 8pt;}
--.RowHeader.hyperlink{color: #333;}
--.ItemHeader, .ItemStyle, .AltItemStyle{border-width: 1px;padding: 8px;font: normal 12px/17px arial;border-style: solid;border-color: #666666;background-color: #dedede;}
--.ItemHeader{border-width: 1px;padding: 8px;border-style: solid;border-color: #666666;}
--.ItemStyle td{border-width: 1px;padding: 8px;border-style: solid;border-color: #666666;background-color: #ffffff;}
--.AltItemStyle{background-color: White;border-color: black;padding: 8px;}
--.AltItemStyle td{padding: 8px;}
--.ItemHeader td{border-width: 1px;padding: 8px;font-weight: bold;border-style: solid;border-color: #666666;background-color: #dedede;}
--.ItemHeader td th{border-width: 1px;padding: 8px;border-style: solid;border-color: #666666;background-color: #dedede;}
--#tblBizDocSumm{font: normal 12px verdana;color: #333;border-width: 1px;border-color: #666666;border-collapse: collapse;background-color: #dedede;}
--#tblBizDocSumm td{border-width: 1px;padding: 8px;border-style: solid;border-color: #666666;}
--.WordWrapSerialNo{width: 30%;word-break: break-all;}' WHERE numBizDocID = @numListItemID

--ROLLBACK


--/* DO NOT RUN FOLLOWING SCRIPT ON PRODUCTION SERVER */
--ALTER TABLE ParentChildCustomFieldMap ALTER
--COLUMN numParentFieldID NUMERIC(18,0)

--ALTER TABLE ParentChildCustomFieldMap ALTER
--COLUMN numChildFieldID NUMERIC(18,0)

--/* Made ContactID Editable in cases form */
--UPDATE DycFieldMaster SET vcFieldName='Contact', vcPropertyName='ContactID', vcDbColumnName='numContactID',vcOrigDbColumnName='numContactID', vcFieldDataType = 'N', vcAssociatedControlType = 'SelectBox', vcListItemType='', bitSettingField = 1 WHERE numFieldID=128
--UPDATE DycFormField_Mapping SET vcFieldName='Contact', vcPropertyName='ContactID' , bitSettingField=1,vcAssociatedControlType = 'SelectBox' WHERE numFormID=12 AND numFieldID = 128
--UPDATE DynamicFormFieldMaster SET vcFieldType='N',vcDbColumnName='numContactID',vcAssociatedControlType = 'SelectBox' WHERE numFormFieldID = 1630


--/* Made ContactID Editable in Opp/Order Detail form */
--UPDATE DycFieldMaster SET vcFieldName='Contact', vcPropertyName='ContactID', vcDbColumnName='numContactID',vcOrigDbColumnName='numContactID', vcFieldDataType = 'N', vcAssociatedControlType = 'SelectBox', vcListItemType='', bitSettingField = 1 WHERE numFieldID=102
--UPDATE DycFormField_Mapping SET vcFieldName='Contact', vcPropertyName='ContactID' , bitSettingField=1,vcAssociatedControlType = 'SelectBox' WHERE numFormID=38 AND numFieldID = 102
--UPDATE DynamicFormFieldMaster SET vcFieldType='N',vcDbColumnName='numContactID',vcAssociatedControlType = 'SelectBox' WHERE numFormFieldID = 1855

--/* Made Customer Side Project Manager Editable in Project Detail form */
--UPDATE DycFieldMaster SET bitInlineEdit=1,bitAllowEdit=1  WHERE numFieldId=152
--UPDATE DycFormField_Mapping SET bitInlineEdit=1,bitAllowEdit=1 WHERE numFormID = 13 AND numFieldID=152

/***************************************************************************/
/*********************** LAST UPDATE ON 09 DECEMBER 2015 ******************/
/***************************************************************************/


--BEGIN TRANSACTION

--UPDATE DynamicFormFieldMaster SET vcFormFieldName = 'Tax Value' WHERE numFormID=45 AND numFormFieldId=2024
--UPDATE DycFormField_Mapping SET vcFieldName = 'Tax Value' WHERE numFormID=45 AND numFieldID=346

--DECLARE @numFieldID AS NUMERIC(18,0)
--DECLARE @numFormFieldID AS NUMERIC(18,0)

--INSERT INTO DycFieldMaster
--(numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,[order],tintRow,tintColumn,
--bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numListID)
--VALUES
--(9,'Tax Value Type','tintTaxType','tintTaxType','tintTaxType','TaxDetails','N','R','TextBox',7,1,7,1,0,0,1,1,0,0,0,1,0)

--SELECT @numFieldID = SCOPE_IDENTITY()

--INSERT INTO DynamicFormFieldMaster
--(numFormID,vcFormFieldName,vcFieldType,vcAssociatedControlType,vcDbColumnName,numListID,bitDeleted,vcFieldDataType,vcOrigDbColumnName,bitAllowEdit,vcLookBackTableName,
-- bitDefault,[order],bitInResults,vcPropertyName,tintRow,tintColumn,bitSettingField,bitAddField,bitDetailField,bitAllowSorting)
--VALUES
--(45,'Tax Value Type','R','EditBox','tintTaxType',0,0,'N','tintTaxType',0,'TaxDetails',1,7,1,'tintTaxType',1,7,1,0,0,0)

--SELECT @numFormFieldID = SCOPE_IDENTITY()

--INSERT INTO DycFormField_Mapping
--(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,bitInResults,bitDeleted,
--bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numFormFieldID)
--VALUES
--(9,@numFieldID,45,0,0,'Tax Value Type','TextBox','tintTaxType',7,1,7,1,0,1,1,0,0,0,1,@numFormFieldID)

--ROLLBACK


--==========================================================

--BEGIN TRANSACTION


--DECLARE @numFieldID NUMERIC(18,0)
--DECLARE @numFormFieldID NUMERIC(18,0)

--INSERT INTO DynamicFormFieldMaster
--(
--	numFormID,vcFormFieldName,vcFieldType,vcAssociatedControlType,vcDbColumnName,numListID,bitDeleted,vcFieldDataType,vcOrigDbColumnName,bitAllowEdit,vcLookBackTableName,bitDefault,bitInResults
--)
--VALUES
--(
--	7,'Ordered Qty','R','EditBox','numTotalUnitHour',0,0,'N','numTotalUnitHour',1,'OpportunityBizDocItems',0,1
--)

--SELECT @numFormFieldID = SCOPE_IDENTITY()


--INSERT INTO DycFieldMaster 
--(
--	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,numListID,bitInlineEdit,bitDeleted,bitAllowEdit,bitDefault,bitAllowFiltering,bitInResults
--)
--VALUES
--(
--	3,'Total Units','numTotalUnitHour','numTotalUnitHour','OpportunityBizDocItems','M','R','TextBox',0,1,0,1,0,1,1
--)

--SELECT @numFieldID = SCOPE_IDENTITY()

--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,bitInResults,bitDeleted,bitDefault,bitAllowFiltering,numFormFieldID
--)
--VALUES
--(
--	3,@numFieldID,7,1,1,'Ordered Qty','TextBox',1,0,0,1,@numFormFieldID
--)

--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,bitInResults,bitDeleted,bitDefault,bitAllowFiltering,numFormFieldID
--)
--VALUES
--(
--	3,@numFieldID,8,1,1,'Ordered Qty','TextBox',1,0,0,1,@numFormFieldID
--)

--ROLLBACK

--========================================================================

--ALTER TABLE DivisionMaster ADD
--tintPriceLevel INT NULL


--========================================================================
--GO

--ALTER TABLE [dbo].[WareHouseItems] DROP CONSTRAINT [DF_WareHouseItems_numReorder]
--GO

--ALTER TABLE WarehouseItems ALTER COLUMN numOnHand FLOAT
--ALTER TABLE WarehouseItems ALTER COLUMN numOnOrder FLOAT
--ALTER TABLE WarehouseItems ALTER COLUMN numReorder FLOAT
--ALTER TABLE WarehouseItems ALTER COLUMN numAllocation FLOAT
--ALTER TABLE WarehouseItems ALTER COLUMN numBackOrder FLOAT

--ALTER TABLE [dbo].[WareHouseItems] ADD  CONSTRAINT [DF_WareHouseItems_numReorder]  DEFAULT ((0)) FOR [numReorder]
--GO


--ALTER TABLE OpportunityItems ALTER COLUMN numUnitHour FLOAT
--ALTER TABLE OpportunityItems ALTER COLUMN [numUnitHourReceived] FLOAT
--ALTER TABLE OpportunityItems ALTER COLUMN [numQtyShipped] FLOAT

--ALTER TABLE OpportunityKitItems ALTER COLUMN numQtyItemsReq FLOAT
--ALTER TABLE OpportunityKitItems ALTER COLUMN numQtyItemsReq_Orig FLOAT
--ALTER TABLE OpportunityKitItems ALTER COLUMN [numQtyShipped] FLOAT

--ALTER TABLE OpportunityKitChildItems ALTER COLUMN numQtyItemsReq FLOAT
--ALTER TABLE OpportunityKitChildItems ALTER COLUMN numQtyItemsReq_Orig FLOAT
--ALTER TABLE OpportunityKitChildItems ALTER COLUMN [numQtyShipped] FLOAT

--ALTER TABLE OpportunityBizDocItems ALTER COLUMN numUnitHour FLOAT

--ALTER TABLE WarehouseItems_Tracking ALTER COLUMN numTotalOnHand FLOAT


--ALTER TABLE SalesTemplateItems ALTER COLUMN numUnitHour FLOAT
--ALTER TABLE SalesTemplateItems ALTER COLUMN UOMConversionFactor DECIMAL(30,16)

--ALTER TABLE ReturnItems ALTER COLUMN monPrice DECIMAL(30,16)
--ALTER TABLE ReturnItems ALTER COLUMN numUnitHour FLOAT
--ALTER TABLE ReturnItems ALTER COLUMN numUnitHourReceived FLOAT

--ALTER TABLE WorkOrderDetails ALTER COLUMN numQtyItemsReq FLOAT
--ALTER TABLE WorkOrderDetails ALTER COLUMN numQtyItemsReq_Orig FLOAT

/***************************************************************************/
/*********************** LAST UPDATE ON 23 NOVEMBER 2015 ******************/
/***************************************************************************/

--ALTER TABLE DivisionMaster ADD
--numAccountClassID NUMERIC(18,0)

---------------------------------------------------------------------------

--ALTER TABLE Domain 
--DROP COLUMN IsEnableClassTracking

---------------------------------------------------------------------------

--ALTER TABLE Domain
--DROP COLUMN IsEnableUserLevelClassTracking

--====================================================================

--GO

--/****** Object:  Table [dbo].[ItemUOMConversion]    Script Date: 23-Nov-15 3:00:10 PM ******/
--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

--CREATE TABLE [dbo].[ItemUOMConversion](
--	[numItemUOMConvID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
--	[numDomainID] [numeric](18, 0) NOT NULL,
--	[numItemCode] [numeric](18, 0) NOT NULL,
--	[numSourceUOM] [numeric](18, 0) NOT NULL,
--	[numTargetUOM] [numeric](18, 0) NOT NULL,
--	[numTargetUnit] [float] NOT NULL,
--	[numCreatedBy] [numeric](18, 0) NOT NULL,
--	[dtCreated] [datetime] NOT NULL,
--	[numModifiedBy] [numeric](18, 0) NULL,
--	[dtModified] [datetime] NULL,
-- CONSTRAINT [PK_ItemUOMConversion] PRIMARY KEY CLUSTERED 
--(
--	[numItemUOMConvID] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
--) ON [PRIMARY]

--GO

--ALTER TABLE [dbo].[ItemUOMConversion]  WITH CHECK ADD  CONSTRAINT [FK_ItemUOMConversion_Domain] FOREIGN KEY([numDomainID])
--REFERENCES [dbo].[Domain] ([numDomainId])
--ON DELETE CASCADE
--GO

--ALTER TABLE [dbo].[ItemUOMConversion] CHECK CONSTRAINT [FK_ItemUOMConversion_Domain]
--GO

--ALTER TABLE [dbo].[ItemUOMConversion]  WITH CHECK ADD  CONSTRAINT [FK_ItemUOMConversion_Item] FOREIGN KEY([numItemCode])
--REFERENCES [dbo].[Item] ([numItemCode])
--ON DELETE CASCADE
--GO

--ALTER TABLE [dbo].[ItemUOMConversion] CHECK CONSTRAINT [FK_ItemUOMConversion_Item]
--GO

--ALTER TABLE [dbo].[ItemUOMConversion]  WITH CHECK ADD  CONSTRAINT [FK_ItemUOMConversion_UOM] FOREIGN KEY([numSourceUOM])
--REFERENCES [dbo].[UOM] ([numUOMId])
--GO

--ALTER TABLE [dbo].[ItemUOMConversion] CHECK CONSTRAINT [FK_ItemUOMConversion_UOM]
--GO

--ALTER TABLE [dbo].[ItemUOMConversion]  WITH CHECK ADD  CONSTRAINT [FK_ItemUOMConversion_UOM1] FOREIGN KEY([numTargetUOM])
--REFERENCES [dbo].[UOM] ([numUOMId])
--GO

--ALTER TABLE [dbo].[ItemUOMConversion] CHECK CONSTRAINT [FK_ItemUOMConversion_UOM1]
--GO

--=====================================================================================

--ALTER TABLE Domain ADD
--bitEnableItemLevelUOM BIT

--=====================================================

--ALTER TABLE OpportunityItems ALTER
--COLUMN monPrice DECIMAL(30,16)

--ALTER TABLE OpportunityItems ALTER
--COLUMN fltDiscount DECIMAL(30,16)


--ALTER TABLE OpportunityBizDocItems ALTER
--COLUMN monPrice DECIMAL(30,16)

--ALTER TABLE OpportunityBizDocItems ALTER
--COLUMN fltDiscount DECIMAL(30,16)
--=====================================================

--BEGIN TRANSACTION

--DECLARE @numFieldID AS NUMERIC(18,0)

--INSERT INTO DycFieldMaster 
--(
--	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcLookBackTableName,vcFieldDataType,
--	vcFieldType,vcAssociatedControlType,vcListItemType,numListID,[order],bitInResults,bitDeleted,bitAllowEdit,
--	bitDefault,bitSettingField,bitAllowFiltering
--)
--SELECT
--	numModuleID,vcFieldName,'monPriceUOM','monPriceUOM',vcLookBackTableName,vcFieldDataType,
--	vcFieldType,vcAssociatedControlType,vcListItemType,numListID,[order],bitInResults,bitDeleted,bitAllowEdit,
--	bitDefault,bitSettingField,bitAllowFiltering
--FROM
--	DycFieldMaster
--WHERE
--	numFieldId = 259

--SELECT @numFieldID = SCOPE_IDENTITY()

--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,[order],bitInResults,
--	bitdeleted,bitDefault,bitSettingField,bitAllowFiltering
--)
--SELECT
--	3,@numFieldID,26,1,1,'Unit Price','TextBox',8,0,0,1,1,1

--ROLLBACK
	
--UPDATE DycFormConfigurationDetails SET numFieldId=@numFieldID WHERE numFormId = 26 AND numFieldId=259 

--DELETE FROM DycFormField_Mapping WHERE numFormID=26 AND numFieldID=259

--=====================================================================================

--ALTER TABLE TaxDetails ADD
--tintTaxType TINYINT NULL

--UPDATE TaxDetails SET tintTaxType = 1


--ALTER TABLE OpportunityMasterTaxItems ADD 
--tintTaxType TINYINT NULL

--UPDATE OpportunityMasterTaxItems SET tintTaxType = 1


--ALTER TABLE OpportunityBizDocTaxItems ADD 
--tintTaxType TINYINT NULL

--UPDATE OpportunityBizDocTaxItems SET tintTaxType = 1

--========================================================================================


--IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_calitemtaxamt')
--DROP FUNCTION fn_calitemtaxamt


/***************************************************************************/
/*********************** LAST UPDATE ON 14 OCTOBER 2015 ******************/
/***************************************************************************/


--ALTER TABLE BillHeader ADD
--numCurrencyID NUMERIC(18,0),
--fltExchangeRate FLOAT


/***************************************************************************/
/*********************** LAST UPDATE ON 09 OCTOBER 2015 ******************/
/***************************************************************************/

--ALTER TABLE BizDocTemplate ADD
--bitDisplayKitChild BIT

---------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------


--/****** Object:  Table [dbo].[OpportunityKitChildItems]    Script Date: 22-Sep-15 10:26:31 AM ******/
--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

--CREATE TABLE [dbo].[OpportunityKitChildItems](
--	[numOppKitChildItemID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
--	[numOppID] [numeric](18, 0) NOT NULL,
--	[numOppItemID] [numeric](18, 0) NOT NULL,
--	[numOppChildItemID] [numeric](18, 0) NOT NULL,
--	[numItemID] [numeric](18, 0) NOT NULL,
--	[numWareHouseItemId] [numeric](18, 0) NULL,
--	[numQtyItemsReq] [int] NULL,
--	[numQtyItemsReq_Orig] [int] NULL,
--	[numUOMId] [numeric](18, 0) NULL,
--	[numQtyShipped] [int] NULL,
-- CONSTRAINT [PK_OpportunityKitChildItems] PRIMARY KEY CLUSTERED 
--(
--	[numOppKitChildItemID] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
--) ON [PRIMARY]

--GO

--ALTER TABLE [dbo].[OpportunityKitChildItems]  WITH CHECK ADD  CONSTRAINT [FK_OpportunityKitChildItems_OpportunityKitItems] FOREIGN KEY([numOppChildItemID])
--REFERENCES [dbo].[OpportunityKitItems] ([numOppChildItemID])
--ON DELETE CASCADE
--GO

--ALTER TABLE [dbo].[OpportunityKitChildItems] CHECK CONSTRAINT [FK_OpportunityKitChildItems_OpportunityKitItems]
--GO

---------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------

--INSERT INTO PageNavigationDTl 
--(
--	numPageNavID,numModuleID,numParentID,vcPageNavName,vcNavURL,bitVisible,numTabID
--)
--VALUES
--(
--	(SELECT MAX(numPageNavID) + 1 FROM PageNavigationDTl),36,86,'Custom Report','../Service/frmCustomReports.aspx',1,-2
--)

---------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------

--GO

--/****** Object:  Table [dbo].[CustomQueryReport]    Script Date: 30-Sep-15 10:49:43 AM ******/
--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

--SET ANSI_PADDING ON
--GO

--CREATE TABLE [dbo].[CustomQueryReport](
--	[numReportID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
--	[numDomainID] [numeric](18, 0) NOT NULL,
--	[vcReportName] [varchar](300) NOT NULL,
--	[vcReportDescription] [varchar](300) NULL,
--	[vcEmailTo] [varchar](1000) NULL,
--	[tintEmailFrequency] [tinyint] NULL,
--	[vcQuery] [text] NOT NULL,
--	[vcCSS] [varchar](max) NULL,
--	[dtCreatedDate] [datetime] NULL,
-- CONSTRAINT [PK_CustomQueryReport] PRIMARY KEY CLUSTERED 
--(
--	[numReportID] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
--) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

--GO

--SET ANSI_PADDING ON
--GO

---------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------

--GO

--/****** Object:  Index [CQR_NCI_numReportID_numDomainID]    Script Date: 30-Sep-15 10:51:16 AM ******/
--CREATE NONCLUSTERED INDEX [CQR_NCI_numReportID_numDomainID] ON [dbo].[CustomQueryReport]
--(
--	[numReportID] ASC,
--	[numDomainID] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
--GO

---------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------

--GO

--/****** Object:  Table [dbo].[CustomQueryReportEmail]    Script Date: 30-Sep-15 10:49:49 AM ******/
--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

--CREATE TABLE [dbo].[CustomQueryReportEmail](
--	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
--	[numReportID] [numeric](18, 0) NOT NULL,
--	[tintEmailFrequency] [tinyint] NOT NULL,
--	[dtDateToSend] [date] NOT NULL,
--	[dtSentDate] [date] NOT NULL,
-- CONSTRAINT [PK_CustomQueryReportEmail] PRIMARY KEY CLUSTERED 
--(
--	[ID] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
--) ON [PRIMARY]

--GO

--ALTER TABLE [dbo].[CustomQueryReportEmail]  WITH CHECK ADD  CONSTRAINT [FK_CustomQueryReportEmail_CustomQueryReport] FOREIGN KEY([numReportID])
--REFERENCES [dbo].[CustomQueryReport] ([numReportID])
--ON DELETE CASCADE
--GO

--ALTER TABLE [dbo].[CustomQueryReportEmail] CHECK CONSTRAINT [FK_CustomQueryReportEmail_CustomQueryReport]
--GO

---------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------


--INSERT INTO ReportList 
--(
--	RptID,NumId,RptHeading,RptDesc,rptGroup,rptSequence,bitEnable
--)
--VALUES
--(
--	 0,0,'Custom Reports','',0,26,1
--)

---------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------

----ADD KEY IN BizService Config
--<add key="EmailCustomQueryReport" value="360" />


---------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------


--SET IDENTITY_INSERT AccountingChargeTypes ON 

--INSERT INTO AccountingChargeTypes
--(
--	numChargeTypeId,
--	vcChageType,
--	vcChargeAccountCode,
--	chChargeCode
--)
--VALUES
--(
--	26,
--	'Deferred Income',
--	'',
--	'DI'
--)

--SET IDENTITY_INSERT AccountingChargeTypes OFF


---------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------

--BEGIN TRANSACTION

--DECLARE @TMEP TABLE
--(
--	ID INT IDENTITY(1,1),
--	numDomainID NUMERIC(18,0)
--)

--INSERT INTO @TMEP SELECT numDomainID FROM Domain WHERE numDomainId > 0 ORDER BY numDomainId


--DECLARE @i AS INT = 1
--DECLARE @numDomainID NUMERIC(18,0)
--DECLARE @Count AS INT
--SELECT @Count=COUNT(*) FROM @TMEP

--DECLARE @dtTodayDate AS DATETIME
--SET @dtTodayDate = GETDATE()    

--WHILE @i <= @Count
--BEGIN
--	SELECT @numDomainID = numDomainID FROM @TMEP WHERE ID=@i

--	DECLARE @numLiabilityAccountTypeID  AS NUMERIC(9,0) = 0
--	SELECT @numLiabilityAccountTypeID = ISNULL(numAccountTypeId,0) FROM dbo.AccountTypeDetail WHERE vcAccountCode ='0102' AND numDomainID = @numDomainID 

--	DECLARE @numCurrentLiabilitiesAccountTypeID AS NUMERIC(9) = 0
--	SELECT @numCurrentLiabilitiesAccountTypeID = ISNULL(numAccountTypeID,0) FROM dbo.AccountTypeDetail WHERE vcAccountCode = '010201'  AND numDomainID = @numDomainID  


--	EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numLiabilityAccountTypeID,
--		@numParntAcntTypeID = @numCurrentLiabilitiesAccountTypeID, @vcAccountName = 'Deferred Income',
--		@vcAccountDescription = 'Deferred Income',
--		@monOriginalOpeningBal = $0, @monOpeningBal = $0,
--		@dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
--		@bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
--		@bitProfitLoss = 0, @bitDepreciation = 0,
--		@dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
--		@vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0

--	DECLARE @DI AS NUMERIC(18,0) = 0
--	SELECT @DI = numAccountId FROM dbo.Chart_Of_Accounts WHERE numDomainId = @numDomainId AND vcAccountName = 'Deferred Income'

--	PRINT @DI

--	IF ISNULL(@DI,0) > 0
--	BEGIN
--		INSERT INTO AccountingCharges
--		 (
--			[numChargeTypeId],
--			[numAccountID],
--			[numDomainID]
--		)
--		VALUES
--		(
--			26,
--			@DI,
--			@numDomainID
--		)
--	END

--	SET @i = @i + 1
--END


--COMMIT

---------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------

--SET IDENTITY_INSERT ListDetails ON
--INSERT INTO ListDetails
--( numListItemID,numListID,vcData,numCreatedBY,bitDelete,numDomainID,constFlag,sintOrder)
--VALUES
--(304,27,'Deferred Income',1,0,1,1,1)
--SET IDENTITY_INSERT ListDetails OFF

---------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------

--ALTER TABLE Domain ADD
--IsEnableDeferredIncome BIT

---------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------

--ALTER TABLE OpportunityBizDocs ADD
--numDeferredBizDocID NUMERIC(18,0)

/***************************************************************************/
/*********************** LAST UPDATE ON 04 SEPTEMBER 2015 ******************/
/***************************************************************************/

--ALTER TABLE WarehouseItmsDTL ADD 
--bitAddedFromPO BIT


/***************************************************************************/
/*********************** LAST UPDATE ON 29 JUlY 2015 ***********************/
/***************************************************************************/


--ALTER TABLE OpportunityBizDocs ADD
--bitFulFilled BIT

----------------------------------------------------------------------------------------------------------------------

--BEGIN TRANSACTION

--DECLARE @numOrderShipped AS NUMERIC(18,0) = 0
--DECLARE @numOrderReceived AS NUMERIC(18,0) = 0
--DECLARE @numQtyShippedReceived AS  NUMERIC(18,0) = 0


--INSERT INTO DycFieldMaster 
--(numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitDetailField,numListID)
--VALUES
--(
--3,'Ordered / Shipped','vcOrderedShipped','vcOrderedShipped','vcOrderedShipped','OpportunityMaster','V','R','Label',1,0,0,0,1,1,0
--)

--SELECT @numOrderShipped = SCOPE_IDENTITY()

--INSERT INTO DycFormField_Mapping 
--(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField)
--VALUES
--(3,@numOrderShipped,39,0,0,'Ordered / Shipped','Label','vcOrderedShipped',1,0,0,1,0,1)

--INSERT INTO DycFieldMaster 
--(numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitDetailField,numListID)
--VALUES
--(
--3,'Ordered / Received','vcOrderedReceived','vcOrderedReceived','vcOrderedReceived','OpportunityMaster','V','R','Label',1,0,0,0,1,1,0
--)

--SELECT @numOrderReceived = SCOPE_IDENTITY()

--INSERT INTO DycFormField_Mapping 
--(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField)
--VALUES
--(3,@numOrderReceived,41,0,0,'Ordered / Received','Label','vcOrderedReceived',1,0,0,1,0,1)

--INSERT INTO DycFieldMaster 
--(numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitDetailField,numListID)
--VALUES
--(
--3,'Shipped / Received','vcShippedReceived','vcShippedReceived','vcShippedReceived','OpportunityItems','V','R','Label',1,0,0,0,1,1,0
--)

--SELECT @numQtyShippedReceived = SCOPE_IDENTITY()

--INSERT INTO DycFormField_Mapping 
--(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField)
--VALUES
--(4,@numQtyShippedReceived,26,0,0,'Shipped / Received','Label','vcShippedReceived',1,0,0,1,0,1)

--DECLARE @FieldID AS INT = 0
--SELECT @FieldID = numFieldID FROM DycFormField_Mapping WHERE numFormID=26 AND vcFieldName = 'Received'

--UPDATE DycFormConfigurationDetails SET numFieldId=@numQtyShippedReceived WHERE numFormId=26 AND numFieldId = @FieldID
--DELETE FROM DycFormField_Mapping WHERE numFormID=26 AND numFieldId = @FieldID

--COMMIT

------------------------------------------------------------------------------------------------------------------------------

--ALTER TABLE WarehouseItmsDTL
--DROP COLUMN tintStatus

------------------------------------------------------------------------------------------------------------------------------

--SET IDENTITY_INSERT AccountingChargeTypes ON
--INSERT INTO AccountingChargeTypes 
--(numChargeTypeId,vcChageType,vcChargeAccountCode,chChargeCode)
--VALUES
--(25,'Sales Clearing','','SC')
--SET IDENTITY_INSERT AccountingChargeTypes OFF

---------------------------------------------------------------------------------------------------------------------------------

--BEGIN TRANSACTION

--DECLARE @TEMP TABLE
--(
--	ID INT IDENTITY(1,1),
--	DomainID INT
--)

--INSERT INTO @TEMP SELECT numDomainID FROM Domain WHERE numDomainID > 0

--DECLARE @i AS INT = 0
--DECLARE @COUNT AS INT
--DECLARE @numDomainID AS INT

--SELECT @COUNT=COUNT(*) FROM @TEMP

--WHILE @i <= @COUNT
--BEGIN
--	SELECT @numDomainID=DomainID FROM @TEMP WHERE ID=@i
--	PRINT 'DomainID:' + CAST(@numDomainID AS VARCHAR)

--	DECLARE @dtTodayDate AS DATETIME
--	SET @dtTodayDate = GETDATE()

--	DECLARE @numAssetAccountTypeID AS NUMERIC(9,0)
--	SELECT @numAssetAccountTypeID = numAccountTypeId FROM dbo.AccountTypeDetail WHERE vcAccountCode ='0101' AND numDomainID = @numDomainID 

--	DECLARE @numARAccountTypeID AS NUMERIC(9)
--	SELECT @numARAccountTypeID = numAccountTypeID FROM dbo.AccountTypeDetail WHERE vcAccountCode = '01010105'  AND numDomainID = @numDomainID  

--	EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numAssetAccountTypeID,
--    @numParntAcntTypeID = @numARAccountTypeID, @vcAccountName = 'Sales Clearing',
--    @vcAccountDescription = 'Sales Clearing.',
--    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
--    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
--    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
--    @bitProfitLoss = 0, @bitDepreciation = 0,
--    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
--    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0


--	DECLARE @SC AS VARCHAR(10)
--	SELECT @SC = numAccountId FROM dbo.Chart_Of_Accounts WHERE numDomainId = @numDomainId AND vcAccountName = 'Sales Clearing'


--	INSERT  INTO AccountingCharges
--    (
--        numChargeTypeId,
--        numAccountID,
--        numDomainID	
--    )
--    SELECT  
--		( 
--			SELECT    
--				numChargeTypeId
--            FROM      
--				AccountingChargeTypes
--            WHERE     
--				chChargeCode = 'SC'
--        ),
--        @SC,
--        @numDomainID

--	SET @i = @i + 1
--END

--ROLLBACK


/***************************************************************************/
/*********************** LAST UPDATE ON 29 JUNE 2015 ***********************/
/***************************************************************************/

--USE [Production.2014]
--GO

--/****** Object:  Index [PK_ItemExtendedDetails]    Script Date: 19-Jun-15 12:47:33 PM ******/
--ALTER TABLE [dbo].[ItemExtendedDetails] ADD  CONSTRAINT [PK_ItemExtendedDetails] PRIMARY KEY CLUSTERED 
--(
--	[numItemCode] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
--GO

--DECLARE @numBrandID NUMERIC(18,0)
--DECLARE @numItemType NUMERIC(18,0)

--INSERT INTO DycFieldMaster 
--(numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,
--vcFieldType,vcAssociatedControlType,vcToolTip,vcListItemType,numListID,[order],tintRow,tintColumn,bitInResults,bitDeleted,
--bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitImport,bitExport,bitAllowFiltering,
--bitInlineEdit,bitRequired,intColumnWidth,intFieldMaxLength)
--VALUES
--(
--	4,'Brand','vcBrand','vcBrand','Brand','Amazon','V','R','TextBox','Brand of the product','',0,1,1,2,0,0,1,1,0,0,1,0,0,1,0,0,0,0,0,250
--)

--SELECT @numBrandID = SCOPE_IDENTITY()

--INSERT INTO DycFieldMaster 
--(numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,
--vcFieldType,vcAssociatedControlType,vcToolTip,vcListItemType,numListID,[order],tintRow,tintColumn,bitInResults,bitDeleted,
--bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitImport,bitExport,bitAllowFiltering,
--bitInlineEdit,bitRequired,intColumnWidth,intFieldMaxLength)
--VALUES
--(
--	4,'ItemType','vcItemType','vcItemType','ItemType','Amazon','V','R','TextBox','Pre-defined value that specifies where the product should appear within the Amazon browse structure','',0,1,1,2,0,0,1,1,0,0,1,0,0,1,0,0,0,0,0,250
--)


--SELECT @numItemType = SCOPE_IDENTITY()

--INSERT INTO DycFormField_Mapping 
--(numModuleID,numFieldID,numFormID,bitDetailField,bitImport,bitAllowFiltering,intSectionID)
--VALUES
--(4,@numBrandID,50,1,1,1,2),
--(4,@numItemType,50,1,1,1,2)

/***************************************************************************/
/*********************** LAST UPDATE ON 12 JUNE 2015 ***********************/
/***************************************************************************/


--ALTER TABLE OpportunityMaster ADD
--numBillToAddressID NUMERIC(18,0) NULL,
--numShipToAddressID NUMERIC(18,0) NULL

---------------------------------------------------------------------------------------------------------------------------------

--ALTER TABLE BizDocTemplate ADD
--numRelationship NUMERIC(18,0) NULL,
--numProfile NUMERIC(18,0) NULL

----------------------------------------------------------------------------------------------------------------------------------

--INSERT INTO PageNavigationDTL 
--(numPageNavID,numModuleID,numParentID,vcPageNavName,vcNavURL,bitVisible,numTabID)
--VALUES
--(
--242,35,93,'Profit & Loss A/c - New','../Accounting/frmProfitLossReport.aspx',1,45
--)



--BEGIN TRANSACTION

--DECLARE @numPageNavID NUMERIC(18,0) = 242 

--DECLARE @TEMPDomain TABLE
--(
--	ID INT IDENTITY(1,1),
--	numDomainID NUMERIC(18,0)
--)

--INSERT INTO @TEMPDomain SELECT numDomainId FROM Domain WHERE numDomainId <> -255



--DECLARE @i AS INT = 1
--DECLARE @COUNT AS INT
--DECLARE @numDomainID AS NUMERIC(18,0)

--SELECT @COUNT=COUNT(*) FROM @TEMPDomain


--WHILE @i <= @COUNT
--BEGIN
--	SELECT @numDomainID=numDomainID FROM @TEMPDomain WHERE ID = @i

--	DELETE FROM TreeNavigationAuthorization WHERE numPageNavID=@numPageNavID AND numDomainID = @numDomainId

--	INSERT  INTO dbo.TreeNavigationAuthorization
--    (
--        numGroupID,
--        numTabID,
--        numPageNavID,
--        bitVisible,
--        numDomainID,
--        tintType
--    )
--    SELECT    
--		A.numGroupID,
--		45,
--		@numPageNavID,
--		1,
--		A.numDomainID,
--		1
--	FROM      
--		AuthenticationGroupMaster A
--	WHERE    
--		A.numDomainId = @numDomainID


--	SET @i = @i + 1
--END

--ROLLBACK


--------------------------------------------------------------------------------------------------------


--INSERT INTO PageNavigationDTL 
--(numPageNavID,numModuleID,numParentID,vcPageNavName,vcNavURL,bitVisible,numTabID)
--VALUES
--(
--243,35,93,'Balance Sheet - New','../Accounting/frmBalanceSheetReport.aspx',1,45
--)



--BEGIN TRANSACTION

--DECLARE @numPageNavID NUMERIC(18,0) = 243 

--DECLARE @TEMPDomain TABLE
--(
--	ID INT IDENTITY(1,1),
--	numDomainID NUMERIC(18,0)
--)

--INSERT INTO @TEMPDomain SELECT numDomainId FROM Domain WHERE numDomainId <> -255



--DECLARE @i AS INT = 1
--DECLARE @COUNT AS INT
--DECLARE @numDomainID AS NUMERIC(18,0)

--SELECT @COUNT=COUNT(*) FROM @TEMPDomain


--WHILE @i <= @COUNT
--BEGIN
--	SELECT @numDomainID=numDomainID FROM @TEMPDomain WHERE ID = @i

--	DELETE FROM TreeNavigationAuthorization WHERE numPageNavID=@numPageNavID AND numDomainID = @numDomainId

--	INSERT  INTO dbo.TreeNavigationAuthorization
--    (
--        numGroupID,
--        numTabID,
--        numPageNavID,
--        bitVisible,
--        numDomainID,
--        tintType
--    )
--    SELECT    
--		A.numGroupID,
--		45,
--		@numPageNavID,
--		1,
--		A.numDomainID,
--		1
--	FROM      
--		AuthenticationGroupMaster A
--	WHERE    
--		A.numDomainId = @numDomainID


--	SET @i = @i + 1
--END

--ROLLBACK


--------------------------------------------------------------------------------------------------------


--INSERT INTO PageNavigationDTL 
--(numPageNavID,numModuleID,numParentID,vcPageNavName,vcNavURL,bitVisible,numTabID)
--VALUES
--(
--244,35,93,'Trial Balance - New','../Accounting/frmTrialBalanceReport.aspx',1,45
--)



--BEGIN TRANSACTION

--DECLARE @numPageNavID NUMERIC(18,0) = 244

--DECLARE @TEMPDomain TABLE
--(
--	ID INT IDENTITY(1,1),
--	numDomainID NUMERIC(18,0)
--)

--INSERT INTO @TEMPDomain SELECT numDomainId FROM Domain WHERE numDomainId <> -255



--DECLARE @i AS INT = 1
--DECLARE @COUNT AS INT
--DECLARE @numDomainID AS NUMERIC(18,0)

--SELECT @COUNT=COUNT(*) FROM @TEMPDomain


--WHILE @i <= @COUNT
--BEGIN
--	SELECT @numDomainID=numDomainID FROM @TEMPDomain WHERE ID = @i

--	DELETE FROM TreeNavigationAuthorization WHERE numPageNavID=@numPageNavID AND numDomainID = @numDomainId

--	INSERT  INTO dbo.TreeNavigationAuthorization
--    (
--        numGroupID,
--        numTabID,
--        numPageNavID,
--        bitVisible,
--        numDomainID,
--        tintType
--    )
--    SELECT    
--		A.numGroupID,
--		45,
--		@numPageNavID,
--		1,
--		A.numDomainID,
--		1
--	FROM      
--		AuthenticationGroupMaster A
--	WHERE    
--		A.numDomainId = @numDomainID


--	SET @i = @i + 1
--END
--COMMIT
--ROLLBACK

---------------------------------------------------------------------------------------

--ALTER TABLE Domain ADD
--bitReOrderPoint BIT NULL,
--numReOrderPointOrderStatus NUMERIC(18,0) NULL

-----------------------------------------------------------------------------------------------

--INSERT INTO DycFormField_Mapping 
--(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],bitImport,bitAllowFiltering,intSectionID)
--VALUES
--(
--	4,668,55,0,0,'Lead Days','TextBox','LeadTimeDays',8,1,1,1
--)


--------------------------------------------------------------------------------------------

--ALTER TABLE [dbo].[AdditionalContactsInformation]
--ADD [vcImageName] VARCHAR(200)

-------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------

--Change <authentication mode="Windows" />

--to

--<authentication mode="Forms">
--    <forms loginUrl="Login.aspx" timeout="20" />
--</authentication>

--in web.config

--ADD DDL TO GAC
--==============

--iTextShart
--AspNetPager in BIZCART

--INSERT INTO DycFormField_Mapping 
--(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,
--bitDetailField,bitAllowSorting,bitAllowFiltering)
--VALUES
--(
--	1,18,43,0,0,'Follow-up Status','SelectBox','FollowUpStatus',26,1,0,0,1,0,0,1,1
--)

---------------------------------------------------------------------------------------------------

--INSERT INTO PageNavigationDTL 
--(numPageNavID,numModuleID,numParentID,vcPageNavName,vcNavURL,bitVisible,numTabID)
--VALUES
--(
--240,37,62,'Rental Items','../Items/frmItemList.aspx?Page=Rental Items&ItemGroup=0',1,80
--)

--BEGIN TRANSACTION

--DECLARE @numPageNavID NUMERIC(18,0) = 240 

--DECLARE @TEMPDomain TABLE
--(
--	ID INT IDENTITY(1,1),
--	numDomainID NUMERIC(18,0)
--)

--INSERT INTO @TEMPDomain SELECT numDomainId FROM Domain WHERE numDomainId <> -255

--DECLARE @i AS INT = 1
--DECLARE @COUNT AS INT
--DECLARE @numDomainID AS NUMERIC(18,0)

--SELECT @COUNT=COUNT(*) FROM @TEMPDomain


--WHILE @i <= @COUNT
--BEGIN
--	SELECT @numDomainID=numDomainID FROM @TEMPDomain WHERE ID = @i

--	DELETE FROM TreeNavigationAuthorization WHERE numPageNavID=@numPageNavID AND numDomainID = @numDomainId

--	INSERT  INTO dbo.TreeNavigationAuthorization
--    (
--        numGroupID,
--        numTabID,
--        numPageNavID,
--        bitVisible,
--        numDomainID,
--        tintType
--    )
--    SELECT    
--		A.numGroupID,
--		80,
--		@numPageNavID,
--		1,
--		A.numDomainID,
--		1
--	FROM      
--		AuthenticationGroupMaster A
--	WHERE    
--		A.numDomainId = @numDomainID

--	SET @i = @i + 1
--END

--ROLLBACK

-------------------------------------------------------------------------------------------------------------

--UPDATE PageNavigationDTL SET intSortOrder=10000 WHERE numPageNavID=173

-------------------------------------------------------------------------------------------------------------

--INSERT INTO DynamicFormMaster
--(numFormId,vcFormName,cCustomFieldsAssociated,cAOIAssociated,bitDeleted)
--VALUES
--(126,'Inventory Adjustment Grid Columns','Y','N',0)


--INSERT INTO DycFormField_Mapping
--(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,
--vcPropertyName,PopupFunctionName,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,
--bitDetailField,bitAllowSorting,bitImport,bitExport,bitAllowFiltering,numFormFieldID)
--SELECT
--	numModuleID,numFieldID,126,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,
--	vcPropertyName,PopupFunctionName,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,
--	bitDetailField,bitAllowSorting,bitImport,bitExport,bitAllowFiltering,numFormFieldID
--FROM
--	DycFormField_Mapping
--WHERE
--	numFormID = 21
--	AND numFieldID NOT IN (200,198,197,196,195,210,469,351,295,199,211,189,192)

--UPDATE DycFormField_Mapping SET bitDefault = 0 WHERE ISNULL(numFormID,0)=126

---------------------------------------------------------------------------------------------

--UPDATE DycFormField_Mapping SET vcAssociatedControlType='TextBox' WHERE numFieldID=419 AND numFormID=39
--UPDATE DycFormField_Mapping SET vcAssociatedControlType='TextBox' WHERE numFieldID=419 AND numFormID=41

---------------------------------------------------------------------------------------------

--INSERT INTO DycFormField_Mapping 
--(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,bitInResults,bitDeleted,bitDefault,bitAllowFiltering,intSectionID)
--VALUES
--(4,236,29,1,1,'UPC (M)','TextBox',1,0,0,1,1),
--(4,237,29,1,1,'SKU (M)','TextBox',1,0,0,1,1)

----------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------

--/****** Object:  Table [dbo].[CommunicationLinkedOrganization]    Script Date: 02-Mar-15 5:13:26 PM ******/
--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

--CREATE TABLE [dbo].[CommunicationLinkedOrganization](
--	[numCLOID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
--	[numCommID] [numeric](18, 0) NOT NULL,
--	[numDivisionID] [numeric](18, 0) NULL,
--	[numContactID] [numeric](18, 0) NULL,
-- CONSTRAINT [PK_CommunicationLinkedOrganization] PRIMARY KEY CLUSTERED 
--(
--	[numCLOID] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
--) ON [PRIMARY]

--GO

--ALTER TABLE [dbo].[CommunicationLinkedOrganization]  WITH CHECK ADD  CONSTRAINT [FK_CommunicationLinkedOrganization_Communication] FOREIGN KEY([numCommID])
--REFERENCES [dbo].[Communication] ([numCommId])
--ON DELETE CASCADE
--GO

--ALTER TABLE [dbo].[CommunicationLinkedOrganization] CHECK CONSTRAINT [FK_CommunicationLinkedOrganization_Communication]
--GO



-----------------------------------------------------


--ALTER TABLE CommissionRules ADD
--tintComOrgType TINYINT

--GO
--UPDATE CommissionRules SET tintComOrgType=3 


--ALTER TABLE BizDocComission ADD
--numComPayPeriodID NUMERIC(18,0),
--bitFullPaidBiz BIT

--ALTER TABLE BizDocComission 
--ALTER COLUMN numComissionAmount decimal(18,4)


-----------------------------------------------------

--GO

--/****** Object:  Table [dbo].[CommissionRuleOrganization]    Script Date: 03-Mar-15 7:20:34 PM ******/
--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

--CREATE TABLE [dbo].[CommissionRuleOrganization](
--	[numComRuleOrgID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
--	[numComRuleID] [numeric](18, 0) NOT NULL,
--	[numValue] [numeric](18, 0) NULL,
--	[numProfile] [numeric](18, 0) NULL,
--	[tintType] [tinyint] NOT NULL,
-- CONSTRAINT [PK_CommissionRuleOrganization] PRIMARY KEY CLUSTERED 
--(
--	[numComRuleOrgID] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
--) ON [PRIMARY]

--GO

--ALTER TABLE [dbo].[CommissionRuleOrganization]  WITH CHECK ADD  CONSTRAINT [FK_CommissionRuleOrganization_CommissionRules] FOREIGN KEY([numComRuleID])
--REFERENCES [dbo].[CommissionRules] ([numComRuleID])
--ON DELETE CASCADE
--GO

--ALTER TABLE [dbo].[CommissionRuleOrganization] CHECK CONSTRAINT [FK_CommissionRuleOrganization_CommissionRules]
--GO




--GO

--/****** Object:  Table [dbo].[CommissionPayPeriod]    Script Date: 12-Mar-15 3:49:46 PM ******/
--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

--CREATE TABLE [dbo].[CommissionPayPeriod](
--	[numComPayPeriodID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
--	[numDomainID] [numeric](18, 0) NOT NULL,
--	[dtStart] [date] NOT NULL,
--	[dtEnd] [date] NOT NULL,
--	[tintPayPeriod] [int] NOT NULL,
--	[numCreatedBy] [numeric](18, 0) NOT NULL,
--	[dtCreated] [datetime] NOT NULL,
--	[numModifiedBy] [numeric](18, 0) NULL,
--	[dtModified] [datetime] NULL,
-- CONSTRAINT [PK_CommissionPayPeriod] PRIMARY KEY CLUSTERED 
--(
--	[numComPayPeriodID] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
--) ON [PRIMARY]

--GO

--ALTER TABLE [dbo].[CommissionPayPeriod]  WITH CHECK ADD  CONSTRAINT [FK_CommissionPayPeriod_Domain] FOREIGN KEY([numDomainID])
--REFERENCES [dbo].[Domain] ([numDomainId])
--ON DELETE CASCADE
--GO

--ALTER TABLE [dbo].[CommissionPayPeriod] CHECK CONSTRAINT [FK_CommissionPayPeriod_Domain]
--GO


---------------------------------------------------------------------------------------------------

--INSERT INTO ReportFieldGroupMappingMaster 
--(
--	numReportFieldGroupID,
--	numFieldID,
--	vcFieldName,
--	vcAssociatedControlType,
--	vcFieldDataType,
--	bitAllowSorting,
--	bitAllowGrouping,
--	bitAllowAggregate,
--	bitAllowFiltering
--)
--VALUES
--(
--	7,
--	320,
--	'Serial No',
--	'TextBox',
--	'V',
--	0,
--	0,
--	0,
--	0
--)


--INSERT INTO ReportFieldGroupMappingMaster 
--(
--	numReportFieldGroupID,
--	numFieldID,
--	vcFieldName,
--	vcAssociatedControlType,
--	vcFieldDataType,
--	bitAllowSorting,
--	bitAllowGrouping,
--	bitAllowAggregate,
--	bitAllowFiltering
--)
--VALUES
--(
--	11,
--	320,
--	'Serial No',
--	'TextBox',
--	'V',
--	0,
--	0,
--	0,
--	0
--)

--ALTER TABLE AdvSerViewConf ADD
--bitCustom BIT,
--intColumnWidth INT


---------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------

--INSERT INTO DycFormField_Mapping 
--(numModuleID,numFieldID,numFormID,vcFieldName,bitAllowEdit,bitInlineEdit,vcAssociatedControlType,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering)
--VALUES
--(4,280,21,'Base UOM',1,1,'SelectBox',0,0,1,1,1),
--(4,276,21,'Sale UOM',1,1,'SelectBox',0,0,1,1,1),
--(4,275,21,'Purchase UOM',1,1,'SelectBox',0,0,1,1,1)

--ALTER TABLE OpportunityMaster ADD
--bitStopMerge BIT

--ALTER TABLE tblActionItemData ADD
--bitRemind BIT,
--numRemindBeforeMinutes INT

--ALTER TABLE Communication ADD
--LastSnoozDateTimeUtc DATETIME

--INSERT INTO PageMaster VALUES (34,10,'frmDealList.aspx','Merge Purchase Orders',1,0,0,0,0,NULL,NULL)

--UPDATE ShortCutBar SET NewLink='../Admin/actionitemdetails.aspx' WHERE id=163

---------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------

--ALTER TABLE dbo.SalesOrderConfiguration ADD
--bitDisplayCreateInvoice BIT NULL 

--UPDATE SalesOrderConfiguration SET bitDisplayCreateInvoice = 1

--INSERT INTO DycFormField_Mapping
--(
--	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,[order],bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
--)
--VALUES
--(	
--	4,274,21,1,1,'Item Group','SelectBox',17,1,0,1,1,1,1
--)

--INSERT INTO ListDetails 
--(numListID,vcData,numCreatedBY,bitDelete,numDomainID,constFlag,sintOrder)
--VALUES
--(302,'Not Ready to Build',1,1,1,1,0)

--INSERT INTO ListDetails 
--(numListID,vcData,numCreatedBY,bitDelete,numDomainID,constFlag,sintOrder)
--VALUES
--(302,'Ready to Build',1,1,1,1,0)


--ALTER TABLE WorkOrderDetails ADD
--numPOID NUMERIC(18,0)

--ALTER TABLE WorkOrder ADD
--numParentWOID NUMERIC(18,0)

--ALTER TABLE OpportunityMaster ADD
--bitFromWorkOrder BIT




--------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------


----TODO: Add EmailTracking and AmzonSESMaxPerSec in Web.Config of BizService
--<add key="AmazonSESMaxPerSec" value="5"></add>
--<add key="EmailTracking" value="http://portal.bizautomation.com/common/EmailTracking.ashx" />

----TODO: Change ECampaignWakeupTime from 1440 to 30 in Web.Config of BizService
--<!--Value in Minits - Every Half Hour -->
--<add key="ECampaignWakeupTime" value="30"/>


--------------------------------------------------------------------------------------------------------

--ALTER TABLE [dbo].[ECampaignDTLs] 
--ADD tintWaitPeriod TINYINT

--------------------------------------------------------------------------------------------------------

--ALTER TABLE ConECampaignDTL ADD
--bitEmailRead BIT,
--dtExecutionDate DATETIME

--------------------------------------------------------------------------------------------------------

--ALTER TABLE ECampaign
--ADD bitDeleted BIT

--------------------------------------------------------------------------------------------------------

--GO

--/****** Object:  Table [dbo].[ConECampaignDTLLinks]    Script Date: 13-Dec-14 9:56:49 AM ******/
--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

--SET ANSI_PADDING ON
--GO

--CREATE TABLE [dbo].[ConECampaignDTLLinks](
--	[numLinkID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
--	[numConECampaignDTLID] [numeric](18, 0) NOT NULL,
--	[vcOriginalLink] [varchar](1000) NULL,
--	[bitClicked] [bit] NULL,
-- CONSTRAINT [PK_ConECampaignDTLLinks] PRIMARY KEY CLUSTERED 
--(
--	[numLinkID] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
--) ON [PRIMARY]

--GO

--SET ANSI_PADDING ON
--GO

--------------------------------------------------------------------------------------------------------

--BEGIN TRANSACTION

--DECLARE @numFieldID NUMERIC(18,0)

--INSERT INTO DycFieldMaster
--(
--	numModuleID,
--	vcFieldName,
--	vcDbColumnName,
--	vcOrigDbColumnName,
--	vcPropertyName,
--	vcLookBackTableName,
--	vcFieldDataType,
--	vcFieldType,
--	vcAssociatedControlType,
--	PopupFunctionName,
--	numListID,
--	bitInResults,
--	bitDeleted,
--	bitAllowEdit,
--	bitDefault,
--	bitSettingField,
--	bitDetailField
--)
--VALUES
--(
--	2,
--	'Next Drip / Audit',
--	'vcCampaignAudit',
--	'vcCampaignAudit',
--	'vcCampaignAudit',
--	'',
--	'V',
--	'R',
--	'Popup',
--	'openDrip',
--	0,
--	1,
--	0,
--	0,
--	0,
--	1,
--	0
--)

--SET @numFieldID = SCOPE_IDENTITY()

--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (2,@numFieldID,Null,10,0,0,'Next Drip / Audit','Popup','','openDrip',1,1,2,Null,Null,Null,1,0,0,0,0,0,0,0,Null,Null,Null,0)

--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (2,@numFieldID,Null,34,0,0,'Next Drip / Audit','Popup','','openDrip',1,1,2,Null,Null,Null,1,0,0,0,0,0,0,0,Null,Null,Null,0)

--COMMIT


--------------------------------------------------------------------------------------------------------


----Replace following keys in appsettings in BACRMUI and BIZSERVICE
--<add key="ImapLicense" value="MN800-C901FED5013E01A501FB53A6147F-B4F5" />
--<add key="SMTPLicense" value="MN800-C901FED5013E01A501FB53A6147F-B4F5" />

--/******************* ORDER CHANGES ******************/

--ALTER TABLE dbo.SalesOrderConfiguration ADD
--bitDisplaySaveNew BIT NULL DEFAULT(1),
--bitDisplayAddToCart BIT NULL DEFAULT(1)

--/******************* EMAIL CHANGES ******************/

----------------------------------------------------------------

--ALTER TABLE InboxTreeSort ADD
--numFixID NUMERIC(18,0) NULL,
--bitSystem BIT NULL

----------------------------------------------------------------

--ALTER TABLE EmailHistory ADD
--bitInvisible BIT NULL,
--numOldNodeID NUMERIC(18,0) NULL,
--bitArchived BIT NULL

-----------------------------------------------------------------

--ALTER TABLE AlertConfig
--ADD bitCampaign BIT NULL

-----------------------------------------------------------------

--UPDATE InboxTreeSort SET bitSystem=1 WHERE (numParentID IS NULL AND vcNodeName IS NULL) OR vcNodeName IN ('Inbox','Deleted Mails','Sent Mails','Calendar','Folders','Settings','Spam')

----------------------------------------------------------------

--UPDATE InboxTreeSort SET numFixID = 1 WHERE vcNodeName = 'Inbox'
--UPDATE InboxTreeSort SET numFixID = 2 WHERE vcNodeName = 'Deleted Mails'
--UPDATE InboxTreeSort SET numFixID = 4 WHERE vcNodeName = 'Sent Mails'
--UPDATE InboxTreeSort SET numFixID = 5 WHERE vcNodeName = 'Calendar'
--UPDATE InboxTreeSort SET numFixID = 6 WHERE vcNodeName = 'Folders'
--UPDATE InboxTreeSort SET numFixID = 190 WHERE vcNodeName = 'Settings'
--UPDATE InboxTreeSort SET numFixID = 191 WHERE vcNodeName = 'Spam'

----------------------------------------------------------------

--/** Change Label Sent Mails To Sent Messages **/
--UPDATE InboxTree SET vcName = 'Sent Messages' WHERE vcName = 'Sent Mails'
--UPDATE InboxTreeSort SET vcNodeName = 'Sent Messages' WHERE vcNodeName = 'Sent Mails'

----------------------------------------------------------------

--/** Change Label Sent Mails To Sent Messages **/
--UPDATE InboxTree SET vcName = 'Email Archive' WHERE vcName = 'Deleted Mails'
--UPDATE InboxTreeSort SET vcNodeName = 'Email Archive' WHERE vcNodeName = 'Deleted Mails'

-----------------------------------------------------------------

--/** Change Label Folders To Custom Folders **/
--UPDATE InboxTree SET vcName = 'Custom Folders' WHERE vcName = 'Folders'
--UPDATE InboxTreeSort SET vcNodeName = 'Custom Folders' WHERE vcNodeName = 'Folders'

-----------------------------------------------------------------

--/** Remove Settings Node - Not required now **/
--DELETE FROM InboxTreeSort WHERE numFixID = 190
--DELETE FROM InboxTree WHERE numNodeID = 190

------------------------------------------------------------------

--/** Remove Spam Node - Not required now **/
--DELETE FROM InboxTreeSort WHERE numFixID = 191
--DELETE FROM InboxTree WHERE numNodeID = 191

-------------------------------------------------------------------

----1.Deleted Mails
--UPDATE t1
--	SET numNodeId = ISNULL((SELECT numNodeId FROM InboxTreeSort WHERE numDomainID = t1.numDomainID AND numUserCntID = t1.numUserCntID AND ISNULL(numFixID,0) = 2),0)
--FROM
--	EmailHistory t1 
--WHERE 
--	t1.numNodeId = 2 AND
--	t1.numNodeId <> (SELECT numNodeId FROM InboxTreeSort WHERE numDomainID = t1.numDomainID AND numUserCntID = t1.numUserCntID AND ISNULL(numFixID,0) = 2)

----2.Sent Mails
--UPDATE t1
--	SET numNodeId = ISNULL((SELECT numNodeId FROM InboxTreeSort WHERE numDomainID = t1.numDomainID AND numUserCntID = t1.numUserCntID AND ISNULL(numFixID,0) = 4),0)
--FROM
--	EmailHistory t1 
--WHERE 
--	t1.numNodeId = 4 AND
--	t1.numNodeId <> (SELECT numNodeId FROM InboxTreeSort WHERE numDomainID = t1.numDomainID AND numUserCntID = t1.numUserCntID AND ISNULL(numFixID,0) = 4)

----3.Inbox Mails
--UPDATE t1
--	SET numNodeId = ISNULL((SELECT numNodeId FROM InboxTreeSort WHERE numDomainID = t1.numDomainID AND numUserCntID = t1.numUserCntID AND ISNULL(numFixID,0) = 1),0)
--FROM
--	EmailHistory t1 
--WHERE 
--	t1.numNodeId = 0 AND
--	numNodeId <> (SELECT numNodeId FROM InboxTreeSort WHERE numDomainID = t1.numDomainID AND numUserCntID = t1.numUserCntID AND ISNULL(numFixID,0) = 1)


------------------------------------------------------------------------------------------------------------------

--/****** Object:  Index [Division_Domain]    Script Date: 29-Dec-14 5:24:40 PM ******/
--CREATE NONCLUSTERED INDEX [Division_Domain] ON [dbo].[ProjectsMaster]
--(
--	[numDivisionId] ASC,
--	[numDomainId] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
--GO

-------------------------------------------------------------------------------------------------------------------

--/****** Object:  Index [Division_Domain]    Script Date: 29-Dec-14 5:25:47 PM ******/
--CREATE NONCLUSTERED INDEX [Division_Domain] ON [dbo].[Communication]
--(
--	[numDivisionId] ASC,
--	[numDomainID] ASC,
--	[bitClosedFlag] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
--GO

-------------------------------------------------------------------------------------------------------------------

--/****** Object:  Index [Domain_Division]    Script Date: 29-Dec-14 5:26:36 PM ******/
--CREATE NONCLUSTERED INDEX [Domain_Division] ON [dbo].[OpportunityMaster]
--(
--	[numDivisionId] ASC,
--	[numDomainId] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
--GO

-------------------------------------------------------------------------------------------------------------------

--/****** Object:  Index [Domain_Division_OppType_OppStatus]    Script Date: 29-Dec-14 5:27:00 PM ******/
--CREATE NONCLUSTERED INDEX [Domain_Division_OppType_OppStatus] ON [dbo].[OpportunityMaster]
--(
--	[tintOppType] ASC,
--	[numDivisionId] ASC,
--	[numDomainId] ASC,
--	[tintOppStatus] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
--GO



----------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------


--<add key="AmazonSESMaxPerSec" value="5"></add> --IN BACRMUI

----OLD LINK WAS ../Marketing/frmEmailBroadcastConfigDetail.aspx

--UPDATE PageNavigationDTL SET vcNavURL='../Marketing/frmEmailBroadcastSetting.aspx' WHERE numPageNavID=218
--UPDATE PageNavigationDTL SET vcNavURL='../Marketing/frmEmailBroadcastSetting.aspx' WHERE numPageNavID=17

----TODO: First change name of existing EmailBroadcastConfiguration table to EmailBroadcastConfiguration_OLD also change PK name

--/****** Object:  Table [dbo].[EmailBroadcastConfiguration]    Script Date: 18-Nov-14 12:44:29 PM ******/
--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

--CREATE TABLE [dbo].[EmailBroadcastConfiguration](
--	[numConfigurationId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
--	[numDomainID] [numeric](18, 0) NOT NULL,
--	[vcFrom] [varchar](100) NOT NULL,
--	[vcAWSDomain] [varchar](100) NOT NULL,
--	[vcAWSAccessKey] [varchar](100) NOT NULL,
--	[vcAWSSecretKey] [varchar](100) NOT NULL,	
-- CONSTRAINT [PK_EmailBroadcastConfiguration] PRIMARY KEY CLUSTERED 
--(
--	[numConfigurationId] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
--) ON [PRIMARY]


--/****** Object:  Table [dbo].[BroadcastErrorLog]    Script Date: 20-Nov-14 12:07:04 PM ******/
--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

--CREATE TABLE [dbo].[BroadcastErrorLog](
--	[numBroadcastErrorID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
--	[numBroadcastDtlID] [numeric](18, 0) NOT NULL,
--	[vcType] [nvarchar](300) NULL,
--	[vcMessage] [nvarchar](max) NULL,
--	[vcStackStrace] [nvarchar](max) NULL,
-- CONSTRAINT [PK_BroadcastErrorLog] PRIMARY KEY CLUSTERED 
--(
--	[numBroadcastErrorID] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
--) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

--GO

--ALTER TABLE [dbo].[BroadcastErrorLog]  WITH CHECK ADD  CONSTRAINT [FK_BroadcastErrorLog_BroadCastDtls] FOREIGN KEY([numBroadcastDtlID])
--REFERENCES [dbo].[BroadCastDtls] ([numBroadCastDtlId])
--ON DELETE CASCADE
--GO

--ALTER TABLE [dbo].[BroadcastErrorLog] CHECK CONSTRAINT [FK_BroadcastErrorLog_BroadCastDtls]
--GO




--ALTER TABLE Sites ADD
--bitHtml5 BIT,
--vcMetaTags text,
--bitSSLRedirect BIT

----OLD VALUE OF vcFieldName='Serial #s'
--UPDATE DycFormField_Mapping SET vcFieldName='Serial/Lot#s' WHERE numFormID IN (7,8) AND numFieldID=314


--BEGIN TRANSACTION

--DECLARE @numFieldID NUMERIC(18,0)

--INSERT INTO DycFieldMaster
--(
--	numModuleID,
--	vcFieldName,
--	vcDbColumnName,
--	vcOrigDbColumnName,
--	vcPropertyName,
--	vcLookBackTableName,
--	vcFieldDataType,
--	vcFieldType,
--	vcAssociatedControlType,
--	numListID,
--	bitInResults,
--	bitDeleted,
--	bitAllowEdit,
--	bitDefault,
--	bitSettingField,
--	bitDetailField
--)
--VALUES
--(
--	3,
--	'Unit Sale Price',
--	'monUnitSalePrice',
--	'monUnitSalePrice',
--	'monUnitSalePrice',
--	'',
--	'M',
--	'R',
--	'Label',
--	0,
--	1,
--	0,
--	0,
--	0,
--	0,
--	0
--)


--SET @numFieldID = SCOPE_IDENTITY()

--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (3,@numFieldID,Null,7,0,0,'Unit Sale Price','Label','',Null,1,1,2,Null,Null,Null,0,0,0,0,0,0,0,0,Null,Null,Null,0)

--ROLLBACK


-------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------

---------------------------
---BizService App.Config---
---------------------------
--<add key="BizRecurrence" value="1440"/>


--UPDATE
--	ShortCutBar
--SET
--	NewLink = '../opportunity/frmNewOrder.aspx?OppStatus=0&OppType=1'
--WHERE
--	Id=113

--UPDATE
--	ShortCutBar
--SET
--	NewLink = '../opportunity/frmNewOrder.aspx?OppStatus=1&OppType=1'
--WHERE
--	Id=115

-- OLD Links Do Not Run on Server
--UPDATE
--	ShortCutBar
--SET
--	NewLink = '../opportunity/frmAddOpportunity.aspx?OppType=1'
--WHERE
--	Id=113

--UPDATE
--	ShortCutBar
--SET
--	NewLink = '../opportunity/frmNewOrder.aspx'
--WHERE
--	Id=115


--ALTER TABLE OpportunityMaster ADD
--vcRecurrenceType varchar(20),
--bitRecurred BIT

--ALTER TABLE OpportunityItems ADD
--numRecurParentOppItemID	 NUMERIC(18,0)

--ALTER TABLE OpportunityBizDocs ADD
--bitRecurred BIT

--BEGIN TRANSACTION

--DECLARE @numFieldID NUMERIC(18,0)

--INSERT INTO DycFieldMaster
--(
--	numModuleID,
--	vcFieldName,
--	vcDbColumnName,
--	vcOrigDbColumnName,
--	vcPropertyName,
--	vcLookBackTableName,
--	vcFieldDataType,
--	vcFieldType,
--	vcAssociatedControlType,
--	numListID,
--	bitInResults,
--	bitDeleted,
--	bitAllowEdit,
--	bitDefault,
--	bitSettingField,
--	bitDetailField,
--	bitWorkFlowField,
--	vcGroup
--)
--VALUES
--(
--	3,
--	'Recurrence Type',
--	'vcRecurrenceType',
--	'vcRecurrenceType',
--	'RecurrenceType',
--	'OpportunityMaster',
--	'V',
--	'R',
--	'TextBox',
--	0,
--	1,
--	0,
--	0,
--	0,
--	1,
--	1,
--	1,
--	'Order Fields'
--)

--SET @numFieldID = SCOPE_IDENTITY()
--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (3,@numFieldID,Null,70,0,0,'Recurrence Type','TextBox','RecurrenceType',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)
--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)

--VALUES        (3,@numFieldID,Null,39,0,0,'Recurrence Type','Label','RecurrenceType',NULL,NULL,NULL,NULL,1,0,0,1,1,0,1,0,0,0,0,0,NULL,NULL,0)


--DECLARE @numFieldRecurredOpp NUMERIC(18,0)
--INSERT INTO DycFieldMaster
--(
--	numModuleID,
--	vcFieldName,
--	vcDbColumnName,
--	vcOrigDbColumnName,
--	vcPropertyName,
--	vcLookBackTableName,
--	vcFieldDataType,
--	vcFieldType,
--	vcAssociatedControlType,
--	numListID,
--	bitInResults,
--	bitDeleted,
--	bitAllowEdit,
--	bitDefault,
--	bitSettingField,
--	bitDetailField,
--	bitWorkFlowField,
--	vcGroup
--)
--VALUES
--(
--	3,
--	'Recurred Order',
--	'bitRecurred',
--	'bitRecurred',
--	'bitRecurred',
--	'OpportunityMaster',
--	'Y',
--	'R',
--	'CheckBox',
--	0,
--	1,
--	0,
--	0,
--	0,
--	1,
--	1,
--	1,
--	'Order Fields'
--)
--SET @numFieldRecurredOpp = SCOPE_IDENTITY()

--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (3,@numFieldRecurredOpp,Null,70,0,0,'Recurred Order','CheckBox','bitRecurred',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)

--DECLARE @numFieldRecurredBizDoc NUMERIC(18,0)
--INSERT INTO DycFieldMaster
--(
--	numModuleID,
--	vcFieldName,
--	vcDbColumnName,
--	vcOrigDbColumnName,
--	vcPropertyName,
--	vcLookBackTableName,
--	vcFieldDataType,
--	vcFieldType,
--	vcAssociatedControlType,
--	numListID,
--	bitInResults,
--	bitDeleted,
--	bitAllowEdit,
--	bitDefault,
--	bitSettingField,
--	bitDetailField,
--	bitWorkFlowField,
--	vcGroup
--)
--VALUES
--(
--	3,
--	'Recurred BizDoc',
--	'bitRecurred',
--	'bitRecurred',
--	'bitRecurred',
--	'OpportunityBizDocs',
--	'Y',
--	'R',
--	'CheckBox',
--	0,
--	1,
--	0,
--	0,
--	0,
--	1,
--	1,
--	1,
--	'BizDoc Fields'
--)
--SET @numFieldRecurredBizDoc = SCOPE_IDENTITY()

--INSERT INTO DycFormField_Mapping
--                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
--                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
--                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
--VALUES        (3,@numFieldRecurredBizDoc,Null,49,0,0,'Recurred BizDoc','CheckBox','bitRecurred',Null,1,1,2,Null,Null,Null,0,0,0,0,1,0,0,1,Null,Null,Null,0)


--ROLLBACK


--GO

--/****** Object:  Table [dbo].[RecurrenceConfiguration]    Script Date: 16-Oct-14 5:16:54 PM ******/
--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

--SET ANSI_PADDING ON
--GO

--CREATE TABLE [dbo].[RecurrenceConfiguration](
--	[numRecConfigID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
--	[numDomainID] [numeric](18, 0) NOT NULL,
--	[numOppID] [numeric](18, 0) NULL,
--	[numOppBizDocID] [numeric](18, 0) NULL,
--	[dtStartDate] [date] NOT NULL,
--	[dtEndDate] [date] NULL,
--	[numType] [smallint] NOT NULL,
--	[vcType] [varchar](50) NOT NULL,
--	[numFrequency] [smallint] NOT NULL,
--	[vcFrequency] [varchar](50) NOT NULL,
--	[dtNextRecurrenceDate] [date] NULL,
--	[bitCompleted] [bit] NULL,
--	[numCreatedBy] [numeric](18, 0) NOT NULL,
--	[dtCreatedDate] [datetime] NOT NULL,
--	[bitDisabled] [bit] NULL,
--	[dtDisabledDate] [date] NULL,
--	[numDisabledBy] [numeric](18, 0) NULL,
--	[numTransaction] [int] NULL,
--	[bitMarkForDelete] [bit] NULL,
-- CONSTRAINT [PK_RecurrenceConfiguration] PRIMARY KEY CLUSTERED 
--(
--	[numRecConfigID] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
--) ON [PRIMARY]

--GO

--SET ANSI_PADDING ON
--GO

--GO

--/****** Object:  Table [dbo].[RecurrenceErrorLog]    Script Date: 16-Oct-14 5:17:20 PM ******/
--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

--CREATE TABLE [dbo].[RecurrenceErrorLog](
--	[numErrorID] [int] IDENTITY(1,1) NOT NULL,
--	[numDomainID] [numeric](18, 0) NOT NULL,
--	[numRecConfigID] [numeric](18, 0) NOT NULL,
--	[vcType] [nvarchar](100) NULL,
--	[vcSource] [nvarchar](100) NULL,
--	[vcMessage] [nvarchar](max) NULL,
--	[vcStackStrace] [nvarchar](max) NULL,
--	[CreatedDate] [datetime] NULL,
-- CONSTRAINT [PK_RecurrenceErrorLog] PRIMARY KEY CLUSTERED 
--(
--	[numErrorID] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
--) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

--GO

--/****** Object:  Table [dbo].[RecurrenceTransaction]    Script Date: 16-Oct-14 5:17:55 PM ******/
--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

--CREATE TABLE [dbo].[RecurrenceTransaction](
--	[numRecTranID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
--	[numRecConfigID] [numeric](18, 0) NOT NULL,
--	[numRecurrOppID] [numeric](18, 0) NULL,
--	[numRecurrOppBizDocID] [numeric](18, 0) NULL,
--	[dtCreatedDate] [datetime] NOT NULL,
--	[numErrorID] [int] NULL,
-- CONSTRAINT [PK_RecurrenceTransaction] PRIMARY KEY CLUSTERED 
--(
--	[numRecTranID] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
--) ON [PRIMARY]

--GO

--ALTER TABLE [dbo].[RecurrenceTransaction]  WITH CHECK ADD  CONSTRAINT [FK_RecurrenceTransaction_OpportunityBizDocs] FOREIGN KEY([numRecurrOppBizDocID])
--REFERENCES [dbo].[OpportunityBizDocs] ([numOppBizDocsId])
--ON DELETE CASCADE
--GO

--ALTER TABLE [dbo].[RecurrenceTransaction] CHECK CONSTRAINT [FK_RecurrenceTransaction_OpportunityBizDocs]
--GO

--ALTER TABLE [dbo].[RecurrenceTransaction]  WITH CHECK ADD  CONSTRAINT [FK_RecurrenceTransaction_OpportunityMaster] FOREIGN KEY([numRecurrOppID])
--REFERENCES [dbo].[OpportunityMaster] ([numOppId])
--ON DELETE CASCADE
--GO

--ALTER TABLE [dbo].[RecurrenceTransaction] CHECK CONSTRAINT [FK_RecurrenceTransaction_OpportunityMaster]
--GO

--ALTER TABLE [dbo].[RecurrenceTransaction]  WITH CHECK ADD  CONSTRAINT [FK_RecurrenceTransaction_RecurrenceConfiguration] FOREIGN KEY([numRecConfigID])
--REFERENCES [dbo].[RecurrenceConfiguration] ([numRecConfigID])
--ON DELETE CASCADE
--GO

--ALTER TABLE [dbo].[RecurrenceTransaction] CHECK CONSTRAINT [FK_RecurrenceTransaction_RecurrenceConfiguration]
--GO

--GO

--/****** Object:  Table [dbo].[RecurrenceTransactionHistory]    Script Date: 16-Oct-14 5:18:22 PM ******/
--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

--CREATE TABLE [dbo].[RecurrenceTransactionHistory](
--	[numRecTranHistoryID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
--	[numRecConfigID] [numeric](18, 0) NOT NULL,
--	[numRecurrOppID] [numeric](18, 0) NULL,
--	[numRecurrOppBizDocID] [numeric](18, 0) NULL,
--	[dtCreatedDate] [datetime] NOT NULL,
-- CONSTRAINT [PK_RecurrenceTransactionHistory] PRIMARY KEY CLUSTERED 
--(
--	[numRecTranHistoryID] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
--) ON [PRIMARY]

--GO



----Changed Store Procedures

--USP_CreateBizDocs
--USP_DeleteOppurtunity
--USP_GetRecurringPart
--USP_OppBizDocs
--USP_OPPDetails
--USP_OPPDetailsDTLPL
--USP_OPPGetINItems
--USP_ManageOppSettings

----New Store Procedures

--USP_BizRecurrence_BizDoc
--USP_BizRecurrence_Execute
--USP_BizRecurrence_Order
--USP_BizRecurrence_WorkOrder
--USP_RecurrenceConfiguration_Delete
--USP_RecurrenceConfiguration_GetDetail
--USP_RecurrenceConfiguration_GetParentDetail
--USP_RecurrenceConfiguration_Insert
--USP_RecurrenceConfiguration_SetDisable
--USP_RecurrenceErrorLog_Insert

---- New Function
--dbo.CheckAllItemQuantityAddedToBizDoc
------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------



-- BEGIN TRANSACTION

-- DECLARE @numPageNavID numeric 
-- SELECT @numPageNavID = MAX(numPageNavID) + 1  FROM dbo.PageNavigationDTL 
-- INSERT INTO dbo.PageNavigationDTL
--   ( numPageNavID ,
--     numModuleID ,
--     numParentID ,
--     vcPageNavName ,
--     vcNavURL ,
--     vcImageURL ,
--     bitVisible ,
--     numTabID,
--     intSortOrder
--   )
-- SELECT @numPageNavID ,13,73,'Shipping',NULL,NULL,1,-1,4


-- DECLARE CursorTreeNavigation CURSOR FOR
-- SELECT numDomainId FROM Domain WHERE numDomainId <> -255
 
-- OPEN CursorTreeNavigation;

-- DECLARE @numDomainId NUMERIC(18,0);

-- FETCH NEXT FROM CursorTreeNavigation INTO @numDomainId;
-- WHILE (@@FETCH_STATUS = 0)
-- BEGIN
   
--    ---- Give permission for each domain to tree node
    
--	DELETE FROM TreeNavigationAuthorization WHERE numPageNavID=@numPageNavID AND numDomainID = @numDomainId
 
--	SELECT * INTO #tempData1 FROM
--  (
--   SELECT    T.numTabID,
--                        CASE WHEN bitFixed = 1
--                             THEN CASE WHEN EXISTS ( SELECT numTabName
--                                                     FROM   TabDefault
--                                                     WHERE  numTabId = T.numTabId
--                                                            AND tintTabType = T.tintTabType AND numDomainID =D.numDomainID)
--                                       THEN ( SELECT TOP 1
--                                                        numTabName
--                                              FROM      TabDefault
--                                              WHERE     numTabId = T.numTabId
--                                                        AND tintTabType = T.tintTabType AND numDomainID = D.numDomainID
--                                            )
--                                       ELSE T.numTabName
--                                  END
--                             ELSE T.numTabName
--                        END numTabname,
--                        vcURL,
--                        bitFixed,
--                        1 AS tintType,
--                        T.vcImage,
--                        D.numDomainID,
--                        A.numGroupID
--              FROM      TabMaster T
--                        CROSS JOIN Domain D --ON D.numDomainID = T.numDomainID
--                        LEFT JOIN AuthenticationGroupMaster A ON A.numDomainID = D.numDomainId
--              WHERE     bitFixed = 1
--                        AND D.numDomainId <> -255
--                        AND t.tintTabType = 1
              
--              UNION ALL
--              SELECT    -1 AS [numTabID],
--                        'Administration' numTabname,
--                        '' vcURL,
--                        1 bitFixed,
--                        1 AS tintType,
--                        '' AS vcImage,
--                        D.numDomainID,
--                        A.numGroupID
--              FROM      Domain D
--                        JOIN AuthenticationGroupMaster A ON A.numDomainID = D.numDomainId
--                        WHERE D.numDomainID = @numDomainID
     
--     UNION ALL
--     SELECT    -3 AS [numTabID],
--      'Advanced Search' numTabname,
--      '' vcURL,
--      1 bitFixed,
--      1 AS tintType,
--      '' AS vcImage,
--      D.numDomainID,
--      A.numGroupID
--     FROM      Domain D
--      JOIN AuthenticationGroupMaster A ON A.numDomainID = D.numDomainId          
--            ) TABLE2         
                        
--	INSERT  INTO dbo.TreeNavigationAuthorization
--            (
--              numGroupID,
--              numTabID,
--              numPageNavID,
--              bitVisible,
--              numDomainID,
--              tintType
--            )
--            SELECT  numGroupID,
--                    PND.numTabID,
--                    numPageNavID,
--                    bitVisible,
--                    numDomainID,
--                    tintType
--            FROM    dbo.PageNavigationDTL PND
--                    JOIN #tempData1 TD ON TD.numTabID = PND.numTabID 
--                    WHERE PND.numPageNavID= @numPageNavID
  
--    DROP TABLE #tempData1

--    FETCH NEXT FROM CursorTreeNavigation INTO @numDomainId;
--END;

--CLOSE CursorTreeNavigation;
--DEALLOCATE CursorTreeNavigation;


--UPDATE PageNavigationDTL SET bitVisible=0, vcPageNavName='Global Settings',intsortOrder=1 where numPageNavID=120 --Ecommerce Settings
--UPDATE PageNavigationDTL SET bitVisible=0, intsortOrder=2 where numPageNavID=103 --Categories
--UPDATE PageNavigationDTL SET bitVisible=0, vcPageNavName='Location Mapping',intsortOrder=3 where numPageNavID=119 --Warehouse Mapping
--UPDATE PageNavigationDTL SET bitVisible=0, vcPageNavName='Payment Gateways',intsortOrder=5 where numPageNavID=143 --Payment Gateways
--UPDATE PageNavigationDTL SET bitVisible=0, vcPageNavName='Marketplaces',intsortOrder=6 where numPageNavID=150 --Market Places(ol-e-Commerce API)
--UPDATE PageNavigationDTL SET bitVisible=0, vcPageNavName='Promotions',intsortOrder=7 where numPageNavID=187 --Promotions & Offers
--UPDATE PageNavigationDTL SET bitVisible=0, vcPageNavName='Error Codes',intsortOrder=8 where numPageNavID=197 --Error Message 
--UPDATE PageNavigationDTL SET bitVisible=0, vcPageNavName='Reviews & Ratings',intsortOrder=9 where numPageNavID=217 --Reviews & Ratings
--UPDATE PageNavigationDTL SET bitVisible=0, vcPageNavName='Design',intsortOrder=10 where numPageNavID=157 --Design Cart


--UPDATE PageNavigationDTL SET numParentID=@numPageNavID where numPageNavID in (121,193) --under Shipping

--UPDATE PageNavigationDTL SET vcPageNavName='Shipping Carrier Setup' where numPageNavID in (121)
--UPDATE PageNavigationDTL SET vcPageNavName='Shipping Rules' where numPageNavID in (193)

------Ecommerce settings main
--UPDATE PageNavigationDTL SET vcNavURL='../Items/frmECommerceSettings.aspx',vcPageNavName='e-Commerce' where numPageNavID=73


--ROLLBACK TRANSACTION

----New Tables
--/****** Object:  Table [dbo].[DemandForecast]    Script Date: 19-Sep-14 2:18:04 PM ******/
--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

--CREATE TABLE [dbo].[DemandForecast](
--	[numDFID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
--	[numDomainID] [numeric](18, 0) NOT NULL,
--	[numDFAPID] [numeric](18, 0) NOT NULL,
--	[bitLastWeek] [bit] NOT NULL,
--	[bitLastYear] [bit] NULL,
--	[numDFDaysID] [numeric](18, 0) NOT NULL,
--	[bitIncludeSalesOpp] [bit] NOT NULL,
--	[dtCreatedDate] [datetime] NOT NULL,
--	[dtModifiedDate] [datetime] NULL,
--	[numCreatedBy] [numeric](18, 0) NULL,
--	[numModifiedBy] [numeric](18, 0) NULL,
--	[dtExecutionDate] [datetime] NULL,
-- CONSTRAINT [PK_DemandForecast] PRIMARY KEY CLUSTERED 
--(
--	[numDFID] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
--) ON [PRIMARY]

--GO

--/****** Object:  Table [dbo].[DemandForecastAnalysisPattern]    Script Date: 19-Sep-14 2:18:32 PM ******/
--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

--SET ANSI_PADDING ON
--GO

--CREATE TABLE [dbo].[DemandForecastAnalysisPattern](
--	[numDFAPID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
--	[numDomainID] [numeric](18, 0) NULL,
--	[vcAnalysisPattern] [varchar](500) NOT NULL,
--	[numDays] [int] NOT NULL,
--	[bitNextWeekLastYear] [bit] NOT NULL,
-- CONSTRAINT [PK_DemandForecastAnalysisPattern] PRIMARY KEY CLUSTERED 
--(
--	[numDFAPID] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
--) ON [PRIMARY]

--GO

--SET ANSI_PADDING ON
--GO

--/****** Object:  Table [dbo].[DemandForecastDays]    Script Date: 19-Sep-14 2:19:05 PM ******/
--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

--SET ANSI_PADDING ON
--GO

--CREATE TABLE [dbo].[DemandForecastDays](
--	[numDFDaysID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
--	[numDomainID] [numeric](18, 0) NULL,
--	[vcDays] [varchar](500) NOT NULL,
--	[numDays] [int] NOT NULL,
-- CONSTRAINT [PK_DemandForecastDays] PRIMARY KEY CLUSTERED 
--(
--	[numDFDaysID] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
--) ON [PRIMARY]

--GO

--SET ANSI_PADDING ON
--GO

--/****** Object:  Table [dbo].[DemandForecastItemClassification]    Script Date: 19-Sep-14 2:19:35 PM ******/
--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

--CREATE TABLE [dbo].[DemandForecastItemClassification](
--	[numDFICID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
--	[numDFID] [numeric](18, 0) NOT NULL,
--	[numItemClassificationID] [numeric](18, 0) NOT NULL,
-- CONSTRAINT [PK_DemandForecastItemClassification] PRIMARY KEY CLUSTERED 
--(
--	[numDFICID] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
--) ON [PRIMARY]

--GO

--ALTER TABLE [dbo].[DemandForecastItemClassification]  WITH CHECK ADD  CONSTRAINT [FK_DemandForecastItemClassification_DemandForecast] FOREIGN KEY([numDFID])
--REFERENCES [dbo].[DemandForecast] ([numDFID])
--ON DELETE CASCADE
--GO

--ALTER TABLE [dbo].[DemandForecastItemClassification] CHECK CONSTRAINT [FK_DemandForecastItemClassification_DemandForecast]
--GO

--/****** Object:  Table [dbo].[DemandForecastItemGroup]    Script Date: 19-Sep-14 2:20:06 PM ******/
--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

--CREATE TABLE [dbo].[DemandForecastItemGroup](
--	[numDFIGID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
--	[numDFID] [numeric](18, 0) NOT NULL,
--	[numItemGroupID] [numeric](18, 0) NOT NULL,
-- CONSTRAINT [PK_DemandForecastItemGroup] PRIMARY KEY CLUSTERED 
--(
--	[numDFIGID] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
--) ON [PRIMARY]

--GO

--ALTER TABLE [dbo].[DemandForecastItemGroup]  WITH CHECK ADD  CONSTRAINT [FK_DemandForecastItemGroup_DemandForecast] FOREIGN KEY([numDFID])
--REFERENCES [dbo].[DemandForecast] ([numDFID])
--ON DELETE CASCADE
--GO

--ALTER TABLE [dbo].[DemandForecastItemGroup] CHECK CONSTRAINT [FK_DemandForecastItemGroup_DemandForecast]
--GO


--/****** Object:  Table [dbo].[DemandForecastWarehouse]    Script Date: 19-Sep-14 2:20:34 PM ******/
--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

--CREATE TABLE [dbo].[DemandForecastWarehouse](
--	[numDFWID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
--	[numDFID] [numeric](18, 0) NOT NULL,
--	[numWarehouseID] [numeric](18, 0) NOT NULL,
-- CONSTRAINT [PK_DemandForecastWarehouse] PRIMARY KEY CLUSTERED 
--(
--	[numDFWID] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
--) ON [PRIMARY]

--GO

--ALTER TABLE [dbo].[DemandForecastWarehouse]  WITH CHECK ADD  CONSTRAINT [FK_DemandForecastWarehouse_DemandForecast] FOREIGN KEY([numDFID])
--REFERENCES [dbo].[DemandForecast] ([numDFID])
--ON DELETE CASCADE
--GO

--ALTER TABLE [dbo].[DemandForecastWarehouse] CHECK CONSTRAINT [FK_DemandForecastWarehouse_DemandForecast]
--GO

---- Change Planning and Procurement link in orders tab tree navigation
--UPDATE 
--	PageNavigationDTL
--SET
--	vcNavURL = '../DemandForecast/frmDemandForecastList.aspx'
--WHERE
--	numPageNavID = 128 AND
--	numModuleID = 10 AND
--	numParentID = 122


--/* OLD URL  - DO NOT EXECUTE ON SERVER
--UPDATE 
--	PageNavigationDTL
--SET
--	vcNavURL = '../Opportunity/frmPlangProcurement.aspx?R=1'
--WHERE
--	numPageNavID = 128 AND
--	numModuleID = 10 AND
--	numParentID = 122
--*/

---- Insert master detail of analysis pattern 
--INSERT INTO DemandForecastAnalysisPattern 
--	([numDomainID],[vcAnalysisPattern],[numDays],[bitNextWeekLastYear]) 
--VALUES
--(NULL,'Unit sales for the last week (7 days)',7,0),
--(NULL,'Unit sales for the last 2 weeks (14 days)',14,0),
--(NULL,'Unit sales for the last 1 month (30 days)',30,0),
--(NULL,'Unit sales for the last 3 months (90 days)',90,0),
--(NULL,'Unit sales from next week (7 days) based on last year',7,1),
--(NULL,'Unit sales from next 2 weeks (14 days) based on last year',14,1),
--(NULL,'Unit sales from next 1 month (30 days) based on last year',30,1),
--(NULL,'Unit sales for next 3 months (90 days) based on last year',90,1)


---- Insert master detail of forcast days 
--INSERT INTO DemandForecastDays
--	([numDomainID],[vcDays],[numDays])
--VALUES	
--	(NULL,'Enough supply to last us 1 week (7 days)',7),
--	(NULL,'Enough supply to last us 2 weeks (14 days)',14),
--	(NULL,'Enough supply to last us 1 month (30 days)',30),
--	(NULL,'Enough supply to last us 2 months (60 days)',60),
--	(NULL,'Enough supply to last us 3 months (90 days)',90)

----Add column intLeadTimeDays to vendor table
--ALTER TABLE dbo.Vendor ADD
--intLeadTimeDays NUMERIC(18,0) NULL

--BEGIN TRANSACTION

----Add Field to DycFieldMaster
--DECLARE @NewFieldID AS INT

--INSERT INTO DycFieldMaster 
--(numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,bitInlineEdit,bitDeleted,bitAllowEdit,bitDefault,numListID,bitInResults) 
--VALUES
--(4,'Lead Time (Days)','intLeadTimeDays','intLeadTimeDays','Vendor','N','R','TextBox',1,0,1,0,0,0)
--SELECT @NewFieldID = SCOPE_IDENTITY()

--DECLARE @numQtySoldID AS INT
--INSERT INTO DycFieldMaster 
--(numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,bitInlineEdit,bitDeleted,bitAllowEdit,bitDefault,numListID,bitInResults) 
--VALUES
--(5,'Qty Sold','numQuantitySold','numQuantitySold','','V','R','TextArea',1,0,0,0,0,0)
--SELECT @numQtySoldID = SCOPE_IDENTITY()

--DECLARE @numOrderDate AS INT
--INSERT INTO DycFieldMaster 
--(numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,bitInlineEdit,bitDeleted,bitAllowEdit,bitDefault,numListID,bitInResults) 
--VALUES
--(5,'Order Date','vcOrderDate','vcOrderDate','','V','R','TextArea',1,0,1,0,0,0)
--SELECT @numOrderDate = SCOPE_IDENTITY()

----Planning & Procurement Grid Columns Setting
--DECLARE @numFormID AS INT = 125
--INSERT INTO DynamicFormMaster ([numFormId],[vcFormName],[cCustomFieldsAssociated],[cAOIAssociated],[bitDeleted],[tintFlag]) VALUES (@numFormID,'Planning & Procurement Grid Columns Setting','N','N',0,1)

--IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 189 AND numFormID = @numFormID)
--BEGIN
--	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField],[bitDefault],[order]) VALUES (4,189,@numFormID,'Item Name',0,1,0,0,1,1,1)
--END
--IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 211 AND numFormID = @numFormID)
--BEGIN
--	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField],[bitDefault]) VALUES (4,211,@numFormID,'Item Code',0,0,0,0,1,0)
--END
--IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 193 AND numFormID = @numFormID)
--BEGIN
--	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField],[bitDefault]) VALUES (4,193,@numFormID,'Model ID',0,0,0,0,1,0)
--END
--IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 195 AND numFormID = @numFormID)
--BEGIN
--	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField],[bitDefault]) VALUES (4,195,@numFormID,'OnHand',0,0,0,0,1,0)
--END
--IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 196 AND numFormID = @numFormID)
--BEGIN
--	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField],[bitDefault]) VALUES (4,196,@numFormID,'OnOrder',0,0,0,0,1,0)
--END
--IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 197 AND numFormID = @numFormID)
--BEGIN
--	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField],[bitDefault]) VALUES (4,197,@numFormID,'OnAllocation',0,0,0,0,1,0)
--END
--IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 198 AND numFormID = @numFormID)
--BEGIN
--	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField],[bitDefault]) VALUES (4,198,@numFormID,'BackOrder',0,0,0,0,1,0)
--END
--IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 200 AND numFormID = @numFormID)
--BEGIN
--	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField],[bitDefault]) VALUES (4,200,@numFormID,'ReOrder',0,0,0,0,1,0)
--END
--IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 273 AND numFormID = @numFormID)
--BEGIN
--	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField],[bitDefault]) VALUES (4,273,@numFormID,'Item Classification',0,0,0,0,1,0)
--END
--IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 274 AND numFormID = @numFormID)
--BEGIN
--	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField],[bitDefault]) VALUES (4,274,@numFormID,'Item Group',0,0,0,0,1,0)
--END
--IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 281 AND numFormID = @numFormID)
--BEGIN
--	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField],[bitDefault]) VALUES (4,281,@numFormID,'SKU',0,0,0,0,1,0)
--END
--IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 3 AND numFieldID = 298 AND numFormID = @numFormID)
--BEGIN
--	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField],[bitDefault],[order]) VALUES (3,298,@numFormID,'Warehouse',0,1,0,0,1,1,2)
--END
--IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 290 AND numFormID = @numFormID)
--BEGIN
--	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField],[bitDefault]) VALUES (4,290,@numFormID,'Min Order Quantity',0,0,0,0,1,0)
--END
--IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = @NewFieldID AND numFormID = @numFormID)
--BEGIN
--	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField],[bitDefault],[order]) VALUES (4,@NewFieldID,@numFormID,'Lead Time (Days)',0,0,0,0,1,1,3)
--END
--IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 5 AND numFieldID = @numQtySoldID AND numFormID = @numFormID)
--BEGIN
--	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField],[bitDefault],[order]) VALUES (5,@numQtySoldID,@numFormID,'Qty Sold',0,0,0,0,1,1,4)
--END
--IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 5 AND numFieldID = @numOrderDate AND numFormID = @numFormID)
--BEGIN
--	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField],[bitDefault],[order]) VALUES (5,@numOrderDate,@numFormID,'Order Date',0,0,0,0,1,1,5)
--END

--ROLLBACK

--StoreProcedures
-----------------

--USP_GetColumnConfiguration1
--USP_GetVendors
--USP_TicklerActItems
--USP_UpdateVendors

--NewStoreProcedures
--------------------
--USP_DemandForecast_Delete
--USP_DemandForecast_DeleteOpp
--USP_DemandForecast_Execute
--USP_DemandForecast_Get
--USP_DemandForecast_GetByID
--USP_DemandForecast_GetByItemVendor
--USP_DemandForecast_Save
--USP_DemandForecast_UpdateInventory
--USP_DemandForecastAnalysisPattern_GetAll
--USP_DemandForecastDays_GetAll

--Functions
-----------

--GetDFHistoricalSalesOppData
--GetDFHistoricalSalesOrderData
--GetItemPurchaseVendorPrice
--GetItemQuantityToOreder






--ALTER TABLE GroupAuthorization ADD
--bitCustomRelationship bit NOT NULL DEFAULT(0)

---------------------------------------------------------------------------------------------------------------------

--ALTER TABLE Domain ADD
--numDefaultSalesPricing TINYINT 

---------------------------------------------------------------------------------------------------------------------

---- Remove Organization List Page
--DELETE FROM PageMaster WHERE numModuleID = 32 AND numPageID = 1
--DELETE FROM GroupAuthorization WHERE numModuleID = 32 AND numPageID = 1

---------------------------------------------------------------------------------------------------------------------

--/****** Object:  Table [dbo].[TreeNodeOrder]    Script Date: 14-Aug-14 1:40:04 PM ******/
--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

--CREATE TABLE [dbo].[TreeNodeOrder](
--	[ID] [int] IDENTITY(1,1) NOT NULL,
--	[numTabID] [int] NULL,
--	[numPageNavID] [int] NULL,
--	[numDomainID] [int] NULL,
--	[numGroupID] [int] NULL,
--	[numListItemID] [int] NULL,
--	[numParentID] [int] NULL,
--	[numOrder] [int] NULL,
--	[bitVisible] [int] NULL,
--	[tintType] [int] NULL,
-- CONSTRAINT [PK_TreeNodeOrder] PRIMARY KEY CLUSTERED 
--(
--	[ID] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
--) ON [PRIMARY]

--GO

----------------------------------------------------------------------------------------------------------------------

--/****** Object:  Table [dbo].[TicklerListFilterConfiguration]    Script Date: 21-Aug-14 9:42:17 AM ******/
--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

--SET ANSI_PADDING ON
--GO

--CREATE TABLE [dbo].[TicklerListFilterConfiguration](
--	[ID] [int] IDENTITY(1,1) NOT NULL,
--	[numDomainID] [numeric](18, 0) NOT NULL,
--	[numUserCntID] [numeric](18, 0) NOT NULL,
--	[vcSelectedEmployee] [varchar](500) NULL,
--	[numCriteria] [int] NULL,
--	[vcActionTypes] [varchar](500) NULL,
--	[vcSelectedEmployeeOpp] [varchar](500) NULL,
--	[vcSelectedEmployeeCases] [varchar](500) NULL,
--	[vcAssignedStages] [varchar](500) NULL,
--	[numCriteriaCases] [int] NULL,
-- CONSTRAINT [PK_TicklerListFilterConfiguration] PRIMARY KEY CLUSTERED 
--(
--	[ID] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
--) ON [PRIMARY]

--GO

----------------------------------------------------------------------------------------------------------------------

--ALTER TABLE dbo.SalesOrderConfiguration ADD
--bitDisplayCouponDiscount BIT NOT NULL DEFAULT(1),
--bitDisplayShippingCharges BIT NOT NULL DEFAULT(1),
--bitDisplayDiscount BIT NOT NULL DEFAULT(1)

----------------------------------------------------------------------------------------------------------------------

--BEGIN TRANSACTION

--DECLARE @NewFieldID INTEGER

--INSERT INTO dbo.DycFieldMaster 
--(		
--	numModuleID,
--	numDomainID,
--	vcFieldName,
--	vcDbColumnName,
--	vcOrigDbColumnName,
--	vcPropertyName,
--	vcLookBackTableName,
--	vcFieldDataType,
--	vcFieldType,
--	vcAssociatedControlType ,
--	bitInResults,
--	bitDeleted,
--	bitSettingField,
--	numListID,
--	bitAllowEdit,
--	bitDefault,
--	bitDetailField
--)
--VALUES  
--( 
--	3 , 
--	NULL,
--	N'Total Discount' ,
--	N'numTotalDiscount' , 
--	N'numTotalDiscount' , 
--	'TotalDiscount' ,
--	N'OpportunityMaster' ,
--	'M' , 
--	'R' , 
--	N'Label' ,
--	1,
--	0,
--	1,
--	0,
--	0,
--	0,
--	1
--)

--SELECT @NewFieldID = SCOPE_IDENTITY() 

--IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 3 AND numFieldID = @NewFieldID AND numFormID = 39)
--BEGIN
--	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit],[bitDetailField]) VALUES (3,@NewFieldID,39,0,0,0,1)
--END

--ROLLBACK TRANSACTION

--CheckOrderInventoryStatus
--CheckOrderItemInventoryStatus
--GetPriceBasedOnPriceBook
--getpricebasedonpricebookTable

--USP_CopyStageDetailsForDomian
--USP_GetAuthDetailsForDomain
--USP_GetTreeNodesForRelationship
--USP_TicklerListFilterConfiguration_Get
--USP_TicklerListFilterConfiguration_Save
--USP_GetAllSelectedSystemModulesPagesAndAccesses
--USP_GetDomainDetails
--USP_GetPageNavigationAuthorizationDetails
--USP_GetTableInfoDefault
--USP_ItemDetails
--USP_ItemPricingRecommm
--USP_ManagePageNavigationAuthorization
--usp_SetAllSelectedSystemModulesPagesAndAccesses
--USP_TicklerActItems
--USP_TicklerCases
--USP_TicklerOpportunity
--USP_UpdatedomainDetails
--USP_SalesOrderConfiguration_Save
--USP_OPPDetailsDTLPL
--USP_OPPItemDetails

--------------------------------------------------------------------------
/**** New Order Form- Configuration to show hide fields in section 1 ****/
--------------------------------------------------------------------------

--ALTER TABLE SalesOrderConfiguration ADD
--bitDisplayCurrency BIT NOT NULL DEFAULT (1),
--bitDisplayAssignTo BIT NOT NULL DEFAULT (1),
--bitDisplayTemplate BIT NOT NULL DEFAULT (1)


---------------------------------------------------------------------------------------------
/**** Manage Authorization - Add Layout Button and Grid Configuration Visibility Rights ****/
---------------------------------------------------------------------------------------------

--BEGIN TRANSACTION
-- 1. Leads - ModileID = 2

--	INSERT INTO PageMaster 
--		(numPageID, numModuleID, vcFileName, vcPageDesc, bitIsViewApplicable, bitIsAddApplicable, bitIsUpdateApplicable, bitIsDeleteApplicable, bitIsExportApplicable)
--	VALUES
--		(11,2,'','Layout Button',1,0,0,0,0)

--	INSERT INTO PageMaster 
--		(numPageID, numModuleID, vcFileName, vcPageDesc, bitIsViewApplicable, bitIsAddApplicable, bitIsUpdateApplicable, bitIsDeleteApplicable, bitIsExportApplicable)
--	VALUES
--		(12,2,'','Grid Configuration',1,0,0,0,0)


---- 2. Prospects - ModileID = 3

--	INSERT INTO PageMaster 
--		(numPageID, numModuleID, vcFileName, vcPageDesc, bitIsViewApplicable, bitIsAddApplicable, bitIsUpdateApplicable, bitIsDeleteApplicable, bitIsExportApplicable)
--	VALUES
--		(14,3,'','Layout Button',1,0,0,0,0)

--	INSERT INTO PageMaster 
--		(numPageID, numModuleID, vcFileName, vcPageDesc, bitIsViewApplicable, bitIsAddApplicable, bitIsUpdateApplicable, bitIsDeleteApplicable, bitIsExportApplicable)
--	VALUES
--		(15,3,'','Grid Configuration',1,0,0,0,0)
	
---- 3. Accounts - ModileID = 4

--	INSERT INTO PageMaster 
--		(numPageID, numModuleID, vcFileName, vcPageDesc, bitIsViewApplicable, bitIsAddApplicable, bitIsUpdateApplicable, bitIsDeleteApplicable, bitIsExportApplicable)
--	VALUES
--		(16,4,'','Layout Button',1,0,0,0,0)

--	INSERT INTO PageMaster 
--		(numPageID, numModuleID, vcFileName, vcPageDesc, bitIsViewApplicable, bitIsAddApplicable, bitIsUpdateApplicable, bitIsDeleteApplicable, bitIsExportApplicable)
--	VALUES
--		(17,4,'','Grid Configuration',1,0,0,0,0)

---- 4. Support - ModileID = 7

--	INSERT INTO PageMaster 
--		(numPageID, numModuleID, vcFileName, vcPageDesc, bitIsViewApplicable, bitIsAddApplicable, bitIsUpdateApplicable, bitIsDeleteApplicable, bitIsExportApplicable)
--	VALUES
--		(12,7,'','Layout Button',1,0,0,0,0)

--	INSERT INTO PageMaster	
--		(numPageID, numModuleID, vcFileName, vcPageDesc, bitIsViewApplicable, bitIsAddApplicable, bitIsUpdateApplicable, bitIsDeleteApplicable, bitIsExportApplicable)
--	VALUES
--		(13,7,'','Grid Configuration',1,0,0,0,0)

---- 5. Projects - ModileID = 12

--	INSERT INTO PageMaster 
--		(numPageID, numModuleID, vcFileName, vcPageDesc, bitIsViewApplicable, bitIsAddApplicable, bitIsUpdateApplicable, bitIsDeleteApplicable, bitIsExportApplicable)
--	VALUES
--		(13,12,'','Layout Button',1,0,0,0,0)

--	INSERT INTO PageMaster 
--		(numPageID, numModuleID, vcFileName, vcPageDesc, bitIsViewApplicable, bitIsAddApplicable, bitIsUpdateApplicable, bitIsDeleteApplicable, bitIsExportApplicable)
--	VALUES
--		(14,12,'','Grid Configuration',1,0,0,0,0)

---- 6. Opportunities/Orders - ModileID = 10

--	INSERT INTO PageMaster 
--		(numPageID, numModuleID, vcFileName, vcPageDesc, bitIsViewApplicable, bitIsAddApplicable, bitIsUpdateApplicable, bitIsDeleteApplicable, bitIsExportApplicable)
--	VALUES
--		(32,10,'','Layout Button',1,0,0,0,0)

--	INSERT INTO PageMaster 
--		(numPageID, numModuleID, vcFileName, vcPageDesc, bitIsViewApplicable, bitIsAddApplicable, bitIsUpdateApplicable, bitIsDeleteApplicable, bitIsExportApplicable)
--	VALUES
--		(33,10,'','Grid Configuration',1,0,0,0,0)

---- 7. Tickler - ModileID = 1

--	INSERT INTO PageMaster 
--		(numPageID, numModuleID, vcFileName, vcPageDesc, bitIsViewApplicable, bitIsAddApplicable, bitIsUpdateApplicable, bitIsDeleteApplicable, bitIsExportApplicable)
--	VALUES
--		(10,1,'','Grid Configuration',1,0,0,0,0)


--/*************************** Allow Edit Unit Price access to all users for all domains *******************************/

--SELECT * INTO #temp FROM
--(
--	SELECT 
--		ROW_NUMBER() OVER(ORDER BY numDomainId DESC) AS Row, 
--		numDomainId 
--	FROM 
--		Domain 
--	WHERE 
--		numDomainId <> -255
--) TABLE2
	
--DECLARE @RowCount INT
--SET @RowCount = (SELECT COUNT(Row) FROM #temp) 
	
--DECLARE @I INT
--DECLARE @numDomainId NUMERIC(18,0)
	
--SET @I = 1

--WHILE (@I <= @RowCount)
--BEGIN
			
--	SELECT @numDomainId = numDomainID FROM #temp WHERE Row = @I
	
--	-- 1. Leads - ModileID = 2
--	INSERT INTO dbo.GroupAuthorization
--		(numDomainID,numModuleID,numPageID,numGroupID,intExportAllowed,intPrintAllowed,intViewAllowed,intAddAllowed,intUpdateAllowed,intDeleteAllowed)
--	SELECT
--		@numDomainId,2,11,numGroupID,0,0,3,0,0,0
--	FROM
--		AuthenticationGroupMaster
--	WHERE
--		numDomainID = @numDomainId AND
--		tintGroupType = 1
--	INSERT INTO dbo.GroupAuthorization
--		(numDomainID,numModuleID,numPageID,numGroupID,intExportAllowed,intPrintAllowed,intViewAllowed,intAddAllowed,intUpdateAllowed,intDeleteAllowed)
--	SELECT
--		@numDomainId,2,12,numGroupID,0,0,3,0,0,0
--	FROM
--		AuthenticationGroupMaster
--	WHERE
--		numDomainID = @numDomainId AND
--		tintGroupType = 1

--	-- 2. Prospects - ModileID = 3
--	INSERT INTO dbo.GroupAuthorization
--		(numDomainID,numModuleID,numPageID,numGroupID,intExportAllowed,intPrintAllowed,intViewAllowed,intAddAllowed,intUpdateAllowed,intDeleteAllowed)
--	SELECT
--		@numDomainId,3,14,numGroupID,0,0,3,0,0,0
--	FROM
--		AuthenticationGroupMaster
--	WHERE
--		numDomainID = @numDomainId AND
--		tintGroupType = 1
--	INSERT INTO dbo.GroupAuthorization
--		(numDomainID,numModuleID,numPageID,numGroupID,intExportAllowed,intPrintAllowed,intViewAllowed,intAddAllowed,intUpdateAllowed,intDeleteAllowed)
--	SELECT
--		@numDomainId,3,15,numGroupID,0,0,3,0,0,0
--	FROM
--		AuthenticationGroupMaster
--	WHERE
--		numDomainID = @numDomainId AND
--		tintGroupType = 1

--	-- 3. Accounts - ModileID = 4
--	INSERT INTO dbo.GroupAuthorization
--		(numDomainID,numModuleID,numPageID,numGroupID,intExportAllowed,intPrintAllowed,intViewAllowed,intAddAllowed,intUpdateAllowed,intDeleteAllowed)
--	SELECT
--		@numDomainId,4,16,numGroupID,0,0,3,0,0,0
--	FROM
--		AuthenticationGroupMaster
--	WHERE
--		numDomainID = @numDomainId AND
--		tintGroupType = 1
--	INSERT INTO dbo.GroupAuthorization
--		(numDomainID,numModuleID,numPageID,numGroupID,intExportAllowed,intPrintAllowed,intViewAllowed,intAddAllowed,intUpdateAllowed,intDeleteAllowed)
--	SELECT
--		@numDomainId,4,17,numGroupID,0,0,3,0,0,0
--	FROM
--		AuthenticationGroupMaster
--	WHERE
--		numDomainID = @numDomainId AND
--		tintGroupType = 1

--	-- 4. Support - ModileID = 7	
--	INSERT INTO dbo.GroupAuthorization
--		(numDomainID,numModuleID,numPageID,numGroupID,intExportAllowed,intPrintAllowed,intViewAllowed,intAddAllowed,intUpdateAllowed,intDeleteAllowed)
--	SELECT
--		@numDomainId,7,12,numGroupID,0,0,3,0,0,0
--	FROM
--		AuthenticationGroupMaster
--	WHERE
--		numDomainID = @numDomainId AND
--		tintGroupType = 1
--	INSERT INTO dbo.GroupAuthorization
--		(numDomainID,numModuleID,numPageID,numGroupID,intExportAllowed,intPrintAllowed,intViewAllowed,intAddAllowed,intUpdateAllowed,intDeleteAllowed)
--	SELECT
--		@numDomainId,7,13,numGroupID,0,0,3,0,0,0
--	FROM
--		AuthenticationGroupMaster
--	WHERE
--		numDomainID = @numDomainId AND
--		tintGroupType = 1

--	-- 5. Projects - ModileID = 12
--	INSERT INTO dbo.GroupAuthorization
--		(numDomainID,numModuleID,numPageID,numGroupID,intExportAllowed,intPrintAllowed,intViewAllowed,intAddAllowed,intUpdateAllowed,intDeleteAllowed)
--	SELECT
--		@numDomainId,12,13,numGroupID,0,0,3,0,0,0
--	FROM
--		AuthenticationGroupMaster
--	WHERE
--		numDomainID = @numDomainId AND
--		tintGroupType = 1
--	INSERT INTO dbo.GroupAuthorization
--		(numDomainID,numModuleID,numPageID,numGroupID,intExportAllowed,intPrintAllowed,intViewAllowed,intAddAllowed,intUpdateAllowed,intDeleteAllowed)
--	SELECT
--		@numDomainId,12,14,numGroupID,0,0,3,0,0,0
--	FROM
--		AuthenticationGroupMaster
--	WHERE
--		numDomainID = @numDomainId AND
--		tintGroupType = 1

--	-- 6. Opportunities/Orders - ModileID = 10
--	INSERT INTO dbo.GroupAuthorization
--		(numDomainID,numModuleID,numPageID,numGroupID,intExportAllowed,intPrintAllowed,intViewAllowed,intAddAllowed,intUpdateAllowed,intDeleteAllowed)
--	SELECT
--		@numDomainId,10,32,numGroupID,0,0,3,0,0,0
--	FROM
--		AuthenticationGroupMaster
--	WHERE
--		numDomainID = @numDomainId AND
--		tintGroupType = 1
--	INSERT INTO dbo.GroupAuthorization
--		(numDomainID,numModuleID,numPageID,numGroupID,intExportAllowed,intPrintAllowed,intViewAllowed,intAddAllowed,intUpdateAllowed,intDeleteAllowed)
--	SELECT
--		@numDomainId,10,33,numGroupID,0,0,3,0,0,0
--	FROM
--		AuthenticationGroupMaster
--	WHERE
--		numDomainID = @numDomainId AND
--		tintGroupType = 1

--	-- 7. Tickler - ModileID = 1
--	INSERT INTO dbo.GroupAuthorization
--		(numDomainID,numModuleID,numPageID,numGroupID,intExportAllowed,intPrintAllowed,intViewAllowed,intAddAllowed,intUpdateAllowed,intDeleteAllowed)
--	SELECT
--		@numDomainId,1,10,numGroupID,0,0,3,0,0,0
--	FROM
--		AuthenticationGroupMaster
--	WHERE
--		numDomainID = @numDomainId AND
--		tintGroupType = 1

--	SET @I = @I  + 1
--END
	
--DROP TABLE #temp


--ROLLBACK

------------------------------------------------
--/**** BizForm Wizard add module dropdown ****/
------------------------------------------------

--/****** Object:  Table [dbo].[BizFormWizardModule]    Script Date: 21-Jul-14 2:01:33 PM ******/
--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

--CREATE TABLE [dbo].[BizFormWizardModule](
--	[numBizFormModuleID] [int] IDENTITY(1,1) NOT NULL,
--	[vcModule] [nvarchar](200) NOT NULL,
-- CONSTRAINT [PK_BizFormWizardModule] PRIMARY KEY CLUSTERED 
--(
--	[numBizFormModuleID] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
--) ON [PRIMARY]

--GO

--INSERT INTO BizFormWizardModule VALUES ('Search (All Modules)')
--INSERT INTO BizFormWizardModule VALUES ('Organizations & Contacts')
--INSERT INTO BizFormWizardModule VALUES ('Projects, Cases, & Tickler')
--INSERT INTO BizFormWizardModule VALUES ('Opportunities, Orders, & BizDocs')
--INSERT INTO BizFormWizardModule VALUES ('Items')
--INSERT INTO BizFormWizardModule VALUES ('E-Commerce')
--INSERT INTO BizFormWizardModule VALUES ('Web to Lead & Survey Forms')
--INSERT INTO BizFormWizardModule VALUES ('Customer & Partner Portal')
--INSERT INTO BizFormWizardModule VALUES ('Custom Relationships')

--/*** Add numBizFormModuleID column to DynamicFormMaster for parent child relationship in BizForm Wizard in domain details ****/

--ALTER TABLE DynamicFormMaster ADD
--numBizFormModuleID INT NULL


--UPDATE DynamicFormMaster SET numBizFormModuleID = 1 WHERE numFormId IN (1,6,15,17,18,29,30,59)
--UPDATE DynamicFormMaster SET numBizFormModuleID = 2 WHERE numFormId IN (56)
--UPDATE DynamicFormMaster SET numBizFormModuleID = 4 WHERE numFormId IN (7,8,58)
--UPDATE DynamicFormMaster SET numBizFormModuleID = 5 WHERE numFormId IN (67)
--UPDATE DynamicFormMaster SET numBizFormModuleID = 6 WHERE numFormId IN (77,84,85)
--UPDATE DynamicFormMaster SET numBizFormModuleID = 7 WHERE numFormId IN (3,5)
--UPDATE DynamicFormMaster SET numBizFormModuleID = 8 WHERE numFormId IN (78,79,80,82)


--INSERT INTO DynamicFormMaster ([numFormId],[vcFormName],[cCustomFieldsAssociated],[cAOIAssociated],[bitDeleted],[tintFlag],numBizFormModuleID) VALUES (99,'Lead Details','Y','N',0,0,2)
--INSERT INTO DynamicFormMaster ([numFormId],[vcFormName],[cCustomFieldsAssociated],[cAOIAssociated],[bitDeleted],[tintFlag],numBizFormModuleID) VALUES (100,'Prospect & Custom Relationships Details','Y','N',0,0,2)
--INSERT INTO DynamicFormMaster ([numFormId],[vcFormName],[cCustomFieldsAssociated],[cAOIAssociated],[bitDeleted],[tintFlag],numBizFormModuleID) VALUES (101,'Account Details','Y','N',0,0,2)
--INSERT INTO DynamicFormMaster ([numFormId],[vcFormName],[cCustomFieldsAssociated],[cAOIAssociated],[bitDeleted],[tintFlag],numBizFormModuleID) VALUES (102,'Contact Details','Y','N',0,0,2)
--INSERT INTO DynamicFormMaster ([numFormId],[vcFormName],[cCustomFieldsAssociated],[cAOIAssociated],[bitDeleted],[tintFlag],numBizFormModuleID) VALUES (103,'Project Details','Y','N',0,0,3)
--INSERT INTO DynamicFormMaster ([numFormId],[vcFormName],[cCustomFieldsAssociated],[cAOIAssociated],[bitDeleted],[tintFlag],numBizFormModuleID) VALUES (104,'Case Details','Y','N',0,0,3)
--INSERT INTO DynamicFormMaster ([numFormId],[vcFormName],[cCustomFieldsAssociated],[cAOIAssociated],[bitDeleted],[tintFlag],numBizFormModuleID) VALUES (105,'Sales Opportunity Details','Y','N',0,0,4)
--INSERT INTO DynamicFormMaster ([numFormId],[vcFormName],[cCustomFieldsAssociated],[cAOIAssociated],[bitDeleted],[tintFlag],numBizFormModuleID) VALUES (106,'Sales Order Details','Y','N',0,0,4)
--INSERT INTO DynamicFormMaster ([numFormId],[vcFormName],[cCustomFieldsAssociated],[cAOIAssociated],[bitDeleted],[tintFlag],numBizFormModuleID) VALUES (107,'Purchase Opportunity Details','Y','N',0,0,4)
--INSERT INTO DynamicFormMaster ([numFormId],[vcFormName],[cCustomFieldsAssociated],[cAOIAssociated],[bitDeleted],[tintFlag],numBizFormModuleID) VALUES (108,'Purchase Order Details','Y','N',0,0,4)
--INSERT INTO DynamicFormMaster ([numFormId],[vcFormName],[cCustomFieldsAssociated],[cAOIAssociated],[bitDeleted],[tintFlag],numBizFormModuleID) VALUES (109,'Lead Grid Columns','Y','N',0,0,2)
--INSERT INTO DynamicFormMaster ([numFormId],[vcFormName],[cCustomFieldsAssociated],[cAOIAssociated],[bitDeleted],[tintFlag],numBizFormModuleID) VALUES (110,'Prospect Grid Columns','Y','N',0,0,2)
--INSERT INTO DynamicFormMaster ([numFormId],[vcFormName],[cCustomFieldsAssociated],[cAOIAssociated],[bitDeleted],[tintFlag],numBizFormModuleID) VALUES (111,'Account Grid Columns','Y','N',0,0,2)
--INSERT INTO DynamicFormMaster ([numFormId],[vcFormName],[cCustomFieldsAssociated],[cAOIAssociated],[bitDeleted],[tintFlag],numBizFormModuleID) VALUES (112,'Vendor Grid Columns','Y','N',0,0,2)
--INSERT INTO DynamicFormMaster ([numFormId],[vcFormName],[cCustomFieldsAssociated],[cAOIAssociated],[bitDeleted],[tintFlag],numBizFormModuleID) VALUES (113,'Employer Grid Columns','Y','N',0,0,2)
--INSERT INTO DynamicFormMaster ([numFormId],[vcFormName],[cCustomFieldsAssociated],[cAOIAssociated],[bitDeleted],[tintFlag],numBizFormModuleID) VALUES (114,'Contact Grid Columns','Y','N',0,0,2)
--INSERT INTO DynamicFormMaster ([numFormId],[vcFormName],[cCustomFieldsAssociated],[cAOIAssociated],[bitDeleted],[tintFlag],numBizFormModuleID) VALUES (115,'Project Grid Columns','Y','N',0,0,3)
--INSERT INTO DynamicFormMaster ([numFormId],[vcFormName],[cCustomFieldsAssociated],[cAOIAssociated],[bitDeleted],[tintFlag],numBizFormModuleID) VALUES (116,'Case Grid Columns','Y','N',0,0,3)
--INSERT INTO DynamicFormMaster ([numFormId],[vcFormName],[cCustomFieldsAssociated],[cAOIAssociated],[bitDeleted],[tintFlag],numBizFormModuleID) VALUES (117,'Tickler Grid Columns','Y','N',0,0,3)
--INSERT INTO DynamicFormMaster ([numFormId],[vcFormName],[cCustomFieldsAssociated],[cAOIAssociated],[bitDeleted],[tintFlag],numBizFormModuleID) VALUES (118,'Sales Opportunity Grid Columns','Y','N',0,0,4)
--INSERT INTO DynamicFormMaster ([numFormId],[vcFormName],[cCustomFieldsAssociated],[cAOIAssociated],[bitDeleted],[tintFlag],numBizFormModuleID) VALUES (119,'Sales Order Grid Columns','Y','N',0,0,4)
--INSERT INTO DynamicFormMaster ([numFormId],[vcFormName],[cCustomFieldsAssociated],[cAOIAssociated],[bitDeleted],[tintFlag],numBizFormModuleID) VALUES (120,'Purchase Opportunity Grid Columns','Y','N',0,0,4)
--INSERT INTO DynamicFormMaster ([numFormId],[vcFormName],[cCustomFieldsAssociated],[cAOIAssociated],[bitDeleted],[tintFlag],numBizFormModuleID) VALUES (121,'Purchase Order Grid Columns','Y','N',0,0,4)
--INSERT INTO DynamicFormMaster ([numFormId],[vcFormName],[cCustomFieldsAssociated],[cAOIAssociated],[bitDeleted],[tintFlag],numBizFormModuleID) VALUES (122,'Prods.& Srvcs.Grid (Opp/Order)','Y','N',0,0,4)

--GO

--/****** Object:  Table [dbo].[BizFormWizardMasterConfiguration]    Script Date: 23-Jul-14 12:17:41 PM ******/
--SET ANSI_NULLS ON
--GO

--SET QUOTED_IDENTIFIER ON
--GO

--CREATE TABLE [dbo].[BizFormWizardMasterConfiguration](
--	[ID] [int] IDENTITY(1,1) NOT NULL,
--	[numFormID] [numeric](18, 0) NOT NULL,
--	[numFieldID] [numeric](18, 0) NOT NULL,
--	[intColumnNum] [int] NOT NULL,
--	[intRowNum] [int] NOT NULL,
--	[numDomainID] [numeric](18, 0) NOT NULL,
--	[numGroupID] [numeric](18, 0) NOT NULL,
--	[numRelCntType] [numeric](18, 0) NOT NULL,
--	[tintPageType] [tinyint] NULL,
--	[bitCustom] [bit] NOT NULL,
--	[bitGridConfiguration] [bit] NOT NULL,
-- CONSTRAINT [PK_BizFormWizardMasterConfiguration] PRIMARY KEY CLUSTERED 
--(
--	[ID] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
--) ON [PRIMARY]

--GO



--------------------------------------------
------ Add/Edit Order Item Grid Columns ----
--------------------------------------------

--	INSERT INTO DynamicFormMaster ([numFormId],[vcFormName],[cCustomFieldsAssociated],[cAOIAssociated],[bitDeleted],[tintFlag]) VALUES (123,'Add/Edit Order - Item Grid Column Settings','Y','N',0,1)

--	---- Maps fields for Add/Edit Order Item Grid Columns

--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 203 AND numFormID = 123)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (4,203,123,1,0,0,1)
--	END
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 193 AND numFormID = 123)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (4,193,123,1,0,0,1)
--	END
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 281 AND numFormID = 123)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (4,281,123,1,0,0,1)
--	END
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 202 AND numFormID = 123)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (4,202,123,1,0,0,1)
--	END
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 215 AND numFormID = 123)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (4,215,123,1,0,0,1)
--	END
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 191 AND numFormID = 123)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (4,191,123,1,0,0,1)
--	END
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 206 AND numFormID = 123)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (4,206,123,1,0,0,1)
--	END
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 207 AND numFormID = 123)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (4,207,123,1,0,0,1)
--	END
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 205 AND numFormID = 123)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (4,205,123,1,0,0,1)
--	END
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 204 AND numFormID = 123)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (4,204,123,1,0,0,1)
--	END
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 209 AND numFormID = 123)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (4,209,123,1,0,0,1)
--	END
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 313 AND numFormID = 123)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (4,313,123,1,0,0,1)
--	END
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 234 AND numFormID = 123)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (4,234,123,1,0,0,1)
--	END
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 212 AND numFormID = 123)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (4,212,123,1,0,0,1)
--	END
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 213 AND numFormID = 123)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (4,213,123,1,0,0,1)
--	END
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 214 AND numFormID = 123)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (4,214,123,1,0,0,1)
--	END
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 216 AND numFormID = 123)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (4,216,123,1,0,0,1)
--	END
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 195 AND numFormID = 123)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (4,195,123,1,0,0,1)
--	END
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 196 AND numFormID = 123)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (4,196,123,1,0,0,1)
--	END
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 197 AND numFormID = 123)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (4,197,123,1,0,0,1)
--	END
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 198 AND numFormID = 123)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (4,198,123,1,0,0,1)
--	END

----------------------------------------
-------- Changed StoreProcedures and Views-------
----------------------------------------

--View_DynamicColumnsMasterConfig
--View_DynamicCustomColumnsMasterConfig

--USP_AccountList1
--USP_CompanyList1
--USP_GetCaseList1
--usp_GetContactInfo
--USP_GetContactList1
--USP_GetDealsList1
--usp_getDynamicFormList
--USP_GetLeadList1
--USP_GetOpportunityList1
--USP_GetProspectsList1
--USP_GEtSFItemsForImporting
--usp_GetTableInfoDefault
--USP_ItemDetails
--USP_LoadWarehouseBasedOnItem
--USP_OPPItemDetails
--USP_ProjectList1
--USP_TicklerActItems

--USP_BizAPI_GetSalesOrderDetail
--USP_BizFormWizardMasterConfiguration_*
--USP_BizFormWizardModule
--USP_ChartofChildAccounts
--USP_CompanyInfo_Search
--USP_Customer_Get_BIZAPI
--USP_GetOrderItems
--USP_Item_GetAll_BizAPI
--USP_Item_Search_BIZAPI
--USP_SalesOrderConfiguration_Save

-------------------------------------------------------------------------------------------------------------------------------------
/* Adds field numOrientation and bitKeepFooterBottom in Table BizDocTemplate to save bizdoc pdf orientation(Portrait OR Landscape) */
-------------------------------------------------------------------------------------------------------------------------------------
--ALTER TABLE BizDocTemplate ADD 
--bitKeepFooterBottom bit NULL,
--numOrientation int NULL
--GO

-------------------------------------------------------------------
/* Adds Net Days field in section 3 of sales order configuration */
-------------------------------------------------------------------
--DECLARE @numFieldID NUMERIC(18,0)

--SELECT	
--	@numFieldID = numFieldId 
--FROM 
--	dycFieldMaster 
--WHERE 
--	numModuleID = 4 AND 
--	vcDbColumnName = 'numBillingDays' AND 
--	vcOrigDbColumnName = 'numBillingDays' AND 
--	vcPropertyName = 'BillingDays' AND
--	vcLookBackTableName = 'DivisionMaster'

--IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = @numFieldID AND numFormID = 93)
--BEGIN
--	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (1,@numFieldID,93,1,0,0)
--END

-------------------------------------------------------------------------------------------------
/* Adds new field bintClosedDate in OpportunityMaster to save close date when order is closed */
-------------------------------------------------------------------------------------------------
--ALTER TABLE OpportunityMaster
--ADD bintClosedDate DATETIME NULL

--/* Updates OpportunityMaster to set bintClosedDate for all closed order */
--UPDATE 
--	OpportunityMaster
--SET
--	bintClosedDate = bintAccountClosingDate
--WHERE 
--	tintshipped = 1

--/* Change the mapping of Close Date to column bintClosedDate from Old column bintAccountClosingDate which sets filed value even if order is not closed  */
--UPDATE
--	DycFieldMaster
--SET
--	vcDbColumnName = 'bintClosedDate',
--	vcOrigDbColumnName = 'bintClosedDate'
--WHERE
--	numFieldId = 117

-----------------------------------------------------------------
/* Add field default payment method in New order configuration */
-----------------------------------------------------------------
--DECLARE @numFieldID NUMERIC(18,0)
--INSERT INTO 
--	DycFieldMaster
--(
--	numModuleID,
--	vcFieldName,
--	vcDbColumnName,
--	vcOrigDbColumnName,
--	vcPropertyName,
--	vcLookBackTableName,
--	vcFieldDataType,
--	vcFieldType,
--	vcAssociatedControlType,
--	vcListItemType,
--	numListID,
--	bitInResults,
--	bitDeleted,
--	bitAllowEdit,
--	bitDefault
--)
--Values
--(
--	1,
--	'Pref. Payment',
--	'numDefaultPaymentMethod',
--	'numDefaultPaymentMethod',
--	'DefaultPaymentMethod',
--	'DivisionMaster',
--	'N',
--	'R',
--	'SelectBox',
--	'LI',
--	31,
--	1,
--	0,
--	0,
--	0
--)

--SELECT @numFieldID = SCOPE_IDENTITY() 
--IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = @numFieldID AND numFormID = 93)
--BEGIN
--	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (1,@numFieldID,93,1,0,0)
--END

----------------------------------------------------------------------
/***************** Unit Price Approval ******************************/
----------------------------------------------------------------------

	--/****** Object:  Table [dbo].[UnitPriceApprover]    Script Date: 6/25/2014 10:20:38 AM ******/
	--SET ANSI_NULLS ON
	--GO

	--SET QUOTED_IDENTIFIER ON
	--GO

	--CREATE TABLE [dbo].[UnitPriceApprover](
	--	[numID] [int] IDENTITY(1,1) NOT NULL,
	--	[numDomainID] [numeric](18, 0) NOT NULL,
	--	[numUserID] [numeric](18, 0) NOT NULL,
	-- CONSTRAINT [PK_UnitPriceApprover] PRIMARY KEY CLUSTERED 
	--(
	--	[numID] ASC
	--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	--) ON [PRIMARY]

	--GO

	--/**** Added Unit Price Approval related fields in domain table ****/

	--ALTER TABLE dbo.Domain ADD
	--	bitMinUnitPriceRule bit NULL,
	--	numAbovePercent numeric(18,2) NULL,
	--	numAbovePriceField numeric(18,0) NULL,
	--	numBelowPercent numeric(18,2) NULL,
	--	numBelowPriceField numeric(18,0) NULL,
	--	numOrderStatusBeforeApproval numeric(18,0) NULL,
	--	numOrderStatusAfterApproval numeric(18,0) NULL
	--GO

	--/**** Added bitItemPriceApprovalRequired field in OpportunityItems for identifying item requiring approval ****/
	--ALTER TABLE dbo.OpportunityItems ADD
	--	bitItemPriceApprovalRequired bit NULL
	--GO

-----------------------------------------
/**** Edit Unit Price Authorization ****/
-----------------------------------------
	--INSERT INTO 
	--	PageMaster 
	--(
	--	numPageID,
	--	numModuleID, 
	--	vcFileName, 
	--	vcPageDesc, 
	--	bitIsViewApplicable, 
	--	bitIsAddApplicable, 
	--	bitIsUpdateApplicable, 
	--	bitIsDeleteApplicable, 
	--	bitIsExportApplicable
	--)
	--VALUES
	--(
	--	(SELECT ISNULL(MAX(numPageID),0) + 1 FROM PageMaster WHERE numModuleID = 10),
	--	10,
	--	'NewOrderOpportunity.aspx',
	--	'Edit Unit Price',
	--	0,
	--	0,
	--	1,
	--	0,
	--	0
	--)

	--DECLARE @newPageID numeric(18,0)
	--SELECT @newPageID = ISNULL(MAX(numPageID),0) FROM PageMaster WHERE numModuleID = 10 

	--/*************************** Allow Edit Unit Price access to all users for all domains *******************************/

	--SELECT * INTO #temp FROM
 --   (
	--	SELECT 
	--		ROW_NUMBER() OVER(ORDER BY numDomainId DESC) AS Row, 
	--		numDomainId 
	--	FROM 
	--		Domain 
	--	WHERE 
	--		numDomainId <> -255
	--) TABLE2
	
	--DECLARE @RowCount INT
	--SET @RowCount = (SELECT COUNT(Row) FROM #temp) 
	
	--DECLARE @I INT
	--DECLARE @numDomainId NUMERIC(18,0);
	
	--SET @I = 1

	--WHILE (@I <= @RowCount)
	--BEGIN
			
	--	SELECT @numDomainId = numDomainID FROM #temp WHERE Row = @I
		
	--	INSERT INTO 
	--		dbo.GroupAuthorization
	--	(
	--		numDomainID,
	--		numModuleID,
	--		numPageID,
	--		numGroupID,
	--		intExportAllowed,
	--		intPrintAllowed,
	--		intViewAllowed,
	--		intAddAllowed,
	--		intUpdateAllowed,
	--		intDeleteAllowed
	--	)
	--	SELECT
	--		@numDomainId,
	--		10,
	--		@newPageID,
	--		numGroupID,
	--		0,
	--		0,
	--		0,
	--		0,
	--		3,
	--		0
	--	FROM
	--		AuthenticationGroupMaster
	--	WHERE
	--		numDomainID = @numDomainId AND
	--		tintGroupType = 1
			
	--	SET @I = @I  + 1
	--END
	
	--DROP TABLE #temp



------------------------------------------
/* Store Procedure Needs to be Changed  */
------------------------------------------
--USP_ReOpenOppertunity -- Issue 1 - New field for order close date
--USP_OppShippingorReceiving -- Issue 1 - New field for order close date
--usp_oppbizdocs -- Issue 14 - BizDoc Orientation
--USP_OPPGetINItems -- Issue 14 - BizDoc Orientation
--USP_GetBizDocTemplate -- Issue 14 - BizDoc Orientation
--USP_ManageBizDocTemplate -- Issue 14 - BizDoc Orientation
--usp_getmasterlistitems -- Issue 8 - Changed to get terms name with its detail
--USP_GEtSFItemsForImporting -- Issue 12 - To get organization phone and shipping items in packing slip from fullfilment
--usp_getcompanyinfodtlpl -- Retrive default payment method of organization
--USP_CheckUserAtLogin -- Unit Price Approver
--USP_GetDomainDetails -- Unit Price Approver
--USP_OPPItemDetails -- Unit Price Approver
--usp_oppmanage -- Unit Price Approver
--usp_updatedomaindetails -- Unit Price Approver
--USP_GetOrderItems -- Unit Price Approver
--USP_ItemUnitPriceApproval_Check -- Unit Price Approver
--USP_ApproveOpportunityItemsPrice -- Unit Price Approver

--USP_Item_SearchForSalesOrder -- Issue of error coming in search for items in purchase opportunity

-----------------------------------
/* Functions Needs to be Created */
-----------------------------------
--CheckIfPriceApplicableToItem
--GetUnitPriceAfterPriceLevelApplication
--GetUnitPriceAfterPriceRuleApplication
--GetUnitPriceForKitOrAssembly

-------------------------------------------------------------------
-------------------------------------------------------------------
-------------------------------------------------------------------

--INSERT  INTO dbo.DycFormField_Mapping
--        ( numModuleID ,
--          numFieldID ,
--          numFormID ,
--          bitAllowEdit ,
--          bitInlineEdit ,
--          vcFieldName ,
--          vcAssociatedControlType ,
--          [order] ,
--          bitInResults ,
--          bitDeleted ,
--          bitDefault ,
--          bitSettingField ,
--          bitAllowFiltering
--        )
--VALUES  ( 1 ,
--          350 ,
--          22 ,
--          1 ,
--          1 ,
--          N'Attributes' ,
--          N'TextBox' ,
--          11 ,
--          1 ,
--          0 ,
--          0 ,
--          1 ,
--          1
--        )



--UPDATE BizAPIThrottlePolicy SET numPerMinute = 60

--usp_SetUsersWithDomains
--USP_UserList
--USP_Item_SearchForSalesOrder

----------------------------------------------------------------
---------------------  Organization Look Ahed ------------------
----------------------------------------------------------------
--BEGIN TRANSACTION

--INSERT INTO DynamicFormMaster ([numFormId],[vcFormName],[cCustomFieldsAssociated],[cAOIAssociated],[bitDeleted],[tintFlag]) VALUES (96,'Customer/Organization Display Fields','Y','N',0,0)
--INSERT INTO DynamicFormMaster ([numFormId],[vcFormName],[cCustomFieldsAssociated],[cAOIAssociated],[bitDeleted],[tintFlag]) VALUES (97,'Customer/Organization Search Fields','Y','N',0,0)

--- Customer/Organization Available Display Fields

--IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 30 AND numFormID = 96)
--BEGIN
--	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (1,30,96,'Organization Status',0,0,0,0,1)
--END
--IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 29 AND numFormID = 96)
--BEGIN
--	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (1,29,96,'Organization Rating',0,0,0,0,1)
--END
--IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 31 AND numFormID = 96)
--BEGIN
--	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (1,31,96,'Organization Credit Limit',0,0,0,0,1)
--END
--IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 5 AND numFormID = 96)
--BEGIN
--	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (1,5,96,'Organization Profile',0,0,0,0,1)
--END
--IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 28 AND numFormID = 96)
--BEGIN
--	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (1,28,96,'Industry',0,0,0,0,1)
--END
--IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 6 AND numFormID = 96)
--BEGIN
--	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (1,6,96,'Relationship',0,0,0,0,1)
--END
--IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 16 AND numFormID = 96)
--BEGIN
--	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (1,16,96,'Annual Revenue',0,0,0,0,1)
--END
--IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 15 AND numFormID = 96)
--BEGIN
--	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (1,15,96,'Employees',0,0,0,0,1)
--END
--IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 10 AND numFormID = 96)
--BEGIN
--	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (1,10,96,'Organization Phone',0,0,0,0,1)
--END
--IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 32 AND numFormID = 96)
--BEGIN
--	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (1,32,96,'Organization Fax',0,0,0,0,1)
--END
--IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 22 AND numFormID = 96)
--BEGIN
--	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (1,22,96,'Lead Source',0,0,0,0,1)
--END
--IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 1 AND numFormID = 96)
--BEGIN
--	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (1,1,96,'Company Differentiation',0,0,0,0,1)
--END
--IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 2 AND numFormID = 96)
--BEGIN
--	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (1,2,96,'Company Differentiation Value',0,0,0,0,1)
--END
--IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 38 AND numFormID = 96)
--BEGIN
--	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (1,38,96,'Organization Comments',0,0,0,0,1)
--END
--IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 17 AND numFormID = 96)
--BEGIN
--	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (1,17,96,'Organization Website',0,0,0,0,1)
--END
--IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 222 AND numFormID = 96)
--BEGIN
--	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (1,222,96,'Bill-to Street',0,0,0,0,1)
--END
--IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 223 AND numFormID = 96)
--BEGIN
--	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (1,223,96,'Bill-to City',0,0,0,0,1)
--END
--IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 224 AND numFormID = 96)
--BEGIN
--	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (1,224,96,'Bill-to State',0,0,0,0,1)
--END
--IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 225 AND numFormID = 96)
--BEGIN
--	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (1,225,96,'Bill-to Postal Code',0,0,0,0,1)
--END
--IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 226 AND numFormID = 96)
--BEGIN
--	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (1,226,96,'Bill-to Country',0,0,0,0,1)
--END
--IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 217 AND numFormID = 96)
--BEGIN
--	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (1,217,96,'Ship-to Street',0,0,0,0,1)
--END
--IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 218 AND numFormID = 96)
--BEGIN
--	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (1,218,96,'Ship-to City',0,0,0,0,1)
--END
--IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 219 AND numFormID = 96)
--BEGIN
--	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (1,219,96,'Ship-to State',0,0,0,0,1)
--END
--IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 220 AND numFormID = 96)
--BEGIN
--	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (1,220,96,'Ship-to Postal Code',0,0,0,0,1)
--END
--IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 221 AND numFormID = 96)
--BEGIN
--	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (1,221,96,'Ship-to Country',0,0,0,0,1)
--END
--IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 21 AND numFormID = 96)
--BEGIN
--	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (1,21,96,'Territory',0,0,0,0,1)
--END

----- Customer/Organization Available Search Fields

--IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID =3  AND numFormID = 97)
--BEGIN
--	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (1,3,97,'Organization Name',0,0,0,0,1)
--END
--IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 30 AND numFormID = 97)
--BEGIN
--	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (1,30,97,'Organization Status',0,0,0,0,1)
--END
--IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 29 AND numFormID = 97)
--BEGIN
--	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (1,29,97,'Organization Rating',0,0,0,0,1)
--END
--IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 31 AND numFormID = 97)
--BEGIN
--	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (1,31,97,'Organization Credit Limit',0,0,0,0,1)
--END
--IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 5 AND numFormID = 97)
--BEGIN
--	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (1,5,97,'Organization Profile',0,0,0,0,1)
--END
--IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 28 AND numFormID = 97)
--BEGIN
--	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (1,28,97,'Industry',0,0,0,0,1)
--END
--IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 6 AND numFormID = 97)
--BEGIN
--	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (1,6,97,'Relationship',0,0,0,0,1)
--END
--IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 16 AND numFormID = 97)
--BEGIN
--	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (1,16,97,'Annual Revenue',0,0,0,0,1)
--END
--IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 15 AND numFormID = 97)
--BEGIN
--	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (1,15,97,'Employees',0,0,0,0,1)
--END
--IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 10 AND numFormID = 97)
--BEGIN
--	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (1,10,97,'Organization Phone',0,0,0,0,1)
--END
--IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 32 AND numFormID = 97)
--BEGIN
--	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (1,32,97,'Organization Fax',0,0,0,0,1)
--END
--IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 22 AND numFormID = 97)
--BEGIN
--	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (1,22,97,'Lead Source',0,0,0,0,1)
--END
--IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 1 AND numFormID = 97)
--BEGIN
--	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (1,1,97,'Company Differentiation',0,0,0,0,1)
--END
--IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 2 AND numFormID = 97)
--BEGIN
--	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (1,2,97,'Company Differentiation Value',0,0,0,0,1)
--END
--IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 38 AND numFormID = 97)
--BEGIN
--	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (1,38,97,'Organization Comments',0,0,0,0,1)
--END
--IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 17 AND numFormID = 97)
--BEGIN
--	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (1,17,97,'Organization Website',0,0,0,0,1)
--END
--IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 222 AND numFormID = 97)
--BEGIN
--	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (1,222,97,'Bill-to Street',0,0,0,0,1)
--END
--IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 223 AND numFormID = 97)
--BEGIN
--	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (1,223,97,'Bill-to City',0,0,0,0,1)
--END
--IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 224 AND numFormID = 97)
--BEGIN
--	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (1,224,97,'Bill-to State',0,0,0,0,1)
--END
--IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 225 AND numFormID = 97)
--BEGIN
--	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (1,225,97,'Bill-to Postal Code',0,0,0,0,1)
--END
--IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 226 AND numFormID = 97)
--BEGIN
--	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (1,226,97,'Bill-to Country',0,0,0,0,1)
--END
--IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 217 AND numFormID = 97)
--BEGIN
--	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (1,217,97,'Ship-to Street',0,0,0,0,1)
--END
--IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 218 AND numFormID = 97)
--BEGIN
--	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (1,218,97,'Ship-to City',0,0,0,0,1)
--END
--IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 219 AND numFormID = 97)
--BEGIN
--	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (1,219,97,'Ship-to State',0,0,0,0,1)
--END
--IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 220 AND numFormID = 97)
--BEGIN
--	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (1,220,97,'Ship-to Postal Code',0,0,0,0,1)
--END
--IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 221 AND numFormID = 97)
--BEGIN
--	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (1,221,97,'Ship-to Country',0,0,0,0,1)
--END
--IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 21 AND numFormID = 97)
--BEGIN
--	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (1,21,97,'Territory',0,0,0,0,1)
--END
--IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 2 AND numFieldID = 51 AND numFormID = 97)
--BEGIN
--	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (2,51,97,'Contact First Name',0,0,0,0,1)
--END
--IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 2 AND numFieldID = 52 AND numFormID = 97)
--BEGIN
--	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (2,52,97,'Contact Last Name',0,0,0,0,1)
--END
--IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 5 AND numFieldID = 148 AND numFormID = 97)
--BEGIN
--	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (5,148,97,'Project Name',0,0,0,0,1)
--END
--IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 7 AND numFieldID = 127 AND numFormID = 97)
--BEGIN
--	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (7,127,97,'Case Name',0,0,0,0,1)
--END
--IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 3 AND numFieldID = 96 AND numFormID = 97)
--BEGIN
--	INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[vcFieldName],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit],[bitSettingField]) VALUES (3,96,97,'Order Name',0,0,0,0,1)
--END


------------------------------------------------------------------
-----------  New Sales Order Form --------
------------------------------------------------------------------

--DECLARE @NewFieldID INTEGER
----- Inserts Dynamic Forms for creating new inventory, non - invemtory and serialized items

--	INSERT INTO DynamicFormMaster ([numFormId],[vcFormName],[cCustomFieldsAssociated],[cAOIAssociated],[bitDeleted],[tintFlag]) VALUES (89,'New Order','Y','N',0,0)
--	INSERT INTO DynamicFormMaster ([numFormId],[vcFormName],[cCustomFieldsAssociated],[cAOIAssociated],[bitDeleted],[tintFlag]) VALUES (90,'New Order - Order Details Settings','Y','N',0,1)
--	INSERT INTO DynamicFormMaster ([numFormId],[vcFormName],[cCustomFieldsAssociated],[cAOIAssociated],[bitDeleted],[tintFlag]) VALUES (91,'New Order - Item Details Settings','Y','N',0,1)
--	INSERT INTO DynamicFormMaster ([numFormId],[vcFormName],[cCustomFieldsAssociated],[cAOIAssociated],[bitDeleted],[tintFlag]) VALUES (92,'New Order - Item Column Settings','Y','N',0,1)
--	INSERT INTO DynamicFormMaster ([numFormId],[vcFormName],[cCustomFieldsAssociated],[cAOIAssociated],[bitDeleted],[tintFlag]) VALUES (93,'New Order - Customer Detail Settings','Y','N',0,1)

---- Add On Credit Hold Field in DycFieldMaster

--INSERT INTO dbo.DycFieldMaster 
--(		
--	numModuleID ,
--	numDomainID ,
--	vcFieldName ,
--	vcDbColumnName ,
--	vcOrigDbColumnName ,
--	vcPropertyName ,
--	vcLookBackTableName ,
--	vcFieldDataType ,
--	vcFieldType ,
--	vcAssociatedControlType ,
--	vcToolTip ,
--	vcListItemType ,
--	numListID ,
--	PopupFunctionName ,
--	[order] ,
--	tintRow ,
--	tintColumn ,
--	bitInResults ,
--	bitDeleted ,
--	bitAllowEdit ,
--	bitDefault ,
--	bitSettingField ,
--	bitAddField ,
--	bitDetailField ,
--	bitAllowSorting ,
--	bitWorkFlowField ,
--	bitImport ,
--	bitExport ,
--	bitAllowFiltering ,
--	bitInlineEdit ,
--	bitRequired ,
--	intColumnWidth ,
--	intFieldMaxLength
--)
--VALUES  
--( 
--	1 , 
--	NULL,
--	N'On Credit Hold' ,
--	N'bitOnCreditHold' , 
--	N'bitOnCreditHold' , 
--	'OnCreditHold' ,
--	N'DivisionMaster' ,
--	'Y' , 
--	'R' , 
--	N'CheckBox' ,
--	NULL ,
--	'' , 
--	0 ,
--	NULL ,
--	0 ,
--	0 , 
--	0 , 
--	1 , 
--	0 ,
--	1 , 
--	0 ,
--	0 , 
--	1 , 
--	1 ,
--	0 , 
--	1 , 
--	NULL , 
--	NULL ,
--	NULL ,
--	NULL , 
--	NULL , 
--	NULL , 
--	NULL  
--)

--SELECT @NewFieldID = SCOPE_IDENTITY() 

---- Following script creates tree menu New Item in Field Management Section

--	DECLARE @numPageNavID numeric 
--	SELECT @numPageNavID = MAX(numPageNavID) + 1  FROM dbo.PageNavigationDTL 
--	INSERT INTO dbo.PageNavigationDTL
--			( numPageNavID ,
--			  numModuleID ,
--			  numParentID ,
--			  vcPageNavName ,
--			  vcNavURL ,
--			  vcImageURL ,
--			  bitVisible ,
--			  numTabID
--			)
--	SELECT @numPageNavID ,13,109,'New Order','../opportunity/frmConfNewOrder.aspx',NULL,1,-1
	
---------------------------------------------------------------------------------
------------- Start - Give permission for each domain to tree node --------------
---------------------------------------------------------------------------------
	
--	SELECT * INTO #temp FROM
--    (
--		SELECT 
--			ROW_NUMBER() OVER(ORDER BY numDomainId DESC) AS Row, 
--			numDomainId 
--		FROM 
--			Domain 
--		WHERE 
--			numDomainId <> -255
--	) TABLE2
	
--	DECLARE @RowCount INT
--	SET @RowCount = (SELECT COUNT(Row) FROM #temp) 
	
--	DECLARE @I INT
--	DECLARE @numDomainId NUMERIC(18,0);
	
--	SET @I = 1

--	WHILE (@I <= @RowCount)
--	BEGIN
			
--		SELECT @numDomainId = numDomainID FROM #temp WHERE Row = @I
		
--		DELETE FROM TreeNavigationAuthorization WHERE numPageNavID=@numPageNavID AND numDomainID = @numDomainId

--		SELECT * INTO #tempData1 FROM
--		(
--		 SELECT    T.numTabID,
--						CASE WHEN bitFixed = 1
--							 THEN CASE WHEN EXISTS ( SELECT numTabName
--													 FROM   TabDefault
--													 WHERE  numTabId = T.numTabId
--															AND tintTabType = T.tintTabType AND numDomainID =D.numDomainID)
--									   THEN ( SELECT TOP 1
--														numTabName
--											  FROM      TabDefault
--											  WHERE     numTabId = T.numTabId
--														AND tintTabType = T.tintTabType AND numDomainID = D.numDomainID
--											)
--									   ELSE T.numTabName
--								  END
--							 ELSE T.numTabName
--						END numTabname,
--						vcURL,
--						bitFixed,
--						1 AS tintType,
--						T.vcImage,
--						D.numDomainID,
--						A.numGroupID
--			  FROM      TabMaster T
--						CROSS JOIN Domain D --ON D.numDomainID = T.numDomainID
--						LEFT JOIN AuthenticationGroupMaster A ON A.numDomainID = D.numDomainId
--			  WHERE     bitFixed = 1
--						AND D.numDomainId <> -255
--						AND t.tintTabType = 1
	          
--			  UNION ALL
--			  SELECT    -1 AS [numTabID],
--						'Administration' numTabname,
--						'' vcURL,
--						1 bitFixed,
--						1 AS tintType,
--						'' AS vcImage,
--						D.numDomainID,
--						A.numGroupID
--			  FROM      Domain D
--						JOIN AuthenticationGroupMaster A ON A.numDomainID = D.numDomainId
--						WHERE D.numDomainID = @numDomainID
			  
--			  UNION ALL
--			  SELECT    -3 AS [numTabID],
--						'Advanced Search' numTabname,
--						'' vcURL,
--						1 bitFixed,
--						1 AS tintType,
--						'' AS vcImage,
--						D.numDomainID,
--						A.numGroupID
--			  FROM      Domain D
--						JOIN AuthenticationGroupMaster A ON A.numDomainID = D.numDomainId          
--			) TABLE2       		
	                    
--		INSERT  INTO dbo.TreeNavigationAuthorization
--			(
--			  numGroupID,
--			  numTabID,
--			  numPageNavID,
--			  bitVisible,
--			  numDomainID,
--			  tintType
--			)
--			SELECT  numGroupID,
--					PND.numTabID,
--					numPageNavID,
--					bitVisible,
--					numDomainID,
--					tintType
--			FROM    dbo.PageNavigationDTL PND
--					JOIN #tempData1 TD ON TD.numTabID = PND.numTabID 
--					WHERE PND.numPageNavID= @numPageNavID
			
--			DROP TABLE #tempData1
			
--			SET @I = @I  + 1
--	END
	
--	DROP TABLE #temp


-------- Maps fields for New Order - Order Details Settings

--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 3 AND numFieldID = 116 AND numFormID = 90)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (3,116,90,1,1,1)
--	END
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 3 AND numFieldID = 97 AND numFormID = 90)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (3,97,90,1,1,1)
--	END
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 3 AND numFieldID = 122 AND numFormID = 90)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (3,122,90,1,1,1)
--	END
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 3 AND numFieldID = 101 AND numFormID = 90)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (3,101,90,1,1,1)
--	END


-------- Maps fields for New Order - Item Details Settings

--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 189 AND numFormID = 91)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,189,91,1,0,0)
--	END
	
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 193 AND numFormID = 91)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,193,91,1,0,0)
--	END
	
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 202 AND numFormID = 91)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,202,91,1,0,0)
--	END
	
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 273 AND numFormID = 91)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,273,91,1,0,0)
--	END
	
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 274 AND numFormID = 91)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,274,91,1,0,0)
--	END
	
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 277 AND numFormID = 91)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,277,91,1,0,0)
--	END
	
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 399 AND numFormID = 91)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,399,91,1,0,0)
--	END
	
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 215 AND numFormID = 91)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,215,91,1,0,0)
--	END
	
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 191 AND numFormID = 91)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,191,91,1,0,0)
--	END
	
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 206 AND numFormID = 91)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,206,91,1,0,0)
--	END
	
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 204 AND numFormID = 91)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,204,91,1,0,0)
--	END
	
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 207 AND numFormID = 91)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,207,91,1,0,0)
--	END
	
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 205 AND numFormID = 91)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,205,91,1,0,0)
--	END
	
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 276 AND numFormID = 91)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,276,91,1,0,0)
--	END
	
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 275 AND numFormID = 91)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,275,91,1,0,0)
--	END
	
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 280 AND numFormID = 91)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,280,91,1,0,0)
--	END
	
------ Maps fields for New Order - Item Column Settings

--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 203 AND numFormID = 92)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,203,92,1,0,0)
--	END
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 193 AND numFormID = 92)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,193,92,1,0,0)
--	END
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 281 AND numFormID = 92)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,281,92,1,0,0)
--	END
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 202 AND numFormID = 92)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,202,92,1,0,0)
--	END
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 215 AND numFormID = 92)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,215,92,1,0,0)
--	END
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 191 AND numFormID = 92)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,191,92,1,0,0)
--	END
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 206 AND numFormID = 92)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,206,92,1,0,0)
--	END
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 207 AND numFormID = 92)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,207,92,1,0,0)
--	END
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 205 AND numFormID = 92)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,205,92,1,0,0)
--	END
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 204 AND numFormID = 92)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,204,92,1,0,0)
--	END
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 209 AND numFormID = 92)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,209,92,1,0,0)
--	END
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 313 AND numFormID = 92)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,313,92,1,0,0)
--	END
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 234 AND numFormID = 92)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,234,92,1,0,0)
--	END
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 212 AND numFormID = 92)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,212,92,1,0,0)
--	END
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 213 AND numFormID = 92)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,213,92,1,0,0)
--	END
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 214 AND numFormID = 92)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,214,92,1,0,0)
--	END
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 216 AND numFormID = 92)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,216,92,1,0,0)
--	END
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 195 AND numFormID = 92)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,195,92,1,0,0)
--	END
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 196 AND numFormID = 92)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,196,92,1,0,0)
--	END
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 197 AND numFormID = 92)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,197,92,1,0,0)
--	END
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 198 AND numFormID = 92)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,198,92,1,0,0)
--	END
	
-------- Maps fields for New Order - Customer Details Settings
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 1 AND numFormID = 93)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (1,1,93,1,0,0)
--	END
	
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 5 AND numFormID = 93)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (1,5,93,1,0,0)
--	END
	
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 6 AND numFormID = 93)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (1,6,93,1,0,0)
--	END
	
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 7 AND numFormID = 93)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (1,7,93,1,0,0)
--	END
	
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 10 AND numFormID = 93)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (1,10,93,1,0,0)
--	END
	
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 14 AND numFormID = 93)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (1,14,93,1,0,0)
--	END
	
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 15 AND numFormID = 93)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (1,15,93,1,0,0)
--	END
	
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 16 AND numFormID = 93)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (1,16,93,1,0,0)
--	END
	
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 18 AND numFormID = 93)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (1,18,93,1,0,0)
--	END
	
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 21 AND numFormID = 93)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (1,21,93,1,0,0)
--	END
	
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 22 AND numFormID = 93)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (1,22,93,1,0,0)
--	END
	
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 28 AND numFormID = 93)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (1,28,93,1,0,0)
--	END
	
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 29 AND numFormID = 93)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (1,29,93,1,0,0)
--	END
	
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 30 AND numFormID = 93)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (1,30,93,1,0,0)
--	END
	
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 32 AND numFormID = 93)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (1,32,93,1,0,0)
--	END
	
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 38 AND numFormID = 93)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (1,38,93,1,0,0)
--	END
	
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 40 AND numFormID = 93)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (1,40,93,1,0,0)
--	END
	
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = 41 AND numFormID = 93)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (1,41,93,1,0,0)
--	END
	
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 1 AND numFieldID = @NewFieldID AND numFormID = 93)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (1,@NewFieldID,93,1,0,0)
--	END
	
----- Change Sales Order Form Shortcut
	
--	UPDATE
--		ShortCutBar
--	SET	
--		NewLink = '../opportunity/frmNewOrder.aspx'
--	WHERE
--		vcLinkName = 'Sales Orders' AND id = 115
		
---- Update dycfield vc property		
--		UPDATE 
--			dycFieldMaster
--		SET
--			vcPropertyName = 'OnHand'
--		WHERE 
--			numFieldId = 195
			
--		UPDATE 
--			dycFieldMaster
--		SET
--			vcPropertyName = 'OnOrder'
--		WHERE 
--			numFieldId = 196
			
--		UPDATE 
--			dycFieldMaster
--		SET
--			vcPropertyName = 'OnAllocation'
--		WHERE 
--			numFieldId = 197
			
--		UPDATE 
--			dycFieldMaster
--		SET
--			vcPropertyName = 'BackOrder'
--		WHERE 
--			numFieldId = 198

----TABLE

--GO
--/****** Object:  Table [dbo].[SalesOrderConfiguration]    Script Date: 05/30/2014 13:53:47 ******/
--SET ANSI_NULLS ON
--GO
--SET QUOTED_IDENTIFIER ON
--GO
--SET ANSI_PADDING ON
--GO
--CREATE TABLE [dbo].[SalesOrderConfiguration](
--	[numSOCID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
--	[numDomainID] [numeric](18, 0) NOT NULL,
--	[bitAutoFocusCustomer] [bit] NULL,
--	[bitAutoFocusItem] [bit] NULL,
--	[bitDisplayRootLocation] [bit] NULL,
--	[bitAutoAssignOrder] [bit] NULL,
--	[bitDisplayLocation] [bit] NULL,
--	[bitDisplayFinancialStamp] [bit] NULL,
--	[bitDisplayItemDetails] [bit] NULL,
--	[bitDisplayUnitCost] [bit] NULL,
--	[bitDisplayProfitTotal] [bit] NULL,
--	[bitDisplayShippingRates] [bit] NULL,
--	[bitDisplayPaymentMethods] [bit] NULL,
--	[bitCreateOpenBizDoc] [bit] NULL,
--	[numListItemID] [numeric](18, 0) NULL,
--	[vcPaymentMethodIDs] [varchar](100) NULL,
-- CONSTRAINT [PK_SalesOrderConfiguration] PRIMARY KEY CLUSTERED 
--(
--	[numSOCID] ASC
--)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
--) ON [PRIMARY]

--GO
--SET ANSI_PADDING ON



--ROLLBACK



----------------------------------------------------------------
---------  Administration - Field Management - New Item --------
----------------------------------------------------------------

--BEGIN TRANSACTION

	
--	--- Inserts Dynamic Forms for creating new inventory, non - invemtory and serialized items
	
--	INSERT INTO DynamicFormMaster ([numFormId],[vcFormName],[cCustomFieldsAssociated],[cAOIAssociated],[bitDeleted],[tintFlag]) VALUES (86,'Inventory Item','Y','N',0,0)
--	INSERT INTO DynamicFormMaster ([numFormId],[vcFormName],[cCustomFieldsAssociated],[cAOIAssociated],[bitDeleted],[tintFlag]) VALUES (87,'Non-Inventory Item','Y','N',0,0)
--	INSERT INTO DynamicFormMaster ([numFormId],[vcFormName],[cCustomFieldsAssociated],[cAOIAssociated],[bitDeleted],[tintFlag]) VALUES (88,'Serialized Item','Y','N',0,0)

--	-- Following script creates tree menu New Item in Field Management Section

--	DECLARE @numPageNavID numeric 
--	SELECT @numPageNavID = MAX(numPageNavID) + 1  FROM dbo.PageNavigationDTL 
--	INSERT INTO dbo.PageNavigationDTL
--			( numPageNavID ,
--			  numModuleID ,
--			  numParentID ,
--			  vcPageNavName ,
--			  vcNavURL ,
--			  vcImageURL ,
--			  bitVisible ,
--			  numTabID
--			)
--	SELECT @numPageNavID ,13,109,'New Item','../pagelayout/frmCustomisePageLayout.aspx?Ctype=I&type=1&PType=2&FormId=86',NULL,1,-1
	

--	DECLARE CursorTreeNavigation CURSOR FOR
--	SELECT numDomainId FROM Domain WHERE numDomainId <> -255
	
--	OPEN CursorTreeNavigation;

--	DECLARE @numDomainId NUMERIC(18,0);

--	FETCH NEXT FROM CursorTreeNavigation INTO @numDomainId;
--	WHILE (@@FETCH_STATUS = 0)
--	BEGIN
	  
--	   ---- Give permission for each domain to tree node
	   
--   		DELETE FROM TreeNavigationAuthorization WHERE numPageNavID=@numPageNavID AND numDomainID = @numDomainId
	
--		SELECT * INTO #tempData1 FROM
--		(
--		 SELECT    T.numTabID,
--                        CASE WHEN bitFixed = 1
--                             THEN CASE WHEN EXISTS ( SELECT numTabName
--                                                     FROM   TabDefault
--                                                     WHERE  numTabId = T.numTabId
--                                                            AND tintTabType = T.tintTabType AND numDomainID =D.numDomainID)
--                                       THEN ( SELECT TOP 1
--                                                        numTabName
--                                              FROM      TabDefault
--                                              WHERE     numTabId = T.numTabId
--                                                        AND tintTabType = T.tintTabType AND numDomainID = D.numDomainID
--                                            )
--                                       ELSE T.numTabName
--                                  END
--                             ELSE T.numTabName
--                        END numTabname,
--                        vcURL,
--                        bitFixed,
--                        1 AS tintType,
--                        T.vcImage,
--                        D.numDomainID,
--                        A.numGroupID
--              FROM      TabMaster T
--                        CROSS JOIN Domain D --ON D.numDomainID = T.numDomainID
--                        LEFT JOIN AuthenticationGroupMaster A ON A.numDomainID = D.numDomainId
--              WHERE     bitFixed = 1
--                        AND D.numDomainId <> -255
--                        AND t.tintTabType = 1
              
--              UNION ALL
--              SELECT    -1 AS [numTabID],
--                        'Administration' numTabname,
--                        '' vcURL,
--                        1 bitFixed,
--                        1 AS tintType,
--                        '' AS vcImage,
--                        D.numDomainID,
--                        A.numGroupID
--              FROM      Domain D
--                        JOIN AuthenticationGroupMaster A ON A.numDomainID = D.numDomainId
--                        WHERE D.numDomainID = @numDomainID
			  
--			  UNION ALL
--			  SELECT    -3 AS [numTabID],
--						'Advanced Search' numTabname,
--						'' vcURL,
--						1 bitFixed,
--						1 AS tintType,
--						'' AS vcImage,
--						D.numDomainID,
--						A.numGroupID
--			  FROM      Domain D
--						JOIN AuthenticationGroupMaster A ON A.numDomainID = D.numDomainId          
--            ) TABLE2       		
                        
--		INSERT  INTO dbo.TreeNavigationAuthorization
--            (
--              numGroupID,
--              numTabID,
--              numPageNavID,
--              bitVisible,
--              numDomainID,
--              tintType
--            )
--            SELECT  numGroupID,
--                    PND.numTabID,
--                    numPageNavID,
--                    bitVisible,
--                    numDomainID,
--                    tintType
--            FROM    dbo.PageNavigationDTL PND
--                    JOIN #tempData1 TD ON TD.numTabID = PND.numTabID 
--                    WHERE PND.numPageNavID= @numPageNavID
		
--          DROP TABLE #tempData1
	   
	   
--	    ---- Inserts required field mapping in DycFormConfigurationDetails table so it can not be removed form configuration wizard for new inventory item form
--	    IF NOT EXISTS (SELECT * FROM DycFormConfigurationDetails WHERE numFormId = 86 AND numFieldId = 189 AND numDomainId = @numDomainId)
--	    BEGIN
--			INSERT INTO DycFormConfigurationDetails ([numFormId],[numFieldId],[intColumnNum],[intRowNum],[numDomainId],[numUserCntID],[numRelCntType],[tintPageType],[bitCustom]) VALUES (86,189,1,1,@numDomainId,0,0,2,0)
--		END
--		IF NOT EXISTS (SELECT * FROM DycFormConfigurationDetails WHERE numFormId = 86 AND numFieldId = 270 AND numDomainId = @numDomainId)
--	    BEGIN
--			INSERT INTO DycFormConfigurationDetails ([numFormId],[numFieldId],[intColumnNum],[intRowNum],[numDomainId],[numUserCntID],[numRelCntType],[tintPageType],[bitCustom]) VALUES (86,270,1,2,@numDomainId,0,0,2,0)
--		END
--		IF NOT EXISTS (SELECT * FROM DycFormConfigurationDetails WHERE numFormId = 86 AND numFieldId = 271 AND numDomainId = @numDomainId)
--	    BEGIN
--			INSERT INTO DycFormConfigurationDetails ([numFormId],[numFieldId],[intColumnNum],[intRowNum],[numDomainId],[numUserCntID],[numRelCntType],[tintPageType],[bitCustom]) VALUES (86,271,2,1,@numDomainId,0,0,2,0)
--		END
--		IF NOT EXISTS (SELECT * FROM DycFormConfigurationDetails WHERE numFormId = 86 AND numFieldId = 272 AND numDomainId = @numDomainId)
--	    BEGIN
--			INSERT INTO DycFormConfigurationDetails ([numFormId],[numFieldId],[intColumnNum],[intRowNum],[numDomainId],[numUserCntID],[numRelCntType],[tintPageType],[bitCustom]) VALUES (86,272,2,2,@numDomainId,0,0,2,0)
--		END
		
--		--- Inserts required field mapping in DynamicFormField_Validation for validation on form new inventory item form
--		IF NOT EXISTS (SELECT * FROM DynamicFormField_Validation WHERE numFormId = 86 AND numFormFieldId = 189 AND numDomainId = @numDomainId)
--		BEGIN
--			INSERT INTO DynamicFormField_Validation (numFormId,numFormFieldId,numDomainId,vcNewFormFieldName,bitIsRequired) VALUES (86,189,@numDomainId,'Item',1)
--		END
--		IF NOT EXISTS (SELECT * FROM DynamicFormField_Validation WHERE numFormId = 86 AND numFormFieldId = 270 AND numDomainId = @numDomainId)
--		BEGIN
--			INSERT INTO DynamicFormField_Validation (numFormId,numFormFieldId,numDomainId,vcNewFormFieldName,bitIsRequired) VALUES (86,270,@numDomainId,'Income Account',1)
--		END
--		IF NOT EXISTS (SELECT * FROM DynamicFormField_Validation WHERE numFormId = 86 AND numFormFieldId = 271 AND numDomainId = @numDomainId)
--		BEGIN
--			INSERT INTO DynamicFormField_Validation (numFormId,numFormFieldId,numDomainId,vcNewFormFieldName,bitIsRequired) VALUES (86,271,@numDomainId,'Asset Account',1)
--		END
--		IF NOT EXISTS (SELECT * FROM DynamicFormField_Validation WHERE numFormId = 86 AND numFormFieldId = 272 AND numDomainId = @numDomainId)
--		BEGIN
--			INSERT INTO DynamicFormField_Validation (numFormId,numFormFieldId,numDomainId,vcNewFormFieldName,bitIsRequired) VALUES (86,272,@numDomainId,'COGS Account',1)
--		END
		
--		---- Inserts required field mapping in DycFormConfigurationDetails table so it can not be removed form configuration wizard for new non inventory item form
--		IF NOT EXISTS (SELECT * FROM DycFormConfigurationDetails WHERE numFormId = 87 AND numFieldId = 189 AND numDomainId = @numDomainId)
--	    BEGIN
--			INSERT INTO DycFormConfigurationDetails ([numFormId],[numFieldId],[intColumnNum],[intRowNum],[numDomainId],[numUserCntID],[numRelCntType],[tintPageType],[bitCustom]) VALUES (87,189,1,1,@numDomainId,0,0,2,0)
--		END
--		IF NOT EXISTS (SELECT * FROM DycFormConfigurationDetails WHERE numFormId = 87 AND numFieldId = 270 AND numDomainId = @numDomainId)
--	    BEGIN
--			INSERT INTO DycFormConfigurationDetails ([numFormId],[numFieldId],[intColumnNum],[intRowNum],[numDomainId],[numUserCntID],[numRelCntType],[tintPageType],[bitCustom]) VALUES (87,270,1,2,@numDomainId,0,0,2,0)
--		END
--		IF NOT EXISTS (SELECT * FROM DycFormConfigurationDetails WHERE numFormId = 87 AND numFieldId = 272 AND numDomainId = @numDomainId)
--			BEGIN
--		INSERT INTO DycFormConfigurationDetails ([numFormId],[numFieldId],[intColumnNum],[intRowNum],[numDomainId],[numUserCntID],[numRelCntType],[tintPageType],[bitCustom]) VALUES (87,272,2,1,@numDomainId,0,0,2,0)
--		END
		
--		--- Inserts resuired field mapping in DynamicFormField_Validation for validation on form
--		IF NOT EXISTS (SELECT * FROM DynamicFormField_Validation WHERE numFormId = 87 AND numFormFieldId = 189 AND numDomainId = @numDomainId)
--	    BEGIN
--			INSERT INTO DynamicFormField_Validation (numFormId,numFormFieldId,numDomainId,vcNewFormFieldName,bitIsRequired) VALUES (87,189,@numDomainId,'Item',1)
--		END
--		IF NOT EXISTS (SELECT * FROM DynamicFormField_Validation WHERE numFormId = 87 AND numFormFieldId = 270 AND numDomainId = @numDomainId)
--	    BEGIN
--			INSERT INTO DynamicFormField_Validation (numFormId,numFormFieldId,numDomainId,vcNewFormFieldName,bitIsRequired) VALUES (87,270,@numDomainId,'Income Account',1)
--		END
--		IF NOT EXISTS (SELECT * FROM DynamicFormField_Validation WHERE numFormId = 87 AND numFormFieldId = 272 AND numDomainId = @numDomainId)
--	    BEGIN
--			INSERT INTO DynamicFormField_Validation (numFormId,numFormFieldId,numDomainId,vcNewFormFieldName,bitIsRequired) VALUES (87,272,@numDomainId,'COGS Account',1)
--		END
		
--		 ---- Inserts required field mapping in DycFormConfigurationDetails table so it can not be removed form configuration wizard for new service/lot item form
--		 IF NOT EXISTS (SELECT * FROM DycFormConfigurationDetails WHERE numFormId = 88 AND numFieldId = 189 AND numDomainId = @numDomainId)
--	    BEGIN
--			INSERT INTO DycFormConfigurationDetails ([numFormId],[numFieldId],[intColumnNum],[intRowNum],[numDomainId],[numUserCntID],[numRelCntType],[tintPageType],[bitCustom]) VALUES (88,189,1,1,@numDomainId,0,0,2,0)
--		END
--		IF NOT EXISTS (SELECT * FROM DycFormConfigurationDetails WHERE numFormId = 88 AND numFieldId = 270 AND numDomainId = @numDomainId)
--	    BEGIN
--			INSERT INTO DycFormConfigurationDetails ([numFormId],[numFieldId],[intColumnNum],[intRowNum],[numDomainId],[numUserCntID],[numRelCntType],[tintPageType],[bitCustom]) VALUES (88,270,1,2,@numDomainId,0,0,2,0)
--		END
--		IF NOT EXISTS (SELECT * FROM DycFormConfigurationDetails WHERE numFormId = 88 AND numFieldId = 271 AND numDomainId = @numDomainId)
--	    BEGIN
--			INSERT INTO DycFormConfigurationDetails ([numFormId],[numFieldId],[intColumnNum],[intRowNum],[numDomainId],[numUserCntID],[numRelCntType],[tintPageType],[bitCustom]) VALUES (88,271,2,1,@numDomainId,0,0,2,0)
--		END
--		IF NOT EXISTS (SELECT * FROM DycFormConfigurationDetails WHERE numFormId = 88 AND numFieldId = 272 AND numDomainId = @numDomainId)
--	    BEGIN
--			INSERT INTO DycFormConfigurationDetails ([numFormId],[numFieldId],[intColumnNum],[intRowNum],[numDomainId],[numUserCntID],[numRelCntType],[tintPageType],[bitCustom]) VALUES (88,272,2,2,@numDomainId,0,0,2,0)
--		END
		
--		--- Inserts required field mapping in DynamicFormField_Validation for validation on form new service lot item form
--		IF NOT EXISTS (SELECT * FROM DynamicFormField_Validation WHERE numFormId = 88 AND numFormFieldId = 189 AND numDomainId = @numDomainId)
--		BEGIN
--			INSERT INTO DynamicFormField_Validation (numFormId,numFormFieldId,numDomainId,vcNewFormFieldName,bitIsRequired) VALUES (88,189,@numDomainId,'Item',1)
--		END
--		IF NOT EXISTS (SELECT * FROM DynamicFormField_Validation WHERE numFormId = 88 AND numFormFieldId = 270 AND numDomainId = @numDomainId)
--		BEGIN
--			INSERT INTO DynamicFormField_Validation (numFormId,numFormFieldId,numDomainId,vcNewFormFieldName,bitIsRequired) VALUES (88,270,@numDomainId,'Income Account',1)
--		END
--		IF NOT EXISTS (SELECT * FROM DynamicFormField_Validation WHERE numFormId = 88 AND numFormFieldId = 271 AND numDomainId = @numDomainId)
--		BEGIN
--			INSERT INTO DynamicFormField_Validation (numFormId,numFormFieldId,numDomainId,vcNewFormFieldName,bitIsRequired) VALUES (88,271,@numDomainId,'Asset Account',1)
--		END
--		IF NOT EXISTS (SELECT * FROM DynamicFormField_Validation WHERE numFormId = 88 AND numFormFieldId = 272 AND numDomainId = @numDomainId)
--		BEGIN
--			INSERT INTO DynamicFormField_Validation (numFormId,numFormFieldId,numDomainId,vcNewFormFieldName,bitIsRequired) VALUES (88,272,@numDomainId,'COGS Account',1)
--		END
	   
--	   FETCH NEXT FROM CursorTreeNavigation INTO @numDomainId;
--	END;

--	CLOSE CursorTreeNavigation;
--	DEALLOCATE CursorTreeNavigation;

------ Maps fields with new inventory item form

--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 189 AND numFormID = 86)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit]) VALUES (4,189,86,1,1,1,1)
--	END
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 270 AND numFormID = 86)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit]) VALUES (4,270,86,1,1,1,1)
--	END
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 271 AND numFormID = 86)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit]) VALUES (4,271,86,1,1,1,1)
--	END
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 272 AND numFormID = 86)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit]) VALUES (4,272,86,1,1,1,1)
--	END
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 216 AND numFormID = 86)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,216,86,1,1,1)
--	END
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 212 AND numFormID = 86)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,212,86,1,1,1)
--	END
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 203 AND numFormID = 86)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,203,86,1,1,1)
--	END
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 273 AND numFormID = 86)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,273,86,1,1,1)
--	END
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 193 AND numFormID = 86)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,193,86,1,1,1)
--	END
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 281 AND numFormID = 86)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,281,86,1,1,1)
--	END
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 202 AND numFormID = 86)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,202,86,1,1,1)
--	END
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 274 AND numFormID = 86)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,274,86,1,1,1)
--	END
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 277 AND numFormID = 86)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,277,86,1,1,1)
--	END
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 399 AND numFormID = 86)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,399,86,1,1,1)
--	END
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 215 AND numFormID = 86)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,215,86,1,1,1)
--	END
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 191 AND numFormID = 86)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,191,86,1,1,1)
--	END
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 206 AND numFormID = 86)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,206,86,1,1,1)
--	END
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 207 AND numFormID = 86)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,207,86,1,1,1)
--	END
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 205 AND numFormID = 86)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,205,86,1,1,1)
--	END
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 204 AND numFormID = 86)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,204,86,1,1,1)
--	END
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 209 AND numFormID = 86)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,209,86,1,1,1)
--	END
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 313 AND numFormID = 86)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,313,86,1,1,1)
--	END
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 280 AND numFormID = 86)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,280,86,1,1,1)
--	END
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 276 AND numFormID = 86)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,276,86,1,1,1)
--	END
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 275 AND numFormID = 86)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,275,86,1,1,1)
--	END
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 293 AND numFormID = 86)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,293,86,1,1,1)
--	END
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 233 AND numFormID = 86)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,233,86,1,1,1)
--	END
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 234 AND numFormID = 86)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,234,86,1,1,1)
--	END

------ Maps fields with new non inventory item form

--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 189 AND numFormID = 87)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit]) VALUES (4,189,87,1,1,1,1)
--	END
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 270 AND numFormID = 87)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit]) VALUES (4,270,87,1,1,1,1)
--	END
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 272 AND numFormID = 87)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit]) VALUES (4,272,87,1,1,1,1)
--	END
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 193 AND numFormID = 87)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,193,87,1,1,1)
--	END
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 202 AND numFormID = 87)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,202,87,1,1,1)
--	END
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 273 AND numFormID = 87)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,273,87,1,1,1)
--	END
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 274 AND numFormID = 87)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,274,87,1,1,1)
--	END
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 190 AND numFormID = 87)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,190,87,1,1,1)
--	END
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 277 AND numFormID = 87)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,277,87,1,1,1)
--	END
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 399 AND numFormID = 87)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,399,87,1,1,1)
--	END
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 215 AND numFormID = 87)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,215,87,1,1,1)
--	END
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 191 AND numFormID = 87)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,191,87,1,1,1)
--	END
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 206 AND numFormID = 87)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,206,87,1,1,1)
--	END
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 204 AND numFormID = 87)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,204,87,1,1,1)
--	END
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 207 AND numFormID = 87)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,207,87,1,1,1)
--	END
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 205 AND numFormID = 87)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,205,87,1,1,1)
--	END
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 313 AND numFormID = 87)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,313,87,1,1,1)
--	END
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 276 AND numFormID = 87)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,276,87,1,1,1)
--	END
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 275 AND numFormID = 87)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,275,87,1,1,1)
--	END
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 280 AND numFormID = 87)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,280,87,1,1,1)
--	END

------ Maps fields with new Serialized/Lot item form

	
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 189 AND numFormID = 88)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit]) VALUES (4,189,88,1,1,1,1)
--	END
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 270 AND numFormID = 88)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit]) VALUES (4,270,88,1,1,1,1)
--	END
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 271 AND numFormID = 88)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit]) VALUES (4,271,88,1,1,1,1)
--	END
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 272 AND numFormID = 88)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitRequired],[bitAllowEdit],[bitInlineEdit]) VALUES (4,272,88,1,1,1,1)
--	END
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 216 AND numFormID = 88)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,216,88,1,1,1)
--	END
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 212 AND numFormID = 88)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,212,88,1,1,1)
--	END
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 203 AND numFormID = 88)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,203,88,1,1,1)
--	END
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 273 AND numFormID = 88)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,273,88,1,1,1)
--	END
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 193 AND numFormID = 88)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,193,88,1,1,1)
--	END
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 281 AND numFormID = 88)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,281,88,1,1,1)
--	END
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 202 AND numFormID = 88)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,202,88,1,1,1)
--	END
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 274 AND numFormID = 88)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,274,88,1,1,1)
--	END
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 277 AND numFormID = 88)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,277,88,1,1,1)
--	END
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 399 AND numFormID = 88)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,399,88,1,1,1)
--	END
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 215 AND numFormID = 88)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,215,88,1,1,1)
--	END
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 191 AND numFormID = 88)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,191,88,1,1,1)
--	END
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 206 AND numFormID = 88)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,206,88,1,1,1)
--	END
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 207 AND numFormID = 88)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,207,88,1,1,1)
--	END
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 205 AND numFormID = 88)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,205,88,1,1,1)
--	END
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 204 AND numFormID = 88)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,204,88,1,1,1)
--	END
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 209 AND numFormID = 88)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,209,88,1,1,1)
--	END
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 313 AND numFormID = 88)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,313,88,1,1,1)
--	END
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 280 AND numFormID = 88)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,280,88,1,1,1)
--	END
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 276 AND numFormID = 88)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,276,88,1,1,1)
--	END
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 275 AND numFormID = 88)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,275,88,1,1,1)
--	END
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 293 AND numFormID = 88)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,293,88,1,1,1)
--	END
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 233 AND numFormID = 88)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,233,88,1,1,1)
--	END
--	IF NOT EXISTS (SELECT * FROM DycFormField_Mapping WHERE numModuleID = 4 AND numFieldID = 190 AND numFormID = 88)
--	BEGIN
--		INSERT INTO DycFormField_Mapping ([numModuleID],[numFieldID],[numFormID],[bitAddField],[bitAllowEdit],[bitInlineEdit]) VALUES (4,190,88,1,1,1)
--	END

----- Change ShortCutBar value 

--IF EXISTS (SELECT * FROM ShortCutBar WHERE vcLinkName = 'Inventory Items')
--BEGIN
--	UPDATE
--		ShortCutBar
--	SET	
--		bitNewPopup = 1,
--		NewLink = '../Items/frmNewItem.aspx?FormID=86'
--	WHERE
--		vcLinkName = 'Inventory Items'
--END

--IF EXISTS (SELECT * FROM ShortCutBar WHERE vcLinkName = 'Non-Inventory Items')
--BEGIN
--	UPDATE
--		ShortCutBar
--	SET	
--		bitNewPopup = 1,
--		NewLink = '../Items/frmNewItem.aspx?FormID=87'
--	WHERE
--		vcLinkName = 'Non-Inventory Items'
--END

--IF EXISTS (SELECT * FROM ShortCutBar WHERE vcLinkName = 'Serialized/LOT #s Items')
--BEGIN
--	UPDATE
--		ShortCutBar
--	SET	
--		bitNewPopup = 1,
--		NewLink = '../Items/frmNewItem.aspx?FormID=88'
--	WHERE
--		vcLinkName = 'Serialized/LOT #s Items'
--END

--IF EXISTS (SELECT * FROM ShortCutBar WHERE vcLinkName = 'Services')
--BEGIN
--	UPDATE
--		ShortCutBar
--	SET	
--		bitNewPopup = 1,
--		NewLink = '../Items/frmNewItem.aspx?FormID=87&ItemType=Services'
--	WHERE
--		vcLinkName = 'Services'
--END		

--IF EXISTS (SELECT * FROM ShortCutBar WHERE vcLinkName = 'Kits')
--BEGIN 
--	UPDATE
--		ShortCutBar
--	SET	
--		bitNewPopup = 1,
--		NewLink = '../Items/frmNewItem.aspx?FormID=86&ItemType=Kits'
--	WHERE
--		vcLinkName = 'Kits'
--END		

--ROLLBACK

------------------------------------------------------------------