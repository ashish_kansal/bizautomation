/******************************************************************
Project: Release 13.6 Date: 15.MAY.2020
Comments: ALTER SCRIPTS
*******************************************************************/

/******************************************** PRASHANT *********************************************/


ALTER TABLE Item ADD bitTimeContractFromSalesOrder  BIT
ALTER TABLE CFW_Fld_Master ADD vcItemsLocation VARCHAR(MAX)
UPDATE DycFieldMaster SET vcFieldName='Matrix Group' WHERE vcFieldName='Item Group'
UPDATE DynamicFormFieldMaster SET vcFormFieldName='Matrix Group' WHERE vcFormFieldName='Item Group'
UPDATE DycFormField_Mapping SET vcFieldName='Matrix Group' WHERE vcFieldName='Item Group'
ALTER TABLE Domain ADD vcElectiveItemFields VARCHAR(500)
CREATE TABLE [dbo].[PurchaseIncentives](
	[numPurchaseIncentiveId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numDivisionID] [numeric](18, 0) NULL,
	[decBuyingQty] [decimal](18, 2) NULL,
	[intType] [int] NULL,
	[vcIncentives] [varchar](500) NULL,
	[numDomainId] [numeric](18, 0) NULL,
 CONSTRAINT [PK_PurchaseIncentives] PRIMARY KEY CLUSTERED 
(
	[numPurchaseIncentiveId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


DELETE FROM DycFormField_Mapping WHERE numFieldID IN ( 
SELECT numFieldId FROM DycFieldMaster WHERE vcDbColumnName='CalAmount' AND numModuleID=3) AND vcFieldName='Order Sub-total' AND numFormID=39

DELETE FROM DycFormField_Mapping WHERE numFieldID IN ( 
SELECT numFieldId FROM DycFieldMaster WHERE vcDbColumnName='monDealAmount' AND numModuleID=3) AND vcFieldName='Inv-Grand-Tot' AND numFormID=39

DELETE FROM DycFormField_Mapping WHERE numFieldID =(
SELECT numFieldId FROM DycFieldMaster WHERE vcDbColumnName='monAmountPaid' AND numModuleID=3) AND numFormID=39

UPDATE DycFormField_Mapping SET vcFieldName='Order, Total, Paid' WHERE numFieldID =
(SELECT numFieldId FROM DycFieldMaster WHERE vcDbColumnName='vcPoppName' AND numModuleID=3) AND numFormID=39

UPDATE DycFormField_Mapping SET vcFieldName='Invoice - Amount (Order Balance)' WHERE numFieldID =
(SELECT numFieldId FROM DycFieldMaster WHERE vcDbColumnName='tintInvoicing' AND numModuleID=3) AND numFormID=39


UPDATE DycFormField_Mapping SET bitInlineEdit=0 WHERE numFieldID =
(SELECT numFieldId FROM DycFieldMaster WHERE vcDbColumnName='vcPoppName' AND numModuleID=3) AND numFormID=39

DELETE FROM DycFormField_Mapping WHERE numFieldID =(
SELECT numFieldId FROM DycFieldMaster WHERE vcDbColumnName='numShippingService' AND numModuleID=3) AND numFormID=39

DELETE FROM DycFormField_Mapping WHERE numFieldID =(
SELECT numFieldId FROM DycFieldMaster WHERE vcDbColumnName='intUsedShippingCompany' AND numModuleID=3) AND numFormID=39

UPDATE DycFieldMaster SET bitAllowFiltering=1 WHERE vcDbColumnName='vcOrderedShipped' AND numModuleID=3
BEGIN TRY
BEGIN TRANSACTION

DECLARE @numFieldID AS NUMERIC(18,0)
DECLARE @numFormFieldID AS NUMERIC(18,0)

INSERT INTO DycFieldMaster
(numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,[order],tintRow,tintColumn,
bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numListID,PopupFunctionName)
VALUES
(2,'Purchase incentives','Purchaseincentives','Purchaseincentives','Purchaseincentives','DivisionMaster','V','R','Popup',46,1,1,1,0,0,0,1,0,1,1,1,0,'openPurchaseincentivesPopup')

SELECT @numFieldID = SCOPE_IDENTITY()

INSERT INTO DynamicFormFieldMaster
(numFormID,vcFormFieldName,vcFieldType,vcAssociatedControlType,vcDbColumnName,numListID,bitDeleted,vcFieldDataType,vcOrigDbColumnName,bitAllowEdit,vcLookBackTableName,
 bitDefault,[order],bitInResults,vcPropertyName,tintRow,tintColumn,bitSettingField,bitAddField,bitDetailField,bitAllowSorting)
VALUES
(36,'Purchase incentives','R','Popup','Purchaseincentives',0,0,'V','Purchaseincentives',0,'DivisionMaster',0,7,1,'Purchaseincentives',1,7,1,0,1,0)

SELECT @numFormFieldID = SCOPE_IDENTITY()

INSERT INTO DycFormField_Mapping
(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,bitInResults,bitDeleted,
bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numFormFieldID)
VALUES
(2,@numFieldID,36,0,0,'Purchase incentives','Popup','Purchaseincentives',7,1,7,1,0,0,1,0,1,0,1,@numFormFieldID)

COMMIT
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		ERROR_MESSAGE(),
		ERROR_NUMBER(),
		ERROR_SEVERITY(),
		ERROR_STATE(),
		ERROR_LINE(),
		ISNULL(ERROR_PROCEDURE(), '-');
END CATCH

DELETE FROM PageNavigationDTL WHERE vcPageNavName='Activities 1.0'

DELETE FROM DycFormConfigurationDetails WHERE numFormId=43 AND 
numFieldId IN (SELECT numFieldId FROM DycFieldMaster WHERE
vcOrigDbColumnName IN('numActivity','numStatus','intTotalProgress','textDetails','vcCompanyName','bitTask','numAssignedTo','FormattedDate'))

DELETE FROM DycFormField_Mapping WHERE numFormID=43 AND numFieldId IN (SELECT numFieldId FROM DycFieldMaster WHERE
vcOrigDbColumnName IN('numActivity','numStatus','intTotalProgress','textDetails','vcCompanyName','bitTask','numAssignedTo','FormattedDate'))



/******************************************** SANDEEP *********************************************/

ALTER TABLE UserMaster ADD vcMailAccessToken VARCHAR(MAX)
ALTER TABLE UserMaster ADD vcMailRefreshToken VARCHAR(MAX)
ALTER TABLE UserMaster ADD tintMailProvider TINYINT DEFAULT 1
ALTER TABLE UserMaster ADD bitOauthImap BIT
ALTER TABLE UserMaster ADD dtTokenExpiration DATETIME
ALTER TABLE UserMaster ADD vcPSMTPUserName VARCHAR(100)

ALTER TABLE Domain ADD vcMailAccessToken VARCHAR(MAX)
ALTER TABLE Domain ADD vcMailRefreshToken VARCHAR(MAX)
ALTER TABLE Domain ADD tintMailProvider TINYINT DEFAULT 1
ALTER TABLE Domain ADD dtTokenExpiration DATETIME


UPDATE Domain SET tintMailProvider=3 
UPDATE UserMaster SET tintMailProvider=3  

---------------------------------

UPDATE DycFormField_Mapping SET bitAllowFiltering=1,vcAssociatedControlType='DateField' WHERE numFormID=141 AND vcFieldName LIKE '%Release%'

/******************************************** SATVA *********************************************/

CREATE TYPE UTV_Item_UOMConversion AS TABLE 
(
	numItemCode NUMERIC,
	UOM FLOAT,
	UOMPurchase FLOAT
)
GO