/******************************************************************
Project: Release 14.7 Date: 04.JAN.2021
Comments: ALTER SCRIPTS
*******************************************************************/

/******************************************** PRASHANT *********************************************/

UPDATE Domain SET bitAutolinkUnappliedPayment=1
ALTER TABLE Domain Add DEFAULT 1 FOR bitAutolinkUnappliedPayment
ALTER TABLE ProjectsMaster ADD bitDisplayTimeToExternalUsers BIT DEFAULT 0

/******************************************** SANDEEP *********************************************/

ALTER TABLE StagePercentageDetails ADD numParentStageDetailsId NUMERIC(18,0)

/******************************************** BASIL *********************************************/

-- Start 29-12-2020
-- Add sorting field for the Price level 1
SET IDENTITY_INSERT dbo.DycFieldMaster ON 

INSERT INTO dbo.DycFieldMaster
        ( numFieldID,
			numModuleID ,
          numDomainID ,
          vcFieldName ,
          vcDbColumnName ,
          vcOrigDbColumnName ,
          vcPropertyName ,
          vcLookBackTableName ,
          vcFieldDataType ,
          vcFieldType ,
          vcAssociatedControlType ,
          vcToolTip ,
          vcListItemType ,
          numListID ,
          PopupFunctionName ,
          [order] ,
          tintRow ,
          tintColumn ,
          bitInResults ,
          bitDeleted ,
          bitAllowEdit ,
          bitDefault ,
          bitSettingField ,
          bitAddField ,
          bitDetailField ,
          bitAllowSorting ,
          bitWorkFlowField ,
          bitImport ,
          bitExport ,
          bitAllowFiltering ,
          bitInlineEdit ,
          bitRequired ,
          intColumnWidth ,
          intFieldMaxLength
        )
VALUES  ( 513,
		  4 , -- numModuleID - numeric
          NULL , -- numDomainID - numeric
          N'Price Level' , -- vcFieldName - nvarchar(50)
          N'monFirstPriceLevelPrice' , -- vcDbColumnName - nvarchar(50)
          N'monFirstPriceLevelPrice' , -- vcOrigDbColumnName - nvarchar(50)
          'PriceLevel1' , -- vcPropertyName - varchar(100)
          N'Price' , -- vcLookBackTableName - nvarchar(50)
          'V' , -- vcFieldDataType - char(1)
          'R' , -- vcFieldType - char(1)
          N'TextBox' , -- vcAssociatedControlType - nvarchar(50)
          Null , -- vcToolTip - nvarchar(1000)
          Null , -- vcListItemType - varchar(3)
          0, -- numListID - numeric
          NULL , -- PopupFunctionName - varchar(100)
          1 , -- order - tinyint
          NULL , -- tintRow - tinyint
          Null , -- tintColumn - tinyint
          1 , -- bitInResults - bit
          0 , -- bitDeleted - bit
          1 , -- bitAllowEdit - bit
          1, -- bitDefault - bit
          1, -- bitSettingField - bit
          NULL , -- bitAddField - bit
          NULL , -- bitDetailField - bit
          1 , -- bitAllowSorting - bit
          NULL , -- bitWorkFlowField - bit
          null , -- bitImport - bit
          NULL , -- bitExport - bit
          1 , -- bitAllowFiltering - bit
          NULL , -- bitInlineEdit - bit
          NULL , -- bitRequired - bit
          NULL , -- intColumnWidth - int
          NULL  -- intFieldMaxLength - int
        )
SET IDENTITY_INSERT  dbo.DycFieldMaster OFF 

INSERT INTO dbo.DycFormField_Mapping
        ( numModuleID ,
          numFieldID ,
          numDomainID ,
          numFormID ,
          bitAllowEdit ,
          bitInlineEdit ,
          vcFieldName ,
          vcAssociatedControlType ,
          vcPropertyName ,
          PopupFunctionName ,
          [order] ,
          tintRow ,
          tintColumn ,
          bitInResults ,
          bitDeleted ,
          bitDefault ,
          bitSettingField ,
          bitAddField ,
          bitDetailField ,
          bitAllowSorting ,
          bitWorkFlowField ,
          bitImport ,
          bitExport ,
          bitAllowFiltering ,
          bitRequired ,
          numFormFieldID ,
          intSectionID ,
          bitAllowGridColor
        )
VALUES  ( 4 , -- numModuleID - numeric
          513, -- numFieldID - numeric
          NULL , -- numDomainID - numeric
          85 , -- numFormID - numeric
          1 , -- bitAllowEdit - bit
          1 , -- bitInlineEdit - bit
          N'Price Level' , -- vcFieldName - nvarchar(50)
          N'TextBox' , -- vcAssociatedControlType - nvarchar(50)
          null , -- vcPropertyName - varchar(100)
          null , -- PopupFunctionName - varchar(100)
          1 , -- order - tinyint
         NULL , -- tintRow - tinyint
          NULL , -- tintColumn - tinyint
          1 , -- bitInResults - bit
          0, -- bitDeleted - bit
          0 , -- bitDefault - bit
          1 , -- bitSettingField - bit
          null , -- bitAddField - bit
          null, -- bitDetailField - bit
          null, -- bitAllowSorting - bit
          null, -- bitWorkFlowField - bit
          1 , -- bitImport - bit
          NULL , -- bitExport - bit
          1 , -- bitAllowFiltering - bit
          NULL , -- bitRequired - bit
          NULL , -- numFormFieldID - numeric
          1 , -- intSectionID - int
          null  -- bitAllowGridColor - bit
        )
