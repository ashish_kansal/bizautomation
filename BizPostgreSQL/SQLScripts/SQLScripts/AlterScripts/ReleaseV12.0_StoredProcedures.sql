/******************************************************************
Project: Release 12.0 Date: 06.MAY.2019
Comments: STORE PROCEDURES
*******************************************************************/

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_DemandForecastDaysDisplay')
DROP FUNCTION fn_DemandForecastDaysDisplay
GO
CREATE FUNCTION [dbo].[fn_DemandForecastDaysDisplay]
(
	@numDomainID NUMERIC(18,0)
	,@ClientTimeZoneOffset INT
	,@numItemCode NUMERIC(18,0)
	,@numLeadDays NUMERIC(18,0)
	,@numWarehouseItemID NUMERIC(18,0)
	,@numForecastDays NUMERIC(18,0)
	,@bitShowHistoricSales BIT
	,@numHistoricalAnalysisPattern BIT
	,@bitBasedOnLastYear BIT
	,@bitIncludeOpportunity BIT
	,@numOpportunityPercentComplete NUMERIC(18,0)
	,@tintDemandPlanBasedOn TINYINT
)    
RETURNS VARCHAR(MAX)
AS    
BEGIN   
	DECLARE @vcResult VARCHAR(1000) = ''


	DECLARE @fltDemandBasedOnReleaseDate FLOAT = 0
	DECLARE @fltDemandBasedOnHistoricalSales FLOAT = 0
	DECLARE @fltDemandBasedOnOpportunity FLOAT = 0
	DECLARE @dtFromDate DATETIME
	DECLARE @dtToDate DATETIME
	DECLARE @numAnalysisDays INT = 0

	SET @fltDemandBasedOnReleaseDate = ISNULL((SELECT
												SUM(numUnitHour - ISNULL(numQtyShipped,0))
											FROM
												OpportunityMaster OM
											INNER JOIN
												OpportunityItems OI
											ON
												OM.numOppId=OI.numOppId
											WHERE
												OM.numDomainId=@numDomainID
												AND OM.tintOppType=1
												AND OM.tintOppStatus=1
												AND ISNULL(OM.tintshipped,0)=0
												AND (ISNULL(OI.numUnitHour,0) - ISNULL(numQtyShipped,0)) > 0
												AND OI.numItemCode=@numItemCode
												AND OI.numWarehouseItmsID=@numWarehouseItemID
												AND ISNULL(bitDropship,0) = 0
												AND (OM.dtReleaseDate IS NOT NULL OR OI.ItemReleaseDate IS NOT NULL)
												AND ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate) <= DATEADD(DAY,@numForecastDays,DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()))),0)
	

	-- KIT CHILD ITEMS
	SET @fltDemandBasedOnReleaseDate = ISNULL(@fltDemandBasedOnReleaseDate,0) 
										+ ISNULL((SELECT
													SUM(OKI.numQtyItemsReq - ISNULL(OKI.numQtyShipped,0))
												FROM
													OpportunityMaster OM
												INNER JOIN
													OpportunityItems OI
												ON
													OM.numOppId = OI.numOppId
												INNER JOIN
													OpportunityKitItems OKI
												ON
													OI.numoppitemtCode=OKI.numOppItemID
												WHERE
													OM.numDomainId=@numDomainID
													AND OM.tintOppType=1
													AND OM.tintOppStatus=1
													AND ISNULL(OM.tintshipped,0)=0
													AND OKI.numChildItemID=@numItemCode
													AND OKI.numWareHouseItemId=@numWarehouseItemID
													AND ISNULL(OI.bitWorkOrder,0) = 0
													AND (ISNULL(OKI.numQtyItemsReq,0) - ISNULL(OKI.numQtyShipped,0)) > 0
													AND (OM.dtReleaseDate IS NOT NULL OR OI.ItemReleaseDate IS NOT NULL)
													AND ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate) <= DATEADD(DAY,@numForecastDays,DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()))),0)

	-- KIT CHILD ITEMS OF CHILD KITS
	SET @fltDemandBasedOnReleaseDate = ISNULL(@fltDemandBasedOnReleaseDate,0) 
										+ ISNULL((SELECT
													SUM(OKCI.numQtyItemsReq - ISNULL(OKCI.numQtyShipped,0))
												FROM
													OpportunityMaster OM
												INNER JOIN
													OpportunityItems OI
												ON
													OM.numOppId=OI.numOppId
												INNER JOIN
													OpportunityKitChildItems OKCI
												ON
													OI.numoppitemtCode=OKCI.numOppItemID
												WHERE
													OM.numDomainId=@numDomainID
													AND OM.tintOppType=1
													AND OM.tintOppStatus=1
													AND ISNULL(OM.tintshipped,0)=0
													AND OKCI.numItemID=@numItemCode
													AND OKCI.numWareHouseItemId=@numWarehouseItemID
													AND ISNULL(OI.bitWorkOrder,0) = 0
													AND (ISNULL(OKCI.numQtyItemsReq,0) - ISNULL(OKCI.numQtyShipped,0)) > 0
													AND (OM.dtReleaseDate IS NOT NULL OR OI.ItemReleaseDate IS NOT NULL)
													AND ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate) <= DATEADD(DAY,@numForecastDays,DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()))),0)

	--ITEMS USED IN WORK ORDER
	SET @fltDemandBasedOnReleaseDate = ISNULL(@fltDemandBasedOnReleaseDate,0) 
										+ ISNULL((SELECT
													SUM(WOD.numQtyItemsReq)
												FROM
													WorkOrderDetails WOD
												INNER JOIN
													WorkOrder WO
												ON
													WOD.numWOId=WO.numWOId
												LEFT JOIN
													OpportunityItems OI
												ON
													WO.numOppItemID = OI.numoppitemtCode
												LEFT JOIN
													OpportunityMaster OM
												ON
													OI.numOppId=OM.numOppId
												WHERE
													WO.numDomainID=@numDomainID
													AND WO.numWOStatus <> 23184 -- NOT COMPLETED
													AND WOD.numChildItemID = @numItemCode
													AND WOD.numWareHouseItemId=@numWarehouseItemID
													AND ISNULL(WOD.numQtyItemsReq,0) > 0
													AND (OM.dtReleaseDate IS NOT NULL OR OI.ItemReleaseDate IS NOT NULL OR WO.bintCompliationDate IS NOT NULL)
													AND 1 = (CASE WHEN OM.numOppId IS NOT NULL THEN (CASE WHEN OM.tintOppStatus=1 THEN 1 ELSE 0 END) ELSE 1 END)
													AND 1 = (CASE 
															WHEN OI.numoppitemtCode IS NOT NULL 
															THEN (CASE 
																	WHEN ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate) <= DATEADD(DAY,@numForecastDays,DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE())) 
																	THEN 1 
																	ELSE 0 
																END)
															ELSE 
																(CASE 
																WHEN WO.bintCompliationDate <= DATEADD(DAY,@numForecastDays,DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE())) 
																THEN 1 
																ELSE 0 
																END)
															END)),0)


	IF ISNULL(@bitShowHistoricSales,0) = 1
	BEGIN
		-- Get Days of Historical Analysis
		SELECT @numAnalysisDays=numDays FROM DemandForecastAnalysisPattern WHERE numDFAPID = @numHistoricalAnalysisPattern

		IF ISNULL(@bitBasedOnLastYear,0) = 1 --last week from last year
		BEGIN
			SET @dtToDate = DATEADD(yy,-1,DATEADD(d,-1,GETUTCDATE()))
			SET @dtFromDate = DATEADD(yy,-1,DATEADD(d,-(@numAnalysisDays),GETUTCDATE()))
		END
		ELSE
		BEGIN
			SET @dtToDate = DATEADD(d,-1,GETUTCDATE())
			SET @dtFromDate = DATEADD(d,-(@numAnalysisDays),GETUTCDATE())
		END

		SET @fltDemandBasedOnHistoricalSales = ISNULL((SELECT
															SUM(numUnitHour)
														FROM
															OpportunityMaster OM
														INNER JOIN
															OpportunityItems OI	
														ON
															OM.numOppId=OI.numOppId
														WHERE
															OM.numDomainId=@numDomainID
															AND OM.tintOppType=1
															AND OM.tintOppStatus=1
															AND numItemCode=@numItemCode
															AND numWarehouseItmsID=@numWarehouseItemID
															AND ISNULL(OI.numUnitHour,0) > 0
															AND ISNULL(bitDropship,0) = 0
															AND CAST(OM.bintCreatedDate AS DATE) BETWEEN CONVERT(DATE,@dtFromDate) AND CONVERT(DATE,@dtToDate)),0)
		

		-- KIT CHILD ITEMS
		SET @fltDemandBasedOnHistoricalSales = ISNULL(@fltDemandBasedOnHistoricalSales,0) 
												+ ISNULL((SELECT
															SUM(OKI.numQtyItemsReq)
														FROM
															OpportunityMaster OM
														INNER JOIN
															OpportunityItems OI
														ON
															OM.numOppId=OI.numOppId
														INNER JOIN
															OpportunityKitItems OKI
														ON
															OI.numoppitemtCode=OKI.numOppItemID
														WHERE
															OM.numDomainId=@numDomainID
															AND OM.tintOppType=1
															AND OM.tintOppStatus=1
															AND OKI.numChildItemID=@numItemCode
															AND OKI.numWareHouseItemId=@numWarehouseItemID
															AND ISNULL(OKI.numQtyItemsReq,0) > 0
															AND ISNULL(OI.bitWorkOrder,0) = 0
															AND CAST(OM.bintCreatedDate AS DATE) BETWEEN CONVERT(DATE,@dtFromDate) AND CONVERT(DATE,@dtToDate)),0)
		

		-- KIT CHILD ITEMS OF CHILD KITS
		SET @fltDemandBasedOnHistoricalSales = ISNULL(@fltDemandBasedOnHistoricalSales,0) 
												+ ISNULL((SELECT
															SUM(OKCI.numQtyItemsReq)
														FROM
															OpportunityMaster OM
														INNER JOIN
															OpportunityItems OI
														ON
															OM.numOppId=OI.numOppId
														INNER JOIN
															OpportunityKitChildItems OKCI
														ON
															OI.numoppitemtCode=OKCI.numOppItemID
														WHERE
															OM.numDomainId=@numDomainID
															AND OM.tintOppType=1
															AND OM.tintOppStatus=1
															AND OKCI.numItemID=@numItemCode
															AND OKCI.numWareHouseItemId=@numWarehouseItemID
															AND ISNULL(OKCI.numQtyItemsReq,0) > 0
															AND ISNULL(OI.bitWorkOrder,0) = 0
															AND CAST(OM.bintCreatedDate AS DATE) BETWEEN CONVERT(DATE,@dtFromDate) AND CONVERT(DATE,@dtToDate)),0)

		--ITEMS USED IN WORK ORDER
		SET @fltDemandBasedOnHistoricalSales = ISNULL(@fltDemandBasedOnHistoricalSales,0) 
												+ ISNULL((SELECT
															SUM(WOD.numQtyItemsReq)
														FROM
															WorkOrderDetails WOD
														INNER JOIN
															WorkOrder WO
														ON
															WOD.numWOId=WO.numWOId
														LEFT JOIN
															OpportunityItems OI
														ON
															WO.numOppItemID = OI.numoppitemtCode
														LEFT JOIN
															OpportunityMaster OM
														ON
															OI.numOppId=OM.numOppId
														WHERE
															WO.numDomainID=@numDomainID
															AND WOD.numChildItemID = @numItemCode
															AND WOD.numWareHouseItemId=@numWarehouseItemID
															AND ISNULL(WOD.numQtyItemsReq,0) > 0
															AND CAST(WO.bintCreatedDate AS DATE) BETWEEN CONVERT(DATE,@dtFromDate) AND CONVERT(DATE,@dtToDate)),0)

		SET @fltDemandBasedOnHistoricalSales = (@fltDemandBasedOnHistoricalSales/@numAnalysisDays) * @numForecastDays
	END

	IF ISNULL(@bitIncludeOpportunity,0) = 1
	BEGIN
		SET @fltDemandBasedOnOpportunity = ISNULL((SELECT
														SUM(OI.numUnitHour * (PP.intTotalProgress/100))
													FROM
														OpportunityMaster OM
													INNER JOIN
														OpportunityItems OI
													ON
														OM.numOppId=OI.numOppId
													INNER JOIN
														ProjectProgress PP
													ON
														OM.numOppId = PP.numOppId
													WHERE
														OM.numDomainId=@numDomainID
														AND OM.tintOppType=1
														AND OM.tintOppStatus=0
														AND numItemCode=@numItemCode
														AND numWarehouseItmsID=@numWarehouseItemID
														AND ISNULL(bitDropship,0) = 0
														AND (ISNULL(OI.numUnitHour,0) * (PP.intTotalProgress/100)) > 0
														AND (OM.dtReleaseDate IS NOT NULL OR OI.ItemReleaseDate IS NOT NULL)
														AND ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate) <= DATEADD(DAY,@numForecastDays,DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()))
														AND PP.intTotalProgress >=  @numOpportunityPercentComplete
												),0)

		-- KIT CHILD ITEMS
		SET @fltDemandBasedOnOpportunity = ISNULL(@fltDemandBasedOnOpportunity,0) 
											+ ISNULL((SELECT
														SUM(OKI.numQtyItemsReq * (PP.intTotalProgress/100))
													FROM
														OpportunityMaster OM
													INNER JOIN
														OpportunityItems OI
													ON
														OM.numOppId=OI.numOppId
													INNER JOIN
														OpportunityKitItems OKI
													ON
														OI.numoppitemtCode=OKI.numOppItemID
													INNER JOIN
														ProjectProgress PP
													ON
														OM.numOppId = PP.numOppId
													WHERE
														OM.numDomainId=@numDomainID
														AND OM.tintOppType=1
														AND OM.tintOppStatus=0
														AND OKI.numChildItemID=@numItemCode
														AND OKI.numWareHouseItemId=@numWarehouseItemID
														AND ISNULL(OI.bitWorkOrder,0) = 0
														AND (OM.dtReleaseDate IS NOT NULL OR OI.ItemReleaseDate IS NOT NULL)
														AND (ISNULL(OKI.numQtyItemsReq,0) * (PP.intTotalProgress/100)) > 0
														AND ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate) <= DATEADD(DAY,@numForecastDays,DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()))
														AND PP.intTotalProgress >= @numOpportunityPercentComplete),0)
		

		-- KIT CHILD ITEMS OF CHILD KITS
		SET @fltDemandBasedOnOpportunity = ISNULL(@fltDemandBasedOnOpportunity,0) 
											+ ISNULL((SELECT
														SUM(OKCI.numQtyItemsReq * (PP.intTotalProgress/100))
													FROM
														OpportunityMaster OM
													INNER JOIN
														OpportunityItems OI
													ON
														OM.numOppId=OI.numOppId
													INNER JOIN
														OpportunityKitChildItems OKCI
													ON
														OI.numoppitemtCode=OKCI.numOppItemID
													INNER JOIN
														ProjectProgress PP
													ON
														OM.numOppId = PP.numOppId
													WHERE
														OM.numDomainId=@numDomainID
														AND OM.tintOppType=1
														AND OM.tintOppStatus=0
														AND OKCI.numItemID=@numItemCode
														AND OKCI.numWareHouseItemId=@numWarehouseItemID
														AND ISNULL(OI.bitWorkOrder,0) = 0
														AND (OM.dtReleaseDate IS NOT NULL OR OI.ItemReleaseDate IS NOT NULL)
														AND (ISNULL(OKCI.numQtyItemsReq,0) * (PP.intTotalProgress/100)) > 0
														AND ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate) <= DATEADD(DAY,@numForecastDays,DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()))
														AND PP.intTotalProgress >= @numOpportunityPercentComplete),0)

		--ITEMS USED IN WORK ORDER
		SET @fltDemandBasedOnOpportunity = ISNULL(@fltDemandBasedOnOpportunity,0) 
											+ ISNULL((SELECT
														SUM(WOD.numQtyItemsReq * (PP.intTotalProgress/100))
													FROM
														OpportunityMaster OM
													INNER JOIN
														OpportunityItems OI
													ON
														OI.numOppId=OM.numOppId
													INNER JOIN
														WorkOrder WO
													ON
														OI.numoppitemtCode = WO.numOppItemID
													INNER JOIN
														WorkOrderDetails WOD
													ON
														WO.numWOId=WOD.numWOId
													INNER JOIN
														ProjectProgress PP
													ON
														OM.numOppId = PP.numOppId
													WHERE
														OM.tintOppType=1
														AND OM.tintOppStatus=0
														AND WO.numDomainID=@numDomainID
														AND WO.numWOStatus <> 23184 -- NOT COMPLETED
														AND WOD.numChildItemID = @numItemCode
														AND WOD.numWareHouseItemId=@numWarehouseItemID
														AND (ISNULL(WOD.numQtyItemsReq,0) * (PP.intTotalProgress/100)) > 0
														AND (OM.dtReleaseDate IS NOT NULL OR OI.ItemReleaseDate IS NOT NULL OR WO.bintCompliationDate IS NOT NULL)
														AND ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate) <= DATEADD(DAY,@numForecastDays,DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()))
														AND PP.intTotalProgress >= @numOpportunityPercentComplete),0)
	END


	IF @tintDemandPlanBasedOn = 2
	BEGIN
		DECLARE @numBackOrder FLOAT
		DECLARE @numExpectedQty FLOAT

		SET @numBackOrder = ISNULL((SELECT numBackOrder FROM WareHouseItems	WHERE numWareHouseItemID=@numWarehouseItemID),0)
		SET @numExpectedQty = ISNULL((SELECT 
										SUM(OI.numUnitHour) 
									FROM 
										OpportunityMaster OM 
									INNER JOIN 
										OpportunityItems OI 
									ON 
										OM.numOppId=OI.numOppId 
									WHERE 
										OM.tintOppType=2 
										AND OM.tintOppStatus=1 
										AND numItemCode=@numItemCode
										AND numWarehouseItmsID=@numWarehouseItemID 
										AND OI.ItemRequiredDate IS NOT NULL 
										AND OI.ItemRequiredDate <= DATEADD(DAY,@numForecastDays,DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()))),0)

		IF @numBackOrder > @numExpectedQty
		BEGIN
			SET @numBackOrder = @numBackOrder - @numExpectedQty
			SET @numExpectedQty = 0

			SET @vcResult = CONCAT('<ul class=''list-inline list-days''><li><a href="#" id="aRelease',@numForecastDays,'" onclick="return OpenDFRecords(1,',@numForecastDays,',',@numItemCode,',',@numWarehouseItemID,',0,0,0)"><span class="badge bg-red">',@numBackOrder * -1,'</span></a><input id="hRelease',@numForecastDays,'" type="hidden" value="',@fltDemandBasedOnReleaseDate,'"></input></li>',(CASE WHEN @bitShowHistoricSales=1 THEN CONCAT('<li><a href="#" style="color:#8faadc" onclick="return OpenDFRecords(2,',@numForecastDays,',',@numItemCode,',',@numWarehouseItemID,',',@numAnalysisDays,',',ISNULL(@bitBasedOnLastYear,0),',0)"><span class="badge bg-yellow">',@fltDemandBasedOnHistoricalSales,'</span></a></li>') ELSE '' END),(CASE WHEN @bitIncludeOpportunity=1 THEN CONCAT('<li><a href="#" style="color:#a9d18e" onclick="return OpenDFRecords(3,',@numForecastDays,',',@numItemCode,',',@numWarehouseItemID,',0,0,',@numOpportunityPercentComplete,''')"><span class="badge bg-green">',@fltDemandBasedOnOpportunity,'</span></a></li>') ELSE '' END),'</ul>')
		END
		ELSE
		BEGIN
			SET @numExpectedQty = @numExpectedQty - @numBackOrder
			SET @vcResult = CONCAT('<ul class=''list-inline list-days''><li><a href="#" id="aRelease',@numForecastDays,'" onclick="return OpenDFRecords(1,',@numForecastDays,',',@numItemCode,',',@numWarehouseItemID,',0,0,0)"><span class="badge bg-green">',@numExpectedQty,'</span></a><input id="hRelease',@numForecastDays,'" type="hidden" value="',@fltDemandBasedOnReleaseDate,'"></input></li>',(CASE WHEN @bitShowHistoricSales=1 THEN CONCAT('<li><a href="#" style="color:#8faadc" onclick="return OpenDFRecords(2,',@numForecastDays,',',@numItemCode,',',@numWarehouseItemID,',',@numAnalysisDays,',',ISNULL(@bitBasedOnLastYear,0),',0)"><span class="badge bg-yellow">',@fltDemandBasedOnHistoricalSales,'</span></a></li>') ELSE '' END),(CASE WHEN @bitIncludeOpportunity=1 THEN CONCAT('<li><a href="#" style="color:#a9d18e" onclick="return OpenDFRecords(3,',@numForecastDays,',',@numItemCode,',',@numWarehouseItemID,',0,0,',@numOpportunityPercentComplete,''')"><span class="badge bg-green">',@fltDemandBasedOnOpportunity,'</span></a></li>') ELSE '' END),'</ul>')
		END
	END
	ELSE
	BEGIN
		SET @vcResult = CONCAT('<ul class=''list-inline list-days''><li><a href="#" id="aRelease',@numForecastDays,'" onclick="return OpenDFRecords(1,',@numForecastDays,',',@numItemCode,',',@numWarehouseItemID,',0,0,0)"><span class="badge ',CASE WHEN @fltDemandBasedOnReleaseDate > 0 THEN 'bg-red'  ELSE 'bg-light-blue' END,'">',CASE WHEN @fltDemandBasedOnReleaseDate > 0 THEN @fltDemandBasedOnReleaseDate * -1 ELSE @fltDemandBasedOnReleaseDate END,'</span></a><input id="hRelease',@numForecastDays,'" type="hidden" value="',@fltDemandBasedOnReleaseDate,'"></input></li>',(CASE WHEN @bitShowHistoricSales=1 THEN CONCAT('<li><a href="#" style="color:#8faadc" onclick="return OpenDFRecords(2,',@numForecastDays,',',@numItemCode,',',@numWarehouseItemID,',',@numAnalysisDays,',',ISNULL(@bitBasedOnLastYear,0),',0)"><span class="badge bg-yellow">',@fltDemandBasedOnHistoricalSales,'</span></a></li>') ELSE '' END),(CASE WHEN @bitIncludeOpportunity=1 THEN CONCAT('<li><a href="#" style="color:#a9d18e" onclick="return OpenDFRecords(3,',@numForecastDays,',',@numItemCode,',',@numWarehouseItemID,',0,0,',@numOpportunityPercentComplete,''')"><span class="badge bg-green">',@fltDemandBasedOnOpportunity,'</span></a></li>') ELSE '' END),'</ul>')
	END

	RETURN @vcResult
END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='tf'AND NAME ='fn_GetItemPriceBasedOnMethod')
DROP FUNCTION fn_GetItemPriceBasedOnMethod
GO
CREATE FUNCTION [dbo].[fn_GetItemPriceBasedOnMethod] 
(
    @numDomainID NUMERIC(18,0)
	,@numDivisionID NUMERIC(18,0)
	,@numItemCode NUMERIC(18,0)
	,@numQty FLOAT
	,@bitCalculatedPrice BIT
	,@monCalculatedPrice DECIMAL(20,5)
	,@numWarehouseItemID NUMERIC(18,0)
	,@vcSelectedKitChildItems VARCHAR(MAX)
)
RETURNS @ItemPrice TABLE
(
	monPrice DECIMAL(20,5)
	,tintDisountType TINYINT
	,decDiscount FLOAT
	,monFinalPrice DECIMAL(20,5) 
)
AS 
BEGIN	
	DECLARE @monListPrice DECIMAL(20,5)
	DECLARE @monVendorCost DECIMAL(20,5)
	DECLARE @tintPriceLevel AS TINYINT
	DECLARE @tintRuleType AS TINYINT
	DECLARE @tintDisountType AS TINYINT
	DECLARE @decDiscount AS FLOAT
	DECLARE @monPrice AS DECIMAL(20,5) = NULL
	DECLARE @monFinalPrice AS DECIMAL(20,5) = NULL
	DECLARE @numRelationship NUMERIC(18,0)
	DECLARE @numProfile NUMERIC(18,0)
	DECLARE @numItemClassification AS NUMERIC(18,0)
	DECLARE @tintPriceBookDiscount AS TINYINT
	DECLARE @tintPriceMethod TINYINT
	DECLARE @fltUOMConversionFactor INT = 1
	DECLARE @bitKitParent BIT
	DECLARE @bitAssembly BIT
	DECLARE @bitCalAmtBasedonDepItems BIT
	DECLARE @tintKitAssemblyPriceBasedOn TINYINT

	SELECT 
		@monListPrice = (CASE WHEN ISNULL(@bitCalculatedPrice,0) = 1 THEN @monCalculatedPrice ELSE ISNULL(monListPrice,0) END)
		,@numItemClassification=numItemClassification
		,@monVendorCost=(ISNULL(monCost,0) * dbo.fn_UOMConversion(Item.numBaseUnit,Item.numItemCode,Item.numDomainID,Item.numPurchaseUnit))
		,@bitKitParent=ISNULL(bitKitParent,0)
		,@bitAssembly=ISNULL(bitAssembly,0)
		,@bitCalAmtBasedonDepItems=ISNULL(bitCalAmtBasedonDepItems,0)
		,@tintKitAssemblyPriceBasedOn=ISNULL(tintKitAssemblyPriceBasedOn,1)
	FROM 
		Item 
	LEFT JOIN
		Vendor
	ON
		Item.numVendorID = Vendor.numVendorID
		AND Item.numItemCode = Vendor.numItemCode
	WHERE
		Item.numItemCode=@numItemCode

	IF @bitCalculatedPrice = 0 AND (ISNULL(@bitKitParent,0)=1 OR ISNULL(@bitAssembly,0) = 1) AND ISNULL(@bitCalAmtBasedonDepItems,0) = 1
	BEGIN
		SELECT
			@monListPrice = monPrice
		FROM
			dbo.fn_GetKitAssemblyCalculatedPrice
			(
				@numDomainID
				,@numDivisionID
				,@numItemCode
				,@numQty
				,@numWarehouseItemID
				,@tintKitAssemblyPriceBasedOn
				,0
				,0
				,@vcSelectedKitChildItems
			)    
	END
		

	SELECT 
		@tintPriceMethod=ISNULL(numDefaultSalesPricing,0)
		,@tintPriceBookDiscount=tintPriceBookDiscount 
	FROM 
		Domain 
	WHERE 
		numDomainId=@numDomainID

	-- GET ORGANIZATION DETAL
	SELECT 
		@numRelationship=numCompanyType,
		@numProfile=vcProfile,
		@tintPriceLevel = ISNULL(D.tintPriceLevel,0)
	FROM 
		DivisionMaster D                  
	JOIN 
		CompanyInfo C 
	ON 
		C.numCompanyId=D.numCompanyID                  
	WHERE 
		numDivisionID =@numDivisionID       

	IF @tintPriceMethod= 1 -- PRICE LEVEL
	BEGIN
		IF (SELECT COUNT(*) FROM PricingTable WHERE numItemCode=@numItemCode AND ISNULL(numPriceRuleID,0) = 0) > 0
		BEGIN
			SELECT
				@tintDisountType = 1,
				@decDiscount = 0,
				@tintRuleType = tintRuleType,
				@monPrice = (CASE tintRuleType
										WHEN 1 -- Deduct From List Price
										THEN
											CASE tintDiscountType 
												WHEN 1 -- PERCENT
												THEN (CASE WHEN @monListPrice > 0 THEN @monListPrice - (@monListPrice * (ISNULL(decDiscount,0)/100)) ELSE 0 END)
												WHEN 2 -- FLAT AMOUNT
												THEN (CASE WHEN @monListPrice > 0 THEN (CASE WHEN (@monListPrice * @numQty * @fltUOMConversionFactor) - ISNULL(decDiscount,0) > 0 THEN ((@monListPrice * @numQty * @fltUOMConversionFactor) - ISNULL(decDiscount,0)) / (@numQty * @fltUOMConversionFactor) ELSE 0 END) ELSE 0 END)
												WHEN 3 -- NAMED PRICE
												THEN ISNULL(decDiscount,0)
											END
										WHEN 2 -- Add to primary vendor cost
										THEN
											CASE tintDiscountType 
												WHEN 1  -- PERCENT
												THEN @monVendorCost + (@monVendorCost * (ISNULL(decDiscount,0)/100)) * @fltUOMConversionFactor
												WHEN 2 -- FLAT AMOUNT
												THEN (CASE WHEN (@monVendorCost * @numQty * @fltUOMConversionFactor) + ISNULL(decDiscount,0) > 0 THEN ((@monVendorCost * @numQty * @fltUOMConversionFactor) + ISNULL(decDiscount,0)) / (@numQty * @fltUOMConversionFactor)  ELSE 0 END)
												WHEN 3 -- NAMED PRICE
												THEN ISNULL(decDiscount,0)
											END
										WHEN 3 -- Named price
										THEN
											CASE 
											WHEN ISNULL(@tintPriceLevel,0) > 0
											THEN
												(SELECT 
													 ISNULL(decDiscount,0)
												FROM
												(
													SELECT 
														ROW_NUMBER() OVER(PARTITION BY PricingTable.numItemCode ORDER BY numPricingID) Id,
														decDiscount
													FROM 
														PricingTable 
													WHERE 
														PricingTable.numItemCode = @numItemCode 
														AND tintRuleType = 3 
												) TEMP
												WHERE
													Id = @tintPriceLevel)
											ELSE
												ISNULL(decDiscount,0)
											END
										END
									) 
			FROM
				PricingTable
			WHERE
				numItemCode=@numItemCode AND (@numQty * @fltUOMConversionFactor) >= intFromQty AND (@numQty * @fltUOMConversionFactor) <= intToQty
		END

		INSERT INTO @ItemPrice
		(
			monPrice
			,tintDisountType
			,decDiscount
			,monFinalPrice
		)
		SELECT 
			ISNULL(@monPrice,@monListPrice) AS monPrice,
			1 AS tintDisountType,
			0 AS decDiscount,
			ISNULL(@monPrice,@monListPrice)
	END
	ELSE IF @tintPriceMethod= 2 AND (SELECT COUNT(*) FROM PriceBookRules WHERE numDomainID=@numDomainID AND tintRuleFor=1) > 0 -- PRICE RULE
	BEGIN
		DECLARE @numPriceRuleID NUMERIC(18,0)
		DECLARE @tintPriceRuleType AS TINYINT
		DECLARE @tintPricingMethod TINYINT
		DECLARE @tintPriceBookDiscountType TINYINT
		DECLARE @decPriceBookDiscount FLOAT
		DECLARE @intQntyItems INT
		DECLARE @decMaxDedPerAmt FLOAT

		SELECT 
			TOP 1
			@numPriceRuleID = numPricRuleID,
			@tintPricingMethod = tintPricingMethod,
			@tintPriceRuleType = p.tintRuleType,
			@tintPriceBookDiscountType = P.[tintDiscountType],
			@decPriceBookDiscount = P.[decDiscount],
			@intQntyItems = P.[intQntyItems],
			@decMaxDedPerAmt = P.[decMaxDedPerAmt]
		FROM   
			PriceBookRules P 
		LEFT JOIN 
			PriceBookRuleDTL PDTL 
		ON 
			P.numPricRuleID = PDTL.numRuleID
		LEFT JOIN 
			PriceBookRuleItems PBI 
		ON 
			P.numPricRuleID = PBI.numRuleID
		LEFT JOIN 
			[PriceBookPriorities] PP 
		ON 
			PP.[Step2Value] = P.[tintStep2] 
			AND PP.[Step3Value] = P.[tintStep3]
		WHERE  
			P.numDomainID=@numDomainID 
			AND tintRuleFor=1
			AND (
					((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 1 AND PDTL.numValue = @numDivisionID)) -- Priority 1
					OR ((tintStep2 = 2 AND PBI.numValue = @numItemClassification) AND (tintStep3= 1 AND PDTL.numValue = @numDivisionID)) -- Priority 2
					OR ((tintStep2 = 3) AND (tintStep3 = 1 AND PDTL.numValue = @numDivisionID)) -- Priority 3

					OR ((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 4
					OR ((tintStep2 = 2 AND PBI.numValue = @numItemClassification) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 5
					OR ((tintStep2 = 3) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 6

					OR ((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 3)) -- Priority 7
					OR ((tintStep2 = 2 AND PBI.numValue = @numItemClassification) AND (tintStep3 = 3)) -- Priority 8
					OR ((tintStep2 = 3) and (tintStep3 = 3)) -- Priority 9
				)
		ORDER BY 
			PP.Priority ASC


		IF ISNULL(@numPriceRuleID,0) > 0
		BEGIN
			IF @tintPricingMethod = 1 -- BASED ON PRICE TABLE
			BEGIN
				SELECT
					@tintDisountType = tintDiscountType,
					@decDiscount = ISNULL(decDiscount,0),
					@tintRuleType = tintRuleType,
					@monPrice = (CASE tintRuleType WHEN 1 THEN @monListPrice WHEN 2 THEN @monVendorCost END) ,
					@monFinalPrice = (CASE tintRuleType
											WHEN 1 -- Deduct From List Price
											THEN
												CASE tintDiscountType 
													WHEN 1 -- PERCENT
													THEN (CASE WHEN @monListPrice > 0 THEN @monListPrice - (@monListPrice * (ISNULL(decDiscount,0)/100)) ELSE 0 END)
													WHEN 2 -- FLAT AMOUNT
													THEN (CASE WHEN @monListPrice > 0 THEN (CASE WHEN (@monListPrice * @numQty * @fltUOMConversionFactor) - ISNULL(decDiscount,0) > 0 THEN ((@monListPrice * @numQty * @fltUOMConversionFactor) - ISNULL(decDiscount,0)) / (@numQty * @fltUOMConversionFactor) ELSE 0 END) ELSE 0 END)
													WHEN 3 -- NAMED PRICE
													THEN ISNULL(decDiscount,0)
												END
											WHEN 2 -- Add to primary vendor cost
											THEN
												CASE tintDiscountType 
													WHEN 1  -- PERCENT
													THEN @monVendorCost + (@monVendorCost * (ISNULL(decDiscount,0)/100))
													WHEN 2 -- FLAT AMOUNT
													THEN (CASE WHEN (@monVendorCost * @numQty * @fltUOMConversionFactor) + ISNULL(decDiscount,0) > 0 THEN ((@monVendorCost * @numQty * @fltUOMConversionFactor) + ISNULL(decDiscount,0)) / (@numQty * @fltUOMConversionFactor)  ELSE 0 END)
													WHEN 3 -- NAMED PRICE
													THEN ISNULL(decDiscount,0)
												END
											WHEN 3 -- Named price
											THEN
												CASE 
												WHEN ISNULL(@tintPriceLevel,0) > 0
												THEN
													(SELECT 
														 ISNULL(decDiscount,0)
													FROM
													(
														SELECT 
															ROW_NUMBER() OVER(PARTITION BY PricingTable.numItemCode ORDER BY numPricingID) Id,
															decDiscount
														FROM 
															PricingTable 
														WHERE 
															PricingTable.numItemCode = @numItemCode 
															AND tintRuleType = 3 
													) TEMP
													WHERE
														Id = @tintPriceLevel)
												ELSE
													ISNULL(decDiscount,0)
												END
											END
										) 
				FROM
					PricingTable
				WHERE
					numPriceRuleID=@numPriceRuleID AND @numQty * @fltUOMConversionFactor >= intFromQty AND @numQty * @fltUOMConversionFactor <= intToQty
			END
			ELSE IF @tintPricingMethod = 2 -- BASED ON RULE
			BEGIN
				SET @monPrice = (CASE WHEN @tintPriceRuleType = 2 THEN @monVendorCost ELSE @monListPrice END)

				IF (@tintPriceBookDiscountType = 1 ) -- Percentage 
				BEGIN
					SET @tintDisountType = @tintPriceBookDiscountType
					SET @decPriceBookDiscount = @decPriceBookDiscount * (@numQty/@intQntyItems);
					SET @decDiscount = @decPriceBookDiscount * (CASE WHEN @tintPriceRuleType = 2 THEN @monVendorCost ELSE @monListPrice END) /100;
					SET @decMaxDedPerAmt = @decMaxDedPerAmt * (CASE WHEN @tintPriceRuleType = 2 THEN @monVendorCost ELSE @monListPrice END) /100;
					
					IF (@decDiscount > @decMaxDedPerAmt)
						SET @decDiscount = @decMaxDedPerAmt;
				
					IF ( @tintPriceRuleType = 1 )
							SET @monFinalPrice = (@monListPrice - @decDiscount);
					IF ( @tintPriceRuleType = 2 )
							SET @monFinalPrice = (@monVendorCost + @decDiscount);
				END
				IF ( @tintPriceBookDiscountType = 2 ) -- Flat discount 
				BEGIN
					SET @decDiscount = @decPriceBookDiscount * ((@numQty * @fltUOMConversionFactor)/@intQntyItems);

					IF (@decDiscount > @decMaxDedPerAmt)
						SET @decDiscount = @decMaxDedPerAmt;
					
					IF ( @tintPriceRuleType = 1 )
						SET @monFinalPrice =  ((@monListPrice * @numQty * @fltUOMConversionFactor) - @decDiscount) / (@numQty * @fltUOMConversionFactor);
						
					IF ( @tintPriceRuleType = 2 )
						SET @monFinalPrice = ((@monVendorCost * @numQty * @fltUOMConversionFactor) + @decDiscount) / (@numQty * @fltUOMConversionFactor);
				END
			END
		END
		
		INSERT INTO @ItemPrice
		(
			monPrice
			,tintDisountType
			,decDiscount
			,monFinalPrice
		)
		SELECT 
			(CASE WHEN @monFinalPrice IS NULL THEN @monListPrice ELSE (CASE WHEN @tintPriceBookDiscount = 1 THEN ISNULL(@monPrice,0) ELSE ISNULL(@monFinalPrice,0) END) END),
			(CASE WHEN @monFinalPrice IS NULL THEN 1 ELSE (CASE WHEN @tintPriceBookDiscount = 1 THEN ISNULL(@tintPriceBookDiscountType,1) ELSE 1 END) END),
			(CASE WHEN @monFinalPrice IS NULL THEN 0 ELSE (CASE WHEN @tintPriceBookDiscount = 1 THEN ISNULL(@decPriceBookDiscount,0) ELSE 0 END) END),
			ISNULL(@monFinalPrice,0)
	END
	ELSE IF @tintPriceMethod = 3 -- Last Price
	BEGIN
		SELECT 
			TOP 1 
			@monPrice = monPrice
			,@tintDisountType = ISNULL(OpportunityItems.bitDiscountType,1)
			,@decDiscount = ISNULL(OpportunityItems.fltDiscount,0)
			,@monFinalPrice = (CASE 
								WHEN ISNULL(OpportunityItems.fltDiscount,0) > 0 AND ISNULL(numUnitHour,0) > 0 
								THEN (CASE 
										WHEN ISNULL(OpportunityItems.bitDiscountType,1)=1 
										THEN CAST(((ISNULL(monTotAmtBefDiscount,0) - ISNULL(monTotAmount,0))/numUnitHour) AS DECIMAL(20,5))
										ELSE CAST(monPrice - (monPrice * (ISNULL(OpportunityItems.fltDiscount,0)/100)) AS DECIMAL(20,5)) END) 
								ELSE monPrice 
								END)
			,@tintRuleType = 3
		FROM 
			OpportunityItems 
		JOIN 
			OpportunityMaster 
		ON 
			OpportunityItems.numOppId = OpportunityMaster.numOppId 
		WHERE 
			OpportunityMaster.numDomainId= @numDomainID
			AND tintOppType = 1
			AND tintOppStatus = 1
			AND numItemCode=@numItemCode
		ORDER BY
			OpportunityMaster.numOppId DESC

		INSERT INTO @ItemPrice
		(
			monPrice
			,tintDisountType
			,decDiscount
			,monFinalPrice
		)
		VALUES
		( 
			ISNULL(@monPrice,0),
			@tintDisountType,
			@decDiscount,
			@monFinalPrice
		)
	END
	ELSE
	BEGIN
		INSERT INTO @ItemPrice
		(
			monPrice
			,tintDisountType
			,decDiscount
			,monFinalPrice
		)
		VALUES
		( 
			ISNULL(@monListPrice,0),
			1,
			0,
			ISNULL(@monListPrice,0)
		)
	END

	RETURN
END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='TF'AND NAME ='fn_GetKitAssemblyCalculatedPrice')
DROP FUNCTION fn_GetKitAssemblyCalculatedPrice
GO
CREATE FUNCTION [dbo].[fn_GetKitAssemblyCalculatedPrice]
(
	@numDomainID NUMERIC(18,0)
	,@numDivisionID NUMERIC(18,0)
	,@numItemCode NUMERIC(18,0)
	,@numQty FLOAT
	,@numWarehouseItemID NUMERIC(18,0)
	,@tintKitAssemblyPriceBasedOn TINYINT
	,@numOppID NUMERIC(18,0)
	,@numOppItemID NUMERIC(18,0)
	,@vcSelectedKitChildItems VARCHAR(MAX)
)    
RETURNS @TempPrice TABLE
(
	bitSuccess BIT
	,monPrice DECIMAL(20,5)
	,monMSRPPrice DECIMAL(20,5)
)
AS    
BEGIN   
	DECLARE @monListPrice DECIMAL(20,5)

	SELECT 
		@monListPrice=ISNULL(monListPrice,0)
	FROM 
		Item 
	WHERE 
		numItemCode=@numItemCode

	DECLARE @numWarehouseID NUMERIC(18,0)

	IF ISNULL(@numWarehouseItemID,0) > 0
	BEGIN
		SELECT @numWarehouseID=numWareHouseID FROM WareHouseItems WHERE numWareHouseItemID =@numWarehouseItemID
	END
	ELSE
	BEGIN
		SELECT TOP 1 @numWarehouseID=numWareHouseID FROM Warehouses WHERE numDomainID=@numDomainID
	END

	DECLARE @KitAssemblyPrice AS DECIMAL(20,5) = 0

	DECLARE @TEMPitems TABLE
	(
		numItemCode NUMERIC(18,0)
		,numQtyItemsReq FLOAT
		,numUOMId NUMERIC(18,0)
		,bitKitParent BIT
		,bitFirst BIT
	)

	IF ISNULL(@numOppItemID,0) > 0
	BEGIN
		INSERT INTO @TEMPitems
		(
			numItemCode
			,numQtyItemsReq
			,numUOMId
			,bitKitParent
		)
		SELECT 
			OKI.numChildItemID
			,(OKI.numQtyItemsReq_Orig * ISNULL(dbo.fn_UOMConversion(OKI.numUOMId,OKI.numChildItemID,@numDomainID,I.numBaseUnit),1))
			,OKI.numUOMId
			,ISNULL(I.bitKitParent,0)
		FROM
			OpportunityKitItems OKI
		INNER JOIN
			Item I
		ON
			OKI.numChildItemID = I.numItemCode
		WHERE
			OKI.numOppId = @numOppID
			AND OKI.numOppItemID = @numOppItemID
			AND 1 = (CASE 
						WHEN ISNULL(I.bitKitParent,0) = 1
						THEN (CASE 
								WHEN ISNULL(I.bitCalAmtBasedonDepItems,0) = 1 
								THEN (CASE WHEN ISNULL(I.tintKitAssemblyPriceBasedOn,1) = 4 THEN 1 ELSE 0 END)
								ELSE 1 
							END)
						ELSE 1
					END)

		INSERT INTO @TEMPitems
		(
			numItemCode
			,numQtyItemsReq
			,numUOMId
			,bitKitParent
		)
		SELECT 
			OKCI.numItemID
			,(OKI.numQtyItemsReq_Orig * ISNULL(dbo.fn_UOMConversion(OKI.numUOMId,OKI.numChildItemID,@numDomainID,IOKI.numBaseUnit),1)) * (OKCI.numQtyItemsReq_Orig * ISNULL(dbo.fn_UOMConversion(OKCI.numUOMId,OKCI.numItemID,@numDomainID,IOKCI.numBaseUnit),1))
			,OKCI.numUOMId
			,ISNULL(IOKCI.bitKitParent,0)
		FROM
			OpportunityKitChildItems OKCI
		INNER JOIN
			Item IOKCI
		ON
			OKCI.numItemID = IOKCI.numItemCode
		INNER JOIN
			OpportunityKitItems OKI
		ON
			OKCI.numOppChildItemID = OKI.numOppChildItemID
		INNER JOIN
			Item IOKI
		ON
			OKI.numChildItemID = IOKI.numItemCode
		WHERE
			OKCI.numOppId = @numOppID
			AND OKCI.numOppItemID = @numOppItemID		
			AND ISNULL(IOKI.bitKitParent,0) = 1 
			AND ISNULL(IOKI.bitCalAmtBasedonDepItems,0) = 1	
	END
	ELSE
	BEGIN
		DECLARE @TempExistingItems TABLE
		(
			vcItem VARCHAR(100)
		)

		INSERT INTO @TempExistingItems
		(
			vcItem
		)
		SELECT 
			OutParam 
		FROM 
			SplitString(@vcSelectedKitChildItems,',')

		DECLARE @TEMPSelectedKitChilds TABLE
		(
			ChildKitItemID NUMERIC(18,0),
			ChildKitWarehouseItemID NUMERIC(18,0),
			ChildKitChildItemID NUMERIC(18,0),
			ChildKitChildWarehouseItemID NUMERIC(18,0),
			ChildQty FLOAT
		)

		INSERT INTO @TEMPSelectedKitChilds
		(
			ChildKitItemID
			,ChildKitChildItemID
			,ChildQty
		)
		SELECT 
			Reverse(ParseName(Replace(Reverse(vcItem), '-', '.'), 1))
			,Reverse(ParseName(Replace(Reverse(vcItem), '-', '.'), 2))
			,Reverse(ParseName(Replace(Reverse(vcItem), '-', '.'), 3))
		FROM  
		(
			SELECT 
				vcItem 
			FROM 
				@TempExistingItems
		) As [x]

		UPDATE 
			@TEMPSelectedKitChilds 
		SET 
			ChildKitWarehouseItemID=(SELECT TOP 1 numWareHouseItemID FROM WareHouseItems WHERE numItemID=ChildKitItemID AND numWareHouseID=@numWarehouseID)
			,ChildKitChildWarehouseItemID=(SELECT TOP 1 numWareHouseItemID FROM WareHouseItems WHERE numItemID=ChildKitChildItemID AND numWareHouseID=@numWarehouseID)


		IF (SELECT COUNT(*) FROM @TEMPSelectedKitChilds WHERE ISNULL(ChildKitItemID,0)=0) > 0
		BEGIN
			INSERT INTO @TEMPitems
			(
				numItemCode
				,numQtyItemsReq
				,numUOMId
				,bitKitParent
			)
			SELECT
				I.numItemCode
				,ChildQty
				,I.numBaseUnit
				,0
			FROM
				@TEMPSelectedKitChilds T1
			INNER JOIN
				Item I
			ON
				T1.ChildKitChildItemID = I.numItemCOde
			WHERE
				ISNULL(ChildKitItemID,0)=0
		END
		ELSE
		BEGIN
			;WITH CTE (numItemCode, vcItemName, bitKitParent, bitCalAmtBasedonDepItems, numQtyItemsReq,numUOMId) AS
			(
				SELECT
					ID.numChildItemID,
					I.vcItemName,
					ISNULL(I.bitKitParent,0),
					ISNULL(I.bitCalAmtBasedonDepItems,0),
					CAST((ISNULL(ID.numQtyItemsReq,0) * ISNULL(dbo.fn_UOMConversion(ISNULL(ID.numUOMID, 0), I.numItemCode, @numDomainID, ISNULL(I.numBaseUnit, 0)),1)) AS FLOAT),
					ISNULL(numUOMId,0)
				FROM 
					[ItemDetails] ID
				INNER JOIN 
					[Item] I 
				ON 
					ID.[numChildItemID] = I.[numItemCode]
				WHERE   
					[numItemKitID] = @numItemCode
					AND 1 = (CASE 
								WHEN ISNULL(I.bitKitParent,0) = 1 AND LEN(ISNULL(@vcSelectedKitChildItems,'')) > 0 THEN 
									(CASE 
										WHEN numChildItemID IN (SELECT ChildKitItemID FROM @TEMPSelectedKitChilds) 
										THEN 1 
										ELSE 0 
									END) 
								ELSE (CASE 
										WHEN (SELECT COUNT(*) FROM @TEMPSelectedKitChilds WHERE ISNULL(ChildKitItemID,0)=0) > 0
										THEN	
											(CASE 
												WHEN numChildItemID IN (SELECT ChildKitChildItemID FROM @TEMPSelectedKitChilds WHERE ISNULL(ChildKitItemID,0)=0) 
												THEN 1 
												ELSE 0 
											END) 
										ELSE 1
									END)
							END)
				UNION ALL
				SELECT
					ID.numChildItemID,
					I.vcItemName,
					ISNULL(I.bitKitParent,0),
					ISNULL(I.bitCalAmtBasedonDepItems,0),
					CAST((ISNULL(Temp1.numQtyItemsReq,0) * ISNULL(ID.numQtyItemsReq,0) * ISNULL(dbo.fn_UOMConversion(ISNULL(ID.numUOMID, 0), I.numItemCode, @numDomainID, ISNULL(I.numBaseUnit, 0)),1)) AS FLOAT),
					ISNULL(ID.numUOMId,0)
				FROM 
					CTE As Temp1
				INNER JOIN
					[ItemDetails] ID
				ON
					ID.numItemKitID = Temp1.numItemCode
				INNER JOIN 
					[Item] I 
				ON 
					ID.[numChildItemID] = I.[numItemCode]
				WHERE
					Temp1.bitKitParent = 1
					AND Temp1.bitCalAmtBasedonDepItems = 1
					AND EXISTS (SELECT ChildKitItemID FROM @TEMPSelectedKitChilds T2 WHERE ISNULL(Temp1.bitKitParent,0) = 1 AND t2.ChildKitItemID=ID.numItemKitID AND ID.numChildItemID = T2.ChildKitChildItemID)
			)

			INSERT INTO @TEMPitems
			(
				numItemCode
				,numQtyItemsReq
				,numUOMId
				,bitKitParent
			)
			SELECT
				numItemCode
				,numQtyItemsReq
				,numUOMId
				,bitKitParent
			FROM
				CTE
		END
	END

	IF EXISTS (SELECT ID.numItemCode FROM @TEMPitems ID INNER JOIN [Item] I ON ID.numItemCode = I.[numItemCode] WHERE I.charItemType = 'P' AND (SELECT COUNT(*) FROM WareHouseItems WHERE numItemID=I.numItemCode AND numWareHouseID=@numWarehouseID) = 0)
	BEGIN
		INSERT INTO @TempPrice
		(
			bitSuccess
			,monPrice
			,monMSRPPrice
		)
		VALUES
		(
			0
			,0
			,0
		)
	END
	ELSE
	BEGIN
		DECLARE @monCalculatedPrice DECIMAL(20,5) = 0

		SET @monCalculatedPrice = ISNULL((SELECT
											(CASE WHEN @tintKitAssemblyPriceBasedOn=4 THEN ISNULL(@monListPrice,0) + ISNULL(SUM(CalculatedPrice),0) ELSE ISNULL(SUM(CalculatedPrice),0) END)
										FROM
										(
											SELECT  
												ISNULL(CASE 
														WHEN I.[charItemType]='P' 
														THEN 
															CASE 
															WHEN ISNULL(I.bitAssembly,0) = 1 AND ISNULL(I.bitCalAmtBasedonDepItems,0) = 1
															THEN
																ISNULL((SELECT monPrice FROM dbo.fn_GetKitAssemblyCalculatedPrice(@numDomainID,@numDivisionID,I.numItemCode,ID.[numQtyItemsReq],WI.numWareHouseItemID,I.tintKitAssemblyPriceBasedOn,0,0,'')),0)
															WHEN ISNULL(I.bitKitParent,0) = 1  AND ISNULL(I.bitCalAmtBasedonDepItems,0) = 1
															THEN (CASE WHEN ISNULL(I.tintKitAssemblyPriceBasedOn,0) = 4 THEN WI.[monWListPrice] ELSE 0 END)
															ELSE
																(CASE @tintKitAssemblyPriceBasedOn
																		WHEN 2 THEN ISNULL(I.monAverageCost,0) 
																		WHEN 3 THEN ISNULL(monCost,0) * dbo.fn_UOMConversion(I.numBaseUnit,I.numItemCode,I.numDomainID,I.numPurchaseUnit)
																		ELSE WI.[monWListPrice]
																END) 
															END
														ELSE (CASE @tintKitAssemblyPriceBasedOn 
																WHEN 4 THEN I.[monListPrice]
																WHEN 2 THEN ISNULL(I.monAverageCost,0)
																WHEN 3 THEN ISNULL(V.monCost,0) * dbo.fn_UOMConversion(I.numBaseUnit,I.numItemCode,I.numDomainID,I.numPurchaseUnit)
																ELSE I.[monListPrice] 
															END)
													END,0) * ID.[numQtyItemsReq] AS CalculatedPrice
											FROM 
												@TEMPitems ID
											INNER JOIN 
												[Item] I 
											ON 
												ID.numItemCode = I.[numItemCode]
											LEFT JOIN
												Vendor V
											ON
												I.numVendorID = V.numVendorID
												AND I.numItemCode = V.numItemCode
											OUTER APPLY
											(
												SELECT TOP 1 
													*
												FROM
													WareHouseItems WI
												WHERE 
													WI.numItemID=I.numItemCode 
													AND WI.numWareHouseID = @numWarehouseID
												ORDER BY
													WI.numWareHouseItemID
											) AS WI
										) T1),0)

		
		INSERT INTO @TempPrice
		(
			bitSuccess
			,monPrice
			,monMSRPPrice
		)
		SELECT
			1
			,monFinalPrice
			,@monCalculatedPrice
		FROM
			dbo.fn_GetItemPriceBasedOnMethod(@numDomainID,@numDivisionID,@numItemCode,@numQty,1,@monCalculatedPrice,@numWarehouseItemID,@vcSelectedKitChildItems)
	END

	RETURN
END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='GetCalculatedPriceForKitAssembly')
DROP FUNCTION GetCalculatedPriceForKitAssembly
GO
CREATE FUNCTION [dbo].[GetCalculatedPriceForKitAssembly]
(
	@numDomainID NUMERIC(18,0)
	,@numDivisionID NUMERIC(18,0)
	,@numItemCode NUMERIC(18,0)
	,@numQty FLOAT
	,@numWarehouseItemID NUMERIC(18,0)
	,@tintKitAssemblyPriceBasedOn TINYINT
	,@numOppID NUMERIC(18,0)
	,@numOppItemID NUMERIC(18,0)
	,@vcSelectedKitChildItems VARCHAR(MAX)
)    
RETURNS DECIMAL(20,5)
AS    
BEGIN   
	DECLARE @monCalculatedPrice DECIMAL(20,5) = 0

	SELECT
		@monCalculatedPrice = ISNULL(monPrice,0)
	FROM
		dbo.fn_GetKitAssemblyCalculatedPrice
		(
			@numDomainID
			,@numDivisionID
			,@numItemCode
			,@numQty
			,@numWarehouseItemID
			,@tintKitAssemblyPriceBasedOn
			,@numOppID
			,@numOppItemID
			,@vcSelectedKitChildItems
		)    

	RETURN @monCalculatedPrice
END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='GetUnitPriceAfterPriceLevelApplication')
DROP FUNCTION GetUnitPriceAfterPriceLevelApplication
GO
CREATE FUNCTION [dbo].[GetUnitPriceAfterPriceLevelApplication]
(
	@numDomainID numeric(18,0), 
	@numItemCode NUMERIC(18,0), 
	@numQuantity FLOAT, 
	@numWarehouseItemID NUMERIC(18,0), 
	@isPriceRule BIT, 
	@numPriceRuleID NUMERIC(18,0),
	@numDivisionID NUMERIC(18,0)
)  
RETURNS FLOAT  
AS  
BEGIN  

DECLARE @finalUnitPrice FLOAT = 0
DECLARE @tintRuleType INT
DECLARE @tintDiscountType INT
DECLARE @decDiscount FLOAT
DECLARE @ItemPrice FLOAT

SET @tintRuleType = 0
SET @tintDiscountType = 0
SET @decDiscount  = 0
SET @ItemPrice = 0

DECLARE @TEMPPrice TABLE
(
	bitSuccess BIT
	,monPrice DECIMAL(20,5)
)

DECLARE @monCalculatePrice AS DECIMAL(20,5)

If (SELECT COUNT(*) FROM Item WHERE numDomainID = @numDomainID AND numItemCode = @numItemCode AND (bitKitParent = 1 OR bitAssembly = 1)) > 0
BEGIN
	INSERT INTO @TEMPPrice
	(
		bitSuccess
		,monPrice
	)
	SELECT
		bitSuccess
		,monPrice
	FROM
		dbo.fn_GetKitAssemblyCalculatedPrice(@numDomainID,@numDivisionID,@numItemCode,@numQuantity,@numWarehouseItemID,(SELECT ISNULL(tintKitAssemblyPriceBasedOn,1) FROM Item WHERE numDomainID = @numDomainID AND numItemCode = @numItemCode),0,0,'')

	IF (SELECT bitSuccess FROM @TEMPPrice) = 1
	BEGIN
		SET @monCalculatePrice = (SELECT monPrice FROM @TEMPPrice)
	END
END

IF @isPriceRule = 1
	SELECT TOP 1
		@tintRuleType = tintRuleType,
		@tintDiscountType = tintDiscountType,
		@decDiscount = decDiscount
	FROM 
		PricingTable 
	WHERE 
		numPriceRuleID = @numPriceRuleID AND
		(@numQuantity BETWEEN intFromQty AND intToQty)
ELSE
	SELECT TOP 1
		@tintRuleType = tintRuleType,
		@tintDiscountType = tintDiscountType,
		@decDiscount = decDiscount
	FROM 
		PricingTable 
	WHERE 
		numItemCode = @numItemCode AND
		((@numQuantity BETWEEN intFromQty AND intToQty) OR tintRuleType = 3)

IF @tintRuleType > 0 AND @tintDiscountType > 0
BEGIN
	IF @tintRuleType = 1 -- Deduct from List price
	BEGIN
		IF (SELECT ISNULL(charItemType,'') FROM Item WHERE numDomainID = @numDomainID AND numItemCode = @numItemCode) = 'P'
			If (SELECT COUNT(*) FROM Item WHERE numDomainID = @numDomainID AND numItemCode = @numItemCode AND (bitKitParent = 1 OR bitAssembly = 1)) > 0 -- Kit OR Assembly
				SELECT @ItemPrice = @monCalculatePrice
			ELSE
				SELECT @ItemPrice = ISNULL(monWListPrice,0) FROM WareHouseItems WHERE numDomainID = @numDomainID AND numItemID = @numItemCode AND numWareHouseItemID = @numWarehouseItemID
		ELSE
			SELECT @ItemPrice = ISNULL(monListPrice,0) FROM Item WHERE numDomainID = @numDomainID AND numItemCode = @numItemCode

		IF @tintDiscountType = 1 -- Percentage
		BEGIN
			SELECT @finalUnitPrice = @ItemPrice - (@ItemPrice * (@decDiscount/100))
		END
		ELSE IF @tintDiscountType = 2 -- Flat Amount
		BEGIN
				SELECT @finalUnitPrice = @ItemPrice - @decDiscount
		END
	END
	ELSE IF @tintRuleType = 2 -- Add to Primary Vendor Cost
	BEGIN
		If (SELECT COUNT(*) FROM Item WHERE numDomainID = @numDomainID AND numItemCode = @numItemCode AND (bitKitParent = 1 OR bitAssembly = 1)) > 0 -- Kit OR Assembly
			SELECT @ItemPrice = @monCalculatePrice
		ELSE
			SELECT @ItemPrice = ISNULL(monCost,0) FROM Vendor WHERE numItemCode = @numItemCode AND numVendorID = (SELECT numVendorID FROM Item WHERE numItemCode = @numItemCode)

		IF @tintDiscountType = 1 -- Percentage
		BEGIN
			SELECT @finalUnitPrice = @ItemPrice + (@ItemPrice * (@decDiscount/100))
		END
		ELSE IF @tintDiscountType = 2 -- Flat Amount
		BEGIN
			SELECT @finalUnitPrice = @ItemPrice + @decDiscount
		END
	END
	ELSE IF @tintRuleType = 3 -- Named Price
	BEGIN
		IF ISNULL(@numDivisionID,0) = 0
		BEGIN
			SELECT @finalUnitPrice = @decDiscount
		END
		ELSE
		BEGIN
			DECLARE @monPriceLevelPrice AS FLOAT = 0
			DECLARE @tintPriceLevel INT = 0
			SELECT @tintPriceLevel=ISNULL(tintPriceLevel,0) FROM DivisionMaster WHERE numDivisionID=@numDivisionID

			IF ISNULL(@tintPriceLevel,0) > 0
			BEGIN
				SET @decDiscount = 0

				SELECT 
					@monPriceLevelPrice = ISNULL(decDiscount,0)
				FROM
				(
					SELECT 
						ROW_NUMBER() OVER(PARTITION BY PricingTable.numItemCode ORDER BY numPricingID) Id,
						decDiscount
					FROM 
						PricingTable 
					INNER JOIN
						Item
					ON
						Item.numItemCode = @numItemCode 
						AND Item.numDomainID = @numDomainID
					WHERE 
						tintRuleType = 3 
				) TEMP
				WHERE
					Id BETWEEN @tintPriceLevel AND @tintPriceLevel

				IF ISNULL(@monPriceLevelPrice,0) > 0
				BEGIN
					SELECT @finalUnitPrice = @monPriceLevelPrice
				END
				ELSE
				BEGIN
					SELECT @finalUnitPrice = @decDiscount
				END
			END
			ELSE
			BEGIN
				SELECT @finalUnitPrice = @decDiscount
			END
		END
	END
END

return @finalUnitPrice
END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='GetUnitPriceAfterPriceRuleApplication')
DROP FUNCTION GetUnitPriceAfterPriceRuleApplication
GO
CREATE FUNCTION [dbo].[GetUnitPriceAfterPriceRuleApplication]
(
	@numRuleID NUMERIC(18,0), 
	@numDomainID numeric(18,0), 
	@numItemCode NUMERIC(18,0), 
	@numQuantity FLOAT, 
	@numWarehouseItemID NUMERIC(18,0),
	@numDivisionID NUMERIC(18,0)
)  
RETURNS FLOAT  
AS  
BEGIN  

DECLARE @finalUnitPrice FLOAT = 0
DECLARE @ItemPrice FLOAT = 0
DECLARE @tintRuleType INT
DECLARE @tintPricingMethod INT
DECLARE @tintDiscountType INT
DECLARE @decDiscount FLOAT
DECLARE @decMaxDiscount FLOAT
DECLARE @tempDecMaxDiscount FLOAT
DECLARE @decDiscountPerQty INT

SELECT 
	@tintPricingMethod=tintPricingMethod, 
	@tintRuleType = tintRuleType, 
	@tintDiscountType = tintDiscountType,
	@decDiscount = decDiscount,
	@decMaxDiscount = decMaxDedPerAmt,
	@decDiscountPerQty = intQntyItems
FROM 
	PriceBookRules 
WHERE numPricRuleID = @numRuleID

DECLARE @TEMPPrice TABLE
(
	bitSuccess BIT
	,monPrice DECIMAL(20,5)
)

DECLARE @monCalculatePrice AS DECIMAL(20,5)

If (SELECT COUNT(*) FROM Item WHERE numDomainID = @numDomainID AND numItemCode = @numItemCode AND (bitKitParent = 1 OR bitAssembly = 1)) > 0
BEGIN
	INSERT INTO @TEMPPrice
	(
		bitSuccess
		,monPrice
	)
	SELECT
		bitSuccess
		,monPrice
	FROM
		dbo.fn_GetKitAssemblyCalculatedPrice(@numDomainID,@numDivisionID,@numItemCode,@numQuantity,@numWarehouseItemID,(SELECT ISNULL(tintKitAssemblyPriceBasedOn,1) FROM Item WHERE numDomainID = @numDomainID AND numItemCode = @numItemCode),0,0,'')

	IF (SELECT bitSuccess FROM @TEMPPrice) = 1
	BEGIN
		SET @monCalculatePrice = (SELECT monPrice FROM @TEMPPrice)
	END
END

IF @tintPricingMethod = 1 --Use Pricing Table
BEGIN
	EXEC @finalUnitPrice = GetUnitPriceAfterPriceLevelApplication @numDomainID = @numDomainID, @numItemCode = @numItemCode, @numQuantity = @numQuantity, @numWarehouseItemID = @numWarehouseItemID,  @isPriceRule = 1, @numPriceRuleID = @numRuleID, @numDivisionID = 0 -- PASS 0 FOR DIVISION ID BECAUSE THIS PRICE TABLE IS FROM PRICE BOOK RULE
END
ELSE IF @tintPricingMethod = 2 --Use Pricing Formula
BEGIN
	IF @tintRuleType = 1 --Deduct from List price
	BEGIN
		IF (SELECT ISNULL(charItemType,'') FROM Item WHERE numItemCode = @numItemCode) = 'P'
			If (SELECT COUNT(*) FROM Item WHERE numDomainID = @numDomainID AND numItemCode = @numItemCode AND (bitKitParent = 1 OR bitAssembly = 1)) > 0 -- Kit OR Assembly
				SELECT @ItemPrice = @monCalculatePrice
			ELSE
				SELECT @ItemPrice = ISNULL(monWListPrice,0) FROM WareHouseItems WHERE numDomainID = @numDomainID AND numWareHouseItemID = @numWarehouseItemID
		ELSE
			SELECT @ItemPrice = ISNULL(monListPrice,0) FROM Item WHERE numDomainID = @numDomainID AND numItemCode = @numItemCode

		/* If price > 0 then apply rule calculation */
		IF @ItemPrice > 0
		BEGIN
			IF @decDiscount > 0
			BEGIN
				IF @numQuantity < @decDiscountPerQty
					SET @tempDecMaxDiscount = 1 * @decDiscount
				ELSE
					SET @tempDecMaxDiscount = (@numQuantity/@decDiscountPerQty) * @decDiscount

				IF @decMaxDiscount > 0 AND @tempDecMaxDiscount > @decMaxDiscount
					SET @tempDecMaxDiscount = @decMaxDiscount

				IF @tintDiscountType = 1 --Percentage
				BEGIN
					SELECT @finalUnitPrice = @ItemPrice - (@ItemPrice * (@tempDecMaxDiscount/100))
				END
				ELSE IF @tintDiscountType = 2 --Flat Amount
				BEGIN
					SELECT @finalUnitPrice = @ItemPrice - @tempDecMaxDiscount
				END
			END
		END
	END
	ELSE IF @tintRuleType = 2 --Add to Primary Vendor Cost
	BEGIN
		If (SELECT COUNT(*) FROM Item WHERE numDomainID = @numDomainID AND numItemCode = @numItemCode AND (bitKitParent = 1 OR bitAssembly = 1)) > 0 -- Kit OR Assembly
			SELECT @ItemPrice = @monCalculatePrice
		ELSE
			SELECT @ItemPrice = ISNULL(monCost,0) FROM Vendor WHERE numItemCode = @numItemCode AND numVendorID = (SELECT numVendorID FROM Item WHERE numItemCode = @numItemCode)
		/* If price > 0 then apply rule calculation */
		IF @ItemPrice > 0
		BEGIN
			IF @decDiscount > 0
			BEGIN
				IF @numQuantity < @decDiscountPerQty
					SET @tempDecMaxDiscount = 1 * @decDiscount
				ELSE
					SET @tempDecMaxDiscount = (@numQuantity/@decDiscountPerQty) * @decDiscount

				IF @decMaxDiscount > 0 AND @tempDecMaxDiscount > @decMaxDiscount
					SET @tempDecMaxDiscount = @decMaxDiscount

				IF @tintDiscountType = 1 --Percentage
				BEGIN
					SELECT @finalUnitPrice = @ItemPrice + (@ItemPrice * (@tempDecMaxDiscount/100))
				END
				ELSE IF @tintDiscountType = 2 --Flat Amount
				BEGIN
					SELECT @finalUnitPrice = @ItemPrice + @tempDecMaxDiscount
				END
			END
		END
	END
END


return @finalUnitPrice
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DemandForecast_Execute')
DROP PROCEDURE dbo.USP_DemandForecast_Execute
GO
CREATE PROCEDURE [dbo].[USP_DemandForecast_Execute]
	@numDFID NUMERIC (18,0)
	,@numUserCntID NUMERIC(18,0)
	,@ClientTimeZoneOffset INT
	,@CurrentPage INT
    ,@PageSize INT
	,@columnName VARCHAR(MAX)                                                          
	,@columnSortOrder VARCHAR(10)
	,@numDomainID NUMERIC(18,0)
	,@vcRegularSearchCriteria VARCHAR(MAX)=''
	,@bitShowAllItems BIT
	,@bitShowHistoricSales BIT
	,@numHistoricalAnalysisPattern BIT
	,@bitBasedOnLastYear BIT
	,@bitIncludeOpportunity BIT
	,@numOpportunityPercentComplete NUMERIC(18,0)
	,@tintDemandPlanBasedOn TINYINT
AS 
BEGIN
	DECLARE @dtLastExecution DATETIME

	IF (SELECT COUNT(*) FROM DemandForecast WHERE numDFID = @numDFID) > 0
	BEGIN
		DECLARE @tintUnitsRecommendationForAutoPOBackOrder TINYINT
		DECLARE @bitIncludeRequisitions BIT

		SELECT 
			@tintUnitsRecommendationForAutoPOBackOrder=ISNULL(tintUnitsRecommendationForAutoPOBackOrder,1)
			,@bitIncludeRequisitions=ISNULL(bitIncludeRequisitions,0)
		FROM 
			Domain 
		WHERE 
			numDomainID = @numDomainID

		DECLARE @bitWarehouseFilter BIT = 0
		DECLARE @bitItemClassificationFilter BIT = 0
		DECLARE @bitItemGroupFilter BIT = 0

		IF (SELECT COUNT(*) FROM DemandForecastWarehouse WHERE numDFID = @numDFID) > 0
		BEGIN
			SET @bitWarehouseFilter = 1
		END

		IF (SELECT COUNT(*) FROM DemandForecastItemClassification WHERE numDFID = @numDFID) > 0
		BEGIN
			SET @bitItemClassificationFilter = 1
		END

		IF (SELECT COUNT(*) FROM DemandForecastItemGroup WHERE numDFID = @numDFID) > 0
		BEGIN
			SET @bitItemGroupFilter = 1
		END				

		DECLARE @TEMP TABLE
		(
			numItemCode NUMERIC(18,0)
			,numWarehouseItemID NUMERIC(18,0)
			,numUnitHour FLOAT
			,dtReleaseDate DATE
		)

		INSERT INTO @TEMP
		(
			numItemCode
			,numWarehouseItemID
			,numUnitHour
			,dtReleaseDate
		)
		SELECT
			Item.numItemCode
			,OpportunityItems.numWarehouseItmsID
			,OpportunityItems.numUnitHour - ISNULL(OpportunityItems.numQtyShipped,0)
			,ISNULL(OpportunityItems.ItemReleaseDate,OpportunityMaster.dtReleaseDate)
		FROM
			OpportunityMaster
		INNER JOIN
			OpportunityItems
		ON
			OpportunityMaster.numOppId = OpportunityItems.numOppId
		INNER JOIN
			Item
		ON
			OpportunityItems.numItemCode = Item.numItemCode
		INNER JOIN
			WareHouseItems
		ON
			OpportunityItems.numWarehouseItmsID = WareHouseItems.numWareHouseItemID
		WHERE
			OpportunityMaster.numDomainId=@numDomainID
			AND OpportunityMaster.tintOppType=1
			AND OpportunityMaster.tintOppStatus=1
			AND ISNULL(OpportunityMaster.tintshipped,0)=0
			AND (ISNULL(OpportunityItems.numUnitHour,0)  - ISNULL(OpportunityItems.numQtyShipped,0)) > 0
			AND ISNULL(OpportunityItems.bitDropShip,0) = 0
			AND ISNULL(Item.IsArchieve,0) = 0
			AND ISNULL(Item.bitAssembly,0) = 0
			AND ISNULL(Item.bitKitParent,0) = 0
			AND (OpportunityItems.ItemReleaseDate IS NOT NULL OR OpportunityMaster.dtReleaseDate IS NOT NULL)
			AND
			(
				@bitWarehouseFilter = 0 OR
				WareHouseItems.numWareHouseID IN (SELECT numWareHouseID FROM DemandForecastWarehouse WHERE numDFID =@numDFID)
			)
			AND
			(
				@bitItemClassificationFilter = 0 OR
				Item.numItemClassification IN (SELECT numItemClassificationID FROM DemandForecastItemClassification WHERE numDFID = @numDFID)
			)
			AND
			(
				@bitItemGroupFilter = 0 OR
				Item.numItemGroup IN (SELECT numItemGroupID FROM DemandForecastItemGroup WHERE numDFID = @numDFID)
			)

		INSERT INTO @TEMP
		(
			numItemCode
			,numWarehouseItemID
			,numUnitHour
			,dtReleaseDate
		)
		SELECT
			Item.numItemCode
			,OKI.numWareHouseItemId
			,OKI.numQtyItemsReq - ISNULL(OKI.numQtyShipped,0)
			,ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate)
		FROM
			OpportunityMaster OM
		INNER JOIN
			OpportunityItems OI
		ON
			OM.numOppId = OI.numOppId
		INNER JOIN
			OpportunityKitItems OKI
		ON
			OI.numoppitemtCode=OKI.numOppItemID
		INNER JOIN
			Item
		ON
			OKI.numChildItemID = Item.numItemCode
		INNER JOIN
			WareHouseItems WI
		ON
			OKI.numWareHouseItemId = WI.numWareHouseItemID
		WHERE
			OM.numDomainId=@numDomainID
			AND OM.tintOppType=1
			AND OM.tintOppStatus=1
			AND ISNULL(OM.tintshipped,0)=0
			AND ISNULL(OI.bitDropShip,0) = 0
			AND ISNULL(OI.bitWorkOrder,0) = 0
			AND ISNULL(Item.IsArchieve,0) = 0
			AND ISNULL(Item.bitAssembly,0) = 0
			AND ISNULL(Item.bitKitParent,0) = 0
			AND (ISNULL(OKI.numQtyItemsReq,0) - ISNULL(OKI.numQtyShipped,0)) > 0
			AND (OM.dtReleaseDate IS NOT NULL OR OI.ItemReleaseDate IS NOT NULL)
			AND
			(
				@bitWarehouseFilter = 0 OR
				WI.numWareHouseID IN (SELECT numWareHouseID FROM DemandForecastWarehouse WHERE numDFID =@numDFID)
			)
			AND
			(
				@bitItemClassificationFilter = 0 OR
				Item.numItemClassification IN (SELECT numItemClassificationID FROM DemandForecastItemClassification WHERE numDFID = @numDFID)
			)
			AND
			(
				@bitItemGroupFilter = 0 OR
				Item.numItemGroup IN (SELECT numItemGroupID FROM DemandForecastItemGroup WHERE numDFID = @numDFID)
			)

		INSERT INTO @TEMP
		(
			numItemCode
			,numWarehouseItemID
			,numUnitHour
			,dtReleaseDate
		)
		SELECT
			Item.numItemCode
			,OKCI.numWareHouseItemId
			,OKCI.numQtyItemsReq - ISNULL(OKCI.numQtyShipped,0)
			,ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate)
		FROM
			OpportunityMaster OM
		INNER JOIN
			OpportunityItems OI
		ON
			OM.numOppId = OI.numOppId
		INNER JOIN
			OpportunityKitChildItems OKCI
		ON
			OI.numoppitemtCode = OKCI.numOppItemID
		INNER JOIN
			Item
		ON
			OKCI.numItemID = Item.numItemCode
		INNER JOIN
			WareHouseItems WI
		ON
			OKCI.numWareHouseItemId = WI.numWareHouseItemID
		WHERE
			OM.numDomainId=@numDomainID
			AND OM.tintOppType=1
			AND OM.tintOppStatus=1
			AND ISNULL(OM.tintshipped,0)=0
			AND ISNULL(OI.bitDropShip,0) = 0
			AND ISNULL(OI.bitWorkOrder,0) = 0
			AND ISNULL(Item.IsArchieve,0) = 0
			AND ISNULL(Item.bitAssembly,0) = 0
			AND ISNULL(Item.bitKitParent,0) = 0
			AND (ISNULL(OKCI.numQtyItemsReq,0) - ISNULL(OKCI.numQtyShipped,0)) > 0
			AND (OM.dtReleaseDate IS NOT NULL OR OI.ItemReleaseDate IS NOT NULL)
			AND
			(
				@bitWarehouseFilter = 0 OR
				WI.numWareHouseID IN (SELECT numWareHouseID FROM DemandForecastWarehouse WHERE numDFID =@numDFID)
			)
			AND
			(
				@bitItemClassificationFilter = 0 OR
				Item.numItemClassification IN (SELECT numItemClassificationID FROM DemandForecastItemClassification WHERE numDFID = @numDFID)
			)
			AND
			(
				@bitItemGroupFilter = 0 OR
				Item.numItemGroup IN (SELECT numItemGroupID FROM DemandForecastItemGroup WHERE numDFID = @numDFID)
			)

		INSERT INTO @TEMP
		(
			numItemCode
			,numWarehouseItemID
			,numUnitHour
			,dtReleaseDate
		)
		SELECT
			WOD.numChildItemID
			,WOD.numWareHouseItemId
			,WOD.numQtyItemsReq
			,(CASE WHEN OM.numOppId IS NOT NULL THEN ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate) ELSE WO.bintCompliationDate END)
		FROM
			WorkOrderDetails WOD
		INNER JOIN
			Item
		ON
			WOD.numChildItemID = Item.numItemCode
		INNER JOIN
			WareHouseItems WI
		ON
			WOD.numWareHouseItemId = WI.numWareHouseItemID
		INNER JOIN
			WorkOrder WO
		ON
			WOD.numWOId=WO.numWOId
		LEFT JOIN
			OpportunityItems OI
		ON
			WO.numOppItemID = OI.numoppitemtCode
		LEFT JOIN
			OpportunityMaster OM
		ON
			OI.numOppId=OM.numOppId
		WHERE
			WO.numDomainID=@numDomainID
			AND WO.numWOStatus <> 23184 -- NOT COMPLETED
			AND ISNULL(Item.IsArchieve,0) = 0
			AND ISNULL(Item.bitAssembly,0) = 0
			AND ISNULL(Item.bitKitParent,0) = 0
			AND ISNULL(WOD.numQtyItemsReq,0) > 0
			AND (OM.dtReleaseDate IS NOT NULL OR OI.ItemReleaseDate IS NOT NULL OR WO.bintCompliationDate IS NOT NULL)
			AND 1 = (CASE WHEN OM.numOppId IS NOT NULL THEN (CASE WHEN OM.tintOppStatus=1 THEN 1 ELSE 0 END) ELSE 1 END)
			AND
			(
				@bitWarehouseFilter = 0 OR
				WI.numWareHouseID IN (SELECT numWareHouseID FROM DemandForecastWarehouse WHERE numDFID =@numDFID)
			)
			AND
			(
				@bitItemClassificationFilter = 0 OR
				Item.numItemClassification IN (SELECT numItemClassificationID FROM DemandForecastItemClassification WHERE numDFID = @numDFID)
			)
			AND
			(
				@bitItemGroupFilter = 0 OR
				Item.numItemGroup IN (SELECT numItemGroupID FROM DemandForecastItemGroup WHERE numDFID = @numDFID)
			)

		CREATE TABLE #TEMPItems
		(
			numItemCode NUMERIC(18,0)
			,numWarehouseItemID NUMERIC(18,0)
			,numUnitHour FLOAT
			,dtReleaseDate VARCHAR(MAX)
		)

		INSERT INTO #TEMPItems
		(
			numItemCode
			,numWarehouseItemID
			,numUnitHour
			,dtReleaseDate
		)
		SELECT
			numItemCode
			,numWarehouseItemID
			,SUM(numUnitHour)
			,STUFF((SELECT 
						CONCAT(', <a href="#" onclick="return OpenDFReleaseDateRecords(',numItemCode,',',numWarehouseItemID,',''',CONVERT(VARCHAR(10), dtReleaseDate, 101),''');">',CONVERT(VARCHAR(10), dtReleaseDate, 101),'</a> (',SUM(numUnitHour),')')
					FROM 
						@TEMP
					WHERE 
						numItemCode=T1.numItemCode
						AND numWarehouseItemID=T1.numWarehouseItemID
					GROUP BY
						numItemCode
						,numWarehouseItemID
						,dtReleaseDate
					FOR XML PATH(''),TYPE).value('(./text())[1]','VARCHAR(MAX)')
				  ,1,2,'')
		FROM
			@TEMP T1
		GROUP BY
			numItemCode
			,numWarehouseItemID

		---------------------------- Dynamic Query -------------------------------
		
		CREATE TABLE #tempForm 
		(
			tintOrder TINYINT,vcDbColumnName nvarchar(50),vcOrigDbColumnName nvarchar(50),vcFieldName nvarchar(50),vcAssociatedControlType nvarchar(50),vcListItemType varchar(3),
			numListID numeric(9),vcLookBackTableName varchar(50),bitCustomField BIT,numFieldId NUMERIC,bitAllowSorting BIT,bitAllowEdit BIT,bitIsRequired BIT,bitIsEmail BIT,
			bitIsAlphaNumeric BIT,bitIsNumeric BIT,bitIsLengthValidation BIT,intMaxLength int,intMinLength int,bitFieldMessage BIT,vcFieldMessage VARCHAR(500),
			ListRelID numeric(9),intColumnWidth int,bitAllowFiltering BIT,vcFieldDataType CHAR(1)
		)

		-- FIRST CHECK IF GRID COLUMNS ARE CONFIGURED
		DECLARE  @Nocolumns  AS TINYINT = 0

		SELECT
			@Nocolumns=ISNULL(SUM(TotalRow),0) 
		FROM
		(            
			SELECT 
				COUNT(*) TotalRow 
			FROM 
				View_DynamicColumns 
			WHERE 
				numFormId=139 
				AND numUserCntID=@numUserCntID 
				AND numDomainID=@numDomainID 
				AND tintPageType=1 
				AND numRelCntType = 0
			UNION 
			SELECT 
				COUNT(*) TotalRow 
			FROM 
				View_DynamicCustomColumns 
			WHERE 
				numFormId=139 
				AND numUserCntID=@numUserCntID 
				AND numDomainID=@numDomainID 
				AND tintPageType=1 
				AND numRelCntType = 0
		) TotalRows

		IF @Nocolumns=0
		BEGIN
			INSERT INTO DycFormConfigurationDetails 
			(
				numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth
			)
			SELECT 
				139,numFieldId,0,Row_number() over(order by tintRow desc),@numDomainID,@numUserCntID,0,1,0,intColumnWidth
			FROM 
				View_DynamicDefaultColumns
			WHERE 
				numFormId=139 
				AND bitDefault=1 
				AND ISNULL(bitSettingField,0)=1 
				AND numDomainID=@numDomainID
			ORDER BY 
				tintOrder asc   
		END

		INSERT INTO 
			#tempForm
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcOrigDbColumnName,ISNULL(vcCultureFieldName,vcFieldName),                  
			vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName,bitCustom,numFieldId,bitAllowSorting,bitInlineEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength,bitFieldMessage,
			vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType  
		FROM 
			View_DynamicColumns 
		WHERE 
			numFormId=139 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND ISNULL(bitSettingField,0)=1 
			AND ISNULL(bitCustom,0)=0  
			AND numRelCntType = 0
		UNION
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName,vcAssociatedControlType,'' as vcListItemType,numListID,'',bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,'' 
		FROM 
			View_DynamicCustomColumns
		WHERE 
			numFormId=139 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND ISNULL(bitCustom,0)=1 
			AND numRelCntType = 0
		ORDER BY 
			tintOrder asc
					
		DECLARE  @strColumns AS VARCHAR(MAX) = ''
		SET @strColumns = CONCAT(' COUNT(*) OVER() TotalRowsCount, Item.numItemCode, Item.vcItemName, Item.vcSKU
		,ISNULL((SELECT vcUnitName FROM UOM WHERE numUOMId = Item.numBaseUnit),''-'') AS vcUOM
		,Item.charItemType AS vcItemType
		,(SELECT TOP 1 numContactId FROM AdditionalContactsInformation WHERE numDivisionId = Item.numVendorID ORDER BY ISNULL(bitPrimaryContact,0) DESC) AS numContactID
		,ISNULL(Item.numPurchaseUnit,0) AS numUOM
		,dbo.fn_UOMConversion(ISNULL(numBaseUnit, 0), Item.numItemCode, Item.numDomainId, ISNULL(Item.numPurchaseUnit, 0)) fltUOMConversionfactor
		,dbo.fn_GetAttributes(WI.numWareHouseItemId,1) AS vcAttributes
		,WI.numWareHouseItemID
		,W.vcWarehouse
		,ISNULL(V.numVendorID,0) numVendorID
		,ISNULL(V.intMinQty,0) intMinQty
		,ISNULL(monCost,0) * dbo.fn_UOMConversion(Item.numBaseUnit,Item.numItemCode,Item.numDomainID,Item.numPurchaseUnit) AS monVendorCost
		,T1.numUnitHour AS numQtyToPurchase, T1.dtReleaseDate,(CASE
			WHEN ',@tintUnitsRecommendationForAutoPOBackOrder,'= 3
			THEN 
				(ISNULL(numOnOrder,0) + (CASE WHEN ',@bitIncludeRequisitions,' = 1  
												THEN ISNULL((SELECT 
															SUM(numUnitHour) 
														FROM 
															OpportunityItems 
														INNER JOIN 
															OpportunityMaster 
														ON 
															OpportunityItems.numOppID=OpportunityMaster.numOppId 
														WHERE 
															OpportunityMaster.numDomainId=',@numDomainID,'
															AND OpportunityMaster.tintOppType=2 
															AND OpportunityMaster.tintOppStatus=0 
															AND OpportunityItems.numItemCode=Item.numItemCode 
															AND OpportunityItems.numWarehouseItmsID=WI.numWareHouseItemID),0) 
												ELSE 0 
												END)) - ISNULL(numBackOrder,0) +
				(CASE WHEN ISNULL(Item.fltReorderQty,0) >= ISNULL(V.intMinQty,0) THEN ISNULL(Item.fltReorderQty,0) ELSE ISNULL(V.intMinQty,0) END) 
			WHEN ',@tintUnitsRecommendationForAutoPOBackOrder,'=2
			THEN 
				(CASE
					WHEN ISNULL(Item.fltReorderQty,0) >= ISNULL(V.intMinQty,0) AND ISNULL(Item.fltReorderQty,0) >= ISNULL(numBackOrder,0) THEN ISNULL(Item.fltReorderQty,0)
					WHEN ISNULL(V.intMinQty,0) >= ISNULL(Item.fltReorderQty,0) AND ISNULL(V.intMinQty,0) >= ISNULL(numBackOrder,0) THEN ISNULL(V.intMinQty,0)
					WHEN ISNULL(numBackOrder,0) >= ISNULL(Item.fltReorderQty,0) AND ISNULL(numBackOrder,0) >= ISNULL(V.intMinQty,0) THEN ISNULL(numBackOrder,0)
					ELSE ISNULL(Item.fltReorderQty,0)
				END) - (ISNULL(numOnOrder,0) + (CASE WHEN ',@bitIncludeRequisitions,' = 1  THEN ISNULL((SELECT 
															SUM(numUnitHour) 
														FROM 
															OpportunityItems 
														INNER JOIN 
															OpportunityMaster 
														ON 
															OpportunityItems.numOppID=OpportunityMaster.numOppId 
														WHERE 
															OpportunityMaster.numDomainId=',@numDomainID,'
															AND OpportunityMaster.tintOppType=2 
															AND OpportunityMaster.tintOppStatus=0 
															AND OpportunityItems.numItemCode=Item.numItemCode 
															AND OpportunityItems.numWarehouseItmsID=WI.numWareHouseItemID),0) ELSE 0 END))
			ELSE
				(CASE WHEN ISNULL(Item.fltReorderQty,0) > ISNULL(V.intMinQty,0) THEN ISNULL(Item.fltReorderQty,0) ELSE ISNULL(V.intMinQty,0) END) 
				+ ISNULL(numBackOrder,0) - (ISNULL(numOnOrder,0) + (CASE WHEN ',@bitIncludeRequisitions,' = 1  THEN ISNULL((SELECT 
																														SUM(numUnitHour) 
																													FROM 
																														OpportunityItems 
																													INNER JOIN 
																														OpportunityMaster 
																													ON 
																														OpportunityItems.numOppID=OpportunityMaster.numOppId 
																													WHERE 
																														OpportunityMaster.numDomainId=',@numDomainID,'
																														AND OpportunityMaster.tintOppType=2 
																														AND OpportunityMaster.tintOppStatus=0 
																														AND OpportunityItems.numItemCode=Item.numItemCode 
																														AND OpportunityItems.numWarehouseItmsID=WI.numWareHouseItemID),0) ELSE 0 END)) 
		END) AS numQtyBasedOnPurchasePlan ')

		--Custom field 
		DECLARE @tintOrder AS TINYINT = 0                                                 
		DECLARE @vcFieldName AS VARCHAR(50)                                                  
		DECLARE @vcListItemType AS VARCHAR(3)                                             
		DECLARE @vcAssociatedControlType VARCHAR(20)     
		declare @numListID AS numeric(9)
		DECLARE @vcDbColumnName VARCHAR(50)                      
		DECLARE @WhereCondition VARCHAR(MAX) = ''                   
		DECLARE @vcLookBackTableName VARCHAR(2000)                
		DECLARE @bitCustom AS BIT
		DECLARE @bitAllowEdit AS CHAR(1)                   
		Declare @numFieldId as numeric  
		DECLARE @bitAllowSorting AS CHAR(1)    
		DECLARE @vcColumnName AS VARCHAR(500)             
		Declare @ListRelID as numeric(9) 
		DECLARE @SearchQuery AS VARCHAR(MAX) = '' 
	   
		SELECT TOP 1 
			@tintOrder=tintOrder+1
			,@vcDbColumnName=vcDbColumnName
			,@vcFieldName=vcFieldName
			,@vcAssociatedControlType=vcAssociatedControlType
			,@vcListItemType=vcListItemType
			,@numListID=numListID
			,@vcLookBackTableName=vcLookBackTableName
			,@bitCustom=bitCustomField
			,@numFieldId=numFieldId
			,@bitAllowSorting=bitAllowSorting
			,@bitAllowEdit=bitAllowEdit
			,@ListRelID=ListRelID
		FROM  
			#tempForm 
		ORDER BY 
			tintOrder ASC   

		WHILE @tintOrder>0                                                  
		BEGIN
	
			IF @bitCustom = 0  
			BEGIN
				DECLARE @Prefix AS VARCHAR(10)

				IF @vcLookBackTableName = 'AdditionalContactsInformation'
					SET @Prefix = 'ADC.'
				ELSE IF @vcLookBackTableName = 'DivisionMaster'
					SET @Prefix = 'Div.'
				ELSE IF @vcLookBackTableName = 'OpportunityMaster'
					SET @PreFix = 'Opp.'			
				ELSE IF @vcLookBackTableName = 'CompanyInfo'
					SET @PreFix = 'cmp.'
				ELSE IF @vcLookBackTableName = 'DemandForecastGrid'
					SET @PreFix = 'DF.'
				ELSE IF @vcLookBackTableName = 'Warehouses'
					SET @PreFix = 'W.'
				ELSE IF @vcLookBackTableName = 'WareHouseItems'
					SET @PreFix = 'WI.'
				ELSE 
					SET @PreFix = CONCAT(@vcLookBackTableName,'.')
			
				SET @vcColumnName= @vcDbColumnName+'~'+ convert(varchar(10),@numFieldId) +'~0'

				IF @vcAssociatedControlType = 'TextBox'
				BEGIN
					IF @vcDbColumnName = 'numQtyToPurchase'
					BEGIN
						SET @strColumns=@strColumns+',' + ' (CASE WHEN ISNULL(WI.numBackOrder,0) > ISNULL(WI.numOnOrder,0) THEN  CEILING(ISNULL(WI.numBackOrder,0) - ISNULL(WI.numOnOrder,0))  ELSE 0 END) '  + ' [' + @vcColumnName + ']'
					END
					ELSE IF @vcDbColumnName = 'moncost'
					BEGIN
						SET @strColumns=@strColumns+',' + ' ISNULL((SELECT vcUnitName FROM UOM WHERE numUOMId = Item.numBaseUnit),''-'') '  + ' [' + @vcColumnName + ']'
					END
					ELSE
					BEGIN
						SET @strColumns=@strColumns+','+  @Prefix + @vcDbColumnName +' ['+ @vcColumnName+']' 
					END
				END
				ELSE IF  @vcAssociatedControlType='Label'                                              
				BEGIN
					IF @vcDbColumnName = 'vc7Days'
					BEGIN
						SET @strColumns=@strColumns+ CONCAT(',dbo.fn_DemandForecastDaysDisplay(',@numDomainID,',',@ClientTimeZoneOffset,',Item.numItemCode,ISNULL(TempLeadDays.numLeadDays,0),WI.numWarehouseItemID,7,',@bitShowHistoricSales,',',@numHistoricalAnalysisPattern,',',@bitBasedOnLastYear,',',@bitIncludeOpportunity,',',@numOpportunityPercentComplete,',',@tintDemandPlanBasedOn,') [',@vcColumnName,']')
					END
					ELSE IF @vcDbColumnName = 'vc15Days'
					BEGIN
						SET @strColumns=@strColumns+ CONCAT(',dbo.fn_DemandForecastDaysDisplay(',@numDomainID,',',@ClientTimeZoneOffset,',Item.numItemCode,ISNULL(TempLeadDays.numLeadDays,0),WI.numWarehouseItemID,15,',@bitShowHistoricSales,',',@numHistoricalAnalysisPattern,',',@bitBasedOnLastYear,',',@bitIncludeOpportunity,',',@numOpportunityPercentComplete,',',@tintDemandPlanBasedOn,') [',@vcColumnName,']')
					END
					ELSE IF @vcDbColumnName = 'vc30Days'
					BEGIN
						SET @strColumns=@strColumns+ CONCAT(',dbo.fn_DemandForecastDaysDisplay(',@numDomainID,',',@ClientTimeZoneOffset,',Item.numItemCode,ISNULL(TempLeadDays.numLeadDays,0),WI.numWarehouseItemID,30,',@bitShowHistoricSales,',',@numHistoricalAnalysisPattern,',',@bitBasedOnLastYear,',',@bitIncludeOpportunity,',',@numOpportunityPercentComplete,',',@tintDemandPlanBasedOn,') [',@vcColumnName,']')
					END
					ELSE IF @vcDbColumnName = 'vc60Days'
					BEGIN
						SET @strColumns=@strColumns+ CONCAT(',dbo.fn_DemandForecastDaysDisplay(',@numDomainID,',',@ClientTimeZoneOffset,',Item.numItemCode,ISNULL(TempLeadDays.numLeadDays,0),WI.numWarehouseItemID,60,',@bitShowHistoricSales,',',@numHistoricalAnalysisPattern,',',@bitBasedOnLastYear,',',@bitIncludeOpportunity,',',@numOpportunityPercentComplete,',',@tintDemandPlanBasedOn,') [',@vcColumnName,']')
					END
					ELSE IF @vcDbColumnName = 'vc90Days'
					BEGIN
						SET @strColumns=@strColumns+ CONCAT(',dbo.fn_DemandForecastDaysDisplay(',@numDomainID,',',@ClientTimeZoneOffset,',Item.numItemCode,ISNULL(TempLeadDays.numLeadDays,0),WI.numWarehouseItemID,90,',@bitShowHistoricSales,',',@numHistoricalAnalysisPattern,',',@bitBasedOnLastYear,',',@bitIncludeOpportunity,',',@numOpportunityPercentComplete,',',@tintDemandPlanBasedOn,') [',@vcColumnName,']')
					END
					ELSE IF @vcDbColumnName = 'vc180Days'
					BEGIN
						SET @strColumns=@strColumns+ CONCAT(',dbo.fn_DemandForecastDaysDisplay(',@numDomainID,',',@ClientTimeZoneOffset,',Item.numItemCode,ISNULL(TempLeadDays.numLeadDays,0),WI.numWarehouseItemID,180,',@bitShowHistoricSales,',',@numHistoricalAnalysisPattern,',',@bitBasedOnLastYear,',',@bitIncludeOpportunity,',',@numOpportunityPercentComplete,',',@tintDemandPlanBasedOn,') [',@vcColumnName,']')
					END
					ELSE IF (@vcDbColumnName = 'vcBuyUOM')
					BEGIN
						SET @strColumns=@strColumns+',' + ' ISNULL((SELECT vcUnitName FROM UOM WHERE numUOMId = ISNULL(Item.numPurchaseUnit,0)),'''')' + ' [' + @vcColumnName + ']'
					END
					ELSE IF @vcDbColumnName = 'numOnOrderReq'
					BEGIN
						SET @strColumns=@strColumns+',' + ' (ISNULL(WI.numOnOrder,0) + ISNULL(WI.numReorder,0)) '  + ' [' + @vcColumnName + ']'
					END
					ELSE IF @vcDbColumnName = 'numAvailable'
					BEGIN
						SET @strColumns=@strColumns+',' + ' ISNULL(WI.numOnHand,0) '  + ' [' + @vcColumnName + ']'
					END
					ELSE
					BEGIN
						SET @strColumns=@strColumns + ',' + @Prefix + @vcDbColumnName  +' ['+ @vcColumnName+']' 
					END   
				END
				ELSE IF  @vcAssociatedControlType='SelectBox' or @vcAssociatedControlType='ListBox'  
				BEGIN
					IF @vcDbColumnName = 'numVendorID'
					BEGIN
						SET @strColumns=@strColumns+',' + ' V.numVendorID '  + ' [' + @vcColumnName + ']'
					END
					ELSE IF @vcDbColumnName = 'ShipmentMethod'
					BEGIN
						SET @strColumns=@strColumns+',' + ' '''' '  + ' [' + @vcColumnName + ']'
					END
					ELSE
					BEGIN
						SET @strColumns=@strColumns + ',' + @Prefix + @vcDbColumnName  +' ['+ @vcColumnName+']' 
					END
				END
				ELSE IF @vcAssociatedControlType='DateField'   
				BEGIN
					IF @vcDbColumnName = 'dtExpectedDelivery'
					BEGIN
						SET @strColumns=@strColumns+',' + ' GETDATE() '  + ' [' + @vcColumnName + ']'
					END
					ELSE IF @vcDbColumnName = 'ItemRequiredDate'
					BEGIN
						SET @strColumns=@strColumns+',T1.dtReleaseDate [' + @vcColumnName + ']'
					END
					ELSE
					BEGIN
						SET @strColumns=@strColumns + ',' + @Prefix + @vcDbColumnName  +' ['+ @vcColumnName+']' 
					END					
				END
			END

			SELECT TOP 1 
				@tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName,@vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,
				@numListID=numListID,@vcLookBackTableName=vcLookBackTableName,@bitCustom=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,
				@bitAllowEdit=bitAllowEdit,@ListRelID=ListRelID
			FROM
				#tempForm 
			WHERE 
				tintOrder > @tintOrder-1 
			ORDER BY 
				tintOrder asc            
 
			IF @@rowcount=0 SET @tintOrder=0 

		 END
	  
		DECLARE @StrSql AS VARCHAR(MAX) = ''
		SET @StrSql = CONCAT('SELECT ',@strColumns,' 
							FROM
								#TEMPItems T1
							INNER JOIN
								Item
							ON
								T1.numItemCode = Item.numItemCode
							INNER JOIN
								WarehouseItems WI
							ON
								Item.numItemCode=WI.numItemID
								AND T1.numWarehouseItemID=WI.numWarehouseItemID
							INNER JOIN
								Warehouses W
							ON
								WI.numWarehouseID = W.numWareHouseID
							LEFT JOIN
								Vendor V
							ON
								Item.numVendorID = V.numVendorID
								AND Item.numItemCode = V.numItemCode
							OUTER APPLY
							(
								SELECT TOP 1
									numListValue AS numLeadDays
								FROM
									VendorShipmentMethod VSM
								WHERE
									VSM.numVendorID = V.numVendorID
								ORDER BY
									ISNULL(bitPrimary,0) DESC, bitPreferredMethod DESC, numListItemID ASC
							) TempLeadDays
							WHERE
								Item.numDomainID=',@numDomainID,'
								AND 1 = (CASE 
											WHEN ',@bitShowAllItems,' = 1 
											THEN 1 
											ELSE 
												(CASE 
													WHEN 
														T1.numUnitHour > 0 
													THEN 1 
													ELSE 0 
												END)
										END)
							')

		IF CHARINDEX('I.',@vcRegularSearchCriteria) > 0
		BEGIN			   
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'I.','Item.')
		END
		IF CHARINDEX('WH.',@vcRegularSearchCriteria) > 0
		BEGIN			   
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'WH.','W.')
		END
	
		IF @vcRegularSearchCriteria<>'' 
		BEGIN
			SET @strSql=@strSql +' AND ' + @vcRegularSearchCriteria 
		END
	  
		DECLARE  @firstRec  AS INTEGER
		DECLARE  @lastRec  AS INTEGER
  
		SET @firstRec = (@CurrentPage - 1) * @PageSize
		SET @lastRec = (@CurrentPage * @PageSize + 1)

		-- EXECUTE FINAL QUERY
		DECLARE @strFinal AS NVARCHAR(MAX)
		SET @strFinal = CONCAT(@StrSql,' ORDER BY ',CASE WHEN CHARINDEX(@columnName,@strColumns) > 0 THEN @columnName ELSE 'Item.numItemCode' END,' OFFSET ',(@CurrentPage-1) * @PageSize,' ROWS FETCH NEXT ',@PageSize,' ROWS ONLY')

		PRINT CAST(@strFinal AS NTEXT)
		EXEC sp_executesql @strFinal

		SELECT * FROM #tempForm

		DROP TABLE #tempForm

		DROP TABLE #TEMPItems


		UPDATE DemandForecast SET dtExecutionDate = dateadd(MINUTE,-@ClientTimeZoneOffset,GETDATE()) WHERE numDFID = @numDFID

		SELECT CONVERT(VARCHAR(20),dateadd(MINUTE,-@ClientTimeZoneOffset,@dtLastExecution)) AS dtLastExecution
	END
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DemandForecast_GetRecordDetails')
DROP PROCEDURE dbo.USP_DemandForecast_GetRecordDetails
GO
CREATE PROCEDURE [dbo].[USP_DemandForecast_GetRecordDetails]
	@numDomainID NUMERIC(18,0)
	,@tintType TINYINT
	,@numItemCode NUMERIC(18,0)
	,@numWarehouseItemID NUMERIC(18,0)
	,@numForecastDays INT
	,@numAnalysisDays INT
	,@bitBasedOnLastYear BIT
	,@numOpportunityPercentComplete NUMERIC(18,0)
	,@CurrentPage INT
    ,@PageSize INT
	,@ClientTimeZoneOffset INT
AS 
BEGIN
	DECLARE @TEMP TABLE
	(
		numOppID NUMERIC(18,0)
		,numWOID NUMERIC(18,0)
		,dtCreatedDate DATETIME
		,vcOppName VARCHAR(500)
		,numOrderedQty FLOAT
		,numQtyToShip FLOAT
		,numOpportunityForecastQty FLOAT
		,dtReleaseDate VARCHAR(50)
	)

	IF @tintType = 1 -- Release Dates
	BEGIN
		INSERT INTO @TEMP
		(
			numOppID
			,numWOID
			,dtCreatedDate
			,vcOppName
			,numOrderedQty
			,numQtyToShip
			,numOpportunityForecastQty
			,dtReleaseDate
		)
		SELECT
			OpportunityMaster.numOppId
			,0
			,OpportunityMaster.bintCreatedDate
			,OpportunityMaster.vcPOppName
			,OpportunityItems.numUnitHour
			,OpportunityItems.numUnitHour - ISNULL(numQtyShipped,0)
			,0
			,dbo.FormatedDateFromDate(ISNULL(OpportunityItems.ItemReleaseDate,OpportunityMaster.dtReleaseDate),@numDomainID)
		FROM
			OpportunityMaster
		INNER JOIN
			OpportunityItems
		ON
			OpportunityMaster.numOppId = OpportunityItems.numOppId
		INNER JOIN
			Item
		ON
			OpportunityItems.numItemCode = Item.numItemCode
		WHERE
			OpportunityMaster.numDomainId=@numDomainID
			AND OpportunityMaster.tintOppType=1
			AND OpportunityMaster.tintOppStatus=1
			AND ISNULL(OpportunityMaster.tintshipped,0)=0
			AND (ISNULL(OpportunityItems.numUnitHour,0)  - ISNULL(OpportunityItems.numQtyShipped,0)) > 0
			AND ISNULL(OpportunityItems.bitDropShip,0) = 0
			AND ISNULL(Item.IsArchieve,0) = 0
			AND ISNULL(Item.bitAssembly,0) = 0
			AND ISNULL(Item.bitKitParent,0) = 0
			AND (OpportunityItems.ItemReleaseDate IS NOT NULL OR OpportunityMaster.dtReleaseDate IS NOT NULL)
			AND OpportunityItems.numItemCode=@numItemCode
			AND OpportunityItems.numWarehouseItmsID=@numWarehouseItemID
			AND ISNULL(OpportunityItems.ItemReleaseDate,OpportunityMaster.dtReleaseDate) <= DATEADD(DAY,@numForecastDays,DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()))

		-- USED IN KIT
		INSERT INTO @TEMP
		(
			numOppID
			,numWOID
			,dtCreatedDate
			,vcOppName
			,numOrderedQty
			,numQtyToShip
			,numOpportunityForecastQty
			,dtReleaseDate
		)
		SELECT
			OM.numOppId
			,0
			,OM.bintCreatedDate
			,CONCAT(OM.vcPOppName,' (Kit Memeber)')
			,OKI.numQtyItemsReq
			,OKI.numQtyItemsReq - ISNULL(OKI.numQtyShipped,0)
			,0
			,dbo.FormatedDateFromDate(ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate),@numDomainID)
		FROM
			OpportunityMaster OM
		INNER JOIN
			OpportunityItems OI
		ON
			OM.numOppId = OI.numOppId
		INNER JOIN
			OpportunityKitItems OKI
		ON
			OI.numoppitemtCode=OKI.numOppItemID
		INNER JOIN
			Item
		ON
			OKI.numChildItemID = Item.numItemCode
		INNER JOIN
			WareHouseItems WI
		ON
			OKI.numWareHouseItemId = WI.numWareHouseItemID
		WHERE
			OM.numDomainId=@numDomainID
			AND OM.tintOppType=1
			AND OM.tintOppStatus=1
			AND ISNULL(OM.tintshipped,0)=0
			AND ISNULL(OI.bitDropShip,0) = 0
			AND ISNULL(OI.bitWorkOrder,0) = 0
			AND ISNULL(Item.IsArchieve,0) = 0
			AND ISNULL(Item.bitAssembly,0) = 0
			AND ISNULL(Item.bitKitParent,0) = 0
			AND (ISNULL(OKI.numQtyItemsReq,0) - ISNULL(OKI.numQtyShipped,0)) > 0
			AND (OM.dtReleaseDate IS NOT NULL OR OI.ItemReleaseDate IS NOT NULL)
			AND OKI.numChildItemID=@numItemCode
			AND OKI.numWareHouseItemId=@numWarehouseItemID
			AND ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate) <= DATEADD(DAY,@numForecastDays,DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()))

		-- USED IN KIT WITHIN KIT
		INSERT INTO @TEMP
		(
			numOppID
			,numWOID
			,dtCreatedDate
			,vcOppName
			,numOrderedQty
			,numQtyToShip
			,numOpportunityForecastQty
			,dtReleaseDate
		)
		SELECT
			OM.numOppId
			,0
			,OM.bintCreatedDate
			,CONCAT(OM.vcPOppName,' (Kit Memeber)')
			,OKCI.numQtyItemsReq
			,OKCI.numQtyItemsReq - ISNULL(OKCI.numQtyShipped,0)
			,0
			,dbo.FormatedDateFromDate(ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate),@numDomainID)
		FROM
			OpportunityMaster OM
		INNER JOIN
			OpportunityItems OI
		ON
			OM.numOppId = OI.numOppId
		INNER JOIN
			OpportunityKitChildItems OKCI
		ON
			OI.numoppitemtCode = OKCI.numOppItemID
		INNER JOIN
			Item
		ON
			OKCI.numItemID = Item.numItemCode
		INNER JOIN
			WareHouseItems WI
		ON
			OKCI.numWareHouseItemId = WI.numWareHouseItemID
		WHERE
			OM.numDomainId=@numDomainID
			AND OM.tintOppType=1
			AND OM.tintOppStatus=1
			AND ISNULL(OM.tintshipped,0)=0
			AND ISNULL(OI.bitDropShip,0) = 0
			AND ISNULL(OI.bitWorkOrder,0) = 0
			AND ISNULL(Item.IsArchieve,0) = 0
			AND ISNULL(Item.bitAssembly,0) = 0
			AND ISNULL(Item.bitKitParent,0) = 0
			AND (ISNULL(OKCI.numQtyItemsReq,0) - ISNULL(OKCI.numQtyShipped,0)) > 0
			AND (OM.dtReleaseDate IS NOT NULL OR OI.ItemReleaseDate IS NOT NULL)
			AND OKCI.numItemID=@numItemCode
			AND OKCI.numWareHouseItemId=@numWarehouseItemID
			AND ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate) <= DATEADD(DAY,@numForecastDays,DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()))
		
		-- USED IN WORK ORDER
		INSERT INTO @TEMP
		(
			numOppID
			,numWOID
			,dtCreatedDate
			,vcOppName
			,numOrderedQty
			,numQtyToShip
			,numOpportunityForecastQty
			,dtReleaseDate
		)
		SELECT
			OM.numOppId
			,WO.numWOId
			,CASE WHEN OM.numOppId IS NOT NULL THEN OM.bintCreatedDate ELSE WO.bintCreatedDate END
			,CONCAT(CASE WHEN OM.numOppId IS NOT NULL THEN OM.vcPOppName ELSE 'Work Order' END,' (Assembly Memeber)') 
			,WOD.numQtyItemsReq
			,WOD.numQtyItemsReq 
			,0
			,CASE WHEN OM.numOppId IS NOT NULL THEN dbo.FormatedDateFromDate(ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate),@numDomainID) ELSE dbo.FormatedDateFromDate(WO.bintCompliationDate,@numDomainID) END
		FROM
			WorkOrderDetails WOD
		INNER JOIN
			Item
		ON
			WOD.numChildItemID = Item.numItemCode
		INNER JOIN
			WareHouseItems WI
		ON
			WOD.numWareHouseItemId = WI.numWareHouseItemID
		INNER JOIN
			WorkOrder WO
		ON
			WOD.numWOId=WO.numWOId
		LEFT JOIN
			OpportunityItems OI
		ON
			WO.numOppItemID = OI.numoppitemtCode
		LEFT JOIN
			OpportunityMaster OM
		ON
			OI.numOppId=OM.numOppId
		WHERE
			WO.numDomainID=@numDomainID
			AND WO.numWOStatus <> 23184 -- NOT COMPLETED
			AND ISNULL(Item.IsArchieve,0) = 0
			AND ISNULL(Item.bitAssembly,0) = 0
			AND ISNULL(Item.bitKitParent,0) = 0
			AND ISNULL(WOD.numQtyItemsReq,0) > 0
			AND (OM.dtReleaseDate IS NOT NULL OR OI.ItemReleaseDate IS NOT NULL OR WO.bintCompliationDate IS NOT NULL)
			AND 1 = (CASE WHEN OM.numOppId IS NOT NULL THEN (CASE WHEN OM.tintOppStatus=1 THEN 1 ELSE 0 END) ELSE 1 END)
			AND WOD.numChildItemID = @numItemCode
			AND WOD.numWareHouseItemId=@numWarehouseItemID
			AND 1 = (CASE 
					WHEN OI.numoppitemtCode IS NOT NULL 
					THEN (CASE 
							WHEN ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate) <= DATEADD(DAY,@numForecastDays,DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE())) 
							THEN 1 
							ELSE 0 
						END)
					ELSE 
						(CASE 
						WHEN WO.bintCompliationDate <= DATEADD(DAY,@numForecastDays,DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE())) 
						THEN 1 
						ELSE 0 
						END)
					END)
	END
	ELSE IF @tintType = 2 -- Historical Sales
	BEGIN
		DECLARE @dtFromDate DATETIME
		DECLARE @dtToDate DATETIME

		IF ISNULL(@bitBasedOnLastYear,0) = 1 --last week from last year
		BEGIN
			SET @dtToDate = DATEADD(yy,-1,DATEADD(d,-1,GETUTCDATE()))
			SET @dtFromDate = DATEADD(yy,-1,DATEADD(d,-(@numAnalysisDays),GETUTCDATE()))
		END
		ELSE
		BEGIN
			SET @dtToDate = DATEADD(d,-1,GETUTCDATE())
			SET @dtFromDate = DATEADD(d,-(@numAnalysisDays),GETUTCDATE())
		END

		INSERT INTO @TEMP
		(
			numOppID
			,numWOID
			,dtCreatedDate
			,vcOppName
			,numOrderedQty
			,numQtyToShip
			,numOpportunityForecastQty
			,dtReleaseDate
		)
		SELECT
			OM.numOppId
			,0
			,OM.bintCreatedDate
			,OM.vcPOppName
			,OI.numUnitHour
			,OI.numUnitHour - ISNULL(numQtyShipped,0)
			,0
			,dbo.FormatedDateFromDate(ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate),@numDomainID)
		FROM
			OpportunityItems OI
		INNER JOIN
			OpportunityMaster OM
		ON
			OI.numOppId=OM.numOppId
		WHERE
			OM.numDomainId=@numDomainID
			AND OM.tintOppType=1
			AND OM.tintOppStatus=1
			AND numItemCode=@numItemCode
			AND numWarehouseItmsID=@numWarehouseItemID
			AND ISNULL(bitDropship,0) = 0
			AND CAST(OM.bintCreatedDate AS DATE) BETWEEN CONVERT(DATE,@dtFromDate) AND CONVERT(DATE,@dtToDate)

		-- USED IN KIT
		INSERT INTO @TEMP
		(
			numOppID
			,numWOID
			,dtCreatedDate
			,vcOppName
			,numOrderedQty
			,numQtyToShip
			,numOpportunityForecastQty
			,dtReleaseDate
		)
		SELECT
			OM.numOppId
			,0
			,OM.bintCreatedDate
			,CONCAT(OM.vcPOppName,' (Kit Memeber)')
			,OKI.numQtyItemsReq
			,OKI.numQtyItemsReq - ISNULL(OKI.numQtyShipped,0)
			,0
			,dbo.FormatedDateFromDate(ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate),@numDomainID)
		FROM
			OpportunityKitItems OKI
		INNER JOIN
			OpportunityItems OI
		ON
			OKI.numOppItemID=OI.numoppitemtCode
		INNER JOIN
			OpportunityMaster OM
		ON
			OI.numOppId=OM.numOppId
		WHERE
			OM.numDomainId=@numDomainID
			AND OM.tintOppType=1
			AND OM.tintOppStatus=1
			AND OKI.numChildItemID=@numItemCode
			AND OKI.numWareHouseItemId=@numWarehouseItemID
			AND ISNULL(OI.bitWorkOrder,0) = 0
			AND CAST(OM.bintCreatedDate AS DATE) BETWEEN CONVERT(DATE,@dtFromDate) AND CONVERT(DATE,@dtToDate)

		-- USED IN KIT WITHIN KIT
		INSERT INTO @TEMP
		(
			numOppID
			,numWOID
			,dtCreatedDate
			,vcOppName
			,numOrderedQty
			,numQtyToShip
			,numOpportunityForecastQty
			,dtReleaseDate
		)
		SELECT
			OM.numOppId
			,0
			,OM.bintCreatedDate
			,CONCAT(OM.vcPOppName,' (Kit Memeber)')
			,OKCI.numQtyItemsReq
			,OKCI.numQtyItemsReq - ISNULL(OKCI.numQtyShipped,0)
			,0
			,dbo.FormatedDateFromDate(ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate),@numDomainID)
		FROM
			OpportunityKitChildItems OKCI
		INNER JOIN
			OpportunityItems OI
		ON
			OKCI.numOppItemID=OI.numoppitemtCode
		INNER JOIN
			OpportunityMaster OM
		ON
			OI.numOppId=OM.numOppId
		WHERE
			OM.numDomainId=@numDomainID
			AND OM.tintOppType=1
			AND OM.tintOppStatus=1
			AND OKCI.numItemID=@numItemCode
			AND OKCI.numWareHouseItemId=@numWarehouseItemID
			AND ISNULL(OI.bitWorkOrder,0) = 0
			AND CAST(OM.bintCreatedDate AS DATE) BETWEEN CONVERT(DATE,@dtFromDate) AND CONVERT(DATE,@dtToDate)

		-- USED IN WORK ORDER
		INSERT INTO @TEMP
		(
			numOppID
			,numWOID
			,dtCreatedDate
			,vcOppName
			,numOrderedQty
			,numQtyToShip
			,numOpportunityForecastQty
			,dtReleaseDate
		)
		SELECT
			OM.numOppId
			,WO.numWOId
			,CASE WHEN OM.numOppId IS NOT NULL THEN OM.bintCreatedDate ELSE WO.bintCreatedDate END
			,CONCAT(CASE WHEN OM.numOppId IS NOT NULL THEN OM.vcPOppName ELSE 'Work Order' END,' (Assembly Memeber)') 
			,WOD.numQtyItemsReq
			,(CASE WHEN WO.numWOStatus = 23184 THEN 0 ELSE WOD.numQtyItemsReq END)
			,0
			,CASE WHEN OM.numOppId IS NOT NULL THEN dbo.FormatedDateFromDate(ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate),@numDomainID) ELSE dbo.FormatedDateFromDate(WO.bintCompliationDate,@numDomainID) END
		FROM
			WorkOrderDetails WOD
		INNER JOIN
			WorkOrder WO
		ON
			WOD.numWOId=WO.numWOId
		LEFT JOIN
			OpportunityItems OI
		ON
			WO.numOppItemID = OI.numoppitemtCode
		LEFT JOIN
			OpportunityMaster OM
		ON
			OI.numOppId=OM.numOppId
		WHERE
			WO.numDomainID=@numDomainID
			AND WOD.numChildItemID = @numItemCode
			AND WOD.numWareHouseItemId=@numWarehouseItemID
			AND CAST(WO.bintCreatedDate AS DATE) BETWEEN CONVERT(DATE,@dtFromDate) AND CONVERT(DATE,@dtToDate)
	END
	ELSE IF @tintType = 3 -- Sales Opportunity
	BEGIN
		INSERT INTO @TEMP
		(
			numOppID
			,numWOID
			,dtCreatedDate
			,vcOppName
			,numOrderedQty
			,numQtyToShip
			,numOpportunityForecastQty
			,dtReleaseDate
		)
		SELECT
			OM.numOppId
			,0
			,OM.bintCreatedDate
			,OM.vcPOppName
			,OI.numUnitHour
			,OI.numUnitHour
			,OI.numUnitHour * (PP.intTotalProgress/100)
			,dbo.FormatedDateFromDate(ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate),@numDomainID)
		FROM
			OpportunityItems OI
		INNER JOIN
			OpportunityMaster OM
		ON
			OI.numOppId=OM.numOppId
		INNER JOIN
			ProjectProgress PP
		ON
			OM.numOppId = PP.numOppId
		WHERE
			OM.numDomainId=@numDomainID
			AND OM.tintOppType=1
			AND OM.tintOppStatus=0
			AND numItemCode=@numItemCode
			AND numWarehouseItmsID=@numWarehouseItemID
			AND ISNULL(bitDropship,0) = 0
			AND (OM.dtReleaseDate IS NOT NULL OR OI.ItemReleaseDate IS NOT NULL)
			AND ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate) <= DATEADD(DAY,@numForecastDays,DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()))
			AND PP.intTotalProgress >= @numOpportunityPercentComplete

		-- USED IN KIT
		INSERT INTO @TEMP
		(
			numOppID
			,numWOID
			,dtCreatedDate
			,vcOppName
			,numOrderedQty
			,numQtyToShip
			,numOpportunityForecastQty
			,dtReleaseDate
		)
		SELECT
			OM.numOppId
			,0
			,OM.bintCreatedDate
			,CONCAT(OM.vcPOppName,' (Kit Memeber)')
			,OKI.numQtyItemsReq
			,OKI.numQtyItemsReq
			,OKI.numQtyItemsReq * (PP.intTotalProgress/100)
			,dbo.FormatedDateFromDate(ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate),@numDomainID)
		FROM
			OpportunityKitItems OKI
		INNER JOIN
			OpportunityItems OI
		ON
			OKI.numOppItemID=OI.numoppitemtCode
		INNER JOIN
			OpportunityMaster OM
		ON
			OI.numOppId=OM.numOppId
		INNER JOIN
			ProjectProgress PP
		ON
			OM.numOppId = PP.numOppId
		WHERE
			OM.numDomainId=@numDomainID
			AND OM.tintOppType=1
			AND OM.tintOppStatus=0
			AND OKI.numChildItemID=@numItemCode
			AND OKI.numWareHouseItemId=@numWarehouseItemID
			AND ISNULL(OI.bitWorkOrder,0) = 0
			AND (OM.dtReleaseDate IS NOT NULL OR OI.ItemReleaseDate IS NOT NULL)
			AND ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate) <= DATEADD(DAY,@numForecastDays,DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()))
			AND PP.intTotalProgress >= @numOpportunityPercentComplete

		-- USED IN KIT WITHIN KIT
		INSERT INTO @TEMP
		(
			numOppID
			,numWOID
			,dtCreatedDate
			,vcOppName
			,numOrderedQty
			,numQtyToShip
			,numOpportunityForecastQty
			,dtReleaseDate
		)
		SELECT
			OM.numOppId
			,0
			,OM.bintCreatedDate
			,CONCAT(OM.vcPOppName,' (Kit Memeber)')
			,OKCI.numQtyItemsReq
			,OKCI.numQtyItemsReq
			,OKCI.numQtyItemsReq * (PP.intTotalProgress/100)
			,dbo.FormatedDateFromDate(ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate),@numDomainID)
		FROM
			OpportunityKitChildItems OKCI
		INNER JOIN
			OpportunityItems OI
		ON
			OKCI.numOppItemID=OI.numoppitemtCode
		INNER JOIN
			OpportunityMaster OM
		ON
			OI.numOppId=OM.numOppId
		INNER JOIN
			ProjectProgress PP
		ON
			OM.numOppId = PP.numOppId
		WHERE
			OM.numDomainId=@numDomainID
			AND OM.tintOppType=1
			AND OM.tintOppStatus=0
			AND OKCI.numItemID=@numItemCode
			AND OKCI.numWareHouseItemId=@numWarehouseItemID
			AND ISNULL(OI.bitWorkOrder,0) = 0
			AND (OM.dtReleaseDate IS NOT NULL OR OI.ItemReleaseDate IS NOT NULL)
			AND ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate) <= DATEADD(DAY,@numForecastDays,DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()))
			AND PP.intTotalProgress >= @numOpportunityPercentComplete

		-- USED IN WORK ORDER
		INSERT INTO @TEMP
		(
			numOppID
			,numWOID
			,dtCreatedDate
			,vcOppName
			,numOrderedQty
			,numQtyToShip
			,numOpportunityForecastQty
			,dtReleaseDate
		)
		SELECT
			OM.numOppId
			,WO.numWOId
			,CASE WHEN OM.numOppId IS NOT NULL THEN OM.bintCreatedDate ELSE WO.bintCreatedDate END
			,CONCAT(CASE WHEN OM.numOppId IS NOT NULL THEN OM.vcPOppName ELSE 'Work Order' END,' (Assembly Memeber)') 
			,WOD.numQtyItemsReq
			,WOD.numQtyItemsReq
			,WOD.numQtyItemsReq * (PP.intTotalProgress/100)
			,CASE WHEN OM.numOppId IS NOT NULL THEN dbo.FormatedDateFromDate(ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate),@numDomainID) ELSE dbo.FormatedDateFromDate(WO.bintCompliationDate,@numDomainID) END
		FROM
			OpportunityMaster OM
		INNER JOIN
			OpportunityItems OI
		ON
			OI.numOppId=OM.numOppId
		INNER JOIN
			WorkOrder WO
		ON
			OI.numoppitemtCode = WO.numOppItemID
		INNER JOIN
			WorkOrderDetails WOD
		ON
			WO.numWOId=WOD.numWOId
		INNER JOIN
			ProjectProgress PP
		ON
			OM.numOppId = PP.numOppId
		WHERE
			OM.tintOppType=1
			AND OM.tintOppStatus=0
			AND WO.numDomainID=@numDomainID
			AND WO.numWOStatus <> 23184 -- NOT COMPLETED
			AND WOD.numChildItemID = @numItemCode
			AND WOD.numWareHouseItemId=@numWarehouseItemID
			AND (OM.dtReleaseDate IS NOT NULL OR OI.ItemReleaseDate IS NOT NULL OR WO.bintCompliationDate IS NOT NULL)
			AND ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate) <= DATEADD(DAY,@numForecastDays,DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()))
			AND PP.intTotalProgress >= @numOpportunityPercentComplete
	END

	SELECT * FROM @TEMP ORDER BY dtCreatedDate
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DemandForecast_ReleaseDateDetails')
DROP PROCEDURE dbo.USP_DemandForecast_ReleaseDateDetails
GO
CREATE PROCEDURE [dbo].[USP_DemandForecast_ReleaseDateDetails]
	@numDomainID NUMERIC(18,0)
	,@numItemCode NUMERIC(18,0)
	,@numWarehouseItemID NUMERIC(18,0)
	,@dtReleaseDate DATE
	,@CurrentPage INT
    ,@PageSize INT
	,@ClientTimeZoneOffset INT
AS 
BEGIN
	DECLARE @TEMP TABLE
	(
		numOppID NUMERIC(18,0)
		,numDivisionID NUMERIC(18,0)
		,vcCompanyName VARCHAR(300)
		,numWOID NUMERIC(18,0)
		,dtCreatedDate DATETIME
		,vcOppName VARCHAR(500)
		,numOrderedQty FLOAT
		,numQtyToShip FLOAT
		,dtReleaseDate VARCHAR(50)
	)

	INSERT INTO @TEMP
	(
		numOppID
		,numDivisionID
		,vcCompanyName
		,numWOID
		,dtCreatedDate
		,vcOppName
		,numOrderedQty
		,numQtyToShip
		,dtReleaseDate
	)
	SELECT
		OpportunityMaster.numOppId
		,OpportunityMaster.numDivisionId
		,ISNULL(CompanyInfo.vcCompanyName,'')
		,0
		,OpportunityMaster.bintCreatedDate
		,CONCAT('<a href="#" onclick="return openInNewTab(',OpportunityMaster.numOppId,');">',OpportunityMaster.vcPOppName,'</a>')
		,OpportunityItems.numUnitHour
		,OpportunityItems.numUnitHour - ISNULL(numQtyShipped,0)
		,dbo.FormatedDateFromDate(ISNULL(OpportunityItems.ItemReleaseDate,OpportunityMaster.dtReleaseDate),@numDomainID)
	FROM
		OpportunityMaster
	INNER JOIN 
		DivisionMaster
	ON
		OpportunityMaster.numDivisionId=DivisionMaster.numDivisionID
	INNER JOIN 
		CompanyInfo
	ON
		DivisionMaster.numCompanyID=CompanyInfo.numCompanyId
	INNER JOIN
		OpportunityItems
	ON
		OpportunityMaster.numOppId = OpportunityItems.numOppId
	INNER JOIN
		Item
	ON
		OpportunityItems.numItemCode = Item.numItemCode
	WHERE
		OpportunityMaster.numDomainId=@numDomainID
		AND OpportunityMaster.tintOppType=1
		AND OpportunityMaster.tintOppStatus=1
		AND ISNULL(OpportunityMaster.tintshipped,0)=0
		AND (ISNULL(OpportunityItems.numUnitHour,0)  - ISNULL(OpportunityItems.numQtyShipped,0)) > 0
		AND ISNULL(OpportunityItems.bitDropShip,0) = 0
		AND ISNULL(Item.IsArchieve,0) = 0
		AND ISNULL(Item.bitAssembly,0) = 0
		AND ISNULL(Item.bitKitParent,0) = 0
		AND (OpportunityItems.ItemReleaseDate IS NOT NULL OR OpportunityMaster.dtReleaseDate IS NOT NULL)
		AND OpportunityItems.numItemCode=@numItemCode
		AND OpportunityItems.numWarehouseItmsID=@numWarehouseItemID
		AND ISNULL(OpportunityItems.ItemReleaseDate,OpportunityMaster.dtReleaseDate) = @dtReleaseDate

	-- USED IN KIT
	INSERT INTO @TEMP
	(
		numOppID
		,numDivisionID
		,vcCompanyName
		,numWOID
		,dtCreatedDate
		,vcOppName
		,numOrderedQty
		,numQtyToShip
		,dtReleaseDate
	)
	SELECT
		OM.numOppId
		,OM.numDivisionId
		,ISNULL(CompanyInfo.vcCompanyName,'')
		,0
		,OM.bintCreatedDate
		,CONCAT('<a href="#" onclick="return openInNewTab(',OM.numOppId,');">',OM.vcPOppName,' (Kit Memeber)','</a>')
		,OKI.numQtyItemsReq
		,OKI.numQtyItemsReq - ISNULL(OKI.numQtyShipped,0)
		,dbo.FormatedDateFromDate(ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate),@numDomainID)
	FROM
		OpportunityMaster OM
	INNER JOIN 
		DivisionMaster
	ON
		OM.numDivisionId=DivisionMaster.numDivisionID
	INNER JOIN 
		CompanyInfo
	ON
		DivisionMaster.numCompanyID=CompanyInfo.numCompanyId
	INNER JOIN
		OpportunityItems OI
	ON
		OM.numOppId = OI.numOppId
	INNER JOIN
		OpportunityKitItems OKI
	ON
		OI.numoppitemtCode=OKI.numOppItemID
	INNER JOIN
		Item
	ON
		OKI.numChildItemID = Item.numItemCode
	INNER JOIN
		WareHouseItems WI
	ON
		OKI.numWareHouseItemId = WI.numWareHouseItemID
	WHERE
		OM.numDomainId=@numDomainID
		AND OM.tintOppType=1
		AND OM.tintOppStatus=1
		AND ISNULL(OM.tintshipped,0)=0
		AND ISNULL(OI.bitDropShip,0) = 0
		AND ISNULL(OI.bitWorkOrder,0) = 0
		AND ISNULL(Item.IsArchieve,0) = 0
		AND ISNULL(Item.bitAssembly,0) = 0
		AND ISNULL(Item.bitKitParent,0) = 0
		AND (ISNULL(OKI.numQtyItemsReq,0) - ISNULL(OKI.numQtyShipped,0)) > 0
		AND (OM.dtReleaseDate IS NOT NULL OR OI.ItemReleaseDate IS NOT NULL)
		AND OKI.numChildItemID=@numItemCode
		AND OKI.numWareHouseItemId=@numWarehouseItemID
		AND ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate) = @dtReleaseDate

	-- USED IN KIT WITHIN KIT
	INSERT INTO @TEMP
	(
		numOppID
		,numDivisionID
		,vcCompanyName
		,numWOID
		,dtCreatedDate
		,vcOppName
		,numOrderedQty
		,numQtyToShip
		,dtReleaseDate
	)
	SELECT
		OM.numOppId
		,OM.numDivisionId
		,ISNULL(CompanyInfo.vcCompanyName,'')
		,0
		,OM.bintCreatedDate
		,CONCAT('<a href="#" onclick="return openInNewTab(',OM.numOppId,');">',OM.vcPOppName,' (Kit Memeber)','</a>')
		,OKCI.numQtyItemsReq
		,OKCI.numQtyItemsReq - ISNULL(OKCI.numQtyShipped,0)
		,dbo.FormatedDateFromDate(ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate),@numDomainID)
	FROM
		OpportunityMaster OM
	INNER JOIN 
		DivisionMaster
	ON
		OM.numDivisionId=DivisionMaster.numDivisionID
	INNER JOIN 
		CompanyInfo
	ON
		DivisionMaster.numCompanyID=CompanyInfo.numCompanyId
	INNER JOIN
		OpportunityItems OI
	ON
		OM.numOppId = OI.numOppId
	INNER JOIN
		OpportunityKitChildItems OKCI
	ON
		OI.numoppitemtCode = OKCI.numOppItemID
	INNER JOIN
		Item
	ON
		OKCI.numItemID = Item.numItemCode
	INNER JOIN
		WareHouseItems WI
	ON
		OKCI.numWareHouseItemId = WI.numWareHouseItemID
	WHERE
		OM.numDomainId=@numDomainID
		AND OM.tintOppType=1
		AND OM.tintOppStatus=1
		AND ISNULL(OM.tintshipped,0)=0
		AND ISNULL(OI.bitDropShip,0) = 0
		AND ISNULL(OI.bitWorkOrder,0) = 0
		AND ISNULL(Item.IsArchieve,0) = 0
		AND ISNULL(Item.bitAssembly,0) = 0
		AND ISNULL(Item.bitKitParent,0) = 0
		AND (ISNULL(OKCI.numQtyItemsReq,0) - ISNULL(OKCI.numQtyShipped,0)) > 0
		AND (OM.dtReleaseDate IS NOT NULL OR OI.ItemReleaseDate IS NOT NULL)
		AND OKCI.numItemID=@numItemCode
		AND OKCI.numWareHouseItemId=@numWarehouseItemID
		AND ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate) = @dtReleaseDate
		
	-- USED IN WORK ORDER
	INSERT INTO @TEMP
	(
		numOppID
		,numDivisionID
		,vcCompanyName
		,numWOID
		,dtCreatedDate
		,vcOppName
		,numOrderedQty
		,numQtyToShip
		,dtReleaseDate
	)
	SELECT
		OM.numOppId
		,ISNULL(OM.numDivisionId,0)
		,ISNULL(CompanyInfo.vcCompanyName,'')
		,WO.numWOId
		,CASE WHEN OM.numOppId IS NOT NULL THEN OM.bintCreatedDate ELSE WO.bintCreatedDate END
		,CONCAT(CASE WHEN OM.numOppId IS NOT NULL THEN OM.vcPOppName ELSE 'Work Order' END,' (Assembly Memeber)') 
		,WOD.numQtyItemsReq
		,WOD.numQtyItemsReq 
		,CASE WHEN OM.numOppId IS NOT NULL THEN dbo.FormatedDateFromDate(ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate),@numDomainID) ELSE dbo.FormatedDateFromDate(WO.bintCompliationDate,@numDomainID) END
	FROM
		WorkOrderDetails WOD
	INNER JOIN
		Item
	ON
		WOD.numChildItemID = Item.numItemCode
	INNER JOIN
		WareHouseItems WI
	ON
		WOD.numWareHouseItemId = WI.numWareHouseItemID
	INNER JOIN
		WorkOrder WO
	ON
		WOD.numWOId=WO.numWOId
	LEFT JOIN
		OpportunityItems OI
	ON
		WO.numOppItemID = OI.numoppitemtCode
	LEFT JOIN
		OpportunityMaster OM
	ON
		OI.numOppId=OM.numOppId
	LEFT JOIN 
		DivisionMaster
	ON
		OM.numDivisionId=DivisionMaster.numDivisionID
	LEFT JOIN 
		CompanyInfo
	ON
		DivisionMaster.numCompanyID=CompanyInfo.numCompanyId
	WHERE
		WO.numDomainID=@numDomainID
		AND WO.numWOStatus <> 23184 -- NOT COMPLETED
		AND ISNULL(Item.IsArchieve,0) = 0
		AND ISNULL(Item.bitAssembly,0) = 0
		AND ISNULL(Item.bitKitParent,0) = 0
		AND ISNULL(WOD.numQtyItemsReq,0) > 0
		AND (OM.dtReleaseDate IS NOT NULL OR OI.ItemReleaseDate IS NOT NULL OR WO.bintCompliationDate IS NOT NULL)
		AND 1 = (CASE WHEN OM.numOppId IS NOT NULL THEN (CASE WHEN OM.tintOppStatus=1 THEN 1 ELSE 0 END) ELSE 1 END)
		AND WOD.numChildItemID = @numItemCode
		AND WOD.numWareHouseItemId=@numWarehouseItemID
		AND 1 = (CASE 
				WHEN OI.numoppitemtCode IS NOT NULL 
				THEN (CASE 
						WHEN ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate) = @dtReleaseDate
						THEN 1 
						ELSE 0 
					END)
				ELSE 
					(CASE 
					WHEN WO.bintCompliationDate = @dtReleaseDate
					THEN 1 
					ELSE 0 
					END)
				END)

	SELECT * FROM @TEMP ORDER BY dtCreatedDate
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetItemPromotionDiscountItems')
DROP PROCEDURE USP_GetItemPromotionDiscountItems
GO
CREATE PROCEDURE [dbo].[USP_GetItemPromotionDiscountItems]
	@numDomainID NUMERIC(18,0), 
	@numDivisionID NUMERIC(18,0),
	@numPromotionID NUMERIC(18,0),
	@numItemCode NUMERIC(18,0),
	@numWarehouseItemID NUMERIC(18,0),
	@numPageIndex INT,
	@numPageSize INT
AS
BEGIN
	DECLARE @tintDiscountBasedOn TINYINT
	DECLARE @tintDiscountType TINYINT
	DECLARE @fltDiscountValue FLOAT
	DECLARE @vcCurrency VARCHAR(10)
	DECLARE @numWarehouseID NUMERIC(18,0)
	DECLARE @tintItemCalDiscount TINYINT
	DECLARE @monDiscountedItemPrice DECIMAL(20,5)
	
	SELECT @vcCurrency=ISNULL(vcCurrency,'$') FROm Domain WHERE numDomainId=@numDomainID

	SELECT @numWarehouseID=ISNULL(numWareHouseID,0) FROM WarehouseItems WHERE numWarehouseItemID = @numWarehouseItemID
	
	SELECT 
		@tintDiscountBasedOn=tintDiscoutBaseOn
		,@tintDiscountType=tintDiscountType 
		,@fltDiscountValue=ISNULL(fltDiscountValue,0)
		,@tintItemCalDiscount=ISNULL(tintItemCalDiscount,0)
		,@monDiscountedItemPrice=ISNULL(monDiscountedItemPrice,0)
	FROM 
		PromotionOffer 
	WHERE 
		numProId = @numPromotionID 

	DECLARE @TempItem TABLE
	(
		numItemCode NUMERIC(18,0)
	)

	PRINT @tintDiscountBasedOn

	IF @tintDiscountBasedOn = 1 -- Based on individual item(s)
	BEGIN
		INSERT INTO @TempItem
		(
			numItemCode
		)
		SELECT DISTINCT
			numValue
		FROM
			PromotionOfferItems
		WHERE
			numProId=@numPromotionID
			AND tintRecordType = 6
			AND tintType = 1
	END
	ELSE IF @tintDiscountBasedOn = 2 -- Based on individual item classification(s)
	BEGIN
		INSERT INTO @TempItem
		(
			numItemCode
		)
		SELECT DISTINCT
			numItemCode
		FROM
			Item
		WHERE
			numDomainID=@numDomainID
			AND numItemClassification IN (SELECT
												numValue
											FROM
												PromotionOfferItems
											WHERE
												numProId=@numPromotionID
												AND tintRecordType = 6
												AND tintType = 2)
	END
	ELSE IF @tintDiscountBasedOn = 3  -- Based on related item(s)
	BEGIN
		INSERT INTO @TempItem
		(
			numItemCode
		)
		SELECT DISTINCT
			numItemCode
		FROM
			SimilarItems
		WHERE
			numParentItemCode =@numItemCode
	END
	ELSE IF @tintDiscountBasedOn = 6 
	BEGIN
		INSERT INTO @TempItem
		(
			numItemCode
		)
		SELECT DISTINCT
			numValue
		FROM
			PromotionOfferItems
		WHERE
			numProId=@numPromotionID
			AND tintRecordType = 6
			AND tintType = 4
	END

	SELECT
		COUNT(Item.numItemCode) OVER() AS TotalRecords
		,Item.numItemCode
		,ISNULL(Item.vcItemName,'') vcItemName
		,ISNULL(Item.txtItemDesc,'') txtItemDesc
		,(CASE 
			WHEN @tintDiscountBasedOn = 6
			THEN @monDiscountedItemPrice
			ELSE
				(CASE 
					WHEN (ISNULL(bitKitParent,0)=1 OR ISNULL(bitAssembly,0) = 1) AND ISNULL(bitCalAmtBasedonDepItems,0) = 1
					THEN [dbo].[GetCalculatedPriceForKitAssembly](@numDomainID,@numDivisionID,Item.numItemCode,1,@numWarehouseItemID,ISNULL(tintKitAssemblyPriceBasedOn,1),0,0,'')
					ELSE ISNULL(monListPrice,0)
				END)
		END) monListPrice
		,(CASE @tintDiscountType
			WHEN 1 THEN 0
			WHEN 2 THEN 1
			WHEN 3 THEN 0
		END) tintDiscountType
		,(CASE @tintDiscountType
			WHEN 1 THEN CONCAT(@fltDiscountValue,'%')
			WHEN 2 THEN CONCAT(@vcCurrency,@fltDiscountValue)
			WHEN 3 THEN '100%'
		END) vcDiscountType
		,(CASE @tintDiscountType
			WHEN 1 THEN @fltDiscountValue
			WHEN 2 THEN @fltDiscountValue
			WHEN 3 THEN 100
		END) fltDiscount
		,(CASE @tintDiscountType
			WHEN 1 THEN (CASE WHEN @fltDiscountValue > 0 THEN (CASE 
																	WHEN (ISNULL(bitKitParent,0)=1 OR ISNULL(bitAssembly,0) = 1) AND ISNULL(bitCalAmtBasedonDepItems,0) = 1
																	THEN [dbo].[GetCalculatedPriceForKitAssembly](@numDomainID,@numDivisionID,Item.numItemCode,1,@numWarehouseItemID,ISNULL(tintKitAssemblyPriceBasedOn,1),0,0,'')
																	ELSE ISNULL(monListPrice,0)
																END) - ((CASE 
																			WHEN (ISNULL(bitKitParent,0)=1 OR ISNULL(bitAssembly,0) = 1) AND ISNULL(bitCalAmtBasedonDepItems,0) = 1
																			THEN [dbo].[GetCalculatedPriceForKitAssembly](@numDomainID,@numDivisionID,Item.numItemCode,1,@numWarehouseItemID,ISNULL(tintKitAssemblyPriceBasedOn,1),0,0,'')
																			ELSE ISNULL(monListPrice,0)
																		END) * (@fltDiscountValue/100))  ELSE (CASE 
																													WHEN (ISNULL(bitKitParent,0)=1 OR ISNULL(bitAssembly,0) = 1) AND ISNULL(bitCalAmtBasedonDepItems,0) = 1
																													THEN [dbo].[GetCalculatedPriceForKitAssembly](@numDomainID,@numDivisionID,Item.numItemCode,1,@numWarehouseItemID,ISNULL(tintKitAssemblyPriceBasedOn,1),0,0,'')
																													ELSE ISNULL(monListPrice,0)
																												END) END)
			WHEN 2 THEN (CASE WHEN @fltDiscountValue > 0 THEN (CASE WHEN (CASE 
																				WHEN (ISNULL(bitKitParent,0)=1 OR ISNULL(bitAssembly,0) = 1) AND ISNULL(bitCalAmtBasedonDepItems,0) = 1
																				THEN [dbo].[GetCalculatedPriceForKitAssembly](@numDomainID,@numDivisionID,Item.numItemCode,1,@numWarehouseItemID,ISNULL(tintKitAssemblyPriceBasedOn,1),0,0,'')
																				ELSE ISNULL(monListPrice,0)
																			END) > @fltDiscountValue THEN ((CASE 
																												WHEN (ISNULL(bitKitParent,0)=1 OR ISNULL(bitAssembly,0) = 1) AND ISNULL(bitCalAmtBasedonDepItems,0) = 1
																												THEN [dbo].[GetCalculatedPriceForKitAssembly](@numDomainID,@numDivisionID,Item.numItemCode,1,@numWarehouseItemID,ISNULL(tintKitAssemblyPriceBasedOn,1),0,0,'')
																												ELSE ISNULL(monListPrice,0)
																											END) - @fltDiscountValue) ELSE 0 END)  ELSE (CASE 
																																							WHEN (ISNULL(bitKitParent,0)=1 OR ISNULL(bitAssembly,0) = 1) AND ISNULL(bitCalAmtBasedonDepItems,0) = 1
																																							THEN [dbo].[GetCalculatedPriceForKitAssembly](@numDomainID,@numDivisionID,Item.numItemCode,1,@numWarehouseItemID,ISNULL(tintKitAssemblyPriceBasedOn,1),0,0,'')
																																							ELSE ISNULL(monListPrice,0)
																																						END) END)
			WHEN 3 THEN 0
		END) monSalePrice
		,(CASE @tintDiscountType
			WHEN 1 THEN (CASE WHEN @fltDiscountValue > 0 THEN (CASE 
																	WHEN (ISNULL(bitKitParent,0)=1 OR ISNULL(bitAssembly,0) = 1) AND ISNULL(bitCalAmtBasedonDepItems,0) = 1
																	THEN [dbo].[GetCalculatedPriceForKitAssembly](@numDomainID,@numDivisionID,Item.numItemCode,1,@numWarehouseItemID,ISNULL(tintKitAssemblyPriceBasedOn,1),0,0,'')
																	ELSE ISNULL(monListPrice,0)
																END) - ((CASE 
																			WHEN (ISNULL(bitKitParent,0)=1 OR ISNULL(bitAssembly,0) = 1) AND ISNULL(bitCalAmtBasedonDepItems,0) = 1
																			THEN [dbo].[GetCalculatedPriceForKitAssembly](@numDomainID,@numDivisionID,Item.numItemCode,1,@numWarehouseItemID,ISNULL(tintKitAssemblyPriceBasedOn,1),0,0,'')
																			ELSE ISNULL(monListPrice,0)
																		END) * (@fltDiscountValue/100))  ELSE (CASE 
																													WHEN (ISNULL(bitKitParent,0)=1 OR ISNULL(bitAssembly,0) = 1) AND ISNULL(bitCalAmtBasedonDepItems,0) = 1
																													THEN [dbo].[GetCalculatedPriceForKitAssembly](@numDomainID,@numDivisionID,Item.numItemCode,1,@numWarehouseItemID,ISNULL(tintKitAssemblyPriceBasedOn,1),0,0,'')
																													ELSE ISNULL(monListPrice,0)
																												END) END)
			WHEN 2 THEN (CASE WHEN @fltDiscountValue > 0 THEN (CASE WHEN (CASE 
																				WHEN (ISNULL(bitKitParent,0)=1 OR ISNULL(bitAssembly,0) = 1) AND ISNULL(bitCalAmtBasedonDepItems,0) = 1
																				THEN [dbo].[GetCalculatedPriceForKitAssembly](@numDomainID,@numDivisionID,Item.numItemCode,1,@numWarehouseItemID,ISNULL(tintKitAssemblyPriceBasedOn,1),0,0,'')
																				ELSE ISNULL(monListPrice,0)
																			END) > @fltDiscountValue THEN ((CASE 
																												WHEN (ISNULL(bitKitParent,0)=1 OR ISNULL(bitAssembly,0) = 1) AND ISNULL(bitCalAmtBasedonDepItems,0) = 1
																												THEN [dbo].[GetCalculatedPriceForKitAssembly](@numDomainID,@numDivisionID,Item.numItemCode,1,@numWarehouseItemID,ISNULL(tintKitAssemblyPriceBasedOn,1),0,0,'')
																												ELSE ISNULL(monListPrice,0)
																											END) - @fltDiscountValue) ELSE 0 END)  ELSE (CASE 
																																							WHEN (ISNULL(bitKitParent,0)=1 OR ISNULL(bitAssembly,0) = 1) AND ISNULL(bitCalAmtBasedonDepItems,0) = 1
																																							THEN [dbo].[GetCalculatedPriceForKitAssembly](@numDomainID,@numDivisionID,Item.numItemCode,1,@numWarehouseItemID,ISNULL(tintKitAssemblyPriceBasedOn,1),0,0,'')
																																							ELSE ISNULL(monListPrice,0)
																																						END) END)
			WHEN 3 THEN 0
		END) monTotalAmount
		,numWareHouseItemID
		,ISNULL(Item.bitMatrix,0) bitMatrix
		,ISNULL(numItemGroup,0) numItemGroup
		,(CASE WHEN ISNULL(bitKitParent,0) = 1 THEN (CASE WHEN ISNULL((SELECT COUNT(*) FROM ItemDetails INNER JOIN Item IInner ON ItemDetails.numChildItemID=IInner.numItemCode WHERE numItemKitID=Item.numItemCode AND ISNULL(IInner.bitKitParent,0) = 1),0) > 0 THEN 1 ELSE 0 END) ELSE 0 END) bitHasChildKits
		,(CASE WHEN ISNULL(bitKitParent,0) = 1 OR ISNULL(bitAssembly,0) = 1 THEN ISNULL(bitCalAmtBasedonDepItems,0) ELSE 0 END) bitCalAmtBasedonDepItems
	FROM
		Item
	INNER JOIN
		@TempItem T1
	ON
		Item.numItemCode = T1.numItemCode
	OUTER APPLY
	(
		SELECT TOP 1
			numWareHouseItemID
		FROM
			WareHouseItems
		WHERE
			numItemID=Item.numItemCode
			AND (numWareHouseID=@numWarehouseID OR @numWarehouseItemID=0)
	) T2
	WHERE
		numDomainID=@numDomainID
	ORDER BY
		vcItemName
	OFFSET
		((@numPageIndex - 1) * @numPageSize) ROWS
	FETCH NEXT
		@numPageSize ROWS ONLY
END
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_Item_GetAll_BIZAPI' ) 
    DROP PROCEDURE USP_Item_GetAll_BIZAPI
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Sandeep Patel
-- Create date: 9 April 2014
-- Description:	Gets list of items domain wise
-- =============================================
CREATE PROCEDURE USP_Item_GetAll_BIZAPI
	@numDomainID NUMERIC(18,0) = NULL,
	@numPageIndex AS INT = 0,
    @numPageSize AS INT = 0,
    @TotalRecords INT OUTPUT,
	@dtCreatedAfter DateTime = NULL,
	@dtModifiedAfter DateTime = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @TEMP TABLE
	(
		numItemCode NUMERIC(18,0)
	)

	INSERT INTO @TEMP
	(
		numItemCode
	)
	SELECT DISTINCT
		numItemCode
	FROM 
		Item 
	LEFT JOIN
		WareHouseItems
	ON
		Item.numItemCode=numItemID
	WHERE 
		Item.numDomainID = @numDomainID
		AND (@dtCreatedAfter IS NULL OR Item.bintCreatedDate > @dtCreatedAfter)
		AND (@dtModifiedAfter IS NULL OR Item.bintModifiedDate > @dtModifiedAfter OR DATEADD(hh, DATEDIFF(hh, GETDATE(), GETUTCDATE()), dtModified) > @dtModifiedAfter)

	SET @TotalRecords = ISNULL((SELECT COUNT(*) FROM @TEMP),0)

	SELECT
		ROW_NUMBER() OVER (ORDER BY Item.numItemCode asc) AS RowNumber,
		Item.numItemCode,
		Item.vcItemName,
		(SELECT TOP 1 vcPathForImage FROM ItemImages WHERE numItemCode = Item.numItemCode AND ISNULL(bitDefault,0) = 1) AS vcPathForImage,
		(SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = Item.numItemCode AND ISNULL(bitDefault,0) = 1) AS vcPathForTImage,
		Item.txtItemDesc,
		Item.charItemType,
		CASE 
			WHEN Item.charItemType='P' THEN 'Inventory Item' 
			WHEN Item.charItemType='N' THEN 'Non Inventory Item' 
			WHEN Item.charItemType='S' THEN 'Service' 
			WHEN Item.charItemType='A' THEN 'Accessory' 
		END AS ItemType,
		Item.vcSKU,
		Item.vcModelID,
		Item.numBarCodeId,
		Item.vcManufacturer,
		Item.vcUnitofMeasure,
		Item.monListPrice,
		(CASE WHEN ISNULL(Item.bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(Item.monAverageCost,0.00) END) AS monAverageCost,
		Item.monCampaignLabourCost,
		Item.numItemGroup,
		Item.numItemClass,
		Item.numShipClass,
		Item.numItemClassification,
		Item.fltWidth,
		Item.fltWeight,
		Item.fltHeight,
		Item.fltLength,
		Item.numBaseUnit,
		Item.numSaleUnit,
		Item.numPurchaseUnit,
		Item.bitKitParent,
		Item.bitLotNo,
		Item.bitAssembly,
		Item.bitSerialized,
		Item.bitTaxable,
		Item.bitAllowBackOrder,
		Item.bitFreeShipping,
		Item.numCOGsChartAcntId,
		Item.numAssetChartAcntId,
		Item.numIncomeChartAcntId,
		Item.numVendorID,
		WI.numOnHand,                      
		WI.numOnOrder,                      
		WI.numReorder,                      
		WI.numAllocation,                      
		WI.numBackOrder,
		bintCreatedDate,
		CASE WHEN DATEADD(hh, DATEDIFF(hh, GETDATE(), GETUTCDATE()), WI.dtModified) > bintModifiedDate THEN DATEADD(hh, DATEDIFF(hh, GETDATE(), GETUTCDATE()), WI.dtModified) ELSE bintModifiedDate END AS bintModifiedDate,
		STUFF((SELECT CONCAT(',',numCategoryID) FROM ItemCategory WHERE numItemID=Item.numItemCode GROUP BY numCategoryID FOR XML PATH('')),1,1,'') AS vcCategories
	INTO
		#Temp
	FROM
		Item WITH (NOLOCK)
	INNER JOIN
		@TEMP T1
	ON
		Item.numItemCode = T1.numItemCode
	OUTER APPLY
	(
		SELECT
			SUM(numOnHand) AS numOnHand,                      
			SUM(numOnOrder) AS numOnOrder,                      
			SUM(numReorder) AS numReorder,                      
			SUM(numAllocation) AS numAllocation,                      
			SUM(numBackOrder) AS numBackOrder,
			MAX(dtModified) dtModified
		FROM
			WarehouseItems
		WHERE
			WarehouseItems.numItemID=Item.numItemCode
	) WI
	ORDER BY
		Item.numItemCode
	OFFSET (CASE WHEN ISNULL(@numPageIndex,0) > 0 THEN (@numPageIndex - 1) ELSE 0 END) * @numPageSize ROWS FETCH NEXT (CASE WHEN ISNULL(@numPageSize,0) > 0 THEN @numPageSize ELSE 999999999 END) ROWS ONLY

	SELECT * FROM #Temp

	--GET CHILD ITEMS
	SELECT
		ItemDetails.numItemKitID,
		ItemDetails.numChildItemID,
		ItemDetails.numQtyItemsReq,
		(SELECT vcUnitName FROM UOM WHERE numUOMId = ItemDetails.numUOMId) AS vcUnitName
	FROM
		#Temp T1
	INNER JOIN
		ItemDetails WITH (NOLOCK)
	ON
		T1.numItemCode = ItemDetails.numItemKitID

	--GET CUSTOM FIELDS
	SELECT 
		I.numItemCode,
		TEMPCustFields.Fld_id AS Id,
		TEMPCustFields.Fld_label AS Name,
		TEMPCustFields.Fld_type AS [Type],
		TEMPCustFields.Fld_Value AS ValueId,
		CASE 
			WHEN TEMPCustFields.Fld_type='TextBox' or TEMPCustFields.Fld_type='TextArea' THEN
				(CASE WHEN TEMPCustFields.Fld_Value='0' OR TEMPCustFields.Fld_Value='' THEN '-' ELSE TEMPCustFields.Fld_Value END)
			WHEN TEMPCustFields.Fld_type='SelectBox' THEN
				(CASE WHEN TEMPCustFields.Fld_Value='' THEN '-' ELSE dbo.GetListIemName(TEMPCustFields.Fld_Value) END)
			WHEN TEMPCustFields.Fld_type='CheckBox' THEN
				(CASE WHEN TEMPCustFields.Fld_Value=0 THEN 'No' ELSE 'Yes' END)
			ELSE
				''
		END AS Value
	FROM 
		#Temp I
	OUTER APPLY
	(
		SELECT
			CFW_Fld_Master.Fld_id,
			CFW_Fld_Master.Fld_label,
			CFW_Fld_Master.Fld_type,
			CFW_FLD_Values_Item.Fld_Value
		FROM
			CFW_Fld_Master WITH (NOLOCK)
		LEFT JOIN
			CFW_FLD_Values_Item WITH (NOLOCK)
		ON
			CFW_FLD_Values_Item.Fld_ID = CFW_Fld_Master.Fld_id
			AND I.numItemCode = CFW_FLD_Values_Item.RecId
		WHERE 
			numDomainID = @numDomainID
			AND Grp_id = 5
	) TEMPCustFields

	DROp TABLE #Temp
END
GO


GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_Item_GetCalculatedPrice')
DROP PROCEDURE USP_Item_GetCalculatedPrice
GO
CREATE PROCEDURE [dbo].[USP_Item_GetCalculatedPrice]
	@numDomainID NUMERIC(18,0)
	,@numSiteID NUMERIC(18,0)
	,@numDivisionID NUMERIC(18,0)
	,@numItemCode NUMERIC(18,0)
	,@numQty FLOAT
	,@vcChildKitSelectedItem VARCHAR(MAX)
AS
BEGIN
	DECLARE @numWarehouseID NUMERIC(18,0)
	DECLARE @numWarehouseItemID NUMERIC(18,0)

	SELECT @numWarehouseID=ISNULL(numDefaultWareHouseID,0) FROM [eCommerceDTL] WHERE  numSiteId=@numSiteID
	

	IF ISNULL(@numWarehouseID,0) > 0 AND EXISTS (SELECT numWareHouseItemID FROM WareHouseItems WHERE numItemID=@numItemCode AND numWareHouseID=@numWarehouseID)
	BEGIN
		SELECT TOP 1 @numWarehouseItemID = numWareHouseItemID FROM WareHouseItems WHERE numItemID=@numItemCode AND numWareHouseID=@numWarehouseID ORDER BY numWareHouseItemID ASC
	END
	ELSE 
	BEGIN
		SELECT TOP 1 @numWarehouseItemID = numWareHouseItemID FROM WareHouseItems WHERE numItemID=@numItemCode ORDER BY numWareHouseItemID ASC
	END


	DECLARE @tintKitAssemblyPriceBasedOn TINYINT
	SELECT @tintKitAssemblyPriceBasedOn=ISNULL(tintKitAssemblyPriceBasedOn,1) FROM Item WHERE numItemCode=@numItemCode

	SELECT
		ISNULL(monPrice,0) monPrice
		,ISNULL(monMSRPPrice,0) monMSRP
	FROM
		dbo.fn_GetKitAssemblyCalculatedPrice(@numDomainID,@numDivisionID,@numItemCode,@numQty,@numWarehouseItemID,@tintKitAssemblyPriceBasedOn,0,0,@vcChildKitSelectedItem)
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_Item_GetKitAssemblyCalculatedPrice')
DROP PROCEDURE USP_Item_GetKitAssemblyCalculatedPrice
GO
CREATE PROCEDURE [dbo].[USP_Item_GetKitAssemblyCalculatedPrice]
	@numDomainID NUMERIC(18,0),
	@numDivisionID NUMERIC(18,0),
	@numItemCode NUMERIC(18,0),
	@numQty FLOAT,
	@tintKitAssemblyPriceBasedOn TINYINT
AS
BEGIN
	SELECT
		bitSuccess
		,monPrice
	FROM
		dbo.fn_GetKitAssemblyCalculatedPrice(@numDomainID,@numDivisionID,@numItemCode,@numQty,0,@tintKitAssemblyPriceBasedOn,0,0,'')
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_Item_GetPriceBasedOnPricingMethod')
DROP PROCEDURE USP_Item_GetPriceBasedOnPricingMethod
GO
CREATE PROCEDURE [dbo].[USP_Item_GetPriceBasedOnPricingMethod]
	@numDomainID NUMERIC(18,0)
	,@numDivisionID NUMERIC(18,0)
	,@numItemCode NUMERIC(18,0)
	,@numWarehouseItemID NUMERIC(18,0) 
	,@numQty FLOAT
	,@vcChildKits VARCHAR(MAX)
AS
BEGIN
	DECLARE @monPrice DECIMAL(20,5)
	SELECT @monPrice=ISNULL(monListPrice,0) FROM Item WHERE numItemCode=@numItemCode

	IF EXISTS (SELECT 
					numItemCode 
				FROM 
					Item 
				WHERE 
					numDomainID=@numDomainID 
					AND numItemCode=@numItemCode 
					AND ISNULL(bitCalAmtBasedonDepItems,0)=1 
					AND (ISNULL(bitKitParent,0) = 1 OR ISNULL(bitAssembly,0)=1))
	BEGIN
		SET @monPrice = dbo.GetCalculatedPriceForKitAssembly(@numDomainID,@numDivisionID,@numItemCode,@numQty,@numWarehouseItemID,1,0,0,@vcChildKits)
	END
	ELSE
	BEGIN
		SELECT
			@monPrice=monFinalPrice
		FROM
			dbo.fn_GetItemPriceBasedOnMethod(@numDomainID,@numDivisionID,@numItemCode,@numQty,0,0,@numWarehouseItemID,@vcChildKits)
	END

	SELECT @monPrice AS monPrice
END
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ItemDetails_GetChildKitsOfKit' ) 
    DROP PROCEDURE USP_ItemDetails_GetChildKitsOfKit
GO

CREATE PROCEDURE USP_ItemDetails_GetChildKitsOfKit  
(  
	@numDomainID NUMERIC(18,0),  
	@numItemCode NUMERIC(18,0)
)  
AS  
BEGIN  
	-- SET NOCOUNT ON added to prevent extra result sets from  
	-- interfering with SELECT statements.  
	SET NOCOUNT ON; 
	DECLARE @numMaxOrder INT
	SET @numMaxOrder = ISNULL((SELECT MAX(sintOrder) FROM ItemDetails WHERE numItemKitID=@numItemCode),0)
	 

	DECLARE @TEMP TABLE
	(
		ID INT IDENTITY(1,1)
		,numItemCode NUMERIC(18,0)
		,tintOrder INT
	)

	INSERT INTO  @TEMP
	(	
		numItemCode
		,tintOrder
	)
	SELECT
		Item.numItemCode
		,1
	FROM
		ItemDetails
	INNER JOIN
		Item
	ON
		ItemDetails.numChildItemID = Item.numItemCode
		AND ISNULL(Item.bitKitParent,0) = 1
	WHERE
		ItemDetails.numItemKitID = @numItemCode
		AND (SELECT 
				COUNT(*) 
			FROM 
				FieldRelationship FR 
			WHERE 
				FR.numDomainID=@numDomainID 
				AND FR.numModuleID = 14 
				AND numPrimaryListID IN (SELECT numChildItemID FROM ItemDetails ID WHERE ID.numItemKitID=@numItemCode) 
				AND FR.numSecondaryListID=Item.numItemCode) = 0
	ORDER BY
		ISNULL(NULLIF(sintOrder,0),@numMaxOrder + 1),numItemDetailID


	DECLARE @TEMPDependedKits TABLE
	(
		numParentKitItemCode NUMERIC(18,0)
		,numChildKitItemCode NUMERIC(18,0)
	)

	INSERT INTO @TEMPDependedKits
	(
		numParentKitItemCode
		,numChildKitItemCode
	)
	SELECT
		numPrimaryListID
		,numSecondaryListID
	FROM
		FieldRelationship FR
	WHERE
		FR.numDomainID=@numDomainID
		AND FR.numModuleID = 14
		AND numPrimaryListID IN (SELECT numChildItemID FROM ItemDetails WHERE numItemKitID=@numItemCode)
		AND numSecondaryListID IN (SELECT numChildItemID FROM ItemDetails WHERE numItemKitID=@numItemCode)
	GROUP BY
		numPrimaryListID
		,numSecondaryListID


	;WITH CTE (ID,numItemCode,tintOrder) AS
	(
		SELECT
			T1.ID
			,T1.numItemCode
			,T1.tintOrder
		FROM
			@TEMP T1
		UNION ALL
		SELECT 
			C1.ID
			,T2.numChildKitItemCode
			,C1.tintOrder + 1
		FROM
			@TEMPDependedKits T2
		INNER JOIN	
			ItemDetails
		ON
			T2.numChildKitItemCode = ItemDetails.numChildItemID
			AND ItemDetails.numItemKitID = @numItemCode
		INNER JOIN
			CTE C1
		ON
			T2.numParentKitItemCode=C1.numItemCode
	)
  
	SELECT
		Item.numItemCode,
		Item.vcItemName,
		ISNULL(Item.bitKitSingleSelect,0) AS bitKitSingleSelect,
		ISNULL(STUFF((SELECT 
					CONCAT(',',numSecondaryListID)
				FROM 
					FieldRelationship FR
				WHERE 
					FR.numDomainID=@numDomainID
					AND FR.numModuleID = 14
					AND numPrimaryListID=Item.numItemCode
					AND numSecondaryListID IN (SELECT numChildItemID FROM ItemDetails WHERE numItemKitID=@numItemCode)
				GROUP BY
					numSecondaryListID
				FOR XML PATH('')),1,1,''),'') vcDependedKits,
		ISNULL(STUFF((SELECT 
					CONCAT(',',numPrimaryListID)
				FROM 
					FieldRelationship FR 
				WHERE 
					FR.numDomainID=@numDomainID
					AND FR.numModuleID = 14 
					AND numSecondaryListID=Item.numItemCode
					AND numPrimaryListID IN (SELECT numChildItemID FROM ItemDetails WHERE numItemKitID=@numItemCode)
				GROUP BY
					numPrimaryListID
				FOR XML PATH('')),1,1,''),'') vcParentKits
	FROM
		CTE C1
	INNER JOIN
		Item
	ON
		C1.numItemCode = Item.numItemCode
	ORDER BY
		C1.ID,C1.tintOrder
END  
GO

--Created By Anoop Jayaraj          
--exec USP_ItemDetailsForEcomm @numItemCode=859064,@numWareHouseID=1074,@numDomainID=172,@numSiteId=90
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_itemdetailsforecomm')
DROP PROCEDURE USP_ItemDetailsForEcomm
GO
CREATE PROCEDURE [dbo].[USP_ItemDetailsForEcomm]
@numItemCode as numeric(9),          
@numWareHouseID as numeric(9),    
@numDomainID as numeric(9),
@numSiteId as numeric(9),
@vcCookieId as VARCHAR(MAX)=null,
@numUserCntID AS NUMERIC(9)=0,
@numDivisionID AS NUMERIC(18,0) = 0                                        
AS     
BEGIN
/*Removed warehouse parameter, and taking Default warehouse by default from DB*/
    
	DECLARE @bitShowInStock AS BIT        
	DECLARE @bitShowQuantity AS BIT
	DECLARE @bitAutoSelectWarehouse AS BIT              
	DECLARE @numDefaultWareHouseID AS NUMERIC(9)
	DECLARE @numDefaultRelationship AS NUMERIC(18,0)
	DECLARE @numDefaultProfile AS NUMERIC(18,0)
	DECLARE @tintPreLoginPriceLevel AS TINYINT
	DECLARE @tintPriceLevel AS TINYINT
	DECLARE @bitMatrix BIT
	DECLARE @numItemGroup NUMERIC(18,0)
	SET @bitShowInStock=0        
	SET @bitShowQuantity=0        
       
        
	IF ISNULL(@numDivisionID,0) > 0
	BEGIN
		SELECT 
			@numDefaultRelationship=ISNULL(numCompanyType,0)
			,@numDefaultProfile=ISNULL(vcProfile,0)
			,@tintPriceLevel=ISNULL(tintPriceLevel,0) 
		FROM 
			DivisionMaster 
		INNER JOIN 
			CompanyInfo 
		ON 
			DivisionMaster.numCompanyID=CompanyInfo.numCompanyId 
		WHERE 
			numDivisionID=@numDivisionID 
			AND DivisionMaster.numDomainID=@numDomainID
	END     
      
	--KEEP THIS BELOW ABOVE IF CONDITION 
	SELECT 
		@bitShowInStock=ISNULL(bitShowInStock,0)
		,@bitShowQuantity=ISNULL(bitShowQOnHand,0)
		,@bitAutoSelectWarehouse=ISNULL(bitAutoSelectWarehouse,0)
		,@numDefaultRelationship=(CASE WHEN ISNULL(@numDivisionID,0) > 0 THEN @numDefaultRelationship ELSE numRelationshipId END)
		,@numDefaultProfile=(CASE WHEN ISNULL(@numDivisionID,0) > 0 THEN @numDefaultProfile ELSE numProfileId END)
		,@tintPreLoginPriceLevel=(CASE WHEN ISNULL(bitShowPriceUsingPriceLevel,0)=1 THEN (CASE WHEN @numDivisionID > 0 THEN (CASE WHEN ISNULL(bitShowPriceUsingPriceLevel,0)=1 THEN ISNULL(@tintPriceLevel,0) ELSE 0 END) ELSE ISNULL(tintPreLoginProceLevel,0) END) ELSE 0 END)
	FROM 
		eCommerceDTL 
	WHERE 
		numDomainID=@numDomainID

	IF @numWareHouseID=0
	BEGIN
		SELECT @numDefaultWareHouseID=ISNULL(numDefaultWareHouseID,0) FROM [eCommerceDTL] WHERE [numDomainID]=@numDomainID	
		SET @numWareHouseID =@numDefaultWareHouseID;
	END

	DECLARE @vcWarehouseIDs AS VARCHAR(1000)
	IF @bitAutoSelectWarehouse = 1
	BEGIN		
		SELECT @vcWarehouseIDs = ISNULL( STUFF((SELECT DISTINCT ',' + CAST(numWarehouseID AS VARCHAR(10)) FROM [dbo].[Warehouses] WHERE [Warehouses].[numDomainID] = @numDomainID FOR XML PATH('')),1, 1, '') , '')
	END
	ELSE
	BEGIN
		SET @vcWarehouseIDs = @numWareHouseID
	END

	/*If qty is not found in default warehouse then choose next warehouse with sufficient qty */
	IF @bitAutoSelectWarehouse = 1 AND ( SELECT COUNT(*) FROM dbo.WareHouseItems WHERE  numItemID = @numItemCode AND numWarehouseID= @numWareHouseID ) = 0 
	BEGIN
		SELECT TOP 1 
			@numDefaultWareHouseID = numWareHouseID 
		FROM 
			dbo.WareHouseItems 
		WHERE 
			numItemID = @numItemCode 
			AND numOnHand > 0 
		ORDER BY 
			numOnHand DESC 
		
		IF (ISNULL(@numDefaultWareHouseID,0)>0)
			SET @numWareHouseID =@numDefaultWareHouseID;
	END

    DECLARE @UOMConversionFactor AS DECIMAL(18,5)
      
    SELECT 
		@UOMConversionFactor= ISNULL(dbo.fn_UOMConversion(ISNULL(I.numSaleUnit,0),I.numItemCode,I.numDomainId,ISNULL(I.numBaseUnit,0)),0)
		,@bitMatrix=ISNULL(bitMatrix,0)
		,@numItemGroup=ISNULL(numItemGroup,0)
    FROM 
		Item I 
	WHERE 
		numItemCode=@numItemCode   	
 
	DECLARE @strSql AS VARCHAR(MAX)
	
	SET @strSql = CONCAT('WITH tblItem AS 
							(SELECT 
								I.numItemCode
								,vcItemName
								,txtItemDesc
								,charItemType
								,ISNULL(I.bitKitParent,0) bitKitParent
								,ISNULL(bitCalAmtBasedonDepItems,0) bitCalAmtBasedonDepItems
								,ISNULL(I.bitMatrix,0) bitMatrix,(CASE WHEN (ISNULL(I.bitKitParent,0) = 1 OR ISNULL(I.bitAssembly,0) = 1) AND ISNULL(bitCalAmtBasedonDepItems,0) = 1 THEN dbo.GetCalculatedPriceForKitAssembly(I.numDomainID,',@numDivisionID,',I.numItemCode,1,0,ISNULL(I.tintKitAssemblyPriceBasedOn,1),0,0,'''') ELSE 
								',(CASE 
									WHEN @tintPreLoginPriceLevel > 0 
									THEN CONCAT('ISNULL(PricingOption.monPrice,ISNULL(CASE WHEN I.[charItemType]=''P'' THEN ',@UOMConversionFactor,' * W.[monWListPrice] ELSE ',@UOMConversionFactor,' * monListPrice END,0))') 
									ELSE CONCAT('ISNULL(CASE WHEN I.[charItemType]=''P'' THEN ',@UOMConversionFactor,' * W.[monWListPrice] ELSE ',@UOMConversionFactor,' * monListPrice END,0)')
								END),'END) AS monListPrice
								,(CASE WHEN (ISNULL(I.bitKitParent,0) = 1 OR ISNULL(I.bitAssembly,0) = 1) AND ISNULL(bitCalAmtBasedonDepItems,0) = 1 THEN dbo.GetCalculatedPriceForKitAssembly(I.numDomainID,',@numDivisionID,',I.numItemCode,1,0,ISNULL(I.tintKitAssemblyPriceBasedOn,1),0,0,'''') ELSE ISNULL(CASE WHEN I.[charItemType]=''P'' 
											THEN ',@UOMConversionFactor,' * W.[monWListPrice]  
											ELSE ',@UOMConversionFactor,' * monListPrice 
										END,0) END) AS monMSRP
										,numItemClassification
										,bitTaxable
										,vcSKU
										,I.numModifiedBy
										,(SELECT numItemImageId,vcPathForImage,vcPathForTImage,bitDefault,case when bitdefault=1  then -1 else isnull(intDisplayOrder,0) end as intDisplayOrder FROM ItemImages  WHERE numItemCode=I.numItemCode and bitIsImage = 1 order by case when bitdefault=1 then -1 else isnull(intDisplayOrder,0) end  asc FOR XML AUTO,ROOT(''Images''))  vcImages
										,(SELECT vcPathForImage FROM ItemImages  WHERE numItemCode=I.numItemCode and bitIsImage = 0 order by intDisplayOrder  asc FOR XML AUTO,ROOT(''Documents''))  vcDocuments
										,ISNULL(bitSerialized,0) as bitSerialized
										,vcModelID
										,numItemGroup
										,(ISNULL(sum(numOnHand),0) / ',@UOMConversionFactor,') as numOnHand
										,sum(numOnOrder) as numOnOrder,                    
										sum(numReorder)  as numReorder,                    
										sum(numAllocation)  as numAllocation,                    
										sum(numBackOrder)  as numBackOrder,              
										isnull(fltWeight,0) as fltWeight,              
										isnull(fltHeight,0) as fltHeight,              
										isnull(fltWidth,0) as fltWidth,              
										isnull(fltLength,0) as fltLength,              
										case when charItemType=''P''then isnull(bitFreeShipping,0) else 1 end as bitFreeShipping,
										Case When (case when charItemType=''P''then isnull(bitFreeShipping,0) else 1 end )=1 then ''Free shipping'' else ''Calculated at checkout'' end as Shipping,              
										isnull(bitAllowBackOrder,0) as bitAllowBackOrder,bitShowInStock,bitShowQOnHand,isnull(numWareHouseItemID,0)  as numWareHouseItemID,
										(CASE WHEN charItemType<>''S'' AND charItemType<>''N'' AND ISNULL(bitKitParent,0)=0 THEN         
										 (CASE 
											WHEN I.bitAllowBackOrder = 1 THEN 1
											WHEN (ISNULL(WAll.numTotalOnHand,0)<=0) THEN 0
											ELSE 1
										END) ELSE 1 END ) as bitInStock,
										(
											CASE WHEN ISNULL(bitKitParent,0) = 1
											THEN
												(CASE 
													WHEN ((SELECT COUNT(*) FROM ItemDetails ID INNER JOIN Item IInner ON ID.numChildItemID = IInner.numItemCode WHERE ID.numItemKitID = I.numItemCode AND ISNULL(IInner.bitKitParent,0) = 1)) > 0 
													THEN 1 
													ELSE 0 
												END)
											ELSE
												0
											END
										) bitHasChildKits,
										(case when charItemType<>''S'' AND charItemType<>''N'' AND ISNULL(bitKitParent,0)=0 then         
										 (Case when ',@bitShowInStock,'=1  then 
										 (CASE 
											WHEN I.bitAllowBackOrder = 1 AND I.numDomainID <> 172 AND ISNULL(WAll.numTotalOnHand,0)<=0 AND ISNULL(I.numVendorID,0) > 0 AND dbo.GetVendorPreferredMethodLeadTime(I.numVendorID) > 0 THEN CONCAT(''<font class="vendorLeadTime" style="color:green">Usually ships in '',dbo.GetVendorPreferredMethodLeadTime(I.numVendorID),'' days</font>'')
											WHEN I.bitAllowBackOrder = 1 AND ISNULL(WAll.numTotalOnHand,0)<=0 THEN (CASE WHEN I.numDomainID = 172 THEN ''<font style="color:green">AVAILABLE</font>'' ELSE ''<font style="color:green">In Stock</font>'' END)
											WHEN (ISNULL(WAll.numTotalOnHand,0)<=0) THEN (CASE WHEN I.numDomainID = 172 THEN ''<font style="color:red">ON HOLD</font>'' ELSE ''<font style="color:red">Out Of Stock</font>'' END)
											ELSE (CASE WHEN I.numDomainID = 172 THEN ''<font style="color:green">AVAILABLE</font>'' ELSE ''<font style="color:green">In Stock</font>'' END)  
										END) else '''' end) ELSE '''' END ) as InStock,
										ISNULL(numSaleUnit,0) AS numUOM,
										ISNULL(vcUnitName,'''') AS vcUOMName,
										 ',@UOMConversionFactor,' AS UOMConversionFactor,
										I.numCreatedBy,
										I.numBarCodeId,
										I.[vcManufacturer],
										(SELECT  COUNT(*) FROM  Review where vcReferenceId = CONVERT(VARCHAR(50) ,I.numItemCode) and numDomainId = I.numDomainId and numSiteId = ', @numSiteId,' AND bitHide = 0 ) as ReviewCount ,
										(SELECT  COUNT(*) FROM  Ratings where vcReferenceId = CONVERT(VARCHAR(50) ,I.numItemCode) and numDomainId = I.numDomainId and numSiteId = ', @numSiteId,'  ) as RatingCount ,
										(select top 1 vcMetaKeywords  from MetaTags where numReferenceID = I.numItemCode and tintMetaTagFor =2) as vcMetaKeywords,
										(select top 1 vcMetaDescription  from MetaTags where numReferenceID = I.numItemCode and tintMetaTagFor =2) as vcMetaDescription ,
										(SELECT vcCategoryName  FROM  dbo.Category WHERE numCategoryID =(SELECT TOP 1 numCategoryID FROM dbo.ItemCategory WHERE numItemID = I.numItemCode)) as vcCategoryName
										,ISNULL(TablePromotion.numTotalPromotions,0) numTotalPromotions
										,ISNULL(TablePromotion.vcPromoDesc,'''') vcPromoDesc
										,ISNULL(TablePromotion.bitRequireCouponCode,0) bitRequireCouponCode
										from Item I 
										OUTER APPLY (SELECT * FROM dbo.fn_GetItemPromotions(',@numDomainID,',',@numDivisionID,',I.numItemCode,I.numItemClassification',',',@numDefaultRelationship,',',@numDefaultProfile,',2)) TablePromotion ' + 
										(CASE WHEN @tintPreLoginPriceLevel > 0 THEN CONCAT(' OUTER APPLY(SELECT decDiscount AS monPrice FROM PricingTable WHERE tintRuleType=3 AND numItemCode=I.numItemCode ORDER BY numPricingID OFFSET ',@tintPreLoginPriceLevel-1,' ROWS FETCH NEXT 1 ROWS ONLY) As PricingOption ') ELSE '' END)
										,'                 
										left join  WareHouseItems W                
										on W.numItemID=I.numItemCode and numWareHouseID= ',@numWareHouseID,'
										 OUTER APPLY (SELECT SUM(numOnHand) numTotalOnHand FROM WareHouseItems W 
																							  WHERE  W.numItemID = I.numItemCode
																							  AND numWareHouseID IN (SELECT [OutParam] FROM dbo.[SplitString](''',@vcWarehouseIDs,''','',''))
																							  AND W.numDomainID = ',@numDomainID,') AS WAll
										left join  eCommerceDTL E          
										on E.numDomainID=I.numDomainID     
										LEFT JOIN UOM u ON u.numUOMId=I.numSaleUnit    
										where ISNULL(I.IsArchieve,0)=0 AND I.numDomainID=',@numDomainID,' AND I.numItemCode=',@numItemCode,
										CASE WHEN (SELECT COUNT(*) FROM [dbo].[EcommerceRelationshipProfile] WHERE numSiteID=@numSiteID AND numRelationship = @numDefaultRelationship AND numProfile = @numDefaultProfile) > 0
										THEN
											CONCAT(' AND I.numItemClassification NOT IN (SELECT numItemClassification FROM EcommerceRelationshipProfile ERP INNER JOIN ECommerceItemClassification EIC ON EIC.numECommRelatiionshipProfileID=ERP.ID WHERE ERP.numSiteID=',@numSiteID,' AND ERP.numRelationship=',@numDefaultRelationship,' AND ERP.numProfile=', @numDefaultProfile,')')
										ELSE
											''
										END
										,'           
										group by I.numItemCode, vcItemName, txtItemDesc, charItemType, monListPrice,numItemClassification, bitTaxable, vcSKU, bitKitParent, bitCalAmtBasedonDepItems, bitAssembly, tintKitAssemblyPriceBasedOn, WAll.numTotalOnHand,         
										I.numDomainID,I.numModifiedBy,  bitSerialized, vcModelID,numItemGroup, fltWeight,fltHeight,fltWidth,          
										fltLength,bitFreeShipping,bitAllowBackOrder,bitShowInStock,bitShowQOnHand ,numWareHouseItemID,vcUnitName,I.numCreatedBy,I.numBarCodeId,W.[monWListPrice],I.[vcManufacturer],
										numSaleUnit, I.numVendorID, I.bitMatrix,numTotalPromotions,vcPromoDesc,bitRequireCouponCode ' + (CASE WHEN @tintPreLoginPriceLevel > 0 THEN ',PricingOption.monPrice' ELSE '' END) + ')')

										set @strSql=@strSql+' select numItemCode,vcItemName,txtItemDesc,charItemType, bitMatrix, monListPrice,monMSRP,numItemClassification, bitTaxable, vcSKU, bitKitParent, bitCalAmtBasedonDepItems,                  
										tblItem.numModifiedBy, vcImages,vcDocuments, bitSerialized, vcModelID, numItemGroup,numOnHand,numOnOrder,numReorder,numAllocation, 
										numBackOrder, fltWeight, fltHeight, fltWidth, fltLength, bitFreeShipping,bitAllowBackOrder,bitShowInStock,
										bitShowQOnHand,numWareHouseItemID,bitInStock,bitHasChildKits,InStock,numUOM,vcUOMName,UOMConversionFactor,tblItem.numCreatedBy,numBarCodeId,vcManufacturer,ReviewCount,RatingCount,Shipping,isNull(vcMetaKeywords,'''') as vcMetaKeywords ,isNull(vcMetaDescription,'''') as vcMetaDescription,isNull(vcCategoryName,'''') as vcCategoryName,numTotalPromotions,vcPromoDesc,bitRequireCouponCode'

	set @strSql=@strSql+ ' from tblItem ORDER BY numOnHand DESC'
	PRINT CAST(@strSql AS NTEXT)
	exec (@strSql)


	declare @tintOrder as tinyint                                                  
	declare @vcFormFieldName as varchar(50)                                                  
	declare @vcListItemType as varchar(1)                                             
	declare @vcAssociatedControlType varchar(10)                                                  
	declare @numListID AS numeric(9)                                                  
	declare @WhereCondition varchar(2000)                       
	Declare @numFormFieldId as numeric  
	DECLARE @vcFieldType CHAR(1)
	DECLARE @GRP_ID INT
                  
	set @tintOrder=0                                                  
	set @WhereCondition =''                 
                   
              
	CREATE TABLE #tempAvailableFields
	(
		numFormFieldId NUMERIC(9)
		,vcFormFieldName NVARCHAR(50)
		,vcFieldType CHAR(1)
		,vcAssociatedControlType NVARCHAR(50)
		,numListID NUMERIC(18)
		,vcListItemType CHAR(1)
		,intRowNum INT
		,vcItemValue VARCHAR(3000)
		,GRP_ID INT
	)
   
	INSERT INTO #tempAvailableFields 
	(
		numFormFieldId
		,vcFormFieldName
		,vcFieldType
		,vcAssociatedControlType
        ,numListID
		,vcListItemType
		,intRowNum
		,GRP_ID
	)                         
    SELECT 
		Fld_id
		,REPLACE(Fld_label,' ','') AS vcFormFieldName
		,CASE GRP_ID WHEN 4 then 'C' WHEN 1 THEN 'D' ELSE 'C' END AS vcFieldType
		,fld_type as vcAssociatedControlType
		,ISNULL(C.numListID, 0) numListID
		,CASE WHEN C.numListID > 0 THEN 'L' ELSE '' END vcListItemType
		,ROW_NUMBER() OVER (ORDER BY Fld_id asc) AS intRowNum
		,GRP_ID
    FROM 
		CFW_Fld_Master C 
	LEFT JOIN 
		CFW_Validation V 
	ON 
		V.numFieldID = C.Fld_id
    WHERE 
		C.numDomainID = @numDomainId
        AND GRP_ID  = 5

    SELECT TOP 1 
		@tintOrder=intRowNum
		,@numFormFieldId=numFormFieldId
		,@vcFormFieldName=vcFormFieldName
		,@vcFieldType=vcFieldType
		,@vcAssociatedControlType=vcAssociatedControlType
		,@numListID=numListID
		,@vcListItemType=vcListItemType
		,@GRP_ID=GRP_ID
	FROM 
		#tempAvailableFields 
	ORDER BY 
		intRowNum ASC
   
	while @tintOrder>0                                                  
	begin                   
		IF @GRP_ID=5
		BEGIN
			IF @vcAssociatedControlType = 'TextBox' or @vcAssociatedControlType = 'TextArea'
			BEGIN  
				UPDATE #tempAvailableFields SET vcItemValue=(SELECT Fld_Value FROM CFW_FLD_Values_Item WHERE Fld_Id=@numFormFieldId AND RecId=@numItemCode) WHERE numFormFieldId=@numFormFieldId              
			END   
			ELSE IF @vcAssociatedControlType = 'CheckBox'
			BEGIN      
				UPDATE #tempAvailableFields SET vcItemValue=(SELECT case when isnull(Fld_Value,0)=0 then 'No' when isnull(Fld_Value,0)=1 then 'Yes' END FROM CFW_FLD_Values_Item WHERE Fld_Id=@numFormFieldId AND RecId=@numItemCode) WHERE numFormFieldId=@numFormFieldId              
			END                
			ELSE IF @vcAssociatedControlType = 'DateField'           
			BEGIN   
				UPDATE #tempAvailableFields SET vcItemValue=(SELECT dbo.FormatedDateFromDate(Fld_Value,@numDomainId) FROM CFW_FLD_Values_Item WHERE Fld_Id=@numFormFieldId AND RecId=@numItemCode) WHERE numFormFieldId=@numFormFieldId              
			END                
			ELSE IF @vcAssociatedControlType = 'SelectBox'           
			BEGIN 
				UPDATE #tempAvailableFields SET vcItemValue=(SELECT L.vcData FROM CFW_FLD_Values_Item CFW left Join ListDetails L
					on L.numListItemID=CFW.Fld_Value                
					WHERE CFW.Fld_Id=@numFormFieldId AND CFW.RecId=@numItemCode) WHERE numFormFieldId=@numFormFieldId              
			END   
		END    
 
		SELECT TOP 1 
			@tintOrder=intRowNum
			,@numFormFieldId=numFormFieldId
			,@vcFormFieldName=vcFormFieldName
			,@vcFieldType=vcFieldType
			,@vcAssociatedControlType=vcAssociatedControlType
			,@numListID=numListID
			,@vcListItemType=vcListItemType
			,@GRP_ID=GRP_ID
		FROM 
			#tempAvailableFields 
		WHERE 
			intRowNum > @tintOrder 
		ORDER BY 
			intRowNum ASC
 
	   IF @@rowcount=0 set @tintOrder=0                                                  
	END   


	INSERT INTO #tempAvailableFields 
	(
		numFormFieldId
		,vcFormFieldName
		,vcFieldType
		,vcAssociatedControlType
		,numListID
		,vcListItemType
		,intRowNum
		,vcItemValue
		,GRP_ID
	)                         
	SELECT 
		Fld_id
		,REPLACE(Fld_label,' ','') AS vcFormFieldName
		,CASE GRP_ID WHEN 4 then 'C' WHEN 1 THEN 'D' ELSE 'C' END AS vcFieldType
		,fld_type as vcAssociatedControlType
		,ISNULL(C.numListID, 0) numListID
		,CASE WHEN C.numListID > 0 THEN 'L' ELSE '' END vcListItemType
		,ROW_NUMBER() OVER (ORDER BY Fld_id asc) AS intRowNum
		,''
		,GRP_ID
	FROM 
		CFW_Fld_Master C 
	LEFT JOIN 
		CFW_Validation V 
	ON 
		V.numFieldID = C.Fld_id
	WHERE 
		C.numDomainID = @numDomainId
		AND GRP_ID = 9

	IF ISNULL(@bitMatrix,0) = 1 AND ISNULL(@numItemGroup,0) > 0
	BEGIN
		UPDATE
			TEMP
		SET
			TEMP.vcItemValue = FLD_ValueName
		FROM
			#tempAvailableFields TEMP
		INNER JOIN
		(
			SELECT 
				CFW_Fld_Master.fld_id
				,CASE 
					WHEN CFW_Fld_Master.fld_type='SelectBox' THEN dbo.GetListIemName(Fld_Value)
					WHEN CFW_Fld_Master.fld_type='CheckBox' THEN CASE WHEN isnull(Fld_Value,0)=1 THEN 'Yes' ELSE 'No' END
					ELSE CAST(Fld_Value AS VARCHAR)
				END AS FLD_ValueName
			FROM 
				CFW_Fld_Master 
			INNER JOIN
				ItemGroupsDTL 
			ON 
				CFW_Fld_Master.Fld_id = ItemGroupsDTL.numOppAccAttrID
				AND ItemGroupsDTL.tintType = 2
			LEFT JOIN
				ItemAttributes
			ON
				CFW_Fld_Master.Fld_id = ItemAttributes.FLD_ID
				AND ItemAttributes.numItemCode = @numItemCode
			LEFT JOIN
				ListDetails LD
			ON
				CFW_Fld_Master.numlistid = LD.numListID
			WHERE
				CFW_Fld_Master.numDomainID = @numDomainId
				AND ItemGroupsDTL.numItemGroupID = @numItemGroup
			GROUP BY
				CFW_Fld_Master.Fld_label
				,CFW_Fld_Master.fld_id
				,CFW_Fld_Master.fld_type
				,CFW_Fld_Master.bitAutocomplete
				,CFW_Fld_Master.numlistid
				,CFW_Fld_Master.vcURL
				,ItemAttributes.FLD_Value 
		) T1
		ON
			TEMP.numFormFieldId = T1.fld_id
		WHERE
			TEMP.GRP_ID=9
	END
  
	SELECT * FROM #tempAvailableFields

	DROP TABLE #tempAvailableFields
END
GO
/****** Object:  StoredProcedure [dbo].[USP_ItemPricingRecomm]    Script Date: 02/19/2009 01:33:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- exec USP_ItemPricingRecomm @numItemCode=173028,@units=1,@numOppID=0,@numDivisionID=7052,@numDomainID=72,@tintOppType=1,@CalPrice=$50,@numWareHouseItemID=130634,@bitMode=0
--created by anoop jayaraj                          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_itempricingrecomm')
DROP PROCEDURE usp_itempricingrecomm
GO
CREATE PROCEDURE [dbo].[USP_ItemPricingRecomm]                                        
@numItemCode as numeric(9),                                        
@units Float,                                  
@numOppID as numeric(9)=0,                                
@numDivisionID as numeric(9)=0,                  
@numDomainID as numeric(9)=0,            
@tintOppType as tinyint,          
@CalPrice as DECIMAL(20,5),      
@numWareHouseItemID as numeric(9),
@bitMode tinyint=0,
@vcSelectedKitChildItems VARCHAR(MAX) = '',
@numOppItemID NUMERIC(18,0) = 0
as                 
BEGIN TRY

DECLARE @numRelationship AS NUMERIC(9)
DECLARE @numProfile AS NUMERIC(9)             
DECLARE @monListPrice DECIMAL(20,5)
DECLARE @bitCalAmtBasedonDepItems BIT
DECLARE @numDefaultSalesPricing TINYINT
DECLARE @tintPriceLevel INT
DECLARE @decmUOMConversion decimal(18,2)
DECLARE @intLeadTimeDays INT
DECLARE @intMinQty INT
DECLARE @tintKitAssemblyPriceBasedOn TINYINT

/*Profile and relationship id */
SELECT 
	@numRelationship=numCompanyType,
	@numProfile=vcProfile,
	@tintPriceLevel = ISNULL(D.tintPriceLevel,0)
FROM 
	DivisionMaster D                  
JOIN 
	CompanyInfo C 
ON 
	C.numCompanyId=D.numCompanyID                  
WHERE 
	numDivisionID =@numDivisionID       
            
IF @tintOppType=1            
BEGIN
	SELECT
		@bitCalAmtBasedonDepItems=ISNULL(bitCalAmtBasedonDepItems,0)
		,@tintKitAssemblyPriceBasedOn=ISNULL(tintKitAssemblyPriceBasedOn,1)
	FROM 
		Item 
	WHERE 
		numItemCode = @numItemCode
      
	/*Get List Price for item*/      
	IF((@numWareHouseItemID>0) AND EXISTS(SELECT * FROM Item WHERE numItemCode=@numItemCode AND charItemType='P'))      
	BEGIN      
		SELECT 
			@monListPrice=ISNULL(monWListPrice,0) 
		FROM 
			WareHouseItems 
		WHERE 
			numWareHouseItemID=@numWareHouseItemID      
		
		IF @monListPrice=0 
		BEGIN
			SELECT @monListPrice=monListPrice FROM Item WHERE numItemCode=@numItemCode       
		END
	END 
	ELSE
	BEGIN
		 SELECT 
			@monListPrice=monListPrice 
		FROM 
			Item 
		WHERE 
			numItemCode=@numItemCode      
	END      

	/*Use Calculated Price When item is Assembly/Kit,Calculate price based on sum of dependent items.*/
	IF @bitCalAmtBasedonDepItems = 1 
	BEGIN
		DECLARE @TEMPPrice TABLE
		(
			bitSuccess BIT
			,monPrice DECIMAL(20,5)
		)
		INSERT INTO @TEMPPrice
		(
			bitSuccess
			,monPrice
		)
		SELECT
			bitSuccess
			,monMSRPPrice
		FROM
			dbo.fn_GetKitAssemblyCalculatedPrice(@numDomainID,@numDivisionID,@numItemCode,@units,@numWarehouseItemID,@tintKitAssemblyPriceBasedOn,@numOppID,@numOppItemID,@vcSelectedKitChildItems)

		IF (SELECT bitSuccess FROM @TEMPPrice) = 1
		BEGIN
			SET @CalPrice = (SELECT monPrice FROM @TEMPPrice)
		END
		ELSE
		BEGIN
			RAISERROR('SELECTED_WAREHOUSE_IS_NOT_AVAILABLE_FOR_ALL_CHILD_ITEMS',16,1)
			RETURN
		END
	END


	SELECT 
		@numDefaultSalesPricing = ISNULL(numDefaultSalesPricing,2) 
	FROM 
		Domain 
	WHERE 
		numDomainID = @numDomainID

	IF @numDefaultSalesPricing = 1 -- Use Price Level
	BEGIN
		DECLARE @newPrice DECIMAL(20,5)
		DECLARE @finalUnitPrice FLOAT = 0
		DECLARE @tintRuleType INT
		DECLARE @tintDiscountType INT
		DECLARE @decDiscount FLOAT
		DECLARE @ItemPrice FLOAT

		SET @tintRuleType = 0
		SET @tintDiscountType = 0
		SET @decDiscount  = 0
		SET @ItemPrice = 0

		SELECT TOP 1
			@tintRuleType = tintRuleType,
			@tintDiscountType = tintDiscountType,
			@decDiscount = decDiscount
		FROM 
			PricingTable 
		WHERE 
			numItemCode = @numItemCode AND
			((@units BETWEEN intFromQty AND intToQty) OR tintRuleType = 3)

		IF @tintRuleType > 0 AND (@tintDiscountType > 0 OR @tintRuleType = 3)
		BEGIN
			IF @tintRuleType = 1 -- Deduct from List price
			BEGIN
				IF (SELECT ISNULL(charItemType,'') FROM Item WHERE numDomainID = @numDomainID AND numItemCode = @numItemCode) = 'P'
					If @bitCalAmtBasedonDepItems = 1 
						SELECT @ItemPrice = @CalPrice
					ELSE
						SELECT @ItemPrice = ISNULL(monWListPrice,0) FROM WareHouseItems WHERE numDomainID = @numDomainID AND numItemID = @numItemCode AND numWareHouseItemID = @numWarehouseItemID
				ELSE
					SELECT @ItemPrice = ISNULL(monListPrice,0) FROM Item WHERE numDomainID = @numDomainID AND numItemCode = @numItemCode

				IF @tintDiscountType = 1 -- Percentage
				BEGIN
					IF @decDiscount > 0
						SELECT @finalUnitPrice = @ItemPrice - (@ItemPrice * (@decDiscount/100))
				END
				ELSE IF @tintDiscountType = 2 -- Flat Amount
				BEGIN
					SELECT @finalUnitPrice = @ItemPrice - @decDiscount
				END
				ELSE IF @tintDiscountType = 3 -- Named Price
				BEGIN
					SELECT @finalUnitPrice = @decDiscount
				END
			END
			ELSE IF @tintRuleType = 2 -- Add to Primary Vendor Cost
			BEGIN
				If @bitCalAmtBasedonDepItems = 1 
					SELECT @ItemPrice = @CalPrice
				ELSE
					SELECT @ItemPrice = ISNULL(monCost,0) FROM Vendor WHERE numItemCode = @numItemCode AND numVendorID = (SELECT numVendorID FROM Item WHERE numItemCode = @numItemCode)

				IF @tintDiscountType = 1 AND @ItemPrice > 0 -- Percentage
				BEGIN
					IF @decDiscount > 0
						SELECT @finalUnitPrice = @ItemPrice + (@ItemPrice * (@decDiscount/100))
				END
				ELSE IF @tintDiscountType = 2 AND @ItemPrice > 0 -- Flat Amount
				BEGIN
					SELECT @finalUnitPrice = @ItemPrice + @decDiscount
				END
				ELSE IF @tintDiscountType = 3 -- Named Price
				BEGIN
					SELECT @finalUnitPrice = @decDiscount
				END
			END
			ELSE IF @tintRuleType = 3 -- Named Price
			BEGIN
				IF ISNULL(@tintPriceLevel,0) > 0
				BEGIN
					SET @decDiscount = 0
					SET @tintDiscountType = 2
					SET @finalUnitPrice = 0 
					
					SELECT 
						@finalUnitPrice = ISNULL(decDiscount,0)
					FROM
					(
						SELECT 
							ROW_NUMBER() OVER(PARTITION BY PricingTable.numItemCode ORDER BY numPricingID) Id,
							decDiscount
						FROM 
							PricingTable 
						WHERE 
						    PricingTable.numItemCode = @numItemCode 
							AND tintRuleType = 3 
					) TEMP
					WHERE
						Id = @tintPriceLevel

					IF ISNULL(@finalUnitPrice,0) = 0
					BEGIN
						SET @finalUnitPrice = @monListPrice
					END
					ELSE
					BEGIN
						SET @monListPrice = @finalUnitPrice
					END
				END
				ELSE
				BEGIN
					SET @finalUnitPrice = @decDiscount
					SET @monListPrice = @finalUnitPrice
					-- KEEP THIS LINE AT END
					SET @decDiscount = 0
					SET @tintDiscountType = 2
				END				
			END
		END

		IF @finalUnitPrice = 0
			SET @newPrice = @monListPrice
		ELSE
			SET @newPrice = @finalUnitPrice

		If @tintRuleType = 2
		BEGIN
			IF @finalUnitPrice > 0
			BEGIN
				IF @tintDiscountType = 1 -- Percentage
				BEGIN
					IF @newPrice > @monListPrice
						SET @decDiscount = 0
					ELSE
						If @monListPrice > 0
							SET @decDiscount = ((@monListPrice - @newPrice) * 100) / CAST(@monListPrice AS FLOAT)
						ELSE
							SET @decDiscount = 0
				END
				ELSE IF @tintDiscountType = 2 -- Flat Amount
				BEGIN
					If @newPrice > @monListPrice
						SET @decDiscount = 0
					ELSE
						SET @decDiscount = @monListPrice - @newPrice
				END
			END
			ELSE
			BEGIN
				SET @decDiscount = 0
				SET @tintDiscountType = 0
			END
		END

		SELECT 
			1 as numUintHour, 
			ISNULL(@newPrice,0) AS ListPrice,
			'' AS vcPOppName,
			'' AS bintCreatedDate,
			CASE 
				WHEN @tintRuleType = 0 THEN 'Price - List price'
				ELSE 
				   CASE @tintRuleType
				   WHEN 1 
						THEN 
							'Deduct from List price ' +  CASE 
														   WHEN @tintDiscountType = 1 THEN CONVERT(VARCHAR(20), @decDiscount) + '%'
														   WHEN @tintDiscountType = 2 THEN 'flat ' + CONVERT(VARCHAR(20), CAST(@decDiscount AS int))
														   ELSE 'Name price ' + CONVERT(VARCHAR(20), CAST(@decDiscount AS int))
														   END 
				   WHEN 2 THEN 'Add to primary vendor cost '  +  CASE 
														   WHEN @tintDiscountType = 1 THEN CONVERT(VARCHAR(20), @decDiscount) + '%'
														   WHEN @tintDiscountType = 2 THEN 'flat ' + CONVERT(VARCHAR(20), CAST(@decDiscount AS int))
														   ELSE 'Name price ' + CONVERT(VARCHAR(20), CAST(@decDiscount AS int))
														   END 
				   ELSE 'Flat ' + CONVERT(VARCHAR(20), @decDiscount)
				   END
			END AS OppStatus,
			CAST(0 AS NUMERIC) AS numPricRuleID,
			CAST(1 AS TINYINT) AS tintPricingMethod,
			CAST(@tintDiscountType AS TINYINT) AS tintDiscountTypeOriginal,
			CAST(@decDiscount AS FLOAT) AS decDiscountOriginal,
			CAST(@tintRuleType AS TINYINT) AS tintRuleType
	END
	ELSE -- Use Price Rule
	BEGIN
		/* Checks Pricebook if exist any.. */      
		SELECT TOP 1 1 AS numUnitHour,
				dbo.GetPriceBasedOnPriceBook(P.[numPricRuleID],CASE WHEN @bitCalAmtBasedonDepItems = 1 THEN @CalPrice ELSE @monListPrice END,@units,I.numItemCode) 
				* (CASE WHEN (P.tintRuleType=2 OR (P.tintRuleType=0 AND ISNULL(PBT.tintRuleType,0) = 2)) THEN dbo.fn_UOMConversion(numBaseUnit,@numItemCode,@numDomainId,CASE WHEN ISNULL(numPurchaseUnit,0)>0 THEN numPurchaseUnit ELSE numBaseUnit END) ELSE 1 END)  AS ListPrice,
				vcRuleName AS vcPOppName,
				CONVERT(VARCHAR(20), NULL) AS bintCreatedDate,
				CASE WHEN numPricRuleID IS NULL THEN 'Price - List price'
					 ELSE 
					  CASE WHEN P.tintPricingMethod = 1
										 THEN 'Price Book Rule'
										 ELSE CASE PBT.tintRuleType
												WHEN 1 THEN 'Deduct from List price '
												WHEN 2 THEN 'Add to primary vendor cost '
												ELSE ''
											  END
											  + CASE 
													WHEN tintDiscountType = 1 THEN CONVERT(VARCHAR(20), decDiscount) + '%'
													WHEN tintDiscountType = 2 THEN 'flat ' + CONVERT(VARCHAR(20), CAST(decDiscount AS int))
													ELSE 'Named price ' + CONVERT(VARCHAR(20), CAST(decDiscount AS int))
												END + ' for every '
											  + CONVERT(VARCHAR(20), CASE WHEN [intQntyItems] = 0 THEN 1
																		  ELSE intQntyItems
																	 END)
											  + ' units, until maximum of '
											  + CONVERT(VARCHAR(20), decMaxDedPerAmt)
											  + CASE WHEN tintDiscountType = 1 THEN ' %' ELSE ' Flat' END END
				END AS OppStatus,P.numPricRuleID,P.tintPricingMethod,PBT.tintDiscountTypeOriginal,CAST(PBT.decDiscountOriginal AS FLOAT) AS decDiscountOriginal,PBT.tintRuleType
		FROM    Item I
				LEFT JOIN PriceBookRules P ON I.numDomainID = P.numDomainID
				LEFT JOIN PriceBookRuleDTL PDTL ON P.numPricRuleID = PDTL.numRuleID
				LEFT JOIN PriceBookRuleItems PBI ON P.numPricRuleID = PBI.numRuleID
				LEFT JOIN [PriceBookPriorities] PP ON PP.[Step2Value] = P.[tintStep2] AND PP.[Step3Value] = P.[tintStep3]
				CROSS APPLY dbo.GetPriceBasedOnPriceBookTable(P.[numPricRuleID],CASE WHEN bitCalAmtBasedonDepItems=1 then @CalPrice ELSE @monListPrice END ,@units,I.numItemCode) as PBT
		WHERE   numItemCode = @numItemCode AND tintRuleFor=@tintOppType
		AND 
		(
		((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 1 AND PDTL.numValue = @numDivisionID)) -- Priority 1
		OR ((tintStep2 = 2 AND PBI.numValue = I.numItemClassification) AND (tintStep3= 1 AND PDTL.numValue = @numDivisionID)) -- Priority 2
		OR ((tintStep2 = 3) AND (tintStep3 = 1 AND PDTL.numValue = @numDivisionID)) -- Priority 3

		OR ((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 4
		OR ((tintStep2 = 2 AND PBI.numValue = I.numItemClassification) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 5
		OR ((tintStep2 = 3) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 6

		OR ((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 3)) -- Priority 7
		OR ((tintStep2 = 2 AND PBI.numValue = I.numItemClassification) AND (tintStep3 = 3)) -- Priority 8
		OR ((tintStep2 = 3) and (tintStep3 = 3)) -- Priority 9
		)
		ORDER BY PP.Priority ASC
	END

	IF @bitMode=2  -- When this procedure is being called from USP_GetPriceOfItem for e-com
		RETURN 

                                   
	select 1 as numUnitHour,convert(DECIMAL(20,5),ISNULL((monTotAmount/(case when numUnitHour< 1 then 1 else numUnitHour end)),0)) as ListPrice,vcPOppName,convert(varchar(20),bintCreatedDate) as bintCreatedDate,case when tintOppStatus=0 then 'Opportunity' when tintOppStatus=1 then 'Deal won'   
	when tintOppStatus=2 then 'Deal Lost' end as OppStatus,CAST(0 AS NUMERIC) AS numPricRuleID,CAST(0 AS TINYINT) AS tintPricingMethod,CAST(0 AS TINYINT) as tintDiscountTypeOriginal,CAST(0 AS FLOAT) as decDiscountOriginal,CAST(0 AS TINYINT) as tintRuleType                         
	from OpportunityItems itm                                        
	join OpportunityMaster mst                                        
	on mst.numOppId=itm.numOppId              
	where  tintOppType=1 and numItemCode=@numItemCode and mst.numDivisionID=@numDivisionID order by OppStatus desc  
  
  
	select 1 as numUnitHour,CASE WHEN @bitCalAmtBasedonDepItems=1 THEN convert(DECIMAL(20,5),ISNULL(@CalPrice,0)) ELSE convert(DECIMAL(20,5),ISNULL(@monListPrice,0)) End as ListPrice,'' as vcPOppName,convert(varchar(20),null) as bintCreatedDate,CASE WHEN @bitCalAmtBasedonDepItems=1 THEN 'Sum of dependent items' ELSE '' END as OppStatus
	,CAST(0 AS NUMERIC) AS numPricRuleID,CAST(0 AS TINYINT) AS tintPricingMethod ,CAST(0 AS TINYINT) as tintDiscountTypeOriginal,CAST(0 AS FLOAT) as decDiscountOriginal,CAST(0 AS TINYINT) as tintRuleType                    
END             
ELSE            
BEGIN

	If @numDivisionID=0
	BEGIN
		SELECT 
			@numDivisionID=V.numVendorID 
		FROM 
			[Vendor] V 
		INNER JOIN 
			Item I 
		ON 
			V.numVendorID = I.numVendorID 
			AND V.numItemCode=I.numItemCode 
		WHERE 
			V.[numItemCode]=@numItemCode 
			AND V.[numDomainID]=@numDomainID 
	END

	IF EXISTS
	(
		SELECT 
			ISNULL([monCost],0) ListPrice 
		FROM 
			[Vendor] V 
		INNER JOIN 
			dbo.Item I 
		ON 
			V.numItemCode = I.numItemCode
		WHERE
			V.[numItemCode]=@numItemCode 
			AND V.[numDomainID]=@numDomainID 
			AND V.numVendorID=@numDivisionID
	)       
	BEGIN      
		SELECT 
			@monListPrice=ISNULL([monCost],0) 
		FROM 
			[Vendor] V 
		INNER JOIN 
			dbo.Item I 
		ON 
			V.numItemCode = I.numItemCode
		WHERE 
			V.[numItemCode]=@numItemCode 
			AND V.[numDomainID]=@numDomainID 
			AND V.numVendorID=@numDivisionID
	END      
	ELSE
	BEGIN   
		SELECT 
			@monListPrice=ISNULL([monCost],0) 
		FROM 
			[Vendor] 
		WHERE 
			[numItemCode]=@numItemCode 
			AND [numDomainID]=@numDomainID
	END      
 

	SELECT TOP 1 
		1 AS numUnitHour,
		ISNULL(dbo.GetPriceBasedOnPriceBook(P.[numPricRuleID],@monListPrice,@units,I.numItemCode),0) AS ListPrice,
		vcRuleName AS vcPOppName,
		CONVERT(VARCHAR(20), NULL) AS bintCreatedDate,
		CASE 
		WHEN numPricRuleID IS NULL THEN 'Price - List price'
		ELSE  
				CASE 
				WHEN P.tintPricingMethod = 1 THEN 'Price Book Rule'
				ELSE 
					(CASE tintRuleType
						WHEN 1 THEN 'Deduct from List price '
						WHEN 2 THEN 'Add to primary vendor cost '
						ELSE ''
					END) + 
					(CASE 
						WHEN tintDiscountType = 1 THEN CONVERT(VARCHAR(20), decDiscount) + '%'
						ELSE 'flat ' + CONVERT(VARCHAR(20), CAST(decDiscount AS int))
					END) + ' for every ' + 
					CONVERT(VARCHAR(20), (CASE WHEN [intQntyItems] = 0 THEN 1 ELSE intQntyItems END))
					+ ' units, until maximum of '
					+ CONVERT(VARCHAR(20), decMaxDedPerAmt)
					+ (CASE WHEN tintDiscountType = 1 THEN ' %' ELSE ' Flat' END)
				END
		END AS OppStatus,
		P.numPricRuleID,
		P.tintPricingMethod,
		@numDivisionID AS numDivisionID
	FROM
		Item I 
	LEFT JOIN PriceBookRules P ON I.numDomainID = P.numDomainID
	LEFT JOIN PriceBookRuleDTL PDTL ON P.numPricRuleID = PDTL.numRuleID
	LEFT JOIN PriceBookRuleItems PBI ON P.numPricRuleID = PBI.numRuleID
	LEFT JOIN [PriceBookPriorities] PP ON PP.[Step2Value] = P.[tintStep2] AND PP.[Step3Value] = P.[tintStep3]
	WHERE   
		numItemCode = @numItemCode 
		AND tintRuleFor=@tintOppType
		AND (
			((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 1 AND PDTL.numValue = @numDivisionID)) -- Priority 1
			OR ((tintStep2 = 2 AND PBI.numValue = I.numItemClassification) AND (tintStep3= 1 AND PDTL.numValue = @numDivisionID)) -- Priority 2
			OR ((tintStep2 = 3) AND (tintStep3 = 1 AND PDTL.numValue = @numDivisionID)) -- Priority 3

			OR ((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 4
			OR ((tintStep2 = 2 AND PBI.numValue = I.numItemClassification) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 5
			OR ((tintStep2 = 3) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 6

			OR ((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 3)) -- Priority 7
			OR ((tintStep2 = 2 AND PBI.numValue = I.numItemClassification) AND (tintStep3 = 3)) -- Priority 8
			OR ((tintStep2 = 3) and (tintStep3 = 3)) -- Priority 9
			)
	ORDER BY PP.Priority ASC

	SET @decmUOMConversion=(SELECT 
								dbo.fn_UOMConversion(ISNULL(I.numPurchaseUnit, 0) , I.numItemCode,I.numDomainId, ISNULL(I.numBaseUnit, 0)) 
							FROM 
								Item I 
							WHERE 
								I.numItemCode=@numItemCode)

	SELECT
		@intLeadTimeDays = ISNULL(VSM.numListValue,0),
		@intMinQty=ISNULL(intMinQty,0)
	FROM
		Item
	JOIN
		Vendor
	ON
		Item.numVendorID = Vendor.numVendorID
		AND Item.numItemCode=Vendor.numItemCode
	LEFT JOIN
		VendorShipmentMethod VSM
	ON
		Vendor.numVendorID = VSM.numVendorID
		AND VSM.numDomainID=@numDomainID
		AND VSM.bitPrimary = 1
		AND VSM.bitPreferredMethod = 1
	WHERE
		Item.numItemCode = @numItemCode

	select 1 as numUnitHour,convert(DECIMAL(20,5),ISNULL((monTotAmount/(case when numUnitHour< 1 then 1 else numUnitHour end)),0)) as ListPrice,
	@decmUOMConversion *(convert(DECIMAL(20,5),ISNULL((monTotAmount/(case when numUnitHour< 1 then 1 else numUnitHour end)),0)))
	as convrsPrice
	,vcPOppName,convert(varchar(20),bintCreatedDate) as bintCreatedDate,case when tintOppStatus=0 then 'Opportunity' 
		 when tintOppStatus=1 then 'Deal won'  
		 when tintOppStatus=2 then 'Deal Lost' end as OppStatus
		 ,CAST(0 AS NUMERIC) AS numPricRuleID,CAST(0 AS TINYINT) AS tintPricingMethod,@numDivisionID as numDivisionID                         
		 from OpportunityItems itm                                        
		 join OpportunityMaster mst                                        
		 on mst.numOppId=itm.numOppId                                        
		 where  tintOppType=2 and numItemCode=@numItemCode and mst.numDivisionID=@numDivisionID order by OppStatus,bintCreatedDate DESC

 		select 1 as numUnitHour,convert(DECIMAL(20,5),ISNULL(@monListPrice,0)) as ListPrice,
		@decmUOMConversion*(Convert(DECIMAL(20,5),ISNULL(@monListPrice,0))) as convrsPrice
		,@intLeadTimeDays as LeadTime,@intMinQty as intMinQty,
		(SELECT DATEADD(day,@intLeadTimeDays,GETDATE())) AS EstimatedArrival
		,'' as vcPOppName,convert(varchar(20),null) as bintCreatedDate,''  as OppStatus,@numDivisionID as numDivisionID
 		,CAST(0 AS NUMERIC) AS numPricRuleID,CAST(0 AS TINYINT) AS tintPricingMethod
END

END TRY
BEGIN CATCH
    SELECT 0 AS ListPrice
    SELECT 0 AS ListPrice
    SELECT 0 AS ListPrice
END CATCH
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ItemUnitPriceApproval_Check' ) 
    DROP PROCEDURE USP_ItemUnitPriceApproval_Check
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Sandeep Patel
-- Create date: 1 July 2014
-- Description:	Checks wheather unit price approval is required or not for item
-- =============================================
CREATE PROCEDURE USP_ItemUnitPriceApproval_Check
	@numDomainID numeric(18,0),
	@numDivisionID numeric(18,0),
	@numItemCode numeric(18,0),
	@numQuantity FLOAT,
	@numUnitPrice float,
	@numTotalAmount float,
	@numCost FLOAT,
	@numWarehouseItemID numeric(18,0)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @numItemClassification NUMERIC(18,0)
	DECLARE @numAbovePercent FLOAT
	DECLARE @numAboveField FLOAT
	DECLARE @numBelowPercent FLOAT
	DECLARE @numBelowField FLOAT
	DECLARE @numDefaultCost TINYINT
	DECLARE @bitCostApproval BIT
	DECLARE @bitListPriceApproval BIT
	DECLARE @numCostDomain INT
	DECLARE @tintKitAssemblyPriceBasedOn TINYINT
	DECLARE @bitCalAmtBasedonDepItems BIT
	SELECT @numCostDomain = numCost FROM Domain WHERE numDomainId = @numDomainID
	SELECT 
		@numItemClassification=ISNULL(numItemClassification,0)
		,@tintKitAssemblyPriceBasedOn=ISNULL(tintKitAssemblyPriceBasedOn,1)
		,@bitCalAmtBasedonDepItems=ISNULL(bitCalAmtBasedonDepItems,0) 
	FROM 
		Item 
	WHERE 
		numItemCode=@numItemCode

	IF ISNULL(@numItemClassification,0) > 0 AND EXISTS (SELECT numApprovalRuleID FROM ApprovalProcessItemsClassification WHERE numDomainID=@numDomainID AND numListItemID=@numItemClassification)
	BEGIN
		SELECT
			@numAbovePercent = ISNULL(numAbovePercent,0)
			,@numBelowPercent = ISNULL(numBelowPercent,0)
			,@numBelowField = ISNULL(numBelowPriceField,0)
			,@numAboveField=ISNULL(@numCostDomain,1)
			,@bitCostApproval=ISNULL(bitCostApproval,0)
			,@bitListPriceApproval=ISNULL(bitListPriceApproval,0)
		FROM
			ApprovalProcessItemsClassification
		WHERE
			numDomainId=@numDomainID
			AND ISNULL(numListItemID,0)=@numItemClassification
	END
	ELSE
	BEGIN
		SELECT
			@numAbovePercent = ISNULL(numAbovePercent,0)
			,@numBelowPercent = ISNULL(numBelowPercent,0)
			,@numBelowField = ISNULL(numBelowPriceField,0)
			,@numAboveField=ISNULL(@numCostDomain,1)
			,@bitCostApproval=ISNULL(bitCostApproval,0)
			,@bitListPriceApproval=ISNULL(bitListPriceApproval,0)
		FROM
			ApprovalProcessItemsClassification
		WHERE
			numDomainId=@numDomainID
			AND ISNULL(numListItemID,0)=0
	END
	/*SELECT
		@numAbovePercent = ISNULL(numAbovePercent,0)
		,@numBelowPercent = ISNULL(numBelowPercent,0)
		,@numBelowField = ISNULL(numBelowPriceField,0)
		,@numAboveField=ISNULL(numCost,1)
		,@bitCostApproval=ISNULL(bitCostApproval,0)
		,@bitListPriceApproval=ISNULL(bitListPriceApproval,0)
	FROM
		Domain
	WHERE
		numDomainId=@numDomainID*/


	DECLARE @IsApprovalRequired BIT
	DECLARE @ItemAbovePrice FLOAT
	DECLARE @ItemBelowPrice FLOAT

	SET @IsApprovalRequired = 0
	SET @ItemAbovePrice = 0
	SET @ItemBelowPrice = 0

	IF @numQuantity > 0
		SET @numUnitPrice = (@numTotalAmount / @numQuantity)
	ELSE
		SET @numUnitPrice = 0

	DECLARE @TEMPPrice TABLE
	(
		bitSuccess BIT
		,monPrice DECIMAL(20,5)
	)


	IF @numAboveField > 0 AND ISNULL(@bitCostApproval,0) = 1
	BEGIN
		IF (SELECT COUNT(*) FROM Item WHERE numDomainID = @numDomainID AND numItemCode = @numItemCode  AND (bitKitParent = 1 OR bitAssembly = 1)) > 0 AND @bitCalAmtBasedonDepItems=1
		BEGIN
			DELETE FROM @TEMPPrice

			INSERT INTO @TEMPPrice
			(
				bitSuccess
				,monPrice
			)
			SELECT
				bitSuccess
				,monPrice
			FROM
				dbo.fn_GetKitAssemblyCalculatedPrice(@numDomainID,@numDivisionID,@numItemCode,@numQuantity,@numWarehouseItemID,@tintKitAssemblyPriceBasedOn,0,0,'')

			IF (SELECT bitSuccess FROM @TEMPPrice) = 1
			BEGIN
				SET @ItemAbovePrice = (SELECT monPrice FROM @TEMPPrice)
			END
			ELSE
			BEGIN
				RAISERROR('SELECTED_WAREHOUSE_IS_NOT_AVAILABLE_FOR_ALL_CHILD_ITEMS',16,1)
			END
		END
		ELSE
		BEGIN
			IF @numAboveField = 3 -- Primaty Vendor Cost
				SELECT @ItemAbovePrice = ISNULL(monCost,0) FROM Vendor WHERE numItemCode = @numItemCode AND numVendorID = (SELECT numVendorID FROM Item WHERE numItemCode = @numItemCode)
			ELSE IF @numAboveField = 2 -- Product & Sevices Cost
				SELECT @ItemAbovePrice = ISNULL(@numCost,0)
			ELSE IF @numAboveField = 1 -- Average Cost
				SELECT @ItemAbovePrice = (CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(monAverageCost,0) END) FROM Item WHERE numDomainID = @numDomainID AND numItemCode = @numItemCode
		END

		IF @ItemAbovePrice > 0
		BEGIN
			IF @numUnitPrice < (@ItemAbovePrice + (@ItemAbovePrice * (@numAbovePercent/100)))
				SET @IsApprovalRequired = 1
		END
		ELSE IF @ItemAbovePrice = 0
		BEGIN
			SET @IsApprovalRequired = 1
		END
	END

	IF @IsApprovalRequired = 0 AND @numBelowField > 0  AND ISNULL(@bitListPriceApproval,0) = 1
	BEGIN
		IF @numBelowField = 1 -- List Price
		BEGIN
			IF (SELECT ISNULL(charItemType,'') AS charItemType FROM Item WHERE numDomainID = @numDomainID AND numItemCode = @numItemCode) = 'P' -- Inventory Item
				IF (SELECT COUNT(*) FROM Item WHERE numDomainID = @numDomainID AND numItemCode = @numItemCode  AND (bitKitParent = 1 OR bitAssembly = 1)) > 0 AND @bitCalAmtBasedonDepItems=1
				BEGIN
					DELETE FROM @TEMPPrice

					INSERT INTO @TEMPPrice
					(
						bitSuccess
						,monPrice
					)
					SELECT
						bitSuccess
						,monPrice
					FROM
						dbo.fn_GetKitAssemblyCalculatedPrice(@numDomainID,@numDivisionID,@numItemCode,@numQuantity,@numWarehouseItemID,@tintKitAssemblyPriceBasedOn,0,0,'')

					IF (SELECT bitSuccess FROM @TEMPPrice) = 1
					BEGIN
						SET @ItemBelowPrice = (SELECT monPrice FROM @TEMPPrice)
					END
					ELSE
					BEGIN
						RAISERROR('SELECTED_WAREHOUSE_IS_NOT_AVAILABLE_FOR_ALL_CHILD_ITEMS',16,1)
					END
				END
				ELSE
				BEGIN
					SELECT @ItemBelowPrice = ISNULL(monWListPrice,0) FROM WareHouseItems WHERE numDomainID = @numDomainID AND numItemID = @numItemCode AND numWareHouseItemID = @numWarehouseItemID
				END
			ELSE
				SELECT @ItemBelowPrice = ISNULL(monListPrice,0) FROM Item WHERE numDomainID = @numDomainID AND numItemCode = @numItemCode
		END
		ELSE IF @numBelowField = 2 -- Price Rule
		BEGIN
			/* Check if valid price book rules exists for sales in domain */
			IF (SELECT COUNT(*) FROM PriceBookRules WHERE numDomainId = @numDomainID AND tintRuleFor = 1 AND tintStep2 > 0 AND tintStep3 > 0) > 0
			BEGIN
				DECLARE @i INT = 0
				DECLARE @Count int = 0
				DECLARE @tempPriority INT
				DECLARE @tempNumPriceRuleID NUMERIC(18,0)
				DECLARE @numPriceRuleIDApplied NUMERIC(18,0) = 0
				DECLARE @TempTable TABLE (numID INT, numRuleID numeric(18,0), numPriority INT)

				INSERT INTO 
					@TempTable
				SELECT 
					ROW_NUMBER() OVER (ORDER BY PriceBookPriorities.Priority) AS id,
					numPricRuleID,
					PriceBookPriorities.Priority 
				FROM 
					PriceBookRules
				INNER JOIN
					PriceBookPriorities
				ON
					PriceBookRules.tintStep2 = PriceBookPriorities.Step2Value AND
					PriceBookRules.tintStep3 = PriceBookPriorities.Step3Value
				WHERE 
					PriceBookRules.numDomainId = @numDomainID AND 
					PriceBookRules.tintRuleFor = 1 AND 
					PriceBookRules.tintStep2 > 0 AND PriceBookRules.tintStep3 > 0
				ORDER BY
					PriceBookPriorities.Priority

				SELECT @Count = COUNT(*) FROM @TempTable

				/* Loop all price rule with priority */
				WHILE (@i < @count)
				BEGIN
					SELECT @tempNumPriceRuleID = numRuleID, @tempPriority = numPriority FROM @TempTable WHERE numID = (@i + 1)
					
					/* IF proprity is 9 then price rule is applied to all items and all customers. 
					So price rule must be applied to item or not.*/
					IF @tempPriority = 9
					BEGIN
						SET @numPriceRuleIDApplied = @tempNumPriceRuleID
						BREAK
					END
					/* Check if current item exists in rule. if eixts then exit loop with rule id else continie loop. if item not exist in any rule then nothing to check */
					ELSE
					BEGIN
						DECLARE @isRuleApplicable BIT = 0
						EXEC @isRuleApplicable = dbo.CheckIfPriceRuleApplicableToItem @numRuleID = @tempNumPriceRuleID, @numItemID = @numItemCode, @numDivisionID = @numDivisionID 

						IF @isRuleApplicable = 1
						BEGIN
							SET @numPriceRuleIDApplied = @tempNumPriceRuleID
							BREAK
						END
					END

					SET @i = @i + 1
				END

				/* If @numPriceRuleIDApplied > 0 Get final unit price limit after applying below rule */
				PRINT @numPriceRuleIDApplied
				IF @numPriceRuleIDApplied > 0
				BEGIN
					EXEC @ItemBelowPrice = GetUnitPriceAfterPriceRuleApplication @numRuleID = @numPriceRuleIDApplied, @numDomainID = @numDomainID, @numItemCode = @numItemCode, @numQuantity = @numQuantity, @numWarehouseItemID = @numWarehouseItemID, @numDivisionID = @numDivisionID
				END
			END
		END
		ELSE IF @numBelowField = 3 -- Price Level
		BEGIN
			EXEC @ItemBelowPrice = GetUnitPriceAfterPriceLevelApplication @numDomainID = @numDomainID, @numItemCode = @numItemCode, @numQuantity = @numQuantity, @numWarehouseItemID = @numWarehouseItemID, @isPriceRule = 0, @numPriceRuleID = 0, @numDivisionID = @numDivisionID
		END
		
		IF @numUnitPrice < (@ItemBelowPrice - (@ItemBelowPrice * (@numBelowPercent/100)))
				SET @IsApprovalRequired = 1
	END

	SELECT @IsApprovalRequired
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS (SELECT * FROM sysobjects WHERE xtype = 'p' AND NAME = 'USP_PromotionOffer_ApplyItemPromotionToECommerce') 
DROP PROCEDURE USP_PromotionOffer_ApplyItemPromotionToECommerce
GO
CREATE  PROCEDURE [dbo].[USP_PromotionOffer_ApplyItemPromotionToECommerce]
(
	@numDomainID NUMERIC(18,0),
	@numUserCntId NUMERIC(18,0),
	@numSiteID NUMERIC(18,0),
	@vcCookieId VARCHAR(100),
	@bitBasedOnDiscountCode BIT,
	@vcDiscountCode VARCHAR(100)	
)
AS
BEGIN
	DECLARE @numDivisionID NUMERIC(18,0)
	DECLARE @numRelationship NUMERIC(18,0)
	DECLARE @numProfile NUMERIC(18,0)

	SET @numDivisionID = ISNULL((SELECT numDivisionId FROM AdditionalContactsInformation WHERE numContactId=@numUserCntId),0)

	IF @numDivisionID > 0
	BEGIN
		SELECT 
			@numRelationship=numCompanyType,
			@numProfile=vcProfile
		FROM 
			DivisionMaster D                  
		JOIN 
			CompanyInfo C 
		ON 
			C.numCompanyId=D.numCompanyID                  
		WHERE 
			numDivisionID =@numDivisionID
	END
	ELSE IF ISNULL(@numSiteID,0) > 0
	BEGIN
		SELECT
			@numRelationship=ISNULL(numRelationshipId,0),
			@numProfile=ISNULL(numProfileId,0)
		FROM
			eCommerceDTL
		WHERE
			numSiteId=@numSiteID
	END

	DECLARE @TEMP TABLE
	(
		ID INT IDENTITY(1,1)
		,numCartID NUMERIC(18,0)
        ,numItemCode NUMERIC(18,0)
		,numItemClassification NUMERIC(18,0)
        ,numUnits FLOAT
		,numWarehouseItemID NUMERIC(18,0)
        ,monUnitPrice DECIMAL(20,5)
        ,fltDiscount DECIMAL(20,5)
        ,bitDiscountType BIT
		,monInsertUnitPrice DECIMAL(20,5)
        ,fltInsertDiscount DECIMAL(20,5)
        ,bitInsertDiscountType BIT
        ,monTotalAmount DECIMAL(20,5)
        ,numPromotionID NUMERIC(18,0)
        ,bitPromotionTriggered BIT
        ,vcPromotionDescription VARCHAR(MAX)
		,bitPromotionDiscount BIT
		,vcKitChildItems VARCHAR(MAX)
		,numSelectedPromotionID NUMERIC(18,0)
		,bitChanged BIT		
	)

	INSERT INTO @TEMP
	(
		numCartID
        ,numItemCode
		,numItemClassification
        ,numUnits
		,numWarehouseItemID
        ,monUnitPrice
        ,fltDiscount
        ,bitDiscountType
		,monInsertUnitPrice
        ,fltInsertDiscount
        ,bitInsertDiscountType
        ,monTotalAmount
        ,numPromotionID
        ,bitPromotionTriggered
		,bitPromotionDiscount
		,vcKitChildItems
		,numSelectedPromotionID
		,bitChanged
	)
	SELECT
		numCartID
        ,CI.numItemCode
		,ISNULL(I.numItemClassification,0)
        ,numUnitHour * ISNULL(decUOMConversionFactor,1)
		,numWarehouseItmsID
        ,monPrice
        ,fltDiscount
        ,bitDiscountType
		,monInsertPrice
		,fltInsertDiscount
		,bitInsertDiscountType
        ,monTotAmount
        ,PromotionID
        ,bitPromotionTrigerred
		,bitPromotionDiscount
		,vcChildKitItemSelection
		,numPreferredPromotionID
		,0
	FROM
		CartItems CI
	INNER JOIN
		Item I
	ON
		CI.numItemCode = I.numItemCode
	WHERE
		CI.numDomainId=@numDomainId
		AND vcCookieId=@vcCookieId  
		AND numUserCntId=@numUserCntId 

	DECLARE @i INT = 1
	DECLARE @iCount INT 
	SET @iCount = (SELECT COUNT(*) FROM @TEMP)

	DECLARE @l INT = 1
	DECLARE @lCount INT
	SET @lCount = (SELECT COUNT(*) FROM @TEMP)

	DECLARE @numTempPromotionID NUMERIC(18,0)
	DECLARE @tintOfferTriggerValueType TINYINT
	DECLARE @tintOfferBasedOn TINYINT
	DECLARE @tintOfferTriggerValueTypeRange TINYINT
	DECLARE @fltOfferTriggerValue FLOAT
	DECLARE @fltOfferTriggerValueRange FLOAT
	DECLARE @vcShortDesc VARCHAR(500)
	DECLARE @tintItemCalDiscount TINYINT
	DECLARE @monDiscountedItemPrice DECIMAL(20,5)
	DECLARE @numItemCode NUMERIC(18,0)
	DECLARE @numTempWarehouseItemID NUMERIC(18,0)
	DECLARE @numUnits FLOAT
	DECLARE @monTotalAmount DECIMAL(20,5)

	DECLARE @bitRemainingCheckRquired AS BIT
	DECLARE @numRemainingPromotion FLOAT
	DECLARE @fltDiscountValue FLOAT
	DECLARE @tintDiscountType TINYINT
	DECLARE @tintDiscoutBaseOn TINYINT

	DECLARE @TEMPUsedPromotion TABLE
	(
		ID INT
		,numPromotionID NUMERIC(18,0)
		,numUnits FLOAT
		,monTotalAmount DECIMAL(20,5)
	)

	INSERT INTO @TEMPUsedPromotion
	(
		ID
		,numPromotionID
		,numUnits
		,monTotalAmount
	)
	SELECT
		ROW_NUMBER() OVER(ORDER BY T1.numPromotionID ASC)
		,numPromotionID
		,SUM(numUnits)
		,SUM(monTotalAmount)
	FROM
		@TEMP T1
	WHERE
		ISNULL(numPromotionID,0) > 0
		AND ISNULL(bitPromotionTriggered,0) = 1
	GROUP BY
		numPromotionID


	DECLARE @j INT = 1
	DECLARE @jCount INT
	SET @jCount = ISNULL((SELECT COUNT(*) FROM @TEMPUsedPromotion),0)

	-- FIRST REMOVE ALL PROMOTIONS WHERE ITEM WHICH TRIGERRED PROMOTION IS DELETED
	UPDATE 
		T1
	SET 
		numPromotionID=0
		,bitPromotionTriggered=0
		,bitPromotionDiscount=0
		,vcPromotionDescription=''
		,bitChanged=1
		,monUnitPrice=ISNULL(T1.monInsertUnitPrice,0)
		,fltDiscount=ISNULL(T1.fltInsertDiscount,0)
		,bitDiscountType=ISNULL(T1.bitInsertDiscountType,0)
	FROM
		@TEMP T1
	WHERE 
		numPromotionID > 0
		AND ISNULL(bitPromotionTriggered,0) = 0
		AND (SELECT COUNT(*) FROM @TEMP T2 WHERE T2.numPromotionID=T1.numPromotionID AND ISNULL(T2.bitPromotionTriggered,0) = 1) = 0

	-- FIRST REMOVE APPLIED PROMOTION IF THEY ARE NOT VALIES
	WHILE @j <= @jCount
	BEGIN
		SELECT 
			@numTempPromotionID=numPromotionID
			,@numUnits = numUnits
			,@monTotalAmount=monTotalAmount
		FROM 
			@TEMPUsedPromotion 
		WHERE 
			ID = @j

		IF NOT EXISTS (SELECT 
						PO.numProId 
					FROM 
						PromotionOffer PO
					LEFT JOIN	
						PromotionOffer POOrder
					ON
						PO.numOrderPromotionID = POOrder.numProId
					LEFT JOIN	
						DiscountCodes DC
					ON
						PO.numOrderPromotionID = DC.numPromotionID
					WHERE 
						PO.numDomainId=@numDomainID 
						AND PO.numProId=@numTempPromotionID
						AND ISNULL(PO.bitEnabled,0)=1 
						AND ISNULL(PO.IsOrderBasedPromotion,0) = 0
						AND 1 = (CASE 
									WHEN ISNULL(PO.numOrderPromotionID,0) > 0 
									THEN (CASE WHEN POOrder.bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN GETUTCDATE()>=POOrder.dtValidFrom AND GETUTCDATE()<=POOrder.dtValidTo THEN 1 ELSE 0 END) END)
									ELSE (CASE WHEN PO.bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN GETUTCDATE()>=PO.dtValidFrom AND GETUTCDATE()<=PO.dtValidTo THEN 1 ELSE 0 END) END)
								END)
						AND 1 = (CASE 
									WHEN ISNULL(PO.numOrderPromotionID,0) > 0 
									THEN
										(CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=PO.numOrderPromotionID AND tintType=2 AND numRelationship=@numRelationship AND numProfile=@numProfile) > 0 THEN 1 ELSE 0 END)
									ELSE
										(CASE PO.tintCustomersBasedOn 
											WHEN 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=PO.numProId AND tintType=1 AND numDivisionID=@numDivisionID) > 0 THEN 1 ELSE 0 END)
											WHEN 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=PO.numProId AND tintType=2 AND numRelationship=@numRelationship AND numProfile=@numProfile) > 0 THEN 1 ELSE 0 END)
											WHEN 3 THEN 1
											ELSE 0
										END)
								END)
						AND 1 = (CASE 
									WHEN ISNULL(PO.tintOfferTriggerValueType,1) = 1 --Based on quantity
									THEN
										CASE
											WHEN ISNULL(PO.tintOfferTriggerValueTypeRange,1) = 2
											THEN
												(CASE WHEN ISNULL(@numUnits,0) BETWEEN ISNULL(PO.fltOfferTriggerValue,0) AND ISNULL(PO.fltOfferTriggerValueRange,0) THEN 1 ELSE 0 END)
											ELSE
												(CASE WHEN ISNULL(@numUnits,0) >= PO.fltOfferTriggerValue THEN 1 ELSE 0 END)
										END
									WHEN ISNULL(PO.tintOfferTriggerValueType,1) = 2 --Based on amount
									THEN
										CASE
											WHEN ISNULL(PO.tintOfferTriggerValueTypeRange,1) = 2
											THEN
												(CASE WHEN ISNULL(@monTotalAmount,0) BETWEEN ISNULL(PO.fltOfferTriggerValue,0) AND ISNULL(PO.fltOfferTriggerValueRange,0) THEN 1 ELSE 0 END)
											ELSE
												(CASE WHEN ISNULL(@monTotalAmount,0) >= PO.fltOfferTriggerValue THEN 1 ELSE 0 END)
										END
								END)
						AND 1 = (CASE 
									WHEN ISNULL(PO.numOrderPromotionID,0) > 0 
									THEN 
										(CASE WHEN ISNULL(DC.CodeUsageLimit,0) <> 0 THEN (CASE WHEN ISNULL((SELECT intCodeUsed FROM DiscountCodeUsage WHERE numDiscountId=DC.numDiscountId AND numDivisionId=@numDivisionId),0) >= DC.CodeUsageLimit THEN 0 ELSE 1 END) ELSE 1 END)
									ELSE 1 
								END)
				)
		BEGIN
			-- IF Promotion is not valid than revert line item price to based on default pricing
			UPDATE 
				T1
			SET 
				numPromotionID=0
				,bitPromotionTriggered=0
				,bitPromotionDiscount=0
				,vcPromotionDescription=''
				,bitChanged=1
				,monUnitPrice=ISNULL(T1.monInsertUnitPrice,0)
				,fltDiscount=ISNULL(T1.fltInsertDiscount,0)
				,bitDiscountType=ISNULL(T1.bitInsertDiscountType,0)
			FROM
				@TEMP T1
			WHERE 
				numPromotionID=@numTempPromotionID
		END

		SET @j = @j + 1
	END

	UPDATE
		T1
	SET 
		T1.bitPromotionDiscount = 0
		,T1.bitChanged=1
		,T1.monUnitPrice=ISNULL(T2.monPrice,0)
		,T1.fltDiscount=ISNULL(T2.decDiscount,0)
		,T1.bitDiscountType=ISNULL(T2.tintDisountType,0)
	FROM
		@TEMP T1
	INNER JOIN
		PromotionOffer PO
	ON
		T1.numPromotionID=PO.numProId
	CROSS APPLY
		dbo.fn_GetItemPriceBasedOnMethod(@numDomainID,@numDivisionID,T1.numItemCode,T1.numUnits,0,0,T1.numWarehouseItemID,T1.vcKitChildItems) T2
	WHERE
		ISNULL(numPromotionID,0) > 0
		AND ISNULL(bitPromotionDiscount,0) = 1
		AND ISNULL(PO.tintDiscountType,0) = 3

	IF ISNULL(@bitBasedOnDiscountCode,0) = 1
	BEGIN
		DECLARE @TableItemCouponPromotion TABLE
		(
			ID INT IDENTITY(1,1)
			,numPromotionID NUMERIC(18,0)
			,tintOfferTriggerValueType TINYINT
			,tintOfferTriggerValueTypeRange TINYINT
			,fltOfferTriggerValue FLOAT
			,fltOfferTriggerValueRange FLOAT
			,tintOfferBasedOn TINYINT
			,vcShortDesc VARCHAR(300)
		)

		INSERT INTO @TableItemCouponPromotion
		(
			numPromotionID
			,tintOfferTriggerValueType
			,tintOfferTriggerValueTypeRange
			,fltOfferTriggerValue
			,fltOfferTriggerValueRange
			,tintOfferBasedOn
			,vcShortDesc
		)
		SELECT
			PO.numProId
			,PO.tintOfferTriggerValueType
			,PO.tintOfferTriggerValueTypeRange
			,PO.fltOfferTriggerValue
			,PO.fltOfferTriggerValueRange
			,PO.tintOfferBasedOn
			,ISNULL(PO.vcShortDesc,'-')
		FROM
			PromotionOffer PO
		INNER JOIN
			PromotionOffer POOrder
		ON
			PO.numOrderPromotionID = POOrder.numProId
		INNER JOIN 
			DiscountCodes DC
		ON 
			POOrder.numProId = DC.numPromotionID
		WHERE
			PO.numDomainId = @numDomainID
			AND POOrder.numDomainId=@numDomainID 
			AND ISNULL(PO.bitEnabled,0)=1 
			AND ISNULL(POOrder.bitEnabled,0)=1 
			AND ISNULL(PO.bitUseOrderPromotion,0)=1 
			AND ISNULL(POOrder.IsOrderBasedPromotion,0) = 1
			AND ISNULL(POOrder.bitRequireCouponCode,0) = 1
			AND 1 = (CASE WHEN POOrder.bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN GETUTCDATE()>=POOrder.dtValidFrom AND GETUTCDATE()<=POOrder.dtValidTo THEN 1 ELSE 0 END) END)
			AND 1 = (CASE WHEN ISNULL(DC.CodeUsageLimit,0) <> 0 THEN (CASE WHEN ISNULL((SELECT intCodeUsed FROM DiscountCodeUsage WHERE numDiscountId=DC.numDiscountId AND numDivisionId=@numDivisionId),0) >= DC.CodeUsageLimit THEN 0 ELSE 1 END) ELSE 1 END)
			AND 1 = (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=POOrder.numProId AND tintType=2 AND numRelationship=@numRelationship AND numProfile=@numProfile) > 0 THEN 1 ELSE 0 END)
			AND DC.vcDiscountCode = @vcDiscountCode
		ORDER BY
			(CASE 
				WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 1 THEN 1
				WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 1 THEN 2
				WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 2 THEN 3
				WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 2 THEN 4
				WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 3 THEN 5
				WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 3 THEN 6
			END) ASC,ISNULL(PO.fltOfferTriggerValue,0) DESC
			
		SET @l = 1
		DECLARE @k INT = 1
		DECLARE @kCount INT 
		SET @kCount = (SELECT COUNT(*) FROM @TableItemCouponPromotion)

		WHILE @k <= @kCount
		BEGIN
			SELECT
				@numTempPromotionID=numPromotionID
				,@tintOfferTriggerValueType=tintOfferTriggerValueType
				,@tintOfferTriggerValueTypeRange = tintOfferTriggerValueTypeRange
				,@fltOfferTriggerValue=fltOfferTriggerValue
				,@fltOfferTriggerValueRange=fltOfferTriggerValueRange
				,@tintOfferBasedOn=tintOfferBasedOn
				,@vcShortDesc=vcShortDesc
			FROM
				@TableItemCouponPromotion
			WHERE
				ID=@k

			IF (SELECT COUNT(*) FROM @TEMP WHERE numPromotionID=@numTempPromotionID AND ISNULL(bitPromotionTriggered,0)=1) = 0
			BEGIN
				IF ISNULL((SELECT 
							(CASE WHEN @tintOfferTriggerValueType = 2 THEN SUM(ISNULL(monTotalAmount,0)) ELSE SUM(ISNULL(numUnits,0)) END)
						FROM
							@TEMP T1
						INNER JOIN
							Item I
						ON
							T1.numItemCode = I.numItemCode
						WHERE
							ISNULL(numPromotionID,0) = 0
							AND 1 = (CASE 
										WHEN @tintOfferBasedOn = 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=@numTempPromotionID AND tintRecordType=5 AND tintType=1 AND numValue=T1.numItemCode) > 0 THEN 1 ELSE 0 END)
										WHEN @tintOfferBasedOn = 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=@numTempPromotionID AND tintRecordType=5 AND tintType=2 AND numValue=ISNULL(I.numItemClassification,0)) > 0 THEN 1 ELSE 0 END)
										WHEN @tintOfferBasedOn = 4 THEN 1 
										ELSE 0
									END)
						),0) >= ISNULL(@fltOfferTriggerValue,0)
				BEGIN
					SET @i = 1

					WHILE @i <= @iCount
					BEGIN

						IF ISNULL((SELECT 
									(CASE WHEN @tintOfferTriggerValueType = 2 THEN SUM(ISNULL(monTotalAmount,0)) ELSE SUM(ISNULL(numUnits,0)) END)
								FROM
									@TEMP T1
								WHERE
									ISNULL(numPromotionID,0) = @numTempPromotionID
									AND ISNULL(bitPromotionTriggered,0) = 1
								),0) < ISNULL(@fltOfferTriggerValue,0)
						BEGIN
							UPDATE
								T1
							SET
								bitChanged = 1
								,numPromotionID=@numTempPromotionID
								,bitPromotionTriggered=1
								,vcPromotionDescription=ISNULL(@vcShortDesc,'')
							FROM
								@TEMP T1
							INNER JOIN
								Item I
							ON
								T1.numItemCode = I.numItemCode
							WHERE
								T1.ID = @i
								AND ISNULL(numPromotionID,0) = 0
								AND 1 = (CASE 
											WHEN ISNULL(@tintOfferTriggerValueType,1) = 1 --Based on quantity
											THEN
												CASE
													WHEN ISNULL(@tintOfferTriggerValueTypeRange,1) = 2
													THEN
														(CASE WHEN ISNULL(T1.numUnits,0) + ISNULL((SELECT SUM(ISNULL(numUnits,0)) FROM @TEMP WHERE ISNULL(numPromotionID,0) = @numTempPromotionID AND ISNULL(bitPromotionTriggered,0) = 1),0) BETWEEN ISNULL(@fltOfferTriggerValue,0) AND ISNULL(@fltOfferTriggerValueRange,0) THEN 1 ELSE 0 END)
													ELSE
														1
												END
											WHEN ISNULL(@tintOfferTriggerValueType,1) = 2 --Based on amount
											THEN
												CASE
													WHEN ISNULL(@tintOfferTriggerValueTypeRange,1) = 2
													THEN
														(CASE WHEN ISNULL(T1.monTotalAmount,0) + ISNULL((SELECT SUM(ISNULL(monTotalAmount,0)) FROM @TEMP WHERE ISNULL(numPromotionID,0) = @numTempPromotionID AND ISNULL(bitPromotionTriggered,0) = 1),0) BETWEEN ISNULL(@fltOfferTriggerValue,0) AND ISNULL(@fltOfferTriggerValueRange,0) THEN 1 ELSE 0 END)
													ELSE
														1
												END
										END)
								AND 1 = (CASE 
											WHEN @tintOfferBasedOn = 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=@numTempPromotionID AND tintRecordType=5 AND tintType=1 AND numValue=T1.numItemCode) > 0 THEN 1 ELSE 0 END)
											WHEN @tintOfferBasedOn = 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=@numTempPromotionID AND tintRecordType=5 AND tintType=2 AND numValue=ISNULL(I.numItemClassification,0)) > 0 THEN 1 ELSE 0 END)
											WHEN @tintOfferBasedOn = 4 THEN 1 
											ELSE 0
										END)
						END

						SET @i = @i + 1
					END

				END
			END

			SET @k = @k + 1
		END
	END

	-- TRIGGER PROMOTION FOR ITEM WHICH ARE ELIGIBLE
	SET @i = 1
	WHILE @i <= @iCount
	BEGIN
		SET @numTempPromotionID = NULL

		SELECT
			@numTempPromotionID=numProId
			,@tintOfferTriggerValueType=tintOfferTriggerValueType
			,@tintOfferTriggerValueTypeRange = tintOfferTriggerValueTypeRange
			,@fltOfferTriggerValue=fltOfferTriggerValue
			,@fltOfferTriggerValueRange=fltOfferTriggerValueRange
			,@tintOfferBasedOn=tintOfferBasedOn
			,@vcShortDesc=vcShortDesc
		FROM
			@TEMP T1
		INNER JOIN
			Item I
		ON
			T1.numItemCode=I.numItemCode
		CROSS APPLY
		(
			SELECT TOP 1
				PO.numProId
				,PO.tintOfferBasedOn
				,PO.tintOfferTriggerValueType
				,PO.tintOfferTriggerValueTypeRange
				,PO.fltOfferTriggerValue
				,PO.fltOfferTriggerValueRange
				,ISNULL(PO.vcShortDesc,'') vcShortDesc
			FROM 
				PromotionOffer PO
			WHERE
				PO.numDomainId=@numDomainID
				AND 1 = (CASE WHEN ISNULL(T1.numSelectedPromotionID,0) > 0 THEN (CASE WHEN PO.numProId=T1.numSelectedPromotionID THEN 1 ELSE 0 END) ELSE 1 END) 
				AND ISNULL(PO.bitEnabled,0)=1 
				AND ISNULL(PO.IsOrderBasedPromotion,0) = 0
				AND ISNULL(PO.bitUseOrderPromotion,0) = 0
				AND ISNULL(PO.bitRequireCouponCode,0) = 0
				AND 1 = (CASE WHEN PO.bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN GETUTCDATE()>=PO.dtValidFrom AND GETUTCDATE()<=PO.dtValidTo THEN 1 ELSE 0 END) END)
				AND 1 = (CASE 
							WHEN ISNULL(numOrderPromotionID,0) > 0
							THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=numOrderPromotionID AND tintType=2 AND numRelationship=@numRelationship AND numProfile=@numProfile) > 0 THEN 1 ELSE 0 END)
							ELSE
								(CASE tintCustomersBasedOn 
									WHEN 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=PO.numProId AND tintType=1 AND numDivisionID=@numDivisionID) > 0 THEN 1 ELSE 0 END)
									WHEN 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=PO.numProId AND tintType=2 AND numRelationship=@numRelationship AND numProfile=@numProfile) > 0 THEN 1 ELSE 0 END)
									WHEN 3 THEN 1
									ELSE 0
								END)
						END)
				AND 1 = (CASE 
							WHEN PO.tintOfferBasedOn = 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=1 AND numValue=T1.numItemCode) > 0 THEN 1 ELSE 0 END)
							WHEN PO.tintOfferBasedOn = 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=2 AND numValue=ISNULL(I.numItemClassification,0)) > 0 THEN 1 ELSE 0 END)
							WHEN PO.tintOfferBasedOn = 4 THEN 1 
							ELSE 0
						END)
			ORDER BY
				(CASE 
					WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 1 THEN 1
					WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 1 THEN 2
					WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 2 THEN 3
					WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 2 THEN 4
					WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 3 THEN 5
					WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 3 THEN 6
				END) ASC,ISNULL(PO.fltOfferTriggerValue,0) DESC

		) T2
		WHERE
			ISNULL(T1.numPromotionID,0)  = 0
			AND T1.ID = @i
			AND ISNULL((SELECT COUNT(*) FROM @TEMP T3 WHERE T3.numPromotionID=T2.numProId AND T3.bitPromotionTriggered=1),0) = 0

		IF ISNULL(@numTempPromotionID,0) > 0 AND (SELECT COUNT(*) FROM @TEMP WHERE numPromotionID=@numTempPromotionID AND ISNULL(bitPromotionTriggered,0)=1) = 0
		BEGIN
			IF ISNULL((SELECT 
							(CASE WHEN @tintOfferTriggerValueType = 2 THEN SUM(ISNULL(monTotalAmount,0)) ELSE SUM(ISNULL(numUnits,0)) END)
						FROM
							@TEMP T1
						INNER JOIN
							Item I
						ON
							T1.numItemCode = I.numItemCode
						WHERE
							ISNULL(numPromotionID,0) = 0
							AND 1 = (CASE 
										WHEN @tintOfferBasedOn = 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=@numTempPromotionID AND tintRecordType=5 AND tintType=1 AND numValue=T1.numItemCode) > 0 THEN 1 ELSE 0 END)
										WHEN @tintOfferBasedOn = 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=@numTempPromotionID AND tintRecordType=5 AND tintType=2 AND numValue=ISNULL(I.numItemClassification,0)) > 0 THEN 1 ELSE 0 END)
										WHEN @tintOfferBasedOn = 4 THEN 1 
										ELSE 0
									END)
						),0) >= ISNULL(@fltOfferTriggerValue,0)
			BEGIN
				SET @l = 1

				WHILE @l <= @lCount
				BEGIN
						
					IF ISNULL((SELECT 
									(CASE WHEN @tintOfferTriggerValueType = 2 THEN SUM(ISNULL(monTotalAmount,0)) ELSE SUM(ISNULL(numUnits,0)) END)
								FROM
									@TEMP T1
								WHERE
									ISNULL(numPromotionID,0) = @numTempPromotionID
									AND ISNULL(bitPromotionTriggered,0) = 1
								),0) < ISNULL(@fltOfferTriggerValue,0)
					BEGIN
						UPDATE
							T1
						SET
							bitChanged = 1
							,numPromotionID=@numTempPromotionID
							,bitPromotionTriggered=1
							,vcPromotionDescription=ISNULL(@vcShortDesc,'')
						FROM
							@TEMP T1
						INNER JOIN
							Item I
						ON
							T1.numItemCode = I.numItemCode
						WHERE
							T1.ID = @l
							AND ISNULL(numPromotionID,0) = 0
							AND 1 = (CASE 
										WHEN ISNULL(@tintOfferTriggerValueType,1) = 1 --Based on quantity
										THEN
											CASE
												WHEN ISNULL(@tintOfferTriggerValueTypeRange,1) = 2
												THEN
													(CASE WHEN ISNULL(T1.numUnits,0) + ISNULL((SELECT SUM(ISNULL(numUnits,0)) FROM @TEMP WHERE ISNULL(numPromotionID,0) = @numTempPromotionID AND ISNULL(bitPromotionTriggered,0) = 1),0) BETWEEN ISNULL(@fltOfferTriggerValue,0) AND ISNULL(@fltOfferTriggerValueRange,0) THEN 1 ELSE 0 END)
												ELSE
													1
											END
										WHEN ISNULL(@tintOfferTriggerValueType,1) = 2 --Based on amount
										THEN
											CASE
												WHEN ISNULL(@tintOfferTriggerValueTypeRange,1) = 2
												THEN
													(CASE WHEN ISNULL(T1.monTotalAmount,0) + ISNULL((SELECT SUM(ISNULL(monTotalAmount,0)) FROM @TEMP WHERE ISNULL(numPromotionID,0) = @numTempPromotionID AND ISNULL(bitPromotionTriggered,0) = 1),0) BETWEEN ISNULL(@fltOfferTriggerValue,0) AND ISNULL(@fltOfferTriggerValueRange,0) THEN 1 ELSE 0 END)
												ELSE
													1
											END
									END)
							AND 1 = (CASE 
										WHEN @tintOfferBasedOn = 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=@numTempPromotionID AND tintRecordType=5 AND tintType=1 AND numValue=T1.numItemCode) > 0 THEN 1 ELSE 0 END)
										WHEN @tintOfferBasedOn = 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=@numTempPromotionID AND tintRecordType=5 AND tintType=2 AND numValue=ISNULL(I.numItemClassification,0)) > 0 THEN 1 ELSE 0 END)
										WHEN @tintOfferBasedOn = 4 THEN 1 
										ELSE 0
									END)
					END

					SET @l = @l + 1
				END

			END
		END

		SET @i = @i + 1
	END
		

	DECLARE @TEMPPromotion TABLE
	(
		ID INT
		,numPromotionID NUMERIC(18,0)
		,vcShortDesc VARCHAR(500)
		,numTriggerItemCode NUMERIC(18,0)
		,numWarehouseItemID NUMERIC(18,0)
		,numUnits FLOAT
		,monTotalAmount DECIMAL(20,5)
	)

	INSERT INTO @TEMPPromotion
	(
		ID
		,numPromotionID
		,numTriggerItemCode
		,numWarehouseItemID
		,numUnits
		,monTotalAmount
	)
	SELECT
		ROW_NUMBER() OVER(ORDER BY T1.numCartID ASC)
		,numPromotionID
		,numItemCode
		,numWarehouseItemID
		,numUnits
		,monTotalAmount
	FROM
		@TEMP T1
	INNER JOIN
		PromotionOffer PO
	ON
		T1.numPromotionID = PO.numProId
	WHERE
		ISNULL(numPromotionID,0) > 0
		AND ISNULL(bitPromotionTriggered,0) = 1
		AND 1 = (CASE WHEN @bitBasedOnDiscountCode=0 THEN (CASE WHEN ISNULL(PO.numOrderPromotionID,0) = 0 THEN 1 ELSE 0 END) ELSE 1 END)

	SET @j = 1
	SET @jCount = ISNULL((SELECT COUNT(*) FROM @TEMPPromotion),0)

	WHILE @j <= @jCount
	BEGIN
		SELECT 
			@numTempPromotionID=numPromotionID
			,@numItemCode=numTriggerItemCode
			,@numTempWarehouseItemID=numWarehouseItemID
			,@numUnits = numUnits
			,@monTotalAmount=monTotalAmount
		FROM 
			@TEMPPromotion 
		WHERE 
			ID = @j

			
		SELECT
			@fltDiscountValue=ISNULL(fltDiscountValue,0)
			,@tintDiscountType=ISNULL(tintDiscountType,0)
			,@tintDiscoutBaseOn=tintDiscoutBaseOn
			,@vcShortDesc=ISNULL(vcShortDesc,'')
			,@tintItemCalDiscount=ISNULL(tintItemCalDiscount,1)
			,@monDiscountedItemPrice=ISNULL(monDiscountedItemPrice,0)
		FROM
			PromotionOffer
		WHERE
			numProId=@numTempPromotionID

		-- If discount is based on amount or quantity than we have to checked remaining amount or qty left to give as discount 
		IF @tintDiscountType = 2 OR @tintDiscountType = 3
		BEGIN
			SET @bitRemainingCheckRquired = 1
		END
		ELSE
		BEGIN
			SET @bitRemainingCheckRquired = 0
			SET @numRemainingPromotion = 0
		END

		-- If promotion is valid than check whether any item left to apply promotion
		SET @i = 1
		WHILE @i <= @iCount
		BEGIN
			IF @bitRemainingCheckRquired=1
			BEGIN
				IF @tintDiscountType = 2 -- Discount by amount
				BEGIN
					SET @numRemainingPromotion = @fltDiscountValue - ISNULL((SELECT SUM(fltDiscount) FROM @TEMP WHERE numPromotionID = @numTempPromotionID	AND ISNULL(bitDiscountType,0)=1 AND bitPromotionDiscount = 1),0)
				END
				ELSE IF @tintDiscountType = 3 -- Discount by quantity
				BEGIN
					IF @tintDiscoutBaseOn IN (5,6)
					BEGIN
						SET @numRemainingPromotion = @fltDiscountValue - ISNULL((SELECT SUM(numUnits) FROM @TEMP WHERE numPromotionID = @numTempPromotionID	AND ISNULL(bitDiscountType,0)=1 AND bitPromotionDiscount = 1),0)
					END
					ELSE
					BEGIN
						SET @numRemainingPromotion = @fltDiscountValue - ISNULL((SELECT SUM(fltDiscount/monUnitPrice) FROM @TEMP WHERE numPromotionID = @numTempPromotionID	AND ISNULL(bitDiscountType,0)=1 AND bitPromotionDiscount = 1),0)
					END
				END
			END

			IF @bitRemainingCheckRquired=0 OR (@bitRemainingCheckRquired=1 AND @numRemainingPromotion > 0)
			BEGIN
				UPDATE
					T1
				SET
					monUnitPrice = (CASE WHEN @tintItemCalDiscount = 1 THEN ISNULL(T1.monInsertUnitPrice,0) ELSE ISNULL(I.monListPrice,0) END)
					,fltDiscount = (CASE 
										WHEN @bitRemainingCheckRquired=0 
										THEN 
											@fltDiscountValue
										ELSE 
											(CASE 
												WHEN @tintDiscountType = 2 -- Discount by amount
												THEN
													(CASE 
														WHEN T1.numUnits >= @numRemainingPromotion 
														THEN @numRemainingPromotion 
														ELSE T1.numUnits 
													END) * (CASE 
																WHEN (CASE WHEN @tintItemCalDiscount = 1 THEN ISNULL(T1.monInsertUnitPrice,0) ELSE ISNULL(I.monListPrice,0) END) > @monDiscountedItemPrice
																THEN ((CASE WHEN @tintItemCalDiscount = 1 THEN ISNULL(T1.monInsertUnitPrice,0) ELSE ISNULL(I.monListPrice,0) END) - @monDiscountedItemPrice)
																ELSE 
																	(CASE WHEN @tintItemCalDiscount = 1 THEN ISNULL(T1.monInsertUnitPrice,0) ELSE ISNULL(I.monListPrice,0) END) 
															END)
												WHEN @tintDiscountType = 3 -- Discount by quantity
												THEN
													(CASE 
														WHEN @tintDiscoutBaseOn IN (5,6)
														THEN
															(T1.numUnits * (CASE 
																				WHEN @tintItemCalDiscount = 1 
																				THEN ISNULL(T1.monInsertUnitPrice,0) 
																				ELSE ISNULL(I.monListPrice,0) 
																			END)) - (CASE 
																						WHEN T1.numUnits >= @numRemainingPromotion 
																						THEN @numRemainingPromotion 
																						ELSE T1.numUnits 
																					END) * (CASE 
																								WHEN (CASE WHEN @tintItemCalDiscount = 1 THEN ISNULL(T1.monInsertUnitPrice,0) ELSE ISNULL(I.monListPrice,0) END) > @monDiscountedItemPrice
																								THEN ((CASE WHEN @tintItemCalDiscount = 1 THEN ISNULL(T1.monInsertUnitPrice,0) ELSE ISNULL(I.monListPrice,0) END) - @monDiscountedItemPrice)
																								ELSE 
																									(CASE WHEN @tintItemCalDiscount = 1 THEN ISNULL(T1.monInsertUnitPrice,0) ELSE ISNULL(I.monListPrice,0) END) 
																							END)
														ELSE
															(CASE 
																WHEN T1.numUnits >= @numRemainingPromotion 
																THEN (@numRemainingPromotion * (CASE 
																									WHEN (ISNULL(bitKitParent,0)=1 OR ISNULL(bitAssembly,0) = 1) AND ISNULL(bitCalAmtBasedonDepItems,0) = 1
																									THEN dbo.GetCalculatedPriceForKitAssembly(@numDomainID,@numDivisionID,I.numItemCode,T1.numUnits,@numTempWarehouseItemID,ISNULL(tintKitAssemblyPriceBasedOn,1),0,0,ISNULL(T1.vcKitChildItems,''))
																									ELSE (CASE WHEN @tintItemCalDiscount = 1 THEN ISNULL(T1.monInsertUnitPrice,0) ELSE ISNULL(I.monListPrice,0) END)
																								END)) 
																ELSE (T1.numUnits * (CASE 
																						WHEN (ISNULL(bitKitParent,0)=1 OR ISNULL(bitAssembly,0) = 1) AND ISNULL(bitCalAmtBasedonDepItems,0) = 1
																						THEN dbo.GetCalculatedPriceForKitAssembly(@numDomainID,@numDivisionID,I.numItemCode,T1.numUnits,@numTempWarehouseItemID,ISNULL(tintKitAssemblyPriceBasedOn,1),0,0,ISNULL(T1.vcKitChildItems,''))
																						ELSE (CASE WHEN @tintItemCalDiscount = 1 THEN ISNULL(T1.monInsertUnitPrice,0) ELSE ISNULL(I.monListPrice,0) END)
																					END)) 
															END)
													END)
												ELSE 0
											END)
										END
									)
					,bitDiscountType = (CASE WHEN @bitRemainingCheckRquired=0 THEN 0 ELSE 1 END)
					,numPromotionID=@numTempPromotionID
					,bitPromotionTriggered=(CASE WHEN bitPromotionTriggered=1 THEN 1 ELSE 0 END)
					,vcPromotionDescription=@vcShortDesc
					,bitChanged=1
					,bitPromotionDiscount=1
				FROM
					@TEMP T1
				INNER JOIN
					Item I
				ON
					T1.numItemCode = I.numItemCode
				WHERE
					ID=@i
					AND (ISNULL(T1.numPromotionID,0) = 0 OR (ISNULL(T1.numPromotionID,0) = @numTempPromotionID AND ISNULL(bitPromotionDiscount,0)=0))
					AND 1 = (CASE 
								WHEN @tintDiscoutBaseOn = 1 -- Selected Items
								THEN
									(CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=@numTempPromotionID AND tintRecordType=6 AND tintType=1 AND numValue=I.numItemCode) > 0 THEN 1 ELSE 0 END)
								WHEN @tintDiscoutBaseOn = 2 -- Selected Item Classification
								THEN 
									(CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=@numTempPromotionID AND tintRecordType=6 AND tintType=2 AND numValue=ISNULL(I.numItemClassification,0)) > 0 THEN 1 ELSE 0 END)
								WHEN @tintDiscoutBaseOn = 3 -- Related Items
								THEN
									(CASE WHEN (SELECT COUNT(*) FROM SimilarItems WHERE numDomainId=@numDomainID AND numParentItemCode=@numItemCode AND numItemCode=I.numItemCode) > 0 THEN 1 ELSE 0 END)
								WHEN @tintDiscoutBaseOn IN  (4,5) -- Item with list price lesser or equal
								THEN
									(CASE WHEN ISNULL(I.monListPrice,0) <= ISNULL((SELECT monListPrice FROM Item WHERE numItemCode=@numItemCode),0) AND ISNULL(T1.bitPromotionTriggered,0)=0 THEN 1 ELSE 0 END)
								WHEN @tintDiscoutBaseOn = 6 -- Selected Items
								THEN
									(CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=@numTempPromotionID AND tintRecordType=6 AND tintType=4 AND numValue=I.numItemCode) > 0 THEN 1 ELSE 0 END)
								ELSE 0
							END)
			END

			SET @i = @i + 1
		END

		SET @j = @j + 1
	END

	-- IF ANY COUPON BASE PROMOTION IS TRIGERRED BUT THERE ARE NO ITEMS TO APPLY PROMOTION DISCOUNT THEN CLEAR PROMOTIO TRIGGER
	IF (SELECT 
			COUNT(*) 
		FROM 
			@TEMP T1 
		INNER JOIN
			PromotionOffer
		ON
			T1.numPromotionID=PromotionOffer.numProId
			AND ISNULL(PromotionOffer.bitUseOrderPromotion,0) = 1
		WHERE 
			ISNULL(T1.numPromotionID,0) > 0
			AND ISNULL(T1.bitPromotionTriggered,0)=1 
			AND ISNULL(T1.bitPromotionDiscount,0)=0 
			AND (SELECT COUNT(*) FROM @TEMP T2 WHERE T2.numPromotionID=T1.numPromotionID AND ISNULL(bitPromotionTriggered,0) = 0) = 0) > 0
	BEGIN
		UPDATE
			T1
		SET
			T1.numPromotionID = 0
			,T1.bitPromotionTriggered=0
			,T1.bitPromotionDiscount=0
			,T1.vcPromotionDescription=''
			,T1.bitChanged=1
			,T1.monUnitPrice=ISNULL(T1.monInsertUnitPrice,0)
			,T1.fltDiscount=ISNULL(T1.fltInsertDiscount,0)
			,T1.bitDiscountType=ISNULL(T1.bitInsertDiscountType,0)
		FROM 
			@TEMP T1 
		INNER JOIN
			PromotionOffer
		ON
			T1.numPromotionID=PromotionOffer.numProId
			AND ISNULL(PromotionOffer.bitUseOrderPromotion,0) = 1
		WHERE 
			ISNULL(T1.numPromotionID,0) > 0
			AND ISNULL(T1.bitPromotionTriggered,0)=1 
			AND ISNULL(T1.bitPromotionDiscount,0)=0 
			AND (SELECT COUNT(*) FROM @TEMP T2 WHERE T2.numPromotionID=T1.numPromotionID AND ISNULL(bitPromotionTriggered,0) = 0) = 0

		SET @j = 1
		SET @jCount = ISNULL((SELECT COUNT(*) FROM @TEMPPromotion),0)

		WHILE @j <= @jCount
		BEGIN
			SELECT 
				@numTempPromotionID=numPromotionID
				,@numItemCode=numTriggerItemCode
				,@numTempWarehouseItemID=numWarehouseItemID
				,@numUnits = numUnits
				,@monTotalAmount=monTotalAmount
			FROM 
				@TEMPPromotion 
			WHERE 
				ID = @j

			
			SELECT
				@fltDiscountValue=ISNULL(fltDiscountValue,0)
				,@tintDiscountType=ISNULL(tintDiscountType,0)
				,@tintDiscoutBaseOn=tintDiscoutBaseOn
				,@vcShortDesc=ISNULL(vcShortDesc,'')
				,@tintItemCalDiscount=ISNULL(tintItemCalDiscount,1)
			FROM
				PromotionOffer
			WHERE
				numProId=@numTempPromotionID

			-- If discount is based on amount or quantity than we have to checked remaining amount or qty left to give as discount 
			IF @tintDiscountType = 2 OR @tintDiscountType = 3
			BEGIN
				SET @bitRemainingCheckRquired = 1
			END
			ELSE
			BEGIN
				SET @bitRemainingCheckRquired = 0
				SET @numRemainingPromotion = 0
			END

			-- If promotion is valid than check whether any item left to apply promotion
			SET @i = 1
			WHILE @i <= @iCount
			BEGIN
				IF @bitRemainingCheckRquired=1
				BEGIN
					IF @tintDiscountType = 2 -- Discount by amount
					BEGIN
						SET @numRemainingPromotion = @fltDiscountValue - ISNULL((SELECT SUM(fltDiscount) FROM @TEMP WHERE numPromotionID = @numTempPromotionID	AND ISNULL(bitDiscountType,0)=1 AND bitPromotionDiscount = 1),0)
					END
					ELSE IF @tintDiscountType = 3 -- Discount by quantity
					BEGIN
						IF @tintDiscoutBaseOn IN (5,6)
						BEGIN
							SET @numRemainingPromotion = @fltDiscountValue - ISNULL((SELECT SUM(numUnits) FROM @TEMP WHERE numPromotionID = @numTempPromotionID	AND ISNULL(bitDiscountType,0)=1 AND bitPromotionDiscount = 1),0)
						END
						ELSE
						BEGIN
							SET @numRemainingPromotion = @fltDiscountValue - ISNULL((SELECT SUM(fltDiscount/monUnitPrice) FROM @TEMP WHERE numPromotionID = @numTempPromotionID	AND ISNULL(bitDiscountType,0)=1 AND bitPromotionDiscount = 1),0)
						END
					END
				END

				IF @bitRemainingCheckRquired=0 OR (@bitRemainingCheckRquired=1 AND @numRemainingPromotion > 0)
				BEGIN
					UPDATE
						T1
					SET
						monUnitPrice= (CASE WHEN @tintItemCalDiscount = 1 THEN ISNULL(T1.monInsertUnitPrice,0) ELSE ISNULL(I.monListPrice,0) END)
						,fltDiscount = (CASE 
											WHEN @bitRemainingCheckRquired=0 
											THEN 
												@fltDiscountValue
											ELSE 
												(CASE 
													WHEN @tintDiscountType = 2 -- Discount by amount
													THEN
														(CASE 
															WHEN (T1.numUnits * (CASE 
																								WHEN (ISNULL(bitKitParent,0)=1 OR ISNULL(bitAssembly,0) = 1) AND ISNULL(bitCalAmtBasedonDepItems,0) = 1
																								THEN dbo.GetCalculatedPriceForKitAssembly(@numDomainID,@numDivisionID,I.numItemCode,T1.numUnits,@numTempWarehouseItemID,ISNULL(tintKitAssemblyPriceBasedOn,1),0,0,ISNULL(T1.vcKitChildItems,''))
																								ELSE (CASE WHEN @tintItemCalDiscount = 1 THEN ISNULL(T1.monInsertUnitPrice,0) ELSE ISNULL(I.monListPrice,0) END)
																							END)) >= @numRemainingPromotion 
															THEN @numRemainingPromotion 
															ELSE (T1.numUnits * (CASE 
																								WHEN (ISNULL(bitKitParent,0)=1 OR ISNULL(bitAssembly,0) = 1) AND ISNULL(bitCalAmtBasedonDepItems,0) = 1
																								THEN dbo.GetCalculatedPriceForKitAssembly(@numDomainID,@numDivisionID,I.numItemCode,T1.numUnits,@numTempWarehouseItemID,ISNULL(tintKitAssemblyPriceBasedOn,1),0,0,ISNULL(T1.vcKitChildItems,''))
																								ELSE (CASE WHEN @tintItemCalDiscount = 1 THEN ISNULL(T1.monInsertUnitPrice,0) ELSE ISNULL(I.monListPrice,0) END)
																							END)) 
														END)
													WHEN @tintDiscountType = 3 -- Discount by quantity
													THEN
														(CASE 
															WHEN @tintDiscoutBaseOn IN (5,6)
															THEN
																(CASE 
																		WHEN T1.numUnits >= @numRemainingPromotion 
																		THEN @numRemainingPromotion 
																		ELSE T1.numUnits 
																	END) * (CASE 
																				WHEN (CASE WHEN @tintItemCalDiscount = 1 THEN ISNULL(T1.monInsertUnitPrice,0) ELSE ISNULL(I.monListPrice,0) END) > @monDiscountedItemPrice
																				THEN ((CASE WHEN @tintItemCalDiscount = 1 THEN ISNULL(T1.monInsertUnitPrice,0) ELSE ISNULL(I.monListPrice,0) END) - @monDiscountedItemPrice)
																				ELSE 
																					(CASE WHEN @tintItemCalDiscount = 1 THEN ISNULL(T1.monInsertUnitPrice,0) ELSE ISNULL(I.monListPrice,0) END) 
																			END)
															ELSE
																(CASE 
																	WHEN T1.numUnits >= @numRemainingPromotion 
																	THEN (@numRemainingPromotion * (CASE 
																										WHEN (ISNULL(bitKitParent,0)=1 OR ISNULL(bitAssembly,0) = 1) AND ISNULL(bitCalAmtBasedonDepItems,0) = 1
																										THEN dbo.GetCalculatedPriceForKitAssembly(@numDomainID,@numDivisionID,I.numItemCode,T1.numUnits,@numTempWarehouseItemID,ISNULL(tintKitAssemblyPriceBasedOn,1),0,0,ISNULL(T1.vcKitChildItems,''))
																										ELSE (CASE WHEN @tintItemCalDiscount = 1 THEN ISNULL(T1.monInsertUnitPrice,0) ELSE ISNULL(I.monListPrice,0) END)
																									END)) 
																	ELSE (T1.numUnits * (CASE 
																							WHEN (ISNULL(bitKitParent,0)=1 OR ISNULL(bitAssembly,0) = 1) AND ISNULL(bitCalAmtBasedonDepItems,0) = 1
																							THEN dbo.GetCalculatedPriceForKitAssembly(@numDomainID,@numDivisionID,I.numItemCode,T1.numUnits,@numTempWarehouseItemID,ISNULL(tintKitAssemblyPriceBasedOn,1),0,0,ISNULL(T1.vcKitChildItems,''))
																							ELSE (CASE WHEN @tintItemCalDiscount = 1 THEN ISNULL(T1.monInsertUnitPrice,0) ELSE ISNULL(I.monListPrice,0) END)
																						END)) 
																END)
														END)
													ELSE 0
												END)
											END
										)
						,bitDiscountType = (CASE WHEN @bitRemainingCheckRquired=0 THEN 0 ELSE 1 END)
						,numPromotionID=@numTempPromotionID
						,bitPromotionTriggered=(CASE WHEN bitPromotionTriggered=1 THEN 1 ELSE 0 END)
						,vcPromotionDescription=@vcShortDesc
						,bitChanged=1
						,bitPromotionDiscount=1
					FROM
						@TEMP T1
					INNER JOIN
						Item I
					ON
						T1.numItemCode = I.numItemCode
					WHERE
						ID=@i
						AND (ISNULL(T1.numPromotionID,0) = 0 OR (ISNULL(T1.numPromotionID,0) = @numTempPromotionID AND ISNULL(bitPromotionDiscount,0)=0))
						AND 1 = (CASE 
									WHEN @tintDiscoutBaseOn = 1 -- Selected Items
									THEN
										(CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=@numTempPromotionID AND tintRecordType=6 AND tintType=1 AND numValue=I.numItemCode) > 0 THEN 1 ELSE 0 END)
									WHEN @tintDiscoutBaseOn = 2 -- Selected Item Classification
									THEN 
										(CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=@numTempPromotionID AND tintRecordType=6 AND tintType=2 AND numValue=ISNULL(I.numItemClassification,0)) > 0 THEN 1 ELSE 0 END)
									WHEN @tintDiscoutBaseOn = 3 -- Related Items
									THEN
										(CASE WHEN (SELECT COUNT(*) FROM SimilarItems WHERE numDomainId=@numDomainID AND numParentItemCode=@numItemCode AND numItemCode=I.numItemCode) > 0 THEN 1 ELSE 0 END)
									WHEN @tintDiscoutBaseOn IN  (4,5) -- Item with list price lesser or equal
									THEN
										(CASE WHEN ISNULL(I.monListPrice,0) <= ISNULL((SELECT monListPrice FROM Item WHERE numItemCode=@numItemCode),0) AND ISNULL(T1.bitPromotionTriggered,0)=0 THEN 1 ELSE 0 END)
									WHEN @tintDiscoutBaseOn = 6 -- Selected Items
									THEN
										(CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=@numTempPromotionID AND tintRecordType=6 AND tintType=4 AND numValue=I.numItemCode) > 0 THEN 1 ELSE 0 END)
									ELSE 0
								END)
				END

				SET @i = @i + 1
			END

			SET @j = @j + 1
		END
	END


	UPDATE
		CI
	SET
		monPrice=T1.monUnitPrice
		,fltDiscount=T1.fltDiscount
		,bitDiscountType=T1.bitDiscountType
		,monTotAmtBefDiscount = ISNULL(CI.numUnitHour,0) * ISNULL(T1.monUnitPrice,0)
		,monTotAmount = (CASE 
							WHEN ISNULL(T1.fltDiscount,0) > 0
							THEN (CASE 
									WHEN ISNULL(T1.bitDiscountType,0) = 1 
									THEN (CASE 
											WHEN (ISNULL(CI.numUnitHour,0) * ISNULL(T1.monUnitPrice,0)) - T1.fltDiscount >= 0
											THEN (ISNULL(CI.numUnitHour,0) * ISNULL(T1.monUnitPrice,0)) - T1.fltDiscount
											ELSE 0
										END)
									ELSE ISNULL(CI.numUnitHour,0) * (ISNULL(T1.monUnitPrice,0) - (ISNULL(T1.monUnitPrice,0) * (T1.fltDiscount/100)))
								END)
							ELSE ISNULL(CI.numUnitHour,0) * ISNULL(T1.monUnitPrice,0)
						END)
		,PromotionID=T1.numPromotionID
		,bitPromotionTrigerred=T1.bitPromotionTriggered
		,PromotionDesc=T1.vcPromotionDescription
		,bitPromotionDiscount=T1.bitPromotionDiscount
	FROM
		CartItems CI
	INNER JOIN
		@Temp T1
	ON
		CI.numCartID = T1.numCartID
	WHERE
		T1.bitChanged=1
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_PromotionOffer_ApplyItemPromotionToOrder')
DROP PROCEDURE USP_PromotionOffer_ApplyItemPromotionToOrder
GO
CREATE PROCEDURE [dbo].[USP_PromotionOffer_ApplyItemPromotionToOrder]
	@numDomainID NUMERIC(18,0),
	@numDivisionID NUMERIC(18,0),
	@vcItems VARCHAR(MAX),
	@bitBasedOnDiscountCode BIT,
	@vcDiscountCode VARCHAR(100)
AS
BEGIN
	DECLARE @hDocItem INT
	DECLARE @numRelationship NUMERIC(18,0)
	DECLARE @numProfile NUMERIC(18,0)

	SELECT 
		@numRelationship=numCompanyType,
		@numProfile=vcProfile
	FROM 
		DivisionMaster D                  
	JOIN 
		CompanyInfo C 
	ON 
		C.numCompanyId=D.numCompanyID                  
	WHERE 
		numDivisionID =@numDivisionID 		

	DECLARE @TEMP TABLE
	(
		ID INT IDENTITY(1,1)
		,numOppItemID NUMERIC(18,0)
        ,numItemCode NUMERIC(18,0)
        ,numUnits FLOAT
		,numWarehouseItemID NUMERIC(18,0)
        ,monUnitPrice DECIMAL(20,5)
        ,fltDiscount DECIMAL(20,5)
        ,bitDiscountType BIT
        ,monTotalAmount DECIMAL(20,5)
        ,numPromotionID NUMERIC(18,0)
        ,bitPromotionTriggered BIT
        ,vcPromotionDescription VARCHAR(MAX)
		,bitPromotionDiscount BIT
		,vcKitChildItems VARCHAR(MAX)
		,vcInclusionDetail VARCHAR(MAX)
		,numSelectedPromotionID NUMERIC(18,0)
		,bitChanged BIT		
	)

	IF ISNULL(@vcItems,'') <> ''
	BEGIN
		EXEC sp_xml_preparedocument @hDocItem OUTPUT, @vcItems

		INSERT INTO @TEMP
		(
			numOppItemID
			,numItemCode
			,numUnits
			,numWarehouseItemID
			,monUnitPrice
			,fltDiscount
			,bitDiscountType
			,monTotalAmount
			,numPromotionID
			,bitPromotionTriggered
			,vcPromotionDescription
			,bitPromotionDiscount
			,vcKitChildItems
			,vcInclusionDetail
			,numSelectedPromotionID
			,bitChanged
		)

		SELECT 
			numOppItemID
			,numItemCode
			,numUnits
			,numWarehouseItemID
			,monUnitPrice
			,fltDiscount
			,bitDiscountType
			,monTotalAmount
			,numPromotionID
			,bitPromotionTriggered
			,vcPromotionDescription
			,bitPromotionDiscount
			,vcKitChildItems
			,vcInclusionDetail
			,numSelectedPromotionID
			,0
		FROM 
			OPENXML (@hDocItem,'/NewDataSet/Item',2)                                                                          
		WITH                       
		(                                                                          
			numOppItemID NUMERIC(18,0)
			,numItemCode NUMERIC(18,0)
			,numUnits FLOAT
			,numWarehouseItemID NUMERIC(18,0)
			,monUnitPrice DECIMAL(20,5)
			,fltDiscount DECIMAL(20,5)
			,bitDiscountType BIT
			,monTotalAmount DECIMAL(20,5)
			,numPromotionID NUMERIC(18,0)
			,bitPromotionTriggered BIT
			,bitPromotionDiscount BIT
			,vcKitChildItems VARCHAR(MAX)
			,vcInclusionDetail VARCHAR(MAX)
			,vcPromotionDescription VARCHAR(MAX)
			,numSelectedPromotionID NUMERIC(18,0)
		)

		EXEC sp_xml_removedocument @hDocItem

		DECLARE @i INT = 1
		DECLARE @iCount INT 
		SET @iCount = (SELECT COUNT(*) FROM @TEMP)

		DECLARE @l INT = 1
		DECLARE @lCount INT
		SET @lCount = (SELECT COUNT(*) FROM @TEMP)

		DECLARE @numTempPromotionID NUMERIC(18,0)
		DECLARE @tintOfferTriggerValueType TINYINT
		DECLARE @tintOfferBasedOn TINYINT
		DECLARE @tintOfferTriggerValueTypeRange TINYINT
		DECLARE @fltOfferTriggerValue FLOAT
		DECLARE @fltOfferTriggerValueRange FLOAT
		DECLARE @vcShortDesc VARCHAR(500)
		DECLARE @tintItemCalDiscount TINYINT
		DECLARE @monDiscountedItemPrice DECIMAL(20,5)
		DECLARE @numItemCode NUMERIC(18,0)
		DECLARE @numTempWarehouseItemID NUMERIC(18,0)
		DECLARE @numUnits FLOAT
		DECLARE @monTotalAmount DECIMAL(20,5)
		DECLARE @numItemClassification AS NUMERIC(18,0)

		DECLARE @bitRemainingCheckRquired AS BIT
		DECLARE @numRemainingPromotion FLOAT
		DECLARE @fltDiscountValue FLOAT
		DECLARE @tintDiscountType TINYINT
		DECLARE @tintDiscoutBaseOn TINYINT

		DECLARE @TEMPUsedPromotion TABLE
		(
			ID INT
			,numPromotionID NUMERIC(18,0)
			,numUnits FLOAT
			,monTotalAmount DECIMAL(20,5)
		)

		INSERT INTO @TEMPUsedPromotion
		(
			ID
			,numPromotionID
			,numUnits
			,monTotalAmount
		)
		SELECT
			ROW_NUMBER() OVER(ORDER BY T1.numPromotionID ASC)
			,numPromotionID
			,SUM(numUnits)
			,SUM(monTotalAmount)
		FROM
			@TEMP T1
		WHERE
			ISNULL(numPromotionID,0) > 0
			AND ISNULL(bitPromotionTriggered,0) = 1
		GROUP BY
			numPromotionID

		DECLARE @j INT = 1
		DECLARE @jCount INT
		SET @jCount = ISNULL((SELECT COUNT(*) FROM @TEMPUsedPromotion),0)

		-- FIRST REMOVE ALL PROMOTIONS WHERE ITEM WHICH TRIGERRED PROMOTION IS DELETED
		UPDATE 
			T1
		SET 
			numPromotionID=0
			,bitPromotionTriggered=0
			,vcPromotionDescription=''
			,bitPromotionDiscount=0
			,bitChanged=1
			,monUnitPrice=ISNULL(T2.monPrice,0)
			,fltDiscount=ISNULL(T2.decDiscount,0)
			,bitDiscountType=ISNULL(T2.tintDisountType,0)
		FROM
			@TEMP T1
		CROSS APPLY
			dbo.fn_GetItemPriceBasedOnMethod(@numDomainID,@numDivisionID,T1.numItemCode,T1.numUnits,0,0,T1.numWarehouseItemID,T1.vcKitChildItems) T2
		WHERE 
			numPromotionID > 0
			AND ISNULL(bitPromotionTriggered,0) = 0
			AND (SELECT COUNT(*) FROM @TEMP T3 WHERE T3.numPromotionID=T1.numPromotionID AND ISNULL(T3.bitPromotionTriggered,0) = 1) = 0

		-- FIRST REMOVE APPLIED PROMOTION IF THEY ARE NOT VALIES
		WHILE @j <= @jCount
		BEGIN
			SELECT 
				@numTempPromotionID=numPromotionID
				,@numUnits = numUnits
				,@monTotalAmount=monTotalAmount
			FROM 
				@TEMPUsedPromotion 
			WHERE 
				ID = @j

			IF NOT EXISTS (SELECT 
								PO.numProId 
							FROM 
								PromotionOffer PO
							LEFT JOIN	
								PromotionOffer POOrder
							ON
								PO.numOrderPromotionID = POOrder.numProId
							LEFT JOIN	
								DiscountCodes DC
							ON
								PO.numOrderPromotionID = DC.numPromotionID
							WHERE 
								PO.numDomainId=@numDomainID 
								AND PO.numProId=@numTempPromotionID
								AND ISNULL(PO.bitEnabled,0)=1 
								AND ISNULL(PO.IsOrderBasedPromotion,0) = 0
								AND 1 = (CASE 
											WHEN ISNULL(PO.numOrderPromotionID,0) > 0 
											THEN (CASE WHEN POOrder.bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN GETUTCDATE()>=POOrder.dtValidFrom AND GETUTCDATE()<=POOrder.dtValidTo THEN 1 ELSE 0 END) END)
											ELSE (CASE WHEN PO.bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN GETUTCDATE()>=PO.dtValidFrom AND GETUTCDATE()<=PO.dtValidTo THEN 1 ELSE 0 END) END)
										END)
								AND 1 = (CASE 
											WHEN ISNULL(PO.numOrderPromotionID,0) > 0 
											THEN
												(CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=PO.numOrderPromotionID AND tintType=2 AND numRelationship=@numRelationship AND numProfile=@numProfile) > 0 THEN 1 ELSE 0 END)
											ELSE
												(CASE PO.tintCustomersBasedOn 
													WHEN 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=PO.numProId AND tintType=1 AND numDivisionID=@numDivisionID) > 0 THEN 1 ELSE 0 END)
													WHEN 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=PO.numProId AND tintType=2 AND numRelationship=@numRelationship AND numProfile=@numProfile) > 0 THEN 1 ELSE 0 END)
													WHEN 3 THEN 1
													ELSE 0
												END)
										END)
								AND 1 = (CASE 
											WHEN ISNULL(PO.tintOfferTriggerValueType,1) = 1 --Based on quantity
											THEN
												CASE
													WHEN ISNULL(PO.tintOfferTriggerValueTypeRange,1) = 2
													THEN
														(CASE WHEN ISNULL(@numUnits,0) BETWEEN ISNULL(PO.fltOfferTriggerValue,0) AND ISNULL(PO.fltOfferTriggerValueRange,0) THEN 1 ELSE 0 END)
													ELSE
														(CASE WHEN ISNULL(@numUnits,0) >= PO.fltOfferTriggerValue THEN 1 ELSE 0 END)
												END
											WHEN ISNULL(PO.tintOfferTriggerValueType,1) = 2 --Based on amount
											THEN
												CASE
													WHEN ISNULL(PO.tintOfferTriggerValueTypeRange,1) = 2
													THEN
														(CASE WHEN ISNULL(@monTotalAmount,0) BETWEEN ISNULL(PO.fltOfferTriggerValue,0) AND ISNULL(PO.fltOfferTriggerValueRange,0) THEN 1 ELSE 0 END)
													ELSE
														(CASE WHEN ISNULL(@monTotalAmount,0) >= PO.fltOfferTriggerValue THEN 1 ELSE 0 END)
												END
										END)
								AND 1 = (CASE 
											WHEN ISNULL(PO.numOrderPromotionID,0) > 0 
											THEN 
												(CASE WHEN ISNULL(DC.CodeUsageLimit,0) <> 0 THEN (CASE WHEN ISNULL((SELECT intCodeUsed FROM DiscountCodeUsage WHERE numDiscountId=DC.numDiscountId AND numDivisionId=@numDivisionId),0) >= DC.CodeUsageLimit THEN 0 ELSE 1 END) ELSE 1 END)
											ELSE 1 
										END)
					)
			BEGIN
				-- IF Promotion is not valid than revert line item price to based on default pricing
				UPDATE 
					T1
				SET 
					numPromotionID=0
					,bitPromotionTriggered=0
					,vcPromotionDescription=''
					,bitPromotionDiscount=0
					,bitChanged=1
					,monUnitPrice=ISNULL(T2.monPrice,0)
					,fltDiscount=ISNULL(T2.decDiscount,0)
					,bitDiscountType=ISNULL(T2.tintDisountType,0)
				FROM
					@TEMP T1
				CROSS APPLY
					dbo.fn_GetItemPriceBasedOnMethod(@numDomainID,@numDivisionID,T1.numItemCode,T1.numUnits,0,0,T1.numWarehouseItemID,T1.vcKitChildItems) T2
				WHERE 
					numPromotionID=@numTempPromotionID
			END

			SET @j = @j + 1
		END

		UPDATE
			T1
		SET 
			T1.bitPromotionDiscount = 0
			,T1.bitChanged=1
			,T1.monUnitPrice=ISNULL(T2.monPrice,0)
			,T1.fltDiscount=ISNULL(T2.decDiscount,0)
			,T1.bitDiscountType=ISNULL(T2.tintDisountType,0)
		FROM
			@TEMP T1
		INNER JOIN
			PromotionOffer PO
		ON
			T1.numPromotionID=PO.numProId
		CROSS APPLY
			dbo.fn_GetItemPriceBasedOnMethod(@numDomainID,@numDivisionID,T1.numItemCode,T1.numUnits,0,0,T1.numWarehouseItemID,T1.vcKitChildItems) T2
		WHERE
			ISNULL(numPromotionID,0) > 0
			AND ISNULL(bitPromotionDiscount,0) = 1
			AND ISNULL(PO.tintDiscountType,0) = 3

		IF ISNULL(@bitBasedOnDiscountCode,0) = 1
		BEGIN
			DECLARE @TableItemCouponPromotion TABLE
			(
				ID INT IDENTITY(1,1)
				,numPromotionID NUMERIC(18,0)
				,tintOfferTriggerValueType TINYINT
				,tintOfferTriggerValueTypeRange TINYINT
				,fltOfferTriggerValue FLOAT
				,fltOfferTriggerValueRange FLOAT
				,tintOfferBasedOn TINYINT
				,vcShortDesc VARCHAR(300)
			)

			INSERT INTO @TableItemCouponPromotion
			(
				numPromotionID
				,tintOfferTriggerValueType
				,tintOfferTriggerValueTypeRange
				,fltOfferTriggerValue
				,fltOfferTriggerValueRange
				,tintOfferBasedOn
				,vcShortDesc
			)
			SELECT
				PO.numProId
				,PO.tintOfferTriggerValueType
				,PO.tintOfferTriggerValueTypeRange
				,PO.fltOfferTriggerValue
				,PO.fltOfferTriggerValueRange
				,PO.tintOfferBasedOn
				,ISNULL(PO.vcShortDesc,'-')
			FROM
				PromotionOffer PO
			INNER JOIN
				PromotionOffer POOrder
			ON
				PO.numOrderPromotionID = POOrder.numProId
			INNER JOIN 
				DiscountCodes DC
			ON 
				POOrder.numProId = DC.numPromotionID
			WHERE
				PO.numDomainId = @numDomainID
				AND POOrder.numDomainId=@numDomainID 
				AND ISNULL(PO.bitEnabled,0)=1 
				AND ISNULL(POOrder.bitEnabled,0)=1 
				AND ISNULL(PO.bitUseOrderPromotion,0)=1 
				AND ISNULL(POOrder.IsOrderBasedPromotion,0) = 1
				AND ISNULL(POOrder.bitRequireCouponCode,0) = 1
				AND 1 = (CASE WHEN POOrder.bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN GETUTCDATE()>=POOrder.dtValidFrom AND GETUTCDATE()<=POOrder.dtValidTo THEN 1 ELSE 0 END) END)
				AND 1 = (CASE WHEN ISNULL(DC.CodeUsageLimit,0) <> 0 THEN (CASE WHEN ISNULL((SELECT intCodeUsed FROM DiscountCodeUsage WHERE numDiscountId=DC.numDiscountId AND numDivisionId=@numDivisionId),0) >= DC.CodeUsageLimit THEN 0 ELSE 1 END) ELSE 1 END)
				AND 1 = (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=POOrder.numProId AND tintType=2 AND numRelationship=@numRelationship AND numProfile=@numProfile) > 0 THEN 1 ELSE 0 END)
				AND DC.vcDiscountCode = @vcDiscountCode
			
			SET @l = 1
			DECLARE @k INT = 1
			DECLARE @kCount INT 
			SET @kCount = (SELECT COUNT(*) FROM @TableItemCouponPromotion)

			WHILE @k <= @kCount
			BEGIN
				SELECT
					@numTempPromotionID=numPromotionID
					,@tintOfferTriggerValueType=tintOfferTriggerValueType
					,@tintOfferTriggerValueTypeRange = tintOfferTriggerValueTypeRange
					,@fltOfferTriggerValue=fltOfferTriggerValue
					,@fltOfferTriggerValueRange=fltOfferTriggerValueRange
					,@tintOfferBasedOn=tintOfferBasedOn
					,@vcShortDesc=vcShortDesc
				FROM
					@TableItemCouponPromotion
				WHERE
					ID=@k

				IF (SELECT COUNT(*) FROM @TEMP WHERE numPromotionID=@numTempPromotionID AND ISNULL(bitPromotionTriggered,0)=1) = 0
				BEGIN
					IF ISNULL((SELECT 
								(CASE WHEN @tintOfferTriggerValueType = 2 THEN SUM(ISNULL(monTotalAmount,0)) ELSE SUM(ISNULL(numUnits,0)) END)
							FROM
								@TEMP T1
							INNER JOIN
								Item I
							ON
								T1.numItemCode = I.numItemCode
							WHERE
								ISNULL(numPromotionID,0) = 0
								AND 1 = (CASE 
											WHEN @tintOfferBasedOn = 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=@numTempPromotionID AND tintRecordType=5 AND tintType=1 AND numValue=T1.numItemCode) > 0 THEN 1 ELSE 0 END)
											WHEN @tintOfferBasedOn = 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=@numTempPromotionID AND tintRecordType=5 AND tintType=2 AND numValue=ISNULL(I.numItemClassification,0)) > 0 THEN 1 ELSE 0 END)
											WHEN @tintOfferBasedOn = 4 THEN 1 
											ELSE 0
										END)
							),0) >= ISNULL(@fltOfferTriggerValue,0)
					BEGIN
						SET @i = 1

						WHILE @i <= @iCount
						BEGIN

							IF ISNULL((SELECT 
										(CASE WHEN @tintOfferTriggerValueType = 2 THEN SUM(ISNULL(monTotalAmount,0)) ELSE SUM(ISNULL(numUnits,0)) END)
									FROM
										@TEMP T1
									WHERE
										ISNULL(numPromotionID,0) = @numTempPromotionID
										AND ISNULL(bitPromotionTriggered,0) = 1
									),0) < ISNULL(@fltOfferTriggerValue,0)
							BEGIN
								UPDATE
									T1
								SET
									bitChanged = 1
									,numPromotionID=@numTempPromotionID
									,bitPromotionTriggered=1
									,vcPromotionDescription=ISNULL(@vcShortDesc,'')
								FROM
									@TEMP T1
								INNER JOIN
									Item I
								ON
									T1.numItemCode = I.numItemCode
								WHERE
									T1.ID = @i
									AND ISNULL(numPromotionID,0) = 0
									AND 1 = (CASE 
												WHEN ISNULL(@tintOfferTriggerValueType,1) = 1 --Based on quantity
												THEN
													CASE
														WHEN ISNULL(@tintOfferTriggerValueTypeRange,1) = 2
														THEN
															(CASE WHEN ISNULL(T1.numUnits,0) + ISNULL((SELECT SUM(ISNULL(numUnits,0)) FROM @TEMP WHERE ISNULL(numPromotionID,0) = @numTempPromotionID AND ISNULL(bitPromotionTriggered,0) = 1),0) BETWEEN ISNULL(@fltOfferTriggerValue,0) AND ISNULL(@fltOfferTriggerValueRange,0) THEN 1 ELSE 0 END)
														ELSE
															1
													END
												WHEN ISNULL(@tintOfferTriggerValueType,1) = 2 --Based on amount
												THEN
													CASE
														WHEN ISNULL(@tintOfferTriggerValueTypeRange,1) = 2
														THEN
															(CASE WHEN ISNULL(T1.monTotalAmount,0) + ISNULL((SELECT SUM(ISNULL(monTotalAmount,0)) FROM @TEMP WHERE ISNULL(numPromotionID,0) = @numTempPromotionID AND ISNULL(bitPromotionTriggered,0) = 1),0) BETWEEN ISNULL(@fltOfferTriggerValue,0) AND ISNULL(@fltOfferTriggerValueRange,0) THEN 1 ELSE 0 END)
														ELSE
															1
													END
											END)
									AND 1 = (CASE 
												WHEN @tintOfferBasedOn = 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=@numTempPromotionID AND tintRecordType=5 AND tintType=1 AND numValue=T1.numItemCode) > 0 THEN 1 ELSE 0 END)
												WHEN @tintOfferBasedOn = 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=@numTempPromotionID AND tintRecordType=5 AND tintType=2 AND numValue=ISNULL(I.numItemClassification,0)) > 0 THEN 1 ELSE 0 END)
												WHEN @tintOfferBasedOn = 4 THEN 1 
												ELSE 0
											END)
							END

							SET @i = @i + 1
						END

					END
				END

				SET @k = @k + 1
			END
		END

		-- TRIGGER PROMOTION FOR ITEM WHICH ARE ELIGIBLE
		SET @i = 1
		WHILE @i <= @iCount
		BEGIN
			SET @numTempPromotionID = NULL

			SELECT
				@numTempPromotionID=numProId
				,@tintOfferTriggerValueType=tintOfferTriggerValueType
				,@tintOfferTriggerValueTypeRange = tintOfferTriggerValueTypeRange
				,@fltOfferTriggerValue=fltOfferTriggerValue
				,@fltOfferTriggerValueRange=fltOfferTriggerValueRange
				,@tintOfferBasedOn=tintOfferBasedOn
				,@vcShortDesc=vcShortDesc
			FROM
				@TEMP T1
			INNER JOIN
				Item I
			ON
				T1.numItemCode=I.numItemCode
			CROSS APPLY
			(
				SELECT TOP 1
					PO.numProId
					,PO.tintOfferBasedOn
					,PO.tintOfferTriggerValueType
					,PO.tintOfferTriggerValueTypeRange
					,PO.fltOfferTriggerValue
					,PO.fltOfferTriggerValueRange
					,ISNULL(PO.vcShortDesc,'') vcShortDesc
				FROM 
					PromotionOffer PO
				WHERE
					PO.numDomainId=@numDomainID
					AND 1 = (CASE WHEN ISNULL(T1.numSelectedPromotionID,0) > 0 THEN (CASE WHEN PO.numProId=T1.numSelectedPromotionID THEN 1 ELSE 0 END) ELSE 1 END) 
					AND ISNULL(PO.bitEnabled,0)=1 
					AND ISNULL(PO.IsOrderBasedPromotion,0) = 0
					AND ISNULL(PO.bitUseOrderPromotion,0) = 0
					AND ISNULL(PO.bitRequireCouponCode,0) = 0
					AND 1 = (CASE WHEN PO.bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN GETUTCDATE()>=PO.dtValidFrom AND GETUTCDATE()<=PO.dtValidTo THEN 1 ELSE 0 END) END)
					AND 1 = (CASE 
								WHEN ISNULL(numOrderPromotionID,0) > 0
								THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=numOrderPromotionID AND tintType=2 AND numRelationship=@numRelationship AND numProfile=@numProfile) > 0 THEN 1 ELSE 0 END)
								ELSE
									(CASE tintCustomersBasedOn 
										WHEN 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=PO.numProId AND tintType=1 AND numDivisionID=@numDivisionID) > 0 THEN 1 ELSE 0 END)
										WHEN 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=PO.numProId AND tintType=2 AND numRelationship=@numRelationship AND numProfile=@numProfile) > 0 THEN 1 ELSE 0 END)
										WHEN 3 THEN 1
										ELSE 0
									END)
							END)
					AND 1 = (CASE 
								WHEN PO.tintOfferBasedOn = 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=1 AND numValue=T1.numItemCode) > 0 THEN 1 ELSE 0 END)
								WHEN PO.tintOfferBasedOn = 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=2 AND numValue=ISNULL(I.numItemClassification,0)) > 0 THEN 1 ELSE 0 END)
								WHEN PO.tintOfferBasedOn = 4 THEN 1 
								ELSE 0
							END)
				ORDER BY
					(CASE 
						WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 1 THEN 1
						WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 1 THEN 2
						WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 2 THEN 3
						WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 2 THEN 4
						WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 3 THEN 5
						WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 3 THEN 6
					END) ASC,ISNULL(PO.fltOfferTriggerValue,0) DESC

			) T2
			WHERE
				ISNULL(T1.numPromotionID,0)  = 0
				AND T1.ID = @i
				AND ISNULL((SELECT COUNT(*) FROM @TEMP T3 WHERE T3.numPromotionID=T2.numProId AND T3.bitPromotionTriggered=1),0) = 0

			IF ISNULL(@numTempPromotionID,0) > 0 AND (SELECT COUNT(*) FROM @TEMP WHERE numPromotionID=@numTempPromotionID AND ISNULL(bitPromotionTriggered,0)=1) = 0
			BEGIN
				IF ISNULL((SELECT 
								(CASE WHEN @tintOfferTriggerValueType = 2 THEN SUM(ISNULL(monTotalAmount,0)) ELSE SUM(ISNULL(numUnits,0)) END)
							FROM
								@TEMP T1
							INNER JOIN
								Item I
							ON
								T1.numItemCode = I.numItemCode
							WHERE
								ISNULL(numPromotionID,0) = 0
								AND 1 = (CASE 
											WHEN @tintOfferBasedOn = 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=@numTempPromotionID AND tintRecordType=5 AND tintType=1 AND numValue=T1.numItemCode) > 0 THEN 1 ELSE 0 END)
											WHEN @tintOfferBasedOn = 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=@numTempPromotionID AND tintRecordType=5 AND tintType=2 AND numValue=ISNULL(I.numItemClassification,0)) > 0 THEN 1 ELSE 0 END)
											WHEN @tintOfferBasedOn = 4 THEN 1 
											ELSE 0
										END)
							),0) >= ISNULL(@fltOfferTriggerValue,0)
				BEGIN
					SET @l = 1

					WHILE @l <= @lCount
					BEGIN
						
						IF ISNULL((SELECT 
										(CASE WHEN @tintOfferTriggerValueType = 2 THEN SUM(ISNULL(monTotalAmount,0)) ELSE SUM(ISNULL(numUnits,0)) END)
									FROM
										@TEMP T1
									WHERE
										ISNULL(numPromotionID,0) = @numTempPromotionID
										AND ISNULL(bitPromotionTriggered,0) = 1
									),0) < ISNULL(@fltOfferTriggerValue,0)
						BEGIN
							UPDATE
								T1
							SET
								bitChanged = 1
								,numPromotionID=@numTempPromotionID
								,bitPromotionTriggered=1
								,vcPromotionDescription=ISNULL(@vcShortDesc,'')
							FROM
								@TEMP T1
							INNER JOIN
								Item I
							ON
								T1.numItemCode = I.numItemCode
							WHERE
								T1.ID = @l
								AND ISNULL(numPromotionID,0) = 0
								AND 1 = (CASE 
											WHEN ISNULL(@tintOfferTriggerValueType,1) = 1 --Based on quantity
											THEN
												CASE
													WHEN ISNULL(@tintOfferTriggerValueTypeRange,1) = 2
													THEN
														(CASE WHEN ISNULL(T1.numUnits,0) + ISNULL((SELECT SUM(ISNULL(numUnits,0)) FROM @TEMP WHERE ISNULL(numPromotionID,0) = @numTempPromotionID AND ISNULL(bitPromotionTriggered,0) = 1),0) BETWEEN ISNULL(@fltOfferTriggerValue,0) AND ISNULL(@fltOfferTriggerValueRange,0) THEN 1 ELSE 0 END)
													ELSE
														1
												END
											WHEN ISNULL(@tintOfferTriggerValueType,1) = 2 --Based on amount
											THEN
												CASE
													WHEN ISNULL(@tintOfferTriggerValueTypeRange,1) = 2
													THEN
														(CASE WHEN ISNULL(T1.monTotalAmount,0) + ISNULL((SELECT SUM(ISNULL(monTotalAmount,0)) FROM @TEMP WHERE ISNULL(numPromotionID,0) = @numTempPromotionID AND ISNULL(bitPromotionTriggered,0) = 1),0) BETWEEN ISNULL(@fltOfferTriggerValue,0) AND ISNULL(@fltOfferTriggerValueRange,0) THEN 1 ELSE 0 END)
													ELSE
														1
												END
										END)
								AND 1 = (CASE 
											WHEN @tintOfferBasedOn = 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=@numTempPromotionID AND tintRecordType=5 AND tintType=1 AND numValue=T1.numItemCode) > 0 THEN 1 ELSE 0 END)
											WHEN @tintOfferBasedOn = 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=@numTempPromotionID AND tintRecordType=5 AND tintType=2 AND numValue=ISNULL(I.numItemClassification,0)) > 0 THEN 1 ELSE 0 END)
											WHEN @tintOfferBasedOn = 4 THEN 1 
											ELSE 0
										END)
						END

						SET @l = @l + 1
					END

				END
			END

			SET @i = @i + 1
		END
		

		DECLARE @TEMPPromotion TABLE
		(
			ID INT
			,numPromotionID NUMERIC(18,0)
			,vcShortDesc VARCHAR(500)
			,numTriggerItemCode NUMERIC(18,0)
			,numWarehouseItemID NUMERIC(18,0)
			,numUnits FLOAT
			,monTotalAmount DECIMAL(20,5)
		)

		INSERT INTO @TEMPPromotion
		(
			ID
			,numPromotionID
			,numTriggerItemCode
			,numWarehouseItemID
			,numUnits
			,monTotalAmount
		)
		SELECT
			ROW_NUMBER() OVER(ORDER BY T1.numOppItemID ASC)
			,numPromotionID
			,numItemCode
			,numWarehouseItemID
			,numUnits
			,monTotalAmount
		FROM
			@TEMP T1
		INNER JOIN
			PromotionOffer PO
		ON
			T1.numPromotionID = PO.numProId
		WHERE
			ISNULL(numPromotionID,0) > 0
			AND ISNULL(bitPromotionTriggered,0) = 1
			AND 1 = (CASE WHEN @bitBasedOnDiscountCode=0 THEN (CASE WHEN ISNULL(PO.numOrderPromotionID,0) = 0 THEN 1 ELSE 0 END) ELSE 1 END)

		SET @j = 1
		SET @jCount = ISNULL((SELECT COUNT(*) FROM @TEMPPromotion),0)

		WHILE @j <= @jCount
		BEGIN
			SELECT 
				@numTempPromotionID=numPromotionID
				,@numItemCode=numTriggerItemCode
				,@numTempWarehouseItemID=numWarehouseItemID
				,@numUnits = numUnits
				,@monTotalAmount=monTotalAmount
			FROM 
				@TEMPPromotion 
			WHERE 
				ID = @j

			SELECT
				@fltDiscountValue=ISNULL(fltDiscountValue,0)
				,@tintDiscountType=ISNULL(tintDiscountType,0)
				,@tintDiscoutBaseOn=tintDiscoutBaseOn
				,@vcShortDesc=ISNULL(vcShortDesc,'')
				,@tintItemCalDiscount=ISNULL(tintItemCalDiscount,1)
				,@monDiscountedItemPrice=ISNULL(monDiscountedItemPrice,0)
			FROM
				PromotionOffer
			WHERE
				numProId=@numTempPromotionID

			-- If discount is based on amount or quantity than we have to checked remaining amount or qty left to give as discount 
			IF @tintDiscountType = 2 OR @tintDiscountType = 3
			BEGIN
				SET @bitRemainingCheckRquired = 1
			END
			ELSE
			BEGIN
				SET @bitRemainingCheckRquired = 0
				SET @numRemainingPromotion = 0
			END

			-- If promotion is valid than check whether any item left to apply promotion
			SET @i = 1

			WHILE @i <= @iCount
			BEGIN
				IF @bitRemainingCheckRquired=1
				BEGIN
					IF @tintDiscountType = 2 -- Discount by amount
					BEGIN
						SET @numRemainingPromotion = @fltDiscountValue - ISNULL((SELECT SUM(fltDiscount) FROM @TEMP WHERE numPromotionID = @numTempPromotionID	AND ISNULL(bitDiscountType,0)=1 AND bitPromotionDiscount = 1),0)
					END
					ELSE IF @tintDiscountType = 3 -- Discount by quantity
					BEGIN
						IF @tintDiscoutBaseOn IN (5,6)
						BEGIN
							SET @numRemainingPromotion = @fltDiscountValue - ISNULL((SELECT SUM(numUnits) FROM @TEMP WHERE numPromotionID = @numTempPromotionID	AND ISNULL(bitDiscountType,0)=1 AND bitPromotionDiscount = 1),0)
						END
						ELSE
						BEGIN
							SET @numRemainingPromotion = @fltDiscountValue - ISNULL((SELECT SUM(fltDiscount/monUnitPrice) FROM @TEMP WHERE numPromotionID = @numTempPromotionID	AND ISNULL(bitDiscountType,0)=1 AND bitPromotionDiscount = 1),0)
						END
					END
				END

				IF @bitRemainingCheckRquired=0 OR (@bitRemainingCheckRquired=1 AND @numRemainingPromotion > 0)
				BEGIN
					UPDATE
						T1
					SET
						monUnitPrice= CASE 
											WHEN (ISNULL(bitKitParent,0)=1 OR ISNULL(bitAssembly,0) = 1) AND ISNULL(bitCalAmtBasedonDepItems,0) = 1
											THEN dbo.GetCalculatedPriceForKitAssembly(@numDomainID,@numDivisionID,I.numItemCode,T1.numUnits,@numTempWarehouseItemID,ISNULL(tintKitAssemblyPriceBasedOn,1),0,0,ISNULL(T1.vcKitChildItems,''))
											ELSE (CASE WHEN @tintItemCalDiscount = 1 THEN ISNULL(T2.monFinalPrice,0) ELSE ISNULL(I.monListPrice,0) END)
										END
						,fltDiscount = (CASE 
											WHEN @bitRemainingCheckRquired=0 
											THEN 
												@fltDiscountValue
											ELSE 
												(CASE 
													WHEN @tintDiscountType = 2 -- Discount by amount
													THEN
														(CASE 
															WHEN (T1.numUnits * (CASE 
																								WHEN (ISNULL(bitKitParent,0)=1 OR ISNULL(bitAssembly,0) = 1) AND ISNULL(bitCalAmtBasedonDepItems,0) = 1
																								THEN dbo.GetCalculatedPriceForKitAssembly(@numDomainID,@numDivisionID,I.numItemCode,T1.numUnits,@numTempWarehouseItemID,ISNULL(tintKitAssemblyPriceBasedOn,1),0,0,ISNULL(T1.vcKitChildItems,''))
																								ELSE (CASE WHEN @tintItemCalDiscount = 1 THEN ISNULL(T2.monFinalPrice,0) ELSE ISNULL(I.monListPrice,0) END)
																							END)) >= @numRemainingPromotion 
															THEN @numRemainingPromotion 
															ELSE (T1.numUnits * (CASE 
																								WHEN (ISNULL(bitKitParent,0)=1 OR ISNULL(bitAssembly,0) = 1) AND ISNULL(bitCalAmtBasedonDepItems,0) = 1
																								THEN dbo.GetCalculatedPriceForKitAssembly(@numDomainID,@numDivisionID,I.numItemCode,T1.numUnits,@numTempWarehouseItemID,ISNULL(tintKitAssemblyPriceBasedOn,1),0,0,ISNULL(T1.vcKitChildItems,''))
																								ELSE (CASE WHEN @tintItemCalDiscount = 1 THEN ISNULL(T2.monFinalPrice,0) ELSE ISNULL(I.monListPrice,0) END)
																							END)) 
														END)
													WHEN @tintDiscountType = 3 -- Discount by quantity
													THEN
														(CASE 
															WHEN @tintDiscoutBaseOn IN (5,6)
															THEN
																(CASE 
																	WHEN T1.numUnits >= @numRemainingPromotion 
																	THEN @numRemainingPromotion 
																	ELSE T1.numUnits 
																END) * (CASE 
																			WHEN (CASE WHEN @tintItemCalDiscount = 1 THEN ISNULL(T2.monFinalPrice,0) ELSE ISNULL(I.monListPrice,0) END) > @monDiscountedItemPrice
																			THEN ((CASE WHEN @tintItemCalDiscount = 1 THEN ISNULL(T2.monFinalPrice,0) ELSE ISNULL(I.monListPrice,0) END) - @monDiscountedItemPrice)
																			ELSE 
																				(CASE WHEN @tintItemCalDiscount = 1 THEN ISNULL(T2.monFinalPrice,0) ELSE ISNULL(I.monListPrice,0) END) 
																		END)
															ELSE
																(CASE 
																	WHEN T1.numUnits >= @numRemainingPromotion 
																	THEN (@numRemainingPromotion * (CASE 
																										WHEN (ISNULL(bitKitParent,0)=1 OR ISNULL(bitAssembly,0) = 1) AND ISNULL(bitCalAmtBasedonDepItems,0) = 1
																										THEN dbo.GetCalculatedPriceForKitAssembly(@numDomainID,@numDivisionID,I.numItemCode,T1.numUnits,@numTempWarehouseItemID,ISNULL(tintKitAssemblyPriceBasedOn,1),0,0,ISNULL(T1.vcKitChildItems,''))
																										ELSE (CASE WHEN @tintItemCalDiscount = 1 THEN ISNULL(T2.monFinalPrice,0) ELSE ISNULL(I.monListPrice,0) END)
																									END)) 
																	ELSE (T1.numUnits * (CASE 
																							WHEN (ISNULL(bitKitParent,0)=1 OR ISNULL(bitAssembly,0) = 1) AND ISNULL(bitCalAmtBasedonDepItems,0) = 1
																							THEN dbo.GetCalculatedPriceForKitAssembly(@numDomainID,@numDivisionID,I.numItemCode,T1.numUnits,@numTempWarehouseItemID,ISNULL(tintKitAssemblyPriceBasedOn,1),0,0,ISNULL(T1.vcKitChildItems,''))
																							ELSE (CASE WHEN @tintItemCalDiscount = 1 THEN ISNULL(T2.monFinalPrice,0) ELSE ISNULL(I.monListPrice,0) END)
																						END)) 
																END)
														END)
													ELSE 0
												END)
											END
										)
						,bitDiscountType = (CASE WHEN @bitRemainingCheckRquired=0 THEN 0 ELSE 1 END)
						,numPromotionID=@numTempPromotionID
						,bitPromotionTriggered=(CASE WHEN bitPromotionTriggered=1 THEN 1 ELSE 0 END)
						,vcPromotionDescription=@vcShortDesc
						,bitChanged=1
						,bitPromotionDiscount=1
					FROM
						@TEMP T1
					INNER JOIN
						Item I
					ON
						T1.numItemCode = I.numItemCode
					CROSS APPLY
						dbo.fn_GetItemPriceBasedOnMethod(@numDomainID,@numDivisionID,T1.numItemCode,T1.numUnits,0,0,T1.numWarehouseItemID,T1.vcKitChildItems) T2
					WHERE
						ID=@i
						AND (ISNULL(T1.numPromotionID,0) = 0 OR (ISNULL(T1.numPromotionID,0) = @numTempPromotionID AND ISNULL(bitPromotionDiscount,0)=0))
						AND 1 = (CASE 
									WHEN @tintDiscoutBaseOn = 1 -- Selected Items
									THEN
										(CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=@numTempPromotionID AND tintRecordType=6 AND tintType=1 AND numValue=I.numItemCode) > 0 THEN 1 ELSE 0 END)
									WHEN @tintDiscoutBaseOn = 2 -- Selected Item Classification
									THEN 
										(CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=@numTempPromotionID AND tintRecordType=6 AND tintType=2 AND numValue=ISNULL(I.numItemClassification,0)) > 0 THEN 1 ELSE 0 END)
									WHEN @tintDiscoutBaseOn = 3 -- Related Items
									THEN
										(CASE WHEN (SELECT COUNT(*) FROM SimilarItems WHERE numDomainId=@numDomainID AND numParentItemCode=@numItemCode AND numItemCode=I.numItemCode) > 0 THEN 1 ELSE 0 END)
									WHEN @tintDiscoutBaseOn IN  (4,5) -- Item with list price lesser or equal
									THEN
										(CASE WHEN ISNULL(I.monListPrice,0) <= ISNULL((SELECT monListPrice FROM Item WHERE numItemCode=@numItemCode),0) AND ISNULL(T1.bitPromotionTriggered,0)=0 THEN 1 ELSE 0 END)
									WHEN @tintDiscoutBaseOn = 6 -- Selected Items
									THEN
										(CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=@numTempPromotionID AND tintRecordType=6 AND tintType=4 AND numValue=I.numItemCode) > 0 THEN 1 ELSE 0 END)
									ELSE 0
								END)
				END

				SET @i = @i + 1
			END

			SET @j = @j + 1
		END

		-- IF ANY COUPON BASE PROMOTION IS TRIGERRED BUT THERE ARE NO ITEMS TO APPLY PROMOTION DISCOUNT THEN CLEAR PROMOTIO TRIGGER
		IF (SELECT 
				COUNT(*) 
			FROM 
				@TEMP T1 
			INNER JOIN
				PromotionOffer
			ON
				T1.numPromotionID=PromotionOffer.numProId
				AND ISNULL(PromotionOffer.bitUseOrderPromotion,0) = 1
			WHERE 
				ISNULL(T1.numPromotionID,0) > 0
				AND ISNULL(T1.bitPromotionTriggered,0)=1 
				AND ISNULL(T1.bitPromotionDiscount,0)=0 
				AND (SELECT COUNT(*) FROM @TEMP T2 WHERE T2.numPromotionID=T1.numPromotionID AND ISNULL(bitPromotionTriggered,0) = 0) = 0) > 0
		BEGIN
			UPDATE
				T1
			SET
				T1.numPromotionID = 0
				,T1.bitPromotionTriggered=0
				,T1.bitPromotionDiscount=0
				,T1.vcPromotionDescription=''
				,T1.bitChanged=1
				,T1.monUnitPrice=ISNULL(T2.monPrice,0)
				,T1.fltDiscount=ISNULL(T2.decDiscount,0)
				,T1.bitDiscountType=ISNULL(T2.tintDisountType,0)
			FROM 
				@TEMP T1 
			INNER JOIN
				PromotionOffer
			ON
				T1.numPromotionID=PromotionOffer.numProId
				AND ISNULL(PromotionOffer.bitUseOrderPromotion,0) = 1
			CROSS APPLY
				dbo.fn_GetItemPriceBasedOnMethod(@numDomainID,@numDivisionID,T1.numItemCode,T1.numUnits,0,0,T1.numWarehouseItemID,T1.vcKitChildItems) T2
			WHERE 
				ISNULL(T1.numPromotionID,0) > 0
				AND ISNULL(T1.bitPromotionTriggered,0)=1 
				AND ISNULL(T1.bitPromotionDiscount,0)=0 
				AND (SELECT COUNT(*) FROM @TEMP T2 WHERE T2.numPromotionID=T1.numPromotionID AND ISNULL(bitPromotionTriggered,0) = 0) = 0

			SET @j = 1
			SET @jCount = ISNULL((SELECT COUNT(*) FROM @TEMPPromotion),0)

			WHILE @j <= @jCount
			BEGIN
				SELECT 
					@numTempPromotionID=numPromotionID
					,@numItemCode=numTriggerItemCode
					,@numTempWarehouseItemID=numWarehouseItemID
					,@numUnits = numUnits
					,@monTotalAmount=monTotalAmount
				FROM 
					@TEMPPromotion 
				WHERE 
					ID = @j

				SELECT
					@fltDiscountValue=ISNULL(fltDiscountValue,0)
					,@tintDiscountType=ISNULL(tintDiscountType,0)
					,@tintDiscoutBaseOn=tintDiscoutBaseOn
					,@vcShortDesc=ISNULL(vcShortDesc,'')
					,@tintItemCalDiscount=ISNULL(tintItemCalDiscount,1)
				FROM
					PromotionOffer
				WHERE
					numProId=@numTempPromotionID

				-- If discount is based on amount or quantity than we have to checked remaining amount or qty left to give as discount 
				IF @tintDiscountType = 2 OR @tintDiscountType = 3
				BEGIN
					SET @bitRemainingCheckRquired = 1
				END
				ELSE
				BEGIN
					SET @bitRemainingCheckRquired = 0
					SET @numRemainingPromotion = 0
				END

				-- If promotion is valid than check whether any item left to apply promotion
				SET @i = 1

				WHILE @i <= @iCount
				BEGIN
					IF @bitRemainingCheckRquired=1
					BEGIN
						IF @tintDiscountType = 2 -- Discount by amount
						BEGIN
							SET @numRemainingPromotion = @fltDiscountValue - ISNULL((SELECT SUM(fltDiscount) FROM @TEMP WHERE numPromotionID = @numTempPromotionID	AND ISNULL(bitDiscountType,0)=1 AND bitPromotionDiscount = 1),0)
						END
						ELSE IF @tintDiscountType = 3 -- Discount by quantity
						BEGIN
							IF @tintDiscoutBaseOn IN (5,6)
							BEGIN
								SET @numRemainingPromotion = @fltDiscountValue - ISNULL((SELECT SUM(numUnits) FROM @TEMP WHERE numPromotionID = @numTempPromotionID	AND ISNULL(bitDiscountType,0)=1 AND bitPromotionDiscount = 1),0)
							END
							ELSE
							BEGIN
								SET @numRemainingPromotion = @fltDiscountValue - ISNULL((SELECT SUM(fltDiscount/monUnitPrice) FROM @TEMP WHERE numPromotionID = @numTempPromotionID	AND ISNULL(bitDiscountType,0)=1 AND bitPromotionDiscount = 1),0)
							END
						END
					END

					IF @bitRemainingCheckRquired=0 OR (@bitRemainingCheckRquired=1 AND @numRemainingPromotion > 0)
					BEGIN
						UPDATE
							T1
						SET
							monUnitPrice= CASE 
												WHEN (ISNULL(bitKitParent,0)=1 OR ISNULL(bitAssembly,0) = 1) AND ISNULL(bitCalAmtBasedonDepItems,0) = 1
												THEN dbo.GetCalculatedPriceForKitAssembly(@numDomainID,@numDivisionID,I.numItemCode,T1.numUnits,@numTempWarehouseItemID,ISNULL(tintKitAssemblyPriceBasedOn,1),0,0,ISNULL(T1.vcKitChildItems,''))
												ELSE (CASE WHEN @tintItemCalDiscount = 1 THEN ISNULL(T2.monFinalPrice,0) ELSE ISNULL(I.monListPrice,0) END)
											END
							,fltDiscount = (CASE 
												WHEN @bitRemainingCheckRquired=0 
												THEN 
													@fltDiscountValue
												ELSE 
													(CASE 
														WHEN @tintDiscountType = 2 -- Discount by amount
														THEN
															(CASE 
																WHEN (T1.numUnits * (CASE 
																									WHEN (ISNULL(bitKitParent,0)=1 OR ISNULL(bitAssembly,0) = 1) AND ISNULL(bitCalAmtBasedonDepItems,0) = 1
																									THEN dbo.GetCalculatedPriceForKitAssembly(@numDomainID,@numDivisionID,I.numItemCode,T1.numUnits,@numTempWarehouseItemID,ISNULL(tintKitAssemblyPriceBasedOn,1),0,0,ISNULL(T1.vcKitChildItems,''))
																									ELSE (CASE WHEN @tintItemCalDiscount = 1 THEN ISNULL(T2.monFinalPrice,0) ELSE ISNULL(I.monListPrice,0) END)
																								END)) >= @numRemainingPromotion 
																THEN @numRemainingPromotion 
																ELSE (T1.numUnits * (CASE 
																									WHEN (ISNULL(bitKitParent,0)=1 OR ISNULL(bitAssembly,0) = 1) AND ISNULL(bitCalAmtBasedonDepItems,0) = 1
																									THEN dbo.GetCalculatedPriceForKitAssembly(@numDomainID,@numDivisionID,I.numItemCode,T1.numUnits,@numTempWarehouseItemID,ISNULL(tintKitAssemblyPriceBasedOn,1),0,0,ISNULL(T1.vcKitChildItems,''))
																									ELSE (CASE WHEN @tintItemCalDiscount = 1 THEN ISNULL(T2.monFinalPrice,0) ELSE ISNULL(I.monListPrice,0) END)
																								END)) 
															END)
														WHEN @tintDiscountType = 3 -- Discount by quantity
														THEN
															(CASE 
																WHEN @tintDiscoutBaseOn IN (5,6)
																THEN
																	(CASE 
																		WHEN T1.numUnits >= @numRemainingPromotion 
																		THEN @numRemainingPromotion 
																		ELSE T1.numUnits 
																	END) * (CASE 
																				WHEN (CASE WHEN @tintItemCalDiscount = 1 THEN ISNULL(T2.monFinalPrice,0) ELSE ISNULL(I.monListPrice,0) END) > @monDiscountedItemPrice
																				THEN ((CASE WHEN @tintItemCalDiscount = 1 THEN ISNULL(T2.monFinalPrice,0) ELSE ISNULL(I.monListPrice,0) END) - @monDiscountedItemPrice)
																				ELSE 
																					(CASE WHEN @tintItemCalDiscount = 1 THEN ISNULL(T2.monFinalPrice,0) ELSE ISNULL(I.monListPrice,0) END) 
																			END)
																ELSE
																	(CASE 
																		WHEN T1.numUnits >= @numRemainingPromotion 
																		THEN (@numRemainingPromotion * (CASE 
																											WHEN (ISNULL(bitKitParent,0)=1 OR ISNULL(bitAssembly,0) = 1) AND ISNULL(bitCalAmtBasedonDepItems,0) = 1
																											THEN dbo.GetCalculatedPriceForKitAssembly(@numDomainID,@numDivisionID,I.numItemCode,T1.numUnits,@numTempWarehouseItemID,ISNULL(tintKitAssemblyPriceBasedOn,1),0,0,ISNULL(T1.vcKitChildItems,''))
																											ELSE (CASE WHEN @tintItemCalDiscount = 1 THEN ISNULL(T2.monFinalPrice,0) ELSE ISNULL(I.monListPrice,0) END)
																										END)) 
																		ELSE (T1.numUnits * (CASE 
																								WHEN (ISNULL(bitKitParent,0)=1 OR ISNULL(bitAssembly,0) = 1) AND ISNULL(bitCalAmtBasedonDepItems,0) = 1
																								THEN dbo.GetCalculatedPriceForKitAssembly(@numDomainID,@numDivisionID,I.numItemCode,T1.numUnits,@numTempWarehouseItemID,ISNULL(tintKitAssemblyPriceBasedOn,1),0,0,ISNULL(T1.vcKitChildItems,''))
																								ELSE (CASE WHEN @tintItemCalDiscount = 1 THEN ISNULL(T2.monFinalPrice,0) ELSE ISNULL(I.monListPrice,0) END)
																							END)) 
																	END)
															END)
														ELSE 0
													END)
												END
											)
							,bitDiscountType = (CASE WHEN @bitRemainingCheckRquired=0 THEN 0 ELSE 1 END)
							,numPromotionID=@numTempPromotionID
							,bitPromotionTriggered=(CASE WHEN bitPromotionTriggered=1 THEN 1 ELSE 0 END)
							,vcPromotionDescription=@vcShortDesc
							,bitChanged=1
							,bitPromotionDiscount=1
						FROM
							@TEMP T1
						INNER JOIN
							Item I
						ON
							T1.numItemCode = I.numItemCode
						CROSS APPLY
							dbo.fn_GetItemPriceBasedOnMethod(@numDomainID,@numDivisionID,T1.numItemCode,T1.numUnits,0,0,T1.numWarehouseItemID,T1.vcKitChildItems) T2
						WHERE
							ID=@i
							AND (ISNULL(T1.numPromotionID,0) = 0 OR (ISNULL(T1.numPromotionID,0) = @numTempPromotionID AND ISNULL(bitPromotionDiscount,0)=0))
							AND 1 = (CASE 
										WHEN @tintDiscoutBaseOn = 1 -- Selected Items
										THEN
											(CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=@numTempPromotionID AND tintRecordType=6 AND tintType=1 AND numValue=I.numItemCode) > 0 THEN 1 ELSE 0 END)
										WHEN @tintDiscoutBaseOn = 2 -- Selected Item Classification
										THEN 
											(CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=@numTempPromotionID AND tintRecordType=6 AND tintType=2 AND numValue=ISNULL(I.numItemClassification,0)) > 0 THEN 1 ELSE 0 END)
										WHEN @tintDiscoutBaseOn = 3 -- Related Items
										THEN
											(CASE WHEN (SELECT COUNT(*) FROM SimilarItems WHERE numDomainId=@numDomainID AND numParentItemCode=@numItemCode AND numItemCode=I.numItemCode) > 0 THEN 1 ELSE 0 END)
										WHEN @tintDiscoutBaseOn IN (4,5) -- Item with list price lesser or equal
										THEN
											(CASE WHEN ISNULL(I.monListPrice,0) <= ISNULL((SELECT monListPrice FROM Item WHERE numItemCode=@numItemCode),0) AND ISNULL(T1.bitPromotionTriggered,0)=0 THEN 1 ELSE 0 END)
										WHEN @tintDiscoutBaseOn = 6 -- Selected Items
										THEN
											(CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=@numTempPromotionID AND tintRecordType=6 AND tintType=4 AND numValue=I.numItemCode) > 0 THEN 1 ELSE 0 END)
										ELSE 0
									END)
					END

					SET @i = @i + 1
				END

				SET @j = @j + 1
			END
		END
	END

	SELECT * FROM @TEMP WHERE bitChanged=1
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_PromotionOffer_ClearCouponCodeItemPromotion')
DROP PROCEDURE USP_PromotionOffer_ClearCouponCodeItemPromotion
GO
CREATE PROCEDURE [dbo].[USP_PromotionOffer_ClearCouponCodeItemPromotion]
	@numDomainID NUMERIC(18,0),
	@numDivisionID NUMERIC(18,0),
	@numPromotionID NUMERIC(18,0),
	@vcItems VARCHAR(MAX)
AS
BEGIN
	DECLARE @hDocItem INT

	DECLARE @TEMP TABLE
	(
		ID INT IDENTITY(1,1)
		,numOppItemID NUMERIC(18,0)
        ,numItemCode NUMERIC(18,0)
        ,numUnits FLOAT
		,numWarehouseItemID NUMERIC(18,0)
        ,monUnitPrice DECIMAL(20,5)
        ,fltDiscount DECIMAL(20,5)
        ,bitDiscountType BIT
        ,monTotalAmount DECIMAL(20,5)
        ,numPromotionID NUMERIC(18,0)
        ,bitPromotionTriggered BIT
		,bitPromotionDiscount BIT
        ,vcPromotionDescription VARCHAR(MAX)
		,vcKitChildItems VARCHAR(MAX)
		,vcInclusionDetail VARCHAR(MAX)
		,bitChanged BIT		
	)

	IF ISNULL(@vcItems,'') <> ''
	BEGIN
		EXEC sp_xml_preparedocument @hDocItem OUTPUT, @vcItems

		INSERT INTO @TEMP
		(
			numOppItemID
			,numItemCode
			,numUnits
			,numWarehouseItemID
			,monUnitPrice
			,fltDiscount
			,bitDiscountType
			,monTotalAmount
			,numPromotionID
			,bitPromotionTriggered
			,bitPromotionDiscount
			,vcPromotionDescription
			,vcKitChildItems
			,vcInclusionDetail
			,bitChanged
		)

		SELECT 
			numOppItemID
			,numItemCode
			,numUnits
			,numWarehouseItemID
			,monUnitPrice
			,fltDiscount
			,bitDiscountType
			,monTotalAmount
			,numPromotionID
			,bitPromotionTriggered
			,bitPromotionDiscount
			,vcPromotionDescription
			,vcKitChildItems
			,vcInclusionDetail
			,0
		FROM 
			OPENXML (@hDocItem,'/NewDataSet/Item',2)                                                                          
		WITH                       
		(                                                                          
			numOppItemID NUMERIC(18,0)
			,numItemCode NUMERIC(18,0)
			,numUnits FLOAT
			,numWarehouseItemID NUMERIC(18,0)
			,monUnitPrice DECIMAL(20,5)
			,fltDiscount DECIMAL(20,5)
			,bitDiscountType BIT
			,monTotalAmount DECIMAL(20,5)
			,numPromotionID NUMERIC(18,0)
			,bitPromotionTriggered BIT
			,bitPromotionDiscount BIT
			,vcKitChildItems VARCHAR(MAX)
			,vcInclusionDetail VARCHAR(MAX)
			,vcPromotionDescription VARCHAR(MAX)
		)

		EXEC sp_xml_removedocument @hDocItem

		UPDATE 
			T1
		SET 
			numPromotionID=0
			,bitPromotionTriggered=0
			,vcPromotionDescription=''
			,bitChanged=1
			,monUnitPrice=ISNULL(T2.monPrice,0)
			,fltDiscount=ISNULL(T2.decDiscount,0)
			,bitDiscountType=ISNULL(T2.tintDisountType,0)
			,bitPromotionDiscount=0
		FROM
			@TEMP T1
		CROSS APPLY
			dbo.fn_GetItemPriceBasedOnMethod(@numDomainID,@numDivisionID,T1.numItemCode,T1.numUnits,0,0,T1.numWarehouseItemID,T1.vcKitChildItems) T2
		WHERE 
			ISNULL(numPromotionID,0)=@numPromotionID
	END

	SELECT * FROM @TEMP WHERE bitChanged=1
END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetOrderSelectedItemDetail')
DROP PROCEDURE USP_GetOrderSelectedItemDetail
GO
CREATE PROCEDURE [dbo].[USP_GetOrderSelectedItemDetail]
	@numDomainID NUMERIC(18,0),
	@numDivisionID NUMERIC(18,0),
	@tintOppType AS TINYINT,
	@numItemCode NUMERIC(18,0),
	@numUOMID NUMERIC(18,0),
	@numWarehouseItemID AS NUMERIC(18,0),
	@numQty INT,
	@monPrice FLOAT,
	@numOppID NUMERIC(18,0),
	@numOppItemID NUMERIC(18,0),
	@vcSelectedKitChildItems VARCHAR(MAX),
	@numShippingCountry AS NUMERIC(18,0),
	@numWarehouseID AS NUMERIC(18,0),
	@vcCoupon AS VARCHAR(200)
AS
BEGIN
	DECLARE @vcItemName AS VARCHAR(500)
	DECLARE @bitDropship AS BIT
	DECLARE @bitMatrix AS BIT
	DECLARE @chrItemType AS CHAR(1)
	DECLARE @numItemGroup AS NUMERIC(18,0)
	DECLARE @numBaseUnit AS NUMERIC(18,0)
	DECLARE @numSaleUnit AS NUMERIC(18,0)
	DECLARE @fltUOMConversionFactor AS FLOAT
	DECLARE @numPurchaseUnit AS NUMERIC(18,0)
	DECLARE @bitAssemblyOrKit BIT
	DECLARE @bitAssembly AS BIT
	DECLARE @bitKit AS BIT
	DECLARE @bitHasChildKits BIT
	DECLARE @bitSOWorkOrder BIT
	DECLARE @bitCalAmtBasedonDepItems AS BIT
	DECLARE @ItemDesc AS VARCHAR(1000)
	DECLARE @numItemClassification AS NUMERIC(18,0)

	DECLARE @monListPrice AS DECIMAL(20,5) = 0.0000
	DECLARE @monVendorCost AS DECIMAL(20,5) = 0.0000
	DECLARE @tintPricingBasedOn AS TINYINT = 0.0000

	DECLARE @tintRuleType AS TINYINT
	DECLARE @monPriceLevelPrice AS DECIMAL(20,5) = 0.0000
	DECLARE @monPriceFinalPrice AS DECIMAL(20,5) = 0.0000
	DECLARE @monPriceRulePrice AS DECIMAL(20,5) = 0.0000
	DECLARE @monPriceRuleFinalPrice AS DECIMAL(20,5) = 0.0000
	DECLARE @monLastPrice AS DECIMAL(20,5) = 0.0000
	DECLARE @dtLastOrderDate VARCHAR(200) = ''

	DECLARE @tintDisountType TINYINT
	DECLARE @decDiscount AS FLOAT

	DECLARE @numVendorID AS NUMERIC(18,0)
	DECLARE @vcMinOrderQty AS VARCHAR(5) = '-'
	DECLARE @vcVendorNotes AS VARCHAR(300) = ''
	DECLARE @numRelationship NUMERIC(18,0)
	DECLARE @numProfile NUMERIC(18,0)
	DECLARE @tintPriceLevel AS TINYINT
	DECLARE @tintDefaultSalesPricing AS TINYINT
	DECLARE @tintPriceBookDiscount AS TINYINT
	DECLARE @tintKitAssemblyPriceBasedOn TINYINT

	IF ISNULL(@numDivisionID,0) > 0
	BEGIN
		SELECT @vcMinOrderQty=ISNULL(CAST(intMinQty AS VARCHAR),'-'),@vcVendorNotes=ISNULL(vcNotes,'') FROM Vendor WHERE numVendorID=@numDivisionID AND numItemCode=@numItemCode
	END

	SELECT @tintDefaultSalesPricing = numDefaultSalesPricing,@tintPriceBookDiscount=ISNULL(tintPriceBookDiscount,0) FROM Domain WHERE numDomainId=@numDomainID

	IF ISNULL(@numWarehouseID,0) > 0
	BEGIN
		SELECT TOP 1 @numWarehouseItemID=numWareHouseItemID FROM WareHouseItems WHERE numItemID=@numItemCode AND numWareHouseID=@numWarehouseID ORDER BY numWareHouseItemID ASC
	END

	-- GET ORGANIZATION DETAL
	SELECT 
		@numRelationship=numCompanyType,
		@numProfile=vcProfile,
		@tintPriceLevel = ISNULL(D.tintPriceLevel,0)
	FROM 
		DivisionMaster D                  
	JOIN 
		CompanyInfo C 
	ON 
		C.numCompanyId=D.numCompanyID                  
	WHERE 
		numDivisionID =@numDivisionID       

	-- GET ITEM DETAILS
	SELECT
		@vcItemName = vcItemName,
		@bitDropship = bitAllowDropShip,
		@ItemDesc = Item.txtItemDesc,
		@bitMatrix = ISNULL(Item.bitMatrix,0),
		@chrItemType = charItemType,
		@numItemGroup = numItemGroup,
		@bitAssembly = ISNULL(bitAssembly,0),
		@bitSOWorkOrder = ISNULL(bitSOWorkOrder,0),
		@numBaseUnit = ISNULL(numBaseUnit,0),
		@numSaleUnit = (CASE WHEN ISNULL(@numUOMID,0) > 0 THEN @numUOMID ELSE ISNULL(numSaleUnit,0) END),
		@numPurchaseUnit = (CASE WHEN ISNULL(@numUOMID,0) > 0 THEN @numUOMID ELSE ISNULL(numPurchaseUnit,0) END),
		@numItemClassification = numItemClassification,
		@bitAssemblyOrKit = (CASE WHEN ISNULL(bitKitParent,0)=1 OR ISNULL(bitAssembly,0)=1 THEN 1 ELSE 0 END),
		@monListPrice = (CASE WHEN Item.charItemType='P' THEN ISNULL(WareHouseItems.monWListPrice,0) ELSE ISNULL(Item.monListPrice,0) END),
		@monVendorCost = ISNULL((SELECT ISNULL(monCost,0) * dbo.fn_UOMConversion(Item.numBaseUnit,Item.numItemCode,Item.numDomainID,Item.numPurchaseUnit) FROM Vendor WHERE numVendorID=Item.numVendorID AND Vendor.numItemCode=Item.numItemCode),0),
		@numVendorID = ISNULL(Item.numVendorID,0),
		@numWarehouseItemID = (CASE WHEN ISNULL(@numWarehouseItemID,0) > 0 THEN @numWarehouseItemID ELSE WareHouseItems.numWareHouseItemID END),
		@bitCalAmtBasedonDepItems = ISNULL(bitCalAmtBasedonDepItems,0),
		@tintKitAssemblyPriceBasedOn=ISNULL(tintKitAssemblyPriceBasedOn,1),
		@bitKit=ISNULL(bitKitParent,0),
		@bitHasChildKits = (CASE 
								WHEN ISNULL(bitKitParent,0) = 1 
								THEN 
									(CASE 
										WHEN EXISTS (SELECT ID.numItemDetailID FROM ItemDetails ID INNER JOIN Item IInner ON ID.numChildItemID=IInner.numItemCode WHERE ID.numItemKitID=Item.numItemCode AND ISNULL(IInner.bitKitParent,0)=1)
										THEN 1
										ELSE 0
									END)
								ELSE 0 
							END)
	FROM
		Item
	OUTER APPLY
	(
		SELECT TOP 1 numWareHouseItemID,monWListPrice FROM WareHouseItems WHERE numItemID=@numItemCode AND (numWareHouseItemID = @numWarehouseItemID OR ISNULL(@numWarehouseItemID,0) = 0)
	) AS WareHouseItems
	WHERE
		Item.numItemCode = @numItemCode

	IF ISNULL(@numDivisionID,0) > 0 AND EXISTS (SELECT * FROM Vendor WHERE numItemCode=@numItemCode AND numVendorID=@numDivisionID)
	BEGIN
		SELECT @monVendorCost=ISNULL(monCost,0) FROM Vendor WHERE numVendorID=@numDivisionID AND numItemCode=@numItemCode
	END


	IF @chrItemType = 'P' AND ISNULL(@numWarehouseItemID,0) = 0
	BEGIN
		RAISERROR('WAREHOUSE_DOES_NOT_EXISTS',16,1)
	END

	SET @fltUOMConversionFactor = dbo.fn_UOMConversion((CASE WHEN  @tintOppType = 1 THEN @numSaleUnit ELSE @numPurchaseUnit END),@numItemCode,@numDomainId,@numBaseUnit)

	IF @tintOppType = 1
	BEGIN
		/*Use Calculated Price When item is Assembly/Kit,Calculate price based on sum of dependent items.*/
		IF @bitCalAmtBasedonDepItems = 1 
		BEGIN
			PRINT 'IN DEPENDED ITEM'

			DECLARE @TEMPPrice TABLE
			(
				bitSuccess BIT
				,monPrice DECIMAL(20,5)
			)
			INSERT INTO @TEMPPrice
			(
				bitSuccess
				,monPrice
			)
			SELECT
				bitSuccess
				,monPrice
			FROM
				dbo.fn_GetKitAssemblyCalculatedPrice(@numDomainID,@numDivisionID,@numItemCode,@numQty,@numWarehouseItemID,@tintKitAssemblyPriceBasedOn,@numOppID,@numOppItemID,@vcSelectedKitChildItems)

			IF (SELECT bitSuccess FROM @TEMPPrice) = 1
			BEGIN
				SET @monListPrice = (SELECT monPrice FROM @TEMPPrice)
			END
			ELSE
			BEGIN
				RAISERROR('SELECTED_WAREHOUSE_IS_NOT_AVAILABLE_FOR_ALL_CHILD_ITEMS',16,1)
			END
		END

		IF @tintDefaultSalesPricing = 1 AND (SELECT COUNT(*) FROM PricingTable WHERE numItemCode=@numItemCode AND ISNULL(numPriceRuleID,0) = 0) > 0 -- PRICE LEVEL
		BEGIN
			SELECT
				@tintDisountType = tintDiscountType,
				@decDiscount = ISNULL(decDiscount,0),
				@tintRuleType = tintRuleType,
				@monPriceLevelPrice = (CASE tintRuleType WHEN 1 THEN @monListPrice WHEN 2 THEN @monVendorCost ELSE ISNULL(decDiscount,0) END),
				@monPriceFinalPrice = (CASE tintRuleType
										WHEN 1 -- Deduct From List Price
										THEN
											CASE tintDiscountType 
												WHEN 1 -- PERCENT
												THEN (CASE WHEN @monListPrice > 0 THEN @monListPrice - (@monListPrice * (ISNULL(decDiscount,0)/100)) ELSE 0 END)
												WHEN 2 -- FLAT AMOUNT
												THEN (CASE WHEN @monListPrice > 0 THEN (CASE WHEN (@monListPrice * @numQty * @fltUOMConversionFactor) - ISNULL(decDiscount,0) > 0 THEN ((@monListPrice * @numQty * @fltUOMConversionFactor) - ISNULL(decDiscount,0)) / (@numQty * @fltUOMConversionFactor) ELSE 0 END) ELSE 0 END)
												WHEN 3 -- NAMED PRICE
												THEN ISNULL(decDiscount,0)
											END
										WHEN 2 -- Add to primary vendor cost
										THEN
											CASE tintDiscountType 
												WHEN 1  -- PERCENT
												THEN @monVendorCost + (@monVendorCost * (ISNULL(decDiscount,0)/100))  * @fltUOMConversionFactor
												WHEN 2 -- FLAT AMOUNT
												THEN (CASE WHEN (@monVendorCost * @numQty * @fltUOMConversionFactor) + ISNULL(decDiscount,0) > 0 THEN ((@monVendorCost * @numQty * @fltUOMConversionFactor) + ISNULL(decDiscount,0)) / (@numQty * @fltUOMConversionFactor)  ELSE 0 END)
												WHEN 3 -- NAMED PRICE
												THEN ISNULL(decDiscount,0)
											END
										WHEN 3 -- Named price
										THEN
											ISNULL(decDiscount,0)
										END
									) 
			FROM
			(
				SELECT 
					ROW_NUMBER() OVER(PARTITION BY PricingTable.numItemCode ORDER BY numPricingID) Id,
					*
				FROM
					PricingTable
				WHERE 
					PricingTable.numItemCode = @numItemCode
					
			) TEMP
			WHERE
				numItemCode=@numItemCode AND ((tintRuleType = 3 AND Id=@tintPriceLevel) OR (tintRuleType <> 3 AND @numQty * @fltUOMConversionFactor >= intFromQty AND @numQty * @fltUOMConversionFactor <= intToQty))
		END
		ELSE IF @tintDefaultSalesPricing = 2 AND (SELECT COUNT(*) FROM PriceBookRules WHERE numDomainID=@numDomainID AND tintRuleFor=@tintOppType) > 0 -- PRICE RULE
		BEGIN
			DECLARE @numPriceRuleID NUMERIC(18,0)
			DECLARE @tintPriceRuleType AS TINYINT
			DECLARE @tintPricingMethod TINYINT
			DECLARE @tintPriceBookDiscountType TINYINT
			DECLARE @decPriceBookDiscount FLOAT
			DECLARE @intQntyItems INT
			DECLARE @decMaxDedPerAmt FLOAT

			SELECT 
				TOP 1
				@numPriceRuleID = numPricRuleID,
				@tintPricingMethod = tintPricingMethod,
				@tintPriceRuleType = p.tintRuleType,
				@tintPriceBookDiscountType = P.[tintDiscountType],
				@decPriceBookDiscount = P.[decDiscount],
				@intQntyItems = P.[intQntyItems],
				@decMaxDedPerAmt = P.[decMaxDedPerAmt]
			FROM   
				PriceBookRules P 
			LEFT JOIN 
				PriceBookRuleDTL PDTL 
			ON 
				P.numPricRuleID = PDTL.numRuleID
			LEFT JOIN 
				PriceBookRuleItems PBI 
			ON 
				P.numPricRuleID = PBI.numRuleID
			LEFT JOIN 
				[PriceBookPriorities] PP 
			ON 
				PP.[Step2Value] = P.[tintStep2] 
				AND PP.[Step3Value] = P.[tintStep3]
			WHERE  
				P.numDomainID=@numDomainID 
				AND tintRuleFor=@tintOppType
				AND (
						((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 1 AND PDTL.numValue = @numDivisionID)) -- Priority 1
						OR ((tintStep2 = 2 AND PBI.numValue = @numItemClassification) AND (tintStep3= 1 AND PDTL.numValue = @numDivisionID)) -- Priority 2
						OR ((tintStep2 = 3) AND (tintStep3 = 1 AND PDTL.numValue = @numDivisionID)) -- Priority 3

						OR ((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 4
						OR ((tintStep2 = 2 AND PBI.numValue = @numItemClassification) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 5
						OR ((tintStep2 = 3) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 6

						OR ((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 3)) -- Priority 7
						OR ((tintStep2 = 2 AND PBI.numValue = @numItemClassification) AND (tintStep3 = 3)) -- Priority 8
						OR ((tintStep2 = 3) and (tintStep3 = 3)) -- Priority 9
					)
			ORDER BY 
				PP.Priority ASC


			IF ISNULL(@numPriceRuleID,0) > 0
			BEGIN
				IF @tintPricingMethod = 1 -- BASED ON PRICE TABLE
				BEGIN
					SELECT
						@tintDisountType = tintDiscountType,
						@decDiscount = ISNULL(decDiscount,0),
						@tintRuleType = tintRuleType,
						@monPriceRulePrice = (CASE tintRuleType WHEN 1 THEN @monListPrice WHEN 2 THEN @monVendorCost END) ,
						@monPriceRuleFinalPrice = (CASE tintRuleType
												WHEN 1 -- Deduct From List Price
												THEN
													CASE tintDiscountType 
														WHEN 1 -- PERCENT
														THEN (CASE WHEN @monListPrice > 0 THEN @monListPrice - (@monListPrice * (ISNULL(decDiscount,0)/100)) ELSE 0 END)
														WHEN 2 -- FLAT AMOUNT
														THEN (CASE WHEN @monListPrice > 0 THEN (CASE WHEN (@monListPrice * @numQty * @fltUOMConversionFactor) - ISNULL(decDiscount,0) > 0 THEN ((@monListPrice * @numQty * @fltUOMConversionFactor) - ISNULL(decDiscount,0)) / (@numQty * @fltUOMConversionFactor) ELSE 0 END) ELSE 0 END)
														WHEN 3 -- NAMED PRICE
														THEN ISNULL(decDiscount,0)
													END
												WHEN 2 -- Add to primary vendor cost
												THEN
													CASE tintDiscountType 
														WHEN 1  -- PERCENT
														THEN @monVendorCost + (@monVendorCost * (ISNULL(decDiscount,0)/100))
														WHEN 2 -- FLAT AMOUNT
														THEN (CASE WHEN (@monVendorCost * @numQty * @fltUOMConversionFactor) + ISNULL(decDiscount,0) > 0 THEN ((@monVendorCost * @numQty * @fltUOMConversionFactor) + ISNULL(decDiscount,0)) / (@numQty * @fltUOMConversionFactor)  ELSE 0 END)
														WHEN 3 -- NAMED PRICE
														THEN ISNULL(decDiscount,0)
													END
												WHEN 3 -- Named price
												THEN
													CASE 
													WHEN ISNULL(@tintPriceLevel,0) > 0
													THEN
														(SELECT 
															 ISNULL(decDiscount,0)
														FROM
														(
															SELECT 
																ROW_NUMBER() OVER(PARTITION BY PricingTable.numItemCode ORDER BY numPricingID) Id,
																decDiscount
															FROM 
																PricingTable 
															WHERE 
																PricingTable.numItemCode = @numItemCode 
																AND tintRuleType = 3 
														) TEMP
														WHERE
															Id = @tintPriceLevel)
													ELSE
														ISNULL(decDiscount,0)
													END
												END
											) 
					FROM
						PricingTable
					WHERE
						numPriceRuleID=@numPriceRuleID AND @numQty * @fltUOMConversionFactor >= intFromQty AND @numQty * @fltUOMConversionFactor <= intToQty
				END
				ELSE IF @tintPricingMethod = 2 -- BASED ON RULE
				BEGIN
					SET @tintRuleType = @tintPriceRuleType
					SET @monPriceRulePrice = (CASE @tintPriceRuleType WHEN 1 THEN @monListPrice WHEN 2 THEN @monVendorCost END) 

					IF (@tintPriceBookDiscountType = 1 ) -- Percentage 
					BEGIN
						SET @tintDisountType = @tintPriceBookDiscountType
						SET @decDiscount = @decPriceBookDiscount * (@numQty/@intQntyItems);
						SET @decDiscount = @decPriceBookDiscount * (CASE WHEN @tintPriceRuleType = 2 THEN @monVendorCost ELSE @monListPrice END) /100;
						SET @decMaxDedPerAmt = @decMaxDedPerAmt * (CASE WHEN @tintPriceRuleType = 2 THEN @monVendorCost ELSE @monListPrice END) /100;
						
						IF (@decDiscount > @decMaxDedPerAmt)
							SET @decDiscount = @decMaxDedPerAmt;
				
						IF ( @tintPriceRuleType = 1 )
								SET @monPriceRuleFinalPrice = (@monListPrice - @decDiscount);
						IF ( @tintPriceRuleType = 2 )
								SET @monPriceRuleFinalPrice = (@monVendorCost + @decDiscount);						
					END
					IF ( @tintPriceBookDiscountType = 2 ) -- Flat discount 
					BEGIN
						SET @decDiscount = @decPriceBookDiscount * ((@numQty * @fltUOMConversionFactor)/@intQntyItems);

						IF (@decDiscount > @decMaxDedPerAmt)
							SET @decDiscount = @decMaxDedPerAmt;
					
						IF ( @tintPriceRuleType = 1 )
							SET @monPriceRuleFinalPrice =  ((@monListPrice * @numQty * @fltUOMConversionFactor) - @decDiscount)/(@numQty * @fltUOMConversionFactor);
						IF ( @tintPriceRuleType = 2 )
							SET @monPriceRuleFinalPrice = ((@monVendorCost * @numQty * @fltUOMConversionFactor) + @decDiscount)/(@numQty * @fltUOMConversionFactor);
					END
				END
			END
		
		END
	
		-- GET Last Price
		DECLARE @LastDiscountType TINYINT
		DECLARE @LastDiscount FLOAT

		SELECT 
			TOP 1 
			@monLastPrice = monPrice
			,@dtLastOrderDate = dbo.FormatedDateFromDate(OpportunityMaster.bintCreatedDate,@numDomainID)
			,@LastDiscountType = ISNULL(OpportunityItems.bitDiscountType,1)
			,@LastDiscount = ISNULL(OpportunityItems.fltDiscount,0)
		FROM 
			OpportunityItems 
		JOIN 
			OpportunityMaster 
		ON 
			OpportunityItems.numOppId = OpportunityMaster.numOppId 
		WHERE 
			OpportunityMaster.numDomainId= @numDomainID
			AND tintOppType = @tintOppType
			AND tintOppStatus = 1
			AND numItemCode=@numItemCode
		ORDER BY
			OpportunityMaster.numOppId DESC

		IF @tintDefaultSalesPricing = 1 AND ISNULL(@monPriceFinalPrice,0) > 0
		BEGIN
			-- WE ARE NOT DISPLAYING DISCOUNT SEPERATELY IN PRICE LEVEL
			SET @tintPricingBasedOn = 1
			SET @monPrice = @monPriceFinalPrice
			SET @tintDisountType = 1
			SET @decDiscount = 0
		END
		ELSE IF @tintDefaultSalesPricing = 2 AND ISNULL(@monPriceRuleFinalPrice,0) > 0
		BEGIN
			SET @tintPricingBasedOn = 2
		
			IF @tintPriceBookDiscount = 1 -- Display Unit Price And Disocunt Price Seperately
			BEGIN
				SET @monPrice = @monPriceRulePrice
			END
			ELSE -- Display Disocunt Price As Unit Price
			BEGIN
				SET @monPrice = @monPriceRuleFinalPrice
				SET @tintDisountType = 1
				SET @decDiscount = 0
			END
		END
		ELSE IF @tintDefaultSalesPricing = 3 AND ISNULL(@monLastPrice,0) > 0
		BEGIN
			SET @tintPricingBasedOn = 3
			SET @monPrice = @monLastPrice
			SET @tintDisountType = @LastDiscountType
			SET @decDiscount = @LastDiscount
		END
		ELSE
		BEGIN
			SET @tintPricingBasedOn = 0
			SET @tintDisountType = 1
			SET @decDiscount = 0
			SET @tintRuleType = 0

			IF @tintOppType = 1
				SET @monPrice = @monListPrice
			ELSE
				SET @monPrice = @monVendorCost
		END
	END
	ELSE IF @tintOppType = 2
	BEGIN
		SELECT 
			TOP 1
			@numPriceRuleID = numPricRuleID,
			@tintPricingMethod = tintPricingMethod,
			@tintPriceRuleType = p.tintRuleType,
			@tintPriceBookDiscountType = P.[tintDiscountType],
			@decPriceBookDiscount = P.[decDiscount],
			@intQntyItems = P.[intQntyItems],
			@decMaxDedPerAmt = P.[decMaxDedPerAmt]
		FROM   
			PriceBookRules P 
		LEFT JOIN 
			PriceBookRuleDTL PDTL 
		ON 
			P.numPricRuleID = PDTL.numRuleID
		LEFT JOIN 
			PriceBookRuleItems PBI 
		ON 
			P.numPricRuleID = PBI.numRuleID
		LEFT JOIN 
			[PriceBookPriorities] PP 
		ON 
			PP.[Step2Value] = P.[tintStep2] 
			AND PP.[Step3Value] = P.[tintStep3]
		WHERE  
			P.numDomainID=@numDomainID 
			AND tintRuleFor=@tintOppType
			AND (
					((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 1 AND PDTL.numValue = @numDivisionID)) -- Priority 1
					OR ((tintStep2 = 2 AND PBI.numValue = @numItemClassification) AND (tintStep3= 1 AND PDTL.numValue = @numDivisionID)) -- Priority 2
					OR ((tintStep2 = 3) AND (tintStep3 = 1 AND PDTL.numValue = @numDivisionID)) -- Priority 3

					OR ((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 4
					OR ((tintStep2 = 2 AND PBI.numValue = @numItemClassification) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 5
					OR ((tintStep2 = 3) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 6

					OR ((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 3)) -- Priority 7
					OR ((tintStep2 = 2 AND PBI.numValue = @numItemClassification) AND (tintStep3 = 3)) -- Priority 8
					OR ((tintStep2 = 3) and (tintStep3 = 3)) -- Priority 9
				)
		ORDER BY 
			PP.Priority ASC


		IF ISNULL(@numPriceRuleID,0) > 0
		BEGIN
			IF @tintPricingMethod = 1 -- BASED ON PRICE TABLE
			BEGIN
				SELECT
					@tintDisountType = tintDiscountType,
					@decDiscount = ISNULL(decDiscount,0),
					@tintRuleType = tintRuleType,
					@monPriceRulePrice = (CASE tintRuleType WHEN 1 THEN @monListPrice WHEN 2 THEN @monVendorCost END) ,
					@monPriceRuleFinalPrice = (CASE tintRuleType
											WHEN 1 -- Deduct From List Price
											THEN
												CASE tintDiscountType 
													WHEN 1 -- PERCENT
													THEN (CASE WHEN @monListPrice > 0 THEN @monListPrice - (@monListPrice * (ISNULL(decDiscount,0)/100)) ELSE 0 END)
													WHEN 2 -- FLAT AMOUNT
													THEN (CASE WHEN @monListPrice > 0 THEN (CASE WHEN (@monListPrice * @numQty * @fltUOMConversionFactor) - ISNULL(decDiscount,0) > 0 THEN ((@monListPrice * @numQty * @fltUOMConversionFactor) - ISNULL(decDiscount,0)) / (@numQty * @fltUOMConversionFactor) ELSE 0 END) ELSE 0 END)
													WHEN 3 -- NAMED PRICE
													THEN ISNULL(decDiscount,0)
												END
											WHEN 2 -- Add to primary vendor cost
											THEN
												CASE tintDiscountType 
													WHEN 1  -- PERCENT
													THEN @monVendorCost + (@monVendorCost * (ISNULL(decDiscount,0)/100))
													WHEN 2 -- FLAT AMOUNT
													THEN (CASE WHEN (@monVendorCost * @numQty * @fltUOMConversionFactor) + ISNULL(decDiscount,0) > 0 THEN ((@monVendorCost * @numQty * @fltUOMConversionFactor) + ISNULL(decDiscount,0)) / (@numQty * @fltUOMConversionFactor)  ELSE 0 END)
													WHEN 3 -- NAMED PRICE
													THEN ISNULL(decDiscount,0)
												END
											WHEN 3 -- Named price
											THEN
												CASE 
												WHEN ISNULL(@tintPriceLevel,0) > 0
												THEN
													(SELECT 
															ISNULL(decDiscount,0)
													FROM
													(
														SELECT 
															ROW_NUMBER() OVER(PARTITION BY PricingTable.numItemCode ORDER BY numPricingID) Id,
															decDiscount
														FROM 
															PricingTable 
														WHERE 
															PricingTable.numItemCode = @numItemCode 
															AND tintRuleType = 3 
													) TEMP
													WHERE
														Id = @tintPriceLevel)
												ELSE
													ISNULL(decDiscount,0)
												END
											END
										) 
				FROM
					PricingTable
				WHERE
					numPriceRuleID=@numPriceRuleID AND @numQty * @fltUOMConversionFactor >= intFromQty AND @numQty * @fltUOMConversionFactor <= intToQty
			END
			ELSE IF @tintPricingMethod = 2 -- BASED ON RULE
			BEGIN
				SET @tintRuleType = @tintPriceRuleType
				SET @monPriceRulePrice = (CASE @tintPriceRuleType WHEN 1 THEN @monListPrice WHEN 2 THEN @monVendorCost END) 

				IF (@tintPriceBookDiscountType = 1 ) -- Percentage 
				BEGIN
					SET @tintDisountType = @tintPriceBookDiscountType
					SET @decDiscount = @decDiscount * (@numQty/@intQntyItems);
					SET @decDiscount = @decDiscount * (CASE WHEN @tintPriceRuleType = 2 THEN @monVendorCost ELSE @monListPrice END) /100;
					SET @decMaxDedPerAmt = @decMaxDedPerAmt * (CASE WHEN @tintPriceRuleType = 2 THEN @monVendorCost ELSE @monListPrice END) /100;
					
					IF (@decDiscount > @decMaxDedPerAmt)
						SET @decDiscount = @decMaxDedPerAmt;
				
					IF ( @tintPriceRuleType = 1 )
							SET @monPriceRuleFinalPrice = (@monListPrice - @decDiscount);
					IF ( @tintPriceRuleType = 2 )
							SET @monPriceRuleFinalPrice = (@monVendorCost + @decDiscount);
				END
				IF ( @tintPriceBookDiscountType = 2 ) -- Flat discount 
				BEGIN
					SET @decDiscount = @decDiscount * ((@numQty * @fltUOMConversionFactor)/@intQntyItems);

					IF (@decDiscount > @decMaxDedPerAmt)
						SET @decDiscount = @decMaxDedPerAmt;
					
					IF ( @tintPriceRuleType = 1 )
						SET @monPriceRuleFinalPrice =  ((@monListPrice * @numQty * @fltUOMConversionFactor) - @decDiscount)/(@numQty * @fltUOMConversionFactor);
					IF ( @tintPriceRuleType = 2 )
						SET @monPriceRuleFinalPrice = ((@monVendorCost * @numQty * @fltUOMConversionFactor) + @decDiscount)/(@numQty * @fltUOMConversionFactor);
				END
			END
		END

		If @monPriceRuleFinalPrice > 0
		BEGIN
			SET @monPrice = @monPriceRuleFinalPrice
		END
		ELSE
		BEGIN
			SET @monPrice = @monVendorCost
		END


		SET @monPriceLevelPrice = 0
		SET @monPriceFinalPrice = 0
		SET @monPriceRulePrice = 0
		SET @monPriceRuleFinalPrice = 0
		SET @monLastPrice = 0
		SET @dtLastOrderDate = NULL
		SET @tintDisountType = 1
		SET @decDiscount =0 
	END

	DECLARE @numTempWarehouseID NUMERIC(18,0)
	SELECT @numTempWarehouseID=numWareHouseID FROM WareHouseItems WHERE numWareHouseItemID=@numWarehouseItemID
	

	SELECT
		@vcItemName AS vcItemName,
		@bitDropship AS bitDropship,
		@chrItemType AS ItemType,
		@bitMatrix AS bitMatrix,
		@numItemClassification AS ItemClassification,
		@numItemGroup AS ItemGroup,
		@ItemDesc AS ItemDescription,
		@bitAssembly AS bitAssembly,
		@bitKit AS bitKitParent,
		@bitHasChildKits AS bitHasChildKits,
		@bitSOWorkOrder AS bitSOWorkOrder,
		case when ISNULL(@bitAssembly,0)=1 then dbo.fn_GetAssemblyPossibleWOQty(@numItemCode,@numTempWarehouseID) else 0 end AS MaxWorkOrderQty,
		@tintPricingBasedOn AS tintPriceBaseOn,
		@monPrice AS monPrice,
		@fltUOMConversionFactor AS UOMConversionFactor,
		(CASE WHEN @tintOppType=1 THEN @monListPrice ELSE @monVendorCost END) AS monListPrice,
		@monVendorCost AS VendorCost,
		@numVendorID AS numVendorID,
		@tintRuleType AS tintRuleType,
		@monPriceLevelPrice AS monPriceLevel,
		@monPriceFinalPrice AS monPriceLevelFinalPrice,
		@monPriceRulePrice AS monPriceRule,
		@monPriceRuleFinalPrice AS monPriceRuleFinalPrice,
		@monLastPrice AS monLastPrice,
		@dtLastOrderDate AS LastOrderDate,
		@tintDisountType AS DiscountType,
		@decDiscount AS Discount,
		@numBaseUnit AS numBaseUnit,
		@numSaleUnit AS numSaleUnit,
		@numPurchaseUnit AS numPurchaseUnit,
		dbo.fn_GetUOMName(@numBaseUnit) AS vcBaseUOMName,
		dbo.fn_GetUOMName(@numSaleUnit) AS vcSaleUOMName,
		@numPurchaseUnit AS numPurchaseUnit,
		@numWarehouseItemID AS numWarehouseItemID,
		dbo.fn_GetItemTransitCount(@numItemcode,@numDomainID) AS OrdersInTransist,
		(SELECT COUNT(*) FROM SimilarItems SI INNER JOIN Item I ON I.numItemCode = SI.numItemCode WHERE SI.numDomainId = @numDomainID AND SI.numParentItemCode = @numItemCode) AS RelatedItemsCount,
		@tintPriceLevel AS tintPriceLevel,
		@vcMinOrderQty AS vcMinOrderQty,
		@vcVendorNotes AS vcVendorNotes,
		@bitCalAmtBasedonDepItems AS bitCalAmtBasedonDepItems,
		@tintKitAssemblyPriceBasedOn AS tintKitAssemblyPriceBasedOn
END 
GO
