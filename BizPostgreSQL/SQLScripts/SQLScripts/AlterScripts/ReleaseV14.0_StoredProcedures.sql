/******************************************************************
Project: Release 14.0 Date: 05.SEP.2020
Comments: STORE PROCEDURES
*******************************************************************/

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='tf'AND NAME ='fn_GetItemPriceBasedOnMethod')
DROP FUNCTION fn_GetItemPriceBasedOnMethod
GO
CREATE FUNCTION [dbo].[fn_GetItemPriceBasedOnMethod] 
(
    @numDomainID NUMERIC(18,0)
	,@numDivisionID NUMERIC(18,0)
	,@numItemCode NUMERIC(18,0)
	,@numQty FLOAT
	,@bitCalculatedPrice BIT
	,@monCalculatedPrice DECIMAL(20,5)
	,@numWarehouseItemID NUMERIC(18,0)
	,@vcSelectedKitChildItems VARCHAR(MAX)
	,@numCurrencyID NUMERIC(18,0)
)
RETURNS @ItemPrice TABLE
(
	monPrice DECIMAL(20,5)
	,tintDisountType TINYINT
	,decDiscount FLOAT
	,monFinalPrice DECIMAL(20,5) 
)
AS 
BEGIN	
	DECLARE @monListPrice DECIMAL(20,5)
	DECLARE @monVendorCost DECIMAL(20,5)
	DECLARE @tintPriceLevel AS TINYINT
	DECLARE @tintRuleType AS TINYINT
	DECLARE @tintDisountType AS TINYINT
	DECLARE @decDiscount AS FLOAT
	DECLARE @monPrice AS DECIMAL(20,5) = NULL
	DECLARE @monFinalPrice AS DECIMAL(20,5) = NULL
	DECLARE @numRelationship NUMERIC(18,0)
	DECLARE @numProfile NUMERIC(18,0)
	DECLARE @numItemClassification AS NUMERIC(18,0)
	DECLARE @tintPriceBookDiscount AS TINYINT
	DECLARE @tintPriceMethod TINYINT
	DECLARE @fltUOMConversionFactor INT = 1
	DECLARE @bitKitParent BIT
	DECLARE @bitAssembly BIT
	DECLARE @bitCalAmtBasedonDepItems BIT
	DECLARE @tintKitAssemblyPriceBasedOn TINYINT
	DECLARE @numBaseCurrency NUMERIC(18,0)
	DECLARE @fltExchangeRate FLOAT

	SELECT 
		@monListPrice = (CASE 
							WHEN ISNULL(@bitCalculatedPrice,0) = 1 
							THEN @monCalculatedPrice 
							ELSE (CASE 
									WHEN EXISTS (SELECT ID FROM ItemCurrencyPrice WHERE ItemCurrencyPrice.numDomainID=@numDomainID AND ItemCurrencyPrice.numItemCode=@numItemCode AND ItemCurrencyPrice.numCurrencyID=ISNULL(@numCurrencyID,0))  
									THEN ISNULL((SELECT monListPrice FROM ItemCurrencyPrice WHERE ItemCurrencyPrice.numDomainID=@numDomainID AND ItemCurrencyPrice.numItemCode=@numItemCode AND ItemCurrencyPrice.numCurrencyID=ISNULL(@numCurrencyID,0)),0)
									ELSE (CASE WHEN @fltExchangeRate <> 1 THEN CAST((ISNULL(Item.monListPrice,0) / @fltExchangeRate) AS INT) ELSE ISNULL(Item.monListPrice,0) END)
								END) 
						END)
		,@numItemClassification=numItemClassification
		,@monVendorCost=((CASE WHEN @fltExchangeRate <> 1 THEN CAST((ISNULL(monCost,0) / @fltExchangeRate) AS INT) ELSE ISNULL(monCost,0) END) * dbo.fn_UOMConversion(Item.numBaseUnit,Item.numItemCode,Item.numDomainID,Item.numPurchaseUnit))
		,@bitKitParent=ISNULL(bitKitParent,0)
		,@bitAssembly=ISNULL(bitAssembly,0)
		,@bitCalAmtBasedonDepItems=ISNULL(bitCalAmtBasedonDepItems,0)
		,@tintKitAssemblyPriceBasedOn=ISNULL(tintKitAssemblyPriceBasedOn,1)
	FROM 
		Item 
	LEFT JOIN
		Vendor
	ON
		Item.numVendorID = Vendor.numVendorID
		AND Item.numItemCode = Vendor.numItemCode
	WHERE
		Item.numItemCode=@numItemCode

	SELECT 
		@numBaseCurrency=ISNULL(numCurrencyID,0)
	FROM 
		Domain 
	WHERE 
		numDomainId=@numDomainID

	IF @numBaseCurrency = @numCurrencyID
	BEGIN
		SET @fltExchangeRate = 1
	END
	ELSE
	BEGIN
		SET @fltExchangeRate = ISNULL((SELECT fltExchangeRate FROM Currency WHERE numDomainID=@numDomainID AND numCurrencyID=@numCurrencyID),1)

		IF ISNULL(@fltExchangeRate,0) = 0
		BEGIN
			SET @fltExchangeRate = 1
		END
	END

	IF @bitCalculatedPrice = 0 AND (ISNULL(@bitKitParent,0)=1 OR ISNULL(@bitAssembly,0) = 1) AND ISNULL(@bitCalAmtBasedonDepItems,0) = 1
	BEGIN
		SELECT
			@monListPrice = monPrice
		FROM
			dbo.fn_GetKitAssemblyCalculatedPrice
			(
				@numDomainID
				,@numDivisionID
				,@numItemCode
				,@numQty
				,@numWarehouseItemID
				,@tintKitAssemblyPriceBasedOn
				,0
				,0
				,@vcSelectedKitChildItems
				,@numCurrencyID
				,@fltExchangeRate
			)    
	END
		

	SELECT 
		@tintPriceMethod=ISNULL(numDefaultSalesPricing,0)
		,@tintPriceBookDiscount=tintPriceBookDiscount 
	FROM 
		Domain 
	WHERE 
		numDomainId=@numDomainID

	-- GET ORGANIZATION DETAL
	SELECT 
		@numRelationship=numCompanyType,
		@numProfile=vcProfile,
		@tintPriceLevel = ISNULL(D.tintPriceLevel,0)
	FROM 
		DivisionMaster D                  
	JOIN 
		CompanyInfo C 
	ON 
		C.numCompanyId=D.numCompanyID                  
	WHERE 
		numDivisionID =@numDivisionID       

	IF @tintPriceMethod= 1 -- PRICE LEVEL
	BEGIN
		IF (SELECT 
				COUNT(*) 
			FROM 
				PricingTable 
			WHERE 
				numItemCode=@numItemCode 
				AND ISNULL(numPriceRuleID,0) = 0 
				AND 1 = (CASE 
							WHEN EXISTS (SELECT * FROM PricingTable WHERE numItemCode=@numItemCode AND ISNULL(numPriceRuleID,0) = 0 AND numCurrencyID=@numCurrencyID)
							THEN (CASE WHEN ISNULL(numCurrencyID,0) = @numCurrencyID THEN 1 ELSE 0 END)
							ELSE (CASE WHEN ISNULL(numCurrencyID,0) = 0 THEN 1 ELSE 0 END)
						END)) > 0
		BEGIN
			SELECT
				@tintDisountType = 1,
				@decDiscount = 0,
				@tintRuleType = tintRuleType,
				@monPrice = (CASE tintRuleType
										WHEN 1 -- Deduct From List Price
										THEN
											CASE tintDiscountType 
												WHEN 1 -- PERCENT
												THEN (CASE WHEN @monListPrice > 0 THEN @monListPrice - (@monListPrice * (ISNULL(decDiscount,0)/100)) ELSE 0 END)
												WHEN 2 -- FLAT AMOUNT
												THEN (CASE WHEN @monListPrice > 0 THEN (CASE WHEN (@monListPrice * @numQty * @fltUOMConversionFactor) - (CASE WHEN @numCurrencyID <> numCurrencyID THEN CAST((ISNULL(decDiscount,0) / @fltExchangeRate) AS INT) ELSE ISNULL(decDiscount,0) END) > 0 THEN ((@monListPrice * @numQty * @fltUOMConversionFactor) - (CASE WHEN @numCurrencyID <> numCurrencyID THEN CAST((ISNULL(decDiscount,0) / @fltExchangeRate) AS INT) ELSE ISNULL(decDiscount,0) END)) / (@numQty * @fltUOMConversionFactor) ELSE 0 END) ELSE 0 END)
												WHEN 3 -- NAMED PRICE
												THEN (CASE WHEN @numCurrencyID <> numCurrencyID THEN CAST((ISNULL(decDiscount,0) / @fltExchangeRate) AS INT) ELSE ISNULL(decDiscount,0) END)
											END
										WHEN 2 -- Add to primary vendor cost
										THEN
											CASE 
												WHEN ISNULL(@bitKitParent,0) = 1
												THEN
													@monListPrice
												ELSE
													CASE tintDiscountType 
														WHEN 1  -- PERCENT
														THEN @monVendorCost + (@monVendorCost * (ISNULL(decDiscount,0)/100)) * @fltUOMConversionFactor
														WHEN 2 -- FLAT AMOUNT
														THEN (CASE WHEN (@monVendorCost * @numQty * @fltUOMConversionFactor) + (CASE WHEN @numCurrencyID <> numCurrencyID THEN CAST((ISNULL(decDiscount,0) / @fltExchangeRate) AS INT) ELSE ISNULL(decDiscount,0) END) > 0 THEN ((@monVendorCost * @numQty * @fltUOMConversionFactor) + (CASE WHEN @numCurrencyID <> numCurrencyID THEN CAST((ISNULL(decDiscount,0) / @fltExchangeRate) AS INT) ELSE ISNULL(decDiscount,0) END)) / (@numQty * @fltUOMConversionFactor)  ELSE 0 END)
														WHEN 3 -- NAMED PRICE
														THEN (CASE WHEN @numCurrencyID <> numCurrencyID THEN CAST((ISNULL(decDiscount,0) / @fltExchangeRate) AS INT) ELSE ISNULL(decDiscount,0) END)
													END
											END
										WHEN 3 -- Named price
										THEN (CASE WHEN @numCurrencyID <> numCurrencyID THEN CAST((ISNULL(decDiscount,0) / @fltExchangeRate) AS INT) ELSE ISNULL(decDiscount,0) END)
										END
									) 
			FROM
			(
				SELECT 
					ROW_NUMBER() OVER(PARTITION BY PricingTable.numItemCode ORDER BY numPricingID) Id,
					*
				FROM
					PricingTable
				WHERE 
					PricingTable.numItemCode = @numItemCode
					AND ISNULL(PricingTable.numPriceRuleID,0) = 0
					AND 1 = (CASE 
								WHEN EXISTS (SELECT * FROM PricingTable WHERE numItemCode=@numItemCode AND ISNULL(numPriceRuleID,0) = 0 AND numCurrencyID=@numCurrencyID)
								THEN (CASE WHEN ISNULL(numCurrencyID,0) = @numCurrencyID THEN 1 ELSE 0 END)
								ELSE (CASE WHEN ISNULL(numCurrencyID,0) = 0 THEN 1 ELSE 0 END)
							END)
			) TEMP
			WHERE
				numItemCode=@numItemCode 
				AND ISNULL(numPriceRuleID,0) = 0 
				AND 1 = (CASE 
							WHEN EXISTS (SELECT * FROM PricingTable WHERE numItemCode=@numItemCode AND ISNULL(numPriceRuleID,0) = 0 AND numCurrencyID=@numCurrencyID)
							THEN (CASE WHEN ISNULL(numCurrencyID,0) = @numCurrencyID THEN 1 ELSE 0 END)
							ELSE (CASE WHEN ISNULL(numCurrencyID,0) = 0 THEN 1 ELSE 0 END)
						END)
				AND 1 = (CASE 
							WHEN ISNULL(@tintPriceLevel,0) > 0 
							THEN (CASE WHEN Id=@tintPriceLevel THEN 1 ELSE 0 END) 
							ELSE (CASE WHEN @numQty * @fltUOMConversionFactor >= intFromQty AND @numQty * @fltUOMConversionFactor <= intToQty THEN 1 ELSE 0 END)
						END)
		END

		INSERT INTO @ItemPrice
		(
			monPrice
			,tintDisountType
			,decDiscount
			,monFinalPrice
		)
		SELECT 
			ISNULL(@monPrice,@monListPrice) AS monPrice,
			1 AS tintDisountType,
			0 AS decDiscount,
			ISNULL(@monPrice,@monListPrice)
	END
	ELSE IF @tintPriceMethod= 2 AND (SELECT COUNT(*) FROM PriceBookRules WHERE numDomainID=@numDomainID AND tintRuleFor=1) > 0 -- PRICE RULE
	BEGIN
		DECLARE @numPriceRuleID NUMERIC(18,0)
		DECLARE @tintPriceRuleType AS TINYINT
		DECLARE @tintPricingMethod TINYINT
		DECLARE @tintPriceBookDiscountType TINYINT
		DECLARE @decPriceBookDiscount FLOAT
		DECLARE @intQntyItems INT
		DECLARE @decMaxDedPerAmt FLOAT

		SELECT 
			TOP 1
			@numPriceRuleID = numPricRuleID,
			@tintPricingMethod = tintPricingMethod,
			@tintPriceRuleType = p.tintRuleType,
			@tintPriceBookDiscountType = P.[tintDiscountType],
			@decPriceBookDiscount = P.[decDiscount],
			@intQntyItems = P.[intQntyItems],
			@decMaxDedPerAmt = P.[decMaxDedPerAmt]
		FROM   
			PriceBookRules P 
		LEFT JOIN 
			PriceBookRuleDTL PDTL 
		ON 
			P.numPricRuleID = PDTL.numRuleID
		LEFT JOIN 
			PriceBookRuleItems PBI 
		ON 
			P.numPricRuleID = PBI.numRuleID
		LEFT JOIN 
			[PriceBookPriorities] PP 
		ON 
			PP.[Step2Value] = P.[tintStep2] 
			AND PP.[Step3Value] = P.[tintStep3]
		WHERE  
			P.numDomainID=@numDomainID 
			AND tintRuleFor=1
			AND (
					((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 1 AND PDTL.numValue = @numDivisionID)) -- Priority 1
					OR ((tintStep2 = 2 AND PBI.numValue = @numItemClassification) AND (tintStep3= 1 AND PDTL.numValue = @numDivisionID)) -- Priority 2
					OR ((tintStep2 = 3) AND (tintStep3 = 1 AND PDTL.numValue = @numDivisionID)) -- Priority 3

					OR ((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 4
					OR ((tintStep2 = 2 AND PBI.numValue = @numItemClassification) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 5
					OR ((tintStep2 = 3) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 6

					OR ((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 3)) -- Priority 7
					OR ((tintStep2 = 2 AND PBI.numValue = @numItemClassification) AND (tintStep3 = 3)) -- Priority 8
					OR ((tintStep2 = 3) and (tintStep3 = 3)) -- Priority 9
				)
		ORDER BY 
			PP.Priority ASC


		IF ISNULL(@numPriceRuleID,0) > 0
		BEGIN
			IF @tintPricingMethod = 1 -- BASED ON PRICE TABLE
			BEGIN
				SELECT
					@tintDisountType = tintDiscountType,
					@decDiscount = ISNULL(decDiscount,0),
					@tintRuleType = tintRuleType,
					@monPrice = (CASE tintRuleType WHEN 1 THEN @monListPrice WHEN 2 THEN @monVendorCost END) ,
					@monFinalPrice = (CASE tintRuleType
											WHEN 1 -- Deduct From List Price
											THEN
												CASE tintDiscountType 
													WHEN 1 -- PERCENT
													THEN (CASE WHEN @monListPrice > 0 THEN @monListPrice - (@monListPrice * (ISNULL(decDiscount,0)/100)) ELSE 0 END)
													WHEN 2 -- FLAT AMOUNT
													THEN (CASE WHEN @monListPrice > 0 THEN (CASE WHEN (@monListPrice * @numQty * @fltUOMConversionFactor) - (CASE WHEN @numCurrencyID <> numCurrencyID THEN CAST((ISNULL(decDiscount,0) / @fltExchangeRate) AS INT) ELSE ISNULL(decDiscount,0) END) > 0 THEN ((@monListPrice * @numQty * @fltUOMConversionFactor) - (CASE WHEN @numCurrencyID <> numCurrencyID THEN CAST((ISNULL(decDiscount,0) / @fltExchangeRate) AS INT) ELSE ISNULL(decDiscount,0) END)) / (@numQty * @fltUOMConversionFactor) ELSE 0 END) ELSE 0 END)
													WHEN 3 -- NAMED PRICE
													THEN (CASE WHEN @numCurrencyID <> numCurrencyID THEN CAST((ISNULL(decDiscount,0) / @fltExchangeRate) AS INT) ELSE ISNULL(decDiscount,0) END)
												END
											WHEN 2 -- Add to primary vendor cost
											THEN
												CASE
												WHEN ISNULL(@bitKitParent,0) = 1
												THEN @monListPrice
												ELSE
													CASE tintDiscountType 
														WHEN 1  -- PERCENT
														THEN @monVendorCost + (@monVendorCost * (ISNULL(decDiscount,0)/100))
														WHEN 2 -- FLAT AMOUNT
														THEN (CASE WHEN (@monVendorCost * @numQty * @fltUOMConversionFactor) + (CASE WHEN @numCurrencyID <> numCurrencyID THEN CAST((ISNULL(decDiscount,0) / @fltExchangeRate) AS INT) ELSE ISNULL(decDiscount,0) END) > 0 THEN ((@monVendorCost * @numQty * @fltUOMConversionFactor) + (CASE WHEN @numCurrencyID <> numCurrencyID THEN CAST((ISNULL(decDiscount,0) / @fltExchangeRate) AS INT) ELSE ISNULL(decDiscount,0) END)) / (@numQty * @fltUOMConversionFactor)  ELSE 0 END)
														WHEN 3 -- NAMED PRICE
														THEN (CASE WHEN @numCurrencyID <> numCurrencyID THEN CAST((ISNULL(decDiscount,0) / @fltExchangeRate) AS INT) ELSE ISNULL(decDiscount,0) END)
													END
												END
											WHEN 3 -- Named price
											THEN (CASE WHEN @numCurrencyID <> numCurrencyID THEN CAST((ISNULL(decDiscount,0) / @fltExchangeRate) AS INT) ELSE ISNULL(decDiscount,0) END)
											END
										) 
				FROM
				(
					SELECT 
						ROW_NUMBER() OVER(PARTITION BY PricingTable.numItemCode ORDER BY numPricingID) Id,
						*
					FROM
						PricingTable
					WHERE 
						PricingTable.numItemCode = @numItemCode
						AND ISNULL(PricingTable.numPriceRuleID,0) = 0
						AND 1 = (CASE 
									WHEN EXISTS (SELECT * FROM PricingTable WHERE numItemCode=@numItemCode AND ISNULL(numPriceRuleID,0) = 0 AND numCurrencyID=@numCurrencyID)
									THEN (CASE WHEN ISNULL(numCurrencyID,0) = @numCurrencyID THEN 1 ELSE 0 END)
									ELSE (CASE WHEN ISNULL(numCurrencyID,0) = 0 THEN 1 ELSE 0 END)
								END)
				) TEMP
				WHERE
					((tintRuleType = 3 AND Id=@tintPriceLevel) OR (tintRuleType <> 3 AND @numQty * @fltUOMConversionFactor >= intFromQty AND @numQty * @fltUOMConversionFactor <= intToQty))
			END
			ELSE IF @tintPricingMethod = 2 -- BASED ON RULE
			BEGIN
				SET @monPrice = (CASE WHEN @tintPriceRuleType = 2 THEN @monVendorCost ELSE @monListPrice END)

				IF (@tintPriceBookDiscountType = 1 ) -- Percentage 
				BEGIN
					SET @tintDisountType = @tintPriceBookDiscountType
					SET @decPriceBookDiscount = @decPriceBookDiscount * (@numQty/@intQntyItems);
					SET @decDiscount = @decPriceBookDiscount * (CASE WHEN @tintPriceRuleType = 2 THEN @monVendorCost ELSE @monListPrice END) /100;
					SET @decMaxDedPerAmt = @decMaxDedPerAmt * (CASE WHEN @tintPriceRuleType = 2 THEN @monVendorCost ELSE @monListPrice END) /100;
					
					IF (@decDiscount > @decMaxDedPerAmt)
						SET @decDiscount = @decMaxDedPerAmt;
				
					IF ( @tintPriceRuleType = 1 )
							SET @monFinalPrice = (@monListPrice - @decDiscount);
					IF ( @tintPriceRuleType = 2 )
							SET @monFinalPrice = (@monVendorCost + @decDiscount);

					SET @monFinalPrice = (CASE WHEN @numBaseCurrency <> @numCurrencyID THEN CAST((ISNULL(@monFinalPrice,0) / @fltExchangeRate) AS INT) ELSE ISNULL(@monFinalPrice,0) END)
				END
				IF ( @tintPriceBookDiscountType = 2 ) -- Flat discount 
				BEGIN
					SET @decDiscount = (CASE WHEN @numBaseCurrency <> @numCurrencyID THEN CAST((ISNULL(@decPriceBookDiscount,0) / @fltExchangeRate) AS INT) ELSE ISNULL(@decPriceBookDiscount,0) END) * ((@numQty * @fltUOMConversionFactor)/@intQntyItems);

					IF (@decDiscount > @decMaxDedPerAmt)
						SET @decDiscount = @decMaxDedPerAmt;
					
					IF ( @tintPriceRuleType = 1 )
						SET @monFinalPrice =  ((@monListPrice * @numQty * @fltUOMConversionFactor) - @decDiscount) / (@numQty * @fltUOMConversionFactor);
						
					IF ( @tintPriceRuleType = 2 )
						SET @monFinalPrice = ((@monVendorCost * @numQty * @fltUOMConversionFactor) + @decDiscount) / (@numQty * @fltUOMConversionFactor);
				END
			END
		END
		
		INSERT INTO @ItemPrice
		(
			monPrice
			,tintDisountType
			,decDiscount
			,monFinalPrice
		)
		SELECT 
			(CASE WHEN @monFinalPrice IS NULL THEN @monListPrice ELSE (CASE WHEN @tintPriceBookDiscount = 1 THEN ISNULL(@monPrice,0) ELSE ISNULL(@monFinalPrice,0) END) END),
			(CASE WHEN @monFinalPrice IS NULL THEN 1 ELSE (CASE WHEN @tintPriceBookDiscount = 1 THEN ISNULL(@tintPriceBookDiscountType,1) ELSE 1 END) END),
			(CASE WHEN @monFinalPrice IS NULL THEN 0 ELSE (CASE WHEN @tintPriceBookDiscount = 1 THEN ISNULL(@decPriceBookDiscount,0) ELSE 0 END) END),
			(CASE WHEN @monFinalPrice IS NULL THEN @monListPrice ELSE ISNULL(@monFinalPrice,0) END)
	END
	ELSE IF @tintPriceMethod = 3 -- Last Price
	BEGIN
		SELECT 
			TOP 1 
			@monPrice = (CASE WHEN @numBaseCurrency <> @numCurrencyID THEN CAST(((ISNULL(monPrice,0) * ISNULL(OpportunityMaster.fltExchangeRate,1)) / @fltExchangeRate) AS INT) ELSE (ISNULL(monPrice,0) * ISNULL(OpportunityMaster.fltExchangeRate,1)) END) 
			,@tintDisountType = ISNULL(OpportunityItems.bitDiscountType,1)
			,@decDiscount = (CASE 
								WHEN ISNULL(OpportunityItems.bitDiscountType,1) = 1 
								THEN (CASE WHEN @numBaseCurrency <> @numCurrencyID THEN CAST(((ISNULL(OpportunityItems.fltDiscount,0) * ISNULL(OpportunityMaster.fltExchangeRate,1)) / @fltExchangeRate) AS INT) ELSE (ISNULL(OpportunityItems.fltDiscount,0) * ISNULL(OpportunityMaster.fltExchangeRate,1)) END) 
								ELSE ISNULL(OpportunityItems.fltDiscount,0) END) 
			,@monFinalPrice = (CASE 
								WHEN ISNULL(OpportunityItems.fltDiscount,0) > 0 AND ISNULL(numUnitHour,0) > 0 
								THEN (CASE 
										WHEN ISNULL(OpportunityItems.bitDiscountType,1)=1 
										THEN (CASE WHEN @numBaseCurrency <> @numCurrencyID THEN CAST(((((ISNULL(monTotAmtBefDiscount,0) - ISNULL(monTotAmount,0))/numUnitHour) * ISNULL(OpportunityMaster.fltExchangeRate,1)) / @fltExchangeRate) AS INT) ELSE (((ISNULL(monTotAmtBefDiscount,0) - ISNULL(monTotAmount,0))/numUnitHour) * ISNULL(OpportunityMaster.fltExchangeRate,1)) END)
										ELSE CAST((CASE WHEN @numBaseCurrency <> @numCurrencyID THEN CAST(((ISNULL(monPrice,0) * ISNULL(OpportunityMaster.fltExchangeRate,1)) / @fltExchangeRate) AS INT) ELSE (ISNULL(monPrice,0) * ISNULL(OpportunityMaster.fltExchangeRate,1)) END)  - ((CASE WHEN @numBaseCurrency <> @numCurrencyID THEN CAST(((ISNULL(monPrice,0) * ISNULL(OpportunityMaster.fltExchangeRate,1)) / @fltExchangeRate) AS INT) ELSE (ISNULL(monPrice,0) * ISNULL(OpportunityMaster.fltExchangeRate,1)) END)  * (ISNULL(OpportunityItems.fltDiscount,0)/100)) AS DECIMAL(20,5)) END) 
								ELSE monPrice 
								END)
			,@tintRuleType = 3
		FROM 
			OpportunityItems 
		JOIN 
			OpportunityMaster 
		ON 
			OpportunityItems.numOppId = OpportunityMaster.numOppId 
		WHERE 
			OpportunityMaster.numDomainId= @numDomainID
			AND tintOppType = 1
			AND tintOppStatus = 1
			AND numItemCode=@numItemCode
		ORDER BY
			OpportunityMaster.numOppId DESC

		INSERT INTO @ItemPrice
		(
			monPrice
			,tintDisountType
			,decDiscount
			,monFinalPrice
		)
		VALUES
		( 
			ISNULL(@monPrice,0),
			@tintDisountType,
			@decDiscount,
			@monFinalPrice
		)
	END
	ELSE
	BEGIN
		INSERT INTO @ItemPrice
		(
			monPrice
			,tintDisountType
			,decDiscount
			,monFinalPrice
		)
		VALUES
		( 
			ISNULL(@monListPrice,0),
			1,
			0,
			ISNULL(@monListPrice,0)
		)
	END

	RETURN
END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='GetOrderItemAvailableQtyToShip')
DROP FUNCTION GetOrderItemAvailableQtyToShip
GO
CREATE FUNCTION [dbo].[GetOrderItemAvailableQtyToShip] 
(	
	@numOppItemCode AS NUMERIC(18,0),
	@numWarehouseItemID NUMERIC(18,0),
	@bitKitParent BIT,
	@tintCommitAllocation TINYINT,
	@bitAllocateInventoryOnPickList BIT
)
RETURNS FLOAT
AS
BEGIN
	DECLARE @numQuantity AS FLOAT = 0
    DECLARE @numAvailableQtyToShip FLOAT = 0
	DECLARE @numOnHand FLOAT
	DECLARE @numAllocation FLOAT
	DECLARE @numQtyShipped FLOAT = 0
	DECLARE @numItemCode NUMERIC(18,0)
	DECLARE @numWarehouseID NUMERIC(18,0)

	SELECT @numQuantity=ISNULL(numUnitHour,0),@numQtyShipped = ISNULL(numQtyShipped,0) FROM OpportunityItems WHERE numoppitemtCode = @numOppItemCode
	SELECT @numItemCode=numItemID,@numWarehouseID = numWareHouseID FROM WareHouseItems WHERE numWareHouseItemID=@numWarehouseItemID

	IF @bitKitParent = 1
	BEGIN
		DECLARE @TEMPTABLE TABLE
		(
			numItemCode NUMERIC(18,0),
			numParentItemID NUMERIC(18,0),
			numOppChildItemID NUMERIC(18,0),
			vcItemName VARCHAR(300),
			bitKitParent BIT,
			numWarehouseItmsID NUMERIC(18,0),
			numQtyItemsReq FLOAT,
			numQtyItemReq_Orig FLOAT,
			numOnHand FLOAT,
			numAllocation FLOAT,
			numAvailableQtyToShip FLOAT,
			numWOID NUMERIC(18,0)
		)

		INSERT INTO 
			@TEMPTABLE
		SELECT
			OKI.numChildItemID,
			0,
			OKI.numOppChildItemID,
			I.vcItemName,
			I.bitKitParent,
			OKI.numWareHouseItemId,
			OKI.numQtyItemsReq_Orig * (ISNULL(OI.numUnitHour,0) - ISNULL(OI.numQtyShipped,0)),
			OKI.numQtyItemsReq_Orig,
			ISNULL(WI.numOnHand,0),
			ISNULL(WI.numAllocation,0) + ISNULL((SELECT 
														SUM(ISNULL(WIInner.numOnHand,0) + ISNULL(WIInner.numAllocation,0))
													FROM 
														WareHouseItems WIInner
													WHERE 
														WIInner.numItemID=WI.numItemID 
														AND WIInner.numWareHouseID = WI.numWareHouseID
														AND WIInner.numWareHouseItemID <> WI.numWareHouseItemID),0),
			0,
			ISNULL((SELECT numWOId FROM WorkOrder WHERE numOppId=OKi.numOppId AND numOppItemID=OKi.numOppItemID AND numOppChildItemID=OKI.numOppChildItemID AND ISNULL(numOppKitChildItemID,0) = 0 AND ISNULL(numParentWOID,0)=0),0)
		FROM
			OpportunityKitItems OKI
		INNER JOIN
			WareHouseItems WI
		ON
			OKI.numWareHouseItemId = WI.numWareHouseItemID
		INNER JOIN
			OpportunityItems OI
		ON
			OKI.numOppItemID = OI.numoppitemtCode
		INNER JOIN
			Item I
		ON
			OKI.numChildItemID = I.numItemCode
		WHERE
			OI.numoppitemtCode = @numOppItemCode  

		INSERT INTO
			@TEMPTABLE
		SELECT
			OKCI.numItemID,
			t1.numItemCode,
			t1.numOppChildItemID,
			I.vcItemName,
			I.bitKitParent,
			OKCI.numWareHouseItemId,
			OKCI.numQtyItemsReq_Orig * t1.numQtyItemsReq,
			OKCI.numQtyItemsReq_Orig,
			ISNULL(WI.numOnHand,0),
			ISNULL(WI.numAllocation,0) + ISNULL((SELECT 
														SUM(ISNULL(WIInner.numOnHand,0) + ISNULL(WIInner.numAllocation,0))
													FROM 
														WareHouseItems WIInner
													WHERE 
														WIInner.numItemID=WI.numItemID 
														AND WIInner.numWareHouseID = WI.numWareHouseID
														AND WIInner.numWareHouseItemID <> WI.numWareHouseItemID),0),
			0,
			ISNULL((SELECT numWOId FROM WorkOrder WHERE numOppId=OKCI.numOppId AND numOppItemID=OKCI.numOppItemID AND numOppChildItemID=OKCI.numOppChildItemID AND numOppKitChildItemID = OKCI.numOppKitChildItemID AND ISNULL(numParentWOID,0)=0),0)
		FROM
			OpportunityKitChildItems OKCI
		INNER JOIN
			WareHouseItems WI
		ON
			OKCI.numWareHouseItemId = WI.numWareHouseItemID
		INNER JOIN
			@TEMPTABLE t1
		ON
			OKCI.numOppChildItemID = t1.numOppChildItemID
		INNER JOIN
			Item I
		ON
			OKCI.numItemID = I.numItemCode
		
		-- FIRST GET SUB KIT BACK ORDER NUMBER
		IF @tintCommitAllocation=2 AND ISNULL(@bitAllocateInventoryOnPickList,1)=0
		BEGIN
			UPDATE 
				@TEMPTABLE 
			SET 
				numAvailableQtyToShip = (CASE 
											WHEN ISNULL(numWOID,0) > 0
											THEN
												(CASE 
													WHEN ISNULL(numOnHand,0) = 0 THEN 0
													WHEN  dbo.GetWorkOrderQtyReadyToBuild(numWOID,numQtyItemsReq)>= numQtyItemsReq THEN (numQtyItemsReq/(CASE WHEN ISNULL(numQtyItemReq_Orig,0) > 0 THEN numQtyItemReq_Orig ELSE 1 END))
													ELSE FLOOR(dbo.GetWorkOrderQtyReadyToBuild(numWOID,numQtyItemsReq)/(CASE WHEN ISNULL(numQtyItemReq_Orig,0) > 0 THEN numQtyItemReq_Orig ELSE 1 END))
												END)
											ELSE
												(CASE 
													WHEN ISNULL(numOnHand,0) = 0 THEN 0
													WHEN  numOnHand>= numQtyItemsReq THEN (numQtyItemsReq/(CASE WHEN ISNULL(numQtyItemReq_Orig,0) > 0 THEN numQtyItemReq_Orig ELSE 1 END))
													ELSE FLOOR(ISNULL(numOnHand,0)/(CASE WHEN ISNULL(numQtyItemReq_Orig,0) > 0 THEN numQtyItemReq_Orig ELSE 1 END))
												END)
										END)
			WHERE
				bitKitParent = 0
		END
		ELSE
		BEGIN
			UPDATE 
				@TEMPTABLE 
			SET 
				numAvailableQtyToShip = (CASE 
											WHEN ISNULL(numWOID,0) > 0
											THEN
												(CASE 
													WHEN ISNULL(numOnHand,0) = 0 THEN 0
													WHEN  dbo.GetWorkOrderQtyReadyToBuild(numWOID,numQtyItemsReq)>= numQtyItemsReq THEN (numQtyItemsReq/(CASE WHEN ISNULL(numQtyItemReq_Orig,0) > 0 THEN numQtyItemReq_Orig ELSE 1 END))
													ELSE FLOOR(dbo.GetWorkOrderQtyReadyToBuild(numWOID,numQtyItemsReq)/(CASE WHEN ISNULL(numQtyItemReq_Orig,0) > 0 THEN numQtyItemReq_Orig ELSE 1 END))
												END)
											ELSE
												(CASE 
													WHEN ISNULL(numAllocation,0) = 0 THEN 0
													WHEN  numAllocation>= numQtyItemsReq THEN (numQtyItemsReq/(CASE WHEN ISNULL(numQtyItemReq_Orig,0) > 0 THEN numQtyItemReq_Orig ELSE 1 END))
													ELSE FLOOR(ISNULL(numAllocation,0)/(CASE WHEN ISNULL(numQtyItemReq_Orig,0) > 0 THEN numQtyItemReq_Orig ELSE 1 END))
												END)
										END)
			WHERE
				bitKitParent = 0
		END

		
		-- NOW USE SUB KIT BACK ORDER NUMBER TO GENERATE PARENT KIT BACK ORDER NUMBER
		UPDATE 
			t2
		SET 
			t2.numAvailableQtyToShip = CEILING((SELECT MAX(numAvailableQtyToShip) FROM @TEMPTABLE t1 WHERE t1.numParentItemID = t2.numItemCode) / (CASE WHEN ISNULL(t2.numQtyItemReq_Orig,0) > 0 THEN t2.numQtyItemReq_Orig ELSE 1 END))
		FROM
			@TEMPTABLE t2
		WHERE
			bitKitParent = 1

		IF (SELECT COUNT(*) FROM @TEMPTABLE) > 0
		BEGIN
			SELECT @numAvailableQtyToShip = MIN(numAvailableQtyToShip) FROM @TEMPTABLE WHERE ISNULL(numParentItemID,0) = 0
		END
		ELSE
		BEGIN
			-- KIT WITH ALL CHILD ITEMS OF TYPE NON INVENTORY OR SERVICE ITEMS
			SET @numAvailableQtyToShip = @numQuantity
		END
	END
	ELSE
	BEGIN
		IF @tintCommitAllocation=2 AND ISNULL(@bitAllocateInventoryOnPickList,1)=0
		BEGIN
			SELECT
				@numOnHand = SUM(numOnHand)
			FROM
				WareHouseItems 
			WHERE
				numItemID=@numItemCode 
				AND numWareHouseID = @numWarehouseID

			IF (@numQuantity - @numQtyShipped) >= @numOnHand
				SET @numAvailableQtyToShip = @numOnHand
			ELSE
				SET @numAvailableQtyToShip = (@numQuantity - @numQtyShipped)
		END
		ELSE
		BEGIN
			SELECT
				@numAllocation = numAllocation
			FROM
				WareHouseItems 
			WHERE
				numWareHouseItemID = @numWarehouseItemID

		SET @numAllocation = ISNULL(@numAllocation,0) + ISNULL((SELECT 
															SUM(ISNULL(WareHouseItems.numOnHand,0) + ISNULL(WareHouseItems.numAllocation,0))
														FROM 
															WareHouseItems 
														WHERE 
															numItemID=@numItemCode 
															AND numWareHouseID = @numWarehouseID
															AND numWareHouseItemID <> @numWarehouseItemID),0)

			IF (@numQuantity - @numQtyShipped) >= @numAllocation
				SET @numAvailableQtyToShip = @numAllocation
			ELSE
				SET @numAvailableQtyToShip = (@numQuantity - @numQtyShipped)
		END
	END

	RETURN @numAvailableQtyToShip
END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='GetUnitPriceAfterPriceLevelApplication')
DROP FUNCTION GetUnitPriceAfterPriceLevelApplication
GO
CREATE FUNCTION [dbo].[GetUnitPriceAfterPriceLevelApplication]
(
	@numDomainID numeric(18,0), 
	@numItemCode NUMERIC(18,0), 
	@numQuantity FLOAT, 
	@numWarehouseItemID NUMERIC(18,0), 
	@isPriceRule BIT, 
	@numPriceRuleID NUMERIC(18,0),
	@numDivisionID NUMERIC(18,0)
)  
RETURNS FLOAT  
AS  
BEGIN  

DECLARE @finalUnitPrice FLOAT = 0
DECLARE @tintRuleType INT
DECLARE @tintDiscountType INT
DECLARE @decDiscount FLOAT
DECLARE @ItemPrice FLOAT

SET @tintRuleType = 0
SET @tintDiscountType = 0
SET @decDiscount  = 0
SET @ItemPrice = 0

DECLARE @TEMPPrice TABLE
(
	bitSuccess BIT
	,monPrice DECIMAL(20,5)
)

DECLARE @monCalculatePrice AS DECIMAL(20,5)
DECLARE @tintPriceLevel INT = 0
SELECT @tintPriceLevel=ISNULL(tintPriceLevel,0) FROM DivisionMaster WHERE numDivisionID=@numDivisionID

If (SELECT COUNT(*) FROM Item WHERE numDomainID = @numDomainID AND numItemCode = @numItemCode AND (bitKitParent = 1 OR bitAssembly = 1)) > 0
BEGIN
	INSERT INTO @TEMPPrice
	(
		bitSuccess
		,monPrice
	)
	SELECT
		bitSuccess
		,monPrice
	FROM
		dbo.fn_GetKitAssemblyCalculatedPrice(@numDomainID,@numDivisionID,@numItemCode,@numQuantity,@numWarehouseItemID,(SELECT ISNULL(tintKitAssemblyPriceBasedOn,1) FROM Item WHERE numDomainID = @numDomainID AND numItemCode = @numItemCode),0,0,'',0,1)

	IF (SELECT bitSuccess FROM @TEMPPrice) = 1
	BEGIN
		SET @monCalculatePrice = (SELECT monPrice FROM @TEMPPrice)
	END
END

IF @isPriceRule = 1
BEGIN
	SELECT TOP 1
		@tintRuleType = tintRuleType,
		@tintDiscountType = tintDiscountType,
		@decDiscount = decDiscount
	FROM 
		PricingTable 
	WHERE 
		numPriceRuleID = @numPriceRuleID 
		AND ISNULL(numCurrencyID,0) = 0
		AND (@numQuantity BETWEEN intFromQty AND intToQty)
END
ELSE
BEGIN
	IF ISNULL(@tintPriceLevel,0) > 0
	BEGIN
		SELECT
			@tintRuleType = tintRuleType,
			@tintDiscountType = tintDiscountType,
			@decDiscount = decDiscount
		FROM
		(
			SELECT 
				ROW_NUMBER() OVER(PARTITION BY PricingTable.numItemCode ORDER BY numPricingID) Id,
				tintRuleType,
				tintDiscountType,
				decDiscount
			FROM 
				PricingTable 
			WHERE 
				PricingTable.numItemCode = @numItemCode
				AND ISNULL(numCurrencyID,0) = 0
		) TEMP
		WHERE
			Id = @tintPriceLevel
	END
	ELSE
	BEGIN
		SELECT TOP 1
			@tintRuleType = tintRuleType,
			@tintDiscountType = tintDiscountType,
			@decDiscount = decDiscount
		FROM 
			PricingTable 
		WHERE 
			numItemCode = @numItemCode 
			AND ISNULL(numCurrencyID,0) = 0
			AND ((@numQuantity BETWEEN intFromQty AND intToQty) OR tintRuleType = 3)
	END
END

	

IF @tintRuleType > 0 AND @tintDiscountType > 0
BEGIN
	IF @tintRuleType = 1 -- Deduct from List price
	BEGIN
		IF (SELECT ISNULL(charItemType,'') FROM Item WHERE numDomainID = @numDomainID AND numItemCode = @numItemCode) = 'P'
			If (SELECT COUNT(*) FROM Item WHERE numDomainID = @numDomainID AND numItemCode = @numItemCode AND (bitKitParent = 1 OR bitAssembly = 1)) > 0 -- Kit OR Assembly
				SELECT @ItemPrice = @monCalculatePrice
			ELSE
				SELECT @ItemPrice = ISNULL(monWListPrice,0) FROM WareHouseItems WHERE numDomainID = @numDomainID AND numItemID = @numItemCode AND numWareHouseItemID = @numWarehouseItemID
		ELSE
			SELECT @ItemPrice = ISNULL(monListPrice,0) FROM Item WHERE numDomainID = @numDomainID AND numItemCode = @numItemCode

		IF @tintDiscountType = 1 -- Percentage
		BEGIN
			SELECT @finalUnitPrice = @ItemPrice - (@ItemPrice * (@decDiscount/100))
		END
		ELSE IF @tintDiscountType = 2 -- Flat Amount
		BEGIN
				SELECT @finalUnitPrice = @ItemPrice - @decDiscount
		END
	END
	ELSE IF @tintRuleType = 2 -- Add to Primary Vendor Cost
	BEGIN
		If (SELECT COUNT(*) FROM Item WHERE numDomainID = @numDomainID AND numItemCode = @numItemCode AND (bitKitParent = 1 OR bitAssembly = 1)) > 0 -- Kit OR Assembly
			SELECT @ItemPrice = @monCalculatePrice
		ELSE
			SELECT @ItemPrice = ISNULL(monCost,0) FROM Vendor WHERE numItemCode = @numItemCode AND numVendorID = (SELECT numVendorID FROM Item WHERE numItemCode = @numItemCode)

		IF @tintDiscountType = 1 -- Percentage
		BEGIN
			SELECT @finalUnitPrice = @ItemPrice + (@ItemPrice * (@decDiscount/100))
		END
		ELSE IF @tintDiscountType = 2 -- Flat Amount
		BEGIN
			SELECT @finalUnitPrice = @ItemPrice + @decDiscount
		END
	END
	ELSE IF @tintRuleType = 3 -- Named Price
	BEGIN
		SELECT @finalUnitPrice = @decDiscount
	END
END

return @finalUnitPrice
END
GO

GO
/****** Object:  StoredProcedure [dbo].[usp_GenDocList]    Script Date: 02/16/2010 00:39:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Anoop Jayaraj                  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_gendoclist')
DROP PROCEDURE usp_gendoclist
GO
CREATE PROCEDURE [dbo].[usp_GenDocList]                    
(
	@numDomainID NUMERIC(18,0)=0,           
	@numUserCntID NUMERIC(18,0)=0,	                   
	@tintUserRightType TINYINT=0,
	@CurrentPage INT,
	@PageSize INT,
	@columnName VARCHAR(50),
	@columnSortOrder VARCHAR(10),
	@DocCategory NUMERIC(18,0)=0,
	@numDocStatus NUMERIC(18,0)=0,
	@ClientTimeZoneOffset INT,
	@numGenericDocId NUMERIC(18,0) = 0,
	@SortChar char(1)='0'
)                   
AS
BEGIN  
	IF @columnSortOrder <> 'Desc' AND @columnSortOrder <> 'Asc'
	BEGIN
		SET @columnSortOrder = 'Asc'
	END

	SELECT 
		COUNT(*) OVER() AS numTotalRecords,
		numGenericDocId,                    
		VcFileName ,                    
		vcDocName ,                  
		numDocCategory,                    
		(CASE numDocCategory 
			WHEN 369 THEN CONCAT('Email Template',' <font color=green>(',ISNULL(EMM.vcModuleName,'-'),')</font> ')
			ELSE dbo.fn_GetListItemName(numDocCategory) 
		END) Category,
		vcfiletype,                    
		cUrlType,                    
		dbo.fn_GetListItemName(numDocStatus) as BusClass,                    
		dbo.fn_GetContactName(numModifiedBy)+','+ convert(varchar(20),dateadd(minute,-@ClientTimeZoneOffset,bintModifiedDate)) as [Mod]
	FROM 
		GenericDocuments 
	LEFT JOIN 
		EmailMergeModule EMM 
	ON 
		EMM.numModuleID = GenericDocuments.numModuleID 
		AND EMM.tintModuleType=0 
	WHERE 
		numDomainID = @numDomainID
		AND (ISNULL(@numGenericDocId,0) = 0 OR numGenericDocID=@numGenericDocId)
		AND tintDocumentType=1
		AND (ISNULL(@numDocStatus,0) = 0 OR numDocStatus=@numDocStatus)
		AND (ISNULL(@DocCategory,0) = 0 OR numDocCategory=@DocCategory)
		AND 1 =(CASE WHEN @tintUserRightType=1 THEN (CASE WHEN numCreatedby = @numUserCntID THEN 1 ELSE 0 END) ELSE 1 END)
		AND 1 = (CASE WHEN @SortChar <> '0' THEN (CASE WHEN vcDocName like CONCAT(@SortChar,'%') THEN 1 ELSE 0 END) ELSE 1 END)
	ORDER BY
		CASE @columnSortOrder WHEN 'Desc' THEN
			CASE @ColumnName
			   WHEN 'vcDocName' THEN vcDocName
			   WHEN 'Category' THEN (CASE numDocCategory 
										WHEN 369 THEN CONCAT('Email Template',' <font color=green>(',ISNULL(EMM.vcModuleName,'-'),')</font> ')
										ELSE dbo.fn_GetListItemName(numDocCategory) 
									END)
			   WHEN 'vcfiletype' THEN vcfiletype
			   WHEN 'BusClass' THEN dbo.fn_GetListItemName(numDocStatus)
			   WHEN 'Mod' THEN bintModifiedDate
			   ELSE bintcreateddate
			END
		END DESC,
		CASE @columnSortOrder WHEN 'Asc' THEN
			CASE @ColumnName
			   WHEN 'vcDocName' THEN vcDocName
			   WHEN 'Category' THEN (CASE numDocCategory 
										WHEN 369 THEN CONCAT('Email Template',' <font color=green>(',ISNULL(EMM.vcModuleName,'-'),')</font> ')
										ELSE dbo.fn_GetListItemName(numDocCategory) 
									END)
			   WHEN 'vcfiletype' THEN vcfiletype
			   WHEN 'BusClass' THEN dbo.fn_GetListItemName(numDocStatus)
			   WHEN 'Mod' THEN bintModifiedDate
			   ELSE bintcreateddate
			END
		END ASC
	OFFSET (@CurrentPage-1) * @PageSize ROWS FETCH NEXT @PageSize ROWS ONLY		
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GenericDocuments_GetByFileName')
DROP PROCEDURE USP_GenericDocuments_GetByFileName
GO
CREATE PROCEDURE [dbo].[USP_GenericDocuments_GetByFileName]                          
(                          
	@numDomainID NUMERIC(18,0)
	,@VcFileName VARCHAR(200)
)                          
as 
BEGIN
	SELECT
		*
	FROM
		GenericDocuments
	WHERE
		numDomainID=@numDomainID
		AND VcFileName = @VcFileName
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GenericDocuments_Search')
DROP PROCEDURE USP_GenericDocuments_Search
GO
CREATE PROCEDURE [dbo].[USP_GenericDocuments_Search]                          
(                          
	@numDomainID NUMERIC(18,0)
	,@numDocCategory NUMERIC(18,0)
	,@tintDocType TINYINT
	,@vcSearchText VARCHAR(100)
	,@numPageIndex INT
	,@numPageSize INT
)                          
as 
BEGIN
	SELECT
		COUNT(*) OVER() AS numTotalRecords
		,numGenericDocID AS id
		,vcDocName AS [text]
		,ISNULL(EMM.vcModuleName,'-') vcModuleName
	FROM
		GenericDocuments
	LEFT JOIN 
		EmailMergeModule EMM 
	ON 
		EMM.numModuleID = GenericDocuments.numModuleID 
		AND EMM.tintModuleType=0
	WHERE 
		numDomainID = @numDomainID
		AND tintDocumentType=1
		AND (ISNULL(@numDocCategory,0) = 0 OR numDocCategory=@numDocCategory)
		AND 1 = (CASE @tintDocType 
					WHEN 2 THEN (CASE WHEN VcFileName LIKE '#SYS#%' THEN 1 ELSE 0 END)
					WHEN 1 THEN (CASE WHEN VcFileName NOT LIKE '#SYS#%' THEN 1 ELSE 0 END) 
					ELSE 1 
				END)
		AND vcDocName LIKE CONCAT('%',@vcSearchText,'%')
	ORDER BY
		vcDocName
	OFFSET 
		((@numPageIndex -1) * @numPageSize) ROWS 
	FETCH NEXT @numPageSize ROWS ONLY
END
GO
/****** Object:  StoredProcedure [dbo].[Usp_getCorrespondance]    Script Date: 07/26/2008 16:17:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcorrespondance')
DROP PROCEDURE usp_getcorrespondance
GO
CREATE PROCEDURE [dbo].[usp_getcorrespondance]                          
@FromDate as datetime,                          
@toDate as datetime,                          
@numContactId as numeric,                          
@vcMessageFrom as varchar(200)='' ,                        
@tintSortOrder as numeric,                        
@SeachKeyword as varchar(100),                        
@CurrentPage as INT,                        
@PageSize as INT,                        
@TotRecs as numeric output,                        
@columnName as varchar(100),                        
@columnSortOrder as varchar(50),                        
@filter as numeric,                        
@numdivisionId as numeric =0,              
@numDomainID as numeric(9)=0,              
@numCaseID as numeric(9)=0,              
@ClientTimeZoneOffset INT,
@numOpenRecordID NUMERIC=0,
@tintMode TINYINT=0,
@bitOpenCommu BIT=0,
@numUserCntID NUMERIC=0,
@vcTypes VARCHAR(200)
as 
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
DECLARE @vcUserLoginEmail AS VARCHAR(500)=''
SET @vcUserLoginEmail = (SELECT TOP 1 replace(vcEmailID,'''','''''') FROM UserMaster where numUserDetailId=@numUserCntID)
set @vcMessageFrom=replace(@vcMessageFrom,'''','|')
                         
if @filter>0 set @tintSortOrder=  @filter
              
if ((@filter <=2 and @tintSortOrder<=2) or (@tintSortOrder>2 and @filter>2))                 
begin  
                                                                  
 declare @strSql as nvarchar(MAX)  =''     
                                  
  declare @firstRec as integer                                                        
  declare @lastRec as integer                                                        
  set @firstRec= (@CurrentPage-1) * @PageSize                                                        
  set @lastRec= (@CurrentPage*@PageSize+1)                 
  set @strSql=''                

 if (@filter <=2 and @tintSortOrder<=2 and @bitOpenCommu=0)                 
 begin  
 
 declare @strCondition as varchar(MAX);
 declare @vcContactEmail as varchar(MAX)='';
              
  if @numdivisionId =0 and  @vcMessageFrom <> '' 
 BEGIN 
	If @filter = 1 --ReceivedMessages
		set  @strCondition=' and (vcFrom like ''%'+ REPLACE(@vcMessageFrom,'''','''''')+'%'')  ' 
	ELSE IF @filter=2 --SentMessages
		set  @strCondition=' and (vcTo like ''%'+ REPLACE(@vcMessageFrom,'''','''''')+'%'')  ' 
	ELSE
		set  @strCondition=' and (vcFrom like ''%'+ REPLACE(@vcMessageFrom,'''','''''')+'%'' or vcTo like ''%'+ REPLACE(@vcMessageFrom,'''','''''')+'%'')  ' 
 END
  else if @numdivisionId <>0 
 BEGIN

 /* This approach does not work as numEmailId stores who sent email, id of sender
  set @strCondition=' and HDR.numEmailID in (
  SELECT numEmailID FROM dbo.EmailMaster WHERE numDomainID= ' + CONVERT(VARCHAR(15),@numDomainID) +' AND numContactID 
  IN (SELECT numcontactid FROM dbo.AdditionalContactsInformation WHERE numDivisionID = ' + convert(varchar(15),@numDivisionID) + ' AND numDomainID = ' + CONVERT(VARCHAR(15),@numDomainID) +' ))';
*/
-- bug fix id 1591,1628 
  DECLARE @TnumContactID NUMERIC(9);set @strCondition=''

  select TOP 1 @TnumContactID=numContactID,@vcContactEmail=replace(vcEmail,'''','''''') from AdditionalContactsInformation where numdivisionid=@numdivisionId and len(vcEmail)>2
  ORDER BY numContactID ASC
  SET @vcContactEmail = ISNULL(@vcContactEmail,'')
  WHILE @TnumContactID>0
  BEGIN
   if len(@strCondition)>0
    SET @strCondition=@strCondition + ' or '

	If @filter = 1 --ReceivedMessages
		set  @strCondition = @strCondition + ' (vcFrom like ''%'+ REPLACE(@vcContactEmail,'''','''''') +'%'')  ' 
	ELSE IF @filter=2 --SentMessages
		set  @strCondition= @strCondition + ' (vcTo like ''%'+ REPLACE(@vcContactEmail,'''','''''') +'%'')  ' 
	ELSE
		set  @strCondition= @strCondition + ' (vcFrom like ''%'+ REPLACE(@vcContactEmail,'''','''''')+'%'' or vcTo like ''%'+ REPLACE(@vcContactEmail,'''','''''')+'%'')  '

   select TOP 1 @TnumContactID=numContactID,@vcContactEmail=vcEmail from AdditionalContactsInformation where numdivisionid=@numdivisionId and len(vcEmail)>2 and numContactID>@TnumContactID
   ORDER BY numContactID ASC

   IF @@rowcount = 0 SET @TnumContactID = 0 
  END
 
  if len(@strCondition)>0
   SET @strCondition= 'and (' + @strCondition + ')'
  else if @tintMode <> 6 AND @tintMode <> 5
   SET @strCondition=' and (1=0)' 
  --set @strCondition=' and DTL.numEmailId  in (select numEmailId from EmailMaster where vcEmailID in (select vcEmail  from AdditionalContactsInformation where numdivisionid='''+ convert(varchar(15),@numdivisionId)+''' ))    '          
  END
  else if @numContactId>0 
 BEGIN
  select @vcContactEmail=isnull(replace(vcEmail,'''',''''''),'') from AdditionalContactsInformation where numContactID=@numContactId and len(vcEmail)>2
  SET @vcContactEmail = ISNULL(@vcContactEmail,'')
  if len(@vcContactEmail)>0
	If @filter = 1 --ReceivedMessages
		set  @strCondition=' and (vcFrom like ''%'+ REPLACE(@vcContactEmail,'''','''''') +'%'')  ' 
	ELSE IF @filter=2 --SentMessages
		set  @strCondition=' and (vcTo like ''%'+ REPLACE(@vcContactEmail,'''','''''')+'%'')  ' 
	ELSE
		set  @strCondition=' and (vcFrom like ''%'+ REPLACE(@vcContactEmail,'''','''''')+'%'' or vcTo like ''%'+ REPLACE(@vcContactEmail,'''','''''')+'%'')' 
  else if @tintMode <> 6 AND @tintMode <> 5
   SET @strCondition=' and (1=0)'
 END
 else 
  SET @strCondition=''

  CREATE TABLE #TEMPData
  (
		numTotalRecords INT
		,tintCountType TINYINT
		,numEmailHstrID NUMERIC(18,0)
		,DelData VARCHAR(200)
		,bintCreatedOn DATETIME
		,[date] VARCHAR(100)
		,[Subject] VARCHAR(MAX)
		,[type] VARCHAR(100)
		,phone VARCHAR(100)
		,assignedto VARCHAR(100)
		,caseid NUMERIC(18,0)
		,vcCasenumber VARCHAR(200)
		,caseTimeid NUMERIC(18,0)
		,caseExpid NUMERIC(18,0)
		,tinttype NUMERIC(18,0)
		,dtCreatedDate DATETIME
		,[From] VARCHAR(MAX)
		,bitClosedflag BIT
		,bitHasAttachments BIT
		,vcBody VARCHAR(MAX)
		,InlineEdit VARCHAR(1000)
		,numCreatedBy NUMERIC(18,0)
		,numOrgTerId NUMERIC(18,0)
		,TypeClass VARCHAR(100)
  )


  set @strSql= 'INSERT INTO 
					#TEMPData 
				SELECT 
					COUNT(*) OVER() AS numTotalRecords
					,1 AS tintCountType
					,HDR.numEmailHstrID
					,convert(varchar(15),HDR.numEmailHstrID)+''~1~0''+convert(varchar(15),HDR.numUserCntId) as DelData
					,HDR.dtReceivedOn AS bintCreatedOn
					,dbo.FormatedDateTimeFromDate(DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+', dtReceivedOn),'+convert(varchar(15),@numDomainID)+') as [date]
					,vcSubject as [Subject]
					,CASE WHEN vcFrom like ''%'+CAST(REPLACE(@vcUserLoginEmail,'''','''''') AS VARCHAR(300))+'%'' THEN ''Email Out'' ELSE ''Email In'' END as [type]
					,'''' as phone
					,'''' as assignedto
					,0 as caseid
					,null as vcCasenumber
					,0 as caseTimeid
					,0 as caseExpid
					,hdr.tinttype
					,HDR.dtReceivedOn as dtCreatedDate
					,vcFrom + '','' + vcTo as [From]
					,0 as bitClosedflag
					,ISNULL(HDR.bitHasAttachments,0) bitHasAttachments
					,CASE WHEN LEN(Cast(vcBodyText AS VARCHAR(1000)))>150 THEN SUBSTRING(vcBodyText,0,150) + ''...'' ELSE Cast(vcBodyText AS VARCHAR(1000)) END AS vcBody
					,''''  AS InlineEdit
					,HDR.numUserCntId AS numCreatedBy
					,0 AS numOrgTerId
					,CASE WHEN vcFrom like ''%'+CAST(REPLACE(@vcUserLoginEmail,'''','''''') AS VARCHAR(300))+'%'' THEN ''redColorLabel'' ELSE ''greenColorLabel'' END as TypeClass
			  FROM EmailHistory HDR                                 
			  LEFT JOIN [Correspondence] CR ON CR.numEmailHistoryID = HDR.numEmailHstrID ' 
     

   set  @strSql=@strSql +' where ISNULL(HDR.bitInvisible,0) = 0 AND HDR.numDomainID='+convert(varchar(15),@numDomainID)
   + ' AND (CR.numOpenRecordID = '+ convert(varchar(15),@numOpenRecordID) +' OR '+ convert(varchar(15),@numOpenRecordID) +'=0)'
   + (CASE WHEN ISNULL(@vcTypes,'') <> '' THEN ' AND 1 = 0' ELSE ' AND 1 = 1' END)
   
   IF(@FromDate IS NOT NULL AND @FromDate<>'')
   BEGIN
  SET @strSql = @strSql +' and  (dtReceivedOn between '''+ convert(varchar(30),@FromDate)+''' and '''+ convert(varchar(30),@ToDate)+''') '
  END
 set  @strSql=@strSql+ @strCondition


  IF @tintMode=1 SET @strSql=@strSql+ ' and  (CR.tintCorrType IN(1,3) OR '+ convert(varchar(15),@numOpenRecordID) +'=0)'
  IF @tintMode=2 SET @strSql=@strSql+ ' and  (CR.tintCorrType IN(2,4) OR '+ convert(varchar(15),@numOpenRecordID) +'=0)'
  IF @tintMode=5 SET @strSql=@strSql+ ' and  (CR.tintCorrType IN(5) OR '+ convert(varchar(15),@numOpenRecordID) +'=0)'
  IF @tintMode=6 SET @strSql=@strSql+ ' and  (CR.tintCorrType IN(6) OR '+ convert(varchar(15),@numOpenRecordID) +'=0)'
  
  if @filter =0 set  @strSql=@strSql+ ' and (hdr.tinttype=1 or hdr.tinttype=2 or  hdr.tinttype=5)'                  
  else if @filter =1 OR @filter =2 set  @strSql=@strSql+ ' and (hdr.tinttype=1 OR hdr.tinttype=2)'                                             
  if @filter > 2 set  @strSql=@strSql+ ' and hdr.tinttype=0'                 
                        
  if @tintSortOrder =0 and @SeachKeyword <> ''                 
  begin                
   set  @strSql=@strSql+ ' and (vcFrom like ''%'+ @SeachKeyword+'%'' or vcTo like ''%'+ @SeachKeyword+'%'' or vcsubject like ''%'+@SeachKeyword+'%'' or vcBody like ''%'+@SeachKeyword+'%'')'               
  end                
  if @tintSortOrder =1 and @SeachKeyword <> ''                 
  begin                
     set  @strSql=@strSql+ ' and (vcFrom like ''%'+ @SeachKeyword+'%'' or vcsubject like ''%'+@SeachKeyword+'%'' or  vcBody like ''%'+@SeachKeyword+'%'')'                 
  end                
  if @tintSortOrder =2 and @SeachKeyword <> ''                 
  begin                
     set  @strSql=@strSql+ ' and (vcTo like ''%'+ @SeachKeyword+'%'' or vcsubject like ''%'+@SeachKeyword+'%'' or  vcBody like ''%'+@SeachKeyword+'%'')'                 
  end                            
 end                

 

 if ((@filter=0 or @filter>2) and (@tintSortOrder=0 or @tintSortOrder>2))                
 begin                
  set  @strSql=@strSql+ '                     
  INSERT INTO #TEMPData SELECT COUNT(*) OVER() AS numTotalRecords, 2 AS tintCountType, c.numCommId as numEmailHstrID,convert(varchar(15),C.numCommId)+''~2~''+convert(varchar(15),C.numCreatedBy) as DelData,CASE WHEN c.bitTask = 973 THEN dtCreatedDate WHEN c.bitTask = 974 THEN dtCreatedDate WHEN c.bitTask = 971 THEN dtStartTime ELSE dtEndTime END AS bintCreatedOn,
  case when c.bitTask=973 then dbo.FormatedDateTimeFromDate(DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+', dtCreatedDate),'+convert(varchar(15),@numDomainID)+') 
  when c.bitTask=974 then dbo.FormatedDateTimeFromDate(DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+', dtEndTime),'+convert(varchar(15),@numDomainID)+') 
  when c.bitTask=971 THEN dbo.FormatedDateFromDate(DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+', dtEndTime),'+convert(varchar(15),@numDomainID)+') + '' '' + RIGHT(CONVERT(VARCHAR,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+', [dtStartTime]),100),7)  +'' - '' + RIGHT(CONVERT(VARCHAR,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+', [dtEndTime]),100),7)
  else  dbo.FormatedDateFromDate(DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+', dtEndTime),'+convert(varchar(15),@numDomainID)+') + '' '' + RIGHT(CONVERT(VARCHAR,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+', [dtStartTime]),100),7)  +'' - '' + RIGHT(CONVERT(VARCHAR,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+', [dtEndTime]),100),7)  END AS [date],
  convert(varchar(8000),textdetails) as [Subject],                                                                         
 dbo.fn_GetListItemName(c.bitTask)  AS [Type],                                                                 
  A1.vcFirstName +'' ''+ A1.vcLastName +'' ''+                                      
  case when A1.numPhone<>'''' then + A1.numPhone +case when A1.numPhoneExtension<>'''' then '' - '' + A1.numPhoneExtension else '''' end  else '''' end as [Phone],                        
  A2.vcFirstName+ '' ''+A2.vcLastName  as assignedto,                         
  c.caseid,                           
  (select top 1 vcCaseNumber from cases where cases.numcaseid= c.caseid )as vcCasenumber,                               
  isnull(caseTimeid,0)as caseTimeid,                              
  isnull(caseExpid,0)as caseExpid  ,                        
  1 as tinttype ,dtCreatedDate,'''' as [FROM]     ,isnull(C.bitClosedflag,0) as bitClosedflag,0 as bitHasAttachments,
'''' as vcBody,
''id="Tickler~184~False~''+CAST(c.bitTask AS VARCHAR)+''~''+CAST(c.numCommId AS VARCHAR)+''" class="editable_textarea" onmousemove="bgColor=''''lightgoldenRodYellow''''"  title="Double click to edit..." bgcolor="lightgoldenRodYellow" '' AS InlineEdit,
C.numCreatedBy,
ISNULL(Div.numTerId,0) AS numOrgTerId,
''blueColorLabel'' as TypeClass
  from  Communication C                  
  JOIN AdditionalContactsInformation  A1                                           
  ON c.numContactId = A1.numContactId
  LEFT JOIN CommunicationLinkedOrganization clo ON C.numCommID=clo.numCommID                                                                             
  left join AdditionalContactsInformation A2 on A2.numContactID=c.numAssign
  left join UserMaster UM on A2.numContactid=UM.numUserDetailId and isnull(UM.bitActivateFlag,0)=1                                                                           
  left join listdetails on numActivity=numListID                           
  LEFT JOIN [Correspondence] CR ON CR.numCommID = C.numCommID
  JOIN DivisionMaster Div                                                                         
 ON A1.numDivisionId = Div.numDivisionID    
  where  C.numDomainID=' +convert(varchar(15),@numDomainID)
  + (CASE WHEN ISNULL(@vcTypes,'') <> '' THEN ' AND C.bitTask IN (SELECT Id FROM dbo.SplitIDs(''' + ISNULL(@vcTypes,'') + ''','',''))' ELSE '' END) + 
  + ' AND (numOpenRecordID = '+ convert(varchar(15),@numOpenRecordID) +' OR '+ convert(varchar(15),@numOpenRecordID) +'=0)'
  if (@numdivisionId =0 and @numContactId<>0) set  @strSql=@strSql+' and C.numContactId='+ convert(varchar(15),@numContactId)                        
  if @numdivisionId <> 0 set  @strSql=@strSql+' and  (C.numDivisionId='+ convert(varchar(15),@numdivisionId) + ' OR clo.numDivisionID='+ convert(varchar(15),@numdivisionId) + ') '             
  if @numCaseID>0  set  @strSql=@strSql+' and  C.CaseId='+ convert(varchar(15),@numCaseID)                       
  if @filter =0  
  BEGIN
	IF @tintMode <> 9
	BEGIN
		
		IF(@FromDate IS NOT NULL AND @FromDate<>'')
		BEGIN
			SET @strSql=@strSql+ ' and dtStartTime  >= '''+ convert(varchar(30),@FromDate)+''' and dtStartTime  <= '''+ convert(varchar(30),@ToDate)  +''''
		END
	END
  END
  else
    SET @strSql=@strSql+ ' and c.bitTask=' + CONVERT(varchar(12),@filter)  
		IF(@FromDate IS NOT NULL AND @FromDate<>'')
		BEGIN
			SET @strSql=@strSql+ ' and ((dtStartTime  >= '''+ convert(varchar(30),@FromDate)+''' and dtStartTime  <= '''+ convert(varchar(30),@ToDate)  +''') OR (dtEndTime  >= '''+ convert(varchar(30),@FromDate)+''' and dtEndTime  <= '''+ convert(varchar(30),@ToDate)  +'''))'
		END
  IF @tintMode=1 SET @strSql=@strSql+ ' and  (CR.tintCorrType IN(1,3) OR '+ convert(varchar(15),@numOpenRecordID) +'=0)'
  IF @tintMode=2 SET @strSql=@strSql+ ' and  (CR.tintCorrType IN(2,4) OR '+ convert(varchar(15),@numOpenRecordID) +'=0)'
  IF @tintMode=5 SET @strSql=@strSql+ ' and  (CR.tintCorrType IN(5) OR '+ convert(varchar(15),@numOpenRecordID) +'=0)'
  IF @tintMode=6 SET @strSql=@strSql+ ' and  (CR.tintCorrType IN(6) OR '+ convert(varchar(15),@numOpenRecordID) +'=0)'
                          
  IF @tintSortOrder =0 and @SeachKeyword <>'' 
	SET @strSql=@strSql+ ' and textdetails like ''%'+@SeachKeyword+'%'''
  ELSE
	IF ISNULL(@filter,0) = 0 
	BEGIN
		IF @SeachKeyword <>''
		BEGIN
			SET @strSql=@strSql+ ' and textdetails like ''%'+@SeachKeyword+'%'''
		END
	END
	ELSE
	BEGIN
		SET @strSql=@strSql+ ' and c.bitTask=' + CONVERT(varchar(12),@filter)
		
		IF @SeachKeyword <>''
		BEGIN
			SET @strSql= @strSql + ' and textdetails like ''%'+@SeachKeyword+'%'''
		END                     
	END
  IF @bitOpenCommu=1  SET @strSql=@strSql+ ' and isnull(C.bitClosedflag,0)=0'
 end                




 --set @strSql='With tblCorr as (SELECT ROW_NUMBER() OVER (ORDER BY '+@columnName+' '+ @columnSortOrder+') AS RowNumber,X.* from('+@strSql+')X)'                

 set @strSql= CONCAT(@strSql,'; SELECT ROW_NUMBER() OVER(ORDER BY bintCreatedOn DESC) RowNumber, * FROM #TEMPData ORDER BY bintCreatedOn DESC OFFSET ',((@CurrentPage-1) * @PageSize),' ROWS FETCH NEXT ',@PageSize,' ROWS ONLY;')
                    

print CAST(@strSql AS NTEXT)          

set @strSql = CONCAT(@strSql,'; SET @TotRecs = ISNULL((SELECT SUM(numTotalRecords) FROM (SELECT tintCountType,numTotalRecords FROM #TEMPData GROUP BY tintCountType,numTotalRecords) X),0)')
EXEC sp_executesql @strSql,N'@TotRecs INT OUTPUT',@TotRecs OUTPUT;
                   
IF OBJECT_ID(N'tempdb..#TEMPData') IS NOT NULL
BEGIN
	DROP TABLE #TEMPData
END
                
              
end              
END
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetGrossProfitEstimate')
DROP PROCEDURE USP_GetGrossProfitEstimate
GO
CREATE PROCEDURE [dbo].[USP_GetGrossProfitEstimate] 
( 
@numDomainID as numeric(9)=0,    
@numOppID AS NUMERIC(9)=0
)
AS
BEGIN
	DECLARE @avgCost INT
	DECLARE @numShippingServiceItemID NUMERIC(18,0)
	SELECT 
		@avgCost=numCost
		,@numShippingServiceItemID=ISNULL(numShippingServiceItemID,0)
	FROM 
		Domain 
	WHERE 
		numDOmainId=@numDomainID

	DECLARE @monShippingProfit DECIMAL(20,5)
	DECLARE @monShippingAverageCost DECIMAL(20,5) = 0
	DECLARE @monMarketplaceShippingCost DECIMAL(20,5)

	SELECT @monMarketplaceShippingCost = ISNULL(monMarketplaceShippingCost,0) FROM OpportunityMaster WHERE numDomainID=@numDomainID AND numOppID=@numOppID 

	IF ISNULL(@monMarketplaceShippingCost,0) > 0
	BEGIN
		IF EXISTS (SELECT * FROM OpportunityItems WHERE numOppID=@numOppID AND numItemCode=@numShippingServiceItemID)
		BEGIN
			SET @monShippingAverageCost = @monMarketplaceShippingCost / ISNULL((SELECT COUNT(*) FROM OpportunityItems WHERE numOppID=@numOppID AND numItemCode=@numShippingServiceItemID),0)
			SET @monShippingProfit = ISNULL((SELECT SUM(monTotAmount) FROM OpportunityItems WHERE numOppID=@numOppID AND numItemCode=@numShippingServiceItemID),0) - @monMarketplaceShippingCost
		END
		ELSE
		BEGIN
			SET @monShippingProfit = 0
		END
	END
	ELSE
	BEGIN
		SET @monShippingProfit = 0
	END

	SELECT 
		numOppId
		,@monShippingProfit AS monShippingProfit
		,vcPOppName
		,vcItemName
		,monAverageCost
		,vcVendor
		,(ISNULL(monTotAmount,0) * fltExchangeRate)/ISNULL(NULLIF(numUnitHour,0),1) AS monPrice
		,VendorCost
		,numUnitHour
		,(ISNULL(monTotAmount,0) * fltExchangeRate) - (CASE WHEN numItemCode=@numShippingServiceItemID THEN (ISNULL(numUnitHour,0) *  @monShippingAverageCost) ELSE (ISNULL(numUnitHour,0) * (CASE @avgCost WHEN 3 THEN VendorCost * dbo.fn_UOMConversion(numBaseUnit,numItemCode,@numDomainID,numPurchaseUnit) WHEN 2 THEN numCost * dbo.fn_UOMConversion(numBaseUnit,numItemCode,@numDomainID,numUOMId) ELSE monAverageCost END)) END) AS Profit
		,((((ISNULL(monTotAmount,0) * fltExchangeRate)/ISNULL(NULLIF(numUnitHour,0),1)) - (CASE 
																							WHEN numItemCode=@numShippingServiceItemID 
																							THEN @monShippingAverageCost
																							ELSE 
																								(CASE @avgCost 
																									WHEN 3 THEN VendorCost * dbo.fn_UOMConversion(numBaseUnit,numItemCode,@numDomainID,numPurchaseUnit) 
																									WHEN 2 THEN numCost * dbo.fn_UOMConversion(numBaseUnit,numItemCode,@numDomainID,numUOMId) 
																									ELSE monAverageCost 
																								END) 
																							END)) / (CASE WHEN ((ISNULL(monTotAmount,0) * fltExchangeRate)/ISNULL(NULLIF(numUnitHour,0),1))=0 then 1 ELSE ((ISNULL(monTotAmount,0) * fltExchangeRate)/ISNULL(NULLIF(numUnitHour,0),1)) end)) * 100 ProfitPer
		,bitItemPriceApprovalRequired
	FROM
	(
		SELECT 
			opp.numOppId
			,Opp.vcPOppName
			,OppI.vcItemName
			,(CASE WHEN ISNULL(I.bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(monAverageCost,0) END) monAverageCost,
			V.numVendorID AS numVendorID
			,dbo.fn_getcomapnyname(V.numVendorID) as vcVendor
			,oppI.numUnitHour
			,ISNULL(OPPI.monPrice,0) AS monPrice
			,ISNULL(OPPI.monTotAmount,0) AS monTotAmount
			,ISNULL(V.monCost,0) VendorCost
			,ISNULL(OppI.bitItemPriceApprovalRequired,0) AS bitItemPriceApprovalRequired
			,ISNULL(OppI.numCost,0) numCost
			,I.numItemCode
			,numBaseUnit
			,numPurchaseUnit
			,ISNULL(OppI.numUOMId,numBaseUnit) AS numUOMId
			,ISNULL(Opp.fltExchangeRate,1) fltExchangeRate
		FROM 
			OpportunityMaster Opp 
		INNER JOIN 
			OpportunityItems OppI 
		ON 
			opp.numOppId=OppI.numOppId 
		INNER JOIN 
			Item I 
		ON 
			OppI.numItemCode=i.numItemcode
		Left JOIN 
			Vendor V 
		ON 
			V.numVendorID=I.numVendorID 
			AND V.numItemCode=I.numItemCode
		WHERE 
			Opp.numDomainId=@numDomainID 
			AND opp.numOppId=@numOppID
			AND ISNULL(I.bitContainer,0) = 0
	) temp
END

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetItemOrderDependencies')
DROP PROCEDURE USP_GetItemOrderDependencies
GO
CREATE PROCEDURE [dbo].[USP_GetItemOrderDependencies]         
	@numDomainID as numeric(9)=0,    
	@numItemCode as numeric(9)=0,
	@ClientTimeZoneOffset Int,
	@tintOppType as tinyint=0,
	@byteMode as tinyint=0,
	@numWareHouseId as numeric(9)=0
as        
IF @byteMode=0 
BEGIN

	DECLARE @vcItemName AS VARCHAR(100)
	DECLARE @OnOrder AS numeric(18)
	DECLARE @vcShippedReceivedStatus VARCHAR(100) = ''

	Create table #temp (numoppid numeric(18), vcPOppName varchar(100), tintOppType tinyint, bintCreatedDate datetime, bintAccountClosingDate datetime, OppType varchar(20),
		OppStatus varchar(20), vcCompanyname varchar(200), vcItemName varchar(200), numReturnHeaderID numeric(18), vcWareHouse varchar(100), 
		numQtyOrdered FLOAT, numQtyReleasedReceived FLOAT, numQtyOnAllocationOnOrder FLOAT)

	-- NOT Sales and Purchase Return
	IF @tintOppType <> 5 AND @tintOppType <> 6 AND ISNULL(@tintOppType,0) <> 11
	BEGIN
		--Regular Item
		INSERT INTO 
			#temp
		SELECT 
			OM.numoppid
			,OM.vcPoppName
			,OM.tintOppType
			,OM.bintCreatedDate
			,OM.bintAccountClosingDate
			,CASE 
				WHEN OM.tintOppType=1 
				THEN CASE WHEN OM.tintOppStatus=0 THEN 'Sales Opp' ELSE 'Sales Order' END 
				ELSE CASE WHEN OM.tintOppStatus=0 THEN 'Purchase Opp' ELSE 'Purchase Order' END 
			END as OppType
			,CASE ISNULL(OM.tintshipped, 0) WHEN 0 THEN 'Open' WHEN 1 THEN 'Closed' END OppStatus
			,CI.vcCompanyName + Case when isnull(DM.numCompanyDiff,0)>0 then  '  ' + dbo.fn_getlistitemname(DM.numCompanyDiff) + ':' + isnull(DM.vcCompanyDiff,'') else '' end as vcCompanyname
			,''
			,0
			,W.vcWareHouse 
			,ISNULL(dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), Opp.numItemCode,I.numDomainId, ISNULL(Opp.numUOMId, 0)),1) * numUnitHour AS numQtyOrdered
			,ISNULL(dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), Opp.numItemCode,I.numDomainId, ISNULL(Opp.numUOMId, 0)),1) * (CASE WHEN OM.tintOppType=1 THEN ISNULL(numQtyShipped,0) ELSE ISNULL(numUnitHourReceived,0) END) AS numQtyReleasedReceived
			,(ISNULL(dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), Opp.numItemCode,I.numDomainId, ISNULL(Opp.numUOMId, 0)),1) * numUnitHour) - (ISNULL(dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), Opp.numItemCode,I.numDomainId, ISNULL(Opp.numUOMId, 0)),1) * (CASE WHEN OM.tintOppType=1 THEN ISNULL(numQtyShipped,0) ELSE ISNULL(numUnitHourReceived,0) END)) As numQtyOnAllocationOnOrder
		FROM 
			OpportunityItems Opp
		INNER JOIN OpportunityMaster OM ON Opp.numOppID = oM.numOppID
		INNER JOIN item I ON Opp.numItemCode = i.numItemcode
		INNER JOIN divisionMaster DM ON OM.numDivisionID = DM.numDivisionID
		LEFT JOIN CompanyInfo CI ON CI.numCompanyID = DM.numCompanyID
		LEFT JOIN WareHouseItems WHI ON WHI.[numWareHouseItemID] = Opp.numWarehouseItmsID
		LEFT JOIN Warehouses W ON W.numWareHouseID = WHI.numWareHouseID
		WHERE   
			OM.numDomainId = @numDomainID
			AND I.numDomainId = @numDomainID 
			AND I.numItemCode=@numItemCode
			AND 1 = (CASE WHEN @numWareHouseId > 0 THEN 
							Case WHEN W.numWareHouseID = @numWareHouseId then 1 else 0 end
						Else 1 end) 
			AND 1=(CASE 
						WHEN ISNULL(@tintOppType,0) = 1 -- Sales Order
						THEN CASE WHEN ISNULL(OM.tintOppType,0) = 1 AND ISNULL(OM.tintOppStatus,0)=1 THEN 1 ELSE 0 END
						WHEN ISNULL(@tintOppType,0) = 2 -- Purchase Order
						THEN CASE WHEN ISNULL(OM.tintOppType,0) = 2 AND ISNULL(OM.tintOppStatus,0)=1 THEN 1 ELSE 0 END
						WHEN ISNULL(@tintOppType,0) = 3 -- Sales Opportunity
						THEN CASE WHEN ISNULL(OM.tintOppType,0) = 1 AND ISNULL(OM.tintOppStatus,0)=0 THEN 1 ELSE 0 END
						WHEN ISNULL(@tintOppType,0) = 4 -- Purchase Opportunity
						THEN CASE WHEN ISNULL(OM.tintOppType,0) = 2 AND ISNULL(OM.tintOppStatus,0)=0 THEN 1 ELSE 0 END
						WHEN ISNULL(@tintOppType,0) = 7 -- Open Sales Order
						THEN CASE WHEN ISNULL(OM.tintOppType,0) = 1 AND ISNULL(OM.tintOppStatus,0)=1 AND ISNULL(OM.tintshipped,0) = 0 THEN 1 ELSE 0 END
						WHEN ISNULL(@tintOppType,0) = 8 -- Closed Sales Order
						THEN CASE WHEN ISNULL(OM.tintOppType,0) = 1 AND ISNULL(OM.tintOppStatus,0)=1 AND ISNULL(OM.tintshipped,0) = 1 THEN 1 ELSE 0 END
						WHEN ISNULL(@tintOppType,0) = 9 -- Open Purchase Order
						THEN CASE WHEN ISNULL(OM.tintOppType,0) = 2 AND ISNULL(OM.tintOppStatus,0)=1 AND ISNULL(OM.tintshipped,0) = 0 THEN 1 ELSE 0 END
						WHEN ISNULL(@tintOppType,0) = 10 -- Closed Purchase Order
						THEN CASE WHEN ISNULL(OM.tintOppType,0) = 2 AND ISNULL(OM.tintOppStatus,0)=1 AND ISNULL(OM.tintshipped,0) = 1 THEN 1 ELSE 0 END 
						ELSE 1 
					END)
		ORDER BY 
			OM.bintCreatedDate desc
	END
	

	DECLARE @strSQL AS NVARCHAR(MAX) = ''
	DECLARE @strWHERE AS NVARCHAR(MAX)
	SET @strWHERE = ' WHERE 1=1 '

	IF @tintOppType = 5 
	BEGIN
	 SET @strWHERE = @strWHERE + ' AND [RH].[tintReturnType] = 1 '
	END
	ELSE IF @tintOppType = 6
	BEGIN
	 SET @strWHERE = @strWHERE + ' AND [RH].[tintReturnType] = 2 '
	END
	ELSE IF @tintOppType = 0
	BEGIN
	 SET @strWHERE = @strWHERE + ' AND [RH].[tintReturnType] IN (1,2) '
	END


	-- NOT Sales and Purchase Return
	IF @tintOppType <> 5 AND @tintOppType <> 6 AND ISNULL(@tintOppType,0) <> 11
	BEGIN
		SET @strSQL = CONCAT('INSERT INTO 
								#temp
							SELECT 
								OM.numoppid
								,OM.vcPoppName
								,OM.tintOppType
								,OM.bintCreatedDate
								,OM.bintAccountClosingDate
								,CASE 
									WHEN OM.tintOppType=1 
									THEN CASE WHEN OM.tintOppStatus=0 THEN ''Sales Opp'' ELSE ''Sales Order'' END 
									ELSE CASE WHEN OM.tintOppStatus=0 THEN ''Purchase Opp'' ELSE ''Purchase Order'' END 
								END as OppType
								,CASE ISNULL(OM.tintshipped, 0) WHEN 0 THEN ''Open'' WHEN 1 THEN ''Closed'' END OppStatus
								,CI.vcCompanyName + Case when isnull(DM.numCompanyDiff,0)>0 then  ''  '' + dbo.fn_getlistitemname(DM.numCompanyDiff) + '':'' + isnull(DM.vcCompanyDiff,'''') else '''' end as vcCompanyname
								,IParent.vcItemName + Case when IParent.bitAssembly=1 then '' (Assembly Member)'' When IParent.bitKitParent=1 then '' (Kit Member)'' else '''' end
								,0 [numReturnHeaderID]
								,W.vcWareHouse
								,ISNULL(dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), I.numItemCode,I.numDomainId, ISNULL(OKI.numUOMId, 0)),1) * OKI.numQtyItemsReq AS numQtyOrdered
								,ISNULL(dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), I.numItemCode,I.numDomainId, ISNULL(OKI.numUOMId, 0)),1) * (CASE WHEN OM.tintOppType=1 THEN ISNULL(OKI.numQtyShipped,0) ELSE 0 END) AS numQtyReleasedReceived
								,(ISNULL(dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), I.numItemCode,I.numDomainId, ISNULL(OKI.numUOMId, 0)),1) * OKI.numQtyItemsReq) - (ISNULL(dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), I.numItemCode,I.numDomainId, ISNULL(OKI.numUOMId, 0)),1) * (CASE WHEN OM.tintOppType=1 THEN ISNULL(OKI.numQtyShipped,0) ELSE 0 END)) As numQtyOnAllocationOnOrder
							FROM 
								OpportunityKitItems OKI 
							INNER JOIN OpportunityItems OI
							ON OKI.numOppItemID=OI.numoppitemtcode
							INNER JOIN Item IParent on OI.numItemCode =IParent.numItemCode 
							INNER JOIN Item I on OKI.numChildItemID =I.numItemCode  
							INNER JOIN OpportunityMaster OM ON OKI.numOppID = oM.numOppID
							INNER JOIN divisionMaster DM ON OM.numDivisionID = DM.numDivisionID
							LEFT JOIN CompanyInfo CI ON CI.numCompanyID = DM.numCompanyID
							LEFT JOIN WareHouseItems WHI ON WHI.numWarehouseItemID = OKI.numWarehouseItemID
							LEFT JOIN Warehouses W ON W.numWareHouseID = WHI.numWareHouseID
							WHERE 
								ISNULL(I.bitAssembly,0)=0 
								AND OKI.numChildItemID=',@numItemCode,'  
								AND om.numDomainId = ',@numDomainID,' 
								AND 1 = (CASE WHEN ',@numWareHouseId,' > 0 THEN 
												Case WHEN W.numWareHouseID = ',@numWareHouseId,' then 1 else 0 end
											Else 1 end) 
								AND I.numDomainId = ',@numDomainID,'  
								AND 1=(CASE 
										WHEN ',ISNULL(@tintOppType,0),' = 1
										THEN CASE WHEN ISNULL(OM.tintOppType,0) = 1 AND ISNULL(OM.tintOppStatus,0)=1 THEN 1 ELSE 0 END
										WHEN ',ISNULL(@tintOppType,0),' = 2
										THEN CASE WHEN ISNULL(OM.tintOppType,0) = 2 AND ISNULL(OM.tintOppStatus,0)=1 THEN 1 ELSE 0 END
										WHEN ',ISNULL(@tintOppType,0),' = 3
										THEN CASE WHEN ISNULL(OM.tintOppType,0) = 1 AND ISNULL(OM.tintOppStatus,0)=0 THEN 1 ELSE 0 END
										WHEN ',ISNULL(@tintOppType,0),' = 4
										THEN CASE WHEN ISNULL(OM.tintOppType,0) = 2 AND ISNULL(OM.tintOppStatus,0)=0 THEN 1 ELSE 0 END
										WHEN ',ISNULL(@tintOppType,0),' = 7
										THEN CASE WHEN ISNULL(OM.tintOppType,0) = 1 AND ISNULL(OM.tintOppStatus,0)=1 AND ISNULL(OM.tintshipped,0) = 0 THEN 1 ELSE 0 END
										WHEN ',ISNULL(@tintOppType,0),' = 8
										THEN CASE WHEN ISNULL(OM.tintOppType,0) = 1 AND ISNULL(OM.tintOppStatus,0)=1 AND ISNULL(OM.tintshipped,0) = 1 THEN 1 ELSE 0 END
										WHEN ',ISNULL(@tintOppType,0),' = 9
										THEN CASE WHEN ISNULL(OM.tintOppType,0) = 2 AND ISNULL(OM.tintOppStatus,0)=1 AND ISNULL(OM.tintshipped,0) = 0 THEN 1 ELSE 0 END
										WHEN ',ISNULL(@tintOppType,0),' = 10
										THEN CASE WHEN ISNULL(OM.tintOppType,0) = 2 AND ISNULL(OM.tintOppStatus,0)=1 AND ISNULL(OM.tintshipped,0) = 1 THEN 1 ELSE 0 END 
										ELSE 1 
									END)')

		SET @strSQL = CONCAT(@strSQL,' INSERT INTO 
								#temp
							SELECT 
								OM.numoppid
								,OM.vcPoppName
								,OM.tintOppType
								,OM.bintCreatedDate
								,OM.bintAccountClosingDate
								,CASE 
									WHEN OM.tintOppType=1 
									THEN CASE WHEN OM.tintOppStatus=0 THEN ''Sales Opp'' ELSE ''Sales Order'' END 
									ELSE CASE WHEN OM.tintOppStatus=0 THEN ''Purchase Opp'' ELSE ''Purchase Order'' END 
								END as OppType
								,CASE ISNULL(OM.tintshipped, 0) WHEN 0 THEN ''Open'' WHEN 1 THEN ''Closed'' END OppStatus
								,CI.vcCompanyName + Case when isnull(DM.numCompanyDiff,0)>0 then  ''  '' + dbo.fn_getlistitemname(DM.numCompanyDiff) + '':'' + isnull(DM.vcCompanyDiff,'''') else '''' end as vcCompanyname
								,IParent.vcItemName + Case when IParent.bitAssembly=1 then '' (Assembly Member)'' When IParent.bitKitParent=1 then '' (Kit Member)'' else '''' end
								,0 [numReturnHeaderID]
								,W.vcWareHouse
								,ISNULL(dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), I.numItemCode,I.numDomainId, ISNULL(OKI.numUOMId, 0)),1) * OKI.numQtyItemsReq AS numQtyOrdered
								,ISNULL(dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), I.numItemCode,I.numDomainId, ISNULL(OKI.numUOMId, 0)),1) * (CASE WHEN OM.tintOppType=1 THEN ISNULL(OKI.numQtyShipped,0) ELSE 0 END) AS numQtyReleasedReceived
								,(ISNULL(dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), I.numItemCode,I.numDomainId, ISNULL(OKI.numUOMId, 0)),1) * OKI.numQtyItemsReq) - (ISNULL(dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), I.numItemCode,I.numDomainId, ISNULL(OKI.numUOMId, 0)),1) * (CASE WHEN OM.tintOppType=1 THEN ISNULL(OKI.numQtyShipped,0) ELSE 0 END)) As numQtyOnAllocationOnOrder
							FROM 
								OpportunityKitChildItems OKI 
							INNER JOIN OpportunityItems OI
							ON OKI.numOppItemID=OI.numoppitemtcode
							INNER JOIN Item IParent on OI.numItemCode =IParent.numItemCode 
							INNER JOIN Item I on OKI.numItemID =I.numItemCode  
							INNER JOIN OpportunityMaster OM ON OKI.numOppID = oM.numOppID
							INNER JOIN divisionMaster DM ON OM.numDivisionID = DM.numDivisionID
							LEFT JOIN CompanyInfo CI ON CI.numCompanyID = DM.numCompanyID
							LEFT JOIN WareHouseItems WHI ON WHI.numWarehouseItemID = OKI.numWarehouseItemID
							LEFT JOIN Warehouses W ON W.numWareHouseID = WHI.numWareHouseID
							WHERE 
								ISNULL(I.bitAssembly,0)=0 
								AND OKI.numItemID=',@numItemCode,'  
								AND om.numDomainId = ',@numDomainID,' 
								AND 1 = (CASE WHEN ',@numWareHouseId,' > 0 THEN 
												Case WHEN W.numWareHouseID = ',@numWareHouseId,' then 1 else 0 end
											Else 1 end) 
								AND I.numDomainId = ',@numDomainID,'  
								AND 1=(CASE 
										WHEN ',ISNULL(@tintOppType,0),' = 1
										THEN CASE WHEN ISNULL(OM.tintOppType,0) = 1 AND ISNULL(OM.tintOppStatus,0)=1 THEN 1 ELSE 0 END
										WHEN ',ISNULL(@tintOppType,0),' = 2
										THEN CASE WHEN ISNULL(OM.tintOppType,0) = 2 AND ISNULL(OM.tintOppStatus,0)=1 THEN 1 ELSE 0 END
										WHEN ',ISNULL(@tintOppType,0),' = 3
										THEN CASE WHEN ISNULL(OM.tintOppType,0) = 1 AND ISNULL(OM.tintOppStatus,0)=0 THEN 1 ELSE 0 END
										WHEN ',ISNULL(@tintOppType,0),' = 4
										THEN CASE WHEN ISNULL(OM.tintOppType,0) = 2 AND ISNULL(OM.tintOppStatus,0)=0 THEN 1 ELSE 0 END
										WHEN ',ISNULL(@tintOppType,0),' = 7
										THEN CASE WHEN ISNULL(OM.tintOppType,0) = 1 AND ISNULL(OM.tintOppStatus,0)=1 AND ISNULL(OM.tintshipped,0) = 0 THEN 1 ELSE 0 END
										WHEN ',ISNULL(@tintOppType,0),' = 8
										THEN CASE WHEN ISNULL(OM.tintOppType,0) = 1 AND ISNULL(OM.tintOppStatus,0)=1 AND ISNULL(OM.tintshipped,0) = 1 THEN 1 ELSE 0 END
										WHEN ',ISNULL(@tintOppType,0),' = 9
										THEN CASE WHEN ISNULL(OM.tintOppType,0) = 2 AND ISNULL(OM.tintOppStatus,0)=1 AND ISNULL(OM.tintshipped,0) = 0 THEN 1 ELSE 0 END
										WHEN ',ISNULL(@tintOppType,0),' = 10
										THEN CASE WHEN ISNULL(OM.tintOppType,0) = 2 AND ISNULL(OM.tintOppStatus,0)=1 AND ISNULL(OM.tintshipped,0) = 1 THEN 1 ELSE 0 END 
										ELSE 1 
									END)')
	END

	IF @tintOppType = 5 AND @tintOppType = 6 OR @tintOppType = 0
	BEGIN
		SET @strSQL = @strSQL + CONCAT(' INSERT INTO #temp
										SELECT 
											RH.[numReturnHeaderID] numoppid
											,RH.[vcRMA] vcPoppName
											,[RH].[tintReturnType] tintOppType
											,RH.[dtCreatedDate] bintCreatedDate
											,NULL bintAccountClosingDate
											,CASE [RH].[tintReturnType] WHEN 1 THEN ''Sales Return'' WHEN 2 THEN ''Purchase Return'' ELSE '''' END OppType
											,'''' OppStatus
											,CI.vcCompanyName + CASE WHEN ISNULL(DM.numCompanyDiff,0) > 0 THEN ''  '' + dbo.fn_getlistitemname(DM.numCompanyDiff) + '':'' + ISNULL(DM.vcCompanyDiff,'''') ELSE '''' END AS vcCompanyname
											,I.vcItemName + CASE WHEN I.bitAssembly = 1 THEN '' (Assembly)'' WHEN I.bitKitParent = 1 THEN '' (Kit)'' ELSE '''' END
											,RH.[numReturnHeaderID]
											,W.vcWareHouse
											,0
											,0
											,0
										FROM [dbo].[ReturnHeader] AS RH 
										INNER JOIN [dbo].[ReturnItems] AS RI  ON [RH].[numReturnHeaderID] = [RI].[numReturnHeaderID] AND [RH].[tintReturnType] IN (1,2)
										INNER JOIN [dbo].[Item] AS I ON [RI].[numItemCode] = [I].[numItemCode] AND [I].[numDomainID] = RH.[numDomainId]
										INNER JOIN divisionMaster DM ON RH.numDivisionID = DM.numDivisionID
										LEFT JOIN CompanyInfo CI ON CI.numCompanyID = DM.numCompanyID 
										LEFT JOIN WareHouseItems WHI ON WHI.numWareHouseItemID = RI.numWareHouseItemID  
										LEFT JOIN Warehouses W ON W.numWareHouseID = WHI.numWareHouseID
										', @strWHERE,' AND RH.[numDomainId] = ',@numDomainID,' AND [RI].[numItemCode] = ',@numItemCode,' ORDER BY bintCreatedDate desc')
	END

	-- Stock Transfer
	IF ISNULL(@tintOppType,0) = 0 OR ISNULL(@tintOppType,0) = 11
	BEGIN
		INSERT INTO 
			#temp
		SELECT 
			MST.ID
			,CONCAT('Stock Transfer (To Warehouse:',CONCAT(WTo.vcWareHouse, CASE WHEN LEN(ISNULL(WLTo.vcLocation,'')) > 0 THEN CONCAT(' (',WLTo.vcLocation,')') ELSE '' END),')')
			,0
			,MST.dtTransferredDate
			,NULL
			,'Stock Transfer' as OppType
			,'Closed' OppStatus
			,'' as vcCompanyname
			,''
			,0
			,CONCAT(WFrom.vcWareHouse, CASE WHEN LEN(ISNULL(WLFrom.vcLocation,'')) > 0 THEN CONCAT(' (',WLFrom.vcLocation,')') ELSE '' END)
			,MST.numQty AS numQtyOrdered
			,MST.numQty AS numQtyReleasedReceived
			,0 As numQtyOnAllocationOnOrder
		FROM 
			MassStockTransfer MST
		INNER JOIN 
			Item I 
		ON 
			MST.numItemCode = I.numItemcode
		INNER JOIN 
			WareHouseItems WHIFrom 
		ON 
			WHIFrom.[numWareHouseItemID] = MST.numFromWarehouseItemID
		INNER JOIN 
			Warehouses WFrom 
		ON 
			WFrom.numWareHouseID = WHIFrom.numWareHouseID
		LEFT JOIN
			WarehouseLocation WLFrom
		ON
			WHIFrom.numWLocationID = WLFrom.numWLocationID
		INNER JOIN 
			WareHouseItems WHITo 
		ON 
			WHITo.[numWareHouseItemID] = MST.numToWarehouseItemID
		INNER JOIN 
			Warehouses WTo 
		ON 
			WTo.numWareHouseID = WHITo.numWareHouseID
		LEFT JOIN
			WarehouseLocation WLTo
		ON
			WHITo.numWLocationID = WLTo.numWLocationID
		WHERE   
			MST.numDomainId = @numDomainID
			AND MST.numItemCode=@numItemCode
			AND I.numDomainId = @numDomainID 
	END


PRINT CAST(@strSQL AS NTEXT)
EXEC (@strSQL)


select numoppid,vcPoppName,tintOppType,
case when convert(varchar(11),DateAdd(minute, -@ClientTimeZoneOffset,bintCreatedDate) )= convert(varchar(11),getdate()) then '<b><font color=red>Today</font></b>'
	 when convert(varchar(11),DateAdd(minute, -@ClientTimeZoneOffset,bintCreatedDate) )= convert(varchar(11),dateadd(day,-1,getdate())) then '<b><font color=purple>YesterDay</font></b>'
	 when convert(varchar(11),DateAdd(minute, -@ClientTimeZoneOffset,bintCreatedDate) )= convert(varchar(11),dateadd(day,1,getdate())) then'<b><font color=orange>Tommorow</font></b>' 
	else dbo.FormatedDateFromDate(DateAdd(minute, -@ClientTimeZoneOffset,bintCreatedDate ),1) end  CreatedDate,
case when convert(varchar(11),DateAdd(minute, -@ClientTimeZoneOffset,bintAccountClosingDate) )= convert(varchar(11),getdate()) then '<b><font color=red>Today</font></b>'
	 when convert(varchar(11),DateAdd(minute, -@ClientTimeZoneOffset,bintAccountClosingDate) )= convert(varchar(11),dateadd(day,-1,getdate())) then '<b><font color=purple>YesterDay</font></b>'
	 when convert(varchar(11),DateAdd(minute, -@ClientTimeZoneOffset,bintAccountClosingDate) )= convert(varchar(11),dateadd(day,1,getdate())) then'<b><font color=orange>Tommorow</font></b>' 
	else dbo.FormatedDateFromDate(DateAdd(minute, -@ClientTimeZoneOffset,bintAccountClosingDate ),1) end  AccountClosingDate,
 OppType,OppStatus,vcCompanyName,vcItemName,numReturnHeaderID, vcWareHouse, ISNULL(numQtyOrdered,0) AS numOnOrder, ISNULL(numQtyOnAllocationOnOrder,0) AS numAllocation,ISNULL(numQtyReleasedReceived,0) numQtyReleasedReceived  from #temp 
  ORDER by bintCreatedDate desc 

drop table #temp
END


ELSE IF @byteMode=1 --Parent Items of selected item
BEGIN

;WITH CTE(numItemCode,vcItemName,bitAssembly,bitKitParent)
AS
(
select numItemKitID,vcItemName,bitAssembly,bitKitParent
from item                                
INNER join ItemDetails Dtl on numItemKitID=numItemCode
where  numChildItemID=@numItemCode

UNION ALL

select dtl.numItemKitID ,i.vcItemName,i.bitAssembly,i.bitKitParent
from item i                               
INNER JOIN ItemDetails Dtl on Dtl.numItemKitID=i.numItemCode
INNER JOIN CTE c ON Dtl.numChildItemID = c.numItemCode
where Dtl.numChildItemID!=@numItemCode
)

select numItemCode,vcItemName,Case when bitAssembly=1 then 'Assembly' When bitKitParent=1 then 'Kit' end as ItemType
from CTE                                
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--modified by chintan
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetOtherContactsFromOrganization')
DROP PROCEDURE USP_GetOtherContactsFromOrganization
GO
CREATE PROCEDURE [dbo].[USP_GetOtherContactsFromOrganization]
(
	@numDomainID NUMERIC(18,0)
	,@tintMode TINYINT
	,@vcContactIds VARCHAR(MAX)
)
AS
BEGIN
	SELECT STUFF((SELECT 
					CONCAT(' <li><input type="checkbox" onchange="return OtherContactSelectionChanged(',@tintMode,',this);" id="', CONCAT(ADC.vcEmail,'~',ADC.numContactID,'~',ADC.vcFirstName,'~',ADC.vcLastName),'" />&nbsp;&nbsp;<label style="font-weight:normal;">',CONCAT(ISNULL(ADC.vcFirstName,'-'),' ',ISNULL(ADC.vcLastName,'-'),', ',ADC.vcEmail,', ',C.vcCompanyName,(CASE WHEN LD.vcData IS NOT NULL THEN CONCAT(' (',LD.vcData,')') ELSE '' END)),'</label></li>')
				FROM
					AdditionalContactsInformation ADC
				INNER JOIN 
					dbo.DivisionMaster DM 
				ON 
					ADC.numDivisionId = DM.numDivisionID 
				INNER JOIN 
					dbo.CompanyInfo C 
				ON
					DM.numCompanyID = C.numCompanyId 
				LEFT JOIN 
					dbo.ListDetails LD 
				ON 
					LD.numListItemID=numCompanyType
				WHERE
					ADC.numDomainID = @numDomainID
					AND ADC.numDivisionId IN (SELECT numDivisionId FROM AdditionalContactsInformation ACI WHERE ACI.numDomainID=@numDomainID AND ACI.numContactId IN (SELECT Id FROM SplitIDs(@vcContactIds,',')))
					AND ADC.numContactId NOT IN (SELECT Id FROM SplitIDs(@vcContactIds,','))
					AND LEN(ISNULL(ADC.vcEmail,'')) > 0
				ORDER BY
					ADC.vcFirstName
				FOR XML PATH(''), TYPE).value('.', 'varchar(max)'),1,1,'')
END
GO
--EXEC USP_GetSalesTemplateItems 6,72
--SELECT * FROM SalesTemplateItems
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetSalesTemplateItems')
DROP PROCEDURE USP_GetSalesTemplateItems
GO
CREATE PROCEDURE [dbo].[USP_GetSalesTemplateItems]
    @numSalesTemplateID NUMERIC(18,0),
    @numDomainID NUMERIC(18,0),
	@numDivisionID NUMERIC(18,0)
AS
	DECLARE @tintPriceLevel TINYINT
	DECLARE @tintDefaultSalesPricing AS TINYINT

	SELECT @tintDefaultSalesPricing=numDefaultSalesPricing FROM Domain WHERE numDomainId=@numDomainID

	SELECT 
		@tintPriceLevel = ISNULL(D.tintPriceLevel,0)
	FROM 
		DivisionMaster D                
	WHERE 
		numDivisionID =@numDivisionID       

    SELECT  
		X.*
		,(SELECT TOP 1 vcCategoryName FROM [Category] WHERE numCategoryID IN (SELECT numCategoryID FROM [ItemCategory] IC WHERE X.numItemCode = IC.[numItemID])) AS vcCategoryName
    FROM    
	( 
		SELECT DISTINCT
            STI.[numSalesTemplateItemID],
            STI.[numSalesTemplateID],
            STI.[numSalesTemplateItemID] AS numoppitemtCode,
            STI.[numItemCode],
            STI.[numUnitHour],
            CASE WHEN ISNULL(@numDivisionID,0) > 0 AND ISNULL(@tintPriceLevel,0) > 0 AND @tintDefaultSalesPricing = 1 AND (SELECT COUNT(*) FROM PricingTable WHERE numItemCode=STI.[numItemCode] AND ISNULL(numPriceRuleID,0) = 0 AND ISNULL(numCurrencyID,0) = 0) > 0 THEN 
			ISNULL((SELECT 
						ISNULL((CASE 
											WHEN TEMP.tintRuleType = 1 AND TEMP.tintDiscountType = 1
											THEN ISNULL(I.monListPrice,0) - (ISNULL(I.monListPrice,0) * (TEMP.decDiscount / 100 ) )
											WHEN TEMP.tintRuleType = 1 AND TEMP.tintDiscountType = 2
											THEN ISNULL(I.monListPrice,0) - TEMP.decDiscount
											WHEN TEMP.tintRuleType = 2 AND TEMP.tintDiscountType = 1
											THEN ISNULL(Vendor.monCost,0) + (ISNULL(Vendor.monCost,0) * (TEMP.decDiscount / 100 ) )
											WHEN TEMP.tintRuleType = 2 AND TEMP.tintDiscountType = 2
											THEN ISNULL(Vendor.monCost,0) + TEMP.decDiscount
											WHEN TEMP.tintRuleType = 3
											THEN TEMP.decDiscount
										END),STI.[monPrice])
					FROM
					(
						SELECT 
							ROW_NUMBER() OVER(PARTITION BY PricingTable.numItemCode ORDER BY numPricingID) Id,
							*
						FROM
							PricingTable
						WHERE 
							PricingTable.numItemCode = STI.[numItemCode]
							AND ISNULL(numCurrencyID,0) = 0
					) TEMP
					WHERE
						numItemCode=STI.[numItemCode] 
						AND (tintRuleType = 3 AND Id=@tintPriceLevel)),STI.[monPrice]) ELSE STI.[monPrice] END monPrice,
            STI.[monTotAmount],
            STI.[numSourceID],
            STI.[numWarehouseID],
            STI.[Warehouse],
            STI.[numWarehouseItmsID],
            STI.[ItemType],
            STI.[ItemType] as vcType,
            STI.[Attributes],
			STI.[Attributes] as vcAttributes,
            STI.[AttrValues],
            I.[bitFreeShipping] FreeShipping, --Use from Item table
            STI.[Weight],
            STI.[Op_Flag],
            STI.[bitDiscountType],
            STI.[fltDiscount],
            STI.[monTotAmtBefDiscount],
            STI.[ItemURL],
            STI.[numDomainID],
            '' vcItemDesc,
            (SELECT TOP 1 vcPathForTImage FROM dbo.ItemImages II WHERE II.numItemCode = I.numItemCode AND II.numDomainId= I.numDomainId AND II.bitDefault=1) vcPathForTImage,
            I.vcItemName,
            'False' AS DropShip,
			0 as bitDropShip,
            I.[bitTaxable],
            ISNULL(STI.numUOM,0) numUOM,
			ISNULL(STI.numUOM,0) as numUOMId,
            ISNULL(STI.vcUOMName,'') vcUOMName,
            ISNULL(STI.UOMConversionFactor,1) UOMConversionFactor,
			ISNULL(STI.numVendorWareHouse,0) as numVendorWareHouse
			,ISNULL(STI.numShipmentMethod,0) as numShipmentMethod
			,ISNULL(STI.numSOVendorId,0) as numSOVendorId
			,0 as bitWorkOrder
			,I.charItemType
			,dbo.fn_GetUOMName(I.numBaseUnit) AS vcBaseUOMName
			,0 AS numSortOrder
        FROM 
			SalesTemplateItems STI
        INNER JOIN item I ON I.numItemCode = STI.numItemCode
		LEFT JOIN Vendor ON I.numItemCode=Vendor.numItemCode AND I.numVendorID=Vendor.numVendorID
        LEFT JOIN dbo.UOM U ON U.numUOMID = I.numSaleUnit
        WHERE 
			STI.[numSalesTemplateID] = @numSalesTemplateID
            AND STI.[numDomainID] = @numDomainID
) X
--Created By Anoop Jayaraj          
--exec USP_ItemDetailsForEcomm @numItemCode=859064,@numWareHouseID=1074,@numDomainID=172,@numSiteId=90
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_itemdetailsforecomm')
DROP PROCEDURE USP_ItemDetailsForEcomm
GO
CREATE PROCEDURE [dbo].[USP_ItemDetailsForEcomm]
@numItemCode as numeric(9),          
@numWareHouseID as numeric(9),    
@numDomainID as numeric(9),
@numSiteId as numeric(9),
@vcCookieId as VARCHAR(MAX)=null,
@numUserCntID AS NUMERIC(9)=0,
@numDivisionID AS NUMERIC(18,0) = 0                                        
AS     
BEGIN
/*Removed warehouse parameter, and taking Default warehouse by default from DB*/
    
	DECLARE @bitShowInStock AS BIT        
	DECLARE @bitShowQuantity AS BIT
	DECLARE @bitAutoSelectWarehouse AS BIT              
	DECLARE @numDefaultWareHouseID AS NUMERIC(9)
	DECLARE @numDefaultRelationship AS NUMERIC(18,0)
	DECLARE @numDefaultProfile AS NUMERIC(18,0)
	DECLARE @tintPreLoginPriceLevel AS TINYINT
	DECLARE @tintPriceLevel AS TINYINT
	DECLARE @bitMatrix BIT
	DECLARE @numItemGroup NUMERIC(18,0)
	SET @bitShowInStock=0        
	SET @bitShowQuantity=0        
       
        
	IF ISNULL(@numDivisionID,0) > 0
	BEGIN
		SELECT 
			@numDefaultRelationship=ISNULL(numCompanyType,0)
			,@numDefaultProfile=ISNULL(vcProfile,0)
			,@tintPriceLevel=ISNULL(tintPriceLevel,0) 
		FROM 
			DivisionMaster 
		INNER JOIN 
			CompanyInfo 
		ON 
			DivisionMaster.numCompanyID=CompanyInfo.numCompanyId 
		WHERE 
			numDivisionID=@numDivisionID 
			AND DivisionMaster.numDomainID=@numDomainID
	END     
      
	--KEEP THIS BELOW ABOVE IF CONDITION 
	SELECT 
		@bitShowInStock=ISNULL(bitShowInStock,0)
		,@bitShowQuantity=ISNULL(bitShowQOnHand,0)
		,@bitAutoSelectWarehouse=ISNULL(bitAutoSelectWarehouse,0)
		,@numDefaultRelationship=(CASE WHEN ISNULL(@numDivisionID,0) > 0 THEN @numDefaultRelationship ELSE numRelationshipId END)
		,@numDefaultProfile=(CASE WHEN ISNULL(@numDivisionID,0) > 0 THEN @numDefaultProfile ELSE numProfileId END)
		,@tintPreLoginPriceLevel=(CASE WHEN ISNULL(bitShowPriceUsingPriceLevel,0)=1 THEN (CASE WHEN @numDivisionID > 0 THEN (CASE WHEN ISNULL(bitShowPriceUsingPriceLevel,0)=1 THEN ISNULL(@tintPriceLevel,0) ELSE 0 END) ELSE ISNULL(tintPreLoginProceLevel,0) END) ELSE 0 END)
	FROM 
		eCommerceDTL 
	WHERE 
		numDomainID=@numDomainID

	IF @numWareHouseID=0
	BEGIN
		SELECT @numDefaultWareHouseID=ISNULL(numDefaultWareHouseID,0) FROM [eCommerceDTL] WHERE [numDomainID]=@numDomainID	
		SET @numWareHouseID =@numDefaultWareHouseID;
	END

	DECLARE @vcWarehouseIDs AS VARCHAR(1000)
	IF @bitAutoSelectWarehouse = 1
	BEGIN		
		SELECT @vcWarehouseIDs = ISNULL( STUFF((SELECT DISTINCT ',' + CAST(numWarehouseID AS VARCHAR(10)) FROM [dbo].[Warehouses] WHERE [Warehouses].[numDomainID] = @numDomainID FOR XML PATH('')),1, 1, '') , '')
	END
	ELSE
	BEGIN
		SET @vcWarehouseIDs = @numWareHouseID
	END

	/*If qty is not found in default warehouse then choose next warehouse with sufficient qty */
	IF @bitAutoSelectWarehouse = 1 AND ( SELECT COUNT(*) FROM dbo.WareHouseItems WHERE  numItemID = @numItemCode AND numWarehouseID= @numWareHouseID ) = 0 
	BEGIN
		SELECT TOP 1 
			@numDefaultWareHouseID = numWareHouseID 
		FROM 
			dbo.WareHouseItems 
		WHERE 
			numItemID = @numItemCode 
			AND numOnHand > 0 
		ORDER BY 
			numOnHand DESC 
		
		IF (ISNULL(@numDefaultWareHouseID,0)>0)
			SET @numWareHouseID =@numDefaultWareHouseID;
	END

    DECLARE @UOMConversionFactor AS DECIMAL(18,5)
      
    SELECT 
		@UOMConversionFactor= ISNULL(dbo.fn_UOMConversion(ISNULL(I.numSaleUnit,0),I.numItemCode,I.numDomainId,ISNULL(I.numBaseUnit,0)),0)
		,@bitMatrix=ISNULL(bitMatrix,0)
		,@numItemGroup=ISNULL(numItemGroup,0)
    FROM 
		Item I 
	WHERE 
		numItemCode=@numItemCode   	

	DECLARE @strSql1 AS NVARCHAR(MAX)
	DECLARE @strSql2 AS NVARCHAR(MAX)
	DECLARE @strSql3 AS NVARCHAR(MAX)
	DECLARE @strSql4 AS NVARCHAR(MAX)
	
	SET @strSql1 = CONCAT('SELECT 
								I.numItemCode
								,vcItemName
								,txtItemDesc
								,charItemType
								,ISNULL(I.bitKitParent,0) bitKitParent
								,ISNULL(bitCalAmtBasedonDepItems,0) bitCalAmtBasedonDepItems
								,ISNULL(I.bitMatrix,0) bitMatrix,(CASE WHEN (ISNULL(I.bitKitParent,0) = 1 OR ISNULL(I.bitAssembly,0) = 1) AND ISNULL(bitCalAmtBasedonDepItems,0) = 1 THEN dbo.GetCalculatedPriceForKitAssembly(I.numDomainID,',@numDivisionID,',I.numItemCode,1,0,ISNULL(I.tintKitAssemblyPriceBasedOn,1),0,0,'''',0,1) ELSE 
								',(CASE 
									WHEN @tintPreLoginPriceLevel > 0 
									THEN CONCAT('ISNULL((CASE 
											WHEN PricingOption.tintRuleType = 1 AND PricingOption.tintDiscountType = 1
											THEN ISNULL((CASE WHEN I.[charItemType] = ''P'' THEN (',@UOMConversionFactor,' * ISNULL(W.[monWListPrice],0)) ELSE (',@UOMConversionFactor,' * monListPrice) END),0) - (ISNULL((CASE WHEN I.[charItemType] = ''P'' THEN (',@UOMConversionFactor,' * ISNULL(W.[monWListPrice],0)) ELSE (',@UOMConversionFactor,' * monListPrice) END),0) * (PricingOption.decDiscount / 100 ) )
											WHEN PricingOption.tintRuleType = 1 AND PricingOption.tintDiscountType = 2
											THEN ISNULL((CASE WHEN I.[charItemType] = ''P'' THEN (',@UOMConversionFactor,' * ISNULL(W.[monWListPrice],0)) ELSE (',@UOMConversionFactor,' * monListPrice) END),0) - PricingOption.decDiscount
											WHEN PricingOption.tintRuleType = 2 AND PricingOption.tintDiscountType = 1
											THEN ISNULL(Vendor.monCost,0) + (ISNULL(Vendor.monCost,0) * (PricingOption.decDiscount / 100 ) )
											WHEN PricingOption.tintRuleType = 2 AND PricingOption.tintDiscountType = 2
											THEN ISNULL(Vendor.monCost,0) + PricingOption.decDiscount
											WHEN PricingOption.tintRuleType = 3
											THEN (',@UOMConversionFactor,' * PricingOption.decDiscount)
										END),ISNULL(CASE WHEN I.[charItemType]=''P'' THEN ',@UOMConversionFactor,' * W.[monWListPrice] ELSE ',@UOMConversionFactor,' * monListPrice END,0))') 
									ELSE CONCAT('ISNULL(CASE WHEN I.[charItemType]=''P'' THEN ',@UOMConversionFactor,' * W.[monWListPrice] ELSE ',@UOMConversionFactor,' * monListPrice END,0)')
								END),'END) AS monListPrice
								,(CASE WHEN (ISNULL(I.bitKitParent,0) = 1 OR ISNULL(I.bitAssembly,0) = 1) AND ISNULL(bitCalAmtBasedonDepItems,0) = 1 THEN dbo.GetCalculatedPriceForKitAssembly(I.numDomainID,',@numDivisionID,',I.numItemCode,1,0,ISNULL(I.tintKitAssemblyPriceBasedOn,1),0,0,'''',0,1) ELSE ISNULL(CASE WHEN I.[charItemType]=''P'' 
											THEN ',@UOMConversionFactor,' * W.[monWListPrice]  
											ELSE ',@UOMConversionFactor,' * monListPrice 
										END,0) END) AS monMSRP
										,numItemClassification
										,bitTaxable
										,vcSKU
										,I.numModifiedBy
										,(SELECT numItemImageId,vcPathForImage,vcPathForTImage,bitDefault,case when bitdefault=1  then -1 else isnull(intDisplayOrder,0) end as intDisplayOrder FROM ItemImages  WHERE numItemCode=I.numItemCode and bitIsImage = 1 order by case when bitdefault=1 then -1 else isnull(intDisplayOrder,0) end  asc FOR XML AUTO,ROOT(''Images''))  vcImages
										,(SELECT vcPathForImage FROM ItemImages  WHERE numItemCode=I.numItemCode and bitIsImage = 0 order by intDisplayOrder  asc FOR XML AUTO,ROOT(''Documents''))  vcDocuments
										,ISNULL(bitSerialized,0) as bitSerialized
										,vcModelID
										,numItemGroup
										,(ISNULL(sum(numOnHand),0) / ',@UOMConversionFactor,') as numOnHand
										,sum(numOnOrder) as numOnOrder,                    
										sum(numReorder)  as numReorder,                    
										sum(numAllocation)  as numAllocation,                    
										sum(numBackOrder)  as numBackOrder,              
										isnull(fltWeight,0) as fltWeight,              
										isnull(fltHeight,0) as fltHeight,              
										isnull(fltWidth,0) as fltWidth,              
										isnull(fltLength,0) as fltLength,              
										case when charItemType=''P''then isnull(bitFreeShipping,0) else 1 end as bitFreeShipping,
										Case When (case when charItemType=''P''then isnull(bitFreeShipping,0) else 1 end )=1 then ''Free shipping'' else ''Calculated at checkout'' end as Shipping,              
										isnull(bitAllowBackOrder,0) as bitAllowBackOrder,bitShowInStock,bitShowQOnHand,isnull(numWareHouseItemID,0)  as numWareHouseItemID,
										(CASE WHEN charItemType<>''S'' AND charItemType<>''N'' AND ISNULL(bitKitParent,0)=0 THEN         
										 (CASE 
											WHEN I.bitAllowBackOrder = 1 THEN 1
											WHEN (ISNULL(WAll.numTotalOnHand,0)<=0) THEN 0
											ELSE 1
										END) ELSE 1 END ) as bitInStock,
										(
											CASE WHEN ISNULL(bitKitParent,0) = 1
											THEN
												(CASE 
													WHEN ((SELECT COUNT(*) FROM ItemDetails ID INNER JOIN Item IInner ON ID.numChildItemID = IInner.numItemCode WHERE ID.numItemKitID = I.numItemCode AND ISNULL(IInner.bitKitParent,0) = 1)) > 0 
													THEN 1 
													ELSE 0 
												END)
											ELSE
												0
											END
										) bitHasChildKits,
										(case when charItemType<>''S'' AND charItemType<>''N'' AND ISNULL(bitKitParent,0)=0 then         
										 (Case when ',@bitShowInStock,'=1  then 
										 (CASE 
											WHEN I.bitAllowBackOrder = 1 AND I.numDomainID <> 172 AND ISNULL(WAll.numTotalOnHand,0)<=0 AND ISNULL(I.numVendorID,0) > 0 AND dbo.GetVendorPreferredMethodLeadTime(I.numVendorID,0) > 0 THEN CONCAT(''<font class="vendorLeadTime" style="color:green">Usually ships in '',dbo.GetVendorPreferredMethodLeadTime(I.numVendorID,0),'' days</font>'')
											WHEN I.bitAllowBackOrder = 1 AND ISNULL(WAll.numTotalOnHand,0)<=0 THEN (CASE WHEN I.numDomainID = 172 THEN ''<font style="color:green">AVAILABLE</font>'' ELSE ''<font style="color:green">In Stock</font>'' END)
											WHEN (ISNULL(WAll.numTotalOnHand,0)<=0) THEN (CASE WHEN I.numDomainID = 172 THEN ''<font style="color:red">ON HOLD</font>'' ELSE ''<font style="color:red">Out Of Stock</font>'' END)
											ELSE (CASE WHEN I.numDomainID = 172 THEN ''<font style="color:green">AVAILABLE</font>'' ELSE ''<font style="color:green">In Stock</font>'' END)  
										END) else '''' end) ELSE '''' END ) as InStock,
										ISNULL(numSaleUnit,0) AS numUOM,
										ISNULL(vcUnitName,'''') AS vcUOMName,
										 ',@UOMConversionFactor,' AS UOMConversionFactor,
										I.numCreatedBy,
										I.numBarCodeId,
										I.[vcManufacturer],
										(SELECT  COUNT(*) FROM  Review where vcReferenceId = CONVERT(VARCHAR(50) ,I.numItemCode) and numDomainId = I.numDomainId and numSiteId = ', @numSiteId,' AND bitHide = 0 ) as ReviewCount ,
										(SELECT  COUNT(*) FROM  Ratings where vcReferenceId = CONVERT(VARCHAR(50) ,I.numItemCode) and numDomainId = I.numDomainId and numSiteId = ', @numSiteId,'  ) as RatingCount ,
										(select top 1 vcMetaKeywords  from MetaTags where numReferenceID = I.numItemCode and tintMetaTagFor =2) as vcMetaKeywords,
										(select top 1 vcMetaDescription  from MetaTags where numReferenceID = I.numItemCode and tintMetaTagFor =2) as vcMetaDescription ,
										(SELECT vcCategoryName  FROM  dbo.Category WHERE numCategoryID =(SELECT TOP 1 numCategoryID FROM dbo.ItemCategory WHERE numItemID = I.numItemCode)) as vcCategoryName
										,ISNULL(TablePromotion.numTotalPromotions,0) numTotalPromotions
										,ISNULL(TablePromotion.vcPromoDesc,'''') vcPromoDesc
										,ISNULL(TablePromotion.bitRequireCouponCode,0) bitRequireCouponCode')
	SET @strSql2 = CONCAT(' from Item I 
										LEFT JOIN Vendor ON Vendor.numVendorID =I.numVendorID AND Vendor.numItemCode=I.numItemCode
										OUTER APPLY (SELECT * FROM dbo.fn_GetItemPromotions(',@numDomainID,',',@numDivisionID,',I.numItemCode,I.numItemClassification',',',@numDefaultRelationship,',',@numDefaultProfile,',2)) TablePromotion ' + 
										(CASE WHEN @tintPreLoginPriceLevel > 0 THEN CONCAT(' OUTER APPLY(SELECT tintRuleType,tintDiscountType,decDiscount FROM PricingTable WHERE numItemCode=I.numItemCode AND ISNULL(numPriceRuleID,0) = 0 AND ISNULL(numCurrencyID,0) = 0 ORDER BY numPricingID OFFSET ',@tintPreLoginPriceLevel-1,' ROWS FETCH NEXT 1 ROWS ONLY) As PricingOption ') ELSE '' END)
										,'                 
										left join  WareHouseItems W                
										on W.numItemID=I.numItemCode and numWareHouseID= ',@numWareHouseID,'
										 OUTER APPLY (SELECT SUM(numOnHand) numTotalOnHand FROM WareHouseItems W 
																							  WHERE  W.numItemID = I.numItemCode
																							  AND numWareHouseID IN (SELECT [OutParam] FROM dbo.[SplitString](''',@vcWarehouseIDs,''','',''))
																							  AND W.numDomainID = ',@numDomainID,') AS WAll
										left join  eCommerceDTL E          
										on E.numDomainID=I.numDomainID     
										LEFT JOIN UOM u ON u.numUOMId=I.numSaleUnit')

	SET @strSql3 = CONCAT(' where ISNULL(I.IsArchieve,0)=0 AND I.numDomainID=',@numDomainID,' AND I.numItemCode=',@numItemCode,
										CASE WHEN (SELECT COUNT(*) FROM [dbo].[EcommerceRelationshipProfile] WHERE numSiteID=@numSiteID AND numRelationship = @numDefaultRelationship AND numProfile = @numDefaultProfile) > 0
										THEN
											CONCAT(' AND I.numItemClassification NOT IN (SELECT numItemClassification FROM EcommerceRelationshipProfile ERP INNER JOIN ECommerceItemClassification EIC ON EIC.numECommRelatiionshipProfileID=ERP.ID WHERE ERP.numSiteID=',@numSiteID,' AND ERP.numRelationship=',@numDefaultRelationship,' AND ERP.numProfile=', @numDefaultProfile,')')
										ELSE
											''
										END
										,' GROUP BY I.numItemCode, vcItemName, txtItemDesc, charItemType, monListPrice,numItemClassification, bitTaxable, vcSKU, bitKitParent, bitCalAmtBasedonDepItems, bitAssembly, tintKitAssemblyPriceBasedOn, WAll.numTotalOnHand,         
										I.numDomainID,I.numModifiedBy,  bitSerialized, vcModelID,numItemGroup, fltWeight,fltHeight,fltWidth,          
										fltLength,bitFreeShipping,bitAllowBackOrder,bitShowInStock,bitShowQOnHand ,numWareHouseItemID,vcUnitName,I.numCreatedBy,I.numBarCodeId,W.[monWListPrice],I.[vcManufacturer],
										numSaleUnit, I.numVendorID, I.bitMatrix,numTotalPromotions,vcPromoDesc,bitRequireCouponCode,Vendor.monCost ',(CASE WHEN @tintPreLoginPriceLevel > 0 THEN ',PricingOption.tintRuleType,PricingOption.tintDiscountType,PricingOption.decDiscount' ELSE '' END))

	SET @strSql4 = ' select numItemCode,vcItemName,txtItemDesc,charItemType, bitMatrix, monListPrice,monMSRP,numItemClassification, bitTaxable, vcSKU, bitKitParent, bitCalAmtBasedonDepItems,                  
										tblItem.numModifiedBy, vcImages,vcDocuments, bitSerialized, vcModelID, numItemGroup,numOnHand,numOnOrder,numReorder,numAllocation, 
										numBackOrder, fltWeight, fltHeight, fltWidth, fltLength, bitFreeShipping,bitAllowBackOrder,bitShowInStock,
										bitShowQOnHand,numWareHouseItemID,bitInStock,bitHasChildKits,InStock,numUOM,vcUOMName,UOMConversionFactor,tblItem.numCreatedBy,numBarCodeId,vcManufacturer,ReviewCount,RatingCount,Shipping,isNull(vcMetaKeywords,'''') as vcMetaKeywords ,isNull(vcMetaDescription,'''') as vcMetaDescription,isNull(vcCategoryName,'''') as vcCategoryName,numTotalPromotions,vcPromoDesc,bitRequireCouponCode from tblItem ORDER BY numOnHand DESC'


	PRINT CAST(CONCAT('WITH tblItem AS (',@strSql1,@strSql2,@strSql3,')',@strSql4) AS NTEXT)

	exec ('WITH tblItem AS (' + @strSql1 + @strSql2 + @strSql3 + ')' + @strSql4)


	declare @tintOrder as tinyint                                                  
	declare @vcFormFieldName as varchar(50)                                                  
	declare @vcListItemType as varchar(1)                                             
	declare @vcAssociatedControlType varchar(10)                                                  
	declare @numListID AS numeric(9)                                                  
	declare @WhereCondition varchar(2000)                       
	Declare @numFormFieldId as numeric  
	DECLARE @vcFieldType CHAR(1)
	DECLARE @GRP_ID INT
                  
	set @tintOrder=0                                                  
	set @WhereCondition =''                 
                   
              
	CREATE TABLE #tempAvailableFields
	(
		numFormFieldId NUMERIC(9)
		,vcFormFieldName NVARCHAR(50)
		,vcFieldType CHAR(1)
		,vcAssociatedControlType NVARCHAR(50)
		,numListID NUMERIC(18)
		,vcListItemType CHAR(1)
		,intRowNum INT
		,vcItemValue VARCHAR(3000)
		,GRP_ID INT
	)
   
	INSERT INTO #tempAvailableFields 
	(
		numFormFieldId
		,vcFormFieldName
		,vcFieldType
		,vcAssociatedControlType
        ,numListID
		,vcListItemType
		,intRowNum
		,GRP_ID
	)                         
    SELECT 
		Fld_id
		,REPLACE(Fld_label,' ','') AS vcFormFieldName
		,CASE GRP_ID WHEN 4 then 'C' WHEN 1 THEN 'D' ELSE 'C' END AS vcFieldType
		,fld_type as vcAssociatedControlType
		,ISNULL(C.numListID, 0) numListID
		,CASE WHEN C.numListID > 0 THEN 'L' ELSE '' END vcListItemType
		,ROW_NUMBER() OVER (ORDER BY Fld_id asc) AS intRowNum
		,GRP_ID
    FROM 
		CFW_Fld_Master C 
	LEFT JOIN 
		CFW_Validation V 
	ON 
		V.numFieldID = C.Fld_id
    WHERE 
		C.numDomainID = @numDomainId
        AND GRP_ID  = 5

    SELECT TOP 1 
		@tintOrder=intRowNum
		,@numFormFieldId=numFormFieldId
		,@vcFormFieldName=vcFormFieldName
		,@vcFieldType=vcFieldType
		,@vcAssociatedControlType=vcAssociatedControlType
		,@numListID=numListID
		,@vcListItemType=vcListItemType
		,@GRP_ID=GRP_ID
	FROM 
		#tempAvailableFields 
	ORDER BY 
		intRowNum ASC
   
	while @tintOrder>0                                                  
	begin                   
		IF @GRP_ID=5
		BEGIN
			IF @vcAssociatedControlType = 'TextBox' or @vcAssociatedControlType = 'TextArea'
			BEGIN  
				UPDATE #tempAvailableFields SET vcItemValue=(SELECT Fld_Value FROM CFW_FLD_Values_Item WHERE Fld_Id=@numFormFieldId AND RecId=@numItemCode) WHERE numFormFieldId=@numFormFieldId              
			END   
			ELSE IF @vcAssociatedControlType = 'CheckBox'
			BEGIN      
				UPDATE #tempAvailableFields SET vcItemValue=(SELECT case when isnull(Fld_Value,0)=0 then 'No' when isnull(Fld_Value,0)=1 then 'Yes' END FROM CFW_FLD_Values_Item WHERE Fld_Id=@numFormFieldId AND RecId=@numItemCode) WHERE numFormFieldId=@numFormFieldId              
			END                
			ELSE IF @vcAssociatedControlType = 'DateField'           
			BEGIN   
				UPDATE #tempAvailableFields SET vcItemValue=(SELECT dbo.FormatedDateFromDate(Fld_Value,@numDomainId) FROM CFW_FLD_Values_Item WHERE Fld_Id=@numFormFieldId AND RecId=@numItemCode) WHERE numFormFieldId=@numFormFieldId              
			END                
			ELSE IF @vcAssociatedControlType = 'SelectBox'           
			BEGIN 
				UPDATE #tempAvailableFields SET vcItemValue=(SELECT L.vcData FROM CFW_FLD_Values_Item CFW left Join ListDetails L
					on L.numListItemID=CFW.Fld_Value                
					WHERE CFW.Fld_Id=@numFormFieldId AND CFW.RecId=@numItemCode) WHERE numFormFieldId=@numFormFieldId              
			END   
		END    
 
		SELECT TOP 1 
			@tintOrder=intRowNum
			,@numFormFieldId=numFormFieldId
			,@vcFormFieldName=vcFormFieldName
			,@vcFieldType=vcFieldType
			,@vcAssociatedControlType=vcAssociatedControlType
			,@numListID=numListID
			,@vcListItemType=vcListItemType
			,@GRP_ID=GRP_ID
		FROM 
			#tempAvailableFields 
		WHERE 
			intRowNum > @tintOrder 
		ORDER BY 
			intRowNum ASC
 
	   IF @@rowcount=0 set @tintOrder=0                                                  
	END   


	INSERT INTO #tempAvailableFields 
	(
		numFormFieldId
		,vcFormFieldName
		,vcFieldType
		,vcAssociatedControlType
		,numListID
		,vcListItemType
		,intRowNum
		,vcItemValue
		,GRP_ID
	)                         
	SELECT 
		Fld_id
		,REPLACE(Fld_label,' ','') AS vcFormFieldName
		,CASE GRP_ID WHEN 4 then 'C' WHEN 1 THEN 'D' ELSE 'C' END AS vcFieldType
		,fld_type as vcAssociatedControlType
		,ISNULL(C.numListID, 0) numListID
		,CASE WHEN C.numListID > 0 THEN 'L' ELSE '' END vcListItemType
		,ROW_NUMBER() OVER (ORDER BY Fld_id asc) AS intRowNum
		,''
		,GRP_ID
	FROM 
		CFW_Fld_Master C 
	LEFT JOIN 
		CFW_Validation V 
	ON 
		V.numFieldID = C.Fld_id
	WHERE 
		C.numDomainID = @numDomainId
		AND GRP_ID = 9

	IF ISNULL(@bitMatrix,0) = 1 AND ISNULL(@numItemGroup,0) > 0
	BEGIN
		UPDATE
			TEMP
		SET
			TEMP.vcItemValue = FLD_ValueName
		FROM
			#tempAvailableFields TEMP
		INNER JOIN
		(
			SELECT 
				CFW_Fld_Master.fld_id
				,CASE 
					WHEN CFW_Fld_Master.fld_type='SelectBox' THEN dbo.GetListIemName(Fld_Value)
					WHEN CFW_Fld_Master.fld_type='CheckBox' THEN CASE WHEN isnull(Fld_Value,0)=1 THEN 'Yes' ELSE 'No' END
					ELSE CAST(Fld_Value AS VARCHAR)
				END AS FLD_ValueName
			FROM 
				CFW_Fld_Master 
			INNER JOIN
				ItemGroupsDTL 
			ON 
				CFW_Fld_Master.Fld_id = ItemGroupsDTL.numOppAccAttrID
				AND ItemGroupsDTL.tintType = 2
			LEFT JOIN
				ItemAttributes
			ON
				CFW_Fld_Master.Fld_id = ItemAttributes.FLD_ID
				AND ItemAttributes.numItemCode = @numItemCode
			LEFT JOIN
				ListDetails LD
			ON
				CFW_Fld_Master.numlistid = LD.numListID
			WHERE
				CFW_Fld_Master.numDomainID = @numDomainId
				AND ItemGroupsDTL.numItemGroupID = @numItemGroup
			GROUP BY
				CFW_Fld_Master.Fld_label
				,CFW_Fld_Master.fld_id
				,CFW_Fld_Master.fld_type
				,CFW_Fld_Master.bitAutocomplete
				,CFW_Fld_Master.numlistid
				,CFW_Fld_Master.vcURL
				,ItemAttributes.FLD_Value 
		) T1
		ON
			TEMP.numFormFieldId = T1.fld_id
		WHERE
			TEMP.GRP_ID=9
	END
  
	SELECT * FROM #tempAvailableFields

	DROP TABLE #tempAvailableFields
END
GO
/****** Object:  StoredProcedure [dbo].[USP_ItemPricingRecomm]    Script Date: 02/19/2009 01:33:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- exec USP_ItemPricingRecomm @numItemCode=173028,@units=1,@numOppID=0,@numDivisionID=7052,@numDomainID=72,@tintOppType=1,@CalPrice=$50,@numWareHouseItemID=130634,@bitMode=0
--created by anoop jayaraj                          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_itempricingrecomm')
DROP PROCEDURE usp_itempricingrecomm
GO
CREATE PROCEDURE [dbo].[USP_ItemPricingRecomm]                                        
@numItemCode as numeric(9),                                        
@units Float,                                  
@numOppID as numeric(9)=0,                                
@numDivisionID as numeric(9)=0,                  
@numDomainID as numeric(9)=0,            
@tintOppType as tinyint,          
@CalPrice as DECIMAL(20,5),      
@numWareHouseItemID as numeric(9),
@bitMode tinyint=0,
@vcSelectedKitChildItems VARCHAR(MAX) = '',
@numOppItemID NUMERIC(18,0) = 0
as                 
BEGIN TRY

DECLARE @numRelationship AS NUMERIC(9)
DECLARE @numProfile AS NUMERIC(9)             
DECLARE @monListPrice DECIMAL(20,5)
DECLARE @bitCalAmtBasedonDepItems BIT
DECLARE @numDefaultSalesPricing TINYINT
DECLARE @tintPriceLevel INT
DECLARE @decmUOMConversion decimal(18,2)
DECLARE @intLeadTimeDays INT
DECLARE @intMinQty INT
DECLARE @tintKitAssemblyPriceBasedOn TINYINT

/*Profile and relationship id */
SELECT 
	@numRelationship=numCompanyType,
	@numProfile=vcProfile,
	@tintPriceLevel = ISNULL(D.tintPriceLevel,0)
FROM 
	DivisionMaster D                  
JOIN 
	CompanyInfo C 
ON 
	C.numCompanyId=D.numCompanyID                  
WHERE 
	numDivisionID =@numDivisionID
            
IF @tintOppType=1            
BEGIN
	SELECT
		@bitCalAmtBasedonDepItems=ISNULL(bitCalAmtBasedonDepItems,0)
		,@tintKitAssemblyPriceBasedOn=ISNULL(tintKitAssemblyPriceBasedOn,1)
	FROM 
		Item 
	WHERE 
		numItemCode = @numItemCode
      
	/*Get List Price for item*/      
	IF((@numWareHouseItemID>0) AND EXISTS(SELECT * FROM Item WHERE numItemCode=@numItemCode AND charItemType='P'))      
	BEGIN      
		SELECT 
			@monListPrice=ISNULL(monWListPrice,0) 
		FROM 
			WareHouseItems 
		WHERE 
			numWareHouseItemID=@numWareHouseItemID      
		
		IF @monListPrice=0 
		BEGIN
			SELECT @monListPrice=monListPrice FROM Item WHERE numItemCode=@numItemCode       
		END
	END 
	ELSE
	BEGIN
		 SELECT 
			@monListPrice=monListPrice 
		FROM 
			Item 
		WHERE 
			numItemCode=@numItemCode      
	END      

	/*Use Calculated Price When item is Assembly/Kit,Calculate price based on sum of dependent items.*/
	IF @bitCalAmtBasedonDepItems = 1 
	BEGIN
		DECLARE @TEMPPrice TABLE
		(
			bitSuccess BIT
			,monPrice DECIMAL(20,5)
		)
		INSERT INTO @TEMPPrice
		(
			bitSuccess
			,monPrice
		)
		SELECT
			bitSuccess
			,monMSRPPrice
		FROM
			dbo.fn_GetKitAssemblyCalculatedPrice(@numDomainID,@numDivisionID,@numItemCode,@units,@numWarehouseItemID,@tintKitAssemblyPriceBasedOn,@numOppID,@numOppItemID,@vcSelectedKitChildItems,0,1)

		IF (SELECT bitSuccess FROM @TEMPPrice) = 1
		BEGIN
			SET @CalPrice = (SELECT monPrice FROM @TEMPPrice)
		END
		ELSE
		BEGIN
			RAISERROR('SELECTED_WAREHOUSE_IS_NOT_AVAILABLE_FOR_ALL_CHILD_ITEMS',16,1)
			RETURN
		END
	END


	SELECT 
		@numDefaultSalesPricing = ISNULL(numDefaultSalesPricing,2) 
	FROM 
		Domain 
	WHERE 
		numDomainID = @numDomainID

	IF @numDefaultSalesPricing = 1 -- Use Price Level
	BEGIN
		DECLARE @newPrice DECIMAL(20,5)
		DECLARE @finalUnitPrice FLOAT = 0
		DECLARE @tintRuleType INT
		DECLARE @tintDiscountType INT
		DECLARE @decDiscount FLOAT
		DECLARE @ItemPrice FLOAT

		SET @tintRuleType = 0
		SET @tintDiscountType = 0
		SET @decDiscount  = 0
		SET @ItemPrice = 0

		IF ISNULL(@tintPriceLevel,0) > 0
		BEGIN
			SELECT
				@tintRuleType = tintRuleType,
				@tintDiscountType = tintDiscountType,
				@decDiscount = decDiscount
			FROM
			(
				SELECT 
					ROW_NUMBER() OVER(PARTITION BY PricingTable.numItemCode ORDER BY numPricingID) Id,
					tintRuleType,
					tintDiscountType,
					decDiscount
				FROM 
					PricingTable 
				WHERE 
					PricingTable.numItemCode = @numItemCode
					AND ISNULL(numCurrencyID,0) = 0
			) TEMP
			WHERE
				Id = @tintPriceLevel
		END
		ELSE
		BEGIN
			SELECT TOP 1
				@tintRuleType = tintRuleType,
				@tintDiscountType = tintDiscountType,
				@decDiscount = decDiscount
			FROM 
				PricingTable 
			WHERE 
				numItemCode = @numItemCode 
				AND ISNULL(numCurrencyID,0) = 0
				AND (@units BETWEEN intFromQty AND intToQty)
		END

		IF @tintRuleType > 0 AND (@tintDiscountType > 0 OR @tintRuleType = 3)
		BEGIN
			IF @tintRuleType = 1 -- Deduct from List price
			BEGIN
				IF (SELECT ISNULL(charItemType,'') FROM Item WHERE numDomainID = @numDomainID AND numItemCode = @numItemCode) = 'P'
					If @bitCalAmtBasedonDepItems = 1 
						SELECT @ItemPrice = @CalPrice
					ELSE
						SELECT @ItemPrice = ISNULL(monWListPrice,0) FROM WareHouseItems WHERE numDomainID = @numDomainID AND numItemID = @numItemCode AND numWareHouseItemID = @numWarehouseItemID
				ELSE
					SELECT @ItemPrice = ISNULL(monListPrice,0) FROM Item WHERE numDomainID = @numDomainID AND numItemCode = @numItemCode

				IF @tintDiscountType = 1 -- Percentage
				BEGIN
					IF @decDiscount > 0
						SELECT @finalUnitPrice = @ItemPrice - (@ItemPrice * (@decDiscount/100))
				END
				ELSE IF @tintDiscountType = 2 -- Flat Amount
				BEGIN
					SELECT @finalUnitPrice = @ItemPrice - @decDiscount
				END
				ELSE IF @tintDiscountType = 3 -- Named Price
				BEGIN
					SELECT @finalUnitPrice = @decDiscount
				END
			END
			ELSE IF @tintRuleType = 2 -- Add to Primary Vendor Cost
			BEGIN
				If @bitCalAmtBasedonDepItems = 1 
					SELECT @ItemPrice = @CalPrice
				ELSE
					SELECT @ItemPrice = ISNULL(monCost,0) FROM Vendor WHERE numItemCode = @numItemCode AND numVendorID = (SELECT numVendorID FROM Item WHERE numItemCode = @numItemCode)

				IF @tintDiscountType = 1 AND @ItemPrice > 0 -- Percentage
				BEGIN
					IF @decDiscount > 0
						SELECT @finalUnitPrice = @ItemPrice + (@ItemPrice * (@decDiscount/100))
				END
				ELSE IF @tintDiscountType = 2 AND @ItemPrice > 0 -- Flat Amount
				BEGIN
					SELECT @finalUnitPrice = @ItemPrice + @decDiscount
				END
				ELSE IF @tintDiscountType = 3 -- Named Price
				BEGIN
					SELECT @finalUnitPrice = @decDiscount
				END
			END
			ELSE IF @tintRuleType = 3 -- Named Price
			BEGIN
				IF ISNULL(@decDiscount,0) = 0
				BEGIN
					SET @finalUnitPrice = @monListPrice
				END
				ELSE
				BEGIN
					SET @finalUnitPrice = @decDiscount
					SET @monListPrice = @finalUnitPrice
				END
				
				-- KEEP THIS LINE AT END
				SET @decDiscount = 0
				SET @tintDiscountType = 2		
			END
		END

		IF @finalUnitPrice = 0
			SET @newPrice = @monListPrice
		ELSE
			SET @newPrice = @finalUnitPrice

		If @tintRuleType = 2
		BEGIN
			IF @finalUnitPrice > 0
			BEGIN
				IF @tintDiscountType = 1 -- Percentage
				BEGIN
					IF @newPrice > @monListPrice
						SET @decDiscount = 0
					ELSE
						If @monListPrice > 0
							SET @decDiscount = ((@monListPrice - @newPrice) * 100) / CAST(@monListPrice AS FLOAT)
						ELSE
							SET @decDiscount = 0
				END
				ELSE IF @tintDiscountType = 2 -- Flat Amount
				BEGIN
					If @newPrice > @monListPrice
						SET @decDiscount = 0
					ELSE
						SET @decDiscount = @monListPrice - @newPrice
				END
			END
			ELSE
			BEGIN
				SET @decDiscount = 0
				SET @tintDiscountType = 0
			END
		END

		SELECT 
			1 as numUintHour, 
			ISNULL(@newPrice,0) AS ListPrice,
			'' AS vcPOppName,
			'' AS bintCreatedDate,
			CASE 
				WHEN @tintRuleType = 0 THEN 'Price - List price'
				ELSE 
				   CASE @tintRuleType
				   WHEN 1 
						THEN 
							'Deduct from List price ' +  CASE 
														   WHEN @tintDiscountType = 1 THEN CONVERT(VARCHAR(20), @decDiscount) + '%'
														   WHEN @tintDiscountType = 2 THEN 'flat ' + CONVERT(VARCHAR(20), CAST(@decDiscount AS int))
														   ELSE 'Name price ' + CONVERT(VARCHAR(20), CAST(@decDiscount AS int))
														   END 
				   WHEN 2 THEN 'Add to primary vendor cost '  +  CASE 
														   WHEN @tintDiscountType = 1 THEN CONVERT(VARCHAR(20), @decDiscount) + '%'
														   WHEN @tintDiscountType = 2 THEN 'flat ' + CONVERT(VARCHAR(20), CAST(@decDiscount AS int))
														   ELSE 'Name price ' + CONVERT(VARCHAR(20), CAST(@decDiscount AS int))
														   END 
				   ELSE 'Flat ' + CONVERT(VARCHAR(20), @decDiscount)
				   END
			END AS OppStatus,
			CAST(0 AS NUMERIC) AS numPricRuleID,
			CAST(1 AS TINYINT) AS tintPricingMethod,
			CAST(@tintDiscountType AS TINYINT) AS tintDiscountTypeOriginal,
			CAST(@decDiscount AS FLOAT) AS decDiscountOriginal,
			CAST(@tintRuleType AS TINYINT) AS tintRuleType
	END
	ELSE -- Use Price Rule
	BEGIN
		/* Checks Pricebook if exist any.. */      
		SELECT TOP 1 1 AS numUnitHour,
				dbo.GetPriceBasedOnPriceBook(P.[numPricRuleID],CASE WHEN @bitCalAmtBasedonDepItems = 1 THEN @CalPrice ELSE @monListPrice END,@units,I.numItemCode) 
				* (CASE WHEN (P.tintRuleType=2 OR (P.tintRuleType=0 AND ISNULL(PBT.tintRuleType,0) = 2)) THEN dbo.fn_UOMConversion(numBaseUnit,@numItemCode,@numDomainId,CASE WHEN ISNULL(numPurchaseUnit,0)>0 THEN numPurchaseUnit ELSE numBaseUnit END) ELSE 1 END)  AS ListPrice,
				vcRuleName AS vcPOppName,
				CONVERT(VARCHAR(20), NULL) AS bintCreatedDate,
				CASE WHEN numPricRuleID IS NULL THEN 'Price - List price'
					 ELSE 
					  CASE WHEN P.tintPricingMethod = 1
										 THEN 'Price Book Rule'
										 ELSE CASE PBT.tintRuleType
												WHEN 1 THEN 'Deduct from List price '
												WHEN 2 THEN 'Add to primary vendor cost '
												ELSE ''
											  END
											  + CASE 
													WHEN tintDiscountType = 1 THEN CONVERT(VARCHAR(20), decDiscount) + '%'
													WHEN tintDiscountType = 2 THEN 'flat ' + CONVERT(VARCHAR(20), CAST(decDiscount AS int))
													ELSE 'Named price ' + CONVERT(VARCHAR(20), CAST(decDiscount AS int))
												END + ' for every '
											  + CONVERT(VARCHAR(20), CASE WHEN [intQntyItems] = 0 THEN 1
																		  ELSE intQntyItems
																	 END)
											  + ' units, until maximum of '
											  + CONVERT(VARCHAR(20), decMaxDedPerAmt)
											  + CASE WHEN tintDiscountType = 1 THEN ' %' ELSE ' Flat' END END
				END AS OppStatus,P.numPricRuleID,P.tintPricingMethod,PBT.tintDiscountTypeOriginal,CAST(PBT.decDiscountOriginal AS FLOAT) AS decDiscountOriginal,PBT.tintRuleType
		FROM    Item I
				LEFT JOIN PriceBookRules P ON I.numDomainID = P.numDomainID
				LEFT JOIN PriceBookRuleDTL PDTL ON P.numPricRuleID = PDTL.numRuleID
				LEFT JOIN PriceBookRuleItems PBI ON P.numPricRuleID = PBI.numRuleID
				LEFT JOIN [PriceBookPriorities] PP ON PP.[Step2Value] = P.[tintStep2] AND PP.[Step3Value] = P.[tintStep3]
				CROSS APPLY dbo.GetPriceBasedOnPriceBookTable(P.[numPricRuleID],CASE WHEN bitCalAmtBasedonDepItems=1 then @CalPrice ELSE @monListPrice END ,@units,I.numItemCode) as PBT
		WHERE   numItemCode = @numItemCode AND tintRuleFor=@tintOppType
		AND 
		(
		((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 1 AND PDTL.numValue = @numDivisionID)) -- Priority 1
		OR ((tintStep2 = 2 AND PBI.numValue = I.numItemClassification) AND (tintStep3= 1 AND PDTL.numValue = @numDivisionID)) -- Priority 2
		OR ((tintStep2 = 3) AND (tintStep3 = 1 AND PDTL.numValue = @numDivisionID)) -- Priority 3

		OR ((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 4
		OR ((tintStep2 = 2 AND PBI.numValue = I.numItemClassification) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 5
		OR ((tintStep2 = 3) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 6

		OR ((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 3)) -- Priority 7
		OR ((tintStep2 = 2 AND PBI.numValue = I.numItemClassification) AND (tintStep3 = 3)) -- Priority 8
		OR ((tintStep2 = 3) and (tintStep3 = 3)) -- Priority 9
		)
		ORDER BY PP.Priority ASC
	END

	IF @bitMode=2  -- When this procedure is being called from USP_GetPriceOfItem for e-com
		RETURN 

                                   
	select 1 as numUnitHour,convert(DECIMAL(20,5),ISNULL((monTotAmount/(case when numUnitHour< 1 then 1 else numUnitHour end)),0)) as ListPrice,vcPOppName,convert(varchar(20),bintCreatedDate) as bintCreatedDate,case when tintOppStatus=0 then 'Opportunity' when tintOppStatus=1 then 'Deal won'   
	when tintOppStatus=2 then 'Deal Lost' end as OppStatus,CAST(0 AS NUMERIC) AS numPricRuleID,CAST(0 AS TINYINT) AS tintPricingMethod,CAST(0 AS TINYINT) as tintDiscountTypeOriginal,CAST(0 AS FLOAT) as decDiscountOriginal,CAST(0 AS TINYINT) as tintRuleType                         
	from OpportunityItems itm                                        
	join OpportunityMaster mst                                        
	on mst.numOppId=itm.numOppId              
	where  tintOppType=1 and numItemCode=@numItemCode and mst.numDivisionID=@numDivisionID order by OppStatus desc  
  
  
	select 1 as numUnitHour,CASE WHEN @bitCalAmtBasedonDepItems=1 THEN convert(DECIMAL(20,5),ISNULL(@CalPrice,0)) ELSE convert(DECIMAL(20,5),ISNULL(@monListPrice,0)) End as ListPrice,'' as vcPOppName,convert(varchar(20),null) as bintCreatedDate,CASE WHEN @bitCalAmtBasedonDepItems=1 THEN 'Sum of dependent items' ELSE '' END as OppStatus
	,CAST(0 AS NUMERIC) AS numPricRuleID,CAST(0 AS TINYINT) AS tintPricingMethod ,CAST(0 AS TINYINT) as tintDiscountTypeOriginal,CAST(0 AS FLOAT) as decDiscountOriginal,CAST(0 AS TINYINT) as tintRuleType                    
END             
ELSE            
BEGIN

	If @numDivisionID=0
	BEGIN
		SELECT 
			@numDivisionID=V.numVendorID 
		FROM 
			[Vendor] V 
		INNER JOIN 
			Item I 
		ON 
			V.numVendorID = I.numVendorID 
			AND V.numItemCode=I.numItemCode 
		WHERE 
			V.[numItemCode]=@numItemCode 
			AND V.[numDomainID]=@numDomainID 
	END

	IF EXISTS
	(
		SELECT 
			ISNULL([monCost],0) ListPrice 
		FROM 
			[Vendor] V 
		INNER JOIN 
			dbo.Item I 
		ON 
			V.numItemCode = I.numItemCode
		WHERE
			V.[numItemCode]=@numItemCode 
			AND V.[numDomainID]=@numDomainID 
			AND V.numVendorID=@numDivisionID
	)       
	BEGIN      
		SELECT 
			@monListPrice=ISNULL([monCost],0) 
		FROM 
			[Vendor] V 
		INNER JOIN 
			dbo.Item I 
		ON 
			V.numItemCode = I.numItemCode
		WHERE 
			V.[numItemCode]=@numItemCode 
			AND V.[numDomainID]=@numDomainID 
			AND V.numVendorID=@numDivisionID
	END      
	ELSE
	BEGIN   
		SELECT 
			@monListPrice=ISNULL([monCost],0) 
		FROM 
			[Vendor] 
		WHERE 
			[numItemCode]=@numItemCode 
			AND [numDomainID]=@numDomainID
	END      
 

	SELECT TOP 1 
		1 AS numUnitHour,
		ISNULL(dbo.GetPriceBasedOnPriceBook(P.[numPricRuleID],@monListPrice,@units,I.numItemCode),0) AS ListPrice,
		vcRuleName AS vcPOppName,
		CONVERT(VARCHAR(20), NULL) AS bintCreatedDate,
		CASE 
		WHEN numPricRuleID IS NULL THEN 'Price - List price'
		ELSE  
				CASE 
				WHEN P.tintPricingMethod = 1 THEN 'Price Book Rule'
				ELSE 
					(CASE tintRuleType
						WHEN 1 THEN 'Deduct from List price '
						WHEN 2 THEN 'Add to primary vendor cost '
						ELSE ''
					END) + 
					(CASE 
						WHEN tintDiscountType = 1 THEN CONVERT(VARCHAR(20), decDiscount) + '%'
						ELSE 'flat ' + CONVERT(VARCHAR(20), CAST(decDiscount AS int))
					END) + ' for every ' + 
					CONVERT(VARCHAR(20), (CASE WHEN [intQntyItems] = 0 THEN 1 ELSE intQntyItems END))
					+ ' units, until maximum of '
					+ CONVERT(VARCHAR(20), decMaxDedPerAmt)
					+ (CASE WHEN tintDiscountType = 1 THEN ' %' ELSE ' Flat' END)
				END
		END AS OppStatus,
		P.numPricRuleID,
		P.tintPricingMethod,
		@numDivisionID AS numDivisionID
	FROM
		Item I 
	LEFT JOIN PriceBookRules P ON I.numDomainID = P.numDomainID
	LEFT JOIN PriceBookRuleDTL PDTL ON P.numPricRuleID = PDTL.numRuleID
	LEFT JOIN PriceBookRuleItems PBI ON P.numPricRuleID = PBI.numRuleID
	LEFT JOIN [PriceBookPriorities] PP ON PP.[Step2Value] = P.[tintStep2] AND PP.[Step3Value] = P.[tintStep3]
	WHERE   
		numItemCode = @numItemCode 
		AND tintRuleFor=@tintOppType
		AND (
			((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 1 AND PDTL.numValue = @numDivisionID)) -- Priority 1
			OR ((tintStep2 = 2 AND PBI.numValue = I.numItemClassification) AND (tintStep3= 1 AND PDTL.numValue = @numDivisionID)) -- Priority 2
			OR ((tintStep2 = 3) AND (tintStep3 = 1 AND PDTL.numValue = @numDivisionID)) -- Priority 3

			OR ((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 4
			OR ((tintStep2 = 2 AND PBI.numValue = I.numItemClassification) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 5
			OR ((tintStep2 = 3) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 6

			OR ((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 3)) -- Priority 7
			OR ((tintStep2 = 2 AND PBI.numValue = I.numItemClassification) AND (tintStep3 = 3)) -- Priority 8
			OR ((tintStep2 = 3) and (tintStep3 = 3)) -- Priority 9
			)
	ORDER BY PP.Priority ASC

	SET @decmUOMConversion=(SELECT 
								dbo.fn_UOMConversion(ISNULL(I.numPurchaseUnit, 0) , I.numItemCode,I.numDomainId, ISNULL(I.numBaseUnit, 0)) 
							FROM 
								Item I 
							WHERE 
								I.numItemCode=@numItemCode)

	SELECT
		@intLeadTimeDays = ISNULL(VSM.numListValue,0),
		@intMinQty=ISNULL(intMinQty,0)
	FROM
		Item
	JOIN
		Vendor
	ON
		Item.numVendorID = Vendor.numVendorID
		AND Item.numItemCode=Vendor.numItemCode
	LEFT JOIN
		VendorShipmentMethod VSM
	ON
		Vendor.numVendorID = VSM.numVendorID
		AND VSM.numDomainID=@numDomainID
		AND VSM.bitPrimary = 1
		AND VSM.bitPreferredMethod = 1
	WHERE
		Item.numItemCode = @numItemCode

	select 1 as numUnitHour,convert(DECIMAL(20,5),ISNULL((monTotAmount/(case when numUnitHour< 1 then 1 else numUnitHour end)),0)) as ListPrice,
	@decmUOMConversion *(convert(DECIMAL(20,5),ISNULL((monTotAmount/(case when numUnitHour< 1 then 1 else numUnitHour end)),0)))
	as convrsPrice
	,vcPOppName,convert(varchar(20),bintCreatedDate) as bintCreatedDate,case when tintOppStatus=0 then 'Opportunity' 
		 when tintOppStatus=1 then 'Deal won'  
		 when tintOppStatus=2 then 'Deal Lost' end as OppStatus
		 ,CAST(0 AS NUMERIC) AS numPricRuleID,CAST(0 AS TINYINT) AS tintPricingMethod,@numDivisionID as numDivisionID                         
		 from OpportunityItems itm                                        
		 join OpportunityMaster mst                                        
		 on mst.numOppId=itm.numOppId                                        
		 where  tintOppType=2 and numItemCode=@numItemCode and mst.numDivisionID=@numDivisionID order by OppStatus,bintCreatedDate DESC

 		select 1 as numUnitHour,convert(DECIMAL(20,5),ISNULL(@monListPrice,0)) as ListPrice,
		@decmUOMConversion*(Convert(DECIMAL(20,5),ISNULL(@monListPrice,0))) as convrsPrice
		,@intLeadTimeDays as LeadTime,@intMinQty as intMinQty,
		(SELECT DATEADD(day,@intLeadTimeDays,GETDATE())) AS EstimatedArrival
		,'' as vcPOppName,convert(varchar(20),null) as bintCreatedDate,''  as OppStatus,@numDivisionID as numDivisionID
 		,CAST(0 AS NUMERIC) AS numPricRuleID,CAST(0 AS TINYINT) AS tintPricingMethod
END

END TRY
BEGIN CATCH
    SELECT 0 AS ListPrice
    SELECT 0 AS ListPrice
    SELECT 0 AS ListPrice
END CATCH
--SELECT * FROM Category
--- Alterd By Chintan Prajapati
-- EXEC dbo.USP_ItemsForECommerce 9,60,1,1,15,0,''
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ItemsForECommerce' ) 
    DROP PROCEDURE USP_ItemsForECommerce
GO
CREATE PROCEDURE [dbo].[USP_ItemsForECommerce]
    @numSiteID AS NUMERIC(9) = 0,
    @numCategoryID AS NUMERIC(18) = 0,
    @numWareHouseID AS NUMERIC(9) = 0,
    @SortBy NVARCHAR(MAX) = 0,
    @CurrentPage INT,
    @PageSize INT,
    @TotRecs INT = 0 OUTPUT,
    @SearchText AS NVARCHAR(MAX) = '',--Comma seperated item ids
    @FilterQuery AS NVARCHAR(MAX) = '',----Comma seperated Attributes Ids
    @FilterRegularCondition AS NVARCHAR(MAX),
    @FilterCustomFields AS NVARCHAR(MAX),
    @FilterCustomWhere AS NVARCHAR(MAX),
	@numDivisionID AS NUMERIC(18,0) = 0,
	@numManufacturerID AS NUMERIC(18,0)=0,
	@FilterItemAttributes AS NVARCHAR(MAX),
	@numESItemCodes AS NVARCHAR(MAX)=''
AS 
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	DECLARE @numDomainID AS NUMERIC(9)
	DECLARE @numDefaultRelationship AS NUMERIC(18,0)
	DECLARE @numDefaultProfile AS NUMERIC(18,0)
	DECLARE @tintPreLoginPriceLevel AS TINYINT
	DECLARE @tintPriceLevel AS TINYINT
	DECLARE @tintDisplayCategory AS TINYINT
	DECLARE @bitAutoSelectWarehouse AS BIT

	SELECT  
		@numDomainID = numDomainID
    FROM 
		[Sites]
    WHERE 
		numSiteID = @numSiteID
        
	IF ISNULL(@numDivisionID,0) > 0
	BEGIN
		SELECT 
			@numDefaultRelationship=ISNULL(numCompanyType,0)
			,@numDefaultProfile=ISNULL(vcProfile,0)
			,@tintPriceLevel=ISNULL(tintPriceLevel,0) 
		FROM 
			DivisionMaster 
		INNER JOIN 
			CompanyInfo 
		ON 
			DivisionMaster.numCompanyID=CompanyInfo.numCompanyId 
		WHERE 
			numDivisionID=@numDivisionID 
			AND DivisionMaster.numDomainID=@numDomainID
	END

	--KEEP THIS BELOW ABOVE IF CONDITION
	SELECT 
		@tintDisplayCategory = ISNULL(bitDisplayCategory,0)
		,@numDefaultRelationship=(CASE WHEN ISNULL(@numDivisionID,0) > 0 THEN @numDefaultRelationship ELSE numRelationshipId END)
		,@numDefaultProfile=(CASE WHEN ISNULL(@numDivisionID,0) > 0 THEN @numDefaultProfile ELSE numProfileId END)
		,@tintPreLoginPriceLevel=(CASE WHEN ISNULL(bitShowPriceUsingPriceLevel,0)=1 THEN (CASE WHEN @numDivisionID > 0 THEN (CASE WHEN ISNULL(bitShowPriceUsingPriceLevel,0)=1 THEN ISNULL(@tintPriceLevel,0) ELSE 0 END) ELSE ISNULL(tintPreLoginProceLevel,0) END) ELSE 0 END)
		,@bitAutoSelectWarehouse = ISNULL(bitAutoSelectWarehouse,0)
	FROM 
		dbo.eCommerceDTL 
	WHERE 
		numSiteID = @numSiteId
		
	CREATE TABLE #tmpItemCat(numCategoryID NUMERIC(18,0))
	CREATE TABLE #tmpItemCode(numItemCode NUMERIC(18,0))
		
	IF LEN(@FilterCustomFields) > 0 AND LEN(@FilterCustomWhere) > 0
	BEGIN			
		DECLARE @FilterCustomFieldIds AS NVARCHAR(MAX)
		SET @FilterCustomFieldIds = Replace(@FilterCustomFields,'[','')
		SET @FilterCustomFieldIds = Replace(@FilterCustomFieldIds,']','')
			
		DECLARE @strCustomSql AS NVARCHAR(MAX)
		SET @strCustomSql = 'INSERT INTO #tmpItemCode(numItemCode)
								SELECT DISTINCT T.RecId FROM 
														(
															SELECT * FROM 
																(
																	SELECT  t2.RecId, t1.fld_id, t2.Fld_Value FROM CFW_Fld_Master t1 
																	JOIN CFW_FLD_Values_Item AS t2 ON t1.Fld_ID = t2.Fld_ID 
																	WHERE t1.Grp_id  In (5,9)
																	AND t1.numDomainID = ' + CONVERT(VARCHAR(10),@numDomainID) + '
																	AND t1.fld_type <> ''Link'' 
																	AND t1.fld_id IN (' + @FilterCustomFieldIds + ')
																) p  PIVOT (MAX([Fld_Value]) FOR fld_id IN (' + @FilterCustomFields + ')) AS pvt 
														) T  WHERE 1=1 AND ' + @FilterCustomWhere
								 
		EXEC SP_EXECUTESQL @strCustomSql								 					 
	END
		
    DECLARE @strSQL NVARCHAR(MAX)
    DECLARE @firstRec AS INTEGER
    DECLARE @lastRec AS INTEGER
    DECLARE @Where NVARCHAR(MAX)
    DECLARE @SortString NVARCHAR(MAX)
    DECLARE @searchPositionColumn NVARCHAR(MAX)
	DECLARE @searchPositionColumnGroupBy NVARCHAR(MAX)
    DECLARE @row AS INTEGER
    DECLARE @data AS VARCHAR(100)
    DECLARE @dynamicFilterQuery NVARCHAR(MAX)
    DECLARE @checkFldType VARCHAR(100)
    DECLARE @count NUMERIC(9,0)
    DECLARE @whereNumItemCode NVARCHAR(MAX)
    DECLARE @filterByNumItemCode VARCHAR(100)
        
	SET @SortString = ''
    set @searchPositionColumn = ''
	SET @searchPositionColumnGroupBy = ''
    SET @strSQL = ''
	SET @whereNumItemCode = ''
	SET @dynamicFilterQuery = ''
		 
    SET @firstRec = ( @CurrentPage - 1 ) * @PageSize
    SET @lastRec = ( @CurrentPage * @PageSize + 1 )
                
	DECLARE @vcWarehouseIDs AS VARCHAR(1000)
    IF (@numWareHouseID = 0 OR @bitAutoSelectWarehouse = 1)
    BEGIN
		IF @bitAutoSelectWarehouse = 1
		BEGIN
			SELECT @vcWarehouseIDs = ISNULL( STUFF((SELECT DISTINCT ',' + CAST(numWarehouseID AS VARCHAR(10)) FROM [dbo].[Warehouses] WHERE [Warehouses].[numDomainID] = @numDomainID FOR XML PATH('')),1, 1, '') , '') 
		END
		ELSE
		BEGIN
			SELECT 
				@numWareHouseID = ISNULL(numDefaultWareHouseID,0)
			FROM 
				[eCommerceDTL]
			WHERE 
				[numDomainID] = @numDomainID	

			SET @vcWarehouseIDs = @numWareHouseID
		END
    END
	ELSE
	BEGIN
		SET @vcWarehouseIDs = @numWareHouseID
	END

	--PRIAMRY SORTING USING CART DROPDOWN
	DECLARE @Join AS NVARCHAR(MAX)
	DECLARE @SortFields AS NVARCHAR(MAX)
	DECLARE @vcSort AS VARCHAR(MAX)

	SET @Join = ''
	SET @SortFields = ''
	SET @vcSort = ''

	IF @SortBy = ''
	BEGIN
		SET @SortString = ' vcItemName Asc '
	END
	ELSE IF CHARINDEX('~0',@SortBy) > 0 OR CHARINDEX('~1',@SortBy) > 0
	BEGIN
		DECLARE @fldID AS NUMERIC(18,0)
		DECLARE @RowNumber AS VARCHAR(MAX)
				
		SELECT * INTO #fldValues FROM dbo.SplitIDsWithPosition(@SortBy,'~')
		SELECT @RowNumber = RowNumber, @fldID = id FROM #fldValues WHERE RowNumber = 1
		SELECT @vcSort = CASE WHEN Id = 0 THEN 'ASC' ELSE 'DESC' END FROM #fldValues WHERE RowNumber = 2
				
		SET @Join = @Join + ' LEFT JOIN CFW_FLD_Values_Item CFVI' + @RowNumber + ' ON I.numItemCode = CFVI' + @RowNumber + '.RecID AND CFVI' + @RowNumber + '.fld_Id = ' + CONVERT(VARCHAR(10),@fldID)
		---SET @SortString = 'CFVI' + @RowNumber + '.fld_Value' 
		SET @SortString = '[fld_Value' + @RowNumber + '] ' --+ @vcSort
		SET @SortFields = ', CFVI' + @RowNumber + '.fld_Value [fld_Value' + @RowNumber + ']'
	END
	ELSE
	BEGIN	
		IF CHARINDEX('monListPrice',@SortBy) > 0
		BEGIN
			DECLARE @bitSortPriceMode AS BIT
			SELECT @bitSortPriceMode = ISNULL(bitSortPriceMode,0) FROM dbo.eCommerceDTL WHERE numSiteId = @numSiteID
			
			IF @bitSortPriceMode = 1
			BEGIN
				SET @SortString = ' '
			END
			ELSE
			BEGIN
				SET @SortString = @SortBy	
			END
		END
		ELSE
		BEGIN
			SET @SortString = @SortBy	
		END	
	END	

	--SECONDARY SORTING USING DEFAULT SETTINGS FROM ECOMMERCE
	SELECT 
		ROW_NUMBER() OVER (ORDER BY Custom,vcFieldName) AS [RowID]
		,* 
	INTO 
		#tempSort 
	FROM 
	(
		SELECT 
			CONVERT(VARCHAR(9),numFieldID)+'~0' AS numFieldID,
			ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName,
			0 AS Custom,
			vcDbColumnName 
		FROM 
			View_DynamicColumns
		WHERE 
			numFormID = 84 
			AND ISNULL(bitSettingField,0) = 1 
			AND ISNULL(bitDeleted,0)=0 
			AND ISNULL(bitCustom,0) = 0
			AND numDomainID = @numDomainID	       
		UNION     
		SELECT 
			CONVERT(VARCHAR(9),numFieldID)+'~1' AS numFieldID,
			vcFieldName ,
			1 AS Custom,
			'' AS vcDbColumnName
		FROM 
			View_DynamicCustomColumns          
		WHERE 
			numFormID = 84
			AND numDomainID = @numDomainID
			AND grp_id IN (5,9)
			AND vcAssociatedControlType = 'TextBox' 
			AND ISNULL(bitCustom,0) = 1 
			AND tintPageType=1  
	) TABLE1 
	ORDER BY 
		Custom,vcFieldName
		
	--SELECT * FROM #tempSort
	DECLARE @DefaultSort AS NVARCHAR(MAX)
	DECLARE @DefaultSortFields AS NVARCHAR(MAX)
	DECLARE @DefaultSortJoin AS NVARCHAR(MAX)
	DECLARE @intCnt AS INT
	DECLARE @intTotalCnt AS INT	
	DECLARE @strColumnName AS VARCHAR(100)
	DECLARE @bitCustom AS BIT
	DECLARE @FieldID AS VARCHAR(100)				

	SET @DefaultSort = ''
	SET @DefaultSortFields = ''
	SET @DefaultSortJoin = ''		
	SET @intCnt = 0
	SET @intTotalCnt = ( SELECT COUNT(*) FROM #tempSort )
	SET @strColumnName = ''
		
	IF (SELECT ISNULL(bitEnableSecSorting,0) FROM EcommerceDtl WHERE numSiteID = @numSiteID ) = 1
	BEGIN
		CREATE TABLE #fldDefaultValues(RowNumber INT,Id VARCHAR(1000))

		WHILE(@intCnt < @intTotalCnt)
		BEGIN
			SET @intCnt = @intCnt + 1
			SELECT @FieldID = numFieldID, @strColumnName = vcDbColumnName, @bitCustom = Custom FROM #tempSort WHERE RowID = @intCnt

			IF @bitCustom = 0
			BEGIN
				SET @DefaultSort =  @strColumnName + ',' + @DefaultSort 
			END
			ELSE
			BEGIN
				DECLARE @fldID1 AS NUMERIC(18,0)
				DECLARE @RowNumber1 AS VARCHAR(MAX)
				DECLARE @vcSort1 AS VARCHAR(10)
				DECLARE @vcSortBy AS VARCHAR(1000)
				SET @vcSortBy = CONVERT(VARCHAR(100),@FieldID) + '~' + '0'

				INSERT INTO #fldDefaultValues(RowNumber,Id) SELECT RowNumber, id FROM dbo.SplitIDsWithPosition(@vcSortBy,'~')
				SELECT @RowNumber1 = RowNumber + 100 + @intCnt, @fldID1 = id FROM #fldDefaultValues WHERE RowNumber = 1
				SELECT @vcSort1 = 'ASC' 
						
				SET @DefaultSortJoin = @DefaultSortJoin + ' LEFT JOIN CFW_FLD_Values_Item CFVI' + @RowNumber1 + ' ON I.numItemCode = CFVI' + @RowNumber1 + '.RecID AND CFVI' + @RowNumber1 + '.fld_Id = ' + CONVERT(VARCHAR(10),@fldID1)
				SET @DefaultSort = '[fld_Value' + @RowNumber1 + '] ' + ',' + @DefaultSort 
				SET @DefaultSortFields = @DefaultSortFields + ', CFVI' + @RowNumber1 + '.fld_Value [fld_Value' + @RowNumber1 + ']'
			END
		END

		IF LEN(@DefaultSort) > 0
		BEGIN
			SET  @DefaultSort = LEFT(@DefaultSort,LEN(@DefaultSort) - 1)
			SET @SortString = @SortString + ',' 
		END
	END
                                		
    IF LEN(@SearchText) > 0
	BEGIN
		IF CHARINDEX('SearchOrder',@SortString) > 0
		BEGIN
			SET @searchPositionColumn = ' ,SearchItems.RowNumber AS SearchOrder ' 
			SET @searchPositionColumnGroupBy = ' ,SearchItems.RowNumber ' 
		END

		SET @Where = ' INNER JOIN dbo.SplitIDsWithPosition('''+ @SearchText +''','','') AS SearchItems 
                        ON SearchItems.Id = I.numItemCode  and SC.numSiteID = ' + CONVERT(VARCHAR(15), @numSiteID) + ' WHERE ISNULL(IsArchieve,0) = 0'
	END
	ELSE IF @SearchText = ''
	BEGIN
		SET @Where = ' WHERE 1=2 AND I.numDomainID=' + CONVERT(VARCHAR(15), @numDomainID)
		+ ' and SC.numSiteID = ' + CONVERT(VARCHAR(15), @numSiteID)
		+ ' AND ISNULL(IsArchieve,0) = 0'
	END
	ELSE
	BEGIN
		SET @Where = CONCAT(' WHERE 1=1 AND I.numDomainID=',@numDomainID,' and SC.numSiteID = ',@numSiteID,' AND ISNULL(IsArchieve,0) = 0')

		IF @tintDisplayCategory = 1
		BEGIN
					
			;WITH CTE AS(
			SELECT C.numCategoryID,C.tintLevel,C.numDepCategory,CASE WHEN tintLevel=1 THEN 1 ELSE 0 END AS bitChild 
				FROM SiteCategories SC INNER JOIN Category C ON SC.numCategoryID=C.numCategoryID WHERE SC.numSiteID=@numSiteID AND (C.numCategoryID = @numCategoryID OR ISNULL(@numCategoryID,0) = 0) AND numDomainID = @numDomainID
			UNION ALL
			SELECT  C.numCategoryID,C.tintLevel,C.numDepCategory,CTE.bitChild FROM Category C JOIN CTE ON 
			C.numDepCategory=CTE.numCategoryID AND CTE.bitChild=1 AND numDomainID = @numDomainID)
					
			INSERT INTO #tmpItemCat(numCategoryID)												 
			SELECT DISTINCT C.numCategoryID FROM CTE C JOIN dbo.ItemCategory IC ON C.numCategoryID = IC.numCategoryID 
												JOIN View_Item_Warehouse ON View_Item_Warehouse.numDOmainID=@numDomainID AND  View_Item_Warehouse.numItemID=IC.numItemID
												WHERE ISNULL(View_Item_Warehouse.numAvailable,0) > 0
		END

	END

	SET @strSQL = @strSQL + CONCAT(' WITH Items AS 
									( 
											SELECT
											I.numDomainID
											,I.numItemCode
											,ROW_NUMBER() OVER ( Partition by I.numItemCode Order by I.numItemCode) RowID
											,I.bitMatrix
											,I.bitKitParent
											,I.numItemGroup
											,vcItemName
											,I.bintCreatedDate
											,vcManufacturer
											,ISNULL(txtItemDesc,'''') txtItemDesc
											,vcSKU
											,fltHeight
											,fltWidth
											,fltLength
											,vcModelID
											,fltWeight
											,numBaseUnit
											,numSaleUnit
											,numPurchaseUnit
											,bitFreeShipping
											,C.vcCategoryName
											,C.vcDescription as CategoryDesc
											,charItemType
											,monListPrice
											,bitAllowBackOrder
											,bitShowInStock
											,numVendorID
											,numItemClassification',@searchPositionColumn,@SortFields,@DefaultSortFields,'
											,SUM(numOnHand) numOnHand
											,SUM(numAllocation) numAllocation
											' + (CASE WHEN LEN(ISNULL(@numESItemCodes,'')) > 0 THEN ',ISNULL(C.numCategoryID,0) numCategoryID' ELSE '' END)  + '
											,ISNULL(I.numManufacturer,0) numManufacturer
											FROM      
											Item AS I
											INNER JOIN eCommerceDTL E ON E.numDomainID = I.numDomainID AND E.numSiteId=',@numSiteID,'
											LEFT JOIN WareHouseItems WI ON WI.numDomainID=' + CONVERT(VARCHAR(10),@numDomainID) + ' AND WI.numItemID=I.numItemCode AND numWareHouseID IN (SELECT [OutParam] FROM dbo.[SplitString](''' + @vcWarehouseIDs + ''','',''))
											INNER JOIN ItemCategory IC ON I.numItemCode = IC.numItemID
											INNER JOIN Category C ON IC.numCategoryID = C.numCategoryID
											INNER JOIN SiteCategories SC ON IC.numCategoryID = SC.numCategoryID 
											',@Join,' ',@DefaultSortJoin)

	IF LEN(ISNULL(@FilterItemAttributes,'')) > 0
	BEGIN
		SET @strSQL = @strSQL + @FilterItemAttributes
	END

	IF LEN(@FilterCustomFields) > 0 AND LEN(@FilterCustomWhere) > 0
	BEGIN
		SET @strSQL = @strSQL + ' INNER JOIN #tmpItemCode TP ON TP.numItemCode = I.numItemCode ' 
	END
			
	IF (SELECT COUNT(*) FROM #tmpItemCat) > 0
	BEGIN
		SET @strSQL = @strSQL + ' INNER JOIN #tmpItemCat TC ON TC.numCategoryID = IC.numCategoryID '
	END
	ELSE IF @numManufacturerID>0
	BEGIN
		SET @Where = REPLACE(@Where,'WHERE',' WHERE I.numManufacturer = ' + CONVERT(VARCHAR(10),@numManufacturerID) + ' AND ')
	END 
	ELSE IF @numCategoryID>0
	BEGIN
		SET @Where = REPLACE(@Where,'WHERE',' WHERE IC.numCategoryID = ' + CONVERT(VARCHAR(10),@numCategoryID) + ' AND ')
	END 
			
	IF (SELECT COUNT(*) FROM [dbo].[EcommerceRelationshipProfile] WHERE numSiteID=@numSiteID AND numRelationship = @numDefaultRelationship AND numProfile = @numDefaultProfile) > 0
	BEGIN
		SET @Where = @Where + CONCAT(' AND I.numItemClassification NOT IN (SELECT numItemClassification FROM EcommerceRelationshipProfile ERP INNER JOIN ECommerceItemClassification EIC ON EIC.numECommRelatiionshipProfileID=ERP.ID WHERE ERP.numSiteID=',@numSiteID,' AND ERP.numRelationship=',@numDefaultRelationship,' AND ERP.numProfile=', @numDefaultProfile,')')
	END

	SET @strSQL = @strSQL + @Where
	
	IF LEN(@FilterRegularCondition) > 0
	BEGIN
		SET @strSQL = @strSQL + @FilterRegularCondition
	END                                         

    IF LEN(@whereNumItemCode) > 0
    BEGIN
			SET @strSQL = @strSQL + @filterByNumItemCode	
	END

    SET @strSQL = CONCAT(@strSQL,' GROUP BY
										I.numDomainID
										,I.numItemCode
										,I.bitMatrix
										,I.bitKitParent
										,I.numItemGroup
										,vcItemName
										,I.bintCreatedDate
										,vcManufacturer
										,txtItemDesc
										,vcSKU
										,fltHeight
										,fltWidth
										,fltLength
										,vcModelID
										,fltWeight
										,numBaseUnit
										,numSaleUnit
										,numPurchaseUnit
										,bitFreeShipping
										,C.vcCategoryName
										,C.vcDescription
										,charItemType
										,monListPrice
										,bitAllowBackOrder
										,bitShowInStock
										,numVendorID
										,numItemClassification
										' + (CASE WHEN LEN(ISNULL(@numESItemCodes,'')) > 0 THEN ',C.numCategoryID' ELSE '' END)  + '
										,I.numManufacturer',@searchPositionColumnGroupBy,@SortFields,@DefaultSortFields,(CASE WHEN ISNULL(@tintDisplayCategory,0) = 1 THEN ' HAVING 1 = (CASE WHEN (I.charItemType=''P'' AND ISNULL(I.bitKitParent,0) = 0) THEN CASE WHEN (ISNULL(SUM(numOnHand), 0) > 0 OR ISNULL(SUM(numAllocation), 0) > 0) THEN 1 ELSE 0 END ELSE 1 END) ' ELSE '' END), ') SELECT * INTO #TEMPItems FROM Items;')
                                        
	IF @numDomainID IN (204,214,215)
	BEGIN
		SET @strSQL = @strSQL + 'DELETE FROM 
									#TEMPItems
								WHERE 
									numItemCode IN (
														SELECT 
															F.numItemCode
														FROM 
															#TEMPItems AS F
														WHERE 
															ISNULL(F.bitMatrix,0) = 1 
															AND EXISTS (
																		SELECT 
																			t1.vcItemName, t1.bitMatrix, t1.numItemGroup, Count(t1.numItemCode)
																		FROM 
																			#TEMPItems t1
																		WHERE 
																			t1.vcItemName = F.vcItemName
																			AND t1.bitMatrix = 1
																			AND t1.numItemGroup = F.numItemGroup
																		GROUP BY 
																			t1.vcItemName, t1.bitMatrix, t1.numItemGroup
																		HAVING 
																			Count(t1.numItemCode) > 1
																	)
													)
									AND numItemCode NOT IN (
																			SELECT 
																				Min(numItemCode)
																			FROM 
																				#TEMPItems AS F
																			WHERE
																				ISNULL(F.bitMatrix,0) = 1 
																				AND Exists (
																							SELECT 
																								t2.vcItemName, t2.bitMatrix, t2.numItemGroup, Count(t2.numItemCode)
																							FROM 
																								#TEMPItems t2
																							WHERE 
																								t2.vcItemName = F.vcItemName
																								AND t2.bitMatrix = 1
																								AND t2.numItemGroup = F.numItemGroup
																							GROUP BY 
																								t2.vcItemName, t2.bitMatrix, t2.numItemGroup
																							HAVING 
																								Count(t2.numItemCode) > 1
																						)
																			GROUP BY 
																				vcItemName, bitMatrix, numItemGroup
																		);'

	END
									  
									  
	SET @strSQL = @strSQL + ' WITH ItemSorted AS (SELECT  ROW_NUMBER() OVER ( ORDER BY  ' + @SortString + @DefaultSort + ' ' + @vcSort + ' ) AS Rownumber,* FROM  #TEMPItems WHERE RowID = 1)'

    DECLARE @fldList AS NVARCHAR(MAX)
	DECLARE @fldList1 AS NVARCHAR(MAX)
	SELECT @fldList=COALESCE(@fldList + ',','') + QUOTENAME(REPLACE(Fld_label,' ','') + '_C') FROM dbo.CFW_Fld_Master WHERE numDomainID=@numDomainID AND Grp_id=5
	SELECT @fldList1=COALESCE(@fldList1 + ',','') + QUOTENAME(REPLACE(Fld_label,' ','') + '_' + CAST(Fld_Id AS VARCHAR)) FROM dbo.CFW_Fld_Master WHERE numDomainID=@numDomainID AND Grp_id=9
            
	/***Changed By: Chirag R:****************/
	/***Note: Changes for getting records Elastic Search Affected Items ********/
    --SET @strSQL = @strSQL + 'SELECT * INTO #tmpPagedItems FROM ItemSorted WHERE Rownumber > ' + CONVERT(VARCHAR(10), @firstRec) + '
    --                            AND   Rownumber < ' + CONVERT(VARCHAR(10), @lastRec) + '
    --                            order by Rownumber '

	DECLARE @bitGetItemForElasticSearch BIT;
	SET @bitGetItemForElasticSearch = IIF(LEN(@numESItemCodes) > 0,1,0)
    SET @strSQL = @strSQL + 'SELECT * INTO #tmpPagedItems FROM ItemSorted ';
	
	IF(@bitGetItemForElasticSearch = 1)
	BEGIN
		SET @strSQL = @strSQL + ' WHERE numItemCode IN (' + @numESItemCodes + ')'
	END 
	ELSE
	BEGIN
		SET @strSQL = @strSQL + ' WHERE Rownumber > ' + CONVERT(VARCHAR(10), @firstRec) + '
							AND   Rownumber < ' + CONVERT(VARCHAR(10), @lastRec)
	END
    SET @strSQL = @strSQL + ' order by Rownumber '

    /***Changes Completed: Chirag R:****************/                

	SET @strSQL = CONCAT(@strSQL,'SELECT  
								Rownumber
								,I.numItemCode
								,vcCategoryName
								,CategoryDesc
								,vcItemName
								,txtItemDesc
								,vcManufacturer
								,vcSKU
								,fltHeight
								,fltWidth
								,fltLength
								,vcModelID
								,fltWeight
								,bitFreeShipping
								,', 
									(CASE 
									WHEN @tintPreLoginPriceLevel > 0 
									THEN 'ISNULL((CASE 
											WHEN PricingOption.tintRuleType = 1 AND PricingOption.tintDiscountType = 1
											THEN ISNULL((CASE WHEN I.[charItemType] = ''P'' THEN ( UOM * ISNULL(W1.[monWListPrice],0)) ELSE (UOM * monListPrice) END),0) - (ISNULL((CASE WHEN I.[charItemType] = ''P'' THEN ( UOM * ISNULL(W1.[monWListPrice],0)) ELSE (UOM * monListPrice) END),0) * (PricingOption.decDiscount / 100 ) )
											WHEN PricingOption.tintRuleType = 1 AND PricingOption.tintDiscountType = 2
											THEN ISNULL((CASE WHEN I.[charItemType] = ''P'' THEN ( UOM * ISNULL(W1.[monWListPrice],0)) ELSE (UOM * monListPrice) END),0) - PricingOption.decDiscount
											WHEN PricingOption.tintRuleType = 2 AND PricingOption.tintDiscountType = 1
											THEN ISNULL(Vendor.monCost,0) + (ISNULL(Vendor.monCost,0) * (PricingOption.decDiscount / 100 ) )
											WHEN PricingOption.tintRuleType = 2 AND PricingOption.tintDiscountType = 2
											THEN ISNULL(Vendor.monCost,0) + PricingOption.decDiscount
											WHEN PricingOption.tintRuleType = 3
											THEN PricingOption.decDiscount
										END),ISNULL(CASE WHEN I.[charItemType] = ''P'' THEN ( UOM * ISNULL(W1.[monWListPrice],0)) ELSE (UOM * monListPrice) END, 0))' 
									ELSE 'ISNULL(CASE WHEN I.[charItemType] = ''P'' THEN ( UOM * ISNULL(W1.[monWListPrice],0)) ELSE (UOM * monListPrice) END, 0)' 
									END)
									,' AS monListPrice,
									ISNULL(CASE WHEN I.[charItemType] = ''P'' THEN ( UOM * ISNULL(W1.[monWListPrice],0)) ELSE (UOM * monListPrice) END, 0) monMSRP					   
									',(CASE WHEN @numDomainID = 172 THEN ',ISNULL(CASE 
											WHEN tintRuleType = 1 AND tintDiscountType = 1
											THEN ISNULL((CASE WHEN I.[charItemType] = ''P'' THEN ( UOM * ISNULL(W1.[monWListPrice],0)) ELSE (UOM * monListPrice) END),0) - (ISNULL((CASE WHEN I.[charItemType] = ''P'' THEN ( UOM * ISNULL(W1.[monWListPrice],0)) ELSE (UOM * monListPrice) END),0) * ( decDiscount / 100 ) )
											WHEN tintRuleType = 1 AND tintDiscountType = 2
											THEN ISNULL((CASE WHEN I.[charItemType] = ''P'' THEN ( UOM * ISNULL(W1.[monWListPrice],0)) ELSE (UOM * monListPrice) END),0) - decDiscount
											WHEN tintRuleType = 2 AND tintDiscountType = 1
											THEN ISNULL(Vendor.monCost,0) + (ISNULL(Vendor.monCost,0) * ( decDiscount / 100 ) )
											WHEN tintRuleType = 2 AND tintDiscountType = 2
											THEN ISNULL(Vendor.monCost,0) + decDiscount
											WHEN tintRuleType = 3
											THEN decDiscount
										END, 0) AS monFirstPriceLevelPrice,ISNULL(CASE 
										WHEN tintRuleType = 1 AND tintDiscountType = 1 
										THEN decDiscount 
										WHEN tintRuleType = 1 AND tintDiscountType = 2
										THEN (decDiscount * 100) / (ISNULL((CASE WHEN I.[charItemType] = ''P'' THEN ( UOM * ISNULL(W1.[monWListPrice],0)) ELSE (UOM * monListPrice) END),0) - decDiscount)
										WHEN tintRuleType = 2 AND tintDiscountType = 1
										THEN decDiscount
										WHEN tintRuleType = 2 AND tintDiscountType = 2
										THEN (decDiscount * 100) / (ISNULL(Vendor.monCost,0) + decDiscount)
										WHEN tintRuleType = 3
										THEN 0
									END,'''') AS fltFirstPriceLevelDiscount' ELSE ',0 AS monFirstPriceLevelPrice, 0 AS fltFirstPriceLevelDiscount' END), '  
								,vcPathForImage
								,vcpathForTImage
								,UOM AS UOMConversionFactor
								,UOMPurchase AS UOMPurchaseConversionFactor
								,(CASE WHEN charItemType <> ''S'' AND charItemType <> ''N'' AND ISNULL(bitKitParent,0)=0
											THEN (CASE 
													WHEN bitAllowBackOrder = 1 THEN 1
													WHEN (ISNULL(I.numOnHand,0)<=0) THEN 0
													ELSE 1
												END)
											ELSE 1
								END) AS bitInStock,
								(
									CASE WHEN ISNULL(bitKitParent,0) = 1
									THEN
										(CASE 
											WHEN ((SELECT COUNT(*) FROM ItemDetails ID INNER JOIN Item IInner ON ID.numChildItemID = IInner.numItemCode WHERE ID.numItemKitID = I.numItemCode AND ISNULL(IInner.bitKitParent,0) = 1)) > 0 
											THEN 1 
											ELSE 0 
										END)
									ELSE
										0
									END
								) bitHasChildKits
								,(CASE WHEN charItemType <> ''S'' AND charItemType <> ''N''
											THEN ( CASE WHEN bitShowInStock = 1
														THEN (CASE 
																WHEN bitAllowBackOrder = 1 AND I.numDomainID <> 172 AND ISNULL(I.numOnHand,0)<=0 AND ISNULL(I.numVendorID,0) > 0 AND dbo.GetVendorPreferredMethodLeadTime(I.numVendorID,0) > 0 THEN CONCAT(''<font class="vendorLeadTime" style="color:green">Usually ships in '',dbo.GetVendorPreferredMethodLeadTime(I.numVendorID,0),'' days</font>'')
																WHEN bitAllowBackOrder = 1 AND ISNULL(I.numOnHand,0)<=0 THEN (CASE WHEN I.numDomainID = 172 THEN ''<font style="color:green">AVAILABLE</font>'' ELSE ''<font style="color:green">In Stock</font>'' END)
																WHEN (ISNULL(I.numOnHand,0)<=0) THEN (CASE WHEN I.numDomainID = 172 THEN ''<font style="color:red">ON HOLD</font>'' ELSE ''<font style="color:red">Out Of Stock</font>'' END)
																ELSE (CASE WHEN I.numDomainID = 172 THEN ''<font style="color:green">AVAILABLE</font>'' ELSE ''<font style="color:green">In Stock</font>'' END)  
															END)
														ELSE ''''
													END )
											ELSE ''''
										END ) AS InStock
								, ISNULL(W1.numWareHouseItemID,0) [numWareHouseItemID]
								,ISNULL(TablePromotion.numTotalPromotions,0) numTotalPromotions
								,ISNULL(TablePromotion.vcPromoDesc,'''') vcPromoDesc
								,ISNULL(TablePromotion.bitRequireCouponCode,0) bitRequireCouponCode
								' + (CASE WHEN LEN(ISNULL(@numESItemCodes,'')) > 0 THEN ',I.numCategoryID' ELSE '' END)  + '
								,I.numManufacturer
								,I.bintCreatedDate
								',(CASE WHEN LEN(ISNULL(@fldList,'')) > 0 THEN CONCAT(',',@fldList) ELSE '' END),(CASE WHEN LEN(ISNULL(@fldList1,'')) > 0 THEN CONCAT(',',@fldList1) ELSE '' END),'
                            FROM 
								#tmpPagedItems I
							LEFT JOIN ItemImages IM ON I.numItemCode = IM.numItemCode AND IM.bitdefault=1 AND IM.bitIsImage = 1
							LEFT JOIN Vendor ON Vendor.numVendorID =I.numVendorID AND Vendor.numItemCode=I.numItemCode
							OUTER APPLY (SELECT * FROM dbo.fn_GetItemPromotions(',@numDomainID,',',@numDivisionID,',I.numItemCode,I.numItemClassification',',',@numDefaultRelationship,',',@numDefaultProfile,',1)) TablePromotion ',
							(CASE 
								WHEN @tintPreLoginPriceLevel > 0 
								THEN CONCAT(' OUTER APPLY(SELECT * FROM PricingTable WHERE numItemCode=I.numItemCode AND ISNULL(numPriceRuleID,0) = 0 AND ISNULL(numCurrencyID,0) = 0 ORDER BY numPricingID OFFSET ',CAST(@tintPreLoginPriceLevel-1 AS VARCHAR),' ROWS FETCH NEXT 1 ROWS ONLY) As PricingOption ' )
								ELSE '' 
							END),(CASE 
								WHEN @numDomainID = 172
								THEN ' OUTER APPLY (SELECT TOP 1 * FROM PricingTable WHERE numItemCode=I.numItemCode AND ISNULL(numCurrencyID,0) = 0) AS PT'
								ELSE '' 
							END),' 
							OUTER APPLY (SELECT TOP 1 numWareHouseItemID,monWListPrice FROM WareHouseItems W 
											WHERE  W.numItemID = I.numItemCode 
											AND W.numDomainID = ',CONVERT(VARCHAR(10),@numDomainID),'
											AND numWareHouseID IN (SELECT [OutParam] FROM dbo.[SplitString](''' + @vcWarehouseIDs + ''','',''))) AS W1 			  
								CROSS APPLY (SELECT dbo.fn_UOMConversion(ISNULL(I.numSaleUnit, 0),I.numItemCode, I.numDomainId,ISNULL(I.numBaseUnit, 0)) AS UOM) AS UOM
								CROSS APPLY (SELECT dbo.fn_UOMConversion(ISNULL(I.numPurchaseUnit, 0),I.numItemCode, I.numDomainId,ISNULL(I.numBaseUnit, 0)) AS UOMPurchase) AS UOMPurchase')
			
	IF LEN(ISNULL(@fldList,'')) > 0
	BEGIN
		SET @strSQL = @strSQL +' left join (
											SELECT  t2.RecId, REPLACE(t1.Fld_label,'' '','''') + ''_C'' as Fld_label, 
												CASE t1.fld_type WHEN ''SelectBox'' THEN isnull(dbo.fn_GetListItemName(t2.Fld_Value),'''')
																	WHEN ''CheckBox'' THEN isnull(dbo.fn_getCustomDataValue(t2.Fld_Value,9),'''')
																	WHEN ''DateField'' THEN dbo.FormatedDateFromDate(t2.Fld_Value,' + CAST(@numDomainId AS VARCHAR(18)) +')
																	ELSE Replace(ISNULL(t2.Fld_Value,''0''),''$'',''$$'') end AS Fld_Value FROM CFW_Fld_Master t1 
											JOIN CFW_FLD_Values_Item AS t2 ON t1.Fld_ID = t2.Fld_ID where t1.Grp_id = 5 and t1.numDomainID=' + CAST(@numDomainId AS VARCHAR(18)) +' AND t1.fld_type <> ''Link''
											AND t2.RecId IN (SELECT numItemCode FROM #tmpPagedItems )
											) p PIVOT (MAX([Fld_Value]) FOR Fld_label IN (' + @fldList + ')) AS pvt on I.numItemCode=pvt.RecId' 
	END

	IF LEN(ISNULL(@fldList1,'')) > 0
	BEGIN
					
		SET @strSQL = @strSQL +' left join (
											SELECT  t2.numItemCode AS RecId, REPLACE(t1.Fld_label,'' '','''') + ''_'' + CAST(t1.Fld_Id AS VARCHAR) as Fld_label, 
												CASE t1.fld_type WHEN ''SelectBox'' THEN isnull(dbo.fn_GetListItemName(t2.Fld_Value),'''')
																	WHEN ''CheckBox'' THEN isnull(dbo.fn_getCustomDataValue(t2.Fld_Value,9),'''')
																	WHEN ''DateField'' THEN dbo.FormatedDateFromDate(t2.Fld_Value,' + CAST(@numDomainId AS VARCHAR(18)) +')
																	ELSE Replace(ISNULL(t2.Fld_Value,''0''),''$'',''$$'') end AS Fld_Value FROM CFW_Fld_Master t1 
											JOIN ItemAttributes AS t2 ON t1.Fld_ID = t2.Fld_ID where t1.Grp_id = 9 and t1.numDomainID=' + CAST(@numDomainId AS VARCHAR(18)) +' AND t1.fld_type <> ''Link''
											AND t2.numItemCode IN (SELECT numItemCode FROM #tmpPagedItems )
											) p PIVOT (MAX([Fld_Value]) FOR Fld_label IN (' + @fldList1 + ')) AS pvt1 on I.numItemCode=pvt1.RecId'
	END
	ELSE
	BEGIN
		SET @strSQL = REPLACE(@strSQL, ',##FLDLIST1##', '');
	END

	SET  @strSQL = @strSQL +' order by Rownumber';                
    SET @strSQL = @strSQL + ' SELECT @TotRecs = COUNT(*) FROM #TEMPItems WHERE RowID=1 '                            


	DECLARE @tmpSQL AS NVARCHAR(MAX)
	SET @tmpSQL = @strSQL
	
	PRINT CAST(@tmpSQL AS NTEXT)
	EXEC sp_executeSQL @strSQL, N'@TotRecs INT OUTPUT', @TotRecs OUTPUT
             
             
    DECLARE @tintOrder AS TINYINT                                                  
	DECLARE @vcFormFieldName AS VARCHAR(50)                                                  
	DECLARE @vcListItemType AS VARCHAR(1)                                             
	DECLARE @vcAssociatedControlType VARCHAR(10)                                                  
	DECLARE @numListID AS NUMERIC(9)                                                  
	DECLARE @WhereCondition VARCHAR(MAX)                       
	DECLARE @numFormFieldId AS NUMERIC  
	--DECLARE @vcFieldType CHAR(1)
	DECLARE @vcFieldType VARCHAR(15)
		                  
	SET @tintOrder=0                                                  
	SET @WhereCondition =''                 
                   
              
	CREATE TABLE #tempAvailableFields
	(
		numFormFieldId  NUMERIC(9)
		,vcFormFieldName NVARCHAR(50)
		,vcFieldType VARCHAR(15)
		,vcAssociatedControlType NVARCHAR(50)
		,numListID NUMERIC(18)
		,vcListItemType CHAR(1)
		,intRowNum INT
		,vcItemValue VARCHAR(3000)
	)
		   
	INSERT INTO #tempAvailableFields
	(
		numFormFieldId
		,vcFormFieldName
		,vcFieldType
		,vcAssociatedControlType
		,numListID
		,vcListItemType
		,intRowNum
	)                         
	SELECT 
		Fld_id
		,REPLACE(Fld_label,' ','') AS vcFormFieldName
		,CASE GRP_ID 
			WHEN 4 then 'C' 
			WHEN 1 THEN 'D'
			WHEN 9 THEN CONVERT(VARCHAR(15), Fld_id)
			ELSE 'C' 
		END AS vcFieldType
		,fld_type as vcAssociatedControlType
		,ISNULL(C.numListID, 0) numListID
		,CASE 
			WHEN C.numListID > 0 THEN 'L'
			ELSE ''
		END vcListItemType
		,ROW_NUMBER() OVER (ORDER BY Fld_id ASC) AS intRowNum
	FROM  
		CFW_Fld_Master C 
	LEFT JOIN 
		CFW_Validation V 
	ON 
		V.numFieldID = C.Fld_id
	WHERE 
		C.numDomainID = @numDomainId
		AND GRP_ID IN (5,9)

		  
	SELECT * FROM #tempAvailableFields
	
	IF OBJECT_ID('tempdb..#tempAvailableFields') IS NOT NULL
	DROP TABLE #tempAvailableFields
		
	IF OBJECT_ID('tempdb..#tmpItemCode') IS NOT NULL
	DROP TABLE #tmpItemCode
		
	IF OBJECT_ID('tempdb..#tmpItemCat') IS NOT NULL
	DROP TABLE #tmpItemCat
		
	IF OBJECT_ID('tempdb..#fldValues') IS NOT NULL
	DROP TABLE #fldValues
		
	IF OBJECT_ID('tempdb..#tempSort') IS NOT NULL
	DROP TABLE #tempSort
		
	IF OBJECT_ID('tempdb..#fldDefaultValues') IS NOT NULL
	DROP TABLE #fldDefaultValues

	IF OBJECT_ID('tempdb..#tmpPagedItems') IS NOT NULL
	DROP TABLE #tmpPagedItems


	IF OBJECT_ID('tempdb..#TEMPItems') IS NOT NULL
	DROP TABLE #TEMPItems
END
GO
/****** Object:  StoredProcedure [dbo].[usp_ManageCompany]    Script Date: 07/26/2008 16:19:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--modified by chintan
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_managecompany')
DROP PROCEDURE usp_managecompany
GO
CREATE PROCEDURE [dbo].[usp_ManageCompany]                                
 @numCompanyId numeric=0 ,                
 @vcCompanyName varchar (100)='',                
 @numCompanyType numeric=0,                
 @numCompanyRating numeric=0,                      
 @numCompanyIndustry numeric=0,                
 @numCompanyCredit numeric=0,                      
 @txtComments text='',                
 @vcWebSite varchar (255)='',                
 @vcWebLabel1 varchar (100)='',                
 @vcWebLink1 varchar (255)='',                
 @vcWebLabel2 varchar (100)='',                
 @vcWebLink2 varchar (255)='',                
 @vcWebLabel3 varchar (100)='',                
 @vcWebLink3 varchar (255)='',                
 @vcWebLabel4 varchar (100)='',                
 @vcWebLink4 varchar (255)='',                
 @numAnnualRevID numeric=0,                
 @numNoOfEmployeesId numeric=0,                
 @vcHow numeric=0,                
 @vcProfile numeric=0,                
 @numUserCntID numeric=0,                                               
 @bitPublicFlag bit = 0,                         
 @numDomainID numeric=0,
 @bitSelectiveUpdate BIT=0
--                
AS                
BEGIN                      
 IF @numCompanyId=0                      
  BEGIN       
	IF @numDomainID=170 AND (CHARINDEX('www.',@vcCompanyName) > 0 OR CHARINDEX('http://',@vcCompanyName) > 0 OR CHARINDEX('https://',@vcCompanyName) > 0)
	BEGIN
		RAISERROR('INVALID',16,1)
		RETURN
	END
                 
    INSERT INTO CompanyInfo      
   (      
   vcCompanyName,      
   numCompanyType,      
   numCompanyRating,       
   numCompanyIndustry,      
   numCompanyCredit,      
   txtComments,      
   vcWebSite,      
   vcWebLabel1,      
   vcWebLink1,      
   vcWebLabel2,      
   vcWebLink2,      
   vcWebLabel3,      
   vcWebLink3,      
   vcWeblabel4,      
   vcWebLink4,      
   numAnnualRevID,      
   numNoOfEmployeesId,      
   vcHow,      
   vcProfile,      
   numCreatedBy,      
   bintCreatedDate,      
   numModifiedBy,      
   bintModifiedDate,      
   bitPublicFlag,      
   numDomainID)       
 values      
   (      
   @vcCompanyName,       
   @numCompanyType,       
   @numCompanyRating,                   
   @numCompanyIndustry,       
   @numCompanyCredit,                  
   @txtComments,       
   @vcWebSite,       
   @vcWebLabel1,       
   @vcWebLink1,       
   @vcWebLabel2,       
   @vcWebLink2,                 
   @vcWebLabel3,       
   @vcWebLink3,       
   @vcWebLabel4,       
   @vcWebLink4,        
   @numAnnualRevID,                 
   @numNoOfEmployeesId,       
   @vcHow,       
   @vcProfile,       
   @numUserCntID,       
   getutcdate(),       
   @numUserCntID,       
   getutcdate(),                
   @bitPublicFlag,       
   @numDomainID      
   )                
              set @numCompanyId =SCOPE_IDENTITY()            
           select @numCompanyId                
  END                
 ELSE if  @numCompanyId>0                   
  BEGIN                

	IF @bitSelectiveUpdate = 0 
	BEGIN
		UPDATE CompanyInfo SET                
		vcCompanyName = @vcCompanyName,                
		numCompanyType = @numCompanyType,                
		numCompanyRating = @numCompanyRating,                     
		numCompanyIndustry = @numCompanyIndustry,                
		numCompanyCredit = @numCompanyCredit,               
		txtComments = @txtComments,                
		vcWebSite = @vcWebSite,                
		vcWebLabel1 = @vcWebLabel1,                 
		vcWebLabel2 = @vcWebLabel2,                 
		vcWebLabel3 = @vcWebLabel3,                
		vcWebLabel4 = @vcWebLabel4,                
		vcWebLink1 = @vcWebLink1,                
		vcWebLink2 = @vcWebLink2,                
		vcWebLink3 = @vcWebLink3,                
		vcWebLink4 = @vcWebLink4,                         
		numAnnualRevID = @numAnnualRevID,                
		numNoOfEmployeesId = @numNoOfEmployeesId,                
		vcHow = @vcHow,                
		vcProfile = @vcProfile,                
		numModifiedBy = @numUserCntID,                
		bintModifiedDate = getutcdate(),                
		bitPublicFlag = @bitPublicFlag                
	   WHERE                 
		numCompanyId=@numCompanyId    and numDomainID=@numDomainID       		
		
		SELECT @numcompanyId
	END	
	ELSE IF @bitSelectiveUpdate = 1
	BEGIN
		DECLARE @sql VARCHAR(1000)
		SET @sql = 'update CompanyInfo Set '

		IF(LEN(@vcCompanyName)>0)
		BEGIN
			SET @sql =@sql + ' vcCompanyName=''' + @vcCompanyName + ''', '
		END
		IF(LEN(@vcWebSite)>0)
		BEGIN
			SET @sql =@sql + ' vcWebSite=''' + @vcWebSite + ''', '
		END
		
		IF(@numUserCntID>0)
		BEGIN
			SET @sql =@sql + ' numModifiedBy='+ CONVERT(VARCHAR(10),@numUserCntID) +', '
		END
	
		SET @sql=SUBSTRING(@sql,0,LEN(@sql)) + ' '
		SET @sql =@sql + 'where numCompanyId= '+CONVERT(VARCHAR(10),@numCompanyId)

		PRINT @sql
		EXEC(@sql) 
	END
    
  END              
            
         
END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_MassSalesFulfillment_GetRecords')
DROP PROCEDURE dbo.USP_MassSalesFulfillment_GetRecords
GO

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_MassSalesFulfillment_GetRecords]
(
	@numDomainID NUMERIC(18,0)
	,@numUserCntID NUMERIC(18,0)
	,@ClientTimeZoneOffset INT
	,@numViewID NUMERIC(18,0)
	,@numWarehouseID NUMERIC(18,0)
	,@bitGroupByOrder BIT
	,@bitRemoveBO BIT
	,@numShippingZone NUMERIC(18,0)
	,@bitIncludeSearch BIT
	,@vcOrderStauts VARCHAR(MAX)
	,@tintFilterBy TINYINT
	,@vcFilterValue VARCHAR(MAX)
	,@vcCustomSearchValue VARCHAR(MAX)
	,@numBatchID NUMERIC(18,0)
	,@tintPackingViewMode TINYINT
	,@tintPrintBizDocViewMode TINYINT
	,@tintPendingCloseFilter TINYINT
	,@numPageIndex INT
	,@vcSortColumn VARCHAR(100) OUTPUT
	,@vcSortOrder VARCHAR(4) OUTPUT
	,@numTotalRecords NUMERIC(18,0) OUTPUT
)
AS 
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	IF @numViewID = 4 OR @numViewID = 5 OR @numViewID = 6 OR (@numViewID = 2 AND @tintPackingViewMode=1) 
	BEGIN
		SET @bitGroupByOrder = 1
	END
	
	SET @vcCustomSearchValue = REPLACE(@vcCustomSearchValue,'OpportunityItems.vcItemReleaseDate','OpportunityItems.ItemReleaseDate')

	If @numViewID = 6 AND CHARINDEX('OpportunityMaster.bintCreatedDate',@vcCustomSearchValue) > 0
	BEGIN
		SET @vcCustomSearchValue = REPLACE(@vcCustomSearchValue,'OpportunityMaster.bintCreatedDate','DATEADD(MINUTE,@ClientTimeZoneOffset * -1,OpportunityBizDocs.dtCreatedDate)')
	END
	ELSE IF CHARINDEX('OpportunityMaster.bintCreatedDate',@vcCustomSearchValue) > 0
	BEGIN
		SET @vcCustomSearchValue = REPLACE(@vcCustomSearchValue,'OpportunityMaster.bintCreatedDate','DATEADD(MINUTE,@ClientTimeZoneOffset * -1,OpportunityMaster.bintCreatedDate)')
	END

	IF @bitGroupByOrder = 1
	BEGIN
		IF @vcSortColumn NOT IN ('CompanyInfo.vcCompanyName','OpportunityBizDocs.monAmountPaid','OpportunityBizDocs.vcBizDocID','OpportunityMaster.bintCreatedDate','OpportunityMaster.bitPaidInFull','OpportunityMaster.numAge','OpportunityMaster.numStatus','OpportunityMaster.vcPoppName','Item.dtItemReceivedDate','OpportunityMaster.numAssignedTo','OpportunityMaster.numRecOwner')
		BEGIN
			SET @vcSortColumn = 'OpportunityMaster.bintCreatedDate'
			SET @vcSortOrder = 'DESC'
		END
	END
	ELSE 
	BEGIN
		IF @vcSortColumn NOT IN ('CompanyInfo.vcCompanyName'
								,'Item.charItemType'
								,'Item.numBarCodeId'
								,'Item.numItemClassification'
								,'Item.numItemGroup'
								,'Item.vcSKU'
								,'OpportunityBizDocs.monAmountPaid'
								,'OpportunityMaster.dtExpectedDate'
								,'OpportunityItems.numQtyPacked'
								,'OpportunityItems.numQtyPicked'
								,'OpportunityItems.numQtyShipped'
								,'OpportunityItems.numRemainingQty'
								,'OpportunityItems.numUnitHour'
								,'OpportunityItems.vcInvoiced'
								,'OpportunityItems.vcItemName'
								,'OpportunityItems.vcItemReleaseDate'
								,'OpportunityMaster.bintCreatedDate'
								,'OpportunityMaster.bitPaidInFull'
								,'OpportunityMaster.numAge'
								,'OpportunityMaster.numStatus'
								,'OpportunityMaster.vcPoppName'
								,'WareHouseItems.vcLocation'
								,'Item.dtItemReceivedDate'
								,'OpportunityBizDocs.vcBizDocID'
								,'Warehouses.vcWareHouse')
		BEGIN
			SET @vcSortColumn = 'OpportunityMaster.bintCreatedDate'
			SET @vcSortOrder = 'DESC'
		END
	END

	DECLARE @tintPackingMode TINYINT
	DECLARE @tintInvoicingType TINYINT
	DECLARE @tintCommitAllocation TINYINT
	DECLARE @bitEnablePickListMapping BIT
	DECLARE @bitEnableFulfillmentBizDocMapping BIT
	DECLARE @numShippingServiceItemID NUMERIC(18,0)
	DECLARE @numDefaultSalesShippingDoc NUMERIC(18,0)
	SELECT 
		@tintCommitAllocation=ISNULL(tintCommitAllocation,1) 
		,@numShippingServiceItemID=ISNULL(numShippingServiceItemID,0)
		,@numDefaultSalesShippingDoc=ISNULL(numDefaultSalesShippingDoc,0)
	FROM 
		Domain 
	WHERE 
		numDomainId=@numDomainID 

	IF @numViewID = 2 AND @tintPackingViewMode=3 AND @numDefaultSalesShippingDoc = 0
	BEGIN
		RAISERROR('DEFAULT_SHIPPING_BIZDOC_NOT_CONFIGURED',16,1)
		RETURN
	END

	SELECT
		@tintPackingMode=tintPackingMode
		,@tintInvoicingType=tintInvoicingType
		,@bitEnablePickListMapping=bitEnablePickListMapping
		,@bitEnableFulfillmentBizDocMapping=bitEnableFulfillmentBizDocMapping
	FROM
		MassSalesFulfillmentConfiguration
	WHERE
		numDomainID = @numDomainID 

	DECLARE @TempFieldsLeft TABLE
	(
		numPKID INT IDENTITY(1,1)
		,ID VARCHAR(50)
		,numFieldID NUMERIC(18,0)
		,vcFieldName VARCHAR(100)
		,vcOrigDbColumnName VARCHAR(100)
		,vcListItemType VARCHAR(10)
		,numListID NUMERIC(18,0)
		,bitAllowSorting BIT
		,bitAllowFiltering BIT
		,vcAssociatedControlType VARCHAR(20)
		,vcLookBackTableName VARCHAR(100)
		,bitCustomField BIT
		,intRowNum INT
		,intColumnWidth FLOAT
		,bitIsRequired BIT
		,bitIsEmail BIT
		,bitIsAlphaNumeric BIT
		,bitIsNumeric BIT
		,bitIsLengthValidation BIT
		,intMaxLength INT
		,intMinLength INT
		,bitFieldMessage BIT
		,vcFieldMessage VARCHAR(MAX)
		,Grp_id INT
	)

	INSERT INTO @TempFieldsLeft
	(
		ID
		,numFieldID
		,vcFieldName
		,vcOrigDbColumnName
		,vcListItemType
		,numListID
		,bitAllowSorting
		,bitAllowFiltering
		,vcAssociatedControlType
		,vcLookBackTableName
		,bitCustomField
		,intRowNum
		,intColumnWidth
		,bitIsRequired
		,bitIsEmail
		,bitIsAlphaNumeric
		,bitIsNumeric
		,bitIsLengthValidation
		,intMaxLength
		,intMinLength
		,bitFieldMessage
		,vcFieldMessage
		,Grp_id
	)
	SELECT
		CONCAT(141,'~',DFM.numFieldID,'~',0)
		,DFM.numFieldID
		,(CASE WHEN @numViewID=6 AND DFM.numFieldID=248 THEN 'BizDoc' ELSE DFFM.vcFieldName END)
		,DFM.vcOrigDbColumnName
		,ISNULL(DFM.vcListItemType,'')
		,ISNULL(DFM.numListID,0)
		,DFFM.bitAllowSorting
		,DFFM.bitAllowFiltering
		,DFFM.vcAssociatedControlType
		,DFM.vcLookBackTableName
		,0
		,DFCD.intRowNum
		,ISNULL(DFCD.intColumnWidth,0)
		,0
		,0
		,0
		,(CASE WHEN vcFieldDataType IN ('N','M') THEN 1 ELSE 0 END)
		,0
		,0
		,0
		,0
		,''
		,0
	FROM
		DycFormConfigurationDetails DFCD
	INNER JOIN
		DycFormField_Mapping DFFM
	ON
		DFCD.numFormId = DFFM.numFormID
		AND DFCD.numFieldId = DFFM.numFieldID
	INNER JOIN
		DycFieldMaster DFM
	ON
		DFFM.numFieldID = DFM.numFieldId
	WHERE 
		DFCD.numDomainId=@numDomainID 
		AND DFCD.numUserCntID=@numUserCntID 
		AND DFCD.numFormId=141 
		AND DFCD.numViewId=@numViewID
		AND ISNULL(DFFM.bitDeleted,0) = 0
		AND ISNULL(DFFM.bitSettingField,0) = 1
		AND ISNULL(DFCD.bitCustom,0) = 0
	UNION
	SELECT
		CONCAT(141,'~',CFM.Fld_id,'~',0)
		,CFM.Fld_id
		,CFM.Fld_label
		,CONCAT('CUST',CFM.Fld_id)
		,''
		,ISNULL(CFM.numListID,0)
		,0
		,0
		,CFM.Fld_type
		,''
		,1
		,DFCD.intRowNum
		,ISNULL(DFCD.intColumnWidth,0)
		,ISNULL(bitIsRequired,0)
		,ISNULL(bitIsEmail,0)
		,ISNULL(bitIsAlphaNumeric,0)
		,ISNULL(bitIsNumeric,0)
		,ISNULL(bitIsLengthValidation,0)
		,ISNULL(intMaxLength,0)
		,ISNULL(intMinLength,0)
		,ISNULL(bitFieldMessage,0)
		,ISNULL(vcFieldMessage,'')
		,CFM.Grp_id
	FROM
		DycFormConfigurationDetails DFCD
	INNER JOIN
		CFW_Fld_Master CFM
	ON
		DFCD.numFieldId = CFM.Fld_id
	LEFT JOIN
		CFW_Validation CV
	ON
		CFM.Fld_id = CV.numFieldID
	WHERE
		DFCD.numDomainId=@numDomainID 
		AND DFCD.numUserCntID=@numUserCntID 
		AND DFCD.numFormId=141 
		AND DFCD.numViewId=@numViewID
		AND ISNULL(DFCD.bitCustom,0) = 1

	IF (SELECT COUNT(*) FROM @TempFieldsLeft) = 0
	BEGIN
		INSERT INTO @TempFieldsLeft
		(
			ID
			,numFieldID
			,vcFieldName
			,vcOrigDbColumnName
			,vcListItemType
			,numListID
			,bitAllowSorting
			,bitAllowFiltering
			,vcAssociatedControlType
			,vcLookBackTableName
			,bitCustomField
			,intRowNum
			,intColumnWidth
		)
		SELECT
			CONCAT(141,'~',DFM.numFieldID,'~',0)
			,DFM.numFieldID
			,DFFM.vcFieldName
			,DFM.vcOrigDbColumnName
			,ISNULL(DFM.vcListItemType,'')
			,ISNULL(DFM.numListID,0)
			,DFFM.bitAllowSorting
			,DFFM.bitAllowFiltering
			,DFFM.vcAssociatedControlType
			,DFM.vcLookBackTableName
			,0
			,DFFM.[order]
			,100
		FROM
			DycFormField_Mapping DFFM
		INNER JOIN
			DycFieldMaster DFM
		ON
			DFFM.numFieldID = DFM.numFieldId
		WHERE 
			DFFM.numFormId=141 
			AND ISNULL(DFFM.bitDeleted,0) = 0
			AND ISNULL(DFFM.bitSettingField,0) = 1
			AND ISNULL(DFFM.bitDefault,0) = 1
	END

	DECLARE @vcSelect NVARCHAR(MAX) = CONCAT('SELECT 
												OpportunityMaster.numOppID
												,OpportunityMaster.dtExpectedDate AS dtExpectedDateOrig
												,DivisionMaster.numDivisionID
												,ISNULL(DivisionMaster.numTerID,0) numTerID
												,ISNULL(OpportunityMaster.intUsedShippingCompany,0) numOppShipVia
												,ISNULL(OpportunityMaster.numShippingService,0) numOppShipService
												,ISNULL(DivisionMaster.intShippingCompany,0) AS numPreferredShipVia
												,ISNULL(DivisionMaster.numDefaultShippingServiceID,0) numPreferredShipService
												,ISNULL(OpportunityBizDocs.numOppBizDocsId,0) AS numOppBizDocID
												,ISNULL(OpportunityBizDocs.vcBizDocID,'''') vcBizDocID
												,ISNULL(OpportunityBizDocs.monDealAmount,0) - ISNULL(OpportunityBizDocs.monAmountPaid,0) AS monAmountToPay
												,',(CASE WHEN ISNULL(@bitGroupByOrder,0)=1 THEN '0' ELSE 'ISNULL(OpportunityItems.numoppitemtCode,0)' END),' AS numOppItemID
												',(CASE WHEN ISNULL(@bitGroupByOrder,0)=1
														THEN ',0 AS numRemainingQty'
														ELSE ',(CASE 
															WHEN @numViewID = 1
															THEN ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyPicked,0)
															WHEN @numViewID = 2 AND @tintPackingViewMode = 2
															THEN (CASE WHEN ISNULL(@tintPackingMode,1) = 2 THEN ISNULL(OpportunityItems.numQtyPicked,0) - ISNULL(OpportunityItems.numQtyShipped,0) ELSE ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyShipped,0) END)
															WHEN @numViewID = 2 AND @tintPackingViewMode = 3 AND ISNULL(OpportunityBizDocs.numOppBizDocsId,0) = 0
															THEN (CASE WHEN ISNULL(@tintPackingMode,1) = 2 THEN ISNULL(OpportunityItems.numQtyPicked,0) ELSE ISNULL(OpportunityItems.numUnitHour,0) END) - ISNULL((SELECT 
																																																						SUM(OBIInner.numUnitHour)
																																																					FROM
																																																						OpportunityBizDocs OBInner
																																																					INNER JOIN
																																																						OpportunityBizDocItems OBIInner
																																																					ON
																																																						OBInner.numOppBizDocsId = OBIInner.numOppBizDocID
																																																					WHERE
																																																						OBInner.numOppId = OpportunityMaster.numOppID
																																																						AND OBInner.numBizDocId=@numDefaultSalesShippingDoc
					 																																																	AND OBIInner.numOppItemID = OpportunityItems.numoppitemtcode),0)
															WHEN @numViewID = 2 AND @tintPackingViewMode = 3 AND ISNULL(OpportunityBizDocs.numOppBizDocsId,0) > 0
															THEN ISNULL(OpportunityBizDocItems.numUnitHour,0)
															WHEN @numViewID = 3
															THEN (CASE 
																	WHEN ISNULL(@tintInvoicingType,0)=1 
																	THEN ISNULL(OpportunityItems.numQtyShipped,0) 
																	ELSE ISNULL(OpportunityItems.numUnitHour,0) 
																END) - ISNULL((SELECT 
																					SUM(OpportunityBizDocItems.numUnitHour)
																				FROM
																					OpportunityBizDocs
																				INNER JOIN
																					OpportunityBizDocItems
																				ON
																					OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																				WHERE
																					OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
																					AND OpportunityBizDocs.numBizDocId=287
																					AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode),0)
															ELSE 0
														END) numRemainingQty'
													END)
													,','''' dtAnticipatedDelivery,'''' vcShipStatus,'''' vcPaymentStatus,0 numShipRate
													,(CASE 
														WHEN @numViewID = 2 AND @tintPackingViewMode = 2 AND ISNULL(@bitEnablePickListMapping,0) = 1 
														THEN CONCAT(''['',STUFF((SELECT 
																	CONCAT('', {"numOppBizDocsId":'',OB.numOppBizDocsId,'', "vcBizDocName":"'',OB.vcBizDocID,'' '',ISNULL(BT.vcTemplateName,''''),''"}'') 
																FROM 
																	OpportunityBizDocs OB
																LEFT JOIN
																	BizDocTemplate AS BT
																ON
																	OB.numBizDocTempID=BT.numBizDocTempID
																WHERE 
																	OB.numOppId = OpportunityMaster.numOppID
																	AND OB.numBizDocId=29397
																ORDER BY
																	OB.numOppBizDocsId
																FOR XML PATH('''')),1,1,''''),'']'')
														WHEN @numViewID = 3 AND ISNULL(@bitEnableFulfillmentBizDocMapping,0) = 1 
														THEN  CONCAT(''['',STUFF((SELECT 
																	CONCAT('', {"numOppBizDocsId":'',OB.numOppBizDocsId,'', "vcBizDocName":"'',OB.vcBizDocID,'' '',ISNULL(BT.vcTemplateName,''''),''"}'') 
																FROM 
																	OpportunityBizDocs OB
																LEFT JOIN
																	BizDocTemplate AS BT
																ON
																	OB.numBizDocTempID=BT.numBizDocTempID
																WHERE 
																	OB.numOppId = OpportunityMaster.numOppID
																	AND OB.numBizDocId IN (29397,296)
																ORDER BY
																	OB.numOppBizDocsId
																FOR XML PATH('''')),1,1,''''),'']'')
														ELSE ''''
													END) vcLinkingBizDocs,
													(CASE WHEN EXISTS (SELECT OIInner.numOppItemtcode FROM OpportunityItems OIInner WHERE OIInner.numOppID=OpportunityMaster.numOppId AND ISNULL(OIInner.bitRequiredWarehouseCorrection,0)=1) THEN 1 ELSE 0 END) bitRequiredWarehouseCorrection',
													(CASE WHEN @numViewID=2 AND @tintPackingViewMode = 1 THEN ',ISNULL(OpportunityItems.ItemReleaseDate,CAST(OpportunityMaster.dtReleaseDate AS DATE)) dtShipByDate' ELSE ',CAST(OpportunityMaster.dtReleaseDate AS DATE) AS dtShipByDate' END))

	DECLARE @vcFrom NVARCHAR(MAX) = CONCAT(' FROM
												OpportunityMaster
											INNER JOIN
												DivisionMaster
											ON
												OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
											INNER JOIN
												CompanyInfo
											ON
												DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
											INNER JOIN
												OpportunityItems
											ON
												OpportunityMaster.numOppId = OpportunityItems.numOppId
											LEFT JOIN 
												MassSalesFulfillmentBatchOrders
											ON
												MassSalesFulfillmentBatchOrders.numBatchID = @numBatchID
												AND OpportunityMaster.numOppId = MassSalesFulfillmentBatchOrders.numOppID
												AND (OpportunityItems.numOppItemtCode = MassSalesFulfillmentBatchOrders.numOppItemID OR ISNULL(MassSalesFulfillmentBatchOrders.numOppItemID,0) = 0)
											LEFT JOIN
												WareHouseItems
											ON
												OpportunityItems.numWarehouseItmsID = WareHouseItems.numWareHouseItemID
											LEFT JOIN
												WareHouses
											ON
												WareHouseItems.numWarehouseID = WareHouses.numWarehouseID
											LEFT JOIN
												WarehouseLocation
											ON
												WareHouseItems.numWLocationID = WarehouseLocation.numWLocationID
											INNER JOIN
												Item
											ON
												OpportunityItems.numItemCode = Item.numItemCode
											',(CASE WHEN @numViewID=6 THEN ' INNER JOIN ' ELSE ' LEFT JOIN ' END),'
												OpportunityBizDocs
											ON
												OpportunityMaster.numOppId = OpportunityBizDocs.numOppId
												AND OpportunityBizDocs.numBizDocID= ',(CASE 
																						WHEN @numViewID=4 THEN 287 
																						WHEN @numViewID=6 AND @tintPrintBizDocViewMode=1 THEN 55206 
																						WHEN @numViewID=6 AND @tintPrintBizDocViewMode=2 THEN 29397 
																						WHEN @numViewID=6 AND @tintPrintBizDocViewMode=3 THEN 296 
																						WHEN @numViewID=6 AND @tintPrintBizDocViewMode=4 THEN 287 
																						ELSE 0
																					END),'
												AND @numViewID IN (4,6) 
											LEFT JOIN
												OpportunityBizDocItems
											ON
												OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocId
												AND OpportunityItems.numoppitemtCode = OpportunityBizDocItems.numOppItemID')

	DECLARE @vcFrom1 NVARCHAR(MAX) = ' FROM
										OpportunityMaster
									INNER JOIN
										DivisionMaster
									ON
										OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
									INNER JOIN
										CompanyInfo
									ON
										DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
									INNER JOIN
										OpportunityBizDocs
									ON
										OpportunityMaster.numOppId = OpportunityBizDocs.numOppId
										AND OpportunityBizDocs.numBizDocID= @numDefaultSalesShippingDoc
									INNER JOIN
										OpportunityBizDocItems
									ON
										OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocId
									INNER JOIN
										OpportunityItems
									ON
										OpportunityMaster.numOppId = OpportunityItems.numOppId
										AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtCode 
									LEFT JOIN 
										MassSalesFulfillmentBatchOrders
									ON
										MassSalesFulfillmentBatchOrders.numBatchID = @numBatchID
										AND OpportunityMaster.numOppId = MassSalesFulfillmentBatchOrders.numOppID
										AND (OpportunityItems.numOppItemtCode = MassSalesFulfillmentBatchOrders.numOppItemID OR ISNULL(MassSalesFulfillmentBatchOrders.numOppItemID,0) = 0)
									LEFT JOIN
										WareHouseItems
									ON
										OpportunityItems.numWarehouseItmsID = WareHouseItems.numWareHouseItemID
									LEFT JOIN
										WareHouses
									ON
										WareHouseItems.numWarehouseID = WareHouses.numWarehouseID
									LEFT JOIN
										WarehouseLocation
									ON
										WareHouseItems.numWLocationID = WarehouseLocation.numWLocationID
									INNER JOIN
										Item
									ON
										OpportunityItems.numItemCode = Item.numItemCode
										AND 1 = (CASE WHEN @numViewID = 2 AND @tintPackingViewMode = 3 THEN (CASE WHEN ISNULL(Item.bitContainer,0)=1 THEN 0 ELSE 1 END) ELSE 1 END)'

	DECLARE @vcWhere VARCHAR(MAX) = CONCAT(' WHERE
												OpportunityMaster.numDomainId=@numDomainID
												AND OpportunityMaster.tintOppType = 1
												AND OpportunityMaster.tintOppStatus = 1'
												,(CASE 
													WHEN @numViewID=6 AND @tintPendingCloseFilter=1 THEN ' AND ISNULL(OpportunityMaster.tintshipped,0) = 0'
													WHEN @numViewID=6 AND @tintPendingCloseFilter=2 THEN ' AND ISNULL(OpportunityMaster.tintshipped,0) = 1'
													WHEN @numViewID=6 AND @tintPendingCloseFilter=3 THEN ''
													ELSE ' AND ISNULL(OpportunityMaster.tintshipped,0) = 0'
												END)
												,(CASE WHEN @numViewID <> 6 AND ISNULL(@numWarehouseID,0) > 0 THEN ' AND 1 = (CASE WHEN ISNULL(OpportunityItems.numWarehouseItmsID,0) > 0 THEN (CASE WHEN @numWarehouseID=WareHouseItems.numWarehouseID OR ISNULL(OpportunityItems.bitRequiredWarehouseCorrection,0) = 1 THEN 1 ELSE 0 END) ELSE 1 END)' ELSE '' END)
												,(CASE 
													WHEN (@numViewID = 1 OR (@numViewID = 2 AND @tintPackingViewMode <> 1)) AND ISNULL(@bitRemoveBO,0) = 1
													THEN ' AND 1 = (CASE 
																		WHEN (Item.charItemType = ''p'' AND ISNULL(OpportunityItems.bitDropShip, 0) = 0 AND ISNULL(Item.bitAsset,0)=0)
																		THEN 
																			(CASE 
																				WHEN dbo.CheckOrderItemInventoryStatus(OpportunityItems.numItemCode,(CASE 
																																						WHEN @numViewID = 1
																																						THEN ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyPicked,0)
																																						WHEN @numViewID = 2 AND @tintPackingViewMode = 2
																																						THEN (CASE WHEN ISNULL(@tintPackingMode,1) = 2 THEN ISNULL(OpportunityItems.numQtyPicked,0) - ISNULL(OpportunityItems.numQtyShipped,0) ELSE ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyShipped,0) END)
																																						WHEN @numViewID = 2 AND @tintPackingViewMode = 3
																																						THEN (CASE WHEN ISNULL(@tintPackingMode,1) = 2 THEN ISNULL(OpportunityItems.numQtyPicked,0) ELSE ISNULL(OpportunityItems.numUnitHour,0) END) - ISNULL((SELECT 
																																																																													SUM(OpportunityBizDocItems.numUnitHour)
																																																																												FROM
																																																																													OpportunityBizDocs
																																																																												INNER JOIN
																																																																													OpportunityBizDocItems
																																																																												ON
																																																																													OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																																																																												WHERE
																																																																													OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
																																																																													AND OpportunityBizDocs.numBizDocId=@numDefaultSalesShippingDoc
					 																																																																							AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode),0)
				
																																						ELSE OpportunityItems.numUnitHour
																																					END),OpportunityItems.numoppitemtCode,OpportunityItems.numWarehouseItmsID,(CASE WHEN ISNULL(Item.bitKitParent,0) = 1 THEN 1 ELSE 0 END)) > 0 THEN 0 ELSE 1 END)
																							ELSE 1
																				END)'
														ELSE ''
												END)
												,(CASE 
													WHEN LEN(ISNULL(@vcOrderStauts,'')) > 0 
													THEN (CASE 
															WHEN ISNULL(@bitIncludeSearch,0) = 0 
															THEN ' AND 1 = (CASE WHEN OpportunityMaster.numStatus NOT IN (SELECT Id FROM dbo.SplitIDs(@vcOrderStauts,'','')) THEN 1 ELSE 0 END)'
															ELSE ' AND 1 = (CASE WHEN OpportunityMaster.numStatus IN (SELECT Id FROM dbo.SplitIDs(@vcOrderStauts,'','')) THEN 1 ELSE 0 END)'
														END)
													ELSE ''
												END)
												,(CASE 
													WHEN LEN(ISNULL(@vcFilterValue,'')) > 0
													THEN
														CASE 
															WHEN @tintFilterBy=1 --Item Classification
															THEN ' AND 1 = (CASE 
																				WHEN ISNULL(@bitIncludeSearch,0) = 0 
																				THEN (CASE WHEN Item.numItemClassification NOT IN (SELECT Id FROM dbo.SplitIDs(@vcFilterValue,'','')) THEN 1 ELSE 0 END) 
																				ELSE (CASE WHEN Item.numItemClassification IN (SELECT Id FROM dbo.SplitIDs(@vcFilterValue,'','')) THEN 1 ELSE 0 END) 
																			END)'
															ELSE ''
														END
													ELSE ''
												END)
												,(CASE
													WHEN @numViewID = 1 THEN ' AND 1 = (CASE WHEN ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyPicked,0) > 0 THEN 1 ELSE 0 END)'
													WHEN @numViewID = 2 THEN ' AND 1 = (CASE 
																							WHEN @tintPackingViewMode = 1 
																							THEN (CASE 
																									WHEN ISNULL((SELECT COUNT(*) FROM OpportunityItems OIInner WHERE OIInner.numOppID=OpportunityMaster.numOppId AND OIInner.numItemCode=ISNULL(@numShippingServiceItemID,0) AND OIInner.numShipFromWarehouse=@numWarehouseID AND OIInner.ItemReleaseDate=ISNULL(OpportunityItems.ItemReleaseDate,CAST(OpportunityMaster.dtReleaseDate AS DATE))),0) = 0 
																									THEN 1 
																									ELSE 0 
																								END)
																							WHEN @tintPackingViewMode = 2
																							THEN (CASE 
																									WHEN (CASE WHEN ISNULL(@tintPackingMode,1) = 2 THEN ISNULL(OpportunityItems.numQtyPicked,0) ELSE ISNULL(OpportunityItems.numUnitHour,0) END) - ISNULL(OpportunityItems.numQtyShipped,0) > 0
																									THEN 1 
																									ELSE 0 
																								END)
																							WHEN @tintPackingViewMode = 3
																							THEN (CASE 
																									WHEN (CASE WHEN ISNULL(@tintPackingMode,1) = 2 THEN ISNULL(OpportunityItems.numQtyPicked,0) ELSE ISNULL(OpportunityItems.numUnitHour,0) END) - ISNULL((SELECT 
																																																																SUM(OBIInner.numUnitHour)
																																																															FROM
																																																																OpportunityBizDocs OBInner
																																																															INNER JOIN
																																																																OpportunityBizDocItems OBIInner
																																																															ON
																																																																OBInner.numOppBizDocsId = OBIInner.numOppBizDocID
																																																															WHERE
																																																																OBInner.numOppId = OpportunityMaster.numOppID
																																																																AND OBInner.numBizDocId=@numDefaultSalesShippingDoc
																																																																AND OBIInner.numOppItemID = OpportunityItems.numoppitemtcode),0) > 0
																									THEN 1 
																									ELSE 0 
																								END)
																							ELSE 0
																						END)'
													WHEN @numViewID = 3
													THEN ' AND 1 = (CASE 
																		WHEN (CASE 
																				WHEN ISNULL(@tintInvoicingType,0)=1 
																				THEN ISNULL(OpportunityItems.numQtyShipped,0) 
																				ELSE ISNULL(OpportunityItems.numUnitHour,0) 
																			END) - ISNULL((SELECT 
																								SUM(OpportunityBizDocItems.numUnitHour)
																							FROM
																								OpportunityBizDocs
																							INNER JOIN
																								OpportunityBizDocItems
																							ON
																								OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																							WHERE
																								OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
																								AND OpportunityBizDocs.numBizDocId=287
																								AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode),0) > 0 
																		THEN 1
																		ELSE 0 
																	END)'
												END)
												,(CASE
															WHEN @numViewID = 1 THEN 'AND 1 = (CASE WHEN ISNULL(WareHouseItems.numWareHouseItemID,0) > 0 AND ISNULL(OpportunityItems.bitDropship,0) = 0 THEN 1 ELSE 0 END)'
															WHEN @numViewID = 2 AND @tintPackingViewMode <> 2 THEN ' AND 1 = (CASE WHEN ISNULL(WareHouseItems.numWareHouseItemID,0) > 0 AND ISNULL(OpportunityItems.bitDropship,0) = 0 THEN 1 ELSE 0 END)'
															WHEN @numViewID = 2 AND @tintPackingViewMode = 2 THEN ' AND 1=1'
															WHEN @numViewID = 3 THEN ' AND 1=1'
															WHEN @numViewID = 4 THEN ' AND 1=1'
															WHEN @numViewID = 5 THEN ' AND 1=1'
															WHEN @numViewID = 6 THEN ' AND 1=1'
															ELSE ' AND 1=0'
													END)
												,(CASE WHEN ISNULL(@numBatchID,0) > 0 THEN ' AND 1 = (CASE WHEN MassSalesFulfillmentBatchOrders.ID IS NOT NULL THEN 1 ELSE 0 END)' ELSE '' END)
												,(CASE WHEN @numViewID = 4 OR @numViewID=6 THEN ' AND 1= (CASE WHEN OpportunityBizDocs.numOppBizDocsId IS NOT NULL THEN 1 ELSE 0 END)' ELSE '' END)
												,(CASE WHEN @numViewID = 4 THEN ' AND 1 = (CASE WHEN ISNULL(OpportunityBizDocs.monDealAmount,0) - ISNULL(OpportunityBizDocs.monAmountPaid,0) > 0 THEN 1 ELSE 0 END)' ELSE '' END)
												,(CASE 
													WHEN @numViewID = 5 AND ISNULL(@tintPendingCloseFilter,0) = 2
													THEN 
														' AND 1 = (CASE 
																	WHEN (SELECT 
																				COUNT(*) 
																			FROM 
																			(
																				SELECT
																					OI.numoppitemtCode,
																					ISNULL(OI.numUnitHour,0) AS OrderedQty,
																					ISNULL(TempFulFilled.FulFilledQty,0) AS FulFilledQty
																				FROM
																					OpportunityItems OI
																				INNER JOIN
																					Item I
																				ON
																					OI.numItemCode = I.numItemCode
																				OUTER APPLY
																				(
																					SELECT
																						SUM(OpportunityBizDocItems.numUnitHour) AS FulFilledQty
																					FROM
																						OpportunityBizDocs
																					INNER JOIN
																						OpportunityBizDocItems 
																					ON
																						OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																					WHERE
																						OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
																						AND ISNULL(OpportunityBizDocs.numBizDocId,0) = 296
																						AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
																				) AS TempFulFilled
																				WHERE
																					OI.numOppID = OpportunityMaster.numOppID
																					AND ISNULL(OI.numUnitHour,0) > 0
																					AND ISNULL(OI.bitDropShip,0) = 0
																			) X
																			WHERE
																				X.OrderedQty <> X.FulFilledQty) = 0
																		AND (SELECT 
																				COUNT(*) 
																			FROM 
																			(
																				SELECT
																					OI.numoppitemtCode,
																					ISNULL(OI.numUnitHour,0) AS OrderedQty,
																					ISNULL(TempInvoice.InvoicedQty,0) AS InvoicedQty
																				FROM
																					OpportunityItems OI
																				INNER JOIN
																					Item I
																				ON
																					OI.numItemCode = I.numItemCode
																				OUTER APPLY
																				(
																					SELECT
																						SUM(OpportunityBizDocItems.numUnitHour) AS InvoicedQty
																					FROM
																						OpportunityBizDocs
																					INNER JOIN
																						OpportunityBizDocItems 
																					ON
																						OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																					WHERE
																						OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
																						AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
																						AND ISNULL(OpportunityBizDocs.bitAuthoritativeBizDocs,0) = 1
																				) AS TempInvoice
																				WHERE
																					OI.numOppID = OpportunityMaster.numOppID
																					AND ISNULL(OI.numUnitHour,0) > 0
																			) X
																			WHERE
																				X.OrderedQty > X.InvoicedQty) = 0
																	THEN 1 
																	ELSE 0 
																END)'
															ELSE ''
														END)
												,(CASE 
													WHEN ISNULL(@numShippingZone,0) > 0 
													THEN ' AND 1 = (CASE 
																		WHEN ISNULL((SELECT TOP 1 numState FROM fn_getOPPAddressDetails(OpportunityMaster.numOppId,OpportunityMaster.numDomainID,2)),0) > 0 
																		THEN
																			CASE 
																				WHEN (SELECT COUNT(*) FROM dbo.State WHERE numDomainID=@numDomainId AND numStateID =  (SELECT TOP 1 numState FROM fn_getOPPAddressDetails(OpportunityMaster.numOppId,OpportunityMaster.numDomainID,2)) AND numShippingZone=@numShippingZone) > 0 
																				THEN 1
																				ELSE 0
																			END
																		ELSE 0 
																	END)'
													ELSE ''
												END)
												,(CASE WHEN ISNULL(@bitGroupByOrder,0)=0 THEN ' AND 1 = (CASE WHEN ISNULL(OpportunityBizDocs.numOppBizDocsId,0) > 0 THEN (CASE WHEN OpportunityBizDocItems.numOppItemID > 0 THEN 1 ELSE 0 END) ELSE 1 END)' ELSE '' END))

	DECLARE @vcWhere1 VARCHAR(MAX) = CONCAT(' WHERE
												OpportunityMaster.numDomainId=@numDomainID
												AND OpportunityMaster.tintOppType = 1
												AND OpportunityMaster.tintOppStatus = 1
												AND ISNULL(OpportunityMaster.tintshipped,0) = 0
												AND 1 = (CASE 
															WHEN LEN(ISNULL(@vcOrderStauts,'''')) > 0 
															THEN (CASE 
																	WHEN ISNULL(@bitIncludeSearch,0) = 0 
																	THEN (CASE WHEN OpportunityMaster.numStatus NOT IN (SELECT Id FROM dbo.SplitIDs(@vcOrderStauts,'','')) THEN 1 ELSE 0 END) 
																	ELSE (CASE WHEN OpportunityMaster.numStatus IN (SELECT Id FROM dbo.SplitIDs(@vcOrderStauts,'','')) THEN 1 ELSE 0 END) 
																END)
															ELSE 1 
														END)
												AND 1 = (CASE 
															WHEN LEN(ISNULL(@vcFilterValue,'''')) > 0
															THEN
																CASE 
																	WHEN @tintFilterBy=1 --Item Classification
																	THEN (CASE 
																			WHEN ISNULL(@bitIncludeSearch,0) = 0 
																			THEN (CASE WHEN Item.numItemClassification NOT IN (SELECT Id FROM dbo.SplitIDs(@vcFilterValue,'','')) THEN 1 ELSE 0 END) 
																			ELSE (CASE WHEN Item.numItemClassification IN (SELECT Id FROM dbo.SplitIDs(@vcFilterValue,'','')) THEN 1 ELSE 0 END) 
																		END)
																	ELSE 1
																END
															ELSE 1
														END)
												AND 1 = (CASE 
															WHEN ISNULL((SELECT COUNT(*) FROM ShippingBox WHERE ISNULL(vcShippingLabelImage,'''') <> '''' AND ISNULL(vcTrackingNumber,'''') <> '''' AND numShippingReportId IN (SELECT numShippingReportId FROM ShippingReport WHERE ShippingReport.numOppID=OpportunityMaster.numOppId AND ShippingReport.numOppBizDocId=OpportunityBizDocs.numOppBizDocsId)),0) = 0 
															THEN 1 
															ELSE 0 
														END)
												AND 1 = (CASE WHEN ISNULL(WareHouseItems.numWareHouseItemID,0) > 0 AND ISNULL(OpportunityItems.bitDropship,0) = 0 THEN 1 ELSE 0 END)
												AND 1 = (CASE WHEN ISNULL(@numWarehouseID,0) > 0 AND ISNULL(OpportunityItems.numWarehouseItmsID,0) > 0 THEN (CASE WHEN @numWarehouseID=WareHouseItems.numWarehouseID OR ISNULL(OpportunityItems.bitRequiredWarehouseCorrection,0) = 1 THEN 1 ELSE 0 END) ELSE 1 END)
												AND 1 = (CASE WHEN ISNULL(@numBatchID,0) > 0 THEN (CASE WHEN MassSalesFulfillmentBatchOrders.ID IS NOT NULL THEN 1 ELSE 0 END) ELSE 1 END)'
												,(CASE 
													WHEN ISNULL(@numShippingZone,0) > 0 
													THEN ' AND 1 = (CASE 
																		WHEN ISNULL((SELECT TOP 1 numState FROM fn_getOPPAddressDetails(OpportunityMaster.numOppId,OpportunityMaster.numDomainID,2)),0) > 0 
																		THEN
																			CASE 
																				WHEN (SELECT COUNT(*) FROM dbo.State WHERE numDomainID=@numDomainId AND numStateID =  (SELECT TOP 1 numState FROM fn_getOPPAddressDetails(OpportunityMaster.numOppId,OpportunityMaster.numDomainID,2)) AND numShippingZone=@numShippingZone) > 0 
																				THEN 1
																				ELSE 0
																			END
																		ELSE 0 
																	END)'
													ELSE ''
												END)
												,(CASE WHEN ISNULL(@bitGroupByOrder,0)=0 THEN ' AND 1 = (CASE WHEN ISNULL(OpportunityBizDocs.numOppBizDocsId,0) > 0 THEN (CASE WHEN OpportunityBizDocItems.numOppItemID > 0 THEN 1 ELSE 0 END) ELSE 1 END)' ELSE '' END))

	SET @vcWhere = CONCAT(@vcWhere,' ',@vcCustomSearchValue)
	SET @vcWhere1 = CONCAT(@vcWhere1,' ',@vcCustomSearchValue)
	
	DECLARE @vcGroupBy NVARCHAR(MAX) = (CASE 
											WHEN @bitGroupByOrder = 1 
											THEN CONCAT('GROUP BY OpportunityMaster.numOppID
																	,DivisionMaster.numDivisionID
																	,DivisionMaster.numTerID
																	,DivisionMaster.intShippingCompany
																	,DivisionMaster.numDefaultShippingServiceID
																	,OpportunityBizDocs.numOppBizDocsId
																	,OpportunityBizDocs.monDealAmount
																	,OpportunityBizDocs.monAmountPaid
																	,CompanyInfo.vcCompanyName
																	,OpportunityMaster.vcPoppName
																	,OpportunityMaster.numAssignedTo
																	,OpportunityMaster.numStatus
																	,OpportunityMaster.numRecOwner
																	,OpportunityMaster.bintCreatedDate
																	,OpportunityMaster.txtComments
																	,OpportunityBizDocs.vcBizDocID
																	,OpportunityMaster.intUsedShippingCompany
																	,OpportunityMaster.numShippingService
																	,DivisionMaster.intShippingCompany
																	,OpportunityBizDocs.dtCreatedDate
																	,OpportunityMaster.dtReleaseDate
																	,OpportunityMaster.dtExpectedDate',(CASE WHEN @numViewID = 2 AND @tintPackingViewMode = 1 THEN ',OpportunityItems.ItemReleaseDate' ELSE '' END))
											ELSE '' 
										END)

	DECLARE @vcOrderBy NVARCHAR(MAX) = CONCAT(' ORDER BY ',
					(CASE @vcSortColumn
						WHEN 'CompanyInfo.vcCompanyName' THEN 'CompanyInfo.vcCompanyName' 
						WHEN 'Item.charItemType' THEN '(CASE 
															WHEN Item.charItemType=''P''  THEN ''Inventory Item''
															WHEN Item.charItemType=''N'' THEN ''Non Inventory Item'' 
															WHEN Item.charItemType=''S'' THEN ''Service''
															WHEN Item.charItemType=''A'' THEN ''Accessory''
														END)'
						WHEN 'Item.numBarCodeId' THEN 'Item.numBarCodeId'
						WHEN 'Item.numItemClassification' THEN 'dbo.GetListIemName(Item.numItemClassification)'
						WHEN 'Item.numItemGroup' THEN 'ISNULL((SELECT vcItemGroup FROM ItemGroups WHERE numItemGroupID = Item.numItemGroup),'''')'
						WHEN 'Item.vcSKU' THEN 'Item.vcSKU'
						WHEN 'OpportunityItems.vcItemName' THEN 'Item.vcItemName'
						WHEN 'OpportunityMaster.numStatus' THEN 'dbo.GetListIemName(OpportunityMaster.numStatus)'
						WHEN 'OpportunityMaster.vcPoppName' THEN 'OpportunityMaster.vcPoppName'
						WHEN 'OpportunityMaster.numAssignedTo' THEN 'dbo.fn_GetContactName(OpportunityMaster.numAssignedTo)'
						WHEN 'OpportunityMaster.numRecOwner' THEN 'dbo.fn_GetContactName(OpportunityMaster.numRecOwner)'
						WHEN 'WareHouseItems.vcLocation' THEN 'WarehouseLocation.vcLocation'
						WHEN 'OpportunityBizDocs.monAmountPaid' THEN 'TempPaid.monAmountPaid'
						WHEN 'OpportunityBizDocs.vcBizDocID' THEN 'LEN(OpportunityBizDocs.vcBizDocID) DESC, OpportunityBizDocs.vcBizDocID'
						WHEN 'OpportunityItems.numQtyPacked' THEN 'ISNULL((SELECT SUM(numUnitHour) FROM OpportunityBizDocs OBD INNER JOIN OpportunityBizDocItems OBDI ON OBD.numOppBizDocsId=OBDI.numOppBizDocID  WHERE OBD.numOppId=OpportunityMaster.numOppId AND OBD.numBizDocId=29397 AND OBDI.numOppItemID=OpportunityItems.numoppitemtCode),0)'
						WHEN 'OpportunityItems.numQtyPicked' THEN 'OpportunityItems.numQtyPicked'
						WHEN 'OpportunityItems.numQtyShipped' THEN 'OpportunityItems.numQtyShipped'
						WHEN 'OpportunityItems.numRemainingQty' THEN '(CASE 
																			WHEN @numViewID = 1
																			THEN ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyPicked,0)
																			WHEN @numViewID = 2
																			THEN ISNULL(OpportunityItems.numUnitHour,0) - ISNULL((SELECT SUM(numUnitHour) FROM OpportunityBizDocs OBD INNER JOIN OpportunityBizDocItems OBDI ON OBD.numOppBizDocsId=OBDI.numOppBizDocID  WHERE OBD.numOppId=OpportunityMaster.numOppId AND OBD.numBizDocId=29397 AND OBDI.numOppItemID=OpportunityItems.numoppitemtCode),0)
																			ELSE 0
																		END)'
						WHEN 'OpportunityItems.numUnitHour' THEN 'OpportunityItems.numUnitHour'
						WHEN 'OpportunityItems.vcInvoiced' THEN 'ISNULL((SELECT SUM(numUnitHour) FROM OpportunityBizDocs OBD INNER JOIN OpportunityBizDocItems OBDI ON OBD.numOppBizDocsId=OBDI.numOppBizDocID  WHERE OBD.numOppId=OpportunityMaster.numOppId AND OBD.numBizDocId=287 AND OBDI.numOppItemID=OpportunityItems.numoppitemtCode),0)'
						WHEN 'OpportunityMaster.dtExpectedDate' THEN 'OpportunityMaster.dtExpectedDate'
						WHEN 'OpportunityItems.vcItemReleaseDate' THEN 'OpportunityItems.ItemReleaseDate'
						WHEN 'OpportunityMaster.bintCreatedDate' THEN 'OpportunityMaster.bintCreatedDate'
						WHEN 'OpportunityMaster.numAge' THEN 'OpportunityMaster.bintCreatedDate'
						WHEN 'OpportunityMaster.bitPaidInFull' THEN '(CASE WHEN ISNULL(TempPaid.monAmountPaid,0) >= ISNULL(OpportunityMaster.monDealAmount,0) THEN 1 ELSE 0 END)'
						WHEN 'Warehouses.vcWareHouse' THEN '(CASE WHEN ISNULL(OpportunityItems.bitRequiredWarehouseCorrection,0) = 1 THEN '''' ELSE Warehouses.vcWareHouse END)'
						WHEN 'Item.dtItemReceivedDate' THEN 'Item.dtItemReceivedDate'
						ELSE 'OpportunityMaster.bintCreatedDate'
					END),' ',(CASE WHEN ISNULL(@vcSortOrder,'') <> '' THEN @vcSortOrder ELSE 'ASC' END))

	DECLARE @vcOrderBy1 NVARCHAR(MAX) = CONCAT(' ORDER BY ',
					(CASE @vcSortColumn
						WHEN 'CompanyInfo.vcCompanyName' THEN 'TEMP.vcCompanyNameOrig' 
						WHEN 'Item.charItemType' THEN '(CASE 
															WHEN TEMP.charItemTypeOrig=''P''  THEN ''Inventory Item''
															WHEN TEMP.charItemTypeOrig=''N'' THEN ''Non Inventory Item'' 
															WHEN TEMP.charItemTypeOrig=''S'' THEN ''Service''
															WHEN TEMP.charItemTypeOrig=''A'' THEN ''Accessory''
														END)'
						WHEN 'Item.numBarCodeId' THEN 'TEMP.numBarCodeIdOrig'
						WHEN 'Item.numItemClassification' THEN 'dbo.GetListIemName(TEMP.numItemClassificationOrig)'
						WHEN 'Item.numItemGroup' THEN 'ISNULL((SELECT vcItemGroup FROM ItemGroups WHERE numItemGroupID = TEMP.numItemGroupOrig),'''')'
						WHEN 'Item.vcSKU' THEN 'TEMP.vcSKUOrig'
						WHEN 'OpportunityItems.vcItemName' THEN 'TEMP.vcItemNameOrig'
						WHEN 'OpportunityMaster.numStatus' THEN 'dbo.GetListIemName(TEMP.numStatusOrig)'
						WHEN 'OpportunityMaster.vcPoppName' THEN 'TEMP.vcPoppNameOrig'
						WHEN 'OpportunityMaster.numAssignedTo' THEN 'dbo.fn_GetContactName(TEMP.numAssignedToOrig)'
						WHEN 'OpportunityMaster.numRecOwner' THEN 'dbo.fn_GetContactName(TEMP.numRecOwnerOrig)'
						WHEN 'WareHouseItems.vcLocation' THEN 'TEMP.vcLocationOrig'
						WHEN 'OpportunityBizDocs.monAmountPaid' THEN 'TEMP.monAmountPaidOrig'
						WHEN 'OpportunityBizDocs.vcBizDocID' THEN 'LEN(TEMP.vcBizDocID) DESC, TEMP.vcBizDocID'
						WHEN 'OpportunityItems.numQtyPacked' THEN 'ISNULL((SELECT SUM(numUnitHour) FROM OpportunityBizDocs OBD INNER JOIN OpportunityBizDocItems OBDI ON OBD.numOppBizDocsId=OBDI.numOppBizDocID  WHERE OBD.numOppId=TEMP.numOppId AND OBD.numBizDocId=29397 AND OBDI.numOppItemID=TEMP.numOppItemID),0)'
						WHEN 'OpportunityItems.numQtyPicked' THEN 'TEMP.numQtyPickedOrig'
						WHEN 'OpportunityItems.numQtyShipped' THEN 'TEMP.numQtyShippedOrig'
						WHEN 'OpportunityItems.numRemainingQty' THEN '(CASE 
																			WHEN @numViewID = 1
																			THEN ISNULL(TEMP.numUnitHourOrig,0) - ISNULL(OpportunityItems.numQtyPickedOrig,0)
																			WHEN @numViewID = 2
																			THEN ISNULL(TEMP.numUnitHourOrig,0) - ISNULL((SELECT SUM(numUnitHour) FROM OpportunityBizDocs OBD INNER JOIN OpportunityBizDocItems OBDI ON OBD.numOppBizDocsId=OBDI.numOppBizDocID  WHERE OBD.numOppId=TEMP.numOppId AND OBD.numBizDocId=29397 AND OBDI.numOppItemID=TEMP.numOppItemID),0)
																			ELSE 0
																		END)'
						WHEN 'OpportunityItems.numUnitHour' THEN 'TEMP.numUnitHourOrig'
						WHEN 'OpportunityItems.vcInvoiced' THEN 'ISNULL((SELECT SUM(numUnitHour) FROM OpportunityBizDocs OBD INNER JOIN OpportunityBizDocItems OBDI ON OBD.numOppBizDocsId=OBDI.numOppBizDocID  WHERE OBD.numOppId=TEMP.numOppId AND OBD.numBizDocId=287 AND OBDI.numOppItemID=TEMP.numOppItemID),0)'
						WHEN 'OpportunityMaster.dtExpectedDate' THEN 'TEMP.dtExpectedDateOrig'
						WHEN 'OpportunityItems.vcItemReleaseDate' THEN 'TEMP.ItemReleaseDateOrig'
						WHEN 'OpportunityMaster.bintCreatedDate' THEN 'TEMP.bintCreatedDateOrig'
						WHEN 'OpportunityMaster.numAge' THEN 'TEMP.bintCreatedDateOrig'
						WHEN 'OpportunityMaster.bitPaidInFull' THEN '(CASE WHEN ISNULL(TEMP.monAmountPaidOrig,0) >= ISNULL(TEMP.monDealAmountOrig,0) THEN 1 ELSE 0 END)'
						WHEN 'Item.dtItemReceivedDate' THEN 'TEMP.dtItemReceivedDate'
						WHEN 'Warehouses.vcWareHouse' THEN '(CASE WHEN ISNULL(OpportunityItems.bitRequiredWarehouseCorrection,0) = 1 THEN '''' ELSE Warehouses.vcWareHouse END)'
						ELSE 'TEMP.bintCreatedDateOrig'
					END),' ',(CASE WHEN ISNULL(@vcSortOrder,'') <> '' THEN @vcSortOrder ELSE 'ASC' END))

	DECLARE @vcText NVARCHAR(MAX)

	IF EXISTS (SELECT ID FROM @TempFieldsLeft WHERE vcOrigDbColumnName='bitPaidInFull') OR @vcSortColumn='OpportunityMaster.bitPaidInFull'
	BEGIN
		SET @vcText = ' OUTER APPLY
						(
							SELECT 
								SUM(OBInner.monAmountPaid) monAmountPaid
							FROM 
								OpportunityBizDocs OBInner 
							WHERE 
								OBInner.numOppId=OpportunityMaster.numOppId 
								AND OBInner.numBizDocId=287
						) TempPaid'

		SET @vcFrom = CONCAT(@vcFrom,@vcText)
		SET @vcFrom1 = CONCAT(@vcFrom1,@vcText)
	END

	IF EXISTS (SELECT ID FROM @TempFieldsLeft WHERE vcOrigDbColumnName='numQtyPacked') OR @vcSortColumn='OpportunityItems.numQtyPacked'
	BEGIN
		SET @vcText = ' OUTER APPLY
						(
							SELECT 
								SUM(OpportunityBizDocItems.numUnitHour) numPackedQty
							FROM 
								OpportunityBizDocs
							INNER JOIN
								OpportunityBizDocItems
							ON
								OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
							WHERE 
								OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
								AND OpportunityBizDocs.numBizDocId=@numDefaultSalesShippingDoc
								AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode
						) TempPacked'

		SET @vcFrom = CONCAT(@vcFrom,@vcText)
		SET @vcFrom1 = CONCAT(@vcFrom1,@vcText)
	END

	DECLARE @j INT = 0
	DECLARE @jCount INT
	DECLARE @numFieldId NUMERIC(18,0)
	DECLARE @vcListItemType VARCHAR(10)
	DECLARE @vcFieldName AS VARCHAR(50)                                                                                                
	DECLARE @vcAssociatedControlType VARCHAR(10)  
	DECLARE @vcLookBackTableName VARCHAR(100)
	DECLARE @vcDbColumnName VARCHAR(100)
	DECLARE @Grp_id INT                                               
	DECLARE @bitCustomField BIT      
	DECLARE @bitIsNumeric BIT
	SET @jCount = (SELECT COUNT(*) FROM @TempFieldsLeft)


	WHILE @j <= @jCount
	BEGIN
		SELECT
			@numFieldId=numFieldID,
			@vcFieldName = vcFieldName,
			@vcAssociatedControlType = vcAssociatedControlType,
			@vcDbColumnName = (CASE WHEN ISNULL(bitCustomField,0) = 1 THEN CONCAT('Cust',numFieldID) ELSE vcOrigDbColumnName END),
			@vcLookBackTableName = vcLookBackTableName,
			@bitCustomField=ISNULL(bitCustomField,0),
			@Grp_id = ISNULL(Grp_id,0),
			@bitIsNumeric=ISNULL(bitIsNumeric,0),
			@vcListItemType=ISNULL(vcListItemType,'')
		FROM 
			@TempFieldsLeft
		WHERE 
			numPKID = @j

		IF @vcDbColumnName NOT IN ('vcBizDocID','numRemainingQty','dtAnticipatedDelivery','vcShipStatus','vcPaymentStatus','numShipRate')
		BEGIN
			IF ISNULL(@bitCustomField,0) = 0
			BEGIN
				IF @vcDbColumnName = 'vcItemReleaseDate'
				BEGIN
					SET @vcSelect = CONCAT(@vcSelect,',',(CASE WHEN @numViewID=2 AND @tintPackingViewMode = 1 THEN 'ISNULL(OpportunityItems.ItemReleaseDate,CAST(OpportunityMaster.dtReleaseDate AS DATE))' ELSE 'CAST(OpportunityMaster.dtReleaseDate AS DATE)' END),' [',@vcDbColumnName,']')
				END
				ELSE IF (@vcLookBackTableName = 'Item' OR @vcLookBackTableName = 'WareHouseItems' OR @vcLookBackTableName = 'OpportunityItems' OR @vcLookBackTableName='Warehouses') AND ISNULL(@bitGroupByOrder,0)=1
				BEGIN
					SET @vcSelect = CONCAT(@vcSelect,',',(CASE WHEN @bitIsNumeric = 1 THEN '0' ELSE '''''' END),' [',@vcDbColumnName,']')
				END
				ELSE
				BEGIN
					IF @vcDbColumnName = 'numAge'
					BEGIN
						SET @vcSelect = CONCAT(@vcSelect,',CONCAT(''<span style="color:'',(CASE
																							WHEN (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24) = 0 AND (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) < 4  
																							THEN ''#00ff00''
																							WHEN (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24) = 0 AND ((DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) >= 4 AND (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) < 8)
																							THEN ''#00b050''
																							WHEN (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24) = 0 AND ((DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) >= 8 AND (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) < 12)
																							THEN ''#3399ff''
																							WHEN (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24) = 0 AND ((DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) >= 12 AND (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) < 24)
																							THEN ''#ff9900''
																							WHEN (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24) BETWEEN 1 AND 2  
																							THEN ''#9b3596''
																							ELSE ''#ff0000''
																						END),''">'',CONCAT(DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24,''d '',DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24,''h''),''</span>'')',' [',@vcDbColumnName,']')
					END
					ELSE IF @vcDbColumnName = 'dtItemReceivedDate'
					BEGIN
						SET @vcSelect = CONCAT(@vcSelect,',dbo.FormatedDateFromDate(Item.dtItemReceivedDate,@numDomainID) [',@vcDbColumnName,']')
					END
					ELSE IF @vcDbColumnName = 'bitPaidInFull'
					BEGIN
						SET @vcSelect = CONCAT(@vcSelect,',(CASE WHEN ISNULL(TempPaid.monAmountPaid,0) >= ISNULL(OpportunityMaster.monDealAmount,0) THEN 1 ELSE 0 END) [',@vcDbColumnName,']')
					END
					ELSE IF @vcDbColumnName = 'dtExpectedDate'
					BEGIN
						SET @vcSelect = CONCAT(@vcSelect,',dbo.FormatedDateFromDate(OpportunityMaster.dtExpectedDate,@numDomainID) [',@vcDbColumnName,']')
					END
					ELSE IF @vcDbColumnName = 'bintCreatedDate'
					BEGIN
						SET @vcSelect = CONCAT(@vcSelect,',(CASE WHEN @numViewID = 6 THEN dbo.FormatedDateFromDate(DATEADD(MINUTE,@ClientTimeZoneOffset * -1,OpportunityBizDocs.dtCreatedDate),@numDomainID) ELSE dbo.FormatedDateFromDate(DATEADD(MINUTE,@ClientTimeZoneOffset * -1,OpportunityMaster.bintCreatedDate),@numDomainID) END) [',@vcDbColumnName,']')
					END
					ELSE IF @vcDbColumnName = 'numQtyPacked'
					BEGIN
						SET @vcSelect = CONCAT(@vcSelect,',ISNULL(TempPacked.numPackedQty,0) [',@vcDbColumnName,']')
					END
					ELSE IF @vcDbColumnName = 'vcInvoiced'
					BEGIN
						SET @vcSelect = CONCAT(@vcSelect,',ISNULL((SELECT SUM(numUnitHour) FROM OpportunityBizDocs OBD INNER JOIN OpportunityBizDocItems OBDI ON OBD.numOppBizDocsId=OBDI.numOppBizDocID  WHERE OBD.numOppId=OpportunityMaster.numOppId AND OBD.numBizDocId=287 AND OBDI.numOppItemID=OpportunityItems.numoppitemtCode),0) [',@vcDbColumnName,']')
					END
					ELSE IF @vcDbColumnName = 'vcLocation'
					BEGIN
						SET @vcSelect = CONCAT(@vcSelect,',ISNULL(WarehouseLocation.vcLocation,'''') [',@vcDbColumnName,']')
					END
					ELSE IF @vcDbColumnName = 'charItemType'
					BEGIN
						SET @vcSelect = CONCAT(@vcSelect,',(CASE 
																	WHEN Item.charItemType=''P'' 
																	THEN 
																		CASE 
																			WHEN ISNULL(Item.bitAssembly,0) = 1 THEN ''Assembly''
																			WHEN ISNULL(Item.bitKitParent,0) = 1 THEN ''Kit''
																			WHEN ISNULL(Item.bitAsset,0) = 1 AND ISNULL(Item.bitRental,0)=0 THEN ''Asset''
																			WHEN ISNULL(Item.bitAsset,0) = 1 AND ISNULL(Item.bitRental,0)=1 THEN ''Rental Asset''
																			WHEN ISNULL(Item.bitSerialized,0) = 1 AND ISNULL(Item.bitAsset,0) = 0  THEN ''Serialized''
																			WHEN ISNULL(Item.bitSerialized,0) = 1 AND ISNULL(Item.bitAsset,0) = 1 AND ISNULL(Item.bitRental,0)=0 THEN ''Serialized Asset''
																			WHEN ISNULL(Item.bitSerialized,0) = 1 AND ISNULL(Item.bitAsset,0) = 1 AND ISNULL(Item.bitRental,0)=1 THEN ''Serialized Rental Asset''
																			WHEN ISNULL(Item.bitLotNo,0)=1 THEN ''Lot #''
																			ELSE ''Inventory Item'' 
																		END
																	WHEN Item.charItemType=''N'' THEN ''Non Inventory Item'' 
																	WHEN Item.charItemType=''S'' THEN ''Service'' 
																	WHEN Item.charItemType=''A'' THEN ''Accessory'' 
																END)',' [',@vcDbColumnName,']') 
					END
					ELSE IF @vcDbColumnName = 'vcItemReleaseDate'
					BEGIN
						SET @vcSelect = CONCAT(@vcSelect,',dbo.FormatedDateFromDate(CAST(OpportunityItems.ItemReleaseDate AS DATETIME),@numDomainID) [',@vcDbColumnName,']')
					END
					ELSE IF @vcDbColumnName = 'numStatus'
					BEGIN
						SET @vcSelect = CONCAT(@vcSelect,',dbo.GetListIemName(',@vcLookBackTableName,'.',@vcDbColumnName,') [',@vcDbColumnName,']')
					END
					ELSE IF @vcDbColumnName = 'tintSource'
					BEGIN
						SET @vcSelect = CONCAT(@vcSelect,',dbo.fn_GetOpportunitySourceValue(ISNULL(OpportunityMaster.tintSource,0),ISNULL(OpportunityMaster.tintSourceType,0),OpportunityMaster.numDomainID) [',@vcDbColumnName,']')
					END
					ELSE IF @vcDbColumnName = 'vcWareHouse'
					BEGIN
						SET @vcSelect = CONCAT(@vcSelect,',(CASE WHEN ISNULL(OpportunityItems.bitRequiredWarehouseCorrection,0) = 1 THEN '''' ELSE ISNULL(Warehouses.vcWarehouse,''-'') END) [',@vcDbColumnName,']')
					END
					ELSE IF @vcDbColumnName = 'intUsedShippingCompany'
					BEGIN
						SET @vcSelect = CONCAT(@vcSelect,',CONCAT(ISNULL(LDOpp.vcData,''-''),'', '',ISNULL(SSOpp.vcShipmentService,''-''))  [',@vcDbColumnName,']')

						SET @vcGroupBy = (CASE 
											WHEN @bitGroupByOrder = 1 
											THEN CONCAT(@vcGroupBy,',LDOpp.vcData,SSOpp.vcShipmentService')
											ELSE '' 
										END)

						SET @vcFrom = CONCAT(@vcFrom ,' LEFT JOIN
															ListDetails  LDOpp
														ON
															LDOpp.numListID = 82
															AND (LDOpp.numDomainID = @numDomainID OR LDOpp.constFlag=1)
															AND ISNULL(OpportunityMaster.intUsedShippingCompany,0) = LDOpp.numListItemID
														LEFT JOIN
															ShippingService AS SSOpp
														ON
															(SSOpp.numDomainID=@numDomainID OR ISNULL(SSOpp.numDomainID,0)=0) 
															AND OpportunityMaster.numShippingService = SSOpp.numShippingServiceID')
						SET @vcFrom1 = CONCAT(@vcFrom1 ,' LEFT JOIN
															ListDetails  LDOpp
														ON
															LDOpp.numListID = 82
															AND (LDOpp.numDomainID = @numDomainID OR LDOpp.constFlag=1)
															AND ISNULL(OpportunityMaster.intUsedShippingCompany,0) = LDOpp.numListItemID
														LEFT JOIN
															ShippingService AS SSOpp
														ON
															(SSOpp.numDomainID=@numDomainID OR ISNULL(SSOpp.numDomainID,0)=0) 
															AND OpportunityMaster.numShippingService = SSOpp.numShippingServiceID')
					END
					ELSE IF @vcDbColumnName = 'intShippingCompany'
					BEGIN
						SET @vcSelect = CONCAT(@vcSelect,',CONCAT(ISNULL(LDDiv.vcData,''-''),'', '',ISNULL(SSDiv.vcShipmentService,''-''))  [',@vcDbColumnName,']')

						SET @vcGroupBy = (CASE 
											WHEN @bitGroupByOrder = 1 
											THEN CONCAT(@vcGroupBy,',LDDiv.vcData,SSDiv.vcShipmentService')
											ELSE '' 
										END)

						SET @vcFrom = CONCAT(@vcFrom ,' LEFT JOIN
															ListDetails  LDDiv
														ON
															LDDiv.numListID = 82
															AND (LDDiv.numDomainID = @numDomainID OR LDDiv.constFlag=1)
															AND ISNULL(DivisionMaster.intShippingCompany,0) = LDDiv.numListItemID
														LEFT JOIN
															ShippingService AS SSDiv
														ON
															(SSDiv.numDomainID=@numDomainID OR ISNULL(SSDiv.numDomainID,0)=0) 
															AND ISNULL(DivisionMaster.numDefaultShippingServiceID,0) = SSDiv.numShippingServiceID')

						SET @vcFrom1 = CONCAT(@vcFrom1 ,' LEFT JOIN
															ListDetails  LDDiv
														ON
															LDDiv.numListID = 82
															AND (LDDiv.numDomainID = @numDomainID OR LDDiv.constFlag=1)
															AND ISNULL(DivisionMaster.intShippingCompany,0) = LDDiv.numListItemID
														LEFT JOIN
															ShippingService AS SSDiv
														ON
															(SSDiv.numDomainID=@numDomainID OR ISNULL(SSDiv.numDomainID,0)=0) 
															AND ISNULL(DivisionMaster.numDefaultShippingServiceID,0) = SSDiv.numShippingServiceID')
					END
					ELSE IF @vcAssociatedControlType = 'SelectBox'
					BEGIN
						IF @vcListItemType = 'LI'
						BEGIN
							SET @vcSelect = CONCAT(@vcSelect,',L',CONVERT(VARCHAR(3),@j),'.vcData',' [',@vcDbColumnName,']')

							SET @vcFrom = CONCAT(@vcFrom ,' LEFT JOIN ListDetails L',@j,' ON L',@j,'.numListItemID=',@vcLookBackTableName,'.',@vcDbColumnName)
							SET @vcFrom1 = CONCAT(@vcFrom1 ,' LEFT JOIN ListDetails L',@j,' ON L',@j,'.numListItemID=',@vcLookBackTableName,'.',@vcDbColumnName)
						END
						ELSE IF @vcListItemType = 'IG'
						BEGIN
							SET @vcSelect = CONCAT(@vcSelect,',ISNULL((SELECT vcItemGroup FROM ItemGroups WHERE numItemGroupID = I.numItemGroup),'''')',' [',@vcDbColumnName,']') 
						END
						ELSE IF @vcListItemType = 'U'
						BEGIN
							SET @vcSelect = CONCAT(@vcSelect,',dbo.fn_GetContactName(',@vcLookBackTableName,'.',@vcDbColumnName,') [',@vcDbColumnName,']')
						END
						ELSE
						BEGIN
							SET @vcSelect = CONCAT(@vcSelect,',',(CASE WHEN @bitIsNumeric = 1 THEN '0' ELSE '''''' END),' [',@vcDbColumnName,']')
						END
					END
					ELSE IF @vcAssociatedControlType = 'DateField'
					BEGIN
						SET @vcSelect = CONCAT(@vcSelect,',dbo.FormatedDateFromDate(DATEADD(MINUTE,',-@ClientTimeZoneOffset,',',@vcLookBackTableName,'.',@vcDbColumnName,'),',@numDomainId,') [', @vcDbColumnName,']')
					END
					ELSE IF @vcAssociatedControlType = 'TextBox'
					BEGIN
						SET @vcSelect = CONCAT(@vcSelect,',ISNULL(',@vcLookBackTableName,'.',@vcDbColumnName,(CASE WHEN @bitIsNumeric = 1 THEN ',0' ELSE ',''''' END),') [',@vcDbColumnName,']')
					END
					ELSE IF @vcAssociatedControlType = 'TextArea'
					BEGIN
						SET @vcSelect = CONCAT(@vcSelect,',ISNULL(',@vcLookBackTableName,'.',@vcDbColumnName,(CASE WHEN @bitIsNumeric = 1 THEN ',0' ELSE ',''''' END),') [',@vcDbColumnName,']')
					END
					ELSE IF @vcAssociatedControlType = 'Label'
					BEGIN
						SET @vcSelect = CONCAT(@vcSelect,',ISNULL(',@vcLookBackTableName,'.',@vcDbColumnName,(CASE WHEN @bitIsNumeric = 1 THEN ',0' ELSE '''''' END),') [',@vcDbColumnName,']')
					END
					ELSE
					BEGIN
						SET @vcSelect = CONCAT(@vcSelect,',ISNULL(',@vcLookBackTableName,'.',@vcDbColumnName,(CASE WHEN @bitIsNumeric = 1 THEN ',0' ELSE '''''' END),') [',@vcDbColumnName,']')
					END
				END
			END
			ELSE IF @bitCustomField = 1
			BEGIN
				IF @Grp_id = 5
				BEGIN
					IF ISNULL(@bitGroupByOrder,0) = 1
					BEGIN
						SET @vcSelect = CONCAT(@vcSelect,',''''',' [',@vcDbColumnName,']')
					END
					ELSE
					BEGIN
						SET @vcSelect = CONCAT(@vcSelect,CONCAT(',dbo.GetCustFldValueOppItems(',@numFieldId,',OpportunityItems.numoppitemtCode,Item.numItemCode)'),' [',@vcDbColumnName,']')
					END
				END
				ELSE
				BEGIN
					IF @vcAssociatedControlType = 'TextBox' OR @vcAssociatedControlType = 'TextArea' 
					BEGIN
						SET @vcSelect = CONCAT(@vcSelect,',CFW',@numFieldId,'.Fld_Value  [',@vcDbColumnName,']')
             
						SET @vcText = CONCAT(' left Join CFW_FLD_Values_Opp CFW',@numFieldId
										,' ON CFW' , @numFieldId , '.Fld_Id='
										,@numFieldId
										, ' and CFW' , @numFieldId
										, '.RecId=OpportunityMaster.numOppID')

						SET @vcGroupBy = (CASE 
											WHEN @bitGroupByOrder = 1 
											THEN CONCAT(@vcGroupBy,',CFW',@numFieldId,'.Fld_Value')
											ELSE '' 
										END)
						SET @vcFrom = CONCAT(@vcFrom,@vcText)
						SET @vcFrom1 = CONCAT(@vcFrom1,@vcText)
					END   
					ELSE IF @vcAssociatedControlType = 'CheckBox' 
					BEGIN
						SET @vcSelect =CONCAT( @vcSelect
							, ',case when isnull(CFW'
							, @numFieldId
							, '.Fld_Value,0)=0 then 0 when isnull(CFW'
							, @numFieldId
							, '.Fld_Value,0)=1 then 1 end   ['
							,  @vcDbColumnName
							, ']')
							
						SET @vcText = CONCAT(' left Join CFW_FLD_Values_Opp CFW'
							, @numFieldId
							, ' ON CFW',@numFieldId,'.Fld_Id='
							, @numFieldId
							, ' and CFW'
							, @numFieldId
							, '.RecId=OpportunityMaster.numOppID')

						SET @vcGroupBy = (CASE 
											WHEN @bitGroupByOrder = 1 
											THEN CONCAT(@vcGroupBy,',CFW',@numFieldId,'.Fld_Value')
											ELSE '' 
										END)
						SET @vcFrom = CONCAT(@vcFrom,@vcText)
						SET @vcFrom1 = CONCAT(@vcFrom1,@vcText)                                                 
					END                
					ELSE IF @vcAssociatedControlType = 'DateField' 
					BEGIN
						SET @vcSelect = CONCAT(@vcSelect
							, ',dbo.FormatedDateFromDate(CFW'
							, @numFieldId
							, '.Fld_Value,'
							, @numDomainId
							, ')  [',@vcDbColumnName ,']' )  
					                  
						SET @vcText = CONCAT(' left Join CFW_FLD_Values_Opp CFW'
							, @numFieldId
							, ' on CFW', @numFieldId, '.Fld_Id='
							, @numFieldId
							, ' and CFW'
							, @numFieldId
							, '.RecId=OpportunityMaster.numOppID')

						SET @vcGroupBy = (CASE 
										WHEN @bitGroupByOrder = 1 
										THEN CONCAT(@vcGroupBy,',CFW',@numFieldId,'.Fld_Value')
										ELSE '' 
									END)
						SET @vcFrom = CONCAT(@vcFrom,@vcText)
						SET @vcFrom1 = CONCAT(@vcFrom1,@vcText)                                                 
					END                
					ELSE IF @vcAssociatedControlType = 'SelectBox' 
					BEGIN
						SET @vcSelect = CONCAT(@vcSelect,',L',@numFieldId,'.vcData',' [',@vcDbColumnName,']')
					        
						SET @vcText = CONCAT(' left Join CFW_FLD_Values_Opp CFW'
							, @numFieldId
							, ' on CFW',@numFieldId ,'.Fld_Id='
							, @numFieldId
							, ' and CFW'
							, @numFieldId
							, '.RecId=OpportunityMaster.numOppID')

						SET @vcGroupBy = (CASE 
											WHEN @bitGroupByOrder = 1 
											THEN CONCAT(@vcGroupBy,',L',@numFieldId,'.vcData')
											ELSE '' 
										END)
						SET @vcFrom = CONCAT(@vcFrom,@vcText)
						SET @vcFrom1 = CONCAT(@vcFrom1,@vcText)

						SET @vcText = CONCAT(' left Join ListDetails L'
							, @numFieldId
							, ' on L'
							, @numFieldId
							, '.numListItemID=CFW'
							, @numFieldId
							, '.Fld_Value')

						SET @vcFrom = CONCAT(@vcFrom,@vcText)
						SET @vcFrom1 = CONCAT(@vcFrom1,@vcText)            
					END
					ELSE
					BEGIN
						SET @vcSelect = CONCAT(@vcSelect,CONCAT(',dbo.GetCustFldValueOpp(',@numFieldId,',OpportunityMaster.numOppID)'),' [',@vcDbColumnName,']')
					END 
				END
			END
		END

		SET @j = @j + 1
	END

	PRINT CAST(@vcSelect AS NTEXT)
	PRINT CAST(@vcFrom AS NTEXT)
	PRINT CAST(@vcWhere AS NTEXT)
	PRINT CAST(@vcGroupBy AS NTEXT)
	PRINT CAST(@vcOrderBy AS NTEXT)

	DECLARE @vcFinal NVARCHAR(MAX) = ''

	If @numViewID = 2 AND @tintPackingViewMode = 3
	BEGIN
		SET @vcSelect = CONCAT(@vcSelect,',',(CASE @vcSortColumn
													WHEN 'CompanyInfo.vcCompanyName' THEN 'CompanyInfo.vcCompanyName AS vcCompanyNameOrig' 
													WHEN 'Item.charItemType' THEN 'Item.charItemType AS charItemTypeOrig'
													WHEN 'Item.numBarCodeId' THEN 'Item.numBarCodeId AS numBarCodeIdOrig'
													WHEN 'Item.numItemClassification' THEN 'Item.numItemClassification AS numItemClassificationOrig'
													WHEN 'Item.numItemGroup' THEN 'Item.numItemGroup AS numItemGroupOrig'
													WHEN 'Item.vcSKU' THEN 'Item.vcSKU AS vcSKUOrig'
													WHEN 'OpportunityItems.vcItemName' THEN 'Item.vcItemName AS vcItemNameOrig'
													WHEN 'OpportunityMaster.numStatus' THEN 'OpportunityMaster.numStatus AS numStatusOrig '
													WHEN 'OpportunityMaster.vcPoppName' THEN 'OpportunityMaster.vcPoppName AS vcPoppNameOrig'
													WHEN 'OpportunityMaster.numAssignedTo' THEN 'OpportunityMaster.numAssignedTo AS numAssignedToOrig'
													WHEN 'OpportunityMaster.numRecOwner' THEN 'OpportunityMaster.numRecOwner AS numRecOwnerOrig'
													WHEN 'WareHouseItems.vcLocation' THEN 'WarehouseLocation.vcLocation AS vcLocationOrig'
													WHEN 'OpportunityBizDocs.monAmountPaid' THEN 'TempPaid.monAmountPaid AS monAmountPaidOrig'
													WHEN 'OpportunityItems.numQtyPicked' THEN 'OpportunityItems.numQtyPicked AS numQtyPickedOrig'
													WHEN 'OpportunityItems.numQtyShipped' THEN 'OpportunityItems.numQtyShipped AS numQtyShippedOrig'
													WHEN 'OpportunityItems.numRemainingQty' THEN 'OpportunityItems.numUnitHour AS numUnitHourOrig,OpportunityItems.numQtyPicked AS numQtyPickedOrig'
													WHEN 'OpportunityItems.numUnitHour' THEN 'OpportunityItems.numUnitHour AS numUnitHourOrig'
													WHEN 'OpportunityMaster.dtExpectedDate' THEN 'OpportunityMaster.dtExpectedDate AS dtExpectedDateOrig'
													WHEN 'OpportunityItems.vcItemReleaseDate' THEN 'OpportunityItems.ItemReleaseDate AS ItemReleaseDateOrig'
													WHEN 'OpportunityMaster.bintCreatedDate' THEN 'OpportunityMaster.bintCreatedDate AS bintCreatedDateOrig'
													WHEN 'OpportunityMaster.numAge' THEN 'OpportunityMaster.bintCreatedDate AS bintCreatedDateOrig'
													WHEN 'OpportunityMaster.bitPaidInFull' THEN 'TempPaid.monAmountPaid AS monAmountPaidOrig, OpportunityMaster.monDealAmount AS monDealAmountOrig'
													WHEN 'Item.dtItemReceivedDate' THEN 'Item.dtItemReceivedDate AS dtItemReceivedDateOrig'
													WHEN 'Warehouses.vcWareHouse' THEN '(CASE WHEN ISNULL(OpportunityItems.bitRequiredWarehouseCorrection,0) = 1 THEN '''' ELSE Warehouses.vcWareHouse END)'
													ELSE 'OpportunityMaster.bintCreatedDate AS bintCreatedDateOrig'
												END))

		DECLARE @vcGroupBy1 NVARCHAR(MAX) = (CASE 
											WHEN @bitGroupByOrder = 1 
											THEN CONCAT('GROUP BY TEMP.numOppId
																	,TEMP.numDivisionID
																	,TEMP.numTerID
																	,TEMP.intShippingCompany
																	,TEMP.numDefaultShippingServiceID
																	,TEMP.numOppBizDocsId
																	,TEMP.monDealAmount
																	,TEMP.monAmountPaid
																	,',(CASE WHEN EXISTS (SELECT ID FROM @TempFieldsLeft WHERE vcLookBackTableName='CompanyInfo' AND vcOrigDbColumnName='vcCompanyName') THEN 'TEMP.vcCompanyName' ELSE '' END),'
																	,',(CASE WHEN EXISTS (SELECT ID FROM @TempFieldsLeft WHERE vcLookBackTableName='OpportunityMaster' AND vcOrigDbColumnName='vcPoppName') THEN 'TEMP.vcPoppName' ELSE '' END),'
																	,',(CASE WHEN EXISTS (SELECT ID FROM @TempFieldsLeft WHERE vcLookBackTableName='OpportunityMaster' AND vcOrigDbColumnName='numAssignedTo') THEN 'TEMP.numAssignedTo' ELSE '' END),'.
																	,',(CASE WHEN EXISTS (SELECT ID FROM @TempFieldsLeft WHERE vcLookBackTableName='OpportunityMaster' AND vcOrigDbColumnName='numRecOwner') THEN 'TEMP.numRecOwner' ELSE '' END),'.
																	,',(CASE WHEN EXISTS (SELECT ID FROM @TempFieldsLeft WHERE vcLookBackTableName='OpportunityMaster' AND vcOrigDbColumnName='numStatus') THEN 'TEMP.numStatus' ELSE '' END),'.
																	,',(CASE WHEN EXISTS (SELECT ID FROM @TempFieldsLeft WHERE vcLookBackTableName='OpportunityMaster' AND vcOrigDbColumnName='bintCreatedDate') THEN 'TEMP.bintCreatedDate' ELSE '' END),'.
																	,',(CASE WHEN EXISTS (SELECT ID FROM @TempFieldsLeft WHERE vcLookBackTableName='OpportunityMaster' AND vcOrigDbColumnName='txtComments') THEN 'TEMP.txtComments' ELSE '' END),'.
																	,',(CASE WHEN EXISTS (SELECT ID FROM @TempFieldsLeft WHERE vcLookBackTableName='OpportunityBizDocs' AND vcOrigDbColumnName='vcBizDocID') THEN 'TEMP.vcBizDocID' ELSE '' END),'.
																	,',(CASE WHEN EXISTS (SELECT ID FROM @TempFieldsLeft WHERE vcLookBackTableName='OpportunityMaster' AND vcOrigDbColumnName='intUsedShippingCompany') THEN 'TEMP.intUsedShippingCompany' ELSE '' END),'.
																	,',(CASE WHEN EXISTS (SELECT ID FROM @TempFieldsLeft WHERE vcLookBackTableName='DivisionMaster' AND vcOrigDbColumnName='intShippingCompany') THEN 'TEMP.intShippingCompany' ELSE '' END),'.
																	,',(CASE WHEN EXISTS (SELECT ID FROM @TempFieldsLeft WHERE vcLookBackTableName='OpportunityBizDocs' AND vcOrigDbColumnName='dtCreatedDate') THEN 'TEMP.dtCreatedDate' ELSE '' END),'.
																	,',(CASE WHEN EXISTS (SELECT ID FROM @TempFieldsLeft WHERE vcLookBackTableName='OpportunityMaster' AND vcOrigDbColumnName='dtExpectedDate') THEN 'TEMP.dtExpectedDate' ELSE '' END),'.
																	,',(CASE WHEN EXISTS (SELECT ID FROM @TempFieldsLeft WHERE vcLookBackTableName='Item' AND vcOrigDbColumnName='dtItemReceivedDate') THEN 'TEMP.dtItemReceivedDateOrig' ELSE '' END),'.')
											ELSE '' 
										END)
		SET @vcFinal= CONCAT('SELECT COUNT(*) OVER() AS TotalRecords,* FROM (',@vcSelect,@vcFrom,@vcWhere,' UNION ',@vcSelect,@vcFrom1,@vcWhere1,') TEMP',@vcGroupBy1,@vcOrderBy1,' OFFSET (@numPageIndex-1) * 200 ROWS FETCH NEXT 200 ROWS ONLY OPTION (OPTIMIZE FOR (@numDomainID UNKNOWN))')
	END
	ELSE
	BEGIN
		SET @vcFinal = CONCAT(@vcSelect,',COUNT(*) OVER() AS TotalRecords',@vcFrom,@vcWhere,@vcGroupBy,@vcOrderBy,' OFFSET (@numPageIndex-1) * 200 ROWS FETCH NEXT 200 ROWS ONLY OPTION (OPTIMIZE FOR (@numDomainID UNKNOWN))')
	END

	EXEC sp_executesql @vcFinal, N'@numDomainID NUMERIC(18,0), @vcOrderStauts VARCHAR(MAX), @vcFilterValue VARCHAR(MAX), @tintFilterBy TINYINT, @numViewID NUMERIC(18,0), @tintPackingViewMode TINYINT, @numShippingServiceItemID NUMERIC(18,0), @numWarehouseID NUMERIC(18,0), @numShippingZone NUMERIC(18,0), @numDefaultSalesShippingDoc NUMERIC(18,0), @tintInvoicingType TINYINT, @tintPackingMode TINYINT, @bitRemoveBO BIT, @ClientTimeZoneOffset INT,@numBatchID NUMERIC(18,0),@tintPrintBizDocViewMode TINYINT,@tintPendingCloseFilter TINYINT, @bitIncludeSearch BIT, @bitEnablePickListMapping BIT, @bitEnableFulfillmentBizDocMapping BIT,@numPageIndex INT', @numDomainID, @vcOrderStauts, @vcFilterValue, @tintFilterBy, @numViewID, @tintPackingViewMode, @numShippingServiceItemID, @numWarehouseID, @numShippingZone, @numDefaultSalesShippingDoc, @tintInvoicingType, @tintPackingMode, @bitRemoveBO, @ClientTimeZoneOffset,@numBatchID,@tintPrintBizDocViewMode,@tintPendingCloseFilter,@bitIncludeSearch,@bitEnablePickListMapping, @bitEnableFulfillmentBizDocMapping,@numPageIndex;

	SELECT * FROM @TempFieldsLeft ORDER BY intRowNum
END
GO
/****** Object:  StoredProcedure [dbo].[USP_OPPBizDocItems]    Script Date: 07/26/2008 16:20:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                                                                                                                                                        
-- exec USP_OPPBizDocItems @numOppId=12388,@numOppBizDocsId=11988,@numDomainID=110
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'usp_oppbizdocitems' ) 
    DROP PROCEDURE usp_oppbizdocitems
GO
CREATE PROCEDURE [dbo].[USP_OPPBizDocItems]
    (
      @numOppId AS NUMERIC(9) = NULL,
      @numOppBizDocsId AS NUMERIC(9) = NULL,
      @numDomainID AS NUMERIC(9) = 0      
    )
AS 
    DECLARE @DivisionID AS NUMERIC(9)      
    DECLARE @tintTaxOperator AS TINYINT   
                                                                                                                                                                                                                                                                                                                                          
    SELECT  @DivisionID = numDivisionID, @tintTaxOperator = [tintTaxOperator]           
    FROM    OpportunityMaster
    WHERE   numOppId = @numOppId                                                                                                                                                                          
--Decimal points to show in bizdoc 
    DECLARE @DecimalPoint AS TINYINT
    SELECT  @DecimalPoint = tintDecimalPoints
    FROM    domain
    WHERE   numDomainID = @numDomainID
                                                                                          
    DECLARE @strSQL AS VARCHAR(MAX)                                                                                          
    DECLARE @strSQL1 AS VARCHAR(8000)                                       
    DECLARE @strSQLCusFields VARCHAR(1000)                      
    DECLARE @strSQLEmpFlds VARCHAR(500)                                                                         
    SET @strSQLCusFields = ''                                                                                          
    SET @strSQLEmpFlds = ''                                                                                          
    DECLARE @tintType AS TINYINT   
    DECLARE @tintOppType AS TINYINT                                                      
    DECLARE @intRowNum AS INT                                                  
    DECLARE @numFldID AS VARCHAR(15)                                          
    DECLARE @vcFldname AS VARCHAR(50)                                            
    DECLARE @numBizDocId AS NUMERIC(9)
    DECLARE @numBizDocTempID AS NUMERIC(9) ;
        SET @numBizDocTempID = 0
                                           
    SELECT  @numBizDocTempID = ISNULL(numBizDocTempID, 0),
            @numBizDocId = numBizDocId
    FROM    OpportunityBizDocs
    WHERE   numOppBizDocsId = @numOppBizDocsId
    SELECT  @tintType = tintOppType,
            @tintOppType = tintOppType
    FROM    OpportunityMaster
    WHERE   numOppID = @numOppId                                                                                          
                                                                                   
    IF @tintType = 1 
        SET @tintType = 7
    ELSE 
        SET @tintType = 8                                                               
                                                                                       

    SELECT  *
    INTO    #Temp1
    FROM    ( SELECT   Opp.vcitemname AS vcItemName,Opp.numCost,Opp.vcNotes as txtNotes,
                        ISNULL(OBD.numOppBizDocItemID, 0) OppBizDocItemID,
                        CASE WHEN charitemType = 'P' THEN 'Product'
                             WHEN charitemType = 'S' THEN 'Service'
                        END AS charitemType,
                        CASE WHEN charitemType = 'P' THEN 'Inventory'
	                         WHEN charitemType = 'N' THEN 'Non-Inventory'
                             WHEN charitemType = 'S' THEN 'Service'
                        END AS vcItemType,
                        CASE WHEN bitShowDeptItemDesc = 0
                             THEN CONVERT(VARCHAR(2500), OBD.vcitemDesc)
                             WHEN bitShowDeptItemDesc = 1
                                  AND bitKitParent = 1
                                  AND bitAssembly = 0
                             THEN dbo.fn_PopulateKitDesc(opp.numOppId,
                                                         opp.numoppitemtCode)
                             WHEN bitShowDeptItemDesc = 1
                                  AND bitKitParent = 1
                                  AND bitAssembly = 1
                             THEN dbo.fn_PopulateAssemblyDesc(opp.numoppitemtCode, OBD.numUnitHour)
                             ELSE CONVERT(VARCHAR(2500), OBD.vcitemDesc)
                        END AS txtItemDesc, 
                        dbo.fn_UOMConversion(ISNULL(i.numBaseUnit, 0),
                                             i.numItemCode, @numDomainID,
                                             ISNULL(opp.numUOMId, 0))
                        * OBD.numUnitHour AS numUnitHour,
                        CONVERT(DECIMAL(20,5), (dbo.fn_UOMConversion(ISNULL(opp.numUOMId, 0), i.numItemCode, @numDomainID, ISNULL(i.numBaseUnit, 0)) * OBD.monPrice)) monPrice,
                        CONVERT(VARCHAR, CONVERT(DECIMAL(20,5), OBD.monTotAmount)) Amount,
                        OBD.monTotAmount/*Fo calculating sum*/,
                        ISNULL(CONVERT(VARCHAR, i.monListPrice), 0) AS monListPrice,
                        i.numItemCode AS ItemCode,
                        opp.numoppitemtCode,
                        L.vcdata AS numItemClassification,
                        CASE WHEN bitTaxable = 0 THEN 'No'
                             ELSE 'Yes'
                        END AS Taxable,
                        numIncomeChartAcntId AS itemIncomeAccount,
                        'NI' AS ItemType,
                        ISNULL(GJH.numJournal_Id, 0) AS numJournalId,
                        ( SELECT TOP 1
                                    ISNULL(GJD.numTransactionId, 0)
                          FROM      General_Journal_Details GJD
                          WHERE     GJH.numJournal_Id = GJD.numJournalId
                                    AND GJD.chBizDocItems = 'NI'
                                    AND GJD.numoppitemtCode = opp.numoppitemtCode
                        ) AS numTransactionId,
                        ISNULL(Opp.vcModelID, '') vcModelID,
                        (CASE WHEN charitemType = 'P' THEN dbo.USP_GetAttributes(OBD.numWarehouseItmsID,bitSerialized) ELSE ISNULL(OBD.vcAttributes,0) END) AS vcAttributes,
                        ISNULL(vcPartNo, '') AS vcPartNo,
                        ( ISNULL(OBD.monTotAmtBefDiscount, OBD.monTotAmount)
                          - OBD.monTotAmount ) AS DiscAmt,
						(CASE WHEN OBD.numUnitHour = 0 THEN 0 ELSE CONVERT(DECIMAL(20,5),ISNULL(OBD.monTotAmount,0) / OBD.numUnitHour) END) As monUnitSalePrice,
                        OBD.vcNotes,
                        OBD.vcTrackingNo,
                        OBD.vcShippingMethod,
                        OBD.monShipCost,
                        [dbo].[FormatedDateFromDate](OBD.dtDeliveryDate,
                                                     @numDomainID) dtDeliveryDate,
                        Opp.numWarehouseItmsID,
                        ISNULL(u.vcUnitName, '') numUOMId,
                        ISNULL(Opp.vcManufacturer, '') AS vcManufacturer,
                        ISNULL(V.monCost, 0) AS VendorCost,
                        Opp.vcPathForTImage,
                        i.[fltLength],
                        i.[fltWidth],
                        i.[fltHeight],
                        (CASE 
							WHEN ISNULL(bitKitParent,0)=1 
									AND (CASE 
											WHEN EXISTS (SELECT ID.numItemDetailID FROM ItemDetails ID INNER JOIN Item IInner ON ID.numChildItemID=IInner.numItemCode WHERE ID.numItemKitID=I.numItemCode AND ISNULL(IInner.bitKitParent,0)=1)
											THEN 1
											ELSE 0
										END) = 0
							THEN 
								dbo.GetKitWeightBasedOnItemSelection(I.numItemCode,opp.vcChildKitSelectedItems)
							ELSE 
								ISNULL(fltWeight,0) 
						END) [fltWeight],
                        ISNULL(I.numVendorID, 0) numVendorID,
                        Opp.bitDropShip AS DropShip,
                        SUBSTRING(( SELECT  ' ,' + vcSerialNo
                                            + CASE WHEN ISNULL(I.bitLotNo, 0) = 1
                                                   THEN ' ('
                                                        + CONVERT(VARCHAR(15), oppI.numQty)
                                                        + ')'
                                                   ELSE ''
                                              END
                                    FROM    OppWarehouseSerializedItem oppI
                                            JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID = whi.numWareHouseItmsDTLID
                                    WHERE   oppI.numOppID = Opp.numOppId
                                            AND oppI.numOppItemID = Opp.numoppitemtCode
                                    ORDER BY vcSerialNo
                                  FOR
                                    XML PATH('')
                                  ), 3, 200000) AS SerialLotNo,
                        ISNULL(opp.numUOMId, 0) AS numUOM,
                        ISNULL(u.vcUnitName, '') vcUOMName,
                        dbo.fn_UOMConversion(opp.numUOMId, Opp.numItemCode,
                                             @numDomainID, NULL) AS UOMConversionFactor,
                        ISNULL(Opp.numSOVendorId, 0) AS numSOVendorId,
                        [dbo].FormatedDateTimeFromDate(OBD.dtRentalStartDate,
                                                       @numDomainID) dtRentalStartDate,
                        [dbo].FormatedDateTimeFromDate(OBD.dtRentalReturnDate,
                                                       @numDomainID) dtRentalReturnDate,
                        ISNULL(W.vcWareHouse, '') AS Warehouse,
						(CASE WHEN ISNULL(WI.numWareHouseItemID,0) > 0 THEN CONCAT(W.vcWareHouse,CASE WHEN WL.numWLocationID > 0 THEN CONCAT('(',WL.vcLocation ,')') ELSE '' END) ELSE '' END) vcWarehouse,
					--	ISNULL(W.vcWareHouse, '') AS vcWareHouse,
                        CASE WHEN charitemType = 'P' THEN WI.vcWHSKU
                             ELSE i.vcSKU
                        END vcSKU,
                        CASE WHEN charitemType = 'P' THEN WI.vcBarCode
                             ELSE i.numBarCodeId
                        END vcBarcode,
                        ISNULL(opp.numClassID, 0) AS numClassID,
                        ISNULL(opp.numProjectID, 0) AS numProjectID,
                        ISNULL(I.[numShipClass],0) AS numShipClass,OBD.numOppBizDocID,WL.vcLocation,ISNULL(bitSerialized,0) AS bitSerialized,ISNULL(bitLotNo,0) AS bitLotNo,
                        opp.numOppId,OBD.numUnitHour AS numUnitHourOrig,ISNULL(I.numItemCode,0) AS [numItemCode], 
						ISNULL(opp.vcPromotionDetail, '') AS vcPromotionDetail

						, CASE WHEN ISNULL(opp.ItemReleaseDate,'') = '' THEN
							 ISNULL(OM.dtReleaseDate,'') 
						ELSE opp.ItemReleaseDate 
						END AS ItemRequiredDate

              FROM      OpportunityItems opp
                        JOIN OpportunityBizDocItems OBD ON OBD.numOppItemID = opp.numoppitemtCode
                        LEFT JOIN item i ON opp.numItemCode = i.numItemCode
                        LEFT JOIN ListDetails L ON i.numItemClassification = L.numListItemID
                        LEFT JOIN Vendor V ON V.numVendorID = @DivisionID
                                              AND V.numItemCode = opp.numItemCode
                        LEFT JOIN General_Journal_Header GJH ON opp.numoppid = GJH.numOppId
                                                                AND GJH.numBizDocsPaymentDetId IS NULL
                                                                AND GJH.numOppBizDocsId = @numOppBizDocsId
                        LEFT JOIN UOM u ON u.numUOMId = opp.numUOMId                                                                       
                        LEFT JOIN dbo.WareHouseItems WI ON opp.numWarehouseItmsID = WI.numWareHouseItemID
                                                           AND WI.numDomainID = @numDomainID
                        LEFT JOIN dbo.Warehouses W ON WI.numDomainID = W.numDomainID
                                                      AND WI.numWareHouseID = W.numWareHouseID
                        LEFT JOIN dbo.WarehouseLocation WL ON WL.numWLocationID = WI.numWLocationID
						LEFT JOIN OpportunityMaster OM ON opp.numOppId = OM.numOppId
              WHERE     opp.numOppId = @numOppId
                        AND ( bitShowDeptItem IS NULL
                              OR bitShowDeptItem = 0
                            )
                        AND OBD.numOppBizDocID = @numOppBizDocsId
            ) X


    --IF @DecimalPoint = 1 
    --    BEGIN
    --        UPDATE  #Temp1
    --        SET     monPrice = CONVERT(DECIMAL(18, 1), monPrice),
    --                Amount = CONVERT(DECIMAL(18, 1), Amount)
    --    END
    --IF @DecimalPoint = 2 
    --    BEGIN
    --        UPDATE  #Temp1
    --        SET     monPrice = CONVERT(DECIMAL(18, 2), monPrice),
    --                Amount = CONVERT(DECIMAL(18, 2), Amount)
    --    END
    --IF @DecimalPoint = 3 
    --    BEGIN
    --        UPDATE  #Temp1
    --        SET     monPrice = CONVERT(DECIMAL(18, 3), monPrice),
    --                Amount = CONVERT(DECIMAL(18, 3), Amount)
    --    END
    --IF @DecimalPoint = 4 
    --    BEGIN
    --        UPDATE  #Temp1
    --        SET     monPrice = CONVERT(DECIMAL(20,5), monPrice),
    --                Amount = CONVERT(DECIMAL(20,5), Amount)
    --    END

    DECLARE @strSQLUpdate AS VARCHAR(2000);SET @strSQLUpdate=''
   
    EXEC ('ALTER TABLE #Temp1 ADD [TotalTax] DECIMAL(20,5)')
	EXEC ('ALTER TABLE #Temp1 ADD [CRV] DECIMAL(20,5)')

    IF @tintTaxOperator = 1 --add Sales tax
	BEGIN
        SET @strSQLUpdate = @strSQLUpdate
            + ',[TotalTax]= dbo.fn_CalBizDocTaxAmt('
            + CONVERT(VARCHAR(20), @numDomainID) + ',0,'
            + CONVERT(VARCHAR(20), @numOppId) +','+
            + 'numoppitemtCode,1,Amount,numUnitHour)'

		SET @strSQLUpdate = @strSQLUpdate
            + ',[CRV]= dbo.fn_CalBizDocTaxAmt('
            + CONVERT(VARCHAR(20), @numDomainID) + ',1,'
            + CONVERT(VARCHAR(20), @numOppId) +','+
            + 'numoppitemtCode,1,Amount,numUnitHour)'
	END
    ELSE IF @tintTaxOperator = 2 -- remove sales tax
	BEGIN
        SET @strSQLUpdate = @strSQLUpdate + ',[TotalTax]= 0'
		SET @strSQLUpdate = @strSQLUpdate + ',[CRV]= 0'
	END
    ELSE 
	BEGIN
        SET @strSQLUpdate = @strSQLUpdate
            + ',[TotalTax]= dbo.fn_CalBizDocTaxAmt('
            + CONVERT(VARCHAR(20), @numDomainID) + ',0,'
            + CONVERT(VARCHAR(20), @numOppId) +','+
            + 'numoppitemtCode,1,Amount,numUnitHour)'

		SET @strSQLUpdate = @strSQLUpdate
            + ',[CRV]= dbo.fn_CalBizDocTaxAmt('
            + CONVERT(VARCHAR(20), @numDomainID) + ',1,'
            + CONVERT(VARCHAR(20), @numOppId) +','+
            + 'numoppitemtCode,1,Amount,numUnitHour)'
	END


	DECLARE @vcTaxName AS VARCHAR(100)
    DECLARE @numTaxItemID AS NUMERIC(9)
    SET @numTaxItemID = 0
    SELECT TOP 1
            @vcTaxName = vcTaxName,
            @numTaxItemID = numTaxItemID
    FROM    TaxItems
    WHERE   numDomainID = @numDomainID


    WHILE @numTaxItemID > 0
    BEGIN
        EXEC ('ALTER TABLE #Temp1 ADD [' + @vcTaxName + '] DECIMAL(20,5)')

        SET @strSQLUpdate = @strSQLUpdate + ',[' + @vcTaxName
            + ']= dbo.fn_CalBizDocTaxAmt('
            + CONVERT(VARCHAR(20), @numDomainID) + ','
            + CONVERT(VARCHAR(20), @numTaxItemID) + ','
            + CONVERT(VARCHAR(20), @numOppId) +','+
            + 'numoppitemtCode,1,Amount,numUnitHour)'
       

        SELECT TOP 1
                @vcTaxName = vcTaxName,
                @numTaxItemID = numTaxItemID
        FROM    TaxItems
        WHERE   numDomainID = @numDomainID
                AND numTaxItemID > @numTaxItemID

        IF @@rowcount = 0 
            SET @numTaxItemID = 0
    END

    IF @strSQLUpdate <> '' 
    BEGIN
        SET @strSQLUpdate = 'UPDATE #Temp1 SET ItemCode=ItemCode' + @strSQLUpdate + ' WHERE ItemCode > 0'
        EXEC (@strSQLUpdate)
    END

    DECLARE @vcDbColumnName NVARCHAR(50)
    SELECT TOP 1
            @numFldID = numfieldID,
            @vcFldname = vcFieldName,
            @vcDbColumnName = vcDbColumnName,
            @intRowNum = ( tintRow + 1 )
    FROM    View_DynamicCustomColumns
    WHERE   numFormID = @tintType
            AND Grp_id = 5
            AND numDomainID = @numDomainID
            AND numAuthGroupID = @numBizDocId
            AND bitCustom = 1
            AND ISNULL(numRelCntType, 0) = @numBizDocTempID
    ORDER BY tintRow                                                                                          
    WHILE @intRowNum > 0                                                                                  
        BEGIN    
            EXEC
                ( 'alter table #Temp1 add [' + @vcDbColumnName
                  + '] varchar(100)'
                )
	
            SET @strSQLUpdate = 'update #Temp1 set ItemCode=ItemCode,['
                + @vcDbColumnName + ']=dbo.GetCustFldValueItem(' + @numFldID
                + ',ItemCode) where ItemCode>0'
            EXEC ( @strSQLUpdate
                )
		                   
            SELECT TOP 1
                    @numFldID = numfieldID,
                    @vcFldname = vcFieldName,
                    @vcDbColumnName = vcDbColumnName,
                    @intRowNum = ( tintRow + 1 )
            FROM    View_DynamicCustomColumns
            WHERE   numFormID = @tintType
                    AND Grp_id = 5
                    AND numDomainID = @numDomainID
                    AND numAuthGroupID = @numBizDocId
                    AND bitCustom = 1
                    AND tintRow >= @intRowNum
                    AND ISNULL(numRelCntType, 0) = @numBizDocTempID
            ORDER BY tintRow                                                                  
		           
            IF @@rowcount = 0 
                SET @intRowNum = 0                                                                                          
        END


    SELECT  Row_NUmber() OVER ( ORDER BY numoppitemtCode ) RowNumber,
            *
    FROM    #Temp1
    ORDER BY numoppitemtCode ASC 

    DROP TABLE #Temp1

    SELECT  ISNULL(tintOppStatus, 0)
    FROM    OpportunityMaster
    WHERE   numOppID = @numOppId                                                                                                 


IF (SELECT COUNT(*) FROM View_DynamicColumns WHERE   numFormID = @tintType AND numDomainID = @numDomainID
            AND numAuthGroupID = @numBizDocId AND vcFieldType = 'R'AND ISNULL(numRelCntType, 0) = @numBizDocTempID)=0
BEGIN            
	INSERT INTO DycFormConfigurationDetails (numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numAuthGroupID,numRelCntType)
	select numFormId,numFieldId,1,tintRow,@numDomainID,@numBizDocId,@numBizDocTempID
	 FROM View_DynamicDefaultColumns
	 where numFormId=@tintType and bitDefault=1 and numDomainID=@numDomainID order by tintRow ASC	
	 
	 
	 IF(SELECT COUNT(*) FROM DycFrmConfigBizDocsSumm Ds where numBizDocID=@numBizDocId and Ds.numFormID=@tintType 
	and isnull(Ds.numBizDocTempID,0)=@numBizDocTempID and Ds.numDomainID=@numDomainID)=0
	BEGIN
		INSERT INTO DycFrmConfigBizDocsSumm (numFormID,numBizDocID,numFieldID,numDomainID,numBizDocTempID)
		 SELECT @tintType,@numBizDocId,DFM.numFieldID,@numDomainID,@numBizDocTempID FROM
		 DycFormField_Mapping DFFM 
			JOIN DycFieldMaster DFM on DFFM.numModuleID=DFFM.numModuleID and DFFM.numFieldID=DFM.numFieldID
			where numFormID=16 and DFFM.numDomainID is null
	END

END      


    SELECT  vcFieldName AS vcFormFieldName,
            vcDbColumnName,
            vcFieldDataType,
            tintRow AS intRowNum
    FROM    View_DynamicColumns
    WHERE   numFormID = @tintType
            AND numDomainID = @numDomainID
            AND numAuthGroupID = @numBizDocId
            AND vcFieldType = 'R'
            AND ISNULL(numRelCntType, 0) = @numBizDocTempID
    UNION
    SELECT  vcFieldName AS vcFormFieldName,
            vcDbColumnName,
            'C' vcFieldDataType,
            tintRow AS intRowNum
    FROM    View_DynamicCustomColumns
    WHERE   numFormID = @tintType
            AND Grp_id = 5
            AND numDomainID = @numDomainID
            AND numAuthGroupID = @numBizDocId
            AND bitCustom = 1
            AND ISNULL(numRelCntType, 0) = @numBizDocTempID
    ORDER BY tintRow
GO
/****** Object:  StoredProcedure [dbo].[USP_OPPItemDetails]    Script Date: 09/01/2009 18:59:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- exec USP_OPPItemDetails @numItemCode=31455,@OpportunityId=12387,@numDomainId=110
--created by anooop jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityMaster_GetItems')
DROP PROCEDURE USP_OpportunityMaster_GetItems
GO
CREATE PROCEDURE [dbo].[USP_OpportunityMaster_GetItems]
(
    @numUserCntID NUMERIC(9) = 0,
    @OpportunityId NUMERIC(9) = 0,
    @numDomainId NUMERIC(9),
    @ClientTimeZoneOffset  INT
)
AS 
BEGIN
    IF @OpportunityId >0
    BEGIN
        DECLARE @DivisionID AS NUMERIC(9)
        DECLARE @TaxPercentage AS FLOAT
        DECLARE @tintOppType AS TINYINT
        DECLARE @fld_Name AS VARCHAR(500)
        DECLARE @fld_ID AS NUMERIC(9)
        DECLARE @numBillCountry AS NUMERIC(9)
        DECLARE @numBillState AS NUMERIC(9)
    
        SELECT  
			@DivisionID = numDivisionID
			,@tintOppType = tintOpptype
        FROM 
			OpportunityMaster
        WHERE 
			numOppId = @OpportunityId
               

		CREATE TABLE #tempForm 
		(
			ID INT IDENTITY(1,1)
			,tintOrder TINYINT
			,vcDbColumnName NVARCHAR(50)
			,vcOrigDbColumnName NVARCHAR(50)
			,vcFieldName NVARCHAR(50)
			,vcAssociatedControlType NVARCHAR(50)
			,vcListItemType VARCHAR(3)
			,numListID NUMERIC(9)
			,vcLookBackTableName VARCHAR(50)
			,bitCustomField BIT
			,numFieldId NUMERIC
			,bitAllowSorting BIT
			,bitAllowEdit BIT
			,bitIsRequired BIT
			,bitIsEmail BIT
			,bitIsAlphaNumeric BIT
			,bitIsNumeric BIT
			,bitIsLengthValidation BIT
			,intMaxLength INT
			,intMinLength INT
			,bitFieldMessage BIT
			,vcFieldMessage VARCHAR(500)
			,ListRelID NUMERIC(9)
			,intColumnWidth INT
			,intVisible INT
			,vcFieldDataType CHAR(1)
			,numUserCntID NUMERIC(18,0)
		)

		
		IF (
				SELECT 
					COUNT(*)
				FROM 
					View_DynamicColumns DC 
				WHERE 
					DC.numFormId=(CASE WHEN @tintOppType = 2 THEN 129 ELSE 26 END) and DC.numDomainID=@numDomainID AND numUserCntID = @numUserCntID AND
					ISNULL(DC.bitSettingField,0)=1 AND ISNULL(DC.bitDeleted,0)=0 AND ISNULL(DC.bitCustom,0)=0) > 0
			OR
			(
				SELECT 
					COUNT(*)
				FROM 
					View_DynamicCustomColumns
				WHERE 
					numFormId=(CASE WHEN @tintOppType = 2 THEN 129 ELSE 26 END) AND numUserCntID=@numUserCntID AND numDomainID=@numDomainID AND tintPageType=1 AND numRelCntType=0 AND ISNULL(bitCustom,0)=1
			) > 0
		BEGIN
			INSERT INTO 
				#tempForm
			SELECT 
				ISNULL(DC.tintRow,0) + 1 as tintOrder
				,DDF.vcDbColumnName
				,DDF.vcOrigDbColumnName
				,ISNULL(DDF.vcCultureFieldName,DDF.vcFieldName)
				,DDF.vcAssociatedControlType
				,DDF.vcListItemType
				,DDF.numListID,DC.vcLookBackTableName                                                 
				,DDF.bitCustom,DC.numFieldId
				,DDF.bitAllowSorting
				,DDF.bitInlineEdit
				,DDF.bitIsRequired
				,DDF.bitIsEmail
				,DDF.bitIsAlphaNumeric
				,DDF.bitIsNumeric
				,DDF.bitIsLengthValidation
				,DDF.intMaxLength
				,DDF.intMinLength
				,DDF.bitFieldMessage
				,DDF.vcFieldMessage vcFieldMessage
				,DDF.ListRelID
				,DC.intColumnWidth
				,CASE WHEN DC.numFieldID IS NULL THEN 0 ELSE 1 END
				,DDF.vcFieldDataType
				,DC.numUserCntID
			FROM  
				View_DynamicDefaultColumns DDF 
			LEFT JOIN 
				View_DynamicColumns DC 
			ON 
				DC.numFormId=(CASE WHEN @tintOppType = 2 THEN 129 ELSE 26 END) 
				AND DDF.numFieldId=DC.numFieldId 
				AND DC.numUserCntID=@numUserCntID 
				AND DC.numDomainID=@numDomainID 
				AND numRelCntType=0 
				AND tintPageType=1 
			WHERE 
				DDF.numFormId=(CASE WHEN @tintOppType = 2 THEN 129 ELSE 26 END) 
				AND DDF.numDomainID=@numDomainID 
				AND ISNULL(DDF.bitSettingField,0)=1 
				AND ISNULL(DDF.bitDeleted,0)=0 
				AND ISNULL(DDF.bitCustom,0)=0
			UNION
			SELECT 
				tintRow + 1 AS tintOrder
				,vcDbColumnName
				,vcFieldName
				,vcFieldName
				,vcAssociatedControlType
				,'' as vcListItemType
				,numListID
				,''                                                 
				,bitCustom
				,numFieldId
				,bitAllowSorting
				,0 as bitAllowEdit
				,bitIsRequired
				,bitIsEmail
				,bitIsAlphaNumeric
				,bitIsNumeric
				,bitIsLengthValidation
				,intMaxLength
				,intMinLength
				,bitFieldMessage
				,vcFieldMessage
				,ListRelID
				,intColumnWidth
				,1
				,''
				,0
			FROM 
				View_DynamicCustomColumns
			WHERE 
				numFormId=(CASE WHEN @tintOppType = 2 THEN 129 ELSE 26 END) 
				AND numUserCntID=@numUserCntID 
				AND numDomainID=@numDomainID 
				AND tintPageType=1 
				AND numRelCntType=0
				AND ISNULL(bitCustom,0)=1
		END
		ELSE
		BEGIN
			--Check if Master Form Configuration is created by administrator if NoColumns=0
			DECLARE @IsMasterConfAvailable BIT = 0
			DECLARE @numUserGroup NUMERIC(18,0)

			SELECT @numUserGroup = numGroupID FROM UserMaster WHERE numUserDetailId = @numUserCntID 

			IF (SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = @numDomainID AND numFormID = (CASE WHEN @tintOppType = 2 THEN 129 ELSE 26 END) AND bitGridConfiguration = 1 AND numGroupID = @numUserGroup) > 0
			BEGIN
				SET @IsMasterConfAvailable = 1
			END

			--If MasterConfiguration is available then load it otherwise load default columns
			IF @IsMasterConfAvailable = 1
			BEGIN
				INSERT INTO DycFormConfigurationDetails 
				(
					numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth
				)
				SELECT 
					(CASE WHEN @tintOppType = 2 THEN 129 ELSE 26 END),numFieldId,intColumnNum,intRowNum,@numDomainID,@numUserCntID,0,1,0,intColumnWidth
				FROM 
					View_DynamicColumnsMasterConfig
				WHERE
					View_DynamicColumnsMasterConfig.numFormId=(CASE WHEN @tintOppType = 2 THEN 129 ELSE 26 END) AND 
					View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
					View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
					View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
					ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
					ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
				UNION
				SELECT 
					(CASE WHEN @tintOppType = 2 THEN 129 ELSE 26 END),numFieldId,intColumnNum,intRowNum,@numDomainID,@numUserCntID,0,1,1,intColumnWidth
				FROM 
					View_DynamicCustomColumnsMasterConfig
				WHERE 
					View_DynamicCustomColumnsMasterConfig.numFormId=(CASE WHEN @tintOppType = 2 THEN 129 ELSE 26 END) AND 
					View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
					View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
					View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
					ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1


				INSERT INTO #tempForm
				SELECT 
					(intRowNum + 1) as tintOrder, vcDbColumnName, vcOrigDbColumnName,ISNULL(vcCultureFieldName,vcFieldName),                  
					vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName,bitCustom,numFieldId,bitAllowSorting,bitInlineEdit,
					bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength,
					bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth,1,vcFieldDataType,@numUserCntID
				FROM 
					View_DynamicColumnsMasterConfig
				WHERE
					View_DynamicColumnsMasterConfig.numFormId=(CASE WHEN @tintOppType = 2 THEN 129 ELSE 26 END) AND 
					View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
					View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
					View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
					ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
					ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
				UNION
				SELECT 
					tintRow+1 as tintOrder,vcDbColumnName,vcFieldName,vcFieldName,                  
					 vcAssociatedControlType,'' as vcListItemType,numListID,''                                                 
					,bitCustom,numFieldId,bitAllowSorting,0 as bitAllowEdit,
					bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
					intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,'',0
				FROM 
					View_DynamicCustomColumnsMasterConfig
				WHERE 
					View_DynamicCustomColumnsMasterConfig.numFormId=(CASE WHEN @tintOppType = 2 THEN 129 ELSE 26 END) AND 
					View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
					View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
					View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
					ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1
				ORDER BY 
					tintOrder ASC  
			END
		END

		
		DECLARE @strSQL VARCHAR(MAX) = ''

		DECLARE @avgCost INT
		DECLARE @tintCommitAllocation TINYINT
		SELECT @avgCost=ISNULL(numCost,0),@tintCommitAllocation=ISNULL(tintCommitAllocation,1) FROM Domain WHERE numDomainId=@numDomainID

		--------------Make Dynamic Query--------------
		SET @strSQL = @strSQL + 'SELECT 
									Opp.numoppitemtCode
									,Opp.vcNotes
									,WItems.numOnOrder
									,WItems.numBackOrder
									,CAST(ISNULL(Opp.numCost,0) AS DECIMAL(20,5)) AS numCost
									,ISNULL(Opp.bitItemPriceApprovalRequired,0) AS bitItemPriceApprovalRequired
									,ISNULL(Opp.numSOVendorID,0) AS numVendorID
									,' 
									+ CONCAT('ISNULL(Opp.monTotAmount,0) - (ISNULL(Opp.numUnitHour,0) * (CASE ',@avgCost,' WHEN 3 THEN ISNULL(V.monCost,0) / dbo.fn_UOMConversion(numPurchaseUnit,I.numItemCode,',@numDomainID,',numBaseUnit) WHEN 2 THEN (CASE WHEN ISNULL(Opp.numSOVendorID,0) > 0 THEN Opp.numCost ELSE ISNULL(V.monCost,0) / dbo.fn_UOMConversion(numPurchaseUnit,I.numItemCode,',@numDomainID,',numBaseUnit) END) ELSE monAverageCost END)) AS vcMargin,') + '
									OM.tintOppType
									,Opp.numItemCode
									,I.charItemType
									,ISNULL(I.bitAsset,0) as bitAsset
									,ISNULL(I.bitRental,0) as bitRental
									,ISNULL(I.bitSerialized,0) bitSerialized
									,ISNULL(I.bitLotNo,0) bitLotNo
									,Opp.numOppId
									,Opp.bitWorkOrder
									,Opp.fltDiscount
									,ISNULL(Opp.bitDiscountType,0) bitDiscountType
									,dbo.fn_UOMConversion(Opp.numUOMId, Opp.numItemCode,' + CAST(@numDomainId AS VARCHAR) +', NULL) AS UOMConversionFactor
									,ISNULL(Opp.bitWorkOrder,0) bitWorkOrder
									,ISNULL(Opp.numUnitHourReceived,0) numUnitHourReceived
									,ISNULL((SELECT COUNT(*) FROM OppWarehouseSerializedItem WHERE numOppId=Opp.numOppID AND numOppItemID=Opp.numoppitemtCode),0) AS numSerialNoAssigned
									,(CASE 
										WHEN (SELECT 
												COUNT(*) 
											FROM 
												OpportunityBizDocs OB 
											JOIN 
												dbo.OpportunityBizDocItems OBI 
											ON 
												ob.numOppBizDocsId=obi.numOppBizDocID 
												AND (ob.bitAuthoritativeBizDocs=1 OR ob.numBizDocId=304) 
												AND obi.numOppItemID=Opp.numoppitemtCode 
												AND ob.numOppId=Opp.numOppId)>0 
											THEN 1 
											ELSE 0 
									END) AS bitIsAuthBizDoc
									,(CASE 
										WHEN (SELECT 
													COUNT(*) 
												FROM 
													OpportunityBizDocs OB 
												JOIN 
													dbo.OpportunityBizDocItems OBI 
												ON 
													OB.numOppBizDocsId=OBI.numOppBizDocID 
												WHERE 
													OB.numOppId = Opp.numOppId 
													AND OBI.numOppItemID=Opp.numoppitemtCode 
													AND OB.numBizDocId=296) > 0 
											THEN 1 
											ELSE 0 
										END) AS bitAddedFulFillmentBizDoc
									,ISNULL(numPromotionID,0) AS numPromotionID
									,ISNULL(opp.numUOMId, 0) AS numUOM
									,(SELECT vcUnitName FROM dbo.UOM WHERE numUOMId = opp.numUOMId AND ISNULL(bitEnabled,0) = 1)  AS  [vcBaseUOMName]
									,Opp.numUnitHour AS numOriginalUnitHour 
									,ISNULL(M.vcPOppName,''Internal'') as Source
									,numSourceID
									,(CASE WHEN ISNULL(bitKitParent,0) = 1 AND ISNULL(bitAssembly,0) = 1 THEN 0 WHEN ISNULL(bitKitParent,0) = 1 THEN 1 ELSE 0 END) as bitKitParent
									,(CASE 
										WHEN OM.tintOppType = 1 AND OM.tintOppStatus = 1 
										THEN 
											(CASE 
												WHEN (I.charItemType = ''p'' AND ISNULL(bitDropShip, 0) = 0 AND ISNULL(bitAsset,0)=0)
												THEN 
													dbo.CheckOrderItemInventoryStatus(Opp.numItemCode,Opp.numUnitHour,Opp.numoppitemtCode,Opp.numWarehouseItmsID,(CASE WHEN ISNULL(bitKitParent,0) = 1 AND ISNULL(bitAssembly,0) = 1 THEN 0  WHEN ISNULL(bitKitParent,0) = 1 THEN 1 ELSE 0 END)) 
												ELSE 0 
											END) 
										ELSE 0 
									END) AS bitBackOrder
									,(SELECT vcData FROM dbo.ListDetails WHERE 1=1 AND numListItemID = numShipClass ) AS [numShipClass]
									,ISNULL(Opp.numQtyShipped,0) numQtyShipped
									,ISNULL(Opp.numUnitHourReceived,0) AS numUnitHourReceived
									,ISNULL(OM.fltExchangeRate,0) AS fltExchangeRate
									,ISNULL(OM.numCurrencyID,0) AS numCurrencyID
									,ISNULL(OM.numDivisionID,0) AS numDivisionID
									,(CASE 
										WHEN OM.tintOppType = 1 AND OM.tintOppStatus = 1
										THEN CAST(ISNULL(Opp.numQtyShipped,0) AS VARCHAR)
										WHEN OM.tintOppType = 2 AND OM.tintOppStatus = 1
										THEN CAST(ISNULL(Opp.numUnitHourReceived,0) AS VARCHAR)
										ELSE '''' 
									END) AS vcShippedReceived
									,ISNULL(I.numIncomeChartAcntId,0) AS itemIncomeAccount
									,ISNULL(I.numAssetChartAcntId,0) AS itemInventoryAsset
									,ISNULL(i.numCOGsChartAcntId,0) AS itemCoGs
									,ISNULL(Opp.numRentalIN,0) as numRentalIN
									,ISNULL(Opp.numRentalLost,0) as numRentalLost
									,ISNULL(Opp.numRentalOut,0) as numRentalOut
									,ISNULL(bitFreeShipping,0) AS [IsFreeShipping]
									,Opp.bitDiscountType,isnull(Opp.monLandedCost,0) as monLandedCost
									, ISNULL(Opp.vcPromotionDetail,'''') AS vcPromotionDetail
									,'''' AS numPurchasedQty
									,Opp.vcPathForTImage
									,Opp.vcItemName
									,Opp.vcModelID
									,vcWarehouse
									,ISNULL(WItems.numWarehouseID,0) AS numWarehouseID
									,ISNULL(Opp.numShipToAddressID,0) AS numShipToAddressID
									,Case when ISNULL(I.bitKitParent,0)=1 then CAST(ISNULL(dbo.fn_GetKitInventoryByWarehouse(I.numItemCode, WItems.numWareHouseID),0) AS DECIMAL(18,2)) ELSE CAST(ISNULL(WItems.numOnHand,0) AS FLOAT) END numOnHand
									,ISNULL(WItems.numAllocation,0) as numAllocation
									,WL.vcLocation
									,ISNULL(Opp.vcType,'''') ItemType
									,ISNULL(Opp.vcType,'''') vcType
									,ISNULL(Opp.vcItemDesc, '''') vcItemDesc
									,Opp.vcAttributes
									,ISNULL(bitDropShip, 0) as DropShip
									,CASE ISNULL(bitDropShip, 0) WHEN 1 THEN ''Yes'' ELSE '''' END AS bitDropShip
									,CAST(( dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), Opp.numItemCode,om.numDomainId, ISNULL(opp.numUOMId, 0))* Opp.numUnitHour ) AS NUMERIC(18, 2)) AS numUnitHour
									,Opp.monPrice
									,CAST(( dbo.fn_UOMConversion(ISNULL(opp.numUOMId, 0), Opp.numItemCode,om.numDomainId, ISNULL(I.numBaseUnit, 0)) * Opp.monPrice) AS NUMERIC(18, 6)) AS monPriceUOM
									,Opp.monTotAmount
									,Opp.monTotAmtBefDiscount
									,ISNULL(WItems.vcWHSKU,ISNULL(I.vcSKU,'''')) [vcSKU]
									,Opp.vcManufacturer
									,dbo.[fn_GetListItemName](I.[numItemClassification]) numItemClassification
									,(SELECT [vcItemGroup] FROM [ItemGroups] WHERE [numItemGroupID] = I.numItemGroup) numItemGroup
									,ISNULL(u.vcUnitName, '''') numUOMId
									,(CASE 
										WHEN ISNULL(I.bitKitParent,0)=1  
												AND (CASE 
														WHEN EXISTS (SELECT ID.numItemDetailID FROM ItemDetails ID INNER JOIN Item IInner ON ID.numChildItemID=IInner.numItemCode WHERE ID.numItemKitID=I.numItemCode AND ISNULL(IInner.bitKitParent,0)=1)
														THEN 1
														ELSE 0
													END) = 0
										THEN 
											dbo.GetKitWeightBasedOnItemSelection(I.numItemCode,Opp.vcChildKitSelectedItems)
										ELSE 
											ISNULL(I.fltWeight,0) 
									END) fltWeight
									,I.fltHeight
									,I.fltWidth
									,I.fltLength
									,I.numBarCodeId
									,I.IsArchieve
									,ISNULL(I.numContainer,0) AS numContainer
									,ISNULL(I.numNoItemIntoContainer,0) AS numContainerQty
									,CASE WHEN Opp.bitDiscountType = 0
										 THEN REPLACE(STR(ISNULL(Opp.monTotAmtBefDiscount, Opp.monTotAmount)
											  - Opp.monTotAmount,20,4), '' '', '''') + '' (''
											  + CAST(FORMAT(Opp.fltDiscount,''0.00##'') AS VARCHAR(50)) + ''%)''
										 ELSE REPLACE(STR(ISNULL(Opp.monTotAmtBefDiscount, Opp.monTotAmount)
											  - Opp.monTotAmount,20,4), '' '', '''')
									END AS DiscAmt
									,ISNULL(Opp.numProjectID, 0) numProjectID
									,CASE WHEN ( SELECT TOP 1
														ISNULL(OBI.numOppBizDocItemID, 0)
												FROM    dbo.OpportunityBizDocs OBD
														INNER JOIN dbo.OpportunityBizDocItems OBI ON OBD.numOppBizDocsID = OBI.numOppBizDocID
												WHERE   OBI.numOppItemID = Opp.numoppitemtCode
														AND ISNULL(OBD.bitAuthoritativeBizDocs, 0) = 1
														and isnull(OBD.numOppID,0)=OM.numOppID
											  ) > 0 THEN 1
										 ELSE 0
									END AS bitItemAddedToAuthBizDoc
									,ISNULL(Opp.numClassID, 0) numClassID
									,ISNULL(Opp.numProjectStageID, 0) numProjectStageID
									,CASE 
										WHEN OPP.numProjectStageID > 0 THEN dbo.fn_GetProjectStageName(OPP.numProjectId,OPP.numProjectStageID)
										WHEN OPP.numProjectStageID = -1 THEN ''T&E''
										WHEN OPP.numProjectId > 0 THEN CASE WHEN OM.tintOppType = 1 THEN ''S.O.'' ELSE ''P.O.'' END
										ELSE ''-''
									END AS vcProjectStageName
									,CAST(Opp.numUnitHour AS FLOAT) numQty
									,ISNULL((SELECT sum(numQty) FROM OppWarehouseSerializedItem WHERE numOppID=OM.numOppID AND numOppItemID=Opp.numoppitemtCode),0) AS SerialLotNo
									,Opp.numWarehouseItmsID,CPN.CustomerPartNo
									,(CASE WHEN ISNULL(Opp.bitMarkupDiscount,0) = 0 THEN ''0'' ELSE ''1'' END) AS bitMarkupDiscount
									,CAST(Opp.ItemRequiredDate AS DATE) AS ItemRequiredDate,ISNULL(bitWinningBid,0) bitWinningBid, ISNULL(numParentOppItemID,0) numParentOppItemID,ISNULL(Opp.numQtyReceived,0) numQtyReceived
									,ISNULL(Opp.monAvgCost,0) monAverageCost,'


		
			   IF EXISTS (SELECT * FROM #tempForm WHERE vcOrigDbColumnName='vcWorkOrderStatus' AND ISNULL(numUserCntID,0) = @numUserCntID)
			   BEGIN
					SET @strSQL = @strSQL + CONCAT('dbo.GetWorkOrderStatus(om.numDomainID,',@tintCommitAllocation,',Opp.numOppID,Opp.numoppitemtCode) AS vcWorkOrderStatus,')
			   END     

			   IF EXISTS (SELECT * FROM #tempForm WHERE vcOrigDbColumnName='vcBackordered' AND ISNULL(numUserCntID,0) = @numUserCntID)
			   BEGIN
					SET @strSQL = @strSQL + '(CASE 
												WHEN OM.tintOppType = 1 AND OM.tintOppStatus = 1 
												THEN 
													(CASE 
														WHEN (I.charItemType = ''p'' AND ISNULL(bitDropShip, 0) = 0 AND ISNULL(bitAsset,0)=0)
														THEN 
															CAST(dbo.CheckOrderItemInventoryStatus(Opp.numItemCode,Opp.numUnitHour,Opp.numoppitemtCode,Opp.numWarehouseItmsID,(CASE WHEN ISNULL(bitKitParent,0) = 1 AND ISNULL(bitAssembly,0) = 1 THEN 0  WHEN ISNULL(bitKitParent,0) = 1 THEN 1 ELSE 0 END)) AS VARCHAR)
														ELSE ''''
													END) 
												ELSE ''''
											END) vcBackordered, '
			   END


			   IF EXISTS (SELECT * FROM #tempForm WHERE vcOrigDbColumnName='vcItemReleaseDate' AND ISNULL(numUserCntID,0) = @numUserCntID)
			   BEGIN
					SET @strSQL = @strSQL + '(CASE WHEN I.charItemType = ''p'' AND ISNULL(bitDropShip, 0) = 0 THEN ISNULL(dbo.FormatedDateFromDate(Opp.ItemReleaseDate,om.numDomainId),''Not Available'') ELSE '''' END)  AS vcItemReleaseDate,'
			   END

			   IF EXISTS (SELECT * FROM #tempForm WHERE vcOrigDbColumnName='SerialLotNo' AND ISNULL(numUserCntID,0) = @numUserCntID)
			   BEGIN
					SET @strSQL = @strSQL + 'SUBSTRING((SELECT  
													'', '' + vcSerialNo + CASE WHEN ISNULL(I.bitLotNo, 0) = 1 THEN '' ('' + CONVERT(VARCHAR(15), oppI.numQty) + '')'' ELSE ''''	END
												FROM 
													OppWarehouseSerializedItem oppI
												JOIN 
													WareHouseItmsDTL whi 
												ON 
													oppI.numWareHouseItmsDTLID = whi.numWareHouseItmsDTLID
												WHERE 
													oppI.numOppID = Opp.numOppId
													AND oppI.numOppItemID = Opp.numoppitemtCode
												ORDER BY 
													vcSerialNo
												FOR
													XML PATH('''')), 3, 200000) AS vcSerialLotNo,'
			   END
			   
			   IF EXISTS (SELECT * FROM #tempForm WHERE vcOrigDbColumnName='vcInclusionDetails' AND ISNULL(numUserCntID,0) = @numUserCntID)
			   BEGIN
					SET @strSQL = @strSQL + CONCAT('dbo.GetOrderAssemblyKitInclusionDetails(OM.numOppID,Opp.numoppitemtCode,Opp.numUnitHour,',@tintCommitAllocation,',0) AS vcInclusionDetails,')
			   END    
			   
			   IF EXISTS (SELECT * FROM #tempForm WHERE vcOrigDbColumnName='OppItemShipToAddress' )
			   BEGIN
					SET @strSQL = @strSQL +  '(CASE WHEN Opp.numShipToAddressID > 0 THEN (+ isnull((AD.vcStreet)  +'', '','''') + isnull((AD.vcCity)+ '', '','''')  + '' <br>''
                            + isnull(dbo.fn_GetState(AD.numState) + '', '' ,'''') 
                            + isnull(AD.vcPostalCode + '', '' ,'''')  + '' <br>''  +
                            + isnull(dbo.fn_GetListItemName(AD.numCountry),'''') )
							ELSE '''' END ) AS OppItemShipToAddress,'
			   END   
	
			-- (CASE WHEN @tintOppType=1 THEN 26 ELSE 129 END) --

		--------------Add custom fields to query----------------
		SELECT TOP 1 @fld_Name= vcFieldName,@fld_ID = numFieldId FROM #tempForm WHERE ISNULL(bitCustomField,0)=1 ORDER BY numFieldId
		WHILE @fld_ID > 0
		BEGIN 
			SET @strSQL = @strSQL + CONCAT('dbo.GetCustFldValueOppItems(',@fld_ID,',Opp.numoppitemtCode,Opp.numItemCode) as ''',@fld_Name,''',')
       
			SELECT TOP 1 @fld_Name= vcFieldName,@fld_ID = numFieldId FROM #tempForm WHERE ISNULL(bitCustomField,0)=1 AND numFieldId > @fld_ID ORDER BY numFieldId
		 
			IF @@ROWCOUNT=0
				SET @fld_ID = 0
		END

		 SET @strSQL = left(@strSQL,len(@strSQL)-1) + '   
				FROM    OpportunityItems Opp
				LEFT JOIN OpportunityMaster OM ON Opp.numOppID = oM.numOppID
				INNER JOIN DivisionMaster DM ON OM.numDivisionID=DM.numDivisionID
				JOIN item I ON Opp.numItemCode = i.numItemcode
				LEFT JOIN WareHouseItems WItems ON WItems.numWareHouseItemID = Opp.numWarehouseItmsID
				LEFT JOIN Warehouses W ON W.numWarehouseID = WItems.numWarehouseID
				LEFT JOIN WarehouseLocation WL ON WL.numWLocationID = WItems.numWLocationID
				LEFT JOIN OpportunityMaster M ON Opp.numSourceID = M.numOppID
				LEFT JOIN UOM u ON u.numUOMId = opp.numUOMId
				LEFT JOIN Vendor V ON I.numVendorID = V.numVendorID  AND V.numItemCode = I.numItemCode
				LEFT JOIN CustomerPartNumber CPN ON CPN.numItemCode = Opp.numItemCode AND CPN.numCompanyId=DM.numCompanyID
				LEFT JOIN AddressDetails AD ON Opp.numShipToAddressID = AD.numAddressID
		WHERE   om.numDomainId = '+ CONVERT(VARCHAR(10),@numDomainId) + '
				AND Opp.numOppId = '+ CONVERT(VARCHAR(15),@OpportunityId) +'
		ORDER BY opp.numSortOrder,numoppitemtCode ASC '
  
		--PRINT CAST(@strSQL AS NTEXT);
		EXEC (@strSQL);
		SELECT  vcSerialNo,
			   vcComments AS Comments,
				numOppItemID AS numoppitemtCode,
				W.numWarehouseItmsDTLID,
				numWarehouseItmsID AS numWItmsID,
				dbo.Fn_getattributes(W.numWarehouseItmsDTLID,
									 1) Attributes
		FROM    WareHouseItmsDTL W
				JOIN OppWarehouseSerializedItem O ON O.numWarehouseItmsDTLID = W.numWareHouseItmsDTLID
		WHERE   numOppID = @OpportunityId


		select * from #tempForm order by tintOrder

		drop table #tempForm

    END         
END
GO
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityMaster_GetPOForMerge')
DROP PROCEDURE dbo.USP_OpportunityMaster_GetPOForMerge
GO
CREATE PROCEDURE [dbo].[USP_OpportunityMaster_GetPOForMerge]
(
	@numDomainID NUMERIC(18,0)
	,@numWarehouseID NUMERIC(18,0)
	,@bitOnlyMergable BIT
	,@bitExcludePOSendToVendor BIT
	,@bitDisplayOnlyCostSaving BIT
	,@tintCostType TINYINT
	,@ClientTimeZoneOffset INT
)
AS 
BEGIN
	DECLARE @TEMP TABLE
	(
		numDivisionID NUMERIC(18,0)
		,numOppId NUMERIC(18,0)
		,numItemCode NUMERIC(18,0)
		,vcCompanyName VARCHAR(300)
		,vcNavigationURL VARCHAR(300)
		,vcPOppName VARCHAR(300)
		,vcCreatedBy VARCHAR(300)
		,bintCreatedDate VARCHAR(50)
		,monDealAmount DECIMAL(20,5)
		,vcItemName VARCHAR(300)
		,vcSKU VARCHAR(100)
		,fltWeight FLOAT
		,numoppitemtCode NUMERIC(18,0)
		,numUnitHour FLOAT
		,monPrice DECIMAL(20,5)
		,vcItemCostSaving VARCHAR(MAX)
	)

	INSERT INTO @TEMP
	(
		numDivisionID
		,numOppId
		,numItemCode
		,vcCompanyName
		,vcNavigationURL
		,vcPOppName
		,vcCreatedBy
		,bintCreatedDate
		,monDealAmount
		,vcItemName
		,vcSKU
		,fltWeight
		,numoppitemtCode
		,numUnitHour
		,monPrice
		,vcItemCostSaving
	)
	SELECT
		DivisionMaster.numDivisionID
		,OpportunityMaster.numOppId
		,Item.numItemCode
		,CompanyInfo.vcCompanyName
		,(CASE DivisionMaster.tintCRMType 
			WHEN 2 THEN CONCAT('../account/frmAccounts.aspx?DivID=',DivisionMaster.numDivisionID) 
			WHEN 1 THEN CONCAT('../prospects/frmProspects.aspx?DivID=',DivisionMaster.numDivisionID) 
			ELSE CONCAT('../Leads/frmLeads.aspx?DivID=',DivisionMaster.numDivisionID) 
		END)
		,OpportunityMaster.vcPOppName
		,dbo.fn_GetContactName(OpportunityMaster.numCreatedBy) vcCreatedBy
		,dbo.FormatedDateFromDate(DATEADD(MINUTE,@ClientTimeZoneOffset * -1,OpportunityMaster.bintCreatedDate),@numDomainID) bintCreatedDate
		,OpportunityMaster.monDealAmount
		,Item.vcItemName
		,Item.vcSKU
		,ISNULL(Item.fltWeight,0)
		,OpportunityItems.numoppitemtCode
		,ISNULL(OpportunityItems.numUnitHour,0)
		,ISNULL(OpportunityItems.monPrice,0)
		,(CASE 
			WHEN @tintCostType = 2 
			THEN ISNULL(STUFF((SELECT 
					CONCAT(', <input type="button" class="btn btn-xs btn-success" value="$" onClick="return RecordSelected(this,',OM.numDivisionId,',',ISNULL(OI.monPrice,0),',',ISNULL(V.intMinQty,0),')" /> <b>$'
					,FORMAT((ISNULL(OpportunityItems.numUnitHour,0) * ISNULL(OI.monPrice,0)) - (ISNULL(OpportunityItems.numUnitHour,0) * ISNULL(OpportunityItems.monPrice,0)), '#,##0.#####')
					,'</b> (',ISNULL(OI.numUnitHour,0),' U) '
					,CONCAT('<span style="color:',(CASE
								WHEN (DATEDIFF(DAY,OM.bintCreatedDate,GETUTCDATE()) = 0 AND DATEDIFF(DAY,OM.bintCreatedDate,GETUTCDATE()) <= 90)
								THEN '#00b050'
								WHEN (DATEDIFF(DAY,OM.bintCreatedDate,GETUTCDATE()) = 91 AND DATEDIFF(DAY,OM.bintCreatedDate,GETUTCDATE()) <= 180)
								THEN '#ff9933'
								WHEN (DATEDIFF(DAY,OM.bintCreatedDate,GETUTCDATE()) = 181 AND DATEDIFF(DAY,OM.bintCreatedDate,GETUTCDATE()) <= 270)
								THEN '#9148c8'
								ELSE '#ff0000'
							END),'">',DATEDIFF(DAY,OM.bintCreatedDate,GETUTCDATE()),'d</span> ')
						,'<b><a target="_blank" href="../opportunity/frmOpportunities.aspx?OpID=',OM.numOppId,'">',OM.vcPOppName,'</a></b>'
						,' (',CI.vcCompanyName,')') 
				FROM 
					OpportunityMaster OM 
				INNER JOIN
					DivisionMaster DM
				ON
					OM.numDivisionId = DM.numDivisionID
				INNER JOIN
					CompanyInfo CI
				ON
					DM.numCompanyID = CI.numCompanyId
				INNER JOIN 
					OpportunityItems OI 
				ON 
					OM.numOppID=OI.numOppID 
				LEFT JOIN
					Vendor V
				ON
					V.numVendorID = DM.numDivisionID
					AND V.numItemCode = OI.numItemCode
				WHERE 
					OM.numDomainId=@numDomainID 
					AND OM.numOppId <> OpportunityMaster.numOppId
					AND OM.tintOppType = 2 
					AND OM.tintOppStatus = 1 
					AND ISNULL(OM.bitStockTransfer,0) = 0
					AND OI.numItemCode = Item.numItemCode
					AND 1 = (CASE 
								WHEN ISNULL(@bitDisplayOnlyCostSaving,0) = 1 
								THEN (CASE WHEN ISNULL(OI.numUnitHour,0) <> 0 AND ISNULL(OI.numUnitHour,0) < ISNULL(OpportunityItems.numUnitHour,0) THEN 1 ELSE 0 END)
								ELSE 1 
							END)
					AND DATEDIFF(DAY,OM.bintCreatedDate,GETUTCDATE()) <= 600
				ORDER BY
					(ISNULL(OpportunityItems.numUnitHour,0) * ISNULL(OI.monPrice,0)) - (ISNULL(OpportunityItems.numUnitHour,0) * ISNULL(OpportunityItems.monPrice,0)) ASC OFFSET 0 ROWS FETCH NEXT 5 ROWS ONLY
				FOR XML PATH(''), TYPE).value('.', 'varchar(max)'),1,1,''),'') 
			ELSE ''
		END) vcItemCostSaving
	FROM
		OpportunityMaster
	INNER JOIN
		DivisionMaster
	ON
		OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
	INNER JOIN
		CompanyInfo
	ON
		DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
	INNER JOIN
		OpportunityItems
	ON
		OpportunityMaster.numOppId = OpportunityItems.numOppId
	INNER JOIN
		Item
	ON
		OpportunityItems.numItemCode = Item.numItemCode
	INNER JOIN
		WareHouseItems
	ON
		OpportunityItems.numWarehouseItmsID = WareHouseItems.numWareHouseItemID
	WHERE
		OpportunityMaster.numDomainId=@numDomainID
		AND OpportunityMaster.tintOppType = 2
		AND OpportunityMaster.tintOppStatus = 1
		AND ISNULL(OpportunityMaster.bitStockTransfer,0) = 0
		AND ISNULL(OpportunityMaster.tintshipped,0) = 0
		AND (SELECT COUNT(*) FROM OpportunityBizDocs WHERE OpportunityBizDocs.numOppId=OpportunityMaster.numOppId) = 0
		AND WareHouseItems.numWareHouseID=@numWarehouseID
		AND ISNULL(OpportunityItems.numUnitHourReceived,0) = 0
		AND 1 = (CASE WHEN ISNULL(@bitExcludePOSendToVendor,0)=1 THEN (CASE WHEN EXISTS (SELECT numCorrespondenceID FROM Correspondence WHERE numOpenRecordID=OpportunityMaster.numOppId AND tintCorrType IN (2,4)) THEN 0 ELSE 1 END) ELSE 1 END)
		AND 1 = (CASE 
					WHEN @tintCostType = 2 
					THEN (CASE WHEN EXISTS (SELECT 
												OI.numoppitemtCode
											FROM 
												OpportunityMaster OM 
											INNER JOIN
												DivisionMaster DM
											ON
												OM.numDivisionId = DM.numDivisionID
											INNER JOIN
												CompanyInfo CI
											ON
												DM.numCompanyID = CI.numCompanyId
											INNER JOIN 
												OpportunityItems OI 
											ON 
												OM.numOppID=OI.numOppID 
											WHERE 
												OM.numDomainId=@numDomainID 
												AND OM.numOppId <> OpportunityMaster.numOppId
												AND OM.tintOppType = 2 
												AND OM.tintOppStatus = 1 
												AND ISNULL(OM.bitStockTransfer,0) = 0
												AND OI.numItemCode = Item.numItemCode
												AND 1 = (CASE 
															WHEN ISNULL(@bitDisplayOnlyCostSaving,0) = 1 
															THEN (CASE WHEN ISNULL(OI.numUnitHour,0) <> 0 AND ISNULL(OI.numUnitHour,0) < ISNULL(OpportunityItems.numUnitHour,0) THEN 1 ELSE 0 END)
															ELSE 1 
														END)
												AND DATEDIFF(DAY,OM.bintCreatedDate,GETUTCDATE()) <= 600) THEN 1 ELSE 0 END)
					ELSE 1 
				END)
	
	IF ISNULL(@bitOnlyMergable,0) = 1
	BEGIN
		DELETE FROM @TEMP WHERE numDivisionID IN (SELECT numDivisionID FROM (SELECT numDivisionID,numOppId FROM @TEMP GROUP BY numDivisionID,numOppId) TEMP GROUP BY numDivisionID HAVING COUNT(*) < 2)
	END

	SELECT 
		numDivisionID
		,vcCompanyName
		,vcNavigationURL
		,CONCAT('[',STUFF((SELECT 
					CONCAT(', {"intType":"',PurchaseIncentives.intType ,'", "vcIncentives":"',PurchaseIncentives.vcIncentives,'", "vcbuyingqty":"',PurchaseIncentives.vcbuyingqty,'", "tintDiscountType":"',ISNULL(PurchaseIncentives.tintDiscountType,0),'"}') 
				FROM 
					PurchaseIncentives 
				WHERE
					PurchaseIncentives.numDivisionID = T.numDivisionID
				FOR XML PATH('')),1,1,''),']') AS vcPurchaseIncentive 
	FROM 
		@TEMP T 
	GROUP BY 
		numDivisionID,vcCompanyName,vcNavigationURL
	ORDER BY
		T.vcCompanyName

	SELECT
		numDivisionID
		,numOppId
		,vcPOppName
		,vcCreatedBy
		,bintCreatedDate
		,monDealAmount
		,(CASE WHEN EXISTS (SELECT numCorrespondenceID FROM Correspondence WHERE numOpenRecordID=T.numOppId AND tintCorrType IN (2,4)) THEN 1 ELSE 0 END) AS bitPOSendToVendor
		,ISNULL((SELECT TOP 1
					(CASE 
						WHEN EmailHistory.numEmailHstrID IS NOT NULL 
						THEN CONCAT('<i></i>',dbo.FormatedDateFromDate(DateAdd(minute,-@ClientTimeZoneOffset,EmailHistory.dtReceivedOn),@numDomainID))
						ELSE dbo.FormatedDateFromDate(DateAdd(minute,-@ClientTimeZoneOffset, dtCreatedDate),@numDomainID)
					END) 
				FROM 
					Correspondence 
				LEFT JOIN 
					EmailHistory 
				ON 
					Correspondence.numEmailHistoryID=EmailHistory.numEmailHstrID 
				LEFT JOIN 
					Communication
				ON
					Correspondence.numCommID = Communication.numCommId
				WHERE 
					numOpenRecordID=T.numOppId
					AND tintCorrType IN (2,4)),'') vcPOSend
	FROM
		@TEMP T
	GROUP BY
		numDivisionID,numOppId,vcPOppName,vcCreatedBy,bintCreatedDate,monDealAmount
	ORDER BY
		numOppId

	SELECT
		numOppId
		,numoppitemtCode
		,numItemCode
		,vcItemName
		,vcSKU
		,fltWeight
		,numUnitHour
		,monPrice
		,vcItemCostSaving
	FROM 
		@TEMP T
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityMaster_MergePurchaseOrders')
DROP PROCEDURE USP_OpportunityMaster_MergePurchaseOrders
GO
CREATE PROCEDURE [dbo].[USP_OpportunityMaster_MergePurchaseOrders]  
	@numDomainID NUMERIC(18,0),
	@numUserCntID NUMERIC(18,0),
	@numMasterPOID NUMERIC(18,0),
	@vcItemsToMerge VARCHAR(MAX)
AS  
BEGIN
	IF EXISTS (SELECT numOppId FROM OpportunityMaster WHERE numDomainId=@numDomainID AND numOppId=@numMasterPOID)
	BEGIN
		DECLARE @hDocItem INT

		DECLARE @TEMP TABLE
		(
			ID INT IDENTITY(1,1)
			,numOppID NUMERIC(18,0)
			,numOppItemID NUMERIC(18,0)
			,numUnitHour FLOAT
			,monPrice DECIMAL(20,5)
		)

		EXEC sp_xml_preparedocument @hDocItem OUTPUT, @vcItemsToMerge

		INSERT INTO @TEMP
		(
			numOppID
			,numOppItemID
			,numUnitHour
			,monPrice
		)
		SELECT
			ISNULL(OppID,0)
			,ISNULL(OppItemID,0)
			,ISNULL(Units,0)
			,ISNULL(UnitCost,0)
		FROM
			OPENXML (@hDocItem,'/NewDataSet/Table1',2)
		WITH
		(
			OppID  NUMERIC(18,0)
			,OppItemID NUMERIC(18,0)
			,Units FLOAT
			,UnitCost DECIMAL(20,5)
		)

		EXEC sp_xml_removedocument @hDocItem

		IF EXISTS (SELECT OB.numOppBizDocsId FROM OpportunityBizDocs OB INNER JOIN @TEMP T ON OB.numOppId = T.numOppID)
		BEGIN
			RAISERROR('BIZ_DOC_EXISTS',16,1)
		END
		ELSE
		BEGIN
			BEGIN TRY
			BEGIN TRANSACTION                                                                  
				DECLARE @numContactId NUMERIC(18,0)=null                                                                        
				DECLARE @numDivisionId numeric(9)=null                                                                          
				DECLARE @tintSource numeric(9)=null                                                                          
				DECLARE @vcPOppName Varchar(100)=''                                                                 
				DECLARE @Comments varchar(1000)=''                                                                          
				DECLARE @bitPublicFlag bit=0                                                                                                                                                         
				DECLARE @monPAmount DECIMAL(20,5) =0                                                 
				DECLARE @numAssignedTo as numeric(9)=0                                                                                                                                                                                                                                                                                         
				DECLARE @strItems as VARCHAR(MAX)=null                                                                          
				DECLARE @strMilestone as VARCHAR(100)=null                                                                          
				DECLARE @dtEstimatedCloseDate datetime                                                                          
				DECLARE @CampaignID as numeric(9)=null                                                                          
				DECLARE @lngPConclAnalysis as numeric(9)=null                                                                         
				DECLARE @tintOppType as tinyint                                                                                                                                             
				DECLARE @tintActive as tinyint                                                              
				DECLARE @numSalesOrPurType as numeric(9)              
				DECLARE @numRecurringId as numeric(9)
				DECLARE @numCurrencyID as numeric(9)=0
				DECLARE @DealStatus as tinyint =0
				DECLARE @numStatus AS NUMERIC(9)
				DECLARE @vcOppRefOrderNo VARCHAR(100)
				DECLARE @numBillAddressId numeric(9)=0
				DECLARE @numShipAddressId numeric(9)=0
				DECLARE @bitStockTransfer BIT=0
				DECLARE @WebApiId INT = 0
				DECLARE @tintSourceType TINYINT=0
				DECLARE @bitDiscountType as bit
				DECLARE @fltDiscount  as float
				DECLARE @bitBillingTerms as bit
				DECLARE @intBillingDays as integer
				DECLARE @bitInterestType as bit
				DECLARE @fltInterest as float
				DECLARE @tintTaxOperator AS TINYINT
				DECLARE @numDiscountAcntType AS NUMERIC(9)
				DECLARE @vcWebApiOrderNo VARCHAR(100)=NULL
				DECLARE @vcCouponCode		VARCHAR(100) = ''
				DECLARE @vcMarketplaceOrderID VARCHAR(100)=NULL
				DECLARE @vcMarketplaceOrderDate  datetime=NULL
				DECLARE @vcMarketplaceOrderReportId VARCHAR(100)=NULL
				DECLARE @numPercentageComplete NUMERIC(9)
				DECLARE @bitUseShippersAccountNo BIT = 0
				DECLARE @bitUseMarkupShippingRate BIT = 0
				DECLARE @numMarkupShippingRate NUMERIC(19,2) = 0
				DECLARE @intUsedShippingCompany INT = 0
				DECLARE @numShipmentMethod NUMERIC(18,0)=0
				DECLARE @dtReleaseDate DATE = NULL
				DECLARE @numPartner NUMERIC(18,0)=0
				DECLARE @tintClickBtn INT=0
				DECLARE @numPartenerContactId NUMERIC(18,0)=0
				DECLARE @numAccountClass NUMERIC(18,0) = 0
				DECLARE @numWillCallWarehouseID NUMERIC(18,0) = 0
				DECLARE @numVendorAddressID NUMERIC(18,0) = 0

				--GET MASTER PO FIELDS 
				SELECT
					@numContactId =numContactId,@numDivisionId=numDivisionId,@tintSource=tintSource,@vcPOppName=vcPOppName,@Comments=txtComments,
					@bitPublicFlag=bitPublicFlag,@monPAmount=monPAmount,@numAssignedTo=numAssignedTo,@strMilestone=null,@dtEstimatedCloseDate=intPEstimatedCloseDate,
					@CampaignID=numCampainID,@lngPConclAnalysis=lngPConclAnalysis,@tintOppType=tintOppType,@tintActive=tintActive,@numSalesOrPurType=numSalesOrPurType,
					@numRecurringId=null,@numCurrencyID=numCurrencyID,@DealStatus=tintOppStatus,@numStatus=numStatus,@vcOppRefOrderNo=vcOppRefOrderNo,@numBillAddressId=0,
					@numShipAddressId=0,@bitStockTransfer=0,@WebApiId=0,@tintSourceType=tintSourceType,@bitDiscountType=bitDiscountType,@fltDiscount=fltDiscount,
					@bitBillingTerms=bitBillingTerms,@intBillingDays=intBillingDays,@bitInterestType=bitInterestType,@fltInterest=fltInterest,@tintTaxOperator=tintTaxOperator,
					@numDiscountAcntType=numDiscountAcntType,@vcWebApiOrderNo=vcWebApiOrderNo,@vcCouponCode=vcCouponCode,@vcMarketplaceOrderID=vcMarketplaceOrderID,
					@vcMarketplaceOrderDate=bintCreatedDate,@vcMarketplaceOrderReportId=vcMarketplaceOrderReportId,@numPercentageComplete= numPercentageComplete,
					@bitUseShippersAccountNo=bitUseShippersAccountNo,@bitUseMarkupShippingRate=bitUseMarkupShippingRate,@numMarkupShippingRate=numMarkupShippingRate,
					@intUsedShippingCompany= intUsedShippingCompany,@numShipmentMethod=numShipmentMethod,@dtReleaseDate=dtReleaseDate,@numPartner=numPartner,@numPartenerContactId=numPartenerContact
					,@numAccountClass=numAccountClass,@numVendorAddressID=numVendorAddressID
				FROM
					OpportunityMaster
				WHERE
					numDomainId = @numDomainID
					AND numOppId = @numMasterPOID 

				DECLARE @TEMPMasterItems TABLE
				(
					numoppitemtCode NUMERIC(18,0),numItemCode NUMERIC(18,0),numUnitHour FLOAT,monPrice DECIMAL(20,5),
					monTotAmount DECIMAL(20,5),vcItemDesc VARCHAR(1000),numWarehouseItmsID NUMERIC(18,0),numToWarehouseItemID NUMERIC(18,0),
					Op_Flag INT, ItemType VARCHAR(50),DropShip BIT,bitDiscountType BIT, fltDiscount FLOAT, monTotAmtBefDiscount DECIMAL(20,5),
					vcItemName VARCHAR(300),numUOM NUMERIC(18,0), bitWorkOrder BIT,numVendorWareHouse NUMERIC(18,0),
					numShipmentMethod NUMERIC(18,0),numSOVendorId NUMERIC(18,0),numProjectID NUMERIC(18,0),
					numProjectStageID NUMERIC(18,0),Attributes VARCHAR(200),bitItemPriceApprovalRequired BIT
				)

				--GET ITEMS OF MASTER PO
				INSERT INTO 
					@TEMPMasterItems
				SELECT 
					numoppitemtCode,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,numToWarehouseItemID,
					2,vcType,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount,vcItemName,numUOMId,bitWorkOrder,numVendorWareHouse,
					numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,vcAttributes,bitItemPriceApprovalRequired
				FROM 
					OpportunityItems
				WHERE 
					numOppId = @numMasterPOID

				--GET ITEMS OF PO NEEDS TO BE MERGED TO MASTER PO
				INSERT INTO 
					@TEMPMasterItems
				SELECT 
					0,numItemCode,T.numUnitHour,T.monPrice,(T.numUnitHour * T.monPrice),vcItemDesc,numWarehouseItmsID,numToWarehouseItemID,
					1,vcType,bitDropShip,bitDiscountType,0,(T.numUnitHour * T.monPrice),vcItemName,numUOMId,bitWorkOrder,numVendorWareHouse,
					numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,vcAttributes,bitItemPriceApprovalRequired
				FROM 
					@TEMP T
				INNER JOIN
					OpportunityItems OI
				ON
					T.numOppItemID = OI.numoppitemtCode
		
				-- Op_Flag is 2(UPDATE) FOR MASTER PO AND 1(INSERT) FOR OTHER
				SET @strItems =(
									SELECT 
										numoppitemtCode,numItemCode,CAST(numUnitHour AS DECIMAL(18,8)) AS numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,numToWarehouseItemID,
										Op_Flag, ItemType,DropShip,	bitDiscountType, CAST(fltDiscount AS DECIMAL(18,8)) AS fltDiscount, monTotAmtBefDiscount,	vcItemName,numUOM, bitWorkOrder,
										numVendorWareHouse,numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,Attributes,bitItemPriceApprovalRequired
									FROM 
										@TEMPMasterItems
									FOR XML RAW ('Item'), ROOT ('NewDataSet'), ELEMENTS
								)
		
				--UPDATE MASTER PO AND ADD MERGE PO ITEMS
				EXEC USP_OppManage @numOppID=@numMasterPOID OUTPUT,@numContactId=@numContactId,@numDivisionId=@numDivisionId,@tintSource=@tintSource,@vcPOppName=@vcPOppName output,
									@Comments=@Comments,@bitPublicFlag=@bitPublicFlag,@numUserCntID=@numUserCntID,@monPAmount=@monPAmount,@numAssignedTo=@numAssignedTo,
									@numDomainId=@numDomainId,@strItems=@strItems,@strMilestone=@strMilestone,@dtEstimatedCloseDate=@dtEstimatedCloseDate,@CampaignID=@CampaignID,
									@lngPConclAnalysis=@lngPConclAnalysis,@tintOppType=@tintOppType,@tintActive=@tintActive,@numSalesOrPurType=@numSalesOrPurType,
									@numRecurringId=@numRecurringId,@numCurrencyID=@numCurrencyID,@DealStatus=@DealStatus,@numStatus=@numStatus,@vcOppRefOrderNo=@vcOppRefOrderNo,
									@numBillAddressId=@numBillAddressId,@numShipAddressId=@numShipAddressId,@bitStockTransfer=@bitStockTransfer,@WebApiId=@WebApiId,
									@tintSourceType=@tintSourceType,@bitDiscountType=@bitDiscountType,@fltDiscount=@fltDiscount,@bitBillingTerms=@bitBillingTerms,
									@intBillingDays=@intBillingDays,@bitInterestType=@bitInterestType,@fltInterest=@fltInterest,@tintTaxOperator=@tintTaxOperator,
									@numDiscountAcntType=@numDiscountAcntType,@vcWebApiOrderNo=@vcWebApiOrderNo,@vcCouponCode=@vcCouponCode,@vcMarketplaceOrderID=@vcMarketplaceOrderID,
									@vcMarketplaceOrderDate=@vcMarketplaceOrderDate,@vcMarketplaceOrderReportId=@vcMarketplaceOrderReportId,@numPercentageComplete=@numPercentageComplete,
									@bitUseShippersAccountNo=@bitUseShippersAccountNo,@bitUseMarkupShippingRate=@bitUseMarkupShippingRate,
									@numMarkupShippingRate=@numMarkupShippingRate,@intUsedShippingCompany=@intUsedShippingCompany,@numShipmentMethod=@numShipmentMethod,@dtReleaseDate=@dtReleaseDate,@numPartner=@numPartner,@numPartenerContactId=@numPartenerContactId
									,@numAccountClass=@numAccountClass,@numVendorAddressID=@numVendorAddressID					

				DECLARE @TEMPOpp TABLE
				(
					ID INT IDENTITY(1,1)
					,numOppID NUMERIC(18,0)
				)

				INSERT INTO @TEMPOpp
				(
					numOppID
				)
				SELECT DISTINCT
					numOppID
				FROM
					@TEMP


				DECLARE @i INT = 1
				DECLARE @iCount INT
				DECLARE @numOppID NUMERIC(18,0)

				SELECT @iCount=COUNT(*) FROM @TEMPOpp

				-- DELETE MERGED POS
				WHILE @i <= @iCount
				BEGIN
					SELECT @numOppID=numOppID FROM @TEMPOpp WHERE ID = @i

					IF EXISTS (SELECT numOppId FROM OpportunityMaster WHERE numDomainId=@numDomainID AND numOppId=@numOppID) AND NOT EXISTS (SELECT numoppitemtCode FROM OpportunityItems OI WHERE OI.numOppId=@numOppID AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM @TEMP))
					BEGIN
						EXEC USP_DeleteOppurtunity @numOppId=@numOppID,@numDomainID=@numDomainID,@numUserCntID=@numUserCntID
					END
					ELSE
					BEGIN
						EXEC USP_RevertDetailsOpp @numOppID,0,@numUserCntID

						DELETE
							OI
						FROM
							@TEMP T
						INNER JOIN
							OpportunityItems OI
						ON
							T.numOppItemID = OI.numoppitemtCode
						WHERE
							T.numOppID = @numOppID

						EXEC USP_UpdatingInventoryonCloseDeal @numOppID,0,@numUserCntID


						UPDATE OpportunityMaster SET monDealAmount=dbo.GetDealAmount(@numOppID,getdate(),0),bintModifiedDate=GETUTCDATE(),numModifiedBy=@numUserCntID WHERE numOppId=@numOppID

						--INSERT TAX FOR DIVISION   
						IF (select isnull(bitPurchaseTaxCredit,0) from Domain where numDomainId=@numDomainId)=1
						BEGIN
							--Delete Tax for Opportunity Items if item deleted 
							DELETE FROM OpportunityItemsTaxItems WHERE numOppId=@numOppId AND numOppItemID NOT IN (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId=@numOppID)

							--Insert Tax for Opportunity Items
							INSERT INTO dbo.OpportunityItemsTaxItems 
							(
								numOppId,
								numOppItemID,
								numTaxItemID,
								numTaxID
							) 
							SELECT 
								@numOppId,
								OI.numoppitemtCode,
								TI.numTaxItemID,
								0
							FROM 
								dbo.OpportunityItems OI 
							JOIN 
								dbo.ItemTax IT 
							ON 
								OI.numItemCode=IT.numItemCode 
							JOIN
								TaxItems TI 
							ON 
								TI.numTaxItemID = IT.numTaxItemID 
							WHERE 
								OI.numOppId=@numOppID 
								AND IT.bitApplicable=1 
								AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)
							UNION
							SELECT 
								@numOppId,
								OI.numoppitemtCode,
								0,
								0
							FROM 
								dbo.OpportunityItems OI 
							JOIN 
								dbo.Item I 
							ON 
								OI.numItemCode=I.numItemCode
							WHERE 
								OI.numOppId=@numOppID 
								AND I.bitTaxable=1 
								AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)
							UNION
							SELECT
								@numOppId,
								OI.numoppitemtCode,
								1,
								TD.numTaxID
							FROM
								dbo.OpportunityItems OI 
							INNER JOIN
								ItemTax IT
							ON
								IT.numItemCode = OI.numItemCode
							INNER JOIN
								TaxDetails TD
							ON
								TD.numTaxID = IT.numTaxID
								AND TD.numDomainId = @numDomainId
							WHERE
								OI.numOppId = @numOppID
								AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)
						END
					END

					SET @i = @i + 1
				END
			COMMIT
			END TRY
			BEGIN CATCH
				DECLARE @ErrorMessage NVARCHAR(4000)
				DECLARE @ErrorNumber INT
				DECLARE @ErrorSeverity INT
				DECLARE @ErrorState INT
				DECLARE @ErrorLine INT
				DECLARE @ErrorProcedure NVARCHAR(200)

				IF @@TRANCOUNT > 0
					ROLLBACK TRANSACTION;

				SELECT 
					@ErrorMessage = ERROR_MESSAGE(),
					@ErrorNumber = ERROR_NUMBER(),
					@ErrorSeverity = ERROR_SEVERITY(),
					@ErrorState = ERROR_STATE(),
					@ErrorLine = ERROR_LINE(),
					@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

				RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
			END CATCH	
		END
	END
	ELSE
	BEGIN
		RAISERROR('MASTER_PO_DOES_NOT_EXISTS',16,1)
	END
END
GO



GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityMaster_UpdatePurchaseOrderCost')
DROP PROCEDURE USP_OpportunityMaster_UpdatePurchaseOrderCost
GO
CREATE PROCEDURE [dbo].[USP_OpportunityMaster_UpdatePurchaseOrderCost]  
	@numDomainID NUMERIC(18,0),
	@numUserCntID NUMERIC(18,0),
	@vcSelectedRecords VARCHAR(MAX),
	@ClientTimeZoneOffset INT
AS  
BEGIN
	DECLARE @hDocItem INT

	DECLARE @TEMP TABLE
	(
		numVendorID NUMERIC(18,0)
		,numSelectedVendorID NUMERIC(18,0)
		,numOppID NUMERIC(18,0)
		,numOppItemID NUMERIC(18,0)
		,numUnits FLOAT
		,monUnitCost DECIMAL(20,5)
	)

	EXEC sp_xml_preparedocument @hDocItem OUTPUT, @vcSelectedRecords

	INSERT INTO @TEMP
	(
		numVendorID
		,numSelectedVendorID
		,numOppID
		,numOppItemID
		,numUnits
		,monUnitCost
	)
	SELECT
		ISNULL(VendorID,0)
		,ISNULL(SelectedVendorID,0)
		,ISNULL(OppID,0)
		,ISNULL(OppItemID,0)
		,ISNULL(Units,0)
		,ISNULL(UnitCost,0)
	FROM
		OPENXML (@hDocItem,'/NewDataSet/Table1',2)
	WITH
	(
		VendorID NUMERIC(18,0)
		,SelectedVendorID NUMERIC(18,0)
		,OppID NUMERIC(18,0)
		,OppItemID NUMERIC(18,0)
		,Units FLOAT
		,UnitCost DECIMAL(20,5)
	)

	EXEC sp_xml_removedocument @hDocItem

	IF EXISTS (SELECT OB.numOppBizDocsId FROM OpportunityBizDocs OB INNER JOIN @TEMP T ON OB.numOppId = T.numOppID)
	BEGIN
		RAISERROR('BIZ_DOC_EXISTS',16,1)
		RETURN
	END
	ELSE
	BEGIN
		DECLARE @TEMPVendorItems TABLE
		(
			ID INT IDENTITY(1,1)
			,numVendorID NUMERIC(18,0)
			,numItemCode NUMERIC(18,0)
			,numWarehouseItemID NUMERIC(18,0)
			,numUnitHour FLOAT
			,monPrice DECIMAL(20,5)
		)

		INSERT INTO @TEMPVendorItems
		(
			numVendorID
			,numItemCode
			,numWarehouseItemID
			,numUnitHour
			,monPrice
		)
		SELECT 
			T.numSelectedVendorID
			,OI.numItemCode
			,OI.numWarehouseItmsID
			,OI.numUnitHour
			,T.monUnitCost
		FROM
			@TEMP T
		INNER JOIN
			OpportunityItems OI
		ON
			T.numOppID = OI.numOppId
			AND T.numOppItemID = OI.numoppitemtCode
		INNER JOIN
			OpportunityMaster OM
		ON
			OI.numOppId = OM.numOppId
		WHERE
			OM.numDomainId = @numDomainID
			AND OM.numDivisionId <> T.numSelectedVendorID

		BEGIN TRY
		BEGIN TRANSACTION
			DECLARE @TEMPOpp TABLE
			(
				ID INT IDENTITY(1,1)
				,numOppID NUMERIC(18,0)
			)

			INSERT INTO @TEMPOpp
			(
				numOppID
			)
			SELECT DISTINCT
				numOppID
			FROM
				@TEMP

			DECLARE @i INT = 1
			DECLARE @iCount INT
			DECLARE @numOppID NUMERIC(18,0)

			SELECT @iCount=COUNT(*) FROM @TEMPOpp

			-- DELETE PO NOT ITEM LEFT IN ORDER OR UPDATE ORDER TOTAL AMOUNT AND TAXES
			WHILE @i <= @iCount
			BEGIN
				SELECT @numOppID=numOppID FROM @TEMPOpp WHERE ID = @i

				EXEC USP_RevertDetailsOpp @numOppID,0,@numUserCntID

				-- DELETE ITEMS FROM ORDER FOR WHOM ANOTHER VENDOR IS SELECTED TO CREATE NEW ORDER
				DELETE 
					OI
				FROM
					OpportunityMaster OM
				INNER JOIN
					OpportunityItems OI
				ON
					OM.numOppId = OI.numOppId
				INNER JOIN
					@TEMP T
				ON
					OI.numOppId = T.numOppID
					AND OI.numoppitemtCode = T.numOppItemID
				WHERE
					OM.numDomainId = @numDomainID
					AND OM.numOppId = @numOppID
					AND OM.numDivisionId <> T.numSelectedVendorID

				-- UPDATE LINE ITEM UNITS AND COST WHERE VENDOR IS SAME
				UPDATE
					OI
				SET
					OI.numUnitHour = ISNULL(T.numUnits,0)
					,OI.monPrice = ISNULL(T.monUnitCost,0)
					,monTotAmount = (ISNULL(T.numUnits,0) * (CASE 
																	WHEN ISNULL(OI.bitDiscountType,0) = 0 
																	THEN (ISNULL(T.monUnitCost,0) - (ISNULL(T.monUnitCost,0) * (ISNULL(OI.fltDiscount,0) / 100)))
																	ELSE (ISNULL(T.monUnitCost,0) -ISNULL(OI.fltDiscount,0)) 
																END))  
					,monTotAmtBefDiscount = ISNULL(T.numUnits,0) * ISNULL(T.monUnitCost,0)
				FROM
					OpportunityMaster OM
				INNER JOIN
					OpportunityItems OI
				ON
					OM.numOppId = OI.numOppId
				INNER JOIN
					@TEMP T
				ON
					OI.numOppId = T.numOppID
					AND OI.numoppitemtCode = T.numOppItemID
				WHERE
					OM.numDomainId = @numDomainID
					AND OM.numOppId = @numOppID
					AND OM.numDivisionId = T.numSelectedVendorID

				EXEC USP_UpdatingInventoryonCloseDeal @numOppID=@numOppID,@numOppItemId=0,@numUserCntID=@numUserCntID

				IF EXISTS (SELECT numOppId FROM OpportunityMaster WHERE numDomainId=@numDomainID AND numOppId=@numOppID) AND NOT EXISTS (SELECT numoppitemtCode FROM OpportunityItems OI WHERE OI.numOppId=@numOppID)
				BEGIN
					EXEC USP_DeleteOppurtunity @numOppId=@numOppID,@numDomainID=@numDomainID,@numUserCntID=@numUserCntID
				END
				ELSE
				BEGIN
					UPDATE OpportunityMaster SET monDealAmount=dbo.GetDealAmount(@numOppID,GETDATE(),0),bintModifiedDate=GETUTCDATE(),numModifiedBy=@numUserCntID WHERE numOppId=@numOppID

					--INSERT TAX FOR DIVISION   
					IF (select ISNULL(bitPurchaseTaxCredit,0) from Domain where numDomainId=@numDomainId)=1
					BEGIN
						--Delete Tax for Opportunity Items if item deleted 
						DELETE FROM OpportunityItemsTaxItems WHERE numOppId=@numOppId AND numOppItemID NOT IN (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId=@numOppID)

						--Insert Tax for Opportunity Items
						INSERT INTO dbo.OpportunityItemsTaxItems 
						(
							numOppId,
							numOppItemID,
							numTaxItemID,
							numTaxID
						) 
						SELECT 
							@numOppId,
							OI.numoppitemtCode,
							TI.numTaxItemID,
							0
						FROM 
							dbo.OpportunityItems OI 
						JOIN 
							dbo.ItemTax IT 
						ON 
							OI.numItemCode=IT.numItemCode 
						JOIN
							TaxItems TI 
						ON 
							TI.numTaxItemID = IT.numTaxItemID 
						WHERE 
							OI.numOppId=@numOppID 
							AND IT.bitApplicable=1 
							AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)
						UNION
						SELECT 
							@numOppId,
							OI.numoppitemtCode,
							0,
							0
						FROM 
							dbo.OpportunityItems OI 
						JOIN 
							dbo.Item I 
						ON 
							OI.numItemCode=I.numItemCode
						WHERE 
							OI.numOppId=@numOppID 
							AND I.bitTaxable=1 
							AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)
						UNION
						SELECT
							@numOppId,
							OI.numoppitemtCode,
							1,
							TD.numTaxID
						FROM
							dbo.OpportunityItems OI 
						INNER JOIN
							ItemTax IT
						ON
							IT.numItemCode = OI.numItemCode
						INNER JOIN
							TaxDetails TD
						ON
							TD.numTaxID = IT.numTaxID
							AND TD.numDomainId = @numDomainId
						WHERE
							OI.numOppId = @numOppID
							AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)
					END
				END

				SET @i = @i + 1
			END

			DECLARE @TEMPVendor TABLE
			(
				ID INT IDENTITY(1,1)
				,numVendorID NUMERIC(18,0)
				,numContactID NUMERIC(18,0)
				,intBillingDays INT
			)

			INSERT INTO @TEMPVendor
			(
				numVendorID
				,numContactID
				,intBillingDays
			)
			SELECT DISTINCT
				numVendorID
				,ISNULL((SELECT TOP 1 numContactId FROM AdditionalContactsInformation WHERE numDivisionId=numVendorID ORDER BY ISNULL(bitPrimaryContact,0) DESC),0)
				,ISNULL(numBillingDays,0)
			FROM
				@TEMPVendorItems T
			INNER JOIN
				DivisionMaster
			ON
				T.numVendorID = DivisionMaster.numDivisionID

			DECLARE @j INT = 1
			DECLARE @jCount INT
			DECLARE @numVendorID NUMERIC(18,0)
			DECLARE @numContactID NUMERIC(18,0)
			DECLARE @numCurrencyID NUMERIC(18,0)
			DECLARE @intBillingDays INT
			DECLARE @strItems NVARCHAR(MAX) = ''
			DECLARE @dtEstimatedCloseDate DATETIME

			SELECT @numCurrencyID = ISNULL(numCurrencyID,0) FROM Domain WHERE numDomainId = @numDomainID

			SELECT @jCount=COUNT(*) FROM @TEMPVendor

			WHILE @j <= @jCount
			BEGIN
				SELECT @numVendorID=numVendorID,@numContactID=numContactID,@intBillingDays=intBillingDays FROM @TEMPVendor WHERE ID = @j

				SET @strItems =(
									SELECT 
										0 AS numoppitemtCode
										,T.numItemCode
										,CAST(numUnitHour AS VARCHAR) AS numUnitHour
										,CAST(monPrice AS VARCHAR)
										,CAST(ISNULL(T.numUnitHour,0) * ISNULL(T.monPrice,0) AS VARCHAR) monTotAmount
										,ISNULL(I.txtItemDesc,'')
										,T.numWarehouseItemID AS numWarehouseItmsID
										,0 AS numToWarehouseItemID
										,1 AS Op_Flag
										,I.charItemType AS  ItemType
										,0 AS DropShip
										,0 AS bitDiscountType
										,0 AS fltDiscount
										,CAST(ISNULL(T.numUnitHour,0) * ISNULL(T.monPrice,0) AS VARCHAR) AS monTotAmtBefDiscount
										,vcItemName
										,I.numBaseUnit AS numUOM
										,0 AS bitWorkOrder
										,0 AS numVendorWareHouse
										,0 AS numShipmentMethod
										,0
										,0 AS numProjectID
										,0 AS numProjectStageID
										,'' Attributes
										,0 AS bitItemPriceApprovalRequired
									FROM 
										@TEMPVendorItems T
									INNER JOIN
										Item I
									ON
										T.numItemCode = I.numItemCode
									WHERE
										T.numVendorID = @numVendorID
									FOR XML RAW ('Item'), ROOT ('NewDataSet'), ELEMENTS
								)
		
				SET @dtEstimatedCloseDate = DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,GETUTCDATE())

				--UPDATE MASTER PO AND ADD MERGE PO ITEMS
				EXEC USP_OppManage @numOppID=0,@numContactId=@numContactId,@numDivisionId=@numVendorID,@tintSource=0,@vcPOppName='',
									@Comments='',@bitPublicFlag=0,@numUserCntID=@numUserCntID,@monPAmount=0,@numAssignedTo=@numUserCntID,
									@numDomainId=@numDomainId,@strItems=@strItems,@strMilestone='',@dtEstimatedCloseDate=@dtEstimatedCloseDate,@CampaignID=0,
									@lngPConclAnalysis=0,@tintOppType=2,@tintActive=1,@numSalesOrPurType=0,
									@numRecurringId=0,@numCurrencyID=@numCurrencyID,@DealStatus=1,@numStatus=0,@vcOppRefOrderNo='',
									@numBillAddressId=0,@numShipAddressId=0,@bitStockTransfer=0,@WebApiId=0,
									@tintSourceType=0,@bitDiscountType=0,@fltDiscount=0,@bitBillingTerms=0,
									@intBillingDays=@intBillingDays,@bitInterestType=0,@fltInterest=0,@tintTaxOperator=0,
									@numDiscountAcntType=0,@vcWebApiOrderNo='',@vcCouponCode='',@vcMarketplaceOrderID=0,
									@vcMarketplaceOrderDate=NULL,@vcMarketplaceOrderReportId='',@numPercentageComplete=0,
									@bitUseShippersAccountNo=0,@bitUseMarkupShippingRate=0,
									@numMarkupShippingRate=0,@intUsedShippingCompany=0,@numShipmentMethod=0,@dtReleaseDate=NULL,@numPartner=0,@numPartenerContactId=0
									,@numAccountClass=0,@numVendorAddressID=0	

				SET @j = @j + 1
			END

		COMMIT
		END TRY
		BEGIN CATCH
			DECLARE @ErrorMessage NVARCHAR(4000)
			DECLARE @ErrorNumber INT
			DECLARE @ErrorSeverity INT
			DECLARE @ErrorState INT
			DECLARE @ErrorLine INT
			DECLARE @ErrorProcedure NVARCHAR(200)

			IF @@TRANCOUNT > 0
				ROLLBACK TRANSACTION;

			SELECT 
				@ErrorMessage = ERROR_MESSAGE(),
				@ErrorNumber = ERROR_NUMBER(),
				@ErrorSeverity = ERROR_SEVERITY(),
				@ErrorState = ERROR_STATE(),
				@ErrorLine = ERROR_LINE(),
				@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

			RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
		END CATCH	
	END
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ReportQueryExecute')
DROP PROCEDURE USP_ReportQueryExecute
GO
CREATE PROCEDURE [dbo].[USP_ReportQueryExecute]     
    @numDomainID NUMERIC(18,0),
    @numUserCntID NUMERIC(18,0),
	@numReportID NUMERIC(18,0),
    @ClientTimeZoneOffset INT, 
    @textQuery NTEXT,
	@numCurrentPage NUMERIC(18,0)
AS                 
BEGIN
	DECLARE @tintReportType INT
	DECLARE @numReportModuleID INT
	SELECT @tintReportType=ISNULL(tintReportType,0),@numReportModuleID=ISNULL(numReportModuleID,0) FROM ReportListMaster WHERE numReportID=@numReportID

	DECLARE @avgCost int
	SET @avgCost =(select TOP 1 numCost from Domain where numDOmainId=@numDomainID)

	IF CHARINDEX('WareHouseItems.numBackOrder',CAST(@textQuery AS VARCHAR(MAX))) > 0 AND @numReportModuleID <> 6
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'WareHouseItems.numBackOrder','(CASE WHEN ISNULL((SELECT SUM(WIInner.numOnHand) FROM WareHouseItems WIInner WHERE WIInner.numItemID=WareHouseItems.numItemID AND WIInner.numWareHouseItemID <> WareHouseItems.numWareHouseItemID AND WIInner.numWareHouseID = WareHouseItems.numWareHouseID),0) >= WareHouseItems.numBackOrder THEN 0 ELSE ISNULL(WareHouseItems.numBackOrder,0) END)')
	END
	
	IF CHARINDEX('isnull(OpportunityItems.numRemainingQtyToPick,0)',CAST(@textQuery AS VARCHAR(MAX))) > 0	
	BEGIN	
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'isnull(OpportunityItems.numRemainingQtyToPick,0)','(isnull(OpportunityItems.numUnitHour,0) - isnull(OpportunityItems.numQtyPicked,0))')	
	END	

	IF CHARINDEX('OpportunityItems.numRemainingQtyToPick',CAST(@textQuery AS VARCHAR(MAX))) > 0	
	BEGIN	
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'OpportunityItems.numRemainingQtyToPick','(isnull(OpportunityItems.numUnitHour,0) - isnull(OpportunityItems.numQtyPicked,0))')	
	END		

	IF CHARINDEX('isnull(OpportunityItems.numRemainingQtyToShip,0)',CAST(@textQuery AS VARCHAR(MAX))) > 0	
	BEGIN	
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'isnull(OpportunityItems.numRemainingQtyToShip,0)','(isnull(OpportunityItems.numUnitHour,0) - isnull(OpportunityItems.numQtyShipped,0))')	
	END	

	IF CHARINDEX('OpportunityItems.numRemainingQtyToShip',CAST(@textQuery AS VARCHAR(MAX))) > 0	
	BEGIN	
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'OpportunityItems.numRemainingQtyToShip','(isnull(OpportunityItems.numUnitHour,0) - isnull(OpportunityItems.numQtyShipped,0))')	
	END	

	IF CHARINDEX('isnull(OpportunityItems.numRemainingQtyToInvoice,0)',CAST(@textQuery AS VARCHAR(MAX))) > 0	
	BEGIN	
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'isnull(OpportunityItems.numRemainingQtyToInvoice,0)','(isnull(OpportunityItems.numUnitHour,0) - isnull((SELECT SUM(numUnitHour) FROM OpportunityBizDocItems INNER JOIN OpportunityBizDocs ON OpportunityBizDocItems.numOppBizDocID = OpportunityBizDocs.numOppBizDocsId  WHERE OpportunityBizDocs.numOppId = OpportunityItems.numOppId AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtCode AND ISNULL(bitAuthoritativeBizDocs, 0) = 1 AND ISNULL(OpportunityBizDocs.numBizDocID,0) = 287),0))')	
	END	

	IF CHARINDEX('OpportunityItems.numRemainingQtyToInvoice',CAST(@textQuery AS VARCHAR(MAX))) > 0	
	BEGIN	
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'OpportunityItems.numRemainingQtyToInvoice','(isnull(OpportunityItems.numUnitHour,0) - isnull((SELECT SUM(numUnitHour) FROM OpportunityBizDocItems INNER JOIN OpportunityBizDocs ON OpportunityBizDocItems.numOppBizDocID = OpportunityBizDocs.numOppBizDocsId  WHERE OpportunityBizDocs.numOppId = OpportunityItems.numOppId AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtCode AND ISNULL(bitAuthoritativeBizDocs, 0) = 1 AND ISNULL(OpportunityBizDocs.numBizDocID,0) = 287),0))')	
	END	

	IF CHARINDEX('AND OpportunityItems.monPendingSales',CAST(@textQuery AS VARCHAR(MAX))) > 0	
	BEGIN	
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'AND OpportunityItems.monPendingSales','AND (ISNULL((ISNULL(ISNULL(dbo.fn_UOMConversion(ISNULL(Item.numBaseUnit, 0), OpportunityItems.numItemCode, Item.numDomainId, ISNULL(OpportunityItems.numUOMId, 0)), 1) * OpportunityItems.numUnitHour, 0) - isnull(OpportunityItems.numQtyShipped, 0) ) * (isnull(OpportunityItems.monPrice, 0)), 0))')	
	END	
	
	IF CHARINDEX('AND OpportunityItems.numPendingToBill',CAST(@textQuery AS VARCHAR(MAX))) > 0	
	BEGIN	
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'AND OpportunityItems.numPendingToBill','AND ISNULL((isnull(OpportunityItems.numUnitHourReceived, 0)) - (ISNULL(( SELECT SUM(numUnitHour)  FROM	 OpportunityBizDocItems  INNER JOIN	OpportunityBizDocs ON OpportunityBizDocItems.numOppBizDocID = OpportunityBizDocs.numOppBizDocsId  WHERE OpportunityBizDocs.numOppId = OpportunityItems.numOppId AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtCode AND ISNULL(bitAuthoritativeBizDocs, 0) = 1), 0)), 0)')	
	END	
		
	IF CHARINDEX('AND OpportunityItems.numPendingtoReceive',CAST(@textQuery AS VARCHAR(MAX))) > 0	
	BEGIN	
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'AND OpportunityItems.numPendingtoReceive','AND isnull((isnull((SELECT SUM(numUnitHour) FROM OpportunityBizDocItems INNER JOIN OpportunityBizDocs ON OpportunityBizDocItems.numOppBizDocID = OpportunityBizDocs.numOppBizDocsId WHERE OpportunityBizDocs.numOppId = OpportunityItems.numOppId AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtCode AND ISNULL(bitAuthoritativeBizDocs, 0) = 1), 0)) - (isnull(OpportunityItems.numUnitHourReceived, 0)), 0)')	
	END

	IF CHARINDEX('AND WareHouseItems.vcLocation',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'AND WareHouseItems.vcLocation','AND WareHouseItems.numWLocationID')
	END

	IF CHARINDEX('ReturnItems.monPrice',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'ReturnItems.monPrice','CAST(ReturnItems.monPrice AS NUMERIC(18,4))')
	END

	IF CHARINDEX('OpportunityItems.monPrice',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'OpportunityItems.monPrice','CAST(OpportunityItems.monPrice AS NUMERIC(18,4))')
	END

	IF CHARINDEX('isnull(category.numCategoryID,0)',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'isnull(category.numCategoryID,0)' ,'(SELECT ISNULL(STUFF((SELECT '','' + CONVERT(VARCHAR(50) , Category.vcCategoryName) FROM ItemCategory LEFT JOIN Category ON ItemCategory.numCategoryID = Category.numCategoryID WHERE ItemCategory.numItemID = Item.numItemCode FOR XML PATH('''')), 1, 1, ''''),''''))')
	END

	IF CHARINDEX('dbo.FormatedDateFromDate(dateadd(minute,-@ClientTimeZoneOffset,OpportunityBizDocs.dtFullPaymentDate),@numDomainId)',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'dbo.FormatedDateFromDate(dateadd(minute,-@ClientTimeZoneOffset,OpportunityBizDocs.dtFullPaymentDate),@numDomainId)' ,'(CASE 
																																															WHEN ISNULL(OpportunityMaster.tintOppType,0) = 2 AND ISNULL(OpportunityBizDocs.monDealAmount,0) > 0 AND ISNULL(OpportunityBizDocs.monAmountPaid,0) >= ISNULL(OpportunityBizDocs.monDealAmount,0)
																																															THEN dbo.FormatedDateFromDate((SELECT MAX(BPH.dtPaymentDate) FROM BillPaymentDetails BPD INNER JOIN BillPaymentHeader BPH ON BPD.numBillPaymentID=BPH.numBillPaymentID  WHERE BPD.numOppBizDocsID=OpportunityBizDocs.numOppBizDocsId),@numDomainID)
																																															WHEN ISNULL(OpportunityMaster.tintOppType,0) = 1 AND ISNULL(OpportunityBizDocs.monDealAmount,0) > 0 AND ISNULL(OpportunityBizDocs.monAmountPaid,0) >= ISNULL(OpportunityBizDocs.monDealAmount,0)
																																															THEN dbo.FormatedDateFromDate((SELECT MAX(dtCreatedDate) FROM DepositeDetails WHERE DepositeDetails.numOppBizDocsID=OpportunityBizDocs.numOppBizDocsId),@numDomainID)
																																															ELSE ''''
																																														END)')
	END

	--KEEP IT BELOW ABOVE REPLACEMENT
	IF CHARINDEX('OpportunityBizDocs.dtFullPaymentDate',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'OpportunityBizDocs.dtFullPaymentDate' ,'CAST((CASE 
																												WHEN ISNULL(OpportunityMaster.tintOppType,0) = 2 AND ISNULL(OpportunityBizDocs.monDealAmount,0) > 0 AND ISNULL(OpportunityBizDocs.monAmountPaid,0) >= ISNULL(OpportunityBizDocs.monDealAmount,0)
																												THEN (SELECT MAX(BPH.dtPaymentDate) FROM BillPaymentDetails BPD INNER JOIN BillPaymentHeader BPH ON BPD.numBillPaymentID=BPH.numBillPaymentID  WHERE BPD.numOppBizDocsID=OpportunityBizDocs.numOppBizDocsId)
																												WHEN ISNULL(OpportunityMaster.tintOppType,0) = 1 AND ISNULL(OpportunityBizDocs.monDealAmount,0) > 0 AND ISNULL(OpportunityBizDocs.monAmountPaid,0) >= ISNULL(OpportunityBizDocs.monDealAmount,0)
																												THEN (SELECT MAX(dtCreatedDate) FROM DepositeDetails WHERE DepositeDetails.numOppBizDocsID=OpportunityBizDocs.numOppBizDocsId)
																												ELSE ''''
																											END) AS DATE)')
	END

	IF CHARINDEX('isnull(OpportunityMaster.monProfit,''0'')',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'isnull(OpportunityMaster.monProfit,''0'')' ,CONCAT('CAST(ISNULL(OpportunityItems.monTotAmount,0) - (ISNULL(OpportunityItems.numUnitHour,0) * (CASE ',ISNULL(@avgCost,1),' WHEN 3 THEN ISNULL(OpportunityItems.monVendorCost,0) * dbo.fn_UOMConversion(Item.numBaseUnit,Item.numItemCode,@numDomainID,Item.numPurchaseUnit) WHEN 2 THEN ISNULL(OpportunityItems.numCost,0) * dbo.fn_UOMConversion(Item.numBaseUnit,Item.numItemCode,@numDomainID,OpportunityItems.numUOMId) ELSE ISNULL(OpportunityItems.monAvgCost,0) END)) AS DECIMAL(20,5))'))
	END

	-- THIS ONE SHOULD BE AFTER ABOVE ONE
	IF CHARINDEX('OpportunityMaster.monProfit',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'OpportunityMaster.monProfit' ,CONCAT('CAST(ISNULL(OpportunityItems.monTotAmount,0) - (ISNULL(OpportunityItems.numUnitHour,0) * (CASE ',ISNULL(@avgCost,1),' WHEN 3 THEN ISNULL(OpportunityItems.monVendorCost,0) * dbo.fn_UOMConversion(Item.numBaseUnit,Item.numItemCode,@numDomainID,Item.numPurchaseUnit) WHEN 2 THEN ISNULL(OpportunityItems.numCost,0) * dbo.fn_UOMConversion(Item.numBaseUnit,Item.numItemCode,@numDomainID,OpportunityItems.numUOMId) ELSE ISNULL(OpportunityItems.monAvgCost,0) END)) AS DECIMAL(20,5))'))
	END

	IF CHARINDEX('isnull(OpportunityBizDocItems.monProfit,''0'')',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'isnull(OpportunityBizDocItems.monProfit,''0'')' ,CONCAT('CAST(ISNULL(OpportunityBizDocItems.monTotAmount,0) - (ISNULL(OpportunityBizDocItems.numUnitHour,0) * (CASE ',ISNULL(@avgCost,1),' WHEN 3 THEN ISNULL(OpportunityItems.monVendorCost,0) * dbo.fn_UOMConversion(Item.numBaseUnit,Item.numItemCode,@numDomainID,Item.numPurchaseUnit) WHEN 2 THEN ISNULL(OpportunityItems.numCost,0) * dbo.fn_UOMConversion(Item.numBaseUnit,Item.numItemCode,@numDomainID,OpportunityItems.numUOMId) ELSE ISNULL(OpportunityBizDocItems.monAverageCost,ISNULL(OpportunityItems.monAvgCost,0)) END)) AS DECIMAL(20,5))'))
	END

	-- THIS ONE SHOULD BE AFTER ABOVE ONE
	IF CHARINDEX('OpportunityBizDocItems.monProfit',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'OpportunityBizDocItems.monProfit' ,CONCAT('CAST(ISNULL(OpportunityBizDocItems.monTotAmount,0) - (ISNULL(OpportunityBizDocItems.numUnitHour,0) * (CASE ',ISNULL(@avgCost,1),' WHEN 3 THEN ISNULL(OpportunityItems.monVendorCost,0) * dbo.fn_UOMConversion(Item.numBaseUnit,Item.numItemCode,@numDomainID,Item.numPurchaseUnit) WHEN 2 THEN ISNULL(OpportunityItems.numCost,0) * dbo.fn_UOMConversion(Item.numBaseUnit,Item.numItemCode,@numDomainID,OpportunityItems.numUOMId) ELSE ISNULL(OpportunityBizDocItems.monAverageCost,ISNULL(OpportunityItems.monAvgCost,0)) END)) AS DECIMAL(20,5))'))
	END

	IF CHARINDEX('ISNULL(PT.numItemCode,0)=Item.numItemCode',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'ISNULL(PT.numItemCode,0)=Item.numItemCode' ,'Item.numItemCode=PT.numItemCode')
	END

	IF CHARINDEX('isnull(OpportunityMaster.numProfitPercent,0)',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'isnull(OpportunityMaster.numProfitPercent,0)' ,CONCAT('CAST((((ISNULL(OpportunityItems.monTotAmount,0)/ISNULL(NULLIF(OpportunityItems.numUnitHour,0),1)) - (CASE ',ISNULL(@avgCost,1),' WHEN 3 THEN ISNULL(OpportunityItems.monVendorCost,0) * dbo.fn_UOMConversion(Item.numBaseUnit,Item.numItemCode,@numDomainID,Item.numPurchaseUnit) WHEN 2 THEN ISNULL(OpportunityItems.numCost,0) * dbo.fn_UOMConversion(Item.numBaseUnit,Item.numItemCode,@numDomainID,OpportunityItems.numUOMId) ELSE ISNULL(OpportunityItems.monAvgCost,0) END)) / (CASE WHEN (ISNULL(OpportunityItems.monTotAmount,0)/ISNULL(NULLIF(OpportunityItems.numUnitHour,0),1))=0 then 1 ELSE (ISNULL(OpportunityItems.monTotAmount,0)/ISNULL(NULLIF(OpportunityItems.numUnitHour,0),1)) end) * 100) AS NUMERIC(18,2))'))
	END
	
	-- THIS ONE SHOULD BE AFTER ABOVE ONE
	IF CHARINDEX('OpportunityMaster.numProfitPercent',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'OpportunityMaster.numProfitPercent' ,CONCAT('CAST((((ISNULL(OpportunityItems.monTotAmount,0)/ISNULL(NULLIF(OpportunityItems.numUnitHour,0),1)) - (CASE ',ISNULL(@avgCost,1),' WHEN 3 THEN ISNULL(OpportunityItems.monVendorCost,0) * dbo.fn_UOMConversion(Item.numBaseUnit,Item.numItemCode,@numDomainID,Item.numPurchaseUnit) WHEN 2 THEN ISNULL(OpportunityItems.numCost,0) * dbo.fn_UOMConversion(Item.numBaseUnit,Item.numItemCode,@numDomainID,OpportunityItems.numUOMId) ELSE ISNULL(OpportunityItems.monAvgCost,0) END)) / (CASE WHEN (ISNULL(OpportunityItems.monTotAmount,0)/ISNULL(NULLIF(OpportunityItems.numUnitHour,0),1))=0 then 1 ELSE (ISNULL(OpportunityItems.monTotAmount,0)/ISNULL(NULLIF(OpportunityItems.numUnitHour,0),1)) end) * 100) AS NUMERIC(18,2))'))
	END

	IF CHARINDEX('isnull(OpportunityBizDocItems.numProfitPercent,0)',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'isnull(OpportunityBizDocItems.numProfitPercent,0)' ,CONCAT('CAST((((ISNULL(OpportunityBizDocItems.monTotAmount,0)/ISNULL(NULLIF(OpportunityBizDocItems.numUnitHour,0),1)) - (CASE ',ISNULL(@avgCost,1),' WHEN 3 THEN ISNULL(OpportunityItems.monVendorCost,0) * dbo.fn_UOMConversion(Item.numBaseUnit,Item.numItemCode,@numDomainID,Item.numPurchaseUnit) WHEN 2 THEN ISNULL(OpportunityItems.numCost,0) * dbo.fn_UOMConversion(Item.numBaseUnit,Item.numItemCode,@numDomainID,OpportunityItems.numUOMId) ELSE ISNULL(OpportunityBizDocItems.monAverageCost,ISNULL(OpportunityItems.monAvgCost,0)) END)) / (CASE WHEN (ISNULL(OpportunityBizDocItems.monTotAmount,0)/ISNULL(NULLIF(OpportunityBizDocItems.numUnitHour,0),1))=0 then 1 ELSE (ISNULL(OpportunityBizDocItems.monTotAmount,0)/ISNULL(NULLIF(OpportunityBizDocItems.numUnitHour,0),1)) end) * 100) AS NUMERIC(18,2))'))
	END
	
	-- THIS ONE SHOULD BE AFTER ABOVE ONE
	IF CHARINDEX('OpportunityBizDocItems.numProfitPercent',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'OpportunityBizDocItems.numProfitPercent' ,CONCAT('CAST((((ISNULL(OpportunityBizDocItems.monTotAmount,0)/ISNULL(NULLIF(OpportunityBizDocItems.numUnitHour,0),1)) - (CASE ',ISNULL(@avgCost,1),' WHEN 3 THEN ISNULL(OpportunityItems.monVendorCost,0) * dbo.fn_UOMConversion(Item.numBaseUnit,Item.numItemCode,@numDomainID,Item.numPurchaseUnit) WHEN 2 THEN ISNULL(OpportunityItems.numCost,0) * dbo.fn_UOMConversion(Item.numBaseUnit,Item.numItemCode,@numDomainID,OpportunityItems.numUOMId) ELSE ISNULL(OpportunityBizDocItems.monAverageCost,ISNULL(OpportunityItems.monAvgCost,0)) END)) / (CASE WHEN (ISNULL(OpportunityBizDocItems.monTotAmount,0)/ISNULL(NULLIF(OpportunityBizDocItems.numUnitHour,0),1))=0 then 1 ELSE (ISNULL(OpportunityBizDocItems.monTotAmount,0)/ISNULL(NULLIF(OpportunityBizDocItems.numUnitHour,0),1)) end) * 100) AS NUMERIC(18,2))'))
	END

	IF ISNULL(@numCurrentPage,0) > 0 
		AND ISNULL(@tintReportType,0) <> 3 
		AND ISNULL(@tintReportType,0) <> 4 
		AND ISNULL(@tintReportType,0) <> 5 
		AND CHARINDEX('Select top',CAST(@textQuery AS VARCHAR(MAX))) <> 1
		AND (SELECT COUNT(*) FROM ReportSummaryGroupList WHERE numReportID=@numReportID) = 0
	BEGIN
		SET @textQuery = CONCAT(CAST(@textQuery AS VARCHAR(MAX)),' OFFSET ',(@numCurrentPage-1) * 100 ,' ROWS FETCH NEXT 100 ROWS ONLY OPTION (OPTIMIZE FOR (@numDomainID UNKNOWN));')
	END
	ELSE
	BEGIN
		SET @textQuery = CONCAT(CAST(@textQuery AS VARCHAR(MAX)),' OPTION (OPTIMIZE FOR (@numDomainID UNKNOWN));')
	END
	 
	 print CAST(@textQuery AS ntext)
	EXECUTE sp_executesql @textQuery,N'@numDomainID numeric(18, 0),@numUserCntID numeric(18,0),@ClientTimeZoneOffset int',@numDomainID=@numDomainID,@numUserCntID=@numUserCntID,@ClientTimeZoneOffset=@ClientTimeZoneOffset
END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetCompanyDetailsFrom850')
DROP PROCEDURE dbo.USP_GetCompanyDetailsFrom850
GO

CREATE PROCEDURE [dbo].[USP_GetCompanyDetailsFrom850]
	@numDivisionId NUMERIC(18,0)
AS 
BEGIN
	SELECT 
		ISNULL(DM.numAssignedTo,ACI.numContactId) AS numContactID
		,DM.numDivisionID
		,ISNULL(DMEmployer.tintInbound850PickItem,0) AS tintInbound850PickItem
		,CI.numCompanyId
		,CI.vcCompanyName
	FROM
		DivisionMaster DM
	INNER JOIN 
		CompanyInfo CI
	ON 
		DM.numCompanyID = CI.numCompanyId
	INNER JOIN
		AdditionalContactsInformation ACI 
	ON
		DM.numDivisionID = ACI.numDivisionId
	INNER JOIN 
		Domain D
	ON
		DM.numDomainID = D.numDomainID
	INNER JOIN
		DivisionMaster DMEmployer
	ON
		D.numDivisionID = DMEmployer.numDivisionID
	WHERE
		DM.numDivisionID = @numDivisionId
		AND ISNULL(ACI.bitPrimaryContact,0)=1
END
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetItemPriceBasedOnMethod')
DROP PROCEDURE USP_GetItemPriceBasedOnMethod
GO
CREATE PROCEDURE [dbo].[USP_GetItemPriceBasedOnMethod]
	@numDomainID NUMERIC(18,0),
	@numDivisionID NUMERIC(18,0),
	@tintOppType AS TINYINT,
	@numItemCode NUMERIC(18,0),
	@tintPriceMethod TINYINT,
	@monListPrice AS DECIMAL(20,5),
	@monVendorCost AS DECIMAL(20,5),
	@numQty FLOAT,
	@fltUOMConversionFactor AS FLOAT
AS
BEGIN
	DECLARE @tintPriceLevel AS TINYINT
	DECLARE @tintRuleType AS TINYINT
	DECLARE @tintDisountType AS TINYINT
	DECLARE @decDiscount AS FLOAT
	DECLARE @monPrice AS DECIMAL(20,5) = 0
	DECLARE @monFinalPrice AS DECIMAL(20,5) = NULL
	DECLARE @numRelationship NUMERIC(18,0)
	DECLARE @numProfile NUMERIC(18,0)
	DECLARE @numItemClassification AS NUMERIC(18,0)
	DECLARE @tintPriceBookDiscount AS TINYINT

	SELECT @numItemClassification=numItemClassification FROM Item WHERE numItemCode=@numItemCode

	SELECT @tintPriceBookDiscount=tintPriceBookDiscount FROM Domain WHERE numDomainId=@numDomainID

	-- GET ORGANIZATION DETAL
	SELECT 
		@numRelationship=numCompanyType,
		@numProfile=vcProfile,
		@tintPriceLevel = ISNULL(D.tintPriceLevel,0)
	FROM 
		DivisionMaster D                  
	JOIN 
		CompanyInfo C 
	ON 
		C.numCompanyId=D.numCompanyID                  
	WHERE 
		numDivisionID =@numDivisionID       

	IF @tintPriceMethod= 1 -- PRICE LEVEL
	BEGIN
		IF (SELECT COUNT(*) FROM PricingTable WHERE numItemCode=@numItemCode AND ISNULL(numCurrencyID,0) = 0 AND ISNULL(numPriceRuleID,0) = 0) > 0
		BEGIN
			SELECT
				@tintDisountType = 1,
				@decDiscount = 0,
				@tintRuleType = tintRuleType,
				@monPrice = (CASE tintRuleType
										WHEN 1 -- Deduct From List Price
										THEN
											CASE tintDiscountType 
												WHEN 1 -- PERCENT
												THEN (CASE WHEN @monListPrice > 0 THEN @monListPrice - (@monListPrice * (ISNULL(decDiscount,0)/100)) ELSE 0 END)
												WHEN 2 -- FLAT AMOUNT
												THEN (CASE WHEN @monListPrice > 0 THEN (CASE WHEN (@monListPrice * @numQty * @fltUOMConversionFactor) - ISNULL(decDiscount,0) > 0 THEN ((@monListPrice * @numQty * @fltUOMConversionFactor) - ISNULL(decDiscount,0)) / (@numQty * @fltUOMConversionFactor) ELSE 0 END) ELSE 0 END)
												WHEN 3 -- NAMED PRICE
												THEN ISNULL(decDiscount,0)
											END
										WHEN 2 -- Add to primary vendor cost
										THEN
											CASE tintDiscountType 
												WHEN 1  -- PERCENT
												THEN @monVendorCost + (@monVendorCost * (ISNULL(decDiscount,0)/100)) * @fltUOMConversionFactor
												WHEN 2 -- FLAT AMOUNT
												THEN (CASE WHEN (@monVendorCost * @numQty * @fltUOMConversionFactor) + ISNULL(decDiscount,0) > 0 THEN ((@monVendorCost * @numQty * @fltUOMConversionFactor) + ISNULL(decDiscount,0)) / (@numQty * @fltUOMConversionFactor)  ELSE 0 END)
												WHEN 3 -- NAMED PRICE
												THEN ISNULL(decDiscount,0)
											END
										WHEN 3 -- Named price
										THEN
											ISNULL(decDiscount,0)
										END
									) 
			FROM
			(
				SELECT 
					ROW_NUMBER() OVER(PARTITION BY PricingTable.numItemCode ORDER BY numPricingID) Id,
					*
				FROM
					PricingTable
				WHERE 
					PricingTable.numItemCode = @numItemCode
					AND ISNULL(PricingTable.numPriceRuleID,0) = 0
					AND ISNULL(numCurrencyID,0) = 0 
			) TEMP
			WHERE
				1 = (CASE 
						WHEN ISNULL(@tintPriceLevel,0) > 0 
						THEN (CASE WHEN Id=@tintPriceLevel THEN 1 ELSE 0 END) 
						ELSE (CASE WHEN ((@numQty * @fltUOMConversionFactor) >= intFromQty AND (@numQty * @fltUOMConversionFactor) <= intToQty) THEN 1 ELSE 0 END)
					END)
		END

		SELECT 
			ISNULL(@monPrice,@monListPrice) AS monPrice,
			ISNULL(@tintRuleType,1) AS tintRuleType,
			1 AS tintDisountType,
			0 AS decDiscount
	END
	ELSE IF @tintPriceMethod= 2 AND (SELECT COUNT(*) FROM PriceBookRules WHERE numDomainID=@numDomainID AND tintRuleFor=@tintOppType) > 0 -- PRICE RULE
	BEGIN
		DECLARE @numPriceRuleID NUMERIC(18,0)
		DECLARE @tintPriceRuleType AS TINYINT
		DECLARE @tintPricingMethod TINYINT
		DECLARE @tintPriceBookDiscountType TINYINT
		DECLARE @decPriceBookDiscount FLOAT
		DECLARE @intQntyItems INT
		DECLARE @decMaxDedPerAmt FLOAT

		SELECT 
			TOP 1
			@numPriceRuleID = numPricRuleID,
			@tintPricingMethod = tintPricingMethod,
			@tintPriceRuleType = p.tintRuleType,
			@tintPriceBookDiscountType = P.[tintDiscountType],
			@decPriceBookDiscount = P.[decDiscount],
			@intQntyItems = P.[intQntyItems],
			@decMaxDedPerAmt = P.[decMaxDedPerAmt]
		FROM   
			PriceBookRules P 
		LEFT JOIN 
			PriceBookRuleDTL PDTL 
		ON 
			P.numPricRuleID = PDTL.numRuleID
		LEFT JOIN 
			PriceBookRuleItems PBI 
		ON 
			P.numPricRuleID = PBI.numRuleID
		LEFT JOIN 
			[PriceBookPriorities] PP 
		ON 
			PP.[Step2Value] = P.[tintStep2] 
			AND PP.[Step3Value] = P.[tintStep3]
		WHERE  
			P.numDomainID=@numDomainID 
			AND tintRuleFor=@tintOppType
			AND (
					((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 1 AND PDTL.numValue = @numDivisionID)) -- Priority 1
					OR ((tintStep2 = 2 AND PBI.numValue = @numItemClassification) AND (tintStep3= 1 AND PDTL.numValue = @numDivisionID)) -- Priority 2
					OR ((tintStep2 = 3) AND (tintStep3 = 1 AND PDTL.numValue = @numDivisionID)) -- Priority 3

					OR ((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 4
					OR ((tintStep2 = 2 AND PBI.numValue = @numItemClassification) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 5
					OR ((tintStep2 = 3) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 6

					OR ((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 3)) -- Priority 7
					OR ((tintStep2 = 2 AND PBI.numValue = @numItemClassification) AND (tintStep3 = 3)) -- Priority 8
					OR ((tintStep2 = 3) and (tintStep3 = 3)) -- Priority 9
				)
		ORDER BY 
			PP.Priority ASC


		IF ISNULL(@numPriceRuleID,0) > 0
		BEGIN
			IF @tintPricingMethod = 1 -- BASED ON PRICE TABLE
			BEGIN
				SELECT
					@tintDisountType = tintDiscountType,
					@decDiscount = ISNULL(decDiscount,0),
					@tintRuleType = tintRuleType,
					@monPrice = (CASE tintRuleType WHEN 1 THEN @monListPrice WHEN 2 THEN @monVendorCost END) ,
					@monFinalPrice = (CASE tintRuleType
											WHEN 1 -- Deduct From List Price
											THEN
												CASE tintDiscountType 
													WHEN 1 -- PERCENT
													THEN (CASE WHEN @monListPrice > 0 THEN @monListPrice - (@monListPrice * (ISNULL(decDiscount,0)/100)) ELSE 0 END)
													WHEN 2 -- FLAT AMOUNT
													THEN (CASE WHEN @monListPrice > 0 THEN (CASE WHEN (@monListPrice * @numQty * @fltUOMConversionFactor) - ISNULL(decDiscount,0) > 0 THEN ((@monListPrice * @numQty * @fltUOMConversionFactor) - ISNULL(decDiscount,0)) / (@numQty * @fltUOMConversionFactor) ELSE 0 END) ELSE 0 END)
													WHEN 3 -- NAMED PRICE
													THEN ISNULL(decDiscount,0)
												END
											WHEN 2 -- Add to primary vendor cost
											THEN
												CASE tintDiscountType 
													WHEN 1  -- PERCENT
													THEN @monVendorCost + (@monVendorCost * (ISNULL(decDiscount,0)/100))
													WHEN 2 -- FLAT AMOUNT
													THEN (CASE WHEN (@monVendorCost * @numQty * @fltUOMConversionFactor) + ISNULL(decDiscount,0) > 0 THEN ((@monVendorCost * @numQty * @fltUOMConversionFactor) + ISNULL(decDiscount,0)) / (@numQty * @fltUOMConversionFactor)  ELSE 0 END)
													WHEN 3 -- NAMED PRICE
													THEN ISNULL(decDiscount,0)
												END
											WHEN 3 -- Named price
											THEN
												CASE 
												WHEN ISNULL(@tintPriceLevel,0) > 0
												THEN
													(SELECT 
														 ISNULL(decDiscount,0)
													FROM
													(
														SELECT 
															ROW_NUMBER() OVER(PARTITION BY PricingTable.numItemCode ORDER BY numPricingID) Id,
															decDiscount
														FROM 
															PricingTable 
														WHERE 
															PricingTable.numItemCode = @numItemCode
															AND ISNULL(numCurrencyID,0) = 0
															AND tintRuleType = 3 
													) TEMP
													WHERE
														Id = @tintPriceLevel)
												ELSE
													ISNULL(decDiscount,0)
												END
											END
										) 
				FROM
					PricingTable
				WHERE
					numPriceRuleID=@numPriceRuleID 
					AND ISNULL(numCurrencyID,0) = 0
					AND @numQty * @fltUOMConversionFactor >= intFromQty AND @numQty * @fltUOMConversionFactor <= intToQty
			END
			ELSE IF @tintPricingMethod = 2 -- BASED ON RULE
			BEGIN
				SET @monPrice = (CASE WHEN @tintPriceRuleType = 2 THEN @monVendorCost ELSE @monListPrice END)

				IF (@tintPriceBookDiscountType = 1 ) -- Percentage 
				BEGIN
					SET @tintDisountType = @tintPriceBookDiscountType
					SET @decDiscount = @decPriceBookDiscount * (@numQty/@intQntyItems);
					SET @decDiscount = @decPriceBookDiscount * (CASE WHEN @tintPriceRuleType = 2 THEN @monVendorCost ELSE @monListPrice END) /100;
					SET @decMaxDedPerAmt = @decMaxDedPerAmt * (CASE WHEN @tintPriceRuleType = 2 THEN @monVendorCost ELSE @monListPrice END) /100;
					
					IF (@decDiscount > @decMaxDedPerAmt)
						SET @decDiscount = @decMaxDedPerAmt;
				
					IF ( @tintPriceRuleType = 1 )
							SET @monFinalPrice = (@monListPrice - @decDiscount);
					IF ( @tintPriceRuleType = 2 )
							SET @monFinalPrice = (@monVendorCost + @decDiscount);
				END
				IF ( @tintPriceBookDiscountType = 2 ) -- Flat discount 
				BEGIN
					SET @decDiscount = @decPriceBookDiscount * ((@numQty * @fltUOMConversionFactor)/@intQntyItems);

					IF (@decDiscount > @decMaxDedPerAmt)
						SET @decDiscount = @decMaxDedPerAmt;
					
					IF ( @tintPriceRuleType = 1 )
						SET @monFinalPrice =  ((@monListPrice * @numQty * @fltUOMConversionFactor) - @decDiscount) / (@numQty * @fltUOMConversionFactor);
						
					IF ( @tintPriceRuleType = 2 )
						SET @monFinalPrice = ((@monVendorCost * @numQty * @fltUOMConversionFactor) + @decDiscount) / (@numQty * @fltUOMConversionFactor);
				END
			END
		END
		
		SELECT
			(CASE WHEN @monFinalPrice IS NULL THEN @monListPrice ELSE (CASE WHEN @tintPriceBookDiscount = 1 THEN ISNULL(@monPrice,0) ELSE ISNULL(@monFinalPrice,0) END) END) AS monPrice,
			ISNULL(@tintRuleType,1) AS tintRuleType,
			(CASE WHEN @monFinalPrice IS NULL THEN 1 ELSE (CASE WHEN @tintPriceBookDiscount = 1 THEN ISNULL(@tintPriceBookDiscountType,1) ELSE 1 END) END) tintDisountType,
			(CASE WHEN @monFinalPrice IS NULL THEN 0 ELSE (CASE WHEN @tintPriceBookDiscount = 1 THEN ISNULL(@decPriceBookDiscount,0) ELSE 0 END) END) decDiscount
	END
	ELSE IF @tintPriceMethod = 3 -- Last Price
	BEGIN
		SELECT 
			TOP 1 
			@monPrice = monPrice
			,@tintDisountType = ISNULL(OpportunityItems.bitDiscountType,1)
			,@decDiscount = ISNULL(OpportunityItems.fltDiscount,0)
			,@tintRuleType = 3
		FROM 
			OpportunityItems 
		JOIN 
			OpportunityMaster 
		ON 
			OpportunityItems.numOppId = OpportunityMaster.numOppId 
		WHERE 
			OpportunityMaster.numDomainId= @numDomainID
			AND tintOppType = @tintOppType
			AND tintOppStatus = 1
			AND numItemCode=@numItemCode
		ORDER BY
			OpportunityMaster.numOppId DESC

		SELECT 
			(CASE WHEN @tintPriceBookDiscount = 1 THEN ISNULL(@monPrice,0) ELSE ISNULL(@monFinalPrice,0) END) AS monPrice,
			ISNULL(@tintRuleType,1) AS tintRuleType,
			(CASE WHEN @tintPriceBookDiscount = 1 THEN ISNULL(@tintDisountType,1) ELSE 1 END) AS tintDisountType,
			(CASE WHEN @tintPriceBookDiscount = 1 THEN ISNULL(@decDiscount,0) ELSE 0 END) AS decDiscount
	END
END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetOrderSelectedItemDetail')
DROP PROCEDURE USP_GetOrderSelectedItemDetail
GO
CREATE PROCEDURE [dbo].[USP_GetOrderSelectedItemDetail]
	@numDomainID NUMERIC(18,0),
	@numDivisionID NUMERIC(18,0),
	@tintOppType AS TINYINT,
	@numItemCode NUMERIC(18,0),
	@numUOMID NUMERIC(18,0),
	@numWarehouseItemID AS NUMERIC(18,0),
	@numQty INT,
	@monPrice FLOAT,
	@numOppID NUMERIC(18,0),
	@numOppItemID NUMERIC(18,0),
	@vcSelectedKitChildItems VARCHAR(MAX),
	@numWarehouseID AS NUMERIC(18,0),
	@vcCoupon AS VARCHAR(200),
	@numCurrencyID NUMERIC(18,0)
AS
BEGIN
	DECLARE @vcItemName AS VARCHAR(500)
	DECLARE @bitDropship AS BIT
	DECLARE @bitMatrix AS BIT
	DECLARE @chrItemType AS CHAR(1)
	DECLARE @numItemGroup AS NUMERIC(18,0)
	DECLARE @numBaseUnit AS NUMERIC(18,0)
	DECLARE @numSaleUnit AS NUMERIC(18,0)
	DECLARE @fltUOMConversionFactor AS FLOAT
	DECLARE @numPurchaseUnit AS NUMERIC(18,0)
	DECLARE @bitAssemblyOrKit BIT
	DECLARE @bitAssembly AS BIT
	DECLARE @bitKit AS BIT
	DECLARE @bitHasChildKits BIT
	DECLARE @bitSOWorkOrder BIT
	DECLARE @bitCalAmtBasedonDepItems AS BIT
	DECLARE @ItemDesc AS VARCHAR(1000)
	DECLARE @numItemClassification AS NUMERIC(18,0)

	DECLARE @monListPrice AS DECIMAL(20,5) = 0.0000
	DECLARE @monVendorCost AS DECIMAL(20,5) = 0.0000
	DECLARE @tintPricingBasedOn AS TINYINT = 0.0000

	DECLARE @tintRuleType AS TINYINT
	DECLARE @monPriceLevelPrice AS DECIMAL(20,5) = 0.0000
	DECLARE @monPriceFinalPrice AS DECIMAL(20,5) = 0.0000
	DECLARE @monPriceRulePrice AS DECIMAL(20,5) = 0.0000
	DECLARE @monPriceRuleFinalPrice AS DECIMAL(20,5) = 0.0000
	DECLARE @monLastPrice AS DECIMAL(20,5) = 0.0000
	DECLARE @dtLastOrderDate VARCHAR(200) = ''

	DECLARE @tintDisountType TINYINT
	DECLARE @decDiscount AS FLOAT

	DECLARE @numVendorID AS NUMERIC(18,0)
	DECLARE @vcMinOrderQty AS VARCHAR(5) = '-'
	DECLARE @vcVendorNotes AS VARCHAR(300) = ''
	DECLARE @numRelationship NUMERIC(18,0)
	DECLARE @numProfile NUMERIC(18,0)
	DECLARE @tintPriceLevel AS TINYINT
	DECLARE @tintDefaultSalesPricing AS TINYINT
	DECLARE @tintPriceBookDiscount AS TINYINT
	DECLARE @tintKitAssemblyPriceBasedOn TINYINT
	DECLARE @numBaseCurrency NUMERIC(18,0)
	DECLARE @fltExchangeRate FLOAT

	IF ISNULL(@numDivisionID,0) > 0
	BEGIN
		SELECT @vcMinOrderQty=ISNULL(CAST(intMinQty AS VARCHAR),'-'),@vcVendorNotes=ISNULL(vcNotes,'') FROM Vendor WHERE numVendorID=@numDivisionID AND numItemCode=@numItemCode
	END

	SELECT 
		@tintDefaultSalesPricing = numDefaultSalesPricing
		,@tintPriceBookDiscount=ISNULL(tintPriceBookDiscount,0) 
		,@numBaseCurrency=ISNULL(numCurrencyID,0)
	FROM 
		Domain 
	WHERE 
		numDomainId=@numDomainID

	IF @numBaseCurrency = @numCurrencyID
	BEGIN
		SET @fltExchangeRate = 1
	END
	ELSE
	BEGIN
		SET @fltExchangeRate = ISNULL((SELECT fltExchangeRate FROM Currency WHERE numDomainID=@numDomainID AND numCurrencyID=@numCurrencyID),1)

		IF ISNULL(@fltExchangeRate,0) = 0
		BEGIN
			SET @fltExchangeRate = 1
		END
	END

	IF ISNULL(@numWarehouseID,0) > 0
	BEGIN
		SELECT TOP 1 @numWarehouseItemID=numWareHouseItemID FROM WareHouseItems WHERE numItemID=@numItemCode AND numWareHouseID=@numWarehouseID ORDER BY numWareHouseItemID ASC
	END

	-- GET ORGANIZATION DETAL
	SELECT 
		@numRelationship=numCompanyType,
		@numProfile=vcProfile,
		@tintPriceLevel = ISNULL(D.tintPriceLevel,0)
	FROM 
		DivisionMaster D                  
	JOIN 
		CompanyInfo C 
	ON 
		C.numCompanyId=D.numCompanyID                  
	WHERE 
		numDivisionID =@numDivisionID       

	-- GET ITEM DETAILS
	SELECT
		@vcItemName = vcItemName,
		@bitDropship = bitAllowDropShip,
		@ItemDesc = Item.txtItemDesc,
		@bitMatrix = ISNULL(Item.bitMatrix,0),
		@chrItemType = charItemType,
		@numItemGroup = numItemGroup,
		@bitAssembly = ISNULL(bitAssembly,0),
		@bitSOWorkOrder = ISNULL(bitSOWorkOrder,0),
		@numBaseUnit = ISNULL(numBaseUnit,0),
		@numSaleUnit = (CASE WHEN ISNULL(@numUOMID,0) > 0 THEN @numUOMID ELSE ISNULL(numSaleUnit,0) END),
		@numPurchaseUnit = (CASE WHEN ISNULL(@numUOMID,0) > 0 THEN @numUOMID ELSE ISNULL(numPurchaseUnit,0) END),
		@numItemClassification = numItemClassification,
		@bitAssemblyOrKit = (CASE WHEN ISNULL(bitKitParent,0)=1 OR ISNULL(bitAssembly,0)=1 THEN 1 ELSE 0 END),
		@monListPrice = (CASE 
							WHEN EXISTS (SELECT ID FROM ItemCurrencyPrice WHERE numDomainID=@numDomainID AND numItemCode=@numItemCode AND numCurrencyID=ISNULL(@numCurrencyID,0))  
							THEN ISNULL((SELECT monListPrice FROM ItemCurrencyPrice WHERE numDomainID=@numDomainID AND numItemCode=@numItemCode AND numCurrencyID=ISNULL(@numCurrencyID,0)),0)
							ELSE (CASE WHEN @fltExchangeRate <> 1 THEN CAST((ISNULL(Item.monListPrice,0) / @fltExchangeRate) AS INT) ELSE ISNULL(Item.monListPrice,0) END)
						END),
		@monVendorCost = ISNULL((SELECT (CASE WHEN @fltExchangeRate <> 1 THEN CAST((ISNULL(monCost,0) / @fltExchangeRate) AS INT) ELSE ISNULL(monCost,0) END) * dbo.fn_UOMConversion(Item.numBaseUnit,Item.numItemCode,Item.numDomainID,Item.numPurchaseUnit) FROM Vendor WHERE numVendorID=Item.numVendorID AND Vendor.numItemCode=Item.numItemCode),0),
		@numVendorID = ISNULL(Item.numVendorID,0),
		@numWarehouseItemID = (CASE WHEN ISNULL(@numWarehouseItemID,0) > 0 THEN @numWarehouseItemID ELSE WareHouseItems.numWareHouseItemID END),
		@bitCalAmtBasedonDepItems = ISNULL(bitCalAmtBasedonDepItems,0),
		@tintKitAssemblyPriceBasedOn=ISNULL(tintKitAssemblyPriceBasedOn,1),
		@bitKit=ISNULL(bitKitParent,0),
		@bitHasChildKits = (CASE 
								WHEN ISNULL(bitKitParent,0) = 1 
								THEN 
									(CASE 
										WHEN EXISTS (SELECT ID.numItemDetailID FROM ItemDetails ID INNER JOIN Item IInner ON ID.numChildItemID=IInner.numItemCode WHERE ID.numItemKitID=Item.numItemCode AND ISNULL(IInner.bitKitParent,0)=1)
										THEN 1
										ELSE 0
									END)
								ELSE 0 
							END)
	FROM
		Item
	OUTER APPLY
	(
		SELECT TOP 1 numWareHouseItemID,monWListPrice FROM WareHouseItems WHERE numItemID=@numItemCode AND (numWareHouseItemID = @numWarehouseItemID OR ISNULL(@numWarehouseItemID,0) = 0)
	) AS WareHouseItems
	WHERE
		Item.numItemCode = @numItemCode

	IF ISNULL(@numDivisionID,0) > 0 AND EXISTS (SELECT * FROM Vendor WHERE numItemCode=@numItemCode AND numVendorID=@numDivisionID)
	BEGIN
		SELECT @monVendorCost= (CASE WHEN @fltExchangeRate <> 1 THEN CAST((ISNULL(monCost,0) / @fltExchangeRate) AS INT) ELSE ISNULL(monCost,0) END) FROM Vendor WHERE numVendorID=@numDivisionID AND numItemCode=@numItemCode
	END


	IF @chrItemType = 'P' AND ISNULL(@numWarehouseItemID,0) = 0
	BEGIN
		RAISERROR('WAREHOUSE_DOES_NOT_EXISTS',16,1)
	END

	SET @fltUOMConversionFactor = dbo.fn_UOMConversion((CASE WHEN  @tintOppType = 1 THEN @numSaleUnit ELSE @numPurchaseUnit END),@numItemCode,@numDomainId,@numBaseUnit)

	IF @tintOppType = 1
	BEGIN
		/*Use Calculated Price When item is Assembly/Kit,Calculate price based on sum of dependent items.*/
		IF @bitCalAmtBasedonDepItems = 1 
		BEGIN
			PRINT 'IN DEPENDED ITEM'

			DECLARE @TEMPPrice TABLE
			(
				bitSuccess BIT
				,monPrice DECIMAL(20,5)
			)
			INSERT INTO @TEMPPrice
			(
				bitSuccess
				,monPrice
			)
			SELECT
				bitSuccess
				,monMSRPPrice
			FROM
				dbo.fn_GetKitAssemblyCalculatedPrice(@numDomainID,@numDivisionID,@numItemCode,@numQty,@numWarehouseItemID,@tintKitAssemblyPriceBasedOn,@numOppID,@numOppItemID,@vcSelectedKitChildItems,@numCurrencyID,@fltExchangeRate)

			IF (SELECT bitSuccess FROM @TEMPPrice) = 1
			BEGIN
				SET @monListPrice = (SELECT monPrice FROM @TEMPPrice)
			END
			ELSE
			BEGIN
				RAISERROR('SELECTED_WAREHOUSE_IS_NOT_AVAILABLE_FOR_ALL_CHILD_ITEMS',16,1)
			END
		END

		IF @tintDefaultSalesPricing = 1 AND (SELECT 
												COUNT(*) 
											FROM 
												PricingTable 
											WHERE 
												numItemCode=@numItemCode 
												AND ISNULL(numPriceRuleID,0) = 0
												AND 1 = (CASE 
													WHEN EXISTS (SELECT * FROM PricingTable WHERE numItemCode=@numItemCode AND ISNULL(numPriceRuleID,0) = 0 AND numCurrencyID=@numCurrencyID)
													THEN (CASE WHEN ISNULL(numCurrencyID,0) = @numCurrencyID THEN 1 ELSE 0 END)
													ELSE (CASE WHEN ISNULL(numCurrencyID,0) = 0 THEN 1 ELSE 0 END)
												END)) > 0 -- PRICE LEVEL
		BEGIN
			SELECT
				@tintDisountType = tintDiscountType,
				@decDiscount = ISNULL(decDiscount,0),
				@tintRuleType = tintRuleType,
				@monPriceLevelPrice = (CASE tintRuleType WHEN 1 THEN @monListPrice WHEN 2 THEN @monVendorCost ELSE ISNULL(decDiscount,0) END),
				@monPriceFinalPrice = (CASE tintRuleType
										WHEN 1 -- Deduct From List Price
										THEN
											(CASE tintDiscountType 
												WHEN 1 -- PERCENT
												THEN (CASE WHEN @monListPrice > 0 THEN @monListPrice - (@monListPrice * (ISNULL(decDiscount,0)/100)) ELSE 0 END)
												WHEN 2 -- FLAT AMOUNT
												THEN (CASE WHEN @monListPrice > 0 THEN (CASE WHEN (@monListPrice * @numQty * @fltUOMConversionFactor) - (CASE WHEN @numCurrencyID <> numCurrencyID THEN CAST((ISNULL(decDiscount,0) / @fltExchangeRate) AS INT) ELSE ISNULL(decDiscount,0) END) > 0 THEN ((@monListPrice * @numQty * @fltUOMConversionFactor) - (CASE WHEN @numCurrencyID <> numCurrencyID THEN CAST((ISNULL(decDiscount,0) / @fltExchangeRate) AS INT) ELSE ISNULL(decDiscount,0) END)) / (@numQty * @fltUOMConversionFactor) ELSE 0 END) ELSE 0 END)
												WHEN 3 -- NAMED PRICE
												THEN (CASE WHEN @numCurrencyID <> numCurrencyID THEN CAST((ISNULL(decDiscount,0) / @fltExchangeRate) AS INT) ELSE ISNULL(decDiscount,0) END)
											END)
										WHEN 2 -- Add to primary vendor cost
										THEN
											(CASE tintDiscountType 
												WHEN 1  -- PERCENT
												THEN @monVendorCost + (@monVendorCost * (ISNULL(decDiscount,0)/100))  * @fltUOMConversionFactor
												WHEN 2 -- FLAT AMOUNT
												THEN (CASE WHEN (@monVendorCost * @numQty * @fltUOMConversionFactor) + (CASE WHEN @numCurrencyID <> numCurrencyID THEN CAST((ISNULL(decDiscount,0) / @fltExchangeRate) AS INT) ELSE ISNULL(decDiscount,0) END) > 0 THEN ((@monVendorCost * @numQty * @fltUOMConversionFactor) + (CASE WHEN @numCurrencyID <> numCurrencyID THEN CAST((ISNULL(decDiscount,0) / @fltExchangeRate) AS INT) ELSE ISNULL(decDiscount,0) END)) / (@numQty * @fltUOMConversionFactor)  ELSE 0 END)
												WHEN 3 -- NAMED PRICE
												THEN (CASE WHEN @numCurrencyID <> numCurrencyID THEN CAST((ISNULL(decDiscount,0) / @fltExchangeRate) AS INT) ELSE ISNULL(decDiscount,0) END)
											END)
										WHEN 3 -- Named price
										THEN
											(CASE WHEN @numCurrencyID <> numCurrencyID THEN CAST((ISNULL(decDiscount,0) / @fltExchangeRate) AS INT) ELSE ISNULL(decDiscount,0) END)
										END
									) 
			FROM
			(
				SELECT 
					ROW_NUMBER() OVER(PARTITION BY PricingTable.numItemCode ORDER BY numPricingID) Id,
					*
				FROM
					PricingTable
				WHERE 
					PricingTable.numItemCode = @numItemCode
					AND ISNULL(PricingTable.numPriceRuleID,0) = 0
					AND 1 = (CASE 
								WHEN EXISTS (SELECT * FROM PricingTable WHERE numItemCode=@numItemCode AND ISNULL(numPriceRuleID,0) = 0 AND numCurrencyID=@numCurrencyID)
								THEN (CASE WHEN ISNULL(numCurrencyID,0) = @numCurrencyID THEN 1 ELSE 0 END)
								ELSE (CASE WHEN ISNULL(numCurrencyID,0) = 0 THEN 1 ELSE 0 END)
							END)
			) TEMP
			WHERE
				numItemCode=@numItemCode 
				AND ISNULL(numPriceRuleID,0) = 0
				AND 1 = (CASE 
							WHEN ISNULL(@tintPriceLevel,0) > 0 
							THEN (CASE WHEN Id=@tintPriceLevel THEN 1 ELSE 0 END) 
							ELSE (CASE WHEN @numQty * @fltUOMConversionFactor >= intFromQty AND @numQty * @fltUOMConversionFactor <= intToQty THEN 1 ELSE 0 END)
						END)
		END
		ELSE IF @tintDefaultSalesPricing = 2 AND (SELECT COUNT(*) FROM PriceBookRules WHERE numDomainID=@numDomainID AND tintRuleFor=@tintOppType) > 0 -- PRICE RULE
		BEGIN
			DECLARE @numPriceRuleID NUMERIC(18,0)
			DECLARE @tintPriceRuleType AS TINYINT
			DECLARE @tintPricingMethod TINYINT
			DECLARE @tintPriceBookDiscountType TINYINT
			DECLARE @decPriceBookDiscount FLOAT
			DECLARE @intQntyItems INT
			DECLARE @decMaxDedPerAmt FLOAT

			SELECT 
				TOP 1
				@numPriceRuleID = numPricRuleID,
				@tintPricingMethod = tintPricingMethod,
				@tintPriceRuleType = p.tintRuleType,
				@tintPriceBookDiscountType = P.[tintDiscountType],
				@decPriceBookDiscount = P.[decDiscount],
				@intQntyItems = P.[intQntyItems],
				@decMaxDedPerAmt = P.[decMaxDedPerAmt]
			FROM   
				PriceBookRules P 
			LEFT JOIN 
				PriceBookRuleDTL PDTL 
			ON 
				P.numPricRuleID = PDTL.numRuleID
			LEFT JOIN 
				PriceBookRuleItems PBI 
			ON 
				P.numPricRuleID = PBI.numRuleID
			LEFT JOIN 
				[PriceBookPriorities] PP 
			ON 
				PP.[Step2Value] = P.[tintStep2] 
				AND PP.[Step3Value] = P.[tintStep3]
			WHERE  
				P.numDomainID=@numDomainID 
				AND tintRuleFor=@tintOppType
				AND (
						((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 1 AND PDTL.numValue = @numDivisionID)) -- Priority 1
						OR ((tintStep2 = 2 AND PBI.numValue = @numItemClassification) AND (tintStep3= 1 AND PDTL.numValue = @numDivisionID)) -- Priority 2
						OR ((tintStep2 = 3) AND (tintStep3 = 1 AND PDTL.numValue = @numDivisionID)) -- Priority 3

						OR ((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 4
						OR ((tintStep2 = 2 AND PBI.numValue = @numItemClassification) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 5
						OR ((tintStep2 = 3) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 6

						OR ((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 3)) -- Priority 7
						OR ((tintStep2 = 2 AND PBI.numValue = @numItemClassification) AND (tintStep3 = 3)) -- Priority 8
						OR ((tintStep2 = 3) and (tintStep3 = 3)) -- Priority 9
					)
			ORDER BY 
				PP.Priority ASC


			IF ISNULL(@numPriceRuleID,0) > 0
			BEGIN
				IF @tintPricingMethod = 1 -- BASED ON PRICE TABLE
				BEGIN
					SELECT
						@tintDisountType = tintDiscountType,
						@decDiscount = (CASE WHEN tintDiscountType=3 THEN 0 ELSE ISNULL(decDiscount,0) END),
						@tintRuleType = tintRuleType,
						@monPriceRulePrice = (CASE tintRuleType WHEN 1 THEN @monListPrice WHEN 2 THEN @monVendorCost END) ,
						@monPriceRuleFinalPrice = (CASE tintRuleType
												WHEN 1 -- Deduct From List Price
												THEN
													(CASE tintDiscountType 
														WHEN 1 -- PERCENT
														THEN (CASE WHEN @monListPrice > 0 THEN @monListPrice - (@monListPrice * (ISNULL(decDiscount,0)/100)) ELSE 0 END)
														WHEN 2 -- FLAT AMOUNT
														THEN (CASE WHEN @monListPrice > 0 THEN (CASE WHEN (@monListPrice * @numQty * @fltUOMConversionFactor) - (CASE WHEN @numCurrencyID <> numCurrencyID THEN CAST((ISNULL(decDiscount,0) / @fltExchangeRate) AS INT) ELSE ISNULL(decDiscount,0) END) > 0 THEN ((@monListPrice * @numQty * @fltUOMConversionFactor) - (CASE WHEN @numCurrencyID <> numCurrencyID THEN CAST((ISNULL(decDiscount,0) / @fltExchangeRate) AS INT) ELSE ISNULL(decDiscount,0) END)) / (@numQty * @fltUOMConversionFactor) ELSE 0 END) ELSE 0 END)
														WHEN 3 -- NAMED PRICE
														THEN (CASE WHEN @numCurrencyID <> numCurrencyID THEN CAST((ISNULL(decDiscount,0) / @fltExchangeRate) AS INT) ELSE ISNULL(decDiscount,0) END)
													END)
												WHEN 2 -- Add to primary vendor cost
												THEN
													(CASE tintDiscountType 
														WHEN 1  -- PERCENT
														THEN @monVendorCost + (@monVendorCost * (ISNULL(decDiscount,0)/100))
														WHEN 2 -- FLAT AMOUNT
														THEN (CASE WHEN (@monVendorCost * @numQty * @fltUOMConversionFactor) + (CASE WHEN @numCurrencyID <> numCurrencyID THEN CAST((ISNULL(decDiscount,0) / @fltExchangeRate) AS INT) ELSE ISNULL(decDiscount,0) END) > 0 THEN ((@monVendorCost * @numQty * @fltUOMConversionFactor) + (CASE WHEN @numCurrencyID <> numCurrencyID THEN CAST((ISNULL(decDiscount,0) / @fltExchangeRate) AS INT) ELSE ISNULL(decDiscount,0) END)) / (@numQty * @fltUOMConversionFactor)  ELSE 0 END)
														WHEN 3 -- NAMED PRICE
														THEN (CASE WHEN @numCurrencyID <> numCurrencyID THEN CAST((ISNULL(decDiscount,0) / @fltExchangeRate) AS INT) ELSE ISNULL(decDiscount,0) END)
													END)
												WHEN 3 -- Named price
												THEN
													(CASE 
														WHEN ISNULL(@tintPriceLevel,0) > 0
														THEN
															(SELECT 
																 (CASE WHEN @numCurrencyID <> numCurrencyID THEN CAST((ISNULL(decDiscount,0) / @fltExchangeRate) AS INT) ELSE ISNULL(decDiscount,0) END)
															FROM
															(
																SELECT 
																	ROW_NUMBER() OVER(PARTITION BY PricingTable.numItemCode ORDER BY numPricingID) Id,
																	decDiscount
																FROM 
																	PricingTable 
																WHERE 
																	PricingTable.numItemCode = @numItemCode 
																	AND tintRuleType = 3 
															) TEMP
															WHERE
																Id = @tintPriceLevel)
														ELSE (CASE WHEN @numCurrencyID <> numCurrencyID THEN CAST((ISNULL(decDiscount,0) / @fltExchangeRate) AS INT) ELSE ISNULL(decDiscount,0) END)
													END)
												END
											) 
					FROM
						PricingTable
					WHERE
						numPriceRuleID=@numPriceRuleID 
						AND 1 = (CASE 
									WHEN EXISTS (SELECT * FROM PricingTable WHERE numItemCode=@numItemCode AND ISNULL(numPriceRuleID,0) = 0 AND numCurrencyID=@numCurrencyID)
									THEN (CASE WHEN ISNULL(numCurrencyID,0) = @numCurrencyID THEN 1 ELSE 0 END)
									ELSE (CASE WHEN ISNULL(numCurrencyID,0) = 0 THEN 1 ELSE 0 END)
								END)
						AND @numQty * @fltUOMConversionFactor >= intFromQty 
						AND @numQty * @fltUOMConversionFactor <= intToQty
				END
				ELSE IF @tintPricingMethod = 2 -- BASED ON RULE
				BEGIN
					SET @tintRuleType = @tintPriceRuleType
					SET @monPriceRulePrice = (CASE @tintPriceRuleType WHEN 1 THEN @monListPrice WHEN 2 THEN @monVendorCost END) 

					IF (@tintPriceBookDiscountType = 1 ) -- Percentage 
					BEGIN
						SET @tintDisountType = @tintPriceBookDiscountType
						SET @decDiscount = @decPriceBookDiscount * (@numQty/@intQntyItems);
						SET @decDiscount = @decPriceBookDiscount * (CASE WHEN @tintPriceRuleType = 2 THEN @monVendorCost ELSE @monListPrice END) /100;
						SET @decMaxDedPerAmt = @decMaxDedPerAmt * (CASE WHEN @tintPriceRuleType = 2 THEN @monVendorCost ELSE @monListPrice END) /100;
						
						IF (@decDiscount > @decMaxDedPerAmt)
							SET @decDiscount = @decMaxDedPerAmt;
				
						IF ( @tintPriceRuleType = 1 )
								SET @monPriceRuleFinalPrice = (@monListPrice - @decDiscount);
						IF ( @tintPriceRuleType = 2 )
								SET @monPriceRuleFinalPrice = (@monVendorCost + @decDiscount);						
					
						SET @monPriceRuleFinalPrice = (CASE WHEN @numBaseCurrency <> @numCurrencyID THEN CAST((ISNULL(@monPriceRuleFinalPrice,0) / @fltExchangeRate) AS INT) ELSE ISNULL(@monPriceRuleFinalPrice,0) END)
					END
					IF ( @tintPriceBookDiscountType = 2 ) -- Flat discount 
					BEGIN
						SET @decDiscount = (CASE WHEN @numBaseCurrency <> @numCurrencyID THEN CAST((ISNULL(@decPriceBookDiscount,0) / @fltExchangeRate) AS INT) ELSE ISNULL(@decPriceBookDiscount,0) END) * ((@numQty * @fltUOMConversionFactor)/@intQntyItems);

						IF (@decDiscount > @decMaxDedPerAmt)
							SET @decDiscount = @decMaxDedPerAmt;
					
						IF ( @tintPriceRuleType = 1 )
							SET @monPriceRuleFinalPrice =  ((@monListPrice * @numQty * @fltUOMConversionFactor) - @decDiscount)/(@numQty * @fltUOMConversionFactor);
						IF ( @tintPriceRuleType = 2 )
							SET @monPriceRuleFinalPrice = ((@monVendorCost * @numQty * @fltUOMConversionFactor) + @decDiscount)/(@numQty * @fltUOMConversionFactor);
					END
				END
			END
		
		END
	
		-- GET Last Price
		DECLARE @LastDiscountType TINYINT
		DECLARE @LastDiscount FLOAT

		SELECT 
			TOP 1 
			@monLastPrice = ((ISNULL(monPrice,0) * ISNULL(OpportunityMaster.fltExchangeRate,1)) / @fltExchangeRate)
			,@LastDiscountType = ISNULL(OpportunityItems.bitDiscountType,1)
			,@LastDiscount = (CASE 
								WHEN ISNULL(OpportunityItems.bitDiscountType,1) = 1 
								THEN ((ISNULL(OpportunityItems.fltDiscount,0) * ISNULL(OpportunityMaster.fltExchangeRate,1)) / @fltExchangeRate)
								ELSE ISNULL(OpportunityItems.fltDiscount,0) END) 
			,@dtLastOrderDate = dbo.FormatedDateFromDate(OpportunityMaster.bintCreatedDate,@numDomainID)
		FROM 
			OpportunityItems 
		JOIN 
			OpportunityMaster 
		ON 
			OpportunityItems.numOppId = OpportunityMaster.numOppId 
		WHERE 
			OpportunityMaster.numDomainId= @numDomainID
			AND tintOppType = @tintOppType
			AND tintOppStatus = 1
			AND numItemCode=@numItemCode
		ORDER BY
			OpportunityMaster.numOppId DESC

		IF @tintDefaultSalesPricing = 1 AND ISNULL(@monPriceFinalPrice,0) > 0
		BEGIN
			-- WE ARE NOT DISPLAYING DISCOUNT SEPERATELY IN PRICE LEVEL
			SET @tintPricingBasedOn = 1
			SET @monPrice = @monPriceFinalPrice
			SET @tintDisountType = 1
			SET @decDiscount = 0
		END
		ELSE IF @tintDefaultSalesPricing = 2 AND ISNULL(@monPriceRuleFinalPrice,0) > 0
		BEGIN
			SET @tintPricingBasedOn = 2
		
			IF @tintDisountType = 3
			BEGIN
				SET @monPrice = @monPriceRuleFinalPrice
				SET @tintDisountType = 1
				SET @decDiscount = 0
			END
			ELSE
			BEGIN
				IF @tintPriceBookDiscount = 1 -- Display Unit Price And Disocunt Price Seperately
				BEGIN
					SET @monPrice = @monPriceRulePrice
				END
				ELSE -- Display Disocunt Price As Unit Price
				BEGIN
					SET @monPrice = @monPriceRuleFinalPrice
					SET @tintDisountType = 1
					SET @decDiscount = 0
				END
			END
		END
		ELSE IF @tintDefaultSalesPricing = 3 AND ISNULL(@monLastPrice,0) > 0
		BEGIN
			SET @tintPricingBasedOn = 3
			SET @monPrice = @monLastPrice
			SET @tintDisountType = @LastDiscountType
			SET @decDiscount = @LastDiscount
		END
		ELSE
		BEGIN
			SET @tintPricingBasedOn = 0
			SET @tintDisountType = 1
			SET @decDiscount = 0
			SET @tintRuleType = 0

			IF @tintOppType = 1
				SET @monPrice = @monListPrice
			ELSE
				SET @monPrice = @monVendorCost
		END
	END
	ELSE IF @tintOppType = 2
	BEGIN
		SELECT 
			TOP 1
			@numPriceRuleID = numPricRuleID,
			@tintPricingMethod = tintPricingMethod,
			@tintPriceRuleType = p.tintRuleType,
			@tintPriceBookDiscountType = P.[tintDiscountType],
			@decPriceBookDiscount = P.[decDiscount],
			@intQntyItems = P.[intQntyItems],
			@decMaxDedPerAmt = P.[decMaxDedPerAmt]
		FROM   
			PriceBookRules P 
		LEFT JOIN 
			PriceBookRuleDTL PDTL 
		ON 
			P.numPricRuleID = PDTL.numRuleID
		LEFT JOIN 
			PriceBookRuleItems PBI 
		ON 
			P.numPricRuleID = PBI.numRuleID
		LEFT JOIN 
			[PriceBookPriorities] PP 
		ON 
			PP.[Step2Value] = P.[tintStep2] 
			AND PP.[Step3Value] = P.[tintStep3]
		WHERE  
			P.numDomainID=@numDomainID 
			AND tintRuleFor=@tintOppType
			AND (
					((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 1 AND PDTL.numValue = @numDivisionID)) -- Priority 1
					OR ((tintStep2 = 2 AND PBI.numValue = @numItemClassification) AND (tintStep3= 1 AND PDTL.numValue = @numDivisionID)) -- Priority 2
					OR ((tintStep2 = 3) AND (tintStep3 = 1 AND PDTL.numValue = @numDivisionID)) -- Priority 3

					OR ((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 4
					OR ((tintStep2 = 2 AND PBI.numValue = @numItemClassification) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 5
					OR ((tintStep2 = 3) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 6

					OR ((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 3)) -- Priority 7
					OR ((tintStep2 = 2 AND PBI.numValue = @numItemClassification) AND (tintStep3 = 3)) -- Priority 8
					OR ((tintStep2 = 3) and (tintStep3 = 3)) -- Priority 9
				)
		ORDER BY 
			PP.Priority ASC


		IF ISNULL(@numPriceRuleID,0) > 0
		BEGIN
			IF @tintPricingMethod = 1 -- BASED ON PRICE TABLE
			BEGIN
				SELECT
					@tintDisountType = tintDiscountType,
					@decDiscount = (CASE WHEN tintDiscountType=3 THEN 0 ELSE ISNULL(decDiscount,0) END),
					@tintRuleType = tintRuleType,
					@monPriceRulePrice = (CASE tintRuleType WHEN 1 THEN @monListPrice WHEN 2 THEN @monVendorCost END) ,
					@monPriceRuleFinalPrice = (CASE tintRuleType
											WHEN 1 -- Deduct From List Price
											THEN
												CASE tintDiscountType 
													WHEN 1 -- PERCENT
													THEN (CASE WHEN @monListPrice > 0 THEN @monListPrice - (@monListPrice * (ISNULL(decDiscount,0)/100)) ELSE 0 END)
													WHEN 2 -- FLAT AMOUNT
													THEN (CASE WHEN @monListPrice > 0 THEN (CASE WHEN (@monListPrice * @numQty * @fltUOMConversionFactor) - ISNULL(decDiscount,0) > 0 THEN ((@monListPrice * @numQty * @fltUOMConversionFactor) - ISNULL(decDiscount,0)) / (@numQty * @fltUOMConversionFactor) ELSE 0 END) ELSE 0 END)
													WHEN 3 -- NAMED PRICE
													THEN ISNULL(decDiscount,0)
												END
											WHEN 2 -- Add to primary vendor cost
											THEN
												CASE tintDiscountType 
													WHEN 1  -- PERCENT
													THEN @monVendorCost + (@monVendorCost * (ISNULL(decDiscount,0)/100))
													WHEN 2 -- FLAT AMOUNT
													THEN (CASE WHEN (@monVendorCost * @numQty * @fltUOMConversionFactor) + ISNULL(decDiscount,0) > 0 THEN ((@monVendorCost * @numQty * @fltUOMConversionFactor) + ISNULL(decDiscount,0)) / (@numQty * @fltUOMConversionFactor)  ELSE 0 END)
													WHEN 3 -- NAMED PRICE
													THEN ISNULL(decDiscount,0)
												END
											WHEN 3 -- Named price
											THEN
												CASE 
												WHEN ISNULL(@tintPriceLevel,0) > 0
												THEN
													(SELECT 
															ISNULL(decDiscount,0)
													FROM
													(
														SELECT 
															ROW_NUMBER() OVER(PARTITION BY PricingTable.numItemCode ORDER BY numPricingID) Id,
															decDiscount
														FROM 
															PricingTable 
														WHERE 
															PricingTable.numItemCode = @numItemCode 
															AND tintRuleType = 3 
													) TEMP
													WHERE
														Id = @tintPriceLevel)
												ELSE
													ISNULL(decDiscount,0)
												END
											END
										) 
				FROM
					PricingTable
				WHERE
					numPriceRuleID=@numPriceRuleID AND @numQty * @fltUOMConversionFactor >= intFromQty AND @numQty * @fltUOMConversionFactor <= intToQty
			END
			ELSE IF @tintPricingMethod = 2 -- BASED ON RULE
			BEGIN
				SET @tintRuleType = @tintPriceRuleType
				SET @monPriceRulePrice = (CASE @tintPriceRuleType WHEN 1 THEN @monListPrice WHEN 2 THEN @monVendorCost END) 

				IF (@tintPriceBookDiscountType = 1 ) -- Percentage 
				BEGIN
					SET @tintDisountType = @tintPriceBookDiscountType
					SET @decDiscount = @decDiscount * (@numQty/@intQntyItems);
					SET @decDiscount = @decDiscount * (CASE WHEN @tintPriceRuleType = 2 THEN @monVendorCost ELSE @monListPrice END) /100;
					SET @decMaxDedPerAmt = @decMaxDedPerAmt * (CASE WHEN @tintPriceRuleType = 2 THEN @monVendorCost ELSE @monListPrice END) /100;
					
					IF (@decDiscount > @decMaxDedPerAmt)
						SET @decDiscount = @decMaxDedPerAmt;
				
					IF ( @tintPriceRuleType = 1 )
							SET @monPriceRuleFinalPrice = (@monListPrice - @decDiscount);
					IF ( @tintPriceRuleType = 2 )
							SET @monPriceRuleFinalPrice = (@monVendorCost + @decDiscount);
				END
				IF ( @tintPriceBookDiscountType = 2 ) -- Flat discount 
				BEGIN
					SET @decDiscount = @decDiscount * ((@numQty * @fltUOMConversionFactor)/@intQntyItems);

					IF (@decDiscount > @decMaxDedPerAmt)
						SET @decDiscount = @decMaxDedPerAmt;
					
					IF ( @tintPriceRuleType = 1 )
						SET @monPriceRuleFinalPrice =  ((@monListPrice * @numQty * @fltUOMConversionFactor) - @decDiscount)/(@numQty * @fltUOMConversionFactor);
					IF ( @tintPriceRuleType = 2 )
						SET @monPriceRuleFinalPrice = ((@monVendorCost * @numQty * @fltUOMConversionFactor) + @decDiscount)/(@numQty * @fltUOMConversionFactor);
				END
			END
		END

		If @monPriceRuleFinalPrice > 0
		BEGIN
			SET @monPrice = @monPriceRuleFinalPrice
		END
		ELSE
		BEGIN
			SET @monPrice = @monVendorCost
		END


		SET @monPriceLevelPrice = 0
		SET @monPriceFinalPrice = 0
		SET @monPriceRulePrice = 0
		SET @monPriceRuleFinalPrice = 0
		SET @monLastPrice = 0
		SET @dtLastOrderDate = NULL
		SET @tintDisountType = 1
		SET @decDiscount =0 
	END

	DECLARE @numTempWarehouseID NUMERIC(18,0)
	IF ISNULL(@numWarehouseItemID,0) > 0
	BEGIN
		SELECT @numTempWarehouseID=numWareHouseID FROM WareHouseItems WHERE numWareHouseItemID=@numWarehouseItemID
	END
	ELSE
	BEGIN
		SET @numTempWarehouseID = @numWarehouseID
	END

	SELECT
		@vcItemName AS vcItemName,
		@bitDropship AS bitDropship,
		@chrItemType AS ItemType,
		@bitMatrix AS bitMatrix,
		@numItemClassification AS ItemClassification,
		@numItemGroup AS ItemGroup,
		@ItemDesc AS ItemDescription,
		@bitAssembly AS bitAssembly,
		@bitKit AS bitKitParent,
		@bitHasChildKits AS bitHasChildKits,
		@bitSOWorkOrder AS bitSOWorkOrder,
		case when ISNULL(@bitAssembly,0)=1 then dbo.fn_GetAssemblyPossibleWOQty(@numItemCode,@numTempWarehouseID) else 0 end AS MaxWorkOrderQty,
		@tintPricingBasedOn AS tintPriceBaseOn,
		@monPrice AS monPrice,
		@fltUOMConversionFactor AS UOMConversionFactor,
		(CASE WHEN @tintOppType=1 THEN @monListPrice ELSE @monVendorCost END) AS monListPrice,
		@monVendorCost AS VendorCost,
		@numVendorID AS numVendorID,
		@tintRuleType AS tintRuleType,
		@monPriceLevelPrice AS monPriceLevel,
		@monPriceFinalPrice AS monPriceLevelFinalPrice,
		@monPriceRulePrice AS monPriceRule,
		@monPriceRuleFinalPrice AS monPriceRuleFinalPrice,
		@monLastPrice AS monLastPrice,
		@dtLastOrderDate AS LastOrderDate,
		@tintDisountType AS DiscountType,
		@decDiscount AS Discount,
		@numBaseUnit AS numBaseUnit,
		@numSaleUnit AS numSaleUnit,
		@numPurchaseUnit AS numPurchaseUnit,
		dbo.fn_GetUOMName(@numBaseUnit) AS vcBaseUOMName,
		dbo.fn_GetUOMName(@numSaleUnit) AS vcSaleUOMName,
		@numPurchaseUnit AS numPurchaseUnit,
		@numWarehouseItemID AS numWarehouseItemID,
		ISNULL((SELECT numOnHand FROM WareHouseItems WI WHERE WI.numWareHouseItemID=@numWarehouseItemID),0) numOnhand,
		dbo.fn_GetItemTransitCount(@numItemcode,@numDomainID) AS OrdersInTransist,
		(SELECT COUNT(*) FROM SimilarItems SI INNER JOIN Item I ON I.numItemCode = SI.numItemCode WHERE SI.numDomainId = @numDomainID AND SI.numParentItemCode = @numItemCode) AS RelatedItemsCount,
		@tintPriceLevel AS tintPriceLevel,
		@vcMinOrderQty AS vcMinOrderQty,
		@vcVendorNotes AS vcVendorNotes,
		@bitCalAmtBasedonDepItems AS bitCalAmtBasedonDepItems,
		@tintKitAssemblyPriceBasedOn AS tintKitAssemblyPriceBasedOn
END 
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_TicklerActItemsV2')
DROP PROCEDURE USP_TicklerActItemsV2
GO
CREATE Proc [dbo].[USP_TicklerActItemsV2]       
@numUserCntID as numeric(9)=null,                                                                        
@numDomainID as numeric(9)=null,                                                                        
@startDate as datetime,                                                                        
@endDate as datetime,                                                                    
@ClientTimeZoneOffset Int,  --Added by Debasish to enable calculation of date according to client machine             
@bitFilterRecord bit=0,
@columnName varchar(50), 
@columnSortOrder varchar(50),
@vcBProcessValue varchar(500),
@RegularSearchCriteria VARCHAR(MAX)='',
@CustomSearchCriteria VARCHAR(MAX)='', 
@OppStatus int,
@CurrentPage int,                                                              
@PageSize int
AS
BEGIN
	SET @endDate = DATEADD(MILLISECOND,-2,DATEADD(DAY,1,@endDate))

--SET @PageSize=10
DECLARE @tintActionItemsViewRights TINYINT  = 3
DECLARE @bitREQPOApproval AS BIT
DECLARE @bitARInvoiceDue AS BIT
DECLARE @bitAPBillsDue AS BIT
DECLARE @vchARInvoiceDue AS VARCHAR(500)
DECLARE @vchAPBillsDue AS VARCHAR(500)
DECLARE @decReqPOMinValue AS DECIMAL(18,2)
DECLARE @vchREQPOApprovalEmp AS VARCHAR(500)=''
DECLARE @numDomainDivisionID NUMERIC(18,0)
DECLARE @dynamicPartQuery AS NVARCHAR(MAX)=''
DECLARE @ActivityRegularSearch AS VARCHAR(MAX)=''
DECLARE @OtherRegularSearch AS VARCHAR(MAX)=''
DECLARE @IndivRecordPaging AS NVARCHAR(MAX)=''
--SET @IndivRecordPaging=' '
DECLARE @TotalRecordCount INT =0
SET @IndivRecordPaging=  ' ORDER BY dtDueDate DESC OFFSET '+CAST(((@CurrentPage - 1 ) * 10) AS VARCHAR) +' ROWS FETCH NEXT '+CAST(10 AS VARCHAR)+' ROWS ONLY'

SELECT TOP 1 
	@bitREQPOApproval=bitREQPOApproval,
	@vchREQPOApprovalEmp=vchREQPOApprovalEmp,
	@decReqPOMinValue=ISNULL(decReqPOMinValue,0),
	@bitARInvoiceDue=bitARInvoiceDue,
	@bitAPBillsDue = bitAPBillsDue,
	@vchARInvoiceDue = vchARInvoiceDue,
	@vchAPBillsDue = vchAPBillsDue,
	@numDomainDivisionID=numDivisionID
FROM 
	Domain 
WHERE 
	numDomainId=@numDomainID

IF EXISTS (SELECT * FROM GroupAuthorization WHERE numDomainID=@numDomainID AND numModuleID=1 AND numPageID=2 AND numGroupID IN (SELECT ISNULL(numGroupID,0) FROM UserMaster WHERE numDomainID=@numDomainID AND numUserDetailId=@numUserCntID))
BEGIN
	SELECT @tintActionItemsViewRights=ISNULL(intViewAllowed,0) FROM GroupAuthorization WHERE numDomainID=@numDomainID AND numModuleID=1 AND numPageID=2 AND numGroupID IN (SELECT ISNULL(numGroupID,0) FROM UserMaster WHERE numDomainID=@numDomainID AND numUserDetailId=@numUserCntID)
END
--12 - Communication task
--1 - Case Task
--2 - Project Task
--3 - Opportunity Task
--4 - Sales Order Task
--5 - Work Order Management
--6 - Work Center Task
--7 - Requisition Approval
--8 - PO Approval
--9 - Price Margin Approval
--10 - A/R Invoice Due
--11 - A/P Bill Due

DECLARE @dynamicQuery AS NVARCHAR(MAX)=''
DECLARE @dynamicQueryCount AS NVARCHAR(MAX)=''

Create table #tempRecords (RecordCount NUMERIC(9),RecordId NUMERIC(9),numTaskId numeric(9),IsTaskClosed bit,TaskTypeName VARCHAR(100),TaskType NUMERIC(9),
OrigDescription NVARCHAR(MAX),Description NVARCHAR(MAX),TotalProgress INT,numAssignToId NUMERIC(9),
Priority NVARCHAR(MAX),PriorityId  NUMERIC(9),ActivityId NUMERIC(9),OrgName NVARCHAR(MAX),CompanyRating NVARCHAR(MAX),
numContactID numeric(9),numDivisionID numeric(9),tintCRMType tinyint,
Id numeric(9),bitTask varchar(100),Startdate datetime,EndTime datetime,
itemDesc text,[Name] varchar(200),vcFirstName varchar(100),vcLastName varchar(100),numPhone varchar(15),numPhoneExtension varchar(7),
[Phone] varchar(30),vcCompanyName varchar(100),vcProfile VARCHAR(100), vcEmail varchar(50),Task varchar(100),Activity varchar(MAX),
Status varchar(100),numCreatedBy numeric(9),numRecOwner VARCHAR(500), numModifiedBy VARCHAR(500), numOrgTerId NUMERIC(18,0), numTerId VARCHAR(200),numAssignedTo varchar(1000),numAssignedBy varchar(50),
 caseid numeric(9),vcCasenumber varchar(50),casetimeId numeric(9),caseExpId numeric(9),type int,itemid varchar(50),
changekey varchar(50),dtDueDate DATETIME, DueDate varchar(50),bitFollowUpAnyTime bit,numNoOfEmployeesId varchar(50),vcComPhone varchar(50),numFollowUpStatus varchar(200),dtLastFollowUp varchar(500),vcLastSalesOrderDate VARCHAR(100),monDealAmount DECIMAL(20,5), vcPerformance VARCHAR(100),vcColorScheme VARCHAR(50),
Slp_Name varchar(100),intTotalProgress INT,vcPoppName varchar(100), numOppId NUMERIC(18,0),numBusinessProcessID numeric(18,0),
numCompanyRating varchar(500),numStatusID varchar(500),numCampaignID varchar(500),[Action-Item Participants] NVARCHAR(MAX),HtmlLink varchar(500),
Location nvarchar(150),OrignalDescription text)                                                         
 

declare @strSql as varchar(MAX)                                                            
declare @strSql1 as varchar(MAX)                         
declare @strSql2 as varchar(MAX)                                                            
declare @vcCustomColumnName AS VARCHAR(8000)
declare @vcnullCustomColumnName AS VARCHAR(8000)
   -------Change Row Color-------
Declare @vcCSOrigDbCOlumnName as varchar(50),@vcCSLookBackTableName as varchar(50),@vcCSAssociatedControlType as varchar(50)
SET @vcCSOrigDbCOlumnName=''
SET @vcCSLookBackTableName=''

declare @tintOrder as tinyint                                                
declare @vcFieldName as varchar(50)                                                
declare @vcListItemType as varchar(3)                                           
declare @vcAssociatedControlType varchar(20)                                                
declare @numListID AS numeric(9)                                                
declare @vcDbColumnName varchar(20)                    
declare @WhereCondition varchar(2000)                     
declare @vcLookBackTableName varchar(2000)              
Declare @bitCustomField as BIT
DECLARE @bitAllowEdit AS CHAR(1)                 
Declare @numFieldId as numeric  
DECLARE @bitAllowSorting AS CHAR(1)   
declare @vcColumnName AS VARCHAR(500)       
declare @vcCustomColumnNameOnly AS VARCHAR(500)       


IF(@RegularSearchCriteria='')
BEGIN
	SET @RegularSearchCriteria =@RegularSearchCriteria + ' 1=1'
END
if(@OppStatus=0)
BEGIN
SET @RegularSearchCriteria =@RegularSearchCriteria + ' AND IsTaskClosed = 0'
END
if(@OppStatus=1)
BEGIN
SET @RegularSearchCriteria = @RegularSearchCriteria + ' AND IsTaskClosed = 1'
END
 SET @RegularSearchCriteria  = REPLACE(@RegularSearchCriteria,'TaskTypeName','TaskType')

 SET @ActivityRegularSearch = ''
 SET @OtherRegularSearch = ''

 SET @ActivityRegularSearch = ISNULL((SELECT STUFF( 
(
	SELECT  'AND ' + [Items] FROM 
	(
	  SELECT [Items] FROM SplitByString(@RegularSearchCriteria,'AND') WHERE Items<>'' AND 1=
	  (CASE WHEN Items LIKE '%IsTaskClosed%' THEN 0
	 WHEN Items LIKE '%TaskType%'  THEN 0
	 WHEN  Items LIKE '%numAssignToId%'  THEN 0
	 WHEN  Items LIKE '%PriorityId%'  THEN 0
	 WHEN  Items LIKE '%ActivityId%'  THEN 0
	 WHEN  Items LIKE '%Description%'   THEN 0
	 WHEN  Items LIKE '%OrgName%'   THEN 0 ELSE 1 END)
	) AS T FOR XML PATH('')
)
,1,3,'') AS [Items]),'')
SET @ActivityRegularSearch = REPLACE(@ActivityRegularSearch,'&gt;','>')
SET @ActivityRegularSearch = REPLACE(@ActivityRegularSearch,'&lt;','<')
PRINT @ActivityRegularSearch
 SET @OtherRegularSearch = ISNULL((SELECT STUFF( 
(
	SELECT  'AND ' + [Items] FROM 
	(
	  SELECT [Items] FROM SplitByString(@RegularSearchCriteria,'AND') WHERE Items<>'' AND 1=
	  (CASE WHEN Items LIKE '%IsTaskClosed%' THEN 1
	 WHEN Items LIKE '%TaskType%'  THEN 1
	 WHEN  Items LIKE '%numAssignToId%'  THEN 1
	 WHEN  Items LIKE '%PriorityId%'  THEN 1
	 WHEN  Items LIKE '%ActivityId%'  THEN 1
	 WHEN  Items LIKE '%Description%'   THEN 1
	 WHEN  Items LIKE '%OrgName%'   THEN 1 ELSE 0 END)
	) AS T FOR XML PATH('')
)
,1,3,'') AS [Items]),'')
SET @OtherRegularSearch = ISNULL(REPLACE(@OtherRegularSearch,'T.',''),'')
SET @OtherRegularSearch = REPLACE(@OtherRegularSearch,'&gt;','>')
SET @OtherRegularSearch = REPLACE(@OtherRegularSearch,'&lt;','<')
IF(LEN(@OtherRegularSearch)=0)
BEGIN
	SET @OtherRegularSearch = ' 1=1 '
END
PRINT @OtherRegularSearch
Declare @ListRelID as numeric(9)             
set @tintOrder=0   

CREATE TABLE #tempForm 
	(
		tintOrder INT,vcDbColumnName nvarchar(50),vcFieldDataType nvarchar(50),vcOrigDbColumnName nvarchar(50),bitAllowFiltering BIT,vcFieldName nvarchar(50)
		,vcAssociatedControlType nvarchar(50),vcListItemType char(3),numListID numeric(9),vcLookBackTableName varchar(50),bitCustomField BIT,numFieldId NUMERIC
		,bitAllowSorting BIT,bitAllowEdit BIT,bitIsRequired BIT,bitIsEmail BIT,bitIsAlphaNumeric BIT,bitIsNumeric BIT,bitIsLengthValidation BIT,intMaxLength int
		,intMinLength int,bitFieldMessage BIT,vcFieldMessage VARCHAR(500),ListRelID numeric(9),intColumnWidth NUMERIC(18,2)
	)
                
	--Custom Fields related to Organization
	INSERT INTO 
		#tempForm
	SELECT  
		tintRow + 1 AS tintOrder,vcDbColumnName,'',vcDbColumnName,1,vcFieldName,vcAssociatedControlType,'' as vcListItemType,numListID,'' as vcLookBackTableName                                                 
		,bitCustom,numFieldId,bitAllowSorting,0,bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength
		,bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth
	FROM 
		View_DynamicCustomColumns
	WHERE 
		numFormId=43 
		AND numUserCntID=@numUserCntID 
		AND numDomainID=@numDomainID 
		AND tintPageType=1
		AND ISNULL(bitCustom,0)=1
 
	SELECT TOP 1 
		@tintOrder = tintOrder + 1
		,@vcDbColumnName=vcDbColumnName
		,@vcFieldName=vcFieldName
		,@vcAssociatedControlType=vcAssociatedControlType
		,@vcListItemType=vcListItemType
		,@numListID=numListID
		,@vcLookBackTableName=vcLookBackTableName                                               
		,@bitCustomField=bitCustomField
		,@numFieldId=numFieldId
		,@bitAllowSorting=bitAllowSorting
		,@bitAllowEdit=0
		,@ListRelID=ListRelID
	FROM 
		#tempForm 
	ORDER BY 
		tintOrder ASC     
  
	SET @vcCustomColumnName=''
	SET @vcnullCustomColumnName=''
	SET @vcCustomColumnNameOnly = ''
	WHILE @tintOrder>0      
	BEGIN  
		SET @vcColumnName= @vcDbColumnName
		SET @strSql = 'ALTER TABLE #tempRecords ADD [' + @vcColumnName + '] NVARCHAR(MAX)'
		SET @vcCustomColumnNameOnly = @vcCustomColumnNameOnly+','+@vcColumnName
		EXEC(@strSql)
		
		IF @bitCustomField = 1
		BEGIN 
			IF @vcAssociatedControlType = 'TextBox' OR @vcAssociatedControlType = 'TextArea'            
			BEGIN            
				SET @vcCustomColumnName= @vcCustomColumnName+', ISNULL((SELECT TOP 1 CFW.Fld_Value FROM CFW_FLD_Values AS CFW WHERE CFW.Fld_ID='+CAST(@numFieldId AS VARCHAR(50))+' AND CFW.RecId=Div.numDivisionID),'''') ['+ @vcColumnName +']'
				SET @vcnullCustomColumnName= @vcnullCustomColumnName+','''' AS  ['+ @vcColumnName +']'
			END  
			ELSE IF @vcAssociatedControlType = 'CheckBox'         
			BEGIN            
				SET @vcCustomColumnName= @vcCustomColumnName+',ISNULL((SELECT TOP 1 CFW.Fld_Value FROM CFW_FLD_Values AS CFW WHERE CFW.Fld_ID='+CAST(@numFieldId AS VARCHAR(50))+' AND CFW.RecId=Div.numDivisionID),0)  ['+ @vcColumnName + ']'
				SET @vcnullCustomColumnName= @vcnullCustomColumnName+',0 AS ['+ @vcColumnName + ']'
			END            
			ELSE IF @vcAssociatedControlType = 'DateField'        
			BEGIN            
				SET @vcCustomColumnName= @vcCustomColumnName+',dbo.FormatedDateFromDate((SELECT TOP 1 CFW.Fld_Value FROM CFW_FLD_Values AS CFW WHERE CFW.Fld_ID='+CAST(@numFieldId AS VARCHAR(50))+' AND CFW.RecId=Div.numDivisionID),'+convert(varchar(10),@numDomainId)+')  ['+ @vcColumnName +']'              
				SET @vcnullCustomColumnName= @vcnullCustomColumnName+','''' AS  ['+ @vcColumnName +']'
			END            
			ELSE IF @vcAssociatedControlType = 'SelectBox'           
			BEGIN            
				SET @vcCustomColumnName=@vcCustomColumnName+',(SELECT TOP 1 ISNULL(L.vcData,'''') FROM CFW_FLD_Values CFW left Join ListDetails L on L.numListItemID=CFW.Fld_Value WHERE  CFW.Fld_ID='+CAST(@numFieldId AS VARCHAR(50))+' AND CFW.RecId=Div.numDivisionID)'+' ['+ @vcColumnName +']'                                                         
				SET @vcnullCustomColumnName=@vcnullCustomColumnName+','''''+'AS  ['+ @vcColumnName +']'                                                         
			END
			ELSE IF @vcAssociatedControlType = 'CheckBoxList'
			BEGIN
				SET @vcCustomColumnName=@vcCustomColumnName+',(SELECT STUFF((SELECT CONCAT('', '', vcData) FROM ListDetails WHERE numListID= ' + CAST(@numListID AS varchar) + ' AND numlistitemid IN (SELECT ISNULL(Id,0) FROM dbo.SplitIDs(ISNULL((SELECT CFWInner.Fld_Value FROM CFW_FLD_Values CFWInner WHERE CFWInner.Fld_ID='+CAST(@numFieldId AS VARCHAR(50))+' AND CFWInner.RecId=Div.numDivisionID),''''),'','')) FOR XML PATH('''')), 1, 1, ''''))'+' ['+ @vcColumnName +']'
				SET @vcnullCustomColumnName=@vcnullCustomColumnName+','''''+'AS  ['+ @vcColumnName +']'
			END
			ELSE
			BEGIN
				SET @vcCustomColumnName= CONCAT(@vcCustomColumnName,',(SELECT dbo.fn_GetCustFldStringValue(',@numFieldId,',CFW.RecId,CFW.Fld_Value',') FROM CFW_FLD_Values CFW left Join ListDetails L on L.numListItemID=CFW.Fld_Value WHERE  CFW.Fld_ID=',@numFieldId,' AND CFW.RecId=Div.numDivisionID)',' [', @vcColumnName ,']')
				SET @vcnullCustomColumnName=@vcnullCustomColumnName+','''''+'AS  ['+ @vcColumnName +']' 
			END
		END
		  
		SELECT TOP 1 
			@tintOrder=tintOrder + 1
			,@vcDbColumnName=vcDbColumnName
			,@vcFieldName=vcFieldName
			,@vcAssociatedControlType=vcAssociatedControlType
			,@vcListItemType=vcListItemType
			,@numListID=numListID
			,@vcLookBackTableName=vcLookBackTableName                                               
			,@bitCustomField=bitCustomField
			,@numFieldId=numFieldId
			,@bitAllowSorting=bitAllowSorting
			,@bitAllowEdit=0
			,@ListRelID=ListRelID
		FROM 
			#tempForm 
		WHERE 
			tintOrder > @tintOrder-1 
		ORDER BY 
			tintOrder asc            
 
		IF @@rowcount=0 set @tintOrder=0 
	END
SET @strSql=''

Create table #tempColorScheme(vcOrigDbCOlumnName varchar(50),vcLookBackTableName varchar(50),vcFieldValue varchar(50),vcFieldValue1 varchar(50),vcColorScheme varchar(50),vcAssociatedControlType varchar(50))

insert into #tempColorScheme select DFM.vcOrigDbCOlumnName,DFM.vcLookBackTableName,DFCS.vcFieldValue,DFCS.vcFieldValue1,DFCS.vcColorScheme,DFFM.vcAssociatedControlType 
from DycFieldColorScheme DFCS join DycFormField_Mapping DFFM on DFCS.numFieldID=DFFM.numFieldID
join DycFieldMaster DFM on DFM.numModuleID=DFFM.numModuleID and DFM.numFieldID=DFFM.numFieldID
where DFCS.numDomainID=@numDomainID and DFFM.numFormID=43 AND DFCS.numFormID=43 and isnull(DFFM.bitAllowGridColor,0)=1

IF(SELECT COUNT(*) FROM #tempColorScheme)>0
BEGIN
   select top 1 @vcCSOrigDbCOlumnName=vcOrigDbCOlumnName,@vcCSLookBackTableName=vcLookBackTableName,@vcCSAssociatedControlType=vcAssociatedControlType from #tempColorScheme
END   
-------------------- 0 - TASK----------------------------

IF(LEN(@CustomSearchCriteria)>0)
BEGIN
SET @CustomSearchCriteria = ' AND '+@CustomSearchCriteria
END
IF(LEN(@ActivityRegularSearch)=0)
BEGIN
	SET @ActivityRegularSearch = '1=1'
END
DECLARE @StaticColumns AS NVARCHAR(MAX)=''

SET @StaticColumns = ' RecordCount,RecordId,
numCreatedBy,
numOrgTerId,
numTaskId,
IsTaskClosed,
TaskTypeName,
TaskType,
OrigDescription,
Description,
dtDueDate,
DueDate,
TotalProgress,
numAssignedTo,
numAssignToId,
Priority,
Activity,
PriorityId,
ActivityId,
OrgName,
CompanyRating,
numContactID,numDivisionID,tintCRMType,Id,bitTask,Startdate,EndTime,itemDesc,
[Name],vcFirstName,vcLastName,numPhone,numPhoneExtension,[Phone],vcCompanyName,vcProfile,vcEmail,Task,Status,numRecOwner,numModifiedBy,numTerId,numAssignedBy,caseid,vcCasenumber,
casetimeId,caseExpId,type,itemid,bitFollowUpAnyTime,numNoOfEmployeesId,vcComPhone,numFollowUpStatus,dtLastFollowUp,vcLastSalesOrderDate,monDealAmount,vcPerformance'
SET @dynamicQuery = ' INSERT INTO #tempRecords(
 '+@StaticColumns +@vcCustomColumnNameOnly+'
) SELECT COUNT(*) OVER() RecordCount,* FROM (SELECT 
Comm.numCommId AS RecordId,
Comm.numCreatedBy AS numCreatedBy, 
ISNULL(Div.numTerId,0) AS numOrgTerId,
-2 AS numTaskId,
bitClosedFlag AS IsTaskClosed,
''<a onclick="openCommTask(''+CAST(Comm.numCommId AS VARCHAR)+'',0)" href="javascript:void(0)">''+dbo.GetListIemName(Comm.bitTask)+''</a>'' As TaskTypeName,
Comm.bitTask AS TaskType,
'''' AS OrigDescription,
comm.textDetails AS Description,
Comm.dtStartTime AS dtDueDate,
cast(Comm.dtStartTime as datetime) as DueDate,
''-'' As TotalProgress,
Substring( 
dbo.fn_GetContactName(numAssign) + 
ISNULL((SELECT SUBSTRING((SELECT '','' + dbo.fn_GetContactName(numContactId) FROM CommunicationAttendees WHERE numCommId=Comm.numCommId FOR XML PATH('''')),1,200000)),''''),0,50)
As numAssignedTo,
numAssign As numAssignToId,
listdetailsStatus.VcData As Priority, -- Priority column   
 listdetailsActivity.VcData As Activity ,
listdetailsStatus.NumlistItemID As PriorityId, -- Priority column   
 listdetailsActivity.NumlistItemID As ActivityId ,
 ''<a href="../account/frmAccounts.aspx?DivID=''+CAST(Div.numDivisionID AS VARCHAR)+''">''+Comp.vcCompanyName+ CASE WHEN  ISNULL(listCompanyRating.VcData,'''')<>'''' THEN ''(''+listCompanyRating.VcData+'')'' ELSE '''' END +''</a>''  AS OrgName,
 listCompanyRating.VcData As CompanyRating,
 Addc.numContactID,Addc.numDivisionID,Div.tintCRMType,Comm.numCommId AS Id,convert(varchar(15),bitTask) as bitTask,
  DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', dtStartTime) as Startdate,                        
 DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', dtEndTime) as EndTime ,                                                                        
 Comm.textDetails AS itemDesc,  
 AddC.vcFirstName + '' '' + AddC.vcLastName  as [Name], 
 AddC.vcFirstName,AddC.vcLastName,Addc.numPhone,AddC.numPhoneExtension,                               
 case when Addc.numPhone<>'''' then + AddC.numPhone +case when AddC.numPhoneExtension<>'''' then '' - '' + AddC.numPhoneExtension else '''' end  else '''' end as [Phone], 
  Comp.vcCompanyName As vcCompanyName , ISNULL((SELECT vcData FROM ListDetails WHERE numListID =21 AND numDomainID='+ CAST(@numDomainID AS VARCHAR) +' AND  numListItemID=Comp.vcProfile ) ,'''') vcProfile ,                                                        
 AddC.vcEmail As vcEmail ,    
 dbo.GetListIemName(Comm.bitTask)AS Task,   
 listdetailsStatus.VcData As Status,dbo.fn_GetContactName(Comm.numCreatedBy) AS numRecOwner,dbo.fn_GetContactName(Comm.numModifiedBy) AS numModifiedBy,  
  (select TOP 1 vcData from ListDetails where numListItemID=Div.numTerId) AS numTerId,dbo.fn_GetContactName(Comm.numAssignedBy) AS numAssignedBy,
  convert(varchar(50),comm.caseid) as caseid,(select top 1 vcCaseNumber from cases where cases.numcaseid= comm.caseid )as vcCasenumber,
  ISNULL(comm.casetimeId,0) casetimeId,ISNULL(comm.caseExpId,0) caseExpId,0 as type,'''' as itemid,ISNULL(bitFollowUpAnyTime,0) bitFollowUpAnyTime,  
  dbo.GetListIemName(Comp.numNoOfEmployeesId) AS numNoOfEmployeesId,Div.vcComPhone,
dbo.GetListIemName(Div.numFollowUpStatus) AS numFollowUpStatus,' + 
CONCAT('ISNULL(dbo.FormatedDateFromDate(Div.dtLastFollowUp,',@numDomainID,'),'''')') + ' AS dtLastFollowUp,
' + CONCAT('ISNULL(dbo.FormatedDateFromDate(TempLastOrder.bintCreatedDate,',@numDomainID,'),'''')') + ' AS vcLastSalesOrderDate,ISNULL(TempPerformance.monDealAmount,0) monDealAmount,
(CASE WHEN ISNULL(TempPerformance.monDealAmount,0) > 0 THEN CONCAT(FORMAT(TempPerformance.monDealAmount,''##,####.00''),'' ('',CAST(TempPerformance.fltProfitPer AS DECIMAL(18,2)),''%)'') ELSE '''' END) AS vcPerformance
  '+@vcCustomColumnName+'
 FROM  Communication Comm                              
 JOIN AdditionalContactsInformation AddC                                                  
 ON Comm.numContactId = AddC.numContactId                                  
 JOIN DivisionMaster Div                                                                         
 ON AddC.numDivisionId = Div.numDivisionID                                   
 JOIN CompanyInfo  Comp                                                                         
 ON Div.numCompanyID = Comp.numCompanyId  
 OUTER APPLY(' + CONCAT('SELECT MAX(bintCreatedDate) AS bintCreatedDate FROM OpportunityMaster WHERE numDomainID = ',@numDomainID,' AND numDivisionID=Div.numDivisionID AND tintOppType = 1 AND tintOppStatus = 1') + ' ) AS TempLastOrder
 OUTER APPLY(' + CONCAT('SELECT
																	SUM(monDealAmount) AS monDealAmount
																	,AVG(ProfitPer) AS fltProfitPer
																FROM
																(
																	SELECT
																		OM.numDivisionId
																		,monDealAmount
																		,AVG(((ISNULL(OI.monPrice,0) - (CASE D.numCost WHEN 3 THEN ISNULL(V.monCost,0) * dbo.fn_UOMConversion(I.numBaseUnit,I.numItemCode,I.numDomainID,I.numPurchaseUnit) WHEN 2 THEN ISNULL(OI.numCost,0) ELSE ISNULL(I.monAverageCost,0) END)) / ISNULL(NULLIF(OI.monPrice,0),1)) * 100) ProfitPer
																	FROM
																		OpportunityMaster OM
																	INNER JOIN
																		Domain D
																	ON
																		OM.numDomainID = D.numDomainID
																	INNER JOIN
																		OpportunityItems OI
																	ON
																		OM.numOppId = OI.numOppId
																	INNER JOIN
																		Item I
																	ON
																		OI.numItemCode = I.numItemCode
																	LEFT JOIN
																		Vendor V
																	ON
																		V.numVendorID = I.numVendorID
																		AND V.numItemCode = I.numItemCode
																	WHERE
																		OM.numDomainId = ',@numDomainID,'
																		AND OM.numDivisionId = Div.numDivisionID
																		AND ISNULL(I.bitContainer,0) = 0
																		AND tintOppType = 1
																		AND tintOppStatus=1
																	GROUP BY
																		OM.numDivisionId
																		,OM.numOppId
																		,monDealAmount
																) T1
																GROUP BY
																	T1.numDivisionId )') + ' AS TempPerformance
 LEFT JOIN OpportunityMaster Opp ON Opp.numDivisionId = Div.numDivisionID AND Opp.numContactId = AddC.numContactId  AND Opp.numOppID = Comm.numOppID 
 LEFT JOIN Sales_process_List_Master splm ON splm.Slp_Id = Opp.numBusinessProcessID 
 LEFT JOIN ProjectProgress PP On PP.numOppId = Opp.numOppId
  Left Join listdetails listdetailsStatus                                                 
 On Comm.numStatus = listdetailsStatus.NumlistItemID
  Left Join listdetails listdetailsActivity                                                  
 On Comm.numActivity = listdetailsActivity.NumlistItemID
 LEFT JOIN ListDetails AS listCompanyRating
 ON Comp.numCompanyRating=listCompanyRating.NumlistItemID
WHERE '+@ActivityRegularSearch+' AND
	Comm.numDomainID = ' + Cast(@numDomainID as varchar(10))  + ' AND 1 = (CASE ' + CAST( @tintActionItemsViewRights AS VARCHAR) + 
' WHEN 3 THEN 1 
WHEN 2 THEN (CASE WHEN Div.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+ CONVERT(VARCHAR(15),@numUserCntID) + ' ) THEN 1 ELSE 0 END)
WHEN 1 THEN (CASE WHEN (Comm.numCreatedBy= ' + CONVERT(VARCHAR(15),@numUserCntID) + ' or Comm.numAssign= '+ CONVERT(VARCHAR(15),@numUserCntID) + ') THEN 1 ELSE 0 END) 
ELSE 0 END)  AND (DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',dtStartTime) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',dtStartTime) <= '''+Cast(@endDate as varchar(30))+''')
'+@CustomSearchCriteria+'
 )   Q WHERE '+@OtherRegularSearch+'
  '+@IndivRecordPaging+'
'


EXEC (@dynamicQuery)
PRINT 'HI'
PRINT CAST(@dynamicQuery AS TEXT)
PRINT LEN(@ActivityRegularSearch)
-------------------- 0 - Email Communication----------------------------
--''<a onclick="openCommTask(''+CAST(ac.activityid AS VARCHAR)+'',1)" href="javascript:void(0)">Calendar</a>'' As TaskTypeName,
IF(LEN(@ActivityRegularSearch)=0 OR RTRIM(LTRIM(@ActivityRegularSearch))='1=1')
BEGIN
DECLARE @FormattedItems As VARCHAR(MAX)
SET @FormattedItems ='
RecordCount,RecordId,
numCreatedBy,
numOrgTerId,
numTaskId,
IsTaskClosed,
TaskTypeName,
TaskType,
OrigDescription,
Description,
dtDueDate,
DueDate,
TotalProgress,
numAssignedTo,
numAssignToId,
Priority,
Activity,
PriorityId,
ActivityId,
OrgName,
CompanyRating'
SET @dynamicQuery =  ' INSERT INTO #tempRecords(
'+@FormattedItems+'
) SELECT  COUNT(*) OVER() RecordCount,* FROM (  SELECT 
ac.activityid AS RecordId,
0 AS numCreatedBy,
ISNULL(Div.numTerId,0) AS numOrgTerId,
-2 AS numTaskId,
case when alldayevent = 1 then ''1'' else ''0'' end AS IsTaskClosed,
''<a href="javascript:void(0)">Calendar</a>'' As TaskTypeName,
12 AS TaskType,
CASE WHEN ISNULL(ac.Comments,'''') ='''' THEN ac.ActivityDescription ELSE ISNULL(ac.Comments,'''') END AS OrigDescription,
(SELECT DISTINCT  
replace(replace((SELECT 
CASE(ISNULL(bitpartycalendarTitle,1)) WHEN 1 THEN ''<b><font color=red>Title</font></b>'' + CASE(ISNULL(ac.Subject,'''')) WHEN '''' THEN '''' else ac.Subject END + ''<br>'' else ''''  END  
+ CASE(ISNULL(bitpartycalendarLocation,1)) WHEN 1 THEN ''<b><font color=green>Location</font></b>'' + CASE(ISNULL(ac.location,'''')) WHEN '''' THEN '''' else ac.location END + ''<br>'' else ''''  END  
+ CASE ISNULL(bitpartycalendarDescription,1) 
	WHEN 1 
	THEN CONCAT(''<b><font color=#3c8dbc>Description</font></b>'',
				(CASE 
					WHEN LEN(CASE WHEN ISNULL(ac.Comments,'''') ='''' THEN ac.ActivityDescription ELSE ISNULL(ac.Comments,'''') END) > 100 
					THEN CONCAT(CAST((CASE WHEN ISNULL(ac.Comments,'''') ='''' THEN ac.ActivityDescription ELSE ISNULL(ac.Comments,'''') END) AS VARCHAR(100)),CONCAT(''...'',''<a href="#" role="button" title="Description" data-toggle="popover" data-content="'',(CASE WHEN ISNULL(ac.Comments,'''') ='''' THEN ac.ActivityDescription ELSE ISNULL(ac.Comments,'''') END) ,''"> more</a>'')) 
					ELSE (CASE WHEN ISNULL(ac.Comments,'''') ='''' THEN ac.ActivityDescription ELSE ISNULL(ac.Comments,'''') END) 
				END)) 
	ELSE ''''  
	END  
FROM Domain where numDomainId='+ CAST(@numDomainID AS VARCHAR) +' 
FOR XML PATH('''')
),''&lt;'', ''<''), ''&gt;'', ''>''))   AS Description,
DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', dateadd(second,duration,startdatetimeutc)) AS dtDueDate,
DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', dateadd(second,duration,startdatetimeutc)) as DueDate,
''0'' As TotalProgress,
''-'' As numAssignedTo,
''0'' As numAssignToId,
'''' As Priority, -- Priority column   
'''' As Activity ,
0 As PriorityId, -- Priority column   
 0 As ActivityId ,
 ''<a href="../account/frmAccounts.aspx?DivID=''+CAST(Div.numDivisionID AS VARCHAR)+''">''+Comp.vcCompanyName+ CASE WHEN  ISNULL(listCompanyRating.VcData,'''')<>'''' THEN ''(''+listCompanyRating.VcData+'')'' ELSE '''' END +''</a>''  AS OrgName,
 listCompanyRating.VcData As CompanyRating
 From activity ac join             
activityresource ar on  ac.activityid=ar.activityid             
join resource res on ar.resourceid=res.resourceid                         
--join usermaster UM on um.numUserId = res.numUserCntId                
join AdditionalContactsInformation ACI on ACI.numContactId = res.numUserCntId                
 JOIN DivisionMaster Div                                                          
 ON ACI.numDivisionId = Div.numDivisionID                                   
 JOIN CompanyInfo  Comp                             
 ON Div.numCompanyID = Comp.numCompanyId
  LEFT JOIN ListDetails AS listCompanyRating
 ON Comp.numCompanyRating=listCompanyRating.NumlistItemID
 OUTER APPLY(' + CONCAT('SELECT MAX(bintCreatedDate) AS bintCreatedDate FROM OpportunityMaster WHERE numDomainID = ',@numDomainID,' AND numDivisionID=Div.numDivisionID AND tintOppType = 1 AND tintOppStatus = 1') + ' ) AS TempLastOrder                 
 OUTER APPLY(' + CONCAT('SELECT
																	SUM(monDealAmount) AS monDealAmount
																	,AVG(ProfitPer) AS fltProfitPer
																FROM
																(
																	SELECT
																		OM.numDivisionId
																		,monDealAmount
																		,AVG(((ISNULL(OI.monPrice,0) - (CASE D.numCost WHEN 3 THEN ISNULL(V.monCost,0) * dbo.fn_UOMConversion(I.numBaseUnit,I.numItemCode,I.numDomainID,I.numPurchaseUnit) WHEN 2 THEN ISNULL(OI.numCost,0) ELSE ISNULL(I.monAverageCost,0) END)) / ISNULL(NULLIF(OI.monPrice,0),1)) * 100) ProfitPer
																	FROM
																		OpportunityMaster OM
																	INNER JOIN
																		Domain D
																	ON
																		OM.numDomainID = D.numDomainID
																	INNER JOIN
																		OpportunityItems OI
																	ON
																		OM.numOppId = OI.numOppId
																	INNER JOIN
																		Item I
																	ON
																		OI.numItemCode = I.numItemCode
																	LEFT JOIN
																		Vendor V
																	ON
																		V.numVendorID = I.numVendorID
																		AND V.numItemCode = I.numItemCode
																	WHERE
																		OM.numDomainId = ',@numDomainID,'
																		AND OM.numDivisionId = Div.numDivisionID
																		AND ISNULL(I.bitContainer,0) = 0
																		AND tintOppType = 1
																		AND tintOppStatus=1
																		
																	GROUP BY
																		OM.numDivisionId
																		,OM.numOppId
																		,monDealAmount
																) T1
																GROUP BY
																	T1.numDivisionId )') + ' AS TempPerformance

 where  res.numUserCntId = ' + Cast(@numUserCntID as varchar(10)) +'             
and ac.OriginalStartDateTimeUtc IS NULL            
and res.numDomainID = ' + Cast(@numDomainID as varchar(10)) +'  and 
(DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', startdatetimeutc) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', startdatetimeutc) <= '''+Cast(@endDate as varchar(30))+''')
--(startdatetimeutc between dateadd(month,-1,getutcdate()) and dateadd(month,1,getutcdate())) 
and [status] <> -1                                                     
and ac.activityid not in 
(select distinct(numActivityId) from communication Comm where Comm.numDomainID = ' + Cast(@numDomainID as varchar(10)) +'  
AND 1=(Case ' + Cast(@bitFilterRecord as varchar(10)) +' when 1 then Case when Comm.numAssign =' + Cast(@numUserCntID as varchar(10)) +'  then 1 else 0 end 
		else  Case when (Comm.numAssign =' + Cast(@numUserCntID as varchar(10)) +'  or Comm.numCreatedBy=' + Cast(@numUserCntID as varchar(10)) +' ) then 1 else 0 end end)   
		AND 1 = (CASE ' + CAST( @tintActionItemsViewRights AS VARCHAR) + 
' WHEN 3 THEN 1 
WHEN 2 THEN (CASE WHEN Div.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+ CONVERT(VARCHAR(15),@numUserCntID) + ' ) THEN 1 ELSE 0 END)
WHEN 1 THEN (CASE WHEN (Comm.numCreatedBy= ' + CONVERT(VARCHAR(15),@numUserCntID) + ' or Comm.numAssign= '+ CONVERT(VARCHAR(15),@numUserCntID) + ') THEN 1 ELSE 0 END) 
ELSE 0 END)                                                                   

)) Q WHERE '+@OtherRegularSearch+'  '+@IndivRecordPaging+'
'

EXEC (@dynamicQuery)
-------------------- 1 - CASE----------------------------

SET @dynamicQuery =  ' INSERT INTO #tempRecords(
RecordCount,
RecordId,
numCreatedBy,
numOrgTerId,
numTaskId,
IsTaskClosed,
TaskTypeName,
TaskType,
OrigDescription,
Description,
dtDueDate,
DueDate,
TotalProgress,
numAssignedTo,
numAssignToId,
Priority,
Activity,
PriorityId,
ActivityId,
OrgName,
CompanyRating
) SELECT  COUNT(*) OVER() RecordCount,* FROM (  SELECT 
C.numCaseId AS RecordId,
C.numCreatedBy AS numCreatedBy,
ISNULL(Div.numTerId,0) AS numOrgTerId,
0 AS numTaskId,
CASE WHEN C.numStatus=1008 THEN 1 ELSE 0 END AS IsTaskClosed,
''<a onclick="openCaseDetails(''+CAST(C.numCaseId AS VARCHAR)+'')" href="javascript:void(0)">Case</a>'' As TaskTypeName,
1 AS TaskType,
'''' AS OrigDescription,
C.textSubject AS Description,
cast(C.intTargetResolveDate as datetime) AS dtDueDate,
cast(C.intTargetResolveDate as datetime) as DueDate,
''0'' As TotalProgress,
dbo.fn_GetContactName(C.numAssignedTo) As numAssignedTo,
C.numAssignedTo As numAssignToId,
listdetailsStatus.VcData As Priority, -- Priority column   
'''' As Activity ,
listdetailsStatus.NumlistItemID As PriorityId, -- Priority column   
 0 As ActivityId ,
 ''<a href="../account/frmAccounts.aspx?DivID=''+CAST(Div.numDivisionID AS VARCHAR)+''">''+Com.vcCompanyName+ CASE WHEN  ISNULL(listCompanyRating.VcData,'''')<>'''' THEN ''(''+listCompanyRating.VcData+'')'' ELSE '''' END +''</a>''  AS OrgName,
 listCompanyRating.VcData As CompanyRating

 FROM 
	Cases  C                          
INNER JOIN  
	AdditionalContactsInformation  AddC                          
ON 
	C.numContactId = AddC.numContactId                           
INNER JOIN 
	DivisionMaster Div                           
ON 
	AddC.numDivisionId = Div.numDivisionID AND  
	AddC.numDivisionId = Div.numDivisionID                           
INNER JOIN  
	CompanyInfo  Com                          
ON 
	Div.numCompanyID = Com.numCompanyId    
 LEFT JOIN ListDetails AS listCompanyRating
 ON Com.numCompanyRating=listCompanyRating.NumlistItemID
  Left Join listdetails listdetailsStatus                                                 
 On C.numPriority = listdetailsStatus.NumlistItemID
WHERE
	C.numDomainID = ' + Cast(@numDomainID as varchar(10))  + ' AND C.numStatus <> 136  AND 1 = (CASE ' + CAST( @tintActionItemsViewRights AS VARCHAR) + 
' WHEN 3 THEN 1 
WHEN 2 THEN (CASE WHEN Div.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+ CONVERT(VARCHAR(15),@numUserCntID) + ' ) THEN 1 ELSE 0 END)
WHEN 1 THEN (CASE WHEN (C.numCreatedBy= ' + CONVERT(VARCHAR(15),@numUserCntID) + ' or C.numAssignedTo= '+ CONVERT(VARCHAR(15),@numUserCntID) + ') THEN 1 ELSE 0 END) 
ELSE 0 END)  AND (DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',intTargetResolveDate) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',intTargetResolveDate) <= '''+Cast(@endDate as varchar(30))+''') 
) Q WHERE '+@OtherRegularSearch+' '+@IndivRecordPaging+'
'
EXEC (@dynamicQuery)

-------------------- 2 - Project Task ----------------------------

SET @dynamicQuery =  ' INSERT INTO #tempRecords(
RecordCount,
RecordId,
numCreatedBy,
numOrgTerId,
numTaskId,
IsTaskClosed,
TaskTypeName,
TaskType,
OrigDescription,
Description,
dtDueDate,
DueDate,
TotalProgress,
numAssignedTo,
numAssignToId,
Priority,
Activity,
PriorityId,
ActivityId,
OrgName,
CompanyRating
)  SELECT  COUNT(*) OVER() RecordCount,* FROM (SELECT
T.numProjectId AS RecordId,
T.numCreatedBy AS numCreatedBy,
ISNULL(Div.numTerId,0) AS numOrgTerId,
T.numTaskId AS numTaskId,
T.bitTaskClosed AS IsTaskClosed,
''<a onclick="openProjectTask(''+CAST(T.numProjectId AS VARCHAR)+'')" href="javascript:void(0)">Project Task</a>'' As TaskTypeName,
2 AS TaskType,
'''' AS OrigDescription,
T.vcTaskName AS Description,
CASE WHEN S.bitIsDueDaysUsed=1 THEN CONVERT(VARCHAR(10),DATEADD(DAY,S.intDueDays,CAST(S.dtStartDate AS DATE)), 101)
		ELSE CONVERT(VARCHAR(10),DATEADD(DAY,(SELECT TOP 1 CAST(Items AS DECIMAL(18,2)) FROM Split(dbo.fn_calculateTotalHoursMinutesByStageDetails(T.numStageDetailsId,T.numOppId,T.numProjectId),''_'')),CAST(S.dtStartDate AS DATE)), 101) END AS dtDueDate,
CASE WHEN S.bitIsDueDaysUsed=1 THEN CONVERT(VARCHAR(10),DATEADD(DAY,S.intDueDays,CAST(S.dtStartDate AS DATE)), 101)
		ELSE CONVERT(VARCHAR(10),DATEADD(DAY,(SELECT TOP 1 CAST(Items AS DECIMAL(18,2)) FROM Split(dbo.fn_calculateTotalHoursMinutesByStageDetails(T.numStageDetailsId,T.numOppId,T.numProjectId),''_'')),CAST(S.dtStartDate AS DATE)), 101) END as DueDate,
ISNULL((SELECT TOP 1 intTotalProgress FROM ProjectProgress WHERE numProId = T.numProjectId),0) As TotalProgress,
dbo.fn_GetContactName(T.numAssignTo) As numAssignedTo,
T.numAssignTo As numAssignToId,
'''' As Priority, -- Priority column   
'''' As Activity ,
0 As PriorityId, -- Priority column   
 0 As ActivityId ,
 ''<a href="../account/frmAccounts.aspx?DivID=''+CAST(Div.numDivisionID AS VARCHAR)+''">''+Com.vcCompanyName+ CASE WHEN  ISNULL(listCompanyRating.VcData,'''')<>'''' THEN ''(''+listCompanyRating.VcData+'')'' ELSE '''' END +''</a>''  AS OrgName,
 listCompanyRating.VcData As CompanyRating

 FROM 
	StagePercentageDetailsTask AS T
	LEFT JOIN StagePercentageDetails AS S
	ON S.numStageDetailsId=T.numStageDetailsId AND S.numProjectID=T.numProjectID
	LEFT JOIN ProjectsMaster AS OP 
	ON T.numProjectID=OP.numProId
	LEFT JOIN StagePercentageMaster AS SM
	ON S.numStagePercentageId=SM.numStagePercentageId                        
INNER JOIN 
	DivisionMaster Div                           
ON 
	OP.numDivisionId = Div.numDivisionID                       
INNER JOIN  
	CompanyInfo  Com                          
ON 
	Div.numCompanyID = Com.numCompanyId    
LEFT JOIN ListDetails AS listCompanyRating
 ON Com.numCompanyRating=listCompanyRating.NumlistItemID
WHERE
	T.numDomainID = ' + Cast(@numDomainID as varchar(10))  + ' AND T.numProjectId>0 AND s.numProjectId>0 AND T.bitSavedTask=1 AND T.bitTaskClosed=0 AND 1 = (CASE ' + CAST( @tintActionItemsViewRights AS VARCHAR) + 
' WHEN 3 THEN 1 
WHEN 2 THEN (CASE WHEN Div.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+ CONVERT(VARCHAR(15),@numUserCntID) + ' ) THEN 1 ELSE 0 END)
WHEN 1 THEN (CASE WHEN (T.numCreatedBy= ' + CONVERT(VARCHAR(15),@numUserCntID) + ' or T.numAssignTo= '+ CONVERT(VARCHAR(15),@numUserCntID) + ') THEN 1 ELSE 0 END) 
ELSE 0 END) ) Q WHERE '+@OtherRegularSearch+' AND (DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',Q.DueDate) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',Q.DueDate) <= '''+Cast(@endDate as varchar(30))+''') 
'+@IndivRecordPaging+'
'

EXEC (@dynamicQuery)

------------------ 3 - Opportunity  Task ----------------------------

SET @dynamicQuery =  ' INSERT INTO #tempRecords(
RecordCount,
RecordId,
numCreatedBy,
numOrgTerId,
numTaskId,
IsTaskClosed,
TaskTypeName,
TaskType,
OrigDescription,
Description,
dtDueDate,
DueDate,
TotalProgress,
numAssignedTo,
numAssignToId,
Priority,
Activity,
PriorityId,
ActivityId,
OrgName,
CompanyRating
) SELECT  COUNT(*) OVER() RecordCount,* FROM ( SELECT 
T.numOppId AS RecordId,
T.numCreatedBy AS numCreatedBy,
ISNULL(Div.numTerId,0) AS numOrgTerId,
T.numTaskId AS numTaskId,
T.bitTaskClosed AS IsTaskClosed,
''<a onclick="openTask(''+CAST(T.numOppId AS VARCHAR)+'',''+CAST(S.numStagePercentageId AS VARCHAR)+'',''+CAST(S.tinProgressPercentage AS VARCHAR)+'',''+CAST(S.tintConfiguration AS VARCHAR)+'')" href="javascript:void(0)">Opportunity Task</a>'' As TaskTypeName,
3 AS TaskType,
'''' AS OrigDescription,
T.vcTaskName AS Description,
CASE WHEN S.bitIsDueDaysUsed=1 THEN CONVERT(VARCHAR(10),DATEADD(DAY,S.intDueDays,CAST(S.dtStartDate AS DATE)), 101)
		ELSE CONVERT(VARCHAR(10),DATEADD(DAY,(SELECT TOP 1 CAST(Items AS DECIMAL(18,2)) FROM Split(dbo.fn_calculateTotalHoursMinutesByStageDetails(T.numStageDetailsId,T.numOppId,T.numProjectId),''_'')),CAST(S.dtStartDate AS DATE)), 101) END AS dtDueDate,
CASE WHEN S.bitIsDueDaysUsed=1 THEN CONVERT(VARCHAR(10),DATEADD(DAY,S.intDueDays,CAST(S.dtStartDate AS DATE)), 101)
		ELSE CONVERT(VARCHAR(10),DATEADD(DAY,(SELECT TOP 1 CAST(Items AS DECIMAL(18,2)) FROM Split(dbo.fn_calculateTotalHoursMinutesByStageDetails(T.numStageDetailsId,T.numOppId,T.numProjectId),''_'')),CAST(S.dtStartDate AS DATE)), 101) END as DueDate,
ISNULL((SELECT TOP 1 intTotalProgress FROM ProjectProgress WHERE numOppId = T.numOppId),0) As TotalProgress,
dbo.fn_GetContactName(T.numAssignTo) As numAssignedTo,
T.numAssignTo As numAssignToId,
'''' As Priority, -- Priority column   
'''' As Activity ,
0 As PriorityId, -- Priority column   
 0 As ActivityId ,
 ''<a href="../account/frmAccounts.aspx?DivID=''+CAST(Div.numDivisionID AS VARCHAR)+''">''+Com.vcCompanyName+ CASE WHEN  ISNULL(listCompanyRating.VcData,'''')<>'''' THEN ''(''+listCompanyRating.VcData+'')'' ELSE '''' END +''</a>''  AS OrgName,
 listCompanyRating.VcData As CompanyRating

 FROM 
	StagePercentageDetailsTask AS T
	LEFT JOIN StagePercentageDetails AS S
	ON S.numStageDetailsId=T.numStageDetailsId AND S.numOppId=T.numOppId
	LEFT JOIN OpportunityMaster AS OP 
	ON T.numOppId=OP.numOppId
	LEFT JOIN StagePercentageMaster AS SM
	ON S.numStagePercentageId=SM.numStagePercentageId                        
INNER JOIN 
	DivisionMaster Div                           
ON 
	OP.numDivisionId = Div.numDivisionID                       
INNER JOIN  
	CompanyInfo  Com                          
ON 
	Div.numCompanyID = Com.numCompanyId    
LEFT JOIN ListDetails AS listCompanyRating
 ON Com.numCompanyRating=listCompanyRating.NumlistItemID
WHERE
	T.numDomainID = ' + Cast(@numDomainID as varchar(10))  + ' AND OP.tintOppType=1 AND OP.tintOppStatus=0 AND T.numOppId>0 AND s.numOppId>0 AND T.bitSavedTask=1 AND T.bitTaskClosed=0 AND 1 = (CASE ' + CAST( @tintActionItemsViewRights AS VARCHAR) + 
' WHEN 3 THEN 1 
WHEN 2 THEN (CASE WHEN Div.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+ CONVERT(VARCHAR(15),@numUserCntID) + ' ) THEN 1 ELSE 0 END)
WHEN 1 THEN (CASE WHEN (T.numCreatedBy= ' + CONVERT(VARCHAR(15),@numUserCntID) + ' or T.numAssignTo= '+ CONVERT(VARCHAR(15),@numUserCntID) + ') THEN 1 ELSE 0 END) 
ELSE 0 END) ) Q WHERE '+@OtherRegularSearch+' AND (DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',Q.DueDate) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',Q.DueDate) <= '''+Cast(@endDate as varchar(30))+''')
'+@IndivRecordPaging+' 
 '
 
 EXEC (@dynamicQuery)

------------------ 4 -  Sales Order Task ----------------------------

SET @dynamicQuery =  ' INSERT INTO #tempRecords(
RecordCount,
RecordId,
numCreatedBy,
numOrgTerId,
numTaskId,
IsTaskClosed,
TaskTypeName,
TaskType,
OrigDescription,
Description,
dtDueDate,
DueDate,
TotalProgress,
numAssignedTo,
numAssignToId,
Priority,
Activity,
PriorityId,
ActivityId,
OrgName,
CompanyRating
)SELECT  COUNT(*) OVER() RecordCount,* FROM (  SELECT 
T.numOppId AS RecordId,
T.numCreatedBy AS numCreatedBy,
ISNULL(Div.numTerId,0) AS numOrgTerId,
T.numTaskId AS numTaskId,
T.bitTaskClosed AS IsTaskClosed,
''<a onclick="openTask(''+CAST(T.numOppId AS VARCHAR)+'',''+CAST(S.numStagePercentageId AS VARCHAR)+'',''+CAST(S.tinProgressPercentage AS VARCHAR)+'',''+CAST(S.tintConfiguration AS VARCHAR)+'')" href="javascript:void(0)">Sales Order Task</a>'' As TaskTypeName,
4 AS TaskType,
'''' AS OrigDescription,
T.vcTaskName AS Description,
CASE WHEN S.bitIsDueDaysUsed=1 THEN CONVERT(VARCHAR(10),DATEADD(DAY,S.intDueDays,CAST(S.dtStartDate AS DATE)), 101)
		ELSE CONVERT(VARCHAR(10),DATEADD(DAY,(SELECT TOP 1 CAST(Items AS DECIMAL(18,2)) FROM Split(dbo.fn_calculateTotalHoursMinutesByStageDetails(T.numStageDetailsId,T.numOppId,T.numProjectId),''_'')),CAST(S.dtStartDate AS DATE)), 101) END AS dtDueDate,
CASE WHEN S.bitIsDueDaysUsed=1 THEN CONVERT(VARCHAR(10),DATEADD(DAY,S.intDueDays,CAST(S.dtStartDate AS DATE)), 101)
		ELSE CONVERT(VARCHAR(10),DATEADD(DAY,(SELECT TOP 1 CAST(Items AS DECIMAL(18,2)) FROM Split(dbo.fn_calculateTotalHoursMinutesByStageDetails(T.numStageDetailsId,T.numOppId,T.numProjectId),''_'')),CAST(S.dtStartDate AS DATE)), 101) END as DueDate,
ISNULL((SELECT TOP 1 intTotalProgress FROM ProjectProgress WHERE numOppId = T.numOppId),0) As TotalProgress,
dbo.fn_GetContactName(T.numAssignTo) As numAssignedTo,
T.numAssignTo As numAssignToId,
'''' As Priority, -- Priority column   
'''' As Activity ,
0 As PriorityId, -- Priority column   
 0 As ActivityId ,
 ''<a href="../account/frmAccounts.aspx?DivID=''+CAST(Div.numDivisionID AS VARCHAR)+''">''+Com.vcCompanyName+ CASE WHEN  ISNULL(listCompanyRating.VcData,'''')<>'''' THEN ''(''+listCompanyRating.VcData+'')'' ELSE '''' END +''</a>''  AS OrgName,
 listCompanyRating.VcData As CompanyRating

 FROM 
	StagePercentageDetailsTask AS T
	LEFT JOIN StagePercentageDetails AS S
	ON S.numStageDetailsId=T.numStageDetailsId AND S.numOppId=T.numOppId
	LEFT JOIN OpportunityMaster AS OP 
	ON T.numOppId=OP.numOppId
	LEFT JOIN StagePercentageMaster AS SM
	ON S.numStagePercentageId=SM.numStagePercentageId                        
INNER JOIN 
	DivisionMaster Div                           
ON 
	OP.numDivisionId = Div.numDivisionID                       
INNER JOIN  
	CompanyInfo  Com                          
ON 
	Div.numCompanyID = Com.numCompanyId    
LEFT JOIN ListDetails AS listCompanyRating
 ON Com.numCompanyRating=listCompanyRating.NumlistItemID
WHERE
	T.numDomainID = ' + Cast(@numDomainID as varchar(10))  + ' AND OP.tintOppType=1 AND OP.tintOppStatus=1 AND T.numOppId>0 AND s.numOppId>0 AND T.bitSavedTask=1 AND T.bitTaskClosed=0 AND 1 = (CASE ' + CAST( @tintActionItemsViewRights AS VARCHAR) + 
' WHEN 3 THEN 1 
WHEN 2 THEN (CASE WHEN Div.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+ CONVERT(VARCHAR(15),@numUserCntID) + ' ) THEN 1 ELSE 0 END)
WHEN 1 THEN (CASE WHEN (T.numCreatedBy= ' + CONVERT(VARCHAR(15),@numUserCntID) + ' or T.numAssignTo= '+ CONVERT(VARCHAR(15),@numUserCntID) + ') THEN 1 ELSE 0 END) 
ELSE 0 END) ) Q WHERE '+@OtherRegularSearch+' AND (DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',Q.DueDate) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',Q.DueDate) <= '''+Cast(@endDate as varchar(30))+''')
'+@IndivRecordPaging+'
 '
 EXEC (@dynamicQuery)
  
------------------ 5 -  Work Order Task ----------------------------

SET @dynamicQuery =  ' INSERT INTO #tempRecords(
RecordCount,
RecordId,
numCreatedBy,
numOrgTerId,
numTaskId,
IsTaskClosed,
TaskTypeName,
TaskType,
OrigDescription,
Description,
dtDueDate,
DueDate,
TotalProgress,
numAssignedTo,
numAssignToId,
Priority,
Activity,
PriorityId,
ActivityId,
OrgName,
CompanyRating
)SELECT  COUNT(*) OVER() RecordCount,* FROM ( SELECT 
WorkOrder.numWOID AS RecordId,
WorkOrder.numCreatedBy AS numCreatedBy,
ISNULL(Div.numTerId,0) AS numOrgTerId,
WorkOrder.numWOID AS numTaskId,
(CASE WHEN WorkOrder.numWOStatus = 23184 THEN 1 ELSE 0 END) AS IsTaskClosed,
CONCAT(''<a href="../items/frmWorkOrder.aspx?WOID='',WorkOrder.numWOID,''" target="_blank">Work Order Management</a>'') As TaskTypeName,
5 AS TaskType,
'''' AS OrigDescription,
CONCAT(''<b>'',Item.vcItemName,''</b>'','' ('',WorkOrder.numQtyItemsReq,'')'','' <b>Work Order Status :'', ''</b>'',''<label class="lblWorkOrderStatus" id="'',WorkOrder.numWOID,''"><i class="fa fa-refresh fa-spin"></i></lable>'') AS Description,
DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',WorkOrder.dtmEndDate) AS dtDueDate,
DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',WorkOrder.dtmEndDate) as DueDate,
dbo.GetTotalProgress(WorkOrder.numDomainID,WorkOrder.numWOID,1,1,'''',0) As TotalProgress,
dbo.fn_GetContactName(WorkOrder.numAssignedTo) As numAssignedTo,
WorkOrder.numAssignedTo As numAssignToId,
'''' As Priority, -- Priority column   
'''' As Activity ,
0 As PriorityId, -- Priority column   
 0 As ActivityId ,
 ''<a href="../account/frmAccounts.aspx?DivID=''+CAST(Div.numDivisionID AS VARCHAR)+''">''+Com.vcCompanyName+ CASE WHEN  ISNULL(listCompanyRating.VcData,'''')<>'''' THEN ''(''+listCompanyRating.VcData+'')'' ELSE '''' END +''</a>''  AS OrgName,
 listCompanyRating.VcData As CompanyRating

 FROM 
	WorkOrder
INNER JOIN 
	Item
ON
	WorkOrder.numItemCode = Item.numItemCode
LEFT JOIN 
	OpportunityMaster
ON 
	WorkOrder.numOppId=OpportunityMaster.numOppId
INNER JOIN 
	DivisionMaster Div                           
ON 
	Div.numDivisionId = (CASE WHEN OpportunityMaster.numOppID IS NOT NULL THEN OpportunityMaster.numDivisionID ELSE ' + CAST(@numDomainDivisionID AS VARCHAR) + ' END)                        
INNER JOIN  
	CompanyInfo  Com                          
ON 
	Div.numCompanyID = Com.numCompanyId    
LEFT JOIN 
	ListDetails AS listCompanyRating
ON 
	Com.numCompanyRating=listCompanyRating.NumlistItemID
WHERE
	WorkOrder.numDomainID = ' + Cast(@numDomainID as varchar(10))  + ' 
	AND WorkOrder.numWOStatus <> 23184
	AND WorkOrder.numAssignedTo= '+ CONVERT(VARCHAR(15),@numUserCntID) + '
	AND ((DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',WorkOrder.dtmStartDate) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',WorkOrder.dtmStartDate) <= '''+Cast(@endDate as varchar(30))+''') OR (DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',WorkOrder.dtmEndDate) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',WorkOrder.dtmEndDate) <= '''+Cast(@endDate as varchar(30))+'''))
) Q WHERE '+@OtherRegularSearch+' '+@IndivRecordPaging+'
 '
 EXEC (@dynamicQuery)
------------------ 6 -  Work Center Task ----------------------------

SET @dynamicQuery =  ' INSERT INTO #tempRecords(
RecordCount,
RecordId,
numCreatedBy,
numOrgTerId,
numTaskId,
IsTaskClosed,
TaskTypeName,
TaskType,
OrigDescription,
Description,
dtDueDate,
DueDate,
TotalProgress,
numAssignedTo,
numAssignToId,
Priority,
Activity,
PriorityId,
ActivityId,
OrgName,
CompanyRating
) SELECT  COUNT(*) OVER() RecordCount,* FROM ( SELECT 
T.numTaskId AS RecordId,
T.numCreatedBy AS numCreatedBy,
ISNULL(Div.numTerId,0) AS numOrgTerId,
T.numTaskId AS numTaskId,
(CASE WHEN EXISTS (SELECT * FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID=T.numTaskID AND SPDTTL.tintAction=4) THEN 1 ELSE 0 END) AS IsTaskClosed,
CONCAT(''<a href="../items/frmWorkOrder.aspx?WOID='',W.numWOID,''" target="_blank">Work Center Task</a>'') As TaskTypeName,
6 AS TaskType,
'''' AS OrigDescription,
T.vcTaskName AS Description,
DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',W.dtmEndDate) AS dtDueDate,
DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',W.dtmEndDate) as DueDate,
(CASE 
	WHEN EXISTS (SELECT * FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID=T.numTaskID AND SPDTTL.tintAction=4) THEN CAST(100 AS INT)
	ELSE CAST(((ISNULL((SELECT SUM(numProcessedQty) FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID=T.numTaskID),0) / W.numQtyItemsReq) * 100) AS INT)
END) As TotalProgress,
dbo.fn_GetContactName(T.numAssignTo) As numAssignedTo,
T.numAssignTo As numAssignToId,
'''' As Priority, -- Priority column   
'''' As Activity ,
0 As PriorityId, -- Priority column   
 0 As ActivityId ,
 ''<a href="../account/frmAccounts.aspx?DivID=''+CAST(Div.numDivisionID AS VARCHAR)+''">''+Com.vcCompanyName+ CASE WHEN  ISNULL(listCompanyRating.VcData,'''')<>'''' THEN ''(''+listCompanyRating.VcData+'')'' ELSE '''' END +''</a>''  AS OrgName,
 listCompanyRating.VcData As CompanyRating

 FROM 
	StagePercentageDetailsTask AS T
	INNER JOIN WorkOrder AS W 
	ON T.numWorkOrderId=W.numWOId     
LEFT JOIN OpportunityMaster AS OP 
	ON W.numOppId=OP.numOppId
INNER JOIN 
	DivisionMaster Div                           
ON 
	Div.numDivisionId = (CASE WHEN OP.numOppID IS NOT NULL THEN OP.numDivisionID ELSE ' + CAST(@numDomainDivisionID AS VARCHAR) + ' END)                         
INNER JOIN  
	CompanyInfo  Com                          
ON 
	Div.numCompanyID = Com.numCompanyId    
LEFT JOIN ListDetails AS listCompanyRating
 ON Com.numCompanyRating=listCompanyRating.NumlistItemID
WHERE
	W.numDomainID = ' + Cast(@numDomainID as varchar(10))  + '
	AND W.numWOStatus <> 23184 
	AND NOT EXISTS (SELECT * FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID=T.numTaskID AND SPDTTL.tintAction=4) 
	AND T.numAssignTo = ' + CAST(@numUserCntID AS VARCHAR) + '
	AND ((DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',W.dtmStartDate) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',W.dtmStartDate) <= '''+Cast(@endDate as varchar(30))+''') OR (DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',W.dtmEndDate) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',W.dtmEndDate) <= '''+Cast(@endDate as varchar(30))+''')) 
) Q WHERE '+@OtherRegularSearch+' '+@IndivRecordPaging+'
'
EXEC (@dynamicQuery)

------------------ 7 -  Requisition Approval ----------------------------

IF(@bitREQPOApproval=1 AND @numUserCntID IN(SELECT * FROM Split(@vchREQPOApprovalEmp,',') WHERE Items<>''))
BEGIN
SET @dynamicQuery =  ' INSERT INTO #tempRecords(
RecordCount,
RecordId,
numCreatedBy,
numOrgTerId,
numTaskId,
IsTaskClosed,
TaskTypeName,
TaskType,
OrigDescription,
Description,
dtDueDate,
DueDate,
TotalProgress,
numAssignedTo,
numAssignToId,
Priority,
Activity,
PriorityId,
ActivityId,
OrgName,
CompanyRating
) SELECT  COUNT(*) OVER() RecordCount,* FROM( SELECT 
OP.numOppId AS RecordId,
OP.numCreatedBy AS numCreatedBy,
ISNULL(Div.numTerId,0) AS numOrgTerId,
0 AS numTaskId,
CASE WHEN ISNULL(OP.intReqPOApproved,0)=0 THEN 0 ELSE 1 END AS IsTaskClosed,
''<a  onclick="openSalesOrder(''+CAST(OP.numOppId AS VARCHAR)+'')" href="javascript:void(0)">Requisition Approval</a>'' As TaskTypeName,
7 AS TaskType,
'''' AS OrigDescription,
''<b>For :</b> <a class="underLineHyperLink" onclick="openSalesOrder(''+CAST(OP.numOppId AS VARCHAR)+'')" href="javascript:void(0)">''+OP.vcPOppName+''</a> &nbsp;<i onclick="ApproveRejectPORequistion(''+CAST(OP.numOppId AS VARCHAR)+'',1)"  class="fa fa-thumbs-up cursor"></i>  &nbsp;<i onclick="ApproveRejectPORequistion(''+CAST(OP.numOppId AS VARCHAR)+'',2)"  class="fa fa-thumbs-down cursor"></i>'' AS Description,
OP.intPEstimatedCloseDate AS dtDueDate,
OP.intPEstimatedCloseDate as DueDate,
0 As TotalProgress,
dbo.fn_GetContactName('+CAST(@numUserCntID AS VARCHAR(200))+') As numAssignedTo,
'+CAST(@numUserCntID AS VARCHAR(200))+' As numAssignToId,
'''' As Priority, -- Priority column   
'''' As Activity ,
0 As PriorityId, -- Priority column   
 0 As ActivityId ,
 ''<a href="../account/frmAccounts.aspx?DivID=''+CAST(Div.numDivisionID AS VARCHAR)+''">''+Com.vcCompanyName+ CASE WHEN  ISNULL(listCompanyRating.VcData,'''')<>'''' THEN ''(''+listCompanyRating.VcData+'')'' ELSE '''' END +''</a>''  AS OrgName,
 listCompanyRating.VcData As CompanyRating

 FROM  
 OpportunityMaster AS OP 
INNER JOIN 
	DivisionMaster Div                           
ON 
	OP.numDivisionId = Div.numDivisionID                       
INNER JOIN  
	CompanyInfo  Com                          
ON 
	Div.numCompanyID = Com.numCompanyId    
LEFT JOIN ListDetails AS listCompanyRating
 ON Com.numCompanyRating=listCompanyRating.NumlistItemID
WHERE
	OP.numDomainID = ' + Cast(@numDomainID as varchar(10))  + ' AND OP.tintOppType = 2 AND OP.tintOppStatus=0 
	AND 1 = (CASE WHEN '+CAST(@decReqPOMinValue AS VARCHAR(100))+'>0 AND Op.monPAmount>'+CAST(@decReqPOMinValue AS VARCHAR(100))+' THEN 1  WHEN '+CAST(@decReqPOMinValue AS VARCHAR(100))+'=0 THEN 1  ELSE 0 END ) ) AS Q
WHERE '+@OtherRegularSearch+' AND (DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',Q.DueDate) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',Q.DueDate) <= '''+Cast(@endDate as varchar(30))+''') 
'+@IndivRecordPaging+'
'
EXEC (@dynamicQuery)

END
------------------ 8 -  PO Approval ----------------------------
IF(@bitREQPOApproval=1 AND @numUserCntID IN(SELECT * FROM Split(@vchREQPOApprovalEmp,',') WHERE Items<>''))
BEGIN
SET @dynamicQuery =  ' INSERT INTO #tempRecords(
RecordCount,
RecordId,
numCreatedBy,
numOrgTerId,
numTaskId,
IsTaskClosed,
TaskTypeName,
TaskType,
OrigDescription,
Description,
dtDueDate,
DueDate,
TotalProgress,
numAssignedTo,
numAssignToId,
Priority,
Activity,
PriorityId,
ActivityId,
OrgName,
CompanyRating
) SELECT  COUNT(*) OVER() RecordCount,* FROM ( SELECT
OP.numOppId AS RecordId,
OP.numCreatedBy AS numCreatedBy,
ISNULL(Div.numTerId,0) AS numOrgTerId,
0 AS numTaskId,
CASE WHEN ISNULL(OP.intReqPOApproved,0)=0 THEN 0 ELSE 1 END AS IsTaskClosed,
''<a onclick="openSalesOrder(''+CAST(OP.numOppId AS VARCHAR)+'')" href="javascript:void(0)">PO Approval</a>'' As TaskTypeName,
8 AS TaskType,
'''' AS OrigDescription,
''<b>For:</b> <a href="javascript:void(0)"  class="underLineHyperLink" onclick="openSalesOrder(''+CAST(OP.numOppId AS VARCHAR)+'')">''+OP.vcPOppName+''</a> &nbsp;<i onclick="ApproveRejectPORequistion(''+CAST(OP.numOppId AS VARCHAR)+'',1)"  class="fa fa-thumbs-up cursor"></i>  &nbsp;<i onclick="ApproveRejectPORequistion(''+CAST(OP.numOppId AS VARCHAR)+'',2)"  class="fa fa-thumbs-down cursor"></i> '' AS Description,
OP.intPEstimatedCloseDate AS dtDueDate,
OP.intPEstimatedCloseDate as DueDate,
0 As TotalProgress,
dbo.fn_GetContactName('+CAST(@numUserCntID AS VARCHAR(200))+') As numAssignedTo,
'+CAST(@numUserCntID AS VARCHAR(200))+' As numAssignToId,
'''' As Priority, -- Priority column   
'''' As Activity ,
0 As PriorityId, -- Priority column   
 0 As ActivityId ,
 ''<a href="../account/frmAccounts.aspx?DivID=''+CAST(Div.numDivisionID AS VARCHAR)+''">''+Com.vcCompanyName+ CASE WHEN  ISNULL(listCompanyRating.VcData,'''')<>'''' THEN ''(''+listCompanyRating.VcData+'')'' ELSE '''' END +''</a>''  AS OrgName,
 listCompanyRating.VcData As CompanyRating

 FROM  
 OpportunityMaster AS OP 
INNER JOIN 
	DivisionMaster Div                           
ON 
	OP.numDivisionId = Div.numDivisionID                       
INNER JOIN  
	CompanyInfo  Com                          
ON 
	Div.numCompanyID = Com.numCompanyId    
LEFT JOIN ListDetails AS listCompanyRating
 ON Com.numCompanyRating=listCompanyRating.NumlistItemID
WHERE
	OP.numDomainID = ' + Cast(@numDomainID as varchar(10))  + ' AND OP.tintOppType = 2 AND OP.tintOppStatus=1 
	AND 1 = (CASE WHEN '+CAST(@decReqPOMinValue AS VARCHAR(100))+'>0 AND Op.monPAmount>'+CAST(@decReqPOMinValue AS VARCHAR(100))+' THEN 1  WHEN '+CAST(@decReqPOMinValue AS VARCHAR(100))+'=0 THEN 1  ELSE 0 END )) AS Q
	WHERE  '+@OtherRegularSearch+' AND  (DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',Q.DueDate) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',Q.DueDate) <= '''+Cast(@endDate as varchar(30))+''') 
'+@IndivRecordPaging+'
'
EXEC (@dynamicQuery)

END

------------------ 9 -  Price Margin Approval ----------------------------

SET @dynamicQuery =  ' INSERT INTO #tempRecords(
RecordCount,
RecordId,
numCreatedBy,
numOrgTerId,
numTaskId,
IsTaskClosed,
TaskTypeName,
TaskType,
OrigDescription,
Description,
dtDueDate,
DueDate,
TotalProgress,
numAssignedTo,
numAssignToId,
Priority,
Activity,
PriorityId,
ActivityId,
OrgName,
CompanyRating
) SELECT COUNT(*) OVER() RecordCount,* FROM (SELECT
OP.numOppId AS RecordId,
OP.numCreatedBy AS numCreatedBy,
ISNULL(Div.numTerId,0) AS numOrgTerId,
0 AS numTaskId,
ISNULL(OP.bitReqPOApproved,0) AS IsTaskClosed,
''<a onclick="OpenMarginApproval(''+CAST(OP.numOppId AS VARCHAR)+'')" href="javascript:void(0)">Price Margin Approval</a>'' As TaskTypeName,
9 AS TaskType,
'''' AS OrigDescription,
''<b>For : </b> <a href="javascript:void(0)"  onclick="openSalesOrder(''+CAST(OP.numOppId AS VARCHAR)+'')">''+OP.vcPOppName+''</a> : '' +dbo.ItemUnitPriceApproval_CheckAndGetItemDetails(OP.numOppId,OP.numDomainId)+''  &nbsp;<i onclick="ApproveRejectPriceMarginApproval(''+CAST(OP.numOppId AS VARCHAR)+'',1)" class="fa fa-thumbs-up cursor"></i>  &nbsp;<i onclick="ApproveRejectPriceMarginApproval(''+CAST(OP.numOppId AS VARCHAR)+'',2)" class="fa fa-thumbs-down cursor"></i>''  AS Description,
OP.bintCreatedDate AS dtDueDate,
OP.bintCreatedDate as DueDate,
0 As TotalProgress,
dbo.fn_GetContactName('+CAST(@numUserCntID AS VARCHAR(200))+') As numAssignedTo,
'+CAST(@numUserCntID AS VARCHAR(200))+' As numAssignToId,
'''' As Priority, -- Priority column   
'''' As Activity ,
0 As PriorityId, -- Priority column   
 0 As ActivityId ,
 ''<a href="../account/frmAccounts.aspx?DivID=''+CAST(Div.numDivisionID AS VARCHAR)+''">''+Com.vcCompanyName+ CASE WHEN  ISNULL(listCompanyRating.VcData,'''')<>'''' THEN ''(''+listCompanyRating.VcData+'')'' ELSE '''' END +''</a>''  AS OrgName,
 listCompanyRating.VcData As CompanyRating

 FROM  
 OpportunityMaster AS OP 
INNER JOIN 
	DivisionMaster Div                           
ON 
	OP.numDivisionId = Div.numDivisionID                       
INNER JOIN  
	CompanyInfo  Com                          
ON 
	Div.numCompanyID = Com.numCompanyId    
LEFT JOIN ListDetails AS listCompanyRating
 ON Com.numCompanyRating=listCompanyRating.NumlistItemID
WHERE
	OP.numDomainID = ' + Cast(@numDomainID as varchar(10))  + ' AND OP.tintOppType = 1 AND (SELECT COUNT(OpportunityItems.numoppitemtCode) FROM OpportunityItems 
						WHERE OpportunityItems.bitItemPriceApprovalRequired=1 AND OpportunityItems.numOppId=OP.numOppId) >0
	AND '+CAST(@numUserCntID AS VARCHAR(100))+' IN (SELECT numUserID FROM UnitPriceApprover WHERE numDomainId= ' + Cast(@numDomainID as varchar(10))  + ' )) AS Q
	WHERE  '+@OtherRegularSearch+' AND  (DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',Q.DueDate) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',Q.DueDate) <= '''+Cast(@endDate as varchar(30))+''') 
'+@IndivRecordPaging+'

'
EXEC (@dynamicQuery)

------------------------10 - A/R Invoice Due------------------------------------
IF(@bitARInvoiceDue=1 AND @numUserCntID IN(SELECT * FROM Split(@vchARInvoiceDue,',') WHERE Items<>''))
BEGIN
 DECLARE  @AuthoritativeSalesBizDocId  AS INTEGER
    SELECT @AuthoritativeSalesBizDocId = isnull(numAuthoritativeSales,0)
    FROM   AuthoritativeBizDocs
    WHERE  numDomainId = @numDomainId;
SET @dynamicQuery=  ' INSERT INTO #tempRecords(
RecordCount,
RecordId,
numCreatedBy,
numOrgTerId,
numTaskId,
IsTaskClosed,
TaskTypeName,
TaskType,
OrigDescription,
Description,
dtDueDate,
DueDate,
TotalProgress,
numAssignedTo,
numAssignToId,
Priority,
Activity,
PriorityId,
ActivityId,
OrgName,
CompanyRating
) SELECT  COUNT(*) OVER() RecordCount,* FROM ( SELECT
OM.numOppId AS RecordId,
OM.numCreatedBy AS numCreatedBy,
ISNULL(DM.numTerId,0) AS numOrgTerId,
OB.numOppBizDocsId AS numTaskId,
0 AS IsTaskClosed,
''<a href="javascript:void(0)"  onclick="openBizDoc(''+CAST(OM.numOppId AS VARCHAR)+'',''+CAST(OB.numOppBizDocsId AS VARCHAR)+'')">A/R Invoice Due</a>'' As TaskTypeName,
10 AS TaskType,
'''' AS OrigDescription,
''<b>For : </b> <a href="javascript:void(0)" class="underLineHyperLink" onclick="openBizDoc(''+CAST(OM.numOppId AS VARCHAR)+'',''+CAST(OB.numOppBizDocsId AS VARCHAR)+'')">''+CAST(OB.[vcBizDocID] AS VARCHAR)+''</a> of <a href="javascript:void(0)" class="underLineHyperLink" onclick="openSalesOrder(''+CAST(OM.numOppId AS VARCHAR)+'')">''+CAST(OM.[vcPOppName] AS VARCHAR)+''</a> <b>Balance Due</b> <a href="javascript:void(0)" onclick="OpenAmtPaid(''+CAST(OM.numOppId AS VARCHAR)+'',''+CAST(OB.numOppBizDocsId AS VARCHAR)+'',''+CAST(OM.[numDivisionId] AS VARCHAR)+'')"   class="underLineHyperLink">'' + CAST(ISNULL(C.[varCurrSymbol],'''') AS VARCHAR) + '''' + CAST(CAST(isnull(OB.monDealAmount  - ISNULL(TablePayments.monPaidAmount,0),0) AS DECIMAL(18,2)) AS VARCHAR)+''</a>''  AS Description,
 CASE ISNULL(bitBillingTerms,0) 
                          WHEN 1 THEN DATEADD(dd,convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0)),OB.dtFromDate)
                          WHEN 0 THEN ob.[dtFromDate]
                        END AS dtDueDate,
 CASE ISNULL(bitBillingTerms,0) 
                          WHEN 1 THEN [dbo].[FormatedDateFromDate](DATEADD(dd,convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0)),OB.dtFromDate),
                                                                   1)
                          WHEN 0 THEN [dbo].[FormatedDateFromDate](ob.[dtFromDate],1)
                        END as DueDate,
0 As TotalProgress,
'''' As numAssignedTo,
0 As numAssignToId,
'''' As Priority, -- Priority column   
'''' As Activity ,
0 As PriorityId, -- Priority column   
 0 As ActivityId ,
 ''<a href="../account/frmAccounts.aspx?DivID=''+CAST(DM.numDivisionID AS VARCHAR)+''">''+Com.vcCompanyName+ CASE WHEN  ISNULL(listCompanyRating.VcData,'''')<>'''' THEN ''(''+listCompanyRating.VcData+'')'' ELSE '''' END +''</a>''  AS OrgName,
 listCompanyRating.VcData As CompanyRating

 FROM   [OpportunityMaster] OM INNER JOIN [DivisionMaster] DM ON OM.[numDivisionId] = DM.[numDivisionID]
				LEFT JOIN CompanyInfo  Com ON DM.numCompanyID = Com.numCompanyId    
				LEFT JOIN ListDetails AS listCompanyRating
 ON Com.numCompanyRating=listCompanyRating.NumlistItemID
               LEFT OUTER JOIN OpportunityBizDocs OB ON OB.numOppId = OM.numOppID
               LEFT OUTER JOIN [Currency] C ON OM.[numCurrencyID] = C.[numCurrencyID]
			   OUTER APPLY
			   (
					SELECT 
						SUM(monAmountPaid) AS monPaidAmount
					FROM
						DepositeDetails DD
					INNER JOIN
						DepositMaster DM
					ON
						DD.numDepositID=DM.numDepositId
					WHERE 
						numOppID = OM.numOppID
						AND numOppBizDocsID=OB.numOppBizDocsID
						AND CONVERT(DATE,DM.dtDepositDate) <= DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +','''+Cast(@endDate as varchar(100))+''')
				) TablePayments
        WHERE  OM.[tintOppType] = 1
               AND OM.[tintOppStatus] = 1
               AND om.[numDomainId] = ' + Cast(@numDomainId as varchar(10))  + '
               AND OB.[numBizDocId] = '+CAST(@AuthoritativeSalesBizDocId As VARCHAR)+'
               AND OB.bitAuthoritativeBizDocs=1
               AND ISNULL(OB.tintDeferred,0) <> 1
			   AND (OM.numAccountClass= 0 OR 0=0)
			   AND CONVERT(DATE,OB.[dtCreatedDate]) <= DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +','''+Cast(@endDate as varchar(100))+''')
               AND isnull(OB.monDealAmount
                            ,0) > 0  AND isnull(OB.monDealAmount * OM.fltExchangeRate
                        - ISNULL(TablePayments.monPaidAmount,0) * OM.fltExchangeRate,0) != 0
	) AS Q
	WHERE  '+@OtherRegularSearch+' AND  (DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',Q.DueDate) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',Q.DueDate) <= '''+Cast(@endDate as varchar(30))+''') 
'+@IndivRecordPaging+'
'
EXEC (@dynamicQuery)
END
------------------------11 - A/P Bill Due------------------------------------
IF(@bitAPBillsDue=1 AND @numUserCntID IN(SELECT * FROM Split(@vchAPBillsDue,',') WHERE Items<>''))
BEGIN   
   DECLARE  @AuthoritativePurchaseBizDocId  AS INTEGER
    SELECT  @AuthoritativePurchaseBizDocId= isnull(numAuthoritativePurchase,0)
    FROM   AuthoritativeBizDocs
    WHERE  numDomainId =  @numDomainId
	

SET @dynamicQuery = '  INSERT INTO #tempRecords(
RecordCount,
RecordId,
numCreatedBy,
numOrgTerId,
numTaskId,
IsTaskClosed,
TaskTypeName,
TaskType,
OrigDescription,
Description,
dtDueDate,
DueDate,
TotalProgress,
numAssignedTo,
numAssignToId,
Priority,
Activity,
PriorityId,
ActivityId,
OrgName,
CompanyRating
) SELECT  COUNT(*) OVER() RecordCount,* FROM (SELECT
OM.numOppId AS RecordId,
OM.numCreatedBy AS numCreatedBy,
ISNULL(DM.numTerId,0) AS numOrgTerId,
OB.numOppBizDocsId AS numTaskId,
0 AS IsTaskClosed,
''<a href="javascript:void(0)" onclick="openPurchaseBill(''+CAST(DM.[numDivisionID] As VARCHAR)+'')">A/P Bill Due<a/>'' As TaskTypeName,
11 AS TaskType,
'''' AS OrigDescription,
''<b>For : </b> <a href="javascript:void(0)" class="underLineHyperLink" onclick="openBizDoc(''+CAST(OM.numOppId AS VARCHAR)+'',''+CAST(OB.numOppBizDocsId AS VARCHAR)+'')">''+CAST(OB.[vcBizDocID] AS VARCHAR)+''</a> of <a href="javascript:void(0)" class="underLineHyperLink" onclick="openSalesOrder(''+CAST(OM.numOppId AS VARCHAR)+'')">''+CAST(OM.[vcPOppName] AS VARCHAR)+''</a> <b>Balance Due : </b> <a href="javascript:void(0)" onclick="openBizDoc(''+CAST(OM.numOppId AS VARCHAR)+'',''+CAST(OB.numOppBizDocsId AS VARCHAR)+'')"   class="underLineHyperLink">'' + CAST(ISNULL(C.[varCurrSymbol],'''') AS VARCHAR) + '''' + CAST(CAST(isnull((OB.monDealAmount * OM.fltExchangeRate)  - (ISNULL(TablePayment.monPaidAmount,0)* OM.fltExchangeRate),0) AS DECIMAL(18,2))  AS VARCHAR)+''</a>''  AS Description,
 CASE ISNULL(bitBillingTerms,0) 
                          WHEN 1 THEN DATEADD(dd,convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0)),OB.dtFromDate)
                          WHEN 0 THEN ob.[dtFromDate]
                        END AS dtDueDate,
 CASE ISNULL(bitBillingTerms,0) 
                          WHEN 1 THEN [dbo].[FormatedDateFromDate](DATEADD(dd,convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0)),OB.dtFromDate),
                                                                   1)
                          WHEN 0 THEN [dbo].[FormatedDateFromDate](ob.[dtFromDate],1)
                        END as DueDate,
0 As TotalProgress,
'''' As numAssignedTo,
0 As numAssignToId,
'''' As Priority, -- Priority column   
'''' As Activity ,
0 As PriorityId, -- Priority column   
 0 As ActivityId ,
 ''<a href="../account/frmAccounts.aspx?DivID=''+CAST(DM.numDivisionID AS VARCHAR)+''">''+Com.vcCompanyName+ CASE WHEN  ISNULL(listCompanyRating.VcData,'''')<>'''' THEN ''(''+listCompanyRating.VcData+'')'' ELSE '''' END +''</a>''  AS OrgName,
 listCompanyRating.VcData As CompanyRating

   FROM   [OpportunityMaster] OM
               INNER JOIN [DivisionMaster] DM
                 ON OM.[numDivisionId] = DM.[numDivisionID]
				 LEFT JOIN CompanyInfo  Com ON DM.numCompanyID = Com.numCompanyId    
				LEFT JOIN ListDetails AS listCompanyRating
 ON Com.numCompanyRating=listCompanyRating.NumlistItemID
               LEFT OUTER JOIN OpportunityBizDocs OB
                 ON OB.numOppId = OM.numOppID
               LEFT OUTER JOIN [Currency] C
                 ON OM.[numCurrencyID] = C.[numCurrencyID]
				 OUTER APPLY
				(
					SELECT
						SUm(BPD.monAmount) monPaidAmount
					FROM	
						BillPaymentDetails BPD
					INNER JOIN
						BillPaymentHeader BPH
					ON
						BPD.numBillPaymentID=BPH.numBillPaymentID
					WHERE
						BPD.numOppBizDocsID = OB.numOppBizDocsId
						AND CAST(BPH.dtPaymentDate AS DATE) <= DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +','''+Cast(@endDate as varchar(100))+''')
				) TablePayment
        WHERE  OM.[tintOppType] = 2
               AND OM.[tintOppStatus] = 1
               AND om.[numDomainId] = 1
               AND OB.[numBizDocId] = '+CAST(@AuthoritativePurchaseBizDocId AS VARCHAR)+' 
               AND OB.bitAuthoritativeBizDocs=1
               AND ISNULL(OB.tintDeferred,0) <> 1
			   AND (OM.numAccountClass= 0 OR 0=0)
			   AND CONVERT(DATE,OB.[dtCreatedDate]) <= DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +','''+Cast(@endDate as varchar(100))+''')
               AND  isnull(OB.monDealAmount * OM.fltExchangeRate,0) > 0 
			   AND isnull(OB.monDealAmount * OM.fltExchangeRate
                        - ISNULL(TablePayment.monPaidAmount,0) * OM.fltExchangeRate,0) > 0
	) AS Q
	WHERE  '+@OtherRegularSearch+' AND  (DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',Q.DueDate) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',Q.DueDate) <= '''+Cast(@endDate as varchar(30))+''') 
'+@IndivRecordPaging+'
'

EXEC (@dynamicQuery)
END

END

SET @FormattedItems =''
SET @FormattedItems=' UPDATE #tempRecords SET DueDate = CASE WHEN convert(varchar(11),DueDate)=convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',getdate())) then ''<b><font color=red>Today</font></b>''
WHEN convert(varchar(11),DueDate)=convert(varchar(11),DateAdd(day,-1,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',getdate()))) then ''<b><font color=purple>Yesterday</font></b>''
WHEN convert(varchar(11),DueDate)=convert(varchar(11),DateAdd(day,1,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',getdate()))) then ''<b><font color=orange>Tommorow</font></b>''
ELSE  dbo.FormatedDateFromDate(DueDate,'+ CONVERT(VARCHAR(10),@numDomainId)+') END '

 EXEC (@FormattedItems)
 DECLARE @strSql3 VARCHAR(MAX)=''
 IF LEN(@columnName) > 0
BEGIN
	SET @strSql3= CONCAT(@strSql3,' order by ',CASE WHEN @columnName = 'Action-Item Participants' THEN CONCAT('[','Action-Item Participants',']') ELSE @columnName END,' ',@columnSortOrder)
END
ELSE
BEGIN
	SET @strSql3= CONCAT(@strSql3,' ORDER BY dtDueDate DESC  ')
END
PRINT @strSql3
SET @TotalRecordCount=(SELECT ISNULL(MAX(RecordCount),0) FROM #tempRecords)

IF(@TotalRecordCount>0)
BEGIN
	DECLARE @totalPageRequired AS float = CEILING((@TotalRecordCount/CAST(10 AS float)))
	DECLARE @totalPageSize AS INT=0
	SET @totalPageSize = (SELECT TOP 1 tintCustomPagingRows FROM Domain WHERE numDomainId=@numDomainID)
	SET @TotalRecordCount = @totalPageRequired*@totalPageSize
END
SET @dynamicQuery = 'SELECT '+CAST(@TotalRecordCount AS varchar(400))+' AS TotalRecords,T.*
 FROM #tempRecords T WHERE 1=1 '+@strSql3


EXEC(@dynamicQuery)
DECLARE @TempColumns TABLE
	(
        vcFieldName VARCHAR(200)
        ,vcDbColumnName VARCHAR(200)
        ,vcOrigDbColumnName VARCHAR(200)
        ,bitAllowSorting BIT
        ,numFieldId INT
        ,bitAllowEdit BIT
        ,bitCustomField BIT
        ,vcAssociatedControlType VARCHAR(50)
        ,vcListItemType  VARCHAR(200)
        ,numListID INT
        ,ListRelID INT
        ,vcLookBackTableName VARCHAR(200)
        ,bitAllowFiltering BIT
        ,vcFieldDataType  VARCHAR(20)
		,intColumnWidth INT
		,bitClosedColumn BIT
		,bitFieldMessage BIT DEFAULT 0
		,vcFieldMessage VARCHAR(500)
		,bitIsRequired BIT DEFAULT 0
		,bitIsNumeric BIT DEFAULT 0
		,bitIsAlphaNumeric BIT DEFAULT 0
		,bitIsEmail BIT DEFAULT 0
		,bitIsLengthValidation  BIT DEFAULT 0
		,intMaxLength INT DEFAULT 0
		,intMinLength INT DEFAULT 0
	)



		declare @Nocolumns as tinyint         
set @Nocolumns=0       
         
Select @Nocolumns=isnull(count(*),0) from View_DynamicColumns where numFormId=43 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 
  
  
if @Nocolumns > 0            
begin      
INSERT INTO #tempForm
select tintRow+1 as tintOrder,vcDbColumnName,vcFieldDataType,vcOrigDbColumnName,bitAllowFiltering,ISNULL(vcCultureFieldName,vcFieldName),                  
 vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName                                                 
,bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth
 FROM View_DynamicColumns 
 where numFormId=43 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 
  AND ISNULL(bitSettingField,0)=1 AND ISNULL(bitCustom,0)=0
  order by tintOrder asc  
  
end            
else            
begin 

	--Check if Master Form Configuration is created by administrator if NoColumns=0
	DECLARE @IsMasterConfAvailable BIT = 0
	DECLARE @numUserGroup NUMERIC(18,0)

	SELECT @numUserGroup = numGroupID FROM UserMaster WHERE numUserDetailId = @numUserCntID 

	IF (SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = @numDomainID AND numFormID = 43 AND bitGridConfiguration = 1 AND numGroupID = @numUserGroup) > 0
	BEGIN
		SET @IsMasterConfAvailable = 1
	END

	--If MasterConfiguration is available then load it otherwise load default columns
	IF @IsMasterConfAvailable = 1
	BEGIN
		INSERT INTO DycFormConfigurationDetails 
		(
			numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth
		)
		SELECT 
			43,numFieldId,intColumnNum,intRowNum,@numDomainID,@numUserCntID,NULL,1,0,intColumnWidth
		FROM 
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId=43 AND 
			View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
			ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0

		INSERT INTO #tempForm
		SELECT 
			(intRowNum + 1) as tintOrder, vcDbColumnName,vcFieldDataType,vcOrigDbColumnName,bitAllowFiltering, ISNULL(vcCultureFieldName,vcFieldName), vcAssociatedControlType,
			vcListItemType,numListID,vcLookBackTableName, bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth
		FROM 
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId=43 AND 
			View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
			ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
	ORDER BY 
		tintOrder ASC  
	END
	ELSE                                        
	BEGIN
		 INSERT INTO #tempForm
		select tintOrder,vcDbColumnName,vcFieldDataType
		,vcOrigDbColumnName,bitAllowFiltering,ISNULL(vcCultureFieldName,vcFieldName),                  
		 vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName                                                 
		,bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
		bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
		intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth
		 FROM View_DynamicDefaultColumns
		 where numFormId=43 and bitDefault=1 AND ISNULL(bitSettingField,0)=1 AND numDomainID=@numDomainID
		order by tintOrder asc  
	END 
enD

DELETE FROM #tempForm WHERE vcOrigDbColumnName IN('numActivity','numStatus','intTotalProgress','textDetails','vcCompanyName','bitTask','numAssignedTo','FormattedDate')
--UPDATE #tempForm SET bitCustomField=1
INSERT INTO #tempForm
		(
			tintOrder
			,vcFieldName
			,vcDbColumnName
			,vcOrigDbColumnName
			,bitAllowSorting
			,numFieldId
			,bitAllowEdit
			,bitCustomField
			,vcAssociatedControlType
			,vcListItemType
			,numListID
			,ListRelID
			,vcLookBackTableName
			,bitAllowFiltering
			,vcFieldDataType
			
		)
		VALUES
		(-9,'Type','TaskTypeName','TaskTypeName',0,1,0,0,'SelectBox','',0,0,'T',1,'V')
		,(-8,'Descriptions & Comments','Description','Description',0,184,1,0,'TextArea','',0,0,'T',1,'V')
		,(-7,'Due Date','DueDate','DueDate',1,3,0,0,'DateField','',0,0,'T',1,'V')
		,(-6,'Total Progress','TotalProgress','TotalProgress',1,4,0,0,'TextBox','',0,0,'T',0,'V')
		,(-5,'Assigned to','numAssignedTo','numAssignedTo',0,5,0,0,'SelectBox','',0,0,'T',1,'V')
		,(-4,'Priority','Priority','Priority',0,183,1,0,'SelectBox','',0,0,'T',1,'V')
		,(-3,'Activity','Activity','Activity',0,182,1,0,'SelectBox','',0,0,'T',1,'V')
		,(-2,'Organization (Rating)','OrgName','OrgName',0,8,0,0,'TextBox','',0,0,'T',1,'V')
UPDATE
		TC
	SET
		TC.intColumnWidth = ISNULL(DFCD.intColumnWidth,0)
	FROM
		#tempForm TC
	INNER JOIN
		DycFormConfigurationDetails DFCD
	ON
		TC.numFieldId = DFCD.numFieldId
	WHERE
		DFCD.numDomainId = @numDomainID
		AND DFCD.numUserCntID = @numUserCntID
		AND DFCD.numFormId = 43
		AND DFCD.tintPageType = 1 


	SELECT * FROM #tempForm order by tintOrder asc  
END
GO
