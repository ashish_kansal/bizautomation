/******************************************************************
Project: Release 12.9 Date: 09.NOV.2019
Comments: ALTER SCRIPTS
*******************************************************************/


/******************************************** SANDEEP *********************************************/

DECLARE @numFieldID NUMERIC(18,0)
INSERT INTO DycFieldMaster
(
	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcListItemType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitAllowFiltering
)
VALUES
(
	3,'BizDoc Paid Date (fully Paid)','dtFullPaymentDate','dtFullPaymentDate','FullPaymentDate','OpportunityBizDocs','V','R','DateField','',0,1,0,0,0,1,0,0,1,0,1
)

SET @numFieldID = SCOPE_IDENTITY()

INSERT INTO ReportFieldGroupMappingMaster
(
	numReportFieldGroupID,numFieldID,vcFieldName,vcAssociatedControlType,vcFieldDataType,bitAllowSorting,bitAllowGrouping,bitAllowAggregate,bitAllowFiltering
)
VALUES
(
	5,@numFieldID,'BizDoc Paid Date (fully Paid)','DateField','D',1,1,0,1
)

-----------------------------------

ALTER TABLE ECommerceDtl ADD tintWarehouseAvailability TINYINT
ALTER TABLE ECommerceDtl ADD bitDisplayQtyAvailable BIT


ALTER TABLE AccountTypeDetail ADD tintSortOrder INT
ALTER TABLE AccountTypeDetail ADD bitActive BIT


UPDATE AccountTypeDetail SET bitActive = 1

------------------------------------------------------------------------------------------------------

INSERT INTO PageNavigationDTL 
(numPageNavID,numModuleID,numParentID,vcPageNavName,vcNavURL,bitVisible,numTabID)
VALUES
(
266,35,93,'Income & Expense Statement 2.0','../Accounting/frmIncomeExpenseNew.aspx',1,45
)

-------------------------------

INSERT INTO PageNavigationDTL 
(numPageNavID,numModuleID,numParentID,vcPageNavName,vcNavURL,bitVisible,numTabID)
VALUES
(
267,35,93,'Profit & Loss A/c 2.0','../Accounting/frmProfitLossReportNew.aspx',1,45
)

------------------------------------------------------------------------------------------------------

INSERT INTO PageNavigationDTL 
(numPageNavID,numModuleID,numParentID,vcPageNavName,vcNavURL,bitVisible,numTabID)
VALUES
(
268,35,93,'Balance Sheet 2.0','../Accounting/frmBalanceSheetReportNew.aspx',1,45
)

------------------------------------------------------------------------------------------------------

INSERT INTO PageNavigationDTL 
(numPageNavID,numModuleID,numParentID,vcPageNavName,vcNavURL,bitVisible,numTabID)
VALUES
(
269,35,93,'Trial Balance 2.0','../Accounting/frmTrialBalanceReportNew.aspx',1,45
)
