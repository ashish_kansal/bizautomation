/******************************************************************
Project: Release 11.6 Date: 08.APR.2019
Comments: ALTER SCRIPTS
*******************************************************************/

/******************************************** PRASANT *********************************************/

DELETE FROM DycFormField_Mapping WHERE vcFieldName='Percentage Complete'

UPDATE DynamicFormFieldMaster SET vcFormFieldName='Position' WHERE vcFormFieldName='Contact Position'
UPDATE DycFormField_Mapping SET vcFieldName='Position' WHERE vcFieldName='Contact Position'
UPDATE DycFieldMaster SET vcFieldName='Position' WHERE vcFieldName='Contact Position'
UPDATE DynamicFormField_Validation SET vcNewFormFieldName='Position' WHERE vcNewFormFieldName='Contact Position'

UPDATE DynamicFormFieldMaster SET vcFormFieldName='Ship-State' WHERE vcFormFieldName='Ship To State'
UPDATE DycFormField_Mapping SET vcFieldName='Ship-State' WHERE vcFieldName='Ship To State'
UPDATE DycFieldMaster SET vcFieldName='Ship-State' WHERE vcFieldName='Ship To State'
UPDATE DynamicFormField_Validation SET vcNewFormFieldName='Ship-State' WHERE vcNewFormFieldName='Ship To State'

UPDATE DynamicFormFieldMaster SET vcFormFieldName='Ship-Country' WHERE vcFormFieldName='Ship To Country'
UPDATE DycFormField_Mapping SET vcFieldName='Ship-Country' WHERE vcFieldName='Ship To Country'
UPDATE DycFieldMaster SET vcFieldName='Ship-Country' WHERE vcFieldName='Ship To Country'
UPDATE DynamicFormField_Validation SET vcNewFormFieldName='Ship-Country' WHERE vcNewFormFieldName='Ship To Country'

UPDATE DynamicFormFieldMaster SET vcFormFieldName='Org Rec Owner' WHERE vcFormFieldName='Record Owner'
UPDATE DycFormField_Mapping SET vcFieldName='Org Rec Owner' WHERE vcFieldName='Record Owner'
UPDATE DycFieldMaster SET vcFieldName='Org Rec Owner' WHERE vcFieldName='Record Owner'
UPDATE DynamicFormField_Validation SET vcNewFormFieldName='Org Rec Owner' WHERE vcNewFormFieldName='Record Owner'

UPDATE DynamicFormFieldMaster SET vcFormFieldName='Org Status' WHERE vcFormFieldName='Organization Status'
UPDATE DycFormField_Mapping SET vcFieldName='Org Status' WHERE vcFieldName='Organization Status'
UPDATE DycFieldMaster SET vcFieldName='Org Status' WHERE vcFieldName='Organization Status'
UPDATE DynamicFormField_Validation SET vcNewFormFieldName='Org Status' WHERE vcNewFormFieldName='Organization Status'

UPDATE DynamicFormFieldMaster SET vcFormFieldName='Org Profile' WHERE vcFormFieldName='Organization Profile'
UPDATE DycFormField_Mapping SET vcFieldName='Org Profile' WHERE vcFieldName='Organization Profile'
UPDATE DycFieldMaster SET vcFieldName='Org Profile' WHERE vcFieldName='Organization Profile'
UPDATE DynamicFormField_Validation SET vcNewFormFieldName='Org Profile' WHERE vcNewFormFieldName='Organization Profile'

UPDATE DynamicFormFieldMaster SET vcFormFieldName='Org Dif Val' WHERE vcFormFieldName='Company Differentiation Value'
UPDATE DycFormField_Mapping SET vcFieldName='Org Dif Val' WHERE vcFieldName='Company Differentiation Value'
UPDATE DycFieldMaster SET vcFieldName='Org Dif Val' WHERE vcFieldName='Company Differentiation Value'
UPDATE DynamicFormField_Validation SET vcNewFormFieldName='Org Dif Val' WHERE vcNewFormFieldName='Company Differentiation Value'

UPDATE DynamicFormFieldMaster SET vcFormFieldName='Org Dif' WHERE vcFormFieldName='Company Differentiation'
UPDATE DycFormField_Mapping SET vcFieldName='Org Dif' WHERE vcFieldName='Company Differentiation'
UPDATE DycFieldMaster SET vcFieldName='Org Dif' WHERE vcFieldName='Company Differentiation'
UPDATE DynamicFormField_Validation SET vcNewFormFieldName='Org Dif' WHERE vcNewFormFieldName='Company Differentiation'

UPDATE DynamicFormFieldMaster SET vcFormFieldName='Bill-State' WHERE vcFormFieldName='Bill To State'
UPDATE DycFormField_Mapping SET vcFieldName='Bill-State' WHERE vcFieldName='Bill To State'
UPDATE DycFieldMaster SET vcFieldName='Bill-State' WHERE vcFieldName='Bill To State'
UPDATE DynamicFormField_Validation SET vcNewFormFieldName='Bill-State' WHERE vcNewFormFieldName='Bill To State'

UPDATE DynamicFormFieldMaster SET vcFormFieldName='Bill-Country' WHERE vcFormFieldName='Bill To Country'
UPDATE DycFormField_Mapping SET vcFieldName='Bill-Country' WHERE vcFieldName='Bill To Country'
UPDATE DycFieldMaster SET vcFieldName='Bill-Country' WHERE vcFieldName='Bill To Country'
UPDATE DynamicFormField_Validation SET vcNewFormFieldName='Bill-Country' WHERE vcNewFormFieldName='Bill To Country'

UPDATE DynamicFormFieldMaster SET vcFormFieldName='Relationship' WHERE vcFormFieldName='Organization Relationship'
UPDATE DycFormField_Mapping SET vcFieldName='Relationship' WHERE vcFieldName='Organization Relationship'
UPDATE DycFieldMaster SET vcFieldName='Relationship' WHERE vcFieldName='Organization Relationship'
UPDATE DynamicFormField_Validation SET vcNewFormFieldName='Relationship' WHERE vcNewFormFieldName='Organization Relationship'

UPDATE DynamicFormFieldMaster SET vcFormFieldName='Org Rating' WHERE vcFormFieldName='Organization Rating'
UPDATE DycFormField_Mapping SET vcFieldName='Org Rating' WHERE vcFieldName='Organization Rating'
UPDATE DycFieldMaster SET vcFieldName='Org Rating' WHERE vcFieldName='Organization Rating'
UPDATE DynamicFormField_Validation SET vcNewFormFieldName='Org Rating' WHERE vcNewFormFieldName='Organization Rating'

UPDATE DynamicFormFieldMaster SET vcFormFieldName='Org Phone' WHERE vcFormFieldName='Organization Phone'
UPDATE DycFormField_Mapping SET vcFieldName='Org Phone' WHERE vcFieldName='Organization Phone'
UPDATE DycFieldMaster SET vcFieldName='Org Phone' WHERE vcFieldName='Organization Phone'
UPDATE DynamicFormField_Validation SET vcNewFormFieldName='Org Phone' WHERE vcNewFormFieldName='Organization Phone'

UPDATE DynamicFormFieldMaster SET vcFormFieldName='Org Created' WHERE vcFormFieldName='Organization Created Date'
UPDATE DycFormField_Mapping SET vcFieldName='Org Created' WHERE vcFieldName='Organization Created Date'
UPDATE DycFieldMaster SET vcFieldName='Org Created' WHERE vcFieldName='Organization Created Date'
UPDATE DynamicFormField_Validation SET vcNewFormFieldName='Org Created' WHERE vcNewFormFieldName='Organization Created Date'

UPDATE DynamicFormFieldMaster SET vcFormFieldName='Department' WHERE vcFormFieldName='Department Name'
UPDATE DycFormField_Mapping SET vcFieldName='Department' WHERE vcFieldName='Department Name'
UPDATE DycFieldMaster SET vcFieldName='Department' WHERE vcFieldName='Department Name'
UPDATE DynamicFormField_Validation SET vcNewFormFieldName='Department' WHERE vcNewFormFieldName='Department Name'

UPDATE DynamicFormFieldMaster SET vcFormFieldName='Type' WHERE vcFormFieldName='Contact Type'
UPDATE DycFormField_Mapping SET vcFieldName='Type' WHERE vcFieldName='Contact Type'
UPDATE DycFieldMaster SET vcFieldName='Type' WHERE vcFieldName='Contact Type'
UPDATE DynamicFormField_Validation SET vcNewFormFieldName='Type' WHERE vcNewFormFieldName='Contact Type'

UPDATE DynamicFormFieldMaster SET vcFormFieldName='Title' WHERE vcFormFieldName='Contact Title'
UPDATE DycFormField_Mapping SET vcFieldName='Title' WHERE vcFieldName='Contact Title'
UPDATE DycFieldMaster SET vcFieldName='Title' WHERE vcFieldName='Contact Title'
UPDATE DynamicFormField_Validation SET vcNewFormFieldName='Title' WHERE vcNewFormFieldName='Contact Title'

UPDATE DynamicFormFieldMaster SET vcFormFieldName='Team' WHERE vcFormFieldName='Contact Team'
UPDATE DycFormField_Mapping SET vcFieldName='Team' WHERE vcFieldName='Contact Team'
UPDATE DycFieldMaster SET vcFieldName='Team' WHERE vcFieldName='Contact Team'
UPDATE DynamicFormField_Validation SET vcNewFormFieldName='Team' WHERE vcNewFormFieldName='Contact Team'

UPDATE DynamicFormFieldMaster SET vcFormFieldName='Street' WHERE vcFormFieldName='Contact Street'
UPDATE DycFormField_Mapping SET vcFieldName='Street' WHERE vcFieldName='Contact Street'
UPDATE DycFieldMaster SET vcFieldName='Street' WHERE vcFieldName='Contact Street'
UPDATE DynamicFormField_Validation SET vcNewFormFieldName='Street' WHERE vcNewFormFieldName='Contact Street'

UPDATE DynamicFormFieldMaster SET vcFormFieldName='Status' WHERE vcFormFieldName='Contact Status'
UPDATE DycFormField_Mapping SET vcFieldName='Status' WHERE vcFieldName='Contact Status'
UPDATE DycFieldMaster SET vcFieldName='Status' WHERE vcFieldName='Contact Status'
UPDATE DynamicFormField_Validation SET vcNewFormFieldName='Status' WHERE vcNewFormFieldName='Contact Status'

UPDATE DynamicFormFieldMaster SET vcFormFieldName='State' WHERE vcFormFieldName='Contact State'
UPDATE DycFormField_Mapping SET vcFieldName='State' WHERE vcFieldName='Contact State'
UPDATE DycFieldMaster SET vcFieldName='State' WHERE vcFieldName='Contact State'
UPDATE DynamicFormField_Validation SET vcNewFormFieldName='State' WHERE vcNewFormFieldName='Contact State'

UPDATE DynamicFormFieldMaster SET vcFormFieldName='Zip Code' WHERE vcFormFieldName='Contact Postal Code'
UPDATE DycFormField_Mapping SET vcFieldName='Zip Code' WHERE vcFieldName='Contact Postal Code'
UPDATE DycFieldMaster SET vcFieldName='Zip Code' WHERE vcFieldName='Contact Postal Code'
UPDATE DynamicFormField_Validation SET vcNewFormFieldName='Zip Code' WHERE vcNewFormFieldName='Contact Postal Code'

UPDATE DynamicFormFieldMaster SET vcFormFieldName='Ext' WHERE vcFormFieldName='Contact Phone Ext'
UPDATE DycFormField_Mapping SET vcFieldName='Ext' WHERE vcFieldName='Contact Phone Ext'
UPDATE DycFieldMaster SET vcFieldName='Ext' WHERE vcFieldName='Contact Phone Ext'
UPDATE DynamicFormField_Validation SET vcNewFormFieldName='Ext' WHERE vcNewFormFieldName='Contact Phone Ext'

UPDATE DynamicFormFieldMaster SET vcFormFieldName='Phone' WHERE vcFormFieldName='Contact Phone'
UPDATE DycFormField_Mapping SET vcFieldName='Phone' WHERE vcFieldName='Contact Phone'
UPDATE DycFieldMaster SET vcFieldName='Phone' WHERE vcFieldName='Contact Phone'
UPDATE DynamicFormField_Validation SET vcNewFormFieldName='Phone' WHERE vcNewFormFieldName='Contact Phone'

UPDATE DynamicFormFieldMaster SET vcFormFieldName='Home Phone' WHERE vcFormFieldName='Contact Home Phone'
UPDATE DycFormField_Mapping SET vcFieldName='Home Phone' WHERE vcFieldName='Contact Home Phone'
UPDATE DycFieldMaster SET vcFieldName='Home Phone' WHERE vcFieldName='Contact Home Phone'
UPDATE DynamicFormField_Validation SET vcNewFormFieldName='Home Phone' WHERE vcNewFormFieldName='Contact Home Phone'

UPDATE DynamicFormFieldMaster SET vcFormFieldName='Fax' WHERE vcFormFieldName='Contact Fax'
UPDATE DycFormField_Mapping SET vcFieldName='Fax' WHERE vcFieldName='Contact Fax'
UPDATE DycFieldMaster SET vcFieldName='Fax' WHERE vcFieldName='Contact Fax'
UPDATE DynamicFormField_Validation SET vcNewFormFieldName='Fax' WHERE vcNewFormFieldName='Contact Fax'

UPDATE DynamicFormFieldMaster SET vcFormFieldName='Email' WHERE vcFormFieldName='Contact Email'
UPDATE DycFormField_Mapping SET vcFieldName='Email' WHERE vcFieldName='Contact Email'
UPDATE DycFieldMaster SET vcFieldName='Email' WHERE vcFieldName='Contact Email'
UPDATE DynamicFormField_Validation SET vcNewFormFieldName='Email' WHERE vcNewFormFieldName='Contact Email'

UPDATE DynamicFormFieldMaster SET vcFormFieldName='Contact Created' WHERE vcFormFieldName='Contact Created Date'
UPDATE DycFormField_Mapping SET vcFieldName='Contact Created' WHERE vcFieldName='Contact Created Date'
UPDATE DycFieldMaster SET vcFieldName='Contact Created' WHERE vcFieldName='Contact Created Date'
UPDATE DynamicFormField_Validation SET vcNewFormFieldName='Contact Created' WHERE vcNewFormFieldName='Contact Created Date'

UPDATE DynamicFormFieldMaster SET vcFormFieldName='Country' WHERE vcFormFieldName='Contact Country'
UPDATE DycFormField_Mapping SET vcFieldName='Country' WHERE vcFieldName='Contact Country'
UPDATE DycFieldMaster SET vcFieldName='Country' WHERE vcFieldName='Contact Country'
UPDATE DynamicFormField_Validation SET vcNewFormFieldName='Country' WHERE vcNewFormFieldName='Contact Country'

UPDATE DynamicFormFieldMaster SET vcFormFieldName='City' WHERE vcFormFieldName='Contact City'
UPDATE DycFormField_Mapping SET vcFieldName='City' WHERE vcFieldName='Contact City'
UPDATE DycFieldMaster SET vcFieldName='City' WHERE vcFieldName='Contact City'
UPDATE DynamicFormField_Validation SET vcNewFormFieldName='City' WHERE vcNewFormFieldName='Contact City'

UPDATE DynamicFormFieldMaster SET vcFormFieldName='Cell' WHERE vcFormFieldName='Contact Cell Phone'
UPDATE DycFormField_Mapping SET vcFieldName='Cell' WHERE vcFieldName='Contact Cell Phone'
UPDATE DycFieldMaster SET vcFieldName='Cell' WHERE vcFieldName='Contact Cell Phone'
UPDATE DynamicFormField_Validation SET vcNewFormFieldName='Cell' WHERE vcNewFormFieldName='Contact Cell Phone'

UPDATE DynamicFormFieldMaster SET vcFormFieldName='Category' WHERE vcFormFieldName='Contact Category'
UPDATE DycFormField_Mapping SET vcFieldName='Category' WHERE vcFieldName='Contact Category'
UPDATE DycFieldMaster SET vcFieldName='Category' WHERE vcFieldName='Contact Category'
UPDATE DynamicFormField_Validation SET vcNewFormFieldName='Category' WHERE vcNewFormFieldName='Contact Category'

UPDATE DynamicFormFieldMaster SET vcFormFieldName='Amt Paid' WHERE vcFormFieldName='Total Amount Paid'
UPDATE DycFormField_Mapping SET vcFieldName='Amt Paid' WHERE vcFieldName='Total Amount Paid'
UPDATE DycFieldMaster SET vcFieldName='Amt Paid' WHERE vcFieldName='Total Amount Paid'
UPDATE DynamicFormField_Validation SET vcNewFormFieldName='Amt Paid' WHERE vcNewFormFieldName='Total Amount Paid'

UPDATE DynamicFormFieldMaster SET vcFormFieldName='Ship-State' WHERE vcFormFieldName='Ship-to State'
UPDATE DycFormField_Mapping SET vcFieldName='Ship-State' WHERE vcFieldName='Ship-to State'
UPDATE DycFieldMaster SET vcFieldName='Ship-State' WHERE vcFieldName='Ship-to State'
UPDATE DynamicFormField_Validation SET vcNewFormFieldName='Ship-State' WHERE vcNewFormFieldName='Ship-to State'

UPDATE DynamicFormFieldMaster SET vcFormFieldName='Ship-Service' WHERE vcFormFieldName='Shipping Service'
UPDATE DycFormField_Mapping SET vcFieldName='Ship-Service' WHERE vcFieldName='Shipping Service'
UPDATE DycFieldMaster SET vcFieldName='Ship-Service' WHERE vcFieldName='Shipping Service'
UPDATE DynamicFormField_Validation SET vcNewFormFieldName='Ship-Service' WHERE vcNewFormFieldName='Shipping Service'

UPDATE DynamicFormFieldMaster SET vcFormFieldName='Name' WHERE vcFormFieldName='Sales Order Name'
UPDATE DycFormField_Mapping SET vcFieldName='Name' WHERE vcFieldName='Sales Order Name'
UPDATE DycFieldMaster SET vcFieldName='Name' WHERE vcFieldName='Sales Order Name'
UPDATE DynamicFormField_Validation SET vcNewFormFieldName='Name' WHERE vcNewFormFieldName='Sales Order Name'

UPDATE DynamicFormFieldMaster SET vcFormFieldName='Recur-Type' WHERE vcFormFieldName='Recurrence Type'
UPDATE DycFormField_Mapping SET vcFieldName='Recur-Type' WHERE vcFieldName='Recurrence Type'
UPDATE DycFieldMaster SET vcFieldName='Recur-Type' WHERE vcFieldName='Recurrence Type'
UPDATE DynamicFormField_Validation SET vcNewFormFieldName='Recur-Type' WHERE vcNewFormFieldName='Recurrence Type'

UPDATE DynamicFormFieldMaster SET vcFormFieldName='Organization' WHERE vcFormFieldName='Organization Name'
UPDATE DycFormField_Mapping SET vcFieldName='Organization' WHERE vcFieldName='Organization Name'
UPDATE DycFieldMaster SET vcFieldName='Organization' WHERE vcFieldName='Organization Name'
UPDATE DynamicFormField_Validation SET vcNewFormFieldName='Organization' WHERE vcNewFormFieldName='Organization Name'

UPDATE DynamicFormFieldMaster SET vcFormFieldName='Status' WHERE vcFormFieldName='Order Status'
UPDATE DycFormField_Mapping SET vcFieldName='Status' WHERE vcFieldName='Order Status'
UPDATE DycFieldMaster SET vcFieldName='Status' WHERE vcFieldName='Order Status'
UPDATE DynamicFormField_Validation SET vcNewFormFieldName='Status' WHERE vcNewFormFieldName='Order Status'

UPDATE DynamicFormFieldMaster SET vcFormFieldName='Next-Order' WHERE vcFormFieldName='Next Billing Date'
UPDATE DycFormField_Mapping SET vcFieldName='Next-Order' WHERE vcFieldName='Next Billing Date'
UPDATE DycFieldMaster SET vcFieldName='Next-Order' WHERE vcFieldName='Next Billing Date'
UPDATE DynamicFormField_Validation SET vcNewFormFieldName='Next-Order' WHERE vcNewFormFieldName='Next Billing Date'

UPDATE DynamicFormFieldMaster SET vcFormFieldName='Marketplace' WHERE vcFormFieldName='Marketplace Order ID'
UPDATE DycFormField_Mapping SET vcFieldName='Marketplace' WHERE vcFieldName='Marketplace Order ID'
UPDATE DycFieldMaster SET vcFieldName='Marketplace' WHERE vcFieldName='Marketplace Order ID'
UPDATE DynamicFormField_Validation SET vcNewFormFieldName='Marketplace' WHERE vcNewFormFieldName='Marketplace Order ID'

UPDATE DynamicFormFieldMaster SET vcFormFieldName='Inv-Grand-Tot' WHERE vcFormFieldName='Invoice Grand-total'
UPDATE DycFormField_Mapping SET vcFieldName='Inv-Grand-Tot' WHERE vcFieldName='Invoice Grand-total'
UPDATE DycFieldMaster SET vcFieldName='Inv-Grand-Tot' WHERE vcFieldName='Invoice Grand-total'
UPDATE DynamicFormField_Validation SET vcNewFormFieldName='Inv-Grand-Tot' WHERE vcNewFormFieldName='Invoice Grand-total'

UPDATE DynamicFormFieldMaster SET vcFormFieldName='Estimated Close' WHERE vcFormFieldName='Estimated close date'
UPDATE DycFormField_Mapping SET vcFieldName='Estimated Close' WHERE vcFieldName='Estimated close date'
UPDATE DycFieldMaster SET vcFieldName='Estimated Close' WHERE vcFieldName='Estimated close date'
UPDATE DynamicFormField_Validation SET vcNewFormFieldName='Estimated Close' WHERE vcNewFormFieldName='Estimated close date'

UPDATE DynamicFormFieldMaster SET vcFormFieldName='Cust PO#' WHERE vcFormFieldName='Customer PO#'
UPDATE DycFormField_Mapping SET vcFieldName='Cust PO#' WHERE vcFieldName='Customer PO#'
UPDATE DycFieldMaster SET vcFieldName='Cust PO#' WHERE vcFieldName='Customer PO#'
UPDATE DynamicFormField_Validation SET vcNewFormFieldName='Cust PO#' WHERE vcNewFormFieldName='Customer PO#'

UPDATE DynamicFormFieldMaster SET vcFormFieldName='Last Name' WHERE vcFormFieldName='Contact Last Name'
UPDATE DycFormField_Mapping SET vcFieldName='Last Name' WHERE vcFieldName='Contact Last Name'
UPDATE DycFieldMaster SET vcFieldName='Last Name' WHERE vcFieldName='Contact Last Name'
UPDATE DynamicFormField_Validation SET vcNewFormFieldName='Last Name' WHERE vcNewFormFieldName='Contact Last Name'

UPDATE DynamicFormFieldMaster SET vcFormFieldName='First Name' WHERE vcFormFieldName='Contact First Name'
UPDATE DycFormField_Mapping SET vcFieldName='First Name' WHERE vcFieldName='Contact First Name'
UPDATE DycFieldMaster SET vcFieldName='First Name' WHERE vcFieldName='Contact First Name'
UPDATE DynamicFormField_Validation SET vcNewFormFieldName='First Name' WHERE vcNewFormFieldName='Contact First Name'

UPDATE DynamicFormFieldMaster SET vcFormFieldName='Last Order Date' WHERE vcFormFieldName='Last Sales Order Date'
UPDATE DycFormField_Mapping SET vcFieldName='Last Order Date' WHERE vcFieldName='Last Sales Order Date'
UPDATE DycFieldMaster SET vcFieldName='Last Order Date' WHERE vcFieldName='Last Sales Order Date'
UPDATE DynamicFormField_Validation SET vcNewFormFieldName='Last Order Date' WHERE vcNewFormFieldName='Last Sales Order Date'

UPDATE DynamicFormFieldMaster SET vcFormFieldName='Org Credit' WHERE vcFormFieldName='Organization Credit Limit'
UPDATE DycFormField_Mapping SET vcFieldName='Org Credit' WHERE vcFieldName='Organization Credit Limit'
UPDATE DycFieldMaster SET vcFieldName='Org Credit' WHERE vcFieldName='Organization Credit Limit'
UPDATE DynamicFormField_Validation SET vcNewFormFieldName='Org Credit' WHERE vcNewFormFieldName='Organization Credit Limit'
---------------------------------------END-------------------------------------
UPDATE DycFieldMaster SET vcAssociatedControlType='TextBox' WHERE vcDbColumnName='vcCompactContactDetails'
UPDATE DycFieldMaster SET bitAllowSorting=0 WHERE vcDbColumnName='vcCompactContactDetails'
UPDATE DynamicFormFieldMaster SET vcAssociatedControlType='TextBox' WHERE vcDbColumnName='vcCompactContactDetails'
UPDATE DynamicFormFieldMaster SET bitAllowSorting=0 WHERE vcDbColumnName='vcCompactContactDetails'
UPDATE DycFormField_Mapping SET vcAssociatedControlType='TextBox' WHERE numFieldId IN(select numFieldId from DycFieldMaster where vcDbColumnName='vcCompactContactDetails')
UPDATE DycFormField_Mapping SET bitAllowSorting=0 WHERE numFieldId IN(select numFieldId from DycFieldMaster where vcDbColumnName='vcCompactContactDetails')
BEGIN TRY
BEGIN TRANSACTION

DECLARE @numFieldID AS NUMERIC(18,0)
DECLARE @numFormFieldID AS NUMERIC(18,0)

INSERT INTO DycFieldMaster
(
	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcListItemType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting
)
VALUES
(
	1,'Contact','vcCompactContactDetails','vcContactName','AdditionalContactsInformation','V','R','DateField','',0,1,0,0,0,1,0,0,1
)
SELECT @numFieldID = SCOPE_IDENTITY()
--Lead Form
INSERT INTO DynamicFormFieldMaster
(numFormID,vcFormFieldName,vcFieldType,vcAssociatedControlType,vcDbColumnName,numListID,bitDeleted,vcFieldDataType,vcOrigDbColumnName,bitAllowEdit,vcLookBackTableName,
 bitDefault,[order],bitInResults,vcPropertyName,tintRow,tintColumn,bitSettingField,bitAddField,bitDetailField,bitAllowSorting)
VALUES
(34,'Contact','R','DateField','vcCompactContactDetails',0,0,'V','vcContactName',0,'AdditionalContactsInformation',0,7,1,'vcContactName',1,7,1,0,0,1)

SELECT @numFormFieldID = SCOPE_IDENTITY()

INSERT INTO DycFormField_Mapping
(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,bitInResults,bitDeleted,
bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numFormFieldID)
VALUES
(2,@numFieldID,34,0,0,'Contact','DateField',NULL,7,1,7,1,0,0,1,0,1,1,1,@numFormFieldID)

--Prospect Form
INSERT INTO DynamicFormFieldMaster
(numFormID,vcFormFieldName,vcFieldType,vcAssociatedControlType,vcDbColumnName,numListID,bitDeleted,vcFieldDataType,vcOrigDbColumnName,bitAllowEdit,vcLookBackTableName,
 bitDefault,[order],bitInResults,vcPropertyName,tintRow,tintColumn,bitSettingField,bitAddField,bitDetailField,bitAllowSorting)
VALUES
(35,'Contact','R','DateField','vcCompactContactDetails',0,0,'V','vcContactName',0,'AdditionalContactsInformation',0,7,1,'vcContactName',1,7,1,0,0,1)

SELECT @numFormFieldID = SCOPE_IDENTITY()

INSERT INTO DycFormField_Mapping
(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,bitInResults,bitDeleted,
bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numFormFieldID)
VALUES
(3,@numFieldID,35,0,0,'Contact','DateField',NULL,7,1,7,1,0,0,1,0,1,1,1,@numFormFieldID)

--Contact Form
INSERT INTO DynamicFormFieldMaster
(numFormID,vcFormFieldName,vcFieldType,vcAssociatedControlType,vcDbColumnName,numListID,bitDeleted,vcFieldDataType,vcOrigDbColumnName,bitAllowEdit,vcLookBackTableName,
 bitDefault,[order],bitInResults,vcPropertyName,tintRow,tintColumn,bitSettingField,bitAddField,bitDetailField,bitAllowSorting)
VALUES
(10,'Contact','R','TextBox','vcCompactContactDetails',0,0,'V','vcContactName',0,'AdditionalContactsInformation',0,7,1,'vcContactName',1,7,1,0,0,1)

SELECT @numFormFieldID = SCOPE_IDENTITY()

INSERT INTO DycFormField_Mapping
(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,bitInResults,bitDeleted,
bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numFormFieldID)
VALUES
(32,@numFieldID,10,0,0,'Contact','TextBox',NULL,7,1,7,1,0,0,1,0,1,1,1,@numFormFieldID)

--Case Form
INSERT INTO DynamicFormFieldMaster
(numFormID,vcFormFieldName,vcFieldType,vcAssociatedControlType,vcDbColumnName,numListID,bitDeleted,vcFieldDataType,vcOrigDbColumnName,bitAllowEdit,vcLookBackTableName,
 bitDefault,[order],bitInResults,vcPropertyName,tintRow,tintColumn,bitSettingField,bitAddField,bitDetailField,bitAllowSorting)
VALUES
(12,'Contact','R','TextBox','vcCompactContactDetails',0,0,'V','vcContactName',0,'AdditionalContactsInformation',0,7,1,'vcContactName',1,7,1,0,0,1)

SELECT @numFormFieldID = SCOPE_IDENTITY()

INSERT INTO DycFormField_Mapping
(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,bitInResults,bitDeleted,
bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numFormFieldID)
VALUES
(7,@numFieldID,12,0,0,'Contact','TextBox',NULL,7,1,7,1,0,0,1,0,1,1,1,@numFormFieldID)


--Account Form
INSERT INTO DynamicFormFieldMaster
(numFormID,vcFormFieldName,vcFieldType,vcAssociatedControlType,vcDbColumnName,numListID,bitDeleted,vcFieldDataType,vcOrigDbColumnName,bitAllowEdit,vcLookBackTableName,
 bitDefault,[order],bitInResults,vcPropertyName,tintRow,tintColumn,bitSettingField,bitAddField,bitDetailField,bitAllowSorting)
VALUES
(36,'Contact','R','DateField','vcCompactContactDetails',0,0,'V','vcContactName',0,'AdditionalContactsInformation',0,7,1,'vcContactName',1,7,1,0,0,1)

SELECT @numFormFieldID = SCOPE_IDENTITY()

INSERT INTO DycFormField_Mapping
(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,bitInResults,bitDeleted,
bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numFormFieldID)
VALUES
(4,@numFieldID,36,0,0,'Contact','DateField',NULL,7,1,7,1,0,0,1,0,1,1,1,@numFormFieldID)


----Sales Opportunity
INSERT INTO DynamicFormFieldMaster
(numFormID,vcFormFieldName,vcFieldType,vcAssociatedControlType,vcDbColumnName,numListID,bitDeleted,vcFieldDataType,vcOrigDbColumnName,bitAllowEdit,vcLookBackTableName,
 bitDefault,[order],bitInResults,vcPropertyName,tintRow,tintColumn,bitSettingField,bitAddField,bitDetailField,bitAllowSorting)
VALUES
(38,'Contact','R','DateField','vcCompactContactDetails',0,0,'V','vcContactName',0,'AdditionalContactsInformation',0,7,1,'vcContactName',1,7,1,0,0,1)

SELECT @numFormFieldID = SCOPE_IDENTITY()

INSERT INTO DycFormField_Mapping
(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,bitInResults,bitDeleted,
bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numFormFieldID)
VALUES
(10,@numFieldID,38,0,0,'Contact','DateField',NULL,7,1,7,1,0,0,1,0,1,1,1,@numFormFieldID)

--Purchase Opportunity
INSERT INTO DynamicFormFieldMaster
(numFormID,vcFormFieldName,vcFieldType,vcAssociatedControlType,vcDbColumnName,numListID,bitDeleted,vcFieldDataType,vcOrigDbColumnName,bitAllowEdit,vcLookBackTableName,
 bitDefault,[order],bitInResults,vcPropertyName,tintRow,tintColumn,bitSettingField,bitAddField,bitDetailField,bitAllowSorting)
VALUES
(40,'Contact','R','DateField','vcCompactContactDetails',0,0,'V','vcContactName',0,'AdditionalContactsInformation',0,7,1,'vcContactName',1,7,1,0,0,1)

SELECT @numFormFieldID = SCOPE_IDENTITY()

INSERT INTO DycFormField_Mapping
(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,bitInResults,bitDeleted,
bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numFormFieldID)
VALUES
(10,@numFieldID,40,0,0,'Contact','DateField',NULL,7,1,7,1,0,0,1,0,1,1,1,@numFormFieldID)

----Sales Order
INSERT INTO DynamicFormFieldMaster
(numFormID,vcFormFieldName,vcFieldType,vcAssociatedControlType,vcDbColumnName,numListID,bitDeleted,vcFieldDataType,vcOrigDbColumnName,bitAllowEdit,vcLookBackTableName,
 bitDefault,[order],bitInResults,vcPropertyName,tintRow,tintColumn,bitSettingField,bitAddField,bitDetailField,bitAllowSorting)
VALUES
(39,'Contact','R','DateField','vcCompactContactDetails',0,0,'V','vcContactName',0,'AdditionalContactsInformation',0,7,1,'vcContactName',1,7,1,0,0,1)

SELECT @numFormFieldID = SCOPE_IDENTITY()

INSERT INTO DycFormField_Mapping
(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,bitInResults,bitDeleted,
bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numFormFieldID)
VALUES
(10,@numFieldID,39,0,0,'Contact','DateField',NULL,7,1,7,1,0,0,1,0,1,1,1,@numFormFieldID)

--Purchase Order
INSERT INTO DynamicFormFieldMaster
(numFormID,vcFormFieldName,vcFieldType,vcAssociatedControlType,vcDbColumnName,numListID,bitDeleted,vcFieldDataType,vcOrigDbColumnName,bitAllowEdit,vcLookBackTableName,
 bitDefault,[order],bitInResults,vcPropertyName,tintRow,tintColumn,bitSettingField,bitAddField,bitDetailField,bitAllowSorting)
VALUES
(41,'Contact','R','DateField','vcCompactContactDetails',0,0,'V','vcContactName',0,'AdditionalContactsInformation',0,7,1,'vcContactName',1,7,1,0,0,1)

SELECT @numFormFieldID = SCOPE_IDENTITY()

INSERT INTO DycFormField_Mapping
(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,bitInResults,bitDeleted,
bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numFormFieldID)
VALUES
(10,@numFieldID,41,0,0,'Contact','DateField',NULL,7,1,7,1,0,0,1,0,1,1,1,@numFormFieldID)

COMMIT
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		ERROR_MESSAGE(),
		ERROR_NUMBER(),
		ERROR_SEVERITY(),
		ERROR_STATE(),
		ERROR_LINE(),
		ISNULL(ERROR_PROCEDURE(), '-');
END CATCH

SET IDENTITY_INSERT modulemaster ON;
INSERT INTO modulemaster(numModuleId,vcModuleName,tintGroupType)VALUES(44,'Add Action Item',1)
SET IDENTITY_INSERT modulemaster OFF;


INSERT INTO PageMaster(
numPageID,numModuleID, vcFileName, vcPageDesc, bitIsViewApplicable, bitIsAddApplicable, bitIsUpdateApplicable, bitIsDeleteApplicable, bitIsExportApplicable, vcToolTip, bitDeleted
)VALUES(
143,44,'','Accounts',1,0,0,0,0,'',0
)

INSERT INTO PageMaster(
numPageID,numModuleID, vcFileName, vcPageDesc, bitIsViewApplicable, bitIsAddApplicable, bitIsUpdateApplicable, bitIsDeleteApplicable, bitIsExportApplicable, vcToolTip, bitDeleted
)VALUES(
144,44,'','Contacts',1,0,0,0,0,'',0
)

INSERT INTO PageMaster(
numPageID,numModuleID, vcFileName, vcPageDesc, bitIsViewApplicable, bitIsAddApplicable, bitIsUpdateApplicable, bitIsDeleteApplicable, bitIsExportApplicable, vcToolTip, bitDeleted
)VALUES(
145,44,'','Leads',1,0,0,0,0,'',0
)

INSERT INTO PageMaster(
numPageID,numModuleID, vcFileName, vcPageDesc, bitIsViewApplicable, bitIsAddApplicable, bitIsUpdateApplicable, bitIsDeleteApplicable, bitIsExportApplicable, vcToolTip, bitDeleted
)VALUES(
146,44,'','Opportunities/Orders',1,0,0,0,0,'',0
)

INSERT INTO PageMaster(
numPageID,numModuleID, vcFileName, vcPageDesc, bitIsViewApplicable, bitIsAddApplicable, bitIsUpdateApplicable, bitIsDeleteApplicable, bitIsExportApplicable, vcToolTip, bitDeleted
)VALUES(
147,44,'','Prospects',1,0,0,0,0,'',0
)

INSERT INTO PageMaster(
numPageID,numModuleID, vcFileName, vcPageDesc, bitIsViewApplicable, bitIsAddApplicable, bitIsUpdateApplicable, bitIsDeleteApplicable, bitIsExportApplicable, vcToolTip, bitDeleted
)VALUES(
148,44,'','Relationships',1,0,0,0,0,'',0
)



ALTER TABLE Sales_process_List_Master ADD numProjectId NUMERIC(18,0)
ALTER TABLE ProjectsMaster ADD numBusinessProcessId NUMERIC(18,0)
ALTER TABLE GenericDocuments ADD numFollowUpStatusId NUMERIC DEFAULT 0
ALTER TABLE GenericDocuments ADD bitUpdateFollowupStatus BIT DEFAULT 0