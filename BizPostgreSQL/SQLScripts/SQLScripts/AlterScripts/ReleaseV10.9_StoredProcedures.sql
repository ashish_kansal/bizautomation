/******************************************************************
Project: Release 10.9 Date: 21.JAN.2019
Comments: STORE PROCEDURES
*******************************************************************/

/****** Object:  StoredProcedure [dbo].[USP_OPPCheckCanBeShipped]    Script Date: 07/26/2008 16:20:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON            
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_CheckOrderedAndInvoicedOrBilledQty')
DROP PROCEDURE USP_CheckOrderedAndInvoicedOrBilledQty
GO
CREATE PROCEDURE [dbo].[USP_CheckOrderedAndInvoicedOrBilledQty]              
@numOppID AS NUMERIC(18,0)
AS             
BEGIN
	DECLARE @TMEP TABLE
	(
		ID INT,
		vcDescription VARCHAR(100),
		bitTrue BIT
	)

	INSERT INTO 
		@TMEP
	VALUES
		(1,'Fulfillment bizdoc is added but not yet fulfilled',0),
		(2,'Invoice/Bill and Ordered Qty is not same',0),
		(3,'Ordered & Fulfilled Qty is not same',0),
		(4,'Invoice is not generated against diferred income bizDocs.',0),
		(5,'Qty is not available to add in deferred bizdoc.',0),
		(6,'Ordered & Purchase Fulfilled Qty is not same',0)

	DECLARE @tintOppType AS TINYINT
	DECLARE @tintOppStatus AS TINYINT

	SELECT @tintOppType=tintOppType,@tintOppStatus=tintOppStatus FROM OpportunityMaster WHERe numOppID=@numOppID


	-- CHECK IF FULFILLMENT BIZDOC IS ADDED TO SALES ORDER BUT IT IS NOT FULFILLED FROM SALES FULFILLMENT SCREEN
	IF @tintOppType = 1 AND @tintOppStatus = 1  --SALES ORDER
	BEGIN
		IF (SELECT COUNT(*) FROM OpportunityBizDocs WHERE numOppId=@numOppID AND numBizDocId=296 AND ISNULL(bitFulFilled,0) = 0) > 0
		BEGIN
			UPDATE @TMEP SET bitTrue = 1 WHERE ID = 1
		END

		-- CHECK IF FULFILLMENT BIZDOC QTY OF ITEMS IS NOT SAME AS ORDERED QTY
		IF (SELECT 
				COUNT(*) 
			FROM 
			(
				SELECT
					OI.numoppitemtCode,
					ISNULL(OI.numUnitHour,0) AS OrderedQty,
					ISNULL(TempFulFilled.FulFilledQty,0) AS FulFilledQty
				FROM
					OpportunityItems OI
				INNER JOIN
					Item I
				ON
					OI.numItemCode = I.numItemCode
				OUTER APPLY
				(
					SELECT
						SUM(OpportunityBizDocItems.numUnitHour) AS FulFilledQty
					FROM
						OpportunityBizDocs
					INNER JOIN
						OpportunityBizDocItems 
					ON
						OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
					WHERE
						OpportunityBizDocs.numOppId = @numOppID
						AND ISNULL(OpportunityBizDocs.numBizDocId,0) = 296
						AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
				) AS TempFulFilled
				WHERE
					OI.numOppID = @numOppID
					AND ISNULL(OI.numUnitHour,0) > 0
					AND UPPER(I.charItemType) = 'P'
					AND ISNULL(OI.bitDropShip,0) = 0
			) X
			WHERE
				X.OrderedQty <> X.FulFilledQty) > 0
		BEGIN
			UPDATE @TMEP SET bitTrue = 1 WHERE ID = 3
		END

		-- CHECK WHETHER INVOICES ARE GENERATED AGAINST DIFERRED INCOME BIZDOC
		IF (SELECT 
				COUNT(*)
			FROM  
			(
				SELECT 
					OBDI.numOppItemID,
					(ISNULL(SUM(OBDI.numUnitHour),0) - ISNULL(SUM(TEMPInvoiceAgainstDeferred.numUnitHour),0)) AS intQtyRemaining
				FROM
					OpportunityBizDocs OBD
				INNER JOIN
					OpportunityBizDocItems OBDI
				ON
					OBD.numOppBizDocsId = OBDI.numOppBizDocID
				CROSS APPLY
				(
					SELECT 
						SUM(OpportunityBizDocItems.numUnitHour) AS numUnitHour
					FROM
						OpportunityBizDocs
					INNER JOIN
						OpportunityBizDocItems
					ON
						OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
					WHERE
						numOppId=@numOppID
						AND ISNULL(numDeferredBizDocID,0) > 0
						AND OpportunityBizDocItems.numOppItemID = OBDI.numOppItemID

				) AS TEMPInvoiceAgainstDeferred
				WHERE
					numOppId=@numOppID 
					AND numBizDocId=304
				GROUP BY
					OBDI.numOppItemID
			) AS TEMP
			WHERE
				intQtyRemaining > 0) > 0
		BEGIN
			UPDATE @TMEP SET bitTrue = 1 WHERE ID = 4
		END

		-- CHECK WHETHER ITEM QTY LEFT TO ADD TO DIFERRED INCOME BIZDOC
		IF (SELECT 
				COUNT(*) 
			FROM 
			(
				SELECT
					OI.numoppitemtCode,
					ISNULL(OI.numUnitHour,0) AS OrderedQty,
					ISNULL(TempQtyLeftForDifferedIncome.intQty,0) AS intQty
				FROM
					OpportunityItems OI
				INNER JOIN
					Item I
				ON
					OI.numItemCode = I.numItemCode
				OUTER APPLY
				(
					SELECT
						SUM(OpportunityBizDocItems.numUnitHour) AS intQty
					FROM
						OpportunityBizDocs
					INNER JOIN
						OpportunityBizDocItems 
					ON
						OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
					WHERE
						OpportunityBizDocs.numOppId = @numOppID
						AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
						AND (ISNULL(OpportunityBizDocs.bitAuthoritativeBizDocs,0) = 1 OR OpportunityBizDocs.numBizDocID = 304)
				) AS TempQtyLeftForDifferedIncome
				WHERE
					OI.numOppID = @numOppID
			) X
			WHERE
				X.OrderedQty <> X.intQty) = 0
		BEGIN
			UPDATE @TMEP SET bitTrue = 1 WHERE ID = 5
		END
	END
	ELSE IF @tintOppType = 2 AND @tintOppStatus = 1  --SALES ORDER
	BEGIN
		IF (SELECT 
				COUNT(*) 
			FROM 
			(
				SELECT
					OI.numoppitemtCode,
					ISNULL(OI.numUnitHour,0) AS OrderedQty,
					ISNULL(OI.numUnitHourReceived,0) AS FulFilledQty
				FROM
					OpportunityItems OI
				WHERE
					OI.numOppID = @numOppID
					AND ISNULL(OI.numUnitHour,0) > 0
					AND ISNULL(OI.bitDropShip,0) = 0
			) X
			WHERE
				X.OrderedQty <> X.FulFilledQty) > 0
		BEGIN
			UPDATE @TMEP SET bitTrue = 1 WHERE ID = 6
		END
	END

	-- IF INVOICED/BILLED QTY OF ITEMS IS NOT SAME AS ORDERED QTY THEN ORDER CAN NOT BE SHIPPED/RECEIVED
	IF (SELECT 
			COUNT(*) 
		FROM 
		(
			SELECT
				OI.numoppitemtCode,
				ISNULL(OI.numUnitHour,0) AS OrderedQty,
				ISNULL(TempInvoice.InvoicedQty,0) AS InvoicedQty
			FROM
				OpportunityItems OI
			INNER JOIN
				Item I
			ON
				OI.numItemCode = I.numItemCode
			OUTER APPLY
			(
				SELECT
					SUM(OpportunityBizDocItems.numUnitHour) AS InvoicedQty
				FROM
					OpportunityBizDocs
				INNER JOIN
					OpportunityBizDocItems 
				ON
					OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
				WHERE
					OpportunityBizDocs.numOppId = @numOppID
					AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
					AND ISNULL(OpportunityBizDocs.bitAuthoritativeBizDocs,0) = 1
			) AS TempInvoice
			WHERE
				OI.numOppID = @numOppID
				AND ISNULL(OI.numUnitHour,0) > 0
		) X
		WHERE
			X.OrderedQty > X.InvoicedQty) > 0
	BEGIN
		UPDATE @TMEP SET bitTrue = 1 WHERE ID = 2
	END

	SELECT * FROM @TMEP

	--GET FULFILLMENT BIZDOCS WHICH ARE NOT FULFILLED YET
	SELECT numOppBizDocsId FROM OpportunityBizDocs WHERE OpportunityBizDocs.numOppId = @numOppID AND ISNULL(OpportunityBizDocs.numBizDocId,0) = 296 AND ISNULL(OpportunityBizDocs.bitFulFilled,0) = 0
END


/****** Object:  StoredProcedure [dbo].[USP_DeleteCartItem]    Script Date: 11/08/2011 17:28:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_DeleteCartItem' ) 
                    DROP PROCEDURE USP_DeleteCartItem
                    Go
CREATE PROCEDURE [dbo].[USP_DeleteCartItem]
    (
      @numUserCntId NUMERIC(18, 0),
      @numDomainId NUMERIC(18, 0),
      @vcCookieId VARCHAR(100),
      @numCartId NUMERIC(18,0) = 0,
      @bitDeleteAll BIT=0,
	  @numSiteID NUMERIC(18,0) = 0,
	  @numItemCode NUMERIC(18,0) = 0
    )
AS 
BEGIN
	IF @bitDeleteAll = 0
	BEGIN
		IF @numCartId <> 0 
		BEGIN
			-- REMOVE DISCOUNT APPLIED TO ALL ITEMS FROM PROMOTION TRIGGERED OF ITEM TO BE DELETED
			UPDATE 
				CartItems
			SET 
				PromotionDesc='',PromotionID=0,fltDiscount=0,monTotAmount=(monPrice*numUnitHour),monTotAmtBefDiscount=(monPrice*numUnitHour),vcCoupon=NULL,bitParentPromotion=0
			WHERE
				PromotionID=(SELECT TOP 1 PromotionID FROM CartItems WHERE numDomainId = @numDomainId AND numUserCntId = @numUserCntId AND numCartId = @numCartId AND vcCookieId=@vcCookieId AND ISNULL(bitParentPromotion,0)=1)
				AND numDomainId = @numDomainId
				AND numUserCntId = @numUserCntId
				AND vcCookieId=@vcCookieId
				AND ISNULL(bitParentPromotion,0)=0

			--DELETE FROM CartItems WHERE numDomainId = @numDomainId AND numUserCntId = @numUserCntId AND numItemCode = @numItemCode	

			DECLARE @numTempItemCode NUMERIC(18,0)
			SELECT @numTempItemCode=numItemCode FROM CartItems WHERE numCartId=@numCartId

			DELETE FROM CartItems 
			WHERE numDomainId = @numDomainId AND numUserCntId = @numUserCntId
				AND (numCartId = @numCartId 
					OR numItemCode IN (SELECT numItemCode FROM SimilarItems WHERE numParentItemCode = @numTempItemCode AND numDomainId = @numDomainId ))	--AND bitrequired = 1

			EXEC USP_ManageECommercePromotion @numUserCntId,@numDomainId,@vcCookieId,0,@numSiteID
		END
		ELSE IF ISNULL(@numItemCode,0) > 0
		BEGIN
			-- REMOVE DISCOUNT APPLIED TO ALL ITEMS FROM PROMOTION TRIGGERED OF ITEM TO BE DELETED
			UPDATE 
				CartItems
			SET 
				PromotionDesc='',PromotionID=0,fltDiscount=0,monTotAmount=(monPrice*numUnitHour),monTotAmtBefDiscount=(monPrice*numUnitHour),vcCoupon=NULL,bitParentPromotion=0
			WHERE
				PromotionID=(SELECT TOP 1 PromotionID FROM CartItems WHERE numDomainId = @numDomainId AND numUserCntId = @numUserCntId AND numItemCode = @numItemCode AND vcCookieId=@vcCookieId AND ISNULL(bitParentPromotion,0)=1)
				AND numDomainId = @numDomainId
				AND numUserCntId = @numUserCntId
				AND vcCookieId=@vcCookieId
				AND ISNULL(bitParentPromotion,0)=0

			DELETE FROM CartItems WHERE numDomainId = @numDomainId AND numUserCntId = @numUserCntId AND vcCookieId = @vcCookieId AND numItemCode=@numItemCode
		END
		ELSE
		BEGIN
			UPDATE 
				CartItems
			SET 
				PromotionDesc='',PromotionID=0,fltDiscount=0,monTotAmount=(monPrice*numUnitHour),monTotAmtBefDiscount=(monPrice*numUnitHour),vcCoupon=NULL,bitParentPromotion=0
			WHERE
				numDomainId = @numDomainId
				AND numUserCntId = @numUserCntId
				AND vcCookieId=@vcCookieId

			DELETE FROM CartItems 
			WHERE numDomainId = @numDomainId AND numUserCntId = 0 AND vcCookieId = @vcCookieId		
		END
	END
	ELSE
	BEGIN
		DELETE FROM CartItems WHERE numDomainId = @numDomainId AND numUserCntId = @numUserCntId	AND vcCookieId <> ''
	END
END

 




SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='Usp_GetDashboardAllowedReports')
DROP PROCEDURE Usp_GetDashboardAllowedReports
GO
CREATE PROCEDURE [dbo].[Usp_GetDashboardAllowedReports]        
@numDomainId as numeric(9) ,
@numGroupId as numeric(9),
@tintMode AS TINYINT=0
as                                                 
      
IF @tintMode=0 --All Custom Reports
BEGIN
	SELECT RLM.numReportID,RLM.vcReportName,RLM.vcReportDescription,RMM.vcModuleName,RMGM.vcGroupName,  
	CASE WHEN ISNULL(DAR.numReportID,0)=0 THEN convert(bit,'false') ELSE convert(bit,'true') END AS bitallowed
	from ReportListMaster RLM JOIN ReportModuleMaster RMM ON RLM.numReportModuleID=RMM.numReportModuleID
	JOIN ReportModuleGroupMaster RMGM ON RLM.numReportModuleGroupID=RMGM.numReportModuleGroupID                                                  
	LEFT join ReportDashboardAllowedReports DAR on RLM.numReportID=DAR.numReportID AND DAR.numDomainID=@numDomainId AND DAR.numGrpID =  @numGroupId AND DAR.tintReportCategory=0
	where RLM.numDomainID=@numDomainID
	order by RLM.numReportID
END    
ELSE IF @tintMode=1 --Only Allowed Custom Reports
BEGIN
	SELECT RLM.numReportID,RLM.vcReportName,RLM.vcReportDescription,RMM.vcModuleName,RMGM.vcGroupName,  
	CASE WHEN ISNULL(DAR.numReportID,0)=0 THEN convert(bit,'false') ELSE convert(bit,'true') END AS bitallowed
	from ReportListMaster RLM JOIN ReportModuleMaster RMM ON RLM.numReportModuleID=RMM.numReportModuleID
	JOIN ReportModuleGroupMaster RMGM ON RLM.numReportModuleGroupID=RMGM.numReportModuleGroupID                                                  
	join ReportDashboardAllowedReports DAR on RLM.numReportID=DAR.numReportID 
	where RLM.numDomainID=@numDomainID AND DAR.numDomainID=@numDomainId AND DAR.numGrpID =  @numGroupId AND DAR.tintReportCategory=0
	order by RLM.numReportID 
END   
ELSE IF @tintMode=2 --All KPI Groups Reports
BEGIN
	SELECT KPI.numReportKPIGroupID,KPI.vcKPIGroupName,KPI.vcKPIGroupDescription,
	CASE KPI.tintKPIGroupReportType WHEN 3 THEN 'KPI' WHEN 4 THEN 'ScoreCard' END AS vcKPIGroupReportType,
	CASE WHEN ISNULL(DAR.numReportID,0)=0 THEN convert(bit,'false') ELSE convert(bit,'true') END AS bitallowed
	from ReportKPIGroupListMaster KPI                                                 
	LEFT join ReportDashboardAllowedReports DAR on KPI.numReportKPIGroupID=DAR.numReportID AND DAR.numDomainID=@numDomainId AND DAR.numGrpID =  @numGroupId AND DAR.tintReportCategory=1
	where KPI.numDomainID=@numDomainID
	order by KPI.numReportKPIGroupID
END    
ELSE IF @tintMode=3 --Only Allowed KPI Groups Reports
BEGIN
	SELECT KPI.numReportKPIGroupID,KPI.vcKPIGroupName,KPI.vcKPIGroupDescription,
	CASE KPI.tintKPIGroupReportType WHEN 3 THEN 'KPI' WHEN 4 THEN 'ScoreCard' END AS vcKPIGroupReportType,
	CASE WHEN ISNULL(DAR.numReportID,0)=0 THEN convert(bit,'false') ELSE convert(bit,'true') END AS bitallowed
	from ReportKPIGroupListMaster KPI                                                  
	join ReportDashboardAllowedReports DAR on KPI.numReportKPIGroupID=DAR.numReportID 
	where KPI.numDomainID=@numDomainID AND DAR.numDomainID=@numDomainId AND DAR.numGrpID =  @numGroupId AND DAR.tintReportCategory=1
	order by KPI.numReportKPIGroupID 
END   
ELSE IF @tintMode=4 --Default Reports
BEGIN
	SELECT 
		RLM.numReportID
		,RLM.vcReportName
		,RLM.intDefaultReportID
	FROM 
		ReportListMaster RLM 
	INNER JOIN 
		ReportDashboardAllowedReports DAR 
	ON
		RLM.intDefaultReportID=DAR.numReportID 
		AND DAR.numDomainID=@numDomainId 
		AND DAR.numGrpID =  @numGroupId 
		AND DAR.tintReportCategory=2
	WHERE 
		ISNULL(RLM.bitDefault,0) = 1
	ORDER BY 
		RLM.vcReportName
END
ELSE IF @tintMode=5 -- Default Reports With Permission
BEGIN
	SELECT 
		RLM.intDefaultReportID
		,RLM.vcReportName
		,RLM.vcReportDescription
		,CASE WHEN ISNULL(DAR.numReportID,0)=0 THEN convert(bit,'false') ELSE convert(bit,'true') END AS bitallowed
	FROM 
		ReportListMaster RLM 
	LEFT JOIN 
		ReportDashboardAllowedReports DAR 
	ON
		RLM.intDefaultReportID=DAR.numReportID 
		AND DAR.numDomainID=@numDomainId 
		AND DAR.numGrpID =  @numGroupId 
		AND DAR.tintReportCategory=2
	WHERE 
		ISNULL(RLM.numDomainID,0)=0
		AND ISNULL(RLM.bitDefault,0) = 1
	ORDER BY 
		RLM.vcReportName
END   
GO


--Created by Anoop Jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetFieldRelationship')
DROP PROCEDURE USP_GetFieldRelationship
GO
CREATE PROCEDURE USP_GetFieldRelationship
@byteMode as tinyint,
@numFieldRelID as numeric(9),
@numDomainID as numeric(9),
@numPrimaryListItemID as numeric(9),
@numSecondaryListID as numeric(9),
@bitTaskRelation AS BIT
as
BEGIN
	if @byteMode=1
	begin
	
	IF(@bitTaskRelation=1)
	BEGIN
		SELECT 
			FR.numFieldRelID,
			FR.numPrimaryListID,
			SD.vcStageName as RelationshipName
		FROM
			FieldRelationship AS FR
		LEFT JOIN
			StagePercentageDetails AS SD
		ON
			FR.numPrimaryListID=SD.numStageDetailsId
		WHERE
			FR.numDomainId=@numDomainID AND
			ISNULL(FR.bitTaskRelation,0)=1
	END
	ELSE
	BEGIN
		SELECT 
			numFieldRelID,L1.vcListName +' - ' + L2.vcListName as RelationshipName 
		FROM 
			FieldRelationship 
		JOIN 
			ListMaster L1
		ON 
			L1.numListID=numPrimaryListID
		JOIN 
			ListMaster L2
		ON
			L2.numListID=numSecondaryListID
		WHERE 
			FieldRelationship.numDomainID=@numDomainID 
			AND ISNULL(FieldRelationship.numModuleID,0) = 14
			AND ISNULL(bitTaskRelation,0)=0
		UNION
		SELECT 
			numFieldRelID,L1.vcItemName +' - ' + L2.vcItemName as RelationshipName 
		FROM 
			FieldRelationship 
		JOIN 
			Item L1
		ON 
			L1.numItemCode=numPrimaryListID
		JOIN 
			Item L2
		ON 
			L2.numItemCode=numSecondaryListID
		WHERE 
			FieldRelationship.numDomainID=@numDomainID
			AND ISNULL(FieldRelationship.numModuleID,0) = 14
			AND ISNULL(bitTaskRelation,0) = 0
	END
	end
	else if @byteMode=2
	begin

	Select numFieldRelID,ISNULL(FieldRelationship.numModuleID,0) numModuleID,numPrimaryListID,numSecondaryListID,L1.vcListName +' - ' + L2.vcListName as RelationshipName,
	L1.vcListName as Item1,  L2.vcListName as Item2 from FieldRelationship 
	Join ListMaster L1
	on L1.numListID=numPrimaryListID
	Join ListMaster L2
	on L2.numListID=numSecondaryListID
	where  numFieldRelID=@numFieldRelID AND ISNULL(FieldRelationship.numModuleID,0) <> 14 AND ISNULL(bitTaskRelation,0)=0
	UNION
		SELECT 
			numFieldRelID
			,ISNULL(numModuleID,0) numModuleID
			,numPrimaryListID
			,numSecondaryListID
			,L1.vcItemName +' - ' + L2.vcItemName AS RelationshipName
			,L1.vcItemName as Item1
			,L2.vcItemName as Item2 
		FROM 
			FieldRelationship 
		JOIN 
			Item L1
		ON 
			L1.numItemCode=numPrimaryListID
		JOIN 
			Item L2
		ON 
			L2.numItemCode=numSecondaryListID
		WHERE 
			numFieldRelID=@numFieldRelID 
			AND ISNULL(numModuleID,0) = 14
			 AND ISNULL(bitTaskRelation,0)=0

	if @numPrimaryListItemID >0  AND @bitTaskRelation=1
	begin
		Select FieldRelationship.numFieldRelID,numFieldRelDTLID,SLI1.vcTaskName as PrimaryListItem,SLI2.vcTaskName as SecondaryListItem   from FieldRelationship
		Join FieldRelationshipDTL 
		on FieldRelationshipDTL.numFieldRelID=FieldRelationship.numFieldRelID
		Join StagePercentageDetailsTask SLI1
		on SLI1.numTaskId=numPrimaryListItemID
		Join StagePercentageDetailsTask SLI2
		on SLI2.numTaskId=numSecondaryListItemID
		where FieldRelationship.numFieldRelID=@numFieldRelID and numPrimaryListItemID=@numPrimaryListItemID AND ISNULL(bitTaskRelation,0)=1
	end
	ELSE if @numPrimaryListItemID >0 
	begin
		SELECT 
			FieldRelationship.numFieldRelID
			,numFieldRelDTLID
			,L1.vcData AS PrimaryListItem
			,L2.vcData AS SecondaryListItem 
		FROM 
			FieldRelationship
		JOIN 
			FieldRelationshipDTL 
		ON 
			FieldRelationshipDTL.numFieldRelID=FieldRelationship.numFieldRelID
		JOIN 
			ListDetails L1
		ON 
			L1.numListItemID=numPrimaryListItemID
		JOIN 
			ListDetails L2
		ON 
			L2.numListItemID=numSecondaryListItemID
		WHERE 
			FieldRelationship.numFieldRelID=@numFieldRelID 
			AND numPrimaryListItemID=@numPrimaryListItemID
			AND ISNULL(numModuleID,0) <> 14
			AND ISNULL(bitTaskRelation,0)=0
		UNION
		SELECT 
			FieldRelationship.numFieldRelID
			,numFieldRelDTLID
			,L1.vcItemName AS PrimaryListItem
			,L2.vcItemName AS SecondaryListItem 
		FROM 
			FieldRelationship
		JOIN 
			FieldRelationshipDTL 
		ON 
			FieldRelationshipDTL.numFieldRelID=FieldRelationship.numFieldRelID
		JOIN 
			Item L1
		ON 
			L1.numItemCode=numPrimaryListItemID
		JOIN 
			Item L2
		ON 
			L2.numItemCode=numSecondaryListItemID
		WHERE 
			FieldRelationship.numFieldRelID=@numFieldRelID 
			AND numPrimaryListItemID=@numPrimaryListItemID
			AND ISNULL(numModuleID,0) = 14
			AND ISNULL(bitTaskRelation,0)=0
	end

	end
	else if @byteMode=3
	begin


		Select L2.numListItemID,L2.vcData as SecondaryListItem   from FieldRelationship
		Join FieldRelationshipDTL 
		on FieldRelationshipDTL.numFieldRelID=FieldRelationship.numFieldRelID
		Join ListDetails L2
		on L2.numListItemID=numSecondaryListItemID
		where FieldRelationship.numDomainID=@numDomainID and numPrimaryListItemID=@numPrimaryListItemID
		and FieldRelationship.numSecondaryListID=@numSecondaryListID AND ISNULL(bitTaskRelation,0)=0
	end
END
GO

/****** Object:  StoredProcedure [dbo].[usp_GetSalesFulfillmentOrder]    Script Date: 03/06/2009 00:39:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetSalesFulfillmentOrder')
DROP PROCEDURE USP_GetSalesFulfillmentOrder
GO
CREATE PROCEDURE [dbo].[USP_GetSalesFulfillmentOrder]
(
    @numDomainId AS NUMERIC(9) = 0,
    @SortCol VARCHAR(50),
    @SortDirection VARCHAR(4),
    @CurrentPage INT,
    @PageSize INT,
    @TotRecs INT OUTPUT,
    @numUserCntID NUMERIC,
    @vcBizDocStatus VARCHAR(500),
    @vcBizDocType varchar(500),
    @numDivisionID as numeric(9)=0,
    @ClientTimeZoneOffset INT=0,
    @bitIncludeHistory bit=0,
    @vcOrderStatus VARCHAR(500),
    @vcOrderSource VARCHAR(1000),
	@numOppID AS NUMERIC(18,0) = 0,
	@vcShippingService AS VARCHAR(MAX) = '',
	@numShippingZone NUMERIC(18,0)=0,
	@vcRegularSearchCriteria VARCHAR(MAX) = '',
	@vcCustomSearchCriteria VARCHAr(MAX) = ''
)
AS 
BEGIN

	DECLARE @firstRec AS INTEGER                                                              
	DECLARE @lastRec AS INTEGER                                                     
	SET @firstRec = ( @CurrentPage - 1 ) * @PageSize                             
	SET @lastRec = ( @CurrentPage * @PageSize + 1 )      
	
	DECLARE @tintCommitAllocation TINYINT
	SELECT @tintCommitAllocation=ISNULL(tintCommitAllocation,1) FROM Domain WHERE numDomainId=@numDomainId                                                              

	IF LEN(ISNULL(@SortCol,''))=0 OR CHARINDEX('OBD.',@SortCol) > 0 OR @SortCol='dtCreatedDate'
		SET @SortCol = 'Opp.bintCreatedDate'
		
	IF LEN(ISNULL(@SortDirection,''))=0	
		SET @SortDirection = 'DESC'

	CREATE TABLE #tempForm 
	(
		ID INT IDENTITY(1,1), tintOrder TINYINT,vcDbColumnName nvarchar(50),vcOrigDbColumnName nvarchar(50),vcFieldName nvarchar(50),vcAssociatedControlType nvarchar(50),vcListItemType varchar(3),
		numListID numeric(9),vcLookBackTableName varchar(50),bitCustomField BIT,numFieldId NUMERIC,bitAllowSorting BIT,bitAllowEdit BIT,bitIsRequired BIT,bitIsEmail BIT,
		bitIsAlphaNumeric BIT,bitIsNumeric BIT,bitIsLengthValidation BIT,intMaxLength int,intMinLength int,bitFieldMessage BIT,vcFieldMessage VARCHAR(500),
		ListRelID numeric(9),intColumnWidth int,bitAllowFiltering BIT,vcFieldDataType CHAR(1)
	)

	-- FIRST CHECK IF GRID COLUMNS ARE CONFIGURED
	DECLARE  @Nocolumns  AS TINYINT = 0
	DECLARE @numFormId INT = 23

	SELECT 
		@Nocolumns=ISNULL(SUM(TotalRow),0) 
	FROM
	(            
		SELECT 
			COUNT(*) TotalRow 
		FROM 
			View_DynamicColumns 
		WHERE 
			numFormId=@numFormId 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1
		UNION 
		SELECT 
			COUNT(*) TotalRow 
		FROM 
			View_DynamicCustomColumns 
		WHERE 
			numFormId=@numFormId 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
	) TotalRows

	-- IF GRID COLUMNS ARE NOT CONFIGURES THEN CHECK IF MASTER FORM CONFIGURATION IS CREATED BY ADMINISTRATOR
	DECLARE @IsMasterConfAvailable BIT = 0
	DECLARE @numUserGroup NUMERIC(18,0)

	IF @Nocolumns=0
	BEGIN
		SELECT @numUserGroup = numGroupID FROM UserMaster WHERE numUserDetailId = @numUserCntID 

		IF (SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = @numDomainID AND numFormID = @numFormId AND bitGridConfiguration = 1 AND numGroupID = @numUserGroup) > 0
		BEGIN
			SET @IsMasterConfAvailable = 1
		END
	END

	--IF MASTERCONFIGURATION IS AVAILABLE THEN LOAD IT OTHERWISE LOAD DEFAULT COLUMNS
	IF @IsMasterConfAvailable = 1
	BEGIN
		INSERT INTO DycFormConfigurationDetails 
		(
			numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth
		)
		SELECT 
			@numFormId,numFieldId,intColumnNum,intRowNum,@numDomainID,@numUserCntID,0,1,0,intColumnWidth
		FROM 
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId=@numFormId AND 
			View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
			ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
		UNION
		SELECT 
			@numFormId,numFieldId,intColumnNum,intRowNum,@numDomainID,@numUserCntID,0,1,1,intColumnWidth
		FROM 
			View_DynamicCustomColumnsMasterConfig
		WHERE 
			View_DynamicCustomColumnsMasterConfig.numFormId=@numFormId AND 
			View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1


		INSERT INTO 
			#tempForm
		SELECT 
			(intRowNum + 1) as tintOrder, vcDbColumnName,	vcOrigDbColumnName,	ISNULL(vcCultureFieldName,vcFieldName), vcAssociatedControlType,
			vcListItemType, numListID, vcLookBackTableName,	bitCustom, numFieldId, bitAllowSorting,	bitInlineEdit, bitIsRequired,
			bitIsEmail,	bitIsAlphaNumeric, bitIsNumeric, bitIsLengthValidation,	intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,
			ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType 
		FROM 
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId=@numFormId AND 
			View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
			ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
		UNION
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName, vcAssociatedControlType,'' as vcListItemType,numListID,'',
			bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,''
		FROM 
			View_DynamicCustomColumnsMasterConfig
		WHERE 
			View_DynamicCustomColumnsMasterConfig.numFormId=@numFormId AND 
			View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1
		ORDER BY 
			tintOrder ASC  
	END
	ELSE                                        
	BEGIN
		IF @Nocolumns=0
		BEGIN
			INSERT INTO DycFormConfigurationDetails 
			(
				numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth
			)
			SELECT 
				@numFormId,numFieldId,0,Row_number() over(order by tintRow desc),@numDomainID,@numUserCntID,0,1,0,intColumnWidth
			FROM 
				View_DynamicDefaultColumns
			WHERE 
				numFormId=@numFormId 
				AND bitDefault=1 
				AND ISNULL(bitSettingField,0)=1 
				AND numDomainID=@numDomainID
			ORDER BY 
				tintOrder asc   
		END

		INSERT INTO 
			#tempForm
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcOrigDbColumnName,ISNULL(vcCultureFieldName,vcFieldName),                  
			vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName,bitCustom,numFieldId,bitAllowSorting,bitInlineEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength,bitFieldMessage,
			vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType  
		FROM 
			View_DynamicColumns 
		WHERE 
			numFormId=@numFormId 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND ISNULL(bitSettingField,0)=1 
			AND ISNULL(bitCustom,0)=0  
		UNION
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName,vcAssociatedControlType,'' as vcListItemType,numListID,'',bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,'' 
		FROM 
			View_DynamicCustomColumns
		WHERE 
			numFormId=@numFormId 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND ISNULL(bitCustom,0)=1
		ORDER BY 
			tintOrder asc  
	END

	DECLARE @strColumns VARCHAR(MAX) = ''
    DECLARE @FROM NVARCHAR(MAX) = ''
    DECLARE @WHERE NVARCHAR(MAX) = ''

	SET @FROM =' FROM 
						OpportunityMaster Opp
					INNER JOIN 
						DivisionMaster DM 
					ON 
						Opp.numDivisionId = DM.numDivisionID
					INNER JOIN 
						CompanyInfo cmp 
					ON 
						cmp.numCompanyID=DM.numCompanyID
					LEFT JOIN
						AdditionalContactsInformation ADC
					ON
						Opp.numContactID=ADC.numContactID
					LEFT JOIN
						DivisionMasterShippingConfiguration DMSC
					ON
						DM.numDivisionID=DMSC.numDivisionID'
					
	SET @WHERE = CONCAT(' WHERE 
							(Opp.numOppID = ',@numOppID,' OR ',@numOppID,' = 0) 
							AND Opp.tintopptype = 1 
							AND Opp.tintOppStatus=1 ',(CASE WHEN @numDomainID <> 214 THEN ' AND (CHARINDEX(''Ready to Ship'', dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID)) > 0 OR CHARINDEX(''Not Applicable'', dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID)) > 0) ' ELSE '' END), ' 
							AND Opp.numDomainID = ',@numDomainId) --CONCAT(' AND 1 = dbo.IsSalesOrderReadyToFulfillment(Opp.numDomainID,Opp.numOppID,',@tintCommitAllocation,')') For domain 214 -- Removed because of performance problem

	IF @SortCol like 'CFW.Cust%'             
	BEGIN            
		SET @FROM = @FROM + ' LEFT JOIN CFW_Fld_Values_Opp CFW on CFW.RecId=Opp.numOppid and CFW.fld_id= '+ REPLACE(@SortCol,'CFW.Cust','') +' '                                                     
		SET @SortCol='CFW.Fld_Value'            
	END 
      
    DECLARE @strSql NVARCHAR(MAX)
    DECLARE @SELECT NVARCHAR(MAX) = 'WITH bizdocs AS 
									(SELECT
										Opp.numOppID
										,Opp.numRecOwner
										,Opp.vcpOppName
										,DM.tintCRMType
										,ISNULL(DM.numTerID,0) numTerID
										,DM.numDivisionID
										,ISNULL(Opp.intUsedShippingCompany,ISNULL(DM.intShippingCompany,90)) AS numShipVia
										,ISNULL(Opp.intUsedShippingCompany,ISNULL(DM.intShippingCompany,90)) as intUsedShippingCompany
										,ISNULL(Opp.numShippingService,0) numShippingService
										,CASE WHEN (SELECT COUNT(*) FROM ShippingBox INNER JOIN ShippingReport ON ShippingBox.numShippingReportId=ShippingReport.numShippingReportId WHERE ShippingReport.numOppID=Opp.numOppID AND LEN(ISNULL(vcTrackingNumber,'''')) > 0) > 0 THEN 1 ELSE 0 END AS bitTrackingNumGenerated
										,Opp.monDealAmount
										,(CASE WHEN (SELECT COUNT(*) FROM SalesFulfillmentQueue WHERE numDomainID = Opp.numDomainId AND numOppId=Opp.numOppId AND ISNULL(bitExecuted,0)=0) > 0 THEN 1 ELSE 0 END) as bitPendingExecution
										,ISNULL(Opp.monDealAmount,0) AS monDealAmount1'

	IF CHARINDEX('OBD.monAmountPaid',@vcRegularSearchCriteria) > 0
	BEGIN
		IF CHARINDEX('OBD.monAmountPaid like ''%0%''',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'OBD.monAmountPaid like ''%0%''',' 1 = (SELECT ISNULL((SELECT SUM(ISNULL(monAmountPaid,0)) FROM  OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)) ')
		IF CHARINDEX('OBD.monAmountPaid like ''%1%''',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'OBD.monAmountPaid like ''%1%''','  1 = (SELECT (CASE WHEN (SELECT (CASE WHEN (SUM(ISNULL(monDealAmount,0)) = SUM(ISNULL(monAmountPaid,0)) AND SUM(ISNULL(monAmountPaid,0)) <> 0) THEN 1 ELSE 0 END) FROM OpportunityBizDocs BD WHERE BD.numOppId =  Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0) = 1) > 0 THEN 1 ELSE 0 END))  ')
		IF CHARINDEX('OBD.monAmountPaid like ''%2%''',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'OBD.monAmountPaid like ''%2%''',' 1 = (SELECT CASE WHEN (SELECT (CASE WHEN ((SUM(ISNULL(monDealAmount,0)) - SUM(ISNULL(monAmountPaid,0))) > 0 AND SUM(ISNULL(monAmountPaid,0)) <> 0) THEN 1 ELSE 0 END) FROM OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0) = 1) > 0 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('OBD.monAmountPaid like ''%3%''',@vcRegularSearchCriteria) > 0								   
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'OBD.monAmountPaid like ''%3%''',' 1 = (SELECT CASE WHEN (SELECT (CASE WHEN (SUM(ISNULL(monAmountPaid,0)) = 0) THEN 1 ELSE 0 END) FROM OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0) = 1) > 0 THEN 1 ELSE 0 END) ')
	END
	
	IF CHARINDEX('OI.vcInventoryStatus',@vcRegularSearchCriteria) > 0
	BEGIN
		IF CHARINDEX('OI.vcInventoryStatus=1',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'OI.vcInventoryStatus=1',' CHARINDEX(''Not Applicable'', dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID)) > 0 ')
		IF CHARINDEX('OI.vcInventoryStatus=2',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'OI.vcInventoryStatus=2',' CHARINDEX(''Allocation Cleared'', dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID)) > 0 ')
		IF CHARINDEX('OI.vcInventoryStatus=3',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'OI.vcInventoryStatus=3',' CHARINDEX(''Back Order'', dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID)) > 0 ')
		IF CHARINDEX('OI.vcInventoryStatus=4',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'OI.vcInventoryStatus=4',' CHARINDEX(''Ready to Ship'', dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID)) > 0 ')
	END

	IF CHARINDEX('dbo.fn_getOPPAddressState(Opp.numOppId,Opp.numDomainID,2)',@vcRegularSearchCriteria) > 0
	BEGIN
		SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'dbo.fn_getOPPAddressState(Opp.numOppId,Opp.numDomainID,2)','(CASE 
																																		WHEN Opp.tintShipToType IS NULL OR Opp.tintShipToType = 1
																																		THEN
																																			(SELECT  
																																				ISNULL(dbo.fn_GetState(AD.numState),'''')
																																			FROM 
																																				AddressDetails AD 
																																			WHERE 
																																				AD.numDomainID=Opp.numDomainID AND AD.numRecordID=Opp.numDivisionID 
																																				AND AD.tintAddressOf = 2 AND AD.tintAddressType = 2 AND AD.bitIsPrimary=1)
																																		WHEN Opp.tintShipToType = 0
																																		THEN
																																			(SELECT 
																																				ISNULL(dbo.fn_GetState(AD.numState),'''')
																																			FROM 
																																				CompanyInfo [Com1] 
																																			JOIN 
																																				DivisionMaster div1 
																																			ON 
																																				com1.numCompanyID = div1.numCompanyID
																																			JOIN Domain D1 ON D1.numDivisionID = div1.numDivisionID
																																			JOIN dbo.AddressDetails AD ON AD.numDomainID = div1.numDomainID AND AD.numRecordID = div1.numDivisionID AND tintAddressOf = 2 AND tintAddressType = 2 AND bitIsPrimary=1
																																			WHERE  D1.numDomainID = Opp.numDomainID)
																																		WHEN Opp.tintShipToType = 2 OR Opp.tintShipToType = 3
																																		THEN
																																			(SELECT
																																				ISNULL(dbo.fn_GetState(numShipState),'''')
																																			FROM 
																																				OpportunityAddress 
																																			WHERE 
																																				numOppID = Opp.numOppId)
																																		ELSE ''''
																																	END)')
	END
	

	IF @vcRegularSearchCriteria<>'' 
	BEGIN
		SET @WHERE = @WHERE + ' AND ' + @vcRegularSearchCriteria 
	END

	IF @vcCustomSearchCriteria<>'' 
	BEGIN
		SET @WHERE = @WHERE +' AND ' +  @vcCustomSearchCriteria
	END

	DECLARE @tintOrder  AS TINYINT;SET @tintOrder = 0
	DECLARE @vcFieldName  AS VARCHAR(50)
	DECLARE @vcListItemType  AS VARCHAR(3)
	DECLARE @vcListItemType1  AS VARCHAR(1)
	DECLARE @vcAssociatedControlType VARCHAR(30)
	DECLARE @numListID  AS NUMERIC(9)
	DECLARE @vcDbColumnName VARCHAR(40)
	DECLARE @vcLookBackTableName VARCHAR(2000)
	DECLARE @bitCustom  AS BIT
	Declare @numFieldId as numeric  
	DECLARE @bitAllowSorting AS CHAR(1)   
	DECLARE @bitAllowEdit AS CHAR(1)                 
	DECLARE @vcColumnName AS VARCHAR(500)
	DECLARE @ListRelID AS NUMERIC(9) 

	DECLARE @i AS INT = 1
	DECLARE @iCount AS INT 
	SELECT @iCount = COUNT(*) FROM #tempForm

	WHILE @i <= @iCount
	BEGIN
		SELECT 
			@tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName,@vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,
			@numListID=numListID,@vcLookBackTableName=vcLookBackTableName ,@bitCustom=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,@bitAllowEdit=bitAllowEdit,
			@ListRelID=ListRelID
		FROM 
			#tempForm 
		WHERE
			ID=@i

		IF @bitCustom = 0
		BEGIN
			DECLARE  @Prefix  AS VARCHAR(5)

			IF @vcLookBackTableName = 'AdditionalContactsInformation'
				SET @Prefix = 'ADC.'
			IF @vcLookBackTableName = 'DivisionMaster'
				SET @Prefix = 'DM.'
			IF @vcLookBackTableName = 'OpportunityMaster'
				SET @PreFix = 'Opp.'
			IF @vcLookBackTableName = 'CompanyInfo'
				SET @PreFix = 'cmp.'
			IF @vcLookBackTableName = 'DivisionMasterShippingConfiguration'
				SET @PreFix = 'DMSC.'

			SET @vcColumnName= @vcDbColumnName+'~'+ convert(varchar(10),@numFieldId) +'~0'

			IF @vcAssociatedControlType = 'SelectBox'
			BEGIN
				IF @vcDbColumnName = 'numPartner'
				BEGIN
					SET @strColumns=@strColumns+ ' ,(SELECT D.vcPartnerCode+''-''+C.vcCompanyName FROM DivisionMaster AS D LEFT JOIN CompanyInfo AS C ON D.numCompanyID=C.numCompanyID WHERE D.numDivisionID=Opp.numPartner) '+' ['+ @vcColumnName+']'    
					SET @strColumns=@strColumns+ ' ,(SELECT D.vcPartnerCode+''-''+C.vcCompanyName FROM DivisionMaster AS D LEFT JOIN CompanyInfo AS C ON D.numCompanyID=C.numCompanyID WHERE D.numDivisionID=Opp.numPartner) AS vcPartner' 
				END
				ELSE IF @vcDbColumnName = 'tintSource'
				BEGIN
					SET @strColumns = @strColumns + ',ISNULL(dbo.fn_GetOpportunitySourceValue(ISNULL(Opp.tintSource,0),ISNULL(Opp.tintSourceType,0),Opp.numDomainID),''Internal Order'')' + ' [' + @vcColumnName + ']'
				END
				ELSE IF @vcDbColumnName = 'numContactID'
				BEGIN
					SET @strColumns = @strColumns + ',dbo.fn_GetContactName(' + @Prefix + @vcDbColumnName + ') [' + @vcColumnName + ']'
				END
				ELSE IF @vcListItemType = 'LI'
				BEGIN
					SET @strColumns = @strColumns + ',L' + CONVERT(VARCHAR(3),@tintOrder) + '.vcData' + ' [' + @vcColumnName + ']'
					SET @FROM = @FROM + ' LEFT JOIN ListDetails L' + CONVERT(VARCHAR(3),@tintOrder) + ' on L' + CONVERT(VARCHAR(3),@tintOrder) + '.numListItemID=' + @vcDbColumnName
				END
				ELSE IF @vcListItemType = 'U'
				BEGIN
					SET @strColumns = @strColumns + ',dbo.fn_GetContactName(' + @Prefix + @vcDbColumnName + ') [' + @vcColumnName + ']'
				END
				ELSE IF @vcListItemType = 'SGT'
				BEGIN
					SET @strColumns = @strColumns + ',(CASE ' + @Prefix + @vcDbColumnName + ' 
														WHEN 0 THEN ''Service Default''
														WHEN 1 THEN ''Adult Signature Required''
														WHEN 2 THEN ''Direct Signature''
														WHEN 3 THEN ''InDirect Signature''
														WHEN 4 THEN ''No Signature Required''
														ELSE ''''
														END) [' + @vcColumnName + ']'
				END
				ELSE IF @vcListItemType = 'PSS'
				BEGIN
					SET @strColumns = @strColumns + ',ISNULL((SELECT vcShipmentService FROM ShippingService WHERE (numDomainID=Opp.numDomainID OR ISNULL(numDomainID,0)=0) AND numShippingServiceID = ' + @Prefix + @vcDbColumnName + '),'''') [' + @vcColumnName + ']'
				END
            END
			ELSE IF @vcAssociatedControlType = 'DateField'
			BEGIN
				IF @vcDbColumnName = 'dtReleaseDate'
				BEGIN
					SET @strColumns = @strColumns
                                + ',case when convert(varchar(11),'
                                + @Prefix
                                + @vcDbColumnName
                                + ')= convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',getdate())) then ''<b><font color=red>Today</font></b>'''
					SET @strColumns = @strColumns
									+ 'when convert(varchar(11),'
									+ @Prefix
									+ @vcDbColumnName
									+ ')= convert(varchar(11),dateadd(day,-1,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',getdate()))) then ''<b><font color=purple>Yesterday</font></b>'''
					SET @strColumns = @strColumns
									+ 'when convert(varchar(11),'
									+ @Prefix
									+ @vcDbColumnName
									+ ')= convert(varchar(11),dateadd(day,1,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',getdate()))) then''<b><font color=orange>Tommorow</font></b>'' '
					SET @strColumns = @strColumns
									+ 'else dbo.FormatedDateFromDate('
									+ @Prefix
									+ @vcDbColumnName
									+ ','
									+ CONVERT(VARCHAR(10),@numDomainId)
									+ ') end  ['
									+ @vcColumnName + ']'
				END
				ELSE
				BEGIN
					SET @strColumns = @strColumns
                                + ',case when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+','
                                + @Prefix
                                + @vcDbColumnName
                                + ') )= convert(varchar(11),getdate()) then ''<b><font color=red>Today</font></b>'''
					SET @strColumns = @strColumns
									+ 'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+','
									+ @Prefix
									+ @vcDbColumnName
									+ ') )= convert(varchar(11),dateadd(day,-1,getdate())) then ''<b><font color=purple>Yesterday</font></b>'''
					SET @strColumns = @strColumns
									+ 'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+','
									+ @Prefix
									+ @vcDbColumnName
									+ ') )= convert(varchar(11),dateadd(day,1,getdate())) then''<b><font color=orange>Tommorow</font></b>'' '
					SET @strColumns = @strColumns
									+ 'else dbo.FormatedDateFromDate('
									+ @Prefix
									+ @vcDbColumnName
									+ ','
									+ CONVERT(VARCHAR(10),@numDomainId)
									+ ') end  ['
									+ @vcColumnName + ']'
				 END	
            END
            ELSE IF @vcAssociatedControlType = 'TextBox' OR @vcAssociatedControlType = 'TextArea'
            BEGIN
				IF @vcDbColumnName = 'vcCompanyName'
				BEGIN
					SET @strColumns = @strColumns + ',' + @Prefix + @vcDbColumnName + ' [' + @vcColumnName + ']'
					SET @strColumns = @strColumns + ',dbo.fn_GetListItemName(Opp.[numstatus]) vcOrderStatus'
				END
				ELSE IF @vcDbColumnName = 'numShipState'
				BEGIN
					SET @strColumns = @strColumns + ',dbo.fn_getOPPAddressState(Opp.numOppId,Opp.numDomainID,2)' + ' [' + @vcColumnName + ']'
				END
				ELSE
				BEGIN
					SET @strColumns = @strColumns + ',' + @Prefix + @vcDbColumnName + ' [' + @vcColumnName + ']'
				END
            END
            ELSE IF  @vcAssociatedControlType='Label'                                              
			BEGIN
				SET @strColumns=@strColumns + ',' + CASE   
				WHEN @vcDbColumnName = 'vcFulfillmentStatus' THEN 'ISNULL(STUFF((SELECT CONCAT(''<br />'',vcMessage) FROM SalesFulfillmentLog WHERE numDomainID = Opp.numDomainId AND numOppID=Opp.numOppId AND ISNULL(bitSuccess,0) = 1 FOR XML PATH('''')) ,1,12,''''),'''')'                 
				WHEN @vcDbColumnName = 'vcPricedBoxedTracked' THEN 'STUFF((SELECT CONCAT('', '',OpportunityBizDocs.numOppID,''~'',numOppBizDocsId,''~'',ISNULL(numShippingReportId,0),''~'',(CASE WHEN (SELECT COUNT(*) FROM ShippingBox WHERE numShippingReportId=ShippingReport.numShippingReportId) > 0 THEN 1 ELSE 0 END)) vcText FROM OpportunityBizDocs LEFT JOIN ShippingReport ON ShippingReport.numOppBizDocId=OpportunityBizDocs.numOppBizDocsId WHERE OpportunityBizDocs.numBizDocId IN (SELECT ISNULL(numDefaultSalesShippingDoc,0) FROM Domain WHERE numDomainID=Opp.numDomainID) AND OpportunityBizDocs.numOppId=Opp.numOppID AND OpportunityBizDocs.bitShippingGenerated=1 FOR XML PATH(''''), TYPE).value(''.'',''NVARCHAR(MAX)''),1,2,'' '')'
				WHEN @vcDbColumnName = 'CalAmount' THEN 'Opp.monDealAmount'--'[dbo].[getdealamount](Opp.numOppId,Getutcdate(),0)'
				WHEN @vcDbColumnName = 'vcInventoryStatus' THEN 'ISNULL(dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID),'''')'
				WHEN @vcDbColumnName = 'vcOrderedShipped' THEN 'dbo.GetOrderShipReceiveStatus(Opp.numOppId,1,Opp.tintshipped)'
				WHEN @vcDbColumnName = 'vcOrderedReceived' THEN 'dbo.GetOrderShipReceiveStatus(Opp.numOppId,2,Opp.tintshipped)'
				WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'monDealAmount' THEN 'ISNULL((SELECT SUM(ISNULL(monDealAmount,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)'
				WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'monAmountPaid' THEN 'ISNULL((SELECT SUM(ISNULL(monAmountPaid,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)'
				ELSE @Prefix + @vcDbColumnName END +' ['+ @vcColumnName+']'        
			END
        END
		ELSE
        BEGIN
			SET @vcColumnName= @vcDbColumnName +'~'+ convert(varchar(10),@numFieldId)+'~1'

			IF @vcAssociatedControlType = 'TextBox' or @vcAssociatedControlType = 'TextArea'
            BEGIN
              SET @strColumns = @strColumns
                              + ',CFW'
                              + CONVERT(VARCHAR(3),@tintOrder)
                              + '.Fld_Value  ['
                              + @vcColumnName + ']'
              SET @FROM = @FROM
                                      + ' left Join CFW_FLD_Values_Opp CFW'
                                      + CONVERT(VARCHAR(3),@tintOrder)
                                      + ' on CFW'
                                      + CONVERT(VARCHAR(3),@tintOrder)
                                      + '.Fld_Id='
                                      + CONVERT(VARCHAR(10),@numFieldId)
                                      + 'and CFW'
                                      + CONVERT(VARCHAR(3),@tintOrder)
                                      + '.RecId=Opp.numOppid   '
			END
			ELSE
            IF @vcAssociatedControlType = 'CheckBox' 
            BEGIN
                SET @strColumns = @strColumns
                                + ',case when isnull(CFW'
                                + CONVERT(VARCHAR(3),@tintOrder)
                                + '.Fld_Value,0)=0 then 0 when isnull(CFW'
                                + CONVERT(VARCHAR(3),@tintOrder)
                                + '.Fld_Value,0)=1 then 1 end   ['
                                + @vcColumnName + ']'
                SET @FROM = @FROM
                                        + ' left Join CFW_FLD_Values_Opp CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + ' on CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.Fld_Id='
                                        + CONVERT(VARCHAR(10),@numFieldId)
                                        + 'and CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.RecId=Opp.numOppid   '
            END
            ELSE
            IF @vcAssociatedControlType = 'DateField'
            BEGIN
                  SET @strColumns = @strColumns
                                  + ',dbo.FormatedDateFromDate(CFW'
                                  + CONVERT(VARCHAR(3),@tintOrder)
                                  + '.Fld_Value,'
                                  + CONVERT(VARCHAR(10),@numDomainId)
                                  + ')  ['
                                  + @vcColumnName + ']'
                  SET @FROM = @FROM
                                          + ' left Join CFW_FLD_Values_Opp CFW'
                                          + CONVERT(VARCHAR(3),@tintOrder)
                                          + ' on CFW'
                                          + CONVERT(VARCHAR(3),@tintOrder)
                                          + '.Fld_Id='
                                          + CONVERT(VARCHAR(10),@numFieldId)
                                          + 'and CFW'
                                          + CONVERT(VARCHAR(3),@tintOrder)
                                          + '.RecId=Opp.numOppid    '
            END
            ELSE
            IF @vcAssociatedControlType = 'SelectBox'
            BEGIN
                SET @vcDbColumnName = 'DCust'
                                            + CONVERT(VARCHAR(10),@numFieldId)
                SET @strColumns = @strColumns
                                + ',L'
                                + CONVERT(VARCHAR(3),@tintOrder)
                                + '.vcData'
                                + ' ['
                                + @vcColumnName + ']'

                SET @FROM = @FROM
                                        + ' left Join CFW_FLD_Values_Opp CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + ' on CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.Fld_Id='
                                        + CONVERT(VARCHAR(10),@numFieldId)
                                        + 'and CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.RecId=Opp.numOppid '

                SET @FROM = @FROM
                                        + ' left Join ListDetails L'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + ' on L'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.numListItemID=CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.Fld_Value'
            END
			ELSE
			BEGIN
				SET @strColumns = @strColumns + CONCAT(',dbo.GetCustFldValueOpp(',@numFieldId,',opp.numOppID)') + ' [' + @vcColumnName + ']'
			END
		END

		SET @i = @i + 1
	END

		
		DECLARE @numShippingServiceItemID AS NUMERIC
		SELECT @numShippingServiceItemID=ISNULL(numShippingServiceItemID,0) FROM domain WHERE numDomainID=@numDomainID
		
		IF  LEN(ISNULL(@vcOrderStatus,''))>0
		BEGIN
			SET @WHERE = @WHERE + ' and isnull(Opp.numStatus,0) in (SELECT Items FROM dbo.Split(''' +@vcOrderStatus + ''','',''))' 			
		END

		IF LEN(ISNULL(@vcShippingService,''))>0
		BEGIN
			SET @WHERE = @WHERE + ' and isnull(Opp.numShippingService,0) in (SELECT Items FROM dbo.Split(''' +@vcShippingService + ''','',''))'
		END

		IF ISNULL(@numShippingZone,0) > 0
		BEGIN
			SET @WHERE = @WHERE + ' AND 1 = (CASE 
												WHEN ISNULL((SELECT TOP 1 numState FROM fn_getOPPAddressDetails(Opp.numOppId,Opp.numDomainID,2)),0) > 0 
												THEN
													CASE 
														WHEN (SELECT COUNT(*) FROM dbo.State WHERE numDomainID=' + CAST(@numDomainId AS VARCHAR) + ' AND numStateID =  (SELECT TOP 1 numState FROM fn_getOPPAddressDetails(Opp.numOppId,Opp.numDomainID,2)) AND numShippingZone=' + CAST(@numShippingZone as VARCHAR) + ' ) > 0 
														THEN 1
														ELSE 0
													END
												ELSE 0 
											END)'
		END
		
		IF LEN(ISNULL(@vcOrderSource,''))>0
		BEGIN
				SET @WHERE = @WHERE + ' and (' + @vcOrderSource +')' 			
		END
		
		SET @WHERE = @WHERE + ' AND (Opp.numDivisionID ='+ CONVERT(VARCHAR(15), @numDivisionID) + ' OR '+ CONVERT(VARCHAR(15), @numDivisionID) + ' = 0) '
		
        SET @strSql = CONCAT(@SELECT,@strColumns,@FROM,@WHERE,' ORDER BY ',@SortCol,' ',@SortDirection,' OFFSET ',((@CurrentPage - 1) * @PageSize),' ROWS FETCH NEXT ',@PageSize, ' ROWS ONLY) SELECT * INTO #temp FROM [bizdocs];')
        
		SET @strSql = @strSql + CONCAT('SELECT 
											OM.*
											,CAST((Case WHEN ISNULL(OI.numoppitemtCode,0)>0 then 1 else 0 end) as bit) as IsShippingServiceItemID
										FROM #temp OM 
											LEFT JOIN (SELECT numOppId,numoppitemtCode,ISNULL(vcItemDesc, '''') vcItemDesc,monTotAmount,
											Row_number() OVER(PARTITION BY numOppId ORDER BY numoppitemtCode) AS row 
											FROM OpportunityItems OI WHERE OI.numOppId in (SELECT numOppId FROM #temp) AND numItemCode=',@numShippingServiceItemID,') 
											OI ON OI.row=1 AND OM.numOppId = OI.numOppId; 
											
											SELECT 
											opp.numOppId
											,Opp.vcitemname AS vcItemName
											,ISNULL(dbo.FormatedDateFromDate(opp.ItemReleaseDate,',@numDomainId,'),'''')  AS vcItemReleaseDate
											,ISNULL((SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numDomainId=',@numDomainId,' AND ItemImages.numItemCode=i.numItemCode AND bitDefault=1 AND bitIsImage=1),'''') AS vcImage
											,CASE 
												WHEN charitemType = ''P'' THEN ''Product''
												WHEN charitemType = ''S'' THEN ''Service''
											END AS charitemType
											,CASE WHEN charitemType = ''P'' THEN ''Inventory''
												WHEN charitemType = ''N'' THEN ''Non-Inventory''
												WHEN charitemType = ''S'' THEN ''Service''
											END AS vcItemType
											,CASE WHEN ISNULL(numItemGroup,0) > 0 THEN WI.vcWHSKU ELSE i.vcSKU END vcSKU
											,dbo.USP_GetAttributes(opp.numWarehouseItmsID,bitSerialized) AS vcAttributes
											,ISNULL(W.vcWareHouse, '''') AS Warehouse,WL.vcLocation,opp.numUnitHour AS numUnitHourOrig
											,Opp.bitDropShip AS DropShip
											,SUBSTRING((SELECT  
															'' ,'' + vcSerialNo
																				+ CASE WHEN ISNULL(I.bitLotNo, 0) = 1
																					   THEN ''(''
																							+ CONVERT(VARCHAR(15), oppI.numQty)
																							+ '')''
																					   ELSE ''''
																				  END
														FROM    OppWarehouseSerializedItem oppI
																JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID = whi.numWareHouseItmsDTLID
														WHERE   oppI.numOppID = Opp.numOppId
																AND oppI.numOppItemID = Opp.numoppitemtCode
														ORDER BY vcSerialNo
														FOR
														XML PATH('''')
														), 3, 200000) AS SerialLotNo
											,ISNULL(bitSerialized,0) AS bitSerialized
											,ISNULL(bitLotNo,0) AS bitLotNo
											,Opp.numWarehouseItmsID
											,opp.numoppitemtCode
										FROM 
											OpportunityItems opp 
										JOIN 
											#temp OM 
										ON 
											opp.numoppid=OM.numoppid 
										LEFT JOIN 
											Item i 
										ON 
											opp.numItemCode = i.numItemCode
										INNER JOIN 
											dbo.WareHouseItems WI 
										ON 
											opp.numWarehouseItmsID = WI.numWareHouseItemID
											AND WI.numDomainID = ',@numDomainId,'
										INNER JOIN 
											dbo.Warehouses W 
										ON 
											WI.numDomainID = W.numDomainID
											AND WI.numWareHouseID = W.numWareHouseID
										LEFT JOIN 
											dbo.WarehouseLocation WL 
											ON WL.numWLocationID= WI.numWLocationID;
			
										DROP TABLE #temp;')

        PRINT CAST(@strSql AS NTEXT);
        EXEC ( @strSql ) ;
       
      
        
        /*Get total count */
        SET @strSql =  ' (SELECT @TotRecs =COUNT(*) ' + @FROM + @WHERE + ')';
        
        PRINT @strSql ;
        EXEC sp_executesql 
        @query = @strSql, 
        @params = N'@TotRecs INT OUTPUT', 
        @TotRecs = @TotRecs OUTPUT 
        
		SELECT * FROM #tempForm
		DROP TABLE #tempForm
    END
  
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_InLineEditDataSave')
DROP PROCEDURE USP_InLineEditDataSave
GO
CREATE PROCEDURE [dbo].[USP_InLineEditDataSave] 
( 
 @numDomainID NUMERIC(9),
 @numUserCntID NUMERIC(9),
 @numFormFieldId NUMERIC(9),
 @bitCustom AS BIT = 0,
 @InlineEditValue VARCHAR(8000),
 @numContactID NUMERIC(9),
 @numDivisionID NUMERIC(9),
 @numWOId NUMERIC(9),
 @numProId NUMERIC(9),
 @numOppId NUMERIC(9),
 @numRecId NUMERIC(9),
 @vcPageName NVARCHAR(100),
 @numCaseId NUMERIC(9),
 @numWODetailId NUMERIC(9),
 @numCommID NUMERIC(9)
 )
AS 

declare @strSql as nvarchar(4000)
DECLARE @vcDbColumnName AS VARCHAR(100),@vcOrigDbColumnName AS VARCHAR(100),@vcLookBackTableName AS varchar(100)
DECLARE @vcListItemType as VARCHAR(3),@numListID AS numeric(9),@vcAssociatedControlType varchar(20)
declare @tempAssignedTo as numeric(9);set @tempAssignedTo=null           
DECLARE @Value1 AS VARCHAR(18),@Value2 AS VARCHAR(18)

IF  @bitCustom =0
BEGIN
	SELECT @vcDbColumnName=vcDbColumnName,@vcLookBackTableName=vcLookBackTableName,
		   @vcListItemType=vcListItemType,@numListID=numListID,@vcAssociatedControlType=vcAssociatedControlType,
		   @vcOrigDbColumnName=vcOrigDbColumnName
	 FROM DycFieldMaster WHERE numFieldId=@numFormFieldId
	 
	IF @vcAssociatedControlType = 'DateField' AND  @InlineEditValue=''
	BEGIN
		SET @InlineEditValue = null
	END

	DECLARE @intExecuteDiv INT=0
	if @vcLookBackTableName = 'DivisionMaster' 
	 BEGIN 
		
	   IF @vcDbColumnName='numFollowUpStatus' --Add to FollowUpHistory
	    Begin
			declare @numFollow as varchar(10),@binAdded as varchar(20),@PreFollow as varchar(10),@RowCount as varchar(2)                            
			                      
			set @PreFollow=(select count(*)  numFollowUpStatus from FollowUpHistory where numDivisionID= @numDivisionID and numDomainId= @numDomainID)                          
			set @RowCount=@@rowcount  
			                        
			select   @numFollow=numFollowUpStatus, @binAdded=bintModifiedDate from divisionmaster where numDivisionID=@numDivisionID and numDomainId= @numDomainID                    
			
			if @numFollow <>'0' and @numFollow <> @InlineEditValue                          
			begin                          
				if @PreFollow<>0                          
					begin                          
			                   
						if @numFollow <> (select top 1  numFollowUpStatus from FollowUpHistory where numDivisionID= @numDivisionID and numDomainId= @numDomainID order by numFollowUpStatusID desc)                          
							begin                          
			                      	insert into FollowUpHistory(numFollowUpstatus,numDivisionID,bintAddedDate,numDomainId)                          
									values(@numFollow,@numDivisionID,@binAdded,@numDomainID)                          
							end                          
					end                          
				else                          
					begin                          
						insert into FollowUpHistory(numFollowUpstatus,numDivisionID,bintAddedDate,numDomainId)                          
						 values(@numFollow,@numDivisionID,@binAdded,@numDomainID)                          
					end                          
			   end 
			END
		 IF @vcDbColumnName='numAssignedTo' ---Updating if organization is assigned to someone
			BEGIN
				select @tempAssignedTo=isnull(numAssignedTo,0) from DivisionMaster where numDivisionID = @numDivisionID and numDomainId= @numDomainID           
			
				if (@tempAssignedTo<>@InlineEditValue and  @InlineEditValue<>'0')          
					begin            
						update DivisionMaster set numAssignedTo=@InlineEditValue ,numAssignedBy=@numUserCntID where numDivisionID = @numDivisionID  and numDomainId= @numDomainID  
						UPDATE CompanyInfo SET bintModifiedDate=GETUTCDATE() WHERE numCompanyId IN (SELECT numCompanyId 
																									FROM 
																										DivisionMaster
																									WHERE 
																										numDivisionID = @numDivisionID  and numDomainId= @numDomainID)         
					end					
				else if  (@InlineEditValue ='0')          
					begin          
						update DivisionMaster set numAssignedTo=0 ,numAssignedBy=0 where numDivisionID = @numDivisionID  and numDomainId= @numDomainID
						UPDATE CompanyInfo SET bintModifiedDate=GETUTCDATE() WHERE numCompanyId IN (SELECT numCompanyId 
																									FROM 
																										DivisionMaster
																									WHERE 
																										numDivisionID = @numDivisionID  and numDomainId= @numDomainID)          
					end    
			END
	    UPDATE CompanyInfo SET numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() WHERE numCompanyId=(SELECT TOP 1 numCompanyId FROM DivisionMaster where numDivisionID=@numDivisionID and numDomainID=@numDomainID) and numDomainID=@numDomainID
		
		IF @vcDbColumnName='vcPartnerCode'
		BEGIN
			IF(@InlineEditValue<>'')
			BEGIN
				IF EXISTS(SELECT vcPartnerCode FROM DivisionMaster where numDivisionID<>@numDivisionID and numDomainID=@numDomainID AND vcPartnerCode=@InlineEditValue)
				BEGIN
					SET @intExecuteDiv=1
					SET @InlineEditValue='-#12'
				END
			END
		END
		IF @intExecuteDiv=0
		BEGIN
			SET @strSql='update DivisionMaster set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numDivisionID=@numDivisionID and numDomainID=@numDomainID'
			
		END
	 END 
	 ELSE if @vcLookBackTableName = 'AdditionalContactsInformation' 
	 BEGIN 
			IF @vcDbColumnName='bitPrimaryContact' AND @InlineEditValue='True'
			BEGIN
				DECLARE @bitPrimaryContactCheck AS BIT             
				DECLARE @numID AS NUMERIC(9) 
				
				SELECT TOP 1 @numID = numContactID,@bitPrimaryContactCheck = ISNULL(bitPrimaryContact,0)
					FROM AdditionalContactsInformation WHERE numDivisionID = @numDivisionID 
                   
					IF @@rowcount = 0 SET @numID = 0             
				
					WHILE @numID > 0            
					BEGIN            
						IF @bitPrimaryContactCheck = 1 
							UPDATE  AdditionalContactsInformation SET bitPrimaryContact = 0 WHERE numContactID = @numID  
                              
						SELECT TOP 1 @numID = numContactID,@bitPrimaryContactCheck = ISNULL(bitPrimaryContact,0)
						FROM AdditionalContactsInformation WHERE numDivisionID = @numDivisionID AND numContactID > @numID    
                                
						IF @@rowcount = 0 SET @numID = 0             
					END 
				END
				ELSE IF @vcDbColumnName='bitPrimaryContact' AND @InlineEditValue='False'
				BEGIN
					IF (SELECT COUNT(*) FROM AdditionalContactsInformation WHERE numDivisionID = @numDivisionID AND numContactId!=@numContactId AND ISNULL(bitPrimaryContact,0)=1) = 0
						SET @InlineEditValue='True'
				END 
				-- Added by Priya(to check no Campaign got engaged wothout contacts email
				ELSE IF @vcDbColumnName = 'numECampaignID' 
				BEGIN
					
					IF EXISTS (select 1 from AdditionalContactsInformation WHERE numContactId = @numContactID AND (vcEmail IS NULL OR vcEmail = ''))
					BEGIN
						RAISERROR('EMAIL_ADDRESS_REQUIRED',16,1)
								RETURN
					END

				END
				
		SET @strSql='update AdditionalContactsInformation set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numDivisionID=@numDivisionID and numDomainID=@numDomainID and numContactId=@numContactId'
	 END 
	 ELSE if @vcLookBackTableName = 'CompanyInfo' 
	 BEGIN 
		SET @strSql='update CompanyInfo set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numCompanyId=(select numCompanyId from divisionmaster where numDivisionID=@numDivisionID and numDomainID=@numDomainID) and numDomainID=@numDomainID'
	 END 
	 ELSE if @vcLookBackTableName = 'WorkOrder' 
	 BEGIN 
		DECLARE @numItemCode AS NUMERIC(9)
	 
		IF @numWODetailId>0
		BEGIN
			SET @strSql='update WorkOrderDetails set ' + @vcDbColumnName + '=@InlineEditValue where numWOId=@numWOId and numWODetailId=@numWODetailId'
			
			IF @vcDbColumnName='numQtyItemsReq'
			BEGIN 
				SELECT @numItemCode=numChildItemID FROM WorkOrderDetails WHERE numWOId=@numWOId and numWODetailId=@numWODetailId
				
				EXEC USP_UpdateWorkOrderDetailQuantity @numItemCode,@InlineEditValue,@numWOId
				--UPDATE WorkOrderDetails SET numQtyItemsReq=CONVERT(NUMERIC(9),numQtyItemsReq_Orig * CONVERT(NUMERIC(9,0),@InlineEditValue)) WHERE numWOId=@numWOId
			END 
		END 	
		ELSE
		BEGIN
			SET @strSql='update WorkOrder set ' + @vcDbColumnName + '=@InlineEditValue where numWOId=@numWOId and numDomainID=@numDomainID'
			
			IF @vcDbColumnName='numQtyItemsReq'
			BEGIN 
				SELECT @numItemCode=numItemCode FROM WorkOrder WHERE numWOId=@numWOId and numDomainID=@numDomainID
			
				EXEC USP_UpdateWorkOrderDetailQuantity @numItemCode,@InlineEditValue,@numWOId
				--UPDATE WorkOrderDetails SET numQtyItemsReq=CONVERT(NUMERIC(9),numQtyItemsReq_Orig * CONVERT(NUMERIC(9,0),@InlineEditValue)) WHERE numWOId=@numWOId
			END 
		END	
	 END
	 ELSE if @vcLookBackTableName = 'ProjectsMaster' 
	 BEGIN 
		IF @vcDbColumnName='numAssignedTo' ---Updating if Project is assigned to someone 
			BEGIN
				select @tempAssignedTo=isnull(numAssignedTo,0) from ProjectsMaster where numProId=@numProId and numDomainId= @numDomainID           
			
				if (@tempAssignedTo<>@InlineEditValue and  @InlineEditValue<>'0')          
					begin            
						update ProjectsMaster set numAssignedTo=@InlineEditValue ,numAssignedBy=@numUserCntID,bintModifiedDate=GETUTCDATE() where numProId = @numProId  and numDomainId= @numDomainID           
					end           
				else if  (@InlineEditValue ='0')          
					begin          
						update ProjectsMaster set numAssignedTo=0 ,numAssignedBy=0,bintModifiedDate=GETUTCDATE() where numProId = @numProId  and numDomainId= @numDomainID        
					end    
			END
		else IF @vcDbColumnName='numProjectStatus'  ---Updating All Statge to 100% if Completed
			BEGIN
				IF @InlineEditValue='27492'
				BEGIN
					UPDATE PM SET dtCompletionDate=GETUTCDATE(),monTotalExpense=PIE.monExpense,
					monTotalIncome=PIE.monIncome,monTotalGrossProfit=PIE.monBalance,numProjectStatus=27492
					FROM ProjectsMaster PM,dbo.GetProjectIncomeExpense(@numDomainId,@numProId) PIE
					WHERE PM.numProId=@numProId
					
					EXEC USP_ManageProjectCommission @numDomainId,@numProId
						
					Update StagePercentageDetails set tinProgressPercentage=100,bitclose=1 where numProjectID=@numProId

					EXEC dbo.USP_GetProjectTotalProgress
							@numDomainID = @numDomainID, --  numeric(9, 0)
							@numProId = @numProId, --  numeric(9, 0)
							@tintMode = 1 --  tinyint
				END			
				ELSE
				BEGIN
					DELETE from BizDocComission WHERE numDomainID = @numDomainID AND numProId=@numProId AND numComissionID 
						NOT IN (SELECT BC.numComissionID FROM dbo.BizDocComission BC JOIN dbo.PayrollTracking PT 
							ON BC.numComissionID = PT.numComissionID AND BC.numDomainId = PT.numDomainId WHERE BC.numDomainID = @numDomainID AND ISNULL(BC.numProId,0) = @numProId)
				END
			END
			

		SET @strSql='update ProjectsMaster set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numProId=@numProId and numDomainID=@numDomainID'
	 END 
	 ELSE if @vcLookBackTableName = 'OpportunityMaster' 
	 BEGIN 
		IF @vcDbColumnName='numAssignedTo' ---Updating if organization is assigned to someone  
			BEGIN
				select @tempAssignedTo=isnull(numAssignedTo,0) from OpportunityMaster where numOppId=@numOppId and numDomainId= @numDomainID           
			
				if (@tempAssignedTo<>@InlineEditValue and  @InlineEditValue<>'0')          
					begin            
						update OpportunityMaster set numAssignedTo=@InlineEditValue ,numAssignedBy=@numUserCntID,bintModifiedDate=GETUTCDATE() where numOppId = @numOppId  and numDomainId= @numDomainID           
					end           
				else if  (@InlineEditValue ='0')          
					begin          
						update OpportunityMaster set numAssignedTo=0 ,numAssignedBy=0,bintModifiedDate=GETUTCDATE() where numOppId = @numOppId  and numDomainId= @numDomainID        
					end    
			END
		ELSE IF @vcDbColumnName='numPartner' OR @vcDbColumnName='numPartenerSource'
		 BEGIN
			update OpportunityMaster set numPartner=@InlineEditValue ,bintModifiedDate=GETUTCDATE() where numOppId = @numOppId  and numDomainId= @numDomainID           
		 END
		
		ELSE IF @vcDbColumnName='tintActive'
		 BEGIN
		 	SET @InlineEditValue= CASE @InlineEditValue WHEN 'True' THEN 1 ELSE 0 END 
		 END
		 ELSE IF @vcDbColumnName='tintSource'
		 BEGIN
			IF CHARINDEX('~', @InlineEditValue)>0
			BEGIN
				SET @Value1=SUBSTRING(@InlineEditValue, 1,CHARINDEX('~', @InlineEditValue)-1) 
				SET @Value2=SUBSTRING(@InlineEditValue, CHARINDEX('~', @InlineEditValue)+1,LEN(@InlineEditValue)) 
			END	
			ELSE
			BEGIN
				SET @Value1=@InlineEditValue
				SET @Value2=0
			END
			
		 	update OpportunityMaster set tintSourceType=@Value2 where numOppId = @numOppId  and numDomainId= @numDomainID        
		 	
		 	SET @InlineEditValue= @Value1
		 END
		ELSE IF @vcDbColumnName='tintOppStatus' --Update Inventory  0=Open,1=Won,2=Lost
		 BEGIN			
			DECLARE @bitViolatePrice BIT
			DECLARE @intPendingApprovePending INT
		
			IF EXISTS (SELECT numApprovalRuleID FROM ApprovalProcessItemsClassification WHERE numDomainId=@numDomainID AND ISNULL(bitMarginPriceViolated,0) = 1)
			BEGIN
				SET @bitViolatePrice = 1
			END
			SET @intPendingApprovePending=(SELECT COUNT(*) FROM OpportunityItems WHERE numOppId=@numOppId AND ISNULL(bitItemPriceApprovalRequired,0)=1)

			IF(@intPendingApprovePending > 0 AND @bitViolatePrice=1 AND @InlineEditValue='1')
			BEGIN
				SET @intExecuteDiv=1
				SET @InlineEditValue='-#123'
			END

			IF ISNULL(@intExecuteDiv,0)=0
			BEGIN
				DECLARE @tintOppStatus AS TINYINT
				SELECT @tintOppStatus=tintOppStatus FROM OpportunityMaster WHERE numOppID=@numOppID              

				IF @tintOppStatus=0 AND @InlineEditValue='1' --Open to Won
				BEGIN
					SELECT @numDivisionID=numDivisionID FROM OpportunityMaster WHERE numOppID=@numOppID   
					
					DECLARE @tintCRMType AS TINYINT      
					SELECT @tintCRMType=tintCRMType FROM divisionmaster WHERE numDivisionID =@numDivisionID 

					-- Promote Lead to Account when Sales/Purchase Order is created against it
					IF @tintCRMType=0 --Lead & Order
					BEGIN        
						UPDATE 
							divisionmaster 
						SET 
							tintCRMType=2
							,bintLeadProm=GETUTCDATE()
							,bintLeadPromBy=@numUserCntID
							,bintProsProm=GETUTCDATE()
							,bintProsPromBy=@numUserCntID        
						WHERE 
							numDivisionID=@numDivisionID        
					END
					--Promote Prospect to Account
					ELSE IF @tintCRMType=1 
					BEGIN        
						UPDATE 
							divisionmaster 
						SET 
							tintCRMType=2
							,bintProsProm=GETUTCDATE()
							,bintProsPromBy=@numUserCntID        
						WHERE 
							numDivisionID=@numDivisionID        
					END    

		 			EXEC USP_UpdatingInventoryonCloseDeal @numOppID,@numUserCntID

					UPDATE OpportunityMaster SET bintOppToOrder=GETUTCDATE() WHERE numOppId=@numOppId and numDomainID=@numDomainID
				END
				ELSE IF (@InlineEditValue='2') -- Win to Lost
				BEGIN
					UPDATE OpportunityMaster SET dtDealLost=GETUTCDATE() WHERE numOppId=@numOppId and numDomainID=@numDomainID
				END
				
				IF (@tintOppStatus=1 and @InlineEditValue='2') or (@tintOppStatus=1 and @InlineEditValue='0') --Won to Lost or Won to Open
						EXEC USP_RevertDetailsOpp @numOppID,0,@numUserCntID 

				UPDATE OpportunityMaster SET tintOppStatus = @InlineEditValue WHERE numOppId=@numOppID
				
			END
		 END
		 ELSE IF @vcDbColumnName='vcOppRefOrderNo' --Update vcRefOrderNo OpportunityBizDocs
		 BEGIN
			 IF LEN(ISNULL(@InlineEditValue,''))>0
			 BEGIN
	   			UPDATE dbo.OpportunityBizDocs SET vcRefOrderNo=@InlineEditValue WHERE numOppId=@numOppID
			 END
		 END
		 ELSE IF @vcDbColumnName='numStatus' 
		 BEGIN
			DECLARE @tintOppType AS TINYINT
			select @tintOppStatus=tintOppStatus,@tintOppType=tintOppType from OpportunityMaster where numOppID=@numOppID

			IF @tintOppType=1 AND @tintOppStatus=1
			BEGIN
				DECLARE @numOppStatus AS NUMERIC(18,0) = CAST(@InlineEditValue AS NUMERIC(18,0))
				EXEC USP_SalesFulfillmentQueue_Save @numDomainID,@numUserCntID,@numOppID,@numOppStatus
			END

		 	 IF EXISTS (SELECT 1 FROM dbo.OpportunityMaster WHERE numOppId = @numOppID AND tintOppType=1 AND isnull(tintOppStatus,0)=1 AND ISNULL(numStatus,0)!= @InlineEditValue)
			 BEGIN
	   				EXEC USP_ManageOpportunityAutomationQueue
							@numOppQueueID = 0, -- numeric(18, 0)
							@numDomainID = @numDomainID, -- numeric(18, 0)
							@numOppId = @numOppID, -- numeric(18, 0)
							@numOppBizDocsId = 0, -- numeric(18, 0)
							@numOrderStatus = @InlineEditValue, -- numeric(18, 0)
							@numUserCntID = @numUserCntID, -- numeric(18, 0)
							@tintProcessStatus = 1, -- tinyint
							@tintMode = 1 -- TINYINT
			END 
		 END	
		 ELSE IF @vcDbColumnName = 'dtReleaseDate'
		 BEGIN
			IF EXISTS (SELECT 1 FROM OpportunityMaster WHERE numDomainId=@numDomainID AND numOppId=@numOppId AND tintOppType=1 AND tintOppStatus=0)
			BEGIN
				IF (SELECT dbo.GetListIemName(ISNULL(numPercentageComplete,0)) FROM OpportunityMaster WHERE numDomainId=@numDomainID AND numOppId=@numOppId) = '100'
				BEGIN
					SET @strSql='update OpportunityMaster set ' + @vcDbColumnName + '=DATEADD(hh, DATEDIFF(hh, GETDATE(), GETUTCDATE()), @InlineEditValue),numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numOppId=@numOppId and numDomainID=@numDomainID'
				END
				ELSE
				BEGIN
					RAISERROR('PERCENT_COMPLETE_MUST_BE_100',16,1)
					RETURN
				END
			END
			ELSE
			BEGIN
				SET @strSql='update OpportunityMaster set ' + @vcDbColumnName + '=DATEADD(hh, DATEDIFF(hh, GETDATE(), GETUTCDATE()), @InlineEditValue),numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numOppId=@numOppId and numDomainID=@numDomainID'
			END
		END
		ELSE IF @vcDbColumnName = 'numPercentageComplete'
		BEGIN
			IF EXISTS (SELECT 1 FROM OpportunityMaster WHERE numDomainId=@numDomainID AND numOppId=@numOppId AND tintOppType=1 AND tintOppStatus=0 AND dbo.GetListIemName(ISNULL(numPercentageComplete,0))='100')
			BEGIN
				IF EXISTS(SELECT dtReleaseDate FROM OpportunityMaster WHERE numDomainId=@numDomainID AND numOppId=@numOppId AND dtReleaseDate IS NULL)
				BEGIN
					SET @strSql='update OpportunityMaster set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numOppId=@numOppId and numDomainID=@numDomainID'
				END
				ELSE
				BEGIN
					RAISERROR('REMOVE_RELEASE_DATE',16,1)
					RETURN
				END
			END
			ELSE
			BEGIN
				SET @strSql='update OpportunityMaster set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numOppId=@numOppId and numDomainID=@numDomainID'
			END
		END

		IF(@vcDbColumnName!='tintOppStatus')
		BEGIN
			SET @strSql='update OpportunityMaster set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numOppId=@numOppId and numDomainID=@numDomainID'
		END
	 END
	 ELSE if @vcLookBackTableName = 'Cases' 
	 BEGIN 
	 		IF @vcDbColumnName='numAssignedTo' ---Updating if Case is assigned to someone  
			BEGIN
				select @tempAssignedTo=isnull(numAssignedTo,0) from Cases where numCaseId=@numCaseId and numDomainId= @numDomainID           
			
				if (@tempAssignedTo<>@InlineEditValue and  @InlineEditValue<>'0')          
					begin            
						update Cases set numAssignedTo=@InlineEditValue ,numAssignedBy=@numUserCntID,bintModifiedDate=GETUTCDATE() where numCaseId = @numCaseId  and numDomainId= @numDomainID           
					end           
				else if  (@InlineEditValue ='0')          
					begin          
						update Cases set numAssignedTo=0 ,numAssignedBy=0,bintModifiedDate=GETUTCDATE() where numCaseId = @numCaseId  and numDomainId= @numDomainID        
					end    
			END
		UPDATE Cases SET bintModifiedDate=GETUTCDATE() WHERE numCaseId = @numCaseId  and numDomainId= @numDomainID    
		SET @strSql='update Cases set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numCaseId=@numCaseId and numDomainID=@numDomainID'
	 END 
	 ELSE if @vcLookBackTableName = 'Tickler' 
	 BEGIN 	
		SET @strSql='update Communication set ' + @vcOrigDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,dtModifiedDate=getutcdate() where numCommId=@numCommId and numDomainID=@numDomainID'
	 END
	 ELSE IF @vcLookBackTableName = 'ExtarnetAccounts'
	 BEGIN
		IF @vcDbColumnName='bitEcommerceAccess'
		BEGIN
			IF LOWER(@InlineEditValue) = 'yes' OR LOWER(@InlineEditValue) = 'true'
			BEGIN
				IF NOT EXISTS (SELECT * FROM ExtarnetAccounts where numDivisionID=@numDivisionID) 
				BEGIN
					DECLARE @numGroupID NUMERIC
					DECLARE @numCompanyID NUMERIC(18,0)
					SELECT @numCompanyID=numCompanyID FROM DivisionMaster WHERE numDivisionID=@numDivisionID

					SELECT TOP 1 @numGroupID=numGroupID FROM dbo.AuthenticationGroupMaster WHERE numDomainID=@numDomainID AND tintGroupType=2
				
					IF @numGroupID IS NULL SET @numGroupID = 0 
					INSERT INTO ExtarnetAccounts (numCompanyID,numDivisionID,numGroupID,numDomainID)                                                
					VALUES(@numCompanyID,@numDivisionID,@numGroupID,@numDomainID)
				END
			END
			ELSE
			BEGIN
				DELETE FROM ExtranetAccountsDtl WHERE numExtranetID IN (SELECT ISNULL(numExtranetID,0) FROM ExtarnetAccounts where numDivisionID=@numDivisionID)
				DELETE FROM ExtarnetAccounts where numDivisionID=@numDivisionID
			END

			SELECT (CASE WHEN LOWER(@InlineEditValue) = 'yes' OR LOWER(@InlineEditValue) = 'true' THEN 'Yes' ELSE 'No' END) AS vcData

			RETURN
		END
	 END
	ELSE IF @vcLookBackTableName = 'ExtranetAccountsDtl'
	BEGIN
		IF @vcDbColumnName='vcPassword'
		BEGIN
			IF LEN(@InlineEditValue) = 0
			BEGIN
				DELETE FROM ExtranetAccountsDtl WHERE numContactID=@numContactID
			END
			ELSE
			BEGIN
				IF EXISTS (SELECT numExtranetID FROM ExtarnetAccounts WHERE numDivisionID = @numDivisionID)
				BEGIN
					IF EXISTS (SELECT numExtranetDtlID FROM ExtranetAccountsDtl WHERE numContactID=@numContactID)
					BEGIN
						UPDATE
							ExtranetAccountsDtl
						SET 
							vcPassword=@InlineEditValue
							,tintAccessAllowed=1
						WHERE
							numContactID=@numContactID
					END
					ELSE
					BEGIN
						DECLARE @numExtranetID NUMERIC(18,0)
						SELECT @numExtranetID=numExtranetID FROM ExtarnetAccounts WHERE numDivisionID = @numDivisionID

						INSERT INTO ExtranetAccountsDtl (numExtranetID,numContactID,vcPassword,tintAccessAllowed,numDomainID)
						VALUES (@numExtranetID,@numContactID,@InlineEditValue,1,@numDomainID)
					END
				END
				ELSE
				BEGIN
					RAISERROR('Organization e-commerce access is not enabled',16,1)
				END
			END

			SELECT @InlineEditValue AS vcData

			RETURN
		END
	END
	ELSE IF @vcLookBackTableName = 'CustomerPartNumber'
	BEGIN
		IF @vcDbColumnName='CustomerPartNo'
		BEGIN
			SELECT @numCompanyID=numCompanyID FROM DivisionMaster WHERE numDivisionID=@numDivisionID
			
			IF EXISTS(SELECT CustomerPartNoID FROM CustomerPartNumber WHERE numItemCode = @numRecId AND numCompanyID = @numCompanyID)
			BEGIN
				UPDATE CustomerPartNumber SET CustomerPartNo = @InlineEditValue WHERE numItemCode = @numRecId AND numCompanyId = @numCompanyID AND numDomainID = @numDomainId 
			END
			ELSE
			BEGIN
				INSERT INTO CustomerPartNumber
				(numItemCode, numCompanyId, numDomainID, CustomerPartNo)
				VALUES
				(@numRecId, @numCompanyID, @numDomainID, @InlineEditValue)
			END

			SELECT @InlineEditValue AS vcData

			RETURN
		END
		
	END
	--ELSE IF @vcLookBackTableName = 'Item'
	--BEGIN
	--	IF @vcDbColumnName = 'monListPrice'
	--	BEGIN
	--		IF EXISTS (SELECT numItemCode FROM Item WHERE numDomainID=@numDomainID AND numItemCode=@numRecId AND 1 = (CASE WHEN ISNULL(numItemGroup,0) > 0 THEN (CASE WHEN ISNULL(bitMatrix,0)=1 THEN 1 ELSE 0 END) ELSE 1 END))
	--		BEGIN
	--			UPDATE WareHouseItems SET monWListPrice=@InlineEditValue WHERE numDomainID=@numDomainID AND numItemID=@numRecId
	--			UPDATE Item SET monListPrice=@InlineEditValue WHERE numDomainID=@numDomainID AND numItemCode=@numRecId

	--			SELECT @InlineEditValue AS vcData
	--		END
	--		ELSE
	--		BEGIN
	--			RAISERROR('You are not allowed to edit list price for warehouse level matrix item.',16,1)
	--		END

	--		RETURN
	--	END

	--	SET @strSql='UPDATE Item set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numItemCode=@numRecId AND numDomainID=@numDomainID'
	--END

	EXECUTE sp_executeSQL @strSql, N'@numDomainID NUMERIC(9),@numUserCntID NUMERIC(9),@numDivisionID NUMERIC(9),@numContactID NUMERIC(9),@InlineEditValue VARCHAR(8000),@numWOId NUMERIC(9),@numProId NUMERIC(9),@numOppId NUMERIC(9),@numCaseId NUMERIC(9),@numWODetailId NUMERIC(9),@numCommID Numeric(9),@numRecId NUMERIC(18,0)',
		     @numDomainID,@numUserCntID,@numDivisionID,@numContactID,@InlineEditValue,@numWOId,@numProId,@numOppId,@numCaseId,@numWODetailId,@numCommID,@numRecId;
		     
	IF @vcAssociatedControlType='SelectBox'                                                    
       BEGIN       
			IF @vcDbColumnName='tintSource' AND @vcLookBackTableName = 'OpportunityMaster'
			BEGIN
				SELECT dbo.fn_GetOpportunitySourceValue(@Value1,@Value2,@numDomainID) AS vcData                                             
			END
			ELSE IF @vcDbColumnName='numContactID'
			BEGIN
				SELECT dbo.fn_GetContactName(@InlineEditValue)
			END
			ELSE IF @vcDbColumnName='numPartner' OR @vcDbColumnName='numPartenerSource'
			BEGIN
				SET @intExecuteDiv=1
				SELECT D.vcPartnerCode+'-'+C.vcCompanyName  AS vcData FROM DivisionMaster AS D 
				LEFT JOIN CompanyInfo AS C
				ON D.numCompanyID=C.numCompanyID WHERE D.numDivisionID=@InlineEditValue
			END
			
			ELSE IF @vcDbColumnName='tintOppStatus' AND @intExecuteDiv=1
			BEGIN
				SELECT @InlineEditValue AS vcData
			END
			ELSE IF @vcDbColumnName='numReleaseStatus'
			BEGIN
				If @InlineEditValue = '1'
				BEGIN
					SELECT 'Open'
				END
				ELSE IF @InlineEditValue = '2'
				BEGIN
					SELECT 'Purchased'
				END
				ELSE
				BEGIN
					SELECT ''
				END
			END
			ELSE IF @vcDbColumnName='numPartenerContact'
			BEGIN
				SET @InlineEditValue=(SELECT TOP 1 A.vcGivenName FROM AdditionalContactsInformation AS A 
										LEFT JOIN DivisionMaster AS D 
										ON D.numDivisionID=A.numDivisionId
										WHERE D.numDomainID=@numDomainID AND A.numCOntactId=@InlineEditValue )
				SELECT @InlineEditValue AS vcData
			END
			ELSE IF @vcDbColumnName='numPartenerSource'
			BEGIN
				
				SET @InlineEditValue=(SELECT D.vcPartnerCode+'-'+C.vcCompanyName  AS vcData FROM DivisionMaster AS D 
				LEFT JOIN CompanyInfo AS C
				ON D.numCompanyID=C.numCompanyID WHERE D.numDivisionID=@InlineEditValue)
				PRINT 'MU'
				SELECT @InlineEditValue AS vcData
			END
			ELSE IF @vcDbColumnName='numShippingService'
			BEGIN
				SELECT ISNULL((SELECT vcShipmentService FROM ShippingService WHERE (numDomainID=@numDomainID OR ISNULL(numDomainID,0)=0) AND numShippingServiceID = @InlineEditValue),'') AS vcData
			END
			else
			BEGIN
				SELECT dbo.fn_getSelectBoxValue(@vcListItemType,@InlineEditValue,@vcDbColumnName) AS vcData                                             
			END
		end 
		ELSE
		BEGIN
			 IF @vcDbColumnName='tintActive'
		  	 BEGIN
			 	SET @InlineEditValue= CASE @InlineEditValue WHEN 1 THEN 'True' ELSE 'False' END 
			 END

			IF @vcAssociatedControlType = 'Check box' or @vcAssociatedControlType = 'Checkbox'
			BEGIN
	 			SET @InlineEditValue= Case @InlineEditValue WHEN 'True' THEN 'Yes' ELSE 'No' END
			END
			
		    SELECT @InlineEditValue AS vcData
		END  
		
	IF @vcLookBackTableName = 'AdditionalContactsInformation' AND @vcDbColumnName = 'numECampaignID'
	BEGIN
		--Manage ECampiagn When Campaign is changed from inline edit
		DECLARE @Date AS DATETIME 
		SELECT @Date = dbo.[GetUTCDateWithoutTime]()
		DECLARE @numECampaignID AS NUMERIC(18,0)
		SET @numECampaignID = @InlineEditValue

		IF @numECampaignID = -1 
		SET @numECampaignID=0
	 
 		EXEC [USP_ManageConEmailCampaign]
		@numConEmailCampID = 0, --  numeric(9, 0)
		@numContactID = @numContactID, --  numeric(9, 0)
		@numECampaignID = @numECampaignID, --  numeric(9, 0)
		@intStartDate = @Date, --  datetime
		@bitEngaged = 1, --  bit
		@numUserCntID = @numUserCntID --  numeric(9, 0)
	END  
END
ELSE
BEGIN	 

	SET @InlineEditValue=Cast(@InlineEditValue as varchar(1000))
	SELECT @vcAssociatedControlType=Fld_type
	 FROM CFW_Fld_Master WHERE Fld_Id=@numFormFieldId 

	IF @vcAssociatedControlType = 'CheckBox'
	BEGIN
	    SET @InlineEditValue=(case when @InlineEditValue='False' then 0 ELSE 1 END)
	END

	IF @vcPageName='organization'
	BEGIN 
		IF (SELECT COUNT(*) FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)>0
			BEGIN
				UPDATE CFW_FLD_Values SET Fld_Value=@InlineEditValue WHERE FldDTLID=(SELECT CFW.FldDTLID FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)
			END	
		ELSE
			BEGIN
				insert into CFW_FLD_Values (Fld_ID,Fld_Value,RecId) VALUES (@numFormFieldId,@InlineEditValue,@numRecId)         
			END

			--Added By :Sachin Sadhu ||Date:13thOct2014
			--Purpose  :To manage CustomFields in Queue
			EXEC USP_CFW_FLD_Values_CT
					@numDomainID =@numDomainID,
					@numUserCntID =@numUserCntID,
					@numRecordID =@numRecId ,
					@numFormFieldId  =@numFormFieldId
  
	  END
	ELSE IF @vcPageName='contact'
	BEGIN 
		IF (SELECT COUNT(*) FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Cont CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)>0
			BEGIN
				UPDATE CFW_FLD_Values_Cont SET Fld_Value=@InlineEditValue WHERE FldDTLID=(SELECT CFW.FldDTLID FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Cont CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)
			END	
		ELSE
			BEGIN
				insert into CFW_FLD_Values_Cont (Fld_ID,Fld_Value,RecId) VALUES (@numFormFieldId,@InlineEditValue,@numRecId)         
			END
			
			--Added By :Sachin Sadhu ||Date:13thOct2014
			--Purpose  :To manage CustomFields in Queue
			EXEC USP_CFW_FLD_Values_Cont_CT
					@numDomainID =@numDomainID,
					@numUserCntID =@numUserCntID,
					@numRecordID =@numRecId ,
					@numFormFieldId  =@numFormFieldId

	END
	ELSE IF @vcPageName='project'
	BEGIN 
		IF (SELECT COUNT(*) FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Pro CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)>0
			BEGIN
				UPDATE CFW_FLD_Values_Pro SET Fld_Value=@InlineEditValue WHERE FldDTLID=(SELECT CFW.FldDTLID FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Pro CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)
			END	
		ELSE
			BEGIN
				insert into CFW_FLD_Values_Pro (Fld_ID,Fld_Value,RecId) VALUES (@numFormFieldId,@InlineEditValue,@numRecId)         
			END

			
			--Added By :Sachin Sadhu ||Date:14thOct2014
			--Purpose  :To manage CustomFields in Queue
			EXEC USP_CFW_FLD_Values_Pro_CT
					@numDomainID =@numDomainID,
					@numUserCntID =@numUserCntID,
					@numRecordID =@numRecId ,
					@numFormFieldId  =@numFormFieldId
	END
	ELSE IF @vcPageName='opp'
	BEGIN 
		DECLARE @dtDate AS DATETIME = GETUTCDATE()

		IF (SELECT COUNT(*) FROM CFW_Fld_Master CFM JOIN CFW_Fld_Values_Opp CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)>0
		BEGIN
			UPDATE 
				CFW_Fld_Values_Opp 
			SET 
				Fld_Value=@InlineEditValue ,
				numModifiedBy = @numUserCntID,
				bintModifiedDate = @dtDate
			WHERE 
				FldDTLID= (SELECT CFW.FldDTLID FROM CFW_Fld_Master CFM JOIN CFW_Fld_Values_Opp CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)
		END	
		ELSE
		BEGIN
			INSERT INTO CFW_Fld_Values_Opp (Fld_ID,Fld_Value,RecId,numModifiedBy,bintModifiedDate) VALUES (@numFormFieldId,@InlineEditValue,@numRecId,@numUserCntID,@dtDate)         
		END

		UPDATE OpportunityMaster SET numModifiedBy=@numUserCntID,bintModifiedDate=@dtDate WHERE numOppID = @numRecId 

		--Added By :Sachin Sadhu ||Date:14thOct2014
		--Purpose  :To manage CustomFields in Queue
		EXEC USP_CFW_Fld_Values_Opp_CT
				@numDomainID =@numDomainID,
				@numUserCntID =@numUserCntID,
				@numRecordID =@numRecId ,
				@numFormFieldId  =@numFormFieldId

	END
	ELSE IF @vcPageName='case'
	BEGIN 
		IF (SELECT COUNT(*) FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Case CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)>0
			BEGIN
				UPDATE CFW_FLD_Values_Case SET Fld_Value=@InlineEditValue WHERE FldDTLID=(SELECT CFW.FldDTLID FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Case CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)
			END	
		ELSE
			BEGIN
				insert into CFW_FLD_Values_Case (Fld_ID,Fld_Value,RecId) VALUES (@numFormFieldId,@InlineEditValue,@numRecId)         
			END

			
			--Added By :Sachin Sadhu ||Date:14thOct2014
			--Purpose  :To manage CustomFields in Queue
			EXEC USP_CFW_FLD_Values_Case_CT
					@numDomainID =@numDomainID,
					@numUserCntID =@numUserCntID,
					@numRecordID =@numRecId ,
					@numFormFieldId  =@numFormFieldId
	END
	ELSE IF @vcPageName='item'
	BEGIN
		IF (SELECT COUNT(*) FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Item CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)>0
		BEGIN
			UPDATE CFW_FLD_Values_Item SET Fld_Value=@InlineEditValue WHERE FldDTLID=(SELECT CFW.FldDTLID FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Item CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)
		END	
		ELSE
		BEGIN
			INSERT INTO CFW_FLD_Values_Item (Fld_ID,Fld_Value,RecId) VALUES (@numFormFieldId,@InlineEditValue,@numRecId)         
		END
	END


	IF @vcAssociatedControlType = 'SelectBox'            
	BEGIN            
		    
		 SELECT vcData FROM ListDetails WHERE numListItemID=@InlineEditValue            
	END
	ELSE IF @vcAssociatedControlType = 'CheckBoxList'
	BEGIN
		DECLARE @str AS VARCHAR(MAX)
		SET @str = 'SELECT STUFF((SELECT CONCAT('','', vcData) FROM ListDetails WHERE numlistitemid IN (' + (CASE WHEN LEN(@InlineEditValue) > 0 THEN @InlineEditValue ELSE '0' END) + ') FOR XML PATH('''')), 1, 1, '''')'
		EXEC(@str)
	END
	ELSE IF @vcAssociatedControlType = 'CheckBox'
	BEGIN
	 	select CASE @InlineEditValue WHEN 1 THEN 'Yes' ELSE 'No' END AS vcData
	END
	ELSE
	BEGIN
		SELECT @InlineEditValue AS vcData
	END 
END

/****** Object:  StoredProcedure [dbo].[USP_ManageDivisions]    Script Date: 07/26/2008 16:19:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Modified by anoop jayaraj                                  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_managedivisions')
DROP PROCEDURE usp_managedivisions
GO
CREATE PROCEDURE [dbo].[USP_ManageDivisions]                                                        
 @numDivisionID  numeric=0,                                                         
 @numCompanyID  numeric=0,                                                         
 @vcDivisionName  varchar (100)='',                                                        
 @vcBillStreet  varchar (50)='',                                                        
 @vcBillCity   varchar (50)='',                                                        
 @vcBillState  NUMERIC=0,                                                        
 @vcBillPostCode  varchar (15)='',                                                        
 @vcBillCountry  numeric=0,                                                        
 @bitSameAddr  bit=0,                                                        
 @vcShipStreet  varchar (50)='',                                                        
 @vcShipCity   varchar (50)='',                                                        
 @vcShipState  varchar (50)='',                                                        
 @vcShipPostCode  varchar (15)='',                                                        
 @vcShipCountry  varchar (50)='',                                                        
 @numGrpId   numeric=0,                                                                                          
 @numTerID   numeric=0,                                                        
 @bitPublicFlag  bit=0,                                                        
 @tintCRMType  tinyint=0,                                                        
 @numUserCntID  numeric=0,                                                                                                                                                              
 @numDomainID  numeric=0,                                                        
 @bitLeadBoxFlg  bit=0,  --Added this because when called form leadbox this value will be 1 else 0                                                        
 @numStatusID  numeric=0,                                                      
 @numCampaignID numeric=0,                                          
 @numFollowUpStatus numeric(9)=0,                                                                                  
 @tintBillingTerms as tinyint,                                                        
 @numBillingDays as numeric(18,0),                                                       
 @tintInterestType as tinyint,                                                        
 @fltInterest as float,                                        
 @vcComFax as varchar(50)=0,                          
 @vcComPhone as varchar(50)=0,                
 @numAssignedTo as numeric(9)=0,    
 @bitNoTax as bit,
 @UpdateDefaultTax as bit=1,
@numCompanyDiff as numeric(9)=0,
@vcCompanyDiff as varchar(100)='',
@numCurrencyID	AS NUMERIC(9,0),
@numAccountClass AS NUMERIC(18,0) = 0,
@numBillAddressID AS NUMERIC(18,0) = 0,
@numShipAddressID AS NUMERIC(18,0) = 0,
@vcPartenerSource AS VARCHAR(300)=0,              
@vcPartenerContact AS VARCHAR(300)=0,
@numPartner AS NUMERIC(18,0) = 0
AS   
BEGIN
	DECLARE @numPartenerSource NUMERIC(18,0)      
	DECLARE @numPartenerContact NUMERIC(18,0)     

	IF ISNULL(@numPartner,0) = 0
	BEGIN
		SET @numPartenerSource=(SELECT TOP 1 numDivisionID FROM DivisionMaster WHERE numDomainId=@numDomainID AND vcPartnerCode=@vcPartenerSource)
	END
	ELSE 
	BEGIN
		SET @numPartenerSource = @numPartner
	END

	SET @numPartenerContact=(SELECT TOP 1 numContactId FROM AdditionalContactsInformation WHERE numDomainId=@numDomainID AND numDivisionId=@numPartenerSource AND vcEmail=@vcPartenerContact)
                                 
	IF ISNULL(@numGrpId,0) = 0 AND ISNULL(@tintCRMType,0) = 0
	BEGIN
 		SET @numGrpId = 0
 		SET @tintCRMType = 1
	END
                                                                                                               
	IF @numDivisionId is null OR @numDivisionId=0                                                 
	BEGIN
		IF @UpdateDefaultTax=1 
			SET @bitNoTax=0
                                                                                  
		INSERT INTO DivisionMaster                                    
		(                      
			numCompanyID,vcDivisionName,numGrpId,numFollowUpStatus,bitPublicFlag,numCreatedBy,bintCreatedDate,numModifiedBy,bintModifiedDate,tintCRMType,numDomainID,                                    
			bitLeadBoxFlg,numTerID,numStatusID,numRecOwner,tintBillingTerms,numBillingDays,tintInterestType,fltInterest,numCampaignID,vcComPhone,vcComFax,bitNoTax,
			numCompanyDiff,vcCompanyDiff,bitActiveInActive,numCurrencyID,numAccountClassID,numPartenerSource,numPartenerContact        
		)                                    
		VALUES                      
		(                      
			@numCompanyID,@vcDivisionName,@numGrpId,@numFollowUpStatus,@bitPublicFlag,@numUserCntID,GETUTCDATE(),@numUserCntID,GETUTCDATE(),@tintCRMType,@numDomainID,                       
			@bitLeadBoxFlg,@numTerID,@numStatusID,@numUserCntID,(CASE WHEN ISNULL(@numBillingDays,0) > 0 THEN 1 ELSE 0 END),@numBillingDays,0,0,@numCampaignID,@vcComPhone,            
			@vcComFax,@bitNoTax,@numCompanyDiff,@vcCompanyDiff,1,@numCurrencyID,@numAccountClass,@numPartenerSource,@numPartenerContact                         
		)                                  
	
		SELECT @numDivisionID = SCOPE_IDENTITY()

		IF @numBillAddressID > 0 OR @numShipAddressID > 0
		BEGIN
			IF @numBillAddressID > 0
			BEGIN
				UPDATE AddressDetails SET numRecordID=@numDivisionID WHERE numAddressID=@numBillAddressID
			END

			IF @numShipAddressID > 0
			BEGIN
				UPDATE AddressDetails SET numRecordID=@numDivisionID WHERE numAddressID=@numShipAddressID
			END
		END
		ELSE
		BEGIN
		  --added By Sachin Sadhu||Issued Founded on:1stFeb14||By:JJ-AudioJenex
			IF EXISTS(SELECT 'col1' FROM dbo.AddressDetails WHERE  bitIsPrimary=1 AND numDomainID=@numDomainID AND numRecordID=@numDivisionID )
			BEGIN
				INSERT INTO dbo.AddressDetails 
				(
					vcAddressName,
					vcStreet,
					vcCity,
					vcPostalCode,
					numState,
					numCountry,
					bitIsPrimary,
					tintAddressOf,
					tintAddressType,
					numRecordID,
					numDomainID
				) 
				SELECT 'Address1',@vcBillStreet,@vcBillCity,@vcBillPostCode,@vcBillState,@vcBillCountry,0,2,1,@numDivisionID,@numDomainID
				union
				SELECT 'Address1',@vcShipStreet,@vcShipCity,@vcShipPostCode,@vcShipState,@vcShipCountry,0,2,2,@numDivisionID,@numDomainID
			END
			ELSE
			BEGIN
				INSERT INTO dbo.AddressDetails 
				(
					vcAddressName,
					vcStreet,
					vcCity,
					vcPostalCode,
					numState,
					numCountry,
					bitIsPrimary,
					tintAddressOf,
					tintAddressType,
					numRecordID,
					numDomainID
				) 
				SELECT 'Address1',@vcBillStreet,@vcBillCity,@vcBillPostCode,@vcBillState,@vcBillCountry,1,2,1,@numDivisionID,@numDomainID
				union
				SELECT 'Address1',@vcShipStreet,@vcShipCity,@vcShipPostCode,@vcShipState,@vcShipCountry,1,2,2,@numDivisionID,@numDomainID
			END
		END
       --End of Sachin Script                   

		IF @UpdateDefaultTax=1 
		BEGIN
			INSERT INTO DivisionTaxTypes
			SELECT 
				@numDivisionID,numTaxItemID,1 
			FROM 
				TaxItems
			WHERE 
				numDomainID=@numDomainID
			UNION
			SELECT 
				@numDivisionID,0,1 
		END                                                
		                  
		DECLARE @numGroupID NUMERIC
		SELECT TOP 1 
			@numGroupID=numGroupID 
		FROM 
			dbo.AuthenticationGroupMaster 
		WHERE 
			numDomainID=@numDomainID 
			AND tintGroupType=2
		
		IF @numGroupID IS NULL 
			SET @numGroupID = 0 
		
		INSERT INTO ExtarnetAccounts 
		(numCompanyID,numDivisionID,numGroupID,numDomainID)                                                
		VALUES
		(@numCompanyID,@numDivisionID,@numGroupID,@numDomainID)                                                                                                                 
	END                                                        
	ELSE if @numDivisionId>0                                                      
	BEGIN
		DECLARE @numFollow AS VARCHAR(10)                                        
		DECLARE @binAdded AS VARCHAR(20)                                        
		DECLARE @PreFollow AS VARCHAR(10)                                        
		DECLARE @RowCount AS VARCHAR(2)                                        
		SET @PreFollow=(SELECT COUNT(*) numFollowUpStatus FROM FollowUpHistory WHERE numDivisionID= @numDivisionID)                                        
    
		SET @RowCount=@@rowcount                                        
    
		SELECT 
			@numFollow=numFollowUpStatus,
			@binAdded=bintModifiedDate 
		FROM 
			divisionmaster
		WHERE 
			numDivisionID=@numDivisionID                                         
    
		IF @numFollow <>'0' AND @numFollow <> @numFollowUpStatus                                        
		BEGIN
			SELECT TOP 1 
				numFollowUpStatus 
			FROM 
				FollowUpHistory 
			WHERE 
				numDivisionID= @numDivisionID 
			ORDER BY 
				numFollowUpStatusID DESC                                        
                                         
			IF @PreFollow<>0                                        
			BEGIN
				IF @numFollow <> (select top 1  numFollowUpStatus from FollowUpHistory where numDivisionID= @numDivisionID order by numFollowUpStatusID desc)                                        
				BEGIN
					INSERT INTO FollowUpHistory
					(numFollowUpstatus,numDivisionID,bintAddedDate)                                        
					VALUES
					(@numFollow,@numDivisionID,@binAdded)                                        
				END                                        
			END                              
			ELSE
			BEGIN
				INSERT INTO FollowUpHistory
				(numFollowUpstatus,numDivisionID,bintAddedDate)                                        
				VALUES
				(@numFollow,@numDivisionID,@binAdded)                                   
			END                                                                 
		END                                                                                            
                                                        
		UPDATE 
			DivisionMaster                       
		SET                        
			numCompanyID = @numCompanyID ,                     
			vcDivisionName = @vcDivisionName,                                                        
			numGrpId = @numGrpId,                                                                       
			numFollowUpStatus =@numFollowUpStatus,                                                        
			numTerID = @numTerID,                                                   
			bitPublicFlag =@bitPublicFlag,                                                        
			numModifiedBy = @numUserCntID,                                                  
			bintModifiedDate = getutcdate(),                                                     
			tintCRMType = @tintCRMType,                                                      
			numStatusID = @numStatusID,                                                 
			tintBillingTerms = (CASE WHEN ISNULL(@numBillingDays,0) > 0 THEN 1 ELSE 0 END),                                                      
			numBillingDays = @numBillingDays,                                                     
			tintInterestType = @tintInterestType,                                                       
			fltInterest =@fltInterest,                          
			vcComPhone=@vcComPhone,                          
			vcComFax= @vcComFax,                      
			numCampaignID=@numCampaignID,    
			bitNoTax=@bitNoTax
			,numCompanyDiff=@numCompanyDiff
			,vcCompanyDiff=@vcCompanyDiff
			,numCurrencyID = @numCurrencyID
			,numPartenerSource=@numPartenerSource
			,numPartenerContact=@numPartenerContact                                                         
		WHERE 
			numDivisionID = @numDivisionID       

		UPDATE 
			dbo.AddressDetails 
		SET
			vcStreet=@vcBillStreet,                                    
			vcCity =@vcBillCity,                                    
			vcPostalCode=@vcBillPostCode,                                     
			numState=@vcBillState,                                    
			numCountry=@vcBillCountry
		WHERE 
			numDomainID=@numDomainID 
			AND numRecordID=@numDivisionID 
			AND bitIsPrimary=1 
			AND tintAddressOf=2 
			AND tintAddressType=1         
	   
		UPDATE 
			dbo.AddressDetails 
		SET
			vcStreet=@vcShipStreet,                                    
			vcCity =@vcShipCity,                                    
			vcPostalCode=@vcShipPostCode,                                     
			numState=@vcShipState,                                    
			numCountry=@vcShipCountry
		WHERE 
			numDomainID=@numDomainID 
			AND numRecordID=@numDivisionID 
			AND bitIsPrimary=1 
			AND tintAddressOf=2 
			AND tintAddressType=2
	END

    ---Updating if organization is assigned to someone                
	DECLARE @tempAssignedTo AS NUMERIC(9)              
              
	SELECT 
		@tempAssignedTo=isnull(numAssignedTo,0) 
	FROM 
		DivisionMaster 
	WHERE 
		numDivisionID = @numDivisionID                
        
	IF (@tempAssignedTo<>@numAssignedTo and  @numAssignedTo<>'0')              
	BEGIN                
		UPDATE 
			DivisionMaster 
		SET 
			numAssignedTo=@numAssignedTo
			,numAssignedBy=@numUserCntID 
		WHERE 
			numDivisionID = @numDivisionID                
	END               
  
	SELECT @numDivisionID
 END
---Created By Anoop Jayaraj

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageFieldRelationship')
DROP PROCEDURE USP_ManageFieldRelationship
GO
CREATE PROCEDURE USP_ManageFieldRelationship
@byteMode as tinyint,
@numFieldRelID as numeric(9),
@numModuleID NUMERIC(18,0),
@numPrimaryListID as numeric(9),
@numSecondaryListID as numeric(9),
@numDomainID as numeric(9),
@numFieldRelDTLID as numeric(9),
@numPrimaryListItemID as numeric(9),
@numSecondaryListItemID as numeric(9),
@bitTaskRelation AS BIT=0
as
BEGIN
if @byteMode =1
BEGIN
	IF(@bitTaskRelation=1)
	BEGIN
		IF NOT EXISTS(SELECT * FROM dbo.FieldRelationship WHERE numDomainID=@numDomainID AND numPrimaryListID=@numPrimaryListID)
		BEGIN
			insert into FieldRelationship (numPrimaryListID,numSecondaryListID,numDomainID,bitTaskRelation)
			values (@numPrimaryListID,@numSecondaryListID,@numDomainID,@bitTaskRelation)
			set @numFieldRelID=@@identity 
		END
		ELSE 
			set @numFieldRelID=0
	END
	ELSE
	BEGIN
	IF NOT EXISTS(SELECT * FROM dbo.FieldRelationship WHERE numDomainID=@numDomainID AND numPrimaryListID=@numPrimaryListID AND numSecondaryListID=@numSecondaryListID AND (numModuleID IS NULL OR numModuleID=@numModuleID))
	BEGIN
		insert into FieldRelationship (numPrimaryListID,numSecondaryListID,numDomainID,numModuleID,bitTaskRelation)
		values (@numPrimaryListID,@numSecondaryListID,@numDomainID,@numModuleID,@bitTaskRelation)
		set @numFieldRelID=@@identity 
	END
	ELSE
	BEGIN
		SET @numFieldRelID=0
	END
END
END
else if @byteMode =2
begin

	 if not exists(select * from FieldRelationshipDTL where numFieldRelID=@numFieldRelID 
	and numPrimaryListItemID=@numPrimaryListItemID and numSecondaryListItemID=@numSecondaryListItemID)
		insert into FieldRelationshipDTL (numFieldRelID,numPrimaryListItemID,numSecondaryListItemID)
		values (@numFieldRelID,@numPrimaryListItemID,@numSecondaryListItemID)
	else
	set @numFieldRelID=0 -- to identify that there is already one

END
	else if @byteMode =3
	begin
		Delete from FieldRelationshipDTL where numFieldRelDTLID=@numFieldRelDTLID
	end
	else if @byteMode =4
	begin
		Delete from FieldRelationshipDTL where numFieldRelID=@numFieldRelID
		Delete from FieldRelationship where numFieldRelID=@numFieldRelID
	end

	select @numFieldRelID
END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManagegeReportDashboard')
DROP PROCEDURE USP_ManagegeReportDashboard
GO
CREATE PROCEDURE [dbo].[USP_ManagegeReportDashboard]
@numDomainID AS NUMERIC(18,0),
@numDashBoardID AS NUMERIC(18,0),
@numReportID AS NUMERIC(18,0),
@numUserCntID AS NUMERIC(9)=0,
@tintReportType AS TINYINT,
@tintChartType AS TINYINT,
@vcHeaderText AS VARCHAR(100)='',
@vcFooterText AS VARCHAR(100)='',
@vcXAxis AS VARCHAR(50),
@vcYAxis AS VARCHAR(50),
@vcAggType AS VARCHAR(50),
@tintReportCategory AS TINYINT,
@numDashboardTemplateID NUMERIC(18,0),
@vcTimeLine VARCHAR(50)
,@vcGroupBy VARCHAR(50)
,@vcTeritorry VARCHAR(MAX)
,@vcFilterBy TINYINT
,@vcFilterValue VARCHAR(MAX)
,@dtFromDate DATE
,@dtToDate DATE
AS
BEGIN
	IF @numDashBoardID=0 
	BEGIN
		DECLARE @tintRow AS TINYINT;SET @tintRow=0
		SELECT @tintRow = ISNULL(MAX(tintRow),0) + 1 FROM ReportDashboard WHERE numDomainID=@numDomainID AND numUserCntID=@numUserCntID AND ISNULL(numDashboardTemplateID,0)=ISNULL(@numDashboardTemplateID,0)
		
		INSERT INTO ReportDashboard 
		(
			numDomainID
			,numReportID
			,numUserCntID
			,tintReportType
			,tintChartType
			,vcHeaderText
			,vcFooterText
			,tintRow
			,tintColumn
			,vcXAxis
			,vcYAxis
			,vcAggType
			,tintReportCategory
			,intHeight
			,intWidth
			,numDashboardTemplateID
			,bitNewAdded
			,vcTimeLine
			,vcGroupBy
			,vcTeritorry
			,vcFilterBy
			,vcFilterValue
			,dtFromDate
			,dtToDate
		)
		VALUES
		(
			@numDomainID
			,@numReportID
			,@numUserCntID
			,@tintReportType
			,@tintChartType
			,@vcHeaderText
			,@vcFooterText
			,@tintRow
			,1
			,@vcXAxis
			,@vcYAxis
			,@vcAggType
			,@tintReportCategory
			,7
			,8
			,@numDashboardTemplateID
			,1
			,@vcTimeLine
			,@vcGroupBy
			,@vcTeritorry
			,@vcFilterBy
			,@vcFilterValue
			,@dtFromDate
			,@dtToDate
		)
	END
	ELSE IF @numDashBoardID>0
	BEGIN
		UPDATE 
			ReportDashboard 
		SET
			numReportID=@numReportID, 
			tintReportType=@tintReportType, 
			tintChartType=@tintChartType, 
			vcHeaderText=@vcHeaderText, 
			vcFooterText=@vcFooterText,
			vcXAxis=@vcXAxis,
			vcYAxis=@vcYAxis,
			vcAggType=@vcAggType,
			tintReportCategory=@tintReportCategory
			,vcTimeLine=@vcTimeLine
			,vcGroupBy=@vcGroupBy
			,vcTeritorry=@vcTeritorry
			,vcFilterBy=@vcFilterBy
			,vcFilterValue=@vcFilterValue
			,dtFromDate=@dtFromDate
			,dtToDate=@dtToDate
		WHERE
			numDashBoardID=@numDashBoardID 
			AND numDomainID=@numDomainID
	END
END	
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ReportListMaster_RunPrebuildReport')
DROP PROCEDURE USP_ReportListMaster_RunPrebuildReport
GO
CREATE PROCEDURE [dbo].[USP_ReportListMaster_RunPrebuildReport]   
@numDomainID AS NUMERIC(18,0),               
@numUserCntID AS NUMERIC(18,0),
@ClientTimeZoneOffset AS INT,
@intDefaultReportID INT,
@numDashBoardID NUMERIC(18,0)
AS  
BEGIN
	DECLARE @vcTimeLine VARCHAR(50)
		,@vcGroupBy VARCHAR(50)
		,@vcTeritorry VARCHAR(MAX)
		,@vcFilterBy TINYINT
		,@vcFilterValue VARCHAR(MAX)
		,@dtFromDate DATE
		,@dtToDate DATE

	IF @intDefaultReportID = 1 --1 - A/R
	BEGIN
		EXEC USP_ReportListMaster_ARPreBuildReport @numDomainID
	END
	ELSE IF @intDefaultReportID = 2 -- A/P
	BEGIN
		EXEC USP_ReportListMaster_APPreBuildReport @numDomainID
	END
	ELSE IF @intDefaultReportID = 3 -- MONEY IN THE BANK
	BEGIN
		EXEC USP_ReportListMaster_BankPreBuildReport @numDomainID
	END
	ELSE IF @intDefaultReportID = 5 -- TOP 10 ITEMS BY PROFIT AMOUNT (LAST 12 MONTHS)
	BEGIN
		EXEC USP_ReportListMaster_Top10ItemByProfitPreBuildReport @numDomainID
	END
	ELSE IF @intDefaultReportID = 6 -- TOP 10 ITEMS BY REVENUE SOLD (LAST 12 MONTHS)
	BEGIN
		EXEC USP_ReportListMaster_Top10ItemByRevenuePreBuildReport @numDomainID
	END
	ELSE IF @intDefaultReportID = 7  -- PROFIT MARGIN BY ITEM CLASSIFICATION (LAST 12 MONTHS)
	BEGIN
		EXEC USP_ReportListMaster_ItemClassificationProfitPreBuildReport @numDomainID
	END
	ELSE IF @intDefaultReportID = 8  -- TOP 10 CUSTOMERS BY PROFIT MARGIN
	BEGIN
		EXEC USP_ReportListMaster_Top10CustomerByProfitPercentPreBuildReport @numDomainID
	END
	ELSE IF @intDefaultReportID = 9  -- TOP 10 CUSTOMERS BY PROFIT AMOUNT
	BEGIN
		EXEC USP_ReportListMaster_Top10CustomerByProfitAmountPreBuildReport @numDomainID
	END
	ELSE IF @intDefaultReportID = 10  -- ACTION ITEMS & MEETINGS DUE TODAY
	BEGIN
		EXEC USP_ReportListMaster_ActionItemPreBuildReport @numDomainID,0,@ClientTimeZoneOffset
	END
	ELSE IF @intDefaultReportID = 11  -- YTD VS SAME PERIOD LAST YEAR
	BEGIN
		EXEC USP_ReportListMaster_RPEYTDAndSamePeriodLastYear @numDomainID
	END
	ELSE IF @intDefaultReportID = 12  -- ACTION ITEMS & MEETINGS DUE TODAY
	BEGIN
		EXEC USP_ReportListMaster_RPELastMonthAndSamePeriodLastYear @numDomainID
	END
	ELSE IF @intDefaultReportID = 13  --  SALES VS EXPENSES (LAST 12 MONTHS)
	BEGIN
		EXEC USP_ReportListMaster_SalesVsExpenseLast12Months @numDomainID
	END
	ELSE IF @intDefaultReportID = 14  --  TOP SOURCES OF SALES ORDERS (LAST 12 MONTHS)
	BEGIN
		EXEC USP_ReportListMaster_TopSourceOfSalesOrder @numDomainID
	END
	ELSE IF @intDefaultReportID = 15  --  TOP 10 ITEMS BY PROFIT MARGIN
	BEGIN
		EXEC USP_ReportListMaster_Top10ItemByProfitMargin @numDomainID
	END
	ELSE IF @intDefaultReportID = 16  --  TOP 10 SALES OPPORTUNITIES BY REVENUE (LAST 12 MONTHS)
	BEGIN
		EXEC USP_ReportListMaster_Top10SalesOpportunityByRevenue @numDomainID
	END
	ELSE IF @intDefaultReportID = 17  --  TOP 10 SALES OPPORTUNITIES BY TOTAL PROGRESS
	BEGIN
		EXEC USP_ReportListMaster_Top10SalesOpportunityByTotalProgress @numDomainID
	END
	ELSE IF @intDefaultReportID = 18  --  TOP 10 ITEMS RETURNED VS QTY SOLD
	BEGIN
		EXEC USP_ReportListMaster_Top10ItemsByReturnedVsSold @numDomainID
	END
	ELSE IF @intDefaultReportID = 19  --  LARGEST 10 SALES OPPORTUNITIES PAST THEIR DUE DATE
	BEGIN
		EXEC USP_ReportListMaster_Top10SalesOpportunityByPastDue @numDomainID,@ClientTimeZoneOffset
	END
	ELSE IF @intDefaultReportID = 20  --  Top 10 Campaigns by ROI
	BEGIN
		EXEC USP_ReportListMaster_Top10CampaignsByROI @numDomainID
	END
	ELSE IF @intDefaultReportID = 21  --  MARKET FRAGMENTATION � TOP 10 CUSTOMERS, AS A PORTION TOTAL SALES
	BEGIN
		EXEC USP_ReportListMaster_TopCustomersByPortionOfTotalSales @numDomainID
	END
	ELSE IF @intDefaultReportID = 22  --  SCORE CARD
	BEGIN
		EXEC USP_ReportListMaster_PrebuildScoreCard @numDomainID
	END
	ELSE IF @intDefaultReportID = 23  --  TOP 10 LEAD SOURCES (NEW LEADS)
	BEGIN
		EXEC USP_ReportListMaster_Top10LeadSource @numDomainID
	END
	ELSE IF @intDefaultReportID = 24  --  LAST 10 EMAIL MESSAGES FROM PEOPLE I DO BUSINESS WITH
	BEGIN
		EXEC USP_ReportListMaster_Top10Email @numDomainID,@numUserCntID
	END
	ELSE IF @intDefaultReportID = 25  --  Reminders
	BEGIN
		EXEC USP_ReportListMaster_RemindersPreBuildReport @numDomainID,@ClientTimeZoneOffset
	END
	ELSE IF @intDefaultReportID = 26  --  TOP REASONS WHY WE�RE WINING DEALS
	BEGIN
		EXEC USP_ReportListMaster_Top10ReasonsDealWins @numDomainID
	END
	ELSE IF @intDefaultReportID = 27  --  TOP REASONS WHY WE�RE LOOSING DEALS
	BEGIN
		EXEC USP_ReportListMaster_Top10ReasonsDealLost @numDomainID
	END
	ELSE IF @intDefaultReportID = 28  --  TOP 10 REASONS FOR SALES RETURNS
	BEGIN
		EXEC USP_ReportListMaster_Top10ReasonsForSalesReturns @numDomainID
	END
	ELSE IF @intDefaultReportID = 29  --  EMPLOYEE SALES PERFORMANCE PANEL 1
	BEGIN
		EXEC USP_ReportListMaster_EmployeeSalesPerformancePanel1 @numDomainID
	END
	ELSE IF @intDefaultReportID = 30  --  PARTNER REVENUES, MARGINS & PROFITS (LAST 12 MONTHS)
	BEGIN
		EXEC USP_ReportListMaster_PartnerRMPLast12Months @numDomainID
	END
	ELSE IF @intDefaultReportID = 31  --  EMPLOYEE SALES PERFORMANCE PT 1 (LAST 12 MONTHS)
	BEGIN
		EXEC USP_ReportListMaster_EmployeeSalesPerformance1 @numDomainID
	END
	ELSE IF @intDefaultReportID = 32  --  EMPLOYEE BENEFIT TO COMPANY (LAST 12 MONTHS)
	BEGIN
		EXEC USP_ReportListMaster_EmployeeBenefitToCompany @numDomainID,@ClientTimeZoneOffset
	END
	ELSE IF @intDefaultReportID = 33  --  FIRST 10 SAVED SEARCHES
	BEGIN
		EXEC USP_ReportListMaster_First10SavedSearch @numDomainID
	END
	ELSE IF @intDefaultReportID = 34  --  SALES OPPORTUNITY WON/LOST REPORT
	BEGIN
		SELECT
			@vcTimeLine=vcTimeLine
			,@vcGroupBy=vcGroupBy
			,@vcTeritorry=vcTeritorry
			,@vcFilterBy=vcFilterBy
			,@vcFilterValue=vcFilterValue
			,@dtFromDate=dtFromDate
			,@dtToDate=dtToDate
		FROM
			ReportDashboard
		WHERE
			numDashBoardID = @numDashBoardID

		EXEC USP_ReportListMaster_SalesOppWonLostPreBuildReport @numDomainID,@ClientTimeZoneOffset,@vcTimeLine,@vcGroupBy,@vcTeritorry,@vcFilterBy,@vcFilterValue,@dtFromDate,@dtToDate
	END
	ELSE IF @intDefaultReportID = 35  --  SALES OPPORTUNITY PIPELINE
	BEGIN
		SELECT
			@vcTimeLine=vcTimeLine
			,@vcGroupBy=vcGroupBy
			,@vcTeritorry=vcTeritorry
			,@vcFilterBy=vcFilterBy
			,@vcFilterValue=vcFilterValue
			,@dtFromDate=dtFromDate
			,@dtToDate=dtToDate
		FROM
			ReportDashboard
		WHERE
			numDashBoardID = @numDashBoardID

		EXEC USP_ReportListMaster_SalesOppPipeLine @numDomainID,@ClientTimeZoneOffset,@vcTimeLine,@vcGroupBy,@vcTeritorry,@vcFilterBy,@vcFilterValue,@dtFromDate,@dtToDate
	END
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ReportListMaster_SalesOppPipeLine')
DROP PROCEDURE USP_ReportListMaster_SalesOppPipeLine
GO
CREATE PROCEDURE [dbo].[USP_ReportListMaster_SalesOppPipeLine]
	@numDomainID NUMERIC(18,0)
	,@ClientTimeZoneOffset INT
	,@vcTimeLine VARCHAR(50)
	,@vcGroupBy VARCHAR(50)
	,@vcTeritorry VARCHAR(MAX)
	,@vcFilterBy TINYINT  -- 1:Assign TO, 2: Record Owner, 3: Teams (Based on Assigned To) 
	,@vcFilterValue VARCHAR(MAX)
	,@dtFromDate DATE
	,@dtToDate DATE
AS
BEGIN 
	DECLARE @tintFiscalStartMonth TINYINT

	SELECT @tintFiscalStartMonth=ISNULL(tintFiscalStartMonth,1) FROM Domain WHERE numDomainId=@numDomainID
	DECLARE @CurrentYearStartDate DATE 
	DECLARE @CurrentYearEndDate DATE

	SET @CurrentYearStartDate = DATEFROMPARTS(YEAR(DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE())),@tintFiscalStartMonth,1)
	IF @CurrentYearStartDate > DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE())
	BEGIN
		SET @CurrentYearStartDate = DATEADD(YEAR,-1,@CurrentYearStartDate)
	END	
	SET @CurrentYearEndDate = DATEADD(DAY,-1, DATEADD(YEAR,1,@CurrentYearStartDate))

	DECLARE @TABLEQuater TABLE
	(
		ID INT
		,QuaterStartDate DATE
		,QuaterEndDate DATE
	)

	INSERT INTO @TABLEQuater
	(
		ID
		,QuaterStartDate
		,QuaterEndDate
	)
	VALUES
	(
		1,DATEFROMPARTS(YEAR(@CurrentYearStartDate),@tintFiscalStartMonth,1),DATEADD(DAY,-1,DATEADD(MONTH,3,DATEFROMPARTS(YEAR(@CurrentYearStartDate),@tintFiscalStartMonth,1)))
	),
	(
		2,DATEADD(MONTH,3,DATEFROMPARTS(YEAR(@CurrentYearStartDate),@tintFiscalStartMonth,1)),DATEADD(DAY,-1,DATEADD(MONTH,6,DATEFROMPARTS(YEAR(@CurrentYearStartDate),@tintFiscalStartMonth,1)))
	),
	(
		3,DATEADD(MONTH,6,DATEFROMPARTS(YEAR(@CurrentYearStartDate),@tintFiscalStartMonth,1)),DATEADD(DAY,-1,DATEADD(MONTH,9,DATEFROMPARTS(YEAR(@CurrentYearStartDate),@tintFiscalStartMonth,1)))
	),
	(
		4,DATEADD(MONTH,9,DATEFROMPARTS(YEAR(@CurrentYearStartDate),@tintFiscalStartMonth,1)),DATEADD(DAY,-1,DATEADD(MONTH,12,DATEFROMPARTS(YEAR(@CurrentYearStartDate),@tintFiscalStartMonth,1)))
	)

	IF @vcTimeLine = 'CurYear'
	BEGIN
		SET @dtFromDate = @CurrentYearStartDate
		SET @dtToDate = @CurrentYearEndDate
	END
	ELSE IF @vcTimeLine = 'PreYear'
	BEGIN
		SET @dtFromDate =  DATEADD(YEAR,-1,@CurrentYearStartDate)
		SET @dtToDate = DATEADD(DAY,-1, DATEADD(YEAR,1,@dtFromDate))
	END
	ELSE IF @vcTimeLine = 'Pre2Year'
	BEGIN
		SET @dtFromDate = DATEADD(YEAR,-2,@CurrentYearStartDate)
		SET @dtToDate = DATEADD(DAY,-1,DATEADD(YEAR,2,@dtFromDate))
	END
	ELSE IF @vcTimeLine = 'Ago2Year'
	BEGIN
		SET @dtFromDate =  NULL
		SET @dtToDate = DATEADD(DAY,-1,DATEADD(YEAR,-2,@CurrentYearStartDate))
	END
	ELSE IF @vcTimeLine = 'CurPreYear'
	BEGIN
		SET @dtFromDate = DATEADD(YEAR,-1,@CurrentYearStartDate)
		SET @dtToDate = DATEADD(DAY,-1, DATEADD(YEAR,1,@dtFromDate))
	END
	ELSE IF @vcTimeLine = 'CurPre2Year'
	BEGIN
		SET @dtFromDate = DATEADD(YEAR,-2,@CurrentYearStartDate)
		SET @dtToDate = DATEADD(DAY,-1,DATEADD(YEAR,1, DATEFROMPARTS(YEAR(DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE())),@tintFiscalStartMonth,1)))
	END
	ELSE IF @vcTimeLine = 'CuQur'
	BEGIN
		SELECT
			@dtFromDate = QuaterStartDate
			,@dtToDate = QuaterEndDate
		FROM
			@TABLEQuater
		WHERE
			DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()) BETWEEN QuaterStartDate AND QuaterEndDate
	END
	ELSE IF @vcTimeLine = 'PreQur'
	BEGIN
		SELECT
			@dtFromDate = DATEADD(MONTH,-3,QuaterStartDate)
			,@dtToDate = DATEADD(MONTH,-3,QuaterEndDate)
		FROM
			@TABLEQuater
		WHERE
			DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()) BETWEEN QuaterStartDate AND QuaterEndDate
	END
	ELSE IF @vcTimeLine = 'CurPreQur'
	BEGIN
		SELECT
			@dtFromDate = DATEADD(MONTH,-3,QuaterStartDate)
			,@dtToDate = QuaterEndDate
		FROM
			@TABLEQuater
		WHERE
			DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()) BETWEEN QuaterStartDate AND QuaterEndDate
	END
	ELSE IF @vcTimeLine = 'ThisMonth'
	BEGIN
		SET @dtFromDate = dbo.get_month_start(DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()))
		SET @dtToDate = dbo.get_month_end(DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()))
	END
	ELSE IF @vcTimeLine = 'LastMonth'
	BEGIN
		SET @dtFromDate = DATEADD(MONTH,-1,dbo.get_month_start(DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE())))
		SET @dtToDate = DATEADD(MONTH,-1,dbo.get_month_end(DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE())))
	END
	ELSE IF @vcTimeLine = 'CurPreMonth'
	BEGIN
		SET @dtFromDate = DATEADD(MONTH,-1,dbo.get_month_start(DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE())))
		SET @dtToDate = dbo.get_month_end(DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()))
	END
	ELSE IF @vcTimeLine = 'LastWeek'
	BEGIN
		SET @dtFromDate = DATEADD(DAY,-7,dbo.get_week_start(DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE())))
		SET @dtToDate = DATEADD(DAY,-7,dbo.get_week_end(DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE())))
	END
	ELSE IF @vcTimeLine = 'ThisWeek'
	BEGIN
		SET @dtFromDate = dbo.get_week_start(DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()))
		SET @dtToDate = dbo.get_week_end(DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()))
	END
	ELSE IF @vcTimeLine = 'Yesterday'
	BEGIN
		SET @dtFromDate = DATEADD(DAY,-1,DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()))
		SET @dtToDate = DATEADD(DAY,-1,DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()))
	END
	ELSE IF @vcTimeLine = 'Today'
	BEGIN
		SET @dtFromDate = DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE())
		SET @dtToDate = DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE())
	END
	ELSE IF @vcTimeLine = 'Last7Day'
	BEGIN
		SET @dtFromDate = DATEADD(DAY,-7,DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()))
		SET @dtToDate = DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE())
	END
	ELSE IF @vcTimeLine = 'Last30Day'
	BEGIN
		SET @dtFromDate = DATEADD(DAY,-30,DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()))
		SET @dtToDate = DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE())
	END
	ELSE IF @vcTimeLine = 'Last60Day'
	BEGIN
		SET @dtFromDate = DATEADD(DAY,-60,DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()))
		SET @dtToDate = DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE())
	END
	ELSE IF @vcTimeLine = 'Last90Day'
	BEGIN
		SET @dtFromDate = DATEADD(DAY,-90,DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()))
		SET @dtToDate = DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE())
	END
	ELSE IF @vcTimeLine = 'Last120Day'
	BEGIN
		SET @dtFromDate = DATEADD(DAY,-120,DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()))
		SET @dtToDate = DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE())
	END

	DECLARE @TempResult TABLE
	(
		ID INT IDENTITY(1,1)
		,GroupValue VARCHAR(100)
		,StartDate DATE
		,EndDate DATE
		,SalesOppAmount DECIMAL(20,5)
		,numTotalProgress DECIMAL(20,2)
	)

	IF @vcGroupBy = 'Quater'
	BEGIN
		;WITH CTEYear
		AS
		(
			SELECT  
				CASE WHEN @dtFromDate < DATEFROMPARTS(DATEPART(YEAR,@dtFromDate),@tintFiscalStartMonth,1) THEN DATEPART(YEAR,DATEADD(YEAR,-1,@dtFromDate)) ELSE DATEPART(YEAR,@dtFromDate) END  AS yr
			UNION ALL
			SELECT 
				yr + 1
			FROM 
				CTEYear
			WHERE 
				yr < DATEPART(YEAR, @dtToDate)
		)
		INSERT INTO @TempResult
		(
			GroupValue
			,StartDate
			,EndDate
		)
		SELECT 
			CONCAT(yr,'-',Quater)
			,QuaterStart
			,QuaterEnd
		FROM 
			CTEYear
		CROSS APPLY
		(
			SELECT
				'Q1' Quater,
				DATEFROMPARTS(yr,@tintFiscalStartMonth,1) QuaterStart
				,DATEADD(DAY,-1,DATEADD(MONTH,3,DATEFROMPARTS(yr,@tintFiscalStartMonth,1))) QuaterEnd
			UNION
			SELECT
				'Q2' Quater,
				DATEADD(MONTH,3,DATEFROMPARTS(yr,@tintFiscalStartMonth,1)) QuaterStart
				,DATEADD(DAY,-1,DATEADD(MONTH,6,DATEFROMPARTS(yr,@tintFiscalStartMonth,1))) QuaterEnd
			UNION
			SELECT
				'Q3' Quater,
				DATEADD(MONTH,6,DATEFROMPARTS(yr,@tintFiscalStartMonth,1)) QuaterStart
				,DATEADD(DAY,-1,DATEADD(MONTH,9,DATEFROMPARTS(yr,@tintFiscalStartMonth,1))) QuaterEnd
			UNION
			SELECT
				'Q4' Quater,
				DATEADD(MONTH,9,DATEFROMPARTS(yr,@tintFiscalStartMonth,1)) QuaterStart
				,DATEADD(DAY,-1,DATEADD(MONTH,12,DATEFROMPARTS(yr,@tintFiscalStartMonth,1))) QuaterEnd
		) Test
		WHERE
			(QuaterStart BETWEEN @dtFromDate AND @dtToDate) OR 
			(QuaterEnd BETWEEN @dtFromDate AND @dtToDate) OR 
			(QuaterStart <= @dtFromDate AND QuaterEnd >= @dtToDate)
			
	END
	ELSE IF @vcGroupBy = 'Year'
	BEGIN
		;WITH CTEYear
		AS
		(
			SELECT  
				DATEPART(YEAR,@dtFromDate) AS yr
			UNION ALL
			SELECT 
				yr + 1
			FROM 
				CTEYear
			WHERE 
				yr < DATEPART(YEAR, @dtToDate)
		)
		INSERT INTO @TempResult
		(
			GroupValue
			,StartDate
			,EndDate
		)
		SELECT 
			CAST(yr AS VARCHAR(100))
			,DATEFROMPARTS(yr,@tintFiscalStartMonth,1)
			,DATEADD(DAY,-1,DATEADD(YEAR,1,DATEFROMPARTS(yr,@tintFiscalStartMonth,1))) 
		FROM 
			CTEYear
	END
	ELSE -- MONTH
	BEGIN
		;with CTEMonth (monthDate,StartDate,EndDate)
		AS
		(
			SELECT 
				@dtFromDate
				,dbo.get_month_start(@dtFromDate)
				,dbo.get_month_end(@dtFromDate)
			UNION ALL
			SELECT 
				DATEADD(month,1,monthDate)
				,dbo.get_month_start(DATEADD(month,1,monthDate))
				,dbo.get_month_end(DATEADD(month,1,monthDate))
			FROM 
				CTEMonth
			WHERE 
				DATEADD(month,1,monthDate)<=@dtToDate
		)
		
		INSERT INTO @TempResult
		(
			GroupValue
			,StartDate
			,EndDate
		)
		SELECT 
			CONCAT(DATENAME(MONTH,monthDate),'-',DATENAME(YEAR,monthDate))
			,StartDate
			,EndDate 
		FROM 
			CTEMonth
	END

	DECLARE @TempOppData TABLE
	(
		TotalRows NUMERIC(18,0)
		,CreatedDate DATETIME
		,monDealAmount DECIMAL(20,5)
		,numTotalProgress FLOAT
	)

	INSERT INTO @TempOppData
	(
		TotalRows
		,CreatedDate
		,monDealAmount
		,numTotalProgress
	)	
	SELECT
		COUNT(OpportunityMaster.numOppId) OVER() AS TotalRows
		,DATEADD(MINUTE,-@ClientTimeZoneOffset,OpportunityMaster.bintCreatedDate) CreatedDate
		,monDealAmount
		,(CASE WHEN PP.numOppID > 0 then ISNULL(PP.intTotalProgress,0) ELSE dbo.GetListIemName(OpportunityMaster.numPercentageComplete) END)
	FROM
		OpportunityMaster
	INNER JOIN
		DivisionMaster
	ON
		OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
	LEFT JOIN 
		ProjectProgress PP 
	ON 
		PP.numOppID = OpportunityMaster.numOppID
	WHERE
		OpportunityMaster.numDomainId=@numDomainID
		AND ISNULL(tintOppType,0)=1
		AND (OpportunityMaster.bintOppToOrder IS NOT NULL OR OpportunityMaster.dtDealLost IS NOT NULL)
		AND 1 = (CASE WHEN LEN(ISNULL(@vcTeritorry,'')) > 0 THEN (CASE WHEN DivisionMaster.numTerID IN (SELECT Id FROM dbo.SplitIDs(@vcFilterValue,',')) THEN 1 ELSE 0 END) ELSE 1 END)
		AND 1 = (CASE 
					WHEN LEN(ISNULL(@vcFilterValue,0)) > 0 AND @vcFilterBy IN (1,2,3)
					THEN 
						(CASE @vcFilterBy
							WHEN 1  -- Assign To
							THEN (CASE WHEN OpportunityMaster.numAssignedTo IN (SELECT Id FROM dbo.SplitIDs(@vcFilterValue,',')) THEN 1 ELSE 0 END)
							WHEN 2  -- Record Owner
							THEN (CASE WHEN OpportunityMaster.numRecOwner IN (SELECT Id FROM dbo.SplitIDs(@vcFilterValue,',')) THEN 1 ELSE 0 END)
							WHEN 3  -- Teams (Based on Assigned To)
							THEN (CASE WHEN OpportunityMaster.numAssignedTo IN (SELECT numUserCntID FROM UserTeams WHERE numDomainID=@numDomainID AND numTeam IN (SELECT Id FROM dbo.SplitIDs(@vcFilterValue,','))) THEN 1 ELSE 0 END)
						END)
					ELSE 1 
				END)
		AND CAST(DATEADD(MINUTE,-@ClientTimeZoneOffset,OpportunityMaster.bintCreatedDate) AS DATE) BETWEEN @dtFromDate AND @dtToDate 

	DECLARE @i AS INT = 1
	DECLARE @iCount AS INT
	SELECT @iCount=COUNT(*) FROM @TempResult

	WHILE @i <= @iCount
	BEGIN

		UPDATE
			@TempResult
		SET
			SalesOppAmount = ISNULL((SELECT SUM(monDealAmount) FROM @TempOppData WHERE CreatedDate BETWEEN StartDate AND EndDate),0)
			,numTotalProgress = ISNULL((SELECT CAST(AVG(numTotalProgress) AS DECIMAL(20,2)) FROM @TempOppData WHERE CreatedDate BETWEEN StartDate AND EndDate),0)
		WHERE
			ID=@i


		SET @i = @i + 1
	END

	SELECT * FROM @TempResult
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ReportListMaster_SalesOppWonLostPreBuildReport')
DROP PROCEDURE USP_ReportListMaster_SalesOppWonLostPreBuildReport
GO
CREATE PROCEDURE [dbo].[USP_ReportListMaster_SalesOppWonLostPreBuildReport]
	@numDomainID NUMERIC(18,0)
	,@ClientTimeZoneOffset INT
	,@vcTimeLine VARCHAR(50)
	,@vcGroupBy VARCHAR(50)
	,@vcTeritorry VARCHAR(MAX)
	,@vcFilterBy TINYINT  -- 1:Assign TO, 2: Record Owner, 3: Teams (Based on Assigned To) 
	,@vcFilterValue VARCHAR(MAX)
	,@dtFromDate DATE
	,@dtToDate DATE
AS
BEGIN 
	DECLARE @tintFiscalStartMonth TINYINT

	SELECT @tintFiscalStartMonth=ISNULL(tintFiscalStartMonth,1) FROM Domain WHERE numDomainId=@numDomainID
	DECLARE @CurrentYearStartDate DATE 
	DECLARE @CurrentYearEndDate DATE

	SET @CurrentYearStartDate = DATEFROMPARTS(YEAR(DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE())),@tintFiscalStartMonth,1)
	IF @CurrentYearStartDate > DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE())
	BEGIN
		SET @CurrentYearStartDate = DATEADD(YEAR,-1,@CurrentYearStartDate)
	END	
	SET @CurrentYearEndDate = DATEADD(DAY,-1, DATEADD(YEAR,1,@CurrentYearStartDate))

	DECLARE @TABLEQuater TABLE
	(
		ID INT
		,QuaterStartDate DATE
		,QuaterEndDate DATE
	)

	INSERT INTO @TABLEQuater
	(
		ID
		,QuaterStartDate
		,QuaterEndDate
	)
	VALUES
	(
		1,DATEFROMPARTS(YEAR(@CurrentYearStartDate),@tintFiscalStartMonth,1),DATEADD(DAY,-1,DATEADD(MONTH,3,DATEFROMPARTS(YEAR(@CurrentYearStartDate),@tintFiscalStartMonth,1)))
	),
	(
		2,DATEADD(MONTH,3,DATEFROMPARTS(YEAR(@CurrentYearStartDate),@tintFiscalStartMonth,1)),DATEADD(DAY,-1,DATEADD(MONTH,6,DATEFROMPARTS(YEAR(@CurrentYearStartDate),@tintFiscalStartMonth,1)))
	),
	(
		3,DATEADD(MONTH,6,DATEFROMPARTS(YEAR(@CurrentYearStartDate),@tintFiscalStartMonth,1)),DATEADD(DAY,-1,DATEADD(MONTH,9,DATEFROMPARTS(YEAR(@CurrentYearStartDate),@tintFiscalStartMonth,1)))
	),
	(
		4,DATEADD(MONTH,9,DATEFROMPARTS(YEAR(@CurrentYearStartDate),@tintFiscalStartMonth,1)),DATEADD(DAY,-1,DATEADD(MONTH,12,DATEFROMPARTS(YEAR(@CurrentYearStartDate),@tintFiscalStartMonth,1)))
	)

	IF @vcTimeLine = 'CurYear'
	BEGIN
		SET @dtFromDate = @CurrentYearStartDate
		SET @dtToDate = @CurrentYearEndDate
	END
	ELSE IF @vcTimeLine = 'PreYear'
	BEGIN
		SET @dtFromDate =  DATEADD(YEAR,-1,@CurrentYearStartDate)
		SET @dtToDate = DATEADD(DAY,-1, DATEADD(YEAR,1,@dtFromDate))
	END
	ELSE IF @vcTimeLine = 'Pre2Year'
	BEGIN
		SET @dtFromDate = DATEADD(YEAR,-2,@CurrentYearStartDate)
		SET @dtToDate = DATEADD(DAY,-1,DATEADD(YEAR,2,@dtFromDate))
	END
	ELSE IF @vcTimeLine = 'Ago2Year'
	BEGIN
		SET @dtFromDate =  NULL
		SET @dtToDate = DATEADD(DAY,-1,DATEADD(YEAR,-2,@CurrentYearStartDate))
	END
	ELSE IF @vcTimeLine = 'CurPreYear'
	BEGIN
		SET @dtFromDate = DATEADD(YEAR,-1,@CurrentYearStartDate)
		SET @dtToDate = DATEADD(DAY,-1, DATEADD(YEAR,1,@dtFromDate))
	END
	ELSE IF @vcTimeLine = 'CurPre2Year'
	BEGIN
		SET @dtFromDate = DATEADD(YEAR,-2,@CurrentYearStartDate)
		SET @dtToDate = DATEADD(DAY,-1,DATEADD(YEAR,1, DATEFROMPARTS(YEAR(DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE())),@tintFiscalStartMonth,1)))
	END
	ELSE IF @vcTimeLine = 'CuQur'
	BEGIN
		SELECT
			@dtFromDate = QuaterStartDate
			,@dtToDate = QuaterEndDate
		FROM
			@TABLEQuater
		WHERE
			DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()) BETWEEN QuaterStartDate AND QuaterEndDate
	END
	ELSE IF @vcTimeLine = 'PreQur'
	BEGIN
		SELECT
			@dtFromDate = DATEADD(MONTH,-3,QuaterStartDate)
			,@dtToDate = DATEADD(MONTH,-3,QuaterEndDate)
		FROM
			@TABLEQuater
		WHERE
			DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()) BETWEEN QuaterStartDate AND QuaterEndDate
	END
	ELSE IF @vcTimeLine = 'CurPreQur'
	BEGIN
		SELECT
			@dtFromDate = DATEADD(MONTH,-3,QuaterStartDate)
			,@dtToDate = QuaterEndDate
		FROM
			@TABLEQuater
		WHERE
			DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()) BETWEEN QuaterStartDate AND QuaterEndDate
	END
	ELSE IF @vcTimeLine = 'ThisMonth'
	BEGIN
		SET @dtFromDate = dbo.get_month_start(DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()))
		SET @dtToDate = dbo.get_month_end(DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()))
	END
	ELSE IF @vcTimeLine = 'LastMonth'
	BEGIN
		SET @dtFromDate = DATEADD(MONTH,-1,dbo.get_month_start(DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE())))
		SET @dtToDate = DATEADD(MONTH,-1,dbo.get_month_end(DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE())))
	END
	ELSE IF @vcTimeLine = 'CurPreMonth'
	BEGIN
		SET @dtFromDate = DATEADD(MONTH,-1,dbo.get_month_start(DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE())))
		SET @dtToDate = dbo.get_month_end(DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()))
	END
	ELSE IF @vcTimeLine = 'LastWeek'
	BEGIN
		SET @dtFromDate = DATEADD(DAY,-7,dbo.get_week_start(DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE())))
		SET @dtToDate = DATEADD(DAY,-7,dbo.get_week_end(DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE())))
	END
	ELSE IF @vcTimeLine = 'ThisWeek'
	BEGIN
		SET @dtFromDate = dbo.get_week_start(DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()))
		SET @dtToDate = dbo.get_week_end(DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()))
	END
	ELSE IF @vcTimeLine = 'Yesterday'
	BEGIN
		SET @dtFromDate = DATEADD(DAY,-1,DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()))
		SET @dtToDate = DATEADD(DAY,-1,DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()))
	END
	ELSE IF @vcTimeLine = 'Today'
	BEGIN
		SET @dtFromDate = DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE())
		SET @dtToDate = DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE())
	END
	ELSE IF @vcTimeLine = 'Last7Day'
	BEGIN
		SET @dtFromDate = DATEADD(DAY,-7,DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()))
		SET @dtToDate = DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE())
	END
	ELSE IF @vcTimeLine = 'Last30Day'
	BEGIN
		SET @dtFromDate = DATEADD(DAY,-30,DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()))
		SET @dtToDate = DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE())
	END
	ELSE IF @vcTimeLine = 'Last60Day'
	BEGIN
		SET @dtFromDate = DATEADD(DAY,-60,DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()))
		SET @dtToDate = DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE())
	END
	ELSE IF @vcTimeLine = 'Last90Day'
	BEGIN
		SET @dtFromDate = DATEADD(DAY,-90,DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()))
		SET @dtToDate = DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE())
	END
	ELSE IF @vcTimeLine = 'Last120Day'
	BEGIN
		SET @dtFromDate = DATEADD(DAY,-120,DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()))
		SET @dtToDate = DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE())
	END

	DECLARE @TempResult TABLE
	(
		ID INT IDENTITY(1,1)
		,GroupValue VARCHAR(100)
		,StartDate DATE
		,EndDate DATE
		,SalesOppAmount DECIMAL(20,5)
		,ClosedWonAmount DECIMAL(20,5)
		,CloseWonPercent DECIMAL(20,2)
		,CloseLostAmount DECIMAL(20,5)
		,CloseLostPercent DECIMAL(20,2)
	)


	IF @vcGroupBy = 'Quater'
	BEGIN
		;WITH CTEYear
		AS
		(
			SELECT  
				CASE WHEN @dtFromDate < DATEFROMPARTS(DATEPART(YEAR,@dtFromDate),@tintFiscalStartMonth,1) THEN DATEPART(YEAR,DATEADD(YEAR,-1,@dtFromDate)) ELSE DATEPART(YEAR,@dtFromDate) END  AS yr
			UNION ALL
			SELECT 
				yr + 1
			FROM 
				CTEYear
			WHERE 
				yr < DATEPART(YEAR, @dtToDate)
		)
		INSERT INTO @TempResult
		(
			GroupValue
			,StartDate
			,EndDate
		)
		SELECT 
			CONCAT(yr,'-',Quater)
			,QuaterStart
			,QuaterEnd
		FROM 
			CTEYear
		CROSS APPLY
		(
			SELECT
				'Q1' Quater,
				DATEFROMPARTS(yr,@tintFiscalStartMonth,1) QuaterStart
				,DATEADD(DAY,-1,DATEADD(MONTH,3,DATEFROMPARTS(yr,@tintFiscalStartMonth,1))) QuaterEnd
			UNION
			SELECT
				'Q2' Quater,
				DATEADD(MONTH,3,DATEFROMPARTS(yr,@tintFiscalStartMonth,1)) QuaterStart
				,DATEADD(DAY,-1,DATEADD(MONTH,6,DATEFROMPARTS(yr,@tintFiscalStartMonth,1))) QuaterEnd
			UNION
			SELECT
				'Q3' Quater,
				DATEADD(MONTH,6,DATEFROMPARTS(yr,@tintFiscalStartMonth,1)) QuaterStart
				,DATEADD(DAY,-1,DATEADD(MONTH,9,DATEFROMPARTS(yr,@tintFiscalStartMonth,1))) QuaterEnd
			UNION
			SELECT
				'Q4' Quater,
				DATEADD(MONTH,9,DATEFROMPARTS(yr,@tintFiscalStartMonth,1)) QuaterStart
				,DATEADD(DAY,-1,DATEADD(MONTH,12,DATEFROMPARTS(yr,@tintFiscalStartMonth,1))) QuaterEnd
		) Test
		WHERE
			(QuaterStart BETWEEN @dtFromDate AND @dtToDate) OR 
			(QuaterEnd BETWEEN @dtFromDate AND @dtToDate) OR 
			(QuaterStart <= @dtFromDate AND QuaterEnd >= @dtToDate)
			
	END
	ELSE IF @vcGroupBy = 'Year'
	BEGIN
		;WITH CTEYear
		AS
		(
			SELECT  
				DATEPART(YEAR,@dtFromDate) AS yr
			UNION ALL
			SELECT 
				yr + 1
			FROM 
				CTEYear
			WHERE 
				yr < DATEPART(YEAR, @dtToDate)
		)
		INSERT INTO @TempResult
		(
			GroupValue
			,StartDate
			,EndDate
		)
		SELECT 
			CAST(yr AS VARCHAR(100))
			,DATEFROMPARTS(yr,@tintFiscalStartMonth,1)
			,DATEADD(DAY,-1,DATEADD(YEAR,1,DATEFROMPARTS(yr,@tintFiscalStartMonth,1))) 
		FROM 
			CTEYear
	END
	ELSE -- MONTH
	BEGIN
		;with CTEMonth (monthDate,StartDate,EndDate)
		AS
		(
			SELECT 
				@dtFromDate
				,dbo.get_month_start(@dtFromDate)
				,dbo.get_month_end(@dtFromDate)
			UNION ALL
			SELECT 
				DATEADD(month,1,monthDate)
				,dbo.get_month_start(DATEADD(month,1,monthDate))
				,dbo.get_month_end(DATEADD(month,1,monthDate))
			FROM 
				CTEMonth
			WHERE 
				DATEADD(month,1,monthDate)<=@dtToDate
		)
		
		INSERT INTO @TempResult
		(
			GroupValue
			,StartDate
			,EndDate
		)
		SELECT 
			CONCAT(DATENAME(MONTH,monthDate),'-',DATENAME(YEAR,monthDate))
			,StartDate
			,EndDate 
		FROM 
			CTEMonth
	END

	DECLARE @TempOppData TABLE
	(
		TotalRows NUMERIC(18,0)
		,CreatedDate DATETIME
		,WonDate DATE
		,LostDate DATE
		,monDealAmount DECIMAL(20,5)
	)

	INSERT INTO @TempOppData
	(
		TotalRows
		,CreatedDate
		,WonDate
		,LostDate
		,monDealAmount
	)	
	SELECT
		COUNT(numOppId) OVER() AS TotalRows
		,DATEADD(MINUTE,-@ClientTimeZoneOffset,OpportunityMaster.bintCreatedDate) CreatedDate
		,(CASE WHEN ISNULL(tintOppStatus,0)=1 AND OpportunityMaster.bintOppToOrder IS NOT NULL THEN DATEADD(MINUTE,-@ClientTimeZoneOffset,OpportunityMaster.bintOppToOrder) ELSE NULL END) WonDate
		,(CASE WHEN ISNULL(tintOppStatus,0)=2 AND OpportunityMaster.dtDealLost IS NOT NULL THEN DATEADD(MINUTE,-@ClientTimeZoneOffset,OpportunityMaster.dtDealLost) ELSE NULL END) LostDate
		,monDealAmount
	FROM
		OpportunityMaster
	INNER JOIN
		DivisionMaster
	ON
		OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
	WHERE
		OpportunityMaster.numDomainId=@numDomainID
		AND ISNULL(tintOppType,0)=1
		AND (OpportunityMaster.bintOppToOrder IS NOT NULL OR OpportunityMaster.dtDealLost IS NOT NULL)
		AND 1 = (CASE WHEN LEN(ISNULL(@vcTeritorry,'')) > 0 THEN (CASE WHEN DivisionMaster.numTerID IN (SELECT Id FROM dbo.SplitIDs(@vcFilterValue,',')) THEN 1 ELSE 0 END) ELSE 1 END)
		AND 1 = (CASE 
					WHEN LEN(ISNULL(@vcFilterValue,0)) > 0 AND @vcFilterBy IN (1,2,3)
					THEN 
						(CASE @vcFilterBy
							WHEN 1  -- Assign To
							THEN (CASE WHEN OpportunityMaster.numAssignedTo IN (SELECT Id FROM dbo.SplitIDs(@vcFilterValue,',')) THEN 1 ELSE 0 END)
							WHEN 2  -- Record Owner
							THEN (CASE WHEN OpportunityMaster.numRecOwner IN (SELECT Id FROM dbo.SplitIDs(@vcFilterValue,',')) THEN 1 ELSE 0 END)
							WHEN 3  -- Teams (Based on Assigned To)
							THEN (CASE WHEN OpportunityMaster.numAssignedTo IN (SELECT numUserCntID FROM UserTeams WHERE numDomainID=@numDomainID AND numTeam IN (SELECT Id FROM dbo.SplitIDs(@vcFilterValue,','))) THEN 1 ELSE 0 END)
						END)
					ELSE 1 
				END)
		AND (CAST(DATEADD(MINUTE,-@ClientTimeZoneOffset,OpportunityMaster.bintCreatedDate) AS DATE) BETWEEN @dtFromDate AND @dtToDate 
			OR CAST(DATEADD(MINUTE,-@ClientTimeZoneOffset,OpportunityMaster.bintOppToOrder) AS DATE) BETWEEN @dtFromDate AND @dtToDate
			OR CAST(DATEADD(MINUTE,-@ClientTimeZoneOffset,OpportunityMaster.dtDealLost) AS DATE) BETWEEN @dtFromDate AND @dtToDate)

	DECLARE @i AS INT = 1
	DECLARE @iCount AS INT
	SELECT @iCount=COUNT(*) FROM @TempResult

	WHILE @i <= @iCount
	BEGIN

		UPDATE
			@TempResult
		SET
			SalesOppAmount = ISNULL((SELECT SUM(monDealAmount) FROM @TempOppData WHERE (CreatedDate BETWEEN StartDate AND EndDate OR LostDate BETWEEN StartDate AND EndDate OR WonDate BETWEEN StartDate AND EndDate)),0)
			,ClosedWonAmount = ISNULL((SELECT SUM(monDealAmount) FROM @TempOppData WHERE WonDate BETWEEN StartDate AND EndDate),0)
			,CloseWonPercent = (CASE WHEN ISNULL((SELECT COUNT(*) FROM @TempOppData WHERE (CreatedDate BETWEEN StartDate AND EndDate OR LostDate BETWEEN StartDate AND EndDate OR WonDate BETWEEN StartDate AND EndDate)),0) > 0 THEN (ISNULL((SELECT COUNT(*) FROM @TempOppData WHERE WonDate BETWEEN StartDate AND EndDate),0) * 100) / ISNULL((SELECT COUNT(*) FROM @TempOppData WHERE (CreatedDate BETWEEN StartDate AND EndDate OR LostDate BETWEEN StartDate AND EndDate OR WonDate BETWEEN StartDate AND EndDate)),0) ELSE 0 END)
			,CloseLostAmount = ISNULL((SELECT SUM(monDealAmount) FROM @TempOppData WHERE LostDate BETWEEN StartDate AND EndDate),0)
			,CloseLostPercent = (CASE WHEN ISNULL((SELECT COUNT(*) FROM @TempOppData WHERE (CreatedDate BETWEEN StartDate AND EndDate OR LostDate BETWEEN StartDate AND EndDate OR WonDate BETWEEN StartDate AND EndDate)),0) > 0 THEN (ISNULL((SELECT COUNT(*) FROM @TempOppData WHERE LostDate BETWEEN StartDate AND EndDate),0) * 100) / ISNULL((SELECT COUNT(*) FROM @TempOppData WHERE (CreatedDate BETWEEN StartDate AND EndDate OR LostDate BETWEEN StartDate AND EndDate OR WonDate BETWEEN StartDate AND EndDate)),0) ELSE 0 END)
		WHERE
			ID=@i


		SET @i = @i + 1
	END

	SELECT * FROM @TempResult
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_TaxReports')
DROP PROCEDURE USP_TaxReports
GO
CREATE PROCEDURE [dbo].[USP_TaxReports]     
    @numDomainID numeric(18, 0),
    @dtFromDate DATETIME,
    @dtToDate DATETIME
AS                 
BEGIN                
	SELECT 
		T.numOppBizDocsId,
		T.numOppId,
		vcBizDocID,
		TI.vcTaxName,
		T.fltPercentage,
		T.tintTaxType,
		SUM(Case When T.tintOppType=1 then TaxAmount else 0 end) SalesTax,
		SUM(Case When T.tintOppType=2 then TaxAmount else 0 end) PurchaseTax,
		T.INVCreatedDate,
		T.Organization,
		T.vcShipState,
		ISNULL((SELECT SUM(monTotAmtBefDiscount) FROM OpportunityBizDocItems WHERE numOppBizDocID=T.numOppBizDocsId),0) AS SubTotal,
		T.GrandTotal
	FROM 
	(
		SELECT 
			OBD.numOppBizDocsId,
		    OBD.vcBizDocID,
			OMTI.*,
			OM.tintOppType,
			ISNULL(dbo.fn_CalOppItemTotalTaxAmt(OM.numDomainId,OMTI.numTaxItemID,OM.numOppId,OBD.numOppBizDocsId),0) AS TaxAmount,
			[dbo].[FormatedDateFromDate](OBD.dtCreatedDate,CONVERT(VARCHAR(10), @numDomainId)) AS INVCreatedDate,
			CI.vcCompanyName AS Organization,
			ISNULL(dbo.fn_getOPPState(OM.numOppId,@numDomainId,2),'') AS vcShipState,			
			OBD.monDealAmount AS GrandTotal
			
		FROM OpportunityMaster OM 
		JOIN OpportunityBizDocs OBD 
		ON OM.numOppId=OBD.numOppId
		JOIN OpportunityMasterTaxItems OMTI 
		ON OMTI.numOppId=OBD.numOppId
		LEFT JOIN DivisionMaster DM
		ON OM.numDivisionId = DM.numDivisionID
		LEFT JOIN CompanyInfo CI
		ON DM.numCompanyID = CI.numCompanyId
		
		WHERE 
			OM.numDomainID=@numDomainID 
			AND OBD.bitAuthoritativeBizDocs=1 
			AND OMTI.fltPercentage>0
			AND OBD.dtFromDate BETWEEN  @dtFromDate AND @dtToDate
	) T 
	JOIN 
	(
		SELECT 
			numTaxItemId,
			vcTaxName 
		FROM 
			TaxItems 
		WHERE 
			numDomainID=@numDomainID 
		UNION 
		SELECT 
			0,
			'Sales Tax'
		UNION 
		SELECT 
			1,
			'CRV'
	) TI 
	ON T.numTaxItemId=TI.numTaxItemID 
	WHERE 
		TaxAmount > 0
	GROUP BY 
		T.vcBizDocID,
		TI.vcTaxName,
		T.fltPercentage,
		T.tintTaxType,
		T.INVCreatedDate,
		T.Organization,
		T.vcShipState,
		T.GrandTotal,
		T.numOppBizDocsId,
		T.numOppId
END
GO
