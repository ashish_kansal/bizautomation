/******************************************************************
Project: Release 13.3 Date: 02.APR.2020
Comments: ALTER SCRIPTS
*******************************************************************/


/******************************************** SANDEEP *********************************************/


ALTER TABLE WorkOrder ADD numQtyReceived FLOAT DEFAULT 0
ALTER TABLE WorkOrder ADD numUnitHourReceived FLOAT DEFAULT 0
ALTER TABLE OppWarehouseSerializedItem ADD numWOID NUMERIC(18,0)
ALTER TABLE Sales_process_List_Master ADD numBuildManager NUMERIC(18,0)

----------------------------------------------

UPDATE PageNavigationDtl SET vcPageNavName='Assemblies & BOMs' WHERE numPageNavID=3 AND numModuleID=37 AND numParentID=62 AND vcPageNavName='Assemblies'

----------------------------------------------

--CHECK IF FIELD IS NOT ALREADY ADDED ON PRODUCTION
INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,[order],tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
)
VALUES
(
	3,314,143,0,0,'Serial/Lot #s','TextBox',11,11,1,1,0,0,1,0,0
)

------------------------------

BEGIN TRY
BEGIN TRANSACTION

	DECLARE @numMAXPageNavID INT
	SELECT @numMAXPageNavID = MAX(numPageNavID) FROM PageNavigationDTL


	INSERT INTO PageNavigationDTL
	(
		numPageNavID,numModuleID,numParentID,vcPageNavName,vcNavURL,bitVisible,numTabID,vcAddURL,bitAddIsPopup
	)
	VALUES
	(
		(@numMAXPageNavID + 1)
		,(SELECT TOP 1 numModuleID FROM ModuleMaster WHERE vcModuleName='Manufacturing')
		,(SELECT TOP 1 numPageNavID FROM PageNavigationDTL WHERE vcPageNavName='Manufacturing')
		,'Routings'
		,'../admin/frmAdminBusinessProcess.aspx?tintProcessType=3'
		,1
		,(SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Manufacturing' AND numDomainID=1)
		,''
		,0
	)

	INSERT INTO dbo.TreeNavigationAuthorization
	(
		numGroupID,
		numTabID,
		numPageNavID,
		bitVisible,
		numDomainID,
		tintType
	)
	SELECT  
		MIN(numGroupID),
		(SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Manufacturing' AND numDomainID=1),
		(@numMAXPageNavID + 1),
		1,
		numDomainID,
		1
	FROM    
		AuthenticationGroupMaster
	GROUP BY
		numDomainID
COMMIT
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		ERROR_MESSAGE(),
		ERROR_NUMBER(),
		ERROR_SEVERITY(),
		ERROR_STATE(),
		ERROR_LINE(),
		ISNULL(ERROR_PROCEDURE(), '-');
END CATCH

------------------------------

BEGIN TRY
BEGIN TRANSACTION

	DECLARE @numMAXPageNavID INT
	SELECT @numMAXPageNavID = MAX(numPageNavID) FROM PageNavigationDTL


	INSERT INTO PageNavigationDTL
	(
		numPageNavID,numModuleID,numParentID,vcPageNavName,vcNavURL,bitVisible,numTabID,vcAddURL,bitAddIsPopup
	)
	VALUES
	(
		(@numMAXPageNavID + 1)
		,(SELECT TOP 1 numModuleID FROM ModuleMaster WHERE vcModuleName='Manufacturing')
		,(SELECT TOP 1 numPageNavID FROM PageNavigationDTL WHERE vcPageNavName='Manufacturing')
		,'Work Centers'
		,'../admin/frmMasterList.aspx?ModuleId=9&ListId=35'
		,1
		,(SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Manufacturing' AND numDomainID=1)
		,''
		,0
	)

	INSERT INTO dbo.TreeNavigationAuthorization
	(
		numGroupID,
		numTabID,
		numPageNavID,
		bitVisible,
		numDomainID,
		tintType
	)
	SELECT  
		MIN(numGroupID),
		(SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Manufacturing' AND numDomainID=1),
		(@numMAXPageNavID + 1),
		1,
		numDomainID,
		1
	FROM    
		AuthenticationGroupMaster
	GROUP BY
		numDomainID
COMMIT
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		ERROR_MESSAGE(),
		ERROR_NUMBER(),
		ERROR_SEVERITY(),
		ERROR_STATE(),
		ERROR_LINE(),
		ISNULL(ERROR_PROCEDURE(), '-');
END CATCH


/******************************************** PRASHANT *********************************************/


BEGIN TRY
BEGIN TRANSACTION

DECLARE @numFieldID AS NUMERIC(18,0)
DECLARE @numFormFieldID AS NUMERIC(18,0)

INSERT INTO DycFieldMaster
(numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,[order],tintRow,tintColumn,
bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numListID,PopupFunctionName)
VALUES
(3,'Approval Status','intReqPOApproved','intReqPOApproved','intReqPOApproved','OpportunityMaster','N','R','SelectBox',46,1,1,1,1,0,0,1,0,1,1,1,0,'')

SELECT @numFieldID = SCOPE_IDENTITY()

INSERT INTO DynamicFormFieldMaster
(numFormID,vcFormFieldName,vcFieldType,vcAssociatedControlType,vcDbColumnName,numListID,bitDeleted,vcFieldDataType,vcOrigDbColumnName,bitAllowEdit,vcLookBackTableName,
 bitDefault,[order],bitInResults,vcPropertyName,tintRow,tintColumn,bitSettingField,bitAddField,bitDetailField,bitAllowSorting)
VALUES
(41,'Approval Status','R','SelectBox','intReqPOApproved',0,0,'N','intReqPOApproved',0,'OpportunityMaster',0,7,1,'intReqPOApproved',1,7,1,0,1,0)

SELECT @numFormFieldID = SCOPE_IDENTITY()

INSERT INTO DycFormField_Mapping
(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,bitInResults,bitDeleted,
bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numFormFieldID)
VALUES
(3,@numFieldID,41,1,1,'Approval Status','SelectBox','intReqPOApproved',7,1,7,1,0,0,1,0,1,0,1,@numFormFieldID)

COMMIT
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		ERROR_MESSAGE(),
		ERROR_NUMBER(),
		ERROR_SEVERITY(),
		ERROR_STATE(),
		ERROR_LINE(),
		ISNULL(ERROR_PROCEDURE(), '-');
END CATCH

----------------------------------------------------------------------------------------

BEGIN TRY
BEGIN TRANSACTION

DECLARE @numFieldID AS NUMERIC(18,0)
DECLARE @numFormFieldID AS NUMERIC(18,0)

INSERT INTO DycFieldMaster
(numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,[order],tintRow,tintColumn,
bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numListID,PopupFunctionName)
VALUES
(3,'Approval Status','intReqPOApproved','intReqPOApproved','intReqPOApproved','OpportunityMaster','N','R','SelectBox',46,1,1,1,1,0,0,1,0,1,1,1,0,'')

SELECT @numFieldID = SCOPE_IDENTITY()

INSERT INTO DynamicFormFieldMaster
(numFormID,vcFormFieldName,vcFieldType,vcAssociatedControlType,vcDbColumnName,numListID,bitDeleted,vcFieldDataType,vcOrigDbColumnName,bitAllowEdit,vcLookBackTableName,
 bitDefault,[order],bitInResults,vcPropertyName,tintRow,tintColumn,bitSettingField,bitAddField,bitDetailField,bitAllowSorting)
VALUES
(40,'Approval Status','R','SelectBox','intReqPOApproved',0,0,'N','intReqPOApproved',0,'OpportunityMaster',0,7,1,'intReqPOApproved',1,7,1,0,1,0)

SELECT @numFormFieldID = SCOPE_IDENTITY()

INSERT INTO DycFormField_Mapping
(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,bitInResults,bitDeleted,
bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numFormFieldID)
VALUES
(3,@numFieldID,40,1,1,'Approval Status','SelectBox','intReqPOApproved',7,1,7,1,0,0,1,0,1,0,1,@numFormFieldID)

COMMIT
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		ERROR_MESSAGE(),
		ERROR_NUMBER(),
		ERROR_SEVERITY(),
		ERROR_STATE(),
		ERROR_LINE(),
		ISNULL(ERROR_PROCEDURE(), '-');
END CATCH

--USE [Production.2014]
--GO
--CREATE NONCLUSTERED INDEX [NonClusterDomainAssignCreatedActivityId]
--ON [dbo].[Communication] ([numDomainID])
--INCLUDE ([numAssign],[numCreatedBy],[numActivityId])
--GO

ALTER TABLE OpportunityMaster ADD intReqPOApproved INT DEFAULT 0
ALTER TABLE ApprovalProcessItemsClassification DROP CONSTRAINT  FK_ApprovalProcessItemsClassification_ApprovalProcessItemsClassification
UPDATE DycFormField_Mapping SET vcPropertyName='CaseStatus' WHERE numFormID=12 AND vcFieldName='Status'
DELETE FROM CFw_Grp_Master WHERE Loc_Id=3 AND Grp_Name='Case Details'
UPDATE PageNavigationDTL SET vcNavUrl='../common/frmTicklerdisplayOld.aspx?SelectedIndex=0' WHERE vcPageNavName='Activities 1.0'
BEGIN TRY
BEGIN TRANSACTION

	DECLARE @numMAXPageNavID INT
	SELECT @numMAXPageNavID = MAX(numPageNavID) FROM PageNavigationDTL


	INSERT INTO PageNavigationDTL
	(
		numPageNavID,numModuleID,numParentID,vcPageNavName,vcNavURL,bitVisible,numTabID
	)
	VALUES
	(
		(@numMAXPageNavID + 1),(SELECT TOP 1 numModuleID FROM ModuleMaster WHERE vcModuleName='Tickler'),(SELECT numPageNavID FROM PageNavigationDTL WHERE vcPageNavName='Activities'),'Activities 1.0','../common/frmTicklerdisplayOld.aspx',1,(SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Activities' AND numDomainID=1)
	)

	INSERT INTO dbo.TreeNavigationAuthorization
	(
		numGroupID,
		numTabID,
		numPageNavID,
		bitVisible,
		numDomainID,
		tintType
	)
	SELECT  
		numGroupID,
		(SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Activities' AND numDomainID=1),
		(@numMAXPageNavID + 1),
		1,
		numDomainID,
		1
	FROM    
		AuthenticationGroupMaster
	WHERE
		vcGroupName='System Administrator'
COMMIT
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		ERROR_MESSAGE(),
		ERROR_NUMBER(),
		ERROR_SEVERITY(),
		ERROR_STATE(),
		ERROR_LINE(),
		ISNULL(ERROR_PROCEDURE(), '-');
END CATCH