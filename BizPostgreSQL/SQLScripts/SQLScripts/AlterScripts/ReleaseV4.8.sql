/******************************************************************
Project: Release 4.8 Date: 29.July.2015
Comments: ALTER SCRIPTS
*******************************************************************/

-------------- SANDEEP --------------

ALTER TABLE OpportunityBizDocs ADD
bitFulFilled BIT

--------------------------------------------------------------------------------------------------------------------

BEGIN TRANSACTION

DECLARE @numOrderShipped AS NUMERIC(18,0) = 0
DECLARE @numOrderReceived AS NUMERIC(18,0) = 0
DECLARE @numQtyShippedReceived AS  NUMERIC(18,0) = 0


INSERT INTO DycFieldMaster 
(numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitDetailField,numListID)
VALUES
(
3,'Ordered / Shipped','vcOrderedShipped','vcOrderedShipped','vcOrderedShipped','OpportunityMaster','V','R','Label',1,0,0,0,1,1,0
)

SELECT @numOrderShipped = SCOPE_IDENTITY()

INSERT INTO DycFormField_Mapping 
(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField)
VALUES
(3,@numOrderShipped,39,0,0,'Ordered / Shipped','Label','vcOrderedShipped',1,0,0,1,0,1)

INSERT INTO DycFieldMaster 
(numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitDetailField,numListID)
VALUES
(
3,'Ordered / Received','vcOrderedReceived','vcOrderedReceived','vcOrderedReceived','OpportunityMaster','V','R','Label',1,0,0,0,1,1,0
)

SELECT @numOrderReceived = SCOPE_IDENTITY()

INSERT INTO DycFormField_Mapping 
(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField)
VALUES
(3,@numOrderReceived,41,0,0,'Ordered / Received','Label','vcOrderedReceived',1,0,0,1,0,1)

INSERT INTO DycFieldMaster 
(numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitDetailField,numListID)
VALUES
(
3,'Shipped / Received','vcShippedReceived','vcShippedReceived','vcShippedReceived','OpportunityItems','V','R','Label',1,0,0,0,1,1,0
)

SELECT @numQtyShippedReceived = SCOPE_IDENTITY()

INSERT INTO DycFormField_Mapping 
(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField)
VALUES
(4,@numQtyShippedReceived,26,0,0,'Shipped / Received','Label','vcShippedReceived',1,0,0,1,0,1)

DECLARE @FieldID AS INT = 0
SELECT @FieldID = numFieldID FROM DycFormField_Mapping WHERE numFormID=26 AND vcFieldName = 'Received'

UPDATE DycFormConfigurationDetails SET numFieldId=@numQtyShippedReceived WHERE numFormId=26 AND numFieldId = @FieldID
DELETE FROM DycFormField_Mapping WHERE numFormID=26 AND numFieldId = @FieldID

COMMIT

----------------------------------------------------------------------------------------------------------------------------

ALTER TABLE WarehouseItmsDTL
DROP COLUMN tintStatus

----------------------------------------------------------------------------------------------------------------------------

SET IDENTITY_INSERT AccountingChargeTypes ON
INSERT INTO AccountingChargeTypes 
(numChargeTypeId,vcChageType,vcChargeAccountCode,chChargeCode)
VALUES
(25,'Sales Clearing','','SC')
SET IDENTITY_INSERT AccountingChargeTypes OFF

-------------------------------------------------------------------------------------------------------------------------------

BEGIN TRANSACTION

DECLARE @TEMP TABLE
(
	ID INT IDENTITY(1,1),
	DomainID INT
)

INSERT INTO @TEMP SELECT numDomainID FROM Domain WHERE numDomainID > 0

DECLARE @i AS INT = 0
DECLARE @COUNT AS INT
DECLARE @numDomainID AS INT

SELECT @COUNT=COUNT(*) FROM @TEMP

WHILE @i <= @COUNT
BEGIN
	SELECT @numDomainID=DomainID FROM @TEMP WHERE ID=@i
	PRINT 'DomainID:' + CAST(@numDomainID AS VARCHAR)

	DECLARE @dtTodayDate AS DATETIME
	SET @dtTodayDate = GETDATE()

	DECLARE @numAssetAccountTypeID AS NUMERIC(9,0)
	SELECT @numAssetAccountTypeID = numAccountTypeId FROM dbo.AccountTypeDetail WHERE vcAccountCode ='0101' AND numDomainID = @numDomainID 

	DECLARE @numARAccountTypeID AS NUMERIC(9)
	SELECT @numARAccountTypeID = numAccountTypeID FROM dbo.AccountTypeDetail WHERE vcAccountCode = '01010105'  AND numDomainID = @numDomainID  

	EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numAssetAccountTypeID,
    @numParntAcntTypeID = @numARAccountTypeID, @vcAccountName = 'Sales Clearing',
    @vcAccountDescription = 'Sales Clearing.',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0


	DECLARE @SC AS VARCHAR(10)
	SELECT @SC = numAccountId FROM dbo.Chart_Of_Accounts WHERE numDomainId = @numDomainId AND vcAccountName = 'Sales Clearing'


	INSERT  INTO AccountingCharges
    (
        numChargeTypeId,
        numAccountID,
        numDomainID	
    )
    SELECT  
		( 
			SELECT    
				numChargeTypeId
            FROM      
				AccountingChargeTypes
            WHERE     
				chChargeCode = 'SC'
        ),
        @SC,
        @numDomainID

	SET @i = @i + 1
END

ROLLBACK

--INCASE NEEDED TO COMARE IF SLAES CLEARING ACCOUNT IS ADDED FOR ALL DOMAIN OR NOT
--SELECT DISTINCT numDomainID FROM AccountingCharges WHERE numChargeTypeId=25
--SELECT COUNT(*) FROM Domain