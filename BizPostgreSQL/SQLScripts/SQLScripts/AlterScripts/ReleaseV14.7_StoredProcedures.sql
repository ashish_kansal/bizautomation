/******************************************************************
Project: Release 14.7 Date: 04.JAN.2021
Comments: STORE PROCEDURES
*******************************************************************/
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_GetEmailStringAsJson')
DROP FUNCTION fn_GetEmailStringAsJson
GO
CREATE FUNCTION [dbo].[fn_GetEmailStringAsJson]
(
	@vcEmailId VARCHAR(4000)
) 
RETURNS VARCHAR(4000)
As
BEGIN
	DECLARE @vcNewEmailId AS VARCHAR(2000) = '['

	DECLARE @TEMPEmails TABLE
	(
		ID INT IDENTITY(1,1)
		,vcContactEmail VARCHAR(1000)
	)

	DECLARE @TEMPEMailPart TABLE
	(
		ID INT
		,vcPartValue VARCHAR(300)
	)

	INSERT INTO @TEMPEmails (vcContactEmail) SELECT Items FROM dbo.SplitByString(@vcEmailId,'#^#')

	DECLARE @i INT = 1
	DECLARE @iCount INT
	DECLARE @vcContactEmail VARCHAR(1000)
	DECLARE @vcEmailJson VARCHAR(1000)

	SELECT @iCount=COUNT(*) FROM @TEMPEmails

	WHILE @i <= @iCount
	BEGIN
		DELETE FROM @TEMPEMailPart
		SELECT @vcContactEmail = vcContactEmail FROM @TEMPEmails WHERE ID = @i

		INSERT INTO @TEMPEMailPart (ID,vcPartValue) SELECT ROW_NUMBER() OVER(ORDER BY (SELECT NULL)),Items FROM dbo.SplitByString(@vcContactEmail,'$^$')

		 SET @vcEmailJson = CONCAT('{',
										'"id":"',(CASE WHEN ISNULL((SELECT COUNT(*) FROM @TEMPEMailPart),0) > 0 THEN ISNULL((SELECT EM.numContactId FROM @TEMPEMailPart TEP INNER JOIN EmailMaster EM ON CAST(TEP.vcPartValue AS NUMERIC)=Em.numEmailId WHERE TEP.ID=1),0) ELSE '0' END), '",',
										'"first_name":"',(CASE 
															WHEN ISNULL((SELECT COUNT(*) FROM @TEMPEMailPart),0) > 0
															 THEN (CASE 
																		WHEN LTRIM(RTRIM(ISNULL((SELECT EM.vcName FROM @TEMPEMailPart TEP INNER JOIN EmailMaster EM ON CAST(TEP.vcPartValue AS NUMERIC)=Em.numEmailId WHERE TEP.ID=1),''))) = '' 
																		THEN ISNULL((SELECT vcPartValue FROM @TEMPEMailPart WHERE ID=2),'-')
																		ELSE ISNULL((SELECT EM.vcName FROM @TEMPEMailPart TEP INNER JOIN EmailMaster EM ON CAST(TEP.vcPartValue AS NUMERIC)=Em.numEmailId WHERE TEP.ID=1),'-')
																	END) 
															 ELSE '-' 
														END),'",',
										'"last_name":"","email":"' ,(CASE WHEN ISNULL((SELECT COUNT(*) FROM @TEMPEMailPart),0) > 2 THEN ISNULL((SELECT vcPartValue FROM @TEMPEMailPart WHERE ID=3),'-') ELSE '0' END),'","url":"#"}')

		SET @vcNewEmailId = CONCAT(@vcNewEmailId,(CASE WHEN @vcNewEmailId = '[' THEN '' ELSE ',' END),@vcEmailJson)

		SET @i = @i + 1
	END

	SET @vcNewEmailId = CONCAT(@vcNewEmailId,']')

	RETURN @vcNewEmailId
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_CalculateCommission')
DROP PROCEDURE USP_CalculateCommission
GO
CREATE PROCEDURE [dbo].[USP_CalculateCommission]
	-- Add the parameters for the stored procedure here
	 @numComPayPeriodID AS NUMERIC(18,0),
	 @numDomainID AS NUMERIC(18,0),
	 @ClientTimeZoneOffset INT
AS
BEGIN
	-- NOTE: CALLED FROM USP_CommissionPayPeriod_Save WITH TRANSACTION

	SET NOCOUNT ON;

	DECLARE @dtPayStart DATE
	DECLARE @dtPayEnd DATE

	DECLARE @TempBizCommission TABLE
	(
		numUserCntID NUMERIC(18,0),
		numOppID NUMERIC(18,0),
		numOppBizDocID NUMERIC(18,0),
		numOppBizDocItemID NUMERIC(18,0),
		monCommission DECIMAL(20,5),
		numComRuleID NUMERIC(18,0),
		tintComType TINYINT,
		tintComBasedOn TINYINT,
		decCommission FLOAT,
		tintAssignTo TINYINT,
		numOppItemID NUMERIC(18,0),
		monInvoiceSubTotal DECIMAL(20,5),
		monOrderSubTotal DECIMAL(20,5)
	)

	DECLARE @TABLEPAID TABLE
	(
		ID INT IDENTITY(1,1),
		numDivisionID NUMERIC(18,0),
		numRelationship NUMERIC(18,0),
		numProfile NUMERIC(18,0),
		numAssignedTo NUMERIC(18,0),
		numRecordOwner NUMERIC(18,0),
		numItemCode NUMERIC(18,0),
		numItemClassification NUMERIC(18,0),
		numOppID NUMERIC(18,0),
		numOppItemID NUMERIC(18,0),
		numOppBizDocID NUMERIC(18,0),
		numOppBizDocItemID NUMERIC(18,0),
		numUnitHour FLOAT,
		monTotAmount DECIMAL(20,5),
		monInvoiceSubTotal DECIMAL(20,5),
		monOrderSubTotal DECIMAL(20,5),
		monVendorCost DECIMAL(20,5),
		monAvgCost DECIMAL(20,5),
		numPartner NUMERIC(18,0),
		numPOID NUMERIC(18,0),
		numPOItemID NUMERIC(18,0),
		monPOCost DECIMAL(20,5)
	)

	SELECT 
		@dtPayStart=dtStart
		,@dtPayEnd=dtEnd 
	FROM 
		CommissionPayPeriod 
	WHERE 
		numComPayPeriodID = @numComPayPeriodID

	--GET COMMISSION GLOBAL SETTINGS
	DECLARE @numDiscountServiceItemID NUMERIC(18,0)
	DECLARE @numShippingServiceItemID NUMERIC(18,0)
	DECLARE @tintCommissionType TINYINT
	DECLARE @bitCommissionBasedOn BIT
	DECLARE @tintCommissionBasedOn TINYINT
	DECLARE @bitIncludeTaxAndShippingInCommission BIT

	SELECT 
		@numDiscountServiceItemID=numDiscountServiceItemID, 
		@numShippingServiceItemID = numShippingServiceItemID, 
		@tintCommissionType=tintCommissionType, 
		@bitIncludeTaxAndShippingInCommission=bitIncludeTaxAndShippingInCommission
		,@tintCommissionBasedOn=ISNULL(tintCommissionBasedOn,1)
		,@bitCommissionBasedOn=ISNULL(bitCommissionBasedOn,0)
	FROM 
		Domain 
	WHERE 
		numDomainID=@numDomainID

	DELETE 
		BC 
	FROM 
		BizDocComission BC
	LEFT JOIN
		OpportunityMaster OM
	ON
		BC.numOppID = OM.numOppId
	WHERE 
		BC.numDomainId=@numDomainID
		AND ISNULL(BC.numOppID,0) > 0
		AND ISNULL(BC.bitCommisionPaid,0)=0
		AND OM.numOppId IS NULL

	DELETE 
		BC 
	FROM 
		BizDocComission BC
	LEFT JOIN
		OpportunityItems OI
	ON
		BC.numOppItemID = OI.numoppitemtCode
	WHERE 
		BC.numDomainId=@numDomainID 
		AND ISNULL(BC.numOppItemID,0) > 0
		AND ISNULL(BC.bitCommisionPaid,0)=0
		AND OI.numoppitemtCode IS NULL

	DELETE 
		BC 
	FROM 
		BizDocComission BC
	LEFT JOIN
		OpportunityBizDocs OBD
	ON
		BC.numOppBizDocId = OBD.numOppBizDocsId
	WHERE 
		BC.numDomainId=@numDomainID
		AND ISNULL(BC.numOppBizDocId,0) > 0
		AND ISNULL(BC.bitCommisionPaid,0)=0
		AND OBD.numOppBizDocsId IS NULL

	DELETE 
		BC 
	FROM 
		BizDocComission BC
	LEFT JOIN
		OpportunityBizDocItems OBDI
	ON
		BC.numOppBizDocItemID = OBDI.numOppBizDocItemID
	WHERE 
		BC.numDomainId=@numDomainID 
		AND ISNULL(BC.numOppBizDocItemID,0) > 0
		AND ISNULL(BC.bitCommisionPaid,0)=0
		AND OBDI.numOppBizDocItemID IS NULL

	-- DELETE UNPAID COMMISSION ENTRIES BETWEEN PAY PERIOD DATE RANGE	
	IF @tintCommissionType = 3 --Sales Order Sub-Total Amount
	BEGIN
		DELETE FROM 
			BizDocComission 
		WHERE 
			numDomainId=@numDomainID 
			AND ISNULL(bitCommisionPaid,0)=0 
			AND numOppItemID IN (SELECT 
									OI.numoppitemtCode
								FROM 
									OpportunityMaster OM
								INNER JOIN
									OpportunityItems OI
								ON
									OM.numOppId = OI.numOppId
								WHERE
									OM.numDomainId = @numDomainID 
									AND OM.tintOppType = 1
									AND OM.tintOppStatus = 1
									AND CAST(DATEADD(MINUTE,@ClientTimeZoneOffset * -1,OM.bintCreatedDate) AS DATE) BETWEEN @dtPayStart AND @dtPayEnd)

		INSERT INTO 
			@TABLEPAID
		SELECT 
			OM.numDivisionID,
			ISNULL(CompanyInfo.numCompanyType,0),
			ISNULL(CompanyInfo.vcProfile,0),
			OM.numAssignedTo,
			OM.numRecOwner,
			OI.numItemCode,
			Item.numItemClassification,
			OM.numOppID,
			OI.numoppitemtCode,
			0,
			0,
			ISNULL(OI.numUnitHour,0),
			(CASE WHEN ISNULL(OM.fltExchangeRate,0)=0 OR ISNULL(OM.fltExchangeRate,0)=1 THEN ISNULL(OI.monTotAmount,0) ELSE ROUND(ISNULL(OI.monTotAmount,0) * OM.fltExchangeRate,2) END),
			0,
			(CASE WHEN ISNULL(OM.fltExchangeRate,0)=0 OR ISNULL(OM.fltExchangeRate,0)=1 THEN ISNULL(OI.monTotAmount,0) ELSE ROUND(ISNULL(OI.monTotAmount,0) * OM.fltExchangeRate,2) END),
			(ISNULL(OI.monVendorCost,0) * dbo.fn_UOMConversion(Item.numPurchaseUnit,Item.numItemCode,@numDomainID,Item.numBaseUnit) * ISNULL(OI.numUnitHour,0)),
			(ISNULL(OI.monAvgCost,0) * ISNULL(OI.numUnitHour,0)),
			OM.numPartner,
			ISNULL(TEMPMatchedPO.numOppId,TEMPPO.numOppId),
			ISNULL(TEMPMatchedPO.numoppitemtCode,TEMPPO.numoppitemtCode),
			ISNULL(ISNULL(TEMPMatchedPO.monPrice,TEMPPO.monPrice),0)
		FROM 
			OpportunityMaster OM
		INNER JOIN
			DivisionMaster 
		ON
			OM.numDivisionID = DivisionMaster.numDivisionID
		INNER JOIN
			CompanyInfo
		ON
			DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
		INNER JOIN
			OpportunityItems OI 
		ON
			OM.numOppId = OI.numOppId
		INNER JOIN
			Item
		ON
			OI.numItemCode = Item.numItemCode
		OUTER APPLY
		(
			SELECT TOP 1
				OMInner.numOppID
				,OMInner.vcPOppName
				,OIInner.numoppitemtCode
				,OIInner.monPrice
			FROM
				SalesOrderLineItemsPOLinking SOLIPL
			INNER JOIN
				OpportunityMaster OMInner
			ON
				SOLIPL.numPurchaseOrderID = OMInner.numOppId
			INNER JOIN
				OpportunityItems OIInner
			ON
				OMInner.numOppId = OIInner.numOppId
				AND SOLIPL.numPurchaseOrderItemID = OIInner.numoppitemtCode
			WHERE
				SOLIPL.numSalesOrderID = OM.numOppId
				AND SOLIPL.numSalesOrderItemID = OI.numoppitemtCode
		) TEMPMatchedPO
		OUTER APPLY
		(
			SELECT TOP 1
				OMInner.numOppID
				,OMInner.vcPOppName
				,OIInner.numoppitemtCode
				,OIInner.monPrice
			FROM
				OpportunityLinking OL
			INNER JOIN
				OpportunityMaster OMInner
			ON
				OL.numChildOppID = OMInner.numOppId
			INNER JOIN
				OpportunityItems OIInner
			ON
				OMInner.numOppId = OIInner.numOppId
			WHERE
				OL.numParentOppID = OM.numOppId
				AND OIInner.numItemCode = OI.numItemCode
				AND ISNULL(OIInner.vcAttrValues,'') = ISNULL(OI.vcAttrValues,'')
		) TEMPPO
		LEFT JOIN
			BizDocComission
		ON
			OM.numOppID = BizDocComission.numOppID
			AND OI.numoppitemtCode = BizDocComission.numOppItemID
			AND ISNULL(bitCommisionPaid,0) = 1
		WHERE
			OM.numDomainId = @numDomainID 
			AND OM.tintOppType = 1
			AND OM.tintOppStatus = 1
			AND CAST(DATEADD(MINUTE,@ClientTimeZoneOffset * -1,OM.bintCreatedDate) AS DATE) BETWEEN @dtPayStart AND @dtPayEnd
			AND BizDocComission.numComissionID IS NULL
			AND OI.numItemCode NOT IN ( @numDiscountServiceItemID )  /*DO NOT INCLUDE DISCOUNT ITEM*/
			AND 1 = (
						CASE 
							WHEN OI.numItemCode = @numShippingServiceItemID 
							THEN 
								CASE 
									WHEN ISNULL(@bitIncludeTaxAndShippingInCommission,0) = 1
									THEN	
										1
									ELSE	
										0
								END
							ELSE 
								1 
						END
					)/*CONDITION TO INCLUDE/EXCLUDE INCLUDE SHIPPING ITEM */
	END
	ELSE IF @tintCommissionType = 2 --Invoice Sub-Total Amount (Paid or Unpaid)
	BEGIN
		DELETE FROM 
			BizDocComission 
		WHERE 
			numDomainId=@numDomainID
			AND ISNULL(numOppBizDocID,0) = 0
			AND ISNULL(bitCommisionPaid,0)=0 
			AND numOppItemID IN (SELECT 
									OI.numoppitemtCode
								FROM 
									OpportunityMaster OM
								INNER JOIN
									OpportunityItems OI
								ON
									OM.numOppId = OI.numOppId
								WHERE
									OM.numDomainId = @numDomainID 
									AND OM.tintOppType = 1
									AND OM.tintOppStatus = 1
									AND CAST(DATEADD(MINUTE,@ClientTimeZoneOffset * -1,OM.bintCreatedDate) AS DATE) BETWEEN @dtPayStart AND @dtPayEnd)

		DELETE FROM 
			BizDocComission 
		WHERE 
			numDomainId=@numDomainID 
			AND ISNULL(bitCommisionPaid,0)=0 
			AND numOppBizDocItemID IN (SELECT 
											ISNULL(numOppBizDocItemID,0)
										FROM 
											OpportunityBizDocs OppBiz
										INNER JOIN
											OpportunityMaster OppM
										ON
											OppBiz.numOppID = OppM.numOppID
										INNER JOIN
											OpportunityBizDocItems OppBizItems
										ON
											OppBiz.numOppBizDocsId = OppBizItems.numOppBizDocID
										OUTER APPLY
										(
											SELECT 
												MAX(DM.dtDepositDate) dtDepositDate
											FROM 
												DepositMaster DM 
											JOIN 
												dbo.DepositeDetails DD
											ON 
												DM.numDepositId=DD.numDepositID 
											WHERE 
												DM.tintDepositePage=2 
												AND DM.numDomainId=@numDomainId
												AND DD.numOppID=OppBiz.numOppID 
												AND DD.numOppBizDocsID =OppBiz.numOppBizDocsID
										) TempDepositMaster
										WHERE
											OppM.numDomainId = @numDomainID 
											AND OppM.tintOppType = 1
											AND OppM.tintOppStatus = 1
											AND OppBiz.bitAuthoritativeBizDocs = 1
											AND ((TempDepositMaster.dtDepositDate IS NOT NULL AND (CAST(TempDepositMaster.dtDepositDate AS DATE) BETWEEN @dtPayStart AND @dtPayEnd)) OR CAST(DATEADD(MINUTE,@ClientTimeZoneOffset * -1,OppBiz.dtCreatedDate) AS DATE) BETWEEN @dtPayStart AND @dtPayEnd))

		INSERT INTO 
			@TABLEPAID
		SELECT 
			OM.numDivisionID,
			ISNULL(CompanyInfo.numCompanyType,0),
			ISNULL(CompanyInfo.vcProfile,0),
			OM.numAssignedTo,
			OM.numRecOwner,
			OppBizItems.numItemCode,
			Item.numItemClassification,
			OM.numOppID,
			OppMItems.numoppitemtCode,
			OppBizItems.numOppBizDocID,
			OppBizItems.numOppBizDocItemID,
			ISNULL(OppBizItems.numUnitHour,0),
			(CASE WHEN ISNULL(OM.fltExchangeRate,0)=0 OR ISNULL(OM.fltExchangeRate,0)=1 THEN ISNULL(OppBizItems.monTotAmount,0) ELSE ROUND(ISNULL(OppBizItems.monTotAmount,0) * OM.fltExchangeRate,2) END),
			(CASE WHEN ISNULL(OM.fltExchangeRate,0)=0 OR ISNULL(OM.fltExchangeRate,0)=1 THEN ISNULL(OppBizItems.monTotAmount,0) ELSE ROUND(ISNULL(OppBizItems.monTotAmount,0) * OM.fltExchangeRate,2) END),
			(CASE WHEN ISNULL(OM.fltExchangeRate,0)=0 OR ISNULL(OM.fltExchangeRate,0)=1 THEN ISNULL(OppMItems.monTotAmount,0) ELSE ROUND(ISNULL(OppMItems.monTotAmount,0) * OM.fltExchangeRate,2) END),
			(ISNULL(OppMItems.monVendorCost,0) * dbo.fn_UOMConversion(Item.numPurchaseUnit,Item.numItemCode,@numDomainID,Item.numBaseUnit) * ISNULL(OppBizItems.numUnitHour,0)),
			(ISNULL(OppMItems.monAvgCost,0) * ISNULL(OppBizItems.numUnitHour,0)),
			OM.numPartner,
			ISNULL(TEMPMatchedPO.numOppId,TEMPPO.numOppId),
			ISNULL(TEMPMatchedPO.numoppitemtCode,TEMPPO.numoppitemtCode),
			ISNULL(ISNULL(TEMPMatchedPO.monPrice,TEMPPO.monPrice),0)
		FROM 
			OpportunityMaster OM
		INNER JOIN	
			OpportunityBizDocs OBD
		ON
			OM.numOppId = OBD.numOppId
		INNER JOIN
			DivisionMaster 
		ON
			OM.numDivisionID = DivisionMaster.numDivisionID
		INNER JOIN
			CompanyInfo
		ON
			DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
		INNER JOIN
			OpportunityBizDocItems OppBizItems
		ON
			OBD.numOppBizDocsId = OppBizItems.numOppBizDocID
		INNER JOIN
			OpportunityItems OppMItems 
		ON
			OppBizItems.numOppItemID = OppMItems.numoppitemtCode
		INNER JOIN
			Item
		ON
			OppBizItems.numItemCode = Item.numItemCode
		OUTER APPLY
		(
			SELECT TOP 1
				OMInner.numOppID
				,OMInner.vcPOppName
				,OIInner.numoppitemtCode
				,OIInner.monPrice
			FROM
				SalesOrderLineItemsPOLinking SOLIPL
			INNER JOIN
				OpportunityMaster OMInner
			ON
				SOLIPL.numPurchaseOrderID = OMInner.numOppId
			INNER JOIN
				OpportunityItems OIInner
			ON
				OMInner.numOppId = OIInner.numOppId
				AND SOLIPL.numPurchaseOrderItemID = OIInner.numoppitemtCode
			WHERE
				SOLIPL.numSalesOrderID = OM.numOppId
				AND SOLIPL.numSalesOrderItemID = OppMItems.numoppitemtCode
		) TEMPMatchedPO
		OUTER APPLY
		(
			SELECT TOP 1
				OMInner.numOppID
				,OMInner.vcPOppName
				,OIInner.numoppitemtCode
				,OIInner.monPrice
			FROM
				OpportunityLinking OL
			INNER JOIN
				OpportunityMaster OMInner
			ON
				OL.numChildOppID = OMInner.numOppId
			INNER JOIN
				OpportunityItems OIInner
			ON
				OMInner.numOppId = OIInner.numOppId
			WHERE
				OL.numParentOppID = OM.numOppId
				AND OIInner.numItemCode = OppMItems.numItemCode
				AND ISNULL(OIInner.vcAttrValues,'') = ISNULL(OppMItems.vcAttrValues,'')
		) TEMPPO
		LEFT JOIN
			BizDocComission BDC1
		ON
			OM.numOppID = BDC1.numOppID
			AND OppMItems.numoppitemtCode = BDC1.numOppItemID
			AND ISNULL(bitCommisionPaid,0) = 1
		LEFT JOIN
			BizDocComission BDC2
		ON
			OM.numOppID = BDC2.numOppID
			AND OppMItems.numoppitemtCode = BDC2.numOppItemID
			AND ISNULL(BDC2.bitCommisionPaid,0) = 1
			AND OppBizItems.numOppBizDocID = BDC2.numOppBizDocId 
			AND OppBizItems.numOppBizDocItemID =  BDC2.numOppBizDocItemID
		WHERE
			OM.numDomainId = @numDomainID 
			AND OM.tintOppType = 1
			AND OM.tintOppStatus = 1
			AND OBD.bitAuthoritativeBizDocs = 1
			AND CAST(DATEADD(MINUTE,@ClientTimeZoneOffset * -1,OBD.dtCreatedDate) AS DATE) BETWEEN @dtPayStart AND @dtPayEnd
			AND OppBizItems.numItemCode NOT IN ( @numDiscountServiceItemID )  /*DO NOT INCLUDE DISCOUNT ITEM*/
			AND BDC1.numComissionID IS NULL
			AND BDC2.numComissionID IS NULL
			AND 1 = (
						CASE 
							WHEN OppBizItems.numItemCode = @numShippingServiceItemID 
							THEN 
								CASE 
									WHEN ISNULL(@bitIncludeTaxAndShippingInCommission,0) = 1
									THEN	
										1
									ELSE	
										0
								END
							ELSE 
								1 
						END
					)/*CONDITION TO INCLUDE/EXCLUDE INCLUDE SHIPPING ITEM */
	END
	ELSE
	BEGIN
		DELETE FROM 
			BizDocComission 
		WHERE 
			numDomainId=@numDomainID
			AND ISNULL(numOppBizDocID,0) = 0
			AND ISNULL(bitCommisionPaid,0)=0 
			AND numOppItemID IN (SELECT 
									OI.numoppitemtCode
								FROM 
									OpportunityMaster OM
								INNER JOIN
									OpportunityItems OI
								ON
									OM.numOppId = OI.numOppId
								WHERE
									OM.numDomainId = @numDomainID 
									AND OM.tintOppType = 1
									AND OM.tintOppStatus = 1
									AND CAST(DATEADD(MINUTE,@ClientTimeZoneOffset * -1,OM.bintCreatedDate) AS DATE) BETWEEN @dtPayStart AND @dtPayEnd)

		DELETE FROM 
			BizDocComission 
		WHERE 
			numDomainId=@numDomainID 
			AND ISNULL(bitCommisionPaid,0)=0 
			AND numOppBizDocItemID IN (SELECT 
											ISNULL(numOppBizDocItemID,0)
										FROM 
											OpportunityBizDocs OppBiz
										INNER JOIN
											OpportunityMaster OppM
										ON
											OppBiz.numOppID = OppM.numOppID
										INNER JOIN
											OpportunityBizDocItems OppBizItems
										ON
											OppBiz.numOppBizDocsId = OppBizItems.numOppBizDocID
										OUTER APPLY
										(
											SELECT 
												MAX(DM.dtDepositDate) dtDepositDate
											FROM 
												DepositMaster DM 
											JOIN 
												dbo.DepositeDetails DD
											ON 
												DM.numDepositId=DD.numDepositID 
											WHERE 
												DM.tintDepositePage=2 
												AND DM.numDomainId=@numDomainId
												AND DD.numOppID=OppBiz.numOppID 
												AND DD.numOppBizDocsID =OppBiz.numOppBizDocsID
										) TempDepositMaster
										WHERE
											OppM.numDomainId = @numDomainID 
											AND OppM.tintOppType = 1
											AND OppM.tintOppStatus = 1
											AND OppBiz.bitAuthoritativeBizDocs = 1
											AND ((TempDepositMaster.dtDepositDate IS NOT NULL AND (CAST(TempDepositMaster.dtDepositDate AS DATE) BETWEEN @dtPayStart AND @dtPayEnd)) OR CAST(DATEADD(MINUTE,@ClientTimeZoneOffset * -1,OppBiz.dtCreatedDate) AS DATE) BETWEEN @dtPayStart AND @dtPayEnd))

		INSERT INTO 
			@TABLEPAID
		SELECT 
			OppM.numDivisionID,
			ISNULL(CompanyInfo.numCompanyType,0),
			ISNULL(CompanyInfo.vcProfile,0),
			OppM.numAssignedTo,
			OppM.numRecOwner,
			OppBizItems.numItemCode,
			Item.numItemClassification,
			OppM.numOppID,
			OppMItems.numoppitemtCode,
			OppBizItems.numOppBizDocID,
			OppBizItems.numOppBizDocItemID,
			ISNULL(OppBizItems.numUnitHour,0),
			(CASE WHEN ISNULL(OppM.fltExchangeRate,0)=0 OR ISNULL(OppM.fltExchangeRate,0)=1 THEN ISNULL(OppBizItems.monTotAmount,0) ELSE ROUND(ISNULL(OppBizItems.monTotAmount,0) * OppM.fltExchangeRate,2) END),
			(CASE WHEN ISNULL(OppM.fltExchangeRate,0)=0 OR ISNULL(OppM.fltExchangeRate,0)=1 THEN ISNULL(OppBizItems.monTotAmount,0) ELSE ROUND(ISNULL(OppBizItems.monTotAmount,0) * OppM.fltExchangeRate,2) END),
			(CASE WHEN ISNULL(OppM.fltExchangeRate,0)=0 OR ISNULL(OppM.fltExchangeRate,0)=1 THEN ISNULL(OppMItems.monTotAmount,0) ELSE ROUND(ISNULL(OppMItems.monTotAmount,0) * OppM.fltExchangeRate,2) END),
			(ISNULL(OppMItems.monVendorCost,0) * dbo.fn_UOMConversion(Item.numPurchaseUnit,Item.numItemCode,@numDomainID,Item.numBaseUnit) * ISNULL(OppBizItems.numUnitHour,0)),
			(ISNULL(OppMItems.monAvgCost,0) * ISNULL(OppBizItems.numUnitHour,0)),
			OppM.numPartner,
			ISNULL(TEMPMatchedPO.numOppId,TEMPPO.numOppId),
			ISNULL(TEMPMatchedPO.numoppitemtCode,TEMPPO.numoppitemtCode),
			ISNULL(ISNULL(TEMPMatchedPO.monPrice,TEMPPO.monPrice),0)
		FROM 
			OpportunityBizDocs OppBiz
		INNER JOIN
			OpportunityMaster OppM
		ON
			OppBiz.numOppID = OppM.numOppID
		INNER JOIN
			DivisionMaster 
		ON
			OppM.numDivisionID = DivisionMaster.numDivisionID
		INNER JOIN
			CompanyInfo
		ON
			DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
		INNER JOIN
			OpportunityBizDocItems OppBizItems
		ON
			OppBiz.numOppBizDocsId = OppBizItems.numOppBizDocID
		INNER JOIN
			OpportunityItems OppMItems 
		ON
			OppBizItems.numOppItemID = OppMItems.numoppitemtCode
		INNER JOIN
			Item
		ON
			OppBizItems.numItemCode = Item.numItemCode
		OUTER APPLY
		(
			SELECT TOP 1
				OMInner.numOppID
				,OMInner.vcPOppName
				,OIInner.numoppitemtCode
				,OIInner.monPrice
			FROM
				SalesOrderLineItemsPOLinking SOLIPL
			INNER JOIN
				OpportunityMaster OMInner
			ON
				SOLIPL.numPurchaseOrderID = OMInner.numOppId
			INNER JOIN
				OpportunityItems OIInner
			ON
				OMInner.numOppId = OIInner.numOppId
				AND SOLIPL.numPurchaseOrderItemID = OIInner.numoppitemtCode
			WHERE
				SOLIPL.numSalesOrderID = OppM.numOppId
				AND SOLIPL.numSalesOrderItemID = OppMItems.numoppitemtCode
		) TEMPMatchedPO
		OUTER APPLY
		(
			SELECT TOP 1
				OMInner.numOppID
				,OMInner.vcPOppName
				,OIInner.numoppitemtCode
				,OIInner.monPrice
			FROM
				OpportunityLinking OL
			INNER JOIN
				OpportunityMaster OMInner
			ON
				OL.numChildOppID = OMInner.numOppId
			INNER JOIN
				OpportunityItems OIInner
			ON
				OMInner.numOppId = OIInner.numOppId
			WHERE
				OL.numParentOppID = OppM.numOppId
				AND OIInner.numItemCode = OppMItems.numItemCode
				AND ISNULL(OIInner.vcAttrValues,'') = ISNULL(OppMItems.vcAttrValues,'')
		) TEMPPO
		LEFT JOIN
			BizDocComission BDC1
		ON
			OppM.numOppID = BDC1.numOppID
			AND OppMItems.numoppitemtCode = BDC1.numOppItemID
			AND ISNULL(bitCommisionPaid,0) = 1
		LEFT JOIN
			BizDocComission BDC2
		ON
			OppM.numOppID = BDC2.numOppID
			AND OppMItems.numoppitemtCode = BDC2.numOppItemID
			AND ISNULL(BDC2.bitCommisionPaid,0) = 1
			AND OppBizItems.numOppBizDocID = BDC2.numOppBizDocId 
			AND OppBizItems.numOppBizDocItemID =  BDC2.numOppBizDocItemID
		OUTER APPLY
		(
			SELECT 
				MAX(DM.dtDepositDate) dtDepositDate
			FROM 
				DepositMaster DM 
			JOIN 
				dbo.DepositeDetails DD
			ON 
				DM.numDepositId=DD.numDepositID 
			WHERE 
				DM.tintDepositePage=2 
				AND DM.numDomainId=@numDomainId
				AND DD.numOppID=OppBiz.numOppID 
				AND DD.numOppBizDocsID =OppBiz.numOppBizDocsID
		) TempDepositMaster
		WHERE
			OppM.numDomainId = @numDomainID 
			AND OppM.tintOppType = 1
			AND OppM.tintOppStatus = 1
			AND OppBiz.bitAuthoritativeBizDocs = 1
			AND BDC1.numComissionID IS NULL
			AND BDC2.numComissionID IS NULL
			AND ISNULL(OppBiz.monAmountPaid,0) >= OppBiz.monDealAmount
			AND (TempDepositMaster.dtDepositDate IS NOT NULL AND (CAST(TempDepositMaster.dtDepositDate AS DATE) BETWEEN @dtPayStart AND @dtPayEnd))
			AND OppBizItems.numItemCode NOT IN ( @numDiscountServiceItemID )  /*DO NOT INCLUDE DISCOUNT ITEM*/
			AND 1 = (
						CASE 
							WHEN OppBizItems.numItemCode = @numShippingServiceItemID 
							THEN 
								CASE 
									WHEN ISNULL(@bitIncludeTaxAndShippingInCommission,0) = 1
									THEN	
										1
									ELSE	
										0
								END
							ELSE 
								1 
						END
					)/*CONDITION TO INCLUDE/EXCLUDE INCLUDE SHIPPING ITEM */
	END	

	--GET COMMISSION RULE OF DOMAIN
	DECLARE @TempCommissionRule TABLE
	(
		ID INT IDENTITY(1,1),
		numComRuleID NUMERIC(18,0),
		tintComBasedOn TINYINT,
		tintComType TINYINT,
		tintComAppliesTo TINYINT,
		tintComOrgType TINYINT,
		tintAssignTo TINYINT
	)

	INSERT INTO
		@TempCommissionRule
	SELECT
		CR.numComRuleID,
		CR.tintComBasedOn,
		CR.tintComType,
		CR.tintComAppliesTo,
		CR.tintComOrgType,
		CR.tintAssignTo
	FROM
		CommissionRules CR
	LEFT JOIN
		PriceBookPriorities PBP
	ON	
		CR.tintComAppliesTo = PBP.Step2Value 
		AND CR.tintComOrgType = PBP.Step3Value
	WHERE
		CR.numDomainID = @numDomainID
		AND ISNULL(PBP.[Priority],0) > 0
	ORDER BY
		ISNULL(PBP.[Priority],0) ASC

	-- LOOP ALL COMMISSION RULES
	DECLARE @i INT = 1
	DECLARE @COUNT INT = 0

	SELECT @COUNT=COUNT(*) FROM @TempCommissionRule

	DECLARE @TEMPITEM TABLE
	(
		ID INT,
		UniqueID INT,
		numUserCntID NUMERIC(18,0),
		numOppID NUMERIC(18,0),
		numOppBizDocID NUMERIC(18,0),
		numOppBizDocItemID NUMERIC(18,0),
		numItemCode NUMERIC(18,0),
		numUnitHour FLOAT,
		monTotAmount DECIMAL(20,5),
		monVendorCost DECIMAL(20,5),
		monAvgCost DECIMAL(20,5),
		numOppItemID NUMERIC(18,0),
		monInvoiceSubTotal DECIMAL(20,5),
		monOrderSubTotal DECIMAL(20,5),
		numPOID NUMERIC(18,0),
		numPOItemID NUMERIC(18,0),
		monPOCost DECIMAL(20,5)
	)

	WHILE @i <= @COUNT
	BEGIN
		DECLARE @numComRuleID NUMERIC(18,0)
		DECLARE @tintComBasedOn TINYINT
		DECLARE @tintComAppliesTo TINYINT
		DECLARE @tintComOrgType TINYINT
		DECLARE @tintComType TINYINT
		DECLARE @tintAssignTo TINYINT
		DECLARE @numTotalAmount AS DECIMAL(20,5)
		DECLARE @numTotalUnit AS FLOAT

		--CLEAR PREVIOUS VALUES FROM TEMP TABLE
		DELETE FROM @TEMPITEM

		--FETCH COMMISSION RULE
		SELECT @numComRuleID=numComRuleID,@tintComBasedOn=tintComBasedOn,@tintComType=tintComType,@tintComAppliesTo=tintComAppliesTo,@tintComOrgType=tintComOrgType,@tintAssignTo=tintAssignTo FROM @TempCommissionRule WHERE ID=@i

		--FIND ALL BIZ DOC ITEMS WHERE COMMISSION IS NOT PAID AND CURRENT RULE ITEMS, ORGANIZATION AND ASSINGED TO FILTER CAN BE APPLIED
		INSERT INTO
			@TEMPITEM
		SELECT
			ROW_NUMBER() OVER (ORDER BY ID),
			ID,
			(CASE @tintAssignTo WHEN 1 THEN numAssignedTo WHEN 2 THEN numRecordOwner ELSE numPartner END),
			numOppID,
			numOppBizDocID,
			numOppBizDocItemID,
			numItemCode,
			numUnitHour,
			monTotAmount,
			monVendorCost,
			monAvgCost,
			numOppItemID,
			monInvoiceSubTotal,
			monOrderSubTotal,
			numPOID,
			numPOItemID,
			monPOCost
		FROM
			@TABLEPAID
		WHERE
			1 = (CASE @tintComAppliesTo
					--SPECIFIC ITEMS
					WHEN 1 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleItems WHERE numComRuleID = @numComRuleID AND ISNULL(numValue,0) = numItemCode) > 0 THEN 1 ELSE 0 END
					-- ITEM WITH SPECIFIC CLASSIFICATIONS
					WHEN 2 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleItems WHERE numComRuleID = @numComRuleID AND ISNULL(numValue,0) = numItemClassification) > 0 THEN 1 ELSE 0 END
					-- ALL ITEMS
					ELSE 1
				END)
			AND 1 = (CASE @tintComOrgType
						-- SPECIFIC ORGANIZATION
						WHEN 1 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleOrganization WHERE numComRuleID = @numComRuleID AND ISNULL(numValue,0) = numDivisionID) > 0 THEN 1 ELSE 0 END
						-- ORGANIZATION WITH SPECIFIC RELATIONSHIP AND PROFILE
						WHEN 2 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleOrganization WHERE numComRuleID = @numComRuleID AND ISNULL(numValue,0) = numRelationship AND ISNULL(numProfile,0) = numProfile) > 0 THEN 1 ELSE 0 END
						-- ALL ORGANIZATIONS
						ELSE 1
					END)
			AND 1 = (CASE @tintAssignTo 
						-- ORDER ASSIGNED TO
						WHEN 1 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleContacts WHERE numComRuleID = @numComRuleID AND numValue=numAssignedTo) > 0 THEN 1 ELSE 0 END
						-- ORDER OWNER
						WHEN 2 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleContacts WHERE numComRuleID = @numComRuleID AND numValue=numRecordOwner) > 0 THEN 1 ELSE 0 END
						-- Order Partner
						WHEN 3 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleContacts WHERE numComRuleID = @numComRuleID AND numValue=numPartner) > 0 THEN 1 ELSE 0 END
						-- NO OPTION SELECTED IN RULE
						ELSE 0
					END)


		--CALCULATE COMMISSION AND STORE IN TEMPARORY TABLE
		INSERT INTO @TempBizCommission 
		(
			numUserCntID,
			numOppID,
			numOppBizDocID,
			numOppBizDocItemID,
			monCommission,
			numComRuleID,
			tintComType,
			tintComBasedOn,
			decCommission,
			tintAssignTo,
			numOppItemID,
			monInvoiceSubTotal,
			monOrderSubTotal
		)
		SELECT
			numUserCntID,
			numOppID,
			numOppBizDocID,
			numOppBizDocItemID,
			CASE @tintComType 
				WHEN 1 --PERCENT
					THEN 
						CASE 
							WHEN ISNULL(@bitCommissionBasedOn,0) = 1
							THEN
								(CASE 
									WHEN numPOItemID IS NOT NULL
									THEN (ISNULL(monTotAmount,0) - ISNULL(monPOCost,0)) * (T2.decCommission / CAST(100 AS FLOAT))
									ELSE 
										CASE @tintCommissionBasedOn
											--ITEM GROSS PROFIT (VENDOR COST)
											WHEN 1 THEN (ISNULL(monTotAmount,0) - ISNULL(monVendorCost,0)) * (T2.decCommission / CAST(100 AS FLOAT))
											--ITEM GROSS PROFIT (AVERAGE COST)
											WHEN 2 THEN(ISNULL(monTotAmount,0) - ISNULL(monAvgCost,0)) * (T2.decCommission / CAST(100 AS FLOAT))
										END
								END)
							ELSE
								monTotAmount * (T2.decCommission / CAST(100 AS FLOAT))
						END
				ELSE  --FLAT
					T2.decCommission
			END,
			@numComRuleID,
			@tintComType,
			@tintComBasedOn,
			T2.decCommission,
			@tintAssignTo,
			numOppItemID,
			monInvoiceSubTotal,
			monOrderSubTotal
		FROM 
			@TEMPITEM AS T1
		CROSS APPLY
		(
			SELECT TOP 1 
				ISNULL(decCommission,0) AS decCommission
			FROM 
				CommissionRuleDtl 
			WHERE 
				numComRuleID=@numComRuleID 
				AND ISNULL(decCommission,0) > 0
				AND 1 = (CASE 
						WHEN @tintComBasedOn = 1 --BASED ON AMOUNT SOLD 
						THEN (CASE WHEN (T1.monTotAmount BETWEEN intFrom AND intTo) THEN 1 ELSE 0 END)
						WHEN @tintComBasedOn = 2 --BASED ON UNITS SOLD
						THEN (CASE WHEN (T1.numUnitHour BETWEEN intFrom AND intTo) THEN 1 ELSE 0 END)
						ELSE 0
						END)
		) AS T2
		WHERE
			(CASE 
				WHEN ISNULL(@bitCommissionBasedOn,0) = 1
				THEN
					(CASE 
						WHEN numPOItemID IS NOT NULL
						THEN (ISNULL(monTotAmount,0) - ISNULL(monPOCost,0)) * (T2.decCommission / CAST(100 AS FLOAT))
						ELSE
							CASE @tintCommissionBasedOn
								--ITEM GROSS PROFIT (VENDOR COST)
								WHEN 1 THEN (ISNULL(monTotAmount,0) - ISNULL(monVendorCost,0)) * (T2.decCommission / CAST(100 AS FLOAT))
								--ITEM GROSS PROFIT (AVERAGE COST)
								WHEN 2 THEN(ISNULL(monTotAmount,0) - ISNULL(monAvgCost,0)) * (T2.decCommission / CAST(100 AS FLOAT))
							END
					END)
				ELSE
					monTotAmount * (T2.decCommission / CAST(100 AS FLOAT))
			END) > 0 
			AND (
					SELECT 
						COUNT(*) 
					FROM 
						@TempBizCommission TempBDC
					WHERE 
						TempBDC.numUserCntID=T1.numUserCntID 
						AND TempBDC.numOppID=T1.numOppID 
						AND TempBDC.numOppBizDocID = T1.numOppBizDocID 
						AND TempBDC.numOppBizDocItemID = T1.numOppBizDocItemID
						AND TempBDC.numOppItemID = T1.numOppItemID 
						AND tintAssignTo = @tintAssignTo
				) = 0

		SET @i = @i + 1

		SET @numComRuleID = 0
		SET @tintComBasedOn = 0
		SET @tintComAppliesTo = 0
		SET @tintComOrgType = 0
		SET @tintComType = 0
		SET @tintAssignTo = 0
		SET @numTotalAmount = 0
		SET @numTotalUnit = 0
	END

	--INSERT CALCULATED ENTERIES IN BIZDOCCOMMISSION TABLE
	INSERT INTO BizDocComission
	(	
		numDomainID,
		numUserCntID,
		numOppBizDocId,
		numComissionAmount,
		numOppBizDocItemID,
		numComRuleID,
		tintComType,
		tintComBasedOn,
		decCommission,
		numOppID,
		bitFullPaidBiz,
		numComPayPeriodID,
		tintAssignTo,
		numOppItemID,
		monInvoiceSubTotal,
		monOrderSubTotal,
		bitDomainCommissionBasedOn,
		tintDomainCommissionBasedOn
	)
	SELECT
		@numDomainID,
		numUserCntID,
		numOppBizDocID,
		monCommission,
		numOppBizDocItemID,
		numComRuleID,
		tintComType,
		tintComBasedOn,
		decCommission,
		numOppID,
		1,
		@numComPayPeriodID,
		tintAssignTo,
		numOppItemID,
		monInvoiceSubTotal,
		monOrderSubTotal,
		@bitCommissionBasedOn,
		@tintCommissionBasedOn
	FROM
		@TempBizCommission
END
GO
--Created by anoop jayaraj                                                          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_checkuseratlogin')
DROP PROCEDURE usp_checkuseratlogin
GO
CREATE PROCEDURE [dbo].[USP_CheckUserAtLogin]                                                              
(                                                                                    
	@UserName as varchar(100)='',                                                                        
	@vcPassword as varchar(100)='',
	@vcLinkedinId as varchar(300)='',
	@numDomainID NUMERIC(18,0) = 0
)                                                              
AS             
BEGIN
	DECLARE @listIds VARCHAR(MAX)

	SELECT 
		@listIds = COALESCE(@listIds+',' ,'') + CAST(UDTL.numUserId AS VARCHAR(100)) 
	FROM 
		UnitPriceApprover U
	Join 
		Domain D                              
	on 
		D.numDomainID=U.numDomainID
	Join  
		Subscribers S                            
	on 
		S.numTargetDomainID=D.numDomainID
	LEFT JOIN 
		UserMaster As UDTL
	ON
		U.numUserID=UDTL.numUserDetailId
	WHERE 
		1 = (CASE 
				WHEN @vcLinkedinId='N' THEN (CASE WHEN UDTL.vcEmailID=@UserName and UDTL.vcPassword=@vcPassword THEN 1 ELSE 0 END)
				ELSE (CASE WHEN UDTL.vcLinkedinId=@vcLinkedinId THEN 1 ELSE 0 END)
			END)
       

	DECLARE @bitIsBusinessPortalUser AS BIT = 0
	DECLARE @numUserCount INT = 0 

	IF ISNULL(@numDomainID,0) = 0
	BEGIN
		SET @numUserCount = ISNULL((SELECT 
									COUNT(*)
								FROM 
									UserMaster U                              
								JOIN 
									Domain D                              
								ON 
									D.numDomainID=U.numDomainID
								JOIN 
									Subscribers S                            
								ON 
									S.numTargetDomainID=D.numDomainID
								WHERE 
									1 = (CASE 
											WHEN @vcLinkedinId='N' 
											THEN (CASE WHEN vcEmailID=@UserName and vcPassword=@vcPassword THEN 1 ELSE 0 END)
											ELSE (CASE WHEN vcLinkedinId=@vcLinkedinId THEN 1 ELSE 0 END)
										END) 
									AND bitActive=1),0)

		SET @bitIsBusinessPortalUser = 0

		/*check subscription validity */
		IF EXISTS(SELECT 
					*
				FROM 
					UserMaster U                              
				JOIN 
					Domain D                              
				ON 
					D.numDomainID=U.numDomainID
				JOIN 
					Subscribers S                            
				ON 
					S.numTargetDomainID=D.numDomainID
				WHERE 
					1 = (CASE 
							WHEN @vcLinkedinId='N' THEN (CASE WHEN vcEmailID=@UserName and vcPassword=@vcPassword THEN 1 ELSE 0 END)
							ELSE (CASE WHEN vcLinkedinId=@vcLinkedinId THEN 1 ELSE 0 END)
						END) 
					AND bitActive=1 
					AND getutcdate() > dtSubEndDate)
		BEGIN
			RAISERROR('SUB_RENEW',16,1)
			RETURN;
		END
	END
	ELSE
	BEGIN
		SET @numUserCount = ISNULL((SELECT 
										COUNT(*)
									FROM 
										AdditionalContactsInformation A                                  
									INNER JOIN 
										ExtranetAccountsDtl E                                  
									ON 
										A.numContactID=E.numContactID
									INNER JOIN
										ExtarnetAccounts EA
									ON
										E.numExtranetID = EA.numExtranetID
									WHERE 
										A.numDomainID = @numDomainID
										AND EA.numDomainID = @numDomainID
										AND (E.vcUserName=@UserName OR vcemail=@UserName) 
										AND (vcPassword=@vcPassword) 
										AND bitPartnerAccess=1),0)

		/*check subscription validity */
		IF EXISTS(SELECT 
						*
					FROM 
						AdditionalContactsInformation A                                  
					INNER JOIN 
						ExtranetAccountsDtl E                                  
					ON 
						A.numContactID=E.numContactID
					INNER JOIN
						ExtarnetAccounts EA
					ON
						E.numExtranetID = EA.numExtranetID
					INNER JOIN
						Domain D
					ON
						EA.numDomainID = D.numDomainId
					INNER JOIN 
						Subscribers S                            
					ON 
						S.numTargetDomainID=D.numDomainID
					WHERE 
						A.numDomainID = @numDomainID
						AND EA.numDomainID = @numDomainID
						AND (E.vcUserName=@UserName OR vcemail=@UserName) 
						AND (vcPassword=@vcPassword) 
						AND bitPartnerAccess=1 
						AND bitActive=1 
						AND getutcdate() > dtSubEndDate)
		BEGIN
			RAISERROR('SUB_RENEW',16,1)
			RETURN;
		END

		SET @bitIsBusinessPortalUser = 1
	END

	IF @numUserCount > 1
	BEGIN
		RAISERROR('MULTIPLE_RECORDS_WITH_SAME_EMAIL',16,1)
		RETURN;
	END   

	IF @bitIsBusinessPortalUser = 1
	BEGIN
		SELECT TOP 1 
			0 numUserID
			,EAD.numExtranetDtlID
			,A.numContactId AS numUserDetailId
			,D.numCost
			,isnull(A.vcEmail,'') vcEmailID
			,'' vcEmailAlias
			,'' vcEmailAliasPassword
			,isnull(numGroupID,0) numGroupID
			,1 AS bitActivateFlag
			,'' vcMailNickName
			,'' txtSignature
			,isnull(EAD.numDomainID,0) numDomainID
			,isnull(EA.numDivisionID,0) numDivisionID
			,isnull(vcCompanyName,'') vcCompanyName
			,0 bitExchangeIntegration
			,0 bitAccessExchange
			,'' vcExchPath
			,'' vcExchDomain
			,'' vcExchUserName
			,isnull(vcFirstname,'') + ' ' + isnull(vcLastName,'') as ContactName
			,'' as vcExchPassword
			,tintCustomPagingRows
			,vcDateFormat
			,numDefCountry
			,tintComposeWindow
			,dateadd(day,-sintStartDate,getutcdate()) as StartDate
			,dateadd(day,sintNoofDaysInterval,dateadd(day,-sintStartDate,getutcdate())) as EndDate
			,tintAssignToCriteria
			,bitIntmedPage
			,tintFiscalStartMonth
			,numAdminID
			,isnull(A.numTeam,0) numTeam
			,isnull(D.vcCurrency,'') as vcCurrency
			,isnull(D.numCurrencyID,0) as numCurrencyID, 
			isnull(D.bitMultiCurrency,0) as bitMultiCurrency,              
			bitTrial,isnull(tintChrForComSearch,0) as tintChrForComSearch,  
			isnull(tintChrForItemSearch,0) as tintChrForItemSearch,              
			'' as vcSMTPServer  ,          
			0 as numSMTPPort      
			,0 bitSMTPAuth    
			,'' vcSmtpPassword    
			,0 bitSMTPSSL
			,0 bitSMTPServer,       
			0 bitImapIntegration, isnull(charUnitSystem,'E')   UnitSystem, datediff(day,getutcdate(),dtSubEndDate) as NoOfDaysLeft,
			ISNULL(bitCreateInvoice,0) bitCreateInvoice,
			ISNULL([numDefaultSalesBizDocId],0) numDefaultSalesBizDocId,
			ISNULL([numDefaultPurchaseBizDocId],0) numDefaultPurchaseBizDocId,
			vcDomainName,
			S.numSubscriberID,
			D.[tintBaseTax],
			'' ProfilePic,
			ISNULL(D.[numShipCompany],0) numShipCompany,
			ISNULL(D.[bitEmbeddedCost],0) bitEmbeddedCost,
			(Select isnull(numAuthoritativeSales,0) From AuthoritativeBizDocs Where numDomainId=D.numDomainID) numAuthoritativeSales,
			(Select isnull(numAuthoritativePurchase,0) From AuthoritativeBizDocs Where numDomainId=D.numDomainID) numAuthoritativePurchase,
			ISNULL(D.[numDefaultSalesOppBizDocId],0) numDefaultSalesOppBizDocId,
			ISNULL(tintDecimalPoints,2) tintDecimalPoints,
			ISNULL(D.tintBillToForPO,0) tintBillToForPO, 
			ISNULL(D.tintShipToForPO,0) tintShipToForPO,
			ISNULL(D.tintOrganizationSearchCriteria,0) tintOrganizationSearchCriteria,
			ISNULL(D.tintLogin,0) tintLogin,
			ISNULL(S.intFullUserConcurrency,0) intFullUserConcurrency,
			ISNULL(bitDocumentRepositary,0) bitDocumentRepositary,
			ISNULL(D.vcPortalName,'') vcPortalName,
			(SELECT COUNT(*) FROM dbo.Subscribers WHERE getutcdate() between dtSubStartDate and dtSubEndDate and bitActive=1),
			ISNULL(D.bitGtoBContact,0) bitGtoBContact,
			ISNULL(D.bitBtoGContact,0) bitBtoGContact,
			ISNULL(D.bitGtoBCalendar,0) bitGtoBCalendar,
			ISNULL(D.bitBtoGCalendar,0) bitBtoGCalendar,
			ISNULL(bitExpenseNonInventoryItem,0) bitExpenseNonInventoryItem,
			ISNULL(D.bitInlineEdit,0) bitInlineEdit,
			0 numDefaultClass,
			ISNULL(D.bitRemoveVendorPOValidation,0) bitRemoveVendorPOValidation,
			ISNULL(D.bitTrakDirtyForm,1) bitTrakDirtyForm,
			isnull(D.tintBaseTaxOnArea,0) tintBaseTaxOnArea,
			ISNULL(D.bitAmountPastDue,0) bitAmountPastDue,
			CONVERT(DECIMAL(18,2),ISNULL(D.monAmountPastDue,0)) monAmountPastDue,
			ISNULL(D.bitSearchOrderCustomerHistory,0) bitSearchOrderCustomerHistory,
			0 as tintTabEvent,
			isnull(D.bitRentalItem,0) as bitRentalItem,
			isnull(D.numRentalItemClass,0) as numRentalItemClass,
			isnull(D.numRentalHourlyUOM,0) as numRentalHourlyUOM,
			isnull(D.numRentalDailyUOM,0) as numRentalDailyUOM,
			isnull(D.tintRentalPriceBasedOn,0) as tintRentalPriceBasedOn,
			isnull(D.tintCalendarTimeFormat,24) as tintCalendarTimeFormat,
			isnull(D.tintDefaultClassType,0) as tintDefaultClassType,
			0 numDefaultWarehouse,
			ISNULL(D.tintPriceBookDiscount,0) tintPriceBookDiscount,
			ISNULL(D.bitAutoSerialNoAssign,0) bitAutoSerialNoAssign,
			ISNULL(IsEnableProjectTracking,0) AS [IsEnableProjectTracking],
			ISNULL(IsEnableCampaignTracking,0) AS [IsEnableCampaignTracking],
			ISNULL(IsEnableResourceScheduling,0) AS [IsEnableResourceScheduling],
			ISNULL(D.numSOBizDocStatus,0) AS numSOBizDocStatus,
			ISNULL(D.bitAutolinkUnappliedPayment,0) AS bitAutolinkUnappliedPayment,
			ISNULL(numShippingServiceItemID,0) AS [numShippingServiceItemID],
			ISNULL(D.numDiscountServiceItemID,0) AS numDiscountServiceItemID,
			ISNULL(D.tintCommissionType,1) AS tintCommissionType,
			ISNULL(D.bitDiscountOnUnitPrice,0) AS bitDiscountOnUnitPrice,
			ISNULL(D.intShippingImageWidth,0) AS intShippingImageWidth ,
			ISNULL(D.intShippingImageHeight,0) AS intShippingImageHeight,
			ISNULL(D.numTotalInsuredValue,0) AS numTotalInsuredValue,
			ISNULL(D.numTotalCustomsValue,0) AS numTotalCustomsValue,
			ISNULL(D.bitUseBizdocAmount,1) AS [bitUseBizdocAmount],
			ISNULL(D.bitDefaultRateType,1) AS [bitDefaultRateType],
			ISNULL(D.bitIncludeTaxAndShippingInCommission,1) AS [bitIncludeTaxAndShippingInCommission],
			ISNULL(D.[bitLandedCost],0) AS bitLandedCost,
			ISNULL(D.vcLanedCostDefault,'') AS vcLanedCostDefault,
			ISNULL(D.bitMinUnitPriceRule,0) AS bitMinUnitPriceRule,
			ISNULL(@listIds,'') AS vcUnitPriceApprover,
			ISNULL(D.numPODropShipBizDoc,0) AS numPODropShipBizDoc,
			ISNULL(D.numPODropShipBizDocTemplate,0) AS numPODropShipBizDocTemplate,
			ISNULL(D.IsEnableDeferredIncome,0) AS IsEnableDeferredIncome,
			ISNULL(D.bitEnableItemLevelUOM,0) AS bitEnableItemLevelUOM,
			ISNULL(D.bitPurchaseTaxCredit,0) AS bitPurchaseTaxCredit,
			NULL AS bintCreatedDate,
			(CASE WHEN LEN(ISNULL(TempDefaultPage.vcDefaultNavURL,'')) = 0 THEN ISNULL(TempDefaultTab.vcDefaultNavURL,'Items/frmItemList.aspx?Page=All Items&ItemGroup=0') ELSE ISNULL(TempDefaultPage.vcDefaultNavURL,'') END) vcDefaultNavURL,
			ISNULL(D.bitApprovalforTImeExpense,0) AS bitApprovalforTImeExpense,
			ISNULL(D.intTimeExpApprovalProcess,0) AS intTimeExpApprovalProcess,ISNULL(D.bitApprovalforOpportunity,0) AS bitApprovalforOpportunity,
			ISNULL(D.intOpportunityApprovalProcess,0) AS intOpportunityApprovalProcess,
			ISNULL(D.numDefaultSalesShippingDoc,0) AS numDefaultSalesShippingDoc,
			ISNULL(D.numDefaultPurchaseShippingDoc,0) AS numDefaultPurchaseShippingDoc,
			ISNULL((SELECT TOP 1 bitMarginPriceViolated FROM ApprovalProcessItemsClassification  WHERE numDomainID=D.numDomainID AND ISNULL(bitMarginPriceViolated,0) = 1),0) AS bitMarginPriceViolated,
			ISNULL((SELECT TOP 1 intViewAllowed FROM GroupAuthorization WHERE numGroupID=EA.numGroupID AND numModuleID=2 AND numPageID=2),0) AS tintLeadRights,
			ISNULL((SELECT TOP 1 intViewAllowed FROM GroupAuthorization WHERE numGroupID=EA.numGroupID AND numModuleID=3 AND numPageID=2),0) AS tintProspectRights,
			ISNULL((SELECT TOP 1 intViewAllowed FROM GroupAuthorization WHERE numGroupID=EA.numGroupID AND numModuleID=4 AND numPageID=2),0) AS tintAccountRights,
			ISNULL((SELECT TOP 1 intViewAllowed FROM GroupAuthorization WHERE numGroupID=EA.numGroupID AND numModuleID=10 AND numPageID=10),0) AS tintSalesOrderListRights,
			ISNULL((SELECT TOP 1 intViewAllowed FROM GroupAuthorization WHERE numGroupID=EA.numGroupID AND numModuleID=10 AND numPageID=11),0) AS tintPurchaseOrderListRights,
			ISNULL((SELECT TOP 1 intViewAllowed FROM GroupAuthorization WHERE numGroupID=EA.numGroupID AND numModuleID=10 AND numPageID=2),0) AS tintSalesOpportunityListRights,
			ISNULL((SELECT TOP 1 intViewAllowed FROM GroupAuthorization WHERE numGroupID=EA.numGroupID AND numModuleID=10 AND numPageID=8),0) AS tintPurchaseOpportunityListRights,
			ISNULL((SELECT TOP 1 intViewAllowed FROM GroupAuthorization WHERE numGroupID=EA.numGroupID AND numModuleID=11 AND numPageID=2),0) AS tintContactListRights,
			ISNULL((SELECT TOP 1 intViewAllowed FROM GroupAuthorization WHERE numGroupID=EA.numGroupID AND numModuleID=7 AND numPageID=2),0) AS tintCaseListRights,
			ISNULL((SELECT TOP 1 intViewAllowed FROM GroupAuthorization WHERE numGroupID=EA.numGroupID AND numModuleID=12 AND numPageID=2),0) AS tintProjectListRights,
			ISNULL((SELECT TOP 1 numCriteria FROM TicklerListFilterConfiguration WHERE CHARINDEX(CONCAT(',',A.numContactId,','),CONCAT(',',vcSelectedEmployee,',')) > 0),0) AS tintActionItemRights,
			ISNULL(D.numAuthorizePercentage,0) AS numAuthorizePercentage,
			ISNULL(D.bitAllowDuplicateLineItems,0) bitAllowDuplicateLineItems,
			ISNULL(D.bitLogoAppliedToBizTheme,0) bitLogoAppliedToBizTheme,
			ISNULL(D.vcLogoForBizTheme,'') vcLogoForBizTheme,
			ISNULL(D.bitLogoAppliedToLoginBizTheme,0) bitLogoAppliedToLoginBizTheme,
			ISNULL(D.vcLogoForLoginBizTheme,'') vcLogoForLoginBizTheme,
			ISNULL(D.vcThemeClass,'1473b4') vcThemeClass,
			ISNULL(D.vcLoginURL,'') AS vcLoginURL,
			ISNULL(D.bitDisplayCustomField,0) AS bitDisplayCustomField,
			ISNULL(D.bitFollowupAnytime,0) AS bitFollowupAnytime,
			ISNULL(D.bitpartycalendarTitle,0) AS bitpartycalendarTitle,
			ISNULL(D.bitpartycalendarLocation,0) AS bitpartycalendarLocation,
			ISNULL(D.bitpartycalendarDescription,0) AS bitpartycalendarDescription,
			ISNULL(D.bitREQPOApproval,0) AS bitREQPOApproval,
			ISNULL(D.bitARInvoiceDue,0) AS bitARInvoiceDue,
			ISNULL(D.bitAPBillsDue,0) AS bitAPBillsDue,
			ISNULL(D.bitItemsToPickPackShip,0) AS bitItemsToPickPackShip,
			ISNULL(D.bitItemsToInvoice,0) AS bitItemsToInvoice,
			ISNULL(D.bitSalesOrderToClose,0) AS bitSalesOrderToClose,
			ISNULL(D.bitItemsToPutAway,0) AS bitItemsToPutAway,
			ISNULL(D.bitItemsToBill,0) AS bitItemsToBill,
			ISNULL(D.bitPosToClose,0) AS bitPosToClose,
			ISNULL(D.bitPOToClose,0) AS bitPOToClose,
			ISNULL(D.bitBOMSToPick,0) AS bitBOMSToPick,
			ISNULL(D.vchREQPOApprovalEmp,'') AS vchREQPOApprovalEmp,
			ISNULL(D.vchARInvoiceDue,'') AS vchARInvoiceDue,
			ISNULL(D.vchAPBillsDue,'') AS vchAPBillsDue,
			ISNULL(D.vchItemsToPickPackShip,'') AS vchItemsToPickPackShip,
			ISNULL(D.vchItemsToInvoice,'') AS vchItemsToInvoice,
			ISNULL(stuff((
			select ',' + CAST(UPC.numUserID AS VARCHAR(10))
			from UnitPriceApprover UPC
			where UPC.numDomainID = D.numDomainId
			order by UPC.numUserID
			for xml path('')),1,1,''),'') AS vchPriceMarginApproval,
			ISNULL(D.vchSalesOrderToClose,'') AS vchSalesOrderToClose,
			ISNULL(D.vchItemsToPutAway,'') AS vchItemsToPutAway,
			ISNULL(D.vchItemsToBill,'') AS vchItemsToBill,
			ISNULL(D.vchPosToClose,'') AS vchPosToClose,
			ISNULL(D.vchPOToClose,'') AS vchPOToClose,
			ISNULL(D.vchBOMSToPick,'') AS vchBOMSToPick,
			ISNULL(D.decReqPOMinValue,0) AS decReqPOMinValue,
			0 tintPayrollType,
			(CASE WHEN EXISTS (SELECT numCategoryHDRID FROM TimeAndExpense WHERE numUserCntID=A.numContactId AND numCategory=1 AND numType=7 AND dtToDate IS NULL) THEN 1 ELSE 0 END) bitUserClockedIn,
			ISNULL(D.bitUseOnlyActionItems,0) AS bitUseOnlyActionItems,
			ISNULL(D.vcElectiveItemFields,'') AS vcElectiveItemFields,
			ISNULL(D.bitDisplayContractElement,0) AS bitDisplayContractElement,
			ISNULL(D.vcEmployeeForContractTimeElement,'') AS vcEmployeeForContractTimeElement,
			ISNULL(D.bitEnableSmartyStreets,0) bitEnableSmartyStreets,
			ISNULL(D.vcSmartyStreetsAPIKeys,'') vcSmartyStreetsAPIKeys
		FROM 
			ExtranetAccountsDtl EAD
		INNER JOIN
			ExtarnetAccounts EA
		ON
			EAD.numExtranetID = EA.numExtranetID                                                
		INNER JOIN 
			Domain D                              
		ON 
			D.numDomainID=EAD.numDomainID
		INNER JOIN 
			AdditionalContactsInformation A                              
		ON 
			A.numContactID=EAD.numContactID
		INNER JOIN 
			Subscribers S            
		ON 
			S.numTargetDomainID=D.numDomainID 
		LEFT JOIN 
			DivisionMaster Div                                            
		ON 
			EA.numDivisionID=Div.numDivisionID                               
		LEFT JOIN 
			CompanyInfo C                              
		ON 
			C.numCompanyID=Div.numCompanyID    
		OUTER APPLY
		(
			SELECT  
				TOP 1 REPLACE(SCB.Link,'../','') vcDefaultNavURL
			FROM    
				TabMaster T
			JOIN 
				GroupTabDetails G ON G.numTabId = T.numTabId
			LEFT JOIN
				ShortCutGrpConf SCUC ON G.numTabId=SCUC.numTabId AND SCUC.numDomainId=EAD.numDomainID AND SCUC.numGroupId=EA.numGroupID AND bitDefaultTab=1
			LEFT JOIN
				ShortCutBar SCB
			ON
				SCB.Id=SCUC.numLinkId
			WHERE   
				(T.numDomainID = EAD.numDomainID OR bitFixed = 1)
				AND G.numGroupID = EA.numGroupID
				AND ISNULL(G.[tintType], 0) <> 1
				AND tintTabType =1
				AND T.numTabID NOT IN (2,68)
			ORDER BY 
				SCUC.bitInitialPage DESC
		)  TempDefaultPage 
		 OUTER APPLY
		 (
			SELECT  
				TOP 1 REPLACE(T.vcURL,'../','') vcDefaultNavURL
			FROM    
				TabMaster T
			JOIN 
				GroupTabDetails G 
			ON 
				G.numTabId = T.numTabId
			WHERE   
				(T.numDomainID = EAD.numDomainID OR bitFixed = 1)
				AND G.numGroupID = EA.numGroupID
				AND ISNULL(G.[tintType], 0) <> 1
				AND tintTabType =1
				AND T.numTabID NOT IN (2,68)
				AND ISNULL(G.bitInitialTab,0) = 1 
				AND ISNULL(bitallowed,0)=1
			ORDER BY 
				G.bitInitialTab DESC
		 ) TEMPDefaultTab                   
		WHERE 
			(EAD.vcUserName=@UserName OR A.vcemail=@UserName) 
			AND (vcPassword=@vcPassword) 
			AND bitPartnerAccess=1 
			AND GETUTCDATE() BETWEEN dtSubStartDate and dtSubEndDate
	END
	ELSE
	BEGIN
	SELECT TOP 1 
		U.numUserID
		,numUserDetailId
		,D.numCost
		,isnull(vcEmailID,'') vcEmailID
		,ISNULL(vcEmailAlias,'') vcEmailAlias
		,ISNULL(vcEmailAliasPassword,'') vcEmailAliasPassword
		,isnull(numGroupID,0) numGroupID
		,bitActivateFlag
		,isnull(vcMailNickName,'') vcMailNickName
		,isnull(txtSignature,'') txtSignature
		,isnull(U.numDomainID,0) numDomainID
		,isnull(Div.numDivisionID,0) numDivisionID
		,isnull(vcCompanyName,'') vcCompanyName
		,isnull(E.bitExchangeIntegration,0) bitExchangeIntegration
		,isnull(E.bitAccessExchange,0) bitAccessExchange
		,isnull(E.vcExchPath,'') vcExchPath
		,isnull(E.vcExchDomain,'') vcExchDomain
		,isnull(D.vcExchUserName,'') vcExchUserName
		,isnull(vcFirstname,'') + ' ' + isnull(vcLastName,'') as ContactName
		,Case when E.bitAccessExchange=0 then  E.vcExchPassword when E.bitAccessExchange=1 then D.vcExchPassword end as vcExchPassword
		,tintCustomPagingRows
		,vcDateFormat
		,numDefCountry
		,tintComposeWindow
		,dateadd(day,-sintStartDate,getutcdate()) as StartDate
		,dateadd(day,sintNoofDaysInterval,dateadd(day,-sintStartDate,getutcdate())) as EndDate
		,tintAssignToCriteria
		,bitIntmedPage
		,tintFiscalStartMonth
		,numAdminID
		,isnull(A.numTeam,0) numTeam
		,isnull(D.vcCurrency,'') as vcCurrency
		,isnull(D.numCurrencyID,0) as numCurrencyID, 
		isnull(D.bitMultiCurrency,0) as bitMultiCurrency,              
		bitTrial,isnull(tintChrForComSearch,0) as tintChrForComSearch,  
		isnull(tintChrForItemSearch,0) as tintChrForItemSearch,              
		case when bitSMTPServer= 1 then vcSMTPServer else '' end as vcSMTPServer  ,          
		case when bitSMTPServer= 1 then  isnull(numSMTPPort,0)  else 0 end as numSMTPPort      
		,isnull(bitSMTPAuth,0) bitSMTPAuth    
		,isnull([vcSmtpPassword],'') vcSmtpPassword    
		,isnull(bitSMTPSSL,0) bitSMTPSSL   ,isnull(bitSMTPServer,0) bitSMTPServer,       
		isnull(IM.bitImap,0) bitImapIntegration, isnull(charUnitSystem,'E')   UnitSystem, datediff(day,getutcdate(),dtSubEndDate) as NoOfDaysLeft,
		ISNULL(bitCreateInvoice,0) bitCreateInvoice,
		ISNULL([numDefaultSalesBizDocId],0) numDefaultSalesBizDocId,
		ISNULL([numDefaultPurchaseBizDocId],0) numDefaultPurchaseBizDocId,
		vcDomainName,
		S.numSubscriberID,
		D.[tintBaseTax],
		u.ProfilePic,
		ISNULL(D.[numShipCompany],0) numShipCompany,
		ISNULL(D.[bitEmbeddedCost],0) bitEmbeddedCost,
		(Select isnull(numAuthoritativeSales,0) From AuthoritativeBizDocs Where numDomainId=D.numDomainID) numAuthoritativeSales,
		(Select isnull(numAuthoritativePurchase,0) From AuthoritativeBizDocs Where numDomainId=D.numDomainID) numAuthoritativePurchase,
		ISNULL(D.[numDefaultSalesOppBizDocId],0) numDefaultSalesOppBizDocId,
		ISNULL(tintDecimalPoints,2) tintDecimalPoints,
		ISNULL(D.tintBillToForPO,0) tintBillToForPO, 
		ISNULL(D.tintShipToForPO,0) tintShipToForPO,
		ISNULL(D.tintOrganizationSearchCriteria,0) tintOrganizationSearchCriteria,
		ISNULL(D.tintLogin,0) tintLogin,
			ISNULL(S.intFullUserConcurrency,0) intFullUserConcurrency,
		ISNULL(bitDocumentRepositary,0) bitDocumentRepositary,
		ISNULL(D.vcPortalName,'') vcPortalName,
		(SELECT COUNT(*) FROM dbo.Subscribers WHERE getutcdate() between dtSubStartDate and dtSubEndDate and bitActive=1),
		ISNULL(D.bitGtoBContact,0) bitGtoBContact,
		ISNULL(D.bitBtoGContact,0) bitBtoGContact,
		ISNULL(D.bitGtoBCalendar,0) bitGtoBCalendar,
		ISNULL(D.bitBtoGCalendar,0) bitBtoGCalendar,
		ISNULL(bitExpenseNonInventoryItem,0) bitExpenseNonInventoryItem,
		ISNULL(D.bitInlineEdit,0) bitInlineEdit,
		ISNULL(U.numDefaultClass,0) numDefaultClass,
		ISNULL(D.bitRemoveVendorPOValidation,0) bitRemoveVendorPOValidation,
		ISNULL(D.bitTrakDirtyForm,1) bitTrakDirtyForm,
		isnull(D.tintBaseTaxOnArea,0) tintBaseTaxOnArea,
		ISNULL(D.bitAmountPastDue,0) bitAmountPastDue,
		CONVERT(DECIMAL(18,2),ISNULL(D.monAmountPastDue,0)) monAmountPastDue,
		ISNULL(D.bitSearchOrderCustomerHistory,0) bitSearchOrderCustomerHistory,
		isnull(U.tintTabEvent,0) as tintTabEvent,
		isnull(D.bitRentalItem,0) as bitRentalItem,
		isnull(D.numRentalItemClass,0) as numRentalItemClass,
		isnull(D.numRentalHourlyUOM,0) as numRentalHourlyUOM,
		isnull(D.numRentalDailyUOM,0) as numRentalDailyUOM,
		isnull(D.tintRentalPriceBasedOn,0) as tintRentalPriceBasedOn,
		isnull(D.tintCalendarTimeFormat,24) as tintCalendarTimeFormat,
		isnull(D.tintDefaultClassType,0) as tintDefaultClassType,
		ISNULL(U.numDefaultWarehouse,0) numDefaultWarehouse,
		ISNULL(D.tintPriceBookDiscount,0) tintPriceBookDiscount,
		ISNULL(D.bitAutoSerialNoAssign,0) bitAutoSerialNoAssign,
		ISNULL(IsEnableProjectTracking,0) AS [IsEnableProjectTracking],
		ISNULL(IsEnableCampaignTracking,0) AS [IsEnableCampaignTracking],
		ISNULL(IsEnableResourceScheduling,0) AS [IsEnableResourceScheduling],
		ISNULL(D.numSOBizDocStatus,0) AS numSOBizDocStatus,
		ISNULL(D.bitAutolinkUnappliedPayment,0) AS bitAutolinkUnappliedPayment,
		ISNULL(numShippingServiceItemID,0) AS [numShippingServiceItemID],
		ISNULL(D.numDiscountServiceItemID,0) AS numDiscountServiceItemID,
		ISNULL(D.tintCommissionType,1) AS tintCommissionType,
		ISNULL(D.bitDiscountOnUnitPrice,0) AS bitDiscountOnUnitPrice,
		ISNULL(D.intShippingImageWidth,0) AS intShippingImageWidth ,
		ISNULL(D.intShippingImageHeight,0) AS intShippingImageHeight,
		ISNULL(D.numTotalInsuredValue,0) AS numTotalInsuredValue,
		ISNULL(D.numTotalCustomsValue,0) AS numTotalCustomsValue,
		ISNULL(D.bitUseBizdocAmount,1) AS [bitUseBizdocAmount],
		ISNULL(D.bitDefaultRateType,1) AS [bitDefaultRateType],
		ISNULL(D.bitIncludeTaxAndShippingInCommission,1) AS [bitIncludeTaxAndShippingInCommission],
		ISNULL(D.[bitLandedCost],0) AS bitLandedCost,
		ISNULL(D.vcLanedCostDefault,'') AS vcLanedCostDefault,
		ISNULL(D.bitMinUnitPriceRule,0) AS bitMinUnitPriceRule,
		ISNULL(@listIds,'') AS vcUnitPriceApprover,
		ISNULL(D.numPODropShipBizDoc,0) AS numPODropShipBizDoc,
		ISNULL(D.numPODropShipBizDocTemplate,0) AS numPODropShipBizDocTemplate,
		ISNULL(D.IsEnableDeferredIncome,0) AS IsEnableDeferredIncome,
		ISNULL(D.bitEnableItemLevelUOM,0) AS bitEnableItemLevelUOM,
		ISNULL(D.bitPurchaseTaxCredit,0) AS bitPurchaseTaxCredit,
		ISNULL(U.bintCreatedDate,0) AS bintCreatedDate,
		(CASE WHEN LEN(ISNULL(TempDefaultPage.vcDefaultNavURL,'')) = 0 THEN ISNULL(TempDefaultTab.vcDefaultNavURL,'Items/frmItemList.aspx?Page=All Items&ItemGroup=0') ELSE ISNULL(TempDefaultPage.vcDefaultNavURL,'') END) vcDefaultNavURL,
		ISNULL(D.bitApprovalforTImeExpense,0) AS bitApprovalforTImeExpense,
		ISNULL(D.intTimeExpApprovalProcess,0) AS intTimeExpApprovalProcess,ISNULL(D.bitApprovalforOpportunity,0) AS bitApprovalforOpportunity,
		ISNULL(D.intOpportunityApprovalProcess,0) AS intOpportunityApprovalProcess,
		ISNULL(D.numDefaultSalesShippingDoc,0) AS numDefaultSalesShippingDoc,
		ISNULL(D.numDefaultPurchaseShippingDoc,0) AS numDefaultPurchaseShippingDoc,
		ISNULL((SELECT TOP 1 bitMarginPriceViolated FROM ApprovalProcessItemsClassification  WHERE numDomainID=D.numDomainID AND ISNULL(bitMarginPriceViolated,0) = 1),0) AS bitMarginPriceViolated,
		ISNULL((SELECT TOP 1 intViewAllowed FROM GroupAuthorization WHERE numGroupID=U.numGroupID AND numModuleID=2 AND numPageID=2),0) AS tintLeadRights,
		ISNULL((SELECT TOP 1 intViewAllowed FROM GroupAuthorization WHERE numGroupID=U.numGroupID AND numModuleID=3 AND numPageID=2),0) AS tintProspectRights,
		ISNULL((SELECT TOP 1 intViewAllowed FROM GroupAuthorization WHERE numGroupID=U.numGroupID AND numModuleID=4 AND numPageID=2),0) AS tintAccountRights,
		ISNULL((SELECT TOP 1 intViewAllowed FROM GroupAuthorization WHERE numGroupID=U.numGroupID AND numModuleID=10 AND numPageID=10),0) AS tintSalesOrderListRights,
		ISNULL((SELECT TOP 1 intViewAllowed FROM GroupAuthorization WHERE numGroupID=U.numGroupID AND numModuleID=10 AND numPageID=11),0) AS tintPurchaseOrderListRights,
		ISNULL((SELECT TOP 1 intViewAllowed FROM GroupAuthorization WHERE numGroupID=U.numGroupID AND numModuleID=10 AND numPageID=2),0) AS tintSalesOpportunityListRights,
		ISNULL((SELECT TOP 1 intViewAllowed FROM GroupAuthorization WHERE numGroupID=U.numGroupID AND numModuleID=10 AND numPageID=8),0) AS tintPurchaseOpportunityListRights,
		ISNULL((SELECT TOP 1 intViewAllowed FROM GroupAuthorization WHERE numGroupID=U.numGroupID AND numModuleID=11 AND numPageID=2),0) AS tintContactListRights,
		ISNULL((SELECT TOP 1 intViewAllowed FROM GroupAuthorization WHERE numGroupID=U.numGroupID AND numModuleID=7 AND numPageID=2),0) AS tintCaseListRights,
		ISNULL((SELECT TOP 1 intViewAllowed FROM GroupAuthorization WHERE numGroupID=U.numGroupID AND numModuleID=12 AND numPageID=2),0) AS tintProjectListRights,
		ISNULL((SELECT TOP 1 numCriteria FROM TicklerListFilterConfiguration WHERE CHARINDEX(CONCAT(',',U.numUserDetailID,','),CONCAT(',',vcSelectedEmployee,',')) > 0),0) AS tintActionItemRights,
		ISNULL(D.numAuthorizePercentage,0) AS numAuthorizePercentage,
		ISNULL(D.bitAllowDuplicateLineItems,0) bitAllowDuplicateLineItems,
		ISNULL(D.bitLogoAppliedToBizTheme,0) bitLogoAppliedToBizTheme,
		ISNULL(D.vcLogoForBizTheme,'') vcLogoForBizTheme,
		ISNULL(D.bitLogoAppliedToLoginBizTheme,0) bitLogoAppliedToLoginBizTheme,
		ISNULL(D.vcLogoForLoginBizTheme,'') vcLogoForLoginBizTheme,
		ISNULL(D.vcThemeClass,'1473b4') vcThemeClass,
		ISNULL(D.vcLoginURL,'') AS vcLoginURL,
		ISNULL(D.bitDisplayCustomField,0) AS bitDisplayCustomField,
		ISNULL(D.bitFollowupAnytime,0) AS bitFollowupAnytime,
		ISNULL(D.bitpartycalendarTitle,0) AS bitpartycalendarTitle,
		ISNULL(D.bitpartycalendarLocation,0) AS bitpartycalendarLocation,
		ISNULL(D.bitpartycalendarDescription,0) AS bitpartycalendarDescription,
		ISNULL(D.bitREQPOApproval,0) AS bitREQPOApproval,
		ISNULL(D.bitARInvoiceDue,0) AS bitARInvoiceDue,
		ISNULL(D.bitAPBillsDue,0) AS bitAPBillsDue,
		ISNULL(D.bitItemsToPickPackShip,0) AS bitItemsToPickPackShip,
		ISNULL(D.bitItemsToInvoice,0) AS bitItemsToInvoice,
		ISNULL(D.bitSalesOrderToClose,0) AS bitSalesOrderToClose,
		ISNULL(D.bitItemsToPutAway,0) AS bitItemsToPutAway,
		ISNULL(D.bitItemsToBill,0) AS bitItemsToBill,
		ISNULL(D.bitPosToClose,0) AS bitPosToClose,
		ISNULL(D.bitPOToClose,0) AS bitPOToClose,
		ISNULL(D.bitBOMSToPick,0) AS bitBOMSToPick,
		ISNULL(D.vchREQPOApprovalEmp,'') AS vchREQPOApprovalEmp,
		ISNULL(D.vchARInvoiceDue,'') AS vchARInvoiceDue,
		ISNULL(D.vchAPBillsDue,'') AS vchAPBillsDue,
		ISNULL(D.vchItemsToPickPackShip,'') AS vchItemsToPickPackShip,
		ISNULL(D.vchItemsToInvoice,'') AS vchItemsToInvoice,
		ISNULL(stuff((
        select ',' + CAST(UPC.numUserID AS VARCHAR(10))
        from UnitPriceApprover UPC
        where UPC.numDomainID = D.numDomainId
        order by UPC.numUserID
			for xml path('')),1,1,''),'') AS vchPriceMarginApproval,
		ISNULL(D.vchSalesOrderToClose,'') AS vchSalesOrderToClose,
		ISNULL(D.vchItemsToPutAway,'') AS vchItemsToPutAway,
		ISNULL(D.vchItemsToBill,'') AS vchItemsToBill,
		ISNULL(D.vchPosToClose,'') AS vchPosToClose,
		ISNULL(D.vchPOToClose,'') AS vchPOToClose,
		ISNULL(D.vchBOMSToPick,'') AS vchBOMSToPick,
		ISNULL(D.decReqPOMinValue,0) AS decReqPOMinValue,
		ISNULL(U.tintPayrollType,1) tintPayrollType,
		(CASE WHEN EXISTS (SELECT numCategoryHDRID FROM TimeAndExpense WHERE numUserCntID=U.numUserDetailId AND numCategory=1 AND numType=7 AND dtToDate IS NULL) THEN 1 ELSE 0 END) bitUserClockedIn,
		ISNULL(D.bitUseOnlyActionItems,0) AS bitUseOnlyActionItems,
		ISNULL(D.vcElectiveItemFields,'') AS vcElectiveItemFields,
		ISNULL(D.bitDisplayContractElement,0) AS bitDisplayContractElement,
		ISNULL(D.vcEmployeeForContractTimeElement,'') AS vcEmployeeForContractTimeElement,
		ISNULL(D.bitEnableSmartyStreets,0) bitEnableSmartyStreets,
		ISNULL(D.vcSmartyStreetsAPIKeys,'') vcSmartyStreetsAPIKeys
	FROM 
		UserMaster U                              
	left join ExchangeUserDetails E                              
	on E.numUserID=U.numUserID                              
	left join ImapUserDetails IM                              
	on IM.numUserCntID=U.numUserDetailID          
	Join Domain D                              
	on D.numDomainID=U.numDomainID                                           
	left join DivisionMaster Div                                            
	on D.numDivisionID=Div.numDivisionID                               
	left join CompanyInfo C                              
	on C.numCompanyID=Div.numCompanyID                               
	left join AdditionalContactsInformation A                              
	on  A.numContactID=U.numUserDetailId                            
	Join  Subscribers S                            
	on S.numTargetDomainID=D.numDomainID    
	 OUTER APPLY
	 (
		SELECT  
			TOP 1 REPLACE(SCB.Link,'../','') vcDefaultNavURL
		FROM    
			TabMaster T
		JOIN 
			GroupTabDetails G ON G.numTabId = T.numTabId
		LEFT JOIN
			ShortCutGrpConf SCUC ON G.numTabId=SCUC.numTabId AND SCUC.numDomainId=U.numDomainID AND SCUC.numGroupId=U.numGroupID AND bitDefaultTab=1
		LEFT JOIN
			ShortCutBar SCB
		ON
			SCB.Id=SCUC.numLinkId
		WHERE   
			(T.numDomainID = U.numDomainID OR bitFixed = 1)
			AND G.numGroupID = U.numGroupID
			AND ISNULL(G.[tintType], 0) <> 1
			AND tintTabType =1
			AND T.numTabID NOT IN (2,68)
		ORDER BY 
			SCUC.bitInitialPage DESC
	 )  TempDefaultPage 
	 OUTER APPLY
	 (
		SELECT  
			TOP 1 REPLACE(T.vcURL,'../','') vcDefaultNavURL
		FROM    
			TabMaster T
		JOIN 
			GroupTabDetails G 
		ON 
			G.numTabId = T.numTabId
		WHERE   
			(T.numDomainID = U.numDomainID OR bitFixed = 1)
			AND G.numGroupID = U.numGroupID
			AND ISNULL(G.[tintType], 0) <> 1
			AND tintTabType =1
			AND T.numTabID NOT IN (2,68)
			AND ISNULL(G.bitInitialTab,0) = 1 
			AND ISNULL(bitallowed,0)=1
		ORDER BY 
			G.bitInitialTab DESC
	 ) TEMPDefaultTab                   
	WHERE 1 = (CASE 
		WHEN @vcLinkedinId='N' THEN (CASE WHEN vcEmailID=@UserName and vcPassword=@vcPassword THEN 1 ELSE 0 END)
		ELSE (CASE WHEN U.vcLinkedinId=@vcLinkedinId THEN 1 ELSE 0 END)
		END) and bitActive IN (1,2) AND getutcdate() between dtSubStartDate and dtSubEndDate
	END
             
	SELECT 
		numTerritoryId 
	FROM
		UserTerritory UT                              
	JOIN 
		UserMaster U                              
	ON 
		numUserDetailId =UT.numUserCntID                                         
	WHERE 
		1 = (CASE 
			WHEN @vcLinkedinId='N' THEN (CASE WHEN vcEmailID=@UserName and vcPassword=@vcPassword THEN 1 ELSE 0 END)
			ELSE (CASE WHEN vcLinkedinId=@vcLinkedinId THEN 1 ELSE 0 END)
			END)

END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_CommissionPayPeriod_Save')
DROP PROCEDURE USP_CommissionPayPeriod_Save
GO
Create PROCEDURE [dbo].[USP_CommissionPayPeriod_Save]
	-- Add the parameters for the stored procedure here
	 @numComPayPeriodID AS NUMERIC(18,0) OUTPUT,
	 @numDomainID AS NUMERIC(18,0),
	 @dtStart DATE,
	 @dtEnd DATE,
	 @tintPayPeriod INT,
	 @numUserID NUMERIC(18,0),
	 @ClientTimeZoneOffset INT     
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
SET NOCOUNT ON;

BEGIN TRY
	--NEW PAY PERIOD COMMISSION CALCULATION
	IF @numComPayPeriodID = 0
	BEGIN
		INSERT INTO CommissionPayPeriod
		(
			numDomainID,
			dtStart,
			dtEnd,
			tintPayPeriod,
			numCreatedBy,
			dtCreated
		)
		VALUES
		(
			@numDomainID,
			@dtStart,
			@dtEnd,
			@tintPayPeriod,
			@numUserID,
			GETDATE()
		)

		SET @numComPayPeriodID = SCOPE_IDENTITY()
	END
	-- RECALCULATE PAY PERIOD COMMISSION
	ELSE
	BEGIN
		UPDATE 
			CommissionPayPeriod
		SET 
			numModifiedBy = @numUserID,
			dtModified = GETDATE()
		WHERE
			numComPayPeriodID = @numComPayPeriodID
	END

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	BEGIN TRANSACTION
	
	EXEC USP_CalculateCommission @numComPayPeriodID=@numComPayPeriodID,@numDomainID=@numDomainID,@ClientTimeZoneOffset=@ClientTimeZoneOffset
	EXEC USP_CalculateTimeAndExpense @numComPayPeriodID=@numComPayPeriodID,@numDomainID=@numDomainID,@ClientTimeZoneOffset=@ClientTimeZoneOffset
	EXEC USP_CalculateCommissionSalesReturn @numComPayPeriodID=@numComPayPeriodID,@numDomainID=@numDomainID,@ClientTimeZoneOffset=@ClientTimeZoneOffset
	EXEC USP_CalculateCommissionOverPayment @numDomainID=@numDomainID
	
	COMMIT

	SELECT @numComPayPeriodID
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
END
GO

GO
/****** Object:  StoredProcedure [dbo].[USP_GET_PACKAGES]    Script Date: 30/07/2012 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO

IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GET_PACKAGES' ) 
    DROP PROCEDURE USP_GET_PACKAGES
GO
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE USP_GET_PACKAGES
@DomainId NUMERIC(18,0)
AS 
BEGIN
	SELECT
		0 AS numCustomPackageID,
		0 AS numPackageTypeID,
		'None' AS vcPackageName,
		0.00 AS fltWidth,
		0.00 AS fltHeight,
		0.00 AS fltLength,
		0.00 AS fltTotalWeight,
		0 AS numShippingCompanyID,
		'' AS vcShippingCompany,
		CONCAT(0,',',0,',',0,',',0,',',0,',',0)  AS [ValueDimension]
	UNION 
	SELECT
		numItemCode AS numCustomPackageID,
		0 AS numPackageTypeID,
		vcItemName AS vcPackageName,
		fltWidth,
		fltHeight,
		fltLength,
		fltWeight AS fltTotalWeight,
		0 AS numShippingCompanyID,
		'Custom Box' AS vcShippingCompany,
		CONCAT(0,',',fltWidth,',',fltHeight,',',fltLength,',',fltWeight,',',numItemCode)  AS [ValueDimension]
	FROM 
		Item WITH (NOLOCK)
	where 
		numDomainID=@DomainId
		AND ISNULL(bitContainer,0)=1
		
END
GO

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetAdvancedSearchFieldList' ) 
    DROP PROCEDURE USP_GetAdvancedSearchFieldList
GO
CREATE PROCEDURE [dbo].[USP_GetAdvancedSearchFieldList]
    @numDomainId NUMERIC(9),
    @numFormId INT,
    @numAuthGroupId NUMERIC(9)
AS  
	--added by chintan to select Custom field form multiple location
	DECLARE @vcLocationID VARCHAR(100);SET @vcLocationID='0'
	--DECLARE @vcSectionName VARCHAR(500);SET @vcSectionName=''

	Select @vcLocationID =isnull(vcLocationID,'0') from dynamicFormMaster where numFormID=@numFormId

	--select ItemNumber as intSectionID,Item as vcSectionName from dbo.DelimitedSplit8K(@vcSectionName,',')
	select intSectionID,vcSectionName,Loc_id from DycFormSectionDetail where @numFormId=numFormId

    CREATE TABLE #tempFieldsList(ID int IDENTITY(1,1) NOT NULL,RowNo int,intSectionID int,numModuleID numeric(9),numFormFieldId  NVARCHAR(20),vcFormFieldName NVARCHAR(100),
        vcFieldType CHAR(1),vcAssociatedControlType NVARCHAR(50),numListID NUMERIC(18),vcDbColumnName NVARCHAR(100),vcPropertyName NVARCHAR(100),vcToolTip NVARCHAR(1000),
        vcListItemType CHAR(3),intColumnNum INT,intRowNum INT,boolRequired tinyint, numAuthGroupID NUMERIC(18),
          vcFieldDataType CHAR(1),boolAOIField tinyint,numFormID NUMERIC(18),vcLookBackTableName NVARCHAR(100),GRP_ID int,intFieldMaxLength int)

DECLARE @Nocolumns AS TINYINT;SET @Nocolumns = 0                

select @Nocolumns=isnull(sum(TotalRow),0)  from(            
Select count(*) TotalRow from View_DynamicColumns where numFormId=@numFormId and numAuthGroupID = @numAuthGroupId and numDomainID=@numDomainID 
Union 
Select count(*) TotalRow from View_DynamicCustomColumns where numFormId=@numFormId and numAuthGroupID = @numAuthGroupId and numDomainID=@numDomainID ) TotalRows

if @Nocolumns>0 
BEGIN

  INSERT INTO #tempFieldsList (intSectionID,numModuleID,RowNo,numFormFieldId,vcFormFieldName,vcFieldType,vcAssociatedControlType
        ,numListID,vcDbColumnName,vcPropertyName,vcToolTip,vcListItemType,intColumnNum,intRowNum,boolRequired,numAuthGroupID,
          vcFieldDataType,boolAOIField,numFormID,vcLookBackTableName,GRP_ID,intFieldMaxLength)                         
SELECT intSectionID,numModuleID,row_number() over (PARTITION BY intSectionID order by intSectionID), CONVERT(VARCHAR(15),numFieldId) + vcFieldType AS numFormFieldId,
                    ISNULL(vcCultureFieldName,vcFieldName) AS vcFormFieldName,
                    'R' as vcFieldType,
                    vcAssociatedControlType,
                    numListID,
                    vcDbColumnName,
                    vcPropertyName,
                    vcToolTip,
                    vcListItemType,
                    tintColumn as intColumnNum,
                    tintRow as intRowNum,
                    ISNULL(bitIsRequired,0) boolRequired,
                    0 numAuthGroupID,
                    vcFieldDataType,
                    0 boolAOIField,
                    numFormID,
                    vcLookBackTableName,0 as GRP_ID, intFieldMaxLength
 FROM View_DynamicColumns 
 where numFormId=@numFormId and numAuthGroupID = @numAuthGroupId and numDomainID=@numDomainID AND 1 = (CASE WHEN numFormId=29 AND numFieldID=311 THEN 0 ELSE 1 END)
       
       UNION
    
    SELECT 0 as intSectionID,0 as numModuleID,row_number() over (PARTITION BY GRP_ID order by GRP_ID),CONVERT(VARCHAR(15), numFieldId) + 'C' AS numFormFieldId,
                    vcFieldName AS vcFormFieldName,
                    vcFieldType AS vcFieldType,
                    vcAssociatedControlType,
                    numListID,
                    CONVERT(VARCHAR(15), numFieldId) + 'C' vcDbColumnName,
                    '' AS vcPropertyName,
                    '' AS vcToolTip,
                    CASE WHEN numListID > 0 THEN 'LI' ELSE '' END vcListItemType,
                   tintColumn as intColumnNum,
                    tintRow as intRowNum,
                    ISNULL(bitIsRequired,0) as boolRequired,
                    0 numAuthGroupID,
                    CASE WHEN numListID > 0 THEN 'N' ELSE 'V' END vcFieldDataType,
                    0 boolAOIField,
                    @numFormId AS numFormID,
                    '' AS vcLookBackTableName,GRP_ID,0 as intFieldMaxLength
 from View_DynamicCustomColumns
 where numFormId=@numFormId and numAuthGroupID = @numAuthGroupId and numDomainID=@numDomainID  AND GRP_ID IN(	SELECT ID FROM dbo.SplitIDs(@vcLocationID,','))

END
ELSE
BEGIN 
   INSERT INTO #tempFieldsList (intSectionID,numModuleID,RowNo,numFormFieldId,vcFormFieldName,vcFieldType,vcAssociatedControlType
        ,numListID,vcDbColumnName,vcPropertyName,vcToolTip,vcListItemType,intColumnNum,intRowNum,boolRequired,numAuthGroupID,
          vcFieldDataType,boolAOIField,numFormID,vcLookBackTableName,GRP_ID,intFieldMaxLength)                         
            SELECT intSectionID,numModuleID,row_number() over (PARTITION BY intSectionID order by intSectionID), CONVERT(VARCHAR(15),numFieldId) + vcFieldType AS numFormFieldId,
                    ISNULL(vcCultureFieldName,vcFieldName) AS vcFormFieldName,
                    'R' as vcFieldType,
                    vcAssociatedControlType,
                    numListID,
                    vcDbColumnName,
                    vcPropertyName,
                    vcToolTip,
                    vcListItemType,
                    0 intColumnNum,
                    0 intRowNum,
                    ISNULL(bitIsRequired,0) boolRequired,
                    0 numAuthGroupID,
                    vcFieldDataType,
                    0 boolAOIField,
                    numFormID,
                    vcLookBackTableName,0 as GRP_ID,intFieldMaxLength
            FROM    View_DynamicDefaultColumns
            WHERE   numFormID = @numFormId 
					AND bitDeleted=0
					AND numDomainID = @numDomainId and intSectionID!=0
					 AND 1 = (CASE WHEN numFormId=29 AND numFieldID=311 THEN 0 ELSE 1 END)
			UNION
            
			SELECT 0 as intSectionID,0 as numModuleID,row_number() over (PARTITION BY GRP_ID order by GRP_ID),CONVERT(VARCHAR(15), Fld_id) + 'C' AS numFormFieldId,
                    Fld_label AS vcFormFieldName,
                    L.vcFieldType AS vcFieldType,
                    fld_type as vcAssociatedControlType,
                    ISNULL(C.numListID, 0) numListID,
                    CONVERT(VARCHAR(15), Fld_id) + 'C' vcDbColumnName,
                    '' AS vcPropertyName,
                    '' AS vcToolTip,
                    CASE WHEN C.numListID > 0 THEN 'LI' ELSE '' END vcListItemType,
                    0 intColumnNum,
                    0 intRowNum,
                    ISNULL(V.bitIsRequired,0) as boolRequired,
                    0 numAuthGroupID,
                    CASE WHEN C.numListID > 0 THEN 'N' ELSE 'V' END vcFieldDataType,
                    0 boolAOIField,
                    @numFormId AS numFormID,
                    '' AS vcLookBackTableName,GRP_ID,0 as intFieldMaxLength
            FROM    CFW_Fld_Master C LEFT JOIN CFW_Validation V ON V.numFieldID = C.Fld_id
					JOIN CFW_Loc_Master L on C.GRP_ID=L.Loc_Id
            WHERE   C.numDomainID = @numDomainId
					AND GRP_ID IN(	SELECT ID FROM dbo.SplitIDs(@vcLocationID,','))
   
END
Update #tempFieldsList set RowNO=RowNO-1
Update #tempFieldsList set  intRowNum=(RowNo/3)+1 ,intColumnNum= (RowNo%3)+1 

SELECT * FROM #tempFieldsList ORDER BY intRowNum,intColumnNum
        
DROP TABLE #tempFieldsList
        
GO
/****** Object:  StoredProcedure [dbo].[USP_GetBizDocAttachmnts]    Script Date: 07/26/2008 16:16:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj      
--exec USP_GetBizDocAttachmnts,
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getbizdocattachmnts')
DROP PROCEDURE usp_getbizdocattachmnts
GO
CREATE PROCEDURE [dbo].[usp_getbizdocattachmnts]      
 @numBizDocID as numeric(9)=0,      
 @numDomainId as numeric(9)=0,      
 @numAttachmntID as numeric(9)=0,
 @numBizDocTempID as numeric(9)=0     ,
 @intAddReferenceDocument AS NUMERIC(10)=0
as      
       
if (@numAttachmntID=0 and   @numBizDocID <>0)  
begin    
    
 if not exists(select *,'' AS vcFileName from BizDocAttachments where numBizDocID=@numBizDocID and numDomainID=@numDomainID and vcDocName='BizDoc' and isnull(numBizDocTempID,0)=@numBizDocTempID)        
  begin        
   insert into BizDocAttachments (numBizDocID,numDomainID,vcURL,numOrder,vcDocName,numBizDocTempID)        
   values (@numBizDocID,@numDomainID,'BizDoc',1,'BizDoc',@numBizDocTempID)        
  end       
     
 
  IF(@intAddReferenceDocument=1)
  BEGIN
	 (select numAttachmntID,vcDocName,vcURL,numBizDocID,numBizDocTempID,numOrder,(SELECT TOP 1 vcBizDocId FROM OpportunityBizDocs WHERE numOppBizDocsId=@numBizDocID) AS vcFileName from BizDocAttachments       
	 where numBizDocID=@numBizDocID and numDomainId=@numDomainId and isnull(numBizDocTempID,0)=@numBizDocTempID)
	 UNION
	 (SELECT numAttachmntID,vcDocName,vcURL,numBizDocID,numBizDocTempID,numOrder,'' as vcFileName from BizDocAttachments       
	 where vcURL<>'BizDoc' AND numBizDocID=(SELECT TOP 1 numBizDocId FROM OpportunityBizDocs WHERE numOppBizDocsId=@numBizDocID) and numDomainId=@numDomainId and isnull(numBizDocTempID,0)=(SELECT TOP 1 numBizDocTempID FROM OpportunityBizDocs WHERE numOppBizDocsId=@numBizDocID)  )
	order by numOrder 
  END
  ELSE
  BEGIN
	 select numAttachmntID,vcDocName,vcURL,numBizDocID,numBizDocTempID,'' AS vcFileName from BizDocAttachments       
  where numBizDocID=@numBizDocID and numDomainId=@numDomainId and isnull(numBizDocTempID,0)=@numBizDocTempID order by numOrder      
  END
end    
else if @numAttachmntID>0      
      
select *,'' AS vcFileName from BizDocAttachments where numAttachmntID=@numAttachmntID
GO
/****** Object:  StoredProcedure [dbo].[USP_GetDealsList1]    Script Date: 05/07/2009 22:12:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getdealslist1')
DROP PROCEDURE usp_getdealslist1
GO
CREATE PROCEDURE [dbo].[USP_GetDealsList1]
               @numUserCntID              NUMERIC(9)  = 0,
               @numDomainID               NUMERIC(9)  = 0,
               @tintSalesUserRightType    TINYINT  = 0,
               @tintPurchaseUserRightType TINYINT  = 0,
               @tintSortOrder             TINYINT  = 4,
               @CurrentPage               INT,
               @PageSize                  INT,
               @TotRecs                   INT  OUTPUT,
               @columnName                AS VARCHAR(300),
               @columnSortOrder           AS VARCHAR(10),
               @numCompanyID              AS NUMERIC(9)  = 0,
               @bitPartner                AS BIT  = 0,
               @ClientTimeZoneOffset      AS INT,
               @tintShipped               AS TINYINT,
               @intOrder                  AS TINYINT,
               @intType                   AS TINYINT,
               @tintFilterBy              AS TINYINT  = 0,
               @numOrderStatus			  AS NUMERIC,
			   @vcRegularSearchCriteria varchar(MAX)='',
			   @vcCustomSearchCriteria varchar(MAX)='',
			   @SearchText VARCHAR(300) = '',
			   @SortChar char(1)='0',
			   @tintDashboardReminderType TINYINT = 0
AS
BEGIN
	DECLARE @PageId  AS TINYINT
	DECLARE @numFormId  AS INT 
	DECLARE @tintDecimalPoints TINYINT

	SELECT
		@tintDecimalPoints=ISNULL(tintDecimalPoints,0)
	FROM
		Domain 
	WHERE 
		numDomainId=@numDomainID

	SET @PageId = 0

	IF @inttype = 1
	BEGIN
		SET @PageId = 2
		SET @inttype = 3
		SET @numFormId=39
	END
	ELSE IF @inttype = 2
	BEGIN
		SET @PageId = 6
		SET @inttype = 4
		SET @numFormId=41
	END

	CREATE TABLE #tempForm 
	(
		tintOrder TINYINT,vcDbColumnName nvarchar(200),vcOrigDbColumnName nvarchar(50),vcFieldName nvarchar(50),vcAssociatedControlType nvarchar(50),vcListItemType varchar(3),
		numListID numeric(9),vcLookBackTableName varchar(50),bitCustomField BIT,numFieldId NUMERIC,bitAllowSorting BIT,bitAllowEdit BIT,bitIsRequired BIT,bitIsEmail BIT,
		bitIsAlphaNumeric BIT,bitIsNumeric BIT,bitIsLengthValidation BIT,intMaxLength int,intMinLength int,bitFieldMessage BIT,vcFieldMessage VARCHAR(500),
		ListRelID numeric(9),intColumnWidth int,bitAllowFiltering BIT,vcFieldDataType CHAR(1)
	)

	-- FIRST CHECK IF GRID COLUMNS ARE CONFIGURED
	DECLARE  @Nocolumns  AS TINYINT = 0

	SELECT 
		@Nocolumns=ISNULL(SUM(TotalRow),0) 
	FROM
	(            
		SELECT 
			COUNT(*) TotalRow 
		FROM 
			View_DynamicColumns 
		WHERE 
			numFormId=@numFormId 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND numRelCntType = @numFormId
			AND 1 = (CASE WHEN @numFormId=39 AND numFieldID=771 THEN 0 ELSE 1 END)
		UNION 
		SELECT 
			COUNT(*) TotalRow 
		FROM 
			View_DynamicCustomColumns 
		WHERE 
			numFormId=@numFormId 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND numRelCntType = @numFormId
	) TotalRows

	-- IF GRID COLUMNS ARE NOT CONFIGURES THEN CHECK IF MASTER FORM CONFIGURATION IS CREATED BY ADMINISTRATOR
	DECLARE @IsMasterConfAvailable BIT = 0
	DECLARE @numUserGroup NUMERIC(18,0)

	IF @Nocolumns=0
	BEGIN
		SELECT @numUserGroup = numGroupID FROM UserMaster WHERE numUserDetailId = @numUserCntID 

		IF (SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = @numDomainID AND numFormID = @numFormId AND numRelCntType=@numFormId AND bitGridConfiguration = 1 AND numGroupID = @numUserGroup) > 0
		BEGIN
			SET @IsMasterConfAvailable = 1
		END
	END

	--IF MASTERCONFIGURATION IS AVAILABLE THEN LOAD IT OTHERWISE LOAD DEFAULT COLUMNS
	IF @IsMasterConfAvailable = 1
	BEGIN
		INSERT INTO DycFormConfigurationDetails 
		(
			numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth
		)
		SELECT 
			@numFormId,numFieldId,intColumnNum,intRowNum,@numDomainID,@numUserCntID,@numFormId,1,0,intColumnWidth
		FROM 
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId=@numFormId AND 
			View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicColumnsMasterConfig.numRelCntType=@numFormId AND 
			View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
			ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
			AND 1 = (CASE WHEN @numFormId=39 AND numFieldID=771 THEN 0 ELSE 1 END)
		UNION
		SELECT 
			@numFormId,numFieldId,intColumnNum,intRowNum,@numDomainID,@numUserCntID,@numFormId,1,1,intColumnWidth
		FROM 
			View_DynamicCustomColumnsMasterConfig
		WHERE 
			View_DynamicCustomColumnsMasterConfig.numFormId=@numFormId AND 
			View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicCustomColumnsMasterConfig.numRelCntType=@numFormId AND 
			View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1

		INSERT INTO 
			#tempForm
		SELECT 
			(intRowNum + 1) as tintOrder, vcDbColumnName,	vcOrigDbColumnName,	ISNULL(vcCultureFieldName,vcFieldName), vcAssociatedControlType,
			vcListItemType, numListID, vcLookBackTableName,	bitCustom, numFieldId, bitAllowSorting,	bitInlineEdit, bitIsRequired,
			bitIsEmail,	bitIsAlphaNumeric, bitIsNumeric, bitIsLengthValidation,	intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,
			ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType 
		FROM 
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId=@numFormId AND 
			View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicColumnsMasterConfig.numRelCntType=@numFormId AND 
			View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
			ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
		UNION
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName, vcAssociatedControlType,'' as vcListItemType,numListID,'',
			bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,''
		FROM 
			View_DynamicCustomColumnsMasterConfig
		WHERE 
			View_DynamicCustomColumnsMasterConfig.numFormId=@numFormId AND 
			View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicCustomColumnsMasterConfig.numRelCntType=@numFormId AND 
			View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1
		ORDER BY 
			tintOrder ASC  
	END
	ELSE                                        
	BEGIN
		IF @Nocolumns=0
		BEGIN
			INSERT INTO DycFormConfigurationDetails 
			(
				numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth
			)
			SELECT 
				@numFormId,numFieldId,0,Row_number() over(order by tintRow desc),@numDomainID,@numUserCntID,@numFormId,1,0,intColumnWidth
			FROM 
				View_DynamicDefaultColumns
			WHERE 
				numFormId=@numFormId 
				AND bitDefault=1 
				AND ISNULL(bitSettingField,0)=1 
				AND numDomainID=@numDomainID
				AND 1 = (CASE WHEN @numFormId=39 AND numFieldID=771 THEN 0 ELSE 1 END)
			ORDER BY 
				tintOrder asc   
		END

		INSERT INTO 
			#tempForm
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcOrigDbColumnName,ISNULL(vcCultureFieldName,vcFieldName),                  
			vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName,bitCustom,numFieldId,bitAllowSorting,bitInlineEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength,bitFieldMessage,
			vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType  
		FROM 
			View_DynamicColumns 
		WHERE 
			numFormId=@numFormId 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND ISNULL(bitSettingField,0)=1 
			AND ISNULL(bitCustom,0)=0  
			AND numRelCntType = @numFormId
			AND 1 = (CASE WHEN @numFormId=39 AND numFieldID=771 THEN 0 ELSE 1 END)
		UNION
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName,vcAssociatedControlType,'' as vcListItemType,numListID,'',bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,'' 
		FROM 
			View_DynamicCustomColumns
		WHERE 
			numFormId=@numFormId 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND ISNULL(bitCustom,0)=1 
			AND numRelCntType = @numFormId
		ORDER BY 
			tintOrder asc  
	END
	UPDATE #tempForm SET vcFieldMessage = CAST((SELECT numFieldId FROM DycFieldMaster WHERE vcDbColumnName='intUsedShippingCompany' AND numModuleID=3)  AS varchar)+'#'+CAST((SELECT numFieldId FROM DycFieldMaster WHERE vcDbColumnName='numShippingService' AND numModuleID=3)  AS varchar) 
	WHERE vcDbColumnName='vcOrderedShipped'
	UPDATE #tempForm SET vcFieldMessage = CAST((SELECT numFieldId FROM DycFieldMaster WHERE vcDbColumnName='monAmountPaid' AND numModuleID=3)  AS varchar)
	WHERE vcDbColumnName='vcPOppName'
	DECLARE  @strColumns AS VARCHAR(MAX)
	SET @strColumns = ' ADC.numContactId, ADC.vcEmail, Div.numDivisionID,ISNULL(Div.numTerID,0) numTerID, opp.numRecOwner as numRecOwner, Div.tintCRMType, opp.numOppId, ISNULL(Opp.monDealAmount,0) monDealAmount 
	,ISNULL(opp.numShippingService,0) AS numShippingService,ADC.vcFirstName+'' ''+ADC.vcLastName AS vcContactName,ADC.numPhone AS numContactPhone,ADC.numPhoneExtension AS numContactPhoneExtension,ADC.vcEmail AS vcContactEmail,Opp.vcPOppName,cmp.vcCompanyName,ISNULL((SELECT SUM(ISNULL(monAmountPaid,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0) as monAmountPaid '
	--Corr.bitIsEmailSent,

	DECLARE  @tintOrder  AS TINYINT;SET @tintOrder = 0
	DECLARE  @vcFieldName  AS VARCHAR(50)
	DECLARE  @vcListItemType  AS VARCHAR(3)
	DECLARE  @vcListItemType1  AS VARCHAR(1)
	DECLARE  @vcAssociatedControlType VARCHAR(30)
	DECLARE  @numListID  AS NUMERIC(9)
	DECLARE  @vcDbColumnName VARCHAR(200)
	DECLARE  @WhereCondition VARCHAR(2000);SET @WhereCondition = ''
	DECLARE  @vcLookBackTableName VARCHAR(2000)
	DECLARE  @bitCustom  AS BIT
	Declare @numFieldId as numeric  
	DECLARE @bitAllowSorting AS CHAR(1)   
	DECLARE @bitAllowEdit AS CHAR(1)                 
	DECLARE @vcColumnName AS VARCHAR(500)          
	
	Declare @ListRelID as numeric(9) 
	DECLARE @SearchQuery AS VARCHAR(MAX) = ''        

	SELECT TOP 1 
		@tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName,@vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,
		@numListID=numListID,@vcLookBackTableName=vcLookBackTableName ,@bitCustom=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,@bitAllowEdit=bitAllowEdit,
		@ListRelID=ListRelID
	FROM 
		#tempForm 
	ORDER BY 
		tintOrder ASC            

	WHILE @tintOrder > 0
    BEGIN
		IF @bitCustom = 0
		BEGIN
			DECLARE  @Prefix  AS VARCHAR(10)

			IF @vcLookBackTableName = 'AdditionalContactsInformation'
				SET @Prefix = 'ADC.'
			IF @vcLookBackTableName = 'DivisionMaster'
				SET @Prefix = 'Div.'
			IF @vcLookBackTableName = 'OpportunityMaster'
				SET @PreFix = 'Opp.'
			IF @vcLookBackTableName = 'OpportunityRecurring'
				SET @PreFix = 'OPR.'
			IF @vcLookBackTableName = 'CompanyInfo'
				SET @PreFix = 'cmp.'
			IF @vcLookBackTableName = 'ProjectProgress'
				SET @PreFix = 'PP.'
			SET @vcColumnName= @vcDbColumnName+'~'+ convert(varchar(10),@numFieldId) +'~0'

			IF @vcAssociatedControlType = 'SelectBox'
			BEGIN
				-- KEEP THIS IF CONDITION SEPERATE FROM IF ELSE
				IF @vcDbColumnName = 'numStatus'
				BEGIN
					SET @strColumns=@strColumns+ ' ,ISNULL(TEMPApproval.numApprovalCount,0) ApprovalMarginCount'
				END


				IF @vcDbColumnName = 'numPartner'
				BEGIN
					SET @strColumns=@strColumns+ ' ,(Opp.numPartner) '+' ['+ @vcColumnName+']'    
					SET @strColumns=@strColumns+ ' ,(SELECT D.vcPartnerCode+''-''+C.vcCompanyName FROM DivisionMaster AS D LEFT JOIN CompanyInfo AS C ON D.numCompanyID=C.numCompanyID WHERE D.numDivisionID=Opp.numPartner) AS vcPartner' 
				END
				ELSE IF @vcDbColumnName = 'vcSignatureType'
				BEGIN
					SET @strColumns=@strColumns+ ' ,(SELECT CASE WHEN vcSignatureType = 0 THEN ''Service Default''
																	WHEN vcSignatureType = 1 THEN ''Adult Signature Required''
																	WHEN vcSignatureType = 2 THEN ''Direct Signature''
																	WHEN vcSignatureType = 3 THEN ''InDirect Signature''
																	WHEN vcSignatureType = 4 THEN ''No Signature Required''
																ELSE '''' END 
					FROM DivisionMaster AS D INNER JOIN DivisionMasterShippingConfiguration AS DMSC ON DMSC.numDivisionID=D.numDivisionID 
					WHERE D.numDivisionID=Div.numDivisionID) '  + ' [' + @vcColumnName + ']'
				END
				ELSE IF @vcDbColumnName = 'numShippingService'
				BEGIN
					SET @strColumns=@strColumns+ ' ,ISNULL((SELECT vcShipmentService FROM ShippingService WHERE (numDomainID=Opp.numDomainID OR ISNULL(numDomainID,0)=0) AND numShipmentServiceID = ISNULL(Opp.numShippingService,0)),'''')' + ' [' + @vcColumnName + ']'
				END
				ELSE IF @vcDbColumnName = 'tintEDIStatus'
				BEGIN
					--SET @strColumns = @strColumns + ',(CASE Opp.tintEDIStatus WHEN 1 THEN ''850 Received'' WHEN 2 THEN ''850 Acknowledged'' WHEN 3 THEN ''940 Sent'' WHEN 4 THEN ''940 Acknowledged'' WHEN 5 THEN ''856 Received'' WHEN 6 THEN ''856 Acknowledged'' WHEN 7 THEN ''856 Sent'' ELSE '''' END)' + ' [' + @vcColumnName + ']'
					SET @strColumns = @strColumns + ',(CASE Opp.tintEDIStatus WHEN 2 THEN ''850 Acknowledged'' WHEN 3 THEN ''940 Sent'' WHEN 4 THEN ''940 Acknowledged'' WHEN 5 THEN ''856 Received'' WHEN 6 THEN ''856 Acknowledged'' WHEN 7 THEN ''856 Sent'' WHEN 11 THEN ''850 Partially Created'' WHEN 12 THEN ''850 SO Created'' ELSE '''' END)' + ' [' + @vcColumnName + ']'
				END
				ELSE IF @vcDbColumnName = 'numPartenerContact'
				BEGIN
					SET @strColumns=@strColumns+ ' ,(Opp.numPartenerContact) '+' ['+ @vcColumnName+']'    
					SET @strColumns=@strColumns+ ' ,(SELECT A.vcFirstName+'' ''+ A.vcLastName FROM AdditionalContactsInformation AS A WHERE A.numContactId=Opp.numPartenerContact) AS vcPartenerContact' 
				END
				ELSE IF @vcDbColumnName = 'tintInvoicing'
				BEGIN
					SET @strColumns=CONCAT(@strColumns,', CONCAT(ISNULL((SELECT SUBSTRING((SELECT ''$^$'' + CONCAT(numOppBizDocsId,''#^#'',vcBizDocID,''#^#'',ISNULL(bitisemailsent,0),''#^#'',ISNULL(monDealAmount,0),''#^#'',ISNULL(monAmountPaid,0),''#^#'', convert(varchar(10), cast(dtFromDate as date), 101),''#^#'')
										FROM OpportunityBizDocs WHERE numOppId=Opp.numOppId AND numBizDocId = ISNULL((SELECT TOP 1 numAuthoritativeSales FROM AuthoritativeBizDocs WHERE numDomainId=' + CAST (@numDomainID AS VARCHAR) + '),287) FOR XML PATH('''')),4,200000)),'''')',
										CASE 
										WHEN CHARINDEX('tintInvoicing=3',@vcRegularSearchCriteria) > 0
											THEN ', (SELECT 
													CONCAT(''('',SUM(X.InvoicedQty),'' Out of '',SUM(X.OrderedQty),'')'')
												FROM 
												(
													SELECT
														OI.numoppitemtCode,
														ISNULL(OI.numUnitHour,0) AS OrderedQty,
														ISNULL(TempInvoice.InvoicedQty,0) AS InvoicedQty
													FROM
														OpportunityItems OI
													INNER JOIN
														Item I
													ON
														OI.numItemCode = I.numItemCode
													OUTER APPLY
													(
														SELECT
															SUM(OpportunityBizDocItems.numUnitHour) AS InvoicedQty
														FROM
															OpportunityBizDocs
														INNER JOIN
															OpportunityBizDocItems 
														ON
															OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
														WHERE
															OpportunityBizDocs.numOppId = Opp.numOppId
															AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
															AND ISNULL(OpportunityBizDocs.bitAuthoritativeBizDocs,0) = 1
													) AS TempInvoice
													WHERE
														OI.numOppID = Opp.numOppId
												) X)'

											WHEN CHARINDEX('tintInvoicing=4',@vcRegularSearchCriteria) > 0
											THEN ', (SELECT 
													CONCAT(''('',(X.InvoicedPercentage),'' % '','')'')
												FROM 
												(

													SELECT fltBreakupPercentage  AS InvoicedPercentage
													FROM dbo.OpportunityRecurring OPR 
														INNER JOIN dbo.OpportunityMaster OM ON OPR.numOppID = OM.numOppID
														LEFT JOIN dbo.OpportunityBizDocs OBD ON OBD.numOppBizDocsID = OPR.numOppBizDocID
													WHERE OPR.numOppId = Opp.numOppId 
														and tintRecurringType <> 4
												) X)'

											ELSE 
													','''''
											END,') [', @vcColumnName,']')


				END
				ELSE IF @vcDbColumnName = 'tintSource'
				BEGIN
					SET @strColumns = @strColumns + ',CONCAT(ISNULL(dbo.fn_GetOpportunitySourceValue(ISNULL(Opp.tintSource,0),ISNULL(Opp.tintSourceType,0),Opp.numDomainID),''Internal Order''),(CASE WHEN ISNULL(TEMPMapping.numMappingCount,0) > 0 THEN CONCAT(''&nbsp;<a target="_blank" href="../opportunity/frmOpportunities.aspx?frm=deallist&OpID='',Opp.numOppID,''&SelectedTab=ProductsServices"><i class="fa fa-map-marker" style="color:red;font-size:18px" aria-hidden="true"></i></a>'') ELSE '''' END))' + ' [' + @vcColumnName + ']'

					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'ISNULL(dbo.fn_GetOpportunitySourceValue(ISNULL(Opp.tintSource,0),ISNULL(Opp.tintSourceType,0),Opp.numDomainID),''Internal Order'') LIKE ''%' + @SearchText + '%'''
					END
				END
				ELSE IF @vcDbColumnName = 'numCampainID'
				BEGIN
					SET @strColumns = @strColumns + ', (SELECT vcCampaignName FROM CampaignMaster WHERE numCampaignID= Opp.numCampainID) ' + ' [' + @vcColumnName + ']'
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'ISNULL((SELECT vcCampaignName FROM CampaignMaster WHERE numCampaignID= Opp.numCampainID),'''') LIKE ''%' + @SearchText + '%'''
					END
				END
				ELSE IF @vcDbColumnName = 'numContactID'
				BEGIN
					SET @strColumns = @strColumns + ',dbo.fn_GetContactName(' + @Prefix + @vcDbColumnName + ') [' + @vcColumnName + ']'
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'dbo.fn_GetContactName(' + @Prefix + @vcDbColumnName + ') LIKE ''%' + @SearchText + '%'''
					END
				END
				ELSE IF @vcListItemType = 'LI'
				BEGIN
					SET @strColumns = @strColumns + ',L' + CONVERT(VARCHAR(3),@tintOrder) + '.vcData' + ' [' + @vcColumnName + ']'
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'L' + CONVERT(VARCHAR(3),@tintOrder) + '.vcData LIKE ''%' + @SearchText + '%'''
					END

					SET @WhereCondition = @WhereCondition + ' left Join ListDetails L' + CONVERT(VARCHAR(3),@tintOrder) + ' on L' + CONVERT(VARCHAR(3),@tintOrder) + '.numListItemID=' + @vcDbColumnName
				END
				ELSE IF @vcListItemType = 'S'
				BEGIN
					SET @strColumns = @strColumns + ',S' + CONVERT(VARCHAR(3),@tintOrder) + '.vcState' + ' [' + @vcColumnName + ']'
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'S' + CONVERT(VARCHAR(3),@tintOrder) + '.vcState LIKE ''%' + @SearchText + '%'''
					END

					SET @WhereCondition = @WhereCondition + ' left join State S' + CONVERT(VARCHAR(3),@tintOrder) + ' on S' + CONVERT(VARCHAR(3),@tintOrder) + '.numStateID=' + @vcDbColumnName
				END
				ELSE IF @vcListItemType = 'BP'
				BEGIN
					SET @strColumns = @strColumns + ',SPLM.Slp_Name' + ' [' + @vcColumnName + ']'
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'SPLM.Slp_Name LIKE ''%' + @SearchText + '%'''
					END

					SET @WhereCondition = @WhereCondition + ' LEFT JOIN Sales_process_List_Master SPLM on SPLM.Slp_Id = OPP.numBusinessProcessID '
				END
				ELSE IF @vcListItemType = 'PP'
				BEGIN
					SET @strColumns = @strColumns + ',ISNULL(PP.intTotalProgress,0)' + ' [' + @vcColumnName + ']'

					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'ISNULL(PP.intTotalProgress,0) LIKE ''%' + @SearchText + '%'''
					END
				END
				ELSE IF @vcListItemType = 'T'
				BEGIN
					SET @strColumns = @strColumns + ',L' + CONVERT(VARCHAR(3),@tintOrder) + '.vcData' + ' [' + @vcColumnName + ']'
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'L' + CONVERT(VARCHAR(3),@tintOrder) + '.vcData LIKE ''%' + @SearchText + '%'''
					END

					SET @WhereCondition = @WhereCondition + ' left Join ListDetails L' + CONVERT(VARCHAR(3),@tintOrder) + ' on L' + CONVERT(VARCHAR(3),@tintOrder) + '.numListItemID=' + @vcDbColumnName
				END
				ELSE IF @vcListItemType = 'U'
				BEGIN
					SET @strColumns = @strColumns + ',dbo.fn_GetContactName(' + @Prefix + @vcDbColumnName + ') [' + @vcColumnName + ']'
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'dbo.fn_GetContactName(' + @Prefix + @vcDbColumnName + ') LIKE ''%' + @SearchText + '%'''
					END
				END
				ELSE IF @vcListItemType = 'WI'
				BEGIN
					SET @strColumns = @strColumns + ', Case when isnull(OL.numSiteID,0)>0 then Sites.vcSiteName else WebAPI.vcProviderName end [' + @vcColumnName + ']'
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + '(Case when isnull(OL.numSiteID,0)>0 then Sites.vcSiteName else WebAPI.vcProviderName end) LIKE ''%' + @SearchText + '%'''
					END

					set @WhereCondition= @WhereCondition +' left JOIN OpportunityLinking OL on OL.numChildOppID=Opp.numOppId left join WebAPI on WebAPI.WebApiId=isnull(OL.numWebApiId,0)
													left join Sites on isnull(OL.numSiteID,0)=Sites.numSIteID and Sites.numDomainID=' + convert(varchar(15),@numDomainID)                                                       
				END 
            END
			ELSE IF @vcAssociatedControlType = 'DateField'
			BEGIN
				IF @Prefix ='OPR.'
				BEGIN
					SET @strColumns = @strColumns + ', (SELECT TOP 1 dbo.FormatedDateFromDate( DateAdd(minute,+'+convert(varchar(15),-@ClientTimeZoneOffset)+', dtRecurringDate)'+ ','+ CONVERT(VARCHAR(10),@numDomainId) +') FROM  dbo.OpportunityRecurring WHERE numOppID=OPP.numOppID AND ISNULL(numOppBizDocID,0)=0 and dtRecurringDate > dbo.GetUTCDateWithoutTime()) AS  ['+ @vcColumnName + ']'

					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + '(SELECT TOP 1 dbo.FormatedDateFromDate( DateAdd(minute,+'+convert(varchar(15),-@ClientTimeZoneOffset)+', dtRecurringDate)'+ ','+ CONVERT(VARCHAR(10),@numDomainId) +') FROM  dbo.OpportunityRecurring WHERE numOppID=OPP.numOppID AND ISNULL(numOppBizDocID,0)=0 and dtRecurringDate > dbo.GetUTCDateWithoutTime()) LIKE ''%' + @SearchText + '%'''
					END
				END
				ELSE IF @vcDbColumnName = 'dtReleaseDate'
				BEGIN
					SET @strColumns = @strColumns
                                + ',case when convert(varchar(11),'
                                + @Prefix
                                + @vcDbColumnName
                                + ')= convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',getdate())) then ''<b><font color=red>Today</font></b>'''
					SET @strColumns = @strColumns
									+ 'when convert(varchar(11),'
									+ @Prefix
									+ @vcDbColumnName
									+ ')= convert(varchar(11),dateadd(day,-1,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',getdate()))) then ''<b><font color=purple>Yesterday</font></b>'''
					SET @strColumns = @strColumns
									+ 'when convert(varchar(11),'
									+ @Prefix
									+ @vcDbColumnName
									+ ')= convert(varchar(11),dateadd(day,1,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',getdate()))) then''<b><font color=orange>Tommorow</font></b>'' '
					SET @strColumns = @strColumns
									+ 'else dbo.FormatedDateFromDate('
									+ @Prefix
									+ @vcDbColumnName
									+ ','
									+ CONVERT(VARCHAR(10),@numDomainId)
									+ ') end  ['
									+ @vcColumnName + ']' 	

					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + @Prefix + @vcDbColumnName + ' LIKE ''%' + @SearchText + '%'''
					END

				END
				ELSE
				BEGIN
					SET @strColumns = @strColumns
                                + ',case when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+','
                                + @Prefix
                                + @vcDbColumnName
                                + ') )= convert(varchar(11),getdate()) then ''<b><font color=red>Today</font></b>'''
					SET @strColumns = @strColumns
									+ 'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+','
									+ @Prefix
									+ @vcDbColumnName
									+ ') )= convert(varchar(11),dateadd(day,-1,getdate())) then ''<b><font color=purple>Yesterday</font></b>'''
					SET @strColumns = @strColumns
									+ 'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+','
									+ @Prefix
									+ @vcDbColumnName
									+ ') )= convert(varchar(11),dateadd(day,1,getdate())) then''<b><font color=orange>Tommorow</font></b>'' '
					SET @strColumns = @strColumns
									+ 'else dbo.FormatedDateFromDate(DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+','
									+ @Prefix
									+ @vcDbColumnName
									+ '),'
									+ CONVERT(VARCHAR(10),@numDomainId)
									+ ') end  ['
									+ @vcColumnName + ']' 	

					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + @Prefix + @vcDbColumnName + ' LIKE ''%' + @SearchText + '%'''
					END
				 END	
            END
            ELSE
            IF @vcAssociatedControlType = 'TextBox'
            BEGIN
				IF @vcDbColumnName = 'vcCompactContactDetails'
				BEGIN
					SET @strColumns=@strColumns+ ' ,'''' AS vcCompactContactDetails'   
				END 
				ELSE
				BEGIN 
                SET @strColumns = @strColumns
                                  + ','
                                  + CASE 
                                      WHEN @vcDbColumnName = 'numAge' THEN 'year(getutcdate())-year(bintDOB)'
                                      WHEN @vcDbColumnName = 'monPAmount' THEN 'isnull((SELECT varCurrSymbol FROM [Currency] WHERE numCurrencyID = Opp.[numCurrencyID]),'''') + '' ''+ CONVERT(VARCHAR,CONVERT(DECIMAL(20,5),monPAmount),1)'
                                      WHEN @vcDbColumnName = 'numDivisionID' THEN 'Div.numDivisionID'
                                      WHEN @vcDbColumnName = 'tintOppStatus' THEN 'CASE tintOppStatus WHEN 0 then ''Open'' WHEN 1 THEN ''Closed-Won'' WHEN 2 THEN ''Closed-Lost'' END '
                                      WHEN @vcDbColumnName = 'tintOppType' THEN 'CASE tintOppType WHEN 1 then ''Sales'' WHEN 2 THEN ''Purchase'' Else ''NA'' END '
									  WHEN @vcDbColumnName = 'vcPOppName' THEN 'dbo.CheckChildRecord(Opp.numOppId,Opp.numDomainID)'
									  WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'vcBizDocsList' THEN 'ISNULL((SELECT SUBSTRING((SELECT ''$^$'' + CAST(numOppBizDocsId AS VARCHAR(18)) +''#^#''+ vcBizDocID
										FROM OpportunityBizDocs WHERE numOppId=Opp.numOppId AND 1 = (Case When bitAuthoritativeBizDocs=1 AND 1=' + (CASE WHEN @inttype=1 THEN '1' ELSE '0' END) + '  Then 0 ELSE 1 END) FOR XML PATH('''')),4,200000)),'''')'
                                      ELSE @Prefix + @vcDbColumnName
                                    END
                                  + ' ['
                                  + @vcColumnName + ']'
				END
				IF(@vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'vcBizDocsList')
				BEGIN
					SET @strColumns=@strColumns+','+' (SELECT COUNT(I.vcItemName) FROM 
													 OpportunityItems AS t LEFT JOIN  Item as I ON 
													 t.numItemCode=I.numItemCode WHERE 
													 t.numOppId=Opp.numOppId AND t.numoppItemtCode NOT IN 
													 (SELECT numOppItemId FROM OpportunityBizDocs JOIN 
													 OpportunityBizDocItems ON OpportunityBizDocs.numOppBizDocsId=OpportunityBizDocItems.numOppBizDocID  
													 WHERE OpportunityBizDocs.numOppId=Opp.numOppId 
													 AND 1 = (Case When bitAuthoritativeBizDocs=1 Or numBizDocID=293 Then 0 ELSE 1 END) 
													 AND ISNULL(bitAuthoritativeBizDocs,0)=1)) AS [List_Item_Approval_UNIT] '
				END
				
				IF LEN(@SearchText) > 0
				BEGIN
					SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + (CASE 
                                      WHEN @vcDbColumnName = 'numAge' THEN 'year(getutcdate())-year(bintDOB)'
                                      WHEN @vcDbColumnName = 'monPAmount' THEN 'isnull((SELECT varCurrSymbol FROM [Currency] WHERE numCurrencyID = Opp.[numCurrencyID]),'''') + '' ''+ CONVERT(VARCHAR,CONVERT(DECIMAL(20,5),monPAmount),1)'
                                      WHEN @vcDbColumnName = 'numDivisionID' THEN 'Div.numDivisionID'
                                      WHEN @vcDbColumnName = 'tintOppStatus' THEN 'CASE tintOppStatus WHEN 0 then ''Open'' WHEN 1 THEN ''Closed-Won'' WHEN 2 THEN ''Closed-Lost'' END '
                                      WHEN @vcDbColumnName = 'tintOppType' THEN 'CASE tintOppType WHEN 1 then ''Sales'' WHEN 2 THEN ''Purchase'' Else ''NA'' END '
									  WHEN @vcDbColumnName = 'vcPOppName' THEN 'dbo.CheckChildRecord(Opp.numOppId,Opp.numDomainID)'
									  WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'vcBizDocsList' THEN 'ISNULL((SELECT SUBSTRING((SELECT ''$^$'' + CAST(numOppBizDocsId AS VARCHAR(18)) +''#^#''+ vcBizDocID
										FROM OpportunityBizDocs WHERE numOppId=Opp.numOppId AND 1 = (Case When bitAuthoritativeBizDocs=1 Or numBizDocID=293 Then 0 ELSE 1 END)  FOR XML PATH('''')),4,200000)),'''')'
                                      ELSE @Prefix + @vcDbColumnName
                                    END) + ' LIKE ''%' + @SearchText + '%'''
				END
            END
            ELSE
            IF @vcAssociatedControlType = 'TextArea'
            BEGIN
				SET @strColumns=@strColumns+','+  @Prefix + @vcDbColumnName +' ['+ @vcColumnName+']' 

				IF LEN(@SearchText) > 0
				BEGIN
					SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + @Prefix + @vcDbColumnName + ' LIKE ''%' + @SearchText + '%'''
				END
            END
            ELSE IF  @vcAssociatedControlType='Label'                                              
			BEGIN
				SET @strColumns=@strColumns + ',' + CASE               
				WHEN @vcDbColumnName = 'vcTrackingDetail' THEN 'STUFF((SELECT 
																			CONCAT(''<br/>'',OpportunityBizDocs.vcBizDocID,'': '',vcTrackingDetail)
																		FROM 
																			ShippingReport 
																		INNER JOIN 
																			OpportunityBizDocs 
																		ON 
																			ShippingReport.numOppBizDocId=OpportunityBizDocs.numOppBizDocsId 
																		WHERE 
																			ShippingReport.numOppId=Opp.numOppID
																			AND ISNULL(ShippingReport.vcTrackingDetail,'''') <> ''''
																		FOR XML PATH(''''), TYPE).value(''(./text())[1]'',''varchar(max)''), 1, 5, '''')'     
				WHEN @vcDbColumnName = 'CalAmount' THEN 'Opp.monDealAmount'--'[dbo].[getdealamount](Opp.numOppId,Getutcdate(),0)'
				WHEN @vcDbColumnName = 'vcInventoryStatus' THEN 'ISNULL(dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID,1),'''')'
				WHEN @vcDbColumnName = 'vcOrderedShipped' THEN CONCAT('dbo.GetOrderShipReceiveStatus(Opp.numOppId,1,Opp.tintshipped,',@tintDecimalPoints,')')
				WHEN @vcDbColumnName = 'vcOrderedReceived' THEN CONCAT('dbo.GetOrderShipReceiveStatus(Opp.numOppId,2,Opp.tintshipped,',@tintDecimalPoints,')')
				WHEN @vcDbColumnName = 'vcShipStreet' THEN 'dbo.fn_getOPPState(Opp.numOppId,Opp.numDomainID,2)'
				WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'monDealAmount' THEN 'ISNULL((SELECT SUM(ISNULL(monDealAmount,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)'
				WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'monAmountPaid' THEN 'ISNULL((SELECT SUM(ISNULL(monAmountPaid,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)'
				ELSE @Prefix + @vcDbColumnName END +' ['+ @vcColumnName+']'   
				
				IF @vcDbColumnName='vcOrderedShipped'
				BEGIN
				DECLARE @temp VARCHAR(MAX)
				SET @temp = '(SELECT 
				STUFF((SELECT '', '' + CAST((CAST(numOppBizDocsId AS varchar)+''~''+CAST(ISNULL(numShippingReportId,0) AS varchar) ) AS VARCHAR(MAX)) [text()]
				FROM OpportunityBizDocs
				LEFT JOIN ShippingReport
				ON ShippingReport.numOppBizDocId=OpportunityBizDocs.numOppBizDocsId
				WHERE OpportunityBizDocs.numOppId=Opp.numOppId  AND OpportunityBizDocs.bitShippingGenerated=1  FOR XML PATH(''''), TYPE)
						.value(''.'',''NVARCHAR(MAX)''),1,2,'' ''))'
					SET @strColumns = @strColumns + ' , (SELECT SUBSTRING('+ @temp +', 2, 200000))AS ShippingIcons '
					SET @strColumns = @strColumns + ' , CASE WHEN (SELECT COUNT(*) FROM ShippingBox INNER JOIN ShippingReport ON ShippingBox.numShippingReportId = ShippingReport.numShippingReportId WHERE ShippingReport.numOppID = Opp.numOppID AND LEN(vcTrackingNumber) > 0) > 0 THEN 1 ELSE 0 END [ISTrackingNumGenerated]'
					SET @strColumns = @strColumns + ' ,dbo.fn_GetListItemName(intUsedShippingCompany) AS ShipVia '
					SET @strColumns = @strColumns + ' ,Opp.intUsedShippingCompany AS intUsedShippingCompany '
					--SET @strColumns = @strColumns + ' ,Opp.numShippingService AS numShippingService '
					SET @strColumns = @strColumns + ' ,(SELECT numFieldId FROM DycFieldMaster WHERE vcDbColumnName=''intUsedShippingCompany'' AND numModuleID=3) AS ShipViaFieldId '
					SET @strColumns = @strColumns + ' ,(SELECT numFieldId FROM DycFieldMaster WHERE vcDbColumnName=''numShippingService'' AND numModuleID=3) AS ShippingServiceFieldId '
					SET @strColumns=@strColumns+ ' ,ISNULL((SELECT vcShipmentService FROM ShippingService WHERE (numDomainID=Opp.numDomainID OR ISNULL(numDomainID,0)=0) AND numShipmentServiceID = ISNULL(Opp.numShippingService,0)),'''')' + ' AS vcShipmentService'
				
				END

				IF LEN(@SearchText) > 0
				BEGIN
					SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + (CASE                    
					WHEN @vcDbColumnName = 'CalAmount' THEN 'Opp.monDealAmount'
					WHEN @vcDbColumnName = 'vcInventoryStatus' THEN 'ISNULL(dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID,1),'''')'
					WHEN @vcDbColumnName = 'vcOrderedShipped' THEN CONCAT('dbo.GetOrderShipReceiveStatus(Opp.numOppId,1,Opp.tintshipped,',@tintDecimalPoints,')')
					WHEN @vcDbColumnName = 'vcOrderedReceived' THEN CONCAT('dbo.GetOrderShipReceiveStatus(Opp.numOppId,2,Opp.tintshipped,',@tintDecimalPoints,')')
					WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'monDealAmount' THEN 'ISNULL((SELECT SUM(ISNULL(monDealAmount,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)'
					WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'monAmountPaid' THEN 'ISNULL((SELECT SUM(ISNULL(monAmountPaid,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)'
					else @Prefix + @vcDbColumnName END) + ' LIKE ''%' + @SearchText + '%'''
				END    
			END
            ELSE IF @vcAssociatedControlType='Popup' and @vcDbColumnName='numShareWith'                                                   
			BEGIN
				SET @strColumns = @strColumns + ',(SELECT SUBSTRING(
								(SELECT '','' + A.vcFirstName+'' ''+A.vcLastName + CASE WHEN ISNULL(SR.numContactType,0)>0 THEN ''('' + dbo.fn_GetListItemName(SR.numContactType) + '')'' ELSE '''' END
								FROM ShareRecord SR JOIN UserMaster UM ON UM.numUserDetailId=SR.numAssignedTo 
								JOIN AdditionalContactsInformation A ON UM.numUserDetailId=A.numContactID 
								WHERE SR.numDomainID=Opp.numDomainID AND SR.numModuleID=3 AND SR.numRecordID=Opp.numOppId
								AND UM.numDomainID=Opp.numDomainID and UM.numDomainID=A.numDomainID FOR XML PATH('''')),2,200000)) ['+ @vcColumnName+']'                
			END 
        END
		ELSE
        BEGIN
			SET @vcColumnName= @vcDbColumnName +'~'+ convert(varchar(10),@numFieldId)+'~1'

			IF @vcAssociatedControlType = 'TextBox' or @vcAssociatedControlType = 'TextArea'
            BEGIN
              SET @strColumns = @strColumns
                              + ',CFW'
                              + CONVERT(VARCHAR(3),@tintOrder)
                              + '.Fld_Value  ['
                              + @vcColumnName + ']'
              SET @WhereCondition = @WhereCondition
                                      + ' left Join CFW_FLD_Values_Opp CFW'
                                      + CONVERT(VARCHAR(3),@tintOrder)
                                      + ' on CFW'
                                      + CONVERT(VARCHAR(3),@tintOrder)
                                      + '.Fld_Id='
                                      + CONVERT(VARCHAR(10),@numFieldId)
                                      + 'and CFW'
                                      + CONVERT(VARCHAR(3),@tintOrder)
                                      + '.RecId=Opp.numOppid   '
			END
			ELSE
            IF @vcAssociatedControlType = 'CheckBox' 
            BEGIN
                SET @strColumns = @strColumns
                                + ',case when isnull(CFW'
                                + CONVERT(VARCHAR(3),@tintOrder)
                                + '.Fld_Value,0)=0 then 0 when isnull(CFW'
                                + CONVERT(VARCHAR(3),@tintOrder)
                                + '.Fld_Value,0)=1 then 1 end   ['
                                + @vcColumnName + ']'
                SET @WhereCondition = @WhereCondition
                                        + ' left Join CFW_FLD_Values_Opp CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + ' on CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.Fld_Id='
                                        + CONVERT(VARCHAR(10),@numFieldId)
                                        + 'and CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.RecId=Opp.numOppid   '
            END
            ELSE
            IF @vcAssociatedControlType = 'DateField'
            BEGIN
                  SET @strColumns = @strColumns
                                  + ',dbo.FormatedDateFromDate(CFW'
                                  + CONVERT(VARCHAR(3),@tintOrder)
                                  + '.Fld_Value,'
                                  + CONVERT(VARCHAR(10),@numDomainId)
                                  + ')  ['
                                  + @vcColumnName + ']'
                  SET @WhereCondition = @WhereCondition
                                          + ' left Join CFW_FLD_Values_Opp CFW'
                                          + CONVERT(VARCHAR(3),@tintOrder)
                                          + ' on CFW'
                                          + CONVERT(VARCHAR(3),@tintOrder)
                                          + '.Fld_Id='
                                          + CONVERT(VARCHAR(10),@numFieldId)
                                          + 'and CFW'
                                          + CONVERT(VARCHAR(3),@tintOrder)
                                          + '.RecId=Opp.numOppid    '
            END
            ELSE
            IF @vcAssociatedControlType = 'SelectBox'
            BEGIN
                SET @vcDbColumnName = 'DCust'
                                            + CONVERT(VARCHAR(10),@numFieldId)
                SET @strColumns = @strColumns
                                + ',L'
                                + CONVERT(VARCHAR(3),@tintOrder)
                                + '.vcData'
                                + ' ['
                                + @vcColumnName + ']'

                SET @WhereCondition = @WhereCondition
                                        + ' left Join CFW_FLD_Values_Opp CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + ' on CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.Fld_Id='
                                        + CONVERT(VARCHAR(10),@numFieldId)
                                        + 'and CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.RecId=Opp.numOppid '

                SET @WhereCondition = @WhereCondition
                                        + ' left Join ListDetails L'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + ' on L'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.numListItemID=CFW'
                                        + CONVERT(VARCHAR(3),@tintOrder)
                                        + '.Fld_Value'
            END
			ELSE
			BEGIN
				SET @strColumns = @strColumns + CONCAT(',dbo.GetCustFldValueOpp(',@numFieldId,',opp.numOppID)') + ' [' + @vcColumnName + ']'
			END

			IF LEN(@SearchText) > 0
			BEGIN
				SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + CONCAT('dbo.GetCustFldValueOpp(',@numFieldId,',opp.numOppID) LIKE ''%',@SearchText,'%''')
			END
		END
      
     
		SELECT TOP 1 
			@tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName,@vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,
			@numListID=numListID,@vcLookBackTableName=vcLookBackTableName,@bitCustom=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,@bitAllowEdit=bitAllowEdit,
			@ListRelID=ListRelID
		FROM 
			#tempForm 
		WHERE 
			tintOrder > @tintOrder-1 
		ORDER BY 
			tintOrder asc            
 
		IF @@rowcount=0 SET @tintOrder=0 
	END 

	

	DECLARE @strShareRedordWith AS VARCHAR(300);
	SET @strShareRedordWith=' Opp.numOppId in (select SR.numRecordID FROM ShareRecord SR WHERE SR.numDomainID='+ convert(varchar(15),@numDomainID )+' AND SR.numModuleID=3 AND SR.numAssignedTo='+convert(varchar(15),@numUserCntId)+') '

	DECLARE @strExternalUser AS VARCHAR(MAX) = ''
	SET @strExternalUser = CONCAT(' (NOT EXISTS (SELECT numUserDetailID FROM UserMaster WHERE numDomainID=',@numDomainID,' AND numUserDetailID=',@numUserCntId,') AND EXISTS (SELECT EAD.numExtranetDtlID FROM ExtranetAccountsDtl EAD INNER JOIN ExtarnetAccounts EA ON EAD.numExtranetID=EA.numExtranetID WHERE EAD.numDomainID=',@numDomainID,' AND EAD.numContactID=',@numUserCntID,' AND EA.numDivisionID=Div.numDivisionID))')

	DECLARE @StrSql AS VARCHAR(MAX) = ''

	SET @StrSql = @StrSql + ' FROM OpportunityMaster Opp                                               
                                 LEFT JOIN AdditionalContactsInformation ADC ON Opp.numContactId = ADC.numContactId   
								 LEFT JOIN (SELECT OIInner.numOppID,COUNT(*) numApprovalCount FROM OpportunityItems OIInner WHERE ISNULL(OIInner.bitItemPriceApprovalRequired,0)=1 GROUP BY OIInner.numOppID) TEMPApproval ON Opp.numOppID=TEMPApproval.numOppID
								 LEFT JOIN (SELECT OIInner.numOppID,COUNT(*) numMappingCount FROM OpportunityItems OIInner WHERE ISNULL(OIInner.bitMappingRequired,0)=1 GROUP BY OIInner.numOppID) TEMPMapping ON Opp.numOppID=TEMPMapping.numOppID                                                            
                                 INNER JOIN DivisionMaster Div ON Opp.numDivisionId = Div.numDivisionID 
								 INNER JOIN CompanyInfo cmp ON Div.numCompanyID = cmp.numCompanyId 
								 LEFT JOIN ProjectProgress PP On PP.numOppId = Opp.numOppId ' + @WhereCondition
								 --INNER JOIN Correspondence Corr ON opp.numOppId = Corr.numOpenRecordID 
	-------Change Row Color-------
	
	DECLARE @vcCSOrigDbCOlumnName AS VARCHAR(50) = ''
	DECLARE @vcCSLookBackTableName AS VARCHAR(50) = ''
	DECLARE @vcCSAssociatedControlType AS VARCHAR(50)

	CREATE TABLE #tempColorScheme
	(
		vcOrigDbCOlumnName VARCHAR(50),
		vcLookBackTableName VARCHAR(50),
		vcFieldValue VARCHAR(50),
		vcFieldValue1 VARCHAR(50),
		vcColorScheme VARCHAR(50),
		vcAssociatedControlType varchar(50)
	)

	INSERT INTO 
		#tempColorScheme 
	SELECT 
		DFM.vcOrigDbCOlumnName,
		DFM.vcLookBackTableName,
		DFCS.vcFieldValue,
		DFCS.vcFieldValue1,
		DFCS.vcColorScheme,
		DFFM.vcAssociatedControlType 
	FROM 
		DycFieldColorScheme DFCS 
	JOIN 
		DycFormField_Mapping DFFM 
	ON 
		DFCS.numFieldID=DFFM.numFieldID
	JOIN 
		DycFieldMaster DFM 
	ON 
		DFM.numModuleID=DFFM.numModuleID and DFM.numFieldID=DFFM.numFieldID
	WHERE
		DFCS.numDomainID=@numDomainID 
		AND DFFM.numFormID=@numFormId 
		AND DFCS.numFormID=@numFormId 
		AND isnull(DFFM.bitAllowGridColor,0)=1

	IF(SELECT COUNT(*) FROM #tempColorScheme)>0
	BEGIN
		SELECT TOP 1 @vcCSOrigDbCOlumnName=vcOrigDbCOlumnName,@vcCSLookBackTableName=vcLookBackTableName,@vcCSAssociatedControlType=vcAssociatedControlType from #tempColorScheme
	END                        

	IF len(@vcCSOrigDbCOlumnName)>0 and len(@vcCSLookBackTableName)>0
	BEGIN
		SET @strColumns=@strColumns + ',tCS.vcColorScheme'
	END

	IF len(@vcCSOrigDbCOlumnName)>0 and len(@vcCSLookBackTableName)>0
	BEGIN
		IF @vcCSLookBackTableName = 'AdditionalContactsInformation'                  
			SET @Prefix = 'ADC.'                  
		IF @vcCSLookBackTableName = 'DivisionMaster'                  
			SET @Prefix = 'Div.'
		if @vcCSLookBackTableName = 'CompanyInfo'                  
			SET @Prefix = 'CMP.'   
		if @vcCSLookBackTableName = 'OpportunityMaster'                  
			SET @Prefix = 'Opp.'   
 
		IF @vcCSAssociatedControlType='DateField'
		BEGIN
			SET @strSql=@strSql+' LEFT JOIN #tempColorScheme tCS ON CONVERT(VARCHAR(10),' + @Prefix + @vcCSOrigDbCOlumnName + ',111) >= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue as int),GETUTCDATE()),111)
				 and CONVERT(VARCHAR(10),' + @Prefix + @vcCSOrigDbCOlumnName + ',111) <= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue1 as int),GETUTCDATE()),111)'
		END
		ELSE IF @vcCSAssociatedControlType='SelectBox' OR @vcCSAssociatedControlType='ListBox' OR @vcCSAssociatedControlType='CheckBox'
		BEGIN
			SET @strSql=@strSql+' LEFT JOIN #tempColorScheme tCS on tCS.vcFieldValue=' + @Prefix + @vcCSOrigDbCOlumnName
		END
	END
                          
	IF @columnName like 'CFW.Cust%'             
	BEGIN            
		SET @strSql = @strSql + ' LEFT JOIN CFW_Fld_Values_Opp CFW on CFW.RecId=Opp.numOppid and CFW.fld_id= '+ REPLACE(@columnName,'CFW.Cust','') +' '                                                     
		DECLARE @vcSortFieldType VARCHAR(50)

		SELECT @vcSortFieldType = Fld_type FROM CFW_Fld_Master WHERE Fld_id=CAST(REPLACE(@columnName,'CFW.Cust','') AS INT)

		IF ISNULL(@vcSortFieldType,'') = 'DateField'
		BEGIN
			SET @columnName='(CASE WHEN ISDATE(CFW.Fld_Value) = 1 THEN CAST(CFW.Fld_Value AS DATE) ELSE NULL END)'
		END
		ELSE IF CAST(REPLACE(@columnName,'CFW.Cust','') AS INT) IN (12745,12846)
		BEGIN
			SET @columnName='(CASE WHEN ISNUMERIC(CFW.Fld_Value) = 1 THEN CAST(CFW.Fld_Value AS FLOAT) ELSE NULL END)'
		END
		ELSE
		BEGIN
			SET @columnName='CFW.Fld_Value'
		END            
	END 

	IF @bitPartner = 1
		SET @strSql = @strSql + ' LEFT JOIN OpportunityContact OppCont ON OppCont.numOppId=Opp.numOppId AND OppCont.bitPartner=1 AND OppCont.numContactId=' + CONVERT(VARCHAR(15),@numUserCntID)
                    
                
	IF @tintFilterBy = 1 --Partially Fulfilled Orders 
		SET @strSql = @strSql + ' LEFT JOIN AdditionalContactsInformation ADC1 ON ADC1.numContactId=Opp.numRecOwner                                                               
                             LEFT OUTER JOIN [OpportunityBizDocs] OB ON OB.[numOppId] = Opp.[numOppId] 
                             WHERE Opp.numDomainID=' + CONVERT(VARCHAR(15),@numDomainID) + ' and Opp.tintOppstatus=1 and (Opp.tintShipped=' + CONVERT(VARCHAR(15),@tintShipped) + ' OR ' + CONVERT(VARCHAR(15),@tintShipped) + '=2)'
	ELSE
		SET @strSql = @strSql + ' LEFT JOIN AdditionalContactsInformation ADC1 on ADC1.numContactId=Opp.numRecOwner                                                               
                             WHERE Opp.numDomainID=' + CONVERT(VARCHAR(15),@numDomainID) + ' and Opp.tintOppstatus=1 and (Opp.tintShipped='+ CONVERT(VARCHAR(15),@tintShipped)  + ' OR ' + CONVERT(VARCHAR(15),@tintShipped) + '=2)'
  
  
	IF @intOrder <> 0
    BEGIN
      IF @intOrder = 1
        SET @strSql = @strSql + ' and Opp.bitOrder = 0 '
      IF @intOrder = 2
        SET @strSql = @strSql + ' and Opp.bitOrder = 1'
    END
    
	IF @numCompanyID <> 0
		SET @strSql = @strSql + ' And Div.numCompanyID ='+ CONVERT(VARCHAR(15),@numCompanyID)

	IF @SortChar <> '0' 
		SET @strSql = @strSql + ' And Opp.vcPOppName like ''' + @SortChar + '%'''
  

	IF @inttype = 3
	BEGIN
		IF @tintSalesUserRightType = 0
			SET @strSql = @strSql + ' AND opp.tintOppType=0'
		ELSE IF @tintSalesUserRightType = 1
			SET @strSql = CONCAT(@strSql,' AND (Opp.numRecOwner= ',@numUserCntID,' or Opp.numAssignedTo=',@numUserCntID
						,(CASE WHEN @bitPartner = 1 THEN ' or ( OppCont.bitPartner=1 and OppCont.numContactId='+ CONVERT(VARCHAR(15),@numUserCntID) + ')' ELSE '' END)
						,' or ',@strShareRedordWith
						,' or ',@strExternalUser,')')
		ELSE IF @tintSalesUserRightType = 2
			SET @strSql = @strSql + ' AND (Div.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+ CONVERT(VARCHAR(15),@numUserCntID) + ' ) 
									 or Opp.numAssignedTo= '+ CONVERT(VARCHAR(15),@numUserCntID)
									+ ' or ' + @strShareRedordWith +')'
	END
	ELSE IF @inttype = 4
	BEGIN
		IF @tintPurchaseUserRightType = 0
			SET @strSql = @strSql + ' AND opp.tintOppType=0'
		ELSE IF @tintPurchaseUserRightType = 1
			SET @strSql = CONCAT(@strSql,' AND (Opp.numRecOwner= ',@numUserCntID
						, ' or Opp.numAssignedTo= ',@numUserCntID
						, CASE WHEN @bitPartner = 1 THEN ' or ( OppCont.bitPartner=1 and OppCont.numContactId='+ CONVERT(VARCHAR(15),@numUserCntID) + ')' ELSE '' END
						, ' or ' + @strShareRedordWith
						,' or ',@strExternalUser,')')
		ELSE IF @tintPurchaseUserRightType = 2
			SET @strSql = @strSql + ' AND (Div.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+ CONVERT(VARCHAR(15),@numUserCntID) + ' ) 
									 or Opp.numAssignedTo= '+ CONVERT(VARCHAR(15),@numUserCntID)
									+ ' or ' + @strShareRedordWith +')'
	END

	
					
	IF @tintSortOrder <> '0'
		SET @strSql = @strSql + '  AND Opp.tintOppType= '+ CONVERT(VARCHAR(1),@tintSortOrder)
    
    
	IF @tintFilterBy = 1 --Partially Fulfilled Orders
		SET @strSql = @strSql + ' AND OB.bitPartialFulfilment = 1 AND OB.bitAuthoritativeBizDocs = 1 '
	ELSE IF @tintFilterBy = 2 --Fulfilled Orders
		SET @strSql = @strSql + ' AND Opp.[numOppId] IN (SELECT OM.numOppId FROM [OpportunityMaster] OM
                      			INNER JOIN [OpportunityBizDocs] OB ON OB.[numOppId] = OM.[numOppId]
                      			INNER JOIN [OpportunityItems] OI ON OM.[numOppId] = OI.[numOppId]
                      			INNER JOIN [OpportunityBizDocItems] BI ON BI.[numOppBizDocID] = OB.[numOppBizDocsId]
                      			Where OM.numDomainId=' + CONVERT(VARCHAR(15),@numDomainId) + 'AND OB.[bitAuthoritativeBizDocs] = 1 GROUP BY OM.[numOppId]
                      			HAVING   COUNT(OI.[numUnitHour]) = COUNT(BI.[numUnitHour])) '
	ELSE IF @tintFilterBy = 3 --Orders without Authoritative-BizDocs
		SET @strSql = @strSql + ' AND Opp.[numOppId] not IN (SELECT DISTINCT OM.[numOppId]
                      		  FROM [OpportunityMaster] OM JOIN [OpportunityBizDocs] OB ON OM.[numOppId] = OB.[numOppId]
                      		  Where OM.numDomainId=' + CONVERT(VARCHAR(15),@numDomainId) + ' AND OM.tintOppstatus=1 AND (OM.tintShipped=' + CONVERT(VARCHAR(15),@tintShipped) + ' OR ' + CONVERT(VARCHAR(15),@tintShipped) + '=2)' + ' AND OM.tintOppType= '+ CONVERT(VARCHAR(1),@tintSortOrder) + ' AND isnull(OB.[bitAuthoritativeBizDocs],0)=1) '
	ELSE IF @tintFilterBy = 4 --Orders with items yet to be added to Invoices
		SET @strSql = @strSql + ' AND Opp.[numOppId] IN (SELECT distinct OPPM.numOppId FROM [OpportunityMaster] OPPM
                                  INNER JOIN [OpportunityItems] OPPI ON OPPM.[numOppId] = OPPI.[numOppId]
                                  Where OPPM.numDomainId=' + CONVERT(VARCHAR(15),@numDomainId) + ' 
								  and OPPI.numoppitemtCode not in (select numOppItemID from [OpportunityBizDocs] OB INNER JOIN [OpportunityBizDocItems] BI
                      			  ON BI.[numOppBizDocID] = OB.[numOppBizDocsId] where OB.[numOppId] = OPPM.[numOppId]))'      
	ELSE IF @tintFilterBy = 6 --Orders with deferred Income/Invoices
		SET @strSql = @strSql + ' AND Opp.[numOppId] IN (SELECT DISTINCT OM.[numOppId]
                      		 FROM [OpportunityMaster] OM LEFT OUTER JOIN [OpportunityBizDocs] OB ON OM.[numOppId] = OB.[numOppId]
                      		 Where OM.numDomainId=' + CONVERT(VARCHAR(15),@numDomainId) + ' AND OB.[bitAuthoritativeBizDocs] =1 and OB.tintDeferred=1)'

	IF @numOrderStatus <>0 
		SET @strSql = @strSql + ' AND Opp.numStatus = ' + CONVERT(VARCHAR(15),@numOrderStatus);

	IF CHARINDEX('OI.vcInventoryStatus',@vcRegularSearchCriteria) > 0
	BEGIN
		IF CHARINDEX('OI.vcInventoryStatus=1',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'OI.vcInventoryStatus=1',' CHARINDEX(''Not Applicable'', dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID,1)) > 0 ')
		IF CHARINDEX('OI.vcInventoryStatus=2',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'OI.vcInventoryStatus=2',' CHARINDEX(''Shipped'', dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID,1)) > 0 ')
		IF CHARINDEX('OI.vcInventoryStatus=3',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'OI.vcInventoryStatus=3',' CHARINDEX(''BO'', dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID,1)) > 0 AND CHARINDEX(''Shippable'', dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID,1)) = 0 ')
		IF CHARINDEX('OI.vcInventoryStatus=4',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'OI.vcInventoryStatus=4',' CHARINDEX(''Shippable'', dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID,1)) > 0 ')
	END
	
	IF CHARINDEX('Opp.tintInvoicing',@vcRegularSearchCriteria) > 0
	BEGIN
		IF CHARINDEX('Opp.tintInvoicing=0',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.tintInvoicing=0',' CHARINDEX(''All'', dbo.CheckOrderInvoicingStatus(Opp.numOppID,Opp.numDomainID,1)) > 0 ')
		IF CHARINDEX('Opp.tintInvoicing=1',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.tintInvoicing=1',' (SELECT (CASE WHEN SUM(ISNULL(InvoicedQty,0)) = SUM(ISNULL(OrderedQty,0)) THEN 1 ELSE 0 END)
	FROM 
	(
		SELECT OI.numoppitemtCode, ISNULL(OI.numUnitHour,0) AS OrderedQty, ISNULL(TempInvoice.InvoicedQty,0) AS InvoicedQty
		FROM OpportunityItems OI
		INNER JOIN Item I ON OI.numItemCode = I.numItemCode
		OUTER APPLY
		(
			SELECT SUM(OpportunityBizDocItems.numUnitHour) AS InvoicedQty
			FROM OpportunityBizDocs
				INNER JOIN OpportunityBizDocItems ON OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
			WHERE OpportunityBizDocs.numOppId = Opp.numOppID
				AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
				AND numBizDocId IN (SELECT numAuthoritativeSales FROM AuthoritativeBizDocs WHERE numDomainId = Opp.numDomainID)
		) AS TempInvoice
		WHERE
		OI.numOppID = Opp.numOppID
	) X) > 0 ')
		IF CHARINDEX('Opp.tintInvoicing=2',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.tintInvoicing=2',' (SELECT COUNT(*) 
		FROM OpportunityBizDocs 
		WHERE numOppId = Opp.numOppID 
			AND numBizDocId IN (SELECT numAuthoritativeSales FROM AuthoritativeBizDocs WHERE numDomainId = Opp.numDomainID)) = 0 ')
		IF CHARINDEX('Opp.tintInvoicing=3',@vcRegularSearchCriteria) > 0 
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.tintInvoicing=3',' (SELECT (CASE WHEN SUM(ISNULL(InvoicedQty,0)) > 0 THEN SUM(ISNULL(OrderedQty,0)) - SUM(ISNULL(InvoicedQty,0)) ELSE 0 END)
	FROM 
	(
		SELECT OI.numoppitemtCode, ISNULL(OI.numUnitHour,0) AS OrderedQty, ISNULL(TempInvoice.InvoicedQty,0) AS InvoicedQty
		FROM OpportunityItems OI
		INNER JOIN Item I ON OI.numItemCode = I.numItemCode
		OUTER APPLY
		(
			SELECT SUM(OpportunityBizDocItems.numUnitHour) AS InvoicedQty
			FROM OpportunityBizDocs
				INNER JOIN OpportunityBizDocItems ON OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
			WHERE OpportunityBizDocs.numOppId = Opp.numOppID
				AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
				AND numBizDocId IN (SELECT numAuthoritativeSales FROM AuthoritativeBizDocs WHERE numDomainId = Opp.numDomainID)
		) AS TempInvoice
		WHERE
		OI.numOppID = Opp.numOppID
	) X) > 0 ')
		
		IF CHARINDEX('Opp.tintInvoicing=4',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.tintInvoicing=4',' (SELECT (CASE WHEN SUM(ISNULL(InvoicedQty,0)) > 0 THEN SUM(ISNULL(OrderedQty,0)) - SUM(ISNULL(InvoicedQty,0)) ELSE 0 END)
	FROM 
	(
		SELECT OI.numoppitemtCode, ISNULL(OI.numUnitHour,0) AS OrderedQty, ISNULL(TempInvoice.InvoicedQty,0) AS InvoicedQty
		FROM OpportunityItems OI
		INNER JOIN Item I ON OI.numItemCode = I.numItemCode
		OUTER APPLY
		(
			SELECT SUM(OpportunityBizDocItems.numUnitHour) AS InvoicedQty
			FROM OpportunityBizDocs
				INNER JOIN OpportunityBizDocItems ON OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
			WHERE OpportunityBizDocs.numOppId = Opp.numOppID
				AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
				AND numBizDocId IN (SELECT numAuthoritativeSales FROM AuthoritativeBizDocs WHERE numDomainId = Opp.numDomainID)
		) AS TempInvoice
		WHERE
		OI.numOppID = Opp.numOppID
	) X) > 0 ')

		IF CHARINDEX('Opp.tintInvoicing=5',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.tintInvoicing=5',' 
				Opp.[numOppId] IN (SELECT DISTINCT OM.[numOppId]
                      		 FROM [OpportunityMaster] OM INNER JOIN [OpportunityBizDocs] OB ON OM.[numOppId] = OB.[numOppId]
                      		 Where OM.numDomainId = Opp.numDomainID AND OB.[bitAuthoritativeBizDocs] = 1 and OB.tintDeferred=1) ')
	END
	
	IF CHARINDEX('Opp.tintEDIStatus',@vcRegularSearchCriteria) > 0
	BEGIN
		--IF CHARINDEX('Opp.tintEDIStatus=1',@vcRegularSearchCriteria) > 0
		--	SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.tintEDIStatus=1',' 1 = (SELECT CASE WHEN Opp.tintEDIStatus = 1 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.tintEDIStatus=2',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.tintEDIStatus=2',' 1 = (SELECT CASE WHEN Opp.tintEDIStatus = 2 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.tintEDIStatus=3',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.tintEDIStatus=3',' 1 = (SELECT CASE WHEN Opp.tintEDIStatus = 3 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.tintEDIStatus=4',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.tintEDIStatus=4',' 1 = (SELECT CASE WHEN Opp.tintEDIStatus = 4 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.tintEDIStatus=5',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.tintEDIStatus=5',' 1 = (SELECT CASE WHEN Opp.tintEDIStatus = 5 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.tintEDIStatus=6',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.tintEDIStatus=6',' 1 = (SELECT CASE WHEN Opp.tintEDIStatus = 6 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.tintEDIStatus=7',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.tintEDIStatus=7',' 1 = (SELECT CASE WHEN Opp.tintEDIStatus = 7 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.tintEDIStatus=7',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.tintEDIStatus=11',' 1 = (SELECT CASE WHEN Opp.tintEDIStatus = 11 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.tintEDIStatus=7',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.tintEDIStatus=12',' 1 = (SELECT CASE WHEN Opp.tintEDIStatus = 12 THEN 1 ELSE 0 END) ')
	END

	IF CHARINDEX('Opp.vcSignatureType',@vcRegularSearchCriteria) > 0
	BEGIN
		IF CHARINDEX('Opp.vcSignatureType=0',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.vcSignatureType=0',' 1 = (SELECT CASE WHEN (SELECT COUNT(*) FROM DivisionMaster AS D 
																			LEFT JOIN DivisionMasterShippingConfiguration AS DMSC ON DMSC.numDivisionID=D.numDivisionID 
																			WHERE D.numDivisionID=Div.numDivisionID AND vcSignatureType=0 ) > 0 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.vcSignatureType=1',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.vcSignatureType=1','  1 = (SELECT CASE WHEN (SELECT COUNT(*) FROM DivisionMaster AS D 
																			LEFT JOIN DivisionMasterShippingConfiguration AS DMSC ON DMSC.numDivisionID=D.numDivisionID 
																			WHERE D.numDivisionID=Div.numDivisionID AND vcSignatureType=1 ) > 0 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.vcSignatureType=2',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.vcSignatureType=2',' 1 = (SELECT CASE WHEN (SELECT COUNT(*) FROM DivisionMaster AS D 
																			LEFT JOIN DivisionMasterShippingConfiguration AS DMSC ON DMSC.numDivisionID=D.numDivisionID 
																			WHERE D.numDivisionID=Div.numDivisionID AND vcSignatureType=2 ) > 0 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.vcSignatureType=3',@vcRegularSearchCriteria) > 0								   
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.vcSignatureType=3',' 1 = (SELECT CASE WHEN (SELECT COUNT(*) FROM DivisionMaster AS D 
																			LEFT JOIN DivisionMasterShippingConfiguration AS DMSC ON DMSC.numDivisionID=D.numDivisionID 
																			WHERE D.numDivisionID=Div.numDivisionID AND vcSignatureType=3 ) > 0 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('Opp.vcSignatureType=4',@vcRegularSearchCriteria) > 0								   
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'Opp.vcSignatureType=4',' 1 = (SELECT CASE WHEN (SELECT COUNT(*) FROM DivisionMaster AS D 
																			LEFT JOIN DivisionMasterShippingConfiguration AS DMSC ON DMSC.numDivisionID=D.numDivisionID 
																			WHERE D.numDivisionID=Div.numDivisionID AND vcSignatureType=4 ) > 0 THEN 1 ELSE 0 END) ')
	END

	IF CHARINDEX('OBD.monAmountPaid',@vcRegularSearchCriteria) > 0
	BEGIN
		IF CHARINDEX('OBD.monAmountPaid like ''%0%''',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'OBD.monAmountPaid like ''%0%''',' 1 = (SELECT ISNULL((SELECT SUM(ISNULL(monAmountPaid,0)) FROM  OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)) ')
		IF CHARINDEX('OBD.monAmountPaid like ''%1%''',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'OBD.monAmountPaid like ''%1%''','  1 = (SELECT (CASE WHEN (SELECT (CASE WHEN (SUM(ISNULL(monDealAmount,0)) = SUM(ISNULL(monAmountPaid,0)) AND SUM(ISNULL(monAmountPaid,0)) <> 0) THEN 1 ELSE 0 END) FROM OpportunityBizDocs BD WHERE BD.numOppId =  Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0) = 1) > 0 THEN 1 ELSE 0 END))  ')
		IF CHARINDEX('OBD.monAmountPaid like ''%2%''',@vcRegularSearchCriteria) > 0
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'OBD.monAmountPaid like ''%2%''',' 1 = (SELECT CASE WHEN (SELECT (CASE WHEN ((SUM(ISNULL(monDealAmount,0)) - SUM(ISNULL(monAmountPaid,0))) > 0 AND SUM(ISNULL(monAmountPaid,0)) <> 0) THEN 1 ELSE 0 END) FROM OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0) = 1) > 0 THEN 1 ELSE 0 END) ')
		IF CHARINDEX('OBD.monAmountPaid like ''%3%''',@vcRegularSearchCriteria) > 0								   
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'OBD.monAmountPaid like ''%3%''',' 1 = (SELECT CASE WHEN (SELECT (CASE WHEN (SUM(ISNULL(monAmountPaid,0)) = 0) THEN 1 ELSE 0 END) FROM OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0) = 1) > 0 THEN 1 ELSE 0 END) ')
	END
	
	
	IF @vcRegularSearchCriteria<>'' 
	BEGIN
		SET @strSql=@strSql +' AND ' + @vcRegularSearchCriteria 
	END
	
	


	IF @vcCustomSearchCriteria<>'' 
	BEGIN
		SET @strSql = @strSql +' AND ' +  @vcCustomSearchCriteria
	END

	IF LEN(@SearchQuery) > 0
	BEGIN
		SET @strSql = @strSql + ' AND (' + @SearchQuery + ') '
	END

	IF ISNULL(@tintDashboardReminderType,0) = 19
	BEGIN
		SET @strSql = CONCAT(@strSql,' AND Opp.numOppID IN (','SELECT DISTINCT
																	OMInner.numOppID
																FROM 
																	StagePercentageDetails SPDInner
																INNER JOIN 
																	OpportunityMaster OMInner
																ON 
																	SPDInner.numOppID=OMInner.numOppId 
																WHERE 
																	SPDInner.numDomainId=',@numDOmainID,'
																	AND OMInner.numDomainId=',@numDOmainID,' 
																	AND OMInner.tintOppType = 1
																	AND ISNULL(OMInner.tintOppStatus,0) = 1
																	AND tinProgressPercentage <> 100',') ')

	END
	ELSE IF ISNULL(@tintDashboardReminderType,0) = 10
	BEGIN
		SET @strSql = CONCAT(@strSql,' AND Opp.numOppID IN (','SELECT DISTINCT
																	OMInner.numOppId
																FROM
																	OpportunityMaster OMInner
																INNER JOIN
																	OpportunityItems OIInner
																ON
																	OIInner.numOppId = OMInner.numOppID
																INNER JOIN
																	Item IInner
																ON
																	OIInner.numItemCode = IInner.numItemCode
																OUTER APPLY
																(
																	SELECT
																		SUM(OpportunityBizDocItems.numUnitHour) AS BilledQty
																	FROM
																		OpportunityBizDocs
																	INNER JOIN
																		OpportunityBizDocItems 
																	ON
																		OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																	WHERE
																		OpportunityBizDocs.numOppId = OMInner.numOppID
																		AND OpportunityBizDocItems.numOppItemID = OIInner.numoppitemtCode
																		AND OpportunityBizDocs.numBizDocId = (SELECT ISNULL(numAuthoritativePurchase,0) FROM AuthoritativeBizDocs WHERE numDomainId=',@numDomainID,')
																) AS TempBilled
																WHERE
																	OMInner.numDomainId = ',@numDomainID,'
																	AND ISNULL(OMInner.bitStockTransfer,0) = 0
																	AND OMInner.tintOppType = 2
																	AND OMInner.tintOppStatus  = 1
																	AND ISNULL(OIInner.numUnitHour,0) <> ISNULL(TempBilled.BilledQty,0)',') ')
	END
	ELSE IF ISNULL(@tintDashboardReminderType,0) = 11
	BEGIN
		SET @strSql = CONCAT(@strSql,' AND Opp.numOppID IN (','SELECT DISTINCT
																	OMInner.numOppId
																FROM
																	OpportunityMaster OMInner
																INNER JOIN
																	OpportunityItems OIInner
																ON
																	OIInner.numOppId = OMInner.numOppId
																INNER JOIN
																	Item IInner
																ON
																	OIInner.numItemCode = IInner.numItemCode
																OUTER APPLY
																(
																	SELECT
																		SUM(OpportunityBizDocItems.numUnitHour) AS PickPacked
																	FROM
																		OpportunityBizDocs
																	INNER JOIN
																		OpportunityBizDocItems 
																	ON
																		OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																	WHERE
																		OpportunityBizDocs.numOppId = OMInner.numOppId
																		AND ISNULL(OpportunityBizDocs.numBizDocId,0) = ',(SELECT ISNULL(numDefaultSalesShippingDoc,0) FROM Domain WHERE numDomainId=@numDomainID),'
																		AND OpportunityBizDocItems.numOppItemID = OIInner.numoppitemtCode
																) AS TempFulFilled
																WHERE
																	OMInner.numDomainId = ',@numDomainID,'
																	AND OMInner.tintOppType = 1
																	AND OMInner.tintOppStatus = 1
																	AND UPPER(IInner.charItemType) = ''P''
																	AND ISNULL(OMInner.tintshipped,0) = 0
																	AND ISNULL(OIInner.bitDropShip,0) = 0
																	AND ISNULL(OIInner.numUnitHour,0) <> ISNULL(TempFulFilled.PickPacked,0)',') ')
	END
	ELSE IF ISNULL(@tintDashboardReminderType,0) = 12
	BEGIN
		SET @strSql = CONCAT(@strSql,' AND Opp.numOppID IN (','SELECT DISTINCT
																	OMInner.numOppId
																FROM
																	OpportunityMaster OMInner
																INNER JOIN
																	OpportunityItems OIInner
																ON
																	OIInner.numOppId = OMInner.numOppId
																INNER JOIN
																	Item IInner
																ON
																	OIInner.numItemCode = IInner.numItemCode
																OUTER APPLY
																(
																	SELECT
																		SUM(OpportunityBizDocItems.numUnitHour) AS FulFilledQty
																	FROM
																		OpportunityBizDocs
																	INNER JOIN
																		OpportunityBizDocItems 
																	ON
																		OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																	WHERE
																		OpportunityBizDocs.numOppId = OMInner.numOppId
																		AND ISNULL(OpportunityBizDocs.numBizDocId,0) = 296
																		AND ISNULL(OpportunityBizDocs.bitFulFilled,0) = 1
																		AND OpportunityBizDocItems.numOppItemID = OIInner.numoppitemtCode
																) AS TempFulFilled
																WHERE
																	OMInner.numDomainId = ',@numDomainID,'
																	AND OMInner.tintOppType = 1
																	AND OMInner.tintOppStatus = 1
																	AND ISNULL(OMInner.tintshipped,0) = 0
																	AND UPPER(IInner.charItemType) = ''P''
																	AND ISNULL(OIInner.bitDropShip,0) = 0
																	AND ISNULL(OIInner.numUnitHour,0) <> ISNULL(TempFulFilled.FulFilledQty,0)',') ')
	END
	ELSE IF ISNULL(@tintDashboardReminderType,0) = 13
	BEGIN
		SET @strSql = CONCAT(@strSql,' AND Opp.numOppID IN (','SELECT DISTINCT
																	OMInner.numOppId
																FROM
																	OpportunityMaster OMInner
																INNER JOIN
																	OpportunityItems OIInner
																ON
																	OIInner.numOppId = OMInner.numOppID
																INNER JOIN
																	Item IInner
																ON
																	OIInner.numItemCode = IInner.numItemCode
																OUTER APPLY
																(
																	SELECT
																		SUM(OpportunityBizDocItems.numUnitHour) AS InvoicedQty
																	FROM
																		OpportunityBizDocs
																	INNER JOIN
																		OpportunityBizDocItems 
																	ON
																		OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																	WHERE
																		OpportunityBizDocs.numOppId = OMInner.numOppID
																		AND OpportunityBizDocItems.numOppItemID = OIInner.numoppitemtCode
																		AND OpportunityBizDocs.numBizDocId = ISNULL((SELECT TOP 1 numAuthoritativeSales FROM AuthoritativeBizDocs WHERE numDomainId=',@numDomainID,'),287)
																) AS TempInvoice
																WHERE
																	OMInner.numDomainId = ',@numDomainID,'
																	AND OMInner.tintOppType = 1
																	AND OMInner.tintOppStatus  = 1
																	AND ISNULL(OIInner.numUnitHour,0) <> ISNULL(TempInvoice.InvoicedQty,0)',') ')
	END
	ELSE IF ISNULL(@tintDashboardReminderType,0) = 14
	BEGIN
		DECLARE @TEMPOppID TABLE
		(
			numOppID NUMERIC(18,0)
		)

		INSERT INTO @TEMPOppID
		(
			numOppID
		)
		SELECT DISTINCT
			OM.numOppID
		FROM
			OpportunityMaster OM
		INNER JOIN
			OpportunityItems OI
		ON
			OM.numOppId = OI.numOppId
		WHERE   
			OM.numDomainId = @numDomainId
			AND OM.tintOppType = 1
			AND OM.tintOppStatus=1
			AND ISNULL(OI.bitDropShip, 0) = 1
			AND 1 = (CASE WHEN (SELECT COUNT(*) FROM OpportunityLinking OMInner INNER JOIN OpportunityItems OIInner ON OMInner.numChildOppID=OIInner.numOppId WHERE OMInner.numParentOppID = OM.numOppID AND OIInner.numItemCode=OI.numItemCode AND ISNULL(OIInner.numWarehouseItmsID,0)=ISNULL(OI.numWarehouseItmsID,0)) > 0 THEN 0 ELSE 1 END)


		If ISNULL((SELECT bitReOrderPoint FROM Domain WHERE numDomainId=@numDomainID),0) = 1
		BEGIN
			INSERT INTO @TEMPOppID
			(
				numOppID
			)
			SELECT DISTINCT
				OM.numOppID
			FROM
				OpportunityMaster OM
			INNER JOIN
				OpportunityItems OI
			ON
				OI.numOppId = OM.numOppId
			INNER JOIN 
				Item I 
			ON 
				I.numItemCode = OI.numItemCode
			INNER JOIN 
				WarehouseItems WI 
			ON 
				OI.numWarehouseItmsID = WI.numWareHouseItemID
			WHERE   
				OM.numDomainId = @numDomainId
				AND ISNULL((SELECT bitReOrderPoint FROM Domain WHERE numDomainId=@numDomainID),0) = 1
				AND (ISNULL(WI.numOnHand,0) + ISNULL(WI.numOnOrder,0) <= ISNULL(WI.numReOrder,0))
				AND OM.tintOppType = 1
				AND OM.tintOppStatus=1
				AND ISNULL(I.bitAssembly, 0) = 0
				AND ISNULL(I.bitKitParent, 0) = 0
				AND ISNULL(OI.bitDropship,0) = 0
				AND OM.numOppId NOT IN (SELECT numOppID FROM @TEMPOppID)

			IF (SELECT COUNT(*) FROM @TEMPOppID) > 0
			BEGIN
				DECLARE @tmpIds VARCHAR(MAX) = ''
				SELECT @tmpIds = CONCAT(@tmpIds,numOppID,', ') FROM @TEMPOppID
				SET @strSql = CONCAT(@strSql,' AND Opp.numOppID IN (',SUBSTRING(@tmpIds, 0, LEN(@tmpIds)),') ')
			END
			ELSE
			BEGIN
				SET @strSql = CONCAT(@strSql,' AND Opp.numOppID IN (0) ')
			END
		END
	END
	ELSE IF ISNULL(@tintDashboardReminderType,0) = 15
	BEGIN
		SET @strSql = CONCAT(@strSql,' AND Opp.numOppID IN (','SELECT
																	OMInner.numOppID
																FROM
																	OpportunityMaster OMInner
																WHERE
																	numDomainId=',@numDomainID,'
																	AND tintOppType=1
																	AND tintOppStatus=1
																	AND ISNULL(OMInner.tintshipped,0) = 0
																	AND dtReleaseDate IS NOT NULL
																	AND dtReleaseDate < DateAdd(minute,',@ClientTimeZoneOffset * -1,',GETUTCDATE())
																	AND (SELECT 
																			COUNT(*) 
																		FROM 
																		(
																			SELECT
																				OIInner.numoppitemtCode,
																				ISNULL(OIInner.numUnitHour,0) AS OrderedQty,
																				ISNULL(TempFulFilled.FulFilledQty,0) AS FulFilledQty
																			FROM
																				OpportunityItems OIInner
																			INNER JOIN
																				Item IInner
																			ON
																				OIInner.numItemCode = IInner.numItemCode
																			OUTER APPLY
																			(
																				SELECT
																					SUM(OpportunityBizDocItems.numUnitHour) AS FulFilledQty
																				FROM
																					OpportunityBizDocs
																				INNER JOIN
																					OpportunityBizDocItems 
																				ON
																					OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																				WHERE
																					OpportunityBizDocs.numOppId = OMInner.numOppId
																					AND ISNULL(OpportunityBizDocs.numBizDocId,0) = 296
																					AND ISNULL(OpportunityBizDocs.bitFulFilled,0) = 1
																					AND OpportunityBizDocItems.numOppItemID = OIInner.numoppitemtCode
																			) AS TempFulFilled
																			WHERE
																				OIInner.numOppID = OMInner.numOppID
																				AND UPPER(IInner.charItemType) = ''P''
																				AND ISNULL(OIInner.bitDropShip,0) = 0
																		) X
																		WHERE
																			X.OrderedQty <> X.FulFilledQty) > 0',') ')
	END



	DECLARE  @firstRec  AS INTEGER
	DECLARE  @lastRec  AS INTEGER
  
	SET @firstRec = (@CurrentPage - 1) * @PageSize
	SET @lastRec = (@CurrentPage * @PageSize + 1)



	-- EXECUTE FINAL QUERY
	DECLARE @strFinal AS NVARCHAR(MAX)
	IF @CurrentPage = -1 AND @PageSize = -1
	BEGIN
		SET @strFinal = CONCAT('SELECT COUNT(*) OVER() AS TotalRecords, ',@strColumns,@strSql,' ORDER BY ',@columnName,' ',@columnSortOrder,' OFFSET ',0,' ROWS FETCH NEXT ',25,' ROWS ONLY;')
	END
	ELSE
	BEGIN
		SET @strFinal = CONCAT('SELECT COUNT(*) OVER() AS TotalRecords, ',@strColumns,@strSql,' ORDER BY ',@columnName,' ',@columnSortOrder,' OFFSET ',(@CurrentPage - 1) * @PageSize,' ROWS FETCH NEXT ',@PageSize,' ROWS ONLY;')
	END
	PRINT CAST(@strFinal AS NTEXT)
	EXEC sp_executesql @strFinal

	SELECT * FROM #tempForm

	DROP TABLE #tempForm

	DROP TABLE #tempColorScheme
END
GO
--created by anoop jayaraj                                          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getdomaindetails')
DROP PROCEDURE usp_getdomaindetails
GO
CREATE PROCEDURE [dbo].[USP_GetDomainDetails]                                          
@numDomainID as numeric(9)=0                                          
as           

DECLARE @listIds VARCHAR(MAX)
SELECT @listIds = COALESCE(@listIds+',' ,'') + CAST(numUserID AS VARCHAR(100)) FROM UnitPriceApprover WHERE numDomainID = @numDomainID
   
DECLARE @canChangeDiferredIncomeSelection AS BIT = 1

IF EXISTS(SELECT 
			OB.numOppBizDocsId 
		FROM 
			OpportunityBizDocs OB 
		INNER JOIN 
			OpportunityMaster OM 
		ON 
			OB.numOppId=OM.numOppId 
		WHERE 
			numBizDocId=304 
			AND OM.numDomainId=@numDomainID)
BEGIN
	SET @canChangeDiferredIncomeSelection = 0
END   
   
                               
select                                           
vcDomainName,                                           
isnull(vcDomainDesc,'') vcDomainDesc,                                           
isnull(bitExchangeIntegration,0) bitExchangeIntegration,                                           
isnull(bitAccessExchange,0) bitAccessExchange,                                           
isnull(vcExchUserName,'') vcExchUserName,                                           
isnull(vcExchPassword,'') vcExchPassword,                                           
isnull(vcExchPath,'') vcExchPath,                                           
isnull(vcExchDomain,'') vcExchDomain,                                         
tintCustomPagingRows,                                         
vcDateFormat,                                         
numDefCountry,                                         
tintComposeWindow,                                         
sintStartDate,                                         
sintNoofDaysInterval,                                         
tintAssignToCriteria,                                         
bitIntmedPage,
ISNULL(D.numCost,0) as numCost,                                         
tintFiscalStartMonth,                                    
--isnull(bitDeferredIncome,0) as bitDeferredIncome,                                  
isnull(tintPayPeriod,0) as tintPayPeriod,                                
isnull(numCurrencyID,0) as numCurrencyID,                            
isnull(vcCurrency,'') as vcCurrency,                      
isnull(charUnitSystem,'') as charUnitSystem ,                    
isnull(vcPortalLogo,'') as vcPortalLogo,                  
isnull(tintChrForComSearch,2) as tintChrForComSearch,                
isnull(tintChrForItemSearch,'') as tintChrForItemSearch,            
isnull(intPaymentGateWay,0) as intPaymentGateWay  ,        
 case when bitPSMTPServer= 1 then vcPSMTPServer else '' end as vcPSMTPServer  ,              
 case when bitPSMTPServer= 1 then  isnull(numPSMTPPort,0)  else 0 end as numPSMTPPort          
,isnull(bitPSMTPAuth,0) bitPSMTPAuth        
,isnull([vcPSmtpPassword],'')vcPSmtpPassword        
,isnull(bitPSMTPSSL,0) bitPSMTPSSL     
,isnull(bitPSMTPServer,0) bitPSMTPServer  ,isnull(vcPSMTPUserName,'') vcPSMTPUserName,      
--isnull(numDefaultCategory,0) numDefaultCategory,
isnull(numShipCompany,0)   numShipCompany,
isnull(bitAutoPopulateAddress,0) bitAutoPopulateAddress,
isnull(tintPoulateAddressTo,0) tintPoulateAddressTo,
isnull(bitMultiCompany,0) bitMultiCompany,
isnull(bitMultiCurrency,0) bitMultiCurrency,
ISNULL(bitCreateInvoice,0) bitCreateInvoice,
ISNULL([numDefaultSalesBizDocId],0) numDefaultSalesBizDocId,
ISNULL([bitSaveCreditCardInfo],0) bitSaveCreditCardInfo,
ISNULL([intLastViewedRecord],10) intLastViewedRecord,
ISNULL([tintCommissionType],1) tintCommissionType,
ISNULL([tintBaseTax],2) tintBaseTax,
ISNULL([numDefaultPurchaseBizDocId],0) numDefaultPurchaseBizDocId,
ISNULL(bitEmbeddedCost,0) bitEmbeddedCost,
ISNULL(numDefaultSalesOppBizDocId,0) numDefaultSalesOppBizDocId,
ISNULL(tintSessionTimeOut,1) tintSessionTimeOut,
ISNULL(tintDecimalPoints,2) tintDecimalPoints,
ISNULL(bitCustomizePortal,0) bitCustomizePortal,
ISNULL(tintBillToForPO,0) tintBillToForPO,
ISNULL(tintShipToForPO,0) tintShipToForPO,
ISNULL(tintOrganizationSearchCriteria,1) tintOrganizationSearchCriteria,
ISNULL(tintLogin,0) tintLogin,
lower(ISNULL(vcPortalName,'')) vcPortalName,
ISNULL(bitDocumentRepositary,0) bitDocumentRepositary,
D.numDomainID,
ISNULL(vcPSMTPDisplayName,'') vcPSMTPDisplayName,
ISNULL(bitGtoBContact,0) bitGtoBContact,
ISNULL(bitBtoGContact,0) bitBtoGContact,
ISNULL(bitGtoBCalendar,0) bitGtoBCalendar,
ISNULL(bitBtoGCalendar,0) bitBtoGCalendar,
ISNULL(bitExpenseNonInventoryItem,0) bitExpenseNonInventoryItem,
ISNULL(bitInlineEdit,0) bitInlineEdit,
ISNULL(bitRemoveVendorPOValidation,0) bitRemoveVendorPOValidation,
isnull(D.tintBaseTaxOnArea,0) tintBaseTaxOnArea,
ISNULL(D.bitAmountPastDue,0) bitAmountPastDue,
CONVERT(DECIMAL(18,2),ISNULL(D.monAmountPastDue,0)) monAmountPastDue,
ISNULL(bitSearchOrderCustomerHistory,0) bitSearchOrderCustomerHistory,
isnull(D.bitRentalItem,0) as bitRentalItem,
isnull(D.numRentalItemClass,0) as numRentalItemClass,
isnull(D.numRentalHourlyUOM,0) as numRentalHourlyUOM,
isnull(D.numRentalDailyUOM,0) as numRentalDailyUOM,
isnull(D.tintRentalPriceBasedOn,0) as tintRentalPriceBasedOn,
isnull(D.tintCalendarTimeFormat,24) as tintCalendarTimeFormat,
isnull(D.tintDefaultClassType,0) as tintDefaultClassType,
ISNULL(D.tintPriceBookDiscount,0) tintPriceBookDiscount,
ISNULL(D.bitAutoSerialNoAssign,0) as bitAutoSerialNoAssign,
ISNULL(D.bitIsShowBalance,0) AS [bitIsShowBalance],
ISNULL(numIncomeAccID,0) AS [numIncomeAccID],
ISNULL(numCOGSAccID,0) AS [numCOGSAccID],
ISNULL(numAssetAccID,0) AS [numAssetAccID],
ISNULL(IsEnableProjectTracking,0) AS [IsEnableProjectTracking],
ISNULL(IsEnableCampaignTracking,0) AS [IsEnableCampaignTracking],
ISNULL(IsEnableResourceScheduling,0) AS [IsEnableResourceScheduling],
ISNULL(numShippingServiceItemID,0) AS [numShippingServiceItemID],
ISNULL(D.numSOBizDocStatus,0) AS numSOBizDocStatus,
ISNULL(D.bitAutolinkUnappliedPayment,0) AS bitAutolinkUnappliedPayment,
ISNULL(D.numDiscountServiceItemID,0) AS numDiscountServiceItemID,
ISNULL(D.vcShipToPhoneNo,0) AS vcShipToPhoneNo,
ISNULL(D.vcGAUserEMail,'') AS vcGAUserEMail,
ISNULL(D.vcGAUserPassword,'') AS vcGAUserPassword,
ISNULL(D.vcGAUserProfileId,0) AS vcGAUserProfileId,
ISNULL(D.bitDiscountOnUnitPrice,0) AS bitDiscountOnUnitPrice,
ISNULL(D.intShippingImageWidth,0) AS intShippingImageWidth ,
ISNULL(D.intShippingImageHeight,0) AS intShippingImageHeight,
ISNULL(D.numTotalInsuredValue,0) AS numTotalInsuredValue,
ISNULL(D.numTotalCustomsValue,0) AS numTotalCustomsValue,
ISNULL(D.vcHideTabs,'') AS vcHideTabs,
ISNULL(D.bitUseBizdocAmount,0) AS bitUseBizdocAmount,
ISNULL(D.bitDefaultRateType,0) AS bitDefaultRateType,
ISNULL(D.bitIncludeTaxAndShippingInCommission,0) AS bitIncludeTaxAndShippingInCommission,
/*,ISNULL(D.bitAllowPPVariance,0) AS bitAllowPPVariance*/
ISNULL(D.bitPurchaseTaxCredit,0) AS bitPurchaseTaxCredit,
ISNULL(D.bitLandedCost,0) AS bitLandedCost,
ISNULL(D.vcLanedCostDefault,'') AS vcLanedCostDefault,
ISNULL(D.bitMinUnitPriceRule,0) AS bitMinUnitPriceRule,
ISNULL(AP.numAbovePercent,0) AS numAbovePercent,
ISNULL(AP.numBelowPercent,0) AS numBelowPercent,
ISNULL(AP.numBelowPriceField,0) AS numBelowPriceField,
@listIds AS vcUnitPriceApprover,
ISNULL(D.numDefaultSalesPricing,2) AS numDefaultSalesPricing,
ISNULL(D.bitReOrderPoint,0) AS bitReOrderPoint,
ISNULL(D.numReOrderPointOrderStatus,0) AS numReOrderPointOrderStatus,
ISNULL(D.numPODropShipBizDoc,0) AS numPODropShipBizDoc,
ISNULL(D.numPODropShipBizDocTemplate,0) AS numPODropShipBizDocTemplate,
ISNULL(D.IsEnableDeferredIncome,0) AS IsEnableDeferredIncome,
ISNULL(D.bitApprovalforTImeExpense,0) AS bitApprovalforTImeExpense,
ISNULL(D.intTimeExpApprovalProcess,0) AS intTimeExpApprovalProcess,
ISNULL(D.bitApprovalforOpportunity,0) AS bitApprovalforOpportunity,
ISNULL(D.intOpportunityApprovalProcess,0) AS intOpportunityApprovalProcess,
@canChangeDiferredIncomeSelection AS canChangeDiferredIncomeSelection,
ISNULL(CAST(D.numDefaultSalesShippingDoc AS int),0) AS numDefaultSalesShippingDoc,
ISNULL(CAST(D.numDefaultPurchaseShippingDoc AS int),0) AS numDefaultPurchaseShippingDoc,
ISNULL(D.bitchkOverRideAssignto,0) AS bitchkOverRideAssignto,
ISNULL(D.vcPrinterIPAddress,'') vcPrinterIPAddress,
ISNULL(D.vcPrinterPort,'') vcPrinterPort,
ISNULL(AP.bitCostApproval,0) bitCostApproval
,ISNULL(AP.bitListPriceApproval,0) bitListPriceApproval
,ISNULL(AP.bitMarginPriceViolated,0) bitMarginPriceViolated
,ISNULL(vcSalesOrderTabs,'Sales Orders') AS vcSalesOrderTabs
,ISNULL(vcSalesQuotesTabs,'Sales Quotes') AS vcSalesQuotesTabs
,ISNULL(vcItemPurchaseHistoryTabs,'Item Purchase History') AS vcItemPurchaseHistoryTabs
,ISNULL(vcItemsFrequentlyPurchasedTabs,'Items Frequently Purchased') AS vcItemsFrequentlyPurchasedTabs
,ISNULL(vcOpenCasesTabs,'Open Cases') AS vcOpenCasesTabs
,ISNULL(vcOpenRMATabs,'Open RMAs') AS vcOpenRMATabs
,ISNULL(bitSalesOrderTabs,0) AS bitSalesOrderTabs
,ISNULL(bitSalesQuotesTabs,0) AS bitSalesQuotesTabs
,ISNULL(bitItemPurchaseHistoryTabs,0) AS bitItemPurchaseHistoryTabs
,ISNULL(bitItemsFrequentlyPurchasedTabs,0) AS bitItemsFrequentlyPurchasedTabs
,ISNULL(bitOpenCasesTabs,0) AS bitOpenCasesTabs
,ISNULL(bitOpenRMATabs,0) AS bitOpenRMATabs
,ISNULL(bitSupportTabs,0) AS bitSupportTabs
,ISNULL(vcSupportTabs,'Support') AS vcSupportTabs
,ISNULL(D.numDefaultSiteID,0) AS numDefaultSiteID
,ISNULL(tintOppStautsForAutoPOBackOrder,0) AS tintOppStautsForAutoPOBackOrder
,ISNULL(tintUnitsRecommendationForAutoPOBackOrder,1) AS tintUnitsRecommendationForAutoPOBackOrder
,ISNULL(bitRemoveGlobalLocation,0) AS bitRemoveGlobalLocation
,ISNULL(numAuthorizePercentage,0) AS numAuthorizePercentage
,ISNULL(bitEDI,0) AS bitEDI
,ISNULL(bit3PL,0) AS bit3PL
,ISNULL(numListItemID,0) AS numListItemID
,ISNULL(bitAllowDuplicateLineItems,0) bitAllowDuplicateLineItems
,ISNULL(tintCommitAllocation,1) tintCommitAllocation
,ISNULL(tintInvoicing,1) tintInvoicing
,ISNULL(tintMarkupDiscountOption,1) tintMarkupDiscountOption
,ISNULL(tintMarkupDiscountValue,1) tintMarkupDiscountValue
,ISNULL(bitIncludeRequisitions,0) bitIncludeRequisitions
,ISNULL(bitCommissionBasedOn,0) bitCommissionBasedOn
,ISNULL(tintCommissionBasedOn,0) tintCommissionBasedOn
,ISNULL(bitDoNotShowDropshipPOWindow,0) bitDoNotShowDropshipPOWindow
,ISNULL(tintReceivePaymentTo,2) tintReceivePaymentTo
,ISNULL(numReceivePaymentBankAccount,0) numReceivePaymentBankAccount
,ISNULL(numOverheadServiceItemID,0) numOverheadServiceItemID,
ISNULL(D.bitDisplayCustomField,0) AS bitDisplayCustomField,
ISNULL(D.bitFollowupAnytime,0) AS bitFollowupAnytime,
ISNULL(D.bitpartycalendarTitle,0) AS bitpartycalendarTitle,
ISNULL(D.bitpartycalendarLocation,0) AS bitpartycalendarLocation,
ISNULL(D.bitpartycalendarDescription,0) AS bitpartycalendarDescription,
ISNULL(D.bitREQPOApproval,0) AS bitREQPOApproval,
ISNULL(D.bitARInvoiceDue,0) AS bitARInvoiceDue,
ISNULL(D.bitAPBillsDue,0) AS bitAPBillsDue,
ISNULL(D.bitItemsToPickPackShip,0) AS bitItemsToPickPackShip,
ISNULL(D.bitItemsToInvoice,0) AS bitItemsToInvoice,
ISNULL(D.bitSalesOrderToClose,0) AS bitSalesOrderToClose,
ISNULL(D.bitItemsToPutAway,0) AS bitItemsToPutAway,
ISNULL(D.bitItemsToBill,0) AS bitItemsToBill,
ISNULL(D.bitPosToClose,0) AS bitPosToClose,
ISNULL(D.bitPOToClose,0) AS bitPOToClose,
ISNULL(D.bitBOMSToPick,0) AS bitBOMSToPick,

ISNULL(D.vchREQPOApprovalEmp,'') AS vchREQPOApprovalEmp,
ISNULL(D.vchARInvoiceDue,'') AS vchARInvoiceDue,
ISNULL(D.vchAPBillsDue,'') AS vchAPBillsDue,
ISNULL(D.vchItemsToPickPackShip,'') AS vchItemsToPickPackShip,
ISNULL(D.vchItemsToInvoice,'') AS vchItemsToInvoice,
ISNULL(D.vchPriceMarginApproval,'') AS vchPriceMarginApproval,
ISNULL(D.vchSalesOrderToClose,'') AS vchSalesOrderToClose,
ISNULL(D.vchItemsToPutAway,'') AS vchItemsToPutAway,
ISNULL(D.vchItemsToBill,'') AS vchItemsToBill,
ISNULL(D.vchPosToClose,'') AS vchPosToClose,
ISNULL(D.vchPOToClose,'') AS vchPOToClose,
ISNULL(D.vchBOMSToPick,'') AS vchBOMSToPick,
ISNULL(D.decReqPOMinValue,0) AS decReqPOMinValue,
ISNULL(D.bitUseOnlyActionItems,0) AS bitUseOnlyActionItems,
ISNULL(D.vcElectiveItemFields,'') AS vcElectiveItemFields,
ISNULL(tintMailProvider,3) tintMailProvider,
ISNULL(D.bitDisplayContractElement,0) AS bitDisplayContractElement,
ISNULL(D.vcEmployeeForContractTimeElement,'') AS vcEmployeeForContractTimeElement,
ISNULL(numARContactPosition,0) numARContactPosition,
ISNULL(bitShowCardConnectLink,0) bitShowCardConnectLink,
ISNULL(vcBluePayFormName,'') vcBluePayFormName,
ISNULL(vcBluePaySuccessURL,'') vcBluePaySuccessURL,
ISNULL(vcBluePayDeclineURL,'') vcBluePayDeclineURL,
ISNULL(bitUseDeluxeCheckStock,0) bitUseDeluxeCheckStock,
ISNULL(D.bitEnableSmartyStreets,0) bitEnableSmartyStreets,
ISNULL(D.vcSmartyStreetsAPIKeys,'') vcSmartyStreetsAPIKeys,
ISNULL(bitReceiveOrderWithNonMappedItem,0) bitReceiveOrderWithNonMappedItem,
ISNULL(numItemToUseForNonMappedItem,0) numItemToUseForNonMappedItem,
ISNULL(bitUsePredefinedCustomer,0) bitUsePredefinedCustomer,
ISNULL(bitUsePreviousEmailBizDoc,0) bitUsePreviousEmailBizDoc,
CASE WHEN ISNULL((SELECT COUNT(*) FROM [ImapUserDetails] WHERE 
			numUserCntId = -1 
			AND numDomainID = @numDomainID),0) > 0 THEN 1 ELSE 0 END AS bitImapConfigured 
from Domain D  
LEFT JOIN eCommerceDTL eComm  on eComm.numDomainID=D.numDomainID  
LEFT JOIN ApprovalProcessItemsClassification AP ON AP.numDomainID = D.numDomainID
where (D.numDomainID=@numDomainID OR @numDomainID=-1)



GO
/****** Object:  StoredProcedure [dbo].[USP_GetSitePageDetail]    Script Date: 08/08/2009 16:16:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--SELECT * FROM [PageElementMaster]
-- exec USP_GetSitePageDetail 'home.aspx',3
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetSitePageDetail')
DROP PROCEDURE USP_GetSitePageDetail
GO
CREATE PROCEDURE [dbo].[USP_GetSitePageDetail]
          @vcPageURL varchar(1000),
          @numSiteID NUMERIC(9)
AS
  DECLARE  @numPageID NUMERIC(9)
  DECLARE  @numTemplateID NUMERIC(9)
  DECLARE  @numDomainID NUMERIC(9)
  
  SELECT @numPageID = numPageID,@numTemplateID=numTemplateID,@numDomainID=numDomainID
  FROM   [SitePages]
  WHERE  LOWER([vcPageURL]) = LOWER(@vcPageURL)
         AND [numSiteID] = @numSiteID
  IF @numPageID > 0
    BEGIN
      SELECT numPageID,
             ISNULL(SP.vcPageName,'') vcPageName,
             ISNULL(SP.vcPageURL,'') vcPageURL,
             tintPageType,
             ISNULL(SP.vcPageTitle,'') vcPageTitle,
--             [numTemplateID],
--             (SELECT vcTemplateName
--              FROM   [SiteTemplates] st
--              WHERE  st.[numTemplateID] = SP.numTemplateID) vcTemplateName,
             ISNULL(MT.vcMetaTag,'') vcMetaTag,
             ISNULL(MT.vcMetaKeywords,'') vcMetaKeywords,
             ISNULL(MT.vcMetaDescription,'') vcMetaDescription,
             ISNULL(MT.numMetaID,0) numMetaID,
             [numDomainID]
      FROM   SitePages SP
             LEFT OUTER JOIN MetaTags MT
               ON MT.numReferenceID = SP.numPageID
      WHERE  (numPageID = @numPageID
               OR @numPageID = 0)
             AND numSiteID = @numSiteID
      --Select StyleSheets for Page
      SELECT S.[numCssID],
             S.[StyleFileName]
      FROM   [StyleSheetDetails] SD
             INNER JOIN [StyleSheets] S
               ON SD.[numCssID] = S.[numCssID]
      WHERE  [numPageID] = @numPageID
             AND [tintType] = 0
      --Select Javascripts for Page
      SELECT S.[numCssID],
             S.[StyleFileName]
      FROM   [StyleSheetDetails] SD
             INNER JOIN [StyleSheets] S
               ON SD.[numCssID] = S.[numCssID]
      WHERE  [numPageID] = @numPageID
             AND [tintType] = 1
             ORDER BY intDisplayOrder 
	 -- Select Templates
	 EXECUTE USP_GetSiteTemplates @numTemplateID,@numSiteID,@numDomainID;
	 --select all templates from site to match for nested template
--	 
    END
    


GO
/****** Object:  StoredProcedure [dbo].[USP_GetSitePages]    Script Date: 08/08/2009 16:10:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetSitePages')
DROP PROCEDURE USP_GetSitePages
GO
CREATE PROCEDURE [dbo].[USP_GetSitePages]
          @numPageID   NUMERIC(9),
          @numSiteID   NUMERIC(9),
          @numDomainID NUMERIC(9)
AS
  SELECT numPageID,
		 numSiteID,
         ISNULL(SP.vcPageName,'') vcPageName,
         ISNULL(SP.vcPageURL,'') vcPageURL,
         ISNULL(SP.tintPageType,2) tintPageType,
         ISNULL(SP.vcPageTitle,'') vcPageTitle,
         [numTemplateID],
         (SELECT vcTemplateName FROM [SiteTemplates] st WHERE st.[numTemplateID] = SP.numTemplateID) vcTemplateName,
         --ISNULL(MT.vcMetaTag,'') vcMetaTag,
         ISNULL([vcMetaDescription],'') AS vcMetaDescription,
         ISNULL([vcMetaKeywords],'') AS vcMetaKeywords,
         ISNULL(MT.numMetaID,0) numMetaID,
         ISNULL((SELECT vcHostName FROM Sites WHERE [numSiteID] = @numSiteID),'') vcHostName,
		 CASE WHEN [bitIsActive] = 1 THEN 'Inactivate'
		      ELSE 'Activate'
		 END AS 'Status',
		 [bitIsActive],
		 ISNULL([bitIsMaintainScroll],0) AS bitIsMaintainScroll
  FROM   SitePages SP
         LEFT OUTER JOIN MetaTags MT
           ON MT.numReferenceID = SP.numPageID
  WHERE  (numPageID = @numPageID
           OR @numPageID = 0)
         AND numSiteID = @numSiteID
         AND numDomainID = @numDomainID
	ORDER BY
		vcPageName

  IF @numPageID > 0
    BEGIN
      --Select StyleSheets for Page
      SELECT S.[numCssID]
      FROM   [StyleSheetDetails] SD
             INNER JOIN [StyleSheets] S
               ON SD.[numCssID] = S.[numCssID]
      WHERE  [numPageID] = @numPageID
             AND [tintType] = 0
      --Select Javascripts for Page
      SELECT S.[numCssID]
      FROM   [StyleSheetDetails] SD
             INNER JOIN [StyleSheets] S
               ON SD.[numCssID] = S.[numCssID]
      WHERE  [numPageID] = @numPageID
             AND [tintType] = 1
    END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_Import_SaveItemDetails')
DROP PROCEDURE USP_Import_SaveItemDetails
GO
CREATE PROCEDURE [dbo].[USP_Import_SaveItemDetails]  
	@numDomainID NUMERIC(18,0)
	,@numUserCntID NUMERIC(18,0)
	,@numItemCode NUMERIC(18,0) OUTPUT
	,@numShipClass NUMERIC(18,0)
	,@vcItemName VARCHAR(300)
	,@monListPrice DECIMAL(20,5)
	,@txtItemDesc VARCHAR(1000)
	,@vcModelID VARCHAR(200)
	,@vcManufacturer VARCHAR(250)
	,@numBarCodeId VARCHAR(50)
	,@fltWidth FLOAT
	,@fltHeight FLOAT
	,@fltWeight FLOAT
	,@fltLength FLOAT
	,@monAverageCost DECIMAL(20,5)
	,@bitSerialized BIT
	,@bitKitParent BIT
	,@bitAssembly BIT
	,@IsArchieve BIT
	,@bitLotNo BIT
	,@bitAllowBackOrder BIT
	,@numIncomeChartAcntId NUMERIC(18,0)
	,@numAssetChartAcntId NUMERIC(18,0)
	,@numCOGsChartAcntId NUMERIC(18,0)
	,@numItemClassification NUMERIC(18,0)
	,@numItemGroup NUMERIC(18,0)
	,@numPurchaseUnit NUMERIC(18,0)
	,@numSaleUnit NUMERIC(18,0)
	,@numItemClass NUMERIC(18,0)
	,@numBaseUnit NUMERIC(18,0)
	,@vcSKU VARCHAR(50)
	,@charItemType CHAR(1)
	,@bitTaxable BIT
	,@txtDesc NVARCHAR(MAX)
	,@vcExportToAPI VARCHAR(50)
	,@bitAllowDropShip BIT
	,@bitMatrix BIT
	,@vcItemAttributes VARCHAR(2000)
	,@vcCategories VARCHAR(1000)
	,@txtShortDesc NVARCHAR(MAX)
	,@numReOrder FLOAT
	,@bitAutomateReorderPoint BIT
AS
BEGIN
BEGIN TRY
BEGIN TRANSACTION
	-- IMPORTANT: PLEASE MAKE SURE NEW FIELD WHICH YOUR ARE ADDING IN STORE PROCEDURE FOR UPDATE ARE ADDED IN SELECT IN PROCEDURE USP_Import_GetItemDetails

	IF @numAssetChartAcntId = 0
	BEGIN
		SET @numAssetChartAcntId =NULL
	END

	DECLARE @bitAttributesChanged AS BIT  = 1
	DECLARE @bitOppOrderExists AS BIT = 0
	DECLARE @hDoc AS INT     
	                                                                        	
	DECLARE @TempTable TABLE 
	(
		ID INT IDENTITY(1,1),
		Fld_ID NUMERIC(18,0),
		Fld_Value NUMERIC(18,0)
	)   

	IF ISNULL(@bitMatrix,0) = 1 AND ISNULL(@numItemGroup,0) = 0
	BEGIN
		RAISERROR('ITEM_GROUP_NOT_SELECTED',16,1)
		RETURN
	END
	ELSE IF ISNULL(@bitMatrix,0) = 1 AND LEN(ISNULL(@vcItemAttributes,'')) = 0 AND ISNULL(@numItemCode,0) = 0
	BEGIN
		RAISERROR('ITEM_ATTRIBUTES_NOT_SELECTED',16,1)
		RETURN
	END
	ELSE IF ISNULL(@bitMatrix,0) = 1 AND LEN(ISNULL(@vcItemAttributes,'')) > 0
	BEGIN
		DECLARE @bitDuplicate AS BIT = 0
	
		IF DATALENGTH(ISNULL(@vcItemAttributes,''))>2
		BEGIN
			EXEC sp_xml_preparedocument @hDoc OUTPUT, @vcItemAttributes
	                                      
			INSERT INTO @TempTable 
			(
				Fld_ID,
				Fld_Value
			)    
			SELECT
				*          
			FROM 
				OPENXML (@hDoc,'/NewDataSet/CusFlds',2)                                                  
			WITH 
				(Fld_ID NUMERIC(18,0),Fld_Value NUMERIC(18,0)) 
			ORDER BY 
				Fld_ID       

			DECLARE @strAttribute VARCHAR(500) = ''
			DECLARE @i AS INT = 1
			DECLARE @COUNT AS INT 
			DECLARE @numFldID AS NUMERIC(18,0)
			DECLARE @numFldValue AS NUMERIC(18,0)
			SELECT @COUNT = COUNT(*) FROM @TempTable

			WHILE @i <= @COUNT
			BEGIN
				SELECT @numFldID=Fld_ID,@numFldValue=Fld_Value FROM @TempTable WHERE ID=@i

				IF ISNUMERIC(@numFldValue)=1
				BEGIN
					SELECT @strAttribute=@strAttribute+Fld_label FROM CFW_Fld_Master WHERE Fld_id=@numFldID AND Grp_id = 9
                
					IF LEN(@strAttribute) > 0
					BEGIN
						SELECT @strAttribute=@strAttribute+':'+ vcData +',' FROM ListDetails WHERE numListItemID=@numFldValue
					END
				END

				SET @i = @i + 1
			END

			SET @strAttribute = SUBSTRING(@strAttribute,0,LEN(@strAttribute))

			IF
				(SELECT
					COUNT(*)
				FROM
					Item
				INNER JOIN
					ItemAttributes
				ON
					Item.numItemCode = ItemAttributes.numItemCode
					AND ItemAttributes.numDomainID = @numDomainID                     
				WHERE
					Item.numItemCode <> @numItemCode
					AND Item.vcItemName = @vcItemName
					AND Item.numItemGroup = @numItemGroup
					AND Item.numDomainID = @numDomainID
					AND dbo.fn_GetItemAttributes(@numDomainID,Item.numItemCode) = @strAttribute
				) > 0
			BEGIN
				RAISERROR('ITEM_WITH_ATTRIBUTES_ALREADY_EXISTS',16,1)
				RETURN
			END

			If ISNULL(@numItemCode,0) > 0 AND dbo.fn_GetItemAttributes(@numDomainID,@numItemCode) = @strAttribute
			BEGIN
				SET @bitAttributesChanged = 0
			END

			EXEC sp_xml_removedocument @hDoc 
		END	
	END
	ELSE IF ISNULL(@bitMatrix,0) = 0
	BEGIN
		-- REMOVE ATTRIBUTES CONFIGURATION OF ITEM AND WAREHOUSE
		DELETE FROM ItemAttributes WHERE numDomainID=@numDomainID AND numItemCode=@numItemCode
	END 


	IF ISNULL(@numItemGroup,0) > 0 AND ISNULL(@bitMatrix,0) = 1
	BEGIN
		IF (SELECT COUNT(*) FROM ItemGroupsDTL WHERE numItemGroupID = @numItemGroup AND tintType=2) = 0
		BEGIN
			RAISERROR('ITEM_GROUP_NOT_CONFIGURED',16,1)
			RETURN
		END
		ELSE IF EXISTS (SELECT 
							CFM.Fld_id
						FROM 
							CFW_Fld_Master CFM
						LEFT JOIN
							ListDetails LD
						ON
							CFM.numlistid = LD.numListID
						WHERE
							CFM.numDomainID = @numDomainID
							AND CFM.Fld_id IN (SELECT numOppAccAttrID FROM ItemGroupsDTL WHERE numItemGroupID=@numItemGroup AND tintType = 2)
						GROUP BY
							CFM.Fld_id HAVING COUNT(LD.numListItemID) = 0)
		BEGIN
			RAISERROR('ITEM_GROUP_NOT_CONFIGURED',16,1)
			RETURN
		END
	END


	IF ISNULL(@numItemCode,0) = 0
	BEGIN
		IF (ISNULL(@vcItemName,'') = '')
		BEGIN
			RAISERROR('ITEM_NAME_NOT_SELECTED',16,1)
			RETURN
		END
        ELSE IF (ISNULL(@charItemType,'') = '' OR ISNULL(@charItemType,'') = '0' )
		BEGIN
			RAISERROR('ITEM_TYPE_NOT_SELECTED',16,1)
			RETURN
		END
        ELSE
		BEGIN 
		    If (@charItemType = 'P')
			BEGIN
				IF ((SELECT COUNT(*) FROM Chart_Of_Accounts WHERE numDomainId=@numDomainID AND numAccountId=@numAssetChartAcntId) = 0)
				BEGIN
					RAISERROR('INVALID_ASSET_ACCOUNT',16,1)
					RETURN
				END
			END
			ELSE IF @numAssetChartAcntId = 0
			BEGIN
				SET @numAssetChartAcntId = NULL
			END

			-- This is common check for CharItemType P and Other
			IF ((SELECT COUNT(*) FROM Chart_Of_Accounts WHERE numDomainId=@numDomainID AND numAccountId=@numCOGSChartAcntId) = 0)
			BEGIN
				RAISERROR('INVALID_COGS_ACCOUNT',16,1)
				RETURN
			END

			IF ((SELECT COUNT(*) FROM Chart_Of_Accounts WHERE numDomainId=@numDomainID AND numAccountId=@numIncomeChartAcntId) = 0)
			BEGIN
				RAISERROR('INVALID_INCOME_ACCOUNT',16,1)
				RETURN
			END
		END


		INSERT INTO Item
		(
			numDomainID
			,numCreatedBy
			,bintCreatedDate
			,bintModifiedDate
			,numModifiedBy
			,numShipClass
			,vcItemName
			,monListPrice
			,txtItemDesc
			,vcModelID
			,vcManufacturer
			,numBarCodeId
			,fltWidth
			,fltHeight
			,fltWeight
			,fltLength
			,monAverageCost
			,bitSerialized
			,bitKitParent
			,bitAssembly
			,IsArchieve
			,bitLotNo
			,bitAllowBackOrder
			,numIncomeChartAcntId
			,numAssetChartAcntId
			,numCOGsChartAcntId
			,numItemClassification
			,numItemGroup
			,numPurchaseUnit
			,numSaleUnit
			,numItemClass
			,numBaseUnit
			,vcSKU
			,charItemType
			,bitTaxable
			,vcExportToAPI
			,bitAllowDropShip
			,bitMatrix
			,numManufacturer
			,bitAutomateReorderPoint
		)
		VALUES
		(
			@numDomainID
			,@numUserCntID
			,GETUTCDATE()
			,GETUTCDATE()
			,@numUserCntID
			,@numShipClass
			,@vcItemName
			,@monListPrice
			,@txtItemDesc
			,@vcModelID
			,@vcManufacturer
			,@numBarCodeId
			,@fltWidth
			,@fltHeight
			,@fltWeight
			,@fltLength
			,@monAverageCost
			,@bitSerialized
			,@bitKitParent
			,@bitAssembly
			,@IsArchieve
			,@bitLotNo
			,@bitAllowBackOrder
			,NULLIF(@numIncomeChartAcntId,0)
			,NULLIF(@numAssetChartAcntId,0)
			,NULLIF(@numCOGsChartAcntId,0)
			,@numItemClassification
			,@numItemGroup 
			,@numPurchaseUnit 
			,@numSaleUnit
			,@numItemClass
			,@numBaseUnit
			,@vcSKU
			,@charItemType
			,@bitTaxable
			,@vcExportToAPI
			,@bitAllowDropShip
			,@bitMatrix
			,(CASE WHEN LEN(ISNULL(@vcManufacturer,'')) > 0 THEN ISNULL((SELECT TOP 1 numDivisionID FROM CompanyInfo INNER JOIN DivisionMaster ON CompanyInfo.numCompanyId=DivisionMaster.numCompanyID WHERE CompanyInfo.numDOmainID=@numDomainID AND LOWER(vcCompanyName) = LOWER(@vcManufacturer)),0) ELSE 0 END)
			,@bitAutomateReorderPoint
		)

		SET @numItemCode = SCOPE_IDENTITY()

		INSERT INTO ItemExtendedDetails (numItemCode,txtDesc,txtShortDesc) VALUES (@numItemCode,@txtDesc,@txtShortDesc)

		IF @charItemType = 'P'
		BEGIN
			
			DECLARE @TEMPWarehouse TABLE
			(
				ID INT IDENTITY(1,1)
				,numWarehouseID NUMERIC(18,0)
			)
		
			INSERT INTO @TEMPWarehouse (numWarehouseID) SELECT numWareHouseID FROM Warehouses WHERE numDomainID=@numDomainID

			DECLARE @j AS INT = 1
			DECLARE @jCount AS INT
			DECLARE @numTempWarehouseID NUMERIC(18,0)
			SELECT @jCount = COUNT(*) FROM @TEMPWarehouse

			WHILE @j <= @jCount
			BEGIN
				SELECT @numTempWarehouseID=numWarehouseID FROM @TEMPWarehouse WHERE ID=@j

				EXEC USP_WarehouseItems_Save @numDomainID,
											@numUserCntID,
											0,
											@numTempWarehouseID,
											0,
											@numItemCode,
											'',
											@monListPrice,
											0,
											0,
											'',
											'',
											'',
											'',
											0
 
				SET @j = @j + 1
			END
		END 
	END
	ELSE IF ISNULL(@numItemCode,0) > 0 AND EXISTS (SELECT numItemCode FROM Item WHERE numDomainID=@numDomainID AND numItemCode=@numItemCode)
	BEGIN
		IF (SELECT COUNT(OI.numOppId) FROM dbo.OpportunityItems OI WHERE OI.numItemCode =@numItemCode) > 0
		BEGIN
			SET @bitOppOrderExists = 1
		END
		ELSE IF (SELECT COUNT(OI.numOppId) FROM dbo.OpportunityKitItems OI WHERE OI.numChildItemID =@numItemCode) > 0
		BEGIN
			SET @bitOppOrderExists = 1
		END
		ELSE IF (SELECT COUNT(OI.numOppId) FROM dbo.OpportunityKitChildItems OI WHERE OI.numItemID =@numItemCode) > 0
		BEGIN
			SET @bitOppOrderExists = 1
		END

		DECLARE @OldGroupID NUMERIC 
		SELECT @OldGroupID = ISNULL(numItemGroup,0) FROM [Item] WHERE numitemcode = @numItemCode AND [numDomainId] = @numDomainID
	
		DECLARE @monOldAverageCost AS DECIMAL(18,4);SET @monOldAverageCost=0
		SELECT @monOldAverageCost=(CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(monAverageCost,0) END) FROM Item WHERE numItemCode =@numItemCode AND [numDomainId] = @numDomainID
	   
		UPDATE 
			Item 
		SET 
			vcItemName=@vcItemName,txtItemDesc=@txtItemDesc,charItemType=@charItemType,monListPrice= @monListPrice,numItemClassification=@numItemClassification,                                             
			bitTaxable=@bitTaxable,vcSKU = @vcSKU,bitKitParent=@bitKitParent,numDomainID=@numDomainID,bintModifiedDate=getutcdate(),numModifiedBy=@numUserCntID
			,bitSerialized=@bitSerialized,vcModelID=@vcModelID,numItemGroup=@numItemGroup,numCOGsChartAcntId=NULLIF(@numCOGsChartAcntId,0),numAssetChartAcntId=NULLIF(@numAssetChartAcntId,0),                                      
			numIncomeChartAcntId=NULLIF(@numIncomeChartAcntId,0),fltWeight=@fltWeight,fltHeight=@fltHeight,fltWidth=@fltWidth,     
			fltLength=@fltLength,bitAllowBackOrder=@bitAllowBackOrder,bitAssembly=@bitAssembly,numBarCodeId=@numBarCodeId,
			vcManufacturer=@vcManufacturer,numBaseUnit=@numBaseUnit,numPurchaseUnit=@numPurchaseUnit,numSaleUnit=@numSaleUnit,bitLotNo=@bitLotNo,
			IsArchieve = @IsArchieve,numItemClass=@numItemClass,vcExportToAPI=@vcExportToAPI,
			numShipClass=@numShipClass,bitAllowDropShip=@bitAllowDropShip, bitArchiveItem = @IsArchieve,bitMatrix=@bitMatrix,
			numManufacturer=(CASE WHEN LEN(ISNULL(@vcManufacturer,'')) > 0 THEN ISNULL((SELECT TOP 1 numDivisionID FROM CompanyInfo INNER JOIN DivisionMaster ON CompanyInfo.numCompanyId=DivisionMaster.numCompanyID WHERE CompanyInfo.numDOmainID=@numDomainID AND LOWER(vcCompanyName) = LOWER(@vcManufacturer)),0) ELSE 0 END)
			,bitAutomateReorderPoint=@bitAutomateReorderPoint
		WHERE 
			numItemCode=@numItemCode 
			AND numDomainID=@numDomainID

		IF NOT EXISTS(SELECT numItemCode FROM dbo.OpportunityItems WHERE numItemCode = @numItemCode)
		BEGIN
			IF (((SELECT ISNULL(SUM(numOnHand),0) FROM WareHouseItems WHERE numItemID = @numItemCode AND numDomainId=@numDomainID) = 0) AND ((SELECT (CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(monAverageCost,0) END) FROM Item  WHERE numItemCode = @numItemCode AND numDomainId=@numDomainID ) = 0))
			BEGIN
				UPDATE Item SET monAverageCost=@monAverageCost  where numItemCode=@numItemCode  AND numDomainID=@numDomainID	
			END
		END
	
		--If Average Cost changed then add in Tracking
		IF ISNULL(@monAverageCost,0) != ISNULL(@monOldAverageCost,0)
		BEGIN
			DECLARE @numWareHouseItemID NUMERIC(9);SET @numWareHouseItemID=0
			DECLARE @numTotal int;SET @numTotal=0
			SET @i=0
			DECLARE @vcDescription AS VARCHAR(100);SET @vcDescription=''
		
			SELECT @numTotal=COUNT(*) FROM WareHouseItems where numItemID=@numItemCode AND numDomainId=@numDomainId
	     
			WHILE @i < @numTotal
			BEGIN
				SELECT @numWareHouseItemID = min(numWareHouseItemID) FROM WareHouseItems 
					WHERE numItemID=@numItemCode AND numDomainId=@numDomainId and numWareHouseItemID > @numWareHouseItemID 
				
				IF @numWareHouseItemID>0
				BEGIN
					SET @vcDescription='Average Cost Changed Manually OLD : ' + CAST(ISNULL(@monOldAverageCost,0) AS VARCHAR(20))
				
					DECLARE @numDomain AS NUMERIC(18,0)
					SET @numDomain = @numDomainID
					EXEC dbo.USP_ManageWareHouseItems_Tracking
						@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)		
						@numReferenceID = @numItemCode, --  numeric(9, 0)
						@tintRefType = 1, --  tinyint
						@vcDescription = @vcDescription, --  varchar(100)
						@tintMode = 0, --  tinyint
						@numModifiedBy = @numUserCntID, --  numeric(9, 0)
						@numDomainID = @numDomain
				END
		    
				SET @i = @i + 1
			END
		END
 
		IF	@bitTaxable = 1
		BEGIN
			IF NOT EXISTS(SELECT * FROM dbo.ItemTax WHERE numItemCode = @numItemCode AND numTaxItemID = 0)
			BEGIN
			    INSERT INTO dbo.ItemTax 
				(
					numItemCode,
					numTaxItemID,
					bitApplicable
				)
				VALUES 
				( 
					@numItemCode,
					0,
					1 
				) 						
			END
		END	 
      
		IF @charItemType='S' or @charItemType='N'                                                                       
		BEGIN
			DELETE FROM WareHouseItmsDTL where numWareHouseItemID in (select numWareHouseItemID from WareHouseItems where numItemID=@numItemCode AND [WareHouseItems].[numDomainID] = @numDomainID)
			DELETE FROM WareHouseItems where numItemID=@numItemCode AND [WareHouseItems].[numDomainID] = @numDomainID
		END

		IF NOT EXISTS (SELECT numItemCode FROM ItemExtendedDetails  where numItemCode=@numItemCode)
		BEGIN
			INSERT INTO ItemExtendedDetails (numItemCode,txtDesc,txtShortDesc) VALUES (@numItemCode,@txtDesc,@txtShortDesc)
		END
		ELSE
		BEGIN
			UPDATE ItemExtendedDetails SET txtDesc=@txtDesc,@txtShortDesc=txtShortDesc WHERE numItemCode=@numItemCode
		END
	END

	IF ISNULL(@numItemGroup,0) = 0 OR ISNULL(@bitMatrix,0) = 1
	BEGIN
		UPDATE WareHouseItems SET monWListPrice = @monListPrice, vcWHSKU=@vcSKU, vcBarCode=@numBarCodeId WHERE numDomainID = @numDomainID AND numItemID = @numItemCode
	END

	IF @vcCategories <> ''	
	BEGIN
		INSERT INTO ItemCategory( numItemID,numCategoryID )  SELECT @numItemCode,ID from dbo.SplitIDs(@vcCategories,',')	
	END

	-- SAVE MATRIX ITEM ATTRIBUTES
	IF @numItemCode > 0 AND ISNULL(@bitAttributesChanged,0)=1 AND ISNULL(@bitMatrix,0) = 1 AND LEN(ISNULL(@vcItemAttributes,'')) > 0 AND ISNULL(@bitOppOrderExists,0) <> 1 AND (SELECT COUNT(*) FROM @TempTable) > 0
	BEGIN
		-- DELETE PREVIOUS ITEM ATTRIBUTES
		DELETE FROM ItemAttributes WHERE numDomainID=@numDomainID AND numItemCode=@numItemCode

		-- INSERT NEW ITEM ATTRIBUTES
		INSERT INTO ItemAttributes
		(
			numDomainID
			,numItemCode
			,FLD_ID
			,FLD_Value
		)
		SELECT
			@numDomainID
			,@numItemCode
			,Fld_ID
			,Fld_Value
		FROM
			@TempTable

		IF (SELECT COUNT(*) FROM WarehouseItems WHERE numDomainID=@numDomainID AND numItemID=@numItemCode) > 0
		BEGIN

			-- DELETE PREVIOUS WAREHOUSE ATTRIBUTES
			DELETE FROM CFW_Fld_Values_Serialized_Items WHERE RecId IN (SELECT ISNULL(numWarehouseItemID,0) FROM WarehouseItems WHERE numDomainID=@numDomainID AND numItemID=@numItemCode)

			-- INSERT NEW WAREHOUSE ATTRIBUTES
			INSERT INTO CFW_Fld_Values_Serialized_Items
			(
				Fld_ID,
				Fld_Value,
				RecId,
				bitSerialized
			)                                                  
			SELECT 
				Fld_ID,
				ISNULL(CAST(Fld_Value AS VARCHAR),'') AS Fld_Value,
				TEMPWarehouse.numWarehouseItemID,
				0 
			FROM 
				@TempTable  
			OUTER APPLY
			(
				SELECT
					numWarehouseItemID
				FROM 
					WarehouseItems 
				WHERE 
					numDomainID=@numDomainID 
					AND numItemID=@numItemCode
			) AS TEMPWarehouse
		END
	END	 

	UPDATE WareHouseItems SET numReorder=ISNULL(@numReOrder,0) WHERE numDomainID=@numDomain AND numItemID=@numItemCode
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH        
END
/****** Object:  StoredProcedure [dbo].[USP_InsertImapUserDtls]    Script Date: 07/26/2008 16:19:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_insertimapuserdtls')
DROP PROCEDURE usp_insertimapuserdtls
GO
CREATE PROCEDURE [dbo].[USP_InsertImapUserDtls]
    @vcImapPassword AS VARCHAR(100),
    @vcImapServerUrl AS VARCHAR(200),
    @numImapSSLPort AS NUMERIC,
    @bitImapSsl AS BIT,
    @bitImap AS BIT,
    @numUserCntId AS NUMERIC(9),--tintMode=0 then supply UserID instead of UserContactID
    @numDomainID AS NUMERIC(9),
    @bitUseUserName AS BIT,
    @numLastUID AS NUMERIC(9),
    @tintMode TINYINT,
    @vcImapUserName AS VARCHAR(50)--For Support Email Address
AS 
DECLARE @numUserDetailId NUMERIC(9);SET @numUserDetailId=@numUserCntId

--IF @numUserCntId=-1 : For Support Email 
IF @numUserCntId>0
BEGIN
	SELECT  @numUserDetailId = numUserDetailId
	FROM    UserMaster
	WHERE   numUserId = @numUserCntId

	UPDATE UserMaster SET tintMailProvider=3 WHERE numUserId=@numUserCntId
END

--delete [ImapUserDetails] where numdomainId = @numDomainID and numUserCntId = @numUserDetailId
IF @tintMode = 0 
    BEGIN
        IF NOT EXISTS ( SELECT  *
                        FROM    [ImapUserDetails]
                        WHERE   numdomainId = @numDomainID
                                AND numUserCntId = @numUserDetailId ) 
            BEGIN
                INSERT  INTO [ImapUserDetails] ( bitImap,
                                                 [vcImapServerUrl],
                                                 [vcImapPassword],
                                                 [bitSSl],
                                                 [numPort],
                                                 [numDomainId],
                                                 [numUserCntId],
                                                 bitUseUserName,
                                                 tintRetryLeft,
                                                 numLastUID,
                                                 bitNotified,vcImapUserName)
                VALUES  (
                          @bitImap,
                          @vcImapServerUrl,
                          @vcImapPassword,
                          @bitImapSsl,
                          @numImapSSLPort,
                          @numDomainID,
                          @numUserDetailId,
                          @bitUseUserName,
                          10,
                          0,
                          1,@vcImapUserName
                        )
            END
        ELSE 
            BEGIN
                UPDATE  ImapUserDetails
                SET     bitImap = @bitImap,
                        [vcImapServerUrl] = @vcImapServerUrl,
                        [vcImapPassword] = @vcImapPassword,
                        [bitSSl] = @bitImapSsl,
                        [numPort] = @numImapSSLPort,
                        bitUseUserName = @bitUseUserName,
                        bitNotified=1,
                        tintRetryLeft=10,vcImapUserName=@vcImapUserName
                WHERE   numdomainId = @numDomainID
                        AND numUserCntId = @numUserDetailId 
            END
            
    END 
ELSE 
    IF @tintMode = 1 
        BEGIN
		
            UPDATE  ImapUserDetails
            SET     numLastUID = @numLastUID
            WHERE   numdomainId = @numDomainID
                    AND numUserCntId = @numUserCntId
        END
        
ELSE IF @tintMode = 2 
        BEGIN
		
            UPDATE  ImapUserDetails
            SET     bitNotified=1
            WHERE   numdomainId = @numDomainID
                    AND numUserCntId = @numUserCntId
        END
--Created By Anoop Jayaraj          
--exec USP_ItemDetailsForEcomm @numItemCode=859064,@numWareHouseID=1074,@numDomainID=172,@numSiteId=90
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_itemdetailsforecomm')
DROP PROCEDURE USP_ItemDetailsForEcomm
GO
CREATE PROCEDURE [dbo].[USP_ItemDetailsForEcomm]
@numItemCode as numeric(9),          
@numWareHouseID as numeric(9),    
@numDomainID as numeric(9),
@numSiteId as numeric(9),
@vcCookieId as VARCHAR(MAX)=null,
@numUserCntID AS NUMERIC(9)=0,
@numDivisionID AS NUMERIC(18,0) = 0                                        
AS     
BEGIN
/*Removed warehouse parameter, and taking Default warehouse by default from DB*/
    
	DECLARE @bitShowInStock AS BIT        
	DECLARE @bitShowQuantity AS BIT
	DECLARE @bitAutoSelectWarehouse AS BIT              
	DECLARE @numDefaultWareHouseID AS NUMERIC(9)
	DECLARE @numDefaultRelationship AS NUMERIC(18,0)
	DECLARE @numDefaultProfile AS NUMERIC(18,0)
	DECLARE @tintPreLoginPriceLevel AS TINYINT
	DECLARE @tintPriceLevel AS TINYINT
	DECLARE @bitMatrix BIT
	DECLARE @numItemGroup NUMERIC(18,0)
	SET @bitShowInStock=0        
	SET @bitShowQuantity=0        
       
        
	IF ISNULL(@numDivisionID,0) > 0
	BEGIN
		SELECT 
			@numDefaultRelationship=ISNULL(numCompanyType,0)
			,@numDefaultProfile=ISNULL(vcProfile,0)
			,@tintPriceLevel=ISNULL(tintPriceLevel,0) 
		FROM 
			DivisionMaster 
		INNER JOIN 
			CompanyInfo 
		ON 
			DivisionMaster.numCompanyID=CompanyInfo.numCompanyId 
		WHERE 
			numDivisionID=@numDivisionID 
			AND DivisionMaster.numDomainID=@numDomainID
	END     
      
	--KEEP THIS BELOW ABOVE IF CONDITION 
	SELECT 
		@bitShowInStock=ISNULL(bitShowInStock,0)
		,@bitShowQuantity=ISNULL(bitShowQOnHand,0)
		,@bitAutoSelectWarehouse=ISNULL(bitAutoSelectWarehouse,0)
		,@numDefaultRelationship=(CASE WHEN ISNULL(@numDivisionID,0) > 0 THEN @numDefaultRelationship ELSE numRelationshipId END)
		,@numDefaultProfile=(CASE WHEN ISNULL(@numDivisionID,0) > 0 THEN @numDefaultProfile ELSE numProfileId END)
		,@tintPreLoginPriceLevel=(CASE WHEN ISNULL(bitShowPriceUsingPriceLevel,0)=1 THEN (CASE WHEN @numDivisionID > 0 THEN (CASE WHEN ISNULL(bitShowPriceUsingPriceLevel,0)=1 THEN ISNULL(@tintPriceLevel,0) ELSE 0 END) ELSE ISNULL(tintPreLoginProceLevel,0) END) ELSE 0 END)
	FROM 
		eCommerceDTL 
	WHERE 
		numDomainID=@numDomainID

	IF @numWareHouseID=0
	BEGIN
		SELECT @numDefaultWareHouseID=ISNULL(numDefaultWareHouseID,0) FROM [eCommerceDTL] WHERE [numDomainID]=@numDomainID	
		SET @numWareHouseID =@numDefaultWareHouseID;
	END

	DECLARE @vcWarehouseIDs AS VARCHAR(1000)
	IF @bitAutoSelectWarehouse = 1
	BEGIN		
		SELECT @vcWarehouseIDs = ISNULL( STUFF((SELECT DISTINCT ',' + CAST(numWarehouseID AS VARCHAR(10)) FROM [dbo].[Warehouses] WHERE [Warehouses].[numDomainID] = @numDomainID FOR XML PATH('')),1, 1, '') , '')
	END
	ELSE
	BEGIN
		SET @vcWarehouseIDs = @numWareHouseID
	END

	/*If qty is not found in default warehouse then choose next warehouse with sufficient qty */
	IF @bitAutoSelectWarehouse = 1 AND ( SELECT COUNT(*) FROM dbo.WareHouseItems WHERE  numItemID = @numItemCode AND numWarehouseID= @numWareHouseID ) = 0 
	BEGIN
		SELECT TOP 1 
			@numDefaultWareHouseID = numWareHouseID 
		FROM 
			dbo.WareHouseItems 
		WHERE 
			numItemID = @numItemCode 
			AND numOnHand > 0 
		ORDER BY 
			numOnHand DESC 
		
		IF (ISNULL(@numDefaultWareHouseID,0)>0)
			SET @numWareHouseID =@numDefaultWareHouseID;
	END

    DECLARE @UOMConversionFactor AS DECIMAL(18,5)
      
    SELECT 
		@UOMConversionFactor= ISNULL(dbo.fn_UOMConversion(ISNULL(I.numSaleUnit,0),I.numItemCode,I.numDomainId,ISNULL(I.numBaseUnit,0)),0)
		,@bitMatrix=ISNULL(bitMatrix,0)
		,@numItemGroup=ISNULL(numItemGroup,0)
    FROM 
		Item I 
	WHERE 
		numItemCode=@numItemCode   	

	DECLARE @strSql1 AS NVARCHAR(MAX)
	DECLARE @strSql2 AS NVARCHAR(MAX)
	DECLARE @strSql3 AS NVARCHAR(MAX)
	DECLARE @strSql4 AS NVARCHAR(MAX)
	
	SET @strSql1 = CONCAT('SELECT 
								I.numItemCode
								,vcItemName
								,txtItemDesc
								,charItemType
								,ISNULL(I.bitKitParent,0) bitKitParent
								,ISNULL(bitCalAmtBasedonDepItems,0) bitCalAmtBasedonDepItems
								,ISNULL(I.bitMatrix,0) bitMatrix,(CASE WHEN (ISNULL(I.bitKitParent,0) = 1 OR ISNULL(I.bitAssembly,0) = 1) AND ISNULL(bitCalAmtBasedonDepItems,0) = 1 THEN dbo.GetCalculatedPriceForKitAssembly(I.numDomainID,',@numDivisionID,',I.numItemCode,1,0,ISNULL(I.tintKitAssemblyPriceBasedOn,1),0,0,'''',0,1) ELSE 
								',(CASE 
									WHEN @tintPreLoginPriceLevel > 0 
									THEN CONCAT('ISNULL((CASE 
											WHEN PricingOption.tintRuleType = 1 AND PricingOption.tintDiscountType = 1
											THEN ISNULL((CASE WHEN I.[charItemType] = ''P'' THEN (',@UOMConversionFactor,' * ISNULL(W.[monWListPrice],0)) ELSE (',@UOMConversionFactor,' * monListPrice) END),0) - (ISNULL((CASE WHEN I.[charItemType] = ''P'' THEN (',@UOMConversionFactor,' * ISNULL(W.[monWListPrice],0)) ELSE (',@UOMConversionFactor,' * monListPrice) END),0) * (PricingOption.decDiscount / 100 ) )
											WHEN PricingOption.tintRuleType = 1 AND PricingOption.tintDiscountType = 2
											THEN ISNULL((CASE WHEN I.[charItemType] = ''P'' THEN (',@UOMConversionFactor,' * ISNULL(W.[monWListPrice],0)) ELSE (',@UOMConversionFactor,' * monListPrice) END),0) - PricingOption.decDiscount
											WHEN PricingOption.tintRuleType = 2 AND PricingOption.tintDiscountType = 1
											THEN ISNULL(Vendor.monCost,0) + (ISNULL(Vendor.monCost,0) * (PricingOption.decDiscount / 100 ) )
											WHEN PricingOption.tintRuleType = 2 AND PricingOption.tintDiscountType = 2
											THEN ISNULL(Vendor.monCost,0) + PricingOption.decDiscount
											WHEN PricingOption.tintRuleType = 3
											THEN (',@UOMConversionFactor,' * PricingOption.decDiscount)
										END),ISNULL(CASE WHEN I.[charItemType]=''P'' THEN ',@UOMConversionFactor,' * W.[monWListPrice] ELSE ',@UOMConversionFactor,' * monListPrice END,0))') 
									ELSE CONCAT('ISNULL(CASE WHEN I.[charItemType]=''P'' THEN ',@UOMConversionFactor,' * W.[monWListPrice] ELSE ',@UOMConversionFactor,' * monListPrice END,0)')
								END),'END) AS monListPrice
								,(CASE WHEN (ISNULL(I.bitKitParent,0) = 1 OR ISNULL(I.bitAssembly,0) = 1) AND ISNULL(bitCalAmtBasedonDepItems,0) = 1 THEN dbo.GetCalculatedPriceForKitAssembly(I.numDomainID,',@numDivisionID,',I.numItemCode,1,0,ISNULL(I.tintKitAssemblyPriceBasedOn,1),0,0,'''',0,1) ELSE ISNULL(CASE WHEN I.[charItemType]=''P'' 
											THEN ',@UOMConversionFactor,' * W.[monWListPrice]  
											ELSE ',@UOMConversionFactor,' * monListPrice 
										END,0) END) AS monMSRP
										,numItemClassification
										,bitTaxable
										,vcSKU
										,I.numModifiedBy
										,(SELECT numItemImageId,vcPathForImage,vcPathForTImage,bitDefault,case when bitdefault=1  then -1 else isnull(intDisplayOrder,0) end as intDisplayOrder FROM ItemImages  WHERE numItemCode=I.numItemCode and bitIsImage = 1 order by case when bitdefault=1 then -1 else isnull(intDisplayOrder,0) end  asc FOR XML AUTO,ROOT(''Images''))  vcImages
										,(SELECT vcPathForImage FROM ItemImages  WHERE numItemCode=I.numItemCode and bitIsImage = 0 order by intDisplayOrder  asc FOR XML AUTO,ROOT(''Documents''))  vcDocuments
										,ISNULL(bitSerialized,0) as bitSerialized
										,vcModelID
										,numItemGroup
										,(ISNULL(sum(numOnHand),0) / ',@UOMConversionFactor,') as numOnHand
										,sum(numOnOrder) as numOnOrder,                    
										sum(numReorder)  as numReorder,                    
										sum(numAllocation)  as numAllocation,                    
										sum(numBackOrder)  as numBackOrder,              
										isnull(fltWeight,0) as fltWeight,              
										isnull(fltHeight,0) as fltHeight,              
										isnull(fltWidth,0) as fltWidth,              
										isnull(fltLength,0) as fltLength,              
										case when charItemType=''P''then isnull(bitFreeShipping,0) else 1 end as bitFreeShipping,
										Case When (case when charItemType=''P''then isnull(bitFreeShipping,0) else 1 end )=1 then ''Free shipping'' else ''Calculated at checkout'' end as Shipping,              
										isnull(bitAllowBackOrder,0) as bitAllowBackOrder,bitShowInStock,bitShowQOnHand,isnull(numWareHouseItemID,0)  as numWareHouseItemID,
										(CASE WHEN charItemType<>''S'' AND charItemType<>''N'' AND ISNULL(bitKitParent,0)=0 THEN         
										 (CASE 
											WHEN I.bitAllowBackOrder = 1 THEN 1
											WHEN (ISNULL(WAll.numTotalOnHand,0)<=0) THEN 0
											ELSE 1
										END) ELSE 1 END ) as bitInStock,
										(
											CASE WHEN ISNULL(bitKitParent,0) = 1
											THEN
												(CASE 
													WHEN ((SELECT COUNT(*) FROM ItemDetails ID INNER JOIN Item IInner ON ID.numChildItemID = IInner.numItemCode WHERE ID.numItemKitID = I.numItemCode AND ISNULL(IInner.bitKitParent,0) = 1)) > 0 
													THEN 1 
													ELSE 0 
												END)
											ELSE
												0
											END
										) bitHasChildKits,
										(case when charItemType<>''S'' AND charItemType<>''N'' AND ISNULL(bitKitParent,0)=0 then         
										 (Case when ',@bitShowInStock,'=1  then 
										 (CASE 
											WHEN I.bitAllowBackOrder = 1 AND I.numDomainID <> 172 AND ISNULL(WAll.numTotalOnHand,0)<=0 AND ISNULL(I.numVendorID,0) > 0 AND dbo.GetVendorPreferredMethodLeadTime(I.numVendorID,0) > 0 THEN CONCAT(''<font class="vendorLeadTime" style="color:green">Usually ships in '',dbo.GetVendorPreferredMethodLeadTime(I.numVendorID,0),'' days</font>'')
											WHEN I.bitAllowBackOrder = 1 AND ISNULL(WAll.numTotalOnHand,0)<=0 THEN (CASE WHEN I.numDomainID = 172 THEN ''<font style="color:green">AVAILABLE</font>'' ELSE ''<font style="color:green">In Stock</font>'' END)
											WHEN (ISNULL(WAll.numTotalOnHand,0)<=0) THEN (CASE WHEN I.numDomainID = 172 THEN ''<font style="color:red">ON HOLD</font>'' ELSE ''<font style="color:red">Out Of Stock</font>'' END)
											ELSE (CASE WHEN I.numDomainID = 172 THEN ''<font style="color:green">AVAILABLE</font>'' ELSE ''<font style="color:green">In Stock</font>'' END)  
										END) else '''' end) ELSE '''' END ) as InStock,
										ISNULL(numSaleUnit,0) AS numUOM,
										ISNULL(vcUnitName,'''') AS vcUOMName,
										 ',@UOMConversionFactor,' AS UOMConversionFactor,
										I.numCreatedBy,
										I.numBarCodeId,
										I.[vcManufacturer],
										(SELECT  COUNT(*) FROM  Review where vcReferenceId = CONVERT(VARCHAR(50) ,I.numItemCode) and numDomainId = I.numDomainId and numSiteId = ', @numSiteId,' AND bitHide = 0 ) as ReviewCount ,
										(SELECT  COUNT(*) FROM  Ratings where vcReferenceId = CONVERT(VARCHAR(50) ,I.numItemCode) and numDomainId = I.numDomainId and numSiteId = ', @numSiteId,'  ) as RatingCount ,
										MT.vcPageTitle,
										MT.vcMetaKeywords,
										MT.vcMetaDescription,
										(SELECT vcCategoryName  FROM  dbo.Category WHERE numCategoryID =(SELECT TOP 1 numCategoryID FROM dbo.ItemCategory WHERE numItemID = I.numItemCode)) as vcCategoryName
										,ISNULL(TablePromotion.numTotalPromotions,0) numTotalPromotions
										,ISNULL(TablePromotion.vcPromoDesc,'''') vcPromoDesc
										,ISNULL(TablePromotion.bitRequireCouponCode,0) bitRequireCouponCode')
	SET @strSql2 = CONCAT(' from Item I 
										LEFT JOIN Vendor ON Vendor.numVendorID =I.numVendorID AND Vendor.numItemCode=I.numItemCode
										LEFT JOIN MetaTags MT ON MT.numReferenceID = I.numItemCode AND MT.tintMetaTagFor =2
										OUTER APPLY (SELECT * FROM dbo.fn_GetItemPromotions(',@numDomainID,',',@numDivisionID,',I.numItemCode,I.numItemClassification',',',@numDefaultRelationship,',',@numDefaultProfile,',2)) TablePromotion ' + 
										(CASE WHEN @tintPreLoginPriceLevel > 0 THEN CONCAT(' OUTER APPLY(SELECT tintRuleType,tintDiscountType,decDiscount FROM PricingTable WHERE numItemCode=I.numItemCode AND ISNULL(numPriceRuleID,0) = 0 AND ISNULL(numCurrencyID,0) = 0 ORDER BY numPricingID OFFSET ',@tintPreLoginPriceLevel-1,' ROWS FETCH NEXT 1 ROWS ONLY) As PricingOption ') ELSE '' END)
										,'                 
										left join  WareHouseItems W                
										on W.numItemID=I.numItemCode and numWareHouseID= ',@numWareHouseID,'
										 OUTER APPLY (SELECT SUM(numOnHand) numTotalOnHand FROM WareHouseItems W 
																							  WHERE  W.numItemID = I.numItemCode
																							  AND numWareHouseID IN (SELECT [OutParam] FROM dbo.[SplitString](''',@vcWarehouseIDs,''','',''))
																							  AND W.numDomainID = ',@numDomainID,') AS WAll
										left join  eCommerceDTL E          
										on E.numDomainID=I.numDomainID     
										LEFT JOIN UOM u ON u.numUOMId=I.numSaleUnit')

	SET @strSql3 = CONCAT(' where ISNULL(I.IsArchieve,0)=0 AND I.numDomainID=',@numDomainID,' AND I.numItemCode=',@numItemCode,
										CASE WHEN (SELECT COUNT(*) FROM [dbo].[EcommerceRelationshipProfile] WHERE numSiteID=@numSiteID AND numRelationship = @numDefaultRelationship AND numProfile = @numDefaultProfile) > 0
										THEN
											CONCAT(' AND I.numItemClassification NOT IN (SELECT numItemClassification FROM EcommerceRelationshipProfile ERP INNER JOIN ECommerceItemClassification EIC ON EIC.numECommRelatiionshipProfileID=ERP.ID WHERE ERP.numSiteID=',@numSiteID,' AND ERP.numRelationship=',@numDefaultRelationship,' AND ERP.numProfile=', @numDefaultProfile,')')
										ELSE
											''
										END
										,' GROUP BY I.numItemCode, vcItemName, txtItemDesc, charItemType, monListPrice,numItemClassification, bitTaxable, vcSKU, bitKitParent, bitCalAmtBasedonDepItems, bitAssembly, tintKitAssemblyPriceBasedOn, WAll.numTotalOnHand,         
										I.numDomainID,I.numModifiedBy,  bitSerialized, vcModelID,numItemGroup, fltWeight,fltHeight,fltWidth,MT.vcPageTitle,MT.vcMetaKeywords,MT.vcMetaDescription,          
										fltLength,bitFreeShipping,bitAllowBackOrder,bitShowInStock,bitShowQOnHand ,numWareHouseItemID,vcUnitName,I.numCreatedBy,I.numBarCodeId,W.[monWListPrice],I.[vcManufacturer],
										numSaleUnit, I.numVendorID, I.bitMatrix,numTotalPromotions,vcPromoDesc,bitRequireCouponCode,Vendor.monCost ',(CASE WHEN @tintPreLoginPriceLevel > 0 THEN ',PricingOption.tintRuleType,PricingOption.tintDiscountType,PricingOption.decDiscount' ELSE '' END))

	SET @strSql4 = ' select numItemCode,vcItemName,txtItemDesc,charItemType, bitMatrix, monListPrice,monMSRP,numItemClassification, bitTaxable, vcSKU, bitKitParent, bitCalAmtBasedonDepItems,                  
										tblItem.numModifiedBy, vcImages,vcDocuments, bitSerialized, vcModelID, numItemGroup,numOnHand,numOnOrder,numReorder,numAllocation, 
										numBackOrder, fltWeight, fltHeight, fltWidth, fltLength, bitFreeShipping,bitAllowBackOrder,bitShowInStock,
										bitShowQOnHand,numWareHouseItemID,bitInStock,bitHasChildKits,InStock,numUOM,vcUOMName,UOMConversionFactor,tblItem.numCreatedBy,numBarCodeId,vcManufacturer,ReviewCount,RatingCount,Shipping,ISNULL(vcPageTitle,'''') vcPageTitle,isNull(vcMetaKeywords,'''') as vcMetaKeywords ,isNull(vcMetaDescription,'''') as vcMetaDescription,isNull(vcCategoryName,'''') as vcCategoryName,numTotalPromotions,vcPromoDesc,bitRequireCouponCode from tblItem ORDER BY numOnHand DESC'


	PRINT CAST(CONCAT('WITH tblItem AS (',@strSql1,@strSql2,@strSql3,')',@strSql4) AS NTEXT)

	exec ('WITH tblItem AS (' + @strSql1 + @strSql2 + @strSql3 + ')' + @strSql4)


	declare @tintOrder as tinyint                                                  
	declare @vcFormFieldName as varchar(50)                                                  
	declare @vcListItemType as varchar(1)                                             
	declare @vcAssociatedControlType varchar(10)                                                  
	declare @numListID AS numeric(9)                                                  
	declare @WhereCondition varchar(2000)                       
	Declare @numFormFieldId as numeric  
	DECLARE @vcFieldType CHAR(1)
	DECLARE @GRP_ID INT
                  
	set @tintOrder=0                                                  
	set @WhereCondition =''                 
                   
              
	CREATE TABLE #tempAvailableFields
	(
		numFormFieldId NUMERIC(9)
		,vcFormFieldName NVARCHAR(50)
		,vcFieldType CHAR(1)
		,vcAssociatedControlType NVARCHAR(50)
		,numListID NUMERIC(18)
		,vcListItemType CHAR(1)
		,intRowNum INT
		,vcItemValue VARCHAR(3000)
		,GRP_ID INT
	)
   
	INSERT INTO #tempAvailableFields 
	(
		numFormFieldId
		,vcFormFieldName
		,vcFieldType
		,vcAssociatedControlType
        ,numListID
		,vcListItemType
		,intRowNum
		,GRP_ID
	)                         
    SELECT 
		Fld_id
		,REPLACE(Fld_label,' ','') AS vcFormFieldName
		,CASE GRP_ID WHEN 4 then 'C' WHEN 1 THEN 'D' ELSE 'C' END AS vcFieldType
		,fld_type as vcAssociatedControlType
		,ISNULL(C.numListID, 0) numListID
		,CASE WHEN C.numListID > 0 THEN 'L' ELSE '' END vcListItemType
		,ROW_NUMBER() OVER (ORDER BY Fld_id asc) AS intRowNum
		,GRP_ID
    FROM 
		CFW_Fld_Master C 
	LEFT JOIN 
		CFW_Validation V 
	ON 
		V.numFieldID = C.Fld_id
    WHERE 
		C.numDomainID = @numDomainId
        AND GRP_ID  = 5

    SELECT TOP 1 
		@tintOrder=intRowNum
		,@numFormFieldId=numFormFieldId
		,@vcFormFieldName=vcFormFieldName
		,@vcFieldType=vcFieldType
		,@vcAssociatedControlType=vcAssociatedControlType
		,@numListID=numListID
		,@vcListItemType=vcListItemType
		,@GRP_ID=GRP_ID
	FROM 
		#tempAvailableFields 
	ORDER BY 
		intRowNum ASC
   
	while @tintOrder>0                                                  
	begin                   
		IF @GRP_ID=5
		BEGIN
			IF @vcAssociatedControlType = 'TextBox' or @vcAssociatedControlType = 'TextArea'
			BEGIN  
				UPDATE #tempAvailableFields SET vcItemValue=(SELECT Fld_Value FROM CFW_FLD_Values_Item WHERE Fld_Id=@numFormFieldId AND RecId=@numItemCode) WHERE numFormFieldId=@numFormFieldId              
			END   
			ELSE IF @vcAssociatedControlType = 'CheckBox'
			BEGIN      
				UPDATE #tempAvailableFields SET vcItemValue=(SELECT case when isnull(Fld_Value,0)=0 then 'No' when isnull(Fld_Value,0)=1 then 'Yes' END FROM CFW_FLD_Values_Item WHERE Fld_Id=@numFormFieldId AND RecId=@numItemCode) WHERE numFormFieldId=@numFormFieldId              
			END                
			ELSE IF @vcAssociatedControlType = 'DateField'           
			BEGIN   
				UPDATE #tempAvailableFields SET vcItemValue=(SELECT dbo.FormatedDateFromDate(Fld_Value,@numDomainId) FROM CFW_FLD_Values_Item WHERE Fld_Id=@numFormFieldId AND RecId=@numItemCode) WHERE numFormFieldId=@numFormFieldId              
			END                
			ELSE IF @vcAssociatedControlType = 'SelectBox'           
			BEGIN 
				UPDATE #tempAvailableFields SET vcItemValue=(SELECT L.vcData FROM CFW_FLD_Values_Item CFW left Join ListDetails L
					on L.numListItemID=CFW.Fld_Value                
					WHERE CFW.Fld_Id=@numFormFieldId AND CFW.RecId=@numItemCode) WHERE numFormFieldId=@numFormFieldId              
			END   
		END    
 
		SELECT TOP 1 
			@tintOrder=intRowNum
			,@numFormFieldId=numFormFieldId
			,@vcFormFieldName=vcFormFieldName
			,@vcFieldType=vcFieldType
			,@vcAssociatedControlType=vcAssociatedControlType
			,@numListID=numListID
			,@vcListItemType=vcListItemType
			,@GRP_ID=GRP_ID
		FROM 
			#tempAvailableFields 
		WHERE 
			intRowNum > @tintOrder 
		ORDER BY 
			intRowNum ASC
 
	   IF @@rowcount=0 set @tintOrder=0                                                  
	END   


	INSERT INTO #tempAvailableFields 
	(
		numFormFieldId
		,vcFormFieldName
		,vcFieldType
		,vcAssociatedControlType
		,numListID
		,vcListItemType
		,intRowNum
		,vcItemValue
		,GRP_ID
	)                         
	SELECT 
		Fld_id
		,REPLACE(Fld_label,' ','') AS vcFormFieldName
		,CASE GRP_ID WHEN 4 then 'C' WHEN 1 THEN 'D' ELSE 'C' END AS vcFieldType
		,fld_type as vcAssociatedControlType
		,ISNULL(C.numListID, 0) numListID
		,CASE WHEN C.numListID > 0 THEN 'L' ELSE '' END vcListItemType
		,ROW_NUMBER() OVER (ORDER BY Fld_id asc) AS intRowNum
		,''
		,GRP_ID
	FROM 
		CFW_Fld_Master C 
	LEFT JOIN 
		CFW_Validation V 
	ON 
		V.numFieldID = C.Fld_id
	WHERE 
		C.numDomainID = @numDomainId
		AND GRP_ID = 9

	IF ISNULL(@bitMatrix,0) = 1 AND ISNULL(@numItemGroup,0) > 0
	BEGIN
		UPDATE
			TEMP
		SET
			TEMP.vcItemValue = FLD_ValueName
		FROM
			#tempAvailableFields TEMP
		INNER JOIN
		(
			SELECT 
				CFW_Fld_Master.fld_id
				,CASE 
					WHEN CFW_Fld_Master.fld_type='SelectBox' THEN dbo.GetListIemName(Fld_Value)
					WHEN CFW_Fld_Master.fld_type='CheckBox' THEN CASE WHEN isnull(Fld_Value,0)=1 THEN 'Yes' ELSE 'No' END
					ELSE CAST(Fld_Value AS VARCHAR)
				END AS FLD_ValueName
			FROM 
				CFW_Fld_Master 
			INNER JOIN
				ItemGroupsDTL 
			ON 
				CFW_Fld_Master.Fld_id = ItemGroupsDTL.numOppAccAttrID
				AND ItemGroupsDTL.tintType = 2
			LEFT JOIN
				ItemAttributes
			ON
				CFW_Fld_Master.Fld_id = ItemAttributes.FLD_ID
				AND ItemAttributes.numItemCode = @numItemCode
			LEFT JOIN
				ListDetails LD
			ON
				CFW_Fld_Master.numlistid = LD.numListID
			WHERE
				CFW_Fld_Master.numDomainID = @numDomainId
				AND ItemGroupsDTL.numItemGroupID = @numItemGroup
			GROUP BY
				CFW_Fld_Master.Fld_label
				,CFW_Fld_Master.fld_id
				,CFW_Fld_Master.fld_type
				,CFW_Fld_Master.bitAutocomplete
				,CFW_Fld_Master.numlistid
				,CFW_Fld_Master.vcURL
				,ItemAttributes.FLD_Value 
		) T1
		ON
			TEMP.numFormFieldId = T1.fld_id
		WHERE
			TEMP.GRP_ID=9
	END
  
	SELECT * FROM #tempAvailableFields

	DROP TABLE #tempAvailableFields
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_LandedCostExpenseAccountSave' )
    DROP PROCEDURE [USP_LandedCostExpenseAccountSave]
GO
CREATE PROC [dbo].[USP_LandedCostExpenseAccountSave]
    @numDomainId NUMERIC(18, 0) ,
    @strItems VARCHAR(MAX),
	@vcLanedCostDefault VARCHAR(MAX)
AS
    BEGIN TRY
        BEGIN TRANSACTION 
		UPDATE Domain SET vcLanedCostDefault=@vcLanedCostDefault WHERE 
		numDomainId=@numDomainID
        DECLARE @hDocItem INT
        IF CONVERT(VARCHAR(MAX), @strItems) <> ''
            BEGIN
                EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems

                SELECT  *
                INTO    #temp
                FROM    OPENXML (@hDocItem, '/NewDataSet/Table1', 2)
                         WITH (numLCExpenseId NUMERIC(18,0), vcDescription VARCHAR(50), numAccountId NUMERIC(18, 0)) X
     
                DELETE  FROM [dbo].[LandedCostExpenseAccount]
                WHERE   [numDomainId] = @numDomainId
                        AND numLCExpenseId NOT IN ( SELECT  numLCExpenseId
                                                    FROM    #temp
                                                    WHERE   numLCExpenseId > 0 )
	       
                UPDATE  LCEA
                SET     [vcDescription] = T.vcDescription ,
                        [numAccountId] = T.numAccountId
                FROM    [LandedCostExpenseAccount] LCEA
                        JOIN #temp T ON LCEA.numLCExpenseId = T.numLCExpenseId
                WHERE   T.numLCExpenseId > 0
                        AND LCEA.[numDomainId] = @numDomainId
			                   
                INSERT  INTO [dbo].[LandedCostExpenseAccount]
                        ( [numDomainId] ,
                          [vcDescription] ,
                          [numAccountId]
                        )
                        SELECT  @numDomainId ,
                                vcDescription ,
                                numAccountId
                        FROM    #temp
                        WHERE   numLCExpenseId = 0

                EXEC sp_xml_removedocument @hDocItem
            END

        COMMIT TRANSACTION

    END TRY
    BEGIN CATCH
        DECLARE @strMsg VARCHAR(200)
        SET @strMsg = ERROR_MESSAGE()
        IF ( @@TRANCOUNT > 0 )
            BEGIN
                RAISERROR ( @strMsg, 16, 1 );
                ROLLBACK TRAN
                RETURN 1
            END
    END CATCH
/****** Object:  StoredProcedure [dbo].[USP_ManageCategory]    Script Date: 07/26/2008 16:19:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj        
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_managecategory')
DROP PROCEDURE usp_managecategory
GO
CREATE PROCEDURE [dbo].[USP_ManageCategory]        
@numCatergoryId as numeric(9),        
@vcCatgoryName as varchar(1000),       
@numDomainID as numeric(9)=0,
@vcDescription as VARCHAR(MAX),
@intDisplayOrder AS INT ,
@vcPathForCategoryImage AS VARCHAR(100),
@numDepCategory AS NUMERIC(9,0),
@numCategoryProfileID AS NUMERIC(18,0),
@vcCatgoryNameURL VARCHAR(1000),
@vcMetaTitle as varchar(1000),
@vcMetaKeywords as varchar(1000),
@vcMetaDescription as varchar(1000)
as 

	DECLARE @tintLevel AS TINYINT
	SET @tintLevel = 1

	IF	@numDepCategory <> 0
	BEGIN
		SELECT @tintLevel = (tintLevel + 1) FROM dbo.Category WHERE numCategoryID = @numDepCategory
	END       
       
	IF @numCatergoryId=0        
	BEGIN        
		INSERT INTO Category 
		(
			vcCategoryName,
			tintLevel,
			numDomainID,
			vcDescription,
			intDisplayOrder,
			vcPathForCategoryImage,
			numDepCategory,
			numCategoryProfileID,
			vcCategoryNameURL,
			vcMetaTitle,
			vcMetaKeywords,
			vcMetaDescription
		)        
		VALUES
		(
			@vcCatgoryName,
			@tintLevel,
			@numDomainID,
			@vcDescription,
			@intDisplayOrder,
			@vcPathForCategoryImage,
			@numDepCategory,
			@numCategoryProfileID,
			@vcCatgoryNameURL,
			@vcMetaTitle,
			@vcMetaKeywords,
			@vcMetaDescription
		) 
		set @numCatergoryId = SCOPE_IDENTITY();   
		insert into SiteCategories 
		(
			numSiteID,
			numCategoryID
		)
		SELECT
			numSiteID
			,@numCatergoryId
		FROM
			CategoryProfileSites
		WHERE
			 numCategoryProfileID=@numCategoryProfileID    
	END        
	ELSE IF @numCatergoryId>0        
	BEGIN        
		UPDATE 
			Category 
		SET 
			vcCategoryName=@vcCatgoryName,
			vcDescription=@vcDescription,
			intDisplayOrder = @intDisplayOrder,
			tintLevel = @tintLevel,
			vcPathForCategoryImage = @vcPathForCategoryImage,
			numDepCategory = @numDepCategory,
			vcCategoryNameURL=@vcCatgoryNameURL,
			vcMetaTitle=@vcMetaTitle,
			vcMetaKeywords=@vcMetaKeywords,
			vcMetaDescription=@vcMetaDescription
		WHERE 
			numCategoryID=@numCatergoryId        
	END
	
GO
GO
/****** Object:  StoredProcedure [dbo].[USP_ManageItemsAndKits]    Script Date: 02/15/2010 21:44:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Anoop Jayaraj                                                          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_manageitemsandkits')
DROP PROCEDURE usp_manageitemsandkits
GO
CREATE PROCEDURE [dbo].[USP_ManageItemsAndKits]                                                                
@numItemCode as numeric(9) output,                                                                
@vcItemName as varchar(300),                                                                
@txtItemDesc as varchar(2000),                                                                
@charItemType as char(1),                                                                
@monListPrice as DECIMAL(20,5),                                                                
@numItemClassification as numeric(9),                                                                
@bitTaxable as bit,                                                                
@vcSKU as varchar(50),                                                                                                                                      
@bitKitParent as bit,                                                                                   
--@dtDateEntered as datetime,                                                                
@numDomainID as numeric(9),                                                                
@numUserCntID as numeric(9),                                                                                                  
@numVendorID as numeric(9),                                                            
@bitSerialized as bit,                                               
@strFieldList as text,                                            
@vcModelID as varchar(200)='' ,                                            
@numItemGroup as numeric(9)=0,                                      
@numCOGSChartAcntId as numeric(9)=0,                                      
@numAssetChartAcntId as numeric(9)=0,                                      
@numIncomeChartAcntId as numeric(9)=0,                            
@monAverageCost as DECIMAL(20,5),                          
@monLabourCost as DECIMAL(20,5),                  
@fltWeight as float,                  
@fltHeight as float,                  
@fltLength as float,                  
@fltWidth as float,                  
@bitFreeshipping as bit,                
@bitAllowBackOrder as bit,              
@UnitofMeasure as varchar(30)='',              
@strChildItems as text,            
@bitShowDeptItem as bit,              
@bitShowDeptItemDesc as bit,        
@bitCalAmtBasedonDepItems as bit,    
@bitAssembly as BIT,
@intWebApiId INT=null,
@vcApiItemId AS VARCHAR(50)=null,
@numBarCodeId as VARCHAR(25)=null,
@vcManufacturer as varchar(250)=NULL,
@numBaseUnit AS NUMERIC(18)=0,
@numPurchaseUnit AS NUMERIC(18)=0,
@numSaleUnit AS NUMERIC(18)=0,
@bitLotNo as BIT,
@IsArchieve AS BIT,
@numItemClass as numeric(9)=0,
@tintStandardProductIDType AS TINYINT=0,
@vcExportToAPI VARCHAR(50),
@numShipClass NUMERIC,
@ProcedureCallFlag SMALLINT =0,
@vcCategories AS VARCHAR(2000) = '',
@bitAllowDropShip AS BIT=0,
@bitArchiveItem AS BIT = 0,
@bitAsset AS BIT = 0,
@bitRental AS BIT = 0,
@numCategoryProfileID AS NUMERIC(18,0) = 0,
@bitVirtualInventory BIT,
@bitContainer BIT,
@numContainer NUMERIC(18,0)=0,
@numNoItemIntoContainer NUMERIC(18,0),
@bitMatrix BIT = 0 ,
@vcItemAttributes VARCHAR(2000) = '',
@numManufacturer NUMERIC(18,0),
@vcASIN as varchar(50),
@tintKitAssemblyPriceBasedOn TINYINT = 1,
@fltReorderQty FLOAT = 0,
@bitSOWorkOrder BIT = 0,
@bitKitSingleSelect BIT = 0,
@bitExpenseItem BIT = 0,
@numExpenseChartAcntId NUMERIC(18,0) = 0,
@numBusinessProcessId NUMERIC(18,0)=0,
@bitTimeContractFromSalesOrder BIT =0 
AS
BEGIN     
	SET @vcManufacturer = CASE WHEN ISNULL(@numManufacturer,0) > 0 THEN ISNULL((SELECT vcCompanyName FROM DivisionMaster INNER JOIN CompanyInfo ON DivisionMaster.numCompanyID=CompanyInfo.numCompanyId WHERE numDivisionID=@numManufacturer),'')  ELSE ISNULL(@vcManufacturer,'') END

	DECLARE @bitAttributesChanged AS BIT  = 1
	DECLARE @bitOppOrderExists AS BIT = 0

	IF ISNULL(@numItemCode,0) > 0
	BEGIN
		IF (SELECT COUNT(OI.numOppId) FROM dbo.OpportunityItems OI WHERE OI.numItemCode =@numItemCode) > 0
		BEGIN
			SET @bitOppOrderExists = 1
		END
		ELSE IF (SELECT COUNT(OI.numOppId) FROM dbo.OpportunityKitItems OI WHERE OI.numChildItemID =@numItemCode) > 0
		BEGIN
			SET @bitOppOrderExists = 1
		END
		ELSE IF (SELECT COUNT(OI.numOppId) FROM dbo.OpportunityKitChildItems OI WHERE OI.numItemID =@numItemCode) > 0
		BEGIN
			SET @bitOppOrderExists = 1
		END
	END
	ELSE 
	BEGIN
		IF (ISNULL(@vcItemName,'') = '')
		BEGIN
			RAISERROR('ITEM_NAME_NOT_SELECTED',16,1)
			RETURN
		END
        ELSE IF (ISNULL(@charItemType,'') = '0')
		BEGIN
			RAISERROR('ITEM_TYPE_NOT_SELECTED',16,1)
			RETURN
		END
	END

	If (@charItemType = 'P' AND ISNULL(@bitKitParent,0) = 0)
	BEGIN
		IF ((SELECT COUNT(*) FROM Chart_Of_Accounts WHERE numDomainId=@numDomainID AND numAccountId=@numAssetChartAcntId) = 0)
		BEGIN
			RAISERROR('INVALID_ASSET_ACCOUNT',16,1)
			RETURN
		END
	END

	If @charItemType = 'P' OR ISNULL((SELECT bitExpenseNonInventoryItem FROM Domain WHERE numDomainID=@numDomainID),0) = 1
	BEGIN
		-- This is common check for CharItemType P and Other
		IF ((SELECT COUNT(*) FROM Chart_Of_Accounts WHERE numDomainId=@numDomainID AND numAccountId=@numCOGSChartAcntId) = 0)
		BEGIN
			RAISERROR('INVALID_COGS_ACCOUNT',16,1)
			RETURN
		END
	END

	IF ((SELECT COUNT(*) FROM Chart_Of_Accounts WHERE numDomainId=@numDomainID AND numAccountId=@numIncomeChartAcntId) = 0)
	BEGIN
		RAISERROR('INVALID_INCOME_ACCOUNT',16,1)
		RETURN
	END

	IF ISNULL(@bitExpenseItem,0) = 1
	BEGIN
		IF ((SELECT COUNT(*) FROM Chart_Of_Accounts WHERE numDomainId=@numDomainID AND numAccountId=@numExpenseChartAcntId) = 0)
		BEGIN
			RAISERROR('INVALID_EXPENSE_ACCOUNT',16,1)
			RETURN
		END
	END

	--INSERT CUSTOM FIELDS BASE ON WAREHOUSEITEMS
	DECLARE @hDoc AS INT     
	                                                                        	
	DECLARE @TempTable TABLE 
	(
		ID INT IDENTITY(1,1),
		Fld_ID NUMERIC(18,0),
		Fld_Value NUMERIC(18,0)
	)   

	IF ISNULL(@bitMatrix,0) = 1 AND ISNULL(@numItemGroup,0) = 0
	BEGIN
		RAISERROR('ITEM_GROUP_NOT_SELECTED',16,1)
		RETURN
	END
	ELSE IF ISNULL(@bitMatrix,0) = 1 AND LEN(ISNULL(@vcItemAttributes,'')) = 0 
	BEGIN
		RAISERROR('ITEM_ATTRIBUTES_NOT_SELECTED',16,1)
		RETURN
	END
	ELSE IF ISNULL(@bitMatrix,0) = 1 
	BEGIN
		DECLARE @bitDuplicate AS BIT = 0
	
		IF DATALENGTH(ISNULL(@vcItemAttributes,''))>2
		BEGIN
			SET @vcItemAttributes = CONCAT('<?xml version="1.0" encoding="iso-8859-1"?>',@vcItemAttributes)

			EXEC sp_xml_preparedocument @hDoc OUTPUT, @vcItemAttributes
	                                      
			INSERT INTO @TempTable 
			(
				Fld_ID,
				Fld_Value
			)    
			SELECT
				*          
			FROM 
				OPENXML (@hDoc,'/NewDataSet/CusFlds',2)                                                  
			WITH 
				(Fld_ID NUMERIC(18,0),Fld_Value NUMERIC(18,0)) 
			ORDER BY 
				Fld_ID       

			DECLARE @strAttribute VARCHAR(500) = ''
			DECLARE @i AS INT = 1
			DECLARE @COUNT AS INT 
			DECLARE @numFldID AS NUMERIC(18,0)
			DECLARE @numFldValue AS NUMERIC(18,0)
			SELECT @COUNT = COUNT(*) FROM @TempTable

			WHILE @i <= @COUNT
			BEGIN
				SELECT @numFldID=Fld_ID,@numFldValue=Fld_Value FROM @TempTable WHERE ID=@i

				IF ISNUMERIC(@numFldValue)=1
				BEGIN
					SELECT @strAttribute=@strAttribute+Fld_label FROM CFW_Fld_Master WHERE Fld_id=@numFldID AND Grp_id = 9
                
					IF LEN(@strAttribute) > 0
					BEGIN
						SELECT @strAttribute=@strAttribute+':'+ vcData +',' FROM ListDetails WHERE numListItemID=@numFldValue
					END
				END

				SET @i = @i + 1
			END

			SET @strAttribute = SUBSTRING(@strAttribute,0,LEN(@strAttribute))

			IF
				(SELECT
					COUNT(*)
				FROM
					Item
				INNER JOIN
					ItemAttributes
				ON
					Item.numItemCode = ItemAttributes.numItemCode
					AND ItemAttributes.numDomainID = @numDomainID                     
				WHERE
					Item.numItemCode <> @numItemCode
					AND Item.vcItemName = @vcItemName
					AND Item.numItemGroup = @numItemGroup
					AND Item.numDomainID = @numDomainID
					AND dbo.fn_GetItemAttributes(@numDomainID,Item.numItemCode) = @strAttribute
				) > 0
			BEGIN
				RAISERROR('ITEM_WITH_ATTRIBUTES_ALREADY_EXISTS',16,1)
				RETURN
			END

			If ISNULL(@numItemCode,0) > 0 AND dbo.fn_GetItemAttributes(@numDomainID,@numItemCode) = @strAttribute
			BEGIN
				SET @bitAttributesChanged = 0
			END

			EXEC sp_xml_removedocument @hDoc 
		END	
	END
	ELSE IF ISNULL(@bitMatrix,0) = 0
	BEGIN
		-- REMOVE ATTRIBUTES CONFIGURATION OF ITEM AND WAREHOUSE
		DELETE FROM ItemAttributes WHERE numDomainID=@numDomainID AND numItemCode=@numItemCode
	END

	IF ISNULL(@numItemGroup,0) > 0 AND ISNULL(@bitMatrix,0) = 1
	BEGIN
		IF (SELECT COUNT(*) FROM ItemGroupsDTL WHERE numItemGroupID = @numItemGroup AND tintType=2) = 0
		BEGIN
			RAISERROR('ITEM_GROUP_NOT_CONFIGURED',16,1)
			RETURN
		END
		ELSE IF EXISTS (SELECT 
							CFM.Fld_id
						FROM 
							CFW_Fld_Master CFM
						LEFT JOIN
							ListDetails LD
						ON
							CFM.numlistid = LD.numListID
						WHERE
							CFM.numDomainID = @numDomainID
							AND CFM.Fld_id IN (SELECT numOppAccAttrID FROM ItemGroupsDTL WHERE numItemGroupID=@numItemGroup AND tintType = 2)
						GROUP BY
							CFM.Fld_id HAVING COUNT(LD.numListItemID) = 0)
		BEGIN
			RAISERROR('ITEM_GROUP_NOT_CONFIGURED',16,1)
			RETURN
		END
	END

BEGIN TRY
BEGIN TRANSACTION

	

	IF @numCOGSChartAcntId=0 SET @numCOGSChartAcntId=NULL
	IF @numAssetChartAcntId=0 SET @numAssetChartAcntId=NULL  
	IF @numIncomeChartAcntId=0 SET @numIncomeChartAcntId=NULL     
	IF @numExpenseChartAcntId=0 SET @numExpenseChartAcntId=NULL

	DECLARE @ParentSKU VARCHAR(50) = @vcSKU                        
	DECLARE @ItemID AS NUMERIC(9)
	DECLARE @cnt AS INT

	IF ISNULL(@charItemType,'') = '' OR ISNULL(@charItemType,'') = '0'  
		SET @charItemType = 'N'

	IF @intWebApiId = 2 AND LEN(ISNULL(@vcApiItemId, '')) > 0 AND ISNULL(@numItemGroup, 0) > 0 
    BEGIN 
        -- check wether this id already Mapped in ITemAPI 
        SELECT 
			@cnt = COUNT([numItemID])
        FROM 
			[ItemAPI]
        WHERE 
			[WebApiId] = @intWebApiId
            AND [numDomainId] = @numDomainID
            AND [vcAPIItemID] = @vcApiItemId

        IF @cnt > 0 
        BEGIN
            SELECT 
				@ItemID = [numItemID]
            FROM 
				[ItemAPI]
            WHERE 
				[WebApiId] = @intWebApiId
                AND [numDomainId] = @numDomainID
                AND [vcAPIItemID] = @vcApiItemId
        
			--Update Existing Product coming from API
            SET @numItemCode = @ItemID
        END
    END
	ELSE IF @intWebApiId > 1 AND LEN(ISNULL(@vcSKU, '')) > 0 
    BEGIN
        SET @ParentSKU = @vcSKU
		 
        SELECT 
			@cnt = COUNT([numItemCode])
        FROM 
			[Item]
        WHERE 
			[numDomainId] = @numDomainID
            AND (vcSKU = @vcSKU OR @vcSKU IN (SELECT vcWHSKU FROM dbo.WareHouseItems WHERE numItemID = Item.[numItemCode] AND vcWHSKU = @vcSKU))
            
		IF @cnt > 0 
        BEGIN
            SELECT 
				@ItemID = [numItemCode],
                @ParentSKU = vcSKU
            FROM 
				[Item]
            WHERE 
				[numDomainId] = @numDomainID
                AND (vcSKU = @vcSKU OR @vcSKU IN (SELECT vcWHSKU FROM dbo.WareHouseItems WHERE numItemID = Item.[numItemCode] AND vcWHSKU = @vcSKU))

            SET @numItemCode = @ItemID
        END
		ELSE 
		BEGIN
			-- check wether this id already Mapped in ITemAPI 
			SELECT 
				@cnt = COUNT([numItemID])
			FROM 
				[ItemAPI]
			WHERE 
				[WebApiId] = @intWebApiId
				AND [numDomainId] = @numDomainID
				AND [vcAPIItemID] = @vcApiItemId
                    
			IF @cnt > 0 
			BEGIN
				SELECT 
					@ItemID = [numItemID]
				FROM 
					[ItemAPI]
				WHERE 
					[WebApiId] = @intWebApiId
					AND [numDomainId] = @numDomainID
					AND [vcAPIItemID] = @vcApiItemId
        
				--Update Existing Product coming from API
				SET @numItemCode = @ItemID
			END
		END
    END
                                                         
	IF @numItemCode=0 OR @numItemCode IS NULL
	BEGIN
		INSERT INTO Item 
		(                                             
			vcItemName,txtItemDesc,charItemType,monListPrice,numItemClassification,bitTaxable,vcSKU,bitKitParent,
			numVendorID,numDomainID,numCreatedBy,bintCreatedDate,bintModifiedDate,numModifiedBy,bitSerialized,
			vcModelID,numItemGroup,numCOGsChartAcntId,numAssetChartAcntId,numIncomeChartAcntId,monAverageCost,                          
			monCampaignLabourCost,fltWeight,fltHeight,fltWidth,fltLength,bitFreeShipping,bitAllowBackOrder,vcUnitofMeasure,
			bitShowDeptItem,bitShowDeptItemDesc,bitCalAmtBasedonDepItems,bitAssembly,numBarCodeId,vcManufacturer,numBaseUnit,
			numPurchaseUnit,numSaleUnit,bitLotNo,IsArchieve,numItemClass,tintStandardProductIDType,vcExportToAPI,numShipClass,
			bitAllowDropShip,bitArchiveItem,bitAsset,bitRental,bitVirtualInventory,bitContainer,numContainer,numNoItemIntoContainer
			,bitMatrix,numManufacturer,vcASIN,tintKitAssemblyPriceBasedOn,fltReorderQty,bitSOWorkOrder,bitKitSingleSelect,bitExpenseItem,numExpenseChartAcntId,numBusinessProcessId,bitTimeContractFromSalesOrder 
		)                                                              
		VALUES                                                                
		(                                                                
			@vcItemName,@txtItemDesc,@charItemType,@monListPrice,@numItemClassification,@bitTaxable,@ParentSKU,@bitKitParent,
			@numVendorID,@numDomainID,@numUserCntID,getutcdate(),getutcdate(),@numUserCntID,@bitSerialized,@vcModelID,@numItemGroup,                                      
			@numCOGsChartAcntId,@numAssetChartAcntId,@numIncomeChartAcntId,@monAverageCost,@monLabourCost,@fltWeight,@fltHeight,@fltWidth,                  
			@fltLength,@bitFreeshipping,@bitAllowBackOrder,@UnitofMeasure,@bitShowDeptItem,@bitShowDeptItemDesc,@bitCalAmtBasedonDepItems,    
			@bitAssembly,@numBarCodeId,@vcManufacturer,@numBaseUnit,@numPurchaseUnit,@numSaleUnit,@bitLotNo,@IsArchieve,@numItemClass,
			@tintStandardProductIDType,@vcExportToAPI,@numShipClass,@bitAllowDropShip,@bitArchiveItem ,@bitAsset,@bitRental,
			@bitVirtualInventory,@bitContainer,@numContainer,@numNoItemIntoContainer
			,@bitMatrix,@numManufacturer,@vcASIN,@tintKitAssemblyPriceBasedOn,@fltReorderQty,@bitSOWorkOrder,@bitKitSingleSelect,@bitExpenseItem,@numExpenseChartAcntId,@numBusinessProcessId,@bitTimeContractFromSalesOrder 
		)                                             
 
		SET @numItemCode = SCOPE_IDENTITY()
  
		IF  @intWebApiId > 1 
		BEGIN
			 -- insert new product
			 --insert this id into linking table
			 INSERT INTO [ItemAPI] (
	 			[WebApiId],
	 			[numDomainId],
	 			[numItemID],
	 			[vcAPIItemID],
	 			[numCreatedby],
	 			[dtCreated],
	 			[numModifiedby],
	 			[dtModified],vcSKU
			 ) VALUES     
			 (
				@intWebApiId,
				@numDomainID,
				@numItemCode,
				@vcApiItemId,
	 			@numUserCntID,
	 			GETUTCDATE(),
	 			@numUserCntID,
	 			GETUTCDATE(),@vcSKU
	 		) 
		END
 
		IF @charItemType = 'P'
		BEGIN
			
			DECLARE @TEMPWarehouse TABLE
			(
				ID INT IDENTITY(1,1)
				,numWarehouseID NUMERIC(18,0)
			)
		
			INSERT INTO @TEMPWarehouse (numWarehouseID) SELECT numWareHouseID FROM Warehouses WHERE numDomainID=@numDomainID

			DECLARE @j AS INT = 1
			DECLARE @jCount AS INT
			DECLARE @numTempWarehouseID NUMERIC(18,0)
			SELECT @jCount = COUNT(*) FROM @TEMPWarehouse

			WHILE @j <= @jCount
			BEGIN
				SELECT @numTempWarehouseID=numWarehouseID FROM @TEMPWarehouse WHERE ID=@j

				EXEC USP_WarehouseItems_Save @numDomainID,
											@numUserCntID,
											0,
											@numTempWarehouseID,
											0,
											@numItemCode,
											'',
											@monListPrice,
											0,
											0,
											'',
											'',
											'',
											'',
											0
 
				SET @j = @j + 1
			END
		END 
	END         
	ELSE IF @numItemCode>0 AND @intWebApiId > 0 
	BEGIN 
		IF @ProcedureCallFlag = 1 
		BEGIN
			DECLARE @ExportToAPIList AS VARCHAR(30)
			SELECT @ExportToAPIList = vcExportToAPI FROM dbo.Item WHERE numItemCode = @numItemCode

			IF @ExportToAPIList != ''
			BEGIN
				IF NOT EXISTS(select * from (SELECT * FROM dbo.Split(@ExportToAPIList ,','))as A where items= @vcExportToAPI ) 
				BEGIN
					SET @vcExportToAPI = @ExportToAPIList + ',' + @vcExportToAPI
				End
				ELSE
				BEGIN
					SET @vcExportToAPI = @ExportToAPIList 
				End
			END

			--update ExportToAPI String value
			UPDATE dbo.Item SET vcExportToAPI=@vcExportToAPI where numItemCode=@numItemCode  AND numDomainID=@numDomainID
					
			SELECT 
				@cnt = COUNT([numItemID]) 
			FROM 
				[ItemAPI] 
			WHERE  
				[WebApiId] = @intWebApiId 
				AND [numDomainId] = @numDomainID
				AND [vcAPIItemID] = @vcApiItemId
				AND numItemID = @numItemCode

			IF @cnt > 0
			BEGIN
				--update lastUpdated Date 
				UPDATE [ItemAPI] SET [dtModified] =GETUTCDATE() WHERE [WebApiId] =@intWebApiId AND [numDomainId] = @numDomainID
			END
			ELSE
			--Insert ItemAPI mapping
			BEGIN
				INSERT INTO [ItemAPI] 
				(
					[WebApiId],
					[numDomainId],
					[numItemID],
					[vcAPIItemID],
					[numCreatedby],
					[dtCreated],
					[numModifiedby],
					[dtModified],vcSKU
				) VALUES 
				(
					@intWebApiId,
					@numDomainID,
					@numItemCode,
					@vcApiItemId,
					@numUserCntID,
					GETUTCDATE(),
					@numUserCntID,
					GETUTCDATE(),@vcSKU
				)	
			END
		END   
	END                                            
	ELSE IF @numItemCode > 0 AND @intWebApiId <= 0                                                           
	BEGIN
		DECLARE @OldGroupID NUMERIC 
		SELECT @OldGroupID = ISNULL(numItemGroup,0) FROM [Item] WHERE numitemcode = @numItemCode AND [numDomainId] = @numDomainID
	
		DECLARE @monOldAverageCost AS DECIMAL(18,4);SET @monOldAverageCost=0
		SELECT @monOldAverageCost=(CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(monAverageCost,0) END) FROM Item WHERE numItemCode =@numItemCode AND [numDomainId] = @numDomainID
	   
		UPDATE 
			Item 
		SET 
			vcItemName=@vcItemName,txtItemDesc=@txtItemDesc,charItemType=@charItemType,monListPrice= @monListPrice,numItemClassification=@numItemClassification,                                             
			bitTaxable=@bitTaxable,vcSKU = (CASE WHEN ISNULL(@ParentSKU,'') = '' THEN @vcSKU ELSE @ParentSKU END),bitKitParent=@bitKitParent,                                             
			numVendorID=@numVendorID,numDomainID=@numDomainID,bintModifiedDate=getutcdate(),numModifiedBy=@numUserCntID,bitSerialized=@bitSerialized,                                                         
			vcModelID=@vcModelID,numItemGroup=@numItemGroup,numCOGsChartAcntId=@numCOGsChartAcntId,numAssetChartAcntId=@numAssetChartAcntId,                                      
			numIncomeChartAcntId=@numIncomeChartAcntId,monCampaignLabourCost=@monLabourCost,fltWeight=@fltWeight,fltHeight=@fltHeight,fltWidth=@fltWidth,                  
			fltLength=@fltLength,bitFreeShipping=@bitFreeshipping,bitAllowBackOrder=@bitAllowBackOrder,vcUnitofMeasure=@UnitofMeasure,bitShowDeptItem=@bitShowDeptItem,            
			bitShowDeptItemDesc=@bitShowDeptItemDesc,bitCalAmtBasedonDepItems=@bitCalAmtBasedonDepItems,bitAssembly=@bitAssembly,numBarCodeId=@numBarCodeId,
			vcManufacturer=@vcManufacturer,numBaseUnit=@numBaseUnit,numPurchaseUnit=@numPurchaseUnit,numSaleUnit=@numSaleUnit,bitLotNo=@bitLotNo,
			IsArchieve = @IsArchieve,numItemClass=@numItemClass,tintStandardProductIDType=@tintStandardProductIDType,vcExportToAPI=@vcExportToAPI,
			numShipClass=@numShipClass,bitAllowDropShip=@bitAllowDropShip, bitArchiveItem = @bitArchiveItem,bitAsset=@bitAsset,bitRental=@bitRental,
			bitVirtualInventory = @bitVirtualInventory,numContainer=@numContainer,numNoItemIntoContainer=@numNoItemIntoContainer,bitMatrix=@bitMatrix,numManufacturer=@numManufacturer,
			vcASIN = @vcASIN,tintKitAssemblyPriceBasedOn=@tintKitAssemblyPriceBasedOn,fltReorderQty=@fltReorderQty,bitSOWorkOrder=@bitSOWorkOrder,bitKitSingleSelect=@bitKitSingleSelect
			,bitExpenseItem=@bitExpenseItem,numExpenseChartAcntId=@numExpenseChartAcntId,numBusinessProcessId=@numBusinessProcessId,bitTimeContractFromSalesOrder =@bitTimeContractFromSalesOrder 
		WHERE 
			numItemCode=@numItemCode 
			AND numDomainID=@numDomainID

		IF NOT EXISTS(SELECT numItemCode FROM dbo.OpportunityItems WHERE numItemCode = @numItemCode)
		BEGIN
			IF (((SELECT ISNULL(SUM(numOnHand),0) FROM WareHouseItems WHERE numItemID = @numItemCode AND numDomainId=@numDomainID) = 0) AND ((SELECT ISNULL(bitVirtualInventory,0) FROM Item  WHERE numItemCode = @numItemCode AND numDomainId=@numDomainID) = 0))
			BEGIN
				UPDATE Item SET monAverageCost=@monAverageCost  where numItemCode=@numItemCode  AND numDomainID=@numDomainID	
			END
		END
	
		--If Average Cost changed then add in Tracking
		IF ISNULL(@monAverageCost,0) != ISNULL(@monOldAverageCost,0)
		BEGIN
			DECLARE @numWareHouseItemID NUMERIC(9);SET @numWareHouseItemID=0
			DECLARE @numTotal int;SET @numTotal=0
			SET @i=0
			DECLARE @vcDescription AS VARCHAR(100);SET @vcDescription=''
		
			SELECT @numTotal=COUNT(*) FROM WareHouseItems where numItemID=@numItemCode AND numDomainId=@numDomainId
	     
			WHILE @i < @numTotal
			BEGIN
				SELECT @numWareHouseItemID = min(numWareHouseItemID) FROM WareHouseItems 
					WHERE numItemID=@numItemCode AND numDomainId=@numDomainId and numWareHouseItemID > @numWareHouseItemID 
				
				IF @numWareHouseItemID>0
				BEGIN
					SET @vcDescription='Average Cost Changed Manually OLD : ' + CAST(ISNULL(@monOldAverageCost,0) AS VARCHAR(20))
				
					DECLARE @numDomain AS NUMERIC(18,0)
					SET @numDomain = @numDomainID
					EXEC dbo.USP_ManageWareHouseItems_Tracking
						@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)		
						@numReferenceID = @numItemCode, --  numeric(9, 0)
						@tintRefType = 1, --  tinyint
						@vcDescription = @vcDescription, --  varchar(100)
						@tintMode = 0, --  tinyint
						@numModifiedBy = @numUserCntID, --  numeric(9, 0)
						@numDomainID = @numDomain
				END
		    
				SET @i = @i + 1
			END
		END
 
		DECLARE @bitCartFreeShipping as  bit 
		SELECT @bitCartFreeShipping = bitFreeShipping FROM dbo.CartItems where numItemCode = @numItemCode  AND numDomainID = @numDomainID 
 
		IF @bitCartFreeShipping <> @bitFreeshipping
		BEGIN
			UPDATE dbo.CartItems SET bitFreeShipping = @bitFreeshipping WHERE numItemCode = @numItemCode  AND numDomainID=@numDomainID
		END 
 
		IF @intWebApiId > 1 
		BEGIN
			SELECT 
				@cnt = COUNT([numItemID])
			FROM 
				[ItemAPI]
			WHERE 
				[WebApiId] = @intWebApiId
				AND [numDomainId] = @numDomainID
				AND [vcAPIItemID] = @vcApiItemId

			IF @cnt > 0
			BEGIN
				--update lastUpdated Date 
				UPDATE [ItemAPI] SET [dtModified] =GETUTCDATE() WHERE [WebApiId] =@intWebApiId AND [numDomainId] = @numDomainID
			END
			ELSE --Insert ItemAPI mapping
			BEGIN
				INSERT INTO [ItemAPI] 
				(
	 				[WebApiId],
	 				[numDomainId],
	 				[numItemID],
	 				[vcAPIItemID],
	 				[numCreatedby],
	 				[dtCreated],
	 				[numModifiedby],
	 				[dtModified],vcSKU
				 )
				 VALUES 
				 (
					@intWebApiId,
					@numDomainID,
					@numItemCode,
					@vcApiItemId,
	 				@numUserCntID,
	 				GETUTCDATE(),
	 				@numUserCntID,
	 				GETUTCDATE(),@vcSKU
	 			) 
			END
		END   
		                                                                            
		-- kishan It is necessary to add item into itemTax table . 
		IF	@bitTaxable = 1
		BEGIN
			IF NOT EXISTS(SELECT * FROM dbo.ItemTax WHERE numItemCode = @numItemCode AND numTaxItemID = 0)
			BEGIN
			    INSERT INTO dbo.ItemTax 
				(
					numItemCode,
					numTaxItemID,
					bitApplicable
				)
				VALUES 
				( 
					@numItemCode,
					0,
					1 
				) 						
			END
		END	 
      
		IF @charItemType='S' or @charItemType='N'                                                                       
		BEGIN
			DELETE FROM WareHouseItmsDTL where numWareHouseItemID in (select numWareHouseItemID from WareHouseItems where numItemID=@numItemCode AND [WareHouseItems].[numDomainID] = @numDomainID)
			DELETE FROM WareHouseItems where numItemID=@numItemCode AND [WareHouseItems].[numDomainID] = @numDomainID AND numWareHouseItemID NOT IN (SELECT OI.numWarehouseItmsID FROM OpportunityItems OI WHERE OI.numItemCode=@numItemCode)
		END                                      
   
		IF @bitKitParent=1 OR @bitAssembly = 1              
		BEGIN              
			IF DATALENGTH(ISNULL(@strChildItems,'')) > 0
			BEGIN
				SET @strChildItems = CONCAT('<?xml version="1.0" encoding="iso-8859-1"?>',@strChildItems)
			END

			DECLARE @hDoc1 AS INT                                            
			EXEC sp_xml_preparedocument @hDoc1 OUTPUT, @strChildItems                                                                        
                                             
			UPDATE 
				ItemDetails 
			SET
				numQtyItemsReq=X.QtyItemsReq
				,numUOMId=X.numUOMId
				,vcItemDesc=X.vcItemDesc
				,sintOrder=X.sintOrder                                                                                          
			From 
			(
				SELECT 
					QtyItemsReq,ItemKitID,ChildItemID,numUOMId,vcItemDesc,sintOrder, numItemDetailID As ItemDetailID                             
				FROM 
					OPENXML(@hDoc1,'/NewDataSet/Table1',2)                                                                         
				WITH
				(
					ItemKitID NUMERIC(9),                                                          
					ChildItemID NUMERIC(9),                                                                        
					QtyItemsReq DECIMAL(30, 16),
					numUOMId NUMERIC(9),
					vcItemDesc VARCHAR(1000),
					sintOrder int,
					numItemDetailID NUMERIC(18,0)
				)
			)X                                                                         
			WHERE 
				numItemKitID=X.ItemKitID 
				AND numChildItemID=ChildItemID 
				AND numItemDetailID = X.ItemDetailID

			EXEC sp_xml_removedocument @hDoc1
		 END   
		 ELSE
		 BEGIN
 			DELETE FROM ItemDetails WHERE numItemKitID=@numItemCode
		 END                                                
	END

	IF ISNULL(@numItemGroup,0) = 0 OR ISNULL(@bitMatrix,0) = 1
	BEGIN
		UPDATE WareHouseItems SET monWListPrice = @monListPrice, vcWHSKU=@vcSKU, vcBarCode=@numBarCodeId WHERE numDomainID = @numDomainID AND numItemID = @numItemCode
	END

	-- SAVE MATRIX ITEM ATTRIBUTES
	IF @numItemCode > 0 AND ISNULL(@bitAttributesChanged,0)=1 AND ISNULL(@bitMatrix,0) = 1 AND LEN(ISNULL(@vcItemAttributes,'')) > 0 AND ISNULL(@bitOppOrderExists,0) <> 1 AND (SELECT COUNT(*) FROM @TempTable) > 0
	BEGIN
		-- DELETE PREVIOUS ITEM ATTRIBUTES
		DELETE FROM ItemAttributes WHERE numDomainID=@numDomainID AND numItemCode=@numItemCode

		-- INSERT NEW ITEM ATTRIBUTES
		INSERT INTO ItemAttributes
		(
			numDomainID
			,numItemCode
			,FLD_ID
			,FLD_Value
		)
		SELECT
			@numDomainID
			,@numItemCode
			,Fld_ID
			,Fld_Value
		FROM
			@TempTable

		IF (SELECT COUNT(*) FROM WarehouseItems WHERE numDomainID=@numDomainID AND numItemID=@numItemCode) > 0
		BEGIN

			-- DELETE PREVIOUS WAREHOUSE ATTRIBUTES
			DELETE FROM CFW_Fld_Values_Serialized_Items WHERE RecId IN (SELECT ISNULL(numWarehouseItemID,0) FROM WarehouseItems WHERE numDomainID=@numDomainID AND numItemID=@numItemCode)

			-- INSERT NEW WAREHOUSE ATTRIBUTES
			INSERT INTO CFW_Fld_Values_Serialized_Items
			(
				Fld_ID,
				Fld_Value,
				RecId,
				bitSerialized
			)                                                  
			SELECT 
				Fld_ID,
				ISNULL(CAST(Fld_Value AS VARCHAR),'') AS Fld_Value,
				TEMPWarehouse.numWarehouseItemID,
				0 
			FROM 
				@TempTable  
			OUTER APPLY
			(
				SELECT
					numWarehouseItemID
				FROM 
					WarehouseItems 
				WHERE 
					numDomainID=@numDomainID 
					AND numItemID=@numItemCode
			) AS TEMPWarehouse
		END
	END	  
COMMIT

	IF @numItemCode > 0
	BEGIN
		DELETE 
			IC
		FROM 
			dbo.ItemCategory IC
		INNER JOIN 
			Category 
		ON 
			IC.numCategoryID = Category.numCategoryID
		WHERE 
			numItemID = @numItemCode		
			AND numCategoryProfileID = @numCategoryProfileID

		IF @vcCategories <> ''	
		BEGIN
			INSERT INTO ItemCategory( numItemID , numCategoryID )  SELECT  @numItemCode AS numItemID,ID from dbo.SplitIDs(@vcCategories,',')	
		END
	END
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH        
END
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OPPGetINItems')
DROP PROCEDURE USP_OPPGetINItems
GO
-- =============================================  
-- Modified by: <Author,,Sachin Sadhu>  
-- Create date: <Create Date,,24thDec2013>  
-- Description: <Description,,add 3 fields:vcBizDocImagePath,vcBizDocFooter,vcPurBizDocFooter>  
-- =============================================  
CREATE PROCEDURE [dbo].[USP_OPPGetINItems]
(
	@byteMode AS TINYINT  = NULL,
	@numOppId AS NUMERIC(18,0)  = NULL,
	@numOppBizDocsId AS NUMERIC(18,0)  = NULL,
	@numDomainID AS NUMERIC(18,0)  = 0,
	@numUserCntID AS NUMERIC(9)  = 0,
	@ClientTimeZoneOffset INT=0
)
AS
BEGIN
	-- TRANSFERRED PARAMETER VALUES TO TEMPORARY VARIABLES TO AVAOID PARAMETER SNIFFING
	DECLARE @byteModeTemp AS TINYINT
	DECLARE @numOppIdTemp AS NUMERIC(18,0)
	DECLARE @numOppBizDocsIdTemp AS NUMERIC(18,0)
	DECLARE @numDomainIDTemp AS NUMERIC(18,0)
	DECLARE @numUserCntIDTemp AS NUMERIC(18,0)
	DECLARE @ClientTimeZoneOffsetTemp INT

	SET @byteModeTemp= @byteMode
	SET @numOppIdTemp = @numOppId
	SET @numOppBizDocsIdTemp=@numOppBizDocsId
	SET @numDomainIDTemp = @numDomainID
	SET @numUserCntIDTemp = @numUserCntID
	SET @ClientTimeZoneOffsetTemp = @ClientTimeZoneOffset


	IF @byteModeTemp = 1
    BEGIN
		DECLARE  @BizDcocName  AS VARCHAR(100)
		DECLARE  @OppName  AS VARCHAR(100)
		DECLARE  @Contactid  AS VARCHAR(100)
		DECLARE  @shipAmount  AS DECIMAL(20,5)
		DECLARE  @tintOppType  AS TINYINT
		DECLARE  @numlistitemid  AS VARCHAR(15)
		DECLARE  @RecOwner  AS VARCHAR(15)
		DECLARE  @MonAmount  AS VARCHAR(15)
		DECLARE  @OppBizDocID  AS VARCHAR(100)
		DECLARE  @bizdocOwner  AS VARCHAR(15)
		DECLARE  @tintBillType  AS TINYINT
		DECLARE  @tintShipType  AS TINYINT
		DECLARE  @numCustomerDivID AS NUMERIC(18,0)
		DECLARE @numDivisionID NUMERIC(18,0)

		DECLARE @numARContactPosition NUMERIC(18,0)
		DECLARE @numBizDocID NUMERIC(18,0)

		SELECT @numARContactPosition=ISNULL(numARContactPosition,0) FROM Domain WHERE numDomainId=@numDomainId
      
      SELECT @RecOwner = numRecOwner,@MonAmount = CONVERT(VARCHAR(20),monPAmount),
             @Contactid = numContactId,@OppName = vcPOppName,
             @tintOppType = tintOppType,@tintBillType = tintBillToType,@tintShipType = tintShipToType,@numDivisionID=numDivisionId
      FROM   OpportunityMaster WHERE  numOppId = @numOppIdTemp
      
      -- When Creating PO from SO and Bill type is Customer selected 
      SELECT @numCustomerDivID = ISNULL(numDivisionID,0) FROM dbo.OpportunityMaster WHERE 
			numOppID IN (SELECT  ISNULL(numParentOppID,0) FROM dbo.OpportunityLinking WHERE numChildOppID = @numOppIdTemp)
			
      --select @BizDcocName=vcBizDocID from OpportunityBizDocs where numOppBizDocsId=@numOppBizDocsIdTemp
      SELECT @BizDcocName = vcdata,
             @shipAmount = monShipCost,
             @numlistitemid = numlistitemid,
             @OppBizDocID = vcBizDocID,
             @bizdocOwner = OpportunityBizDocs.numCreatedBy,
			 @numBizDocID = ISNULL(OpportunityBizDocs.numBizDocId,0)
      FROM   listdetails JOIN OpportunityBizDocs ON numBizDocId = numlistitemid
      WHERE  numOppBizDocsId = @numOppBizDocsIdTemp

		IF @numBizDocID=287 AND ISNULL(@numARContactPosition,0) > 0 AND EXISTS (SELECT numContactId FROM AdditionalContactsInformation ADC WHERE ADC.numDivisionId=@numDivisionID AND ADC.vcPosition=@numARContactPosition) 
		BEGIN 
			SELECT TOP 1 @Contactid = numContactId FROM AdditionalContactsInformation ADC WHERE ADC.numDivisionId=@numDivisionID AND ADC.vcPosition=@numARContactPosition
		END
      
      SELECT @bizdocOwner AS BizDocOwner,
             @OppBizDocID AS BizDocID,
             @MonAmount AS Amount,
             isnull(@RecOwner,1) AS Owner,
             @BizDcocName AS BizDcocName,
             @numlistitemid AS BizDoc,
             @OppName AS OppName,
             VcCompanyName AS CompName,
             @shipAmount AS ShipAmount,
             dbo.fn_getOPPAddress(@numOppIdTemp,@numDomainIDTemp,1) AS BillAdd,
             dbo.fn_getOPPAddress(@numOppIdTemp,@numDomainIDTemp,2) AS ShipAdd,
             AD.VcStreet,
             AD.VcCity,
             isnull(dbo.fn_GetState(AD.numState),'') AS vcBilState,
             AD.vcPostalCode,
             isnull(dbo.fn_GetListItemName(AD.numCountry),
                    '') AS vcBillCountry,
             con.vcFirstName AS ConName,
             ISNULL(con.numPhone,'') numPhone,
             con.vcFax,
             isnull(con.vcEmail,'') AS vcEmail,
             div.numDivisionID,
             numContactid,
             isnull(vcFirstFldValue,'') vcFirstFldValue,
             isnull(vcSecndFldValue,'') vcSecndFldValue,
             isnull(bitTest,0) AS bitTest,
			dbo.fn_getOPPAddress(@numOppIdTemp,@numDomainIDTemp,1) AS BillToAddressName,
			dbo.fn_getOPPAddress(@numOppIdTemp,@numDomainIDTemp,1) AS ShipToAddressName,
             CASE 
               WHEN @tintBillType IS NULL THEN Com.vcCompanyName
             	-- When Sales order and bill to is set to customer 
               WHEN @tintBillType = 1 AND @tintOppType = 1 THEN Com.vcCompanyName
             -- When Create PO from SO and Bill to is set to Customer 
			   WHEN @tintBillType = 1 AND @tintOppType = 2 THEN dbo.fn_GetComapnyName(@numCustomerDivID)
               WHEN @tintBillType = 0 THEN (SELECT TOP 1 Com1.vcCompanyName
                                            FROM   companyinfo [Com1]
                                                   JOIN divisionmaster div1
                                                     ON com1.numCompanyID = div1.numCompanyID
                                                   JOIN Domain D1
                                                     ON D1.numDivisionID = div1.numDivisionID
                                                    JOIN dbo.AddressDetails AD1
														ON AD1.numDomainID = div1.numDomainID AND AD1.numRecordID = div1.numDivisionID AND tintAddressOf = 2 AND tintAddressType = 1 AND bitIsPrimary=1
                                            WHERE  D1.numDomainID = @numDomainIDTemp)
               WHEN @tintBillType = 2 THEN (SELECT vcBillCompanyName
                                            FROM   OpportunityAddress
                                            WHERE  numOppID = @numOppIdTemp)
				WHEN @tintBillType = 3 THEN  Com.vcCompanyName
              END AS BillToCompanyName,
             CASE 
               WHEN @tintShipType IS NULL THEN Com.vcCompanyName
               WHEN @tintShipType = 1 AND @tintOppType = 1 THEN Com.vcCompanyName
				-- When Create PO from SO and Ship to is set to Customer 
   			   WHEN @tintShipType = 1 AND @tintOppType = 2 THEN dbo.fn_GetComapnyName(@numCustomerDivID)
               WHEN @tintShipType = 0 THEN (SELECT TOP 1 Com1.vcCompanyName
                                            FROM   companyinfo [Com1]
                                                   JOIN divisionmaster div1
                                                     ON com1.numCompanyID = div1.numCompanyID
                                                   JOIN Domain D1
                                                     ON D1.numDivisionID = div1.numDivisionID
                                                    JOIN dbo.AddressDetails AD1
													 ON AD1.numDomainID = div1.numDomainID AND AD1.numRecordID = div1.numDivisionID AND tintAddressOf = 2 AND tintAddressType = 2 AND bitIsPrimary=1
                                            WHERE  D1.numDomainID = @numDomainIDTemp)
               WHEN @tintShipType = 2 THEN (SELECT vcShipCompanyName
                                            FROM   OpportunityAddress
                                            WHERE  numOppID = @numOppIdTemp)
				WHEN @tintShipType = 3 THEN (SELECT vcShipCompanyName
														FROM   OpportunityAddress
														WHERE  numOppID = @numOppIdTemp)
             END AS ShipToCompanyName
      FROM   companyinfo [Com] JOIN divisionmaster div ON com.numCompanyID = div.numCompanyID
             JOIN AdditionalContactsInformation con ON con.numdivisionId = div.numdivisionId
             JOIN Domain D ON D.numDomainID = div.numDomainID
             LEFT JOIN PaymentGatewayDTLID PGDTL ON PGDTL.intPaymentGateWay = D.intPaymentGateWay AND D.numDomainID = PGDTL.numDomainID
             LEFT JOIN AddressDetails AD --Billing add
				ON AD.numDomainID=DIV.numDomainID AND AD.numRecordID=div.numDivisionID AND AD.tintAddressOf = 2 AND AD.tintAddressType = 1 AND AD.bitIsPrimary=1
			 LEFT JOIN AddressDetails AD2--Shipping address
				ON AD2.numDomainID=DIV.numDomainID AND AD2.numRecordID=div.numDivisionID AND AD2.tintAddressOf = 2 AND AD2.tintAddressType = 2 AND AD2.bitIsPrimary=1	
		WHERE  numContactid = @Contactid AND Div.numDomainID = @numDomainIDTemp
    END
  IF @byteModeTemp = 2
    BEGIN
      SELECT opp.vcBizDocID,
             Mst.fltDiscount AS decDiscount,
             isnull(opp.monAmountPaid,0) AS monAmountPaid,
             isnull(opp.vcComments,'') AS vcComments,
             CONVERT(VARCHAR(20),DATEADD(minute,-@ClientTimeZoneOffsetTemp,opp.dtCreatedDate)) dtCreatedDate,
			 dbo.fn_GetContactName(Opp.numCreatedby)+ ' ' + CONVERT(VARCHAR(20),dateadd(MINUTE,-@ClientTimeZoneOffsetTemp,Opp.dtCreatedDate)) AS CreatedBy,
             dbo.fn_GetContactName(opp.numCreatedby) AS numCreatedby,
             dbo.fn_GetContactName(opp.numModifiedBy) AS numModifiedBy,
             CONVERT(VARCHAR(20),DATEADD(minute,-@ClientTimeZoneOffsetTemp,opp.dtModifiedDate)) dtModifiedDate,
			 dbo.fn_GetContactName(Opp.numModifiedBy) + ' ' + CONVERT(VARCHAR(20),dateadd(MINUTE,-@ClientTimeZoneOffsetTemp,Opp.dtModifiedDate)) AS ModifiedBy,
             CASE 
               WHEN isnull(opp.numViewedBy,0) = 0 THEN ''
               ELSE dbo.fn_GetContactName(opp.numViewedBy)
             END AS numViewedBy,
             CASE 
               WHEN opp.numViewedBy = 0 THEN NULL
               ELSE opp.dtViewedDate
             END AS dtViewedDate,
             (CASE WHEN isnull(Mst.intBillingDays,0) > 0 THEN CAST(1 AS BIT) ELSE isnull(Mst.bitBillingTerms,0) END) AS tintBillingTerms,
             isnull(Mst.intBillingDays,0) AS numBillingDays,
			 ISNULL(BTR.numNetDueInDays,0) AS numBillingDaysName,
			 BTR.vcTerms AS vcBillingTermsName,
             isnull(Mst.bitInterestType,0) AS tintInterestType,
             isnull(Mst.fltInterest,0) AS fltInterest,
             tintOPPType,
			 tintOppStatus,
             Opp.numBizDocId,
             dbo.fn_GetContactName(numApprovedBy) AS ApprovedBy,
             dtApprovedDate,
             Mst.bintAccountClosingDate,
             tintShipToType,
             tintBillToType,
             tintshipped,
             dtShippedDate bintShippedDate,
             isnull(opp.monShipCost,0) AS monShipCost,
             (SELECT COUNT(* )
              FROM   DocumentWorkflow
              WHERE  numDocID = @numOppBizDocsIdTemp
                     AND numContactID = @numUserCntIDTemp
                     AND cDocType = 'B'
                     AND tintApprove = 0) AS AppReq,
             (SELECT COUNT(* )
              FROM   DocumentWorkflow
              WHERE  numDocID = @numOppBizDocsIdTemp
                     AND cDocType = 'B'
                     AND tintApprove = 1) AS Approved,
             (SELECT COUNT(* )
              FROM   DocumentWorkflow
              WHERE  numDocID = @numOppBizDocsIdTemp
                     AND cDocType = 'B'
                     AND tintApprove = 2) AS Declined,
             (SELECT COUNT(* )
              FROM   DocumentWorkflow
              WHERE  numDocID = @numOppBizDocsIdTemp
                     AND cDocType = 'B'
                     AND tintApprove = 0) AS Pending,
             ISNULL(BT.vcBizDocFooter,D.vcBizDocFooter) AS BizdocFooter,
             ISNULL(BT.vcBizDocImagePath,D.vcBizDocImagePath) As vcBizDocImagePath, 
             Mst.numDivisionID,
             ISNULL(BT.vcPurBizDocFooter,D.vcPurBizDocFooter) AS PurBizdocFooter, 
             isnull(Mst.fltDiscount,0) AS fltDiscount,
             isnull(Mst.bitDiscountType,0) AS bitDiscountType,
             isnull(Opp.numShipVia,ISNULL(Mst.intUsedShippingCompany,0)) AS numShipVia,
--             isnull(vcTrackingURL,'') AS vcTrackingURL,
			ISNULL( (SELECT vcShipFieldValue FROM dbo.ShippingFieldValues WHERE numDomainID=MSt.numDomainID AND intShipFieldID IN ( SELECT intShipFieldID FROM dbo.ShippingFields WHERE numListItemID=Opp.numShipVia  AND vcFieldName='Tracking URL')),'')		AS vcTrackingURL,
             isnull(Opp.numBizDocStatus,0) AS numBizDocStatus,
             CASE 
               WHEN Opp.numBizDocStatus IS NULL THEN '-'
               ELSE dbo.fn_GetListItemName(Opp.numBizDocStatus)
             END AS BizDocStatus,
             CASE 
               WHEN ISNULL(Opp.numShipVia,0) = 0 THEN (CASE WHEN Mst.intUsedShippingCompany IS NULL THEN '-' WHEN Mst.intUsedShippingCompany = -1 THEN 'Will-call' ELSE dbo.fn_GetListItemName(Mst.intUsedShippingCompany) END)
			   WHEN Opp.numShipVia = -1 THEN 'Will-call'
               ELSE dbo.fn_GetListItemName(Opp.numShipVia)
             END AS ShipVia,
			 ISNULL((SELECT vcShipmentService FROM ShippingService WHERE (numDomainID=@numDomainIDTemp OR ISNULL(numDomainID,0)=0) AND numShippingServiceID = Mst.numShippingService),'') AS vcShippingService,
             ISNULL(C.varCurrSymbol,'') varCurrSymbol,
             (SELECT COUNT(*) FROM [ShippingReport] WHERE numOppBizDocId = opp.numOppBizDocsId )AS ShippingReportCount,
			 isnull(bitPartialFulfilment,0) bitPartialFulfilment,
			 ISNULL(Opp.vcBizDocName,'') vcBizDocName,
			 dbo.[fn_GetContactName](opp.[numModifiedBy])  + ', '
				+ CONVERT(VARCHAR(20),dateadd(MINUTE,-@ClientTimeZoneOffsetTemp,Opp.dtModifiedDate)) AS ModifiedBy,
			 dbo.[fn_GetContactName](Mst.[numRecOwner]) + ', ' +  dbo.[fn_GetContactName](Mst.[numAssignedTo]) AS OrderRecOwner,
			 dbo.[fn_GetContactName](DM.[numRecOwner]) + ', ' +  dbo.[fn_GetContactName](DM.[numAssignedTo]) AS AccountRecOwner,
			 DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,Opp.dtFromDate) AS dtFromDate,
			 Mst.tintTaxOperator,
			 dbo.GetAccountedEmbeddedCost(opp.numOppBizDocsID,1) AS monTotalEmbeddedCost,
			 dbo.GetAccountedEmbeddedCost(opp.numOppBizDocsID,2) AS monTotalAccountedCost,
			 ISNULL(BT.bitEnabled,0) bitEnabled,
			 ISNULL(BT.txtBizDocTemplate,'') txtBizDocTemplate, 
			 ISNULL(BT.txtCSS,'') txtCSS,
			 ISNULL(BT.numOrientation,0) numOrientation,
			 ISNULL(BT.bitKeepFooterBottom,0) bitKeepFooterBottom,
			 dbo.fn_GetContactName(Mst.numAssignedTo) AS AssigneeName,
			 ISNULL((SELECT vcEmail FROM dbo.AdditionalContactsInformation WHERE numContactID = Mst.numAssignedTo),'') AS AssigneeEmail,
			 ISNULL((SELECT numPhone FROM dbo.AdditionalContactsInformation WHERE numContactID = Mst.numAssignedTo),'') AS AssigneePhone,
			 dbo.fn_GetComapnyName(Mst.numDivisionId) OrganizationName,
			 dbo.fn_GetContactName(Mst.numContactID)  OrgContactName,

--CASE WHEN Mst.tintOppType = 1 THEN 
--dbo.getCompanyAddress((select ISNULL(DM.numDivisionID,0)  as numDivisionID from AdditionalContactsInformation AC INNER JOIN DivisionMaster DM 
--on AC.numDivisionID = DM.numDivisionID where numContactID = Mst.numCreatedBy),1,Mst.numDomainId)
--               ELSE dbo.getCompanyAddress(Mst.numDivisionId,1,Mst.numDomainId)
--             END AS CompanyBillingAddress1,

CASE WHEN Mst.tintOppType = 1 THEN 
dbo.getCompanyAddress((select ISNULL(AC.numDivisionID,0)  as numDivisionID from AdditionalContactsInformation AC
INNER JOIN Domain D ON D.numDomainID = AC.numDomainID where AC.numContactID = D.numAdminID AND D.numDomainID = Mst.numDomainId),1,Mst.numDomainId)
               ELSE dbo.getCompanyAddress(Mst.numDivisionId,1,Mst.numDomainId)
             END AS CompanyBillingAddress,

CASE WHEN Mst.tintOppType = 1 THEN 
dbo.getCompanyAddress((select ISNULL(AC.numDivisionID,0)  as numDivisionID from AdditionalContactsInformation AC
INNER JOIN Domain D ON D.numDomainID = AC.numDomainID where AC.numContactID = D.numAdminID AND D.numDomainID = Mst.numDomainId),1,Mst.numDomainId)
               ELSE dbo.getCompanyAddress(Mst.numDivisionId,2,Mst.numDomainId)
             END AS CompanyShippingAddress,
--
--			 dbo.getCompanyAddress(Mst.numDivisionId,1,Mst.numDomainId) CompanyBillingAddress,
--			 dbo.getCompanyAddress(Mst.numDivisionId,2,Mst.numDomainId) CompanyShippingAddress,
			 case when ACI.numPhone<>'' then ACI.numPhone +case when ACI.numPhoneExtension<>'' then ' - ' + ACI.numPhoneExtension else '' end  else '' END OrgContactPhone,
			 DM.vcComPhone as OrganizationPhone,
             ISNULL(ACI.vcEmail,'') AS OrgContactEmail,
 			 dbo.[fn_GetContactName](Mst.[numRecOwner]) AS OnlyOrderRecOwner,
 			 ISNULL(Opp.bitAuthoritativeBizDocs,0) bitAuthoritativeBizDocs,
			 ISNULL(Opp.tintDeferred,0) tintDeferred,isnull(Opp.monCreditAmount,0) AS monCreditAmount,
			 isnull(Opp.bitRentalBizDoc,0) as bitRentalBizDoc,isnull(BT.numBizDocTempID,0) as numBizDocTempID,isnull(BT.vcTemplateName,'') as vcTemplateName,
		     [dbo].[GetDealAmount](opp.numOppId ,getutcdate(),opp.numOppBizDocsId ) as monPAmount,
			 --(select top 1 SD.vcSignatureFile from SignatureDetail SD join OpportunityBizDocsDetails OBD on SD.numSignID=OBD.numSignID where SD.numDomainID = @numDomainIDTemp and OBD.numDomainID = @numDomainIDTemp and OBD.numSignID IS not null and OBD.numBizDocsId=opp.numOppBizDocsId order by OBD.numBizDocsPaymentDetId desc) as vcSignatureFile,
			 isnull(CMP.txtComments,'') as vcOrganizationComments,
			 isnull(Opp.vcTrackingNo,'') AS  vcTrackingNo
			 ,'' AS  vcShippingMethod
			 ,Opp.dtDeliveryDate,
			 CASE WHEN Opp.vcRefOrderNo IS NULL OR LEN(Opp.vcRefOrderNo)=0 THEN ISNULL(Mst.vcOppRefOrderNo,'') ELSE 
			  isnull(Opp.vcRefOrderNo,'') END AS vcRefOrderNo,ISNULL(Mst.numDiscountAcntType,0) AS numDiscountAcntType,
			 ISNULL(opp.numSequenceId,'') AS numSequenceId,
			 CASE WHEN ISNULL(Opp.numBizDocId,0)=296 THEN ISNULL((SELECT SUBSTRING((SELECT '$^$' + dbo.fn_GetListItemName(numBizDocStatus) +'#^#'+ dbo.fn_GetContactName(numUserCntID) +'#^#'+ dbo.FormatedDateTimeFromDate(DateAdd(minute, -@ClientTimeZoneOffsetTemp , dtCreatedDate),@numDomainIDTemp) + '#^#'+ isnull(DFCS.vcColorScheme,'NoForeColor')
					FROM OppFulfillmentBizDocsStatusHistory OFBS left join DycFieldColorScheme DFCS on OFBS.numBizDocStatus=DFCS.vcFieldValue AND DFCS.numDomainID=@numDomainIDTemp
					WHERE numOppId=opp.numOppId and numOppBizDocsId=opp.numOppBizDocsId FOR XML PATH('')),4,200000)),'') ELSE '' END AS vcBizDocsStatusList,ISNULL(Mst.numCurrencyID,0) AS numCurrencyID,
			ISNULL(Opp.fltExchangeRateBizDoc,Mst.fltExchangeRate) AS fltExchangeRateBizDoc,
			ISNULL(TBLEmployer.vcCompanyName,'') AS EmployerOrganizationName,ISNULL(TBLEmployer.vcComPhone,'') as EmployerOrganizationPhone,ISNULL(opp.bitAutoCreated,0) AS bitAutoCreated,
			ISNULL(RecurrenceConfiguration.numRecConfigID,0) AS numRecConfigID,
			(CASE WHEN RecurrenceConfiguration.numRecConfigID IS NULL THEN 0 ELSE 1 END) AS bitRecur,
			(CASE WHEN RecurrenceTransaction.numRecTranID IS NULL THEN 0 ELSE 1 END) AS bitRecurred,
			ISNULL(Mst.bitRecurred,0) AS bitOrderRecurred,
			dbo.FormatedDateFromDate(RecurrenceConfiguration.dtStartDate,@numDomainIDTemp) AS dtStartDate,
			RecurrenceConfiguration.vcFrequency AS vcFrequency,
			ISNULL(RecurrenceConfiguration.bitDisabled,0) AS bitDisabled,
			ISNULL(opp.bitFulfilled,0) AS bitFulfilled,
			ISNULL(opp.numDeferredBizDocID,0) AS numDeferredBizDocID,
			dbo.GetTotalQtyByUOM(opp.numOppBizDocsId) AS vcTotalQtybyUOM,
			ISNULL(Mst.numShippingService,0) AS numShippingService,
			dbo.FormatedDateFromDate(Mst.dtReleaseDate,@numDomainIDTemp) AS vcReleaseDate,
			dbo.FormatedDateFromDate(Mst.intpEstimatedCloseDate,@numDomainIDTemp) AS vcRequiredDate,
			(CASE WHEN ISNULL(Mst.numPartner,0) > 0 THEN ISNULL((SELECT CONCAT(DivisionMaster.vcPartnerCode,'-',CompanyInfo.vcCompanyName) FROM DivisionMaster INNER JOIN CompanyInfo ON DivisionMaster.numCompanyID=CompanyInfo.numCompanyId WHERE DivisionMaster.numDivisionID=Mst.numPartner),'') ELSE '' END) AS vcPartner,
			dbo.FormatedDateFromDate(mst.dtReleaseDate,@numDomainIDTemp) AS dtReleaseDate,
			(select TOP 1 vcBizDocID from OpportunityBizDocs where numOppId=opp.numOppId and numBizDocId=296) as vcFulFillment,
			(select TOP 1 vcOppRefOrderNo from OpportunityMaster WHERE numOppId=opp.numOppId) AS vcPOName,
			ISNULL(vcCustomerPO#,'') AS vcCustomerPO#,
			ISNULL(Mst.txtComments,'') vcSOComments,
			ISNULL((SELECT DMSA.vcAccountNumber FROm [dbo].[DivisionMasterShippingAccount] DMSA WHERE DMSA.numDivisionID=DM.numDivisionID AND DMSA.numShipViaID=opp.numShipVia),'') AS vcShippersAccountNo,
			CONVERT(VARCHAR(20),DATEADD(minute,-@ClientTimeZoneOffsetTemp,Mst.bintCreatedDate)) OrderCreatedDate,
			(CASE WHEN ISNULL(opp.numSourceBizDocId,0) > 0 THEN ISNULL((SELECT vcVendorInvoice FROM OpportunityBizDocs WHERE OpportunityBizDocs.numOppBizDocsId=opp.numSourceBizDocId),'') ELSE ISNULL(vcVendorInvoice,'') END) vcVendorInvoice,
			(CASE 
				WHEN ISNULL(opp.numSourceBizDocId,0) > 0
				THEN ISNULL((SELECT vcBizDocID FROM OpportunityBizDocs WHERE OpportunityBizDocs.numOppBizDocsId=Opp.numSourceBizDocId),'')
				ELSE ''
			END) vcPackingSlip
			,ISNULL(numARAccountID,0) numARAccountID
			,(CASE WHEN (SELECT COUNT(*) FROM OpportunityBizDocs OBInner WHERE OBInner.numOppID=@numOppID AND OBInner.numBizDocId=296 AND OBInner.numSourceBizDocId=opp.numOppBizDocsId AND opp.numBizDocId=29397) > 0 THEN 1 ELSE 0 END) AS IsFulfillmentGeneratedOnPickList
			,ISNULL((SELECT COUNT(*) FROM OpportunityBizDocs WHERE numOppID=@numOppID AND ISNULL(numDeferredBizDocID,0) = ISNULL(opp.numOppBizDocsId,0)),0) AS intInvoiceForDiferredBizDoc
			,ISNULL(dbo.CheckOrderInventoryStatus(Mst.numOppID,Mst.numDomainID,2),'') vcInventoryStatus,
			CASE WHEN ISNULL(DM.intDropShip,0)=1 THEN 'Not Available'
				 WHEN ISNULL(DM.intDropShip,0)=2 THEN 'Blank Available'
				 WHEN ISNULL(DM.intDropShip,0)=3 THEN 'Vendor Label'
				 WHEN ISNULL(DM.intDropShip,0)=4 THEN 'Private Label'
				 ELSE '-'
			END AS vcDropShip
			,ISNULL(D.numShippingServiceItemID,0) numShippingServiceItemID
			,ISNULL(D.numDiscountServiceItemID,0) numDiscountServiceItemID 
			,CASE WHEN ISNULL(opp.numSourceBizDocId,0)=0 THEN
			ISNULL((SELECT TOP 1 vcBizDocID FROM OpportunityBizDocs WHERE numSourceBizDocId=opp.numOppBizDocsId),'NA') ELSE 
			ISNULL((SELECT TOP 1 vcBizDocID FROM OpportunityBizDocs WHERE numOppBizDOcsId=opp.numSourceBizDocId),'NA') END AS vcSourceBizDocID
		FROM   OpportunityBizDocs opp JOIN OpportunityMaster Mst ON Mst.numOppId = opp.numOppId
             JOIN Domain D ON D.numDomainID = Mst.numDomainID
             LEFT OUTER JOIN [Currency] C ON C.numCurrencyID = Mst.numCurrencyID
             LEFT JOIN [DivisionMaster] DM ON DM.numDivisionID = Mst.numDivisionID   
			 LEFT JOIN CompanyInfo CMP ON DM.numCompanyId = CMP.numCompanyId and CMP.numDomainID = @numDomainIDTemp  
             LEFT JOIN dbo.AdditionalContactsInformation ACI ON ACI.numContactID = Mst.numContactID
             LEFT JOIN [BizDocTemplate] BT ON BT.numDomainID = @numDomainIDTemp AND BT.numBizDocTempID=opp.numBizDocTempID
             LEFT JOIN BillingTerms BTR ON BTR.numTermsID = ISNULL(Mst.intBillingDays,0)
             JOIN divisionmaster div1 ON Mst.numDivisionID = div1.numDivisionID
			 JOIN companyinfo [Com1] ON com1.numCompanyID = div1.numCompanyID
			 LEFT JOIN RecurrenceConfiguration ON Opp.numOppBizDocsId = RecurrenceConfiguration.numOppBizDocID
			 LEFT JOIN RecurrenceTransaction ON Opp.numOppBizDocsId = RecurrenceTransaction.numRecurrOppBizDocID
			 OUTER APPLY
			 (
				SELECT TOP 1 
					Com1.vcCompanyName, div1.vcComPhone
                FROM   companyinfo [Com1]
                        JOIN divisionmaster div1
                            ON com1.numCompanyID = div1.numCompanyID
                        JOIN Domain D1
                            ON D1.numDivisionID = div1.numDivisionID
                        JOIN dbo.AddressDetails AD1
							ON AD1.numDomainID = div1.numDomainID AND AD1.numRecordID = div1.numDivisionID AND tintAddressOf = 2 AND tintAddressType = 1 AND bitIsPrimary=1
                WHERE  D1.numDomainID = @numDomainIDTemp
			 ) AS TBLEmployer
      WHERE  opp.numOppBizDocsId = @numOppBizDocsIdTemp
             AND Mst.numDomainID = @numDomainIDTemp
    END
END
GO
/****** Object:  StoredProcedure [dbo].[USP_Projects]    Script Date: 07/26/2008 16:20:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_projects')
DROP PROCEDURE usp_projects
GO
CREATE PROCEDURE [dbo].[USP_Projects]                          
(                          
@numProId numeric(9)=null ,  
@numDomainID as numeric(9),  
@ClientTimeZoneOffset as int         
                         
)                          
as                          
                          
begin                          
                                          
 select  pro.numProId,                          
  pro.vcProjectName,                         
  pro.numintPrjMgr,                          
  pro.numOppId,                    
  pro.intDueDate,                          
  pro.numCustPrjMgr,                          
  pro.numDivisionId,                          
  pro.txtComments,      
  Div.tintCRMType,                          
  div.vcDivisionName,                          
  com.vcCompanyName,
  A.vcFirstname + ' ' + A.vcLastName AS vcContactName,
  isnull(A.vcEmail,'') AS vcEmail,
         isnull(A.numPhone,'') AS Phone,
         isnull(A.numPhoneExtension,'') AS PhoneExtension,
		 		 (SELECT TOP 1 ISNULL(vcData,'') FROM ListDetails WHERE numListID=5 AND numListItemID=com.numCompanyType AND numDomainId=@numDomainID) AS vcCompanyType,
pro.numcontractId,                       
  dbo.fn_GetContactName(pro.numCreatedBy)+' '+convert(varchar(20),dateadd(minute,-@ClientTimeZoneOffset,pro.bintCreatedDate)) as vcCreatedBy ,                          
  dbo.fn_GetContactName(pro.numModifiedBy)+' '+convert(varchar(20),dateadd(minute,-@ClientTimeZoneOffset,pro.bintModifiedDate)) as vcModifiedby,                        
  dbo.fn_GetContactName(pro.numCompletedBy)+' '+convert(varchar(20),dateadd(minute,-@ClientTimeZoneOffset,pro.dtCompletionDate)) as vcFinishedby,                        
  pro.numDomainId,                          
  dbo.fn_GetContactName(pro.numRecOwner) as vcRecOwner,  pro.numRecOwner,   div.numTerID,       
  pro.numAssignedby,pro.numAssignedTo,          
(select  count(*) from dbo.GenericDocuments where numRecID=@numProId and  vcDocumentSection='P') as DocumentCount,
	
 numAccountID,
 '' TimeAndMaterial,
isnull((select sum(isnull(monTimeBudget,0)) from StagePercentageDetails where numProjectID=@numProId),0) TimeBudget,
isnull((select sum(isnull(monExpenseBudget,0)) from StagePercentageDetails where numProjectID=@numProId),0) ExpenseBudget,
isnull((select min(isnull(dtStartDate,getdate()))-1 from StagePercentageDetails where numProjectID=@numProId),getdate()) dtStartDate,
isnull((select max(isnull(dtEndDate,getdate()))+1 from StagePercentageDetails where numProjectID=@numProId),getdate()) dtEndDate,
ISNULL(numProjectType,0) AS numProjectType,
isnull(dbo.fn_GetExpenseDtlsbyProStgID(pro.numProId,0,0),0) as UsedExpense,                        
isnull(dbo.fn_GeTimeDtlsbyProStgID_BT_NBT(pro.numProId,0,1),'Billable Time (0)  Non Billable Time (0)') as UsedTime,
isnull(Pro.numProjectStatus,0) as numProjectStatus,
(SELECT ISNULL(vcStreet,'') + ',<pre>' + ISNULL(vcCity,'') + ',<pre>' +  dbo.[fn_GetState]([numState]) + ',<pre>' + dbo.fn_GetListItemName(numCountry) + ',<pre>' + vcPostalCode 
 FROM dbo.AddressDetails WHERE numDomainID = pro.numDomainID AND numRecordID = pro.numAddressID AND tintAddressOf = 4 AND tintAddressType = 2 AND bitIsPrimary = 1 )as ShippingAddress,
Pro.numBusinessProcessID,
SLP.Slp_Name AS vcProcessName,
pro.dtCompletionDate AS vcProjectedFinish,
NULL AS dtActualStartDate,
DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,pro.dtmEndDate) dtmEndDate,
DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,pro.dtmStartDate) dtmStartDate,
ISNULL(pro.numClientBudget,0) AS numClientBudget,
0 AS numClientBudgetBalance,
(CASE 
	WHEN EXISTS (SELECT CInner.numContractId FROM Contracts CInner WHERE CInner.numDomainId=@numDomainID AND CInner.numDivisonId=pro.numDivisionID AND intType=1) 
	THEN 1
	ELSE 0
END) AS bitContractExists,
0 AS numClientBudgetBalance,
(CASE 
	WHEN EXISTS (SELECT CInner.numContractId FROM Contracts CInner WHERE CInner.numDomainId=@numDomainID AND CInner.numDivisonId=pro.numDivisionID AND intType=1) 
	THEN dbo.fn_SecondsConversion(ISNULL((SELECT TOP 1 ((ISNULL(numHours,0) * 60 * 60) + (ISNULL(numMinutes,0) * 60)) - ISNULL((SELECT SUM(CLInner.numUsedTime) FROM ContractsLog CLInner WHERE CLInner.numContractId=C.numContractId),0) FROM Contracts C WHERE C.numDomainId=@numDomainID AND C.numDivisonId=pro.numDivisionID AND intType=1 ORDER BY C.numContractID DESC),0))
	ELSE '' 
END) AS timeLeft,
(CASE 
	WHEN EXISTS (SELECT CInner.numContractId FROM Contracts CInner WHERE CInner.numDomainId=@numDomainID AND CInner.numDivisonId=pro.numDivisionID AND intType=1) 
	THEN ISNULL((SELECT TOP 1 ((ISNULL(numHours,0) * 60 * 60) + (ISNULL(numMinutes,0) * 60)) - ISNULL((SELECT SUM(CLInner.numUsedTime) FROM ContractsLog CLInner WHERE CLInner.numContractId=C.numContractId),0) FROM Contracts C WHERE C.numDomainId=@numDomainID AND C.numDivisonId=pro.numDivisionID AND intType=1 ORDER BY C.numContractID DESC),0)
	ELSE 0
END) AS timeLeftInMinutes,
ISNULL(bitDisplayTimeToExternalUsers,0) AS bitDisplayTimeToExternalUsers
from ProjectsMaster pro                          
  left join DivisionMaster div                          
  on pro.numDivisionID=div.numDivisionID    
  LEFT JOIN AdditionalContactsInformation A ON pro.numCustPrjMgr = A.numContactId
  left join CompanyInfo com                          
  on div.numCompanyID=com.numCompanyId         
  LEFT JOIN dbo.AddressDetails AD ON AD.numAddressID = pro.numAddressID       
		LEFT JOIN Sales_process_List_Master AS SLP ON pro.numBusinessProcessID=SLP.Slp_Id          
  where numProId=@numProId    and pro.numDomainID=@numDomainID                      
                          
end
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ReportQueryExecute')
DROP PROCEDURE USP_ReportQueryExecute
GO
CREATE PROCEDURE [dbo].[USP_ReportQueryExecute]     
    @numDomainID NUMERIC(18,0),
    @numUserCntID NUMERIC(18,0),
	@numReportID NUMERIC(18,0),
    @ClientTimeZoneOffset INT, 
    @textQuery NTEXT,
	@numCurrentPage NUMERIC(18,0)
AS                 
BEGIN
	DECLARE @tintReportType INT
	DECLARE @numReportModuleID INT
	SELECT @tintReportType=ISNULL(tintReportType,0),@numReportModuleID=ISNULL(numReportModuleID,0) FROM ReportListMaster WHERE numReportID=@numReportID

	IF CHARINDEX('WareHouseItems.numBackOrder',CAST(@textQuery AS VARCHAR(MAX))) > 0 AND @numReportModuleID <> 6
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'WareHouseItems.numBackOrder','(CASE WHEN ISNULL((SELECT SUM(WIInner.numOnHand) FROM WareHouseItems WIInner WHERE WIInner.numItemID=WareHouseItems.numItemID AND WIInner.numWareHouseItemID <> WareHouseItems.numWareHouseItemID AND WIInner.numWareHouseID = WareHouseItems.numWareHouseID),0) >= WareHouseItems.numBackOrder THEN 0 ELSE ISNULL(WareHouseItems.numBackOrder,0) END)')
	END
	
	IF CHARINDEX('OpportunityItems.vcOpenBalance',CAST(@textQuery AS VARCHAR(MAX))) > 0	
	BEGIN	
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'OpportunityItems.vcOpenBalance','(CASE WHEN OpportunityMaster.tintOppType=2 THEN (ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numUnitHourReceived,0)) ELSE (ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyShipped,0)) END)')	
	END	

	IF CHARINDEX('isnull(OpportunityItems.numRemainingQtyToPick,0)',CAST(@textQuery AS VARCHAR(MAX))) > 0	
	BEGIN	
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'isnull(OpportunityItems.numRemainingQtyToPick,0)','(isnull(OpportunityItems.numUnitHour,0) - isnull(OpportunityItems.numQtyPicked,0))')	
	END	

	IF CHARINDEX('OpportunityItems.numRemainingQtyToPick',CAST(@textQuery AS VARCHAR(MAX))) > 0	
	BEGIN	
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'OpportunityItems.numRemainingQtyToPick','(isnull(OpportunityItems.numUnitHour,0) - isnull(OpportunityItems.numQtyPicked,0))')	
	END		

	IF CHARINDEX('isnull(OpportunityItems.numRemainingQtyToShip,0)',CAST(@textQuery AS VARCHAR(MAX))) > 0	
	BEGIN	
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'isnull(OpportunityItems.numRemainingQtyToShip,0)','(isnull(OpportunityItems.numUnitHour,0) - isnull(OpportunityItems.numQtyShipped,0))')	
	END	

	IF CHARINDEX('OpportunityItems.numRemainingQtyToShip',CAST(@textQuery AS VARCHAR(MAX))) > 0	
	BEGIN	
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'OpportunityItems.numRemainingQtyToShip','(isnull(OpportunityItems.numUnitHour,0) - isnull(OpportunityItems.numQtyShipped,0))')	
	END	

	IF CHARINDEX('isnull(OpportunityItems.numRemainingQtyToInvoice,0)',CAST(@textQuery AS VARCHAR(MAX))) > 0	
	BEGIN	
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'isnull(OpportunityItems.numRemainingQtyToInvoice,0)','(isnull(OpportunityItems.numUnitHour,0) - isnull((SELECT SUM(numUnitHour) FROM OpportunityBizDocItems INNER JOIN OpportunityBizDocs ON OpportunityBizDocItems.numOppBizDocID = OpportunityBizDocs.numOppBizDocsId  WHERE OpportunityBizDocs.numOppId = OpportunityItems.numOppId AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtCode AND ISNULL(bitAuthoritativeBizDocs, 0) = 1 AND ISNULL(OpportunityBizDocs.numBizDocID,0) = 287),0))')	
	END	

	IF CHARINDEX('OpportunityItems.numRemainingQtyToInvoice',CAST(@textQuery AS VARCHAR(MAX))) > 0	
	BEGIN	
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'OpportunityItems.numRemainingQtyToInvoice','(isnull(OpportunityItems.numUnitHour,0) - isnull((SELECT SUM(numUnitHour) FROM OpportunityBizDocItems INNER JOIN OpportunityBizDocs ON OpportunityBizDocItems.numOppBizDocID = OpportunityBizDocs.numOppBizDocsId  WHERE OpportunityBizDocs.numOppId = OpportunityItems.numOppId AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtCode AND ISNULL(bitAuthoritativeBizDocs, 0) = 1 AND ISNULL(OpportunityBizDocs.numBizDocID,0) = 287),0))')	
	END	

	IF CHARINDEX('AND OpportunityItems.monPendingSales',CAST(@textQuery AS VARCHAR(MAX))) > 0	
	BEGIN	
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'AND OpportunityItems.monPendingSales','AND (ISNULL((ISNULL(ISNULL(dbo.fn_UOMConversion(ISNULL(Item.numBaseUnit, 0), OpportunityItems.numItemCode, Item.numDomainId, ISNULL(OpportunityItems.numUOMId, 0)), 1) * OpportunityItems.numUnitHour, 0) - isnull(OpportunityItems.numQtyShipped, 0) ) * (isnull(OpportunityItems.monPrice, 0)), 0))')	
	END	
	
	IF CHARINDEX('AND OpportunityItems.numPendingToBill',CAST(@textQuery AS VARCHAR(MAX))) > 0	
	BEGIN	
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'AND OpportunityItems.numPendingToBill','AND ISNULL((isnull(OpportunityItems.numUnitHourReceived, 0)) - (ISNULL(( SELECT SUM(numUnitHour)  FROM	 OpportunityBizDocItems  INNER JOIN	OpportunityBizDocs ON OpportunityBizDocItems.numOppBizDocID = OpportunityBizDocs.numOppBizDocsId  WHERE OpportunityBizDocs.numOppId = OpportunityItems.numOppId AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtCode AND ISNULL(bitAuthoritativeBizDocs, 0) = 1), 0)), 0)')	
	END	
		
	IF CHARINDEX('AND OpportunityItems.numPendingtoReceive',CAST(@textQuery AS VARCHAR(MAX))) > 0	
	BEGIN	
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'AND OpportunityItems.numPendingtoReceive','AND isnull((isnull((SELECT SUM(numUnitHour) FROM OpportunityBizDocItems INNER JOIN OpportunityBizDocs ON OpportunityBizDocItems.numOppBizDocID = OpportunityBizDocs.numOppBizDocsId WHERE OpportunityBizDocs.numOppId = OpportunityItems.numOppId AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtCode AND ISNULL(bitAuthoritativeBizDocs, 0) = 1), 0)) - (isnull(OpportunityItems.numUnitHourReceived, 0)), 0)')	
	END

	IF CHARINDEX('AND WareHouseItems.vcLocation',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'AND WareHouseItems.vcLocation','AND WareHouseItems.numWLocationID')
	END

	IF CHARINDEX('ReturnItems.monPrice',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'ReturnItems.monPrice','CAST(ReturnItems.monPrice AS NUMERIC(18,4))')
	END

	IF CHARINDEX('OpportunityItems.monPrice',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'OpportunityItems.monPrice','CAST(OpportunityItems.monPrice AS NUMERIC(18,4))')
	END

	IF CHARINDEX('isnull(category.numCategoryID,0)',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'isnull(category.numCategoryID,0)' ,'(SELECT ISNULL(STUFF((SELECT '','' + CONVERT(VARCHAR(50) , Category.vcCategoryName) FROM ItemCategory LEFT JOIN Category ON ItemCategory.numCategoryID = Category.numCategoryID WHERE ItemCategory.numItemID = Item.numItemCode FOR XML PATH('''')), 1, 1, ''''),''''))')
	END

	IF CHARINDEX('dbo.FormatedDateFromDate(dateadd(minute,-@ClientTimeZoneOffset,OpportunityBizDocs.dtFullPaymentDate),@numDomainId)',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'dbo.FormatedDateFromDate(dateadd(minute,-@ClientTimeZoneOffset,OpportunityBizDocs.dtFullPaymentDate),@numDomainId)' ,'(CASE 
																																															WHEN ISNULL(OpportunityMaster.tintOppType,0) = 2 AND ISNULL(OpportunityBizDocs.monDealAmount,0) > 0 AND ISNULL(OpportunityBizDocs.monAmountPaid,0) >= ISNULL(OpportunityBizDocs.monDealAmount,0)
																																															THEN dbo.FormatedDateFromDate((SELECT MAX(BPH.dtPaymentDate) FROM BillPaymentDetails BPD INNER JOIN BillPaymentHeader BPH ON BPD.numBillPaymentID=BPH.numBillPaymentID  WHERE BPD.numOppBizDocsID=OpportunityBizDocs.numOppBizDocsId),@numDomainID)
																																															WHEN ISNULL(OpportunityMaster.tintOppType,0) = 1 AND ISNULL(OpportunityBizDocs.monDealAmount,0) > 0 AND ISNULL(OpportunityBizDocs.monAmountPaid,0) >= ISNULL(OpportunityBizDocs.monDealAmount,0)
																																															THEN dbo.FormatedDateFromDate((SELECT MAX(dtCreatedDate) FROM DepositeDetails WHERE DepositeDetails.numOppBizDocsID=OpportunityBizDocs.numOppBizDocsId),@numDomainID)
																																															ELSE ''''
																																														END)')
	END

	--KEEP IT BELOW ABOVE REPLACEMENT
	IF CHARINDEX('OpportunityBizDocs.dtFullPaymentDate',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'OpportunityBizDocs.dtFullPaymentDate' ,'CAST((CASE 
																												WHEN ISNULL(OpportunityMaster.tintOppType,0) = 2 AND ISNULL(OpportunityBizDocs.monDealAmount,0) > 0 AND ISNULL(OpportunityBizDocs.monAmountPaid,0) >= ISNULL(OpportunityBizDocs.monDealAmount,0)
																												THEN (SELECT MAX(BPH.dtPaymentDate) FROM BillPaymentDetails BPD INNER JOIN BillPaymentHeader BPH ON BPD.numBillPaymentID=BPH.numBillPaymentID  WHERE BPD.numOppBizDocsID=OpportunityBizDocs.numOppBizDocsId)
																												WHEN ISNULL(OpportunityMaster.tintOppType,0) = 1 AND ISNULL(OpportunityBizDocs.monDealAmount,0) > 0 AND ISNULL(OpportunityBizDocs.monAmountPaid,0) >= ISNULL(OpportunityBizDocs.monDealAmount,0)
																												THEN (SELECT MAX(dtCreatedDate) FROM DepositeDetails WHERE DepositeDetails.numOppBizDocsID=OpportunityBizDocs.numOppBizDocsId)
																												ELSE ''''
																											END) AS DATE)')
	END

	IF CHARINDEX('isnull(OpportunityMaster.monProfit,''0'')',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'isnull(OpportunityMaster.monProfit,''0'')' ,'ISNULL(dbo.GetOrderItemProfitAmountOrMargin(OpportunityMaster.numDomainID,OpportunityItems.numOppId,OpportunityItems.numoppitemtCode,1),0)')
	END

	-- THIS ONE SHOULD BE AFTER ABOVE ONE
	IF CHARINDEX('OpportunityMaster.monProfit',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'OpportunityMaster.monProfit' ,'ISNULL(dbo.GetOrderItemProfitAmountOrMargin(OpportunityMaster.numDomainID,OpportunityItems.numOppId,OpportunityItems.numoppitemtCode,1),0)')
	END

	IF CHARINDEX('isnull(OpportunityBizDocItems.monProfit,''0'')',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'isnull(OpportunityBizDocItems.monProfit,''0'')','ISNULL(dbo.GetBizDocItemProfitAmountOrMargin(OpportunityMaster.numDomainID,OpportunityMaster.numOppId,OpportunityBizDocItems.numOppBizDocID,OpportunityBizDocItems.numOppBizDocItemID,1),0)')
	END

	-- THIS ONE SHOULD BE AFTER ABOVE ONE
	IF CHARINDEX('OpportunityBizDocItems.monProfit',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'OpportunityBizDocItems.monProfit','ISNULL(dbo.GetBizDocItemProfitAmountOrMargin(OpportunityMaster.numDomainID,OpportunityMaster.numOppId,OpportunityBizDocItems.numOppBizDocID,OpportunityBizDocItems.numOppBizDocItemID,1),0)')
	END

	IF CHARINDEX('ISNULL(PT.numItemCode,0)=Item.numItemCode',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'ISNULL(PT.numItemCode,0)=Item.numItemCode' ,'Item.numItemCode=PT.numItemCode')
	END

	IF CHARINDEX('isnull(OpportunityMaster.numProfitPercent,0)',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'isnull(OpportunityMaster.numProfitPercent,0)' ,'ISNULL(dbo.GetOrderItemProfitAmountOrMargin(OpportunityMaster.numDomainID,OpportunityItems.numOppId,OpportunityItems.numoppitemtCode,2),0)')
	END
	
	-- THIS ONE SHOULD BE AFTER ABOVE ONE
	IF CHARINDEX('OpportunityMaster.numProfitPercent',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'OpportunityMaster.numProfitPercent' ,'ISNULL(dbo.GetOrderItemProfitAmountOrMargin(OpportunityMaster.numDomainID,OpportunityItems.numOppId,OpportunityItems.numoppitemtCode,2),0)')
	END

	IF CHARINDEX('isnull(OpportunityBizDocItems.numProfitPercent,0)',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'isnull(OpportunityBizDocItems.numProfitPercent,0)','ISNULL(dbo.GetBizDocItemProfitAmountOrMargin(OpportunityMaster.numDomainID,OpportunityItems.numOppId,OpportunityItems.numoppitemtCode,2),0)')
	END
	
	-- THIS ONE SHOULD BE AFTER ABOVE ONE
	IF CHARINDEX('OpportunityBizDocItems.numProfitPercent',CAST(@textQuery AS VARCHAR(MAX))) > 0
	BEGIN
		SET @textQuery = REPLACE(CAST(@textQuery AS VARCHAR(MAX)),'OpportunityBizDocItems.numProfitPercent' ,'ISNULL(dbo.GetBizDocItemProfitAmountOrMargin(OpportunityMaster.numDomainID,OpportunityItems.numOppId,OpportunityItems.numoppitemtCode,2),0)')
	END

	IF ISNULL(@numCurrentPage,0) > 0 
		AND ISNULL(@tintReportType,0) <> 3 
		AND ISNULL(@tintReportType,0) <> 4 
		AND ISNULL(@tintReportType,0) <> 5 
		AND CHARINDEX('Select top',CAST(@textQuery AS VARCHAR(MAX))) <> 1
		AND (SELECT COUNT(*) FROM ReportSummaryGroupList WHERE numReportID=@numReportID) = 0
	BEGIN
		SET @textQuery = CONCAT(CAST(@textQuery AS VARCHAR(MAX)),' OFFSET ',(@numCurrentPage-1) * 100 ,' ROWS FETCH NEXT 100 ROWS ONLY OPTION (OPTIMIZE FOR (@numDomainID UNKNOWN));')
	END
	ELSE
	BEGIN
		SET @textQuery = CONCAT(CAST(@textQuery AS VARCHAR(MAX)),' OPTION (OPTIMIZE FOR (@numDomainID UNKNOWN));')
	END
	 
	 print CAST(@textQuery AS ntext)
	EXECUTE sp_executesql @textQuery,N'@numDomainID numeric(18, 0),@numUserCntID numeric(18,0),@ClientTimeZoneOffset int',@numDomainID=@numDomainID,@numUserCntID=@numUserCntID,@ClientTimeZoneOffset=@ClientTimeZoneOffset
END
/****** Object:  StoredProcedure [dbo].[usp_SetUsersWithDomains]    Script Date: 07/26/2008 16:21:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_setuserswithdomains')
DROP PROCEDURE usp_setuserswithdomains
GO
CREATE  PROCEDURE [dbo].[usp_SetUsersWithDomains]
	@numUserID NUMERIC(9),                                    
	@vcUserName VARCHAR(50),                                    
	@vcUserDesc VARCHAR(250),                              
	@numGroupId as numeric(9),                                           
	@numUserDetailID numeric ,                              
	@numUserCntID as numeric(9),                              
	@strTerritory as varchar(4000) ,                              
	@numDomainID as numeric(9),
	@strTeam as varchar(4000),
	@vcEmail as varchar(100),
	@vcPassword as varchar(100),          
	@Active as BIT,
	@numDefaultClass NUMERIC,
	@numDefaultWarehouse as numeric(18),
	@vcLinkedinId varchar(300)=null,
	@intAssociate int=0,
	@ProfilePic varchar(100)=null
	,@bitPayroll BIT = 0
	,@monHourlyRate DECIMAL(20,5) = 0
	,@tintPayrollType TINYINT = 0
	,@tintHourType TINYINT = 0
	,@monOverTimeRate DECIMAL(20,5) = 0
	,@bitOauthImap BIT = 0
	,@tintMailProvider TINYINT = 3
AS
BEGIN
	DECLARE @numNewFullUsers AS INT
	DECLARE @numNewLimitedAccessUsers AS INT

	SET @numNewFullUsers = ISNULL((SELECT
										COUNT(*)
									FROM
										UserMaster 
									INNER JOIN
										AuthenticationGroupMaster
									ON
										UserMaster.numGroupID = AuthenticationGroupMaster.numGroupID
									WHERE
										UserMaster.numDomainID = @numDomainID
										AND AuthenticationGroupMaster.tintGroupType=1
										AND ISNULL(UserMaster.bitActivateFlag,0)=1
										AND UserMaster.numUserId <> ISNULL(@numUserID,0)),0)

	SET @numNewLimitedAccessUsers = ISNULL((SELECT
										COUNT(*)
									FROM
										UserMaster 
									INNER JOIN
										AuthenticationGroupMaster
									ON
										UserMaster.numGroupID = AuthenticationGroupMaster.numGroupID
									WHERE
										UserMaster.numDomainID = @numDomainID
										AND AuthenticationGroupMaster.tintGroupType=4
										AND ISNULL(UserMaster.bitActivateFlag,0)=1
										AND UserMaster.numUserId <> ISNULL(@numUserID,0)),0)
		
	IF ISNULL(@Active,0)=1 AND ISNULL((SELECT tintGroupType FROM AuthenticationGroupMaster WHERE numGroupID=@numGroupId),0)=1 AND EXISTS (SELECT numSubscriberID FROM Subscribers WHERE numTargetDomainID=@numDomainID AND (@numNewFullUsers + 1) > ISNULL(intNoofUsersSubscribed,0))
	BEGIN
		RAISERROR('FULL_USERS_EXCEED',16,1)
		RETURN
	END
	ELSE IF ISNULL(@Active,0)=1 AND ISNULL((SELECT tintGroupType FROM AuthenticationGroupMaster WHERE numGroupID=@numGroupId),0)=4 AND EXISTS (SELECT numSubscriberID FROM Subscribers WHERE numTargetDomainID=@numDomainID AND (@numNewLimitedAccessUsers + 1) > ISNULL(intNoofLimitedAccessUsers,0))
	BEGIN
		RAISERROR('LIMITED_ACCESS_USERS_EXCEED',16,1)
		RETURN
	END

	IF @numUserID=0             
	BEGIN 
		DECLARE @APIPublicKey varchar(20)
		SELECT @APIPublicKey = dbo.GenerateBizAPIPublicKey() 
           
		INSERT INTO UserMaster
		(
			vcUserName
			,vcUserDesc
			,numGroupId
			,numUserDetailId
			,numModifiedBy
			,bintModifiedDate
			,vcEmailID
			,vcPassword
			,numDomainID
			,numCreatedBy
			,bintCreatedDate
			,bitActivateFlag
			,numDefaultClass
			,numDefaultWarehouse
			,vcBizAPIPublicKey
			,vcBizAPISecretKey
			,bitBizAPIAccessEnabled
			,vcLinkedinId
			,intAssociate
			,bitPayroll
			,monHourlyRate
			,tintPayrollType
			,tintHourType
			,monOverTimeRate
			,bitOauthImap
			,tintMailProvider
		)
		VALUES
		(
			@vcUserName
			,@vcUserDesc
			,@numGroupId
			,@numUserDetailID
			,@numUserCntID
			,GETUTCDATE()
			,@vcEmail
			,@vcPassword
			,@numDomainID
			,@numUserCntID
			,GETUTCDATE()
			,@Active
			,@numDefaultClass
			,@numDefaultWarehouse
			,@APIPublicKey
			,NEWID()
			,0
			,@vcLinkedinId
			,@intAssociate
			,@bitPayroll
			,@monHourlyRate
			,@tintPayrollType
			,@tintHourType
			,(CASE WHEN ISNULL(@monOverTimeRate,0) = 0 THEN @monHourlyRate ELSE @monOverTimeRate END)
			,@bitOauthImap
			,ISNULL(@tintMailProvider,3)
		)            
		
		SET @numUserID=@@identity          
           
		INSERT INTO BizAPIThrottlePolicy 
		(
			[RateLimitKey],
			[bitIsIPAddress],
			[numPerSecond],
			[numPerMinute],
			[numPerHour],
			[numPerDay],
			[numPerWeek]
		)
		VALUES
		(
			@APIPublicKey,
			0,
			3,
			60,
			1200,
			28800,
			200000
		)
    
		EXECUTE [dbo].[Resource_Add]   @vcUserName  ,@vcUserDesc  ,@vcEmail  ,1  ,@numDomainID  ,@numUserDetailID  
	END
	ELSE            
	BEGIN
		UPDATE 
			UserMaster 
		SET 
			vcUserName = @vcUserName,                                    
			vcUserDesc = @vcUserDesc,                              
			numGroupId =@numGroupId,                                    
			numUserDetailId = @numUserDetailID ,                              
			numModifiedBy= @numUserCntID ,                              
			bintModifiedDate= getutcdate(),                          
			vcEmailID=@vcEmail,            
			vcPassword=@vcPassword,          
			bitActivateFlag=@Active,
			numDefaultClass=@numDefaultClass,
			numDefaultWarehouse=@numDefaultWarehouse,
			vcLinkedinId=@vcLinkedinId,
			intAssociate=@intAssociate
			,bitPayroll=@bitPayroll
			,monHourlyRate=@monHourlyRate
			,tintPayrollType=@tintPayrollType
			,tintHourType=@tintHourType
			,monOverTimeRate=(CASE WHEN ISNULL(@monOverTimeRate,0) = 0 THEN @monHourlyRate ELSE @monOverTimeRate END)
			,bitOauthImap=@bitOauthImap
			,tintMailProvider = ISNULL(@tintMailProvider,3)
		WHERE 
			numUserID = @numUserID          
          
  
		IF NOT EXISTS (SELECT * FROM RESOURCE WHERE numUserCntId = @numUserDetailID and numdomainId = @numDomainId)  
		BEGIN   
			EXECUTE [dbo].[Resource_Add]   @vcUserName  ,@vcUserDesc  ,@vcEmail  ,1  ,@numDomainID  ,@numUserDetailID     
		END      
	END
                       
	DECLARE @separator_position AS INTEGER
	DECLARE @strPosition AS VARCHAR(1000)                 
                                   
	DELETE FROM UserTerritory WHERE numUserCntID = @numUserDetailID and numDomainId=@numDomainID                              
          
	WHILE PATINDEX('%,%' , @strTerritory) <> 0                              
    BEGIN -- Begin for While Loop                              
		SELECT @separator_position = PATINDEX('%,%' , @strTerritory)                        
		SELECT @strPosition = LEFT(@strTerritory, @separator_position - 1)      
		SELECT @strTerritory = STUFF(@strTerritory, 1, @separator_position,'')                              
     
		INSERT INTO UserTerritory 
		(
			numUserCntID
			,numTerritoryID
			,numDomainID
		)                              
		VALUES
		(
			@numUserDetailID
			,@strPosition
			,@numDomainID
		)                        
	END                              
                                           
	DELETE FROM UserTeams WHERE numUserCntID = @numUserDetailID AND numDomainId=@numDomainID                              
                               
	WHILE PATINDEX('%,%' , @strTeam) <> 0                              
    BEGIN -- Begin for While Loop                              
		SELECT @separator_position =  PATINDEX('%,%' , @strTeam)                              
		SELECT @strPosition = left(@strTeam, @separator_position - 1)                  
		SELECT @strTeam = stuff(@strTeam, 1, @separator_position,'')                              
     
		INSERT INTO UserTeams 
		(
			numUserCntID
			,numTeam
			,numDomainID
		)                              
		VALUES 
		(
			@numUserDetailID
			,@strPosition
			,@numDomainID
		)                        
	END              
                
                                      
                              
	DELETE FROM 
		ForReportsByTeam 
	WHERE 
		numUserCntID = @numUserDetailID                              
		AND numDomainId=@numDomainID AND numTeam NOT IN (SELECT numTeam FROM UserTeams WHERE numUserCntID = @numUserCntID AND numDomainId=@numDomainID)                                                
END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USp_UpdateUserSMTP')
DROP PROCEDURE USp_UpdateUserSMTP
GO
CREATE PROCEDURE  USp_UpdateUserSMTP
@numUserID NUMERIC(9),                                     
@SMTPAuth as bit   ,
@SMTPSSL as bit   ,
@vcSMTPPassword as varchar(100), 
@SMTPServer as varchar(200), 
@SMTPPort  as numeric(9),
@bitSMTPServer as bit

AS                                    
BEGIN

UPDATE UserMaster SET [bitSMTPAuth]=@SMTPAuth
      ,[vcSmtpPassword]=@vcSMTPPassword
      ,[vcSMTPServer]=@SMTPServer
      ,[numSMTPPort]=@SMTPPort
      ,[bitSMTPSSL]  =  @SMTPSSL ,bitSMTPServer=@bitSMTPServer,tintMailProvider=3                        
    WHERE numUserID = @numUserID  


END
/****** Object:  StoredProcedure [dbo].[USP_LeadConversionReport]    Script Date: 07/26/2008 16:19:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Create by Siva           
             
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ItemListForContainer')
DROP PROCEDURE USP_ItemListForContainer
GO
CREATE PROCEDURE [dbo].[USP_ItemListForContainer]            
@numDomainID numeric=0           
As                     
Begin                    
       SELECT 
			numItemCode,vcItemName
	   FROM
			Item WITH (NOLOCK)
		WHERE
			numDomainID=@numDomainID AND ISNULL(bitContainer,0)=1
End
GO

GO
/****** Object:  StoredProcedure [dbo].[usp_UpdateStagePercentageDetails]    Script Date: 09/21/2010 12:00:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----Created By Prasanta    
    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageProcesstoOpportunity')
DROP PROCEDURE USP_ManageProcesstoOpportunity
GO
CREATE PROCEDURE [dbo].[USP_ManageProcesstoOpportunity]
@numDomainID as numeric(9)=0,    
@numProcessId as numeric(18)=0,      
@numCreatedBy as numeric(18)=0,
@numOppId AS NUMERIC(18,0)=0,   
@numProjectId AS NUMERIC(18,0)=0
as    
BEGIN 
 INSERT  INTO Sales_process_List_Master ( Slp_Name,
                                                 numdomainid,
                                                 pro_type,
                                                 numCreatedby,
                                                 dtCreatedon,
                                                 numModifedby,
                                                 dtModifiedOn,tintConfiguration,bitAutomaticStartTimer,bitAssigntoTeams,numOppId,numTaskValidatorId,numProjectId)
     SELECT Slp_Name,
            numdomainid,
            pro_type,
            numCreatedby,
            dtCreatedon,
            numModifedby,
            dtModifiedOn,tintConfiguration,bitAutomaticStartTimer,bitAssigntoTeams,@numOppId,numTaskValidatorId,@numProjectId FROM Sales_process_List_Master WHERE Slp_Id=@numProcessId    
	DECLARE @numNewProcessId AS NUMERIC(18,0)=0
	SET @numNewProcessId=(SELECT SCOPE_IDENTITY())  
	IF(@numOppId>0)
	BEGIN
		UPDATE OpportunityMaster SET numBusinessProcessID=@numNewProcessId WHERE numOppId=@numOppId
	END
	IF(@numProjectId>0)
	BEGIN
		UPDATE ProjectsMaster SET numBusinessProcessID=@numNewProcessId WHERE numProId=@numProjectId
	END

	INSERT INTO StagePercentageDetails(
		numStagePercentageId, 
		tintConfiguration, 
		vcStageName, 
		numDomainId, 
		numCreatedBy, 
		bintCreatedDate, 
		numModifiedBy, 
		bintModifiedDate, 
		slp_id, 
		numAssignTo, 
		vcMileStoneName, 
		tintPercentage, 
		tinProgressPercentage, 
		dtStartDate, 
		numParentStageID, 
		intDueDays, 
		numProjectID, 
		numOppID, 
		vcDescription, 
		bitIsDueDaysUsed,
		numTeamId, 
		bitRunningDynamicMode,
		numStageOrder,
		numParentStageDetailsId
	)
	SELECT
		numStagePercentageId, 
		tintConfiguration, 
		vcStageName, 
		numDomainId, 
		@numCreatedBy, 
		GETDATE(), 
		@numCreatedBy, 
		GETDATE(), 
		@numNewProcessId, 
		numAssignTo, 
		vcMileStoneName, 
		tintPercentage, 
		tinProgressPercentage, 
		GETDATE(), 
		numParentStageID, 
		intDueDays, 
		@numProjectId, 
		@numOppId, 
		vcDescription, 
		bitIsDueDaysUsed,
		numTeamId, 
		bitRunningDynamicMode,
		numStageOrder,
		numStageDetailsId
	FROM
		StagePercentageDetails
	WHERE
		slp_id=@numProcessId	

	INSERT INTO StagePercentageDetailsTask(
		numStageDetailsId, 
		vcTaskName, 
		numHours, 
		numMinutes, 
		numAssignTo, 
		numDomainID, 
		numCreatedBy, 
		dtmCreatedOn,
		numOppId,
		numProjectId,
		numParentTaskId,
		bitDefaultTask,
		bitSavedTask,
		numOrder,
		numReferenceTaskId
	)
	SELECT 
		CASE WHEN @numOppId>0
		THEN
		(SELECT TOP 1 SFD.numStageDetailsId FROM StagePercentageDetails AS SFD WHERE SFD.vcStageName=SP.vcStageName AND SFD.numOppID=@numOppId AND SFD.numParentStageDetailsId=SP.numStageDetailsId)
		WHEN @numProjectId>0
		THEN
		(SELECT TOP 1 SFD.numStageDetailsId FROM StagePercentageDetails AS SFD WHERE SFD.vcStageName=SP.vcStageName AND SFD.numProjectID=@numProjectId AND SFD.numParentStageDetailsId=SP.numStageDetailsId) ELSE 0 END,
		vcTaskName,
		numHours,
		numMinutes,
		ST.numAssignTo,
		@numDomainID,
		@numCreatedBy,
		GETDATE(),
		@numOppId,
		@numProjectId,
		0,
		1,
		CASE WHEN SP.bitRunningDynamicMode=1 THEN 0 ELSE 1 END,
		numOrder,
		numTaskId
	FROM 
		StagePercentageDetailsTask AS ST
	LEFT JOIN
		StagePercentageDetails As SP
	ON
		ST.numStageDetailsId=SP.numStageDetailsId
	WHERE
		ISNULL(ST.numOppId,0)=0 AND ISNULL(ST.numProjectId,0)=0 AND
		ST.numStageDetailsId IN(SELECT numStageDetailsId FROM 
		StagePercentageDetails As ST
	WHERE
		ST.slp_id=@numProcessId)
	ORDER BY ST.numOrder

END 

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_Project_GetMilestones')
DROP PROCEDURE USP_Project_GetMilestones
GO
CREATE PROCEDURE [dbo].[USP_Project_GetMilestones]
(
	@numDomainID NUMERIC(18,0)
	,@numProId NUMERIC(18,0)
)
AS 
BEGIN
	SELECT 
		ROW_NUMBER() OVER(ORDER BY vcMileStoneName ASC) numMileStoneID
		,vcMileStoneName
		,CONCAT(StagePercentageDetails.vcMileStoneName,' (',ISNULL(StagePercentageMaster.numStagePercentage,0),'% of total)') vcMileStone
		,dbo.GetTotalProgress(@numDomainID,@numProId,2,2,vcMileStoneName,0) numTotalProgress
		,(SELECT TOP 1 intTotalProgress FROM ProjectProgress where numProId=@numProId AND numDomainID=@numDomainID) AS numTotalCompletedProgress
	FROM 
		ProjectsMaster
	INNER JOIN
		StagePercentageDetails
	ON
		ProjectsMaster.numBusinessProcessId = StagePercentageDetails.slp_id
	LEFT JOIN
		StagePercentageMaster
	ON
		StagePercentageDetails.numStagePercentageId = StagePercentageMaster.numStagePercentageId
	WHERE
		ProjectsMaster.numDomainID = @numDomainID
		AND ProjectsMaster.numProId = @numProId
	GROUP BY
		vcMileStoneName
		,StagePercentageMaster.numStagePercentage
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_Project_GetStages')
DROP PROCEDURE USP_Project_GetStages
GO
CREATE PROCEDURE [dbo].[USP_Project_GetStages]
(
	@numDomainID NUMERIC(18,0)
	,@numProId NUMERIC(18,0)
	,@numMileStoneID NUMERIC(18,0)
	,@vcMilestoneName VARCHAR(500)
)
AS 
BEGIN
	SELECT 
		@numMileStoneID numMileStoneID
		,StagePercentageDetails.numStageDetailsId
		,StagePercentageDetails.vcStageName AS vcStageName
		,dbo.GetTotalProgress(@numDomainID,@numProId,2,3,'',StagePercentageDetails.numStageDetailsId) numTotalProgress
	FROM
		StagePercentageDetails
	WHERE
		StagePercentageDetails.numDomainID = @numDomainID
		AND StagePercentageDetails.numProjectID = @numProId
		AND vcMileStoneName = @vcMilestoneName
END

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_Project_GetTasks')
DROP PROCEDURE USP_Project_GetTasks
GO
CREATE PROCEDURE [dbo].[USP_Project_GetTasks]
(
	@numDomainID NUMERIC(18,0)
	,@numProId NUMERIC(18,0)
	,@numMileStoneID NUMERIC(18,0)
	,@numStageDetailsId NUMERIC(18,0)
	,@ClientTimeZoneOffset INT
	,@tintMode TINYINT
)
AS 
BEGIN
	IF @tintMode = 1
	BEGIN
		DECLARE @numQtyToBuild FLOAT
		DECLARE @dtPlannedStartDate DATETIME
		SELECT 
			@numQtyToBuild=1
			,@dtPlannedStartDate=ISNULL(ProjectsMaster.dtmStartDate,ProjectsMaster.bintCreatedDate) 
		FROM 
			ProjectsMaster 
		WHERE
			ProjectsMaster.numDomainID=@numDomainID 
			AND ProjectsMaster.numProId=@numProId

		DECLARE @TempTaskAssignee TABLE
		(
			numAssignedTo NUMERIC(18,0)
			,dtLastTaskCompletionTime DATETIME
		)

		DECLARE @TempTasks TABLE
		(
			ID INT IDENTITY(1,1)
			,numTaskID NUMERIC(18,0)
			,numTaskTimeInMinutes NUMERIC(18,0)
			,numTaskAssignee NUMERIC(18,0)
			,intTaskType INT --1:Parallel, 2:Sequential
			,dtPlannedStartDate DATETIME
		)

		INSERT INTO @TempTasks
		(
			numTaskID
			,numTaskTimeInMinutes
			,intTaskType
			,numTaskAssignee
		)
		SELECT 
			SPDT.numTaskID
			,((ISNULL(SPDT.numHours,0) * 60) + ISNULL(SPDT.numMinutes,0)) * @numQtyToBuild
			,ISNULL(SPDT.intTaskType,1)
			,SPDT.numAssignTo
		FROM 
			StagePercentageDetailsTask SPDT
		INNER JOIN
			ProjectsMaster PO
		ON
			SPDT.numProjectId = PO.numProId
		WHERE 
			SPDT.numDomainId=@numDomainID
			AND SPDT.numProjectId = @numProId

		UPDATE @TempTasks SET dtPlannedStartDate=@dtPlannedStartDate

		INSERT INTO @TempTaskAssignee
		(
			numAssignedTo
		)
		SELECT DISTINCT
			numTaskAssignee
		FROM
			@TempTasks

		DECLARE @i INT = 1
		DECLARE @iCount INT 	
		SELECT @iCount = COUNT(*) FROM @TempTasks

		DECLARE @numTaskID NUMERIC(18,0)
		DECLARE @numTaskAssignee NUMERIC(18,0)
		DECLARE @intTaskType INT
		DECLARE @numWorkScheduleID NUMERIC(18,0)
		DECLARE @numTempUserCntID NUMERIC(18,0)
		DECLARE @dtStartDate DATETIME
		DECLARE @numTotalTaskInMinutes NUMERIC(18,0)
		DECLARE @numProductiveTimeInMinutes NUMERIC(18,0)
		DECLARE @tmStartOfDay TIME(7)
		DECLARE @numTimeLeftForDay NUMERIC(18,0)
		DECLARE @vcWorkDays VARCHAR(20)
		DECLARE @bitParallelStartSet BIT = 0

		WHILE @i <= @iCount
		BEGIN
			SELECT
				@numTaskID=numTaskID
				,@numTaskAssignee=numTaskAssignee
				,@numTotalTaskInMinutes=numTaskTimeInMinutes
				,@intTaskType = intTaskType
			FROM
				@TempTasks 
			WHERE
				ID=@i

			IF NOT EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID=@numTaskID AND tintAction=4)
			BEGIN
				-- GET TASK COMPLETION TIME WHICH WILL BE START DATETIME FOR NEXT TASK
				SELECT
					@numWorkScheduleID = WS.ID
					,@numProductiveTimeInMinutes=(ISNULL(WS.numProductiveHours,0) * 60) + ISNULL(WS.numProductiveMinutes,0)
					,@tmStartOfDay = tmStartOfDay
					,@vcWorkDays=vcWorkDays
					,@dtStartDate = DATEADD(MINUTE,@ClientTimeZoneOffset,CONVERT(DATETIME,CONVERT(VARCHAR(8),DATEADD(MINUTE,-@ClientTimeZoneOffset,@dtPlannedStartDate),112)) + tmStartOfDay)
				FROM
					WorkSchedule WS
				INNER JOIN
					UserMaster
				ON
					WS.numUserCntID = UserMaster.numUserDetailId
				WHERE 
					WS.numUserCntID = @numTaskAssignee

				IF @intTaskType=1 AND EXISTS (SELECT numAssignedTo FROM @TempTaskAssignee WHERE numAssignedTo = @numTaskAssignee AND dtLastTaskCompletionTime IS NOT NULL)
				BEGIN
					SELECT @dtStartDate = dtLastTaskCompletionTime FROM @TempTaskAssignee WHERE numAssignedTo = @numTaskAssignee
				END
				ELSE IF EXISTS (SELECT numAssignedTo FROM @TempTaskAssignee WHERE numAssignedTo = @numTaskAssignee AND dtLastTaskCompletionTime IS NOT NULL)
				BEGIN
					SELECT @dtStartDate = dtLastTaskCompletionTime FROM @TempTaskAssignee WHERE numAssignedTo = @numTaskAssignee
				END

				UPDATE @TempTasks SET dtPlannedStartDate=@dtStartDate WHERE ID=@i 

				IF @numProductiveTimeInMinutes > 0 AND @numTotalTaskInMinutes > 0
				BEGIN
					WHILE @numTotalTaskInMinutes > 0
					BEGIN
						-- IF ITS WORKING DAY AND ALSO NOT HOLIDAY THEN PROCEED OR INCRESE DATE TO NEXT DAY
						IF DATEPART(WEEKDAY,@dtStartDate) IN (SELECT Id FROM dbo.SplitIDs(@vcWorkDays,',')) AND NOT EXISTS (SELECT ID FROM WorkScheduleDaysOff WHERE numWorkScheduleID=@numWorkScheduleID AND CAST(DATEADD(MINUTE,@ClientTimeZoneOffset * -1,@dtStartDate) AS DATE) BETWEEN dtDayOffFrom AND dtDayOffTo)
						BEGIN
							IF CAST(@dtStartDate AS DATE) = CAST(GETUTCDATE() AS DATE)
							BEGIN
								-- CHECK TIME LEFT FOR DAY BASED
								SET @numTimeLeftForDay = DATEDIFF(MINUTE,@dtStartDate,DATEADD(MINUTE,@numProductiveTimeInMinutes,DATEADD(MINUTE,@ClientTimeZoneOffset,CONVERT(DATETIME,CONVERT(VARCHAR(8),DATEADD(MINUTE,-@ClientTimeZoneOffset,@dtStartDate),112)) + @tmStartOfDay)))
							END
							ELSE
							BEGIN
								SET @numTimeLeftForDay = @numProductiveTimeInMinutes
							END

							IF @numTimeLeftForDay > 0
							BEGIN
								IF @numTimeLeftForDay > @numTotalTaskInMinutes
								BEGIN
									SET @dtStartDate = DATEADD(MINUTE,@numTotalTaskInMinutes,@dtStartDate)
								END
								ELSE
								BEGIN
									SET @dtStartDate = DATEADD(MINUTE,@ClientTimeZoneOffset,CONVERT(DATETIME,CONVERT(VARCHAR(8),DATEADD(MINUTE,-@ClientTimeZoneOffset,DATEADD(DAY,1,@dtStartDate)),112)) + @tmStartOfDay)
								END

								SET @numTotalTaskInMinutes = @numTotalTaskInMinutes - @numTimeLeftForDay
							END
							ELSE
							BEGIN
								SET @dtStartDate = DATEADD(MINUTE,@ClientTimeZoneOffset,CONVERT(DATETIME,CONVERT(VARCHAR(8),DATEADD(MINUTE,-@ClientTimeZoneOffset,DATEADD(DAY,1,@dtStartDate)),112)) + @tmStartOfDay)
							END
						END
						ELSE
						BEGIN
							SET @dtStartDate = DATEADD(MINUTE,@ClientTimeZoneOffset,CONVERT(DATETIME,CONVERT(VARCHAR(8),DATEADD(MINUTE,-@ClientTimeZoneOffset,DATEADD(DAY,1,@dtStartDate)),112)) + @tmStartOfDay)					
						END				
					END
				END	

				UPDATE @TempTaskAssignee SET dtLastTaskCompletionTime=@dtStartDate WHERE numAssignedTo=@numTaskAssignee
			END

			SET @i = @i + 1
		END

		SELECT	
			@numMileStoneID numMileStoneID
			,@numStageDetailsId numStageDetailsId
			,SPDT.numTaskId
			,SPDT.numReferenceTaskId
			,dbo.fn_GetContactName(ACustomer.numContactId) vcCustomerName
			,SPDT.vcTaskName
			,ISNULL(ACustomer.vcEmail,'') AS vcEmail
			,SPDT.numAssignTo AS numAssignedTo 
			,ISNULL(SPDT.bitTimeAddedToContract,0) AS bitTimeAddedToContract
			,dbo.fn_GetContactName(SPDT.numAssignTo) vcAssignedTo
			,ISNULL(L.vcData,'-') AS vcWorkStation
			,ISNULL((SELECT SUM(numProcessedQty) FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId),0) AS numProcessedQty
			,1 - ISNULL((SELECT SUM(numProcessedQty) FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId),0) AS numRemainingQty
			,CASE ISNULL((SELECT TOP 1 tintAction FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId ORDER BY dtActionTime DESC,ID DESC),0) 
				WHEN 4 THEN '<img src="../images/comflag.png" />'
				WHEN 3 THEN CONCAT('<ul class="list-inline" style="margin-bottom:0px;"><li style="vertical-align:middle;padding-left:0px;padding-right:0px;"><button class="btn btn-xs btn-info" onclick="return ShowTaskTimeLogWindowProject(',SPDT.numTaskId,');"><i class="fa fa-clock-o"></i></button></li><li style="vertical-align:middle;padding-right:0px;"><button class="btn btn-xs btn-flat btn-warning" onclick="return ShowTaskPausedWindowProject(this,',SPDT.numTaskId,');">Pause</button></li><li style="vertical-align:middle;padding-right:0px;"><button class="btn btn-flat btn-task-finish" onclick="return TaskFinishedProject(this,',SPDT.numTaskId,',4);"><img src="../images/comflag.png" />&nbsp;Finish</button></li><li style="vertical-align:middle;padding-right:0px;"><label class="taskTimerInitial"></label></li></ul>')
				WHEN 2 THEN CONCAT('<ul class="list-inline" style="margin-bottom:0px;"><li style="vertical-align:middle;padding-left:0px;padding-right:0px;"><button class="btn btn-xs btn-info" onclick="return ShowTaskTimeLogWindowProject(',SPDT.numTaskId,');"><i class="fa fa-clock-o"></i></button></li><li style="vertical-align:middle;padding-right:0px;"><button class="btn btn-xs btn-flat bg-purple" onclick="return TaskStartedProject(this,',SPDT.numTaskId,',1);">Resume</button></li><li style="vertical-align:middle;padding-right:0px;"><button class="btn btn-flat btn-task-finish" onclick="return TaskFinishedProject(this,',SPDT.numTaskId,',4);"><img src="../images/comflag.png" />&nbsp;Finish</button></li></ul>')
				WHEN 1 THEN CONCAT('<ul class="list-inline" style="margin-bottom:0px;"><li style="vertical-align:middle;padding-left:0px;padding-right:0px;"><button class="btn btn-xs btn-info" onclick="return ShowTaskTimeLogWindowProject(',SPDT.numTaskId,');"><i class="fa fa-clock-o"></i></button></li><li style="vertical-align:middle;padding-right:0px;"><button class="btn btn-xs btn-flat btn-warning" onclick="return ShowTaskPausedWindowProject(this,',SPDT.numTaskId,');">Pause</button></li><li style="vertical-align:middle;padding-right:0px;"><button class="btn btn-flat btn-task-finish" onclick="return TaskFinishedProject(this,',SPDT.numTaskId,',4);"><img src="../images/comflag.png" />&nbsp;Finish</button></li><li style="vertical-align:middle;padding-right:0px;"><label class="taskTimerInitial"></label></li></ul>')
				ELSE CONCAT('<ul class="list-inline" style="margin-bottom:0px;"><li style="vertical-align:middle;padding-left:0px;padding-right:0px;"><button class="btn btn-xs btn-info" onclick="return ShowTaskTimeLogWindowProject(',SPDT.numTaskId,');"><i class="fa fa-clock-o"></i></button></li><li style="vertical-align:middle;padding-right:0px;"><button class="btn btn-xs btn-flat btn-success btn-task-start" onclick="return TaskStartedProject(this,',SPDT.numTaskId,',0);">Start</button></li><li style="vertical-align:middle;padding-right:0px;"><button class="btn btn-flat btn-task-finish" onclick="return TaskFinishedProject(this,',SPDT.numTaskId,',4);"><img src="../images/comflag.png" />&nbsp;Finish</button></li></ul>')
			END  AS vcTaskControls
			,FORMAT((((ISNULL(SPDT.numHours,0) * 60) + ISNULL(SPDT.numMinutes,0)) * @numQtyToBuild) / 60,'00') + ':' + FORMAT((((ISNULL(SPDT.numHours,0) * 60) + ISNULL(SPDT.numMinutes,0)) * CAST(@numQtyToBuild AS DECIMAL)) % 60.0,'00')  vcEstimatedTaskTime
			,(((ISNULL(SPDT.numHours,0) * 60) + ISNULL(SPDT.numMinutes,0)) * @numQtyToBuild) numTaskEstimationInMinutes
			,(CASE ISNULL((SELECT TOP 1 tintAction FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID=SPDT.numTaskId ORDER BY SPDTTL.dtActionTime DESC,SPDTTL.ID DESC),0) 
				WHEN 4 THEN 0
				WHEN 3 THEN dbo.GetTimeSpendOnTaskByProject(SPDT.numDomainID,SPDT.numTaskId,2)
				WHEN 2 THEN dbo.GetTimeSpendOnTaskByProject(SPDT.numDomainID,SPDT.numTaskId,2)
				WHEN 1 THEN 0
			END) numTimeSpentInMinutes
			,(CASE ISNULL((SELECT TOP 1 tintAction FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId ORDER BY dtActionTime DESC,ID DESC),0) 
				WHEN 4 THEN NULL
				WHEN 3 THEN DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,(SELECT TOP 1 dtActionTime FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID=SPDT.numTaskId ORDER BY SPDTTL.dtActionTime DESC,SPDTTL.ID DESC))
				WHEN 2 THEN NULL
				WHEN 1 THEN DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,(SELECT TOP 1 dtActionTime FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID=SPDT.numTaskId ORDER BY SPDTTL.dtActionTime DESC,SPDTTL.ID DESC))
			END) dtLastStartDate
			,(CASE WHEN dbo.GetTimeSpendOnTaskByProject(SPDT.numDomainID,SPDT.numTaskId,0) = 'Invalid time sequence' THEN '00:00' ELSE dbo.GetTimeSpendOnTaskByProject(SPDT.numDomainID,SPDT.numTaskId,0) END) vcActualTaskTime
			,CASE WHEN EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId AND tintAction=4)
				THEN dbo.GetTimeSpendOnTaskByProject(SPDT.numDomainID,SPDT.numTaskId,1)
				ELSE ''
			END vcActualTaskTimeHtml
			,CASE 
				WHEN EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId AND tintAction=1)
				THEN CONCAT('<span>',dbo.FormatedDateTimeFromDate(DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,(SELECT dtActionTime FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId AND tintAction=1)),@numDomainID),'</span>')
				ELSE CONCAT('<i style="color:#a6a6a6">',dbo.FormatedDateTimeFromDate((CASE
										WHEN TT.dtPlannedStartDate IS NULL
										THEN DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,ISNULL(PO.dtmStartDate,PO.bintCreatedDate))
										ELSE DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,TT.dtPlannedStartDate)
									END),@numDomainID),' (planned)</i>')
			END dtPlannedStartDate
			,CASE WHEN EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId AND tintAction=1)
				THEN DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,(SELECT dtActionTime FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId AND tintAction=1))
				ELSE ''
			END AS dtStartDate
			,CASE WHEN EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId AND tintAction=4)
				THEN DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,(SELECT dtActionTime FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId AND tintAction=4))
				ELSE ''
			END AS dtFinishDate
			,CASE WHEN EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId AND tintAction=4)
				THEN dbo.FormatedDateTimeFromDate(DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,(SELECT dtActionTime FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId AND tintAction=4)),@numDomainID)
				ELSE ''
			END AS vcFinishDate
			,CASE WHEN EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId AND tintAction=4)
				THEN 1
				ELSE 0
			END AS bitTaskCompleted
			,(SELECT COUNT(*) FROM TopicMaster WHERE intRecordType=4 AND numRecordId=SPDT.numTaskId) AS TopicCount
			,(CASE 
				WHEN EXISTS (SELECT ID FROM StagePercentageDetailsTaskNotes SPDTN WHERE SPDTN.numTaskID=SPDT.numTaskId) 
				THEN CONCAT('<ul class="list-inline"><li><i class="fa fa-file-text-o" style="font-size: 23px;" onclick="return OpenTaskNotes(',SPDT.numTaskId,')"></i></li>',(CASE WHEN EXISTS (SELECT ID FROM StagePercentageDetailsTaskNotes SPDTN WHERE SPDTN.numTaskID=SPDT.numTaskId AND ISNULL(SPDTN.bitDone,0) = 1) THEN '<li><i class="fa fa-check-circle text-green" style="font-size: 23px;" aria-hidden="true"></i></li>' ELSE '' END),'</ul>') 
				ELSE CONCAT('<i class="fa fa-file-o" style="font-size: 23px;" onclick="return OpenTaskNotes(',SPDT.numTaskId,')"></i>') 
			END) vcNotesLink
		FROM 
			StagePercentageDetailsTask SPDT
		INNER JOIN
			ProjectsMaster PO
		ON
			SPDT.numProjectId = PO.numProId
		LEFT JOIN
			@TempTasks TT
		ON
			SPDT.numTaskId = TT.numTaskID
		LEFT JOIN
			AdditionalContactsInformation AS A
		ON
			SPDT.numAssignTo=A.numContactId
		LEFT JOIN 
			AdditionalContactsInformation ACustomer
		ON 
			PO.numCustPrjMgr = ACustomer.numContactId
		LEFT JOIN
			ListDetails AS L
		ON
			A.numTeam=L.numListItemID
		WHERE
			SPDT.numStageDetailsId = @numStageDetailsId
			AND SPDT.numProjectId = @numProId
	END
	ELSE IF @tintMode = 2
	BEGIN
		SELECT	
			@numMileStoneID numMileStoneID
			,SPDT.numTaskId
			,SPDT.numReferenceTaskId
			,SPDT.numStageDetailsId
			,SPDT.vcTaskName
			,ISNULL(SPDT.numHours,0) numHours
			,ISNULL(SPDT.numMinutes,0) numMinutes
			,SPDT.numAssignTo
			,SPDT.numAssignTo AS numAssignedTo 
			,ISNULL(SPDT.bitTimeAddedToContract,0) AS bitTimeAddedToContract
			,ADC.numTeam
			,CONCAT(FORMAT(ISNULL(SPDT.numHours,0),'00'),':',FORMAT(ISNULL(SPDT.numMinutes,0),'00')) vcEstimatedTaskTime
			,(CASE WHEN dbo.GetTimeSpendOnTaskByProject(SPDT.numDomainID,SPDT.numTaskId,0) = 'Invalid time sequence' THEN '00:00' ELSE dbo.GetTimeSpendOnTaskByProject(SPDT.numDomainID,SPDT.numTaskId,0) END) vcActualTaskTime
			,CASE WHEN EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId AND tintAction=1)
				THEN DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,(SELECT dtActionTime FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId AND tintAction=1))
				ELSE ''
			END AS dtStartDate
			,CASE WHEN EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId AND tintAction=4)
				THEN DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,(SELECT dtActionTime FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId AND tintAction=4))
				ELSE ''
			END AS dtFinishDate
			,CASE WHEN EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=SPDT.numTaskId)
				THEN 1
				ELSE 0
			END bitTaskStarted,
			(SELECT COUNT(*) FROM TopicMaster WHERE intRecordType=4 AND numRecordId=SPDT.numTaskId) AS TopicCount
		FROM 
			StagePercentageDetailsTask SPDT
		INNER JOIN
			ProjectsMaster PO
		ON
			SPDT.numProjectId = PO.numProId
		LEFT JOIN
			AdditionalContactsInformation ADC
		ON
			SPDT.numAssignTo = ADC.numContactId
		LEFT JOIN
			@TempTasks TT
		ON
			SPDT.numTaskId = TT.numTaskID
		WHERE
			SPDT.numStageDetailsId = @numStageDetailsId
			AND SPDT.numProjectId = @numProId
	END
END
