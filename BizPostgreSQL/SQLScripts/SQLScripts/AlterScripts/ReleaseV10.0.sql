/******************************************************************
Project: Release 10.0 Date: 25.JUL.2018
Comments: ALTER SCRIPTS
*******************************************************************/

/*************************** SANDEEP *******************************/

-- DO NOT EXECUTE ON SERVER
INSERT INTO DynamicFormMaster
(
	numFormId,vcFormName,cCustomFieldsAssociated,cAOIAssociated,bitDeleted,tintFlag
)
VALUES
(
	136,'Import Sales Order(s)','N','N',0,2
),
(
	137,'Import Purchase Order(s)','N','N',0,2
)


INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,numFormID
)
VALUES
(
	1,539,'Organization ID','Label','DivisionID',1,1,1,136
),
(
	1,3,'Organization Name','TextBox','CompanyName',1,1,1,136
),
(
	1,5,'Organization Profile','SelectBox','Profile',1,1,1,136
),
(
	1,6,'Relationship (e.g. Customer, Vendor, etc�)','SelectBox','CompanyType',1,1,1,136
),
(
	1,10,'Organization Phone','TextBox','ComPhone',1,1,1,136
),
(
	1,14,'Organization Assigned To','SelectBox','AssignedTo',1,1,1,136
),
(
	1,21,'Territory','SelectBox','TerritoryID',1,1,1,136
),
(
	1,28,'Industry','SelectBox','CompanyIndustry',1,1,1,136
),
(
	1,30,'Organization Status','SelectBox','StatusID',1,1,1,136
),
(
	1,451,'Relationship Type (e.g. Leads, Prospect or Accounts)','SelectBox','CRMType',1,1,1,136
),
(
	1,217,'Ship To Street','TextBox','SStreet',1,1,1,136
),
(
	1,218,'Ship To City','TextBox','SCity',1,1,1,136
),
(
	1,219,'Ship To State','SelectBox',NULL,1,1,1,136
),
(
	1,220,'Ship To Postal Code','TextBox','SPostalCode',1,1,1,136
),
(
	1,221,'Ship To Country','SelectBox',NULL,1,1,1,136
),
(
	1,222,'Bill To Street','TextBox','Street',1,1,1,136
),
(
	1,223,'Bill To City','TextBox','City',1,1,1,136
),
(
	1,224,'Bill To State','SelectBox',NULL,1,1,1,136
),
(
	1,225,'Bill To Postal Code','TextBox','PostalCode',1,1,1,136
),
(
	1,226,'Bill To Country','SelectBox','Country',1,1,1,136
),
(
	4,189,'Item Name','TextBox',NULL,1,1,1,136
),
(
	4,281,'SKU','TextBox',NULL,1,1,1,136
),
(
	4,203,'UPC','TextBox',NULL,1,1,1,136
),
(
	4,899,'Customer Part#','TextBox',NULL,1,1,1,136
),
(
	4,211,'Item ID','TextBox',NULL,1,1,1,136
),
(
	4,291,'Vendor Part#','TextBox',NULL,1,1,1,136
),
(
	3,258,'Units','TextBox',NULL,1,1,1,136
),
(
	3,259,'Unit Price','TextBox',NULL,1,1,1,136
),
(
	3,348,'UOM','SelectBox',NULL,1,1,1,136
),
(
	3,262,'Discount','TextBox',NULL,1,1,1,136
),
(
	3,257,'Drop Ship','CheckBox',NULL,1,1,1,136
),
(
	3,293,'Warehouse','SelectBox',NULL,1,1,1,136
),
(
	3,233,'Warehouse Location','SelectBox',NULL,1,1,1,136
),
(
	3,(SELECT TOP 1 numFieldId FROM DycFieldMaster WHERE vcDbColumnName='intUsedShippingCompany' AND vcLookBackTableName='OpportunityMaster'),'Ship Via (e.g. Fedex, UPS, USPS etc...)','SelectBox',NULL,1,1,1,136
),
(
	3,(SELECT TOP 1 numFieldId FROM DycFieldMaster WHERE vcDbColumnName='numShippingService' AND vcLookBackTableName='OpportunityMaster'),'Shipping Service (e.g. Fedex Ground, UPS Ground etc...)','SelectBox',NULL,1,1,1,136
),
(
	3,101,'Order Status','SelectBox',NULL,1,1,1,136
),
(
	3,742,'Cost','TextBox',NULL,1,1,1,136
),
(
	3,535,'Customer PO#','TextBox',NULL,1,1,1,136
)

INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,numFormID
)
VALUES
(
	3,914,'Is Work Order','CheckBox',NULL,1,1,1,136
)

INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,numFormID
)
VALUES
(
	3,915,'Discount Type (Percentage, Flat Amount)','TextBox',NULL,1,1,1,136
)

--------------------------------


INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,numFormID
)
VALUES
(
	1,539,'Organization ID','Label','DivisionID',1,1,1,137
),
(
	1,3,'Organization Name','TextBox','CompanyName',1,1,1,137
),
(
	1,5,'Organization Profile','SelectBox','Profile',1,1,1,137
),
(
	1,6,'Relationship (e.g. Customer, Vendor, etc�)','SelectBox','CompanyType',1,1,1,137
),
(
	1,10,'Organization Phone','TextBox','ComPhone',1,1,1,137
),
(
	1,14,'Assigned To','SelectBox','AssignedTo',1,1,1,137
),
(
	1,21,'Territory','SelectBox','TerritoryID',1,1,1,137
),
(
	1,28,'Industry','SelectBox','CompanyIndustry',1,1,1,137
),
(
	1,30,'Organization Status','SelectBox','StatusID',1,1,1,137
),
(
	1,451,'Relationship Type (e.g. Leads, Prospect or Accounts)','SelectBox','CRMType',1,1,1,137
),
(
	1,217,'Ship To Street','TextBox','SStreet',1,1,1,137
),
(
	1,218,'Ship To City','TextBox','SCity',1,1,1,137
),
(
	1,219,'Ship To State','SelectBox',NULL,1,1,1,137
),
(
	1,220,'Ship To Postal Code','TextBox','SPostalCode',1,1,1,137
),
(
	1,221,'Ship To Country','SelectBox',NULL,1,1,1,137
),
(
	1,222,'Bill To Street','TextBox','Street',1,1,1,137
),
(
	1,223,'Bill To City','TextBox','City',1,1,1,137
),
(
	1,224,'Bill To State','SelectBox',NULL,1,1,1,137
),
(
	1,225,'Bill To Postal Code','TextBox','PostalCode',1,1,1,137
),
(
	1,226,'Bill To Country','SelectBox','Country',1,1,1,137
),
(
	4,189,'Item Name','TextBox',NULL,1,1,1,137
),
(
	4,281,'SKU','TextBox',NULL,1,1,1,137
),
(
	4,203,'UPC','TextBox',NULL,1,1,1,137
),
(
	4,899,'Customer Part#','TextBox',NULL,1,1,1,137
),
(
	4,211,'Item ID','TextBox',NULL,1,1,1,137
),
(
	4,291,'Vendor Part#','TextBox',NULL,1,1,1,137
),
(
	3,258,'Units','TextBox',NULL,1,1,1,137
),
(
	3,259,'Unit Price','TextBox',NULL,1,1,1,137
),
(
	3,348,'UOM','SelectBox',NULL,1,1,1,137
),
(
	3,293,'Warehouse','SelectBox',NULL,1,1,1,137
),
(
	3,233,'Warehouse Location','SelectBox',NULL,1,1,1,137
),
(
	3,(SELECT TOP 1 numFieldId FROM DycFieldMaster WHERE vcDbColumnName='intUsedShippingCompany' AND vcLookBackTableName='OpportunityMaster'),'Ship Via(e.g. Fedex, UPS, USPS etc...)','SelectBox',NULL,1,1,1,137
),
(
	3,(SELECT TOP 1 numFieldId FROM DycFieldMaster WHERE vcDbColumnName='numShippingService' AND vcLookBackTableName='OpportunityMaster'),'Shipping Service(e.g. Fedex Ground, UPS Ground etc...)','SelectBox',NULL,1,1,1,137
),
(
	3,101,'Order Status','SelectBox',NULL,1,1,1,137
)


UPDATE DycFormField_Mapping SET intSectionID=1 WHERE numFormID IN (136,137) AND numFieldID IN (3,5,6,10,14,21,28,30,451,539)
UPDATE DycFormField_Mapping SET intSectionID=2 WHERE numFormID IN (136,137) AND numFieldID IN (217,218,219,220,221,222,223,224,225,226,101,100,(SELECT TOP 1 numFieldId FROM DycFieldMaster WHERE vcDbColumnName='intUsedShippingCompany' AND vcLookBackTableName='OpportunityMaster'),(SELECT TOP 1 numFieldId FROM DycFieldMaster WHERE vcDbColumnName='numShippingService' AND vcLookBackTableName='OpportunityMaster'))
UPDATE DycFormField_Mapping SET intSectionID=3 WHERE numFormID IN (136,137) AND numFieldID IN (293,233,257,258,259,262,306,189,203,211,281,291,899,@numFieldIDWorkOrder,@numFieldIDDiscountType)

UPDATE DycFormField_Mapping SET bitImport=1 WHERE numFormID IN (136,137)

ALTER TABLE Import_File_Master ADD bitSingleOrder BIT
ALTER TABLE Import_File_Master ADD bitExistingOrganization BIT
ALTER TABLE Import_File_Master ADD numDivisionID NUMERIC(18,0)
ALTER TABLE Import_File_Master ADD numContactID NUMERIC(18,0)
ALTER TABLE Import_File_Master ADD vcCompanyName VARCHAR(300)
ALTER TABLE Import_File_Master ADD vcContactFirstName VARCHAR(100)
ALTER TABLE Import_File_Master ADD vcContactLastName VARCHAR(100)
ALTER TABLE Import_File_Master ADD vcContactEmail VARCHAR(100)
ALTER TABLE Import_File_Master ADD bitMatchOrganizationID BIT
ALTER TABLE Import_File_Master ADD numMatchFieldID NUMERIC(18,0)

INSERT INTO DycFormSectionDetail
(
	numFormID,intSectionID,vcSectionName,Loc_id
)
VALUES
(
	136,1,'Organization Fields',0
),
(
	136,2,'Order Fields',0
),
(
	136,3,'Order Item(s) Fields',0
)


INSERT INTO DycFormSectionDetail
(
	numFormID,intSectionID,vcSectionName,Loc_id
)
VALUES
(
	137,1,'Organization Fields',0
),
(
	137,2,'Order Fields',0
),
(
	137,3,'Order Item(s) Fields',0
)