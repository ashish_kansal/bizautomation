/******************************************************************
Project: Release 4.6 Date: 29.June.2015
Comments: ALTER SCRIPTS
*******************************************************************/


GO

/****** Object:  Index [PK_ItemExtendedDetails]    Script Date: 19-Jun-15 12:47:33 PM ******/
ALTER TABLE [dbo].[ItemExtendedDetails] ADD  CONSTRAINT [PK_ItemExtendedDetails] PRIMARY KEY CLUSTERED 
(
	[numItemCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

DECLARE @numBrandID NUMERIC(18,0)
DECLARE @numItemType NUMERIC(18,0)

INSERT INTO DycFieldMaster 
(numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,
vcFieldType,vcAssociatedControlType,vcToolTip,vcListItemType,numListID,[order],tintRow,tintColumn,bitInResults,bitDeleted,
bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitImport,bitExport,bitAllowFiltering,
bitInlineEdit,bitRequired,intColumnWidth,intFieldMaxLength)
VALUES
(
	4,'Brand','vcBrand','vcBrand','Brand','Amazon','V','R','TextBox','Brand of the product','',0,1,1,2,0,0,1,1,0,0,1,0,0,1,0,0,0,0,0,250
)

SELECT @numBrandID = SCOPE_IDENTITY()

INSERT INTO DycFieldMaster 
(numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,
vcFieldType,vcAssociatedControlType,vcToolTip,vcListItemType,numListID,[order],tintRow,tintColumn,bitInResults,bitDeleted,
bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitImport,bitExport,bitAllowFiltering,
bitInlineEdit,bitRequired,intColumnWidth,intFieldMaxLength)
VALUES
(
	4,'ItemType','vcItemType','vcItemType','ItemType','Amazon','V','R','TextBox','Pre-defined value that specifies where the product should appear within the Amazon browse structure','',0,1,1,2,0,0,1,1,0,0,1,0,0,1,0,0,0,0,0,250
)


SELECT @numItemType = SCOPE_IDENTITY()

INSERT INTO DycFormField_Mapping 
(numModuleID,numFieldID,numFormID,bitDetailField,bitImport,bitAllowFiltering,intSectionID)
VALUES
(4,@numBrandID,50,1,1,1,2),
(4,@numItemType,50,1,1,1,2)