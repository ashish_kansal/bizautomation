/******************************************************************
Project: Release 3.9 Date: 29.NOV.2014
Comments: 
*******************************************************************/


------------------ SANDEEP ---------------------


<add key="AmazonSESMaxPerSec" value="5"></add> --IN BACRMUI

--OLD LINK WAS ../Marketing/frmEmailBroadcastConfigDetail.aspx

UPDATE PageNavigationDTL SET vcNavURL='../Marketing/frmEmailBroadcastSetting.aspx' WHERE numPageNavID=218
UPDATE PageNavigationDTL SET vcNavURL='../Marketing/frmEmailBroadcastSetting.aspx' WHERE numPageNavID=17

--TODO: First change name of existing EmailBroadcastConfiguration table to EmailBroadcastConfiguration_OLD also change PK name

/****** Object:  Table [dbo].[EmailBroadcastConfiguration]    Script Date: 18-Nov-14 12:44:29 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[EmailBroadcastConfiguration](
	[numConfigurationId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numDomainID] [numeric](18, 0) NOT NULL,
	[vcFrom] [varchar](100) NOT NULL,
	[vcAWSDomain] [varchar](100) NOT NULL,
	[vcAWSAccessKey] [varchar](100) NOT NULL,
	[vcAWSSecretKey] [varchar](100) NOT NULL,	
 CONSTRAINT [PK_EmailBroadcastConfiguration] PRIMARY KEY CLUSTERED 
(
	[numConfigurationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]


/****** Object:  Table [dbo].[BroadcastErrorLog]    Script Date: 20-Nov-14 12:07:04 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[BroadcastErrorLog](
	[numBroadcastErrorID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numBroadcastDtlID] [numeric](18, 0) NOT NULL,
	[vcType] [nvarchar](300) NULL,
	[vcMessage] [nvarchar](max) NULL,
	[vcStackStrace] [nvarchar](max) NULL,
 CONSTRAINT [PK_BroadcastErrorLog] PRIMARY KEY CLUSTERED 
(
	[numBroadcastErrorID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

ALTER TABLE [dbo].[BroadcastErrorLog]  WITH CHECK ADD  CONSTRAINT [FK_BroadcastErrorLog_BroadCastDtls] FOREIGN KEY([numBroadcastDtlID])
REFERENCES [dbo].[BroadCastDtls] ([numBroadCastDtlId])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[BroadcastErrorLog] CHECK CONSTRAINT [FK_BroadcastErrorLog_BroadCastDtls]
GO




ALTER TABLE Sites ADD
bitHtml5 BIT,
vcMetaTags text,
bitSSLRedirect BIT

--OLD VALUE OF vcFieldName='Serial #s'
UPDATE DycFormField_Mapping SET vcFieldName='Serial/Lot#s' WHERE numFormID IN (7,8) AND numFieldID=314


BEGIN TRANSACTION

DECLARE @numFieldID NUMERIC(18,0)

INSERT INTO DycFieldMaster
(
	numModuleID,
	vcFieldName,
	vcDbColumnName,
	vcOrigDbColumnName,
	vcPropertyName,
	vcLookBackTableName,
	vcFieldDataType,
	vcFieldType,
	vcAssociatedControlType,
	numListID,
	bitInResults,
	bitDeleted,
	bitAllowEdit,
	bitDefault,
	bitSettingField,
	bitDetailField
)
VALUES
(
	3,
	'Unit Sale Price',
	'monUnitSalePrice',
	'monUnitSalePrice',
	'monUnitSalePrice',
	'',
	'M',
	'R',
	'Label',
	0,
	1,
	0,
	0,
	0,
	0,
	0
)


SET @numFieldID = SCOPE_IDENTITY()

INSERT INTO DycFormField_Mapping
                         (numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, 
                         [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, 
                         bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
VALUES        (3,@numFieldID,Null,7,0,0,'Unit Sale Price','Label','',Null,1,1,2,Null,Null,Null,0,0,0,0,0,0,0,0,Null,Null,Null,0)

ROLLBACK


---------------------------------------------------------------------------


---------------------------------------------- MANISH -----------------------------

------/******************************************************************
------Project: BACRMUI   Date: 22.Nov.2014
------Comments: Add new column to custom reports for Serial Number
------*******************************************************************/

INSERT INTO [dbo].[ReportFieldGroupMappingMaster]([numReportFieldGroupID], [numFieldID], [vcFieldName], 
[vcAssociatedControlType], [vcFieldDataType], [bitAllowSorting], [bitAllowGrouping], [bitAllowAggregate], [bitAllowFiltering])
SELECT 4, numFieldId, vcFieldName, vcDbColumnName, vcFieldDataType, 0, 0, 0, 0 FROM DycFieldMaster 
WHERE numModuleID=4 AND vcLookBackTableName='WareHouseItmsDTL' AND [DycFieldMaster].[numFieldId] = 320

--=======================================================================


