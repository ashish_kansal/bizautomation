/******************************************************************
Project: Release 12.3 Date: 05.AUG.2019
Comments: ALTER SCRIPTS
*******************************************************************/


/******************************************** SANDEEP *********************************************/

ALTER TABLE OpportunityItems ADD bitWinningBid BIT
ALTER TABLE OpportunityItems ADD numParentOppItemID NUMERIC(18,0)


DECLARE @numFieldID NUMERIC(18,0)

INSERT INTO DycFieldMaster
(
	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcListItemType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField
)
VALUES
(
	3,'Group','vcGroupNonDbField','vcGroupNonDbField','OpportunityItems','V','R','Label','',0,0,0,0,0,1
)

SET @numFieldID = SCOPE_IDENTITY()

INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,bitInResults,bitDeleted,bitDefault,bitSettingField
)
VALUES
(
	3,@numFieldID,26,0,0,'Group','Label',0,0,0,1
)
