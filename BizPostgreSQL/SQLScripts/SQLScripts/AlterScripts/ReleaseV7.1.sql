/******************************************************************
Project: Release 7.1 Date: 14.MAR.2017
Comments: ALTER SCRIPTS
*******************************************************************/

/************************** SANDEEP ************************/


INSERT INTO ReportModuleGroupFieldMappingMaster
(
	numReportModuleGroupID,numReportFieldGroupID
)
VALUES
(19,20),(9,20),(10,20)



DECLARE @numFieldID NUMERIC(18,0)
SELECT @numFieldID=numFieldId FROM DycFieldMaster WHERE vcDbColumnName='numPartenerSource' AND vcLookBackTableName='DivisionMaster'


INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering
)
VALUES
(
	3,@numFieldID,35,1,1,'Partner Source','SelectBox','numPartenerSource',1,0,0,1,1,1,1,1
),
(
	3,@numFieldID,36,1,1,'Partner Source','SelectBox','numPartenerSource',1,0,0,1,1,1,1,1
)


-----------------------------

USE [Production.2014]
GO

/****** Object:  Table [dbo].[EcommerceRelationshipProfile]    Script Date: 06-Mar-17 12:29:02 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[EcommerceRelationshipProfile](
	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numSiteID] [numeric](18, 0) NOT NULL,
	[numRelationship] [numeric](18, 0) NOT NULL,
	[numProfile] [numeric](18, 0) NOT NULL,
 CONSTRAINT [PK_EcommerceRelationshipProfile] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[EcommerceRelationshipProfile]  WITH CHECK ADD  CONSTRAINT [FK_EcommerceRelationshipProfile_Sites] FOREIGN KEY([numSiteID])
REFERENCES [dbo].[Sites] ([numSiteID])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[EcommerceRelationshipProfile] CHECK CONSTRAINT [FK_EcommerceRelationshipProfile_Sites]
GO


----------------------------------


USE [Production.2014]
GO

/****** Object:  Table [dbo].[ECommerceItemClassification]    Script Date: 06-Mar-17 12:29:19 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ECommerceItemClassification](
	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numECommRelatiionshipProfileID] [numeric](18, 0) NOT NULL,
	[numItemClassification] [numeric](18, 0) NOT NULL,
 CONSTRAINT [PK_ECommerceItemClassification] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[ECommerceItemClassification]  WITH CHECK ADD  CONSTRAINT [FK_ECommerceItemClassification_EcommerceRelationshipProfile] FOREIGN KEY([numECommRelatiionshipProfileID])
REFERENCES [dbo].[EcommerceRelationshipProfile] ([ID])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[ECommerceItemClassification] CHECK CONSTRAINT [FK_ECommerceItemClassification_EcommerceRelationshipProfile]
GO


----------------------------------------------

USE [Production.2014]
GO

/****** Object:  Index [NonClusteredIndex-20170304-104654]    Script Date: 06-Mar-17 12:29:43 PM ******/
CREATE NONCLUSTERED INDEX [NonClusteredIndex-20170304-104654] ON [dbo].[EcommerceRelationshipProfile]
(
	[numSiteID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO


---------------------------------

ALTER TABLE BizDocComission ADD tintAssignTo TINYINT
ALTER TABLE ECommerceDTL ADD tintPreLoginProceLevel TINYINT
UPDATE ECommerceDTL SET tintPreLoginProceLevel = 0