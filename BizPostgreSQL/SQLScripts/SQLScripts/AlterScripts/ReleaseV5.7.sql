/******************************************************************
Project: Release 5.7 Date: 8.JUNE.2016
Comments: ALTER SCRIPTS
*******************************************************************/

/*************************** PRASANT ****************************/

ALTER TABLE ItemGroups ADD bitCombineAssemblies BIT DEFAULT 0
ALTER TABLE ItemGroups ADD numMapToDropdownFld NUMERIC(18,0) DEFAULT 0
ALTER TABLE ItemGroups ADD numProfileItem NUMERIC(18,0) DEFAULT 0
ALTER TABLE ItemGroupsDTL ADD numListid NUMERIC(18,0) DEFAULT 0

======================================================

ALTER TABLE OpportunityMaster ADD numPartner NUMERIC(18,0)

ALTER TABLE DivisionMaster ADD vcPartnerCode VARCHAR(100)

=============================================================================================================

BEGIN TRY
BEGIN TRANSACTION

DECLARE @numFieldID AS NUMERIC(18,0)
DECLARE @numFormFieldID AS NUMERIC(18,0)

INSERT INTO DycFieldMaster
(numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,[order],tintRow,tintColumn,
bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numListID)
VALUES
(3,'Partner Source','numPartner','numPartner','numPartner','OpportunityMaster','V','R','SelectBox',46,1,1,1,1,0,0,1,1,1,1,1,0)

SELECT @numFieldID = SCOPE_IDENTITY()

INSERT INTO DynamicFormFieldMaster
(numFormID,vcFormFieldName,vcFieldType,vcAssociatedControlType,vcDbColumnName,numListID,bitDeleted,vcFieldDataType,vcOrigDbColumnName,bitAllowEdit,vcLookBackTableName,
 bitDefault,[order],bitInResults,vcPropertyName,tintRow,tintColumn,bitSettingField,bitAddField,bitDetailField,bitAllowSorting)
VALUES
(90,'Partner Source','R','SelectBox','numPartner',0,0,'V','numPartner',1,'OpportunityMaster',0,7,1,'numPartner',1,7,1,1,1,0)

SELECT @numFormFieldID = SCOPE_IDENTITY()

INSERT INTO DycFormField_Mapping
(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,bitInResults,bitDeleted,
bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numFormFieldID)
VALUES
(3,@numFieldID,90,1,1,'Partner Source','SelectBox','numPartner',7,1,7,1,0,0,1,1,1,0,1,@numFormFieldID)

INSERT INTO DynamicFormFieldMaster
(numFormID,vcFormFieldName,vcFieldType,vcAssociatedControlType,vcDbColumnName,numListID,bitDeleted,vcFieldDataType,vcOrigDbColumnName,bitAllowEdit,vcLookBackTableName,
 bitDefault,[order],bitInResults,vcPropertyName,tintRow,tintColumn,bitSettingField,bitAddField,bitDetailField,bitAllowSorting)
VALUES
(39,'Partner Source','R','SelectBox','numPartner',0,0,'V','numPartner',1,'OpportunityMaster',0,7,1,'numPartner',1,7,1,0,1,0)

SELECT @numFormFieldID = SCOPE_IDENTITY()

INSERT INTO DycFormField_Mapping
(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,bitInResults,bitDeleted,
bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numFormFieldID)
VALUES
(3,@numFieldID,39,1,1,'Partner Source','SelectBox','numPartner',7,1,7,1,0,0,1,0,1,0,1,@numFormFieldID)


COMMIT
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		ERROR_MESSAGE(),
		ERROR_NUMBER(),
		ERROR_SEVERITY(),
		ERROR_STATE(),
		ERROR_LINE(),
		ISNULL(ERROR_PROCEDURE(), '-');
END CATCH

=============================================================================================================

BEGIN TRY
BEGIN TRANSACTION

DECLARE @numFieldID AS NUMERIC(18,0)
DECLARE @numFormFieldID AS NUMERIC(18,0)

INSERT INTO DycFieldMaster
(numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,[order],tintRow,tintColumn,
bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numListID)
VALUES
(3,'Partner Code','vcPartnerCode','vcPartnerCode','PartnerCode','DivisionMaster','V','R','TextBox',46,1,1,1,0,0,0,1,0,1,1,1,0)

SELECT @numFieldID = SCOPE_IDENTITY()

INSERT INTO DynamicFormFieldMaster
(numFormID,vcFormFieldName,vcFieldType,vcAssociatedControlType,vcDbColumnName,numListID,bitDeleted,vcFieldDataType,vcOrigDbColumnName,bitAllowEdit,vcLookBackTableName,
 bitDefault,[order],bitInResults,vcPropertyName,tintRow,tintColumn,bitSettingField,bitAddField,bitDetailField,bitAllowSorting)
VALUES
(35,'Partner Code','R','TextBox','vcPartnerCode',0,0,'V','vcPartnerCode',0,'DivisionMaster',0,7,1,'PartnerCode',1,7,1,0,1,0)

SELECT @numFormFieldID = SCOPE_IDENTITY()

INSERT INTO DycFormField_Mapping
(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,bitInResults,bitDeleted,
bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numFormFieldID)
VALUES
(3,@numFieldID,35,1,1,'Partner Code','TextBox','PartnerCode',7,1,7,1,0,0,1,0,1,0,1,@numFormFieldID)

INSERT INTO DynamicFormFieldMaster
(numFormID,vcFormFieldName,vcFieldType,vcAssociatedControlType,vcDbColumnName,numListID,bitDeleted,vcFieldDataType,vcOrigDbColumnName,bitAllowEdit,vcLookBackTableName,
 bitDefault,[order],bitInResults,vcPropertyName,tintRow,tintColumn,bitSettingField,bitAddField,bitDetailField,bitAllowSorting)
VALUES
(34,'Partner Code','R','TextBox','vcPartnerCode',0,0,'V','vcPartnerCode',0,'DivisionMaster',0,7,1,'PartnerCode',1,7,1,0,1,0)

SELECT @numFormFieldID = SCOPE_IDENTITY()

INSERT INTO DycFormField_Mapping
(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,bitInResults,bitDeleted,
bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numFormFieldID)
VALUES
(1,@numFieldID,34,1,1,'Partner Code','TextBox','PartnerCode',7,1,7,1,0,0,1,0,1,0,1,@numFormFieldID)

COMMIT
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		ERROR_MESSAGE(),
		ERROR_NUMBER(),
		ERROR_SEVERITY(),
		ERROR_STATE(),
		ERROR_LINE(),
		ISNULL(ERROR_PROCEDURE(), '-');
END CATCH


=============================================================

BEGIN TRY
BEGIN TRANSACTION

DECLARE @numFieldID AS NUMERIC(18,0)
SET @numFieldID=(SELECT TOP 1 numFieldId FROM DycFieldMaster WHERE vcFieldName='Partner Source' AND numModuleID=3)
INSERT INTO ReportFieldGroupMappingMaster
			(numReportFieldGroupID,numFieldID,vcFieldName,vcAssociatedControlType,vcFieldDataType,bitAllowSorting,bitAllowGrouping,bitAllowAggregate,bitAllowFiltering)
			VALUES(3,@numFieldID,'Partner Source','SelectBox','N',1,1,0,1)

COMMIT
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		ERROR_MESSAGE(),
		ERROR_NUMBER(),
		ERROR_SEVERITY(),
		ERROR_STATE(),
		ERROR_LINE(),
		ISNULL(ERROR_PROCEDURE(), '-');
END CATCH

===================================================================

BEGIN TRY
BEGIN TRANSACTION

DECLARE @numFieldID AS NUMERIC(18,0)
SET @numFieldID=(SELECT TOP 1 numFieldId FROM DycFieldMaster WHERE vcFieldName='Partner Code' AND numModuleID=3)
INSERT INTO ReportFieldGroupMappingMaster
			(numReportFieldGroupID,numFieldID,vcFieldName,vcAssociatedControlType,vcFieldDataType,bitAllowSorting,bitAllowGrouping,bitAllowAggregate,bitAllowFiltering)
			VALUES(1,@numFieldID,'Partner Code','TextBox','V',1,1,0,1)

COMMIT
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		ERROR_MESSAGE(),
		ERROR_NUMBER(),
		ERROR_SEVERITY(),
		ERROR_STATE(),
		ERROR_LINE(),
		ISNULL(ERROR_PROCEDURE(), '-');
END CATCH




/*************************** SANDEEP ****************************/

=========================================================================================

ALTER TABLE PromotionOffer DROP COLUMN [vcTerms]
ALTER TABLE PromotionOffer DROP COLUMN [numSiteId]
ALTER TABLE PromotionOffer DROP COLUMN [bintStartDate]
ALTER TABLE PromotionOffer DROP COLUMN [bintEndDate]
ALTER TABLE PromotionOffer DROP COLUMN [tintAppliesTo]
ALTER TABLE PromotionOffer DROP COLUMN [intLimitation]
ALTER TABLE PromotionOffer DROP COLUMN [tintLimitationBasedOn]
ALTER TABLE PromotionOffer DROP COLUMN [vcCouponCode]
ALTER TABLE PromotionOffer DROP COLUMN [tintDiscountType]
ALTER TABLE PromotionOffer DROP COLUMN [decDiscount]
ALTER TABLE PromotionOffer DROP COLUMN [tintBasedOn]
ALTER TABLE PromotionOffer DROP COLUMN [tintContactsType]
ALTER TABLE PromotionOffer DROP COLUMN [decBasedOnValue]

=========================================================================================

ALTER TABLE PromotionOffer ADD
dtValidFrom DATE,
dtValidTo DATE,
bitNeverExpires BIT,
bitApplyToInternalOrders BIT,
bitAppliesToSite BIT,
bitRequireCouponCode BIT,
txtCouponCode VARCHAR(100),
tintUsageLimit INT,
bitFreeShiping BIT,
monFreeShippingOrderAmount MONEY,
numFreeShippingCountry NUMERIC(18,0),
bitFixShipping1 BIT,
monFixShipping1OrderAmount MONEY,
monFixShipping1Charge MONEY,
bitFixShipping2 BIT,
monFixShipping2OrderAmount MONEY,
monFixShipping2Charge MONEY,
bitDisplayPostUpSell BIT,
intPostSellDiscount INT,
tintOfferTriggerValueType TINYINT,
fltOfferTriggerValue FLOAT,
tintOfferBasedOn TINYINT,
fltDiscountValue FLOAT,
tintDiscountType TINYINT,
tintDiscoutBaseOn TINYINT,
numCreatedBy NUMERIC(18,0),
dtCreated DATETIME,
numModifiedBy NUMERIC(18,0),
dtModified DATETIME,
intCouponCodeUsed INT,
bitEnabled BIT
==========================================================================================

USE [Production.2014]
GO

/****** Object:  Table [dbo].[PromotionOfferSites]    Script Date: 22-Apr-16 2:02:08 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[PromotionOfferSites](
	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numPromotionID] [numeric](18, 0) NOT NULL,
	[numSiteID] [numeric](18, 0) NOT NULL,
 CONSTRAINT [PK_PromotionOfferSites] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[PromotionOfferSites]  WITH CHECK ADD  CONSTRAINT [FK_PromotionOfferSites_PromotionOffer] FOREIGN KEY([numPromotionID])
REFERENCES [dbo].[PromotionOffer] ([numProId])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[PromotionOfferSites] CHECK CONSTRAINT [FK_PromotionOfferSites_PromotionOffer]
GO

ALTER TABLE [dbo].[PromotionOfferSites]  WITH CHECK ADD  CONSTRAINT [FK_PromotionOfferSites_Sites] FOREIGN KEY([numSiteID])
REFERENCES [dbo].[Sites] ([numSiteID])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[PromotionOfferSites] CHECK CONSTRAINT [FK_PromotionOfferSites_Sites]
GO

====================================================================================================
ALTER TABLE OpportunityItems ADD
numPromotionID NUMERIC(18,0),
bitPromotionTriggered BIT,
vcPromotionDetail VARCHAR(2000)


ALTER TABLE OpportunityMaster ADD
dtReleaseDate DATE
====================================================================================================

BEGIN TRY
BEGIN TRANSACTION

	DECLARE @numFieldID AS NUMERIC(18,0)
	DECLARE @numFormFieldID AS NUMERIC(18,0)

	INSERT INTO DycFieldMaster
	(numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,[order],tintRow,tintColumn,
	bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numListID)
	VALUES
	(3,'Promotion Detail','vcPromotionDetail','vcPromotionDetail','vcPromotionDetail','OpportunityItems','V','R','Label',47,1,1,1,0,0,0,1,0,0,0,0,0)

	SELECT @numFieldID = SCOPE_IDENTITY()

	INSERT INTO DynamicFormFieldMaster
	(numFormID,vcFormFieldName,vcFieldType,vcAssociatedControlType,vcDbColumnName,numListID,bitDeleted,vcFieldDataType,vcOrigDbColumnName,bitAllowEdit,vcLookBackTableName,
	 bitDefault,[order],bitInResults,vcPropertyName,tintRow,tintColumn,bitSettingField,bitAddField,bitDetailField,bitAllowSorting)
	VALUES
	(26,'Promotion Detail','R','Label','vcPromotionDetail',0,0,'V','vcPromotionDetail',0,'OpportunityItems',0,47,1,'vcPromotionDetail',47,1,1,0,0,0)

	SELECT @numFormFieldID = SCOPE_IDENTITY()

	INSERT INTO DycFormField_Mapping
	(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,bitInResults,bitDeleted,
	bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numFormFieldID)
	VALUES
	(3,@numFieldID,26,0,0,'Promotion Detail','Label','vcPromotionDetail',47,47,1,1,0,0,1,0,0,0,0,@numFormFieldID)

	INSERT INTO DynamicFormFieldMaster
	(numFormID,vcFormFieldName,vcFieldType,vcAssociatedControlType,vcDbColumnName,numListID,bitDeleted,vcFieldDataType,vcOrigDbColumnName,bitAllowEdit,vcLookBackTableName,
	 bitDefault,[order],bitInResults,vcPropertyName,tintRow,tintColumn,bitSettingField,bitAddField,bitDetailField,bitAllowSorting)
	VALUES
	(7,'Promotion Detail','R','Label','vcPromotionDetail',0,0,'V','vcPromotionDetail',0,'OpportunityItems',0,47,1,'vcPromotionDetail',47,1,1,0,0,0)

	SELECT @numFormFieldID = SCOPE_IDENTITY()

	INSERT INTO DycFormField_Mapping
	(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,bitInResults,bitDeleted,
	bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numFormFieldID)
	VALUES
	(3,@numFieldID,7,0,0,'Promotion Detail','Label','vcPromotionDetail',47,47,1,1,0,0,1,0,0,0,0,@numFormFieldID)

COMMIT
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		ERROR_MESSAGE(),
		ERROR_NUMBER(),
		ERROR_SEVERITY(),
		ERROR_STATE(),
		ERROR_LINE(),
		ISNULL(ERROR_PROCEDURE(), '-');
END CATCH

=================================================================================================

ALTER TABLE PromotionOfferItems 
ALTER COLUMN numValue NUMERIC(18,0)

====================================================
BEGIN TRY
BEGIN TRANSACTION

	DECLARE @numMAXPageNavID INT
	SELECT @numMAXPageNavID = MAX(numPageNavID) FROM PageNavigationDTL


	INSERT INTO PageNavigationDTL
	(
		numPageNavID,numModuleID,numParentID,vcPageNavName,vcNavURL,bitVisible,numTabID
	)
	VALUES
	(
		(@numMAXPageNavID + 1),13,78,'Promotion Management','../ECommerce/frmPromotionOfferList.aspx',1,-1
	)

	INSERT INTO dbo.TreeNavigationAuthorization
	(
		numGroupID,
		numTabID,
		numPageNavID,
		bitVisible,
		numDomainID,
		tintType
	)
	SELECT  
		numGroupID,
		-1,
		(@numMAXPageNavID + 1),
		1,
		numDomainID,
		1
	FROM    
		AuthenticationGroupMaster
	WHERE
		tintGroupType=1
COMMIT
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		ERROR_MESSAGE(),
		ERROR_NUMBER(),
		ERROR_SEVERITY(),
		ERROR_STATE(),
		ERROR_LINE(),
		ISNULL(ERROR_PROCEDURE(), '-');
END CATCH

--SELECT * FROM PageNavigationDTL WHERE numTabID=-1 ANd numParentID=35


--UPDATE PageNavigationDTL SET intSortOrder=1 WHERE numPageNavID=57
--UPDATE PageNavigationDTL SET intSortOrder=2 WHERE numPageNavID=61
--UPDATE PageNavigationDTL SET intSortOrder=3 WHERE numPageNavID=64
--UPDATE PageNavigationDTL SET intSortOrder=4 WHERE numPageNavID=66
--UPDATE PageNavigationDTL SET intSortOrder=5 WHERE numPageNavID=73
--UPDATE PageNavigationDTL SET intSortOrder=6 WHERE numPageNavID=78
--UPDATE PageNavigationDTL SET intSortOrder=7 WHERE numPageNavID=260
--UPDATE PageNavigationDTL SET intSortOrder=8 WHERE numPageNavID=109
--UPDATE PageNavigationDTL SET intSortOrder=9 WHERE numPageNavID=181
--UPDATE PageNavigationDTL SET intSortOrder=10 WHERE numPageNavID=205
--UPDATE PageNavigationDTL SET intSortOrder=11 WHERE numPageNavID=79
--UPDATE PageNavigationDTL SET intSortOrder=12 WHERE numPageNavID=101