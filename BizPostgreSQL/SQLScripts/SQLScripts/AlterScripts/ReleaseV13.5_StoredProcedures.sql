/******************************************************************
Project: Release 13.5 Date: 27.APR.2020
Comments: STORE PROCEDURES
*******************************************************************/

--Created by anoop jayaraj                                                          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_checkuseratlogin')
DROP PROCEDURE usp_checkuseratlogin
GO
CREATE PROCEDURE [dbo].[USP_CheckUserAtLogin]                                                              
(                                                                                    
	@UserName as varchar(100)='',                                                                        
	@vcPassword as varchar(100)='',
	@vcLinkedinId as varchar(300)=''                                                                           
)                                                              
AS             
BEGIN
	DECLARE @listIds VARCHAR(MAX)

	SELECT 
		@listIds = COALESCE(@listIds+',' ,'') + CAST(UDTL.numUserId AS VARCHAR(100)) 
	FROM 
		UnitPriceApprover U
	Join 
		Domain D                              
	on 
		D.numDomainID=U.numDomainID
	Join  
		Subscribers S                            
	on 
		S.numTargetDomainID=D.numDomainID
	LEFT JOIN UserMaster As UDTL
	ON
		U.numUserID=UDTL.numUserDetailId
	WHERE 
		1 = (CASE 
			WHEN @vcLinkedinId='N' THEN (CASE WHEN UDTL.vcEmailID=@UserName and UDTL.vcPassword=@vcPassword THEN 1 ELSE 0 END)
			ELSE (CASE WHEN UDTL.vcLinkedinId=@vcLinkedinId THEN 1 ELSE 0 END)
			END)
       
	IF (SELECT 
			COUNT(*)
		FROM 
			UserMaster U                              
		JOIN 
			Domain D                              
		ON 
			D.numDomainID=U.numDomainID
		JOIN 
			Subscribers S                            
		ON 
			S.numTargetDomainID=D.numDomainID
		WHERE 
			1 = (CASE 
					WHEN @vcLinkedinId='N' THEN (CASE WHEN vcEmailID=@UserName and vcPassword=@vcPassword THEN 1 ELSE 0 END)
					ELSE (CASE WHEN vcLinkedinId=@vcLinkedinId THEN 1 ELSE 0 END)
				END) 
			AND bitActive=1) > 1
	BEGIN
		RAISERROR('MULTIPLE_RECORDS_WITH_SAME_EMAIL',16,1)
		RETURN;
	END   
	                                          
	/*check subscription validity */
	IF EXISTS(SELECT 
					*
			FROM 
				UserMaster U                              
			JOIN 
				Domain D                              
			ON 
				D.numDomainID=U.numDomainID
			JOIN 
				Subscribers S                            
			ON 
				S.numTargetDomainID=D.numDomainID
			WHERE 
				1 = (CASE 
						WHEN @vcLinkedinId='N' THEN (CASE WHEN vcEmailID=@UserName and vcPassword=@vcPassword THEN 1 ELSE 0 END)
						ELSE (CASE WHEN vcLinkedinId=@vcLinkedinId THEN 1 ELSE 0 END)
					END) 
				AND bitActive=1 
				AND getutcdate() > dtSubEndDate)
	BEGIN
		RAISERROR('SUB_RENEW',16,1)
		RETURN;
	END

	SELECT TOP 1 
		U.numUserID
		,numUserDetailId
		,D.numCost
		,isnull(vcEmailID,'') vcEmailID
		,ISNULL(vcEmailAlias,'') vcEmailAlias
		,ISNULL(vcEmailAliasPassword,'') vcEmailAliasPassword
		,isnull(numGroupID,0) numGroupID
		,bitActivateFlag
		,isnull(vcMailNickName,'') vcMailNickName
		,isnull(txtSignature,'') txtSignature
		,isnull(U.numDomainID,0) numDomainID
		,isnull(Div.numDivisionID,0) numDivisionID
		,isnull(vcCompanyName,'') vcCompanyName
		,isnull(E.bitExchangeIntegration,0) bitExchangeIntegration
		,isnull(E.bitAccessExchange,0) bitAccessExchange
		,isnull(E.vcExchPath,'') vcExchPath
		,isnull(E.vcExchDomain,'') vcExchDomain
		,isnull(D.vcExchUserName,'') vcExchUserName
		,isnull(vcFirstname,'') + ' ' + isnull(vcLastName,'') as ContactName
		,Case when E.bitAccessExchange=0 then  E.vcExchPassword when E.bitAccessExchange=1 then D.vcExchPassword end as vcExchPassword
		,tintCustomPagingRows
		,vcDateFormat
		,numDefCountry
		,tintComposeWindow
		,dateadd(day,-sintStartDate,getutcdate()) as StartDate
		,dateadd(day,sintNoofDaysInterval,dateadd(day,-sintStartDate,getutcdate())) as EndDate
		,tintAssignToCriteria
		,bitIntmedPage
		,tintFiscalStartMonth
		,numAdminID
		,isnull(A.numTeam,0) numTeam
		,isnull(D.vcCurrency,'') as vcCurrency
		,isnull(D.numCurrencyID,0) as numCurrencyID, 
		isnull(D.bitMultiCurrency,0) as bitMultiCurrency,              
		bitTrial,isnull(tintChrForComSearch,0) as tintChrForComSearch,  
		isnull(tintChrForItemSearch,0) as tintChrForItemSearch,              
		case when bitSMTPServer= 1 then vcSMTPServer else '' end as vcSMTPServer  ,          
		case when bitSMTPServer= 1 then  isnull(numSMTPPort,0)  else 0 end as numSMTPPort      
		,isnull(bitSMTPAuth,0) bitSMTPAuth    
		,isnull([vcSmtpPassword],'') vcSmtpPassword    
		,isnull(bitSMTPSSL,0) bitSMTPSSL   ,isnull(bitSMTPServer,0) bitSMTPServer,       
		isnull(IM.bitImap,0) bitImapIntegration, isnull(charUnitSystem,'E')   UnitSystem, datediff(day,getutcdate(),dtSubEndDate) as NoOfDaysLeft,
		ISNULL(bitCreateInvoice,0) bitCreateInvoice,
		ISNULL([numDefaultSalesBizDocId],0) numDefaultSalesBizDocId,
		ISNULL([numDefaultPurchaseBizDocId],0) numDefaultPurchaseBizDocId,
		vcDomainName,
		S.numSubscriberID,
		D.[tintBaseTax],
		u.ProfilePic,
		ISNULL(D.[numShipCompany],0) numShipCompany,
		ISNULL(D.[bitEmbeddedCost],0) bitEmbeddedCost,
		(Select isnull(numAuthoritativeSales,0) From AuthoritativeBizDocs Where numDomainId=D.numDomainID) numAuthoritativeSales,
		(Select isnull(numAuthoritativePurchase,0) From AuthoritativeBizDocs Where numDomainId=D.numDomainID) numAuthoritativePurchase,
		ISNULL(D.[numDefaultSalesOppBizDocId],0) numDefaultSalesOppBizDocId,
		ISNULL(tintDecimalPoints,2) tintDecimalPoints,
		ISNULL(D.tintBillToForPO,0) tintBillToForPO, 
		ISNULL(D.tintShipToForPO,0) tintShipToForPO,
		ISNULL(D.tintOrganizationSearchCriteria,0) tintOrganizationSearchCriteria,
		ISNULL(D.tintLogin,0) tintLogin,
		ISNULL(bitDocumentRepositary,0) bitDocumentRepositary,
		ISNULL(D.vcPortalName,'') vcPortalName,
		(SELECT COUNT(*) FROM dbo.Subscribers WHERE getutcdate() between dtSubStartDate and dtSubEndDate and bitActive=1),
		ISNULL(D.bitGtoBContact,0) bitGtoBContact,
		ISNULL(D.bitBtoGContact,0) bitBtoGContact,
		ISNULL(D.bitGtoBCalendar,0) bitGtoBCalendar,
		ISNULL(D.bitBtoGCalendar,0) bitBtoGCalendar,
		ISNULL(bitExpenseNonInventoryItem,0) bitExpenseNonInventoryItem,
		ISNULL(D.bitInlineEdit,0) bitInlineEdit,
		ISNULL(U.numDefaultClass,0) numDefaultClass,
		ISNULL(D.bitRemoveVendorPOValidation,0) bitRemoveVendorPOValidation,
		ISNULL(D.bitTrakDirtyForm,1) bitTrakDirtyForm,
		isnull(D.tintBaseTaxOnArea,0) tintBaseTaxOnArea,
		ISNULL(D.bitAmountPastDue,0) bitAmountPastDue,
		CONVERT(DECIMAL(18,2),ISNULL(D.monAmountPastDue,0)) monAmountPastDue,
		ISNULL(D.bitSearchOrderCustomerHistory,0) bitSearchOrderCustomerHistory,
		isnull(U.tintTabEvent,0) as tintTabEvent,
		isnull(D.bitRentalItem,0) as bitRentalItem,
		isnull(D.numRentalItemClass,0) as numRentalItemClass,
		isnull(D.numRentalHourlyUOM,0) as numRentalHourlyUOM,
		isnull(D.numRentalDailyUOM,0) as numRentalDailyUOM,
		isnull(D.tintRentalPriceBasedOn,0) as tintRentalPriceBasedOn,
		isnull(D.tintCalendarTimeFormat,24) as tintCalendarTimeFormat,
		isnull(D.tintDefaultClassType,0) as tintDefaultClassType,
		ISNULL(U.numDefaultWarehouse,0) numDefaultWarehouse,
		ISNULL(D.tintPriceBookDiscount,0) tintPriceBookDiscount,
		ISNULL(D.bitAutoSerialNoAssign,0) bitAutoSerialNoAssign,
		ISNULL(IsEnableProjectTracking,0) AS [IsEnableProjectTracking],
		ISNULL(IsEnableCampaignTracking,0) AS [IsEnableCampaignTracking],
		ISNULL(IsEnableResourceScheduling,0) AS [IsEnableResourceScheduling],
		ISNULL(D.numSOBizDocStatus,0) AS numSOBizDocStatus,
		ISNULL(D.bitAutolinkUnappliedPayment,0) AS bitAutolinkUnappliedPayment,
		ISNULL(numShippingServiceItemID,0) AS [numShippingServiceItemID],
		ISNULL(D.numDiscountServiceItemID,0) AS numDiscountServiceItemID,
		ISNULL(D.tintCommissionType,1) AS tintCommissionType,
		ISNULL(D.bitDiscountOnUnitPrice,0) AS bitDiscountOnUnitPrice,
		ISNULL(D.intShippingImageWidth,0) AS intShippingImageWidth ,
		ISNULL(D.intShippingImageHeight,0) AS intShippingImageHeight,
		ISNULL(D.numTotalInsuredValue,0) AS numTotalInsuredValue,
		ISNULL(D.numTotalCustomsValue,0) AS numTotalCustomsValue,
		ISNULL(D.bitUseBizdocAmount,1) AS [bitUseBizdocAmount],
		ISNULL(D.bitDefaultRateType,1) AS [bitDefaultRateType],
		ISNULL(D.bitIncludeTaxAndShippingInCommission,1) AS [bitIncludeTaxAndShippingInCommission],
		ISNULL(D.[bitLandedCost],0) AS bitLandedCost,
		ISNULL(D.vcLanedCostDefault,'') AS vcLanedCostDefault,
		ISNULL(D.bitMinUnitPriceRule,0) AS bitMinUnitPriceRule,
		ISNULL(@listIds,'') AS vcUnitPriceApprover,
		ISNULL(D.numPODropShipBizDoc,0) AS numPODropShipBizDoc,
		ISNULL(D.numPODropShipBizDocTemplate,0) AS numPODropShipBizDocTemplate,
		ISNULL(D.IsEnableDeferredIncome,0) AS IsEnableDeferredIncome,
		ISNULL(D.bitEnableItemLevelUOM,0) AS bitEnableItemLevelUOM,
		ISNULL(D.bitPurchaseTaxCredit,0) AS bitPurchaseTaxCredit,
		ISNULL(U.bintCreatedDate,0) AS bintCreatedDate,
		(CASE WHEN LEN(ISNULL(TempDefaultPage.vcDefaultNavURL,'')) = 0 THEN ISNULL(TempDefaultTab.vcDefaultNavURL,'Items/frmItemList.aspx?Page=All Items&ItemGroup=0') ELSE ISNULL(TempDefaultPage.vcDefaultNavURL,'') END) vcDefaultNavURL,
		ISNULL(D.bitApprovalforTImeExpense,0) AS bitApprovalforTImeExpense,
		ISNULL(D.intTimeExpApprovalProcess,0) AS intTimeExpApprovalProcess,ISNULL(D.bitApprovalforOpportunity,0) AS bitApprovalforOpportunity,
		ISNULL(D.intOpportunityApprovalProcess,0) AS intOpportunityApprovalProcess,
		ISNULL(D.numDefaultSalesShippingDoc,0) AS numDefaultSalesShippingDoc,
		ISNULL(D.numDefaultPurchaseShippingDoc,0) AS numDefaultPurchaseShippingDoc,
		ISNULL((SELECT TOP 1 bitMarginPriceViolated FROM ApprovalProcessItemsClassification  WHERE numDomainID=D.numDomainID AND ISNULL(bitMarginPriceViolated,0) = 1),0) AS bitMarginPriceViolated,
		ISNULL((SELECT TOP 1 intViewAllowed FROM GroupAuthorization WHERE numGroupID=U.numGroupID AND numModuleID=2 AND numPageID=2),0) AS tintLeadRights,
		ISNULL((SELECT TOP 1 intViewAllowed FROM GroupAuthorization WHERE numGroupID=U.numGroupID AND numModuleID=3 AND numPageID=2),0) AS tintProspectRights,
		ISNULL((SELECT TOP 1 intViewAllowed FROM GroupAuthorization WHERE numGroupID=U.numGroupID AND numModuleID=4 AND numPageID=2),0) AS tintAccountRights,
		ISNULL((SELECT TOP 1 intViewAllowed FROM GroupAuthorization WHERE numGroupID=U.numGroupID AND numModuleID=10 AND numPageID=10),0) AS tintSalesOrderListRights,
		ISNULL((SELECT TOP 1 intViewAllowed FROM GroupAuthorization WHERE numGroupID=U.numGroupID AND numModuleID=10 AND numPageID=11),0) AS tintPurchaseOrderListRights,
		ISNULL((SELECT TOP 1 intViewAllowed FROM GroupAuthorization WHERE numGroupID=U.numGroupID AND numModuleID=10 AND numPageID=2),0) AS tintSalesOpportunityListRights,
		ISNULL((SELECT TOP 1 intViewAllowed FROM GroupAuthorization WHERE numGroupID=U.numGroupID AND numModuleID=10 AND numPageID=8),0) AS tintPurchaseOpportunityListRights,
		ISNULL((SELECT TOP 1 intViewAllowed FROM GroupAuthorization WHERE numGroupID=U.numGroupID AND numModuleID=11 AND numPageID=2),0) AS tintContactListRights,
		ISNULL((SELECT TOP 1 intViewAllowed FROM GroupAuthorization WHERE numGroupID=U.numGroupID AND numModuleID=7 AND numPageID=2),0) AS tintCaseListRights,
		ISNULL((SELECT TOP 1 intViewAllowed FROM GroupAuthorization WHERE numGroupID=U.numGroupID AND numModuleID=12 AND numPageID=2),0) AS tintProjectListRights,
		ISNULL((SELECT TOP 1 numCriteria FROM TicklerListFilterConfiguration WHERE CHARINDEX(CONCAT(',',U.numUserDetailID,','),CONCAT(',',vcSelectedEmployee,',')) > 0),0) AS tintActionItemRights,
		ISNULL(D.numAuthorizePercentage,0) AS numAuthorizePercentage,
		ISNULL(D.bitAllowDuplicateLineItems,0) bitAllowDuplicateLineItems,
		ISNULL(D.bitLogoAppliedToBizTheme,0) bitLogoAppliedToBizTheme,
		ISNULL(D.vcLogoForBizTheme,'') vcLogoForBizTheme,
		ISNULL(D.bitLogoAppliedToLoginBizTheme,0) bitLogoAppliedToLoginBizTheme,
		ISNULL(D.vcLogoForLoginBizTheme,'') vcLogoForLoginBizTheme,
		ISNULL(D.vcThemeClass,'1473b4') vcThemeClass,
		ISNULL(D.vcLoginURL,'') AS vcLoginURL,
		ISNULL(D.bitDisplayCustomField,0) AS bitDisplayCustomField,
		ISNULL(D.bitFollowupAnytime,0) AS bitFollowupAnytime,
		ISNULL(D.bitpartycalendarTitle,0) AS bitpartycalendarTitle,
		ISNULL(D.bitpartycalendarLocation,0) AS bitpartycalendarLocation,
		ISNULL(D.bitpartycalendarDescription,0) AS bitpartycalendarDescription,
		ISNULL(D.bitREQPOApproval,0) AS bitREQPOApproval,
		ISNULL(D.bitARInvoiceDue,0) AS bitARInvoiceDue,
		ISNULL(D.bitAPBillsDue,0) AS bitAPBillsDue,
		ISNULL(D.bitItemsToPickPackShip,0) AS bitItemsToPickPackShip,
		ISNULL(D.bitItemsToInvoice,0) AS bitItemsToInvoice,
		ISNULL(D.bitSalesOrderToClose,0) AS bitSalesOrderToClose,
		ISNULL(D.bitItemsToPutAway,0) AS bitItemsToPutAway,
		ISNULL(D.bitItemsToBill,0) AS bitItemsToBill,
		ISNULL(D.bitPosToClose,0) AS bitPosToClose,
		ISNULL(D.bitPOToClose,0) AS bitPOToClose,
		ISNULL(D.bitBOMSToPick,0) AS bitBOMSToPick,

		ISNULL(D.vchREQPOApprovalEmp,'') AS vchREQPOApprovalEmp,
		ISNULL(D.vchARInvoiceDue,'') AS vchARInvoiceDue,
		ISNULL(D.vchAPBillsDue,'') AS vchAPBillsDue,
		ISNULL(D.vchItemsToPickPackShip,'') AS vchItemsToPickPackShip,
		ISNULL(D.vchItemsToInvoice,'') AS vchItemsToInvoice,
		ISNULL(stuff((
        select ',' + CAST(UPC.numUserID AS VARCHAR(10))
        from UnitPriceApprover UPC
        where UPC.numDomainID = D.numDomainId
        order by UPC.numUserID
        for xml path('')
    ),1,1,''),'') AS vchPriceMarginApproval,
		ISNULL(D.vchSalesOrderToClose,'') AS vchSalesOrderToClose,
		ISNULL(D.vchItemsToPutAway,'') AS vchItemsToPutAway,
		ISNULL(D.vchItemsToBill,'') AS vchItemsToBill,
		ISNULL(D.vchPosToClose,'') AS vchPosToClose,
		ISNULL(D.vchPOToClose,'') AS vchPOToClose,
		ISNULL(D.vchBOMSToPick,'') AS vchBOMSToPick,
		ISNULL(D.decReqPOMinValue,0) AS decReqPOMinValue,
		ISNULL(U.tintPayrollType,1) tintPayrollType,
		(CASE WHEN EXISTS (SELECT numCategoryHDRID FROM TimeAndExpense WHERE numUserCntID=U.numUserDetailId AND numCategory=1 AND numType=7 AND dtToDate IS NULL) THEN 1 ELSE 0 END) bitUserClockedIn,
		ISNULL(D.bitUseOnlyActionItems,0) AS bitUseOnlyActionItems
	FROM 
		UserMaster U                              
	left join ExchangeUserDetails E                              
	on E.numUserID=U.numUserID                              
	left join ImapUserDetails IM                              
	on IM.numUserCntID=U.numUserDetailID          
	Join Domain D                              
	on D.numDomainID=U.numDomainID                                           
	left join DivisionMaster Div                                            
	on D.numDivisionID=Div.numDivisionID                               
	left join CompanyInfo C                              
	on C.numCompanyID=Div.numCompanyID                               
	left join AdditionalContactsInformation A                              
	on  A.numContactID=U.numUserDetailId                            
	Join  Subscribers S                            
	on S.numTargetDomainID=D.numDomainID    
	 OUTER APPLY
	 (
		SELECT  
			TOP 1 REPLACE(SCB.Link,'../','') vcDefaultNavURL
		FROM    
			TabMaster T
		JOIN 
			GroupTabDetails G ON G.numTabId = T.numTabId
		LEFT JOIN
			ShortCutGrpConf SCUC ON G.numTabId=SCUC.numTabId AND SCUC.numDomainId=U.numDomainID AND SCUC.numGroupId=U.numGroupID AND bitDefaultTab=1
		LEFT JOIN
			ShortCutBar SCB
		ON
			SCB.Id=SCUC.numLinkId
		WHERE   
			(T.numDomainID = U.numDomainID OR bitFixed = 1)
			AND G.numGroupID = U.numGroupID
			AND ISNULL(G.[tintType], 0) <> 1
			AND tintTabType =1
			AND T.numTabID NOT IN (2,68)
		ORDER BY 
			SCUC.bitInitialPage DESC
	 )  TempDefaultPage 
	 OUTER APPLY
	 (
		SELECT  
			TOP 1 REPLACE(T.vcURL,'../','') vcDefaultNavURL
		FROM    
			TabMaster T
		JOIN 
			GroupTabDetails G 
		ON 
			G.numTabId = T.numTabId
		WHERE   
			(T.numDomainID = U.numDomainID OR bitFixed = 1)
			AND G.numGroupID = U.numGroupID
			AND ISNULL(G.[tintType], 0) <> 1
			AND tintTabType =1
			AND T.numTabID NOT IN (2,68)
			AND ISNULL(G.bitInitialTab,0) = 1 
			AND ISNULL(bitallowed,0)=1
		ORDER BY 
			G.bitInitialTab DESC
	 ) TEMPDefaultTab                   
	WHERE 1 = (CASE 
		WHEN @vcLinkedinId='N' THEN (CASE WHEN vcEmailID=@UserName and vcPassword=@vcPassword THEN 1 ELSE 0 END)
		ELSE (CASE WHEN U.vcLinkedinId=@vcLinkedinId THEN 1 ELSE 0 END)
		END) and bitActive IN (1,2) AND getutcdate() between dtSubStartDate and dtSubEndDate
             
	SELECT 
		numTerritoryId 
	FROM
		UserTerritory UT                              
	JOIN 
		UserMaster U                              
	ON 
		numUserDetailId =UT.numUserCntID                                         
	WHERE 
		1 = (CASE 
			WHEN @vcLinkedinId='N' THEN (CASE WHEN vcEmailID=@UserName and vcPassword=@vcPassword THEN 1 ELSE 0 END)
			ELSE (CASE WHEN vcLinkedinId=@vcLinkedinId THEN 1 ELSE 0 END)
			END)

END
GO
/****** Object:  StoredProcedure [dbo].[USP_EcommerceSettings]    Script Date: 03/25/2009 16:46:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj              
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_ecommercesettings')
DROP PROCEDURE usp_ecommercesettings
GO
CREATE PROCEDURE [dbo].[USP_EcommerceSettings]              
@numDomainID as numeric(9),              
@vcPaymentGateWay as int ,                
@vcPGWUserId as varchar(100)='' ,                
@vcPGWPassword as varchar(100),              
@bitShowInStock as bit,              
@bitShowQOnHand as bit,
@bitCheckCreditStatus as BIT,
@numDefaultWareHouseID as numeric(9),
@numRelationshipId as numeric(9),
@numProfileId as numeric(9),
@bitHidePriceBeforeLogin	BIT,
@bitAuthOnlyCreditCard BIT=0,
@bitSendMail BIT = 1,
@vcGoogleMerchantID VARCHAR(1000)  = '',
@vcGoogleMerchantKey VARCHAR(1000) = '',
@IsSandbox BIT ,
@numAuthrizedOrderStatus NUMERIC,
@numSiteID numeric(18, 0),
@vcPaypalUserName VARCHAR(50)  = '',
@vcPaypalPassword VARCHAR(50)  = '',
@vcPaypalSignature VARCHAR(500)  = '',
@IsPaypalSandbox BIT,
@bitSkipStep2 BIT = 0,
@bitDisplayCategory BIT = 0,
@bitShowPriceUsingPriceLevel BIT = 0,
@bitEnableSecSorting BIT = 0,
@bitSortPriceMode   BIT=0,
@numPageSize    INT=15,
@numPageVariant  INT=6,
@bitAutoSelectWarehouse BIT =0,
@bitPreSellUp BIT = 0, 
@bitPostSellUp BIT = 0, 
@numDefaultClass NUMERIC(18,0) = 0,
@vcPreSellUp VARCHAR(200)=NULL,
@vcPostSellUp VARCHAR(200)=NULL,
@vcRedirectThankYouUrl VARCHAR(300)=NULL,
@tintPreLoginProceLevel TINYINT = 0,
@bitHideAddtoCart BIT=1,
@bitShowPromoDetailsLink BIT = 1,
@tintWarehouseAvailability TINYINT = 1,
@bitDisplayQtyAvailable BIT = 0,
@vcSalesOrderTabs VARCHAR(200)='',
@vcSalesQuotesTabs VARCHAR(200)='',
@vcItemPurchaseHistoryTabs VARCHAR(200)='',
@vcItemsFrequentlyPurchasedTabs VARCHAR(200)='',
@vcOpenCasesTabs VARCHAR(200)='',
@vcOpenRMATabs VARCHAR(200)='',
@bitSalesOrderTabs BIT=0,
@bitSalesQuotesTabs BIT=0,
@bitItemPurchaseHistoryTabs BIT=0,
@bitItemsFrequentlyPurchasedTabs BIT=0,
@bitOpenCasesTabs BIT=0,
@bitOpenRMATabs BIT=0,
@bitSupportTabs BIT=0,
@vcSupportTabs VARCHAR(200)='',
@bitElasticSearch BIT=0
as              
BEGIN	        
	IF NOT EXISTS(select 'col1' from eCommerceDTL where numDomainID=@numDomainID AND [numSiteId] = @numSiteID)
	BEGIN
		INSERT INTO eCommerceDTL
        (
			numDomainId,
            vcPaymentGateWay,
            vcPGWManagerUId,
            vcPGWManagerPassword,
            bitShowInStock,
            bitShowQOnHand,
            numDefaultWareHouseID,
            bitCheckCreditStatus,
            [numRelationshipId],
            [numProfileId],
            [bitHidePriceBeforeLogin],
            bitAuthOnlyCreditCard,
            bitSendEmail,
            vcGoogleMerchantID,
            vcGoogleMerchantKey,
            IsSandbox ,
            numAuthrizedOrderStatus,
			numSiteId,
			vcPaypalUserName,
			vcPaypalPassword,
			vcPaypalSignature,
			IsPaypalSandbox,
			bitSkipStep2,
			bitDisplayCategory,
			bitShowPriceUsingPriceLevel,
			bitEnableSecSorting,
			bitSortPriceMode,
			numPageSize,
			numPageVariant,
			bitAutoSelectWarehouse,
			[bitPreSellUp],
			[bitPostSellUp],
			numDefaultClass,
			vcPreSellUp,
			vcPostSellUp,
			vcRedirectThankYouUrl,
			tintPreLoginProceLevel,
			bitHideAddtoCart,
			bitShowPromoDetailsLink,
			tintWarehouseAvailability,
			bitDisplayQtyAvailable,
			bitElasticSearch
		)
		VALUES
		(
			@numDomainID,
            @vcPaymentGateWay,
            @vcPGWUserId,
            @vcPGWPassword,
            @bitShowInStock,
            @bitShowQOnHand,
            @numDefaultWareHouseID,
            @bitCheckCreditStatus,
            @numRelationshipId,
            @numProfileId,
            @bitHidePriceBeforeLogin,
            @bitAuthOnlyCreditCard,
            @bitSendMail,
            @vcGoogleMerchantID,
            @vcGoogleMerchantKey ,
            @IsSandbox,
            @numAuthrizedOrderStatus,
			@numSiteID,
			@vcPaypalUserName,
			@vcPaypalPassword,
			@vcPaypalSignature,
			@IsPaypalSandbox,
			@bitSkipStep2,
			@bitDisplayCategory,
			@bitShowPriceUsingPriceLevel,
			@bitEnableSecSorting,
			@bitSortPriceMode,
			@numPageSize,
			@numPageVariant,
			@bitAutoSelectWarehouse,
			@bitPreSellUp,
			@bitPostSellUp,
			@numDefaultClass,
			@vcPreSellUp,
			@vcPostSellUp,
			@vcRedirectThankYouUrl,
			@tintPreLoginProceLevel,
			@bitHideAddtoCart,
			@bitShowPromoDetailsLink,
			@tintWarehouseAvailability,
			@bitDisplayQtyAvailable,
			@bitElasticSearch
        )

		
		IF(@bitElasticSearch = 1)
		BEGIN
			IF(NOT EXISTS(SELECT 1 FROM eCommerceDTL where numSiteId <> @numSiteID AND numDomainId = @numDomainID AND bitElasticSearch = 1))
			BEGIN
				INSERT INTO ElasticSearchBizCart 
				(
					numDomainID
					,numRecordID
					,vcAction
					,vcModule
					,numSiteID
				)
				SELECT 
					@numDomainID
					,numItemID
					,'Insert'
					,'Item'
					,@numSiteID  
				FROM 
					SiteCategories
				INNER JOIN
					ItemCategory
				ON
					SiteCategories.numCategoryID = ItemCategory.numCategoryID
				INNER JOIN
					Item
				ON
					ItemCategory.numItemID = Item.numItemCode
				WHERE 
					SiteCategories.numSiteID = @numSiteID
					AND ISNULL(Item.IsArchieve,0) =0
				GROUP BY
					numItemID
			END
		END

		IF ISNULL((SELECT numDefaultSiteID FROM DOmain WHERE numDomainId=@numDomainID ),0) = 0
		BEGIN
			UPDATE Domain SET numDefaultSiteID=SCOPE_IDENTITY() WHERE numDomainId=@numDomainID
		END
	END
	ELSE
	BEGIN
		IF(@bitElasticSearch = 1)
		BEGIN
			INSERT INTO ElasticSearchBizCart 
			(
				numDomainID
				,numRecordID
				,vcAction
				,vcModule
				,numSiteID
			)
			SELECT 
				@numDomainID
				,numItemID
				,'Insert'
				,'Item'
				,@numSiteID  
			FROM 
				SiteCategories
			INNER JOIN
				ItemCategory
			ON
				SiteCategories.numCategoryID = ItemCategory.numCategoryID
			INNER JOIN
				Item
			ON
				ItemCategory.numItemID = Item.numItemCode
			WHERE 
				SiteCategories.numSiteID = @numSiteID
				AND ISNULL(Item.IsArchieve,0)=0
			GROUP BY
				numItemID
		END
		ELSE IF(EXISTS(SELECT 1 FROM eCommerceDTL where numSiteId = @numSiteID AND numDomainId = @numDomainID AND bitElasticSearch = 1))
		BEGIN
			DELETE FROM ElasticSearchBizCart WHERE numDomainID = @numDomainID And numSiteID = @numSiteID
		END	

		UPDATE 
			eCommerceDTL                                   
		SET              
			vcPaymentGateWay=@vcPaymentGateWay,                
			vcPGWManagerUId=@vcPGWUserId ,                
			vcPGWManagerPassword= @vcPGWPassword,              
			bitShowInStock=@bitShowInStock ,               
			bitShowQOnHand=@bitShowQOnHand,        
			bitCheckCreditStatus=@bitCheckCreditStatus ,
			[bitHidePriceBeforeLogin]=@bitHidePriceBeforeLogin,
			bitAuthOnlyCreditCard=@bitAuthOnlyCreditCard,
			bitSendEmail = @bitSendMail,
			vcGoogleMerchantID = @vcGoogleMerchantID ,
			vcGoogleMerchantKey =  @vcGoogleMerchantKey ,
			IsSandbox = @IsSandbox,
			numAuthrizedOrderStatus = @numAuthrizedOrderStatus,
			numSiteId = @numSiteID,
			vcPaypalUserName = @vcPaypalUserName ,
			vcPaypalPassword = @vcPaypalPassword,
			vcPaypalSignature = @vcPaypalSignature,
			IsPaypalSandbox = @IsPaypalSandbox,
			bitSkipStep2 = @bitSkipStep2,
			bitDisplayCategory = @bitDisplayCategory,
			bitShowPriceUsingPriceLevel = @bitShowPriceUsingPriceLevel,
			bitEnableSecSorting = @bitEnableSecSorting,
			bitSortPriceMode=@bitSortPriceMode,
			numPageSize=@numPageSize,
			numPageVariant=@numPageVariant,
			bitAutoSelectWarehouse=@bitAutoSelectWarehouse,
			[bitPreSellUp] = @bitPreSellUp,
			[bitPostSellUp] = @bitPostSellUp,
			vcPreSellUp=@vcPreSellUp,
			vcPostSellUp=@vcPostSellUp,
			vcRedirectThankYouUrl=@vcRedirectThankYouUrl,
			bitHideAddtoCart=@bitHideAddtoCart,
			bitShowPromoDetailsLink=@bitShowPromoDetailsLink,
			tintWarehouseAvailability=@tintWarehouseAvailability,
			bitDisplayQtyAvailable=@bitDisplayQtyAvailable,
			bitElasticSearch=@bitElasticSearch
		WHERE 
			numDomainId=@numDomainID 
			AND [numSiteId] = @numSiteID
		IF(@numDefaultWareHouseID>0)
		BEGIN
			UPDATE 
				eCommerceDTL                                   
			SET 
				numDefaultWareHouseID =@numDefaultWareHouseID
			WHERE 
				numDomainId=@numDomainID 
				AND [numSiteId] = @numSiteID
		END
		IF(@numRelationshipId>0)
		BEGIN
			UPDATE 
				eCommerceDTL                                   
			SET 
				numRelationshipId =@numRelationshipId
			WHERE 
				numDomainId=@numDomainID 
				AND [numSiteId] = @numSiteID
		END
		IF(@numDefaultClass>0)
		BEGIN
			UPDATE 
				eCommerceDTL                                   
			SET 
				numDefaultClass =@numDefaultClass
			WHERE 
				numDomainId=@numDomainID 
				AND [numSiteId] = @numSiteID
		END
		IF(@numProfileId>0)
		BEGIN
			UPDATE 
				eCommerceDTL                                   
			SET 
				numProfileId =@numProfileId
			WHERE 
				numDomainId=@numDomainID 
				AND [numSiteId] = @numSiteID
		END
		IF(@tintPreLoginProceLevel>0)
		BEGIN
			UPDATE 
				eCommerceDTL                                   
			SET 
				tintPreLoginProceLevel =@tintPreLoginProceLevel
			WHERE 
				numDomainId=@numDomainID 
				AND [numSiteId] = @numSiteID
		END

	END

	UPDATE
		Domain
	SET
		vcSalesOrderTabs=@vcSalesOrderTabs,
		vcSalesQuotesTabs=@vcSalesQuotesTabs,
		vcItemPurchaseHistoryTabs=@vcItemPurchaseHistoryTabs,
		vcItemsFrequentlyPurchasedTabs=@vcItemsFrequentlyPurchasedTabs,
		vcOpenCasesTabs=@vcOpenCasesTabs,
		vcOpenRMATabs=@vcOpenRMATabs,
		vcSupportTabs=@vcSupportTabs,
		bitSalesOrderTabs=@bitSalesOrderTabs,
		bitSalesQuotesTabs=@bitSalesQuotesTabs,
		bitItemPurchaseHistoryTabs=@bitItemPurchaseHistoryTabs,
		bitItemsFrequentlyPurchasedTabs=@bitItemsFrequentlyPurchasedTabs,
		bitOpenCasesTabs=@bitOpenCasesTabs,
		bitOpenRMATabs=@bitOpenRMATabs,
		bitSupportTabs=@bitSupportTabs
	WHERE
		numDomainId=@numDomainID
END
GO
GO
/****** Object:  StoredProcedure [dbo].[USP_ElasticSearchBizCart_Get]    Script Date: 09/04/2020 16:26:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ElasticSearchBizCart_Get' ) 
    DROP PROCEDURE USP_ElasticSearchBizCart_Get

GO
CREATE PROCEDURE [dbo].[USP_ElasticSearchBizCart_Get]

AS 
BEGIN
	DELETE FROM ElasticSearchBizCart WHERE vcAction != 'Delete' AND numRecordID NOT IN (SELECT I.numItemCode 
								FROM Item I
								INNER JOIN ItemCategory IC ON (IC.numItemID = I.NumItemCode)
								INNER JOIN Category C ON (C.numCategoryID = IC.numCategoryID)
								INNER JOIN CategoryProfile CP ON (CP.ID = C.numCategoryProfileID)
								INNER JOIN CategoryProfileSites CPS ON (CPS.numCategoryProfileID = CP.ID)
								INNER JOIN Sites S ON (S.numSiteID = CPS.numSiteID)
								INNER JOIN eCommerceDTL ECD ON (ECD.numSiteId = S.numSiteId AND ECD.bitElasticSearch = 1)
								WHERE ISNULL(I.IsArchieve,0)=0)

	DECLARE @TEMP TABLE
	(
		ID NUMERIC(18,0)
		,numDomainID NUMERIC(18,0)
		,vcModule VARCHAR(15)
		,numRecordID NUMERIC(18,0)
		,vcAction VARCHAR(10)
		,numSiteID NUMERIC(18,0)
	)
	INSERT INTO @TEMP (ID,numDomainID,vcModule,numRecordID,vcAction,numSiteID) 
	SELECT TOP 400 numElasticSearchBizCartID,numDomainID,vcModule,numRecordID,vcAction,numSiteID FROM ElasticSearchBizCart ORDER BY numElasticSearchBizCartID
	DELETE FROM ElasticSearchBizCart WHERE numElasticSearchBizCartID IN (SELECT ID FROM @TEMP)

	SELECT * FROM (
		SELECT MIN(ID) AS ID,T1.numDomainID,vcModule,numRecordID,vcAction,T1.numSiteId 
		FROM @TEMP T1 
		INNER JOIN eCommerceDTL ECD ON T1.numDomainID=ECD.numDomainId 
		WHERE ISNULL(ECD.bitElasticSearch,0)=1 AND vcAction='Insert' AND T1.numDomainID > 0 
		GROUP BY T1.numDomainID,vcModule,numRecordID,vcAction,T1.numSiteId) TEMP 
	ORDER BY ID
	
	
	SELECT * FROM (
		SELECT MIN(ID) AS ID,T1.numDomainID,vcModule,numRecordID,vcAction ,T1.numSiteId
		FROM @TEMP T1 
		INNER JOIN eCommerceDTL ECD ON T1.numDomainID=ECD.numDomainId 
		WHERE ISNULL(ECD.bitElasticSearch,0)=1 AND vcAction='Update' AND T1.numDomainID > 0 
		GROUP BY T1.numDomainID,vcModule,numRecordID,vcAction,T1.numSiteId) TEMP 
	ORDER BY ID
	
	SELECT * FROM (
		SELECT MIN(ID) AS ID,T1.numDomainID,vcModule,numRecordID,vcAction ,T1.numSiteId
		FROM @TEMP T1 
		INNER JOIN eCommerceDTL ECD ON T1.numDomainID=ECD.numDomainId 
		WHERE ISNULL(ECD.bitElasticSearch,0)=1 AND vcAction='Delete' AND T1.numDomainID > 0 
		GROUP BY T1.numDomainID,vcModule,numRecordID,vcAction,T1.numSiteId) TEMP 
	ORDER BY ID
END
GO
IF EXISTS(SELECT * FROM sysobjects SYSOBJ WHERE SYSOBJ.xtype='p'AND SYSOBJ.NAME ='USP_GET_DROPDOWN_DATA_FOR_IMPORT')
DROP PROCEDURE USP_GET_DROPDOWN_DATA_FOR_IMPORT
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- USP_GET_DROPDOWN_DATA_FOR_IMPORT 75,1
CREATE PROCEDURE USP_GET_DROPDOWN_DATA_FOR_IMPORT
(
	@intImportFileID BIGINT,
	@numDomainID BIGINT,
	@tintMode TINYINT,
	@numFormID NUMERIC(18,0),
	@vcFields VARCHAR(MAX)
)
AS 
BEGIN
	DECLARE @TempFields TABLE
	(
		numFieldID NUMERIC(18,0)
		,intMapColumnNo INT
		,bitCustom BIT
	)

	IF @tintMode = 2
	BEGIN
		DECLARE @hDocItem int
		EXEC sp_xml_preparedocument @hDocItem OUTPUT, @vcFields

		INSERT INTO @TempFields
		(
			numFieldID
			,intMapColumnNo
			,bitCustom
		)
		SELECT
			FormFieldID
			,ImportFieldID
			,IsCustomField
		FROM
			OPENXML (@hDocItem,'/NewDataSet/Table1',2)
		WITH
		(
			FormFieldID NUMERIC(18,0)
			,ImportFieldID NUMERIC(18,0)
			,IsCustomField BIT
		)
	
		EXEC sp_xml_removedocument @hDocItem
	END
	ELSE
	BEGIN
		SELECT @numFormID = numMasterID  FROM dbo.Import_File_Master  WHERE intImportFileID=@intImportFileID
	
		INSERT INTO @TempFields
		(
			numFieldID
			,intMapColumnNo
			,bitCustom
		)
		SELECT
			numFormFieldID
			,intImportFileID
			,ISNULL(bitCustomField,0)
		FROM
			Import_File_Field_Mapping
		WHERE
			intImportFileID=@intImportFileID
	END

	SELECT 
		ROW_NUMBER() OVER (ORDER BY intMapColumnNo) AS [ID]
		,* 
	INTO 
		#tempDropDownData 
	FROM
	(
		SELECT  
			DFFM.numFieldId [numFormFieldId],
			DFFM.vcAssociatedControlType,
			DYNAMIC_FIELD.vcDbColumnName,
			DFFM.vcFieldName [vcFormFieldName],
			FIELD_MAP.intMapColumnNo,
			DYNAMIC_FIELD.numListID,
			0 AS [bitCustomField]					
		FROM 
			@TempFields FIELD_MAP
		INNER JOIN dbo.DycFormField_Mapping DFFM ON FIELD_MAP.numFieldID = DFFM.numFieldId
		INNER JOIN dbo.DycFieldMaster DYNAMIC_FIELD ON DYNAMIC_FIELD.numFieldId = DFFM.numFieldId
		WHERE 
			ISNULL(FIELD_MAP.bitCustom,0) = 0
			AND DFFM.vcAssociatedControlType ='SelectBox'  
			AND DFFM.numFormID = @numFormID	
		UNION ALL
		SELECT 
			CFm.Fld_id AS [numFormFieldId],
			CASE WHEN CFM.Fld_type = 'Drop Down List Box'
					THEN 'SelectBox'
					ELSE CFM.Fld_type 
			END	 AS [vcAssociatedControlType],
			cfm.Fld_label AS [vcDbColumnName],
			cfm.Fld_label AS [vcFormFieldName],
			FIELD_MAP.intMapColumnNo  AS [intMapColumnNo],
			CFM.numlistid AS [numListID],
			1 AS [bitCustomField]
		FROM 
			dbo.CFW_Fld_Master CFM
		INNER JOIN @TempFields FIELD_MAP ON CFM.Fld_id = FIELD_MAP.numFieldID AND ISNULL(FIELD_MAP.bitCustom,0) = 0		
		WHERE 
			numDomainID = @numDomainID
	) TABLE1 
	WHERE  
		vcAssociatedControlType = 'SelectBox' 
              
	SELECT * FROM #tempDropDownData   
	
	DECLARE @intRow AS INT
	SELECT @intRow = COUNT(*) FROM #tempDropDownData
	PRINT @intRow
	
	DECLARE @intCnt AS INT
	DECLARE @numListIDValue AS BIGINT
	DECLARE @numFormFieldIDValue AS BIGINT
	DECLARE @bitCustomField AS BIT
		
	DECLARE @strSQL AS NVARCHAR(MAX)
	DECLARE @vcTableNames AS VARCHAR(MAX) 
	DECLARE @strDBColumnName AS NVARCHAR(1000)
	
	SET @intCnt = 0
	SET @strSQL = '' 
	SET @vcTableNames = ''

	CREATE TABLE #tempGroups
	(
		[Key] NUMERIC(9),
		[Value] VARCHAR(100)
	) 	
	INSERT INTO #tempGroups([Key],[Value]) 
	SELECT 0 AS [Key],'Groups' AS [Value]
	UNION ALL
	SELECT 1 AS [Key],'Prospects' AS [Value]
	UNION ALL
	SELECT 1 AS [Key],'Prospect' AS [Value]
	UNION ALL
	SELECT 1 AS [Key],'Pros' AS [Value]
	UNION ALL
	SELECT 2 AS [Key],'Accounts' AS [Value]
	UNION ALL
	SELECT 2 AS [Key],'Account' AS [Value]
	UNION ALL
	SELECT 2 AS [Key],'Acc' AS [Value]

	--SELECT * FROM #tempGroups
	
	WHILE(@intCnt < @intRow   )
		BEGIN
			SET @intCnt = @intCnt + 1
				
			SELECT  
					@numListIDValue = numListID,
					@strDBColumnName = vcDbColumnName, 
					@numFormFieldIDValue = numFormFieldID,
					@bitCustomField = bitCustomField 	 
			FROM #tempDropDownData 
			WHERE ID = @intCnt
			
			PRINT 'BIT'
			PRINT @bitCustomField
			PRINT @strDBColumnName
			SELECT @strSQL = CASE  
								WHEN @strDBColumnName = 'numItemGroup'
								THEN @strSQL + 'SELECT ITEM_GROUP.numItemGroupID AS [Key],ITEM_GROUP.vcItemGroup AS [Value] FROM dbo.ItemGroups ITEM_GROUP WHERE ITEM_GROUP.numDomainID = ' + CONVERT(VARCHAR,@numDomainID)
								
								WHEN @strDBColumnName = 'tintCRMType'
								THEN @strSQL + 'SELECT DISTINCT [Key],[Value] FROM #tempGroups '
								
								WHEN @strDBColumnName = 'vcLocation'
								THEN @strSQL + 'SELECT numWLocationID AS [Key],vcLocation AS [Value] FROM dbo.WarehouseLocation WHERE numDomainID = ' + CONVERT(VARCHAR,@numDomainID)
								
								WHEN @strDBColumnName = 'vcExportToAPI'
								THEN @strSQL + 'SELECT DISTINCT WebApiId AS [Key],vcProviderName AS [Value] FROM dbo.WebAPI '
								
								WHEN @strDBColumnName = 'numCategoryID'
								THEN @strSQL + 'SELECT numCategoryID AS [Key], vcCategoryName AS [Value] FROM dbo.Category WHERE numDomainID =' + CONVERT(VARCHAR,@numDomainID)
								
								WHEN @strDBColumnName = 'numGrpID'
								THEN @strSQL + 'SELECT GRP.numGrpID AS [Key],GRP.vcGrpName AS [Value] FROM dbo.Groups GRP ' 
											    			    
								WHEN @strDBColumnName = 'numWareHouseID' --OR @strDBColumnName = 'numWareHouseItemID'															  
								THEN @strSQL + 'SELECT WAREHOUSE.numWareHouseID AS [Key],WAREHOUSE.vcWareHouse AS [Value] FROM dbo.Warehouses WAREHOUSE WHERE WAREHOUSE.numDomainID = '+ CONVERT(VARCHAR,@numDomainID) 
								
								WHEN @strDBColumnName = 'numWareHouseItemID'
								THEN @strSQL + 'SELECT WI.numWareHouseItemID AS [Key],W.vcWareHouse AS [Value] FROM dbo.WareHouseItems WI INNER JOIN dbo.Warehouses W ON WI.numWareHouseID = W.numWareHouseID WHERE W.numDomainID = '+ CONVERT(VARCHAR,@numDomainID)
												
								WHEN @strDBColumnName = 'numChildItemID' OR @strDBColumnName = 'numItemKitID'
								THEN @strSQL + ' SELECT ITEM.numItemCode AS [Key],ITEM.vcItemName AS [Value] FROM dbo.Item ITEM WHERE ITEM.numDomainID = '+ CONVERT(VARCHAR,@numDomainID)
								
								WHEN (@strDBColumnName = 'numUOMId' OR @strDBColumnName = 'numPurchaseUnit' OR @strDBColumnName = 'numSaleUnit' OR @strDBColumnName = 'numBaseUnit') 
								THEN @strSQL + '  SELECT numUOMId AS [Key], vcUnitName AS [Value] FROM dbo.UOM
												  JOIN Domain d ON dbo.UOM.numDomainID = d.numDomainID 
												  WHERE dbo.UOM.numDomainId = ' + CONVERT(VARCHAR(10),@numDomainID) +' 
												  AND bitEnabled=1 
												  AND dbo.UOM.tintUnitType=(CASE WHEN ISNULL(d.charUnitSystem,''E'')=''E'' THEN 1 WHEN d.charUnitSystem=''M'' THEN 2 END) '	
								
								WHEN (@strDBColumnName = 'numIncomeChartAcntId' OR @strDBColumnName = 'numAssetChartAcntId' OR @strDBColumnName = 'numCOGsChartAcntId')
								THEN @strSQL + '  SELECT numAccountId AS [Key], LTRIM(RTRIM(REPLACE(vcAccountName,ISNULL(vcNumber,''''),''''))) AS [Value] FROM dbo.Chart_Of_Accounts WHERE numDomainID= ' + CONVERT(VARCHAR(10),@numDomainID)

								WHEN @strDBColumnName = 'numCampaignID'
								THEN @strSQL + '  SELECT numCampaignID AS [Key], vcCampaignName AS [Value] FROM  CampaignMaster WHERE CampaignMaster.numDomainID = ' + CONVERT(VARCHAR(10),@numDomainID)
								
								WHEN @strDBColumnName = 'numVendorID'
								THEN @strSQL + '  SELECT DM.numDivisionID AS [Key], CI.vcCompanyName AS [Value] FROM dbo.CompanyInfo CI INNER JOIN dbo.DivisionMaster DM ON DM.numCompanyID = CI.numCompanyId 
												  WHERE DM.numDomainID = ' + CONVERT(VARCHAR(10),@numDomainID)
								
								WHEN @strDBColumnName = 'numAssignedTo' OR @strDBColumnName = 'numRecOwner' 
								THEN @strSQL + '  SELECT A.numContactID AS [Key], A.vcFirstName+'' ''+A.vcLastName AS [Value] from UserMaster UM join AdditionalContactsInformation A  on UM.numUserDetailId=A.numContactID  where UM.numDomainID = A.numDomainID AND UM.numDomainID = ' + CONVERT(VARCHAR,@numDomainID)	
								
------								 SELECT A.numContactID,A.vcFirstName+' '+A.vcLastName as vcUserName from UserMaster UM         
------								 join AdditionalContactsInformation A        
------								 on UM.numUserDetailId=A.numContactID          
------								 where UM.numDomainID=@numDomainID and UM.numDomainID=A.numDomainID   
							
								WHEN @strDBColumnName = 'numTermsID'
								THEN @strSQL + 'SELECT DISTINCT numTermsID AS [Key],vcTerms AS [Value] FROM BillingTerms 
												WHERE numDomainID = ' + CONVERT(VARCHAR,@numDomainID) 
												
								WHEN @strDBColumnName = 'numShipState' OR @strDBColumnName = 'numBillState' OR @strDBColumnName = 'numState' OR @strDBColumnName = 'State'
								THEN @strSQL + '  SELECT numStateID AS [Key], vcState AS [Value], (CASE WHEN LEN(ISNULL(vcAbbreviations,'''')) > 0 THEN vcAbbreviations ELSE ''NOTAVAILABLE'' END) vcAbbreviations FROM State where numDomainID=' + CONVERT(VARCHAR,@numDomainID)
								
								WHEN @strDBColumnName = 'charItemType'
								THEN @strSQL + ' SELECT ''A'' AS [Key], ''asset'' AS [Value]
											     UNION 				
											     SELECT ''P'' AS [Key], ''inventory item'' AS [Value]
											     UNION 				
											     SELECT ''S'' AS [Key], ''service'' AS [Value]
											     UNION 				
											     SELECT ''N'' AS [Key], ''non-inventory item'' AS [Value]'	
								
								WHEN @strDBColumnName = 'tintRuleType'
								THEN @strSQL + ' SELECT 1 AS [Key], ''Deduct from List price'' AS [Value]
											     UNION 				
											     SELECT 2 AS [Key], ''Add to Primary Vendor Cost'' AS [Value]
											     UNION 				
											     SELECT 3 AS [Key], ''Named Price'' AS [Value]'	
											     
								WHEN @strDBColumnName = 'tintDiscountType'
								THEN @strSQL + ' SELECT 1 AS [Key], ''Percentage'' AS [Value]
											     UNION 				
											     SELECT 2 AS [Key], ''Flat Amount'' AS [Value]
												 UNION 				
											     SELECT 3 AS [Key], ''Named Price'' AS [Value]'
												 											     
								WHEN @strDBColumnName = 'numShippingService'
								THEN @strSQL + 'SELECT 
													ShippingService.numShippingServiceID AS [Key]
													,ShippingService.vcShipmentService AS [Value]
													,ISNULL(ShippingServiceAbbreviations.vcAbbreviation,'''') vcAbbreviations
												FROM 
													ShippingService 
												LEFT JOIN
													ShippingServiceAbbreviations
												ON
													ShippingServiceAbbreviations.numShippingServiceID = ShippingService.numShippingServiceID
												WHERE 
													ShippingService.numDomainID=' + CONVERT(VARCHAR,@numDomainID) + ' OR ISNULL(ShippingService.numDomainID,0) = 0'					     																		    
								WHEN @strDBColumnName = 'numChartAcntId'
								THEN @strSQL + 'SELECT 
													numAccountId AS [Key]
													,vcAccountName AS [Value] 
												FROM 
													Chart_Of_Accounts 
												WHERE 
													numDomainId=' + CONVERT(VARCHAR,@numDomainID)
								ELSE 
									 @strSQL + ' SELECT LIST.numListItemID AS [Key],vcData AS [Value] FROM ListDetails LIST LEFT JOIN listorder LIST_ORDER ON LIST.numListItemID = LIST_ORDER.numListItemID AND LIST_ORDER.numDomainId = ' + CONVERT(VARCHAR,@numDomainID) + ' WHERE ( LIST.numDomainID = ' + CONVERT(VARCHAR,@numDomainID) + ' OR LIST.constFlag = 1) AND LIST.numListID = ' + CONVERT(VARCHAR,@numListIDValue) + ' AND  (LIST.numDomainID = ' + CONVERT(VARCHAR,@numDomainID) + ' OR constFlag = 1 )  ORDER BY intSortOrder '
						     END						
						     
			SET @vcTableNames = @vcTableNames + CONVERT(VARCHAR,@numFormFieldIDValue) + '_' + @strDBColumnName + ',' 
		END
	
	PRINT @vcTableNames
	PRINT @strSQL	
	
	SELECT @vcTableNames AS TableNames
	EXEC SP_EXECUTESQL @strSQL		
	
	DROP TABLE #tempDropDownData
	DROP TABLE #tempGroups
END
GO
/****** Object:  StoredProcedure [dbo].[USP_GET_DYNAMIC_IMPORT_FIELDS]  ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO

IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GET_DYNAMIC_IMPORT_FIELDS' ) 
    DROP PROCEDURE USP_GET_DYNAMIC_IMPORT_FIELDS
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- USP_GET_DYNAMIC_IMPORT_FIELDS 48,2,40,1
CREATE PROCEDURE [dbo].[USP_GET_DYNAMIC_IMPORT_FIELDS]
    (
      @numFormID BIGINT,
      @intMode INT,
      @numImportFileID NUMERIC = 0,
      @numDomainID NUMERIC = 0,
	  @tintInsertUpdateType TINYINT = 0,
	  @tintItemLinkingID NUMERIC(18,0) = 0
    )
AS 
BEGIN
		
    IF @intMode = 1 -- TO BIND CATEGORY DROP DOWN
    BEGIN
        SELECT 20 AS [intImportMasterID],'Item' AS [vcImportName]
        UNION ALL
        SELECT  48 AS [intImportMasterID],'Item WareHouse' AS [vcImportName]
        UNION ALL
        SELECT  31 AS [intImportMasterID],'Kit sub-items' AS [vcImportName]
        UNION ALL
        SELECT  54 AS [intImportMasterID],'Item Images' AS [vcImportName]
        UNION ALL
        SELECT  55 AS [intImportMasterID],'Item Vendor' AS [vcImportName]
        UNION ALL
        SELECT  133 AS [intImportMasterID],'Organization' AS [vcImportName]
        UNION ALL
		SELECT  134 AS [intImportMasterID],'Contact' AS [vcImportName]
        UNION ALL
        SELECT  43 AS [intImportMasterID],'Organization Correspondence' AS [vcImportName]                        
        UNION ALL
        SELECT  98 AS [intImportMasterID],'Address Details' AS [vcImportName]
		UNION ALL
        SELECT  136 AS [intImportMasterID],'Import Sales Order(s)' AS [vcImportName]
		UNION ALL
        SELECT  137 AS [intImportMasterID],'Import Purchase Order(s)' AS [vcImportName]
		UNION ALL
        SELECT  140 AS [intImportMasterID],'Import Accounting Entries' AS [vcImportName]           		
    END
	
    IF @intMode = 2 -- TO GET DETAILS OF DYNAMIC FORM FIELDS
    BEGIN
        SELECT DISTINCT
            intSectionID AS [Id],
            vcSectionName AS [Data]
        FROM 
			dbo.DycFormSectionDetail
        WHERE 
			numFormID = @numFormID			
		
        SELECT 
			ROW_NUMBER() OVER (ORDER BY X.intSectionID, X.numFormFieldID) AS SrNo
			,X.*
        FROM  
			(SELECT    
				ISNULL((SELECT 
							intImportTransactionID
						FROM 
							dbo.Import_File_Field_Mapping FFM
						INNER JOIN 
							Import_File_Master IFM 
						ON 
							IFM.intImportFileID = FFM.intImportFileID
							AND IFM.intImportFileID = @numImportFileID
						WHERE 
							FFM.numFormFieldID = DCOL.numFieldID),0) AS [intImportFieldID]
				,DCOL.numFieldID AS [numFormFieldID]
				,'' AS [vcSectionNames]
				,DCOL.vcFieldName AS [vcFormFieldName]
				,DCOL.vcDbColumnName
				,DCOL.intSectionID AS [intSectionID]
				,0 [IsCustomField]
				,ISNULL((SELECT 
							intMapColumnNo
						FROM 
							dbo.Import_File_Field_Mapping FFM
						WHERE  
							1 = 1
							AND FFM.numFormFieldID = DCOL.numFieldID
							AND FFM.intImportFileID = @numImportFileID),0) intMapColumnNo
				,DCOL.vcPropertyName
				,CASE WHEN ISNULL(DCOL.bitRequired, 0) = 0 THEN '' ELSE '*' END AS [vcReqString]
				,DCOL.bitRequired AS [bitRequired]
				,CAST(DCOL.numFieldID AS VARCHAR) + '~' + CAST(0 AS VARCHAR(10)) AS [keyFieldID]
				,DCOL.tintOrder
			FROM 
				dbo.View_DynamicDefaultColumns DCOL
            WHERE 
				DCOL.numDomainID = @numDomainID
                AND DCOL.numFormID = @numFormID
                AND ISNULL(DCOL.bitImport, 0) = 1
                AND ISNULL(DCOL.bitDeleted, 0) = 0
				AND 1 = (CASE 
							WHEN @numFormID=20 THEN (CASE WHEN DCOL.numFieldID IN (189,294,271,272,270) THEN 0 ELSE 1 END)
							ELSE 1
						END)
            UNION ALL
            SELECT 
				-1
				,CAST(CFm.Fld_id AS NUMERIC(18,0))
				,'CustomField'
				,cfm.Fld_label
				,cfm.Fld_label
				,DFSD.intSectionId
				,1
				,ISNULL((SELECT 
							intMapColumnNo
                        FROM 
							dbo.Import_File_Field_Mapping IIFM
                        LEFT JOIN 
							dbo.DycFormField_Mapping DMAP 
						ON 
							IIFM.numFormFieldID = DMAP.numFieldID
                        WHERE 
							IIFM.intImportFileID = @numImportFileID
                            AND IIFM.numFormFieldID = CFM.Fld_id
                            AND bitCustomField = 1
                            AND ISNULL(DMAP.bitImport, 0) = 1),0) intMapColumnNo
				,'' AS [vcPropertyName]
				,'' AS [vcReqString]
				,0 AS [bitRequired]
				,CAST(CFm.Fld_id AS VARCHAR) + '~' + CAST(1 AS VARCHAR(10)) AS [keyFieldID]
				,100 AS [tintOrder]
            FROM 
				dbo.CFW_Fld_Master CFM
                LEFT JOIN dbo.CFW_Loc_Master LOC ON CFM.Grp_id = LOC.Loc_id
                LEFT JOIN dbo.DycFormSectionDetail DFSD ON DFSD.Loc_id = LOC.Loc_id
            WHERE 
				Grp_id = DFSD.Loc_Id
                AND numDomainID = @numDomainID
                AND (DFSD.numFormID = @numFormID OR DFSD.numFormID = (CASE WHEN @numFormID = 20 THEN 48 ELSE @numFormID END))
        ) X
		ORDER BY 
			X.tintOrder,
			X.intSectionID        		
        
        
		SELECT 
			ROW_NUMBER() OVER ( ORDER BY X.intSectionID, X.numFormFieldID ) AS SrNo
			,X.*
		FROM 
			(SELECT 
				ISNULL((SELECT 
							intImportTransactionID
						FROM 
							dbo.Import_File_Field_Mapping FFM
						INNER JOIN 
							Import_File_Master IFM 
						ON 
							IFM.intImportFileID = FFM.intImportFileID
							AND IFM.intImportFileID = @numImportFileID
						WHERE 
							FFM.numFormFieldID = DCOL.numFieldID),0) AS [intImportFieldID]
				,DCOL.numFieldID AS [numFormFieldID]
				,'' AS [vcSectionNames]
				,DCOL.vcFieldName AS [vcFormFieldName]
				,DCOL.vcDbColumnName
				,DCOL.intSectionID AS [intSectionID]
				,0 [IsCustomField]
				,ISNULL((SELECT 
							intMapColumnNo
						FROM 
							dbo.Import_File_Field_Mapping FFM
						WHERE 
							1 = 1
							AND FFM.numFormFieldID = DCOL.numFieldID
							AND FFM.intImportFileID = @numImportFileID), 0) intMapColumnNo
				,DCOL.vcPropertyName
				,CASE 
					WHEN @numFormID = 20 
					THEN CASE 
							WHEN DCOl.numFieldID IN (189,294,271,272,270,(CASE @tintItemLinkingID
																						WHEN 1 THEN 211
																						WHEN 2 THEN 281
																						WHEN 3 THEN 203
																						WHEN 4 THEN 193
																						ELSE 0 
																					END)) THEN '*' ELSE (CASE WHEN ISNULL(DCOL.bitRequired, 0) = 0 THEN '' ELSE '*' END) END
					WHEN @numFormID = 133
					THEN CASE 
							WHEN DCOl.numFieldID IN (3,6,380,451,(CASE @tintItemLinkingID
																						WHEN 1 THEN 380
																						WHEN 2 THEN 539
																						ELSE 0 
																					END)) THEN '*' ELSE (CASE WHEN ISNULL(DCOL.bitRequired, 0) = 0 THEN '' ELSE '*' END) END
					WHEN @numFormID = 134
					THEN CASE 
							WHEN DCOl.numFieldID IN (51,52,386,(CASE @tintItemLinkingID
																	WHEN 3 THEN 380
																	WHEN 4 THEN 539
																	ELSE 0 
																END)) THEN '*' ELSE (CASE WHEN ISNULL(DCOL.bitRequired, 0) = 0 THEN '' ELSE '*' END) END
					WHEN @numFormID IN (136,137)
					THEN CASE WHEN DCOL.numFieldID IN (258,259,293,ISNULL(@tintItemLinkingID,0)) THEN '*' ELSE (CASE WHEN ISNULL(DCOL.bitRequired, 0) = 0 THEN '' ELSE '*' END) END
					ELSE (CASE WHEN ISNULL(DCOL.bitRequired, 0) = 0 THEN '' ELSE '*' END)
				END AS [vcReqString]
				,CASE 
					WHEN @numFormID = 20 
					THEN CASE 
							WHEN DCOl.numFieldID IN (189,294,271,272,270,(CASE @tintItemLinkingID
																						WHEN 1 THEN 211
																						WHEN 2 THEN 281
																						WHEN 3 THEN 203
																						WHEN 4 THEN 193
																						ELSE 0 
																					END)) THEN 1 ELSE ISNULL(DCOL.bitRequired,0) END
					WHEN @numFormID = 133
					THEN CASE 
							WHEN DCOl.numFieldID IN (3,6,380,451,(CASE @tintItemLinkingID
																						WHEN 1 THEN 380
																						WHEN 2 THEN 539
																						ELSE 0 
																					END)) THEN 1 ELSE ISNULL(DCOL.bitRequired,0) END
					WHEN @numFormID = 134
					THEN CASE 
							WHEN DCOl.numFieldID IN (51,52,386,(CASE @tintItemLinkingID
																	WHEN 3 THEN 380
																	WHEN 4 THEN 539
																	ELSE 0 
																END)) THEN 1 ELSE ISNULL(DCOL.bitRequired,0) END
					WHEN @numFormID IN (136,137)
					THEN CASE WHEN DCOL.numFieldID IN (258,259,293,ISNULL(@tintItemLinkingID,0)) THEN 1 ELSE ISNULL(DCOL.bitRequired,0) END
					ELSE DCOL.bitRequired
				END AS [bitRequired]
				,CAST(DCOL.numFieldID AS VARCHAR) + '~' + CAST(0 AS VARCHAR(10)) AS [keyFieldID]
				,ISNULL(DCOL.tintColumn, 0) AS SortOrder
				,ISNULL(DCOL.tintRow,0) AS tintRow
			FROM 
				dbo.View_DynamicColumns DCOL
			WHERE 
				DCOL.numDomainID = @numDomainID
				AND DCOL.numFormID = @numFormID
				AND ISNULL(DCOL.bitImport, 0) = 1
				AND ISNULL(tintPageType, 0) = 4
			UNION ALL
			SELECT    
				ISNULL((SELECT 
							intImportTransactionID
						FROM 
							dbo.Import_File_Field_Mapping FFM
						INNER JOIN 
							Import_File_Master IFM 
						ON 
							IFM.intImportFileID = FFM.intImportFileID
							AND IFM.intImportFileID = @numImportFileID
						WHERE 
							FFM.numFormFieldID = DCOL.numFieldID),0) AS [intImportFieldID]
				,DCOL.numFieldID AS [numFormFieldID]
				,'' AS [vcSectionNames]
				,DCOL.vcFieldName AS [vcFormFieldName]
				,DCOL.vcDbColumnName
				,DCOL.intSectionID AS [intSectionID]
				,0 [IsCustomField]
				,ISNULL((SELECT 
							intMapColumnNo
						FROM 
							dbo.Import_File_Field_Mapping FFM
						WHERE  
							1 = 1
							AND FFM.numFormFieldID = DCOL.numFieldID
							AND FFM.intImportFileID = @numImportFileID),0) intMapColumnNo
				,DCOL.vcPropertyName
				,'*' AS [vcReqString]
				,1 AS [bitRequired]
				,CAST(DCOL.numFieldID AS VARCHAR) + '~' + CAST(0 AS VARCHAR(10)) AS [keyFieldID]
				,0 AS SortOrder
				,DCOL.tintOrder
			FROM 
				dbo.View_DynamicDefaultColumns DCOL
            WHERE 
				DCOL.numDomainID = @numDomainID
                AND DCOL.numFormID = @numFormID
                AND ISNULL(DCOL.bitImport, 0) = 1
                AND ISNULL(DCOL.bitDeleted, 0) = 0
				AND DCOL.numFieldID NOT IN (SELECT 
												VDC.numFieldID 
											FROM 
												dbo.View_DynamicColumns VDC
											WHERE 
												VDC.numDomainID = @numDomainID
												AND VDC.numFormID = @numFormID
												AND ISNULL(VDC.bitImport, 0) = 1
												AND ISNULL(VDC.tintPageType, 0) = 4)
				AND @tintInsertUpdateType IN (2,3)
				AND 1 = (CASE 
							WHEN @numFormID=20
							THEN (CASE 
									WHEN DCOL.numFieldID IN (189,294,271,272,270,(CASE @tintItemLinkingID
																						WHEN 1 THEN 211
																						WHEN 2 THEN 281
																						WHEN 3 THEN 203
																						WHEN 4 THEN 193
																						ELSE 0 
																					END)) 
									THEN 1 
									ELSE 0 
								END)
							WHEN @numFormID=133
							THEN (CASE 
									WHEN DCOL.numFieldID IN (3,6,380,451,(CASE @tintItemLinkingID
																				WHEN 1 THEN 380
																				WHEN 2 THEN 539
																				ELSE 0 
																			END)) 
									THEN 1 
									ELSE 0 
								END)
							WHEN @numFormID = 134
							THEN (CASE 
									WHEN DCOl.numFieldID IN (51,52,386,(CASE @tintItemLinkingID
																			WHEN 3 THEN 380
																			WHEN 4 THEN 539
																			ELSE 0 
																		END)) 
									THEN 1 
									ELSE 0
								END)
							WHEN @numFormID IN (136,137)
							THEN (CASE WHEN DCOL.numFieldID IN (258,259,293,ISNULL(@tintItemLinkingID,0)) THEN 1 ELSE 0 END)
							WHEN @numFormID IN (140)
							THEN (CASE WHEN DCOL.numFieldID IN (931,932,933,934) THEN 1 ELSE 0 END)
							ELSE 0
						END)
			UNION ALL
			SELECT 
				-1
				,CAST(CFm.Fld_id AS NUMERIC(18,0))
				,'CustomField'
				,cfm.Fld_label
				,cfm.Fld_label
				,DFSD.intSectionId
				,1
				,ISNULL((SELECT 
							intMapColumnNo
						FROM 
							dbo.Import_File_Field_Mapping IIFM
						LEFT JOIN 
							dbo.DycFormField_Mapping DMAP 
						ON 
							IIFM.numFormFieldID = DMAP.numFieldID
						WHERE 
							IIFM.intImportFileID = @numImportFileID
							AND IIFM.numFormFieldID = CFM.Fld_id
							AND bitCustomField = 1
							AND ISNULL(DMAP.bitImport, 0) = 1),0) intMapColumnNo
				,'' AS [vcPropertyName]
				,'' AS [vcReqString]
				,0 AS [bitRequired]
				,CAST(CFm.Fld_id AS VARCHAR) + '~' + CAST(1 AS VARCHAR(10)) AS [keyFieldID]
				,DFCD.intColumnNum AS [SortOrder]
				,ISNULL(DFCD.intRowNum,0) AS tintRow
			FROM      
				dbo.CFW_Fld_Master CFM
			LEFT JOIN dbo.CFW_Loc_Master LOC ON CFM.Grp_id = LOC.Loc_id
			LEFT JOIN dbo.DycFormSectionDetail DFSD ON DFSD.Loc_id = LOC.Loc_id
			JOIN DycFormConfigurationDetails DFCD ON DFCD.numFieldID = CFM.Fld_Id AND DFCD.numFormID = @numFormID AND DFCD.bitCustom = 1
			WHERE 
				Grp_id = DFSD.Loc_Id
				AND DFCD.numDomainID = @numDomainID
				AND (DFSD.numFormID = @numFormID OR DFSD.numFormID = (CASE WHEN @numFormID = 20 THEN 48 ELSE @numFormID END))
				AND (DFCD.tintPageType = 4 OR DFCD.tintPageType IS NULL)                          
		) X
		ORDER BY 
			X.SortOrder
			,X.intSectionID 
	END	
END
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GET_IMPORT_MAPPED_DATA')
DROP PROCEDURE USP_GET_IMPORT_MAPPED_DATA
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- SELECT * FROM dbo.Import_File_Master
-- USP_GET_IMPORT_MAPPED_DATA 39,1,48
CREATE PROCEDURE [dbo].[USP_GET_IMPORT_MAPPED_DATA] 
(
	@intImportFileID	BIGINT,
	@numDomainID		NUMERIC(18,0),
	@numFormID			NUMERIC(18,0)
)
AS 
BEGIN
	DECLARE @strSQL AS NVARCHAR(MAX)
	DECLARE @strWHERE AS VARCHAR(MAX)
	
	SET @strWHERE = ' WHERE 1=1 '
	
	IF ISNULL(@intImportFileID,0) <> 0
		BEGIN
			SET @strWHERE = @strWHERE + ' AND PARENT.intImportFileID = ' + CONVERT(VARCHAR,@intImportFileID)
		END
		
		SET @strSQL = 'SELECT   PARENT.intImportFileID AS [ImportFileID],
			   					CONCAT((CASE PARENT.tintImportType WHEN 1 THEN ''Update'' WHEN 2 THEN ''Import'' ELSE ''Import/Update'' END),'' ('',IMPORT.vcFormName,'')'') AS [ImportType],
								PARENT.vcImportFileName AS [ImportFileName],
								(CASE 
									WHEN PARENT.tintStatus = 0 THEN ''Pending ''
									WHEN PARENT.tintStatus = 1 THEN ''Completed''
									WHEN PARENT.tintStatus = 2 THEN ''Pending''	
									WHEN PARENT.tintStatus = 3 THEN ''Incomplete''	
								END) AS [Status],								
								dbo.FormatedDateTimeFromDate(PARENT.dtCreateDate,numDomainId) AS [CreatedDate],
								dbo.fn_GetContactName(PARENT.numUserContactID) AS [CreatedBy] ,
								ISNULL(
										CASE WHEN ISNULL(tintStatus,0) = 1
											 THEN CONCAT((CASE WHEN ISNULL(numRecordAdded,0) <> 0 THEN CAST(numRecordAdded AS VARCHAR) + '' records added. '' ELSE '''' END),
												  (CASE WHEN ISNULL(numRecordUpdated,0) <> 0 THEN CAST(numRecordUpdated AS VARCHAR) + '' records updated. '' ELSE '''' END) ,
												  (CASE WHEN ISNULL(numErrors,0) <> 0 THEN ''Error occured in '' + CAST(numErrors AS VARCHAR) + '' records. '' ELSE '''' END))
												 
											 ELSE	''''
												  
											END	  	   
								,''-'') AS [Records],
								PARENT.tintStatus AS [intStatus],
								PARENT.numMasterID AS [MasterID],
								ISNULL(HISTORY.vcHistory_Added_Value,'''') AS [ItemCodes],
								ISNULL(numRecordAdded,0) AS numRecordAdded,
								ISNULL(numRecordUpdated,0) AS numRecordUpdated,
								ISNULL(CASE WHEN (ISNULL(tintStatus,0) = 1 OR ISNULL(tintStatus,0) = 3)
											THEN 
											     CASE WHEN ISNULL(numRecordAdded,0) > 0 AND ISNULL(numRecordUpdated,0) = 0 THEN 1 
											          ELSE 0
												 END
									   END,0)	AS [IsForRollback]
									   ,ISNULL(PARENT.tintItemLinkingID,0) tintItemLinkingID,tintImportType
						FROM dbo.Import_File_Master PARENT 
						INNER JOIN dbo.DynamicFormMaster IMPORT ON PARENT.numMasterID = IMPORT.numFormId
						LEFT JOIN dbo.Import_History HISTORY ON HISTORY.intImportFileID = PARENT.intImportFileID '
						+ @strWHERE + ' AND PARENT.numDomainId = ' + CONVERT(VARCHAR,@numDomainID) +
						' ORDER BY PARENT.intImportFileID DESC'
		PRINT @strSQL
		EXEC SP_EXECUTESQL @strSQL
			
		-------------------------------------------------------------------------------------------------------------
		
		SELECT * FROM 
		(SELECT DISTINCT PARENT.numFieldID AS [FormFieldID],
						IFFM.intMapColumnNo AS [ImportFieldID],
						IFFM.intImportFileID,
						IFFM.intImportTransactionID,
						CASE WHEN ISNULL(bitCustomField,0) = 1
							 THEN CFW.Fld_label
							 ELSE PARENT.vcDbColumnName
						END AS [dbFieldName],	 
						CASE WHEN ISNULL(bitCustomField,0) = 1
							 THEN CFW.Fld_label
							 ELSE PARENT.vcFieldName 
						END AS [FormFieldName],	 				
						CASE WHEN CFW.fld_Type = 'Drop Down List Box'
							 THEN 'SelectBox'
							 ELSE ISNULL(PARENT.vcAssociatedControlType,'') 
						END	 vcAssociatedControlType,
						ISNULL(PARENT.bitDefault,0) bitDefault,
						ISNULL(PARENT.vcFieldType,'') vcFieldType,
						ISNULL(PARENT.vcListItemType,'') vcListItemType,
						ISNULL(PARENT.vcLookBackTableName,'') vcLookBackTableName,
						CASE WHEN ISNULL(bitCustomField,0) = 1
							 THEN CFW.Fld_label
							 ELSE PARENT.vcOrigDbColumnName
						END AS [vcOrigDbColumnName],
						ISNULL(PARENT.vcPropertyName,'') vcPropertyName,
						ISNULL(bitCustomField,0) AS [bitCustomField],
						PARENT.numDomainID AS [DomainID]
		FROM dbo.View_DynamicDefaultColumns PARENT  
		LEFT JOIN dbo.Import_File_Field_Mapping IFFM ON PARENT.numFieldID = IFFM.numFormFieldID    		
		LEFT JOIN dbo.CFW_Fld_Master CFW ON CFW.numDomainID = PARENT.numDomainID
		WHERE IFFM.intImportFileID = @intImportFileID
		  AND PARENT.numDomainID = @numDomainID
		  AND numFormID = @numFormID
	
		UNION ALL
		
		SELECT DISTINCT IFFM.numFormFieldID AS [FormFieldID],
						IFFM.intMapColumnNo AS [ImportFieldID],
						IFFM.intImportFileID,
						IFFM.intImportTransactionID,
						CFW.Fld_label AS [dbFieldName],	 
						CFW.Fld_label AS [FormFieldName],	 				
						CASE WHEN CFW.fld_Type = 'Drop Down List Box'
							 THEN 'SelectBox'
							 ELSE CFW.fld_Type
						END	 vcAssociatedControlType,
						0 AS bitDefault,
						'R' AS vcFieldType,
						'' AS vcListItemType,
						'' AS  vcLookBackTableName,
						CFW.Fld_label AS [vcOrigDbColumnName],
						'' AS vcPropertyName,
						1 AS [bitCustomField],
						0
						--IFM.numDomainID AS [DomainID]
		FROM dbo.Import_File_Field_Mapping IFFM 
		INNER JOIN dbo.CFW_Fld_Master CFW ON IFFM.numFormFieldID = CFW.Fld_id 
		INNER JOIN dbo.CFW_Loc_Master LOC ON CFW.Grp_id = LOC.Loc_id										 
		INNER JOIN dbo.DycFormSectionDetail DFSD ON DFSD.Loc_id = LOC.Loc_id
		WHERE 1=1
		  --AND DFSD.numDomainID = CFW.numDomainID
		  AND IFFM.intImportFileID = @intImportFileID
		  AND (DFSD.numFormID = @numFormID OR DFSD.numFormID = (CASE WHEN @numFormID = 20 THEN 48 ELSE @numFormID END))
		  AND Grp_id = DFSD.Loc_Id ) TABLE1
		ORDER BY intImportFileID DESC 
		
	/*
	SELECT * FROM Import_File_Field_Mapping		
	SELECT * FROM Import_File_Master
	SELECT * FROM View_DynamicDefaultColumns	
	SELECT * FROM CFW_Fld_Master
	*/
	/*
		SELECT DISTINCT PARENT.numFieldID AS [FormFieldID],
						IFFM.intMapColumnNo AS [ImportFieldID],
						IFFM.intImportFileID,
						IFFM.intImportTransactionID,
						CASE WHEN ISNULL(bitCustomField,0) = 1
							 THEN CFW.Fld_label
							 ELSE PARENT.vcDbColumnName
						END AS [dbFieldName],	 
						CASE WHEN ISNULL(bitCustomField,0) = 1
							 THEN CFW.Fld_label
							 ELSE PARENT.vcFieldName 
						END AS [FormFieldName],	 				
						CASE WHEN CFW.fld_Type = 'Drop Down List Box'
							 THEN 'SelectBox'
							 ELSE ISNULL(PARENT.vcAssociatedControlType,'') 
						END	 vcAssociatedControlType,
						ISNULL(PARENT.bitDefault,0) bitDefault,
						ISNULL(PARENT.vcFieldType,'') vcFieldType,
						ISNULL(PARENT.vcListItemType,'') vcListItemType,
						ISNULL(PARENT.vcLookBackTableName,'') vcLookBackTableName,
						CASE WHEN ISNULL(bitCustomField,0) = 1
							 THEN CFW.Fld_label
							 ELSE PARENT.vcOrigDbColumnName
						END AS [vcOrigDbColumnName],
						ISNULL(PARENT.vcPropertyName,'') vcPropertyName,
						ISNULL(bitCustomField,0) AS [bitCustomField],
						PARENT.numDomainID AS [DomainID] 
		FROM View_DynamicDefaultColumns PARENT
		LEFT JOIN dbo.Import_File_Field_Mapping IFFM ON PARENT.numFieldID = IFFM.numFormFieldID    		
		LEFT JOIN dbo.CFW_Fld_Master CFW ON CFW.Grp_id = PARENT.numFormId AND CFW.numDomainID = PARENT.numDomainID
		WHERE numFormID = 48 AND PARENT.numDomainID = 1 AND IFFM.intImportFileID = 2 */	
		
--		SELECT  CHILD.numFormFieldID AS [FormFieldID],
--				CHILD.intMapColumnNo AS [ImportFieldID],
--				CHILD.intImportFileID,
--				CHILD.intImportTransactionID,
--				CASE WHEN ISNULL(bitCustomField,0) = 1
--					 THEN CFW.Fld_label
--					 ELSE DYNAMIC_FIELD.vcDbColumnName
--				END AS [dbFieldName],	 
--				--DYNAMIC_FIELD.vcDbColumnName AS [dbFieldName],
--				CASE WHEN ISNULL(bitCustomField,0) = 1
--					 THEN CFW.Fld_label
--					 ELSE DYNAMIC_FIELD.vcFormFieldName
--				END AS [FormFieldName],	 
--				--DYNAMIC_FIELD.vcFormFieldName AS [FormFieldName],
--				CASE WHEN CFW.fld_Type = 'Drop Down List Box'
--					 THEN 'SelectBox'
--					 ELSE ISNULL(DYNAMIC_FIELD.vcAssociatedControlType,'') 
--				END	 vcAssociatedControlType,
--				ISNULL(DYNAMIC_FIELD.bitDefault,0) bitDefault,
--				ISNULL(DYNAMIC_FIELD.vcFieldType,'') vcFieldType,
--				ISNULL(DYNAMIC_FIELD.vcListItemType,'') vcListItemType,
--				ISNULL(DYNAMIC_FIELD.vcLookBackTableName,'') vcLookBackTableName,
--				CASE WHEN ISNULL(bitCustomField,0) = 1
--					 THEN CFW.Fld_label
--					 ELSE DYNAMIC_FIELD.vcOrigDbColumnName
--				END AS [vcOrigDbColumnName],
--				ISNULL(DYNAMIC_FIELD.vcPropertyName,'') vcPropertyName,
--				ISNULL(bitCustomField,0) AS [bitCustomField],
--			    PARENT.numDomainID AS [DomainID]
--		FROM Import_File_Field_Mapping CHILD 
--		INNER JOIN dbo.Import_File_Master PARENT ON PARENT.intImportFileID = CHILD.intImportFileID
--		INNER JOIN dbo.Import_Master IMPORT ON IMPORT.intImportMasterID = PARENT.numMasterID
--		LEFT JOIN DynamicFormFieldMaster DYNAMIC_FIELD ON CHILD.numFormFieldID = DYNAMIC_FIELD.numFormFieldId 
--		LEFT JOIN dbo.DycFormField_Mapping DYNAMIC_FIELD_CHILD ON CHILD.numFormFieldID = DYNAMIC_FIELD_CHILD.numFieldId 
--		INNER JOIN dbo.DycFieldMaster DYNAMIC_FIELD ON DYNAMIC_FIELD_CHILD.= DYNAMIC_FIELD
--		LEFT JOIN dbo.CFW_Fld_Master CFW ON CHILD.numFormFieldID = CFW.Fld_id
--		WHERE CHILD.intImportFileID = @intImportFileID

END		
/*
-- Added Support for Bills in AP Aging summary,by chintan
*/

-- [dbo].[USP_GetAccountPayableAging] 1,null 6719
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getaccountpayableaging')
DROP PROCEDURE usp_getaccountpayableaging
GO
set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

CREATE PROCEDURE [dbo].[USP_GetAccountPayableAging]
(
    @numDomainId AS NUMERIC(9) = 0,
    @numDivisionId AS NUMERIC(9) = NULL,
    @numAccountClass AS NUMERIC(9)=0,
	@dtFromDate AS DATETIME,
	@dtToDate AS DATETIME,
	@ClientTimeZoneOffset INT = 0
)
AS 
BEGIN 
	DECLARE @numTempDomainId NUMERIC(18,0)
	DECLARE @numTempDivisionId NUMERIC(18,0)
	DECLARE @numTempAccountClass NUMERIC(18,0)
	DECLARE @dtTempFromDate AS DATETIME
	DECLARE @dtTempToDate AS DATETIME


	SET @numTempDomainId = @numDomainId
	SET @numTempDivisionId = @numDivisionId
	SET @numTempAccountClass = @numAccountClass
	SET @dtTempFromDate = @dtFromDate
	SET @dtTempToDate = @dtToDate

		IF @dtTempFromDate IS NULL
			SET @dtTempFromDate = '1990-01-01 00:00:00.000'
		IF @dtTempToDate IS NULL
			SET @dtTempToDate = GETUTCDATE()

        CREATE TABLE #TempAPRecord
            (
              [ID] [int] IDENTITY(1, 1)
                         NOT NULL,
              [numUserCntID] [numeric](18, 0) NOT NULL,
              [numOppId] [numeric](18, 0) NULL,
              [numDivisionId] [numeric](18, 0) NULL,
              [numOppBizDocsId] [numeric](18, 0) NULL,
              [DealAmount] [float] NULL,
              [numBizDocId] [numeric](18, 0) NULL,
              dtDueDate DATETIME NULL,
              [numCurrencyID] [numeric](18, 0) NULL,AmountPaid FLOAT NULL,monUnAppliedAmount DECIMAL(20,5) NULL 
            ) ;
  
  
        CREATE TABLE #TempAPRecord1
            (
              [numID] [numeric](18, 0) IDENTITY(1, 1)
                                       NOT NULL,
              [numUserCntID] [numeric](18, 0) NOT NULL,
              [numDivisionId] [numeric](18, 0) NULL,
              [tintCRMType] [tinyint] NULL,
              [vcCompanyName] [varchar](200) NULL,
              [vcCustPhone] [varchar](50) NULL,
              [numContactId] [numeric](18, 0) NULL,
              [vcEmail] [varchar](100) NULL,
              [numPhone] [varchar](50) NULL,
              [vcContactName] [varchar](100) NULL,
			  [numCurrentDays] [numeric](18, 2) NULL,
              [numThirtyDays] [numeric](18, 2) NULL,
              [numSixtyDays] [numeric](18, 2) NULL,
              [numNinetyDays] [numeric](18, 2) NULL,
              [numOverNinetyDays] [numeric](18, 2) NULL,
              [numThirtyDaysOverDue] [numeric](18, 2) NULL,
              [numSixtyDaysOverDue] [numeric](18, 2) NULL,
              [numNinetyDaysOverDue] [numeric](18, 2) NULL,
              [numOverNinetyDaysOverDue] [numeric](18, 2) NULL,
              [numCompanyID] [numeric](18, 2) NULL,
              [numDomainID] [numeric](18, 2) NULL,
			  [intCurrentDaysCount] [int] NULL,
              [intThirtyDaysCount] [int] NULL,
              [intThirtyDaysOverDueCount] [int] NULL,
              [intSixtyDaysCount] [int] NULL,
              [intSixtyDaysOverDueCount] [int] NULL,
              [intNinetyDaysCount] [int] NULL,
              [intOverNinetyDaysCount] [int] NULL,
              [intNinetyDaysOverDueCount] [int] NULL,
              [intOverNinetyDaysOverDueCount] [int] NULL,
			  [numCurrentDaysPaid] [numeric](18, 2) NULL,
              [numThirtyDaysPaid] [numeric](18, 2) NULL,
              [numSixtyDaysPaid] [numeric](18, 2) NULL,
              [numNinetyDaysPaid] [numeric](18, 2) NULL,
              [numOverNinetyDaysPaid] [numeric](18, 2) NULL,
              [numThirtyDaysOverDuePaid] [numeric](18, 2) NULL,
              [numSixtyDaysOverDuePaid] [numeric](18, 2) NULL,
              [numNinetyDaysOverDuePaid] [numeric](18, 2) NULL,
              [numOverNinetyDaysOverDuePaid] [numeric](18, 2) NULL
              ,monUnAppliedAmount DECIMAL(20,5) NULL,numTotal DECIMAL(20,5) NULL 
            ) ;
  
  
  
        DECLARE @AuthoritativePurchaseBizDocId AS INTEGER
        SELECT  @AuthoritativePurchaseBizDocId = isnull(numAuthoritativePurchase,
                                                        0)
        FROM    AuthoritativeBizDocs
        WHERE   numDomainId = @numTempDomainId ;
        
     
        INSERT  INTO [#TempAPRecord]
                (
                  numUserCntID,
                  [numOppId],
                  [numDivisionId],
                  [numOppBizDocsId],
                  [DealAmount],
                  [numBizDocId],
                  dtDueDate,
                  [numCurrencyID],AmountPaid
                )
                SELECT  @numTempDomainId,
                        OB.numOppId,
                        Opp.numDivisionId,
                        OB.numOppBizDocsId,
                        ISNULL(OB.monDealAmount --dbo.GetDealAmount(OB.numOppID,getutcdate(),OB.numOppBizDocsId)
                               * ISNULL(Opp.fltExchangeRate, 1), 0)
                        - ISNULL(ISNULL(TablePayment.monPaidAmount,0)
                                 * ISNULL(Opp.fltExchangeRate, 1), 0) DealAmount,
                        OB.numBizDocId,
                        DATEADD(DAY,
                                CASE WHEN Opp.bitBillingTerms = 1
                                     --THEN convert(int, ISNULL(dbo.fn_GetListItemName(isnull(Opp.intBillingDays, 0)), 0))
                                     THEN convert(int, ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(Opp.intBillingDays,0)), 0))
                                     ELSE 0
                                END, dtFromDate) AS dtDueDate,
                        ISNULL(Opp.numCurrencyID, 0) numCurrencyID,
                        ISNULL(ISNULL(TablePayment.monPaidAmount,0) * ISNULL(Opp.fltExchangeRate, 1), 0) AmountPaid
                FROM    OpportunityMaster Opp
                        JOIN OpportunityBizDocs OB ON OB.numOppId = Opp.numOppID
                        LEFT OUTER JOIN [Currency] C ON Opp.[numCurrencyID] = C.[numCurrencyID]
						OUTER APPLY
						(
							SELECT
								SUm(BPD.monAmount) monPaidAmount
							FROM	
								BillPaymentDetails BPD
							INNER JOIN
								BillPaymentHeader BPH
							ON
								BPD.numBillPaymentID=BPH.numBillPaymentID
							WHERE
								BPD.numOppBizDocsID = OB.numOppBizDocsId
								AND CAST(BPH.dtPaymentDate AS DATE) <= CAST(@dtTempToDate AS DATE)
						) TablePayment
                WHERE   tintOpptype = 2
                        AND tintoppStatus = 1
                        AND opp.numDomainID = @numTempDomainId
                        AND OB.bitAuthoritativeBizDocs = 1
                        AND ISNULL(OB.tintDeferred, 0) <> 1
                        AND numBizDocId = @AuthoritativePurchaseBizDocId
                        AND ( Opp.numDivisionId = @numTempDivisionId
                              OR @numTempDivisionId IS NULL
                            )
                        AND (Opp.numAccountClass=@numTempAccountClass OR @numTempAccountClass=0)
						AND OB.[dtCreatedDate] <= @dtTempToDate
                GROUP BY OB.numOppId,
                        Opp.numDivisionId,
                        OB.numOppBizDocsId,
                        Opp.fltExchangeRate,
                        OB.numBizDocId,
                        OB.dtCreatedDate,
                        Opp.bitBillingTerms,
                        Opp.intBillingDays,
                        OB.monDealAmount,
                        Opp.numCurrencyID,
                        OB.[dtFromDate],TablePayment.monPaidAmount
                HAVING  ( ISNULL(OB.monDealAmount * ISNULL(Opp.fltExchangeRate,
                                                           1), 0)
                          - ISNULL(ISNULL(TablePayment.monPaidAmount,0)
                                   * ISNULL(Opp.fltExchangeRate, 1), 0) ) > 0
                UNION ALL 
		--Add Bill Amount
                SELECT
					@numTempDomainId,
					0 numOppId,
					numDivisionId,
					0 numBizDocsId,
					ISNULL(SUM(monAmountDue), 0) - ISNULL(SUM(monAmountPaid),0) DealAmount,
					0 numBizDocId,
					dtDueDate AS dtDueDate,
					0 numCurrencyID
					,ISNULL(SUM(monAmountPaid),0) AS AmountPaid
				FROM
				(
					SELECT 
						BH.numBillID
						,BH.numDivisionId
						,BH.dtDueDate
						,BH.monAmountDue
						,ISNULL(SUM(BPD.monAmount),0) monAmountPaid
					FROM
						BillHeader BH
					LEFT JOIN
						BillPaymentDetails BPD
					ON
						BH.numBillID = BPD.numBillID
					LEFT JOIN
						BillPaymentHeader BPH
					ON
						BPD.numBillPaymentID=BPH.numBillPaymentID
						AND CAST(BPH.dtPaymentDate AS DATE) <= @dtTempToDate
					WHERE
						BH.numDomainID = @numTempDomainId
						AND (BH.numDivisionId = @numTempDivisionId OR @numTempDivisionId IS NULL)
						AND (BH.numAccountClass=@numTempAccountClass OR @numTempAccountClass=0)
						AND BH.[dtBillDate] <= @dtTempToDate
					GROUP BY
						BH.numBillID
						,BH.numDivisionId
						,BH.dtDueDate
						,BH.monAmountDue
					HAVING
						 ISNULL(BH.monAmountDue, 0) - ISNULL(SUM(BPD.monAmount),0) > 0  
				) TEMP
				GROUP BY 
					numDivisionId,
					dtDueDate
				HAVING
					ISNULL(SUM(monAmountDue), 0) - ISNULL(SUM(monAmountPaid),0) > 0 
                UNION ALL
		--Add Write Check entry (As Amount Paid)
                SELECT  @numTempDomainId,
                        0 numOppId,
                        CD.numCustomerId as numDivisionId,
                        0 numBizDocsId,
                        0 DealAmount,
                        0 numBizDocId,
                        CH.dtCheckDate AS dtDueDate,
                        0 numCurrencyID,ISNULL(SUM(CD.monAmount), 0) AS AmountPaid
                FROM    CheckHeader CH
						JOIN  dbo.CheckDetails CD ON CH.numCheckHeaderID=CD.numCheckHeaderID
                        INNER JOIN dbo.Chart_Of_Accounts COA ON COA.numAccountId = CD.numChartAcntId
                WHERE   CH.numDomainID = @numTempDomainId AND CH.tintReferenceType=1
						AND COA.vcAccountCode LIKE '01020102%'
                        AND ( CD.numCustomerId = @numTempDivisionId
                              OR @numTempDivisionId IS NULL
                            )
                        AND (CH.numAccountClass=@numTempAccountClass OR @numTempAccountClass=0)
						AND CH.[dtCheckDate] <= @dtTempToDate
                GROUP BY CD.numCustomerId,
                        CH.dtCheckDate
		--Show Impact of AP journal entries in report as well 
                UNION ALL
                SELECT  @numTempDomainId,
                        0,
                        GJD.numCustomerId,
                        0,
                        CASE WHEN GJD.numDebitAmt > 0 THEN -1 * GJD.numDebitAmt
                             ELSE GJD.numCreditAmt
                        END,
                        0,
                        GJH.datEntry_Date,
                        GJD.[numCurrencyID],0 AS AmountPaid
                FROM    dbo.General_Journal_Header GJH
                        INNER JOIN dbo.General_Journal_Details GJD ON GJH.numJournal_Id = GJD.numJournalId
                                                                      AND GJH.numDomainId = GJD.numDomainId
                        INNER JOIN dbo.Chart_Of_Accounts COA ON COA.numAccountId = GJD.numChartAcntId
                WHERE   GJH.numDomainId = @numTempDomainId 
                        AND COA.vcAccountCode LIKE '01020102%'
                        AND ISNULL(numOppId, 0) = 0
                        AND ISNULL(numOppBizDocsId, 0) = 0
                        AND ISNULL(GJH.numBillID,0)=0 AND ISNULL(GJH.numBillPaymentID,0)=0 AND ISNULL(GJH.numCheckHeaderID,0)=0 AND ISNULL(GJH.numReturnID,0)=0
                        AND ISNULL(GJH.numPayrollDetailID,0)=0
                         AND ( GJD.numCustomerId = @numTempDivisionId
                              OR @numTempDivisionId IS NULL
                            )
                            AND (GJH.numAccountClass=@numTempAccountClass OR @numTempAccountClass=0)
						AND [GJH].[datEntry_Date] <= @dtTempToDate
                UNION ALL
				--Add Commission Amount
                SELECT  @numTempDomainId,
                        OBD.numOppId numOppId,
                        D.numDivisionId,
                        BDC.numOppBizDocId numBizDocsId,
                       isnull(sum(BDC.numComissionAmount - ISNULL(PT.monCommissionAmt,0)),0)  DealAmount,
                        0 numBizDocId,
                        OBD.dtCreatedDate AS dtDueDate,
                        ISNULL(OPP.numCurrencyID, 0) numCurrencyID,0 AS AmountPaid
                FROM    BizDocComission BDC
                        JOIN OpportunityBizDocs OBD ON BDC.numOppBizDocId = OBD.numOppBizDocsId
                        join OpportunityMaster Opp on OPP.numOppId = OBD.numOppId
                        JOIN Domain D on D.numDomainID = BDC.numDomainID
                        LEFT JOIN dbo.PayrollTracking PT ON BDC.numComissionID=PT.numComissionID
                WHERE   OPP.numDomainID = @numTempDomainId
                        and BDC.numDomainID = @numTempDomainId
                        AND (D.numDivisionId = @numTempDivisionId OR @numTempDivisionId IS NULL)
                        AND (Opp.numAccountClass=@numTempAccountClass OR @numTempAccountClass=0)
						AND OBD.[dtCreatedDate] <= @dtTempToDate
						AND ISNULL(BDC.bitCommisionPaid,0)=0
                        AND OBD.bitAuthoritativeBizDocs = 1
                GROUP BY D.numDivisionId,
                        OBD.numOppId,
                        BDC.numOppBizDocId,
                        OPP.numCurrencyID,
                        BDC.numComissionAmount,
                        OBD.dtCreatedDate
                HAVING  ISNULL(BDC.numComissionAmount, 0) > 0
			UNION ALL --Standalone Refund against Account Receivable
			 SELECT @numTempDomainId,
					0 AS numOppID,
					RH.numDivisionId,
					0,
					CASE WHEN GJD.numDebitAmt > 0 THEN GJD.numDebitAmt * -1 ELSE GJD.numCreditAmt END DealAmount,
					0 numBizDocId,
					GJH.datEntry_Date,GJD.[numCurrencyID],0 AS AmountPaid
			 FROM   dbo.ReturnHeader RH JOIN dbo.General_Journal_Header GJH ON RH.numReturnHeaderID = GJH.numReturnID
			JOIN General_Journal_Details GJD ON GJH.numJournal_Id = GJD.numJournalId 
			INNER JOIN dbo.Chart_Of_Accounts COA ON COA.numAccountId = GJD.numChartAcntId
			WHERE RH.tintReturnType=4 AND RH.numDomainId=@numTempDomainId  AND COA.vcAccountCode LIKE '01020102%'
					AND (RH.numDivisionId = @numTempDivisionId OR @numTempDivisionId IS NULL)
					AND (monBizDocAmount > 0) AND ISNULL(RH.numBillPaymentIDRef,0)>0
					AND (RH.numAccountClass=@numTempAccountClass OR @numTempAccountClass=0)
					AND GJH.datEntry_Date <= @dtTempToDate
					--GROUP BY DM.numDivisionId,DM.dtDepositDate
		
		
---SELECT  SUM(monUnAppliedAmount) FROM    [#TempAPRecord]

   INSERT  INTO [#TempAPRecord]
        (
          numUserCntID,
          [numOppId],
          [numDivisionId],
          [numOppBizDocsId],
          [DealAmount],
          [numBizDocId],
          [numCurrencyID],
          [dtDueDate],AmountPaid,monUnAppliedAmount
        )
SELECT @numTempDomainId,0, BPH.numDivisionId,0,0,0,0,NULL,0,sum(ISNULL(monPaymentAmount,0) - ISNULL(monAppliedAmount,0))       
 FROM BillPaymentHeader BPH
		WHERE BPH.numDomainId=@numTempDomainId   
		AND (BPH.numDivisionId = @numTempDivisionId
                            OR @numTempDivisionId IS NULL)
		AND (ISNULL(monPaymentAmount,0) - ISNULL(monAppliedAmount,0)) > 0
		AND (BPH.numAccountClass=@numTempAccountClass OR @numTempAccountClass=0)
		AND [BPH].[dtPaymentDate] <= @dtTempToDate
		GROUP BY BPH.numDivisionId
		

--SELECT  GETDATE()
        INSERT  INTO [#TempAPRecord1]
                (
                  [numUserCntID],
                  [numDivisionId],
                  [tintCRMType],
                  [vcCompanyName],
                  [vcCustPhone],
                  [numContactId],
                  [vcEmail],
                  [numPhone],
                  [vcContactName],
				  [numCurrentDays],
                  [numThirtyDays],
                  [numSixtyDays],
                  [numNinetyDays],
                  [numOverNinetyDays],
                  [numThirtyDaysOverDue],
                  [numSixtyDaysOverDue],
                  [numNinetyDaysOverDue],
                  [numOverNinetyDaysOverDue],
                  numCompanyID,
                  numDomainID,
				  [intCurrentDaysCount],
                  [intThirtyDaysCount],
                  [intThirtyDaysOverDueCount],
                  [intSixtyDaysCount],
                  [intSixtyDaysOverDueCount],
                  [intNinetyDaysCount],
                  [intOverNinetyDaysCount],
                  [intNinetyDaysOverDueCount],
                  [intOverNinetyDaysOverDueCount],
				  [numCurrentDaysPaid],
                  [numThirtyDaysPaid],
                  [numSixtyDaysPaid],
                  [numNinetyDaysPaid],
                  [numOverNinetyDaysPaid],
                  [numThirtyDaysOverDuePaid],
                  [numSixtyDaysOverDuePaid],
                  [numNinetyDaysOverDuePaid],
                  [numOverNinetyDaysOverDuePaid],monUnAppliedAmount
                )
                SELECT DISTINCT
                        @numTempDomainId,
                        Div.numDivisionID,
                        tintCRMType AS tintCRMType,
                        vcCompanyName AS vcCompanyName,
                        '',
                        NULL,
                        '',
                        '',
                        '',
						0 numCurrentDays,
                        0 numThirtyDays,
                        0 numSixtyDays,
                        0 numNinetyDays,
                        0 numOverNinetyDays,
                        0 numThirtyDaysOverDue,
                        0 numSixtyDaysOverDue,
                        0 numNinetyDaysOverDue,
                        0 numOverNinetyDaysOverDue,
                        C.numCompanyID,
                        Div.numDomainID,
						0 [intCurrentDaysCount],
                        0 [intThirtyDaysCount],
                        0 [intThirtyDaysOverDueCount],
                        0 [intSixtyDaysCount],
                        0 [intSixtyDaysOverDueCount],
                        0 [intNinetyDaysCount],
                        0 [intOverNinetyDaysCount],
                        0 [intNinetyDaysOverDueCount],
                        0 [intOverNinetyDaysOverDueCount],
						0 numCurrentDaysPaid,
                         0 numThirtyDaysPaid,
                        0 numSixtyDaysPaid,
                        0 numNinetyDaysPaid,
                        0 numOverNinetyDaysPaid,
                        0 numThirtyDaysOverDuePaid,
                        0 numSixtyDaysOverDuePaid,
                        0 numNinetyDaysOverDuePaid,
                        0 numOverNinetyDaysOverDuePaid,0 monUnAppliedAmount
                FROM    Divisionmaster Div
                        INNER JOIN CompanyInfo C ON C.numCompanyId = Div.numCompanyID
                    --JOIN [#TempAPRecord] t ON Div.[numDivisionID] = t.[numDivisionId]
                WHERE   Div.[numDomainID] = @numTempDomainId
                        AND Div.[numDivisionID] IN ( SELECT DISTINCT
                                                            [numDivisionID]
                                                     FROM   [#TempAPRecord] )


--Update Custome Phone 
        UPDATE  #TempAPRecord1
        SET     [vcCustPhone] = X.[vcCustPhone]
        FROM    ( SELECT    CASE WHEN numPhone IS NULL THEN ''
                                 WHEN numPhone = '' THEN ''
                                 ELSE ', ' + numPhone
                            END AS vcCustPhone,
                            numDivisionId
                  FROM      AdditionalContactsInformation
                  WHERE     bitPrimaryContact = 1
                            AND numDivisionId IN (
                            SELECT DISTINCT
                                    numDivisionID
                            FROM    [#TempAPRecord1]
                            WHERE   numUserCntID = @numTempDomainId )
                ) X
        WHERE   #TempAPRecord1.numDivisionID = X.numDivisionID

--Update Customer info 
        UPDATE  #TempAPRecord1
        SET     [numContactId] = X.[numContactID],
                [vcEmail] = X.[vcEmail],
                [vcContactName] = X.[vcContactName],
                numPhone = X.numPhone
        FROM    ( SELECT  numContactId AS numContactID,
                            ISNULL(vcEmail, '') AS vcEmail,
                            ISNULL(vcFirstname, '') + ' ' + ISNULL(vcLastName, '') AS vcContactName,
                            CAST(ISNULL(CASE WHEN numPhone IS NULL THEN ''
                                             WHEN numPhone = '' THEN ''
                                             ELSE ', ' + numPhone
                                        END, '') AS VARCHAR) AS numPhone,
                            [numDivisionId]
                  FROM      AdditionalContactsInformation
                  WHERE     (vcPosition = 843 or isnull(bitPrimaryContact,0)=1)
                            AND numDivisionId IN (
                            SELECT DISTINCT
                                    numDivisionID
                            FROM    [#TempAPRecord1]
                            WHERE   numUserCntID = @numTempDomainId )
                ) X
        WHERE   #TempAPRecord1.numDivisionID = X.numDivisionID

                    
--SELECT  GETDATE()
------------------------------------------      
        DECLARE @baseCurrency AS NUMERIC
        SELECT  @baseCurrency = numCurrencyID
        FROM    dbo.Domain
        WHERE   numDomainId = @numTempDomainId
/*when any Order is placed in Foreign currency (other than base currency)then show it in purple color */

------------------------------------------

-----------------------------------
        UPDATE  #TempAPRecord1
        SET     numCurrentDays = X.numCurrentDays
        FROM    ( SELECT    SUM(DealAmount) AS numCurrentDays,
                            [numDivisionId]
                  FROM      [#TempAPRecord]
                  WHERE     numUserCntID = @numTempDomainId AND
                            DATEDIFF(DAY,dbo.GetUTCDateWithoutTime(),[dtDueDate]) >= 0

                  GROUP BY  numDivisionId
                ) X
        WHERE   #TempAPRecord1.numDivisionID = X.numDivisionID
        
         UPDATE  #TempAPRecord1
        SET     numCurrentDaysPaid = X.numCurrentDaysPaid
        FROM    ( SELECT    SUM(AmountPaid) AS numCurrentDaysPaid,
                            [numDivisionId]
                  FROM      [#TempAPRecord]
                  WHERE     numUserCntID = @numTempDomainId
                            AND DATEDIFF(DAY,dbo.GetUTCDateWithoutTime(),[dtDueDate]) >= 0
                  GROUP BY  numDivisionId
                ) X
        WHERE   #TempAPRecord1.numDivisionID = X.numDivisionID
        
        UPDATE  #TempAPRecord1
        SET     intCurrentDaysCount = 2
        WHERE   numDivisionId IN (
                SELECT DISTINCT
                        [numDivisionId]
                FROM    [#TempAPRecord]
                WHERE   numUserCntID = @numTempDomainId
                        AND DATEDIFF(DAY,dbo.GetUTCDateWithoutTime(),[dtDueDate]) >= 0
                        AND numCurrencyID <> @baseCurrency )
------------------------------------------

        UPDATE  #TempAPRecord1
        SET     numThirtyDays = X.numThirtyDays
        FROM    ( SELECT    SUM(DealAmount) AS numThirtyDays,
                            [numDivisionId]
                  FROM      [#TempAPRecord]
                  WHERE     numUserCntID = @numTempDomainId
                            AND DATEDIFF(DAY, dbo.GetUTCDateWithoutTime(),
                                         [dtDueDate]) BETWEEN 1
                                                      AND     30
                  GROUP BY  numDivisionId
                ) X
        WHERE   #TempAPRecord1.numDivisionID = X.numDivisionID
        
         UPDATE  #TempAPRecord1
        SET     numThirtyDaysPaid = X.numThirtyDaysPaid
        FROM    ( SELECT    SUM(AmountPaid) AS numThirtyDaysPaid,
                            [numDivisionId]
                  FROM      [#TempAPRecord]
                  WHERE     numUserCntID = @numTempDomainId
                            AND DATEDIFF(DAY, dbo.GetUTCDateWithoutTime(),
                                         [dtDueDate]) BETWEEN 1
                                                      AND     30
                  GROUP BY  numDivisionId
                ) X
        WHERE   #TempAPRecord1.numDivisionID = X.numDivisionID
        
        UPDATE  #TempAPRecord1
        SET     intThirtyDaysCount = 2
        WHERE   numDivisionId IN (
                SELECT DISTINCT
                        [numDivisionId]
                FROM    [#TempAPRecord]
                WHERE   numUserCntID = @numTempDomainId
                        AND DATEDIFF(DAY, dbo.GetUTCDateWithoutTime(),
                                     [dtDueDate]) BETWEEN 1
                                                  AND     30
                        AND numCurrencyID <> @baseCurrency )
------------------------------------------
        UPDATE  #TempAPRecord1
        SET     numSixtyDays = X.numSixtyDays
        FROM    ( SELECT    SUM(DealAmount) AS numSixtyDays,
                            numDivisionId
                  FROM      [#TempAPRecord]
                  WHERE     numUserCntID = @numTempDomainId
                            AND DATEDIFF(DAY, dbo.GetUTCDateWithoutTime(),
                                         [dtDueDate]) BETWEEN 31
                                                      AND     60
                            AND dbo.GetUTCDateWithoutTime() <= [dtDueDate]
                  GROUP BY  numDivisionId
                ) X
        WHERE   #TempAPRecord1.numDivisionID = X.numDivisionID
        
        UPDATE  #TempAPRecord1
        SET     numSixtyDaysPaid = X.numSixtyDaysPaid
        FROM    ( SELECT    SUM(AmountPaid) AS numSixtyDaysPaid,
                            numDivisionId
                  FROM      [#TempAPRecord]
                  WHERE     numUserCntID = @numTempDomainId
                            AND DATEDIFF(DAY, dbo.GetUTCDateWithoutTime(),
                                         [dtDueDate]) BETWEEN 31
                                                      AND     60
                            AND dbo.GetUTCDateWithoutTime() <= [dtDueDate]
                  GROUP BY  numDivisionId
                ) X
        WHERE   #TempAPRecord1.numDivisionID = X.numDivisionID
        
        UPDATE  #TempAPRecord1
        SET     intSixtyDaysCount = 2
        WHERE   numDivisionId IN (
                SELECT DISTINCT
                        [numDivisionId]
                FROM    [#TempAPRecord]
                WHERE   numUserCntID = @numTempDomainId
                        AND DATEDIFF(DAY, dbo.GetUTCDateWithoutTime(),
                                     [dtDueDate]) BETWEEN 31
                                                  AND     60
                        AND dbo.GetUTCDateWithoutTime() <= [dtDueDate]
                        AND numCurrencyID <> @baseCurrency )

------------------------------------------
        UPDATE  #TempAPRecord1
        SET     numNinetyDays = X.numNinetyDays
        FROM    ( SELECT    SUM(DealAmount) AS numNinetyDays,
                            numDivisionId
                  FROM      #TempAPRecord
                  WHERE     numUserCntID = @numTempDomainId
                            AND DATEDIFF(DAY, dbo.GetUTCDateWithoutTime(),
                                         [dtDueDate]) BETWEEN 61
                                                      AND     90
                            AND dbo.GetUTCDateWithoutTime() <= [dtDueDate]
                  GROUP BY  numDivisionId
                ) X
        WHERE   #TempAPRecord1.numDivisionID = X.numDivisionID
        
         UPDATE  #TempAPRecord1
        SET     numNinetyDaysPaid = X.numNinetyDaysPaid
        FROM    ( SELECT    SUM(AmountPaid) AS numNinetyDaysPaid,
                            numDivisionId
                  FROM      #TempAPRecord
                  WHERE     numUserCntID = @numTempDomainId
                            AND DATEDIFF(DAY, dbo.GetUTCDateWithoutTime(),
                                         [dtDueDate]) BETWEEN 61
                                                      AND     90
                            AND dbo.GetUTCDateWithoutTime() <= [dtDueDate]
                  GROUP BY  numDivisionId
                ) X
        WHERE   #TempAPRecord1.numDivisionID = X.numDivisionID
        
        UPDATE  #TempAPRecord1
        SET     intNinetyDaysCount = 2
        WHERE   numDivisionId IN (
                SELECT DISTINCT
                        [numDivisionId]
                FROM    [#TempAPRecord]
                WHERE   numUserCntID = @numTempDomainId
                        AND DATEDIFF(DAY, dbo.GetUTCDateWithoutTime(),
                                     [dtDueDate]) BETWEEN 61
                                                  AND     90
                        AND dbo.GetUTCDateWithoutTime() <= [dtDueDate]
                        AND numCurrencyID <> @baseCurrency )

------------------------------------------
        UPDATE  #TempAPRecord1
        SET     numOverNinetyDays = X.numOverNinetyDays
        FROM    ( SELECT    SUM(DealAmount) AS numOverNinetyDays,
                            numDivisionId
                  FROM      #TempAPRecord
                  WHERE     numUserCntID = @numTempDomainId
                            AND [dtDueDate] >= DATEADD(DAY, 91,
                                                       dbo.GetUTCDateWithoutTime())
                            AND dbo.GetUTCDateWithoutTime() <= [dtDueDate]
                  GROUP BY  numDivisionId
                ) X
        WHERE   #TempAPRecord1.numDivisionID = X.numDivisionID
        
        UPDATE  #TempAPRecord1
        SET     numOverNinetyDaysPaid = X.numOverNinetyDaysPaid
        FROM    ( SELECT    SUM(AmountPaid) AS numOverNinetyDaysPaid,
                            numDivisionId
                  FROM      #TempAPRecord
                  WHERE     numUserCntID = @numTempDomainId
                            AND [dtDueDate] >= DATEADD(DAY, 91,
                                                       dbo.GetUTCDateWithoutTime())
                            AND dbo.GetUTCDateWithoutTime() <= [dtDueDate]
                  GROUP BY  numDivisionId
                ) X
        WHERE   #TempAPRecord1.numDivisionID = X.numDivisionID
        
        UPDATE  #TempAPRecord1
        SET     intOverNinetyDaysCount = 2
        WHERE   numDivisionId IN (
                SELECT DISTINCT
                        [numDivisionId]
                FROM    [#TempAPRecord]
                WHERE   numUserCntID = @numTempDomainId
                        AND [dtDueDate] >= DATEADD(DAY, 91,
                                                   dbo.GetUTCDateWithoutTime())
                        AND dbo.GetUTCDateWithoutTime() <= [dtDueDate]
                        AND numCurrencyID <> @baseCurrency )

------------------------------------------
        UPDATE  #TempAPRecord1
        SET     numThirtyDaysOverDue = X.numThirtyDaysOverDue
        FROM    ( SELECT    SUM(DealAmount) AS numThirtyDaysOverDue,
                            numDivisionId
                  FROM      #TempAPRecord
                  WHERE     numUserCntID = @numTempDomainId
                            AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
                            AND DATEDIFF(DAY, [dtDueDate],
                                         dbo.GetUTCDateWithoutTime()) BETWEEN 1
                                                                      AND     30
                  GROUP BY  numDivisionId
                ) X
        WHERE   #TempAPRecord1.numDivisionID = X.numDivisionID
        
         UPDATE  #TempAPRecord1
        SET     numThirtyDaysOverDuePaid = X.numThirtyDaysOverDuePaid
        FROM    ( SELECT    SUM(AmountPaid) AS numThirtyDaysOverDuePaid,
                            numDivisionId
                  FROM      #TempAPRecord
                  WHERE     numUserCntID = @numTempDomainId
                            AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
                            AND DATEDIFF(DAY, [dtDueDate],
                                         dbo.GetUTCDateWithoutTime()) BETWEEN 1
                                                                      AND     30
                  GROUP BY  numDivisionId
                ) X
        WHERE   #TempAPRecord1.numDivisionID = X.numDivisionID
        
        UPDATE  #TempAPRecord1
        SET     intThirtyDaysOverDueCount = 2
        WHERE   numDivisionId IN (
                SELECT DISTINCT
                        [numDivisionId]
                FROM    [#TempAPRecord]
                WHERE   numUserCntID = @numTempDomainId
                        AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
                        AND DATEDIFF(DAY, [dtDueDate],
                                     dbo.GetUTCDateWithoutTime()) BETWEEN 1
                                                                  AND     30
                        AND numCurrencyID <> @baseCurrency )

----------------------------------
        UPDATE  #TempAPRecord1
        SET     numSixtyDaysOverDue = X.numSixtyDaysOverDue
        FROM    ( SELECT    SUM(DealAmount) AS numSixtyDaysOverDue,
                            numDivisionId
                  FROM      #TempAPRecord
                  WHERE     numUserCntID = @numTempDomainId
                            AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
                            AND DATEDIFF(DAY, [dtDueDate],
                                         dbo.GetUTCDateWithoutTime()) BETWEEN 31
                                                                      AND     60
                  GROUP BY  numDivisionId
                ) X
        WHERE   #TempAPRecord1.numDivisionID = X.numDivisionID
        
          UPDATE  #TempAPRecord1
        SET     numSixtyDaysOverDuePaid = X.numSixtyDaysOverDuePaid
        FROM    ( SELECT    SUM(AmountPaid) AS numSixtyDaysOverDuePaid,
                            numDivisionId
                  FROM      #TempAPRecord
                  WHERE     numUserCntID = @numTempDomainId
                            AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
                            AND DATEDIFF(DAY, [dtDueDate],
                                         dbo.GetUTCDateWithoutTime()) BETWEEN 31
                                                                      AND     60
                  GROUP BY  numDivisionId
                ) X
        WHERE   #TempAPRecord1.numDivisionID = X.numDivisionID
        
        UPDATE  #TempAPRecord1
        SET     intSixtyDaysOverDueCount = 2
        WHERE   numDivisionId IN (
                SELECT DISTINCT
                        [numDivisionId]
                FROM    [#TempAPRecord]
                WHERE   numUserCntID = @numTempDomainId
                        AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
                        AND DATEDIFF(DAY, [dtDueDate],
                                     dbo.GetUTCDateWithoutTime()) BETWEEN 31
                                                                  AND     60
                        AND numCurrencyID <> @baseCurrency )
------------------------------------------
        UPDATE  #TempAPRecord1
        SET     numNinetyDaysOverDue = X.numNinetyDaysOverDue
        FROM    ( SELECT    SUM(DealAmount) AS numNinetyDaysOverDue,
                            numDivisionId
                  FROM      #TempAPRecord
                  WHERE     numUserCntID = @numTempDomainId
                            AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
                            AND DATEDIFF(DAY, [dtDueDate],
                                         dbo.GetUTCDateWithoutTime()) BETWEEN 61
                                                                      AND     90
                  GROUP BY  numDivisionId
                ) X
        WHERE   #TempAPRecord1.numDivisionID = X.numDivisionID
        
         UPDATE  #TempAPRecord1
        SET     numNinetyDaysOverDuePaid = X.numNinetyDaysOverDuePaid
        FROM    ( SELECT    SUM(AmountPaid) AS numNinetyDaysOverDuePaid,
                            numDivisionId
                  FROM      #TempAPRecord
                  WHERE     numUserCntID = @numTempDomainId
                            AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
                            AND DATEDIFF(DAY, [dtDueDate],
                                         dbo.GetUTCDateWithoutTime()) BETWEEN 61
                                                                      AND     90
                  GROUP BY  numDivisionId
                ) X
        WHERE   #TempAPRecord1.numDivisionID = X.numDivisionID
        
        UPDATE  #TempAPRecord1
        SET     intNinetyDaysOverDueCount = 2
        WHERE   numDivisionId IN (
                SELECT DISTINCT
                        [numDivisionId]
                FROM    [#TempAPRecord]
                WHERE   numUserCntID = @numTempDomainId
                        AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
                        AND DATEDIFF(DAY, [dtDueDate],
                                     dbo.GetUTCDateWithoutTime()) BETWEEN 61
                                                                  AND     90
                        AND numCurrencyID <> @baseCurrency )
------------------------------------------
        UPDATE  #TempAPRecord1
        SET     numOverNinetyDaysOverDue = X.numOverNinetyDaysOverDue
        FROM    ( SELECT    SUM(DealAmount) AS numOverNinetyDaysOverDue,
                            numDivisionId
                  FROM      #TempAPRecord
                  WHERE     numUserCntID = @numTempDomainId
                            AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
                            AND DATEDIFF(DAY, [dtDueDate],
                                         dbo.GetUTCDateWithoutTime()) > 90
                  GROUP BY  numDivisionId
                ) X
        WHERE   #TempAPRecord1.numDivisionID = X.numDivisionID
        
           UPDATE  #TempAPRecord1
        SET     numOverNinetyDaysOverDuePaid = X.numOverNinetyDaysOverDuePaid
        FROM    ( SELECT    SUM(AmountPaid) AS numOverNinetyDaysOverDuePaid,
                            numDivisionId
                  FROM      #TempAPRecord
                  WHERE     numUserCntID = @numTempDomainId
                            AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
                            AND DATEDIFF(DAY, [dtDueDate],
                                         dbo.GetUTCDateWithoutTime()) > 90
                  GROUP BY  numDivisionId
                ) X
        WHERE   #TempAPRecord1.numDivisionID = X.numDivisionID
        
        UPDATE  #TempAPRecord1
        SET     intOverNinetyDaysOverDueCount = 2
        WHERE   numDivisionId IN (
                SELECT DISTINCT
                        [numDivisionId]
                FROM    [#TempAPRecord]
                WHERE   numUserCntID = @numTempDomainId
                        AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
                        AND DATEDIFF(DAY, [dtDueDate],
                                     dbo.GetUTCDateWithoutTime()) > 90
                        AND numCurrencyID <> @baseCurrency )
             


UPDATE  
	TR1
SET
	monUnAppliedAmount = ISNULL((SELECT SUM(ISNULL(monUnAppliedAmount,0)) FROM  #TempAPRecord TR2 WHERE TR2.numDivisionId=TR1.numDivisionId),0)
FROM
	#TempAPRecord1 TR1

UPDATE  #TempAPRecord1 SET numTotal=  isnull(numCurrentDays, 0) + --isnull(numThirtyDays, 0) + 
				isnull(numThirtyDaysOverDue, 0)
                --+ isnull(numSixtyDays, 0) 
				+ isnull(numSixtyDaysOverDue, 0)
                --+ isnull(numNinetyDays, 0) 
				+ isnull(numNinetyDaysOverDue, 0)
                --+ isnull(numOverNinetyDays, 0)
                + isnull(numOverNinetyDaysOverDue, 0) 			
             
        SELECT  [numDivisionId],
                [numContactId],
                [tintCRMType],
                [vcCompanyName],
                [vcCustPhone],
                [vcContactName] as vcApName,
                [vcEmail],
                [numPhone] as vcPhone,
				[numCurrentDays],
                [numThirtyDays],
                [numSixtyDays],
                [numNinetyDays],
                [numOverNinetyDays],
                [numThirtyDaysOverDue],
                [numSixtyDaysOverDue],
                [numNinetyDaysOverDue],
                [numOverNinetyDaysOverDue],
             ISNULL(numTotal,0) - ISNULL(monUnAppliedAmount,0) AS numTotal,
                numCompanyID,
                numDomainID,
				[intCurrentDaysCount],
                [intThirtyDaysCount],
                [intThirtyDaysOverDueCount],
                [intSixtyDaysCount],
                [intSixtyDaysOverDueCount],
                [intNinetyDaysCount],
                [intOverNinetyDaysCount],
                [intNinetyDaysOverDueCount],
                [intOverNinetyDaysOverDueCount],
				[numCurrentDaysPaid],
                [numThirtyDaysPaid],
                [numSixtyDaysPaid],
                [numNinetyDaysPaid],
                [numOverNinetyDaysPaid],
                [numThirtyDaysOverDuePaid],
                [numSixtyDaysOverDuePaid],
                [numNinetyDaysOverDuePaid],
                [numOverNinetyDaysOverDuePaid],ISNULL(monUnAppliedAmount,0) AS monUnAppliedAmount
        FROM    #TempAPRecord1
        WHERE   numUserCntID = @numTempDomainId AND (numDivisionId = @numTempDivisionId OR @numTempDivisionId IS NULL)
        ORDER BY [vcCompanyName]

        drop table #TempAPRecord1
        drop table #TempAPRecord 

        SET NOCOUNT OFF   
    
    END
GO
/****** Object:  StoredProcedure [dbo].[USP_GetColumnConfiguration1]    Script Date: 07/26/2008 16:16:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'usp_getcolumnconfiguration1' )
    DROP PROCEDURE usp_getcolumnconfiguration1
GO
CREATE PROCEDURE [dbo].[USP_GetColumnConfiguration1]
    @numDomainID NUMERIC(9) ,
    @numUserCntId NUMERIC(9) ,
    @FormId TINYINT ,
    @numtype NUMERIC(9) ,
    @numViewID NUMERIC(9) = 0
AS
    DECLARE @PageId AS TINYINT         
    IF @FormId = 10 OR @FormId = 44
        SET @PageId = 4          
    ELSE IF @FormId = 11 OR @FormId = 34 OR @FormId = 35 OR @FormId = 36 OR @FormId = 96 OR @FormId = 97
            SET @PageId = 1          
    ELSE IF @FormId = 12
                SET @PageId = 3          
    ELSE IF @FormId = 13
                    SET @PageId = 11       
    ELSE IF (@FormId = 14 AND (@numtype = 1 OR @numtype = 3)) OR @FormId = 33 OR @FormId = 38 OR @FormId = 39 OR @FormId = 23
                        SET @PageId = 2          
    ELSE IF @FormId = 14 AND (@numtype = 2 OR @numtype = 4) OR @FormId = 40 OR @FormId = 41
                            SET @PageId = 6  
    ELSE IF @FormId = 21 OR @FormId = 26 OR @FormId = 32 OR @FormId = 126 OR @FormId = 129
                                SET @PageId = 5   
    ELSE IF @FormId = 22 OR @FormId = 30
                                    SET @PageId = 5    
    ELSE IF @FormId = 76
                                        SET @PageId = 10 
    ELSE IF @FormId = 84 OR @FormId = 85
                                            SET @PageId = 5

              
    IF @PageId = 1 OR @PageId = 4
    BEGIN
        SELECT  CONVERT(VARCHAR(9), numFieldID) + '~0' AS numFieldID ,
                ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                0 AS Custom
        FROM    View_DynamicDefaultColumns
        WHERE   numFormID = @FormId
                AND ISNULL(bitSettingField, 0) = 1
                AND ISNULL(bitDeleted, 0) = 0
                AND numDomainID = @numDomainID
        UNION
        SELECT  CONVERT(VARCHAR(9), fld_id) + '~1' AS numFieldID ,
                fld_label AS vcFieldName ,
                1 AS Custom
        FROM    CFW_Fld_Master
                LEFT JOIN CFW_Fld_Dtl ON Fld_id = numFieldId
                                            AND numRelation = CASE
                                                            WHEN @numtype IN (
                                                            1, 2, 3 )
                                                            THEN numRelation
                                                            ELSE @numtype
                                                            END
                LEFT JOIN CFw_Grp_Master ON subgrp = CFw_Grp_Master.Grp_id
        WHERE   CFW_Fld_Master.grp_id = @PageId
                AND CFW_Fld_Master.numDomainID = @numDomainID
                AND Fld_type <> 'Link'
        ORDER BY Custom ,
                vcFieldName                                       
    END  
    ELSE IF @PageId = 10
    BEGIN
        SELECT  CONVERT(VARCHAR(9), numFieldID) + '~0' AS numFieldID ,
                ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                0 AS Custom
        FROM    View_DynamicDefaultColumns
        WHERE   numFormID = @FormId
                AND ISNULL(bitSettingField, 0) = 1
                AND ISNULL(bitDeleted, 0) = 0
                AND numDomainID = @numDomainID
        UNION
        SELECT  CONVERT(VARCHAR(9), fld_id) + '~1' AS numFieldID ,
                fld_label AS vcFieldName ,
                1 AS Custom
        FROM    CFW_Fld_Master
                LEFT JOIN CFW_Fld_Dtl ON Fld_id = numFieldId
                                            AND numRelation = CASE
                                                        WHEN @numtype IN (
                                                        1, 2, 3 )
                                                        THEN numRelation
                                                        ELSE @numtype
                                                        END
                LEFT JOIN CFw_Grp_Master ON subgrp = CFw_Grp_Master.Grp_id
        WHERE   CFW_Fld_Master.grp_id = 5
                AND CFW_Fld_Master.numDomainID = @numDomainID
                AND Fld_Type = 'SelectBox'
        ORDER BY Custom ,
                vcFieldName       
    END 
	ELSE IF @FormId=43
	BEGIN
		PRINT 'ABC'
		SELECT  CONVERT(VARCHAR(9), numFieldID) + '~0' AS numFieldID ,
                    ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                    0 AS Custom ,
                    vcDbColumnName
            FROM    View_DynamicDefaultColumns
            WHERE   numFormID = @FormId
                    AND ISNULL(bitSettingField, 0) = 1
                    AND ISNULL(bitDeleted, 0) = 0
                    AND numDomainID = @numDomainID
            UNION
            SELECT  CONVERT(VARCHAR(9), fld_id) + '~1' AS numFieldID ,
                    fld_label AS vcFieldName ,
                    1 AS Custom ,
                    CONVERT(VARCHAR(10), fld_id) AS vcDbColumnName
            FROM    CFW_Fld_Master
                    LEFT JOIN CFw_Grp_Master ON subgrp = CFw_Grp_Master.Grp_id
            WHERE   CFW_Fld_Master.grp_id = 1
                    AND CFW_Fld_Master.numDomainID = @numDomainID
                    AND Fld_type <> 'Link'
            ORDER BY Custom ,
                    vcFieldName          
	END
	ELSE IF @FormId = 125 AND @PageId=0
	BEGIN
			SELECT  CONVERT(VARCHAR(9), numFieldID) + '~0' AS numFieldID ,
                    ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                    0 AS Custom
            FROM    View_DynamicDefaultColumns
            WHERE   numFormID = @FormId
                    AND ISNULL(bitSettingField, 0) = 1
                    AND ISNULL(bitDeleted, 0) = 0
                    AND numDomainID = @numDomainID
	END
    ELSE IF @PageId = 5 AND (@FormId = 84 OR @FormId = 85)
    BEGIN
        SELECT  CONVERT(VARCHAR(9), numFieldID) + '~0' AS numFieldID ,
                ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                0 AS Custom ,
                vcDbColumnName
        FROM    View_DynamicDefaultColumns
        WHERE   numFormID = @FormId
                AND ISNULL(bitSettingField, 0) = 1
                AND ISNULL(bitDeleted, 0) = 0
                AND numDomainID = @numDomainID
        UNION
        SELECT  CONVERT(VARCHAR(9), fld_id) + '~1' AS numFieldID ,
                fld_label AS vcFieldName ,
                1 AS Custom ,
                CONVERT(VARCHAR(10), fld_id) AS vcDbColumnName
        FROM    CFW_Fld_Master
                LEFT JOIN CFw_Grp_Master ON subgrp = CFw_Grp_Master.Grp_id
        WHERE   CFW_Fld_Master.grp_id = @PageId
                AND CFW_Fld_Master.numDomainID = @numDomainID
                AND Fld_type = 'TextBox'
        ORDER BY Custom ,
                vcFieldName             
    END 
	ELSE IF @FormId=141
	BEGIN
		SELECT  
			CONVERT(VARCHAR(9), numFieldID) + '~0' AS numFieldID ,
            (CASE WHEN @numViewID=6 AND numFieldID=248 THEN 'BizDoc' ELSE ISNULL(vcCultureFieldName, vcFieldName) END) AS vcFieldName ,
            0 AS Custom,
            vcDbColumnName,
			bitRequired
        FROM
			View_DynamicDefaultColumns
        WHERE 
			numFormID = @FormId
            AND ISNULL(bitSettingField, 0) = 1
            AND ISNULL(bitDeleted, 0) = 0
            AND numDomainID = @numDomainID
        UNION
        SELECT 
			CONVERT(VARCHAR(9), fld_id) + '~1' AS numFieldID ,
            fld_label AS vcFieldName ,
            1 AS Custom ,
            CONVERT(VARCHAR(10), fld_id) AS vcDbColumnName,
			0 AS bitRequired
        FROM 
			CFW_Fld_Master
        LEFT JOIN 
			CFw_Grp_Master 
		ON 
			subgrp = CFw_Grp_Master.Grp_id
        WHERE 
			CFW_Fld_Master.grp_id IN (2,5)
            AND CFW_Fld_Master.numDomainID = @numDomainID
            AND Fld_type <> 'Link'
        ORDER BY 
			Custom,
            (CASE WHEN @numViewID=6 AND numFieldID=248 THEN 'BizDoc' ELSE ISNULL(vcCultureFieldName, vcFieldName) END)
	END
	ELSE IF @FormId=142
	BEGIN
		SELECT  
			CONVERT(VARCHAR(9), numFieldID) + '~0' AS numFieldID,
            ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName,
            0 AS Custom,
            vcDbColumnName,
			bitRequired
        FROM
			View_DynamicDefaultColumns
        WHERE 
			numFormID = @FormId
            AND ISNULL(bitSettingField, 0) = 1
            AND ISNULL(bitDeleted, 0) = 0
            AND numDomainID = @numDomainID
		ORDER BY
			vcFieldName
	END
	ELSE IF @FormId = 145
	BEGIN
		SELECT 
			'6~0' AS numFieldID,'Actual Start' AS vcFieldName,0 AS Custom,'dtmActualStartDate' AS vcDbColumnName,0 bitRequired
			UNION SELECT '12~0' AS numFieldID,'Build Manager' AS vcFieldName,0 AS Custom,'vcAssignedTo' AS vcDbColumnName,0 bitRequired
			UNION SELECT '11~0' AS numFieldID,'Build Process' AS vcFieldName,0 AS Custom,'Slp_Name' AS vcDbColumnName,0 bitRequired
			UNION SELECT '13~0' AS numFieldID,'Capacity Load' AS vcFieldName,0 AS Custom,'CapacityLoad' AS vcDbColumnName,0 bitRequired
			UNION SELECT '10~0' AS numFieldID,'Forecast' AS vcFieldName,0 AS Custom,'ForecastDetails' AS vcDbColumnName,0 bitRequired
			UNION SELECT '4~0' AS numFieldID,'Item' AS vcFieldName,0 AS Custom,'AssemblyItemSKU' AS vcDbColumnName,0 bitRequired
			UNION SELECT '9~0' AS numFieldID,'Item Release' AS vcFieldName,0 AS Custom,'ItemReleaseDate' AS vcDbColumnName,0 bitRequired
			UNION SELECT '2~0' AS numFieldID,'Max Build Qty' AS vcFieldName,0 AS Custom,'MaxBuildQty' AS vcDbColumnName,0 bitRequired
			UNION SELECT '15~0' AS numFieldID,'Picked | Remaining' AS vcFieldName,0 AS Custom,'vcPickedRemaining' AS vcDbColumnName,0 bitRequired
			UNION SELECT '5~0' AS numFieldID,'Planned Start' AS vcFieldName,0 AS Custom,'dtmStartDate' AS vcDbColumnName,0 bitRequired
			UNION SELECT '8~0' AS numFieldID,'Projected Finish' AS vcFieldName,0 AS Custom,'dtmProjectFinishDate' AS vcDbColumnName,0 bitRequired
			UNION SELECT '7~0' AS numFieldID,'Requested Finish' AS vcFieldName,0 AS Custom,'dtmEndDate' AS vcDbColumnName,0 bitRequired
			UNION SELECT '3~0' AS numFieldID,'Total Progress' AS vcFieldName,0 AS Custom,'TotalProgress' AS vcDbColumnName,0 bitRequired
			UNION SELECT '14~0' AS numFieldID,'Work Order Status' AS vcFieldName,0 AS Custom,'vcWorkOrderStatus' AS vcDbColumnName,0 bitRequired
			UNION SELECT '1~0' AS numFieldID,'Work Order(Qty)' AS vcFieldName,0 AS Custom,'vcWorkOrderNameWithQty' AS vcDbColumnName,0 bitRequired
	END
    ELSE
    BEGIN
        SELECT  CONVERT(VARCHAR(9), numFieldID) + '~0' AS numFieldID ,
                ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                0 AS Custom ,
                vcDbColumnName
        FROM    View_DynamicDefaultColumns
        WHERE   numFormID = @FormId
                AND ISNULL(bitSettingField, 0) = 1
                AND ISNULL(bitDeleted, 0) = 0
                AND numDomainID = @numDomainID
        UNION
        SELECT  CONVERT(VARCHAR(9), fld_id) + '~1' AS numFieldID ,
                fld_label AS vcFieldName ,
                1 AS Custom ,
                CONVERT(VARCHAR(10), fld_id) AS vcDbColumnName
        FROM    CFW_Fld_Master
                LEFT JOIN CFw_Grp_Master ON subgrp = CFw_Grp_Master.Grp_id
        WHERE   1 = (CASE WHEN @FormId=21 THEN (CASE WHEN  CFW_Fld_Master.grp_id IN (5,9) THEN 1 ELSE 0 END) ELSE (CASE WHEN  CFW_Fld_Master.grp_id = @PageId THEN 1 ELSE 0 END) END)
                AND CFW_Fld_Master.numDomainID = @numDomainID
                AND Fld_type <> 'Link'
        ORDER BY Custom ,
                vcFieldName             
    END
	                          
                   
    IF @PageId = 1 OR @PageId = 4
    BEGIN
		IF (@FormId = 96 OR @FormId = 97)
                SELECT  CONVERT(VARCHAR(9), numFieldID) + '~0' numFieldID ,
                        ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                        vcDbColumnName ,
                        0 AS Custom ,
                        tintRow AS tintOrder
                FROM    View_DynamicColumns
                WHERE   numFormId = @FormId
                        AND numDomainID = @numDomainID
                        AND ISNULL(bitCustom, 0) = 0
                        AND tintPageType = 1
                        AND ISNULL(bitSettingField, 0) = 1
                        AND numRelCntType = @numtype
                        AND ISNULL(bitDeleted, 0) = 0
                UNION
                SELECT  CONVERT(VARCHAR(9), numFieldID) + '~1' AS numFieldID ,
                        vcFieldName ,
                        vcFieldName AS vcDbColumnName ,
                        1 AS Custom ,
                        tintRow AS tintOrder
                FROM    View_DynamicCustomColumns
                WHERE   numFormID = @FormId
                        AND numDomainID = @numDomainID
                        AND grp_id = @PageId
                        AND numDomainID = @numDomainID
                        AND vcAssociatedControlType <> 'Link'
                        AND ISNULL(bitCustom, 0) = 1
                        AND tintPageType = 1
                        AND numRelCntType = @numtype
                ORDER BY tintOrder                
            ELSE
                SELECT  CONVERT(VARCHAR(9), numFieldID) + '~0' numFieldID ,
                        ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                        vcDbColumnName ,
                        0 AS Custom ,
                        tintRow AS tintOrder
                FROM    View_DynamicColumns
                WHERE   numFormId = @FormId
                        AND numUserCntID = @numUserCntID
                        AND numDomainID = @numDomainID
                        AND ISNULL(bitCustom, 0) = 0
                        AND tintPageType = 1
                        AND ISNULL(bitSettingField, 0) = 1
                        AND numRelCntType = @numtype
                        AND ISNULL(bitDeleted, 0) = 0
                UNION
                SELECT  CONVERT(VARCHAR(9), numFieldID) + '~1' AS numFieldID ,
                        vcFieldName ,
                        '' AS vcDbColumnName ,
                        1 AS Custom ,
                        tintRow AS tintOrder
                FROM    View_DynamicCustomColumns
                WHERE   numFormID = @FormId
                        AND numUserCntID = @numUserCntID
                        AND numDomainID = @numDomainID
                        AND grp_id = @PageId
                        AND numDomainID = @numDomainID
                        AND vcAssociatedControlType <> 'Link'
                        AND ISNULL(bitCustom, 0) = 1
                        AND tintPageType = 1
                        AND numRelCntType = @numtype
                ORDER BY tintOrder          
    END     
    ELSE IF @PageId = 10
    BEGIN
        SELECT  CONVERT(VARCHAR(9), numFieldID) + '~0' numFieldID ,
                ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                vcDbColumnName ,
                0 AS Custom ,
                tintRow AS tintOrder ,
                numListID ,
                numFieldID AS [numFieldID1]
        FROM    View_DynamicColumns
        WHERE   numFormId = @FormId
                AND numUserCntID = @numUserCntID
                AND numDomainID = @numDomainID
                AND ISNULL(bitCustom, 0) = 0
                AND tintPageType = 1
                AND ISNULL(bitSettingField, 0) = 1
                AND numRelCntType = @numtype
                AND ISNULL(bitDeleted, 0) = 0
        UNION
        SELECT  CONVERT(VARCHAR(9), numFieldID) + '~1' AS numFieldID ,
                vcFieldName ,
                '' AS vcDbColumnName ,
                1 AS Custom ,
                tintRow AS tintOrder ,
                numListID ,
                numFieldID AS [numFieldID1]
        FROM    View_DynamicCustomColumns
        WHERE   numFormID = @FormId
                AND numUserCntID = @numUserCntID
                AND numDomainID = @numDomainID   
                AND numDomainID = @numDomainID
                AND vcAssociatedControlType = 'SelectBox'
                AND ISNULL(bitCustom, 0) = 1
                AND tintPageType = 1
                AND numRelCntType = @numtype
        ORDER BY tintOrder    
    END	   
    ELSE IF @PageId = 5 AND (@FormId = 84 OR @FormId = 85)
    BEGIN
        IF EXISTS (SELECT 
						'col1'
                    FROM 
						DycFormConfigurationDetails
                    WHERE 
						numDomainID = @numDomainID
                        AND numFormId = @FormID
                        AND numFieldID IN (507, 508, 509, 510, 511, 512)
					)
        BEGIN
            SELECT  CONVERT(VARCHAR(9), numFieldID) + '~0' numFieldID ,
                    ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                    vcDbColumnName ,
                    0 AS Custom ,
                    tintRow AS tintOrder ,
                    vcAssociatedControlType ,
                    numlistid ,
                    vcLookBackTableName ,
                    numFieldID AS FieldId ,
                    bitAllowEdit ,
                    0 AS AZordering
            FROM    View_DynamicColumns
            WHERE   numFormId = @FormID
                    AND numDomainID = @numDomainID
                    AND ISNULL(bitCustom, 0) = 0   
                    AND ISNULL(bitSettingField, 0) = 1
                    AND ISNULL(bitDeleted, 0) = 0
            UNION
            SELECT  CONVERT(VARCHAR(9), numFieldID) + '~1' AS numFieldID ,
                    vcFieldName ,
                    '' AS vcDbColumnName ,
                    1 AS Custom ,
                    tintRow AS tintOrder ,
                    vcAssociatedControlType ,
                    numlistid ,
                    '' AS vcLookBackTableName ,
                    numFieldID AS FieldId ,
                    0 AS bitAllowEdit ,
                    0 AS AZordering
            FROM    View_DynamicCustomColumns
            WHERE   numFormID = @FormID
                    AND numDomainID = @numDomainID
                    AND grp_id = @PageID
                    AND vcAssociatedControlType = 'TextBox'
                    AND ISNULL(bitCustom, 0) = 1
            ORDER BY tintOrder 
        END
        ELSE
        BEGIN
            SELECT  CONVERT(VARCHAR(9), numFieldID) + '~0' numFieldID ,
                    ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                    vcDbColumnName ,
                    0 AS Custom ,
                    tintRow AS tintOrder ,
                    vcAssociatedControlType ,
                    numlistid ,
                    vcLookBackTableName ,
                    numFieldID AS FieldId ,
                    bitAllowEdit ,
                    0 AS AZordering
            FROM    View_DynamicColumns
            WHERE   numFormId = @FormID
                    AND numDomainID = @numDomainID
                    AND ISNULL(bitCustom, 0) = 0   
                    AND ISNULL(bitSettingField, 0) = 1
                    AND ISNULL(bitDeleted, 0) = 0
            UNION
            SELECT  CONVERT(VARCHAR(9), numFieldID) + '~1' AS numFieldID ,
                    vcFieldName ,
                    '' AS vcDbColumnName ,
                    1 AS Custom ,
                    tintRow AS tintOrder ,
                    vcAssociatedControlType ,
                    numlistid ,
                    '' AS vcLookBackTableName ,
                    numFieldID AS FieldId ,
                    0 AS bitAllowEdit ,
                    0 AS AZordering
            FROM    View_DynamicCustomColumns
            WHERE   numFormID = @FormID
                    AND numDomainID = @numDomainID
                    AND grp_id = @PageID
                    AND vcAssociatedControlType = 'TextBox'
                    AND ISNULL(bitCustom, 0) = 1
            UNION
            SELECT  '507~0' AS numFieldID ,
                    'Title:A-Z' AS vcFieldName ,
                    'Title:A-Z' AS vcDbColumnName ,
                    1 AS Custom ,
                    0 AS tintOrder ,
                    ' ' AS vcAssociatedControlType ,
                    1 AS numlistid ,
                    '' AS vcLookBackTableName ,
                    507 AS FieldId ,
                    0 AS bitAllowEdit ,
                    1 AS IsFixed
            UNION
            SELECT  '508~0' AS numFieldID ,
                    'Title:Z-A' AS vcFieldName ,
                    'Title:Z-A' AS vcDbColumnName ,
                    1 AS Custom ,
                    0 AS tintOrder ,
                    ' ' AS vcAssociatedControlType ,
                    1 AS numlistid ,
                    '' AS vcLookBackTableName ,
                    508 AS FieldId ,
                    0 AS bitAllowEdit ,
                    1 AS IsFixed
            UNION
            SELECT  '509~0' AS numFieldID ,
                    'Price:Low to High' AS vcFieldName ,
                    'Price:Low to High' AS vcDbColumnName ,
                    1 AS Custom ,
                    0 AS tintOrder ,
                    ' ' AS vcAssociatedControlType ,
                    1 AS numlistid ,
                    '' AS vcLookBackTableName ,
                    509 AS FieldId ,
                    0 AS bitAllowEdit ,
                    1 AS IsFixed
            UNION
            SELECT  '510~0' AS numFieldID ,
                    'Price:High to Low' AS vcFieldName ,
                    'Price:High to Low' AS vcDbColumnName ,
                    1 AS Custom ,
                    0 AS tintOrder ,
                    ' ' AS vcAssociatedControlType ,
                    1 AS numlistid ,
                    '' AS vcLookBackTableName ,
                    510 AS FieldId ,
                    0 AS bitAllowEdit ,
                    1 AS IsFixed
            UNION
            SELECT  '511~0' AS numFieldID ,
                    'New Arrival' AS vcFieldName ,
                    'New Arrival' AS vcDbColumnName ,
                    1 AS Custom ,
                    0 AS tintOrder ,
                    ' ' AS vcAssociatedControlType ,
                    1 AS numlistid ,
                    '' AS vcLookBackTableName ,
                    511 AS FieldId ,
                    0 AS bitAllowEdit ,
                    1 AS IsFixed
            UNION
            SELECT  '512~0' AS numFieldID ,
                    'Oldest' AS vcFieldName ,
                    'Oldest' AS vcDbColumnName ,
                    1 AS Custom ,
                    0 AS tintOrder ,
                    ' ' AS vcAssociatedControlType ,
                    1 AS numlistid ,
                    '' AS vcLookBackTableName ,
                    512 AS FieldId ,
                    0 AS bitAllowEdit ,
                    1 AS IsFixed
            ORDER BY tintOrder 
        END
    END
	ELSE IF @FormId=43
	BEGIN
            SELECT  CONVERT(VARCHAR(9), numFieldID) + '~0' numFieldID ,
                    ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                    vcDbColumnName ,
                    0 AS Custom ,
                    tintRow AS tintOrder ,
                    vcAssociatedControlType ,
                    numlistid ,
                    vcLookBackTableName ,
                    numFieldID AS FieldId ,
                    bitAllowEdit
            FROM    View_DynamicColumns
            WHERE   numFormId = @FormId
                    AND numUserCntID = @numUserCntID
                    AND numDomainID = @numDomainID
                    AND ISNULL(bitCustom, 0) = 0
                    AND tintPageType = 1
                    AND ISNULL(bitSettingField, 0) = 1
                    AND ISNULL(numRelCntType, 0) = @numtype
                    AND ISNULL(bitDeleted, 0) = 0
                    AND ISNULL(numViewID, 0) = @numViewID
            UNION
            SELECT  CONVERT(VARCHAR(9), numFieldID) + '~1' AS numFieldID ,
                    vcFieldName ,
                    '' AS vcDbColumnName ,
                    1 AS Custom ,
                    tintRow AS tintOrder ,
                    vcAssociatedControlType ,
                    numlistid ,
                    '' AS vcLookBackTableName ,
                    numFieldID AS FieldId ,
                    0 AS bitAllowEdit
            FROM    View_DynamicCustomColumns
            WHERE   numFormID = @FormId
                    AND numUserCntID = @numUserCntID
                    AND numDomainID = @numDomainID
                    AND grp_id = 1
                    AND numDomainID = @numDomainID
                    AND vcAssociatedControlType <> 'Link'
                    AND ISNULL(bitCustom, 0) = 1
                    AND tintPageType = 1
                    AND ISNULL(numRelCntType, 0) = @numtype
                    AND ISNULL(numViewID, 0) = @numViewID
            ORDER BY tintOrder       
	END
	ELSE IF @FormId=141
	BEGIN
		SELECT  
			CONVERT(VARCHAR(9), numFieldID) + '~0' numFieldID ,
            (CASE WHEN @numViewID=6 AND numFieldID=248 THEN 'BizDoc' ELSE ISNULL(vcCultureFieldName, vcFieldName) END) AS vcFieldName ,
            vcDbColumnName,
            0 AS Custom,
            tintRow AS tintOrder,
            vcAssociatedControlType,
            numlistid,
            vcLookBackTableName,
            numFieldID AS FieldId,
            bitAllowEdit
        FROM 
			View_DynamicColumns
        WHERE 
			numFormId = @FormId
            AND numUserCntID = @numUserCntID
            AND numDomainID = @numDomainID
            AND ISNULL(bitCustom, 0) = 0
            AND tintPageType = 1
            AND ISNULL(bitSettingField, 0) = 1
            AND ISNULL(numRelCntType, 0) = @numtype
            AND ISNULL(bitDeleted, 0) = 0
            AND ISNULL(numViewID, 0) = @numViewID
        UNION
        SELECT 
			CONVERT(VARCHAR(9), numFieldID) + '~1' AS numFieldID ,
            vcFieldName ,
            '' AS vcDbColumnName ,
            1 AS Custom ,
            tintRow AS tintOrder ,
            vcAssociatedControlType ,
            numlistid ,
            '' AS vcLookBackTableName ,
            numFieldID AS FieldId ,
            0 AS bitAllowEdit
        FROM 
			View_DynamicCustomColumns
        WHERE 
			numFormID = @FormId
            AND numUserCntID = @numUserCntID
            AND numDomainID = @numDomainID
            AND grp_id IN (2,5)
            AND numDomainID = @numDomainID
            AND vcAssociatedControlType <> 'Link'
            AND ISNULL(bitCustom, 0) = 1
            AND tintPageType = 1
            AND ISNULL(numRelCntType, 0) = @numtype
            AND ISNULL(numViewID, 0) = @numViewID
        ORDER BY 
			tintOrder 
	END
	ELSE IF @FormId=142
	BEGIN
		SELECT  
			CONVERT(VARCHAR(9), numFieldID) + '~0' numFieldID ,
            ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
            vcDbColumnName ,
            0 AS Custom ,
            tintRow AS tintOrder ,
            vcAssociatedControlType ,
            numlistid ,
            vcLookBackTableName ,
            numFieldID AS FieldId ,
            bitAllowEdit
        FROM 
			View_DynamicColumns
        WHERE 
			numFormId = @FormId
            AND numUserCntID = @numUserCntID
            AND numDomainID = @numDomainID
            AND ISNULL(bitCustom, 0) = 0
            AND tintPageType = 1
            AND ISNULL(bitSettingField, 0) = 1
            AND ISNULL(numRelCntType, 0) = @numtype
            AND ISNULL(bitDeleted, 0) = 0
            AND ISNULL(numViewID, 0) = @numViewID
	END
	ELSE IF @FormId=145
	BEGIN
		SELECT 
			CONVERT(VARCHAR(9), numFieldID) + '~0' numFieldID ,
            (CASE numFieldID
				WHEN 1 THEN 'Work Order(Qty)'
				WHEN 2 THEN 'Max Build Qty'
				WHEN 3 THEN 'Total Progress'
				WHEN 4 THEN 'Item'
				WHEN 5 THEN 'Planned Start'
				WHEN 6 THEN 'Actual Start'
				WHEN 7 THEN 'Requested Finish'
				WHEN 8 THEN 'Projected Finish'
				WHEN 9 THEN 'Item Release'
				WHEN 10 THEN 'Forecast'
				WHEN 11 THEN 'Build Process'
				WHEN 12 THEN 'Build Manager'
				WHEN 13 THEN 'Capacity Load'
				WHEN 14 THEN 'Work Order Status'
				WHEN 15 THEN 'Picked | Remaining'
				ELSE '-'
			END) AS vcFieldName ,
            (CASE numFieldID
				WHEN 1 THEN 'vcWorkOrderNameWithQty'
				WHEN 2 THEN 'MaxBuildQty'
				WHEN 3 THEN 'TotalProgress'
				WHEN 4 THEN 'AssemblyItemSKU'
				WHEN 5 THEN 'dtmStartDate'
				WHEN 6 THEN 'dtmActualStartDate'
				WHEN 7 THEN 'dtmEndDate'
				WHEN 8 THEN 'dtmProjectFinishDate'
				WHEN 9 THEN 'ItemReleaseDate'
				WHEN 10 THEN 'ForecastDetails'
				WHEN 11 THEN 'Slp_Name'
				WHEN 12 THEN 'vcAssignedTo'
				WHEN 13 THEN 'CapacityLoad'
				WHEN 14 THEN 'vcWorkOrderStatus'
				WHEN 15 THEN 'vcPickedRemaining'
				ELSE '-'
			END) AS vcDbColumnName ,
            0 AS Custom ,
            intRowNum AS tintOrder ,
            'Label' vcAssociatedControlType ,
            0 AS numlistid,
            'WorkOrder' AS vcLookBackTableName ,
            numFieldID AS FieldId ,
            0 AS  bitAllowEdit
		FROM 
			DycFormConfigurationDetails 
		WHERE 
			numFormId=145
			AND numUserCntID = @numUserCntID
            AND numDomainID = @numDomainID
	END
    ELSE
    BEGIN
		SELECT  CONVERT(VARCHAR(9), numFieldID) + '~0' numFieldID ,
				ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
				vcDbColumnName ,
				0 AS Custom ,
				tintRow AS tintOrder ,
				vcAssociatedControlType ,
				numlistid ,
				vcLookBackTableName ,
				numFieldID AS FieldId ,
				bitAllowEdit
		FROM    View_DynamicColumns
		WHERE   numFormId = @FormId
				AND numUserCntID = @numUserCntID
				AND numDomainID = @numDomainID
				AND ISNULL(bitCustom, 0) = 0
				AND tintPageType = 1
				AND ISNULL(bitSettingField, 0) = 1
				AND ISNULL(numRelCntType, 0) = @numtype
				AND ISNULL(bitDeleted, 0) = 0
				AND ISNULL(numViewID, 0) = @numViewID
		UNION
		SELECT  CONVERT(VARCHAR(9), numFieldID) + '~1' AS numFieldID ,
				vcFieldName ,
				'' AS vcDbColumnName ,
				1 AS Custom ,
				tintRow AS tintOrder ,
				vcAssociatedControlType ,
				numlistid ,
				'' AS vcLookBackTableName ,
				numFieldID AS FieldId ,
				0 AS bitAllowEdit
		FROM    View_DynamicCustomColumns
		WHERE   numFormID = @FormId
				AND numUserCntID = @numUserCntID
				AND numDomainID = @numDomainID
				AND 1 = (CASE WHEN @FormId=21 THEN (CASE WHEN  grp_id IN (5,9) THEN 1 ELSE 0 END) ELSE (CASE WHEN  grp_id = @PageId THEN 1 ELSE 0 END) END)
				AND numDomainID = @numDomainID
				AND vcAssociatedControlType <> 'Link'
				AND ISNULL(bitCustom, 0) = 1
				AND tintPageType = 1
				AND ISNULL(numRelCntType, 0) = @numtype
				AND ISNULL(numViewID, 0) = @numViewID
		ORDER BY tintOrder          
    END
GO

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetOppSerialLotNumber')
DROP PROCEDURE USP_GetOppSerialLotNumber
GO
CREATE PROCEDURE [dbo].[USP_GetOppSerialLotNumber]
	@numOppID AS NUMERIC(18,0),
	@numOppItemID AS NUMERIC(18,0),
	@numWarehouseItemID AS NUMERIC(18,0)
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @numDomainID AS NUMERIC(18,0)
	DECLARE @numItemID NUMERIC(18,0)
	DECLARE @numWarehouseID NUMERIC(18,0)
	SELECT @numDomainID= numDomainId FROM OpportunityMaster WHERE numOppId = @numOppID
	SELECT @numItemID=numItemID,@numWarehouseID=numWarehouseID FROM WarehouseItems WHERE numWarehouseItemID=@numWarehouseItemID

	-- Available Serail Lot Numbers
	SELECT
		ROW_NUMBER() OVER(ORDER BY dExpirationDate DESC) AS OrderNo,
		numWareHouseItmsDTLID,
		vcSerialNo,
		numQty,
		dbo.FormatedDateFromDate(dExpirationDate,@numDomainID) AS dExpirationDate
	FROM
		WareHouseItmsDTL
	WHERE
		numWareHouseItemID IN (SELECT numWareHouseItemID FROM WareHouseItems WHERE numItemID=@numItemID AND numWareHouseID=@numWarehouseID)
		AND ISNULL(numQty,0) > 0 
	ORDER BY
		dExpirationDate DESC

    -- Selected Serail Lot Numbers
	SELECT
		ROW_NUMBER() OVER(ORDER BY WareHouseItmsDTL.dExpirationDate DESC) AS OrderNo,
		WareHouseItmsDTL.numWareHouseItmsDTLID,
		WareHouseItmsDTL.vcSerialNo,
		dbo.FormatedDateFromDate(WareHouseItmsDTL.dExpirationDate,@numDomainID) AS dExpirationDate,
		OppWarehouseSerializedItem.numQty,
		OppWarehouseSerializedItem.numOppBizDocsId
	FROM
		OppWarehouseSerializedItem
	JOIN
		WareHouseItmsDTL
	ON
		OppWarehouseSerializedItem.numWarehouseItmsDTLID = WareHouseItmsDTL.numWareHouseItmsDTLID
	WHERE
		numOppID = @numOppID AND
		numOppItemID = @numOppItemID
	ORDER BY
		WareHouseItmsDTL.dExpirationDate DESC

END



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetReportDashboardDTL')
DROP PROCEDURE USP_GetReportDashboardDTL
GO
CREATE PROCEDURE [dbo].[USP_GetReportDashboardDTL]   
@numDomainID AS NUMERIC(9),               
@numDashBoardID AS NUMERIC(9)                 
AS  
BEGIN
	SELECT 
		ReportDashboard.numDomainID
		,ReportDashboard.numReportID
		,[numUserCntID]
		,ReportDashboard.tintReportType
		,[tintChartType]
		,[vcHeaderText]
		,[vcFooterText]
		,[tintRow]
		,[tintColumn]
		,[vcXAxis]
		,[vcYAxis]
		,[vcAggType]
		,[tintReportCategory]
		,[intHeight]
		,[intWidth]
		,[numDashboardTemplateID]
		,[bitNewAdded]
		,[vcTimeLine]
		,[vcGroupBy]
		,[vcTeritorry]
		,[vcFilterBy]
		,[vcFilterValue]
		,ReportDashboard.dtFromDate
		,ReportDashboard.dtToDate
		,[numRecordCount]
		,[tintControlField]
		,[vcDealAmount]
		,[tintOppType]
		,[lngPConclAnalysis]
		,[bitTask]
		,[tintTotalProgress]
		,[tintMinNumber]
		,[tintMaxNumber]
		,[tintQtyToDisplay]
		,[vcDueDate] 
		,ISNULL(bitDealWon,0) bitDealWon
		,ISNULL(bitGrossRevenue,0) bitGrossRevenue
		,ISNULL(bitRevenue,0) bitRevenue
		,ISNULL(bitGrossProfit,0) bitGrossProfit
		,ISNULL(bitCustomTimeline,0) bitCustomTimeline
		,ISNULL(bitBilledButNotReceived,0) bitBilledButNotReceived
		,ISNULL(bitReceviedButNotBilled,0) bitReceviedButNotBilled
		,ISNULL(bitSoldButNotShipped,0) bitSoldButNotShipped
		,ISNULL(ReportListMaster.bitDefault,0) bitDefault
		,ISNULL(ReportListMaster.intDefaultReportID,0) intDefaultReportID
	FROM 
		ReportDashboard 
	LEFT JOIN 
		ReportListMaster 
	ON 
		ReportDashboard.numReportID=ReportListMaster.numReportID 
	WHERE 
		ReportDashboard.numDomainID=@numDomainID 
		AND numDashBoardID=@numDashBoardID
END
GO
GO
/****** Object:  StoredProcedure [dbo].[USP_ItemList1]    Script Date: 05/07/2009 18:14:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Created By Anoop Jayaraj                                        
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'usp_itemlist1' ) 
    DROP PROCEDURE usp_itemlist1
GO
CREATE PROCEDURE [dbo].[USP_ItemList1]
    @ItemClassification AS NUMERIC(9) = 0,
    @IsKit AS TINYINT,
    @SortChar CHAR(1) = '0',
    @CurrentPage INT,
    @PageSize INT,
    @TotRecs INT OUTPUT, 
    @columnName AS VARCHAR(50),
    @columnSortOrder AS VARCHAR(10),
    @numDomainID AS NUMERIC(9) = 0,
    @ItemType AS CHAR(1),
    @bitSerialized AS BIT,
    @numItemGroup AS NUMERIC(9),
    @bitAssembly AS BIT,
    @numUserCntID AS NUMERIC(9),
    @Where	AS VARCHAR(MAX),
    @IsArchive AS BIT = 0,
    @byteMode AS TINYINT,
	@bitAsset as BIT=0,
	@bitRental as BIT=0,
	@bitContainer AS BIT=0,
	@SearchText VARCHAR(300),
	@vcRegularSearchCriteria varchar(MAX)='',
	@vcCustomSearchCriteria varchar(MAX)='',
	@tintDashboardReminderType TINYINT = 0,
	@bitMultiSelect AS TINYINT,
	@numFilteredWarehouseID AS NUMERIC(18,0)=0,
	@vcAsile AS VARCHAR(500)='',
	@vcRack AS VARCHAR(500)='',
	@vcShelf AS VARCHAR(500)='',
	@vcBin AS VARCHAR(500)='',
	@bitApplyFilter As BIT=0
AS 
BEGIN	
	DECLARE @numWarehouseID NUMERIC(18,0)
	SELECT TOP 1 @numWarehouseID = numWareHouseID FROM Warehouses WHERE numDomainID = @numDomainID


	CREATE TABLE #tempForm 
	(
		tintOrder TINYINT,vcDbColumnName nvarchar(50),vcOrigDbColumnName nvarchar(50),vcFieldName nvarchar(50),vcAssociatedControlType nvarchar(50),vcListItemType varchar(3),
		numListID numeric(9),vcLookBackTableName varchar(50),bitCustomField BIT,numFieldId NUMERIC,bitAllowSorting BIT,bitAllowEdit BIT,bitIsRequired BIT,bitIsEmail BIT,
		bitIsAlphaNumeric BIT,bitIsNumeric BIT,bitIsLengthValidation BIT,intMaxLength int,intMinLength int,bitFieldMessage BIT,vcFieldMessage VARCHAR(500),
		ListRelID numeric(9),intColumnWidth int,bitAllowFiltering BIT,vcFieldDataType CHAR(1),Grp_Id TINYINT
	)

	-- FIRST CHECK IF GRID COLUMNS ARE CONFIGURED
	DECLARE  @Nocolumns  AS TINYINT = 0

	SELECT 
		@Nocolumns=ISNULL(SUM(TotalRow),0) 
	FROM
	(            
		SELECT 
			COUNT(*) TotalRow 
		FROM 
			View_DynamicColumns 
		WHERE 
			numFormId=21 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND numRelCntType = 0
		UNION 
		SELECT 
			COUNT(*) TotalRow 
		FROM 
			View_DynamicCustomColumns 
		WHERE 
			numFormId=21 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND numRelCntType = 0
	) TotalRows

	-- IF GRID COLUMNS ARE NOT CONFIGURES THEN CHECK IF MASTER FORM CONFIGURATION IS CREATED BY ADMINISTRATOR
	DECLARE @IsMasterConfAvailable BIT = 0
	DECLARE @numUserGroup NUMERIC(18,0)

	IF @Nocolumns=0
	BEGIN
		SELECT @numUserGroup = numGroupID FROM UserMaster WHERE numUserDetailId = @numUserCntID 

		IF (SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = @numDomainID AND numFormID = 21 AND numRelCntType=0 AND bitGridConfiguration = 1 AND numGroupID = @numUserGroup) > 0
		BEGIN
			SET @IsMasterConfAvailable = 1
		END
	END

	--IF MASTERCONFIGURATION IS AVAILABLE THEN LOAD IT OTHERWISE LOAD DEFAULT COLUMNS
	IF @IsMasterConfAvailable = 1
	BEGIN
		INSERT INTO DycFormConfigurationDetails 
		(
			numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth
		)
		SELECT 
			21,numFieldId,intColumnNum,intRowNum,@numDomainID,@numUserCntID,0,1,0,intColumnWidth
		FROM 
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId=21 AND 
			View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicColumnsMasterConfig.numRelCntType=0 AND 
			View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
			ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
		UNION
		SELECT 
			21,numFieldId,intColumnNum,intRowNum,@numDomainID,@numUserCntID,0,1,1,intColumnWidth
		FROM 
			View_DynamicCustomColumnsMasterConfig
		WHERE 
			View_DynamicCustomColumnsMasterConfig.numFormId=21 AND 
			View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicCustomColumnsMasterConfig.numRelCntType=0 AND 
			View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1

		INSERT INTO 
			#tempForm
		SELECT 
			(intRowNum + 1) as tintOrder, vcDbColumnName,	vcOrigDbColumnName,	ISNULL(vcCultureFieldName,vcFieldName), vcAssociatedControlType,
			vcListItemType, numListID, vcLookBackTableName,	bitCustom, numFieldId, bitAllowSorting,	bitInlineEdit, bitIsRequired,
			bitIsEmail,	bitIsAlphaNumeric, bitIsNumeric, bitIsLengthValidation,	intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,
			ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType,0
		FROM 
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId=21 AND 
			View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicColumnsMasterConfig.numRelCntType=0 AND 
			View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
			ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
		UNION
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName, vcAssociatedControlType,'' as vcListItemType,numListID,'',
			bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,'',Grp_Id
		FROM 
			View_DynamicCustomColumnsMasterConfig
		WHERE 
			View_DynamicCustomColumnsMasterConfig.numFormId=21 AND 
			View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicCustomColumnsMasterConfig.numRelCntType=0 AND 
			View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1
		ORDER BY 
			tintOrder ASC  
	END
	ELSE                                        
	BEGIN
		IF @Nocolumns=0
		BEGIN
			INSERT INTO DycFormConfigurationDetails 
			(
				numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth
			)
			SELECT 
				21,numFieldId,0,Row_number() over(order by tintRow desc),@numDomainID,@numUserCntID,0,1,0,intColumnWidth
			FROM 
				View_DynamicDefaultColumns
			WHERE 
				numFormId=21 
				AND bitDefault=1 
				AND ISNULL(bitSettingField,0)=1 
				AND numDomainID=@numDomainID
			ORDER BY 
				tintOrder asc   
		END

		INSERT INTO 
			#tempForm
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcOrigDbColumnName,ISNULL(vcCultureFieldName,vcFieldName),                  
			vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName,bitCustom,numFieldId,bitAllowSorting,bitInlineEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength,bitFieldMessage,
			vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType,0
		FROM 
			View_DynamicColumns 
		WHERE 
			numFormId=21 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND ISNULL(bitSettingField,0)=1 
			AND ISNULL(bitCustom,0)=0  
			AND numRelCntType = 0
		UNION
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName,vcAssociatedControlType,'' as vcListItemType,numListID,'',bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,'',Grp_Id
		FROM 
			View_DynamicCustomColumns
		WHERE 
			numFormId=21 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND ISNULL(bitCustom,0)=1 
			AND numRelCntType = 0
		ORDER BY 
			tintOrder asc  
	END   

	IF @byteMode=0
	BEGIN
		DECLARE @strColumns AS VARCHAR(MAX)
		SET @strColumns = ' I.numItemCode,I.vcItemName,ISNULL(DivisionMaster.numDivisionID,0) numDivisionID,ISNULL(DivisionMaster.tintCRMType,0) tintCRMType, ' + CAST(ISNULL(@numWarehouseID,0) AS VARCHAR) + ' AS numWarehouseID, I.numDomainID,0 AS numRecOwner,0 AS numTerID, I.charItemType,I.bitSerialized,I.bitLotNo, I.numItemCode AS [numItemCode~211~0]'

		DECLARE @tintOrder AS TINYINT                                                  
		DECLARE @vcFieldName AS VARCHAR(50)                                                  
		DECLARE @vcListItemType AS VARCHAR(3)                                             
		DECLARE @vcListItemType1 AS VARCHAR(1)                                                 
		DECLARE @vcAssociatedControlType VARCHAR(10)                                                  
		DECLARE @numListID AS NUMERIC(9)                                                  
		DECLARE @vcDbColumnName VARCHAR(50)                      
		DECLARE @WhereCondition VARCHAR(2000)                       
		DECLARE @vcLookBackTableName VARCHAR(2000)                
		DECLARE @bitCustom AS BIT                  
		DECLARE @numFieldId AS NUMERIC  
		DECLARE @bitAllowSorting AS CHAR(1)   
		DECLARE @bitAllowEdit AS CHAR(1)                   
        DECLARE @vcColumnName AS VARCHAR(200)
		DECLARE @Grp_ID AS TINYINT
		SET @tintOrder = 0                                                  
		SET @WhereCondition = ''                 
                   
		DECLARE @ListRelID AS NUMERIC(9) 
		DECLARE @SearchQuery AS VARCHAR(MAX) = ''

		IF LEN(@SearchText) > 0
		BEGIN
			SET @SearchQuery = ' I.numItemCode LIKE ''%' + @SearchText + '%'' OR I.vcItemName LIKE ''%' + @SearchText + '%'' OR I.vcModelID LIKE ''%' + @SearchText + '%'' OR I.vcSKU LIKE ''%' + @SearchText + '%'' OR cmp.vcCompanyName LIKE ''%' + @SearchText + '%'' OR I.vcManufacturer LIKE ''%' + @SearchText + '%'''
		END


		DECLARE @j INT = 0
  
		SELECT TOP 1
				@tintOrder = tintOrder + 1,
				@vcDbColumnName = vcDbColumnName,
				@vcFieldName = vcFieldName,
				@vcAssociatedControlType = vcAssociatedControlType,
				@vcListItemType = vcListItemType,
				@numListID = numListID,
				@vcLookBackTableName = vcLookBackTableName,
				@bitCustom = bitCustomField,
				@numFieldId = numFieldId,
				@bitAllowSorting = bitAllowSorting,
				@bitAllowEdit = bitAllowEdit,
				@ListRelID = ListRelID
		FROM    #tempForm --WHERE bitCustomField=1
		ORDER BY tintOrder ASC        
		IF((SELECT COUNt(*) FROM #tempForm WHERE vcDbColumnName='StockQtyCount'  OR  vcDbColumnName='StockQtyAdjust')  >0)
		BEGIN
		IF ISNULL(@numFilteredWarehouseID,0)=0
		BEGIN
			SET @strColumns = @strColumns+ ',0 as numIndividualOnHandQty,0 as numAvailabilityQty,0 AS monAverageCost,0 as numWareHouseItemId,0 AS numAssetChartAcntId'
		END   
		ELSE IF ISNULL(@numFilteredWarehouseID,0)>0
		BEGIN
			SET @strColumns = @strColumns+ ',WarehouseItems.numOnHand as numIndividualOnHandQty,(WarehouseItems.numOnHand + WarehouseItems.numAllocation) as numAvailabilityQty,I.monAverageCost,WarehouseItems.numWareHouseItemId as numWareHouseItemId,I.numAssetChartAcntId,
			((isnull(WarehouseItems.numOnHand,0) + isnull(WarehouseItems.numAllocation,0))*isnull(I.monAverageCost ,0)) as StockValue'
		END  
		END
		WHILE @tintOrder > 0                                                  
		BEGIN
			IF @bitCustom = 0
			BEGIN
				DECLARE  @Prefix  AS VARCHAR(5)

				IF @vcLookBackTableName = 'Item'
					SET @Prefix = 'I.'
				ELSE IF @vcLookBackTableName = 'CompanyInfo'
					SET @Prefix = 'cmp.'
				ELSE IF @vcLookBackTableName = 'Vendor'
					SET @Prefix = 'V.'

				SET @vcColumnName= @vcDbColumnName+'~'+ convert(varchar(10),@numFieldId) +'~0'

				IF @vcAssociatedControlType='SelectBox' 
				BEGIN
					IF @vcListItemType='LI' 
					BEGIN
						SET @strColumns = @strColumns+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName+']'
						SET @WhereCondition= @WhereCondition +' LEFT JOIN ListDetails L'+ convert(varchar(3),@tintOrder)+ ' ON L'+convert(varchar(3),@tintOrder)+ '.numListItemID='+@vcDbColumnName                                                      
					END
					ELSE IF @vcListItemType = 'UOM'
					BEGIN
						SET @strColumns = @strColumns + ',ISNULL((SELECT vcUnitName FROM UOM WHERE numUOMId = I.' + @vcDbColumnName + '),'''')' + ' ['+ @vcColumnName+']'
					END
					ELSE IF @vcListItemType = 'IG'
					BEGIN
						SET @strColumns = @strColumns+ ',ISNULL((SELECT vcItemGroup FROM ItemGroups WHERE numItemGroupID = I.numItemGroup),'''')' + ' ['+ @vcColumnName+']'  
					END
				END
				ELSE IF @vcDbColumnName = 'vcPathForTImage'
		   		BEGIN
					SET @strColumns = @strColumns + ',ISNULL(II.vcPathForTImage,'''') AS ' + ' ['+ @vcColumnName+']'                  
					SET @WhereCondition = @WhereCondition + ' LEFT JOIN ItemImages II ON II.numItemCode = I.numItemCode AND bitDefault = 1'
				END
				ELSE IF @vcLookBackTableName = 'Item' AND @vcDbColumnName = 'monListPrice'
				BEGIN
					SET @strColumns = @strColumns + ',CASE WHEN I.charItemType=''P'' THEN ISNULL((SELECT TOP 1 monWListPrice FROM WarehouseItems WHERE numDomainID=I.numDomainID AND numItemID=I.numItemCode AND monWListPrice > 0),0.00) ELSE ISNULL(I.monListPrice,0.00) END AS ' + ' ['+ @vcColumnName+']'
				END
				ELSE IF @vcLookBackTableName = 'Item' AND @vcDbColumnName = 'ItemType'
				BEGIN
					SET @strColumns = @strColumns + ',' + '(CASE 
																WHEN I.charItemType=''P'' 
																THEN 
																	CASE 
																		WHEN ISNULL(I.bitAssembly,0) = 1 THEN ''Assembly''
																		WHEN ISNULL(I.bitKitParent,0) = 1 THEN ''Kit''
																		WHEN ISNULL(I.bitAsset,0) = 1 AND ISNULL(I.bitRental,0)=0 THEN ''Asset''
																		WHEN ISNULL(I.bitAsset,0) = 1 AND ISNULL(I.bitRental,0)=1 THEN ''Rental Asset''
																		WHEN ISNULL(I.bitSerialized,0) = 1 AND ISNULL(I.bitAsset,0) = 0  THEN ''Serialized''
																		WHEN ISNULL(I.bitSerialized,0) = 1 AND ISNULL(I.bitAsset,0) = 1 AND ISNULL(I.bitRental,0)=0 THEN ''Serialized Asset''
																		WHEN ISNULL(I.bitSerialized,0) = 1 AND ISNULL(I.bitAsset,0) = 1 AND ISNULL(I.bitRental,0)=1 THEN ''Serialized Rental Asset''
																		WHEN ISNULL(I.bitLotNo,0)=1 THEN ''Lot #''
																		ELSE ''Inventory Item'' 
																	END
																WHEN I.charItemType=''N'' THEN ''Non Inventory Item'' 
																WHEN I.charItemType=''S'' THEN ''Service'' 
																WHEN I.charItemType=''A'' THEN ''Accessory'' 
															END)' + ' AS ' + ' [' + @vcColumnName + ']'  
				END
				ELSE IF @vcLookBackTableName = 'Item' AND @vcDbColumnName = 'monStockValue'
				BEGIN
					SET @strColumns = @strColumns+',WarehouseItems.monStockValue'+' ['+ @vcColumnName+']'                                                      
				END
				ELSE IF @vcLookBackTableName = 'Item' AND @vcDbColumnName = 'StockQtyCount'
				BEGIN
					SET @strColumns = @strColumns+',0'+' ['+ @vcColumnName+']'                                                      
				END
				ELSE IF @vcLookBackTableName = 'Item' AND @vcDbColumnName = 'StockQtyAdjust'
				BEGIN
					SET @strColumns = @strColumns+',0'+' ['+ @vcColumnName+']'     
					                                             
				END
				ELSE IF @vcLookBackTableName = 'WareHouseItems' AND @vcDbColumnName = 'vcLocation' AND ISNULL(@numFilteredWarehouseID,0)=0
				BEGIN
					SET @strColumns = @strColumns + ', (SELECT STUFF((SELECT CONCAT('', '', WIL.vcLocation) FROM WareHouseItems WI  LEFT JOIN WarehouseLocation AS WIL ON WI.numWLocationId=WIL.numWLocationId WHERE WI.numItemID=I.numItemCode FOR XML PATH('''')), 1, 1, ''''))' + ' AS ' + ' [' + @vcColumnName + '] '
				END
				ELSE IF @vcLookBackTableName = 'WareHouseItems' AND @vcDbColumnName = 'vcLocation' AND ISNULL(@numFilteredWarehouseID,0)>0
				BEGIN
					SET @strColumns = @strColumns + ', WFL.vcLocation AS ' + ' [' + @vcColumnName + '] '
				END
				ELSE IF @vcLookBackTableName = 'Warehouses' AND @vcDbColumnName = 'vcWareHouse'
				BEGIN
					SET @strColumns = @strColumns + ', (SELECT STUFF((SELECT CONCAT('', '', vcWareHouse) FROM WareHouseItems WI JOIN Warehouses W ON WI.numWareHouseID=W.numWareHouseID WHERE WI.numItemID=I.numItemCode FOR XML PATH('''')), 1, 1, ''''))' + ' AS ' + ' [' + @vcColumnName + ']'
				END
				ELSE IF @vcLookBackTableName = 'WorkOrder' AND @vcDbColumnName = 'WorkOrder'
				BEGIN
					SET @strColumns = @strColumns + ',(CASE WHEN (SELECT SUM(WO.numQtyItemsReq) from WorkOrder WO where WO.numItemCode=I.numItemCode and WO.numWOStatus=0) > 0 THEN 1 ELSE 0 END)' + ' AS ' + ' [' + @vcColumnName + ']'
				END
				ELSE IF @vcDbColumnName = 'vcPriceLevelDetail' 
				BEGIN
					SELECT @strColumns = @strColumns + CONCAT(', dbo.fn_GetItemPriceLevelDetail( ',@numDomainID,',I.numItemCode,',@numWarehouseID,')') +' ['+ @vcColumnName+']' 
				END
				ELSE
				BEGIN
					If @vcDbColumnName <> 'numItemCode' AND @vcDbColumnName <> 'vcPriceLevelDetail' --WE AE ALREADY ADDING COLUMN IN SELECT CLUASE DIRECTLY
					BEGIN
						SET @strColumns = @strColumns + ',' + (CASE @vcLookBackTableName WHEN 'Item' THEN 'I' WHEN 'CompanyInfo' THEN 'cmp' WHEN 'Vendor' THEN 'V' ELSE @vcLookBackTableName END) + '.' + @vcDbColumnName + ' AS ' + ' [' + @vcColumnName + ']' 
					END
				END
			END                              
			ELSE IF @bitCustom = 1 
			BEGIN
				
				SET @vcColumnName= @vcDbColumnName +'~'+ convert(varchar(10),@numFieldId)+'~1'

				SELECT 
					@vcFieldName = FLd_label,
					@vcAssociatedControlType = fld_type,
					@vcDbColumnName = 'Cust' + CONVERT(VARCHAR(10), Fld_Id),
					@Grp_ID = Grp_id
				FROM 
					CFW_Fld_Master
				WHERE 
					CFW_Fld_Master.Fld_Id = @numFieldId                 
        
				IF @Grp_ID = 9
				BEGIN
					SET @strColumns = @strColumns + ',L' + CONVERT(VARCHAR(3), @tintOrder) + '.vcData' + ' [' + @vcColumnName + ']'     
             
					SET @WhereCondition = @WhereCondition + ' left Join ItemAttributes CFW' + CONVERT(VARCHAR(3), @tintOrder)
									+ ' ON CFW' + CONVERT(VARCHAR(3), @tintOrder) + '.numDOmainID=' + CAST(@numDomainID AS VARCHAR) + ' AND CFW' + CONVERT(VARCHAR(3), @tintOrder) + '.Fld_Id='
									+ CONVERT(VARCHAR(10), @numFieldId)
									+ 'and CFW' + CONVERT(VARCHAR(3), @tintOrder)
									+ '.numItemCode=I.numItemCode   '  
					SET @WhereCondition = @WhereCondition
						+ ' left Join ListDetails L'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ ' on L'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ '.numListItemID=CFW'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ '.Fld_Value'      
				END
				ELSE IF @vcAssociatedControlType = 'TextBox' OR @vcAssociatedControlType = 'TextArea' 
				BEGIN
					SET @strColumns = @strColumns + ',CFW'  + CONVERT(VARCHAR(3), @tintOrder)  + '.Fld_Value  [' + @vcColumnName + ']'     
             
					SET @WhereCondition = @WhereCondition + ' left Join CFW_FLD_Values_Item CFW' + CONVERT(VARCHAR(3), @tintOrder)
									+ ' ON CFW' + CONVERT(VARCHAR(3), @tintOrder) + '.Fld_Id='
									+ CONVERT(VARCHAR(10), @numFieldId)
									+ 'and CFW' + CONVERT(VARCHAR(3), @tintOrder)
									+ '.RecId=I.numItemCode   '                                                         
				END   
				ELSE IF @vcAssociatedControlType = 'CheckBox' 
				BEGIN
					SET @strColumns = @strColumns
						+ ',case when isnull(CFW'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ '.Fld_Value,0)=0 then 0 when isnull(CFW'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ '.Fld_Value,0)=1 then 1 end   ['
						+  @vcColumnName
						+ ']'               
 
					SET @WhereCondition = @WhereCondition
						+ ' left Join CFW_FLD_Values_Item CFW'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ ' ON CFW' + CONVERT(VARCHAR(3), @tintOrder) + '.Fld_Id='
						+ CONVERT(VARCHAR(10), @numFieldId)
						+ ' and CFW'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ '.RecId=I.numItemCode   '                                                     
				END                
				ELSE IF @vcAssociatedControlType = 'DateField' 
				BEGIN
					SET @strColumns = @strColumns
						+ ',dbo.FormatedDateFromDate(CFW'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ '.Fld_Value,'
						+ CONVERT(VARCHAR(10), @numDomainId)
						+ ')  [' +@vcColumnName + ']'    
					                  
					SET @WhereCondition = @WhereCondition
						+ ' left Join CFW_FLD_Values_Item CFW'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ ' on CFW' + CONVERT(VARCHAR(3), @tintOrder) + '.Fld_Id='
						+ CONVERT(VARCHAR(10), @numFieldId)
						+ ' and CFW'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ '.RecId=I.numItemCode   '                                                         
				END                
				ELSE IF @vcAssociatedControlType = 'SelectBox' 
				BEGIN
					SET @vcDbColumnName = 'Cust' + CONVERT(VARCHAR(10), @numFieldId)

					SET @strColumns = @strColumns + ',L'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ '.vcData' + ' [' + @vcColumnName + ']'          
					                                                    
					SET @WhereCondition = @WhereCondition
						+ ' left Join CFW_FLD_Values_Item CFW'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ ' on CFW' + CONVERT(VARCHAR(3), @tintOrder) + '.Fld_Id='
						+ CONVERT(VARCHAR(10), @numFieldId)
						+ ' and CFW'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ '.RecId=I.numItemCode    '     
					                                                    
					SET @WhereCondition = @WhereCondition
						+ ' left Join ListDetails L'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ ' on L'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ '.numListItemID=CFW'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ '.Fld_Value'                
				END
				ELSE
				BEGIN
					SET @strColumns = @strColumns + CONCAT(',dbo.GetCustFldValueItem(',@numFieldId,',I.numItemCode)') + ' [' + @vcColumnName + ']'
				END                
			END        
  
			

			SELECT TOP 1
					@tintOrder = tintOrder + 1,
					@vcDbColumnName = vcDbColumnName,
					@vcFieldName = vcFieldName,
					@vcAssociatedControlType = vcAssociatedControlType,
					@vcListItemType = vcListItemType,
					@numListID = numListID,
					@vcLookBackTableName = vcLookBackTableName,
					@bitCustom = bitCustomField,
					@numFieldId = numFieldId,
					@bitAllowSorting = bitAllowSorting,
					@bitAllowEdit = bitAllowEdit,
					@ListRelID = ListRelID
			FROM    #tempForm
			WHERE   tintOrder > @tintOrder - 1 --AND bitCustomField=1
			ORDER BY tintOrder ASC            
 
			IF @@rowcount = 0 
				SET @tintOrder = 0 
		END  
		
		--DECLARE @vcApplyRackFilter as VARCHAR(MAX)=''
		--DECLARE @vcApplyRackFilterSearch as VARCHAR(MAX)=''
		--IF(ISNULL(@numFilteredWarehouseID,0)>0)
		--BEGIN
		--	SET @vcApplyRackFilter=' LEFT JOIN WareHouseItems AS WRF ON WRF.numItemId=I.numItemCode LEFT JOIN WarehouseLocation AS WFL ON WRF.numWLocationId=WFL.numWLocationId AND WRF.numWareHouseId=WFL.numWareHouseId '
		--	SET @vcApplyRackFilterSearch=@vcApplyRackFilterSearch + ' AND WRF.numWareHouseId='''+CAST(@numFilteredWarehouseID AS VARCHAR(500))+''' '
		--	SET @vcApplyRackFilterSearch=@vcApplyRackFilterSearch + ' AND WFL.numWareHouseId='''+CAST(@numFilteredWarehouseID AS VARCHAR(500))+''' '
		--	IF(ISNULL(@vcAsile,'')<>'')
		--	BEGIN
		--		SET @vcApplyRackFilterSearch=@vcApplyRackFilterSearch + ' AND WFL.vcAisle='''+CAST(@vcAsile AS VARCHAR(500))+''' '
		--	END
		--	IF(ISNULL(@vcRack,'')<>'')
		--	BEGIN
		--		SET @vcApplyRackFilterSearch=@vcApplyRackFilterSearch + ' AND WFL.vcRack='''+CAST(@vcRack AS VARCHAR(500))+''' '
		--	END
		--	IF(ISNULL(@vcShelf,'')<>'')
		--	BEGIN
		--		SET @vcApplyRackFilterSearch=@vcApplyRackFilterSearch + ' AND WFL.vcShelf='''+CAST(@vcShelf AS VARCHAR(500))+''' '
		--	END
		--	IF(ISNULL(@vcBin,'')<>'')
		--	BEGIN
		--		SET @vcApplyRackFilterSearch=@vcApplyRackFilterSearch + ' AND WFL.vcBin='''+CAST(@vcBin AS VARCHAR(500))+''' '
		--	END
		--END
		
		DECLARE @strSQL AS VARCHAR(MAX)
		SET @strSQL = CONCAT(' FROM 
							Item I
						LEFT JOIN
							Vendor V
						ON
							I.numItemCode = V.numItemCode
							AND I.numVendorID = V.numVendorID
						LEFT JOIN
							DivisionMaster
						ON
							V.numVendorID = DivisionMaster.numDivisionID
						LEFT JOIN
							CompanyInfo cmp
						ON
							DivisionMaster.numCompanyId = cmp.numCompanyId
						LEFT JOIN
							CustomerPartNumber
						ON
							CustomerPartNumber.numItemCode = I.numItemCode AND CustomerPartNumber.numCompanyId = 0
						',(CASE 
								WHEN ISNULL(@numFilteredWarehouseID,0) > 0 
								THEN CONCAT(' CROSS APPLY (
											SELECT
												WareHouseItems.numWareHouseItemID,
												numOnHand AS numOnHand,
												ISNULL(numOnHand,0) + ISNULL(numAllocation,0) AS numAvailable,
												numBackOrder AS numBackOrder,
												numOnOrder AS numOnOrder,
												numAllocation AS numAllocation,
												numReorder AS numReorder,
												numOnHand * (CASE WHEN ISNULL(I.bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(I.monAverageCost,0) END) AS monStockValue,
												WarehouseLocation.vcLocation
											FROM
												WareHouseItems
											LEFT JOIN 
												WarehouseLocation 
											ON 
												WareHouseItems.numWLocationId=WarehouseLocation.numWLocationId
											WHERE
												numItemID = I.numItemCode
												AND (WareHouseItems.numWareHouseID=',ISNULL(@numFilteredWarehouseID,0),' OR ',ISNULL(@numFilteredWarehouseID,0),'=0)'
												,CASE WHEN ISNULL(@vcAsile,'') <> '' THEN CONCAT(' AND WarehouseLocation.vcAisle=''',@vcAsile+'''') ELSE '' END  
												,CASE WHEN ISNULL(@vcRack,'') <> '' THEN CONCAT(' AND WarehouseLocation.vcRack=''',@vcRack +'''') ELSE '' END  
												,CASE WHEN ISNULL(@vcShelf,'') <> '' THEN CONCAT(' AND WarehouseLocation.vcShelf=''',@vcShelf+'''') ELSE '' END  
												,CASE WHEN ISNULL(@vcBin,'') <> '' THEN CONCAT(' AND WarehouseLocation.vcBin=''',@vcBin+'''') ELSE '' END  
										,') WarehouseItems ')
								ELSE ' OUTER APPLY (
							SELECT
								SUM(numOnHand) AS numOnHand,
								SUM(ISNULL(numOnHand,0) + ISNULL(numAllocation,0)) AS numAvailable,
								SUM(numBackOrder) AS numBackOrder,
								SUM(numOnOrder) AS numOnOrder,
								SUM(numAllocation) AS numAllocation,
								SUM(numReorder) AS numReorder,
								SUM(numOnHand) * (CASE WHEN ISNULL(I.bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(I.monAverageCost,0) END) AS monStockValue
							FROM
								WareHouseItems
							WHERE
								numItemID = I.numItemCode) WarehouseItems ' 
							END),'
						  ', @WhereCondition)

		IF @columnName = 'Backorder' OR @columnName = 'WHI.numBackOrder'
			SET @columnName = 'numBackOrder'
		ELSE IF @columnName = 'OnOrder' OR @columnName = 'WHI.numOnOrder' 
			SET @columnName = 'numOnOrder'
		ELSE IF @columnName = 'OnAllocation' OR @columnName = 'WHI.numAllocation' 
			SET @columnName = 'numAllocation'
		ELSE IF @columnName = 'Reorder' OR @columnName = 'WHI.numReorder' 
			SET @columnName = 'numReorder'
		ELSE IF @columnName = 'WHI.numOnHand'
			SET @columnName = 'numOnHand'
		ELSE IF @columnName = 'WHI.numAvailable'
			SET @columnName = 'numAvailable'
		ELSE IF @columnName = 'monStockValue' 
			SET @columnName = 'sum(numOnHand) * (CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(monAverageCost,0) END)'
		ELSE IF @columnName = 'ItemType' 
			SET @columnName = 'charItemType'
		ELSE IF @columnName = 'numItemCode' 
			SET @columnName = 'I.numItemCode'

		DECLARE @column AS VARCHAR(50)              
		SET @column = @columnName
	         
		IF @columnName LIKE '%Cust%' 
		BEGIN
			DECLARE @CustomFieldType AS VARCHAR(10)
			DECLARE @fldId AS VARCHAR(10)
			
			IF CHARINDEX('CFW.Cust',@columnName) > 0
			BEGIN
				SET @fldId = REPLACE(@columnName, 'CFW.Cust', '')
			END
			ELSE
			BEGIN
				SET @fldId = REPLACE(@columnName, 'Cust', '')
			END
			SELECT TOP 1 @CustomFieldType = ISNULL(vcAssociatedControlType,'') FROM View_DynamicCustomColumns WHERE numFieldID = @fldId
            
			IF ISNULL(@CustomFieldType,'') = 'SelectBox' OR ISNULL(@CustomFieldType,'') = 'ListBox'
			BEGIN
				SET @strSQL = @strSQL + ' left Join CFW_FLD_Values_Item CFW on CFW.RecId=i.numItemCode and CFW.fld_id= ' + @fldId + ' ' 
				SET @strSQL = @strSQL + ' left Join ListDetails LstCF on LstCF.numListItemID = CFW.Fld_Value  '            
				SET @columnName = ' LstCF.vcData ' 	
			END
			ELSE
			BEGIN
				SET @strSQL =  @strSQL + ' left Join CFW_FLD_Values_Item CFW on CFW.RecId=i.numItemCode and CFW.fld_id= ' + @fldId + ' '                                                         
				SET @columnName = 'CFW.Fld_Value'                	
			END
		END                                      
		DECLARE @strWhere AS VARCHAR(8000)
		SET @strWhere = CONCAT(' WHERE I.numDomainID = ',@numDomainID)
    
		IF @ItemType = 'P'
		BEGIN
			SET @strWhere = @strWhere + ' AND I.charItemType= ''P''' 
		END
		ELSE IF @ItemType = 'S'
		BEGIN
			SET @strWhere = @strWhere + ' AND I.charItemType= ''S'''
		END
		ELSE IF @ItemType = 'N'    
		BEGIN
			SET @strWhere = @strWhere + ' AND I.charItemType= ''N'''
		END
	    
		IF @bitContainer= '1'
		BEGIN
			SET @strWhere = @strWhere + ' AND I.bitContainer= ' + CONVERT(VARCHAR(2), @bitContainer) + ''  
		END

		IF @bitAssembly = '1' 
		BEGIN
			SET @strWhere = @strWhere + ' AND I.bitAssembly	= ' + CONVERT(VARCHAR(2), @bitAssembly) + ''  
		END 
		ELSE IF @ItemType = 'P'
		BEGIN
			SET @strWhere = @strWhere + ' AND ISNULL(I.bitAssembly,0)=0 '
		END

		IF @IsKit = '1' 
		BEGIN
			SET @strWhere = @strWhere + ' AND ISNULL(I.bitAssembly,0)=0 AND I.bitKitParent= ' + CONVERT(VARCHAR(2), @IsKit) + ''  
		END           
		ELSE IF @ItemType = 'P'
		BEGIN
			SET @strWhere = @strWhere + ' AND ISNULL(I.bitKitParent,0)=0 '
		END

		IF @bitSerialized = 1 
		BEGIN
			SET @strWhere = @strWhere + ' AND (I.bitSerialized=1 OR I.bitLotNo=1)'
		END
		--ELSE IF @ItemType = 'P'
		--BEGIN
		--	SET @strWhere = @strWhere + ' AND ISNULL(Item.bitSerialized,0)=0 AND ISNULL(Item.bitLotNo,0)=0 '
		--END

		IF @bitAsset = 1 
		BEGIN
			SET @strWhere = @strWhere + ' AND ISNULL(I.bitAsset,0) = 1'
		END
		ELSE IF @ItemType = 'P'
		BEGIN
			SET @strWhere = @strWhere + ' AND ISNULL(I.bitAsset,0) = 0 '
		END

		IF @bitRental = 1
		BEGIN
			SET @strWhere = @strWhere + ' AND ISNULL(I.bitRental,0) = 1 AND ISNULL(I.bitAsset,0) = 1 '
		END
		ELSE IF @ItemType = 'P'
		BEGIN
			SET @strWhere = @strWhere + ' AND ISNULL(I.bitRental,0) = 0 '
		END

		IF @SortChar <> '0' 
		BEGIN
			SET @strWhere = @strWhere + ' AND I.vcItemName like ''' + @SortChar + '%'''                                       
		END

		IF @ItemClassification <> '0' 
		BEGIN
			SET @strWhere = @strWhere + ' AND I.numItemClassification=' + CONVERT(VARCHAR(15), @ItemClassification)    
		END

		IF @IsArchive = 0 
		BEGIN
			SET @strWhere = @strWhere + ' AND ISNULL(I.IsArchieve,0) = 0'       
		END
        
		IF @numItemGroup > 0 
		BEGIN
			SET @strWhere = @strWhere + ' AND I.numItemGroup = ' + CONVERT(VARCHAR(20), @numItemGroup)  
        END  
		
		IF @numItemGroup = -1 
		BEGIN
			SET @strWhere = @strWhere + ' AND I.numItemGroup in (select numItemGroupID from ItemGroups where numDomainID=' + CONVERT(VARCHAR(20), @numDomainID) + ')'                                      
        END

		IF @bitMultiSelect = '1'
		BEGIN
			SET  @strWhere = @strWhere + 'AND ISNULL(I.bitKitParent,0)=0 '
		END

		IF @vcRegularSearchCriteria<>'' 
		BEGIN
			SET @strWhere = @strWhere +' AND ' + @vcRegularSearchCriteria 
		END
	
		IF @vcCustomSearchCriteria<>'' 
		BEGIN 
		--comment
			SET @strWhere = @strWhere + ' AND ' + @vcCustomSearchCriteria  		END
		
		IF LEN(@SearchQuery) > 0
		BEGIN
			SET @strWhere = @strWhere + ' AND (' + @SearchQuery + ') '
		END
		
		SET @strWhere = @strWhere + ISNULL(@Where,'')

		IF ISNULL(@tintDashboardReminderType,0) = 17
		BEGIN
			SET @strWhere = CONCAT(@strWhere,' AND I.numItemCode IN (','SELECT DISTINCT
																		numItemID
																	FROM
																		WareHouseItems WIInner
																	INNER JOIN
																		Item IInner
																	ON
																		WIInner.numItemID = IInner.numItemCode
																	WHERE
																		WIInner.numDomainID=',@numDomainID,'
																		AND IInner.numDomainID = ',@numDomainID,'
																		AND ISNULL(IInner.bitArchiveItem,0) = 0
																		AND ISNULL(IInner.IsArchieve,0) = 0
																		AND ISNULL(WIInner.numReorder,0) > 0 
																		AND ISNULL(WIInner.numOnHand,0) < ISNULL(WIInner.numReorder,0)',') ')

		END
  
		DECLARE @firstRec AS INTEGER                                        
		DECLARE @lastRec AS INTEGER                                        
		SET @firstRec = ( @CurrentPage - 1 ) * @PageSize                                        
		SET @lastRec = ( @CurrentPage * @PageSize + 1 )

		-- EXECUTE FINAL QUERY
		DECLARE @strFinal AS NVARCHAR(MAX)=''
		IF ISNULL(@numFilteredWarehouseID,0)>0
		BEGIN
			SET @strFinal = CONCAT(@strFinal,'SELECT ROW_NUMBER() OVER(ORDER BY ',@columnName,' ',@columnSortOrder,',WarehouseItems.vcLocation asc) RowID, ',@strColumns,' INTO #temp2',@strSql,@strWhere,' ;')
			SET @strFinal = @strFinal + ' SELECT @TotalRecords = COUNT(*) FROM #temp2; '
			SET @strFinal = @strFinal + ' SELECT * FROM #temp2 WHERE RowID > '+ CAST(@firstRec AS VARCHAR(100)) +' and RowID <'+CAST(@lastRec AS VARCHAR(100))+';DROP TABLE #temp2; '
		END
		ELSE IF @CurrentPage = -1 AND @PageSize = -1
		BEGIN
			SET @strFinal = CONCAT('SELECT I.numItemCode, I.vcItemName, I.vcModelID INTO #tempTable ',@strSql  , @strWhere ,'; SELECT * FROM #tempTable; SELECT @TotalRecords = COUNT(*) FROM #tempTable; DROP TABLE #tempTable;')
		END
		ELSE
		BEGIN
			SET @strFinal = CONCAT('SELECT ROW_NUMBER() OVER(ORDER BY ',@columnName,' ',@columnSortOrder,') RowID,I.numItemCode INTO #temp2 ',@strSql  , @strWhere,'; SELECT RowID, ',@strColumns,' INTO #tempTable ',@strSql, ' JOIN #temp2 tblAllItems ON I.numItemCode = tblAllItems.numItemCode ',@strWhere,' AND tblAllItems.RowID > ',@firstRec,' and tblAllItems.RowID <',@lastRec,'; SELECT  * FROM  #tempTable ORDER BY RowID; DROP TABLE #tempTable; SELECT @TotalRecords = COUNT(*) FROM #temp2; DROP TABLE #temp2;')
		END

		
		PRINT @strFInal
		exec sp_executesql @strFinal, N'@TotalRecords INT OUT', @TotRecs OUT

	END

	UPDATE  
		#tempForm
    SET 
		intColumnWidth = (CASE WHEN ISNULL(intColumnWidth,0) <= 0 THEN 25 ELSE intColumnWidth END)
                                  
	SELECT * FROM #tempForm
    DROP TABLE #tempForm
END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_MassPurchaseFulfillment_GetRecordsForPutAway')
DROP PROCEDURE dbo.USP_MassPurchaseFulfillment_GetRecordsForPutAway
GO

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_MassPurchaseFulfillment_GetRecordsForPutAway]
(
	@numDomainID NUMERIC(18,0)
	,@numUserCntID NUMERIC(18,0)
	,@tintMode TINYINT
	,@vcSelectedRecords VARCHAR(MAX)
)
AS
BEGIN
	DECLARE @numDomainDivisionID NUMERIC(18,0)
	SELECT @numDomainDivisionID=ISNULL(@numDomainDivisionID,0) FROM Domain WHERE numDomainId=@numDomainID

	DECLARE @TempFieldsRight TABLE
	(
		ID VARCHAR(50)
		,numFieldID NUMERIC(18,0)
		,vcFieldName VARCHAR(100)
		,vcOrigDbColumnName VARCHAR(100)
		,bitAllowSorting BIT
		,bitAllowFiltering BIT
		,vcAssociatedControlType VARCHAR(20)
		,bitCustomField BIT
		,intRowNum INT
		,intColumnWidth FLOAT
	)

	INSERT INTO @TempFieldsRight
	(
		ID
		,numFieldID
		,vcFieldName
		,vcOrigDbColumnName
		,bitAllowSorting
		,bitAllowFiltering
		,vcAssociatedControlType
		,bitCustomField
		,intRowNum
		,intColumnWidth
	)
	SELECT
		CONCAT(143,'~',DFM.numFieldID,'~',0)
		,DFM.numFieldID
		,DFFM.vcFieldName
		,DFM.vcOrigDbColumnName
		,DFFM.bitAllowSorting
		,DFFM.bitAllowFiltering
		,DFM.vcAssociatedControlType
		,0
		,DFCD.intRowNum
		,ISNULL(DFCD.intColumnWidth,0)
	FROM
		DycFormConfigurationDetails DFCD
	INNER JOIN
		DycFormField_Mapping DFFM
	ON
		DFCD.numFormId = DFFM.numFormID
		AND DFCD.numFieldId = DFFM.numFieldID
	INNER JOIN
		DycFieldMaster DFM
	ON
		DFFM.numFieldID = DFM.numFieldId
	WHERE 
		DFCD.numDomainId=@numDomainID 
		AND DFCD.numUserCntID=@numUserCntID 
		AND DFCD.numFormId=143 
		AND ISNULL(DFFM.bitDeleted,0) = 0
		AND ISNULL(DFFM.bitSettingField,0) = 1
		AND ISNULL(DFCD.bitCustom,0) = 0

	IF (SELECT COUNT(*) FROM @TempFieldsRight) = 0
	BEGIN
		INSERT INTO @TempFieldsRight
		(
			ID
			,numFieldID
			,vcFieldName
			,vcOrigDbColumnName
			,bitAllowSorting
			,bitAllowFiltering
			,vcAssociatedControlType
			,bitCustomField
			,intRowNum
			,intColumnWidth
		)
		SELECT
			CONCAT(143,'~',DFM.numFieldID,'~',0)
			,DFM.numFieldID
			,DFFM.vcFieldName
			,DFM.vcOrigDbColumnName
			,DFFM.bitAllowSorting
			,DFFM.bitAllowFiltering
			,DFM.vcAssociatedControlType
			,0
			,DFFM.[order]
			,100
		FROM
			DycFormField_Mapping DFFM
		INNER JOIN
			DycFieldMaster DFM
		ON
			DFFM.numFieldID = DFM.numFieldId
		WHERE 
			DFFM.numFormId=143 
			AND ISNULL(DFFM.bitDeleted,0) = 0
			AND ISNULL(DFFM.bitSettingField,0) = 1
			AND (ISNULL(DFFM.bitDefault,0) = 1 OR ISNULL(DFFM.bitRequired,0) = 1)
	END
	ELSE
	BEGIN
		INSERT INTO @TempFieldsRight
		(
			ID
			,numFieldID
			,vcFieldName
			,vcOrigDbColumnName
			,bitAllowSorting
			,bitAllowFiltering
			,vcAssociatedControlType
			,bitCustomField
			,intRowNum
			,intColumnWidth
		)
		SELECT
			CONCAT(143,'~',DFM.numFieldID,'~',0)
			,DFM.numFieldID
			,DFFM.vcFieldName
			,DFM.vcOrigDbColumnName
			,DFFM.bitAllowSorting
			,DFFM.bitAllowFiltering
			,DFM.vcAssociatedControlType
			,0
			,DFFM.[order]
			,100
		FROM
			DycFormField_Mapping DFFM
		INNER JOIN
			DycFieldMaster DFM
		ON
			DFFM.numFieldID = DFM.numFieldId
		WHERE 
			DFFM.numFormId=143 
			AND ISNULL(DFFM.bitDeleted,0) = 0
			AND ISNULL(DFFM.bitSettingField,0) = 1
			AND ISNULL(DFFM.bitRequired,0) = 1
			AND DFM.numFieldId NOT IN (SELECT T1.numFieldID FROM @TempFieldsRight T1)
	END

	DECLARE @TEMP TABLE
	(
		ID INT IDENTITY(1,1)
		,numOppID NUMERIC(18,0)
		,numWOID NUMERIC(18,0)
		,numOppItemID NUMERIC(18,0)
	)

	DECLARE @hDocItem int
	EXEC sp_xml_preparedocument @hDocItem OUTPUT, @vcSelectedRecords

	INSERT INTO @TEMP
	(
		numOppID
		,numWOID
		,numOppItemID
	)
	SELECT
		ISNULL(OppID,0)
		,ISNULL(WOID,0)
		,ISNULL(OppItemID,0)
	FROM
		OPENXML (@hDocItem,'/NewDataSet/Table1',2)
	WITH
	(
		OppID NUMERIC(18,0)
		,WOID NUMERIC(18,0)
		,OppItemID NUMERIC(18,0)
	)
	
	EXEC sp_xml_removedocument @hDocItem

	DECLARE @TEMPItems TABLE
	(	
		ID NUMERIC(18,0)
		,numSortOrder INT
		,numOppID NUMERIC(18,0)
		,numWOID NUMERIC(18,0)
		,bitStockTransfer BIT
		,numDivisionID NUMERIC(18,0)
		,numOppItemID NUMERIC(18,0)
		,numItemCode NUMERIC(18,0)
		,numWarehouseItemID NUMERIC(18,0)
		,vcPoppName VARCHAR(300)
		,numBarCodeId VARCHAR(300)
		,vcItemName VARCHAR(300)
		,vcItemDesc VARCHAR(MAX)
		,vcSKU VARCHAR(300)
		,vcPathForTImage VARCHAR(MAX)
		,vcAttributes VARCHAR(MAX)
		,numRemainingQty FLOAT
		,numQtyToShipReceive FLOAT
		,bitSerialized BIT
		,bitLotNo BIT
		,bitPPVariance BIT
		,numCurrencyID NUMERIC(18,0)
		,fltExchangeRate FLOAT
		,charItemType VARCHAR(3)
		,vcItemType VARCHAR(50)
		,monPrice DECIMAL(20,5)
		,bitDropShip BIT
		,numProjectID NUMERIC(18,0)
		,numClassID NUMERIC(18,0)
		,numAssetChartAcntId NUMERIC(18,0)
		,numCOGsChartAcntId NUMERIC(18,0)
		,numIncomeChartAcntId NUMERIC(18,0)
		,SerialLotNo VARCHAR(MAX)
		,vcWarehouses VARCHAR(MAX)
	)

	INSERT INTO @TEMPItems
	(	
		ID
		,numSortOrder
		,numOppID
		,numWOID
		,bitStockTransfer
		,numDivisionID
		,numOppItemID 
		,numItemCode
		,numWarehouseItemID
		,vcPoppName
		,numBarCodeId
		,vcItemName
		,vcItemDesc
		,vcSKU
		,vcPathForTImage
		,vcAttributes 
		,numRemainingQty
		,numQtyToShipReceive
		,bitSerialized
		,bitLotNo
		,bitPPVariance
		,numCurrencyID
		,fltExchangeRate
		,charItemType
		,vcItemType
		,monPrice
		,bitDropShip
		,numProjectID
		,numClassID
		,numAssetChartAcntId
		,numCOGsChartAcntId
		,numIncomeChartAcntId
		,SerialLotNo
		,vcWarehouses
	)
	SELECT 
		T1.ID
		,ISNULL(OpportunityItems.numSortOrder,0)
		,OpportunityMaster.numOppID
		,WorkOrder.numWOId
		,ISNULL(OpportunityMaster.bitStockTransfer,0)
		,(CASE WHEN WorkOrder.numWOId IS NOT NULL THEN @numDomainDivisionID ELSE OpportunityMaster.numDivisionId END)
		,OpportunityItems.numoppitemtCode
		,Item.numItemCode
		,WareHouseItems.numWareHouseItemID
		,(CASE WHEN WorkOrder.numWOId IS NOT NULL THEN ISNULL(WorkOrder.vcWorkOrderName,'-') ELSE OpportunityMaster.vcPoppName END)
		,Item.numBarCodeId
		,Item.vcItemName
		,Item.txtItemDesc AS vcItemDesc
		,Item.vcSKU
		,ISNULL(ItemImages.vcPathForTImage,'') vcPathForTImage
		,ISNULL(OpportunityItems.vcAttributes,'') AS vcAttributes
		,(CASE 
				WHEN @tintMode=1 THEN ISNULL((CASE WHEN WorkOrder.numWOId IS NOT NULL THEN WorkOrder.numQtyBuilt ELSE OpportunityItems.numUnitHour END),0) - ISNULL((CASE WHEN WorkOrder.numWOId IS NOT NULL THEN WorkOrder.numUnitHourReceived ELSE OpportunityItems.numUnitHourReceived END),0)
				WHEN @tintMode=3 THEN ISNULL((CASE WHEN WorkOrder.numWOId IS NOT NULL THEN WorkOrder.numQtyReceived ELSE OpportunityItems.numQtyReceived END),0) - ISNULL((CASE WHEN WorkOrder.numWOId IS NOT NULL THEN WorkOrder.numUnitHourReceived ELSE OpportunityItems.numUnitHourReceived END),0)
				ELSE 0
			END) numRemainingQty
		,0 numQtyToShipReceive
		,ISNULL(bitSerialized,0)
		,ISNULL(bitLotNo,0)
		,ISNULL(OpportunityMaster.bitPPVariance,0)
		,ISNULL(OpportunityMaster.numCurrencyID,0)
		,ISNULL(OpportunityMaster.fltExchangeRate,0)
		,Item.charItemType
		,OpportunityItems.vcType
		,ISNULL(OpportunityItems.monPrice,0)
		,ISNULL(OpportunityItems.bitDropShip,0)
		,ISNULL(OpportunityItems.numProjectID,0)
		,ISNULL(OpportunityItems.numClassID,0)
		,ISNULL(Item.numAssetChartAcntId,0)
		,ISNULL(Item.numCOGsChartAcntId,0)
		,ISNULL(Item.numIncomeChartAcntId,0)
		,(CASE 
			WHEN WorkOrder.numWOId IS NOT NULL THEN '' 
			ELSE SUBSTRING((SELECT 
								CONCAT(',',vcSerialNo,(CASE WHEN isnull(Item.bitLotNo,0)=1 THEN ' (' + CONVERT(VARCHAR(15),whi.numQty) + ')' ELSE '' END))
							FROM 
								OppWarehouseSerializedItem oppI 
							JOIN 
								WareHouseItmsDTL whi
							ON 
								oppI.numWareHouseItmsDTLID=whi.numWareHouseItmsDTLID 
							WHERE 
								oppI.numOppID=OpportunityItems.numOppId 
								and oppI.numOppItemID=OpportunityItems.numoppitemtCode 
							ORDER BY vcSerialNo FOR XML PATH('')),2,200000) END) 
		,(CASE 
			WHEN EXISTS (SELECT numWareHouseItemID FROM WareHouseItems WIInner INNER JOIN WarehouseLocation WL ON WIInner.numWLocationID=WL.numWLocationID WHERE WIInner.numDomainID=@numDomainID AND WIInner.numItemID=Item.numItemCode AND WIInner.numWareHouseID = WareHouseItems.numWareHouseID)
			THEN CONCAT('[',STUFF((SELECT 
										CONCAT(', {"WarehouseItemID":',WIInner.numWareHouseItemID,', "Location":"',ISNULL(WL.vcLocation,''),'"}') 
									FROM 
										WareHouseItems WIInner
									INNER JOIN
										WarehouseLocation WL
									ON
										WIInner.numWLocationID = WL.numWLocationID
									WHERE 
										WIInner.numDomainID=@numDomainID 
										AND WIInner.numItemID=Item.numItemCode 
										AND WIInner.numWareHouseID = WareHouseItems.numWareHouseID
									ORDER BY
										WL.vcLocation ASC
									FOR XML PATH('')),1,1,''),']')
			ELSE CONCAT('[',STUFF((SELECT 
								CONCAT(', {"WarehouseItemID":',WIInner.numWareHouseItemID,', "Location":"',ISNULL(WL.vcLocation,''),'"}') 
							FROM 
								WareHouseItems WIInner
							LEFT JOIN
								WarehouseLocation WL
							ON
								WIInner.numWLocationID = WL.numWLocationID
							WHERE 
								WIInner.numDomainID=@numDomainID 
								AND WIInner.numItemID=Item.numItemCode 
								AND WIInner.numWareHouseItemID=WareHouseItems.numWareHouseItemID
							ORDER BY
								WL.vcLocation ASC
							FOR XML PATH('')),1,1,''),']')
		END) 
	FROM
		@TEMP T1
	LEFT JOIN
		WorkOrder
	ON
		T1.numWOID = WorkOrder.numWOId
	LEFT JOIN
		OpportunityMaster
	ON
		OpportunityMaster.numOppID= T1.numOppID
	LEFT JOIN
		OpportunityItems
	ON
		OpportunityMaster.numOppID = OpportunityItems.numOppID
		AND (OpportunityItems.numOppItemtcode = T1.numOppItemID OR ISNULL(T1.numOppItemID,0) = 0)
	LEFT JOIN
		WareHouseItems 
	ON
		WareHouseItems.numWareHouseItemID = (CASE 
												WHEN WorkOrder.numWOId IS NOT NULL 
												THEN WorkOrder.numWareHouseItemId 
												ELSE (CASE WHEN ISNULL(OpportunityMaster.bitStockTransfer,0) = 1 THEN OpportunityItems.numToWarehouseItemID ELSE OpportunityItems.numWarehouseItmsID END) 
											END)
	INNER JOIN
		Item 
	ON
		(CASE WHEN WorkOrder.numWOId IS NOT NULL THEN WorkOrder.numItemCode ELSE OpportunityItems.numItemCode END) = Item.numItemCode
	LEFT JOIN
		ItemImages
	ON
		ItemImages.numDomainId=@numDomainID
		AND ItemImages.numItemCode = Item.numItemCode
		AND ItemImages.bitDefault=1 
		AND ItemImages.bitIsImage=1
	WHERE
		1 = (CASE 
				WHEN WorkOrder.numWOId IS NOT NULL 
				THEN (CASE WHEN WorkOrder.numDomainID=@numDomainID THEN 1 ELSE 0 END) 
				ELSE (CASE WHEN OpportunityMaster.numDomainID=@numDomainID THEN 1 ELSE 0 END)
			END)
		AND 1 = (CASE 
					WHEN WorkOrder.numWOId IS NOT NULL 
					THEN 1
					ELSE (CASE WHEN ISNULL(OpportunityMaster.tintshipped,0) = 0 THEN 1 ELSE 0 END)
				END)
		AND (WorkOrder.numWOId IS NOT NULL OR (OpportunityMaster.numOppId IS NOT NULL AND OpportunityItems.numoppitemtCode IS NOT NULL))
		AND (CASE 
				WHEN @tintMode=1 THEN ISNULL((CASE WHEN WorkOrder.numWOId IS NOT NULL THEN WorkOrder.numQtyBuilt ELSE OpportunityItems.numUnitHour END),0) - ISNULL((CASE WHEN WorkOrder.numWOId IS NOT NULL THEN WorkOrder.numUnitHourReceived ELSE OpportunityItems.numUnitHourReceived END),0)
				WHEN @tintMode=3 THEN ISNULL((CASE WHEN WorkOrder.numWOId IS NOT NULL THEN WorkOrder.numQtyReceived ELSE OpportunityItems.numQtyReceived END),0) - ISNULL((CASE WHEN WorkOrder.numWOId IS NOT NULL THEN WorkOrder.numUnitHourReceived ELSE OpportunityItems.numUnitHourReceived END),0)
				ELSE 0
			END) > 0
	ORDER BY
		T1.ID, OpportunityItems.numSortOrder

	SELECT * FROM @TEMPItems ORDER BY ID	
	SELECT * FROM @TempFieldsRight ORDER BY intRowNum
END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_MassSalesFulfillment_GetRecords')
DROP PROCEDURE dbo.USP_MassSalesFulfillment_GetRecords
GO

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_MassSalesFulfillment_GetRecords]
(
	@numDomainID NUMERIC(18,0)
	,@numUserCntID NUMERIC(18,0)
	,@ClientTimeZoneOffset INT
	,@numViewID NUMERIC(18,0)
	,@numWarehouseID NUMERIC(18,0)
	,@bitGroupByOrder BIT
	,@bitRemoveBO BIT
	,@numShippingZone NUMERIC(18,0)
	,@bitIncludeSearch BIT
	,@vcOrderStauts VARCHAR(MAX)
	,@tintFilterBy TINYINT
	,@vcFilterValue VARCHAR(MAX)
	,@vcCustomSearchValue VARCHAR(MAX)
	,@numBatchID NUMERIC(18,0)
	,@tintPackingViewMode TINYINT
	,@tintPrintBizDocViewMode TINYINT
	,@tintPendingCloseFilter TINYINT
	,@numPageIndex INT
	,@vcSortColumn VARCHAR(100) OUTPUT
	,@vcSortOrder VARCHAR(4) OUTPUT
	,@numTotalRecords NUMERIC(18,0) OUTPUT
)
AS 
BEGIN
	IF @numViewID = 4 OR @numViewID = 5 OR @numViewID = 6
	BEGIN
		SET @bitGroupByOrder = 1
	END

	SET @vcCustomSearchValue = REPLACE(@vcCustomSearchValue,'OpportunityMaster.dtItemReceivedDate','(SELECT MAX(OMInner.dtItemReceivedDate) FROM OpportunityMaster OMInner INNER JOIN OpportunityItems OIInner ON OMInner.numOppID=OIInner.numOppID WHERE OMInner.tintOppType=2 AND OMInner.tintOppStatus=1 AND OIInner.numItemCode=OpportunityItems.numItemCode)')

	If @numViewID = 6 AND CHARINDEX('OpportunityMaster.bintCreatedDate',@vcCustomSearchValue) > 0
	BEGIN
		SET @vcCustomSearchValue = REPLACE(@vcCustomSearchValue,'OpportunityMaster.bintCreatedDate','DATEADD(MINUTE,@ClientTimeZoneOffset * -1,OpportunityBizDocs.dtCreatedDate)')
	END
	ELSE IF CHARINDEX('OpportunityMaster.bintCreatedDate',@vcCustomSearchValue) > 0
	BEGIN
		SET @vcCustomSearchValue = REPLACE(@vcCustomSearchValue,'OpportunityMaster.bintCreatedDate','DATEADD(MINUTE,@ClientTimeZoneOffset * -1,OpportunityMaster.bintCreatedDate)')
	END

	DECLARE @TempFieldsLeft TABLE
	(
		numPKID INT IDENTITY(1,1)
		,ID VARCHAR(50)
		,numFieldID NUMERIC(18,0)
		,vcFieldName VARCHAR(100)
		,vcOrigDbColumnName VARCHAR(100)
		,vcListItemType VARCHAR(10)
		,numListID NUMERIC(18,0)
		,bitAllowSorting BIT
		,bitAllowFiltering BIT
		,vcAssociatedControlType VARCHAR(20)
		,vcLookBackTableName VARCHAR(100)
		,bitCustomField BIT
		,intRowNum INT
		,intColumnWidth FLOAT
		,bitIsRequired BIT
		,bitIsEmail BIT
		,bitIsAlphaNumeric BIT
		,bitIsNumeric BIT
		,bitIsLengthValidation BIT
		,intMaxLength INT
		,intMinLength INT
		,bitFieldMessage BIT
		,vcFieldMessage VARCHAR(MAX)
		,Grp_id INT
	)

	CREATE TABLE #TEMPResult
	(
		numOppID NUMERIC(18,0)
		,numDivisionID NUMERIC(18,0)
		,numTerID NUMERIC(18,0)
		,numOppItemID NUMERIC(18,0)
		,numOppBizDocID NUMERIC(18,0)
		,bintCreatedDate VARCHAR(50)
		,numItemCode NUMERIC(18,0)
		,vcCompanyName VARCHAR(300)
		,vcPoppName VARCHAR(300)
		,numAssignedTo VARCHAR(100)
		,numRecOwner VARCHAR(100)
		,vcBizDocID VARCHAR(300)
		,numStatus VARCHAR(300)
		,txtComments VARCHAR(MAX)
		,numBarCodeId VARCHAR(50)
		,vcLocation VARCHAR(100)
		,vcItemName VARCHAR(300)
		,numItemGroup VARCHAR(300)
		,vcItemDesc VARCHAR(2000)
		,numQtyShipped FLOAT
		,numUnitHour FLOAT
		,monAmountPaid DECIMAL(20,5)
		,monAmountToPay DECIMAL(20,5)
		,numItemClassification VARCHAR(100)
		,vcSKU VARCHAR(50)
		,charItemType VARCHAR(50)
		,vcAttributes VARCHAR(300)
		,vcNotes VARCHAR(MAX)
		,vcItemReleaseDate VARCHAR(50)
		,dtItemReceivedDate VARCHAR(50)
		,intUsedShippingCompany VARCHAR(300)
		,intShippingCompany VARCHAR(300)
		,numPreferredShipVia NUMERIC(18,0)
		,numPreferredShipService NUMERIC(18,0)
		,vcInvoiced FLOAT
		,vcInclusionDetails VARCHAR(MAX)
		,bitPaidInFull BIT
		,numRemainingQty FLOAT
		,numAge VARCHAR(50)
		,dtAnticipatedDelivery  DATE
		,vcShipStatus VARCHAR(MAX)
		,dtExpectedDate VARCHAR(20)
		,dtExpectedDateOrig DATE
		,numQtyPicked FLOAT
		,numQtyPacked FLOAT
		,vcPaymentStatus VARCHAR(MAX)
		,numShipRate VARCHAR(40)
		,vcShippingLabel VARCHAR(MAX)
	)

	IF @bitGroupByOrder = 1
	BEGIN
		IF @vcSortColumn NOT IN ('CompanyInfo.vcCompanyName','OpportunityBizDocs.monAmountPaid','OpportunityBizDocs.vcBizDocID','OpportunityMaster.bintCreatedDate','OpportunityMaster.bitPaidInFull','OpportunityMaster.numAge','OpportunityMaster.numStatus','OpportunityMaster.vcPoppName','OpportunityMaster.dtItemReceivedDate','OpportunityMaster.numAssignedTo','OpportunityMaster.numRecOwner')
		BEGIN
			SET @vcSortColumn = 'OpportunityMaster.bintCreatedDate'
			SET @vcSortOrder = 'DESC'
		END
	END
	ELSE 
	BEGIN
		IF @vcSortColumn NOT IN ('CompanyInfo.vcCompanyName'
								,'Item.charItemType'
								,'Item.numBarCodeId'
								,'Item.numItemClassification'
								,'Item.numItemGroup'
								,'Item.vcSKU'
								,'OpportunityBizDocs.monAmountPaid'
								,'OpportunityMaster.dtExpectedDate'
								,'OpportunityItems.numQtyPacked'
								,'OpportunityItems.numQtyPicked'
								,'OpportunityItems.numQtyShipped'
								,'OpportunityItems.numRemainingQty'
								,'OpportunityItems.numUnitHour'
								,'OpportunityItems.vcInvoiced'
								,'OpportunityItems.vcItemName'
								,'OpportunityItems.vcItemReleaseDate'
								,'OpportunityMaster.bintCreatedDate'
								,'OpportunityMaster.bitPaidInFull'
								,'OpportunityMaster.numAge'
								,'OpportunityMaster.numStatus'
								,'OpportunityMaster.vcPoppName'
								,'WareHouseItems.vcLocation'
								,'OpportunityMaster.dtItemReceivedDate'
								,'OpportunityBizDocs.vcBizDocID')
		BEGIN
			SET @vcSortColumn = 'OpportunityMaster.bintCreatedDate'
			SET @vcSortOrder = 'DESC'
		END
	END

	DECLARE @tintPackingMode TINYINT
	DECLARE @tintInvoicingType TINYINT
	DECLARE @tintCommitAllocation TINYINT
	DECLARE @numShippingServiceItemID NUMERIC(18,0)
	DECLARE @numDefaultSalesShippingDoc NUMERIC(18,0)
	SELECT 
		@tintCommitAllocation=ISNULL(tintCommitAllocation,1) 
		,@numShippingServiceItemID=ISNULL(numShippingServiceItemID,0)
		,@numDefaultSalesShippingDoc=ISNULL(numDefaultSalesShippingDoc,0)
	FROM 
		Domain 
	WHERE 
		numDomainId=@numDomainID 

	IF @numViewID = 2 AND @tintPackingViewMode=3 AND @numDefaultSalesShippingDoc = 0
	BEGIN
		RAISERROR('DEFAULT_SHIPPING_BIZDOC_NOT_CONFIGURED',16,1)
		RETURN
	END

	SELECT
		@tintPackingMode=tintPackingMode
		,@tintInvoicingType=tintInvoicingType
	FROM
		MassSalesFulfillmentConfiguration
	WHERE
		numDomainID = @numDomainID 
		AND numUserCntID = @numUserCntID

	INSERT INTO @TempFieldsLeft
	(
		ID
		,numFieldID
		,vcFieldName
		,vcOrigDbColumnName
		,vcListItemType
		,numListID
		,bitAllowSorting
		,bitAllowFiltering
		,vcAssociatedControlType
		,vcLookBackTableName
		,bitCustomField
		,intRowNum
		,intColumnWidth
		,bitIsRequired
		,bitIsEmail
		,bitIsAlphaNumeric
		,bitIsNumeric
		,bitIsLengthValidation
		,intMaxLength
		,intMinLength
		,bitFieldMessage
		,vcFieldMessage
		,Grp_id
	)
	SELECT
		CONCAT(141,'~',DFM.numFieldID,'~',0)
		,DFM.numFieldID
		,(CASE WHEN @numViewID=6 AND DFM.numFieldID=248 THEN 'BizDoc' ELSE DFFM.vcFieldName END)
		,DFM.vcOrigDbColumnName
		,ISNULL(DFM.vcListItemType,'')
		,ISNULL(DFM.numListID,0)
		,DFFM.bitAllowSorting
		,DFFM.bitAllowFiltering
		,DFM.vcAssociatedControlType
		,DFM.vcLookBackTableName
		,0
		,DFCD.intRowNum
		,ISNULL(DFCD.intColumnWidth,0)
		,0
		,0
		,0
		,0
		,0
		,0
		,0
		,0
		,''
		,0
	FROM
		DycFormConfigurationDetails DFCD
	INNER JOIN
		DycFormField_Mapping DFFM
	ON
		DFCD.numFormId = DFFM.numFormID
		AND DFCD.numFieldId = DFFM.numFieldID
	INNER JOIN
		DycFieldMaster DFM
	ON
		DFFM.numFieldID = DFM.numFieldId
	WHERE 
		DFCD.numDomainId=@numDomainID 
		AND DFCD.numUserCntID=@numUserCntID 
		AND DFCD.numFormId=141 
		AND DFCD.numViewId=@numViewID
		AND ISNULL(DFFM.bitDeleted,0) = 0
		AND ISNULL(DFFM.bitSettingField,0) = 1
		AND ISNULL(DFCD.bitCustom,0) = 0
	UNION
	SELECT
		CONCAT(141,'~',CFM.Fld_id,'~',0)
		,CFM.Fld_id
		,CFM.Fld_label
		,CONCAT('CUST',CFM.Fld_id)
		,''
		,ISNULL(CFM.numListID,0)
		,0
		,0
		,CFM.Fld_type
		,''
		,1
		,DFCD.intRowNum
		,ISNULL(DFCD.intColumnWidth,0)
		,ISNULL(bitIsRequired,0)
		,ISNULL(bitIsEmail,0)
		,ISNULL(bitIsAlphaNumeric,0)
		,ISNULL(bitIsNumeric,0)
		,ISNULL(bitIsLengthValidation,0)
		,ISNULL(intMaxLength,0)
		,ISNULL(intMinLength,0)
		,ISNULL(bitFieldMessage,0)
		,ISNULL(vcFieldMessage,'')
		,CFM.Grp_id
	FROM
		DycFormConfigurationDetails DFCD
	INNER JOIN
		CFW_Fld_Master CFM
	ON
		DFCD.numFieldId = CFM.Fld_id
	LEFT JOIN
		CFW_Validation CV
	ON
		CFM.Fld_id = CV.numFieldID
	WHERE
		DFCD.numDomainId=@numDomainID 
		AND DFCD.numUserCntID=@numUserCntID 
		AND DFCD.numFormId=141 
		AND DFCD.numViewId=@numViewID
		AND ISNULL(DFCD.bitCustom,0) = 1

	IF (SELECT COUNT(*) FROM @TempFieldsLeft) = 0
	BEGIN
		INSERT INTO @TempFieldsLeft
		(
			ID
			,numFieldID
			,vcFieldName
			,vcOrigDbColumnName
			,vcListItemType
			,numListID
			,bitAllowSorting
			,bitAllowFiltering
			,vcAssociatedControlType
			,vcLookBackTableName
			,bitCustomField
			,intRowNum
			,intColumnWidth
		)
		SELECT
			CONCAT(141,'~',DFM.numFieldID,'~',0)
			,DFM.numFieldID
			,DFFM.vcFieldName
			,DFM.vcOrigDbColumnName
			,ISNULL(DFM.vcListItemType,'')
			,ISNULL(DFM.numListID,0)
			,DFFM.bitAllowSorting
			,DFFM.bitAllowFiltering
			,DFM.vcAssociatedControlType
			,DFM.vcLookBackTableName
			,0
			,DFFM.[order]
			,100
		FROM
			DycFormField_Mapping DFFM
		INNER JOIN
			DycFieldMaster DFM
		ON
			DFFM.numFieldID = DFM.numFieldId
		WHERE 
			DFFM.numFormId=141 
			AND ISNULL(DFFM.bitDeleted,0) = 0
			AND ISNULL(DFFM.bitSettingField,0) = 1
			AND ISNULL(DFFM.bitDefault,0) = 1
	END

	DECLARE @j INT = 0
	DECLARE @jCount INT
	DECLARE @numFieldId NUMERIC(18,0)
	DECLARE @vcFieldName AS VARCHAR(50)                                                                                                
	DECLARE @vcAssociatedControlType VARCHAR(10)  
	DECLARE @vcDbColumnName VARCHAR(100)
	DECLARE @Grp_id INT                                          
	--DECLARE @numListID AS NUMERIC(9)       
	DECLARE @bitCustomField BIT       
	SET @jCount = (SELECT COUNT(*) FROM @TempFieldsLeft)
	DECLARE @vcCustomFields VARCHAR(MAX) = ''
	DECLARE @vcCustomWhere VARCHAR(MAX) = ''

	WHILE @j <= @jCount
	BEGIN
		SELECT
			@numFieldId=numFieldID,
			@vcFieldName = vcFieldName,
			@vcAssociatedControlType = vcAssociatedControlType,
			@vcDbColumnName = CONCAT('Cust',numFieldID),
			@bitCustomField=ISNULL(bitCustomField,0),
			@Grp_id = ISNULL(Grp_id,0)
		FROM 
			@TempFieldsLeft
		WHERE 
			numPKID = @j     
		
		
			
		IF @bitCustomField = 1
		BEGIN         
			IF @Grp_id = 5
			BEGIN
				SET @vcCustomFields = CONCAT(@vcCustomFields,CONCAT(',dbo.GetCustFldValueOppItems(',@numFieldId,',T1.numOppItemID,T1.numItemCode)'),' [',@vcDbColumnName,']')
			END
			ELSE
			BEGIN
				IF @vcAssociatedControlType = 'TextBox' OR @vcAssociatedControlType = 'TextArea' 
				BEGIN
					SET @vcCustomFields = CONCAT(@vcCustomFields,',CFW',@numFieldId,'.Fld_Value  [',@vcDbColumnName,']')
             
					SET @vcCustomWhere = CONCAT(@vcCustomWhere,' left Join CFW_FLD_Values_Opp CFW',@numFieldId
									,' ON CFW' , @numFieldId , '.Fld_Id='
									,@numFieldId
									, ' and CFW' , @numFieldId
									, '.RecId=T1.numOppID')                                                         
				END   
				ELSE IF @vcAssociatedControlType = 'CheckBox' 
				BEGIN
					SET @vcCustomFields =CONCAT( @vcCustomFields
						, ',case when isnull(CFW'
						, @numFieldId
						, '.Fld_Value,0)=0 then 0 when isnull(CFW'
						, @numFieldId
						, '.Fld_Value,0)=1 then 1 end   ['
						,  @vcDbColumnName
						, ']')               
 
					SET @vcCustomWhere = CONCAT(@vcCustomWhere
						, ' left Join CFW_FLD_Values_Opp CFW'
						, @numFieldId
						, ' ON CFW',@numFieldId,'.Fld_Id='
						, @numFieldId
						, ' and CFW'
						, @numFieldId
						, '.RecId=T1.numOppID   ')                                                    
				END                
				ELSE IF @vcAssociatedControlType = 'DateField' 
				BEGIN
					SET @vcCustomFields = CONCAT(@vcCustomFields
						, ',dbo.FormatedDateFromDate(CFW'
						, @numFieldId
						, '.Fld_Value,'
						, @numDomainId
						, ')  [',@vcDbColumnName ,']' )  
					                  
					SET @vcCustomWhere = CONCAT(@vcCustomWhere
						, ' left Join CFW_FLD_Values_Opp CFW'
						, @numFieldId
						, ' on CFW', @numFieldId, '.Fld_Id='
						, @numFieldId
						, ' and CFW'
						, @numFieldId
						, '.RecId=T1.numOppID   ' )                                                        
				END                
				ELSE IF @vcAssociatedControlType = 'SelectBox' 
				BEGIN
					SET @vcCustomFields = CONCAT(@vcCustomFields,',L',@numFieldId,'.vcData',' [',@vcDbColumnName,']')
					                                                    
					SET @vcCustomWhere = CONCAT(@vcCustomWhere
						, ' left Join CFW_FLD_Values_Opp CFW'
						, @numFieldId
						, ' on CFW',@numFieldId ,'.Fld_Id='
						, @numFieldId
						, ' and CFW'
						, @numFieldId
						, '.RecId=T1.numOppID    ')     
					                                                    
					SET @vcCustomWhere = CONCAT(@vcCustomWhere
						, ' left Join ListDetails L'
						, @numFieldId
						, ' on L'
						, @numFieldId
						, '.numListItemID=CFW'
						, @numFieldId
						, '.Fld_Value' )              
				END
				ELSE
				BEGIN
					SET @vcCustomFields = CONCAT(@vcCustomFields,CONCAT(',dbo.GetCustFldValueOpp(',@numFieldId,',T1.numOppID)'),' [',@vcDbColumnName,']')
				END 
			END
			
		END

		SET @j = @j + 1
	END

	CREATE TABLE #TEMPMSRecords
	(
		numOppID NUMERIC(18,0)
		,numOppItemID NUMERIC(18,0)
		,numOppBizDocID NUMERIC(18,0)
	)

	DECLARE @vcSQL NVARCHAR(MAX)
	DECLARE @vcSQLFinal NVARCHAR(MAX)
	
	SET @vcSQL = CONCAT('INSERT INTO #TEMPMSRecords
						(
							numOppID
							,numOppItemID
							,numOppBizDocID
						)
						SELECT
							OpportunityMaster.numOppId
							,',(CASE WHEN @bitGroupByOrder = 1 THEN '0' ELSE 'OpportunityItems.numoppitemtCode' END),'
							,',(CASE 
									WHEN @numViewID = 4 OR @numViewID = 6
									THEN 'OpportunityBizDocs.numOppBizDocsId'  
									ELSE '0' 
								END),'
						FROM
							OpportunityMaster
						INNER JOIN
							DivisionMaster
						ON
							OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
						INNER JOIN
							CompanyInfo
						ON
							DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
						INNER JOIN 
							OpportunityItems
						ON
							OpportunityMaster.numOppId = OpportunityItems.numOppId
						INNER JOIN
							Item
						ON
							OpportunityItems.numItemCode = Item.numItemCode
						LEFT JOIN
							WareHouseItems
						ON
							OpportunityItems.numWarehouseItmsID = WareHouseItems.numWareHouseItemID
						',
						(CASE 
							WHEN ISNULL(@numBatchID,0) > 0 
							THEN 
								' INNER JOIN 
										MassSalesFulfillmentBatchOrders
								ON
									OpportunityMaster.numOppId = MassSalesFulfillmentBatchOrders.numOppID
									AND (OpportunityItems.numOppItemtCode = MassSalesFulfillmentBatchOrders.numOppItemID OR ISNULL(MassSalesFulfillmentBatchOrders.numOppItemID,0) = 0)'
							ELSE 
								'' 
						END)	
						,
						(CASE 
							WHEN @numViewID = 4 
							THEN
								'INNER JOIN 
									OpportunityBizDocs
								ON
									OpportunityMaster.numOppId = OpportunityBizDocs.numOppId
									AND OpportunityBizDocs.numBizDocID=287'
							WHEN @numViewID = 6
							THEN
								CONCAT('INNER JOIN 
											OpportunityBizDocs
										ON
											OpportunityMaster.numOppId = OpportunityBizDocs.numOppId
											AND OpportunityBizDocs.numBizDocID=',(CASE @tintPrintBizDocViewMode WHEN 2 THEN 29397 WHEN 3 THEN 296 WHEN 4 THEN 287 ELSE 55206 END))
							ELSE ''

						END)
						,'
						WHERE
							OpportunityMaster.numDomainId=@numDomainID
							AND OpportunityMaster.tintOppType = 1
							AND OpportunityMaster.tintOppStatus = 1
							',(CASE 
								WHEN @numViewID=6 AND @tintPendingCloseFilter=1 THEN 'AND ISNULL(OpportunityMaster.tintshipped,0) = 0'
								WHEN @numViewID=6 AND @tintPendingCloseFilter=2 THEN 'AND ISNULL(OpportunityMaster.tintshipped,0) = 1'
								WHEN @numViewID=6 AND @tintPendingCloseFilter=3 THEN ''
								ELSE 'AND ISNULL(OpportunityMaster.tintshipped,0) = 0'
							END),' 
							AND 1 = (CASE 
										WHEN @numViewID IN (1,2) AND ISNULL(@bitRemoveBO,0) = 1
										THEN (CASE 
												WHEN (Item.charItemType = ''p'' AND ISNULL(OpportunityItems.bitDropShip, 0) = 0 AND ISNULL(Item.bitAsset,0)=0)
												THEN 
													(CASE WHEN dbo.CheckOrderItemInventoryStatus(OpportunityItems.numItemCode,(CASE 
				WHEN @numViewID = 1
				THEN ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyPicked,0)
				WHEN @numViewID = 2 AND @tintPackingViewMode = 2
				THEN (CASE WHEN ISNULL(@tintPackingMode,1) = 2 THEN ISNULL(OpportunityItems.numQtyPicked,0) - ISNULL(OpportunityItems.numQtyShipped,0) ELSE ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyShipped,0) END)
				WHEN @numViewID = 2 AND @tintPackingViewMode = 3
				THEN (CASE WHEN ISNULL(@tintPackingMode,1) = 2 THEN ISNULL(OpportunityItems.numQtyPicked,0) ELSE ISNULL(OpportunityItems.numUnitHour,0) END) - ISNULL((SELECT 
																																											SUM(OpportunityBizDocItems.numUnitHour)
																																										FROM
																																											OpportunityBizDocs
																																										INNER JOIN
																																											OpportunityBizDocItems
																																										ON
																																											OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																																										WHERE
																																											OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
																																											AND OpportunityBizDocs.numBizDocId=@numDefaultSalesShippingDoc
					 																																					AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode),0)
				
				ELSE OpportunityItems.numUnitHour
			END),OpportunityItems.numoppitemtCode,OpportunityItems.numWarehouseItmsID,(CASE WHEN ISNULL(Item.bitKitParent,0) = 1 THEN 1 ELSE 0 END)) = 1 THEN 0 ELSE 1 END)
												ELSE 1
											END) 
										ELSE 1
									END)		
							AND 1 = (CASE WHEN LEN(ISNULL(@vcOrderStauts,'''')) > 0 THEN (CASE WHEN OpportunityMaster.numStatus ',(CASE WHEN ISNULL(@bitIncludeSearch,0) = 0 THEN 'NOT IN' ELSE 'IN' END),' (SELECT Id FROM dbo.SplitIDs(@vcOrderStauts,'','')) THEN 1 ELSE 0 END) ELSE 1 END)
							AND 1 = (CASE 
										WHEN LEN(ISNULL(@vcFilterValue,'''')) > 0
										THEN
											CASE 
												WHEN @tintFilterBy=1 --Item Classification
												THEN (CASE WHEN Item.numItemClassification ',(CASE WHEN ISNULL(@bitIncludeSearch,0) = 0 THEN 'NOT IN' ELSE 'IN' END),' (SELECT Id FROM dbo.SplitIDs(@vcFilterValue,'','')) THEN 1 ELSE 0 END)
												ELSE 1
											END
										ELSE 1
									END)
							AND 1 = (CASE
										WHEN @numViewID = 1 THEN (CASE WHEN ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyPicked,0) > 0 THEN 1 ELSE 0 END)
										WHEN @numViewID = 2 THEN (CASE 
																		WHEN @tintPackingViewMode = 1 
																		THEN (CASE 
																				WHEN ISNULL((SELECT COUNT(*) FROM OpportunityItems OIInner WHERE OIInner.numOppID=OpportunityMaster.numOppId AND OIInner.numItemCode=ISNULL(@numShippingServiceItemID,0)),0) = 0 
																				THEN 1 
																				ELSE 0 
																			END)
																		WHEN @tintPackingViewMode = 2
																		THEN (CASE 
																				WHEN ',(CASE WHEN ISNULL(@tintPackingMode,1) = 2 THEN 'ISNULL(OpportunityItems.numQtyPicked,0)' ELSE 'ISNULL(OpportunityItems.numUnitHour,0)' END),' - ISNULL(OpportunityItems.numQtyShipped,0) > 0
																				THEN 1 
																				ELSE 0 
																			END)
																		WHEN @tintPackingViewMode = 3
																		THEN (CASE 
																				WHEN ',(CASE WHEN ISNULL(@tintPackingMode,1) = 2 THEN 'ISNULL(OpportunityItems.numQtyPicked,0)' ELSE 'ISNULL(OpportunityItems.numUnitHour,0)' END),' - ISNULL((SELECT 
																																																													SUM(OpportunityBizDocItems.numUnitHour)
																																																												FROM
																																																													OpportunityBizDocs
																																																												INNER JOIN
																																																													OpportunityBizDocItems
																																																												ON
																																																													OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																																																												WHERE
																																																													OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
																																																													AND OpportunityBizDocs.numBizDocId=@numDefaultSalesShippingDoc
																																																													AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode),0) > 0
																				THEN 1 
																				ELSE 0 
																			END)','
																		ELSE 0
																	END)
										WHEN @numViewID = 3
										THEN (CASE 
												WHEN (CASE 
														WHEN ISNULL(@tintInvoicingType,0)=1 
														THEN ISNULL(OpportunityItems.numQtyShipped,0) 
														ELSE ISNULL(OpportunityItems.numUnitHour,0) 
													END) - ISNULL((SELECT 
																		SUM(OpportunityBizDocItems.numUnitHour)
																	FROM
																		OpportunityBizDocs
																	INNER JOIN
																		OpportunityBizDocItems
																	ON
																		OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																	WHERE
																		OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
																		AND OpportunityBizDocs.numBizDocId=287
																		AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode),0) > 0 
												THEN 1
												ELSE 0 
											END)
										WHEN @numViewID = 4 THEN 1
										WHEN @numViewID = 5 THEN 1
										WHEN @numViewID = 6 THEN 1
									END)
							AND 1 = (CASE
										WHEN @numViewID = 1 THEN (CASE WHEN ISNULL(WareHouseItems.numWareHouseItemID,0) > 0 AND ISNULL(OpportunityItems.bitDropship,0) = 0 THEN 1 ELSE 0 END)
										WHEN @numViewID = 2 AND @tintPackingViewMode <> 2 THEN (CASE WHEN ISNULL(WareHouseItems.numWareHouseItemID,0) > 0 AND ISNULL(OpportunityItems.bitDropship,0) = 0 THEN 1 ELSE 0 END)
										WHEN @numViewID = 2 AND @tintPackingViewMode = 2 THEN 1
										WHEN @numViewID = 3 THEN 1
										WHEN @numViewID = 4 THEN 1
										WHEN @numViewID = 5 THEN 1
										WHEN @numViewID = 6 THEN 1
										ELSE 0
									END)',
							(CASE 
								WHEN @numViewID = 5 AND ISNULL(@tintPendingCloseFilter,0) = 2
								THEN ' AND (SELECT 
												COUNT(*) 
											FROM 
											(
												SELECT
													OI.numoppitemtCode,
													ISNULL(OI.numUnitHour,0) AS OrderedQty,
													ISNULL(TempFulFilled.FulFilledQty,0) AS FulFilledQty
												FROM
													OpportunityItems OI
												INNER JOIN
													Item I
												ON
													OI.numItemCode = I.numItemCode
												OUTER APPLY
												(
													SELECT
														SUM(OpportunityBizDocItems.numUnitHour) AS FulFilledQty
													FROM
														OpportunityBizDocs
													INNER JOIN
														OpportunityBizDocItems 
													ON
														OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
													WHERE
														OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
														AND ISNULL(OpportunityBizDocs.numBizDocId,0) = 296
														AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
												) AS TempFulFilled
												WHERE
													OI.numOppID = OpportunityMaster.numOppID
													AND ISNULL(OI.numUnitHour,0) > 0
													AND ISNULL(OI.bitDropShip,0) = 0
											) X
											WHERE
												X.OrderedQty <> X.FulFilledQty) = 0
										AND (SELECT 
												COUNT(*) 
											FROM 
											(
												SELECT
													OI.numoppitemtCode,
													ISNULL(OI.numUnitHour,0) AS OrderedQty,
													ISNULL(TempInvoice.InvoicedQty,0) AS InvoicedQty
												FROM
													OpportunityItems OI
												INNER JOIN
													Item I
												ON
													OI.numItemCode = I.numItemCode
												OUTER APPLY
												(
													SELECT
														SUM(OpportunityBizDocItems.numUnitHour) AS InvoicedQty
													FROM
														OpportunityBizDocs
													INNER JOIN
														OpportunityBizDocItems 
													ON
														OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
													WHERE
														OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
														AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
														AND ISNULL(OpportunityBizDocs.bitAuthoritativeBizDocs,0) = 1
												) AS TempInvoice
												WHERE
													OI.numOppID = OpportunityMaster.numOppID
													AND ISNULL(OI.numUnitHour,0) > 0
											) X
											WHERE
												X.OrderedQty > X.InvoicedQty) = 0'
								ELSE '' 
							END)
							,(CASE WHEN @numViewID = 4 THEN 'AND 1 = (CASE WHEN ISNULL(OpportunityBizDocs.monDealAmount,0) - ISNULL(OpportunityBizDocs.monAmountPaid,0) > 0 THEN 1 ELSE 0 END)' ELSE '' END)
							,' AND 1 = (CASE WHEN @numViewID = 2 AND @tintPackingViewMode = 3 THEN (CASE WHEN ISNULL(Item.bitContainer,0)=1 THEN 0 ELSE 1 END) ELSE 1 END)
								AND 1 = (CASE WHEN @numViewID <> 6 AND ISNULL(@numWarehouseID,0) > 0 AND ISNULL(OpportunityItems.numWarehouseItmsID,0) > 0 THEN (CASE WHEN @numWarehouseID=WareHouseItems.numWarehouseID THEN 1 ELSE 0 END) ELSE 1 END)'
								,(CASE WHEN ISNULL(@numShippingZone,0) > 0 THEN ' AND 1 = (CASE 
																				WHEN ISNULL((SELECT TOP 1 numState FROM fn_getOPPAddressDetails(OpportunityMaster.numOppId,OpportunityMaster.numDomainID,2)),0) > 0 
																				THEN
																					CASE 
																						WHEN (SELECT COUNT(*) FROM dbo.State WHERE numDomainID=@numDomainId AND numStateID =  (SELECT TOP 1 numState FROM fn_getOPPAddressDetails(OpportunityMaster.numOppId,OpportunityMaster.numDomainID,2)) AND numShippingZone=@numShippingZone) > 0 
																						THEN 1
																						ELSE 0
																					END
																				ELSE 0 
																			END)' ELSE '' END),
								@vcCustomSearchValue,(CASE WHEN @bitGroupByOrder = 1 THEN CONCAT('GROUP BY OpportunityMaster.numOppId',(CASE WHEN @numViewID = 4 THEN ',OpportunityBizDocs.numOppBizDocsId,OpportunityBizDocs.monAmountPaid' WHEN @numViewID = 6 THEN ',OpportunityBizDocs.numOppBizDocsId'  ELSE '' END)) ELSE '' END));

	EXEC sp_executesql @vcSQL, N'@numDomainID NUMERIC(18,0), @vcOrderStauts VARCHAR(MAX), @vcFilterValue VARCHAR(MAX), @tintFilterBy TINYINT, @numViewID NUMERIC(18,0), @tintPackingViewMode TINYINT, @numShippingServiceItemID NUMERIC(18,0), @numWarehouseID NUMERIC(18,0), @numShippingZone NUMERIC(18,0), @numDefaultSalesShippingDoc NUMERIC(18,0), @tintInvoicingType TINYINT, @tintPackingMode TINYINT, @bitRemoveBO BIT, @ClientTimeZoneOffset INT', @numDomainID, @vcOrderStauts, @vcFilterValue, @tintFilterBy, @numViewID, @tintPackingViewMode, @numShippingServiceItemID, @numWarehouseID, @numShippingZone, @numDefaultSalesShippingDoc, @tintInvoicingType, @tintPackingMode, @bitRemoveBO, @ClientTimeZoneOffset;

	If @numViewID = 2 AND @tintPackingViewMode = 3
	BEGIN
		SET @vcSQL = CONCAT('INSERT INTO #TEMPMSRecords
						(
							numOppID
							,numOppItemID
							,numOppBizDocID
						)
						SELECT
							OpportunityMaster.numOppId
							,',(CASE WHEN @bitGroupByOrder = 1 THEN '0' ELSE 'OpportunityItems.numoppitemtCode' END),'
							,OpportunityBizDocs.numOppBizDocsId
						FROM
							OpportunityMaster
						INNER JOIN
							DivisionMaster
						ON
							OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
						INNER JOIN
							CompanyInfo
						ON
							DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
						INNER JOIN 
							OpportunityBizDocs
						ON
							OpportunityMaster.numOppId = OpportunityBizDocs.numOppId
							AND OpportunityBizDocs.numBizDocID=@numDefaultSalesShippingDoc
						INNER JOIN
							OpportunityBizDocItems
						ON
							OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
						INNER JOIN 
							OpportunityItems
						ON
							OpportunityMaster.numOppId = OpportunityItems.numOppId
							AND OpportunityItems.numoppitemtCode = OpportunityBizDocItems.numOppItemID
						INNER JOIN
							Item
						ON
							OpportunityItems.numItemCode = Item.numItemCode
						LEFT JOIN
							WareHouseItems
						ON
							OpportunityItems.numWarehouseItmsID = WareHouseItems.numWareHouseItemID					
						',
						(CASE 
							WHEN ISNULL(@numBatchID,0) > 0 
							THEN 
								' INNER JOIN 
										MassSalesFulfillmentBatchOrders
								ON
									OpportunityMaster.numOppId = MassSalesFulfillmentBatchOrders.numOppID
									AND (OpportunityItems.numOppItemtCode = MassSalesFulfillmentBatchOrders.numOppItemID OR ISNULL(MassSalesFulfillmentBatchOrders.numOppItemID,0) = 0)'
							ELSE 
								'' 
						END)	
						,'
						WHERE
							OpportunityMaster.numDomainId=@numDomainID
							AND OpportunityMaster.tintOppType = 1
							AND OpportunityMaster.tintOppStatus = 1
							AND ISNULL(OpportunityMaster.tintshipped,0) = 0		
							AND 1 = (CASE WHEN LEN(ISNULL(@vcOrderStauts,'''')) > 0 THEN (CASE WHEN OpportunityMaster.numStatus ',(CASE WHEN ISNULL(@bitIncludeSearch,0) = 0 THEN 'NOT IN' ELSE 'IN' END),' (SELECT Id FROM dbo.SplitIDs(@vcOrderStauts,'','')) THEN 1 ELSE 0 END) ELSE 1 END)
							AND 1 = (CASE 
										WHEN LEN(ISNULL(@vcFilterValue,'''')) > 0
										THEN
											CASE 
												WHEN @tintFilterBy=1 --Item Classification
												THEN (CASE WHEN Item.numItemClassification ',(CASE WHEN ISNULL(@bitIncludeSearch,0) = 0 THEN 'NOT IN' ELSE 'IN' END),' (SELECT Id FROM dbo.SplitIDs(@vcFilterValue,'','')) THEN 1 ELSE 0 END)
												ELSE 1
											END
										ELSE 1
									END)
							AND 1 = (CASE 
										WHEN ISNULL((SELECT COUNT(*) FROM ShippingBox WHERE ISNULL(vcShippingLabelImage,'''') <> '''' AND ISNULL(vcTrackingNumber,'''') <> '''' AND numShippingReportId IN (SELECT numShippingReportId FROM ShippingReport WHERE ShippingReport.numOppID=OpportunityMaster.numOppId AND ShippingReport.numOppBizDocId=OpportunityBizDocs.numOppBizDocsId)),0) = 0 
										THEN 1 
										ELSE 0 
									END)
										
							AND 1 = (CASE WHEN ISNULL(WareHouseItems.numWareHouseItemID,0) > 0 AND ISNULL(OpportunityItems.bitDropship,0) = 0 THEN 1 ELSE 0 END)',
							' AND 1 = (CASE WHEN @numViewID = 2 AND @tintPackingViewMode = 3 THEN (CASE WHEN ISNULL(Item.bitContainer,0)=1 THEN 0 ELSE 1 END) ELSE 1 END)
								AND 1 = (CASE WHEN ISNULL(@numWarehouseID,0) > 0 AND ISNULL(OpportunityItems.numWarehouseItmsID,0) > 0 THEN (CASE WHEN @numWarehouseID=WareHouseItems.numWarehouseID THEN 1 ELSE 0 END) ELSE 1 END)'
								,(CASE WHEN ISNULL(@numShippingZone,0) > 0 THEN ' AND 1 = (CASE 
																				WHEN ISNULL((SELECT TOP 1 numState FROM fn_getOPPAddressDetails(OpportunityMaster.numOppId,OpportunityMaster.numDomainID,2)),0) > 0 
																				THEN
																					CASE 
																						WHEN (SELECT COUNT(*) FROM dbo.State WHERE numDomainID=@numDomainId AND numStateID =  (SELECT TOP 1 numState FROM fn_getOPPAddressDetails(OpportunityMaster.numOppId,OpportunityMaster.numDomainID,2)) AND numShippingZone=@numShippingZone) > 0 
																						THEN 1
																						ELSE 0
																					END
																				ELSE 0 
																			END)' ELSE '' END),
								@vcCustomSearchValue,(CASE WHEN @bitGroupByOrder = 1 THEN ' GROUP BY OpportunityMaster.numOppId,OpportunityBizDocs.numOppBizDocsId,OpportunityBizDocs.monAmountPaid' ELSE '' END));

		EXEC sp_executesql @vcSQL, N'@numDomainID NUMERIC(18,0), @vcOrderStauts VARCHAR(MAX), @vcFilterValue VARCHAR(MAX), @tintFilterBy TINYINT, @numViewID NUMERIC(18,0), @tintPackingViewMode TINYINT, @numShippingServiceItemID NUMERIC(18,0), @numWarehouseID NUMERIC(18,0), @numShippingZone NUMERIC(18,0), @numDefaultSalesShippingDoc NUMERIC(18,0), @tintInvoicingType TINYINT, @tintPackingMode TINYINT, @bitRemoveBO BIT, @ClientTimeZoneOffset INT', @numDomainID, @vcOrderStauts, @vcFilterValue, @tintFilterBy, @numViewID, @tintPackingViewMode, @numShippingServiceItemID, @numWarehouseID, @numShippingZone, @numDefaultSalesShippingDoc, @tintInvoicingType, @tintPackingMode, @bitRemoveBO, @ClientTimeZoneOffset;
	END

	SET @numTotalRecords = (SELECT COUNT(*) FROM #TEMPMSRecords)

	IF @bitGroupByOrder = 1
	BEGIN
		
		SET @vcSQLFinal = CONCAT('

		INSERT INTO #TEMPResult
		(
			numOppID
			,numDivisionID
			,numTerID
			,numOppItemID
			,numOppBizDocID
			,bintCreatedDate
			,vcCompanyName
			,vcPoppName
			,numAssignedTo
			,numRecOwner
			,vcBizDocID
			,numStatus
			,txtComments
			,monAmountPaid
			,monAmountToPay
			,intUsedShippingCompany
			,intShippingCompany
			,numPreferredShipVia
			,numPreferredShipService
			,bitPaidInFull
			,numAge
			,dtAnticipatedDelivery
			,vcShipStatus
			,dtExpectedDate
			,dtExpectedDateOrig
			,vcPaymentStatus
			,numShipRate
			,vcShippingLabel
		)
		SELECT
			OpportunityMaster.numOppId
			,OpportunityMaster.numDivisionID
			,ISNULL(DivisionMaster.numTerID,0) numTerID
			,0
			,TEMPOrder.numOppBizDocID
			,(CASE WHEN @numViewID = 6 THEN dbo.FormatedDateFromDate(DATEADD(MINUTE,@ClientTimeZoneOffset * -1,OpportunityBizDocs.dtCreatedDate),@numDomainID) ELSE dbo.FormatedDateFromDate(DATEADD(MINUTE,@ClientTimeZoneOffset * -1,OpportunityMaster.bintCreatedDate),@numDomainID) END) 
			,ISNULL(CompanyInfo.vcCompanyName,'''')
			,ISNULL(OpportunityMaster.vcPoppName,'''')
			,dbo.fn_GetContactName(OpportunityMaster.numAssignedTo)
			,dbo.fn_GetContactName(OpportunityMaster.numRecOwner)
			,(CASE WHEN @numViewID = 4 OR @numViewID = 6 THEN ISNULL(OpportunityBizDocs.vcBizDocID,''-'') ELSE '''' END)
			,dbo.GetListIemName(OpportunityMaster.numStatus)
			,ISNULL(OpportunityMaster.txtComments,'''')
			,ISNULL(TempPaid.monAmountPaid,0)
			,ISNULL(OpportunityBizDocs.monDealAmount,0) - ISNULL(OpportunityBizDocs.monAmountPaid,0)
			,CONCAT(ISNULL(LDOpp.vcData,''-''),'', '',ISNULL(SSOpp.vcShipmentService,''-'')) 
			,CONCAT(ISNULL(LDDiv.vcData,''-''),'', '',ISNULL(SSDiv.vcShipmentService,''-''))
			,ISNULL(DivisionMaster.intShippingCompany,0)
			,ISNULL(DivisionMaster.numDefaultShippingServiceID,0)
			,(CASE WHEN ISNULL(TempPaid.monAmountPaid,0) >= ISNULL(OpportunityMaster.monDealAmount,0) THEN 1 ELSE 0 END)
			,CONCAT(''<span style="color:'',(CASE
						WHEN (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24) = 0 AND (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) < 4  
						THEN ''#00ff00''
						WHEN (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24) = 0 AND ((DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) >= 4 AND (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) < 8)
						THEN ''#00b050''
						WHEN (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24) = 0 AND ((DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) >= 8 AND (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) < 12)
						THEN ''#3399ff''
						WHEN (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24) = 0 AND ((DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) >= 12 AND (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) < 24)
						THEN ''#ff9900''
						WHEN (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24) BETWEEN 1 AND 2  
						THEN ''#9b3596''
						ELSE ''#ff0000''
					END),''">'',CONCAT(DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24,''d '',DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24,''h''),''</span>'')
			,'''' dtAnticipatedDelivery
			,'''' vcShipStatus
			,dbo.FormatedDateFromDate(OpportunityMaster.dtExpectedDate,@numDomainID)
			,OpportunityMaster.dtExpectedDate
			,'''' vcPaymentStatus
			,ISNULL(CAST((SELECT SUM(monTotAmount) FROM OpportunityItems OIInner WHERE OIInner.numOppID=OpportunityMaster.numOppId AND OIInner.numItemCode=@numShippingServiceItemID) AS VARCHAR),'''')
			,(CASE WHEN @numViewID=6 THEN STUFF((SELECT 
													CONCAT('', '',''<a style="text-decoration: none" href="javascript:void(0);" onclick="openReportList('',ShippingReport.numOppID ,'','',ShippingReport.numOppBizDocId,'','',ShippingReport.numShippingReportId,'');"><img src="../images/barcode_generated_22x22.png"></a>'')
												FROM 
													ShippingReport
												INNER JOIN
													ShippingBox 
												ON 
													ShippingReport.numShippingReportId=ShippingBox.numShippingReportId 
												WHERE 
													ShippingReport.numOppID= OpportunityMaster.numOppId
													AND ShippingReport.numOppBizDocId= OpportunityBizDocs.numOppBizDocsId
													AND LEN(ISNULL(vcShippingLabelImage,'''')) > 0
												for xml path('''')),1,2,'''') ELSE '''' END)
		FROM
			#TEMPMSRecords TEMPOrder
		INNER JOIN
			OpportunityMaster
		ON
			TEMPOrder.numOppID = OpportunityMaster.numOppId
		INNER JOIN
			DivisionMaster
		ON
			OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
		INNER JOIN
			CompanyInfo
		ON
			DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
		LEFT JOIN
			OpportunityBizDocs
		ON
			OpportunityMaster.numOppId = OpportunityBizDocs.numOppId
			AND OpportunityBizDocs.numOppBizDocsId = ISNULL(TEMPOrder.numOppBizDocID,0)
		LEFT JOIN
			ListDetails  LDOpp
		ON
			LDOpp.numListID = 82
			AND (LDOpp.numDomainID = @numDomainID OR LDOpp.constFlag=1)
			AND ISNULL(OpportunityMaster.intUsedShippingCompany,0) = LDOpp.numListItemID
		LEFT JOIN
			ShippingService AS SSOpp
		ON
			(SSOpp.numDomainID=@numDomainID OR ISNULL(SSOpp.numDomainID,0)=0) 
			AND OpportunityMaster.numShippingService = SSOpp.numShippingServiceID
		LEFT JOIN
			ListDetails  LDDiv
		ON
			LDDiv.numListID = 82
			AND (LDDiv.numDomainID = @numDomainID OR LDDiv.constFlag=1)
			AND ISNULL(DivisionMaster.intShippingCompany,0) = LDDiv.numListItemID
		LEFT JOIN
			ShippingService AS SSDiv
		ON
			(SSDiv.numDomainID=@numDomainID OR ISNULL(SSDiv.numDomainID,0)=0) 
			AND ISNULL(DivisionMaster.numDefaultShippingServiceID,0) = SSDiv.numShippingServiceID
		OUTER APPLY
		(
			SELECT 
				SUM(OpportunityBizDocs.monAmountPaid) monAmountPaid
			FROM 
				OpportunityBizDocs 
			WHERE 
				OpportunityBizDocs.numOppId=OpportunityMaster.numOppId 
				AND OpportunityBizDocs.numBizDocId=287
		) TempPaid
		ORDER BY ',
		(CASE @vcSortColumn
			WHEN 'CompanyInfo.vcCompanyName' THEN 'CompanyInfo.vcCompanyName '
			WHEN 'OpportunityMaster.numStatus' THEN 'dbo.GetListIemName(OpportunityMaster.numStatus)'
			WHEN 'OpportunityMaster.vcPoppName' THEN 'OpportunityMaster.vcPoppName'
			WHEN 'OpportunityMaster.numAssignedTo' THEN 'dbo.fn_GetContactName(OpportunityMaster.numAssignedTo)'
			WHEN 'OpportunityMaster.numRecOwner' THEN 'dbo.fn_GetContactName(OpportunityMaster.numRecOwner)'
			WHEN 'OpportunityMaster.numAge' THEN 'OpportunityMaster.bintCreatedDate'
			WHEN 'OpportunityBizDocs.monAmountPaid' THEN 'TempPaid.monAmountPaid'
			WHEN 'OpportunityBizDocs.vcBizDocID' THEN 'LEN(OpportunityBizDocs.vcBizDocID) DESC, OpportunityBizDocs.vcBizDocID'
			WHEN 'OpportunityMaster.bitPaidInFull' THEN '(CASE WHEN ISNULL(TempPaid.monAmountPaid,0) >= ISNULL(OpportunityMaster.monDealAmount,0) THEN 1 ELSE 0 END)'
			ELSE (CASE WHEN @numViewID = 6 THEN 'OpportunityBizDocs.dtCreatedDate' ELSE 'OpportunityMaster.bintCreatedDate' END)
		END),' ',(CASE WHEN ISNULL(@vcSortOrder,'') <> '' THEN @vcSortOrder ELSE 'DESC' END),' OFFSET (@numPageIndex-1) * 200 ROWS FETCH NEXT 200 ROWS ONLY')

		--PRINT CAST(@vcSQLFinal AS NTEXT)

		EXEC sp_executesql @vcSQLFinal, N'@numDomainID NUMERIC(18,0), @vcOrderStauts VARCHAR(MAX), @vcFilterValue VARCHAR(MAX), @tintFilterBy TINYINT, @numViewID NUMERIC(18,0), @tintPackingViewMode TINYINT, @numShippingServiceItemID NUMERIC(18,0), @numWarehouseID NUMERIC(18,0), @numShippingZone NUMERIC(18,0), @numDefaultSalesShippingDoc NUMERIC(18,0), @tintInvoicingType TINYINT, @ClientTimeZoneOffset INT, @tintPackingMode TINYINT, @numPageIndex INT', @numDomainID, @vcOrderStauts, @vcFilterValue, @tintFilterBy, @numViewID, @tintPackingViewMode, @numShippingServiceItemID, @numWarehouseID, @numShippingZone, @numDefaultSalesShippingDoc, @tintInvoicingType, @ClientTimeZoneOffset, @tintPackingMode, @numPageIndex;
	END
	ELSE
	BEGIN
		SET @vcSQLFinal = CONCAT('

		INSERT INTO #TEMPResult
		(
			numOppID
			,numDivisionID
			,numTerID
			,numOppItemID
			,numOppBizDocID
			,bintCreatedDate
			,numItemCode
			,vcCompanyName
			,vcPoppName
			,numAssignedTo
			,numRecOwner
			,vcBizDocID
			,numStatus
			,txtComments
			,numBarCodeId
			,vcLocation
			,vcItemName
			,vcItemDesc
			,numQtyShipped
			,numUnitHour
			,monAmountPaid
			,numItemGroup
			,numItemClassification
			,vcSKU
			,charItemType
			,vcAttributes
			,vcNotes
			,vcItemReleaseDate
			,dtItemReceivedDate
			,intUsedShippingCompany
			,intShippingCompany
			,numPreferredShipVia
			,numPreferredShipService
			,vcInvoiced
			,vcInclusionDetails
			,bitPaidInFull
			,numAge
			,dtAnticipatedDelivery
			,vcShipStatus
			,numRemainingQty
			,dtExpectedDate
			,dtExpectedDateOrig
			,numQtyPicked
			,numQtyPacked
			,vcPaymentStatus
			,numShipRate
			,vcShippingLabel
		)
		SELECT
			OpportunityMaster.numOppId
			,OpportunityMaster.numDivisionID
			,ISNULL(DivisionMaster.numTerID,0) numTerID
			,OpportunityItems.numoppitemtCode
			,TEMPOrder.numOppBizDocID
			,dbo.FormatedDateFromDate(DATEADD(MINUTE,@ClientTimeZoneOffset * -1,OpportunityMaster.bintCreatedDate),@numDomainID)
			,Item.numItemCode
			,ISNULL(CompanyInfo.vcCompanyName,'''')
			,ISNULL(OpportunityMaster.vcPoppName,'''')
			,dbo.fn_GetContactName(OpportunityMaster.numAssignedTo)
			,dbo.fn_GetContactName(OpportunityMaster.numRecOwner)
			,ISNULL(OpportunityBizDocs.vcBizDocID,'''')
			,dbo.GetListIemName(OpportunityMaster.numStatus)
			,ISNULL(OpportunityMaster.txtComments,'''')
			,ISNULL(Item.numBarCodeId,'''')
			,ISNULL(WarehouseLocation.vcLocation,'''')
			,ISNULL(Item.vcItemName,'''')
			,ISNULL(OpportunityItems.vcItemDesc,'''')
			,ISNULL(OpportunityItems.numQtyShipped,0)
			,ISNULL(OpportunityItems.numUnitHour,0)
			,ISNULL(TempPaid.monAmountPaid,0)
			,ISNULL(ItemGroups.vcItemGroup,'''')
			,dbo.GetListIemName(Item.numItemClassification)
			,ISNULL(Item.vcSKU,'''')
			,(CASE 
				WHEN Item.charItemType=''P''  THEN ''Inventory Item''
				WHEN Item.charItemType=''N'' THEN ''Non Inventory Item'' 
				WHEN Item.charItemType=''S'' THEN ''Service'' 
				WHEN Item.charItemType=''A'' THEN ''Accessory'' 
			END)
			,ISNULL(OpportunityItems.vcAttributes,'''')
			,ISNULL(OpportunityItems.vcNotes,'''')
			,dbo.FormatedDateFromDate(CAST(OpportunityItems.ItemReleaseDate AS DATETIME),@numDomainID)
			,dbo.FormatedDateFromDate((SELECT MAX(OMInner.dtItemReceivedDate) FROM OpportunityMaster OMInner INNER JOIN OpportunityItems OIInner ON OMInner.numOppID=OIInner.numOppID WHERE OMInner.tintOppType=2 AND OMInner.tintOppStatus=1 AND OIInner.numItemCode=OpportunityItems.numItemCode),@numDomainID)
			,CONCAT(ISNULL(LDOpp.vcData,''-''),'', '',ISNULL(SSOpp.vcShipmentService,''-'')) 
			,CONCAT(ISNULL(LDDiv.vcData,''-''),'', '',ISNULL(SSDiv.vcShipmentService,''-''))
			,ISNULL(DivisionMaster.intShippingCompany,0)
			,ISNULL(DivisionMaster.numDefaultShippingServiceID,0)
			,ISNULL((SELECT SUM(numUnitHour) FROM OpportunityBizDocs OBD INNER JOIN OpportunityBizDocItems OBDI ON OBD.numOppBizDocsId=OBDI.numOppBizDocID  WHERE OBD.numOppId=OpportunityMaster.numOppId AND OBD.numBizDocId=287 AND OBDI.numOppItemID=OpportunityItems.numoppitemtCode),0)
			,ISNULL(OpportunityItems.vcInclusionDetail,'''')
			,(CASE WHEN ISNULL(TempPaid.monAmountPaid,0) >= ISNULL(OpportunityMaster.monDealAmount,0) THEN 1 ELSE 0 END)
			,CONCAT(''<span style="color:'',(CASE
						WHEN (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24) = 0 AND (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) < 4  
						THEN ''#00ff00''
						WHEN (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24) = 0 AND ((DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) >= 4 AND (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) < 8)
						THEN ''#00b050''
						WHEN (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24) = 0 AND ((DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) >= 8 AND (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) < 12)
						THEN ''#3399ff''
						WHEN (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24) = 0 AND ((DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) >= 12 AND (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24) < 24)
						THEN ''#ff9900''
						WHEN (DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24) BETWEEN 1 AND 2  
						THEN ''#9b3596''
						ELSE ''#ff0000''
					END),''">'',CONCAT(DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE())/24,''d '',DateDiff(HOUR,OpportunityMaster.bintCreatedDate,GETUTCDATE()) % 24,''h''),''</span>'')
			,'''' dtAnticipatedDelivery
			,'''' vcShipStatus
			,(CASE 
				WHEN @numViewID = 1
				THEN ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyPicked,0)
				WHEN @numViewID = 2 AND @tintPackingViewMode = 2
				THEN (CASE WHEN ISNULL(@tintPackingMode,1) = 2 THEN ISNULL(OpportunityItems.numQtyPicked,0) - ISNULL(OpportunityItems.numQtyShipped,0) ELSE ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyShipped,0) END)
				WHEN @numViewID = 2 AND @tintPackingViewMode = 3 AND ISNULL(TEMPOrder.numOppBizDocID,0) = 0
				THEN (CASE WHEN ISNULL(@tintPackingMode,1) = 2 THEN ISNULL(OpportunityItems.numQtyPicked,0) ELSE ISNULL(OpportunityItems.numUnitHour,0) END) - ISNULL((SELECT 
																																											SUM(OpportunityBizDocItems.numUnitHour)
																																										FROM
																																											OpportunityBizDocs
																																										INNER JOIN
																																											OpportunityBizDocItems
																																										ON
																																											OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																																										WHERE
																																											OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
																																											AND OpportunityBizDocs.numBizDocId=@numDefaultSalesShippingDoc
					 																																					AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode),0)
				WHEN @numViewID = 2 AND @tintPackingViewMode = 3 AND ISNULL(OpportunityBizDocs.numOppBizDocsId,0) > 0
				THEN ISNULL(OpportunityBizDocItems.numUnitHour,0)
				WHEN @numViewID = 3
				THEN (CASE 
						WHEN ISNULL(@tintInvoicingType,0)=1 
						THEN ISNULL(OpportunityItems.numQtyShipped,0) 
						ELSE ISNULL(OpportunityItems.numUnitHour,0) 
					END) - ISNULL((SELECT 
										SUM(OpportunityBizDocItems.numUnitHour)
									FROM
										OpportunityBizDocs
									INNER JOIN
										OpportunityBizDocItems
									ON
										OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
									WHERE
										OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
										AND OpportunityBizDocs.numBizDocId=287
										AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode),0)
				ELSE 0
			END) numRemainingQty
			,dbo.FormatedDateFromDate(OpportunityMaster.dtExpectedDate,@numDomainID)
			,OpportunityMaster.dtExpectedDate
			,ISNULL(OpportunityItems.numQtyPicked,0)
			,ISNULL(TempInvoiced.numInvoicedQty,0)
			,'''' vcPaymentStatus
			,ISNULL(CAST((SELECT SUM(monTotAmount) FROM OpportunityItems OIInner WHERE OIInner.numOppID=OpportunityMaster.numOppId AND OIInner.numItemCode=@numShippingServiceItemID) AS VARCHAR),'''')
			,(CASE WHEN @numViewID=6 THEN STUFF((SELECT 
													CONCAT('', '',''<a style="text-decoration: none" href="javascript:void(0);" onclick="openReportList('',ShippingReport.numOppID ,'','',ShippingReport.numOppBizDocId,'','',ShippingReport.numShippingReportId,'');"><img src="../images/barcode_generated_22x22.png"></a>'')
												FROM 
													ShippingReport
												INNER JOIN
													ShippingBox 
												ON 
													ShippingReport.numShippingReportId=ShippingBox.numShippingReportId 
												WHERE 
													ShippingReport.numOppID= OpportunityMaster.numOppId
													AND ShippingReport.numOppBizDocId= OpportunityBizDocs.numOppBizDocsId
													AND LEN(ISNULL(vcShippingLabelImage,'''')) > 0
												for xml path('''')),1,2,'''') ELSE '''' END)
		FROM
			#TEMPMSRecords TEMPOrder
		INNER JOIN
			OpportunityMaster
		ON
			TEMPOrder.numOppID = OpportunityMaster.numOppId
		INNER JOIN
			DivisionMaster
		ON
			OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
		INNER JOIN
			CompanyInfo
		ON
			DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
		INNER JOIN 
			OpportunityItems
		ON
			OpportunityMaster.numOppId = OpportunityItems.numOppId
			AND TEMPOrder.numOppItemID = OpportunityItems.numoppitemtCode
		LEFT JOIN
			OpportunityBizDocs 
		ON
			TEMPOrder.numOppId = OpportunityBizDocs.numOppID
			AND TEMPOrder.numOppBizDocID = OpportunityBizDocs.numOppBizDocsId
		LEFT JOIN
			OpportunityBizDocItems
		ON
			OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocId
			AND OpportunityItems.numoppitemtCode = OpportunityBizDocItems.numOppItemID
		INNER JOIN
			Item
		ON
			OpportunityItems.numItemCode = Item.numItemCode
		LEFT JOIN
			ItemGroups
		ON
			Item.numItemGroup = ItemGroups.numItemGroupID
		LEFT JOIN
			ListDetails  LDOpp
		ON
			LDOpp.numListID = 82
			AND (LDOpp.numDomainID = @numDomainID OR LDOpp.constFlag=1)
			AND ISNULL(OpportunityMaster.intUsedShippingCompany,0) = LDOpp.numListItemID
		LEFT JOIN
			ShippingService AS SSOpp
		ON
			(SSOpp.numDomainID=@numDomainID OR ISNULL(SSOpp.numDomainID,0)=0) 
			AND OpportunityMaster.numShippingService = SSOpp.numShippingServiceID
		LEFT JOIN
			ListDetails  LDDiv
		ON
			LDDiv.numListID = 82
			AND (LDDiv.numDomainID = @numDomainID OR LDDiv.constFlag=1)
			AND ISNULL(DivisionMaster.intShippingCompany,0) = LDDiv.numListItemID
		LEFT JOIN
			ShippingService AS SSDiv
		ON
			(SSDiv.numDomainID=@numDomainID OR ISNULL(SSDiv.numDomainID,0)=0) 
			AND ISNULL(DivisionMaster.numDefaultShippingServiceID,0) = SSDiv.numShippingServiceID
		LEFT JOIN
			WareHouseItems
		ON
			OpportunityItems.numWarehouseItmsID = WareHouseItems.numWareHouseItemID
		LEFT JOIN
			WarehouseLocation
		ON
			WareHouseItems.numWLocationID = WarehouseLocation.numWLocationID
		OUTER APPLY
		(
			SELECT 
				SUM(OpportunityBizDocItems.numUnitHour) numInvoicedQty
			FROM 
				OpportunityBizDocs
			INNER JOIN
				OpportunityBizDocItems
			ON
				OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
			WHERE 
				OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
				AND OpportunityBizDocs.numBizDocId=@numDefaultSalesShippingDoc
				AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode
		) TempInvoiced
		OUTER APPLY
		(
			SELECT 
				SUM(OpportunityBizDocs.monAmountPaid) monAmountPaid
			FROM 
				OpportunityBizDocs 
			WHERE 
				OpportunityBizDocs.numOppId=OpportunityMaster.numOppId 
				AND OpportunityBizDocs.numBizDocId=287
		) TempPaid
		WHERE
			1 = (CASE WHEN @numViewID = 2 AND @tintPackingViewMode = 3 THEN (CASE WHEN ISNULL(Item.bitContainer,0)=1 THEN 0 ELSE 1 END) ELSE 1 END)
			AND 1 = (CASE WHEN ISNULL(TEMPOrder.numOppBizDocID,0) > 0 THEN (CASE WHEN OpportunityBizDocItems.numOppItemID > 0 THEN 1 ELSE 0 END) ELSE 1 END)',
			(CASE WHEN @numViewID=2 AND @tintPackingViewMode=3 THEN ' AND 1 = (CASE WHEN ISNULL(OpportunityItems.numWarehouseItmsID,0) > 0 AND ISNULL(OpportunityItems.bitDropShip,0) = 0 THEN 1 ELSE 0 END)' ELSE '' END),'
		ORDER BY ',
		(CASE @vcSortColumn
			WHEN 'CompanyInfo.vcCompanyName' THEN 'CompanyInfo.vcCompanyName' 
			WHEN 'Item.charItemType' THEN '(CASE 
												WHEN Item.charItemType=''P''  THEN ''Inventory Item''
												WHEN Item.charItemType=''N'' THEN ''Non Inventory Item'' 
												WHEN Item.charItemType=''S'' THEN ''Service''
												WHEN Item.charItemType=''A'' THEN ''Accessory''
											END)'
			WHEN 'Item.numBarCodeId' THEN 'Item.numBarCodeId'
			WHEN 'Item.numItemClassification' THEN 'dbo.GetListIemName(Item.numItemClassification)'
			WHEN 'Item.numItemGroup' THEN 'ISNULL(ItemGroups.vcItemGroup,'''')'
			WHEN 'Item.vcSKU' THEN 'Item.vcSKU'
			WHEN 'OpportunityItems.vcItemName' THEN 'Item.vcItemName'
			WHEN 'OpportunityMaster.numStatus' THEN 'dbo.GetListIemName(OpportunityMaster.numStatus)'
			WHEN 'OpportunityMaster.vcPoppName' THEN 'OpportunityMaster.vcPoppName'
			WHEN 'OpportunityMaster.numAssignedTo' THEN 'dbo.fn_GetContactName(OpportunityMaster.numAssignedTo)'
			WHEN 'OpportunityMaster.numRecOwner' THEN 'dbo.fn_GetContactName(OpportunityMaster.numRecOwner)'
			WHEN 'WareHouseItems.vcLocation' THEN 'WarehouseLocation.vcLocation'
			WHEN 'OpportunityBizDocs.monAmountPaid' THEN 'TempPaid.monAmountPaid'
			WHEN 'OpportunityBizDocs.vcBizDocID' THEN 'LEN(OpportunityBizDocs.vcBizDocID) DESC, OpportunityBizDocs.vcBizDocID'
			WHEN 'OpportunityItems.numQtyPacked' THEN 'ISNULL((SELECT SUM(numUnitHour) FROM OpportunityBizDocs OBD INNER JOIN OpportunityBizDocItems OBDI ON OBD.numOppBizDocsId=OBDI.numOppBizDocID  WHERE OBD.numOppId=OpportunityMaster.numOppId AND OBD.numBizDocId=29397 AND OBDI.numOppItemID=OpportunityItems.numoppitemtCode),0)'
			WHEN 'OpportunityItems.numQtyPicked' THEN 'OpportunityItems.numQtyPicked'
			WHEN 'OpportunityItems.numQtyShipped' THEN 'OpportunityItems.numQtyShipped'
			WHEN 'OpportunityItems.numRemainingQty' THEN '(CASE 
																WHEN @numViewID = 1
																THEN ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyPicked,0)
																WHEN @numViewID = 2
																THEN ISNULL(OpportunityItems.numUnitHour,0) - ISNULL((SELECT SUM(numUnitHour) FROM OpportunityBizDocs OBD INNER JOIN OpportunityBizDocItems OBDI ON OBD.numOppBizDocsId=OBDI.numOppBizDocID  WHERE OBD.numOppId=OpportunityMaster.numOppId AND OBD.numBizDocId=29397 AND OBDI.numOppItemID=OpportunityItems.numoppitemtCode),0)
																ELSE 0
															END)'
			WHEN 'OpportunityItems.numUnitHour' THEN 'OpportunityItems.numUnitHour'
			WHEN 'OpportunityItems.vcInvoiced' THEN 'ISNULL((SELECT SUM(numUnitHour) FROM OpportunityBizDocs OBD INNER JOIN OpportunityBizDocItems OBDI ON OBD.numOppBizDocsId=OBDI.numOppBizDocID  WHERE OBD.numOppId=OpportunityMaster.numOppId AND OBD.numBizDocId=287 AND OBDI.numOppItemID=OpportunityItems.numoppitemtCode),0)'
			WHEN 'OpportunityMaster.dtExpectedDate' THEN 'OpportunityMaster.dtExpectedDate'
			WHEN 'OpportunityItems.vcItemReleaseDate' THEN 'OpportunityItems.ItemReleaseDate'
			WHEN 'OpportunityMaster.bintCreatedDate' THEN 'OpportunityMaster.bintCreatedDate'
			WHEN 'OpportunityMaster.numAge' THEN 'OpportunityMaster.bintCreatedDate'
			WHEN 'OpportunityMaster.bitPaidInFull' THEN '(CASE WHEN ISNULL(TempPaid.monAmountPaid,0) >= ISNULL(OpportunityMaster.monDealAmount,0) THEN 1 ELSE 0 END)'
			WHEN 'OpportunityMaster.dtItemReceivedDate' THEN '(SELECT MAX(OMInner.dtItemReceivedDate) FROM OpportunityMaster OMInner INNER JOIN OpportunityItems OIInner ON OMInner.numOppID=OIInner.numOppID WHERE OMInner.tintOppType=2 AND OMInner.tintOppStatus=1 AND OIInner.numItemCode=OpportunityItems.numItemCode)'
			ELSE 'OpportunityMaster.bintCreatedDate'
		END),' ',(CASE WHEN ISNULL(@vcSortOrder,'') <> '' THEN @vcSortOrder ELSE 'ASC' END),' OFFSET (@numPageIndex-1) * 200 ROWS FETCH NEXT 200 ROWS ONLY')

		PRINT CAST(@vcSQLFinal AS NTEXT)

		EXEC sp_executesql @vcSQLFinal, N'@numDomainID NUMERIC(18,0), @vcOrderStauts VARCHAR(MAX), @vcFilterValue VARCHAR(MAX), @tintFilterBy TINYINT, @numViewID NUMERIC(18,0), @tintPackingViewMode TINYINT, @numShippingServiceItemID NUMERIC(18,0), @numWarehouseID NUMERIC(18,0), @numShippingZone NUMERIC(18,0), @numDefaultSalesShippingDoc NUMERIC(18,0), @tintInvoicingType TINYINT, @ClientTimeZoneOffset INT, @tintPackingMode TINYINT, @numPageIndex INT', @numDomainID, @vcOrderStauts, @vcFilterValue, @tintFilterBy, @numViewID, @tintPackingViewMode, @numShippingServiceItemID, @numWarehouseID, @numShippingZone, @numDefaultSalesShippingDoc, @tintInvoicingType, @ClientTimeZoneOffset, @tintPackingMode, @numPageIndex;
	END

	SET @vcSQLFinal = CONCAT('SELECT T1.*',@vcCustomFields,' FROM #TEMPResult T1',@vcCustomWhere)

	EXEC sp_executesql @vcSQLFinal

	SELECT * FROM @TempFieldsLeft ORDER BY intRowNum

	IF OBJECT_ID('tempdb..#TEMPMSRecords') IS NOT NULL DROP TABLE #TEMPMSRecords
	IF OBJECT_ID('tempdb..#TEMPResult') IS NOT NULL DROP TABLE #TEMPResult
END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_MassSalesFulfillment_GetRecordsForPicking')
DROP PROCEDURE dbo.USP_MassSalesFulfillment_GetRecordsForPicking
GO

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_MassSalesFulfillment_GetRecordsForPicking]
(
	@numDomainID NUMERIC(18,0)
	,@numUserCntID NUMERIC(18,0)
	,@bitGroupByOrder BIT
	,@numBatchID NUMERIC(18,0)
	,@vcSelectedRecords VARCHAR(MAX)
)
AS
BEGIN
	DECLARE @bitGeneratePickListByOrder BIT = 0

	IF EXISTS (SELECT ID FROM MassSalesFulfillmentConfiguration WHERE numDomainID=@numDomainID AND numUserCntID = @numUserCntID)
	BEGIN
		SELECT @bitGeneratePickListByOrder=ISNULL(bitGeneratePickListByOrder,0) FROM MassSalesFulfillmentConfiguration WHERE numDomainID=@numDomainID AND numUserCntID = @numUserCntID
	END
	

	DECLARE @TempFieldsRight TABLE
	(
		ID VARCHAR(50)
		,numFieldID NUMERIC(18,0)
		,vcFieldName VARCHAR(100)
		,vcOrigDbColumnName VARCHAR(100)
		,bitAllowSorting BIT
		,bitAllowFiltering BIT
		,vcAssociatedControlType VARCHAR(20)
		,bitCustomField BIT
		,intRowNum INT
		,intColumnWidth FLOAT
	)

	INSERT INTO @TempFieldsRight
	(
		ID
		,numFieldID
		,vcFieldName
		,vcOrigDbColumnName
		,bitAllowSorting
		,bitAllowFiltering
		,vcAssociatedControlType
		,bitCustomField
		,intRowNum
		,intColumnWidth
	)
	SELECT
		CONCAT(142,'~',DFM.numFieldID,'~',0)
		,DFM.numFieldID
		,DFFM.vcFieldName
		,DFM.vcOrigDbColumnName
		,DFFM.bitAllowSorting
		,DFFM.bitAllowFiltering
		,DFM.vcAssociatedControlType
		,0
		,DFCD.intRowNum
		,ISNULL(DFCD.intColumnWidth,0)
	FROM
		DycFormConfigurationDetails DFCD
	INNER JOIN
		DycFormField_Mapping DFFM
	ON
		DFCD.numFormId = DFFM.numFormID
		AND DFCD.numFieldId = DFFM.numFieldID
	INNER JOIN
		DycFieldMaster DFM
	ON
		DFFM.numFieldID = DFM.numFieldId
	WHERE 
		DFCD.numDomainId=@numDomainID 
		AND DFCD.numUserCntID=@numUserCntID 
		AND DFCD.numFormId=142 
		AND ISNULL(DFFM.bitDeleted,0) = 0
		AND ISNULL(DFFM.bitSettingField,0) = 1
		AND ISNULL(DFCD.bitCustom,0) = 0

	IF (SELECT COUNT(*) FROM @TempFieldsRight) = 0
	BEGIN
		INSERT INTO @TempFieldsRight
		(
			ID
			,numFieldID
			,vcFieldName
			,vcOrigDbColumnName
			,bitAllowSorting
			,bitAllowFiltering
			,vcAssociatedControlType
			,bitCustomField
			,intRowNum
			,intColumnWidth
		)
		SELECT
			CONCAT(142,'~',DFM.numFieldID,'~',0)
			,DFM.numFieldID
			,DFFM.vcFieldName
			,DFM.vcOrigDbColumnName
			,DFFM.bitAllowSorting
			,DFFM.bitAllowFiltering
			,DFM.vcAssociatedControlType
			,0
			,DFFM.[order]
			,100
		FROM
			DycFormField_Mapping DFFM
		INNER JOIN
			DycFieldMaster DFM
		ON
			DFFM.numFieldID = DFM.numFieldId
		WHERE 
			DFFM.numFormId=142 
			AND ISNULL(DFFM.bitDeleted,0) = 0
			AND ISNULL(DFFM.bitSettingField,0) = 1
			AND (ISNULL(DFFM.bitDefault,0) = 1 OR ISNULL(DFFM.bitRequired,0) = 1)
	END
	ELSE
	BEGIN
		INSERT INTO @TempFieldsRight
		(
			ID
			,numFieldID
			,vcFieldName
			,vcOrigDbColumnName
			,bitAllowSorting
			,bitAllowFiltering
			,vcAssociatedControlType
			,bitCustomField
			,intRowNum
			,intColumnWidth
		)
		SELECT
			CONCAT(142,'~',DFM.numFieldID,'~',0)
			,DFM.numFieldID
			,DFFM.vcFieldName
			,DFM.vcOrigDbColumnName
			,DFFM.bitAllowSorting
			,DFFM.bitAllowFiltering
			,DFM.vcAssociatedControlType
			,0
			,DFFM.[order]
			,100
		FROM
			DycFormField_Mapping DFFM
		INNER JOIN
			DycFieldMaster DFM
		ON
			DFFM.numFieldID = DFM.numFieldId
		WHERE 
			DFFM.numFormId=142 
			AND ISNULL(DFFM.bitDeleted,0) = 0
			AND ISNULL(DFFM.bitSettingField,0) = 1
			AND ISNULL(DFFM.bitRequired,0) = 1
			AND DFM.numFieldId NOT IN (SELECT T1.numFieldID FROM @TempFieldsRight T1)
	END


	DECLARE @TEMP TABLE
	(
		ID INT IDENTITY(1,1)
		,numOppID NUMERIC(18,0)
		,numOppItemID NUMERIC(18,0)
	)

	IF ISNULL(@numBatchID,0) > 0
	BEGIN
		INSERT INTO @TEMP
		(
			numOppID
			,numOppItemID
		)
		SELECT
			numOppID
			,ISNULL(numOppItemID,0)
		FROM
			MassSalesFulfillmentBatchOrders
		WHERE
			numBatchID=@numBatchID
	END
	ELSE
	BEGIN
		INSERT INTO @TEMP
		(
			numOppID
			,numOppItemID
		)
		SELECT
			CAST(SUBSTRING(OutParam,0,CHARINDEX('-',OutParam)) AS NUMERIC(18,0)) numOppID
			,CAST(SUBSTRING(OutParam,CHARINDEX('-',OutParam) + 1,LEN(OutParam)) AS NUMERIC(18,0)) numOppItemID
		FROM
			dbo.SplitString(@vcSelectedRecords,',')
	END

	DECLARE @TEMPItems TABLE
	(	
		ID NUMERIC(18,0)
		,numOppID NUMERIC(18,0)
		,numOppItemID NUMERIC(18,0)
		,numItemCode NUMERIC(18,0)
		,numWarehouseID NUMERIC(18,0)
		,numWareHouseItemID NUMERIC(18,0)
		,vcWarehouse VARCHAR(300)
		,vcLocation VARCHAR(300)
		,vcKitChildItems VARCHAR(MAX)
		,vcKitChildKitItems VARCHAR(MAX)
		,vcPoppName VARCHAR(300)
		,numBarCodeId VARCHAR(300)
		,vcItemName VARCHAR(300)
		,vcItemDesc VARCHAR(MAX)
		,bitSerial BIT
		,bitLot BIT
		,vcSKU VARCHAR(300)
		,vcPathForTImage VARCHAR(MAX)
		,vcInclusionDetails VARCHAR(MAX)
		,numRemainingQty FLOAT
		,numQtyPicked FLOAT
	)

	INSERT INTO @TEMPItems
	(	
		ID
		,numOppID
		,numOppItemID 
		,numItemCode
		,numWarehouseID
		,numWareHouseItemID
		,vcWarehouse 
		,vcLocation
		,vcKitChildItems
		,vcKitChildKitItems
		,vcPoppName
		,numBarCodeId
		,vcItemName
		,vcItemDesc
		,bitSerial
		,bitLot
		,vcSKU
		,vcPathForTImage
		,vcInclusionDetails
		,numRemainingQty
		,numQtyPicked
	)
	SELECT 
		T1.ID
		,OpportunityMaster.numOppID
		,OpportunityItems.numoppitemtCode
		,OpportunityItems.numItemCode
		,WareHouseItems.numWareHouseID
		,WareHouseItems.numWareHouseItemID
		,Warehouses.vcWareHouse
		,WarehouseLocation.vcLocation
		,STUFF((SELECT CONCAT(', ',OpportunityItems.numItemCode,'-',OKI.numChildItemID) FROM OpportunityKitItems OKI WHERE OKI.numOppItemID=1 ORDER BY OKI.numChildItemID FOR XML PATH('')),1,1,'') AS vcKitChildItems
		,STUFF((SELECT CONCAT(', ',OpportunityItems.numItemCode,'-',OKI.numChildItemID,'-',OKCI.numItemID) FROM OpportunityKitChildItems OKCI INNER JOIN OpportunityKitItems OKI ON OKCI.numOppChildItemID = OKI.numOppChildItemID  WHERE OKCI.numOppItemID=1 ORDER BY OKI.numChildItemID,OKCI.numItemID FOR XML PATH('')),1,1,'') vcKitChildKitItems
		,OpportunityMaster.vcPoppName
		,Item.numBarCodeId
		,Item.vcItemName
		,Item.txtItemDesc AS vcItemDesc
		,ISNULL(Item.bitSerialized,0)
		,ISNULL(Item.bitLotNo,0)
		,Item.vcSKU
		,ISNULL(ItemImages.vcPathForTImage,'') vcPathForTImage
		,ISNULL(OpportunityItems.vcInclusionDetail,'') AS vcInclusionDetails
		,ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyPicked,0) numRemainingQty
		,0 numQtyPicked
	FROM
		@TEMP T1
	INNER JOIN
		OpportunityMaster
	ON
		OpportunityMaster.numOppID= T1.numOppID
	INNER JOIN
		OpportunityItems
	ON
		OpportunityMaster.numOppID = OpportunityItems.numOppID
		AND (OpportunityItems.numOppItemtcode = T1.numOppItemID OR ISNULL(T1.numOppItemID,0) = 0)
	INNER JOIN
		Item 
	ON
		OpportunityItems.numItemCode = Item.numItemCode
	LEFT JOIN
		ItemImages
	ON
		ItemImages.numDomainId=@numDomainID
		AND ItemImages.numItemCode = Item.numItemCode
		AND ItemImages.bitDefault=1 
		AND ItemImages.bitIsImage=1
	INNER JOIN
		WareHouseItems
	ON
		OpportunityItems.numWarehouseItmsID = WareHouseItems.numWareHouseItemID
	INNER JOIN
		Warehouses
	ON
		WareHouseItems.numWareHouseID = Warehouses.numWareHouseID
	LEFT JOIN
		WarehouseLocation
	ON
		WareHouseItems.numWLocationID = WarehouseLocation.numWLocationID
	WHERE
		OpportunityMaster.numDomainID=@numDomainID
		AND ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyPicked,0) > 0
	ORDER BY
		T1.ID, OpportunityItems.numSortOrder

	IF @bitGeneratePickListByOrder = 1
	BEGIN
		SELECT
			TEMP.numOppId
			,TEMP.numOppItemID 
			,TEMP.numItemCode
			,TEMP.numWareHouseID
			,TEMP.vcKitChildItems
			,TEMP.vcKitChildKitItems
			,CONCAT(TEMP.numOppItemID,'(',TEMP.numRemainingQty,')') vcOppItemIDs
			,vcPoppName
			,numBarCodeId
			,CONCAT(TEMP.vcWarehouse, (CASE WHEN LEN(ISNULL(TEMP.vcLocation,'')) > 0 THEN CONCAT(' (',TEMP.vcLocation,')') ELSE '' END)) vcLocation
			,vcItemName
			,vcItemDesc
			,bitSerial
			,bitLot
			,vcSKU
			,dbo.fn_GetItemAttributes(@numDomainID,TEMP.numItemCode) vcAttributes
			,vcPathForTImage
			,TEMP.vcInclusionDetails
			,TEMP.numRemainingQty
			,0 numQtyPicked
			,(CASE 
					WHEN EXISTS (SELECT numWareHouseItemID FROM WareHouseItems WIInner INNER JOIN WarehouseLocation WL ON WIInner.numWLocationID=WL.numWLocationID WHERE WIInner.numDomainID=@numDomainID AND WIInner.numItemID=TEMP.numItemCode AND WIInner.numWareHouseID = TEMP.numWareHouseID)
					THEN CONCAT('[',STUFF((SELECT 
												CONCAT(', {"WarehouseItemID":',WIInner.numWareHouseItemID,', "Available":',(ISNULL(WIInner.numOnHand,0) + ISNULL(WIInner.numAllocation,0)),', "Location":"',WL.vcLocation,'"}') 
											FROM 
												WareHouseItems WIInner
											LEFT JOIN
												WarehouseLocation WL
											ON
												WIInner.numWLocationID = WL.numWLocationID
											WHERE 
												WIInner.numDomainID=@numDomainID 
												AND WIInner.numItemID=TEMP.numItemCode 
												AND WIInner.numWareHouseID = TEMP.numWareHouseID
												AND (ISNULL(WL.numWLocationID,0) > 0 OR WIInner.numWareHouseItemID=TEMP.numWareHouseItemID)
											ORDER BY
												WL.vcLocation
											FOR XML PATH('')),1,1,''),']')
					ELSE CONCAT('[',STUFF((SELECT 
										CONCAT(', {"WarehouseItemID":',WIInner.numWareHouseItemID,', "Available":',(ISNULL(WIInner.numOnHand,0) + ISNULL(WIInner.numAllocation,0)),', "Location":""}')
									FROM 
										WareHouseItems WIInner
									WHERE 
										WIInner.numDomainID=@numDomainID 
										AND WIInner.numItemID=TEMP.numItemCode
										AND WIInner.numWareHouseID = TEMP.numWareHouseID
									FOR XML PATH('')),1,1,''),']')
				END)  vcWarehouseLocations
		FROM
			@TEMPItems TEMP
		ORDER BY
			ID
	END
	ELSE
	BEGIN
		IF ISNULL(@bitGroupByOrder,0) = 1
		BEGIN
			SELECT
				TEMP.numOppId
				,0  AS numOppItemID
				,TEMP.numItemCode
				,TEMP.numWareHouseID
				,TEMP.vcKitChildItems
				,TEMP.vcKitChildKitItems
				,STUFF((SELECT 
							CONCAT(',',T1.numOppItemID,'(',T1.numRemainingQty,')') 
						FROM 
							@TEMPItems T1 
						WHERE 
							T1.numOppId = TEMP.numOppId
							AND T1.numItemCode = TEMP.numItemCode
							AND T1.numWareHouseID = TEMP.numWareHouseID
							AND ISNULL(T1.vcKitChildItems,'') = ISNULL(TEMP.vcKitChildItems,'')
							AND ISNULL(T1.vcKitChildKitItems,'') = ISNULL(TEMP.vcKitChildKitItems ,'')
						FOR XML PATH('')),1,1,'') vcOppItemIDs
				,vcPoppName
				,numBarCodeId
				,CONCAT(TEMP.vcWarehouse, (CASE WHEN LEN(ISNULL(TEMP.vcLocation,'')) > 0 THEN CONCAT(' (',TEMP.vcLocation,')') ELSE '' END)) vcLocation
				,vcItemName
				,vcItemDesc
				,bitSerial
				,bitLot
				,vcSKU
				,dbo.fn_GetItemAttributes(@numDomainID,TEMP.numItemCode) vcAttributes
				,vcPathForTImage
				,MIN(vcInclusionDetails) vcInclusionDetails
				,SUM(TEMP.numRemainingQty) numRemainingQty
				,0 numQtyPicked
				,(CASE 
					WHEN EXISTS (SELECT numWareHouseItemID FROM WareHouseItems WIInner INNER JOIN WarehouseLocation WL ON WIInner.numWLocationID=WL.numWLocationID WHERE WIInner.numDomainID=@numDomainID AND WIInner.numItemID=TEMP.numItemCode AND WIInner.numWareHouseID = TEMP.numWareHouseID)
					THEN CONCAT('[',STUFF((SELECT 
												CONCAT(', {"WarehouseItemID":',WIInner.numWareHouseItemID,', "Available":',(ISNULL(WIInner.numOnHand,0) + ISNULL(WIInner.numAllocation,0)),', "Location":"',WL.vcLocation,'"}') 
											FROM 
												WareHouseItems WIInner
											LEFT JOIN
												WarehouseLocation WL
											ON
												WIInner.numWLocationID = WL.numWLocationID
											WHERE 
												WIInner.numDomainID=@numDomainID 
												AND WIInner.numItemID=TEMP.numItemCode 
												AND WIInner.numWareHouseID = TEMP.numWareHouseID
												AND (ISNULL(WL.numWLocationID,0) > 0 OR WIInner.numWareHouseItemID=TEMP.numWareHouseItemID)
											ORDER BY
												WL.vcLocation
											FOR XML PATH('')),1,1,''),']')
					ELSE CONCAT('[',STUFF((SELECT 
										CONCAT(', {"WarehouseItemID":',WIInner.numWareHouseItemID,', "Available":',(ISNULL(WIInner.numOnHand,0) + ISNULL(WIInner.numAllocation,0)),', "Location":""}')
									FROM 
										WareHouseItems WIInner
									WHERE 
										WIInner.numDomainID=@numDomainID 
										AND WIInner.numItemID=TEMP.numItemCode
										AND WIInner.numWareHouseID = TEMP.numWareHouseID
									FOR XML PATH('')),1,1,''),']')
				END)  vcWarehouseLocations
			FROM
				@TEMPItems TEMP
			GROUP BY
				TEMP.numOppId
				,TEMP.numItemCode
				,TEMP.numWareHouseID
				,TEMP.numWareHouseItemID
				,TEMP.vcKitChildItems
				,TEMP.vcKitChildKitItems
				,TEMP.vcPoppName
				,TEMP.vcPathForTImage
				,TEMP.vcItemName
				,TEMP.bitSerial
				,TEMP.bitLot
				,TEMP.vcItemDesc
				,TEMP.vcSKU
				,TEMP.numBarCodeId
				,TEMP.vcWarehouse
				,TEMP.vcLocation
			ORDER BY
				MIN(ID)
		END
		ELSE
		BEGIN
			SELECT
				0 AS numOppId
				,0 AS numOppItemID
				,TEMP.numItemCode
				,TEMP.numWareHouseID
				,TEMP.vcKitChildItems
				,TEMP.vcKitChildKitItems
				,STUFF((SELECT 
							CONCAT(', ',T1.numOppItemID,'(',T1.numRemainingQty,')') 
						FROM 
							@TEMPItems T1 
						WHERE 
							T1.numItemCode = TEMP.numItemCode
							AND T1.numWareHouseID = TEMP.numWareHouseID
							AND ISNULL(T1.vcKitChildItems,'') = ISNULL(TEMP.vcKitChildItems,'')
							AND ISNULL(T1.vcKitChildKitItems,'') = ISNULL(TEMP.vcKitChildKitItems ,'')
						FOR XML PATH('')),1,1,'') vcOppItemIDs
				,'' AS vcPoppName
				,numBarCodeId
				,CONCAT(TEMP.vcWarehouse, (CASE WHEN LEN(ISNULL(TEMP.vcLocation,'')) > 0 THEN CONCAT(' (',TEMP.vcLocation,')') ELSE '' END)) vcLocation
				,vcItemName
				,vcItemDesc
				,bitSerial
				,bitLot
				,vcSKU
				,dbo.fn_GetItemAttributes(@numDomainID,TEMP.numItemCode) vcAttributes
				,vcPathForTImage
				,MIN(vcInclusionDetails) vcInclusionDetails
				,SUM(TEMP.numRemainingQty) numRemainingQty
				,0 numQtyPicked
				,(CASE 
					WHEN EXISTS (SELECT numWareHouseItemID FROM WareHouseItems WIInner INNER JOIN WarehouseLocation WL ON WIInner.numWLocationID=WL.numWLocationID WHERE WIInner.numDomainID=@numDomainID AND WIInner.numItemID=TEMP.numItemCode AND WIInner.numWareHouseID = TEMP.numWareHouseID)
					THEN CONCAT('[',STUFF((SELECT 
												CONCAT(', {"WarehouseItemID":',WIInner.numWareHouseItemID,', "Available":',(ISNULL(WIInner.numOnHand,0) + ISNULL(WIInner.numAllocation,0)),', "Location":"',WL.vcLocation,'"}') 
											FROM 
												WareHouseItems WIInner
											LEFT JOIN
												WarehouseLocation WL
											ON
												WIInner.numWLocationID = WL.numWLocationID
											WHERE 
												WIInner.numDomainID=@numDomainID 
												AND WIInner.numItemID=TEMP.numItemCode 
												AND WIInner.numWareHouseID = TEMP.numWareHouseID
												AND (ISNULL(WL.numWLocationID,0) > 0 OR WIInner.numWareHouseItemID=TEMP.numWareHouseItemID)
											ORDER BY
												WL.vcLocation
											FOR XML PATH('')),1,1,''),']')
					ELSE CONCAT('[',STUFF((SELECT 
										CONCAT(', {"WarehouseItemID":',WIInner.numWareHouseItemID,', "Available":',(ISNULL(WIInner.numOnHand,0) + ISNULL(WIInner.numAllocation,0)),', "Location":""}')
									FROM 
										WareHouseItems WIInner
									WHERE 
										WIInner.numDomainID=@numDomainID 
										AND WIInner.numItemID=TEMP.numItemCode
										AND WIInner.numWareHouseID = TEMP.numWareHouseID
									FOR XML PATH('')),1,1,''),']')
				END)  vcWarehouseLocations
			FROM
				@TEMPItems TEMP
			GROUP BY
				TEMP.numItemCode
				,TEMP.numWareHouseID
				,TEMP.numWareHouseItemID
				,TEMP.vcKitChildItems
				,TEMP.vcKitChildKitItems
				,TEMP.vcPathForTImage
				,TEMP.vcItemName
				,TEMP.vcItemDesc
				,TEMP.bitSerial
				,TEMP.bitLot
				,TEMP.vcSKU
				,TEMP.numBarCodeId
				,TEMP.vcWarehouse
				,TEMP.vcLocation
			ORDER BY
				MIN(ID)
		END
	END
	
	SELECT * FROM @TempFieldsRight ORDER BY intRowNum
END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_MassSalesFulfillment_UpdateItemsPickedQty')
DROP PROCEDURE dbo.USP_MassSalesFulfillment_UpdateItemsPickedQty
GO

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_MassSalesFulfillment_UpdateItemsPickedQty]
(
	@numDomainID NUMERIC(18,0)
	,@numUserCntID NUMERIC(18,0)
	,@vcSelectedRecords VARCHAR(MAX)
	,@ClientTimeZoneOffset INT
)
AS
BEGIN
	DECLARE @numStatus NUMERIC(18,0) = 0
	IF EXISTS (SELECT ID FROM MassSalesFulfillmentConfiguration WHERE numDomainID=@numDomainID AND numUserCntID=@numUserCntID)
	BEGIN
		SELECT @numStatus = ISNULL(numOrderStatusPicked,0) FROM MassSalesFulfillmentConfiguration WHERE numDomainID=@numDomainID AND numUserCntID=@numUserCntID
	END

	DECLARE @TempOrders TABLE
	(
		ID INT IDENTITY(1,1)
		,numOppID NUMERIC(18,0)
		,numOppItemID NUMERIC(18,0)
		,numQtyPicked FLOAT
	)

	DECLARE @hDocItem INT

	DECLARE @Temp TABLE
	(
		ID INT IDENTITY(1,1)
		,numPickedQTY FLOAT
		,vcWarehouseItemIDs XML
		,vcOppItems VARCHAR(MAX)
	)

	EXEC sp_xml_preparedocument @hDocItem OUTPUT, @vcSelectedRecords

	INSERT INTO @Temp
	(
		numPickedQty,
		vcWarehouseItemIDs,
		vcOppItems
	)
	SELECT
		numPickedQty,
		WarehouseItemIDs,
		vcOppItems
	FROM
		OPENXML (@hDocItem,'/NewDataSet/Table1',2)
	WITH
	(
		numPickedQty FLOAT,
		WarehouseItemIDs xml,
		vcOppItems VARCHAR(MAX)
	)

	EXEC sp_xml_removedocument @hDocItem 

	DECLARE @TEMPItems TABLE
	(
		ID INT
		,numOppID NUMERIC(18,0)
		,numOppItemID NUMERIC(18,0)
		,bitSerial BIT
		,bitLot BIT
		,numWarehouseItemID NUMERIC(18,0)
		,numQtyLeftToPick FLOAT		
	)

	DECLARE @TEMPWarehouseLocations TABLE
	(
		ID INT
		,numWarehouseItemID NUMERIC(18,0)
		,numPickedQty FLOAT
		,vcSerialLot# VARCHAR(MAX)
	)

	DECLARE @TEMPSerialLot TABLE
	(
		ID INT
		,numWarehouseItemID NUMERIC(18,0)
		,vcSerialNo VARCHAR(300)
		,numQty FLOAT
	)

	DECLARE @TEMPWarehouseDTL TABLE
	(
		ID INT
		,numWareHouseItmsDTLID NUMERIC(18,0)
	)


	DECLARE @i INT = 1
	DECLARE @iCount INT
	DECLARE @j INT = 1
	DECLARE @jCount INT
	DECLARE @x INT = 1
	DECLARE @xCount INT = 0
	DECLARE @y INT = 1
	DECLARE @yCount INT = 0
	DECLARE @numPickedQty FLOAT
	DECLARE @numOppID NUMERIC(18,0)
	DECLARE @numOppItemID NUMERIC(18,0)
	DECLARE @bitSerial BIT
	DECLARE @bitLot BIT
	DECLARE @numWarehouseItemID NUMERIC(18,0)
	DECLARE @numQtyLeftToPick FLOAT
	DECLARE @vcWarehouseItemIDs VARCHAR(4000)
	DECLARE @vcOppItems VARCHAR(MAX)
	DECLARE @numTempWarehouseItemID NUMERIC(18,0)
	DECLARE @numWarehousePickedQty FLOAT
	DECLARE @numFromOnHand FLOAT
	DECLARE @numFromOnOrder FLOAT
	DECLARE @numFromAllocation FLOAT
	DECLARE @numFromBackOrder FLOAT
	DECLARE @numToOnHand FLOAT
	DECLARE @numToOnOrder FLOAT
	DECLARE @numToAllocation FLOAT
	DECLARE @numToBackOrder FLOAT
	DECLARE @vcDescription VARCHAR(300)
	DECLARE @numSerialWarehoueItemID NUMERIC(18,0)
	DECLARE @numMaxID NUMERIC(18,0)
	DECLARE @vcSerialLot# VARCHAR(MAX)
	DECLARE @vcSerialLotNo VARCHAR(200)	
	DECLARE @numSerialLotQty FLOAT
	DECLARE @numSerialLotQtyToPick FLOAT
	DECLARE @numWareHouseItmsDTLID NUMERIC(18,0)

	SET @iCount = (SELECT COUNT(*) FROM @TEMP)

	BEGIN TRY
	BEGIN TRANSACTION

		WHILE @i <= @iCount
		BEGIN
			SELECT
				@numPickedQty=ISNULL(numPickedQty,0)
				,@vcWarehouseItemIDs = CAST(vcWarehouseItemIDs.query('//WarehouseItemIDs/child::*') AS VARCHAR(MAX))
				,@vcOppItems=ISNULL(vcOppItems,'')
			FROM 
				@TEMP 
			WHERE 
				ID=@i

		
			DELETE FROM @TEMPItems
			DELETE FROM @TEMPWarehouseLocations
			DELETE FROM @TEMPSerialLot

			INSERT INTO @TEMPItems
			(
				ID
				,numOppID
				,numOppItemID
				,bitSerial
				,bitLot
				,numWarehouseItemID
				,numQtyLeftToPick
			)
			SELECT 
				ROW_NUMBER() OVER(ORDER BY numOppItemID ASC)
				,OpportunityItems.numOppId
				,TEMP.numOppItemID
				,ISNULL(Item.bitSerialized,0)
				,ISNULL(Item.bitLotNo,0)
				,OpportunityItems.numWarehouseItmsID
				,ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyPicked,0)
			FROM
			(
				SELECT
					CAST(SUBSTRING(OutParam,0,CHARINDEX('(',OutParam)) AS NUMERIC) numOppItemID
				FROM
					dbo.SplitString(@vcOppItems,',')
			) TEMP
			INNER JOIN
				OpportunityItems
			ON
				Temp.numOppItemID = OpportunityItems.numoppitemtCode
			INNER JOIN
				Item
			ON
				OpportunityItems.numItemCode = Item.numItemCode

			IF LEN(ISNULL(@vcWarehouseItemIDs,'')) > 0
			BEGIN
				DECLARE @hDocSerialLotItem int
				EXEC sp_xml_preparedocument @hDocSerialLotItem OUTPUT, @vcWarehouseItemIDs

				INSERT INTO @TEMPWarehouseLocations
				(
					ID
					,numWarehouseItemID
					,numPickedQty
					,vcSerialLot#
				)
				SELECT 
					ROW_NUMBER() OVER(ORDER BY ID DESC)
					,ID
					,ISNULL(Qty,0)
					,ISNULL(SerialLotNo,'') 
				FROM 
					OPENXML (@hDocSerialLotItem, '/NewDataSet/Table1',2)
				WITH 
				(
					ID NUMERIC(18,0),
					Qty FLOAT,
					SerialLotNo VARCHAR(MAX)
				)

				EXEC sp_xml_removedocument @hDocSerialLotItem
			END

			IF ISNULL((SELECT TOP 1 bitSerial FROM @TEMPItems),0) = 1 AND (SELECT COUNT(*) FROM @TEMPWarehouseLocations) > 0
			BEGIN
				SET @x = 1
				SELECT @xCount=COUNT(*) FROM @TEMPWarehouseLocations

				WHILE @x <= @xCount
				BEGIN
					SELECT @numSerialWarehoueItemID=numWarehouseItemID,@vcSerialLot#=ISNULL(vcSerialLot#,'') FROM @TEMPWarehouseLocations WHERE ID=@x
					SET @numMaxID = ISNULL((SELECT MAX(ID) FROM @TEMPSerialLot),0)

					IF @vcSerialLot# <> ''
					BEGIN
						INSERT INTO @TEMPSerialLot
						(
							ID
							,numWarehouseItemID
							,vcSerialNo
							,numQty
						)
						SELECT
							@numMaxID + ROW_NUMBER() OVER(ORDER BY OutParam)
							,@numSerialWarehoueItemID
							,RTRIM(LTRIM(OutParam))
							,1
						FROM
							dbo.SplitString(@vcSerialLot#,',')
					END
					

					SET @x = @x + 1
				END
			END
			ELSE IF ISNULL((SELECT TOP 1 bitLot FROM @TEMPItems),0) = 1
			BEGIN
				SET @x = 1
				SELECT @xCount=COUNT(*) FROM @TEMPWarehouseLocations

				WHILE @x <= @xCount
				BEGIN
					SELECT @numSerialWarehoueItemID=numWarehouseItemID,@vcSerialLot#=ISNULL(vcSerialLot#,'') FROM @TEMPWarehouseLocations WHERE ID=@x
					SET @numMaxID = ISNULL((SELECT MAX(ID) FROM @TEMPSerialLot),0)

					IF @vcSerialLot# <> ''
					BEGIN
						INSERT INTO @TEMPSerialLot
						(
							ID
							,numWarehouseItemID
							,vcSerialNo
							,numQty
						)
						SELECT
							@numMaxID + ROW_NUMBER() OVER(ORDER BY OutParam)
							,@numSerialWarehoueItemID
							,(CASE 
								WHEN PATINDEX('%(%', OutParam) > 0 AND PATINDEX('%)%', OutParam) > 0 AND PATINDEX('%)%',OutParam) > (PATINDEX('%(%',OutParam) + 1)
								THEN RTRIM(LTRIM(SUBSTRING(OutParam,0,PATINDEX('%(%', OutParam))))
								ELSE ''
							END)
							,(CASE 
								WHEN PATINDEX('%(%', OutParam) > 0 AND PATINDEX('%)%', OutParam) > 0 AND PATINDEX('%)%',OutParam) > (PATINDEX('%(%',OutParam) + 1)
								THEN (CASE WHEN ISNUMERIC(SUBSTRING(OutParam,(PATINDEX('%(%', OutParam) + 1),(PATINDEX('%)%', OutParam) - PATINDEX('%(%', OutParam) - 1))) = 1 
										THEN CAST(SUBSTRING(OutParam,(PATINDEX('%(%', OutParam) + 1),(PATINDEX('%)%', OutParam) - PATINDEX('%(%', OutParam) - 1)) AS FLOAT)  
										ELSE -1 
										END) 
								ELSE -1
							END)	
						FROM
							dbo.SplitString(@vcSerialLot#,',')
					END
					

					SET @x = @x + 1
				END
			END

			IF EXISTS (SELECT vcSerialNo FROM @TEMPSerialLot WHERE numQty = -1)
			BEGIN
				RAISERROR('INVALID_SERIALLOT_FORMAT',16,1)
				RETURN
			END
		

			SET @j = 1
			SET @jCount = (SELECT COUNT(*) FROM @TEMPItems)

			WHILE @j <= @jCount
			BEGIN
				SELECT 
					@numOppID=numOppID
					,@numOppItemID=numOppItemID
					,@bitSerial=bitSerial
					,@bitLot=bitLot
					,@numWarehouseItemID=numWarehouseItemID
					,@numQtyLeftToPick=numQtyLeftToPick
				FROM
					@TEMPItems
				WHERE
					ID=@j

				UPDATE
					OpportunityItems
				SET 
					numQtyPicked = ISNULL(numQtyPicked,0) + (CASE WHEN @numPickedQty > @numQtyLeftToPick THEN @numQtyLeftToPick ELSE @numPickedQty END)
				WHERE 
					numoppitemtCode=@numOppItemID

				IF NOT EXISTS (SELECT ID FROM @TempOrders WHERE numOppID=@numOppID AND numOppItemID=@numOppItemID)
				BEGIN
					INSERT INTO @TempOrders
					(
						numOppID
						,numOppItemID
						,numQtyPicked
					)
					VALUES
					(
						@numOppID
						,@numOppItemID
						,(CASE WHEN @numPickedQty > @numQtyLeftToPick THEN @numQtyLeftToPick ELSE @numPickedQty END)
					)
				END
				ELSE
				BEGIN
					UPDATE
						@TempOrders
					SET
						numQtyPicked=ISNULL(numQtyPicked,0) + ISNULL((CASE WHEN @numPickedQty > @numQtyLeftToPick THEN @numQtyLeftToPick ELSE @numPickedQty END),0)
					WHERE
						numOppID=@numOppID
						AND numOppItemID=@numOppItemID
				END

				SET @numPickedQty = @numPickedQty - (CASE WHEN @numPickedQty > @numQtyLeftToPick THEN @numQtyLeftToPick ELSE @numPickedQty END)


				IF EXISTS (SELECT numoppitemtCode FROM OpportunityItems WHERE numoppitemtCode=@numOppItemID AND ISNULL(numQtyPicked,0) > ISNULL(numUnitHour,0))
				BEGIN
					RAISERROR('PICKED_QTY_IS_GREATER_THEN_REMAINING_QTY',16,1)
				END

				IF EXISTS (SELECT ID FROM @TEMPWarehouseLocations WHERE numWarehouseItemID=@numWarehouseItemID)
				BEGIN
					SELECT 
						@numTempWarehouseItemID = numWarehouseItemID
						,@numWarehousePickedQty = ISNULL(numPickedQty,0)
					FROM 
						@TEMPWarehouseLocations
					WHERE 
						numWarehouseItemID=@numWarehouseItemID

					UPDATE 
						@TEMPWarehouseLocations
					SET
						numPickedQty = ISNULL(numPickedQty,0) - (CASE WHEN ISNULL(numPickedQty,0) > @numQtyLeftToPick THEN @numQtyLeftToPick ELSE ISNULL(numPickedQty,0) END)
					WHERE
						numWarehouseItemID=@numTempWarehouseItemID

					IF ISNULL(@bitSerial,0)=1 OR ISNULL(@bitLot,0)=1
					BEGIN
						IF  ISNULL((SELECT 
										SUM(ISNULL(TSL.numQty,0))
									FROM 
										WareHouseItmsDTL WID
									INNER JOIN
										@TEMPSerialLot TSL
									ON
										WID.vcSerialNo = WID.vcSerialNo
									WHERE 
										WID.numWareHouseItemID=@numWarehouseItemID
										AND TSL.numWarehouseItemID = @numWarehouseItemID
										AND ISNULL(TSL.numQty,0) > 0
										AND ISNULL(WID.numQty,0) >= ISNULL(TSL.numQty,0)),0) >= (CASE WHEN @numWarehousePickedQty >= @numQtyLeftToPick THEN @numQtyLeftToPick ELSE @numWarehousePickedQty END)
										

						BEGIN
							SET @x = 1
							SELECT @xCount=COUNT(*) FROM @TEMPSerialLot
							SET @numSerialLotQtyToPick = (CASE WHEN @numWarehousePickedQty >= @numQtyLeftToPick THEN @numQtyLeftToPick ELSE @numWarehousePickedQty END)

							WHILE @x <= @xCount AND @numSerialLotQtyToPick > 0
							BEGIN
								SELECT @vcSerialLotNo=vcSerialNo,@numSerialLotQty=numQty FROM @TEMPSerialLot WHERE ID=@x

								IF ISNULL(@numSerialLotQty,0) > 0
								BEGIN
									IF @bitSerial = 1
									BEGIN
										IF EXISTS (SELECT numWareHouseItmsDTLID FROM WareHouseItmsDTL WHERE numWareHouseItemID=@numWarehouseItemID AND vcSerialNo=@vcSerialLotNo AND numQty>=@numSerialLotQty)
										BEGIN
											SELECT TOP 1
												@numWareHouseItmsDTLID =numWareHouseItmsDTLID
											FROM
												WareHouseItmsDTL 
											WHERE 
												numWareHouseItemID=@numWarehouseItemID 
												AND vcSerialNo=@vcSerialLotNo 
												AND numQty>=@numSerialLotQty

											INSERT INTO OppWarehouseSerializedItem
											(
												numWarehouseItmsDTLID
												,numWarehouseItmsID
												,numOppID
												,numOppItemID
												,numQty
											)
											SELECT
												numWareHouseItmsDTLID
												,numWareHouseItemID
												,@numOppID
												,@numOppItemID
												,@numSerialLotQty
											FROM 
												WareHouseItmsDTL 
											WHERE 
												numWareHouseItmsDTLID=@numWareHouseItmsDTLID

											SET @numSerialLotQtyToPick = @numSerialLotQtyToPick - @numSerialLotQty
											UPDATE @TEMPSerialLot SET numQty = ISNULL(numQty,0) - @numSerialLotQty  WHERE ID=@x
											UPDATE WareHouseItmsDTL SET numQty = ISNULL(numQty,0) - @numSerialLotQty  WHERE numWareHouseItmsDTLID=@numWareHouseItmsDTLID
										END
										ELSE
										BEGIN
											RAISERROR('INVLAID_SERIALLOT_QTY',16,1)
											RETURN
										END
									END
									ELSE IF @bitLot = 1
									BEGIN
										IF ISNULL((SELECT SUM(numQty) FROM WareHouseItmsDTL WHERE numWareHouseItemID=@numWarehouseItemID AND vcSerialNo=@vcSerialLotNo),0) >= @numSerialLotQty
										BEGIN
											DELETE FROM @TEMPWarehouseDTL

											INSERT INTO @TEMPWarehouseDTL
											(
												ID
												,numWareHouseItmsDTLID
											)
											SELECT
												ROW_NUMBER() OVER(ORDER BY numWareHouseItmsDTLID)
												,numWareHouseItmsDTLID
											FROM
												WareHouseItmsDTL 
											WHERE 
												numWareHouseItemID=@numWarehouseItemID 
												AND vcSerialNo=@vcSerialLotNo

											SET @y = 1
											SELECT COUNT(*) FROM @TEMPWarehouseDTL

											WHILE @y <= @yCount AND @numSerialLotQty > 0
											BEGIN
												SELECT @numWareHouseItmsDTLID=numWareHouseItmsDTLID FROM @TEMPWarehouseDTL WHERE ID=@y

												IF ISNULL((SELECT numQty FROM WareHouseItmsDTL WHERE numWareHouseItmsDTLID=@numWareHouseItmsDTLID),0) > @numSerialLotQty
												BEGIN
													INSERT INTO OppWarehouseSerializedItem
													(
														numWarehouseItmsDTLID
														,numWarehouseItmsID
														,numOppID
														,numOppItemID
														,numQty
													)
													SELECT
														numWareHouseItmsDTLID
														,@numWarehouseItemID
														,@numOppID
														,@numOppItemID
														,@numSerialLotQty
													FROM 
														WareHouseItmsDTL 
													WHERE 
														numWareHouseItmsDTLID=@numWareHouseItmsDTLID

													SET @numSerialLotQtyToPick = @numSerialLotQtyToPick - @numSerialLotQty
													SET @numSerialLotQty = 0
													UPDATE @TEMPSerialLot SET numQty = ISNULL(numQty,0) - @numSerialLotQty WHERE ID=@x
													UPDATE WareHouseItmsDTL SET numQty = ISNULL(numQty,0) -  @numSerialLotQty  WHERE numWareHouseItmsDTLID=@numWareHouseItmsDTLID
												END
												ELSE
												BEGIN
													INSERT INTO OppWarehouseSerializedItem
													(
														numWarehouseItmsDTLID
														,numWarehouseItmsID
														,numOppID
														,numOppItemID
														,numQty
													)
													SELECT
														numWareHouseItmsDTLID
														,@numWarehouseItemID
														,@numOppID
														,@numOppItemID
														,numQty
													FROM 
														WareHouseItmsDTL 
													WHERE 
														numWareHouseItmsDTLID=@numWareHouseItmsDTLID

													SET @numSerialLotQtyToPick = @numSerialLotQtyToPick - ISNULL((SELECT numQty FROM WareHouseItmsDTL WHERE numWareHouseItmsDTLID=@numWareHouseItmsDTLID),0)
													SET @numSerialLotQty = @numSerialLotQty - ISNULL((SELECT numQty FROM WareHouseItmsDTL WHERE numWareHouseItmsDTLID=@numWareHouseItmsDTLID),0)
													UPDATE @TEMPSerialLot SET numQty = ISNULL(numQty,0) - ISNULL((SELECT numQty FROM WareHouseItmsDTL WHERE numWareHouseItmsDTLID=@numWareHouseItmsDTLID),0)  WHERE ID=@x
													UPDATE WareHouseItmsDTL SET numQty = 0  WHERE numWareHouseItmsDTLID=@numWareHouseItmsDTLID
												END

												SET @y = @y + 1
											END

											IF ISNULL(@numSerialLotQty,0) > 0
											BEGIN
												RAISERROR('INVLAID_SERIALLOT_QTY',16,1)
												RETURN
											END
										ELSE
										BEGIN
											RAISERROR('INVLAID_SERIALLOT_QTY',16,1)
											RETURN
										END
									END
								END
								END

								SET @x = @x + 1
							END

							IF @numSerialLotQtyToPick > 0
							BEGIN
								RAISERROR('SUFFICIENT_SERIALLOT_QTY_NOT_PICKED',16,1)
								RETURN
							END
						END
						ELSE
						BEGIN
							RAISERROR('INVLAID_SERIALLOT_QTY',16,1)
							RETURN
						END

					END

					SET @numQtyLeftToPick = @numQtyLeftToPick - (CASE WHEN @numWarehousePickedQty >= @numQtyLeftToPick THEN @numQtyLeftToPick ELSE @numWarehousePickedQty END)
				END
				
				IF @numQtyLeftToPick > 0
				BEGIN
					DECLARE @k INT = 1
					DECLARE @kCount INT 

					SELECT @kCount=COUNT(*) FROM @TEMPWarehouseLocations

					WHILE @k <= @kCount
					BEGIN
						SELECT 
							@numTempWarehouseItemID = numWarehouseItemID
							,@numWarehousePickedQty = ISNULL(numPickedQty,0)
						FROM 
							@TEMPWarehouseLocations 
						WHERE 
							ID = @k

						UPDATE 
							@TEMPWarehouseLocations
						SET
							numPickedQty = ISNULL(numPickedQty,0) - (CASE WHEN ISNULL(numPickedQty,0) > @numQtyLeftToPick THEN @numQtyLeftToPick ELSE ISNULL(numPickedQty,0) END)
						WHERE
							numWarehouseItemID=@numTempWarehouseItemID

						IF ISNULL(@bitSerial,0)=1 OR ISNULL(@bitLot,0)=1
						BEGIN
							IF  ISNULL((SELECT 
											SUM(ISNULL(TSL.numQty,0))
										FROM 
											WareHouseItmsDTL WID
										INNER JOIN
											@TEMPSerialLot TSL
										ON
											WID.vcSerialNo = WID.vcSerialNo
										WHERE 
											WID.numWareHouseItemID=@numTempWarehouseItemID
											AND TSL.numWarehouseItemID = @numTempWarehouseItemID
											AND ISNULL(TSL.numQty,0) > 0
											AND ISNULL(WID.numQty,0) >= ISNULL(TSL.numQty,0)),0) >= (CASE WHEN @numWarehousePickedQty >= @numQtyLeftToPick THEN @numQtyLeftToPick ELSE @numWarehousePickedQty END)										
							BEGIN
								SET @x = 1
								SELECT @xCount=COUNT(*) FROM @TEMPSerialLot
								SET @numSerialLotQtyToPick = (CASE WHEN @numWarehousePickedQty >= @numQtyLeftToPick THEN @numQtyLeftToPick ELSE @numWarehousePickedQty END)

								WHILE @x <= @xCount AND @numSerialLotQtyToPick > 0
								BEGIN
									SELECT @vcSerialLotNo=vcSerialNo,@numSerialLotQty=numQty FROM @TEMPSerialLot WHERE ID=@x

									IF ISNULL(@numSerialLotQty,0) > 0
									BEGIN
										IF @bitSerial = 1
										BEGIN
											IF EXISTS (SELECT numWareHouseItmsDTLID FROM WareHouseItmsDTL WHERE numWareHouseItemID=@numTempWarehouseItemID AND vcSerialNo=@vcSerialLotNo AND numQty>=@numSerialLotQty)
											BEGIN
												SELECT TOP 1
													@numWareHouseItmsDTLID =numWareHouseItmsDTLID
												FROM
													WareHouseItmsDTL 
												WHERE 
													numWareHouseItemID=@numTempWarehouseItemID 
													AND vcSerialNo=@vcSerialLotNo 
													AND numQty>=@numSerialLotQty

												INSERT INTO OppWarehouseSerializedItem
												(
													numWarehouseItmsDTLID
													,numWarehouseItmsID
													,numOppID
													,numOppItemID
													,numQty
												)
												SELECT
													numWareHouseItmsDTLID
													,@numWarehouseItemID
													,@numOppID
													,@numOppItemID
													,@numSerialLotQty
												FROM 
													WareHouseItmsDTL 
												WHERE 
													numWareHouseItmsDTLID=@numWareHouseItmsDTLID

												SET @numSerialLotQtyToPick = @numSerialLotQtyToPick - @numSerialLotQty
												UPDATE @TEMPSerialLot SET numQty = ISNULL(numQty,0) - @numSerialLotQty  WHERE ID=@x
												UPDATE WareHouseItmsDTL SET numQty = ISNULL(numQty,0) - @numSerialLotQty,numWareHouseItemID=@numWarehouseItemID  WHERE numWareHouseItmsDTLID=@numWareHouseItmsDTLID
											END
											ELSE
											BEGIN
												RAISERROR('INVLAID_SERIALLOT_QTY',16,1)
												RETURN
											END
										END
										ELSE IF @bitLot = 1
										BEGIN
											IF ISNULL((SELECT SUM(numQty) FROM WareHouseItmsDTL WHERE numWareHouseItemID=@numTempWarehouseItemID AND vcSerialNo=@vcSerialLotNo),0) >= @numSerialLotQty
											BEGIN
												DELETE FROM @TEMPWarehouseDTL

												INSERT INTO @TEMPWarehouseDTL
												(
													ID
													,numWareHouseItmsDTLID
												)
												SELECT
													ROW_NUMBER() OVER(ORDER BY numWareHouseItmsDTLID)
													,numWareHouseItmsDTLID
												FROM
													WareHouseItmsDTL 
												WHERE 
													numWareHouseItemID=@numTempWarehouseItemID 
													AND vcSerialNo=@vcSerialLotNo

												SET @y = 1
												SELECT COUNT(*) FROM @TEMPWarehouseDTL

												WHILE @y <= @yCount AND @numSerialLotQty > 0
												BEGIN
													SELECT @numWareHouseItmsDTLID=numWareHouseItmsDTLID FROM @TEMPWarehouseDTL WHERE ID=@y

													IF ISNULL((SELECT numQty FROM WareHouseItmsDTL WHERE numWareHouseItmsDTLID=@numWareHouseItmsDTLID),0) > @numSerialLotQty
													BEGIN
														INSERT INTO OppWarehouseSerializedItem
														(
															numWarehouseItmsDTLID
															,numWarehouseItmsID
															,numOppID
															,numOppItemID
															,numQty
														)
														SELECT
															numWareHouseItmsDTLID
															,@numWarehouseItemID
															,@numOppID
															,@numOppItemID
															,@numSerialLotQty
														FROM 
															WareHouseItmsDTL 
														WHERE 
															numWareHouseItmsDTLID=@numWareHouseItmsDTLID

														INSERT INTO WareHouseItmsDTL
														(
															numWareHouseItemID
															,vcSerialNo
															,numQty
														)
														VALUES
														(
															@numWarehouseItemID
															,@vcSerialLotNo
															,@numSerialLotQty
														)

														SET @numSerialLotQtyToPick = @numSerialLotQtyToPick - @numSerialLotQty
														SET @numSerialLotQty = 0
														UPDATE @TEMPSerialLot SET numQty = ISNULL(numQty,0) - @numSerialLotQty WHERE ID=@x
														UPDATE WareHouseItmsDTL SET numQty = ISNULL(numQty,0) -  @numSerialLotQty  WHERE numWareHouseItmsDTLID=@numWareHouseItmsDTLID
													END
													ELSE
													BEGIN
														INSERT INTO OppWarehouseSerializedItem
														(
															numWarehouseItmsDTLID
															,numWarehouseItmsID
															,numOppID
															,numOppItemID
															,numQty
														)
														SELECT
															numWareHouseItmsDTLID
															,@numWarehouseItemID
															,@numOppID
															,@numOppItemID
															,numQty
														FROM 
															WareHouseItmsDTL 
														WHERE 
															numWareHouseItmsDTLID=@numWareHouseItmsDTLID

														SET @numSerialLotQtyToPick = @numSerialLotQtyToPick - ISNULL((SELECT numQty FROM WareHouseItmsDTL WHERE numWareHouseItmsDTLID=@numWareHouseItmsDTLID),0)
														SET @numSerialLotQty = @numSerialLotQty - ISNULL((SELECT numQty FROM WareHouseItmsDTL WHERE numWareHouseItmsDTLID=@numWareHouseItmsDTLID),0)
														UPDATE @TEMPSerialLot SET numQty = ISNULL(numQty,0) - ISNULL((SELECT numQty FROM WareHouseItmsDTL WHERE numWareHouseItmsDTLID=@numWareHouseItmsDTLID),0)  WHERE ID=@x
														UPDATE WareHouseItmsDTL SET numWareHouseItemID=@numWarehouseItemID WHERE numWareHouseItmsDTLID=@numWareHouseItmsDTLID
													END

													SET @y = @y + 1
												END

												IF ISNULL(@numSerialLotQty,0) > 0
												BEGIN
													RAISERROR('INVLAID_SERIALLOT_QTY',16,1)
													RETURN
												END
											ELSE
											BEGIN
												RAISERROR('INVLAID_SERIALLOT_QTY',16,1)
												RETURN
											END
										END
									END
									END

									SET @x = @x + 1
								END

								IF @numSerialLotQtyToPick > 0
								BEGIN
									RAISERROR('SUFFICIENT_SERIALLOT_QTY_NOT_PICKED',16,1)
									RETURN
								END
							END
							ELSE
							BEGIN
								RAISERROR('INVLAID_SERIALLOT_QTY',16,1)
								RETURN
							END
						END

						IF @numWarehousePickedQty > 0 AND (@numWarehouseItemID <> @numTempWarehouseItemID)
						BEGIN
							SELECT
								@numFromOnHand=ISNULL(numOnHand,0)
								,@numFromOnOrder=ISNULL(numOnOrder,0)
								,@numFromAllocation=ISNULL(numAllocation,0)
								,@numFromBackOrder=ISNULL(numBackOrder,0)
							FROM
								WareHouseItems
							WHERE
								numWareHouseItemID=@numTempWarehouseItemID

							SELECT
								@numToOnHand=ISNULL(numOnHand,0)
								,@numToOnOrder=ISNULL(numOnOrder,0)
								,@numToAllocation=ISNULL(numAllocation,0)
								,@numToBackOrder=ISNULL(numBackOrder,0)
							FROM
								WareHouseItems
							WHERE
								numWareHouseItemID=@numWarehouseItemID

							IF @numWarehousePickedQty >= @numQtyLeftToPick
							BEGIN
								SET @vcDescription = CONCAT('Items picked Qty(:',@numQtyLeftToPick,')') 

								IF (@numFromOnHand+@numFromAllocation) >= @numQtyLeftToPick
								BEGIN
									IF @numFromOnHand >= @numQtyLeftToPick
									BEGIN
										SET @numFromOnHand = @numFromOnHand - @numQtyLeftToPick
									END
									ELSE 
									BEGIN
										SET @numFromOnHand = 0
										SET @numFromAllocation = @numFromAllocation - (@numQtyLeftToPick - @numFromOnHand)
										SET @numFromBackOrder = @numFromBackOrder + (@numQtyLeftToPick - @numFromOnHand)
									END

									UPDATE @TEMPWarehouseLocations SET numPickedQty = numPickedQty - @numQtyLeftToPick WHERE ID = @k
								END
								ELSE
								BEGIN
									RAISERROR('AVAILABLE_QUANTITY_LESS_THEN_PICKED_QTY',16,1)
								END

								IF @numToBackOrder >= @numQtyLeftToPick
								BEGIN
									SET @numToBackOrder = @numToBackOrder - @numQtyLeftToPick
									SET @numToAllocation = @numToAllocation + @numQtyLeftToPick
								END
								ELSE
								BEGIN
									SET @numToBackOrder = 0
									SET @numToAllocation = @numToAllocation + @numToBackOrder
									SET @numToOnHand = @numToOnHand + (@numQtyLeftToPick - @numToBackOrder)
								END

								SET @numQtyLeftToPick = 0
							END
							ELSE
							BEGIN
								SET @vcDescription = CONCAT('Items picked Qty(:',@numWarehousePickedQty,')') 

								IF (@numFromOnHand+@numFromAllocation) >= @numWarehousePickedQty
								BEGIN
									IF @numFromOnHand >= @numWarehousePickedQty
									BEGIN
										SET @numFromOnHand = @numFromOnHand - @numWarehousePickedQty
									END
									ELSE 
									BEGIN
										SET @numFromOnHand = 0
										SET @numFromAllocation = @numFromAllocation - (@numWarehousePickedQty - @numFromOnHand)
										SET @numFromBackOrder = @numFromBackOrder + (@numWarehousePickedQty - @numFromOnHand)
									END

									UPDATE @TEMPWarehouseLocations SET numPickedQty = numPickedQty - @numWarehousePickedQty WHERE ID = @k
								END
								ELSE
								BEGIN
									RAISERROR('AVAILABLE_QUANTITY_LESS_THEN_PICKED_QTY',16,1)
								END

								IF @numToBackOrder >= @numWarehousePickedQty
								BEGIN
									SET @numToBackOrder = @numToBackOrder - @numWarehousePickedQty
									SET @numToAllocation = @numToAllocation + @numWarehousePickedQty
								END
								ELSE
								BEGIN
									SET @numToBackOrder = 0
									SET @numToAllocation = @numToAllocation + @numToBackOrder
									SET @numToOnHand = @numToOnHand + (@numWarehousePickedQty - @numToBackOrder)
								END
								

								SET @numQtyLeftToPick = @numQtyLeftToPick - @numWarehousePickedQty
							END

							--UPDATE INVENTORY AND WAREHOUSE TRACKING
							EXEC USP_UpdateInventoryAndTracking 
							@numOnHand=@numFromOnHand,
							@numOnAllocation=@numFromAllocation,
							@numOnBackOrder=@numFromBackOrder,
							@numOnOrder=@numFromOnOrder,
							@numWarehouseItemID=@numTempWarehouseItemID,
							@numReferenceID=@numOppID,
							@tintRefType=3,
							@numDomainID=@numDomainID,
							@numUserCntID=@numUserCntID,
							@Description=@vcDescription 

							SET @vcDescription = REPLACE(@vcDescription,'picked','receieved')
							EXEC USP_UpdateInventoryAndTracking 
							@numOnHand=@numToOnHand,
							@numOnAllocation=@numToAllocation,
							@numOnBackOrder=@numToBackOrder,
							@numOnOrder=@numToOnOrder,
							@numWarehouseItemID=@numWarehouseItemID,
							@numReferenceID=@numOppID,
							@tintRefType=3,
							@numDomainID=@numDomainID,
							@numUserCntID=@numUserCntID,
							@Description=@vcDescription 

						END
						--ELSE IF @numWarehousePickedQty > 0
						--BEGIN
						--	IF @numWarehousePickedQty > @numFromAllocation
						--	BEGIN
						--		RAISERROR('AVAILABLE_QUANTITY_LESS_THEN_PICKED_QTY',16,1)
						--	END
						--	ELSE
						--	BEGIN
						--		UPDATE @TEMPWarehouseLocations SET numPickedQty = numPickedQty - @numWarehousePickedQty WHERE ID = @k
						--		SET @numQtyLeftToPick = @numQtyLeftToPick - @numWarehousePickedQty
						--	END
						--END

						IF @numQtyLeftToPick <= 0
							BREAK

						SET @k = @k + 1
					END

					IF ISNULL(@numQtyLeftToPick,0) > 0
					BEGIN
						RAISERROR('INVALID_PICKED_QTY',16,1)
					END
				END

				IF @numPickedQty <= 0
				BEGIN
					BREAK
				END

				SET @j = @j + 1
			END

			IF @numPickedQty > 0
			BEGIN
				RAISERROR('PICKED_QTY_IS_GREATER_THEN_REMAINING_QTY',16,1)
			END

			SET @i = @i + 1
		END

		IF ISNULL(@numStatus,0) > 0
		BEGIN
			UPDATE
				OpportunityMaster
			SET
				numStatus=@numStatus
			WHERE
				numDomainId=@numDomainID
				AND numOppId IN (SELECT DISTINCT OI.numOppId FROM @TEMPItems T1 INNER JOIN OpportunityItems OI ON T1.numOppItemID = OI.numoppitemtCode)
				AND NOT EXISTS (SELECT
									numOppItemtcode
								FROM
									OpportunityItems
								WHERE
									numOppId = OpportunityMaster.numOppId
									AND ISNULL(numWarehouseItmsID,0) > 0
									AND ISNULL(bitDropShip,0) = 0
									AND ISNULL(numUnitHour,0) <> ISNULL(numQtyPicked,0))
		END


		DECLARE @TempPickList TABLE
		(
			ID INT IDENTITY(1,1)
			,numOppID NUMERIC(18,0)
			,vcItems VARCHAR(MAX)
		)

		INSERT INTO @TempPickList
		(
			numOppID
			,vcItems
		)
		SELECT
			numOppID
			,CONCAT('<NewDataSet>',stuff((
					SELECT 
						CONCAT(' <BizDocItems><OppItemID>',numOppItemID,'</OppItemID>','<Quantity>',ISNULL(numQtyPicked,0),'</Quantity>','<Notes></Notes><monPrice>0</monPrice></BizDocItems>')
					FROM 
						@TempOrders TOR
					WHERE 
						TOR.numOppID = TORMain.numOppID
					FOR XML PATH, TYPE).value(N'.[1]', N'nvarchar(max)'),1,1,''),'</NewDataSet>')
		FROM
			@TempOrders TORMain
		GROUP BY
			numOppID


		SET @k = 1
		SELECT @kCount=COUNT(*) FROM @TempPickList
		DECLARE @numTempOppID NUMERIC(18,0)
		DECLARE @vcItems VARCHAR(MAX)
		DECLARE @intUsedShippingCompany NUMERIC(18,0)
		DECLARE @dtFromDate DATETIME
		DECLARE @numBizDocTempID NUMERIC(18,0)

		SELECT TOP 1
			@numBizDocTempID=numBizDocTempID
		FROM
			BizDocTemplate
		WHERE
			numDomainID=@numDomainID
			AND numBizDocID= 29397
		ORDER BY
			ISNULL(bitEnabled,0) DESC
			,ISNULL(bitDefault,0) DESC

		WHILE @k <= @kCount
		BEGIN
			SELECT @numTempOppID=numOppID,@vcItems=vcItems FROM @TempPickList WHERE ID=@k
			
			IF ISNULL(@vcItems,'') <> ''
			BEGIN
				SELECT 
					@intUsedShippingCompany = ISNULL(intUsedShippingCompany,0)
					,@dtFromDate = DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,GETUTCDATE())
				FROM 
					OpportunityMaster
				WHERE
					numOppId=@numTempOppID

				EXEC USP_CreateBizDocs                                  
					 @numTempOppID,                        
					 29397,                        
					 @numUserCntID,                        
					 0,
					 '',
					 1,
					 @vcItems,
					 @intUsedShippingCompany,
					 '',
					 @dtFromDate,
					 0,
					 0,
					 '',
					 0,
					 0,
					 @ClientTimeZoneOffset,
					 0,
					 0,
					 @numBizDocTempID,
					 '',
					 '',
					 0, --(joseph) Added to Get Online Marketplace Sales Tax Percentage
					 0,
					 1,
					 0,
					 0,
					 0,
					 0,
					 NULL,
					 NULL,
					 0,
					 '',
					 0,
					 0,
					 0,
					 0,
					 0,
					 NULL,
					 0
			END

			SET @k = @k + 1
		END
	COMMIT
	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000)
		DECLARE @ErrorNumber INT
		DECLARE @ErrorSeverity INT
		DECLARE @ErrorState INT
		DECLARE @ErrorLine INT
		DECLARE @ErrorProcedure NVARCHAR(200)

		IF @@TRANCOUNT > 0
			ROLLBACK TRANSACTION;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorNumber = ERROR_NUMBER(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE(),
			@ErrorLine = ERROR_LINE(),
			@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

		RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
	END CATCH
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_MirrorOPPBizDocItems' ) 
    DROP PROCEDURE USP_MirrorOPPBizDocItems
GO
CREATE PROCEDURE [dbo].[USP_MirrorOPPBizDocItems]
    (
      @numOppId AS NUMERIC(9) = NULL,
      @numDomainID AS NUMERIC(9) = 0      
    )
AS 
    DECLARE @DivisionID AS NUMERIC(9)      
    DECLARE @tintTaxOperator AS TINYINT   
    DECLARE @tintType AS TINYINT   
    DECLARE @tintOppStatus AS TINYINT   
    
    DECLARE @tintOppType AS TINYINT   
    DECLARE @numBizDocTempID AS NUMERIC(9) ;
      SET @numBizDocTempID = 0
                                                                                                                                                                                                                                                                                                                                          
    SELECT  @DivisionID = numDivisionID, @tintTaxOperator = [tintTaxOperator]  ,
    @tintType = tintOppType,@tintOppStatus=tintOppStatus,
            @tintOppType = tintOppType   ,@numBizDocTempID = ISNULL(numOppBizDocTempID, 0)      
    FROM    OpportunityMaster
    WHERE   numOppId = @numOppId    
                                                                                                                                                                          
--Decimal points to show in bizdoc 
    DECLARE @DecimalPoint AS TINYINT
	DECLARE @tintCommitAllocation TINYINT

    SELECT 
		@DecimalPoint = tintDecimalPoints
		,@tintCommitAllocation=ISNULL(tintCommitAllocation,1)
    FROM 
		Domain
    WHERE 
		numDomainID = @numDomainID
                                                                                          
    DECLARE @strSQL AS VARCHAR(MAX)                                                                                          
    DECLARE @strSQL1 AS VARCHAR(8000)                                       
    DECLARE @strSQLCusFields VARCHAR(1000)                      
    DECLARE @strSQLEmpFlds VARCHAR(500)                                                                         
    SET @strSQLCusFields = ''                                                                                          
    SET @strSQLEmpFlds = ''                                                                                          
                                                   
    DECLARE @intRowNum AS INT                                                  
    DECLARE @numFldID AS VARCHAR(15)                                          
    DECLARE @vcFldname AS VARCHAR(50)                                            
    DECLARE @numBizDocId AS NUMERIC(9)
                                          
    
       IF @tintOppType=1 AND @tintOppStatus=0
			SELECT  @numBizDocID = numListItemID FROM dbo.ListDetails WHERE numListID=27 AND vcData='Sales Opportunity' AND constFlag=1
       ELSE IF @tintOppType=1 AND @tintOppStatus=1
			SELECT  @numBizDocID = numListItemID FROM dbo.ListDetails WHERE numListID=27 AND vcData='Sales Order' AND constFlag=1
       ELSE IF @tintOppType=2 AND @tintOppStatus=0
			SELECT  @numBizDocID = numListItemID FROM dbo.ListDetails WHERE numListID=27 AND vcData='Purchase Opportunity' AND constFlag=1
       ELSE IF @tintOppType=2 AND @tintOppStatus=1
	 		SELECT  @numBizDocID = numListItemID FROM dbo.ListDetails WHERE numListID=27 AND vcData='Purchase Order' AND constFlag=1
                                                                              
    IF @tintType = 1 
        SET @tintType = 7
    ELSE 
        SET @tintType = 8                                                                                      
                                                                                
    SELECT  
		*
    INTO    
		#Temp1
    FROM    
	( 
		SELECT   
			Opp.vcitemname AS vcItemName,
            ISNULL(OBD.numOppBizDocItemID, 0) OppBizDocItemID,
			(CASE 
				WHEN OM.tintOppType = 1 AND OM.tintOppStatus = 1 
				THEN 
					(CASE 
						WHEN (I.charItemType = 'p' AND ISNULL(Opp.bitDropShip, 0) = 0 AND ISNULL(I.bitAsset,0)=0)
						THEN 
							CAST(dbo.CheckOrderItemInventoryStatus(Opp.numItemCode,Opp.numUnitHour,Opp.numoppitemtCode,Opp.numWarehouseItmsID,(CASE WHEN ISNULL(bitKitParent,0) = 1 AND ISNULL(bitAssembly,0) = 1 THEN 0  WHEN ISNULL(bitKitParent,0) = 1 THEN 1 ELSE 0 END)) AS VARCHAR)
						ELSE ''
					END) 
				ELSE ''
			END) vcBackordered,
			ISNULL(WI.numBackOrder,0) AS numBackOrder,
            CASE WHEN charitemType = 'P' THEN 'Product'
                    WHEN charitemType = 'S' THEN 'Service'
            END AS charitemType,
            CASE WHEN charitemType = 'P' THEN 'Inventory'
	                WHEN charitemType = 'N' THEN 'Non-Inventory'
                    WHEN charitemType = 'S' THEN 'Service'
            END AS vcItemType,
			CONVERT(VARCHAR(2500), OBD.vcitemDesc) AS txtItemDesc, 
			opp.numUnitHour AS numOrgTotalUnitHour,
			dbo.fn_UOMConversion(ISNULL(i.numBaseUnit, 0),i.numItemCode, @numDomainID,ISNULL(opp.numUOMId, 0)) * opp.numUnitHour AS numTotalUnitHour,                                   
            dbo.fn_UOMConversion(ISNULL(i.numBaseUnit, 0),i.numItemCode, @numDomainID,ISNULL(opp.numUOMId, 0)) * OBD.numUnitHour AS numUnitHour,
            CONVERT(DECIMAL(20,5), (dbo.fn_UOMConversion(ISNULL(opp.numUOMId, 0), i.numItemCode, @numDomainID, ISNULL(i.numBaseUnit, 0)) * OBD.monPrice)) monPrice,
            CONVERT(VARCHAR, CONVERT(DECIMAL(20,5), OBD.monTotAmount)) Amount,
            OBD.monTotAmount/*Fo calculating sum*/,
            ISNULL(CONVERT(VARCHAR, i.monListPrice), 0) AS monListPrice,
            i.numItemCode AS ItemCode,
            opp.numoppitemtCode,
            L.vcdata AS numItemClassification,
            CASE WHEN bitTaxable = 0 THEN 'No'
                    ELSE 'Yes'
            END AS Taxable,
            numIncomeChartAcntId AS itemIncomeAccount,
            'NI' AS ItemType,
            0 AS numJournalId,
            0 AS numTransactionId,
            ISNULL(Opp.vcModelID, '') vcModelID,
            (CASE WHEN charitemType = 'P' THEN dbo.USP_GetAttributes(OBD.numWarehouseItmsID, bitSerialized) ELSE ISNULL(OBD.vcAttributes,0) END) AS vcAttributes,
            ISNULL(vcPartNo, '') AS vcPartNo,
           (CASE WHEN ISNULL(opp.bitMarkupDiscount,0) = 1 
					THEN 0
					ELSE (ISNULL(OBD.monTotAmtBefDiscount, OBD.monTotAmount) - OBD.monTotAmount ) 
			END)
            AS DiscAmt,		
			(CASE WHEN OBD.numUnitHour = 0 THEN 0 ELSE CONVERT(DECIMAL(20,5),(dbo.fn_UOMConversion(ISNULL(opp.numUOMId, 0), i.numItemCode, @numDomainID, ISNULL(i.numBaseUnit, 0)) * (ISNULL(OBD.monTotAmount,0) / OBD.numUnitHour))) END) As monUnitSalePrice,
            ISNULL(OBD.vcNotes,ISNULL(opp.vcNotes,'')) vcNotes,
            OBD.vcTrackingNo,
            OBD.vcShippingMethod,
            OBD.monShipCost,
            [dbo].[FormatedDateFromDate](OBD.dtDeliveryDate,@numDomainID) dtDeliveryDate,
            Opp.numWarehouseItmsID,
            ISNULL(u.vcUnitName, '') numUOMId,
            ISNULL(Opp.vcManufacturer, '') AS vcManufacturer,
            ISNULL(V.monCost, 0) AS VendorCost,
            Opp.vcPathForTImage,
            i.[fltLength],
            i.[fltWidth],
            i.[fltHeight],
            i.[fltWeight],
            ISNULL(I.numVendorID, 0) numVendorID,
            Opp.bitDropShip AS DropShip,
            SUBSTRING(( SELECT  ' ,' + vcSerialNo
                                + CASE WHEN ISNULL(I.bitLotNo, 0) = 1
                                        THEN ' ('
                                            + CONVERT(VARCHAR(15), oppI.numQty)
                                            + ')'
                                        ELSE ''
                                    END
                        FROM    OppWarehouseSerializedItem oppI
                                JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID = whi.numWareHouseItmsDTLID
                        WHERE   oppI.numOppID = Opp.numOppId
                                AND oppI.numOppItemID = Opp.numoppitemtCode
                        ORDER BY vcSerialNo
                        FOR
                        XML PATH('')
                        ), 3, 200000) AS SerialLotNo,
            ISNULL(opp.numUOMId, 0) AS numUOM,
            ISNULL(u.vcUnitName, '') vcUOMName,
            dbo.fn_UOMConversion(opp.numUOMId, Opp.numItemCode,
                                    @numDomainID, NULL) AS UOMConversionFactor,
            ISNULL(Opp.numSOVendorId, 0) AS numSOVendorId,
            [dbo].FormatedDateTimeFromDate(OBD.dtRentalStartDate,
                                            @numDomainID) dtRentalStartDate,
            [dbo].FormatedDateTimeFromDate(OBD.dtRentalReturnDate,
                                            @numDomainID) dtRentalReturnDate,
            ISNULL(W.vcWareHouse, '') AS Warehouse,
            CASE WHEN charitemType = 'P' AND ISNULL(numItemGroup,0) > 0 AND ISNULL(bitMatrix,0)=0 THEN ISNULL(WI.vcWHSKU,I.vcSKU) ELSE I.vcSKU END vcSKU,
            CASE WHEN charitemType = 'P' AND ISNULL(numItemGroup,0) > 0 AND ISNULL(bitMatrix,0)=0 THEN ISNULL(WI.vcBarCode,I.numBarCodeId) ELSE I.numBarCodeId END vcBarcode,
			CASE WHEN charitemType = 'P' AND ISNULL(numItemGroup,0) > 0 AND ISNULL(bitMatrix,0)=0 THEN ISNULL(WI.vcBarCode,I.numBarCodeId) ELSE I.numBarCodeId END numBarCodeId,
            ISNULL(opp.numClassID, 0) AS numClassID,
            ISNULL(opp.numProjectID, 0) AS numProjectID,
            ISNULL(I.[numShipClass],0) AS numShipClass,OBD.numOppBizDocID
			,(CASE 
				WHEN OB.numBizDocId IN (29397) 
				THEN  STUFF((
					SELECT 
						CONCAT(' ,',WLInner.vcLocation,' (',ISNULL(WIInner.numOnHand,0) + ISNULL(WIInner.numAllocation,0),')')
					FROM 
						WareHouseItems WIInner
					INNER JOIN
						WarehouseLocation WLInner
					ON
						WIInner.numWLocationID = WLInner.numWLocationID
					WHERE 
						WIInner.numItemID = opp.numoppitemtCode
						AND (ISNULL(WIInner.numOnHand,0) + ISNULL(WIInner.numAllocation,0)) > 0
					FOR XML PATH, TYPE).value(N'.[1]', N'nvarchar(max)'),1,1,'')
				ELSE WL.vcLocation 
			END) vcLocation
			,ISNULL(bitSerialized,0) AS bitSerialized,ISNULL(bitLotNo,0) AS bitLotNo,
            opp.numOppId,OBD.numUnitHour AS numUnitHourOrig,ISNULL(I.numItemCode,0) AS [numItemCode],
			ISNULL(I.bitKitParent,0) AS bitKitParent,
			0 AS numOppChildItemID, 
			0 AS numOppKitChildItemID,
			0 AS bitChildItem,
			0 AS tintLevel,
			ISNULL(opp.vcPromotionDetail,'') AS vcPromotionDetail,
			(CASE WHEN ISNULL(WI.numWareHouseItemID,0) > 0 THEN W.vcWareHouse ELSE '' END) vcWarehouse,
			dbo.GetOrderAssemblyKitInclusionDetails(opp.numOppID,Opp.numoppitemtCode,OBD.numUnitHour,@tintCommitAllocation,1) AS vcInclusionDetails,
			Opp.numSortOrder
			,CAST(ISNULL(Opp.numCost,0) AS DECIMAL(20,5)) AS numCost, numOnHand
			,dbo.FormatedDateFromDate(opp.ItemReleaseDate,@numDomainID) dtReleaseDate
			,(CASE WHEN I.bitKitParent = 1 THEN ISNULL(dbo.fn_GetKitAvailabilityByWarehouse(I.numItemCode,W.numWareHouseID),0) ELSE (ISNULL(numOnHand,0) + ISNULL(numAllocation,0)) END) AS numAvailable
			,CPN.CustomerPartNo
			,ISNULL(opp.bitWinningBid,0) bitWinningBid
			,ISNULL(opp.numParentOppItemID,0) numParentOppItemID
			,ISNULL(opp.numUnitHour,0) - ISNULL(TEMPBizDocQty.numBizDocQty,0) AS numRemainingQty
			,dbo.FormatedDateFromDate(opp.ItemRequiredDate,@numDomainID) ItemRequiredDate
        FROM      
			OpportunityItems opp
        JOIN 
			OpportunityBizDocItems OBD 
		ON 
			OBD.numOppItemID = opp.numoppitemtCode
		JOIN
			OpportunityBizDocs OB
		ON
			OBD.numOppBizDocID=OB.numOppBizDocsID
        LEFT JOIN 
			item i 
		ON 
			opp.numItemCode = i.numItemCode
        LEFT JOIN 
			ListDetails L 
		ON 
			i.numItemClassification = L.numListItemID
        LEFT JOIN 
			Vendor V 
		ON 
			V.numVendorID = @DivisionID
            AND V.numItemCode = opp.numItemCode
        LEFT JOIN 
			UOM u 
		ON 
			u.numUOMId = opp.numUOMId                                      
        LEFT JOIN 
			dbo.WareHouseItems WI 
		ON 
			opp.numWarehouseItmsID = WI.numWareHouseItemID
			AND WI.numDomainID = @numDomainID
        LEFT JOIN 
			dbo.Warehouses W 
		ON 
			WI.numDomainID = W.numDomainID
            AND WI.numWareHouseID = W.numWareHouseID
        LEFT JOIN 
			dbo.WarehouseLocation WL 
		ON 
			WL.numWLocationID = WI.numWLocationID
		LEFT JOIN
			dbo.OpportunityMaster OM
		ON 
			OM.numOppId = opp.numOppId
		LEFT JOIN	
			DivisionMaster DM
		ON
			OM.numDivisionId= DM.numDivisionID
		LEFT JOIN 
			CustomerPartNumber CPN
		ON 
			opp.numItemCode = CPN.numItemCode AND CPN.numDomainId = @numDomainID AND CPN.numCompanyId = DM.numCompanyID
		OUTER APPLY
		(
			SELECT
				SUM(OBDIInner.numUnitHour) AS numBizDocQty
			FROM
				OpportunityBizDocs OBDInner
			INNER JOIN
				OpportunityBizDocItems OBDIInner
			ON
				OBDInner.numOppBizDocsId = OBDIInner.numOppBizDocID
			WHERE
				OBDInner.numOppId = @numOppId
				AND OBDInner.numBizDocId = OB.numBizDocId
				AND OBDIInner.numOppItemID = OBD.numOppItemID
		) TEMPBizDocQty
        WHERE     
			opp.numOppId = @numOppId
            AND (bitShowDeptItem IS NULL OR bitShowDeptItem = 0)
    ) X
	ORDER BY
		numSortOrder


    IF @DecimalPoint = 1 
        BEGIN
            UPDATE  #Temp1
            SET     monPrice = CONVERT(DECIMAL(18, 1), monPrice),
                    Amount = CONVERT(DECIMAL(18, 1), Amount)
        END
    IF @DecimalPoint = 2 
        BEGIN
            UPDATE  #Temp1
            SET     monPrice = CONVERT(DECIMAL(18, 2), monPrice),
                    Amount = CONVERT(DECIMAL(18, 2), Amount)
        END
    IF @DecimalPoint = 3 
        BEGIN
            UPDATE  #Temp1
            SET     monPrice = CONVERT(DECIMAL(18, 3), monPrice),
                    Amount = CONVERT(DECIMAL(18, 3), Amount)
        END
    IF @DecimalPoint = 4 
        BEGIN
            UPDATE  #Temp1
            SET     monPrice = CONVERT(DECIMAL(18, 4), monPrice),
                    Amount = CONVERT(DECIMAL(18, 4), Amount)
        END

    DECLARE @strSQLUpdate AS VARCHAR(2000);SET @strSQLUpdate=''
   
    EXEC ('ALTER TABLE #Temp1 ADD [TotalTax] DECIMAL(20,5)')
	EXEC ('ALTER TABLE #Temp1 ADD [CRV] DECIMAL(20,5)')

    IF @tintOppType = 1 AND @tintTaxOperator = 1 --add Sales tax
	BEGIN
        SET @strSQLUpdate = @strSQLUpdate
            + ',[TotalTax]= dbo.fn_CalBizDocTaxAmt('
            + CONVERT(VARCHAR(20), @numDomainID) + ',0,'
            + CONVERT(VARCHAR(20), @numOppId) +','+
            + 'numoppitemtCode,1,Amount,numUnitHour)'

		SET @strSQLUpdate = @strSQLUpdate
            + ',[CRV]= dbo.fn_CalBizDocTaxAmt('
            + CONVERT(VARCHAR(20), @numDomainID) + ',1,'
            + CONVERT(VARCHAR(20), @numOppId) +','+
            + 'numoppitemtCode,1,Amount,numUnitHour)'
	END
    ELSE IF @tintOppType = 1 AND @tintTaxOperator = 2 -- remove sales tax
	BEGIN
        SET @strSQLUpdate = @strSQLUpdate + ',[TotalTax]= 0'
		SET @strSQLUpdate = @strSQLUpdate + ',[CRV]= 0'
	END
    ELSE IF @tintOppType = 1
	BEGIN
        SET @strSQLUpdate = @strSQLUpdate
            + ',[TotalTax]= dbo.fn_CalBizDocTaxAmt('
            + CONVERT(VARCHAR(20), @numDomainID) + ',0,'
            + CONVERT(VARCHAR(20), @numOppId) +','+
            + 'numoppitemtCode,1,Amount,numUnitHour)'

		SET @strSQLUpdate = @strSQLUpdate
            + ',[CRV]= dbo.fn_CalBizDocTaxAmt('
            + CONVERT(VARCHAR(20), @numDomainID) + ',1,'
            + CONVERT(VARCHAR(20), @numOppId) +','+
            + 'numoppitemtCode,1,Amount,numUnitHour)'
	END
    ELSE
	BEGIN 
        SET @strSQLUpdate = @strSQLUpdate + ',[TotalTax]= 0'
		SET @strSQLUpdate = @strSQLUpdate + ',[CRV]= 0'
	END

	DECLARE @vcTaxName AS VARCHAR(100)
    DECLARE @numTaxItemID AS NUMERIC(9)
    SET @numTaxItemID = 0
    SELECT TOP 1
            @vcTaxName = vcTaxName,
            @numTaxItemID = numTaxItemID
    FROM    TaxItems
    WHERE   numDomainID = @numDomainID


    WHILE @numTaxItemID > 0
        BEGIN

            EXEC ( 'alter table #Temp1 add [' + @vcTaxName + '] DECIMAL(20,5)'
                )

            IF @tintOppType = 1 
                SET @strSQLUpdate = @strSQLUpdate + ',[' + @vcTaxName
                    + ']= dbo.fn_CalBizDocTaxAmt('
                    + CONVERT(VARCHAR(20), @numDomainID) + ','
                    + CONVERT(VARCHAR(20), @numTaxItemID) + ','
                    + CONVERT(VARCHAR(20), @numOppId) +','+
                    + 'numoppitemtCode,1,Amount,numUnitHour)'
            ELSE 
                SET @strSQLUpdate = @strSQLUpdate + ',[' + @vcTaxName + ']= 0'

            SELECT TOP 1
                    @vcTaxName = vcTaxName,
                    @numTaxItemID = numTaxItemID
            FROM    TaxItems
            WHERE   numDomainID = @numDomainID
                    AND numTaxItemID > @numTaxItemID

            IF @@rowcount = 0 
                SET @numTaxItemID = 0


        END

    IF @strSQLUpdate <> '' 
        BEGIN
            SET @strSQLUpdate = 'update #Temp1 set ItemCode=ItemCode'
                + @strSQLUpdate + ' where ItemCode>0'
            PRINT @strSQLUpdate
            EXEC ( @strSQLUpdate
                )
        END

    DECLARE @vcDbColumnName NVARCHAR(50)
    SELECT TOP 1
            @numFldID = numfieldID,
            @vcFldname = vcFieldName,
            @vcDbColumnName = vcDbColumnName,
            @intRowNum = ( tintRow + 1 )
    FROM    View_DynamicCustomColumns
    WHERE   numFormID = @tintType
            AND Grp_id = 5
            AND numDomainID = @numDomainID
            AND numAuthGroupID = @numBizDocId
            AND bitCustom = 1
            AND ISNULL(numRelCntType, 0) = @numBizDocTempID
    ORDER BY tintRow                                                                                          
    WHILE @intRowNum > 0                                                                                  
        BEGIN    
            EXEC
                ( 'alter table #Temp1 add [' + @vcDbColumnName
                  + '] varchar(100)'
                )
	
            SET @strSQLUpdate = 'update #Temp1 set ItemCode=ItemCode,['
                + @vcDbColumnName + ']=dbo.GetCustFldValueItem(' + @numFldID
                + ',ItemCode) where ItemCode>0'
            EXEC ( @strSQLUpdate
                )
		                   
            SELECT TOP 1
                    @numFldID = numfieldID,
                    @vcFldname = vcFieldName,
                    @vcDbColumnName = vcDbColumnName,
                    @intRowNum = ( tintRow + 1 )
            FROM    View_DynamicCustomColumns
            WHERE   numFormID = @tintType
                    AND Grp_id = 5
                    AND numDomainID = @numDomainID
                    AND numAuthGroupID = @numBizDocId
                    AND bitCustom = 1
                    AND tintRow >= @intRowNum
                    AND ISNULL(numRelCntType, 0) = @numBizDocTempID
            ORDER BY tintRow                                                                  
		           
            IF @@rowcount = 0 
                SET @intRowNum = 0                                                                                          
        END


    SELECT  Row_NUmber() OVER ( ORDER BY numoppitemtCode ) RowNumber,
            *
    FROM    #Temp1
    ORDER BY numSortOrder ASC 

    DROP TABLE #Temp1

--    SELECT  ISNULL(tintOppStatus, 0)
--    FROM    OpportunityMaster
--    WHERE   numOppID = @numOppId                                                                                                 

IF (SELECT COUNT(*) FROM View_DynamicColumns WHERE   numFormID = @tintType AND numDomainID = @numDomainID
            AND numAuthGroupID = @numBizDocId AND vcFieldType = 'R'AND ISNULL(numRelCntType, 0) = @numBizDocTempID)=0
BEGIN            
	INSERT INTO DycFormConfigurationDetails (numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numAuthGroupID,numRelCntType)
	select numFormId,numFieldId,1,tintRow,@numDomainID,@numBizDocId,@numBizDocTempID
	 FROM View_DynamicDefaultColumns
	 where numFormId=@tintType AND bitDefault=1 AND numDomainID=@numDomainID AND vcLookBackTableName = (CASE @numBizDocId WHEN 40911 THEN 'CSGrid' ELSE REPLACE(vcLookBackTableName,'CSGrid','') END) order by tintRow ASC	
	 
	 
	  IF(SELECT COUNT(*) FROM DycFrmConfigBizDocsSumm Ds where numBizDocID=@numBizDocId and Ds.numFormID=@tintType 
	and isnull(Ds.numBizDocTempID,0)=@numBizDocTempID and Ds.numDomainID=@numDomainID)=0
	BEGIN
		INSERT INTO DycFrmConfigBizDocsSumm (numFormID,numBizDocID,numFieldID,numDomainID,numBizDocTempID)
		 SELECT @tintType,@numBizDocId,DFM.numFieldID,@numDomainID,@numBizDocTempID FROM
		 DycFormField_Mapping DFFM 
			JOIN DycFieldMaster DFM on DFFM.numModuleID=DFFM.numModuleID and DFFM.numFieldID=DFM.numFieldID
			where numFormID=16 and DFFM.numDomainID is null
	END
	
END      

    SELECT  vcFieldName AS vcFormFieldName,
            vcDbColumnName,
            vcFieldDataType,
            tintRow AS intRowNum
    FROM    View_DynamicColumns
    WHERE   numFormID = @tintType
            AND numDomainID = @numDomainID
            AND numAuthGroupID = @numBizDocId
            AND vcFieldType = 'R'
            AND ISNULL(numRelCntType, 0) = @numBizDocTempID
    UNION
    SELECT  vcFieldName AS vcFormFieldName,
            vcDbColumnName,
            'C' vcFieldDataType,
            tintRow AS intRowNum
    FROM    View_DynamicCustomColumns
    WHERE   numFormID = @tintType
            AND Grp_id = 5
            AND numDomainID = @numDomainID
            AND numAuthGroupID = @numBizDocId
            AND bitCustom = 1
            AND ISNULL(numRelCntType, 0) = @numBizDocTempID
    ORDER BY tintRow
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_OPPBizDocItems_GetWithKitChilds' ) 
    DROP PROCEDURE USP_OPPBizDocItems_GetWithKitChilds
GO
CREATE PROCEDURE [dbo].[USP_OPPBizDocItems_GetWithKitChilds]
(
    @numOppId AS NUMERIC(9) = NULL,
    @numOppBizDocsId AS NUMERIC(9) = NULL,
    @numDomainID AS NUMERIC(9) = 0      
)
AS 
BEGIN
    DECLARE @DivisionID AS NUMERIC(9)      
    DECLARE @tintTaxOperator AS TINYINT
	DECLARE @tintCommitAllocation TINYINT
	SELECT @tintCommitAllocation=ISNULL(tintCommitAllocation,1) FROM Domain WHERE numDomainId=@numDomainID
                                                                                                                                                                                                                                                                                                                                          
    SELECT  
		@DivisionID = numDivisionID, 
		@tintTaxOperator = [tintTaxOperator]           
    FROM    
		OpportunityMaster
    WHERE   
		numOppId = @numOppId                                                                                                                                                                          

    
                                                                                          
    DECLARE @strSQL AS VARCHAR(MAX)                                                                                          
    DECLARE @strSQL1 AS VARCHAR(8000)                                       
    DECLARE @strSQLCusFields VARCHAR(1000) = ''
    DECLARE @strSQLEmpFlds VARCHAR(500) = ''                                                                         
                                                                                              
    DECLARE @tintType AS TINYINT   
    DECLARE @tintOppType AS TINYINT                                                      
    DECLARE @intRowNum AS INT                                                  
    DECLARE @numFldID AS VARCHAR(15)                                          
    DECLARE @vcFldname AS VARCHAR(50)                                            
    DECLARE @numBizDocId AS NUMERIC(9)
    DECLARE @numBizDocTempID AS NUMERIC(9) = 0
	DECLARE @bitDisplayKitChild AS BIT = 0
        
                                           
    SELECT  
		@numBizDocTempID = ISNULL(numBizDocTempID, 0),
        @numBizDocId = numBizDocId
    FROM    
		OpportunityBizDocs
    WHERE   
		numOppBizDocsId = @numOppBizDocsId

	
	SELECT
		@bitDisplayKitChild = ISNULL(bitDisplayKitChild,0)
	FROM	
		BizDocTemplate
	WHERE
		numDomainID = @numDomainID
		AND numBizDocTempID = @numBizDocTempID


    SELECT  
		@tintType = tintOppType,
        @tintOppType = tintOppType
    FROM    
		OpportunityMaster
    WHERE   
		numOppID = @numOppId                                                                                          
                                                                                   
    IF @tintType = 1 
        SET @tintType = 7
    ELSE 
        SET @tintType = 8     
		
		
	IF (SELECT 
			COUNT(*) 
		FROM 
			View_DynamicColumns 
		WHERE   
			numFormID = @tintType 
			AND numDomainID = @numDomainID
            AND numAuthGroupID = @numBizDocId 
			AND vcFieldType = 'R'
			AND ISNULL(numRelCntType, 0) = @numBizDocTempID
		)=0
	BEGIN            
		INSERT INTO DycFormConfigurationDetails 
		(
			numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numAuthGroupID,numRelCntType
		)
		SELECT 
			numFormId,numFieldId,1,tintRow,@numDomainID,@numBizDocId,@numBizDocTempID
		FROM 
			View_DynamicDefaultColumns
		WHERE 
			numFormId=@tintType and bitDefault=1 and numDomainID=@numDomainID order by tintRow ASC	
	 
	 
		IF (SELECT 
				COUNT(*) 
			FROM 
				DycFrmConfigBizDocsSumm Ds 
			WHERE 
				numBizDocID=@numBizDocId 
				AND Ds.numFormID=@tintType 
				AND ISNULL(Ds.numBizDocTempID,0)=@numBizDocTempID 
				AND Ds.numDomainID=@numDomainID
			)=0
		BEGIN
			INSERT INTO DycFrmConfigBizDocsSumm 
			(
				numFormID,numBizDocID,numFieldID,numDomainID,numBizDocTempID
			)
			SELECT 
				@tintType,@numBizDocId,DFM.numFieldID,@numDomainID,@numBizDocTempID 
			FROM
				DycFormField_Mapping DFFM 
			JOIN 
				DycFieldMaster DFM 
			ON 
				DFFM.numModuleID=DFFM.numModuleID and DFFM.numFieldID=DFM.numFieldID
			WHERE 
				numFormID=16 
				AND DFFM.numDomainID IS NULL
		END
	END      

	IF OBJECT_ID('tempdb..#TempBizDocProductGridColumns') IS NOT NULL DROP TABLE #TempBizDocProductGridColumns
	SELECT * INTO #TempBizDocProductGridColumns FROM (
    SELECT  
		vcFieldName AS vcFormFieldName,
        vcDbColumnName,
        vcFieldDataType,
        tintRow AS intRowNum
    FROM    
		View_DynamicColumns
    WHERE   
		numFormID = @tintType
        AND numDomainID = @numDomainID
        AND numAuthGroupID = @numBizDocId
        AND vcFieldType = 'R'
        AND ISNULL(numRelCntType, 0) = @numBizDocTempID
    UNION
    SELECT  
		vcFieldName AS vcFormFieldName,
        vcDbColumnName,
        'C' vcFieldDataType,
        tintRow AS intRowNum
    FROM    
		View_DynamicCustomColumns
    WHERE   
		numFormID = @tintType
        AND Grp_id = 5
        AND numDomainID = @numDomainID
        AND numAuthGroupID = @numBizDocId
        AND bitCustom = 1
        AND ISNULL(numRelCntType, 0) = @numBizDocTempID
    ) t1   
	ORDER BY 
		intRowNum                                                                              
                                                                                
    SELECT  
		*
    INTO    
		#Temp1
    FROM    
	( 
		SELECT   
			Opp.vcitemname AS vcItemName,
            ISNULL(OBD.numOppBizDocItemID, 0) OppBizDocItemID,
			(CASE 
				WHEN OM.tintOppType = 1 AND OM.tintOppStatus = 1 
				THEN 
					(CASE 
						WHEN (I.charItemType = 'p' AND ISNULL(Opp.bitDropShip, 0) = 0 AND ISNULL(I.bitAsset,0)=0)
						THEN 
							CAST(dbo.CheckOrderItemInventoryStatus(Opp.numItemCode,Opp.numUnitHour,Opp.numoppitemtCode,Opp.numWarehouseItmsID,(CASE WHEN ISNULL(bitKitParent,0) = 1 AND ISNULL(bitAssembly,0) = 1 THEN 0  WHEN ISNULL(bitKitParent,0) = 1 THEN 1 ELSE 0 END)) AS VARCHAR)
						ELSE ''
					END) 
				ELSE ''
			END) vcBackordered,
			ISNULL(WI.numBackOrder,0) AS numBackOrder,
            CASE WHEN charitemType = 'P' THEN 'Product'
                    WHEN charitemType = 'S' THEN 'Service'
            END AS charitemType,
            CASE WHEN charitemType = 'P' THEN 'Inventory'
	                WHEN charitemType = 'N' THEN 'Non-Inventory'
                    WHEN charitemType = 'S' THEN 'Service'
            END AS vcItemType,
			CONVERT(VARCHAR(2500), OBD.vcitemDesc) AS txtItemDesc, 
			opp.numUnitHour AS numOrgTotalUnitHour,
			dbo.fn_UOMConversion(ISNULL(i.numBaseUnit, 0),i.numItemCode, @numDomainID,ISNULL(opp.numUOMId, 0)) * opp.numUnitHour AS numTotalUnitHour,                                   
            dbo.fn_UOMConversion(ISNULL(i.numBaseUnit, 0),i.numItemCode, @numDomainID,ISNULL(opp.numUOMId, 0)) * OBD.numUnitHour AS numUnitHour,
            CONVERT(DECIMAL(20,5), (dbo.fn_UOMConversion(ISNULL(opp.numUOMId, 0), i.numItemCode, @numDomainID, ISNULL(i.numBaseUnit, 0)) * OBD.monPrice)) monPrice,
            CONVERT(VARCHAR, CONVERT(DECIMAL(20,5), OBD.monTotAmount)) Amount,
            OBD.monTotAmount/*Fo calculating sum*/,
            ISNULL(CONVERT(VARCHAR, i.monListPrice), 0) AS monListPrice,
            i.numItemCode AS ItemCode,
            opp.numoppitemtCode,
            L.vcdata AS numItemClassification,
            CASE WHEN bitTaxable = 0 THEN 'No'
                    ELSE 'Yes'
            END AS Taxable,
            numIncomeChartAcntId AS itemIncomeAccount,
            'NI' AS ItemType,
            ISNULL(GJH.numJournal_Id, 0) AS numJournalId,
            ( SELECT TOP 1
                        ISNULL(GJD.numTransactionId, 0)
                FROM      General_Journal_Details GJD
                WHERE     GJH.numJournal_Id = GJD.numJournalId
                        AND GJD.chBizDocItems = 'NI'
                        AND GJD.numoppitemtCode = opp.numoppitemtCode
            ) AS numTransactionId,
            ISNULL(Opp.vcModelID, '') vcModelID,
            (CASE WHEN charitemType = 'P' THEN dbo.USP_GetAttributes(OBD.numWarehouseItmsID, bitSerialized) ELSE ISNULL(OBD.vcAttributes,0) END) AS vcAttributes,
            ISNULL(vcPartNo, '') AS vcPartNo,
           (CASE WHEN ISNULL(opp.bitMarkupDiscount,0) = 1 
					THEN 0
					ELSE (ISNULL(OBD.monTotAmtBefDiscount, OBD.monTotAmount) - OBD.monTotAmount ) 
			END)
            AS DiscAmt,		
			(CASE WHEN OBD.numUnitHour = 0 THEN 0 ELSE CONVERT(DECIMAL(20,5),(dbo.fn_UOMConversion(ISNULL(opp.numUOMId, 0), i.numItemCode, @numDomainID, ISNULL(i.numBaseUnit, 0)) * (ISNULL(OBD.monTotAmount,0) / OBD.numUnitHour))) END) As monUnitSalePrice,
            ISNULL(OBD.vcNotes,ISNULL(opp.vcNotes,'')) vcNotes,
            OBD.vcTrackingNo,
            OBD.vcShippingMethod,
            OBD.monShipCost,
            [dbo].[FormatedDateFromDate](OBD.dtDeliveryDate,@numDomainID) dtDeliveryDate,
            Opp.numWarehouseItmsID,
            ISNULL(u.vcUnitName, '') numUOMId,
            ISNULL(Opp.vcManufacturer, '') AS vcManufacturer,
            ISNULL(V.monCost, 0) AS VendorCost,
            Opp.vcPathForTImage,
            i.[fltLength],
            i.[fltWidth],
            i.[fltHeight],
            i.[fltWeight],
            ISNULL(I.numVendorID, 0) numVendorID,
            Opp.bitDropShip AS DropShip,
            SUBSTRING(( SELECT  ' ,' + vcSerialNo
                                + CASE WHEN ISNULL(I.bitLotNo, 0) = 1
                                        THEN ' ('
                                            + CONVERT(VARCHAR(15), oppI.numQty)
                                            + ')'
                                        ELSE ''
                                    END
                        FROM    OppWarehouseSerializedItem oppI
                                JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID = whi.numWareHouseItmsDTLID
                        WHERE   oppI.numOppID = Opp.numOppId
                                AND oppI.numOppItemID = Opp.numoppitemtCode
                        ORDER BY vcSerialNo
                        FOR
                        XML PATH('')
                        ), 3, 200000) AS SerialLotNo,
            ISNULL(opp.numUOMId, 0) AS numUOM,
            ISNULL(u.vcUnitName, '') vcUOMName,
            dbo.fn_UOMConversion(opp.numUOMId, Opp.numItemCode,
                                    @numDomainID, NULL) AS UOMConversionFactor,
            ISNULL(Opp.numSOVendorId, 0) AS numSOVendorId,
            [dbo].FormatedDateTimeFromDate(OBD.dtRentalStartDate,
                                            @numDomainID) dtRentalStartDate,
            [dbo].FormatedDateTimeFromDate(OBD.dtRentalReturnDate,
                                            @numDomainID) dtRentalReturnDate,
            ISNULL(W.vcWareHouse, '') AS Warehouse,
            CASE WHEN charitemType = 'P' AND ISNULL(numItemGroup,0) > 0 AND ISNULL(bitMatrix,0)=0 THEN ISNULL(WI.vcWHSKU,I.vcSKU) ELSE I.vcSKU END vcSKU,
            CASE WHEN charitemType = 'P' AND ISNULL(numItemGroup,0) > 0 AND ISNULL(bitMatrix,0)=0 THEN ISNULL(WI.vcBarCode,I.numBarCodeId) ELSE I.numBarCodeId END vcBarcode,
			CASE WHEN charitemType = 'P' AND ISNULL(numItemGroup,0) > 0 AND ISNULL(bitMatrix,0)=0 THEN ISNULL(WI.vcBarCode,I.numBarCodeId) ELSE I.numBarCodeId END numBarCodeId,
            ISNULL(opp.numClassID, 0) AS numClassID,
            ISNULL(opp.numProjectID, 0) AS numProjectID,
            ISNULL(I.[numShipClass],0) AS numShipClass,OBD.numOppBizDocID
			,(CASE 
				WHEN OB.numBizDocId IN (29397) 
				THEN  STUFF((
					SELECT 
						CONCAT(' ,',WLInner.vcLocation,' (',ISNULL(WIInner.numOnHand,0) + ISNULL(WIInner.numAllocation,0),')')
					FROM 
						WareHouseItems WIInner
					INNER JOIN
						WarehouseLocation WLInner
					ON
						WIInner.numWLocationID = WLInner.numWLocationID
					WHERE 
						WIInner.numItemID = opp.numoppitemtCode
						AND (ISNULL(WIInner.numOnHand,0) + ISNULL(WIInner.numAllocation,0)) > 0
					FOR XML PATH, TYPE).value(N'.[1]', N'nvarchar(max)'),1,1,'')
				ELSE WL.vcLocation 
			END) vcLocation
			,ISNULL(bitSerialized,0) AS bitSerialized,ISNULL(bitLotNo,0) AS bitLotNo,
            opp.numOppId,OBD.numUnitHour AS numUnitHourOrig,ISNULL(I.numItemCode,0) AS [numItemCode],
			ISNULL(I.bitKitParent,0) AS bitKitParent,
			0 AS numOppChildItemID, 
			0 AS numOppKitChildItemID,
			0 AS bitChildItem,
			0 AS tintLevel,
			ISNULL(opp.vcPromotionDetail,'') AS vcPromotionDetail,
			(CASE WHEN ISNULL(WI.numWareHouseItemID,0) > 0 THEN W.vcWareHouse ELSE '' END) vcWarehouse,
			(CASE WHEN (SELECT COUNT(*) FROM #TempBizDocProductGridColumns WHERE vcDbColumnName='vcInclusionDetails') > 0 THEN dbo.GetOrderAssemblyKitInclusionDetails(opp.numOppID,Opp.numoppitemtCode,OBD.numUnitHour,@tintCommitAllocation,1) ELSE '' END) AS vcInclusionDetails,
			Opp.numSortOrder
			,CAST(ISNULL(Opp.numCost,0) AS DECIMAL(20,5)) AS numCost, numOnHand
			,dbo.FormatedDateFromDate(opp.ItemReleaseDate,@numDomainID) dtReleaseDate
			,(CASE WHEN I.bitKitParent = 1 THEN ISNULL(dbo.fn_GetKitAvailabilityByWarehouse(I.numItemCode,W.numWareHouseID),0) ELSE (ISNULL(numOnHand,0) + ISNULL(numAllocation,0)) END) AS numAvailable
			,CPN.CustomerPartNo
			,ISNULL(opp.bitWinningBid,0) bitWinningBid
			,ISNULL(opp.numParentOppItemID,0) numParentOppItemID
			,ISNULL(opp.numUnitHour,0) - ISNULL(TEMPBizDocQty.numBizDocQty,0) AS numRemainingQty
			,dbo.FormatedDateFromDate(opp.ItemRequiredDate,@numDomainID) ItemRequiredDate
        FROM      
			OpportunityItems opp
        JOIN 
			OpportunityBizDocItems OBD 
		ON 
			OBD.numOppItemID = opp.numoppitemtCode
		JOIN
			OpportunityBizDocs OB
		ON
			OBD.numOppBizDocID=OB.numOppBizDocsID
        LEFT JOIN 
			item i 
		ON 
			opp.numItemCode = i.numItemCode
        LEFT JOIN 
			ListDetails L 
		ON 
			i.numItemClassification = L.numListItemID
        LEFT JOIN 
			Vendor V 
		ON 
			V.numVendorID = @DivisionID
            AND V.numItemCode = opp.numItemCode
        LEFT JOIN 
			General_Journal_Header GJH 
		ON 
			opp.numoppid = GJH.numOppId
            AND GJH.numBizDocsPaymentDetId IS NULL
            AND GJH.numOppBizDocsId = @numOppBizDocsId
        LEFT JOIN 
			UOM u 
		ON 
			u.numUOMId = opp.numUOMId                                      
        LEFT JOIN 
			dbo.WareHouseItems WI 
		ON 
			opp.numWarehouseItmsID = WI.numWareHouseItemID
			AND WI.numDomainID = @numDomainID
        LEFT JOIN 
			dbo.Warehouses W 
		ON 
			WI.numDomainID = W.numDomainID
            AND WI.numWareHouseID = W.numWareHouseID
        LEFT JOIN 
			dbo.WarehouseLocation WL 
		ON 
			WL.numWLocationID = WI.numWLocationID
		LEFT JOIN
			dbo.OpportunityMaster OM
		ON 
			OM.numOppId = opp.numOppId
		LEFT JOIN	
			DivisionMaster DM
		ON
			OM.numDivisionId= DM.numDivisionID
		LEFT JOIN 
			CustomerPartNumber CPN
		ON 
			opp.numItemCode = CPN.numItemCode AND CPN.numDomainId = @numDomainID AND CPN.numCompanyId = DM.numCompanyID
		OUTER APPLY
		(
			SELECT
				SUM(OBDIInner.numUnitHour) AS numBizDocQty
			FROM
				OpportunityBizDocs OBDInner
			INNER JOIN
				OpportunityBizDocItems OBDIInner
			ON
				OBDInner.numOppBizDocsId = OBDIInner.numOppBizDocID
			WHERE
				OBDInner.numOppId = @numOppId
				AND OBDInner.numBizDocId = OB.numBizDocId
				AND OBDIInner.numOppItemID = OBD.numOppItemID
		) TEMPBizDocQty
        WHERE     
			opp.numOppId = @numOppId
            AND (bitShowDeptItem IS NULL OR bitShowDeptItem = 0)
            AND OBD.numOppBizDocID = @numOppBizDocsId
    ) X
	ORDER BY
		numSortOrder

	--GET KIT CHILD ITEMS (NOW WE ARE SUPPORTING KIT WITHIN KIT BUT ONLY 1 LEVEL MEANS CHILD KIT INSIDE PARENT KIT CAN NOT HAVE ANOTHER KIT AS CHILD)
	IF (@bitDisplayKitChild=1 AND (SELECT COUNT(*) FROM OpportunityKitItems WHERE numOppId = @numOppId ) > 0)
	BEGIN
		DECLARE @TEMPTABLE TABLE(
			numOppItemCode NUMERIC(18,0),
			numOppChildItemID NUMERIC(18,0), 
			numOppKitChildItemID NUMERIC(18,0),
			numItemCode NUMERIC(18,0),
			numWarehouseItemID NUMERIC(18,0),
			numTotalQty FLOAT,
			numQty FLOAT,
			numUOMID NUMERIC(18,0),
			numQtyShipped FLOAT,
			tintLevel TINYINT,
			numSortOrder INT
		);

		--FIRST LEVEL CHILD
		INSERT INTO @TEMPTABLE
		(
			numOppItemCode,numOppChildItemID,numOppKitChildItemID,numItemCode,numWarehouseItemID,numTotalQty,numQty,numUOMID,numQtyShipped,tintLevel,numSortOrder
		)
		SELECT 
			OKI.numOppItemID,
			OKI.numOppChildItemID,
			CAST(0 AS NUMERIC(18,0)),
			OKI.numChildItemID,
			ISNULL(OKI.numWareHouseItemId,0),
			CAST((t1.numOrgTotalUnitHour * OKI.numQtyItemsReq_Orig) AS FLOAT),
			CAST((t1.numUnitHour * OKI.numQtyItemsReq_Orig) AS FLOAT),
			ISNULL(OKI.numUOMID,0),
			ISNULL(numQtyShipped,0)
			,1
			,t1.numSortOrder
		FROM 
			#Temp1 t1
		INNER JOIN
			OpportunityKitItems OKI
		ON
			t1.numoppitemtCode = OKI.numOppItemID
		WHERE
			t1.bitKitParent = 1

		--SECOND LEVEL CHILD
		INSERT INTO @TEMPTABLE
		(
			numOppItemCode,numOppChildItemID,numOppKitChildItemID,numItemCode,numWarehouseItemID,numTotalQty,numQty,numUOMID,numQtyShipped,tintLevel,numSortOrder
		)
		SELECT 
			OKCI.numOppItemID,
			OKCI.numOppChildItemID,
			OKCI.numOppKitChildItemID,
			OKCI.numItemID,
			ISNULL(OKCI.numWareHouseItemId,0),
			CAST((c.numTotalQty * OKCI.numQtyItemsReq_Orig) AS NUMERIC(18,2)),
			CAST((c.numQty * OKCI.numQtyItemsReq_Orig) AS NUMERIC(18,2)),
			ISNULL(OKCI.numUOMID,0),
			ISNULL(OKCI.numQtyShipped,0),
			2
			,c.numSortOrder
		FROM 
			OpportunityKitChildItems OKCI
		INNER JOIN
			@TEMPTABLE c
		ON
			OKCI.numOppID = @numOppId
			AND OKCI.numOppItemID = c.numOppItemCode
			AND OKCI.numOppChildItemID = c.numOppChildItemID

		INSERT INTO 
			#Temp1
		SELECT   
			(CASE WHEN t2.tintLevel = 1 THEN CONCAT('&nbsp;&nbsp;',I.vcItemName) WHEN t2.tintLevel = 2 THEN CONCAT('&nbsp;&nbsp;&nbsp;&nbsp;',I.vcItemName) ELSE I.vcItemName END) AS vcItemName,
            0 OppBizDocItemID
			,(CASE WHEN (t2.numTotalQty-ISNULL(t2.numQtyShipped,0)) > ISNULL(WI.numAllocation,0) THEN ((t2.numTotalQty-ISNULL(t2.numQtyShipped,0)) - ISNULL(WI.numAllocation,0)) * dbo.fn_UOMConversion(ISNULL(i.numBaseUnit, 0),i.numItemCode, @numDomainID,ISNULL(t2.numUOMID, 0))  ELSE 0 END) AS vcBackordered
			,ISNULL(WI.numBackOrder,0) AS numBackOrder
            ,(CASE WHEN I.charitemType = 'P' THEN 'Product' WHEN charitemType = 'S' THEN 'Service' END) AS charitemType,
            (CASE WHEN charitemType = 'P' THEN 'Inventory'
	                WHEN charitemType = 'N' THEN 'Non-Inventory'
                    WHEN charitemType = 'S' THEN 'Service'
            END) AS vcItemType,
			CONVERT(VARCHAR(2500), I.txtItemDesc) AS txtItemDesc,   
			t2.numTotalQty AS numOrigTotalUnitHour,
			t2.numTotalQty AS numTotalUnitHour,
            t2.numQty AS numUnitHour,
            (ISNULL(dbo.fn_UOMConversion(ISNULL(t2.numUOMID, 0), i.numItemCode, @numDomainID, ISNULL(i.numBaseUnit, 0)),1) * (CASE WHEN I.charitemType = 'P' THEN ISNULL(WI.monWListPrice,0) ELSE ISNULL(I.monListPrice,0) END)) monPrice,
            CONVERT(VARCHAR, CONVERT(DECIMAL(20,5),((t2.numQty * ISNULL(dbo.fn_UOMConversion(ISNULL(t2.numUOMID, 0), i.numItemCode, @numDomainID, ISNULL(i.numBaseUnit, 0)),1)) * (CASE WHEN I.charitemType = 'P' THEN ISNULL(WI.monWListPrice,0) ELSE ISNULL(I.monListPrice,0) END)))) Amount,
            ((t2.numQty * ISNULL(dbo.fn_UOMConversion(ISNULL(t2.numUOMID, 0), i.numItemCode, @numDomainID, ISNULL(i.numBaseUnit, 0)),1)) * (CASE WHEN I.charitemType = 'P' THEN ISNULL(WI.monWListPrice,0) ELSE ISNULL(I.monListPrice,0) END)) AS monTotAmount/*Fo calculating sum*/,
            ISNULL(CONVERT(VARCHAR, (CASE WHEN I.charitemType = 'P' THEN ISNULL(WI.monWListPrice,0) ELSE ISNULL(I.monListPrice,0) END)), 0) AS monListPrice,
            I.numItemCode AS ItemCode,
            t2.numOppItemCode,
            L.vcdata AS numItemClassification,
            CASE WHEN bitTaxable = 0 THEN 'No' ELSE 'Yes' END AS Taxable,
            numIncomeChartAcntId AS itemIncomeAccount,
            'NI' AS ItemType,
            0 AS numJournalId,
            0 AS numTransactionId,
            ISNULL(I.vcModelID, '') vcModelID,
            dbo.USP_GetAttributes(t2.numWarehouseItemID, bitSerialized) AS vcAttributes,
            ISNULL(vcPartNo, '') AS vcPartNo,
            0 AS DiscAmt,
			(ISNULL(dbo.fn_UOMConversion(ISNULL(t2.numUOMID, 0), i.numItemCode, @numDomainID, ISNULL(i.numBaseUnit, 0)),1) * (CASE WHEN I.charitemType = 'P' THEN ISNULL(WI.monWListPrice,0) ELSE ISNULL(I.monListPrice,0) END)) As monUnitSalePrice,
            '' AS vcNotes,'' AS vcTrackingNo,'' AS vcShippingMethod,0 AS monShipCost,NULL AS dtDeliveryDate,t2.numWarehouseItemID,
            ISNULL(u.vcUnitName, '') numUOMId,ISNULL(I.vcManufacturer, '') AS vcManufacturer,ISNULL(V.monCost, 0) AS VendorCost,
            ISNULL((SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = I.numItemCode AND bitDefault=1),'') AS vcPathForTImage,
            I.[fltLength],I.[fltWidth],I.[fltHeight],I.[fltWeight],ISNULL(I.numVendorID, 0) numVendorID,0 AS DropShip,
            '' AS SerialLotNo,
            ISNULL(t2.numUOMId, 0) AS numUOM,
            ISNULL(u.vcUnitName, '') vcUOMName,
            dbo.fn_UOMConversion(t2.numUOMId, t2.numItemCode,@numDomainID, NULL) AS UOMConversionFactor,
            0 AS numSOVendorId,
            NULL AS dtRentalStartDate,
            NULL dtRentalReturnDate,
            ISNULL(W.vcWareHouse, '') AS Warehouse,
            CASE WHEN charitemType = 'P' AND ISNULL(numItemGroup,0) > 0 AND ISNULL(bitMatrix,0)=0 THEN ISNULL(WI.vcWHSKU,I.vcSKU) ELSE I.vcSKU END vcSKU,
            CASE WHEN charitemType = 'P' AND ISNULL(numItemGroup,0) > 0 AND ISNULL(bitMatrix,0)=0 THEN ISNULL(WI.vcBarCode,I.numBarCodeId) ELSE I.numBarCodeId END vcBarcode,
			CASE WHEN charitemType = 'P' AND ISNULL(numItemGroup,0) > 0 AND ISNULL(bitMatrix,0)=0 THEN ISNULL(WI.vcBarCode,I.numBarCodeId) ELSE I.numBarCodeId END numBarCodeId,
            ISNULL(I.numItemClass, 0) AS numClassID,
            0 AS numProjectID,
            ISNULL(I.[numShipClass],0) AS numShipClass,
			@numOppBizDocsId AS numOppBizDocID,WL.vcLocation,ISNULL(bitSerialized,0) AS bitSerialized,ISNULL(bitLotNo,0) AS bitLotNo,
            @numOppId AS numOppId,t2.numQty AS numUnitHourOrig,
			ISNULL(I.numItemCode,0) AS [numItemCode],
			ISNULL(I.bitKitParent,0) AS bitKitParent,
			ISNULL(t2.numOppChildItemID,0) AS numOppChildItemID, 
			ISNULL(t2.numOppKitChildItemID,0) AS numOppKitChildItemID,
			1 AS bitChildItem,
			t2.tintLevel AS tintLevel,
			'' AS vcPromotionDetail,
			(CASE WHEN ISNULL(WI.numWareHouseItemID,0) > 0 THEN W.vcWareHouse ELSE '' END) vcWarehouse,
			'' vcInclusionDetails
			,0
			,0
			,numOnHand
			, ''
			,(CASE WHEN I.bitKitParent = 1 THEN ISNULL(dbo.fn_GetKitAvailabilityByWarehouse(I.numItemCode,W.numWareHouseID),0) ELSE (ISNULL(numOnHand,0) + ISNULL(numAllocation,0)) END) AS numAvailable
			,''
			,0
			,0
			,0
			,NULL
		FROM
			@TEMPTABLE t2
		INNER JOIN
			Item I
		ON
			t2.numItemCode = I.numItemCode
		LEFT JOIN
			WareHouseItems WI
		ON
			WI.numWareHouseItemID = t2.numWarehouseItemID
		LEFT JOIN 
			dbo.Warehouses W 
		ON 
			WI.numDomainID = W.numDomainID
            AND WI.numWareHouseID = W.numWareHouseID
        LEFT JOIN 
			dbo.WarehouseLocation WL 
		ON 
			WL.numWLocationID = WI.numWLocationID
		LEFT JOIN 
			ListDetails L 
		ON 
			I.numItemClassification = L.numListItemID
		LEFT JOIN 
			Vendor V 
		ON 
			V.numVendorID = @DivisionID
            AND V.numItemCode = t2.numItemCode
		LEFT JOIN 
			UOM u 
		ON 
			u.numUOMId = t2.numUOMId 
	END


    DECLARE @strSQLUpdate AS VARCHAR(2000);SET @strSQLUpdate=''
   
    EXEC ('ALTER TABLE #Temp1 ADD [TotalTax] DECIMAL(20,5)')

    IF @tintTaxOperator = 1 --add Sales tax
	BEGIN
		SET @strSQLUpdate = @strSQLUpdate
            + ',[TotalTax]= (CASE WHEN ISNULL(bitChildItem,0) = 0 THEN dbo.fn_CalBizDocTaxAmt('
            + CONVERT(VARCHAR(20), @numDomainID) + ',0,'
            + CONVERT(VARCHAR(20), @numOppId) +','+
            + 'numoppitemtCode,1,Amount,numUnitHour) ELSE 0 END)'
	END        
    ELSE IF @tintTaxOperator = 2 -- remove sales tax
	BEGIN
        SET @strSQLUpdate = @strSQLUpdate + ',[TotalTax]= 0'
	END
    ELSE 
	BEGIN
        SET @strSQLUpdate = @strSQLUpdate
            + ',[TotalTax]= (CASE WHEN ISNULL(bitChildItem,0) = 0 THEN dbo.fn_CalBizDocTaxAmt('
            + CONVERT(VARCHAR(20), @numDomainID) + ',0,'
            + CONVERT(VARCHAR(20), @numOppId) +','+
            + 'numoppitemtCode,1,Amount,numUnitHour) ELSE 0 END)'
    END

	-- CRV TAX TYPE
	EXEC ('ALTER TABLE #Temp1 ADD [CRV] DECIMAL(20,5)')

	IF @tintTaxOperator = 2
	BEGIN
		SET @strSQLUpdate = @strSQLUpdate
				+ ',[CRV]= (CASE WHEN ISNULL(bitChildItem,0) = 0 THEN dbo.fn_CalBizDocTaxAmt('
				+ CONVERT(VARCHAR(20), @numDomainID) + ',1,'
				+ CONVERT(VARCHAR(20), @numOppId) +','+
				+ 'numoppitemtCode,1,Amount,numUnitHour) ELSE 0 END)'
	END
	ELSE
	BEGIN
		SET @strSQLUpdate = @strSQLUpdate + ',[CRV]= 0'
	END

	DECLARE @vcTaxName AS VARCHAR(100)
    DECLARE @numTaxItemID AS NUMERIC(9)
    SET @numTaxItemID = 0
    
	SELECT TOP 1
        @vcTaxName = vcTaxName,
        @numTaxItemID = numTaxItemID
    FROM    
		TaxItems
    WHERE   
		numDomainID = @numDomainID

    WHILE @numTaxItemID > 0
    BEGIN
        EXEC ('ALTER TABLE #Temp1 ADD [' + @vcTaxName + '] DECIMAL(20,5)')

		IF @tintTaxOperator = 2 -- remove sales tax
		BEGIN
			SET @strSQLUpdate = @strSQLUpdate + ',[' + @vcTaxName + ']=0' 
		END
		ELSE
		BEGIN
			SET @strSQLUpdate = @strSQLUpdate + ',[' + @vcTaxName
				+ ']= (CASE WHEN ISNULL(bitChildItem,0) = 0 THEN dbo.fn_CalBizDocTaxAmt('
				+ CONVERT(VARCHAR(20), @numDomainID) + ','
				+ CONVERT(VARCHAR(20), @numTaxItemID) + ','
				+ CONVERT(VARCHAR(20), @numOppId) +','+
				+ 'numoppitemtCode,1,Amount,numUnitHour) ELSE 0 END)'
		END

        SELECT TOP 1
			@vcTaxName = vcTaxName,
            @numTaxItemID = numTaxItemID
        FROM    
			TaxItems
        WHERE   
			numDomainID = @numDomainID
            AND numTaxItemID > @numTaxItemID

        IF @@rowcount = 0 
            SET @numTaxItemID = 0
    END

    IF @strSQLUpdate <> '' 
    BEGIN
        SET @strSQLUpdate = 'UPDATE #Temp1 SET ItemCode=ItemCode' + @strSQLUpdate + ' WHERE ItemCode > 0 AND bitChildItem=0'
        EXEC (@strSQLUpdate)
    END

    DECLARE @vcDbColumnName NVARCHAR(50)

    SELECT TOP 1
            @numFldID = numfieldID,
            @vcFldname = vcFieldName,
            @vcDbColumnName = vcDbColumnName,
            @intRowNum = ( tintRow + 1 )
    FROM    View_DynamicCustomColumns
    WHERE   numFormID = @tintType
            AND Grp_id = 5
            AND numDomainID = @numDomainID
            AND numAuthGroupID = @numBizDocId
            AND bitCustom = 1
            AND ISNULL(numRelCntType, 0) = @numBizDocTempID
    ORDER BY tintRow     
	                                                                                     
    WHILE @intRowNum > 0                                                                                  
    BEGIN    
        EXEC('ALTER TABLE #Temp1 ADD [' + @vcDbColumnName + '] varchar(100)')
	
        SET @strSQLUpdate = 'UPDATE #Temp1 set ItemCode=ItemCode,[' + @vcDbColumnName + ']= dbo.GetCustFldValueOppItems(' + @numFldID  + ',numoppitemtCode,ItemCode) WHERE ItemCode > 0 AND bitChildItem=0'
            
		EXEC (@strSQLUpdate)
		                   
        SELECT 
			TOP 1
            @numFldID = numfieldID,
            @vcFldname = vcFieldName,
            @vcDbColumnName = vcDbColumnName,
            @intRowNum = ( tintRow + 1 )
        FROM    
			View_DynamicCustomColumns
        WHERE   
			numFormID = @tintType
            AND Grp_id = 5
            AND numDomainID = @numDomainID
            AND numAuthGroupID = @numBizDocId
            AND bitCustom = 1
            AND tintRow >= @intRowNum
            AND ISNULL(numRelCntType, 0) = @numBizDocTempID
        ORDER BY 
			tintRow                                                                  
		           
        IF @@rowcount = 0 
            SET @intRowNum = 0                                                                                          
    END


    SELECT  
		ROW_NUMBER() OVER ( ORDER BY numSortOrder) RowNumber,
        *
    FROM    
		#Temp1
    ORDER BY 
		numSortOrder ASC, tintLevel ASC, numoppitemtCode ASC, numOppChildItemID ASC, numOppKitChildItemID ASC

    DROP TABLE #Temp1

    SELECT  
		ISNULL(tintOppStatus, 0)
    FROM    
		OpportunityMaster
    WHERE   
		numOppID = @numOppId                                                                                                 


	SELECT * FROM #TempBizDocProductGridColumns ORDER BY intRowNum

	IF OBJECT_ID('tempdb..#TempBizDocProductGridColumns') IS NOT NULL DROP TABLE #TempBizDocProductGridColumns
END
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityBizDocs_AutoFulFill')
DROP PROCEDURE USP_OpportunityBizDocs_AutoFulFill
GO
CREATE PROCEDURE [dbo].[USP_OpportunityBizDocs_AutoFulFill] 
	@numDomainID NUMERIC(18,0),
	@numUserCntID NUMERIC(18,0),
	@numOppId NUMERIC(18,0),
	@numOppBizDocID NUMERIC(18,0)
AS  
BEGIN 
BEGIN TRY
	DECLARE @vcItemType AS CHAR(1)
	DECLARE @bitDropShip AS BIT
	DECLARE @numOppItemID AS INT
	DECLARE @numWarehouseItemID AS INT
	DECLARE @numQty AS FLOAT
	DECLARE @numQtyShipped AS FLOAT
	DECLARE @bitSerial AS BIT
	DECLARE @bitLot AS BIT
	DECLARE @bitAssembly AS BIT
	DECLARE @bitKit AS BIT
	DECLARE @tintCommitAllocation AS TINYINT
	DECLARE @bitAllocateInventoryOnPickList AS BIT

	DECLARE @i AS INT = 1
	DECLARE @COUNT AS INT  
	DECLARE @numNewQtyShipped AS FLOAT

	/* VERIFY BIZDOC IS EXISTS OR NOT */
	IF NOT EXISTS (SELECT numOppBizDocsId FROM OpportunityBizDocs WHERE numOppBizDocsId = @numOppBizDocID)
	BEGIN
		RAISERROR('FULFILLMENT_BIZDOC_IS_DELETED',16,1)
	END

	/* VERIFY WHETHER BIZDOC IS ALREADY FULLFILLED BY SOMEBODY ELSE */
	IF EXISTS (SELECT numOppBizDocsId FROM OpportunityBizDocs WHERE numOppBizDocsId = @numOppBizDocID AND bitFulfilled = 1) 
	BEGIN
		RAISERROR('FULFILLMENT_BIZDOC_ALREADY_FULFILLED',16,1)
	END

	--IF EXISTS(SELECT numWOId FROm WorkOrder WHERE numOppId=@numOppId AND numWOStatus <> 23184) --WORK ORDER NOT YET COMPLETED
	--BEGIN
	--	RAISERROR('WORK_ORDER_NOT_COMPLETED',16,1)
	--END

	SELECT @tintCommitAllocation=ISNULL(tintCommitAllocation,1) FROM Domain WHERE numDomainId=@numDomainID
	SELECT @bitAllocateInventoryOnPickList=ISNULL(bitAllocateInventoryOnPickList,0) FROM OpportunityMaster INNER JOIN DivisionMaster ON OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID WHERE numOppID=@numOppID

	BEGIN TRANSACTION

		-- CHANGE BIZDIC QTY TO AVAIALLBE QTY TO SHIP
		UPDATE
			OBI
		SET
			OBI.numUnitHour = (CASE 
								WHEN OM.tintOppType = 1 AND OM.tintOppStatus = 1 
								THEN 
									(CASE 
										WHEN (I.charItemType = 'p' AND ISNULL(OBI.bitDropShip, 0) = 0 AND ISNULL(bitAsset,0)=0)
										THEN 
											(CASE WHEN OBI.numUnitHour <= dbo.GetOrderItemAvailableQtyToShip(OBI.numOppItemID,OBI.numWarehouseItmsID,ISNULL(I.bitKitParent,0),@tintCommitAllocation,@bitAllocateInventoryOnPickList) THEN OBI.numUnitHour ELSE dbo.GetOrderItemAvailableQtyToShip(OBI.numOppItemID,OBI.numWarehouseItmsID,ISNULL(I.bitKitParent,0),@tintCommitAllocation,@bitAllocateInventoryOnPickList) END)
										ELSE OBI.numUnitHour
									END) 
								ELSE OBI.numUnitHour
							END)
		FROM
			OpportunityBizDocItems OBI
		INNER JOIN
			OpportunityItems OI
		ON
			OBI.numOppItemID=OI.numOppItemtcode
		INNER JOIN
			OpportunityMaster OM
		ON
			OI.numOppId=OM.numOppId
		INNER JOIN
			WareHouseItems WI
		ON
			OI.numWarehouseItmsID = WI.numWareHouseItemID
		INNER JOIN
			Item I
		ON
			OBI.numItemCode = I.numItemCode
		WHERE
			numOppBizDocID=@numOppBizDocID

		--CHANGE TOTAL AMOUNT
		UPDATE
			OBI
		SET
			OBI.monTotAmount = (OI.monTotAmount/OI.numUnitHour) * ISNULL(OBI.numUnitHour,0),
			OBI.monTotAmtBefDiscount = (OI.monTotAmtBefDiscount/OI.numUnitHour) * ISNULL(OBI.numUnitHour,0)
		FROM
			OpportunityBizDocItems OBI
		INNER JOIN
			OpportunityItems OI
		ON
			OBI.numOppItemID=OI.numOppItemtcode
		INNER JOIN
			OpportunityMaster OM
		ON
			OI.numOppId=OM.numOppId
		INNER JOIN
			WareHouseItems WI
		ON
			OBI.numWarehouseItmsID = WI.numWareHouseItemID
		INNER JOIN
			Item I
		ON
			OBI.numItemCode = I.numItemCode
		WHERE
			numOppBizDocID=@numOppBizDocID

		DECLARE @TempFinalTable TABLE
		(
			ID INT IDENTITY(1,1),
			numItemCode NUMERIC(18,0),
			vcItemType CHAR(1),
			bitSerial BIT,
			bitLot BIT,
			bitAssembly BIT,
			bitKit BIT,
			numOppItemID NUMERIC(18,0),
			numWarehouseItemID NUMERIC(18,0),
			numQtyShipped FLOAT,
			numQty FLOAT,
			bitDropShip BIT
		)

		INSERT INTO @TempFinalTable
		(
			numItemCode,
			vcItemType,
			bitSerial,
			bitLot,
			bitAssembly,
			bitKit,
			numOppItemID,
			numWarehouseItemID,
			numQtyShipped,
			numQty,
			bitDropShip
		)
		SELECT
			OpportunityBizDocItems.numItemCode,
			ISNULL(Item.charItemType,''),
			ISNULL(Item.bitSerialized,0),
			ISNULL(Item.bitLotNo,0),
			ISNULL(Item.bitAssembly,0),
			ISNULL(Item.bitKitParent,0),
			ISNULL(OpportunityItems.numoppitemtcode,0),
			ISNULL(OpportunityBizDocItems.numWarehouseItmsID,0),
			ISNULL(OpportunityItems.numQtyShipped,0),
			ISNULL(OpportunityBizDocItems.numUnitHour,0),
			ISNULL(OpportunityItems.bitDropShip,0)
		FROM
			OpportunityBizDocs
		INNER JOIN
			OpportunityBizDocItems
		ON
			OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
		INNER JOIN
			OpportunityItems
		ON
			OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtCode
		INNER JOIN
			Item
		ON
			OpportunityBizDocItems.numItemCode = Item.numItemCode
		WHERE
			numOppBizDocsId = @numOppBizDocID

		/* VALIDATE ALL THE DETAILS BEFORE FULFILLING ITEMS */
		SELECT @COUNT=COUNT(*) FROM @TempFinalTable

		WHILE @i <= @COUNT
		BEGIN
			SELECT
				@numOppItemID=numOppItemID,
				@numWarehouseItemID=numWarehouseItemID,
				@numQty=numQty,
				@bitSerial=bitSerial,
				@bitLot=bitLot 
			FROM 
				@TempFinalTable 
			WHERE 
				ID = @i

			--IF ITEM IS SERIAL ITEM CHEKC THAT VALID AND REQUIRED NUMBER OF SERIALS ARE PROVIDED BY USER
			IF @bitSerial = 1
			BEGIN
				IF ISNULL((SELECT SUM(numQty) FROM OppWarehouseSerializedItem WHERE numOppID=@numOppId AND numOppItemID=@numOppItemID AND ISNULL(numOppBizDocsId,0) = 0),0) <> @numQty
				BEGIN
					RAISERROR('REQUIRED_SERIALS_NOT_PROVIDED',16,1)
				END
				ELSE
				BEGIN
					IF EXISTS (SELECT 
									OppWarehouseSerializedItem.numWarehouseItmsDTLID
								FROM 
									OppWarehouseSerializedItem 
								LEFT JOIN	
									WareHouseItmsDTL
								ON
									OppWarehouseSerializedItem.numWarehouseItmsDTLID = WareHouseItmsDTL.numWareHouseItmsDTLID
								WHERE 
									numOppID=@numOppId 
									AND numOppItemID=@numOppItemID 
									AND ISNULL(numOppBizDocsId,0) = 0
									AND WareHouseItmsDTL.numWareHouseItmsDTLID IS NULL)
					BEGIN
						RAISERROR('INVALID_SERIAL_NUMBERS',16,1)
					END
				END
			END

			IF @bitLot = 1
			BEGIN
				IF ISNULL((SELECT SUM(numQty) FROM OppWarehouseSerializedItem WHERE numOppID=@numOppId AND numOppItemID=@numOppItemID AND ISNULL(numOppBizDocsId,0) = 0),0) <> @numQty
				BEGIN
					RAISERROR('REQUIRED_LOTNO_NOT_PROVIDED',16,1)
				END
				ELSE
				BEGIN
					IF EXISTS (SELECT 
									OppWarehouseSerializedItem.numWarehouseItmsDTLID
								FROM 
									OppWarehouseSerializedItem 
								LEFT JOIN	
									WareHouseItmsDTL
								ON
									OppWarehouseSerializedItem.numWarehouseItmsDTLID = WareHouseItmsDTL.numWareHouseItmsDTLID
								WHERE 
									numOppID=@numOppId 
									AND numOppItemID=@numOppItemID 
									AND ISNULL(numOppBizDocsId,0) = 0
									AND WareHouseItmsDTL.numWareHouseItmsDTLID IS NULL)
					BEGIN
						RAISERROR('INVALID_LOT_NUMBERS',16,1)
					END
				END
			END

			SET @i = @i + 1
		END
		
			
	
	
		SET @i = 1
		SELECT @COUNT = COUNT(*) FROM @TempFinalTable
		DECLARE @vcDescription AS VARCHAR(300)
		DECLARE @numAllocation AS FLOAT
		DECLARE @numOnHand AS FLOAT

		DECLARE @TEMPSERIALLOT TABLE
		(
			ID INT
			,numWarehouseItemID NUMERIC(18,0)
			,vcSerialLot VARCHAR(100)
			,numQty FLOAT
		)

		DECLARE @j INT = 0
		DECLARE @ChildCount AS INT
		DECLARE @vcChildDescription AS VARCHAR(300)
		DECLARE @numChildAllocation AS FLOAT
		DECLARE @numChildOnHand AS FLOAT
		DECLARE @numOppChildItemID AS INT
		DECLARE @numChildItemCode AS INT
		DECLARE @bitChildIsKit AS BIT
		DECLARE @numChildWarehouseItemID AS INT
		DECLARE @numChildQty AS FLOAT
		DECLARE @numChildQtyShipped AS FLOAT

		DECLARE @TempKitSubItems TABLE
		(
			ID INT,
			numOppChildItemID NUMERIC(18,0),
			numChildItemCode NUMERIC(18,0),
			bitChildIsKit BIT,
			numChildWarehouseItemID NUMERIC(18,0),
			numChildQty FLOAT,
			numChildQtyShipped FLOAT
		)

		DECLARE @k AS INT = 1
		DECLARE @CountKitChildItems AS INT
		DECLARE @vcKitChildDescription AS VARCHAR(300)
		DECLARE @numKitChildAllocation AS FLOAT
		DECLARE @numKitChildOnHand AS FLOAT
		DECLARE @numOppKitChildItemID AS NUMERIC(18,0)
		DECLARE @numKitChildItemCode AS NUMERIC(18,0)
		DECLARE @numKitChildWarehouseItemID AS NUMERIC(18,0)
		DECLARE @numKitChildQty AS FLOAT
		DECLARE @numKitChildQtyShipped AS FLOAT

		DECLARE @TempKitChildKitSubItems TABLE
		(
			ID INT,
			numOppKitChildItemID NUMERIC(18,0),
			numKitChildItemCode NUMERIC(18,0),
			numKitChildWarehouseItemID NUMERIC(18,0),
			numKitChildQty FLOAT,
			numKitChildQtyShipped FLOAT
		)

		WHILE @i <= @COUNT
		BEGIN
			SELECT 
				@vcItemType=vcItemType,
				@bitSerial=bitSerial,
				@bitLot=bitLot,
				@bitAssembly=bitAssembly,
				@bitKit=bitKit,
				@bitDropShip=bitDropShip,
				@numOppItemID=numOppItemID,
				@numQty=numQty,
				@numQtyShipped = numQtyShipped,
				@numWarehouseItemID=numWarehouseItemID
			FROM 
				@TempFinalTable 
			WHERE 
				ID = @i

			--MAKE INVENTIRY CHANGES IF INVENTORY ITEM
			IF (UPPER(@vcItemType) = 'P' AND @bitDropShip = 0)
			BEGIN
				SET @vcDescription=CONCAT('SO Qty Shipped (Qty:',CAST(@numQty AS NUMERIC),' Shipped:',CAST(@numQtyShipped AS NUMERIC),')')
						 
				IF @bitKit = 1
				BEGIN
					-- CLEAR DATA OF PREVIOUS ITERATION
					DELETE FROM @TempKitSubItems

					-- GET KIT SUB ITEMS DETAIL
					INSERT INTO @TempKitSubItems
					(
						ID,
						numOppChildItemID,
						numChildItemCode,
						bitChildIsKit,
						numChildWarehouseItemID,
						numChildQty,
						numChildQtyShipped
					)
					SELECT 
						ROW_NUMBER() OVER(ORDER BY numOppChildItemID),
						ISNULL(numOppChildItemID,0),
						ISNULL(I.numItemCode,0),
						ISNULL(I.bitKitParent,0),
						ISNULL(numWareHouseItemId,0),
						((ISNULL(numQtyItemsReq_Orig,0) * ISNULL(dbo.fn_UOMConversion(OKI.numUOMId,OKI.numChildItemID,@numDomainID,I.numBaseUnit),1)) * @numQty),
						ISNULL(numQtyShipped,0)
					FROM 
						OpportunityKitItems OKI 
					JOIN 
						Item I 
					ON 
						OKI.numChildItemID=I.numItemCode    
					WHERE 
						charitemtype='P' 
						AND ISNULL(numWareHouseItemId,0) > 0 
						AND OKI.numOppID=@numOppID 
						AND OKI.numOppItemID=@numOppItemID 

					SET @j = 1
					SELECT @ChildCount=COUNT(*) FROM @TempKitSubItems

					--LOOP OVER ALL CHILD ITEMS AND RELESE ALLOCATION
					WHILE @j <= @ChildCount
					BEGIN
						SELECT
							@numOppChildItemID=numOppChildItemID,
							@numChildItemCode=numChildItemCode,
							@bitChildIsKit=bitChildIsKit,
							@numChildWarehouseItemID=numChildWarehouseItemID,
							@numChildQty=numChildQty,
							@numChildQtyShipped=numChildQtyShipped
						FROM
							@TempKitSubItems
						WHERE 
							ID = @j

						SELECT @numChildAllocation=ISNULL(numAllocation,0),@numChildOnHand=ISNULL(numOnHand,0) FROM WareHouseItems WHERE numWareHouseItemID=@numChildWarehouseItemID

						-- IF CHILD ITEM IS KIT THEN LOOK OUT FOR ITS CHILD ITEMS WHICH ARE STORED IN OpportunityKitChildItems TABLE
						IF @bitChildIsKit = 1
						BEGIN
							-- IF KIT IS ADDED WITHIN KIT THEN THIS SUB KIT HAS ITS OWN ITEM CONFIGURATION WHICH ID STORED IN OpportunityKitChildItems
							IF EXISTS (SELECT numOppKitChildItemID FROM OpportunityKitChildItems WHERE numOppID=@numOppId AND numOppItemID=@numOppItemID AND numOppChildItemID=@numOppChildItemID)
							BEGIN
								-- CLEAR DATA OF PREVIOUS ITERATION
								DELETE FROM @TempKitChildKitSubItems

								-- GET SUB KIT SUB ITEMS DETAIL
								INSERT INTO @TempKitChildKitSubItems
								(
									ID,
									numOppKitChildItemID,
									numKitChildItemCode,
									numKitChildWarehouseItemID,
									numKitChildQty,
									numKitChildQtyShipped
								)
								SELECT 
									ROW_NUMBER() OVER(ORDER BY numOppKitChildItemID),
									ISNULL(numOppKitChildItemID,0),
									ISNULL(OKCI.numItemID,0),
									ISNULL(OKCI.numWareHouseItemId,0),
									((ISNULL(OKCI.numQtyItemsReq_Orig,0) * ISNULL(dbo.fn_UOMConversion(OKCI.numUOMId,OKCI.numItemID,@numDomainID,I.numBaseUnit),1)) * @numChildQty),
									ISNULL(numQtyShipped,0)
								FROM 
									OpportunityKitChildItems OKCI 
								JOIN 
									Item I 
								ON 
									OKCI.numItemID=I.numItemCode    
								WHERE 
									charitemtype='P' 
									AND ISNULL(numWareHouseItemId,0) > 0 
									AND OKCI.numOppID=@numOppID 
									AND OKCI.numOppItemID=@numOppItemID 
									AND OKCI.numOppChildItemID = @numOppChildItemID

								SET @k = 1
								SELECT @CountKitChildItems=COUNT(*) FROM @TempKitChildKitSubItems

								WHILE @k <= @CountKitChildItems
								BEGIN
									SELECT
										@numOppKitChildItemID=numOppKitChildItemID,
										@numKitChildItemCode=numKitChildItemCode,
										@numKitChildWarehouseItemID=numKitChildWarehouseItemID,
										@numKitChildQty=numKitChildQty,
										@numKitChildQtyShipped=numKitChildQtyShipped
									FROM
										@TempKitChildKitSubItems
									WHERE 
										ID = @k

									SELECT @numKitChildAllocation=ISNULL(numAllocation,0),@numKitChildOnHand=ISNULL(numOnHand,0) FROM WareHouseItems WHERE numWareHouseItemID=@numKitChildWarehouseItemID

									IF @tintCommitAllocation = 1 OR (@tintCommitAllocation = 2 AND @bitAllocateInventoryOnPickList=1)--When order is created OR Allocation on pick list add
									BEGIN
										--REMOVE QTY FROM ALLOCATION
										SET @numKitChildAllocation = @numKitChildAllocation - @numKitChildQty

										IF @numKitChildAllocation >= 0
										BEGIN
											UPDATE  
												WareHouseItems
											SET     
												numAllocation = @numKitChildAllocation,
												dtModified = GETDATE() 
											WHERE   
												numWareHouseItemID = @numKitChildWarehouseItemID
										END
										ELSE
										BEGIN
											RAISERROR ('NOTSUFFICIENTQTY_ALLOCATION',16,1);
										END
									END
									ELSE  -- When added fulfillment order
									BEGIN
										--REMOVE QTY FROM ON HAND
										SET @numKitChildOnHand = @numKitChildOnHand - @numKitChildQty

										IF @numKitChildOnHand >= 0
										BEGIN
											UPDATE  
												WareHouseItems
											SET     
												numOnHand = @numKitChildOnHand,
												dtModified = GETDATE() 
											WHERE   
												numWareHouseItemID = @numKitChildWarehouseItemID
										END
										ELSE
										BEGIN
											RAISERROR ('NOTSUFFICIENTQTY_ONHAND',16,1);
										END
									END

									UPDATE  
										OpportunityKitChildItems
									SET 
										numQtyShipped = ISNULL(numQtyShipped,0) + ISNULL(@numKitChildQty,0)
									WHERE   
										numOppKitChildItemID = @numOppKitChildItemID
										AND numOppChildItemID = @numOppChildItemID 
										AND numOppId=@numOppId
										AND numOppItemID=@numOppItemID

									SET @vcKitChildDescription = CONCAT('SO CHILD KIT Qty Shipped (Qty:',CAST(@numKitChildQty AS NUMERIC),' Shipped:',CAST(@numKitChildQtyShipped AS NUMERIC),')')

									EXEC dbo.USP_ManageWareHouseItems_Tracking
										@numWareHouseItemID = @numKitChildWarehouseItemID,
										@numReferenceID = @numOppId,
										@tintRefType = 3,
										@vcDescription = @vcKitChildDescription,
										@numModifiedBy = @numUserCntID,
										@numDomainID = @numDomainID	 

									SET @k = @k + 1
								END
							END 

							UPDATE  
								OpportunityKitItems
							SET 
								numQtyShipped = ISNULL(numQtyShipped,0) + ISNULL(@numChildQty,0)
							WHERE   
								numOppChildItemID = @numOppChildItemID 
								AND numOppId=@numOppId
								AND numOppItemID=@numOppItemID

							SET @vcChildDescription = CONCAT('SO KIT Qty Shipped (Qty:',CAST(@numChildQty AS NUMERIC),' Shipped:',CAST(@numChildQtyShipped AS NUMERIC),')')

							EXEC dbo.USP_ManageWareHouseItems_Tracking
								@numWareHouseItemID = @numChildWarehouseItemID,
								@numReferenceID = @numOppId,
								@tintRefType = 3,
								@vcDescription = @vcChildDescription,
								@numModifiedBy = @numUserCntID,
								@numDomainID = @numDomainID	
						END
						ELSE
						BEGIN
							IF @tintCommitAllocation = 1 OR (@tintCommitAllocation = 2 AND @bitAllocateInventoryOnPickList=1)--When order is created OR Allocation on pick list add
							BEGIN
								--REMOVE QTY FROM ALLOCATION
								SET @numChildAllocation = @numChildAllocation - @numChildQty

								IF @numChildAllocation >= 0
								BEGIN
									UPDATE  
										WareHouseItems
									SET     
										numAllocation = @numChildAllocation,
										dtModified = GETDATE() 
									WHERE   
										numWareHouseItemID = @numChildWarehouseItemID 
								END
								ELSE
								BEGIN
									RAISERROR ('NOTSUFFICIENTQTY_ALLOCATION',16,1);
								END
							END
							ELSE -- When added fulfillment order
							BEGIN
								--REMOVE QTY FROM ONHAND
								SET @numChildOnHand = @numChildOnHand - @numChildQty

								IF @numChildOnHand >= 0
								BEGIN
									UPDATE  
										WareHouseItems
									SET     
										numOnHand = @numChildOnHand,
										dtModified = GETDATE() 
									WHERE   
										numWareHouseItemID = @numChildWarehouseItemID 
								END
								ELSE
								BEGIN
									RAISERROR ('NOTSUFFICIENTQTY_ONHAND',16,1);
								END
							END     	
							
							UPDATE  
								OpportunityKitItems
							SET 
								numQtyShipped = ISNULL(numQtyShipped,0) + ISNULL(@numChildQty,0)
							WHERE   
								numOppChildItemID = @numOppChildItemID 
								AND numOppId=@numOppId
								AND numOppItemID=@numOppItemID

							SET @vcChildDescription = CONCAT('SO KIT Qty Shipped (Qty:',CAST(@numChildQty AS NUMERIC),' Shipped:',CAST(@numChildQtyShipped AS NUMERIC),')')

							EXEC dbo.USP_ManageWareHouseItems_Tracking
								@numWareHouseItemID = @numChildWarehouseItemID,
								@numReferenceID = @numOppId,
								@tintRefType = 3,
								@vcDescription = @vcChildDescription,
								@numModifiedBy = @numUserCntID,
								@numDomainID = @numDomainID
						END

						SET @j = @j + 1
					END
				END
				ELSE
				BEGIN
					IF @bitSerial = 1 OR @bitSerial = 1
					BEGIN
						DELETE FROM @TEMPSERIALLOT

						INSERT INTO @TEMPSERIALLOT
						(
							ID
							,numWarehouseItemID
							,vcSerialLot
							,numQty
						)
						SELECT 
							ROW_NUMBER() OVER(ORDER BY OWSI.numWarehouseItmsDTLID)
							,OWSI.numWarehouseItmsID
							,ISNULL(WID.vcSerialNo,'')
							,ISNULL(OWSI.numQty,0)
						FROM
							OppWarehouseSerializedItem OWSI
						INNER JOIN
							WareHouseItmsDTL WID
						ON
							OWSI.numWarehouseItmsDTLID = WID.numWareHouseItmsDTLID
						INNER JOIN	
							WarehouseItems WI
						ON
							OWSI.numWarehouseItmsID = WI.numWareHouseItemID
						WHERE 
							numOppID=@numOppId 
							AND numOppItemID=@numOppItemID 
							AND ISNULL(numOppBizDocsId,0) = 0

						DECLARE @l INT = 1
						DECLARE @lCount INT
						DECLARE @numTempWarehouseItemID NUMERIC(18,0)
						DECLARE @vcSerialLot VARCHAR(100)
						DECLARE @vcTempDescription VARCHAR(500)
						DECLARE @numTempQty FLOAT
						SELECT @lCount = COUNT(*) FROM @TEMPSERIALLOT

						WHILE @l <= @lCount
						BEGIN
							SELECT
								@numTempWarehouseItemID=numWarehouseItemID
								,@vcSerialLot=vcSerialLot
								,@numTempQty=numQty 
							FROM 
								@TEMPSERIALLOT 
							WHERE 
								ID=@l

							IF @numWarehouseItemID <> @numTempWarehouseItemID
							BEGIN
								IF ISNULL((SELECT numOnHand FROM WareHouseItems WHERE numWareHouseItemID=@numTempWarehouseItemID),0) >= @numTempQty
								BEGIN
									UPDATE 
										WareHouseItems
									SET
										numOnHand = ISNULL(numOnHand,0) - @numTempQty
									WHERE
										numWareHouseItemID=@numTempWarehouseItemID

									SET @vcTempDescription = CONCAT('Serial/Lot(',@vcSerialLot,') Picked Qty(',@numTempQty,')') 

									EXEC dbo.USP_ManageWareHouseItems_Tracking
									@numWareHouseItemID = @numTempWarehouseItemID, --  numeric(9, 0)
									@numReferenceID = @numOppId, --  numeric(9, 0)
									@tintRefType = 3, --  tinyint
									@vcDescription = @vcTempDescription, --  varchar(100)
									@numModifiedBy = @numUserCntID,
									@numDomainID = @numDomainID	 
								END
								ELSE
								BEGIN
									RAISERROR ('NOTSUFFICIENTQTY_ONHAND',16,1);
								END
								
								UPDATE
									WareHouseItems
								SET
									numBackOrder = (CASE WHEN @numTempQty >= ISNULL(numBackOrder,0) THEN 0 ELSE (ISNULL(numBackOrder,0) - @numTempQty) END)
									,numAllocation = ISNULL(numAllocation,0) + (CASE WHEN @numTempQty >= ISNULL(numBackOrder,0) THEN ISNULL(numBackOrder,0) ELSE 0 END)
									,numOnHand = ISNULL(numOnHand,0) + (CASE WHEN @numTempQty >= ISNULL(numBackOrder,0) THEN (@numTempQty - ISNULL(numBackOrder,0)) ELSE 0 END)
								WHERE
									numWareHouseItemID=@numWarehouseItemID

								SET @vcTempDescription = CONCAT('Serial/Lot(',@vcSerialLot,') Received Qty(',@numTempQty,')') 

								EXEC dbo.USP_ManageWareHouseItems_Tracking
								@numWareHouseItemID = @numWarehouseItemID, --  numeric(9, 0)
								@numReferenceID = @numOppId, --  numeric(9, 0)
								@tintRefType = 3, --  tinyint
								@vcDescription = @vcTempDescription, --  varchar(100)
								@numModifiedBy = @numUserCntID,
								@numDomainID = @numDomainID	 
							END

							SET @l = @l + 1
						END
					END
					
					SELECT @numAllocation = ISNULL(numAllocation,0),@numOnHand=ISNULL(numOnHand,0) FROM WareHouseItems WHERE numWareHouseItemID = @numWareHouseItemID
				       
					IF @tintCommitAllocation = 1 OR (@tintCommitAllocation = 2 AND @bitAllocateInventoryOnPickList=1)--When order is created OR Allocation on pick list add
					BEGIN
						--REMOVE QTY FROM ALLOCATION
						SET @numAllocation = @numAllocation - @numQty

						IF (@numAllocation >= 0 )
						BEGIN
							UPDATE  
								WareHouseItems
							SET     
								numAllocation = @numAllocation,
								dtModified = GETDATE() 
							WHERE   
								numWareHouseItemID = @numWareHouseItemID
						END
						ELSE
						BEGIN
							RAISERROR ('NOTSUFFICIENTQTY_ALLOCATION',16,1);
						END
					END
					ELSE 
					BEGIN
						--REMOVE QTY FROM ON HAND
						SET @numOnHand = @numOnHand - @numQty

						IF (@numOnHand >= 0 )
						BEGIN
							UPDATE  
								WareHouseItems
							SET     
								numOnHand = @numOnHand,
								dtModified = GETDATE() 
							WHERE   
								numWareHouseItemID = @numWareHouseItemID
						END
						ELSE
						BEGIN
							RAISERROR ('NOTSUFFICIENTQTY_ONHAND',16,1);
						END
					END
				END

				EXEC dbo.USP_ManageWareHouseItems_Tracking
							@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
							@numReferenceID = @numOppId, --  numeric(9, 0)
							@tintRefType = 3, --  tinyint
							@vcDescription = @vcDescription, --  varchar(100)
							@numModifiedBy = @numUserCntID,
							@numDomainID = @numDomainID	 
			END

			UPDATE  
				OpportunityItems
			SET 
				numQtyShipped = ISNULL(numQtyShipped,0) + ISNULL(@numQty,0)
			WHERE   
				numoppitemtCode = @numOppItemID 
				AND numOppId=@numOppId

			SET @i = @i + 1
		END

		UPDATE OppWarehouseSerializedItem SET numOppBizDocsId=@numOppBizDocID WHERE numOppID=@numOppId AND ISNULL(numOppBizDocsId,0) = 0
		UPDATE OpportunityBizDocs SET bitFulfilled = 1,dtShippedDate=GETUTCDATE() WHERE numOppBizDocsId=@numOppBizDocID
	COMMIT TRANSACTION
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
END


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityBizDocs_FulFill')
DROP PROCEDURE USP_OpportunityBizDocs_FulFill
GO
CREATE PROCEDURE [dbo].[USP_OpportunityBizDocs_FulFill] 
	@numDomainID NUMERIC(18,0),
	@numUserCntID NUMERIC(18,0),
	@numOppId NUMERIC(18,0),
	@numOppBizDocID NUMERIC(18,0),
	@vcItems VARCHAR(MAX),
	@dtShippedDate DATETIME
AS  
BEGIN 
BEGIN TRY
	DECLARE @vcItemType AS CHAR(1)
	DECLARE @bitDropShip AS BIT
	DECLARE @numOppItemID AS INT
	DECLARE @numWarehouseItemID AS INT
	DECLARE @numQty AS FLOAT
	DECLARE @numQtyShipped AS FLOAT
	DECLARE @numXmlQty AS FLOAT
	DECLARE @vcSerialLot AS VARCHAR(MAX)
	DECLARE @numSerialCount AS INT
	DECLARE @bitSerial AS BIT
	DECLARE @bitLot AS BIT
	DECLARE @bitAssembly AS BIT
	DECLARE @bitKit AS BIT

	DECLARE @i AS INT = 1
	DECLARE @COUNT AS INT  
	DECLARE @numNewQtyShipped AS FLOAT


	/* VERIFY BIZDOC IS EXISTS OR NOT */
	IF NOT EXISTS (SELECT numOppBizDocsId FROM OpportunityBizDocs WHERE numOppBizDocsId = @numOppBizDocID)
	BEGIN
		RAISERROR('FULFILLMENT_BIZDOC_IS_DELETED',16,1)
	END

	/* VERIFY WHETHER BIZDOC IS ALREADY FULLFILLED BY SOMEBODY ELSE */
	IF EXISTS (SELECT numOppBizDocsId FROM OpportunityBizDocs WHERE numOppBizDocsId = @numOppBizDocID AND bitFulfilled = 1) 
	BEGIN
		RAISERROR('FULFILLMENT_BIZDOC_ALREADY_FULFILLED',16,1)
	END

	--IF EXISTS(SELECT numWOId FROm WorkOrder WHERE numOppId=@numOppId AND numWOStatus <> 23184) --WORK ORDER NOT YET COMPLETED
	--BEGIN
	--	RAISERROR('WORK_ORDER_NOT_COMPLETED',16,1)
	--END

	BEGIN TRANSACTION

		BEGIN /* REVERT SEIRAL/LOT# NUMBER ASSIGNED FROM PRODCUT&SERVICES TAB */
			UPDATE WHIDL
				SET WHIDL.numQty = (CASE WHEN Item.bitLotNo = 1 THEN ISNULL(WHIDL.numQty,0) + ISNULL(OWSI.numQty,0) ELSE 1 END)
			FROM 
				WareHouseItmsDTL WHIDL
			INNER JOIN
				OppWarehouseSerializedItem OWSI
			ON
				WHIDL.numWareHouseItmsDTLID = OWSI.numWarehouseItmsDTLID
				AND WHIDL.numWareHouseItemID = OWSI.numWarehouseItmsID
			INNER JOIN
				WareHouseItems
			ON
				WHIDL.numWareHouseItemID = WareHouseItems.numWareHouseItemID
			INNER JOIN
				Item
			ON
				WareHouseItems.numItemID = Item.numItemCode
			WHERE
				OWSI.numOppID=@numOppId
				AND OWSI.numOppBizDocsId IS NULL
		END

		BEGIN /* GET FULFILLMENT BIZDOC ITEMS */
			DECLARE @TempFinalTable TABLE
			(
				ID INT IDENTITY(1,1),
				numItemCode NUMERIC(18,0),
				vcItemType CHAR(1),
				bitSerial BIT,
				bitLot BIT,
				bitAssembly BIT,
				bitKit BIT,
				numOppItemID NUMERIC(18,0),
				numWarehouseItemID NUMERIC(18,0),
				numQtyShipped FLOAT,
				numQty FLOAT,
				bitDropShip BIT,
				vcSerialLot VARCHAR(MAX)
			)

			INSERT INTO @TempFinalTable
			(
				numItemCode,
				vcItemType,
				bitSerial,
				bitLot,
				bitAssembly,
				bitKit,
				numOppItemID,
				numWarehouseItemID,
				numQtyShipped,
				numQty,
				bitDropShip,
				vcSerialLot
			)
			SELECT
				OpportunityBizDocItems.numItemCode,
				ISNULL(Item.charItemType,''),
				ISNULL(Item.bitSerialized,0),
				ISNULL(Item.bitLotNo,0),
				ISNULL(Item.bitAssembly,0),
				ISNULL(Item.bitKitParent,0),
				ISNULL(OpportunityItems.numoppitemtcode,0),
				ISNULL(OpportunityBizDocItems.numWarehouseItmsID,0),
				ISNULL(OpportunityItems.numQtyShipped,0),
				ISNULL(OpportunityBizDocItems.numUnitHour,0),
				ISNULL(OpportunityItems.bitDropShip,0),
				''
			FROM
				OpportunityBizDocs
			INNER JOIN
				OpportunityBizDocItems
			ON
				OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
			INNER JOIN
				OpportunityItems
			ON
				OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtCode
			INNER JOIN
				Item
			ON
				OpportunityBizDocItems.numItemCode = Item.numItemCode
			WHERE
				numOppBizDocsId = @numOppBizDocID
				AND ISNULL(OpportunityItems.bitDropShip,0) = 0
				AND ISNULL(OpportunityItems.numWarehouseItmsID,0) > 0
		END

		BEGIN /* CONVERT vcItems XML to TABLE */
			DECLARE @TempItemXML TABLE
			(
				numOppItemID NUMERIC(18,0),
				numQty FLOAT,
				numWarehouseItemID NUMERIC(18,0),
				vcSerialLot VARCHAR(MAX)
			)

			DECLARE @idoc int
			EXEC sp_xml_preparedocument @idoc OUTPUT, @vcItems;

			INSERT INTO
				@TempItemXML
			SELECT
				numOppItemID,
				numQty,
				numWarehouseItemID,
				vcSerialLot
			FROM 
				OPENXML (@idoc, '/Items/Item',2)
			WITH 
			(
				numOppItemID NUMERIC(18,0),
				numQty FLOAT,
				numWarehouseItemID NUMERIC(18,0),
				vcSerialLot VARCHAR(MAX)
			);
		END

		BEGIN /* VALIDATE ALL THE DETAILS BEFORE FULFILLING ITEMS */
		
			SELECT @COUNT=COUNT(*) FROM @TempFinalTable

			DECLARE @TempLot TABLE
			(
				vcLotNo VARCHAR(100),
				numQty FLOAT 
			)

			WHILE @i <= @COUNT
			BEGIN
				SELECT
					@numOppItemID=numOppItemID,
					@numWarehouseItemID=numWarehouseItemID,
					@numQty=numQty,
					@bitSerial=bitSerial,
					@bitLot=bitLot 
				FROM 
					@TempFinalTable 
				WHERE 
					ID = @i

				--GET QUENTITY RECEIVED FROM XML PARAMETER
				SELECT 
					@numXmlQty = numQty, 
					@vcSerialLot=vcSerialLot, 
					@numSerialCount = (CASE WHEN (@bitSerial = 1 OR @bitLot = 1) THEN (SELECT COUNT(*) FROM dbo.SplitString(vcSerialLot,',')) ELSE 0 END)  
				FROM 
					@TempItemXML 
				WHERE 
					numOppItemID = @numOppItemID 

				--CHECK IF QUANTITY OF ITEM IN OpportunityBizDocItems TBALE IS SAME AS QUANTITY SUPPLIED FROM XML
				IF @numQty <> @numXmlQty
				BEGIN
					RAISERROR('QTY_MISMATCH',16,1)
				END

				--IF ITEM IS SERIAL ITEM CHEKC THAT VALID AND REQUIRED NUMBER OF SERIALS ARE PROVIDED BY USER
				IF @bitSerial = 1 
				BEGIN
					IF @numSerialCount <> @numQty
					BEGIN
						RAISERROR('REQUIRED_SERIALS_NOT_PROVIDED',16,1)
					END
					ELSE
					BEGIN
						IF (SELECT COUNT(*) FROM WareHouseItmsDTL WHERE numWareHouseItemID = @numWarehouseItemID AND numQty > 0 AND vcSerialNo IN (SELECT OutParam FROM dbo.SplitString(@vcSerialLot,','))) <> @numSerialCount
						BEGIN
							RAISERROR('INVALID_SERIAL_NUMBERS',16,1)
						END
					END
				END

				IF @bitLot = 1
				BEGIN
					-- DELETE DATA OF PREVIOUS ITERATION FROM TABLE
					DELETE FROM @TempLot

					--CONVERTS COMMA SEPERATED STRING TO LOTNo AND QTY 
					INSERT INTO
						@TempLot
					SELECT
						SUBSTRING(OutParam,0,PATINDEX('%(%', OutParam)),
						CAST(SUBSTRING(OutParam,(PATINDEX('%(%', OutParam) + 1),(PATINDEX('%)%', OutParam) - PATINDEX('%(%', OutParam) - 1)) AS INT)
					FROM
						dbo.SplitString(@vcSerialLot,',')

					--CHECKS WHETHER PROVIDED QTY OF LOT NO IS SAME IS REQUIRED QTY
					IF (SELECT SUM(numQty) FROM @TempLot) <> @numQty
					BEGIN
						RAISERROR('REQUIRED_LOTNO_NOT_PROVIDED',16,1)
					END
					ELSE
					BEGIN
						-- CHECKS WHETHER LOT NO EXISTS IN WareHouseItmsDTL
						IF (SELECT COUNT(*) FROM WareHouseItmsDTL WHERE numWareHouseItemID = @numWarehouseItemID AND vcSerialNo IN (SELECT vcLotNo FROM @TempLot)) <> (SELECT COUNT(*) FROM @TempLot)
						BEGIN
							RAISERROR('INVALID_LOT_NUMBERS',16,1)
						END
						ELSE
						BEGIN
							-- CHECKS WHETHER LOT NO HAVE ENOUGH QTY TO FULFILL ITEM QTY
							IF (SELECT 
									COUNT(*) 
								FROM 
									WareHouseItmsDTL 
								INNER JOIN
									@TempLot TEMPLOT
								ON
									WareHouseItmsDTL.vcSerialNo = TEMPLOT.vcLotNo
								WHERE 
									numWareHouseItemID = @numWarehouseItemID
									AND TEMPLOT.numQty > WareHouseItmsDTL.numQty) > 0
							BEGIN
								RAISERROR('SOME_LOTNO_DO_NOT_HAVE_ENOUGH_QTY',16,1)
							END
						END
					END
				END

		
				UPDATE @TempFinalTable SET vcSerialLot = @vcSerialLot WHERE ID=@i

				SET @i = @i + 1
			END
		END
	
		SET @i = 1
		SELECT @COUNT = COUNT(*) FROM @TempFinalTable
		DECLARE @vcDescription AS VARCHAR(300)
		DECLARE @numAllocation AS FLOAT

		DECLARE @j INT = 0
		DECLARE @ChildCount AS INT
		DECLARE @vcChildDescription AS VARCHAR(300)
		DECLARE @numChildAllocation AS FLOAT
		DECLARE @numOppChildItemID AS INT
		DECLARE @numChildItemCode AS INT
		DECLARE @bitChildIsKit AS BIT
		DECLARE @numChildWarehouseItemID AS INT
		DECLARE @numChildQty AS FLOAT
		DECLARE @numChildQtyShipped AS FLOAT

		DECLARE @TempKitSubItems TABLE
		(
			ID INT,
			numOppChildItemID NUMERIC(18,0),
			numChildItemCode NUMERIC(18,0),
			bitChildIsKit BIT,
			numChildWarehouseItemID NUMERIC(18,0),
			numChildQty FLOAT,
			numChildQtyShipped FLOAT
		)


		DECLARE @k AS INT = 1
		DECLARE @CountKitChildItems AS INT
		DECLARE @vcKitChildDescription AS VARCHAR(300)
		DECLARE @numKitChildAllocation AS FLOAT
		DECLARE @numOppKitChildItemID AS NUMERIC(18,0)
		DECLARE @numKitChildItemCode AS NUMERIC(18,0)
		DECLARE @numKitChildWarehouseItemID AS NUMERIC(18,0)
		DECLARE @numKitChildQty AS FLOAT
		DECLARE @numKitChildQtyShipped AS FLOAT

		DECLARE @TempKitChildKitSubItems TABLE
		(
			ID INT,
			numOppKitChildItemID NUMERIC(18,0),
			numKitChildItemCode NUMERIC(18,0),
			numKitChildWarehouseItemID NUMERIC(18,0),
			numKitChildQty FLOAT,
			numKitChildQtyShipped FLOAT
		)

		WHILE @i <= @COUNT
		BEGIN
			SELECT 
				@vcItemType=vcItemType,
				@bitSerial=bitSerial,
				@bitLot=bitLot,
				@bitAssembly=bitAssembly,
				@bitKit=bitKit,
				@bitDropShip=bitDropShip,
				@numOppItemID=numOppItemID,
				@numQty=numQty,
				@numQtyShipped = numQtyShipped,
				@numWarehouseItemID=numWarehouseItemID,
				@vcSerialLot = vcSerialLot
			FROM 
				@TempFinalTable 
			WHERE 
				ID = @i

			--MAKE INVENTIRY CHANGES IF INVENTORY ITEM
			IF (UPPER(@vcItemType) = 'P' AND @bitDropShip = 0)
			BEGIN
				SET @vcDescription=CONCAT('SO Qty Shipped (Qty:',@numQty,' Shipped:',@numQtyShipped,')')
						 
				IF @bitKit = 1
				BEGIN
					-- CLEAR DATA OF PREVIOUS ITERATION
					DELETE FROM @TempKitSubItems

					-- GET KIT SUB ITEMS DETAIL
					INSERT INTO @TempKitSubItems
					(
						ID,
						numOppChildItemID,
						numChildItemCode,
						bitChildIsKit,
						numChildWarehouseItemID,
						numChildQty,
						numChildQtyShipped
					)
					SELECT 
						ROW_NUMBER() OVER(ORDER BY numOppChildItemID),
						ISNULL(numOppChildItemID,0),
						ISNULL(I.numItemCode,0),
						ISNULL(I.bitKitParent,0),
						ISNULL(numWareHouseItemId,0),
						((ISNULL(numQtyItemsReq_Orig,0) * ISNULL(dbo.fn_UOMConversion(OKI.numUOMId,OKI.numChildItemID,@numDomainID,I.numBaseUnit),1)) * @numQty),
						ISNULL(numQtyShipped,0)
					FROM 
						OpportunityKitItems OKI 
					JOIN 
						Item I 
					ON 
						OKI.numChildItemID=I.numItemCode    
					WHERE 
						charitemtype='P' 
						AND ISNULL(numWareHouseItemId,0) > 0 
						AND OKI.numOppID=@numOppID 
						AND OKI.numOppItemID=@numOppItemID 

					SET @j = 1
					SELECT @ChildCount=COUNT(*) FROM @TempKitSubItems

					--LOOP OVER ALL CHILD ITEMS AND RELESE ALLOCATION
					WHILE @j <= @ChildCount
					BEGIN
						SELECT
							@numOppChildItemID=numOppChildItemID,
							@numChildItemCode=numChildItemCode,
							@bitChildIsKit=bitChildIsKit,
							@numChildWarehouseItemID=numChildWarehouseItemID,
							@numChildQty=numChildQty,
							@numChildQtyShipped=numChildQtyShipped
						FROM
							@TempKitSubItems
						WHERE 
							ID = @j

						SELECT @numChildAllocation=ISNULL(numAllocation,0) FROM WareHouseItems WHERE numWareHouseItemID=@numChildWarehouseItemID

						-- IF CHILD ITEM IS KIT THEN LOOK OUT FOR ITS CHILD ITEMS WHICH ARE STORED IN OpportunityKitChildItems TABLE
						IF @bitChildIsKit = 1
						BEGIN
							-- IF KIT IS ADDED WITHIN KIT THEN THIS SUB KIT HAS ITS OWN ITEM CONFIGURATION WHICH ID STORED IN OpportunityKitChildItems
							IF EXISTS (SELECT numOppKitChildItemID FROM OpportunityKitChildItems WHERE numOppID=@numOppId AND numOppItemID=@numOppItemID AND numOppChildItemID=@numOppChildItemID)
							BEGIN
								-- CLEAR DATA OF PREVIOUS ITERATION
								DELETE FROM @TempKitChildKitSubItems

								-- GET SUB KIT SUB ITEMS DETAIL
								INSERT INTO @TempKitChildKitSubItems
								(
									ID,
									numOppKitChildItemID,
									numKitChildItemCode,
									numKitChildWarehouseItemID,
									numKitChildQty,
									numKitChildQtyShipped
								)
								SELECT 
									ROW_NUMBER() OVER(ORDER BY numOppKitChildItemID),
									ISNULL(numOppKitChildItemID,0),
									ISNULL(OKCI.numItemID,0),
									ISNULL(OKCI.numWareHouseItemId,0),
									((ISNULL(OKCI.numQtyItemsReq_Orig,0) * ISNULL(dbo.fn_UOMConversion(OKCI.numUOMId,OKCI.numItemID,@numDomainID,I.numBaseUnit),1)) * @numChildQty),
									ISNULL(numQtyShipped,0)
								FROM 
									OpportunityKitChildItems OKCI 
								JOIN 
									Item I 
								ON 
									OKCI.numItemID=I.numItemCode    
								WHERE 
									charitemtype='P' 
									AND ISNULL(numWareHouseItemId,0) > 0 
									AND OKCI.numOppID=@numOppID 
									AND OKCI.numOppItemID=@numOppItemID 
									AND OKCI.numOppChildItemID = @numOppChildItemID

								SET @k = 1
								SELECT @CountKitChildItems=COUNT(*) FROM @TempKitChildKitSubItems

								WHILE @k <= @CountKitChildItems
								BEGIN
									SELECT
										@numOppKitChildItemID=numOppKitChildItemID,
										@numKitChildItemCode=numKitChildItemCode,
										@numKitChildWarehouseItemID=numKitChildWarehouseItemID,
										@numKitChildQty=numKitChildQty,
										@numKitChildQtyShipped=numKitChildQtyShipped
									FROM
										@TempKitChildKitSubItems
									WHERE 
										ID = @k

									SELECT @numKitChildAllocation=ISNULL(numAllocation,0) FROM WareHouseItems WHERE numWareHouseItemID=@numKitChildWarehouseItemID

									--REMOVE QTY FROM ALLOCATION
									SET @numKitChildAllocation = @numKitChildAllocation - @numKitChildQty

									IF @numKitChildAllocation >= 0
									BEGIN
										UPDATE  
											WareHouseItems
										SET     
											numAllocation = @numKitChildAllocation,
											dtModified = GETDATE() 
										WHERE   
											numWareHouseItemID = @numKitChildWarehouseItemID      	
							
										UPDATE  
											OpportunityKitChildItems
										SET 
											numQtyShipped = ISNULL(numQtyShipped,0) + ISNULL(@numKitChildQty,0)
										WHERE   
											numOppKitChildItemID = @numOppKitChildItemID
											AND numOppChildItemID = @numOppChildItemID 
											AND numOppId=@numOppId
											AND numOppItemID=@numOppItemID

										SET @vcKitChildDescription = CONCAT('SO CHILD KIT Qty Shipped (Qty:',@numKitChildQty,' Shipped:',@numKitChildQtyShipped,')')

										EXEC dbo.USP_ManageWareHouseItems_Tracking
											@numWareHouseItemID = @numKitChildWarehouseItemID,
											@numReferenceID = @numOppId,
											@tintRefType = 3,
											@vcDescription = @vcKitChildDescription,
											@numModifiedBy = @numUserCntID,
											@numDomainID = @numDomainID	 
									END
									ELSE
									BEGIN
										RAISERROR ('NOTSUFFICIENTQTY_ALLOCATION',16,1);
									END

									SET @k = @k + 1
								END
							END

							UPDATE  
								OpportunityKitItems
							SET 
								numQtyShipped = ISNULL(numQtyShipped,0) + ISNULL(@numChildQty,0)
							WHERE   
								numOppChildItemID = @numOppChildItemID 
								AND numOppId=@numOppId
								AND numOppItemID=@numOppItemID

							SET @vcChildDescription = CONCAT('SO KIT Qty Shipped (Qty:',@numChildQty,' Shipped:',@numChildQtyShipped,')')

							EXEC dbo.USP_ManageWareHouseItems_Tracking
								@numWareHouseItemID = @numChildWarehouseItemID,
								@numReferenceID = @numOppId,
								@tintRefType = 3,
								@vcDescription = @vcChildDescription,
								@numModifiedBy = @numUserCntID,
								@numDomainID = @numDomainID	 
						END
						ELSE 
						BEGIN
							--REMOVE QTY FROM ALLOCATION
							SET @numChildAllocation = @numChildAllocation - @numChildQty
							IF @numChildAllocation >= 0
							BEGIN
								UPDATE  
									WareHouseItems
								SET     
									numAllocation = @numChildAllocation,
									dtModified = GETDATE() 
								WHERE   
									numWareHouseItemID = @numChildWarehouseItemID      	
							
								UPDATE  
									OpportunityKitItems
								SET 
									numQtyShipped = ISNULL(numQtyShipped,0) + ISNULL(@numChildQty,0)
								WHERE   
									numOppChildItemID = @numOppChildItemID 
									AND numOppId=@numOppId
									AND numOppItemID=@numOppItemID

								SET @vcChildDescription = CONCAT('SO KIT Qty Shipped (Qty:',@numChildQty,' Shipped:',@numChildQtyShipped,')')

								EXEC dbo.USP_ManageWareHouseItems_Tracking
									@numWareHouseItemID = @numChildWarehouseItemID,
									@numReferenceID = @numOppId,
									@tintRefType = 3,
									@vcDescription = @vcChildDescription,
									@numModifiedBy = @numUserCntID,
									@numDomainID = @numDomainID	 
							END
							ELSE
							BEGIN
								RAISERROR ('NOTSUFFICIENTQTY_ALLOCATION',16,1);
							END
						END

						SET @j = @j + 1
					END
				END
				ELSE
				BEGIN
					SELECT @numAllocation = ISNULL(numAllocation, 0) FROM WareHouseItems WHERE numWareHouseItemID = @numWareHouseItemID
				       
					--REMOVE QTY FROM ALLOCATION
					SET @numAllocation = @numAllocation - @numQty	
				       
					IF (@numAllocation >= 0 )
					BEGIN
						UPDATE  
							WareHouseItems
						SET     
							numAllocation = @numAllocation,
							dtModified = GETDATE() 
						WHERE   
							numWareHouseItemID = @numWareHouseItemID      	

						IF @bitSerial = 1
						BEGIN								
							DELETE FROM OppWarehouseSerializedItem WHERE numOppID=@numOppID AND numOppItemID=@numOppItemID AND numWarehouseItmsID=@numWarehouseItemID

							INSERT INTO OppWarehouseSerializedItem
							(
								numWarehouseItmsDTLID,
								numOppID,
								numOppItemID,
								numWarehouseItmsID,
								numQty,
								numOppBizDocsId	
							)
							SELECT
								(SELECT TOP 1 numWarehouseItmsDTLID FROM WareHouseItmsDTL WHERE numWareHouseItemID=@numWarehouseItemID AND vcSerialNo=OutParam AND  numQty = 1),
								@numOppID,
								@numOppItemID,
								@numWarehouseItemID,
								1,
								@numOppBizDocID
							FROM
								dbo.SplitString(@vcSerialLot,',')


							UPDATE WareHouseItmsDTL SET numQty=0 WHERE numWareHouseItemID=@numWarehouseItemID AND vcSerialNo IN (SELECT OutParam FROM dbo.SplitString(@vcSerialLot,','))
						END

						IF @bitLot = 1
						BEGIN
							DELETE FROM @TempLot

							INSERT INTO
								@TempLot
							SELECT
								SUBSTRING(OutParam,0,PATINDEX('%(%', OutParam)),
								CAST(SUBSTRING(OutParam,(PATINDEX('%(%', OutParam) + 1),(PATINDEX('%)%', OutParam) - PATINDEX('%(%', OutParam) - 1)) AS INT)
							FROM
								dbo.SplitString(@vcSerialLot,',')

							DELETE FROM OppWarehouseSerializedItem WHERE numOppID=@numOppID AND numOppItemID=@numOppItemID AND numWarehouseItmsID=@numWarehouseItemID

							INSERT INTO OppWarehouseSerializedItem
							(
								numWarehouseItmsDTLID,
								numOppID,
								numOppItemID,
								numWarehouseItmsID,
								numQty,
								numOppBizDocsId	
							)
							SELECT
								(SELECT TOP 1 numWarehouseItmsDTLID FROM WareHouseItmsDTL WHERE numWareHouseItemID=@numWarehouseItemID AND vcSerialNo=TEMPLOT.vcLotNo AND numQty >= TEMPLOT.numQty),
								@numOppID,
								@numOppItemID,
								@numWarehouseItemID,
								TEMPLOT.numQty,
								@numOppBizDocID
							FROM
								@TempLot TEMPLOT


							UPDATE	TEMPWareHouseItmsDTL
								SET TEMPWareHouseItmsDTL.numQty = ISNULL(TEMPWareHouseItmsDTL.numQty,0) - ISNULL(TEMPLOT.numQty,0)
							FROM
								WareHouseItmsDTL TEMPWareHouseItmsDTL
							INNER JOIN
								@TempLot TEMPLOT
							ON
								TEMPWareHouseItmsDTL.vcSerialNo = TEMPLOT.vcLotNo
							WHERE
								TEMPWareHouseItmsDTL.numWareHouseItemID = @numWarehouseItemID
						END
					END
					ELSE
					BEGIN
						PRINT 'PARENT: ' + CAST(@numChildWarehouseItemID AS VARCHAR)
						RAISERROR ('NOTSUFFICIENTQTY_ALLOCATION',16,1);
					END
				END

				EXEC dbo.USP_ManageWareHouseItems_Tracking
							@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
							@numReferenceID = @numOppId, --  numeric(9, 0)
							@tintRefType = 3, --  tinyint
							@vcDescription = @vcDescription, --  varchar(100)
							@numModifiedBy = @numUserCntID,
							@numDomainID = @numDomainID	 
			END

			UPDATE  
				OpportunityItems
			SET 
				numQtyShipped = ISNULL(numQtyShipped,0) + ISNULL(@numQty,0)
			WHERE   
				numoppitemtCode = @numOppItemID 
				AND numOppId=@numOppId

			SET @i = @i + 1
		END

		UPDATE OpportunityBizDocs SET bitFulfilled = 1,dtShippedDate=@dtShippedDate WHERE numOppBizDocsId=@numOppBizDocID
	COMMIT TRANSACTION
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
END

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityBizDocs_RevertFulFillment')
DROP PROCEDURE USP_OpportunityBizDocs_RevertFulFillment
GO
CREATE PROCEDURE [dbo].[USP_OpportunityBizDocs_RevertFulFillment] 
	@numDomainID NUMERIC(18,0),
	@numUserCntID NUMERIC(18,0),
	@numOppId NUMERIC(18,0),
	@numOppBizDocID NUMERIC(18,0)
AS  
BEGIN 
BEGIN TRY
	DECLARE @tintCommitAllocation AS TINYINT
	DECLARE @bitAllocateInventoryOnPickList AS BIT

	SELECT @tintCommitAllocation=ISNULL(tintCommitAllocation,1) FROM Domain WHERE numDomainId=@numDomainID
	SELECT @bitAllocateInventoryOnPickList=ISNULL(bitAllocateInventoryOnPickList,0) FROM OpportunityMaster INNER JOIN DivisionMaster ON OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID WHERE numOppID=@numOppID

	DECLARE @vcItemType AS CHAR(1)
	DECLARE @bitDropShip AS BIT
	DECLARE @bitSerial AS BIT
	DECLARE @bitLot AS BIT
	DECLARE @bitAssembly AS BIT
	DECLARE @bitKit AS BIT
	DECLARE @numOppItemID AS INT
	DECLARE @numWarehouseItemID AS INT
	DECLARE @numQty AS FLOAT
	DECLARE @numQtyShipped AS FLOAT
	DECLARE @vcDescription AS VARCHAR(300)

	DECLARE @TEMPSERIALLOT TABLE
	(
		ID INT
		,numWarehouseItemID NUMERIC(18,0)
		,vcSerialLot VARCHAR(100)
		,numQty FLOAT
	)

	DECLARE @j INT = 0
	DECLARE @ChildCount AS INT
	DECLARE @vcChildDescription AS VARCHAR(300)
	DECLARE @numOppChildItemID AS INT
	DECLARE @numChildItemCode AS INT
	DECLARE @bitChildIsKit AS BIT
	DECLARE @numChildWarehouseItemID AS INT
	DECLARE @numChildQty AS FLOAT
	DECLARE @numChildQtyShipped AS FLOAT

	DECLARE @TempKitSubItems TABLE
	(
		ID INT,
		numOppChildItemID NUMERIC(18,0),
		numChildItemCode NUMERIC(18,0),
		bitChildIsKit BIT,
		numChildWarehouseItemID NUMERIC(18,0),
		numChildQty FLOAT,
		numChildQtyShipped FLOAT
	)

	DECLARE @k AS INT = 1
	DECLARE @CountKitChildItems AS INT
	DECLARE @vcKitChildDescription AS VARCHAR(300)
	DECLARE @numOppKitChildItemID AS NUMERIC(18,0)
	DECLARE @numKitChildItemCode AS NUMERIC(18,0)
	DECLARE @numKitChildWarehouseItemID AS NUMERIC(18,0)
	DECLARE @numKitChildQty AS FLOAT
	DECLARE @numKitChildQtyShipped AS FLOAT

	DECLARE @TempKitChildKitSubItems TABLE
	(
		ID INT,
		numOppKitChildItemID NUMERIC(18,0),
		numKitChildItemCode NUMERIC(18,0),
		numKitChildWarehouseItemID NUMERIC(18,0),
		numKitChildQty FLOAT,
		numKitChildQtyShipped FLOAT
	)

	DECLARE @TempFinalTable TABLE
	(
		ID INT IDENTITY(1,1),
		numItemCode NUMERIC(18,0),
		vcItemType CHAR(1),
		bitSerial BIT,
		bitLot BIT,
		bitAssembly BIT,
		bitKit BIT,
		numOppItemID NUMERIC(18,0),
		numWarehouseItemID NUMERIC(18,0),
		numQtyShipped FLOAT,
		numQty FLOAT,
		bitDropShip BIT
	)

	INSERT INTO @TempFinalTable
	(
		numItemCode,
		vcItemType,
		bitSerial,
		bitLot,
		bitAssembly,
		bitKit,
		numOppItemID,
		numWarehouseItemID,
		numQtyShipped,
		numQty,
		bitDropShip
	)
	SELECT
		OpportunityBizDocItems.numItemCode,
		ISNULL(Item.charItemType,''),
		ISNULL(Item.bitSerialized,0),
		ISNULL(Item.bitLotNo,0),
		ISNULL(Item.bitAssembly,0),
		ISNULL(Item.bitKitParent,0),
		ISNULL(OpportunityItems.numoppitemtcode,0),
		ISNULL(OpportunityItems.numWarehouseItmsID,0),
		ISNULL(OpportunityItems.numQtyShipped,0),
		ISNULL(OpportunityBizDocItems.numUnitHour,0),
		ISNULL(OpportunityItems.bitDropShip,0)
	FROM
		OpportunityBizDocs
	INNER JOIN
		OpportunityBizDocItems
	ON
		OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
	INNER JOIN
		OpportunityItems
	ON
		OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtCode
	INNER JOIN
		Item
	ON
		OpportunityBizDocItems.numItemCode = Item.numItemCode
	WHERE
		numOppBizDocsId = @numOppBizDocID

	BEGIN TRANSACTION
		DECLARE @i INT = 1
		DECLARE @COUNT INT = 0
		SELECT @COUNT = COUNT(*) FROM @TempFinalTable

		WHILE @i <= @COUNT
		BEGIN
			SELECT 
				@vcItemType=vcItemType,
				@bitSerial=bitSerial,
				@bitLot=bitLot,
				@bitAssembly=bitAssembly,
				@bitKit=bitKit,
				@bitDropShip=bitDropShip,
				@numOppItemID=numOppItemID,
				@numQty=numQty,
				@numQtyShipped = numQtyShipped,
				@numWarehouseItemID=numWarehouseItemID
			FROM 
				@TempFinalTable 
			WHERE 
				ID = @i

			IF @numQty > @numQtyShipped
			BEGIN
				RAISERROR ('INVALID_SHIPPED_QTY',16,1);
			END

			--REVERT QUANTITY TO ON ALLOCATION IF INVENTORY ITEM
			IF (UPPER(@vcItemType) = 'P' AND @bitDropShip = 0)
			BEGIN
				SET @vcDescription=CONCAT('SO Qty Shipped Return (Qty:',CAST(@numQty AS NUMERIC),' Shipped:',CAST(@numQtyShipped AS NUMERIC),')')
				
				IF @bitKit = 1
				BEGIN
					-- CLEAR DATA OF PREVIOUS ITERATION
					DELETE FROM @TempKitSubItems

					-- GET KIT SUB ITEMS DETAIL
					INSERT INTO @TempKitSubItems
					(
						ID,
						numOppChildItemID,
						numChildItemCode,
						bitChildIsKit,
						numChildWarehouseItemID,
						numChildQty,
						numChildQtyShipped
					)
					SELECT 
						ROW_NUMBER() OVER(ORDER BY numOppChildItemID),
						ISNULL(numOppChildItemID,0),
						ISNULL(I.numItemCode,0),
						ISNULL(I.bitKitParent,0),
						ISNULL(OKI.numWareHouseItemId,0),
						((ISNULL(OKI.numQtyItemsReq_Orig,0) * ISNULL(dbo.fn_UOMConversion(OKI.numUOMId,OKI.numChildItemID,@numDomainID,I.numBaseUnit),1)) * @numQty),
						ISNULL(OKI.numQtyShipped,0)
					FROM 
						OpportunityKitItems OKI 
					JOIN 
						Item I 
					ON 
						OKI.numChildItemID=I.numItemCode    
					WHERE 
						charitemtype='P' 
						AND ISNULL(numWareHouseItemId,0) > 0 
						AND OKI.numOppID=@numOppID 
						AND OKI.numOppItemID=@numOppItemID 

					SET @j = 1
					SELECT @ChildCount=COUNT(*) FROM @TempKitSubItems

					--LOOP OVER ALL CHILD ITEMS AND REVERT ALLOCATION
					WHILE @j <= @ChildCount
					BEGIN
					
						SELECT
							@numOppChildItemID=numOppChildItemID,
							@numChildItemCode=numChildItemCode,
							@bitChildIsKit=bitChildIsKit,
							@numChildWarehouseItemID=numChildWarehouseItemID,
							@numChildQty=numChildQty,
							@numChildQtyShipped=numChildQtyShipped
						FROM
							@TempKitSubItems
						WHERE 
							ID = @j

						IF @numChildQty > @numChildQtyShipped
						BEGIN
							RAISERROR ('INVALID_KIT_SUB_ITEM_SHIPPED_QTY',16,1);
						END

						IF @bitChildIsKit = 1
						BEGIN
							-- IF KIT IS ADDED WITHIN KIT THEN THIS SUB KIT HAS ITS OWN ITEM CONFIGURATION WHICH ID STORED IN OpportunityKitChildItems
							IF EXISTS (SELECT numOppKitChildItemID FROM OpportunityKitChildItems WHERE numOppID=@numOppId AND numOppItemID=@numOppItemID AND numOppChildItemID=@numOppChildItemID)
							BEGIN
								-- CLEAR DATA OF PREVIOUS ITERATION
								DELETE FROM @TempKitChildKitSubItems

								-- GET SUB KIT SUB ITEMS DETAIL
								INSERT INTO @TempKitChildKitSubItems
								(
									ID,
									numOppKitChildItemID,
									numKitChildItemCode,
									numKitChildWarehouseItemID,
									numKitChildQty,
									numKitChildQtyShipped
								)
								SELECT 
									ROW_NUMBER() OVER(ORDER BY numOppKitChildItemID),
									ISNULL(numOppKitChildItemID,0),
									ISNULL(OKCI.numItemID,0),
									ISNULL(OKCI.numWareHouseItemId,0),
									((ISNULL(OKCI.numQtyItemsReq_Orig,0) * ISNULL(dbo.fn_UOMConversion(OKCI.numUOMId,OKCI.numItemID,@numDomainID,I.numBaseUnit),1)) * @numChildQty),
									ISNULL(numQtyShipped,0)
								FROM 
									OpportunityKitChildItems OKCI 
								JOIN 
									Item I 
								ON 
									OKCI.numItemID=I.numItemCode    
								WHERE 
									charitemtype='P' 
									AND ISNULL(numWareHouseItemId,0) > 0 
									AND OKCI.numOppID=@numOppID 
									AND OKCI.numOppItemID=@numOppItemID 
									AND OKCI.numOppChildItemID = @numOppChildItemID

								SET @k = 1
								SELECT @CountKitChildItems=COUNT(*) FROM @TempKitChildKitSubItems


								--LOOP OVER ALL CHILD ITEMS AND REVERT ALLOCATION
								WHILE @k <= @CountKitChildItems
								BEGIN
									SELECT
										@numOppKitChildItemID=numOppKitChildItemID,
										@numKitChildItemCode=numKitChildItemCode,
										@numKitChildWarehouseItemID=numKitChildWarehouseItemID,
										@numKitChildQty=numKitChildQty,
										@numKitChildQtyShipped=numKitChildQtyShipped
									FROM
										@TempKitChildKitSubItems
									WHERE 
										ID = @k

									IF @numKitChildQty > @numKitChildQtyShipped
									BEGIN
										RAISERROR ('INVALID_KIT_SUB_ITEM_SHIPPED_QTY',16,1);
									END

									IF @tintCommitAllocation = 1 OR (@tintCommitAllocation = 2 AND @bitAllocateInventoryOnPickList=1)--When order is created OR Allocation on pick list add
									BEGIN						
										--PLACE QTY BACK ON ALLOCATION
										UPDATE  
											WareHouseItems
										SET     
											numAllocation = ISNULL(numAllocation,0) + @numKitChildQty,
											dtModified = GETDATE() 
										WHERE   
											numWareHouseItemID = @numKitChildWarehouseItemID
									END
									ELSE -- When added fulfillment order
									BEGIN
										--PLACE QTY BACK ON ONHAND
										UPDATE  
											WareHouseItems
										SET     
											numOnHand = ISNULL(numOnHand,0) + @numKitChildQty,
											dtModified = GETDATE() 
										WHERE   
											numWareHouseItemID = @numKitChildWarehouseItemID
									END

									--DECREASE QTY SHIPPED OF ORDER ITEM	
									UPDATE  
										OpportunityKitChildItems
									SET 
										numQtyShipped = ISNULL(numQtyShipped,0) - ISNULL(@numKitChildQty,0)
									WHERE   
										numOppKitChildItemID = @numOppKitChildItemID
										AND numOppChildItemID = @numOppChildItemID 
										AND numOppId=@numOppId
										AND numOppItemID=@numOppItemID

									SET @vcKitChildDescription = CONCAT('SO CHILD KIT Qty Shipped Return (Qty:',CAST(@numKitChildQty AS NUMERIC),' Shipped:',CAST(@numKitChildQtyShipped AS NUMERIC),')')

									-- CREATE WAREHOUSE INVENTORY TRACKING ENTRY
									EXEC dbo.USP_ManageWareHouseItems_Tracking
										@numWareHouseItemID = @numKitChildWarehouseItemID,
										@numReferenceID = @numOppId,
										@tintRefType = 3,
										@vcDescription = @vcKitChildDescription,
										@numModifiedBy = @numUserCntID,
										@numDomainID = @numDomainID	 
									
									SET @k = @k + 1
								END
							END

							--DECREASE QTY SHIPPED OF ORDER ITEM	
							UPDATE  
								OpportunityKitItems
							SET 
								numQtyShipped = ISNULL(numQtyShipped,0) - ISNULL(@numChildQty,0)
							WHERE   
								numOppChildItemID = @numOppChildItemID 
								AND numOppId=@numOppId
								AND numOppItemID=@numOppItemID

							SET @vcChildDescription = CONCAT('SO KIT Qty Shipped Return (Qty:',CAST(@numChildQty AS NUMERIC),' Shipped:',CAST(@numChildQtyShipped AS NUMERIC),')')

							-- CREATE WAREHOUSE INVENTORY TRACKING ENTRY
							EXEC dbo.USP_ManageWareHouseItems_Tracking
								@numWareHouseItemID = @numChildWarehouseItemID,
								@numReferenceID = @numOppId,
								@tintRefType = 3,
								@vcDescription = @vcChildDescription,
								@numModifiedBy = @numUserCntID,
								@numDomainID = @numDomainID	 
						END
						ELSE
						BEGIN
							IF @tintCommitAllocation = 1 OR (@tintCommitAllocation = 2 AND @bitAllocateInventoryOnPickList=1)--When order is created OR Allocation on pick list add
							BEGIN
								--PLACE QTY BACK ON ALLOCATION
								UPDATE  
									WareHouseItems
								SET     
									numAllocation = ISNULL(numAllocation,0) + @numChildQty,
									dtModified = GETDATE() 
								WHERE   
									numWareHouseItemID = @numChildWarehouseItemID
							END
							ELSE  -- When added fulfillment order
							BEGIN
								--PLACE QTY BACK ON ONHAND
								UPDATE  
									WareHouseItems
								SET     
									numOnHand = ISNULL(numOnHand,0) + @numChildQty,
									dtModified = GETDATE() 
								WHERE   
									numWareHouseItemID = @numChildWarehouseItemID
							END
							      	
						
							--DECREASE QTY SHIPPED OF ORDER ITEM	
							UPDATE  
								OpportunityKitItems
							SET 
								numQtyShipped = ISNULL(numQtyShipped,0) - ISNULL(@numChildQty,0)
							WHERE   
								numOppChildItemID = @numOppChildItemID 
								AND numOppId=@numOppId
								AND numOppItemID=@numOppItemID

							SET @vcChildDescription = CONCAT('SO KIT Qty Shipped Return (Qty:',CAST(@numChildQty AS NUMERIC),' Shipped:',CAST(@numChildQtyShipped AS NUMERIC),')')

							-- CREATE WAREHOUSE INVENTORY TRACKING ENTRY
							EXEC dbo.USP_ManageWareHouseItems_Tracking
								@numWareHouseItemID = @numChildWarehouseItemID,
								@numReferenceID = @numOppId,
								@tintRefType = 3,
								@vcDescription = @vcChildDescription,
								@numModifiedBy = @numUserCntID,
								@numDomainID = @numDomainID	
						END

						SET @j = @j + 1
					END
				END
				ELSE
				BEGIN
					IF @bitSerial = 1 OR @bitLot = 1
					BEGIN 
						IF @tintCommitAllocation = 1 OR (@tintCommitAllocation = 2 AND @bitAllocateInventoryOnPickList=1)--When order is created OR Allocation on pick list add
						BEGIN
							UPDATE
								WareHouseItems
							SET
								numAllocation = ISNULL(numAllocation,0) + @numQty
							WHERE
								numWareHouseItemID=@numWarehouseItemID
						END
						ELSE -- When added fulfillment order
						BEGIN
							-- ADD QUANTITY BACK ON ONHAND
							UPDATE  
								WareHouseItems
							SET     
								numOnHand = ISNULL(numOnHand,0) + @numQty
								,dtModified = GETDATE() 
							WHERE   
								numWareHouseItemID = @numWareHouseItemID
						END

						-- CREATE WAREHOUSE INVENTORY TRACKING ENTRY
						EXEC dbo.USP_ManageWareHouseItems_Tracking
									@numWareHouseItemID = @numWareHouseItemID,
									@numReferenceID = @numOppId,
									@tintRefType = 3,
									@vcDescription = @vcDescription,
									@numModifiedBy = @numUserCntID,
									@numDomainID = @numDomainID	 


						DELETE FROM @TEMPSERIALLOT

						INSERT INTO @TEMPSERIALLOT
						(
							ID
							,numWarehouseItemID
							,vcSerialLot
							,numQty
						)
						SELECT 
							ROW_NUMBER() OVER(ORDER BY OWSI.numWarehouseItmsDTLID)
							,OWSI.numWarehouseItmsID
							,ISNULL(WID.vcSerialNo,'')
							,ISNULL(OWSI.numQty,0)
						FROM
							OppWarehouseSerializedItem OWSI
						INNER JOIN
							WareHouseItmsDTL WID
						ON
							OWSI.numWarehouseItmsDTLID = WID.numWareHouseItmsDTLID
						INNER JOIN	
							WarehouseItems WI
						ON
							OWSI.numWarehouseItmsID = WI.numWareHouseItemID
						WHERE 
							numOppID=@numOppId 
							AND numOppItemID=@numOppItemID 
							AND ISNULL(numOppBizDocsId,0) = @numOppBizDocID

						DECLARE @l INT = 1
						DECLARE @lCount INT
						DECLARE @numTempWarehouseItemID NUMERIC(18,0)
						DECLARE @vcSerialLot VARCHAR(100)
						DECLARE @vcTempDescription VARCHAR(500)
						DECLARE @numTempQty FLOAT
						SELECT @lCount = COUNT(*) FROM @TEMPSERIALLOT

						WHILE @l <= @lCount
						BEGIN
							SELECT
								@numTempWarehouseItemID=numWarehouseItemID
								,@vcSerialLot=vcSerialLot
								,@numTempQty=numQty 
							FROM 
								@TEMPSERIALLOT 
							WHERE 
								ID=@l

							IF @numWarehouseItemID <> @numTempWarehouseItemID
							BEGIN
								UPDATE 
									WareHouseItems
								SET
									numOnHand = ISNULL(numOnHand,0) + @numTempQty
								WHERE
									numWareHouseItemID=@numTempWarehouseItemID

								SET @vcTempDescription = CONCAT('Serial/Lot(',@vcSerialLot,') Shipped Return Qty(',@numTempQty,')') 

								EXEC dbo.USP_ManageWareHouseItems_Tracking
								@numWareHouseItemID = @numTempWarehouseItemID, --  numeric(9, 0)
								@numReferenceID = @numOppId, --  numeric(9, 0)
								@tintRefType = 3, --  tinyint
								@vcDescription = @vcTempDescription, --  varchar(100)
								@numModifiedBy = @numUserCntID,
								@numDomainID = @numDomainID
								
								UPDATE
									WareHouseItems
								SET
									numOnHand = (CASE WHEN ISNULL(numOnHand,0) >= @numTempQty THEN (ISNULL(numOnHand,0) - @numTempQty) ELSE 0 END)
									,numAllocation = ISNULL(numAllocation,0) - (CASE WHEN ISNULL(numOnHand,0) >= @numTempQty THEN 0 ELSE (CASE WHEN ISNULL(numAllocation,0) > ISNULL(numOnHand,0) THEN ISNULL(numOnHand,0) ELSE 0 END) END)
									,numBackOrder = ISNULL(numBackOrder,0) + (CASE WHEN ISNULL(numOnHand,0) >= @numTempQty THEN 0 ELSE (CASE WHEN ISNULL(numAllocation,0) > ISNULL(numOnHand,0) THEN ISNULL(numOnHand,0) ELSE 0 END) END)
								WHERE
									numWareHouseItemID=@numWarehouseItemID

								SET @vcTempDescription = CONCAT('Serial/Lot(',@vcSerialLot,') Shipped Return Qty(',@numTempQty,')') 

								EXEC dbo.USP_ManageWareHouseItems_Tracking
								@numWareHouseItemID = @numWarehouseItemID, --  numeric(9, 0)
								@numReferenceID = @numOppId, --  numeric(9, 0)
								@tintRefType = 3, --  tinyint
								@vcDescription = @vcTempDescription, --  varchar(100)
								@numModifiedBy = @numUserCntID,
								@numDomainID = @numDomainID	 
							END

							SET @l = @l + 1
						END

						UPDATE OppWarehouseSerializedItem SET numOppBizDocsId=NULL WHERE numOppID=@numOppID AND numOppItemID= @numOppItemID AND numOppBizDocsId=@numOppBizDocID
					END
					ELSE
					BEGIN
						IF @tintCommitAllocation = 1 OR (@tintCommitAllocation = 2 AND @bitAllocateInventoryOnPickList=1)--When order is created OR Allocation on pick list add
						BEGIN
							--PLACE QTY BACK ON ALLOCATION
							UPDATE  
								WareHouseItems
							SET     
								numAllocation = ISNULL(numAllocation,0) + @numQty
								,dtModified = GETDATE() 
							WHERE   
								numWareHouseItemID = @numWareHouseItemID
						END
						ELSE -- When added fulfillment order
						BEGIN
							-- ADD QUANTITY BACK ON ONHAND
							UPDATE  
								WareHouseItems
							SET     
								numOnHand = ISNULL(numOnHand,0) + @numQty
								,dtModified = GETDATE() 
							WHERE   
								numWareHouseItemID = @numWareHouseItemID
						END
					END
				END

				IF NOT (@bitSerial = 1 OR @bitLot = 1)
				BEGIN
					-- CREATE WAREHOUSE INVENTORY TRACKING ENTRY
					EXEC dbo.USP_ManageWareHouseItems_Tracking
								@numWareHouseItemID = @numWareHouseItemID,
								@numReferenceID = @numOppId,
								@tintRefType = 3,
								@vcDescription = @vcDescription,
								@numModifiedBy = @numUserCntID,
								@numDomainID = @numDomainID	 
				END
				
			END

			--DECREASE QTY SHIPPED OF ORDER ITEM	
			UPDATE 
				OpportunityItems
			SET     
				numQtyShipped = (ISNULL(numQtyShipped,0) - @numQty)
			WHERE   
				numoppitemtCode = @numOppItemID 
				AND numOppId=@numOppId

			SET @i = @i + 1
		END
	COMMIT TRANSACTION 
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRAN

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO

IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityMaster_PutAway')
DROP PROCEDURE USP_OpportunityMaster_PutAway
GO
CREATE PROCEDURE [dbo].[USP_OpportunityMaster_PutAway]
(
	@numDomainID NUMERIC(18,0)
	,@numUserCntID NUMERIC(18,0)
	,@numOppID NUMERIC(18,0)
	,@numOppItemID NUMERIC(18,0)
	,@numQtyReceived FLOAT
	,@dtItemReceivedDate DATETIME
	,@vcWarehouses VARCHAR(MAX)
	,@numVendorInvoiceBizDocID NUMERIC(18,0)
	,@tintMode TINYINT
)
AS
BEGIN
	IF ISNULL(@numQtyReceived,0) <= 0 
	BEGIN
		RAISERROR('QTY_TO_RECEIVE_NOT_PROVIDED',16,1)
		RETURN
	END 

	IF LEN(ISNULL(@vcWarehouses,'')) = 0
	BEGIN
		RAISERROR('LOCATION_NOT_SELECTED',16,1)
		RETURN
	END

	DECLARE @TEMPWarehouseLot TABLE
	(
		ID INT
		,numWareHouseItmsDTLID NUMERIC(18,0)
		,vcSerialNo VARCHAR(300)
		,numQty FLOAT
	)

	DECLARE @TEMPSerialLot TABLE
	(
		ID INT
		,vcSerialNo VARCHAR(300)
		,numQty FLOAT
	)

	DECLARE @TEMPWarehouseLocations TABLE
	(
		ID INT IDENTITY(1,1)
		,numWarehouseLocationID NUMERIC(18,0)
		,numWarehouseItemID NUMERIC(18,0)
		,numReceivedQty FLOAT
		,vcSerialLotNo VARCHAR(MAX)
	)

	DECLARE @hDocItem int
	EXEC sp_xml_preparedocument @hDocItem OUTPUT, @vcWarehouses

	INSERT INTO @TEMPWarehouseLocations
	(
		numWarehouseLocationID
		,numWarehouseItemID
		,numReceivedQty
		,vcSerialLotNo
	)
	SELECT 
		(CASE WHEN ISNULL(bitNewLocation,0) = 1 THEN ID ELSE 0 END)
		,(CASE WHEN ISNULL(bitNewLocation,0) = 1 THEN 0 ELSE ID END)
		,ISNULL(Qty,0)
		,ISNULL(SerialLotNo,'') 
	FROM 
		OPENXML (@hDocItem, '/NewDataSet/Table1',2)
	WITH 
	(
		bitNewLocation BIT, 
		ID NUMERIC(18,0),
		Qty FLOAT,
		SerialLotNo VARCHAR(MAX)
	)

	EXEC sp_xml_removedocument @hDocItem

	IF @numQtyReceived <> ISNULL((SELECT SUM(numReceivedQty) FROM @TEMPWarehouseLocations),0)
	BEGIN
		RAISERROR('INALID_QTY_RECEIVED',16,1)
		RETURN
	END

	IF ISNULL(@numVendorInvoiceBizDocID,0) > 0
	BEGIN
		-- IF RECEIVED AGAINST VENDOR INVOICE MAKE SURE ENOUGH QTY IS PENDING TO RECIEVED AGAINST VEDOR INVOICE BECAUSE USER CAN RECEIVE QTY WITHOUT SELECTING VENDOR INVOCIE ALSO
		IF (SELECT
				COUNT(*)
			FROM
				OpportunityBizDocItems OBDI
			INNER JOIN
				OpportunityItems OI
			ON
				OBDI.numOppItemID=OI.numoppitemtCode
			WHERE
				numOppBizDocID=@numVendorInvoiceBizDocID
				AND OBDI.numOppItemID = @numOppItemID
				AND @numQtyReceived > (ISNULL(OI.numUnitHour,0) - ISNULL(OI.numUnitHourReceived,0))
			) > 0
		BEGIN
			RAISERROR('INVALID_VENDOR_INVOICE_RECEIVE_QTY',16,1)
			RETURN
		END
	END

	DECLARE @description AS VARCHAR(100)
	DECLARE @numWarehouseItemID AS NUMERIC(18,0)
	DECLARE @numFromWarehouseID AS NUMERIC(18,0)
	DECLARE @numFromWarehouseItemID NUMERIC(18,0)
	DECLARE @numWLocationID AS NUMERIC(18,0)     
	DECLARE @numOldQtyReceived AS FLOAT
	DECLARE @bitStockTransfer BIT
	DECLARE @numToWarehouseItemID NUMERIC
	DECLARE @numUnits FLOAT
	DECLARE @monPrice AS DECIMAL(20,5) 
	DECLARE @numItemCode NUMERIC(18,0)
	DECLARE @monListPrice DECIMAL(20,5)
	DECLARE @numWarehouseID NUMERIC(18,0)
	DECLARE @bitSerial BIT
	DECLARE @bitLot BIT
		
	SELECT  
		@numWarehouseItemID = ISNULL([numWarehouseItmsID], 0),
		@numFromWarehouseID=ISNULL(WI.numWareHouseID,0),
		@numFromWarehouseItemID = ISNULL([numWarehouseItmsID], 0),
		@numWLocationID = ISNULL(WI.numWLocationID,0),
		@numOldQtyReceived = ISNULL([numUnitHourReceived], 0),
		@bitStockTransfer = ISNULL(OM.bitStockTransfer,0),
		@numToWarehouseItemID= ISNULL(numToWarehouseItemID,0),
		@numOppId=OM.numOppId,
		@numUnits=OI.numUnitHour,
		@monPrice=isnull(monPrice,0) * (CASE WHEN ISNULL(OM.fltExchangeRate,0)=0 THEN 1 ELSE OM.fltExchangeRate END),
		@numItemCode=OI.numItemCode,
		@monListPrice=ISNULL(I.monListPrice,0),
		@numWarehouseID=WI.numWareHouseID,
		@bitSerial=ISNULL(I.bitSerialized,0),
		@bitLot=ISNULL(I.bitLotNo,0)
	FROM 
		[OpportunityItems] OI 
	INNER JOIN 
		dbo.OpportunityMaster OM
	ON 
		OI.numOppId = OM.numOppId
	INNER JOIN
		Item I
	ON
		OI.numItemCode = I.numItemCode
	LEFT JOIN
		WareHouseItems WI
	ON
		OI.numWarehouseItmsID = WI.numWareHouseItemID
	WHERE 
		[numoppitemtCode] = @numOppItemID

	IF (@numQtyReceived + @numOldQtyReceived) > @numUnits
	BEGIN
		RAISERROR('RECEIVED_QTY_MUST_BE_LESS_THEN_OR_EQUAL_TO_ORDERED_QTY',16,1)
		RETURN
	END

	IF @bitStockTransfer = 1 --added by chintan
	BEGIN
		DECLARE @p3 VARCHAR(MAX) = ''
		
		EXEC USP_UpdateQtyShipped @numQtyReceived,@numOppItemID,@numUserCntID,@vcError=@p3 OUTPUT

		IF LEN(@p3)>0 
		BEGIN
			RAISERROR ( @p3,16, 1 )
			RETURN ;
		END    

		SET @numWarehouseItemID = @numToWarehouseItemID
	END
	ELSE
	BEGIN
		--Updating the Average Cost
		DECLARE @TotalOnHand AS FLOAT = 0
		DECLARE @monAvgCost AS DECIMAL(20,5) 

		SELECT 
			@TotalOnHand=(SUM(ISNULL(numOnHand,0)) + SUM(ISNULL(numAllocation,0))) 
		FROM 
			dbo.WareHouseItems 
		WHERE 
			numItemID=@numItemCode 
						
		SELECT 
			@monAvgCost = (CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(monAverageCost, 0) END) 
		FROM 
			Item 
		WHERE 
			numitemcode = @numItemCode  
    
  		SET @monAvgCost = ((@TotalOnHand * @monAvgCost) + (@numQtyReceived * @monPrice)) / ( @TotalOnHand + @numQtyReceived )
                            
		UPDATE  
			Item
		SET 
			monAverageCost = (CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE @monAvgCost END)
		WHERE 
			numItemCode = @numItemCode
    END	

	UPDATE
		WareHouseItems
	SET 
		numOnOrder = ISNULL(numOnOrder,0) - @numQtyReceived,
		dtModified = GETDATE()
	WHERE
		numWareHouseItemID=@numWareHouseItemID
		
	SET @description = CONCAT('PO Qty Received (Qty:',@numQtyReceived,')')

	EXEC dbo.USP_ManageWareHouseItems_Tracking
		@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
		@numReferenceID = @numOppId, --  numeric(9, 0)
		@tintRefType = 4, --  tinyint
		@vcDescription = @description,
		@numModifiedBy = @numUserCntID,
		@dtRecordDate =  @dtItemReceivedDate,
		@numDomainID = @numDomainID


	DECLARE @i INT
	DECLARE @j INT
	DECLARE @k INT
	DECLARE @iCount INT = 0
	DECLARE @jCount INT = 0
	DECLARE @kCount INT = 0
	DECLARE @vcSerialLot# VARCHAR(MAX)
	DECLARE @numWareHouseItmsDTLID NUMERIC(18,0)
	DECLARE @vcSerailLotNumber VARCHAR(300)
	DECLARE @numSerialLotQty FLOAT
	DECLARE @numTempWLocationID NUMERIC(18,0)
	DECLARE @numTempWarehouseItemID NUMERIC(18,0)
	DECLARE @numTempReceivedQty FLOAT
	DECLARE @numLotWareHouseItmsDTLID NUMERIC(18,0)
	DECLARE @numLotQty FLOAT
	
	SET @i = 1
	SET @iCount = (SELECT COUNT(*) FROM @TEMPWarehouseLocations)

	WHILE @i <= @iCount
	BEGIN
		SELECT 
			@numTempWLocationID=ISNULL(numWarehouseLocationID,0)
			,@numTempWarehouseItemID=ISNULL(numWarehouseItemID,0)
			,@numTempReceivedQty=ISNULL(numReceivedQty,0)
			,@vcSerialLot# = ISNULL(vcSerialLotNo,'')
		FROM 
			@TEMPWarehouseLocations 
		WHERE 
			ID = @i

		IF (ISNULL(@bitLot,0)=1 OR ISNULL(@bitSerial,0)=1)
		BEGIN
			IF LEN(ISNULL(@vcSerialLot#,'')) = 0
			BEGIN
				RAISERROR('SERIALLOT_REQUIRED',16,1)
				RETURN
			END 
			ELSE
			BEGIN	
				DELETE FROM @TEMPSerialLot

				IF ISNULL(@bitSerial,0) = 1
				BEGIN
					INSERT INTO @TEMPSerialLot
					(
						ID
						,vcSerialNo
						,numQty
					)
					SELECT
						ROW_NUMBER() OVER(ORDER BY OutParam)
						,RTRIM(LTRIM(OutParam))
						,1
					FROM
						dbo.SplitString(@vcSerialLot#,',')
				END
				ELSE IF @bitLot = 1
				BEGIN
					INSERT INTO @TEMPSerialLot
					(
						ID
						,vcSerialNo
						,numQty
					)
					SELECT
						ROW_NUMBER() OVER(ORDER BY OutParam)
						,(CASE 
							WHEN PATINDEX('%(%', OutParam) > 0 AND PATINDEX('%)%', OutParam) > 0 AND PATINDEX('%)%',OutParam) > (PATINDEX('%(%',OutParam) + 1)
							THEN RTRIM(LTRIM(SUBSTRING(OutParam,0,PATINDEX('%(%', OutParam))))
							ELSE ''
						END)
						,(CASE 
							WHEN PATINDEX('%(%', OutParam) > 0 AND PATINDEX('%)%', OutParam) > 0 AND PATINDEX('%)%',OutParam) > (PATINDEX('%(%',OutParam) + 1)
							THEN (CASE WHEN ISNUMERIC(SUBSTRING(OutParam,(PATINDEX('%(%', OutParam) + 1),(PATINDEX('%)%', OutParam) - PATINDEX('%(%', OutParam) - 1))) = 1 
									THEN CAST(SUBSTRING(OutParam,(PATINDEX('%(%', OutParam) + 1),(PATINDEX('%)%', OutParam) - PATINDEX('%(%', OutParam) - 1)) AS FLOAT)  
									ELSE -1 
									END) 
							ELSE -1
						END)	
	
					FROM
						dbo.SplitString(@vcSerialLot#,',')
				END

				IF EXISTS (SELECT vcSerialNo FROM @TEMPSerialLot WHERE numQty = -1)
				BEGIN
					RAISERROR('INVALID_SERIAL_NO',16,1)
					RETURN
				END
				ELSE IF @numTempReceivedQty <> ISNULL((SELECT SUM(numQty) FROM @TEMPSerialLot),0)
				BEGIN
					RAISERROR('INALID_SERIALLOT',16,1)
					RETURN
				END
			END
		END

		IF ISNULL(@numTempWarehouseItemID,0) = 0
		BEGIN
			IF EXISTS (SELECT numWLocationID FROM WarehouseLocation WHERE numDomainID=@numDomainID AND numWLocationID=ISNULL(@numTempWLocationID,0))
			BEGIN
				IF EXISTS (SELECT numWareHouseItemID FROM WareHouseItems WHERE numDomainID=@numDomainID AND numItemID=@numItemCode AND numWareHouseID=@numWarehouseID AND numWLocationID=@numTempWLocationID)
				BEGIN
					SET @numTempWarehouseItemID = (SELECT TOP 1 numWareHouseItemID FROM WareHouseItems WHERE numDomainID=@numDomainID AND numItemID=@numItemCode AND numWareHouseID=@numWarehouseID AND numWLocationID=@numTempWLocationID)
				END
				ELSE 
				BEGIN
					INSERT INTO WareHouseItems 
					(
						numItemID, 
						numWareHouseID,
						numWLocationID,
						numOnHand,
						numAllocation,
						numOnOrder,
						numBackOrder,
						numReorder,
						monWListPrice,
						vcLocation,
						vcWHSKU,
						vcBarCode,
						numDomainID,
						dtModified
					)  
					VALUES
					(
						@numItemCode,
						@numWareHouseID,
						@numTempWLocationID,
						0,
						0,
						0,
						0,
						0,
						@monListPrice,
						ISNULL((SELECT vcLocation FROM WarehouseLocation WHERE numDomainID=@numDomainID AND numWLocationID=ISNULL(@numTempWLocationID,0)),''),
						'',
						'',
						@numDomainID,
						GETDATE()
					)  

					SELECT @numTempWarehouseItemID = SCOPE_IDENTITY()

					DECLARE @dtDATE AS DATETIME = GETUTCDATE()
					DECLARE @vcDescription VARCHAR(100)='INSERT WareHouse'

					EXEC dbo.USP_ManageWareHouseItems_Tracking
						@numWareHouseItemID = @numTempWarehouseItemID,
						@numReferenceID = @numItemCode,
						@tintRefType = 1,
						@vcDescription = @vcDescription,
						@numModifiedBy = @numUserCntID,
						@ClientTimeZoneOffset = 0,
						@dtRecordDate = @dtDATE,
						@numDomainID = @numDomainID 
				END
			END
			ELSE 
			BEGIN
				RAISERROR('INVALID_WAREHOUSE_LOCATION',16,1)
				RETURN
			END
		END

		IF NOT EXISTS (SELECT numWareHouseItemID FROM WareHouseItems WHERE numDomainID=@numDomainID AND numItemID=@numItemCode AND numWareHouseItemID=@numTempWarehouseItemID)
		BEGIN
			RAISERROR('INVALID_WAREHOUSE',16,1)
			RETURN
		END
		ELSE
		BEGIN
			-- INCREASE THE OnHand Of Destination Location
			UPDATE
				WareHouseItems
			SET
				numBackOrder = (CASE WHEN numBackOrder > @numTempReceivedQty THEN ISNULL(numBackOrder,0) - @numTempReceivedQty ELSE 0 END),         
				numAllocation = (CASE WHEN numBackOrder > @numTempReceivedQty THEN ISNULL(numAllocation,0) + @numTempReceivedQty ELSE ISNULL(numAllocation,0) + ISNULL(numBackOrder,0) END),
				numOnHand = (CASE WHEN numBackOrder > @numTempReceivedQty THEN numOnHand ELSE ISNULL(numOnHand,0) + (@numTempReceivedQty - ISNULL(numBackOrder,0)) END),
				dtModified = GETDATE()
			WHERE
				numWareHouseItemID=@numTempWarehouseItemID

			SET @description = CONCAT('PO Qty Put-Away (Qty:',@numTempReceivedQty,')')

			EXEC dbo.USP_ManageWareHouseItems_Tracking
				@numWareHouseItemID = @numTempWarehouseItemID, --  numeric(9, 0)
				@numReferenceID = @numOppId, --  numeric(9, 0)
				@tintRefType = 4, --  tinyint
				@vcDescription = @description,
				@numModifiedBy = @numUserCntID,
				@dtRecordDate =  @dtItemReceivedDate,
				@numDomainID = @numDomainID

			IF ISNULL(@numTempWarehouseItemID,0) > 0 AND ISNULL(@numTempWarehouseItemID,0) <> @numWareHouseItemID		
			BEGIN
				INSERT INTO OpportunityItemsReceievedLocation
				(
					numDomainID,
					numOppID,
					numOppItemID,
					numWarehouseItemID,
					numUnitReceieved
				)
				VALUES
				(
					@numWarehouseItemID,
					@numOppId,
					@numOppItemID,
					@numTempWarehouseItemID,
					@numTempReceivedQty
				)
			END

			IF (ISNULL(@bitLot,0)=1 OR ISNULL(@bitSerial,0)=1)
			BEGIN
				SET @j = 1
				SET @jCount = (SELECT COUNT(*) FROM @TEMPSerialLot)

				WHILE (@j <= @jCount AND @numTempReceivedQty > 0)
				BEGIN
					SELECT 
						@vcSerailLotNumber=ISNULL(vcSerialNo,'')
						,@numSerialLotQty=ISNULL(numQty,0)
					FROM 
						@TEMPSerialLot 
					WHERE 
						ID=@j

					IF @numSerialLotQty > 0
					BEGIN
						IF @bitStockTransfer = 1
						BEGIN
							IF (SELECT COUNT(*) FROM WareHouseItems INNER JOIN WareHouseItmsDTL ON WareHouseItems.numWareHouseItemID = WareHouseItmsDTL.numWareHouseItemID WHERE WareHouseItems.numItemID=@numItemCode AND WareHouseItems.numWareHouseID=@numFromWarehouseID AND LOWER(vcSerialNo)=LOWER(@vcSerailLotNumber)) = 0
							BEGIN
								RAISERROR('INVALID_SERIAL_NO',16,1)
								RETURN
							END
					
							IF @bitLot = 1
							BEGIN
								IF (SELECT SUM(WareHouseItmsDTL.numQty) FROM WareHouseItems INNER JOIN WareHouseItmsDTL ON WareHouseItems.numWareHouseItemID = WareHouseItmsDTL.numWareHouseItemID WHERE WareHouseItems.numItemID=@numItemCode AND WareHouseItems.numWareHouseID=@numFromWarehouseID AND LOWER(vcSerialNo)=LOWER(@vcSerailLotNumber)) < @numTempReceivedQty
								BEGIN
									RAISERROR('INVALID_LOT_QUANTITY',16,1)
									RETURN
								END

								DELETE FROM @TEMPWarehouseLot

								INSERT INTO @TEMPWarehouseLot
								(
									ID
									,numWareHouseItmsDTLID
									,numQty
								)
								SELECT
									ROW_NUMBER() OVER(ORDER BY WareHouseItmsDTL.numQty)
									,WareHouseItmsDTL.numWareHouseItmsDTLID
									,ISNULL(WareHouseItmsDTL.numQty,0)
								FROM
									WareHouseItems 
								INNER JOIN 
									WareHouseItmsDTL 
								ON 
									WareHouseItems.numWareHouseItemID = WareHouseItmsDTL.numWareHouseItemID 
								WHERE 
									WareHouseItems.numItemID=@numItemCode 
									AND WareHouseItems.numWareHouseID=@numFromWarehouseID 
									AND LOWER(vcSerialNo)=LOWER(@vcSerailLotNumber)

								SET @k = 1
								SELECT @kCount=COUNT(*) FROM @TEMPWarehouseLot

								WHILE @k <= @kCount AND @numTempReceivedQty > 0
								BEGIN
									SELECT
										@numLotWareHouseItmsDTLID=numWareHouseItmsDTLID
										,@numLotQty = numQty
									FROM
										@TEMPWarehouseLot
									WHERE
										ID=@k

									IF @numTempReceivedQty > @numLotQty
									BEGIN
										UPDATE
											WareHouseItmsDTL
										SET
											numWareHouseItemID = @numTempWarehouseItemID
										WHERE 
											numWareHouseItmsDTLID=@numLotWareHouseItmsDTLID

										UPDATE @TEMPSerialLot SET numQty = numQty-@numLotQty WHERE ID=@j
										SET @numTempReceivedQty = @numTempReceivedQty - @numLotQty
									END
									ELSE
									BEGIN
										UPDATE 
											WareHouseItmsDTL 
										SET 
											numQty = numQty - @numTempReceivedQty
										WHERE 
											numWareHouseItmsDTLID=@numLotWareHouseItmsDTLID

										INSERT INTO WareHouseItmsDTL
										(
											numWareHouseItemID,
											vcSerialNo,
											numQty,
											dExpirationDate,
											bitAddedFromPO
										)
										SELECT
											@numTempWarehouseItemID,
											vcSerialNo,
											@numTempReceivedQty,
											dExpirationDate,
											bitAddedFromPO
										FROM 
											WareHouseItmsDTL
										WHERE
											numWareHouseItmsDTLID=@numLotWareHouseItmsDTLID

										UPDATE @TEMPSerialLot SET numQty = numQty-@numTempReceivedQty WHERE ID=@j
										SET @numTempReceivedQty = 0
									END
									

									SET @k = @k + 1
								END
							END
							ELSE
							BEGIN
								IF NOT EXISTS (SELECT WareHouseItmsDTL.numWareHouseItmsDTLID FROM WareHouseItems INNER JOIN WareHouseItmsDTL ON WareHouseItems.numWareHouseItemID = WareHouseItmsDTL.numWareHouseItemID WHERE WareHouseItems.numItemID=@numItemCode AND WareHouseItems.numWareHouseID=@numFromWarehouseID AND LOWER(vcSerialNo)=LOWER(@vcSerailLotNumber) AND ISNULL(WareHouseItmsDTL.numQty,0) = 1)
								BEGIN
									RAISERROR('INVALID_SERIAL_QUANTITY',16,1)
									RETURN
								END

								UPDATE
									WHID
								SET
									WHID.numWareHouseItemID = @numTempWarehouseItemID
								FROM
									WareHouseItems WI
								INNER JOIN
									WareHouseItmsDTL WHID
								ON 
									WI.numWareHouseItemID = WHID.numWareHouseItemID 
								WHERE 
									WI.numItemID=@numItemCode 
									AND WI.numWareHouseID=@numFromWarehouseID 
									AND LOWER(WHID.vcSerialNo)=LOWER(@vcSerailLotNumber)

								UPDATE @TEMPSerialLot SET numQty = 0 WHERE ID=@j
								SET @numTempReceivedQty = @numTempReceivedQty - 1
							END
						END
						ELSE
						BEGIN
							IF @bitSerial = 1 AND (SELECT COUNT(*) FROM WareHouseItems INNER JOIN WareHouseItmsDTL ON WareHouseItems.numWareHouseItemID = WareHouseItmsDTL.numWareHouseItemID WHERE WareHouseItems.numItemID=@numItemCode AND LOWER(vcSerialNo)=LOWER(@vcSerailLotNumber)) > 0
							BEGIN
								RAISERROR('DUPLICATE_SERIAL_NO',16,1)
								RETURN
							END

							INSERT INTO WareHouseItmsDTL
							(
								numWareHouseItemID
								,vcSerialNo
								,numQty
								,bitAddedFromPO
							)  
							VALUES
							( 
								@numTempWarehouseItemID
								,@vcSerailLotNumber
								,(CASE WHEN @numTempReceivedQty >= @numSerialLotQty THEN @numSerialLotQty ELSE @numTempReceivedQty END)
								,1 
							)

		    
							SET @numWareHouseItmsDTLID=SCOPE_IDENTITY()
		
							INSERT INTO OppWarehouseSerializedItem
							(
								numWarehouseItmsDTLID
								,numOppId
								,numOppItemId
								,numWarehouseItmsID
								,numQty
							)  
							VALUES
							( 
								@numWareHouseItmsDTLID
								,@numOppID
								,@numOppItemID
								,@numTempWarehouseItemID
								,(CASE WHEN @numTempReceivedQty >= @numSerialLotQty THEN @numSerialLotQty ELSE @numTempReceivedQty END)
							)

							IF @numTempReceivedQty >= @numSerialLotQty
							BEGIN
								SET @numTempReceivedQty = @numTempReceivedQty - @numSerialLotQty
								UPDATE @TEMPSerialLot SET numQty = 0  WHERE ID = @j
							END
							ELSE
							BEGIN
								UPDATE @TEMPSerialLot SET numQty = numQty - @numTempReceivedQty  WHERE ID = @j
								SET @numTempReceivedQty = 0
							END
						END
					END

					SET @j = @j + 1
				END

				IF @numTempReceivedQty > 0
				BEGIN
					RAISERROR('INALID_SERIALLOT',16,1)
					RETURN
				END
			END
		END


		SET @i = @i + 1
	END

	UPDATE  
		[OpportunityItems]
	SET 
		numUnitHourReceived = ISNULL(numUnitHourReceived,0) + ISNULL(@numQtyReceived,0)
		,numQtyReceived = (CASE WHEN ISNULL(@tintMode,0)=1 THEN ISNULL(numQtyReceived,0) + ISNULL(@numQtyReceived,0) ELSE ISNULL(numQtyReceived,0) END)
	WHERE
		[numoppitemtCode] = @numOppItemID


	IF ISNULL(@numVendorInvoiceBizDocID,0) > 0
	BEGIN
		UPDATE  
			OpportunityBizDocItems
		SET 
			numVendorInvoiceUnitReceived = ISNULL(numVendorInvoiceUnitReceived,0) +  ISNULL(@numQtyReceived,0)
		WHERE
			numOppBizDocID=@numVendorInvoiceBizDocID
			AND numOppItemID = @numOppItemID
	END

	UPDATE dbo.OpportunityMaster SET dtItemReceivedDate=@dtItemReceivedDate WHERE numOppId=@numOppId

END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OppWarehouseSerializedItem_Insert')
DROP PROCEDURE USP_OppWarehouseSerializedItem_Insert
GO
CREATE PROCEDURE [dbo].[USP_OppWarehouseSerializedItem_Insert]
	@numOppID AS NUMERIC(18,0),
	@numOppItemID AS NUMERIC(18,0),
	@vcSelectedItems AS VARCHAR(MAX),
	@tintItemType AS TINYINT -- 1 = Serial, 2 = Lot
AS
BEGIN
SET NOCOUNT ON;
BEGIN TRY
BEGIN TRANSACTION	

	IF @tintItemType = 1 OR @tintItemType = 2
	BEGIN

		DECLARE @TEMPSERIALLOT TABLE
		(
			ID INT IDENTITY(1,1),
			numWareHouseItmsDTLID NUMERIC(18,0),
			numWarehouseItemID NUMERIC(18,0),
			numQty INT
		)

		UPDATE 
			WIDL
		SET
			WIDL.numQty = (CASE WHEN @tintItemType = 2 THEN (ISNULL(WIDL.numQty,0) + ISNULL(OWSI.numQty,0)) ELSE 1 END)
		FROM 
			WarehouseItmsDTL WIDL
		INNER JOIN
			OppWarehouseSerializedItem OWSI
		ON
			WIDL.numWareHouseItmsDTLID = OWSI.numWarehouseItmsDTLID
			AND WIDL.numWareHouseItemID = OWSI.numWarehouseItmsID
		WHERE 
			OWSI.numOppID=@numOppID 
			AND OWSI.numOppItemID=@numOppItemID			

		DELETE FROM OppWarehouseSerializedItem WHERE numOppID=@numOppID AND numOppItemID=@numOppItemID

		IF LEN(@vcSelectedItems) > 0
		BEGIN
			INSERT INTO @TEMPSERIALLOT
			(
				numWareHouseItmsDTLID
				,numQty
			)
			SELECT 
				PARSENAME(items,2),
				PARSENAME(items,1) 
			FROM 
				dbo.Split(Replace(@vcSelectedItems, '-', '.'),',')

			UPDATE 
				TSL
			SET
				TSL.numWarehouseItemID = WID.numWareHouseItemID
			FROM
				@TEMPSERIALLOT TSL
			INNER JOIN
				WareHouseItmsDTL WID
			ON
				TSL.numWareHouseItmsDTLID = WID.numWareHouseItmsDTLID


			DECLARE @i AS INT = 1
			DECLARE @COUNT AS INT = 0
			DECLARE @numWareHouseItmsDTLID NUMERIC(18,0)
			DECLARE @numWarehouseItemID AS NUMERIC(18,0)
			DECLARE @numQty INT

			SELECT @COUNT = COUNT(*) FROM @TEMPSERIALLOT

			WHILE @i <= @COUNT
			BEGIN
				SELECT @numWareHouseItmsDTLID=numWareHouseItmsDTLID,@numQty=numQty,@numWarehouseItemID=numWarehouseItemID FROM @TEMPSERIALLOT WHERE ID = @i

				UPDATE WareHouseItmsDTL SET numQty = (CASE WHEN @tintItemType = 2 THEN (ISNULL(numQty,0) - ISNULL(@numQty,0)) ELSE 0 END) WHERE numWareHouseItmsDTLID=@numWareHouseItmsDTLID

				INSERT INTO OppWarehouseSerializedItem
				(
					numWarehouseItmsDTLID,
					numOppID,
					numOppItemID,
					numWarehouseItmsID,
					numQty
				)
				SELECT 
					@numWareHouseItmsDTLID,
					@numOppID,
					@numOppItemID,
					@numWarehouseItemID,
					@numQty 

				SET @i = @i + 1
			END
		END
	END
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
END







SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ReportListMaster_EmployeeSalesPerformancePanel1')
DROP PROCEDURE USP_ReportListMaster_EmployeeSalesPerformancePanel1
GO

CREATE PROCEDURE [dbo].[USP_ReportListMaster_EmployeeSalesPerformancePanel1]
	@numDomainID NUMERIC(18,0)
	,@ClientTimeZoneOffset INT
	,@vcTimeLine VARCHAR(50)
	,@vcFilterBy TINYINT  -- 1:Assign TO, 2: Record Owner, 3: Teams (Based on Assigned To), 4: Employees
	,@vcFilterValue VARCHAR(MAX)
	--,@vcRunDisplay VARCHAR(MAX) -- added by hitesh 
AS
BEGIN 
	DECLARE @numShippingItemID NUMERIC(18,0)
	DECLARE @numDiscountItemID NUMERIC(18,0)
	DECLARE @ProfitCost INT
	SELECT @numShippingItemID=numShippingServiceItemID,@numDiscountItemID=numDiscountServiceItemID,@ProfitCost=numCost FROM Domain WHERE numDomainId=@numDomainID
	---alter sp
 DECLARE @dtStartDate DATETIME --= DATEADD(MINUTE,-@ClientTimeZoneOffset,DATEADD(MONTH,-12,GETUTCDATE()))  
 DECLARE @dtEndDate AS DATETIME = DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()) 
 -------
	DECLARE @TotalSalesReturn FLOAT = 0.0
	----alterd sp-----------

IF @vcTimeLine = 'Last12Months'  
 BEGIN
 set  @dtStartDate =   dateadd(month,datediff(month,0,@dtEndDate)-12,0)  
  --SET @dtStartDate = DATEADD(MONTH, DATEDIFF(MONTH, 0, @dtEndDate), 0)  
 END  
ELSE IF @vcTimeLine = 'Last6Months'  
 BEGIN
 set  @dtStartDate =   dateadd(month,datediff(month,0,@dtEndDate)-6,0)  
  --SET @dtStartDate = DATEADD(MONTH, DATEDIFF(MONTH, 0, @dtEndDate), 0)  
 END  
ELSE IF @vcTimeLine = 'Last3Months'  
BEGIN  
set  @dtStartDate =   dateadd(month,datediff(month,0,@dtEndDate)-3,0)
--SET @dtStartDate = DATEADD(MONTH, DATEDIFF(MONTH, 0, @dtEndDate), 0)  
END  
ELSE IF @vcTimeLine = 'Last30Days'  
BEGIN  
SET @dtStartDate = DATEADD(DAY, DATEDIFF(DAY, -30, @dtEndDate), 0)  
END  
ELSE IF @vcTimeLine = 'Last7Days'  
	BEGIN  
	SET @dtStartDate = DATEADD(DAY, DATEDIFF(DAY, -7, @dtEndDate), 0) 
	END
ELSE IF @vcTimeLine = 'Today' 
BEGIN 
SET @dtStartDate = DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE())
END
ELSE IF @vcTimeLine = 'Yesterday'
BEGIN 
SET @dtStartDate = DATEADD(DAY,-1,DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()))
 END 
ELSE  
BEGIN
 Set @dtStartDate = LEFT(@vcTimeLine,CHARINDEX('-',@vcTimeLine)-1)
 set @dtEndDate = RIGHT(@vcTimeLine,CHARINDEX('-',@vcTimeLine)-1)
 END
  
	-----------------------------------
	SELECT 
		UserMaster.numUserDetailId,CAst(@dtStartDate as date) as StartDate,CAST(@dtEndDate  as date) as EndDate,
		UserMaster.vcUserName,@vcFilterBy as vcFilterBy
		,(SELECT COUNT(*) FROM OpportunityMaster WHERE numDomainId=@numDomainID AND numAssignedTo=TEMP1.numAssignedTo AND tintOppType=1 AND tintOppStatus=1 AND bintOppToOrder IS NOT NULL) * 100.0 / (SELECT COUNT(*) FROM OpportunityMaster WHERE numDomainId=@numDomainID AND numAssignedTo=TEMP1.numAssignedTo AND tintOppType=1 AND ((tintOppStatus=1 AND bintOppToOrder IS NOT NULL) OR tintOppStatus=2)) * 1.0 WonPercent
	FROM
	(
		SELECT DISTINCT
			--numAssignedTo
			CASE @vcFilterBy  
		   WHEN 1  -- Assign To  
		   THEN numAssignedTo
		   --else numRecOwner
		   WHEN 2 
		   THEN numRecOwner
		   else  numAssignedTo
		   End as numAssignedTo
		FROM
			OpportunityMaster
		WHERE
			numDomainId=@numDomainID
			AND tintOppType=1
			AND ((tintOppStatus=1 AND bintOppToOrder IS NOT NULL) OR tintOppStatus=2)
		--	--alter sp
			AND (  
      (DATEADD(MINUTE,-@ClientTimeZoneOffset,bintCreatedDate) BETWEEN @dtStartDate AND @dtEndDate)   
      OR   
      (DATEADD(MINUTE,-@ClientTimeZoneOffset,bintCreatedDate) BETWEEN @dtStartDate AND @dtEndDate)  
     ) 
	 AND
	 1=(CASE   
     WHEN LEN(ISNULL(@vcFilterValue,0)) > 0 AND @vcFilterBy IN (1,2,3)  
     THEN   
      (CASE @vcFilterBy  
       WHEN 1  -- Assign To  
       THEN (CASE WHEN numAssignedTo IN (SELECT Id FROM dbo.SplitIDs(@vcFilterValue,',')) THEN 1 ELSE 0 END)  
       WHEN 2  -- Record Owner  
       THEN (CASE WHEN numRecOwner IN (SELECT Id FROM dbo.SplitIDs(@vcFilterValue,',')) THEN 1 ELSE 0 END)  
       WHEN 3  -- Teams (Based on Assigned To)  
       --THEN (CASE WHEN numAssignedTo IN (SELECT numUserCntID FROM UserTeams WHERE numDomainID=@numDomainID AND numTeam IN (SELECT Id FROM dbo.SplitIDs(@vcFilterValue,','))) THEN 1 ELSE 0 END)
	   THEN (CASE WHEN numAssignedTo IN (SELECT numContactId FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND numTeam IN (SELECT Id FROM dbo.SplitIDs(@vcFilterValue,','))) THEN 1 ELSE 0 END)  
      END)  
     ELSE 1   
    END)  
	 ------
	) TEMP1
	INNER JOIN
		UserMaster
	ON
		TEMP1.numAssignedTo = UserMaster.numUserDetailId
	WHERE
		UserMaster.numDomainID = @numDomainID
		

--#2
	SELECT
		UserMaster.numUserDetailId,
		@vcFilterBy as vcFilterBy,CAst(@dtStartDate as date) as StartDate,CAST(@dtEndDate  as date) as EndDate
		,UserMaster.vcUserName
		,Temp2.Profit
	FROM
	(
		SELECT DISTINCT
			CASE @vcFilterBy  
		   WHEN 1  -- Assign To  
		   THEN numAssignedTo
		   --else numRecOwner
		   WHEN 2 
		   THEN numRecOwner
		   else  numAssignedTo
		   End as numAssignedTo
		FROM
			OpportunityMaster
		WHERE
			numDomainId=@numDomainID
			AND tintOppType=1
			AND tintOppStatus = 1
				AND (  
      (DATEADD(MINUTE,-@ClientTimeZoneOffset,bintCreatedDate) BETWEEN @dtStartDate AND @dtEndDate)   
      OR   
      (DATEADD(MINUTE,-@ClientTimeZoneOffset,bintCreatedDate) BETWEEN @dtStartDate AND @dtEndDate)  
     ) 
			 AND
	 1=(CASE   
     WHEN LEN(ISNULL(@vcFilterValue,0)) > 0 AND @vcFilterBy IN (1,2,3)  
     THEN   
      (CASE @vcFilterBy  
       WHEN 1  -- Assign To  
       THEN (CASE WHEN numAssignedTo IN (SELECT Id FROM dbo.SplitIDs(@vcFilterValue,',')) THEN 1 ELSE 0 END)  
       WHEN 2  -- Record Owner  
       THEN (CASE WHEN numRecOwner IN (SELECT Id FROM dbo.SplitIDs(@vcFilterValue,',')) THEN 1 ELSE 0 END)  
       WHEN 3  -- Teams (Based on Assigned To)  
       --THEN (CASE WHEN numAssignedTo IN (SELECT numUserCntID FROM UserTeams WHERE numDomainID=@numDomainID AND numTeam IN (SELECT Id FROM dbo.SplitIDs(@vcFilterValue,','))) THEN 1 ELSE 0 END)  
	   THEN (CASE WHEN numAssignedTo IN (SELECT numContactId FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND numTeam IN (SELECT Id FROM dbo.SplitIDs(@vcFilterValue,','))) THEN 1 ELSE 0 END)  
      END)  
     ELSE 1   
    END)  
	) TEMP1
	INNER JOIN
		UserMaster
	ON
		TEMP1.numAssignedTo = UserMaster.numUserDetailId
		AND UserMaster.numDomainID = @numDomainID
	CROSS APPLY
	(
		SELECT
			SUM(ISNULL(monTotAmount,0) - (ISNULL(numUnitHour,0) * (CASE @ProfitCost WHEN 3 THEN ISNULL(V.monCost,0) * dbo.fn_UOMConversion(numBaseUnit,I.numItemCode,@numDomainID,numPurchaseUnit) WHEN 2 THEN numCost * dbo.fn_UOMConversion(numBaseUnit,I.numItemCode,@numDomainID,numUOMId) ELSE monAverageCost END))) Profit
		FROM
			OpportunityItems OI
		INNER JOIN
			OpportunityMaster OM
		ON
			OI.numOppId = OM.numOppID
		INNER JOIN
			Item I
		ON
			OI.numItemCode = I.numItemCode
		Left JOIN 
			Vendor V 
		ON 
			V.numVendorID=I.numVendorID 
			AND V.numItemCode=I.numItemCode
			
		WHERE
			OM.numDomainId=@numDomainID
			AND OM.numAssignedTo = TEMP1.numAssignedTo
			AND ISNULL(OI.monTotAmount,0) <> 0
			AND ISNULL(OI.numUnitHour,0) <> 0
			AND ISNULL(OM.tintOppType,0)=1
			AND ISNULL(OM.tintOppStatus,0)=1
			AND I.numItemCode NOT IN (@numShippingItemID,@numDiscountItemID)
			AND (  
		  (DATEADD(MINUTE,-@ClientTimeZoneOffset,OM.bintCreatedDate) BETWEEN @dtStartDate AND @dtEndDate)   
		  OR   
		  (DATEADD(MINUTE,-@ClientTimeZoneOffset,Om.bintCreatedDate) BETWEEN @dtStartDate AND @dtEndDate) 
		  )
					

	) TEMP2
	ORDER BY
		 TEMP2.Profit DESC
--#3
	SELECT
		UserMaster.numUserDetailId
		,UserMaster.vcUserName,@vcFilterBy as vcFilterBy,CAst(@dtStartDate as date) as StartDate,CAST(@dtEndDate  as date) as EndDate
		,AVG(ProfitPercentByOrder) AS AvgGrossProfitMargin
	FROM
	(
		SELECT DISTINCT
			--numAssignedTo
			CASE @vcFilterBy  
		   WHEN 1  -- Assign To  
		   THEN numAssignedTo
		   --else numRecOwner
		   WHEN 2 
		   THEN numRecOwner
		   else  numAssignedTo
		   End as numAssignedTo
		FROM
			OpportunityMaster
		WHERE
			numDomainId=@numDomainID
			AND tintOppType=1
			AND tintOppStatus = 1
				AND (  
      (DATEADD(MINUTE,-@ClientTimeZoneOffset,bintCreatedDate) BETWEEN @dtStartDate AND @dtEndDate)   
      OR   
      (DATEADD(MINUTE,-@ClientTimeZoneOffset,bintCreatedDate) BETWEEN @dtStartDate AND @dtEndDate)  
     ) 
			 AND
	 1=(CASE   
     WHEN LEN(ISNULL(@vcFilterValue,0)) > 0 AND @vcFilterBy IN (1,2,3)  
     THEN   
      (CASE @vcFilterBy  
       WHEN 1  -- Assign To  
       THEN (CASE WHEN numAssignedTo IN (SELECT Id FROM dbo.SplitIDs(@vcFilterValue,',')) THEN 1 ELSE 0 END)  
       WHEN 2  -- Record Owner  
       THEN (CASE WHEN numRecOwner IN (SELECT Id FROM dbo.SplitIDs(@vcFilterValue,',')) THEN 1 ELSE 0 END)  
       WHEN 3  -- Teams (Based on Assigned To)  
       --THEN (CASE WHEN numAssignedTo IN (SELECT numUserCntID FROM UserTeams WHERE numDomainID=@numDomainID AND numTeam IN (SELECT Id FROM dbo.SplitIDs(@vcFilterValue,','))) THEN 1 ELSE 0 END)  
	   THEN (CASE WHEN numAssignedTo IN (SELECT numContactId FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND numTeam IN (SELECT Id FROM dbo.SplitIDs(@vcFilterValue,','))) THEN 1 ELSE 0 END)  
      END)  
     ELSE 1   
    END)  
	) TEMP1
	INNER JOIN
		UserMaster
	ON
		TEMP1.numAssignedTo = UserMaster.numUserDetailId
		AND UserMaster.numDomainID = @numDomainID
	CROSS APPLY
	(
		SELECT
			OM.numOppID
			,(SUM(ISNULL(monTotAmount,0) - (ISNULL(numUnitHour,0) * (CASE @ProfitCost WHEN 3 THEN ISNULL(V.monCost,0) * dbo.fn_UOMConversion(numBaseUnit,I.numItemCode,@numDomainID,numPurchaseUnit) WHEN 2 THEN numCost * dbo.fn_UOMConversion(numBaseUnit,I.numItemCode,@numDomainID,numUOMId) ELSE monAverageCost END)))/SUM(ISNULL(monTotAmount,0))) * 100.0 ProfitPercentByOrder
		FROM
			OpportunityItems OI
		INNER JOIN
			OpportunityMaster OM
		ON
			OI.numOppId = OM.numOppID
		INNER JOIN
			Item I
		ON
			OI.numItemCode = I.numItemCode
		Left JOIN 
			Vendor V 
		ON 
			V.numVendorID=I.numVendorID 
			AND V.numItemCode=I.numItemCode
		WHERE
			OM.numDomainId=@numDomainID
			AND OM.numAssignedTo = TEMP1.numAssignedTo
			AND ISNULL(monTotAmount,0) <> 0
			AND ISNULL(numUnitHour,0) <> 0
			AND ISNULL(OM.tintOppType,0)=1
			AND ISNULL(OM.tintOppStatus,0)=1
			AND I.numItemCode NOT IN (@numShippingItemID,@numDiscountItemID)
			AND (  
		  (DATEADD(MINUTE,-@ClientTimeZoneOffset,OM.bintCreatedDate) BETWEEN @dtStartDate AND @dtEndDate)   
		  OR   
		  (DATEADD(MINUTE,-@ClientTimeZoneOffset,Om.bintCreatedDate) BETWEEN @dtStartDate AND @dtEndDate) 
		  )
		GROUP BY
			OM.numOppId
	) TEMP2
	GROUP BY
		UserMaster.vcUserName,UserMaster.numUserDetailId
	HAVING
		AVG(ProfitPercentByOrder) > 0
	ORDER BY
		AVG(ProfitPercentByOrder) DESC


--#4---Gross revenue
		SELECT
		UserMaster.numUserDetailId,UserMaster.vcUserName,Temp3.InvoiceTotal,@vcFilterBy as vcFilterBy,CAst(@dtStartDate as date) as StartDate,CAST(@dtEndDate  as date) as EndDate
	FROM
	(
		SELECT DISTINCT
			--numAssignedTo
			CASE @vcFilterBy  
		   WHEN 1  -- Assign To  
		   THEN numAssignedTo
		   --else numRecOwner
		   WHEN 2 
		   THEN numRecOwner
		   else  numAssignedTo
		   End as numAssignedTo
		FROM
			OpportunityMaster
		WHERE
			numDomainId=@numDomainID
			AND tintOppType=1
			AND tintOppStatus = 1
				AND (  
      (DATEADD(MINUTE,-@ClientTimeZoneOffset,bintCreatedDate) BETWEEN @dtStartDate AND @dtEndDate)   
      OR   
      (DATEADD(MINUTE,-@ClientTimeZoneOffset,bintCreatedDate) BETWEEN @dtStartDate AND @dtEndDate)  
     ) 
			AND
	 1=(CASE   
     WHEN LEN(ISNULL(@vcFilterValue,0)) > 0 AND @vcFilterBy IN (1,2,3)  
     THEN   
      (CASE @vcFilterBy  
       WHEN 1  -- Assign To  
       THEN (CASE WHEN numAssignedTo IN (SELECT Id FROM dbo.SplitIDs(@vcFilterValue,',')) THEN 1 ELSE 0 END)  
       WHEN 2  -- Record Owner  
       THEN (CASE WHEN numRecOwner IN (SELECT Id FROM dbo.SplitIDs(@vcFilterValue,',')) THEN 1 ELSE 0 END)  
       WHEN 3  -- Teams (Based on Assigned To)  
       --THEN (CASE WHEN numAssignedTo IN (SELECT numUserCntID FROM UserTeams WHERE numDomainID=@numDomainID AND numTeam IN (SELECT Id FROM dbo.SplitIDs(@vcFilterValue,','))) THEN 1 ELSE 0 END)  
	   THEN (CASE WHEN numAssignedTo IN (SELECT numContactId FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND numTeam IN (SELECT Id FROM dbo.SplitIDs(@vcFilterValue,','))) THEN 1 ELSE 0 END)  
      END)  
     ELSE 1   
    END)  
			
	) TEMP1
	INNER JOIN
		UserMaster
	ON
		TEMP1.numAssignedTo = UserMaster.numUserDetailId
		AND UserMaster.numDomainID = @numDomainID
	CROSS APPLY
	(
		SELECT
			Sum(
			ISNULL(OBD.monDealAmount,0) 
			 )
			 InvoiceTotal
		FROM
		
		--	OpportunityItems OI
		--INNER JOIN
		
			OpportunityMaster OM
		--ON
		--	OI.numOppId = OM.numOppID
		Inner join 
			OpportunityBizDocs OBD 
		ON
			OBD.numOppId = OM.numOppId and OBD.bitAuthoritativeBizDocs=1
		--INNER JOIN
		--	Item I
		--ON
		--	OI.numItemCode = I.numItemCode
		--Left JOIN 
		--	Vendor V 
		--ON 
		--	V.numVendorID=I.numVendorID 
		--	AND V.numItemCode=I.numItemCode
		WHERE
			OM.numDomainId=@numDomainID

			and CASE @vcFilterBy  
		   WHEN 1  -- Assign To  
		   THEN Om.numAssignedTo
		   WHEN 2 
		   THEN OM.numRecOwner
		   else  OM.numAssignedTo
		   End  = TEMP1.numAssignedTo
			--AND OM.numRecOwner = TEMP1.numAssignedTo
			--AND ISNULL(OI.monTotAmount,0) <> 0
			--AND ISNULL(OI.numUnitHour,0) <> 0
			AND ISNULL(OM.tintOppType,0)=1
			AND ISNULL(OM.tintOppStatus,0)=1
			AND OBD.numBizDocId = 287
			
			--AND I.numItemCode NOT IN (@numShippingItemID,@numDiscountItemID) /*if we keep this condition then sales order invoice total will not match. also it was producing duplicates.*/
			AND (  
      (DATEADD(MINUTE,-@ClientTimeZoneOffset,OM.bintCreatedDate) BETWEEN @dtStartDate AND @dtEndDate)   
      OR   
      (DATEADD(MINUTE,-@ClientTimeZoneOffset,Om.bintCreatedDate) BETWEEN @dtStartDate AND @dtEndDate) 
	  )

	) TEMP3
	
	
	ORDER BY
		 TEMP3.InvoiceTotal DESC
		-----
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS (SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ReportListMaster_WarehouseInventoryValue')
DROP PROCEDURE USP_ReportListMaster_WarehouseInventoryValue
GO


CREATE procedure [dbo].[USP_ReportListMaster_WarehouseInventoryValue] 
@numDomainID numeric
As
Begin
With 
Cte
as
(
Select --top 15 15 AS TotalParentRecords, 
--isnull(WareHouseItems.numOnHand,'0') as OnHand,
isnull(Item.monAverageCost,'0') as AverageCost,
isnull(Warehouses.vcWareHouse,'') as WareHouse,
--CONCAT('~/Items/frmItemList.aspx?Page=InventoryItems&ItemGroup=0&WID=',Warehouses.numWareHouseID) AS URL,
ISNULL(Warehouses.numWareHouseID,'') as WareHouseID,

--isnull((WareHouseItems.numOnHand + WarehouseItems.numAllocation)*(Item.monAverageCost),0)	as InventoryValue	 
(isnull(WareHouseItems.numOnHand,0) + isnull(WarehouseItems.numAllocation,0))*(isnull(Item.monAverageCost,0))as InventoryValue
FROM Item   
Left Join Vendor on Vendor.numDomainId = @numDomainID and Vendor.numVendorID = Item.numVendorID and  Item.numItemCode= Vendor.numItemCode   
join WareHouseItems  on WareHouseItems.numDomainId = @numDomainID and Item.numItemCode=WareHouseItems.numItemID 
LEFT JOIN WarehouseLocation ON WarehouseLocation.numWLocationID = WareHouseItems.numWLocationID   
join Warehouses Warehouses on Warehouses.numDomainId = @numDomainID and Warehouses.numWareHouseID=WareHouseItems.numWareHouseID 
WHERE 1=1 AND Item.numDomainId = @numDomainID AND (ISNULL(WareHouseItems.numOnHand,0) + ISNULL(WareHouseItems.numAllocation,0) >0)  and isnull(Item.monAverageCost,0) >0 
and  isnull(Item.bitArchiveItem,0) = 0 

)
select cte.WareHouse,
cte.WareHouseID,
--Cte.URL,
cast(SUM(cte.InventoryValue) as DECIMAL(18,0)) as InventoryValue
from  cte group by Cte.WareHouse,Cte.WareHouseID
--Cte.URL 
order by InventoryValue desc

END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_TicklerActItems')
DROP PROCEDURE USP_TicklerActItems
GO
CREATE Proc [dbo].[USP_TicklerActItems]                                                                       
@numUserCntID as numeric(9)=null,                                                                          
@numDomainID as numeric(9)=null,                                                                        
@startDate as datetime,                                                                        
@endDate as datetime,                                                                    
@ClientTimeZoneOffset Int,                                                                 
@bitFilterRecord bit=0,
@columnName varchar(50), 
@columnSortOrder varchar(50),
@vcBProcessValue varchar(500),
@RegularSearchCriteria VARCHAR(MAX)='',
@CustomSearchCriteria VARCHAR(MAX)='', 
@OppStatus int,
@CurrentPage int,                                                              
@PageSize int                                        
As     

SET @RegularSearchCriteria=REPLACE(@RegularSearchCriteria, '.numFollowUpStatus', 'Div.numFollowUpStatus'); 
SET @RegularSearchCriteria=REPLACE(@RegularSearchCriteria, 'itemDesc', 'textDetails'); 

	                                                                    
  
DECLARE @tintPerformanceFilter AS TINYINT
DECLARE @tintActionItemsViewRights TINYINT  = 3

IF EXISTS (SELECT * FROM GroupAuthorization WHERE numDomainID=@numDomainID AND numModuleID=1 AND numPageID=2 AND numGroupID IN (SELECT ISNULL(numGroupID,0) FROM UserMaster WHERE numDomainID=@numDomainID AND numUserDetailId=@numUserCntID))
BEGIN
	SELECT @tintActionItemsViewRights=ISNULL(intViewAllowed,0) FROM GroupAuthorization WHERE numDomainID=@numDomainID AND numModuleID=1 AND numPageID=2 AND numGroupID IN (SELECT ISNULL(numGroupID,0) FROM UserMaster WHERE numDomainID=@numDomainID AND numUserDetailId=@numUserCntID)
END

IF PATINDEX('%cmp.vcPerformance=1%', @RegularSearchCriteria)>0 --Last 3 Months
BEGIN
	SET @tintPerformanceFilter = 1
END
ELSE IF PATINDEX('%cmp.vcPerformance=2%', @RegularSearchCriteria)>0 --Last 6 Months
BEGIN
	SET @tintPerformanceFilter = 2
END
ELSE IF PATINDEX('%cmp.vcPerformance=3%', @RegularSearchCriteria)>0 --Last 1 Year
BEGIN
	SET @tintPerformanceFilter = 3
END
ELSE
BEGIN
	SET @tintPerformanceFilter = 0
END

IF @columnName = 'vcPerformance'   
BEGIN
	SET @columnName = 'monDealAmount'
END

DECLARE @vcSelectedEmployess VARCHAR(100)
DECLARE @numCriteria INT
DECLARE @vcActionTypes VARCHAR(100)

SELECT 
	@vcSelectedEmployess = vcSelectedEmployee,
	@numCriteria = numCriteria,
	@vcActionTypes = vcActionTypes
FROM 
	TicklerListFilterConfiguration
WHERE 
	numDomainID = @numDomainID AND
	numUserCntID = @numUserCntID


Create table #tempRecords (numContactID numeric(9),numDivisionID numeric(9),tintCRMType tinyint,
Id numeric(9),bitTask varchar(100),Startdate datetime,EndTime datetime,
itemDesc text,[Name] varchar(200),vcFirstName varchar(100),vcLastName varchar(100),numPhone varchar(15),numPhoneExtension varchar(7),
[Phone] varchar(30),vcCompanyName varchar(100),vcProfile VARCHAR(100), vcEmail varchar(50),Task varchar(100),Activity varchar(MAX),
Status varchar(100),numCreatedBy numeric(9),numRecOwner VARCHAR(500), numModifiedBy VARCHAR(500), numOrgTerId NUMERIC(18,0), numTerId VARCHAR(200),numAssignedTo varchar(1000),numAssignedBy varchar(50),
 caseid numeric(9),vcCasenumber varchar(50),casetimeId numeric(9),caseExpId numeric(9),type int,itemid varchar(50),
changekey varchar(50),DueDate varchar(50),bitFollowUpAnyTime bit,numNoOfEmployeesId varchar(50),vcComPhone varchar(50),numFollowUpStatus varchar(200),dtLastFollowUp varchar(500),vcLastSalesOrderDate VARCHAR(100),monDealAmount DECIMAL(20,5), vcPerformance VARCHAR(100),vcColorScheme VARCHAR(50),
Slp_Name varchar(100),intTotalProgress INT,vcPoppName varchar(100), numOppId NUMERIC(18,0),numBusinessProcessID numeric(18,0),numCompanyRating varchar(500),numStatusID varchar(500),numCampaignID varchar(500),[Action-Item Participants] NVARCHAR(MAX),HtmlLink varchar(500),Location nvarchar(150),OrignalDescription text)                                                         
 
 
declare @strSql as varchar(MAX)                                                            
declare @strSql1 as varchar(MAX)                         
declare @strSql2 as varchar(MAX)                                                            
declare @vcCustomColumnName AS VARCHAR(8000)
declare @vcnullCustomColumnName AS VARCHAR(8000)
   -------Change Row Color-------
Declare @vcCSOrigDbCOlumnName as varchar(50),@vcCSLookBackTableName as varchar(50),@vcCSAssociatedControlType as varchar(50)
SET @vcCSOrigDbCOlumnName=''
SET @vcCSLookBackTableName=''

declare @tintOrder as tinyint                                                
declare @vcFieldName as varchar(50)                                                
declare @vcListItemType as varchar(3)                                           
declare @vcAssociatedControlType varchar(20)                                                
declare @numListID AS numeric(9)                                                
declare @vcDbColumnName varchar(20)                    
declare @WhereCondition varchar(2000)                     
declare @vcLookBackTableName varchar(2000)              
Declare @bitCustomField as BIT
DECLARE @bitAllowEdit AS CHAR(1)                 
Declare @numFieldId as numeric  
DECLARE @bitAllowSorting AS CHAR(1)   
declare @vcColumnName AS VARCHAR(500)       
       
Declare @ListRelID as numeric(9)             
set @tintOrder=0               


	CREATE TABLE #tempForm 
	(
		tintOrder TINYINT,vcDbColumnName nvarchar(50),vcFieldDataType nvarchar(50),vcOrigDbColumnName nvarchar(50),bitAllowFiltering BIT,vcFieldName nvarchar(50)
		,vcAssociatedControlType nvarchar(50),vcListItemType char(3),numListID numeric(9),vcLookBackTableName varchar(50),bitCustomField BIT,numFieldId NUMERIC
		,bitAllowSorting BIT,bitAllowEdit BIT,bitIsRequired BIT,bitIsEmail BIT,bitIsAlphaNumeric BIT,bitIsNumeric BIT,bitIsLengthValidation BIT,intMaxLength int
		,intMinLength int,bitFieldMessage BIT,vcFieldMessage VARCHAR(500),ListRelID numeric(9),intColumnWidth NUMERIC(18,2)
	)
                
	--Custom Fields related to Organization
	INSERT INTO 
		#tempForm
	SELECT  
		tintRow + 1 AS tintOrder,vcDbColumnName,'',vcDbColumnName,1,vcFieldName,vcAssociatedControlType,'' as vcListItemType,numListID,'' as vcLookBackTableName                                                 
		,bitCustom,numFieldId,bitAllowSorting,0,bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength
		,bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth
	FROM 
		View_DynamicCustomColumns
	WHERE 
		numFormId=43 
		AND numUserCntID=@numUserCntID 
		AND numDomainID=@numDomainID 
		AND tintPageType=1
		AND ISNULL(bitCustom,0)=1
 
	SELECT TOP 1 
		@tintOrder = tintOrder + 1
		,@vcDbColumnName=vcDbColumnName
		,@vcFieldName=vcFieldName
		,@vcAssociatedControlType=vcAssociatedControlType
		,@vcListItemType=vcListItemType
		,@numListID=numListID
		,@vcLookBackTableName=vcLookBackTableName                                               
		,@bitCustomField=bitCustomField
		,@numFieldId=numFieldId
		,@bitAllowSorting=bitAllowSorting
		,@bitAllowEdit=0
		,@ListRelID=ListRelID
	FROM 
		#tempForm 
	ORDER BY 
		tintOrder ASC     
  
	SET @vcCustomColumnName=''
	SET @vcnullCustomColumnName=''

	WHILE @tintOrder>0      
	BEGIN  
		SET @vcColumnName= @vcDbColumnName
		SET @strSql = 'ALTER TABLE #tempRecords ADD [' + @vcColumnName + '] NVARCHAR(MAX)'
		EXEC(@strSql)
		
		IF @bitCustomField = 1
		BEGIN 
			IF @vcAssociatedControlType = 'TextBox' OR @vcAssociatedControlType = 'TextArea'            
			BEGIN            
				SET @vcCustomColumnName= @vcCustomColumnName+', ISNULL((SELECT TOP 1 CFW.Fld_Value FROM CFW_FLD_Values AS CFW WHERE CFW.Fld_ID='+CAST(@numFieldId AS VARCHAR(50))+' AND CFW.RecId=Div.numDivisionID),'''') ['+ @vcColumnName +']'
				SET @vcnullCustomColumnName= @vcnullCustomColumnName+','''' AS  ['+ @vcColumnName +']'
			END  
			ELSE IF @vcAssociatedControlType = 'CheckBox'         
			BEGIN            
				SET @vcCustomColumnName= @vcCustomColumnName+',ISNULL((SELECT TOP 1 CFW.Fld_Value FROM CFW_FLD_Values AS CFW WHERE CFW.Fld_ID='+CAST(@numFieldId AS VARCHAR(50))+' AND CFW.RecId=Div.numDivisionID),0)  ['+ @vcColumnName + ']'
				SET @vcnullCustomColumnName= @vcnullCustomColumnName+',0 AS ['+ @vcColumnName + ']'
			END            
			ELSE IF @vcAssociatedControlType = 'DateField'        
			BEGIN            
				SET @vcCustomColumnName= @vcCustomColumnName+',dbo.FormatedDateFromDate((SELECT TOP 1 CFW.Fld_Value FROM CFW_FLD_Values AS CFW WHERE CFW.Fld_ID='+CAST(@numFieldId AS VARCHAR(50))+' AND CFW.RecId=Div.numDivisionID),'+convert(varchar(10),@numDomainId)+')  ['+ @vcColumnName +']'              
				SET @vcnullCustomColumnName= @vcnullCustomColumnName+','''' AS  ['+ @vcColumnName +']'
			END            
			ELSE IF @vcAssociatedControlType = 'SelectBox'           
			BEGIN            
				SET @vcCustomColumnName=@vcCustomColumnName+',(SELECT TOP 1 ISNULL(L.vcData,'''') FROM CFW_FLD_Values CFW left Join ListDetails L on L.numListItemID=CFW.Fld_Value WHERE  CFW.Fld_ID='+CAST(@numFieldId AS VARCHAR(50))+' AND CFW.RecId=Div.numDivisionID)'+' ['+ @vcColumnName +']'                                                         
				SET @vcnullCustomColumnName=@vcnullCustomColumnName+','''''+'AS  ['+ @vcColumnName +']'                                                         
			END
			ELSE IF @vcAssociatedControlType = 'CheckBoxList'
			BEGIN
				SET @vcCustomColumnName=@vcCustomColumnName+',(SELECT STUFF((SELECT CONCAT('', '', vcData) FROM ListDetails WHERE numListID= ' + CAST(@numListID AS varchar) + ' AND numlistitemid IN (SELECT ISNULL(Id,0) FROM dbo.SplitIDs(ISNULL((SELECT CFWInner.Fld_Value FROM CFW_FLD_Values CFWInner WHERE CFWInner.Fld_ID='+CAST(@numFieldId AS VARCHAR(50))+' AND CFWInner.RecId=Div.numDivisionID),''''),'','')) FOR XML PATH('''')), 1, 1, ''''))'+' ['+ @vcColumnName +']'
				SET @vcnullCustomColumnName=@vcnullCustomColumnName+','''''+'AS  ['+ @vcColumnName +']'
			END
			ELSE
			BEGIN
				SET @vcCustomColumnName= CONCAT(@vcCustomColumnName,',(SELECT dbo.fn_GetCustFldStringValue(',@numFieldId,',CFW.RecId,CFW.Fld_Value',') FROM CFW_FLD_Values CFW left Join ListDetails L on L.numListItemID=CFW.Fld_Value WHERE  CFW.Fld_ID=',@numFieldId,' AND CFW.RecId=Div.numDivisionID)',' [', @vcColumnName ,']')
				SET @vcnullCustomColumnName=@vcnullCustomColumnName+','''''+'AS  ['+ @vcColumnName +']' 
			END
		END
		  
		SELECT TOP 1 
			@tintOrder=tintOrder + 1
			,@vcDbColumnName=vcDbColumnName
			,@vcFieldName=vcFieldName
			,@vcAssociatedControlType=vcAssociatedControlType
			,@vcListItemType=vcListItemType
			,@numListID=numListID
			,@vcLookBackTableName=vcLookBackTableName                                               
			,@bitCustomField=bitCustomField
			,@numFieldId=numFieldId
			,@bitAllowSorting=bitAllowSorting
			,@bitAllowEdit=0
			,@ListRelID=ListRelID
		FROM 
			#tempForm 
		WHERE 
			tintOrder > @tintOrder-1 
		ORDER BY 
			tintOrder asc            
 
		IF @@rowcount=0 set @tintOrder=0 
	END

 SET @strSql=''
 PRINT @vcCustomColumnName
--select top 1 @vcCSOrigDbCOlumnName=DFM.vcOrigDbCOlumnName,@vcCSLookBackTableName=DFM.vcLookBackTableName from DycFieldColorScheme DFCS join DycFieldMaster DFM on 
--DFCS.numModuleID=DFM.numModuleID and DFCS.numFieldID=DFM.numFieldID
--where DFCS.numDomainID=@numDomainID and DFM.numModuleID=1

Create table #tempColorScheme(vcOrigDbCOlumnName varchar(50),vcLookBackTableName varchar(50),vcFieldValue varchar(50),vcFieldValue1 varchar(50),vcColorScheme varchar(50),vcAssociatedControlType varchar(50))

insert into #tempColorScheme select DFM.vcOrigDbCOlumnName,DFM.vcLookBackTableName,DFCS.vcFieldValue,DFCS.vcFieldValue1,DFCS.vcColorScheme,DFFM.vcAssociatedControlType 
from DycFieldColorScheme DFCS join DycFormField_Mapping DFFM on DFCS.numFieldID=DFFM.numFieldID
join DycFieldMaster DFM on DFM.numModuleID=DFFM.numModuleID and DFM.numFieldID=DFFM.numFieldID
where DFCS.numDomainID=@numDomainID and DFFM.numFormID=43 AND DFCS.numFormID=43 and isnull(DFFM.bitAllowGridColor,0)=1

IF(SELECT COUNT(*) FROM #tempColorScheme)>0
BEGIN
   select top 1 @vcCSOrigDbCOlumnName=vcOrigDbCOlumnName,@vcCSLookBackTableName=vcLookBackTableName,@vcCSAssociatedControlType=vcAssociatedControlType from #tempColorScheme
END   
----------------------------                   


SET @strSql = @strSql+'insert into #tempRecords
Select * from (                                                    
 SELECT Addc.numContactID,Addc.numDivisionID,Div.tintCRMType,Comm.numCommId AS Id,convert(varchar(15),bitTask) as bitTask,                                               
 DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', dtStartTime) as Startdate,                        
 DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', dtEndTime) as EndTime ,                                                                        
 Comm.textDetails AS itemDesc,                                                                                                               
 AddC.vcFirstName + '' '' + AddC.vcLastName  as [Name], 
 AddC.vcFirstName,AddC.vcLastName,Addc.numPhone,AddC.numPhoneExtension,                               
 case when Addc.numPhone<>'''' then + AddC.numPhone +case when AddC.numPhoneExtension<>'''' then '' - '' + AddC.numPhoneExtension else '''' end  else '''' end as [Phone],                                                                       
 Comp.vcCompanyName As vcCompanyName , ISNULL((SELECT vcData FROM ListDetails WHERE numListID =21 AND numDomainID='+ CAST(@numDomainID AS VARCHAR) +' AND  numListItemID=Comp.vcProfile ) ,'''') vcProfile ,                                                        
 AddC.vcEmail As vcEmail ,         
dbo.GetListIemName(Comm.bitTask)AS Task,                
 listdetailsActivity.VcData As Activity ,                                                    
 listdetailsStatus.VcData As Status   ,  -- Priority column                                                                                             
 Comm.numCreatedBy,   
 dbo.fn_GetContactName(Comm.numCreatedBy) AS numRecOwner,
 dbo.fn_GetContactName(Comm.numModifiedBy) AS numModifiedBy,                                                                                                          
 ISNULL(Div.numTerId,0) AS numOrgTerId,
 (select TOP 1 vcData from ListDetails where numListItemID=Div.numTerId) AS numTerId,                                           
Substring( 
dbo.fn_GetContactName(numAssign) + 
ISNULL((SELECT SUBSTRING((SELECT '','' + dbo.fn_GetContactName(numContactId) FROM CommunicationAttendees WHERE numCommId=Comm.numCommId FOR XML PATH('''')),1,200000)),''''),0,50)
As numAssignedTo , 
dbo.fn_GetContactName(Comm.numAssignedBy) AS numAssignedBy,        
convert(varchar(50),comm.caseid) as caseid,                          
(select top 1 vcCaseNumber from cases where cases.numcaseid= comm.caseid )as vcCasenumber,                          
ISNULL(comm.casetimeId,0) casetimeId,                          
ISNULL(comm.caseExpId,0) caseExpId,                        
0 as type   ,                    
'''' as itemid ,                    
'''' as changekey ,cast(Null as datetime) as DueDate ,ISNULL(bitFollowUpAnyTime,0) bitFollowUpAnyTime,
dbo.GetListIemName(Comp.numNoOfEmployeesId) AS numNoOfEmployeesId,Div.vcComPhone,
dbo.GetListIemName(Div.numFollowUpStatus) AS numFollowUpStatus,' + 
CONCAT('ISNULL(dbo.FormatedDateFromDate(Div.dtLastFollowUp,',@numDomainID,'),'''')') + ' AS dtLastFollowUp,' 
+ CONCAT('ISNULL(dbo.FormatedDateFromDate(TempLastOrder.bintCreatedDate,',@numDomainID,'),'''')') + ' AS vcLastSalesOrderDate,ISNULL(TempPerformance.monDealAmount,0) monDealAmount,(CASE WHEN ISNULL(TempPerformance.monDealAmount,0) > 0 THEN CONCAT(FORMAT(TempPerformance.monDealAmount,''##,####.00''),'' ('',CAST(TempPerformance.fltProfitPer AS DECIMAL(18,2)),''%)'') ELSE '''' END) AS vcPerformance '

IF len(@vcCSOrigDbCOlumnName)>0 and len(@vcCSLookBackTableName)>0
	set @strSql=@strSql+',tCS.vcColorScheme'
ELSE
	set @strSql=@strSql+','''' as vcColorScheme'

SET @strSql =@strSql + ' ,ISNULL(Slp_Name,'''') AS Slp_Name,ISNULL(intTotalProgress,0) AS [intTotalProgress],ISNULL(vcPoppName,'''') AS [vcPoppName], ISNULL(Opp.numOppId,0) AS [numOppId],ISNULL(Opp.numBusinessProcessID,0) AS numBusinessProcessID'
SET @strSql =@strSql + ' ,(select TOP 1 vcData from ListDetails where numListItemID=Comp.numCompanyRating) AS numCompanyRating ,(select TOP 1 vcData from ListDetails where numListItemID=Div.numStatusID) AS numStatusID,(SELECT TOP 1 vcCampaignName FROM CampaignMaster WHERE numCampaignID= Opp.numCampainID) AS numCampaignID,(SELECT DISTINCT  
replace(replace(STUFF((SELECT ''^<br>'' 
+ CASE(ISNULL(Comp.vcCompanyName,'''')) WHEN '''' THEN '''' else ''<a href=javascript:OpenCompany('' +  cast(isnull(Addc.numDivisionID,0) as varchar(20)) + '','' +  cast(isnull(Div.tintCRMType,0) as varchar(20)) + '',1) style=''''COLOR:black;TEXT-DECORATION:underline;''''>'' + ltrim(RTRIM(vcCompanyName)) +  ''</a> ''  END
+ CASE(isnull(AddC.vcFirstName,'''')) when '''' THEN ''''	else ''<a href=javascript:OpenContact('' +  cast(isnull(AddC.numContactId,0) as varchar(20)) + '',1) style=''''COLOR:#dd8047;''''>'' + ltrim(RTRIM(AddC.vcFirstName)) + ''</a> ''  END
+ CASE(isnull(addc.vcLastName,'''')) when '''' THEN '''' else ''<a href=javascript:OpenContact('' +  cast(isnull(AddC.numContactId,0) as varchar(20)) + '',1) style=''''COLOR:#dd8047;''''>'' + ltrim(RTRIM(AddC.vcLastName)) + ''</a> ''  END 
+ CASE(isnull(AddC.numPhone,'''')) WHEN '''' then '''' ELSE AddC.numPhone + '' '' END
+ CASE(isnull(AddC.numPhoneExtension,'''')) when '''' then '''' else '' ('' + AddC.numPhoneExtension + '') '' END 
+ CASE(isnull(addc.vcEmail,'''')) when '''' THEN '''' ELSE ''<a href=''''#'''' onclick=javascript:OpemEmail(''''../contact/frmComposeWindow.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Lsemail='' + ltrim(RTRIM(AddC.vcEmail)) + '''''') style=''''COLOR:#3c8dbc;''''>'' + ltrim(RTRIM(AddC.vcEmail)) + ''</a> ''  END 
FOR XML PATH('''')
), 1, 1, ''''),''&lt;'', ''<''), ''&gt;'', ''>'')) AS [Action-Item Participants],'''' AS HtmlLink, '''' AS Location,'''' As OrignalDescription'
SET @strSql =@strSql+ @vcCustomColumnName
SET @strSql =@strSql + ' FROM  Communication Comm                              
 JOIN AdditionalContactsInformation AddC                                                  
 ON Comm.numContactId = AddC.numContactId                                  
 JOIN DivisionMaster Div                                                                         
 ON AddC.numDivisionId = Div.numDivisionID                                   
 JOIN CompanyInfo  Comp                                                                         
 ON Div.numCompanyID = Comp.numCompanyId     
 OUTER APPLY(' + CONCAT('SELECT MAX(bintCreatedDate) AS bintCreatedDate FROM OpportunityMaster WHERE numDomainID = ',@numDomainID,' AND numDivisionID=Div.numDivisionID AND tintOppType = 1 AND tintOppStatus = 1') + ' ) AS TempLastOrder
 OUTER APPLY(' + CONCAT('SELECT
																	SUM(monDealAmount) AS monDealAmount
																	,AVG(ProfitPer) AS fltProfitPer
																FROM
																(
																	SELECT
																		OM.numDivisionId
																		,monDealAmount
																		,AVG(((ISNULL(OI.monPrice,0) - (CASE D.numCost WHEN 3 THEN ISNULL(V.monCost,0) * dbo.fn_UOMConversion(I.numBaseUnit,I.numItemCode,I.numDomainID,I.numPurchaseUnit) WHEN 2 THEN ISNULL(OI.numCost,0) ELSE ISNULL(I.monAverageCost,0) END)) / ISNULL(NULLIF(OI.monPrice,0),1)) * 100) ProfitPer
																	FROM
																		OpportunityMaster OM
																	INNER JOIN
																		Domain D
																	ON
																		OM.numDomainID = D.numDomainID
																	INNER JOIN
																		OpportunityItems OI
																	ON
																		OM.numOppId = OI.numOppId
																	INNER JOIN
																		Item I
																	ON
																		OI.numItemCode = I.numItemCode
																	LEFT JOIN
																		Vendor V
																	ON
																		V.numVendorID = I.numVendorID
																		AND V.numItemCode = I.numItemCode
																	WHERE
																		OM.numDomainId = ',@numDomainID,'
																		AND OM.numDivisionId = Div.numDivisionID
																		AND ISNULL(I.bitContainer,0) = 0
																		AND tintOppType = 1
																		AND tintOppStatus=1
																		AND 1 = (CASE ',@tintPerformanceFilter,' 
																				WHEN 1 THEN (CASE WHEN OM.bintCreatedDate BETWEEN DATEADD(MONTH,-3,GETUTCDATE()) AND GETUTCDATE() THEN 1 ELSE 0 END)
																				WHEN 2 THEN (CASE WHEN OM.bintCreatedDate BETWEEN DATEADD(MONTH,-6,GETUTCDATE()) AND GETUTCDATE() THEN 1 ELSE 0 END)
																				WHEN 3 THEN (CASE WHEN OM.bintCreatedDate BETWEEN DATEADD(YEAR,-1,GETUTCDATE()) AND GETUTCDATE() THEN 1 ELSE 0 END)
																				ELSE 1
																				END)
																	GROUP BY
																		OM.numDivisionId
																		,OM.numOppId
																		,monDealAmount
																) T1
																GROUP BY
																	T1.numDivisionId )') + ' AS TempPerformance
 Left Join listdetails listdetailsActivity                                                  
 On Comm.numActivity = listdetailsActivity.NumlistItemID                                               
 Left Join listdetails listdetailsStatus                                                 
 On Comm.numStatus = listdetailsStatus.NumlistItemID
 LEFT JOIN OpportunityMaster Opp ON Opp.numDivisionId = Div.numDivisionID AND Opp.numContactId = AddC.numContactId  AND Opp.numOppID = Comm.numOppID 
 LEFT JOIN Sales_process_List_Master splm ON splm.Slp_Id = Opp.numBusinessProcessID 
 LEFT JOIN ProjectProgress PP On PP.numOppId = Opp.numOppId '
 

DECLARE @Prefix AS VARCHAR(5)
 
 IF len(@vcCSOrigDbCOlumnName)>0 and len(@vcCSLookBackTableName)>0
BEGIN
	 if @vcCSLookBackTableName = 'AdditionalContactsInformation'                  
		set @Prefix = 'AddC.'                  
  else if @vcCSLookBackTableName = 'DivisionMaster'                  
		set @Prefix = 'Div.'
	else if @vcCSLookBackTableName = 'CompanyInfo'                  
		set @Prefix = 'Comp.'  
	ELSE IF @vcCSLookBackTableName='Tickler'	
		set @Prefix = 'Comm.'  
--	else if @vcCSLookBackTableName = 'ProjectProgress'                  
--		set @Prefix = 'pp.'  	
--	else if @vcCSLookBackTableName = 'OpportunityMaster'                  
--		set @Prefix = 'om.'  
--	else if @vcCSLookBackTableName = 'Sales_process_List_Master'                  
--		set @Prefix = 'splm.'  		
	else
		set @Prefix = ''   
	
	
	IF @vcCSAssociatedControlType='DateField'
	BEGIN
	IF @vcCSOrigDbCOlumnName='FormattedDate'
			SET @vcCSOrigDbCOlumnName='dtStartTime'
	
	
			set @strSql=@strSql+' left join #tempColorScheme tCS on CONVERT(VARCHAR(10),DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',' + @Prefix + @vcCSOrigDbCOlumnName + '),111) >= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue as int),GETUTCDATE()),111)
			 and CONVERT(VARCHAR(10),DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',' + @Prefix + @vcCSOrigDbCOlumnName + '),111) <= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue1 as int),GETUTCDATE()),111)'
	END
	ELSE IF @vcCSAssociatedControlType='SelectBox' OR @vcCSAssociatedControlType='ListBox' OR @vcCSAssociatedControlType='CheckBox'  OR @vcCSAssociatedControlType='TextBox'
	BEGIN
		--PRINT 1
		set @strSql=@strSql+' left join #tempColorScheme tCS on tCS.vcFieldValue=' + @Prefix + @vcCSOrigDbCOlumnName
	END
END

set @strSql=@strSql+'  WHERE Comm.numDomainID = ' + Cast(@numDomainID as varchar(10))  + ' AND 1 = (CASE ' + CAST( @tintActionItemsViewRights AS VARCHAR) + 
' WHEN 3 THEN 1 
WHEN 2 THEN (CASE WHEN Div.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+ CONVERT(VARCHAR(15),@numUserCntID) + ' ) THEN 1 ELSE 0 END)
WHEN 1 THEN (CASE WHEN (Comm.numCreatedBy= ' + CONVERT(VARCHAR(15),@numUserCntID) + ' or Comm.numAssign= '+ CONVERT(VARCHAR(15),@numUserCntID) + ') THEN 1 ELSE 0 END) 
ELSE 0 END) '
 
 IF LEN(ISNULL(@vcBProcessValue,''))>0
BEGIN
	SET @strSql = @strSql + ' and ISNULL(Opp.numBusinessProcessID,0) in (SELECT Items FROM dbo.Split(''' +@vcBProcessValue + ''','',''))' 			
END

IF LEN(@vcActionTypes) > 0 
BEGIN
	SET @strSql = @strSql + ' AND Comm.bitTask IN (' + @vcActionTypes + ')'  
END
  
IF @numCriteria  = 1 --Action Items	Assigned
BEGIN
	set @strSql=@strSql+' AND 1=(Case when Comm.numAssign IN (' + CASE WHEN LEN(@vcSelectedEmployess) > 0 THEN @vcSelectedEmployess ELSE Cast(@numUserCntID as varchar(10)) END +') then 1 else 0 end)'
END
ELSE IF @numCriteria = 2 --Action Items Owned
BEGIN
	set @strSql=@strSql+' AND 1=(Case when Comm.numCreatedBy IN (' + CASE WHEN LEN(@vcSelectedEmployess) > 0 THEN @vcSelectedEmployess ELSE Cast(@numUserCntID as varchar(10)) END +' ) then 1 else 0 end)'
END
ELSE --Action Items Assigned Or Owned
BEGIN
	set @strSql=@strSql+' AND 1=(Case when (Comm.numAssign IN (' + CASE WHEN LEN(@vcSelectedEmployess) > 0 THEN @vcSelectedEmployess ELSE Cast(@numUserCntID as varchar(10)) END +')  or Comm.numCreatedBy IN (' + CASE WHEN LEN(@vcSelectedEmployess) > 0 THEN @vcSelectedEmployess ELSE Cast(@numUserCntID as varchar(10)) END +')) then 1 else 0 end)'
END

        
  
IF(LEN(@RegularSearchCriteria)>0)
BEGIN
IF PATINDEX('%cmp.vcPerformance%', @RegularSearchCriteria)>0
BEGIN
	-- WE ARE MANAGING CONDITION IN OUTER APPLY
	set @strSql=@strSql
END
ELSE
BEGIN
	SET @strSql=@strSql+' AND '+@RegularSearchCriteria
END
END

IF LEN(@CustomSearchCriteria) > 0
BEGIN
	SET @strSql=@strSql +' AND '+ @CustomSearchCriteria
END

set @strSql=@strSql+' AND (DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',dtStartTime) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',dtStartTime) <= '''+Cast(@endDate as varchar(30))+''')'                                             

IF @OppStatus  = 0 --OPEN
BEGIN
	set @strSql=@strSql+'  AND Comm.bitclosedflag=0 AND Comm.bitTask <> 973  ) As X '
END
ELSE IF @OppStatus = 1 --CLOSED
BEGIN
	set @strSql=@strSql+' AND Comm.bitclosedflag=1 AND Comm.bitTask <> 973  ) As X '
END
ELSE 
BEGIN
	set @strSql=@strSql+' AND Comm.bitclosedflag in (0,1) AND Comm.bitTask <> 973  ) As X '
END
                                
              PRINT CAST(@strSql AS TEXT)

IF LEN(ISNULL(@vcBProcessValue,''))=0 AND LEN(@RegularSearchCriteria) = 0 AND LEN(@CustomSearchCriteria) = 0
BEGIN
	If LEN(ISNULL(@vcActionTypes,'')) = 0 OR EXISTS (SELECT OutParam FROM dbo.SplitString(@vcActionTypes,',') WHERE OutParam='982')
	BEGIN
		SET @strSql1 =  ' insert into #tempRecords 
select                          
 ACI.numContactID,ACI.numDivisionID,Div.tintCRMType,ac.activityid as id ,case when alldayevent = 1 then ''5'' else ''0'' end as bitTask,                        
DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', startdatetimeutc) startdate,                        
DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', dateadd(second,duration,startdatetimeutc)) as endtime,                        
(SELECT DISTINCT  
replace(replace((SELECT 
CASE(ISNULL(bitTitle,1)) WHEN 1 THEN ''<b><font color=red>Title</font></b><br>'' + CASE(ISNULL(ac.Subject,'''')) WHEN '''' THEN '''' else ac.Subject END + ''<br>'' else ''''  END  
+ CASE(ISNULL(bitLocation,1)) WHEN 1 THEN ''<b><font color=green>Location</font></b><br>'' + CASE(ISNULL(ac.location,'''')) WHEN '''' THEN '''' else ac.location END + ''<br>'' else ''''  END  
+ CASE(isnull(bitDescription,1)) when 1 THEN ''<b><font color=#3c8dbc>Description</font></b><br>$'' else ''''  END  
FROM ActivityDisplayConfiguration where numDomainId='+ CAST(@numDomainID AS VARCHAR) +' and numUserCntId='+ CAST(@numUserCntID AS VARCHAR) +'
FOR XML PATH('''')
),''&lt;'', ''<''), ''&gt;'', ''>'')) AS itemDesc,                        
ACI.vcFirstName + '' '' + ACI.vcLastName name, 
 ACI.vcFirstName,ACI.vcLastName,ACI.numPhone,ACI.numPhoneExtension,                   
 case when ACI.numPhone<>'''' then + ACI.numPhone +case when ACI.numPhoneExtension<>'''' then '' - '' + ACI.numPhoneExtension else '''' end  else '''' end as [Phone],                        
 Substring ( Comp.vcCompanyName , 0 , 100 ) + ''..'' As vcCompanyName , ISNULL((SELECT vcData FROM ListDetails WHERE numListID =21 AND numDomainID='+ CAST(@numDomainID AS VARCHAR) +' AND  numListItemID=Comp.vcProfile ) ,'''') vcProfile ,                      
vcEmail,                        
''Calendar'' as task,   
[subject] as Activity,                   
case when importance =0 then ''Low'' when importance =1 then ''Normal'' when importance =2 then ''High'' end as status,                        
0 as numcreatedby,
'''' as numRecOwner, 
'''' AS numModifiedBy,                  
0 as numorgterid,       
'''' as numterid,                 
''-/-'' as numAssignedTo, 
''-'' AS numAssignedBy,        
''0'' as caseid,                        
null as vccasenumber,                        
0 as casetimeid,                        
0 as caseexpid,                        
1 as type ,                    
isnull(itemid,'''') as itemid  ,                    
changekey  ,cast(Null as datetime) as DueDate,cast(0 as bit) as bitFollowUpAnyTime,
dbo.GetListIemName(Comp.numNoOfEmployeesId) AS numNoOfEmployeesId,Div.vcComPhone,
dbo.GetListIemName(Div.numFollowUpStatus) AS numFollowUpStatus,' + 
CONCAT('ISNULL(dbo.FormatedDateFromDate(Div.dtLastFollowUp,',@numDomainID,'),'''')') + ' AS dtLastFollowUp,' 
+ CONCAT('ISNULL(dbo.FormatedDateFromDate(TempLastOrder.bintCreatedDate,',@numDomainID,'),'''')') + ' AS vcLastSalesOrderDate,ISNULL(TempPerformance.monDealAmount,0) monDealAmount, (CASE WHEN ISNULL(TempPerformance.monDealAmount,0) > 0 THEN CONCAT(FORMAT(TempPerformance.monDealAmount,''##,####.00''),'' ('',CAST(TempPerformance.fltProfitPer AS DECIMAL(18,2)),''%)'') ELSE '''' END) vcPerformance,'''' as vcColorScheme,'''',0,'''',0 AS [numOppId],0 AS numBusinessProcessID '
SET @strSql1 =@strSql1 + ' ,(select TOP 1 vcData from ListDetails where numListItemID=Comp.numCompanyRating) AS numCompanyRating ,(select TOP 1 vcData from ListDetails where numListItemID=Comp.numCompanyRating) AS numCompanyRating,''-'' AS numCampaignID '+@vcnullCustomColumnName 



SET @strSql1=@strSql1 + ' ,(SELECT DISTINCT  
replace(replace(STUFF((SELECT ''^<br>'' 
+ CASE(ISNULL(CI.vcCompanyName,'''')) WHEN '''' THEN ''''		else ''<a href=javascript:OpenCompany('' +  cast(isnull(ATM.DivisionID,0) as varchar(20)) + '','' +  cast(isnull(ATM.tintCRMType,0) as varchar(20)) + '',1) style=''''COLOR:black;TEXT-DECORATION:underline;''''>'' + ltrim(RTRIM(CI.vcCompanyName)) + ''</a> ''  END  
+ CASE(isnull(ATM.AttendeeFirstName,'''')) when '''' THEN ''''	else ''<a href=javascript:OpenContact('' +  cast(isnull(ATM.ContactID,0) as varchar(20)) + '',1) style=''''COLOR:#dd8047;''''>'' + ltrim(RTRIM(ATM.AttendeeFirstName)) + ''</a> ''  END
+ CASE(isnull(ATM.AttendeeLastName,'''')) when '''' THEN '''' else ''<a href=javascript:OpenContact('' +  cast(isnull(ATM.ContactID,0) as varchar(20)) + '',1) style=''''COLOR:#dd8047;''''>'' + ltrim(RTRIM(ATM.AttendeeLastName)) + ''</a> ''  END 
+ CASE(isnull(ATM.AttendeePhone,'''')) WHEN '''' then '''' ELSE ATM.AttendeePhone + '' '' END
+ CASE(isnull(ATM.AttendeePhoneExtension,'''')) when '''' then '''' else '' ('' + ATM.AttendeePhoneExtension + '') '' END 
+ CASE(isnull(ATM.AttendeeEmail,''''))    when '''' THEN '''' ELSE ''<a href=''''#'''' onclick=javascript:OpemEmail(''''../contact/frmComposeWindow.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Lsemail='' + ltrim(RTRIM(ATM.AttendeeEmail)) + '''''') style=''''COLOR:#3c8dbc;''''>'' + ltrim(RTRIM(ATM.AttendeeEmail)) + ''</a> ''  END 
+ CASE(isnull(ATM.ResponseStatus,'''')) WHEN '''' THEN '''' WHEN  ''needsAction''  then '''' ELSE ''<img src=''''../images/'' + ATM.ResponseStatus + ''.gif'''' align=''''BASELINE'''' style=''''float:none;vertical-align:middle;width: 15px;''''/>'' END
FROM ActivityAttendees ATM
Left Join CompanyInfo CI on ATM.CompanyNameinBiz=CI.numCompanyId
WHERE ATM.ActivityID = ac.ActivityID and ATM.AttendeeEmail is not null
FOR XML PATH('''')
), 1, 1, ''''),''&lt;'', ''<''), ''&gt;'', ''>'')) AS [Action-Item Participants],ac.HtmlLink AS HtmlLink,ac.Location As Location,ac.ActivityDescription As OrignalDescription'



SET @strSql1 = @strSql1+' from activity ac join             
activityresource ar on  ac.activityid=ar.activityid             
join resource res on ar.resourceid=res.resourceid                         
--join usermaster UM on um.numUserId = res.numUserCntId                
join AdditionalContactsInformation ACI on ACI.numContactId = res.numUserCntId                
 JOIN DivisionMaster Div                                                          
 ON ACI.numDivisionId = Div.numDivisionID                                   
 JOIN CompanyInfo  Comp                             
 ON Div.numCompanyID = Comp.numCompanyId
 OUTER APPLY(' + CONCAT('SELECT MAX(bintCreatedDate) AS bintCreatedDate FROM OpportunityMaster WHERE numDomainID = ',@numDomainID,' AND numDivisionID=Div.numDivisionID AND tintOppType = 1 AND tintOppStatus = 1') + ' ) AS TempLastOrder                 
 OUTER APPLY(' + CONCAT('SELECT
																	SUM(monDealAmount) AS monDealAmount
																	,AVG(ProfitPer) AS fltProfitPer
																FROM
																(
																	SELECT
																		OM.numDivisionId
																		,monDealAmount
																		,AVG(((ISNULL(OI.monPrice,0) - (CASE D.numCost WHEN 3 THEN ISNULL(V.monCost,0) * dbo.fn_UOMConversion(I.numBaseUnit,I.numItemCode,I.numDomainID,I.numPurchaseUnit) WHEN 2 THEN ISNULL(OI.numCost,0) ELSE ISNULL(I.monAverageCost,0) END)) / ISNULL(NULLIF(OI.monPrice,0),1)) * 100) ProfitPer
																	FROM
																		OpportunityMaster OM
																	INNER JOIN
																		Domain D
																	ON
																		OM.numDomainID = D.numDomainID
																	INNER JOIN
																		OpportunityItems OI
																	ON
																		OM.numOppId = OI.numOppId
																	INNER JOIN
																		Item I
																	ON
																		OI.numItemCode = I.numItemCode
																	LEFT JOIN
																		Vendor V
																	ON
																		V.numVendorID = I.numVendorID
																		AND V.numItemCode = I.numItemCode
																	WHERE
																		OM.numDomainId = ',@numDomainID,'
																		AND OM.numDivisionId = Div.numDivisionID
																		AND ISNULL(I.bitContainer,0) = 0
																		AND tintOppType = 1
																		AND tintOppStatus=1
																		AND 1 = (CASE ',@tintPerformanceFilter,' 
																				WHEN 1 THEN (CASE WHEN OM.bintCreatedDate BETWEEN DATEADD(MONTH,-3,GETUTCDATE()) AND GETUTCDATE() THEN 1 ELSE 0 END)
																				WHEN 2 THEN (CASE WHEN OM.bintCreatedDate BETWEEN DATEADD(MONTH,-6,GETUTCDATE()) AND GETUTCDATE() THEN 1 ELSE 0 END)
																				WHEN 3 THEN (CASE WHEN OM.bintCreatedDate BETWEEN DATEADD(YEAR,-1,GETUTCDATE()) AND GETUTCDATE() THEN 1 ELSE 0 END)
																				ELSE 1
																				END)
																	GROUP BY
																		OM.numDivisionId
																		,OM.numOppId
																		,monDealAmount
																) T1
																GROUP BY
																	T1.numDivisionId )') + ' AS TempPerformance
 where  res.numUserCntId = ' + Cast(@numUserCntID as varchar(10)) +'             
and ac.OriginalStartDateTimeUtc IS NULL            
and res.numDomainID = ' + Cast(@numDomainID as varchar(10)) +'  and 
(DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', startdatetimeutc) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', startdatetimeutc) <= '''+Cast(@endDate as varchar(30))+''')
--(startdatetimeutc between dateadd(month,-1,getutcdate()) and dateadd(month,1,getutcdate())) 
and [status] <> -1                                                     
and ac.activityid not in 
(select distinct(numActivityId) from communication Comm where Comm.numDomainID = ' + Cast(@numDomainID as varchar(10)) +'  
AND 1=(Case ' + Cast(@bitFilterRecord as varchar(10)) +' when 1 then Case when Comm.numAssign =' + Cast(@numUserCntID as varchar(10)) +'  then 1 else 0 end 
		else  Case when (Comm.numAssign =' + Cast(@numUserCntID as varchar(10)) +'  or Comm.numCreatedBy=' + Cast(@numUserCntID as varchar(10)) +' ) then 1 else 0 end end)   
		AND 1 = (CASE ' + CAST( @tintActionItemsViewRights AS VARCHAR) + 
' WHEN 3 THEN 1 
WHEN 2 THEN (CASE WHEN Div.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+ CONVERT(VARCHAR(15),@numUserCntID) + ' ) THEN 1 ELSE 0 END)
WHEN 1 THEN (CASE WHEN (Comm.numCreatedBy= ' + CONVERT(VARCHAR(15),@numUserCntID) + ' or Comm.numAssign= '+ CONVERT(VARCHAR(15),@numUserCntID) + ') THEN 1 ELSE 0 END) 
ELSE 0 END)                                                                   
-- AND (dtStartTime >= '''+Cast(@startDate as varchar(30))+''' and dtStartTime <= '''+Cast(@endDate as varchar(30))+''') 
) 
 Order by endtime OFFSET ' + CAST(((@CurrentPage - 1) * @PageSize) AS VARCHAR(50)) + ' ROWS FETCH NEXT ' + CAST(@PageSize AS VARCHAR(50)) + ' ROWS ONLY'
 
	END

-- btDocType =2 for Document approval request, =1 for bizdoc
SET @strSql2 = ' insert into #tempRecords
select 
B.numContactID,B.numDivisionID,E.tintCRMType,CASE BA.btDocType WHEN 2 THEN BA.numOppBizDocsId WHEN 3 THEN BA.numOppBizDocsId ELSE A.numBizActionID END as ID,
convert(varchar(15),A.bitTask) as bitTask,
DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', dtCreatedDate) as Startdate,                        
DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', dtCreatedDate) as EndTime ,  
'''' as itemDesc,
vcFirstName + '' '' + vcLastName  as [Name],
B.vcFirstName , B.vcLastName ,B.numPhone,B.numPhoneExtension,  
case when B.numPhone<>'''' then + B.numPhone +case when B.numPhoneExtension<>'''' then '' - '' + B.numPhoneExtension else '''' end  else '''' end as [Phone],
vcCompanyName, ISNULL((SELECT vcData FROM ListDetails WHERE numListID =21 AND numDomainID='+ CAST(@numDomainID AS VARCHAR) +' AND  numListItemID=D.vcProfile ) ,'''') vcProfile ,
vcEmail,
CASE BA.btDocType WHEN 2  THEN  ''Document Approval Request'' ELSE ''BizDoc Approval Request'' END as Task,
dbo.GetListIemName(bitTask) as Activity,
'''' as Status,
A.numCreatedBy,
dbo.fn_GetContactName(A.numCreatedBy) as numRecOwner,
dbo.fn_GetContactName(A.numModifiedBy) as numModifiedBy,
 ISNULL(E.numTerId,0) AS numOrgTerId, 
 (select TOP 1 vcData from ListDetails where numListItemID=E.numTerId) AS numTerId, 
case When Len( dbo.fn_GetContactName(numAssign) ) > 12                                           
  Then Substring( dbo.fn_GetContactName(numAssign) , 0  ,  12 ) + ''..''                                           
  Else dbo.fn_GetContactName(numAssign)                                           
 end                                          
 numAssignedTo ,  
 ''-'' AS numAssignedBy,        
''0'' as caseid,
''0'' as vcCasenumber,
0 as CaseTimeId,
0 as CaseExpId,
CASE BA.btDocType WHEN 2 THEN 2 ELSE 3 END as [type], 
'''' AS itemid  ,      
 '''' AS changekey ,   
dtCreatedDate as DueDate  ,cast(0 as bit) as bitFollowUpAnyTime,
dbo.GetListIemName(D.numNoOfEmployeesId) AS numNoOfEmployeesId,E.vcComPhone,
dbo.GetListIemName(E.numFollowUpStatus) AS numFollowUpStatus,' + 
CONCAT('ISNULL(dbo.FormatedDateFromDate(E.dtLastFollowUp,',@numDomainID,'),'''')') + ' AS dtLastFollowUp,' + 
CONCAT('ISNULL(dbo.FormatedDateFromDate(TempLastOrder.bintCreatedDate,',@numDomainID,'),'''')') + ' AS vcLastSalesOrderDate,ISNULL(TempPerformance.monDealAmount,0) monDealAmount, (CASE WHEN ISNULL(TempPerformance.monDealAmount,0) > 0 THEN CONCAT(FORMAT(TempPerformance.monDealAmount,''##,####.00''),'' ('',CAST(TempPerformance.fltProfitPer AS DECIMAL(18,2)),''%)'') ELSE '''' END) AS vcPerformance,'''' as vcColorScheme,'''',0,'''', 0 AS [numOppId],0 AS numBusinessProcessID '
SET @strSql2 =@strSql2 + ' ,(select TOP 1 vcData from ListDetails where numListItemID=D.numCompanyRating) AS numCompanyRating ,(select TOP 1 vcData from ListDetails where numListItemID=D.numCompanyRating) AS numCompanyRating,''-'' AS numCampaignID '+@vcnullCustomColumnName 
SET @strSql2 =@strSql2 +',(SELECT DISTINCT  
replace(replace(STUFF((SELECT ''^<br>'' 
+ CASE(ISNULL(vcCompanyName,'''')) WHEN '''' THEN '''' else ''<a href=javascript:OpenCompany('' +  cast(isnull(B.numDivisionID,0) as varchar(20)) + '','' +  cast(isnull(E.tintCRMType,0) as varchar(20)) + '',1) style=''''COLOR:black;TEXT-DECORATION:underline;''''>'' + ltrim(RTRIM(vcCompanyName)) +  ''</a> ''  END
+ CASE(isnull(B.vcFirstName,'''')) when '''' THEN ''''	else ''<a href=javascript:OpenContact('' +  cast(isnull(B.numContactId,0) as varchar(20)) + '',1) style=''''COLOR:#dd8047;''''>'' + ltrim(RTRIM(B.vcFirstName)) + ''</a> ''  END
+ CASE(isnull(B.vcLastName,'''')) when '''' THEN '''' else ''<a href=javascript:OpenContact('' +  cast(isnull(B.numContactId,0) as varchar(20)) + '',1) style=''''COLOR:#dd8047;''''>'' + ltrim(RTRIM(B.vcLastName)) + ''</a> ''  END 
+ CASE(isnull(B.numPhone,'''')) WHEN '''' then '''' ELSE B.numPhone + '' '' END
+ CASE(isnull(B.numPhoneExtension,'''')) when '''' then '''' else '' ('' + B.numPhoneExtension + '') '' END 
+ CASE(isnull(B.vcEmail,'''')) when '''' THEN '''' ELSE ''<a href=''''#'''' onclick=javascript:OpemEmail(''''../contact/frmComposeWindow.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Lsemail='' + ltrim(RTRIM(B.vcEmail)) + '''''') style=''''COLOR:#3c8dbc;''''>'' + ltrim(RTRIM(B.vcEmail)) + ''</a> ''  END 
FOR XML PATH('''')
), 1, 1, ''''),''&lt;'', ''<''), ''&gt;'', ''>'')) AS [Action-Item Participants],'''' AS HtmlLink,'''' As Location,'''' As OrignalDescription
FROM 
	BizDocAction A 
LEFT JOIN 
	dbo.BizActionDetails BA 
ON 
	BA.numBizActionId = A.numBizActionId, 
AdditionalContactsInformation B, 
CompanyInfo D,
DivisionMaster E
OUTER APPLY(' + CONCAT('SELECT MAX(bintCreatedDate) AS bintCreatedDate FROM OpportunityMaster WHERE numDomainID = ',@numDomainID,' AND numDivisionID=E.numDivisionID AND tintOppType = 1 AND tintOppStatus = 1') + ' ) AS TempLastOrder
OUTER APPLY(' + CONCAT('SELECT
																	SUM(monDealAmount) AS monDealAmount
																	,AVG(ProfitPer) AS fltProfitPer
																FROM
																(
																	SELECT
																		OM.numDivisionId
																		,monDealAmount
																		,AVG(((ISNULL(OI.monPrice,0) - (CASE D.numCost WHEN 3 THEN ISNULL(V.monCost,0) * dbo.fn_UOMConversion(I.numBaseUnit,I.numItemCode,I.numDomainID,I.numPurchaseUnit) WHEN 2 THEN ISNULL(OI.numCost,0) ELSE ISNULL(I.monAverageCost,0) END)) / ISNULL(NULLIF(OI.monPrice,0),1)) * 100) ProfitPer
																	FROM
																		OpportunityMaster OM
																	INNER JOIN
																		Domain D
																	ON
																		OM.numDomainID = D.numDomainID
																	INNER JOIN
																		OpportunityItems OI
																	ON
																		OM.numOppId = OI.numOppId
																	INNER JOIN
																		Item I
																	ON
																		OI.numItemCode = I.numItemCode
																	LEFT JOIN
																		Vendor V
																	ON
																		V.numVendorID = I.numVendorID
																		AND V.numItemCode = I.numItemCode
																	WHERE
																		OM.numDomainId = ',@numDomainID,'
																		AND OM.numDivisionId = E.numDivisionID
																		AND ISNULL(I.bitContainer,0) = 0
																		AND tintOppType = 1
																		AND tintOppStatus=1
																		AND 1 = (CASE ',@tintPerformanceFilter,' 
																				WHEN 1 THEN (CASE WHEN OM.bintCreatedDate BETWEEN DATEADD(MONTH,-3,GETUTCDATE()) AND GETUTCDATE() THEN 1 ELSE 0 END)
																				WHEN 2 THEN (CASE WHEN OM.bintCreatedDate BETWEEN DATEADD(MONTH,-6,GETUTCDATE()) AND GETUTCDATE() THEN 1 ELSE 0 END)
																				WHEN 3 THEN (CASE WHEN OM.bintCreatedDate BETWEEN DATEADD(YEAR,-1,GETUTCDATE()) AND GETUTCDATE() THEN 1 ELSE 0 END)
																				ELSE 1
																				END)
																	GROUP BY
																		OM.numDivisionId
																		,OM.numOppId
																		,monDealAmount
																) T1
																GROUP BY
																	T1.numDivisionId )') + ' AS TempPerformance
WHERE A.numContactId=b.numContactId and a.numDomainID=b.numDomainID  and 
D.numCompanyId=E.numCompanyId and 
D.numDomainID=a.numDomainID and 
E.numDivisionId=B.numDivisionId and 
a.numDomainID= ' + Cast(@numDomainID as varchar(10)) +' and a.numContactId = ' + Cast(@numUserCntID as varchar(10)) +'  and  
A.numStatus=0 
and (DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', dtCreatedDate) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', dtCreatedDate) <= '''+Cast(@endDate as varchar(30))+''')'

END


declare @Nocolumns as tinyint         
set @Nocolumns=0       
         
Select @Nocolumns=isnull(count(*),0) from View_DynamicColumns where numFormId=43 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 
  
  
if @Nocolumns > 0            
begin      
INSERT INTO #tempForm
select tintRow+1 as tintOrder,vcDbColumnName,vcFieldDataType,vcOrigDbColumnName,bitAllowFiltering,ISNULL(vcCultureFieldName,vcFieldName),                  
 vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName                                                 
,bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth
 FROM View_DynamicColumns 
 where numFormId=43 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 
  AND ISNULL(bitSettingField,0)=1 AND ISNULL(bitCustom,0)=0
  order by tintOrder asc  
  
end            
else            
begin 

	--Check if Master Form Configuration is created by administrator if NoColumns=0
	DECLARE @IsMasterConfAvailable BIT = 0
	DECLARE @numUserGroup NUMERIC(18,0)

	SELECT @numUserGroup = numGroupID FROM UserMaster WHERE numUserDetailId = @numUserCntID 

	IF (SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = @numDomainID AND numFormID = 43 AND bitGridConfiguration = 1 AND numGroupID = @numUserGroup) > 0
	BEGIN
		SET @IsMasterConfAvailable = 1
	END

	--If MasterConfiguration is available then load it otherwise load default columns
	IF @IsMasterConfAvailable = 1
	BEGIN
		INSERT INTO DycFormConfigurationDetails 
		(
			numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth
		)
		SELECT 
			43,numFieldId,intColumnNum,intRowNum,@numDomainID,@numUserCntID,NULL,1,0,intColumnWidth
		FROM 
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId=43 AND 
			View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
			ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0

		INSERT INTO #tempForm
		SELECT 
			(intRowNum + 1) as tintOrder, vcDbColumnName,vcFieldDataType,vcOrigDbColumnName,bitAllowFiltering, ISNULL(vcCultureFieldName,vcFieldName), vcAssociatedControlType,
			vcListItemType,numListID,vcLookBackTableName, bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth
		FROM 
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId=43 AND 
			View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
			ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
	ORDER BY 
		tintOrder ASC  
	END
	ELSE                                        
	BEGIN
		 INSERT INTO #tempForm
		select tintOrder,vcDbColumnName,vcFieldDataType
		,vcOrigDbColumnName,bitAllowFiltering,ISNULL(vcCultureFieldName,vcFieldName),                  
		 vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName                                                 
		,bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
		bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
		intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth
		 FROM View_DynamicDefaultColumns
		 where numFormId=43 and bitDefault=1 AND ISNULL(bitSettingField,0)=1 AND numDomainID=@numDomainID
		order by tintOrder asc  
	END 
enD                                            

DECLARE @SQLtemp varchar(max)
SET @SQLtemp= 'update #tempRecords
SET [Action-Item Participants] = SUBSTRING([Action-Item Participants], 5, LEN([Action-Item Participants]))'

exec (@strSql + @strSql1 + @strSql2 + @SQLtemp )

DECLARE @strSql3 AS NVARCHAR(MAX)



SET @strSql3=   'SELECT COUNT(*) OVER() TotalRecords, * FROM #tempRecords '


IF CHARINDEX(@columnName,@strSql) > 0 OR CHARINDEX(@columnName,@strSql1) > 0 OR CHARINDEX(@columnName,@strSql2) > 0
BEGIN
	SET @strSql3= CONCAT(@strSql3,' order by ',CASE WHEN @columnName = 'Action-Item Participants' THEN CONCAT('[','Action-Item Participants',']') ELSE @columnName END,' ',@columnSortOrder,' OFFSET ',( @CurrentPage - 1 ) * @PageSize,' ROWS FETCH NEXT ',@PageSize,' ROWS ONLY')
END
ELSE
BEGIN
	SET @strSql3= CONCAT(@strSql3,' order by EndTime Asc OFFSET ',( @CurrentPage - 1 ) * @PageSize,' ROWS FETCH NEXT ',@PageSize,' ROWS ONLY')
END

     	exec sp_executesql @strSql3
	---- EXECUTE FINAL QUERY
	--DECLARE @strFinal AS VARCHAR(MAX)
	--SET @strFinal = CONCAT('SELECT ROW_NUMBER() OVER(ORDER BY ',@columnName,' ',@columnSortOrder,') ID, ',@strColumns,' INTO #tempTable',@strSql,'; SELECT * FROM #tempTable WHERE ID > ',@firstRec,' and ID <',@lastRec,' ORDER BY ID; DROP TABLE #tempTable;')

	--PRINT @strFinal

	

drop table #tempRecords

SELECT * FROM #tempForm order by tintOrder

DROP TABLE #tempForm
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_WarehouseItems_GetByItem')
DROP PROCEDURE USP_WarehouseItems_GetByItem
GO
CREATE PROCEDURE [dbo].[USP_WarehouseItems_GetByItem]
(   
	@numDomainID NUMERIC(18,0)
	,@numItemCode NUMERIC(18,0)
	,@numWarehouseID NUMERIC(18,0)
	,@numWLocationID NUMERIC(18,0)
)
AS
BEGIN
	DECLARE @numBaseUnit AS NUMERIC(18,0)
	DECLARE @numSaleUnit AS NUMERIC(18,0)
	DECLARE @numPurchaseUnit AS NUMERIC(18,0)
	DECLARE @numSaleUOMFactor AS FLOAT
	DECLARE @numPurchaseUOMFactor AS FLOAT

	SELECT 
		@numBaseUnit = ISNULL(numBaseUnit,0),
		@numSaleUnit = ISNULL(numSaleUnit,0),
		@numPurchaseUnit = ISNULL(numPurchaseUnit,0)
	FROM 
		Item 
	WHERE 
		numItemCode=@numItemCode   

	SELECT @numSaleUOMFactor = dbo.fn_UOMConversion(@numBaseUnit,@numItemCode,@numDOmainID,@numSaleUnit)
	SELECT @numPurchaseUOMFactor = dbo.fn_UOMConversion(@numBaseUnit,@numItemCode,@numDOmainID,@numPurchaseUnit)

	SELECT
		WarehouseItems.numWareHouseItemID
		,WarehouseItems.numWarehouseID
		,WarehouseItems.numWLocationID
		,ISNULL(Warehouses.vcWareHouse,'') vcExternalLocation
		,numItemID
		,Item.numAssetChartAcntId
		,CASE 
			WHEN ISNULL(Item.bitKitParent,0)=1 then CAST(ISNULL(dbo.fn_GetKitInventoryByWarehouse(Item.numItemCode, WareHouseItems.numWareHouseID),0) AS FLOAT) 
			ELSE (CASE 
					WHEN EXISTS (SELECT WI.numWareHouseItemID FROM WareHouseItems WI INNER JOIN WarehouseLocation WL ON WI.numWLocationID=WL.numWLocationID WHERE WI.numDomainID=@numDomainID AND WI.numItemID=@numItemCode AND WI.numWareHouseID=WareHouseItems.numWareHouseID)
					THEN ISNULL((SELECT SUM(WI.numOnHand) FROM WareHouseItems WI INNER JOIN WarehouseLocation WL ON WI.numWLocationID=WL.numWLocationID WHERE WI.numDomainID=@numDomainID AND WI.numItemID=@numItemCode AND WI.numWareHouseID=WareHouseItems.numWareHouseID),0)
					ELSE ISNULL(numOnHand,0)
				END)  
		END AS [OnHand]
		,CASE WHEN ISNULL(Item.bitKitParent,0)=1 THEN 0 ELSE CAST(ISNULL(numOnOrder,0) AS FLOAT) END as [OnOrder]
		,CASE 
			WHEN ISNULL(Item.bitKitParent,0)=1 OR ISNULL(Item.bitAssembly,0)=1 
			THEN 0 
			ELSE CAST(ISNULL((SELECT 
									SUM(numUnitHour) 
								FROM 
									OpportunityItems OI 
								INNER JOIN 
									OpportunityMaster OM 
								ON 
									OI.numOppID=OM.numOppID 
								WHERE 
									OM.numDomainID=@numDomainID 
									AND tintOppType=1 
									AND tintOppStatus=0 
									AND OI.numItemCode=WareHouseItems.numItemID 
									AND OI.numWarehouseItmsID=WareHouseItems.numWareHouseItemID),0) AS FLOAT) 
		END AS [Requisitions]
		,CASE WHEN ISNULL(Item.bitKitParent,0)=1 THEN 0 ELSE CAST(ISNULL(numAllocation,0) AS FLOAT) END as [Allocation] 
		,CASE WHEN ISNULL(Item.bitKitParent,0)=1 THEN 0 ELSE CAST(ISNULL(numBackOrder,0) AS FLOAT) END as [BackOrder]
		,(CASE 
			WHEN EXISTS (SELECT WI.numWareHouseItemID FROM WareHouseItems WI INNER JOIN WarehouseLocation WL ON WI.numWLocationID=WL.numWLocationID WHERE WI.numDomainID=@numDomainID AND WI.numItemID=@numItemCode AND WI.numWareHouseID=WareHouseItems.numWareHouseID)
			THEN ISNULL((SELECT SUM(ISNULL(WI.numOnHand,0) + ISNULL(WI.numAllocation,0)) FROM WareHouseItems WI INNER JOIN WarehouseLocation WL ON WI.numWLocationID=WL.numWLocationID WHERE WI.numDomainID=@numDomainID AND WI.numItemID=@numItemCode AND WI.numWareHouseID=WareHouseItems.numWareHouseID),0)
			ELSE ISNULL(WareHouseItems.numOnHand,0) + ISNULL(WareHouseItems.numAllocation,0)
		END) AS TotalOnHand
		,ISNULL(Item.monAverageCost,0) monAverageCost
		,((ISNULL(WareHouseItems.numOnHand,0) + ISNULL(WareHouseItems.numAllocation,0)) * (CASE WHEN ISNULL(Item.bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(Item.monAverageCost,0) END)) AS monCurrentValue
		,ROUND(ISNULL(WareHouseItems.monWListPrice,0),2) Price
		,ISNULL(WareHouseItems.vcWHSKU,'') as SKU
		,ISNULL(WareHouseItems.vcBarCode,'') as BarCode
		,CASE 
			WHEN ISNULL(Item.numItemGroup,0) > 0 
			THEN dbo.fn_GetAttributes(WareHouseItems.numWareHouseItemID,0)
			ELSE ''
		END AS vcAttribute
		,dbo.fn_GetUOMName(Item.numBaseUnit) As vcBaseUnit
		,dbo.fn_GetUOMName(Item.numSaleUnit) As vcSaleUnit
		,dbo.fn_GetUOMName(Item.numPurchaseUnit) As vcPurchaseUnit
		,CAST((@numSaleUOMFactor * Case when ISNULL(Item.bitKitParent,0)=1 THEN ISNULL(dbo.fn_GetKitInventoryByWarehouse(Item.numItemCode, Warehouses.numWareHouseID),0) ELSE isnull(numOnHand,0) end) as numeric(18,2)) as OnHandUOM
		,CAST((@numPurchaseUOMFactor * Case when ISNULL(Item.bitKitParent,0)=1 THEN 0 ELSE isnull(numOnOrder,0) END) AS DECIMAL(18,2)) as OnOrderUOM
		,CAST((@numPurchaseUOMFactor * Case when ISNULL(Item.bitKitParent,0)=1 THEN 0 ELSE isnull(numReorder,0) END) AS DECIMAL(18,2)) as ReorderUOM
		,CAST((@numSaleUOMFactor * Case when ISNULL(Item.bitKitParent,0)=1 THEN 0 ELSE isnull(numAllocation,0) END) AS DECIMAL(18,2)) as AllocationUOM
		,CAST((@numSaleUOMFactor * Case when ISNULL(Item.bitKitParent,0)=1 THEN 0 ELSE isnull(numBackOrder,0) END) AS DECIMAL(18,2)) as BackOrderUOM
		,CONCAT('[',STUFF((SELECT 
								CONCAT(', {"WarehouseItemID":',WIInner.numWareHouseItemID,',"WarehouseLocationID":',WIInner.numWLocationID,',"Warehouse":"',ISNULL(W.vcWareHouse,''),'", "OnHand":',ISNULL(WIInner.numOnHand,0),', "Location":"',ISNULL(WL.vcLocation,''),'"}') 
							FROM 
								WareHouseItems WIInner
							INNER JOIN
								Warehouses W
							ON
								WIInner.numWareHouseID = W.numWareHouseID
							INNER JOIN
								WarehouseLocation WL
							ON
								WIInner.numWLocationID = WL.numWLocationID
							WHERE 
								WIInner.numDomainID=@numDomainID 
								AND WIInner.numItemID=@numItemCode
								AND WIInner.numWareHouseID=WareHouseItems.numWareHouseID
							ORDER BY
								WL.vcLocation ASC
							FOR XML PATH('')),1,1,''),']') vcInternalLocations
		,(CASE WHEN EXISTS (SELECT numWareHouseItmsDTLID FROM WareHouseItmsDTL WHERE numWareHouseItemID=WareHouseItems.numWareHouseItemID AND ISNULL(numQty,0) > 0) THEN 1 ELSE 0 END) bitSerialLotExists
	FROM
		WareHouseItems
	INNER JOIN
		Item
	ON
		WareHouseItems.numItemID = Item.numItemCode
	INNER JOIN
		Warehouses
	ON
		WareHouseItems.numWareHouseID = Warehouses.numWareHouseID
	WHERE
		WareHouseItems.numDomainID=@numDomainID
		AND WareHouseItems.numItemID=@numItemCode
		AND ISNULL(WareHouseItems.numWLocationID,0) = 0
		AND (ISNULL(@numWarehouseID,0) = 0 OR WareHouseItems.numWareHouseID=@numWarehouseID)
		AND (ISNULL(@numWLocationID,0) = 0 OR EXISTS (SELECT WI.numWareHouseItemID FROM WareHouseItems WI WHERE WI.numDomainID=@numDomainID AND WI.numItemID=@numItemCode AND WI.numWareHouseID=@numWarehouseID AND ISNULL(WI.numWLocationID,0) = @numWLocationID))
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_WarehouseItems_Save')
DROP PROCEDURE USP_WarehouseItems_Save
GO
CREATE PROCEDURE [dbo].[USP_WarehouseItems_Save]  
	@numDomainID AS NUMERIC(18,0),
	@numUserCntID AS NUMERIC(18,0),
	@numWareHouseItemID AS NUMERIC(18,0) OUTPUT,
	@numWareHouseID AS NUMERIC(18,0),
	@numWLocationID NUMERIC(18,0),
	@numItemCode NUMERIC(18,0),
	@vcLocation AS VARCHAR(250),
	@monWListPrice AS DECIMAL(20,5),
	@numOnHand AS FLOAT,
	@numReorder as FLOAT,
	@vcWHSKU AS VARCHAR(100),
	@vcBarCode AS VARCHAR(100),
	@strFieldList AS TEXT,
	@vcSerialLotNo AS TEXT,
	@bitCreateGlobalLocation BIT = 0
AS
BEGIN
BEGIN TRY
BEGIN TRANSACTION
	DECLARE @bitLotNo AS BIT = 0  
	DECLARE @bitSerialized AS BIT = 0
	DECLARE @bitMatrix AS BIT = 0
	DECLARE @numItemGroup NUMERIC(18,0)
	DECLARE @vcDescription AS VARCHAR(100)
	DECLARE @numGlobalWarehouseItemID NUMERIC(18,0) = 0
	DECLARE @monPrice AS DECIMAL(20,5)
	DECLARE @vcSKU AS VARCHAR(200) = ''
	DECLARE @vcUPC AS VARCHAR(200) = ''
	DECLARE @bitRemoveGlobalLocation BIT

	SELECT 
		@bitLotNo=ISNULL(Item.bitLotNo,0),
		@bitSerialized=ISNULL(Item.bitSerialized,0),
		@numItemGroup = ISNULL(Item.numItemGroup,0),
		@monPrice=ISNULL(Item.monListPrice,0),
		@bitMatrix = ISNULL(bitMatrix,0),
		@vcSKU = ISNULL(vcSKU,''),
		@vcUPC = ISNULL(numBarCodeId,''),
		@bitRemoveGlobalLocation = ISNULL(Domain.bitRemoveGlobalLocation,0)
	FROM 
		Item 
	INNER JOIN
		Domain
	ON
		Item.numDomainID = Domain.numDomainId
	WHERE 
		numItemCode=@numItemCode 
		AND Item.numDomainID=@numDomainID

	IF ISNULL(@numWareHouseItemID,0) = 0 
	BEGIN --INSERT
		IF ISNULL(@numWareHouseID,0) > 0 AND ISNULL(@numWLocationID,0) > 0 AND EXISTS (SELECT numWareHouseItemID FROM WareHouseItems WHERE numItemID=@numItemCode AND ISNULL(numWareHouseID,0)=@numWareHouseID AND ISNULL(numWLocationID,0) = 0 AND ISNULL(numOnHand,0) > 0)
		BEGIN
			RAISERROR('QTY_EXISTS_IN_EXTERNAL_LOCATION',16,1)
			RETURN
		END	

		INSERT INTO WareHouseItems 
		(
			numItemID, 
			numWareHouseID,
			numWLocationID,
			numOnHand,
			numReorder,
			monWListPrice,
			vcLocation,
			vcWHSKU,
			vcBarCode,
			numDomainID,
			dtModified
		)  
		VALUES
		(
			@numItemCode,
			@numWareHouseID,
			@numWLocationID,
			(CASE WHEN @bitLotNo=1 OR @bitSerialized=1 THEN 0 ELSE @numOnHand END),
			@numReorder,
			@monPrice,
			@vcLocation,
			(CASE WHEN @numItemGroup > 0 AND ISNULL(@bitMatrix,0) = 1 THEN @vcSKU ELSE @vcWHSKU END),
			(CASE WHEN @numItemGroup > 0 AND ISNULL(@bitMatrix,0) = 1 THEN @vcUPC ELSE @vcBarCode END),
			@numDomainID,
			GETDATE()
		)  

		SELECT @numWareHouseItemID = SCOPE_IDENTITY()

		-- WAREHOUSE LOCATION LEVEL DIFFERENT ITEM PRICE IS AVAILABLE FOR MATRIX ITEM ONLY
		IF @numItemGroup > 0 AND ISNULL(@bitMatrix,0) = 0
		BEGIN
			UPDATE WareHouseItems SET monWListPrice = @monWListPrice WHERE numDomainID=@numDomainID AND numItemID = @numItemCode AND numWareHouseItemID = @numWareHouseItemID
		END

		SET @vcDescription='INSERT WareHouse'
	END
	ELSE 
	BEGIN --UPDATE
		UPDATE 
			WareHouseItems  
		SET 
			numReorder=@numReorder,
			vcLocation=@vcLocation,
			vcWHSKU=(CASE WHEN @numItemGroup > 0 AND ISNULL(@bitMatrix,0) = 1 THEN @vcSKU ELSE @vcWHSKU END),
			vcBarCode=(CASE WHEN @numItemGroup > 0 AND ISNULL(@bitMatrix,0) = 1 THEN @vcUPC ELSE @vcBarCode END),
			dtModified=GETDATE()   
		WHERE 
			numItemID=@numItemCode 
			AND numDomainID=@numDomainID 
			AND numWareHouseItemID=@numWareHouseItemID

		-- WAREHOUSE LOCATION LEVEL DIFFERENT ITEM PRICE IS AVAILABLE FOR MATRIX ITEM ONLY
		IF @numItemGroup > 0 AND ISNULL(@bitMatrix,0) = 0
		BEGIN
			UPDATE WareHouseItems SET monWListPrice = @monWListPrice WHERE numDomainID=@numDomainID AND numItemID = @numItemCode AND numWareHouseItemID = @numWareHouseItemID
		END

		SET @vcDescription='UPDATE WareHouse'
	END

	-- SAVE MATRIX ITEM ATTRIBUTES
	IF DATALENGTH(@strFieldList)>2
	BEGIN
		--INSERT CUSTOM FIELDS BASE ON WAREHOUSEITEMS
		DECLARE @hDoc AS INT     
		EXEC sp_xml_preparedocument @hDoc OUTPUT, @strFieldList                                                                        

		
	    DECLARE @TempTable TABLE 
		(
			ID INT IDENTITY(1,1),
			Fld_ID NUMERIC(18,0),
			Fld_Value VARCHAR(300)
		)   
	                                      
		INSERT INTO @TempTable 
		(
			Fld_ID,
			Fld_Value
		)    
		SELECT
			*          
		FROM 
			OPENXML (@hDoc,'/NewDataSet/CusFlds',2)                                                  
		WITH 
			(Fld_ID NUMERIC(18,0),Fld_Value VARCHAR(300))                                        
	                                                                           

		IF (SELECT COUNT(*) FROM @TempTable) > 0                                        
		BEGIN                           
			DELETE 
				CFW_Fld_Values_Serialized_Items 
			FROM 
				CFW_Fld_Values_Serialized_Items C                                        
			INNER JOIN 
				@TempTable T 
			ON 
				C.Fld_ID=T.Fld_ID 
			WHERE 
				C.RecId= @numWareHouseItemID  
	      
			INSERT INTO CFW_Fld_Values_Serialized_Items
			(
				Fld_ID,
				Fld_Value,
				RecId,
				bitSerialized
			)                                                  
			SELECT 
				Fld_ID,
				ISNULL(Fld_Value,'') AS Fld_Value,
				@numWareHouseItemID,
				0 
			FROM 
				@TempTable  


			IF ISNULL(@bitMatrix,0) = 1
			BEGIN
				DELETE 
					CFW_Fld_Values_Serialized_Items 
				FROM 
					CFW_Fld_Values_Serialized_Items C                                        
				INNER JOIN 
					@TempTable T 
				ON 
					C.Fld_ID=T.Fld_ID 
				WHERE 
					C.RecId= @numGlobalWarehouseItemID  
	      
				INSERT INTO CFW_Fld_Values_Serialized_Items
				(
					Fld_ID,
					Fld_Value,
					RecId,
					bitSerialized
				)                                                  
				SELECT 
					Fld_ID,
					ISNULL(Fld_Value,'') AS Fld_Value,
					@numGlobalWarehouseItemID,
					0 
				FROM 
					@TempTable
			END
		END  

		EXEC sp_xml_removedocument @hDoc 
	END	  

	-- SAVE SERIAL/LOT# 
	IF (@bitSerialized = 1 OR @bitLotNo = 1) AND DATALENGTH(@vcSerialLotNo) > 0
	BEGIN
		DECLARE @bitDuplicateLot BIT
		EXEC  @bitDuplicateLot = USP_WareHouseItmsDTL_CheckForDuplicate @numDomainID,@numWarehouseID,@numWLocationID,@numItemCode,@bitSerialized,@vcSerialLotNo

		IF @bitDuplicateLot = 1
		BEGIN
			RAISERROR('DUPLICATE_SERIALLOT',16,1)
		END

		DECLARE @TempSerialLot TABLE
		(
			vcSerialLot VARCHAR(100),
			numQty INT,
			dtExpirationDate DATETIME
		)

		DECLARE @idoc INT
		EXEC sp_xml_preparedocument @idoc OUTPUT, @vcSerialLotNo;

		INSERT INTO
			@TempSerialLot
		SELECT
			vcSerialLot,
			numQty,
			NULLIF(dtExpirationDate, '1900-01-01 00:00:00.000')
		FROM 
			OPENXML (@idoc, '/SerialLots/SerialLot',2)
		WITH 
			(
				vcSerialLot VARCHAR(100),
				numQty INT,
				dtExpirationDate DATETIME
			);

		INSERT INTO WareHouseItmsDTL
		(
			numWareHouseItemID,
			vcSerialNo,
			numQty,
			dExpirationDate,
			bitAddedFromPO
		)  
		SELECT
			@numWarehouseItemID,
			vcSerialLot,
			numQty,
			dtExpirationDate,
			0
		FROM
			@TempSerialLot
		
		DECLARE @numTotalQty AS INT
		SELECT @numTotalQty = SUM(numQty) FROM @TempSerialLot

		UPDATE WarehouseItems SET numOnHand = ISNULL(numOnHand,0) + @numTotalQty WHERE numWareHouseItemID=@numWareHouseItemID

        SET @vcDescription = CONCAT(@vcDescription + ' (Serial/Lot# Qty Added:', @numTotalQty ,')')
	END
 
	DECLARE @dtDATE AS DATETIME = GETUTCDATE()

	EXEC dbo.USP_ManageWareHouseItems_Tracking
		@numWareHouseItemID = @numWareHouseItemID,
		@numReferenceID = @numItemCode,
		@tintRefType = 1,
		@vcDescription = @vcDescription,
		@numModifiedBy = @numUserCntID,
		@ClientTimeZoneOffset = 0,
		@dtRecordDate = @dtDATE,
		@numDomainID = @numDomainID 

	IF ISNULL(@numGlobalWarehouseItemID,0) > 0
	BEGIN        
		EXEC dbo.USP_ManageWareHouseItems_Tracking
		@numWareHouseItemID = @numGlobalWarehouseItemID,
		@numReferenceID = @numItemCode,
		@tintRefType = 1,
		@vcDescription = @vcDescription,
		@numModifiedBy = @numUserCntID,
		@ClientTimeZoneOffset = 0,
		@dtRecordDate = @dtDATE,
		@numDomainID = @numDomainID 
	END

	UPDATE Item SET bintModifiedDate=@dtDATE,numModifiedBy=@numUserCntID WHERE numItemCode=@numItemCode AND numDomainID=@numDomainID
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SET @numWareHouseItemID = 0

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
END

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO

IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_WorkOrder_PutAway')
DROP PROCEDURE USP_WorkOrder_PutAway
GO
CREATE PROCEDURE [dbo].[USP_WorkOrder_PutAway]
(
	@numDomainID NUMERIC(18,0)
	,@numUserCntID NUMERIC(18,0)
	,@numWOID NUMERIC(18,0)
	,@numQtyReceived FLOAT
	,@dtItemReceivedDate DATETIME
	,@vcWarehouses VARCHAR(MAX)
	,@tintMode TINYINT
)
AS
BEGIN
	IF ISNULL(@numQtyReceived,0) <= 0 
	BEGIN
		RAISERROR('QTY_TO_RECEIVE_NOT_PROVIDED',16,1)
		RETURN
	END 

	IF LEN(ISNULL(@vcWarehouses,'')) = 0
	BEGIN
		RAISERROR('LOCATION_NOT_SELECTED',16,1)
		RETURN
	END

	DECLARE @TEMPWarehouseLot TABLE
	(
		ID INT
		,numWareHouseItmsDTLID NUMERIC(18,0)
		,vcSerialNo VARCHAR(300)
		,numQty FLOAT
	)

	DECLARE @TEMPSerialLot TABLE
	(
		ID INT
		,vcSerialNo VARCHAR(300)
		,numQty FLOAT
	)

	DECLARE @TEMPWarehouseLocations TABLE
	(
		ID INT IDENTITY(1,1)
		,numWarehouseLocationID NUMERIC(18,0)
		,numWarehouseItemID NUMERIC(18,0)
		,numReceivedQty FLOAT
		,vcSerialLotNo VARCHAR(MAX)
	)

	DECLARE @hDocItem int
	EXEC sp_xml_preparedocument @hDocItem OUTPUT, @vcWarehouses

	INSERT INTO @TEMPWarehouseLocations
	(
		numWarehouseLocationID
		,numWarehouseItemID
		,numReceivedQty
		,vcSerialLotNo
	)
	SELECT 
		(CASE WHEN ISNULL(bitNewLocation,0) = 1 THEN ID ELSE 0 END)
		,(CASE WHEN ISNULL(bitNewLocation,0) = 1 THEN 0 ELSE ID END)
		,ISNULL(Qty,0)
		,ISNULL(SerialLotNo,'') 
	FROM 
		OPENXML (@hDocItem, '/NewDataSet/Table1',2)
	WITH 
	(
		bitNewLocation BIT, 
		ID NUMERIC(18,0),
		Qty FLOAT,
		SerialLotNo VARCHAR(MAX)
	)

	EXEC sp_xml_removedocument @hDocItem

	IF @numQtyReceived <> ISNULL((SELECT SUM(numReceivedQty) FROM @TEMPWarehouseLocations),0)
	BEGIN
		RAISERROR('INALID_QTY_RECEIVED',16,1)
		RETURN
	END

	DECLARE @numItemCode NUMERIC(18,0)
	DECLARE @monListPrice DECIMAL(20,5)
	DECLARE @numWarehouseID NUMERIC(18,0)
	DECLARE @numWarehouseItemID NUMERIC(18,0)
	DECLARE @numOverheadServiceItemID NUMERIC(18,0)
	DECLARE @monAverageCost AS DECIMAL(20,5)
	DECLARE @newAverageCost AS DECIMAL(20,5)
	DECLARE @monOverheadCost DECIMAL(20,5) 
	DECLARE @monLabourCost DECIMAL(20,5)
	DECLARE @CurrentAverageCost DECIMAL(20,5)
	DECLARE @TotalCurrentOnHand FLOAT
	DECLARE @bitLot BIT
	DECLARE @bitSerial BIT
	DECLARE @numOldQtyReceived FLOAT
	DECLARE @numUnits FLOAT
	DECLARE @description VARCHAR(300)

	IF EXISTS (SELECT numWOId FROM WorkOrder WHERE numDomainID=@numDomainID AND numWOId=@numWOID)
	BEGIN
		SELECT 
			@numItemCode=Item.numItemCode
			,@monListPrice=ISNULL(Item.monListPrice,0)
			,@numWarehouseID=WareHouseItems.numWareHouseID
			,@numWarehouseItemID=WareHouseItems.numWareHouseItemID
			,@bitLot=ISNULL(bitLotNo,0)
			,@bitSerial=ISNULL(bitSerialized,0)
			,@numUnits=ISNULL(numQtyItemsReq,0)
			,@numOldQtyReceived=ISNULL(numUnitHourReceived,0)
		FROM 
			WorkOrder 
		INNER JOIN
			Item
		ON
			WorkOrder.numItemCode = Item.numItemCode
		INNER JOIN
			WareHouseItems
		ON
			WareHouseItems.numWareHouseItemID = WorkOrder.numWareHouseItemId
		WHERE 
			WorkOrder.numDomainID=@numDomainID 
			AND WorkOrder.numWOId=@numWOID

		IF (@numQtyReceived + @numOldQtyReceived) > @numUnits
		BEGIN
			RAISERROR('RECEIVED_QTY_MUST_BE_LESS_THEN_OR_EQUAL_TO_ORDERED_QTY',16,1)
			RETURN
		END

		SELECT @numOverheadServiceItemID=ISNULL(numOverheadServiceItemID,0) FROM Domain WHERE numDomainId=@numDomainID
	
		SELECT @CurrentAverageCost = (CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE monAverageCost END) FROM dbo.Item WHERE numItemCode =@numItemCode 
		SELECT @TotalCurrentOnHand = ISNULL((SUM(numOnHand) + SUM(numAllocation)),0) FROM dbo.WareHouseItems WHERE numItemID =@numItemCode 
		PRINT @CurrentAverageCost
		PRINT @TotalCurrentOnHand

		-- Store hourly rate for all task assignee
		UPDATE 
			SPDT
		SET
			SPDT.monHourlyRate = ISNULL(UM.monHourlyRate,0)
		FROM
			StagePercentageDetailsTask SPDT
		INNER JOIN
			UserMaster UM
		ON
			SPDT.numAssignTo = UM.numUserDetailId
		WHERE
			SPDT.numDomainID = @numDomainID
			AND SPDT.numWorkOrderId = @numWOID

		SELECT 
			@newAverageCost = SUM(numQtyItemsReq * ISNULL(I.monAverageCost,0))
		FROM 
			WorkOrderDetails WOD
		LEFT JOIN 
			dbo.Item I 
		ON 
			I.numItemCode = WOD.numChildItemID
		WHERE 
			WOD.numWOId = @numWOId
			AND I.charItemType = 'P'

		IF @numOverheadServiceItemID > 0 AND EXISTS (SELECT numWODetailId FROM WorkOrderDetails WHERE numWOId=@numWOId AND numChildItemID=@numOverheadServiceItemID)
		BEGIN
			SET @monOverheadCost = (SELECT SUM(numQtyItemsReq * ISNULL(Item.monListPrice,0)) FROM WorkOrderDetails WOD INNER JOIN Item ON WOD.numChildItemID=Item.numItemCode WHERE WOD.numWOId=@numWOId AND WOD.numChildItemID=@numOverheadServiceItemID)
		END

		SET @monLabourCost = dbo.GetWorkOrderLabourCost(@numDomainID,@numWOId)			

		UPDATE WorkOrder SET monAverageCost=@newAverageCost,monLabourCost=ISNULL(@monLabourCost,0),monOverheadCost=ISNULL(@monOverheadCost,0) WHERE numWOId=@numWOId

		SET @newAverageCost = ((@CurrentAverageCost * @TotalCurrentOnHand)  + (@newAverageCost + ISNULL(@monOverheadCost,0) + ISNULL(@monLabourCost,0))) / (@TotalCurrentOnHand + @numQtyReceived)

		UPDATE
			WareHouseItems
		SET 
			numOnOrder = ISNULL(numOnOrder,0) - @numQtyReceived,
			dtModified = GETDATE()
		WHERE
			numWareHouseItemID=@numWareHouseItemID
		
		SET @description = CONCAT('Work Order Received (Qty:',@numQtyReceived,')')

		EXEC dbo.USP_ManageWareHouseItems_Tracking
			@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
			@numReferenceID = @numWOID, --  numeric(9, 0)
			@tintRefType = 2, --  tinyint
			@vcDescription = @description,
			@numModifiedBy = @numUserCntID,
			@dtRecordDate =  @dtItemReceivedDate,
			@numDomainID = @numDomainID

		DECLARE @i INT
		DECLARE @j INT
		DECLARE @k INT
		DECLARE @iCount INT = 0
		DECLARE @jCount INT = 0
		DECLARE @kCount INT = 0
		DECLARE @vcSerialLot# VARCHAR(MAX)
		DECLARE @numWareHouseItmsDTLID NUMERIC(18,0)
		DECLARE @vcSerailLotNumber VARCHAR(300)
		DECLARE @numSerialLotQty FLOAT
		DECLARE @numTempWLocationID NUMERIC(18,0)
		DECLARE @numTempWarehouseItemID NUMERIC(18,0)
		DECLARE @numTempReceivedQty FLOAT
		DECLARE @numLotWareHouseItmsDTLID NUMERIC(18,0)
		DECLARE @numLotQty FLOAT
	
		SET @i = 1
		SET @iCount = (SELECT COUNT(*) FROM @TEMPWarehouseLocations)

		WHILE @i <= @iCount
		BEGIN
			SELECT 
				@numTempWLocationID=ISNULL(numWarehouseLocationID,0)
				,@numTempWarehouseItemID=ISNULL(numWarehouseItemID,0)
				,@numTempReceivedQty=ISNULL(numReceivedQty,0)
				,@vcSerialLot# = ISNULL(vcSerialLotNo,'')
			FROM 
				@TEMPWarehouseLocations 
			WHERE 
				ID = @i

			IF (ISNULL(@bitLot,0)=1 OR ISNULL(@bitSerial,0)=1)
			BEGIN
				IF LEN(ISNULL(@vcSerialLot#,'')) = 0
				BEGIN
					RAISERROR('SERIALLOT_REQUIRED',16,1)
					RETURN
				END 
				ELSE
				BEGIN	
					DELETE FROM @TEMPSerialLot

					IF ISNULL(@bitSerial,0) = 1
					BEGIN
						INSERT INTO @TEMPSerialLot
						(
							ID
							,vcSerialNo
							,numQty
						)
						SELECT
							ROW_NUMBER() OVER(ORDER BY OutParam)
							,RTRIM(LTRIM(OutParam))
							,1
						FROM
							dbo.SplitString(@vcSerialLot#,',')
					END
					ELSE IF @bitLot = 1
					BEGIN
						INSERT INTO @TEMPSerialLot
						(
							ID
							,vcSerialNo
							,numQty
						)
						SELECT
							ROW_NUMBER() OVER(ORDER BY OutParam)
							,(CASE 
								WHEN PATINDEX('%(%', OutParam) > 0 AND PATINDEX('%)%', OutParam) > 0 AND PATINDEX('%)%',OutParam) > (PATINDEX('%(%',OutParam) + 1)
								THEN RTRIM(LTRIM(SUBSTRING(OutParam,0,PATINDEX('%(%', OutParam))))
								ELSE ''
							END)
							,(CASE 
								WHEN PATINDEX('%(%', OutParam) > 0 AND PATINDEX('%)%', OutParam) > 0 AND PATINDEX('%)%',OutParam) > (PATINDEX('%(%',OutParam) + 1)
								THEN (CASE WHEN ISNUMERIC(SUBSTRING(OutParam,(PATINDEX('%(%', OutParam) + 1),(PATINDEX('%)%', OutParam) - PATINDEX('%(%', OutParam) - 1))) = 1 
										THEN CAST(SUBSTRING(OutParam,(PATINDEX('%(%', OutParam) + 1),(PATINDEX('%)%', OutParam) - PATINDEX('%(%', OutParam) - 1)) AS FLOAT)  
										ELSE -1 
										END) 
								ELSE -1
							END)	
	
						FROM
							dbo.SplitString(@vcSerialLot#,',')
					END

					IF EXISTS (SELECT vcSerialNo FROM @TEMPSerialLot WHERE numQty = -1)
					BEGIN
						RAISERROR('INVALID_SERIAL_NO',16,1)
						RETURN
					END
					ELSE IF @numTempReceivedQty <> ISNULL((SELECT SUM(numQty) FROM @TEMPSerialLot),0)
					BEGIN
						RAISERROR('INALID_SERIALLOT',16,1)
						RETURN
					END
				END
			END

			IF ISNULL(@numTempWarehouseItemID,0) = 0
			BEGIN
				IF EXISTS (SELECT numWLocationID FROM WarehouseLocation WHERE numDomainID=@numDomainID AND numWLocationID=ISNULL(@numTempWLocationID,0))
				BEGIN
					IF EXISTS (SELECT numWareHouseItemID FROM WareHouseItems WHERE numDomainID=@numDomainID AND numItemID=@numItemCode AND numWareHouseID=@numWarehouseID AND numWLocationID=@numTempWLocationID)
					BEGIN
						SET @numTempWarehouseItemID = (SELECT TOP 1 numWareHouseItemID FROM WareHouseItems WHERE numDomainID=@numDomainID AND numItemID=@numItemCode AND numWareHouseID=@numWarehouseID AND numWLocationID=@numTempWLocationID)
					END
					ELSE 
					BEGIN
						INSERT INTO WareHouseItems 
						(
							numItemID, 
							numWareHouseID,
							numWLocationID,
							numOnHand,
							numAllocation,
							numOnOrder,
							numBackOrder,
							numReorder,
							monWListPrice,
							vcLocation,
							vcWHSKU,
							vcBarCode,
							numDomainID,
							dtModified
						)  
						VALUES
						(
							@numItemCode,
							@numWareHouseID,
							@numTempWLocationID,
							0,
							0,
							0,
							0,
							0,
							@monListPrice,
							ISNULL((SELECT vcLocation FROM WarehouseLocation WHERE numDomainID=@numDomainID AND numWLocationID=ISNULL(@numTempWLocationID,0)),''),
							'',
							'',
							@numDomainID,
							GETDATE()
						)  

						SELECT @numTempWarehouseItemID = SCOPE_IDENTITY()

						DECLARE @dtDATE AS DATETIME = GETUTCDATE()
						DECLARE @vcDescription VARCHAR(100)='INSERT WareHouse'

						EXEC dbo.USP_ManageWareHouseItems_Tracking
							@numWareHouseItemID = @numTempWarehouseItemID,
							@numReferenceID = @numItemCode,
							@tintRefType = 1,
							@vcDescription = @vcDescription,
							@numModifiedBy = @numUserCntID,
							@ClientTimeZoneOffset = 0,
							@dtRecordDate = @dtDATE,
							@numDomainID = @numDomainID 
					END
				END
				ELSE 
				BEGIN
					RAISERROR('INVALID_WAREHOUSE_LOCATION',16,1)
					RETURN
				END
			END

			IF NOT EXISTS (SELECT numWareHouseItemID FROM WareHouseItems WHERE numDomainID=@numDomainID AND numItemID=@numItemCode AND numWareHouseItemID=@numTempWarehouseItemID)
			BEGIN
				RAISERROR('INVALID_WAREHOUSE',16,1)
				RETURN
			END
			ELSE
			BEGIN
				-- INCREASE THE OnHand Of Destination Location
				UPDATE
					WareHouseItems
				SET
					numBackOrder = (CASE WHEN numBackOrder > @numTempReceivedQty THEN ISNULL(numBackOrder,0) - @numTempReceivedQty ELSE 0 END),         
					numAllocation = (CASE WHEN numBackOrder > @numTempReceivedQty THEN ISNULL(numAllocation,0) + @numTempReceivedQty ELSE ISNULL(numAllocation,0) + ISNULL(numBackOrder,0) END),
					numOnHand = (CASE WHEN numBackOrder > @numTempReceivedQty THEN numOnHand ELSE ISNULL(numOnHand,0) + (@numTempReceivedQty - ISNULL(numBackOrder,0)) END),
					dtModified = GETDATE()
				WHERE
					numWareHouseItemID=@numTempWarehouseItemID

				SET @description = CONCAT('Work Order Qty Put-Away (Qty:',@numTempReceivedQty,')')

				EXEC dbo.USP_ManageWareHouseItems_Tracking
					@numWareHouseItemID = @numTempWarehouseItemID, --  numeric(9, 0)
					@numReferenceID = @numWOID, --  numeric(9, 0)
					@tintRefType = 2, --  tinyint
					@vcDescription = @description,
					@numModifiedBy = @numUserCntID,
					@dtRecordDate =  @dtItemReceivedDate,
					@numDomainID = @numDomainID

				IF (ISNULL(@bitLot,0)=1 OR ISNULL(@bitSerial,0)=1)
				BEGIN
					SET @j = 1
					SET @jCount = (SELECT COUNT(*) FROM @TEMPSerialLot)

					WHILE (@j <= @jCount AND @numTempReceivedQty > 0)
					BEGIN
						SELECT 
							@vcSerailLotNumber=ISNULL(vcSerialNo,'')
							,@numSerialLotQty=ISNULL(numQty,0)
						FROM 
							@TEMPSerialLot 
						WHERE 
							ID=@j

						IF @numSerialLotQty > 0
						BEGIN
							IF @bitSerial = 1 AND (SELECT COUNT(*) FROM WareHouseItems INNER JOIN WareHouseItmsDTL ON WareHouseItems.numWareHouseItemID = WareHouseItmsDTL.numWareHouseItemID WHERE WareHouseItems.numItemID=@numItemCode AND LOWER(vcSerialNo)=LOWER(@vcSerailLotNumber)) > 0
							BEGIN
								RAISERROR('DUPLICATE_SERIAL_NO',16,1)
								RETURN
							END

							INSERT INTO WareHouseItmsDTL
							(
								numWareHouseItemID
								,vcSerialNo
								,numQty
								,bitAddedFromPO
							)  
							VALUES
							( 
								@numTempWarehouseItemID
								,@vcSerailLotNumber
								,(CASE WHEN @numTempReceivedQty >= @numSerialLotQty THEN @numSerialLotQty ELSE @numTempReceivedQty END)
								,1 
							)

							SET @numWareHouseItmsDTLID=SCOPE_IDENTITY()
		
							INSERT INTO OppWarehouseSerializedItem
							(
								numWarehouseItmsDTLID
								,numWOID
								,numWarehouseItmsID
								,numQty
							)  
							VALUES
							( 
								@numWareHouseItmsDTLID
								,@numWOID
								,@numTempWarehouseItemID
								,(CASE WHEN @numTempReceivedQty >= @numSerialLotQty THEN @numSerialLotQty ELSE @numTempReceivedQty END)
							)

							IF @numTempReceivedQty >= @numSerialLotQty
							BEGIN
								SET @numTempReceivedQty = @numTempReceivedQty - @numSerialLotQty
								UPDATE @TEMPSerialLot SET numQty = 0  WHERE ID = @j
							END
							ELSE
							BEGIN
								UPDATE @TEMPSerialLot SET numQty = numQty - @numTempReceivedQty  WHERE ID = @j
								SET @numTempReceivedQty = 0
							END
						END

						SET @j = @j + 1
					END

					IF @numTempReceivedQty > 0
					BEGIN
						RAISERROR('INALID_SERIALLOT',16,1)
						RETURN
					END
				END
			END


			SET @i = @i + 1
		END

		UPDATE  
			WorkOrder
		SET 
			numUnitHourReceived = ISNULL(numUnitHourReceived,0) + ISNULL(@numQtyReceived,0)
			,numQtyReceived = (CASE WHEN ISNULL(@tintMode,0)=1 THEN ISNULL(numQtyReceived,0) + ISNULL(@numQtyReceived,0) ELSE ISNULL(numQtyReceived,0) END)
		WHERE
			numDomainID = @numDomainID
			AND numWOID = @numWOID
	END
	ELSE
	BEGIN
		RAISERROR('WORKORDER_DOES_NOT_EXISTS',16,1)
	END

	
END
GO
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DycFieldMasterSynonym_GetByFieldID')
DROP PROCEDURE dbo.USP_DycFieldMasterSynonym_GetByFieldID
GO
CREATE PROCEDURE [dbo].[USP_DycFieldMasterSynonym_GetByFieldID]
(
	@numDomainID NUMERIC(18,0)
	,@numFieldID NUMERIC(18,0)
	,@bitCustomField BIT
)
AS 
BEGIN
	SELECT
		ID
		,numFieldID
		,vcSynonym
		,bitDefault
	FROM
		DycFieldMasterSynonym
	WHERE
		(numDomainID=@numDomainID OR bitDefault=1)
		AND numFieldID=@numFieldID
		AND ISNULL(bitCustomField,0) = @bitCustomField
	ORDER BY
		vcSynonym
END
GO
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DycFieldMasterSynonym_Save')
DROP PROCEDURE dbo.USP_DycFieldMasterSynonym_Save
GO
CREATE PROCEDURE [dbo].[USP_DycFieldMasterSynonym_Save]
(
	@numDomainID NUMERIC(18,0)
	,@numFieldID NUMERIC(18,0)
	,@vcSynonym VARCHAR(200)
	,@bitCustomField BIT
)
AS 
BEGIN
	INSERT INTO DycFieldMasterSynonym
	(
		numDomainID
		,numFieldID
		,bitCustomField
		,vcSynonym
		,bitDefault
	)
	VALUES
	(
		@numDomainID
		,@numFieldID
		,@bitCustomField
		,@vcSynonym
		,0
	)
END
GO
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DycFieldMasterSynonym_Delete')
DROP PROCEDURE dbo.USP_DycFieldMasterSynonym_Delete
GO
CREATE PROCEDURE [dbo].[USP_DycFieldMasterSynonym_Delete]
(
	@numDomainID NUMERIC(18,0)
	,@vcIDs VARCHAR(MAX)
)
AS 
BEGIN
	DELETE FROM DycFieldMasterSynonym WHERE numDomainID=@numDomainID AND ID IN (SELECT Id FROM dbo.SplitIDs(@vcIDs,',')) AND ISNULL(bitDefault,0) = 0
END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GET_IMPORT_MAPPED_FIELDS_DATA')
DROP PROCEDURE USP_GET_IMPORT_MAPPED_FIELDS_DATA
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[USP_GET_IMPORT_MAPPED_FIELDS_DATA] 
(
	@numDomainID NUMERIC(18,0),
	@numFormID NUMERIC(18,0),
	@vcFields VARCHAR(MAX)
)
AS 
BEGIN
	DECLARE @TempFields TABLE
	(
		numFieldID NUMERIC(18,0)
		,intMapColumnNo INT
		,bitCustom BIT
	)

	DECLARE @hDocItem int
	EXEC sp_xml_preparedocument @hDocItem OUTPUT, @vcFields

	INSERT INTO @TempFields
	(
		numFieldID
		,intMapColumnNo
		,bitCustom
	)
	SELECT
		FormFieldID
		,ImportFieldID
		,IsCustomField
	FROM
		OPENXML (@hDocItem,'/NewDataSet/Table1',2)
	WITH
	(
		FormFieldID NUMERIC(18,0)
		,ImportFieldID NUMERIC(18,0)
		,IsCustomField BIT
	)
	
	EXEC sp_xml_removedocument @hDocItem


	SELECT 
		* 
	FROM 
	(
		SELECT DISTINCT 
			PARENT.numFieldID AS [FormFieldID]
			,IFFM.intMapColumnNo AS [ImportFieldID]
			,PARENT.vcDbColumnName AS [dbFieldName]
			,PARENT.vcFieldName AS [FormFieldName]
			,ISNULL(PARENT.vcAssociatedControlType,'') vcAssociatedControlType
			,ISNULL(PARENT.bitDefault,0) bitDefault
			,ISNULL(PARENT.vcFieldType,'') vcFieldType
			,ISNULL(PARENT.vcListItemType,'') vcListItemType
			,ISNULL(PARENT.vcLookBackTableName,'') vcLookBackTableName
			,PARENT.vcOrigDbColumnName AS [vcOrigDbColumnName]
			,ISNULL(PARENT.vcPropertyName,'') vcPropertyName
			,0 AS [bitCustomField],
			PARENT.numDomainID AS [DomainID]
			,PARENT.vcFieldDataType
			,PARENT.intFieldMaxLength
		FROM 
			dbo.View_DynamicDefaultColumns PARENT  
		INNER JOIN 
			@TempFields IFFM 
		ON 
			PARENT.numFieldID = IFFM.numFieldID
			AND ISNULL(IFFM.bitCustom,0) = 0    		
		WHERE 
			PARENT.numDomainID = @numDomainID
			AND numFormID = @numFormID
		UNION ALL		
		SELECT DISTINCT 
			IFFM.numFieldID AS [FormFieldID]
			,IFFM.intMapColumnNo AS [ImportFieldID]
			,CFW.Fld_label AS [dbFieldName]
			,CFW.Fld_label AS [FormFieldName]
			,CASE WHEN CFW.fld_Type = 'Drop Down List Box' THEN 'SelectBox' ELSE CFW.fld_Type END vcAssociatedControlType
			,0 AS bitDefault
			,'R' AS vcFieldType
			,'' AS vcListItemType
			,'' AS  vcLookBackTableName
			,CFW.Fld_label AS [vcOrigDbColumnName]
			,'' AS vcPropertyName
			,1 AS [bitCustomField]
			,0
			,'' vcFieldDataType
			,0 intFieldMaxLength
		FROM @TempFields IFFM 
		INNER JOIN dbo.CFW_Fld_Master CFW ON IFFM.numFieldID = CFW.Fld_id AND ISNULL(bitCustom,0) = 1
		INNER JOIN dbo.CFW_Loc_Master LOC ON CFW.Grp_id = LOC.Loc_id										 
		INNER JOIN dbo.DycFormSectionDetail DFSD ON DFSD.Loc_id = LOC.Loc_id
		WHERE (DFSD.numFormID = @numFormID OR DFSD.numFormID = (CASE WHEN @numFormID = 20 THEN 48 ELSE @numFormID END))
		  AND Grp_id = DFSD.Loc_Id 
	) TABLE1
	ORDER BY 
		ImportFieldID DESC 


END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_TicklerActItemsV2')
DROP PROCEDURE USP_TicklerActItemsV2
GO
CREATE Proc [dbo].[USP_TicklerActItemsV2]       
@numUserCntID as numeric(9)=null,                                                                        
@numDomainID as numeric(9)=null,                                                                        
@startDate as datetime,                                                                        
@endDate as datetime,                                                                    
@ClientTimeZoneOffset Int,  --Added by Debasish to enable calculation of date according to client machine             
@bitFilterRecord bit=0,
@columnName varchar(50), 
@columnSortOrder varchar(50),
@vcBProcessValue varchar(500),
@RegularSearchCriteria VARCHAR(MAX)='',
@CustomSearchCriteria VARCHAR(MAX)='', 
@OppStatus int,
@CurrentPage int,                                                              
@PageSize int
AS
BEGIN
--SET @PageSize=10
DECLARE @tintActionItemsViewRights TINYINT  = 3
DECLARE @bitREQPOApproval AS BIT
DECLARE @bitARInvoiceDue AS BIT
DECLARE @bitAPBillsDue AS BIT
DECLARE @vchARInvoiceDue AS VARCHAR(500)
DECLARE @vchAPBillsDue AS VARCHAR(500)
DECLARE @decReqPOMinValue AS DECIMAL(18,2)
DECLARE @vchREQPOApprovalEmp AS VARCHAR(500)=''
DECLARE @numDomainDivisionID NUMERIC(18,0)
DECLARE @dynamicPartQuery AS NVARCHAR(MAX)=''
DECLARE @ActivityRegularSearch AS NVARCHAR(MAX)=''
DECLARE @OtherRegularSearch AS NVARCHAR(MAX)=''
DECLARE @IndivRecordPaging AS NVARCHAR(MAX)=''
--SET @IndivRecordPaging=' '
DECLARE @TotalRecordCount INT =0
SET @IndivRecordPaging=  ' ORDER BY DueDate OFFSET '+CAST(((@CurrentPage - 1 ) * 10) AS VARCHAR) +' ROWS FETCH NEXT '+CAST(10 AS VARCHAR)+' ROWS ONLY'

SELECT TOP 1 
	@bitREQPOApproval=bitREQPOApproval,
	@vchREQPOApprovalEmp=vchREQPOApprovalEmp,
	@decReqPOMinValue=ISNULL(decReqPOMinValue,0),
	@bitARInvoiceDue=bitARInvoiceDue,
	@bitAPBillsDue = bitAPBillsDue,
	@vchARInvoiceDue = vchARInvoiceDue,
	@vchAPBillsDue = vchAPBillsDue,
	@numDomainDivisionID=numDivisionID
FROM 
	Domain 
WHERE 
	numDomainId=@numDomainID

IF EXISTS (SELECT * FROM GroupAuthorization WHERE numDomainID=@numDomainID AND numModuleID=1 AND numPageID=2 AND numGroupID IN (SELECT ISNULL(numGroupID,0) FROM UserMaster WHERE numDomainID=@numDomainID AND numUserDetailId=@numUserCntID))
BEGIN
	SELECT @tintActionItemsViewRights=ISNULL(intViewAllowed,0) FROM GroupAuthorization WHERE numDomainID=@numDomainID AND numModuleID=1 AND numPageID=2 AND numGroupID IN (SELECT ISNULL(numGroupID,0) FROM UserMaster WHERE numDomainID=@numDomainID AND numUserDetailId=@numUserCntID)
END
--12 - Communication task
--1 - Case Task
--2 - Project Task
--3 - Opportunity Task
--4 - Sales Order Task
--5 - Work Order Management
--6 - Work Center Task
--7 - Requisition Approval
--8 - PO Approval
--9 - Price Margin Approval
--10 - A/R Invoice Due
--11 - A/P Bill Due

DECLARE @dynamicQuery AS NVARCHAR(MAX)=''
DECLARE @dynamicQueryCount AS NVARCHAR(MAX)=''

Create table #tempRecords (RecordCount NUMERIC(9),RecordId NUMERIC(9),numTaskId numeric(9),IsTaskClosed bit,TaskTypeName VARCHAR(100),TaskType NUMERIC(9),
OrigDescription NVARCHAR(MAX),Description NVARCHAR(MAX),TotalProgress INT,numAssignToId NUMERIC(9),
Priority NVARCHAR(MAX),PriorityId  NUMERIC(9),ActivityId NUMERIC(9),OrgName NVARCHAR(MAX),CompanyRating NVARCHAR(MAX),
numContactID numeric(9),numDivisionID numeric(9),tintCRMType tinyint,
Id numeric(9),bitTask varchar(100),Startdate datetime,EndTime datetime,
itemDesc text,[Name] varchar(200),vcFirstName varchar(100),vcLastName varchar(100),numPhone varchar(15),numPhoneExtension varchar(7),
[Phone] varchar(30),vcCompanyName varchar(100),vcProfile VARCHAR(100), vcEmail varchar(50),Task varchar(100),Activity varchar(MAX),
Status varchar(100),numCreatedBy numeric(9),numRecOwner VARCHAR(500), numModifiedBy VARCHAR(500), numOrgTerId NUMERIC(18,0), numTerId VARCHAR(200),numAssignedTo varchar(1000),numAssignedBy varchar(50),
 caseid numeric(9),vcCasenumber varchar(50),casetimeId numeric(9),caseExpId numeric(9),type int,itemid varchar(50),
changekey varchar(50),DueDate varchar(50),bitFollowUpAnyTime bit,numNoOfEmployeesId varchar(50),vcComPhone varchar(50),numFollowUpStatus varchar(200),dtLastFollowUp varchar(500),vcLastSalesOrderDate VARCHAR(100),monDealAmount DECIMAL(20,5), vcPerformance VARCHAR(100),vcColorScheme VARCHAR(50),
Slp_Name varchar(100),intTotalProgress INT,vcPoppName varchar(100), numOppId NUMERIC(18,0),numBusinessProcessID numeric(18,0),
numCompanyRating varchar(500),numStatusID varchar(500),numCampaignID varchar(500),[Action-Item Participants] NVARCHAR(MAX),HtmlLink varchar(500),
Location nvarchar(150),OrignalDescription text)                                                         
 

declare @strSql as varchar(MAX)                                                            
declare @strSql1 as varchar(MAX)                         
declare @strSql2 as varchar(MAX)                                                            
declare @vcCustomColumnName AS VARCHAR(8000)
declare @vcnullCustomColumnName AS VARCHAR(8000)
   -------Change Row Color-------
Declare @vcCSOrigDbCOlumnName as varchar(50),@vcCSLookBackTableName as varchar(50),@vcCSAssociatedControlType as varchar(50)
SET @vcCSOrigDbCOlumnName=''
SET @vcCSLookBackTableName=''

declare @tintOrder as tinyint                                                
declare @vcFieldName as varchar(50)                                                
declare @vcListItemType as varchar(3)                                           
declare @vcAssociatedControlType varchar(20)                                                
declare @numListID AS numeric(9)                                                
declare @vcDbColumnName varchar(20)                    
declare @WhereCondition varchar(2000)                     
declare @vcLookBackTableName varchar(2000)              
Declare @bitCustomField as BIT
DECLARE @bitAllowEdit AS CHAR(1)                 
Declare @numFieldId as numeric  
DECLARE @bitAllowSorting AS CHAR(1)   
declare @vcColumnName AS VARCHAR(500)       
declare @vcCustomColumnNameOnly AS VARCHAR(500)       


IF(@RegularSearchCriteria='')
BEGIN
	SET @RegularSearchCriteria =@RegularSearchCriteria + ' 1=1'
END
if(@OppStatus=0)
BEGIN
SET @RegularSearchCriteria =@RegularSearchCriteria + ' AND IsTaskClosed = 0'
END
if(@OppStatus=1)
BEGIN
SET @RegularSearchCriteria = @RegularSearchCriteria + ' AND IsTaskClosed = 1'
END
 SET @RegularSearchCriteria  = REPLACE(@RegularSearchCriteria,'TaskTypeName','TaskType')

 SET @ActivityRegularSearch = ''
 SET @OtherRegularSearch = ''

 SET @ActivityRegularSearch = ISNULL((SELECT STUFF( 
(
	SELECT  'AND ' + [Items] FROM 
	(
	  SELECT [Items] FROM SplitByString(@RegularSearchCriteria,'AND') WHERE Items<>'' AND 1=
	  (CASE WHEN Items LIKE '%IsTaskClosed%' THEN 0
	 WHEN Items LIKE '%TaskType%'  THEN 0
	 WHEN  Items LIKE '%numAssignToId%'  THEN 0
	 WHEN  Items LIKE '%PriorityId%'  THEN 0
	 WHEN  Items LIKE '%ActivityId%'  THEN 0
	 WHEN  Items LIKE '%Description%'   THEN 0
	 WHEN  Items LIKE '%OrgName%'   THEN 0 ELSE 1 END)
	) AS T FOR XML PATH('')
)
,1,3,'') AS [Items]),'')
PRINT @ActivityRegularSearch
 SET @OtherRegularSearch = ISNULL((SELECT STUFF( 
(
	SELECT  'AND ' + [Items] FROM 
	(
	  SELECT [Items] FROM SplitByString(@RegularSearchCriteria,'AND') WHERE Items<>'' AND 1=
	  (CASE WHEN Items LIKE '%IsTaskClosed%' THEN 1
	 WHEN Items LIKE '%TaskType%'  THEN 1
	 WHEN  Items LIKE '%numAssignToId%'  THEN 1
	 WHEN  Items LIKE '%PriorityId%'  THEN 1
	 WHEN  Items LIKE '%ActivityId%'  THEN 1
	 WHEN  Items LIKE '%Description%'   THEN 1
	 WHEN  Items LIKE '%OrgName%'   THEN 1 ELSE 0 END)
	) AS T FOR XML PATH('')
)
,1,3,'') AS [Items]),'')
SET @OtherRegularSearch = ISNULL(REPLACE(@OtherRegularSearch,'T.',''),'')
IF(LEN(@OtherRegularSearch)=0)
BEGIN
	SET @OtherRegularSearch = ' 1=1 '
END
PRINT @OtherRegularSearch
Declare @ListRelID as numeric(9)             
set @tintOrder=0   

CREATE TABLE #tempForm 
	(
		tintOrder INT,vcDbColumnName nvarchar(50),vcFieldDataType nvarchar(50),vcOrigDbColumnName nvarchar(50),bitAllowFiltering BIT,vcFieldName nvarchar(50)
		,vcAssociatedControlType nvarchar(50),vcListItemType char(3),numListID numeric(9),vcLookBackTableName varchar(50),bitCustomField BIT,numFieldId NUMERIC
		,bitAllowSorting BIT,bitAllowEdit BIT,bitIsRequired BIT,bitIsEmail BIT,bitIsAlphaNumeric BIT,bitIsNumeric BIT,bitIsLengthValidation BIT,intMaxLength int
		,intMinLength int,bitFieldMessage BIT,vcFieldMessage VARCHAR(500),ListRelID numeric(9),intColumnWidth NUMERIC(18,2)
	)
                
	--Custom Fields related to Organization
	INSERT INTO 
		#tempForm
	SELECT  
		tintRow + 1 AS tintOrder,vcDbColumnName,'',vcDbColumnName,1,vcFieldName,vcAssociatedControlType,'' as vcListItemType,numListID,'' as vcLookBackTableName                                                 
		,bitCustom,numFieldId,bitAllowSorting,0,bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength
		,bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth
	FROM 
		View_DynamicCustomColumns
	WHERE 
		numFormId=43 
		AND numUserCntID=@numUserCntID 
		AND numDomainID=@numDomainID 
		AND tintPageType=1
		AND ISNULL(bitCustom,0)=1
 
	SELECT TOP 1 
		@tintOrder = tintOrder + 1
		,@vcDbColumnName=vcDbColumnName
		,@vcFieldName=vcFieldName
		,@vcAssociatedControlType=vcAssociatedControlType
		,@vcListItemType=vcListItemType
		,@numListID=numListID
		,@vcLookBackTableName=vcLookBackTableName                                               
		,@bitCustomField=bitCustomField
		,@numFieldId=numFieldId
		,@bitAllowSorting=bitAllowSorting
		,@bitAllowEdit=0
		,@ListRelID=ListRelID
	FROM 
		#tempForm 
	ORDER BY 
		tintOrder ASC     
  
	SET @vcCustomColumnName=''
	SET @vcnullCustomColumnName=''
	SET @vcCustomColumnNameOnly = ''
	WHILE @tintOrder>0      
	BEGIN  
		SET @vcColumnName= @vcDbColumnName
		SET @strSql = 'ALTER TABLE #tempRecords ADD [' + @vcColumnName + '] NVARCHAR(MAX)'
		SET @vcCustomColumnNameOnly = @vcCustomColumnNameOnly+','+@vcColumnName
		EXEC(@strSql)
		
		IF @bitCustomField = 1
		BEGIN 
			IF @vcAssociatedControlType = 'TextBox' OR @vcAssociatedControlType = 'TextArea'            
			BEGIN            
				SET @vcCustomColumnName= @vcCustomColumnName+', ISNULL((SELECT TOP 1 CFW.Fld_Value FROM CFW_FLD_Values AS CFW WHERE CFW.Fld_ID='+CAST(@numFieldId AS VARCHAR(50))+' AND CFW.RecId=Div.numDivisionID),'''') ['+ @vcColumnName +']'
				SET @vcnullCustomColumnName= @vcnullCustomColumnName+','''' AS  ['+ @vcColumnName +']'
			END  
			ELSE IF @vcAssociatedControlType = 'CheckBox'         
			BEGIN            
				SET @vcCustomColumnName= @vcCustomColumnName+',ISNULL((SELECT TOP 1 CFW.Fld_Value FROM CFW_FLD_Values AS CFW WHERE CFW.Fld_ID='+CAST(@numFieldId AS VARCHAR(50))+' AND CFW.RecId=Div.numDivisionID),0)  ['+ @vcColumnName + ']'
				SET @vcnullCustomColumnName= @vcnullCustomColumnName+',0 AS ['+ @vcColumnName + ']'
			END            
			ELSE IF @vcAssociatedControlType = 'DateField'        
			BEGIN            
				SET @vcCustomColumnName= @vcCustomColumnName+',dbo.FormatedDateFromDate((SELECT TOP 1 CFW.Fld_Value FROM CFW_FLD_Values AS CFW WHERE CFW.Fld_ID='+CAST(@numFieldId AS VARCHAR(50))+' AND CFW.RecId=Div.numDivisionID),'+convert(varchar(10),@numDomainId)+')  ['+ @vcColumnName +']'              
				SET @vcnullCustomColumnName= @vcnullCustomColumnName+','''' AS  ['+ @vcColumnName +']'
			END            
			ELSE IF @vcAssociatedControlType = 'SelectBox'           
			BEGIN            
				SET @vcCustomColumnName=@vcCustomColumnName+',(SELECT TOP 1 ISNULL(L.vcData,'''') FROM CFW_FLD_Values CFW left Join ListDetails L on L.numListItemID=CFW.Fld_Value WHERE  CFW.Fld_ID='+CAST(@numFieldId AS VARCHAR(50))+' AND CFW.RecId=Div.numDivisionID)'+' ['+ @vcColumnName +']'                                                         
				SET @vcnullCustomColumnName=@vcnullCustomColumnName+','''''+'AS  ['+ @vcColumnName +']'                                                         
			END
			ELSE IF @vcAssociatedControlType = 'CheckBoxList'
			BEGIN
				SET @vcCustomColumnName=@vcCustomColumnName+',(SELECT STUFF((SELECT CONCAT('', '', vcData) FROM ListDetails WHERE numListID= ' + CAST(@numListID AS varchar) + ' AND numlistitemid IN (SELECT ISNULL(Id,0) FROM dbo.SplitIDs(ISNULL((SELECT CFWInner.Fld_Value FROM CFW_FLD_Values CFWInner WHERE CFWInner.Fld_ID='+CAST(@numFieldId AS VARCHAR(50))+' AND CFWInner.RecId=Div.numDivisionID),''''),'','')) FOR XML PATH('''')), 1, 1, ''''))'+' ['+ @vcColumnName +']'
				SET @vcnullCustomColumnName=@vcnullCustomColumnName+','''''+'AS  ['+ @vcColumnName +']'
			END
			ELSE
			BEGIN
				SET @vcCustomColumnName= CONCAT(@vcCustomColumnName,',(SELECT dbo.fn_GetCustFldStringValue(',@numFieldId,',CFW.RecId,CFW.Fld_Value',') FROM CFW_FLD_Values CFW left Join ListDetails L on L.numListItemID=CFW.Fld_Value WHERE  CFW.Fld_ID=',@numFieldId,' AND CFW.RecId=Div.numDivisionID)',' [', @vcColumnName ,']')
				SET @vcnullCustomColumnName=@vcnullCustomColumnName+','''''+'AS  ['+ @vcColumnName +']' 
			END
		END
		  
		SELECT TOP 1 
			@tintOrder=tintOrder + 1
			,@vcDbColumnName=vcDbColumnName
			,@vcFieldName=vcFieldName
			,@vcAssociatedControlType=vcAssociatedControlType
			,@vcListItemType=vcListItemType
			,@numListID=numListID
			,@vcLookBackTableName=vcLookBackTableName                                               
			,@bitCustomField=bitCustomField
			,@numFieldId=numFieldId
			,@bitAllowSorting=bitAllowSorting
			,@bitAllowEdit=0
			,@ListRelID=ListRelID
		FROM 
			#tempForm 
		WHERE 
			tintOrder > @tintOrder-1 
		ORDER BY 
			tintOrder asc            
 
		IF @@rowcount=0 set @tintOrder=0 
	END
SET @strSql=''

Create table #tempColorScheme(vcOrigDbCOlumnName varchar(50),vcLookBackTableName varchar(50),vcFieldValue varchar(50),vcFieldValue1 varchar(50),vcColorScheme varchar(50),vcAssociatedControlType varchar(50))

insert into #tempColorScheme select DFM.vcOrigDbCOlumnName,DFM.vcLookBackTableName,DFCS.vcFieldValue,DFCS.vcFieldValue1,DFCS.vcColorScheme,DFFM.vcAssociatedControlType 
from DycFieldColorScheme DFCS join DycFormField_Mapping DFFM on DFCS.numFieldID=DFFM.numFieldID
join DycFieldMaster DFM on DFM.numModuleID=DFFM.numModuleID and DFM.numFieldID=DFFM.numFieldID
where DFCS.numDomainID=@numDomainID and DFFM.numFormID=43 AND DFCS.numFormID=43 and isnull(DFFM.bitAllowGridColor,0)=1

IF(SELECT COUNT(*) FROM #tempColorScheme)>0
BEGIN
   select top 1 @vcCSOrigDbCOlumnName=vcOrigDbCOlumnName,@vcCSLookBackTableName=vcLookBackTableName,@vcCSAssociatedControlType=vcAssociatedControlType from #tempColorScheme
END   
-------------------- 0 - TASK----------------------------

IF(LEN(@CustomSearchCriteria)>0)
BEGIN
SET @CustomSearchCriteria = ' AND '+@CustomSearchCriteria
END
IF(LEN(@ActivityRegularSearch)=0)
BEGIN
	SET @ActivityRegularSearch = '1=1'
END
DECLARE @StaticColumns AS NVARCHAR(MAX)=''

SET @StaticColumns = ' RecordCount,RecordId,
numCreatedBy,
numOrgTerId,
numTaskId,
IsTaskClosed,
TaskTypeName,
TaskType,
OrigDescription,
Description,
DueDate,
TotalProgress,
numAssignedTo,
numAssignToId,
Priority,
Activity,
PriorityId,
ActivityId,
OrgName,
CompanyRating,
numContactID,numDivisionID,tintCRMType,Id,bitTask,Startdate,EndTime,itemDesc,
[Name],vcFirstName,vcLastName,numPhone,numPhoneExtension,[Phone],vcCompanyName,vcProfile,vcEmail,Task,Status,numRecOwner,numModifiedBy,numTerId,numAssignedBy,caseid,vcCasenumber,
casetimeId,caseExpId,type,itemid,bitFollowUpAnyTime,numNoOfEmployeesId,vcComPhone,numFollowUpStatus,dtLastFollowUp,vcLastSalesOrderDate,monDealAmount,vcPerformance'
SET @dynamicQuery = ' INSERT INTO #tempRecords(
 '+@StaticColumns +@vcCustomColumnNameOnly+'
) SELECT COUNT(*) OVER() RecordCount,* FROM (SELECT 
Comm.numCommId AS RecordId,
Comm.numCreatedBy AS numCreatedBy, 
ISNULL(Div.numTerId,0) AS numOrgTerId,
-2 AS numTaskId,
bitClosedFlag AS IsTaskClosed,
''<a onclick="openCommTask(''+CAST(Comm.numCommId AS VARCHAR)+'',0)" href="javascript:void(0)">''+dbo.GetListIemName(Comm.bitTask)+''</a>'' As TaskTypeName,
Comm.bitTask AS TaskType,
'''' AS OrigDescription,
comm.textDetails AS Description,
cast(Comm.dtStartTime as datetime) as DueDate,
''-'' As TotalProgress,
Substring( 
dbo.fn_GetContactName(numAssign) + 
ISNULL((SELECT SUBSTRING((SELECT '','' + dbo.fn_GetContactName(numContactId) FROM CommunicationAttendees WHERE numCommId=Comm.numCommId FOR XML PATH('''')),1,200000)),''''),0,50)
As numAssignedTo,
numAssign As numAssignToId,
listdetailsStatus.VcData As Priority, -- Priority column   
 listdetailsActivity.VcData As Activity ,
listdetailsStatus.NumlistItemID As PriorityId, -- Priority column   
 listdetailsActivity.NumlistItemID As ActivityId ,
 ''<a href="../account/frmAccounts.aspx?DivID=''+CAST(Div.numDivisionID AS VARCHAR)+''">''+Comp.vcCompanyName+ CASE WHEN  ISNULL(listCompanyRating.VcData,'''')<>'''' THEN ''(''+listCompanyRating.VcData+'')'' ELSE '''' END +''</a>''  AS OrgName,
 listCompanyRating.VcData As CompanyRating,
 Addc.numContactID,Addc.numDivisionID,Div.tintCRMType,Comm.numCommId AS Id,convert(varchar(15),bitTask) as bitTask,
  DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', dtStartTime) as Startdate,                        
 DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', dtEndTime) as EndTime ,                                                                        
 Comm.textDetails AS itemDesc,  
 AddC.vcFirstName + '' '' + AddC.vcLastName  as [Name], 
 AddC.vcFirstName,AddC.vcLastName,Addc.numPhone,AddC.numPhoneExtension,                               
 case when Addc.numPhone<>'''' then + AddC.numPhone +case when AddC.numPhoneExtension<>'''' then '' - '' + AddC.numPhoneExtension else '''' end  else '''' end as [Phone], 
  Comp.vcCompanyName As vcCompanyName , ISNULL((SELECT vcData FROM ListDetails WHERE numListID =21 AND numDomainID='+ CAST(@numDomainID AS VARCHAR) +' AND  numListItemID=Comp.vcProfile ) ,'''') vcProfile ,                                                        
 AddC.vcEmail As vcEmail ,    
 dbo.GetListIemName(Comm.bitTask)AS Task,   
 listdetailsStatus.VcData As Status,dbo.fn_GetContactName(Comm.numCreatedBy) AS numRecOwner,dbo.fn_GetContactName(Comm.numModifiedBy) AS numModifiedBy,  
  (select TOP 1 vcData from ListDetails where numListItemID=Div.numTerId) AS numTerId,dbo.fn_GetContactName(Comm.numAssignedBy) AS numAssignedBy,
  convert(varchar(50),comm.caseid) as caseid,(select top 1 vcCaseNumber from cases where cases.numcaseid= comm.caseid )as vcCasenumber,
  ISNULL(comm.casetimeId,0) casetimeId,ISNULL(comm.caseExpId,0) caseExpId,0 as type,'''' as itemid,ISNULL(bitFollowUpAnyTime,0) bitFollowUpAnyTime,  
  dbo.GetListIemName(Comp.numNoOfEmployeesId) AS numNoOfEmployeesId,Div.vcComPhone,
dbo.GetListIemName(Div.numFollowUpStatus) AS numFollowUpStatus,' + 
CONCAT('ISNULL(dbo.FormatedDateFromDate(Div.dtLastFollowUp,',@numDomainID,'),'''')') + ' AS dtLastFollowUp,
' + CONCAT('ISNULL(dbo.FormatedDateFromDate(TempLastOrder.bintCreatedDate,',@numDomainID,'),'''')') + ' AS vcLastSalesOrderDate,ISNULL(TempPerformance.monDealAmount,0) monDealAmount,
(CASE WHEN ISNULL(TempPerformance.monDealAmount,0) > 0 THEN CONCAT(FORMAT(TempPerformance.monDealAmount,''##,####.00''),'' ('',CAST(TempPerformance.fltProfitPer AS DECIMAL(18,2)),''%)'') ELSE '''' END) AS vcPerformance
  '+@vcCustomColumnName+'
 FROM  Communication Comm                              
 JOIN AdditionalContactsInformation AddC                                                  
 ON Comm.numContactId = AddC.numContactId                                  
 JOIN DivisionMaster Div                                                                         
 ON AddC.numDivisionId = Div.numDivisionID                                   
 JOIN CompanyInfo  Comp                                                                         
 ON Div.numCompanyID = Comp.numCompanyId  
 OUTER APPLY(' + CONCAT('SELECT MAX(bintCreatedDate) AS bintCreatedDate FROM OpportunityMaster WHERE numDomainID = ',@numDomainID,' AND numDivisionID=Div.numDivisionID AND tintOppType = 1 AND tintOppStatus = 1') + ' ) AS TempLastOrder
 OUTER APPLY(' + CONCAT('SELECT
																	SUM(monDealAmount) AS monDealAmount
																	,AVG(ProfitPer) AS fltProfitPer
																FROM
																(
																	SELECT
																		OM.numDivisionId
																		,monDealAmount
																		,AVG(((ISNULL(OI.monPrice,0) - (CASE D.numCost WHEN 3 THEN ISNULL(V.monCost,0) * dbo.fn_UOMConversion(I.numBaseUnit,I.numItemCode,I.numDomainID,I.numPurchaseUnit) WHEN 2 THEN ISNULL(OI.numCost,0) ELSE ISNULL(I.monAverageCost,0) END)) / ISNULL(NULLIF(OI.monPrice,0),1)) * 100) ProfitPer
																	FROM
																		OpportunityMaster OM
																	INNER JOIN
																		Domain D
																	ON
																		OM.numDomainID = D.numDomainID
																	INNER JOIN
																		OpportunityItems OI
																	ON
																		OM.numOppId = OI.numOppId
																	INNER JOIN
																		Item I
																	ON
																		OI.numItemCode = I.numItemCode
																	LEFT JOIN
																		Vendor V
																	ON
																		V.numVendorID = I.numVendorID
																		AND V.numItemCode = I.numItemCode
																	WHERE
																		OM.numDomainId = ',@numDomainID,'
																		AND OM.numDivisionId = Div.numDivisionID
																		AND ISNULL(I.bitContainer,0) = 0
																		AND tintOppType = 1
																		AND tintOppStatus=1
																	GROUP BY
																		OM.numDivisionId
																		,OM.numOppId
																		,monDealAmount
																) T1
																GROUP BY
																	T1.numDivisionId )') + ' AS TempPerformance
 LEFT JOIN OpportunityMaster Opp ON Opp.numDivisionId = Div.numDivisionID AND Opp.numContactId = AddC.numContactId  AND Opp.numOppID = Comm.numOppID 
 LEFT JOIN Sales_process_List_Master splm ON splm.Slp_Id = Opp.numBusinessProcessID 
 LEFT JOIN ProjectProgress PP On PP.numOppId = Opp.numOppId
  Left Join listdetails listdetailsStatus                                                 
 On Comm.numStatus = listdetailsStatus.NumlistItemID
  Left Join listdetails listdetailsActivity                                                  
 On Comm.numActivity = listdetailsActivity.NumlistItemID
 LEFT JOIN ListDetails AS listCompanyRating
 ON Comp.numCompanyRating=listCompanyRating.NumlistItemID
WHERE '+@ActivityRegularSearch+' AND
	Comm.numDomainID = ' + Cast(@numDomainID as varchar(10))  + ' AND 1 = (CASE ' + CAST( @tintActionItemsViewRights AS VARCHAR) + 
' WHEN 3 THEN 1 
WHEN 2 THEN (CASE WHEN Div.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+ CONVERT(VARCHAR(15),@numUserCntID) + ' ) THEN 1 ELSE 0 END)
WHEN 1 THEN (CASE WHEN (Comm.numCreatedBy= ' + CONVERT(VARCHAR(15),@numUserCntID) + ' or Comm.numAssign= '+ CONVERT(VARCHAR(15),@numUserCntID) + ') THEN 1 ELSE 0 END) 
ELSE 0 END)  AND (DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',dtStartTime) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',dtStartTime) <= '''+Cast(@endDate as varchar(30))+''')
'+@CustomSearchCriteria+'
 )   Q WHERE '+@OtherRegularSearch+'
  '+@IndivRecordPaging+'
'

PRINT CAST(@dynamicQuery AS TEXT)
EXEC (@dynamicQuery)
-------------------- 0 - Email Communication----------------------------
IF(LEN(@ActivityRegularSearch)=0 OR @ActivityRegularSearch='1=1')
BEGIN
DECLARE @FormattedItems As VARCHAR(MAX)
SET @FormattedItems ='
RecordCount,RecordId,
numCreatedBy,
numOrgTerId,
numTaskId,
IsTaskClosed,
TaskTypeName,
TaskType,
OrigDescription,
Description,
DueDate,
TotalProgress,
numAssignedTo,
numAssignToId,
Priority,
Activity,
PriorityId,
ActivityId,
OrgName,
CompanyRating'
SET @dynamicQuery =  ' INSERT INTO #tempRecords(
'+@FormattedItems+'
) SELECT  COUNT(*) OVER() RecordCount,* FROM (  SELECT 
ac.activityid AS RecordId,
0 AS numCreatedBy,
ISNULL(Div.numTerId,0) AS numOrgTerId,
-2 AS numTaskId,
case when alldayevent = 1 then ''1'' else ''0'' end AS IsTaskClosed,
''<a onclick="openCommTask(''+CAST(ac.activityid AS VARCHAR)+'',1)" href="javascript:void(0)">Calendar</a>'' As TaskTypeName,
12 AS TaskType,
ac.ActivityDescription AS OrigDescription,
(SELECT DISTINCT  
replace(replace((SELECT 
CASE(ISNULL(bitpartycalendarTitle,1)) WHEN 1 THEN ''<b><font color=red>Title</font></b><br>'' + CASE(ISNULL(ac.Subject,'''')) WHEN '''' THEN '''' else ac.Subject END + ''<br>'' else ''''  END  
+ CASE(ISNULL(bitpartycalendarLocation,1)) WHEN 1 THEN ''<b><font color=green>Location</font></b><br>'' + CASE(ISNULL(ac.location,'''')) WHEN '''' THEN '''' else ac.location END + ''<br>'' else ''''  END  
+ CASE(isnull(bitpartycalendarDescription,1)) when 1 THEN ''<b><font color=#3c8dbc>Description</font></b><br>$'' else ''''  END  
FROM Domain where numDomainId='+ CAST(@numDomainID AS VARCHAR) +' 
FOR XML PATH('''')
),''&lt;'', ''<''), ''&gt;'', ''>''))  AS Description,
DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', dateadd(second,duration,startdatetimeutc)) as DueDate,
''0'' As TotalProgress,
''-'' As numAssignedTo,
''0'' As numAssignToId,
'''' As Priority, -- Priority column   
'''' As Activity ,
0 As PriorityId, -- Priority column   
 0 As ActivityId ,
 ''<a href="../account/frmAccounts.aspx?DivID=''+CAST(Div.numDivisionID AS VARCHAR)+''">''+Comp.vcCompanyName+ CASE WHEN  ISNULL(listCompanyRating.VcData,'''')<>'''' THEN ''(''+listCompanyRating.VcData+'')'' ELSE '''' END +''</a>''  AS OrgName,
 listCompanyRating.VcData As CompanyRating
 From activity ac join             
activityresource ar on  ac.activityid=ar.activityid             
join resource res on ar.resourceid=res.resourceid                         
--join usermaster UM on um.numUserId = res.numUserCntId                
join AdditionalContactsInformation ACI on ACI.numContactId = res.numUserCntId                
 JOIN DivisionMaster Div                                                          
 ON ACI.numDivisionId = Div.numDivisionID                                   
 JOIN CompanyInfo  Comp                             
 ON Div.numCompanyID = Comp.numCompanyId
  LEFT JOIN ListDetails AS listCompanyRating
 ON Comp.numCompanyRating=listCompanyRating.NumlistItemID
 OUTER APPLY(' + CONCAT('SELECT MAX(bintCreatedDate) AS bintCreatedDate FROM OpportunityMaster WHERE numDomainID = ',@numDomainID,' AND numDivisionID=Div.numDivisionID AND tintOppType = 1 AND tintOppStatus = 1') + ' ) AS TempLastOrder                 
 OUTER APPLY(' + CONCAT('SELECT
																	SUM(monDealAmount) AS monDealAmount
																	,AVG(ProfitPer) AS fltProfitPer
																FROM
																(
																	SELECT
																		OM.numDivisionId
																		,monDealAmount
																		,AVG(((ISNULL(OI.monPrice,0) - (CASE D.numCost WHEN 3 THEN ISNULL(V.monCost,0) * dbo.fn_UOMConversion(I.numBaseUnit,I.numItemCode,I.numDomainID,I.numPurchaseUnit) WHEN 2 THEN ISNULL(OI.numCost,0) ELSE ISNULL(I.monAverageCost,0) END)) / ISNULL(NULLIF(OI.monPrice,0),1)) * 100) ProfitPer
																	FROM
																		OpportunityMaster OM
																	INNER JOIN
																		Domain D
																	ON
																		OM.numDomainID = D.numDomainID
																	INNER JOIN
																		OpportunityItems OI
																	ON
																		OM.numOppId = OI.numOppId
																	INNER JOIN
																		Item I
																	ON
																		OI.numItemCode = I.numItemCode
																	LEFT JOIN
																		Vendor V
																	ON
																		V.numVendorID = I.numVendorID
																		AND V.numItemCode = I.numItemCode
																	WHERE
																		OM.numDomainId = ',@numDomainID,'
																		AND OM.numDivisionId = Div.numDivisionID
																		AND ISNULL(I.bitContainer,0) = 0
																		AND tintOppType = 1
																		AND tintOppStatus=1
																		
																	GROUP BY
																		OM.numDivisionId
																		,OM.numOppId
																		,monDealAmount
																) T1
																GROUP BY
																	T1.numDivisionId )') + ' AS TempPerformance

 where  res.numUserCntId = ' + Cast(@numUserCntID as varchar(10)) +'             
and ac.OriginalStartDateTimeUtc IS NULL            
and res.numDomainID = ' + Cast(@numDomainID as varchar(10)) +'  and 
(DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', startdatetimeutc) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', startdatetimeutc) <= '''+Cast(@endDate as varchar(30))+''')
--(startdatetimeutc between dateadd(month,-1,getutcdate()) and dateadd(month,1,getutcdate())) 
and [status] <> -1                                                     
and ac.activityid not in 
(select distinct(numActivityId) from communication Comm where Comm.numDomainID = ' + Cast(@numDomainID as varchar(10)) +'  
AND 1=(Case ' + Cast(@bitFilterRecord as varchar(10)) +' when 1 then Case when Comm.numAssign =' + Cast(@numUserCntID as varchar(10)) +'  then 1 else 0 end 
		else  Case when (Comm.numAssign =' + Cast(@numUserCntID as varchar(10)) +'  or Comm.numCreatedBy=' + Cast(@numUserCntID as varchar(10)) +' ) then 1 else 0 end end)   
		AND 1 = (CASE ' + CAST( @tintActionItemsViewRights AS VARCHAR) + 
' WHEN 3 THEN 1 
WHEN 2 THEN (CASE WHEN Div.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+ CONVERT(VARCHAR(15),@numUserCntID) + ' ) THEN 1 ELSE 0 END)
WHEN 1 THEN (CASE WHEN (Comm.numCreatedBy= ' + CONVERT(VARCHAR(15),@numUserCntID) + ' or Comm.numAssign= '+ CONVERT(VARCHAR(15),@numUserCntID) + ') THEN 1 ELSE 0 END) 
ELSE 0 END)                                                                   

)) Q WHERE '+@OtherRegularSearch+'  '+@IndivRecordPaging+'
'
EXEC (@dynamicQuery)
-------------------- 1 - CASE----------------------------

SET @dynamicQuery =  ' INSERT INTO #tempRecords(
RecordCount,
RecordId,
numCreatedBy,
numOrgTerId,
numTaskId,
IsTaskClosed,
TaskTypeName,
TaskType,
OrigDescription,
Description,
DueDate,
TotalProgress,
numAssignedTo,
numAssignToId,
Priority,
Activity,
PriorityId,
ActivityId,
OrgName,
CompanyRating
) SELECT  COUNT(*) OVER() RecordCount,* FROM (  SELECT 
C.numCaseId AS RecordId,
C.numCreatedBy AS numCreatedBy,
ISNULL(Div.numTerId,0) AS numOrgTerId,
0 AS numTaskId,
CASE WHEN C.numStatus=1008 THEN 1 ELSE 0 END AS IsTaskClosed,
''<a onclick="openCaseDetails(''+CAST(C.numCaseId AS VARCHAR)+'')" href="javascript:void(0)">Case</a>'' As TaskTypeName,
1 AS TaskType,
'''' AS OrigDescription,
C.textSubject AS Description,
cast(C.intTargetResolveDate as datetime) as DueDate,
''0'' As TotalProgress,
dbo.fn_GetContactName(C.numAssignedTo) As numAssignedTo,
C.numAssignedTo As numAssignToId,
listdetailsStatus.VcData As Priority, -- Priority column   
'''' As Activity ,
listdetailsStatus.NumlistItemID As PriorityId, -- Priority column   
 0 As ActivityId ,
 ''<a href="../account/frmAccounts.aspx?DivID=''+CAST(Div.numDivisionID AS VARCHAR)+''">''+Com.vcCompanyName+ CASE WHEN  ISNULL(listCompanyRating.VcData,'''')<>'''' THEN ''(''+listCompanyRating.VcData+'')'' ELSE '''' END +''</a>''  AS OrgName,
 listCompanyRating.VcData As CompanyRating

 FROM 
	Cases  C                          
INNER JOIN  
	AdditionalContactsInformation  AddC                          
ON 
	C.numContactId = AddC.numContactId                           
INNER JOIN 
	DivisionMaster Div                           
ON 
	AddC.numDivisionId = Div.numDivisionID AND  
	AddC.numDivisionId = Div.numDivisionID                           
INNER JOIN  
	CompanyInfo  Com                          
ON 
	Div.numCompanyID = Com.numCompanyId    
 LEFT JOIN ListDetails AS listCompanyRating
 ON Com.numCompanyRating=listCompanyRating.NumlistItemID
  Left Join listdetails listdetailsStatus                                                 
 On C.numPriority = listdetailsStatus.NumlistItemID
WHERE
	C.numDomainID = ' + Cast(@numDomainID as varchar(10))  + ' AND C.numStatus <> 136  AND 1 = (CASE ' + CAST( @tintActionItemsViewRights AS VARCHAR) + 
' WHEN 3 THEN 1 
WHEN 2 THEN (CASE WHEN Div.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+ CONVERT(VARCHAR(15),@numUserCntID) + ' ) THEN 1 ELSE 0 END)
WHEN 1 THEN (CASE WHEN (C.numCreatedBy= ' + CONVERT(VARCHAR(15),@numUserCntID) + ' or C.numAssignedTo= '+ CONVERT(VARCHAR(15),@numUserCntID) + ') THEN 1 ELSE 0 END) 
ELSE 0 END)  AND (DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',intTargetResolveDate) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',intTargetResolveDate) <= '''+Cast(@endDate as varchar(30))+''') 
) Q WHERE '+@OtherRegularSearch+' '+@IndivRecordPaging+'
'
EXEC (@dynamicQuery)

-------------------- 2 - Project Task ----------------------------

SET @dynamicQuery =  ' INSERT INTO #tempRecords(
RecordCount,
RecordId,
numCreatedBy,
numOrgTerId,
numTaskId,
IsTaskClosed,
TaskTypeName,
TaskType,
OrigDescription,
Description,
DueDate,
TotalProgress,
numAssignedTo,
numAssignToId,
Priority,
Activity,
PriorityId,
ActivityId,
OrgName,
CompanyRating
)  SELECT  COUNT(*) OVER() RecordCount,* FROM (SELECT
T.numProjectId AS RecordId,
T.numCreatedBy AS numCreatedBy,
ISNULL(Div.numTerId,0) AS numOrgTerId,
T.numTaskId AS numTaskId,
T.bitTaskClosed AS IsTaskClosed,
''<a onclick="openProjectTask(''+CAST(T.numProjectId AS VARCHAR)+'')" href="javascript:void(0)">Project Task</a>'' As TaskTypeName,
2 AS TaskType,
'''' AS OrigDescription,
T.vcTaskName AS Description,
CASE WHEN S.bitIsDueDaysUsed=1 THEN CONVERT(VARCHAR(10),DATEADD(DAY,S.intDueDays,CAST(S.dtStartDate AS DATE)), 101)
		ELSE CONVERT(VARCHAR(10),DATEADD(DAY,(SELECT TOP 1 CAST(Items AS DECIMAL(18,2)) FROM Split(dbo.fn_calculateTotalHoursMinutesByStageDetails(T.numStageDetailsId,T.numOppId,T.numProjectId),''_'')),CAST(S.dtStartDate AS DATE)), 101) END as DueDate,
ISNULL((SELECT TOP 1 intTotalProgress FROM ProjectProgress WHERE numProId = T.numProjectId),0) As TotalProgress,
dbo.fn_GetContactName(T.numAssignTo) As numAssignedTo,
T.numAssignTo As numAssignToId,
'''' As Priority, -- Priority column   
'''' As Activity ,
0 As PriorityId, -- Priority column   
 0 As ActivityId ,
 ''<a href="../account/frmAccounts.aspx?DivID=''+CAST(Div.numDivisionID AS VARCHAR)+''">''+Com.vcCompanyName+ CASE WHEN  ISNULL(listCompanyRating.VcData,'''')<>'''' THEN ''(''+listCompanyRating.VcData+'')'' ELSE '''' END +''</a>''  AS OrgName,
 listCompanyRating.VcData As CompanyRating

 FROM 
	StagePercentageDetailsTask AS T
	LEFT JOIN StagePercentageDetails AS S
	ON S.numStageDetailsId=T.numStageDetailsId AND S.numProjectID=T.numProjectID
	LEFT JOIN ProjectsMaster AS OP 
	ON T.numProjectID=OP.numProId
	LEFT JOIN StagePercentageMaster AS SM
	ON S.numStagePercentageId=SM.numStagePercentageId                        
INNER JOIN 
	DivisionMaster Div                           
ON 
	OP.numDivisionId = Div.numDivisionID                       
INNER JOIN  
	CompanyInfo  Com                          
ON 
	Div.numCompanyID = Com.numCompanyId    
LEFT JOIN ListDetails AS listCompanyRating
 ON Com.numCompanyRating=listCompanyRating.NumlistItemID
WHERE
	T.numDomainID = ' + Cast(@numDomainID as varchar(10))  + ' AND T.numProjectId>0 AND s.numProjectId>0 AND T.bitSavedTask=1 AND T.bitTaskClosed=0 AND 1 = (CASE ' + CAST( @tintActionItemsViewRights AS VARCHAR) + 
' WHEN 3 THEN 1 
WHEN 2 THEN (CASE WHEN Div.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+ CONVERT(VARCHAR(15),@numUserCntID) + ' ) THEN 1 ELSE 0 END)
WHEN 1 THEN (CASE WHEN (T.numCreatedBy= ' + CONVERT(VARCHAR(15),@numUserCntID) + ' or T.numAssignTo= '+ CONVERT(VARCHAR(15),@numUserCntID) + ') THEN 1 ELSE 0 END) 
ELSE 0 END) ) Q WHERE '+@OtherRegularSearch+' AND (DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',Q.DueDate) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',Q.DueDate) <= '''+Cast(@endDate as varchar(30))+''') 
'+@IndivRecordPaging+'
'

EXEC (@dynamicQuery)

------------------ 3 - Opportunity  Task ----------------------------

SET @dynamicQuery =  ' INSERT INTO #tempRecords(
RecordCount,
RecordId,
numCreatedBy,
numOrgTerId,
numTaskId,
IsTaskClosed,
TaskTypeName,
TaskType,
OrigDescription,
Description,
DueDate,
TotalProgress,
numAssignedTo,
numAssignToId,
Priority,
Activity,
PriorityId,
ActivityId,
OrgName,
CompanyRating
) SELECT  COUNT(*) OVER() RecordCount,* FROM ( SELECT 
T.numOppId AS RecordId,
T.numCreatedBy AS numCreatedBy,
ISNULL(Div.numTerId,0) AS numOrgTerId,
T.numTaskId AS numTaskId,
T.bitTaskClosed AS IsTaskClosed,
''<a onclick="openTask(''+CAST(T.numOppId AS VARCHAR)+'',''+CAST(S.numStagePercentageId AS VARCHAR)+'',''+CAST(S.tinProgressPercentage AS VARCHAR)+'',''+CAST(S.tintConfiguration AS VARCHAR)+'')" href="javascript:void(0)">Opportunity Task</a>'' As TaskTypeName,
3 AS TaskType,
'''' AS OrigDescription,
T.vcTaskName AS Description,
CASE WHEN S.bitIsDueDaysUsed=1 THEN CONVERT(VARCHAR(10),DATEADD(DAY,S.intDueDays,CAST(S.dtStartDate AS DATE)), 101)
		ELSE CONVERT(VARCHAR(10),DATEADD(DAY,(SELECT TOP 1 CAST(Items AS DECIMAL(18,2)) FROM Split(dbo.fn_calculateTotalHoursMinutesByStageDetails(T.numStageDetailsId,T.numOppId,T.numProjectId),''_'')),CAST(S.dtStartDate AS DATE)), 101) END as DueDate,
ISNULL((SELECT TOP 1 intTotalProgress FROM ProjectProgress WHERE numOppId = T.numOppId),0) As TotalProgress,
dbo.fn_GetContactName(T.numAssignTo) As numAssignedTo,
T.numAssignTo As numAssignToId,
'''' As Priority, -- Priority column   
'''' As Activity ,
0 As PriorityId, -- Priority column   
 0 As ActivityId ,
 ''<a href="../account/frmAccounts.aspx?DivID=''+CAST(Div.numDivisionID AS VARCHAR)+''">''+Com.vcCompanyName+ CASE WHEN  ISNULL(listCompanyRating.VcData,'''')<>'''' THEN ''(''+listCompanyRating.VcData+'')'' ELSE '''' END +''</a>''  AS OrgName,
 listCompanyRating.VcData As CompanyRating

 FROM 
	StagePercentageDetailsTask AS T
	LEFT JOIN StagePercentageDetails AS S
	ON S.numStageDetailsId=T.numStageDetailsId AND S.numOppId=T.numOppId
	LEFT JOIN OpportunityMaster AS OP 
	ON T.numOppId=OP.numOppId
	LEFT JOIN StagePercentageMaster AS SM
	ON S.numStagePercentageId=SM.numStagePercentageId                        
INNER JOIN 
	DivisionMaster Div                           
ON 
	OP.numDivisionId = Div.numDivisionID                       
INNER JOIN  
	CompanyInfo  Com                          
ON 
	Div.numCompanyID = Com.numCompanyId    
LEFT JOIN ListDetails AS listCompanyRating
 ON Com.numCompanyRating=listCompanyRating.NumlistItemID
WHERE
	T.numDomainID = ' + Cast(@numDomainID as varchar(10))  + ' AND OP.tintOppType=1 AND OP.tintOppStatus=0 AND T.numOppId>0 AND s.numOppId>0 AND T.bitSavedTask=1 AND T.bitTaskClosed=0 AND 1 = (CASE ' + CAST( @tintActionItemsViewRights AS VARCHAR) + 
' WHEN 3 THEN 1 
WHEN 2 THEN (CASE WHEN Div.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+ CONVERT(VARCHAR(15),@numUserCntID) + ' ) THEN 1 ELSE 0 END)
WHEN 1 THEN (CASE WHEN (T.numCreatedBy= ' + CONVERT(VARCHAR(15),@numUserCntID) + ' or T.numAssignTo= '+ CONVERT(VARCHAR(15),@numUserCntID) + ') THEN 1 ELSE 0 END) 
ELSE 0 END) ) Q WHERE '+@OtherRegularSearch+' AND (DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',Q.DueDate) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',Q.DueDate) <= '''+Cast(@endDate as varchar(30))+''')
'+@IndivRecordPaging+' 
 '
 
 EXEC (@dynamicQuery)

------------------ 4 -  Sales Order Task ----------------------------

SET @dynamicQuery =  ' INSERT INTO #tempRecords(
RecordCount,
RecordId,
numCreatedBy,
numOrgTerId,
numTaskId,
IsTaskClosed,
TaskTypeName,
TaskType,
OrigDescription,
Description,
DueDate,
TotalProgress,
numAssignedTo,
numAssignToId,
Priority,
Activity,
PriorityId,
ActivityId,
OrgName,
CompanyRating
)SELECT  COUNT(*) OVER() RecordCount,* FROM (  SELECT 
T.numOppId AS RecordId,
T.numCreatedBy AS numCreatedBy,
ISNULL(Div.numTerId,0) AS numOrgTerId,
T.numTaskId AS numTaskId,
T.bitTaskClosed AS IsTaskClosed,
''<a onclick="openTask(''+CAST(T.numOppId AS VARCHAR)+'',''+CAST(S.numStagePercentageId AS VARCHAR)+'',''+CAST(S.tinProgressPercentage AS VARCHAR)+'',''+CAST(S.tintConfiguration AS VARCHAR)+'')" href="javascript:void(0)">Sales Order Task</a>'' As TaskTypeName,
4 AS TaskType,
'''' AS OrigDescription,
T.vcTaskName AS Description,
CASE WHEN S.bitIsDueDaysUsed=1 THEN CONVERT(VARCHAR(10),DATEADD(DAY,S.intDueDays,CAST(S.dtStartDate AS DATE)), 101)
		ELSE CONVERT(VARCHAR(10),DATEADD(DAY,(SELECT TOP 1 CAST(Items AS DECIMAL(18,2)) FROM Split(dbo.fn_calculateTotalHoursMinutesByStageDetails(T.numStageDetailsId,T.numOppId,T.numProjectId),''_'')),CAST(S.dtStartDate AS DATE)), 101) END as DueDate,
ISNULL((SELECT TOP 1 intTotalProgress FROM ProjectProgress WHERE numOppId = T.numOppId),0) As TotalProgress,
dbo.fn_GetContactName(T.numAssignTo) As numAssignedTo,
T.numAssignTo As numAssignToId,
'''' As Priority, -- Priority column   
'''' As Activity ,
0 As PriorityId, -- Priority column   
 0 As ActivityId ,
 ''<a href="../account/frmAccounts.aspx?DivID=''+CAST(Div.numDivisionID AS VARCHAR)+''">''+Com.vcCompanyName+ CASE WHEN  ISNULL(listCompanyRating.VcData,'''')<>'''' THEN ''(''+listCompanyRating.VcData+'')'' ELSE '''' END +''</a>''  AS OrgName,
 listCompanyRating.VcData As CompanyRating

 FROM 
	StagePercentageDetailsTask AS T
	LEFT JOIN StagePercentageDetails AS S
	ON S.numStageDetailsId=T.numStageDetailsId AND S.numOppId=T.numOppId
	LEFT JOIN OpportunityMaster AS OP 
	ON T.numOppId=OP.numOppId
	LEFT JOIN StagePercentageMaster AS SM
	ON S.numStagePercentageId=SM.numStagePercentageId                        
INNER JOIN 
	DivisionMaster Div                           
ON 
	OP.numDivisionId = Div.numDivisionID                       
INNER JOIN  
	CompanyInfo  Com                          
ON 
	Div.numCompanyID = Com.numCompanyId    
LEFT JOIN ListDetails AS listCompanyRating
 ON Com.numCompanyRating=listCompanyRating.NumlistItemID
WHERE
	T.numDomainID = ' + Cast(@numDomainID as varchar(10))  + ' AND OP.tintOppType=1 AND OP.tintOppStatus=1 AND T.numOppId>0 AND s.numOppId>0 AND T.bitSavedTask=1 AND T.bitTaskClosed=0 AND 1 = (CASE ' + CAST( @tintActionItemsViewRights AS VARCHAR) + 
' WHEN 3 THEN 1 
WHEN 2 THEN (CASE WHEN Div.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+ CONVERT(VARCHAR(15),@numUserCntID) + ' ) THEN 1 ELSE 0 END)
WHEN 1 THEN (CASE WHEN (T.numCreatedBy= ' + CONVERT(VARCHAR(15),@numUserCntID) + ' or T.numAssignTo= '+ CONVERT(VARCHAR(15),@numUserCntID) + ') THEN 1 ELSE 0 END) 
ELSE 0 END) ) Q WHERE '+@OtherRegularSearch+' AND (DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',Q.DueDate) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',Q.DueDate) <= '''+Cast(@endDate as varchar(30))+''')
'+@IndivRecordPaging+'
 '
 EXEC (@dynamicQuery)
  
------------------ 5 -  Work Order Task ----------------------------

SET @dynamicQuery =  ' INSERT INTO #tempRecords(
RecordCount,
RecordId,
numCreatedBy,
numOrgTerId,
numTaskId,
IsTaskClosed,
TaskTypeName,
TaskType,
OrigDescription,
Description,
DueDate,
TotalProgress,
numAssignedTo,
numAssignToId,
Priority,
Activity,
PriorityId,
ActivityId,
OrgName,
CompanyRating
)SELECT  COUNT(*) OVER() RecordCount,* FROM ( SELECT 
WorkOrder.numWOID AS RecordId,
WorkOrder.numCreatedBy AS numCreatedBy,
ISNULL(Div.numTerId,0) AS numOrgTerId,
WorkOrder.numWOID AS numTaskId,
(CASE WHEN WorkOrder.numWOStatus = 23184 THEN 1 ELSE 0 END) AS IsTaskClosed,
CONCAT(''<a href="../items/frmWorkOrder.aspx?WOID='',WorkOrder.numWOID,''" target="_blank">Work Order Management</a>'') As TaskTypeName,
5 AS TaskType,
'''' AS OrigDescription,
CONCAT(''<b>'',Item.vcItemName,''</b>'','' ('',WorkOrder.numQtyItemsReq,'')'','' <b>Work Order Status :'', ''</b>'',''<label class="lblWorkOrderStatus" id="'',WorkOrder.numWOID,''"><i class="fa fa-refresh fa-spin"></i></lable>'') AS Description,
DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',WorkOrder.dtmEndDate) as DueDate,
dbo.GetTotalProgress(WorkOrder.numDomainID,WorkOrder.numWOID,1,1,'''',0) As TotalProgress,
dbo.fn_GetContactName(WorkOrder.numAssignedTo) As numAssignedTo,
WorkOrder.numAssignedTo As numAssignToId,
'''' As Priority, -- Priority column   
'''' As Activity ,
0 As PriorityId, -- Priority column   
 0 As ActivityId ,
 ''<a href="../account/frmAccounts.aspx?DivID=''+CAST(Div.numDivisionID AS VARCHAR)+''">''+Com.vcCompanyName+ CASE WHEN  ISNULL(listCompanyRating.VcData,'''')<>'''' THEN ''(''+listCompanyRating.VcData+'')'' ELSE '''' END +''</a>''  AS OrgName,
 listCompanyRating.VcData As CompanyRating

 FROM 
	WorkOrder
INNER JOIN 
	Item
ON
	WorkOrder.numItemCode = Item.numItemCode
LEFT JOIN 
	OpportunityMaster
ON 
	WorkOrder.numOppId=OpportunityMaster.numOppId
INNER JOIN 
	DivisionMaster Div                           
ON 
	Div.numDivisionId = (CASE WHEN OpportunityMaster.numOppID IS NOT NULL THEN OpportunityMaster.numDivisionID ELSE ' + CAST(@numDomainDivisionID AS VARCHAR) + ' END)                        
INNER JOIN  
	CompanyInfo  Com                          
ON 
	Div.numCompanyID = Com.numCompanyId    
LEFT JOIN 
	ListDetails AS listCompanyRating
ON 
	Com.numCompanyRating=listCompanyRating.NumlistItemID
WHERE
	WorkOrder.numDomainID = ' + Cast(@numDomainID as varchar(10))  + ' 
	AND WorkOrder.numWOStatus <> 23184
	AND WorkOrder.numAssignedTo= '+ CONVERT(VARCHAR(15),@numUserCntID) + '
	AND ((DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',WorkOrder.dtmStartDate) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',WorkOrder.dtmStartDate) <= '''+Cast(@endDate as varchar(30))+''') OR (DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',WorkOrder.dtmEndDate) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',WorkOrder.dtmEndDate) <= '''+Cast(@endDate as varchar(30))+'''))
) Q WHERE '+@OtherRegularSearch+' '+@IndivRecordPaging+'
 '
 EXEC (@dynamicQuery)
------------------ 6 -  Work Center Task ----------------------------

SET @dynamicQuery =  ' INSERT INTO #tempRecords(
RecordCount,
RecordId,
numCreatedBy,
numOrgTerId,
numTaskId,
IsTaskClosed,
TaskTypeName,
TaskType,
OrigDescription,
Description,
DueDate,
TotalProgress,
numAssignedTo,
numAssignToId,
Priority,
Activity,
PriorityId,
ActivityId,
OrgName,
CompanyRating
) SELECT  COUNT(*) OVER() RecordCount,* FROM ( SELECT 
T.numTaskId AS RecordId,
T.numCreatedBy AS numCreatedBy,
ISNULL(Div.numTerId,0) AS numOrgTerId,
T.numTaskId AS numTaskId,
(CASE WHEN EXISTS (SELECT * FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID=T.numTaskID AND SPDTTL.tintAction=4) THEN 1 ELSE 0 END) AS IsTaskClosed,
CONCAT(''<a href="../items/frmWorkOrder.aspx?WOID='',W.numWOID,''" target="_blank">Work Center Task</a>'') As TaskTypeName,
6 AS TaskType,
'''' AS OrigDescription,
T.vcTaskName AS Description,
DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',W.dtmEndDate) as DueDate,
(CASE 
	WHEN EXISTS (SELECT * FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID=T.numTaskID AND SPDTTL.tintAction=4) THEN CAST(100 AS INT)
	ELSE CAST(((ISNULL((SELECT SUM(numProcessedQty) FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID=T.numTaskID),0) / W.numQtyItemsReq) * 100) AS INT)
END) As TotalProgress,
dbo.fn_GetContactName(T.numAssignTo) As numAssignedTo,
T.numAssignTo As numAssignToId,
'''' As Priority, -- Priority column   
'''' As Activity ,
0 As PriorityId, -- Priority column   
 0 As ActivityId ,
 ''<a href="../account/frmAccounts.aspx?DivID=''+CAST(Div.numDivisionID AS VARCHAR)+''">''+Com.vcCompanyName+ CASE WHEN  ISNULL(listCompanyRating.VcData,'''')<>'''' THEN ''(''+listCompanyRating.VcData+'')'' ELSE '''' END +''</a>''  AS OrgName,
 listCompanyRating.VcData As CompanyRating

 FROM 
	StagePercentageDetailsTask AS T
	INNER JOIN WorkOrder AS W 
	ON T.numWorkOrderId=W.numWOId     
LEFT JOIN OpportunityMaster AS OP 
	ON W.numOppId=OP.numOppId
INNER JOIN 
	DivisionMaster Div                           
ON 
	Div.numDivisionId = (CASE WHEN OP.numOppID IS NOT NULL THEN OP.numDivisionID ELSE ' + CAST(@numDomainDivisionID AS VARCHAR) + ' END)                         
INNER JOIN  
	CompanyInfo  Com                          
ON 
	Div.numCompanyID = Com.numCompanyId    
LEFT JOIN ListDetails AS listCompanyRating
 ON Com.numCompanyRating=listCompanyRating.NumlistItemID
WHERE
	W.numDomainID = ' + Cast(@numDomainID as varchar(10))  + '
	AND W.numWOStatus <> 23184 
	AND NOT EXISTS (SELECT * FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID=T.numTaskID AND SPDTTL.tintAction=4) 
	AND T.numAssignTo = ' + CAST(@numUserCntID AS VARCHAR) + '
	AND ((DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',W.dtmStartDate) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',W.dtmStartDate) <= '''+Cast(@endDate as varchar(30))+''') OR (DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',W.dtmEndDate) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',W.dtmEndDate) <= '''+Cast(@endDate as varchar(30))+''')) 
) Q WHERE '+@OtherRegularSearch+' '+@IndivRecordPaging+'
'
EXEC (@dynamicQuery)

------------------ 7 -  Requisition Approval ----------------------------

IF(@bitREQPOApproval=1 AND @numUserCntID IN(SELECT * FROM Split(@vchREQPOApprovalEmp,',') WHERE Items<>''))
BEGIN
SET @dynamicQuery =  ' INSERT INTO #tempRecords(
RecordCount,
RecordId,
numCreatedBy,
numOrgTerId,
numTaskId,
IsTaskClosed,
TaskTypeName,
TaskType,
OrigDescription,
Description,
DueDate,
TotalProgress,
numAssignedTo,
numAssignToId,
Priority,
Activity,
PriorityId,
ActivityId,
OrgName,
CompanyRating
) SELECT  COUNT(*) OVER() RecordCount,* FROM( SELECT 
OP.numOppId AS RecordId,
OP.numCreatedBy AS numCreatedBy,
ISNULL(Div.numTerId,0) AS numOrgTerId,
0 AS numTaskId,
CASE WHEN ISNULL(OP.intReqPOApproved,0)=0 THEN 0 ELSE 1 END AS IsTaskClosed,
''<a  onclick="openSalesOrder(''+CAST(OP.numOppId AS VARCHAR)+'')" href="javascript:void(0)">Requisition Approval</a>'' As TaskTypeName,
7 AS TaskType,
'''' AS OrigDescription,
''<b>For :</b> <a class="underLineHyperLink" onclick="openSalesOrder(''+CAST(OP.numOppId AS VARCHAR)+'')" href="javascript:void(0)">''+OP.vcPOppName+''</a> &nbsp;<i onclick="ApproveRejectPORequistion(''+CAST(OP.numOppId AS VARCHAR)+'',1)"  class="fa fa-thumbs-up cursor"></i>  &nbsp;<i onclick="ApproveRejectPORequistion(''+CAST(OP.numOppId AS VARCHAR)+'',2)"  class="fa fa-thumbs-down cursor"></i>'' AS Description,
OP.intPEstimatedCloseDate as DueDate,
0 As TotalProgress,
dbo.fn_GetContactName('+CAST(@numUserCntID AS VARCHAR(200))+') As numAssignedTo,
'+CAST(@numUserCntID AS VARCHAR(200))+' As numAssignToId,
'''' As Priority, -- Priority column   
'''' As Activity ,
0 As PriorityId, -- Priority column   
 0 As ActivityId ,
 ''<a href="../account/frmAccounts.aspx?DivID=''+CAST(Div.numDivisionID AS VARCHAR)+''">''+Com.vcCompanyName+ CASE WHEN  ISNULL(listCompanyRating.VcData,'''')<>'''' THEN ''(''+listCompanyRating.VcData+'')'' ELSE '''' END +''</a>''  AS OrgName,
 listCompanyRating.VcData As CompanyRating

 FROM  
 OpportunityMaster AS OP 
INNER JOIN 
	DivisionMaster Div                           
ON 
	OP.numDivisionId = Div.numDivisionID                       
INNER JOIN  
	CompanyInfo  Com                          
ON 
	Div.numCompanyID = Com.numCompanyId    
LEFT JOIN ListDetails AS listCompanyRating
 ON Com.numCompanyRating=listCompanyRating.NumlistItemID
WHERE
	OP.numDomainID = ' + Cast(@numDomainID as varchar(10))  + ' AND OP.tintOppType = 2 AND OP.tintOppStatus=0 
	AND 1 = (CASE WHEN '+CAST(@decReqPOMinValue AS VARCHAR(100))+'>0 AND Op.monPAmount>'+CAST(@decReqPOMinValue AS VARCHAR(100))+' THEN 1  WHEN '+CAST(@decReqPOMinValue AS VARCHAR(100))+'=0 THEN 1  ELSE 0 END ) ) AS Q
WHERE '+@OtherRegularSearch+' AND (DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',Q.DueDate) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',Q.DueDate) <= '''+Cast(@endDate as varchar(30))+''') 
'+@IndivRecordPaging+'
'
EXEC (@dynamicQuery)

END
------------------ 8 -  PO Approval ----------------------------
IF(@bitREQPOApproval=1 AND @numUserCntID IN(SELECT * FROM Split(@vchREQPOApprovalEmp,',') WHERE Items<>''))
BEGIN
SET @dynamicQuery =  ' INSERT INTO #tempRecords(
RecordCount,
RecordId,
numCreatedBy,
numOrgTerId,
numTaskId,
IsTaskClosed,
TaskTypeName,
TaskType,
OrigDescription,
Description,
DueDate,
TotalProgress,
numAssignedTo,
numAssignToId,
Priority,
Activity,
PriorityId,
ActivityId,
OrgName,
CompanyRating
) SELECT  COUNT(*) OVER() RecordCount,* FROM ( SELECT
OP.numOppId AS RecordId,
OP.numCreatedBy AS numCreatedBy,
ISNULL(Div.numTerId,0) AS numOrgTerId,
0 AS numTaskId,
CASE WHEN ISNULL(OP.intReqPOApproved,0)=0 THEN 0 ELSE 1 END AS IsTaskClosed,
''<a onclick="openSalesOrder(''+CAST(OP.numOppId AS VARCHAR)+'')" href="javascript:void(0)">PO Approval</a>'' As TaskTypeName,
8 AS TaskType,
'''' AS OrigDescription,
''<b>For:</b> <a href="javascript:void(0)"  class="underLineHyperLink" onclick="openSalesOrder(''+CAST(OP.numOppId AS VARCHAR)+'')">''+OP.vcPOppName+''</a> &nbsp;<i onclick="ApproveRejectPORequistion(''+CAST(OP.numOppId AS VARCHAR)+'',1)"  class="fa fa-thumbs-up cursor"></i>  &nbsp;<i onclick="ApproveRejectPORequistion(''+CAST(OP.numOppId AS VARCHAR)+'',2)"  class="fa fa-thumbs-down cursor"></i> '' AS Description,
OP.intPEstimatedCloseDate as DueDate,
0 As TotalProgress,
dbo.fn_GetContactName('+CAST(@numUserCntID AS VARCHAR(200))+') As numAssignedTo,
'+CAST(@numUserCntID AS VARCHAR(200))+' As numAssignToId,
'''' As Priority, -- Priority column   
'''' As Activity ,
0 As PriorityId, -- Priority column   
 0 As ActivityId ,
 ''<a href="../account/frmAccounts.aspx?DivID=''+CAST(Div.numDivisionID AS VARCHAR)+''">''+Com.vcCompanyName+ CASE WHEN  ISNULL(listCompanyRating.VcData,'''')<>'''' THEN ''(''+listCompanyRating.VcData+'')'' ELSE '''' END +''</a>''  AS OrgName,
 listCompanyRating.VcData As CompanyRating

 FROM  
 OpportunityMaster AS OP 
INNER JOIN 
	DivisionMaster Div                           
ON 
	OP.numDivisionId = Div.numDivisionID                       
INNER JOIN  
	CompanyInfo  Com                          
ON 
	Div.numCompanyID = Com.numCompanyId    
LEFT JOIN ListDetails AS listCompanyRating
 ON Com.numCompanyRating=listCompanyRating.NumlistItemID
WHERE
	OP.numDomainID = ' + Cast(@numDomainID as varchar(10))  + ' AND OP.tintOppType = 2 AND OP.tintOppStatus=1 
	AND 1 = (CASE WHEN '+CAST(@decReqPOMinValue AS VARCHAR(100))+'>0 AND Op.monPAmount>'+CAST(@decReqPOMinValue AS VARCHAR(100))+' THEN 1  WHEN '+CAST(@decReqPOMinValue AS VARCHAR(100))+'=0 THEN 1  ELSE 0 END )) AS Q
	WHERE  '+@OtherRegularSearch+' AND  (DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',Q.DueDate) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',Q.DueDate) <= '''+Cast(@endDate as varchar(30))+''') 
'+@IndivRecordPaging+'
'
EXEC (@dynamicQuery)

END

------------------ 9 -  Price Margin Approval ----------------------------

SET @dynamicQuery =  ' INSERT INTO #tempRecords(
RecordCount,
RecordId,
numCreatedBy,
numOrgTerId,
numTaskId,
IsTaskClosed,
TaskTypeName,
TaskType,
OrigDescription,
Description,
DueDate,
TotalProgress,
numAssignedTo,
numAssignToId,
Priority,
Activity,
PriorityId,
ActivityId,
OrgName,
CompanyRating
) SELECT COUNT(*) OVER() RecordCount,* FROM (SELECT
OP.numOppId AS RecordId,
OP.numCreatedBy AS numCreatedBy,
ISNULL(Div.numTerId,0) AS numOrgTerId,
0 AS numTaskId,
ISNULL(OP.bitReqPOApproved,0) AS IsTaskClosed,
''<a onclick="OpenMarginApproval(''+CAST(OP.numOppId AS VARCHAR)+'')" href="javascript:void(0)">Price Margin Approval</a>'' As TaskTypeName,
9 AS TaskType,
'''' AS OrigDescription,
''<b>For : </b> <a href="javascript:void(0)"  onclick="openSalesOrder(''+CAST(OP.numOppId AS VARCHAR)+'')">''+OP.vcPOppName+''</a> : '' +dbo.ItemUnitPriceApproval_CheckAndGetItemDetails(OP.numOppId,OP.numDomainId)+''  &nbsp;<i onclick="ApproveRejectPriceMarginApproval(''+CAST(OP.numOppId AS VARCHAR)+'',1)" class="fa fa-thumbs-up cursor"></i>  &nbsp;<i onclick="ApproveRejectPriceMarginApproval(''+CAST(OP.numOppId AS VARCHAR)+'',2)" class="fa fa-thumbs-down cursor"></i>''  AS Description,
OP.bintCreatedDate as DueDate,
0 As TotalProgress,
dbo.fn_GetContactName('+CAST(@numUserCntID AS VARCHAR(200))+') As numAssignedTo,
'+CAST(@numUserCntID AS VARCHAR(200))+' As numAssignToId,
'''' As Priority, -- Priority column   
'''' As Activity ,
0 As PriorityId, -- Priority column   
 0 As ActivityId ,
 ''<a href="../account/frmAccounts.aspx?DivID=''+CAST(Div.numDivisionID AS VARCHAR)+''">''+Com.vcCompanyName+ CASE WHEN  ISNULL(listCompanyRating.VcData,'''')<>'''' THEN ''(''+listCompanyRating.VcData+'')'' ELSE '''' END +''</a>''  AS OrgName,
 listCompanyRating.VcData As CompanyRating

 FROM  
 OpportunityMaster AS OP 
INNER JOIN 
	DivisionMaster Div                           
ON 
	OP.numDivisionId = Div.numDivisionID                       
INNER JOIN  
	CompanyInfo  Com                          
ON 
	Div.numCompanyID = Com.numCompanyId    
LEFT JOIN ListDetails AS listCompanyRating
 ON Com.numCompanyRating=listCompanyRating.NumlistItemID
WHERE
	OP.numDomainID = ' + Cast(@numDomainID as varchar(10))  + ' AND OP.tintOppType = 1 AND (SELECT COUNT(OpportunityItems.numoppitemtCode) FROM OpportunityItems 
						WHERE OpportunityItems.bitItemPriceApprovalRequired=1 AND OpportunityItems.numOppId=OP.numOppId) >0
	AND '+CAST(@numUserCntID AS VARCHAR(100))+' IN (SELECT numUserID FROM UnitPriceApprover WHERE numDomainId= ' + Cast(@numDomainID as varchar(10))  + ' )) AS Q
	WHERE  '+@OtherRegularSearch+' AND  (DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',Q.DueDate) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',Q.DueDate) <= '''+Cast(@endDate as varchar(30))+''') 
'+@IndivRecordPaging+'

'
EXEC (@dynamicQuery)

------------------------10 - A/R Invoice Due------------------------------------
IF(@bitARInvoiceDue=1 AND @numUserCntID IN(SELECT * FROM Split(@vchARInvoiceDue,',') WHERE Items<>''))
BEGIN
 DECLARE  @AuthoritativeSalesBizDocId  AS INTEGER
    SELECT @AuthoritativeSalesBizDocId = isnull(numAuthoritativeSales,0)
    FROM   AuthoritativeBizDocs
    WHERE  numDomainId = @numDomainId;
SET @dynamicQuery=  ' INSERT INTO #tempRecords(
RecordCount,
RecordId,
numCreatedBy,
numOrgTerId,
numTaskId,
IsTaskClosed,
TaskTypeName,
TaskType,
OrigDescription,
Description,
DueDate,
TotalProgress,
numAssignedTo,
numAssignToId,
Priority,
Activity,
PriorityId,
ActivityId,
OrgName,
CompanyRating
) SELECT  COUNT(*) OVER() RecordCount,* FROM ( SELECT
OM.numOppId AS RecordId,
OM.numCreatedBy AS numCreatedBy,
ISNULL(DM.numTerId,0) AS numOrgTerId,
OB.numOppBizDocsId AS numTaskId,
0 AS IsTaskClosed,
''<a href="javascript:void(0)"  onclick="openBizDoc(''+CAST(OM.numOppId AS VARCHAR)+'',''+CAST(OB.numOppBizDocsId AS VARCHAR)+'')">A/R Invoice Due</a>'' As TaskTypeName,
10 AS TaskType,
'''' AS OrigDescription,
''<b>For : </b> <a href="javascript:void(0)" class="underLineHyperLink" onclick="openBizDoc(''+CAST(OM.numOppId AS VARCHAR)+'',''+CAST(OB.numOppBizDocsId AS VARCHAR)+'')">''+CAST(OB.[vcBizDocID] AS VARCHAR)+''</a> of <a href="javascript:void(0)" class="underLineHyperLink" onclick="openSalesOrder(''+CAST(OM.numOppId AS VARCHAR)+'')">''+CAST(OM.[vcPOppName] AS VARCHAR)+''</a> <b>Balance Due</b> <a href="javascript:void(0)" onclick="OpenAmtPaid(''+CAST(OM.numOppId AS VARCHAR)+'',''+CAST(OB.numOppBizDocsId AS VARCHAR)+'',''+CAST(OM.[numDivisionId] AS VARCHAR)+'')"   class="underLineHyperLink">'' + CAST(ISNULL(C.[varCurrSymbol],'''') AS VARCHAR) + '''' + CAST(CAST(isnull(OB.monDealAmount  - ISNULL(TablePayments.monPaidAmount,0),0) AS DECIMAL(18,2)) AS VARCHAR)+''</a>''  AS Description,
 CASE ISNULL(bitBillingTerms,0) 
                          WHEN 1 THEN [dbo].[FormatedDateFromDate](DATEADD(dd,convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0)),OB.dtFromDate),
                                                                   1)
                          WHEN 0 THEN [dbo].[FormatedDateFromDate](ob.[dtFromDate],1)
                        END as DueDate,
0 As TotalProgress,
'''' As numAssignedTo,
0 As numAssignToId,
'''' As Priority, -- Priority column   
'''' As Activity ,
0 As PriorityId, -- Priority column   
 0 As ActivityId ,
 ''<a href="../account/frmAccounts.aspx?DivID=''+CAST(DM.numDivisionID AS VARCHAR)+''">''+Com.vcCompanyName+ CASE WHEN  ISNULL(listCompanyRating.VcData,'''')<>'''' THEN ''(''+listCompanyRating.VcData+'')'' ELSE '''' END +''</a>''  AS OrgName,
 listCompanyRating.VcData As CompanyRating

 FROM   [OpportunityMaster] OM INNER JOIN [DivisionMaster] DM ON OM.[numDivisionId] = DM.[numDivisionID]
				LEFT JOIN CompanyInfo  Com ON DM.numCompanyID = Com.numCompanyId    
				LEFT JOIN ListDetails AS listCompanyRating
 ON Com.numCompanyRating=listCompanyRating.NumlistItemID
               LEFT OUTER JOIN OpportunityBizDocs OB ON OB.numOppId = OM.numOppID
               LEFT OUTER JOIN [Currency] C ON OM.[numCurrencyID] = C.[numCurrencyID]
			   OUTER APPLY
			   (
					SELECT 
						SUM(monAmountPaid) AS monPaidAmount
					FROM
						DepositeDetails DD
					INNER JOIN
						DepositMaster DM
					ON
						DD.numDepositID=DM.numDepositId
					WHERE 
						numOppID = OM.numOppID
						AND numOppBizDocsID=OB.numOppBizDocsID
						AND CONVERT(DATE,DM.dtDepositDate) <= DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +','''+Cast(@endDate as varchar(100))+''')
				) TablePayments
        WHERE  OM.[tintOppType] = 1
               AND OM.[tintOppStatus] = 1
               AND om.[numDomainId] = ' + Cast(@numDomainId as varchar(10))  + '
               AND OB.[numBizDocId] = '+CAST(@AuthoritativeSalesBizDocId As VARCHAR)+'
               AND OB.bitAuthoritativeBizDocs=1
               AND ISNULL(OB.tintDeferred,0) <> 1
			   AND (OM.numAccountClass= 0 OR 0=0)
			   AND CONVERT(DATE,OB.[dtCreatedDate]) <= DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +','''+Cast(@endDate as varchar(100))+''')
               AND isnull(OB.monDealAmount
                            ,0) > 0  AND isnull(OB.monDealAmount * OM.fltExchangeRate
                        - ISNULL(TablePayments.monPaidAmount,0) * OM.fltExchangeRate,0) != 0
	) AS Q
	WHERE  '+@OtherRegularSearch+' AND  (DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',Q.DueDate) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',Q.DueDate) <= '''+Cast(@endDate as varchar(30))+''') 
'+@IndivRecordPaging+'
'
EXEC (@dynamicQuery)
END
------------------------11 - A/P Bill Due------------------------------------
IF(@bitAPBillsDue=1 AND @numUserCntID IN(SELECT * FROM Split(@vchAPBillsDue,',') WHERE Items<>''))
BEGIN   
   DECLARE  @AuthoritativePurchaseBizDocId  AS INTEGER
    SELECT  @AuthoritativePurchaseBizDocId= isnull(numAuthoritativePurchase,0)
    FROM   AuthoritativeBizDocs
    WHERE  numDomainId =  @numDomainId
	

SET @dynamicQuery = '  INSERT INTO #tempRecords(
RecordCount,
RecordId,
numCreatedBy,
numOrgTerId,
numTaskId,
IsTaskClosed,
TaskTypeName,
TaskType,
OrigDescription,
Description,
DueDate,
TotalProgress,
numAssignedTo,
numAssignToId,
Priority,
Activity,
PriorityId,
ActivityId,
OrgName,
CompanyRating
) SELECT  COUNT(*) OVER() RecordCount,* FROM (SELECT
OM.numOppId AS RecordId,
OM.numCreatedBy AS numCreatedBy,
ISNULL(DM.numTerId,0) AS numOrgTerId,
OB.numOppBizDocsId AS numTaskId,
0 AS IsTaskClosed,
''<a href="javascript:void(0)" onclick="openPurchaseBill(''+CAST(DM.[numDivisionID] As VARCHAR)+'')">A/P Bill Due<a/>'' As TaskTypeName,
11 AS TaskType,
'''' AS OrigDescription,
''<b>For : </b> <a href="javascript:void(0)" class="underLineHyperLink" onclick="openBizDoc(''+CAST(OM.numOppId AS VARCHAR)+'',''+CAST(OB.numOppBizDocsId AS VARCHAR)+'')">''+CAST(OB.[vcBizDocID] AS VARCHAR)+''</a> of <a href="javascript:void(0)" class="underLineHyperLink" onclick="openSalesOrder(''+CAST(OM.numOppId AS VARCHAR)+'')">''+CAST(OM.[vcPOppName] AS VARCHAR)+''</a> <b>Balance Due : </b> <a href="javascript:void(0)" onclick="openBizDoc(''+CAST(OM.numOppId AS VARCHAR)+'',''+CAST(OB.numOppBizDocsId AS VARCHAR)+'')"   class="underLineHyperLink">'' + CAST(ISNULL(C.[varCurrSymbol],'''') AS VARCHAR) + '''' + CAST(CAST(isnull((OB.monDealAmount * OM.fltExchangeRate)  - (ISNULL(TablePayment.monPaidAmount,0)* OM.fltExchangeRate),0) AS DECIMAL(18,2))  AS VARCHAR)+''</a>''  AS Description,
 CASE ISNULL(bitBillingTerms,0) 
                          WHEN 1 THEN [dbo].[FormatedDateFromDate](DATEADD(dd,convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0)),OB.dtFromDate),
                                                                   1)
                          WHEN 0 THEN [dbo].[FormatedDateFromDate](ob.[dtFromDate],1)
                        END as DueDate,
0 As TotalProgress,
'''' As numAssignedTo,
0 As numAssignToId,
'''' As Priority, -- Priority column   
'''' As Activity ,
0 As PriorityId, -- Priority column   
 0 As ActivityId ,
 ''<a href="../account/frmAccounts.aspx?DivID=''+CAST(DM.numDivisionID AS VARCHAR)+''">''+Com.vcCompanyName+ CASE WHEN  ISNULL(listCompanyRating.VcData,'''')<>'''' THEN ''(''+listCompanyRating.VcData+'')'' ELSE '''' END +''</a>''  AS OrgName,
 listCompanyRating.VcData As CompanyRating

   FROM   [OpportunityMaster] OM
               INNER JOIN [DivisionMaster] DM
                 ON OM.[numDivisionId] = DM.[numDivisionID]
				 LEFT JOIN CompanyInfo  Com ON DM.numCompanyID = Com.numCompanyId    
				LEFT JOIN ListDetails AS listCompanyRating
 ON Com.numCompanyRating=listCompanyRating.NumlistItemID
               LEFT OUTER JOIN OpportunityBizDocs OB
                 ON OB.numOppId = OM.numOppID
               LEFT OUTER JOIN [Currency] C
                 ON OM.[numCurrencyID] = C.[numCurrencyID]
				 OUTER APPLY
				(
					SELECT
						SUm(BPD.monAmount) monPaidAmount
					FROM	
						BillPaymentDetails BPD
					INNER JOIN
						BillPaymentHeader BPH
					ON
						BPD.numBillPaymentID=BPH.numBillPaymentID
					WHERE
						BPD.numOppBizDocsID = OB.numOppBizDocsId
						AND CAST(BPH.dtPaymentDate AS DATE) <= DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +','''+Cast(@endDate as varchar(100))+''')
				) TablePayment
        WHERE  OM.[tintOppType] = 2
               AND OM.[tintOppStatus] = 1
               AND om.[numDomainId] = 1
               AND OB.[numBizDocId] = '+CAST(@AuthoritativePurchaseBizDocId AS VARCHAR)+' 
               AND OB.bitAuthoritativeBizDocs=1
               AND ISNULL(OB.tintDeferred,0) <> 1
			   AND (OM.numAccountClass= 0 OR 0=0)
			   AND CONVERT(DATE,OB.[dtCreatedDate]) <= DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +','''+Cast(@endDate as varchar(100))+''')
               AND  isnull(OB.monDealAmount * OM.fltExchangeRate,0) > 0 
			   AND isnull(OB.monDealAmount * OM.fltExchangeRate
                        - ISNULL(TablePayment.monPaidAmount,0) * OM.fltExchangeRate,0) > 0
	) AS Q
	WHERE  '+@OtherRegularSearch+' AND  (DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',Q.DueDate) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',Q.DueDate) <= '''+Cast(@endDate as varchar(30))+''') 
'+@IndivRecordPaging+'
'

EXEC (@dynamicQuery)
END

END

SET @FormattedItems =''
SET @FormattedItems=' UPDATE #tempRecords SET DueDate = CASE WHEN convert(varchar(11),DueDate)=convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',getdate())) then ''<b><font color=red>Today</font></b>''
WHEN convert(varchar(11),DueDate)=convert(varchar(11),DateAdd(day,-1,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',getdate()))) then ''<b><font color=purple>Yesterday</font></b>''
WHEN convert(varchar(11),DueDate)=convert(varchar(11),DateAdd(day,1,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',getdate()))) then ''<b><font color=orange>Tommorow</font></b>''
ELSE  dbo.FormatedDateFromDate(DueDate,'+ CONVERT(VARCHAR(10),@numDomainId)+') END '

 EXEC (@FormattedItems)
 DECLARE @strSql3 VARCHAR(MAX)=''
 IF LEN(@columnName) > 0
BEGIN
	SET @strSql3= CONCAT(@strSql3,' order by ',CASE WHEN @columnName = 'Action-Item Participants' THEN CONCAT('[','Action-Item Participants',']') ELSE @columnName END,' ',@columnSortOrder)
END
ELSE
BEGIN
	SET @strSql3= CONCAT(@strSql3,' order by DueDate Asc  ')
END
PRINT @strSql3
SET @TotalRecordCount=(SELECT ISNULL(MAX(RecordCount),0) FROM #tempRecords)
SET @dynamicQuery = 'SELECT '+CAST(@TotalRecordCount AS varchar(400))+' AS TotalRecords,T.*
 FROM #tempRecords T WHERE 1=1 '+@strSql3


EXEC(@dynamicQuery)
DECLARE @TempColumns TABLE
	(
        vcFieldName VARCHAR(200)
        ,vcDbColumnName VARCHAR(200)
        ,vcOrigDbColumnName VARCHAR(200)
        ,bitAllowSorting BIT
        ,numFieldId INT
        ,bitAllowEdit BIT
        ,bitCustomField BIT
        ,vcAssociatedControlType VARCHAR(50)
        ,vcListItemType  VARCHAR(200)
        ,numListID INT
        ,ListRelID INT
        ,vcLookBackTableName VARCHAR(200)
        ,bitAllowFiltering BIT
        ,vcFieldDataType  VARCHAR(20)
		,intColumnWidth INT
		,bitClosedColumn BIT
		,bitFieldMessage BIT DEFAULT 0
		,vcFieldMessage VARCHAR(500)
		,bitIsRequired BIT DEFAULT 0
		,bitIsNumeric BIT DEFAULT 0
		,bitIsAlphaNumeric BIT DEFAULT 0
		,bitIsEmail BIT DEFAULT 0
		,bitIsLengthValidation  BIT DEFAULT 0
		,intMaxLength INT DEFAULT 0
		,intMinLength INT DEFAULT 0
	)



		declare @Nocolumns as tinyint         
set @Nocolumns=0       
         
Select @Nocolumns=isnull(count(*),0) from View_DynamicColumns where numFormId=43 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 
  
  
if @Nocolumns > 0            
begin      
INSERT INTO #tempForm
select tintRow+1 as tintOrder,vcDbColumnName,vcFieldDataType,vcOrigDbColumnName,bitAllowFiltering,ISNULL(vcCultureFieldName,vcFieldName),                  
 vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName                                                 
,bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth
 FROM View_DynamicColumns 
 where numFormId=43 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 
  AND ISNULL(bitSettingField,0)=1 AND ISNULL(bitCustom,0)=0
  order by tintOrder asc  
  
end            
else            
begin 

	--Check if Master Form Configuration is created by administrator if NoColumns=0
	DECLARE @IsMasterConfAvailable BIT = 0
	DECLARE @numUserGroup NUMERIC(18,0)

	SELECT @numUserGroup = numGroupID FROM UserMaster WHERE numUserDetailId = @numUserCntID 

	IF (SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = @numDomainID AND numFormID = 43 AND bitGridConfiguration = 1 AND numGroupID = @numUserGroup) > 0
	BEGIN
		SET @IsMasterConfAvailable = 1
	END

	--If MasterConfiguration is available then load it otherwise load default columns
	IF @IsMasterConfAvailable = 1
	BEGIN
		INSERT INTO DycFormConfigurationDetails 
		(
			numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth
		)
		SELECT 
			43,numFieldId,intColumnNum,intRowNum,@numDomainID,@numUserCntID,NULL,1,0,intColumnWidth
		FROM 
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId=43 AND 
			View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
			ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0

		INSERT INTO #tempForm
		SELECT 
			(intRowNum + 1) as tintOrder, vcDbColumnName,vcFieldDataType,vcOrigDbColumnName,bitAllowFiltering, ISNULL(vcCultureFieldName,vcFieldName), vcAssociatedControlType,
			vcListItemType,numListID,vcLookBackTableName, bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth
		FROM 
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId=43 AND 
			View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
			ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
	ORDER BY 
		tintOrder ASC  
	END
	ELSE                                        
	BEGIN
		 INSERT INTO #tempForm
		select tintOrder,vcDbColumnName,vcFieldDataType
		,vcOrigDbColumnName,bitAllowFiltering,ISNULL(vcCultureFieldName,vcFieldName),                  
		 vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName                                                 
		,bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
		bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
		intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth
		 FROM View_DynamicDefaultColumns
		 where numFormId=43 and bitDefault=1 AND ISNULL(bitSettingField,0)=1 AND numDomainID=@numDomainID
		order by tintOrder asc  
	END 
enD

DELETE FROM #tempForm WHERE vcOrigDbColumnName IN('numActivity','numStatus','intTotalProgress','textDetails','vcCompanyName','bitTask','numAssignedTo','FormattedDate')
--UPDATE #tempForm SET bitCustomField=1
INSERT INTO #tempForm
		(
			tintOrder
			,vcFieldName
			,vcDbColumnName
			,vcOrigDbColumnName
			,bitAllowSorting
			,numFieldId
			,bitAllowEdit
			,bitCustomField
			,vcAssociatedControlType
			,vcListItemType
			,numListID
			,ListRelID
			,vcLookBackTableName
			,bitAllowFiltering
			,vcFieldDataType
			
		)
		VALUES
		(-9,'Type','TaskTypeName','TaskTypeName',0,1,0,0,'SelectBox','',0,0,'T',1,'V')
		,(-8,'Descriptions & Comments','Description','Description',0,184,1,0,'TextArea','',0,0,'T',1,'V')
		,(-7,'Due Date','DueDate','DueDate',0,3,0,0,'DateField','',0,0,'T',0,'V')
		,(-6,'Total Progress','TotalProgress','TotalProgress',1,4,0,0,'TextBox','',0,0,'T',0,'V')
		,(-5,'Assigned to','numAssignedTo','numAssignedTo',0,5,0,0,'SelectBox','',0,0,'T',1,'V')
		,(-4,'Priority','Priority','Priority',0,183,1,0,'SelectBox','',0,0,'T',1,'V')
		,(-3,'Activity','Activity','Activity',0,182,1,0,'SelectBox','',0,0,'T',1,'V')
		,(-2,'Organization (Rating)','OrgName','OrgName',0,8,0,0,'TextBox','',0,0,'T',1,'V')
UPDATE
		TC
	SET
		TC.intColumnWidth = ISNULL(DFCD.intColumnWidth,0)
	FROM
		#tempForm TC
	INNER JOIN
		DycFormConfigurationDetails DFCD
	ON
		TC.numFieldId = DFCD.numFieldId
	WHERE
		DFCD.numDomainId = @numDomainID
		AND DFCD.numUserCntID = @numUserCntID
		AND DFCD.numFormId = 43
		AND DFCD.tintPageType = 1 


	SELECT * FROM #tempForm order by tintOrder asc  
END
GO

/****** Object:  Trigger [dbo].[ES_ItemCategory_ID]    Script Date: 27/04/2020 14:26:10 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE TRIGGER [dbo].[ES_ItemCategory_ID]
ON [dbo].[ItemCategory]
AFTER INSERT, DELETE
AS 
BEGIN
    SET NOCOUNT ON;

	DECLARE @bitElasticSearch BIT

	SET @bitElasticSearch = ISNULL((SELECT TOP 1 ECD.bitElasticSearch FROM 
	(SELECT * FROM INSERTED UNION SELECT * FROM DELETED) T
	INNER JOIN Item i ON (i.numItemCode = T.numItemID)
	INNER JOIN eCommerceDTL ECD ON (ECD.numDomainId = i.numDomainID AND ECD.bitElasticSearch = 1)),0)

	IF(@bitElasticSearch = 1)
	BEGIN
		DECLARE @action as char(1) = 'I';

		IF EXISTS(SELECT * FROM DELETED)
		BEGIN
			SET @action = CASE WHEN EXISTS(SELECT * FROM INSERTED) THEN 'U' ELSE 'D' END
		END
		ELSE IF NOT EXISTS(SELECT * FROM INSERTED) 
		BEGIN
			RETURN; -- Nothing updated or inserted.
		END

		IF @action = 'I'
		BEGIN
			INSERT INTO ElasticSearchBizCart(vcModule,vcValue, vcAction, numDomainID)--, numSiteID) 
			SELECT 'Item',Item.numItemCode,'Insert',Item.numDomainID--,S.numSiteID 
			FROM Item  
			INNER JOIN INSERTED i ON (i.numItemID = Item.numItemCode)
		END
		ELSE 
		BEGIN
			IF(NOT EXISTS(SELECT 1 FROM ElasticSearchBizCart WHERE vcModule = 'Item' AND vcValue IN (SELECT numItemID FROM deleted)))
			BEGIN	
				INSERT INTO ElasticSearchBizCart(vcModule,vcValue, vcAction, numDomainID) 
				SELECT 'Item',Item.numItemCode,'Update',Item.numDomainID
				FROM Item  
				INNER JOIN DELETED i ON (i.numItemID = Item.numItemCode)

			END
		END
	END
END
GO

ALTER TABLE [dbo].[ItemCategory] ENABLE TRIGGER [ES_ItemCategory_ID]
GO


/****** Object:  StoredProcedure [dbo].[Usp_cflDeleteField]    Script Date: 07/26/2008 16:15:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec Usp_cflDeleteField 2  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_cfldeletefield')
DROP PROCEDURE usp_cfldeletefield
GO
CREATE PROCEDURE [dbo].[Usp_cflDeleteField]      
@fld_id as numeric(9)=NULL,
@numDomainID AS NUMERIC
as      
      
IF @numDomainID >0
BEGIN

	IF (EXISTS (SELECT 1 FROM CFW_Fld_Master where fld_id=@fld_id AND Grp_id IN (5,9)))
	BEGIN
		INSERT INTO ElasticSearchBizCart (numDomainID,vcValue,vcAction,vcModule)
		SELECT @numDomainID,
		CASE 
			WHEN Grp_id=5 THEN REPLACE(Fld_label,' ','') + '_C'
			WHEN Grp_id=9 THEN REPLACE(Fld_label,' ','') + '_' + CAST(Fld_Id AS VARCHAR)
		END
		,'Delete'
		,'CustomField'
		FROM CFW_Fld_Master where fld_id=@fld_id AND Grp_id IN (5,9)
	END
	

	DELETE FROM DycFormConfigurationDetails WHERE bitCustom =1 AND numFieldID = @fld_id AND numDomainId=@numDomainID -- Bug id 278 permenant fix 

	delete from listdetails where numlistID=(select numlistID from  CFW_Fld_Master where fld_id=@fld_id)    
	delete from listmaster where numListID=(select numlistID from  CFW_Fld_Master where fld_id=@fld_id)
	delete from CFW_Fld_Values_Product where Fld_ID=@fld_id   
	delete from CFW_FLD_Values where Fld_ID=@fld_id    
	delete from CFW_FLD_Values_Case where fld_id=@fld_id    
	delete from CFW_FLD_Values_Cont where fld_id=@fld_id    
	delete from CFW_FLD_Values_Item where fld_id=@fld_id      
	delete from CFW_Fld_Values_Opp where fld_id=@fld_id 
	DELETE FROM [dbo].[CFW_Fld_Values_OppItems] where fld_id=@fld_id
	DELETE FROM ItemAttributes WHERE FLD_ID=@fld_id
	
	DELETE FROM dbo.CFW_Validation WHERE numFieldID = @fld_id 
	delete from CFW_Fld_Dtl where numFieldId=@fld_id     
	delete from CFW_Fld_Master where fld_id=@fld_id
	
				

END       
GO
/****** Object:  StoredProcedure [dbo].[USP_cflManage]    Script Date: 07/26/2008 16:15:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Anoop Jayaraj          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_cflmanage')
DROP PROCEDURE usp_cflmanage
GO
CREATE PROCEDURE [dbo].[USP_cflManage]          
@locid as tinyint=0,          
@fldtype as varchar (30)='',          
@fldlbl as varchar(50)='',          
@FieldOdr as tinyint=0,          
@userId as numeric(9)=0,          
@TabId as numeric(9)=0,          
@fldId as numeric(9) OUTPUT,      
@numDomainID as numeric(9)=0,  
@vcURL as varchar(1000)='',
@vcToolTip AS VARCHAR(1000)='',         
@ListId as numeric(9) OUTPUT,
@bitAutocomplete AS BIT
AS
BEGIN          
     
if @fldId =0          
begin          
  --declare @ListId as numeric(9)          
            
            
  if @fldtype='SelectBox' OR @fldtype='CheckBoxList'          
  begin          
  INSERT INTO ListMaster           
   (          
   vcListName,           
   numCreatedBy,           
   bintCreatedDate,           
   bitDeleted,           
   bitFixed ,    
   numModifiedBy,    
   bintModifiedDate,    
   numDomainID,    
   bitFlag,numModuleId         
   )           
   values          
   (          
   @fldlbl,          
   1,          
   getutcdate(),          
   0,          
   0,    
   1,    
   getutcdate(),     
   @numDomainID,    
   0,8        
   )          
            
  SET @ListId = SCOPE_IDENTITY()
         
  end          
            
  Insert into cfw_fld_master          
   (          
   Fld_type,          
   Fld_label,          
   Fld_tab_ord,          
   Grp_id,          
   subgrp,          
   numlistid,      
   numDomainID,  
	   vcURL,vcToolTip,bitAutocomplete         
   )           
   values          
   (          
   @fldtype,          
   @fldlbl,          
   @FieldOdr,          
   @locid,          
   @TabId,          
   @ListId,      
   @numDomainID,  
   @vcURL,@vcToolTip, @bitAutoComplete          
   )       
   
  SET @fldId = SCOPE_IDENTITY() --(SELECT MAX(fld_id) FROM dbo.CFW_Fld_Master WHERE numDomainID = @numDomainID)
   
   IF (EXISTS (SELECT 1 FROM CFW_Fld_Master where fld_id=@fldId AND Grp_id IN (5,9)))
	BEGIN
		INSERT INTO ElasticSearchBizCart (numDomainID,vcValue,vcAction,vcModule)
		SELECT @numDomainID,
		CASE 
			WHEN Grp_id=5 THEN REPLACE(Fld_label,' ','') + '_C'
			WHEN Grp_id=9 THEN REPLACE(Fld_label,' ','') + '_' + CAST(Fld_Id AS VARCHAR)
		END
		,'Insert'
		,'CustomField'
		FROM CFW_Fld_Master where fld_id=@fldId AND Grp_id IN (5,9)
	END

end          
          
else           
begin          
 if @fldtype='SelectBox' OR @fldtype='CheckBoxList'          
  begin         
    
    declare @ddlName as varchar(100)        
  select @ddlName=Fld_label from cfw_fld_master where Fld_id=@fldId  and numDomainID =@numDomainID     
         
  update  ListMaster set  vcListName=@fldlbl where vcListName=@ddlName  and    numDomainID =@numDomainID     
        
  end  
  
      
	IF (EXISTS (SELECT 1 FROM CFW_Fld_Master where fld_id=@fldId AND Grp_id IN (5,9)))
	BEGIN
		IF(@locid NOT IN (5,9))
		BEGIN
			INSERT INTO ElasticSearchBizCart (numDomainID,vcValue,vcAction,vcModule)
			SELECT @numDomainID,
			CASE 
				WHEN Grp_id=5 THEN REPLACE(Fld_label,' ','') + '_C'
				WHEN Grp_id=9 THEN REPLACE(Fld_label,' ','') + '_' + CAST(Fld_Id AS VARCHAR)
			END
			,'Delete'
			,'CustomField'
			FROM CFW_Fld_Master where fld_id=@fldId AND Grp_id IN (5,9)
		END
		ELSE
		BEGIN
			IF(NOT EXISTS (SELECT 1 FROM CFW_Fld_Master where fld_id=@fldId AND Grp_id = @locid AND Fld_label = @fldlbl))
			BEGIN
				DECLARE @UpdateCustomField AS VARCHAR(100)  
				SELECT @UpdateCustomField = CASE 
					WHEN @locid=5 THEN REPLACE(@fldlbl,' ','') + '_C' 
					WHEN @locid=9 THEN REPLACE(@fldlbl,' ','') + '_' + CAST(@fldId AS VARCHAR)
				END

				INSERT INTO ElasticSearchBizCart (numDomainID,vcValue,vcAction,vcModule)
				SELECT @numDomainID,
				CASE 
					WHEN Grp_id=5 THEN REPLACE(Fld_label,' ','') + '_C'  
					WHEN Grp_id=9 THEN REPLACE(Fld_label,' ','') + '_' + CAST(Fld_Id AS VARCHAR) 
				END + '||' + @UpdateCustomField
				,'Update'
				,'CustomField'
				FROM CFW_Fld_Master where fld_id=@fldId AND Grp_id IN (5,9)
			END
		END
	END	
	ELSE IF(@locid IN (5,9))
	BEGIN
		INSERT INTO ElasticSearchBizCart (numDomainID,vcValue,vcAction,vcModule)
		SELECT @numDomainID,
		CASE 
			WHEN @locid=5 THEN REPLACE(@fldlbl,' ','') + '_C'
			WHEN @locid=9 THEN REPLACE(@fldlbl,' ','') + '_' + CAST(@fldId AS VARCHAR)
		END
		,'Insert'
		,'CustomField'
	END
        
 update cfw_fld_master set           
          
  Fld_label=@fldlbl,          
  Grp_id=@locid,          
  subgrp=@TabId,  
  vcURL= @vcURL,vcToolTip=@vcToolTip, bitAutoComplete = @bitAutoComplete        
 where Fld_id=@fldId and   numDomainID =@numDomainID         
            
    
END
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DemandForecast_Execute')
DROP PROCEDURE dbo.USP_DemandForecast_Execute
GO
CREATE PROCEDURE [dbo].[USP_DemandForecast_Execute]
	@numDFID NUMERIC (18,0)
	,@vcSelectedIDs VARCHAR(MAX)
	,@numUserCntID NUMERIC(18,0)
	,@ClientTimeZoneOffset INT
	,@CurrentPage INT
    ,@PageSize INT
	,@columnName VARCHAR(MAX)                                                          
	,@columnSortOrder VARCHAR(10)
	,@numDomainID NUMERIC(18,0)
	,@vcRegularSearchCriteria VARCHAR(MAX)=''
	,@bitShowAllItems BIT
	,@bitShowHistoricSales BIT
	,@numHistoricalAnalysisPattern BIT
	,@bitBasedOnLastYear BIT
	,@bitIncludeOpportunity BIT
	,@numOpportunityPercentComplete NUMERIC(18,0)
	,@tintDemandPlanBasedOn TINYINT
	,@tintPlanType TINYINT
AS 
BEGIN
	DECLARE @dtLastExecution DATETIME
	DECLARE @bitWarehouseFilter BIT = 0
	DECLARE @bitItemClassificationFilter BIT = 0
	DECLARE @bitItemGroupFilter BIT = 0

	IF (SELECT COUNT(*) FROM DemandForecast WHERE numDFID = @numDFID) > 0
	BEGIN
		IF (SELECT COUNT(*) FROM DemandForecastWarehouse WHERE numDFID = @numDFID) > 0
		BEGIN
			SET @bitWarehouseFilter = 1
		END

		IF (SELECT COUNT(*) FROM DemandForecastItemClassification WHERE numDFID = @numDFID) > 0
		BEGIN
			SET @bitItemClassificationFilter = 1
		END

		IF (SELECT COUNT(*) FROM DemandForecastItemGroup WHERE numDFID = @numDFID) > 0
		BEGIN
			SET @bitItemGroupFilter = 1
		END				
	END

	DECLARE @tintUnitsRecommendationForAutoPOBackOrder TINYINT
	DECLARE @bitIncludeRequisitions BIT

	SELECT 
		@tintUnitsRecommendationForAutoPOBackOrder=ISNULL(tintUnitsRecommendationForAutoPOBackOrder,1)
		,@bitIncludeRequisitions=ISNULL(bitIncludeRequisitions,0)
	FROM 
		Domain 
	WHERE 
		numDomainID = @numDomainID

	DECLARE @TEMP TABLE
	(
		numItemCode NUMERIC(18,0)
		,numWarehouseItemID NUMERIC(18,0)
		,numUnitHour FLOAT
		,dtReleaseDate DATE
	)

	INSERT INTO @TEMP
	(
		numItemCode
		,numWarehouseItemID
		,numUnitHour
		,dtReleaseDate
	)
	SELECT
		Item.numItemCode
		,OpportunityItems.numWarehouseItmsID
		,OpportunityItems.numUnitHour - ISNULL(OpportunityItems.numQtyShipped,0)
		,ISNULL(OpportunityItems.ItemReleaseDate,OpportunityMaster.dtReleaseDate)
	FROM
		OpportunityMaster
	INNER JOIN
		OpportunityItems
	ON
		OpportunityMaster.numOppId = OpportunityItems.numOppId
	INNER JOIN
		Item
	ON
		OpportunityItems.numItemCode = Item.numItemCode
	INNER JOIN
		WareHouseItems
	ON
		OpportunityItems.numWarehouseItmsID = WareHouseItems.numWareHouseItemID
	WHERE
		OpportunityMaster.numDomainId=@numDomainID
		AND OpportunityMaster.tintOppType=1
		AND OpportunityMaster.tintOppStatus=1
		AND ISNULL(OpportunityMaster.tintshipped,0)=0
		AND (ISNULL(OpportunityItems.numUnitHour,0)  - ISNULL(OpportunityItems.numQtyShipped,0)) > 0
		AND ISNULL(OpportunityItems.bitDropShip,0) = 0
		AND ISNULL(Item.IsArchieve,0) = 0
		AND ISNULL(Item.bitAssembly,0) = (CASE WHEN @tintPlanType=2 THEN 1 ELSE 0 END)
		AND ISNULL(Item.bitKitParent,0) = 0
		AND (OpportunityItems.ItemReleaseDate IS NOT NULL OR OpportunityMaster.dtReleaseDate IS NOT NULL)
		AND 1 = (CASE WHEN LEN(ISNULL(@vcSelectedIDs,'')) > 0 
					THEN (CASE WHEN Item.numItemCode IN (SELECT Id FROM dbo.SplitIDs(@vcSelectedIDs,',')) THEN 1 ELSE 0 END) 
					ELSE 1 
				END)
		AND
		(
			@bitWarehouseFilter = 0 OR
			WareHouseItems.numWareHouseID IN (SELECT numWareHouseID FROM DemandForecastWarehouse WHERE numDFID =@numDFID)
		)
		AND
		(
			@bitItemClassificationFilter = 0 OR
			Item.numItemClassification IN (SELECT numItemClassificationID FROM DemandForecastItemClassification WHERE numDFID = @numDFID)
		)
		AND
		(
			@bitItemGroupFilter = 0 OR
			Item.numItemGroup IN (SELECT numItemGroupID FROM DemandForecastItemGroup WHERE numDFID = @numDFID)
		)

	INSERT INTO @TEMP
	(
		numItemCode
		,numWarehouseItemID
		,numUnitHour
		,dtReleaseDate
	)
	SELECT
		Item.numItemCode
		,OKI.numWareHouseItemId
		,OKI.numQtyItemsReq - ISNULL(OKI.numQtyShipped,0)
		,ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate)
	FROM
		OpportunityMaster OM
	INNER JOIN
		OpportunityItems OI
	ON
		OM.numOppId = OI.numOppId
	INNER JOIN
		OpportunityKitItems OKI
	ON
		OI.numoppitemtCode=OKI.numOppItemID
	INNER JOIN
		Item
	ON
		OKI.numChildItemID = Item.numItemCode
	INNER JOIN
		WareHouseItems WI
	ON
		OKI.numWareHouseItemId = WI.numWareHouseItemID
	WHERE
		OM.numDomainId=@numDomainID
		AND OM.tintOppType=1
		AND OM.tintOppStatus=1
		AND ISNULL(OM.tintshipped,0)=0
		AND ISNULL(OI.bitDropShip,0) = 0
		AND ISNULL(OI.bitWorkOrder,0) = 0
		AND ISNULL(Item.IsArchieve,0) = 0
		AND ISNULL(Item.bitAssembly,0) = (CASE WHEN @tintPlanType=2 THEN 1 ELSE 0 END)
		AND ISNULL(Item.bitKitParent,0) = 0
		AND (ISNULL(OKI.numQtyItemsReq,0) - ISNULL(OKI.numQtyShipped,0)) > 0
		AND (OM.dtReleaseDate IS NOT NULL OR OI.ItemReleaseDate IS NOT NULL)
		AND 1 = (CASE WHEN LEN(ISNULL(@vcSelectedIDs,'')) > 0 
					THEN (CASE WHEN Item.numItemCode IN (SELECT Id FROM dbo.SplitIDs(@vcSelectedIDs,',')) THEN 1 ELSE 0 END) 
					ELSE 1 
				END)
		AND
		(
			@bitWarehouseFilter = 0 OR
			WI.numWareHouseID IN (SELECT numWareHouseID FROM DemandForecastWarehouse WHERE numDFID =@numDFID)
		)
		AND
		(
			@bitItemClassificationFilter = 0 OR
			Item.numItemClassification IN (SELECT numItemClassificationID FROM DemandForecastItemClassification WHERE numDFID = @numDFID)
		)
		AND
		(
			@bitItemGroupFilter = 0 OR
			Item.numItemGroup IN (SELECT numItemGroupID FROM DemandForecastItemGroup WHERE numDFID = @numDFID)
		)

	INSERT INTO @TEMP
	(
		numItemCode
		,numWarehouseItemID
		,numUnitHour
		,dtReleaseDate
	)
	SELECT
		Item.numItemCode
		,OKCI.numWareHouseItemId
		,OKCI.numQtyItemsReq - ISNULL(OKCI.numQtyShipped,0)
		,ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate)
	FROM
		OpportunityMaster OM
	INNER JOIN
		OpportunityItems OI
	ON
		OM.numOppId = OI.numOppId
	INNER JOIN
		OpportunityKitChildItems OKCI
	ON
		OI.numoppitemtCode = OKCI.numOppItemID
	INNER JOIN
		Item
	ON
		OKCI.numItemID = Item.numItemCode
	INNER JOIN
		WareHouseItems WI
	ON
		OKCI.numWareHouseItemId = WI.numWareHouseItemID
	WHERE
		OM.numDomainId=@numDomainID
		AND OM.tintOppType=1
		AND OM.tintOppStatus=1
		AND ISNULL(OM.tintshipped,0)=0
		AND ISNULL(OI.bitDropShip,0) = 0
		AND ISNULL(OI.bitWorkOrder,0) = 0
		AND ISNULL(Item.IsArchieve,0) = 0
		AND ISNULL(Item.bitAssembly,0) = (CASE WHEN @tintPlanType=2 THEN 1 ELSE 0 END)
		AND ISNULL(Item.bitKitParent,0) = 0
		AND (ISNULL(OKCI.numQtyItemsReq,0) - ISNULL(OKCI.numQtyShipped,0)) > 0
		AND (OM.dtReleaseDate IS NOT NULL OR OI.ItemReleaseDate IS NOT NULL)
		AND 1 = (CASE WHEN LEN(ISNULL(@vcSelectedIDs,'')) > 0 
					THEN (CASE WHEN Item.numItemCode IN (SELECT Id FROM dbo.SplitIDs(@vcSelectedIDs,',')) THEN 1 ELSE 0 END) 
					ELSE 1 
				END)
		AND
		(
			@bitWarehouseFilter = 0 OR
			WI.numWareHouseID IN (SELECT numWareHouseID FROM DemandForecastWarehouse WHERE numDFID =@numDFID)
		)
		AND
		(
			@bitItemClassificationFilter = 0 OR
			Item.numItemClassification IN (SELECT numItemClassificationID FROM DemandForecastItemClassification WHERE numDFID = @numDFID)
		)
		AND
		(
			@bitItemGroupFilter = 0 OR
			Item.numItemGroup IN (SELECT numItemGroupID FROM DemandForecastItemGroup WHERE numDFID = @numDFID)
		)

	INSERT INTO @TEMP
	(
		numItemCode
		,numWarehouseItemID
		,numUnitHour
		,dtReleaseDate
	)
	SELECT
		WOD.numChildItemID
		,WOD.numWareHouseItemId
		,WOD.numQtyItemsReq
		,(CASE WHEN OM.numOppId IS NOT NULL THEN ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate) ELSE WO.dtmEndDate END)
	FROM
		WorkOrderDetails WOD
	INNER JOIN
		Item
	ON
		WOD.numChildItemID = Item.numItemCode
	INNER JOIN
		WareHouseItems WI
	ON
		WOD.numWareHouseItemId = WI.numWareHouseItemID
	INNER JOIN
		WorkOrder WO
	ON
		WOD.numWOId=WO.numWOId
	LEFT JOIN
		OpportunityItems OI
	ON
		WO.numOppItemID = OI.numoppitemtCode
	LEFT JOIN
		OpportunityMaster OM
	ON
		OI.numOppId=OM.numOppId
	WHERE
		WO.numDomainID=@numDomainID
		AND WO.numWOStatus <> 23184 -- NOT COMPLETED
		AND ISNULL(Item.IsArchieve,0) = 0
		AND ISNULL(Item.bitAssembly,0) = (CASE WHEN @tintPlanType=2 THEN 1 ELSE 0 END)
		AND ISNULL(Item.bitKitParent,0) = 0
		AND ISNULL(WOD.numQtyItemsReq,0) > 0
		AND (OM.dtReleaseDate IS NOT NULL OR OI.ItemReleaseDate IS NOT NULL OR WO.dtmEndDate IS NOT NULL)
		AND 1 = (CASE WHEN OM.numOppId IS NOT NULL THEN (CASE WHEN OM.tintOppStatus=1 THEN 1 ELSE 0 END) ELSE 1 END)
		AND 1 = (CASE WHEN LEN(ISNULL(@vcSelectedIDs,'')) > 0 
					THEN (CASE WHEN Item.numItemCode IN (SELECT Id FROM dbo.SplitIDs(@vcSelectedIDs,',')) THEN 1 ELSE 0 END) 
					ELSE 1 
				END)
		AND
		(
			@bitWarehouseFilter = 0 OR
			WI.numWareHouseID IN (SELECT numWareHouseID FROM DemandForecastWarehouse WHERE numDFID =@numDFID)
		)
		AND
		(
			@bitItemClassificationFilter = 0 OR
			Item.numItemClassification IN (SELECT numItemClassificationID FROM DemandForecastItemClassification WHERE numDFID = @numDFID)
		)
		AND
		(
			@bitItemGroupFilter = 0 OR
			Item.numItemGroup IN (SELECT numItemGroupID FROM DemandForecastItemGroup WHERE numDFID = @numDFID)
		)

	CREATE TABLE #TEMPItems
	(
		numItemCode NUMERIC(18,0)
		,numWarehouseItemID NUMERIC(18,0)
		,numUnitHour FLOAT
		,dtReleaseDate VARCHAR(MAX)
		,dtReleaseDateHidden VARCHAR(MAX)
	)

	INSERT INTO #TEMPItems
	(
		numItemCode
		,numWarehouseItemID
		,numUnitHour
		,dtReleaseDate
		,dtReleaseDateHidden
	)
	SELECT
		numItemCode
		,numWarehouseItemID
		,SUM(numUnitHour)
		,STUFF((SELECT 
					CONCAT(', <a href="#" onclick="return OpenDFReleaseDateRecords(',numItemCode,',',numWarehouseItemID,',''',CONVERT(VARCHAR(10), dtReleaseDate, 101),''');">',CONVERT(VARCHAR(10), dtReleaseDate, 101),'</a> (',SUM(numUnitHour),')')
				FROM 
					@TEMP
				WHERE 
					numItemCode=T1.numItemCode
					AND numWarehouseItemID=T1.numWarehouseItemID
				GROUP BY
					numItemCode
					,numWarehouseItemID
					,dtReleaseDate
				FOR XML PATH(''),TYPE).value('(./text())[1]','VARCHAR(MAX)')
				,1,2,'')
		,STUFF((SELECT 
					CONCAT(', ',CONVERT(VARCHAR(10), dtReleaseDate, 101),' (',SUM(numUnitHour),')')
				FROM 
					@TEMP
				WHERE 
					numItemCode=T1.numItemCode
					AND numWarehouseItemID=T1.numWarehouseItemID
				GROUP BY
					numItemCode
					,numWarehouseItemID
					,dtReleaseDate
				FOR XML PATH(''),TYPE).value('(./text())[1]','VARCHAR(MAX)')
				,1,2,'')
	FROM
		@TEMP T1
	GROUP BY
		numItemCode
		,numWarehouseItemID

	---------------------------- Dynamic Query -------------------------------
		
	CREATE TABLE #tempForm 
	(
		tintOrder TINYINT,vcDbColumnName nvarchar(50),vcOrigDbColumnName nvarchar(50),vcFieldName nvarchar(50),vcAssociatedControlType nvarchar(50),vcListItemType varchar(3),
		numListID numeric(9),vcLookBackTableName varchar(50),bitCustomField BIT,numFieldId NUMERIC,bitAllowSorting BIT,bitAllowEdit BIT,bitIsRequired BIT,bitIsEmail BIT,
		bitIsAlphaNumeric BIT,bitIsNumeric BIT,bitIsLengthValidation BIT,intMaxLength int,intMinLength int,bitFieldMessage BIT,vcFieldMessage VARCHAR(500),
		ListRelID numeric(9),intColumnWidth int,bitAllowFiltering BIT,vcFieldDataType CHAR(1)
	)

	-- FIRST CHECK IF GRID COLUMNS ARE CONFIGURED
	DECLARE  @Nocolumns  AS TINYINT = 0

	SELECT
		@Nocolumns=ISNULL(SUM(TotalRow),0) 
	FROM
	(            
		SELECT 
			COUNT(*) TotalRow 
		FROM 
			View_DynamicColumns 
		WHERE 
			numFormId=139 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND numRelCntType = 0
		UNION 
		SELECT 
			COUNT(*) TotalRow 
		FROM 
			View_DynamicCustomColumns 
		WHERE 
			numFormId=139 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND numRelCntType = 0
	) TotalRows

	IF @Nocolumns=0
	BEGIN
		INSERT INTO DycFormConfigurationDetails 
		(
			numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth
		)
		SELECT 
			139,numFieldId,0,Row_number() over(order by tintRow desc),@numDomainID,@numUserCntID,0,1,0,intColumnWidth
		FROM 
			View_DynamicDefaultColumns
		WHERE 
			numFormId=139 
			AND bitDefault=1 
			AND ISNULL(bitSettingField,0)=1 
			AND numDomainID=@numDomainID
		ORDER BY 
			tintOrder asc   
	END

	INSERT INTO 
		#tempForm
	SELECT 
		tintRow+1 as tintOrder,vcDbColumnName,vcOrigDbColumnName,ISNULL(vcCultureFieldName,vcFieldName),                  
		vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName,bitCustom,numFieldId,bitAllowSorting,bitInlineEdit,
		bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength,bitFieldMessage,
		vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType  
	FROM 
		View_DynamicColumns 
	WHERE 
		numFormId=139 
		AND numUserCntID=@numUserCntID 
		AND numDomainID=@numDomainID 
		AND tintPageType=1 
		AND ISNULL(bitSettingField,0)=1 
		AND ISNULL(bitCustom,0)=0  
		AND numRelCntType = 0
	UNION
	SELECT 
		tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName,vcAssociatedControlType,'' as vcListItemType,numListID,'',bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
		bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
		intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,'' 
	FROM 
		View_DynamicCustomColumns
	WHERE 
		numFormId=139 
		AND numUserCntID=@numUserCntID 
		AND numDomainID=@numDomainID 
		AND tintPageType=1 
		AND ISNULL(bitCustom,0)=1 
		AND numRelCntType = 0
	ORDER BY 
		tintOrder asc
					
	DECLARE  @strColumns AS VARCHAR(MAX) = ''
	SET @strColumns = CONCAT(' COUNT(*) OVER() TotalRowsCount, Item.numItemCode, Item.vcItemName, Item.vcSKU
	,ISNULL((SELECT vcUnitName FROM UOM WHERE numUOMId = Item.numBaseUnit),''-'') AS vcUOM
	,Item.charItemType AS vcItemType
	,Item.numAssetChartAcntId
	,Item.monAverageCost
	,Item.numBusinessProcessId
	,SPLM.Slp_Name
	,SPLM.numBuildManager
	,dbo.fn_GetContactName(SPLM.numBuildManager) vcBuildManager
	,(SELECT TOP 1 numContactId FROM AdditionalContactsInformation WHERE numDivisionId = Item.numVendorID ORDER BY ISNULL(bitPrimaryContact,0) DESC) AS numContactID
	,ISNULL(Item.numPurchaseUnit,0) AS numUOM
	,dbo.fn_UOMConversion(ISNULL(numBaseUnit, 0), Item.numItemCode, Item.numDomainId, ISNULL(Item.numPurchaseUnit, 0)) fltUOMConversionfactor
	,dbo.fn_GetAttributes(WI.numWareHouseItemId,1) AS vcAttributes
	,WI.numWareHouseID
	,WI.numWareHouseItemID
	,W.vcWarehouse
	,ISNULL(V.numVendorID,0) numVendorID
	,ISNULL(V.intMinQty,0) intMinQty
	,ISNULL(monCost,0) * dbo.fn_UOMConversion(Item.numBaseUnit,Item.numItemCode,Item.numDomainID,Item.numPurchaseUnit) AS monVendorCost
	,T1.numUnitHour AS numQtyToPurchase, T1.dtReleaseDateHidden, T1.dtReleaseDate,(CASE
		WHEN ',@tintUnitsRecommendationForAutoPOBackOrder,'= 3
		THEN 
			(ISNULL(numBackOrder,0) - ISNULL(numOnOrder,0) + (CASE WHEN ',@bitIncludeRequisitions,' = 1  
											THEN ISNULL((SELECT 
														SUM(numUnitHour) 
													FROM 
														OpportunityItems 
													INNER JOIN 
														OpportunityMaster 
													ON 
														OpportunityItems.numOppID=OpportunityMaster.numOppId 
													WHERE 
														OpportunityMaster.numDomainId=',@numDomainID,'
														AND OpportunityMaster.tintOppType=2 
														AND OpportunityMaster.tintOppStatus=0 
														AND OpportunityItems.numItemCode=Item.numItemCode 
														AND OpportunityItems.numWarehouseItmsID=WI.numWareHouseItemID),0) 
											ELSE 0 
											END)) +
			(CASE WHEN ISNULL(Item.fltReorderQty,0) >= ISNULL(V.intMinQty,0) THEN ISNULL(Item.fltReorderQty,0) ELSE ISNULL(V.intMinQty,0) END) 
		WHEN ',@tintUnitsRecommendationForAutoPOBackOrder,'=2
		THEN 
			(CASE
				WHEN ISNULL(Item.fltReorderQty,0) >= ISNULL(V.intMinQty,0) AND ISNULL(Item.fltReorderQty,0) >= ISNULL(numBackOrder,0) THEN ISNULL(Item.fltReorderQty,0)
				WHEN ISNULL(V.intMinQty,0) >= ISNULL(Item.fltReorderQty,0) AND ISNULL(V.intMinQty,0) >= ISNULL(numBackOrder,0) THEN ISNULL(V.intMinQty,0)
				WHEN ISNULL(numBackOrder,0) >= ISNULL(Item.fltReorderQty,0) AND ISNULL(numBackOrder,0) >= ISNULL(V.intMinQty,0) THEN ISNULL(numBackOrder,0)
				ELSE ISNULL(Item.fltReorderQty,0)
			END) - (ISNULL(numOnOrder,0) + (CASE WHEN ',@bitIncludeRequisitions,' = 1  THEN ISNULL((SELECT 
														SUM(numUnitHour) 
													FROM 
														OpportunityItems 
													INNER JOIN 
														OpportunityMaster 
													ON 
														OpportunityItems.numOppID=OpportunityMaster.numOppId 
													WHERE 
														OpportunityMaster.numDomainId=',@numDomainID,'
														AND OpportunityMaster.tintOppType=2 
														AND OpportunityMaster.tintOppStatus=0 
														AND OpportunityItems.numItemCode=Item.numItemCode 
														AND OpportunityItems.numWarehouseItmsID=WI.numWareHouseItemID),0) ELSE 0 END))
		ELSE
			(CASE WHEN ISNULL(Item.fltReorderQty,0) > ISNULL(V.intMinQty,0) THEN ISNULL(Item.fltReorderQty,0) ELSE ISNULL(V.intMinQty,0) END) 
			+ ISNULL(numBackOrder,0) - (ISNULL(numOnOrder,0) + (CASE WHEN ',@bitIncludeRequisitions,' = 1  THEN ISNULL((SELECT 
																													SUM(numUnitHour) 
																												FROM 
																													OpportunityItems 
																												INNER JOIN 
																													OpportunityMaster 
																												ON 
																													OpportunityItems.numOppID=OpportunityMaster.numOppId 
																												WHERE 
																													OpportunityMaster.numDomainId=',@numDomainID,'
																													AND OpportunityMaster.tintOppType=2 
																													AND OpportunityMaster.tintOppStatus=0 
																													AND OpportunityItems.numItemCode=Item.numItemCode 
																													AND OpportunityItems.numWarehouseItmsID=WI.numWareHouseItemID),0) ELSE 0 END)) 
	END) AS numQtyBasedOnPurchasePlan ')

	--Custom field 
	DECLARE @tintOrder AS TINYINT = 0                                                 
	DECLARE @vcFieldName AS VARCHAR(50)                                                  
	DECLARE @vcListItemType AS VARCHAR(3)                                             
	DECLARE @vcAssociatedControlType VARCHAR(20)     
	declare @numListID AS numeric(9)
	DECLARE @vcDbColumnName VARCHAR(50)                      
	DECLARE @WhereCondition VARCHAR(MAX) = ''                   
	DECLARE @vcLookBackTableName VARCHAR(2000)                
	DECLARE @bitCustom AS BIT
	DECLARE @bitAllowEdit AS CHAR(1)                   
	Declare @numFieldId as numeric  
	DECLARE @bitAllowSorting AS CHAR(1)    
	DECLARE @vcColumnName AS VARCHAR(500)             
	Declare @ListRelID as numeric(9) 
	DECLARE @SearchQuery AS VARCHAR(MAX) = '' 
	   
	SELECT TOP 1 
		@tintOrder=tintOrder+1
		,@vcDbColumnName=vcDbColumnName
		,@vcFieldName=vcFieldName
		,@vcAssociatedControlType=vcAssociatedControlType
		,@vcListItemType=vcListItemType
		,@numListID=numListID
		,@vcLookBackTableName=vcLookBackTableName
		,@bitCustom=bitCustomField
		,@numFieldId=numFieldId
		,@bitAllowSorting=bitAllowSorting
		,@bitAllowEdit=bitAllowEdit
		,@ListRelID=ListRelID
	FROM  
		#tempForm 
	ORDER BY 
		tintOrder ASC   

	WHILE @tintOrder>0                                                  
	BEGIN
	
		IF @bitCustom = 0  
		BEGIN
			DECLARE @Prefix AS VARCHAR(10)

			IF @vcLookBackTableName = 'AdditionalContactsInformation'
				SET @Prefix = 'ADC.'
			ELSE IF @vcLookBackTableName = 'DivisionMaster'
				SET @Prefix = 'Div.'
			ELSE IF @vcLookBackTableName = 'OpportunityMaster'
				SET @PreFix = 'Opp.'			
			ELSE IF @vcLookBackTableName = 'CompanyInfo'
				SET @PreFix = 'cmp.'
			ELSE IF @vcLookBackTableName = 'DemandForecastGrid'
				SET @PreFix = 'DF.'
			ELSE IF @vcLookBackTableName = 'Warehouses'
				SET @PreFix = 'W.'
			ELSE IF @vcLookBackTableName = 'WareHouseItems'
				SET @PreFix = 'WI.'
			ELSE 
				SET @PreFix = CONCAT(@vcLookBackTableName,'.')
			
			SET @vcColumnName= @vcDbColumnName+'~'+ convert(varchar(10),@numFieldId) +'~0'

			IF @vcAssociatedControlType = 'TextBox'
			BEGIN
				IF @vcDbColumnName = 'numQtyToPurchase'
				BEGIN
					SET @strColumns=@strColumns+',' + ' (CASE WHEN ISNULL(WI.numBackOrder,0) > ISNULL(WI.numOnOrder,0) THEN  CEILING(ISNULL(WI.numBackOrder,0) - ISNULL(WI.numOnOrder,0))  ELSE 0 END) '  + ' [' + @vcColumnName + ']'
				END
				ELSE IF @vcDbColumnName = 'moncost'
				BEGIN
					SET @strColumns=@strColumns+',' + ' ISNULL((SELECT vcUnitName FROM UOM WHERE numUOMId = Item.numBaseUnit),''-'') '  + ' [' + @vcColumnName + ']'
				END
				ELSE
				BEGIN
					SET @strColumns=@strColumns+','+  @Prefix + @vcDbColumnName +' ['+ @vcColumnName+']' 
				END
			END
			ELSE IF  @vcAssociatedControlType='Label'                                              
			BEGIN
				IF @vcDbColumnName = 'vc7Days'
				BEGIN
					SET @strColumns=@strColumns+ CONCAT(',dbo.fn_DemandForecastDaysDisplay(',@numDomainID,',',@ClientTimeZoneOffset,',Item.numItemCode,ISNULL(TempLeadDays.numLeadDays,0),WI.numWarehouseItemID,7,',@bitShowHistoricSales,',',@numHistoricalAnalysisPattern,',',@bitBasedOnLastYear,',',@bitIncludeOpportunity,',',@numOpportunityPercentComplete,',',@tintDemandPlanBasedOn,') [',@vcColumnName,']')
				END
				ELSE IF @vcDbColumnName = 'vc15Days'
				BEGIN
					SET @strColumns=@strColumns+ CONCAT(',dbo.fn_DemandForecastDaysDisplay(',@numDomainID,',',@ClientTimeZoneOffset,',Item.numItemCode,ISNULL(TempLeadDays.numLeadDays,0),WI.numWarehouseItemID,15,',@bitShowHistoricSales,',',@numHistoricalAnalysisPattern,',',@bitBasedOnLastYear,',',@bitIncludeOpportunity,',',@numOpportunityPercentComplete,',',@tintDemandPlanBasedOn,') [',@vcColumnName,']')
				END
				ELSE IF @vcDbColumnName = 'vc30Days'
				BEGIN
					SET @strColumns=@strColumns+ CONCAT(',dbo.fn_DemandForecastDaysDisplay(',@numDomainID,',',@ClientTimeZoneOffset,',Item.numItemCode,ISNULL(TempLeadDays.numLeadDays,0),WI.numWarehouseItemID,30,',@bitShowHistoricSales,',',@numHistoricalAnalysisPattern,',',@bitBasedOnLastYear,',',@bitIncludeOpportunity,',',@numOpportunityPercentComplete,',',@tintDemandPlanBasedOn,') [',@vcColumnName,']')
				END
				ELSE IF @vcDbColumnName = 'vc60Days'
				BEGIN
					SET @strColumns=@strColumns+ CONCAT(',dbo.fn_DemandForecastDaysDisplay(',@numDomainID,',',@ClientTimeZoneOffset,',Item.numItemCode,ISNULL(TempLeadDays.numLeadDays,0),WI.numWarehouseItemID,60,',@bitShowHistoricSales,',',@numHistoricalAnalysisPattern,',',@bitBasedOnLastYear,',',@bitIncludeOpportunity,',',@numOpportunityPercentComplete,',',@tintDemandPlanBasedOn,') [',@vcColumnName,']')
				END
				ELSE IF @vcDbColumnName = 'vc90Days'
				BEGIN
					SET @strColumns=@strColumns+ CONCAT(',dbo.fn_DemandForecastDaysDisplay(',@numDomainID,',',@ClientTimeZoneOffset,',Item.numItemCode,ISNULL(TempLeadDays.numLeadDays,0),WI.numWarehouseItemID,90,',@bitShowHistoricSales,',',@numHistoricalAnalysisPattern,',',@bitBasedOnLastYear,',',@bitIncludeOpportunity,',',@numOpportunityPercentComplete,',',@tintDemandPlanBasedOn,') [',@vcColumnName,']')
				END
				ELSE IF @vcDbColumnName = 'vc180Days'
				BEGIN
					SET @strColumns=@strColumns+ CONCAT(',dbo.fn_DemandForecastDaysDisplay(',@numDomainID,',',@ClientTimeZoneOffset,',Item.numItemCode,ISNULL(TempLeadDays.numLeadDays,0),WI.numWarehouseItemID,180,',@bitShowHistoricSales,',',@numHistoricalAnalysisPattern,',',@bitBasedOnLastYear,',',@bitIncludeOpportunity,',',@numOpportunityPercentComplete,',',@tintDemandPlanBasedOn,') [',@vcColumnName,']')
				END
				ELSE IF (@vcDbColumnName = 'vcBuyUOM')
				BEGIN
					SET @strColumns=@strColumns+',' + ' ISNULL((SELECT vcUnitName FROM UOM WHERE numUOMId = ISNULL(Item.numPurchaseUnit,0)),'''')' + ' [' + @vcColumnName + ']'
				END
				ELSE IF @vcDbColumnName = 'numOnOrderReq'
				BEGIN
					SET @strColumns=@strColumns+',' + ' (ISNULL(WI.numOnOrder,0) + ISNULL(WI.numReorder,0)) '  + ' [' + @vcColumnName + ']'
				END
				ELSE IF @vcDbColumnName = 'numAvailable'
				BEGIN
					SET @strColumns=@strColumns+',' + 'CONCAT(ISNULL(WI.numOnHand,0) + ISNULL(WI.numAllocation,0),'' ('',ISNULL(WI.numOnHand,0),'')'')'  + ' [' + @vcColumnName + ']'
				END
				ELSE IF @vcDbColumnName = 'numTotalOnHand'
				BEGIN
					SET @strColumns=@strColumns+',' + 'CONCAT(ISNULL((SELECT SUM(WIInner.numOnHand) + SUM(WIInner.numAllocation) FROM WarehouseItems WIInner WHERE WIInner.numItemID=Item.numItemCode),0),'' ('',ISNULL((SELECT SUM(WIInner.numOnHand) FROM WarehouseItems WIInner WHERE WIInner.numItemID=Item.numItemCode),0),'')'')'  + ' [' + @vcColumnName + ']'
				END
				ELSE
				BEGIN
					SET @strColumns=@strColumns + ',' + @Prefix + @vcDbColumnName  +' ['+ @vcColumnName+']' 
				END   
			END
			ELSE IF  @vcAssociatedControlType='SelectBox' or @vcAssociatedControlType='ListBox'  
			BEGIN
				IF @vcDbColumnName = 'numVendorID'
				BEGIN
					SET @strColumns=@strColumns+',' + ' V.numVendorID '  + ' [' + @vcColumnName + ']'
				END
				ELSE IF @vcDbColumnName = 'ShipmentMethod'
				BEGIN
					SET @strColumns=@strColumns+',' + ' '''' '  + ' [' + @vcColumnName + ']'
				END
				ELSE
				BEGIN
					SET @strColumns=@strColumns + ',' + @Prefix + @vcDbColumnName  +' ['+ @vcColumnName+']' 
				END
			END
			ELSE IF @vcAssociatedControlType='DateField'   
			BEGIN
				IF @vcDbColumnName = 'dtExpectedDelivery'
				BEGIN
					SET @strColumns=@strColumns+',' + ' GETDATE() '  + ' [' + @vcColumnName + ']'
				END
				ELSE IF @vcDbColumnName = 'ItemRequiredDate'
				BEGIN
					SET @strColumns=@strColumns+',T1.dtReleaseDate [' + @vcColumnName + ']'
				END
				ELSE
				BEGIN
					SET @strColumns=@strColumns + ',' + @Prefix + @vcDbColumnName  +' ['+ @vcColumnName+']' 
				END					
			END
		END

		SELECT TOP 1 
			@tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName,@vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,
			@numListID=numListID,@vcLookBackTableName=vcLookBackTableName,@bitCustom=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,
			@bitAllowEdit=bitAllowEdit,@ListRelID=ListRelID
		FROM
			#tempForm 
		WHERE 
			tintOrder > @tintOrder-1 
		ORDER BY 
			tintOrder asc            
 
		IF @@rowcount=0 SET @tintOrder=0 

		END
	  
	DECLARE @StrSql AS VARCHAR(MAX) = ''
	SET @StrSql = CONCAT('SELECT ',@strColumns,' 
						FROM
							#TEMPItems T1
						INNER JOIN
							Item
						ON
							T1.numItemCode = Item.numItemCode
						LEFT JOIN
							Sales_process_List_Master SPLM
						ON
							Item.numBusinessProcessId = SPLM.Slp_Id
						INNER JOIN
							WarehouseItems WI
						ON
							Item.numItemCode=WI.numItemID
							AND T1.numWarehouseItemID=WI.numWarehouseItemID
						INNER JOIN
							Warehouses W
						ON
							WI.numWarehouseID = W.numWareHouseID
						LEFT JOIN
							Vendor V
						ON
							Item.numVendorID = V.numVendorID
							AND Item.numItemCode = V.numItemCode
						OUTER APPLY
						(
							SELECT TOP 1
								numListValue AS numLeadDays
							FROM
								VendorShipmentMethod VSM
							WHERE
								VSM.numVendorID = V.numVendorID
							ORDER BY
								ISNULL(bitPrimary,0) DESC, bitPreferredMethod DESC, numListItemID ASC
						) TempLeadDays
						WHERE
							Item.numDomainID=',@numDomainID,'
							AND 1 = (CASE 
										WHEN ',@bitShowAllItems,' = 1 
										THEN 1 
										ELSE 
											(CASE 
												WHEN ', ISNULL(@tintDemandPlanBasedOn,1),' = 2
												THEN (CASE 
														WHEN (CASE
																WHEN ',@tintUnitsRecommendationForAutoPOBackOrder,'= 3
																THEN 
																	(ISNULL(numBackOrder,0) - ISNULL(numOnOrder,0) + (CASE WHEN ',@bitIncludeRequisitions,' = 1  
																									THEN ISNULL((SELECT 
																												SUM(numUnitHour) 
																											FROM 
																												OpportunityItems 
																											INNER JOIN 
																												OpportunityMaster 
																											ON 
																												OpportunityItems.numOppID=OpportunityMaster.numOppId 
																											WHERE 
																												OpportunityMaster.numDomainId=',@numDomainID,'
																												AND OpportunityMaster.tintOppType=2 
																												AND OpportunityMaster.tintOppStatus=0 
																												AND OpportunityItems.numItemCode=Item.numItemCode 
																												AND OpportunityItems.numWarehouseItmsID=WI.numWareHouseItemID),0) 
																									ELSE 0 
																									END)) +
																	(CASE WHEN ISNULL(Item.fltReorderQty,0) >= ISNULL(V.intMinQty,0) THEN ISNULL(Item.fltReorderQty,0) ELSE ISNULL(V.intMinQty,0) END) 
																WHEN ',@tintUnitsRecommendationForAutoPOBackOrder,'=2
																THEN 
																	(CASE
																		WHEN ISNULL(Item.fltReorderQty,0) >= ISNULL(V.intMinQty,0) AND ISNULL(Item.fltReorderQty,0) >= ISNULL(numBackOrder,0) THEN ISNULL(Item.fltReorderQty,0)
																		WHEN ISNULL(V.intMinQty,0) >= ISNULL(Item.fltReorderQty,0) AND ISNULL(V.intMinQty,0) >= ISNULL(numBackOrder,0) THEN ISNULL(V.intMinQty,0)
																		WHEN ISNULL(numBackOrder,0) >= ISNULL(Item.fltReorderQty,0) AND ISNULL(numBackOrder,0) >= ISNULL(V.intMinQty,0) THEN ISNULL(numBackOrder,0)
																		ELSE ISNULL(Item.fltReorderQty,0)
																	END) - (ISNULL(numOnOrder,0) + (CASE WHEN ',@bitIncludeRequisitions,' = 1  THEN ISNULL((SELECT 
																												SUM(numUnitHour) 
																											FROM 
																												OpportunityItems 
																											INNER JOIN 
																												OpportunityMaster 
																											ON 
																												OpportunityItems.numOppID=OpportunityMaster.numOppId 
																											WHERE 
																												OpportunityMaster.numDomainId=',@numDomainID,'
																												AND OpportunityMaster.tintOppType=2 
																												AND OpportunityMaster.tintOppStatus=0 
																												AND OpportunityItems.numItemCode=Item.numItemCode 
																												AND OpportunityItems.numWarehouseItmsID=WI.numWareHouseItemID),0) ELSE 0 END))
																ELSE
																	(CASE WHEN ISNULL(Item.fltReorderQty,0) > ISNULL(V.intMinQty,0) THEN ISNULL(Item.fltReorderQty,0) ELSE ISNULL(V.intMinQty,0) END) 
																	+ ISNULL(numBackOrder,0) - (ISNULL(numOnOrder,0) + (CASE WHEN ',@bitIncludeRequisitions,' = 1  THEN ISNULL((SELECT 
																																											SUM(numUnitHour) 
																																										FROM 
																																											OpportunityItems 
																																										INNER JOIN 
																																											OpportunityMaster 
																																										ON 
																																											OpportunityItems.numOppID=OpportunityMaster.numOppId 
																																										WHERE 
																																											OpportunityMaster.numDomainId=',@numDomainID,'
																																											AND OpportunityMaster.tintOppType=2 
																																											AND OpportunityMaster.tintOppStatus=0 
																																											AND OpportunityItems.numItemCode=Item.numItemCode 
																																											AND OpportunityItems.numWarehouseItmsID=WI.numWareHouseItemID),0) ELSE 0 END)) 
															END) > 0
														THEN 1
														ELSE 0
													END)
												ELSE
													(CASE 
														WHEN 
															T1.numUnitHour > 0 
														THEN 1 
														ELSE 0 
													END)
											END)
									END)
						')

	IF CHARINDEX('I.',@vcRegularSearchCriteria) > 0
	BEGIN			   
		SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'I.','Item.')
	END
	IF CHARINDEX('WH.',@vcRegularSearchCriteria) > 0
	BEGIN			   
		SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'WH.','W.')
	END
	
	IF @vcRegularSearchCriteria<>'' 
	BEGIN
		SET @strSql=@strSql +' AND ' + @vcRegularSearchCriteria 
	END
	  
	DECLARE  @firstRec  AS INTEGER
	DECLARE  @lastRec  AS INTEGER
  
	SET @firstRec = (@CurrentPage - 1) * @PageSize
	SET @lastRec = (@CurrentPage * @PageSize + 1)

	-- EXECUTE FINAL QUERY
	DECLARE @strFinal AS NVARCHAR(MAX)
	SET @strFinal = CONCAT(@StrSql,' ORDER BY ',CASE WHEN CHARINDEX(@columnName,@strColumns) > 0 THEN @columnName ELSE 'Item.numItemCode' END,' OFFSET ',(@CurrentPage-1) * @PageSize,' ROWS FETCH NEXT ',@PageSize,' ROWS ONLY')

	PRINT CAST(@strFinal AS NTEXT)
	EXEC sp_executesql @strFinal

	SELECT * FROM #tempForm

	DROP TABLE #tempForm

	DROP TABLE #TEMPItems


	UPDATE DemandForecast SET dtExecutionDate = dateadd(MINUTE,-@ClientTimeZoneOffset,GETDATE()) WHERE numDFID = @numDFID

	SELECT CONVERT(VARCHAR(20),dateadd(MINUTE,-@ClientTimeZoneOffset,@dtLastExecution)) AS dtLastExecution
END
GO
/****** Object:  StoredProcedure [dbo].[USP_EcommerceSettings]    Script Date: 03/25/2009 16:46:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj              
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_ecommercesettings')
DROP PROCEDURE usp_ecommercesettings
GO
CREATE PROCEDURE [dbo].[USP_EcommerceSettings]              
@numDomainID as numeric(9),              
@vcPaymentGateWay as int ,                
@vcPGWUserId as varchar(100)='' ,                
@vcPGWPassword as varchar(100),              
@bitShowInStock as bit,              
@bitShowQOnHand as bit,
@bitCheckCreditStatus as BIT,
@numDefaultWareHouseID as numeric(9),
@numRelationshipId as numeric(9),
@numProfileId as numeric(9),
@bitHidePriceBeforeLogin	BIT,
@bitAuthOnlyCreditCard BIT=0,
@bitSendMail BIT = 1,
@vcGoogleMerchantID VARCHAR(1000)  = '',
@vcGoogleMerchantKey VARCHAR(1000) = '',
@IsSandbox BIT ,
@numAuthrizedOrderStatus NUMERIC,
@numSiteID numeric(18, 0),
@vcPaypalUserName VARCHAR(50)  = '',
@vcPaypalPassword VARCHAR(50)  = '',
@vcPaypalSignature VARCHAR(500)  = '',
@IsPaypalSandbox BIT,
@bitSkipStep2 BIT = 0,
@bitDisplayCategory BIT = 0,
@bitShowPriceUsingPriceLevel BIT = 0,
@bitEnableSecSorting BIT = 0,
@bitSortPriceMode   BIT=0,
@numPageSize    INT=15,
@numPageVariant  INT=6,
@bitAutoSelectWarehouse BIT =0,
@bitPreSellUp BIT = 0, 
@bitPostSellUp BIT = 0, 
@numDefaultClass NUMERIC(18,0) = 0,
@vcPreSellUp VARCHAR(200)=NULL,
@vcPostSellUp VARCHAR(200)=NULL,
@vcRedirectThankYouUrl VARCHAR(300)=NULL,
@tintPreLoginProceLevel TINYINT = 0,
@bitHideAddtoCart BIT=1,
@bitShowPromoDetailsLink BIT = 1,
@tintWarehouseAvailability TINYINT = 1,
@bitDisplayQtyAvailable BIT = 0,
@vcSalesOrderTabs VARCHAR(200)='',
@vcSalesQuotesTabs VARCHAR(200)='',
@vcItemPurchaseHistoryTabs VARCHAR(200)='',
@vcItemsFrequentlyPurchasedTabs VARCHAR(200)='',
@vcOpenCasesTabs VARCHAR(200)='',
@vcOpenRMATabs VARCHAR(200)='',
@bitSalesOrderTabs BIT=0,
@bitSalesQuotesTabs BIT=0,
@bitItemPurchaseHistoryTabs BIT=0,
@bitItemsFrequentlyPurchasedTabs BIT=0,
@bitOpenCasesTabs BIT=0,
@bitOpenRMATabs BIT=0,
@bitSupportTabs BIT=0,
@vcSupportTabs VARCHAR(200)='',
@bitElasticSearch BIT=0
as              
BEGIN	        
	IF NOT EXISTS(select 'col1' from eCommerceDTL where numDomainID=@numDomainID AND [numSiteId] = @numSiteID)
	BEGIN
		INSERT INTO eCommerceDTL
        (
			numDomainId,
            vcPaymentGateWay,
            vcPGWManagerUId,
            vcPGWManagerPassword,
            bitShowInStock,
            bitShowQOnHand,
            numDefaultWareHouseID,
            bitCheckCreditStatus,
            [numRelationshipId],
            [numProfileId],
            [bitHidePriceBeforeLogin],
            bitAuthOnlyCreditCard,
            bitSendEmail,
            vcGoogleMerchantID,
            vcGoogleMerchantKey,
            IsSandbox ,
            numAuthrizedOrderStatus,
			numSiteId,
			vcPaypalUserName,
			vcPaypalPassword,
			vcPaypalSignature,
			IsPaypalSandbox,
			bitSkipStep2,
			bitDisplayCategory,
			bitShowPriceUsingPriceLevel,
			bitEnableSecSorting,
			bitSortPriceMode,
			numPageSize,
			numPageVariant,
			bitAutoSelectWarehouse,
			[bitPreSellUp],
			[bitPostSellUp],
			numDefaultClass,
			vcPreSellUp,
			vcPostSellUp,
			vcRedirectThankYouUrl,
			tintPreLoginProceLevel,
			bitHideAddtoCart,
			bitShowPromoDetailsLink,
			tintWarehouseAvailability,
			bitDisplayQtyAvailable,
			bitElasticSearch
		)
		VALUES
		(
			@numDomainID,
            @vcPaymentGateWay,
            @vcPGWUserId,
            @vcPGWPassword,
            @bitShowInStock,
            @bitShowQOnHand,
            @numDefaultWareHouseID,
            @bitCheckCreditStatus,
            @numRelationshipId,
            @numProfileId,
            @bitHidePriceBeforeLogin,
            @bitAuthOnlyCreditCard,
            @bitSendMail,
            @vcGoogleMerchantID,
            @vcGoogleMerchantKey ,
            @IsSandbox,
            @numAuthrizedOrderStatus,
			@numSiteID,
			@vcPaypalUserName,
			@vcPaypalPassword,
			@vcPaypalSignature,
			@IsPaypalSandbox,
			@bitSkipStep2,
			@bitDisplayCategory,
			@bitShowPriceUsingPriceLevel,
			@bitEnableSecSorting,
			@bitSortPriceMode,
			@numPageSize,
			@numPageVariant,
			@bitAutoSelectWarehouse,
			@bitPreSellUp,
			@bitPostSellUp,
			@numDefaultClass,
			@vcPreSellUp,
			@vcPostSellUp,
			@vcRedirectThankYouUrl,
			@tintPreLoginProceLevel,
			@bitHideAddtoCart,
			@bitShowPromoDetailsLink,
			@tintWarehouseAvailability,
			@bitDisplayQtyAvailable,
			@bitElasticSearch
        )

		
		IF(@bitElasticSearch = 1)
		BEGIN
			IF(NOT EXISTS(SELECT 1 FROM eCommerceDTL where numSiteId <> @numSiteID AND numDomainId = @numDomainID AND bitElasticSearch = 1))
			BEGIN
				INSERT INTO ElasticSearchBizCart 
				(
					numDomainID
					,vcValue
					,vcAction
					,vcModule
					--,numSiteID
				)
				SELECT 
					@numDomainID
					,numItemID
					,'Insert'
					,'Item'
					--,@numSiteID  
				FROM 
					SiteCategories
				INNER JOIN
					ItemCategory
				ON
					SiteCategories.numCategoryID = ItemCategory.numCategoryID
				INNER JOIN
					Item
				ON
					ItemCategory.numItemID = Item.numItemCode
				WHERE 
					SiteCategories.numSiteID = @numSiteID
					AND ISNULL(Item.IsArchieve,0) =0
				GROUP BY
					numItemID
			END
		END

		IF ISNULL((SELECT numDefaultSiteID FROM DOmain WHERE numDomainId=@numDomainID ),0) = 0
		BEGIN
			UPDATE Domain SET numDefaultSiteID=SCOPE_IDENTITY() WHERE numDomainId=@numDomainID
		END
	END
	ELSE
	BEGIN
		IF(@bitElasticSearch = 1)
		BEGIN
			INSERT INTO ElasticSearchBizCart 
			(
				numDomainID
				,vcValue
				,vcAction
				,vcModule
				--,numSiteID
			)
			SELECT 
				@numDomainID
				,numItemID
				,'Insert'
				,'Item'
				--,@numSiteID  
			FROM 
				SiteCategories
			INNER JOIN
				ItemCategory
			ON
				SiteCategories.numCategoryID = ItemCategory.numCategoryID
			INNER JOIN
				Item
			ON
				ItemCategory.numItemID = Item.numItemCode
			WHERE 
				SiteCategories.numSiteID = @numSiteID
				AND ISNULL(Item.IsArchieve,0)=0
			GROUP BY
				numItemID
		END
		--ELSE IF(EXISTS(SELECT 1 FROM eCommerceDTL where numSiteId = @numSiteID AND numDomainId = @numDomainID AND bitElasticSearch = 1))
		--BEGIN
		--	DELETE FROM ElasticSearchBizCart WHERE numDomainID = @numDomainID --And numSiteID = @numSiteID
		--END	

		UPDATE 
			eCommerceDTL                                   
		SET              
			vcPaymentGateWay=@vcPaymentGateWay,                
			vcPGWManagerUId=@vcPGWUserId ,                
			vcPGWManagerPassword= @vcPGWPassword,              
			bitShowInStock=@bitShowInStock ,               
			bitShowQOnHand=@bitShowQOnHand,        
			bitCheckCreditStatus=@bitCheckCreditStatus ,
			[bitHidePriceBeforeLogin]=@bitHidePriceBeforeLogin,
			bitAuthOnlyCreditCard=@bitAuthOnlyCreditCard,
			bitSendEmail = @bitSendMail,
			vcGoogleMerchantID = @vcGoogleMerchantID ,
			vcGoogleMerchantKey =  @vcGoogleMerchantKey ,
			IsSandbox = @IsSandbox,
			numAuthrizedOrderStatus = @numAuthrizedOrderStatus,
			numSiteId = @numSiteID,
			vcPaypalUserName = @vcPaypalUserName ,
			vcPaypalPassword = @vcPaypalPassword,
			vcPaypalSignature = @vcPaypalSignature,
			IsPaypalSandbox = @IsPaypalSandbox,
			bitSkipStep2 = @bitSkipStep2,
			bitDisplayCategory = @bitDisplayCategory,
			bitShowPriceUsingPriceLevel = @bitShowPriceUsingPriceLevel,
			bitEnableSecSorting = @bitEnableSecSorting,
			bitSortPriceMode=@bitSortPriceMode,
			numPageSize=@numPageSize,
			numPageVariant=@numPageVariant,
			bitAutoSelectWarehouse=@bitAutoSelectWarehouse,
			[bitPreSellUp] = @bitPreSellUp,
			[bitPostSellUp] = @bitPostSellUp,
			vcPreSellUp=@vcPreSellUp,
			vcPostSellUp=@vcPostSellUp,
			vcRedirectThankYouUrl=@vcRedirectThankYouUrl,
			bitHideAddtoCart=@bitHideAddtoCart,
			bitShowPromoDetailsLink=@bitShowPromoDetailsLink,
			tintWarehouseAvailability=@tintWarehouseAvailability,
			bitDisplayQtyAvailable=@bitDisplayQtyAvailable,
			bitElasticSearch=@bitElasticSearch
		WHERE 
			numDomainId=@numDomainID 
			AND [numSiteId] = @numSiteID
		IF(@numDefaultWareHouseID>0)
		BEGIN
			UPDATE 
				eCommerceDTL                                   
			SET 
				numDefaultWareHouseID =@numDefaultWareHouseID
			WHERE 
				numDomainId=@numDomainID 
				AND [numSiteId] = @numSiteID
		END
		IF(@numRelationshipId>0)
		BEGIN
			UPDATE 
				eCommerceDTL                                   
			SET 
				numRelationshipId =@numRelationshipId
			WHERE 
				numDomainId=@numDomainID 
				AND [numSiteId] = @numSiteID
		END
		IF(@numDefaultClass>0)
		BEGIN
			UPDATE 
				eCommerceDTL                                   
			SET 
				numDefaultClass =@numDefaultClass
			WHERE 
				numDomainId=@numDomainID 
				AND [numSiteId] = @numSiteID
		END
		IF(@numProfileId>0)
		BEGIN
			UPDATE 
				eCommerceDTL                                   
			SET 
				numProfileId =@numProfileId
			WHERE 
				numDomainId=@numDomainID 
				AND [numSiteId] = @numSiteID
		END
		IF(@tintPreLoginProceLevel>0)
		BEGIN
			UPDATE 
				eCommerceDTL                                   
			SET 
				tintPreLoginProceLevel =@tintPreLoginProceLevel
			WHERE 
				numDomainId=@numDomainID 
				AND [numSiteId] = @numSiteID
		END

	END

	UPDATE
		Domain
	SET
		vcSalesOrderTabs=@vcSalesOrderTabs,
		vcSalesQuotesTabs=@vcSalesQuotesTabs,
		vcItemPurchaseHistoryTabs=@vcItemPurchaseHistoryTabs,
		vcItemsFrequentlyPurchasedTabs=@vcItemsFrequentlyPurchasedTabs,
		vcOpenCasesTabs=@vcOpenCasesTabs,
		vcOpenRMATabs=@vcOpenRMATabs,
		vcSupportTabs=@vcSupportTabs,
		bitSalesOrderTabs=@bitSalesOrderTabs,
		bitSalesQuotesTabs=@bitSalesQuotesTabs,
		bitItemPurchaseHistoryTabs=@bitItemPurchaseHistoryTabs,
		bitItemsFrequentlyPurchasedTabs=@bitItemsFrequentlyPurchasedTabs,
		bitOpenCasesTabs=@bitOpenCasesTabs,
		bitOpenRMATabs=@bitOpenRMATabs,
		bitSupportTabs=@bitSupportTabs
	WHERE
		numDomainId=@numDomainID
END
GO
GO
IF EXISTS(SELECT * FROM sysobjects SYSOBJ WHERE SYSOBJ.xtype='p'AND SYSOBJ.NAME ='USP_GET_DROPDOWN_DATA_FOR_IMPORT')
DROP PROCEDURE USP_GET_DROPDOWN_DATA_FOR_IMPORT
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- USP_GET_DROPDOWN_DATA_FOR_IMPORT 75,1
CREATE PROCEDURE USP_GET_DROPDOWN_DATA_FOR_IMPORT
(
	@intImportFileID BIGINT,
	@numDomainID BIGINT,
	@tintMode TINYINT,
	@numFormID NUMERIC(18,0),
	@vcFields VARCHAR(MAX)
)
AS 
BEGIN
	DECLARE @TempFields TABLE
	(
		numFieldID NUMERIC(18,0)
		,intMapColumnNo INT
		,bitCustom BIT
	)

	IF @tintMode = 2
	BEGIN
		DECLARE @hDocItem int
		EXEC sp_xml_preparedocument @hDocItem OUTPUT, @vcFields

		INSERT INTO @TempFields
		(
			numFieldID
			,intMapColumnNo
			,bitCustom
		)
		SELECT
			FormFieldID
			,ImportFieldID
			,IsCustomField
		FROM
			OPENXML (@hDocItem,'/NewDataSet/Table1',2)
		WITH
		(
			FormFieldID NUMERIC(18,0)
			,ImportFieldID NUMERIC(18,0)
			,IsCustomField BIT
		)
	
		EXEC sp_xml_removedocument @hDocItem
	END
	ELSE
	BEGIN
		SELECT @numFormID = numMasterID  FROM dbo.Import_File_Master  WHERE intImportFileID=@intImportFileID
	
		INSERT INTO @TempFields
		(
			numFieldID
			,intMapColumnNo
			,bitCustom
		)
		SELECT
			numFormFieldID
			,intImportFileID
			,ISNULL(bitCustomField,0)
		FROM
			Import_File_Field_Mapping
		WHERE
			intImportFileID=@intImportFileID
	END

	SELECT 
		ROW_NUMBER() OVER (ORDER BY intMapColumnNo) AS [ID]
		,* 
	INTO 
		#tempDropDownData 
	FROM
	(
		SELECT  
			DFFM.numFieldId [numFormFieldId],
			DFFM.vcAssociatedControlType,
			DYNAMIC_FIELD.vcDbColumnName,
			DFFM.vcFieldName [vcFormFieldName],
			FIELD_MAP.intMapColumnNo,
			DYNAMIC_FIELD.numListID,
			0 AS [bitCustomField]					
		FROM 
			@TempFields FIELD_MAP
		INNER JOIN dbo.DycFormField_Mapping DFFM ON FIELD_MAP.numFieldID = DFFM.numFieldId
		INNER JOIN dbo.DycFieldMaster DYNAMIC_FIELD ON DYNAMIC_FIELD.numFieldId = DFFM.numFieldId
		WHERE 
			ISNULL(FIELD_MAP.bitCustom,0) = 0
			AND DFFM.vcAssociatedControlType ='SelectBox'  
			AND DFFM.numFormID = @numFormID	
		UNION ALL
		SELECT 
			CFm.Fld_id AS [numFormFieldId],
			CASE WHEN CFM.Fld_type = 'Drop Down List Box'
					THEN 'SelectBox'
					ELSE CFM.Fld_type 
			END	 AS [vcAssociatedControlType],
			cfm.Fld_label AS [vcDbColumnName],
			cfm.Fld_label AS [vcFormFieldName],
			FIELD_MAP.intMapColumnNo  AS [intMapColumnNo],
			CFM.numlistid AS [numListID],
			1 AS [bitCustomField]
		FROM 
			dbo.CFW_Fld_Master CFM
		INNER JOIN @TempFields FIELD_MAP ON CFM.Fld_id = FIELD_MAP.numFieldID AND ISNULL(FIELD_MAP.bitCustom,0) = 1	
		WHERE 
			numDomainID = @numDomainID
	) TABLE1 
	WHERE  
		vcAssociatedControlType = 'SelectBox' 
              
	SELECT * FROM #tempDropDownData   
	
	DECLARE @intRow AS INT
	SELECT @intRow = COUNT(*) FROM #tempDropDownData
	PRINT @intRow
	
	DECLARE @intCnt AS INT
	DECLARE @numListIDValue AS BIGINT
	DECLARE @numFormFieldIDValue AS BIGINT
	DECLARE @bitCustomField AS BIT
		
	DECLARE @strSQL AS NVARCHAR(MAX)
	DECLARE @vcTableNames AS VARCHAR(MAX) 
	DECLARE @strDBColumnName AS NVARCHAR(1000)
	
	SET @intCnt = 0
	SET @strSQL = '' 
	SET @vcTableNames = ''

	CREATE TABLE #tempGroups
	(
		[Key] NUMERIC(9),
		[Value] VARCHAR(100)
	) 	
	INSERT INTO #tempGroups([Key],[Value]) 
	SELECT 0 AS [Key],'Groups' AS [Value]
	UNION ALL
	SELECT 1 AS [Key],'Prospects' AS [Value]
	UNION ALL
	SELECT 1 AS [Key],'Prospect' AS [Value]
	UNION ALL
	SELECT 1 AS [Key],'Pros' AS [Value]
	UNION ALL
	SELECT 2 AS [Key],'Accounts' AS [Value]
	UNION ALL
	SELECT 2 AS [Key],'Account' AS [Value]
	UNION ALL
	SELECT 2 AS [Key],'Acc' AS [Value]

	--SELECT * FROM #tempGroups
	
	WHILE(@intCnt < @intRow   )
		BEGIN
			SET @intCnt = @intCnt + 1
				
			SELECT  
					@numListIDValue = numListID,
					@strDBColumnName = vcDbColumnName, 
					@numFormFieldIDValue = numFormFieldID,
					@bitCustomField = bitCustomField 	 
			FROM #tempDropDownData 
			WHERE ID = @intCnt
			
			PRINT 'BIT'
			PRINT @bitCustomField
			PRINT @strDBColumnName
			SELECT @strSQL = CASE  
								WHEN @strDBColumnName = 'numItemGroup'
								THEN @strSQL + 'SELECT ITEM_GROUP.numItemGroupID AS [Key],ITEM_GROUP.vcItemGroup AS [Value] FROM dbo.ItemGroups ITEM_GROUP WHERE ITEM_GROUP.numDomainID = ' + CONVERT(VARCHAR,@numDomainID)
								
								WHEN @strDBColumnName = 'tintCRMType'
								THEN @strSQL + 'SELECT DISTINCT [Key],[Value] FROM #tempGroups '
								
								WHEN @strDBColumnName = 'vcLocation'
								THEN @strSQL + 'SELECT numWLocationID AS [Key],vcLocation AS [Value] FROM dbo.WarehouseLocation WHERE numDomainID = ' + CONVERT(VARCHAR,@numDomainID)
								
								WHEN @strDBColumnName = 'vcExportToAPI'
								THEN @strSQL + 'SELECT DISTINCT WebApiId AS [Key],vcProviderName AS [Value] FROM dbo.WebAPI '
								
								WHEN @strDBColumnName = 'numCategoryID'
								THEN @strSQL + 'SELECT numCategoryID AS [Key], vcCategoryName AS [Value] FROM dbo.Category WHERE numDomainID =' + CONVERT(VARCHAR,@numDomainID)
								
								WHEN @strDBColumnName = 'numGrpID'
								THEN @strSQL + 'SELECT GRP.numGrpID AS [Key],GRP.vcGrpName AS [Value] FROM dbo.Groups GRP ' 
											    			    
								WHEN @strDBColumnName = 'numWareHouseID' --OR @strDBColumnName = 'numWareHouseItemID'															  
								THEN @strSQL + 'SELECT WAREHOUSE.numWareHouseID AS [Key],WAREHOUSE.vcWareHouse AS [Value] FROM dbo.Warehouses WAREHOUSE WHERE WAREHOUSE.numDomainID = '+ CONVERT(VARCHAR,@numDomainID) 
								
								WHEN @strDBColumnName = 'numWareHouseItemID'
								THEN @strSQL + 'SELECT WI.numWareHouseItemID AS [Key],W.vcWareHouse AS [Value] FROM dbo.WareHouseItems WI INNER JOIN dbo.Warehouses W ON WI.numWareHouseID = W.numWareHouseID WHERE W.numDomainID = '+ CONVERT(VARCHAR,@numDomainID)
												
								WHEN @strDBColumnName = 'numChildItemID' OR @strDBColumnName = 'numItemKitID'
								THEN @strSQL + ' SELECT ITEM.numItemCode AS [Key],ITEM.vcItemName AS [Value] FROM dbo.Item ITEM WHERE ITEM.numDomainID = '+ CONVERT(VARCHAR,@numDomainID)
								
								WHEN (@strDBColumnName = 'numUOMId' OR @strDBColumnName = 'numPurchaseUnit' OR @strDBColumnName = 'numSaleUnit' OR @strDBColumnName = 'numBaseUnit') 
								THEN @strSQL + '  SELECT numUOMId AS [Key], vcUnitName AS [Value] FROM dbo.UOM
												  JOIN Domain d ON dbo.UOM.numDomainID = d.numDomainID 
												  WHERE dbo.UOM.numDomainId = ' + CONVERT(VARCHAR(10),@numDomainID) +' 
												  AND bitEnabled=1 
												  AND dbo.UOM.tintUnitType=(CASE WHEN ISNULL(d.charUnitSystem,''E'')=''E'' THEN 1 WHEN d.charUnitSystem=''M'' THEN 2 END) '	
								
								WHEN (@strDBColumnName = 'numIncomeChartAcntId' OR @strDBColumnName = 'numAssetChartAcntId' OR @strDBColumnName = 'numCOGsChartAcntId')
								THEN @strSQL + '  SELECT numAccountId AS [Key], LTRIM(RTRIM(REPLACE(vcAccountName,ISNULL(vcNumber,''''),''''))) AS [Value] FROM dbo.Chart_Of_Accounts WHERE numDomainID= ' + CONVERT(VARCHAR(10),@numDomainID)

								WHEN @strDBColumnName = 'numCampaignID'
								THEN @strSQL + '  SELECT numCampaignID AS [Key], vcCampaignName AS [Value] FROM  CampaignMaster WHERE CampaignMaster.numDomainID = ' + CONVERT(VARCHAR(10),@numDomainID)
								
								WHEN @strDBColumnName = 'numVendorID'
								THEN @strSQL + '  SELECT DM.numDivisionID AS [Key], CI.vcCompanyName AS [Value] FROM dbo.CompanyInfo CI INNER JOIN dbo.DivisionMaster DM ON DM.numCompanyID = CI.numCompanyId 
												  WHERE DM.numDomainID = ' + CONVERT(VARCHAR(10),@numDomainID)
								
								WHEN @strDBColumnName = 'numAssignedTo' OR @strDBColumnName = 'numRecOwner' 
								THEN @strSQL + '  SELECT A.numContactID AS [Key], A.vcFirstName+'' ''+A.vcLastName AS [Value] from UserMaster UM join AdditionalContactsInformation A  on UM.numUserDetailId=A.numContactID  where UM.numDomainID = A.numDomainID AND UM.numDomainID = ' + CONVERT(VARCHAR,@numDomainID)	
								
------								 SELECT A.numContactID,A.vcFirstName+' '+A.vcLastName as vcUserName from UserMaster UM         
------								 join AdditionalContactsInformation A        
------								 on UM.numUserDetailId=A.numContactID          
------								 where UM.numDomainID=@numDomainID and UM.numDomainID=A.numDomainID   
							
								WHEN @strDBColumnName = 'numTermsID'
								THEN @strSQL + 'SELECT DISTINCT numTermsID AS [Key],vcTerms AS [Value] FROM BillingTerms 
												WHERE numDomainID = ' + CONVERT(VARCHAR,@numDomainID) 
												
								WHEN @strDBColumnName = 'numShipState' OR @strDBColumnName = 'numBillState' OR @strDBColumnName = 'numState' OR @strDBColumnName = 'State'
								THEN @strSQL + '  SELECT numStateID AS [Key], vcState AS [Value], (CASE WHEN LEN(ISNULL(vcAbbreviations,'''')) > 0 THEN vcAbbreviations ELSE ''NOTAVAILABLE'' END) vcAbbreviations FROM State where numDomainID=' + CONVERT(VARCHAR,@numDomainID)
								
								WHEN @strDBColumnName = 'charItemType'
								THEN @strSQL + ' SELECT ''A'' AS [Key], ''asset'' AS [Value]
											     UNION 				
											     SELECT ''P'' AS [Key], ''inventory item'' AS [Value]
											     UNION 				
											     SELECT ''S'' AS [Key], ''service'' AS [Value]
											     UNION 				
											     SELECT ''N'' AS [Key], ''non-inventory item'' AS [Value]'	
								
								WHEN @strDBColumnName = 'tintRuleType'
								THEN @strSQL + ' SELECT 1 AS [Key], ''Deduct from List price'' AS [Value]
											     UNION 				
											     SELECT 2 AS [Key], ''Add to Primary Vendor Cost'' AS [Value]
											     UNION 				
											     SELECT 3 AS [Key], ''Named Price'' AS [Value]'	
											     
								WHEN @strDBColumnName = 'tintDiscountType'
								THEN @strSQL + ' SELECT 1 AS [Key], ''Percentage'' AS [Value]
											     UNION 				
											     SELECT 2 AS [Key], ''Flat Amount'' AS [Value]
												 UNION 				
											     SELECT 3 AS [Key], ''Named Price'' AS [Value]'
												 											     
								WHEN @strDBColumnName = 'numShippingService'
								THEN @strSQL + 'SELECT 
													ShippingService.numShippingServiceID AS [Key]
													,ShippingService.vcShipmentService AS [Value]
													,ISNULL(ShippingServiceAbbreviations.vcAbbreviation,'''') vcAbbreviations
												FROM 
													ShippingService 
												LEFT JOIN
													ShippingServiceAbbreviations
												ON
													ShippingServiceAbbreviations.numShippingServiceID = ShippingService.numShippingServiceID
												WHERE 
													ShippingService.numDomainID=' + CONVERT(VARCHAR,@numDomainID) + ' OR ISNULL(ShippingService.numDomainID,0) = 0'					     																		    
								WHEN @strDBColumnName = 'numChartAcntId'
								THEN @strSQL + 'SELECT 
													numAccountId AS [Key]
													,vcAccountName AS [Value] 
												FROM 
													Chart_Of_Accounts 
												WHERE 
													numDomainId=' + CONVERT(VARCHAR,@numDomainID)
								ELSE 
									 @strSQL + ' SELECT LIST.numListItemID AS [Key],vcData AS [Value] FROM ListDetails LIST LEFT JOIN listorder LIST_ORDER ON LIST.numListItemID = LIST_ORDER.numListItemID AND LIST_ORDER.numDomainId = ' + CONVERT(VARCHAR,@numDomainID) + ' WHERE ( LIST.numDomainID = ' + CONVERT(VARCHAR,@numDomainID) + ' OR LIST.constFlag = 1) AND LIST.numListID = ' + CONVERT(VARCHAR,@numListIDValue) + ' AND  (LIST.numDomainID = ' + CONVERT(VARCHAR,@numDomainID) + ' OR constFlag = 1 )  ORDER BY intSortOrder '
						     END						
						     
			SET @vcTableNames = @vcTableNames + CONVERT(VARCHAR,@numFormFieldIDValue) + '_' + @strDBColumnName + ',' 
		END
	
	PRINT @vcTableNames
	PRINT @strSQL	
	
	SELECT @vcTableNames AS TableNames
	EXEC SP_EXECUTESQL @strSQL		
	
	DROP TABLE #tempDropDownData
	DROP TABLE #tempGroups
END
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetScalerValue')
DROP PROCEDURE USP_GetScalerValue
GO
CREATE PROCEDURE USP_GetScalerValue 
@numDomainID AS NUMERIC,--Will receive SiteID when mode =2,12,9
@tintMode AS TINYINT,
@vcInput AS VARCHAR(1000),
@numUserCntId AS NUMERIC(9)=0
AS
BEGIN
	IF @tintMode = 1
	BEGIN
		SELECT numJournal_Id FROM dbo.General_Journal_Header WHERE numDomainId=@numDomainID and numBizDocsPaymentDetId= CONVERT(NUMERIC, @vcInput)
	END
	IF @tintMode = 2 
	BEGIN
		SELECT COUNT(*) FROM dbo.Sites WHERE vcHostName=@vcInput AND numSiteID <> @numDomainID 
	END
	IF @tintMode = 3 
	BEGIN
		SELECT COUNT(*) FROM dbo.Domain WHERE vcPortalName=@vcInput AND numDomainId <> @numDomainID
	END
	IF @tintMode = 4 --Item Name
	BEGIN
		SELECT dbo.GetItemName(@vcInput)
	END
	IF @tintMode = 5 --Company Name
	BEGIN
		SELECT dbo.fn_GetComapnyName(@vcInput)
	END
	IF @tintMode = 6 --Vendor Cost
	BEGIN
		SELECT dbo.fn_GetVendorCost(@vcInput)
	END
	IF @tintMode = 7 -- tintRuleFor
	BEGIN
		SELECT tintRuleFor FROM PriceBookRules WHERE numPricRuleID=@vcInput AND numDomainId =@numDomainId
	End
	IF @tintMode = 8 -- monSCreditBalance
	BEGIN
		SELECT isnull(monSCreditBalance,0) as monCreditBalance FROM DivisionMaster WHERE numDivisionID=@vcInput AND numDomainId =@numDomainId
	End
    IF @tintMode = 9 -- Check Site Page Exists
	BEGIN
		if exists(SELECT [vcPageName] FROM   SitePages WHERE  [numSiteID] = @numDomainId AND LOWER([vcPageURL]) = LOWER(@vcInput))
			SELECT 1 as SitePageExists
		else
			SELECT 0 as SitePageExists
	END
	IF @tintMode = 10 -- Category Name
	BEGIN
		SELECT ISNULL(vcCategoryName,'NA') FROM dbo.Category WHERE numCategoryID=CAST(@vcInput AS NUMERIC(9)) AND numDomainId =@numDomainId
	End
	IF @tintMode = 11 -- monPCreditBalance
	BEGIN
		SELECT isnull(monPCreditBalance,0) as monCreditBalance FROM DivisionMaster WHERE numDivisionID=@vcInput AND numDomainId =@numDomainId
	END
	 IF @tintMode = 12 -- Get Style ID from filename
	BEGIN
		SELECT TOP 1 ISNULL([numCssID],0) FROM  dbo.StyleSheets WHERE  [numSiteID] = @numDomainId AND LOWER([styleFileName]) = LOWER(@vcInput)
	END
	IF @tintMode = 13 -- numItemClassification
	BEGIN
		SELECT ISNULL(numItemClassification,0) as numItemClassification FROM  Item WHERE  numDomainID = @numDomainId AND numItemCode = @vcInput
	END
	IF @tintMode = 14 -- numAssignedTo
	BEGIN
		SELECT ISNULL(numAssignedTo,0) as numAssignedTo FROM  DivisionMaster WHERE  numDomainID = @numDomainId AND numDivisionID = @vcInput
	END
	IF @tintMode = 15 -- Get Currency ID 
	BEGIN
		SELECT numCurrencyID FROM Currency WHERE numDomainID = @numDomainID AND chrCurrency = @vcInput
	END
	IF @tintMode = 16 -- Total Order of Items where Warehouse exists
	BEGIN
		SELECT count(*) as TotalRecords FROM OpportunityMaster OM join OpportunityItems OI on OM.numOppId=OI.numOppId
			 WHERE OI.numItemCode = @vcInput AND OM.numDomainID = @numDomainID and OI.numWarehouseItmsID>0
	END
	IF @tintMode = 17 -- Get numBizDocID
	BEGIN
		SELECT numBizDocTempID FROM dbo.BizDocTemplate 
								WHERE numDomainID = @numDomainID
								  AND tintTemplateType = 0
								  AND bitDefault = 1
								  AND bitEnabled = 1
								  AND numBizDocID = @vcInput
	END
	IF @tintMode = 18 -- Get Domain DivisionID
	BEGIN
		SELECT numDivisionID FROM Domain WHERE numDomainID = @numDomainID
	END
	IF @tintMode = 19
	BEGIN
		SELECT numItemID FROM ItemAPI WHERE numDomainID = @numDomainID AND  vcAPIItemID = @vcInput
	END
	IF @tintMode = 20
	BEGIN
		SELECT numItemCode FROM Item WHERE numDomainID = @numDomainID AND  vcSKU = @vcInput
	END
	IF @tintMode = 21
	BEGIN
		SELECT numStateId FROM dbo.State S inner JOIN webApiStateMapping SM
		ON S.vcState =  SM.vcStateName 
		AND S.numDomainId = @numDomainId 
		AND ( LOWER(SM.vcAmazonStateCode) = LOWER(@vcInput) OR LOWER(SM.vcStateName) = LOWER(@vcInput))
	END
	IF @tintMode = 22
	BEGIN
		SELECT LD.numListItemId FROM ListDetails LD INNER Join WebApiCountryMapping CM ON 
		LD.vcData = CM.vcCountryName WHERE LD.numListId = 40 AND LD.numDomainId = @numDomainId AND
		CM.vcAmazonCountryCode = @vcInput
	END
   IF @tintMode = 23
   BEGIN
       select isnull(txtSignature,'') from userMaster where numUserDetailId = convert(numeric , @vcInput) and numDomainId = @numDomainId 
   END
   
   IF @tintMode = 24
   BEGIN
         DECLARE @dtEmailStartDate AS varchar(20),
                 @dtEmailEndDate AS varchar(20) ,
                 @intNoOfEmail AS numeric
            
        select @dtEmailStartDate =convert(varchar(20) , dtEmailStartDate) ,@dtEmailEndDate =convert(varchar(20), dtEmailEndDate), @intNoOfEmail = intNoOfEmail from Subscribers where numTargetDomainID=@numDomainID
       
        IF  Convert(datetime, Convert(int, GetDate())) >=  @dtEmailStartDate AND Convert(datetime, Convert(int, GetDate())) <=  @dtEmailEndDate  
            BEGIN
		          select  (convert(varchar(10) , count(*)) + '-' +(select top 1 convert(varchar(10), intNoOfEmail) from Subscribers where numTargetDomainID = @numDomainID)) as EmailData
                  from broadcastDTLs BD inner join BroadCast B on B.numBroadCastID = BD.numBroadCastID 
                  where B.numDomainId = @numDomainId AND bintBroadCastDate between @dtEmailStartDate and @dtEmailEndDate and bitBroadcasted = 1 and tintSucessfull = 1     		
			END       
   END  
   IF @tintMode = 25
	BEGIN
		SELECT IsNull(vcMsg,'') AS vcMsg FROM dbo.fn_GetEmailAlert(@numDomainId ,@vcInput,@numUserCntId)
	END
	IF @tintMode = 26
	BEGIN
		SELECT COUNT(*) AS Total FROM dbo.Communication WHERE numDomainID = @numDomainID and numContactId = @vcInput
	END 
	IF @tintMode = 27 -- Check Order Closed
	BEGIN
		SELECT ISNULL(tintshipped,0) AS tintshipped FROM OpportunityMaster  
			 WHERE numOppId = convert(numeric , @vcInput) AND numDomainID = @numDomainID
	END
	IF @tintMode = 28 -- General Journal Header for Bank Recon. Adjustments
	BEGIN
		SELECT ISNULL(numJournal_Id,0) AS numJournal_Id FROM General_Journal_Header  
			 WHERE numReconcileID = convert(numeric , @vcInput) AND numDomainID = @numDomainID AND ISNULL(bitReconcileInterest,0) = 0
	END
	IF @tintMode = 29 -- General Journal Header for Write Check
	BEGIN
		SELECT ISNULL(numJournal_Id,0) AS numJournal_Id FROM General_Journal_Header  
			 WHERE numCheckHeaderID = convert(numeric , @vcInput) AND numDomainID = @numDomainID
	END
	
	IF @tintMode = 30 -- Get Units for sample item to be created
	BEGIN
	 SELECT ISNULL(u.numUOMId,0) FROM 
        UOM u INNER JOIN Domain d ON u.numDomainID=d.numDomainID
        WHERE u.numDomainID=@numDomainID 
        AND d.numDomainID=@numDomainID 
        AND u.tintUnitType=(CASE WHEN ISNULL(d.charUnitSystem,'E')='E' THEN 1 WHEN d.charUnitSystem='M' THEN 2 END)
        AND u.vcUnitName ='Units'
	END
	
	IF @tintMode = 31 -- Get Magento Attribute Set for Current Domain
	BEGIN
		SELECT vcEighthFldValue FROM dbo.WebAPIDetail WHERE numDomainId = @numDomainID  AND webApiId = @vcInput
	END
	
	IF @tintMode = 32 -- Get WareHouseItemID for ItemID is existis for Current Domain
	BEGIN
		 SELECT ISNULL((SELECT TOP 1 numWareHouseItemID FROM  dbo.WareHouseItems WHERE numDomainId = @numDomainID  AND numItemID = @vcInput),0)AS  numWareHouseItemID
	END
	
	IF @tintMode = 33
	BEGIN
	SELECT ISNULL(MAX(CAST(ISNULL(numSequenceId,0) AS BigInt)),0) + 1 FROM dbo.OpportunityMaster OM JOIN dbo.OpportunityBizDocs OBD ON OM.numOppId=OBD.numOppId
			 WHERE OM.numDomainId=@numDomainID AND OBD.numBizDocId=@vcInput
	END	
	
	IF @tintMode = 34
	BEGIN
	SELECT ISNULL(vcOppRefOrderNo,'') FROM dbo.OpportunityMaster OM 
			 WHERE OM.numDomainId=@numDomainID AND OM.numOppID=@vcInput
	END
	
	IF @tintMode = 35
	BEGIN 
	SELECT ISNULL(bitAutolinkUnappliedPayment,0) FROM dbo.Domain  WHERE numDomainId=@numDomainID 
	END		 
	
	IF @tintMode = 36
	BEGIN 
		SELECT ISNULL(numJournalId,0) FROM dbo.General_Journal_Details WHERE numDomainId=@numDomainID AND numChartAcntId=@vcInput AND chBizDocItems='OE'
	END	
	
	IF @tintMode = 37
	BEGIN 
		SELECT ISNULL(numShippingServiceItemID,0) FROM dbo.Domain WHERE numDomainId=@numDomainID
	END	
	IF @tintMode = 38
	BEGIN
		DECLARE @numItemCode AS NUMERIC(18,0)
		
		--FIRST CHECK FOR ITEM LEVEL SKU
		SELECT TOP 1 @numItemCode=numItemCode FROM Item WHERE numDomainID = @numDomainID AND  vcSKU = @vcInput

		--IF ITEM NOT AVAILABLE WITH SKU CHECK FOR MATRIX ITEM (WAREHOUSE LEVEL SKU)
		IF ISNULL(@numItemCode,0) = 0
		BEGIN
			SELECT TOP 1 @numItemCode=numItemID FROM WarehouseItems WHERE numDomainID = @numDomainID AND vcWHSKU = @vcInput
		END

		SELECT CONVERT(VARCHAR(50),numItemCode)+'~'+CharItemType as numItemCode FROM Item WHERE numDomainID = @numDomainID AND numItemCode=@numItemCode
	END	
	IF @tintMode = 39 -- Get WareHouseItemID for SKU is exists for Current Domain
	BEGIN
	IF CHARINDEX('~',@vcInput) > 0
		BEGIN
			DECLARE @numItemID NUMERIC(18,0)
			DECLARE @vcSKU VARCHAR(50)
			SELECT @numItemID = strItem FROM dbo.SplitWordWithPosition(@vcInput,'~') WHERE RowNumber = 1
			SELECT @vcSKU = strItem FROM dbo.SplitWordWithPosition(@vcInput,'~') WHERE RowNumber = 2

			
			SELECT   ISNULL(( SELECT TOP 1
                            numWareHouseItemID
                     FROM   dbo.WareHouseItems
                     WHERE  numDomainId = @numDomainID
                            AND vcWHSKU = @vcSKU
                            AND numItemID = @numItemID
                   ), 0) AS numWareHouseItemID
		END
	ELSE
		BEGIN
			SELECT   ISNULL(( SELECT TOP 1
                            numWareHouseItemID
                     FROM   dbo.WareHouseItems
                     WHERE  numDomainId = @numDomainID
                            AND numItemID = @vcInput
                   ), 0) AS numWareHouseItemID
		END
	END
	
	IF @tintMode = 40 -- General Journal Header for Bank Recon Interest Service Charge.
	BEGIN
		SELECT ISNULL(numJournal_Id,0) AS numJournal_Id FROM General_Journal_Header  
			 WHERE numReconcileID = convert(numeric , @vcInput) AND numDomainID = @numDomainID AND ISNULL(bitReconcileInterest,0) = 1
	END 
	IF @tintMode = 41 -- Item On Hand
	BEGIN
		SELECT SUM(ISNULL(numOnHand,0) + ISNULL(numAllocation,0)) FROM WareHouseItems WHERE numDomainID = @numDomainID AND numItemID = @vcInput
	END 
END
--exec USP_GetScalerValue @numDomainID=1,@tintMode=24,@vcInput='0'
 
--Created By                                                          
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'usp_gettableinfodefault' ) 
    DROP PROCEDURE usp_gettableinfodefault
GO
CREATE PROCEDURE [dbo].[usp_GetTableInfoDefault]                                                                        
@numUserCntID numeric=0,                        
@numRecordID numeric=0,                                    
@numDomainId numeric=0,                                    
@charCoType char(1) = 'a',                        
@pageId as numeric,                        
@numRelCntType as NUMERIC,                        
@numFormID as NUMERIC,                        
@tintPageType TINYINT             
                             
                                    
--                                                           
                                               
AS                                      
  
                                                 
IF (
	SELECT 
		ISNULL(sum(TotalRow),0)  
	FROM
		(            
			Select count(*) TotalRow FROM View_DynamicColumns WHERE numUserCntId=@numuserCntid AND numDomainId = @numDomainId AND numFormID=@numFormID AND numRelCntType=@numRelCntType AND tintPageType=@tintPageType
			Union 
			Select count(*) TotalRow from View_DynamicCustomColumns WHERE numUserCntId=@numuserCntid AND numDomainId = @numDomainId AND numFormID=@numFormID AND numRelCntType=@numRelCntType AND tintPageType=@tintPageType
		) TotalRows
	) <> 0              
BEGIN  
	IF @numFormID=88 AND @pageId=5 AND @numRelCntType=0 AND @tintPageType=2
	BEGIN
		IF NOT EXISTS (SELECT numFieldID FROM View_DynamicColumns WHERE numDomainId = @numDomainId AND numUserCntId=@numuserCntid AND numFormID=@numFormID AND numRelCntType=@numRelCntType AND tintPageType=@tintPageType AND numFieldID=212)
		BEGIN
			INSERT INTO DycFormConfigurationDetails
			(
				numFormId,numFieldId,numViewId,intColumnNum,intRowNum,numDomainId,numUserCntID,numRelCntType,tintPageType,bitCustom
			)
			VALUES
			(
				@numFormID
				,212
				,0
				,1
				,ISNULL((SELECT MAX(tintRow) + 1 FROM View_DynamicColumns WHERE numDomainId = @numDomainId AND numUserCntId=@numuserCntid AND numFormID=@numFormID AND numRelCntType=@numRelCntType AND tintPageType=@tintPageType),0)
				,@numDomainId
				,@numUserCntID
				,@numRelCntType
				,@tintPageType
				,0
			)
		END

		IF NOT EXISTS (SELECT numFieldID FROM View_DynamicColumns WHERE numDomainId = @numDomainId AND numUserCntId=@numuserCntid AND numFormID=@numFormID AND numRelCntType=@numRelCntType AND tintPageType=@tintPageType AND numFieldID=216)
		BEGIN
			INSERT INTO DycFormConfigurationDetails
			(
				numFormId,numFieldId,numViewId,intColumnNum,intRowNum,numDomainId,numUserCntID,numRelCntType,tintPageType,bitCustom
			)
			VALUES
			(
				@numFormID
				,216
				,0
				,1
				,ISNULL((SELECT MAX(tintRow) + 1 FROM View_DynamicColumns WHERE numDomainId = @numDomainId AND numUserCntId=@numuserCntid AND numFormID=@numFormID AND numRelCntType=@numRelCntType AND tintPageType=@tintPageType),0)
				,@numDomainId
				,@numUserCntID
				,@numRelCntType
				,@tintPageType
				,0
			)
		END
	END
	                         
IF @pageid=1 or @pageid=4 or @pageid= 12 or  @pageid= 13 or  @pageid= 14
BEGIN
    SELECT 
		numFieldId,ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName,'' as vcURL,vcAssociatedControlType as fld_type
		,tintRow,tintcolumn as intcoulmn,'' as vcValue,bitCustom AS bitCustomField,vcPropertyName,ISNULL(bitAllowEdit,0) AS bitCanBeUpdated
		,numListID,0 numListItemID,PopupFunctionName
		,CASE 
			WHEN vcAssociatedControlType='SelectBox' 
			THEN ISNULL((SELECT numPrimaryListID FROM FieldRelationship WHERE numSecondaryListID=numListID AND numDomainID=@numDomainId),0) 
			ELSE 0 
		END AS ListRelID
		,CASE 
			WHEN vcAssociatedControlType='SelectBox' 
			THEN (SELECT COUNT(*) FROM FieldRelationship WHERE numPrimaryListID=numListID AND numDomainID=@numDomainId) 
			ELSE 0 
		END AS DependentFields,
		bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
		bitFieldMessage,intMaxLength,intMinLength,bitFieldMessage,ISNULL(vcFieldMessage,'') vcFieldMessage
		,(SELECT TOP 1 FldDTLID from CFW_FLD_Values where  RecId=@numRecordID and Fld_Id=numFieldId) as FldDTLID
		,'' as Value,vcDbColumnName,vcToolTip,intFieldMaxLength,ISNULL(numFormFieldGroupId,0) AS numFormFieldGroupId,ISNULL(vcGroupName,'') AS vcGroupName,ISNULL(numOrder,0) AS numOrder
	FROM 
		View_DynamicColumns
	WHERE 
		numFormId=@numFormID 
		AND numUserCntID=@numUserCntID 
		AND numDomainID=@numDomainID  
		AND ISNULL(bitCustom,0)=0 
		AND numRelCntType=@numRelCntType 
		AND tintPageType=@tintPageType 
		AND 1=(CASE WHEN @tintPageType=2 THEN CASE WHEN ISNULL(bitAddField,0)=1 THEN 1 ELSE 0 end 
				WHEN @tintPageType=3 THEN CASE WHEN ISNULL(bitDetailField,0)=1 THEN 1 ELSE 0 END 
				ELSE 0 END) 
	UNION 
	SELECT 
		numFieldId,vcfieldName ,vcURL,vcAssociatedControlType as fld_type,tintRow,tintcolumn as intcoulmn,
		CASE 
			WHEN vcAssociatedControlType = 'SelectBox' 
			THEN (SELECT vcData FROM listdetails WHERE numlistitemid = dbo.GetCustFldValue(numFieldID,@PageId,@numRecordID))
			WHEN vcAssociatedControlType = 'CheckBoxList' 
			THEN STUFF((SELECT CONCAT(',', vcData) FROM ListDetails WHERE numlistitemid IN (SELECT ISNULL(Id,0) FROM dbo.SplitIDs(dbo.GetCustFldValue(numFieldID,@PageId,@numRecordID),',')) FOR XML PATH('')), 1, 1, '')
			ELSE
				dbo.GetCustFldValue(numFieldID,@PageId,@numRecordID) 
		END AS vcValue,                    
		CONVERT(BIT,1) bitCustomField,'' vcPropertyName, CONVERT(BIT,1) AS bitCanBeUpdated,numListID,
		CASE 
			WHEN vcAssociatedControlType = 'SelectBox' 
			THEN dbo.GetCustFldValue(numFieldID,@PageId,@numRecordID) 
			ELSE 0 
		END numListItemID,'' PopupFunctionName,
		CASE 
			WHEN vcAssociatedControlType='SelectBox' 
			THEN ISNULL((SELECT numPrimaryListID FROM FieldRelationship WHERE numSecondaryListID=numListID AND numDomainID=@numDomainId),0) 
			ELSE 0 
		END AS ListRelID ,
		CASE 
			WHEN vcAssociatedControlType='SelectBox' 
			THEN (SELECT COUNT(*) FROM FieldRelationship where numPrimaryListID=numListID AND numDomainID=@numDomainId) 
			ELSE 0 
		END AS DependentFields,
		bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,bitFieldMessage,intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage
		,isnull((SELECT TOP 1 FldDTLID from CFW_FLD_Values where  RecId=@numRecordID and Fld_Id=numFieldID),0) as FldDTLID
		,dbo.GetCustFldValue(numFieldID,@PageId,@numRecordID) as Value,'' AS vcDbColumnName,vcToolTip,0 intFieldMaxLength,ISNULL(numFormFieldGroupId,0) AS numFormFieldGroupId,ISNULL(vcGroupName,'') AS vcGroupName,ISNULL(numOrder,0) AS numOrder
	FROM 
		View_DynamicCustomColumns_RelationShip
	WHERE 
		grp_id=@PageId 
		AND numDomainID=@numDomainID 
		AND subgrp=0
		AND numUserCntID=@numUserCntID 
		AND numFormId=@numFormID 
		AND numRelCntType=@numRelCntType 
		AND tintPageType=@tintPageType
	ORDER BY 
		tintRow,intcoulmn   
END                
                
if @PageId= 2 or  @PageId= 3 or @PageId= 5 or @PageId= 6 or @PageId= 7 or @PageId= 8   or @PageId= 11                          
 begin            
		IF @numFormID = 123 -- Add/Edit Order - Item Grid Column Settings
		BEGIN
			SELECT numFieldId,ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName
				,'' as vcURL,vcAssociatedControlType as fld_type,tintRow,tintcolumn as intcoulmn,          
					'' as vcValue,ISNULL(bitCustom,0) AS bitCustomField,vcPropertyName,ISNULL(bitAllowEdit,0) AS bitCanBeUpdated,
					numListID,0 numListItemID,PopupFunctionName,
		Case when vcAssociatedControlType='SelectBox' then isnull((Select numPrimaryListID from FieldRelationship where numSecondaryListID=numListID and numDomainID=@numDomainId  ),0) else 0 end as ListRelID, 
		Case when vcAssociatedControlType='SelectBox' then (Select count(*) from FieldRelationship where numPrimaryListID=numListID and numDomainID=@numDomainId  ) else 0 end as DependentFields,
		bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
		bitFieldMessage,intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,vcDbColumnName,vcToolTip,intFieldMaxLength,ISNULL(numFormFieldGroupId,0) AS numFormFieldGroupId,ISNULL(vcGroupName,'') AS vcGroupName,ISNULL(numOrder,0) AS numOrder
		FROM View_DynamicColumns
			where numFormId=@numFormID and numUserCntID=@numUserCntID and  numDomainID=@numDomainID  
			and ISNULL(bitCustom,0)=0 and numRelCntType=@numRelCntType AND tintPageType=@tintPageType 

			union          
           
			select numFieldId,vcfieldName ,vcURL,vcAssociatedControlType as fld_type,               
			tintRow,tintcolumn as intcoulmn,
			case when vcAssociatedControlType = 'SelectBox' then (select vcData from listdetails where numlistitemid=dbo.GetCustFldValue(numFieldID,@PageId,@numRecordID))
			when vcAssociatedControlType = 'CheckBoxList' then STUFF((SELECT CONCAT(',', vcData) FROM ListDetails WHERE numlistitemid IN (SELECT ISNULL(Id,0) FROM dbo.SplitIDs(dbo.GetCustFldValue(numFieldID,@PageId,@numRecordID),',')) FOR XML PATH('')), 1, 1, '')
			else dbo.GetCustFldValue(numFieldId,@PageId,@numRecordID) end as vcValue,                    
			convert(bit,1) bitCustomField,'' vcPropertyName,  
			convert(bit,1) as bitCanBeUpdated,numListID,case when vcAssociatedControlType = 'SelectBox' THEN dbo.GetCustFldValue(numFieldId,@PageId,@numRecordID) ELSE 0 END numListItemID,'' PopupFunctionName,
		Case when vcAssociatedControlType='SelectBox' then isnull((Select numPrimaryListID from FieldRelationship where numSecondaryListID=numListID and numDomainID=@numDomainId  ),0) else 0 end as ListRelID ,
		Case when vcAssociatedControlType='SelectBox' then (Select count(*) from FieldRelationship where numPrimaryListID=numListID and numDomainID=@numDomainId  ) else 0 end as DependentFields,
		bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,bitFieldMessage,intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,'' AS vcDbColumnName
		,vcToolTip,0 intFieldMaxLength,ISNULL(numFormFieldGroupId,0) AS numFormFieldGroupId,ISNULL(vcGroupName,'') AS vcGroupName,ISNULL(numOrder,0) AS numOrder
		from View_DynamicCustomColumns
			where grp_id=@PageId and numDomainID=@numDomainID and subgrp=0
			and numUserCntID=@numUserCntID  
			AND numFormId=@numFormID and ISNULL(bitCustom,0)=1 and numRelCntType=@numRelCntType AND tintPageType=@tintPageType
    
			order by tintrow,intcoulmn      
		END
		ELSE
		BEGIN    
				SELECT numFieldId,ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName
				,'' as vcURL,vcAssociatedControlType as fld_type,tintRow,tintcolumn as intcoulmn,          
					'' as vcValue,ISNULL(bitCustom,0) AS bitCustomField,vcPropertyName,
					--DO NOT ALLOW CHANGE OF ASSIGNED TO FIELD AFTER COMMISSION IS PAID
					CAST((CASE 
					WHEN @charCoType='O' AND numFieldID=100
					THEN 
						CASE 
						WHEN (SELECT COUNT(*) FROM BizDocComission WHERE numOppID=@numRecordID AND ISNULL(bitCommisionPaid,0)=1) > 0 
						THEN 
							0 
						ELSE 
							ISNULL(bitAllowEdit,0) 
						END 
					ELSE 
						ISNULL(bitAllowEdit,0) 
					END) AS BIT) AS bitCanBeUpdated,
					numListID,0 numListItemID,PopupFunctionName,
		Case when vcAssociatedControlType='SelectBox' then isnull((Select numPrimaryListID from FieldRelationship where numSecondaryListID=numListID and numDomainID=@numDomainId  ),0) else 0 end as ListRelID, 
		Case when vcAssociatedControlType='SelectBox' then (Select count(*) from FieldRelationship where numPrimaryListID=numListID and numDomainID=@numDomainId  ) else 0 end as DependentFields,
		bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
		bitFieldMessage,intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,vcDbColumnName,vcToolTip,intFieldMaxLength,ISNULL(numFormFieldGroupId,0) AS numFormFieldGroupId,ISNULL(vcGroupName,'') AS vcGroupName,ISNULL(numOrder,0) AS numOrder
		FROM View_DynamicColumns
			where numFormId=@numFormID and numUserCntID=@numUserCntID and  numDomainID=@numDomainID  
			and ISNULL(bitCustom,0)=0 and numRelCntType=@numRelCntType AND tintPageType=@tintPageType 
			AND  1=(CASE WHEN @tintPageType=2 THEN CASE WHEN ISNULL(bitAddField,0)=1 THEN 1 ELSE 0 end 
					WHEN @tintPageType=3 THEN CASE WHEN ISNULL(bitDetailField,0)=1 THEN 1 ELSE 0 END 
					ELSE 0 END) 

			union          
           
			select numFieldId,vcfieldName ,vcURL,vcAssociatedControlType as fld_type,               
			tintRow,tintcolumn as intcoulmn,
			case WHEN vcAssociatedControlType = 'SelectBox' then (select vcData from listdetails where numlistitemid=dbo.GetCustFldValue(numFieldID,@PageId,@numRecordID))
			WHEN vcAssociatedControlType = 'CheckBoxList' then STUFF((SELECT CONCAT(',', vcData) FROM ListDetails WHERE numlistitemid IN (SELECT ISNULL(Id,0) FROM dbo.SplitIDs(dbo.GetCustFldValue(numFieldID,@PageId,@numRecordID),',')) FOR XML PATH('')), 1, 1, '')
			ELSE dbo.GetCustFldValue(numFieldId,@PageId,@numRecordID) end as vcValue,                    
			convert(bit,1) bitCustomField,'' vcPropertyName,  
			convert(bit,1) as bitCanBeUpdated,numListID,case when vcAssociatedControlType = 'SelectBox' THEN dbo.GetCustFldValue(numFieldId,@PageId,@numRecordID) ELSE 0 END numListItemID,'' PopupFunctionName,
		Case when vcAssociatedControlType='SelectBox' then isnull((Select numPrimaryListID from FieldRelationship where numSecondaryListID=numListID and numDomainID=@numDomainId  ),0) else 0 end as ListRelID ,
		Case when vcAssociatedControlType='SelectBox' then (Select count(*) from FieldRelationship where numPrimaryListID=numListID and numDomainID=@numDomainId  ) else 0 end as DependentFields,
		bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,bitFieldMessage,intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,'' AS vcDbColumnName
		,vcToolTip,0 intFieldMaxLength,ISNULL(numFormFieldGroupId,0) AS numFormFieldGroupId,ISNULL(vcGroupName,'') AS vcGroupName,ISNULL(numOrder,0) AS numOrder
		from View_DynamicCustomColumns
			where grp_id=@PageId and numDomainID=@numDomainID and subgrp=0
			and numUserCntID=@numUserCntID  
			AND numFormId=@numFormID and ISNULL(bitCustom,0)=1 and numRelCntType=@numRelCntType AND tintPageType=@tintPageType
    
			order by tintrow,intcoulmn      
	END                          
 end  

	IF @PageId = 153
	BEGIN
		SELECT 
			numFieldId
			,ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName
			,'' as vcURL
			,vcAssociatedControlType as fld_type
			,tintRow
			,tintcolumn as intcoulmn
			,'' as vcValue
			,ISNULL(bitCustom,0) AS bitCustomField
			,vcPropertyName
			,ISNULL(bitAllowEdit,0) AS bitCanBeUpdated
			,numListID
			,0 numListItemID
			,PopupFunctionName
			,Case when vcAssociatedControlType='SelectBox' then isnull((Select numPrimaryListID from FieldRelationship where numSecondaryListID=numListID and numDomainID=@numDomainId  ),0) else 0 end as ListRelID
			,Case when vcAssociatedControlType='SelectBox' then (Select count(*) from FieldRelationship where numPrimaryListID=numListID and numDomainID=@numDomainId  ) else 0 end as DependentFields,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			bitFieldMessage,intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,vcDbColumnName,vcToolTip,intFieldMaxLength,ISNULL(numFormFieldGroupId,0) AS numFormFieldGroupId,ISNULL(vcGroupName,'') AS vcGroupName,ISNULL(numOrder,0) AS numOrder
		FROM 
			View_DynamicColumns
		WHERE 
			numFormId=@numFormID 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID  
			AND ISNULL(bitCustom,0)=0 
			AND numRelCntType=@numRelCntType 
			AND tintPageType=@tintPageType 
		ORDER BY 
			tintrow,intcoulmn   
	END 
               
if @PageId= 0            
  begin         
  SELECT HDR.numFieldId,HDR.vcFieldName,'' as vcURL,'' as fld_type,DTL.tintRow,DTL.intcoulmn,          
  HDR.vcDBColumnName,convert(char(1),DTL.bitCustomField)as bitCustomField,
  0 as bitIsRequired,0 bitIsEmail,0 bitIsAlphaNumeric,0 bitIsNumeric, 0 bitIsLengthValidation,0 bitFieldMessage,0 intMaxLength,0 intMinLength, 0 bitFieldMessage,'' vcFieldMessage
   from PageLayoutdtl DTL                                    
  join pagelayout HDR on DTl.numFieldId= HDR.numFieldId                                    
   where HDR.Ctype = @charCoType and numUserCntId=@numUserCntId  and bitCustomField=0 and            
  numDomainId = @numDomainId and DTL.numRelCntType=@numRelCntType   order by DTL.tintrow,intcoulmn          
  end                
end                 



ELSE IF @tintPageType=2 and @numRelCntType>3 and 
((select isnull(sum(TotalRow),0) from (Select count(*) TotalRow from View_DynamicColumns where numFormId=@numFormID and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=@tintPageType AND numFormID IN(36) AND numRelCntType=2 
Union 
Select count(*) TotalRow from View_DynamicCustomColumns where numFormId=@numFormID and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=@tintPageType AND numFormID IN(36) AND numRelCntType=2) TotalRows
) <> 0 )
BEGIN

	exec usp_GetTableInfoDefault @numUserCntID=@numUserCntID,@numRecordID=@numRecordID,@numDomainId=@numDomainId,@charCoType=@charCoType,@pageId=@pageId,
			@numRelCntType=2,@numFormID=@numFormID,@tintPageType=@tintPageType
END
                           
ELSE IF @charCoType<>'b'/*added by kamal to prevent showing of all  fields on checkout by default*/                  
BEGIN                 
	DECLARE @IsMasterConfAvailable BIT = 0
	DECLARE @numUserGroup NUMERIC(18,0)                                    

	SELECT @numUserGroup = numGroupID FROM UserMaster WHERE numUserDetailId = @numUserCntID   
       
	IF @pageid=1 or @pageid=4 or @pageid= 12 or  @pageid= 13 or  @pageid= 14                
	BEGIN   
		--Check if Master Form Configuration is created by administrator if NoColumns=0

		IF (SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = @numDomainID AND numFormID = @numFormID AND numRelCntType=@numRelCntType AND bitGridConfiguration = 0 AND numGroupID = @numUserGroup) > 0
	BEGIN
		SET @IsMasterConfAvailable = 1
	END

		--If MasterConfiguration is available then load it otherwise load default columns
		IF @IsMasterConfAvailable = 1
		BEGIN
			SELECT 
				numFieldId,ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName
				,'' as vcURL,vcAssociatedControlType as fld_type,tintRow,tintcolumn as intcoulmn,          
				 '' as vcValue,convert(bit,0) AS bitCustomField,vcPropertyName,ISNULL(bitAllowEdit,0) AS bitCanBeUpdated,
				 0 Tabletype,numListID,0 numListItemID,PopupFunctionName,
				Case when vcAssociatedControlType='SelectBox' then isnull((Select numPrimaryListID from FieldRelationship where numSecondaryListID=numListID and numDomainID=@numDomainId  ),0) else 0 end as ListRelID, 
				Case when vcAssociatedControlType='SelectBox' then (Select count(*) from FieldRelationship where numPrimaryListID=numListID and numDomainID=@numDomainId  ) else 0 end as DependentFields,
				bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
				bitFieldMessage,intMaxLength,intMinLength,bitFieldMessage,ISNULL(vcFieldMessage,'') vcFieldMessage
				,vcDbColumnName,vcToolTip,intFieldMaxLength,ISNULL(numFormFieldGroupId,0) AS numFormFieldGroupId,ISNULL(vcGroupName,'') AS vcGroupName,ISNULL(numOrder,0) AS numOrder
			FROM 
				View_DynamicColumnsMasterConfig
			WHERE
				View_DynamicColumnsMasterConfig.numFormId=@numFormID AND 
				View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
				View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
				View_DynamicColumnsMasterConfig.numRelCntType=@numRelCntType AND 
				View_DynamicColumnsMasterConfig.bitGridConfiguration = 0 AND
				ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
			UNION
			SELECT 
				numFieldId as numFieldId,vcFieldName as vcfieldName ,vcURL,vcAssociatedControlType,               
				tintrow,tintColumn,  
				case when vcAssociatedControlType = 'SelectBox' then (select vcData from listdetails where numlistitemid=dbo.GetCustFldValue(numFieldId,@PageId,@numRecordID))
				WHEN vcAssociatedControlType = 'CheckBoxList' then STUFF((SELECT CONCAT(',', vcData) FROM ListDetails WHERE numlistitemid IN (SELECT ISNULL(Id,0) FROM dbo.SplitIDs(dbo.GetCustFldValue(numFieldID,@PageId,@numRecordID),',')) FOR XML PATH('')), 1, 1, '')
				 else dbo.GetCustFldValue(numFieldId,@PageId,@numRecordID) end as vcValue,                    
				 convert(bit,1) bitCustomField,'' vcPropertyName,  
				 convert(bit,1) as bitCanBeUpdated,1 Tabletype,numListID,case when vcAssociatedControlType = 'SelectBox' THEN dbo.GetCustFldValue(numFieldId,@PageId,@numRecordID) ELSE 0 END numListItemID,'' PopupFunctionName,
				Case when vcAssociatedControlType='SelectBox' then isnull((Select numPrimaryListID from FieldRelationship where numSecondaryListID=numListID and numDomainID=@numDomainId  ),0) else 0 end as ListRelID ,
				Case when vcAssociatedControlType='SelectBox' then (Select count(*) from FieldRelationship where numPrimaryListID=numListID and numDomainID=@numDomainId  ) else 0 end as DependentFields,
				ISNULL(bitIsRequired,0) bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,bitFieldMessage,intMaxLength,intMinLength,bitFieldMessage,ISNULL(vcFieldMessage,'') vcFieldMessage
				,'' AS vcDbColumnName,vcToolTip,0 intFieldMaxLength,ISNULL(numFormFieldGroupId,0) AS numFormFieldGroupId,ISNULL(vcGroupName,'') AS vcGroupName,ISNULL(numOrder,0) AS numOrder
			FROM 
				View_DynamicCustomColumnsMasterConfig
			WHERE 
				View_DynamicCustomColumnsMasterConfig.numFormId=@numFormID AND 
				View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
				View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
				View_DynamicCustomColumnsMasterConfig.numRelCntType=@numRelCntType AND 
				View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 0 AND
				ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1
			ORDER BY 
				tintrow,intcoulmn  
		END
		ELSE                                        
		BEGIN
			SELECT numFieldId,ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName
        ,'' as vcURL,vcAssociatedControlType as fld_type,tintRow,tintcolumn as intcoulmn,          
         '' as vcValue,convert(bit,0) AS bitCustomField,vcPropertyName,ISNULL(bitAllowEdit,0) AS bitCanBeUpdated,
         0 Tabletype,numListID,0 numListItemID,PopupFunctionName,
Case when vcAssociatedControlType='SelectBox' then isnull((Select numPrimaryListID from FieldRelationship where numSecondaryListID=numListID and numDomainID=@numDomainId  ),0) else 0 end as ListRelID, 
Case when vcAssociatedControlType='SelectBox' then (Select count(*) from FieldRelationship where numPrimaryListID=numListID and numDomainID=@numDomainId  ) else 0 end as DependentFields,
bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
bitFieldMessage,intMaxLength,intMinLength,bitFieldMessage,ISNULL(vcFieldMessage,'') vcFieldMessage
,vcDbColumnName,vcToolTip,intFieldMaxLength,0 AS numFormFieldGroupId,'' AS vcGroupName,0 AS numOrder
FROM View_DynamicDefaultColumns 
  where numFormId=@numFormID AND numDomainID=@numDomainID AND
   1=(CASE WHEN @tintPageType=2 THEN CASE WHEN ISNULL(bitAddField,0)=1 THEN 1 ELSE 0 end 
		  WHEN @tintPageType=3 THEN CASE WHEN ISNULL(bitDetailField,0)=1 THEN 1 ELSE 0 END 
		  ELSE 0 END) 
                   
   union     
  
     select fld_id as numFieldId,fld_label as vcfieldName ,vcURL,fld_type,               
   convert(tinyint,0) as tintrow,convert(int,0) as intcoulmn,  
 case when fld_type = 'SelectBox' then (select vcData from listdetails where numlistitemid=dbo.GetCustFldValue(fld_id,@PageId,@numRecordID))                
 WHEN fld_type = 'CheckBoxList' then STUFF((SELECT CONCAT(',', vcData) FROM ListDetails WHERE numlistitemid IN (SELECT ISNULL(Id,0) FROM dbo.SplitIDs(dbo.GetCustFldValue(fld_id,@PageId,@numRecordID),',')) FOR XML PATH('')), 1, 1, '')
 else dbo.GetCustFldValue(fld_id,@PageId,@numRecordID) end as vcValue,                    
 convert(bit,1) bitCustomField,'' vcPropertyName,  
 convert(bit,1) as bitCanBeUpdated,1 Tabletype,CFM.numListID,case when fld_type = 'SelectBox' THEN dbo.GetCustFldValue(fld_id,@PageId,@numRecordID) ELSE 0 END numListItemID,'' PopupFunctionName,
Case when fld_type='SelectBox' then isnull((Select numPrimaryListID from FieldRelationship where numSecondaryListID=CFM.numListID and numDomainID=@numDomainId  ),0) else 0 end as ListRelID ,
Case when fld_type='SelectBox' then (Select count(*) from FieldRelationship where numPrimaryListID=CFM.numListID and numDomainID=@numDomainId  ) else 0 end as DependentFields,
ISNULL(V.bitIsRequired,0) bitIsRequired,V.bitIsEmail,V.bitIsAlphaNumeric,V.bitIsNumeric,V.bitIsLengthValidation,V.bitFieldMessage,V.intMaxLength,V.intMinLength,V.bitFieldMessage,ISNULL(V.vcFieldMessage,'') vcFieldMessage
,'' AS vcDbColumnName,CFM.vcToolTip,0 intFieldMaxLength,0 AS numFormFieldGroupId,'' AS vcGroupName,0 AS numOrder
from CFW_Fld_Master CFM join CFW_Fld_Dtl CFD on Fld_id=numFieldId                                          
 left join CFw_Grp_Master CFG on subgrp=CFG.Grp_id                                          
 LEFT JOIN CFW_Validation V ON V.numFieldID = CFM.Fld_id
 where CFM.grp_id=@PageId and CFD.numRelation=@numRelCntType and CFM.numDomainID=@numDomainID and subgrp=0

 order by tintrow,intcoulmn                        
		END     
	END      
             
	IF @PageId= 2 or  @PageId= 3 or @PageId= 5 or @PageId= 6 or @PageId= 7 or @PageId= 8  or @PageId= 11                          
	BEGIN  
		--Check if Master Form Configuration is created by administrator if NoColumns=0

		IF (SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = @numDomainID AND numFormID = @numFormID AND numRelCntType=@numRelCntType AND bitGridConfiguration = 0 AND numGroupID = @numUserGroup) > 0
		BEGIN
			SET @IsMasterConfAvailable = 1
		END

		--If MasterConfiguration is available then load it otherwise load default columns
		IF @IsMasterConfAvailable = 1
		BEGIN
			SELECT 
				numFieldId,ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName
				,'' as vcURL,vcAssociatedControlType as fld_type,tintRow,tintcolumn as intcoulmn,          
				 '' as vcValue,convert(bit,0) AS bitCustomField,vcPropertyName,ISNULL(bitAllowEdit,0) AS bitCanBeUpdated,
				 0 Tabletype,numListID,0 numListItemID,PopupFunctionName,
				Case when vcAssociatedControlType='SelectBox' then isnull((Select numPrimaryListID from FieldRelationship where numSecondaryListID=numListID and numDomainID=@numDomainId  ),0) else 0 end as ListRelID, 
				Case when vcAssociatedControlType='SelectBox' then (Select count(*) from FieldRelationship where numPrimaryListID=numListID and numDomainID=@numDomainId  ) else 0 end as DependentFields,
				bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
				bitFieldMessage,intMaxLength,intMinLength,bitFieldMessage,ISNULL(vcFieldMessage,'') vcFieldMessage
				,vcDbColumnName,vcToolTip,intFieldMaxLength,ISNULL(numFormFieldGroupId,0) AS numFormFieldGroupId,ISNULL(vcGroupName,'') AS vcGroupName,ISNULL(numOrder,0) AS numOrder
			FROM 
				View_DynamicColumnsMasterConfig
			WHERE
				View_DynamicColumnsMasterConfig.numFormId=@numFormID AND 
				View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
				View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
				View_DynamicColumnsMasterConfig.numRelCntType=@numRelCntType AND 
				View_DynamicColumnsMasterConfig.bitGridConfiguration = 0 AND
				ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
			UNION
			SELECT 
				numFieldId as numFieldId,vcFieldName as vcfieldName ,vcURL,vcAssociatedControlType,               
				tintrow,tintColumn,  
				case when vcAssociatedControlType = 'SelectBox' then (select vcData from listdetails where numlistitemid=dbo.GetCustFldValue(numFieldId,@PageId,@numRecordID))
				WHEN vcAssociatedControlType = 'CheckBoxList' then STUFF((SELECT CONCAT(',', vcData) FROM ListDetails WHERE numlistitemid IN (SELECT ISNULL(Id,0) FROM dbo.SplitIDs(dbo.GetCustFldValue(numFieldId,@PageId,@numRecordID),',')) FOR XML PATH('')), 1, 1, '')
				 else dbo.GetCustFldValue(numFieldId,@PageId,@numRecordID) end as vcValue,                    
				 convert(bit,1) bitCustomField,'' vcPropertyName,  
				 convert(bit,1) as bitCanBeUpdated,1 Tabletype,numListID,case when vcAssociatedControlType = 'SelectBox' THEN dbo.GetCustFldValue(numFieldId,@PageId,@numRecordID) ELSE 0 END numListItemID,'' PopupFunctionName,
				Case when vcAssociatedControlType='SelectBox' then isnull((Select numPrimaryListID from FieldRelationship where numSecondaryListID=numListID and numDomainID=@numDomainId  ),0) else 0 end as ListRelID ,
				Case when vcAssociatedControlType='SelectBox' then (Select count(*) from FieldRelationship where numPrimaryListID=numListID and numDomainID=@numDomainId  ) else 0 end as DependentFields,
				ISNULL(bitIsRequired,0) bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,bitFieldMessage,intMaxLength,intMinLength,bitFieldMessage,ISNULL(vcFieldMessage,'') vcFieldMessage
				,'' AS vcDbColumnName,vcToolTip,0 intFieldMaxLength,ISNULL(numFormFieldGroupId,0) AS numFormFieldGroupId,ISNULL(vcGroupName,'') AS vcGroupName,ISNULL(numOrder,0) AS numOrder
			FROM 
				View_DynamicCustomColumnsMasterConfig
			WHERE 
				View_DynamicCustomColumnsMasterConfig.numFormId=@numFormID AND 
				View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
				View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
				View_DynamicCustomColumnsMasterConfig.numRelCntType=@numRelCntType AND 
				View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 0 AND
				ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1
			ORDER BY 
				tintrow,intcoulmn  
		END
		ELSE                                        
		BEGIN
			SELECT numFieldId,ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName
			,'' as vcURL,vcAssociatedControlType as fld_type,tintRow,tintColumn as intcoulmn,          
			 '' as vcValue,convert(bit,0) AS bitCustomField,vcPropertyName,ISNULL(bitAllowEdit,0) AS bitCanBeUpdated,
			 0 Tabletype,numListID,0 numListItemID,PopupFunctionName,
			Case when vcAssociatedControlType='SelectBox' then isnull((Select numPrimaryListID from FieldRelationship where numSecondaryListID=numListID and numDomainID=@numDomainId  ),0) else 0 end as ListRelID, 
			Case when vcAssociatedControlType='SelectBox' then (Select count(*) from FieldRelationship where numPrimaryListID=numListID and numDomainID=@numDomainId  ) else 0 end as DependentFields,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			bitFieldMessage,intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage
			,vcDbColumnName,vcToolTip,intFieldMaxLength,0 AS numFormFieldGroupId,'' AS vcGroupName,0 AS numOrder
			FROM View_DynamicDefaultColumns DFV
			where numFormId=@numFormID AND numDomainID=@numDomainID AND
			1=(CASE WHEN @tintPageType=2 THEN CASE WHEN ISNULL(bitAddField,0)=1 THEN 1 ELSE 0 end 
			  WHEN @tintPageType=3 THEN CASE WHEN ISNULL(bitDetailField,0)=1 THEN 1 ELSE 0 END 
			  ELSE 0 END)   
			UNION 
			select fld_id as numFieldId,fld_label as vcfieldName ,vcURL,fld_type,               
			convert(tinyint,0) as tintRow,convert(int,0) as intcoulmn,
			case when fld_type = 'SelectBox' then (select vcData from listdetails where numlistitemid=dbo.GetCustFldValue(fld_id,@PageId,@numRecordID))
			WHEN fld_type = 'CheckBoxList' then STUFF((SELECT CONCAT(',', vcData) FROM ListDetails WHERE numlistitemid IN (SELECT ISNULL(Id,0) FROM dbo.SplitIDs(dbo.GetCustFldValue(fld_id,@PageId,@numRecordID),',')) FOR XML PATH('')), 1, 1, '')
			else dbo.GetCustFldValue(fld_id,@PageId,@numRecordID) end as vcValue,                    
			convert(bit,1) bitCustomField,'' vcPropertyName,  
			convert(bit,1) as bitCanBeUpdated,1 Tabletype,CFM.numListID,case when fld_type = 'SelectBox' THEN dbo.GetCustFldValue(fld_id,@PageId,@numRecordID) ELSE 0 END numListItemID,'' PopupFunctionName,
			Case when fld_type='SelectBox' then isnull((Select numPrimaryListID from FieldRelationship where numSecondaryListID=CFM.numListID and numDomainID=@numDomainId  ),0) else 0 end as ListRelID ,
			Case when fld_type='SelectBox' then (Select count(*) from FieldRelationship where numPrimaryListID=CFM.numListID and numDomainID=@numDomainId  ) else 0 end as DependentFields,
			ISNULL(V.bitIsRequired,0) bitIsRequired,V.bitIsEmail,V.bitIsAlphaNumeric,V.bitIsNumeric,V.bitIsLengthValidation,V.bitFieldMessage,V.intMaxLength,V.intMinLength,V.bitFieldMessage,ISNULL(V.vcFieldMessage,'') vcFieldMessage,'' AS vcDbColumnName
			,CFM.vcToolTip,0 intFieldMaxLength,0 AS numFormFieldGroupId,'' AS vcGroupName,0 AS numOrder
			FROM CFW_Fld_Master CFM left join CFW_Fld_Dtl CFD on Fld_id=numFieldId  
			left join CFw_Grp_Master CFG on subgrp=CFG.Grp_id                                    
			LEFT JOIN CFW_Validation V ON V.numFieldID = CFM.Fld_id
			WHERE CFM.grp_id=@PageId and CFM.numDomainID=@numDomainID   and subgrp=0           
			ORDER BY tintrow,intcoulmn                             
		END
	END   
	      
	IF @PageId= 153                      
	BEGIN  
		--Check if Master Form Configuration is created by administrator if NoColumns=0
		IF (SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = @numDomainID AND numFormID = @numFormID AND numRelCntType=@numRelCntType AND bitGridConfiguration = 0 AND numGroupID = @numUserGroup) > 0
		BEGIN
			SET @IsMasterConfAvailable = 1
		END

		--If MasterConfiguration is available then load it otherwise load default columns
		IF @IsMasterConfAvailable = 1
		BEGIN
			SELECT 
				numFieldId,ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName
				,'' as vcURL,vcAssociatedControlType as fld_type,tintRow,tintcolumn as intcoulmn,          
				 '' as vcValue,convert(bit,0) AS bitCustomField,vcPropertyName,ISNULL(bitAllowEdit,0) AS bitCanBeUpdated,
				 0 Tabletype,numListID,0 numListItemID,PopupFunctionName,
				Case when vcAssociatedControlType='SelectBox' then isnull((Select numPrimaryListID from FieldRelationship where numSecondaryListID=numListID and numDomainID=@numDomainId  ),0) else 0 end as ListRelID, 
				Case when vcAssociatedControlType='SelectBox' then (Select count(*) from FieldRelationship where numPrimaryListID=numListID and numDomainID=@numDomainId  ) else 0 end as DependentFields,
				bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
				bitFieldMessage,intMaxLength,intMinLength,bitFieldMessage,ISNULL(vcFieldMessage,'') vcFieldMessage
				,vcDbColumnName,vcToolTip,intFieldMaxLength,ISNULL(numFormFieldGroupId,0) AS numFormFieldGroupId,ISNULL(vcGroupName,'') AS vcGroupName
			FROM 
				View_DynamicColumnsMasterConfig
			WHERE
				View_DynamicColumnsMasterConfig.numFormId=@numFormID AND 
				View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
				View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
				View_DynamicColumnsMasterConfig.numRelCntType=@numRelCntType AND 
				View_DynamicColumnsMasterConfig.bitGridConfiguration = 0 AND
				ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0 
		END
		ELSE                                        
		BEGIN
			SELECT numFieldId,ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName
			,'' as vcURL,vcAssociatedControlType as fld_type,tintRow,tintColumn as intcoulmn,          
			 '' as vcValue,convert(bit,0) AS bitCustomField,vcPropertyName,ISNULL(bitAllowEdit,0) AS bitCanBeUpdated,
			 0 Tabletype,numListID,0 numListItemID,PopupFunctionName,
			Case when vcAssociatedControlType='SelectBox' then isnull((Select numPrimaryListID from FieldRelationship where numSecondaryListID=numListID and numDomainID=@numDomainId  ),0) else 0 end as ListRelID, 
			Case when vcAssociatedControlType='SelectBox' then (Select count(*) from FieldRelationship where numPrimaryListID=numListID and numDomainID=@numDomainId  ) else 0 end as DependentFields,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			bitFieldMessage,intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage
			,vcDbColumnName,vcToolTip,intFieldMaxLength,0 AS numFormFieldGroupId,'' AS vcGroupName
			FROM View_DynamicDefaultColumns DFV
			where numFormId=@numFormID AND numDomainID=@numDomainID AND bitDefault=1 AND
			1=(CASE WHEN @tintPageType=2 THEN CASE WHEN ISNULL(bitAddField,0)=1 THEN 1 ELSE 0 end 
			  WHEN @tintPageType=3 THEN CASE WHEN ISNULL(bitDetailField,0)=1 THEN 1 ELSE 0 END 
			  ELSE 0 END)             
			ORDER BY tintrow,intcoulmn                             
		END
	END  

	IF @PageId= 0            
	BEGIN         
	   SELECT numFieldID,vcFieldName,'' as vcURL,'' as fld_type,vcDBColumnName,tintRow,intcolumn as intcoulmn,          
		'0' as bitCustomField,Ctype,'0' as tabletype,
		0 as bitIsRequired,0 bitIsEmail,0 bitIsAlphaNumeric,0 bitIsNumeric, 0 bitIsLengthValidation,0 bitFieldMessage,0 intMaxLength,0 intMinLength, 0 bitFieldMessage,'' vcFieldMessage
		from PageLayout where Ctype = @charCoType    order by tintrow,intcoulmn           
	END                       
END  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p' AND NAME ='USP_Item_ElasticSearchBizCart_GetDynamicValues')
DROP PROCEDURE USP_Item_ElasticSearchBizCart_GetDynamicValues
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[USP_Item_ElasticSearchBizCart_GetDynamicValues]
@numSiteId AS NUMERIC(18,0)
,@numDivisionID AS NUMERIC(18,0) = 0
,@vcItemCodes AS NVARCHAR(MAX) = ''
,@IsAvaiablityTokenCheck AS BIT = 0
AS 
BEGIN
	DECLARE @numDomainID AS NUMERIC(18,0) = 0
	DECLARE @numDefaultRelationship AS NUMERIC(18,0) = 0
	DECLARE @numDefaultProfile AS NUMERIC(18,0) = 0
	DECLARE @bitShowInStock AS BIT = 0
	Declare @isPromotionExist as bit = 0 



	SELECT  
		@numDomainID = numDomainID
    FROM 
		[Sites]
    WHERE 
		numSiteID = @numSiteID

	IF ISNULL(@numDivisionID,0) > 0
		BEGIN
		SELECT 
			@numDefaultRelationship=ISNULL(numCompanyType,0)
			,@numDefaultProfile=ISNULL(vcProfile,0)
		FROM 
			DivisionMaster 
		INNER JOIN 
			CompanyInfo 
		ON 
			DivisionMaster.numCompanyID=CompanyInfo.numCompanyId 
		WHERE 
			numDivisionID=@numDivisionID 
			AND DivisionMaster.numDomainID=@numDomainID
	END

	SET @isPromotionExist = IIF(EXISTS(select 1 from PromotionOffer where numDomainId=@numDomainID),1,0)


	--KEEP THIS BELOW ABOVE IF CONDITION
	SELECT 
		@numDefaultRelationship=(CASE WHEN ISNULL(@numDivisionID,0) > 0 THEN @numDefaultRelationship ELSE numRelationshipId END)
		,@numDefaultProfile=(CASE WHEN ISNULL(@numDivisionID,0) > 0 THEN @numDefaultProfile ELSE numProfileId END)
		,@bitShowInStock = bitShowInStock
	FROM 
		dbo.eCommerceDTL 
	WHERE 
		numSiteID = @numSiteId

	SELECT 
		numItemCode
		,vcItemName
		,numTotalPromotions
		,vcPromoDesc
		,bitRequireCouponCode
		,numItemClassification
		,(CASE  
			WHEN @IsAvaiablityTokenCheck = 0 THEN 0 
				ELSE (CASE WHEN charItemType <> 'S' AND charItemType <> 'N' AND ISNULL(bitKitParent,0)=0
						THEN (CASE 
								WHEN bitAllowBackOrder = 1 THEN 1
								WHEN (ISNULL(numOnHand,0)<=0) THEN 0
								ELSE 1
							END)
						ELSE 1
				END)
		END) AS bitInStock
		,(CASE  
			WHEN @IsAvaiablityTokenCheck = 0 THEN '' 
				ELSE (CASE WHEN charItemType <> 'S' AND charItemType <> 'N'
					THEN (CASE WHEN @bitShowInStock = 1
								THEN (CASE 
										WHEN bitAllowBackOrder = 1 AND numDomainID <> 172 AND ISNULL(numOnHand,0)<=0 AND ISNULL(numVendorID,0) > 0 AND dbo.GetVendorPreferredMethodLeadTime(numVendorID) > 0 THEN CONCAT('<font class="vendorLeadTime" style="color:green">Usually ships in ',dbo.GetVendorPreferredMethodLeadTime(numVendorID),' days</font>')
										WHEN bitAllowBackOrder = 1 AND ISNULL(numOnHand,0)<=0 THEN (CASE WHEN numDomainID = 172 THEN '<font style="color:green">AVAILABLE</font>' ELSE '<font style="color:green">In Stock</font>' END)
										WHEN (ISNULL(numOnHand,0)<=0) THEN (CASE WHEN numDomainID = 172 THEN '<font style="color:red">ON HOLD</font>' ELSE '<font style="color:red">Out Of Stock</font>' END)
										ELSE (CASE WHEN numDomainID = 172 THEN '<font style="color:green">AVAILABLE</font>' ELSE '<font style="color:green">In Stock</font>' END)  
									END)
								ELSE ''
							END)
					ELSE ''
				END)
			END) AS InStock 
			INTO #TempTbl
	FROM 
		(SELECT 
			I.numItemCode
			,I.vcItemName
			,I.charItemType
			,I.bitKitParent
			,I.bitAllowBackOrder
			,I.numDomainID
			,I.numVendorID
			,I.numItemClassification
			,CONVERT(INT,0) numTotalPromotions
			,CONVERT(NVARCHAR(100),'') vcPromoDesc
			,CONVERT(BIT,0) bitRequireCouponCode
			,SUM(ISNULL(WHI.numOnHand,0)) AS numOnHand
		FROM Item I 
		INNER JOIN
		(
			SELECT [OutParam] FROM dbo.[SplitString](@vcItemCodes,',')
		) TEMPItem
		ON
			I.numItemCode = TEMPItem.OutParam
		LEFT JOIN WareHouseItems WHI ON (WHI.numItemID = I.numItemCode)
		--OUTER APPLY (SELECT * FROM dbo.fn_GetItemPromotions(I.numDomainID,@numDivisionID,numItemCode,numItemClassification,@numDefaultRelationship,@numDefaultProfile,1)) TablePromotion
		WHERE 
			WHI.numdomainID = @numDomainID 
		GROUP BY 
			I.numItemCode
			,I.vcItemName
			,I.charItemType
			,I.bitKitParent
			,I.bitAllowBackOrder
			,I.numDomainID
			,I.numVendorID
			,I.numItemClassification
			--,ISNULL(TablePromotion.numTotalPromotions,0)
			--,ISNULL(TablePromotion.vcPromoDesc,'''') 
			--,ISNULL(TablePromotion.bitRequireCouponCode,0) 
		)T


		IF(@isPromotionExist =1)
		BEGIN 
			UPDATE t1 
			SET t1.numTotalPromotions = ISNULL(TablePromotion.numTotalPromotions,0),
			t1.vcPromoDesc = TablePromotion.vcPromoDesc,
			t1.bitRequireCouponCode = ISNULL(TablePromotion.bitRequireCouponCode,0)
			FROM #TempTbl t1
			OUTER APPLY (SELECT * FROM dbo.fn_GetItemPromotions(@numDomainID,@numDivisionID,numItemCode,t1.numItemClassification,@numDefaultRelationship,@numDefaultProfile,1)) TablePromotion
		END

		SELECT * From #TempTbl

		
	IF OBJECT_ID('tempdb..#TempTbl') IS NOT NULL
	DROP TABLE #TempTbl
END


GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageReportListMaster')
DROP PROCEDURE USP_ManageReportListMaster
GO
CREATE PROCEDURE [dbo].[USP_ManageReportListMaster]                                        
	@numReportID numeric(18, 0) OUTPUT,
    @vcReportName varchar(200),
    @vcReportDescription varchar(500),
    @numDomainID numeric(18, 0),
    @numReportModuleID numeric(18, 0),
    @numReportModuleGroupID numeric(18, 0),
    @tintReportType tinyint,
    @numUserCntID numeric(18, 0),
    @bitActive bit,
    @textQuery text,
    @numSortColumnmReportFieldGroupID NUMERIC(18,0),
    @numSortColumnFieldID numeric(18, 0),
    @vcSortColumnDirection varchar(10),
    @bitSortColumnCustom BIT,
    @vcFilterLogic varchar(200),
    @numDateReportFieldGroupID NUMERIC(18,0),
    @numDateFieldID NUMERIC(18),
    @bitDateFieldColumnCustom BIT,
    @vcDateFieldValue VARCHAR(50),
    @dtFromDate DATETIME,
    @dtToDate DATETIME,
    @tintRecordFilter TINYINT,
    @strText TEXT = '',
    @intNoRows INT,
    @vcKPIMeasureFieldID VARCHAR(50),
    @bitDuplicate BIT=0,
    @bitHideSummaryDetail BIT=0
as                 

IF @bitDuplicate=1
BEGIN
	INSERT INTO [dbo].[ReportListMaster] ([vcReportName], [vcReportDescription], [numDomainID], [numReportModuleID], [numReportModuleGroupID], [tintReportType], [numCreatedBy], [numModifiedBy], [dtCreatedDate], [dtModifiedDate], [bitActive], [textQuery], [numSortColumnmReportFieldGroupID], [numSortColumnFieldID], [vcSortColumnDirection], [bitSortColumnCustom], [vcFilterLogic], [numDateReportFieldGroupID], [numDateFieldID], [bitDateFieldColumnCustom], [vcDateFieldValue], [dtFromDate], [dtToDate], [tintRecordFilter], [vcKPIMeasureFieldID], [intNoRows],bitHideSummaryDetail)
	SELECT 'Duplicate - ' + vcReportName, vcReportDescription, numDomainID, numReportModuleID, numReportModuleGroupID, tintReportType, @numUserCntID, @numUserCntID, GETUTCDATE(), GETUTCDATE(), bitActive, textQuery, numSortColumnmReportFieldGroupID, numSortColumnFieldID, vcSortColumnDirection, bitSortColumnCustom, vcFilterLogic, numDateReportFieldGroupID, numDateFieldID, bitDateFieldColumnCustom, vcDateFieldValue, dtFromDate, dtToDate, tintRecordFilter, vcKPIMeasureFieldID, intNoRows,bitHideSummaryDetail
	FROM ReportListMaster WHERE numReportID=@numReportID AND numDomainID=@numDomainID
	
	DECLARE @numOLDReportID AS NUMERIC(18,0);SET @numOLDReportID=@numReportID
	
	SET @numReportID=@@IDENTITY
	
		INSERT INTO [dbo].[ReportFieldsList] ([numReportID], [numFieldID], [bitCustom], [numReportFieldGroupID])
			SELECT @numReportID, numFieldID, bitCustom, numReportFieldGroupID
				FROM ReportFieldsList WHERE numReportID=@numOLDReportID
	
	
		INSERT INTO dbo.ReportFilterList (numReportID,numFieldID,bitCustom,vcFilterValue,vcFilterOperator,vcFilterText,numReportFieldGroupID) 
			SELECT @numReportID,numFieldID,bitCustom,vcFilterValue,vcFilterOperator,vcFilterText,numReportFieldGroupID
			  FROM ReportFilterList WHERE numReportID=@numOLDReportID


		INSERT INTO dbo.ReportSummaryGroupList (numReportID,numFieldID,bitCustom,tintBreakType,numReportFieldGroupID) 
			SELECT @numReportID,numFieldID,bitCustom,tintBreakType,numReportFieldGroupID
			  FROM ReportSummaryGroupList WHERE numReportID=@numOLDReportID


		INSERT INTO dbo.ReportMatrixAggregateList (numReportID,numFieldID,bitCustom,vcAggType,numReportFieldGroupID) 	
				SELECT @numReportID,numFieldID,bitCustom,vcAggType,numReportFieldGroupID
			  FROM ReportMatrixAggregateList WHERE numReportID=@numOLDReportID


		INSERT INTO dbo.ReportKPIThresold (numReportID,vcThresoldType,decValue1,decValue2,decGoalValue) 	
				SELECT @numReportID,vcThresoldType,decValue1,decValue2,decGoalValue
			  FROM ReportKPIThresold WHERE numReportID=@numOLDReportID

END

ELSE
BEGIN
 IF @numSortColumnmReportFieldGroupID = 0 SET @numSortColumnmReportFieldGroupID = NULL  
 IF @numSortColumnFieldID = 0 SET @numSortColumnFieldID = NULL  
 IF @numDateReportFieldGroupID = 0 SET @numDateReportFieldGroupID = NULL  
 IF @numDateFieldID = 0 SET @numDateFieldID = NULL  
 IF @vcKPIMeasureFieldID = '' SET @vcKPIMeasureFieldID = NULL  
 
IF @numReportID=0
BEGIN
	INSERT INTO [dbo].[ReportListMaster] ([vcReportName], [vcReportDescription], [numDomainID], [numReportModuleID], 
		[numReportModuleGroupID], [tintReportType], [numCreatedBy], [dtCreatedDate],
		[bitActive], [textQuery],numSortColumnmReportFieldGroupID, [numSortColumnFieldID], [vcSortColumnDirection], [bitSortColumnCustom], [vcFilterLogic],
		numDateReportFieldGroupID,numDateFieldID,bitDateFieldColumnCustom,vcDateFieldValue,dtFromDate,dtToDate,tintRecordFilter,intNoRows,vcKPIMeasureFieldID,bitHideSummaryDetail)
	SELECT @vcReportName, @vcReportDescription, @numDomainID, @numReportModuleID, 
	    @numReportModuleGroupID, @tintReportType, @numUserCntID, GETUTCDATE(),
	    @bitActive, @textQuery,@numSortColumnmReportFieldGroupID, @numSortColumnFieldID, @vcSortColumnDirection, @bitSortColumnCustom, @vcFilterLogic,
	    @numDateReportFieldGroupID,@numDateFieldID,@bitDateFieldColumnCustom,@vcDateFieldValue,@dtFromDate,@dtToDate,@tintRecordFilter,@intNoRows,@vcKPIMeasureFieldID,@bitHideSummaryDetail
	    
	SET @numReportID=@@IDENTITY
END
ELSE
BEGIN
	UPDATE [dbo].[ReportListMaster]
	SET  vcReportName=@vcReportName,vcReportDescription=@vcReportDescription,[tintReportType] = @tintReportType, [numModifiedBy] = @numUserCntID, [dtModifiedDate] = GETUTCDATE(), 
		 [textQuery] = @textQuery,numSortColumnmReportFieldGroupID=@numSortColumnmReportFieldGroupID, [numSortColumnFieldID] = @numSortColumnFieldID, 
		 [vcSortColumnDirection] = @vcSortColumnDirection, [bitSortColumnCustom] = @bitSortColumnCustom, [vcFilterLogic] = @vcFilterLogic,
		 numDateReportFieldGroupID=@numDateReportFieldGroupID,numDateFieldID=@numDateFieldID,bitDateFieldColumnCustom=@bitDateFieldColumnCustom,vcDateFieldValue=@vcDateFieldValue,
		 dtFromDate=@dtFromDate,dtToDate=@dtToDate,tintRecordFilter=@tintRecordFilter,intNoRows=@intNoRows,vcKPIMeasureFieldID=@vcKPIMeasureFieldID,bitHideSummaryDetail=@bitHideSummaryDetail
	WHERE  [numDomainID] = @numDomainID AND [numReportID] = @numReportID
	
	IF DATALENGTH(@strText)>2
	BEGIN
		DECLARE @hDocItem INT                                                                                                                                                                
	
		EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strText                                                                                                             
        
        --Delete records into ReportFieldsList and Insert
        DELETE FROM ReportFieldsList WHERE  [numReportID] = @numReportID

		INSERT INTO dbo.ReportFieldsList (numReportID,numFieldID,bitCustom,numReportFieldGroupID) 
			SELECT @numReportID,numFieldID,bitCustom,numReportFieldGroupID
			  FROM OPENXML (@hDocItem, '/NewDataSet/ReportFieldsList',2) WITH ( numFieldID NUMERIC, bitCustom BIT ,numReportFieldGroupID NUMERIC )
	
	
		--Delete records into ReportFilterList and Insert
		DELETE FROM ReportFilterList WHERE  [numReportID] = @numReportID

		INSERT INTO dbo.ReportFilterList (numReportID,numFieldID,bitCustom,vcFilterValue,vcFilterOperator,vcFilterText,numReportFieldGroupID) 
			SELECT @numReportID,numFieldID,bitCustom,vcFilterValue,vcFilterOperator,vcFilterText,numReportFieldGroupID
			  FROM OPENXML (@hDocItem, '/NewDataSet/ReportFilterList',2) WITH ( numFieldID NUMERIC, bitCustom BIT, vcFilterValue VARCHAR(500), vcFilterOperator VARCHAR(50),vcFilterText VARCHAR(2000) ,numReportFieldGroupID NUMERIC )


		--Delete records into ReportSummaryGroupList and Insert
		DELETE FROM ReportSummaryGroupList WHERE  [numReportID] = @numReportID

		INSERT INTO dbo.ReportSummaryGroupList (numReportID,numFieldID,bitCustom,tintBreakType,numReportFieldGroupID) 
			SELECT @numReportID,numFieldID,bitCustom,tintBreakType,numReportFieldGroupID
			  FROM OPENXML (@hDocItem, '/NewDataSet/ReportSummaryGroupList',2) WITH ( numFieldID NUMERIC, bitCustom BIT ,tintBreakType TINYINT,numReportFieldGroupID NUMERIC )


		--Delete records into ReportMatrixAggregateList and Insert
		DELETE FROM ReportMatrixAggregateList WHERE  [numReportID] = @numReportID
		
		INSERT INTO dbo.ReportMatrixAggregateList (numReportID,numFieldID,bitCustom,vcAggType,numReportFieldGroupID) 	
				SELECT @numReportID,numFieldID,bitCustom,vcAggType,numReportFieldGroupID
			  FROM OPENXML (@hDocItem, '/NewDataSet/ReportMatrixAggregateList',2) WITH ( numFieldID NUMERIC, bitCustom BIT, vcAggType VARCHAR(50) ,numReportFieldGroupID NUMERIC )


		--Delete records into ReportKPIThresold and Insert
		DELETE FROM ReportKPIThresold WHERE  [numReportID] = @numReportID
		
		INSERT INTO dbo.ReportKPIThresold (numReportID,vcThresoldType,decValue1,decValue2,decGoalValue) 	
				SELECT @numReportID,vcThresoldType,decValue1,decValue2,decGoalValue
			  FROM OPENXML (@hDocItem, '/NewDataSet/ReportKPIThresold',2) WITH ( vcThresoldType VARCHAR(50), decValue1 DECIMAL(18,2), decValue2 DECIMAL(18,2), decGoalValue DECIMAL(18,2))

		EXEC sp_xml_removedocument @hDocItem
	END	
END

DECLARE @numGroupId NUMERIC(18,0)
SELECT @numGroupId=ISNULL(numGroupID,0) FROM UserMaster WHERE numUserDetailId=@numUserCntID

IF NOT EXISTS (SELECT numReportID  FROM ReportDashboardAllowedReports WHERE numDomainID=@numDomainID AND numReportID=@numReportID AND numGrpID=@numGroupId)
BEGIN
	INSERT INTO ReportDashboardAllowedReports
	(
		numDomainId
		,numGrpId
		,numReportId
		,tintReportCategory
	)
	VALUES
	(
		@numDomainId
		,@numGroupId
		,@numReportID
		,0
	)
END

END
	


	
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ManageReturnBizDocs' ) 
    DROP PROCEDURE USP_ManageReturnBizDocs
GO

CREATE PROCEDURE [dbo].[USP_ManageReturnBizDocs]
    (
      @numReturnHeaderID NUMERIC(9) = 0,
      @tintReceiveType TINYINT,
      @numReturnStatus NUMERIC(9),
      @numDomainId NUMERIC(9),
      @numUserCntID NUMERIC(9),
      @numAccountID NUMERIC(9),
      @vcCheckNumber VARCHAR(50),
      @IsCreateRefundReceipt BIT,
      @numItemCode	NUMERIC(18,0)
    )
AS 
BEGIN
BEGIN TRY
BEGIN TRANSACTION
    DECLARE @tintType TINYINT,@tintReturnType TINYINT,@tintOrigReceiveType TINYINT
	
    SELECT @tintReturnType=tintReturnType,@tintOrigReceiveType=tintReceiveType FROM ReturnHeader WHERE numReturnHeaderID = @numReturnHeaderID    

	IF @tintOrigReceiveType = 1
	BEGIN
		RAISERROR('REFUND_RECEIPT_ALREADY_ISSUED', 16, 1)
	END
	ELSE IF @tintOrigReceiveType = 2
	BEGIN
		RAISERROR('CREDIT_MEMO_ALREADY_ISSUED', 16, 1)
	END
	ELSE IF @tintOrigReceiveType = 3
	BEGIN
		RAISERROR('REFUND_ALREADY_ISSUED', 16, 1);
	END
    
	PRINT @tintReturnType
	PRINT @tintReceiveType

    IF @tintReturnType=1 OR @tintReturnType=2 
    BEGIN
		DECLARE @i INT = 1
		DECLARE @COUNT AS INT 
		DECLARE @numTempOppID AS NUMERIC(18,0)
		DECLARE @numTempOppItemID AS NUMERIC(18,0)
		DECLARE @bitTempLotNo AS BIT
		DECLARE @numTempQty AS FLOAT 
		DECLARE @numTempWareHouseItmsDTLID AS INT		

		--SALES RETURN
		IF @tintReturnType = 1
		BEGIN
			UPDATE
				WID
			SET
				WID.numWareHouseItemID = OWSI.numWarehouseItmsID
				,WID.numQty = 1
			FROM
				OppWarehouseSerializedItem OWSI
			INNER JOIN
				WareHouseItmsDTL WID
			ON
				OWSI.numWarehouseItmsDTLID =WID.numWareHouseItmsDTLID
			INNER JOIN
				WareHouseItems
			ON
				OWSI.numWarehouseItmsID = WareHouseItems.numWareHouseItemID
			INNER JOIN
				Item
			ON
				WareHouseItems.numItemID = Item.numItemCode
			WHERE
				OWSI.numReturnHeaderID = @numReturnHeaderID
				AND ISNULL(Item.bitSerialized,0) = 1

			INSERT INTO WareHouseItmsDTL
			(
				numWareHouseItemID
				,vcSerialNo
				,numQty
			)
			SELECT 
				OWSI.numWarehouseItmsID
				,WID.vcSerialNo
				,OWSI.numQty
			FROM
				OppWarehouseSerializedItem OWSI
			INNER JOIN
				WareHouseItmsDTL WID
			ON
				OWSI.numWarehouseItmsDTLID =WID.numWareHouseItmsDTLID
			INNER JOIN
				WareHouseItems
			ON
				OWSI.numWarehouseItmsID = WareHouseItems.numWareHouseItemID
			INNER JOIN
				Item
			ON
				WareHouseItems.numItemID = Item.numItemCode
			WHERE
				OWSI.numReturnHeaderID = @numReturnHeaderID
				AND ISNULL(Item.bitLotNo,0) = 1
		END
		ELSE IF @tintReturnType = 2
		BEGIN
			IF (SELECT
						COUNT(*)
				FROM
					WareHouseItmsDTL WHIDL
				INNER JOIN
					OppWarehouseSerializedItem OWSI
				ON
					WHIDL.numWareHouseItmsDTLID = OWSI.numWarehouseItmsDTLID
					AND WHIDL.numWareHouseItemID = OWSI.numWarehouseItmsID
				INNER JOIN
					WareHouseItems
				ON
					WHIDL.numWareHouseItemID = WareHouseItems.numWareHouseItemID
				INNER JOIN
					Item
				ON
					WareHouseItems.numItemID = Item.numItemCode
				WHERE
					numReturnHeaderID=@numReturnHeaderID
					AND 1 = (CASE 
								WHEN Item.bitLotNo = 1 AND ISNULL(WHIDL.numQty,0) < ISNULL(OWSI.numQty,0) THEN 1 
								WHEN Item.bitSerialized = 1 AND ISNULL(WHIDL.numQty,0) = 0 THEN 1 
								ELSE 0 
							END)
				) > 0
			BEGIN
				RAISERROR ('SERIAL/LOT#_USED', 16, 1 ) ;
			END
			ELSE
			BEGIN
				-- REMOVE SERIAL/LOT# NUUMBER FROM INVENTORY
				UPDATE WHIDL
					SET WHIDL.numQty = (CASE WHEN Item.bitLotNo = 1 THEN ISNULL(WHIDL.numQty,0) - ISNULL(OWSI.numQty,0) ELSE 0 END)
				FROM 
					WareHouseItmsDTL WHIDL
				INNER JOIN
					OppWarehouseSerializedItem OWSI
				ON
					WHIDL.numWareHouseItmsDTLID = OWSI.numWarehouseItmsDTLID
					AND WHIDL.numWareHouseItemID = OWSI.numWarehouseItmsID
				INNER JOIN
					WareHouseItems
				ON
					WHIDL.numWareHouseItemID = WareHouseItems.numWareHouseItemID
				INNER JOIN
					Item
				ON
					WareHouseItems.numItemID = Item.numItemCode
				WHERE
					numReturnHeaderID=@numReturnHeaderID
			END
		END

        EXEC usp_ManageRMAInventory @numReturnHeaderID,@numDomainId,@numUserCntID,1 
    END   
    
	SET @tintType=(CASE When @tintReturnType=1 AND @tintReceiveType=1 THEN 5 
				       When @tintReturnType=1 AND @tintReceiveType=2 THEN 3 
				       When @tintReturnType=2 AND @tintReceiveType=2 THEN 4
				       ELSE 0 
					END) 
		
	PRINT @tintType

	IF 	@tintType>0
	BEGIN
		EXEC dbo.USP_UpdateBizDocNameTemplate @tintType, @numDomainID,@numReturnHeaderID
    END    
    	
    DECLARE @numBizdocTempID AS NUMERIC(18, 0);
	SET @numBizdocTempID=0
        
    IF @tintReturnType=1 OR @tintReturnType=2 
    BEGIN
		IF @tintReceiveType = 2 AND @tintReturnType=1
		BEGIN
		SELECT TOP 1 @numBizdocTempID = numBizDocTempID FROM    BizDocTemplate
			WHERE   numOppType = 1 AND bitDefault = 1 AND bitEnabled = 1
					AND numDomainID = @numDomainID AND numBizDocID IN (SELECT TOP 1 numListItemID
					FROM    dbo.ListDetails WHERE   vcData = 'Sales Credit Memo' AND constFlag = 1 )
		END
		ELSE IF @tintReturnType=1 OR @tintReceiveType=1
        BEGIN 
            SELECT TOP 1 @numBizdocTempID = numBizdocTempID FROM BizDocTemplate
                WHERE   numOppType = 1 AND bitDefault = 1 AND bitEnabled = 1 AND numDomainID = @numDomainID
					AND numBizDocID = ( SELECT TOP 1 numListItemID 
                    FROM    dbo.ListDetails WHERE   vcData = 'Refund Receipt' AND constFlag = 1)
        END
        ELSE IF @tintReceiveType = 2 AND @tintReturnType=2
        BEGIN
                SELECT TOP 1 @numBizdocTempID = numBizDocTempID FROM    BizDocTemplate
                WHERE   numOppType = 2 AND bitDefault = 1 AND bitEnabled = 1
                        AND numDomainID = @numDomainID AND numBizDocID IN (SELECT TOP 1 numListItemID
                        FROM    dbo.ListDetails WHERE   vcData = 'Purchase Credit Memo' AND constFlag = 1 )
        END 
	END
		
	DECLARE @monAmount AS DECIMAL(20,5),@monTotalTax  AS DECIMAL(20,5),@monTotalDiscount AS  DECIMAL(20,5)  
		
	SET @tintType=CASE When @tintReturnType=1 AND @tintReceiveType=1 THEN 9 
				    When @tintReturnType=1 AND @tintReceiveType=2 THEN 7 
				    When @tintReturnType=1 AND @tintReceiveType=3 THEN 9 
				    When @tintReturnType=2 AND @tintReceiveType=2 THEN 8
				    When @tintReturnType=3 THEN 10
				    When @tintReturnType=4 THEN 9 
					END 
				       
	SELECT @monAmount=monAmount,@monTotalTax=monTotalTax,@monTotalDiscount=monTotalDiscount FROM dbo.GetReturnDealAmount(@numReturnHeaderID,@tintType)
		
    UPDATE  dbo.ReturnHeader
    SET     tintReceiveType = @tintReceiveType,
            numReturnStatus = @numReturnStatus,
            numModifiedBy = @numUserCntID,
            dtModifiedDate = GETUTCDATE(),
            numAccountID = @numAccountID,
            vcCheckNumber = @vcCheckNumber,
            IsCreateRefundReceipt = @IsCreateRefundReceipt,
            numBizdocTempID = CASE When @numBizdocTempID>0 THEN @numBizdocTempID ELSE numBizdocTempID END,
            monBizDocAmount= @monAmount + @monTotalTax - @monTotalDiscount,
            numItemCode = @numItemCode
    WHERE   numReturnHeaderID = @numReturnHeaderID
        
	DECLARE @numDepositIDRef AS NUMERIC(18,0)
	SELECT @numDepositIDRef = ISNULL(numDepositIDRef,0) FROM [dbo].[ReturnHeader] AS RH WHERE [RH].[numReturnHeaderID] = @numReturnHeaderID
	PRINT @numDepositIDRef

	IF @numDepositIDRef > 0
	BEGIN
		IF NOT EXISTS(SELECT * FROM [dbo].[DepositeDetails] AS DD WHERE [DD].[numDepositID] = @numDepositIDRef) AND @tintReceiveType = 1 AND @tintReturnType = 4
		BEGIN
			UPDATE dbo.DepositMaster SET [monAppliedAmount] = ISNULL((SELECT SUM([ReturnHeader].[monAmount]) FROM [dbo].[ReturnHeader] 
																	WHERE [ReturnHeader].[numDepositIDRef] = @numDepositIDRef),0)
											,[numReturnHeaderID] = @numReturnHeaderID	
											,[monRefundAmount] = [monDepositAmount] - ISNULL((SELECT SUM([ReturnHeader].[monAmount]) FROM [dbo].[ReturnHeader] WHERE [ReturnHeader].[numDepositIDRef] = @numDepositIDRef),0) 
			WHERE numDepositId=@numDepositIDRef

		END
		ELSE
		BEGIN
			UPDATE dbo.DepositMaster SET [monAppliedAmount] = @monAmount + ISNULL((SELECT SUM([DD].[monAmountPaid]) FROM [dbo].[DepositeDetails] AS DD WHERE [DD].[numDepositID] = @numDepositIDRef),0)
											,[monRefundAmount] = [monDepositAmount] - ISNULL((SELECT SUM([DD].[monAmountPaid]) FROM [dbo].[DepositeDetails] AS DD WHERE [DD].[numDepositID] = @numDepositIDRef),0)
			WHERE numDepositId=@numDepositIDRef						
		END
	END
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS (SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ReportListMaster_WarehouseInventoryValue')
DROP PROCEDURE USP_ReportListMaster_WarehouseInventoryValue
GO


CREATE procedure [dbo].[USP_ReportListMaster_WarehouseInventoryValue] 
@numDomainID numeric
As
Begin
With 
Cte
as
(
Select --top 15 15 AS TotalParentRecords, 
--isnull(WareHouseItems.numOnHand,'0') as OnHand,
isnull(Item.monAverageCost,'0') as AverageCost,
isnull(Warehouses.vcWareHouse,'') as WareHouse,
--CONCAT('~/Items/frmItemList.aspx?Page=InventoryItems&ItemGroup=0&WID=',Warehouses.numWareHouseID) AS URL,
ISNULL(Warehouses.numWareHouseID,'') as WareHouseID,

--isnull((WareHouseItems.numOnHand + WarehouseItems.numAllocation)*(Item.monAverageCost),0)	as InventoryValue	 
(isnull(WareHouseItems.numOnHand,0) + isnull(WarehouseItems.numAllocation,0))*(isnull(Item.monAverageCost,0))as InventoryValue
FROM Item   
Left Join Vendor on Vendor.numDomainId = @numDomainID and Vendor.numVendorID = Item.numVendorID and  Item.numItemCode= Vendor.numItemCode   
join WareHouseItems  on WareHouseItems.numDomainId = @numDomainID and Item.numItemCode=WareHouseItems.numItemID 
LEFT JOIN WarehouseLocation ON WarehouseLocation.numWLocationID = WareHouseItems.numWLocationID   
join Warehouses Warehouses on Warehouses.numDomainId = @numDomainID and Warehouses.numWareHouseID=WareHouseItems.numWareHouseID 
WHERE 1=1 AND Item.numDomainId = @numDomainID AND (ISNULL(WareHouseItems.numOnHand,0) + ISNULL(WareHouseItems.numAllocation,0) >0)  and isnull(Item.monAverageCost,0) >0 
and  isnull(Item.IsArchieve,0) = 0 

)
select cte.WareHouse,
cte.WareHouseID,
--Cte.URL,
cast(SUM(cte.InventoryValue) as DECIMAL(18,0)) as InventoryValue
from  cte group by Cte.WareHouse,Cte.WareHouseID
--Cte.URL 
order by InventoryValue desc

END
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_WarehouseItems_GetByItem')
DROP PROCEDURE USP_WarehouseItems_GetByItem
GO
CREATE PROCEDURE [dbo].[USP_WarehouseItems_GetByItem]
(   
	@numDomainID NUMERIC(18,0)
	,@numItemCode NUMERIC(18,0)
	,@numWarehouseID NUMERIC(18,0)
	,@numWLocationID NUMERIC(18,0)
)
AS
BEGIN
	DECLARE @numBaseUnit AS NUMERIC(18,0)
	DECLARE @numSaleUnit AS NUMERIC(18,0)
	DECLARE @numPurchaseUnit AS NUMERIC(18,0)
	DECLARE @numSaleUOMFactor AS FLOAT
	DECLARE @numPurchaseUOMFactor AS FLOAT

	SELECT 
		@numBaseUnit = ISNULL(numBaseUnit,0),
		@numSaleUnit = ISNULL(numSaleUnit,0),
		@numPurchaseUnit = ISNULL(numPurchaseUnit,0)
	FROM 
		Item 
	WHERE 
		numItemCode=@numItemCode   

	SELECT @numSaleUOMFactor = dbo.fn_UOMConversion(@numBaseUnit,@numItemCode,@numDOmainID,@numSaleUnit)
	SELECT @numPurchaseUOMFactor = dbo.fn_UOMConversion(@numBaseUnit,@numItemCode,@numDOmainID,@numPurchaseUnit)

	SELECT
		WarehouseItems.numWareHouseItemID
		,WarehouseItems.numWarehouseID
		,WarehouseItems.numWLocationID
		,ISNULL(Warehouses.vcWareHouse,'') vcExternalLocation
		,numItemID
		,(CASE WHEN ISNULL(Item.bitAssembly,0) = 1 THEN dbo.fn_GetAssemblyPossibleWOQty(WarehouseItems.numItemID,WarehouseItems.numWareHouseID) ELSE 0 END) numBuildableQty
		,Item.numAssetChartAcntId
		,CASE 
			WHEN ISNULL(Item.bitKitParent,0)=1 then CAST(ISNULL(dbo.fn_GetKitInventoryByWarehouse(Item.numItemCode, WareHouseItems.numWareHouseID),0) AS FLOAT) 
			ELSE ISNULL(numOnHand,0) + ISNULL((SELECT SUM(WI.numOnHand) FROM WareHouseItems WI INNER JOIN WarehouseLocation WL ON WI.numWLocationID=WL.numWLocationID WHERE WI.numDomainID=@numDomainID AND WI.numItemID=@numItemCode AND WI.numWareHouseID=WareHouseItems.numWareHouseID),0)
		END AS [OnHand]
		,CASE WHEN ISNULL(Item.bitKitParent,0)=1 THEN 0 ELSE CAST(ISNULL(numOnOrder,0) AS FLOAT) END as [OnOrder]
		,CASE 
			WHEN ISNULL(Item.bitKitParent,0)=1 OR ISNULL(Item.bitAssembly,0)=1 
			THEN 0 
			ELSE CAST(ISNULL((SELECT 
									SUM(numUnitHour) 
								FROM 
									OpportunityItems OI 
								INNER JOIN 
									OpportunityMaster OM 
								ON 
									OI.numOppID=OM.numOppID 
								WHERE 
									OM.numDomainID=@numDomainID 
									AND tintOppType=1 
									AND tintOppStatus=0 
									AND OI.numItemCode=WareHouseItems.numItemID 
									AND OI.numWarehouseItmsID=WareHouseItems.numWareHouseItemID),0) AS FLOAT) 
		END AS [Requisitions]
		,CASE WHEN ISNULL(Item.bitKitParent,0)=1 THEN 0 ELSE CAST(ISNULL(numAllocation,0) AS FLOAT) END as [Allocation] 
		,CASE WHEN ISNULL(Item.bitKitParent,0)=1 THEN 0 ELSE CAST(ISNULL(numBackOrder,0) AS FLOAT) END as [BackOrder]
		,ISNULL(WareHouseItems.numOnHand,0) + ISNULL(WareHouseItems.numAllocation,0) + ISNULL((SELECT SUM(ISNULL(WI.numOnHand,0) + ISNULL(WI.numAllocation,0)) FROM WareHouseItems WI INNER JOIN WarehouseLocation WL ON WI.numWLocationID=WL.numWLocationID WHERE WI.numDomainID=@numDomainID AND WI.numItemID=@numItemCode AND WI.numWareHouseID=WareHouseItems.numWareHouseID),0) AS TotalOnHand
		,ISNULL(Item.monAverageCost,0) monAverageCost
		,((ISNULL(WareHouseItems.numOnHand,0) + ISNULL(WareHouseItems.numAllocation,0)) * (CASE WHEN ISNULL(Item.bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(Item.monAverageCost,0) END)) AS monCurrentValue
		,ROUND(ISNULL(WareHouseItems.monWListPrice,0),2) Price
		,ISNULL(WareHouseItems.vcWHSKU,'') as SKU
		,ISNULL(WareHouseItems.vcBarCode,'') as BarCode
		,CASE 
			WHEN ISNULL(Item.numItemGroup,0) > 0 
			THEN dbo.fn_GetAttributes(WareHouseItems.numWareHouseItemID,0)
			ELSE ''
		END AS vcAttribute
		,dbo.fn_GetUOMName(Item.numBaseUnit) As vcBaseUnit
		,dbo.fn_GetUOMName(Item.numSaleUnit) As vcSaleUnit
		,dbo.fn_GetUOMName(Item.numPurchaseUnit) As vcPurchaseUnit
		,CAST((@numSaleUOMFactor * Case when ISNULL(Item.bitKitParent,0)=1 THEN ISNULL(dbo.fn_GetKitInventoryByWarehouse(Item.numItemCode, Warehouses.numWareHouseID),0) ELSE isnull(numOnHand,0) end) as numeric(18,2)) as OnHandUOM
		,CAST((@numPurchaseUOMFactor * Case when ISNULL(Item.bitKitParent,0)=1 THEN 0 ELSE isnull(numOnOrder,0) END) AS DECIMAL(18,2)) as OnOrderUOM
		,CAST((@numPurchaseUOMFactor * Case when ISNULL(Item.bitKitParent,0)=1 THEN 0 ELSE isnull(numReorder,0) END) AS DECIMAL(18,2)) as ReorderUOM
		,CAST((@numSaleUOMFactor * Case when ISNULL(Item.bitKitParent,0)=1 THEN 0 ELSE isnull(numAllocation,0) END) AS DECIMAL(18,2)) as AllocationUOM
		,CAST((@numSaleUOMFactor * Case when ISNULL(Item.bitKitParent,0)=1 THEN 0 ELSE isnull(numBackOrder,0) END) AS DECIMAL(18,2)) as BackOrderUOM
		,CONCAT('[',STUFF((SELECT 
								CONCAT(', {"WarehouseItemID":',WIInner.numWareHouseItemID,',"WarehouseLocationID":',WIInner.numWLocationID,',"Warehouse":"',ISNULL(W.vcWareHouse,''),'", "OnHand":',ISNULL(WIInner.numOnHand,0),', "Location":"',ISNULL(WL.vcLocation,''),'"}') 
							FROM 
								WareHouseItems WIInner
							INNER JOIN
								Warehouses W
							ON
								WIInner.numWareHouseID = W.numWareHouseID
							INNER JOIN
								WarehouseLocation WL
							ON
								WIInner.numWLocationID = WL.numWLocationID
							WHERE 
								WIInner.numDomainID=@numDomainID 
								AND WIInner.numItemID=@numItemCode
								AND WIInner.numWareHouseID=WareHouseItems.numWareHouseID
							ORDER BY
								WL.vcLocation ASC
							FOR XML PATH('')),1,1,''),']') vcInternalLocations
		,(CASE WHEN EXISTS (SELECT numWareHouseItmsDTLID FROM WareHouseItmsDTL WHERE numWareHouseItemID=WareHouseItems.numWareHouseItemID AND ISNULL(numQty,0) > 0) THEN 1 ELSE 0 END) bitSerialLotExists
	FROM
		WareHouseItems
	INNER JOIN
		Item
	ON
		WareHouseItems.numItemID = Item.numItemCode
	INNER JOIN
		Warehouses
	ON
		WareHouseItems.numWareHouseID = Warehouses.numWareHouseID
	WHERE
		WareHouseItems.numDomainID=@numDomainID
		AND WareHouseItems.numItemID=@numItemCode
		AND ISNULL(WareHouseItems.numWLocationID,0) = 0
		AND (ISNULL(@numWarehouseID,0) = 0 OR WareHouseItems.numWareHouseID=@numWarehouseID)
		AND (ISNULL(@numWLocationID,0) = 0 OR EXISTS (SELECT WI.numWareHouseItemID FROM WareHouseItems WI WHERE WI.numDomainID=@numDomainID AND WI.numItemID=@numItemCode AND WI.numWareHouseID=@numWarehouseID AND ISNULL(WI.numWLocationID,0) = @numWLocationID))
END
GO
GO
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'USP_GetDealsList1_export') AND type in (N'P', N'PC'))
DROP PROCEDURE USP_GetDealsList1_export
GO 
CREATE  Procedure USP_GetDealsList1_export
 @numDomainID AS NUMERIC(9) = 0,
 @numRecOwner AS NUMERIC(18,0)=0,
 @numAssignTO AS NUMERIC(18,0)=0,
 @vcTimeLine AS VARCHAR(50)=''
as 
BEGIN
	DECLARE @dtStartDate DATETIME 
	DECLARE @dtEndDate AS DATETIME 
	if(@vcTimeline <>'')
	BEGIN 
		Set @dtStartDate = LEFT(@vcTimeLine,CHARINDEX('-',@vcTimeLine)-1)
		set @dtEndDate = RIGHT(@vcTimeLine,CHARINDEX('-',@vcTimeLine)-1)
	END 

	SELECT    
        opp.numrecowner        AS RecordOwnerId, 
		opp.numAssignedTo        AS AssignToId, 
        opp.numoppid, 
        opp.bintcreateddate as CreatedDate,
        opp.vcPOppName as [Name],
        isnull(ADC1.vcFirstname,'')+' '+isnull(ADC1.vcLastName,'') as RecordOwner,
		isnull(ADC2.vcFirstname,'')+' '+isnull(ADC2.vcLastName,'') as AssignTo,
        Isnull( 
        ( 
                SELECT Sum(Isnull(monamountpaid,0)) 
                FROM   opportunitybizdocs BD 
                WHERE  bd.numoppid = opp.numoppid 
                AND    Isnull(bitauthoritativebizdocs,0)=1),0) AmtPaid, 
        Isnull( 
        ( 
                SELECT Sum(Isnull(mondealamount,0)) 
                FROM   opportunitybizdocs BD 
                WHERE  bd.numoppid = opp.numoppid 
                AND    Isnull(bitauthoritativebizdocs,0)=1),0) as InvoiceGrandTotal, 
        opp.mondealamount as SalesOrderDealAmount
          
	FROM       opportunitymaster Opp 
	LEFT JOIN  additionalcontactsinformation ADC1 
	ON         opp.numRecOwner = ADC1.numcontactid 
	LEFT JOIN  additionalcontactsinformation ADC2 
	ON         opp.numAssignedTo = ADC2.numcontactid 

	WHERE      opp.numdomainid=@numDomainID
	AND        opp.tintoppstatus=1 

	AND        opp.tintopptype= 1 
	AND        (opp.numrecowner=@numRecOwner or  @numRecOwner=0)
	AND        (opp.numAssignedTo=@numAssignTO or @numAssignTO = 0) 
	AND        opp.bintcreateddate >= @dtStartDate--'04/01/2019' 
	AND        opp.bintcreateddate <= @dtEndDate -- '04/27/2020' 
	AND        Charindex('All', dbo.Checkorderinvoicingstatus(opp.numoppid,opp.numdomainid)) > 0 
	ORDER BY   opp.numOppId ASC 
END
GO
GO
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'USP_ItemList1_export') AND type in (N'P', N'PC'))
DROP PROCEDURE USP_ItemList1_export
GO
Create Procedure USP_ItemList1_export
 @numDomainID AS NUMERIC(9) = 0,
 @numWarehouseID AS NUMERIC(18,0)=0
as 
begin 

SELECT
	I.numItemCode as ItemId
	,I.vcItemName as Itemname
	,WareHouseItems.numWareHouseItemID
	,numOnHand AS OnHand
	,numAllocation AS OnAllocation
	,ISNULL(numOnHand,0) + ISNULL(numAllocation,0) AS TotalOnHandPlusAllocation
	,isnull(I.monAverageCost,0) as AverageCost
	,(ISNULL(numOnHand,0) + ISNULL(numAllocation,0)) * isnull(I.monAverageCost,0)  AS StockValue
	,numWareHouseID 
											
FROM
	WareHouseItems
	Inner join Item I on I.numItemCode = WareHouseItems.numItemID 
WHERE
	(WareHouseItems.numWareHouseID=@numWarehouseID OR @numWarehouseID=0)
	and  I.numDomainID = @numDomainID
	and ISNULL(I.IsArchieve,0) = 0 
	and (isnull(WarehouseItems.numOnHand,0) + isnull(WarehouseItems.numAllocation,0) >0) and (isnull(I.monAverageCost,0)>0)  
end 
GO
